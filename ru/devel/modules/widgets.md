[comment]: # translation:outdated

[comment]: # aside:3

[comment]: # ({new-7d26eea5})
# Widgets

Widgets are Zabbix frontend modules used for the dashboards.
Unless otherwise noted, all module guidelines are also applicable to widgets.

However, a widget is notably different from the module. To build a widget:

- specify the type "widget" in the [manifest.json file](/devel/modules/file_sructure/manifest) ("type": "widget").
- include at least two views: one for the [widget presentation mode](presentation#widget-view) and one for the [widget configuration mode](configuration#widget-configuration-view) (example.widget.view.php and example.widget.edit.php).
- and a [controller](presentation#widget-actions) for widget presentation (WidgetView.php).
- use and extend default [widget classes](configuration).

[comment]: # ({/new-7d26eea5})
