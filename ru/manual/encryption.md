[comment]: # translation:outdated

[comment]: # ({new-822969ff})
# 17. Шифрование

[comment]: # ({/new-822969ff})

[comment]: # ({new-0b118c23})
#### Обзор

Zabbix поддерживает шифрование соединений между Zabbix сервером, Zabbix
прокси, Zabbix агентом, zabbix\_sender и zabbix\_get утилитами с
использованием Transport Layer Security (TLS) протокола v.1.2.
Шифрование поддерживается начиная с Zabbix 3.0. Поддерживаются
шифрования на основе сертификата и на основе pre-shared ключа.

Шифрование опционально и настраивается для отдельных компонентов
(например, некоторые прокси и агенты можно настроить на использование
шифрования с сервером на основе сертификатов, в то время как другие
могут использовать шифрование на основе pre-shared ключа, а остальные
могут продолжать использовать незашифрованные соединения как и ранее).

Сервер (прокси) может использовать различные настройки с разными узлами
сети.

Программы Zabbix демонов слушают один порт для шифрованных и
незашифрованных входящих подключений. Добавление шифрования не потребует
открывать новые порты на брандмауэрах.

[comment]: # ({/new-0b118c23})

[comment]: # ({new-bdc6b202})
#### Ограничения

-   Приватные ключи хранятся в формате обычного текста в файлах, которые
    Zabbix компоненты считывают в процессе запуска.
-   Введенные pre-shared ключи в веб-интерфейсе Zabbix хранятся в базе
    данных Zabbix в виде обычного текста.
-   Встроенное шифрование не защищает коммуникации:

```{=html}
<!-- -->
```
       * между веб-сервером с веб-интерфейсом Zabbix и веб-браузером на строне пользователя,
       * между Zabbix веб-интерфейсом и Zabbix сервером,
       * между Zabbix сервером (прокси) и базой данных Zabbix.
    * В настоящее время каждое незашифрованное соединение открывается с полными TLS переговорами, кэширование сессий и билеты не реализованы.
    * Добавление шифрования увеличивает время проверок и действий, в зависимости от сетевых задержек.\\ Например, если пакет опаздывает на 100мс, тогда открытие TCP соединение и отправка незашифрованного запроса займет около 200мс.\\ При наличии шифрования на установку TLS соединения добавится около 1000 мс.\\ Возможно потребуется увеличить время ожидания, в противном случае некоторые элементы данных и действия, выполняющие удаленные скрипты на агентах смогут работать с незашифрованными соединениями,\\ но не смогут при шифрованном соединении (будет превышено время ожидания).

[comment]: # ({/new-bdc6b202})

[comment]: # ({new-ba7ecc58})
#### Компиляция Zabbix с поддержкой шифрования

Для поддержки шифрования Zabbix должен быть скомпилировать и связан с по
крайней мере одной крипто библиотекой:

-   *mbed TLS* (ранее *PolarSSL*)(версия 1.3.9 или более новые 1.3.x).
    *mbed TLS* 2.x в настоящее время не поддерживается, это не простая
    замена ветки 1.3, Zabbix не скомпилируется с *mbed TLS* 2.x.
-   *GnuTLS* (с версии 3.1.18)
-   *OpenSSL* (с версии 1.0.1)

Библиотека выбирается при помощи опции в скрипте "configure":

-   `--with-mbedtls[=DIR]`
-   `--with-gnutls[=DIR]`
-   `--with-openssl[=DIR]`

Например, чтобы сконфигурировать исходные коды сервера и агента с
*OpenSSL*, вы можете использовать что-то вроде:\
./configure --enable-server --enable-agent --with-mysql --enable-ipv6
--with-net-snmp --with-libcurl --with-libxml2 --with-openssl

Можно скомпилировать разные компоненты Zabbix с различными крипто
библиотеками (например, сервер с *OpenSSL*, агент с *GnuTLS*).

::: noteimportant
 Если вы планируете использовать pre-shared ключи
(PSK) рассмотрите возможность использования библиотек *GnuTLS* или *mbed
TLS* с компонентами Zabbix, использующих PSK. Библиотеки *GnuTLS* и
*mbed TLS* поддерживают наборы шифров PSK с [Совершенной прямой
секретностью](https://ru.wikipedia.org/wiki/Perfect_forward_secrecy)
(Perfect forward secrecy). *OpenSSL* библиотека (версии 1.0.1, 1.0.2c)
поддерживает PSK, но доступные наборы шифров PSK не обеспечивают
Совершенную прямую секретность. 
:::

[comment]: # ({/new-ba7ecc58})

[comment]: # ({new-2d0e2b80})
#### Управление зашированными соединениями

Соединения в Zabbix могут использовать:

-   без шифрования (по умолчанию)
-   [RSA шифрование на основе
    сертификатов](/ru/manual/encryption/using_certificates)
-   [шифрование на основе
    PSK](/ru/manual/encryption/using_pre_shared_keys)

Имеется два важных параметра, которые используются, чтобы указать
шифрование между компонентами Zabbix:

-   `TLSConnect`
-   `TLSAccept`

`TLSConnect` задает какое использовать шифрование и может принимать
[одно из 3]{.underline} значений (`unencrypted`, `PSK`, `certificate`).
`TLSConnect` используется в файлах конфигурации Zabbix прокси (в
активном режиме задает только подключения к серверу) и Zabbix agentd
(при активных проверках). В веб-интерфейсе Zabbix параметр `TLSConnect`
является эквивалентом поля *Подключения к узлу сети* с вкладки
*Настройка→Узлы сети→<какой-то узел сети>→Шифрование* и поля
*Подключения к прокси* с вкладки *Администрирование→Прокси→<какой-то
прокси>→Шифрование*. Если настроенный тип шифрования для соединения
завершится неудачей, другие типы шифрования не будут опробованы.

`TLSAccept` задает какой тип соединений разрешен при входящих
подключениях. Тип подключений: `unencrypted`, `PSK`, `certificate`.
Можно указать [одно или более]{.underline} значений. `TLSAccept`
используется в файлах конфигурации Zabbix прокси (в пассивном режиме
задает только соединения с сервера) и Zabbix agentd (при пассивных
проверках). В веб-интерфейсе Zabbix параметр `TLSAccept` является
эквивалентом поля *Соединения с узла сети* с вкладки *Настройка→Узлы
сети→<какой-то узел сети>→Шифрование* и поля "Соединения с прокси"
с вкладки *Администрирование→Прокси→<какой-то прокси>→Шифрование*.

Как правило, вы настраиваете только один тип шифрования для входящих
подключений. Но вы можете захотите переключить режим шифрования,
например с незашированного на основанный на сертификатах с минимальным
временем простоя и с возможностью отката. Для этого вы можете задать
`TLSAccept=unencrypted,cert` в файле конфигурации agentd и перезапустить
агента Zabbix.\
Затем вы можете протестировать подключение от `zabbix_get` к агенту,
используя сертификат. Если подключение работает, вы можете перенастроить
шифрование у этого агента в Zabbix веб-интерфейсе на вкладке
*Настройка→Узлы сети→<какой-то узел сети>→Шифрование*, переключив
настройку *Подключения к узлу сети* на "Сертификат".\
Когда кэш конфигурации сервера обновится (и конфигурация прокси
обновится, если узел сети наблюдается через прокси), тогда подключения к
этому агенту будут зашифрованы.\
Если всё работает как ожидается, вы можете задать `TLSAccept=cert` в
файле конфигурации агента и перезапустить Zabbix агента.\
Теперь агент будет принимать только зашифрованные подключения на основе
сертификатов. Незашифрованные и основанные на PSK подключения будут
отклонены.

Шифрование на сервере и прокси работает аналогичным образом. Если в
веб-интерфейсе Zabbix в настройке узла сети *Соединения с узла сети*
задано равным "Сертификат", тогда от агента (активные проверки) и
`zabbix_sender` (траппер элементы данных) будут приниматься только
зашифрованные соединения на основе сертификатов.

Скорее всего вы настроите входящие и исходящие соединения на
использование одного типа шифрования или без шифрования вовсе. Но,
технически, имеется возможность настроить шифрование асимметрично,
например, шифрование на основе сертификатов для входящих подключений и
на основе PSK для исходящих подключений.

Обзорные настройки шифрования отображаются в веб-интерфейсе Zabbix
*Настройка→Узлы сети* по каждому узлу сети по правой стороне, в колонке
*ШИФРОВАНИЕ АГЕНТА*. Примеры отображения настроек:

|Пример|Подключения К узлу сети|Разрешенные подключения ОТ узла сети|Отклоненные подключения С узла сети|
|------------|-------------------------------------------|--------------------------------------------------------------------|------------------------------------------------------------------|
|![none.png](../../assets/en/manual/encryption/none.png)|Незашифровано|Незашифровано|Зашифровано на основе сертификата и PSK|
|![cert.png](../../assets/en/manual/encryption/cert.png)|Зашифровано, на основе сертификата|Зашифровано, на основе сертификата|Незашифровано и на основе PSK|
|![psk\_psk.png](../../assets/en/manual/encryption/psk_psk.png)|Зашифровано на основе PSK|Зашифровано на основе PSK|Незашифровано и на основе сертификата|
|![psk\_none\_psk.png](../../assets/en/manual/encryption/psk_none_psk.png)|Зашифровано на основе PSK|Незашифровано и зашифровано на основе PSK|На основе сертификата|
|![all.png](../../assets/en/manual/encryption/all.png)|Зашифровано на основе сертификата|Незашифровано на основе PSK или зашифровано на основе сертификата|\-|

::: noteimportant
 По умолчанию используются незашифрованные
подключения. Шифрование необходимо настраивать по каждому узлу сети и
прокси отдельно. 
:::

[comment]: # ({/new-2d0e2b80})

[comment]: # ({new-ad15e6ff})
#### zabbix\_get и zabbix\_sender с шифрованием

Смотрите страницы помощи [zabbix\_get](/ru/manpages/zabbix_get) и
[zabbix\_sender](/ru/manpages/zabbix_sender) по использованию этих
утилит при наличии шифрования.

[comment]: # ({/new-ad15e6ff})

[comment]: # ({new-be2996e2})
#### Алгоритмы шифрования

Алгоритмы конфигурируются внутри в процессе запуска Zabbix и зависят от
крипто библиотеки, в настоящее время алгоритмы нельзя настраивать
пользователями.

Настроенные алгоритмы шифрования по типу библитеки с более высокого
уровня к низкому уровню:

|Библиотека|Алгоритмы шифрования сертификатов|Алгоритмы шифрования PSK|
|--------------------|----------------------------------------------------------------|-------------------------------------------|
|*mbed TLS (PolarSSL) 1.3.9*|TLS-ECDHE-RSA-WITH-AES-128-GCM-SHA256<br>TLS-ECDHE-RSA-WITH-AES-128-CBC-SHA256<br>TLS-ECDHE-RSA-WITH-AES-128-CBC-SHA<br>TLS-RSA-WITH-AES-128-GCM-SHA256<br>TLS-RSA-WITH-AES-128-CBC-SHA256<br>TLS-RSA-WITH-AES-128-CBC-SHA|TLS-ECDHE-PSK-WITH-AES-128-CBC-SHA256<br>TLS-ECDHE-PSK-WITH-AES-128-CBC-SHA<br>TLS-PSK-WITH-AES-128-GCM-SHA256<br>TLS-PSK-WITH-AES-128-CBC-SHA256<br>TLS-PSK-WITH-AES-128-CBC-SHA|
|*GnuTLS 3.1.18*|TLS\_ECDHE\_RSA\_AES\_128\_GCM\_SHA256<br>TLS\_ECDHE\_RSA\_AES\_128\_CBC\_SHA256<br>TLS\_ECDHE\_RSA\_AES\_128\_CBC\_SHA1<br>TLS\_RSA\_AES\_128\_GCM\_SHA256<br>TLS\_RSA\_AES\_128\_CBC\_SHA256<br>TLS\_RSA\_AES\_128\_CBC\_SHA1|TLS\_ECDHE\_PSK\_AES\_128\_CBC\_SHA256<br>TLS\_ECDHE\_PSK\_AES\_128\_CBC\_SHA1<br>TLS\_PSK\_AES\_128\_GCM\_SHA256<br>TLS\_PSK\_AES\_128\_CBC\_SHA256<br>TLS\_PSK\_AES\_128\_CBC\_SHA1|
|*OpenSSL 1.0.2c*|ECDHE-RSA-AES128-GCM-SHA256<br>ECDHE-RSA-AES128-SHA256<br>ECDHE-RSA-AES128-SHA<br>AES128-GCM-SHA256<br>AES128-SHA256<br>AES128-SHA|PSK-AES128-CBC-SHA|
|*OpenSSL 1.1.0*|ECDHE-RSA-AES128-GCM-SHA256<br>ECDHE-RSA-AES128-SHA256<br>ECDHE-RSA-AES128-SHA<br>AES128-GCM-SHA256<br>AES128-CCM8<br>AES128-CCM<br>AES128-SHA256<br>AES128-SHA<br>|ECDHE-PSK-AES128-CBC-SHA256<br>ECDHE-PSK-AES128-CBC-SHA<br>PSK-AES128-GCM-SHA256<br>PSK-AES128-CCM8<br>PSK-AES128-CCM<br>PSK-AES128-CBC-SHA256<br>PSK-AES128-CBC-SHA|

Алгоритмы шифрования при использовании сертификатов:

|   |   |   |   |
|---|---|---|---|
|<|TLS сервер|<|<|
|TLS клиент|*mbed TLS (PolarSSL)*|*GnuTLS*|*OpenSSL 1.0.2*|
|*mbed TLS (PolarSSL)*|TLS-ECDHE-RSA-WITH-AES-128-GCM-SHA256|TLS-ECDHE-RSA-WITH-AES-128-GCM-SHA256|TLS-ECDHE-RSA-WITH-AES-128-GCM-SHA256|
|*GnuTLS*|TLS-ECDHE-RSA-WITH-AES-128-GCM-SHA256|TLS-ECDHE-RSA-WITH-AES-128-GCM-SHA256|TLS-ECDHE-RSA-WITH-AES-128-GCM-SHA256|
|*OpenSSL 1.0.2*|TLS-ECDHE-RSA-WITH-AES-128-GCM-SHA256|TLS-ECDHE-RSA-WITH-AES-128-GCM-SHA256|TLS-ECDHE-RSA-WITH-AES-128-GCM-SHA256|

Алгоритмы шифрования при использовании PSK:

|   |   |   |   |
|---|---|---|---|
|<|TLS сервер|<|<|
|TLS клиент|*mbed TLS (PolarSSL)*|*GnuTLS*|*OpenSSL 1.0.2*|
|*mbed TLS (PolarSSL)*|TLS-ECDHE-PSK-WITH-AES-128-CBC-SHA256|TLS-ECDHE-PSK-WITH-AES-128-CBC-SHA256|TLS-PSK-WITH-AES-128-CBC-SHA|
|*GnuTLS*|TLS-ECDHE-PSK-WITH-AES-128-CBC-SHA256|TLS-ECDHE-PSK-WITH-AES-128-CBC-SHA256|TLS-PSK-WITH-AES-128-CBC-SHA|
|*OpenSSL 1.0.2*|TLS-PSK-WITH-AES-128-CBC-SHA|TLS-PSK-WITH-AES-128-CBC-SHA|TLS-PSK-WITH-AES-128-CBC-SHA|

[comment]: # ({/new-be2996e2})






[comment]: # ({new-b5e04652})
#### User-configured ciphersuites

The built-in ciphersuite selection criteria can be overridden with
user-configured ciphersuites.

::: noteimportant
User-configured ciphersuites is a feature intended
for advanced users who understand TLS ciphersuites, their security and
consequences of mistakes, and who are comfortable with TLS
troubleshooting.
:::

The built-in ciphersuite selection criteria can be overridden using the
following parameters:

|Override scope|Parameter|Value|Description|
|--------------|---------|-----|-----------|
|Ciphersuite selection for certificates|TLSCipherCert13|Valid OpenSSL 1.1.1 [cipher strings](https://www.openssl.org/docs/man1.1.1/man1/ciphers.html) for TLS 1.3 protocol (their values are passed to the OpenSSL function SSL\_CTX\_set\_ciphersuites()).|Certificate-based ciphersuite selection criteria for TLS 1.3<br><br>Only OpenSSL 1.1.1 or newer.|
|^|TLSCipherCert|Valid OpenSSL [cipher strings](https://www.openssl.org/docs/man1.1.1/man1/ciphers.html) for TLS 1.2 or valid GnuTLS [priority strings](https://gnutls.org/manual/html_node/Priority-Strings.html). Their values are passed to the SSL\_CTX\_set\_cipher\_list() or gnutls\_priority\_init() functions, respectively.|Certificate-based ciphersuite selection criteria for TLS 1.2/1.3 (GnuTLS), TLS 1.2 (OpenSSL)|
|Ciphersuite selection for PSK|TLSCipherPSK13|Valid OpenSSL 1.1.1 [cipher strings](https://www.openssl.org/docs/man1.1.1/man1/ciphers.html) for TLS 1.3 protocol (their values are passed to the OpenSSL function SSL\_CTX\_set\_ciphersuites()).|PSK-based ciphersuite selection criteria for TLS 1.3<br><br>Only OpenSSL 1.1.1 or newer.|
|^|TLSCipherPSK|Valid OpenSSL [cipher strings](https://www.openssl.org/docs/man1.1.1/man1/ciphers.html) for TLS 1.2 or valid GnuTLS [priority strings](https://gnutls.org/manual/html_node/Priority-Strings.html). Their values are passed to the SSL\_CTX\_set\_cipher\_list() or gnutls\_priority\_init() functions, respectively.|PSK-based ciphersuite selection criteria for TLS 1.2/1.3 (GnuTLS), TLS 1.2 (OpenSSL)|
|Combined ciphersuite list for certificate and PSK|TLSCipherAll13|Valid OpenSSL 1.1.1 [cipher strings](https://www.openssl.org/docs/man1.1.1/man1/ciphers.html) for TLS 1.3 protocol (their values are passed to the OpenSSL function SSL\_CTX\_set\_ciphersuites()).|Ciphersuite selection criteria for TLS 1.3<br><br>Only OpenSSL 1.1.1 or newer.|
|^|TLSCipherAll|Valid OpenSSL [cipher strings](https://www.openssl.org/docs/man1.1.1/man1/ciphers.html) for TLS 1.2 or valid GnuTLS [priority strings](https://gnutls.org/manual/html_node/Priority-Strings.html). Their values are passed to the SSL\_CTX\_set\_cipher\_list() or gnutls\_priority\_init() functions, respectively.|Ciphersuite selection criteria for TLS 1.2/1.3 (GnuTLS), TLS 1.2 (OpenSSL)|

To override the ciphersuite selection in
[zabbix\_get](/manpages/zabbix_get) and
[zabbix\_sender](/manpages/zabbix_sender) utilities - use the
command-line parameters:

-   `--tls-cipher13`
-   `--tls-cipher`

The new parameters are optional. If a parameter is not specified, the
internal default value is used. If a parameter is defined it cannot be
empty.

If the setting of a TLSCipher\* value in the crypto library fails then
the server, proxy or agent will not start and an error is logged.

It is important to understand when each parameter is applicable.

[comment]: # ({/new-b5e04652})

[comment]: # ({new-a74c28c7})
##### Outgoing connections

The simplest case is outgoing connections:

-   For outgoing connections with certificate - use TLSCipherCert13 or
    TLSCipherCert
-   For outgoing connections with PSK - use TLSCipherPSK13 and
    TLSCipherPSK
-   In case of zabbix\_get and zabbix\_sender utilities the command-line
    parameters `--tls-cipher13` and `--tls-cipher` can be used
    (encryption is unambiguously specified with a `--tls-connect`
    parameter)

[comment]: # ({/new-a74c28c7})

[comment]: # ({new-3edf3b2b})
##### Incoming connections

It is a bit more complicated with incoming connections because rules are
specific for components and configuration.

For Zabbix **agent**:

|Agent connection setup|Cipher configuration|
|----------------------|--------------------|
|TLSConnect=cert|TLSCipherCert, TLSCipherCert13|
|TLSConnect=psk|TLSCipherPSK, TLSCipherPSK13|
|TLSAccept=cert|TLSCipherCert, TLSCipherCert13|
|TLSAccept=psk|TLSCipherPSK, TLSCipherPSK13|
|TLSAccept=cert,psk|TLSCipherAll, TLSCipherAll13|

For Zabbix **server** and \*\* proxy\*\*:

|Connection setup|Cipher configuration|
|----------------|--------------------|
|Outgoing connections using PSK|TLSCipherPSK, TLSCipherPSK13|
|Incoming connections using certificates|TLSCipherAll, TLSCipherAll13|
|Incoming connections using PSK if server has no certificate|TLSCipherPSK, TLSCipherPSK13|
|Incoming connections using PSK if server has certificate|TLSCipherAll, TLSCipherAll13|

Some pattern can be seen in the two tables above:

-   TLSCipherAll and TLSCipherAll13 can be specified only if a combined
    list of certificate- **and** PSK-based ciphersuites is used. There
    are two cases when it takes place: server (proxy) with a configured
    certificate (PSK ciphersuites are always configured on server, proxy
    if crypto library supports PSK), agent configured to accept both
    certificate- and PSK-based incoming connections
-   in other cases TLSCipherCert\* and/or TLSCipherPSK\* are sufficient

The following tables show the `TLSCipher*` built-in default values. They
could be a good starting point for your own custom values.

|Parameter|GnuTLS 3.6.12|
|---------|-------------|
|TLSCipherCert|NONE:+VERS-TLS1.2:+ECDHE-RSA:+RSA:+AES-128-GCM:+AES-128-CBC:+AEAD:+SHA256:+SHA1:+CURVE-ALL:+COMP-NULL:+SIGN-ALL:+CTYPE-X.509|
|TLSCipherPSK|NONE:+VERS-TLS1.2:+ECDHE-PSK:+PSK:+AES-128-GCM:+AES-128-CBC:+AEAD:+SHA256:+SHA1:+CURVE-ALL:+COMP-NULL:+SIGN-ALL|
|TLSCipherAll|NONE:+VERS-TLS1.2:+ECDHE-RSA:+RSA:+ECDHE-PSK:+PSK:+AES-128-GCM:+AES-128-CBC:+AEAD:+SHA256:+SHA1:+CURVE-ALL:+COMP-NULL:+SIGN-ALL:+CTYPE-X.509|

|Parameter|OpenSSL 1.1.1d ^**1**^|
|---------|----------------------|
|TLSCipherCert13|<|
|TLSCipherCert|EECDH+aRSA+AES128:RSA+aRSA+AES128|
|TLSCipherPSK13|TLS\_CHACHA20\_POLY1305\_SHA256:TLS\_AES\_128\_GCM\_SHA256|
|TLSCipherPSK|kECDHEPSK+AES128:kPSK+AES128|
|TLSCipherAll13|<|
|TLSCipherAll|EECDH+aRSA+AES128:RSA+aRSA+AES128:kECDHEPSK+AES128:kPSK+AES128|

^**1**^ Default values are different for older OpenSSL versions (1.0.1,
1.0.2, 1.1.0), for LibreSSL and if OpenSSL is compiled without PSK
support.

\*\* Examples of user-configured ciphersuites \*\*

See below the following examples of user-configured ciphersuites:

-   [Testing cipher strings and allowing only PFS
    ciphersuites](#testing_cipher_strings_and_allowing_only_pfs_ciphersuites)
-   [Switching from AES128 to AES256](#switching_from_aes128_to_aes256)

[comment]: # ({/new-3edf3b2b})

[comment]: # ({new-bfb4ef88})
##### Testing cipher strings and allowing only PFS ciphersuites

To see which ciphersuites have been selected you need to set
'DebugLevel=4' in the configuration file, or use the `-vv` option for
zabbix\_sender.

Some experimenting with `TLSCipher*` parameters might be necessary
before you get the desired ciphersuites. It is inconvenient to restart
Zabbix server, proxy or agent multiple times just to tweak `TLSCipher*`
parameters. More convenient options are using zabbix\_sender or the
`openssl` command. Let's show both.

**1.** Using zabbix\_sender.

Let's make a test configuration file, for example
/home/zabbix/test.conf, with the syntax of a zabbix\_agentd.conf file:

      Hostname=nonexisting
      ServerActive=nonexisting
      
      TLSConnect=cert
      TLSCAFile=/home/zabbix/ca.crt
      TLSCertFile=/home/zabbix/agent.crt
      TLSKeyFile=/home/zabbix/agent.key
      TLSPSKIdentity=nonexisting
      TLSPSKFile=/home/zabbix/agent.psk

You need valid CA and agent certificates and PSK for this example.
Adjust certificate and PSK file paths and names for your environment.

If you are not using certificates, but only PSK, you can make a simpler
test file:

      Hostname=nonexisting
      ServerActive=nonexisting
      
      TLSConnect=psk
      TLSPSKIdentity=nonexisting
      TLSPSKFile=/home/zabbix/agentd.psk

The selected ciphersuites can be seen by running zabbix\_sender (example
compiled with OpenSSL 1.1.d):

      $ zabbix_sender -vv -c /home/zabbix/test.conf -k nonexisting_item -o 1 2>&1 | grep ciphersuites
      zabbix_sender [41271]: DEBUG: zbx_tls_init_child() certificate ciphersuites: TLS_AES_256_GCM_SHA384 TLS_CHACHA20_POLY1305_SHA256 TLS_AES_128_GCM_SHA256 ECDHE-RSA-AES128-GCM-SHA256 ECDHE-RSA-AES128-SHA256 ECDHE-RSA-AES128-SHA AES128-GCM-SHA256 AES128-CCM8 AES128-CCM AES128-SHA256 AES128-SHA
      zabbix_sender [41271]: DEBUG: zbx_tls_init_child() PSK ciphersuites: TLS_CHACHA20_POLY1305_SHA256 TLS_AES_128_GCM_SHA256 ECDHE-PSK-AES128-CBC-SHA256 ECDHE-PSK-AES128-CBC-SHA PSK-AES128-GCM-SHA256 PSK-AES128-CCM8 PSK-AES128-CCM PSK-AES128-CBC-SHA256 PSK-AES128-CBC-SHA
      zabbix_sender [41271]: DEBUG: zbx_tls_init_child() certificate and PSK ciphersuites: TLS_AES_256_GCM_SHA384 TLS_CHACHA20_POLY1305_SHA256 TLS_AES_128_GCM_SHA256 ECDHE-RSA-AES128-GCM-SHA256 ECDHE-RSA-AES128-SHA256 ECDHE-RSA-AES128-SHA AES128-GCM-SHA256 AES128-CCM8 AES128-CCM AES128-SHA256 AES128-SHA ECDHE-PSK-AES128-CBC-SHA256 ECDHE-PSK-AES128-CBC-SHA PSK-AES128-GCM-SHA256 PSK-AES128-CCM8 PSK-AES128-CCM PSK-AES128-CBC-SHA256 PSK-AES128-CBC-SHA

Here you see the ciphersuites selected by default. These default values
are chosen to ensure interoperability with Zabbix agents running on
systems with older OpenSSL versions (from 1.0.1).

With newer systems you can choose to tighten security by allowing only a
few ciphersuites, e.g. only ciphersuites with PFS (Perfect Forward
Secrecy). Let's try to allow only ciphersuites with PFS using
`TLSCipher*` parameters.

::: noteimportant
The result will not be interoperable with systems
using OpenSSL 1.0.1 and 1.0.2, if PSK is used. Certificate-based
encryption should work.
:::

Add two lines to the `test.conf` configuration file:

      TLSCipherCert=EECDH+aRSA+AES128
      TLSCipherPSK=kECDHEPSK+AES128

and test again:

      $ zabbix_sender -vv -c /home/zabbix/test.conf -k nonexisting_item -o 1 2>&1 | grep ciphersuites            
      zabbix_sender [42892]: DEBUG: zbx_tls_init_child() certificate ciphersuites: TLS_AES_256_GCM_SHA384 TLS_CHACHA20_POLY1305_SHA256 TLS_AES_128_GCM_SHA256 ECDHE-RSA-AES128-GCM-SHA256 ECDHE-RSA-AES128-SHA256 ECDHE-RSA-AES128-SHA        
      zabbix_sender [42892]: DEBUG: zbx_tls_init_child() PSK ciphersuites: TLS_CHACHA20_POLY1305_SHA256 TLS_AES_128_GCM_SHA256 ECDHE-PSK-AES128-CBC-SHA256 ECDHE-PSK-AES128-CBC-SHA        
      zabbix_sender [42892]: DEBUG: zbx_tls_init_child() certificate and PSK ciphersuites: TLS_AES_256_GCM_SHA384 TLS_CHACHA20_POLY1305_SHA256 TLS_AES_128_GCM_SHA256 ECDHE-RSA-AES128-GCM-SHA256 ECDHE-RSA-AES128-SHA256 ECDHE-RSA-AES128-SHA AES128-GCM-SHA256 AES128-CCM8 AES128-CCM AES128-SHA256 AES128-SHA ECDHE-PSK-AES128-CBC-SHA256 ECDHE-PSK-AES128-CBC-SHA PSK-AES128-GCM-SHA256 PSK-AES128-CCM8 PSK-AES128-CCM PSK-AES128-CBC-SHA256 PSK-AES128-CBC-SHA        

The "certificate ciphersuites" and "PSK ciphersuites" lists have changed
- they are shorter than before, only containing TLS 1.3 ciphersuites and
TLS 1.2 ECDHE-\* ciphersuites as expected.

**2.** TLSCipherAll and TLSCipherAll13 cannot be tested with
zabbix\_sender; they do not affect "certificate and PSK ciphersuites"
value shown in the example above. To tweak TLSCipherAll and
TLSCipherAll13 you need to experiment with the agent, proxy or server.

So, to allow only PFS ciphersuites you may need to add up to three
parameters

      TLSCipherCert=EECDH+aRSA+AES128
      TLSCipherPSK=kECDHEPSK+AES128
      TLSCipherAll=EECDH+aRSA+AES128:kECDHEPSK+AES128

to zabbix\_agentd.conf, zabbix\_proxy.conf and zabbix\_server\_conf if
each of them has a configured certificate and agent has also PSK.

If your Zabbix environment uses only PSK-based encryption and no
certificates, then only one:

      TLSCipherPSK=kECDHEPSK+AES128

Now that you understand how it works you can test the ciphersuite
selection even outside of Zabbix, with the `openssl` command. Let's test
all three `TLSCipher*` parameter values:

      $ openssl ciphers EECDH+aRSA+AES128 | sed 's/:/ /g'
      TLS_AES_256_GCM_SHA384 TLS_CHACHA20_POLY1305_SHA256 TLS_AES_128_GCM_SHA256 ECDHE-RSA-AES128-GCM-SHA256 ECDHE-RSA-AES128-SHA256 ECDHE-RSA-AES128-SHA
      $ openssl ciphers kECDHEPSK+AES128 | sed 's/:/ /g'
      TLS_AES_256_GCM_SHA384 TLS_CHACHA20_POLY1305_SHA256 TLS_AES_128_GCM_SHA256 ECDHE-PSK-AES128-CBC-SHA256 ECDHE-PSK-AES128-CBC-SHA
      $ openssl ciphers EECDH+aRSA+AES128:kECDHEPSK+AES128 | sed 's/:/ /g'
      TLS_AES_256_GCM_SHA384 TLS_CHACHA20_POLY1305_SHA256 TLS_AES_128_GCM_SHA256 ECDHE-RSA-AES128-GCM-SHA256 ECDHE-RSA-AES128-SHA256 ECDHE-RSA-AES128-SHA ECDHE-PSK-AES128-CBC-SHA256 ECDHE-PSK-AES128-CBC-SHA
      

You may prefer `openssl ciphers` with option `-V` for a more verbose
output:

      $ openssl ciphers -V EECDH+aRSA+AES128:kECDHEPSK+AES128
                0x13,0x02 - TLS_AES_256_GCM_SHA384  TLSv1.3 Kx=any      Au=any  Enc=AESGCM(256) Mac=AEAD
                0x13,0x03 - TLS_CHACHA20_POLY1305_SHA256 TLSv1.3 Kx=any      Au=any  Enc=CHACHA20/POLY1305(256) Mac=AEAD
                0x13,0x01 - TLS_AES_128_GCM_SHA256  TLSv1.3 Kx=any      Au=any  Enc=AESGCM(128) Mac=AEAD
                0xC0,0x2F - ECDHE-RSA-AES128-GCM-SHA256 TLSv1.2 Kx=ECDH     Au=RSA  Enc=AESGCM(128) Mac=AEAD
                0xC0,0x27 - ECDHE-RSA-AES128-SHA256 TLSv1.2 Kx=ECDH     Au=RSA  Enc=AES(128)  Mac=SHA256
                0xC0,0x13 - ECDHE-RSA-AES128-SHA    TLSv1 Kx=ECDH     Au=RSA  Enc=AES(128)  Mac=SHA1
                0xC0,0x37 - ECDHE-PSK-AES128-CBC-SHA256 TLSv1 Kx=ECDHEPSK Au=PSK  Enc=AES(128)  Mac=SHA256
                0xC0,0x35 - ECDHE-PSK-AES128-CBC-SHA TLSv1 Kx=ECDHEPSK Au=PSK  Enc=AES(128)  Mac=SHA1

Similarly, you can test the priority strings for GnuTLS:

      $ gnutls-cli -l --priority=NONE:+VERS-TLS1.2:+ECDHE-RSA:+AES-128-GCM:+AES-128-CBC:+AEAD:+SHA256:+CURVE-ALL:+COMP-NULL:+SIGN-ALL:+CTYPE-X.509
      Cipher suites for NONE:+VERS-TLS1.2:+ECDHE-RSA:+AES-128-GCM:+AES-128-CBC:+AEAD:+SHA256:+CURVE-ALL:+COMP-NULL:+SIGN-ALL:+CTYPE-X.509
      TLS_ECDHE_RSA_AES_128_GCM_SHA256                        0xc0, 0x2f      TLS1.2
      TLS_ECDHE_RSA_AES_128_CBC_SHA256                        0xc0, 0x27      TLS1.2
      
      Protocols: VERS-TLS1.2
      Ciphers: AES-128-GCM, AES-128-CBC
      MACs: AEAD, SHA256
      Key Exchange Algorithms: ECDHE-RSA
      Groups: GROUP-SECP256R1, GROUP-SECP384R1, GROUP-SECP521R1, GROUP-X25519, GROUP-X448, GROUP-FFDHE2048, GROUP-FFDHE3072, GROUP-FFDHE4096, GROUP-FFDHE6144, GROUP-FFDHE8192
      PK-signatures: SIGN-RSA-SHA256, SIGN-RSA-PSS-SHA256, SIGN-RSA-PSS-RSAE-SHA256, SIGN-ECDSA-SHA256, SIGN-ECDSA-SECP256R1-SHA256, SIGN-EdDSA-Ed25519, SIGN-RSA-SHA384, SIGN-RSA-PSS-SHA384, SIGN-RSA-PSS-RSAE-SHA384, SIGN-ECDSA-SHA384, SIGN-ECDSA-SECP384R1-SHA384, SIGN-EdDSA-Ed448, SIGN-RSA-SHA512, SIGN-RSA-PSS-SHA512, SIGN-RSA-PSS-RSAE-SHA512, SIGN-ECDSA-SHA512, SIGN-ECDSA-SECP521R1-SHA512, SIGN-RSA-SHA1, SIGN-ECDSA-SHA1

[comment]: # ({/new-bfb4ef88})

[comment]: # ({new-a549cda7})
##### Switching from AES128 to AES256

Zabbix uses AES128 as the built-in default for data. Let's assume you
are using certificates and want to switch to AES256, on OpenSSL 1.1.1.

This can be achieved by adding the respective parameters in
`zabbix_server.conf`:

      TLSCAFile=/home/zabbix/ca.crt
      TLSCertFile=/home/zabbix/server.crt
      TLSKeyFile=/home/zabbix/server.key
      TLSCipherCert13=TLS_AES_256_GCM_SHA384
      TLSCipherCert=EECDH+aRSA+AES256:-SHA1:-SHA384
      TLSCipherPSK13=TLS_CHACHA20_POLY1305_SHA256
      TLSCipherPSK=kECDHEPSK+AES256:-SHA1
      TLSCipherAll13=TLS_AES_256_GCM_SHA384
      TLSCipherAll=EECDH+aRSA+AES256:-SHA1:-SHA384

::: noteimportant
Although only certificate-related ciphersuites
will be used, `TLSCipherPSK*` parameters are defined as well to avoid
their default values which include less secure ciphers for wider
interoperability. PSK ciphersuites cannot be completely disabled on
server/proxy.
:::

And in `zabbix_agentd.conf`:

      TLSConnect=cert
      TLSAccept=cert
      TLSCAFile=/home/zabbix/ca.crt
      TLSCertFile=/home/zabbix/agent.crt
      TLSKeyFile=/home/zabbix/agent.key
      TLSCipherCert13=TLS_AES_256_GCM_SHA384
      TLSCipherCert=EECDH+aRSA+AES256:-SHA1:-SHA384

[comment]: # ({/new-a549cda7})
