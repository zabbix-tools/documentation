[comment]: # translation:outdated

[comment]: # ({00e36b2a-00e36b2a})
# 5 Java gateway

[comment]: # ({/00e36b2a-00e36b2a})

[comment]: # ({1389f3bc-02637e76})
#### Обзор

Начиная с Zabbix 2.0 появился новый демон Zabbix, называемый "Zabbix Java gateway", обеспечивающий нативную поддержку мониторинга JMX приложений. Zabbix Java gateway - это демон написанный на языке Java. Когда Zabbix сервер хочет знать значение конкретного JMX счетчика узла сети, он опрашивает Zabbix Java gateway, который используя [API управления JMX](http://java.sun.com/javase/technologies/core/mntr-mgmt/javamanagement/) опрашивает интересующее удаленное приложение. Приложению не требуется никакого дополнительного программного обеспечения, оно просто должно быть запущено с опцией командной строки `-Dcom.sun.management.jmxremote`.

Java gateway принимает входящие подключения от Zabbix сервера или прокси и может быть использован только как "пассивный прокси". Но в отличии от Zabbix прокси, Java gateway может использоваться с Zabbix прокси (тогда как один Zabbix прокси не может работать через другой Zabbix прокси). Доступ к каждому Java gateway настраивается непосредственно в файле конфигурации Zabbix сервера или прокси, таким образом только один Java gateway может быть настроен на Zabbix сервере или Zabbix прокси. Если у узла сети есть элементы данных типа **JMX агент** и элементы данных других типов, то только элементы данных **JMX агент** будут запрошены через Java gateway.

Когда элемент данных должен быть обновлен через Java gateway, Zabbix сервер или прокси подключается к Java gateway и запрашивает значение, Java gateway в свою очередь запрашивает это значение и возвращает серверу или прокси. Таким образом, Java gateway никакие значения не кэширует.

У Zabbix сервера и прокси есть специальный тип процессов, которые подключается к Java gateway, их количество настраивается опцией **StartJavaPollers**. Внутренне, Java gateway запускается несколькими потоками, настраиваемыми [опцией](/manual/appendix/config/zabbix_java) **START\_POLLERS**. На стороне сервера, если соединение занимает более чем **Timeout** секунд, оно будет завершено, но Java gateway может оставаться занят получением значения JMX счетчика. Чтобы решить эту проблему в  Java gateway имеется опция **TIMEOUT**, позволяющую указать время ожидания сетевых операций JMX.

Zabbix сервер и прокси будут пытаться максимально объединить запросы к одной цели JMX (зависит от интервалов обновления элементов данных) и отправлять их в Java gateway за одно подключение для достижения лучшей производительности.

Рекомендуется выставить значение **StartJavaPollers** меньшим или равным **START\_POLLERS**, в противном случае могут возникнуть ситуации, когда потоков Java gateway может не хватить для обслуживания входящих запросов; в таких случаях Java gateway использует ThreadPoolExecutor.CallerRunsPolicy, что означает, что основной поток будет обрабатывать входящий запрос и временно не будет принимать никакие новые запросы.

[comment]: # ({/1389f3bc-02637e76})

[comment]: # ({new-05197937})
When an item has to be updated over Java gateway, Zabbix server or proxy
will connect to the Java gateway and request the value, which Java
gateway in turn retrieves and passes back to the server or proxy. As
such, Java gateway does not cache any values.

Zabbix server or proxy has a specific type of processes that connect to
Java gateway, controlled by the option **StartJavaPollers**. Internally,
Java gateway starts multiple threads, controlled by the
**START\_POLLERS** [option](/manual/appendix/config/zabbix_java). On the
server side, if a connection takes more than **Timeout** seconds, it
will be terminated, but Java gateway might still be busy retrieving
value from the JMX counter. To solve this, there is the **TIMEOUT**
option in Java gateway that allows to set timeout for JMX network
operations.

[comment]: # ({/new-05197937})

[comment]: # ({new-475ef799})

Zabbix server or proxy will try to pool requests to a single JMX target
together as much as possible (affected by item intervals) and send them
to the Java gateway in a single connection for better performance.

It is suggested to have **StartJavaPollers** less than or equal to
**START\_POLLERS**, otherwise there might be situations when no threads
are available in the Java gateway to service incoming requests; in such
a case Java gateway uses ThreadPoolExecutor.CallerRunsPolicy, meaning
that the main thread will service the incoming request and temporarily[label](https://git.zabbix.com/projects/WEB/repos/documentation/compare)
will not accept any new requests.

If you are trying to monitor Wildfly-based Java applications with Zabbix Java gateway, please install the latest jboss-client.jar available on the [Wildfly download page](https://www.wildfly.org/downloads/).

[comment]: # ({/new-475ef799})

[comment]: # ({7ccd1d82-d8d54db7})
#### Получение Java gateway

Вы можете установить Java gateway как из исходных кодов, так и из пакетов, которые можно загрузить с [Zabbix веб-сайта](https://www.zabbix.com/ru/download).

Воспользовавшись ссылками ниже, вы сможете получить информацию о том как получить и запустить Zabbix Java gateway, как настроить Zabbix сервер (или Zabbix прокси) на использование Zabbix Java gateway для мониторинга JMX, и как настроить элементы данных Zabbix в веб-интерфейсе Zabbix, которые соответствуют частным JMX счетчикам.

|Установка с|Инструкции|Инструкции|
|-----------------|------------|------------|
|*Исходных кодов*|[Инсталляция](/manual/installation/install#установка_java_gateway)|[Установка](/manual/concepts/java/from_sources)|
|*RHEL/CentOS пакетов*|[Инсталляция](/manual/installation/install_from_packages/rhel_centos#установка_java_gateway)|[Инсталляция](/manual/concepts/java/from_rhel_centos)|
|*Debian/Ubuntu пакетов*|[Инсталляция](/manual/installation/install_from_packages/debian_ubuntu#установка_java_gateway)|[Инсталляция](/manual/concepts/java/from_debian_ubuntu)|

[comment]: # ({/7ccd1d82-d8d54db7})
