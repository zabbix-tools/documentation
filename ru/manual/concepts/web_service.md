[comment]: # translation:outdated

[comment]: # ({bc553de2-8e572e45})
# 9 Веб-сервис

[comment]: # ({/bc553de2-8e572e45})

[comment]: # ({fedfaed0-cac50ff6})
#### Обзор

Zabbix веб-сервис - это процесс, который используется для коммуникации с внешними веб-сервисами. В данный момент веб-сервис Zabbix используется для формирования и отправки отчетов по расписанию, также имеются планы на добавление дополнительной функциональности в будущем.

Zabbix сервер подключается к веб-сервису через HTTP(S). Веб-сервису Zabbix требуется Google Chrome, установленный на этом же хосте; на некоторых дистрибутивах сервис также может работать с Chromium (смотрите [известные проблемы](/manual/installation/known_issues#chromium_для_веб_сервиса_zabbix_на_ubuntu_20)).

[comment]: # ({/fedfaed0-cac50ff6})

[comment]: # ({a92e9375-704910f2})
#### Установка

Веб-сервис Zabbix доступен в уже подготовленных скомпилированных пакетах Zabbix для загрузки с [веб-сайта Zabbix](http://www.zabbix.com/download.php). Для компиляции [веб-сервиса Zabbix](/manual/installation/install#установка_веб_сервиса_zabbix) из исходных кодов необходимо указать `--enable-webservice` опцию конфигурирования.

**Смотрите также:**

-   Опции файла конфигурации для [zabbix\_web\_service](/manual/appendix/config/zabbix_web_service);
-   [Настройка отчетов по расписанию](/manual/appendix/install/web_service)

[comment]: # ({/a92e9375-704910f2})
