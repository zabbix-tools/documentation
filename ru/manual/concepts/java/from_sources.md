[comment]: # translation:outdated

[comment]: # ({6c72b087-5380e993})
# - Установка из исходных кодов

[comment]: # ({/6c72b087-5380e993})

[comment]: # ({3231d230-8d0f220e})
#### Обзор

Если Zabbix Java Gateway [устанлвлен](/manual/installation/install#установка_java_gateway) из исходных кодов, следующая информация поможет вам настроить Zabbix [Java gateway](/manual/concepts/java).

[comment]: # ({/3231d230-8d0f220e})

[comment]: # ({38b0fcf1-305c7876})
#### Обзор файлов

Если вы получили Java gateway из исходных кодов, вы должны были получить набор скриптов командной строки, JAR и файлы конфигурации в папке $ПРЕФИКС/sbin/zabbix\_java. Суть этих файлов отражена ниже.

    bin/zabbix-java-gateway-$ВЕРСИЯ.jar

Собственно JAR файл Java gateway.

    lib/logback-core-0.9.27.jar
    lib/logback-classic-0.9.27.jar
    lib/slf4j-api-1.6.1.jar
    lib/android-json-4.3_r3.1.jar

Зависимости Java gateway: [Logback](http://logback.qos.ch/), [SLF4J](http://www.slf4j.org/) и библиотека [Android JSON](https://android.googlesource.com/platform/libcore/+/master/json).

    lib/logback.xml  
    lib/logback-console.xml

Файлы конфигурации для Logback.

    shutdown.sh  
    startup.sh

Скрипты для удобства запуска и остановки Java gateway.

    settings.sh

Файл конфигурации, который используется вышеупомянутыми скриптами запуска и остановки.

[comment]: # ({/38b0fcf1-305c7876})

[comment]: # ({a7c24984-571dbbe2})
#### Configuring and running Java gateway

По умолчанию, Java gateway слушает порт 10052. Если вы планируете работу Java gateway на другом порту, то вы можете указать его в скрипте settings.sh. Смотрите описание [файла конфигурации Java gateway](/manual/appendix/config/zabbix_java) для получения сведений о том как указать эту и другие опции.

::: notewarning
Порт 10052 не [зарегистрирован в IANA](http://www.iana.org/assignments/service-names-port-numbers/service-names-port-numbers.txt).
:::

Выполнив настройки, вы можете запустить Java gateway, выполнив скрипт запуска:

    $ ./startup.sh

Точно так же, если вам более не требуется Java gateway, выполните скрипт завершения работы для остановки Java gateway:

    $ ./shutdown.sh

Обратите внимание, что в отличии от сервера и прокси, Java gateway легок и не требует наличия какой-либо базы данных.

[comment]: # ({/a7c24984-571dbbe2})

[comment]: # ({312fb355-81ca4902})
#### Настройка сервера для использования с Java gateway

Когда Java gateway запущен и работает, вы должны указать Zabbix серверу где искать Zabbix Java gateway. Чтобы это сделать, укажите параметры JavaGateway и JavaGatewayPort в [файле конфигурации сервера](/manual/appendix/config/zabbix_server). Если же узел сети на котором работает JMX приложение наблюдается через Zabbix прокси, то параметры соединения указываются в [файле конфигурации прокси](/manual/appendix/config/zabbix_proxy).

    JavaGateway=192.168.3.14
    JavaGatewayPort=10052

По умолчанию, сервер нe запускает процессы связанные с мониторингом JMX. Если же вы хотите использовать этот тип мониторинга, то вам нужно указать количество экземпляров Java поллеров. Вы можете это сделать таким же способом как и изменение количества поллеров и трапперов.

    StartJavaPollers=5

Не забудьте перезапустить сервер или прокси после того как закончите изменение настроек.

[comment]: # ({/312fb355-81ca4902})

[comment]: # ({56f8f6a9-d86274ab})
#### Отладка Java gateway

В случае возникновения каких-либо проблем с Java gateway или в случае, если сообщение об ошибке элемента данных в веб-интерфейсе недостаточно информативно, вы можете обратиться к файлу журнала Java gateway.

По умолчанию, Java gateway записывает журнал в файл /tmp/zabbix\_java.log с уровнем журналирования "info". Бывает, что этой информации недостаточно и требуется информация уровня журналирования "debug". Чтобы увеличить уровень журналирования, отредактируйте файл lib/logback.xml и измените атрибут level тега <root> на значение "debug":

    <root level="debug">
      <appender-ref ref="FILE" />
    </root>

Обратите внимание, что в отличии от Zabbix сервера или Zabbix прокси, вам не нужно перезапускать Zabbix Java gateway после изменения файла logback.xml - изменения в logback.xml будут применены автоматически. Когда вы завершите отладку, вы можете вернуть уровень журналирования обратно в "info".

Если вы хотите записывать журнал в другой файл или в совершенно другой носитель такой как база данных, настройте файл logback.xml в соответствии с вашими потребностями. Обратитесь к [Руководству по Logback](http://logback.qos.ch/manual/)  для получения более подробных сведений.

Иногда для отладки полезно запустить Java gateway консольном приложением, а не как демоном. Чтобы это сделать, закомментируйте переменную PID\_FILE в settings.sh. Если PID\_FILE не указан, скрипт startup.sh запускает Java gateway консольным приложением, при этом Logback использует файл lib/logback-console.xml, который не только выводит журнал в консоль, но и имеет уровень журналирования "debug".

В заключение, отметим, поскольку Java gateway использует SLF4J для журналирования, вы можете заменить Logback выбранным вами фреймворком, поместив соответствующий JAR файл в каталог lib. Обратитесь к [Руководству по SLF4](http://www.slf4j.org/manual.html) для получения более подробных сведений.

[comment]: # ({/56f8f6a9-d86274ab})

[comment]: # ({7d729478-4332cfb8})
#### Мониторинг JMX

Смотрите страницу [JMX мониторинга](/manual/config/items/itemtypes/jmx_monitoring) для получения более подробных сведений.

[comment]: # ({/7d729478-4332cfb8})
