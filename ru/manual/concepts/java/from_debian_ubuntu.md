[comment]: # translation:outdated

[comment]: # ({9bc27d5e-e983a9df})
# 3 Установка из Debian/Ubuntu пакетов

[comment]: # ({/9bc27d5e-e983a9df})

[comment]: # ({92314dbb-a044dd01})
#### Обзор

Если Zabbix Java Gateway [установлен](/manual/installation/install_from_packages/debian_ubuntu#установка_java_gateway) из Debian/Ubuntu пакетов, следующая информация вам поможет в настройке Zabbix [Java gateway](/manual/concepts/java).

[comment]: # ({/92314dbb-a044dd01})

[comment]: # ({a3b7cd5c-01191552})
#### Настройка и запуск Java gateway

Параметры конфигурации Zabbix Java gateway можно настроить в следующем файле:

    /etc/zabbix/zabbix_java_gateway.conf

Для получении более подробных сведений смотрите [параметры](/manual/appendix/config/zabbix_java) настройки Zabbix Java.

Чтобы запустить Zabbix Java gateway:

    # service zabbix-java-gateway restart

Чтобы автоматически запускать Zabbix Java gateway при загрузке системы:

    # systemctl enable zabbix-java-gateway

[comment]: # ({/a3b7cd5c-01191552})

[comment]: # ({312fb355-81ca4902})
#### Настройка сервера для использования с Java gateway

Когда Java gateway запущен и работает, вы должны указать Zabbix серверу где искать Zabbix Java gateway. Чтобы это сделать, укажите параметры JavaGateway и JavaGatewayPort в [файле конфигурации сервера](/manual/appendix/config/zabbix_server). Если же узел сети на котором работает JMX приложение наблюдается через Zabbix прокси, то параметры соединения указываются в [файле конфигурации прокси](/manual/appendix/config/zabbix_proxy).

    JavaGateway=192.168.3.14
    JavaGatewayPort=10052

По умолчанию, сервер нe запускает процессы связанные с мониторингом JMX. Если же вы хотите использовать этот тип мониторинга, то вам нужно указать количество экземпляров Java поллеров. Вы можете это сделать таким же способом как и изменение количества поллеров и трапперов.

    StartJavaPollers=5

Не забудьте перезапустить сервер или прокси после того как закончите изменение настроек.

[comment]: # ({/312fb355-81ca4902})

[comment]: # ({00ffa698-05f991d0})
#### Отладка Java gateway

Файл журнала Zabbix Java gateway:

    /var/log/zabbix/zabbix_java_gateway.log

Если вы желаете увеличить уровень журналирования, отредактируйте следующий файл:

    /etc/zabbix/zabbix_java_gateway_logback.xml

и измените `level="info"` на "debug" или, даже,  "trace" (для более глубокой отладки):

    <configuration scan="true" scanPeriod="15 seconds">
    [...]
          <root level="info">
                  <appender-ref ref="FILE" />
          </root>

    </configuration>

[comment]: # ({/00ffa698-05f991d0})

[comment]: # ({7d729478-4332cfb8})
#### Мониторинг JMX

Смотрите страницу [JMX мониторинга](/manual/config/items/itemtypes/jmx_monitoring) для получения более подробных сведений.

[comment]: # ({/7d729478-4332cfb8})
