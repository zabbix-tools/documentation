[comment]: # translation:outdated

[comment]: # ({4a0724dd-d60be363})
# 3 Агент 2

[comment]: # ({/4a0724dd-d60be363})

[comment]: # ({new-734dc3ed})
#### Обзор

Zabbix агент 2 - новое поколение Zabbix агента, его можно использовать в качестве замены Zabbix агента. Zabbix агент 2 разработан для:

-   уменьшения количества TCP соединений
-   улучшения многопоточности проверок
-   легкого расширения при помощи плагинов. Плагин должен иметь следующее:
    -   обеспечивать простые проверки, состоящие только из нескольких строк кода
    -   обеспечивать сложные проверки, состоящие из длительно выполняемых скриптов и автономного сбора данных с периодической отправкой данных обратно
-   простой замены Zabbix агента (поскольку он поддерживает весь предыдущий функционал)

Agent 2 написан на Go (с частичным использованием C кода из Zabbix агента). Для сборки Zabbix агент 2 требуется подготовленная среда Go с поддерживаемой в настоящее время [версией Go](https://golang.org/doc/devel/release#policy).

Agent 2 не имеет встроенной поддержки режима демона на Linux; он может быть запущен [службой Windows](/manual/appendix/install/windows_agent).

Пассивные проверки работают аналогично Zabbix агенту. Активные проверки поддерживают интервалы по расписанию/гибкие интервалы, также проверки выполняются параллельно в пределах одного активного сервера.

**Параллелизм проверок**

Проверки из разных плагинов могут выполняться параллельно. Количество параллельных проверок в пределах одного плагина ограничено настройкой производительности плагина. Каждый плагин может иметь жестко зашитую в код настройку производительности (по умолчанию, 100), значение которой можно уменьшить, указав `Plugins.<Имя плагина>.Capacity=N` настройку при настройке *Plugins* [параметра](#файл_конфигурации).

Смотрите также: [Методические указания по разработке плагинов](https://www.zabbix.com/documentation/guidelines/plugins).

[comment]: # ({/new-734dc3ed})

[comment]: # ({new-eaa0f2bb})
##### Passive and active checks

Passive checks work similarly to Zabbix agent. Active checks support
scheduled/flexible intervals and check concurrency within one active
server. 

::: noteclassic
  By default, after a restart, Zabbix agent 2 will schedule the first data collection for active checks at a conditionally random time within the item's update interval to prevent spikes in resource usage. To perform active checks that do not have *Scheduling* [update interval](/manual/config/items/item/custom_intervals#scheduling-intervals) immediately after the agent restart, set `ForceActiveChecksOnStart` parameter (global-level) or `Plugins.<Plugin name>.System.ForceActiveChecksOnStart` (affects only specific plugin checks) in the [configuration file](/manual/appendix/config/zabbix_agent2). Plugin-level parameter, if set, will override the global parameter. Forcing active checks on start is supported since Zabbix 6.0.2. 
:::

[comment]: # ({/new-eaa0f2bb})

[comment]: # ({new-05ea1336})
##### Check concurrency

Checks from different plugins can be executed concurrently. The number
of concurrent checks within one plugin is limited by the plugin capacity
setting. Each plugin may have a hardcoded capacity setting (100 being
default) that can be lowered using the `Plugins.<PluginName>.System.Capacity=N` setting in the *Plugins*
configuration [parameter](#configuration_file). Former name of this parameter `Plugins.<PluginName>.Capacity` is still supported, but has been deprecated in Zabbix 6.0.

[comment]: # ({/new-05ea1336})

[comment]: # ({3eea6f39-a2064519})
#### Поддерживаемые платформы

Агент 2 поддерживается на Linux и Windows платформах.

Для установки из пакетов Агент 2 поддерживается на:

-   RHEL/CentOS 6, 7, 8
-   SLES 15 SP1+
-   Debian 9, 10
-   Ubuntu 18.04, 20.04

На Windows Агент 2 поддерживается на:

-   Windows Server 2008 R2 и более поздние
-   Windows 7 и более поздние

[comment]: # ({/3eea6f39-a2064519})

[comment]: # ({9dd54ff7-cabc0f5f})
#### Инсталляция

Zabbix агент 2 доступен в уже подготовленных, скомпилированных пакетах Zabbix. Для компиляции Zabbix агента 2 из исходных кодов вам необходимо указать опцию конфигурации `--enable-agent2`.

[comment]: # ({/9dd54ff7-cabc0f5f})

[comment]: # ({1aa432f8-45c02f12})
#### Опции

Следующие параметры командной строки могут быть использованы с Zabbix агентом 2:

|**Параметр**|**Описание**|
|-------------|---------------|
|-c --config <файл-конфигурации>|Путь к файлу конфигурации.<br>Вы можете использовать данную опцию, чтобы задать файл конфигурации, размещенный в папке отличной от заданной по умолчанию.<br>В UNIX, путь по умолчанию /usr/local/etc/zabbix\_agent2.conf или как задано [во время компиляции](/manual/installation/install#установка_демонов_zabbix) переменными *--sysconfdir* или *--prefix*|
|-f --foreground|Запуск Zabbix агента в фоновом режиме (по умолчанию: true).|
|-p --print|Вывод известых элементов данных и выход.<br>*Обратите внимание*: Чтобы также получить результаты [пользовательских параметров](/manual/config/items/userparameters), вам необходимо указать файл конфигурации (если он находится вне папки заданной по умолчанию).|
|-t --test <ключ элемента данных>|Тестирование указанного элемента данных и выход.<br>*Обратите внимание*: Чтобы также получить результат [пользовательского параметра](/manual/config/items/userparameters), вам необходимо указать файл конфигурации (если он находится вне папки заданной по умолчанию).|
|-h --help|Вывод справочной информации и выход.|
|-v --verbose|Вывод отладочной информации. Используйте эту опцию совместно с -p и -t опциями.|
|-V --version|Вывод номера версии агента и выход.|
|-R --runtime-control <опция>|Выполнение административных функций. Смотрите [управление работой](/manual/concepts/agent2#управление_работой).|

Отдельные **примеры** использования параметров командной строки:

-   отображение всех встроенных элементов данных с их значениями
-   тестирование пользовательского параметра с ключом "mysql.ping" заданном в указанном файле конфигурации

```{=html}
<!-- -->
```
    shell> zabbix_agent2 --print
    shell> zabbix_agent2 -t "mysql.ping" -c /etc/zabbix/zabbix_agentd.conf

[comment]: # ({/1aa432f8-45c02f12})

[comment]: # ({ac1195ec-11aa0de0})
##### Управление работой

Управление работой агента предоставляет некоторые опции для удаленного управления агентом.

|Опция|Описание|
|------|-----------|
|log\_level\_increase|Увеличение уровня журналирования.|
|log\_level\_decrease|Уменьшение уровня журналирования.|
|metrics|Список доступных метрик.|
|version|Отображение версии агента.|
|userparameter\_reload|Перезагрузка пользовательских параметров из текущего файла конфигурации.<br>Обратите внимание, из всех опций конфигурации агента можно перезагрузить только UserParameter.|
|help|Отображение справочной информации о функции управления работой.|

Примеры:

-   увеличение уровня журналирования для агента 2
-   вывод опций управления работой

```{=html}
<!-- -->
```
    shell> zabbix_agent2 -R log_level_increase
    shell> zabbix_agent2 -R help

[comment]: # ({/ac1195ec-11aa0de0})

[comment]: # ({7e2b5614-77e9b590})
#### Файл конфигурации

Параметры конфигурации агента 2 большей частью совместимы с Zabbix агентом за несколькими исключениями.

|Новые параметры|Описание|
|--------------|-----------|
|*ControlSocket*|Путь к сокету управления работой. Агент 2 использует контрольный сокет для [административных команд](#управление_работой).|
|*EnablePersistentBuffer*,<br>*PersistentBufferFile*,<br>*PersistentBufferPeriod*|Эти параметры используются для настройки постоянного хранилища у агент 2 для активных элементов данных.|
|*Plugins*|Плагины могут иметь свои собственные параметры, в формате `Plugins.<Имя плагина>.<Параметр>=<значение>`. Общий параметр плагинов - *Capacity* управляет ограничением количества проверок, которые могут быть выполнены одновременно.|
|*StatusPort*|Этот порт агент 2 будет слушать для запросов HTTP состояния и отображать список настроенных плагинов с некоторыми внутренними параметрами|
|Исключенные параметры|Описание|
|*AllowRoot*, *User*|Не поддерживается, так как режим демона не поддерживается.|
|*LoadModule*, *LoadModulePath*|Загружаемые модули не поддерживаются.|
|*StartAgents*|Этот параметр использовался в Zabbix агенте для увеличения параллелизма пассивных проверок или для их отключения. В Агенте 2 параллелизм настраивается на уровне плагинов и может быть ограничен настройкой производительности. Между тем полное отключение пассивных проверок в настоящее время не поддерживается.|
|*HostInterface*, *HostInterfaceItem*|Ещё не поддерживается.|

Для получения подробной информации смотрите параметры файла конфигурации по настройке [zabbix\_agent2](/manual/appendix/config/zabbix_agent2).

[comment]: # ({/7e2b5614-77e9b590})

[comment]: # ({0cfcb718-472820ac})
#### Коды выхода

Начиная с версии 4.4.8 Zabbix агент 2 также может быть скомпилирован с более старыми версиями OpenSSL (1.0.1, 1.0.2).

В этом случае Zabbix предоставляет мьютексы для блокировки в OpenSSL. Если блокировка или разблокировка мьютекса не удалась, то в стандартный поток ошибок (STDERR) выводится сообщение об ошибке, и агент 2 завершает работу, возвращая код 2 или 3 соответственно.

[comment]: # ({/0cfcb718-472820ac})
