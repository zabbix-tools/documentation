[comment]: # translation:outdated

[comment]: # ({7b3943c6-fb8f0d5a})
# 1 Кластер высокой доступности

[comment]: # ({/7b3943c6-fb8f0d5a})

[comment]: # ({e09aa40d-ea8bc3db})
#### Обзор

Режим высокой доступности предлагает защиту от сбоев программного обеспечения / аппаратного обеспечения для Zabbix сервера и позволяет свести к минимуму время простоя в течении времени обслуживания программного обеспечения / аппаратного обеспечения.

Кластер высокой доступности (HA) является опциональным решением и поддерживается для Zabbix сервера. Собственное решение HA разработано с учетом более простого использования, это решение будет работать между разными площадками и не потребует никаких особых требований к базам данных, которые распознаются Zabbix. Пользователи могут свободно выбирать использовать собственное решение Zabbix HA или же стороннее решение HA, в зависимости от того, какое решение наилучшим образом подходит под требования высокой доступности в их среде.

Решение состоит из нескольких экземпляров zabbix\_server или нод. Каждая нода:

-   настраивается отдельно (файл конфигурации, скрипты, шифрование, экспорт данных)
-   использует ту же базу данных
-   имеет несколько режимов: активная, резервная, недоступная, остановленная

Только одна нода может быть активной (рабочей) в один момент времени. Резервные ноды не выполняют сбор данных, обработку или какие-либо другие обычные действия сервера; они не слушают порты; имеют минимальное количество подключений к базе данных.

Как активные ноды, так и резервные ноды, эти ноды выполняют обновление времени своего последнего доступа каждые 5 секунд. Каждая резервная нода наблюдает за временем последнего доступа активной ноды. Если время последнего доступа активной ноды превышает 'задержку при аварийном переключении' в секундах, резервная нода переключает себя в в режим активной ноды и назначает состояние 'недоступна' ранее активной ноде.

Активная нода отслеживает свое собственное подключение к базе данных - если подключение потеряно более чем на `задержка при аварийном переключении-5` секунд, нода должна остановить всю обработку и переключиться в резервный режим. Активная нода также отслеживает состояния резервных нод - если время последнего доступа резервной ноды превышает 'задержку при аварийном переключении' в секундах, резервной ноде присваивается состояние 'недоступная'.

Задержка при аварийном переключении настраивается. Минимальное значение задержки - 10 секунд.

Ноды разработаны с учетом совместимости между минорными версиями Zabbix.

[comment]: # ({/e09aa40d-ea8bc3db})

[comment]: # ({2c073384-567d3671})
#### Включение HA кластера

[comment]: # ({/2c073384-567d3671})

[comment]: # ({692aeaf0-23e81771})
##### Настройка сервера

Чтобы переключить любой Zabbix сервер с автономного сервера на ноду HA кластера, укажите HANodeName параметр в [конфигурации](/manual/appendix/config/zabbix_server) сервера.

Параметр NodeAddress (адрес:порт), если указан, должен использовать веб-интерфейсом для подключения к активной ноде, переопределяя значение в zabbix.conf.php.

[comment]: # ({/692aeaf0-23e81771})

[comment]: # ({new-3e9035e5})
##### Preparing frontend

Make sure that Zabbix server address:port is **not defined** in the 
frontend configuration.

Zabbix frontend will autodetect the active node by reading settings 
from the nodes table in Zabbix database. Node address of the active node 
will be used as the Zabbix server address.

[comment]: # ({/new-3e9035e5})

[comment]: # ({1764fc5a-d47b6ef3})
##### Настройка прокси

Для активации подключения к нескольких серверам в инсталляции с высокой доступностью перечислите адреса HA нод в Server [параметре](/manual/appendix/config/zabbix_proxy) прокси, разделив адреса точкой с запятой.

[comment]: # ({/1764fc5a-d47b6ef3})

[comment]: # ({39f057e1-f2fc0e77})
##### Настройка агента

Для активации подключения к нескольким серверам в инсталляции с высокой доступностью перечислите адреса HA нод в ServerActive [параметре](/manual/appendix/config/zabbix_agentd) агента, разделив адреса точкой с запятой.

[comment]: # ({/39f057e1-f2fc0e77})

[comment]: # ({new-311341fc})
### Failover to standby node

Zabbix will fail over to another node automatically if the active node stops. There 
must be at least one node in standby status for the failover to happen.

How fast will the failover be? All nodes update their last access time (and status, if 
it is changed) every 5 seconds. So: 

-   If the active node shuts down and manages to report its status 
as "shut down", another node will take over within **5 seconds**.

-   If the active node shuts down/becomes unavailable without being able to update 
its status, standby nodes will wait for the **failover delay** + 5 seconds to take over

The failover delay is configurable, with the supported range between 10 seconds and 15 
minutes (one minute by default). To change the failover delay, you may run:

```
zabbix_server -R ha_set_failover_delay=5m
```

[comment]: # ({/new-311341fc})

[comment]: # ({e75c5c59-593144b8})
#### Управление HA кластером

Текущим состоянием HA кластера можно управлять при помощи выделенных опций [управления работой](/manual/concepts/server#управление_работой):

-   ha\_status - вывод состояния HA кластера в журнал Zabbix сервера;
-   ha\_remove\_node=цель - удаление HA ноды заданной своим <цель> - номером активной ноды в списке (номер можно получить с вывода выполнения ha\_status). Обратите внимание, что активные / резервные ноды нельзя удалить.
-   ha\_set\_failover\_delay=задержка - установка задержки аварийного переключения HA (поддерживаются суффиксы времени, например, 10s, 1m)

Состояние нод можно отслеживать:

-   в *Отчеты* → *[Информация о системе](/manual/web_interface/frontend_sections/reports/status_of_zabbix#состояние_нод_ha_кластера)*
-   в виджете *Информация о системе* панели
-   используя опцию `ha_status` управления работой на стороне сервера (см. выше).

Для обнаружения нод можно использовать внутренний элемент данных `zabbix[cluster,discovery,nodes]`, поскольку этот элемент данных возвращает JSON данные с информацией о нодах высокой доступности.

[comment]: # ({/e75c5c59-593144b8})

[comment]: # ({6b8b7b04-82cd7e56})
#### Отключение HA кластера

Чтобы отключить кластер высокой доступности:

-   сделайте архивные копии файлов конфигурации
-   остановите резервные ноды
-   удалите HANodeName параметр на основном активном сервере
-   перезапустите основной сервер (он запустится в автономном режиме)

[comment]: # ({/6b8b7b04-82cd7e56})

[comment]: # ({new-7d944a36})
### Upgrading HA cluster

To perform a major version upgrade for the HA nodes:

-   stop all nodes;
-   create a full database backup;
-   if the database uses replication make sure that all nodes are in sync and have no issues. Do not upgrade if replication is broken.
-   select a single node that will perform database upgrade, change its configuration to standalone mode by commenting out HANodeName and [upgrade](/manual/installation/upgrade) it;
-   make sure that database upgrade is fully completed (*System information* should display that Zabbix server is running);
-   restart the node in HA mode;
-   upgrade and start the rest of nodes (it is not required to change them to standalone mode as the database is already upgraded at this point).

In a minor version upgrade it is sufficient to upgrade the first node, make sure it has upgraded and running, and then start upgrade on the next node.

[comment]: # ({/new-7d944a36})

[comment]: # ({new-f4d3143a})

### Implementation details

The high availability (HA) cluster is an opt-in solution and it is
supported for Zabbix server. The native HA solution is designed to be
simple in use, it will work across sites and does not have specific
requirements for the databases that Zabbix recognizes. Users are free to
use the native Zabbix HA solution, or a third party HA solution,
depending on what best suits the high availability requirements in their
environment.

The solution consists of multiple zabbix\_server instances or nodes.
Every node:

-   is configured separately
-   uses the same database
-   may have several modes: active, standby, unavailable, stopped

Only one node can be active (working) at a time. A standby node runs only one 
process - the HA manager. A standby node does no data collection, 
processing or other regular server activities; they do not listen 
on ports; they have minimum database connections.

Both active and standby nodes update their last access time every 5
seconds. Each standby node monitors the last access time of the active
node. If the last access time of the active node is over 'failover
delay' seconds, the standby node switches itself to be the active node
and assigns 'unavailable' status to the previously active node.

The active node monitors its own database connectivity - if it is lost
for more than `failover delay-5` seconds, it must stop all processing
and switch to standby mode. The active node also monitors the status of
the standby nodes - if the last access time of a standby node is over
'failover delay' seconds, the standby node is assigned the 'unavailable'
status.

The failover delay is configurable, with the minimum being 10 seconds.

The nodes are designed to be compatible across minor Zabbix versions.

[comment]: # ({/new-f4d3143a})
