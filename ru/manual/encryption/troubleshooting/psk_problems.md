[comment]: # translation:outdated

[comment]: # ({new-58c98254})
# 3 Проблемы с PSK

[comment]: # ({/new-58c98254})

[comment]: # ({new-aebe0aa7})
#### PSK содержит нечетное количество hex-цифр

Прокси или агент не запускаются, сообщение в журнале прокси или агента:

    invalid PSK in file "/home/zabbix/zabbix_proxy.psk"

[comment]: # ({/new-aebe0aa7})

[comment]: # ({new-887ee505})
#### В GnuTLS передана строка идентификатор PSK длиннее 128 байт

В журнале на стороне TLS клиента:

    gnutls_handshake() failed: -110 The TLS connection was non-properly terminated.

В журнале на стороне TLS сервера.

    gnutls_handshake() failed: -90 The SRP username supplied is illegal.

[comment]: # ({/new-887ee505})

[comment]: # ({new-a2e71eaa})
#### В mbed TLS (PolarSSL) передан PSK длиннее 32 байт

В любом журнале Zabbix:

    ssl_set_psk(): SSL - Bad input parameters to function

#### Используется одинаковая строка идентификации PSK, но разные значения PSK для связи между компонентами (например с OpenSSL)

В журнале на стороне инициатора подключения:

    ...[connect] TCP successful, cannot establish TLS to [[xx.xx.xx.xx]:xxx]: SSL_connect() returned SSL_ERROR_SSL: file s3_pkt.c line 1472: error:140943FC:SSL routines:ssl3_read_bytes:sslv3 alert bad record mac: SSL alert number 20: TLS read fatal alert "bad record mac"

В журнале на принимающей стороне:

    ...failed to accept an incoming connection: from xx.xx.xx.xx: TLS handshake returned error code 1: file s3_pkt.c line 532: error:1408F119:SSL routines:SSL3_GET_RECORD:decryption failed or bad record mac: TLS write fatal alert "bad record mac"

[comment]: # ({/new-a2e71eaa})
