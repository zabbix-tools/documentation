[comment]: # translation:outdated

[comment]: # ({4cdf4b25-0a5cc230})
# 9 Изменения в шаблонах

На этой странице перечислены все изменения шаблонов, которые поставляются с Zabbix. Предлагается изменить эти шаблоны на существующих инсталляциях - в зависимости от изменений, это можно сделать либо импортированием последней версии, либо выполнением изменений вручную.

Обратите внимание, обновление Zabbix на последнюю версию не приведет к автоматическому обновлению используемых шаблонов. В существующих инсталляциях предлагается изменить шаблоны по следующему сценарию:

-   Загрузите последние шаблоны с [Zabbix Git репозитория](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates);
-   Затем, находясь в *Настройка* → *Шаблоны* вы сможете импортировать эти шаблоны в Zabbix. Если шаблоны с таким же именем уже существуют, при импорте необходимо отметить *Удалить отсутствующее* опции, чтобы добиться чистого импорта. Таким образом, старые элементы, которые более не существуют в обновленных шаблонах, будут удалены (обратите внимание, также это означает потерю истории по этим старым элементам данных).

[comment]: # ({/4cdf4b25-0a5cc230})

[comment]: # ({new-94950308})
### CHANGES IN 7.0.0

[comment]: # ({/new-94950308})

[comment]: # ({new-651539db})
#### Updated templates

**Zabbix server/proxy health**

Templates for Zabbix server/proxy health have been updated according to the [changes](/manual/introduction/whatsnew700#concurrency-in-network-discovery) 
in network discovery. The items for monitoring the discoverer process (now removed) has been replaced by items to measure the 
discovery manager and discovery worker process utilization. A new internal item has been added for monitoring the discovery queue. 

[comment]: # ({/new-651539db})







