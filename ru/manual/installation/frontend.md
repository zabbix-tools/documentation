[comment]: # translation:outdated

[comment]: # ({090e0a54-9cceae9f})
# 6 Установка веб-интерфейса

Данные раздел приводит пошаговую инструкцию по установке веб-интерфейса Zabbix. Веб-интерфейс Zabbix написан на PHP, в связи с чем для его запуска необходим веб-сервер с поддержкой PHP.

[comment]: # ({/090e0a54-9cceae9f})

[comment]: # ({new-0c649c0e})
:::notetip
You can find out more about setting up SSL for Zabbix frontend by referring to these [best practices](/manual/installation/requirements/best_practices).
:::

[comment]: # ({/new-0c649c0e})

[comment]: # ({93e7c2bf-6b173526})
#### Экран приветствия

Откройте в браузере URL веб-интерфейса Zabbix. Если вы установили Zabbix из пакетов, то URL:

-   для Apache: *http://<ip\_или\_имя\_сервера>/zabbix*
-   для Nginx: *http://<ip\_или\_имя\_сервера>*

Вы должны увидеть первый экран мастера установки веб-интерфейса.

Используйте выпадающее меню *Default language*, чтобы изменить язык системы по умолчанию, и продолжите установку на выбранном языке (опционально). Для получения более подробной информации, обратитесь к [Установка дополнительных языков для веб-интерфейса](/manual/appendix/install/locales).

![](../../../assets/en/manual/installation/install_1.png){width="550"}

[comment]: # ({/93e7c2bf-6b173526})

[comment]: # ({f93bddd5-2458c643})
#### Проверка предварительных требований

Убедитесь, что соблюдены все предварительные требования программного обеспечения.

![](../../../assets/en/manual/installation/install_2.png){width="550"}

|Требование|Минимальное значение|Описание|
|-------------|-------------|-----------|
|*Версия PHP*|7.2.5|<|
|*PHP memory\_limit*|128MB|В php.ini:<br>memory\_limit = 128M|
|*PHP опция post\_max\_size*|16MB|В php.ini:<br>post\_max\_size = 16M|
|*PHP опция upload\_max\_filesize*|2MB|В php.ini:<br>upload\_max\_filesize = 2M|
|*PHP опция max\_execution\_time*|300 секунд (значения 0 и -1 разрешены)|В php.ini:<br>max\_execution\_time = 300|
|*PHP опция max\_input\_time*|300 seconds (значения 0 и -1 разрешены)|В php.ini:<br>max\_input\_time = 300|
|*PHP опция session.auto\_start*|должна быть отключена|В php.ini:<br>session.auto\_start = 0|
|*Поддержка базы данных*|Одна из: MySQL, Oracle, PostgreSQL.|Один из следующих модулей должен быть установлен:<br>mysql, oci8, pgsql|
|*bcmath*|<|php-bcmath|
|*mbstring*|<|php-mbstring|
|*PHP опция mbstring.func\_overload*|должна быть отключена|В php.ini:<br>mbstring.func\_overload = 0|
|*sockets*|<|php-net-socket. Требуется для поддержки пользовательских скриптов.|
|*gd*|2.0.28|php-gd. Расширение PHP GD должно поддерживать PNG изображения (*--with-png-dir*), JPEG (*--with-jpeg-dir*) изображения и FreeType 2 (*--with-freetype-dir*).|
|*libxml*|2.6.15|php-xml|
|*xmlwriter*|<|php-xmlwriter|
|*xmlreader*|<|php-xmlreader|
|*ctype*|<|php-ctype|
|*session*|<|php-session|
|*gettext*|<|php-gettext<br>Начиная с версии Zabbix 2.2.1, расширение PHP gettext более не является обязательным для установки Zabbix. Если gettext не установлен, веб-интерфейс продолжит работать как обычно, однако, переводы не будут доступны.|

В списке могут также присутствовать необязательные требования. Если необязательное требование не удовлетворено, то оно отображается оранжевым цветом и имеет состояние *Предупреждение* (Warning). Установка можно продолжать, если необязательные требования не удовлетворены.

::: noteimportant
Если имеется необходимость изменить пользователя или группу пользователей Apache, необходимо проверить права доступа к папке сессий. В противном случае установку Zabbix не удастся продолжить.
:::

[comment]: # ({/f93bddd5-2458c643})

[comment]: # ({fe3e3b06-879dc06f})
#### Настройка соединения с базой данных

Укажите информацию для подключения к базе данных. База данных Zabbix должна быть уже создана.

![](../../../assets/en/manual/installation/install_3a.png){width="550"}

Если выбрана опция *TLS шифрование базы данных*, в диалоге появятся дополнительные поля для [настройки TLS соединения](/manual/appendix/install/db_encrypt) к базе данных (только для MySQL или PostgreSQL).

Если для хранения учетных данных была выбрана опция HashiCorp Vault, станут доступны дополнительные поля в которых необходимо будет указать API endpoint Хранилища, пути к секрету и токена аутентификации:

![](../../../assets/en/manual/installation/install_3b.png){width="550"}

[comment]: # ({/fe3e3b06-879dc06f})

[comment]: # ({55c4faac-98277238})
#### Настройки

Ввод имени Zabbix сервера опционален, однако, если задан, оно будет отображаться в разделе меню и в заголовках страниц.

Задайте [часовой пояс](/manual/web_interface/time_zone#обзор) и тему по умолчанию для веб-интерфейса.

![](../../../assets/en/manual/installation/install_4.png){width="550"}

[comment]: # ({/55c4faac-98277238})

[comment]: # ({c1ec5738-42398398})
#### Сводная информация перед установкой

Просмотрите результат настроек.

![](../../../assets/en/manual/installation/install_5.png){width="550"}

[comment]: # ({/c1ec5738-42398398})

[comment]: # ({a1353b4e-1124dd9e})
#### Установка

При установке Zabbix из исходного кода, загрузите файл конфигурации и поместите его в поддиректорию HTML документов conf/, куда вы скопировали PHP файлы Zabbix.

![](../../../assets/en/manual/installation/install_6.png){width="550"}

![](../../../assets/en/manual/installation/saving_zabbix_conf.png){width="350"}

::: notetip
В случае, если веб-сервер имеет право на запись в каталог conf/, файл конфигурации будет сохранен автоматически и можно будет сразу же перейти к следующему шагу.
:::

Завершение установки.

![](../../../assets/en/manual/installation/install_7.png){width="550"}

[comment]: # ({/a1353b4e-1124dd9e})

[comment]: # ({1134d198-d59dc4b9})
#### Авторизация

Веб-интерфейс Zabbix готов! По умолчанию имя пользователя - **Admin**, пароль - **zabbix**.

![](../../../assets/en/manual/quickstart/login.png){width="350"}

Перейти к [началу работы с Zabbix](/manual/quickstart/login).

[comment]: # ({/1134d198-d59dc4b9})
