[comment]: # translation:outdated

[comment]: # ({f045891b-e62e1497})
# 5 Установка из контейнеров

[comment]: # ({/f045891b-e62e1497})

[comment]: # ({new-a3ac452e})

### Overview

This section describes how to deploy Zabbix with [Docker](#docker) or [Docker Compose](#docker-compose).

Zabbix officially provides:

- Separate Docker images for each Zabbix
component to run as portable and self-sufficient containers.
- Compose files for defining and running
multi-container Zabbix components in Docker.

[comment]: # ({/new-a3ac452e})

[comment]: # ({dc48c084-f9acea87})
### Docker

Zabbix предоставляет образы [Docker](https://www.docker.com) для каждого компонента Zabbix в виде переносимых и самодостаточных контейнеров для ускорения процедур развертывания и обновления.

Компоненты Zabbix поставляются с поддержкой баз данных MySQL и PostgreSQL, веб-серверов Apache2 и Nginx. Эти образы разделены на несколько отдельных образов.

[comment]: # ({/dc48c084-f9acea87})

[comment]: # ({new-e498c2aa})

#### Tags

Official Zabbix component images may contain the following tags:

|Tag|Description|Example|
|--|----------|----|
|latest            | The latest stable version of a Zabbix component based on Alpine Linux image. |zabbix-agent:latest |
|<OS>-trunk | The latest nightly build of the Zabbix version that is currently being developed on a specific operating system. <br> <br> **<OS>** - the base operating system. Supported values: <br> *alpine* - Alpine Linux; <br> *ltsc2019* -  Windows 10 LTSC 2019 (agent only); <br> *ol* - Oracle Linux; <br> *ltsc2022* - Windows 11 LTSC 2022 (agent only); <br> *ubuntu* - Ubuntu  |zabbix agent:ubuntu-trunk |
|<OS>-latest     | The latest stable version of a Zabbix component on a specific operating system. <br> <br> **<OS>** - the base operating system. Supported values:  <br> *alpine* - Alpine Linux; <br> *ltsc2019* -  Windows 10 LTSC 2019 (agent only); <br> *ol* - Oracle Linux; <br> *ltsc2022* - Windows 11 LTSC 2022 (agent only); <br> *ubuntu* - Ubuntu|zabbix-agent:ol-latest |
|<OS>-X.X-latest |The latest minor version of a Zabbix component of a specific major version and operating system. <br> <br> **<OS>** - the base operating system. Supported values:  <br> *alpine* - Alpine Linux; <br> *centos* - CentOS (only for Zabbix 4.0) <br> *ltsc2019* -  Windows 10 LTSC 2019 (agent only); <br> *ol* - Oracle Linux; <br> *ltsc2022* - Windows 11 LTSC 2022 (agent only); <br> *ubuntu* - Ubuntu<br><br>**X.X** - the major Zabbix version (supported: *4.0*, *5.0*, *6.0*, *6.2*). |zabbix-agent:alpine-6.0-latest |
|<OS>-X.X.*      |The latest minor version of a Zabbix component of a specific major version and operating system. <br> <br> **<OS>** - the base operating system. Supported values:  <br> *alpine* - Alpine Linux; <br> *centos* - CentOS (only for Zabbix 4.0) <br> *ltsc2019* -  Windows 10 LTSC 2019 (agent only); <br> *ol* - Oracle Linux; <br> *ltsc2022* - Windows 11 LTSC 2022 (agent only); <br> *ubuntu* - Ubuntu<br><br>**X.X** - the major Zabbix version (supported: *4.0*, *5.0*, *6.0*, *6.2*). <br><br> **\*** - the Zabbix minor version  |zabbix-agent:alpine-6.4.1|

[comment]: # ({/new-e498c2aa})


[comment]: # ({5a31793d-fba82ded})
#### Исходные файлы Docker

Каждый может следить на изменениями в Docker файлах используя [официальный репозиторий](https://github.com/zabbix/zabbix-docker) Zabbix на [github.com](https://github.com/). Вы можете создавать ответвления от проекта или свои собственные образы на основе официальных Docker файлов.

[comment]: # ({/5a31793d-fba82ded})



[comment]: # ({79811dae-0b4b68c7})
#### Использование

[comment]: # ({/79811dae-0b4b68c7})

[comment]: # ({5a58c160-de9f41d4})
##### Переменные окружения

Все компоненты Zabbix предусматривают наличие переменных окружения для управления конфигурацией. Эти переменные окружения перечислены в репозиториях каждого из компонентов. Переменные окружения являются параметрами из файлов конфигурации Zabbix, но с другим методом наименования. Например,`ZBX_LOGSLOWQUERIES` идентичен `LogSlowQueries` из файлов конфигурации Zabbix сервера и Zabbix прокси.

::: noteimportant
 Некоторые параметры конфигурации недоступны для изменения. Например, `PIDFile` и `LogType`.
:::

У некоторых компонентов есть специфические переменные окружения, несуществующие в официальных файлах конфигурации Zabbix:

|   |   |   |
|---|---|---|
|**Переменная**|**Компоненты**|**Описание**|
|`DB_SERVER_HOST`|Сервер<br>Прокси<br>Веб-интерфейс|Переменная является IP адресом или DNS именем MySQL или PostgreSQL сервера.<br>По умолчанию, значение `mysql-server` или `postgres-server` для MySQL или PostgreSQL соответственно|
|`DB_SERVER_PORT`|Сервер<br>Прокси<br>Веб-интерфейс|Переменная является портом MySQL или PostgreSQL сервера.<br>По умолчанию, значение '3306' или '5432' соответственно.|
|`MYSQL_USER`|Сервер<br>Прокси<br>Веб-интерфейс|Имя пользователя базы данных MySQL.<br>По умолчанию, значение 'zabbix'.|
|`MYSQL_PASSWORD`|Сервер<br>Прокси<br>Веб-интерфейс|Пароль пользователя базы данных MySQL.<br>По умолчанию, значение 'zabbix'.|
|`MYSQL_DATABASE`|Сервер<br>Прокси<br>Веб-интерфейс|Имя базы данных Zabbix.<br>По умолчанию, значение 'zabbix' для Zabbix сервера и 'zabbix\_proxy' для Zabbix прокси.|
|`POSTGRES_USER`|Сервер<br>Веб-интерфейс|Имя пользователя базы данных PostgreSQL.<br>По умолчанию, значение 'zabbix'.|
|`POSTGRES_PASSWORD`|Сервер<br>Веб-интерфейс|Пароль пользователя базы данных PostgreSQL.<br>По умолчанию, значение 'zabbix'.|
|`POSTGRES_DB`|Сервер<br>Веб-интерфейс|Имя базы данных Zabbix.<br>По умолчанию, значение 'zabbix' для Zabbix сервера и 'zabbix\_proxy' для Zabbix прокси.|
|`PHP_TZ`|Веб-интерфейс|Часовой пояс в PHP формате. Полный список поддерживаемых часовых поясов доступен на [php.net](http://php.net/manual/en/timezones.php).<br>По умолчанию,значение 'Europe/Riga'.|
|`ZBX_SERVER_NAME`|Веб-интерфейс|Видимое имя Zabbix инсталляции в правом верхнем углу веб-интерфейса.<br>По умолчанию, значение 'Zabbix Docker'|
|`ZBX_JAVAGATEWAY_ENABLE`|Сервер<br>Прокси|Включает обмен данными с Zabbix Java gateway для сбора проверок относящихся к Java.<br>По умолчанию, значение "false"|
|`ZBX_ENABLE_SNMP_TRAPS`|Сервер<br>Прокси|Включение функции SNMP трапов. Переменной требуется экземпляр **zabbix-snmptraps** и разделяемый с Zabbix сервером или Zabbix прокси том */var/lib/zabbix/snmptraps*.|

[comment]: # ({/5a58c160-de9f41d4})

[comment]: # ({ec6d9476-7d10f7dc})
##### Тома

Образы позволяют использовать некоторые точки монтирования. Данные точки монтирования имеют отличия и зависят от типа Zabbix компонента:

|   |   |
|---|---|
|**Том**|**Описание**|
|**Zabbix агент**|<|
|*/etc/zabbix/zabbix\_agentd.d*|Том позволяет включать *\*.conf* файлы и расширять Zabbix агент используя функционал `UserParameter`|
|*/var/lib/zabbix/modules*|Том позволяет подгружать дополнительные модули и расширять Zabbix агент используя функционал [LoadModule](/manual/config/items/loadablemodules)|
|*/var/lib/zabbix/enc*|Том используется для хранения файлов связанных с TLS. Имена данных файлов указываются при помощи переменных окружения `ZBX_TLSCAFILE`, `ZBX_TLSCRLFILE`, `ZBX_TLSKEY_FILE` и `ZBX_TLSPSKFILE`|
|**Zabbix сервер**|<|
|*/usr/lib/zabbix/alertscripts*|Том используется для пользовательских скриптов оповещения. Яляется параметром `AlertScriptsPath` в [zabbix\_server.conf](/manual/appendix/config/zabbix_server)|
|*/usr/lib/zabbix/externalscripts*|Том используется для [внешних проверок](/manual/config/items/itemtypes/external). Является параметром `ExternalScripts` в [zabbix\_server.conf](/manual/appendix/config/zabbix_server)|
|*/var/lib/zabbix/modules*|Том позволяет подгружать дополнительные модули и расширять Zabbix сервер используя функционал [LoadModule](/manual/config/items/loadablemodules)|
|*/var/lib/zabbix/enc*|Том используется для хранения файлов связанных с TLS. Имена данных файлов указываются при помощи переменных окружения `ZBX_TLSCAFILE`, `ZBX_TLSCRLFILE`, `ZBX_TLSKEY_FILE` и `ZBX_TLSPSKFILE`|
|*/var/lib/zabbix/ssl/certs*|Том используется для размещения файлов клиентских SSL сертификатов для аутентификации клиентов. Является параметром `SSLCertLocation` в zabbix\_server.conf|
|*/var/lib/zabbix/ssl/keys*|Том используется для размещения файлов приватных SSL ключей для аутентификации клиентов. Является параметром `SSLKeyLocation` в [zabbix\_server.conf](/manual/appendix/config/zabbix_server)|
|*/var/lib/zabbix/ssl/ssl\_ca*|Том используется для размещения файлов центра сертификации (CA) для верификации SSL сертификатов сервера. Яляется параметром `SSLCALocation` в [zabbix\_server.conf](/manual/appendix/config/zabbix_server)|
|*/var/lib/zabbix/snmptraps*|Том используется для размещения snmptraps.log файла. Этот том может быть использован совместно с контейнером zabbix-snmptraps и унаследован используя Docker опцию volumes\_from при создании нового экземпляра Zabbix сервера. Функцию обработки SNMP трапов можно включить используя совместный том и переключив переменную окружения ZBX_ENABLE_SNMP_TRAPS в 'true'|
|*/var/lib/zabbix/mibs*|Том позволяет добавлять новые MIB файлы. Не поддерживает подпапки, все MIB файлы должны быть помещены в `/var/lib/zabbix/mibs`|
|**Zabbix прокси**|<|
|*/usr/lib/zabbix/externalscripts*|Том используется для [внешних проверок](/manual/config/items/itemtypes/external). Является параметром `ExternalScripts` в [zabbix\_proxy.conf](/manual/appendix/config/zabbix_proxy)|
|*/var/lib/zabbix/modules*|Том позволяет подгружать дополнительные модули и расширять Zabbix прокси используя функционал [LoadModule](/manual/config/items/loadablemodules)|
|*/var/lib/zabbix/enc*|Том используется для хранения файлов связанных с TLS. Имена данных файлов указываются при помощи переменных окружения `ZBX_TLSCAFILE`, `ZBX_TLSCRLFILE`, `ZBX_TLSKEY_FILE` и `ZBX_TLSPSKFILE`|
|*/var/lib/zabbix/ssl/certs*|Том используется для размещения файлов клиентских SSL сертификатов для аутентификации клиентов. Является параметром `SSLCertLocation` в [zabbix\_proxy.conf](/manual/appendix/config/zabbix_proxy)|
|*/var/lib/zabbix/ssl/keys*|Том используется для размещения файлов приватных SSL ключей для аутентификации клиентов. Является параметром `SSLKeyLocation` в [zabbix\_proxy.conf](/manual/appendix/config/zabbix_proxy)|
|*/var/lib/zabbix/ssl/ssl\_ca*|Том используется для размещения файлов центра сертификации (CA) для верификации SSL сертификатов сервера. Является параметром `SSLCALocation` в [zabbix\_proxy.conf](/manual/appendix/config/zabbix_proxy)|
|*/var/lib/zabbix/snmptraps*|Том используется для размещения snmptraps.log файла. Этот том может быть использован совместно с контейнером zabbix-snmptraps и унаследован используя Docker опцию volumes\_from при создании нового экземпляра Zabbix прокси. Функцию обработки SNMP трапов можно включить используя совместный том и переключив переменную окружения ZBX_ENABLE_SNMP_TRAPS в 'true'|
|*/var/lib/zabbix/mibs*|Том позволяет добавлять новые MIB файлы. Не поддерживает подпапки, все MIB файлы должны быть помещены в `/var/lib/zabbix/mibs`|
|**веб-интерфейс Zabbix на основе веб-сервера Apache2**|<|
|*/etc/ssl/apache2*|Том позволяет активировать HTTPS для Zabbix веб-интерфейса. Том должен содержать два файла `ssl.crt` и `ssl.key`, подготовленные для SSL соединений Apache2|
|**веб-интерфейс Zabbix на основе веб-сервера Nginx**|<|
|*/etc/ssl/nginx*|Том позволяет активировать HTTPS для Zabbix веб-интерфейса. Том должен содержать три файла `ssl.crt`, `ssl.key` и `dhparam.pem` подготовленные для SSL соединений Nginx|
|**Zabbix snmptraps**|<|
|*/var/lib/zabbix/snmptraps*|Том содержит имя файл журнала `snmptraps.log` с полученными SNMP трапами|
|*/var/lib/zabbix/mibs*|Том позволяет добавлять новые MIB файлы. Не поддерживает подпапки, все MIB файлы должны быть помещены в `/var/lib/zabbix/mibs`|

Для получения дополнительной информации обратитесь к официальными репозиториям Zabbix в Docker Hub.

[comment]: # ({/ec6d9476-7d10f7dc})

[comment]: # ({db8caf8a-492bd3ba})
##### Примеры использования

**Пример 1**

В этом примере продемонстрировано как запустить Zabbix сервер с поддержкой базы данных MySQL, веб-интерфейсом Zabbix на основе веб-сервера Nginx и Zabbix Java gateway.

1\. Создайте выделенную сеть для контейнеров с компонентами Zabbix:

    # docker network create --subnet 172.20.0.0/16 --ip-range 172.20.240.0/20 zabbix-net

2\. Запустите пустой экземпляр MySQL сервера

    # docker run --name mysql-server -t \
          -e MYSQL_DATABASE="zabbix" \
          -e MYSQL_USER="zabbix" \
          -e MYSQL_PASSWORD="zabbix_pwd" \
          -e MYSQL_ROOT_PASSWORD="root_pwd" \
          --network=zabbix-net \
          -d mysql:8.0 \
          --restart unless-stopped \
          --character-set-server=utf8 --collation-server=utf8_bin \
          --default-authentication-plugin=mysql_native_password

3\. Запустите экземпляр Zabbix Java gateway

    # docker run --name zabbix-java-gateway -t \
          --network=zabbix-net \
          --restart unless-stopped \
          -d zabbix/zabbix-java-gateway:alpine-5.4-latest

4\. Запустите экземпляр Zabbix сервера и слинкуйте этот экземпляр с уже созданным экземпляром MySQL сервера

    # docker run --name zabbix-server-mysql -t \
          -e DB_SERVER_HOST="mysql-server" \
          -e MYSQL_DATABASE="zabbix" \
          -e MYSQL_USER="zabbix" \
          -e MYSQL_PASSWORD="zabbix_pwd" \
          -e MYSQL_ROOT_PASSWORD="root_pwd" \
          -e ZBX_JAVAGATEWAY="zabbix-java-gateway" \
          --network=zabbix-net \
          -p 10051:10051 \
          --restart unless-stopped \
          -d zabbix/zabbix-server-mysql:alpine-5.4-latest

::: noteclassic
Экземпляр Zabbix сервера открывает порт 10051/TCP (Zabbix траппер) на узле.
:::

5\. Запустите экземпляр веб-интерфейса Zabbix и слинкуйте этот экземпляр с уже созданным экземплярами MySQL сервера
и Zabbix сервера

    # docker run --name zabbix-web-nginx-mysql -t \
          -e ZBX_SERVER_HOST="zabbix-server-mysql" \
          -e DB_SERVER_HOST="mysql-server" \
          -e MYSQL_DATABASE="zabbix" \
          -e MYSQL_USER="zabbix" \
          -e MYSQL_PASSWORD="zabbix_pwd" \
          -e MYSQL_ROOT_PASSWORD="root_pwd" \
          --network=zabbix-net \
          -p 80:8080 \
          --restart unless-stopped \
          -d zabbix/zabbix-web-nginx-mysql:alpine-5.4-latest

::: noteclassic
Экземпляр веб-интерфейса Zabbix открывает 80/TCP порт (HTTP) на узле.
:::

**Пример 2**

В этом примере продемонстрировано как запустить Zabbix сервер с поддержкой базы данных PostgreSQL,  веб-интерфейсом Zabbix на основе веб-сервера Nginx и с функцией приёма SNMP трапов.

1\. Создайте выделенную сеть для контейнеров с компонентами Zabbix:

    # docker network create --subnet 172.20.0.0/16 --ip-range 172.20.240.0/20 zabbix-net

2\. Запустите пустой экземпляр PostgreSQL сервера

    # docker run --name postgres-server -t \
          -e POSTGRES_USER="zabbix" \
          -e POSTGRES_PASSWORD="zabbix_pwd" \
          -e POSTGRES_DB="zabbix" \
          --network=zabbix-net \
          --restart unless-stopped \
          -d postgres:latest

3\. Запустите экземпляр Zabbix snmptraps

    # docker run --name zabbix-snmptraps -t \
          -v /zbx_instance/snmptraps:/var/lib/zabbix/snmptraps:rw \
          -v /var/lib/zabbix/mibs:/usr/share/snmp/mibs:ro \
          --network=zabbix-net \
          -p 162:1162/udp \
          --restart unless-stopped \
          -d zabbix/zabbix-snmptraps:alpine-5.4-latest

::: noteclassic
Экземпляр Zabbix snmptrap открывает порт 162/UDP (SNMP трапы) на узле.
:::

4\. Запустите экземпляр Zabbix сервера и слинкуйте этот экземпляр с уже созданным экземпляром PostgreSQL сервера

    # docker run --name zabbix-server-pgsql -t \
          -e DB_SERVER_HOST="postgres-server" \
          -e POSTGRES_USER="zabbix" \
          -e POSTGRES_PASSWORD="zabbix_pwd" \
          -e POSTGRES_DB="zabbix" \
          -e ZBX_ENABLE_SNMP_TRAPS="true" \
          --network=zabbix-net \
          -p 10051:10051 \
          --volumes-from zabbix-snmptraps \
          --restart unless-stopped \
          -d zabbix/zabbix-server-pgsql:alpine-5.4-latest

::: noteclassic
Экземпляр Zabbix сервера открывает порт 10051/TCP (Zabbix траппер) к узлу.
:::

5\. Запустите экземпляр веб-интерфейса Zabbix и слинкуйте этот экземпляр с уже созданным экземплярами PostgreSQL сервера и Zabbix сервера

    # docker run --name zabbix-web-nginx-pgsql -t \
          -e ZBX_SERVER_HOST="zabbix-server-pgsql" \
          -e DB_SERVER_HOST="postgres-server" \
          -e POSTGRES_USER="zabbix" \
          -e POSTGRES_PASSWORD="zabbix_pwd" \
          -e POSTGRES_DB="zabbix" \
          --network=zabbix-net \
          -p 443:8443 \
          -p 80:8080 \
          -v /etc/ssl/nginx:/etc/ssl/nginx:ro \
          --restart unless-stopped \
          -d zabbix/zabbix-web-nginx-pgsql:alpine-5.4-latest

::: noteclassic
Экземпляр веб-интерфейса Zabbix открывает порт 443/TCP (HTTPS) на узле.\
Папка /etc/ssl/nginx должна содержать сертификат с требуемым именем.
:::

**Пример 3**

В этом примере продемонстрировано как запустить Zabbix сервер с поддержкой базы данных MySQL, веб-интерфейсом Zabbix на основе веб-сервера Nginx и Zabbix Java gateway на Red Hat 8 используя `podman`.

1\. Создайте новую капсулу(pod) с именем `zabbix` и откройте порты (веб-интерфейс, траппер Zabbix сервера):

    podman pod create --name zabbix -p 80:8080 -p 10051:10051

2\. (опционально) Запустите контейнер Zabbix агента в капсуле(pod) `zabbix`:

    podman run --name zabbix-agent \
        -eZBX_SERVER_HOST="127.0.0.1,localhost" \
        --restart=always \
        --pod=zabbix \
        -d registry.connect.redhat.com/zabbix/zabbix-agent-50:latest

3\. Создайте директорию `./mysql/` на узле и запустите Oracle MySQL сервер версии 8.0:

    podman run --name mysql-server -t \
          -e MYSQL_DATABASE="zabbix" \
          -e MYSQL_USER="zabbix" \
          -e MYSQL_PASSWORD="zabbix_pwd" \
          -e MYSQL_ROOT_PASSWORD="root_pwd" \
          -v ./mysql/:/var/lib/mysql/:Z \
          --restart=always \
          --pod=zabbix \
          -d mysql:8.0 \
          --character-set-server=utf8 --collation-server=utf8_bin \
          --default-authentication-plugin=mysql_native_password

3\. Запустите контейнер Zabbix сервера:

    podman run --name zabbix-server-mysql -t \
                      -e DB_SERVER_HOST="127.0.0.1" \
                      -e MYSQL_DATABASE="zabbix" \
                      -e MYSQL_USER="zabbix" \
                      -e MYSQL_PASSWORD="zabbix_pwd" \
                      -e MYSQL_ROOT_PASSWORD="root_pwd" \
                      -e ZBX_JAVAGATEWAY="127.0.0.1" \
                      --restart=always \
                      --pod=zabbix \
                      -d registry.connect.redhat.com/zabbix/zabbix-server-mysql-50

4\. Запустите контейнер Zabbix Java Gateway:

    podman run --name zabbix-java-gateway -t \
          --restart=always \
          --pod=zabbix \
          -d registry.connect.redhat.com/zabbix/zabbix-java-gateway-50

5\. Запустите контейнер веб-интерфейса Zabbix:

    podman run --name zabbix-web-mysql -t \
                      -e ZBX_SERVER_HOST="127.0.0.1" \
                      -e DB_SERVER_HOST="127.0.0.1" \
                      -e MYSQL_DATABASE="zabbix" \
                      -e MYSQL_USER="zabbix" \
                      -e MYSQL_PASSWORD="zabbix_pwd" \
                      -e MYSQL_ROOT_PASSWORD="root_pwd" \
                      --restart=always \
                      --pod=zabbix \
                      -d registry.connect.redhat.com/zabbix/zabbix-web-mysql-50

::: noteclassic
Капсула (pod) `zabbix` открывает порт 80/TCP (HTTP) на узле через порт 8080/TCP контейнера `zabbix-web-mysql`.
:::

[comment]: # ({/db8caf8a-492bd3ba})

[comment]: # ({c834b790-c443c22e})
### Docker Compose

Zabbix также поставляет файлы конфигурации  для определения и запуска нескольких контейнеров с Zabbix компонентами в Docker. Данные файлы конфигурации доступны в официальном Zabbix docker репозитории на github.com: <https://github.com/zabbix/zabbix-docker>. Эти файлы конфигурации добавлены в качестве примера и перегружены. Например, в них содержится прокси с поддержкой MySQL и SQLite3.

Имеется несколько различных версий файлов конфигурации:

|   |   |
|---|---|
|**Имя файла**|**Описание**|
|`docker-compose_v3_alpine_mysql_latest.yaml`|Файл конфигурации запускает последнюю версию компонентов Zabbix 5.4 на Alpine Linux с поддержкой базы данных MySQL.|
|`docker-compose_v3_alpine_mysql_local.yaml`|Файл конфигурации собирает локально последнюю версию компонентов Zabbix 5.4 и запускает компоненты Zabbix на Alpine Linux с поддержкой базы данных MySQL.|
|`docker-compose_v3_alpine_pgsql_latest.yaml`|Файл конфигурации запускает последнюю версию компонентов Zabbix 5.4 на Alpine Linux с поддержкой базы данных PostgreSQL.|
|`docker-compose_v3_alpine_pgsql_local.yaml`|Файл конфигурации собирает локально последнюю версию компонентов Zabbix 5.4 и запускает компоненты Zabbix на Alpine Linux с поддержкой базы данных PostgreSQL.|
|`docker-compose_v3_centos_mysql_latest.yaml`|Файл конфигурации запускает последнюю версию компонентов Zabbix 5.4 на CentOS 8 с поддержкой базы данных MySQL.|
|`docker-compose_v3_centos_mysql_local.yaml`|Файл конфигурации собирает локально последнюю версию компонентов Zabbix 5.4 и запускает компоненты Zabbix на CentOS 8 с поддержкой базы данных MySQL.|
|`docker-compose_v3_centos_pgsql_latest.yaml`|Файл конфигурации запускает последнюю версию компонентов Zabbix 5.4 на CentOS 8 с поддержкой базы данных PostgreSQL.|
|`docker-compose_v3_centos_pgsql_local.yaml`|Файл конфигурации собирает локально последнюю версию компонентов Zabbix 5.4 и запускает компоненты Zabbix на CentOS 8 с поддержкой базы данных PostgreSQL.|
|`docker-compose_v3_ubuntu_mysql_latest.yaml`|Файл конфигурации запускает последнюю версию компонентов Zabbix 5.4 на Ubuntu 20.04 с поддержкой базы данных MySQL.|
|`docker-compose_v3_ubuntu_mysql_local.yaml`|Файл конфигурации собирает локально последнюю версию компонентов Zabbix 5.4 и запускает компоненты Zabbix на Ubuntu 20.04 с поддержкой базы данных MySQL.|
|`docker-compose_v3_ubuntu_pgsql_latest.yaml`|Файл конфигурации запускает последнюю версию компонентов Zabbix 5.4 на Ubuntu 20.04 с поддержкой базы данных PostgreSQL.|
|`docker-compose_v3_ubuntu_pgsql_local.yaml`|Файл конфигурации собирает локально последнюю версию компонентов Zabbix 5.4 и запускает компоненты Zabbix на Ubuntu 20.04 с поддержкой базы данных PostgreSQL.|

::: noteimportant
Доступные Docker файлы конфигурации поддерживают версию 3 Docker Compose.
:::

[comment]: # ({/c834b790-c443c22e})

[comment]: # ({78192b3b-52e39127})
#### Хранилище

Файлы конфигурации Compose сконфигурированы для поддержки локального хранилища на узле. Docker Compose создаст директорию `zbx_env` в директории с файлом конфигурации compose, когда вы запустите компоненты Zabbix с его помощью. Директория будет содержать структуру, описанную в разделе [Тома](#Тома) выше и директорию для хранения базы данных.

Также в файлах конфигурации compose имеются тома с доступом только на чтение для файлов `/etc/localtime` и `/etc/timezone`.

[comment]: # ({/78192b3b-52e39127})

[comment]: # ({537ab11b-0be3c140})
#### Файлы окружения

В той же директории с файлами конфигурации compose на github.com вы можете найти файлы с переменными окружения по умолчанию для каждого компонента из файлов конфигурации compose. Эти файлы окружения называются приблизительно так `.env_<тип компоненты`.

[comment]: # ({/537ab11b-0be3c140})

[comment]: # ({b9aeb561-ad55959c})
#### Примеры

**Пример 1**

    # git checkout 5.4
    # docker-compose -f ./docker-compose_v3_alpine_mysql_latest.yaml up -d

Эта команда загрузит последние образы Zabbix 6.0 для каждого из компонентов Zabbix и запустит их в detach режиме.

::: noteimportant
Не забудьте загрузить `.env_<тип компонента>` файлы с официального репозитория Zabbix на github.com с файлами конфигурации.
:::

**Пример 2**

    # git checkout 5.4
    # docker-compose -f ./docker-compose_v3_ubuntu_mysql_local.yaml up -d

Эта команда загрузит образ на основе Ubuntu 20.04 (focal), затем локально соберёт компоненты Zabbix 6.0 и запустит их в detach режиме.

[comment]: # ({/b9aeb561-ad55959c})
