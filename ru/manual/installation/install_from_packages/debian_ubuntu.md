[comment]: # translation:outdated

[comment]: # ({d96d3275-d96d3275})
# 2 Debian/Ubuntu/Raspbian

[comment]: # ({/d96d3275-d96d3275})

[comment]: # ({new-9df568e9})
### Обзор

Официальные пакеты Zabbix доступны для:

|   |   |
|---|---|
|Debian 10 (Buster)|[Download](https://www.zabbix.com/ru/download?zabbix=5.4&os_distribution=debian&os_version=10_buster&db=mysql)|
|Debian 9 (Stretch)|[Download](https://www.zabbix.com/ru/download?zabbix=5.4&os_distribution=debian&os_version=9_stretch&db=mysql)|
|Debian 8 (Jessie)|[Download](https://www.zabbix.com/ru/download?zabbix=5.4&os_distribution=debian&os_version=8_jessie&db=mysql)|
|Ubuntu 20.04 (Focal Fossa) LTS|[Download](https://www.zabbix.com/ru/download?zabbix=5.4&os_distribution=ubuntu&os_version=20.04_focal&db=mysql)|
|Ubuntu 18.04 (Bionic Beaver) LTS|[Download](https://www.zabbix.com/ru/download?zabbix=5.4&os_distribution=ubuntu&os_version=18.04_bionic&db=mysql)|
|Ubuntu 16.04 (Xenial Xerus) LTS|[Download](https://www.zabbix.com/ru/download?zabbix=5.4&os_distribution=ubuntu&os_version=16.04_xenial&db=mysql)|
|Ubuntu 14.04 (Trusty Tahr) LTS|[Download](https://www.zabbix.com/ru/download?zabbix=5.4&os_distribution=ubuntu&os_version=14.04_trusty&db=mysql)|
|Raspbian (Buster)|[Download](https://www.zabbix.com/ru/download?zabbix=5.4&os_distribution=raspbian&os_version=10_buster&db=mysql)|
|Raspbian (Stretch)|[Download](https://www.zabbix.com/ru/download?zabbix=5.4&os_distribution=raspbian&os_version=9_stretch&db=mysql)|

Пакеты доступны с поддержкой баз данных MySQL/PostgreSQL и веб-серверами Apache/Nginx.

::: noteimportant
Zabbix 5.4 еще не выпущен. Ссылки на загрузку ведут pre-5.4 пакетам.
:::

[comment]: # ({/new-9df568e9})

[comment]: # ({new-ae631a63})
### Заметки по установке

Обратитесь к [инстракциям по установке](https://www.zabbix.com/ru/download?zabbix=5.0&os_distribution=debian&os_version=10_buster&db=mysql) по каждой платформе на странице загрузки чтобы:

-   установить репозиторий
-   установить сервер / агент / веб-интерфейс
-   создать начальную базу данных, импортировать начальные данные
-   настроить базу данных для работы с Zabbix сервером
-   настроить PHP для работы с Zabbix веб-интерфейсом
-   запустить процессы сервера / агента
-   настроить Zabbix веб-интерфейс

Если вы хотите запускать Zabbix агента с правами root, смотрите [Запуск агента с правами root](/manual/appendix/install/run_agent_as_root).

Процессу веб-сервиса Zabbix, который используется для [генерации регулярных отчетов](/manual/web_interface/frontend_sections/reports/scheduled), требуется браузер Google Chrome. Этот браузер не включен в пакеты и его нужно установить отдельно вручную.

[comment]: # ({/new-ae631a63})

[comment]: # ({new-99a402fa})
#### Импорт данных при использовании Timescale DB

При использовании TimescaleDB в дополнение к команде импорта для базы данных PostgreSQL, также выполните:

    # zcat /usr/share/doc/zabbix-sql-scripts/postgresql/timescaledb.sql.gz | sudo -u zabbix psql zabbix

::: notewarning
TimescaleDB поддерживается только с Zabbix сервером.
:::

[comment]: # ({/new-99a402fa})


[comment]: # ({10009dfe-fa73411a})
#### Настройка SELinux

Смотрите [настройку SELinux](/manual/installation/install_from_packages/rhel_centos#настройка_selinux) для RHEL/CentOS.

После завершения настройки веб-интерфейса и SELinux, перезапустите веб-сервер Apache:

    # service apache2 restart

[comment]: # ({/10009dfe-fa73411a})

[comment]: # ({f4152265-a12da003})
### Установка прокси

Как только требуемый репозиторий добавлен, вы можете установить Zabbix прокси выполнив следующую команду:

    # apt install zabbix-proxy-mysql

Замените 'mysql' в команде на 'pgsql', чтобы использовать PostgreSQL, или на 'sqlite3' чтобы использовать SQLite3 (только прокси).

[comment]: # ({/f4152265-a12da003})

[comment]: # ({58877aa8-fe6abb8e})
##### Создание базы данных

[Создайте](/manual/appendix/install/db_scripts) отдельную базу данных для Zabbix прокси.

Zabbix сервер и Zabbix прокси не могут использовать одну и ту же базу данных. Если они установлены на одном хосте, тогда имя базы данных прокси должно отличаться.

[comment]: # ({/58877aa8-fe6abb8e})

[comment]: # ({new-2ab835d7})
##### Импорт данных

Импортируйте начальную схему:

    # zcat /usr/share/doc/zabbix-sql-scripts/mysql/schema.sql.gz | mysql -uzabbix -p zabbix

Для прокси с PostgreSQL (или SQLite):

    # zcat /usr/share/doc/zabbix-sql-scripts/postgresql/schema.sql.gz | sudo -u zabbix psql zabbix
    # zcat /usr/share/doc/zabbix-sql-scripts/sqlite3/schema.sql.gz | sqlite3 zabbix.db

[comment]: # ({/new-2ab835d7})

[comment]: # ({4de80163-d0a225c7})
##### Настройка базы данных для Zabbix прокси

Отредактируйте zabbix\_proxy.conf:

    # vi /etc/zabbix/zabbix_proxy.conf
    DBHost=localhost
    DBName=zabbix
    DBUser=zabbix
    DBPassword=<пароль>

В DBName для Zabbix прокси используйте базу данных, отличную от базы данных Zabbix сервера.

В DBPassword используйте пароль к базе данных Zabbix для MySQL; пароль к PosgreSQL пользователю для PosgreSQL.

Используйте `DBHost=` с PostgreSQL. Вы возможно захотите оставить настройку по умолчанию `DBHost=localhost` (или IP адрес), но в этом случае PostgreSQL будет использовать сетевой сокет для подключения к Zabbix. Обратитесь к  [соответствующему разделу](/manual/installation/install_from_packages/rhel_centos#настройка_selinux) для получения инструкций от RHEL/CentOS.

[comment]: # ({/4de80163-d0a225c7})

[comment]: # ({44a7219c-27de2ced})
##### Запуск процесса Zabbix прокси

Чтобы запустить процесс Zabbix прокси и добавить его в автозагрузку при загрузке системы, выполните следующие команды:

    # systemctl restart zabbix-proxy
    # systemctl enable zabbix-proxy

[comment]: # ({/44a7219c-27de2ced})

[comment]: # ({c305577f-871a973b})
##### Настройка веб-интерфейса

Zabbix прокси не имеет веб-интерфейса; прокси обменивается информацией только с Zabbix сервером.

[comment]: # ({/c305577f-871a973b})


[comment]: # ({c23dd7ad-cd9340bd})
### Установка Java gateway

[Java gateway](/manual/concepts/java) необходимо устанавливать только, если вы хотите мониторить JMX приложения. Java gateway легковесный и не требует наличия базы данных.

Как только требуемый репозиторий добавлен, вы можете установить Zabbix Java gateway выполнив следующую команду:

    # apt install zabbix-java-gateway

Перейдите к [разделу настройки](/manual/concepts/java/from_debian_ubuntu) для получения более подробных сведений касательно настройки и запуску Java gateway.

[comment]: # ({/c23dd7ad-cd9340bd})
