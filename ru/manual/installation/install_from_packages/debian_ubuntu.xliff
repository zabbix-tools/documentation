<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="ru" datatype="plaintext" original="manual/installation/install_from_packages/debian_ubuntu.md">
    <body>
      <trans-unit id="d96d3275" xml:space="preserve">
        <source># 2 Debian/Ubuntu/Raspbian</source>
        <target># 2 Debian/Ubuntu/Raspbian</target>
      </trans-unit>
      <trans-unit id="9df568e9" xml:space="preserve">
        <source>### Overview

Official Zabbix 6.5 PRE-RELEASE packages for Debian, Ubuntu, and Raspberry Pi OS (Raspbian) are available on
[Zabbix website](https://www.zabbix.com/download). Packages for Zabbix 7.0 will be available upon its release.

Packages are available with either MySQL/PostgreSQL database and Apache/Nginx web server support.</source>
      </trans-unit>
      <trans-unit id="ae631a63" xml:space="preserve">
        <source>### Notes on installation

See the [installation
instructions](https://www.zabbix.com/download?zabbix=7.0&amp;os_distribution=ubuntu&amp;os_version=22.04&amp;components=server_frontend_agent&amp;db=mysql&amp;ws=apache)
per platform in the download page for:

-   installing the repository
-   installing server/agent/frontend
-   creating initial database, importing initial data
-   configuring database for Zabbix server
-   configuring PHP for Zabbix frontend
-   starting server/agent processes
-   configuring Zabbix frontend

If you want to run Zabbix agent as root, see [running agent as
root](/manual/appendix/install/run_agent_as_root).

Zabbix web service process, which is used for [scheduled report
generation](/manual/web_interface/frontend_sections/reports/scheduled),
requires Google Chrome browser. The browser is not included into
packages and has to be installed manually.</source>
      </trans-unit>
      <trans-unit id="99a402fa" xml:space="preserve">
        <source>#### Importing data with Timescale DB

With TimescaleDB, in addition to the import command for PostgreSQL, also
run:

    cat /usr/share/zabbix-sql-scripts/postgresql/timescaledb.sql | sudo -u zabbix psql zabbix

::: notewarning
TimescaleDB is supported with Zabbix server
only.
:::</source>
      </trans-unit>
      <trans-unit id="fa73411a" xml:space="preserve">
        <source>#### SELinux configuration

See [SELinux
configuration](/manual/installation/install_from_packages/rhel#selinux_configuration)
for RHEL.

After the frontend and SELinux configuration is done, restart the Apache
web server:

    service apache2 restart</source>
        <target state="needs-translation">#### Настройка SELinux

Смотрите [настройку SELinux](/manual/installation/install_from_packages/rhel_centos#настройка_selinux) для RHEL/CentOS.

После завершения настройки веб-интерфейса и SELinux, перезапустите веб-сервер Apache:

    # service apache2 restart</target>
      </trans-unit>
      <trans-unit id="a12da003" xml:space="preserve">
        <source>### Proxy installation

Once the required repository is added, you can install Zabbix proxy by
running:

    apt install zabbix-proxy-mysql zabbix-sql-scripts

Substitute 'mysql' in the command with 'pgsql' to use PostgreSQL, or
with 'sqlite3' to use SQLite3.

The package 'zabbix-sql-scripts' contains database schemas for all supported database management systems for both Zabbix server and Zabbix proxy and will be used for data import.</source>
        <target state="needs-translation">### Установка прокси

Как только требуемый репозиторий добавлен, вы можете установить Zabbix прокси выполнив следующую команду:

    # apt install zabbix-proxy-mysql

Замените 'mysql' в команде на 'pgsql', чтобы использовать PostgreSQL, или на 'sqlite3' чтобы использовать SQLite3 (только прокси).</target>
      </trans-unit>
      <trans-unit id="fe6abb8e" xml:space="preserve">
        <source>##### Creating database

[Create](/manual/appendix/install/db_scripts) a separate database for
Zabbix proxy.

Zabbix server and Zabbix proxy cannot use the same database. If they are
installed on the same host, the proxy database must have a different
name.</source>
        <target state="needs-translation">##### Создание базы данных

[Создайте](/manual/appendix/install/db_scripts) отдельную базу данных для Zabbix прокси.

Zabbix сервер и Zabbix прокси не могут использовать одну и ту же базу данных. Если они установлены на одном хосте, тогда имя базы данных прокси должно отличаться.</target>
      </trans-unit>
      <trans-unit id="2ab835d7" xml:space="preserve">
        <source>##### Importing data

Import initial schema:

    cat /usr/share/zabbix-sql-scripts/mysql/proxy.sql | mysql -uzabbix -p zabbix

For proxy with PostgreSQL (or SQLite):

    cat /usr/share/zabbix-sql-scripts/postgresql/proxy.sql | sudo -u zabbix psql zabbix
    cat /usr/share/zabbix-sql-scripts/sqlite3/proxy.sql | sqlite3 zabbix.db</source>
      </trans-unit>
      <trans-unit id="d0a225c7" xml:space="preserve">
        <source>##### Configure database for Zabbix proxy

Edit zabbix\_proxy.conf:

    vi /etc/zabbix/zabbix_proxy.conf
    DBHost=localhost
    DBName=zabbix
    DBUser=zabbix
    DBPassword=&lt;password&gt;

In DBName for Zabbix proxy use a separate database from Zabbix server.

In DBPassword use Zabbix database password for MySQL; PostgreSQL user
password for PostgreSQL.

Use `DBHost=` with PostgreSQL. You might want to keep the default
setting `DBHost=localhost` (or an IP address), but this would make
PostgreSQL use a network socket for connecting to Zabbix. Refer to the
[respective
section](/manual/installation/install_from_packages/rhel#selinux_configuration)
for RHEL for instructions.</source>
        <target state="needs-translation">##### Настройка базы данных для Zabbix прокси

Отредактируйте zabbix\_proxy.conf:

    # vi /etc/zabbix/zabbix_proxy.conf
    DBHost=localhost
    DBName=zabbix
    DBUser=zabbix
    DBPassword=&lt;пароль&gt;

В DBName для Zabbix прокси используйте базу данных, отличную от базы данных Zabbix сервера.

В DBPassword используйте пароль к базе данных Zabbix для MySQL; пароль к PosgreSQL пользователю для PosgreSQL.

Используйте `DBHost=` с PostgreSQL. Вы возможно захотите оставить настройку по умолчанию `DBHost=localhost` (или IP адрес), но в этом случае PostgreSQL будет использовать сетевой сокет для подключения к Zabbix. Обратитесь к  [соответствующему разделу](/manual/installation/install_from_packages/rhel_centos#настройка_selinux) для получения инструкций от RHEL/CentOS.</target>
      </trans-unit>
      <trans-unit id="27de2ced" xml:space="preserve">
        <source>##### Starting Zabbix proxy process

To start a Zabbix proxy process and make it start at system boot:

    systemctl restart zabbix-proxy
    systemctl enable zabbix-proxy</source>
        <target state="needs-translation">##### Запуск процесса Zabbix прокси

Чтобы запустить процесс Zabbix прокси и добавить его в автозагрузку при загрузке системы, выполните следующие команды:

    # systemctl restart zabbix-proxy
    # systemctl enable zabbix-proxy</target>
      </trans-unit>
      <trans-unit id="871a973b" xml:space="preserve">
        <source>##### Frontend configuration

A Zabbix proxy does not have a frontend; it communicates with Zabbix
server only.</source>
        <target state="needs-translation">##### Настройка веб-интерфейса

Zabbix прокси не имеет веб-интерфейса; прокси обменивается информацией только с Zabbix сервером.</target>
      </trans-unit>
      <trans-unit id="cd9340bd" xml:space="preserve">
        <source>### Java gateway installation

It is required to install [Java gateway](/manual/concepts/java) only if
you want to monitor JMX applications. Java gateway is lightweight and
does not require a database.

Once the required repository is added, you can install Zabbix Java
gateway by running:

    apt install zabbix-java-gateway

Proceed to [setup](/manual/concepts/java/from_debian_ubuntu) for more
details on configuring and running Java gateway.</source>
        <target state="needs-translation">### Установка Java gateway

[Java gateway](/manual/concepts/java) необходимо устанавливать только, если вы хотите мониторить JMX приложения. Java gateway легковесный и не требует наличия базы данных.

Как только требуемый репозиторий добавлен, вы можете установить Zabbix Java gateway выполнив следующую команду:

    # apt install zabbix-java-gateway

Перейдите к [разделу настройки](/manual/concepts/java/from_debian_ubuntu) для получения более подробных сведений касательно настройки и запуску Java gateway.</target>
      </trans-unit>
    </body>
  </file>
</xliff>
