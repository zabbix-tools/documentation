[comment]: # translation:outdated

[comment]: # ({51abb0f6-51abb0f6})
# 3 SUSE Linux Enterprise Server

[comment]: # ({/51abb0f6-51abb0f6})

[comment]: # ({new-ee73b6da})
### Обзор

Официальные пакеты Zabbix доступны для:

|   |   |
|---|---|
|SUSE Linux Enterprise Server 15|[Download](https://www.zabbix.com/ru/download?zabbix=6.0&os_distribution=suse_linux_enterprise_server&os_version=15&db=mysql)|
|SUSE Linux Enterprise Server 12|[Download](https://www.zabbix.com/ru/download?zabbix=6.0&os_distribution=suse_linux_enterprise_server&os_version=12&db=mysql)|

::: noteimportant
Zabbix 6.0 еще не выпущен. Ссылки на загрузку ведут pre-6.0 пакетам.
:::

::: noteclassic
 *Удостоверение CA* [encryption mode](/manual/appendix/install/db_encrypt/mysql) не работает на SLES 12 (на всех минорных версиях ОС) с MySQL из-за устаревших библиотек MySQL. 
:::

[comment]: # ({/new-ee73b6da})

[comment]: # ({new-37fbf2db})
### Добавление репозитория Zabbix

Установите пакет конфигурации репозитория. Этот пакет содержит файлы конфигурации yum (менеджер пакетов приложений).

SLES 15:

    # rpm -Uvh --nosignature https://repo.zabbix.com/zabbix/6.0/sles/15/x86_64/zabbix-release-6.0-1.sles15.noarch.rpm
    # zypper --gpg-auto-import-keys refresh 'Zabbix Official Repository' 

SLES 12:

    # rpm -Uvh --nosignature https://repo.zabbix.com/zabbix/6.0/sles/12/x86_64/zabbix-release-6.0-1.sles12.noarch.rpm
    # zypper --gpg-auto-import-keys refresh 'Zabbix Official Repository' 

Пожалуйста, обратите внимание, процессу веб-сервиса Zabbix, который используется для [генерации регулярных отчетов](/manual/web_interface/frontend_sections/reports/scheduled), требуется браузер Google Chrome. Этот браузер не включен в пакеты и его нужно установить отдельно вручную.

[comment]: # ({/new-37fbf2db})

[comment]: # ({e7c666ed-ef4f3e50})
### Установка сервера / веб-интерфейса / агента

Чтобы установить Zabbix сервер / веб-интерфейс / агента с поддержкой MySQL, выполните следующую команду:

    # zypper install zabbix-server-mysql zabbix-web-mysql zabbix-apache-conf zabbix-agent

Замените 'apache' в команде на 'nginx' при использовании Nginx веб-сервера. Смотрите также: [Установка Nginx для Zabbix на SLES 12/15](/manual/appendix/install/nginx).

Замените 'zabbix-agent' на 'zabbix-agent2' в этих командах, чтобы использовать Zabbix агент 2 (только SLES 15 SP1+).

Чтобы установить Zabbix прокси с поддержкой MySQL:

    # zypper install zabbix-proxy-mysql

Замените 'mysql' в командах на 'pgsql', чтобы использовать PostgreSQL.

[comment]: # ({/e7c666ed-ef4f3e50})

[comment]: # ({b0827242-c573e862})
#### Создание базы данных

Для демонов Zabbix [сервера](/manual/concepts/server) и [прокси](/manual/concepts/proxy) требуется база данных. Она не требуется для запуска Zabbix [агента](/manual/concepts/agent).

::: notewarning
Для Zabbix сервера и Zabbix прокси необходимы раздельные базы данных; они не могут использовать одну и ту же базу данных. Поэтому, если они установлены на одном хосте, их базы данных должны быть созданы с разными именами!
:::

Создайте базу данных, используя предоставленные инструкции для [MySQL](/manual/appendix/install/db_scripts#mysql) или [PostgreSQL](/manual/appendix/install/db_scripts#postgresql).

[comment]: # ({/b0827242-c573e862})

[comment]: # ({e9e419e6-35bc057d})
#### Импорт данных

Теперь импортируйте исходную схему и данные для **сервера** с MySQL:

    # zcat /usr/share/doc/packages/zabbix-sql-scripts/mysql/create.sql.gz | mysql -uzabbix -p zabbix

Вам будет предложено ввести пароль к только что созданной базы данных.

С PostgreSQL:

    # zcat /usr/share/doc/packages/zabbix-sql-scripts/postgresql/create.sql.gz | sudo -u zabbix psql zabbix

С TimescaleDB, в дополнение к предыдущей команде, также выполните:

    # zcat /usr/share/doc/packages/zabbix-sql-scripts/postgresql/timescaledb.sql.gz | sudo -u <имя пользователя> psql zabbix

::: notewarning
TimescaleDB поддерживается только с Zabbix сервером.
:::

Для прокси импортируйте исходную схему:

    # zcat /usr/share/doc/packages/zabbix-sql-scripts/mysql/schema.sql.gz | mysql -uzabbix -p zabbix

Для прокси с PostgreSQL:

    # zcat /usr/share/doc/packages/zabbix-sql-scripts/postgresql/schema.sql.gz | sudo -u zabbix psql zabbix

[comment]: # ({/e9e419e6-35bc057d})

[comment]: # ({8eeff15b-0ea127cd})
####  Настройка базы данных для Zabbix сервера / прокси

Отредактируйте /etc/zabbix/zabbix\_server.conf (и zabbix\_proxy.conf), чтобы использовались соответствующие базы данных. Например:

    # vi /etc/zabbix/zabbix_server.conf
    DBHost=localhost
    DBName=zabbix
    DBUser=zabbix
    DBPassword=<пароль>

В DBPassword используйте пароль к базе данных Zabbix для MySQL; пароль к PostgreSQL пользователю для PosgreSQL.

Используйте `DBHost=` с PostgreSQL. Вы возможно захотите оставить настройку по умолчанию `DBHost=localhost` (или IP адрес), но в этом случае PostgreSQL будет использовать сетевой сокет для подключения к Zabbix.

[comment]: # ({/8eeff15b-0ea127cd})

[comment]: # ({9a437151-d162545b})
#### Настройка Zabbix веб-интерфейса

В зависимости от используемого веб-сервера (Apache/Nginx) отредактируйте файл конфигурации веб-интерфейса Zabbix:

-   В Apache файл конфигурации располагается в `/etc/apache2/conf.d/zabbix.conf`. Некоторые настройки PHP уже выполнены. Однако, необходимо раскомментировать настройку "date.timezone" и [задать верный часовой пояс](https://www.php.net/manual/ru/timezones.php) для вас.

```{=html}
<!-- -->
```
    php_value max_execution_time 300
    php_value memory_limit 128M
    php_value post_max_size 16M
    php_value upload_max_filesize 2M
    php_value max_input_time 300
    php_value max_input_vars 10000
    php_value always_populate_raw_post_data -1
    # php_value date.timezone Europe/Riga

-   Пакет zabbix-nginx-conf устанавливает отдельный Nginx сервер для Zabbix веб-интерфейса. Его файл конфигурации располагается в `/etc/nginx/conf.d/zabbix.conf`. Чтобы веб-интерфейс Zabbix заработал, необходимо раскомментировать и задать директивы `listen` и / или `server_name`.

```{=html}
<!-- -->
```
    # listen 80;
    # server_name example.com;

-   Zabbix использует свой выделенный пул соединений с Nginx:

Его файл конфигурации располагается в `/etc/php7/fpm/php-fpm.d/zabbix.conf`. Некоторые настройки PHP уже выполнены. Однако, необходимо задать верное для вас значение [date.timezone](http://php.net/manual/en/timezones.php) настройке.

    php_value[max_execution_time] = 300
    php_value[memory_limit] = 128M
    php_value[post_max_size] = 16M
    php_value[upload_max_filesize] = 2M
    php_value[max_input_time] = 300
    php_value[max_input_vars] = 10000
    ; php_value[date.timezone] = Europe/Riga

Теперь вы готовы перейти к [шагам установки веб-интерфейса](/manual/installation/install#установка_веб_интерфейса) выполнение которых даст вам доступ к недавно установленному Zabbix.

Обратите внимание, что Zabbix прокси не имеет веб-интерфейса; прокси обменивается информацией только с Zabbix сервером.

[comment]: # ({/9a437151-d162545b})

[comment]: # ({137cad12-71fda19d})
#### Запуск процессов Zabbix сервера / агента

Чтобы запустить процессы Zabbix сервера и агента и добавить их в автозагрузку при загрузке системы, выполните следующие команды:

С Apache веб-сервером:

    # systemctl restart zabbix-server zabbix-agent apache2 php-fpm
    # systemctl enable zabbix-server zabbix-agent apache2 php-fpm

Замените 'apache2' на 'nginx' при использовании Nginx веб-сервера.

[comment]: # ({/137cad12-71fda19d})

[comment]: # ({9616bd65-8e553867})
### Установка debuginfo пакетов

Чтобы включить debuginfo репозиторий, отредактируйте */etc/zypp/repos.d/zabbix.repo* файл. Измените `enabled=0` на `enabled=1` у zabbix-debuginfo репозитория.

    [zabbix-debuginfo]
    name=Zabbix Official Repository debuginfo
    type=rpm-md
    baseurl=http://repo.zabbix.com/zabbix/4.5/sles/15/x86_64/debuginfo/
    gpgcheck=1
    gpgkey=http://repo.zabbix.com/zabbix/4.5/sles/15/x86_64/debuginfo/repodata/repomd.xml.key
    enabled=0
    update=1

Это изменение позволит вам установить zabbix-***<компонент>***-debuginfo пакеты.

[comment]: # ({/9616bd65-8e553867})
