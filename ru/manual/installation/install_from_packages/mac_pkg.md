[comment]: # translation:outdated

[comment]: # ({1f6e1ed8-bfa3f768})
# 5 Установка агента на Mac OS из PKG

[comment]: # ({/1f6e1ed8-bfa3f768})

[comment]: # ({ea2dccd2-5eec6c30})
#### Обзор

Zabbix Mac OS агент можно установить из пакетов установщика PKG, которые доступны для [загрузки](https://www.zabbix.com/ru/download_agents#tab:44). Доступны версии с и без поддержки шифрования.

[comment]: # ({/ea2dccd2-5eec6c30})

[comment]: # ({ebf69d5c-3688af62})
#### Установка агента

Агента можно установить с использованием графического пользовательского интерфейса или с командной строки, например:

    sudo installer -pkg zabbix_agent-5.4.0-macos-amd64-openssl.pkg -target /

Убедитесь, что используете верную версию Zabbix пакета в команде. Она должна совпадать с именем загруженного пакета.

[comment]: # ({/ebf69d5c-3688af62})

[comment]: # ({e05c5fb3-052e551c})
#### Запуск агента

Агент автоматически запустится после установки или перезагрузки.

Вы можете отредактировать файл конфигурации в `/usr/local/etc/zabbix/zabbix_agentd.conf`, если требуется.

Чтобы запустить агента вручную, вы можете выполнить:

    sudo launchctl start com.zabbix.zabbix_agentd

Для остановки агента вручную:

    sudo launchctl stop com.zabbix.zabbix_agentd

В процессе обновления существующий файл конфигурации не будет перезаписан. Вместо этого будет создан новый файл `zabbix_agentd.conf.NEW`, который можно просмотреть и затем обновить существующий файл конфигурации, если потребуется. Не забудьте перезапустить агента после внесения изменений в файл конфигурации.

[comment]: # ({/e05c5fb3-052e551c})

[comment]: # ({e678ab00-3150eaf4})
#### Устранение неполадок и удаление агента

Этот раздел перечисляет некоторые полезные команды, которые можно использовать для поиска неполадок и удаления установленного Zabbix агента.

Просмотр, запущен ли Zabbix агент:

    ps aux | grep zabbix_agentd

Просмотр, установлен ли Zabbix агент из пакетов:

    $ pkgutil --pkgs | grep zabbix 
    com.zabbix.pkg.ZabbixAgent

Просмотр списка файлов, которые установлены пакетом установщика (обратите внимание, начальный символ / не отображается в выводе ниже):

    $ pkgutil --only-files --files com.zabbix.pkg.ZabbixAgent
    Library/LaunchDaemons/com.zabbix.zabbix_agentd.plist                                                                                                                                                                                                                           
    usr/local/bin/zabbix_get                                                                                                                                                                                                                                                       
    usr/local/bin/zabbix_sender                                                                                                                                                                                                                                                    
    usr/local/etc/zabbix/zabbix_agentd/userparameter_examples.conf.NEW                                                                                                                                                                                                             
    usr/local/etc/zabbix/zabbix_agentd/userparameter_mysql.conf.NEW                                                                                                                                                                                                                
    usr/local/etc/zabbix/zabbix_agentd.conf.NEW                                                                                                                                                                                                                                    
    usr/local/sbin/zabbix_agentd

Остановка Zabbix агента, если он запущен при помощи `launchctl`:

    sudo launchctl unload /Library/LaunchDaemons/com.zabbix.zabbix_agentd.plist

Удаление файлов (включая конфигурацию и журналы), которые были установлены пакетом установщика:

    sudo rm -f /Library/LaunchDaemons/com.zabbix.zabbix_agentd.plist
    sudo rm -f /usr/local/sbin/zabbix_agentd
    sudo rm -f /usr/local/bin/zabbix_get
    sudo rm -f /usr/local/bin/zabbix_sender
    sudo rm -rf /usr/local/etc/zabbix
    sudo rm -rf /var/log/zabbix

Забыть, что Zabbix агент был установлен:

    sudo pkgutil --forget com.zabbix.pkg.ZabbixAgent

[comment]: # ({/e678ab00-3150eaf4})
