[comment]: # translation:outdated

[comment]: # ({5e865988-7050624a})
# 4 Установка Windows агента из MSI

[comment]: # ({/5e865988-7050624a})

[comment]: # ({5351a40a-04777b60})
#### Обзор

Zabbix агента для Windows можно установить из пакетов установщика Windows MSI (32-бит или 64-бит), который доступен для [загрузки](https://www.zabbix.com/ru/download_agents#tab:44).

32-битный пакет нельзя установить на 64-битную Windows.

Все пакеты поставляются с поддержкой TLS, однако, настройка TLS опциональна.

Поддерживается инсталляция как через UI, так и через командную строку.

[comment]: # ({/5351a40a-04777b60})

[comment]: # ({d2cd284b-0fc3b21c})
#### Шаги установки

Для установки дважды кликните на загруженном MSI файле.

![](../../../../assets/en/manual/installation/install_from_packages/msi0_b.png)

![](../../../../assets/en/manual/installation/install_from_packages/msi0_c.png)

Примите лицензионное соглашение, чтобы перейти на следующий шаг.

![](../../../../assets/en/manual/installation/install_from_packages/msi0_d.png)

Укажите следующие параметры.

|Параметр|Описание|
|---------|-----------|
|*Host name*|Имя узла сети.|
|*Zabbix server IP/DNS*|IP/DNS сервера Zabbix..|
|*Agent listen port*|Порт, который будет слушать агент (10050 по умолчанию).|
|*Server or Proxy for active checks*|IP/DNS сервера / прокси Zabbix для активных проверок агента.|
|*Enable PSK*|Отметьте, чтобы включить поддержку TLS через pre-shared ключ.|
|*Add agent location to the PATH*|Добавление расположения агента в переменную PATH.|

![](../../../../assets/en/manual/installation/install_from_packages/msi0_e.png)

Введите идентификатор и значение pre-shared ключа. Этот шаг доступен только, если  в предыдущем шаге вы отметили опцию *Enable PSK*.

![](../../../../assets/en/manual/installation/install_from_packages/msi0_f.png)

Выберите компоненты Zabbix для установки - [демон Zabbix агента](/manual/concepts/agent), [Zabbix
sender](/manual/concepts/sender), [Zabbix get](/manual/concepts/get).

![](../../../../assets/en/manual/installation/install_from_packages/msi0_g.png)

Zabbix компоненты вместе с файлом конфигурации будут установлены в директорию *Zabbix Agent* в Program Files. zabbix\_agentd.exe будет настроен службой Windows с автоматическим запуском.

![](../../../../assets/en/manual/installation/install_from_packages/msi0_h.png)

[comment]: # ({/d2cd284b-0fc3b21c})

[comment]: # ({e8196e6d-1ca3dd90})
#### Установка через командную строку

[comment]: # ({/e8196e6d-1ca3dd90})

[comment]: # ({new-89028c66})
##### Поддерживаемые параметры

MSI пакет поддерживает следующий набор параметров:

|Порядковый номер|Параметр|Описание|
|------|---------|-----------|
|1|LOGTYPE|<|
|2|LOGFILE|<|
|3|SERVER|<|
|4|LISTENPORT|<|
|5|SERVERACTIVE|<|
|6|HOSTNAME|<|
|7|TIMEOUT|<|
|8|TLSCONNECT|<|
|9|TLSACCEPT|<|
|10|TLSPSKIDENTITY|<|
|11|TLSPSKFILE|<|
|12|TLSPSKVALUE|<|
|13|TLSCAFILE|<|
|14|TLSCRLFILE|<|
|15|TLSSERVERCERTISSUER|<|
|16|TLSSERVERCERTSUBJECT|<|
|17|TLSCERTFILE|<|
|18|TLSKEYFILE|<|
|19|INSTALLFOLDER|<|
|20|ENABLEPATH|<|
|21|SKIP|`SKIP=fw` - dне устанавливать правило исключения брандмауэру|
|22|INCLUDE|Последовательность добавлений, разделенных `;`|
|23|ALLOWDENYKEY|Последовательность "AllowKey" и "DenyKey" [параметров](/manual/config/items/restrict_checks), разделенных `;`. Используйте символ `\\;`, чтобы экранировать символ разделителя.|

Чтобы установить вы можете выполнить, например, следующее:

    SET INSTALLFOLDER=C:\Program Files\za

    msiexec /l*v log.txt /i zabbix_agent-4.0.6-x86.msi /qn^
     LOGTYPE=file^
     LOGFILE="%INSTALLFOLDER%\za.log"^
     SERVER=192.168.6.76^
     LISTENPORT=12345^
     SERVERACTIVE=::1^
     HOSTNAME=myHost^
     TLSCONNECT=psk^
     TLSACCEPT=psk^
     TLSPSKIDENTITY=MyPSKID^
     TLSPSKFILE="%INSTALLFOLDER%\mykey.psk"^
     TLSCAFILE="c:\temp\f.txt1"^
     TLSCRLFILE="c:\temp\f.txt2"^
     TLSSERVERCERTISSUER="My CA"^
     TLSSERVERCERTSUBJECT="My Cert"^
     TLSCERTFILE="c:\temp\f.txt5"^
     TLSKEYFILE="c:\temp\f.txt6"^
     ENABLEPATH=1^
     INSTALLFOLDER="%INSTALLFOLDER%"^
     SKIP=fw^
     ALLOWDENYKEY="DenyKey=vfs.file.contents[/etc/passwd]"

или

    msiexec /l*v log.txt /i zabbix_agent-4.4.0-x86.msi /qn^
     SERVER=192.168.6.76^
     TLSCONNECT=psk^
     TLSACCEPT=psk^
     TLSPSKIDENTITY=MyPSKID^
     TLSPSKVALUE=1f87b595725ac58dd977beef14b97461a7c1045b9a1c963065002c5473194952

[comment]: # ({/new-89028c66})

[comment]: # ({new-bfabef9c})
##### Examples

To install Zabbix Windows agent from the command-line, you may run, for example:

    SET INSTALLFOLDER=C:\Program Files\za

    msiexec /l*v log.txt /i zabbix_agent-7.0.0-x86.msi /qn^
     LOGTYPE=file^
     LOGFILE="%INSTALLFOLDER%\za.log"^
     SERVER=192.168.6.76^
     LISTENPORT=12345^
     SERVERACTIVE=::1^
     HOSTNAME=myHost^
     TLSCONNECT=psk^
     TLSACCEPT=psk^
     TLSPSKIDENTITY=MyPSKID^
     TLSPSKFILE="%INSTALLFOLDER%\mykey.psk"^
     TLSCAFILE="c:\temp\f.txt1"^
     TLSCRLFILE="c:\temp\f.txt2"^
     TLSSERVERCERTISSUER="My CA"^
     TLSSERVERCERTSUBJECT="My Cert"^
     TLSCERTFILE="c:\temp\f.txt5"^
     TLSKEYFILE="c:\temp\f.txt6"^
     ENABLEPATH=1^
     INSTALLFOLDER="%INSTALLFOLDER%"^
     SKIP=fw^
     ALLOWDENYKEY="DenyKey=vfs.file.contents[/etc/passwd]"

You may also run, for example:

    msiexec /l*v log.txt /i zabbix_agent-7.0.0-x86.msi /qn^
     SERVER=192.168.6.76^
     TLSCONNECT=psk^
     TLSACCEPT=psk^
     TLSPSKIDENTITY=MyPSKID^
     TLSPSKVALUE=1f87b595725ac58dd977beef14b97461a7c1045b9a1c963065002c5473194952

::: noteclassic
If both TLSPSKFILE and TLSPSKVALUE are passed, then TLSPSKVALUE will be written to TLSPSKFILE.
:::

[comment]: # ({/new-bfabef9c})
