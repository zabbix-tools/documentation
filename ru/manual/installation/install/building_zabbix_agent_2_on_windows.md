[comment]: # translation:outdated

[comment]: # ({778c11c0-22c598e9})
# Сборка Zabbix агент 2 на Windows

[comment]: # ({/778c11c0-22c598e9})

[comment]: # ({63dc0e95-84651997})
#### Обзор

В этом разделе иллюстрируется как собрать Zabbix агент 2 (Windows) из исходного кода.

[comment]: # ({/63dc0e95-84651997})

[comment]: # ({3a3421a9-2a4da91c})
#### Установка компилятора MinGW

1. Загрузите MinGW-w64 с Обработкой Исключений SJLJ (set jump/long jump) и потоками Windows (например, *x86\_64-8.1.0-release-win32-sjlj-rt\_v6-rev0.7z*)\
2. Извлеките архив и поместите содержимое в *c:\\mingw*\
3. Задайте переменную среды

    @echo off
    set PATH=%PATH%;c:\mingw\bin
    cmd

При компиляции используйте командную строку Windows вместо терминала MSYS, который поставляется с MinGW

[comment]: # ({/3a3421a9-2a4da91c})

[comment]: # ({37a47b8f-dec81522})
#### Компиляция библиотек разработки PCRE

Следующие инструкции помогут скомпилировать и установить 64-битные библиотеки PCRE в *c:\\dev\\pcre* в 32-битные библиотеки в *c:\\dev\\pcre32*:

1\. Загрузите PCRE библиотеку версии 8.XX с pcre.org (<http://ftp.pcre.org/pub/pcre/>) и извлеките содержимое\
2. Откройте *cmd* и перейдите к извлеченному исходному коду

[comment]: # ({/37a47b8f-dec81522})

[comment]: # ({25adefc8-5def516d})
##### Сборка 64-битной PCRE

1\. Удалите старую конфигурацию / кэш, если имеется:

    del CMakeCache.txt
    rmdir /q /s CMakeFiles

2\. Запустите cmake (CMake можно установить с <https://cmake.org/download/>):

    cmake -G "MinGW Makefiles" -DCMAKE_C_COMPILER=gcc -DCMAKE_C_FLAGS="-O2 -g" -DCMAKE_CXX_FLAGS="-O2 -g" -DCMAKE_INSTALL_PREFIX=c:\dev\pcre

3\. Затем, выполните:

    mingw32-make clean
    mingw32-make install

[comment]: # ({/25adefc8-5def516d})

[comment]: # ({af6335b1-04b79926})
##### Сборка 32-битной PCRE

1\. Запустите:

    mingw32-make clean

2\. Удалите *CMakeCache.txt*:

    del CMakeCache.txt
    rmdir /q /s CMakeFiles

3\. Запустите cmake:

    cmake -G "MinGW Makefiles" -DCMAKE_C_COMPILER=gcc -DCMAKE_C_FLAGS="-m32 -O2 -g" -DCMAKE_CXX_FLAGS="-m32 -O2 -g" -DCMAKE_EXE_LINKER_FLAGS="-Wl,-mi386pe" -DCMAKE_INSTALL_PREFIX=c:\dev\pcre32

4\. Затем, выполните:

    mingw32-make install

[comment]: # ({/af6335b1-04b79926})

[comment]: # ({5d25fb29-80e7f73a})
#### Установка библиотек разработки OpenSSL

1\. Загрузите 32 и 64-битные сборки с <https://curl.se/windows/>\
2. Извлеките файлы в *c:\\dev\\openssl32* и *c:\\dev\\openssl* директории соответственно.\
3. После чего, удалите извлеченные *\*.dll.a* (библиотеки оболочки для вызова dll) так как MinGW ставит приоритет использования именно их перед использованием статических библиотек.

[comment]: # ({/5d25fb29-80e7f73a})

[comment]: # ({10e0e573-f4af7b47})
#### Сборка Zabbix агент 2

[comment]: # ({/10e0e573-f4af7b47})

[comment]: # ({faa95aea-5f096f18})
##### 32-битный

Откройте MinGW среду (Windows командную строку) и перейдите к *build/mingw* директории в дереве исходного кода Zabbix.

Выполните:

    mingw32-make clean
    mingw32-make ARCH=x86 PCRE=c:\dev\pcre32 OPENSSL=c:\dev\openssl32

[comment]: # ({/faa95aea-5f096f18})

[comment]: # ({cacb2fa9-f7876f1d})
##### 64-битный

Откройте MinGW среду (Windows командную строку) и перейдите к *build/mingw* директории в дереве исходного кода Zabbix.

Выполните:

    mingw32-make clean
    mingw32-make PCRE=c:\dev\pcre OPENSSL=c:\dev\openssl

::: noteclassic
На 64-битной платформе можно собрать 32-битные и 64-битные версии, но на 32-битной платформе можно собрать только 32-битную версию. При работе на 32-битной платформе следуйте тем же шагам, что и для  64-битной версии на 64-битной платформе. 
:::

[comment]: # ({/cacb2fa9-f7876f1d})
