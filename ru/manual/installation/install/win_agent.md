[comment]: # translation:outdated

[comment]: # ({5dba5d24-1f267b18})
# Сборка Zabbix агента на Windows

[comment]: # ({/5dba5d24-1f267b18})

[comment]: # ({844d1d98-2cee80d5})
#### Oбзор

Этот раздел демонстрирует как можно собрать бинарные файлы Zabbix агента на Windows из исходного кода с / без поддержки TLS.

[comment]: # ({/844d1d98-2cee80d5})

[comment]: # ({44f64a60-8b346a36})
#### Компиляция OpenSSL

Следующие шаги помогут вам скомпилировать OpenSSL из исходного кода на MS Windows 10 (64-bit).

1.  Для компиляции OpenSSL на машине с Windows потребуются:
    1.  Компилятор C языка (например, VS 2017 RC),
    2.  NASM (<https://www.nasm.us/>),
    3.  Perl (например, Strawberry Perl с <http://strawberryperl.com/>),
    4.  Perl модуль Text::Template (cpan Text::Template).
2.  Загрузите исходный код OpenSSL с <https://www.openssl.org/>. В данном примере используется OpenSSL 1.1.1.
3.  Разархивируйте исходный код OpenSSL, например, в E:\\openssl-1.1.1.
4.  Откройте окно командной строки, например, x64 Native Tools Command Prompt для VS 2017 RC.
5.  Перейдите в директорию с исходным кодом OpenSSL, например, E:\\openssl-1.1.1.
    1.  Удостоверьтесь, что NASM доступен:`e:\openssl-1.1.1> nasm --version NASM version 2.13.01 compiled on May  1 2017`
6.  Сконфигурируйте OpenSSL, например:`e:\openssl-1.1.1> perl E:\openssl-1.1.1\Configure VC-WIN64A no-shared no-capieng no-srp no-gost no-dgram no-dtls1-method no-dtls1_2-method  --api=1.1.0 --prefix=C:\OpenSSL-Win64-111-static --openssldir=C:\OpenSSL-Win64-111-static`
    -   Обратите внимание на опцию 'no-shared': при использовании 'no-shared' статические библиотеки OpenSSL libcrypto.lib и libssl.lib будут 'самодостаточными' и в результате бинарные файлы Zabbix будут включать в себя OpenSSL, не потребуется внешних DLL от OpenSSL. Преимущество: бинарные файлы Zabbix можно будет копировать на другие Windows машины без необходимости наличия библиотек OpenSSL. Недостаток: когда будет выпущена новая версия OpenSSL с исправлением ошибок, Zabbix агента будет необходимо скомпилировать и установить заново.
    -   Если 'no-shared' не используется, тогда статические библиотеки libcrypto.lib и libssl.lib будут использоваться во время выполнения DLL OpenSSL. Преимущество: когда будет выпущена новая версия OpenSSL с исправлением ошибок, возможно вы сможете обновить только OpenSSL DLL, без необходимости повторной компиляции Zabbix агента. Недостаток: при копировании Zabbix агента на другую машину необходимо также копировать OpenSSL DLL.
7.  Скомпилируйте OpenSSL, проведите тесты, установите:`e:\openssl-1.1.1> nmake
    e:\openssl-1.1.1> nmake test
    ...
    All tests successful.
    Files=152, Tests=1152, 501 wallclock secs ( 0.67 usr +  0.61 sys =  1.28 CPU)
    Result: PASS
    e:\openssl-1.1.1> nmake install_sw
    `'install\_sw' установит только компоненты приложения (такие как - библиотеки, файлы заголовков, но без документации). Если вы хотите установить всё, используйте "nmake install".

[comment]: # ({/44f64a60-8b346a36})

[comment]: # ({dbf73c31-22b944ad})
#### Компиляция PCRE

1.  Загрузите библиотеку PCRE или PCRE2 (поддерживается начиная с Zabbix 6.0) с pcre.org репозитория:  (<https://github.com/PhilipHazel/pcre2/releases/download/pcre2-10.39/pcre2-10.39.zip>)
2.  Извлеките архив в директорию *E:\\pcre2-10.39*
3.  Установите CMake с <https://cmake.org/download/>, в процессе установки выберите и убедитесь, что cmake\\bin существует в вашей директории (протестировано на версии 3.9.4).
4.  Создайте новую пустую директорию для сборки, предпочтительно создать поддиректорию в директории с исходным кодом. Например, *E:\\pcre2-10.39\\build*.
5.  Откройте окно командной строки, например, x64 Native Tools Command Prompt for VS 2017 и из этой командной оболочки выполните cmake-gui. Не пытайтесь запустить Cmake из меню Пуск Windows, так как такой запуск может привести к ошибкам.
6.  Ввведите *E:\\pcre2-10.39* и *E:\\pcre2-10.39\\build* для директорий с исходным кодом и для готовой сборки, соответственно.
7.  Нажмите на кнопку "Configure".
8.  При выборе генератора для этого проекта выберите "NMake Makefiles".
9.  Создайте новую, пустую директорию для установки. Например, *E:\\pcre2-10.39-install*.
10. Затем GUI перечислит несколько опций конфигурирования. Убедитесь, что следующие опции выбраны:
    -   **PCRE\_SUPPORT\_UNICODE\_PROPERTIES** ON
    -   **PCRE\_SUPPORT\_UTF** ON
    -   **CMAKE\_INSTALL\_PREFIX** *E:\\pcre2-10.39-install*
11. Нажмите "Configure" снова. Соседняя кнопка "Generate" теперь должна быть активной.
12. Нажмите "Generate".
13. В случае возникновения ошибок рекомендуется удалить кэш CMake перед попыткой повторить процесс сборки CMake. В CMake GUI кэш можно удалить выбрав "File > Delete Cache".
14. Директория для готовой сборки теперь должна содержать пригодную систему для сборки - *Makefile*.
15. Откройте окно командной строки, например, x64 Native Tools Command Prompt for VS 2017 и перейдите к вышеупомянутому *Makefile*.
16. Выполните команду NMake: `E:\pcre2-10.39\build> nmake install`

[comment]: # ({/dbf73c31-22b944ad})

[comment]: # ({41ba3c01-30b6b8b5})
#### Компиляция Zabbix

Следующие шаги помогут вам скомпилировать Zabbix из исходного кода на MS Windows 10 (64-bit). Четвертый шаг - единственное существенное отличие при компиляции Zabbix с / без поддержки TLS.

1.  На Linux машине выгрузите исходный код с git:`` $ git clone https://git.zabbix.com/scm/zbx/zabbix.git
    $ cd zabbix
    $ ./bootstrap.sh
    $ ./configure --enable-agent --enable-ipv6 --prefix=`pwd`
    $ make dbschema
    $ make dist
     ``
2.  Скопируйте и распакуйте архив, например zabbix-4.4.0.tar.gz, на Windows машине.
3.  Давайте предположим, что исходный код располагается в e:\\zabbix-4.4.0. Откройте окно командной строки, например, x64 Native Tools Command Prompt for VS 2017 RC. Перейдите в E:\\zabbix-4.4.0\\build\\win32\\project.
4.  Скомпилируйте zabbix\_get, zabbix\_sender и zabbix\_agent.
    -   без поддержки TLS:
        `E:\zabbix-4.4.0\build\win32\project> nmake /K PCREINCDIR=E:\pcre2-10.39-install\include PCRELIBDIR=E:\pcre2-10.39-install\lib`
    -   с поддержкой TLS:
        `E:\zabbix-4.4.0\build\win32\project> nmake /K -f Makefile_get TLS=openssl TLSINCDIR=C:\OpenSSL-Win64-111-static\include TLSLIBDIR=C:\OpenSSL-Win64-111-static\lib PCREINCDIR=E:\pcre2-10.39-install\include PCRELIBDIR=E:\pcre2-10.39-install\lib
        E:\zabbix-4.4.0\build\win32\project> nmake /K -f Makefile_sender TLS=openssl TLSINCDIR="C:\OpenSSL-Win64-111-static\include TLSLIBDIR="C:\OpenSSL-Win64-111-static\lib" PCREINCDIR=E:\pcre2-10.39-install\include PCRELIBDIR=E:\pcre2-10.39-install\lib
        E:\zabbix-4.4.0\build\win32\project> nmake /K -f Makefile_agent TLS=openssl TLSINCDIR=C:\OpenSSL-Win64-111-static\include TLSLIBDIR=C:\OpenSSL-Win64-111-static\lib PCREINCDIR=E:\pcre2-10.39-install\include PCRELIBDIR=E:\pcre2-10.39-install\lib`
5.  Новые бинарные файлы будут располагаться в e:\\zabbix-4.4.0\\bin\\win64. Так как OpenSSL скомпилирован с 'no-shared' опцией, бинарные файлы Zabbix внутри себя будут содержать OpenSSL и их можно будет скопировать на другие машины, на которых OpenSSL отсутствует.

[comment]: # ({/41ba3c01-30b6b8b5})

[comment]: # ({6cad7e55-c8fe4039})
#### Компиляция Zabbix с LibreSSL

Процесс схож с компиляцией с поддержкой OpenSSL, но Вам будет необходимо
внести небольшие изменения в файлы расположенные в директории `build\win32\project`:

      * В ''Makefile_tls'' удалите ''/DHAVE_OPENSSL_WITH_PSK''. например, найдите <code>

CFLAGS = $(CFLAGS) /DHAVE\_OPENSSL
/DHAVE\_OPENSSL\_WITH\_PSK</code>и замените на
`CFLAGS =    $(CFLAGS) /DHAVE_OPENSSL`

      * В ''Makefile_common.inc'' добавьте ''/NODEFAULTLIB:LIBCMT'' например, найдите <code>

/MANIFESTUAC:"level='asInvoker' uiAccess='false'" /DYNAMICBASE:NO
/PDB:$(TARGETDIR)\\$(TARGETNAME).pdb</code>и замените на
`/MANIFESTUAC:"level='asInvoker' uiAccess='false'" /DYNAMICBASE:NO /PDB:$(TARGETDIR)\$(TARGETNAME).pdb /NODEFAULTLIB:LIBCMT`

[comment]: # ({/6cad7e55-c8fe4039})
