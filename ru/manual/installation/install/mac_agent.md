[comment]: # translation:outdated

[comment]: # ({f5cc57fb-0535e4cb})
# Сборка Zabbix агента на macOS

[comment]: # ({/f5cc57fb-0535e4cb})

[comment]: # ({52c64fc2-161fafaa})
#### Обзор

Этот раздел демонстрирует как можно собрать бинарные файлы Zabbix агента для macOS из исходного кода с / без поддержки TLS.

[comment]: # ({/52c64fc2-161fafaa})

[comment]: # ({427b4c89-7e7224fc})
#### Предварительные требования

Вам потребуются средства разработки для командной строки (Xcode не требуется), Automake, pkg-config and PCRE (v8.x) или PCRE2 (v10.x). Если Вы хотите собрать бинарные файлы агента с поддержкой TLS, вам также понадобится OpenSSL или GnuTLS.

Чтобы установит Automake и pkg-config, вам потребуется менеджер пакетов Homebrew с <https://brew.sh/>. Чтобы его установить, откройте терминал и выполните следующую команду:

    $ /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

Затем установите Automake и pkg-config:

    $ brew install automake
    $ brew install pkg-config

Подготовка библиотек PCRE, OpenSSL и GnuTLS зависит от того, каким образом будет выполняться линковка к агенту.

Если вы намерены запускать бинарные файлы агента на macOS машине на котором уже имеются данные библиотеки, можно использовать предварительно скомпилированные библиотеки поставляемые Homebrew. Обычно это macOS машины, которые используют Homebrew для сборки бинарных файлов Zabbix агента или для других целей.

Если бинарные файлы агента будут использоваться на macOS машинах на которых отсутствуют разделяемые версии библиотек, вам потребуется скомпилировать статические версии этих библиотек из исходного кода и выполнить линковку Zabbix агента к ним.

[comment]: # ({/427b4c89-7e7224fc})

[comment]: # ({74632912-5e97a918})
#### Сборка бинарных файлов агента с разделяемыми библиотеками

Установите PCRE2 (замените *pcre2* на *pcre* в команде ниже по необходимости):

    $ brew install pcre2

При сборке с поддержкой TLS, установите OpenSSL и / или GnuTLS:

    $ brew install openssl
    $ brew install gnutls

Загрузите исходный код Zabbix:

    $ git clone https://git.zabbix.com/scm/zbx/zabbix.git

Сборка агента без поддержки TLS:

    $ cd zabbix
    $ ./bootstrap.sh
    $ ./configure --sysconfdir=/usr/local/etc/zabbix --enable-agent --enable-ipv6
    $ make
    $ make install

Сборка агента с OpenSSL:

    $ cd zabbix
    $ ./bootstrap.sh
    $ ./configure --sysconfdir=/usr/local/etc/zabbix --enable-agent --enable-ipv6 --with-openssl=/usr/local/opt/openssl
    $ make
    $ make install

Сборка агента с GnuTLS:

    $ cd zabbix-source/
    $ ./bootstrap.sh
    $ ./configure --sysconfdir=/usr/local/etc/zabbix --enable-agent --enable-ipv6 --with-gnutls=/usr/local/opt/gnutls
    $ make
    $ make install

[comment]: # ({/74632912-5e97a918})

[comment]: # ({57a587e2-90fe7eb9})
#### Сборка бинерных файлов агента со статическими библиотеками без TLS

Предположим, что статические библиотеки PCRE уже установлены в `$HOME/static-libs`. Мы будем использовать PCRE2 10.39.

    $ PCRE_PREFIX="$HOME/static-libs/pcre2-10.39"

Загрузите и соберите PCRE с поддержкой свойств Юникод:

    $ mkdir static-libs-source
    $ cd static-libs-source
    $ curl --remote-name https://github.com/PhilipHazel/pcre2/releases/download/pcre2-10.39/pcre2-10.39.tar.gz
    $ tar xf pcre2-10.39.tar.gz
    $ cd pcre2-10.39
    $ ./configure --prefix="$PCRE_PREFIX" --disable-shared --enable-static --enable-unicode-properties
    $ make
    $ make check
    $ make install

Загрузите исходный код Zabbix и соберите агента:

    $ git clone https://git.zabbix.com/scm/zbx/zabbix.git
    $ cd zabbix
    $ ./bootstrap.sh
    $ ./configure --sysconfdir=/usr/local/etc/zabbix --enable-agent --enable-ipv6 --with-libpcre2="$PCRE_PREFIX"
    $ make
    $ make install

[comment]: # ({/57a587e2-90fe7eb9})

[comment]: # ({f8a3b4d3-b985c38b})
#### Сборка бинарных файлов агента со статическими библиотеками с OpenSSL

После успешной сборки OpenSSL рекомендуется выполнить `make test`. Иногда, даже после успешной сборки, тесты завершаются с ошибками. Если тесты завершаются с ошибками, проблемы необходимо рассмотреть и исправить перед тем как продолжить.

Предположим, что статические библиотеки PCRE и OpenSSL будут установлены в `$HOME/static-libs`. Мы будем использовать PCRE2 10.39 и OpenSSL 1.1.1a.

    $ PCRE_PREFIX="$HOME/static-libs/pcre2-10.39"
    $ OPENSSL_PREFIX="$HOME/static-libs/openssl-1.1.1a"

Давайте соберем статические библиотеки в `static-libs-source`:

    $ mkdir static-libs-source
    $ cd static-libs-source

Загрузите и соберите PCRE с поддержкой свойств Юникод:

    $ curl --remote-name https://github.com/PhilipHazel/pcre2/releases/download/pcre2-10.39/pcre2-10.39.tar.gz
    $ tar xf pcre2-10.39.tar.gz
    $ cd pcre2-10.39
    $ ./configure --prefix="$PCRE_PREFIX" --disable-shared --enable-static --enable-unicode-properties
    $ make
    $ make check
    $ make install
    $ cd ..

Загрузите и соберите OpenSSL:

    $ curl --remote-name https://www.openssl.org/source/openssl-1.1.1a.tar.gz
    $ tar xf openssl-1.1.1a.tar.gz
    $ cd openssl-1.1.1a
    $ ./Configure --prefix="$OPENSSL_PREFIX" --openssldir="$OPENSSL_PREFIX" --api=1.1.0 no-shared no-capieng no-srp no-gost no-dgram no-dtls1-method no-dtls1_2-method darwin64-x86_64-cc
    $ make
    $ make test
    $ make install_sw
    $ cd ..

Загрузите исходный код Zabbix и соберите агента:

    $ git clone https://git.zabbix.com/scm/zbx/zabbix.git
    $ cd zabbix
    $ ./bootstrap.sh
    $ ./configure --sysconfdir=/usr/local/etc/zabbix --enable-agent --enable-ipv6 --with-libpcre2="$PCRE_PREFIX" --with-openssl="$OPENSSL_PREFIX"
    $ make
    $ make install

[comment]: # ({/f8a3b4d3-b985c38b})

[comment]: # ({0455e313-8165604b})
#### Сборка бинарных файлов агента со статическими библиотеками с GnuTLS

GnuTLS зависит от низкоуровневой криптографической библиотеки Nettle и арифметической библиотеки GMP. Вместо использования полновесной библиотеки GMP, в этом руководстве будет использоваться mini-gmp, который включен в Nettle.

После успешной сборки GnuTLS и Nettle рекомендуется выполнить `make test`. Иногда, даже после успешной сборки, тесты завершаются с ошибками. Если тесты завершаются с ошибками, проблемы необходимо рассмотреть и исправить перед тем как продолжить.

Предположим, что статические библиотеки PCRE, Nettle и GnuTLS будут установлены в `$HOME/static-libs`. Мы будем использовать PCRE2 10.39, Nettle 3.4.1 и GnuTLS 3.6.5.

    $ PCRE_PREFIX="$HOME/static-libs/pcre2-10.39"
    $ NETTLE_PREFIX="$HOME/static-libs/nettle-3.4.1"
    $ GNUTLS_PREFIX="$HOME/static-libs/gnutls-3.6.5"

Давайте соберем статические библиотеки в `static-libs-source`:

    $ mkdir static-libs-source
    $ cd static-libs-source

Загрузите и соберите Nettle:

    $ curl --remote-name https://ftp.gnu.org/gnu/nettle/nettle-3.4.1.tar.gz
    $ tar xf nettle-3.4.1.tar.gz
    $ cd nettle-3.4.1
    $ ./configure --prefix="$NETTLE_PREFIX" --enable-static --disable-shared --disable-documentation --disable-assembler --enable-x86-aesni --enable-mini-gmp
    $ make
    $ make check
    $ make install
    $ cd ..

Загрузите и соберите GnuTLS:

    $ curl --remote-name https://www.gnupg.org/ftp/gcrypt/gnutls/v3.6/gnutls-3.6.5.tar.xz
    $ tar xf gnutls-3.6.5.tar.xz
    $ cd gnutls-3.6.5
    $ PKG_CONFIG_PATH="$NETTLE_PREFIX/lib/pkgconfig" ./configure --prefix="$GNUTLS_PREFIX" --enable-static --disable-shared --disable-guile --disable-doc --disable-tools --disable-libdane --without-idn --without-p11-kit --without-tpm --with-included-libtasn1 --with-included-unistring --with-nettle-mini
    $ make
    $ make check
    $ make install
    $ cd ..

Загрузите исходный код Zabbix и соберите агента:

    $ git clone https://git.zabbix.com/scm/zbx/zabbix.git
    $ cd zabbix
    $ ./bootstrap.sh
    $ CFLAGS="-Wno-unused-command-line-argument -framework Foundation -framework Security" \
    > LIBS="-lgnutls -lhogweed -lnettle" \
    > LDFLAGS="-L$GNUTLS_PREFIX/lib -L$NETTLE_PREFIX/lib" \
    > ./configure --sysconfdir=/usr/local/etc/zabbix --enable-agent --enable-ipv6 --with-libpcre2="$PCRE_PREFIX" --with-gnutls="$GNUTLS_PREFIX"
    $ make
    $ make install

[comment]: # ({/0455e313-8165604b})
