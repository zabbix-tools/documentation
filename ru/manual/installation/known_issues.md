[comment]: # translation:outdated

[comment]: # ({5469a5e5-97796c37})
# 8 Известные проблемы

[comment]: # ({/5469a5e5-97796c37})

[comment]: # ({b1a9bce9-e338613a})
#### Запуск прокси с MySQL 8.0.0-8.0.17

zabbix\_proxy на версиях MySQL 8.0.0-8.0.17 завершается со следующей ошибкой "access denied":

    [Z3001] connection to database 'zabbix' failed: [1227] Access denied; you need (at least one of) the SUPER, SYSTEM_VARIABLES_ADMIN or SESSION_VARIABLES_ADMIN privilege(s) for this operation

Такое поведение связано с тем, что MySQL 8.0.0 начал применять специальные права доступа к установке переменных сессий. Однако, в 8.0.18 версии такой подход отменен: [Начиная с MySQL 8.0.18, установка переменной сессий с этой системной переменной более не является запрещенной операцией.](https://dev.mysql.com/doc/refman/8.0/en/server-system-variables.html)

Временное решение основывается на предоставлении дополнительных привилегий `zabbix` пользователю:

Для версий MySQL 8.0.14 - 8.0.17:

    grant SESSION_VARIABLES_ADMIN on *.* to 'zabbix'@'localhost';

Для версий MySQL 8.0.0 - 8.0.13:

    grant SYSTEM_VARIABLES_ADMIN on *.* to 'zabbix'@'localhost';

[comment]: # ({/b1a9bce9-e338613a})


[comment]: # ({961b4888-68c1ee03})
#### Timescale DB

Версии PostgreSQL 9.6-12 используют слишком много памяти при обновлении таблиц с большим количеством секций ([смотрите отчет о проблеме](https://www.postgresql-archive.org/memory-problems-and-crash-of-db-when-deleting-data-from-table-with-thousands-of-partitions-td6108612.html)). Эта проблема проявляет себя, когда Zabbix выполняет обновление динамики изменений на системах с TimescaleDB, в случае если таблицы динамики изменений разбиты на относительно маленькие (наприме, по 1 дню) фрагменты. Что приводит к тому, что в таблицах динамики изменений присутствуют сотни фрагментов при настройках очистки истории по умолчанию - состояние, при котором PostgreSQL скорее всего может исчерпать всю оперативную память.

Эта проблемы решена начиная с Zabbix 5.0.1 для новых инсталляций с TimescaleDB, но если TimescaleDB был сконфигурирован с Zabbix до этого момента, пожалуйста обратитесь к [ZBX-16347](https://support.zabbix.com/browse/ZBX-16347?focusedCommentId=430816&page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel#comment-430816) за информацией по процессу миграции.

[comment]: # ({/961b4888-68c1ee03})

[comment]: # ({12675100-5c2a7ea7})
#### Обновление с MariaDB 10.2.1 и более старыми версиями

Обновление Zabbix может завершится с ошибкой, если таблицы базы данных были созданы с MariaDB 10.2.1 и с более старыми версиями, так как в этих версиях по умолчанию используется компактный формат строк. Такая проблема исправляется изменением формата строк на динамический формат (смотрите также [ZBX-17690](https://support.zabbix.com/browse/ZBX-17690)).

[comment]: # ({/12675100-5c2a7ea7})

[comment]: # ({d4a82255-40e33d04})
#### TLS подключение к базе данных с MariaDB

Подключение TLS к базе данных не поддерживается с опцией 'verify\_ca' для DBTLSConnect [параметра] /manual/appendix/config/zabbix_server) при использовании MariaDB.

[comment]: # ({/d4a82255-40e33d04})

[comment]: # ({new-40ea4656})

#### Possible deadlocks with MySQL/MariaDB

When running under high load, and with more than one LLD worker involved, it is possible to run into a deadlock caused by an InnoDB error related to the row-locking strategy (see [upstream bug](https://github.com/mysql/mysql-server/commit/7037a0bdc83196755a3bf3e935cfb3c0127715d5)). The error has been fixed in MySQL since 8.0.29, but not in MariaDB. For more details, see [ZBX-21506](https://support.zabbix.com/browse/ZBX-21506).

[comment]: # ({/new-40ea4656})

[comment]: # ({cc0defc5-70c19e71})
#### Глобальная корреляция событий

События могут не коррелироваться должным образом, если промежуток времени между первым и вторым событием очень мал, такой как полсекунды или менее.

[comment]: # ({/cc0defc5-70c19e71})

[comment]: # ({46e18c2d-215c95a7})
#### Диапазон типа данных числовой (с дробной точкой) в PostgreSQL 11 и более старых версиях

PostgreSQL 11 и более старые версии поддерживают значения с дробной точкой только в диапазоне приблизительно от -1.34E-154 до 1.34E+154.

[comment]: # ({/46e18c2d-215c95a7})

[comment]: # ({7ce8aecb-dfc40df7})
#### NetBSD 8.0 и более новые

Различные процессы Zabbix могут случайным образом аварийно заверщать работу при запуске на NetBSD версиях 8.X и 9.X. Такое поведение связано со слишком малым размером стека по умолчанию (4МБ), который необходимо увеличить, выполнив:

    ulimit -s 10240

Для получения более подробной информации, пожалуйста, обратитесь к соответствующему отчету о проблеме: [ZBX-18275](https://support.zabbix.com/browse/ZBX-18275).

[comment]: # ({/7ce8aecb-dfc40df7})

[comment]: # ({0fb9be31-3cf04fe3})
#### IPMI проверки

IPMI проверки не будут работать со стандартным пакетом библиотеки OpenIPMI на Debian до 9 (stretch) версии и Ubuntu до 16.04 (xenial). Чтобы исправить проблему, пересоберите OpenIPMI библиотеку с включенным OpenSSL, как обсуждалось в [ZBX-6139](https://support.zabbix.com/browse/ZBX-6139).

[comment]: # ({/0fb9be31-3cf04fe3})

[comment]: # ({d21c7a87-8c5cdd23})
#### SSH проверки

Некоторые Linux дистрибутивы такие как Debian, Ubuntu не поддерживают шифрованные приватные ключи (с ключевой фразой) при установленной libssh2 библиотеке из пакетов. Пожалуйста обратитесь к [ZBX-4850](https://support.zabbix.com/browse/ZBX-4850) для получения более подробных сведений.

При использовании libssh 0.9.x на CentOS 8 с OpenSSH 8 SSH проверки могут иногда сообщать "Cannot read data from SSH server". Это связано с [проблемой](https://gitlab.com/libssh/libssh-mirror/-/merge_requests/101) в libssh ([смотрите более подробный отчет](https://bugs.libssh.org/T231)). Ожидается, что эта ошибка исправлена в стабильном выпуске libssh 0.9.5. Смотрите также [ZBX-17756](https://support.zabbix.com/browse/ZBX-17756) для получения более подробных сведений.

[comment]: # ({/d21c7a87-8c5cdd23})

[comment]: # ({d5dff1d3-0c2fc2b9})
#### ODBC проверки

-   Драйвер MySQL unixODBC лучше не использовать с Zabbix сервером или Zabbix прокси, скомпилированными с библиотекой MariaDB и наоборот, по возможности лучше избегать использование того же коннектора, что и драйвер, по причине [зарегистрированной проблемы](https://bugs.mysql.com/bug.php?id=73709). Предлагаемая установка:

```{=html}
<!-- -->
```
    PostgreSQL, SQLite или Oracle коннекторы → MariaDB или MySQL unixODBC драйвер MariaDB коннектор → MariaDB unixODBC драйвер MySQL коннектор → MySQL unixODBC драйвер

Пожалуйста, обратитесь к [ZBX-7665](https://support.zabbix.com/browse/ZBX-7665) для получения более подробных сведений и вариантов обходных решений.

-   Данные XML запрошенные с Microsoft SQL Server могут обрезаться различными способами на Linux и UNIX системах.

```{=html}
<!-- -->
```
-   Было замечено, что использование ODBC проверок на CentOS 8 для мониторинга Oracle баз данных с использованием Oracle Instant Client for Linux 11.2.0.4.0 приводит к аварийной остановке Zabbix сервера. Эту проблему можно исправить обновив Oracle Instant Client до 12.1.0.2.0, 12.2.0.1.0, 18.5.0.0.0 или 19. Смотрите также [ZBX-18402](https://support.zabbix.com/browse/ZBX-18402).

[comment]: # ({/d5dff1d3-0c2fc2b9})

[comment]: # ({1a54c2e2-1db730d3})
#### Некорректный параметр метода запроса в элементах данных

Параметр метода запроса, используемый только в HTTP проверках, может быть некорректным образом задан значением '1', значением не по умолчанию для всех элементов данных в результате обновления с версии pre-4.0 Zabbix. Для получения более подробных сведений по устранению данной ситуации смотрите [ZBX-19308](https://support.zabbix.com/browse/ZBX-19308).

[comment]: # ({/1a54c2e2-1db730d3})


[comment]: # ({4eecea44-4713dff4})
#### Веб-мониторинг и HTTP агент

Zabbix сервер имеет утечку памяти на CentOS 6, CentOS 7 и, возможно, на других похожих дистрибутивах Linux по причине [ошибки в библиотеке](https://bugzilla.redhat.com/show_bug.cgi?id=1057388) при включенной опции "Проверка SSL узла" в веб-сценариях или HTTP агенте. Пожалуйста, обратитесь к [ZBX-10486](https://support.zabbix.com/browse/ZBX-10486) для получения более подробной информации и возможных вариантов решения.

[comment]: # ({/4eecea44-4713dff4})

[comment]: # ({c4b517ae-9cd7efe1})
#### Простые проверки

Имеется проблема в **fping** утилите в версиях до v3.10, которая приводит в дубликатам ответных echo пакетов. Такое поведение может вызывать неожиданные результаты в icmpping, icmppingloss, icmppingsec элементах данных. Рекомендуется использовать последнюю версию **fping**. Пожалуйста, обратитесь к [ZBX-11726](https://support.zabbix.com/browse/ZBX-11726) для получения более подробной информации.

[comment]: # ({/c4b517ae-9cd7efe1})

[comment]: # ({f1c8d459-a4574c73})
#### SNMP проверки

Если используется операционная система OpenBSD, проблема использования памяти после освобождения памяти в Net-SNMP библиотеке вплоть до 5.7.3 версии может привести к остановке Zabbix сервера, если SourceIP параметр указан в файле конфигурации Zabbix сервера. Как вариант решения, пожалуйста, не задавайте SourceIP параметр. Эта проблема также применима и к Linux, но она не приводит к остановке работы Zabbix сервера. К пакету net-snmp применен локальный патч на OpenBSD и будет выпущен с версией OpenBSD 6.3.

[comment]: # ({/f1c8d459-a4574c73})


















[comment]: # ({661c1a6c-d699f9d6})
#### Резкие скачки в SNMP данных

Наблюдались резкие скачки в SNMP данных, которые могут быть связаны с определенными физическими факторами, такими как скачки напряжения в сети. Смотрите [ZBX-14318](https://support.zabbix.com/browse/ZBX-14318) для получения более подробной информации.

[comment]: # ({/661c1a6c-d699f9d6})

[comment]: # ({34ab2f81-7aeb682d})
#### SNMP трапы

Пакет "net-snmp-perl", требуемый для SNMP трапов, удален в RHEL/CentOS 8.0-8.2; добавлен повторно в RHEL 8.3.

Таким образом, если вы используете RHEL 8.0-8.2, лучшим решением будет обновление до RHEL 8.3; если вы используете CentOS 8.0-8.2, вы можете подождать CentOS 8.3 или использовать пакет с EPEL.

Пожалуйста, также просмотрите [ZBX-17192](https://support.zabbix.com/browse/ZBX-17192) для получения более подробной информации.

[comment]: # ({/34ab2f81-7aeb682d})

[comment]: # ({359359c9-f46cb486})
#### Сбой в процессе alerter в CentOS/RHEL 7

В CentOS/RHEL 7 обнаружены случаи сбоя в процессах alerter Zabbix сервера. Пожалуйста, обратитесь к [ZBX-10461](https://support.zabbix.com/browse/ZBX-10461) для получения более подробной информации.

[comment]: # ({/359359c9-f46cb486})

[comment]: # ({b0d710fb-8ffad918})
#### Компиляция Zabbix агента на HP-UX

Если вы устанавливаете библиотеку PCRE с популярного сайта HP-UX пакетов <http://hpux.connect.org.uk>, например из файла `pcre-8.42-ia64_64-11.31.depot`, вы получите только 64-битную версию библиотеки, которая будет установлена в /usr/local/lib/hpux64 директорию.

В этом случае, для успешной компиляции агента необходимо использовать специальные опции для "configure" скрипта, например:

    CFLAGS="+DD64" ./configure --enable-agent --with-libpcre-include=/usr/local/include --with-libpcre-lib=/usr/local/lib/hpux64

[comment]: # ({/b0d710fb-8ffad918})

[comment]: # ({cb993ca8-6e1fb8fe})
#### Переключение локалей в веб-интерфейсе

Было замечено, что локали в веб-интерфейсе могут переключаться без какой-либо явной логики, то есть некоторые страницы (или части страниц) отображаются на одном языке, тогда как другие страницы (или части страниц), на другом языке. Обычно такая проблема может возникнуть, когда имеется несколько пользователей, некоторые из которых используют одну локаль, тогда как другие используют другую локаль.

Известный вариант решения такой проблемы - отключение многопоточности в PHP и Apache.

Эта проблема связана с тем как локали работают [в PHP](https://www.php.net/manual/en/function.setlocale): информация о локали сохраняется по каждому процессу, не по потоку. Таким образом в многопоточной среде, где несколько проектов выполняются тем одним и тем же процессом Apache, имеется вероятность, что локаль меняется в другом потоке, что меняет способ обработки данных в потоке Zabbix.

Для получения более подробной информации смотрите соответствующие отчеты о проблеме:

-   [ZBX-10911](https://support.zabbix.com/browse/ZBX-10911) (Проблема с переключением локалей в веб-интерфейсе)
-   [ZBX-16297](https://support.zabbix.com/browse/ZBX-16297) (Проблема с обработкой чисел в графиках с использованием `bcdiv` функции из BC Math функций)

[comment]: # ({/cb993ca8-6e1fb8fe})

[comment]: # ({e63994b2-81fe18ae})
#### Конфигурация opcache в PHP 7.3

Если "opcache" включен в конфигурации PHP 7.3, Zabbix веб-интерфейс может отображать пустой экран при первой загрузке. Это известная [проблема PHP](https://bugs.php.net/bug.php?id=78015). Как вариант решения этой проблемы, пожалуйста, задайте "opcache.optimization\_level" параметр значением 0x7FFFBFDF в конфигурации PHP (php.ini файл).

[comment]: # ({/e63994b2-81fe18ae})

[comment]: # ({f9fb7764-4f3b73ce})
#### Графики

Переход на Летнее время (DST) приводит к нарушению отображения подписей к оси X (дублирование данных, отсутствие данных и т.п.).

[comment]: # ({/f9fb7764-4f3b73ce})

[comment]: # ({b5c5eda2-357fdb5b})
#### Мониторинг файлов журналов

Элементы данных `log[]` и `logrt[]` многократно перечитывают файл журнала с самого начала, если файловая система заполнена на 100% и файл журнала продолжает заполняться (смотрите [ZBX-10884](https://support.zabbix.com/browse/ZBX-10884) для получения более подробной информации).

[comment]: # ({/b5c5eda2-357fdb5b})

[comment]: # ({new-82ff58c2})
#### Slow MySQL queries

Zabbix server generates slow select queries in case of non-existing
values for items. This is caused by a known
[issue](https://bugs.mysql.com/bug.php?id=74602) in MySQL 5.6/5.7
versions. A workaround to this is disabling the
index\_condition\_pushdown optimizer in MySQL. For an extended
discussion, see
[ZBX-10652](https://support.zabbix.com/browse/ZBX-10652).

[comment]: # ({/new-82ff58c2})


[comment]: # ({new-7ed39efb})
#### Slow configuration sync with Oracle

Configuration sync might be slow in Zabbix 6.0 installations with Oracle DB that have high number of items and item preprocessing steps.
This is caused by the Oracle database engine speed processing *nclob* type fields.

To improve performance, you can convert the field types from *nclob* to *nvarchar2* by manually applying the database patch [items_nvarchar_prepare.sql](/../assets/en/manual/installation/items_nvarchar_prepare.sql).
Note that this conversion will reduce the maximum field size limit from 65535 bytes to 4000 bytes
for item preprocessing parameters and item parameters such as *Description*, Script item's field *Script*,
HTTP agent item's fields *Request body* and *Headers*, Database monitor item's field *SQL query*.
Queries to determine template names that need to be deleted before applying the patch are provided in the patch as a comment. Alternatively, if MAX_STRING_SIZE is set you can change *nvarchar2(4000)* to *nvarchar2(32767)* in the patch queries to set the 32767 bytes field size limit.

For an extended discussion, see [ZBX-22363](https://support.zabbix.com/browse/ZBX-22363).

[comment]: # ({/new-7ed39efb})

[comment]: # ({0a09ea76-b393528e})
#### API login

У пользователя может быть создано большое количество открытых сессий при использовании пользовательских скриптов с [методом](/manual/api/reference/user/login) `user.login` без последующего использования `user.logout`.

[comment]: # ({/0a09ea76-b393528e})

[comment]: # ({f15d20b1-17c4463f})
#### Проблема с IPv6 адресами в SNMPv3 трапах

По причине проблемы в net-snmp, в SNMP трапах IPv6 адреса могут некорректно отображаться при использовании SNMPv3. Для получения более подробной информации и возможных вариантах решения смотрите [ZBX-14541](https://support.zabbix.com/browse/ZBX-14541).

[comment]: # ({/f15d20b1-17c4463f})

[comment]: # ({32d2208d-d77627ce})
#### Обрезка длинных IPv6 IP адресов в информации о неуспешном входе в систему

Сообщение об ошибочном входе в систему изменено, чтобы отображались только первые 39 символов сохранённого IP адреса, так как это ограничение длины поля базы данных. Что означает, что IPv6 IP адреса длиннее 39 символов будут отображаться неполностью.

[comment]: # ({/32d2208d-d77627ce})

[comment]: # ({a413a6e0-57420738})
#### Проверки Zabbix агент на Windows

Несуществующие записи DNS в параметре `Server` в файле конфигурации (zabbix\_agentd.conf) Zabbix агента могут повысить время ответа Zabbix агента на Windows. Такое случается по причине того, что демон Windows DNS кэширования не кэширует отрицательные ответы по IPv4 адресам. Однако, для IPv6 адресов отрицательные ответы кэшируются, таким образом возможным вариантом решения будет отключение IPv4 на хосте.

[comment]: # ({/a413a6e0-57420738})

[comment]: # ({371f33ab-40001075})
#### Экспорт / импорт YAML

Имеются несколько известных проблем, связанных с [экспортом / импортом](/manual/xml_export_import) YAML:

-   Сообщения об ошибках не поддаются переводу;
-   Валидный JSON с расширением файла .yaml иногда не удается импортировать;
-   Не заключенные в кавычки привычные человеку даты автоматически конвертируются в штампы времени в формате Unix.

[comment]: # ({/371f33ab-40001075})

[comment]: # ({e36886b2-fcbf4bce})
#### Мастер установки в SUSE с NGINX и php-fpm

Мастер установки веб-интерфейса не в состоянии сохранить файл конфигурации в SUSE с NGINX + php-fpm. Эта проблема вызвана настройкой в /usr/lib/systemd/system/php-fpm.service юните, который не позволяет Zabbix выполнять запись в /etc. (введено в [PHP 7.4](https://bugs.php.net/bug.php?id=72510)).

Доступно два варианта решения:

-   Задать [ProtectSystem](https://www.freedesktop.org/software/systemd/man/systemd.exec.html#ProtectSystem=) опции значение 'true' вместо 'full' в php-fpm юните systemd.
-   Вручную сохранить /etc/zabbix/web/zabbix.conf.php файл.

[comment]: # ({/e36886b2-fcbf4bce})

[comment]: # ({59e267ea-8009b04b})
#### Chromium для веб-сервиса Zabbix в Ubuntu 20

Хотя в большинстве случаев веб-сервис Zabbix может работать с Chromium, в Ubuntu 20.04 использование Chromium приводит к следующей ошибке:

    Cannot fetch data: chrome failed to start:cmd_run.go:994:
    WARNING: cannot create user data directory: cannot create 
    "/var/lib/zabbix/snap/chromium/1564": mkdir /var/lib/zabbix: permission denied
    Sorry, home directories outside of /home are not currently supported. See https://forum.snapcraft.io/t/11209 for details.

Эта ошибка возникает из-за того, что `/var/lib/zabbix` используется домашней директорией пользователя 'zabbix'.

[comment]: # ({/59e267ea-8009b04b})

[comment]: # ({407f44c2-1f99c5d8})
#### Специальные коды ошибок MySQL

Если Zabbix используется с MySQL инсталляцией в Azure, в журналах Zabbix может появиться расплывчатое сообщение об ошибке *\[9002\] Some errors occurred*. Этот общий текст ошибки отправляется Zabbix серверу или прокси со стороны базы данных. Для получения более подробных сведений о причине возникновения такой ошибки обратитесь к журналам Azure.

[comment]: # ({/407f44c2-1f99c5d8})

[comment]: # ({new-eb422070})
#### Invalid regular expressions after switching to PCRE2 
In Zabbix 6.0 support for PCRE2 has been added. Even though PCRE is still supported, all Zabbix installation packages have been updated to use PCRE2. While providing many benefits, switching to PCRE2 may cause certain existing PCRE regexp patterns becoming invalid or behaving differently. In particular, this affects the pattern *^[\w-\.]*. In order to make this regexp valid again without affecting semantics, change the expression to *^[-\w\.]* . This happens due to the fact that PCRE2 treats the dash sign as a delimiter, creating a range inside a character class.

[comment]: # ({/new-eb422070})

[comment]: # ({new-093b78e2})
#### Geomap widget error 

The maps in the Geomap widget may not load correctly, if you have upgraded from an older Zabbix version with NGINX and didn't switch to the new NGINX configuration file during the upgrade. 

To fix the issue, you can  discard the old configuration file, use the configuration file from the current version package and reconfigure it as described in the [download instructions](https://www.zabbix.com/download?zabbix=6.0&os_distribution=red_hat_enterprise_linux&os_version=8&db=mysql&ws=nginx) in section *e. Configure PHP for Zabbix frontend*.

Alternatively, you can manually edit an existing NGINX configuration file (typically, */etc/zabbix/nginx.conf*). To do so, open the file and locate the following block: 

    location ~ /(api\/|conf[^\.]|include|locale|vendor) {
            deny            all;
            return          404;
    }

Then, replace this block with: 

    location ~ /(api\/|conf[^\.]|include|locale) {
            deny            all;
            return          404;
    }

    location /vendor {
            deny            all;
            return          404;
    }


[comment]: # ({/new-093b78e2})
