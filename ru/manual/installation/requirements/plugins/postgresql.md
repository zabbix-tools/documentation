[comment]: # translation:outdated

[comment]: # ({new-6e4f67a7})
# 1 PostgreSQL plugin dependencies

[comment]: # ({/new-6e4f67a7})

[comment]: # ({new-26ad07b3})
#### Overview

The required libraries for the PostgreSQL loadable plugin are listed in this page.

[comment]: # ({/new-26ad07b3})

[comment]: # ({new-c874a67d})

#### Golang libraries

|Requirement|Mandatory status|Minimum version|Description|
|--|-|-|-------|
|[git.zabbix.com/ap/plugin-support](https://git.zabbix.com/projects/AP/repos/plugin-support)|Yes|1.X.X|Zabbix own support library. Mostly for plugins. |
|[github.com/jackc/pgx/v4](https://github.com/jackc/pgx)|^|4.17.2|PostgreSQL driver. |
|[github.com/omeid/go-yarn](https://github.com/omeid/go-yarn)|^|0.0.1|Embeddable filesystem mapped key-string store. |
|[github.com/jackc/chunkreader/v2](https://github.com/jackc/chunkreader/v2)|Indirect^**1**^|2.0.1| |
|[github.com/jackc/pgconn](https://github.com/jackc/pgconn)|^|1.13.0| |
|[github.com/jackc/pgio](https://github.com/jackc/pgio)|^|1.0.0| |
|[github.com/jackc/pgpassfile](https://github.com/jackc/pgpassfile)|^|1.0.0| |
|[github.com/jackc/pgproto3/v2](https://github.com/jackc/pgproto3/v2)|^|2.3.1| |
|[github.com/jackc/pgservicefile](https://github.com/jackc/pgservicefile)|^|0.0.0| |
|[github.com/jackc/pgtype](https://github.com/jackc/pgtype)|^|1.12.0| |
|[github.com/jackc/puddle](https://github.com/jackc/puddle)|^|1.3.0| |
|[github.com/natefinch/npipe](https://github.com/natefinch/npipe)|^|0.0.0| |
|[golang.org/x/crypto](https://golang.org/x/crypto)|^|0.0.0| |
|[golang.org/x/sys](https://golang.org/x/sys)|^|0.0.0| |
|[golang.org/x/text](https://golang.org/x/text)|^|0.3.7| |

^**1**^ "Indirect" means that it is used in one of the libraries that the agent uses. It's required since Zabbix uses the library that uses the package.

[comment]: # ({/new-c874a67d})
