[comment]: # translation:outdated

[comment]: # ({6f4fd57d-7ef4b2b9})
# Наилучшие практики для безопасной установки Zabbix

[comment]: # ({/6f4fd57d-7ef4b2b9})

[comment]: # ({fc706a13-b9719f19})
#### Обзор

В этом разделе содержатся рекомендации, соблюдение которых, позволит настроить Zabbix безопасным образом.

Практики, описанные здесь, не требуются для работы Zabbix. Они рекомендуются для повышения безопасности системы.


[comment]: # ({/fc706a13-b9719f19})

[comment]: # ({new-cebf8306})
### Access control

[comment]: # ({/new-cebf8306})

[comment]: # ({1a8dc40d-dc572a02})
#### Принцип наименьших привилегий

Принцип наименьших привилегий всегда должен всегда быть использован для Zabbix. Принцип подразумевает, что учетные записи пользователей (в веб-интерфейсе Zabbix) или пользователь процесса (Zabbix сервер/прокси или агент) имеют привилегии необходимые только для выполнения предусмотренных функций. Другими словами, учётные записи пользователей в любое время должны иметь минимально возможное количество необходимых привилегий.

::: noteimportant
Предоставление дополнительных разрешений пользователю 'zabbix', откроет ему доступ до файлов конфигурации и выполнения операций, которые могут скомпрометировать общую безопасность всей инфраструктуры.
:::

При введении принципа наименьших привилегий, необходимо учитывать [типы пользователей веб-интерфейса](/manual/config/users_and_usergroups/permissions). Важно понимать, что в том время как у пользователя с типом "Администратор" меньше привилегий, чем у пользователя с типом "Супер-Администраторов", у него так же есть доступ к административным функциям позволяющим управлять конфигурацией и выполнять пользовательские скрипты.

::: noteclassic
Некоторая информация доступна даже не привилегированным пользователям. Например, в то время как  *Администрирование* → *Скрипты* не доступны для  пользователей с типом отличным от Супер-Администраторов, сами скрипты доступны для получения используя Zabbix API. Должно применяться ограничение доступа к скриптам и не использование в них конфиденциальной информации(например данных доступа, и т.д.) чтобы избежать раскрытия конфиденциальной информации доступной в глобальных скриптах.
:::

[comment]: # ({/1a8dc40d-dc572a02})

[comment]: # ({5fa12089-1631be73})
#### Защищённый пользователь для Zabbix агента

В конфигурации по умолчанию процессы Zabbix сервера и Zabbix агента делят одного 'zabbix' пользователя. Если вы хотите убедиться, что агент не сможет получить доступ к конфиденциальной информации из конфигурации сервера (например, данные подключения к базе данных), агента необходимо запускать из под другого пользователя:

1.  Создайте защищённого пользователя
2.  Укажите этого пользователя в [файле конфигурации](/manual/appendix/config/zabbix_agentd) агента (параметр 'User')
3.  Перезапустите агента с правами администратора. Привилегии администратора сбросятся на указанного пользователя.

[comment]: # ({/5fa12089-1631be73})

[comment]: # ({21c1f94b-40e70f9e})
#### Кодировка UTF-8

UTF-8 является единственной кодировкой, которая поддерживается Zabbix. Она, как известно, работает без каких-либо проблем с безопасностью. Пользователи должны знать, что существуют известные проблемы с безопасностью при использовании некоторых других кодировок.

[comment]: # ({/21c1f94b-40e70f9e})

[comment]: # ({new-62eeb2ec})
### Windows installer paths
When using Windows installers, it is recommended to use default paths provided by the installer as using custom paths without proper permissions could compromise the security of the installation.

[comment]: # ({/new-62eeb2ec})

[comment]: # ({da55fae2-f8f5054e})
#### Настройка SSL для веб-интерфейса Zabbix

На RHEL/Centos, установите пакет mod\_ssl:

    yum install mod_ssl

Создайте директорию для SSL ключей:

    mkdir -p /etc/httpd/ssl/private
    chmod 700 /etc/httpd/ssl/private

Создайте SSL сертификат:

    openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/httpd/ssl/private/apache-selfsigned.key -out /etc/httpd/ssl/apache-selfsigned.crt

Заполните запросы соответствующим образом. Самая важная строка здесь та, которая запрашивает Common Name. Вам необходимо указать имя домена, которое вы хотите связать с вашим сервером. Вместо него вы можете указать публичный IP адрес, если у вас отсутствует имя домена. В этой статье мы будем использовать *example.com*.

    Country Name (2 letter code) [XX]:
    State or Province Name (full name) []:
    Locality Name (eg, city) [Default City]:
    Organization Name (eg, company) [Default Company Ltd]:
    Organizational Unit Name (eg, section) []:
    Common Name (eg, your name or your server's hostname) []:example.com
    Email Address []:

Отредактируйте конфигурацию Apache SSL:

    /etc/httpd/conf.d/ssl.conf

    DocumentRoot "/usr/share/zabbix"
    ServerName example.com:443
    SSLCertificateFile /etc/httpd/ssl/apache-selfsigned.crt
    SSLCertificateKeyFile /etc/httpd/ssl/private/apache-selfsigned.key

Перезапустите сервис Apache, чтобы применить изменения:

    systemctl restart httpd.service

[comment]: # ({/da55fae2-f8f5054e})

[comment]: # ({new-40cfdec2})
### Web server hardening

[comment]: # ({/new-40cfdec2})

[comment]: # ({0b2f162f-bb3706f4})
#### Включение Zabbix в корневом каталоге URL

Добавьте виртуальный хост в конфигурацию Apache и настройте постоянную переадресацию для корневого каналога на Zabbix SSL URL. Не забудьте заменить *example.com* на актуальное имя сервера.

    /etc/httpd/conf/httpd.conf

    #Добавьте строки

    <VirtualHost *:*>
        ServerName example.com
        Redirect permanent / https://example.com
    </VirtualHost>

Перезапустите сервис Apache, чтобы применить изменения:

    systemctl restart httpd.service

[comment]: # ({/0b2f162f-bb3706f4})

[comment]: # ({ff68fa12-c7ee0bb2})
#### Включение строгой транспортной безопасности HTTP (HSTS) на веб-сервере

Чтобы защитить веб-интерфейс Zabbix от атак понижающих версию протокола, мы рекомендуем включить [HSTS](https://ru.wikipedia.org/wiki/HSTS) политику на веб-сервере.

Например, чтобы включить HSTS политики в конфигурации Apache для веб-интерфейса вашего Zabbix:

    /etc/httpd/conf/httpd.conf

добавьте следующую директиву к конфигурации вашего виртуального хоста:

    <VirtualHost *:443>
       Header set Strict-Transport-Security "max-age=31536000"
    </VirtualHost>

Перезапустите сервис Apache, чтобы применить изменения:

    systemctl restart httpd.service

[comment]: # ({/ff68fa12-c7ee0bb2})







[comment]: # ({b38fd14c-cd09dcd1})
#### Отключение отображения информации о веб-сервере

Рекомендуется отключить все подписи веб-сервера, как часть процесса по улучшению защищенности веб-сервера. По умолчанию веб-сервер раскрывает подпись программного обеспечения:

![](../../../../assets/en/manual/installation/requirements/software_signature.png)

Эту подпись можно отключить, добавив две строки в файл конфигурации Apache (используется как пример):

    ServerSignature Off
    ServerTokens Prod

Подпись PHP (Заголовок X-Powered-By HTTP) можно отключить, изменив файл конфигурации php.ini (подпись отключена по умолчанию):

    expose_php = Off

Чтобы изменения файлов конфигурации вступили в силу, необходимо перезапустить веб-сервер.

Дополнительный уровень безопасности можно достичь, используя  mod\_security (пакет libapache2-mod-security2) с Apache. mod\_security позволяет полностью удалить подпись сервера вместо удаления лишь версии из подписи сервера. После установки mod\_security, подпись можно изменить на любое значение, исправив "SecServerSignature" на любое желаемое значение..

Пожалуйста, обратитесь к документации по вашему веб-серверу для того, чтобы узнать как удалять/изменять подписи к программному обеспечению.

[comment]: # ({/b38fd14c-cd09dcd1})

[comment]: # ({617408eb-720052da})
#### Отключение страниц ошибок веб-сервера по умолчанию

Рекомендуется отключить страницы ошибок по умолчанию, чтобы избежать раскрытия информации. По умолчанию веб-сервер использует встроенные страницы ошибок:

![](../../../../assets/en/manual/installation/requirements/error_page_text.png)

Страницы ошибок по умолчанию необходимо заменить/удалить, как часть процесса по улучшению защищенности веб-сервера. Можно использовать директиву "ErrorDocument", чтобы задать пользовательскую страницу/текст для веб-сервера Apache (используется как пример).

Пожалуйста, обратитесь к документации по вашему веб-серверу для того, чтобы узнать как заменять/удалять страницы ошибок по умолчанию.

[comment]: # ({/617408eb-720052da})

[comment]: # ({fd427001-ba1547c0})
#### Удаление тестовой страницы веб-сервера

Рекомендуется удалить тестовую страницу веб-сервера, чтобы избежать раскрытия информации. По умолчанию, корневой каталог веб-сервера содержит тестовую страницу с именем index.html (Apache2 на Ubuntu используется как пример):

![](../../../../assets/en/manual/installation/requirements/test_page.png)

Тестовую страницу необходимо удалить или сделать недоступной, как часть процесса по улучшению защищенности веб-сервера.

[comment]: # ({/fd427001-ba1547c0})

[comment]: # ({18806072-7e4e4a45})
#### Настройки Zabbix

По умолчанию, в конфигурации Zabbix *HTTP заголовок ответа X-Frame-Options* настроен на `SAMEORIGIN`, это означает, что содержимое может быть загружено только во фрейм с таким же источником, как и у самой страницы.

Элементы веб-интерфейса Zabbix которые извлекают содержимое из внешних URL-адресов (например, [виджет панели](/manual/web_interface/frontend_sections/monitoring/dashboard/widgets#url) URL) отображают полученное содержимое в так называемой песочнице(sandbox), при этом все ограничения песочницы включены.

Эти настройки улучшают безопасность веб-интерфейса Zabbix и обеспечивают защиту от XSS и кликджекинг атак. Супер Администраторы могут [изменить](/manual/web_interface/frontend_sections/administration/general#безопасность) параметры *Использовать sandbox атрибут в iframe* and *HTTP заголовок X-Frame-Options* по мере необходимости. Пожалуйста, тщательно взвесьте риски и преимущества перед изменением настроек по умолчанию. Отключать полностью песочницу или X-Frame-Options не рекомендуется.

[comment]: # ({/18806072-7e4e4a45})

[comment]: # ({1d3b1b55-c02c850f})
#### Zabbix агент на Windows с OpenSSL

Zabbix агент на Windows скомпилированный с OpenSSL попытается получить доступ к файлу конфигурации SSL
в c:\\openssl-64bit. Директория "openssl-64bit" на диске C: может быть создана непривилегированными пользователями.

Поэтому для повышения безопасности системы, необходимо создать данную директорию вручную и убрать доступ на запись для пользователей, не являющихся администраторами.

Пожалуйста, обратите внимание, что имена каталогов будут отличаться на 32-битных и 64-битных версиях Windows.

[comment]: # ({/1d3b1b55-c02c850f})

[comment]: # ({new-6cfe5a3b})
### Cryptography

[comment]: # ({/new-6cfe5a3b})

[comment]: # ({new-3f76ae70})
#### Скрытие файла со списком общих паролей

Для повышения сложности атак методом перебора пароля, рекомендуется ограничить доступ к файлу `ui/data/top_passwords.txt` при помощи изменения конфигурации веб-сервера. В данном файле находится список самых распространённых и контексто-зависимых паролей, используемый для ограничения возможности пользователей от использования подобных паролей в случае если параметр *Избегать легко угадываемых паролей* включен в [политике паролей](/manual/web_interface/frontend_sections/administration/authentication#внутренняя_аутентификация).

Например, в NGINX доступ к файлу может быть ограничен с помощью директивы `location`:

    location = /data/top_passwords.txt {​​​​​​​
        deny all;
        return 404;
    }​​​​​​​

В Apache с использованием файла `.htacess`:

    <Files "top_passwords.txt">  
      Order Allow,Deny
      Deny from all
    </Files>

​

[comment]: # ({/new-3f76ae70})

[comment]: # ({new-ba0d4036})

#### Security vulnerabilities

##### CVE-2021-42550

In Zabbix Java gateway with logback version 1.2.7 and prior versions, an attacker with the required 
privileges to edit configuration files could craft a malicious configuration allowing to execute 
arbitrary code loaded from LDAP servers.

Vulnerability to [CVE-2021-42550](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-42550) 
has been fixed since Zabbix 5.4.9. However, as an additional security measure it is recommended 
to check permissions to the `/etc/zabbix/zabbix_java_gateway_logback.xml` file and set it read-only, 
if write permissions are available for the "zabbix" user. 

[comment]: # ({/new-ba0d4036})
