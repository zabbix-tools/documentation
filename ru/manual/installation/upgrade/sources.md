[comment]: # translation:outdated

[comment]: # ({4bd17861-7900383c})
# Обновление из исходного кода

[comment]: # ({/4bd17861-7900383c})

[comment]: # ({new-92c7a8e5})
#### Обзор

Этот раздел описывает требуемые шаги для успешного [обновления](/manual/installation/upgrade) с Zabbix **5.4** до Zabbix **6.0**.x с использованием официального исходного кода Zabbix.

В то время как обновление Zabbix агентов не является обязательным шагом (но рекомендуемым), Zabbix сервер и прокси должны быть [одной мажорной версии](/manual/appendix/compatibility). Поэтому, при наличии сервер-прокси инсталляции, Zabbix сервер и все прокси должны быть остановлены и обновлены. Оставление работающих прокси при обновлении сервера более не принесет никакой пользы, так как во время обновления прокси серверов их старые данных будут удалены, а новые данные не будут собираться пока конфигурация прокси не будет синхронизирована с сервером.

::: noteimportant
Более невозможно запустить обновленный сервер и более старые, еще не обновленные прокси, которые бы отправляли данные на более новый сервер. Такой подход, который никогда не рекомендовался и не поддерживался в Zabbix, теперь официально отключен, так сервер будет игнорировать данные с не обновленных прокси.
:::

Обратите внимание, что при наличии прокси с SQLite базой данных, данные истории этих прокси будут потеряны до обновления, так как обновление базы данных SQLite не поддерживется и файл базы данных SQLite необходимо удалить вручную. Когда прокси запустится в первый раз и будет отсутствовать файл базы данных SQLite, прокси создаст его автоматически.

В зависимости от размера базы данных обновление базы данных до версии 6.0 может занять продолжительное время.

::: notewarning
Перед выполнением обновления убедитесь, что прочитали все соответствующие **заметки по обновлению**!
:::

Доступны следующие заметки по обновлению:

|Обновление с|Прочитайте заметки по обновлению полностью|Важные заметки/изменения между версиями|
|------------|-----------------------|---------------------------------------|
|5.4.x|Для:<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|<|
|5.2.x|Для:<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/ru/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|Повышены минимально требуемые версии баз данных;<br>Агрегативные элементы данных более не являются отдельным типом.|
|5.0.x|Для:<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/ru/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/ru/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|Минимально требуемая версия PHP повышена с 7.2.0 до 7.2.5.|
|4.4.x|Для:<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/ru/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/ru/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/ru/manual/installation/upgrade_notes_540)|Прекращена поддержка IBM DB2;<br>Минимально требуемая версия PHP повышена с 5.4.0 до 7.2.0;<br>Повышены минимально требуемые версии баз данных;<br>Изменилась директория с Zabbix PHP файлами.|
|4.2.x|Для:<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.4/ru/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/ru/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/ru/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/ru/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|Удалены способы оповещения Jabber, Ez Texting.|
|4.0.x LTS|Для:<br>Zabbix [4.2](https://www.zabbix.com/documentation/4.2/ru/manual/installation/upgrade_notes_420)<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.4/ru/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/ru/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/ru/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/ru/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|Более старые прокси более не могут передавать данные на обновленный сервер;<br>Более новые агенты более не могут работать с более старым Zabbix сервером.|
|3.4.x|Для:<br>Zabbix [4.0](https://www.zabbix.com/documentation/4.0/manual/installation/upgrade_notes_400)<br>Zabbix [4.2](https://www.zabbix.com/documentation/4.2/ru/manual/installation/upgrade_notes_420)<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.4/ru/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/ru/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/ru/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/ru/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|Библиотеки 'libpthread' и 'zlib' теперь обязательны;<br>Поддержка протокола в виде простого текста убрана и заголовок обязателен;<br>Zabbix агенты версий Pre-1.4 более не поддерживаются;<br>Параметр Server в конфигурации пассивного прокси теперь обязателен.|
|3.2.x|Для:<br>Zabbix [3.4](https://www.zabbix.com/documentation/3.4/ru/manual/installation/upgrade_notes_340)<br>Zabbix [4.0](https://www.zabbix.com/documentation/4.0/ru/manual/installation/upgrade_notes_400)<br>Zabbix [4.2](https://www.zabbix.com/documentation/4.2/ru/manual/installation/upgrade_notes_420)<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.4/ru/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/ru/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/ru/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/ru/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|Поддержка SQLite в виде основной базы данных убрана для Zabbix сервера/веб-интерфейса;<br>Поддерживается Perl совместимые регулярные выражения (PCRE) вместо POSIX расширенных;<br>Библиотеки 'libpcre' and 'libevent' обязательны для Zabbix сервера;<br>Добавлены проверки кода выхода для пользовательских параметров, удаленных команд и элементов данных system.run\[\] без 'nowait' флага, а также для выполняемых скриптов Zabbix сервером;<br>Zabbix Java gateway необходимо обновить для поддержки новых функций.|
|3.0.x LTS|Для:<br>Zabbix [3.2](https://www.zabbix.com/documentation/3.2/ru/manual/installation/upgrade_notes_320)<br>Zabbix [3.4](https://www.zabbix.com/documentation/3.4/ru/manual/installation/upgrade_notes_340)<br>Zabbix [4.0](https://www.zabbix.com/documentation/4.0/ru/manual/installation/upgrade_notes_400)<br>Zabbix [4.2](https://www.zabbix.com/documentation/4.2/ru/manual/installation/upgrade_notes_420)<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.4/ru/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/ru/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/ru/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/ru/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|Обновление базы данных может быть медленным, в зависимости от размеров таблиц истории.|
|2.4.x|Для:<br>Zabbix [3.0](https://www.zabbix.com/documentation/3.0/ru/manual/installation/upgrade_notes_300)<br>Zabbix [3.2](https://www.zabbix.com/documentation/3.2/ru/manual/installation/upgrade_notes_320)<br>Zabbix [3.4](https://www.zabbix.com/documentation/3.4/ru/manual/installation/upgrade_notes_340)<br>Zabbix [4.0](https://www.zabbix.com/documentation/4.0/ru/manual/installation/upgrade_notes_400)<br>Zabbix [4.2](https://www.zabbix.com/documentation/4.2/ru/manual/installation/upgrade_notes_420)<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.2/ru/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/ru/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/ru/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/ru/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|Минимально требуемая версия PHP повышена с 5.3.0 до 5.4.0<br>Необходимо указывать LogFile параметр агента|
|2.2.x LTS|Для:<br>Zabbix [2.4](https://www.zabbix.com/documentation/2.4/ru/manual/installation/upgrade_notes_240)<br>Zabbix [3.0](https://www.zabbix.com/documentation/3.0/ru/manual/installation/upgrade_notes_300)<br>Zabbix [3.2](https://www.zabbix.com/documentation/3.2/ru/manual/installation/upgrade_notes_320)<br>Zabbix [3.4](https://www.zabbix.com/documentation/3.4/ru/manual/installation/upgrade_notes_340)<br>Zabbix [4.0](https://www.zabbix.com/documentation/4.0/ru/manual/installation/upgrade_notes_400)<br>Zabbix [4.2](https://www.zabbix.com/documentation/4.2/ru/manual/installation/upgrade_notes_420)<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.2/ru/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/ru/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/ru/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/ru/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|Удалена поддержка распределенного мониторинга на основе нод|
|2.0.x|Для:<br>Zabbix [2.2](https://www.zabbix.com/documentation/2.2/ru/manual/installation/upgrade_notes_220)<br>Zabbix [2.4](https://www.zabbix.com/documentation/2.4/ru/manual/installation/upgrade_notes_240)<br>Zabbix [3.0](https://www.zabbix.com/documentation/3.0/ru/manual/installation/upgrade_notes_300)<br>Zabbix [3.2](https://www.zabbix.com/documentation/3.2/ru/manual/installation/upgrade_notes_320)<br>Zabbix [3.4](https://www.zabbix.com/documentation/3.4/ru/manual/installation/upgrade_notes_340)<br>Zabbix [4.0](https://www.zabbix.com/documentation/4.0/ru/manual/installation/upgrade_notes_400)<br>Zabbix [4.2](https://www.zabbix.com/documentation/4.2/ru/manual/installation/upgrade_notes_420)<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.2/ru/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/ru/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/ru/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/ru/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|Минимально требуемая версия PHP повышена с 5.1.6 до 5.3.0;<br>Требуется чувствительная к регистру MySQL база данных для правильной работы сервера; Для корректной работы с MySQL базой данных требуются кодировка utf8 и utf8\_bin тип сравнения требуются для Zabbix сервера. Смотрите [скрипты создания базы данных](/manual/appendix/install/db_scripts#mysql).<br>Вместо 'mysql' расширения требуется 'mysqli' расширение PHP|

Вы возможно захотите также ознакомиться с [требованиями](/manual/installation/requirements) для 6.0.

::: notetip
Возможно будет удобно запустить две параллельные SSH сессии на время обновления, выполняя шаги обновления в одной сессии и наблюдая за файлами журналов сервера/прокси в другой. Например, при выполнении `tail -f zabbix_server.log` или `tail -f zabbix_proxy.log` во второй SSH сессии будут отображаться последние записи из файла журнала и возможные ошибки в режиме реального времени. Такой подход может быть критичным на продуктивных серверах.
:::

[comment]: # ({/new-92c7a8e5})

[comment]: # ({new-101d6faf})
#### Процесс обновления сервера

[comment]: # ({/new-101d6faf})

[comment]: # ({75ed6160-b8308740})
##### 1 Остановите сервер

Остановите Zabbix сервер, чтобы быть уверенным, что в базу данных не будет происходить запись новых данных.

[comment]: # ({/75ed6160-b8308740})

[comment]: # ({60850928-ab13a6a4})
##### 2 Сделайте архивную копию существующей базы данных Zabbix

Этот шаг очень важен. Убедитесь, что у вас имеется архивная копия вашей базы данных. Копия поможет, если процедура обновления закончится неудачно (отсутствие свободного места на диске, выключение питания, любая неожиданная проблема).

[comment]: # ({/60850928-ab13a6a4})

[comment]: # ({3978d0df-d2778675})
##### 3 Резервное копирование файлов конфигурации, PHP файлов и бинарных файлов Zabbix

Выполните резервное копирование бинарных файлов Zabbix, файлов конфигурации и папки с PHP файлами.

[comment]: # ({/3978d0df-d2778675})

[comment]: # ({52ba9a17-2bb75ddd})
##### 4 Установите новые бинарные файлы сервера

Воспользуйтесь этими [инструкциями](/manual/installation/install#установка_демонов_zabbix) для компиляции Zabbix сервера из исходного кода.

[comment]: # ({/52ba9a17-2bb75ddd})

[comment]: # ({new-ec6edae7})
##### 5 Проверьте параметры конфигурации сервера

Для получения более подробной информации смотрите заметки по обновлению на предмет [обязательных изменений](/manual/installation/upgrade_notes_600#параметры_конфигурации).

Касательно новых опциональных параметров смотрите раздел [Что нового](/manual/introduction/whatsnew600#параметры_конфигурации).

[comment]: # ({/new-ec6edae7})

[comment]: # ({c3ef5a62-a01e967f})
##### 6 Запустите новые бинарные файлы Zabbix

Запустите сервер. Проверьте файлы журналов, чтобы удостовериться, что сервер запустился успешно.

Zabbix сервер после запуска автоматически обновит базу данных. При запуске Zabbix сервер сообщает текущую (обязательную и опциональную) и требуемую версии базы данных. Если текущая обязательная версия старше требуемой версии, Zabbix сервер автоматически выполнит требуемые патчи обновления базы данных. Начало и прогресс (в процентах) обновления базы данных записываются в файл журнала Zabbix сервера. Когда обновление завершится, в файл журнала запишется сообщение "database upgrade fully completed". Если какой-либо из патчей обновления будет ошибочным, Zabbix сервер не запустится. Zabbix сервер также не запустится, если текущая обязательная версия более новая чем требуемая. Zabbix сервер запустится только, если текущая обязательная версия базы данных соответствует требуемой обязательной версии.

    8673:20161117:104750.259 current database version (mandatory/optional): 03040000/03040000
    8673:20161117:104750.259 required mandatory version: 03040000

До запуска сервера:

-   Убедитесь, что пользователь базы данных имеет достаточно прав (create table, drop table, create index, drop index).
-   Убедитесь что у вас достаточно свободного дискового пространства.

[comment]: # ({/c3ef5a62-a01e967f})

[comment]: # ({ef4186c7-22e0e4e9})
##### 7 Установите новый веб-интерфейс Zabbix

Минимально требуемая версия PHP - 7.2.5. Обновите, если потребуется, и затем следуйте [инструкциям по установке](/manual/installation/frontend).

[comment]: # ({/ef4186c7-22e0e4e9})

[comment]: # ({9d2b0a24-49e4f43e})
##### 8 Очистите cookies и кэш в веб-браузере

После обновления вам, возможно, потребуется очистить cookies веб-браузера и кэш веб-браузера, чтобы веб-интерфейс Zabbix работал должным образом.

[comment]: # ({/9d2b0a24-49e4f43e})

[comment]: # ({new-75f5b3ed})
#### Процесс обновления прокси

[comment]: # ({/new-75f5b3ed})

[comment]: # ({f97223cd-f33e6ddb})
##### 1 Остановите прокси

Остановите Zabbix прокси.

[comment]: # ({/f97223cd-f33e6ddb})

[comment]: # ({2f8ce16e-2e03c550})
##### 2 Резервное копирование файлов конфигурации и бинарного файла Zabbix прокси

Выполните резервное копирование бинарного файла Zabbix прокси, файла конфигурации.

[comment]: # ({/2f8ce16e-2e03c550})

[comment]: # ({7376c2b2-024201dc})
##### 3 Установите новые бинарные файлы прокси

Воспользуйтесь этими [инструкциями](/manual/installation/install#установка_демонов_zabbix) для компиляции Zabbix прокси из исходного кода.

[comment]: # ({/7376c2b2-024201dc})

[comment]: # ({b1b5e6f0-2fbda571})
##### 4 Проверьте параметры конфигурации прокси

В этой версии обязательные изменения в [параметрах](/manual/appendix/config/zabbix_proxy) прокси отсутствуют.

[comment]: # ({/b1b5e6f0-2fbda571})

[comment]: # ({20e10575-520f43c6})
##### 6 Запустите новый Zabbix прокси

Запустите прокси. Проверьте файлы журналов, чтобы удостовериться, что прокси запустился успешно.

Zabbix прокси автоматически обновит базу данных. Обновление базы данных выполняется аналогично запуску [Zabbix сервера](/manual/installation/upgrade#запустите_новые_бинарные_файлы_zabbix).

[comment]: # ({/20e10575-520f43c6})

[comment]: # ({5d018f37-59736bba})
#### Процесс обновления агента

::: noteimportant
Обновление агентов не является обязательным. Вы можете обновить агенты только, если это требуется для доступа к новому функционалу.
:::

Процедурой обновления описанной в этом разделе можно воспользоваться для обновления как Zabbix агента, так и Zabbix агента 2.

[comment]: # ({/5d018f37-59736bba})

[comment]: # ({3b9b7078-02698e69})
##### 1 Остановите агента

Остановите Zabbix агент.

[comment]: # ({/3b9b7078-02698e69})

[comment]: # ({8f44e133-50263823})
##### 2 Резервное копирование файлов конфигурации и бинарных файлов Zabbix агента

Выполните резервное копирование бинарного файла Zabbix агента, файла конфигурации.

[comment]: # ({/8f44e133-50263823})

[comment]: # ({a9e799f2-467f56b3})
##### 3 Установите новые бинарные файлы агента

Воспользуйтесь этими [инструкциями](/manual/installation/install#установка_демонов_zabbix) для компиляции Zabbix агента из исходного кода.

В качестве альтернативы, вы можете загрузить уже скомпилированные Zabbix агенты со [страницы загрузки Zabbix](http://www.zabbix.com/ru/download.php).

[comment]: # ({/a9e799f2-467f56b3})

[comment]: # ({eee99efe-34721aad})
##### 4 Проверьте параметры конфигурации агента

В этой версии обязательные изменения в [параметрах](/manual/appendix/config/zabbix_proxy) отсутствуют как для [агента](/manual/appendix/config/zabbix_agentd), так и для [агента 2](/manual/appendix/config/zabbix_agent2).

[comment]: # ({/eee99efe-34721aad})

[comment]: # ({7ff2b3c0-04c253a7})
##### 5 Запустите новый Zabbix агент

Запустите новый Zabbix агент. Проверьте файлы журналов, чтобы удостовериться, что агент запустился успешно.

[comment]: # ({/7ff2b3c0-04c253a7})

[comment]: # ({new-a06019d2})
#### Обновление между минорными версиями

При обновлении между минорными версиями 6.0.x (например, с 6.0.1 до 6.0.3) необходимо выполнить те же самые действия для сервера / прокси / агента, как и при обновлении между мажорными версиями. Единственное различие заключается в том, что при обновлении между минорными версиями, изменения в базе данных отсутствуют.

[comment]: # ({/new-a06019d2})
