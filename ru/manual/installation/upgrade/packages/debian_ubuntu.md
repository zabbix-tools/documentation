[comment]: # translation:outdated

[comment]: # ({91aab3e5-91aab3e5})
# 2 Debian/Ubuntu

[comment]: # ({/91aab3e5-91aab3e5})

[comment]: # ({new-d02acea9})
#### Обзор

Этот раздел описывает требуемые шаги для успешного [обновления](/manual/installation/upgrade) с Zabbix **6.0** до Zabbix **6.2**.x с использованием официальных пакетов Zabbix для Debian/Ubuntu.

В то время как обновление Zabbix агентов не является обязательным шагом (но рекомендуемым), Zabbix сервер и прокси должны быть [одной мажорной версии](/manual/appendix/compatibility). Поэтому, при наличии сервер-прокси инсталляции, Zabbix сервер и все прокси должны быть остановлены и обновлены. Оставление работающих прокси при обновлении сервера более не принесет никакой пользы, так как во время обновления прокси серверов их старые данных будут удалены, а новые данные не будут собираться пока конфигурация прокси не будет синхронизирована с сервером.

Обратите внимание, что при наличии прокси с SQLite базой данных, данные истории этих прокси будут потеряны до обновления, так как обновление базы данных SQLite не поддерживется и файл базы данных SQLite необходимо удалить вручную. Когда прокси запустится в первый раз и будет отсутствовать файл базы данных SQLite, прокси создаст его автоматически.

В зависимости от размера базы данных обновление базы данных до версии 6.2 может занять продолжительное время.

::: notewarning
Перед выполнением обновления убедитесь, что прочитали все соответствующие **заметки по обновлению**!
:::

Доступны следующие заметки по обновлению:

|Обновление с|Прочитайте заметки по обновлению полностью|Важные заметки/изменения между версиями|
|------------|-----------------------|---------------------------------------|
|5.4.x|Для:<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|<|
|5.2.x|Для:<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/ru/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|Повышены минимально требуемые версии баз данных;<br>Агрегативные элементы данных более не являются отдельным типом.|
|5.0.x|Для:<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/ru/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/ru/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|Минимально требуемая версия PHP повышена с 7.2.0 до 7.2.5.|
|4.4.x|Для:<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/ru/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/ru/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/ru/manual/installation/upgrade_notes_540)|Прекращена поддержка IBM DB2;<br>Минимально требуемая версия PHP повышена с 5.4.0 до 7.2.0;<br>Повышены минимально требуемые версии баз данных;<br>Изменилась директория с Zabbix PHP файлами.|
|4.2.x|Для:<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.4/ru/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/ru/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/ru/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/ru/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|Удалены способы оповещения Jabber, Ez Texting.|
|4.0.x LTS|Для:<br>Zabbix [4.2](https://www.zabbix.com/documentation/4.2/ru/manual/installation/upgrade_notes_420)<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.4/ru/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/ru/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/ru/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/ru/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|Более старые прокси более не могут передавать данные на обновленный сервер;<br>Более новые агенты более не могут работать с более старым Zabbix сервером.|
|3.4.x|Для:<br>Zabbix [4.0](https://www.zabbix.com/documentation/4.0/manual/installation/upgrade_notes_400)<br>Zabbix [4.2](https://www.zabbix.com/documentation/4.2/ru/manual/installation/upgrade_notes_420)<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.4/ru/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/ru/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/ru/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/ru/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|Библиотеки 'libpthread' и 'zlib' теперь обязательны;<br>Поддержка протокола в виде простого текста убрана и заголовок обязателен;<br>Zabbix агенты версий Pre-1.4 более не поддерживаются;<br>Параметр Server в конфигурации пассивного прокси теперь обязателен.|
|3.2.x|Для:<br>Zabbix [3.4](https://www.zabbix.com/documentation/3.4/ru/manual/installation/upgrade_notes_340)<br>Zabbix [4.0](https://www.zabbix.com/documentation/4.0/ru/manual/installation/upgrade_notes_400)<br>Zabbix [4.2](https://www.zabbix.com/documentation/4.2/ru/manual/installation/upgrade_notes_420)<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.4/ru/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/ru/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/ru/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/ru/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|Поддержка SQLite в виде основной базы данных убрана для Zabbix сервера/веб-интерфейса;<br>Поддерживается Perl совместимые регулярные выражения (PCRE) вместо POSIX расширенных;<br>Библиотеки 'libpcre' and 'libevent' обязательны для Zabbix сервера;<br>Добавлены проверки кода выхода для пользовательских параметров, удаленных команд и элементов данных system.run\[\] без 'nowait' флага, а также для выполняемых скриптов Zabbix сервером;<br>Zabbix Java gateway необходимо обновить для поддержки новых функций.|
|3.0.x LTS|Для:<br>Zabbix [3.2](https://www.zabbix.com/documentation/3.2/ru/manual/installation/upgrade_notes_320)<br>Zabbix [3.4](https://www.zabbix.com/documentation/3.4/ru/manual/installation/upgrade_notes_340)<br>Zabbix [4.0](https://www.zabbix.com/documentation/4.0/ru/manual/installation/upgrade_notes_400)<br>Zabbix [4.2](https://www.zabbix.com/documentation/4.2/ru/manual/installation/upgrade_notes_420)<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.4/ru/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/ru/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/ru/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/ru/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|Обновление базы данных может быть медленным, в зависимости от размеров таблиц истории.|
|2.4.x|Для:<br>Zabbix [3.0](https://www.zabbix.com/documentation/3.0/ru/manual/installation/upgrade_notes_300)<br>Zabbix [3.2](https://www.zabbix.com/documentation/3.2/ru/manual/installation/upgrade_notes_320)<br>Zabbix [3.4](https://www.zabbix.com/documentation/3.4/ru/manual/installation/upgrade_notes_340)<br>Zabbix [4.0](https://www.zabbix.com/documentation/4.0/ru/manual/installation/upgrade_notes_400)<br>Zabbix [4.2](https://www.zabbix.com/documentation/4.2/ru/manual/installation/upgrade_notes_420)<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.2/ru/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/ru/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/ru/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/ru/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|Минимально требуемая версия PHP повышена с 5.3.0 до 5.4.0<br>Необходимо указывать LogFile параметр агента|
|2.2.x LTS|Для:<br>Zabbix [2.4](https://www.zabbix.com/documentation/2.4/ru/manual/installation/upgrade_notes_240)<br>Zabbix [3.0](https://www.zabbix.com/documentation/3.0/ru/manual/installation/upgrade_notes_300)<br>Zabbix [3.2](https://www.zabbix.com/documentation/3.2/ru/manual/installation/upgrade_notes_320)<br>Zabbix [3.4](https://www.zabbix.com/documentation/3.4/ru/manual/installation/upgrade_notes_340)<br>Zabbix [4.0](https://www.zabbix.com/documentation/4.0/ru/manual/installation/upgrade_notes_400)<br>Zabbix [4.2](https://www.zabbix.com/documentation/4.2/ru/manual/installation/upgrade_notes_420)<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.2/ru/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/ru/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/ru/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/ru/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|Удалена поддержка распределенного мониторинга на основе нод|
|2.0.x|Для:<br>Zabbix [2.2](https://www.zabbix.com/documentation/2.2/ru/manual/installation/upgrade_notes_220)<br>Zabbix [2.4](https://www.zabbix.com/documentation/2.4/ru/manual/installation/upgrade_notes_240)<br>Zabbix [3.0](https://www.zabbix.com/documentation/3.0/ru/manual/installation/upgrade_notes_300)<br>Zabbix [3.2](https://www.zabbix.com/documentation/3.2/ru/manual/installation/upgrade_notes_320)<br>Zabbix [3.4](https://www.zabbix.com/documentation/3.4/ru/manual/installation/upgrade_notes_340)<br>Zabbix [4.0](https://www.zabbix.com/documentation/4.0/ru/manual/installation/upgrade_notes_400)<br>Zabbix [4.2](https://www.zabbix.com/documentation/4.2/ru/manual/installation/upgrade_notes_420)<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.2/ru/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/ru/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/ru/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/ru/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|Минимально требуемая версия PHP повышена с 5.1.6 до 5.3.0;<br>Требуется чувствительная к регистру MySQL база данных для правильной работы сервера; Для корректной работы с MySQL базой данных требуются кодировка utf8 и utf8\_bin тип сравнения требуются для Zabbix сервера. Смотрите [скрипты создания базы данных](/manual/appendix/install/db_scripts#mysql).<br>Вместо 'mysql' расширения требуется 'mysqli' расширение PHP|

Вы возможно захотите также ознакомиться с [требованиями](/manual/installation/requirements) для 6.2.

::: notetip
Возможно будет удобно запустить две параллельные SSH сессии на время обновления, выполняя шаги обновления в одной сессии и наблюдая за файлами журналов сервера/прокси в другой. Например, при выполнении `tail -f zabbix_server.log` или `tail -f zabbix_proxy.log` во второй SSH сессии будут отображаться последние записи из файла журнала и возможные ошибки в режиме реального времени. Такой подход может быть критичным на продуктивных серверах.
:::

[comment]: # ({/new-d02acea9})

[comment]: # ({d220a7d7-93ff8b03})
#### Процедура обновления

[comment]: # ({/d220a7d7-93ff8b03})

[comment]: # ({19b66714-f8102233})
##### 1 Остановите процессы Zabbix

Остановите Zabbix сервер, чтобы быть уверенным, что в базу данных не будет происходить запись новых данных.

    # service stop zabbix-server

При обновлении прокси, остановите также и его.

    # service stop zabbix-proxy

[comment]: # ({/19b66714-f8102233})

[comment]: # ({60850928-ab13a6a4})
##### 2 Сделайте архивную копию существующей базы данных Zabbix

Этот шаг очень важен. Убедитесь, что у вас имеется архивная копия вашей базы данных. Копия поможет, если процедура обновления закончится неудачно (отсутствие свободного места на диске, выключение питания, любая неожиданная проблема).

[comment]: # ({/60850928-ab13a6a4})

[comment]: # ({ddc24b39-6c141723})
##### 3 Резервное копирование файлов конфигурации, PHP файлов и бинарных файлов Zabbix

Выполните резервное копирование бинарных файлов Zabbix, файлов конфигурации и папки с PHP файлами.

Файлы конфигурации:

    # mkdir /opt/zabbix-backup/
    # cp /etc/zabbix/zabbix_server.conf /opt/zabbix-backup/
    # cp /etc/apache2/conf-enabled/zabbix.conf /opt/zabbix-backup/

Файлы PHP и бинарные файлы Zabbix:

    # cp -R /usr/share/zabbix/ /opt/zabbix-backup/
    # cp -R /usr/share/doc/zabbix-* /opt/zabbix-backup/

[comment]: # ({/ddc24b39-6c141723})

[comment]: # ({new-31c75b55})
##### 4 Обновите пакет конфигурации репозитория

Чтобы продолжить обновление, необходимо удалить текущий пакет репозитория.

    # rm -Rf /etc/apt/sources.list.d/zabbix.list

Затем установите новый пакет конфигурации репозитория.

На **Debian 11** выполните:

    # wget https://repo.zabbix.com/zabbix/6.2/debian/pool/main/z/zabbix-release/zabbix-release_6.2-1+debian11_all.deb
    # dpkg -i zabbix-release_6.2-1+debian11_all.deb

На **Debian 10** выполните:

    # wget https://repo.zabbix.com/zabbix/6.2/debian/pool/main/z/zabbix-release/zabbix-release_6.2-1+debian10_all.deb
    # dpkg -i zabbix-release_6.2-1+debian10_all.deb

На **Debian 9** выполните:

    # wget https://repo.zabbix.com/zabbix/6.2/debian/pool/main/z/zabbix-release/zabbix-release_6.2-1+debian9_all.deb
    # dpkg -i zabbix-release_6.2-1+debian9_all.deb

На **Ubuntu 20.04** выполните:

    # wget https://repo.zabbix.com/zabbix/6.2/ubuntu/pool/main/z/zabbix-release/zabbix-release_6.2-1+ubuntu20.04_all.deb
    # dpkg -i zabbix-release_6.2-1+ubuntu20.04_all.deb

На **Ubuntu 18.04** выполните:

    # wget https://repo.zabbix.com/zabbix/6.2/ubuntu/pool/main/z/zabbix-release/zabbix-release_6.2-1+ubuntu18.04_all.deb
    # dpkg -i zabbix-release_6.2-1+ubuntu18.04_all.deb

На **Ubuntu 16.04** выполните:

    # wget https://repo.zabbix.com/zabbix/6.2/ubuntu/pool/main/z/zabbix-release/zabbix-release_6.2-1+ubuntu16.04_all.deb
    # dpkg -i zabbix-release_6.2-1+ubuntu16.04_all.deb

На **Ubuntu 14.04** выполните:

    # wget https://repo.zabbix.com/zabbix/6.2/ubuntu/pool/main/z/zabbix-release/zabbix-release_6.2-1+ubuntu14.04_all.deb
    # dpkg -i zabbix-release_6.2-1+ubuntu14.04_all.deb

Обновите информацию о репозитории.

    # apt-get update

[comment]: # ({/new-31c75b55})

[comment]: # ({036287b5-08c7383c})
##### 5 Обновите компоненты Zabbix

Для обновления компонентов Zabbix вы можете выполнить что-то вроде:

    # apt-get install --only-upgrade zabbix-server-mysql zabbix-frontend-php zabbix-agent

При использовании PostgreSQL замените в команде `mysql` на `pgsql`. При обновлении прокси замените в команде `server` на `proxy`. При обновлении агента 2 замените в команде `zabbix-agent` на `zabbix-agent2`.

Затем, для корректного обновления веб-интерфейса с Apache также выполните:

    # apt-get install zabbix-apache-conf

Дистрибутивы **до Debian 10 (buster) / Ubuntu 18.04 (bionic) / Raspbian 10 (buster)** не предоставляют PHP 7.2 или более новые версии, который требуется веб-интерфейсу Zabbix 5.0. Смотрите [информацию](/manual/installation/frontend/frontend_on_rhel7) касательно установки веб-интерфейса Zabbix на устаревшие дистрибутивы.

[comment]: # ({/036287b5-08c7383c})

[comment]: # ({new-455f4e97})
##### 6 Проверьте параметры конфигурации компонент

Для получения более подробной информации смотрите заметки по обновлению на предмет [обязательных изменений](/manual/installation/upgrade_notes_600#параметры_конфигурации) (если таковые имеются).

Касательно новых опциональных параметров смотрите раздел [Что нового](/manual/introduction/whatsnew600#параметры_конфигурации).

[comment]: # ({/new-455f4e97})

[comment]: # ({6ce1e3ee-624a8fc7})
##### 7 Запустите процессы Zabbix

Запустите обновленные компоненты Zabbix.

    # service start zabbix-server
    # service start zabbix-proxy
    # service start zabbix-agent
    # service start zabbix-agent2

[comment]: # ({/6ce1e3ee-624a8fc7})

[comment]: # ({9d2b0a24-49e4f43e})
##### 8 Очистите cookies и кэш в веб-браузере

После обновления вам, возможно, потребуется очистить cookies веб-браузера и кэш веб-браузера, чтобы веб-интерфейс Zabbix работал должным образом.

[comment]: # ({/9d2b0a24-49e4f43e})

[comment]: # ({new-9bab02f2})
#### Обновление между минорными версиями

Имеется возможность обновления между минорными версиями 6.2.x (например, с 6.2.1 до 6.2.3). Процедура очень проста.

Чтобы выполнить минорное обновления версии Zabbix, пожалуйста, выполните:

    $ sudo apt install --only-upgrade 'zabbix.*'

Чтобы выполнить минорное обновления версии Zabbix сервера , пожалуйста, выполните:

    $ sudo apt install --only-upgrade 'zabbix-server.*'

Чтобы выполнить минорное обновления версии Zabbix агента, пожалуйста, выполните:

    $ sudo apt install --only-upgrade 'zabbix-agent.*'

или, для Zabbix агента 2:

    $ sudo apt install --only-upgrade 'zabbix-agent2.*'

[comment]: # ({/new-9bab02f2})
