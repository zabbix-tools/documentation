[comment]: # translation:outdated

[comment]: # ({4c385bd7-e2c1904c})
# 4 Установка из пакетов

[comment]: # ({/4c385bd7-e2c1904c})

[comment]: # ({27c0d5a3-8d855b7c})
#### Из официального репозитория Zabbix

Zabbix SIA поставляет официальные RPM и DEB пакеты для

-   [Red Hat Enterprise Linux/CentOS](/manual/installation/install_from_packages/rhel_centos)
-   [Debian/Ubuntu/Raspbian](/manual/installation/install_from_packages/debian_ubuntu)
-   [SUSE Linux Enterprise Server](/manual/installation/install_from_packages/suse)

Файлы пакетов для yum/dnf, apt и zypper репозиториев для различных дистрибутивов ИС доступны по адресу [repo.zabbix.com](https://repo.zabbix.com/).

Обратите внимание, что хоть некоторые дистрибутивы ОС (в частности, дистрибутивы на основе Debian) поставляют свои собственные пакеты Zabbix, эти пакеты не поддерживаются Zabbix. Пакеты Zabbix поставляемые сторонними лицами могут быть устаревшими, а также в них могут отсутствовать последние функции и исправления ошибок. Рекомендуется использовать только официальные пакеты с [repo.zabbix.com](https://repo.zabbix.com/). Если ранее вы использовали неофициальные пакеты Zabbix, смотрите заметки касательно [обновления пакетов Zabbix с репозиториев ОС](/manual/installation/upgrade/packages#пакеты_zabbix_с_репозиториев_ос).

[comment]: # ({/27c0d5a3-8d855b7c})
