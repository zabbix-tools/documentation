<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="ru" datatype="plaintext" original="manual/vm_monitoring/discovery_fields.md">
    <body>
      <trans-unit id="e6df368c" xml:space="preserve">
        <source># 2 Virtual machine discovery key fields

The following table lists fields returned by virtual machine related
discovery keys.

|**Item key**|&lt;|&lt;|
|--|--|----|
|**Description**|**Field**|**Retrieved content**|
|**vmware.cluster.discovery**|&lt;|&lt;|
|Performs cluster discovery.|{\#CLUSTER.ID}|Cluster identifier.|
|^|{\#CLUSTER.NAME}|Cluster name.|
|^|"resource_pool" |An array containing resource pool data, including resource group ID, tags array, resource pool path, number of virtual machines: &lt;br&gt;&lt;br&gt; [{"rpid":"resource group id", "tags":[{}], "rpath":"resource group path", "vm_count":0}]&lt;br&gt;&lt;br&gt; For tags array structure, see "tags" field.|
|^|"tags" |An array containing tags with tag name, description and category: &lt;br&gt;&lt;br&gt; [{"tag":"tag name","tag_description":"tag description", "category":"tag category"}]|
|**vmware.datastore.discovery**|&lt;|&lt;|
|Performs datastore discovery.|{\#DATASTORE}|Datastore name.|
|^|{\#DATASTORE.EXTENT}|JSON object with an array of {instanceName:partitionId}.|
|^|{\#DATASTORE.TYPE}|Datastore type.  Value examples: `VMFS`, `NFS`, `vsan`, etc. |
|^|{\#DATASTORE.UUID}|Datastore identifier.|
|^|"tags" |An array containing tags with tag name, description and category: &lt;br&gt;&lt;br&gt; [{"tag":"tag name","tag_description":"tag description", "category":"tag category"}]|
|**vmware.dc.discovery**|&lt;|&lt;|
|Performs datacenter discovery.|{\#DATACENTER}|Datacenter name.|
|^|{\#DATACENTERID}|Datacenter identifier.|
|^|"tags" |An array containing tags with tag name, description and category: &lt;br&gt;&lt;br&gt; [{"tag":"tag name","tag_description":"tag description", "category":"tag category"}]|
|**vmware.dvswitch.discovery**|&lt;|&lt;|
|Performs vSphere distributed switches discovery.|{\#DVS.NAME}|Switch name.|
|^|{\#DVS.UUID}|Switch identifier.|
|**vmware.hv.discovery**|&lt;|&lt;|
|Performs hypervisor discovery.|{\#HV.UUID}|Unique hypervisor identifier.|
|^|{\#HV.ID}|Hypervisor identifier (HostSystem managed object name).|
|^|{\#HV.NAME}|Hypervisor name.|
|^|{\#HV.NETNAME}|Hypervisor network host name.|
|^|{\#HV.IP}|Hypervisor IP address, might be empty.&lt;br&gt;In case of an HA configuration with multiple net interfaces, the following selection priority for interface is observed:&lt;br&gt;- prefer the IP which shares the IP-subnet with the vCenter IP&lt;br&gt;- prefer the IP from IP-subnet with default gateway&lt;br&gt;- prefer the IP from interface with the lowest ID&lt;br&gt;This field is supported since Zabbix 5.2.2.|
|^|{\#CLUSTER.NAME}|Cluster name, might be empty.|
|^|{\#DATACENTER.NAME}|Datacenter name.|
|^|{\#PARENT.NAME}|Name of container that stores the hypervisor.&lt;br&gt;Supported since Zabbix 4.0.3.|
|^|{\#PARENT.TYPE}|Type of container in which the hypervisor is stored. The values could be `Datacenter`, `Folder`, `ClusterComputeResource`, `VMware`, where 'VMware' stands for unknown container type.&lt;br&gt;Supported since Zabbix 4.0.3.|
|^|"resource_pool" |An array containing resource pool data, including resource group ID, tags array, resource pool path, number of virtual machines: &lt;br&gt;&lt;br&gt; [{"rpid":"resource group id", "tags":[{}], "rpath":"resource group path", "vm_count":0}]&lt;br&gt;&lt;br&gt; For tags array structure, see "tags" field.|
|^|"tags" |An array containing tags with tag name, description and category: &lt;br&gt;&lt;br&gt; [{"tag":"tag name","tag_description":"tag description", "category":"tag category"}]|
|**vmware.hv.datastore.discovery**|&lt;|&lt;|
|Performs hypervisor datastore discovery. Note that multiple hypervisors can use the same datastore.|{\#DATASTORE}|Datastore name.|
|^|{\#DATASTORE.TYPE}|Datastore type.  Value examples: `VMFS`, `NFS`, `vsan`, etc. |
|^|{\#DATASTORE.UUID}|Datastore identifier.|
|^|{\#MULTIPATH.COUNT}|Registered number of datastore paths.|
|^|{\#MULTIPATH.PARTITION.COUNT}|Number of available disk partitions.|
|^|"datastore_extent" |An array containing datastore extent instance name and partition ID: &lt;br&gt;&lt;br&gt; [{"instance":"name", "partitionid":1}]|
|^|"tags" |An array containing tags with tag name, description and category: &lt;br&gt;&lt;br&gt; [{"tag":"tag name","tag_description":"tag description", "category":"tag category"}]|
|**vmware.hv.net.if.discovery**|&lt;|&lt;|
|Performs hypervisor network interfaces discovery. |{\#IFNAME}|Interface name.|
|^|{\#IFDRIVER}|Interface driver.|
|^|{\#IFDUPLEX}|Interface duplex settings.|
|^|{\#IFSPEED}|Interface speed.|
|^|{\#IFMAC}|Interface mac address.|
|**vmware.vm.discovery**|&lt;|&lt;|
|Performs virtual machine discovery.|{\#VM.UUID}|Unique virtual machine identifier.|
|^|{\#VM.ID}|Virtual machine identifier (VirtualMachine managed object name).|
|^|{\#VM.NAME}|Virtual machine name.|
|^|{\#HV.NAME}|Hypervisor name.|
|^|{\#HV.UUID}|Unique hypervisor identifier.|
|^|{\#HV.ID}|Hypervisor identifier (HostSystem managed object name).|
|^|{\#CLUSTER.NAME}|Cluster name, might be empty.|
|^|{\#DATACENTER.NAME}|Datacenter name.|
|^|{\#DATASTORE.NAME}|Datastore name.|
|^|{\#DATASTORE.UUID}|Datastore identifier.|
|^|{\#VM.IP}|Virtual machine IP address, might be empty.|
|^|{\#VM.DNS}|Virtual machine DNS name, might be empty.|
|^|{\#VM.GUESTFAMILY}|Guest virtual machine OS family, might be empty.|
|^|{\#VM.GUESTFULLNAME}|Full guest virtual machine OS name, might be empty.|
|^|{\#VM.FOLDER}|The chain of virtual machine parent folders, can be used as value for nested groups; folder names are combined with "/". Might be empty.|
|^|{\#VM.TOOLS.STATUS}|VMware virtual machine tools state.|
|^|{\#VM.POWERSTATE}|VMware virtual machine power state (poweredOFF, poweredOn, or suspended).|
|^|{\#VM.RPOOL.ID}|Resource pool identifier.|
|^|{\#VM.RPOOL.PATH}|Full resource pool path excluding the "root" name "Resources". Folder names are combined with "/". |
|^|{\#VM.SNAPSHOT.COUNT}|Number of VM snapshots. |
|^|"tags" |An array containing tags with tag name, description and category: &lt;br&gt;&lt;br&gt; [{"tag":"tag name","tag_description":"tag description", "category":"tag category"}]|
|^|"vm_customattribute"|An array of virtual machine custom attributes (if defined): &lt;br&gt;&lt;br&gt; [{"name":"custom field name", "value":"custom field value"}] |
|**vmware.vm.net.if.discovery**|&lt;|&lt;|
|Performs virtual machine network interface discovery.|{\#IFNAME}|Network interface name.|
|^|{\#IFMAC}|Interface mac address.|
|^|{\#IFCONNECTED}|Interface connection status (0 - disconnected, 1 - connected).|
|^|{\#IFTYPE}|Interface type.|
|^|{\#IFBACKINGDEVICENAME}|Name of the backing device.|
|^|{\#IFDVSWITCH.UUID}|Unique vSphere Distributed Switch identifier.|
|^|{\#IFDVSWITCH.PORTGROUP}|Distributed port group.|
|^|{\#IFDVSWITCH.PORT}|vSphere Distributed Switch port.|
|**vmware.vm.vfs.dev.discovery**|&lt;|&lt;|
|Performs virtual machine disk device discovery.|{\#DISKNAME}|Disk device name.|
|**vmware.vm.vfs.fs.discovery**|&lt;|&lt;|
|Performs virtual machine file system discovery.|{\#FSNAME}|File system name.|</source>
      </trans-unit>
    </body>
  </file>
</xliff>
