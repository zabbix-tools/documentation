[comment]: # translation:outdated

[comment]: # ({new-764560a7})
# 1 Меню

[comment]: # ({/new-764560a7})

[comment]: # ({new-4edaa9a7})
#### Обзор

Вертикальное меню на боковой панели обеспечивает доступ к различным
разделам веб-интерфейса Zabbix.

В теме по умолчанию это меню темно-синего цвета.

![](../../../assets/en/manual/introduction/vertical_menu.png){width="600"}

[comment]: # ({/new-4edaa9a7})

[comment]: # ({new-6ddbf7a4})
#### Работа с меню

Глобальное окно поиска расположено под логотипом Zabbix.

Меню может быть свернуто или скрыто полностью:

-   Чтобы свернуть, нажмите
    ![](../../../assets/en/manual/web_interface/button_collapse.png)
    рядом с логотипом Zabbix
-   Чтобы скрыть, нажмите
    ![](../../../assets/en/manual/web_interface/button_hide.png) рядом с
    логотипом Zabbix

|   |   |
|---|---|
|![](../../../assets/en/manual/web_interface/collapsed_menu.png)|![](../../../assets/en/manual/web_interface/hidden_menu.png)|
|Свернутое меню с иконками.|Скрытое меню.|

[comment]: # ({/new-6ddbf7a4})

[comment]: # ({new-3086e8b2})
##### Свернутое меню

Когда меню свернуто до значков, полное меню появляется снова при
наведении курсора мыши. Обратите внимание, что оно появляется поверх
содержимого страницы; чтобы переместить содержимое страницы вправо,
нужно нажать на кнопку развернуть. Если переместить курсор мыши за
пределы полного меню, меню снова свернется через две секунды. Вы также
можете развернуть свернутое меню, нажав клавишу Tab. Повторное нажатие
клавиши Tab выделит следующий элемент меню.

[comment]: # ({/new-3086e8b2})

[comment]: # ({new-7dd192ee})
##### Скрытое меню

Даже когда меню полностью скрыто, доступ к нему можно получить всего
одним щелчком мыши. Нажмите на иконку меню (так называемый "гамбургер")
в верхнем правом углу, чтобы открыть меню. Обратите внимание, что меню
появляется поверх содержимого страницы; чтобы переместить содержимое
страницы вправо, измените режим отображения меню, нажав на кнопку
"Отображать боковую панель" рядом с логотипом Zabbix.

[comment]: # ({/new-7dd192ee})

[comment]: # ({new-0d46829f})

##### Menu levels

There are up to three levels in the menu.

![](../../../assets/en/manual/web_interface/menu_levels.png)

[comment]: # ({/new-0d46829f})

[comment]: # ({new-5ec4ea88})
### Context menu

In addition to the main menu, Zabbix provides [host menu](/manual/web_interface/menu/host_menu) and 
[event menu](/manual/web_interface/menu/event_menu) for convenient access to frequently used elements, such as
configuration window, related scripts or external links. The context menus are accessible by clicking on the host or
problem/trigger name in supported locations.

Similarly, in the *Monitoring->Latest data* section clicking on the item name brings up the 
[item menu](/manual/web_interface/frontend_sections/monitoring/latest_data#item-menu).

[comment]: # ({/new-5ec4ea88})
