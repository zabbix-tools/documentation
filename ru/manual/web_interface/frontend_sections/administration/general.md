[comment]: # translation:outdated

[comment]: # ({new-fa0262ff})
# 1 Общие

[comment]: # ({/new-fa0262ff})

[comment]: # ({new-f36c2788})
#### Обзор

Раздел *Администрирование → Общие* содержит несколько страниц по
настройке, связанных с веб-интерфейсом умолчаний, и тонкой настройки
Zabbix.

Выпадающее меню слева позволит вам переключаться между различными
страницами настроек.

![](../../../../../assets/en/manual/web_interface/frontend_sections/administration/adm_general.png)

[comment]: # ({/new-f36c2788})

[comment]: # ({new-4513c0d7})
##### - Веб-интерфейс

Эта страница содержит некоторые настройки умолчаний, связанных с
веб-интерфейсом.

![](../../../../../assets/en/manual/web_interface/frontend_sections/administration/general_gui.png)

Параметры настроек:

|Параметр|Описание|
|----------------|----------------|
|*Язык по умолчанию*|Язык по умолчанию для пользователей, которые не указали язык в своих профилях, и гостевых пользователей.|
|*Часовой пояс по умолчанию*|[Часовой пояс](/manual/web_interface/time_zone#overview) по умолчанию для пользователей, которые не указали часовой пояс в своих профилях, и гостевых пользователей.|
|*Тема по умолчанию*|Тема по умолчанию для пользователей, кто не указал свою собственную тему в своем профиле.|
|*Лимит элементов в поиске/фильтре*|Максимальное количество элементов (строк), которые будут отображаться в списках веб-интерфейса, в таких как, например, *Настройка → Узлы сети*.<br>*Обратите внимание*: Если значение равно, например, '50', будут отображаться только первые 50 элементов во всех попадающих под влияние списках веб-интерфейса. Если какие-нибудь списки содержат больше пятидесяти элементов, тогда такое превышение будет указываться знаком '+' в *"Отображено с 1 по 50 из **50+** найденных"*. Так же, если используется фильтрация и имеется более 50 совпадений, то отобразятся только первые 50 совпадений.|
|*Max number of columns<br>and rows in overview tables*|Максимальное количество столбцов и строчек, которое будет отображаться в разделе *Мониторинг → Обзор*. Один и тот же лимит применяется и к строчкам, и к столбцам. Если существует больше строк и/или столбцов, чем показано, система отобразит предупреждение внизу таблицы: «Отображаются не все результаты. Укажите более конкретные критерии поиска».|
|*Макс. количество<br>элементов в ячейке таблицы*|Для объектов которые отображаются в одной ячейки таблицы, отображать не более чем заданное в этой опции.|
|*Показывать предупреждение, если Zabbix сервер недоступен*|Этот параметр включает предупреждающее сообщение, которое будет отображаться в окне браузера, если не удается "достучаться" (может быть недоступен) до Zabbix сервера. Сообщение остается на экране, даже если пользователь прокрутил страницу вниз, если навести на надпись курсор мыши, то сообщение временно исчезнет, чтобы было возможно увидеть содержимое под сообщением.<br>Этот параметр поддерживается Zabbix начиная с версии 2.0.1.|
|*Working time*|Этот общесистемный параметр определяет рабочее время. На графиках рабочее время отображается на белом фоне, а нерабочее время - на сером.<br>Описание формата времени смотрите на странице [Time period specification](/ru/manual/appendix/time_period).<br>Поддерживаются [пользовательские макросы](/manual/config/macros/user_macros)(с Zabbix 3.4.0).|
|*Показывать технические ошибки*|Если этот флажок установлен, все зарегистрированные пользователи смогут видеть технические ошибки (PHP/SQL). Если не установлен, информация будет доступна только [Супер-администраторам Zabbix](/manual/web_interface/frontend_sections/administration/users#users1) и пользователям принадлежащим к группам пользователя с активрованным [debug mode](/manual/web_interface/debug_mode).|
|*Max history display period*|Maximum time period for which to display historical data in *Monitoring* subsections: *Latest data*, *Web*, *Overview* and in the *Data overview* screen element.<br>Allowed range: 24 hours (default) - 1 week. [Time suffixes](/manual/appendix/suffixes), e.g. 1w (one week), 36h (36 hours), are supported.|
|*Time filter default period*|Time period to be used in graphs and dashboards by default. Allowed range: 1 minute - 10 years (default: 1 hour).<br>[Time suffixes](/manual/appendix/suffixes), e.g. 10m (ten minutes), 5w (five weeks), are supported.<br>Note: when a user changes the time period while viewing a graph, this time period is stored as user preference, replacing the global default or a previous user selection.|
|*Max period for time selector*|Maximum available time period for graphs and dashboards. Users will not be able to visualize older data. Allowed range: 1 year - 10 years (default: 2 years).<br>[Time suffixes](/manual/appendix/suffixes), e.g. 1y (one year), 365w (365 weeks), are supported.|

[comment]: # ({/new-4513c0d7})

[comment]: # ({new-2cf4d492})
##### - Авторегистрация

На этом экране вы можете настроить уровень шифрования для автоматической
регистрации активного агента.

![adm\_autoreg2.png](../../../../../assets/en/manual/web_interface/frontend_sections/administration/adm_autoreg2.png)

Параметры, отмеченные звездочкой, являются обязательными.

Параметры конфигурации:

|Параметр|Описание|<|
|----------------|----------------|-|
|*Уровень шифрования*|Выберите один или оба параметра для уровня шифрования:<br>**Без шифрования** - разрешены незашифрованные соединения<br>**PSK** - разрешены зашифрованные соединения TLS с предопределенным ключом (pre-shared key)|<|
|*идентификатор PSK*|Введите строку идентификатора предварительного общего ключа.<br>Это поле доступно только в том случае, если в качестве // уровня шифрования выбрано «PSK» *.<br>Не помещайте конфиденциальную информацию в идентификатор PSK, она передается незашифрованной по сети в сообщите получателю, какой PSK использовать. \| \|*PSK//|Введите общий ключ (четное число шестнадцатеричных символов).<br>Максимальная длина: 512 шестнадцатеричных цифр (256-байтовый PSK), если Zabbix использует библиотеку GnuTLS или OpenSSL, 64 шестнадцатеричных цифры (32-байтовый PSK), если Zabbix использует библиотеку mbed TLS (PolarSSL).<br>Пример: 1f87b595725ac58dd977beef14b97461a7c1045b9a1c963065002c5473194952<br>Это поле доступно только в том случае, если в качестве уровня шифрования выбрано «PSK».|

См. также: [Безопасная
авторегистрация](/ru/manual/discovery/auto_registration#безопасная_авторегистрация)

[comment]: # ({/new-2cf4d492})



[comment]: # ({new-6905e224})
##### - Соответствия иконок

Этот раздел позволяет создавать соответствия неких узлов сети с некими
иконками. Для создания соответствия используется информация из полей
инвентарных данных узлов сети.

Затем соответствия можно использовать при [настройке карты
сети](/ru/manual/config/visualisation/maps/map) для автоматического
назначения соответствующих иконок к подходящим узлам сети.

Для создания нового соответствия иконок, нажмите на *Создать
соответствие иконок* в верхнем правом углу.

![](../../../../../assets/en/manual/web_interface/frontend_sections/administration/general_iconmap.png){width="600"}

Параметры настроек:

|Параметр|Описание|
|----------------|----------------|
|*Имя*|Уникальное имя соответствия иконок.|
|*Соответствия*|Список соответствий. Порядок соответствий определяет, какой приоритет будет иметь соответствие. Вы можете перемещать соответствия выше и ниже по списку, используя перетаскивание (drag-and-drop).|
|*Поле инвентарных данных*|Поле инвентарных данных узла сети, которое будет просматриваться при поиске совпадений.|
|*Выражение*|Регулярное выражение описывающее совпадение.|
|*Иконка*|Используемая иконка, если совпадение выражения найдено.|
|*По умолчанию*|Используемая по умолчанию иконка.|

[comment]: # ({/new-6905e224})

[comment]: # ({new-f8d0c75f})
##### - Регулярные выражения

Этот раздел позволяет создавать пользовательские регулярные выражения,
которые затем можно использовать в некоторых местах в Zabbix. Смотрите
раздел [Регулярные выражения](/ru/manual/regular_expressions) для
получения подробностей.

[comment]: # ({/new-f8d0c75f})

[comment]: # ({new-b9fd47e9})
##### - Макросы

Этот раздел позволяет определить общесистемные макросы, задать их
значения и описания (опционально).

![](../../../../../assets/en/manual/config/user_macros_secret.png){width="600"}

Смотрите раздел [Пользовательские
макросы](/ru/manual/config/macros/usermacros) для получения более
подробных сведений.

[comment]: # ({/new-b9fd47e9})


[comment]: # ({new-cca854b5})
##### - Рабочее время

Рабочее время является общесистемным параметром, который определяет
рабочее время. Рабочее время отображается белым фоном в графиках, в то
время как нерабочее время отображается серым цветом.

![](../../../../../assets/en/manual/web_interface/frontend_sections/administration/general_worktime.png)

Смотрите страницу [Спецификации периодов
врмени](/ru/manual/appendix/time_period) для получения более подробной
информации о формате времени. Поддерживаются [пользовательские
макросы](/ru/manual/config/macros/usermacros) (начиная с Zabbix 3.4.0).

[comment]: # ({/new-cca854b5})

[comment]: # ({new-09d9d657})
##### - Важности триггеров

Этот раздел позволяет настроить имена и цвета [важностей
триггеров](/ru/manual/config/triggers/severity).

![](../../../../../assets/en/manual/web_interface/frontend_sections/administration/general_severities1.png){width="600"}

Вы можете ввести новые имена и коды цветов или нажать на цвете, чтобы
выбрать другой цвета из представленной палитры.

Смотрите страницу [Настройка важностей
триггеров](/ru/manual/config/triggers/customseverities) для получения
подробной информации.

[comment]: # ({/new-09d9d657})

[comment]: # ({new-ef76d5b4})
##### - Опции отображения триггеров

Этот раздел позволяет настроить, то каким образом состояния триггеров
будет отображаться в веб-интерфейсе.

![](../../../../../assets/en/manual/web_interface/frontend_sections/administration/general_trigger_display1.png)

Выбор "*Использовать пользовательские цвета состояний событий*"
активирует возможность изменения цветов для
подтвержденных/неподтвержденных событий. Соответственно, не выбранная
эта опция деактивирует эту возможность изменения. Эта опция не управляет
миганием.

Кроме того, можно настроить период времени в течении которого будут
отображаться триггеры в состоянии ОК и период отображения мигающих, по
причине изменения своего состояния, триггеров, которые изменили
состояние. Максимальным значением является 86400 секунд (24 часа). В
полях периодов поддерживаются [суффиксы
времени](/ru/manual/appendix/suffixes), например, 5m, 2h, 1d.

[comment]: # ({/new-ef76d5b4})

[comment]: # ({new-72ef5238})
##### - Модули

Этот раздел позволяет администрировать пользовательские [модули внешнего
интерфейса](/ru/manual/modules).

![](../../../../../assets/en/manual/web_interface/frontend_sections/administration/general_modules.png)

Нажмите на *Сканировать директорию*, чтобы зарегистрировать/отменить
регистрацию любых пользовательских модулей. Зарегистрированные модули
появятся в списке вместе с их деталями. Незарегистрированные модули
будут удалены из списка.

Вы можете фильтровать модули по имени или состоянию
(включено/отключено). Нажмите на статус модуля в списке, чтобы
включить/отключить модуль. Вы также можете массово включить/отключить
модули, выбрав их в списке, а затем нажав кнопки *Включить/Отключить*
под списком.

[comment]: # ({/new-72ef5238})

[comment]: # ({new-0e5d17fd})
#### - Connectors

This section allows to configure connectors for Zabbix data [streaming to external systems](/manual/config/export/streaming) over HTTP.

![](../../../../../assets/en/manual/web_interface/frontend_sections/administration/general_connectors.png){width="600"}

Click on *Create connector* to configure a new [connector](/manual/config/export/streaming#configuration).

You may filter connectors by name or status (enabled/disabled). Click on the connector status in the list to enable/disable a connector. 
You may also mass enable/disable connectors by selecting them in the list and then clicking on the *Enable/Disable* buttons below the list.

[comment]: # ({/new-0e5d17fd})






[comment]: # ({new-10e8eeaa})
#### - Other parameters

This section allows configuring miscellaneous other frontend parameters.

![](../../../../../assets/en/manual/web_interface/frontend_sections/administration/general_other.png)

|Parameter|Description|
|---------|-----------|
|*Frontend URL*|URL to Zabbix web interface. This parameter is used by Zabbix web service for communication with frontend and should be specified to enable scheduled reports.|
|*Group for discovered hosts*|Hosts discovered by [network discovery](/manual/discovery/network_discovery) and [agent autoregistration](/manual/discovery/auto_registration) will be automatically placed in the host group, selected here.|
|*Default host inventory mode*|Default [mode](/manual/config/hosts/inventory) for host inventory. It will be followed whenever a new host or host prototype is created by server or frontend unless overridden during host discovery/autoregistration by the *Set host inventory mode* operation.|
|*User group for database down message*|User group for sending alarm message or 'None'.<br>Zabbix server depends on the availability of the backend database. It cannot work without a database. If the database is down, selected users can be notified by Zabbix. Notifications will be sent to the user group set here using all configured user media entries. Zabbix server will not stop; it will wait until the database is back again to continue processing.<br>Notification consists of the following content:<br>`[MySQL\|PostgreSQL\|Oracle] database <DB Name> [on <DB Host>:<DB Port>] is not available: <error message depending on the type of DBMS (database)>`<br><DB Host> is not added to the message if it is defined as an empty value and <DB Port> is not added if it is the default value ("0"). The alert manager (a special Zabbix server process) tries to establish a new connection to the database every 10 seconds. If the database is still down the alert manager repeats sending alerts, but not more often than every 15 minutes.|
|*Log unmatched SNMP traps*|Log [SNMP trap](/manual/config/items/itemtypes/snmptrap) if no corresponding SNMP interfaces have been found.|

[comment]: # ({/new-10e8eeaa})

[comment]: # ({new-1ff15253})
##### Authorization

|Parameter|Description|
|---------|-----------|
|*Login attempts*|Number of unsuccessful login attempts before the possibility to log in gets blocked.|
|*Login blocking interval*|Period of time for which logging in will be prohibited when *Login attempts* limit is exceeded.|

[comment]: # ({/new-1ff15253})

[comment]: # ({new-99496c72})
##### Storage of secrets

*Vault provider* parameter allows selecting secret management software for storing [user macro](/manual/config/macros/user_macros#configuration) values. Supported options: 
- *HashiCorp Vault* (default)
- *CyberArk Vault*

See also: [Storage of secrets](/manual/config/secrets).

[comment]: # ({/new-99496c72})

[comment]: # ({new-2b1937dd})
##### Security

|Parameter|Description|
|---------|-----------|
|*Validate URI schemes*|Uncheck the box to disable URI scheme validation against the whitelist defined in *Valid URI schemes*. (enabled by default).|
|*Valid URI schemes*|A comma-separated list of allowed URI schemes. Applies to all fields in the frontend where URIs are used (for example, map element URLs).<br>this field is editable only if *Validate URI schemes* is selected.|
|*X-Frame-Options HTTP header*|Value of HTTP X-Frame-options header. Supported values:<br>**SAMEORIGIN** (default) - the page can only be displayed in a frame on the same origin as the page itself.<br>**DENY** - the page cannot be displayed in a frame, regardless of the site attempting to do so.<br>**null** - disable X-Frame-options header (not recommended).<br>Or a list (string) of comma-separated hostnames. If a listed hostname is not among allowed, the SAMEORIGIN option is used.|
|*Use iframe sandboxing*|This parameter determines whether retrieved URL content should be put into the sandbox or not. Note, that turning off sandboxing is not recommended.|
|*Iframe sandboxing exceptions*|If sandboxing is enabled and this field is empty, all sandbox attribute restrictions apply. To disable some of the restrictions, specified them in this field. This disables only restrictions listed here, other restrictions will still be applied. See [sandbox attribute](https://www.w3.org/TR/2010/WD-html5-20100624/the-iframe-element.html#attr-iframe-sandbox) description for additional information.|

[comment]: # ({/new-2b1937dd})

[comment]: # ({new-c8d6c320})
##### Communication with Zabbix server

|Parameter|Description|
|---------|-----------|
|*Network timeout*|How many seconds to wait before closing an idle socket (if a connection to Zabbix server has been established earlier, but frontend can not finish read/send data operation during this time, the connection will be dropped). Allowed range: 1 - 300s (default: 3s).|
|*Connection timeout*|How many seconds to wait before stopping an attempt to connect to Zabbix server. Allowed range: 1 - 30s (default: 3s).|
|*Network timeout for media type test*|How many seconds to wait for a response when testing a media type. Allowed range: 1 - 300s (default: 65s).|
|*Network timeout for script execution*|How many seconds to wait for a response when executing a script. Allowed range: 1 - 300s (default: 60s).|
|*Network timeout for item test*|How many seconds to wait for returned data when testing an item. Allowed range: 1 - 300s (default: 60s).|
|*Network timeout for scheduled report test*|How many seconds to wait for returned data when testing a scheduled report. Allowed range: 1 - 300s (default: 60s).|

[comment]: # ({/new-c8d6c320})
