[comment]: # translation:outdated

[comment]: # ({new-98ea19ec})
# 2 Services

[comment]: # ({/new-98ea19ec})

[comment]: # ({new-55d55266})
#### Overview

The Services menu is for the [service monitoring](/manual/it_services) functions of Zabbix.

[comment]: # ({/new-55d55266})
