[comment]: # translation:outdated

[comment]: # ({new-290553c7})
# 1 Информация о систсеме

[comment]: # ({/new-290553c7})

[comment]: # ({new-ede74e69})
#### Обзор

В разделе *Отчеты → Информация о системе* отображается краткая сводка о
ключевых данных системы.

![](../../../../../assets/en/manual/web_interface/system_information.png){width="600"}

Этот отчет также отображается виджетом на
[ПАНЕЛИ](/ru/manual/web_interface/frontend_sections/monitoring/dashboard).

[comment]: # ({/new-ede74e69})

[comment]: # ({new-bfa24672})
##### Отображаемые данные

|Параметр|Значение|Детали|
|----------------|----------------|------------|
|*Zabbix сервер запущен*|Состояние Zabbix сервера:<br>**Да** - сервер запущен<br>**Нет** - сервер не запущен<br>*Обратите внимание:* Чтобы отображать некоторую информацию, веб-интерфейсу необходим работающий сервер и у сервера должен быть запущен по крайней мере один процесс траппера (параметр StartTrappers в [zabbix\_server.conf](/ru/manual/appendix/config/zabbix_server) файле>0).|Размещение и порт Zabbix сервера.|
|*Количество узлов сети*|Отображается общее количество добавленных узлов сети.<br>Шаблоны тоже считаются подтипами узлов сети.|Количество наблюдаемых узлов сети/не наблюдаемых узлов сети/шаблонов.|
|*Количество элементов данных*|Отображается общее количество элементов данных.|Количество наблюдаемых/деактивированных/неподдерживаемых элементов данных.<br>Элементы данных на деактивированных узлах сети считаются деактивированными.|
|*Количество триггеров*|Отображается общее количество триггеров.|Количество активированных/деактивированных триггеров. \[Триггеры в состоянии проблема/ок.\]<br>Триггеры назначенные на деактивированные узлы сети или зависящие от деактивированных элементов данных считаются деактивированными.|
|*Количество пользователей*|Отображается общее количество добавленных пользователей.|Количество пользователей в сети.|
|*Требуемое быстродействие сервера, новые значения в секунду*|Отображается ожидаемое количество новых значений обрабатываемых Zabbix сервером в секунду.|*Требуемое быстродействие сервера* является оценочным и может быть полезным как ориентир. Для точных чисел обработанных значений, используйте [внутренний элемент данных](/ru/manual/config/items/itemtypes/internal) `zabbix[wcache,values,all]`.<br><br>В вычисление включаются активированные элементы данных с узлов сети под наблюдением. Элементы данных журналов считаются как одно значение за период интервала обновления элемента данных. Подсчитываются обычные значений интервалов; значения переменных и интервалов по расписанию не учитываются. Вычисление не корректируется во время периода обслуживания "без данных". Траппер элементы данных не подсчитываются.|

[comment]: # ({/new-bfa24672})


[comment]: # ({new-76697959})
#### High availability nodes

If [high availability cluster](/manual/concepts/server/ha) is enabled,
then another block of data is displayed with the status of each high
availability node.

![](../../../../../assets/en/manual/web_interface/ha_nodes.png){width="600"}

Displayed data:

|Column|Description|
|------|-----------|
|*Name*|Node name, as defined in server configuration.|
|*Address*|Node IP address and port.|
|*Last access*|Time of node last access.<br>Hovering over the cell shows the timestamp of last access in long format.|
|*Status*|Node status:<br>**Active** - node is up and working<br>**Unavailable** - node hasn't been seen for more than failover delay (you may want to find out why)<br>**Stopped** - node has been stopped or couldn't start (you may want to start it or delete it)<br>**Standby** - node is up and waiting|

[comment]: # ({/new-76697959})
