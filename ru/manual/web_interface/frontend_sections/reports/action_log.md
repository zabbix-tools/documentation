[comment]: # translation:outdated

[comment]: # ({new-7d551978})
# 5 Журнал действий

[comment]: # ({/new-7d551978})

[comment]: # ({new-8be899f9})
#### Обзор

На этом экране имеется подробная информация о выполненных действиями
операций (оповещения, удаленные команды).

![](../../../../../assets/en/manual/web_interface/action_log.png){width="600"}

Отображаемые данные:

|Колонка|Описание|
|--------------|----------------|
|*Время*|Время действия.|
|*Действие*|Имя действия, вызвавшего операции.<br>Имя действия отображается начиная с Zabbix 2.4.0.|
|*Тип*|Тип действия - *Email* или *Команда*.|
|*Получатель(и)*|Псевдоним пользователя, его имя и фамилия (в круглых скобках) и e-mail адрес получателя оповещения.<br>Имя пользователя, его имя и фамилия отображаются начиная с Zabbix 2.4.0.|
|*Сообщение*|Содержимое сообщения/удаленной команды.<br>Удаленная команда отделяется от узла сети, который является целью, при помощи символа двоеточия: `<узел сети>:<команда>`. Если удаленная команда выполнялась на Zabbix сервер, тогда информация имеет следующий формат: `Zabbix server:<команда>`|
|*Состояние*|Состояние действия:<br>*в процессе* - действие в процессе выполнения<br>Для действий в состоянии в процессе отправки отображается оставшееся количество попыток - сколько раз сервер будет пытаться еще отправить оповещение.<br>*отправлено* - оповещение было отправлено<br>*выполнено* - команда была выполнена<br>*не отправлено* - действие не завершилось|
|*Инфо*|Информация об ошибке (если имеется) при выполнении действия.|

**Использование фильтра**

Вы можете использовать фильтр чтобы уменьшить количество записей,
используя фильтр по e-mail получателя.

Фильтр расположен ниже надписи раздела *Журнал действий*. Его можно
открыть и свернуть если нажать слева на вкладке *Фильтр*.

**Селектор периода времени**

[Селектор периода
времени](/ru/manual/config/visualisation/graphs/simple#селектор_периода_времени)
позволяет выбирать часто требуемые периоды за одно нажатие мышкой.
Селектор периода времени можно открыть если нажать на вкладку периода
времени, которая расположена после фильтра.

[comment]: # ({/new-8be899f9})

[comment]: # ({new-d84e7bb0})
#### Buttons

The button at the top right corner of the page offers the following option:

|   |   |
|--|--------|
|![](../../../../../assets/en/manual/web_interface/button_csv.png)|Export action log records from all pages to a CSV file. If a filter is applied, only the filtered records will be exported.<br>In the exported CSV file the columns "Recipient" and "Message" are divided into several columns - "Recipient's Zabbix username", "Recipient's name", "Recipient's surname", "Recipient", and "Subject", "Message", "Command".|

[comment]: # ({/new-d84e7bb0})

[comment]: # ({new-accea306})
#### Using filter

The filter is located below the *Action log* bar.
It can be opened and collapsed by clicking on the *Filter* tab at the top right corner of the page.

![](../../../../../assets/en/manual/web_interface/action_log_filter.png){width="600"}

You may use the filter to narrow down the records by notification recipients, actions, media types, status,
or by the message/remote command content (*Search string*).
For better search performance, data is searched with macros unresolved.

[comment]: # ({/new-accea306})

[comment]: # ({new-3eac5e48})
#### Time period selector

The [time period selector](/manual/config/visualization/graphs/simple#time_period_selector) allows to select often required periods with one mouse click.
The time period selector can be opened by clicking on the time period tab next to the filter.

[comment]: # ({/new-3eac5e48})
