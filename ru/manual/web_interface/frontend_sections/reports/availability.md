[comment]: # translation:outdated

[comment]: # ({new-d12677af})
# 2 Отчет о доступности

[comment]: # ({/new-d12677af})

[comment]: # ({new-75fed46f})
#### Overview

В разделе *Отчеты → Отчет о доступности* вы можете просмотреть какую
часть времени каждый триггер был в состояниях проблема/ок/неизвестно.
Для каждого состояния отображается процентное отношение к общему
времени.

Таким образом, можно легко определить ситуации доступности различных
элементов вашей системы.

![](../../../../../assets/en/manual/web_interface/availability_host.png){width="600"}

Из выпадающего списка в верхнем правом углу вы можете выбрать режим
выборки - следует ли отображать триггеры по узлам сети или по триггерам,
принадлежащим шаблону.

![](../../../../../assets/en/manual/web_interface/availability_trigger.png){width="600"}

Имя триггера является ссылкой на последние события этого триггера.

**Использование фильтра**

Вы можете использовать фильтр чтобы уменьшить количество выбранных
записей. Указывая родительскую группу узлов сети косвенным образом будут
выбраны все вложенные группы узлов сети.

Фильтр расположен ниже надписи раздела *Отчет о доступности*. Его можно
открыть и свернуть если нажать слева на вкладке *Фильтр*.

**Селектор периода времени**

[Селектор периода
времени](/ru/manual/config/visualisation/graphs/simple#селектор_периода_времени)
позволяет выбирать часто требуемые периоды за одно нажатие мышкой.
Селектор периода времени можно открыть если нажать на вкладку периода
времени, которая расположена после фильтра.

При нажатии на *Показать* в колонке График, вы увидите столбчатый
график, на котором отобразится информация о доступности в формате, когда
каждый столбец представляет прошлую неделю текущего года.

![](../../../../../assets/en/manual/web_interface/availability_bar.png){width="600"}

Зеленая часть столбцов обозначает время в состоянии ОК, красная в
состоянии проблем.

[comment]: # ({/new-75fed46f})





[comment]: # ({new-f7f02710})
#### Using filter

The filter can help narrow down the number of hosts and/or triggers
displayed. For better search performance, data is searched with macros
unresolved.

The filter is located below the *Availability report* bar. It can be
opened and collapsed by clicking on the *Filter* tab on the left.

[comment]: # ({/new-f7f02710})

[comment]: # ({new-51ddfece})
##### Filtering by trigger template

In the *by trigger template* mode results can be filtered by one or
several parameters listed below.

|Parameter|Description|
|---------|-----------|
|*Template group*|Select all hosts with triggers from templates belonging to that group. Any host group that includes at least one template can be selected.|
|*Template*|Select hosts with triggers from the chosen template and all nested templates. Only triggers inherited from the selected template will be displayed. If a nested template has additional own triggers, those triggers will not be displayed.|
|//Template trigger //|Select hosts with chosen trigger. Other triggers of the selected hosts will not be displayed.|
|*Host group*|Select hosts belonging to the group.|

[comment]: # ({/new-51ddfece})

[comment]: # ({new-22214967})
##### Filtering by host

In the *by host* mode results can be filtered by a host or by the host
group. Specifying a parent host group implicitly selects all nested host
groups.

[comment]: # ({/new-22214967})

[comment]: # ({new-0533be8a})
#### Time period selector

The [time period
selector](/manual/config/visualization/graphs/simple#time_period_selector)
allows to select often required periods with one mouse click. The time
period selector can be opened by clicking on the time period tab next to
the filter.

Clicking on *Show* in the Graph column displays a bar graph where
availability information is displayed in bar format each bar
representing a past week of the current year.

![](../../../../../assets/en/manual/web_interface/availability_bar.png){width="600"}

The green part of a bar stands for OK time and red for problem time.

[comment]: # ({/new-0533be8a})
