[comment]: # translation:outdated

[comment]: # ({new-fa188d70})
# 5 Администрирование

[comment]: # ({/new-fa188d70})

[comment]: # ({new-cded1b68})
#### Обзор

Меню Администрирование используется в Zabbix для административных
функций. Это меню доступно только пользователям с типом
[Супер-Администраторы](/ru/manual/config/users_and_usergroups/permissions).

[comment]: # ({/new-cded1b68})
