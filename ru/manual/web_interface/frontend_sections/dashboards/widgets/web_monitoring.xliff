<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="ru" datatype="plaintext" original="manual/web_interface/frontend_sections/dashboards/widgets/web_monitoring.md">
    <body>
      <trans-unit id="54046c3a" xml:space="preserve">
        <source># 24 Web monitoring</source>
      </trans-unit>
      <trans-unit id="060ef5ad" xml:space="preserve">
        <source>#### Overview

This widget displays a status summary of the active web monitoring
scenarios. See the [Web monitoring widget](#web-monitoring-widget) section for detailed information.</source>
      </trans-unit>
      <trans-unit id="d0a8a9db" xml:space="preserve">
        <source>#### Configuration

![](../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/web_monitoring.png){width="600"}

::: noteclassic
 In cases when a user does not have permission to access
certain widget elements, that element's name will appear as
*Inaccessible* during the widget's configuration. This results in
*Inaccessible Item, Inaccessible Host, Inaccessible Group, Inaccessible
Map, and Inaccessible Graph* appearing instead of the "real" name of the
element.
:::

In addition to the parameters that are [common](/manual/web_interface/frontend_sections/dashboards/widgets#common-parameters) 
for all widgets, you may set the following specific options:

|Parameter|Description|
|--|--------|
|*Host groups*|Enter host groups to display in the widget.&lt;br&gt;This field is auto-complete so starting to type the name of a group will offer a dropdown of matching groups.&lt;br&gt;Specifying a parent host group implicitly selects all nested host groups.&lt;br&gt;Host data from these host groups will be displayed in the widget; if no host groups are entered, all host groups will be displayed.&lt;br&gt;This parameter is not available when configuring the widget on a [template dashboard](/manual/config/templates/template#adding-dashboards).|
|*Exclude host groups*|Enter host groups to hide from the widget.&lt;br&gt;This field is auto-complete so starting to type the name of a group will offer a dropdown of matching groups.&lt;br&gt;Specifying a parent host group implicitly selects all nested host groups.&lt;br&gt;Host data from these host groups will not be displayed in the widget. For example, hosts 001, 002, 003 may be in Group A and hosts 002, 003 in Group B as well. If we select to *show* Group A and *exclude* Group B at the same time, only data from host 001 will be displayed in the dashboard.&lt;br&gt;This parameter is not available when configuring the widget on a [template dashboard](/manual/config/templates/template#adding-dashboards).|
|*Hosts*|Enter hosts to display in the widget.&lt;br&gt;This field is auto-complete so starting to type the name of a host will offer a dropdown of matching hosts.&lt;br&gt;If no hosts are entered, all hosts will be displayed.&lt;br&gt;This parameter is not available when configuring the widget on a [template dashboard](/manual/config/templates/template#adding-dashboards).|
|*Scenario tags*|Specify tags to limit the number of web scenarios displayed in the widget.&lt;br&gt;It is possible to include as well as exclude specific tags and tag values. Several conditions can be set. Tag name matching is always case-sensitive.&lt;br&gt;&lt;br&gt;There are several operators available for each condition:&lt;br&gt;**Exists** - include the specified tag names;&lt;br&gt;**Equals** - include the specified tag names and values (case-sensitive);&lt;br&gt;**Contains** - include the specified tag names where the tag values contain the entered string (substring match, case-insensitive);&lt;br&gt;**Does not exist** - exclude the specified tag names;&lt;br&gt;**Does not equal** - exclude the specified tag names and values (case-sensitive);&lt;br&gt;**Does not contain** - exclude the specified tag names where the tag values contain the entered string (substring match, case-insensitive).&lt;br&gt;&lt;br&gt;There are two calculation types for conditions:&lt;br&gt;**And/Or** - all conditions must be met, conditions having the same tag name will be grouped by the *Or* condition;&lt;br&gt;**Or** - enough if one condition is met.|
|*Show hosts in maintenance*|Include hosts that are in maintenance in the statistics.&lt;br&gt;This parameter is labeled *Show data in maintenance* when configuring the widget on a [template dashboard](/manual/config/templates/template#adding-dashboards).|</source>
      </trans-unit>
      <trans-unit id="1ca6cedc" xml:space="preserve">
        <source>#### Web monitoring widget

Once you have completed the configuration, you might like to see the widget with the data it displays.
To do it, go to *Dashboards*, click on the name of a dashboard where you created the widget.

In this example, you can see the widget named "Zabbix frontend" displaying the status of the web monitoring for three host groups:
"Internal network," "Linux servers," and "Web servers."

![](../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/web_monitoring_2.png)

A web monitoring widget displays the following information:

- a name of a widget; below it, there are four columns:
  -  *Host group* - displays a list of host groups that contain hosts having web scenarios configured;
  -  *Ok* - displays a number of web scenarios (in green color) when two conditions are observed:
     - Zabbix has collected the latest data for a web scenario(s);
     - all steps that were configured in a [web scenario](/manual/web_monitoring/example#scenario) are in "Ok" *[Status](/manual/web_monitoring/example#step-3)*.
  -  *Failed* - displays a number of web scenarios (in red color), which have some failed steps:
     - click on the host name, and it will open a new window; the *Status* column provides detailed information (in red color) on the step
       where Zabbix failed to collect the data; and also,
     - gives a hint for the parameter that has to be corrected in the [configuration form](/manual/web_monitoring/example#scenario).

![](../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/web_monitoring_3.png){width="600"}

  -  *Unknown* - displays a number of web scenarios (in grey color) for which Zabbix has neither collected data,
     nor has an information about the failed steps.

![](../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/web_monitoring_4.png){width="600"}</source>
      </trans-unit>
      <trans-unit id="b138a011" xml:space="preserve">
        <source>##### Viewing the status and data

Clickable links in the widget allow to easily navigate and quickly acquire a full information on each web scenario.
Thus, to view:

-  the [Status](/manual/web_monitoring/example#step-3) of a web scenario, click on the name of a host group.
-  more detailed statistics, click on the scenario name. In this example it is "Zabbix frontend".
-  the details in the case of *Failed* status, click on a host group name; in the window that opens,
   click on a web scenario name in the *Name* column; it will open more detailed information on the configured steps for which Zabbix failed to collect the data.

![](../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/web_monitoring_5.png){width="600"}

Now, you can return to the [web scenario configuration form](/manual/web_monitoring/example#scenario) and correct your settings.

To view the details in the case of *Unknown* status, you can repeat the same steps as explained for *Failed*.

::: noteimportant
At the first monitoring instance, a web scenario is always displayed in *Unknown* state,
which is switched to *Failed* or *Ok* state right after the first check.
In the case when a host is monitored by the proxy,
the status change occurs in accordance with the data collection frequency configured on the proxy.
:::</source>
      </trans-unit>
    </body>
  </file>
</xliff>
