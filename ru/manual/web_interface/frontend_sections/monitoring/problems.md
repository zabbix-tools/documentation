[comment]: # translation:outdated

[comment]: # ({new-621a60b9})
# 2 Проблемы

[comment]: # ({/new-621a60b9})

[comment]: # ({new-d98c719a})
#### Обзор

В разделе *Мониторинг → Проблемы* вы можете увидеть какие проблемы
сейчас у вас имеются. Проблемами называются те триггеры, которые в
состоянии "Проблема".

![](../../../../../assets/en/manual/web_interface/problems2.png){width="600"}

|Колонка|Описание|
|--------------|----------------|
|*Время*|Время начала проблемы.|
|*Важность*|Важность проблемы.<br>Важность проблемы изначально основывается на важности основного триггера проблемы, однако, после того как событие произошло важность проблемы можно обновить, используя [экран](/ru/manual/acknowledges#обновление_проблем) *Обновление проблемы*. Цвет важности проблемы используется фоном ячейки в течении времени проблемы.|
|*Время восстановления*|Время решения проблемы.|
|*Состояние*|Состояние проблемы:<br>**Проблема** - нерешенная проблема<br>**Решено** - недавно решенная проблема. Вы можете скрыть недавно решенные проблемы при помощи фильтра.<br>Новые и недавно решенные проблемы мигают в течении 2 минуты. Решенные проблемы в целом отображаются в течении 5 минут. Оба этих значения настраиваются в *Администрировании* → *Общие* → *[Опции отображения триггеров](/ru/manual/web_interface/frontend_sections/administration/general#опции_отображения_триггеров)*.|
|*Инфо*|Зеленая информационная иконка, если проблема закрыта глобальной корреляцией или вручную при обновлении проблемы. При наведении курсора мыши на иконку будут доступны более детальные сведения:<br>![info.png](../../../../../assets/en/manual/web_interface/info.png)<br>Следующая иконка появляется, если отображаются подавленные проблемы (смотрите в фильтре *Отображение подавленных проблем* опцию). При наведении курсора мыши на иконку будут доступны более детальные сведения:<br>![](../../../../../assets/en/manual/web_interface/info_suppressed2.png)|
|*Узел сети*|Узел сети с проблемой.|
|*Проблема*|Имя проблемы.<br>Имя проблемы основывается от имени основного триггера проблемы.|
|*Длительность*|Длительность проблемы.<br>Смотрите также: [отрицательная длительность проблем](#отрицательная длительность проблем)|
|*Подтверждение*|Состояние подтверждения проблемы:<br>**Да** - зеленый текст сигнализирует о том, что проблема подтверждена. Проблема считается подтверждённой, если все её события подтверждены.<br>**Нет** - красная ссылка сигнулизирует о неподтвержденных событиях.<br>Если вы нажмёте на эту ссылку, вы попадёте на экран [обновления проблемы](/ru/manual/acknowledges#обновление_проблем), где с проблемой можно выполнить различные действия, включая комментирование и подтверждение проблемы.|
|*Действия*|История действий над проблемой в виде символических иконок:<br>![icon\_comment.png](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/icon_comment.png) - добавлены комментарии. Также отображается количество комментариев.<br>![icon\_sev\_up1.png](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/icon_sev_up1.png) - важность проблемы увеличена (например, Информационный → Предупреждение)<br>![icon\_sev\_down1.png](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/icon_sev_down1.png) - важность проблемы уменьшена (например, Предупреждение → Информационный)<br>![icon\_severity\_back.png](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/icon_severity_back.png) - важность проблемы изменена, но затем вернулась к изначальному уровню (например, Предупреждение → Информационный → Предупреждение)<br>![icon\_actions.png](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/icon_actions.png) - выполнены действия. Tакже отображается количество действий.<br>![icon\_actions\_progress1.png](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/icon_actions_progress1.png) - выполнены действия, по крайней мере одно в процессе выполнения. Tакже отображается количество действий.<br>![icon\_actions\_failed.png](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/icon_actions_failed.png) - выполнены действия, по крайней одно завершилось с ошибкой. Tакже отображается количество действий.<br>При наведении курсора мыши на иконки, отобразится всплывающее окно с детальной информацией о действиях. Смотрите [просмотр деталей](#просмотр_деталей) для объяснений используемых иконках во всплывающем меню по предпринятым действиям.|
|*Теги*|[Теги события](/ru/manual/config/event_correlation/trigger/event_tags) (если имеются).|

[comment]: # ({/new-d98c719a})

[comment]: # ({new-8fa0f11a})
#### Отрицательная длительность проблемы

Фактически имеется вероятность в некоторых распространенных ситуациях
наличие отрицательной длительности проблемы то есть, когда время решения
наступает раньше времени создания проблемы, например:

-   Если какой-либо узел сети наблюдается прокси и происходит сетевая
    ошибка, которая приводит к тому, что данные от прокси не поступают
    некоторое время, тогда триггер item.nodata() сработает на стороне
    сервера. Когда соединение восстановится, сервер получит данные по
    элементу данных от прокси, у этих данных будет время из прошлого.
    Тогда проблема item.nodata() будет решена и у нее будет
    отрицательная длительность проблемы;
-   Когда данные элемента данных, которые решают событие проблемы
    поступили от Zabbix sender и содержат штамп времени более старый чем
    время создания проблемы, в этом случае также отобразится
    отрицательная длительность проблемы.

[comment]: # ({/new-8fa0f11a})

[comment]: # ({new-1848ebdc})
##### Опции массового редактирования

Кнопки ниже списка предлагают некоторые опции массового редактирования:

-   *Массовое обновление* - обновление выбранных проблем с переходом к
    экрану [обновление
    проблемы](/manual/acknowledges#обновление_проблем)

Для использования этой опции, отметьте соответствующие проблемы, затем
нажмите на кнопку *Массовое обновление*.

[comment]: # ({/new-1848ebdc})

[comment]: # ({new-462c4f50})
##### Кнопки

Кнопки справа предоставляют следующие опции:

|   |   |
|---|---|
|![](../../../../../assets/en/manual/web_interface/button_csv.png)|Экспорт содержимого страницы в CSV.|
|![](../../../../../assets/en/manual/web_interface/button_fullscreen.png)|Отображение страницы в полноэкранном режиме.|
|![](../../../../../assets/en/manual/web_interface/button_kiosk.png)|Отображение страницы в режиме киоска. В этом режиме отображается только содержимое страницы.<br>Кнопка режима киоска появляется после активации полноэкранного режима.<br>Чтобы выйти из режима киоска, переместите курсор до появления ![](../../../../../assets/en/manual/web_interface/button_kiosk_leave.png) кнопки выхода и нажмите на неё. Обратите внимание, что вы вернётесь в нормальный режим (не в полноэкранный режим).|

[comment]: # ({/new-462c4f50})

[comment]: # ({new-eaadfbbc})
##### Использование фильтра

Вы можете воспользоваться фильтром, чтобы отображать только те проблемы
в которых вы заинтересованы. Фильтр располагается над таблицой.

![](../../../../../assets/en/manual/web_interface/problems_filter2.png){width="600"}

|Параметр|Описание|
|----------------|----------------|
|*Показать*|Фильтр по состоянию проблем:<br>**Недавние проблемы** - нерешенные и недавно решенные проблемы (по умолчанию)<br>**Проблемы** - нерешенные проблемы<br>**История** - история всех событий|
|*Группы узлов сети*|Фильтр по одному или нескольким группам узлов сети.<br>Указывая родительскую группу узлов сети косвенным образом будут выбраны все вложенные группы узлов сети.|
|*Узлы сети*|Фильтр по одному или нескольким узлам сети.|
|*Группа элементов данных*|Фильтр по имени группы элементов данных.|
|*Триггеры*|Фильтр по одному или нескольким триггерам.|
|*Проблема*|Фильтр по имени проблемы.|
|*Минимальная важность*|Фильтр по минимальной важности триггеров (проблем).|
|*Инвентарные данные узла сети*|Фильтр по типу инвентарных данных и значению.|
|*Теги*|Фильтр по имени тега событий и значению тега событий.<br>Можно задать несколько условий. Имеется два типа вычислений для нескольких условий:<br>**И/Или** - все условия совпадают, условия у которых имеется одинаковое имя тега будут сгруппированы по условию Или<br>**Или** - достаточно совпадения одного условия<br>Имеется два способа соответствия значения тегов:<br>**Содержит** - регистрозависимое соответствие подстроке<br>**Равно** - регистрозависимое соответствие строке<br>При фильтрации теги указанные здесь будут сначала отображены с проблемой, если только не предопределены списком *Приоритет отображения тегов* (см. ниже).|
|*Отображать теги*|Выбор количества отображаемых тегов:<br>**Нет**- колонка *Теги* отсутствует в *Мониторинг → Проблемы*<br>**1**- колонка *Теги* содержит один тег<br>**2**- колонка *Теги* содержит два тега<br>**3**- колонка *Теги* содержит три тега<br>Чтобы увидеть все теги по проблеме наведите курсор мыши на иконку с тремя точками.|
|*Имя тега*|Выберите режим отображения имени тега:<br>**Полный** - имена тегов и их значения отображаются в полном формате<br>**Сокращённое** - имена тегов сокращены до 3 символов; значения тегов отображаются в полном формате<br>\*\* Нет\*\* - отображаются только значения тегов; без имён|
|*Приоритет отображения тегов*|Введите приоритет отображения тегов для проблемы, в виде разделённого запятыми списка тегов (например: `Сервисы,Приложения,Приложение`). Необходимо использовать только имена тегов, не значения. Теги из этого списка всегда отображаются первыми, переопределяя естественную сортировку по алфавиту.|
|*Отображение подавленных проблем*|Отметьте, чтобы отображались проблемы, которые в противном случае были бы подавлены (не показаны) по причине обслуживания узлов сети.|
|*Компактный вид*|Активация компактного вида.|
|*Показывать детали*|Отметьте, чтобы по проблемам отображались выражения основных триггеров. Деактивировано, если выбран параметр *Компактный вид*.|
|*Отображать только неподтвержденные*|Отметьте, чтобы отображались только неподтвержденные проблемы.|
|*Отображать выбор времени*|Отметьте, чтобы отображались визуальный выбор времени и группировка. Деактивировано, если выбран параметр *Компактный вид*.|
|*Подсвечивать всю строку*|Отметьте, чтобы по нерешенным проблемам подсвечивалась вся строка. Для подсветки используется цвет важности проблемы.<br>Активно только, если выбран параметр *Компактный вид* в стандартных синей и темной темах. *Подсвечивать всю строку* недоступен в высоко-контрастных темах.|

[comment]: # ({/new-eaadfbbc})

[comment]: # ({new-5dda91fc})
#### Просмотр деталей

В *Мониторинг → Проблемы* поля времени начала проблемы и восстановления
являются ссылками. При нажатии на них, откроется более детальная
информация по событию.

![](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/event_details_new1.png){width="600"}

Обратите внимание на то, как важность проблемы отличается у триггера и
события о проблеме - у события о проблеме цвет был обновлен с
использованием [экрана](/ru/manual/acknowledges#обновление_проблем)
*Обновление проблемы*.

В списке действий для обозначения типов действий используются следующие
иконки:

-   ![icon\_generated.png](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/icon_generated.png) -
    сгенерировано событие о проблеме
-   ![icon\_message.png](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/icon_message.png) -
    сообщение отправлено
-   ![icon\_acknowledged.png](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/icon_acknowledged.png) -
    событие о проблеме подтверждено
-   ![icon\_comment2.png](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/icon_comment2.png) -
    добавлен комментарий
-   ![icon\_sev\_up1.png](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/icon_sev_up1.png) -
    увеличена важность проблемы (например, Информационный →
    Предупреждение)
-   ![icon\_sev\_down1.png](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/icon_sev_down1.png) -
    уменьшена важность проблемы (например, Предупреждение →
    Информационный)
-   ![icon\_severity\_back.png](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/icon_severity_back.png) -
    важность проблемы изменилась, но затем вернулась к изначальному
    уровню (например, Предупреждение → Информационный → Предупреждение)
-   ![icon\_remote.png](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/icon_remote.png) -
    выполнена удаленная команда
-   ![icon\_recovery.png](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/icon_recovery.png) -
    восстановлено событие о проблеме
-   ![icon\_closed.png](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/icon_closed.png) -
    проблема закрыта вручную

[comment]: # ({/new-5dda91fc})





[comment]: # ({new-6c375926})
##### Tabs for favorite filters

Frequently used sets of filter parameters can be saved in tabs.

To save a new set of filter parameters, open the Home tab, and configure
the filter settings, then press the *Save as* button. In a new popup
window, define *Filter properties*.

![problem\_filter0.png](../../../../../assets/en/manual/web_interface/problem_filter0.png)

|Parameter|Description|
|---------|-----------|
|Name|The name of the filter to display in the tab list.|
|Show number of records|Check, if you want the number of problems to be displayed next to the tab name.|
|Set custom time period|Check to set specific default time period for this filter set. If set, you will only be able to change the time period for this tab by updating filter settings. For tabs without a custom time period, the time range can be changed by pressing the time selector button in the top right corner (button name depends on selected time interval: This week, Last 30 minutes, Yesterday, etc.).<br>This option is available only for filters in *Monitoring→Problems*.|
|From/To|[Time period](/manual/config/visualization/graphs/simple#time_period_selector) start and end in absolute (`Y-m-d H:i:s`) or relative time syntax (`now-1d`). Available, if *Set custom time period* is checked.|

To edit *Filter properties* of an existing filter, press the gear symbol
next to the active tab name.

![problem\_filter2.png](../../../../../assets/en/manual/web_interface/problem_filter2.png)

Notes:

-   To hide a filter, press on the name of the current tab. Press on the
    active tab name again to open the filter.
-   Filter tabs can be re-arranged by dragging and dropping.
-   Keyboard navigation is supported: use arrows to switch between tabs,
    press *Enter* to open.
-   Pressing the arrow down icon in the upper right corner will open the
    full list of saved filter tabs as a drop-down menu.

::: noteclassic
 To share filters, copy and send to others a URL of an
active filter. After opening this URL, other users will be able to save
this set of parameters as a permanent filter in their Zabbix account.\
See also: [Page
parameters](/manual/web_interface/page_parameters).
:::

[comment]: # ({/new-6c375926})

[comment]: # ({new-75aa326d})
##### Filter buttons

|   |   |
|---|---|
|![filter\_apply.png](../../../../../assets/en/manual/web_interface/filter_apply.png)|Apply specified filtering criteria (without saving).|
|![filter\_reset.png](../../../../../assets/en/manual/web_interface/filter_reset.png)|Reset current filter and return to saved parameters of the current tab. On the Home tab, this will clear the filter.|
|![filter\_save\_as.png](../../../../../assets/en/manual/web_interface/filter_save_as.png)|Save current filter parameters in a new tab. Only available on the *Home* tab.|
|![filter\_update.png](../../../../../assets/en/manual/web_interface/filter_update.png)|Replace tab parameters with currently specified parameters. Not available on the *Home* tab.|

[comment]: # ({/new-75aa326d})


[comment]: # ({new-bd839887})
#### Viewing details

The times for problem start and recovery in *Monitoring → Problems* are
links. Clicking on them opens more details of the event.

![](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/event_details.png){width="600"}

Note how the problem severity differs for the trigger and the problem
event - for the problem event it has been updated using the *Update
problem* [screen](/manual/acknowledges#updating_problems).

In the action list, the following icons are used to denote the activity
type:

-   ![icon\_generated.png](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/icon_generated.png) -
    problem event generated
-   ![icon\_message.png](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/icon_message.png) -
    message has been sent
-   ![icon\_acknowledged.png](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/icon_acknowledged.png) -
    problem event acknowledged
-   ![icon\_unacknowledged.png](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/icon_unacknowledged.png) -
    problem event unacknowledged
-   ![icon\_comment2.png](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/icon_comment2.png) -
    a comment has been added
-   ![icon\_sev\_up1.png](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/icon_sev_up1.png) -
    problem severity has been increased (e.g. Information → Warning)
-   ![icon\_sev\_down1.png](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/icon_sev_down1.png) -
    problem severity has been decreased (e.g. Warning → Information)
-   ![icon\_severity\_back.png](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/icon_severity_back.png) -
    problem severity has been changed, but returned to the original
    level (e.g. Warning → Information → Warning)
-   ![icon\_remote.png](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/icon_remote.png) -
    a remote command has been executed
-   ![icon\_recovery.png](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/icon_recovery.png) -
    problem event has recovered
-   ![icon\_closed.png](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/icon_closed.png) -
    the problem has been closed manually

[comment]: # ({/new-bd839887})
