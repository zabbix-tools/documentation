[comment]: # translation:outdated

[comment]: # ({new-cd71b995})
# 2 Веб-сценарии

[comment]: # ({/new-cd71b995})

[comment]: # ({new-6c7a491c})
#### Обзор

Информацию о [веб-сценариях](ru/manual/web_monitoring) узла сети можно
получить из раздела *Мониторинг → Узлы сети*, нажав на ссылку Веб в
строке соответствующего узла сети.

![](../../../../../../assets/en/manual/web_interface/web_monitoring.png){width="600"}

На странице отображается список всех веб-сценариев выбранного узла сети.
Чтобы просмотреть веб-сценарии для другого узла сети или группы узлов,
не возвращаясь на страницу *Мониторинг → Узлы сети*, выберите этот узел
сети или группу в фильтре.

Данные по деактивированным узлам сети также доступны. Имя
деактивированного узла сети отображается красным цветом.

Максимальное количество сценариев, отображаемых на странице,
определяется параметром *Строк на странице* в
[настройках](/ru/manual/web_interface/user_profile#configuration)
профиля пользователя.

По умолчанию отображаются только те значения, которые были собраны за
последние 24 часа. Такое ограничение введено с целью улучшения скорости
изначальной загрузки больших страниц веб-мониторинга. Также имеется
возможность изменения этого ограничения, изменив значение
ZBX\_HISTORY\_PERIOD [константы](/ru/manual/web_interface/definitions) в
*include/defines.inc.php*.

Имя сценария является ссылкой к более подробной статистике по этому
сценарию:

![](../../../../../../assets/en/manual/web_monitoring/scenario_details2.png){width="600"}

[comment]: # ({/new-6c7a491c})

[comment]: # ({new-6e8379a0})
##### Кнопки

Кнопки режима просмотра, общие для всех разделов, описаны на странице
[Мониторинг](/ru/manual/web_interface/frontend_sections/monitoring#view_mode_buttons).

[comment]: # ({/new-6e8379a0})


[comment]: # ({new-84099469})
##### Buttons

View mode buttons being common for all sections are described on the
[Monitoring](/manual/web_interface/frontend_sections/monitoring#view_mode_buttons)
page.

[comment]: # ({/new-84099469})
