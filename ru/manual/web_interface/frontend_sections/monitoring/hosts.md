[comment]: # translation:outdated

[comment]: # ({new-ae6d2ebd})
# 3 Узлы сети

[comment]: # ({/new-ae6d2ebd})

[comment]: # ({new-26f6fa20})
#### Обзор

В разделе *Мониторинг → Узлы сети* отображается полный список
отслеживаемых узлов сети с подробной информацией об интерфейсе узла
сети, доступности, тегах, текущих проблемах, состоянии
(активировано/деактивировано) и ссылки для быстрой навигации к последним
данным узла сети, истории проблем, графикам, экранам и веб-сценариям.

![](../../../../../assets/en/manual/web_interface/monitoring_hosts0.png){width="600"}

|Столбец|Описание|
|--------------|----------------|
|*Имя*|Видимое имя узла сети. При нажаии на имя появится [меню узла сети](/ru/manual/web_interface/frontend_sections/monitoring/dashboard#меню_у_узлов_сети).<br>Оранжевый значок гаечного ключа ![](../../../../../assets/en/manual/web_interface/frontend_sections/configuration/maintenance_wrench_icon.png) после имени указывает, что этот узел сети находится в режиме обслуживания.<br>Нажатие на строку заголовка в этом столбце приведет к сортировке узлов сети по имени в алфавитном порядке / порядке возрастания (по умолчанию), а при повторном нажатии - в обратном порядке.|
|*Интерфейс*|Отображается основной интерфейс узла сети.|
|*Доступность*|Отображается [доступность](/manual/web_interface/frontend_sections/configuration/hosts#reading_host_availability) узла сети. Каждая из четырех иконок представляет поддерживаемый интерфейс (Zabbix агент, SNMP, IPMI, JMX).<br>Текущее состояние интерфейса отображается соответствующим цветом:<br>**Зеленый** - доступен<br>**Красный** - недоступен (при наведении курсора отображаются сведения о том, почему интерфейс недоступен)<br>**Серый** - неизвестно или не настроено<br>Обратите внимание, что активные элементы данных Zabbix агента не влияют на доступность узла сети.|
|*Теги*|[Теги](/manual/config/event_correlation/trigger/event_tags) узла сети и все связанные шаблоны, макросы нераскрыты.|
|*Проблемы*|Квадратные значки, которые показывают текущие открытые проблемы.<br>Цвет значка указывает на уровень важности проблемы. Число на значке означает количество проблем данной важности.<br>Используйте фильтр, чтобы выбрать, следует ли включать подавленные проблемы (не включены по умолчанию).|
|*Состояние*|отображается состояние узла сети: *Активровано* или *Деактивировано*.<br>Нажатие на строку заголовка в этом столбце будет сортировать узлы сети по состоянию в порядке возрастания или убывания.|
|//Последние данные //|При нажатии на ссылку откроется страница *Мониторинг - Последние данные* со всеми последними данными, полученными от этого узла сети.|
|*Проблемы*|При нажатии на ссылку откроется раздел *Мониторинг - Проблемы* с настроенным фильтром для отображения только проблем данного узла сети.|
|*Графики*|При нажатии на ссылку отобразятся графики, настроенные для узла сети. Количество графиков показано серым цветом.<br>Если у узла сети нет графиков, ссылка отключена (серый текст) и номер не отображается.|
|*Комплексные экраны*|При нажатии на ссылку отобразятся комплексные экраны, настроенные для узла сети. Количество экранов отображается серым цветом.<br>Если у узла сети нет экранов, ссылка отключена (серый текст) и номер не отображается.|
|*Веб*|При нажатии на ссылку отобразятся веб-сценарии, настроенные для узла. Количество веб-сценариев отображается серым цветом.<br>Если у узла сети нет веб-сценариев, ссылка отключена (серый текст) и номер не отображается.|

[comment]: # ({/new-26f6fa20})

[comment]: # ({new-bd285dea})
##### Кнопки

Кнопки режима просмотра, общие для всех разделов, описаны на странице
[Мониторинг](/ru/manual/web_interface/frontend_sections/monitoring#view_mode_buttons).

[comment]: # ({/new-bd285dea})

[comment]: # ({new-159cadac})
##### Использование фильтра

Вы можете использовать фильтр для отображения только интересующих вас
узлов сети. Фильтр расположен над таблицей. Можно фильтровать узлы сети
по имени, группе узлов сети, IP или DNS, порту интерфейса, тегам,
важности проблемы, состоянию (активировано/деактивировано/любое); Вы
также можете выбрать, отображать ли подавленные проблемы и хосты,
которые в данный момент находятся на обслуживании.

![](../../../../../assets/en/manual/web_interface/monitoring_hosts_filter.png){width="600"}

Обратите внимание, что:

-   Поля *Имя* и *Группа узлов сети* поддерживают несколько записей.\
-   При указании родительской группы узлов сети косвенным образом будут
    выбраны все вложенные группы узлов сети.\
-   По умолчанию отображаются проблемы всех уровней важности, если они
    не подавлены.\
-   По умолчанию узлы сети в режиме обслуживания отображаются, но
    подавленные проблемы на этих узлах сети не отображаются.\
-   Если используется фильтрация по важности проблем и флажок
    *Подавленные проблемы* не установлен, узлы сети с подавленными
    проблемами указанной важности не будут отображаться.\
-   Узлы сети могут быть отфильтрованы по тегам уровня узла сети, а
    также по тегам из всех связанных шаблонов, включая родительские
    шаблоны.

[comment]: # ({/new-159cadac})
