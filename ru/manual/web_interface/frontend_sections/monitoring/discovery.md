[comment]: # translation:outdated

[comment]: # ({new-a637692c})
# 9 Обнаружение

[comment]: # ({/new-a637692c})

[comment]: # ({new-6877d127})
#### Обзор

В разделе *Мониторинг → Обнаружение* отображаются результаты [сетевого
обнаружения](/ru/manual/discovery/network_discovery). Обнаруженные
устройства отсортированы по правилу обнаружения.

![](../../../../../assets/en/manual/web_interface/discovery_status.png){width="600"}

Если это устройство уже наблюдалось, его имя узла сети будет значиться в
колонке *Наблюдаемый узел сети*, и длительность с момента обнаружения
или с момента потери после предыдущего обнаружения будут отображаться в
колонке *Доступен/Недоступен*.

После этих колонок следующие отображают отдельные состояния сервисов
каждого обнаруженного устройства (зеленые ячейки отображают сервисы,
которые доступны, красные ячейки - сервисы, которые недоступны). Время
доступности или время недоступности включены в саму ячейку.

::: noteimportant
Только те сервисы, которые найдены по крайней мере
на одном устройстве, будут иметь колонку, которая отобразит состояние
этих сервисов.
:::

[comment]: # ({/new-6877d127})

[comment]: # ({new-84099469})
##### Кнопки

Кнопки справа предоставляют следующие опции:

|   |   |
|---|---|
|![](../../../../../assets/en/manual/web_interface/button_fullscreen.png)|Отображение страницы в полноэкранном режиме.|
|![](../../../../../assets/en/manual/web_interface/button_kiosk.png)|Отображение страницы в режиме киоска. В этом режиме отображается только содержимое страницы.<br>Кнопка режима киоска появляется после активации полноэкранного режима.<br>Чтобы выйти из режима киоска, переместите курсор до появления ![](../../../../../assets/en/manual/web_interface/button_kiosk_leave.png) кнопки выхода и нажмите на неё. Обратите внимание, что вы вернётесь в нормальный режим (не в полноэкранный режим).|

[comment]: # ({/new-84099469})


[comment]: # ({new-8b46b0e3})
##### Using filter

You can use the filter to display only the discovery rules you are
interested in. For better search performance, data is searched with
macros unresolved.

With nothing selected in the filter, all enabled discovery rules are
displayed. To select a specific discovery rule for display, start typing
its name in the filter. All matching enabled discovery rules will be
listed for selection. More than one discovery rule can be selected.

[comment]: # ({/new-8b46b0e3})
