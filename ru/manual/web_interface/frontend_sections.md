[comment]: # translation:outdated

[comment]: # ({new-226b9a29})
# 1 Разделы веб-интерфейса

[comment]: # ({/new-226b9a29})

[comment]: # ({new-c3f360a5})

### Menu structure

The Zabbix frontend menu has the following structure:

   * Dashboards
   * Monitoring
       * Problems
       * Hosts
       * Latest data
       * Maps
       * Discovery
   * Services
       * Services
       * SLA
       * SLA report
   * Inventory
       * Overview
       * Hosts
   * Reports
       * System information
       * Scheduled reports
       * Availability report
       * Triggers top 100
       * Audit log
       * Action log
       * Notifications
   * Data collection
       * Template groups
       * Host groups
       * Templates
       * Hosts
       * Maintenance
       * Event correlation
       * Discovery
   * Alerts
       * Actions
           * Trigger actions
           * Service actions
           * Discovery actions
           * Autoregistration actions
           * Internal actions
       * Media types
       * Scripts
   * Users
       * User groups
       * User roles
       * Users
       * API tokens
       * Authentication
   * Administration
       * General
           * GUI
           * Autoregistration
           * Images
           * Icon mapping
           * Regular expressions
           * Trigger displaying options
           * Geographical maps
           * Modules
           * Other
       * Audit log
       * Housekeeping
       * Proxies
       * Macros
       * Queue
           * Queue overview
           * Queue overview by proxy
           * Queue details

[comment]: # ({/new-c3f360a5})
