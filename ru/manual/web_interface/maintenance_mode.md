[comment]: # translation:outdated

[comment]: # ({new-8fc97588})
# 4 Режим обслуживания веб-интерфейса

[comment]: # ({/new-8fc97588})

[comment]: # ({new-fca7c17f})
#### Обзор

Имеется возможность временного отключения веб-интерфейса Zabbix, для
того чтобы запретить доступ извне к веб-интерфейсу. Такая возможность
может быть полезна для защиты базы данных Zabbix от любых изменений,
инициированных пользователями, таким образом сохраняется целостность
базы данных.

Пока Zabbix веб-интерфейс находится в режиме обслуживания можно
останавливать базу данных Zabbix и выполнять задачи по обслуживанию
инсталляции.

Пользователи из указанного диапазона IP адресов будут иметь возможность
нормально работать с веб-интерфейсом в процессе обслуживания.

[comment]: # ({/new-fca7c17f})

[comment]: # ({new-dcfd8b13})
#### Настройка

Чтобы включить режим обслуживания, необходимо изменить файл
`maintenance.inc.php` (расположенный в папке/conf документов Zabbix HTML
на веб-сервере). Раскомментируйте следующие строки:

    // Maintenance mode.
    define('ZBX_DENY_GUI_ACCESS',1);

    // Array of IP addresses, which are allowed to connect to frontend (optional).
    $ZBX_GUI_ACCESS_IP_RANGE = array('127.0.0.1');

    // Message shown on warning screen (optional).
    $ZBX_GUI_ACCESS_MESSAGE = 'Мы обновляем базу данных MySQL до 15:00. Оставайтесь с нами...';

|Параметр|Детали|
|----------------|------------|
|**ZBX\_DENY\_GUI\_ACCESS**|Включение режима обслуживания:<br>1 – режим обслуживания включен, в противном случае отключен|
|**ZBX\_GUI\_ACCESS\_IP\_RANGE**|Соединения с этих IP адресов будут разрешены в течении режима обслуживания.<br>Например:<br>`array('192.168.1.1', '192.168.1.2')`|
|**ZBX\_GUI\_ACCESS\_MESSAGE**|Сообщение, которое вы можете ввести для информирования пользователей об обслуживании.|

[comment]: # ({/new-dcfd8b13})

[comment]: # ({new-ae331777})

::: notetip
Mostly the `maintenance.inc.php` file is located in `/conf` of Zabbix HTML document directory on the
web server. However, the location of the directory may differ depending on the operating system and a web server it uses.

For example, the location for:

-  SUSE and RedHat is `/etc/zabbix/web/maintenance.inc.php`.
-  Debian-based systems is `/usr/share/zabbix/conf/`.

See also [Copying PHP files](/manual/installation/install#copying-php-files). 
:::

|Parameter|Details|
|--|--------|
|**ZBX\_DENY\_GUI\_ACCESS**|Enable maintenance mode:<br>1 – maintenance mode is enabled, disabled otherwise|
|**ZBX\_GUI\_ACCESS\_IP\_RANGE**|Array of IP addresses, which are allowed to connect to frontend (optional).<br>For example:<br>`array('192.168.1.1', '192.168.1.2')`|
|**ZBX\_GUI\_ACCESS\_MESSAGE**|A message you can enter to inform users about the maintenance (optional).|

Note that the [location](/manual/installation/install#copying-php-files) of the `/conf` directory will vary based on the operating system and web server. 

[comment]: # ({/new-ae331777})

[comment]: # ({new-987fdc44})
#### Отображение

При попытке входа на веб-интерфейс Zabbix будет отображаться следующий
экран в течении всего режима обслуживания. Экран обновляется каждые 30
секунд, чтобы возвратиться в нормальное состояние без привлечения
внимания пользователя после того как обслуживание завершится.

![](../../../assets/en/manual/web_interface/frontend_maintenance.png)

IP адреса заданные в ZBX\_GUI\_ACCESS\_IP\_RANGE смогут получить доступ
к веб-интерфейсу без каких-либо ограничений.

[comment]: # ({/new-987fdc44})
