[comment]: # translation:outdated

[comment]: # ({new-bd6224b0})
# 1 Настройка правила сетевого обнаружения

[comment]: # ({/new-bd6224b0})

[comment]: # ({new-466f5982})
#### Обзор

Для того чтобы настроить правило обнаружения сети, используемое в Zabbix
для обнаружения узлов сети и сервисов:

-   Перейдите в *Настройка → Обнаружение*
-   Нажмите на *Создать правило* (или на имя существующего правила,
    чтобы изменить его)
-   Измените атрибуты правила обнаружения

[comment]: # ({/new-466f5982})

[comment]: # ({new-f020a6f3})
#### Атрибуты правил

![d\_rule.png](../../../../assets/en/manual/discovery/network_discovery/d_rule.png)

|Параметр|Описание|
|----------------|----------------|
|*Имя*|Уникальное имя правила. Например, "Локальная сеть".|
|*Обнаружение через прокси*|Что осуществляет обнаружение:<br>**без прокси** - обнаружение выполняет Zabbix сервер<br>**<имя прокси>** - обнаружение осуществляется указанным прокси|
|*Диапазон IP адресов*|Диапазон IP адресов обнаружения. Может принимать следующие форматы:<br>Один IP: 192.168.1.33<br>Диапазон IP адресов: 192.168.1-10.1-255. Диапазон ограничен общим количеством покрываемых адресов (менее чем 64К).<br>Маска IP: 192.168.4.0/24<br>поддерживаемые маски IP:<br>/16 - /30 для IPv4 адресов<br>/112 - /128 для IPv6 адресов<br>Список: 192.168.1.1-255, 192.168.2.1-100, 192.168.2.200, 192.168.4.0/24<br>Начиная с Zabbix 3.0.0 это поле поддерживает пробелы, символы табуляции и многострочность.|
|*Интервал обновления*|Этот параметр определяет как часто Zabbix будет выполнять это правило.<br>Задержка отсчитывается после того, как завершится выполнение предыдущего процесса обнаружения, таким образом перекрытия не произойдет.<br>Начиная с 3.4.0, поддерживаются [суффиксы времени](/ru/manual/appendix/suffixes), например, 30s, 1m, 2h, 1d.<br>[Пользовательские макросы](/ru/manual/config/macros/usermacros) поддерживаются начиная с Zabbix 3.4.0.<br>*Обратите внимание* что, если используется пользовательский макрос и его значение изменилось (к примеру, 1w → 1h), следующая проверка будет выполнена в соответствии с предыдущим значением (в далеком будущем с примерами значений).|
|*Проверки*|Zabbix будет использовать этот список проверок для обнаружения.<br>Поддерживаемые проверки: SSH, LDAP, SMTP, FTP, HTTP, HTTPS, POP, NNTP, IMAP, TCP, Telnet, Zabbix агент, SNMPv1 агент, SNMPv2 агент, SNMPv3 агент, ICMP ping.<br>Обнаружение, основанное на протоколах, использует функционал **net.tcp.service\[\]** для тестирования каждого узла сети, исключая SNMP, который выполняет запрос, используя SNMP OID. Zabbix агент тестируется запросом элемента данных. Пожалуйста, смотрите [элементы данных агентов](/ru/manual/config/items/itemtypes/zabbix_agent) для получения подробностей.<br>Параметр 'Порты' может принимать следующие значения:<br>Один порт: 22<br>Диапазон портов: 22-45<br>Список: 22-45,55,60-70|
|*Критерий уникальности устройства*|Критерием уникальности может быть:<br>**IP адрес** - без обработки нескольких устройств с одним IP адресом. Если устройство с таким же IP существует, то оно будет считаться уже обнаруженным и новый узел сети не будет добавлен.<br>**Тип проверки обнаружения** - либо SNMP, либо Zabbix агента проверка.|
|*Активировано*|При отмеченном правиле, оно является активным и обрабатывается Zabbix сервером.<br>Если правило не отмечено, оно неактивно. Таким образом не обрабатывается.|

[comment]: # ({/new-f020a6f3})

[comment]: # ({new-cd84714a})
#### Изменения настройки прокси

Начиная с Zabbix 2.2.0 узлы сети обнаруженные разными zabbix прокси
всегда воспринимаются как разные узлы сети. До тех пор пока разрешено
осуществлять поиск по совпадающим IP диапазонам в разных подсетях,
изменение прокси для подсети уже находящейся под наблюдением усложняется
тем, что все изменения необходимо также применить ко всем уже
обнаруженным узлам сети. Примерный план для замены прокси в правиле
обнаружения:

1.  деактивировать правило обнаружения
2.  синхронизировать конфигурацию прокси
3.  заменить Zabbix прокси в соответсвующем правиле обнаружения
4.  заменить zabbix прокси на всех узлах, обнаруженных соответствующим
    правилом
5.  активировать правило обнаружения

[comment]: # ({/new-cd84714a})

[comment]: # ({new-78f4f491})
#### Сценарий из реальной жизни

Допустим, мы хотим настроить обнаружение для локальной сети, имеющей
диапазон IP адресов 192.168.1.1-192.168.1.254.

В нашем сценарии мы хотим получить:

-   обнаружение тех узлов сети, на которых имеется Zabbix агент
-   выполнение обнаружения каждые 10 минут
-   добавление узла сети для наблюдения, если время работы узла сети
    более 1 часа
-   удаление узла сети, если узел сети недоступен на протяжении более 24
    часов
-   добавление узлов сети Linux в группу "Linux servers"
-   добавление узлов сети Windows в группу "Windows servers"
-   использование *Template OS Linux* для хостов Linux
-   использование *Template OS Windows* для хостов Windows

[comment]: # ({/new-78f4f491})

[comment]: # ({new-b8e28285})
##### Шаг 1

Добавим правило обнаружения в сети для нашего диапазона IP адресов.

![discovery.png](../../../../assets/en/manual/discovery/network_discovery/discovery.png)

Zabbix будет пытаться обнаружить узлы сети в диапазоне IP адресов
192.168.1.1-192.168.1.254, пытаясь подключиться к Zabbix агенту и
получить значение ключа **system.uname**. Полученное значение от агента
можно использовать для выполнения различных действий для разных
операционных систем. Например, присоединение шаблона Template OS Windows
к Windows серверам, шаблона Template OS Linux к Linux серверам.

Правило будет выполняться каждые 10 минут (600 секунд).

Когда правило будет добавлено, Zabbix автоматически запустит обнаружение
и порождение событий, основанных на обнаружении, для дальнейшей их
обработки.

[comment]: # ({/new-b8e28285})

[comment]: # ({new-b1d8800e})
##### Шаг 2

Определим [действие](/ru/manual/config/notifications/action) для
добавления обнаруженных Linux серверов в соответвующие группы/шаблоны.

![](../../../../assets/en/manual/discovery/network_discovery/disc_action_1.png)

Это действие активируется, если:

-   сервис "Zabbix агента" в состоянии “доступен"
-   значение system.uname (ключ Zabbix агента, который мы использовали
    при создании правила) содержит "Linux"
-   Время работы более одного 1 часа (3600 секунд)

![](../../../../assets/en/manual/discovery/network_discovery/disc_action_1b.png)

Это действие будет выполнять следующие операции:

-   добавление обнаруженного узла сети в группу "Linux servers" (также
    добавление узла сети, если он не был добавлен ранее)
-   присоединение к шаблону "Template OS Linux". Zabbix автоматически
    запустит наблюдение за узлом сети, используя элементы данных и
    триггеры из шаблона "Template OS Linux".

[comment]: # ({/new-b1d8800e})

[comment]: # ({new-79eefb1b})
##### Шаг 3

Зададим действие для добавления обнаруженных Windows серверов в
соответвующие группы/шаблоны.

![](../../../../assets/en/manual/discovery/network_discovery/disc_action_2.png)

![](../../../../assets/en/manual/discovery/network_discovery/disc_action_2b.png)

##### Шаг 4

Зададим действие для удаления потерянных серверов.

![](../../../../assets/en/manual/discovery/network_discovery/disc_action_3.png)

![](../../../../assets/en/manual/discovery/network_discovery/disc_action_3b.png)

Сервер будет удален из конфигурации, если сервис "Zabbix агент"
'Недоступен' на протяжении более чем 24 часов (86400 секунд).

[comment]: # ({/new-79eefb1b})
