<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="ru" datatype="plaintext" original="manual/discovery/low_level_discovery/examples/snmp_oids.md">
    <body>
      <trans-unit id="03437da9" xml:space="preserve">
        <source># 5 Discovery of SNMP OIDs (legacy)</source>
      </trans-unit>
      <trans-unit id="cf8bf5a8" xml:space="preserve">
        <source>#### Overview

In this section we will perform an SNMP
[discovery](/manual/discovery/low_level_discovery) on a switch.</source>
      </trans-unit>
      <trans-unit id="d8452a6a" xml:space="preserve">
        <source>#### Item key

Unlike with file system and network interface discovery, the item does
not necessarily has to have an "snmp.discovery" key - item type of SNMP
agent is sufficient.

To configure the discovery rule, do the following:

-   Go to: *Data collection* → *Templates*
-   Click on *Discovery* in the row of an appropriate template

![](../../../../../assets/en/manual/discovery/low_level_discovery/templates_snmp.png)

-   Click on *Create discovery rule* in the upper right corner of the
    screen
-   Fill in the discovery rule form with the required details as in the
    screenshot below

![](../../../../../assets/en/manual/discovery/low_level_discovery/lld_rule_snmp.png)

All mandatory input fields are marked with a red asterisk.

The OIDs to discover are defined in SNMP OID field in the following
format: `discovery[{#MACRO1}, oid1, {#MACRO2}, oid2, …,]`

where *{\#MACRO1}*, *{\#MACRO2}* … are valid lld macro names and *oid1*,
*oid2*... are OIDs capable of generating meaningful values for these
macros. A built-in macro *{\#SNMPINDEX}* containing index of the
discovered OID is applied to discovered entities. The discovered
entities are grouped by *{\#SNMPINDEX}* macro value.

To understand what we mean, let us perform few snmpwalks on our switch:

    $ snmpwalk -v 2c -c public 192.168.1.1 IF-MIB::ifDescr
    IF-MIB::ifDescr.1 = STRING: WAN
    IF-MIB::ifDescr.2 = STRING: LAN1
    IF-MIB::ifDescr.3 = STRING: LAN2

    $ snmpwalk -v 2c -c public 192.168.1.1 IF-MIB::ifPhysAddress
    IF-MIB::ifPhysAddress.1 = STRING: 8:0:27:90:7a:75
    IF-MIB::ifPhysAddress.2 = STRING: 8:0:27:90:7a:76
    IF-MIB::ifPhysAddress.3 = STRING: 8:0:27:2b:af:9e

And set SNMP OID to:
`discovery[{#IFDESCR}, ifDescr, {#IFPHYSADDRESS}, ifPhysAddress]`

Now this rule will discover entities with {\#IFDESCR} macros set to
**WAN**, **LAN1** and **LAN2**, {\#IFPHYSADDRESS} macros set to
**8:0:27:90:7a:75**, **8:0:27:90:7a:76**, and **8:0:27:2b:af:9e**,
{\#SNMPINDEX} macros set to the discovered OIDs indexes **1**, **2** and
**3**:

``` {.java}
[
    {
        "{#SNMPINDEX}": "1",
        "{#IFDESCR}": "WAN",
        "{#IFPHYSADDRESS}": "8:0:27:90:7a:75"
    },
    {
        "{#SNMPINDEX}": "2",
        "{#IFDESCR}": "LAN1",
        "{#IFPHYSADDRESS}": "8:0:27:90:7a:76"
    },
    {
        "{#SNMPINDEX}": "3",
        "{#IFDESCR}": "LAN2",
        "{#IFPHYSADDRESS}": "8:0:27:2b:af:9e"
    }
]
```

If an entity does not have the specified OID, then the corresponding
macro will be omitted for this entity. For example if we have the
following data:

    ifDescr.1 "Interface #1"
    ifDescr.2 "Interface #2"
    ifDescr.4 "Interface #4"

    ifAlias.1 "eth0"
    ifAlias.2 "eth1"
    ifAlias.3 "eth2"
    ifAlias.5 "eth4"

Then in this case SNMP discovery
`discovery[{#IFDESCR}, ifDescr, {#IFALIAS}, ifAlias]` will return the
following structure:

``` {.java}
[
    {
        "{#SNMPINDEX}": 1,
        "{#IFDESCR}": "Interface #1",
        "{#IFALIAS}": "eth0"
    },
    {
        "{#SNMPINDEX}": 2,
        "{#IFDESCR}": "Interface #2",
        "{#IFALIAS}": "eth1"
    },
    {
        "{#SNMPINDEX}": 3,
        "{#IFALIAS}": "eth2"
    },
    {
        "{#SNMPINDEX}": 4,
        "{#IFDESCR}": "Interface #4"
    },
    {
        "{#SNMPINDEX}": 5,
        "{#IFALIAS}": "eth4"
    }
]
```</source>
      </trans-unit>
      <trans-unit id="cc4d2566" xml:space="preserve">
        <source>#### Item prototypes

The following screenshot illustrates how we can use these macros in item
prototypes:

![](../../../../../assets/en/manual/discovery/low_level_discovery/item_prototype_snmp.png)

You can create as many item prototypes as needed:

![](../../../../../assets/en/manual/discovery/low_level_discovery/item_prototypes_snmp.png){width="600"}</source>
      </trans-unit>
      <trans-unit id="042beac9" xml:space="preserve">
        <source>#### Trigger prototypes

The following screenshot illustrates how we can use these macros in
trigger prototypes:

![](../../../../../assets/en/manual/discovery/low_level_discovery/trigger_prototype_snmp.png){width="600"}

![](../../../../../assets/en/manual/discovery/low_level_discovery/trigger_prototypes_snmp.png){width="600"}</source>
      </trans-unit>
      <trans-unit id="fbe4f4fa" xml:space="preserve">
        <source>#### Graph prototypes

The following screenshot illustrates how we can use these macros in
graph prototypes:

![](../../../../../assets/en/manual/discovery/low_level_discovery/graph_prototype_snmp.png){width="600"}

![](../../../../../assets/en/manual/discovery/low_level_discovery/graph_prototypes_snmp.png){width="600"}

A summary of our discovery rule:

![](../../../../../assets/en/manual/discovery/low_level_discovery/lld_rules_snmp.png){width="600"}</source>
      </trans-unit>
      <trans-unit id="76c066a2" xml:space="preserve">
        <source>#### Discovered entities

When server runs, it will create real items, triggers and graphs based
on the values the SNMP discovery rule returns. In the host configuration
they are prefixed with an orange link to a discovery rule they come
from.

![](../../../../../assets/en/manual/discovery/low_level_discovery/discovered_items_snmp.png){width="600"}

![](../../../../../assets/en/manual/discovery/low_level_discovery/discovered_triggers_snmp.png){width="600"}

![](../../../../../assets/en/manual/discovery/low_level_discovery/discovered_graphs_snmp.png){width="600"}</source>
      </trans-unit>
    </body>
  </file>
</xliff>
