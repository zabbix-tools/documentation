[comment]: # translation:outdated

[comment]: # ({new-5508ac64})
# 4 Discovery of SNMP OIDs

[comment]: # ({/new-5508ac64})

[comment]: # ({new-3680a9fa})
#### Overview

In this section we will perform an SNMP
[discovery](/manual/discovery/low_level_discovery) on a switch.

This discovery method of SNMP OIDs has been supported since Zabbix server/proxy 6.4.

[comment]: # ({/new-3680a9fa})

[comment]: # ({new-afd80cd0})

#### Item key

Create an SNMP item, using the following item key in the *SNMP OID* field:

    snmp.walk[1.3.6.1.2.1.2.2.1.2,1.3.6.1.2.1.2.2.1.3]

![](../../../../../assets/en/manual/discovery/low_level_discovery/snmp_walk_item.png)

This item will perform an snmpwalk for the OIDs specified in the parameters (1.3.6.1.2.1.2.2.1.2, 1.3.6.1.2.1.2.2.1.3), returning a concatenated list of values, e.g.:

    .1.3.6.1.2.1.2.2.1.2.1 = STRING: "lo"
    .1.3.6.1.2.1.2.2.1.2.2 = STRING: "ens33"
    .1.3.6.1.2.1.2.2.1.2.3 = STRING: "ens37"
    .1.3.6.1.2.1.2.2.1.3.1 = INTEGER: 24
    .1.3.6.1.2.1.2.2.1.3.2 = INTEGER: 6
    .1.3.6.1.2.1.2.2.1.3.3 = INTEGER: 6

[comment]: # ({/new-afd80cd0})

[comment]: # ({new-3ca02a27})
#### Dependent item

Go to the discovery rules of your template/host. Click on *Create discovery rule* in the upper right corner of the screen. 

Fill in the required details in the **Discovery rule** tab:

-   Select *Dependent item* as item type
-   Select the previously created SNMP walk item as the master item
-   Fill the name and key with meaningful values

![](../../../../../assets/en/manual/discovery/low_level_discovery/snmp_discovery_dep_item.png)

In the **Preprocessing** tab, select the *SNMP walk to JSON* preproccesing step.

![](../../../../../assets/en/manual/discovery/low_level_discovery/snmp_discovery_preprocessing.png){width="600"}

In the field name specify a valid LLD macro name. Select the corresponding OID path to discover values from.

This rule will discover entities and set:

*   {\#IFDESCR} macros to `lo`, `ens33`, and `ens37`;
*   {\#IFTYPE} macros set to `24`, `6`, and `6`.

A built-in macro *{\#SNMPINDEX}* containing the index of the
discovered OIDs is applied to discovered entities. The discovered
entities are grouped by *{\#SNMPINDEX}* macro value: **1**, **2** and
**3**:

``` {.java}
[
    {
        "{#SNMPINDEX}": "1",
        "{#IFDESCR}": "lo",
        "{#IFTYPE}": "24"
    },
    {
        "{#SNMPINDEX}": "2",
        "{#IFDESCR}": "ens33",
        "{#IFTYPE}": "6"
    },
    {
        "{#SNMPINDEX}": "3",
        "{#IFDESCR}": "ens37",
        "{#IFTYPE}": "6"
    }
]
```

If an entity does not have the specified OID, then the corresponding
macro will be omitted for this entity.

[comment]: # ({/new-3ca02a27})

[comment]: # ({new-bff9418f})
#### Item prototypes

The following screenshot illustrates how we can use these macros in item
prototypes:

![](../../../../../assets/en/manual/discovery/low_level_discovery/item_prototype_snmp.png)

Again, creating as many item prototypes as needed:

![](../../../../../assets/en/manual/discovery/low_level_discovery/item_prototypes_snmp.png)

[comment]: # ({/new-bff9418f})



[comment]: # ({new-2be94533})
#### Discovered entities

When server runs, it will create real items, triggers and graphs based
on the values the SNMP discovery rule returns. In the host configuration
they are prefixed with an orange link to a discovery rule they come
from.

![](../../../../../assets/en/manual/discovery/low_level_discovery/discovered_items_snmp.png)

![](../../../../../assets/en/manual/discovery/low_level_discovery/discovered_triggers_snmp.png)

![](../../../../../assets/en/manual/discovery/low_level_discovery/discovered_graphs_snmp.png)

[comment]: # ({/new-2be94533})
