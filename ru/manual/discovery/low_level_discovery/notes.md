[comment]: # translation:outdated

[comment]: # ({new-54ecc26e})
# Заметки по низкоуровневому обнаружению

[comment]: # ({/new-54ecc26e})

[comment]: # ({new-40e2ce9f})
#### Обнаружение групп элементов данных

Прототипы групп элементов данных поддерживают LLD макросы.

Один прототип групп элементов данных может использоваться несколькими
прототипами элементов данных одного правила обнаружения.

Как и другие, обнаруженные объекты групп элементов данных следуют
времени жизни, указанному в правиле обнаружения (настройка 'период
сохранения потерянных ресурсов') - они будут удалены после того как не
обнаруживаются указанное количество дней.

Если группа элементов данных более не обнаруживается, все обнаруженные
элементы данных автоматически из неё удаляются, даже если сама группа
элементов данных еще не удалена из-за настройки 'периода сохранения
потерянных ресурсов'.

Прототипы групп элементов данных, указанные в одном правиле обнаружения
не могут обнаружить такую же группу элементов данных. В этой ситуации
будет успешным первое обнаружение прототипа, остальные сообщат
соответствующую ошибку LLD. Только прототипы групп элементов данных,
указанных в разных правилах обнаружения, могут вызвать обнаружение такой
же группы элементов данных.

[comment]: # ({/new-40e2ce9f})



[comment]: # ({new-071e9701})
#### Multiple LLD rules for the same item

Since Zabbix agent version 3.2 it is possible to define several
low-level discovery rules with the same discovery item.

To do that you need to define the Alias agent
[parameter](/manual/appendix/config/zabbix_agentd), allowing to use
altered discovery item keys in different discovery rules, for example
`vfs.fs.discovery[foo]`, `vfs.fs.discovery[bar]`, etc.

[comment]: # ({/new-071e9701})

[comment]: # ({new-a8d6d602})
#### Data limits for return values

There is no limit for low-level discovery rule JSON data if it is
received directly by Zabbix server, because return values are processed
without being stored in a database. There's also no limit for custom
low-level discovery rules, however, if it is intended to acquire custom
LLD data using a user parameter, then the user parameter return value
limit applies (512 KB).

If data has to go through Zabbix proxy it has to store this data in
database so [database
limits](/manual/config/items/item#text_data_limits) apply.

[comment]: # ({/new-a8d6d602})
