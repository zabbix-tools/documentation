[comment]: # translation:outdated

[comment]: # ({23173bb8-5c86be0b})
# 18. Веб-интерфейс

[comment]: # ({/23173bb8-5c86be0b})

[comment]: # ({36133450-8e039e89})
#### Обзор

Для легкого доступа к Zabbix из любого места и с любой платформы,
предусмотрен веб-интерфейс.

::: noteclassic
Если Вы используете несколько веб-интерфейсов, убедитесь, 
что локали и необходимые библиотеки (LDAP, SAML и т.д.) 
установлены и настроены одинаково для каждого из веб-интерфейсов.
:::

[comment]: # ({/36133450-8e039e89})

[comment]: # ({new-278be5f6})
### Frontend help

A help link ![](../../assets/en/manual/web_interface/help_link.png) is provided in Zabbix frontend forms with direct links to the corresponding parts of the documentation.

[comment]: # ({/new-278be5f6})
