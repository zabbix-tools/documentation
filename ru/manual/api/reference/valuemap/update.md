[comment]: # translation:outdated

[comment]: # ({new-6b085503})
# valuemap.update

[comment]: # ({/new-6b085503})

[comment]: # ({new-ab463b0c})
### Описание

`объект valuemap.update(объект/массив преобразованиязначений)`

Этот метод позволяет обновлять существующие преобразования значений.

[comment]: # ({/new-ab463b0c})

[comment]: # ({new-0094db2a})
### Параметры

`(объект/массив)` [Свойства преобразования
значений](object#преобразование_значений), которые будут обновлены.

Свойство `valuemapid` должно быть указано по каждому преобразованию
значений, все остальные свойства опциональны. Будут обновлены только
переданные свойства, все остальные останутся неизменными.

[comment]: # ({/new-0094db2a})

[comment]: # ({new-0dbe7e67})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID обновленных
преобразований значений под свойством `valuemapids`.

[comment]: # ({/new-0dbe7e67})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-571fcc62})
#### Изменение имени преобразования значений

Изменение имени преобразования значений на "Device status".

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "valuemap.update",
    "params": {
        "valuemapid": "2",
        "name": "Device status"
    },
    "auth": "57562fd409b3b3b9a4d916d45207bbcb",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "valuemapids": [
            "2"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-571fcc62})

[comment]: # ({new-7b3b6bc4})
#### Изменение соответствий значений в одном преобразовании значений

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "valuemap.update",
    "params": {
        "valuemapid": "2",
        "mappings": [
            {
                "value": "0",
                "newvalue": "Online"
            },
            {
                "value": "1",
                "newvalue": "Offline"
            }
        ]
    },
    "auth": "57562fd409b3b3b9a4d916d45207bbcb",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "valuemapids": [
            "2"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-7b3b6bc4})

[comment]: # ({new-fc02e5cf})
### Исходный код

CValueMap::update() в
*frontends/php/include/classes/api/services/CValueMap.php*.

[comment]: # ({/new-fc02e5cf})
