[comment]: # translation:outdated

[comment]: # ({new-6698fb90})
# valuemap.create

[comment]: # ({/new-6698fb90})

[comment]: # ({new-37547a16})
### Описание

`объект valuemap.create(объект/массив преобразованиязначений)`

Этот метод позволяет создавать новые преобразования значений.

[comment]: # ({/new-37547a16})

[comment]: # ({new-efa9b6ec})
### Параметры

`(объект/массив)` Создаваемые преобразования значений.

Этот метод принимает преобразования значений со [стандартными свойствами
преобразования значений](object#преобразование_значений).

[comment]: # ({/new-efa9b6ec})

[comment]: # ({new-6809040a})
### Возващаемые значения

`(объект)` Возвращает объект, который содержит ID созданных
преобразований значений под свойством `hostmacroids`. Порядок
возвращаемых ID совпадает с порядком переданных преобразований значений.

[comment]: # ({/new-6809040a})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-ace763eb})
#### Создание преобразования значений

Создание одного преобразования значений с двумя соответствиями.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "valuemap.create",
    "params": {
        "name": "Service state",
        "mappings": [
            {
                "value": "0",
                "newvalue": "Down"
            },
            {
                "value": "1",
                "newvalue": "Up"
            }
        ]
    },
    "auth": "57562fd409b3b3b9a4d916d45207bbcb",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "valuemapids": [
            "1"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-ace763eb})

[comment]: # ({new-58f99a13})
### Исходный код

CValueMap::create() в
*frontends/php/include/classes/api/services/CValueMap.php*.

[comment]: # ({/new-58f99a13})
