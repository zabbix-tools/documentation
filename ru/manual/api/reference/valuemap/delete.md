[comment]: # translation:outdated

[comment]: # ({new-a1e429c6})
# valuemap.delete

[comment]: # ({/new-a1e429c6})

[comment]: # ({new-b73af783})
### Описание

`объект valuemap.delete(массив valuemapids)`

Этот метод позволяет удалять преобразования значений.

[comment]: # ({/new-b73af783})

[comment]: # ({new-db6b3809})
### Параметры

`(массив)` ID удаляемых преобразований значений.

[comment]: # ({/new-db6b3809})

[comment]: # ({new-59feda24})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID удаленных
преобразований значений под свойством `valuemapids`.

[comment]: # ({/new-59feda24})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-bf1c93a7})
#### Удаление нескольких преобразований значений

Удаление двух преобразований значений.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "valuemap.delete",
    "params": [
        "1",
        "2"
    ],
    "auth": "57562fd409b3b3b9a4d916d45207bbcb",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "valuemapids": [
            "1",
            "2"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-bf1c93a7})

[comment]: # ({new-908d048e})
### Исходный код

CValueMap::delete() в
*frontends/php/include/classes/api/services/CValueMap.php*.

[comment]: # ({/new-908d048e})
