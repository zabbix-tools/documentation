[comment]: # translation:outdated

[comment]: # ({new-9b6159cd})
# Проверка обнаружения

Этот класс предназначен для работы с проверками обнаружения.

Справка по объектам:\

-   [Проверка
    обнаружения](/ru/manual/api/reference/dcheck/object#проверка_обнаружения)

Доступные методы:\

-   [dcheck.get](/ru/manual/api/reference/dcheck/get) - получение
    проверок обнаружения

[comment]: # ({/new-9b6159cd})
