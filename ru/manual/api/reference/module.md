[comment]: # translation:outdated

[comment]: # ({new-b4f3eb78})
# Module

[comment]: # ({/new-b4f3eb78})

[comment]: # ({new-8202f870})

This class is designed to work with frontend modules.

Object references:\

-   [Module](/manual/api/reference/module/object#module)

Available methods:\

-   [module.create](/manual/api/reference/module/create) - installing new modules
-   [module.delete](/manual/api/reference/module/delete) - uninstalling modules
-   [module.get](/manual/api/reference/module/get) - retrieving modules
-   [module.update](/manual/api/reference/module/update) - updating modules

[comment]: # ({/new-8202f870})
