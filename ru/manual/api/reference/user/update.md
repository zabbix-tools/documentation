[comment]: # translation:outdated

[comment]: # ({new-084d9051})
# user.update

[comment]: # ({/new-084d9051})

[comment]: # ({new-dbc24bad})
### Описание

`объект user.update(объект/массив пользователи)`

Этот метод позволяет обновлять существующих пользователей.

[comment]: # ({/new-dbc24bad})

[comment]: # ({new-3b478f18})
### Параметры

`(объект/массив)` Свойства пользователей, которые будут обновлены.

Свойство `userid` должно быть указано по каждому пользователю, все
остальные свойства опциональны. Будут обновлены только переданные
свойства, все остальные останутся неизменными.

В дополнение к [стандартным свойствам
пользователя](object#пользователь), этот метод принимает следующие
параметры.

|Параметр|Тип|Описание|
|----------------|------|----------------|
|passwd|строка|Пароль пользователя.|
|usrgrps|массив|Группы пользователей, которые заменят существующие группы пользователей.<br><br>У групп пользователей должно быть задано свойство `usrgrpid`.|
|user\_medias|массив|Оповещения, которые заменят текущие оповещения пользователя.|

[comment]: # ({/new-3b478f18})

[comment]: # ({new-4c3fb877})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID обновленных
пользователей под свойством `userids`.

[comment]: # ({/new-4c3fb877})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-c0483e48})
#### Переименование пользователя

Переименование пользователя на John Doe.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "user.update",
    "params": {
        "userid": "1",
        "name": "John",
        "surname": "Doe"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "userids": [
            "1"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-c0483e48})

[comment]: # ({new-7ae4c491})
### Исходный код

CUser::update() в
*frontends/php/include/classes/api/services/CUser.php*.

[comment]: # ({/new-7ae4c491})



[comment]: # ({new-1083dc26})
### See also

-   [Authentication](/manual/api/reference/authentication)

[comment]: # ({/new-1083dc26})

[comment]: # ({new-bbd5a07f})
### Source

CUser::update() in *ui/include/classes/api/services/CUser.php*.

[comment]: # ({/new-bbd5a07f})
