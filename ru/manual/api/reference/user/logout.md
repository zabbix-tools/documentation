[comment]: # translation:outdated

[comment]: # ({new-860084e0})
# user.logout

[comment]: # ({/new-860084e0})

[comment]: # ({new-d6ff52eb})
### Описание

`строка/объект user.logout(массив)`

Этот метод позволяет выполнять выход из API и аннулировать текущий токен
аутентификации.

[comment]: # ({/new-d6ff52eb})

[comment]: # ({new-4fa8a419})
### Параметры

`(массив)` Этот метод принимает пустой массив.

[comment]: # ({/new-4fa8a419})

[comment]: # ({new-66018290})
### Возвращаемые значения

`(логическое)` Возвращает `true`, если пользователь успешно выполнил
выход.

[comment]: # ({/new-66018290})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-b5326b2b})
#### Выполнение выхода

Выполнение выхода из API.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "user.logout",
    "params": [],
    "id": 1,
    "auth": "16a46baf181ef9602e1687f3110abf8a"
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": true,
    "id": 1
}
```

[comment]: # ({/new-b5326b2b})

[comment]: # ({new-02f79c40})
### Смотрите также

-   [user.login](login)

[comment]: # ({/new-02f79c40})

[comment]: # ({new-7fd9f53c})
### Исходный код

CUser::login() в *frontends/php/include/classes/api/services/CUser.php*.

[comment]: # ({/new-7fd9f53c})
