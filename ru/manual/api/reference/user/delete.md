[comment]: # translation:outdated

[comment]: # ({new-bab9094f})
# user.delete

[comment]: # ({/new-bab9094f})

[comment]: # ({new-1f1b6511})
### Описание

`объект user.delete(массив пользователи)`

Этот метод позволяет удалять пользователей.

[comment]: # ({/new-1f1b6511})

[comment]: # ({new-213fafcd})
### Параметры

`(массив)` ID удаляемых пользователей.

[comment]: # ({/new-213fafcd})

[comment]: # ({new-17d395f4})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID удаленных
пользователей под свойством `userids`.

[comment]: # ({/new-17d395f4})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-783cb2af})
#### Удаление нескольких пользователей

Удаление двух пользователей.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "user.delete",
    "params": [
        "1",
        "5"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "userids": [
            "1",
            "5"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-783cb2af})

[comment]: # ({new-8c5510d6})
### Исходный код

CUser::delete() в
*frontends/php/include/classes/api/services/CUser.php*.

[comment]: # ({/new-8c5510d6})
