[comment]: # translation:outdated

[comment]: # ({new-c233abea})
# triggerprototype.delete

[comment]: # ({/new-c233abea})

[comment]: # ({new-4ed080b7})
### Описание

`объект triggerprototype.delete(массив triggerPrototypeIds)`

Этот метод позволяет удалять прототипы триггеров.

[comment]: # ({/new-4ed080b7})

[comment]: # ({new-93be9503})
### Параметры

`(массив)` ID удаляемых прототипов триггеров.

[comment]: # ({/new-93be9503})

[comment]: # ({new-91b09722})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID удаленных прототипов
триггеров под свойством `triggerids`.

[comment]: # ({/new-91b09722})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-ed4ab6a8})
#### Удаление нескольких прототипов триггеров

Удаление двух прототипов триггеров.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "triggerprototype.delete",
    "params": [
        "12002",
        "12003"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "triggerids": [
            "12002",
            "12003"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-ed4ab6a8})

[comment]: # ({new-c4ac1758})
### Исходный код

CTriggerPrototype::delete() в
*frontends/php/include/classes/api/services/CTriggerPrototype.php*.

[comment]: # ({/new-c4ac1758})
