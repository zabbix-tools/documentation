[comment]: # translation:outdated

[comment]: # ({new-49f9e765})
# triggerprototype.create

[comment]: # ({/new-49f9e765})

[comment]: # ({new-c66bc66b})
### Описание

`объект triggerprototype.create(объект/массив ПрототипыТриггеров)`

Этот метод позволяет создавать новые прототипы триггеров.

[comment]: # ({/new-c66bc66b})

[comment]: # ({new-e93145a6})
### Параметры

`(объект/массив)` Создаваемые прототипы триггеров.

В дополнение к [стандартным свойствам прототипа
триггеров](object#прототип_триггеров), этот метод принимает следующие
параметры.

|Параметр|Тип|Описание|
|----------------|------|----------------|
|dependencies|массив|Триггеры и прототипы триггеров, от которых будет зависеть создаваемый прототип триггеров.<br><br>У триггеров должно быть задано свойство `triggerid`.|
|tags|массив|Теги прототипа триггеров.|

::: noteimportant
Выражение триггера необходимо указывать в
раскрытой форме и оно должно содержать по крайней мере один прототип
элементов данных.
:::

[comment]: # ({/new-e93145a6})

[comment]: # ({new-8d1f4c4d})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID созданных прототипов
триггеров под свойством `triggerids`. Порядок возвращаемых ID совпадает
с порядком переданных прототипов триггеров.

[comment]: # ({/new-8d1f4c4d})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-87a61295})
#### Создание прототипа триггеров

Создание прототипа триггеров для определения, когда на файловой системе
останется менее чем 20% свободного места на диске.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "triggerprototype.create",
    "params": {
        "description": "Free disk space is less than 20% on volume {#FSNAME}",
        "expression": "{Zabbix server:vfs.fs.size[{#FSNAME},pfree].last()}<20",
        "tags": [
            {
                "tag": "volume",
                "value": "{#FSNAME}"
            },
            {
                "tag": "type",
                "value": "{#FSTYPE}"
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "triggerids": [
            "17372"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-87a61295})

[comment]: # ({new-43cf23db})
### Исходный код

CTriggerPrototype::create() в
*frontends/php/include/classes/api/services/CTriggerPrototype.php*.

[comment]: # ({/new-43cf23db})
