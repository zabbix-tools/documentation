[comment]: # translation:outdated

[comment]: # ({new-46b153da})
# image.delete

[comment]: # ({/new-46b153da})

[comment]: # ({new-60f587fa})
### Описание

`объект image.delete(массив imageIds)`

Этот метод позволяет удалять изображения.

[comment]: # ({/new-60f587fa})

[comment]: # ({new-c7ef61f7})
### Параметры

`(массив)` ID удаляемых изображений.

[comment]: # ({/new-c7ef61f7})

[comment]: # ({new-4b3e57f6})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID удаленных изображений
под свойством `imageids`.

[comment]: # ({/new-4b3e57f6})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-925dee02})
#### Удаление нескольких изображений

Удаление двух изображений.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "image.delete",
    "params": [
        "188",
        "192"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "imageids": [
            "188",
            "192"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-925dee02})

[comment]: # ({new-dfdead8e})
### Исходный код

CImage::delete() в
*frontends/php/include/classes/api/services/CImage.php*.

[comment]: # ({/new-dfdead8e})
