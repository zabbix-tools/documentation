[comment]: # translation:outdated

[comment]: # ({new-03e77d6d})
# Панель

Этот класс предназначен для работы с панелями.

Справка по объектам:\

-   [Панель](/ru/manual/api/reference/dashboard/object#панель)
-   [Виджет
    панели](/ru/manual/api/reference/dashboard/object#виджет_панели)
-   [Поле виджета
    панели](/ru/manual/api/reference/dashboard/object#поле_виджета_панели)
-   [Группа пользователей
    панели](/ru/manual/api/reference/dashboard/object#группа_пользователей_панели)
-   [Пользователь
    панели](/ru/manual/api/reference/dashboard/object#пользователь_панели)

Доступные методы:\

-   [dashboard.create](/ru/manual/api/reference/dashboard/create) -
    создание новых панелей
-   [dashboard.delete](/ru/manual/api/reference/dashboard/delete) -
    удаление панелей
-   [dashboard.get](/ru/manual/api/reference/dashboard/get) - получение
    панелей
-   [dashboard.update](/ru/manual/api/reference/dashboard/update) -
    обновление панелей

[comment]: # ({/new-03e77d6d})
