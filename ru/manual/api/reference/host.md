[comment]: # translation:outdated

[comment]: # ({new-6a1da71f})
# Узел сети

Этот класс предназначен для работы с узлами сети.

Справка по объектам:\

-   [Узел сети](/ru/manual/api/reference/host/object#узел_сети)
-   [Данные инвентаризации узлов
    сети](/ru/manual/api/reference/host/object#данные_инвентаризации_узлов_сети)

Доступные методы:\

-   [host.create](/ru/manual/api/reference/host/create) - создание новых
    узлов сети
-   [host.delete](/ru/manual/api/reference/host/delete) - удаление узлов
    сети
-   [host.get](/ru/manual/api/reference/host/get) - получение узлов сети
-   [host.massadd](/ru/manual/api/reference/host/massadd) - добавление
    связанных объектов к узлам сети
-   [host.massremove](/ru/manual/api/reference/host/massremove) -
    удаление связанных объектов с узлов сети
-   [host.massupdate](/ru/manual/api/reference/host/massupdate) - замена
    или удаление связанных объектов с узлов сети
-   [host.update](/ru/manual/api/reference/host/update) - обновление
    узлов сети

[comment]: # ({/new-6a1da71f})
