[comment]: # translation:outdated

[comment]: # ({new-ce8d2bd2})
# usergroup.get

[comment]: # ({/new-ce8d2bd2})

[comment]: # ({new-ef79b8a9})
### Описание

`целое число/массив usergroup.get(объект параметры)`

Этот метод позволяет получать группы пользователей в соответствии с
заданными параметрами.

[comment]: # ({/new-ef79b8a9})

[comment]: # ({new-4be9c06e})
### Параметры

`(объект)` Параметры задают желаемый вывод.

Этот метод поддерживает следующие параметры.

|Параметр|Тип|Описание|
|----------------|------|----------------|
|status|целое число|Возврат групп пользователей только с заданным состоянием.<br><br>Обратитесь к [странице группы пользователей](object#группа_пользователей) для получения списка поддерживаемых состояний.|
|userids|строка/массив|Возврат только тех групп пользователей, которые содержат заданных пользователей.|
|usrgrpids|строка/массив|Возврат групп пользователей только с заданными ID.|
|with\_gui\_access|целое число|Возврат групп пользователей только с заданным методом аутентификации в веб-интерфейсе.<br><br>Обратитесь к [странице группы пользователей](object#группа_пользователей) для получения списка поддерживаемых методов.|
|selectTagFilters|запрос|Возврат прав доступа на основе тегов группы пользователей в свойстве `tag_filters`.<br><br>Имеет следующие свойства:<br>`groupid` - (строка) ID группы пользователей;<br>`tag` - (строка) имя тега;<br>`value` - (строка) значение тега.|
|selectUsers|запрос|Возврат пользователей из группы пользователей в свойстве `users`.|
|selectRights|запрос|Возврат прав доступа группы узлов сети в свойстве `rights`.<br><br>Имеет следующие свойства:<br>`permission` - (целое число) уровень прав доступа к группе узлов сети;<br>`id` - (строка) ID группы узлов сети.<br><br>Обратитесь к [странице группы пользователей](object#права_доступа) для получения списка уровней прав доступа к группам узлов сети.|
|limitSelects|целое число|Ограничение количества записей, возвращаемых подзапросами.|
|sortfield|строка/массив|Сортировка результата в соответствии с заданными свойствами.<br><br>Возможные значения: `usrgrpid`, `name`.|
|countOutput|логический|Эти параметры являются общими для всех методов `get` и они описаны в [справочных комментариях](/ru/manual/api/reference_commentary#общие_параметры_get_метода).|
|editable|логический|^|
|excludeSearch|логический|^|
|filter|объект|^|
|limit|целое число|^|
|output|запрос|^|
|preservekeys|логический|^|
|search|объект|^|
|searchByAny|логический|^|
|searchWildcardsEnabled|логический|^|
|sortorder|строка/массив|^|
|startSearch|логический|^|

[comment]: # ({/new-4be9c06e})

[comment]: # ({new-7223bab1})
### Возвращаемые значения

`(целое число/массив)` Возвращает либо:

-   массив объектов;
-   количество найденных объектов, если используется параметр
    `countOutput`.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-62cd5d8e})
#### Получение активированных групп пользователей

Получение всех активированных групп пользователей.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "usergroup.get",
    "params": {
        "output": "extend",
        "status": 0
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "usrgrpid": "7",
            "name": "Zabbix administrators",
            "gui_access": "0",
            "users_status": "0",
            "debug_mode": "1"
        },
        {
            "usrgrpid": "8",
            "name": "Guests",
            "gui_access": "0",
            "users_status": "0",
            "debug_mode": "0"
        },
        {
            "usrgrpid": "11",
            "name": "Enabled debug mode",
            "gui_access": "0",
            "users_status": "0",
            "debug_mode": "1"
        },
        {
            "usrgrpid": "12",
            "name": "No access to the frontend",
            "gui_access": "2",
            "users_status": "0",
            "debug_mode": "0"
        },
        {
            "usrgrpid": "14",
            "name": "Read only",
            "gui_access": "0",
            "users_status": "0",
            "debug_mode": "0"
        },
        {
            "usrgrpid": "18",
            "name": "Deny",
            "gui_access": "0",
            "users_status": "0",
            "debug_mode": "0"
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-62cd5d8e})

[comment]: # ({new-039ccba1})
### Смотрите также

-   [Пользователь](/ru/manual/api/reference/user/object#пользователь)

[comment]: # ({/new-039ccba1})

[comment]: # ({new-e72b2ac9})
### Исходный код

CUserGroup::get() в
*frontends/php/include/classes/api/services/CUserGroup.php*.

[comment]: # ({/new-e72b2ac9})
