[comment]: # translation:outdated

[comment]: # ({new-7e2bc778})
# usergroup.delete

[comment]: # ({/new-7e2bc778})

[comment]: # ({new-c1ef47a5})
### Описание

`объект usergroup.delete(массив userGroupIds)`

Этот метод позволяет удалять группы пользователей.

[comment]: # ({/new-c1ef47a5})

[comment]: # ({new-22b6416d})
### Параметры

`(массив)` ID удаляемых групп пользователей.

[comment]: # ({/new-22b6416d})

[comment]: # ({new-dde57d64})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID удаленных групп
пользователей под свойством `usrgrpids`.

[comment]: # ({/new-dde57d64})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-156418d8})
#### Удаление нескольких групп пользователей

Удаление двух групп пользователей.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "usergroup.delete",
    "params": [
        "20",
        "21"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "usrgrpids": [
            "20",
            "21"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-156418d8})

[comment]: # ({new-9a88f22f})
### Исходный код

CUserGroup::delete() в
*frontends/php/include/classes/api/services/CUserGroup.php*.

[comment]: # ({/new-9a88f22f})
