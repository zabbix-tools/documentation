[comment]: # translation:outdated

[comment]: # ({new-a68f24c8})
# Оповещение

Этот класс предназначен для работы с оповещениями.

Справка по объектам:\

-   [Оповещение](/ru/manual/api/reference/alert/object#оповещение)

Доступные методы:\

-   [alert.get](/ru/manual/api/reference/alert/get) - получение
    оповещений

[comment]: # ({/new-a68f24c8})
