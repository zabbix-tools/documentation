[comment]: # translation:outdated

[comment]: # ({new-be32ecd0})
# Динамика изменений

Этот класс предназначен для работы с данными динамики изменений.

Справка по объектам:\

-   [Динамика
    изменений](/ru/manual/api/reference/trend/object#динамика_изменений)

Доступные методы:\

-   [trend.get](/ru/manual/api/reference/trend/get) - получение динамики
    изменений

[comment]: # ({/new-be32ecd0})
