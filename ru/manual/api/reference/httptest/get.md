[comment]: # translation:outdated

[comment]: # ({new-7f8cd3a3})
# httptest.get

[comment]: # ({/new-7f8cd3a3})

[comment]: # ({new-96cbc36d})
### Описание

`целое число/массив httptest.get(объект параметры)`

Этот метод позволяет получать веб-сценарии в соответствии с заданными
параметрами.

[comment]: # ({/new-96cbc36d})

[comment]: # ({new-7be2c19f})
### Параметры

`(объект)` Параметры задают желаемый вывод.

Этот метод поддерживает следующие параметры.

|Параметр|Тип|Тип|
|----------------|------|------|
|applicationids|строка/массив|Возврат только тех веб-сценариев, которые принадлежат заданным группам элементов данных.|
|groupids|строка/массив|Возврат только тех веб-сценариев, которые принадлежат заданным группам узлов сети.|
|hostids|строка/массив|Возврат только тех веб-сценариев, которые принадлежат заданным узлам сети.|
|httptestids|строка/массив|Возврат веб-сценариев только с заданными ID.|
|inherited|логический|Если задано значение `true`, возвращать веб-сценарии уснаследованные из шаблона.|
|monitored|логический|Если задано значение `true`, возвращать только активированные веб-сценарии с узлов сети под наблюдением.|
|templated|логический|Если задано значение `true`, возвращать только те веб-сценарии, которые принадлежат шаблонам.|
|templateids|строка/массив|Возврат только тех веб-сценариев, которые принадлежат заданным шаблонам.|
|expandName|флаг|Раскрытие макросов в имени веб-сценария.|
|expandStepName|флаг|Раскрытие макросов в именах шагов сценария.|
|selectHosts|запрос|Возврат узлов сети, которым принадлежит веб-сценарий, в виде массива в свойстве `hosts`.|
|selectSteps|запрос|Возврат шагов веб-сценария в свойстве `steps`.|
|sortfield|строка/массив|Сортировка результата в соответствии с заданными свойствами.<br><br>Возможные значения: `httptestid` и `name`.|
|countOutput|логический|Эти параметры являются общими для всех методов `get` и они описаны в [справочных комментариях](/ru/manual/api/reference_commentary#общие_параметры_get_метода).|
|editable|логический|^|
|excludeSearch|логический|^|
|filter|объект|^|
|limit|целое число|^|
|output|запрос|^|
|preservekeys|логический|^|
|search|объект|^|
|searchByAny|логический|^|
|searchWildcardsEnabled|логический|^|
|sortorder|строка/массив|^|
|startSearch|логический|^|

[comment]: # ({/new-7be2c19f})

[comment]: # ({new-7223bab1})
### Возвращаемые значения

`(целое число/массив)` Возвращает либо:

-   массив объектов;
-   количество найденных объектов, если используется параметр
    `countOutput`.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-c93618d6})
#### Получение веб-сценария

Получение всех данных веб-сценария "4".

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "httptest.get",
    "params": {
        "output": "extend",
        "selectSteps": "extend",
        "httptestids": "9"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "httptestid": "9",
            "name": "Homepage check",
            "applicationid": "0",
            "nextcheck": "0",
            "delay": "1m",
            "status": "0",
            "variables": [],
            "agent": "Zabbix",
            "authentication": "0",
            "http_user": "",
            "http_password": "",
            "hostid": "10084",
            "templateid": "0",
            "http_proxy": "",
            "retries": "1",
            "ssl_cert_file": "",
            "ssl_key_file": "",
            "ssl_key_password": "",
            "verify_peer": "0",
            "verify_host": "0",
            "headers": [],
            "steps": [
                {
                    "httpstepid": "36",
                    "httptestid": "9",
                    "name": "Homepage",
                    "no": "1",
                    "url": "http://mycompany.com",
                    "timeout": "15s",
                    "posts": "",
                    "required": "",
                    "status_codes": "200",
                    "variables": [  
                        {  
                            "name":"{var}",
                            "value":"12"
                        }
                    ],
                    "follow_redirects": "1",
                    "retrieve_mode": "0",
                    "headers": [],
                    "query_fields": []
                },
                {
                    "httpstepid": "37",
                    "httptestid": "9",
                    "name": "Homepage / About",
                    "no": "2",
                    "url": "http://mycompany.com/about",
                    "timeout": "15s",
                    "posts": "",
                    "required": "",
                    "status_codes": "200",
                    "variables": [],
                    "follow_redirects": "1",
                    "retrieve_mode": "0",
                    "headers": [],
                    "query_fields": []
                }
            ]
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-c93618d6})

[comment]: # ({new-ce685d69})
### Смотрите также

-   [Узел сети](/ru/manual/api/reference/host/object#узел_сети)
-   [Шаг сценария](object#шаг_сценария)

[comment]: # ({/new-ce685d69})

[comment]: # ({new-8ec619ff})
### Исходный код

CHttpTest::get() в
*frontends/php/include/classes/api/services/CHttpTest.php*.

[comment]: # ({/new-8ec619ff})
