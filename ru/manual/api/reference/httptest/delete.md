[comment]: # translation:outdated

[comment]: # ({new-629f1e7c})
# httptest.delete

[comment]: # ({/new-629f1e7c})

[comment]: # ({new-67a29a65})
### Описание

`объект httptest.delete(массив webScenarioIds)`

Этот метод позволяет удалять веб-сценарии.

[comment]: # ({/new-67a29a65})

[comment]: # ({new-16d66a94})
### Параметры

`(массив)` ID удаляемых веб-сценариев.

[comment]: # ({/new-16d66a94})

[comment]: # ({new-fd0832d6})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID удаленных
веб-сценариев под свойством `httptestids`.

[comment]: # ({/new-fd0832d6})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-4924488e})
#### Удаление нескольких веб-сценариев

Удаление двух веб-сценариев.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "httptest.delete",
    "params": [
        "2",
        "3"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "httptestids": [
            "2",
            "3"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-4924488e})

[comment]: # ({new-0ba69a51})
### Исходный код

CHttpTest::delete() в
*frontends/php/include/classes/api/services/CHttpTest.php*.

[comment]: # ({/new-0ba69a51})
