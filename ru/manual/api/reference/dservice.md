[comment]: # translation:outdated

[comment]: # ({new-14213bb4})
# Обнаруженный сервис

Этот класс предназначен для работы с обнаруженными сервисами.

Справка по объектам:\

-   [Обнаруженный
    сервис](/ru/manual/api/reference/dservice/object#обнаруженный_сервис)

Доступные методы:\

-   [dservice.get](/ru/manual/api/reference/dservice/get) - получение
    обнаруженных сервисов

[comment]: # ({/new-14213bb4})
