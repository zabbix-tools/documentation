[comment]: # translation:outdated

[comment]: # ({new-9dae9ddc})
# Триггер

Этот класс предназначен для работы с триггерами.

Справка по объектам:\

-   [Триггер](/ru/manual/api/reference/trigger/object#триггер)

Доступные методы:\

-   [trigger.adddependencies](/ru/manual/api/reference/trigger/adddependencies) -
    добавление новых зависимостей к триггерам
-   [trigger.create](/ru/manual/api/reference/trigger/create) - создание
    новых триггеров
-   [trigger.delete](/ru/manual/api/reference/trigger/delete) - удаление
    триггеров
-   [trigger.deletedependencies](/ru/manual/api/reference/trigger/deletedependencies) -
    удаление зависимостей к триггерам
-   [trigger.get](/ru/manual/api/reference/trigger/get) - получение
    триггеров
-   [trigger.update](/ru/manual/api/reference/trigger/update) -
    обновление триггеров

[comment]: # ({/new-9dae9ddc})
