[comment]: # translation:outdated

[comment]: # ({new-6bf4ac44})
# Способ оповещения

Этот класс предназначен для работы со способами оповещений.

Справка по объектам:\

-   [Способ
    оповещения](/ru/manual/api/reference/mediatype/object#способ_оповещения)

Доступные методы:\

-   [mediatype.create](/ru/manual/api/reference/mediatype/create) -
    создание новых способов оповещения
-   [mediatype.delete](/ru/manual/api/reference/mediatype/delete) -
    удаление способов оповещения
-   [mediatype.get](/ru/manual/api/reference/mediatype/get) - получение
    способов оповещения
-   [mediatype.update](/ru/manual/api/reference/mediatype/update) -
    обновление способов оповещения

[comment]: # ({/new-6bf4ac44})
