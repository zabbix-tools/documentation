[comment]: # translation:outdated

[comment]: # ({new-1359fe15})
# configuration.import

[comment]: # ({/new-1359fe15})

[comment]: # ({new-fe885a4a})
### Описание

`логический configuration.import(объект параметры)`

Этот метод позволяет импортировать данные конфигурации из
сериализованной строки.

[comment]: # ({/new-fe885a4a})

[comment]: # ({new-147fd272})
### Параметры

`(объект)` Параметры, которые содержат импортируемые данные конфигурации
и правила каким образом необходимо обрабатывать эти данные.

|Параметр|Тип|Описание|
|----------------|------|----------------|
|**format**<br>(требуется)|строка|Формат сериализованной строки.<br><br>Возможные значения:<br>`json` - JSON;<br>`xml` - XML.|
|**source**<br>(требуется)|строка|Сериализованная строка, которая содержит данные конфигурации.|
|**rules**<br>(требуется)|объект|Правила, каким образом необходимо импортировать новые и существующие объекты.<br><br>Параметр `rules` детально описан в таблице ниже.|

::: notetip
Если правила не заданы, конфигурация не будет
обновляться.
:::

Объект `rules` поддерживает следующие параметры.

|Параметр|Тип|Описание|
|----------------|------|----------------|
|applications|объект|Правила, каким образом импортировать группы элементов данных.<br><br>Поддерживаемые параметры:<br>`createMissing` - `(логический)` Если задано значение `true`, будут созданы новые группы элементов данных; по умолчанию: `false`;<br>`deleteMissing` - `(логический)` если задано значение `true`, отсутствующие группы элементов данных в импортируемых данных будут удалены из базы данных; по умолчанию: `false`.|
|discoveryRules|объект|Правила, каким образом импортировать LLD правила.<br><br>Поддерживаемые параметры:<br>`createMissing` - `(логический)` Если задано значение `true`, будут созданы новые LLD правила; по умолчанию: `false`;<br>`updateExisting` - `(логический)` Если задано значение `true`, существующие LLD правила будут обновлены; по умолчанию: `false`;<br>`deleteMissing` - `(логический)` если задано значение `true`, отсутствующие LLD правила в импортируемых данных будут удалены из базы данных; по умолчанию: `false`.|
|graphs|объект|Правила, каким образом импортировать графики.<br><br>Поддерживаемые параметры:<br>`createMissing` - `(логический)` Если задано значение `true`, будут созданы новые графики; по умолчанию: `false`;<br>`updateExisting` - `(логический)` Если задано значение `true`, существующие графики будут обновлены; по умолчанию: `false`;<br>`deleteMissing` - `(логический)` если задано значение `true`, отсутствующие графики в импортируемых данных будут удалены из базы данных; по умолчанию: `false`.|
|groups|объект|Правила, каким образом импортировать группы узлов сети.<br><br>Поддерживаемые параметры:<br>`createMissing` - `(логический)` Если задано значение `true`, будут созданы новые группы узлов сети; по умолчанию: `false`.|
|hosts|объект|Правила, каким образом импортировать узлы сети.<br><br>Поддерживаемые параметры:<br>`createMissing` - `(логический)` Если задано значение `true`, будут созданы новые узлы сети; по умолчанию: `false`;<br>`updateExisting` - `(логический)` Если задано значение `true`, существующие узлы сети будут обновлены; по умолчанию: `false`.|
|httptests|объект|Правила, каким образом импортировать вэб-сценарии.<br><br>Поддерживаемые параметры:<br>`createMissing` - `(логический)` Если задано значение `true`, будут созданы новые вэб-сценарии; по умолчанию: `false`;<br>`updateExisting` - `(логический)` Если задано значение `true`, существующие вэб-сценарии будут обновлены; по умолчанию: `false`;<br>`deleteMissing` - `(логический)` если задано значение `true`, отсутствующие вэб-сценарии в импортируемых данных будут удалены из базы данных; по умолчанию: `false`.|
|images|объект|Правила, каким образом импортировать изображения.<br><br>Поддерживаемые параметры:<br>`createMissing` - `(логический)` Если задано значение `true`, будут созданы новые изображения; по умолчанию: `false`;<br>`updateExisting` - `(логический)` Если задано значение `true`, существующие изображения будут обновлены; по умолчанию: `false`.|
|items|объект|Правила, каким образом импортировать элементы данных.<br><br>Поддерживаемые параметры:<br>`createMissing` - `(логический)` Если задано значение `true`, будут созданы новые элементы данных; по умолчанию: `false`;<br>`updateExisting` - `(логический)` Если задано значение `true`, существующие элементы данных будут обновлены; по умолчанию: `false`;<br>`deleteMissing` - `(логический)` если задано значение `true`, отсутствующие элементы данных в импортируемых данных будут удалены из базы данных; по умолчанию: `false`.|
|maps|объект|Правила, каким образом импортировать карты сетей.<br><br>Поддерживаемые параметры:<br>`createMissing` - `(логический)` Если задано значение `true`, будут созданы новые карты сетей; по умолчанию: `false`;<br>`updateExisting` - `(логический)` Если задано значение `true`, существующие карты сетей будут обновлены; по умолчанию: `false`.|
|screens|объект|Правила, каким образом импортировать комплексные экраны.<br><br>Поддерживаемые параметры:<br>`createMissing` - `(логический)` Если задано значение `true`, будут созданы новые комплексные экраны; по умолчанию: `false`;<br>`updateExisting` - `(логический)` Если задано значение `true`, существующие комплексные экраны будут обновлены; по умолчанию: `false`.|
|templateLinkage|объект|Правила, каким образом импортировать соединения с шаблонами.<br><br>Поддерживаемые параметры:<br>`createMissing` - `(логический)` Если задано значение `true`, будут созданы новые соединения между шаблонами и узлами сети; по умолчанию: `false`.|
|templates|объект|Правила, каким образом импортировать шаблоны.<br><br>Поддерживаемые параметры:<br>`createMissing` - `(логический)` Если задано значение `true`, будут созданы новые шаблоны; по умолчанию: `false`;<br>`updateExisting` - `(логический)` Если задано значение `true`, существующие шаблоны будут обновлены; по умолчанию: `false`.|
|templateScreens|объект|Правила, каким образом импортировать комплексные экраны шаблонов.<br><br>Поддерживаемые параметры:<br>`createMissing` - `(логический)` Если задано значение `true`, будут созданы новые комплексные экраны шаблонов; по умолчанию: `false`;<br>`updateExisting` - `(логический)` Если задано значение `true`, существующие комплексные экраны шаблонов будут обновлены; по умолчанию: `false`;<br>`deleteMissing` - `(логический)` если задано значение `true`, отсутствующие комплексные экраны шаблонов в импортируемых данных будут удалены из базы данных; по умолчанию: `false`.|
|triggers|объект|Правила, каким образом импортировать триггеры.<br><br>Поддерживаемые параметры:<br>`createMissing` - `(логический)` Если задано значение `true`, будут созданы новые триггеры; по умолчанию: `false`;<br>`updateExisting` - `(логический)` Если задано значение `true`, существующие триггеры будут обновлены; по умолчанию: `false`;<br>`deleteMissing` - `(логический)` если задано значение `true`, отсутствующие триггеры в импортируемых данных будут удалены из базы данных; по умолчанию: `false`.|
|valueMaps|объект|Правила, каким образом импортировать преобразования значений.<br><br>Поддерживаемые параметры:<br>`createMissing` - `(логический)` Если задано значение `true`, будут созданы новые преобразования значений; по умолчанию: `false`;<br>`updateExisting` - `(логический)` Если задано значение `true`, существующие преобразования значения будут обновлены; по умолчанию: `false`.|

[comment]: # ({/new-147fd272})

[comment]: # ({new-08d02880})
### Возвращаемые значения

`(логический)` Возвращает `true` при успешном импорте.

[comment]: # ({/new-08d02880})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-0801380e})
#### Импорт узлов сети с элементами данных

Импорт узлов сети и элементов данных, которые имеются в строке XML. Если
какие-либо элементы данных отсутствуют в XML, они будут удалены из базы
данных, всё остальное останется не тронутым.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "configuration.import",
    "params": {
        "format": "xml",
        "rules": {
            "applications": {
                "createMissing": true,
                "deleteMissing": false
            },
            "valueMaps": {
                "createMissing": true,
                "updateExisting": false
            },
            "httptests": {
                "createMissing": true,
                "updateExisting": true
            },
            "hosts": {
                "createMissing": true,
                "updateExisting": true
            },
            "items": {
                "createMissing": true,
                "updateExisting": true,
                "deleteMissing": true
            }
        },
        "source": "<?xml version=\"1.0\" encoding=\"UTF-8\"?><zabbix_export><version>4.0</version><date>2012-04-18T11:20:14Z</date><groups><group><name>Zabbix servers</name></group></groups><hosts><host><host>Export host</host><name>Export host</name><description/><proxy/><status>0</status><ipmi_authtype>-1</ipmi_authtype><ipmi_privilege>2</ipmi_privilege><ipmi_username/><ipmi_password/><tls_connect>1</tls_connect><tls_accept>1</tls_accept><tls_issuer/><tls_subject/><tls_psk_identity/><tls_psk/><templates/><groups><group><name>Zabbix servers</name></group></groups><interfaces><interface><default>1</default><type>1</type><useip>1</useip><ip>127.0.0.1</ip><dns/><port>10050</port><bulk>1</bulk><interface_ref>if1</interface_ref></interface></interfaces><applications><application><name>Application</name></application></applications><items><item><name>Item</name><type>0</type><snmp_community/><snmp_oid/><key>item.key</key><delay>30s</delay><history>90d</history><trends>365d</trends><status>0</status><value_type>3</value_type><allowed_hosts/><units/><snmpv3_contextname/><snmpv3_securityname/><snmpv3_securitylevel>0</snmpv3_securitylevel><snmpv3_authprotocol>0</snmpv3_authprotocol><snmpv3_authpassphrase/><snmpv3_privprotocol>0</snmpv3_privprotocol><snmpv3_privpassphrase/><params/><ipmi_sensor/><authtype>0</authtype><username/><password/><publickey/><privatekey/><port/><description/><inventory_link>0</inventory_link><applications><application><name>Application</name></application></applications><valuemap><name>Host status</name></valuemap><logtimefmt/><preprocessing/><interface_ref>if1</interface_ref><jmx_endpoint/><timeout>3s</timeout><url/><query_fields/><posts/><status_codes>200</status_codes><follow_redirects>1</follow_redirects><post_type>0</post_type><http_proxy/><headers/><retrieve_mode>0</retrieve_mode><request_method>1</request_method><output_format>0</output_format><allow_traps>0</allow_traps><ssl_cert_file/><ssl_key_file/><ssl_key_password/><verify_peer>0</verify_peer><verify_host>0</verify_host><master_item/></item></items><discovery_rules/><macros/><inventory/></host></hosts><triggers><trigger><expression>{Export host:item.key.last()}=0</expression><name>Trigger</name><url/><status>0</status><priority>2</priority><description>Host trigger</description><type>0</type><recovery_mode>1</recovery_mode><recovery_expression>{Export host:item.key.last()}=2</recovery_expression><dependencies/><tags/><correlation_mode>1</correlation_mode><correlation_tag>Tag 01</correlation_tag><manual_close>0</manual_close></trigger></triggers><graphs><graph><name>Graph</name><width>900</width><height>200</height><yaxismin>0.0000</yaxismin><yaxismax>100.0000</yaxismax><show_work_period>1</show_work_period><show_triggers>1</show_triggers><type>0</type><show_legend>1</show_legend><show_3d>0</show_3d><percent_left>0.0000</percent_left><percent_right>0.0000</percent_right><ymin_type_1>0</ymin_type_1><ymax_type_1>0</ymax_type_1><ymin_item_1>0</ymin_item_1><ymax_item_1>0</ymax_item_1><graph_items><graph_item><sortorder>0</sortorder><drawtype>0</drawtype><color>C80000</color><yaxisside>0</yaxisside><calc_fnc>7</calc_fnc><type>0</type><item><host>Export host</host><key>item.key</key></item></graph_item></graph_items></graph></graphs><value_maps><value_map><name>Host status</name><mappings><mapping><value>0</value><newvalue>Up</newvalue></mapping><mapping><value>2</value><newvalue>Unreachable</newvalue></mapping></mappings></value_map></value_maps></zabbix_export>"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": true,
    "id": 1
}
```

[comment]: # ({/new-0801380e})

[comment]: # ({new-c5744b74})
### Исходный код

CConfiguration::import() в
*ui/include/classes/api/services/Configuration.php*.

[comment]: # ({/new-c5744b74})
