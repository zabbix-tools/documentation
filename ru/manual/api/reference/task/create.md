[comment]: # translation:outdated

[comment]: # ({new-c9bc126a})
# task.create

[comment]: # ({/new-c9bc126a})

[comment]: # ({new-0e3179d3})
### Описание

`объект task.create(объект задача)`

Этот метод позволяет создавать новую задачу.

[comment]: # ({/new-0e3179d3})

[comment]: # ({new-2574c234})
### Параметры

`(объект)` Создаваемые задачи.

Этот метод принимает следующие параметры.

|Параметр|Тип|Описание|
|----------------|------|----------------|
|**type**<br>(требуется)|целое число|Тип задачи.<br><br>Возможные значения:<br>6 - Проверить сейчас.|
|**itemids**<br>(требуется)|строка/массив|ID элементов данных и правил низкоуровневого обнаружения.<br><br>Элементы данных и правила обнаружения должны быть следующих типов:<br>0 - Zabbix агент;<br>1 - SNMPv1 агент;<br>3 - простая проверка;<br>4 - SNMPv2 агент;<br>5 - Zabbix внутренний;<br>6 - SNMPv3 агент;<br>8 - Zabbix агрегированный;<br>10 - внешняя проверка;<br>11 - монитор баз данных;<br>12 - IPMI агент;<br>13 - SSH агент;<br>14 - TELNET агент;<br>15 - вычисляемый;<br>16 - JMX агент.|

[comment]: # ({/new-2574c234})

[comment]: # ({new-918de761})
If item or discovery ruls is of type *Dependent item*, then top level master item must be of type:
-   Zabbix agent
-   SNMPv1/v2/v3 agent
-   Simple check
-   Internal check
-   External check
-   Database monitor
-   HTTP agent
-   IPMI agent
-   SSH agent
-   TELNET agent
-   Calculated check
-   JMX agent

[comment]: # ({/new-918de761})

[comment]: # ({new-ab87ce2a})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID созданных задач под
свойством `taskids`. Одна задача создается по каждому элементу данных и
правилу низкоуровневого обнаружения. Порядок возвращаемых ID совпадает с
порядком переданных `itemids`.

[comment]: # ({/new-ab87ce2a})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-5c5fa6f1})
#### Создание задачи

Создание задачи `проверить сейчас` по двум элементам. Один из них
элемент данных, второй правило низкоуровневого обнаружения.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "task.create",
    "params": {
        "type": "6",
        "itemids": ["10092", "10093"],
    },
    "auth": "700ca65537074ec963db7efabda78259",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "taskids": [
            "1",
            "2"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-5c5fa6f1})

[comment]: # ({new-d87c906b})
### Исходный код

CTask::create() в
*frontends/php/include/classes/api/services/CTask.php*.

[comment]: # ({/new-d87c906b})


[comment]: # ({new-be350bd3})
### Source

CTask::create() in *ui/include/classes/api/services/CTask.php*.

[comment]: # ({/new-be350bd3})
