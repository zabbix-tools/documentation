[comment]: # translation:outdated

[comment]: # ({new-52ad42a2})
# Конфигурация

Этот класс предназначен для экспорта и импорта данных конфигурации
Zabbix.

Доступные методы:\

-   [configuration.export](/ru/manual/api/reference/configuration/export) -
    экспорт конфигурации
-   [configuration.import](/ru/manual/api/reference/configuration/import) -
    импорт конфигурации

[comment]: # ({/new-52ad42a2})
