[comment]: # translation:outdated

[comment]: # ({new-5c9f356a})
# iconmap.delete

[comment]: # ({/new-5c9f356a})

[comment]: # ({new-5e250e88})
### Описание

`объект iconmap.delete(массив iconMapIds)`

Этот метод позволяет удалять соответствия иконок.

[comment]: # ({/new-5e250e88})

[comment]: # ({new-f911029a})
### Параметры

`(массив)` ID удаляемых соответствий иконок.

[comment]: # ({/new-f911029a})

[comment]: # ({new-44afa603})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID удаленных соответствий
иконок под свойством `iconmapids`.

[comment]: # ({/new-44afa603})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-c7690d07})
#### Удаление нескольких соответствий иконок

Удаление двух соответствий иконок.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "iconmap.delete",
    "params": [
        "2",
        "5"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "iconmapids": [
            "2",
            "5"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-c7690d07})

[comment]: # ({new-6198efd4})
### Исходный код

CIconMap::delete() в
*frontends/php/include/classes/api/services/CIconMap.php*.

[comment]: # ({/new-6198efd4})
