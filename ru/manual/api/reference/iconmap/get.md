[comment]: # translation:outdated

[comment]: # ({new-8e76cf8c})
# iconmap.get

[comment]: # ({/new-8e76cf8c})

[comment]: # ({new-04265e54})
### Описание

`целое число/массив iconmap.get(объект параметры)`

Этот метод позволяет получать соответствия иконок в соответствии с
заданными параметрами.

[comment]: # ({/new-04265e54})

[comment]: # ({new-efcde208})
### Параметры

`(объект)` Параметры задают желаемый вывод.

Этот метод поддерживает следующие параметры.

|Параметр|Тип|Описание|
|----------------|------|----------------|
|iconmapids|строка/массив|Возврат соответствий иконок только с заданными ID.|
|sysmapids|строка/массив|Возврат только тех соответствий иконок, которые используются заданными картами сети.|
|selectMappings|запрос|Возврат используемых карт иконок в свойстве `mappings`.|
|sortfield|строка/массив|Сортировка результата в соответствии с заданными свойствами.<br><br>Возможные значения: `iconmapid` и `name`.|
|countOutput|логический|Эти параметры являются общими для всех методов `get` и они описаны в [справочных комментариях](/ru/manual/api/reference_commentary#общие_параметры_get_метода).|
|editable|логический|^|
|excludeSearch|логический|^|
|filter|объект|^|
|limit|целое число|^|
|output|запрос|^|
|preservekeys|логический|^|
|search|объект|^|
|searchByAny|логический|^|
|searchWildcardsEnabled|логический|^|
|sortorder|строка/массив|^|
|startSearch|логический|^|

[comment]: # ({/new-efcde208})

[comment]: # ({new-7223bab1})
### Возвращаемые значения

`(целое число/массив)` Возвращает либо:

-   массив объектов;
-   количество найденных объектов, если используется параметр
    `countOutput`.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-016ff9fc})
#### Получение соответствия иконок

Получение всех данных соответствия иконок "3".

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "iconmap.get",
    "params": {
        "iconmapids": "3",
        "output": "extend",
        "selectMappings": "extend"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "mappings": [
                {
                    "iconmappingid": "3",
                    "iconmapid": "3",
                    "iconid": "6",
                    "inventory_link": "1",
                    "expression": "server",
                    "sortorder": "0"
                },
                {
                    "iconmappingid": "4",
                    "iconmapid": "3",
                    "iconid": "10",
                    "inventory_link": "1",
                    "expression": "switch",
                    "sortorder": "1"
                }
            ],
            "iconmapid": "3",
            "name": "Host type icons",
            "default_iconid": "2"
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-016ff9fc})

[comment]: # ({new-8df05cf6})
### Смотрите также

-   [Карта иконок](object#карта_иконок)

[comment]: # ({/new-8df05cf6})

[comment]: # ({new-2420d0f0})
### Исходный код

CIconMap::get() в
*frontends/php/include/classes/api/services/CIconMap.php*.

[comment]: # ({/new-2420d0f0})
