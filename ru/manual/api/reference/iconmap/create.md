[comment]: # translation:outdated

[comment]: # ({new-94ac227e})
# iconmap.create

[comment]: # ({/new-94ac227e})

[comment]: # ({new-ac3800fa})
### Описание

`объект iconmap.create(объект/массив СоответвияИконок)`

Этот метод позволяет создавать новые соответствия иконок.

[comment]: # ({/new-ac3800fa})

[comment]: # ({new-ca8671b9})
### Параметры

`(объект/массив)` Создаваемые соответствия иконок.

В дополнение к [стандартным свойствам соответствий
иконок](object#соответствие_иконок), этот метод принимает следующие
параметры.

|Параметр|Тип|Описание|
|----------------|------|----------------|
|**mappings**<br>(требуется)|массив|Создаваемые карты иконок для соответствия иконок.|

[comment]: # ({/new-ca8671b9})

[comment]: # ({new-9dbb2dc6})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID созданных соответствий
иконок под свойством `iconmapids`. Порядок возвращаемых ID совпадает с
порядком переданных соответствий иконок.

[comment]: # ({/new-9dbb2dc6})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-39a9c07f})
#### Создание соответствия иконок

Создание соответствия иконок для отображения разных типов узлов сети.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "iconmap.create",
    "params": {
        "name": "Type icons",
        "default_iconid": "2",
        "mappings": [
            {
                "inventory_link": 1,
                "expression": "server",
                "iconid": "3"
            },
            {
                "inventory_link": 1,
                "expression": "switch",
                "iconid": "4"
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "iconmapids": [
            "2"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-39a9c07f})

[comment]: # ({new-8df05cf6})
### Смотрите также

-   [Карта иконок](object#карта_иконок)

[comment]: # ({/new-8df05cf6})

[comment]: # ({new-25aae98d})
### Исходный код

CIconMap::create() в
*frontends/php/include/classes/api/services/CIconMap.php*.

[comment]: # ({/new-25aae98d})
