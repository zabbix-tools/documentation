[comment]: # translation:outdated

[comment]: # ({new-7e2bad53})
# Прототип элементов данных

Этот класс предназначен для работы с прототипами элементов данных.

Справка по объектам:\

-   [Прототип элементов
    данных](/ru/manual/api/reference/itemprototype/object#прототип_элементов_данных)

Доступные методы:\

-   [itemprototype.create](/ru/manual/api/reference/itemprototype/create) -
    создание новых прототипов элементов данных
-   [itemprototype.delete](/ru/manual/api/reference/itemprototype/delete) -
    удаление прототипов элементов данных
-   [itemprototype.get](/ru/manual/api/reference/itemprototype/get) -
    получение прототипов элементов данных
-   [itemprototype.update](/ru/manual/api/reference/itemprototype/update) -
    обновление прототипов элементов данных

[comment]: # ({/new-7e2bad53})
