[comment]: # translation:outdated

[comment]: # ({77d8c140-06845577})
# Действие

Этот класс предназначен для работы с действиями.

Справка по объектам:\

-   [Действие](/manual/api/reference/action/object#action)
-   [Условие действия](/manual/api/reference/action/object#action_condition)
-   [Операция действия](/manual/api/reference/action/object#action_operation)

Доступные методы:\

-   [action.create](/manual/api/reference/action/create) - создание новых действий
-   [action.delete](/manual/api/reference/action/delete) - удаление действий
-   [action.get](/manual/api/reference/action/get) - получение действия
-   [action.update](/manual/api/reference/action/update) - обновление действий

[comment]: # ({/77d8c140-06845577})
