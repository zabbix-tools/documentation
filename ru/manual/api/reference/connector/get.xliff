<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="ru" datatype="plaintext" original="manual/api/reference/connector/get.md">
    <body>
      <trans-unit id="0fc7ba20" xml:space="preserve">
        <source># connector.get</source>
      </trans-unit>
      <trans-unit id="c152f697" xml:space="preserve">
        <source>### Description

`integer/array connector.get(object parameters)`

The method allows to retrieve connector objects according to the given parameters.

::: noteclassic
This method is only available to *Super admin* user type. Permissions to call the method can be revoked in user role settings.
See [User roles](/manual/web_interface/frontend_sections/users/user_roles) for more information.
:::</source>
      </trans-unit>
      <trans-unit id="b096237f" xml:space="preserve">
        <source>### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|connectorids|string/array|Return only connectors with the given IDs.|
|selectTags|query|Return a `tags` property with connector [tag filter](/manual/api/reference/connector/object#tag-filter).&lt;br&gt;&lt;br&gt;Supports `count`.|
|sortfield|string/array|Sort the result by the given properties.&lt;br&gt;&lt;br&gt;Possible values: `connectorid`, `name`, `data_type`, `status`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters).|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|</source>
      </trans-unit>
      <trans-unit id="830d8762" xml:space="preserve">
        <source>### Return values

`(integer/array)` Returns either:

- an array of objects;
- the count of retrieved objects, if the `countOutput` parameter has been used.</source>
      </trans-unit>
      <trans-unit id="122d6c45" xml:space="preserve">
        <source>### Examples</source>
      </trans-unit>
      <trans-unit id="3a1b6d17" xml:space="preserve">
        <source>#### Retrieving all connectors

Retrieve all data about all connectors and their properties.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "connector.get",
    "params": {
        "output": "extend",
        "selectTags": ["tag", "operator", "value"],
        "preservekeys": true
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": [
        {
            "connectorid": "1",
            "name": "Export of item values",
            "protocol": "0",
            "data_type": "0",
            "url": "{$DATA_EXPORT_VALUES_URL}",
            "max_records": "0",
            "max_senders": "4",
            "max_attempts": "2",
            "timeout": "10s",
            "http_proxy": "{$DATA_EXPORT_VALUES_PROXY}",
            "authtype": "4",
            "username": "{$DATA_EXPORT_VALUES_USERNAME}",
            "password": "{$DATA_EXPORT_VALUES_PASSWORD}",
            "token": "",
            "verify_peer": "1",
            "verify_host": "1",
            "ssl_cert_file": "{$DATA_EXPORT_VALUES_SSL_CERT_FILE}",
            "ssl_key_file": "{$DATA_EXPORT_VALUES_SSL_KEY_FILE}",
            "ssl_key_password": "",
            "description": "",
            "status": "1",
            "tags_evaltype": "0",
            "tags": [
                {
                    "tag": "component",
                    "operator": "0",
                    "value": "memory"
                }
            ]
        },
        {
            "connectorid": "2",
            "name": "Export of events",
            "protocol": "0",
            "data_type": "1",
            "url": "{$DATA_EXPORT_EVENTS_URL}",
            "max_records": "0",
            "max_senders": "2",
            "max_attempts": "2",
            "timeout": "5s",
            "http_proxy": "",
            "authtype": "5",
            "username": "",
            "password": "",
            "token": "{$DATA_EXPORT_EVENTS_BEARER_TOKEN}",
            "verify_peer": "1",
            "verify_host": "1",
            "ssl_cert_file": "",
            "ssl_key_file": "",
            "ssl_key_password": "",
            "description": "",
            "status": "1",
            "tags_evaltype": "0",
            "tags": [
                {
                    "tag": "scope",
                    "operator": "0",
                    "value": "performance"
                }
            ]
        }
    ],
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="4963c0b1" xml:space="preserve">
        <source>### Source

CConnector:get() in *ui/include/classes/api/services/CConnector.php*.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
