[comment]: # translation:outdated

[comment]: # ({new-226837df})
# proxy.delete

[comment]: # ({/new-226837df})

[comment]: # ({new-a16e4b7f})
### Описание

`объект proxy.delete(массив прокси)`

Этот метод позволяет удалять прокси.

[comment]: # ({/new-a16e4b7f})

[comment]: # ({new-0c678a75})
### Параметры

`(массив)` ID удаляемых прокси.

[comment]: # ({/new-0c678a75})

[comment]: # ({new-426b06a5})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID удаленных прокси под
свойством `proxyids`.

[comment]: # ({/new-426b06a5})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-dcfc3683})
#### Удаление нескольких прокси

Удаление двух прокси.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "proxy.delete",
    "params": [
        "10286",
        "10285"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "proxyids": [
            "10286",
            "10285"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-dcfc3683})

[comment]: # ({new-4dd75afc})
### Исходный код

CProxy::delete() в
*frontends/php/include/classes/api/services/CProxy.php*.

[comment]: # ({/new-4dd75afc})
