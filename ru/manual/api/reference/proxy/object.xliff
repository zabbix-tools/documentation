<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="ru" datatype="plaintext" original="manual/api/reference/proxy/object.md">
    <body>
      <trans-unit id="7b0425dd" xml:space="preserve">
        <source># &gt; Proxy object

The following objects are directly related to the `proxy` API.</source>
      </trans-unit>
      <trans-unit id="52ccb0be" xml:space="preserve">
        <source>### Proxy

The proxy object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|proxyid|string|ID of the proxy.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *read-only*&lt;br&gt;- *required* for update operations|
|host|string|Name of the proxy.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* for create operations|
|status|integer|Type of proxy.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;5 - active proxy;&lt;br&gt;6 - passive proxy.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* for create operations|
|description|text|Description of the proxy.|
|lastaccess|timestamp|Time when the proxy last connected to the server.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *read-only*|
|tls\_connect|integer|Connections to host.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;1 - *(default)* No encryption;&lt;br&gt;2 - PSK;&lt;br&gt;4 - certificate.|
|tls\_accept|integer|Connections from host.&lt;br&gt;This is a bitmask field, any combination of possible bitmap values is acceptable.&lt;br&gt;&lt;br&gt;Possible bitmap values:&lt;br&gt;1 - *(default)* No encryption;&lt;br&gt;2 - PSK;&lt;br&gt;4 - certificate.|
|tls\_issuer|string|Certificate issuer.|
|tls\_subject|string|Certificate subject.|
|tls\_psk\_identity|string|PSK identity.&lt;br&gt;Do not put sensitive information in the PSK identity, it is transmitted unencrypted over the network to inform a receiver which PSK to use.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *write-only*&lt;br&gt;- *required* if `tls_connect` is set to "PSK", or `tls_accept` contains the "PSK" bit|
|tls\_psk|string|The preshared key, at least 32 hex digits.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *write-only*&lt;br&gt;- *required* if `tls_connect` is set to "PSK", or `tls_accept` contains the "PSK" bit|
|proxy\_address|string|Comma-delimited IP addresses or DNS names of active Zabbix proxy.|
|auto\_compress|integer|Indicates if communication between Zabbix server and proxy is compressed.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - No compression;&lt;br&gt;1 - Compression enabled.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *read-only*|
|version|integer|Version of proxy.&lt;br&gt;&lt;br&gt;Three-part Zabbix version number, where two decimal digits are used for each part, e.g., 50401 for version 5.4.1, 60200 for version 6.2.0, etc.&lt;br&gt;0 - Unknown proxy version.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *read-only*|
|compatibility|integer|Version of proxy compared to Zabbix server version.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - Undefined;&lt;br&gt;1 - Current version (proxy and server have the same major version);&lt;br&gt;2 - Outdated version (proxy version is older than server version, but is partially supported);&lt;br&gt;3 - Unsupported version (proxy version is older than server previous LTS release version or server major version is older than proxy major version).&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *read-only*|</source>
      </trans-unit>
      <trans-unit id="4e21547f" xml:space="preserve">
        <source>### Proxy interface

The proxy interface object defines the interface used to connect to a passive proxy. It has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|dns|string|DNS name to connect to.&lt;br&gt;&lt;br&gt;Can be empty if connections are made via IP address.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* if `useip` is set to "connect using DNS name"|
|ip|string|IP address to connect to.&lt;br&gt;&lt;br&gt;Can be empty if connections are made via DNS names.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* if `useip` is set to "connect using IP address"|
|port|string|Port number to connect to.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|
|useip|integer|Whether the connection should be made via IP address.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - connect using DNS name;&lt;br&gt;1 - connect using IP address.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|</source>
      </trans-unit>
    </body>
  </file>
</xliff>
