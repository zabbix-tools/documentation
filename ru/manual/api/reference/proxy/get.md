[comment]: # translation:outdated

[comment]: # ({new-70dbc2c9})
# proxy.get

[comment]: # ({/new-70dbc2c9})

[comment]: # ({new-b91ae38b})
### Description

`целое число/массив proxy.get(объект параметры)`

Этот метод позволяет получать прокси в соответствии с заданными
параметрами.

[comment]: # ({/new-b91ae38b})

[comment]: # ({new-74f0aaac})
### Параметры

`(объект)` Параметры задают желаемый вывод.

Этот метод поддерживает следующие параметры.

|Параметр|Тип|Описание|
|----------------|------|----------------|
|proxyids|строка/массив|Возврат прокси только с заданными ID.|
|selectHosts|запрос|Возврат узлов сети, которые наблюдаются через прокси, в свойстве `hosts`.|
|selectInterface|запрос|Возврат интерфейса прокси, который используется пассивным прокси, в свойстве `interface`.|
|sortfield|строка/массив|Сортировка результата в соответствии с заданными свойствами.<br><br>Возможные значения: `hostid`, `host` и `status`.|
|countOutput|логический|Эти параметры являются общими для всех методов `get` и они описаны в [справочных комментариях](/ru/manual/api/reference_commentary#общие_параметры_get_метода).|
|editable|логический|^|
|excludeSearch|логический|^|
|filter|объект|^|
|limit|целое число|^|
|output|запрос|^|
|preservekeys|фл логический аг|^|
|search|объект|^|
|searchByAny|логический|^|
|searchWildcardsEnabled|логический|^|
|sortorder|строка/массив|^|
|startSearch|логический|^|

[comment]: # ({/new-74f0aaac})

[comment]: # ({new-7223bab1})
### Возвращаемые значения

`(целое число/массив)` Возвращает либо:

-   массив объектов;
-   количество найденных объектов, если используется параметр
    `countOutput`.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-b40fb513})
#### Получение всех прокси

Получение всех добавленных прокси и их интерфейсов.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "proxy.get",
    "params": {
        "output": "extend",
        "selectInterface": "extend"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "interface": [],
            "host": "Active proxy",
            "status": "5",
            "lastaccess": "0",
            "proxyid": "30091",
            "description": "",
            "tls_connect": "1",
            "tls_accept": "1",
            "tls_issuer": "",
            "tls_subject": "",
            "tls_psk_identity": "",
            "tls_psk": ""
        },
        {
            "interface": {
                "interfaceid": "30109",
                "hostid": "30092",
                "useip": "1",
                "ip": "127.0.0.1",
                "dns": "",
                "port": "10051"
            ],
            "host": "Passive proxy",
            "status": "6",
            "lastaccess": "0",
            "proxyid": "30092",
            "description": ""
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-b40fb513})

[comment]: # ({new-273e0fd8})
### Смотрите также

-   [Узел сети](/ru/manual/api/reference/host/object#узел_сети)
-   [Интерфейс прокси](object#интерфейс_прокси)

[comment]: # ({/new-273e0fd8})

[comment]: # ({new-b9a96f2c})
### Исходный код

CProxy::get() в *frontends/php/include/classes/api/services/CProxy.php*.

[comment]: # ({/new-b9a96f2c})
