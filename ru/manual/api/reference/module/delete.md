[comment]: # translation:outdated

[comment]: # ({new-67173444})
# module.delete

[comment]: # ({/new-67173444})

[comment]: # ({new-d77031eb})
### Description

`object module.delete(array moduleids)`

This method allows to uninstall modules.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings.
See [User roles](/manual/web_interface/frontend_sections/users/user_roles) for more information.
:::

::: noteimportant
Module files must be removed manually.
:::

[comment]: # ({/new-d77031eb})

[comment]: # ({new-80000d5f})
### Parameters

`(array)` IDs of the modules to uninstall.

[comment]: # ({/new-80000d5f})

[comment]: # ({new-aa920f60})
### Return values

`(object)` Returns an object containing the IDs of the uninstalled modules under the `moduleids` property.

[comment]: # ({/new-aa920f60})

[comment]: # ({new-74412f73})
### Examples

[comment]: # ({/new-74412f73})

[comment]: # ({new-ff2a05e3})
#### Uninstalling multiple modules

Uninstall modules "27" and "28".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "module.delete",
    "params": [
        "27",
        "28"
    ],
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "moduleids": [
            "27",
            "28"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-ff2a05e3})

[comment]: # ({new-489a0d8e})
### Source

CModule::delete() in *ui/include/classes/api/services/CModule.php*.

[comment]: # ({/new-489a0d8e})
