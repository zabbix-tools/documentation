[comment]: # translation:outdated

[comment]: # ({new-392125b2})
# Пользователь

Этот класс предназначен для работы с пользователями.

Справка по объектам:\

-   [Пользователь](/ru/manual/api/reference/user/object#пользователь)

Доступные методы:\

-   [user.create](/ru/manual/api/reference/user/create) - создание новых
    пользователей
-   [user.delete](/ru/manual/api/reference/user/delete) - удаление
    пользователей
-   [user.get](/ru/manual/api/reference/user/get) - получение
    пользователей
-   [user.login](/ru/manual/api/reference/user/login) - выполнение входа
    в API
-   [user.logout](/ru/manual/api/reference/user/logout) - выполнение
    выхода из API
-   [user.update](/ru/manual/api/reference/user/update) - обновление
    пользователей

[comment]: # ({/new-392125b2})
