[comment]: # translation:outdated

[comment]: # ({new-e35cf787})
# Обнаруженный узел сети

Этот класс предназначен для работы с обнаруженными узлами сети.

Справка по объектам:\

-   [Обнаруженный узел
    сети](/ru/manual/api/reference/dhost/object#обнаруженный_узел_сети)

Доступные методы:\

-   [dhost.get](/ru/manual/api/reference/dhost/get) - получение
    обнаруженных узлов сети

[comment]: # ({/new-e35cf787})
