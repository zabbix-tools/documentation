[comment]: # translation:outdated

[comment]: # ({new-0d3af930})
# maintenance.get

[comment]: # ({/new-0d3af930})

[comment]: # ({new-a04dd4c9})
### Описание

`целое число/массив maintenance.get(объект параметры)`

Этот метод позволяет получать обслуживания в соответствии с заданными
параметрами.

[comment]: # ({/new-a04dd4c9})

[comment]: # ({new-9ca798d7})
### Параметры

`(объект)` Параметры задают желаемый вывод.

Этот метод поддерживает следующие параметры.

|Параметр|Тип|Описание|
|----------------|------|----------------|
|groupids|строка/массив|Возврат только тех обслуживаний, которые назначены на заданные группы узлов сети.|
|hostids|строка/массив|Возврат только тех обслуживаний, которые назначены на заданные узлы сети.|
|maintenanceids|строка/массив|Возврат обслуживаний только с заданными ID.|
|selectGroups|запрос|Возврат групп узлов сети, которые назначены на обслуживание, в свойстве `groups`.|
|selectHosts|запрос|Возврат узлов сети, которые назначены на обслуживание, в свойстве `hosts`.|
|selectTimeperiods|запрос|Возврат периодов времени обслуживания в свойстве `timeperiods`.|
|selectTags|запрос|Возврат тегов проблем обслуживания в свойстве `tags`.|
|sortfield|строка/массив|Сортировка результата в соответствии с заданными свойствами.<br><br>Возможные значения: `maintenanceid`, `name` и `maintenance_type`.|
|countOutput|логический|Эти параметры являются общими для всех методов `get` и они описаны в [справочных комментариях](/ru/manual/api/reference_commentary#общие_параметры_get_метода).|
|editable|логический|^|
|excludeSearch|логический|^|
|filter|объект|^|
|limit|целое число|^|
|output|запрос|^|
|preservekeys|логический|^|
|search|объект|^|
|searchByAny|логический|^|
|searchWildcardsEnabled|логический|^|
|sortorder|строка/массив|^|
|startSearch|логический|^|

[comment]: # ({/new-9ca798d7})

[comment]: # ({new-7223bab1})
### Возвращаемые значения

`(целое число/массив)` Возвращает либо:

-   массив объектов;
-   количество найденных объектов, если используется параметр
    `countOutput`.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-0b1cb387})
#### Получение обслуживаний

Получение всех имеющихся обслуживаний, а также данных о назначенных
группах узлов сети, узлов сети и добавленных периодах времени.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "maintenance.get",
    "params": {
        "output": "extend",
        "selectGroups": "extend",
        "selectTimeperiods": "extend",
        "selectTags": "extend"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "maintenanceid": "3",
            "name": "Sunday maintenance",
            "maintenance_type": "0",
            "description": "",
            "active_since": "1358844540",
            "active_till": "1390466940",
            "tags_evaltype": "0",
            "groups": [
                {
                    "groupid": "4",
                    "name": "Zabbix servers",
                    "internal": "0"
                }
            ],
            "timeperiods": [
                {
                    "timeperiodid": "4",
                    "timeperiod_type": "3",
                    "every": "1",
                    "month": "0",
                    "dayofweek": "1",
                    "day": "0",
                    "start_time": "64800",
                    "period": "3600",
                    "start_date": "2147483647"
                }
            ],
            "tags": [
                {
                    "tag": "tag1",
                    "operator": "0",
                    "value": "value1",
                },
                {
                    "tag": "tag2",
                    "operator": "2",
                    "value": "value2",
                }
            ]
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-0b1cb387})

[comment]: # ({new-dbcf678a})
### Смотрите также

-   [Узел сети](/ru/manual/api/reference/host/object#узел_сети)
-   [Группа узлов
    сети](/ru/manual/api/reference/hostgroup/object#группа_узлов_сети)
-   [Период времени](object#период_времени)

[comment]: # ({/new-dbcf678a})

[comment]: # ({new-50ed5be3})
### Исходный код

CMaintenance::get() в
*frontends/php/include/classes/api/services/CMaintenance.php*.

[comment]: # ({/new-50ed5be3})
