[comment]: # translation:outdated

[comment]: # ({new-0aafdb81})
# maintenance.delete

[comment]: # ({/new-0aafdb81})

[comment]: # ({new-5afbd3a3})
### Описание

`объект maintenance.delete(массив maintenanceIds)`

Этот метод позволяет удалять обслуживания.

[comment]: # ({/new-5afbd3a3})

[comment]: # ({new-9828d580})
### Параметры

`(массив)` ID удаляемых обслуживаний.

[comment]: # ({/new-9828d580})

[comment]: # ({new-1bb5e665})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID удаленных обслуживаний
под свойством `maintenanceids`.

[comment]: # ({/new-1bb5e665})

[comment]: # ({new-b41637d2})
### Пример

[comment]: # ({/new-b41637d2})

[comment]: # ({new-9baa4c26})
#### Удаление нескольких обслуживаний

Удаление двух обслуживаний.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "maintenance.delete",
    "params": [
        "3",
        "1"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "maintenanceids": [
            "3",
            "1"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-9baa4c26})

[comment]: # ({new-87f95129})
### Исходный код

CMaintenance::delete() в
*frontends/php/include/classes/api/services/CMaintenance.php*.

[comment]: # ({/new-87f95129})
