[comment]: # translation:outdated

[comment]: # ({new-f47b150e})
# maintenance.create

[comment]: # ({/new-f47b150e})

[comment]: # ({new-e8498ebc})
### Описание

`объект maintenance.create(объект/массив обслуживания)`

Этот метод позволяет создавать новые обслуживания.

[comment]: # ({/new-e8498ebc})

[comment]: # ({new-6c14daac})
### Параметры

`(объект/массив)` Создаваемые обслуживания.

В дополнение к [стандартным свойствам
обслуживания](object#обслуживание), этот метод принимает следующие
параметры.

|Параметр|Тип|Описание|
|----------------|------|----------------|
|**groupids**<br>(требуется)|массив|ID групп узлов сети, которые должны затрагиваться обслуживанием.|
|**hostids**<br>(требуется)|массив|ID узлов сети, которые должны затрагиваться обслуживанием.|
|**timeperiods**<br>(требуется)|массив|Периоды времени обслуживания.|
|tags|массив|Теги проблем.|

::: noteimportant
По каждому обслуживанию необходимо указать по
крайней мере один узел сети или группа узлов сети.
:::

[comment]: # ({/new-6c14daac})

[comment]: # ({new-48c4869c})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID созданных обслуживаний
под свойством `maintenanceids`. Порядок возвращаемых ID совпадает с
порядком переданных обслуживаний.

[comment]: # ({/new-48c4869c})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-bb812c67})
#### Создание обслуживания

Создание обслуживания со сбором данных для группы узлов сети "2". Оно
должно быть активно с 22.01.2013 до 22.01.2014 и вступать в силу каждое
Воскресенье в 18:00 и длится один час.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "maintenance.create",
    "params": {
        "name": "Sunday maintenance",
        "active_since": 1358844540,
        "active_till": 1390466940,
        "tags_evaltype": 0,
        "groupids": [
            "2"
        ],
        "timeperiods": [
            {
                "timeperiod_type": 3,
                "every": 1,
                "dayofweek": 64,
                "start_time": 64800,
                "period": 3600
            }
        ],
        "tags": [
            {
                "tag": "tag1",
                "operator": "0",
                "value": "value1",
            },
            {
                "tag": "tag2",
                "operator": "2",
                "value": "value2",
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "maintenanceids": [
            "3"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-bb812c67})

[comment]: # ({new-82b3ef0e})
### Смотрите также

-   [Период времени](object#период)времени)

[comment]: # ({/new-82b3ef0e})

[comment]: # ({new-3e3a556f})
### Исходный код

CMaintenance::create() в
*frontends/php/include/classes/api/services/CMaintenance.php*.

[comment]: # ({/new-3e3a556f})
