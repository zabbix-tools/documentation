[comment]: # translation:outdated

[comment]: # ({new-b8d971b2})
# График

Этот класс предназначен для работы с графиками.

Справка по объектам:\

-   [График](/ru/manual/api/reference/graph/object#график)

Доступные методы:\

-   [graph.create](/ru/manual/api/reference/graph/create) - создание
    новых графиков
-   [graph.delete](/ru/manual/api/reference/graph/delete) - удаление
    графиков
-   [graph.get](/ru/manual/api/reference/graph/get) - получение графиков
-   [graph.update](/ru/manual/api/reference/graph/update) - обновление
    графиков

[comment]: # ({/new-b8d971b2})
