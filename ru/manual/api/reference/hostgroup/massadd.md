[comment]: # translation:outdated

[comment]: # ({new-3f9b711d})
# hostgroup.massadd

[comment]: # ({/new-3f9b711d})

[comment]: # ({new-30b8c9f4})
### Описание

`объект hostgroup.massadd(объект параметры)`

Этот метод позволяет добавить одновременно несколько связанных объектов
во все заданные группы узлов сети.

[comment]: # ({/new-30b8c9f4})

[comment]: # ({new-b8ff6430})
### Параметры

`(объект)` Параметры, которые содержат ID обновляемых групп узлов сети и
добавляемых объектов в во все эти группы узлов сети.

Этот метод принимает следующие параметры.

|Параметр|Тип|Описание|
|----------------|------|----------------|
|**groups**<br>(требуется)|объект/массив|Обновляемые группы узлов сети.<br><br>У групп узлов сети должно быть задано свойство `groupid`.|
|hosts|объект/массив|Добавляемые узлы сети во все группы узлов сети.<br><br>У узлов сети должно быть задано свойство `hostid`.|
|templates|объект/массив|Добавляемые шаблоны во все группы узлов сети.<br><br>У шаблонов должно быть задано свойство `templateid`.|

[comment]: # ({/new-b8ff6430})

[comment]: # ({new-736f3b05})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID обновленных групп
узлов сети под свойством `groupids`.

[comment]: # ({/new-736f3b05})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-7698103b})
#### Добавление узлов сети в группы узлов сети

Добавление двух узлов сети в группы узлов сети с ID 5 и 6.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "hostgroup.massadd",
    "params": {
        "groups": [
            {
                "groupid": "5"
            },
            {
                "groupid": "6"
            }
        ],
        "hosts": [
            {
                "hostid": "30050"
            },
            {
                "hostid": "30001"
            }
        ]
    },
    "auth": "f223adf833b2bf2ff38574a67bba6372",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "groupids": [
            "5",
            "6"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-7698103b})

[comment]: # ({new-e5f0abe1})
### Смотрите также

-   [Узел сети](/ru/manual/api/reference/host/object#узел_сети)
-   [Шаблон](/ru/manual/api/reference/template/object#шаблон)

[comment]: # ({/new-e5f0abe1})

[comment]: # ({new-19273fb6})
### Исходный код

CHostGroup::massAdd() в
*frontends/php/include/classes/api/services/CHostGroup.php*.

[comment]: # ({/new-19273fb6})
