[comment]: # translation:outdated

[comment]: # ({new-5ebe3ff8})
# hostgroup.create

[comment]: # ({/new-5ebe3ff8})

[comment]: # ({new-06c5b0b6})
### Описание

`объект hostgroup.create(объект/массив ГруппыУзловсети)`

Этот метод позволяет создавать новые группы узлов сети.

[comment]: # ({/new-06c5b0b6})

[comment]: # ({new-7c6d41ff})
### Параметры

`(объект/массив)` Создаваемые группы узлов сети. Этот метод принимает
группы узлов сети со [стандартными свойствами групп узлов
сети](object#группа_узлов_сети).

[comment]: # ({/new-7c6d41ff})

[comment]: # ({new-e3c164f1})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID созданных групп узлов
сети под свойством `groupids`. Порядок возвращаемых ID совпадает с
порядком переданных групп узлов сети.

[comment]: # ({/new-e3c164f1})

[comment]: # ({new-b41637d2})
### Пример

[comment]: # ({/new-b41637d2})

[comment]: # ({new-37cc26c2})
#### Создание группы узлов сети

Создание группы узлов сети с именем "Linux servers".

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "hostgroup.create",
    "params": {
        "name": "Linux servers"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "groupids": [
            "107819"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-37cc26c2})

[comment]: # ({new-df4dc6d7})
### Исходный код

CHostGroup::create() в
*frontends/php/include/classes/api/services/CHostGroup.php*.

[comment]: # ({/new-df4dc6d7})
