[comment]: # translation:outdated

[comment]: # ({new-f8d4c2e5})
# hostgroup.get

[comment]: # ({/new-f8d4c2e5})

[comment]: # ({new-08ff22f2})
### Описание

`целое число/массив hostgroup.get(объект параметры)`

Этот метод позволяет получать группы узлов сети в соответствии с
заданными параметрами.

[comment]: # ({/new-08ff22f2})

[comment]: # ({new-7f042696})
### Параметры

`(объект)` Параметры задают желаемый вывод.

Этот метод поддерживает следующие параметры.

|Параметр|Тип|Описание|
|----------------|------|----------------|
|graphids|строка/массив|Возврат только тех групп узлов сети, которые содержат узлы сети или шаблоны с заданными графиками.|
|groupids|строка/массив|Возврат групп узлов сети только с заданными ID групп узлов сети.|
|hostids|строка/массив|Возврат только тех групп узлов сети, которые содержат заданные узлы сети.|
|maintenanceids|строка/массив|Возврат только тех групп узлов сети, которые задействованы в заданных обслуживаниях.|
|monitored\_hosts|флаг|Возврат только тех групп узлов сети, которые содержат узлы сети под наблюдением.|
|real\_hosts|флаг|Возврат только тех групп узлов сети, которые содержат узлы сети.|
|templated\_hosts|флаг|Возврат только тех групп узлов сети, которые содержат шаблоны.|
|templateids|строка/массив|Возврат только тех групп узлов сети, которые содержат заданные шаблоны.|
|triggerids|строка/массив|Возврат только тех групп узлов сети, которые узлы сети или шаблоны с заданными триггерами.|
|with\_applications|флаг|Возврат только тех групп узлов сети, которые содержат узлы сети с группами элементов данных.|
|with\_graphs|флаг|Возврат только тех групп узлов сети, которые содержат узлы сети с графиками.|
|with\_hosts\_and\_templates|флаг|Возврат только тех групп узлов сети, которые содержат узлы сети *или* шаблоны.|
|with\_httptests|флаг|Возврат только тех групп узлов сети, которые содержат узлы сети с веб проверками.<br><br>Переопределяет параметр `with_monitored_httptests`.|
|with\_items|флаг|Возврат только тех групп узлов сети, которые содержат узлы сети или шаблоны с элементами данных.<br><br>Переопределяет параметры `with_monitored_items` и `with_simple_graph_items`.|
|with\_monitored\_httptests|флаг|Возврат только тех групп узлов сети, которые содержат узлы сети с активированными веб проверками.|
|with\_monitored\_items|флаг|Возврат только тех групп узлов сети, которые узлы сети или шаблоны с активированными элементами данных.<br><br>Переопределяет параметр `with_simple_graph_items`.|
|with\_monitored\_triggers|флаг|Возврат только тех групп узлов сети, которые содержат узлы сети с активированными триггерами. Все элементы данных, используемые в триггере, также должны быть активированы.|
|with\_simple\_graph\_items|флаг|Возврат только тех групп узлов сети, которые содержат узлы сети с элементами данных числового типа.|
|with\_triggers|флаг|Возврат только тех групп узлов сети, которые содержат узлы сети с триггерами.<br><br>Переопределяет параметр `with_monitored_triggers`.|
|selectDiscoveryRule|запрос|Возврат LLD правила, которое создало группу узлов сети, в свойстве `discoveryRule`.|
|selectGroupDiscovery|запрос|Возврат объекта обнаружения группы узлов сети в свойстве `groupDiscovery`.<br><br>Объект обнаружения групп узлов сети связывает обнаруженную группу узов сети с прототипом групп узлов сети и имеет следующие свойства:<br>`groupid` - `(строка)` ID обнаруженной группы узлов сети;<br>`lastcheck` - `(штамп времени)` время, когда группа узлов сети была в последний раз обнаружена;<br>`name` - `(строка)` имя прототипа групп узлов сети;<br>`parent_group_prototypeid` - `(строка)` ID прототипа групп узлов сети с которого была создана группа узлов сети;<br>`ts_delete` - `(штамп времени)` время, когда более необнаруживаемая группа узлов сети будет удалена..|
|selectHosts|запрос|Возврат узлов сети, которые принадлежат группе узлов сети, в свойстве `hosts`.<br><br>Поддерживается `count`.|
|selectTemplates|запрос|Возврат шаблонов, которые принадлежат группе узлов сети, в свойстве `templates`.<br><br>Поддерживается `count`.|
|limitSelects|целое число|Ограничение количества записей, возвращаемых подзапросами.<br><br>Применимо только к следующим подзапросам:<br>`selectHosts` - все результаты сортируются по `host`;<br>`selectTemplates` - все результаты сортируются по `host`.|
|sortfield|строка/массив|Сортировка результата в соответствии с заданными свойствами.<br><br>Возможные значения: `groupid`, `name`.|
|countOutput|логический|Эти параметры являются общими для всех методов `get` и они описаны в [справочных комментариях](/ru/manual/api/reference_commentary#общие_параметры_get_метода).|
|editable|логический|^|
|excludeSearch|логический|^|
|filter|объект|^|
|limit|целое число|^|
|output|запрос|^|
|preservekeys|логический|^|
|search|объект|^|
|searchByAny|логический|^|
|searchWildcardsEnabled|логический|^|
|sortorder|строка/массив|^|
|startSearch|логический|^|

[comment]: # ({/new-7f042696})

[comment]: # ({new-7223bab1})
### Возвращаемые значения

`(целое число/массив)` Возвращает либо:

-   массив объектов;
-   количество найденных объектов, если используется параметр
    `countOutput`.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-8ed2756a})
#### Получение данных по имени

Получение всех данных по двум группам узлов сети с именами "Zabbix
servers" и "Linux servers".

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "hostgroup.get",
    "params": {
        "output": "extend",
        "filter": {
            "name": [
                "Zabbix servers",
                "Linux servers"
            ]
        }
    },
    "auth": "6f38cddc44cfbb6c1bd186f9a220b5a0",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "groupid": "2",
            "name": "Linux servers",
            "internal": "0"
        },
        {
            "groupid": "4",
            "name": "Zabbix servers",
            "internal": "0"
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-8ed2756a})

[comment]: # ({new-e5f0abe1})
### Смотрите также

-   [Узел сети](/ru/manual/api/reference/host/object#узел_сети)
-   [Шаблон](/ru/manual/api/reference/template/object#шаблон)

[comment]: # ({/new-e5f0abe1})

[comment]: # ({new-1e72ea39})
### Исходный код

CHostGroup::get() в
*frontends/php/include/classes/api/services/CHostGroup.php*.

[comment]: # ({/new-1e72ea39})
