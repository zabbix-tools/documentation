[comment]: # translation:outdated

[comment]: # ({new-6a96957a})
# hostgroup.delete

[comment]: # ({/new-6a96957a})

[comment]: # ({new-3c7f9c00})
### Описание

`объект hostgroup.delete(массив hostGroupIds)`

Этот метод позволяет удалять группы узлов сети.

Группу узлов сети нельзя удалить в случае, если:

-   она содержит узлы сети, которые принадлежат только этой группе узлов
    сети;
-   она отмечена как внутренняя;
-   она используется прототипом узлов сети;
-   она используется в глобальном скрипте;
-   она используется в условии корреляции.

[comment]: # ({/new-3c7f9c00})

[comment]: # ({new-af1f0187})
### Параметры

`(массив)` ID удаляемых групп узлов сети.

[comment]: # ({/new-af1f0187})

[comment]: # ({new-288daefc})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID удаленных групп узлов
сети под свойством `groupids`.

[comment]: # ({/new-288daefc})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-a6d8d11e})
#### Удаление нескольких групп узлов сети

Удаление двух групп узлов сети.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "hostgroup.delete",
    "params": [
        "107824",
        "107825"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "groupids": [
            "107824",
            "107825"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-a6d8d11e})

[comment]: # ({new-f496d50e})
### Исходный код

CHostGroup::delete() в
*frontends/php/include/classes/api/services/CHostGroup.php*.

[comment]: # ({/new-f496d50e})
