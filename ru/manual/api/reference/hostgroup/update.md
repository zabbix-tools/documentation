[comment]: # translation:outdated

[comment]: # ({new-259f747f})
# hostgroup.update

[comment]: # ({/new-259f747f})

[comment]: # ({new-32bf01b5})
### Описание

`объект hostgroup.update(объект/массив ГруппыУзловсети)`

Этот метод позволяет обновлять существующие группы узлов сети.

[comment]: # ({/new-32bf01b5})

[comment]: # ({new-9c20e34d})
### Параметры

`(объект/массив)` [Свойства группы узлов
сети](object#группы_узлов сети), которые будут обновлены.

Свойство `groupid` должно быть указано по каждой группе узлов сети, все
остальные свойства опциональны. Будут обновлены только переданные
свойства, все остальные останутся неизменными.

[comment]: # ({/new-9c20e34d})

[comment]: # ({new-736f3b05})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID обновленных групп
узлов сети под свойством `groupids`.

[comment]: # ({/new-736f3b05})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-a296ad88})
#### Переименование группы узлов сети

Переименование группы узлов сети на "Linux hosts."

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "hostgroup.update",
    "params": {
        "groupid": "7",
        "name": "Linux hosts"
    },
    "auth": "700ca65537074ec963db7efabda78259",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "groupids": [
            "7"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-a296ad88})

[comment]: # ({new-b75d89b4})
### Исходный код

CHostGroup::update() в
*frontends/php/include/classes/api/services/CHostGroup.php*.

[comment]: # ({/new-b75d89b4})
