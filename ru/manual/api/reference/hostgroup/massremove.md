[comment]: # translation:outdated

[comment]: # ({new-8703e8df})
# hostgroup.massremove

[comment]: # ({/new-8703e8df})

[comment]: # ({new-bcf1ded4})
### Описание

`объект hostgroup.massremove(объект параметры)`

Этот метод позволяет удалить связанные объекты из нескольких групп узлов
сети.

[comment]: # ({/new-bcf1ded4})

[comment]: # ({new-6b7b7475})
### Параметры

`(объект)` Параметры, которые содержат ID обновляемых групп узлов сети и
объектов, которые необходимо удалить.

|Параметр|Тип|Описание|
|----------------|------|----------------|
|**groupids**<br>(требуется)|строка/массив|ID обновляемых групп узлов сети.|
|hostids|строка/массив|Удаляемые узлы сети со всех групп узлов сети.|
|templateids|строка/массив|Удаляемые шаблоны со всех групп узлов сети.|

[comment]: # ({/new-6b7b7475})

[comment]: # ({new-736f3b05})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID обновленных групп
узлов сети под свойством `groupids`.

[comment]: # ({/new-736f3b05})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-cf36d4e8})
#### Удаление узлов сети из групп узлов сети

Удаление двух узлов сети из заданных групп узлов сети.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "hostgroup.massremove",
    "params": {
        "groupids": [
            "5",
            "6"
        ],
        "hostids": [
            "30050",
            "30001"
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "groupids": [
            "5",
            "6"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-cf36d4e8})

[comment]: # ({new-3700b99d})
### Исходный код

CHostGroup::massRemove() в
*frontends/php/include/classes/api/services/CHostGroup.php*.

[comment]: # ({/new-3700b99d})
