[comment]: # translation:outdated

[comment]: # ({new-c07c9092})
# hostgroup.massupdate

[comment]: # ({/new-c07c9092})

[comment]: # ({new-0ec0a392})
### Описание

`Объект hostgroup.massupdate(объект параметры)`

Этот метод позволяет заменить или удалить связанные объекты и обновить
свойства сразу на нескольких группах узлов сети.

[comment]: # ({/new-0ec0a392})

[comment]: # ({new-d4028a54})
### Параметры

`(объект)` Параметры, которые содержат ID обновляемых групп узлов сети и
их свойства, которые необходимо обновить.

|Параметр|Тип|Описание|
|----------------|------|----------------|
|**groups**<br>(требуется)|объект/массив|Обновляемые группы узлов сети.<br><br>У групп узлов сети должно быть задано свойство `groupid`.|
|hosts|объект/массив|Узлы сети, которые заменят текущие узлы сети в заданных группах узлов сети.<br><br>У узлов сети должно быть задано свойство `hostid`.|
|templates|объект/массив|Шаблоны, которые заменят текущие шаблоны в заданных группах узлов сети.<br><br>У шаблонов должно быть задано свойство `templateid`.|

[comment]: # ({/new-d4028a54})

[comment]: # ({new-736f3b05})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID обновленных групп
узлов сети под свойством `groupids`.

[comment]: # ({/new-736f3b05})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-b4adf299})
#### Замена узлов сети в группе узлов сети

Замена всех узлов сети в группе узлов сети с ID.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "hostgroup.massupdate",
    "params": {
        "groups": [
            {
                "groupid": "6"
            }
        ],
        "hosts": [
            {
                "hostid": "30050"
            }
        ]
    },
    "auth": "f223adf833b2bf2ff38574a67bba6372",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "groupids": [
            "6",
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-b4adf299})

[comment]: # ({new-a1410648})
### Смотрите также

-   [hostgroup.update](update)
-   [hostgroup.massadd](massadd)
-   [Узел сети](/ru/manual/api/reference/host/object#узел_сети)
-   [Шаблон](/ru/manual/api/reference/template/object#шаблон)

[comment]: # ({/new-a1410648})

[comment]: # ({new-243072aa})
### Исходный код

CHostGroup::massUpdate() в
*frontends/php/include/classes/api/services/CHostGroup.php*.

[comment]: # ({/new-243072aa})
