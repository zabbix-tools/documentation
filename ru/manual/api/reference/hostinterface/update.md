[comment]: # translation:outdated

[comment]: # ({new-59b410c0})
# hostinterface.update

[comment]: # ({/new-59b410c0})

[comment]: # ({new-253ed57f})
### Описание

`объект hostinterface.update(объект/массив ИнтерфейсыУзловсети)`

Этот метод позволяет обновлять существующие интерфейсы узлов сети.

[comment]: # ({/new-253ed57f})

[comment]: # ({new-a99ac0ad})
### Параметры

`(объект/массив)` [Свойства интерфейса узла
сети](object#интерфейс_узла_сети), которые будут обновлены.

Свойство `interfaceid` должно быть указано по каждому интерфейсу узла
сети, все остальные свойства опциональны. Будут обновлены только
переданные свойства, все остальные останутся неизменными.

[comment]: # ({/new-a99ac0ad})

[comment]: # ({new-d3be3202})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID обновленных
интерфейсов узлов сети под свойством `interfaceids`.

[comment]: # ({/new-d3be3202})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-eb8b35e8})
#### Изменение порта у интерфейса узла сети

Изменение номера порта у интерфейса узла сети.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "hostinterface.update",
    "params": {
        "interfaceid": "30048",
        "port": "30050"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "interfaceids": [
            "30048"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-eb8b35e8})

[comment]: # ({new-97e4c8d0})
### Исходный код

CHostInterface::update() в
*frontends/php/include/classes/api/services/CHostInterface.php*.

[comment]: # ({/new-97e4c8d0})
