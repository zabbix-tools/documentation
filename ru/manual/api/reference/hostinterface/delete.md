[comment]: # translation:outdated

[comment]: # ({new-fc20d2a9})
# hostinterface.delete

[comment]: # ({/new-fc20d2a9})

[comment]: # ({new-6a90d1d2})
### Описание

`объект hostinterface.delete(массив hostInterfaceIds)`

Этот метод позволяет удалять интерфейсы узлов сети.

[comment]: # ({/new-6a90d1d2})

[comment]: # ({new-617ba57c})
### Параметры

`(массив)` ID удаляемых интерфейсов узлов сети.

[comment]: # ({/new-617ba57c})

[comment]: # ({new-61a8b976})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID удаленных интерфейсов
узлов сети под свойством `interfaceids`.

[comment]: # ({/new-61a8b976})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-91934c15})
#### Удаление интерфейса узла сети

Удаление интерфейса узла сети с ID 30062.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "hostinterface.delete",
    "params": [
        "30062"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "interfaceids": [
            "30062"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-91934c15})

[comment]: # ({new-a3d1e725})
### Смотрите также

-   [hostinterface.massremove](massremove)
-   [host.massremove](/ru/manual/api/reference/host/massremove)

[comment]: # ({/new-a3d1e725})

[comment]: # ({new-cf54c99c})
### Исходный код

CHostInterface::delete() в
*frontends/php/include/classes/api/services/CHostInterface.php*.

[comment]: # ({/new-cf54c99c})
