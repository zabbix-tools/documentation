[comment]: # translation:outdated

[comment]: # ({new-7ed888d2})
# > Объект интерфейса узла сети

Следующие объекты напрямую связаны с `hostinterface` API.

[comment]: # ({/new-7ed888d2})

[comment]: # ({new-ce77a3b0})
### Интерфейс узла сети

Объект интерфейса узла сети имеет следующие свойства.

::: noteimportant
Обратите внимание, что требуется указывать как IP,
так и DNS. Если вы не хотите использовать DNS, задайте это свойство как
пустая строка.
:::

|Свойство|Тип|Описание|
|----------------|------|----------------|
|interfaceid|строка|*(только чтение)* ID интерфейса.|
|**dns**<br>(требуется)|строка|DNS имя используемое интерфейсом.<br><br>Может быть пустым, если соединение выполняется через IP.|
|**hostid**<br>(требуется)|строка|ID узла сети, которому принадлежит интерфейс.|
|**ip**<br>(требуется)|строка|IP адрес используемый интерфейсом.<br><br>Может быть пустым, если соединение выполняется через DNS.|
|**main**<br>(требуется)|целое число|Является ли интерфейс используемым по умолчанию на узле сети. На узле сети в качестве умолчания может быть задан только один интерфейс каждого типа.<br><br>Возможные значения:<br>0 - не по умолчанию;<br>1 - по умолчанию.|
|**port**<br>(требуется)|строка|Номер порта используемого интерфейсом. Может содержать пользовательские макросы.|
|**type**<br>(требуется)|целое число|Тип интерфейса.<br><br>Возможные значения:<br>1 - агент;<br>2 - SNMP;<br>3 - IPMI;<br>4 - JMX.<br>|
|**useip**<br>(требуется)|целое число|Должно ли соединение выполняться через IP адрес.<br><br>Возможные значения:<br>0 - подключение с использованием DNS имени хоста;<br>1 - подключение с использованием IP адреса хоста.|
|bulk|целое число|Необходимо ли использовать массовые запросы SNMP.<br><br>Возможные значения:<br>0 - не использовать массовые запросы;<br>1 - *(по умолчанию)* использовать массовые запросы.|

[comment]: # ({/new-ce77a3b0})


[comment]: # ({new-3bb71fcc})
### Details tag

The details object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**version**<br>(required)|integer|SNMP interface version.<br><br>Possible values are:<br>1 - SNMPv1;<br>2 - SNMPv2c;<br>3 - SNMPv3|
|bulk|integer|Whether to use bulk SNMP requests.<br><br>Possible values are:<br>0 - don't use bulk requests;<br>1 - (default) - use bulk requests.|
|community|string|SNMP community (required). Used only by SNMPv1 and SNMPv2 interfaces.|
|securityname|string|SNMPv3 security name. Used only by SNMPv3 interfaces.|
|securitylevel|integer|SNMPv3 security level. Used only by SNMPv3 interfaces.<br><br>Possible values are:<br>0 - (default) - noAuthNoPriv;<br>1 - authNoPriv;<br>2 - authPriv.|
|authpassphrase|string|SNMPv3 authentication passphrase. Used only by SNMPv3 interfaces.|
|privpassphrase|string|SNMPv3 privacy passphrase. Used only by SNMPv3 interfaces.|
|authprotocol|integer|SNMPv3 authentication protocol. Used only by SNMPv3 interfaces.<br><br>Possible values are:<br>0 - (default) - MD5;<br>1 - SHA1;<br>2 - SHA224;<br>3 - SHA256;<br>4 - SHA384;<br>5 - SHA512.|
|privprotocol|integer|SNMPv3 privacy protocol. Used only by SNMPv3 interfaces.<br><br>Possible values are:<br>0 - (default) - DES;<br>1 - AES128;<br>2 - AES192;<br>3 - AES256;<br>4 - AES192C;<br>5 - AES256C.|
|contextname|string|SNMPv3 context name. Used only by SNMPv3 interfaces.|

[comment]: # ({/new-3bb71fcc})
