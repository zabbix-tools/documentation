[comment]: # translation:outdated

[comment]: # ({new-fff19f5f})
# hostinterface.massadd

[comment]: # ({/new-fff19f5f})

[comment]: # ({new-30999da7})
### Описание

`объект hostinterface.massadd(объект параметры)`

Этот метод позволяет добавить одновременно несколько интерфейсов узлов
сети на узлы сети.

[comment]: # ({/new-30999da7})

[comment]: # ({new-35c0864e})
### Параметры

`(объект)` Параметры, которые содержат интерфейсы узлов сети,
создаваемые на заданных узлах сети.

Этот метод принимает следующие параметры.

|Параметр|Тип|Описание|
|----------------|------|----------------|
|**hosts**<br>(требуется)|объект/массив|Обновляемые узлы сети.<br><br>У узлов сети должно быть задано свойство `hostid`.|
|**interfaces**<br>(требуется)|объект/массив|Создаваемые интерфейсы узлов сети на заданных узлах сети.|

[comment]: # ({/new-35c0864e})

[comment]: # ({new-88283807})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID созданных интерфейсов
узлов сети под свойством `interfaceids`.

[comment]: # ({/new-88283807})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-ad87458a})
#### Создание интерфейсов

Создание интерфейса на двух узлах сети.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "hostinterface.massadd",
    "params": {
        "hosts": [
            {
                "hostid": "30050"
            },
            {
                "hostid": "30052"
            }
        ],
        "interfaces": {
            "dns": "",
            "ip": "127.0.0.1",
            "main": 0,
            "port": "10050",
            "type": 1,
            "useip": 1
        }
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "interfaceids": [
            "30069",
            "30070"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-ad87458a})

[comment]: # ({new-a2a46209})
### Смотрите также

-   [hostinterface.create](create)
-   [host.massadd](/ru/manual/api/reference/host/massadd)
-   [Узел сети](/ru/manual/api/reference/host/object#узел_сети)

[comment]: # ({/new-a2a46209})

[comment]: # ({new-9b65afcc})
### Исходный код

CHostInterface::massAdd() в
*frontends/php/include/classes/api/services/CHostInterface.php*.

[comment]: # ({/new-9b65afcc})
