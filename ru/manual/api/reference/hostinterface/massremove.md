[comment]: # translation:outdated

[comment]: # ({new-9230837a})
# hostinterface.massremove

[comment]: # ({/new-9230837a})

[comment]: # ({new-4559aff6})
### Описание

`объект hostinterface.massremove(объект параметры)`

Этот метод позволяет удалить интерфейсы узлов сети с заданных узлов
сети.

[comment]: # ({/new-4559aff6})

[comment]: # ({new-8970fdbb})
### Параметры

`(объект)` Параметры, которые содержат ID обновляемых узлов сети и
удаляемых интерфейсов.

|Параметр|Тип|Описание|
|----------------|------|----------------|
|**hostids**<br>(требуется)|строка/массив|ID обновляемых узлов сети.|
|**interfaces**<br>(требуется)|строка/массив|Удаляемые интерфейсы узлов сети с заданных узлов сети.<br><br>У объекта интерфейса узла сети должны быть заданы свойства ip, dns и port|

[comment]: # ({/new-8970fdbb})

[comment]: # ({new-61a8b976})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID удаленных интерфейсов
узлов сети под свойством `interfaceids`.

[comment]: # ({/new-61a8b976})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-cceffaf2})
#### Удаление интерфейсов

Удаление "127.0.0.1" SNMP интерфейса с двух узлов сети.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "hostinterface.massremove",
    "params": {
        "hostids": [
            "30050",
            "30052"
        ],
        "interfaces": {
            "dns": "",
            "ip": "127.0.0.1",
            "port": "161"
        }
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "interfaceids": [
            "30069",
            "30070"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-cceffaf2})

[comment]: # ({new-a3245281})
### Смотрите также

-   [hostinterface.delete](delete)
-   [host.massremove](/ru/manual/api/reference/host/massremove)

[comment]: # ({/new-a3245281})

[comment]: # ({new-6853cf7a})
### Исходный код

CHostInterface::massRemove() в
*frontends/php/include/classes/api/services/CHostInterface.php*.

[comment]: # ({/new-6853cf7a})
