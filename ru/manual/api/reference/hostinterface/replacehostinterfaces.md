[comment]: # translation:outdated

[comment]: # ({new-2898fc4b})
# hostinterface.replacehostinterfaces

[comment]: # ({/new-2898fc4b})

[comment]: # ({new-f14e7778})
### Описание

`объект hostinterface.replacehostinterfaces(объект параметры)`

Этот метод позволяет заменить все интерфейсы узла сети на заданном узле
сети.

[comment]: # ({/new-f14e7778})

[comment]: # ({new-f5cf0b40})
### Параметры

`(объект)` Параметры, которые содержат ID обновляемого узла сети и новые
интерфейсы узла сети.

|Параметр|Тип|Описание|
|----------------|------|----------------|
|**hostid**<br>(требуется)|строка|ID обновляемого узла сети.|
|**interfaces**<br>(требуется)|объект/массив|Интерфейсы узла сети, на которые заменят текущие интерфейсы узла сети.|

[comment]: # ({/new-f5cf0b40})

[comment]: # ({new-88283807})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID созданных интерфейсов
узла сети под свойством `interfaceids`.

[comment]: # ({/new-88283807})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-98e67db1})
#### Замена интерфейсов узла сети

Замена всех интерфейсов узла сети на единственный интерфейс агента.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "hostinterface.replacehostinterfaces",
    "params": {
        "hostid": "30052",
        "interfaces": {
            "dns": "",
            "ip": "127.0.0.1",
            "main": 1,
            "port": "10050",
            "type": 1,
            "useip": 1
        }
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "interfaceids": [
            "30081"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-98e67db1})

[comment]: # ({new-272c42ef})
### Смотрите также

-   [host.update](/ru/manual/api/reference/host/update)
-   [host.massupdate](/ru/manual/api/reference/host/massupdate)

[comment]: # ({/new-272c42ef})

[comment]: # ({new-9d1e0b55})
### Исходный код

CHostInterface::replaceHostInterfaces() в
*frontends/php/include/classes/api/services/CHostInterface.php*.

[comment]: # ({/new-9d1e0b55})
