[comment]: # translation:outdated

[comment]: # ({new-0bfa3a60})
# hostinterface.create

[comment]: # ({/new-0bfa3a60})

[comment]: # ({new-e87d74af})
### Описание

`объект hostinterface.create(объект/массив ИнтерфейсыУзловсети)`

Этот метод позволяет создавать новые интерфейсы узлов сети.

[comment]: # ({/new-e87d74af})

[comment]: # ({new-e81b8e01})
### Параметры

`(объект/массив)` Создаваемые интерфейсы узлов сети. Этот метод
принимает интерфейсы узлов сети со [стандартными свойствами интерфейсов
узлов сети](object#интерфейс_узла_сети).

[comment]: # ({/new-e81b8e01})

[comment]: # ({new-4b59ae6b})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID созданных интерфейсов
узлов сети под свойством `interfaceids`. Порядок возвращаемых ID
совпадает с порядком переданных интерфейсов узлов сети.

[comment]: # ({/new-4b59ae6b})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-150295f4})
#### Создание нового интерфейса

Создание вторичного IP агент интерфейса на узле сети "30052".

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "hostinterface.create",
    "params": {
        "hostid": "30052",
        "dns": "",
        "ip": "127.0.0.1",
        "main": 0,
        "port": "10050",
        "type": 1,
        "useip": 1
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "interfaceids": [
            "30062"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-150295f4})

[comment]: # ({new-c22176c4})
### Смотрите также

-   [hostinterface.massadd](massadd)
-   [host.massadd](/ru/manual/api/reference/host/massadd)

[comment]: # ({/new-c22176c4})

[comment]: # ({new-9afed0b0})
### Исходный код

CHostInterface::create() в
*frontends/php/include/classes/api/services/CHostInterface.php*.

[comment]: # ({/new-9afed0b0})


[comment]: # ({new-5b41f219})
### Source

CHostInterface::create() in
*ui/include/classes/api/services/CHostInterface.php*.

[comment]: # ({/new-5b41f219})
