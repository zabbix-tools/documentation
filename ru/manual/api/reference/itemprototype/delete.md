[comment]: # translation:outdated

[comment]: # ({new-1b9515ab})
# itemprototype.delete

[comment]: # ({/new-1b9515ab})

[comment]: # ({new-ee2fdfa0})
### Описание

`объект itemprototype.delete(массив itemPrototypeIds)`

Этот метод позволяет удалять прототипы элементов данных.

[comment]: # ({/new-ee2fdfa0})

[comment]: # ({new-7755a7b3})
### Параметры

`(массив)` ID удаляемых прототипов элементов данных.

[comment]: # ({/new-7755a7b3})

[comment]: # ({new-db63ca62})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID удаленных прототипов
элементов данных под свойством `prototypeids`.

[comment]: # ({/new-db63ca62})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-0c135019})
#### Удаление нескольких прототипов элементов данных

Удаление двух прототипов элементов данных.\
Зависимые прототипы элементов данных автоматически удаляются при
удалении прототипа основного элемента данных или прототипа элементов
данных.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "itemprototype.delete",
    "params": [
        "27352",
        "27356"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "prototypeids": [
            "27352",
            "27356"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-0c135019})

[comment]: # ({new-8cee4f1f})
### Исходный код

CItemPrototype::delete() в
*frontends/php/include/classes/api/services/CItemPrototype.php*.

[comment]: # ({/new-8cee4f1f})
