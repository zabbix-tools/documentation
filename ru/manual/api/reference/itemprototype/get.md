[comment]: # translation:outdated

[comment]: # ({new-8246351f})
# itemprototype.get

[comment]: # ({/new-8246351f})

[comment]: # ({new-5f97ef4a})
### Описание

`целое число/массив itemprototype.get(объект параметры)`

Этот метод позволяет получать прототипы элементов данных в соответствии
с заданными параметрами.

[comment]: # ({/new-5f97ef4a})

[comment]: # ({new-8d6ecc79})
### Параметры

`(объект)` Параметры задают желаемый вывод.

Этот метод поддерживает следующие параметры.

|Параметр|Тип|Описание|
|----------------|------|----------------|
|discoveryids|строка/массив|Возврат только тех прототипов элементов данных, которые используются в заданных LLD правилах.|
|graphids|строка/массив|Возврат только тех прототипов элементов данных, которые используются в заданных прототипах графиков.|
|hostids|строка/массив|Возврат только тех прототипов элементов данных, которые принадлежат заданным узлам сети.|
|inherited|логический|Если задано значение `true`, возвращать только те прототипы элементов данных, которые унаследованы из шаблона.|
|itemids|строка/массив|Возврат прототипов элементов данных только с заданными ID.|
|monitored|логический|Если задано значение `true`, возвращать только активированные прототипы элементов данных, которые принадлежат узлам сети под наблюдением.|
|templated|логический|Если задано значение `true`, возвращать только те прототипы элементов данных, которые принадлежат шаблонам.|
|templateids|строка/массив|Возврат только тех прототипов элементов данных, которые принадлежат заданным шаблонам.|
|triggerids|строка/массив|Возврат только тех прототипов элементов данных, которые используются в заданных прототипах триггеров.|
|selectApplications|запрос|Возврат групп элементов данных, которым принадлежит прототип элементов данных, в свойстве `applications`.|
|selectApplicationPrototypes|запрос|Возврат прототипов групп элементов данных, к которым присоединен прототип элементов данных, в свойстве `applicationPrototypes`.|
|selectDiscoveryRule|запрос|Возврат LLD правила, которому принадлежит прототип элементов данных, в свойстве `discoveryRule`.|
|selectGraphs|запрос|Возврат прототипов графиков, которые содержат прототип элементов данных, в свойстве `graphs`.<br><br>Supports `count`.|
|selectHosts|запрос|Возврат узла сети, которому принадлежит прототип элементов данных, в виде массива в свойстве `hosts`.|
|selectTriggers|запрос|Возврат прототипов триггеров, которые используют прототип элементов данных, в свойстве `triggers`.<br><br>Supports `count`.|
|selectPreprocessing|запрос|Возврат опций предобработки элемента данных в свойстве `preprocessing`.<br><br>Этот параметр имеет следующие свойства:<br>`type` - `(строка)` Типы опций предобработки:<br>1 - Пользовательский множитель;<br>2 - Обрезка справа;<br>3 - Обрезка слева;<br>4 - Обрезка;<br>5 - Соответствие регулярному выражению;<br>6 - Двоичное в десятичное;<br>7 - Восьмеричное в десятичное;<br>8 - Шестнадцатеричное в десятичное;<br>9 - Простое изменение;<br>10 - Изменение в секунду.<br><br>`params` - `(строка)` Дополнительные параметры используемые опцией предварительной обработки. Несколько параметров разделяются символом LF (\\n).|
|filter|объект|Возврат только тех результатов, которые в точности соответствуют заданному фильтру.<br><br>Принимает массив, где ключи являются именами свойств и значения, которые являются либо одним значением, либо массивом сопоставляемых значений.<br><br>Поддерживает дополнительные фильтры:<br>`host` - техническое имя узла сети, которому принадлежит прототип элементов данных.|
|limitSelects|целое число|Ограничение количества записей, возвращаемых подзапросами.<br><br>Применимо только к следующим подзапросам:<br>`selectGraphs` - результаты сортируются по `name`;<br>`selectTriggers` - результаты сортируются по `description`.|
|sortfield|строка/массив|Сортировка результата в соответствии с заданными свойствами.<br><br>Возможные значения: `itemid`, `name`, `key_`, `delay`, `type` и `status`.|
|countOutput|логический|Эти параметры являются общими для всех методов `get` и они описаны в [справочных комментариях](/ru/manual/api/reference_commentary#общие_параметры_get_метода).|
|editable|логический|^|
|excludeSearch|логический|^|
|limit|целое число|^|
|output|запрос|^|
|preservekeys|логический|^|
|search|объект|^|
|searchByAny|логический|^|
|searchWildcardsEnabled|логический|^|
|sortorder|строка/массив|^|
|startSearch|логический|^|

[comment]: # ({/new-8d6ecc79})

[comment]: # ({new-7223bab1})
### Возвращаемые значения

`(целое число/массив)` Возвращает либо:

-   массив объектов;
-   количество найденных объектов, если используется параметр
    `countOutput`.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-92651f4c})
#### Получение прототипов элементов данных с LLD правила

Получение всех прототипов элементов данных с LLD правила.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "itemprototype.get",
    "params": {
        "output": "extend",
        "discoveryids": "27426"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "itemid": "23077",
            "type": "0",
            "snmp_community": "",
            "snmp_oid": "",
            "hostid": "10079",
            "name": "Incoming network traffic on $1",
            "key_": "net.if.in[en0]",
            "delay": "1m",
            "history": "1w",
            "trends": "365d",
            "status": "0",
            "value_type": "3",
            "trapper_hosts": "",
            "units": "bps",
            "snmpv3_securityname": "",
            "snmpv3_securitylevel": "0",
            "snmpv3_authpassphrase": "",
            "snmpv3_privpassphrase": "",
            "formula": "",
            "error": "",
            "lastlogsize": "0",
            "logtimefmt": "",
            "templateid": "0",
            "valuemapid": "0",
            "params": "",
            "ipmi_sensor": "",
            "authtype": "0",
            "username": "",
            "password": "",
            "publickey": "",
            "privatekey": "",
            "mtime": "0",
            "flags": "0",
            "interfaceid": "0",
            "port": "",
            "description": "",
            "inventory_link": "0",
            "lifetime": "30d",
            "snmpv3_authprotocol": "0",
            "snmpv3_privprotocol": "0",
            "state": "0",
            "snmpv3_contextname": "",
            "evaltype": "0",
            "jmx_endpoint": "",
            "master_itemid": "0",
            "timeout": "3s",
            "url": "",
            "query_fields": [],
            "posts": "",
            "status_codes": "200",
            "follow_redirects": "1",
            "post_type": "0",
            "http_proxy": "",
            "headers": [],
            "retrieve_mode": "0",
            "request_method": "0",
            "output_format": "0",
            "ssl_cert_file": "",
            "ssl_key_file": "",
            "ssl_key_password": "",
            "verify_peer": "0",
            "verify_host": "0",
            "allow_traps": "0",
            "lastclock": "0",
            "lastns": "0",
            "lastvalue": "0",
            "prevvalue": "0"
        },
        {
            "itemid": "10010",
            "type": "0",
            "snmp_community": "",
            "snmp_oid": "",
            "hostid": "10001",
            "name": "Processor load (1 min average per core)",
            "key_": "system.cpu.load[percpu,avg1]",
            "delay": "1m",
            "history": "1w",
            "trends": "365d",
            "status": "0",
            "value_type": "0",
            "trapper_hosts": "",
            "units": "",
            "snmpv3_securityname": "",
            "snmpv3_securitylevel": "0",
            "snmpv3_authpassphrase": "",
            "snmpv3_privpassphrase": "",
            "formula": "",
            "error": "",
            "lastlogsize": "0",
            "logtimefmt": "",
            "templateid": "0",
            "valuemapid": "0",
            "params": "",
            "ipmi_sensor": "",
            "authtype": "0",
            "username": "",
            "password": "",
            "publickey": "",
            "privatekey": "",
            "mtime": "0",
            "flags": "0",
            "interfaceid": "0",
            "port": "",
            "description": "The processor load is calculated as system CPU load divided by number of CPU cores.",
            "inventory_link": "0",
            "lifetime": "0",
            "snmpv3_authprotocol": "0",
            "snmpv3_privprotocol": "0",
            "state": "0",
            "snmpv3_contextname": "",
            "evaltype": "0",
            "jmx_endpoint": "",
            "master_itemid": "0",
            "timeout": "3s",
            "url": "",
            "query_fields": [],
            "posts": "",
            "status_codes": "200",
            "follow_redirects": "1",
            "post_type": "0",
            "http_proxy": "",
            "headers": [],
            "retrieve_mode": "0",
            "request_method": "0",
            "output_format": "0",
            "ssl_cert_file": "",
            "ssl_key_file": "",
            "ssl_key_password": "",
            "verify_peer": "0",
            "verify_host": "0",
            "allow_traps": "0",
            "lastclock": "0",
            "lastns": "0",
            "lastvalue": "0",
            "prevvalue": "0"
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-92651f4c})

[comment]: # ({new-acb23a7a})
#### Поиск зависимого элемента данных

Поиск одного зависимого элемента данных у элемента данных с ID "25545".

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "item.get",
    "params": {
        "output": "extend",
        "filter": {
            "type": "18",
            "master_itemid": "25545"
        },
        "limit": "1"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "itemid": "25547",
            "type": "18",
            "snmp_community": "",
            "snmp_oid": "",
            "hostid": "10116",
            "name": "Seconds",
            "key_": "apache.status.uptime.seconds",
            "delay": "0",
            "history": "90d",
            "trends": "365d",
            "status": "0",
            "value_type": "3",
            "trapper_hosts": "",
            "units": "",
            "snmpv3_securityname": "",
            "snmpv3_securitylevel": "0",
            "snmpv3_authpassphrase": "",
            "snmpv3_privpassphrase": "",
            "formula": "",
            "error": "",
            "lastlogsize": "0",
            "logtimefmt": "",
            "templateid": "0",
            "valuemapid": "0",
            "params": "",
            "ipmi_sensor": "",
            "authtype": "0",
            "username": "",
            "password": "",
            "publickey": "",
            "privatekey": "",
            "mtime": "0",
            "flags": "0",
            "interfaceid": "0",
            "port": "",
            "description": "",
            "inventory_link": "0",
            "lifetime": "30d",
            "snmpv3_authprotocol": "0",
            "snmpv3_privprotocol": "0",
            "state": "0",
            "snmpv3_contextname": "",
            "evaltype": "0",
            "master_itemid": "25545",
            "jmx_endpoint": "",
            "master_itemid": "0",
            "timeout": "3s",
            "url": "",
            "query_fields": [],
            "posts": "",
            "status_codes": "200",
            "follow_redirects": "1",
            "post_type": "0",
            "http_proxy": "",
            "headers": [],
            "retrieve_mode": "0",
            "request_method": "0",
            "output_format": "0",
            "ssl_cert_file": "",
            "ssl_key_file": "",
            "ssl_key_password": "",
            "verify_peer": "0",
            "verify_host": "0",
            "allow_traps": "0",
            "lastclock": "0",
            "lastns": "0",
            "lastvalue": "0",
            "prevvalue": "0"
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-acb23a7a})

[comment]: # ({new-18563427})
#### Поиск прототипа элементов данных HTTP агента

Поиск прототипа элементов данных HTTP агента с методом запроса HEAD у
заданного id узла сети.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "itemprototype.get",
    "params": {
        "hostids": "10254",
        "filter": {
            "type": "19",
            "request_method": "3"
        }
    },
    "id": 17,
    "auth": "d678e0b85688ce578ff061bd29a20d3b"
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "itemid": "28257",
            "type": "19",
            "snmp_community": "",
            "snmp_oid": "",
            "hostid": "10254",
            "name": "discovered",
            "key_": "item[{#INAME}]",
            "delay": "{#IUPDATE}",
            "history": "90d",
            "trends": "30d",
            "status": "0",
            "value_type": "3",
            "trapper_hosts": "",
            "units": "",
            "snmpv3_securityname": "",
            "snmpv3_securitylevel": "0",
            "snmpv3_authpassphrase": "",
            "snmpv3_privpassphrase": "",
            "formula": "",
            "error": "",
            "lastlogsize": "0",
            "logtimefmt": "",
            "templateid": "28255",
            "valuemapid": "0",
            "params": "",
            "ipmi_sensor": "",
            "authtype": "0",
            "username": "",
            "password": "",
            "publickey": "",
            "privatekey": "",
            "mtime": "0",
            "flags": "2",
            "interfaceid": "2",
            "port": "",
            "description": "",
            "inventory_link": "0",
            "lifetime": "30d",
            "snmpv3_authprotocol": "0",
            "snmpv3_privprotocol": "0",
            "state": "0",
            "snmpv3_contextname": "",
            "evaltype": "0",
            "jmx_endpoint": "",
            "master_itemid": "0",
            "timeout": "3s",
            "url": "{#IURL}",
            "query_fields": [],
            "posts": "",
            "status_codes": "",
            "follow_redirects": "0",
            "post_type": "0",
            "http_proxy": "",
            "headers": [],
            "retrieve_mode": "0",
            "request_method": "3",
            "output_format": "0",
            "ssl_cert_file": "",
            "ssl_key_file": "",
            "ssl_key_password": "",
            "verify_peer": "0",
            "verify_host": "0",
            "allow_traps": "0"
        }
    ],
    "id": 17
}
```

[comment]: # ({/new-18563427})

[comment]: # ({new-b9b138b8})
### Смотрите также

-   [Группа элементов
    данных](/ru/manual/api/reference/application/object#группа_элементов_данных)
-   [Узел сети](/ru/manual/api/reference/host/object#узел_сети)
-   [Прототип
    графиков](/ru/manual/api/reference/graphprototype/object#прототип_графиков)
-   [Прототип
    триггеров](/ru/manual/api/reference/triggerprototype/object#прототип_триггеров)

[comment]: # ({/new-b9b138b8})

[comment]: # ({new-e1523143})
### Исходный код

CItemPrototype::get() в
*frontends/php/include/classes/api/services/CItemPrototype.php*.

[comment]: # ({/new-e1523143})
