[comment]: # translation:outdated

[comment]: # ({new-450c25c2})
# itemprototype.create

[comment]: # ({/new-450c25c2})

[comment]: # ({new-0ad3a6f6})
### Описание

`объект itemprototype.create(объект/массив ПрототипыЭлементовданных)`

Этот метод позволяет создавать новые прототипы элементов данных.

[comment]: # ({/new-0ad3a6f6})

[comment]: # ({new-56c4ce2f})
### Параметры

`(объект/массив)` Создаваемые прототипы элементов данных.

В дополнение к [стандартным свойствам прототипа элементов
данных](object#прототип_элементов_данных), этот метод принимает
следующие параметры.

|Параметр|Тип|Описание|
|----------------|------|----------------|
|**ruleid**<br>(требуется)|строка|ID правила LLD, которому прототип элементов данных будет принадлежать.|
|applications|массив|ID назначаемых групп элементов данных к обнаруженным элементам данных.|
|applicationPrototypes|массив|Имена прототипов групп элементов данных, которые будут назначены прототипу элементов данных.|
|preprocessing|массив|Опции предварительной обработки прототипа элементов данных.|

[comment]: # ({/new-56c4ce2f})

[comment]: # ({new-4682a6ee})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID созданных прототипов
элементов данных под свойством `itemids`. Порядок возвращаемых ID
совпадает с порядком переданных прототипов элементов данных.

[comment]: # ({/new-4682a6ee})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-9956fee4})
#### Создание прототипа элементов данных

Создание прототипа элементов данных для наблюдения за свободным дисковым
пространством на обнаруженной файловой системе. Обнаруженные элементы
данных должны быть числовыми с типом Zabbix агент и обновляться каждые
30 секунд.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "itemprototype.create",
    "params": {
        "name": "Free disk space on $1",
        "key_": "vfs.fs.size[{#FSNAME},free]",
        "hostid": "10197",
        "ruleid": "27665",
        "type": 0,
        "value_type": 3,
        "interfaceid": "112",
        "delay": "30s"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "27666"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-9956fee4})

[comment]: # ({new-d22f43ae})
#### Создание прототипа элементов данных с предобработкой

Создание прототипа элементов данных, который использует изменение в
секунду и пользовательский множитель вторым шагом.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "itemprototype.create",
    "params": {
        "name": "Incoming network traffic on $1",
        "key_": "net.if.in[{#IFNAME}]",
        "hostid": "10001",
        "ruleid": "27665",
        "type": 0,
        "value_type": 3,
        "delay": "60s",
        "units": "bps",
        "interfaceid": "1155",
        "preprocessing": [
            {
                "type": "10",
                "params": ""
            },
            {
                "type": "1",
                "params": "8"
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "44211"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-d22f43ae})

[comment]: # ({new-76dad6d6})
#### Создание зависимого прототипа элементов данных

Создание зависимого прототипа элементов данных от прототипа основного
элемента данных с ID 44211. Зависимости разрешены только в пределах
одного узла сети (шаблона/правила обнаружения), поэтому у основного и
зависимого элементов данных должен быть одинаковый hostid и ruleid.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "itemprototype.create",
    "params": {
      "hostid": "10001",
      "ruleid": "27665",
      "name": "Dependent test item prototype",
      "key_": "dependent.prototype",
      "type": "18",
      "master_itemid": "44211",
      "value_type": "3"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "44212"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-76dad6d6})

[comment]: # ({new-94e7709f})
#### Создание прототипа элементов данных HTTP агента

Создание прототипа элементов данных с URL, используя пользовательский
макрос, поля запроса и пользовательские заголовки.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "itemprototype.create",
    "params": {
        "type": "19",
        "hostid": "10254",
        "ruleid":"28256",
        "interfaceid":"2",
        "name": "api item prototype example",
        "key_": "api_http_item",
        "value_type": "3",
        "url": "{$URL_PROTOTYPE}",
        "query_fields": [
            {
                "min": "10"
            },
            {
                "max": "100"
            }
        ],
        "headers": {
            "X-Source": "api"
        },
        "delay":"35"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "28305"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-94e7709f})

[comment]: # ({new-9e8477fc})
### Исходный код

CItemPrototype::create() в
*frontends/php/include/classes/api/services/CItemPrototype.php*.

[comment]: # ({/new-9e8477fc})


[comment]: # ({new-4f193e2e})
### Source

CItemPrototype::create() in
*ui/include/classes/api/services/CItemPrototype.php*.

[comment]: # ({/new-4f193e2e})
