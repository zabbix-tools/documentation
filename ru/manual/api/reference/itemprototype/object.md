[comment]: # translation:outdated

[comment]: # ({new-dbb7dd13})
# > Объект прототипа элементов данных

Следующие объекты напрямую связаны с `itemprototype` API.

[comment]: # ({/new-dbb7dd13})

[comment]: # ({new-36a2631c})
### Прототипы элементов данных

Объект прототипа элементов данных имеет следующие свойства.

|Свойство|Тип|Описание|
|----------------|------|----------------|
|itemid|строка|*(только чтение)* ID прототипа элементов данных.|
|**delay**<br>(требуется)|строка|Интервал обновления прототипа элементов данных. Принимает секунды или единицы времени с суффиксом и с или без одного или нескольких [пользовательских интервалов](/ru/manual/config/items/item/custom_intervals), которые состоят как из гибких интервалов, так и интервалов по расписанию в виде сериализованных строк. Также принимает пользовательские макросы и LLD макросы. Гибкие интервалы можно записать в виде двух макросов, разделенных прямой косой чертой. Интервалы разделаются точкой с запятой.<br><br>Опционально для элементов данных Zabbix траппер и Зависимых.|
|**hostid**<br>(требуется)|строка|ID узла сети, которому принадлежит прототип элементов данных.<br><br>При операциях обновления это поле *только для чтения*.|
|**ruleid**<br>(требуется)|строка|ID правила LLD, которому принадлежит прототип элементов данных.<br><br>При операциях обновления это поле *только для чтения*.|
|**interfaceid**<br>(требуется)|строка|ID интерфейса узла сети прототипа элементов данных. Используется только прототипами элементов данных на узлах сети.<br><br>Опционален для прототипов элементов данных Zabbix агента (активного), Zabbix внутреннего, Zabbix траппер, Зависимого элемента данных, Zabbix агрегированного, монитора баз данных и вычисляемого.|
|**key\_**<br>(требуется)|строка|Ключ прототипа элементов данных.|
|**name**<br>(требуется)|строка|Имя прототипа элементов данных.|
|**type**<br>(требуется)|целое число|Тип прототипа элементов данных.<br><br>Возможные значения:<br>0 - Zabbix агент;<br>1 - SNMPv1 агент;<br>2 - Zabbix траппер;<br>3 - простая проверка;<br>4 - SNMPv2 агент;<br>5 - Zabbix внутренний;<br>6 - SNMPv3 агент;<br>7 - Zabbix агент (активный);<br>8 - Zabbix агрегированный;<br>10 - внешняя проверка;<br>11 - монитор баз данных;<br>12 - IPMI агент;<br>13 - SSH агент;<br>14 - TELNET агент;<br>15 - вычисляемый;<br>16 - JMX агент;<br>17 - SNMP трап;<br>18 - Зависимый элемент данных;<br>19 - HTTP агент;<br>|
|**url**<br>(требуется)|строка|Строка URL, требуется для прототипа элементов данных типа HTTP агент. Поддерживаются пользовательские макросы, {HOST.IP}, {HOST.CONN}, {HOST.DNS}, {HOST.HOST}, {HOST.NAME}, {ITEM.ID}, {ITEM.KEY}.|
|**value\_type**<br>(требуется)|целое число|Тип информации прототипа элементов данных.<br><br>Возможные значения:<br>0 - числовое с плавающей точкой;<br>1 - символ;<br>2 - журнал (лог);<br>3 - числовое целое положительное;<br>4 - текст.|
|allow\_traps|целое число|Поле HTTP агента прототипа элементов данных. Позволяет заполнять значение также как и в элементе данных с типом траппер.<br><br>0 - *(по умолчанию)* Не разрешать принимать входящие данные.<br>1 - Разрешать принимать входящие данные.|
|authtype|целое число|Используется только SSH агент и HTTP агент прототипами элементов данных.<br><br>Возможные значения метода аутентификации SSH агента:<br>0 - *(по умолчанию)* пароль;<br>1 - публичный ключ.<br><br>Возможные значения метода аутентификации HTTP агента:<br>0 - *(по умолчанию)* нет<br>1 - простая<br>2 - NTLM|
|description|строка|Описание прототипа элементов данных.|
|follow\_redirects|целое число|Поле HTTP агента прототипа элементов данных. Следование перенаправлениям при опросе данных.<br><br>0 - Не следовать перенаправлениям.<br>1 - *(по умолчанию)* Следовать перенаправлениям.|
|headers|объект|Поле HTTP агента прототипа элементов данных. Объект с HTTP(S) заголовками запроса, где имя заголовка используется ключом, а значение заголовка используется значением.<br><br>Пример:<br>{ "User-Agent": "Zabbix" }|
|history|целое число|Количество времени хранения данных истории. Также принимает пользовательские макросы и LLD макросы.<br><br>По умолчанию: 90d.|
|http\_proxy|строка|Поле HTTP агента прототипа элементов данных. Строка подключения HTTP(S) прокси.|
|ipmi\_sensor|строка|Сенсор IPMI. Используется только прототипами элементов данных IPMI.|
|jmx\_endpoint|строка|Строка пользовательского соединения с JMX агентом.<br><br>Значение по умолчанию:<br>service:jmx:rmi:///jndi/rmi://{HOST.CONN}:{HOST.PORT}/jmxrmi|
|logtimefmt|строка|Формат времени в записях журнала. Используется только в элементах данных журналов.|
|master\_itemid|целое число|ID основного элемента данных.<br>Разрешена рекурсия до 3 зависимых элементов данных и прототипов элементов данных и максимальное количество зависимых элементов данных и прототипов элементов данных допустимо до 999.<br><br>Требуется для Зависимых элементов данных.|
|output\_format|целое число|Поле HTTP агента прототипа элементов данных. Нужно ли конвертировать ответ в JSON.<br><br>0 - *(по умолчанию)* Записывать сырым.<br>1 - Конвертировать в JSON.|
|params|строка|Дополнительные параметры, которые зависят от типа прототипа элементов данных:<br>- выполняемый скрипт в случае элементов данных SSH и Telnet;<br>- SQL запрос в случае элементов данных монитора базы данных;<br>- формула в случае вычисляемых элементов данных.|
|password|строка|Пароль для аутентификации. Используется прототипами элементов данных простой проверки, SSH, Telnet, монитором баз данных, JMX и HTTP агентом.|
|port|строка|Наблюдаемый прототипом элементов данных порт. Используется только прототипами элементов данных SNMP.|
|post\_type|целое число|Поле HTTP агента прототипа элементов данных. Тип тела post данных записываемых в post свойстве.<br><br>0 - *(по умолчанию)* Сырые данные.<br>2 - JSON данные.<br>3 - XML данные.|
|posts|строка|Поле HTTP агента прототипа элементов данных. Данные тела запроса HTTP(S). Используется совместно с post\_type.|
|privatekey|строка|Имя файла приватного ключа.|
|publickey|строка|Имя файла публичного ключа.|
|query\_fields|массив|Поле HTTP агента прототипа элементов данных. Параметры запроса. Массив объектов с парами 'ключ':'значение', где значение может быть пустой строкой.|
|request\_method|целое число|Поле HTTP агента прототипа элементов данных. Тип метода запроса.<br><br>0 - *(по умолчанию)* GET<br>1 - POST<br>2 - PUT<br>3 - HEAD|
|retrieve\_mode|целое число|Поле HTTP агента прототипа элементов данных. Какую часть ответа нужно сохранять.<br><br>0 - *(по умолчанию)* Тело.<br>1 - Заголовки.<br>2 - Сохранять как тело, так и заголовки.<br><br>Для request\_method HEAD разрешенное значение только 1.|
|snmp\_community|строка|SNMP community.<br>Используется только прототипами элементов данных SNMPv1 и SNMPv2.|
|snmp\_oid|строка|SNMP OID.|
|snmpv3\_authpassphrase|строка|Фраза-пароль аутентификации SNMPv3. Используется только прототипами элементов данных SNMPv3.|
|snmpv3\_authprotocol|целое число|Протокол аутентификации SNMPv3. Используется только прототипами элементов данных SNMPv3.<br><br>Возможные значения:<br>0 - *(по умолчанию)* MD5;<br>1 - SHA.|
|snmpv3\_contextname|строка|Имя контекста SNMPv3. Используется только прототипами элементов данных SNMPv3.|
|snmpv3\_privpassphrase|строка|Фраза-пароль безопасности SNMPv3. Используется только прототипами элементов данных SNMPv3.|
|snmpv3\_privprotocol|целое число|Протокол безопасности SNMPv3. Используется только прототипами элементов данных SNMPv3.<br><br>Возможные значения:<br>0 - *(по умолчанию)* DES;<br>1 - AES.|
|snmpv3\_securitylevel|целое число|Уровень безопасности SNMPv3. Используется только прототипами элементов данных SNMPv3.<br><br>Возможные значения:<br>0 - noAuthNoPriv;<br>1 - authNoPriv;<br>2 - authPriv.|
|snmpv3\_securityname|строка|Имя безопасности SNMPv3. Используется только прототипами элементов данных SNMPv3.|
|ssl\_cert\_file|строка|Поле HTTP агента прототипа элементов данных. Путь к файлу публичного SSL ключа.|
|ssl\_key\_file|строка|Поле HTTP агента прототипа элементов данных. Путь к файлу приватного SSL ключа.|
|ssl\_key\_password|строка|Поле HTTP агента прототипа элементов данных. Пароль к файлу SSL ключа.|
|status|целое число|Состояние прототипа элементов данных.<br><br>Возможные значения:<br>0 - *(по умолчанию)* активированный элемент данных;<br>1 - деактивированный элемент данных;<br>3 - неподдерживаемый прототип элементов данных.|
|status\_codes|строка|Поле HTTP агента прототипа элементов данных. Диапазоны требуемых HTTP кодов состояний, разделенные запятыми. Также как часть списка разделенного запятыми поддерживаются пользовательские макросы.<br><br>Пример: 200,200-{$M},{$M},200-400|
|templateid|строка|(только чтение) ID родительского прототипа элементов данных из шаблона.|
|timeout|строка|Поле HTTP агента прототипа элементов данных. Время ожидания запроса данных элемента данных. Поддерживаются пользовательские макросы.<br><br>по умолчанию: 3s<br>максимальное значение: 60s|
|trapper\_hosts|строка|Разрешенные хосты. Используется траппер и HTTP агент прототипами элементов данных.|
|trends|целое число|Количество времени хранения данных динамики изменений. Также принимает пользовательские макросы и LLD макросы.<br><br>По умолчанию: 365d.|
|units|строка|Единицы измерения значения.|
|username|строка|Имя пользователя для аутентификации. Используется прототипами элементов данных простой проверкой, SSH, Telnet, монитором баз данных, JMX и HTTP агентом.<br><br>Требуется для прототипов элементов данных SSH и Telnet.|
|valuemapid|строка|ID связанного преобразования значений.|
|verify\_host|целое число|Поле HTTP агента прототипа элементов данных. Проверка имени узла в URL в полях Common Name или Subject Alternate Name сертификата хоста.<br><br>0 - *(по умолчанию)* Не проверять.<br>1 - Проверять.|
|verify\_peer|целое число|Поле HTTP агента прототипа элементов данных. Проверка подлинности сертификата хоста.<br><br>0 - *(по умолчанию)* Не проверять.<br>1 - Проверять.|

[comment]: # ({/new-36a2631c})



[comment]: # ({new-66f440ed})
### Item prototype tag

The item prototype tag object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**tag**<br>(required)|string|Item prototype tag name.|
|value|string|Item prototype tag value.|

[comment]: # ({/new-66f440ed})

[comment]: # ({new-ad34a8b2})
### Item prototype preprocessing

The item prototype preprocessing object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**type**<br>(required)|integer|The preprocessing option type.<br><br>Possible values:<br>1 - Custom multiplier;<br>2 - Right trim;<br>3 - Left trim;<br>4 - Trim;<br>5 - Regular expression matching;<br>6 - Boolean to decimal;<br>7 - Octal to decimal;<br>8 - Hexadecimal to decimal;<br>9 - Simple change;<br>10 - Change per second;<br>11 - XML XPath;<br>12 - JSONPath;<br>13 - In range;<br>14 - Matches regular expression;<br>15 - Does not match regular expression;<br>16 - Check for error in JSON;<br>17 - Check for error in XML;<br>18 - Check for error using regular expression;<br>19 - Discard unchanged;<br>20 - Discard unchanged with heartbeat;<br>21 - JavaScript;<br>22 - Prometheus pattern;<br>23 - Prometheus to JSON;<br>24 - CSV to JSON;<br>25 - Replace;<br>26 - Check unsupported;<br>27 - XML to JSON.|
|**params**<br>(required)|string|Additional parameters used by preprocessing option. Multiple parameters are separated by LF (\\n) character.|
|**error\_handler**<br>(required)|integer|Action type used in case of preprocessing step failure.<br><br>Possible values:<br>0 - Error message is set by Zabbix server;<br>1 - Discard value;<br>2 - Set custom value;<br>3 - Set custom error message.|
|**error\_handler\_params**<br>(required)|string|Error handler parameters. Used with `error_handler`.<br><br>Must be empty, if `error_handler` is 0 or 1.<br>Can be empty if, `error_handler` is 2.<br>Cannot be empty, if `error_handler` is 3.|

The following parameters and error handlers are supported for each
preprocessing type.

|Preprocessing type|Name|Parameter 1|Parameter 2|Parameter 3|Supported error handlers|
|------------------|----|-----------|-----------|-----------|------------------------|
|1|Custom multiplier|number^1,\ 6^|<|<|0, 1, 2, 3|
|2|Right trim|list of characters^2^|<|<|<|
|3|Left trim|list of characters^2^|<|<|<|
|4|Trim|list of characters^2^|<|<|<|
|5|Regular expression|pattern^3^|output^2^|<|0, 1, 2, 3|
|6|Boolean to decimal|<|<|<|0, 1, 2, 3|
|7|Octal to decimal|<|<|<|0, 1, 2, 3|
|8|Hexadecimal to decimal|<|<|<|0, 1, 2, 3|
|9|Simple change|<|<|<|0, 1, 2, 3|
|10|Change per second|<|<|<|0, 1, 2, 3|
|11|XML XPath|path^4^|<|<|0, 1, 2, 3|
|12|JSONPath|path^4^|<|<|0, 1, 2, 3|
|13|In range|min^1,\ 6^|max^1,\ 6^|<|0, 1, 2, 3|
|14|Matches regular expression|pattern^3^|<|<|0, 1, 2, 3|
|15|Does not match regular expression|pattern^3^|<|<|0, 1, 2, 3|
|16|Check for error in JSON|path^4^|<|<|0, 1, 2, 3|
|17|Check for error in XML|path^4^|<|<|0, 1, 2, 3|
|18|Check for error using regular expression|pattern^3^|output^2^|<|0, 1, 2, 3|
|19|Discard unchanged|<|<|<|<|
|20|Discard unchanged with heartbeat|seconds^5,\ 6^|<|<|<|
|21|JavaScript|script^2^|<|<|<|
|22|Prometheus pattern|pattern^6,\ 7^|output^6,\ 8^|<|0, 1, 2, 3|
|23|Prometheus to JSON|pattern^6,\ 7^|<|<|0, 1, 2, 3|
|24|CSV to JSON|character^2^|character^2^|0,1|0, 1, 2, 3|
|25|Replace|search string^2^|replacement^2^|<|<|
|26|Check unsupported|<|<|<|1, 2, 3|
|27|XML to JSON|<|<|<|0, 1, 2, 3|

^1^ integer or floating-point number\
^2^ string\
^3^ regular expression\
^4^ JSONPath or XML XPath\
^5^ positive integer (with support of time suffixes, e.g. 30s, 1m, 2h,
1d)\
^6^ user macro, LLD macro\
^7^ Prometheus pattern following the syntax:
`<metric name>{<label name>="<label value>", ...} == <value>`. Each
Prometheus pattern component (metric, label name, label value and metric
value) can be user macro or LLD macro.\
^8^ Prometheus output following the syntax: `<label name>`.

[comment]: # ({/new-ad34a8b2})
