[comment]: # translation:outdated

[comment]: # ({new-cc00d3db})
# itemprototype.update

[comment]: # ({/new-cc00d3db})

[comment]: # ({new-63442cbb})
### Описание

`объект itemprototype.update(объект/массив ПрототипыЭлементовданных)`

Этот метод позволяет обновлять существующие прототипы элементов данных.

[comment]: # ({/new-63442cbb})

[comment]: # ({new-16fcb884})
### Параметры

`(объект/массив)` Свойства прототипов элементов данных, которые будут
обновлены.

Свойство `itemid` должно быть указано по каждому прототипу элементов
данных, все остальные свойства опциональны. Будут обновлены только
переданные свойства, все остальные останутся неизменными.

В дополнение к [стандартным свойствам прототипа элементов
данных](object#прототип_элементов_данных), этот метод принимает
следующие параметры.

|Параметр|Тип|Описание|
|----------------|------|----------------|
|applications|массив|ID групп элементов данных, которые заменят текущие группы элементов данных.|
|applicationPrototypes|массив|Имена прототипов групп элементов данных, которые заменят текущие прототипы групп элементов данных.|
|preprocessing|массив|Опции предобработки прототипа элементов данных, которые заменят текущие опции предварительной обработки.|

[comment]: # ({/new-16fcb884})

[comment]: # ({new-f7f1feb9})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID обновленных прототипов
элементов данных под свойством `itemids`.

[comment]: # ({/new-f7f1feb9})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-48cea947})
#### Изменение интерфейса у прототипа элементов данных

Изменение интерфейса узла сети, который будет использоваться
обнаруженными элементами данных.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "itemprototype.update",
    "params": {
        "itemid": "27428",
        "interfaceid": "132"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "27428"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-48cea947})

[comment]: # ({new-e1d8fc73})
#### Обновление прототипа зависимых элементов данных

Обновление ID прототипа основного элемента данных у прототипа зависимых
элементов данных. Зависимости разрешены только в пределах одного узла
сети (шаблона/правила обнаружения), поэтому у основного и зависимого
элементов данных должен быть одинаковый hostid и ruleid.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "itemprototype.update",
    "params": {
        "master_itemid": "25570",
        "itemid": "189030"
    },
    "auth": "700ca65537074ec963db7efabda78259",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "189030"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-e1d8fc73})

[comment]: # ({new-d7fe7206})
#### Обновление прототипа элементов данных HTTP агента

Изменение полей запроса и удаление всех пользовательских заголовков.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "itemprototype.update",
    "params": {
        "itemid":"28305",
        "query_fields": [
            {
                "random": "qwertyuiopasdfghjklzxcvbnm"
            }
        ],
        "headers": []
    }
    "auth": "700ca65537074ec963db7efabda78259",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "28305"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-d7fe7206})

[comment]: # ({new-e5fc44e0})
### Исходный код

CItemPrototype::update() в
*frontends/php/include/classes/api/services/CItemPrototype.php*.

[comment]: # ({/new-e5fc44e0})



[comment]: # ({new-15ba4525})
#### Updating a script item prototype

Update a script item prototype with a different script and remove
unnecessary parameters that were used by previous script.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "itemprototype.update",
    "params": {
        "itemid": "23865",
        "parameters": [],
        "script": "Zabbix.Log(3, 'Log test');\nreturn 1;"
    },
    "auth": "700ca65537074ec963db7efabda78259",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "23865"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-15ba4525})

[comment]: # ({new-bc6956c2})
### Source

CItemPrototype::update() in
*ui/include/classes/api/services/CItemPrototype.php*.

[comment]: # ({/new-bc6956c2})
