[comment]: # translation:outdated

[comment]: # ({new-e5a46c74})
# script.getscriptsbyhosts

[comment]: # ({/new-e5a46c74})

[comment]: # ({new-dfaa829b})
### Описание

`объект script.getscriptsbyhosts(массив hostIds)`

Этот метод позволяет получать скрипты доступные заданным узлам сети.

[comment]: # ({/new-dfaa829b})

[comment]: # ({new-080ad0f7})
### Параметры

`(строка/массив)` ID узлов сети, по которым необходимо вернуть скрипты.

[comment]: # ({/new-080ad0f7})

[comment]: # ({new-1c891d1a})
### Возвращаемые значения

`(объект)` Возвращает объект с ID узлов сети в виде свойств и массивы
доступных скриптов в виде значений.

::: notetip
Этот метод автоматически раскрывает макросы в тексте
`confirmation`.
:::

[comment]: # ({/new-1c891d1a})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-4b630539})
#### Получение скриптов по ID узлам сети

Получение всех скриптов, которые доступны узлам сети "30079" и "30073".

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "script.getscriptsbyhosts",
    "params": [
        "30079",
        "30073"
    ],
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "30079": [
            {
                "scriptid": "3",
                "name": "Detect operating system",
                "command": "sudo /usr/bin/nmap -O {HOST.CONN} 2>&1",
                "host_access": "2",
                "usrgrpid": "7",
                "groupid": "0",
                "description": "",
                "confirmation": "",
                "type": "0",
                "execute_on": "1",
                "hostid": "10001"
            },
            {
                "scriptid": "1",
                "name": "Ping",
                "command": "/bin/ping -c 3 {HOST.CONN} 2>&1",
                "host_access": "2",
                "usrgrpid": "0",
                "groupid": "0",
                "description": "",
                "confirmation": "",
                "type": "0",
                "execute_on": "1",
                "hostid": "10001"
            },
            {
                "scriptid": "2",
                "name": "Traceroute",
                "command": "/usr/bin/traceroute {HOST.CONN} 2>&1",
                "host_access": "2",
                "usrgrpid": "0",
                "groupid": "0",
                "description": "",
                "confirmation": "",
                "type": "0",
                "execute_on": "1",
                "hostid": "10001"
            }
        ],
        "30073": [
            {
                "scriptid": "3",
                "name": "Detect operating system",
                "command": "sudo /usr/bin/nmap -O {HOST.CONN} 2>&1",
                "host_access": "2",
                "usrgrpid": "7",
                "groupid": "0",
                "description": "",
                "confirmation": "",
                "type": "0",
                "execute_on": "1",
                "hostid": "10001"
            },
            {
                "scriptid": "1",
                "name": "Ping",
                "command": "/bin/ping -c 3 {HOST.CONN} 2>&1",
                "host_access": "2",
                "usrgrpid": "0",
                "groupid": "0",
                "description": "",
                "confirmation": "",
                "type": "0",
                "execute_on": "1",
                "hostid": "10001"
            },
            {
                "scriptid": "2",
                "name": "Traceroute",
                "command": "/usr/bin/traceroute {HOST.CONN} 2>&1",
                "host_access": "2",
                "usrgrpid": "0",
                "groupid": "0",
                "description": "",
                "confirmation": "",
                "type": "0",
                "execute_on": "1",
                "hostid": "10001"
            }
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-4b630539})

[comment]: # ({new-a62657e4})
### Исходный код

CScript::getScriptsByHosts() в
*frontends/php/include/classes/api/services/CScript.php*.

[comment]: # ({/new-a62657e4})
