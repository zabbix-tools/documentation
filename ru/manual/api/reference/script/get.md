[comment]: # translation:outdated

[comment]: # ({new-cd91b3fe})
# script.get

[comment]: # ({/new-cd91b3fe})

[comment]: # ({new-96ec37bc})
### Описание

`целое число/массив script.get(объект параметры)`

Этот метод позволяет получать скрипты в соответствии с заданными
параметрами.

[comment]: # ({/new-96ec37bc})

[comment]: # ({new-c425a65a})
### Параметры

`(объект)` Параметры задают желаемый вывод.

Этот метод поддерживает следующие параметры.

|Параметр|Тип|Описание|
|----------------|------|----------------|
|groupids|строка/массив|Возврат только тех скриптов, которые могут запускаться на заданных группах узлов сети.|
|hostids|строка/массив|Возврат только тех скриптов, которые могут запускаться на заданных узлах сети.|
|scriptids|строка/массив|Возвра скриптов только с заданными ID.|
|usrgrpids|строка/массив|Возврат только тех скриптов, которые могут быть запущены пользователями из заданных групп пользователей.|
|selectGroups|запрос|Возврат групп узлов сети, на которых скрипт может быть запущен, в свойстве `groups`.|
|selectHosts|запрос|Возврат узлов сети, на которых скрипт может быть запущен, в свойстве `hosts`.|
|sortfield|строка/массив|Сортировка результата в соответствии с заданными свойствами.<br><br>Возможные значения: `scriptid` и `name`.|
|countOutput|логический|Эти параметры являются общими для всех методов `get` и они описаны в [справочных комментариях](/ru/manual/api/reference_commentary#общие_параметры_get_метода).|
|editable|логический|^|
|excludeSearch|логический|^|
|filter|объект|^|
|limit|целое число|^|
|output|запрос|^|
|preservekeys|логический|^|
|search|объект|^|
|searchByAny|логический|^|
|searchWildcardsEnabled|логический|^|
|sortorder|строка/массив|^|
|startSearch|логический|^|

[comment]: # ({/new-c425a65a})

[comment]: # ({new-7223bab1})
### Возвращаемые значения

`(целое число/массив)` Возвращает либо:

-   массив объектов;
-   количество найденных объектов, если используется параметр
    `countOutput`.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-6d428f32})
#### Получение всех скриптов

Получение всех добавленных скриптов.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "script.get",
    "params": {
        "output": "extend"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [    
        {
            "scriptid": "1",
            "name": "Ping",
            "command": "/bin/ping -c 3 {HOST.CONN} 2>&1",
            "host_access": "2",
            "usrgrpid": "0",
            "groupid": "0",
            "description": "",
            "confirmation": "",
            "type": "0",
            "execute_on": "1"
        },
        {
            "scriptid": "2",
            "name": "Traceroute",
            "command": "/usr/bin/traceroute {HOST.CONN} 2>&1",
            "host_access": "2",
            "usrgrpid": "0",
            "groupid": "0",
            "description": "",
            "confirmation": "",
            "type": "0",
            "execute_on": "1"
        },
        {
            "scriptid": "3",
            "name": "Detect operating system",
            "command": "sudo /usr/bin/nmap -O {HOST.CONN} 2>&1",
            "host_access": "2",
            "usrgrpid": "7",
            "groupid": "0",
            "description": "",
            "confirmation": "",
            "type": "0",
            "execute_on": "1"
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-6d428f32})

[comment]: # ({new-b0b740ec})
### Смотрите также

-   [Узел сети](/ru/manual/api/reference/host/object#узел_сети)
-   [Группа узлов
    сети](/ru/manual/api/reference/hostgroup/object#группа_узлов_сети)

[comment]: # ({/new-b0b740ec})

[comment]: # ({new-90dfc753})
### Исходный код

CScript::get() в
*frontends/php/include/classes/api/services/CScript.php*.

[comment]: # ({/new-90dfc753})
