[comment]: # translation:outdated

[comment]: # ({new-ceca660a})
# script.delete

[comment]: # ({/new-ceca660a})

[comment]: # ({new-db451e99})
### Описание

`объект script.delete(массив scriptIds)`

Этот метод позволяет удалять скрипты.

[comment]: # ({/new-db451e99})

[comment]: # ({new-17e2e442})
### Параметры

`(массив)` ID удаляемых скриптов.

[comment]: # ({/new-17e2e442})

[comment]: # ({new-64908342})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID удаленных скриптов под
свойством `scriptids`.

[comment]: # ({/new-64908342})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-8184d373})
#### Удаление нескольких скриптов

Удаление двух скриптов.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "script.delete",
    "params": [
        "3",
        "4"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "scriptids": [
            "3",
            "4"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-8184d373})

[comment]: # ({new-ccc98e5e})
### Исходный код

CScript::delete() в
*frontends/php/include/classes/api/services/CScript.php*.

[comment]: # ({/new-ccc98e5e})
