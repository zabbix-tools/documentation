[comment]: # translation:outdated

[comment]: # ({new-3a2a8d58})
# script.execute

[comment]: # ({/new-3a2a8d58})

[comment]: # ({new-60a9dbe3})
### Описание

`объект script.execute(объект параметры)`

Этот метод позволять выполнять скрипт на узле сети.

[comment]: # ({/new-60a9dbe3})

[comment]: # ({new-e4fa8ac7})
### Параметры

`(объект)` Параметры, которые содержат ID выполняемого скрипта и ID узла
сети.

|Параметр|Тип|Описание|
|----------------|------|----------------|
|**hostid**<br>(требуется)|строка|ID узла сети, на котором необходимо выполнить скрипт.|
|**scriptid**<br>(требуется)|строка|ID выполняемого скрипта.|

[comment]: # ({/new-e4fa8ac7})

[comment]: # ({new-dde54e60})
### Возвращаемые значения

`(объект)` Возвращает результат выполнения скрипта.

|Свойство|Тип|Описание|
|----------------|------|----------------|
|response|строка|Выполнился ли скрипт успешно.<br><br>Возможные значения: `success` или `failed`.|
|value|строка|Вывод скрипта.|

[comment]: # ({/new-dde54e60})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-03cb1cf3})
#### Выполнение скрипта

Выполнение "ping" скрипта на узле сети.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "script.execute",
    "params": {
        "scriptid": "1",
        "hostid": "30079"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "response": "success",
        "value": "PING 127.0.0.1 (127.0.0.1) 56(84) bytes of data.\n64 bytes from 127.0.0.1: icmp_req=1 ttl=64 time=0.074 ms\n64 bytes from 127.0.0.1: icmp_req=2 ttl=64 time=0.030 ms\n64 bytes from 127.0.0.1: icmp_req=3 ttl=64 time=0.030 ms\n\n--- 127.0.0.1 ping statistics ---\n3 packets transmitted, 3 received, 0% packet loss, time 1998ms\nrtt min/avg/max/mdev = 0.030/0.044/0.074/0.022 ms\n"
    },
    "id": 1
}
```

[comment]: # ({/new-03cb1cf3})

[comment]: # ({new-baebff97})
### Исходный код

CScript::execute() в
*frontends/php/include/classes/api/services/CScript.php*.

[comment]: # ({/new-baebff97})


[comment]: # ({new-e7dc45a5})
### Source

CScript::execute() in *ui/include/classes/api/services/CScript.php*.

[comment]: # ({/new-e7dc45a5})
