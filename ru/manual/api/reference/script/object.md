[comment]: # translation:outdated

[comment]: # ({new-8b1700c1})
# > Объект скрипта

Следующие объекты напрямую связаны с `script` API.

[comment]: # ({/new-8b1700c1})

[comment]: # ({new-9c6db006})
### Скрипт

Объект скрипта имеет следующие свойства.

|Свойство|Тип|Описание|
|----------------|------|----------------|
|scriptid|строка|*(только чтение)* ID скрипта.|
|**command**<br>(требуется)|строка|Выполняемая команда.|
|**name**<br>(требуется)|строка|Имя скрипта.|
|confirmation|строка|Текст подтверждения во всплывающем окне. Всплывающее окно появляется при попытке выполнения скрипта из Zabbix веб-интерфейса.|
|description|строка|Описание скрипта.|
|execute\_on|целое число|Где выполнять скрипт.<br><br>Возможные значения:<br>0 - выполнение на Zabbix агенте;<br>1 - выполнение на Zabbix сервере;<br>2 - *(по умолчанию)* выполнение на Zabbix сервере (прокси).|
|groupid|строка|ID группы узлов сети для которой можно выполнять скрипт. Если задано значение 0, скрипт можно выполнять по всем группам узлов сети.<br><br>По умолчанию: 0.|
|host\_access|целое число|Требуемые права доступа к узлу сети для выполнения скрипта.<br><br>Возможные значения:<br>2 - *(по умолчанию)* чтение;<br>3 - запись.|
|type|целое число|Тип скрипта.<br><br>Возможные значения:<br>0 - *(по умолчанию)* скрипт;<br>1 - IPMI.|
|usrgrpid|строка|ID группы пользователей, которой разрешено выполнение скрипта. Если задано значение 0, скрипт доступен всем группам пользователей.<br><br>По умолчанию: 0.|

[comment]: # ({/new-9c6db006})




[comment]: # ({new-05b0e37d})
### Webhook parameters

Parameters passed to webhook script when it is called have the following
properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**name**<br>(required)|string|Parameter name.|
|value|string|Parameter value. Supports [macros](/manual/appendix/macros/supported_by_location).|

[comment]: # ({/new-05b0e37d})

[comment]: # ({new-587d2fd0})
### Debug

Debug information of executed webhook script. The debug object has the
following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|logs|array|Array of [log entries](/manual/api/reference/script/object#Log entry).|
|ms|string|Script execution duration in milliseconds.|

[comment]: # ({/new-587d2fd0})

[comment]: # ({new-8ea23127})
### Log entry

The log entry object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|level|integer|Log level.|
|ms|string|The time elapsed in milliseconds since the script was run before log entry was added.|
|message|string|Log message.|

[comment]: # ({/new-8ea23127})
