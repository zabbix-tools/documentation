[comment]: # translation:outdated

[comment]: # ({new-4f2ac53b})
# script.update

[comment]: # ({/new-4f2ac53b})

[comment]: # ({new-3bbc6786})
### Описание

`объект script.update(объект/массив скрипты)`

Этот метод позволяет обновлять существующие скрипты.

[comment]: # ({/new-3bbc6786})

[comment]: # ({new-8f315bab})
### Параметры

`(объект/массив)` [Свойства скрипта](object#скрипт), которые будут
обновлены.

Свойство `scriptid` должно быть указано по каждому скрипту, все
остальные свойства опциональны. Будут обновлены только переданные
свойства, все остальные останутся неизменными.

[comment]: # ({/new-8f315bab})

[comment]: # ({new-5062432a})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID обновленных скриптов
под свойством `scriptids`.

[comment]: # ({/new-5062432a})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-4f3506ff})
#### Изменение команды скрипта

Изменение команды скрипта на "/bin/ping -c 10 {HOST.CONN} 2>&1".

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "script.update",
    "params": {
        "scriptid": "1",
        "command": "/bin/ping -c 10 {HOST.CONN} 2>&1"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "scriptids": [
            "1"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-4f3506ff})

[comment]: # ({new-cded8a11})
### Исходный код

CScript::update() в
*frontends/php/include/classes/api/services/CScript.php*.

[comment]: # ({/new-cded8a11})
