[comment]: # translation:outdated

[comment]: # ({new-d373938d})
# Скрипт

Этот класс предназначен для работы со скриптами.

Справка по объектам:\

-   [Скрипт](/ru/manual/api/reference/script/object#скрипт)

Доступные методы:\

-   [script.create](/ru/manual/api/reference/script/create) - создание
    новых скриптов
-   [script.delete](/ru/manual/api/reference/script/delete) - удаление
    скриптов
-   [script.execute](/ru/manual/api/reference/script/execute) -
    выполнение скриптов
-   [script.get](/ru/manual/api/reference/script/get) - получение
    скриптов
-   [script.getscriptsbyhosts](/ru/manual/api/reference/script/getscriptsbyhosts) -
    получение скриптов по узлам сети
-   [script.update](/ru/manual/api/reference/script/update) - обновление
    скриптов

[comment]: # ({/new-d373938d})
