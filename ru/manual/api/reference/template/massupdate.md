[comment]: # translation:outdated

[comment]: # ({new-e42e8a0a})
# template.massupdate

[comment]: # ({/new-e42e8a0a})

[comment]: # ({new-cf27d8f8})
### Описание

`объект template.massupdate(объект параметры)`

Этот метод позволяет заменить или удалить связанные объекты и обновить
свойства сразу на нескольких шаблонах.

[comment]: # ({/new-cf27d8f8})

[comment]: # ({new-1f7d5759})
### Параметры

`(объект)` Параметры, которые содержат ID обновляемых шаблонов и их
свойства, которые необходимо обновить.

В дополнение к [стандартным свойствам шаблона](object#шаблон), этот
метод принимает следующие параметры.

|Параметр|Тип|Описание|
|----------------|------|----------------|
|**templates**<br>(требуется)|объект/массив|Обновляемые шаблоны.<br><br>У шаблонов должно быть задано свойство `templateid`.|
|groups|объект/массив|Группы узлов сети, которые заменят текущие группы узлов сети шаблонов.<br><br>У групп узлов сети должно быть задано свойство `groupid`.|
|hosts|объект/массив|Узлы сети и шаблоны, которые заменят текущие присоединенные узлы сети и шаблоны.<br><br>Как у узлов сети, так и у шаблонов, чтобы указать ID, необходимо использовать свойство `hostid`.|
|macros|объект/массив|Пользовательские макросы, которые заменят текущие макросы у заданных шаблонов.|
|templates\_clear|объект/массив|Шаблоны, которые необходимо отсоединить и очистить от заданных шаблонов.<br><br>У шаблонов должно быть задано свойство `templateid`.|
|templates\_link|объект/массив|Шаблоны, которые заменят присоединенные в настоящий момент шаблоны.<br><br>У шаблонов должно быть задано свойство `templateid`.|

[comment]: # ({/new-1f7d5759})

[comment]: # ({new-dcba01c8})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID обновленных шаблонов
под свойством `templateids`.

[comment]: # ({/new-dcba01c8})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-d18b091c})
#### Unlinking a template

Unlink and clear template "10091" from the given templates.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "template.massupdate",
    "params": {
        "templates": [
            {
                "templateid": "10085"
            },
            {
                "templateid": "10086"
            }
        ],
        "templates_clear": [
            {
                "templateid": "10091"
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "templateids": [
            "10085",
            "10086"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-d18b091c})

[comment]: # ({new-0ecefb75})
#### Замена групп узлов сети

Отсоединение и очистка шаблона "10091" с заданных шаблонов.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "template.massupdate",
    "params": {
        "templates": [
            {
                "templateid": "10085"
            },
            {
                "templateid": "10086"
            }
        ],
        "templates_clear": [
            {
                "templateid": "10091"
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "templateids": [
            "10085",
            "10086"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-0ecefb75})

[comment]: # ({new-5d283e35})
### Смотрите также

-   [template.update](update)
-   [template.massadd](massadd)
-   [Группа узлов
    сети](/ru/manual/api/reference/hostgroup/object#группы_узлов_сети)
-   [Пользовательский
    макрос](/ru/manual/api/reference/usermacro/object#макрос_узла_сети)

[comment]: # ({/new-5d283e35})

[comment]: # ({new-b5f8dcd9})
### Исходный код

CTemplate::massUpdate() в
*frontends/php/include/classes/api/services/CTemplate.php*.

[comment]: # ({/new-b5f8dcd9})
