[comment]: # translation:outdated

[comment]: # ({new-a7242998})
# template.delete

[comment]: # ({/new-a7242998})

[comment]: # ({new-484b4ad2})
### Описание

`объект template.delete(массив templateIds)`

Этот метод позволяет удалять шаблоны.

[comment]: # ({/new-484b4ad2})

[comment]: # ({new-1fbebaa3})
### Параметры

`(массив)` ID удаляемых шаблонов.

[comment]: # ({/new-1fbebaa3})

[comment]: # ({new-fdd33aa6})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID удаленных шаблонов под
свойством `templateids`.

[comment]: # ({/new-fdd33aa6})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-3c8960bb})
#### Удаление нескольких шаблонов

Удаление двух шаблонов.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "template.delete",
    "params": [
        "13",
        "32"
    ],
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "templateids": [
            "13",
            "32"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-3c8960bb})

[comment]: # ({new-bfde186e})
### Исходный код

CTemplate::delete() в *ui/include/classes/api/services/CTemplate.php*.

[comment]: # ({/new-bfde186e})
