[comment]: # translation:outdated

[comment]: # ({new-ae0db7b8})
# template.massremove

[comment]: # ({/new-ae0db7b8})

[comment]: # ({new-ba911402})
### Описание

`объект template.massremove(объект параметры)`

Этот метод позволяет удалить связанные объекты с нескольких шаблонов.

[comment]: # ({/new-ba911402})

[comment]: # ({new-bd7f8b44})
### Параметры

`(объект)` Параметры, которые содержат ID обновляемых шаблонов и
объектов, которые необходимо удалить.

|Параметр|Тип|Описание|
|----------------|------|----------------|
|**templateids**<br>(требуется)|строка/массив|ID обновляемых шаблонов.|
|groupids|строка/массив|Группы узлов сети из которых необходимо убрать заданные шаблоны.|
|hostids|строка/массив|Узлы сети или шаблоны, которые необходимо отсоединить от заданных шаблонов (в порядке убывания).|
|macros|строка/массив|Пользовательские макросы, которые необходимо удалить с заданных шаблонов.|
|templateids\_clear|строка/массив|Шаблоны, которые необходимо отсоединить и очистить с заданных шаблонов (в порядке возрастания).|
|templateids\_link|строка/массив|Шаблоны, которые необходимо отсоединить от заданных шаблонов (в порядке возрастания).|

[comment]: # ({/new-bd7f8b44})

[comment]: # ({new-dcba01c8})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID обновленных шаблонов
под свойством `templateids`.

[comment]: # ({/new-dcba01c8})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-20c1ddc2})
#### Удаление шаблонов из группы

Удаление двух шаблонов из группы "2".

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "template.massremove",
    "params": {
        "templateids": [
            "10085",
            "10086"
        ],
        "groupids": "2"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "templateids": [
            "10085",
            "10086"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-20c1ddc2})

[comment]: # ({new-6459bc33})
#### Unlinking templates from a host

Unlink templates "10106" and "10104" from template "10085".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "template.massremove",
    "params": {
        "templateids": "10085",
        "templateids_link": [
            "10106",
            "10104"
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "templateids": [
            "10085"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-6459bc33})


[comment]: # ({new-186aec66})
### Смотрите также

-   [template.update](update)
-   [Пользовательский
    макрос](/ru/manual/api/reference/usermacro/object#макрос_узла_сети)

[comment]: # ({/new-186aec66})

[comment]: # ({new-aba9c6f9})
### Исходный код

CTemplate::massRemove() в
*frontends/php/include/classes/api/services/CTemplate.php*.

[comment]: # ({/new-aba9c6f9})
