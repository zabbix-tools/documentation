[comment]: # translation:outdated

[comment]: # ({new-8c618db1})
# template.massadd

[comment]: # ({/new-8c618db1})

[comment]: # ({new-399b9834})
### Описание

`объект template.massadd(объект параметры)`

Этот метод позволяет добавить одновременно несколько связанных объектов
во все заданные шаблоны.

[comment]: # ({/new-399b9834})

[comment]: # ({new-4167b841})
### Параметры

`(объект)` Параметры, которые содержат ID обновляемых шаблонов и
добавляемых объектов в во все эти узлы сети.

Этот метод принимает следующие параметры.

|Параметр|Тип|Описание|
|----------------|------|----------------|
|**templates**<br>(требуется)|объект/массив|Обновляемые шаблоны.<br><br>У шаблонов должно быть задано свойство `templateid`.|
|groups|объект/массив|Группы узлов сети, в которые необходимо добавить заданные шаблоны.<br><br>У групп узлов сети должно быть задано свойство `groupid`.|
|hosts|объект/массив|Узлы сети и шаблоны, к которым необходимо присоединить заданные шаблоны.<br><br>У узлов сети должно быть задано свойство `hostid`.|
|macros|объект/массив|Создаваемые пользовательские макросы у заданных шаблонов.|
|templates\_link|объект/массив|Присоединяемые шаблоны к заданным шаблонам.<br><br>У шаблонов должно быть задано свойство `templateid`.|

[comment]: # ({/new-4167b841})

[comment]: # ({new-dcba01c8})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID обновленных шаблонов
под свойством `templateids`.

[comment]: # ({/new-dcba01c8})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-f8fe44e4})
#### Добавление шаблонов в группу

Добавление двух шаблонов в группу узлов сети "2".

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "template.massadd",
    "params": {
        "templates": [
            {
                "templateid": "10085"
            },
            {
                "templateid": "10086"
            }
        ],
        "groups": [
            {
                "groupid": "2"
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "templateids": [
            "10085",
            "10086"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-f8fe44e4})

[comment]: # ({new-7dbed7e5})
#### Link two templates to a template

Link templates "10106" and "10104" to template "10073".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "template.massadd",
    "params": {
        "templates": [
            {
                "templateid": "10073"
            }
        ],
        "templates_link": [
            {
                "templateid": "10106"
            },
            {
                "templateid": "10104"
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "templateids": [
            "10073"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-7dbed7e5})


[comment]: # ({new-bd8dd370})
### Смотрите также

-   [template.update](update)
-   [Узел сети](/ru/manual/api/reference/host/object#узел_сети)
-   [Группа узлов
    сети](/ru/manual/api/reference/hostgroup/object#группа_узлов_сети)
-   [Пользовательский
    макрос](/ru/manual/api/reference/usermacro/object#макрос_узла_сети)

[comment]: # ({/new-bd8dd370})

[comment]: # ({new-2f7abcb1})
### Исходный код

CTemplate::massAdd() в
*frontends/php/include/classes/api/services/CTemplate.php*.

[comment]: # ({/new-2f7abcb1})
