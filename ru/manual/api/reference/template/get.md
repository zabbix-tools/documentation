[comment]: # translation:outdated

[comment]: # ({new-896915aa})
# template.get

[comment]: # ({/new-896915aa})

[comment]: # ({new-4e400711})
### Описание

`целое число/массив template.get(объект параметры)`

Этот метод позволяет получать шаблоны в соответствии с заданными
параметрами.

[comment]: # ({/new-4e400711})

[comment]: # ({new-97f0e40b})
### Параметры

`(объект)` Параметры задают желаемый вывод.

Этот метод поддерживает следующие параметры.

|Параметр|Тип|Описание|
|----------------|------|----------------|
|templateids|строка/массив|Возврат шаблонов только с заданными ID шаблонов.|
|groupids|строка/массив|Возврат только тех шаблонов, которые принадлежат заданным группам узлов сети.|
|parentTemplateids|строка/массив|Возврат только тех шаблонов, которые являются дочерними заданным шаблонам.|
|hostids|строка/массив|Возврат только тех шаблонов, которые присоединены к заданным узлам сети.|
|graphids|строка/массив|Возврат только тех шаблонов, которые содержат заданные графики.|
|itemids|строка/массив|Возврат только тех шаблонов, которые содержат заданные элементы данных.|
|triggerids|строка/массив|Возврат только тех шаблонов, которые содержат заданные триггеры.|
|with\_items|флаг|Возврат только тех шаблонов, которые имеют элементы данных.|
|with\_triggers|флаг|Возврат только тех шаблонов, которые имеют триггеры.|
|with\_graphs|флаг|Возврат только тех шаблонов, которые имеют графики.|
|with\_httptests|флаг|Возврат только тех шаблонов, которые имеют веб-сценарии.|
|selectGroups|запрос|Возврат групп узлов сети, которым принадлежит шаблон, в свойстве `groups`.|
|selectHosts|запрос|Возврат узлов сети, которые присоединены к шаблону, в свойстве `hosts`.<br><br>Поддерживается `count`.|
|selectTemplates|запрос|Возврат дочерних шаблонов в свойстве `templates`.<br><br>Поддерживается `count`.|
|selectParentTemplates|запрос|Возврат родительских шаблонов в свойстве `parentTemplates`.<br><br>Поддерживается `count`.|
|selectHttpTests|запрос|Возврат веб-сценариев из шаблона в свойстве `httpTests`.<br><br>Поддерживается `count`.|
|selectItems|запрос|Возврат элементов данных из шаблона в свойстве `items`.<br><br>Поддерживается `count`.|
|selectDiscoveries|запрос|Возврат низкоуровневых обнаружений из шаблона в свойстве `discoveries`.<br><br>Поддерживается `count`.|
|selectTriggers|запрос|Возврат триггеров из шаблона в свойстве `triggers`.<br><br>Поддерживается `count`.|
|selectGraphs|запрос|Возврат графиков из шаблона в свойстве `graphs`.<br><br>Поддерживается `count`.|
|selectApplications|запрос|Возврат групп элементов данных из шаблона в свойстве `applications`.<br><br>Поддерживается `count`.|
|selectMacros|запрос|Возврат макросов из шаблона в свойстве `macros`.|
|selectScreens|запрос|Возврат комплексных экранов из шаблона в свойстве `screens`.<br><br>Поддерживается `count`.|
|limitSelects|целое число|Ограничение количества записей, возвращаемых подзапросами.<br><br>Применимо только к следующим подзапросам:<br>`selectTemplates` - результаты сортируются по `name`;<br>`selectHosts` - сортируются по `host`;<br>`selectParentTemplates` - сортируются по `host`;<br>`selectItems` - сортируются по `name`;<br>`selectDiscoveries` - сортируются по `name`;<br>`selectTriggers` - сортируются по `description`;<br>`selectGraphs` - сортируются по `name`;<br>`selectApplications` - сортируются по `name`;<br>`selectScreens` - сортируются по `name`.|
|sortfield|строка/массив|Сортировка результата в соответствии с заданными свойствами.<br><br>Возможные значения: `hostid`, `host`, `name`, `status`.|
|countOutput|логический|Эти параметры являются общими для всех методов `get` и они описаны в [справочных комментариях](/ru/manual/api/reference_commentary#общие_параметры_get_метода).|
|editable|логический|^|
|excludeSearch|логический|^|
|filter|объект|^|
|limit|целое число|^|
|output|запрос|^|
|preservekeys|логический|^|
|search|объект|^|
|searchByAny|логический|^|
|searchWildcardsEnabled|логический|^|
|sortorder|строка/массив|^|
|startSearch|логический|^|

[comment]: # ({/new-97f0e40b})

[comment]: # ({new-7223bab1})
### Возвращаемые значения

`(целое число/массив)` Возвращает либо:

-   массив объектов;
-   количество найденных объектов, если используется параметр
    `countOutput`.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-7e546e9a})
#### Получение шаблонов по имени

Получение всех данных о двух шаблонах с именами "Template OS Linux" и
"Template OS Windows".

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "template.get",
    "params": {
        "output": "extend",
        "filter": {
            "host": [
                "Template OS Linux",
                "Template OS Windows"
            ]
        }
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "proxy_hostid": "0",
            "host": "Template OS Linux",
            "status": "3",
            "disable_until": "0",
            "error": "",
            "available": "0",
            "errors_from": "0",
            "lastaccess": "0",
            "ipmi_authtype": "0",
            "ipmi_privilege": "2",
            "ipmi_username": "",
            "ipmi_password": "",
            "ipmi_disable_until": "0",
            "ipmi_available": "0",
            "snmp_disable_until": "0",
            "snmp_available": "0",
            "maintenanceid": "0",
            "maintenance_status": "0",
            "maintenance_type": "0",
            "maintenance_from": "0",
            "ipmi_errors_from": "0",
            "snmp_errors_from": "0",
            "ipmi_error": "",
            "snmp_error": "",
            "jmx_disable_until": "0",
            "jmx_available": "0",
            "jmx_errors_from": "0",
            "jmx_error": "",
            "name": "Template OS Linux",
            "flags": "0",
            "templateid": "10001",
            "description": "",
            "tls_connect": "1",
            "tls_accept": "1",
            "tls_issuer": "",
            "tls_subject": "",
            "tls_psk_identity": "",
            "tls_psk": ""
        },
        {
            "proxy_hostid": "0",
            "host": "Template OS Windows",
            "status": "3",
            "disable_until": "0",
            "error": "",
            "available": "0",
            "errors_from": "0",
            "lastaccess": "0",
            "ipmi_authtype": "0",
            "ipmi_privilege": "2",
            "ipmi_username": "",
            "ipmi_password": "",
            "ipmi_disable_until": "0",
            "ipmi_available": "0",
            "snmp_disable_until": "0",
            "snmp_available": "0",
            "maintenanceid": "0",
            "maintenance_status": "0",
            "maintenance_type": "0",
            "maintenance_from": "0",
            "ipmi_errors_from": "0",
            "snmp_errors_from": "0",
            "ipmi_error": "",
            "snmp_error": "",
            "jmx_disable_until": "0",
            "jmx_available": "0",
            "jmx_errors_from": "0",
            "jmx_error": "",
            "name": "Template OS Windows",
            "flags": "0",
            "templateid": "10081",
            "description": "",
            "tls_connect": "1",
            "tls_accept": "1",
            "tls_issuer": "",
            "tls_subject": "",
            "tls_psk_identity": "",
            "tls_psk": ""
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-7e546e9a})

[comment]: # ({new-84d0cbf3})
#### Retrieving template groups

Retrieve template groups that the template "Linux by Zabbix agent" is a member of.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "template.get",
    "params": {
        "output": ["hostid"],
        "selectTemplateGroups": "extend",
        "filter": {
            "host": [
                "Linux by Zabbix agent"
            ]
        }
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": [
        {
            "templateid": "10001",
            "templategroups": [
                {
                    "groupid": "10",
                    "name": "Templates/Operating systems",
                    "uuid": "846977d1dfed4968bc5f8bdb363285bc"
                }
            ]
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-84d0cbf3})

[comment]: # ({new-0fe2d603})
### Смотрите также

-   [Группа узлов
    сети](/ru/manual/api/reference/hostgroup/object#группа_узлов_сети)
-   [Шаблон](object#шаблон)
-   [Пользовательский
    макрос](/ru/manual/api/reference/usermacro/object#макрос_узла_сети)
-   [Интерфейс узла
    сети](/ru/manual/api/reference/hostinterface/object#интерфейс_узла_сети)

[comment]: # ({/new-0fe2d603})

[comment]: # ({new-ee3fc022})
### Исходный код

CTemplate::get() в *ui/include/classes/api/services/CTemplate.php*.

[comment]: # ({/new-ee3fc022})


[comment]: # ({new-4fdbde5d})
### Source

CTemplate::get() in *ui/include/classes/api/services/CTemplate.php*.

[comment]: # ({/new-4fdbde5d})
