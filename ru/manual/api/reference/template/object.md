[comment]: # translation:outdated

[comment]: # ({new-ac6966b9})
# > Объект шаблона

Следующие объекты напрямую связаны с `template` API.

[comment]: # ({/new-ac6966b9})

[comment]: # ({new-ccfaad06})
### Шаблон

Объект шаблона имеет следующие свойства.

|Свойство|Тип|Описание|
|----------------|------|----------------|
|templateid|строка|*(только чтение)* ID шаблона.|
|**host**<br>(требуется)|строка|Техническое имя шаблона.|
|description|текст|Описание шаблона.|
|name|строка|Видимое имя шаблона.<br><br>По умолчанию: значение свойства `host`.|

[comment]: # ({/new-ccfaad06})

[comment]: # ({new-2248aef2})
### Template tag

The template tag object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**tag**<br>(required)|string|Template tag name.|
|value|string|Template tag value.|

[comment]: # ({/new-2248aef2})

