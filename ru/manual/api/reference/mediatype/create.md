[comment]: # translation:outdated

[comment]: # ({new-fb5f6958})
# mediatype.create

[comment]: # ({/new-fb5f6958})

[comment]: # ({new-34dd1ecc})
### Описание

`объект mediatype.create(объект/массив СпособыОповещения)`

Этот метод позволяет создавать новые способы оповещения.

[comment]: # ({/new-34dd1ecc})

[comment]: # ({new-dbb8bb8d})
### Параметры

`(объект/массив)` Создаваемые способы оповещения.

Этот метод принимает способы оповещения со [стандартными свойствами
способа оповещения](object#способ_оповещения).

[comment]: # ({/new-dbb8bb8d})

[comment]: # ({new-b1eed639})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID созданных способов
оповещения под свойством `mediatypeids`. Порядок возвращаемых ID
совпадает с порядком переданных способов оповещения.

[comment]: # ({/new-b1eed639})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-e61703a8})
#### Создание способа оповещения

Создание способа оповещения с типом e-mail.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "mediatype.create",
    "params": {
        "description": "E-mail",
        "type": 0,
        "smtp_server": "rootmail@company.com",
        "smtp_helo": "company.com",
        "smtp_email": "zabbix@company.com"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "mediatypeids": [
            "7"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-e61703a8})

[comment]: # ({new-1711e041})
#### Создание способа оповещения с пользовательским набором опций

Создание нового способа оповещения скриптом с пользовательским значением
количества попыток и интервала между попытками.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "mediatype.create",
    "params": {
        "type": 1,
        "description": "Push notifications",
        "exec_path": "push-notification.sh",
        "exec_params": "{ALERT.SENDTO}\n{ALERT.SUBJECT}\n{ALERT.MESSAGE}\n",
        "maxattempts": "5",
        "attempt_interval": "11s"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "mediatypeids": [
            "8"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-1711e041})

[comment]: # ({new-9932dd76})
### Исходный код

CMediaType::create() в
*frontends/php/include/classes/api/services/CMediaType.php*.

[comment]: # ({/new-9932dd76})


[comment]: # ({new-06979650})
### Source

CMediaType::create() in
*ui/include/classes/api/services/CMediaType.php*.

[comment]: # ({/new-06979650})
