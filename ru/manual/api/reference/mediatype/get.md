[comment]: # translation:outdated

[comment]: # ({new-16011390})
# mediatype.get

[comment]: # ({/new-16011390})

[comment]: # ({new-10e7fdda})
### Описание

`целое число/массив mediatype.get(объект параметры)`

Этот метод позволяет получать способы оповещения в соответствии с
заданными параметрами.

[comment]: # ({/new-10e7fdda})

[comment]: # ({new-dde2e026})
### Параметры

`(объект)` Параметры задают желаемый вывод.

Этот метод поддерживает следующие параметры.

|Параметр|Тип|Описание|
|----------------|------|----------------|
|mediatypeids|строка/массив|Возврат способов оповещения только с заданными ID.|
|mediaids|строка/массив|Возврат только тех способов оповещения, которые используются заданными оповещениями пользователей.|
|userids|строка/массив|Возврат только тех способов оповещения, которые используются заданными пользователями.|
|selectUsers|запрос|Возврат пользователей, которые используют способ оповещения, в свойстве `users`.|
|sortfield|строка/массив|Сортировка результата в соответствии с заданными свойствами.<br><br>Возможные значения: `mediatypeid`.|
|countOutput|логический|Эти параметры являются общими для всех методов `get` и они описаны в [справочных комментариях](/ru/manual/api/reference_commentary#общие_параметры_get_метода).|
|editable|логический|^|
|excludeSearch|логический|^|
|filter|объект|^|
|limit|целое число|^|
|output|запрос|^|
|preservekeys|логический|^|
|search|объект|^|
|searchByAny|логический|^|
|searchWildcardsEnabled|логический|^|
|sortorder|строка/массив|^|
|startSearch|логический|^|

[comment]: # ({/new-dde2e026})

[comment]: # ({new-7223bab1})
### Возвращаемые значения

`(целое число/массив)` Возвращает либо:

-   массив объектов;
-   количество найденных объектов, если используется параметр
    `countOutput`.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-173c5ab7})
#### Получение способов оповещения

Получение всех добавленных способов оповещения.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "mediatype.get",
    "params": {
        "output": "extend"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "mediatypeid": "1",
            "type": "0",
            "description": "Email",
            "smtp_server": "mail.company.com",
            "smtp_helo": "company.com",
            "smtp_email": "zabbix@company.com",
            "exec_path": "",
            "gsm_modem": "",
            "username": "",
            "passwd": "",
            "status": "0",
            "maxsessions": "1",
            "maxattempts": "7",
            "attempt_interval": "10s"
        },
        {
            "mediatypeid": "2",
            "type": "3",
            "description": "Jabber",
            "smtp_server": "",
            "smtp_helo": "",
            "smtp_email": "",
            "exec_path": "",
            "gsm_modem": "",
            "username": "jabber@company.com",
            "passwd": "zabbix",
            "status": "0",
            "maxsessions": "1",
            "maxattempts": "7",
            "attempt_interval": "10s"
        },
        {
            "mediatypeid": "3",
            "type": "2",
            "description": "SMS",
            "smtp_server": "",
            "smtp_helo": "",
            "smtp_email": "",
            "exec_path": "",
            "gsm_modem": "/dev/ttyS0",
            "username": "",
            "passwd": "",
            "status": "0",
            "maxsessions": "1",
            "maxattempts": "7",
            "attempt_interval": "10s"
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-173c5ab7})

[comment]: # ({new-621ff52f})

#### Retrieve script and webhook media types

The following example returns three media types:

-   script media type with parameters;
-   script media type without parameters;
-   webhook media type with parameters.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "mediatype.get",
    "params": {
        "output": ["mediatypeid", "name", "parameters"],
        "filter": {
            "type": [1, 4]
        }
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": [
        {
            "mediatypeid": "10",
            "name": "Script with parameters",
            "parameters": [
                {
                    "sortorder": "0",
                    "value": "{ALERT.SENDTO}"
                },
                {
                    "sortorder": "1",
                    "value": "{EVENT.NAME}"
                },
                {
                    "sortorder": "2",
                    "value": "{ALERT.MESSAGE}"
                },
                {
                    "sortorder": "3",
                    "value": "Zabbix alert"
                }
            ]
        },
        {
            "mediatypeid": "13",
            "name": "Script without parameters",
            "parameters": []
        },
        {
            "mediatypeid": "11",
            "name": "Webhook",
            "parameters": [
                {
                    "name": "alert_message",
                    "value": "{ALERT.MESSAGE}"
                },
                {
                    "name": "event_update_message",
                    "value": "{EVENT.UPDATE.MESSAGE}"
                },
                {
                    "name": "host_name",
                    "value": "{HOST.NAME}"
                },
                {
                    "name": "trigger_description",
                    "value": "{TRIGGER.DESCRIPTION}"
                },
                {
                    "name": "trigger_id",
                    "value": "{TRIGGER.ID}"
                },
                {
                    "name": "alert_source",
                    "value": "Zabbix"
                }
            ]
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-621ff52f})

[comment]: # ({new-039ccba1})
### Смотрите также

-   [Пользователь](/ru/manual/api/reference/user/object#пользователь)

[comment]: # ({/new-039ccba1})

[comment]: # ({new-b9d01f00})
### Исходный код

CMediaType::get() в
*frontends/php/include/classes/api/services/CMediaType.php*.

[comment]: # ({/new-b9d01f00})
