[comment]: # translation:outdated

[comment]: # ({new-97b0e55a})
# mediatype.update

[comment]: # ({/new-97b0e55a})

[comment]: # ({new-4cbfe789})
### Описание

`объект mediatype.update(объект/массив СпособыОповещения)`

Этот метод позволяет обновлять существующие способы оповещения

[comment]: # ({/new-4cbfe789})

[comment]: # ({new-7db8f4bd})
### Параметры

`(объект/массив)` [Свойства способов
оповещений](object#способ_оповещения), которые будут обновлены.

Свойство `mediatypeid` должно быть указано по каждому способу
оповещения, все остальные свойства опциональны. Будут обновлены только
переданные свойства, все остальные останутся неизменными.

[comment]: # ({/new-7db8f4bd})

[comment]: # ({new-665d000d})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID обновленных способов
оповещений под свойством `mediatypeids`.

[comment]: # ({/new-665d000d})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-8d3ba0cc})
#### Активация способа оповещения

Активация способа оповещения, то есть, изменение состояния на равное 0.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "mediatype.update",
    "params": {
        "mediatypeid": "6",
        "status": 0
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "mediatypeids": [
            "6"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-8d3ba0cc})

[comment]: # ({new-b9cdc31e})
### Исходный код

CMediaType::update() в
*frontends/php/include/classes/api/services/CMediaType.php*.

[comment]: # ({/new-b9cdc31e})
