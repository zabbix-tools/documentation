[comment]: # translation:outdated

[comment]: # ({new-03fad7e1})
# mediatype.delete

[comment]: # ({/new-03fad7e1})

[comment]: # ({new-3ee75870})
### Описание

`объект mediatype.delete(массив mediaTypeIds)`

Этот метод позволяет удалять способы оповещения.

[comment]: # ({/new-3ee75870})

[comment]: # ({new-854e3ea2})
### Параметры

`(массив)` ID удаляемых способов оповещения.

[comment]: # ({/new-854e3ea2})

[comment]: # ({new-ade2598d})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID удаленных способов
оповещения под свойством `mediatypeids`.

[comment]: # ({/new-ade2598d})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-09d9ff4a})
#### Удаление нескольких способов оповещения

Удаление двух способов оповещения.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "mediatype.delete",
    "params": [
        "3",
        "5"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "mediatypeids": [
            "3",
            "5"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-09d9ff4a})

[comment]: # ({new-68bd428e})
### Исходный код

CMediaType::delete() в
*frontends/php/include/classes/api/services/CMediaType.php*.

[comment]: # ({/new-68bd428e})
