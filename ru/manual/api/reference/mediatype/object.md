[comment]: # translation:outdated

[comment]: # ({new-2fbbc9a6})
# > Объект способа оповещения

Следующие объекты напрямую связаны с `mediatype` API.

[comment]: # ({/new-2fbbc9a6})

[comment]: # ({new-4630c87a})
### Способ оповещения

Объект способа оповещения имеет следующие свойства.

|Свойство|Тип|Описание|
|----------------|------|----------------|
|mediatypeid|строка|*(только чтение)* ID способа оповещения.|
|**description**<br>(требуется)|строка|Имя способа оповещения.|
|**type**<br>(требуется)|целое число|Транспорт используемый способом оповещения.<br><br>Возможные значения:<br>0 - e-mail;<br>1 - скрипт;<br>2 - SMS;<br>3 - Jabber;<br>100 - Ez Texting.|
|exec\_path|строка|При способах оповещений при помощи скрипта свойство `exec_path` содержит имя выполняемого скрипта.<br><br>При Ez Texting свойство `exec_path` содержит ограничение текстового сообщения.<br>Возможные значения ограничения текста:<br>0 - США (160 символов);<br>1 - Канада (136 символов).<br><br>Требуется при способах оповещений с типами скрипт и Ez Texting.|
|gsm\_modem|строка|Имя серийного устройства GSM модема.<br><br>Требуется при способе оповещения с типом SMS.|
|passwd|строка|Пароль аутентификации.<br><br>Требуется при способе оповещения с типами Jabber и Ez Texting.|
|smtp\_email|строка|Email адрес с которого будут отправляться оповещения.<br><br>Требуется при способе оповещения с типом email.|
|smtp\_helo|строка|SMTP HELO.<br><br>Требуется при способе оповещения с типом email.|
|smtp\_server|строка|SMTP сервер.<br><br>Требуется при способе оповещения с типом email.|
|status|целое число|Активирован ли способ оповещения.<br><br>Возможные значения:<br>0 - *(по умолчанию)* активирован;<br>1 - деактивирован.|
|username|строка|Имя пользователя или Jabber идентификатор.<br><br>Требуется при способе оповещения с типами Jabber и Ez Texting.|
|exec\_params|строка|Параметры скрипта.<br><br>Каждый параметр заканчивается переводом на новую строку.|
|maxsessions|целое число|Максимальное количество оповещений, которые можно обрабатывать одновременно.<br><br>Возможные значения для SMS:<br>1 - *(по умолчанию)*<br><br>Возможные значения для остальных типов способов оповещений:<br>0-100|
|maxattempts|целое число|Максимальное количество попыток отправки оповещения.<br><br>Возможные значения:<br>1-10<br><br>Значение по умолчанию:<br>3|
|attempt\_interval|строка|Интервал между попытками отправки. Принимает секунды и единицы времени с суффиксом.<br><br>Возможные значения:<br>0-60s<br><br>Значение по умолчанию:<br>10s|

[comment]: # ({/new-4630c87a})



[comment]: # ({new-46e1e59a})
### Webhook parameters

Parameters passed to webhook script when it is called, have the
following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**name**<br>(required)|string|Parameter name.|
|value|string|Parameter value, support macros. Supported macros described on [page](/manual/appendix/macros/supported_by_location).|

[comment]: # ({/new-46e1e59a})

[comment]: # ({new-parameters})
### Script parameters

Parameters passed to a script when it is being called have the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|**sortorder**<br>(required)|integer|The order in which the parameters will be passed to the script as command-line arguments. Starting with 0 as the first one.|
|value|string|Parameter value, supports macros.<br>Supported macros are described on the [Supported macros](/manual/appendix/macros/supported_by_location) page.|

[comment]: # ({/new-parameters})

[comment]: # ({new-9c81491f})
### Message template

The message template object defines a template that will be used as a
default message for action operations to send a notification. It has the
following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**eventsource**<br>(required)|integer|Event source.<br><br>Possible values:<br>0 - triggers;<br>1 - discovery;<br>2 - autoregistration;<br>3 - internal;<br>4 - services.|
|**recovery**<br>(required)|integer|Operation mode.<br><br>Possible values:<br>0 - operations;<br>1 - recovery operations;<br>2 - update operations.|
|subject|string|Message subject.|
|message|string|Message text.|

[comment]: # ({/new-9c81491f})
