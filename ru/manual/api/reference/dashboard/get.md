[comment]: # translation:outdated

[comment]: # ({new-f7884974})
# dashboard.get

[comment]: # ({/new-f7884974})

[comment]: # ({new-8d36c9b5})
### Описание

`целое число/массив dashboard.get(объект параметры)`

Этот метод позволяет получать панели в соответствии с заданными
параметрами.

[comment]: # ({/new-8d36c9b5})

[comment]: # ({new-342bbd26})
### Параметры

`(объект)` Параметры задают желаемый вывод.

Этот метод поддерживает следующие параметры.

|Параметр|Тип|Описание|
|----------------|------|----------------|
|dashboardids|строка/массив|Возврат панелей только с заданными ID.|
|selectWidgets|запрос|Возврат виджетов панели, которые используются на панели, в свойстве `widgets`.|
|selectUsers|запрос|Возврат пользователей, которые имеют общий доступ к панели, в свойстве `users`.|
|selectUserGroups|запрос|Возврат групп пользователей, которые имеют общий доступ к панели, в свойстве `userGroups`.|
|sortfield|строка/массив|Сортировка результата в соответствии с заданными свойствами.<br><br>Возможные значения: `dashboardid`.|
|countOutput|логический|Эти параметры являются общими для всех методов `get` и они описаны в [справочных комментариях](/ru/manual/api/reference_commentary#общие_параметры_get_метода).|
|editable|логический|^|
|excludeSearch|логический|^|
|filter|объект|^|
|limit|целое число|^|
|output|запрос|^|
|preservekeys|логический|^|
|search|объект|^|
|searchByAny|логический|^|
|searchWildcardsEnabled|логический|^|
|sortorder|строка/массив|^|
|startSearch|логический|^|

[comment]: # ({/new-342bbd26})

[comment]: # ({new-7223bab1})
### Возвращаемые значения

`(целое число/массив)` Возвращает либо:

-   массив объектов;
-   количество найденных объектов, если используется параметр
    `countOutput`.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Пример

[comment]: # ({/new-b41637d2})

[comment]: # ({new-84a10cd6})
#### Получение панели по ID

Получение всех данных о панелях "1" и "2".

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "dashboard.get",
    "params": {
        "output": "extend",
        "selectWidgets": "extend",
        "selectUsers": "extend",
        "selectUserGroups": "extend",
        "dashboardids": [
            "1",
            "2"
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "dashboardid": "1",
            "name": "Dashboard",
            "userid": "1",
            "private": "0",
            "users": [],
            "userGroups": [],
            "widgets": [
                {
                    "widgetid": "9",
                    "type": "systeminfo",
                    "name": "",
                    "x": "6",
                    "y": "8",
                    "width": "6",
                    "height": "5",
                    "fields": []
                },
                {
                    "widgetid": "8",
                    "type": "problemsbysv",
                    "name": "",
                    "x": "6",
                    "y": "4",
                    "width": "6",
                    "height": "4",
                    "fields": []
                },
                {
                    "widgetid": "7",
                    "type": "problemhosts",
                    "name": "",
                    "x": "6",
                    "y": "0",
                    "width": "6",
                    "height": "4",
                    "fields": []
                },
                {
                    "widgetid": "6",
                    "type": "discovery",
                    "name": "",
                    "x": "3",
                    "y": "9",
                    "width": "3",
                    "height": "4",
                    "fields": []
                },
                {
                    "widgetid": "5",
                    "type": "web",
                    "name": "",
                    "x": "0",
                    "y": "9",
                    "width": "3",
                    "height": "4",
                    "fields": []
                },
                {
                    "widgetid": "4",
                    "type": "problems",
                    "name": "",
                    "x": "0",
                    "y": "3",
                    "width": "6",
                    "height": "6",
                    "fields": []
                },
                {
                    "widgetid": "3",
                    "type": "favmaps",
                    "name": "",
                    "x": "4",
                    "y": "0",
                    "width": "2",
                    "height": "3",
                    "fields": []
                },
                {
                    "widgetid": "2",
                    "type": "favscreens",
                    "name": "",
                    "x": "2",
                    "y": "0",
                    "width": "2",
                    "height": "3",
                    "fields": []
                },
                {
                    "widgetid": "1",
                    "type": "favgraphs",
                    "name": "",
                    "x": "0",
                    "y": "0",
                    "width": "2",
                    "height": "3",
                    "fields": []
                }
            ]
        },
        {
            "dashboardid": "2",
            "name": "My dashboard",
            "userid": "1",
            "private": "1",
            "users": [
                {
                    "userid": "4",
                    "permission": "3"
                }
            ],
            "userGroups": [
                {
                    "usrgrpid": "7",
                    "permission": "2"
                }
            ],
            "widgets": [
                {
                    "widgetid": "10",
                    "type": "problems",
                    "name": "",
                    "x": "0",
                    "y": "0",
                    "width": "6",
                    "height": "5",
                    "fields": [
                        {
                            "type": "2",
                            "name": "groupids",
                            "value": "4"
                        }
                    ]
                }
            ]
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-84a10cd6})

[comment]: # ({new-299ae089})
### Смотрите также

-   [Виджет панели](object#виджет_панели)
-   [Поле виджета панели](object#поле_виджета_панели)
-   [Пользователь панели](object#пользователь_панели)
-   [Группа пользователей панели](object#группа_пользователей_панели)

[comment]: # ({/new-299ae089})

[comment]: # ({new-75a9042e})
### Исходный код

CDashboard::get() в
*frontends/php/include/classes/api/services/CDashboard.php*.

[comment]: # ({/new-75a9042e})
