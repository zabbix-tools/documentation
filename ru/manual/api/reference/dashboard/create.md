[comment]: # translation:outdated

[comment]: # ({new-c179cb27})
# dashboard.create

[comment]: # ({/new-c179cb27})

[comment]: # ({new-00970602})
### Описание

`объект dashboard.create(объект/массив панелей)`

Этот метод позволяет создавать новые панели.

[comment]: # ({/new-00970602})

[comment]: # ({new-df2301b4})
### Параметры

`(объект/массив)` Создаваемые панели.

В дополнение к [стандартным свойствам панели](object#панель), этот метод
принимает следующие параметры.

|Параметр|Тип|Описание|
|----------------|------|----------------|
|widgets|массив|Создаваемые [виджеты панели](object#виджет_панели) для панели.|
|users|массив|Создаваемый общий доступ [пользователю панели](object#пользователь_панели) для панели.|
|userGroups|массив|Создаваемый общий доступ [группе пользователей панели](object#группа_пользователей_панели) для панели.|

[comment]: # ({/new-df2301b4})

[comment]: # ({new-f460a18e})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID созданных панелей под
свойством `dashboardids`. Порядок возвращаемых ID совпадает с порядком
переданных панелей.

[comment]: # ({/new-f460a18e})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-b33dc2ee})
#### Создание панели

Создание панели с именем "Моя панель" с одним виджетом Проблемы с тегами
и с использованием двух типов общего доступа (группа пользователей и
пользователь).

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "dashboard.create",
    "params": {
        "name": "Моя панель",
        "widgets": [
            {
                "type": "problems",
                "x": 0,
                "y": 0,
                "width": 6,
                "height": 5,
                "fields": [
                    {
                        "type": 1,
                        "name": "tags.tag.0",
                        "value": "service"
                    },
                    {
                        "type": 1,
                        "name": "tags.value.0",
                        "value": "zabbix_server"
                    }
                ]
            }
        ],
        "userGroups": [
            {
                "usrgrpid": "7",
                "permission": "2"
            }
        ],
        "users": [
            {
                "userid": "4",
                "permission": "3"
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "dashboardids": [
            "2"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-b33dc2ee})

[comment]: # ({new-299ae089})
### Смотрите также

-   [Виджет панели](object#виджет_панели)
-   [Поле виджета панели](object#поле_виджета_панели)
-   [Пользователь панели](object#пользователь_панели)
-   [Группа пользователей панели](object#группа_пользователей_панели)

[comment]: # ({/new-299ae089})

[comment]: # ({new-7d488f32})
### Исходный код

CDashboard::create() в
*frontends/php/include/classes/api/services/CDashboard.php*.

[comment]: # ({/new-7d488f32})
