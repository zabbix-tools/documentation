<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="ru" datatype="plaintext" original="manual/api/reference/dashboard/widget_fields/action_log.md">
    <body>
      <trans-unit id="e5b69436" xml:space="preserve">
        <source># 1 Action log</source>
      </trans-unit>
      <trans-unit id="7950f8a2" xml:space="preserve">
        <source>### Description

These parameters and the possible property values for the respective dashboard widget field objects allow to configure
the [*Action log*](/manual/web_interface/frontend_sections/dashboards/widgets/action_log) widget in `dashboard.create` and `dashboard.update` methods.</source>
      </trans-unit>
      <trans-unit id="74bdffdc" xml:space="preserve">
        <source>### Parameters

The following parameters are supported for the *Action log* widget.

|Parameter|[type](/manual/api/reference/dashboard/object#dashboard-widget-field)|name|value|
|-----|-|-----|-------------------|
|*Refresh interval*|0|rf_rate|0 - No refresh;&lt;br&gt;10 - 10 seconds;&lt;br&gt;30 - 30 seconds;&lt;br&gt;60 - *(default)* 1 minute;&lt;br&gt;120 - 2 minutes;&lt;br&gt;600 - 10 minutes;&lt;br&gt;900 - 15 minutes.|
|*Recipients*|11|userids|[User](/manual/api/reference/user/get) ID.&lt;br&gt;&lt;br&gt;Note: To configure multiple users, create a dashboard widget field object for each user.|
|*Actions*|12|actionids|[Action](/manual/api/reference/action/get) ID.&lt;br&gt;&lt;br&gt;Note: To configure multiple actions, create a dashboard widget field object for each action.|
|*Media types*|13|mediatypeids|[Media type](/manual/api/reference/mediatype/get) ID.&lt;br&gt;&lt;br&gt;Note: To configure multiple media types, create a dashboard widget field object for each media type.|
|*Status*|0|statuses|0 - In progress;&lt;br&gt;1 - Sent/Executed;&lt;br&gt;2 - Failed.&lt;br&gt;&lt;br&gt;Note: To configure multiple values, create a dashboard widget field object for each value.|
|*Search string*|1|message|Any string value.|
|*Sort entries by*|0|sort_triggers|3 - Time (ascending);&lt;br&gt;4 - *(default)* Time (descending);&lt;br&gt;5 - Type (ascending);&lt;br&gt;6 - Type (descending);&lt;br&gt;7 - Status (ascending);&lt;br&gt;8 - Status (descending);&lt;br&gt;11 - Recipient (ascending);&lt;br&gt;12 - Recipient (descending).|
|*Show lines*|0|show_lines|Valid values range from 1-100.&lt;br&gt;&lt;br&gt;Default: 25.|</source>
      </trans-unit>
      <trans-unit id="2dd9270a" xml:space="preserve">
        <source>### Examples

The following examples aim to only describe the configuration of the dashboard widget field objects for the *Action log* widget.
For more information on configuring a dashboard, see [`dashboard.create`](/manual/api/reference/dashboard/create).</source>
      </trans-unit>
      <trans-unit id="bf9fdcfc" xml:space="preserve">
        <source>#### Configuring an *Action log* widget

Configure an *Action log* widget that displays 10 entries of action operation details, sorted by time (in ascending order).
In addition, display details only for those action operations that attempted to send an email to user "1", but were unsuccessful.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "dashboard.create",
    "params": {
        "name": "My dashboard",
        "display_period": 30,
        "auto_start": 1,
        "pages": [
            {
                "widgets": [
                    {
                        "type": "actionlog",
                        "name": "Action log",
                        "x": 0,
                        "y": 0,
                        "width": 12,
                        "height": 5,
                        "view_mode": 0,
                        "fields": [
                            {
                                "type": 0,
                                "name": "show_lines",
                                "value": 10
                            },
                            {
                                "type": 0,
                                "name": "sort_triggers",
                                "value": 3
                            },
                            {
                                "type": 11,
                                "name": "userids",
                                "value": 1
                            },
                            {
                                "type": 13,
                                "name": "mediatypeids",
                                "value": 1
                            },
                            {
                                "type": 0,
                                "name": "statuses",
                                "value": 2
                            }
                        ]
                    }
                ]
            }
        ],
        "userGroups": [
            {
                "usrgrpid": 7,
                "permission": 2
            }
        ],
        "users": [
            {
                "userid": 1,
                "permission": 3
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "dashboardids": [
            "3"
        ]
    },
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="5780d59d" xml:space="preserve">
        <source>### See also

-   [Dashboard widget field](/manual/api/reference/dashboard/object#dashboard-widget-field)
-   [`dashboard.create`](/manual/api/reference/dashboard/create)
-   [`dashboard.update`](/manual/api/reference/dashboard/update)</source>
      </trans-unit>
    </body>
  </file>
</xliff>
