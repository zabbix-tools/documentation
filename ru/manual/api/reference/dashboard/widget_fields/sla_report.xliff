<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="ru" datatype="plaintext" original="manual/api/reference/dashboard/widget_fields/sla_report.md">
    <body>
      <trans-unit id="6acd087d" xml:space="preserve">
        <source># 19 SLA report</source>
      </trans-unit>
      <trans-unit id="b112d173" xml:space="preserve">
        <source>### Description

These parameters and the possible property values for the respective dashboard widget field objects allow to configure
the [*SLA report*](/manual/web_interface/frontend_sections/dashboards/widgets/sla_report) widget in `dashboard.create` and `dashboard.update` methods.</source>
      </trans-unit>
      <trans-unit id="b48fed5a" xml:space="preserve">
        <source>### Parameters

The following parameters are supported for the *SLA report* widget.

|Parameter|[type](/manual/api/reference/dashboard/object#dashboard-widget-field)|name|value|
|-----|-|-----|-------------------|
|*Refresh interval*|0|rf_rate|0 - *(default)* No refresh;&lt;br&gt;10 - 10 seconds;&lt;br&gt;30 - 30 seconds;&lt;br&gt;60 - 1 minute;&lt;br&gt;120 - 2 minutes;&lt;br&gt;600 - 10 minutes;&lt;br&gt;900 - 15 minutes.|
|*SLA*|10|slaid|[SLA](/manual/api/reference/sla/get) ID.&lt;br&gt;&lt;br&gt;[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):&lt;br&gt;- *required*|
|*Service*|9|serviceid|[Service](/manual/api/reference/service/get) ID.|
|*Show periods*|0|show_periods|Valid values range from 1-100.&lt;br&gt;&lt;br&gt;Default: 20.|
|*From*|1|date_from|Valid date string in format `YYYY-MM-DD`.&lt;br&gt;[Relative dates](/manual/config/visualization/graphs/simple#time-period-selector) with modifiers `d`, `w`, `M`, `y` (e.g. `now`, `now/d`, `now/w-1w`, etc.) are supported.|
|*To*|1|date_to|Valid date string in format `YYYY-MM-DD`.&lt;br&gt;[Relative dates](/manual/config/visualization/graphs/simple#time-period-selector) with modifiers `d`, `w`, `M`, `y` (e.g. `now`, `now/d`, `now/w-1w`, etc.) are supported.|</source>
      </trans-unit>
      <trans-unit id="50899945" xml:space="preserve">
        <source>### Examples

The following examples aim to only describe the configuration of the dashboard widget field objects for the *SLA report* widget.
For more information on configuring a dashboard, see [`dashboard.create`](/manual/api/reference/dashboard/create).</source>
      </trans-unit>
      <trans-unit id="d501029a" xml:space="preserve">
        <source>#### Configuring an *SLA report* widget

Configure an *SLA report* widget that displays the SLA report for SLA "4" service "2" for the period of last 30 days. 

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "dashboard.create",
    "params": {
        "name": "My dashboard",
        "display_period": 30,
        "auto_start": 1,
        "pages": [
            {
                "widgets": [
                    {
                        "type": "slareport",
                        "name": "SLA report",
                        "x": 0,
                        "y": 0,
                        "width": 12,
                        "height": 5,
                        "view_mode": 0,
                        "fields": [
                            {
                                "type": 10,
                                "name": "slaid",
                                "value": 4
                            },
                            {
                                "type": 9,
                                "name": "serviceid",
                                "value": 2
                            },
                            {
                                "type": 1,
                                "name": "date_from",
                                "value": "now-30d"
                            },
                            {
                                "type": 1,
                                "name": "date_to",
                                "value": "now"
                            }
                        ]
                    }
                ]
            }
        ],
        "userGroups": [
            {
                "usrgrpid": 7,
                "permission": 2
            }
        ],
        "users": [
            {
                "userid": 1,
                "permission": 3
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "dashboardids": [
            "3"
        ]
    },
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="ccbde9ad" xml:space="preserve">
        <source>### See also

-   [Dashboard widget field](/manual/api/reference/dashboard/object#dashboard-widget-field)
-   [`dashboard.create`](/manual/api/reference/dashboard/create)
-   [`dashboard.update`](/manual/api/reference/dashboard/update)</source>
      </trans-unit>
    </body>
  </file>
</xliff>
