[comment]: # translation:outdated

[comment]: # ({new-faff6037})
# dashboard.delete

[comment]: # ({/new-faff6037})

[comment]: # ({new-6f29484b})
### Описание

`объект dashboard.delete(массив dashboardids)`

Этот метод позволяет удалять панели.

[comment]: # ({/new-6f29484b})

[comment]: # ({new-4fd471a4})
### Параметры

`(массив)` ID удаляемых панелей.

[comment]: # ({/new-4fd471a4})

[comment]: # ({new-c04394d8})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID удаленных панелей под
свойством `dashboardids`.

[comment]: # ({/new-c04394d8})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-4d5d4a5d})
#### Удаление нескольких панелей

Удаление двух панелей.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "dashboard.delete",
    "params": [
        "2",
        "3"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "dashboardids": [
            "2",
            "3"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-4d5d4a5d})

[comment]: # ({new-2bc6b04b})
### Исходный код

CDashboard::delete() в
*frontends/php/include/classes/api/services/CDashboard.php*.

[comment]: # ({/new-2bc6b04b})
