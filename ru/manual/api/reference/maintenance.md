[comment]: # translation:outdated

[comment]: # ({new-81ae720b})
# Обслуживание

Этот класс предназначен для работы с обслуживаниями.

Справка по объектам:\

-   [Обслуживание](/ru/manual/api/reference/maintenance/object#обслуживание)
-   [Период
    времени](/ru/manual/api/reference/maintenance/object#период_времени)

Доступные методы:\

-   [maintenance.create](/ru/manual/api/reference/maintenance/create) -
    создание новых обслуживаний
-   [maintenance.delete](/ru/manual/api/reference/maintenance/delete) -
    удаление обслуживаний
-   [maintenance.get](/ru/manual/api/reference/maintenance/get) -
    получение обслуживаний
-   [maintenance.update](/ru/manual/api/reference/maintenance/update) -
    обновление обслуживаний

[comment]: # ({/new-81ae720b})
