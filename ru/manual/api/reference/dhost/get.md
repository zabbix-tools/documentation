[comment]: # translation:outdated

[comment]: # ({new-37c3aad7})
# dhost.get

[comment]: # ({/new-37c3aad7})

[comment]: # ({new-1309d6bb})
### Описание

`целое число/массив dhost.get(объект параметры)`

Этот метод позволяет получать обнаруженные узлы сети в соответствии с
заданными параметрами.

[comment]: # ({/new-1309d6bb})

[comment]: # ({new-061d7cf0})
### Параметры

`(объект)` Параметры задают желаемый вывод.

Этот метод поддерживает следующие параметры.

|Параметр|Тип|Описание|
|----------------|------|----------------|
|dhostids|строка/массив|Возврат обнаруженных узлов сети только с заданными ID.|
|druleids|строка/массив|Возврат только тех обнаруженных узлов сети, которые были созданы заданными правилами обнаружения.|
|dserviceids|строка/массив|Возврат только тех обнаруженных узлов сети, на которых работают заданные сервисы.|
|selectDRules|запрос|Возврат правила обнаружения, которое нашло узел сети, в виде массива в свойстве `drules`.|
|selectDServices|запрос|Возврат обнаруженных сервисов, которые работают на узле сети, в свойстве `dservices`.<br><br>Поддерживает `count`.|
|limitSelects|целое число|Ограничение количества записей, возвращаемых подзапросами.<br><br>Применимо только к следующим подзапросам:<br>`selectDServices` - все результаты сортируются по `dserviceid`.|
|sortfield|строка/массив|Сортировка результата в соответствии с заданными свойствами.<br><br>Возможные значения: `dhostid` и `druleid`.|
|countOutput|логический|Эти параметры являются общими для всех методов `get` и они описаны в [справочных комментариях](/ru/manual/api/reference_commentary#общие_параметры_get_метода).|
|editable|логический|^|
|excludeSearch|логический|^|
|filter|объект|^|
|limit|целое число|^|
|output|запрос|^|
|preservekeys|логический|^|
|search|объект|^|
|searchByAny|логический|^|
|searchWildcardsEnabled|логический|^|
|sortorder|строка/массив|^|
|startSearch|логический|^|

[comment]: # ({/new-061d7cf0})

[comment]: # ({new-7223bab1})
### Возвращаемые значения

`(целое число/массив)` Возвращает либо:

-   массив объектов;
-   количество найденных объектов, если используется параметр
    `countOutput`.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-4b9c1e99})
#### Получение обнаруженных узлов сети по правилу обнаружения

Получение всех узлов сети и обнаруженных работающих сервисов, которые
были найдены при помощи правила обнаружения "4".

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "dhost.get",
    "params": {
        "output": "extend",
        "selectDServices": "extend",
        "druleids": "4"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "dservices": [
                {
                    "dserviceid": "1",
                    "dhostid": "1",
                    "type": "4",
                    "key_": "",
                    "value": "",
                    "port": "80",
                    "status": "0",
                    "lastup": "1337697227",
                    "lastdown": "0",
                    "dcheckid": "5",
                    "ip": "192.168.1.1",
                    "dns": "station.company.lan"
                }
            ],
            "dhostid": "1",
            "druleid": "4",
            "status": "0",
            "lastup": "1337697227",
            "lastdown": "0"
        },
        {
            "dservices": [
                {
                    "dserviceid": "2",
                    "dhostid": "2",
                    "type": "4",
                    "key_": "",
                    "value": "",
                    "port": "80",
                    "status": "0",
                    "lastup": "1337697234",
                    "lastdown": "0",
                    "dcheckid": "5",
                    "ip": "192.168.1.4",
                    "dns": "john.company.lan"
                }
            ],
            "dhostid": "2",
            "druleid": "4",
            "status": "0",
            "lastup": "1337697234",
            "lastdown": "0"
        },
        {
            "dservices": [
                {
                    "dserviceid": "3",
                    "dhostid": "3",
                    "type": "4",
                    "key_": "",
                    "value": "",
                    "port": "80",
                    "status": "0",
                    "lastup": "1337697234",
                    "lastdown": "0",
                    "dcheckid": "5",
                    "ip": "192.168.1.26",
                    "dns": "printer.company.lan"
                }
            ],
            "dhostid": "3",
            "druleid": "4",
            "status": "0",
            "lastup": "1337697234",
            "lastdown": "0"
        },
        {
            "dservices": [
                {
                    "dserviceid": "4",
                    "dhostid": "4",
                    "type": "4",
                    "key_": "",
                    "value": "",
                    "port": "80",
                    "status": "0",
                    "lastup": "1337697234",
                    "lastdown": "0",
                    "dcheckid": "5",
                    "ip": "192.168.1.7",
                    "dns": "mail.company.lan"
                }
            ],
            "dhostid": "4",
            "druleid": "4",
            "status": "0",
            "lastup": "1337697234",
            "lastdown": "0"
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-4b9c1e99})

[comment]: # ({new-e6b09247})
### Смотрите также

-   [Обнаруженный
    сервис](/ru/manual/api/reference/dservice/object#обнаруженный_сервис)
-   [Правило
    обнаружения](/ru/manual/api/reference/drule/object#правило_обнаружения)

[comment]: # ({/new-e6b09247})

[comment]: # ({new-8dc0c01a})
### Исходный код

CDHost::get() в *frontends/php/include/classes/api/services/CDHost.php*.

[comment]: # ({/new-8dc0c01a})
