[comment]: # translation:outdated

[comment]: # ({new-8674a4c3})
# Правило обнаружения

Этот класс предназначен для работы с правилами обнаружения сетей.

::: notetip
Этот API предназначен для работы с правилами обнаружения
сетей. Для правил низкоуровневого обнаружения смотрите [API правила
LLD](discoveryrule).
:::

Справка по объектам:\

-   [Правило
    обнаружения](/ru/manual/api/reference/drule/object#правило_обнаружения)

Доступные методы:\

-   [drule.create](/ru/manual/api/reference/drule/create) - создание
    новых правил обнаружение
-   [drule.delete](/ru/manual/api/reference/drule/delete) - удаление
    правил обнаружения
-   [drule.get](/ru/manual/api/reference/drule/get) - получение правила
    обнаружения
-   [drule.update](/ru/manual/api/reference/drule/update) - обновление
    правил обнаружения

[comment]: # ({/new-8674a4c3})
