[comment]: # translation:outdated

[comment]: # ({new-09ff1eef})
# Преобразование значений

Этот класс предназначен для работы с преобразованиями значений.

Справка по объектам:\

-   [Преобразование
    значений](/ru/manual/api/reference/valuemap/object#преобразование_значений)

Доступные методы:\

-   [valuemap.create](/ru/manual/api/reference/valuemap/create) -
    создание новых преобразований значений
-   [valuemap.delete](/ru/manual/api/reference/valuemap/delete) -
    удаление преобразований значений
-   [valuemap.get](/ru/manual/api/reference/valuemap/get) - получение
    преобразований значений
-   [valuemap.update](/ru/manual/api/reference/valuemap/update) -
    обновление преобразований значений

[comment]: # ({/new-09ff1eef})
