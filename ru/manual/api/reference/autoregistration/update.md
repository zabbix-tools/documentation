[comment]: # translation:outdated

[comment]: # ({new-38f2889f})
# autoregistration.update

[comment]: # ({/new-38f2889f})

[comment]: # ({new-13c79ed6})
### Description

`object autoregistration.update(object autoregistration)`

This method allows to update existing autoregistration.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-13c79ed6})

[comment]: # ({new-dc051772})
### Parameters

`(object)` Autoregistration properties to be updated.

[comment]: # ({/new-dc051772})

[comment]: # ({new-4bb135e8})
### Return values

`(boolean )` Returns boolean true as result on successful update.

[comment]: # ({/new-4bb135e8})

[comment]: # ({new-d84ea91e})
### Examples

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "autoregistration.update",
    "params": {
        "tls_accept": "3",
        "tls_psk_identity": "PSK 001",
        "tls_psk": "11111595725ac58dd977beef14b97461a7c1045b9a1c923453302c5473193478"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": true,
    "id": 1
}
```

[comment]: # ({/new-d84ea91e})

[comment]: # ({new-0e014313})
### Source

CAutoregistration::update() in
*ui/include/classes/api/services/CAutoregistration.php*.

[comment]: # ({/new-0e014313})
