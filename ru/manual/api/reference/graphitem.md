[comment]: # translation:outdated

[comment]: # ({new-ba6b9d7f})
# Элемент графика

Этот класс предназначен для работы с элементами графика.

Справка по объектам:\

-   [Элемент
    графика](/ru/manual/api/reference/graphitem/object#элемент_графика)

Доступные методы:\

-   [graphitem.get](/ru/manual/api/reference/graphitem/get) - получение
    элементов графика

[comment]: # ({/new-ba6b9d7f})
