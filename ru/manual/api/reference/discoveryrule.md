[comment]: # translation:outdated

[comment]: # ({new-2593ef3e})
# Правило LLD

Этот класс предназначен для работы с правилами низкоуровневого
обнаружения.

Справка по объектам:\

-   [Правило
    LLD](/ru/manual/api/reference/discoveryrule/object#правило_lld)

Доступные методы:\

-   [discoveryrule.copy](/ru/manual/api/reference/discoveryrule/copy) -
    копирование правил LLD
-   [discoveryrule.create](/ru/manual/api/reference/discoveryrule/create) -
    создание новых правил LLD
-   [discoveryrule.delete](/ru/manual/api/reference/discoveryrule/delete) -
    удаление правил LLD
-   [discoveryrule.get](/ru/manual/api/reference/discoveryrule/get) -
    получение правил LLD
-   [discoveryrule.update](/ru/manual/api/reference/discoveryrule/update) -
    обновление правил LLD

[comment]: # ({/new-2593ef3e})
