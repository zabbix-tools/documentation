[comment]: # translation:outdated

[comment]: # ({new-4312b82f})
# Событие

Этот класс предназначен для работы с событиями.

Справка по объектам:\

-   [Событие](/ru/manual/api/reference/event/object#узел_сети)

Доступные методы:\

-   [event.get](/ru/manual/api/reference/event/get) - получение событий
-   [event.acknowledge](/ru/manual/api/reference/event/acknowledge) -
    подтверждение событий

[comment]: # ({/new-4312b82f})
