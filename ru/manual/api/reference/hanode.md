[comment]: # translation:outdated

[comment]: # ({new-bd841621})
# High availability node

This class is designed to work with server nodes that are part of a High
availability cluster, or a standalone server instance.

Object references:\

-   [High availability node](/manual/api/reference/hanode/object#hanode)

Available methods:\

-   [hanode.get](/manual/api/reference/hanode/get) - retrieving nodes

[comment]: # ({/new-bd841621})
