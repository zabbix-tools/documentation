[comment]: # translation:outdated

[comment]: # ({new-2d53bfd4})
# Прототип триггеров

Этот класс предназначен для работы с прототипами триггеров.

Справка по объектам:\

-   [Прототип
    триггеров](/ru/manual/api/reference/triggerprototype/object#прототип_триггеров)

Доступные методы:\

-   [triggerprototype.create](/ru/manual/api/reference/triggerprototype/create) -
    создание новых прототипов триггеров
-   [triggerprototype.delete](/ru/manual/api/reference/triggerprototype/delete) -
    удаление прототипов триггеров
-   [triggerprototype.get](/ru/manual/api/reference/triggerprototype/get) -
    получение прототипов триггеров
-   [triggerprototype.update](/ru/manual/api/reference/triggerprototype/update) -
    обновление прототипов триггеров

[comment]: # ({/new-2d53bfd4})
