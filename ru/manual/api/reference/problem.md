[comment]: # translation:outdated

[comment]: # ({new-4fff4611})
# Проблема

Этот класс предназначен для работы с проблемами.

Справка по объектам:\

-   [Проблема](/ru/manual/api/reference/problem/object#проблема)

Доступные методы:\

-   [problem.get](/ru/manual/api/reference/problem/get) - получение
    проблем

[comment]: # ({/new-4fff4611})
