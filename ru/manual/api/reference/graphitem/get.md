[comment]: # translation:outdated

[comment]: # ({new-43e64f56})
# graphitem.get

[comment]: # ({/new-43e64f56})

[comment]: # ({new-fe22ea70})
### Описание

`целое число/массив graphitem.get(объект параметры)`

Этот метод позволяет получать элементы графика в соответствии с
заданными параметрами.

[comment]: # ({/new-fe22ea70})

[comment]: # ({new-d30698ba})
### Параметры

`(объект)` Параметры задают желаемый вывод.

Этот метод поддерживает следующие параметры.

|Параметр|Тип|Описание|
|----------------|------|----------------|
|gitemids|строка/массив|Возврат элементов графика только с заданными ID.|
|graphids|строка/массив|Возврат только тех элементов графика, которые принадлежат заданным графикам.|
|itemids|строка/массив|Возврат только тех элементов графика, которые принадлежат заданным ID элементов данных.|
|type|целое число|Возврат элементов графика только с заданным типом.<br><br>Обратитесь к [странице объекта элемента графика](object#элемент_графика) для получения списка поддерживаемых типов элементов графика.|
|selectGraphs|запрос|Возврат графика, которому принадлежит элемент графика, в виде массива в свойстве `graphs`.|
|sortfield|строка/массив|Сортировка результата в соответствии с заданными свойствами.<br><br>Возможные значения: `gitemid`.|
|countOutput|логический|Эти параметры являются общими для всех методов `get` и они описаны в [справочных комментариях](/ru/manual/api/reference_commentary#общие_параметры_get_метода).|
|editable|логический|^|
|limit|целое число|^|
|output|запрос|^|
|preservekeys|логический|^|
|sortorder|строка/массив|^|

[comment]: # ({/new-d30698ba})

[comment]: # ({new-7223bab1})
### Возвращаемые значения

`(целое число/массив)` Возвращает либо:

-   массив объектов;
-   количество найденных объектов, если используется параметр
    `countOutput`.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-37da5f20})
#### Получение элементов графика из графика

Получение всех элементов графика, которые используются в графике, с
добавлением дополнительной информации о элементе данных и узле сети.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "graphitem.get",
    "params": {
        "output": "extend",
        "expandData": 1,
        "graphids": "387"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "gitemid": "1242",
            "graphid": "387",
            "itemid": "22665",
            "drawtype": "1",
            "sortorder": "1",
            "color": "FF5555",
            "yaxisside": "0",
            "calc_fnc": "2",
            "type": "0",
            "key_": "system.cpu.util[,steal]",
            "hostid": "10001",
            "flags": "0",
            "host": "Template OS Linux"
        },
        {
            "gitemid": "1243",
            "graphid": "387",
            "itemid": "22668",
            "drawtype": "1",
            "sortorder": "2",
            "color": "55FF55",
            "yaxisside": "0",
            "calc_fnc": "2",
            "type": "0",
            "key_": "system.cpu.util[,softirq]",
            "hostid": "10001",
            "flags": "0",
            "host": "Template OS Linux"
        },
        {
            "gitemid": "1244",
            "graphid": "387",
            "itemid": "22671",
            "drawtype": "1",
            "sortorder": "3",
            "color": "009999",
            "yaxisside": "0",
            "calc_fnc": "2",
            "type": "0",
            "key_": "system.cpu.util[,interrupt]",
            "hostid": "10001",
            "flags": "0",
            "host": "Template OS Linux"
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-37da5f20})

[comment]: # ({new-09a26e71})
### Смотрите также

-   [График](/ru/manual/api/reference/graph/object#график)

[comment]: # ({/new-09a26e71})

[comment]: # ({new-8d13cada})
### Исходный код

CGraphItem::get() в *ui/include/classes/api/services/CGraphItem.php*.

[comment]: # ({/new-8d13cada})
