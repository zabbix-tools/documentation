[comment]: # translation:outdated

[comment]: # ({new-b1a4e412})
# Корреляция

Этот класс предназначен для работы с корреляциями.

Справка по объектам:\

-   [Корреляция](/ru/manual/api/reference/correlation/object#корреляция)

Доступные методы:\

-   [correlation.create](/ru/manual/api/reference/correlation/create) -
    создание новых корреляций
-   [correlation.delete](/ru/manual/api/reference/correlation/delete) -
    удаление корреляций
-   [correlation.get](/ru/manual/api/reference/correlation/get) -
    получение корреляций
-   [correlation.update](/ru/manual/api/reference/correlation/update) -
    обновление корреляций

[comment]: # ({/new-b1a4e412})
