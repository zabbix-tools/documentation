[comment]: # translation:outdated

[comment]: # ({new-ddce888e})
# discoveryrule.get

[comment]: # ({/new-ddce888e})

[comment]: # ({new-b87cd1ee})
### Описание

`целое число/массив discoveryrule.get(объект параметры)`

Этот метод позволяет получать правила LLD в соответствии с заданными
параметрами.

[comment]: # ({/new-b87cd1ee})

[comment]: # ({new-57007d3d})
### Параметры

`(объект)` Параметры задают желаемый вывод.

Этот метод поддерживает следующие параметры.

|Параметр|Тип|Описание|
|----------------|------|----------------|
|itemids|строка/массив|Возврат правил LLD только с заданными ID.|
|hostids|строка/массив|Возврат только тех правил LLD, которые принадлежат заданным узлам сети.|
|inherited|логический|Если задано значение `true`, возвращать только те правила LLD, которые унаследованы из шаблона.|
|interfaceids|строка/массив|Возврат только тех правил LLD, которые используют заданные интерфейсы узлов сети.|
|monitored|логический|Если задано значение `true`, возвращать только активированные правила LLD, которые принадлежат узлам сети под наблюдением.|
|templated|логический|Если задано значение `true`, возвращать только те правила LLD, которые принадлежат шаблонам.|
|templateids|строка/массив|Возврат только тех правил LLD, которые принадлежат заданным шаблонам.|
|selectFilter|запрос|Возврат фильтра, который используется правилом LLD, в свойстве `filter`.|
|selectGraphs|запрос|Возврат прототипов графиков, которые принадлежат правилу LLD, в свойстве `graphs`.<br><br>Поддерживается `count`.|
|selectHostPrototypes|запрос|Возврат прототипов узлов сети, которые принадлежат правилу обнаружения, в свойстве `hostPrototypes`.<br><br>Поддерживается `count`.|
|selectHosts|запрос|Возврат узла сети, которому принадлежит правило LLD, в виде массива в свойстве `hosts`.|
|selectItems|запрос|Возврат прототипов элементов данных, которые принадлежат правилу LLD, в свойстве `items`.<br><br>Поддерживается `count`.|
|selectTriggers|запрос|Возврат прототипов триггеров, которые принадлежат правилу обнаружения, в свойстве `triggers`.<br><br>Поддерживается `count`.|
|filter|объект|Возврат только тех результатов, которые в точности соответствуют заданному фильтру.<br><br>Принимает массив, где ключи являются именами свойств и значения, которые являются либо одним значением, либо массивом сопоставляемых значений.<br><br>Поддерживает дополнительные фильтры:<br>`host` - техническое имя узла сети, которому принадлежит правило LLD.|
|limitSelects|целое число|Ограничение количества записей, возвращаемых подзапросами.<br><br>Применимо только к следующим подзапросам:<br>`selctItems`;<br>`selectGraphs`;<br>`selectTriggers`.|
|sortfield|строка/массив|Сортировка результата в соответствии с заданными свойствами.<br><br>Возможные значения: `itemid`, `name`, `key_`, `delay`, `type` и `status`.|
|countOutput|логический|Эти параметры являются общими для всех методов `get` и они описаны в [справочных комментариях](/ru/manual/api/reference_commentary#общие_параметры_get_метода).|
|editable|логический|^|
|excludeSearch|логический|^|
|limit|целое число|^|
|output|запрос|^|
|preservekeys|логический|^|
|search|объект|^|
|searchByAny|логический|^|
|searchWildcardsEnabled|логический|^|
|sortorder|строка/массив|^|
|startSearch|логический|^|

[comment]: # ({/new-57007d3d})

[comment]: # ({new-7223bab1})
### Возвращаемые значения

`(целое число/массив)` Возвращает либо:

-   массив объектов;
-   количество найденных объектов, если используется параметр
    `countOutput`.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-5f8b5429})
#### Получение правил обнаружения с узла сети

Получение всех правил обнаружения с узла сети "10202".

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "discoveryrule.get",
    "params": {
        "output": "extend",
        "hostids": "10202"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "itemid": "27425",
            "type": "0",
            "snmp_community": "",
            "snmp_oid": "",
            "hostid": "10202",
            "name": "Network interface discovery",
            "key_": "net.if.discovery",
            "delay": "1h",
            "state": "0",
            "status": "0",
            "trapper_hosts": "",
            "snmpv3_securityname": "",
            "snmpv3_securitylevel": "0",
            "snmpv3_authpassphrase": "",
            "snmpv3_privpassphrase": "",
            "error": "",
            "templateid": "22444",
            "params": "",
            "ipmi_sensor": "",
            "authtype": "0",
            "username": "",
            "password": "",
            "publickey": "",
            "privatekey": "",
            "interfaceid": "119",
            "port": "",
            "description": "Discovery of network interfaces as defined in global regular expression \"Network interfaces for discovery\".",
            "lifetime": "30d",
            "snmpv3_authprotocol": "0",
            "snmpv3_privprotocol": "0",
            "snmpv3_contextname": "",
            "jmx_endpoint": "",
            "master_itemid": "0",
            "timeout": "3s",
            "url": "",
            "query_fields": [],
            "posts": "",
            "status_codes": "200",
            "follow_redirects": "1",
            "post_type": "0",
            "http_proxy": "",
            "headers": [],
            "retrieve_mode": "0",
            "request_method": "0",
            "ssl_cert_file": "",
            "ssl_key_file": "",
            "ssl_key_password": "",
            "verify_peer": "0",
            "verify_host": "0",
            "allow_traps": "0"
        },
        {
            "itemid": "27426",
            "type": "0",
            "snmp_community": "",
            "snmp_oid": "",
            "hostid": "10202",
            "name": "Mounted filesystem discovery",
            "key_": "vfs.fs.discovery",
            "delay": "1h",
            "state": "0",
            "status": "0",
            "trapper_hosts": "",
            "snmpv3_securityname": "",
            "snmpv3_securitylevel": "0",
            "snmpv3_authpassphrase": "",
            "snmpv3_privpassphrase": "",
            "error": "",
            "templateid": "22450",
            "params": "",
            "ipmi_sensor": "",
            "authtype": "0",
            "username": "",
            "password": "",
            "publickey": "",
            "privatekey": "",
            "interfaceid": "119",
            "port": "",
            "description": "Discovery of file systems of different types as defined in global regular expression \"File systems for discovery\".",
            "lifetime": "30d",
            "snmpv3_authprotocol": "0",
            "snmpv3_privprotocol": "0",
            "snmpv3_contextname": "",
            "jmx_endpoint": "",
            "master_itemid": "0",
            "timeout": "3s",
            "url": "",
            "query_fields": [],
            "posts": "",
            "status_codes": "200",
            "follow_redirects": "1",
            "post_type": "0",
            "http_proxy": "",
            "headers": [],
            "retrieve_mode": "0",
            "request_method": "0",
            "ssl_cert_file": "",
            "ssl_key_file": "",
            "ssl_key_password": "",
            "verify_peer": "0",
            "verify_host": "0",
            "allow_traps": "0"
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-5f8b5429})

[comment]: # ({new-4d0af030})
#### Получение условий фильтрации

Получение имени правила LLD "24681" и условий фильтрации этого правила.
Фильтр использует "и" тип вычисления, таким образом свойство `formula`
пустое и `eval_formula` генерируется автоматически.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "discoveryrule.get",
    "params": {
        "output": [
            "name"
        ],
        "selectFilter": "extend",
        "itemids": ["24681"]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "itemid": "24681",
            "name": "Filtered LLD rule",
            "filter": {
                "evaltype": "1",
                "formula": "",
                "conditions": [
                    {
                        "macro": "{#MACRO1}",
                        "value": "@regex1",
                        "operator": "8",
                        "formulaid": "A"
                    },
                    {
                        "macro": "{#MACRO2}",
                        "value": "@regex2",
                        "operator": "8",
                        "formulaid": "B"
                    },
                    {
                        "macro": "{#MACRO3}",
                        "value": "@regex3",
                        "operator": "8",
                        "formulaid": "C"
                    }
                ],
                "eval_formula": "A and B and C"
            }
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-4d0af030})

[comment]: # ({new-e3de6fe8})
#### Получение LLD правила по URL

Получение LLD правила с узла сети по значению поля URL правила.
Поддерживается только точное соответствие строке URL заданной LLD
правилу.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "discoveryrule.get",
    "params": {
        "hostids": "10257",
        "filter": {
            "type": "19",
            "url": "http://127.0.0.1/discoverer.php"
        }
    },
    "id": 39,
    "auth": "d678e0b85688ce578ff061bd29a20d3b"
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "itemid": "28336",
            "type": "19",
            "snmp_community": "",
            "snmp_oid": "",
            "hostid": "10257",
            "name": "API HTTP agent",
            "key_": "api_discovery_rule",
            "delay": "5s",
            "history": "90d",
            "trends": "0",
            "status": "0",
            "value_type": "4",
            "trapper_hosts": "",
            "units": "",
            "snmpv3_securityname": "",
            "snmpv3_securitylevel": "0",
            "snmpv3_authpassphrase": "",
            "snmpv3_privpassphrase": "",
            "error": "",
            "lastlogsize": "0",
            "logtimefmt": "",
            "templateid": "0",
            "valuemapid": "0",
            "params": "",
            "ipmi_sensor": "",
            "authtype": "0",
            "username": "",
            "password": "",
            "publickey": "",
            "privatekey": "",
            "mtime": "0",
            "flags": "1",
            "interfaceid": "5",
            "port": "",
            "description": "",
            "inventory_link": "0",
            "lifetime": "30d",
            "snmpv3_authprotocol": "0",
            "snmpv3_privprotocol": "0",
            "state": "0",
            "snmpv3_contextname": "",
            "jmx_endpoint": "",
            "master_itemid": "0",
            "timeout": "3s",
            "url": "http://127.0.0.1/discoverer.php",
            "query_fields": [
                {
                    "mode": "json"
                },
                {
                    "elements": "2"
                }
            ],
            "posts": "",
            "status_codes": "200",
            "follow_redirects": "1",
            "post_type": "0",
            "http_proxy": "",
            "headers": {
                "X-Type": "api",
                "Authorization": "Bearer mF_A.B5f-2.1JcM"
            },
            "retrieve_mode": "0",
            "request_method": "1",
            "ssl_cert_file": "",
            "ssl_key_file": "",
            "ssl_key_password": "",
            "verify_peer": "0",
            "verify_host": "0",
            "allow_traps": "0"
        }
    ],
    "id": 39
}
```

[comment]: # ({/new-e3de6fe8})

[comment]: # ({new-c0ae2d21})
### Смотрите также

-   [Прототип
    графиков](/ru/manual/api/reference/graphprototype/object#прототип_графиков)
-   [Узел сети](/ru/manual/api/reference/host/object#узел_сети)
-   [Прототип элементов
    данных](/ru/manual/api/reference/itemprototype/object#прототип_элементов_данных)
-   [Прототип
    графиков](/ru/manual/api/reference/graphprototype/object#прототип_графиков)
-   [Прототип
    триггеров](/ru/manual/api/reference/triggerprototype/object#прототип_триггеров)

[comment]: # ({/new-c0ae2d21})

[comment]: # ({new-f25d02bb})
### Исходный код

CDiscoveryRule::get() в
*frontends/php/include/classes/api/services/CDiscoveryRule.php*.

[comment]: # ({/new-f25d02bb})


[comment]: # ({new-e14efed8})
### Source

CDiscoveryRule::get() in
*ui/include/classes/api/services/CDiscoveryRule.php*.

[comment]: # ({/new-e14efed8})
