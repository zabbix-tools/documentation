[comment]: # translation:outdated

[comment]: # ({new-293a482d})
# discoveryrule.delete

[comment]: # ({/new-293a482d})

[comment]: # ({new-1760a96c})
### Описание

`объект discoveryrule.delete(массив lldRuleIds)`

Этот метод позволяет удалять правила LLD.

[comment]: # ({/new-1760a96c})

[comment]: # ({new-27340eaa})
### Параметры

`(массив)` ID удаляемых правил LLD.

[comment]: # ({/new-27340eaa})

[comment]: # ({new-ae1ac187})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID удаленных правил LLD
под свойством `itemids`.

[comment]: # ({/new-ae1ac187})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-98660baf})
#### Удаление нескольких правил LLD

Удаление двух правил LLD.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "discoveryrule.delete",
    "params": [
        "27665",
        "27668"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "ruleids": [
            "27665",
            "27668"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-98660baf})

[comment]: # ({new-76a51bc0})
### Исходный код

CDiscoveryRule::delete() в
*frontends/php/include/classes/api/services/CDiscoveryRule.php*.

[comment]: # ({/new-76a51bc0})
