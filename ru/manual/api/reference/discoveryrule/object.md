[comment]: # translation:outdated

[comment]: # ({new-c5a9e121})
# > Объект правила LLD

Следующие объекты напрямую связаны с `discoveryrule` API.

[comment]: # ({/new-c5a9e121})

[comment]: # ({new-c17fa830})
### Правило LLD

Объект низкоуровневого правила обнаружения имеет следующие свойства.

|Свойство|Тип|Описание|
|----------------|------|----------------|
|itemid|строка|*(только чтение)* ID правила LLD.|
|**delay**<br>(требуется)|строка|Интервал обновления LLD правила. Принимает секунды или единицы времени с суффиксом и с или без одного или нескольких [пользовательских интервалов](/ru/manual/config/items/item/custom_intervals), которые состоят как из гибких интервалов, так и интервалов по расписанию в виде сериализованных строк. Также принимает пользовательские макросы. Гибкие интервалы можно записать в виде двух макросов, разделенных прямой косой чертой. Интервалы разделаются точкой с запятой.|
|**hostid**<br>(требуется)|строка|ID узла сети, которому принадлежит правило LLD.|
|**interfaceid**<br>(требуется)|строка|ID интерфейса узла сети правила LLD. Используется только правилами LLD на узлах сети.<br><br>Опционален для правил LLD Zabbix агента (активный), Zabbix внутренний, Zabbix траппер и монитор баз данных.|
|**key\_**<br>(требуется)|строка|Ключ правила LLD.|
|**name**<br>(требуется)|строка|Имя правила LLD.|
|**type**<br>(требуется)|целое число|Тип правила LLD.<br><br>Возможные значения:<br>0 - Zabbix агент;<br>1 - SNMPv1 агент;<br>2 - Zabbix траппер;<br>3 - простая проверка;<br>4 - SNMPv2 агент;<br>5 - Zabbix внутренний;<br>6 - SNMPv3 агент;<br>7 - Zabbix агент (активный);<br>10 - внушняя проверка;<br>11 - монитор баз данных;<br>12 - IPMI агент;<br>13 - SSH агент;<br>14 - TELNET агент;<br>16 - JMX агент;<br>19 - HTTP агент;|
|**url**<br>(требуется)|строка|Строка URL, требуется для HTTP агент LLD правила. Поддерживаются пользовательские макросы, {HOST.IP}, {HOST.CONN}, {HOST.DNS}, {HOST.HOST}, {HOST.NAME}, {ITEM.ID}, {ITEM.KEY}.|
|allow\_traps|целое число|Поле HTTP агента LLD правила. Позволяет заполнять значение также как и в элементе данных с типом траппер.<br><br>0 - *(по умолчанию)* Не разрешать принимать входящие данные.<br>1 - Разрешать принимать входящие данные.|
|authtype|целое число|Используется только SSH агент и HTTP агент LLD правилами.<br><br>Возможные значения метода аутентификации SSH агента:<br>0 - *(по умолчанию)* пароль;<br>1 - публичный ключ.<br><br>Возможные значения метода аутентификации HTTP агента:<br>0 - *(по умолчанию)* нет<br>1 - простая<br>2 - NTLM|
|description|строка|Описание правила LLD.|
|error|строка|*(только чтение)* Текст ошибки, если имеются проблемы с обновлением правила LLD.|
|follow\_redirects|целое число|Поле HTTP агента LLD правила. Следование перенаправлениям при опросе данных.<br><br>0 - Не следовать перенаправлениям.<br>1 - *(по умолчанию)* Следовать перенаправлениям.|
|headers|объект|Поле HTTP агента LLD правила. Объект с HTTP(S) заголовками запроса, где имя заголовка используется ключом, а значение заголовка используется значением.<br><br>Пример:<br>{ "User-Agent": "Zabbix" }|
|http\_proxy|строка|Поле HTTP агента LLD правила. Строка подключения HTTP(S) прокси.|
|ipmi\_sensor|строка|Сенсор IPMI. Используется только правилами LLD IPMI.|
|jmx\_endpoint|строка|Строка пользовательского соединения с JMX агентом.<br><br>Значение по умолчанию:<br>service:jmx:rmi:///jndi/rmi://{HOST.CONN}:{HOST.PORT}/jmxrmi|
|lifetime|строка|Период времени после которого элементы данных, которые более не обнаруживаются, будут удалены. Принимает секунды, единицы времени с суффиксом и пользовательские макросы.<br><br>По умолчанию: `30d`.|
|output\_format|целое число|Поле HTTP агента LLD правила. Нужно ли конвертировать ответ в JSON.<br><br>0 - *(по умолчанию)* Записывать сырым.<br>1 - Конвертировать в JSON.|
|params|строка|Дополнительные параметры, которые зависят от типа правила LLD:<br>- выполняемый скрипт в случае SSH и Telnet правил LLD;<br>- SQL запрос в случае правила LLD монитора базы данных;<br>- формула в случае вычисляемых правил LLD.|
|password|строка|Пароль для аутентификации. Используется правилами LLD простой проверки, SSH, Telnet, монитором баз данных, JMX и HTTP агентом.|
|port|строка|Используемый правилом LLD порт. Используется только SNMP правилами LLD.|
|post\_type|целое число|Поле HTTP агента LLD правила. Тип тела post данных записываемых в post свойстве.<br><br>0 - *(по умолчанию)* Сырые данные.<br>2 - JSON данные.<br>3 - XML данные.|
|posts|строка|Поле HTTP агента LLD правила. Данные тела запроса HTTP(S). Используется совместно с post\_type.|
|privatekey|строка|Имя файла приватного ключа.|
|publickey|строка|Имя файла публичного ключа.|
|query\_fields|массив|Поле HTTP агента LLD правила. Параметры запроса. Массив объектов с парами 'ключ':'значение', где значение может быть пустой строкой.|
|request\_method|целое число|Поле HTTP агента LLD правила. Тип метода запроса.<br><br>0 - *(по умолчанию)* GET<br>1 - POST<br>2 - PUT<br>3 - HEAD|
|retrieve\_mode|целое число|Поле HTTP агента LLD правила. Какую часть ответа нужно сохранять.<br><br>0 - *(по умолчанию)* Тело.<br>1 - Заголовки.<br>2 - Сохранять как тело, так и заголовки.<br><br>Для request\_method HEAD разрешенное значение только 1.|
|snmp\_community|строка|SNMP community. Используется только SNMPv1 и SNMPv2 правилами LLD.|
|snmp\_oid|строка|SNMP OID.|
|snmpv3\_authpassphrase|строка|Фраза-пароль аутентификации SNMPv3. Используется только SNMPv3 правилами LLD.|
|snmpv3\_authprotocol|целое число|Протокол аутентификации SNMPv3. Используется только SNMPv3 правилами LLD.<br><br>Возможные значения:<br>0 - *(по умолчанию)* MD5;<br>1 - SHA.|
|snmpv3\_contextname|строка|Имя контекста SNMPv3. Используется только SNMPv3 правилами LLD.|
|snmpv3\_privpassphrase|строка|Фраза-пароль безопасности SNMPv3. Используется только SNMPv3 правилами LLD.|
|snmpv3\_privprotocol|целое число|Протокол безопасности SNMPv3. Используется только SNMPv3 правилами LLD.<br><br>Возможные значения:<br>0 - *(по умолчанию)* DES;<br>1 - AES.|
|snmpv3\_securitylevel|целое число|Уровень безопасности SNMPv3. Используется только SNMPv3 правилами LLD.<br><br>Возможные значения:<br>0 - noAuthNoPriv;<br>1 - authNoPriv;<br>2 - authPriv.|
|snmpv3\_securityname|строка|Имя безопасности SNMPv3. Используется только SNMPv3 правилами LLD.|
|ssl\_cert\_file|строка|Поле HTTP агента LLD правила. Путь к файлу публичного SSL ключа.|
|ssl\_key\_file|строка|Поле HTTP агента LLD правила. Путь к файлу приватного SSL ключа.|
|ssl\_key\_password|строка|Поле HTTP агента LLD правила. Пароль к файлу SSL ключа.|
|state|целое число|*(только чтение)* Статус правила LLD.<br><br>Возможные значения:<br>0 - *(по умолчанию)* нормальный;<br>1 - неподдерживается.|
|status|целое число|Состояние правила LLD.<br><br>Возможные значения:<br>0 - *(по умолчанию)* активированное правило LLD;<br>1 - деактивированное правило LLD.|
|status\_codes|строка|Поле HTTP агента LLD правила. Диапазоны требуемых HTTP кодов состояний, разделенные запятыми. Также как часть списка разделенного запятыми поддерживаются пользовательские макросы.<br><br>Пример: 200,200-{$M},{$M},200-400|
|templateid|строка|(только чтение) ID родительского правила LLD из шаблона.|
|timeout|строка|Поле HTTP агента LLD правила. Время ожидания запроса данных элемента данных. Поддерживаются пользовательские макросы.<br><br>по умолчанию: 3s<br>максимальное значение: 60s|
|trapper\_hosts|строка|Разрешенные хосты. Используется траппер и HTTP агент правилами LLD.|
|username|строка|Имя пользователя для аутентификации. Используется правилами LLD простой проверкой, SSH, Telnet, монитором баз данных, JMX и HTTP агентом.<br><br>Требуется для SSH и Telnet правил LLD.|
|verify\_host|целое число|Поле HTTP агента LLD правила. Проверка имени узла в URL в полях Common Name или Subject Alternate Name сертификата хоста.<br><br>0 - *(по умолчанию)* Не проверять.<br>1 - Проверять.|
|verify\_peer|целое число|Поле HTTP агента LLD правила. Проверка подлинности сертификата хоста.<br><br>0 - *(по умолчанию)* Не проверять.<br>1 - Проверять.|

[comment]: # ({/new-c17fa830})

[comment]: # ({new-b0412737})
### Фильтр правила LLD

Объект фильтра правила LLD определяет набор условий, которые можно
использовать для фильтрации обнаруженных объектов. Этот объект имеет
следующие свойства:

|Свойство|Тип|Описание|
|----------------|------|----------------|
|**conditions**<br>(требуется)|массив|Набор условий фильтрации, которые используются для фильтрации результатов.|
|**evaltype**<br>(требуется)|целое число|Метод вычисления условий фильтрации.<br><br>Возможные значения:<br>0 - и/или;<br>1 - и;<br>2 - или;<br>3 - пользовательское выражение.|
|eval\_formula|строка|*(только чтение)* Сгенерированное выражение, которое будет использоваться для вычисления условий фильтрации. Выражение содержит ID, которые являются ссылкой на определенные условия фильтрации по их `formulaid` полю. Значение `eval_formula` равно значению `formula` для фильтров с пользовательским выражением.|
|formula|строка|Заданное пользователем выражение, которое используется для вычисления условий фильтров с пользовательским выражением. Выражение должно содержать ID, которые являются ссылкой на определенные условия фильтрации по их `formulaid`. Эти ID используемые в выражении должны в точности совпадать с выражениями, которые заданы в условиях фильтрации: ни одно условие не должно остаться неиспользуемым или пропущенным.<br><br>Требуется для фильтров с пользовательским выражением.|

[comment]: # ({/new-b0412737})

[comment]: # ({new-be83ef5c})
#### Условие фильтрации правила LLD

Объект условия фильтрации правила LLD определяет индивидуальную
проверку, которая проверяет соответствие значения LLD макроса. Этот
метод имеет следующие свойства:

|Свойство|Тип|Описание|
|----------------|------|----------------|
|**macro**<br>(требуется)|строка|LLD макрос для которого необходимо выполнять проверку.|
|**value**<br>(требуется)|строка|Значение с которым необходимо выполнить сравнение.|
|formulaid|строка|Произвольный уникальный ID, который используется в качестве ссылки на условие из пользовательского выражения. Может содержать только буквы в верхнем регистре. Этот ID должен быть задан пользователем при изменении условий фильтрации, однако ID будут сгенерированы заново при последующих их запросах.|
|operator|целое число|Оператор условия.<br><br>Возможные значения:<br>8 - *(по умолчанию)* совпадение с регулярным выражением;<br>9 - не соответствует регулярному выражению.|

::: notetip
Чтобы лучше понимать как использовать фильтры с
различными типами выражений, смотрите примеры на страницах
[discoveryrule.get](get#получение_условий_фильтрации) и
[discoveryrule.create](create#использование_фильтрации_при_помощи_пользовательского_выражения)
методов.
:::

[comment]: # ({/new-be83ef5c})
















[comment]: # ({new-4a2b52b9})
### LLD macro path

The LLD macro path has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**lld\_macro**<br>(required)|string|LLD macro.|
|**path**<br>(required)|string|Selector for value which will be assigned to corresponding macro.|

[comment]: # ({/new-4a2b52b9})

[comment]: # ({new-14dc55c1})
### LLD rule preprocessing

The LLD rule preprocessing object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**type**<br>(required)|integer|The preprocessing option type.<br><br>Possible values:<br>5 - Regular expression matching;<br>11 - XML XPath;<br>12 - JSONPath;<br>15 - Does not match regular expression;<br>16 - Check for error in JSON;<br>17 - Check for error in XML;<br>20 - Discard unchanged with heartbeat;<br>23 - Prometheus to JSON;<br>24 - CSV to JSON;<br>25 - Replace;<br>27 - XML to JSON.|
|**params**<br>(required)|string|Additional parameters used by preprocessing option. Multiple parameters are separated by LF (\\n) character.|
|**error\_handler**<br>(required)|integer|Action type used in case of preprocessing step failure.<br><br>Possible values:<br>0 - Error message is set by Zabbix server;<br>1 - Discard value;<br>2 - Set custom value;<br>3 - Set custom error message.|
|**error\_handler\_params**<br>(required)|string|Error handler parameters. Used with `error_handler`.<br><br>Must be empty, if `error_handler` is 0 or 1.<br>Can be empty if, `error_handler` is 2.<br>Cannot be empty, if `error_handler` is 3.|

The following parameters and error handlers are supported for each
preprocessing type.

|Preprocessing type|Name|Parameter 1|Parameter 2|Parameter 3|Supported error handlers|
|------------------|----|-----------|-----------|-----------|------------------------|
|5|Regular expression|pattern^1^|output^2^|<|0, 1, 2, 3|
|11|XML XPath|path^3^|<|<|0, 1, 2, 3|
|12|JSONPath|path^3^|<|<|0, 1, 2, 3|
|15|Does not match regular expression|pattern^1^|<|<|0, 1, 2, 3|
|16|Check for error in JSON|path^3^|<|<|0, 1, 2, 3|
|17|Check for error in XML|path^3^|<|<|0, 1, 2, 3|
|20|Discard unchanged with heartbeat|seconds^4,\ 5,\ 6^|<|<|<|
|23|Prometheus to JSON|pattern^5,\ 7^|<|<|0, 1, 2, 3|
|24|CSV to JSON|character^2^|character^2^|0,1|0, 1, 2, 3|
|25|Replace|search string^2^|replacement^2^|<|<|
|27|XML to JSON|<|<|<|0, 1, 2, 3|

^1^ regular expression\
^2^ string\
^3^ JSONPath or XML XPath\
^4^ positive integer (with support of time suffixes, e.g. 30s, 1m, 2h,
1d)\
^5^ user macro\
^6^ LLD macro\
^7^ Prometheus pattern following the syntax:
`<metric name>{<label name>="<label value>", ...} == <value>`. Each
Prometheus pattern component (metric, label name, label value and metric
value) can be user macro.\
^8^ Prometheus output following the syntax: `<label name>`.

[comment]: # ({/new-14dc55c1})

[comment]: # ({new-2f32e5f0})
### LLD rule overrides

The LLD rule overrides object defines a set of rules (filters,
conditions and operations) that are used to override properties of
different prototype objects. It has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**name**<br>(required)|string|Unique override name.|
|**step**<br>(required)|integer|Unique order number of the override.|
|stop|integer|Stop processing next overrides if matches.<br><br>Possible values:<br>0 - *(default)* don't stop processing overrides;<br>1 - stop processing overrides if filter matches.|
|filter|object|Override filter.|
|operations|array|Override operations.|

[comment]: # ({/new-2f32e5f0})

[comment]: # ({new-573b52c4})
#### LLD rule override filter

The LLD rule override filter object defines a set of conditions that if
they match the discovered object the override is applied. It has the
following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**evaltype**<br>(required)|integer|Override filter condition evaluation method.<br><br>Possible values:<br>0 - and/or;<br>1 - and;<br>2 - or;<br>3 - custom expression.|
|**conditions**<br>(required)|array|Set of override filter conditions to use for matching the discovered objects.|
|eval\_formula|string|*(readonly)* Generated expression that will be used for evaluating override filter conditions. The expression contains IDs that reference specific override filter conditions by its `formulaid`. The value of `eval_formula` is equal to the value of `formula` for filters with a custom expression.|
|formula|string|User-defined expression to be used for evaluating conditions of override filters with a custom expression. The expression must contain IDs that reference specific override filter conditions by its `formulaid`. The IDs used in the expression must exactly match the ones defined in the override filter conditions: no condition can remain unused or omitted.<br><br>Required for custom expression override filters.|

[comment]: # ({/new-573b52c4})

[comment]: # ({new-e47f6c6a})
#### LLD rule override filter condition

The LLD rule override filter condition object defines a separate check
to perform on the value of an LLD macro. It has the following
properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**macro**<br>(required)|string|LLD macro to perform the check on.|
|**value**<br>(required)|string|Value to compare with.|
|formulaid|string|Arbitrary unique ID that is used to reference the condition from a custom expression. Can only contain capital-case letters. The ID must be defined by the user when modifying filter conditions, but will be generated anew when requesting them afterward.|
|operator|integer|Condition operator.<br><br>Possible values:<br>8 - *(default)* matches regular expression;<br>9 - does not match regular expression;<br>12 - exists;<br>13 - does not exist.|

[comment]: # ({/new-e47f6c6a})

[comment]: # ({new-e7b08f3c})
#### LLD rule override operation

The LLD rule override operation is combination of conditions and actions
to perform on the prototype object. It has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**operationobject**<br>(required)|integer|Type of discovered object to perform the action.<br><br>Possible values:<br>0 - Item prototype;<br>1 - Trigger prototype;<br>2 - Graph prototype;<br>3 - Host prototype.|
|operator|integer|Override condition operator.<br><br>Possible values:<br>0 - *(default)* equals;<br>1 - does not equal;<br>2 - contains;<br>3 - does not contain;<br>8 - matches;<br>9 - does not match.|
|value|string|Pattern to match item, trigger, graph or host prototype name depending on selected object.|
|opstatus|object|Override operation status object for item, trigger and host prototype objects.|
|opdiscover|object|Override operation discover status object (all object types).|
|opperiod|object|Override operation period (update interval) object for item prototype object.|
|ophistory|object|Override operation history object for item prototype object.|
|optrends|object|Override operation trends object for item prototype object.|
|opseverity|object|Override operation severity object for trigger prototype object.|
|optag|array|Override operation tag object for trigger and host prototype objects.|
|optemplate|array|Override operation template object for host prototype object.|
|opinventory|object|Override operation inventory object for host prototype object.|

[comment]: # ({/new-e7b08f3c})

[comment]: # ({new-52d3b579})
##### LLD rule override operation status

LLD rule override operation status that is set to discovered object. It
has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**status**<br>(required)|integer|Override the status for selected object.<br><br>Possible values:<br>0 - Create enabled;<br>1 - Create disabled.|

[comment]: # ({/new-52d3b579})

[comment]: # ({new-ff620350})
##### LLD rule override operation discover

LLD rule override operation discover status that is set to discovered
object. It has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**discover**<br>(required)|integer|Override the discover status for selected object.<br><br>Possible values:<br>0 - Yes, continue discovering the objects;<br>1 - No, new objects will not be discovered and existing ones will me marked as lost.|

[comment]: # ({/new-ff620350})

[comment]: # ({new-6e469c1b})
##### LLD rule override operation period

LLD rule override operation period is an update interval value (supports
custom intervals) that is set to discovered item. It has the following
properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**delay**<br>(required)|string|Override the update interval of the item prototype. Accepts seconds or a time unit with suffix (30s,1m,2h,1d) as well as flexible and scheduling intervals and user macros or LLD macros. Multiple intervals are separated by a semicolon.|

[comment]: # ({/new-6e469c1b})

[comment]: # ({new-ff97489f})
##### LLD rule override operation history

LLD rule override operation history value that is set to discovered
item. It has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**history**<br>(required)|string|Override the history of item prototype which is a time unit of how long the history data should be stored. Also accepts user macro and LLD macro.|

[comment]: # ({/new-ff97489f})

[comment]: # ({new-10b52601})
##### LLD rule override operation trends

LLD rule override operation trends value that is set to discovered item.
It has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**trends**<br>(required)|string|Override the trends of item prototype which is a time unit of how long the trends data should be stored. Also accepts user macro and LLD macro.|

[comment]: # ({/new-10b52601})

[comment]: # ({new-991f3bdc})
##### LLD rule override operation severity

LLD rule override operation severity value that is set to discovered
trigger. It has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**severity**<br>(required)|integer|Override the severity of trigger prototype.<br><br>Possible values are: 0 - *(default)* not classified;<br>1 - information;<br>2 - warning;<br>3 - average;<br>4 - high;<br>5 - disaster.|

[comment]: # ({/new-991f3bdc})

[comment]: # ({new-0693b8f3})
##### LLD rule override operation tag

LLD rule override operation tag object contains tag name and value that
are set to discovered object. It has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**tag**<br>(required)|string|New tag name.|
|value|string|New tag value.|

[comment]: # ({/new-0693b8f3})

[comment]: # ({new-5eebc05a})
##### LLD rule override operation template

LLD rule override operation template object that is linked to discovered
host. It has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**templateid**<br>(required)|string|Override the template of host prototype linked templates.|

[comment]: # ({/new-5eebc05a})

[comment]: # ({new-9c5cc3d1})
##### LLD rule override operation inventory

LLD rule override operation inventory mode value that is set to
discovered host. It has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**inventory\_mode**<br>(required)|integer|Override the host prototype inventory mode.<br><br>Possible values are:<br>-1 - disabled;<br>0 - *(default)* manual;<br>1 - automatic.|

[comment]: # ({/new-9c5cc3d1})
