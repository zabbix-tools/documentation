[comment]: # translation:outdated

[comment]: # ({new-2709706a})
# discoveryrule.create

[comment]: # ({/new-2709706a})

[comment]: # ({new-78136d05})
### Описание

`объект discoveryrule.create(объект/массив ПравилаLLD)`

Этот метод позволяет создавать новые правила LLD.

[comment]: # ({/new-78136d05})

[comment]: # ({new-1c165cc1})
### Параметры

`(объект/массив)` Создаваемые правила LLD.

В дополнение к [стандартным свойствам правила LLD](object#правило_lld),
этот метод принимает следующие параметры.

|Параметр|Тип|Описание|
|----------------|------|----------------|
|filter|объект|Объект фильтра правила LLD для правила LLD.|

[comment]: # ({/new-1c165cc1})

[comment]: # ({new-08a72633})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID созданных правил LLD
под свойством `itemids`. Порядок возвращаемых ID совпадает с порядком
переданных правил LLD.

[comment]: # ({/new-08a72633})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-c30fe277})
#### Создание правила LLD

Создание правила LLD с типом Zabbix агент для обнаружения
примонтированных файловых систем. Обнаруженные элементы данных будут
обновляться каждые 30 секунд.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "discoveryrule.create",
    "params": {
        "name": "Mounted filesystem discovery",
        "key_": "vfs.fs.discovery",
        "hostid": "10197",
        "type": "0",
        "interfaceid": "112",
        "delay": "30s"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "27665"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-c30fe277})

[comment]: # ({new-01b95865})
#### Использование фильтра

Создание правила LLD с набором условий по которым необходимо фильтровать
результаты. Условия будут сгруппированы вместе при помощи логического
оператора "и".

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "discoveryrule.create",
    "params": {
        "name": "Filtered LLD rule",
        "key_": "lld",
        "hostid": "10116",
        "type": "0",
        "interfaceid": "13",
        "delay": "30s",
        "filter": {
            "evaltype": 1,
            "conditions": [
                {
                    "macro": "{#MACRO1}",
                    "value": "@regex1"
                },
                {
                    "macro": "{#MACRO2}",
                    "value": "@regex2"
                },
                {
                    "macro": "{#MACRO3}",
                    "value": "@regex3"
                }
            ]
        }
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "27665"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-01b95865})

[comment]: # ({new-a201a7a1})
#### Использование фильтрации при помощи пользовательского выражения

Создание правила LLD с фильтром, который будет использовать
пользовательское выражение для вычисления условий. Правило LLD должно
обнаруживать только объекты у которых значение макроса "{\#MACRO1}"
совпадает с обеими регулярными выражениями "regex1" и "regex2", и
значение "{\#MACRO2}" совпадает либо с "regex3", либо с "regex4". ID
"A", "B", "C" и "D" в формуле были выбраны случайно.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "discoveryrule.create",
    "params": {
        "name": "Filtered LLD rule",
        "key_": "lld",
        "hostid": "10116",
        "type": "0",
        "interfaceid": "13",
        "delay": "30s",
        "filter": {
            "evaltype": 3,
            "formula": "(A and B) and (C or D)",
            "conditions": [
                {
                    "macro": "{#MACRO1}",
                    "value": "@regex1",
                    "formulaid": "A"
                },
                {
                    "macro": "{#MACRO1}",
                    "value": "@regex2",
                    "formulaid": "B"
                },
                {
                    "macro": "{#MACRO2}",
                    "value": "@regex3",
                    "formulaid": "C"
                },
                {
                    "macro": "{#MACRO2}",
                    "value": "@regex4",
                    "formulaid": "D"
                }
            ]
        }
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "27665"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-a201a7a1})

[comment]: # ({new-32af137e})
#### Использование пользовательских полей запроса и заголовков

Создание LLD правила с пользовательскими полями запроса и заголовками.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "discoveryrule.create",
    "params": {
        "hostid": "10257",
        "interfaceid": "5",
        "type": "19",
        "name": "API HTTP agent",
        "key_": "api_discovery_rule",
        "value_type": "3",
        "delay": "5s",
        "url": "http://127.0.0.1?discoverer.php",
        "query_fields": [
            {
                "mode": "json"
            },
            {
                "elements":"2"
            }
        ],
        "headers": {
            "X-Type": "api",
            "Authorization": "Bearer mF_A.B5f-2.1JcM"
        },
        "allow_traps": "1",
        "trapper_hosts": "127.0.0.1",
        "id": 35,
        "auth": "d678e0b85688ce578ff061bd29a20d3b",
    }
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "28336"
        ]
    },
    "id": 35
}
```

[comment]: # ({/new-32af137e})

[comment]: # ({new-001d915b})
### Смотрите также

-   [Фильтр правила LLD](object#фильтр_правила_lld)

[comment]: # ({/new-001d915b})

[comment]: # ({new-3e76a4a9})
### Исходный код

CDiscoveryRule::create() в
*ui/include/classes/api/services/CDiscoveryRule.php*.

[comment]: # ({/new-3e76a4a9})





[comment]: # ({new-f9343aca})
#### Creating a LLD rule with overrides

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "discoveryrule.create",
    "params": {
        "name": "Discover database host",
        "key_": "lld.with.overrides",
        "hostid": "10001",
        "type": 0,
        "value_type": 3,
        "delay": "60s",
        "interfaceid": "1155",
        "overrides": [
            {
                "name": "Discover MySQL host",
                "step": "1",
                "stop": "1",
                "filter": {
                    "evaltype": "2",
                    "conditions": [
                        {
                            "macro": "{#UNIT.NAME}",
                            "operator": "8",
                            "value": "^mysqld\\.service$"
                        },
                        {
                            "macro": "{#UNIT.NAME}",
                            "operator": "8",
                            "value": "^mariadb\\.service$"
                        }
                    ]
                },
                "operations": [
                    {
                        "operationobject": "3",
                        "operator": "2",
                        "value": "Database host",
                        "opstatus": {
                            "status": "0"
                        },
                        "optemplate": [
                            {
                                "templateid": "10170"
                            }
                        ],
                        "optag": [
                            {
                                "tag": "Database",
                                "value": "MySQL"
                            }
                        ]
                    }
                ]
            },
            {
                "name": "Discover PostgreSQL host",
                "step": "2",
                "stop": "1",
                "filter": {
                    "evaltype": "0",
                    "conditions": [
                        {
                            "macro": "{#UNIT.NAME}",
                            "operator": "8",
                            "value": "^postgresql\\.service$"
                        }
                    ]
                },
                "operations": [
                    {
                        "operationobject": "3",
                        "operator": "2",
                        "value": "Database host",
                        "opstatus": {
                            "status": "0"
                        },
                        "optemplate": [
                            {
                                "templateid": "10263"
                            }
                        ],
                        "optag": [
                            {
                                "tag": "Database",
                                "value": "PostgreSQL"
                            }
                        ]
                    }
                ]
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "30980"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-f9343aca})

[comment]: # ({new-c75d7d1e})
#### Create script LLD rule

Create a simple data collection using a script LLD rule.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "discoveryrule.create",
    "params": {
        "name": "Script example",
        "key_": "custom.script.lldrule",
        "hostid": "12345",
        "type": 21,
        "value_type": 4,
        "params": "var request = new CurlHttpRequest();\nreturn request.Post(\"https://postman-echo.com/post\", JSON.parse(value));",
        "parameters": [{
            "name": "host",
            "value": "{HOST.CONN}"
        }],
        "timeout": "6s",
        "delay": "30s"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 2
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "23865"
        ]
    },
    "id": 3
}
```

[comment]: # ({/new-c75d7d1e})

[comment]: # ({new-f40c8a6e})
### See also

-   [LLD rule filter](object#lld_rule_filter)
-   [LLD macro paths](object#lld_macro_path)
-   [LLD rule preprocessing](object#lld_rule_preprocessing)

[comment]: # ({/new-f40c8a6e})

[comment]: # ({new-73c4009f})
### Source

CDiscoveryRule::create() in
*ui/include/classes/api/services/CDiscoveryRule.php*.

[comment]: # ({/new-73c4009f})
