[comment]: # translation:outdated

[comment]: # ({new-04928fa3})
# discoveryrule.copy

[comment]: # ({/new-04928fa3})

[comment]: # ({new-8ae93b5b})
### Описание

`объект discoveryrule.copy(объект параметры)`

Этот метод позволяет копировать правила LLD со всеми их прототипами в
заданные узлы сети.

[comment]: # ({/new-8ae93b5b})

[comment]: # ({new-47295f20})
### Параметры

`(объект)` Параметры, которые задают копируемые правила LLD и целевые
узлы сети.

|Параметр|Тип|Описание|
|----------------|------|----------------|
|discoveryids|массив|ID копируемых правил LLD.|
|hostids|массив|ID узлов сети, в которые скопируются правила LLD.|

[comment]: # ({/new-47295f20})

[comment]: # ({new-b8760eee})
### Возвращаемые значения

`(логическое)` Возвращает `true`, при успешном копировании.

[comment]: # ({/new-b8760eee})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-91be874e})
#### Копирование правила LLD на несколько узлов сети

Копирование правила LLD на два узла сети.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "discoveryrule.copy",
    "params": {
        "discoveryids": [
            "27426"
        ],
        "hostids": [
            "10196",
            "10197"
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": true,
    "id": 1
}
```

[comment]: # ({/new-91be874e})

[comment]: # ({new-8870f994})
### Исходный код

CDiscoveryrule::copy() в
*frontends/php/include/classes/api/services/CDiscoveryRule.php*.

[comment]: # ({/new-8870f994})
