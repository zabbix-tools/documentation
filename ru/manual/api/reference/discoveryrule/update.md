[comment]: # translation:outdated

[comment]: # ({new-d756b50a})
# discoveryrule.update

[comment]: # ({/new-d756b50a})

[comment]: # ({new-0d23e0d8})
### Описание

`объект discoveryrule.update(объект/массив lldRules)`

Этот метод позволяет обновлять существующие правила LLD.

[comment]: # ({/new-0d23e0d8})

[comment]: # ({new-cf98658c})
### Параметры

`(объект/массив)` Свойства правил LLD, которые будут обновлены.

Свойство `itemid` должно быть указано по каждому правилу LLD, все
остальные свойства опциональны. Будут обновлены только переданные
свойства, все остальные останутся неизменными.

В дополнение к [стандартным свойствам правила LLD](object#правило_lld),
этот метод принимает следующие параметры.

|Параметр|Тип|Описание|
|----------------|------|----------------|
|filter|объект|Объект фильтра правила LLD, который заменит текущий фильтр.|

[comment]: # ({/new-cf98658c})

[comment]: # ({new-6f962e26})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID обновленных правил LLD
под свойством `itemids`.

[comment]: # ({/new-6f962e26})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-67c9278f})
#### Добавление фильтра к правилу LLD

Добавление фильтра таким образом, чтобы содержимое макроса *{\#FSTYPE}*
совпадало с регулярным выражением *\@File systems for discovery*.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "discoveryrule.update",
    "params": {
        "itemid": "22450",
        "filter": {
            "evaltype": 1,
            "conditions": [
                {
                    "macro": "{#FSTYPE}",
                    "value": "@File systems for discovery"
                }
            ]
        }
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "22450"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-67c9278f})

[comment]: # ({new-f3fd4d6c})
#### Отключение трапов

Отключение трапов у LLD правила обнаружения.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "discoveryrule.update",
    "params": {
        "itemid": "28336",
        "allow_traps": "0"
    },
    "id": 36,
    "auth": "d678e0b85688ce578ff061bd29a20d3b"
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "28336"
        ]
    },
    "id": 36
}
```

[comment]: # ({/new-f3fd4d6c})

[comment]: # ({new-8d8cc019})
### Исходный код

CDiscoveryRule::update() в
*frontends/php/include/classes/api/services/CDiscoveryRule.php*.

[comment]: # ({/new-8d8cc019})




[comment]: # ({new-bef0a5f5})
#### Updating LLD rule preprocessing options

Update an LLD rule with preprocessing rule “JSONPath”.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "discoveryrule.update",
    "params": {
        "itemid": "44211",
        "preprocessing": [
            {
                "type": "12",
                "params": "$.path.to.json",
                "error_handler": "2",
                "error_handler_params": "5"
            }
        ]
    },
    "auth": "700ca65537074ec963db7efabda78259",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "44211"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-bef0a5f5})

[comment]: # ({new-3f18a1c3})
#### Updating LLD rule script

Update an LLD rule script with a different script and remove unnecessary
parameters that were used by previous script.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "discoveryrule.update",
    "params": {
        "itemid": "23865",
        "parameters": [],
        "script": "Zabbix.Log(3, 'Log test');\nreturn 1;"
    },
    "auth": "700ca65537074ec963db7efabda78259",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "23865"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-3f18a1c3})

[comment]: # ({new-6a73b645})
### Source

CDiscoveryRule::update() in
*ui/include/classes/api/services/CDiscoveryRule.php*.

[comment]: # ({/new-6a73b645})
