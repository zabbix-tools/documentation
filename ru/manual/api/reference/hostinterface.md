[comment]: # translation:outdated

[comment]: # ({new-6bf804b5})
# Интерфейс узла сети

Этот класс предназначен для работы с интерфейсами узлов сети.

Справка по объектам:\

-   [Интерфейс узла
    сети](/ru/manual/api/reference/hostinterface/object#интерфейс_узла_сети)

Доступные методы:\

-   [hostinterface.create](/ru/manual/api/reference/hostinterface/create) -
    создание новых интерфейсов узлов сети
-   [hostinterface.delete](/ru/manual/api/reference/hostinterface/delete) -
    удаление интерфейсов узлов сети
-   [hostinterface.get](/ru/manual/api/reference/hostinterface/get) -
    получение интерфейсов узлов сети
-   [hostinterface.massadd](/ru/manual/api/reference/hostinterface/massadd) -
    добавление интерфейсов узлов сети на узлы сети
-   [hostinterface.massremove](/ru/manual/api/reference/hostinterface/massremove) -
    удаление интерфейсов узлов сети с узлов сети
-   [hostinterface.replacehostinterfaces](/ru/manual/api/reference/hostinterface/replacehostinterfaces) -
    замена интерфейсов узлов сети на узле сети
-   [hostinterface.update](/ru/manual/api/reference/hostinterface/update) -
    обновление свойств интерфейсов узлов сети

[comment]: # ({/new-6bf804b5})
