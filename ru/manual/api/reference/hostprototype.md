[comment]: # translation:outdated

[comment]: # ({new-517ea62c})
# Прототип узлов сети

Этот класс предназначен для работы с прототипами узлов сети.

Справка по объектам:\

-   [Прототип узлов
    сети](/ru/manual/api/reference/hostprototype/object#прототип_узлов_сети)
-   [Данные инвентаризации прототипа узлов
    сети](/ru/manual/api/reference/hostprototype/object#данные_инвентаризации_прототипа_узлов_сети)
-   [Соединение с
    группой](/ru/manual/api/reference/hostprototype/object#соединение_с_группой)
-   [Прототип
    групп](/ru/manual/api/reference/hostprototype/object#прототип_групп)

Доступные методы:\

-   [hostprototype.create](/ru/manual/api/reference/hostprototype/create) -
    создание новых прототипов узлов сети
-   [hostprototype.delete](/ru/manual/api/reference/hostprototype/delete) -
    удаление прототипов узлов сети
-   [hostprototype.get](/ru/manual/api/reference/hostprototype/get) -
    получение прототипов узлов сети
-   [hostprototype.update](/ru/manual/api/reference/hostprototype/update) -
    обновление свойств прототипов узлов сети

[comment]: # ({/new-517ea62c})
