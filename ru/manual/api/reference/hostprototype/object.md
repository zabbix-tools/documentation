[comment]: # translation:outdated

[comment]: # ({new-6e26e29b})
# > Объект прототипа узлов сети

Следующие объекты напрямую связаны с `hostprototype` API.

[comment]: # ({/new-6e26e29b})

[comment]: # ({new-e69daa4d})
### Прототип узлов сети

Объект прототипа узлов сети имеет следующие свойства.

|Свойство|Тип|Описание|
|----------------|------|----------------|
|hostid|строка|*(только чтение)* ID прототипа узлов сети.|
|**host**<br>(требуется)|строка|Техническое имя прототипа узлов сети.|
|name|строка|Видимое имя прототипа узлов сети.<br><br>По умолчанию: значение свойства `host`.|
|status|целое число|Состояние прототипа узлов сети.<br><br>Возможные значения:<br>0 - *(по умолчанию)* узел сети под наблюдением;<br>1 - узел сети без наблюдения.|
|templateid|строка|*(только чтение)* ID родительского прототипа узлов сети из шаблона.|
|tls\_connect|целое число|Подключения к узлу сети.<br><br>Возможные значения:<br>1 - *(по умолчанию)* Без шифрования;<br>2 - PSK;<br>4 - сертификат.|
|tls\_accept|целое число|Соединения с узла сети.<br><br>Возмодные битовые значения:<br>1 - *(по умолчанию)* Без шифрования;<br>2 - PSK;<br>4 - сертификат.|
|tls\_issuer|строка|Эмитент сертификата.|
|tls\_subject|строка|Субъект сертификата.|
|tls\_psk\_identity|строка|Идентификатор PSK. Требуется, если либо в `tls_connect`, либо в `tls_accept` выбран PSK.|
|tls\_psk|строка|Pre-shared ключ, по крайней мере 32 шестнадцатеричных цифры. Требуется, если либо в `tls_connect`, либо в `tls_accept` выбран PSK.|

[comment]: # ({/new-e69daa4d})

[comment]: # ({new-e3297546})
### Данные инвентаризации прототипа узлов сети

Объект данных инвентаризации прототипа узлов сети имеет следующие
свойства.

|Свойство|Тип|Описание|
|----------------|------|----------------|
|inventory\_mode|целое число|Режим заполнения инвентарных данных прототипа узлов сети.<br><br>Возможные значения:<br>-1 - деактивировано;<br>0 - *(по умолчанию)* вручную;<br>1 - автоматически.|

[comment]: # ({/new-e3297546})

[comment]: # ({new-dabb4716})
### Соединение с группой

Объект соединения с группой связывает прототип узлов сети с группой
узлов сети и имеет следующие свойства.

|Свойство|Тип|Описание|
|----------------|------|----------------|
|group\_prototypeid|строка|*(только чтение)* ID соединения с группой.|
|**groupid**<br>(требуется)|строка|ID группы узлов сети.|
|hostid|строка|*(только чтение)* ID прототипа узлов сети|
|templateid|строка|*(только чтение)* ID родительского соединения с группой из шаблона.|

[comment]: # ({/new-dabb4716})

[comment]: # ({new-d42c6308})
### Прототип групп

Объект прототипа группы задает группу, которая будет создана для
обнаруженного узла сети и имеет следующие свойства.

|Свойство|Тип|Описание|
|----------------|------|----------------|
|group\_prototypeid|строка|*(только чтение)* ID прототипа группы.|
|**name**<br>(требуется)|строка|Имя прототипа группы.|
|hostid|строка|*(только чтение)* ID прототипа узлов сети|
|templateid|строка|*(только чтение)* ID родительского прототипа группы из шаблона.|

[comment]: # ({/new-d42c6308})



[comment]: # ({new-ed8d8550})
### Custom interface

The custom interface object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|dns|string|DNS name used by the interface.<br><br>**Required** if the connection is made via DNS. Can contain macros.|
|ip|string|IP address used by the interface.<br><br>**Required** if the connection is made via IP. Can contain macros.|
|**main**<br>(required)|integer|Whether the interface is used as default on the host. Only one interface of some type can be set as default on a host.<br><br>Possible values are:<br>0 - not default;<br>1 - default.|
|**port**<br>(required)|string|Port number used by the interface. Can contain user and LLD macros.|
|**type**<br>(required)|integer|Interface type.<br><br>Possible values are:<br>1 - agent;<br>2 - SNMP;<br>3 - IPMI;<br>4 - JMX.<br>|
|**useip**<br>(required)|integer|Whether the connection should be made via IP.<br><br>Possible values are:<br>0 - connect using host DNS name;<br>1 - connect using host IP address for this host interface.|
|details|array|Additional object for interface. **Required** if interface 'type' is SNMP.|

[comment]: # ({/new-ed8d8550})

[comment]: # ({new-6ea1b012})
### Custom interface details

The details object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**version**<br>(required)|integer|SNMP interface version.<br><br>Possible values are:<br>1 - SNMPv1;<br>2 - SNMPv2c;<br>3 - SNMPv3|
|bulk|integer|Whether to use bulk SNMP requests.<br><br>Possible values are:<br>0 - don't use bulk requests;<br>1 - *(default)* - use bulk requests.|
|community|string|SNMP community. Used only by SNMPv1 and SNMPv2 interfaces.|
|securityname|string|SNMPv3 security name. Used only by SNMPv3 interfaces.|
|securitylevel|integer|SNMPv3 security level. Used only by SNMPv3 interfaces.<br><br>Possible values are:<br>0 - *(default)* - noAuthNoPriv;<br>1 - authNoPriv;<br>2 - authPriv.|
|authpassphrase|string|SNMPv3 authentication passphrase. Used only by SNMPv3 interfaces.|
|privpassphrase|string|SNMPv3 privacy passphrase. Used only by SNMPv3 interfaces.|
|authprotocol|integer|SNMPv3 authentication protocol. Used only by SNMPv3 interfaces.<br><br>Possible values are:<br>0 - *(default)* - MD5;<br>1 - SHA1;<br>2 - SHA224;<br>3 - SHA256;<br>4 - SHA384;<br>5 - SHA512.|
|privprotocol|integer|SNMPv3 privacy protocol. Used only by SNMPv3 interfaces.<br><br>Possible values are:<br>0 - *(default)* - DES;<br>1 - AES128;<br>2 - AES192;<br>3 - AES256;<br>4 - AES192C;<br>5 - AES256C.|
|contextname|string|SNMPv3 context name. Used only by SNMPv3 interfaces.|

[comment]: # ({/new-6ea1b012})
