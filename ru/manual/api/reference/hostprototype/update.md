[comment]: # translation:outdated

[comment]: # ({new-c0df6427})
# hostprototype.update

[comment]: # ({/new-c0df6427})

[comment]: # ({new-796670ab})
### Описание

`объект hostprototype.update(объект/массив ПрототипыУзловсети)`

Этот метод позволяет обновлять существующие прототипы узлов сети.

[comment]: # ({/new-796670ab})

[comment]: # ({new-50727275})
### Параметры

`(объект/массив)` Свойства прототипов узлов сети, которые будут
обновлены.

Свойство `hostid` должно быть указано по каждому прототипу узлов сети,
все остальные свойства опциональны. Будут обновлены только переданные
свойства, все остальные останутся неизменными.

В дополнение к [стандартным свойствам прототипа узлов
сети](object#прототип_узлов_сети), этот метод принимает следующие
параметры.

|Параметр|Тип|Описание|
|----------------|------|----------------|
|groupLinks|массив|Соединения с группами, которые заменят текущие соединения с группами у прототипа узлов сети.|
|groupPrototypes|массив|Прототипы групп, которые заменят существующие прототипы групп у прототипа узлов сети.|
|inventory|объект|Свойства данных инвентаризации прототипа узлов сети.|
|templates|объект/массив|Шаблоны, которые заменят присоединенные в настоящий момент шаблоны.<br><br>У шаблонов должно быть задано свойство `templateid`.|

[comment]: # ({/new-50727275})

[comment]: # ({new-c97defa2})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID обновленных прототипов
узлов сети под свойством `hostids`.

[comment]: # ({/new-c97defa2})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-9e34a671})
#### Деактивация прототипа узлов сети

Деактивация прототипа узлов сети, то есть изменение его состояния на
значение 1.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "hostprototype.update",
    "params": {
        "hostid": "10092",
        "status": 1
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "hostids": [
            "10092"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-9e34a671})

[comment]: # ({new-440003e2})
### Смотрите также

-   [Соединение с группой](object#соединение_с_группой)
-   [Прототип группы](object#прототип_группы)
-   [Данные инвентаризации прототипа узлов
    сети](object#данные_инвентаризации_прототипа_узлов_сети)

[comment]: # ({/new-440003e2})

[comment]: # ({new-7a7b18a4})
### Исходный код

CHostPrototype::update() в
*frontends/php/include/classes/api/services/CHostPrototype.php*.

[comment]: # ({/new-7a7b18a4})



[comment]: # ({new-c4a38bf3})
### See also

-   [Group link](object#group_link)
-   [Group prototype](object#group_prototype)
-   [Host prototype tag](object#host_prototype_tag)
-   [Custom interface](object#custom_interface)
-   [User
    macro](/manual/api/reference/usermacro/object#hosttemplate_level_macro)

[comment]: # ({/new-c4a38bf3})

[comment]: # ({new-a380e3fb})
### Source

CHostPrototype::update() in
*ui/include/classes/api/services/CHostPrototype.php*.

[comment]: # ({/new-a380e3fb})
