[comment]: # translation:outdated

[comment]: # ({new-8f15992f})
# hostprototype.delete

[comment]: # ({/new-8f15992f})

[comment]: # ({new-f7c792a6})
### Описание

`объект hostprototype.delete(массив hostPrototypeIds)`

Этот метод позволяет удалять прототипы узлов сети.

[comment]: # ({/new-f7c792a6})

[comment]: # ({new-65284f99})
### Параметры

`(массив)` ID удаляемых прототипов узлов сети

[comment]: # ({/new-65284f99})

[comment]: # ({new-78e6f07c})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID удаленных прототипов
узлов сети под свойством `hostids`.

[comment]: # ({/new-78e6f07c})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-48be53a2})
#### Удаление нескольких прототипов узлов сети

Удаление двух прототипов узлов сети.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "hostprototype.delete",
    "params": [
        "10103",
        "10105"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "hostids": [
            "10103",
            "10105"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-48be53a2})

[comment]: # ({new-dd993f84})
### Исходный код

CHostPrototype::delete() в
*frontends/php/include/classes/api/services/CHostPrototype.php*.

[comment]: # ({/new-dd993f84})
