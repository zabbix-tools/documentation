[comment]: # translation:outdated

[comment]: # ({new-31d5554a})
# hostprototype.get

[comment]: # ({/new-31d5554a})

[comment]: # ({new-d1534f8a})
### Описание

`целое число/массив hostprototype.get(объект параметры)`

Этот метод позволяет получать прототипы узлов сети в соответствии с
заданными параметрами.

[comment]: # ({/new-d1534f8a})

[comment]: # ({new-0d6ab1f2})
### Параметры

`(объект)` Параметры задают желаемый вывод.

Этот метод поддерживает следующие параметры.

|Параметр|Тип|Описание|
|----------------|------|----------------|
|hostids|строка/массив|Возврат прототипов узлов сети только с заданными ID.|
|discoveryids|строка/массив|Возврат только тех прототипов узлов сети, которые принадлежат заданным LLD правилам.|
|inherited|логический|Если задано значение `true`, возвращать только те прототипы элементов данных, которые унаследованы из шаблона.|
|selectDiscoveryRule|запрос|Возврат LLD правила, которому принадлежит прототип узлов сети, в свойстве `discoveryRule`.|
|selectGroupLinks|запрос|Возврат соединений с группами прототипа узлов сети в свойстве `groupLinks`.|
|selectGroupPrototypes|запрос|Возврат прототипов групп прототипа узлов сети в свойстве `groupPrototypes`.|
|selectInventory|логический/массив|Возврат данных инвентаризации прототипа узлов сети в свойстве `inventory`.<br><br>Возможными значениями являются `true` для получения всех данных или массив имен свойств для получения только указанных свойств.|
|selectParentHost|запрос|Возврат узла сети, которому принадлежит прототип узлов сети, в свойстве `parentHost`.|
|selectTemplates|запрос|Возврат шаблонов, которые соединены с прототипами узлов сети, в свойстве `templates`.|
|sortfield|строка/массив|Сортировка результата в соответствии с заданными свойствами.<br><br>Возможные значения: `hostid`, `host`, `name` и `status`.|
|countOutput|логический|Эти параметры являются общими для всех методов `get` и они описаны в [справочных комментариях](/ru/manual/api/reference_commentary#общие_параметры_get_метода).|
|editable|логический|^|
|excludeSearch|логический|^|
|filter|объект|^|
|limit|целое число|^|
|output|запрос|^|
|preservekeys|логический|^|
|search|объект|^|
|searchByAny|логический|^|
|searchWildcardsEnabled|логический|^|
|sortorder|строка/массив|^|
|startSearch|логический|^|

[comment]: # ({/new-0d6ab1f2})

[comment]: # ({new-7223bab1})
### Возвращаемые значения

`(целое число/массив)` Возвращает либо:

-   массив объектов;
-   количество найденных объектов, если используется параметр
    `countOutput`.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-a91c8b17})
#### Получение прототипов узлов сети с LLD правила

Получение всех прототипов узлов сети и их соединий с группами и
прототипы групп с LLD правила.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "hostprototype.get",
    "params": {
        "output": "extend",
        "selectGroupLinks": "extend",
        "selectGroupPrototypes": "extend",
        "discoveryids": "23554"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "hostid": "10092",
            "host": "{#HV.UUID}",
            "status": "0",
            "name": "{#HV.NAME}",
            "templateid": "0",
            "tls_connect": "1",
            "tls_accept": "1",
            "tls_issuer": "",
            "tls_subject": "",
            "tls_psk_identity": "",
            "tls_psk": "",
            "groupLinks": [
                {
                    "group_prototypeid": "4",
                    "hostid": "10092",
                    "groupid": "7",
                    "templateid": "0"
                }
            ],
            "groupPrototypes": [
                {
                    "group_prototypeid": "7",
                    "hostid": "10092",
                    "name": "{#CLUSTER.NAME}",
                    "templateid": "0"
                }
            ]
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-a91c8b17})

[comment]: # ({new-749ec170})
### Смотрите также

-   [Соединение с группой](object#соединение_с_группой)
-   [Прототип группы](object#прототип_группы)
-   [Данные инвентаризации прототипа узлов
    сети](object#данные_инвентаризации_прототипа_узлов_сети)

[comment]: # ({/new-749ec170})

[comment]: # ({new-437ab274})
### Исходный код

CHostPrototype::get() в
*frontends/php/include/classes/api/services/CHostPrototype.php*.

[comment]: # ({/new-437ab274})
