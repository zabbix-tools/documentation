[comment]: # translation:outdated

[comment]: # ({new-8eb68faf})
# Группа узлов сети

Этот класс предназначен для работы с группами узлов сети.

Справка по объектам:\

-   [Группа узлов
    сети](/ru/manual/api/reference/hostgroup/object#группа_узлов_сети)

Доступные методы:\

-   [hostgroup.create](/ru/manual/api/reference/hostgroup/create) -
    создание новых групп узлов сети
-   [hostgroup.delete](/ru/manual/api/reference/hostgroup/delete) -
    удаление групп узлов сети
-   [hostgroup.get](/ru/manual/api/reference/hostgroup/get) - получение
    групп узлов сети
-   [hostgroup.massadd](/ru/manual/api/reference/hostgroup/massadd) -
    добавление связанных объектов к группам узлов сети
-   [hostgroup.massremove](/ru/manual/api/reference/hostgroup/massremove) -
    удаление связанных объектов с групп узлов сети
-   [hostgroup.massupdate](/ru/manual/api/reference/hostgroup/massupdate) -
    замена или удаление связанных объектов с групп узлов сети
-   [hostgroup.update](/ru/manual/api/reference/hostgroup/update) -
    обновление групп узлов сети

[comment]: # ({/new-8eb68faf})
