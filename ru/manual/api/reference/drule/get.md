[comment]: # translation:outdated

[comment]: # ({new-dbbaf81a})
# drule.get

[comment]: # ({/new-dbbaf81a})

[comment]: # ({new-bbd48f20})
### Описание

`целое число/массив drule.get(объект параметры)`

Этот метод позволяет получать правила обнаружения в соответствии с
заданными параметрами.

[comment]: # ({/new-bbd48f20})

[comment]: # ({new-0b971cc5})
### Параметры

`(объект)` Параметры задают желаемый вывод.

Этот метод поддерживает следующие параметры.

|Параметр|Тип|Описание|
|----------------|------|----------------|
|dhostids|строка/массив|Возврат только тех правил обнаружения, которые создали заданные обнаруженные узлы сети.|
|druleids|строка/массив|Возврат правил обнаружения только с заданными ID.|
|dserviceids|строка/массив|Возврат только тех правил обнаружения, которые создали заданные обнаруженные сервисы.|
|selectDChecks|запрос|Возврат проверок обнаружения, которые используются правилом обнаружения, в свойстве `dchecks`.<br><br>Поддерживает `count`.|
|selectDHosts|запрос|Возврат обнаруженных узлов сети, которые были созданы правилом обнаружения, в свойстве `dhosts`.<br><br>Поддерживает `count`.|
|limitSelects|целое число|Ограничение количества записей, возвращаемых подзапросами.<br><br>Применимо только к следующим подзапросам:<br>`selectDChecks` - результаты отсортируются по `dcheckid`;<br>`selectDHosts` - результаты отсортируются по `dhostsid`.|
|sortfield|строка/массив|Сортировка результата в соответствии с заданными свойствами.<br><br>Возможные значения: `druleid` и `name`.|
|countOutput|логический|Эти параметры являются общими для всех методов `get` и они описаны в [справочных комментариях](/ru/manual/api/reference_commentary#общие_параметры_get_метода).|
|editable|логическое|^|
|excludeSearch|логический|^|
|filter|объект|^|
|limit|целое число|^|
|output|запрос|^|
|preservekeys|логический|^|
|search|объект|^|
|searchByAny|логическое|^|
|searchWildcardsEnabled|логическое|^|
|sortorder|строка/массив|^|
|startSearch|логический|^|

[comment]: # ({/new-0b971cc5})

[comment]: # ({new-7223bab1})
### Возвращаемые значения

`(целое число/массив)` Возвращает либо:

-   массив объектов;
-   количество найденных объектов, если используется параметр
    `countOutput`.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-153ca11b})
#### Получение всех правил обнаружения

Получение всех настроенных правил обнаружения и проверок обнаружения,
которые они используют.

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "drule.get",
    "params": {
        "output": "extend",
        "selectDChecks": "extend"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "druleid": "2",
            "proxy_hostid": "0",
            "name": "Local network",
            "iprange": "192.168.3.1-255",
            "delay": "5s",
            "nextcheck": "1348754327",
            "status": "0",
            "dchecks": [
                {
                    "dcheckid": "7",
                    "druleid": "2",
                    "type": "3",
                    "key_": "",
                    "snmp_community": "",
                    "ports": "21",
                    "snmpv3_securityname": "",
                    "snmpv3_securitylevel": "0",
                    "snmpv3_authpassphrase": "",
                    "snmpv3_privpassphrase": "",
                    "uniq": "0",
                    "snmpv3_authprotocol": "0",
                    "snmpv3_privprotocol": "0"
                },
                {
                    "dcheckid": "8",
                    "druleid": "2",
                    "type": "4",
                    "key_": "",
                    "snmp_community": "",
                    "ports": "80",
                    "snmpv3_securityname": "",
                    "snmpv3_securitylevel": "0",
                    "snmpv3_authpassphrase": "",
                    "snmpv3_privpassphrase": "",
                    "uniq": "0",
                    "snmpv3_authprotocol": "0",
                    "snmpv3_privprotocol": "0"
                }
            ]
        },
        {
            "druleid": "6",
            "proxy_hostid": "0",
            "name": "Zabbix agent discovery",
            "iprange": "192.168.1.1-255",
            "delay": "1h",
            "nextcheck": "0",
            "status": "0",
            "dchecks": [
                {
                    "dcheckid": "10",
                    "druleid": "6",
                    "type": "9",
                    "key_": "system.uname",
                    "snmp_community": "",
                    "ports": "10050",
                    "snmpv3_securityname": "",
                    "snmpv3_securitylevel": "0",
                    "snmpv3_authpassphrase": "",
                    "snmpv3_privpassphrase": "",
                    "uniq": "0",
                    "snmpv3_authprotocol": "0",
                    "snmpv3_privprotocol": "0"
                }
            ]
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-153ca11b})

[comment]: # ({new-f2f964c1})
### Смотрите также

-   [Обнаруженный узел
    сети](/ru/manual/api/reference/dhost/object#обнаруженный_узел)
-   [Проверка
    обнаружения](/ru/manual/api/reference/dcheck/object#проверка_обнаружения)

[comment]: # ({/new-f2f964c1})

[comment]: # ({new-365d15ae})
### Исходный код

CDRule::get() в *frontends/php/include/classes/api/services/CDRule.php*.

[comment]: # ({/new-365d15ae})
