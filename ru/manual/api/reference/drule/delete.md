[comment]: # translation:outdated

[comment]: # ({new-f125871b})
# drule.delete

[comment]: # ({/new-f125871b})

[comment]: # ({new-2430e072})
### Описание

`объект drule.delete(массив discoveryRuleIds)`

Этот метод позволяет удалять правила обнаружения.

[comment]: # ({/new-2430e072})

[comment]: # ({new-5b654054})
### Параметры

`(массив)` ID удаляемых правил обнаружения.

[comment]: # ({/new-5b654054})

[comment]: # ({new-35b8a89c})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID удаленных правил
обнаружения под свойством `druleids`.

[comment]: # ({/new-35b8a89c})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-4df0ae86})
#### Удаление нескольких правил обнаружения

Удаление двух правил обнаружения.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "drule.delete",
    "params": [
        "4",
        "6"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "druleids": [
            "4",
            "6"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-4df0ae86})

[comment]: # ({new-89517129})
### Исходный код

CDRule::delete() в
*frontends/php/include/classes/api/services/CDRule.php*.

[comment]: # ({/new-89517129})
