[comment]: # translation:outdated

[comment]: # ({new-ca37dbd6})
# Задача

Этот класс предназначен для работы с задачами.

Доступные методы:\

-   [task.create](/ru/manual/api/reference/task/create) - создание новых
    задач

[comment]: # ({/new-ca37dbd6})
