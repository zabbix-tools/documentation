[comment]: # translation:outdated

[comment]: # ({new-6525f8c0})
# История

Этот класс предназначен для работы с данными истории.

Справка по объектам:\

-   [История](/ru/manual/api/reference/history/object#история)

Доступные методы:\

-   [history.get](/ru/manual/api/reference/history/get) - получение
    данных истории.

[comment]: # ({/new-6525f8c0})
