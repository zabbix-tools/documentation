[comment]: # translation:outdated

[comment]: # ({new-e73a60a4})
# > Authentication object

The following object are directly related to the `authentication` API.

[comment]: # ({/new-e73a60a4})

[comment]: # ({new-2df9176c})
### Authentication

The authentication object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|authentication\_type|integer|Default authentication.<br><br>Possible values:<br>0 - *(default)* Internal;<br>1 - LDAP.|
|http\_auth\_enabled|integer|Enable HTTP authentication.<br><br>Possible values:<br>0 - *(default)* Disable;<br>1 - Enable.|
|http\_login\_form|integer|Default login form.<br><br>Possible values:<br>0 - *(default)* Zabbix login form;<br>1 - HTTP login form.|
|http\_strip\_domains|string|Remove domain name.|
|http\_case\_sensitive|integer|HTTP case sensitive login.<br><br>Possible values:<br>0 - Off;<br>1 - *(default)* On.|
|ldap\_configured|integer|Enable LDAP authentication.<br><br>Possible values:<br>0 - Disable;<br>1 - *(default)* Enable.|
|ldap\_host|string|LDAP host.|
|ldap\_port|integer|LDAP port.|
|ldap\_base\_dn|string|LDAP base DN.|
|ldap\_search\_attribute|string|LDAP search attribute.|
|ldap\_bind\_dn|string|LDAP bind DN.|
|ldap\_case\_sensitive|integer|LDAP case sensitive login.<br><br>Possible values:<br>0 - Off;<br>1 - *(default)* On.|
|ldap\_bind\_password|string|LDAP bind password.|
|saml\_auth\_enabled|integer|Enable SAML authentication.<br><br>Possible values:<br>0 - *(default)* Disable;<br>1 - Enable.|
|saml\_idp\_entityid|string|SAML IdP entity ID.|
|saml\_sso\_url|string|SAML SSO service URL.|
|saml\_slo\_url|string|SAML SLO service URL.|
|saml\_username\_attribute|string|SAML username attribute.|
|saml\_sp\_entityid|string|SAML SP entity ID.|
|saml\_nameid\_format|string|SAML SP name ID format.|
|saml\_sign\_messages|integer|SAML sign messages.<br><br>Possible values:<br>0 - *(default)* Do not sign messages;<br>1 - Sign messages.|
|saml\_sign\_assertions|integer|SAML sign assertions.<br><br>Possible values:<br>0 - *(default)* Do not sign assertations;<br>1 - Sign assertations.|
|saml\_sign\_authn\_requests|integer|SAML sign AuthN requests.<br><br>Possible values:<br>0 - *(default)* Do not sign AuthN requests;<br>1 - Sign AuthN requests.|
|saml\_sign\_logout\_requests|integer|SAML sign logout requests.<br><br>Possible values:<br>0 - *(default)* Do not sign logout requests;<br>1 - Sign logout requests.|
|saml\_sign\_logout\_responses|integer|SAML sign logout responses.<br><br>Possible values:<br>0 - *(default)* Do not sign logout responses;<br>1 - Sign logout responses.|
|saml\_encrypt\_nameid|integer|SAML encrypt name ID.<br><br>Possible values:<br>0 - *(default)* Do not encrypt name ID;<br>1 - Encrypt name ID.|
|saml\_encrypt\_assertions|integer|SAML encrypt assertions.<br><br>Possible values:<br>0 - *(default)* Do not encrypt assertions;<br>1 - Encrypt assertions.|
|saml\_case\_sensitive|integer|SAML case sensitive login.<br><br>Possible values:<br>0 - Off;<br>1 - *(default)* On.|
|passwd\_min\_length|integer|Password minimal length requirement.<br><br>Possible range of values: 1-70<br>8 - *default*|
|passwd\_check\_rules|integer|Password checking rules.<br><br>Possible bitmap values are:<br>0 - check password length;<br>1 - check if password uses uppercase and lowercase Latin letters;<br>2 - check if password uses digits;<br>4 - check if password uses special characters;<br>8 - *(default)* check if password is not in the list of commonly used passwords, does not contain derivations of word "Zabbix" or user's name, last name or username.|

[comment]: # ({/new-2df9176c})
