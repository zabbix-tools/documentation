[comment]: # translation:outdated

[comment]: # ({new-83b3ff24})
# usermacro.create

[comment]: # ({/new-83b3ff24})

[comment]: # ({new-db3d5c68})
### Описание

`объект usermacro.create(объект/массив МакросыУзласети)`

Этот метод позволяет создавать новые макросы узла сети.

[comment]: # ({/new-db3d5c68})

[comment]: # ({new-7c7df9b5})
### Параметры

`(объект/массив)` Создаваемые макросы узла сети.

Этот метод принимает макросы узла сети со [стандартными свойствами
макроса узла сети](object#макрос_узла_сети).

[comment]: # ({/new-7c7df9b5})

[comment]: # ({new-bd0c3c49})
### Возващаемые значения

`(объект)` Возвращает объект, который содержит ID созданных макросов
узла сети под свойством `hostmacroids`. Порядок возвращаемых ID
совпадает с порядком переданных макросов узла сети.

[comment]: # ({/new-bd0c3c49})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-7c0da88a})
#### Создание макроса узла сети

Создание макроса узла сети "{$SNMP\_COMMUNITY}" со значением "public" на
узле сети "10198".

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "usermacro.create",
    "params": {
        "hostid": "10198",
        "macro": "{$SNMP_COMMUNITY}",
        "value": "public"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "hostmacroids": [
            "11"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-7c0da88a})

[comment]: # ({new-084b8f3c})
### Исходный код

CUserMacro::create() в
*frontends/php/include/classes/api/services/CUserMacro.php*.

[comment]: # ({/new-084b8f3c})
