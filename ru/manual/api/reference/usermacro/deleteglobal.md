[comment]: # translation:outdated

[comment]: # ({new-e91a170d})
# usermacro.deleteglobal

[comment]: # ({/new-e91a170d})

[comment]: # ({new-f33727ba})
### Описание

`объект usermacro.deleteglobal(массив globalMacroIds)`

Этот метод позволяет удалять глобальные макросы.

[comment]: # ({/new-f33727ba})

[comment]: # ({new-0a9d3dbc})
### Параметры

`(массив)` ID удаляемых глобальных макросов.

[comment]: # ({/new-0a9d3dbc})

[comment]: # ({new-30415b7d})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID удаленных глобальных
макросов под свойством `globalmacroids`.

[comment]: # ({/new-30415b7d})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-b377e629})
#### Удаление нескольких глобальных макросов

Удаление двух глобальных макросов.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "usermacro.deleteglobal",
    "params": [
        "32",
        "11"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "globalmacroids": [
            "32",
            "11"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-b377e629})

[comment]: # ({new-331322ff})
### Исходный код

CUserMacro::deleteGlobal() в
*frontends/php/include/classes/api/services/CUserMacro.php*.

[comment]: # ({/new-331322ff})
