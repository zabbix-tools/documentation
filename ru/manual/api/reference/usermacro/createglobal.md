[comment]: # translation:outdated

[comment]: # ({new-f1e165e2})
# usermacro.createglobal

[comment]: # ({/new-f1e165e2})

[comment]: # ({new-2655f9fe})
### Описание

`объект usermacro.createglobal(объект/массив ГлобальныеМакросы)`

Этот метод позволяет создавать новые глобальные макросы.

[comment]: # ({/new-2655f9fe})

[comment]: # ({new-3bf123cd})
### Параметры

`(объект/массив)` Создаваемые глобальные макросы.

Этот метод принимает глобальные макросы со [стандартными свойствами
глобального макроса](object#глобальный_макрос).

[comment]: # ({/new-3bf123cd})

[comment]: # ({new-0b8f323a})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID созданных глобальных
макросов под свойством `globalmacroids`. Порядок возвращаемых ID
совпадает с порядком переданных глобальных макросов.

[comment]: # ({/new-0b8f323a})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-752894f1})
#### Создание глобального макроса

Создание глобального макроса "{$SNMP\_COMMUNITY}" со значением "public".

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "usermacro.createglobal",
    "params":  {
        "macro": "{$SNMP_COMMUNITY}",
        "value": "public"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "globalmacroids": [
            "6"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-752894f1})

[comment]: # ({new-7b4f640f})
### Исходный код

CUserMacro::createGlobal() в
*frontends/php/include/classes/api/services/CUserMacro.php*.

[comment]: # ({/new-7b4f640f})
