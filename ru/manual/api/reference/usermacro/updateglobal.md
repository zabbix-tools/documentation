[comment]: # translation:outdated

[comment]: # ({new-a16741b3})
# usermacro.updateglobal

[comment]: # ({/new-a16741b3})

[comment]: # ({new-19b16b0b})
### Описание

`объект usermacro.updateglobal(объект/массив ГлобальныеМакросы)`

Этот метод позволяет обновлять существующие глобальные макросы

[comment]: # ({/new-19b16b0b})

[comment]: # ({new-be7c969e})
### Параметры

`(объект/массив)` [Свойства глобального
макроса](object#глобальный_макрос), которые будут обновлены.

Свойство `globalmacroid` должно быть указано по каждому глобальному
макросу, все остальные свойства опциональны. Будут обновлены только
переданные свойства, все остальные останутся неизменными.

[comment]: # ({/new-be7c969e})

[comment]: # ({new-9c6d43ad})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID обновленных глобальных
макросов под свойством `globalmacroids`.

[comment]: # ({/new-9c6d43ad})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-bd208b05})
#### Изменение значения глобального макроса

Изменение значения глобального макроса на "public".

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "usermacro.updateglobal",
    "params": {
        "globalmacroid": "1",
        "value": "public"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "globalmacroids": [
            "1"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-bd208b05})

[comment]: # ({new-85ca09bc})
### Исходный код

CUserMacro::updateGlobal() в
*frontends/php/include/classes/api/services/CUserMacro.php*.

[comment]: # ({/new-85ca09bc})
