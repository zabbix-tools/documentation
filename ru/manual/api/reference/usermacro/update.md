[comment]: # translation:outdated

[comment]: # ({new-2ed6e81f})
# usermacro.update

[comment]: # ({/new-2ed6e81f})

[comment]: # ({new-23e3a148})
### Описание

`объект usermacro.update(объект/массив МакросыУзлаСети)`

Этот метод позволяет обновлять существующие макросы узла сети

[comment]: # ({/new-23e3a148})

[comment]: # ({new-f8ce947c})
### Параметры

`(объект/массив)` [Свойства макроса узла сети](object#макрос_узла_сети),
которые будут обновлены.

Свойство `hostmacroid` должно быть указано по каждому макросу узла сети,
все остальные свойства опциональны. Будут обновлены только переданные
свойства, все остальные останутся неизменными.

[comment]: # ({/new-f8ce947c})

[comment]: # ({new-5e19fc38})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID обновленных макросов
узла сети под свойством `hostmacroids`.

[comment]: # ({/new-5e19fc38})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-bc8ce678})
#### Изменение значения макроса узла сети

Изменение значения макроса узла сети на "public".

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "usermacro.update",
    "params": {
        "hostmacroid": "1",
        "value": "public"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "hostmacroids": [
            "1"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-bc8ce678})

[comment]: # ({new-129f453e})
#### Change macro value that was created by discovery rule

Convert discovery rule created "automatic" macro to "manual" and change its value to "new-value".

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "usermacro.update",
    "params": {
        "hostmacroid": "1",
        "value": "new-value",
        "automatic": "0"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "hostmacroids": [
            "1"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-129f453e})

[comment]: # ({new-2eddd7d2})
### Исходный код

CUserMacro::update() в
*frontends/php/include/classes/api/services/CUserMacro.php*.

[comment]: # ({/new-2eddd7d2})
