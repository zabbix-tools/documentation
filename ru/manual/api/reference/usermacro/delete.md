[comment]: # translation:outdated

[comment]: # ({new-edb56cfa})
# usermacro.delete

[comment]: # ({/new-edb56cfa})

[comment]: # ({new-5eb2a780})
### Описание

`объект usermacro.delete(массив hostMacroIds)`

Этот метод позволяет удалять макросы узла сети.

[comment]: # ({/new-5eb2a780})

[comment]: # ({new-dd1b319f})
### Параметры

`(массив)` ID удаляемых макросов узла сети.

[comment]: # ({/new-dd1b319f})

[comment]: # ({new-cd694080})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID удаленных макросов
узла сети под свойством `hostmacroids`.

[comment]: # ({/new-cd694080})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-b2f5bee1})
#### Удаление нескольких макросов узла сети

Удаление двух макросов узла сети.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "usermacro.delete",
    "params": [
        "32",
        "11"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "hostmacroids": [
            "32",
            "11"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-b2f5bee1})

[comment]: # ({new-9d01c95a})
### Исходный код

CUserMacro::delete() в
*frontends/php/include/classes/api/services/CUserMacro.php*.

[comment]: # ({/new-9d01c95a})
