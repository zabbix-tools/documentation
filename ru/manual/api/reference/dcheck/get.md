[comment]: # translation:outdated

[comment]: # ({new-25145295})
# dcheck.get

`целое число/массив dcheck.get(объект параметры)`

Этот метод позволяет получать проверки обнаружения в соответствии с
заданными параметрами.

[comment]: # ({/new-25145295})

[comment]: # ({new-cf2cd01b})
### Параметры

`(объект)` Параметры задают желаемый вывод.

Этот метод поддерживает следующие параметры.

|Параметр|Тип|Описание|
|----------------|------|----------------|
|dcheckids|строка/массив|Возврат проверок обнаружения только с заданными ID.|
|druleids|строка/массив|Возврат только тех проверок обнаружения, которые принадлежат заданным правилам обнаружения.|
|dserviceids|строка/массив|Возврат только тех проверок обнаружения, которые нашли заданные обнаруженные сервисы.|
|sortfield|строка/массив|Сортировка результата в соответствии с заданными свойствами.<br><br>Возможные значения: `dcheckid` и `druleid`.|
|countOutput|логический|Эти параметры являются общими для всех методов `get` и они описаны в [справочных комментариях](/ru/manual/api/reference_commentary#общие_параметры_get_метода).|
|editable|логический|^|
|excludeSearch|логический|^|
|filter|объект|^|
|limit|целое число|^|
|output|запрос|^|
|preservekeys|логический|^|
|search|объект|^|
|searchByAny|логический|^|
|searchWildcardsEnabled|логический|^|
|sortorder|строка/массив|^|
|startSearch|логический|^|

[comment]: # ({/new-cf2cd01b})

[comment]: # ({new-3db1bd90})
### Возвращаемые значения

`(целое число/массив)` Возвращает либо:

-   массив объектов;
-   количество найденных объектов, если используется параметр
    `countOutput`.

[comment]: # ({/new-3db1bd90})

[comment]: # ({new-7223bab1})
### Примеры

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
#### Получение проверок обнаружения одного правила обнаружения

Получение всех проверок обнаружения, которые используются правилом
обнаружения "6".

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "dcheck.get",
    "params": {
        "output": "extend",
        "dcheckids": "6"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "dcheckid": "6",
            "druleid": "4",
            "type": "3",
            "key_": "",
            "snmp_community": "",
            "ports": "21",
            "snmpv3_securityname": "",
            "snmpv3_securitylevel": "0",
            "snmpv3_authpassphrase": "",
            "snmpv3_privpassphrase": "",
            "uniq": "0",
            "snmpv3_authprotocol": "0",
            "snmpv3_privprotocol": "0"
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-b41637d2})

[comment]: # ({new-90f6afd9})
### Исходный код

CDCheck::get() в
*frontends/php/include/classes/api/services/CDCheck.php*.

[comment]: # ({/new-90f6afd9})


[comment]: # ({new-d1a6b685})
### Source

CDCheck::get() in *ui/include/classes/api/services/CDCheck.php*.

[comment]: # ({/new-d1a6b685})
