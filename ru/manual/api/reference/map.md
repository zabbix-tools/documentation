[comment]: # translation:outdated

[comment]: # ({new-79c270c0})
# Карта сети

Этот класс предназначен для работы с картами сетей.

Справка по объектам:\

-   [Карта сети](/ru/manual/api/reference/map/object#карта_сети)
-   [Элемент карты](/ru/manual/api/reference/map/object#элемент_карты)
-   [Связь карты](/ru/manual/api/reference/map/object#связь_карты)
-   [URL карты](/ru/manual/api/reference/map/object#url_карты)
-   [Пользователь
    карты](/ru/manual/api/reference/map/object#пользователь_карты)
-   [Группа пользователей
    карты](/ru/manual/api/reference/map/object#группа_пользователей_карты)
-   [Фигура карты](/ru/manual/api/reference/map/object#фигуры_карты)
-   [Линия карты](/ru/manual/api/reference/map/object#линии_карты)

Доступные методы:\

-   [map.create](/ru/manual/api/reference/map/create) - создание новых
    карт сетей
-   [map.delete](/ru/manual/api/reference/map/delete) - удаление карт
    сетей
-   [map.get](/ru/manual/api/reference/map/get) - получение карт сетей
-   [map.update](/ru/manual/api/reference/map/update) - обновление карт
    сетей

[comment]: # ({/new-79c270c0})
