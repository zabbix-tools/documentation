[comment]: # translation:outdated

[comment]: # ({new-34f25b55})
# apiinfo.version

[comment]: # ({/new-34f25b55})

[comment]: # ({new-bc32199a})
### Описание

`строка apiinfo.version(массив)`

Этот метод позволяет получить версию Zabbix API.

[comment]: # ({/new-bc32199a})

[comment]: # ({new-4fa8a419})
### Параметры

::: noteimportant
Этот метод доступен пользователям не прошедшим
аутентификацию и его необходимо вызывать без параметра `auth` в JSON-RPC
запросе.
:::

`(массив)` Этот метод принимает пустой массив.

[comment]: # ({/new-4fa8a419})

[comment]: # ({new-53521d11})
### Возвращаемые значения

`(строка)` Возвращает версию Zabbix API.

::: notetip
Начиная с Zabbix 2.0.4 версия API совпадает с версией
Zabbix.
:::

[comment]: # ({/new-53521d11})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-6ba4bdd0})
#### Получение версии API

Получение версии Zabbix API.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "apiinfo.version",
    "params": [],
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": "4.0.0",
    "id": 1
}
```

[comment]: # ({/new-6ba4bdd0})

[comment]: # ({new-25499b5e})
### Исходный код

CAPIInfo::version() в
*frontends/php/include/classes/api/services/CAPIInfo.php*.

[comment]: # ({/new-25499b5e})
