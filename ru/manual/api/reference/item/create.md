[comment]: # translation:outdated

[comment]: # ({new-959fe29c})
# item.create

[comment]: # ({/new-959fe29c})

[comment]: # ({new-e3078f54})
### Описание

`объект item.create(объект/массив элементы данных)`

Этот метод позволяет создавать новые элементы данных.

::: noteclassic
Веб элементы данных нельзя создавать через Zabbix
API.
:::

[comment]: # ({/new-e3078f54})

[comment]: # ({new-777e8bec})
### Параметры

`(объект/массив)` Создаваемые элементы данных.

В дополнение к [стандартным свойствам элемента
данных](object#элемент_данных), этот метод принимает следующие
параметры.

|Параметр|Тип|Описание|
|----------------|------|----------------|
|applications|массив|ID добавляемых групп элементов данных к элементам данных.|
|preprocessing|массив|Опции предварительной обработки элемента данных.|

[comment]: # ({/new-777e8bec})

[comment]: # ({new-632959fe})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID созданных элементов
данных под свойством `itemids`. Порядок возвращаемых ID совпадает с
порядком переданных элементов данных.

[comment]: # ({/new-632959fe})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-6ffb1f45})
#### Создание элемента данных

Создание числового элемента данных с типом Zabbix агент для наблюдения
за свободным дисковым пространством на узле сети с ID "30074" и
добавление этого элемента данных в две группы элементов данных.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "item.create",
    "params": {
        "name": "Free disk space on $1",
        "key_": "vfs.fs.size[/home/joe/,free]",
        "hostid": "30074",
        "type": 0,
        "value_type": 3,
        "interfaceid": "30084",
        "applications": [
            "609",
            "610"
        ],
        "delay": "30s"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "24758"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-6ffb1f45})

[comment]: # ({new-0f29886e})
#### Создание элемента данных инвентаря узла сети

Создание элемента данных с типом Zabbix агент, который будет заполнять
поле "ОС" инвентарных данных узла сети.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "item.create",
    "params": {
        "name": "uname",
        "key_": "system.uname",
        "hostid": "30021",
        "type": 0,
        "interfaceid": "30007",
        "value_type": 1,
        "delay": "10s",
        "inventory_link": 5
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "24759"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-0f29886e})

[comment]: # ({new-f45b1a9a})
#### Создание элемента данных с предобработкой

Создание элемента данных, который использует пользовательский множитель.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "item.create",
    "params": {
        "name": "Device uptime",
        "key_": "sysUpTime",
        "hostid": "11312",
        "type": 4,
        "snmp_community": "{$SNMP_COMMUNITY}",
        "snmp_oid": "SNMPv2-MIB::sysUpTime.0",
        "value_type": 1,
        "delay": "60s",
        "units": "uptime",
        "interfaceid": "1156",
        "preprocessing": [
            {
                "type": "1",
                "params": "0.01"
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "44210"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-f45b1a9a})

[comment]: # ({new-6a32a64f})
#### Создание зависимого элемента данных

Создание зависимого элемента данных от основного элемента данных с ID
24759. Зависимости разрешены только в пределах одного узла сети, поэтому
у основного и зависимого элементов данных должен быть одинаковый hostid.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "item.create",
    "params": {
      "hostid": "30074",
      "name": "Dependent test item",
      "key_": "dependent.item",
      "type": "18",
      "master_itemid": "24759",
      "value_type": "2"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "44211"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-6a32a64f})

[comment]: # ({new-70eb9eae})
#### Создание элемента данных HTTP агента

Создание элемента данных с методом POST запроса и с предобработкой JSON
ответа.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "item.create",
    "params": {
        "url":"http://127.0.0.1/http.php",
        "query_fields": [
            {
                "mode":"json"
            },
            {
                "min": "10"
            },
            {
                "max": "100"
            }
        ],
        "interfaceid": "1",
        "type":"19",
        "hostid":"10254",
        "delay":"5s",
        "key_":"json",
        "name":"http agent example JSON",
        "value_type":"0",
        "output_format":"1",
        "preprocessing": [
            {
                "type": "12",
                "params": "$.random"
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 2
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "23865"
        ]
    },
    "id": 3
}
```

[comment]: # ({/new-70eb9eae})

[comment]: # ({new-313c9999})
### Исходный код

CItem::create() в *ui/include/classes/api/services/CItem.php*.

[comment]: # ({/new-313c9999})


[comment]: # ({new-ecb97b1d})
### Source

CItem::create() in *ui/include/classes/api/services/CItem.php*.

[comment]: # ({/new-ecb97b1d})
