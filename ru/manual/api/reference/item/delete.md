[comment]: # translation:outdated

[comment]: # ({new-cd0e20e4})
# item.delete

[comment]: # ({/new-cd0e20e4})

[comment]: # ({new-16951644})
### Описание

`объект item.delete(массив itemIds)`

Этот метод позволяет удалять элементы данных.

::: noteclassic
Веб элементы данных нельзя удалить через Zabbix
API.
:::

[comment]: # ({/new-16951644})

[comment]: # ({new-3574199c})
### Параметры

`(массив)` ID удаляемых элементов данных.

[comment]: # ({/new-3574199c})

[comment]: # ({new-b676662e})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID удаленных элементов
данных под свойством `itemids`.

[comment]: # ({/new-b676662e})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-92fe5da6})
#### Удаление нескольких элементов данных

Удаление двух элементов данных.\
Зависимые элементы данных и прототипы элементов данных автоматически
удаляются при удалении основного элемента данных.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "item.delete",
    "params": [
        "22982",
        "22986"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "22982",
            "22986"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-92fe5da6})

[comment]: # ({new-4c7d30be})
### Исходный код

CItem::delete() в
*frontends/php/include/classes/api/services/CItem.php*.

[comment]: # ({/new-4c7d30be})
