[comment]: # translation:outdated

[comment]: # ({new-0ec30c22})
# item.update

[comment]: # ({/new-0ec30c22})

[comment]: # ({new-38961105})
### Описание

`объект item.update(объект/массив элементы данных)`

Этот метод позволяет обновлять существующие элементы данных.

::: noteclassic
Веб элементы данных нельзя обновлять через Zabbix
API.
:::

[comment]: # ({/new-38961105})

[comment]: # ({new-4e151fd6})
### Параметры

`(объект/массив)` Свойства элементов данных, которые будут обновлены.

Свойство `itemid` должно быть указано по каждому элементу данных, все
остальные свойства опциональны. Будут обновлены только переданные
свойства, все остальные останутся неизменными.

В дополнение к [стандартным свойствам элемента
данных](object#элемент_данных), этот метод принимает следующие
параметры.

|Параметр|Тип|Описание|
|----------------|------|----------------|
|applications|массив|ID групп элементов данных, которые заменят текущие группы элементов данных.|
|preprocessing|массив|Опции предобработки элемента данных, которые заменят текущие опции предварительной обработки.|

[comment]: # ({/new-4e151fd6})

[comment]: # ({new-aefff750})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID обновленных элементов
данных под свойством `itemids`.

[comment]: # ({/new-aefff750})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-5388578b})
#### Активация элемента данных

Активация элемента данных, то есть изменение его состояния на значение
"0".

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "item.update",
    "params": {
        "itemid": "10092",
        "status": 0
    },
    "auth": "700ca65537074ec963db7efabda78259",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "10092"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-5388578b})

[comment]: # ({new-0bf89382})
#### Обновление зависимого элемента данных

Обновление имени и ID основного элемента данных у зависимого элемента
данных. Зависимости разрешены только в пределах одного узла сети,
поэтому у основного и зависимого элементов данных должен быть одинаковый
hostid.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "item.update",
    "params": {
        "name": "Dependent item updated name",
        "master_itemid": "25562",
        "itemid": "189019"
    },
    "auth": "700ca65537074ec963db7efabda78259",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "189019"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-0bf89382})

[comment]: # ({new-8f365363})
#### Обновление элемента данных HTTP агент

Активация трапов получения значений элемента данных.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "item.update",
    "params": {
        "itemid": "23856",
        "allow_traps": "1"
    },
    "auth": "700ca65537074ec963db7efabda78259",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "23856"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-8f365363})

[comment]: # ({new-44f323b8})
### Исходный код

CItem::update() в
*frontends/php/include/classes/api/services/CItem.php*.

[comment]: # ({/new-44f323b8})



[comment]: # ({new-dcfb98a0})
#### Updating a script item

Update a script item with a different script and remove unnecessary
parameters that were used by previous script.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "item.update",
    "params": {
        "itemid": "23865",
        "parameters": [],
        "script": "Zabbix.Log(3, 'Log test');\nreturn 1;"
    },
    "auth": "700ca65537074ec963db7efabda78259",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "23865"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-dcfb98a0})

[comment]: # ({new-646fe32a})
### Source

CItem::update() in *ui/include/classes/api/services/CItem.php*.

[comment]: # ({/new-646fe32a})
