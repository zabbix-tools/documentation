[comment]: # translation:outdated

[comment]: # ({new-58fbd3bc})
# item.get

[comment]: # ({/new-58fbd3bc})

[comment]: # ({new-673edf7f})
### Описание

`целое число/массив item.get(объект параметры)`

Этот метод позволяет получать элементы данных в соответствии с заданными
параметрами.

[comment]: # ({/new-673edf7f})

[comment]: # ({new-cc946363})
### Параметры

`(объект)` Параметры задают желаемый вывод.

Этот метод поддерживает следующие параметры.

|Параметр|Тип|Описание|
|----------------|------|----------------|
|itemids|строка/массив|Возврат элементов данных только с заданными ID.|
|groupids|строка/массив|Возврат только тех элементов данных, которые принадлежат узлам сети с заданных групп узлов сети.|
|templateids|строка/массив|Возврат только тех элементов данных, которые принадлежат заданным шаблонам.|
|hostids|строка/массив|Возврат только тех элементов данных, которые принадлежат заданным узлам сети.|
|proxyids|строка/массив|Возврат только тех элементов данных, которые наблюдаются заданными прокси.|
|interfaceids|строка/массив|Возврат только тех элементов данных, которые используют заданные интерфейсы узлов сети.|
|graphids|строка/массив|Возврат только тех элементов данных, которые используются в заданных графиках.|
|triggerids|строка/массив|Возврат только тех элементов данных, которые используются в заданных триггерах.|
|applicationids|строка/массив|Возврат только тех элементов данных, которые входят в заданные группы элементов данных.|
|webitems|флаг|Включение в результат веб элементов данных.|
|inherited|логический|Если задано значение `true`, возвращать только те элементы данных, которые унаследованы из шаблона.|
|templated|логический|Если задано значение `true`, возвращать только те элементы данных, которые принадлежат шаблонам.|
|monitored|логический|Если задано значение `true`, возвращать только активированные элементы данных, которые принадлежат узлам сети под наблюдением.|
|group|строка|Возврат только тех элементов данных, которые принадлежат группе с заданным именем.|
|host|строка|Возврат только тех элементов данных, которые принадлежат узлу сети с заданным именем.|
|application|строка|Возврат только тех элементов данных, которые входят в группу элементов данных с заданным именем.|
|with\_triggers|логический|Если задано значение `true`, возвращать только те элементы данных, которые используются в триггерах.|
|selectHosts|запрос|Возврат узла сети, которому принадлежит элемент данных, в виде массива в свойстве `hosts`.|
|selectInterfaces|запрос|Возврат интерфейса узла сети, который используется элементом данных, в виде массива в свойстве `interfaces`.|
|selectTriggers|запрос|Возврат триггеров, которые используют элемент данных, в свойстве `triggers`.<br><br>Поддерживается `count`.|
|selectGraphs|запрос|Возврат графиков, которые содержат элемент данных, в свойстве `graphs`.<br><br>Поддерживается `count`.|
|selectApplications|запрос|Возврат групп элементов данных, которым принадлежит элемент данных, в свойстве `applications`.|
|selectDiscoveryRule|запрос|Возврат LLD правила, которое создало элемент данных, в свойстве `discoveryRule`.|
|selectItemDiscovery|запрос|Возврат объекта обнаружения элемента данных в свойстве `itemDiscovery`.<br><br>Объект обнаружения элемента данных связывает элемент данных с прототипом элементов данных и имеет следующие свойства:<br>`itemdiscoveryid` - `(строка)` ID обнаружения элемента данных;<br>`itemid` - `(строка)` ID обнаруженного элемента данных;<br>`parent_itemid` - `(строка)` ID прототипа элемента данных с которого был создан элемент данных;<br>`key_` - `(строка)` ключ прототипа элемента данных;<br>`lastcheck` - `(штамп времени)` время, когда элемент данных был в последний раз обнаружен;<br>`ts_delete` - `(штамп времени)` время, когда более необнаруживаемый элемент данных будет удален.|
|selectPreprocessing|запрос|Возврат опций предобработки элемента данных в свойстве `preprocessing`.<br><br>Этот параметр имеет следующие свойства:<br>`type` - `(строка)` Типы опций предобработки:<br>1 - Пользовательский множитель;<br>2 - Обрезка справа;<br>3 - Обрезка слева;<br>4 - Обрезка;<br>5 - Соответствие регулярному выражению;<br>6 - Двоичное в десятичное;<br>7 - Восьмеричное в десятичное;<br>8 - Шестнадцатеричное в десятичное;<br>9 - Простое изменение;<br>10 - Изменение в секунду.<br><br>`params` - `(строка)` Дополнительные параметры используемые опцией предварительной обработки. Несколько параметров разделяются символом LF (\\n).|
|filter|объект|Возврат только тех результатов, которые в точности соответствуют заданному фильтру.<br><br>Принимает массив, где ключи являются именами свойств и значения, которые являются либо одним значением, либо массивом сопоставляемых значений.<br><br>Поддерживает дополнительные фильтры:<br>`host` - техническое имя узла сети, которому принадлежит элемент данных.|
|limitSelects|целое число|Ограничение количества записей, возвращаемых подзапросами.<br><br>Применимо только к следующим подзапросам:<br>`selectGraphs` - результаты сортируются по `name`;<br>`selectTriggers` - результаты сортируются по `description`.|
|sortfield|строка/массив|Сортировка результата в соответствии с заданными свойствами.<br><br>Возможные значения: `itemid`, `name`, `key_`, `delay`, `history`, `trends`, `type` и `status`.|
|countOutput|логический|Эти параметры являются общими для всех методов `get` и они описаны в [справочных комментариях](/ru/manual/api/reference_commentary#общие_параметры_get_метода).|
|editable|логический|^|
|excludeSearch|логический|^|
|limit|целое число|^|
|output|запрос|^|
|preservekeys|логический|^|
|search|объект|^|
|searchByAny|логический|^|
|searchWildcardsEnabled|логический|^|
|sortorder|строка/массив|^|
|startSearch|логический|^|

[comment]: # ({/new-cc946363})

[comment]: # ({new-7223bab1})
### Возвращаемые значения

`(целое число/массив)` Возвращает либо:

-   массив объектов;
-   количество найденных объектов, если используется параметр
    `countOutput`.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-59b84d93})
#### Поиск элементов данных по ключу

Получение всех элементов данных с узлов сети с ID "10084", которые имеют
в ключе слово "system" и сортировка результата по имени.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "item.get",
    "params": {
        "output": "extend",
        "hostids": "10084",
        "search": {
            "key_": "system"
        },
        "sortfield": "name"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "itemid": "23298",
            "type": "0",
            "snmp_community": "",
            "snmp_oid": "",
            "hostid": "10084",
            "name": "Context switches per second",
            "key_": "system.cpu.switches",
            "delay": "1m",
            "history": "7d",
            "trends": "365d",
            "lastvalue": "2552",
            "lastclock": "1351090998",
            "prevvalue": "2641",
            "state": "0",
            "status": "0",
            "value_type": "3",
            "trapper_hosts": "",
            "units": "sps",
            "snmpv3_securityname": "",
            "snmpv3_securitylevel": "0",
            "snmpv3_authpassphrase": "",
            "snmpv3_privpassphrase": "",
            "snmpv3_authprotocol": "0",
            "snmpv3_privprotocol": "0",
            "snmpv3_contextname": "",
            "error": "",
            "lastlogsize": "0",
            "logtimefmt": "",
            "templateid": "22680",
            "valuemapid": "0",
            "params": "",
            "ipmi_sensor": "",
            "authtype": "0",
            "username": "",
            "password": "",
            "publickey": "",
            "privatekey": "",
            "mtime": "0",
            "lastns": "564054253",
            "flags": "0",
            "interfaceid": "1",
            "port": "",
            "description": "",
            "inventory_link": "0",
            "lifetime": "0s",
            "evaltype": "0",
            "jmx_endpoint": "",
            "master_itemid": "0",
            "timeout": "3s",
            "url": "",
            "query_fields": [],
            "posts": "",
            "status_codes": "200",
            "follow_redirects": "1",
            "post_type": "0",
            "http_proxy": "",
            "headers": [],
            "retrieve_mode": "0",
            "request_method": "0",
            "output_format": "0",
            "ssl_cert_file": "",
            "ssl_key_file": "",
            "ssl_key_password": "",
            "verify_peer": "0",
            "verify_host": "0",
            "allow_traps": "0"
        },
        {
            "itemid": "23299",
            "type": "0",
            "snmp_community": "",
            "snmp_oid": "",
            "hostid": "10084",
            "name": "CPU $2 time",
            "key_": "system.cpu.util[,idle]",
            "delay": "1m",
            "history": "7d",
            "trends": "365d",
            "lastvalue": "86.031879",
            "lastclock": "1351090999",
            "prevvalue": "85.306944",
            "state": "0",
            "status": "0",
            "value_type": "0",
            "trapper_hosts": "",
            "units": "%",
            "snmpv3_securityname": "",
            "snmpv3_securitylevel": "0",
            "snmpv3_authpassphrase": "",
            "snmpv3_privpassphrase": "",
            "snmpv3_authprotocol": "0",
            "snmpv3_privprotocol": "0",
            "snmpv3_contextname": "",
            "error": "",
            "lastlogsize": "0",
            "logtimefmt": "",
            "templateid": "17354",
            "valuemapid": "0",
            "params": "",
            "ipmi_sensor": "",
            "authtype": "0",
            "username": "",
            "password": "",
            "publickey": "",
            "privatekey": "",
            "mtime": "0",
            "lastns": "564256864",
            "flags": "0",
            "interfaceid": "1",
            "port": "",
            "description": "The time the CPU has spent doing nothing.",
            "inventory_link": "0",
            "lifetime": "0s",
            "evaltype": "0",
            "jmx_endpoint": "",
            "master_itemid": "0",
            "timeout": "3s",
            "url": "",
            "query_fields": [],
            "posts": "",
            "status_codes": "200",
            "follow_redirects": "1",
            "post_type": "0",
            "http_proxy": "",
            "headers": [],
            "retrieve_mode": "0",
            "request_method": "0",
            "output_format": "0",
            "ssl_cert_file": "",
            "ssl_key_file": "",
            "ssl_key_password": "",
            "verify_peer": "0",
            "verify_host": "0",
            "allow_traps": "0"
        },
        {
            "itemid": "23300",
            "type": "0",
            "snmp_community": "",
            "snmp_oid": "",
            "hostid": "10084",
            "name": "CPU $2 time",
            "key_": "system.cpu.util[,interrupt]",
            "history": "7d",
            "trends": "365d",
            "lastvalue": "0.008389",
            "lastclock": "1351091000",
            "prevvalue": "0.000000",
            "state": "0",
            "status": "0",
            "value_type": "0",
            "trapper_hosts": "",
            "units": "%",
            "snmpv3_securityname": "",
            "snmpv3_securitylevel": "0",
            "snmpv3_authpassphrase": "",
            "snmpv3_privpassphrase": "",
            "snmpv3_authprotocol": "0",
            "snmpv3_privprotocol": "0",
            "snmpv3_contextname": "",
            "error": "",
            "lastlogsize": "0",
            "logtimefmt": "",
            "templateid": "22671",
            "valuemapid": "0",
            "params": "",
            "ipmi_sensor": "",
            "authtype": "0",
            "username": "",
            "password": "",
            "publickey": "",
            "privatekey": "",
            "mtime": "0",
            "lastns": "564661387",
            "flags": "0",
            "interfaceid": "1",
            "port": "",
            "description": "The amount of time the CPU has been servicing hardware interrupts.",
            "inventory_link": "0",
            "lifetime": "0s",
            "evaltype": "0",
            "jmx_endpoint": "",
            "master_itemid": "0",
            "timeout": "3s",
            "url": "",
            "query_fields": [],
            "posts": "",
            "status_codes": "200",
            "follow_redirects": "1",
            "post_type": "0",
            "http_proxy": "",
            "headers": [],
            "retrieve_mode": "0",
            "request_method": "0",
            "output_format": "0",
            "ssl_cert_file": "",
            "ssl_key_file": "",
            "ssl_key_password": "",
            "verify_peer": "0",
            "verify_host": "0",
            "allow_traps": "0"
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-59b84d93})

[comment]: # ({new-674b0856})
#### Поиск зависимых элементов данных по ключу

Получение всех Зависимых элементов данных с узла сети с ID "10116", у
которых в ключе имеется слово "apache".

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "item.get",
    "params": {
        "output": "extend",
        "hostids": "10116",
        "search": {
            "key_": "apache"
        },
        "filter": {
            "type": "18"
        }
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "itemid": "25550",
            "type": "18",
            "snmp_community": "",
            "snmp_oid": "",
            "hostid": "10116",
            "name": "Days",
            "key_": "apache.status.uptime.days",
            "delay": "",
            "history": "90d",
            "trends": "365d",
            "status": "0",
            "value_type": "3",
            "trapper_hosts": "",
            "units": "",
            "snmpv3_securityname": "",
            "snmpv3_securitylevel": "0",
            "snmpv3_authpassphrase": "",
            "snmpv3_privpassphrase": "",
            "formula": "",
            "error": "",
            "lastlogsize": "0",
            "logtimefmt": "",
            "templateid": "0",
            "valuemapid": "0",
            "params": "",
            "ipmi_sensor": "",
            "authtype": "0",
            "username": "",
            "password": "",
            "publickey": "",
            "privatekey": "",
            "mtime": "0",
            "flags": "0",
            "interfaceid": "0",
            "port": "",
            "description": "",
            "inventory_link": "0",
            "lifetime": "30d",
            "snmpv3_authprotocol": "0",
            "snmpv3_privprotocol": "0",
            "state": "0",
            "snmpv3_contextname": "",
            "evaltype": "0",
            "master_itemid": "25545",
            "jmx_endpoint": "",
            "timeout": "3s",
            "url": "",
            "query_fields": [],
            "posts": "",
            "status_codes": "200",
            "follow_redirects": "1",
            "post_type": "0",
            "http_proxy": "",
            "headers": [],
            "retrieve_mode": "0",
            "request_method": "0",
            "output_format": "0",
            "ssl_cert_file": "",
            "ssl_key_file": "",
            "ssl_key_password": "",
            "verify_peer": "0",
            "verify_host": "0",
            "allow_traps": "0",
            "lastclock": "0",
            "lastns": "0",
            "lastvalue": "0",
            "prevvalue": "0"
        },
        {
            "itemid": "25555",
            "type": "18",
            "snmp_community": "",
            "snmp_oid": "",
            "hostid": "10116",
            "name": "Hours",
            "key_": "apache.status.uptime.hours",
            "delay": "0",
            "history": "90d",
            "trends": "365d",
            "status": "0",
            "value_type": "3",
            "trapper_hosts": "",
            "units": "",
            "snmpv3_securityname": "",
            "snmpv3_securitylevel": "0",
            "snmpv3_authpassphrase": "",
            "snmpv3_privpassphrase": "",
            "formula": "",
            "error": "",
            "lastlogsize": "0",
            "logtimefmt": "",
            "templateid": "0",
            "valuemapid": "0",
            "params": "",
            "ipmi_sensor": "",
            "authtype": "0",
            "username": "",
            "password": "",
            "publickey": "",
            "privatekey": "",
            "mtime": "0",
            "flags": "0",
            "interfaceid": "0",
            "port": "",
            "description": "",
            "inventory_link": "0",
            "lifetime": "30d",
            "snmpv3_authprotocol": "0",
            "snmpv3_privprotocol": "0",
            "state": "0",
            "snmpv3_contextname": "",
            "evaltype": "0",
            "master_itemid": "25545",
            "jmx_endpoint": "",
            "timeout": "3s",
            "url": "",
            "query_fields": [],
            "posts": "",
            "status_codes": "200",
            "follow_redirects": "1",
            "post_type": "0",
            "http_proxy": "",
            "headers": [],
            "retrieve_mode": "0",
            "request_method": "0",
            "output_format": "0",
            "ssl_cert_file": "",
            "ssl_key_file": "",
            "ssl_key_password": "",
            "verify_peer": "0",
            "verify_host": "0",
            "allow_traps": "0",
            "lastclock": "0",
            "lastns": "0",
            "lastvalue": "0",
            "prevvalue": "0"
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-674b0856})

[comment]: # ({new-94dc4b21})
#### Поиск элемента данных HTTP агента

Поиск элемента данных HTTP агента с типом post тела XML у конкретного id
узла сети.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "item.get",
    "params": {
        "hostids": "10255",
        "filter": {
            "type": "19",
            "post_type": "3"
        }
    },
    "id": 3,
    "auth": "d678e0b85688ce578ff061bd29a20d3b"
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "itemid": "28252",
            "type": "19",
            "snmp_community": "",
            "snmp_oid": "",
            "hostid": "10255",
            "name": "template item",
            "key_": "ti",
            "delay": "30s",
            "history": "90d",
            "trends": "365d",
            "status": "0",
            "value_type": "3",
            "trapper_hosts": "",
            "units": "",
            "snmpv3_securityname": "",
            "snmpv3_securitylevel": "0",
            "snmpv3_authpassphrase": "",
            "snmpv3_privpassphrase": "",
            "formula": "",
            "error": "",
            "lastlogsize": "0",
            "logtimefmt": "",
            "templateid": "0",
            "valuemapid": "0",
            "params": "",
            "ipmi_sensor": "",
            "authtype": "0",
            "username": "",
            "password": "",
            "publickey": "",
            "privatekey": "",
            "mtime": "0",
            "flags": "0",
            "interfaceid": "0",
            "port": "",
            "description": "",
            "inventory_link": "0",
            "lifetime": "30d",
            "snmpv3_authprotocol": "0",
            "snmpv3_privprotocol": "0",
            "state": "0",
            "snmpv3_contextname": "",
            "evaltype": "0",
            "jmx_endpoint": "",
            "master_itemid": "0",
            "timeout": "3s",
            "url": "localhost",
            "query_fields": [
                {
                    "mode": "xml"
                }
            ],
            "posts": "<body>\r\n<![CDATA[{$MACRO}<foo></bar>]]>\r\n</body>",
            "status_codes": "200",
            "follow_redirects": "0",
            "post_type": "3",
            "http_proxy": "",
            "headers": [],
            "retrieve_mode": "1",
            "request_method": "3",
            "output_format": "0",
            "ssl_cert_file": "",
            "ssl_key_file": "",
            "ssl_key_password": "",
            "verify_peer": "0",
            "verify_host": "0",
            "allow_traps": "0",
            "lastclock": "0",
            "lastns": "0",
            "lastvalue": "0",
            "prevvalue": "0"
        }
    ],
    "id": 3
}
```

[comment]: # ({/new-94dc4b21})

[comment]: # ({new-f4b01bbe})
### Смотрите также

-   [Группа элементов
    данных](/ru/manual/api/reference/application/object#группа_элементов_данны)
-   [Правило
    обнаружения](/ru/manual/api/reference/discoveryrule/object#правило_обнаружения)
-   [График](/ru/manual/api/reference/graph/object#график)
-   [Узел сети](/ru/manual/api/reference/host/object#узел_сети)
-   [Интерфейс узла
    сети](/ru/manual/api/reference/hostinterface/object#интерфейс_узла_сети)
-   [Триггер](/ru/manual/api/reference/trigger/object#триггер)

[comment]: # ({/new-f4b01bbe})

[comment]: # ({new-d3276b35})
### Исходный код

CItem::get() в *frontends/php/include/classes/api/services/CItem.php*.

[comment]: # ({/new-d3276b35})


[comment]: # ({new-a6ef1d0f})
### Source

CItem::get() in *ui/include/classes/api/services/CItem.php*.

[comment]: # ({/new-a6ef1d0f})
