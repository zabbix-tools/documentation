[comment]: # translation:outdated

[comment]: # ({new-bf4e6957})
# Пользовательский макрос

Этот класс предназначен для работы с макросами узлов сети и глобальными.

Справка по объектам:\

-   [Глобальный
    макрос](/ru/manual/api/reference/usermacro/object#глобальный_макрос)
-   [Макрос узла
    сети](/ru/manual/api/reference/usermacro/object#макрос_узла_сети)

Доступные методы:\

-   [usermacro.create](/ru/manual/api/reference/usermacro/create) -
    создание новых макросов узла сети
-   [usermacro.createglobal](/ru/manual/api/reference/usermacro/createglobal) -
    создание новых глобальных макросов
-   [usermacro.delete](/ru/manual/api/reference/usermacro/delete) -
    удаление макросов узла сети
-   [usermacro.deleteglobal](/ru/manual/api/reference/usermacro/deleteglobal) -
    удаление глобальных макросов
-   [usermacro.get](/ru/manual/api/reference/usermacro/get) - получение
    макросов узла сети и глобальных макросов
-   [usermacro.update](/ru/manual/api/reference/usermacro/update) -
    обновление макросов узла сети
-   [usermacro.updateglobal](/ru/manual/api/reference/usermacro/updateglobal) -
    обновление глобальных макросов

[comment]: # ({/new-bf4e6957})
