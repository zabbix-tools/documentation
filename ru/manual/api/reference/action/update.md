[comment]: # translation:outdated

[comment]: # ({new-e4b5aac4})
# action.update

[comment]: # ({/new-e4b5aac4})

[comment]: # ({new-b1673b49})
### Описание

`объект action.update(объект/массив действий)`

Этот метод позволяет обновлять существующие действия.

[comment]: # ({/new-b1673b49})

[comment]: # ({new-703e85ce})
### Параметры

`(объект/массив)` Свойства действия, которые будут обновлены.

Свойство `actionid` должно быть указано по каждому действию, все
остальные свойства опциональны. Будут обновлены только переданные
свойства, все остальные останутся неизменными.

В дополнение к [стандартным свойствам действия](object#действие), этот
метод принимает следующие параметры.

|Параметр|Тип|Описание|
|----------------|------|----------------|
|filter|объект|Объект фильтра действия, который заменит текущий фильтр.|
|operations|массив|Операции действия, которые заменят существующие операции.|
|recovery\_operations|массив|Операции о восстановления действия, которые заменят текущие операции о восстановлении.|
|acknowledge\_operations|массив|Операции о подтверждении действия, которые заменят текущие операции о подтверждении.|

[comment]: # ({/new-703e85ce})

[comment]: # ({new-8cbaa103})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID обновленных действий
под свойством `actionids`.

[comment]: # ({/new-8cbaa103})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-77b78ec5})
#### Деактивация действия

Деактивация действия, то есть, изменение состояния на значение равное
"1".

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "action.update",
    "params": {
        "actionid": "2",
        "status": "1"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "actionids": [
            "2"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-77b78ec5})

[comment]: # ({new-755496f7})
### Смотрите также

-   [Фильтр действия](object#фильтр_действия)
-   [Операция действия](object#операция_действия)

[comment]: # ({/new-755496f7})

[comment]: # ({new-df9f1d35})
### Исходный код

CAction::update() в
*frontends/php/include/classes/api/services/CAction.php*.

[comment]: # ({/new-df9f1d35})
