[comment]: # translation:outdated

[comment]: # ({3d0c523c-3d0c523c})
# action.delete

[comment]: # ({/3d0c523c-3d0c523c})

[comment]: # ({066c28cd-c056f978})
### Описание

`обьект action.delete(массив actionIds)`

Этот метод позволяет удалять действия.

::: noteclassic
Этот метод доступен только пользователям с типом доступа *Admin* и *Super admin*.
Разрешения на вызов метода могут быть отключены в настройках роли пользователя.
Смотрите [Роли пользователей](/manual/web_interface/frontend_sections/administration/user_roles)
для дополнительной информации.
:::

[comment]: # ({/066c28cd-c056f978})

[comment]: # ({4c7063c7-f66aff82})
### Параметры

`(массив)` ID удаляемых действий

[comment]: # ({/4c7063c7-f66aff82})

[comment]: # ({38cf1c0a-04c6deae})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID удаленных действий под свойством `actionids`.

[comment]: # ({/38cf1c0a-04c6deae})

[comment]: # ({41c52ba7-b41637d2})
### Примеры

[comment]: # ({/41c52ba7-b41637d2})

[comment]: # ({0e748b6e-36abb590})
#### Удаление нескольких действий

Удаление двух действий.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "action.delete",
    "params": [
        "17",
        "18"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "actionids": [
            "17",
            "18"
        ]
    },
    "id": 1
}
```

[comment]: # ({/0e748b6e-36abb590})

[comment]: # ({7bcb11cb-70602a31})
### Исходный код

CAction::delete() в *ui/include/classes/api/services/CAction.php*.

[comment]: # ({/7bcb11cb-70602a31})
