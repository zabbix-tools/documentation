[comment]: # translation:outdated

[comment]: # ({2293b018-2293b018})
# action.get

[comment]: # ({/2293b018-2293b018})

[comment]: # ({23b4a056-42834e1b})
### Описание

`целое число/массив action.get(объект параметры)`

Этот метод позволяет получать действия согласно заданным параметрам.

::: noteclassic
Этот метод доступен всем типам пользователей.
Разрешение на использование данного метода можно отозвать в настройках роли пользователя. 
Подробнее смотрите [Роли пользователей](/manual/web_interface/frontend_sections/administration/user_roles)
:::



[comment]: # ({/23b4a056-42834e1b})

[comment]: # ({aa6c53b5-f6c19c90})
### Параметры

`(объект)` Параметры определяют желаемый результат.

Этот метод поддерживает следующие параметры:

|Параметр|[Тип](/manual/api/reference_commentary#data_types)|Описание|
|---------|---------------------------------------------------|-----------|
|actionids|строка/массив|Возвращает только действия с заданным ID.|
|groupids|строка/массив|Возвращает только те действия, которые используют заданные ID групп узлов сети в условии действия.|
|hostids|строка/массив|Возвращает только те действия, которые используют заданные ID узлов сети в условии действия.|
|triggerids|строка/массив|Возвращает только те действия, которые используют заданные ID триггеров в условии действия.|
|mediatypeids|строка/массив|Возвращает только те действия, которые используют заданные ID способов оповещений для уведомлений.|
|usrgrpids|строка/массив|Возвращает только те действия, которые настроены для отправки сообщений заданным ID групп пользователей.|
|userids|строка/массив|Возвращает только те действия, которые настроены для отправки сообщений заданным ID пользователей.|
|scriptids|строка/массив|Возвращает только те действия, которые настроены для запуска заданных ID скриптов.|
|selectFilter|запрос|Возвращает свойство [filter](/manual/api/reference/action/object#action_filter) с условиями фильтрации для действия.|
|selectOperations|запрос|Возвращает свойство [operations](/manual/api/reference/action/object#action_operation) с операциями действия.|
|selectRecoveryOperations|запрос|Возвращает свойство [recovery_operations](/manual/api/reference/action/object#action_recovery_operation) с операциями востановления действия.|
|selectUpdateOperations|запрос|Возвращает свойство [update\_operations](/manual/api/reference/action/object#action_update_operation) с операциями обновления действия.|
|sortfield|строка/массив|Сортирует результат используя заданные свойства.<br><br>Возможные значения: `actionid`, `name` и `status`.|
|countOutput|логический тип|Эти параметры являются общими для всех методов `get` и они описаны в [cправочных комментариях](/manual/api/reference_commentary#common_get_method_parameters).|
|editable|логический тип|^|
|excludeSearch|логический тип|^|
|filter|объект|^|
|limit|целое число|^|
|output|запрос|^|
|preservekeys|логический тип|^|
|search|объект|^|
|searchByAny|логический тип|^|
|searchWildcardsEnabled|логический тип|^|
|sortorder|строка/массив|^|
|startSearch|логический тип|^|

[comment]: # ({/aa6c53b5-f6c19c90})

[comment]: # ({1052aeba-7223bab1})
### Возвращаемые значения

`(целое число/массив)` Возвращает:

-   массив объектов
или
-   количество полученных объектов , если был использован параметр `countOutput`.

[comment]: # ({/1052aeba-7223bab1})

[comment]: # ({41c52ba7-b41637d2})
### Примеры

[comment]: # ({/41c52ba7-b41637d2})

[comment]: # ({6a93bfaf-f7d2e806})
#### Получение действий триггеров

Вовращает все настроенные действия триггеров с условиями и операциями действия.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "action.get",
    "params": {
        "output": "extend",
        "selectOperations": "extend",
        "selectRecoveryOperations": "extend",
        "selectUpdateOperations": "extend",
        "selectFilter": "extend",
        "filter": {
            "eventsource": 0
        }
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "actionid": "3",
            "name": "Report problems to Zabbix administrators",
            "eventsource": "0",
            "status": "1",
            "esc_period": "1h",
            "pause_suppressed": "1",
            "filter": {
                "evaltype": "0",
                "formula": "",
                "conditions": [],
                "eval_formula": ""
            },
            "operations": [
                {
                    "operationid": "3",
                    "actionid": "3",
                    "operationtype": "0",
                    "esc_period": "0",
                    "esc_step_from": "1",
                    "esc_step_to": "1",
                    "evaltype": "0",
                    "opconditions": [],
                    "opmessage": [
                        {
                            "default_msg": "1",
                            "subject": "",
                            "message": "",
                            "mediatypeid" => "0"
                        }
                    ],
                    "opmessage_grp": [
                        {
                            "usrgrpid": "7"
                        }
                    ]
                }
            ],
            "recovery_operations": [
                {
                    "operationid": "7",
                    "actionid": "3",
                    "operationtype": "11",
                    "evaltype": "0",
                    "opconditions": [],
                    "opmessage": {
                        "default_msg": "0",
                        "subject": "{TRIGGER.STATUS}: {TRIGGER.NAME}",
                        "message": "Trigger: {TRIGGER.NAME}\r\nTrigger status: {TRIGGER.STATUS}\r\nTrigger severity: {TRIGGER.SEVERITY}\r\nTrigger URL: {TRIGGER.URL}\r\n\r\nItem values:\r\n\r\n1. {ITEM.NAME1} ({HOST.NAME1}:{ITEM.KEY1}): {ITEM.VALUE1}\r\n2. {ITEM.NAME2} ({HOST.NAME2}:{ITEM.KEY2}): {ITEM.VALUE2}\r\n3. {ITEM.NAME3} ({HOST.NAME3}:{ITEM.KEY3}): {ITEM.VALUE3}\r\n\r\nOriginal event ID: {EVENT.ID}",
                        "mediatypeid": "0"
                    }
                }
            ],
            "update_operations": [
                {
                    "operationid": "31",
                    "operationtype": "12",
                    "evaltype": "0",
                    "opmessage": {
                        "default_msg": "1",
                        "subject": "",
                        "message": "",
                        "mediatypeid": "0"
                    }
                },
                {
                    "operationid": "32",
                    "operationtype": "0",
                    "evaltype": "0",
                    "opmessage": {
                        "default_msg": "0",
                        "subject": "Updated: {TRIGGER.NAME}",
                        "message": "{USER.FULLNAME} updated problem at {EVENT.UPDATE.DATE} {EVENT.UPDATE.TIME} with the following message:\r\n{EVENT.UPDATE.MESSAGE}\r\n\r\nCurrent problem status is {EVENT.STATUS}",
                        "mediatypeid": "1"
                    },
                    "opmessage_grp": [
                        {
                            "usrgrpid": "7"
                        }
                    ],
                    "opmessage_usr": []
                },
                {
                    "operationid": "33",
                    "operationtype": "1",
                    "evaltype": "0",
                    "opcommand": {
                        "scriptid": "3"
                    },
                    "opcommand_hst": [
                        {
                            "hostid": "10084"
                        }
                    ],
                    "opcommand_grp": []
                }
            ]
        }
    ],
    "id": 1
}
```

[comment]: # ({/6a93bfaf-f7d2e806})

[comment]: # ({3b3baca5-d96da9cf})
#### Получение действий обнаружения

Возвращает все настроенные  действия обнаружения вместе с условиями и операциями действия.  Фильтр использует тип вычисления "и", поэтому свойство `formula` будет пустое а свойство`eval_formula` генерируется автоматически.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "action.get",
    "params": {
        "output": "extend",
        "selectOperations": "extend"
        "selectFilter": "extend",
        "filter": {
            "eventsource": 1
        }
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "actionid": "2",
            "name": "Auto discovery. Linux servers.",
            "eventsource": "1",
            "status": "1",
            "esc_period": "0s",
            "pause_suppressed": "1",
            "filter": {
                "evaltype": "0",
                "formula": "",
                "conditions": [
                    {
                        "conditiontype": "10",
                        "operator": "0",
                        "value": "0",
                        "value2": "",
                        "formulaid": "B"
                    },
                    {
                        "conditiontype": "8",
                        "operator": "0",
                        "value": "9",
                        "value2": "",
                        "formulaid": "C"
                    },
                    {
                        "conditiontype": "12",
                        "operator": "2",
                        "value": "Linux",
                        "value2": "",
                        "formulaid": "A"
                    }
                ],
                "eval_formula": "A and B and C"
            },
            "operations": [
                {
                    "operationid": "1",
                    "actionid": "2",
                    "operationtype": "6",
                    "esc_period": "0s",
                    "esc_step_from": "1",
                    "esc_step_to": "1",
                    "evaltype": "0",
                    "opconditions": [],
                    "optemplate": [
                        {
                            "templateid": "10001"
                        }
                    ]
                },
                {
                    "operationid": "2",
                    "actionid": "2",
                    "operationtype": "4",
                    "esc_period": "0s",
                    "esc_step_from": "1",
                    "esc_step_to": "1",
                    "evaltype": "0",
                    "opconditions": [],
                    "opgroup": [
                        {
                            "groupid": "2"
                        }
                    ]
                }
            ]
        }
    ],
    "id": 1
}
```

[comment]: # ({/3b3baca5-d96da9cf})

[comment]: # ({8267562b-755496f7})
### Смотрите также

-   [Фильтр действия](object#action_filter)
-   [Операции действия](object#action_operation)

[comment]: # ({/8267562b-755496f7})


[comment]: # ({e1243621-025b824e})
### Исходный код

CAction::get() in *ui/include/classes/api/services/CAction.php*.

[comment]: # ({/e1243621-025b824e})
