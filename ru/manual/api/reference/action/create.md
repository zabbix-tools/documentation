[comment]: # translation:outdated

[comment]: # ({c7f202d4-c7f202d4})
# action.create

[comment]: # ({/c7f202d4-c7f202d4})

[comment]: # ({76cececa-71c93aa0})
### Описание

`object action.create(объект/массив действий)`

Этот метод позволяет создавать новые действия.

::: noteclassic
Этот метод доступен только типам пользователей *Admin* и *Super admin*.
Разрешение на использование данного метода можно отозвать в настройках роли пользователя. 
Подробнее смотрите [Роли пользователей](/manual/web_interface/frontend_sections/administration/user_roles)
:::

[comment]: # ({/76cececa-71c93aa0})

[comment]: # ({02a3a2d8-5a9be3c9})
### Параметры

`(объект/массив)` Создаваемые действия.

В дополнение к [стандартным свойствам действия](object#action), этот метод принимает следующие параметры.

|Параметр|[Тип](/manual/api/reference_commentary#data_types)|Описание|
|----------------|------|----------------|
|filter|объект|Объект [фильтра действия](/manual/api/reference/action/object#action_filter) для действия.|
|operations|массив |Создаваемая [операция действия](/manual/api/reference/action/object#action_operation) для действия. |
|recovery\_operations|массив|Создаваемая [операция восстановления](/manual/api/reference/action/object#action_recovery_operation) для действия.|
|update\_operations|массив|Создаваемая [операция обновления](/manual/api/reference/action/object#action_update_operation) для действия.|

[comment]: # ({/02a3a2d8-5a9be3c9})

[comment]: # ({70f441d9-2830affd})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID созданных действий под
свойством `actionids`. Порядок возвращаемых ID совпадает с порядком
переданных действий.

[comment]: # ({/70f441d9-2830affd})

[comment]: # ({41c52ba7-b41637d2})
### Примеры

[comment]: # ({/41c52ba7-b41637d2})

[comment]: # ({new-afc96865})
#### Создание триггера действия

Создает действие, которое сработает если триггер в статусе "Проблема" узла сети "10084" имеет в названии слово "memory". Действие обязано оповестить всех пользователей в группе "7". Если событие не перейдет в статус ОК в течении 4х минут, будет запущен скрипт "3" на всех узлах сети из группы "2". При восстановлении триггера будут уведомлены все пользователи, которые получили сообщение о проблеме ранее. В случае обновления, сообщение с пользовательским заголовком и содержанием будет отправлено всем пользователям кто оставил комментарии и подтвердил проблему, всеми способами оповещения. 

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "action.create",
    "params": {
        "name": "Trigger action",
        "eventsource": 0,
        "status": 0,
        "esc_period": "2m",
        "filter": {
            "evaltype": 0,
            "conditions": [
                {
                    "conditiontype": 1,
                    "operator": 0,
                    "value": "10084"
                },
                {
                    "conditiontype": 3,
                    "operator": 2,
                    "value": "memory"
                }
            ]
        },
        "operations": [
            {
                "operationtype": 0,
                "esc_period": "0s",
                "esc_step_from": 1,
                "esc_step_to": 2,
                "evaltype": 0,
                "opmessage_grp": [
                    {
                        "usrgrpid": "7"
                    }
                ],
                "opmessage": {
                    "default_msg": 1,
                    "mediatypeid": "1"
                }
            },
            {
                "operationtype": 1,
                "esc_step_from": 3,
                "esc_step_to": 4,
                "evaltype": 0,
                "opconditions": [
                    {
                        "conditiontype": 14,
                        "operator": 0,
                        "value": "0"
                    }
                ],
                "opcommand_grp": [
                    {
                        "groupid": "2"
                    }
                ],
                "opcommand": {
                    "scriptid": "3"
                }
            }
        ],
        "recovery_operations": [
            {
                "operationtype": "11",
                "opmessage": {
                    "default_msg": 1
                }
            }    
        ],
        "update_operations": [
            {
                "operationtype": "12",
                "opmessage": {
                    "default_msg": 0,
                    "message": "Custom update operation message body",
                    "subject": "Custom update operation message subject"
                }
            }
        ],
        "pause_suppressed": "0",
        "notify_if_canceled": "0"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "actionids": [
            "17"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-afc96865})

[comment]: # ({new-53bdbb4b})
#### Создание действия обнаружения

Создает действие, которое назначит обнаруженным узлам сети шаблон "10091".

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "action.create",
    "params": {
        "name": "Discovery action",
        "eventsource": 1,
        "status": 0,
        "esc_period": "0s",
        "filter": {
            "evaltype": 0,
            "conditions": [
                {
                    "conditiontype": 21,
                    "value": "1"
                },
                {
                    "conditiontype": 10,
                    "value": "2"
                }
            ]
        },
        "operations": [
            {
                "esc_step_from": 1,
                "esc_period": "0s",
                "optemplate": [
                    {
                        "templateid": "10091"
                    }
                ],
                "operationtype": 6,
                "esc_step_to": 1
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "actionids": [
            "18"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-53bdbb4b})

[comment]: # ({new-de84bbc4})
#### Использование пользовательского фильтра выражения 

Создает триггер действия который будет использовать пользовательский фильтр. Действие отправит сообщение по каждому триггеру с критичностью большей или равной "Предупреждение" для узлов сети "10084" и "10106". Идентификаторы формулы "A",
"B" и "C" выбраны произвольно.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "action.create",
    "params": {
        "name": "Trigger action",
        "eventsource": 0,
        "status": 0,
        "esc_period": "2m",
        "filter": {
            "evaltype": 3,
            "formula": "A and (B or C)",
            "conditions": [
                {
                    "conditiontype": 4,
                    "operator": 5,
                    "value": "2",
                    "formulaid": "A"
                },
                {
                    "conditiontype": 1,
                    "operator": 0,
                    "value": "10084",
                    "formulaid": "B"
                },
                {
                    "conditiontype": 1,
                    "operator": 0,
                    "value": "10106",
                    "formulaid": "C"
                }
            ]
        },
        "operations": [
            {
                "operationtype": 0,
                "esc_period": "0s",
                "esc_step_from": 1,
                "esc_step_to": 2,
                "evaltype": 0,
                "opmessage_grp": [
                    {
                        "usrgrpid": "7"
                    }
                ],
                "opmessage": {
                    "default_msg": 1,
                    "mediatypeid": "1"
                }
            }
        ],
        "pause_suppressed": "0",
        "notify_if_canceled": "0"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "actionids": [
            "18"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-de84bbc4})

[comment]: # ({new-b81a306b})
#### Создание правила авторегистрации агента

Добавляет узел сети в группу узлов сети "Linux servers", когда имя узла сети содержит "SRV" или метаданные содержат "CentOS".

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "action.create",
    "params": {
        "name": "Register Linux servers",
        "eventsource": "2",
        "status": "0",
        "filter": {
            "evaltype": "2",
            "formula": "A or B",
            "conditions": [
                {
                    "conditiontype": "22",
                    "operator": "2",
                    "value": "SRV",
                    "value2": "",
                    "formulaid": "B"
                },
                {
                    "conditiontype": "24",
                    "operator": "2",
                    "value": "CentOS",
                    "value2": "",
                    "formulaid": "A"
                }
            ]
        },
        "operations": [
            {
                "actionid": "9",
                "operationtype": "4",
                "esc_period": "0",
                "esc_step_from": "1",
                "esc_step_to": "1",
                "evaltype": "0",
                "opgroup": [
                    {
                        "operationid": "16",
                        "groupid": "2"
                    }
                ]
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "actionids": [
            19
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-b81a306b})

[comment]: # ({58a2df48-755496f7})
### Смотрите также

-   [Фильтр действия](object#action_filter)
-   [Операция действия](object#action_operation)

[comment]: # ({/58a2df48-755496f7})


[comment]: # ({0981f910-32335876})
### Исходный код

CAction::create() in *ui/include/classes/api/services/CAction.php*.

[comment]: # ({/0981f910-32335876})
