<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="ru" datatype="plaintext" original="manual/api/reference/action/update.md">
    <body>
      <trans-unit id="e4b5aac4" xml:space="preserve">
        <source># action.update</source>
      </trans-unit>
      <trans-unit id="b1673b49" xml:space="preserve">
        <source>### Description

`object action.update(object/array actions)`

This method allows to update existing actions.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::</source>
      </trans-unit>
      <trans-unit id="703e85ce" xml:space="preserve">
        <source>### Parameters

`(object/array)` Action properties to be updated.

The `actionid` property must be defined for each action, all other
properties are optional. Only the passed properties will be updated, all
others will remain unchanged.

Additionally to the [standard action properties](object#action), the
method accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|filter|object|Action [filter](/manual/api/reference/action/object#action_filter) object to replace the current filter.|
|operations|array|Action [operations](/manual/api/reference/action/object#action_operation) to replace existing operations.|
|recovery\_operations|array|Action [recovery operations](/manual/api/reference/action/object#action_recovery_operation) to replace existing recovery operations.&lt;br&gt;&lt;br&gt;[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):&lt;br&gt;- *supported* if `eventsource` of [Action object](/manual/api/reference/action/object#action) is set to "event created by a trigger", "internal event", or "event created on service status update"|
|update\_operations|array|Action [update operations](/manual/api/reference/action/object#action_update_operation) to replace existing update operations.&lt;br&gt;&lt;br&gt;[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):&lt;br&gt;- *supported* if `eventsource` of [Action object](/manual/api/reference/action/object#action) is set to "event created by a trigger" or "event created on service status update"|</source>
      </trans-unit>
      <trans-unit id="8cbaa103" xml:space="preserve">
        <source>### Return values

`(object)` Returns an object containing the IDs of the updated actions
under the `actionids` property.</source>
      </trans-unit>
      <trans-unit id="b41637d2" xml:space="preserve">
        <source>### Examples</source>
      </trans-unit>
      <trans-unit id="77b78ec5" xml:space="preserve">
        <source>#### Disable action

Disable an action, that is, set its status to "1".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "action.update",
    "params": {
        "actionid": "2",
        "status": "1"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "actionids": [
            "2"
        ]
    },
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="755496f7" xml:space="preserve">
        <source>### See also

-   [Action filter](object#action_filter)
-   [Action operation](object#action_operation)</source>
      </trans-unit>
      <trans-unit id="df9f1d35" xml:space="preserve">
        <source>### Source

CAction::update() in *ui/include/classes/api/services/CAction.php*.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
