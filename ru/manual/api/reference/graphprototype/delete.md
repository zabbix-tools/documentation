[comment]: # translation:outdated

[comment]: # ({new-0a247a7d})
# graphprototype.delete

[comment]: # ({/new-0a247a7d})

[comment]: # ({new-8256b270})
### Описание

`объект graphprototype.delete(массив graphIds)`

Этот метод позволяет удалять прототипы графиков.

[comment]: # ({/new-8256b270})

[comment]: # ({new-3e4a53cc})
### Параметры

`(массив)` ID удаляемых прототипов графиков.

[comment]: # ({/new-3e4a53cc})

[comment]: # ({new-82123f70})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID удаленных прототипов
графиков под свойством `graphids`.

[comment]: # ({/new-82123f70})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-d24b4f06})
#### Удаление нескольких прототипов графиков

Удаление двух прототипов графиков.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "graphprototype.delete",
    "params": [
        "652",
        "653"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "graphids": [
            "652",
            "653"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-d24b4f06})

[comment]: # ({new-21f88b2f})
### Исходный код

CGraphPrototype::delete() в
*frontends/php/include/classes/api/services/CGraphPrototype.php*.

[comment]: # ({/new-21f88b2f})
