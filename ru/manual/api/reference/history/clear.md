[comment]: # translation:outdated

[comment]: # ({new-991d071b})
# history.clear

[comment]: # ({/new-991d071b})

[comment]: # ({new-3bf9d0da})
### Description

`object history.clear(array itemids)`

This method allows to clear item history.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-3bf9d0da})

[comment]: # ({new-f449cc26})
### Parameters

`(array)` IDs of items to clear.

[comment]: # ({/new-f449cc26})

[comment]: # ({new-a59d9134})
### Return values

`(object)` Returns an object containing the IDs of the cleared items
under the `itemids` property.

[comment]: # ({/new-a59d9134})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-e482fed3})
#### Clear history

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "history.clear",
    "params": [
        "10325",
        "13205"
    ],
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "10325",
            "13205"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-e482fed3})

[comment]: # ({new-e7aa34e8})
### Source

CHistory::clear() in *ui/include/classes/api/services/CHistory.php*.

[comment]: # ({/new-e7aa34e8})
