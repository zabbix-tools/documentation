[comment]: # translation:outdated

[comment]: # ({new-2587902b})
# service.get

[comment]: # ({/new-2587902b})

[comment]: # ({new-bfd5de7f})
### Описание

`целое число/массив service.get(объект параметры)`

Этот метод позволяет получать услуги в соответствии с заданными
параметрами.

[comment]: # ({/new-bfd5de7f})

[comment]: # ({new-e7637a1d})
### Параметры

`(объект)` Параметры задают желаемый вывод.

Этот метод поддерживает следующие параметры.

|Параметр|Тип|Описание|
|----------------|------|----------------|
|serviceids|строка/массив|Возврат услуг только с заданными ID.|
|parentids|строка/массив|Возврат только тех услуг, которые имеют заданные жёстко-зависимые родительские услуги.|
|childids|строка/массив|Возврат только тех услуг, которые имеют заданные жёстко-зависимые дочерние услуги.|
|selectParent|запрос|Возврат родительских услуг, которые жёстко зависимы, в свойстве `parent`.|
|selectDependencies|запрос|Воврат зависимых дочерних услуг в свойстве `dependencies`.|
|selectParentDependencies|запрос|Возврат зависимостей родительских услуг в свойстве `parentDependencies`.|
|selectTimes|запрос|Вовзврат времен услуг в свойстве `times`.|
|selectAlarms|запрос|Возврат тревог услуг в свойстве `alarms`.|
|selectTrigger|запрос|Возврат связанных триггеров в свойстве `trigger`.|
|sortfield|строка/массив|Сортировка результата в соответствии с заданными свойствами.<br><br>Возможные значения: `name` и `sortorder`.|
|countOutput|логический|Эти параметры являются общими для всех методов `get` и они описаны в [справочных комментариях](/ru/manual/api/reference_commentary#общие_параметры_get_метода).|
|editable|логический|^|
|excludeSearch|логический|^|
|filter|объект|^|
|limit|целое число|^|
|output|запрос|^|
|preservekeys|логический|^|
|search|объект|^|
|searchByAny|логический|^|
|searchWildcardsEnabled|логический|^|
|sortorder|строка/массив|^|
|startSearch|логический|^|

[comment]: # ({/new-e7637a1d})

[comment]: # ({new-7223bab1})
### Возвращаемые значения

`(целое число/массив)` Возвращает либо:

-   массив объектов;
-   количество найденных объектов, если используется параметр
    `countOutput`.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-cd4e4011})
#### Получение всех услуг

Получение всех данных о всех услугах и их зависимостях.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "service.get",
    "params": {
        "output": "extend",
        "selectDependencies": "extend"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "serviceid": "2",
            "name": "Server 1",
            "status": "0",
            "algorithm": "1",
            "triggerid": "0",
            "showsla": "1",
            "goodsla": "99.9000",
            "sortorder": "0",
            "dependencies": []
        },
        {
            "serviceid": "3",
            "name": "Data center 1",
            "status": "0",
            "algorithm": "1",
            "triggerid": "0",
            "showsla": "1",
            "goodsla": "99.9000",
            "sortorder": "0",
            "dependencies": [
                {
                    "linkid": "11",
                    "serviceupid": "3",
                    "servicedownid": "2",
                    "soft": "0",
                    "sortorder": "0",
                    "serviceid": "2"
                },
                {
                    "linkid": "10",
                    "serviceupid": "3",
                    "servicedownid": "5",
                    "soft": "0",
                    "sortorder": "1",
                    "serviceid": "5"
                }
            ]
        },
        {
            "serviceid": "5",
            "name": "Server 2",
            "status": "0",
            "algorithm": "1",
            "triggerid": "0",
            "showsla": "1",
            "goodsla": "99.9900",
            "sortorder": "1",
            "dependencies": []
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-cd4e4011})

[comment]: # ({new-05e3cf18})
### Исходный код

CService::get() в
*frontends/php/include/classes/api/services/CService.php*.

[comment]: # ({/new-05e3cf18})
