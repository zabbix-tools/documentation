[comment]: # translation:outdated

[comment]: # ({new-bf12fc32})
# service.delete

[comment]: # ({/new-bf12fc32})

[comment]: # ({new-8ea5261f})
### Описание

`объект service.delete(массив ServiceIds)`

Этот метод позволяет удалять услуги.

Услуги с жестко зависимыми дочерними услугами нельзя удалять.

[comment]: # ({/new-8ea5261f})

[comment]: # ({new-7af05f92})
### Параметры

`(массив)` ID удаляемых услуг.

[comment]: # ({/new-7af05f92})

[comment]: # ({new-23960c29})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID удаленных услуг под
свойством `serviceids`.

[comment]: # ({/new-23960c29})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-5036ec70})
#### Удаление нескольких услуг

Удаление двух услуг.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "service.delete",
    "params": [
        "4",
        "5"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "serviceids": [
            "4",
            "5"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-5036ec70})

[comment]: # ({new-c99e5046})
### Исходный код

CService::delete() в
*frontends/php/include/classes/api/services/CService.php*.

[comment]: # ({/new-c99e5046})
