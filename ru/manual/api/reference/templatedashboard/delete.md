[comment]: # translation:outdated

[comment]: # ({new-9dee678d})
# templatedashboard.delete

[comment]: # ({/new-9dee678d})

[comment]: # ({new-f76c3800})
### Description

`object templatedashboard.delete(array templateDashboardIds)`

This method allows to delete template dashboards.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-f76c3800})

[comment]: # ({new-276bf1c0})
### Parameters

`(array)` IDs of the template dashboards to delete.

[comment]: # ({/new-276bf1c0})

[comment]: # ({new-73b9628f})
### Return values

`(object)` Returns an object containing the IDs of the deleted template
dashboards under the `dashboardids` property.

[comment]: # ({/new-73b9628f})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-5c967ef8})
#### Deleting multiple template dashboards

Delete two template dashboards.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "templatedashboard.delete",
    "params": [
        "45",
        "46"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "dashboardids": [
            "45",
            "46"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-5c967ef8})

[comment]: # ({new-d4de7a93})
### Source

CTemplateDashboard::delete() in
*ui/include/classes/api/services/CTemplateDashboard.php*.

[comment]: # ({/new-d4de7a93})
