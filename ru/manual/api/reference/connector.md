[comment]: # translation:outdated

[comment]: # ({new-4cd7085b})
# Connector

This class is designed to work with connector objects.

Object references:\

- [Connector](/manual/api/reference/connector/object#connector)
- [Connector tag filter](/manual/api/reference/connector/object#tag-filter)

Available methods:\

- [connector.create](/manual/api/reference/connector/create) - creating new connectors
- [connector.delete](/manual/api/reference/connector/delete) - deleting connectors
- [connector.get](/manual/api/reference/connector/get) - retrieving connectors
- [connector.update](/manual/api/reference/connector/update) - updating connectors

[comment]: # ({/new-4cd7085b})
