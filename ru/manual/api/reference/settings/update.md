[comment]: # translation:outdated

[comment]: # ({new-acca5f57})
# settings.update

[comment]: # ({/new-acca5f57})

[comment]: # ({new-9252b567})
### Description

`object settings.update(object settings)`

This method allows to update existing common settings.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-9252b567})

[comment]: # ({new-7e621fea})
### Parameters

`(object)` Settings properties to be updated.

[comment]: # ({/new-7e621fea})

[comment]: # ({new-440363c6})
### Return values

`(array)` Returns array with the names of updated parameters.

[comment]: # ({/new-440363c6})

[comment]: # ({new-c59f767c})
### Examples

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "settings.update",
    "params": {
        "login_attempts": "1",
        "login_block": "1m"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        "login_attempts",
        "login_block"
    ],
    "id": 1
}
```

[comment]: # ({/new-c59f767c})

[comment]: # ({new-e23f9161})
### Source

CSettings::update() in *ui/include/classes/api/services/CSettings.php*.

[comment]: # ({/new-e23f9161})
