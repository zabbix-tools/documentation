[comment]: # translation:outdated

[comment]: # ({new-7fceb632})
# Информация о API

Этот класс предназначен для получения мета информации о API.

Доступные методы:\

-   [apiinfo.version](/ru/manual/api/reference/apiinfo/version) -
    получение версии Zabbix API

[comment]: # ({/new-7fceb632})
