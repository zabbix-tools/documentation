[comment]: # translation:outdated

[comment]: # ({new-830462e8})
# Услуга

Этот класс предназначен для работы с услугами.

Справка по объектам:\

-   [Услуга](/ru/manual/api/reference/service/object#услуга)
-   [Время услуги](/ru/manual/api/reference/service/object#время_услуги)
-   [Зависимость
    услуги](/ru/manual/api/reference/service/object#зависимость_услуги)
-   [Тревога
    услуги](/ru/manual/api/reference/service/object#тревога_услуги)

Доступные методы:\

-   [service.adddependencies](/ru/manual/api/reference/service/adddependencies) -
    добавление зависимостей между услугами IT
-   [service.addtimes](/ru/manual/api/reference/service/addtimes) -
    добавление времен услуг
-   [service.create](/ru/manual/api/reference/service/create) - создание
    новых услуг IT
-   [service.delete](/ru/manual/api/reference/service/delete) - удаление
    услуг IT
-   [service.deletedependencies](/ru/manual/api/reference/service/deletedependencies) -
    удаление зависимостей между услугами IT
-   [service.deletetimes](/ru/manual/api/reference/service/deletetimes) -
    удаление времен услуг
-   [service.get](/ru/manual/api/reference/service/get) - получение
    услуг IT
-   [service.getsla](/ru/manual/api/reference/service/getsla) -
    получение информации о доступности услуг IT
-   [service.update](/ru/manual/api/reference/service/update) -
    обновление услуг IT

[comment]: # ({/new-830462e8})
