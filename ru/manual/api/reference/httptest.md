[comment]: # translation:outdated

[comment]: # ({new-7aa664d0})
# Веб-сценарий

Этот класс предназначен для работы с веб-сценариями.

Справка по объектам:\

-   [Веб-сценарий](/ru/manual/api/reference/httptest/object#веб-сценарий)
-   [Шаг
    сценария](/ru/manual/api/reference/httptest/object#шаг_сценария)

Доступные методы:\

-   [httptest.create](/ru/manual/api/reference/httptest/create) -
    создание новых веб-сценариев
-   [httptest.delete](/ru/manual/api/reference/httptest/delete) -
    удаление веб-сценариев
-   [httptest.get](/ru/manual/api/reference/httptest/get) - получение
    веб-сценариев
-   [httptest.update](/ru/manual/api/reference/httptest/update) -
    обновление веб-сценариев

[comment]: # ({/new-7aa664d0})
