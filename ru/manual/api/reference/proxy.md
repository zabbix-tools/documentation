[comment]: # translation:outdated

[comment]: # ({new-b9b7d8da})
# Прокси

Этот класс предназначен для работы с прокси.

Справка по объектам:\

-   [Прокси](/ru/manual/api/reference/proxy/object#прокси)
-   [Интерфейс
    прокси](/ru/manual/api/reference/proxy/object#интерфейс_прокси)

Доступные методы:\

-   [proxy.create](/ru/manual/api/reference/proxy/create) - создание
    новых прокси
-   [proxy.delete](/ru/manual/api/reference/proxy/delete) - удаление
    прокси
-   [proxy.get](/ru/manual/api/reference/proxy/get) - получение прокси
-   [proxy.update](/ru/manual/api/reference/proxy/update) - обновление
    прокси

[comment]: # ({/new-b9b7d8da})
