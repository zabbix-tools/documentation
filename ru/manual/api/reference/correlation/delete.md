[comment]: # translation:outdated

[comment]: # ({new-89029829})
# correlation.delete

[comment]: # ({/new-89029829})

[comment]: # ({new-4922e104})
### Описание

`объект correlation.delete(массив correlationids)`

Этот метод позволяет удалять корреляции.

[comment]: # ({/new-4922e104})

[comment]: # ({new-b445698c})
### Параметры

`(массив)` ID удаляемых корреляций.

[comment]: # ({/new-b445698c})

[comment]: # ({new-855af217})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID удаленных корреляций
под свойством `correlationids`.

[comment]: # ({/new-855af217})

[comment]: # ({new-c9f65268})
### Пример

[comment]: # ({/new-c9f65268})

[comment]: # ({new-bfc49991})
#### Удаление нескольких корреляций

Удаление двух корреляций.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "correlation.delete",
    "params": [
        "1",
        "2"
    ],
    "auth": "343baad4f88b4106b9b5961e77437688",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "correlaionids": [
            "1",
            "2"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-bfc49991})

[comment]: # ({new-b0713c0f})
### Исходный код

CCorrelation::delete() в
*frontends/php/include/classes/api/services/CCorrelation.php*.

[comment]: # ({/new-b0713c0f})
