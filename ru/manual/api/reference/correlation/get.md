[comment]: # translation:outdated

[comment]: # ({new-7416ad19})
# correlation.get

[comment]: # ({/new-7416ad19})

[comment]: # ({new-858936aa})
### Описание

`целое число/массив correlation.get(объект параметры)`

Этот метод позволяет получать корреляции в соответствии с заданными
параметрами.

[comment]: # ({/new-858936aa})

[comment]: # ({new-06a650a0})
### Параметры

`(объект)` Параметры задают желаемый вывод.

Этот метод поддерживает следующие параметры.

|Параметр|Тип|Тип|
|----------------|------|------|
|correlationids|строка/массив|Возврат корреляций только с заданными ID.|
|selectFilter|запрос|Возврат фильтра корреляции в свойстве `filter`.|
|selectOperations|запрос|Возврат операций корреляции в свойстве `operations`.|
|sortfield|строка/массив|Сортировка результата в соответствии с заданными свойствами.<br><br>Возможные значения: `correlationid`, `name` и `status`.|
|countOutput|логический|Эти параметры являются общими для всех методов `get` и они описаны в [справочных комментариях](/ru/manual/api/reference_commentary#общие_параметры_get_метода).|
|editable|логический|^|
|excludeSearch|логический|^|
|filter|объект|^|
|limit|целое число|^|
|output|запрос|^|
|preservekeys|логический|^|
|search|объект|^|
|searchByAny|логический|^|
|searchWildcardsEnabled|логический|^|
|sortorder|строка/массив|^|
|startSearch|логический|^|

[comment]: # ({/new-06a650a0})

[comment]: # ({new-7223bab1})
### Возвращаемые значения

`(целое число/массив)` Возвращает либо:

-   массив объектов;
-   количество найденных объектов, если используется параметр
    `countOutput`.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-d14c4f20})
#### Получение корреляций

Получение всех добавленных корреляций вместе с условиями и операциями
корреляции. Фильтр использует "и/или" тип вычисления, таким образом
свойство `formula` пустое и `eval_formula` генерируется автоматически.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "correlation.get",
    "params": {
        "output": "extend",
        "selectOperations": "extend",
        "selectFilter": "extend"
    },
    "auth": "343baad4f88b4106b9b5961e77437688",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "correlationid": "1",
            "name": "Correlation 1",
            "description": "",
            "status": "0",
            "filter": {
                "evaltype": "0",
                "formula": "",
                "conditions": [
                    {
                        "type": "3",
                        "oldtag": "error",
                        "newtag": "ok",
                        "formulaid": "A"
                    }
                ],
                "eval_formula": "A"
            },
            "operations": [
                {
                    "type": "0"
                }
            ]
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-d14c4f20})

[comment]: # ({new-e7d15410})
### Смотрите также

-   [Фильтр корреляции](object#фильтр_корреляции)
-   [Операция корреляции](object#операция_корреляции)

[comment]: # ({/new-e7d15410})

[comment]: # ({new-bbab04bd})
### Исходный код

CCorrelation::get() в
*frontends/php/include/classes/api/services/CCorrelation.php*.

[comment]: # ({/new-bbab04bd})
