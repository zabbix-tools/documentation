[comment]: # translation:outdated

[comment]: # ({new-c71a2e54})
# correlation.update

[comment]: # ({/new-c71a2e54})

[comment]: # ({new-b9562236})
### Описание

`объект correlation.update(объект/массив корреляции)`

Этот метод позволяет обновлять существующие корреляции.

[comment]: # ({/new-b9562236})

[comment]: # ({new-326bc80e})
### Параметры

`(объект/массив)` Свойства корреляции, которые будут обновлены.

Свойство `correlationid` должно быть указано по каждой корреляции, все
остальные свойства опциональны. Будут обновлены только переданные
свойства, все остальные останутся неизменными.

В дополнение к [стандартным свойствам корреляции](object#корреляция),
этот метод принимает следующие параметры.

|Параметр|Тип|Описание|
|----------------|------|----------------|
|filter|объект|Объект фильтра корреляции, который заменит текущий фильтр.|
|operations|массив|Операции корреляции, которые заменят существующие операции.|

[comment]: # ({/new-326bc80e})

[comment]: # ({new-13e1dc13})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID обновленных корреляций
под свойством `correlationids`.

[comment]: # ({/new-13e1dc13})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-67bb2dfb})
#### Деактивация корреляции

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "correlation.update",
    "params": {
        "correlationid": "1",
        "status": "1"
    },
    "auth": "343baad4f88b4106b9b5961e77437688",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "correlationids": [
            "1"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-67bb2dfb})

[comment]: # ({new-d6ef1778})
#### Замена условий, но сохранение метода вычисления

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "correlation.update",
    "params": {
        "correlationid": "1",
        "filter": {
            "conditions": [
                {
                    "type": 3,
                    "oldtag": "error",
                    "newtag": "ok"
                }
            ]
        }
    },
    "auth": "343baad4f88b4106b9b5961e77437688",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "correlationids": [
            "1"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-d6ef1778})

[comment]: # ({new-e7d15410})
### Смотрите также

-   [Фильтр корреляции](object#фильтр_корреляции)
-   [Операция корреляции](object#операция_корреляции)

[comment]: # ({/new-e7d15410})

[comment]: # ({new-40fa665f})
### Исходный код

CCorrelation::update() в
*frontends/php/include/classes/api/services/CCorrelation.php*.

[comment]: # ({/new-40fa665f})
