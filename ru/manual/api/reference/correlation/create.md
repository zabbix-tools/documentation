[comment]: # translation:outdated

[comment]: # ({new-fbeccd23})
# correlation.create

[comment]: # ({/new-fbeccd23})

[comment]: # ({new-c06fb521})
### Описание

`объект correlation.create(объект/массив корреляции)`

Этот метод позволяет создавать новые корреляции.

[comment]: # ({/new-c06fb521})

[comment]: # ({new-8a55e636})
### Параметры

`(объект/массив)` Создаваемые корреляции.

В дополнение к [стандартным свойствам корреляции](object#корреляция),
этот метод принимает следующие параметры.

|Параметр|Тип|Описание|
|----------------|------|----------------|
|**operations**<br>(требуется)|массив|Создаваемые операции корреляции для корреляции.|
|**filter**<br>(требуется)|объект|Объект фильтра корреляции для корреляции.|

[comment]: # ({/new-8a55e636})

[comment]: # ({new-88660193})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID созданных корреляций
под свойством `correlationids`. Порядок возвращаемых ID совпадает с
порядком переданных корреляций.

[comment]: # ({/new-88660193})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-7325d5f8})
#### Создание корреляции на тег нового события

Создание корреляции, которое использует метод вычисления `И/ИЛИ`, с
одним условием и одной операцией. По умолчанию корреляция будет
активирована.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "correlation.create",
    "params": {
        "name": "new event tag correlation",
        "filter": {
            "evaltype": 0,
            "conditions": [
                {
                    "type": 1,
                    "tag": "ok"
                }
            ]
        },
        "operations": [
            {
                "type": 0
            }
        ]
    },
    "auth": "343baad4f88b4106b9b5961e77437688",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "correlationids": [
            "1"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-7325d5f8})

[comment]: # ({new-a12df7f6})
#### Использование фильтрации при помощи пользовательского выражения

Создание корреляции, которая будет использовать пользовательское условие
фильтрации. ID "A" и "B" в формуле были выбраны случайно. Тип условия
будет "Группа узлов сети" с оператором "<>".

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "correlation.create",
    "params": {
        "name": "new host group correlation",
        "description": "a custom description",
        "status": 0,
        "filter": {
            "evaltype": 3,
            "formula": "A or B",
            "conditions": [
                {
                    "type": 2,
                    "operator": 1,
                    "formulaid": "A"
                },
                {
                    "type": 2,
                    "operator": 1,
                    "formulaid": "B"
                }
            ]
        },
        "operations": [
            {
                "type": 1
            }
        ]
    },
    "auth": "343baad4f88b4106b9b5961e77437688",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "correlationids": [
            "2"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-a12df7f6})

[comment]: # ({new-e7d15410})
### Смотрите также

-   [Фильтр корреляции](object#фильтр_корреляции)
-   [Операция корреляции](object#операция_корреляции)

[comment]: # ({/new-e7d15410})

[comment]: # ({new-934ba89b})
### Исходный код

CCorrelation::create() в
*frontends/php/include/classes/api/services/CCorrelation.php*.

[comment]: # ({/new-934ba89b})
