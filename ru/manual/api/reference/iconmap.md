[comment]: # translation:outdated

[comment]: # ({new-5ee95992})
# Соответствие иконок

Этот класс предназначен для работы с соответствиями иконок.

Справка по объектам:\

-   [Соответствие
    иконок](/ru/manual/api/reference/iconmap/object#соответствие_иконок)
-   [Карта иконок](/ru/manual/api/reference/iconmap/object#карта_иконок)

Доступные методы:\

-   [iconmap.create](/ru/manual/api/reference/iconmap/create) - создание
    новых соответствий иконок
-   [iconmap.delete](/ru/manual/api/reference/iconmap/delete) - удаление
    соответствий иконок
-   [iconmap.get](/ru/manual/api/reference/iconmap/get) - получение
    соответствий иконок
-   [iconmap.update](/ru/manual/api/reference/iconmap/update) -
    обновление соответствий иконок

[comment]: # ({/new-5ee95992})
