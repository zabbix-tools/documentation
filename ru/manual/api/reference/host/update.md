[comment]: # translation:outdated

[comment]: # ({new-c572a02a})
# host.update

[comment]: # ({/new-c572a02a})

[comment]: # ({new-12e31652})
### Описание

`объект host.update(объект/массив узлы сети)`

Этот метод позволяет обновлять существующие узлы сети.

[comment]: # ({/new-12e31652})

[comment]: # ({new-3a8eac6a})
### Параметры

`(объект/массив)` Свойства узлов сети, которые будут обновлены.

Свойство `hostid` должно быть указано по каждому узлу сети, все
остальные свойства опциональны. Будут обновлены только переданные
свойства, все остальные останутся неизменными.

В дополнение к [стандартным свойствам узла сети](object#узел_сети), этот
метод принимает следующие параметры.

|Параметр|Тип|Описание|
|----------------|------|----------------|
|groups|объект/массив|Группы узлов сети, которые заменят текущие группы узлов сети в которые входят узлы сети.<br><br>У групп узлов сети должно быть задано свойство `groupid`.|
|interfaces|объект/массив|Интерфейсы узла сети, которые заменят текущие интерфейсы узла сети.|
|inventory|объект|Свойства инвентаризации узлов сети.|
|macros|объект/массив|Пользовательские макросы, которые заменят текущие макросы.|
|templates|объект/массив|Шаблоны, которые заменят присоединенные в настоящий момент шаблоны. Шаблоны, которые не переданы, только отсоединятся.<br><br>У шаблонов должно быть задано свойство `templateid`.|
|templates\_clear|объект/массив|Шаблоны, которые отсоединятся и очистят узлы сети.<br><br>У шаблонов должно быть задано свойство `templateid`.|

::: notetip
В отличие от Zabbix веб-интерфейса, когда `name`
идентично `host`, обновление свойства `host` не означает автоматическое
обновление `name`. Оба эти свойства необходимо обновить в явном
виде.
:::

[comment]: # ({/new-3a8eac6a})

[comment]: # ({new-d166b99b})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID обновленных узлов сети
под свойством `hostids`.

[comment]: # ({/new-d166b99b})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-ce50c453})
#### Активация узла сети

Активация мониторинга двух узлов, то есть изменение их состояния на
значение 0.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.update",
    "params": {
        "hostid": "10126",
        "status": 0
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "hostids": [
            "10126"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-ce50c453})

[comment]: # ({new-bea1db7e})
#### Отсоединение шаблонов

Отсоединение и очистка двух шаблонов с узла сети.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.update",
    "params": {
        "hostid": "10126",
        "templates_clear": [
            {
                "templateid": "10124"
            },
            {
                "templateid": "10125"
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "hostids": [
            "10126"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-bea1db7e})

[comment]: # ({new-98dcfd68})
#### Обновление макросов узла сети

Замена всех макросов узла сети на два новых.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.update",
    "params": {
        "hostid": "10126",
        "macros": [
            {
                "macro": "{$PASS}",
                "value": "password"
            },
            {
                "macro": "{$DISC}",
                "value": "sda"
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "hostids": [
            "10126"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-98dcfd68})

[comment]: # ({new-da99f3b8})
#### Обновление инвентаризации узла сети

Изменение режима инвентаризации и добавление местоположения

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.update",
    "params": {
        "hostid": "10387",
        "inventory_mode": 0,
        "inventory": {
            "location": "Latvia, Riga"
        }
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "hostids": [
            "10387"
        ]
    },
    "id": 2
}
```

[comment]: # ({/new-da99f3b8})

[comment]: # ({new-5cc5d950})
### Смотрите также

-   [host.massadd](massadd)
-   [host.massupdate](massupdate)
-   [host.massremove](massremove)
-   [Группа узлов
    сети](/ru/manual/api/reference/hostgroup/object#группа_узлов_сети)
-   [Шаблон](/ru/manual/api/reference/template/object#шаблон)
-   [Пользовательский
    макрос](/ru/manual/api/reference/usermacro/object#макрос_узла_сети)
-   [Интерфейс узла
    сети](/ru/manual/api/reference/hostinterface/object#интерфейс_узла_сети)
-   [Инвентаризация узла сети](object#данные_инвентаризации_узлов_сети)

[comment]: # ({/new-5cc5d950})

[comment]: # ({new-ae2d89bf})
#### Updating discovered host macros

Convert discovery rule created "automatic" macro to "manual" and change its value to "new-value".

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.update",
    "params": {
        "hostid": "10387",
        "macros": {
            "hostmacroid": "5541",
            "value": "new-value",
            "automatic": "0"
        }
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "hostids": [
            "10387"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-ae2d89bf})

[comment]: # ({new-f620e859})
#### Updating host encryption

Update the host "10590" to use PSK encryption only for connections from host to Zabbix server, and change the PSK identity and PSK key.
Note that the host has to be [pre-configured to use PSK](/manual/encryption/using_pre_shared_keys#configuring-psk-for-server-agent-communication-example).

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "host.update",
    "params": {
        "hostid": "10590",
        "tls_connect": 1,
        "tls_accept": 2,
        "tls_psk_identity": "PSK 002",
        "tls_psk": "e560cb0d918d26d31b4f642181f5f570ad89a390931102e5391d08327ba434e9"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "hostids": [
            "10590"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-f620e859})

[comment]: # ({new-23501347})
### Исходный код

CHost::update() в
*frontends/php/include/classes/api/services/CHost.php*.

[comment]: # ({/new-23501347})


[comment]: # ({new-a39b5a01})
### Source

CHost::update() in *ui/include/classes/api/services/CHost.php*.

[comment]: # ({/new-a39b5a01})
