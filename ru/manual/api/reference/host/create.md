[comment]: # translation:outdated

[comment]: # ({new-ce3b3e73})
# host.create

[comment]: # ({/new-ce3b3e73})

[comment]: # ({new-2e20b613})
### Описание

`объект host.create(объект/массив узлы сети)`

Этот метод позволяет создавать новые узлы сети.

[comment]: # ({/new-2e20b613})

[comment]: # ({new-48df9d2f})
### Параметры

`(объект/массив)` Создаваемые узлы сети.

В дополнение к [стандартным свойствам узла сети](object#узел_сети), этот
метод принимает следующие параметры.

|Параметр|Тип|Описание|
|----------------|------|----------------|
|**groups**<br>(требуется)|объект/массив|Группы узлов сети, в которые необходимо добавить узел сети.<br><br>Группы узлов сети должны иметь заданное свойство `groupid`.|
|**interfaces**<br>(требуется)|объект/массив|Создаваемые интерфейсы у узла сети.|
|templates|объект/массив|Шаблоны, к которым необходимо присоединить узлы сети.<br><br>Шаблоны должны иметь заданное свойство `templateid`.|
|macros|объект/массив|Добавляемые пользовательские макросы к узлу сети.|
|inventory|объект|Свойства данных инвентаризации узла сети.|

[comment]: # ({/new-48df9d2f})

[comment]: # ({new-de799b9d})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID созданных узлов сети
под свойством `hostids`. Порядок возвращаемых ID совпадает с порядком
переданных узлов сети.

[comment]: # ({/new-de799b9d})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-26320041})
#### Создание узла сети

Создание узла сети с именем "Linux server" с интерфейсом с IP адресом,
добавление его в группу, присоединение шаблона и заполнение MAC адрес
поля данных инвентаризации.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.create",
    "params": {
        "host": "Linux server",
        "interfaces": [
            {
                "type": 1,
                "main": 1,
                "useip": 1,
                "ip": "192.168.3.1",
                "dns": "",
                "port": "10050"
            }
        ],
        "groups": [
            {
                "groupid": "50"
            }
        ],
        "templates": [
            {
                "templateid": "20045"
            }
        ],
        "inventory_mode": 0,
        "inventory": {
            "macaddress_a": "01234",
            "macaddress_b": "56768"
        }
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "hostids": [
            "107819"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-26320041})

[comment]: # ({new-9572ca08})
### Смотрите также

-   [Группа узлов
    сети](/ru/manual/api/reference/hostgroup/object#группа_узлов_сети)
-   [Шаблон](/ru/manual/api/reference/template/object#шаблон)
-   [Пользовательский
    макрос](/ru/manual/api/reference/usermacro/object#макрос_узла_сети)
-   [Интерфейс узла
    сети](/ru/manual/api/reference/hostinterface/object#интерфейс_узла_сети)
-   [Данные инвентаризации узла
    сети](object#данные_инвентаризации_узлов_сети)

[comment]: # ({/new-9572ca08})

[comment]: # ({new-4c198338})
#### Creating a host with PSK encryption

Create a host called "PSK host" with PSK encryption configured.
Note that the host has to be [pre-configured to use PSK](/manual/encryption/using_pre_shared_keys#configuring-psk-for-server-agent-communication-example).

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "host.create",
    "params": {
        "host": "PSK host",
        "interfaces": [
            {
                "type": 1,
                "ip": "192.168.3.1",
                "dns": "",
                "port": "10050",
                "useip": 1,
                "main": 1
            }
        ],
        "groups": [
            {
                "groupid": "2"
            }
        ],
        "tls_accept": 2,
        "tls_connect": 2,
        "tls_psk_identity": "PSK 001",
        "tls_psk": "1f87b595725ac58dd977beef14b97461a7c1045b9a1c963065002c5473194952"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "hostids": [
            "10590"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-4c198338})

[comment]: # ({new-203ca033})
### Исходный код

CHost::create() в
*frontends/php/include/classes/api/services/CHost.php*.

[comment]: # ({/new-203ca033})


[comment]: # ({new-f7e47995})
### Source

CHost::create() in *ui/include/classes/api/services/CHost.php*.

[comment]: # ({/new-f7e47995})
