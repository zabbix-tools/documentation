[comment]: # translation:outdated

[comment]: # ({new-65bbffce})
# host.get

[comment]: # ({/new-65bbffce})

[comment]: # ({new-93035b19})
### Description

`целое число/массив host.get(объект параметры)`

Этот метод позволяет получать узлы сети в соответствии с заданными
параметрами.

[comment]: # ({/new-93035b19})

[comment]: # ({new-77a8fabb})
### Параметры

`(объект)` Параметры задают желаемый вывод.

Этот метод поддерживает следующие параметры.

|Параметр|Тип|Описание|
|----------------|------|----------------|
|groupids|строка/массив|Возврат только тех узлов сети, которые принадлежат заданным группам узлов сети.|
|applicationids|строка/массив|Возврат только тех узлов сети, которые имеют заданные группы элементов данных.|
|dserviceids|строка/массив|Возврат только тех узлов сети, которые относятся к заданным обнаруженным сервисам.|
|graphids|строка/массив|Возврат только тех узлов сети, которые имеют заданные графики.|
|hostids|строка/массив|Возврат узлов сети только с заданными ID узлов сети.|
|httptestids|строка/массив|Возврат только тех узлов сети, которые имеют заданные веб проверки.|
|interfaceids|строка/массив|Возврат только тех узлов сети, которые используют заданные интерфейсы.|
|itemids|строка/массив|Возврат только тех узлов сети, которые имеют заданные элементы данных.|
|maintenanceids|строка/массив|Возврат только тех узлов сети, на которых действуют заданные обслуживания.|
|monitored\_hosts|флаг|Возврат узлов сети только под наблюдением.|
|proxy\_hosts|флаг|Возврат только прокси.|
|proxyids|строка/массив|Возврат только тех узлов сети, которые наблюдаются заданными прокси.|
|templated\_hosts|флаг|Возврат как узлов сети, так и шаблонов.|
|templateids|строка/массив|Возврат только тех узлов сети, к которым присоединены заданные шаблоны.|
|triggerids|строка/массив|Возврат только тех узлов сети, которые имеют заданные триггеры.|
|with\_items|флаг|Возврат только тех узлов сети, у которых имеются элементы данных.<br><br>Переопределяет параметры `with_monitored_items` и `with_simple_graph_items`.|
|with\_applications|флаг|Возврат только тех узлов сети, которые имеют группы элементов данных.|
|with\_graphs|флаг|Возврат только тех узлов сети, которые имеют графики.|
|with\_httptests|флаг|Возврат только тех узлов сети, которые имеют веб проверки.<br><br>Переопределяет параметр `with_monitored_httptests`.|
|with\_monitored\_httptests|флаг|Возврат только тех узлов сети, которые имеют активированные веб проверки.|
|with\_monitored\_items|флаг|Возврат только тех узлов сети, которые имеют активированные элементы данных.<br><br>Переопределяет параметр `with_simple_graph_items`.|
|with\_monitored\_triggers|флаг|Возврат только тех узлов сети, которые имеют активированные триггеры. Все элементы данных, используемые в триггере, также должны быть активированы.|
|with\_simple\_graph\_items|флаг|Возврат только тех узлов сети, которые имеют элементы данных с числовым типом информации.|
|with\_triggers|флаг|Возврат только тех узлов сети, которые имеют триггеры.<br><br>Переопределяет параметр `with_monitored_triggers`.|
|withInventory|флаг|Возврат только тех узлов сети, которые имеют данные инвентаризации.|
|selectGroups|запрос|Возврат групп узлов сети, которым принадлежит узел сети, в свойстве `groups`.|
|selectApplications|запрос|Возврат групп элементов данных с узла сети в свойстве `applications`.<br><br>Поддерживается `count`.|
|selectDiscoveries|запрос|Возврат низкоуровневых обнаружений с узла сети в свойстве `discoveries`.<br><br>Поддерживается `count`.|
|selectDiscoveryRule|запрос|Возврат LLD правил, которые создали узел сети, в свойстве `discoveryRule`.|
|selectGraphs|запрос|Возврат графиков с узла сети в свойстве `graphs`.<br><br>Поддерживается `count`.|
|selectHostDiscovery|запрос|Возврат объекта обнаружения узла сети в свойстве `hostDiscovery`.<br><br>Объект обнаружения узлов сети связывает обнаруженный узел сети с прототипом узлов сети или прототипы узлов сети с LLD правилом и имеет следующие свойства:<br>`host` - *(строка)* имя узла сети прототипа узлов сети;<br>`hostid` - *(строка)* ID обнаруженного узла сети или прототипа узлов сети;<br>`parent_hostid` - *(строка)* ID прототипа узлов сети с которого был создан узел сети;<br>`parent_itemid` - *(строка)* ID правила LLD, которое создало обнаруженный узел сети;<br>`lastcheck` - *(штамп времени)* время, когда узел сети был в последний раз обнаружен;<br>`ts_delete` - *(штамп времени)* время, когда более необнаруживаемый узел сети будет удален.|
|selectHttpTests|запрос|Возврат веб-сценариев с узла сети в свойстве `httpTests`.<br><br>Поддерживается `count`.|
|selectInterfaces|запрос|Возврат интерфейсов узла сети в свойстве `interfaces`.<br><br>Поддерживается `count`.|
|selectInventory|запрос|Возврат данных инвентаризации узла сети с узла сети в свойстве `inventory`.|
|selectItems|запрос|Возврат элементов данных с узла сети в свойстве `items`.<br><br>Поддерживается `count`.|
|selectMacros|запрос|Возврат макросов с узла сети в свойстве `macros`.|
|selectParentTemplates|запрос|Возврат шаблонов, к которым присоединен узел сети, в свойстве `parentTemplates`.<br><br>Поддерживается `count`.|
|selectScreens|запрос|Возврат комплексных экранов с узла сети в свойстве `screens`.<br><br>Поддерживается `count`.|
|selectTriggers|запрос|Возврат триггеров с узла сети в свойстве `triggers`.<br><br>Поддерживается `count`.|
|filter|объект|Возврат только тех результатов, которые в точности соответствуют заданному фильтру.<br><br>Принимает массив, где ключи являются именами свойств и значения, которые являются либо одним значением, либо массивом сопоставляемых значений.<br><br>Позволяет фильтровать по свойствам интерфейсов.|
|limitSelects|целое число|Ограничение количества записей, возвращаемых подзапросами.<br><br>Применимо только к следующим подзапросам:<br>`selectParentTemplates` - все результаты сортируются по `host`;<br>`selectInterfaces`;<br>`selectItems` - сортируются по `name`;<br>`selectDiscoveries` - сортируются по `name`;<br>`selectTriggers` - сортируются по `description`;<br>`selectGraphs` - сортируются по `name`;<br>`selectApplications` - сортируются по `name`;<br>`selectScreens` - сортируются по `name`.|
|search|объект|Возврат результатов, которые соответствуют заданному шаблону поиска.<br><br>Принимает массив, где ключи являются именами свойств и значения являются строками по которым производить поиск. Если дополнительные опции не заданы, эта опция выполнит поиск `LIKE "%…%"`.<br><br>Позволяет выполнять поиск по свойствам интерфейсов. Работает только с текстовыми полями.|
|searchInventory|объект|Возврат только тех узлов сети, инвентарные данные которых соответствуют заданному шаблону поиска.<br><br>Этот параметр зависит от тех же дополнительных параметров что и `search`.|
|sortfield|строка/массив|Сортировка результата в соответствии с заданными свойствами.<br><br>Возможные значения: `hostid`, `host`, `name`, `status`.|
|countOutput|логический|Эти параметры являются общими для всех методов `get` и они описаны в [справочных комментариях](/ru/manual/api/reference_commentary#общие_параметры_get_метода).|
|editable|логический|^|
|excludeSearch|логический|^|
|limit|целое число|^|
|output|запрос|^|
|preservekeys|логический|^|
|searchByAny|логический|^|
|searchWildcardsEnabled|логический|^|
|sortorder|строка/массив|^|
|startSearch|логический|^|

[comment]: # ({/new-77a8fabb})

[comment]: # ({new-7223bab1})
### Возвращаемые значения

`(целое число/массив)` Возвращает либо:

-   массив объектов;
-   количество найденных объектов, если используется параметр
    `countOutput`.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-c6ea3cce})
#### Получение данных по имени

Получение всех данных о двух узлах сети с именами "Zabbix server" и
"Linux server".

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.get",
    "params": {
        "filter": {
            "host": [
                "Zabbix server",
                "Linux server"
            ]
        }
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "maintenances": [],
            "hostid": "10160",
            "proxy_hostid": "0",
            "host": "Zabbix server",
            "status": "0",
            "disable_until": "0",
            "error": "",
            "available": "0",
            "errors_from": "0",
            "lastaccess": "0",
            "ipmi_authtype": "-1",
            "ipmi_privilege": "2",
            "ipmi_username": "",
            "ipmi_password": "",
            "ipmi_disable_until": "0",
            "ipmi_available": "0",
            "snmp_disable_until": "0",
            "snmp_available": "0",
            "maintenanceid": "0",
            "maintenance_status": "0",
            "maintenance_type": "0",
            "maintenance_from": "0",
            "ipmi_errors_from": "0",
            "snmp_errors_from": "0",
            "ipmi_error": "",
            "snmp_error": "",
            "jmx_disable_until": "0",
            "jmx_available": "0",
            "jmx_errors_from": "0",
            "jmx_error": "",
            "name": "Zabbix server",
            "description": "The Zabbix monitoring server.",
            "tls_connect": "1",
            "tls_accept": "1",
            "tls_issuer": "",
            "tls_subject": "",
            "tls_psk_identity": "",
            "tls_psk": ""
        },
        {
            "maintenances": [],
            "hostid": "10167",
            "proxy_hostid": "0",
            "host": "Linux server",
            "status": "0",
            "disable_until": "0",
            "error": "",
            "available": "0",
            "errors_from": "0",
            "lastaccess": "0",
            "ipmi_authtype": "-1",
            "ipmi_privilege": "2",
            "ipmi_username": "",
            "ipmi_password": "",
            "ipmi_disable_until": "0",
            "ipmi_available": "0",
            "snmp_disable_until": "0",
            "snmp_available": "0",
            "maintenanceid": "0",
            "maintenance_status": "0",
            "maintenance_type": "0",
            "maintenance_from": "0",
            "ipmi_errors_from": "0",
            "snmp_errors_from": "0",
            "ipmi_error": "",
            "snmp_error": "",
            "jmx_disable_until": "0",
            "jmx_available": "0",
            "jmx_errors_from": "0",
            "jmx_error": "",
            "name": "Linux server",
            "description": "",
            "tls_connect": "1",
            "tls_accept": "1",
            "tls_issuer": "",
            "tls_subject": "",
            "tls_psk_identity": "",
            "tls_psk": ""
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-c6ea3cce})

[comment]: # ({new-9078aff7})
#### Получение групп узлов сети

Получение имен групп узлов сети членами которых является узел сети
"Zabbix server", без деталей о самом узле сети.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.get",
    "params": {
        "output": ["hostid"],
        "selectGroups": "extend",
        "filter": {
            "host": [
                "Zabbix server"
            ]
        }
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 2
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "hostid": "10085",
            "groups": [
                {
                    "groupid": "2",
                    "name": "Linux servers",
                    "internal": "0",
                    "flags": "0"
                },
                {
                    "groupid": "4",
                    "name": "Zabbix servers",
                    "internal": "0",
                    "flags": "0"
                }
            ]
        }
    ],
    "id": 2
}
```

[comment]: # ({/new-9078aff7})

[comment]: # ({new-59c8a46a})
#### Получение присоединенных шаблонов

Получение ID и имен шаблонов, которые присоединены к узлу сети "10084".

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.get",
    "params": {
        "output": ["hostid"],
        "selectParentTemplates": [
            "templateid",
            "name"
        ],
        "hostids": "10084"
    },
    "id": 1,
    "auth": "70785d2b494a7302309b48afcdb3a401"
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "hostid": "10084",
            "parentTemplates": [
                {
                    "name": "Template OS Linux",
                    "templateid": "10001"
                },
                {
                    "name": "Template App Zabbix Server",
                    "templateid": "10047"
                }
            ]
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-59c8a46a})

[comment]: # ({new-8be1d99c})
#### Поиск по данным инвентаризации узла сети

Получение всех узлов сети, которые содержат слово "Linux" в поле "ОС"
инвентаризации узла сети.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.get",
    "params": {
        "output": [
            "host"
        ],
        "selectInventory": [
            "os"
        ],
        "searchInventory": {
            "os": "Linux"
        }
    },
    "id": 2,
    "auth": "7f9e00124c75e8f25facd5c093f3e9a0"
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "hostid": "10084",
            "host": "Zabbix server",
            "inventory": {
                "os": "Linux Ubuntu"
            }
        },
        {
            "hostid": "10107",
            "host": "Linux server",
            "inventory": {
                "os": "Linux Mint"
            }
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-8be1d99c})

[comment]: # ({new-b4e37e5e})
### Смотрите также

-   [Группа узлов
    сети](/ru/manual/api/reference/hostgroup/object#группа_узлов_сети)
-   [Шаблон](/ru/manual/api/reference/template/object#шаблон)
-   [Пользовательский
    макрос](/ru/manual/api/reference/usermacro/object#макрос_узла_сети)
-   [Интерфейс узла
    сети](/ru/manual/api/reference/hostinterface/object#интерфейс_узла_сети)

[comment]: # ({/new-b4e37e5e})

[comment]: # ({new-9ebae84e})
### Исходный код

CHost::get() в *frontends/php/include/classes/api/services/CHost.php*.

[comment]: # ({/new-9ebae84e})




[comment]: # ({new-219d4ee1})
#### Searching hosts by problem severity

Retrieve hosts that have "Disaster" problems.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.get",
    "params": {
        "output": ["name"],
        "severities": 5
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "hostid": "10160",
            "name": "Zabbix server"
        }
    ],
    "id": 1
}
```

Retrieve hosts that have "Average" and "High" problems.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.get",
    "params": {
        "output": ["name"],
        "severities": [3, 4]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "hostid": "20170",
            "name": "Database"
        },
        {
            "hostid": "20183",
            "name": "workstation"
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-219d4ee1})

[comment]: # ({new-06c7fe93})
### See also

-   [Host group](/manual/api/reference/hostgroup/object#host_group)
-   [Template](/manual/api/reference/template/object#template)
-   [User
    macro](/manual/api/reference/usermacro/object#hosttemplate_level_macro)
-   [Host
    interface](/manual/api/reference/hostinterface/object#host_interface)

[comment]: # ({/new-06c7fe93})

[comment]: # ({new-55c08f7a})
### Source

CHost::get() in *ui/include/classes/api/services/CHost.php*.

[comment]: # ({/new-55c08f7a})
