[comment]: # translation:outdated

[comment]: # ({new-b7ea8d04})
# > Объект узла сети

Следующие объекты напрямую связаны с `host` API.

[comment]: # ({/new-b7ea8d04})

[comment]: # ({new-2f6c5c76})
### Узел сети

Объект узла сети имеет следующие свойства.

|Свойство|Тип|Описание|
|----------------|------|----------------|
|hostid|строка|*(только чтение)* ID узла сети.|
|**host**<br>(требуется)|строка|Техническое имя узла сети.|
|available|целое число|*(только чтение)* Доступность Zabbix агента.<br><br>Возможные значения:<br>0 - *(по умолчанию)* неизвестно;<br>1 - доступен;<br>2 - недоступен.|
|description|текст|Описание узла сети.|
|disable\_until|штамп времени|*(только чтение)* Время следующей проверки недоступного Zabbix агента.|
|error|строка|*(только чтение)* Текст ошибки, если Zabbix агент недоступен.|
|errors\_from|штамп времени|*(только чтение)* Время, когда Zabbix агент стал недоступен.|
|flags|целое число|*(только чтение)* Происхождение узла сети.<br><br>Возможные значения:<br>0 - обычный узел сети;<br>4 - обнаруженный узел сети.|
|inventory\_mode|целое число|Режим заполнения данных инвентаризации узла сети.<br><br>Возможные значения:<br>-1 - отключено;<br>0 - *(по умолчанию)* вручную;<br>1 - автоматически.|
|ipmi\_authtype|целое число|Алгоритм аутентификации IPMI.<br><br>Возможные значения:<br>-1 - *(по умочанию)* по умолчанию;<br>0 - нет;<br>1 - MD2;<br>2 - MD5<br>4 - straight;<br>5 - OEM;<br>6 - RMCP+.|
|ipmi\_available|целое число|*(только чтение)* Доступность IPMI агента.<br><br>Возможные значения:<br>0 - *(по умолчанию)* неизвестно;<br>1 - доступен;<br>2 - недоступен.|
|ipmi\_disable\_until|штамп времени|*(только чтение)* Время следующей проверки недоступного IPMI агента.|
|ipmi\_error|строка|*(только чтение)* Текст ошибки, если IPMI агент недоступен.|
|ipmi\_errors\_from|штамп времени|*(только чтение)* Время, когда IPMI агент стал недоступен.|
|ipmi\_password|строка|Пароль IPMI.|
|ipmi\_privilege|целое число|Уровень привилегий IPMI.<br><br>Возможные значения:<br>1 - callback;<br>2 - *(по умолчанию)* пользователь;<br>3 - оператор;<br>4 - админ;<br>5 - OEM.|
|ipmi\_username|строка|Имя пользователя IPMI.|
|jmx\_available|целое число|*(только чтение)* Доступность JMX агента.<br><br>Возможные значения:<br>0 - *(по умолчанию)* неизвестно;<br>1 - доступен;<br>2 - недоступен.|
|jmx\_disable\_until|штамп времени|*(только чтение)* Время следующей проверки недоступного JMX агента.|
|jmx\_error|строка|*(только чтение)* Текст ошибки, если JMX агент недоступен.|
|jmx\_errors\_from|штамп времени|*(только чтение)* Время, когда JMX агент стал недоступен.|
|maintenance\_from|штамп времени|*(только чтение)* Время начала действующего обслуживания.|
|maintenance\_status|целое число|*(только чтение)* Состояние действующего обслуживания.<br><br>Возможные значения:<br>0 - *(по умолчанию)* обслуживание отсутствует;<br>1 - имеется действующее обслуживание.|
|maintenance\_type|целое число|*(только чтение)* Тип действующего обслуживания.<br><br>Возможные значения:<br>0 - *(по умолчанию)* обслуживание со сбором данных;<br>1 - обслуживание без сбора данных.|
|maintenanceid|строка|*(только чтение)* ID обслуживания, которое в данный момент действует на узел сети.|
|name|строка|Видимое имя узла сети.<br><br>По умолчанию: значение свойства `host`.|
|proxy\_hostid|строка|ID прокси, который используется для наблюдения за узлом сети.|
|snmp\_available|целое число|*(только чтение)* Доступность SNMP агента.<br><br>Возможные значения:<br>0 - *(по умолчанию)* неизвестно;<br>1 - доступен;<br>2 - недоступен.|
|snmp\_disable\_until|штамп времени|*(только чтение)* Время следующей проверки недоступного SNMP агента.|
|snmp\_error|строка|*(только чтение)* Текст ошибки, если SNMP агент недоступен.|
|snmp\_errors\_from|штамп времени|*(только чтение)* Время, когда SNMP агент стал недоступен.|
|status|целое число|Состояние и функция узла сети.<br><br>Возможные значения:<br>0 - *(по умолчанию)* узел сети под наблюдением;<br>1 - узел сети без наблюдения.|
|tls\_connect|целое число|Подключения к узлу сети.<br><br>Возможные значения:<br>1 - *(по умолчанию)* Без шифрования;<br>2 - PSK;<br>4 - сертификат.|
|tls\_accept|целое число|Соединения с узла сети.<br><br>Возможные битовые значения:<br>1 - *(по умолчанию)* Без шифрования;<br>2 - PSK;<br>4 - сертификат.|
|tls\_issuer|строка|Эмитент сертификата.|
|tls\_subject|строка|Субъект сертификата.|
|tls\_psk\_identity|строка|Идентификатор PSK. Требуется, если либо в `tls_connect`, либо в `tls_accept` выбран PSK.|
|tls\_psk|строка|Pre-shared ключ, по крайней мере 32 шестнадцатеричных цифры. Требуется, если либо в `tls_connect`, либо в `tls_accept` выбран PSK.|

[comment]: # ({/new-2f6c5c76})

[comment]: # ({new-859da78d})
### Данные инвентаризации узлов сети

Объект данных инвентаризации узла сети имеет следующие свойства.

::: notetip
Каждое свойство имеет свой собственный уникальный ID
номер, который используется для связки полей данных инвентаризации узла
сети с элементами данных.
:::

|ID|Свойство|Тип|Описание|
|--|----------------|------|----------------|
|4|alias|строка|Псевдоним.|
|11|asset\_tag|строка|Этикетка владельца.|
|28|chassis|строка|Шасси.|
|23|contact|строка|Контактная информация.|
|32|contract\_number|строка|Номер контракта.|
|47|date\_hw\_decomm|строка|Дата списания HW.|
|46|date\_hw\_expiry|строка|Дата окончания обслуживания HW.|
|45|date\_hw\_install|строка|Дата установки HW.|
|44|date\_hw\_purchase|строка|Дата покупки HW.|
|34|deployment\_status|строка|Состояние развертывания.|
|14|hardware|строка|Аппаратные средства.|
|15|hardware\_full|строка|Аппаратные средства (полная детализация).|
|39|host\_netmask|строка|Маска подсети узла сети.|
|38|host\_networks|строка|Сети узла сети.|
|40|host\_router|строка|Роутер узла сети.|
|30|hw\_arch|строка|Архитектура HW.|
|33|installer\_name|строка|Имя установщика.|
|24|location|строка|Местоположение.|
|25|location\_lat|строка|Размещение (широта).|
|26|location\_lon|строка|Размещение (долгота).|
|12|macaddress\_a|строка|MAC адрес A.|
|13|macaddress\_b|строка|MAC адрес B.|
|29|model|строка|Модель.|
|3|name|строка|Имя.|
|27|notes|строка|Примечания.|
|41|oob\_ip|строка|OOB IP адрес.|
|42|oob\_netmask|строка|OOB маска подсети.|
|43|oob\_router|строка|OOB роутер.|
|5|os|строка|ОС.|
|6|os\_full|строка|ОС (полная детализация).|
|7|os\_short|строка|ОС (короткое описание.|
|61|poc\_1\_cell|строка|Первичный мобильный для контакта.|
|58|poc\_1\_email|строка|Первичный email для контакта.|
|57|poc\_1\_name|строка|Первичное имя для контакта.|
|63|poc\_1\_notes|строка|Первичные примечания для контакта.|
|59|poc\_1\_phone\_a|строка|Первичный телефон A для контакта.|
|60|poc\_1\_phone\_b|строка|Первичный телефон B для контакта.|
|62|poc\_1\_screen|строка|Первичное ник-имя для контакта.|
|68|poc\_2\_cell|строка|Вторичный мобильный для контакта.|
|65|poc\_2\_email|строка|Вторичный email для контакта.|
|64|poc\_2\_name|строка|Вторичное имя для контакта.|
|70|poc\_2\_notes|строка|Вторичные примечания для контакта.|
|66|poc\_2\_phone\_a|строка|Вторичный телефон A для контакта.|
|67|poc\_2\_phone\_b|строка|Вторичный телефон B для контакта.|
|69|poc\_2\_screen|строка|Вторичное ник-имя для контакта.|
|8|serialno\_a|строка|Серийный номер A.|
|9|serialno\_b|строка|Серийный номер B.|
|48|site\_address\_a|строка|Адрес A.|
|49|site\_address\_b|строка|Адрес B.|
|50|site\_address\_c|строка|Адрес C.|
|51|site\_city|строка|Город.|
|53|site\_country|строка|Страна.|
|56|site\_notes|строка|Заметки.|
|55|site\_rack|строка|Размещение стойки.|
|52|site\_state|строка|Область/Район.|
|54|site\_zip|строка|Почтовый индекс.|
|16|software|строка|Программное обеспечение.|
|18|software\_app\_a|строка|Программное обеспечение A.|
|19|software\_app\_b|строка|Программное обеспечение B.|
|20|software\_app\_c|строка|Программное обеспечение C.|
|21|software\_app\_d|строка|Программное обеспечение D.|
|22|software\_app\_e|строка|Программное обеспечение E.|
|17|software\_full|строка|Программное обеспечение (полная детализация).|
|10|tag|строка|Этикетка.|
|1|type|строка|Тип.|
|2|type\_full|строка|Тип (полная детализация).|
|35|url\_a|строка|URL A.|
|36|url\_b|строка|URL B.|
|37|url\_c|строка|URL C.|
|31|vendor|строка|Производитель.|

[comment]: # ({/new-859da78d})


[comment]: # ({new-0ae1807e})
### Host tag

The host tag object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**tag**<br>(required)|string|Host tag name.|
|value|string|Host tag value.|

[comment]: # ({/new-0ae1807e})
