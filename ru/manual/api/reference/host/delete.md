[comment]: # translation:outdated

[comment]: # ({new-dd2b9519})
# host.delete

[comment]: # ({/new-dd2b9519})

[comment]: # ({new-8f1cd3fa})
### Описание

`объект host.delete(массив узлы сети)`

Этот метод позволяет удалять узлы сети.

[comment]: # ({/new-8f1cd3fa})

[comment]: # ({new-cede3d95})
### Параметры

`(массив)` ID удаляемых узлов сети

[comment]: # ({/new-cede3d95})

[comment]: # ({new-54b4a855})
### Возвращаемые значения

`(Объект)` Возвращает объект, который содержит ID удаленных узлов сети
под свойством `hostids`.

[comment]: # ({/new-54b4a855})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-ef767d02})
#### Удаление нескольких узлов сети

Удаление двух узлов сети.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.delete",
    "params": [
        "13",
        "32"
    ],
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "hostids": [
            "13",
            "32"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-ef767d02})

[comment]: # ({new-9d5f3989})
### Исходный код

CHost::delete() в
*frontends/php/include/classes/api/services/CHost.php*.

[comment]: # ({/new-9d5f3989})
