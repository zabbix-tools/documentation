[comment]: # translation:outdated

[comment]: # ({new-9464a98f})
# event.get

[comment]: # ({/new-9464a98f})

[comment]: # ({new-a2311b6e})
### Описание

`целое число/массив event.get(объект параметры)`

Этот метод позволяет получать события в соответствии с заданными
параметрами.

[comment]: # ({/new-a2311b6e})

[comment]: # ({new-99511e47})
### Параметр

`(объект)` Параметры задают желаемый вывод.

Этот метод поддерживает следующие параметры.

|Параметр|Тип|Описание|
|----------------|------|----------------|
|eventids|строка/массив|Возврат событий только с заданными ID.|
|groupids|строка/массив|Возврат только тех событий, которые созданы объектами принадлежащими заданным группам узлов сети.|
|hostids|строка/массив|Возврат только тех событий, которые созданы объектами принадлежащими заданным узлам сети.|
|objectids|строка/массив|Возврат только тех событий, которые созданы заданными объектами.|
|applicationids|строка/массив|Возврат только тех событий, которые созданы объектами принадлежащими заданным группам элементов данных. Примеряется только, если объектом являются триггер или элемент данных.|
|source|целое число|Возврат событий только с заданным типом.<br><br>Обратитесь к [странице объекта события](object#событие) для получения списка поддерживаемых типов событий.<br><br>По умолчанию: 0 - события на триггеры.|
|object|целое число|Возврат только тех событий, которые созданы объектами заданного типа.<br><br>Обратитесь к [странице объекта события](object#событие) для получения списка поддерживаемых типов объекта.<br><br>По умолчанию: 0 - триггер.|
|acknowledged|логический|Если задано равным `true`, получение только подтвержденных событий.|
|suppressed|логический|`true` - возврат только подавленных событий;<br>`false` - возврат событий в нормальном состоянии.|
|severities|целое число/массив|Возврат событий только с заданными важностями события. Применяется только, если объектом является триггер.|
|evaltype|целое число|Правила поиска тегов.<br><br>Возможные значения:<br>0 - (по умолчанию) И/Или;<br>2 - Или.|
|tags|объект|Возврат событий только с заданными тегами. Точное соответствие тегу и не зависимый от регистра поиск по значению и оператору.<br>Формат: `[{"tag": "<тег>", "value": "<значение>", "operator": "<оператор>"}, ...]`.<br>По всем событиям возвращается пустой массив.<br><br>Возможные типы операторов:<br>0 - (по умолчанию) Содержит;<br>1 - Равен.|
|eventid\_from|строка|Возврат только тех событий, ID которых больше или равен заданному ID.|
|eventid\_till|строка|Возврат только тех событий, ID которых меньше или равен заданному ID.|
|time\_from|штамп времени|Возврат только тех событий, которые были созданы после или в заданное время.|
|time\_till|штамп времени|Возврат только тех событий, которые были созданы до или в заданное время.|
|value|целое число/массив|Возврат только тех событий, которые имеют заданные значения.|
|selectHosts|запрос|Возврат узлов сети содержащих объект, который создал событие, в свойстве `hosts`. Поддерживается только для событий, которые сформированы триггерами, элементами данных или правилами LLD.|
|selectRelatedObject|запрос|Возврат объекта, который создал событие, в свойстве `relatedObject`. Тип возвращаемого объекта зависит от типа события.|
|select\_alerts|запрос|Возврат оповещений, которые сгенерированы событием, в свойстве `alerts`. Оповещения отсортированы в обратном хронологическом порядке.|
|select\_acknowledges|запрос|Возврат обновлений событий в свойстве `acknowledges`. Обновления событий отсортированы в обратном хронологическом порядке.<br><br>Объект обновления события имеет следующие свойства:<br>`acknowledgeid` - `(строка)` ID подтверждения;<br>`userid` - `(строка)` ID пользователя, который обновил событие;<br>`eventid` - `(строка)` ID обновленного события;<br>`clock` - `(штамп времени)` время, когда событие было обновлено;<br>`message` - `(строка)` текст сообщения;<br>`action` - `(целое число)` действие обновления, которое было выполнено, смотрите [event.acknowledge](/ru/manual/api/reference/event/acknowledge);<br>`old_severity` - `(целое число)` важность события до этого действия обновления;<br>`new_severity` - `(целое число)` важность события после этого действия обновления;<br>`alias` - `(строка)` псевдоним пользователя, который обновил событие;<br>`name` - `(строка)` имя пользователя, который обновил событие;<br>`surname` - `(строка)` фамилия пользователя, который обновил событие.<br><br>Поддерживается `count`.|
|selectTags|запрос|Возврат тегов событий в свойстве `tags`.|
|selectSuppressionData|запрос|Возврат списка обслуживаний в свойстве `suppression_data`:<br>`maintenanceid` - *(строка)* ID обслуживания;<br>`suppress_until` - *(целое число)* время, до которого событие подавлено.|
|sortfield|строка/массив|Сортировка результата в соответствии с заданными свойствами.<br><br>Возможные значения: `eventid`, `objectid` и `clock`.|
|countOutput|логический|Эти параметры являются общими для всех методов `get` и они описаны в [справочных комментариях](/ru/manual/api/reference_commentary#общие_параметры_get_метода).|
|editable|логический|^|
|excludeSearch|логический|^|
|filter|объект|^|
|limit|целое число|^|
|output|запрос|^|
|preservekeys|логический|^|
|search|объект|^|
|searchByAny|логический|^|
|searchWildcardsEnabled|логический|^|
|sortorder|строка/массив|^|
|startSearch|логический|^|

[comment]: # ({/new-99511e47})

[comment]: # ({new-7223bab1})
### Возвращаемые значения

`(целое число/массив)` Возвращает либо:

-   массив объектов;
-   количество найденных объектов, если используется параметр
    `countOutput`.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-7bd1e923})
#### Получение событий на триггеры

Получение последних событий с триггера "13926."

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "event.get",
    "params": {
        "output": "extend",
        "select_acknowledges": "extend",
        "selectTags": "extend",
        "selectSuppressionData": "extend",
        "objectids": "13926",
        "sortfield": ["clock", "eventid"],
        "sortorder": "DESC"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "acknowledges": [
                {
                    "acknowledgeid": "1",
                    "userid": "1",
                    "eventid": "9695",
                    "clock": "1350640590",
                    "message": "Problem resolved.\n\r----[BULK ACKNOWLEDGE]----",
                    "action": "6",
                    "old_severity": "0",
                    "new_severity": "0",
                    "alias": "Admin",
                    "name": "Zabbix",
                    "surname": "Administrator"
                }
            ],
            "eventid": "9695",
            "source": "0",
            "object": "0",
            "objectid": "13926",
            "clock": "1347970410",
            "value": "1",
            "acknowledged": "1",
            "suppressed": "1",
            "ns": "413316245",
            "name": "MySQL is down",
            "severity": "5",
            "r_eventid": "0",
            "c_eventid": "0",
            "correlationid": "0",
            "userid": "0",
            "tags": [
                {
                    "tag": "service",
                    "value": "mysqld"
                },
                {
                    "tag": "error",
                    "value": ""
                }
            ],
            "suppression_data": [
                {
                    "maintenanceid": "15",
                    "suppress_until": "1472511600"
                }
            ]
        },
        {
            "acknowledges": [],
            "eventid": "9671",
            "source": "0",
            "object": "0",
            "objectid": "13926",
            "clock": "1347970347",
            "value": "0",
            "acknowledged": "0",
            "suppressed": "0",
            "ns": "0",
            "name": "Unavailable by ICMP ping",
            "severity": "4",
            "r_eventid": "0",
            "c_eventid": "0",
            "correlationid": "0",
            "userid": "0",
            "tags": [],
            "suppression_data": []
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-7bd1e923})

[comment]: # ({new-adbe5436})
#### Получение событий по периоду времени

Получение всех событий, которые созданы между 9 и 10 Октября, 2012, в
обратном хронологическом порядке.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "event.get",
    "params": {
        "output": "extend",
        "time_from": "1349797228",
        "time_till": "1350661228",
        "sortfield": ["clock", "eventid"],
        "sortorder": "desc"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "eventid": "20616",
            "source": "0",
            "object": "0",
            "objectid": "14282",
            "clock": "1350477814",
            "value": "1",
            "acknowledged": "0",
            "suppressed": "0",
            "ns": "0",
            "name": "Less than 25% free in the history cache",
            "severity": "3",
            "r_eventid": "0",
            "c_eventid": "0",
            "correlationid": "0",
            "userid": "0"
        },
        {
            "eventid": "20617",
            "source": "0",
            "object": "0",
            "objectid": "14283",
            "clock": "1350477814",
            "value": "0",
            "acknowledged": "0",
            "suppressed": "0",
            "ns": "0",
            "name": "Zabbix trapper processes more than 75% busy",
            "severity": "3",
            "r_eventid": "0",
            "c_eventid": "0",
            "correlationid": "0",
            "userid": "0"
        },
        {
            "eventid": "20618",
            "source": "0",
            "object": "0",
            "objectid": "14284",
            "clock": "1350477815",
            "value": "1",
            "acknowledged": "0",
            "suppressed": "0",
            "ns": "0",
            "name": "High ICMP ping loss",
            "severity": "3",
            "r_eventid": "0",
            "c_eventid": "0",
            "correlationid": "0",
            "userid": "0"
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-adbe5436})

[comment]: # ({new-by})
#### Retrieving events acknowledged by specified user

Retrieving events acknowledged by user with ID=10

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "event.get",
    "params": {
        "output": "extend",
        "select_acknowledges": ["userid", "action"],
        "filter": {
            "action": 2,
            "action_userid": 10
        },
        "sortfield": ["eventid"],
        "sortorder": "DESC"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": [
        {
            "eventid": "1248566",
            "source": "0",
            "object": "0",
            "objectid": "15142",
            "clock": "1472457242",
            "ns": "209442442",
            "r_eventid": "1245468",
            "r_clock": "1472457285",
            "r_ns": "125644870",
            "correlationid": "0",
            "userid": "10",
            "name": "Zabbix agent on localhost is unreachable for 5 minutes",
            "acknowledged": "1",
            "severity": "3",
            "cause_eventid": "0",
            "opdata": "",
            "acknowledges": [
                {
                    "userid": "10",
                    "action": "2"
                }
            ],
            "suppressed": "0"
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-by})

[comment]: # ({new-c6eb3ba0})
### Смотрите также

-   [Оповещение](/ru/manual/api/reference/alert/object)
-   [Элемент данных](/ru/manual/api/reference/item/object)
-   [Узел сети](/ru/manual/api/reference/host/object)
-   [Правило
    LLD](/ru/manual/api/reference/discoveryrule/object#правило_lld)
-   [Триггер](/ru/manual/api/reference/trigger/object)

[comment]: # ({/new-c6eb3ba0})

[comment]: # ({new-f85dc7ca})
### Исходный код

CEvent::get() в *frontends/php/include/classes/api/services/CEvent.php*.

[comment]: # ({/new-f85dc7ca})
