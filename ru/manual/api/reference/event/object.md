[comment]: # translation:outdated

[comment]: # ({new-bef38847})
# > Объект события

Следующие объекты напрямую связаны с `event` API.

[comment]: # ({/new-bef38847})

[comment]: # ({new-355941f0})
### Событие

::: noteclassic
События создаются Zabbix сервером и их нельзя менять через
API.
:::

Объект события содержит следующие свойства.

|Свойство|Тип|Описание|
|----------------|------|----------------|
|eventid|строка|ID события.|
|source|целое число|Тип события.<br><br>Возможные значения:<br>0 - событие создано триггером;<br>1 - событие создано правилом обнаружения;<br>2 - событие создано авторегистрацией активного агента;<br>3 - внутреннее событие.|
|object|целое число|Тип объекта, к которому относится событие.<br><br>Возможные значения для событий на триггера:<br>0 - триггер.<br><br>Возможные значения для событий на обнаружения:<br>1 - обнаруженный узел сети;<br>2 - обнаруженный сервис.<br><br>Возможные значения для событий на авторегистрацию:<br>3 - автоматически зарегистрированный узел сети.<br><br>Возможные значения для внутренних событий:<br>0 - триггер;<br>4 - элемент данных;<br>5 - правило LLD.|
|objectid|строка|ID связанного объекта.|
|acknowledged|целое число|Подтверждено ли событие.|
|clock|штамп времени|Время, когда событие было создано.|
|ns|целое число|Наносекунды, когда событие было создано.|
|name|строка|Имя решённого события.|
|value|целое число|Состояние связанного объекта.<br><br>Возможные значения для событий на триггера:<br>0 - ОК;<br>1 - проблема.<br><br>Возможные значения для событий на обнаружения:<br>0 - узел сети или сервис доступны;<br>1 - узел сети или сервис недоступны;<br>2 - узел сети или сервис обнаружены;<br>3 - узел сети или сервис потеряны.<br><br>Возможные значения для событий на авторегистрацию:<br>0 - "нормальное" состояние;<br>1 - "неизвестное" или "неподдерживаемое" состояние.<br><br>Этот параметр не используется для событий на авторегистрацию активных агентов.|
|severity|целое число|Текущая важность события.<br><br>Возможные значения:<br>0 - не классифицировано;<br>1 - информационная;<br>2 - предупреждение;<br>3 - средняя;<br>4 - высокая;<br>5 - чрезвычайная.|
|r\_eventid|строка|ID события восстановлении|
|c\_eventid|строка|ID события о проблеме, которое сгенерировало OK событие|
|correlationid|строка|ID корреляции|
|userid|строка|ID пользователя, который закрыл событие вручную.|
|suppressed|целое число|*(только чтение)* Подавлено ли событие или нет.<br><br>Возможные значения:<br>0 - событие в нормальном режиме;<br>1 - событие подавлено.|

[comment]: # ({/new-355941f0})

[comment]: # ({new-b89991b4})
### Event tag

The event tag object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|tag|string|Event tag name.|
|value|string|Event tag value.|

[comment]: # ({/new-b89991b4})


[comment]: # ({new-52ff7df7})
### Media type URLs

Object with media type url have the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|name|string|Media type defined URL name.|
|url|string|Media type defined URL value.|

Results will contain entries only for active media types with enabled
event menu entry. Macro used in properties will be expanded, but if one
of properties contain non expanded macro both properties will be
excluded from results. Supported macros described on
[page](/manual/appendix/macros/supported_by_location).

[comment]: # ({/new-52ff7df7})
