[comment]: # translation:outdated

[comment]: # ({new-a7557797})
# trigger.update

[comment]: # ({/new-a7557797})

[comment]: # ({new-476a7855})
### Описание

`объект trigger.update(объект/массив триггеры)`

Этот метод позволяет обновлять существующие триггеры.

[comment]: # ({/new-476a7855})

[comment]: # ({new-b918ffdf})
### Параметры

`(объект/массив)` Свойства триггеров, которые будут обновлены.

Свойство `triggerid` должно быть указано по каждому триггеру, все
остальные свойства опциональны. Будут обновлены только переданные
свойства, все остальные останутся неизменными.

В дополнение к [стандартным свойствам триггера](object#триггер), этот
метод принимает следующие параметры.

|Параметр|Тип|Описание|
|----------------|------|----------------|
|dependencies|массив|Триггеры, от которых обновляемый триггер зависит.<br><br>У триггеров должно быть задано свойство `triggerid`.|
|tags|массив|Теги триггера.|

::: noteimportant
Выражение триггера необходимо указывать в
раскрытой форме.
:::

[comment]: # ({/new-b918ffdf})

[comment]: # ({new-18d0cc04})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID обновленных триггеров
под свойством `triggerids`.

[comment]: # ({/new-18d0cc04})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-07c64481})
#### Активация триггера

Активация триггера, то есть изменение его состояния на значение "0".

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "trigger.update",
    "params": {
        "triggerid": "13938",
        "status": 0
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "triggerids": [
            "13938"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-07c64481})

[comment]: # ({new-b2bab7ce})
#### Замена тегов у триггеров

Замена тегов у триггера.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "trigger.update",
    "params": {
        "triggerid": "13938",
        "tags": [
            {
                "tag": "service",
                "value": "{{ITEM.VALUE}.regsub(\"Service (.*) has stopped\", \"\\1\")}"
            },
            {
                "tag": "error",
                "value": ""
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "triggerids": [
            "13938"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-b2bab7ce})

[comment]: # ({new-db463204})
#### Replacing dependencies

Replace dependencies for trigger.

Request:

```json
{
    "jsonrpc": "2.0",
    "method": "trigger.update",
    "params": {
        "triggerid": "22713",
        "dependencies": [
            {
                "triggerid": "22712"
            },
            {
                "triggerid": "22772"
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "triggerids": [
            "22713"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-db463204})


[comment]: # ({new-180a8172})
### Исходный код

CTrigger::update() в
*frontends/php/include/classes/api/services/CTrigger.php*.

[comment]: # ({/new-180a8172})
