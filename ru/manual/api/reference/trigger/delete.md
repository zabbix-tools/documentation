[comment]: # translation:outdated

[comment]: # ({new-bce5854d})
# trigger.delete

[comment]: # ({/new-bce5854d})

[comment]: # ({new-adb8b388})
### Описание

`объект trigger.delete(массив triggerIds)`

Этот метод позволяет удалять триггеры.

[comment]: # ({/new-adb8b388})

[comment]: # ({new-fc913e85})
### Параметры

`(массив)` ID удаляемых триггеров.

[comment]: # ({/new-fc913e85})

[comment]: # ({new-a2fb984a})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID удаленных триггеров
под свойством `triggerids`.

[comment]: # ({/new-a2fb984a})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-752e2ebb})
#### Удаление нескольких триггеров

Удаление двух триггеров.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "trigger.delete",
    "params": [
        "12002",
        "12003"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "triggerids": [
            "12002",
            "12003"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-752e2ebb})

[comment]: # ({new-087c0548})
### Исходный код

CTrigger::delete() в
*frontends/php/include/classes/api/services/CTrigger.php*.

[comment]: # ({/new-087c0548})
