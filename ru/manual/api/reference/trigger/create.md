[comment]: # translation:outdated

[comment]: # ({new-56486046})
# trigger.create

[comment]: # ({/new-56486046})

[comment]: # ({new-8efa90c6})
### Описание

`объект trigger.create(объект/массив триггеры)`

Этот метод позволяет создавать новые триггеры.

[comment]: # ({/new-8efa90c6})

[comment]: # ({new-c2c3e90f})
### Параметры

`(объект/массив)` Создаваемые триггеры.

В дополнение к [стандартным свойствам триггера](object#триггер), этот
метод принимает следующие параметры.

|Параметр|Тип|Описание|
|----------------|------|----------------|
|dependencies|массив|Триггеры, от которых будет зависеть создаваемый триггер.<br><br>У триггеров должно быть задано свойство `triggerid`.|
|tags|массив|Теги триггера.|

::: noteimportant
Выражение триггера необходимо указывать в
раскрытой форме.
:::

[comment]: # ({/new-c2c3e90f})

[comment]: # ({new-4938d22d})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID созданных триггеров
под свойством `triggerids`. Порядок возвращаемых ID совпадает с порядком
переданных триггеров.

[comment]: # ({/new-4938d22d})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-f57689d3})
#### Создание триггера

Создание триггера с одной зависимостью триггера.

Запрос:

``` {.java}
<code java>
{
    "jsonrpc": "2.0",
    "method": "trigger.create",
    "params": [
        {
            "description": "Processor load is too high on {HOST.NAME}",
            "expression": "{Linux server:system.cpu.load[percpu,avg1].last()}>5",
            "dependencies": [
                {
                    "triggerid": "17367"
                }
            ]
        },
        {
            "description": "Service status",
            "expression": "{Linux server:log[/var/log/system,Service .* has stopped].strlen()}<>0",
            "dependencies": [
                {
                    "triggerid": "17368"
                }
            ],
            "tags": [
                {
                    "tag": "service",
                    "value": "{{ITEM.VALUE}.regsub(\"Service (.*) has stopped\", \"\\1\")}"
                },
                {
                    "tag": "error",
                    "value": ""
                }
            ]
        }
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "triggerids": [
            "17369",
            "17370"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-f57689d3})

[comment]: # ({new-7d49da78})
### Исходный код

CTrigger::create() в
*frontends/php/include/classes/api/services/CTrigger.php*.

[comment]: # ({/new-7d49da78})
