[comment]: # translation:outdated

[comment]: # ({new-fc5b81d2})
# trigger.get

[comment]: # ({/new-fc5b81d2})

[comment]: # ({new-e77e3d1e})
### Описание

`целое число/массив trigger.get(объект параметры)`

Этот метод позволяет получать триггеры в соответствии с заданными
параметрами.

[comment]: # ({/new-e77e3d1e})

[comment]: # ({new-44938a82})
### Параметры

`(объект)` Параметры задают желаемый вывод.

Этот метод поддерживает следующие параметры.

|Параметр|Тип|Описание|
|----------------|------|----------------|
|triggerids|строка/массив|Возврат триггеров только с заданными ID.|
|groupids|строка/массив|Возврат только тех триггеров, которые принадлежат узлам сети из заданных групп узлов сети.|
|templateids|строка/массив|Возврат только тех триггеров, которые принадлежат заданным шаблонам.|
|hostids|строка/массив|Возврат только тех триггеров, которые принадлежат заданным узлам сети.|
|itemids|строка/массив|Возврат только тех триггеров, которые содержат заданные элементы данных.|
|applicationids|строка/массив|Возврат только тех триггеров, которые содержат элементы данных из заданных групп элементов данных.|
|functions|строка/массив|Возврат только тех триггеров, которые используют заданные функции.<br><br>Обратитесь к странице [поддерживаемых функций триггеров](/ru/manual/appendix/triggers/functions) для получения списка поддерживаемых функций.|
|group|строка|Возврат только тех триггеров, которые принадлежат узлам сети из группы узлов сети с заданным именем.|
|host|строка|Возврат только тех триггеров, которые принадлежат узлу сети с заданным именем.|
|inherited|логический|Если задано значение `true`, возвращать только те триггеры, которые унаследованы из шаблона.|
|templated|логический|Если задано значение `true`, возвращать только те триггеры, которые принадлежат шаблонам.|
|monitored|флаг|Возврат только активированных триггеров, которые принадлежат узлам сети под наблюдением и содержат только активированные элементы данных.|
|active|флаг|Возврат только активированных триггеров, которые принадлежат узлам сети под наблюдением.|
|maintenance|логический|Если задано значение `true`, возвращать только активированные триггера, которые принадлежат узлам сети в обслуживании.|
|withUnacknowledgedEvents|флаг|Возврат только тех триггеров, у которых имеются неподтвержденные события.|
|withAcknowledgedEvents|флаг|Возврат только тех триггеров, все события которых подтверждены.|
|withLastEventUnacknowledged|флаг|Возврат только тех триггеров, последние события которых неподтверждены.|
|skipDependent|флаг|Пропуск триггеров в состоянии проблема, которые зависят от других триггеров. Обратите внимание, что другие триггеры игнорируется, если они деактивированы или имеют деактивированные элементы данных или деактивированные узлы сети элементов данных.|
|lastChangeSince|штамп времени|Возврат только тех триггеров, которые изменили своё состояние после заданного времени.|
|lastChangeTill|штамп времени|Возврат только тех триггеров, которые изменили своё состояние до заданного времени.|
|only\_true|флаг|Возврат только тех триггеров, которые недавно были в состоянии проблема.|
|min\_severity|целое число|Возврат только тех триггеров, у которых важность больше или равна заданной важности.|
|expandComment|флаг|Раскрытие макросов в описании к триггеру.|
|expandDescription|флаг|Раскрытие макросов в имени триггера.|
|expandExpression|флаг|Раскрытие функций и макросов в выражении триггера.|
|selectGroups|запрос|Возврат групп узлов сети, которым принадлежит триггер, в свойстве `groups`.|
|selectHosts|запрос|Возврат узлов сети, которым принадлежит триггер, в свойстве `hosts`.|
|selectItems|запрос|Возврат элементов данных, которые содержатся в выражении триггера, в свойстве `items`.|
|selectFunctions|запрос|Возврат функций, которые используются в триггере, в свойстве `functions`.<br><br>Объект функции представляет собой функции, которые используются в выражении триггера, и имеет следующие свойства:<br>`functionid` - *(строка)* ID функции;<br>`itemid` - *(строка)* ID элемента данных, который используется в функции;<br>`function` - *(строка)* имя функции;<br>`parameter` - *(строка)* переданный параметр в функцию.|
|selectDependencies|запрос|Возврат триггеров, от которых зависит триггер, в свойстве `dependencies`.|
|selectDiscoveryRule|запрос|Возврат правила низкоуровневого правила обнаружения, которое создало триггер.|
|selectLastEvent|запрос|Возврат последнего значимого события триггера в свойстве `lastEvent`.|
|selectTags|запрос|Возврат тегов триггера в свойстве `tags`.|
|selectTriggerDiscovery|запрос|Возврат объекта обнаружения триггеров в свойстве `triggerDiscovery`. Объекты обнаружения триггеров ссылаются от триггера к прототипу триггеров, с которого этот триггер был создан.<br><br>Этот параметр имеет следующие свойства:<br>`parent_triggerid` - `(строка)` ID прототипа триггеров с которого был создан триггер.|
|filter|объект|Возврат только тех результатов, которые в точности соответствуют заданному фильтру.<br><br>Принимает массив, где ключи являются именами свойств и значения, которые являются либо одним значением, либо массивом сопоставляемых значений.<br><br>Поддерживает дополнительные фильтры:<br>`host` - техническое имя узла сети, которому принадлежит триггер;<br>`hostid` - ID узла сети, которому принадлежит триггер.|
|limitSelects|целое число|Ограничение количества записей, возвращаемых подзапросами.<br><br>Применимо только к следующим подзапросам:<br>`selectHosts` - результаты сортируются по `host`.|
|sortfield|строка/массив|[Сортировка](/ru/manual/api/reference_commentary#общие_параметры_get_метода) результата в соответствии с заданными свойствами.<br><br>Возможные значения: `triggerid`, `description`, `status`, `priority`, `lastchange` и `hostname`.|
|countOutput|логический|Эти параметры являются общими для всех методов `get` и они описаны в [справочных комментариях](/ru/manual/api/reference_commentary#общие_параметры_get_метода).|
|editable|логический|^|
|excludeSearch|логический|^|
|limit|целое число|^|
|output|запрос|^|
|preservekeys|логический|^|
|search|объект|^|
|searchByAny|логический|^|
|searchWildcardsEnabled|логический|^|
|sortorder|строка/массив|^|
|startSearch|логический|^|

[comment]: # ({/new-44938a82})

[comment]: # ({new-7223bab1})
### Возвращаемые значения

`(целое число/массив)` Возвращает либо:

-   массив объектов;
-   количество найденных объектов, если используется параметр
    `countOutput`.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-3d27e76d})
#### Получение данных по ID триггера

Получение всех данных и функций, которые используются в триггере
"14062".

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "trigger.get",
    "params": {
        "triggerids": "14062",
        "output": "extend",
        "selectFunctions": "extend"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "functions": [
                {
                    "functionid": "13513",
                    "itemid": "24350",
                    "function": "diff",
                    "parameter": "0"
                }
            ],
            "triggerid": "14062",
            "expression": "{13513}>0",
            "description": "/etc/passwd has been changed on {HOST.NAME}",
            "url": "",
            "status": "0",
            "value": "0",
            "priority": "2",
            "lastchange": "0",
            "comments": "",
            "error": "",
            "templateid": "10016",
            "type": "0",
            "state": "0",
            "flags": "0",
            "recovery_mode": "0",
            "recovery_expression": "",
            "correlation_mode": "0",
            "correlation_tag": "",
            "manual_close": "0"
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-3d27e76d})

[comment]: # ({new-8a31301f})
#### Получение триггеров в состоянии проблема

Получение ID, имени и важности всех триггеров в состоянии проблема и
сортировка их по важности в порядке убывания.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "trigger.get",
    "params": {
        "output": [
            "triggerid",
            "description",
            "priority"
        ],
        "filter": {
            "value": 1
        },
        "sortfield": "priority",
        "sortorder": "DESC"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "triggerid": "13907",
            "description": "Zabbix self-monitoring processes < 100% busy",
            "priority": "4"
        },
        {
            "triggerid": "13824",
            "description": "Zabbix discoverer processes more than 75% busy",
            "priority": "3"
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-8a31301f})

[comment]: # ({new-8005126d})
#### Получение заданного триггера с тегами

Получение заданного триггера с тегами.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "trigger.get",
    "params": {
        "output": [
            "triggerid",
            "description"
        ],
        "selectTags": "extend",
        "triggerids": [
            "17578"
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "triggerid": "17370",
            "description": "Service status",
            "tags": [
                {
                    "tag": "service",
                    "value": "{{ITEM.VALUE}.regsub(\"Service (.*) has stopped\", \"\\1\")}"
                },
                {
                    "tag": "error",
                    "value": ""
                }
            ]
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-8005126d})

[comment]: # ({new-222d46a0})
### Смотрите также

-   [Правило
    обнаружения](/ru/manual/api/reference/discoveryrule/object#правило_обнаружения)
-   [Элемент
    данных](/ru/manual/api/reference/item/object#элемент_данных)
-   [Узел сети](/ru/manual/api/reference/host/object#узел_сети)
-   [Группа узлов
    сети](/ru/manual/api/reference/hostgroup/object#группа_узлов_сети)

[comment]: # ({/new-222d46a0})

[comment]: # ({new-07515392})
### Исходный код

CTrigger::get() в
*frontends/php/include/classes/api/services/CTrigger.php*.

[comment]: # ({/new-07515392})
