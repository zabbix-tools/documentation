[comment]: # translation:outdated

[comment]: # ({new-bc1a928b})
# map.delete

[comment]: # ({/new-bc1a928b})

[comment]: # ({new-24398e69})
### Описание

`объект map.delete(массив mapIds)`

Этот метод позволяет удалять карты сетей.

[comment]: # ({/new-24398e69})

[comment]: # ({new-57a8b77a})
### Параметры

`(массив)` ID удаляемых карт сетей.

[comment]: # ({/new-57a8b77a})

[comment]: # ({new-f04e039f})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID удаленных карт сетей
под свойством `sysmapids`.

[comment]: # ({/new-f04e039f})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-87539545})
#### Удаление нескольких карт сетей

Удаление двух карт сетей.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "map.delete",
    "params": [
        "12",
        "34"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "sysmapids": [
            "12",
            "34"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-87539545})

[comment]: # ({new-4880bc2b})
### Исходный код

CMap::delete() в *frontends/php/include/classes/api/services/CMap.php*.

[comment]: # ({/new-4880bc2b})
