[comment]: # translation:outdated

[comment]: # ({new-df556223})
# map.update

[comment]: # ({/new-df556223})

[comment]: # ({new-821c3f80})
### Описание

`объект map.update(объект/массив карты)`

Этот метод позволяет обновлять существующие карты сетей.

[comment]: # ({/new-821c3f80})

[comment]: # ({new-7a706637})
### Параметры

`(объект/массив)` Свойства карт сетей, которые будут обновлены.

Свойство `mapid` должно быть указано по каждой карте сети, все остальные
свойства опциональны. Будут обновлены только переданные свойства, все
остальные останутся неизменными.

В дополнение к [стандартным свойствам карты сети](object#карта_сети),
этот метод принимает следующие параметры.

|Параметр|Тип|Описание|
|----------------|------|----------------|
|links|массив|Связи карты, которые заменят текущие связи.|
|selements|массив|Элементы карты, которые заменят текущие элементы.|
|urls|массив|URL'ы карты, которые заменят текущие URL'ы.|
|users|массив|Пользователь карты, который заменит существующие разрешения на общий доступ к карте сети.|
|userGroups|массив|Группа пользователей карты, которая заменит существующие разрешения на общий доступ к карте сети.|
|shapes|массив|Фигуры карты, которые заменят текущие фигуры.|
|lines|массив|Линии карты, которые заменят текущие линии.|

::: notetip
Для создания связей на карте сети вам необходимо задать
элементам карты произвольное значение `selementid` и затем использовать
это значение для ссылки на этот элемент в свойствах связи `selementid1`
или `selementid2`. Когда элемент будет создан, это значение заменится
корректным ID, который сгенерирует Zabbix. [Смотрите пример для
map.create](create#создание_карты_сети_с_узлами_сети).
:::

[comment]: # ({/new-7a706637})

[comment]: # ({new-f1256f7a})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID обновленных карт сетей
под свойством `sysmapids`.

[comment]: # ({/new-f1256f7a})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-6df0be65})
#### Изменение размера карты сети

Изменение размера карты сети на 1200x1200 пикселей.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "map.update",
    "params": {
        "sysmapid": "8",
        "width": 1200,
        "height": 1200
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "sysmapids": [
            "8"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-6df0be65})

[comment]: # ({new-76eb7f1c})
#### Изменение владельца карты

Доступно только администраторам и супер-администраторам.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "map.update",
    "params": {
        "sysmapid": "9",
        "userid": "1"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 2
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "sysmapids": [
            "9"
        ]
    },
    "id": 2
}
```

[comment]: # ({/new-76eb7f1c})

[comment]: # ({new-6c6205f1})
### Смотрите также

-   [Элемент карты](object#элемент_карты)
-   [Связь карты](object#связь_карты)
-   [URL карты](object#url_карты)
-   [Пользователь карты](object#пользователь_карты)
-   [Группа пользователей карты](object#группа_пользователей_карты)
-   [Фигура карты](object#фигуры_карты)
-   [Линия карты](object#линии_карты)

[comment]: # ({/new-6c6205f1})

[comment]: # ({new-2f149f32})
### Исходный код

CMap::update() в *frontends/php/include/classes/api/services/CMap.php*.

[comment]: # ({/new-2f149f32})
