[comment]: # translation:outdated

[comment]: # ({new-0067e4a0})
# map.create

[comment]: # ({/new-0067e4a0})

[comment]: # ({new-279559e4})
### Описание

`объект map.create(объект/массив карты)`

Этот метод позволяет создавать новые карты сетей.

[comment]: # ({/new-279559e4})

[comment]: # ({new-73d879ea})
### Параметры

`(объект/массив)` Создаваемые карты сетей.

В дополнение к [стандартным свойствам карты сети](object#карта_сети),
этот метод принимает следующие параметры.

|Параметр|Тип|Описание|
|----------------|------|----------------|
|links|массив|Создаваемые связи карты на карте сети.|
|selements|массив|Создаваемые элементы карты на карте сети.|
|urls|массив|Создаваемые URL'ы на карте сети.|
|users|массив|Пользователь карты, которому будет предоставлен общий доступ к карте сети.|
|userGroups|массив|Группа пользователей карты, которой будет предоставлен общий доступ к карте сети.|
|shapes|массив|Создаваемые фигуры карты на карте сети.|
|lines|массив|Создаваемые линии карты на карте сети.|

::: notetip
Для создания связей на карте сети вам необходимо задать
элементам карты произвольное значение `selementid` и затем использовать
это значение для ссылки на этот элемент в свойствах связи `selementid1`
или `selementid2`. Когда элемент будет создан, это значение заменится
корректным ID, который сгенерирует Zabbix. [Смотрите
пример](create#создание_карты_сети_с_узлами_сети).
:::

[comment]: # ({/new-73d879ea})

[comment]: # ({new-2e617567})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID созданных карт сетей
под свойством `sysmapids`. Порядок возвращаемых ID совпадает с порядком
переданных карт сетей.

[comment]: # ({/new-2e617567})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-584ef8f6})
#### Создание пустой карты сети

Создание карты сети без элементов.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "map.create",
    "params": {
        "name": "Map",
        "width": 600,
        "height": 600
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "sysmapids": [
            "8"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-584ef8f6})

[comment]: # ({new-e32540ab})
#### Создание карты сети с узлами сети

Создание карты сети с двумя элементами узлов сети и связи между ними.
Обратите внимание на использование временных значений "selementid1" и
"selementid2" в объекте связи на карте для ссылки на элементы карты.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "map.create",
    "params": {
        "name": "Host map",
        "width": 600,
        "height": 600,
        "selements": [
            {
                "selementid": "1",
                "elements": [
                    {"hostid": "1033"}
                ],
                "elementtype": 0,
                "iconid_off": "2"
            },

            {
                "selementid": "2",
                "elements": [
                    {"hostid": "1037"}
                ],
                "elementtype": 0,
                "iconid_off": "2"
            }
        ],
        "links": [
            {
                "selementid1": "1",
                "selementid2": "2"
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "sysmapids": [
            "9"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-e32540ab})

[comment]: # ({new-9a8ffa0d})
#### Создание карты с триггерами

Создание карты сети с элементом триггера, который содержит два триггера.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "map.create",
    "params": {
        "name": "Trigger map",
        "width": 600,
        "height": 600,
        "selements": [
            {
                "elements": [
                    {"triggerid": "12345"},
                    {"triggerid": "67890"}
                ],
                "elementtype": 2,
                "iconid_off": "2"
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "sysmapids": [
            "10"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-9a8ffa0d})

[comment]: # ({new-d8024143})
#### Общий доступ к карте сети

Создание карты сети с двумя типами общего доступа (пользователю и группе
пользователей).

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "map.create",
    "params": {
        "name": "Map sharing",
        "width": 600,
        "height": 600,
        "users": [
            {
                "userid": "4",
                "permission": "3"
            }
        ],
        "userGroups": [
            {
                "usrgrpid": "7",
                "permission": "2"
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "sysmapids": [
            "9"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-d8024143})

[comment]: # ({new-2fa44a8b})
#### Фигуры на карте

Создание карты сети с заголовком имени карты.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "map.create",
    "params": {
        "name": "Host map",
        "width": 600,
        "height": 600,
        "shapes": [
            {
                "type": 0,
                "x": 0,
                "y": 0,
                "width": 600,
                "height": 11,
                "text": "{MAP.NAME}"
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "sysmapids": [
            "10"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-2fa44a8b})

[comment]: # ({new-5aa58242})
#### Линии на карте

Создание линии карты.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "map.create",
    "params": {
        "name": "Map API lines",
        "width": 500,
        "height": 500,
        "lines": [
            {
                "x1": 30,
                "y1": 10,
                "x2": 100,
                "y2": 50,
                "line_type": 1,
                "line_width": 10,
                "line_color": "009900"
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "sysmapids": [
            "11"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-5aa58242})

[comment]: # ({new-436079d5})
### Смотрите также

-   [Элемент карты](object#элемент_карты)
-   [Связь карты](object#связь_карты)
-   [URL карты](object#url_карты)
-   [Пользователь карты](object#пользователь_карты)
-   [Группа пользователей карты](object#группа_пользователей_карты)
-   [Фигура карты](object#фигуры_карты)
-   [Линия карты](object#линии_карты)

[comment]: # ({/new-436079d5})

[comment]: # ({new-a9533b09})
### Исходный код

CMap::create() в *frontends/php/include/classes/api/services/CMap.php*.

[comment]: # ({/new-a9533b09})
