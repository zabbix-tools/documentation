[comment]: # translation:outdated

[comment]: # ({new-14e441a9})
# map.get

[comment]: # ({/new-14e441a9})

[comment]: # ({new-480bee08})
### Описание

`целое число/массив map.get(объект параметры)`

Этот метод позволяет получать карты сетей в соответствии с заданными
параметрами.

[comment]: # ({/new-480bee08})

[comment]: # ({new-c0fdc6dd})
### Параметры

`(объект)` Параметры задают желаемый вывод.

Этот метод поддерживает следующие параметры.

|Параметр|Тип|Описание|
|----------------|------|----------------|
|sysmapids|строка/массив|Возврат карт сетей только с заданными ID.|
|userids|строка/массив|Возврат только тех карт сетей, которые принадлежат заданным ID пользователей.|
|expandUrls|флаг|Добавление глобальных URL карты в соответствующие элементы карты и раскрытие макросов во всех URL элементах карты сети.|
|selectIconMap|запрос|Возврат соответствия иконок, которое используется картой сети, в свойстве `iconmap`.|
|selectLinks|запрос|Возврат связей между элементами карты сети в свойстве `links`.|
|selectSelements|запрос|Возврат элементов карты с карты сети в свойстве `selements`.|
|selectUrls|запрос|Возврат URL'ов карты в свойстве `urls`.|
|selectUsers|запрос|Возврат пользователей, которым предоставлен общий доступ к карте сети, в свойстве `users`.|
|selectUserGroups|запрос|Возврат групп пользователей, которым предоставлен общий доступ к карте сети, в свойстве `userGroups`.|
|selectShapes|запрос|Возврат фигур карты с карты сети в свойстве `shapes`.|
|selectLines|запрос|Возврат линий карты с карты сети в свойстве `lines`.|
|sortfield|строка/массив|Сортировка результата в соответствии с заданными свойствами.<br><br>Возможные значения: `name`, `width` и `height`.|
|countOutput|логический|Эти параметры являются общими для всех методов `get` и они описаны в [справочных комментариях](/ru/manual/api/reference_commentary#общие_параметры_get_метода).|
|editable|логический|^|
|excludeSearch|логический|^|
|filter|объект|^|
|limit|целое число|^|
|output|запрос|^|
|preservekeys|логический|^|
|search|объект|^|
|searchByAny|логический|^|
|searchWildcardsEnabled|логический|^|
|sortorder|строка/массив|^|
|startSearch|логический|^|

[comment]: # ({/new-c0fdc6dd})

[comment]: # ({new-7223bab1})
### Возвращаемые значения

`(целое число/массив)` Возвращает либо:

-   массив объектов;
-   количество найденных объектов, если используется параметр
    `countOutput`.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-cb769d11})
#### Получение карты сети

Получение всех данных карты сети "3".

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "map.get",
    "params": {
        "output": "extend",
        "selectSelements": "extend",
        "selectLinks": "extend",
        "selectUsers": "extend",
        "selectUserGroups": "extend",
        "selectShapes": "extend",
        "selectLines": "extend",
        "sysmapids": "3"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "selements": [
                {
                    "selementid": "10",
                    "sysmapid": "3",
                    "elementtype": "4",
                    "iconid_off": "1",
                    "iconid_on": "0",
                    "label": "Zabbix server",
                    "label_location": "3",
                    "x": "11",
                    "y": "141",
                    "iconid_disabled": "0",
                    "iconid_maintenance": "0",
                    "elementsubtype": "0",
                    "areatype": "0",
                    "width": "200",
                    "height": "200",
                    "viewtype": "0",
                    "use_iconmap": "1",
                    "application": "",
                    "urls": [],
                    "elements": []
                },
                {
                    "selementid": "11",
                    "sysmapid": "3",
                    "elementtype": "4",
                    "iconid_off": "1",
                    "iconid_on": "0",
                    "label": "Web server",
                    "label_location": "3",
                    "x": "211",
                    "y": "191",
                    "iconid_disabled": "0",
                    "iconid_maintenance": "0",
                    "elementsubtype": "0",
                    "areatype": "0",
                    "width": "200",
                    "height": "200",
                    "viewtype": "0",
                    "use_iconmap": "1",
                    "application": "",
                    "urls": [],
                    "elements": []
                },
                {
                    "selementid": "12",
                    "sysmapid": "3",
                    "elementtype": "0",
                    "iconid_off": "185",
                    "iconid_on": "0",
                    "label": "{HOST.NAME}\r\n{HOST.CONN}",
                    "label_location": "0",
                    "x": "111",
                    "y": "61",
                    "iconid_disabled": "0",
                    "iconid_maintenance": "0",
                    "elementsubtype": "0",
                    "areatype": "0",
                    "width": "200",
                    "height": "200",
                    "viewtype": "0",
                    "use_iconmap": "0",
                    "application": "",
                    "urls": [],
                    "elements": [
                        {
                            "hostid": "10084"
                        }
                    ]
                }
            ],
            "links": [
                {
                    "linkid": "23",
                    "sysmapid": "3",
                    "selementid1": "10",
                    "selementid2": "11",
                    "drawtype": "0",
                    "color": "00CC00",
                    "label": "",
                    "linktriggers": []
                }
            ],
            "users": [
                {
                    "sysmapuserid": "1",
                    "userid": "2",
                    "permission": "2"
                }
            ],
            "userGroups": [
                {
                    "sysmapusrgrpid": "1",
                    "usrgrpid": "7",
                    "permission": "2"
                }
            ],
            "shapes":[  
                {  
                    "sysmap_shapeid":"1",
                    "type":"0",
                    "x":"0",
                    "y":"0",
                    "width":"680",
                    "height":"15",
                    "text":"{MAP.NAME}",
                    "font":"9",
                    "font_size":"11",
                    "font_color":"000000",
                    "text_halign":"0",
                    "text_valign":"0",
                    "border_type":"0",
                    "border_width":"0",
                    "border_color":"000000",
                    "background_color":"",
                    "zindex":"0"
                }
            ],
            "lines":[
                {
                    "sysmap_shapeid":"2",
                    "x1": 30,
                    "y1": 10,
                    "x2": 100,
                    "y2": 50,
                    "line_type": 1,
                    "line_width": 10,
                    "line_color": "009900",
                    "zindex":"1"
                }
            ],
            "sysmapid": "3",
            "name": "Local nerwork",
            "width": "400",
            "height": "400",
            "backgroundid": "0",
            "label_type": "2",
            "label_location": "3",
            "highlight": "1",
            "expandproblem": "1",
            "markelements": "0",
            "show_unack": "0",
            "grid_size": "50",
            "grid_show": "1",
            "grid_align": "1",
            "label_format": "0",
            "label_type_host": "2",
            "label_type_hostgroup": "2",
            "label_type_trigger": "2",
            "label_type_map": "2",
            "label_type_image": "2",
            "label_string_host": "",
            "label_string_hostgroup": "",
            "label_string_trigger": "",
            "label_string_map": "",
            "label_string_image": "",
            "iconmapid": "0",
            "expand_macros": "0",
            "severity_min": "0",
            "userid": "1",
            "private": "1",
            "show_suppressed": "1"
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-cb769d11})

[comment]: # ({new-4be4cd90})
### Смотрите также

-   [Соответствие
    иконок](/ru/manual/api/reference/iconmap/object#соответствие_иконок)
-   [Элемент карты](object#элемент_карты)
-   [Связь карты](object#связь_карты)
-   [URL карты](object#url_карты)
-   [Пользователь карты](object#пользователь_карты)
-   [Группа пользователей карты](object#группа_пользователей_карты)
-   [Фигура карты](object#фигуры_карты)
-   [Линия карты](object#линии_карты)

[comment]: # ({/new-4be4cd90})

[comment]: # ({new-d61644e7})
### Исходный код

CMap::get() в *frontends/php/include/classes/api/services/CMap.php*.

[comment]: # ({/new-d61644e7})
