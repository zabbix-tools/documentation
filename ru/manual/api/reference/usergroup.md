[comment]: # translation:outdated

[comment]: # ({new-f92dbd28})
# Группа пользователей

Этот класс предназначен для работы с группами пользователей.

Справка по объектам:\

-   [Группа
    пользователей](/ru/manual/api/reference/usergroup/object#группа_пользователей)

Доступные методы:\

-   [usergroup.create](/ru/manual/api/reference/usergroup/create) -
    создание новых групп пользователей
-   [usergroup.delete](/ru/manual/api/reference/usergroup/delete) -
    удаление групп пользователей
-   [usergroup.get](/ru/manual/api/reference/usergroup/get) - получение
    групп пользователей
-   [usergroup.update](/ru/manual/api/reference/usergroup/update) -
    обновление групп пользователей

[comment]: # ({/new-f92dbd28})
