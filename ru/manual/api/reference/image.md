[comment]: # translation:outdated

[comment]: # ({new-f759709d})
# Изображение

Этот класс предназначен для работы с изображениями.

Справка по объектам:\

-   [Изображение](/ru/manual/api/reference/image/object#изображение)

Доступные методы:\

-   [image.create](/ru/manual/api/reference/image/create) - создание
    новых изображений
-   [image.delete](/ru/manual/api/reference/image/delete) - удаление
    изображений
-   [image.get](/ru/manual/api/reference/image/get) - получение
    изображений
-   [image.update](/ru/manual/api/reference/image/update) - обновление
    изображений

[comment]: # ({/new-f759709d})
