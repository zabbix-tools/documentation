[comment]: # translation:outdated

[comment]: # ({new-3816cd64})
# Элемент данных

Этот класс предназначен для работы с элементами данных.

Справка по объектам:\

-   [Элемент
    данных](/ru/manual/api/reference/item/object#элемент_данных)

Доступные методы:\

-   [item.create](/ru/manual/api/reference/item/create) - создание новых
    элементов данных
-   [item.delete](/ru/manual/api/reference/item/delete) - удаление
    элементов данных
-   [item.get](/ru/manual/api/reference/item/get) - получение элементов
    данных
-   [item.update](/ru/manual/api/reference/item/update) - обновление
    элементов данных

[comment]: # ({/new-3816cd64})
