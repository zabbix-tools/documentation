[comment]: # translation:outdated

[comment]: # ({new-48cd3524})
# problem.get

[comment]: # ({/new-48cd3524})

[comment]: # ({new-a18b2713})
### Описание

`целое число/массив problem.get(объект параметры)`

Этот метод позволяет получать проблемы в соответствии с заданными
параметрами.

[comment]: # ({/new-a18b2713})

[comment]: # ({new-98f39c6a})
### Параметры

`(объект)` Параметры задают желаемый вывод.

Этот метод поддерживает следующие параметры.

|Параметр|Тип|Описание|
|----------------|------|----------------|
|eventids|строка/массив|Возврат проблем только с заданными ID.|
|groupids|строка/массив|Возврат только тех проблем, которые созданы объектами принадлежащими заданным группам узлов сети.|
|hostids|строка/массив|Возврат только тех проблем, которые созданы объектами принадлежащими заданным узлам сети.|
|objectids|строка/массив|Возврат только тех проблем, которые созданы заданными объектами.|
|applicationids|строка/массив|Возврат только тех проблем, которые созданы объектами принадлежащими заданным группам элементов данных. Применяется только, если объектом являются триггер или элемент данных.|
|source|целое число|Возврат проблем только с заданным типом.<br><br>Обратитесь к [странице объекта события о проблеме](object#проблема) для получения списка поддерживаемых типов событий.<br><br>По умолчанию: 0 - проблема с триггеров.|
|object|целое число|Возврат только тех проблем, которые созданы объектами заданного типа.<br><br>Обратитесь к [странице объекта события о проблеме](object#проблема) для получения списка поддерживаемых типов объекта.<br><br>По умолчанию: 0 - триггер.|
|acknowledged|логический|`true` - возврат только подтвержденных проблем;<br>`false` - только неподтвержденные.|
|severities|целое число/массив|Возврат проблем только с заданными важностями событий. Применяется только, если объектом является триггер.|
|evaltype|целое число|Правила поиска тегов.<br><br>Возможные значения:<br>0 - (по умолчанию) И/Или;<br>2 - Или.|
|tags|массив объектов|Возврат проблем только с заданными тегами. Точное соответствие тегу и не зависимый от регистра поиск по значению и оператору.<br>Формат: `[{"tag": "<тег>", "value": "<значение>", "operator": "<оператор>"}, ...]`.<br>По всем проблемам возвращается пустой массив.<br><br>Возможные типы операторов:<br>0 - (по умолчанию) Содержит;<br>1 - Равен.|
|recent|логический|`true` - возврат ПРОБЛЕМА и недавно РЕШЁННЫХ проблем (зависит от Отображать триггеры в состоянии ОК в течении N секунд)<br>По умолчанию: `false` - только НЕРЕШЁННЫЕ проблемы|
|eventid\_from|строка|Возврат только тех проблем, ID которых больше или равен заданному ID.|
|eventid\_till|строка|Возврат только тех проблем, ID которых меньше или равен заданному ID.|
|time\_from|штамп времени|Возврат только тех проблем, которые были созданы после или в заданное время.|
|time\_till|штамп времени|Возврат только тех проблем, которые были созданы до или в заданное время.|
|selectAcknowledges|запрос|Возврат обновлений проблемы в свойстве `acknowledges`. Обновления проблем отсортированы в обратном хронологическом порядке.<br><br>Объект обновления проблемы имеет следующие свойства:<br>`acknowledgeid` - `(строка)` ID обновления;<br>`userid` - `(строка)` ID пользователя, который обновил событие;<br>`eventid` - `(строка)` ID обновленного события;<br>`clock` - `(штамп времени)` время, когда событие было обновлено;<br>`message` - `(строка)` текст сообщения;<br>`action` - `(целое число)` действие обновления, которое было выполнено, смотрите [event.acknowledge](/ru/manual/api/reference/event/acknowledge);<br>`old_severity` - `(целое число)` важность события до этого действия обновления;<br>`new_severity` - `(целое число)` важность события после этого действия обновления;<br><br>Поддерживается `count`.|
|selectTags|запрос|Возврат тегов проблем. Формат вывода: `[{"tag": "<тег>", "value": "<значение>"}, ...]`.|
|selectSuppressionData|запрос|Возврат списка обслуживаний в свойстве `suppression_data`:<br>`maintenanceid` - *(строка)* ID обслуживания;<br>`suppress_until` - *(целое число)* время, до которого проблема подавлена.|
|sortfield|строка/массив|Сортировка результата в соответствии с заданными свойствами.<br><br>Возможные значения: `eventid`.|
|countOutput|логический|Эти параметры являются общими для всех методов `get` и они описаны в [справочных комментариях](/ru/manual/api/reference_commentary#общие_параметры_get_метода).|
|editable|логический|^|
|excludeSearch|логический|^|
|filter|объект|^|
|limit|целое число|^|
|output|запрос|^|
|preservekeys|логический|^|
|search|объект|^|
|searchByAny|логический|^|
|searchWildcardsEnabled|логический|^|
|sortorder|строка/массив|^|
|startSearch|логический|^|

[comment]: # ({/new-98f39c6a})

[comment]: # ({new-7223bab1})
### Возвращаемые значения

`(целое число/массив)` Возвращает либо:

-   массив объектов;
-   количество найденных объектов, если используется параметр
    `countOutput`.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-4d83529d})
#### Получение событий о проблемах на триггеры

Получение недавних событий с триггера "15112."

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "problem.get",
    "params": {
        "output": "extend",
        "selectAcknowledges": "extend",
        "selectTags": "extend",
        "selectSuppressionData": "extend",
        "objectids": "15112",
        "recent": "true",
        "sortfield": ["eventid"],
        "sortorder": "DESC"
    },
    "auth": "67f45d3eb1173338e1b1647c4bdc1916",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "eventid": "1245463",
            "source": "0",
            "object": "0",
            "objectid": "15112",
            "clock": "1472457242",
            "ns": "209442442",
            "r_eventid": "1245468",
            "r_clock": "1472457285",
            "r_ns": "125644870",
            "correlationid": "0",
            "userid": "1",
            "name": "Zabbix agent on localhost is unreachable for 5 minutes",
            "acknowledged": "1",
            "suppressed": "1",
            "severity": "3",
            "acknowledges": [
                {
                    "acknowledgeid": "14443",
                    "userid": "1",
                    "eventid": "1245463",
                    "clock": "1472457281",
                    "message": "problem solved",
                    "action": "6",
                    "old_severity": "0",
                    "new_severity": "0"
                }
            ],
            "tags": [
                {
                    "tag": "test tag",
                    "value": "test value"
                }
            ],
            "suppression_data": [
                {
                    "maintenanceid": "15",
                    "suppress_until": "1472511600"
                }
            ]
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-4d83529d})

[comment]: # ({new-by})
#### Retrieving problems acknowledged by specified user

Retrieving problems acknowledged by user with ID=10

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "problem.get",
    "params": {
        "output": "extend",
        "selectAcknowledges": ["userid", "action"],
        "filter": {
            "action": 2,
            "action_userid": 10
        },
        "sortfield": ["eventid"],
        "sortorder": "DESC"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": [
        {
            "eventid": "1248566",
            "source": "0",
            "object": "0",
            "objectid": "15142",
            "clock": "1472457242",
            "ns": "209442442",
            "r_eventid": "1245468",
            "r_clock": "1472457285",
            "r_ns": "125644870",
            "correlationid": "0",
            "userid": "10",
            "name": "Zabbix agent on localhost is unreachable for 5 minutes",
            "acknowledged": "1",
            "severity": "3",
            "cause_eventid": "0",
            "opdata": "",
            "acknowledges": [
                {
                    "userid": "10",
                    "action": "2"
                }
            ],
            "suppressed": "0"
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-by})

[comment]: # ({new-c6eb3ba0})
### Смотрите также

-   [Оповещение](/ru/manual/api/reference/alert/object)
-   [Элемент данных](/ru/manual/api/reference/item/object)
-   [Узел сети](/ru/manual/api/reference/host/object)
-   [Правило
    LLD](/ru/manual/api/reference/discoveryrule/object#правило_lld)
-   [Триггер](/ru/manual/api/reference/trigger/object)

[comment]: # ({/new-c6eb3ba0})

[comment]: # ({new-15f9267d})
### Исходный код

CEvent::get() в
*frontends/php/include/classes/api/services/CProblem.php*.

[comment]: # ({/new-15f9267d})
