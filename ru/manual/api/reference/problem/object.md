[comment]: # translation:outdated

[comment]: # ({new-001bbea1})
# > Объект проблемы

::: noteclassic
Проблемы создаются Zabbix сервером и их нельзя менять через
API.
:::

Объект проблемы содержит следующие свойства.

|Свойство|Тип|Описание|
|----------------|------|----------------|
|eventid|строка|ID события о проблеме.|
|source|целое число|Тип события о проблеме.<br><br>Возможные значения:<br>0 - событие создано на триггер;<br>3 - внутреннее событие.|
|object|целое число|Тип объекта, к которому относится событие о проблеме.<br><br>Возможные значения для событий на триггера:<br>0 - триггер.<br><br>Возможные значения для внутренних событий:<br>0 - триггер;<br>4 - элемент данных;<br>5 - правило LLD.|
|objectid|строка|ID связанного объекта.|
|clock|штамп времени|Время, когда событие о проблеме было создано.|
|ns|целое число|Наносекунды, когда событие о проблеме было создано.|
|r\_eventid|строка|ID события восстановления.|
|r\_clock|штамп времени|Время, когда событие восстановления было создано.|
|r\_ns|целое число|Наносекунды, когда событие восстановления было создано.|
|correlationid|строка|ID правила корреляции, если событие было решено при помощи глобального правила корреляции.|
|userid|строка|ID пользователя, если проблема была закрыта вручную.|
|name|строка|Имя решённой проблемы.|
|acknowledged|целое число|Состояние подтверждения проблемы.<br><br>Возможные значения:<br>0 - не подтверждена;<br>1 - подтверждена.|
|severity|целое число|Текущая важность проблемы.<br><br>Возможные значения:<br>0 - не классифицировано;<br>1 - информационная;<br>2 - предупреждение;<br>3 - средняя;<br>4 - высокая;<br>5 - чрезвычайная.|
|suppressed|целое число|*(только чтение)* Подавлена ли проблема или нет.<br><br>Возможные значения:<br>0 - проблема в нормальном режиме;<br>1 - проблема подавлена.|

[comment]: # ({/new-001bbea1})

[comment]: # ({new-1ce0b7ed})
### Problem

::: noteclassic
Problems are created by the Zabbix server and cannot be
modified via the API.
:::

The problem object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|eventid|string|ID of the problem event.|
|source|integer|Type of the problem event.<br><br>Possible values:<br>0 - event created by a trigger;<br>3 - internal event;<br>4 - event created on service status update.|
|object|integer|Type of object that is related to the problem event.<br><br>Possible values if `source` is set to "event created by a trigger":<br>0 - trigger.<br><br>Possible values if `source` is set to "internal event":<br>0 - trigger;<br>4 - item;<br>5 - LLD rule.<br><br>Possible values if `source` is set to "event created on service status update":<br>6 - service.|
|objectid|string|ID of the related object.|
|clock|timestamp|Time when the problem event was created.|
|ns|integer|Nanoseconds when the problem event was created.|
|r\_eventid|string|Recovery event ID.|
|r\_clock|timestamp|Time when the recovery event was created.|
|r\_ns|integer|Nanoseconds when the recovery event was created.|
|cause_eventid|string|Cause event ID.|
|correlationid|string|Correlation rule ID if this event was recovered by global correlation rule.|
|userid|string|User ID if the problem was manually closed.|
|name|string|Resolved problem name.|
|acknowledged|integer|Acknowledge state for problem.<br><br>Possible values:<br>0 - not acknowledged;<br>1 - acknowledged.|
|severity|integer|Problem current severity.<br><br>Possible values:<br>0 - not classified;<br>1 - information;<br>2 - warning;<br>3 - average;<br>4 - high;<br>5 - disaster.|
|suppressed|integer|Whether the problem is suppressed.<br><br>Possible values:<br>0 - problem is in normal state;<br>1 - problem is suppressed.|
|opdata|string|Operational data with expanded macros.|
|urls|array|Active [media type URLs](/manual/api/reference/problem/object#media-type-urls).|

[comment]: # ({/new-1ce0b7ed})

[comment]: # ({new-acd9b507})
### Problem tag

The problem tag object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|tag|string|Problem tag name.|
|value|string|Problem tag value.|

[comment]: # ({/new-acd9b507})


[comment]: # ({new-52ff7df7})
### Media type URLs

Object with media type url have the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|name|string|Media type defined URL name.|
|url|string|Media type defined URL value.|

Results will contain entries only for active media types with enabled
event menu entry. Macro used in properties will be expanded, but if one
of properties contain non expanded macro both properties will be
excluded from results. Supported macros described on
[page](/manual/appendix/macros/supported_by_location).

[comment]: # ({/new-52ff7df7})
