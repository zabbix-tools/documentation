[comment]: # translation:outdated

[comment]: # ({new-4f0fdd87})
# Шаблон

Этот класс предназначен для работы с шаблонами.

Справка по объектам:\

-   [Шаблон](/ru/manual/api/reference/template/object#шаблон)

Доступные методы:\

-   [template.create](/ru/manual/api/reference/template/create) -
    создание новых шаблонов
-   [template.delete](/ru/manual/api/reference/template/delete) -
    удаление шаблонов
-   [template.get](/ru/manual/api/reference/template/get) - получение
    шаблонов
-   [template.massadd](/ru/manual/api/reference/template/massadd) -
    добавление связанных объектов к шаблонам
-   [template.massremove](/ru/manual/api/reference/template/massremove) -
    удаление связанных объектов с шаблонов
-   [template.massupdate](/ru/manual/api/reference/template/massupdate) -
    замена или удаление связанных объектов с шаблонов
-   [template.update](/ru/manual/api/reference/template/update) -
    обновление шаблонов

[comment]: # ({/new-4f0fdd87})
