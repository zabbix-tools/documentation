[comment]: # translation:outdated

[comment]: # ({new-cf547db2})
# graph.delete

[comment]: # ({/new-cf547db2})

[comment]: # ({new-c740de99})
### Описание

`объект graph.delete(массив graphIds)`

Этот метод позволяет удалять графики.

[comment]: # ({/new-c740de99})

[comment]: # ({new-54514063})
### Параметры

`(массив)` ID удаляемых графиков.

[comment]: # ({/new-54514063})

[comment]: # ({new-eb5c8bd8})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID удаленных графиков под
свойством `graphids`.

[comment]: # ({/new-eb5c8bd8})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-3a78f2ec})
#### Удаление нескольких графиков

Удаление двух графиков.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "graph.delete",
    "params": [
        "652",
        "653"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "graphids": [
            "652",
            "653"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-3a78f2ec})

[comment]: # ({new-e06af123})
### Исходный код

CGraph::delete() в
*frontends/php/include/classes/api/services/CGraph.php*.

[comment]: # ({/new-e06af123})
