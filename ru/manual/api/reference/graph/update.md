[comment]: # translation:outdated

[comment]: # ({new-d2516189})
# graph.update

[comment]: # ({/new-d2516189})

[comment]: # ({new-992bc0de})
### Описание

`объект graph.update(объект/массив графики)`

Этот метод позволяет обновлять существующие графики.

[comment]: # ({/new-992bc0de})

[comment]: # ({new-72c99624})
### Параметры

`(объект/массив)` Свойства графика, которые будут обновлены.

Свойство `graphid` должно быть указано по каждому графику, все остальные
свойства опциональны. Будут обновлены только переданные свойства, все
остальные останутся неизменными.

В дополнение к [стандартным свойствам графика](object#график), этот
метод принимает следующие параметры.

|Параметр|Тип|Описание|
|----------------|------|----------------|
|gitems|массив|Элементы графика, которые заменят существующие элементы графика. Если элемент графика имеет заданное свойство `gitemid`, этот элемент будет обновлен, в противном случае будет создан новый элемент графика.|

[comment]: # ({/new-72c99624})

[comment]: # ({new-9a0b4500})
### Возвращаемые значения

`(object)` Возвращает объект, который содержит ID обновленных графиков
под свойством `graphids`.

[comment]: # ({/new-9a0b4500})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-7b33f5ce})
#### Установка максимального значения оси Y

Установка максимального значения оси Y на фиксированное значение равное
100.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "graph.update",
    "params": {
        "graphid": "439",
        "ymax_type": 1,
        "yaxismax": 100
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "graphids": [
            "439"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-7b33f5ce})

[comment]: # ({new-84b1446b})
### Исходный код

CGraph::update() в *ui/include/classes/api/services/CGraph.php*.

[comment]: # ({/new-84b1446b})
