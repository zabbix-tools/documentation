[comment]: # translation:outdated

[comment]: # ({new-000fdd04})
# graph.create

[comment]: # ({/new-000fdd04})

[comment]: # ({new-e0ee1e5d})
### Описание

`объект graph.create(объект/массив графики)`

Этот метод позволяет создавать новые графики.

[comment]: # ({/new-e0ee1e5d})

[comment]: # ({new-fc8b02f4})
### Параметры

`(объект/массив)` Создаваемые графики.

В дополнение к [стандартным свойствам графика](object#график), этот
метод принимает следующие параметры.

|Параметр|Тип|Описание|
|----------------|------|----------------|
|**gitems**<br>(требуется)|массив|Создаваемые элементы графика.|

[comment]: # ({/new-fc8b02f4})

[comment]: # ({new-d64b873d})
### Возвращаемые значения

`(объект)` Возвращает объект, который содержит ID созданных графиков под
свойством `graphids`. Порядок возвращаемых ID совпадает с порядком
переданных графиков.

[comment]: # ({/new-d64b873d})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-cd12048c})
#### Создание графика

Создание графика с двумя элементами.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "graph.create",
    "params": {
        "name": "MySQL bandwidth",
        "width": 900,
        "height": 200,
        "gitems": [
            {
                "itemid": "22828",
                "color": "00AA00",
                "sortorder": "0"
            },
            {
                "itemid": "22829",
                "color": "3333FF",
                "sortorder": "1"
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "graphids": [
            "652"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-cd12048c})

[comment]: # ({new-88266563})
### Смотрите также

-   [Элемент данных
    графика](/ru/manual/api/reference/graphitem/object#элемент_данных_графика)

[comment]: # ({/new-88266563})

[comment]: # ({new-4e535c8e})
### Исходный код

CGraph::create() в *ui/include/classes/api/services/CGraph.php*.

[comment]: # ({/new-4e535c8e})
