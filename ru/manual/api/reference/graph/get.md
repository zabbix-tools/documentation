[comment]: # translation:outdated

[comment]: # ({new-cdc53a5f})
# graph.get

[comment]: # ({/new-cdc53a5f})

[comment]: # ({new-d2f8406d})
### Описание

`целое число/массив graph.get(объект параметры)`

Этот метод позволяет получать графики в соответствии с заданными
параметрами.

[comment]: # ({/new-d2f8406d})

[comment]: # ({new-5b1c5b22})
### Параметры

`(объект)` Параметры задают желаемый вывод.

Этот метод поддерживает следующие параметры.

|Параметр|Тип|Описание|
|----------------|------|----------------|
|graphids|строка/массив|Возврат графиков только с заданными ID.|
|groupids|строка/массив|Возврат только тех графиков, которые принадлежат узлам сети из заданных групп узлов сети.|
|templateids|строка/массив|Возврат только тех графиков, которые принадлежат заданным шаблонам.|
|hostids|строка/массив|Возврат только тех графиков, которые принадлежат заданным узлам сети.|
|itemids|строка/массив|Возврат только тех графиков, которые содержат заданные элементы данных.|
|templated|логический|Если задано значение `true`, возврат только тех графиков, которые принадлежат шаблонам.|
|inherited|логический|Если задано значение `true`, возврат только тех графиков, которые унаследованы от шаблонов.|
|expandName|флаг|Раскрытие макросов в имени графика.|
|selectGroups|запрос|Возврат групп узлов сети, которым принадлежит график, в свойстве `groups`.|
|selectTemplates|запрос|Возврат шаблонов, которым принадлежит график, в свойстве `templates`.|
|selectHosts|запрос|Возврат узлов сети, которым принадлежит график, в свойстве `hosts`.|
|selectItems|запрос|Возврат элементов данных, которые используются в графике, в свойстве `items`.|
|selectGraphDiscovery|запрос|Возврат объекта обнаружения графика в свойстве `graphDiscovery`. Объект обнаружения графика связывает график с прототипом графиков.<br><br>Этот объект имеет следующие свойства:<br>`graphid` - `(строка)` ID графика;<br>`parent_graphid` - `(строка)` ID прототипа графиков, с которого был создан график.|
|selectGraphItems|запрос|Возврат элементов графика, которые используются в графике, в свойстве `gitems`.|
|selectDiscoveryRule|запрос|Возврат правила низкоуровневого обнаружения, которое создало график, в свойстве `discoveryRule`.|
|filter|объект|Возврат только тех результатов, которые в точности соответствуют заданному фильтру.<br><br>Принимает массив, где ключи являются именами свойств и значения, которые являются либо одним значением, либо массивом сопоставляемых значений.<br><br>Поддерживает дополнительные фильтры:<br>`host` - техническое имя узла сети, которому принадлежит график;<br>`hostid` - ID узла сети, которому принадлежит график.|
|sortfield|строка/массив|Сортировка результата в соответствии с заданными свойствами.<br><br>Возможные значения: `graphid`, `name` и `graphtype`.|
|countOutput|логический|Эти параметры являются общими для всех методов `get` и они описаны в [справочных комментариях](/ru/manual/api/reference_commentary#общие_параметры_get_метода).|
|editable|логический|^|
|excludeSearch|логический|^|
|limit|целое число|^|
|output|запрос|^|
|preservekeys|логический|^|
|search|объект|^|
|searchByAny|логический|^|
|searchWildcardsEnabled|логический|^|
|sortorder|строка/массив|^|
|startSearch|логический|^|

[comment]: # ({/new-5b1c5b22})

[comment]: # ({new-7223bab1})
### Возвращаемые значения

`(целое число/массив)` Возвращает либо:

-   массив объектов;
-   количество найденных объектов, если используется параметр
    `countOutput`.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Примеры

[comment]: # ({/new-b41637d2})

[comment]: # ({new-e7fae381})
#### Получение графиков с узлов сети

Получение всех графиков с узла сети "10107" и сортировка этих графиков
по имени.

Запрос:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "graph.get",
    "params": {
        "output": "extend",
        "hostids": 10107,
        "sortfield": "name"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Ответ:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "graphid": "612",
            "name": "CPU jumps",
            "width": "900",
            "height": "200",
            "yaxismin": "0.0000",
            "yaxismax": "100.0000",
            "templateid": "439",
            "show_work_period": "1",
            "show_triggers": "1",
            "graphtype": "0",
            "show_legend": "1",
            "show_3d": "0",
            "percent_left": "0.0000",
            "percent_right": "0.0000",
            "ymin_type": "0",
            "ymax_type": "0",
            "ymin_itemid": "0",
            "ymax_itemid": "0",
            "flags": "0"
        },
        {
            "graphid": "613",
            "name": "CPU load",
            "width": "900",
            "height": "200",
            "yaxismin": "0.0000",
            "yaxismax": "100.0000",
            "templateid": "433",
            "show_work_period": "1",
            "show_triggers": "1",
            "graphtype": "0",
            "show_legend": "1",
            "show_3d": "0",
            "percent_left": "0.0000",
            "percent_right": "0.0000",
            "ymin_type": "1",
            "ymax_type": "0",
            "ymin_itemid": "0",
            "ymax_itemid": "0",
            "flags": "0"
        },
        {
            "graphid": "614",
            "name": "CPU utilization",
            "width": "900",
            "height": "200",
            "yaxismin": "0.0000",
            "yaxismax": "100.0000",
            "templateid": "387",
            "show_work_period": "1",
            "show_triggers": "0",
            "graphtype": "1",
            "show_legend": "1",
            "show_3d": "0",
            "percent_left": "0.0000",
            "percent_right": "0.0000",
            "ymin_type": "1",
            "ymax_type": "1",
            "ymin_itemid": "0",
            "ymax_itemid": "0",
            "flags": "0"
        },
        {
            "graphid": "645",
            "name": "Disk space usage /",
            "width": "600",
            "height": "340",
            "yaxismin": "0.0000",
            "yaxismax": "0.0000",
            "templateid": "0",
            "show_work_period": "0",
            "show_triggers": "0",
            "graphtype": "2",
            "show_legend": "1",
            "show_3d": "1",
            "percent_left": "0.0000",
            "percent_right": "0.0000",
            "ymin_type": "0",
            "ymax_type": "0",
            "ymin_itemid": "0",
            "ymax_itemid": "0",
            "flags": "4"
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-e7fae381})

[comment]: # ({new-e4f96e80})
### Смотрите также

-   [Правило
    обнаружения](/ru/manual/api/reference/discoveryrule/object#правило_обнаружения)
-   [Элемент
    графика](/ru/manual/api/reference/graphitem/object#элемент_графика)
-   [Элемент
    данных](/ru/manual/api/reference/item/object#элемент_данных)
-   [Узел сети](/ru/manual/api/reference/host/object#узел_сети)
-   [Группа узлов
    сети](/ru/manual/api/reference/hostgroup/object#группа_узлов_сети)
-   [Шаблон](/ru/manual/api/reference/template/object#шаблон)

[comment]: # ({/new-e4f96e80})

[comment]: # ({new-6f7b2d0a})
### Исходный код

CGraph::get() в *frontends/php/include/classes/api/services/CGraph.php*.

[comment]: # ({/new-6f7b2d0a})
