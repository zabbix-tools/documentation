[comment]: # translation:outdated

[comment]: # ({new-62384434})
# Прототип графиков

Этот класс предназначен для работы с прототипами графиков.

Справка по объектам:\

-   [Прототип
    графиков](/ru/manual/api/reference/graphprototype/object#прототип_графиков)

Доступные методы:\

-   [graphprototype.create](/ru/manual/api/reference/graphprototype/create) -
    создание новых прототипов графиков
-   [graphprototype.delete](/ru/manual/api/reference/graphprototype/delete) -
    удаление прототипов графиков
-   [graphprototype.get](/ru/manual/api/reference/graphprototype/get) -
    получение прототипов графиков
-   [graphprototype.update](/ru/manual/api/reference/graphprototype/update) -
    обновление прототипов графиков

[comment]: # ({/new-62384434})
