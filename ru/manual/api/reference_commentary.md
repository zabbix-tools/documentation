[comment]: # translation:outdated

[comment]: # ({new-6d056871})
# Приложение 1. Справочные комментарии

[comment]: # ({/new-6d056871})

[comment]: # ({new-6594bdc4})
### Обозначение

[comment]: # ({/new-6594bdc4})

[comment]: # ({new-2df08058})
#### Типы данных

Zabbix API поддерживает следующие типы данных:

|Тип|Описание|
|------|----------------|
|логический|Логическое значение, принимает либо `true`, либо `false`.|
|флаг|Значение считается `true`, если оно указано и не равно `null` и `false`, в противном случае.|
|целое число|Целое число.|
|дробное число|Число с плавающей точкой.|
|строка|Текстовая строка.|
|текст|Более длинная строка текста.|
|штамп времени|Штамп времени в формате Unix.|
|массив|Упорядоченная последовательность значения, то есть, простой массив.|
|объект|Ассоциативный массив.|
|запрос|Значение, которое определяет какие данные необходимо вернуть.<br><br>Может быть задано массивом имен свойств, чтобы возвращались только указанные свойства или, в том числе, одно из предопределенных значений:<br>`extend` - возвращает все свойства объекта;<br>`count` - возвращает количество полученных записей, поддерживается только некоторыми подзапросами.|

[comment]: # ({/new-2df08058})

[comment]: # ({new-a2c16a18})
#### Подписи к свойствам

Некоторые из свойств объектов маркируются короткими подписями, чтобы
описать их поведение. Используются следующие подписи:

-   *только чтение* - значение этого свойства устанавливается
    автоматически не может быть определено или изменено клиентом;
-   *константа* - значение этого свойства можно устанавливать при
    создании объекта, но нельзя менять после.

[comment]: # ({/new-a2c16a18})

[comment]: # ({new-f255da9d})
#### Parameter behavior

Some of the operation parameters are marked with short labels to describe their behavior for the operation.
The following labels are used:

-   *read-only* - the value of the parameter is set automatically and cannot be defined or changed by the user, even in some specific conditions
    (e.g., *read-only* for inherited objects or discovered objects);
-   *write-only* - the value of the parameter can be set, but cannot be accessed after;
-   *supported* - the value of the parameter is not required to be set, but is allowed to be set in some specific conditions
    (e.g., *supported* if `status` of Proxy object is set to "passive proxy");
-   *required* - the value of the parameter is required to be set.

Parameters that are not marked with labels are optional.

[comment]: # ({/new-f255da9d})

[comment]: # ({new-5e99748d})
### Зарезервированное значение ID равное "0"

Зарезервированное значение ID "0" можно использовать для фильтрации
элементов и удаления связанных объектов. Например, для удаления ссылки
на прокси с узла сети, proxy\_hostid необходимо задать значением 0
("proxy\_hostid": "0") или для фильтрации узлов сети наблюдаемых
сервером, опция proxyids должна быть задана значением 0 ("proxyids":
"0").

[comment]: # ({/new-5e99748d})

[comment]: # ({new-b4e0f5ab})
### Общие параметры "get" метода

Следующие параметры поддерживаются всеми `get` методами:

|Параметр|Тип|Описание|
|----------------|------|----------------|
|countOutput|логический|Возвращает в результате количество записей вместо актуальных данных.|
|editable|логический|Если задано равным `true`, возвращает только те объекты на которые у пользователя есть права на запись.<br><br>По умолчанию: `false`.|
|excludeSearch|логический|Возвращает результаты, которые не совпадают с заданным критерием `search` параметре.|
|filter|объект|Возвращать только те результаты, которые в точности совпадают с заданным фильтром.<br><br>Принимает массив, где ключи являются именами свойств и значения, либо одно значение, либо массив значений соответствий.<br><br>Не работает с `text` полями.|
|limit|целое число|Ограничение количества возвращаемых записей.|
|output|запрос|Свойства возвращаемых объектов.<br><br>По умолчанию: `extend`.|
|preservekeys|логический|Использование ID как ключей в результирующем массиве.|
|search|объект|Возвращаемые результаты, которые соответствуют заданному шаблону поиска (регистрозависимый).<br><br>Принимает массив, где ключами являются имена свойств и строковые значения для поиска. Если не заданы дополнительные опции, эта опция выполнит `LIKE "%…%"` поиск.<br><br>Работает только с `string` и `text` полями.|
|searchByAny|логический|Если задано равным `true`, возвращает результаты которые совпадают с любым из заданных критериев в `filter` или `search` параметрах вместо соответствия по всем.<br><br>По умолчанию: `false`.|
|searchWildcardsEnabled|логический|Если задано равным `true`, включает использование "\*" символом шаблона в `search` параметре.<br><br>По умолчанию: `false`.|
|sortfield|строка/массив|Сортировка результата по заданным свойствам. Обратитесь к отдельным описаниям get методов API для получения списка свойств, которые можно использовать при сортировке. Макросы не раскрываются до сортировки.|
|sortorder|строка/массив|Порядок сортировки. Если передан массив, каждое значение будет сопоставляться соответствующему свойству из `sortfield` параметра.<br><br>Возможные значения:<br>`ASC` - по возрастанию;<br>`DESC` - по убыванию.|
|startSearch|логический|`search` параметр будет сравнивать начало полей, то есть, выполнять `LIKE "…%"` поиск.<br><br>Игнорируется, если `searchWildcardsEnabled` задан равным `true`.|

[comment]: # ({/new-b4e0f5ab})



[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})



[comment]: # ({new-7a121fac})
#### User permission check

Does the user have permission to write to hosts whose names begin with
"MySQL" or "Linux" ?

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.get",
    "params": {
        "countOutput": true,
        "search": {
            "host": ["MySQL", "Linux"]
        },
        "editable": true,
        "startSearch": true,
        "searchByAny": true
    },
    "auth": "766b71ee543230a1182ca5c44d353e36",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": "0",
    "id": 1
}
```

::: noteclassic
Zero result means no hosts with read/write
permissions.
:::

[comment]: # ({/new-7a121fac})

[comment]: # ({new-ea3cccd8})
#### Mismatch counting

Count the number of hosts whose names do not contain the substring
"ubuntu"

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.get",
    "params": {
        "countOutput": true,
        "search": {
            "host": "ubuntu"
        },
        "excludeSearch": true
    },
    "auth": "766b71ee543230a1182ca5c44d353e36",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": "44",
    "id": 1
}
```

[comment]: # ({/new-ea3cccd8})

[comment]: # ({new-e911c08b})
#### Searching for hosts using wildcards

Find hosts whose name contains word "server" and have interface ports
"10050" or "10071". Sort the result by host name in descending order and
limit it to 5 hosts.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.get",
    "params": {
        "output": ["hostid", "host"],
        "selectInterfaces": ["port"],
        "filter": {
            "port": ["10050", "10071"]
        },
        "search": {
            "host": "*server*"
        },
        "searchWildcardsEnabled": true,
        "searchByAny": true,
        "sortfield": "host",
        "sortorder": "DESC",
        "limit": 5
    },
    "auth": "766b71ee543230a1182ca5c44d353e36",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "hostid": "50003",
            "host": "WebServer-Tomcat02",
            "interfaces": [
                {
                    "port": "10071"
                }
            ]
        },
        {
            "hostid": "50005",
            "host": "WebServer-Tomcat01",
            "interfaces": [
                {
                    "port": "10071"
                }
            ]
        },
        {
            "hostid": "50004",
            "host": "WebServer-Nginx",
            "interfaces": [
                {
                    "port": "10071"
                }
            ]
        },
        {
            "hostid": "99032",
            "host": "MySQL server 01",
            "interfaces": [
                {
                    "port": "10050"
                }
            ]
        },
        {
            "hostid": "99061",
            "host": "Linux server 01",
            "interfaces": [
                {
                    "port": "10050"
                }
            ]
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-e911c08b})

[comment]: # ({new-8ce6f554})
#### Searching for hosts using wildcards with "preservekeys"

If you add the parameter "preservekeys" to the previous request, the
result is returned as an associative array, where the keys are the id of
the objects.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.get",
    "params": {
        "output": ["hostid", "host"],
        "selectInterfaces": ["port"],
        "filter": {
            "port": ["10050", "10071"]
        },
        "search": {
            "host": "*server*"
        },
        "searchWildcardsEnabled": true,
        "searchByAny": true,
        "sortfield": "host",
        "sortorder": "DESC",
        "limit": 5,
        "preservekeys": true
    },
    "auth": "766b71ee543230a1182ca5c44d353e36",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "50003": {
            "hostid": "50003",
            "host": "WebServer-Tomcat02",
            "interfaces": [
                {
                    "port": "10071"
                }
            ]
        },
        "50005": {
            "hostid": "50005",
            "host": "WebServer-Tomcat01",
            "interfaces": [
                {
                    "port": "10071"
                }
            ]
        },        
        "50004": {
            "hostid": "50004",
            "host": "WebServer-Nginx",
            "interfaces": [
                {
                    "port": "10071"
                }
            ]
        },
        "99032": {
            "hostid": "99032",
            "host": "MySQL server 01",
            "interfaces": [
                {
                    "port": "10050"
                }
            ]
        },
        "99061": {
            "hostid": "99061",
            "host": "Linux server 01",
            "interfaces": [
                {
                    "port": "10050"
                }
            ]
        }
    },
    "id": 1
}
```

[comment]: # ({/new-8ce6f554})
