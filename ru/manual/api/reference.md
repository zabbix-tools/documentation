[comment]: # translation:outdated

[comment]: # ({new-b0d207c5})
# Справочник методов

В этом разделе содержится обзор функций, которые имеются в Zabbix API и
которые помогут вам найти свой путь относительно доступных классов и
методов.

[comment]: # ({/new-b0d207c5})

[comment]: # ({new-53edf72e})
### Мониторинг

Zabbix API позволяет вам получить доступ к истории и другим данным,
собранным в процессе мониторинга.

[comment]: # ({/new-53edf72e})

[comment]: # ({new-ead86ed5})
#### История

Получение исторических данных, которые собраны процессами мониторинга
Zabbix, для отображения или дальнейшей обработки.

[API истории](/ru/manual/api/reference/history)

[comment]: # ({/new-ead86ed5})

[comment]: # ({new-46aecf9d})
#### Динамика изменений

Получение значений динамики изменений, которые вычислены Zabbix
сервером, для отображения или дальнейшей обработки.

[API динамики изменений](/ru/manual/api/reference/trend)

[comment]: # ({/new-46aecf9d})

[comment]: # ({new-ce08707b})
#### События

Получение событий, которые сгенерированы триггерами, сетевым
обнаружением и другими системами Zabbix, для более гибкого управления
ситуациями или интеграции со сторонними инструментами.

[API событий](/ru/manual/api/reference/event)

[comment]: # ({/new-ce08707b})

[comment]: # ({new-4e280d13})
#### Проблемы

Получение проблем в соответствии с заданными параметрами.

[API проблем](/ru/manual/api/reference/problem)

[comment]: # ({/new-4e280d13})

[comment]: # ({new-087f1047})
#### Мониторинг услуг

Получение подробной информации доступности слоев услуг касательно любой
услуги.

[Вычисление SLA услуг](/ru/manual/api/reference/service/getsla)

[comment]: # ({/new-087f1047})


[comment]: # ({new-2b35beaa})
#### Service Level Agreement

Define Service Level Objectives (SLO), retrieve detailed Service Level Indicators (SLI)
information about service performance.

[SLA API](/manual/api/reference/sla)

[comment]: # ({/new-2b35beaa})

[comment]: # ({new-ea863fe1})
### Настройка

Zabbix API позволяет вам управлять настройкой вашей системы мониторинга.

[comment]: # ({/new-ea863fe1})

[comment]: # ({new-services})
## Services

The Zabbix API allows you to access data gathered
during service monitoring.

[comment]: # ({/new-services})

[comment]: # ({new-467838af})
#### Узлы сети и группы узлов сети

Управление группами узлов сети, узлами сети и всем, что связано с ними,
включая интерфейсы узлов сети, макросы узлов сети и периоды
обслуживания.

[API узлов сети](/ru/manual/api/reference/host) | [API групп узлов
сети](/ru/manual/api/reference/hostgroup) | [API интерфейсов узлов
сети](/ru/manual/api/reference/hostinterface) | [API пользовательских
макросов](/ru/manual/api/reference/usermacro) | [API
обслуживания](/ru/manual/api/reference/maintenance)

[comment]: # ({/new-467838af})

[comment]: # ({new-4fe0de77})
#### Элементы данных и группы элементов данных

Добавление элементов данных для мониторинга. Создавайте и удаляйте
группы элементов данных и назначайте элементы данных в эти группы.

[API элементов данных](/ru/manual/api/reference/item) | [API групп
элементов данных](/ru/manual/api/reference/application)

[comment]: # ({/new-4fe0de77})

[comment]: # ({new-29ae0d97})
#### Триггеры

Настройка триггеров для оповещения вас о наличии проблем в вашей
системе. Управляйте зависимостями триггеров.

[API триггеров](/ru/manual/api/reference/trigger)

[comment]: # ({/new-29ae0d97})

[comment]: # ({new-de0198b3})
#### Графики

Изменение графиков или отдельных элементов графиков для лучшей
презентабельности собранных данных.

[API графиков](/ru/manual/api/reference/graph) | [API элементов
графиков](/ru/manual/api/reference/graphitem)

[comment]: # ({/new-de0198b3})

[comment]: # ({new-f61b98cf})
#### Шаблоны

Управление шаблонами и соединение этих шаблонов с узлами сети или
другими шаблонами.

[API шаблонов](/ru/manual/api/reference/template)

[comment]: # ({/new-f61b98cf})

[comment]: # ({new-7a170077})
#### Экспорт и импорт

Экспорт и импорт данных конфигурации Zabbix для создания архивных копий
конфигурации, миграции или крупномасштабных изменений конфигурации.

[API настройки](/ru/manual/api/reference/configuration)

[comment]: # ({/new-7a170077})

[comment]: # ({new-56f63ece})
#### Низкоуровневое обнаружение

Настройка правил низкоуровневого обнаружения, а также прототипов
элементов данных, триггеров и графиков для мониторинга динамических
объектов.

[API правил LLD](/ru/manual/api/reference/discoveryrule) | [API
прототипов элементов данных](/ru/manual/api/reference/itemprototype) |
[API прототипов триггеров](/ru/manual/api/reference/triggerprototype) |
[API прототипов графиков](/ru/manual/api/reference/graphprototype) |
[API прототипов узлов сети](/ru/manual/api/reference/hostprototype)

[comment]: # ({/new-56f63ece})

[comment]: # ({new-61993c62})
#### Корреляция событий

Создание пользовательских правил корреляции событий.

[API корреляций](/ru/manual/api/reference/correlation)

[comment]: # ({/new-61993c62})

[comment]: # ({new-63c25731})
#### Действия и оповещения

Добавление действий и операций для оповещений пользователей о
наступлении определенных событий или автоматическое выполнение удаленных
команд. Получение доступа к информации о вызванных оповещениях и их
получателях.

[API действий](/ru/manual/api/reference/action) | [API
оповещений](/ru/manual/api/reference/alert)

[comment]: # ({/new-63c25731})

[comment]: # ({new-3ea1b33b})
#### Услуги

Управление услугами мониторинга уровня услуг и получение детальной
информации SLA по поводу любой услуги.

[API услуг](/ru/manual/api/reference/service)

[comment]: # ({/new-3ea1b33b})

[comment]: # ({new-media})
#### Media types

Configure media types and multiple ways users will receive alerts.

[Media type API](/manual/api/reference/mediatype)

[comment]: # ({/new-media})

[comment]: # ({new-891cbcad})
#### Панели

Управление панелями.

[API панелей](/ru/manual/api/reference/dashboard)

[comment]: # ({/new-891cbcad})

[comment]: # ({new-1a3dcbcf})
#### Комплексные экраны

Изменение глобальных и комплексных экранов уровня шаблонов или каждого
элемента комплексного экрана отдельно.

[API комплексных экранов](/ru/manual/api/reference/screen) | [API
элементов комплексного экрана](/ru/manual/api/reference/screenitem) |
[API комплексных экранов
шаблона](/ru/manual/api/reference/templatescreen) | [API элементов
комплексного экрана
шаблонов](/ru/manual/api/reference/templatescreenitem)

[comment]: # ({/new-1a3dcbcf})

[comment]: # ({new-29b6c9a4})
#### Карты сети

Настройка карт сетей для создания детальных динамических представлений
вашей IT инфраструктуры.

[API карт сети](/ru/manual/api/reference/map)

[comment]: # ({/new-29b6c9a4})

[comment]: # ({new-e083e0e0})
#### Веб-мониторинг

Настройка веб-сценариев для мониторинга за вашими веб-приложениями и
сервисами.

[API веб-сценариев](/ru/manual/api/reference/httptest)

[comment]: # ({/new-e083e0e0})

[comment]: # ({new-alerts})
## Alerts

The Zabbix API allows you to manage the actions and alerts of your monitoring system.

[comment]: # ({/new-alerts})

[comment]: # ({new-91788676})
#### Сетевое обнаружение

Управление правилами обнаружения сетевого уровня для автоматического
поиска и мониторинга новых узлов сети. Получение полного доступа к
информации об обнаруженных сервисах и узлах сети.

[API правил обнаружения](/ru/manual/api/reference/drule) | [API проверок
обнаружения](/ru/manual/api/reference/dcheck) | [API обнаруженных узлов
сети](/ru/manual/api/reference/dhost) | [API обнаруженных
сервисов](/ru/manual/api/reference/dservice)

[comment]: # ({/new-91788676})

[comment]: # ({new-ad3cb7b9})
### Администрирование

С Zabbix API вы можете менять настройки администрирования вашей системы
мониторинга.

[comment]: # ({/new-ad3cb7b9})

[comment]: # ({new-c2d096e3})
#### Пользователи

Добавление пользователей, которые будут иметь доступ к Zabbix,
назначение их в группы пользователей и предоставление прав доступа.
Настройка типов оповещений и способов, которыми пользователи будут
получать оповещения.

[API пользователей](/ru/manual/api/reference/user) | [API групп
пользователей](/ru/manual/api/reference/usergroup) | [API способов
оповещений](/ru/manual/api/reference/mediatype)

[comment]: # ({/new-c2d096e3})

[comment]: # ({new-925840a1})
#### Общие

Изменение некоторых опций глобальной конфигурации.

[API соответствий иконок](/ru/manual/api/reference/iconmap) | [API
изображений](/ru/manual/api/reference/image) | [API пользовательских
макросов](/ru/manual/api/reference/usermacro)

[comment]: # ({/new-925840a1})

[comment]: # ({new-audit})
#### Audit log

Track configuration changes each user has done.

[Audit log API](/manual/api/reference/auditlog)

[comment]: # ({/new-audit})

[comment]: # ({new-housekeeping})
#### Housekeeping

Configure housekeeping.

[Housekeeping API](/manual/api/reference/housekeeping)

[comment]: # ({/new-housekeeping})


[comment]: # ({new-feb914d9})
#### Скрипты

Настройка и выполнение скриптов, которые помогут вам с вашими
ежедневными задачами.

[API скриптов](/ru/manual/api/reference/script)

[comment]: # ({/new-feb914d9})

[comment]: # ({new-macros})
#### Macros

Manage macros.

[User macro API](/manual/api/reference/usermacro)

[comment]: # ({/new-macros})

[comment]: # ({new-a1c36c8e})
### Информация о API

Получение версии Zabbix API, таким образом ваше приложение сможет
использовать специфичные для версии возможности.

[API информации о API](/ru/manual/api/reference/apiinfo)

[comment]: # ({/new-a1c36c8e})




[comment]: # ({new-4050de56})
#### API Tokens

Manage authorization tokens.

[Token API](/manual/api/reference/token)

[comment]: # ({/new-4050de56})

[comment]: # ({new-4e5421dd})
#### Scripts

Configure and execute scripts to help you with your daily tasks.

[Script API](/manual/api/reference/script)

[comment]: # ({/new-4e5421dd})

[comment]: # ({new-users})
## Users

The Zabbix API allows you to manage users of your monitoring system.

[comment]: # ({/new-users})

[comment]: # ({new-3b3700b0})
### API information

Retrieve the version of the Zabbix API so that your application could
use version-specific features.

[API info API](/manual/api/reference/apiinfo)

[comment]: # ({/new-3b3700b0})
