<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="ru" datatype="plaintext" original="manual/quickstart/login.md">
    <body>
      <trans-unit id="3f54a0e9" xml:space="preserve">
        <source># 1 Login and configuring user</source>
      </trans-unit>
      <trans-unit id="f6cb6160" xml:space="preserve">
        <source>#### Overview

In this section, you will learn how to log in and set up a system user
in Zabbix.</source>
      </trans-unit>
      <trans-unit id="6cb1478f" xml:space="preserve">
        <source>#### Login

![](../../../assets/en/manual/quickstart/login.png){width="350"}

This is the Zabbix welcome screen. Enter the user name **Admin** with
password **zabbix** to log in as a [Zabbix
superuser](/manual/config/users_and_usergroups/permissions). Access to
all menu sections will be granted.</source>
      </trans-unit>
      <trans-unit id="ecf369b8" xml:space="preserve">
        <source>##### Protection against brute force attacks

In case of five consecutive failed login attempts, Zabbix interface will
pause for 30 seconds in order to prevent brute force and dictionary
attacks.

The IP address of a failed login attempt will be displayed after a
successful login.</source>
      </trans-unit>
      <trans-unit id="6be209b3" xml:space="preserve">
        <source>#### Adding user

To view information about users, go to *Users → Users*.

![](../../../assets/en/manual/quickstart/userlist.png){width="600"}

To add a new user, click on *Create user*.

In the new user form, make sure to add your user to one of the existing
[user groups](/manual/config/users_and_usergroups/usergroup), for
example 'Zabbix administrators'.

![](../../../assets/en/manual/quickstart/new_user.png)

All mandatory input fields are marked with a red asterisk.

By default, new users have no media (notification delivery methods)
defined for them. To create one, go to the 'Media' tab and click on
*Add*.

![](../../../assets/en/manual/quickstart/new_media.png)

In this pop-up, enter an email address for the user.

You can specify a time period when the medium will be active (see [Time
period specification](/manual/appendix/time_period) page for a
description of the format), by default a medium is always active. You
can also customize [trigger severity](/manual/config/triggers/severity)
levels for which the medium will be active, but leave all of them
enabled for now.

Click on *Add* to save the medium, then go to the Permissions tab.

Permissions tab has a mandatory field *Role*. The role determines which
frontend elements the user can view and which actions he is allowed to
perform. Press Select and select one of the roles from the list. For
example, select *Admin role* to allow access to all Zabbix frontend
sections, except Administration. Later on, you can modify permissions or
create more user roles. Upon selecting a role, permissions will appear
in the same tab:

![user\_permissions.png](../../../assets/en/manual/config/user_permissions.png)


Click *Add* in the user properties form to save the user. The new user
appears in the userlist.

![](../../../assets/en/manual/quickstart/userlist2.png){width="600"}</source>
      </trans-unit>
      <trans-unit id="6e8f46b1" xml:space="preserve">
        <source>##### Adding permissions

By default, a new user has no permissions to access hosts and templates. To grant the
user rights, click on the group of the user in the *Groups* column (in
this case - 'Zabbix administrators'). In the group properties form, go
to the *Host permissions* tab to assign permissions to host groups.

![](../../../assets/en/manual/quickstart/group_permissions.png){width="600"}

This user is to have read-only access to *Linux servers* group, so click
on *Select* next to the host group selection field.

![](../../../assets/en/manual/quickstart/add_permissions.png){width="600"}

In this pop-up, mark the checkbox next to 'Linux servers', then click
*Select*. *Linux servers* should be displayed in the selection field.
Click the 'Read' button to set the permission level and then *Add* to
add the group to the list of permissions. In the user group properties
form, click *Update*.

To grant permissions to templates, you will need to switch to the *Template permissions* tab and specify template groups. 

::: noteimportant
In Zabbix, access rights to hosts and templates are assigned to
[user groups](/manual/config/users_and_usergroups/usergroup), not
individual users.
:::

Done! You may try to log in using the credentials of the new user.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
