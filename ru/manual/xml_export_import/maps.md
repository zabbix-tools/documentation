[comment]: # translation:outdated

[comment]: # ({new-c7c352d1})
# 4 Карты сети

[comment]: # ({/new-c7c352d1})

[comment]: # ({new-f69156a2})
#### Обзор

[Экспорт](/ru/manual/xml_export_import) карты сети содержит:

-   все связанные изображения
-   структуру карты - все настройки карты, все находящиеся на карте
    элементы с их настройками, связи карты и индикаторы состояний связей
    карты

Не экспортируются группы узлов сети, узлы сети, триггеры, другие карты и
любые другие элементы, которые могут быть связаны с экспортируемой
картой. Таким образом, если хотя бы один из элементов, на который
ссылается карта, отсутствует, импорт завершится с ошибкой.

Экспорт/импорт карт сетей поддерживается начиная с Zabbix 1.8.2.

[comment]: # ({/new-f69156a2})

[comment]: # ({new-bc82aec8})
#### Экспорт

Для экспорта карт сети сделайте следующее:

-   Перейдите в: *Мониторинг* → *Карты сети*
-   Отметьте карты сети, которые нужно экспортировать
-   Нажмите на *Экспорт* ниже списка

![](../../../assets/en/manual/xml_export_import/export_maps.png)

Выбранные карты сети экспортируются в локальный XML файл с именем по
умолчанию *zabbix\_export\_maps.xml*.

[comment]: # ({/new-bc82aec8})

[comment]: # ({new-cc31311c})
#### Импорт

Для импорта карт сети сделайте следующее:

-   Перейдите в: *Мониторинг* → *Карты сети*
-   Нажмите на *Импорт* справа
-   Выберите импортируемый файл
-   Отметьте необходимые опции в правилах импорта
-   Нажмите на *Импорт*

![](../../../assets/en/manual/xml_export_import/import_maps.png)

Все обязательные поля ввода отмечены красной звёздочкой.

После импорта в веб-интерфейсе отобразится сообщение об успехе или об
ошибке.

Правила импорта:

|Правило|Описание|
|--------------|----------------|
|*Обновить существующее*|Существующие элементы будут обновлены данными взятыми с файла импорта. В противном случае они не будут обновляться.|
|*Создать новое*|Импорт добавит новые элементы, используя данные из файла импорта. В противном случае они не будут добавляться.|

Если вы не отметите обе опции для карты сети и выберите соответствующие
опции для изображений, только изображения будут импортированы. Импорт
изображений доступен только пользователям Zabbix Супер Администраторам.

::: notewarning
Если заменить существующее изображение, это повлияет
на все карты, которые используют это изображение.
:::

[comment]: # ({/new-cc31311c})

[comment]: # ({new-53418cf1})
#### Формат экспорта

Экспорт маленькой карты сети с тремя элементами, их изображениями и
некоторыми связями между ними. Обратите внимание, что изображения
обрезаны, чтобы сэкономить место.

``` {.xml}
<?xml version="1.0" encoding="UTF-8"?>
<zabbix_export>
    <version>4.0</version>
    <date>2016-10-05T08:16:20Z</date>
    <images>
        <image>
            <name>Server_(64)</name>
            <imagetype>1</imagetype>
            <encodedImage>iVBOR...SuQmCC</encodedImage>
        </image>
        <image>
            <name>Workstation_(64)</name>
            <imagetype>1</imagetype>
            <encodedImage>iVBOR...SuQmCC</encodedImage>
        </image>
        <image>
            <name>Zabbix_server_3D_(96)</name>
            <imagetype>1</imagetype>
            <encodedImage>iVBOR...ggg==</encodedImage>
        </image>
    </images>
    <maps>
        <map>
            <name>Network</name>
            <width>590</width>
            <height>400</height>
            <label_type>0</label_type>
            <label_location>0</label_location>
            <highlight>1</highlight>
            <expandproblem>0</expandproblem>
            <markelements>1</markelements>
            <show_unack>0</show_unack>
            <severity_min>2</severity_min>
            <show_suppressed>0</show_suppressed>
            <grid_size>40</grid_size>
            <grid_show>1</grid_show>
            <grid_align>1</grid_align>
            <label_format>0</label_format>
            <label_type_host>2</label_type_host>
            <label_type_hostgroup>2</label_type_hostgroup>
            <label_type_trigger>2</label_type_trigger>
            <label_type_map>2</label_type_map>
            <label_type_image>2</label_type_image>
            <label_string_host/>
            <label_string_hostgroup/>
            <label_string_trigger/>
            <label_string_map/>
            <label_string_image/>
            <expand_macros>0</expand_macros>
            <background/>
            <iconmap/>
            <urls/>
            <selements>
                <selement>
                    <elementtype>0</elementtype>
                    <label>Host 1</label>
                    <label_location>-1</label_location>
                    <x>476</x>
                    <y>28</y>
                    <elementsubtype>0</elementsubtype>
                    <areatype>0</areatype>
                    <width>200</width>
                    <height>200</height>
                    <viewtype>0</viewtype>
                    <use_iconmap>0</use_iconmap>
                    <selementid>8</selementid>
                    <elements>
                        <element>
                            <host>Discovered host</host>
                        </element>
                    </elements>
                    <icon_off>
                        <name>Server_(64)</name>
                    </icon_off>
                    <icon_on/>
                    <icon_disabled/>
                    <icon_maintenance/>
                    <application/>
                    <urls/>
                </selement>
                <selement>
                    <elementtype>0</elementtype>
                    <label>Zabbix server</label>
                    <label_location>-1</label_location>
                    <x>252</x>
                    <y>50</y>
                    <elementsubtype>0</elementsubtype>
                    <areatype>0</areatype>
                    <width>200</width>
                    <height>200</height>
                    <viewtype>0</viewtype>
                    <use_iconmap>0</use_iconmap>
                    <selementid>6</selementid>
                    <elements>
                        <element>
                            <host>Zabbix server</host>
                        </element>
                    </elements>
                    <icon_off>
                        <name>Zabbix_server_3D_(96)</name>
                    </icon_off>
                    <icon_on/>
                    <icon_disabled/>
                    <icon_maintenance/>
                    <application/>
                    <urls/>
                </selement>
                <selement>
                    <elementtype>0</elementtype>
                    <label>New host</label>
                    <label_location>-1</label_location>
                    <x>308</x>
                    <y>230</y>
                    <elementsubtype>0</elementsubtype>
                    <areatype>0</areatype>
                    <width>200</width>
                    <height>200</height>
                    <viewtype>0</viewtype>
                    <use_iconmap>0</use_iconmap>
                    <selementid>7</selementid>
                    <elements>
                        <element>
                            <host>Zabbix host</host>
                        </element>
                    </elements>
                    <icon_off>
                        <name>Workstation_(64)</name>
                    </icon_off>
                    <icon_on/>
                    <icon_disabled/>
                    <icon_maintenance/>
                    <application/>
                    <urls/>
                </selement>
            </selements>
            <links>
                <link>
                    <drawtype>0</drawtype>
                    <color>008800</color>
                    <label/>
                    <selementid1>6</selementid1>
                    <selementid2>8</selementid2>
                    <linktriggers/>
                </link>
                <link>
                    <drawtype>2</drawtype>
                    <color>00CC00</color>
                    <label>100MBps</label>
                    <selementid1>7</selementid1>
                    <selementid2>6</selementid2>
                    <linktriggers>
                        <linktrigger>
                            <drawtype>0</drawtype>
                            <color>DD0000</color>
                            <trigger>
                                <description>Zabbix agent on {HOST.NAME} is unreachable for 5 minutes</description>
                                <expression>{Zabbix host:agent.ping.nodata(5m)}=1</expression>
                                <recovery_expression/>
                            </trigger>
                        </linktrigger>
                    </linktriggers>
                </link>
            </links>
        </map>
    </maps>
</zabbix_export>
```

[comment]: # ({/new-53418cf1})

[comment]: # ({new-efe541b5})
#### Теги элементов

Значения тегов элементов описаны в таблице ниже.

|Элемент|Свойство элемента|Тип|Диапазон|Описание|
|--------------|---------------------------------|------|----------------|----------------|
|images|<|<|<|Корневой элемент изображений.|
|image|<|<|<|Одиночное изображение.|
|<|name|`строка`|<|Уникальное имя изображения.|
|<|imagetype|`целое число`|1 - иконка<br>2 - фон|Тип изображения.|
|<|encodedImage|<|<|Изображение закодированное в Base64.|
|maps|<|<|<|Корневой элемент карт сети.|
|map|<|<|<|Одиночная карта.|
|<|name|`строка`|<|Уникальное имя карты.|
|<|width|`целое число`|<|Ширина карты, в пикселях.|
|<|height|`целое число`|<|Высота карты, в пикселях.|
|<|label\_type|`целое число`|0 - подпись<br>1 - IP адрес узла сети<br>2 - имя элемента<br>3 - только состояние<br>4 - ничего|Тип подписи к элементам карты.|
|<|label\_location|`целое число`|0 - снизу<br>1 - слева<br>2 - справа<br>3 - сверху|Расположение подписи к элементам карты по умолчанию.|
|<|highlight|`целое число`|0 - нет<br>1 - да|Включение подсветки иконок для активных триггеров и состояний узлов сети.|
|<|expandproblem|`целое число`|0 - нет<br>1 - да|Отображение триггера с проблемой у элементах с одной проблемой.|
|<|markelements|`целое число`|0 - нет<br>1 - да|Подсветка элементов карты, которые недавно изменили свое состояние.|
|<|show\_unack|`целое число`|0 - количество всех проблем<br>1 - количество неподтвержденных проблем<br>2 - количество подтвержденных и неподтвержденных проблем раздельно|Отображение проблем.|
|<|severity\_min|`целое число`|0 - не классифицировано<br>1 - информация<br>2 - предупреждение<br>3 - средняя<br>4 - высокая<br>5 - чрезвычайная|Минимальная важность триггеров по умолчанию, которая отображается на карте.|
|<|show\_suppressed|`целое число`|0 - нет<br>1 - да|Отображение проблем, которые в противном случае были бы подавлены (не показаны) по причине обслуживания узлов сети.|
|<|grid\_size|`целое число`|20, 40, 50, 75 или 100|Размер ячейки сетки карты в пикселях, если "grid\_show=1"|
|<|grid\_show|`целое число`|0 - да<br>1 - нет|Отображение сетки в настройке карты.|
|<|grid\_align|`целое число`|0 - да<br>1 - нет|Автоматическое выравнивание иконок в настройке карты.|
|<|label\_format|`целое число`|0 - нет<br>1 - да|Использование расширенной конфигурации подписей.|
|<|label\_type\_host|`целое число`|0 - подпись<br>1 - IP адрес узла сети<br>2 - имя элемента<br>3 - только состояние<br>4 - ничего<br>5 - пользовательская подпись|Метод отображения подписи к узлам сети, если "label\_format=1"|
|<|label\_type\_hostgroup|`целое число`|0 - подпись<br>2 - имя элемента<br>3 - только состояние<br>4 - ничего<br>5 - пользовательская подпись|Метод отображения подписи к группам узлов сети, если "label\_format=1"|
|<|label\_type\_trigger|`целое число`|0 - подпись<br>2 - имя элемента<br>3 - только состояние<br>4 - ничего<br>5 - пользовательская подпись|Метод отображения подписи к триггеров, если "label\_format=1"|
|<|label\_type\_map|`целое число`|0 - подпись<br>2 - имя элемента<br>3 - только состояние<br>4 - ничего<br>5 - пользовательская подпись|Метод отображения подписи к карт сети, если "label\_format=1"|
|<|label\_type\_image|`целое число`|0 - подпись<br>2 - имя элемента<br>4 - ничего<br>5 - пользовательская подпись|Метод отображения подписи к изображений, если "label\_format=1"|
|<|label\_string\_host|`строка`|<|Пользовательская подпись к элементам узлов сети, если "label\_type\_host=5"|
|<|label\_string\_hostgroup|`строка`|<|Пользовательская подпись к элементам групп узлов сети, если "label\_type\_hostgroup=5"|
|<|label\_string\_trigger|`строка`|<|Пользовательская подпись к элементам триггеров, если "label\_type\_trigger=5"|
|<|label\_string\_map|`строка`|<|Пользовательская подпись к элементам карт сети, если "label\_type\_map=5"|
|<|label\_string\_image|`строка`|<|Пользовательская подпись к элементам изображениям, если "label\_type\_image=5"|
|<|expand\_macros|`целое число`|0 - no<br>1 - yes|Раскрытие макросов в подписях при настройке карты.|
|<|background|`id`|<|ID изображения фона (если имеется), если "imagetype=2"|
|<|iconmap|`id`|<|ID соответствия иконок (если имеется).|
|urls|<|<|<|<|
|url|<|<|<|Одиночный URL.|
|<|name|`строка`|<|Имя ссылки.|
|<|url|`строка`|<|Сама ссылка URL.|
|<|elementtype|`целое число`|0 - узел сети<br>1 - карта<br>2 - триггер<br>3 - группа узлов сети<br>4 - изображение|Принадлежность ссылки к типу элемента карты.|
|selements|<|<|<|<|
|selement|<|<|<|Одиночный элемент карты.|
|<|elementtype|`целое число`|0 - узел сети<br>1 - карта сети<br>2 - триггер<br>3 - группа узлов сети<br>4 - изображение|Тип элемента карты.|
|<|label|`строка`|<|Подпись к иконке.|
|<|label\_location|`целое число`|-1 - использование умолчаний карты<br>0 - сниху<br>1 - слева<br>2 - справа<br>3 - сверху|<|
|<|x|`целое число`|<|Расположение на оси X.|
|<|y|`целое число`|<|Расположение на оси Y.|
|<|elementsubtype|`целое число`|0 - одна группа узлов сети<br>1 - все узлы сети группы|Подтип элемента, если "elementtype=3"|
|<|areatype|`целое число`|0 - вписать во всю карту<br>1 - пользовательский размер|Размер области, если "elementsubtype=1"|
|<|width|`целое число`|<|Ширина области, если "areatype=1"|
|<|height|`целое число`|<|Высота области, если "areatype=1"|
|<|viewtype|`целое число`|0 - располагать равномерно в области|Алгоритм расположения в области, если "elementsubtype=1"|
|<|use\_iconmap|`целое число`|0 - нет<br>1 - да|Использование соответствия иконок для этого элемента. Уместно только, если соответствие иконок активировано на уровне карты.|
|<|selementid|`id`|<|Уникальный ID записи элемента.|
|<|application|`строка`|<|Фильтр имени групп элементов данных. Если имя группы элементов данных задана, на карте сети будут отображаться только те проблемы и триггеры, которые принадлежат заданной группе элементов данных.|
|elements|<|<|<|<|
|element|<|<|<|Один Zabbix объект, который представлен на карте (карта, группа узлов сети, узел сети и т.д.).|
|<|host|<|<|<|
|icon\_off|<|<|<|Используемое изображение, когда элемент в состоянии 'OK'.|
|icon\_on|<|<|<|Используемое изображение, когда элемент в состоянии 'Проблема'.|
|icon\_disabled|<|<|<|Используемое изображение, когда элемент деактивирован.|
|icon\_maintenance|<|<|<|Используемое изображение, когда элемент в обслуживании.|
|<|name|`строка`|<|Уникальное имя изображения.|
|links|<|<|<|<|
|link|<|<|<|Одиночная связь между элементами карты.|
|<|drawtype|`целое число`|0 - линия<br>2 - жирная линия<br>3 - точечная линия<br>4 - пунктирная линия|Стиль связи.|
|<|color|`строка`|<|Цвет связи (6 символов, hex).|
|<|label|`строка`|<|Подпись к связи.|
|<|selementid1|`id`|<|ID первого элемента, с которым соединена связь.|
|<|selementid2|`id`|<|ID второго элемента, с которым соединена связь.|
|linktriggers|<|<|<|<|
|linktrigger|<|<|<|Одиночный индикатор состояния связи.|
|<|drawtype|`целое число`|0 - линия<br>2 - жирная линия<br>3 - точечная линия<br>4 - пунктирная линия|Стиль связи, когда триггер в состоянии 'Проблема'.|
|<|color|`строка`|<|Цвет связи (6 символов, hex), когда триггер в состоянии 'Проблема'.|
|trigger|<|<|<|Используемый триггер для индикации состояния связи.|
|<|description|`строка`|<|Имя триггера.|
|<|expression|`строка`|<|Выражение триггера.|
|<|recovery\_expression|`строка`|<|Выражение восстановления триггера.|

[comment]: # ({/new-efe541b5})
