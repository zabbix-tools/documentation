[comment]: # translation:outdated

[comment]: # ({5b415a8d-8c911baa})
# 6. Готовое решение Zabbix

[comment]: # ({/5b415a8d-8c911baa})

[comment]: # ({19299ac1-cfa1a361})
#### Обзор

В качестве альтернативы ручной настройке или повторного использования уже существующего сервера для Zabbix, пользователи
могут [загрузить](http://www.zabbix.com/download_appliance) готовое решение Zabbix
или CD образ готового решения Zabbix.

Версии готового решения Zabbix и установочного CD базируются на CentOS 8
(x86\_64).

Установочный CD готового решения Zabbix можно использовать для быстрого развертывания Zabbix сервера (MySQL).

::: noteimportant
 Вы можете использовать это готовое решение для ознакомления с Zabbix.
Готовое решение не предусмотрено для серьезной эксплуатации. 
:::

[comment]: # ({/19299ac1-cfa1a361})

[comment]: # ({eeaf44e5-cffa82a3})
##### Системные требования:

-   *ОЗУ*: 1.5 ГБ
-   *Место на диске*: как минимум 8 ГБ места должно быть зарезервировано для виртуальной машины.

Загрузочное меню установочного CD/DVD Zabbix:

![](../../assets/en/manual/installation_cd_boot_menu1.png){width="600"}

В готовом решении Zabbix содержится Zabbix сервер (настроенный и работающий с
MySQL) и веб-интерфейс.

Готовое решение Zabbix доступно в следующих форматах:

-   VMWare (.vmx)
-   Open virtualization format (.ovf)
-   Microsoft Hyper-V 2012 (.vhdx)
-   Microsoft Hyper-V 2008 (.vhd)
-   KVM, Parallels, QEMU, USB stick, VirtualBox, Xen (.raw)
-   KVM, QEMU (.qcow2)

Чтобы начать, запустите готовое решение и перейдите в вашем браузере по IP, полученном по DHCP.

::: noteimportant
 DHCP должен быть доступен на узле. 
:::

Чтобы получить IP адрес, на виртуальной машине выполните:

    ip addr show

Для получения доступа к внешнему веб-интерфейсу Zabbix, перейдите на **http://<host\_ip>** (для доступа через браузер узла, режим моста сети должен быть включен в настройках виртуальной машины).

::: notetip
Если готовое решение не запускается в Hyper-V, Вы можете попробовать использовать `Ctrl+Alt+F2` чтобы переключить tty сессии.
:::

[comment]: # ({/eeaf44e5-cffa82a3})

[comment]: # ({f40e9e3c-589fd5e2})
#### - Изменения в конфигурации CentOS 8

Готовое решение базируется на CentOS 8. В базовую конфигурацию CentOS внесены некоторые изменения.

[comment]: # ({/f40e9e3c-589fd5e2})

[comment]: # ({8c4a8df9-193d3b23})
##### - Репозитории

Официальный 
[репозиторий](/manual/installation/install_from_packages/rhel_centos) Zabbix добавлен в */etc/yum.repos.d*:

    [zabbix]
    name=Zabbix Official Repository - $basearch
    baseurl=http://repo.zabbix.com/zabbix/5.2/rhel/8/$basearch/
    enabled=1
    gpgcheck=1
    gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-ZABBIX-A14FE591

[comment]: # ({/8c4a8df9-193d3b23})

[comment]: # ({fb9ca6c8-589dc798})
##### - Конфигурация брандмауэра

Готовое решение использует брандмауэр iptables с предустановленными правилами:

-   Открыт порт SSH (22 TCP);
-   Открыты порты Zabbix агента (10050 TCP) и Zabbix траппера (10051 TCP);
-   Открыты порты HTTP (80 TCP) и HTTPS (443 TCP);
-   Открыт порт SNMP трапов (162 UDP)
-   Открыты исходящие подключения на порт NTP (53 UDP);
-   ICMP пакеты ограничены 5 пакетами в секунду;
-   Все остальные входящие подключения отбрасываются.

[comment]: # ({/fb9ca6c8-589dc798})

[comment]: # ({1243cf3f-b2283a9a})
##### - Использование статического IP адреса

По умолчанию готовое решение использует DHCP для получения IP адреса. Для того, чтобы указать
статический IP адрес:

-   Авторизуйтесь под пользователем root;
-   Откройте файл */etc/sysconfig/network-scripts/ifcfg-eth0*;
-   Замените *BOOTPROTO=dhcp* на *BOOTPROTO=none*
-   Добавьте следующие строки:
    -   *IPADDR=<IP address of the appliance>*
    -   *PREFIX=<CIDR prefix>*
    -   *GATEWAY=<gateway IP address>*
    -   *DNS1=<DNS server IP address>*
-   Выполните команду **systemctl restart network**.

Обратитесь к
[официальной документации](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/deployment_guide/s1-networkscripts-interfaces)
Red Hat в случае необходимости.

[comment]: # ({/1243cf3f-b2283a9a})

[comment]: # ({582b8716-acf06935})
##### - Изменение часового пояса

По умолчанию готовое решение использует UTC для системного времени. Чтобы изменить часовой пояс, скопируйте соответствующий файл из */usr/share/zoneinfo* в
*/etc/localtime*, например:

    cp /usr/share/zoneinfo/Europe/Riga /etc/localtime

[comment]: # ({/582b8716-acf06935})

[comment]: # ({4a771212-2c378c8d})
#### - Конфигурация Zabbix

В инсталляции готового решения Zabbix используются следующие пароли и изменения конфигурации:

[comment]: # ({/4a771212-2c378c8d})

[comment]: # ({e5cb98bf-d39b5151})
##### - Учетные данные (логин:пароль)

Системные:

-   root:zabbix

Внешний интерфейс Zabbix:

-   Admin:zabbix

База данных:

-   root:<random>
-   zabbix:<random>

::: noteclassic
Пароли к базе данных генерируются случайным образом в процессе установки.\
Пароль root хранится в файле /root/.my.cnf. Таким образом не требуется вводить пароль, находясь под аккаунтом "root".
:::

Для изменения пароля пользователя базы данных, потребуется внести изменения в следующих местах:

-   MySQL;
-   /etc/zabbix/zabbix\_server.conf;
-   /etc/zabbix/web/zabbix.conf.php.

::: noteclassic
 Отдельные пользователи `zabbix_srv` и `zabbix_web` настроены для работы с сервером и веб-интерфейсом. 
:::

[comment]: # ({/e5cb98bf-d39b5151})

[comment]: # ({37b689d8-f11ee379})
##### - Размещение файлов

-   Файлы конфигурации расположены в **/etc/zabbix**.
-   Файлы журналов (логи) Zabbix сервера, прокси и агента расположены в
    **/var/log/zabbix**.
-   Внешний интерфейс Zabbix расположен в **/usr/share/zabbix**.
-   Домашний каталог пользователя **zabbix** is **/var/lib/zabbix**.

[comment]: # ({/37b689d8-f11ee379})

[comment]: # ({b50307b6-e00773c5})
##### - Изменения в настройках Zabbix

-   Часовой пояс внешнего веб-интерфейса изменен на Europe/Riga (этот параметр можно изменить в
    **/etc/php-fpm.d/zabbix.conf**);

[comment]: # ({/b50307b6-e00773c5})

[comment]: # ({994f7a1a-2c4e4239})
#### - Доступ к внешнему интерфейсу

По умолчанию доступ к веб-интерфейсу разрешен отовсюду.

Доступ к внешнему интерфейсу может быть получен используя *http://<host>*.

Этот параметр может быть изменен в  **/etc/nginx/conf.d/zabbix.conf**. Необходимо перезапустить Nginx после внесения изменений в данный файл. Чтобы перезапустить веб-сервер, авторизуйтесь как **root** пользователь используя SSH
и выполните:

    systemctl restart nginx

[comment]: # ({/994f7a1a-2c4e4239})

[comment]: # ({da6deb37-4fbc391b})
#### - Брендмауэр

По умолчанию, только адреса портов перечисленные в [изменениях конфигурации](#firewall_configuration) выше открыты для подключений. Чтобы внести дополнительные порты, отредактируйте файл "*/etc/sysconfig/iptables*" и перезагрузите правила брендмауэра:

    systemctl reload iptables

[comment]: # ({/da6deb37-4fbc391b})

[comment]: # ({8a8abdf7-bf60554b})
#### - Обновление

Пакеты готового решения Zabbix могут быть обновлены. Для обновления, выполните:

    dnf update zabbix*

[comment]: # ({/8a8abdf7-bf60554b})

[comment]: # ({27a0a99e-d167c766})
#### - Системные службы

Доступные службы Systemd :

    systemctl list-units zabbix*

[comment]: # ({/27a0a99e-d167c766})

[comment]: # ({bbd3a000-a582d1bf})
#### - Заметки о специфических форматах

[comment]: # ({/bbd3a000-a582d1bf})

[comment]: # ({fbe7901d-a45df55b})
##### - VMware

Образы в формате *vmdk* можно использовать напрямую в продуктах VMware Player, Server
и Workstation. Для использования в ESX, ESXi и vSphere образы должны быть сконвертированы используя [VMware
конвертера](http://www.vmware.com/products/converter/).

[comment]: # ({/fbe7901d-a45df55b})

[comment]: # ({35bdfd17-7d1c1440})
##### - HDD/flash образ (raw)

    dd if=./zabbix_appliance_5.2.0.raw of=/dev/sdc bs=4k conv=fdatasync

Замените */dev/sdc* на ваше Flash/HDD устройство.

[comment]: # ({/35bdfd17-7d1c1440})
