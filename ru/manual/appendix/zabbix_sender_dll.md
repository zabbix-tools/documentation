[comment]: # translation:outdated

[comment]: # ({new-a93134cd})
# 15 Динамическая библиотека Zabbix для Windows

Приложения в среде Windows могут использовать динамическую библиотеку
Zabbix sender (zabbix\_sender.dll)для отправки данных напрямую Zabbix
серверу/прокси, вместо выполнения внешнего процесса
(zabbix\_sender.exe).

Динамическая библиотека вместе с файлами для разработки находятся в
папке bin\\winXX\\dev. Чтобы ею воспользоваться, включите заголовочный
файл zabbix\_sender.h и свяжите с библиотекой zabbix\_sender.lib. Пример
использования Zabbix sender API можно найти в папке
build\\win32\\examples\\zabbix\_sender.

Динамической библиотекой Zabbix sender обеспечиваются следующие
возможности:

|`int zabbix_sender_send_values(const char *address, unsigned short port,const char *source, const zabbix_sender_value_t *values, int count,char **result);`{.c}|<|<|
|---------------------------------------------------------------------------------------------------------------------------------------------------------------|-|-|
|char **result);`{.c}|<|<|

Динамической библиотекой Zabbix sender используются следующие структуры
данных:

``` {.c}
typedef struct
{
    /* host name, must match the name of target host in Zabbix (имя узла сети, должно совпадать с целевым именем узла сети в Zabbix)*/
    char    *host;
    /* the item key (ключ элемента данных)*/
    char    *key;
    /* the item value (значение элемента данных)*/
    char    *value;
}
zabbix_sender_value_t;

typedef struct
{
    /* number of total values processed (общее количество обработанных значений)*/
    int total;
    /* number of failed values (количество неудачных значений)*/
    int failed;
    /* time in seconds the server spent processing the sent values (время  в секундах затраченное сервером на обработку отправленных значений)*/
    double  time_spent;
}
zabbix_sender_info_t;
```

[comment]: # ({/new-a93134cd})
