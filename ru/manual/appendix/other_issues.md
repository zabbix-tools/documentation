[comment]: # translation:outdated

[comment]: # ({new-7c9bb1c5})
# 17 Остальные проблемы

[comment]: # ({/new-7c9bb1c5})

[comment]: # ({new-ab4b69b8})
#### Вход в систему и systemd

Мы рекомендуем
[создать](/ru/manual/installation/install#создайте_аккаунт_пользователя)
*zabbix* пользователя системным пользователем, то есть, без возможности
входа в систему. Некоторые пользователи игнорируют эту рекомендацию и
используют тот же самый аккаунт для входа в систему (например, используя
SSH) на хост с работающим Zabbix. Это может привести к поломке Zabbix
демона при выходе из системы. В этом случае в журнале Zabbix сервера вы
получите что-то наподобие следующего:

    zabbix_server [27730]: [file:'selfmon.c',line:375] lock failed: [22] Invalid argument
    zabbix_server [27716]: [file:'dbconfig.c',line:5266] lock failed: [22] Invalid argument
    zabbix_server [27706]: [file:'log.c',line:238] lock failed: [22] Invalid argument

и в журнале Zabbix агента:

    zabbix_agentd [27796]: [file:'log.c',line:238] lock failed: [22] Invalid argument

Такая проблема происходит так как по умолчанию настройка systemd равна
`RemoveIPC=yes` в `/etc/systemd/logind.conf`. Когда вы выполняете выход
из системы, ранее созданные семафоры Zabbix компонентами удаляются, что
приводит к поломке.

Выдержка из документации по systemd:

    RemoveIPC=

    Controls whether System V and POSIX IPC objects belonging to the user shall be removed when the
    user fully logs out. Takes a boolean argument. If enabled, the user may not consume IPC resources
    after the last of the user's sessions terminated. This covers System V semaphores, shared memory
    and message queues, as well as POSIX shared memory and message queues. Note that IPC objects of the
    root user and other system users are excluded from the effect of this setting. Defaults to "yes".

Имеется 2 способа решения этой проблемы:

1.  (рекомендуемый) Перестаньте использовать *zabbix* аккаунт для
    чего-то кроме zabbix процессов, создайте специальный аккаунт для
    остальных потребностей.
2.  (не рекомендуемый) Задайте `RemoveIPC=no` в
    `/etc/systemd/logind.conf` и перезагрузите систему. Обратите
    внимание, что `RemoveIPC` является общесистемным параметром, его
    изменение повлияет на всю систему.

[comment]: # ({/new-ab4b69b8})




[comment]: # ({new-ce7659d9})
#### Using Zabbix frontend behind proxy

If Zabbix frontend runs behind proxy server, the cookie path in the
proxy configuration file needs to be rewritten in order to match the
reverse-proxied path. See examples below. If the cookie path is not
rewritten, users may experience authorization issues, when trying to
login to Zabbix frontend.

[comment]: # ({/new-ce7659d9})

[comment]: # ({new-4db2f7ca})
##### Example configuration for nginx

    # ..
    location / {
    # ..
    proxy_cookie_path /zabbix /;
    proxy_pass http://192.168.0.94/zabbix/;
    # ..

[comment]: # ({/new-4db2f7ca})

[comment]: # ({new-55cc0aaf})
##### Example configuration for Apache

    # ..
    ProxyPass "/" http://host/zabbix/
    ProxyPassReverse "/" http://host/zabbix/
    ProxyPassReverseCookiePath /zabbix /
    ProxyPassReverseCookieDomain host zabbix.example.com
    # ..

[comment]: # ({/new-55cc0aaf})
