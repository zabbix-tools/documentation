[comment]: # translation:outdated

[comment]: # ({new-d53fbea3})
# 10 Выполнение команд

Zabbix использует единый функционал для выполнения внешних проверок,
пользовательских параметров, элементов данных system.run,
пользовательских скриптов оповещений, удаленных команд и
пользовательских скриптов.

Команда/скрипт выполняется одинаково как на Unix, так и на Windows
платформах:

1.  Zabbix (родительский процесс) создает канал связи
2.  Zabbix устанавливает этот канал связи для вывода данных от
    созданного дочернего процесса
3.  Zabbix создает дочерний процесс (выполняет команду/скрипт)
4.  Создается новая группа процесса (в Unix) или задача (в Windows) для
    дочернего процесса
5.  Zabbix считывает из канала данные по истечении установленного
    времени ожидания или до момента, когда запись прекратится (ВСЕ
    обработчики/файловые дескрипторы закрыты). Обратите внимание, что
    дочерний процесс может создать еще некоторое количество процессов и
    выйти до того, как эти процессы завершатся или будет закрыт
    обработчик/файловый дескриптор.
6.  Если установленное время ожидания не достигнуто, то Zabbix ждет
    завершения начального дочернего процесса или ждет достижения
    установленного времени ожидания.
7.  Если начальный дочерний процесс завершил свою работу и время
    ожидания не вышло, тогда Zabbix проверяет код вызода начального
    дочернего процесса и сравнивает его с 0 (ненулевое значение
    считается ошибкой выполнения, только для пользовательских скриптов
    оповещений, удаленных команд и пользовательских скриптов выполняемых
    на Zabbix сервере и Zabbix прокси)
8.  На этот момент подразумевается, что всё выполнено и всё дерево
    процессов (т.е. группа процессов или задача) завершены.

::: noteimportant
Zabbix предполагает, что команда/скрипт завершили
обработку, в тот момент, когда завершился изначальный дочерний процесс И
никакие другие процессы все еще не держат открытым обработчик/файловый
дескриптор. Когда обработка завершена, ВСЕ созданные процессы
завершаются.
:::

Все двойные кавычки и обратная косая черта в команде экранируются
обратной косой чертой и вся команда заключается в двойные кавычки.

[comment]: # ({/new-d53fbea3})

[comment]: # ({new-a11705f0})
#### Проверка кода выхода

Код выхода проверяется на соответствие следующим условиям:

    *только для пользовательских скриптов оповещений, удаленных команд и пользовательских скриптов выполняемых на Zabbix сервере и Zabbix прокси.
    *Любой код выхода, отличный от 0 считается ошибкой выполнения.
    *Содержимое стандарного вывода ошибки и стандартного вывода собирается при ошибочных выполнениях и доступно в веб-интерфейсе (где отображается результат выполнения).
    *Добавляется дополнительная запись в журнал для удаленных команд на Zabbix сервере, чтобы сохранить вывод выполнения скрипта, эту возможность можно включить, используя [[:ru/manual/appendix/config/zabbix_agentd|параметр]] агента LogRemoteCommands.

Возможные сообщения в веб-интерфейсе и записи в журналах при ошибочных
командах/скриптах:

-   Содержимое стандарного вывода ошибки и стандартного вывода при
    ошибочных выполнениях (если имеется).
-   "Process exited with code: N." (при пустом выводе и коде выхода
    отлично от 0).
-   "Process killed by signal: N." (при завершении процесса сигналом,
    только на Linux).
-   "Process terminated unexpectedly." (при завершениях процесса по
    неизвестным причинам).

------------------------------------------------------------------------

Подробнее о:

-   [Внешние
    проверки](/ru/manual/config/items/itemtypes/external#результат_внешней_проверки)
-   [Пользовательские параметры](/ru/manual/config/items/userparameters)
-   Элементы данных
    [system.run](/ru/manual/config/items/itemtypes/zabbix_agent)
-   [Пользовательские скрипты
    оповещений](/ru/manual/config/notifications/media/script)
-   [Удаленные
    команды](/ru/manual/config/notifications/action/operation/remote_command)
-   [Глобальные
    скрипты](/ru/manual/web_interface/frontend_sections/administration/scripts)

[comment]: # ({/new-a11705f0})


[comment]: # ({new-ddd381e9})
#### Exit code checking

Exit code are checked with the following conditions:

-   Only for custom alert scripts, remote commands and user scripts
    executed on Zabbix server and Zabbix proxy.
-   Any exit code that is different from 0 is considered as execution
    failure.
-   Contents of standard error and standard output for failed executions
    are collected and available in frontend (where execution result is
    displayed).
-   Additional log entry is created for remote commands on Zabbix server
    to save script execution output and can be enabled using
    LogRemoteCommands agent
    [parameter](/manual/appendix/config/zabbix_agentd).

Possible frontend messages and log entries for failed commands/scripts:

-   Contents of standard error and standard output for failed executions
    (if any).
-   "Process exited with code: N." (for empty output, and exit code not
    equal to 0).
-   "Process killed by signal: N." (for process terminated by a signal,
    on Linux only).
-   "Process terminated unexpectedly." (for process terminated for
    unknown reasons).

------------------------------------------------------------------------

Read more about:

-   [External
    checks](/manual/config/items/itemtypes/external#external_check_result)
-   [User parameters](/manual/config/items/userparameters)
-   [system.run](/manual/config/items/itemtypes/zabbix_agent) items
-   [Custom alert scripts](/manual/config/notifications/media/script)
-   [Remote
    commands](/manual/config/notifications/action/operation/remote_command)
-   [Global
    scripts](/manual/web_interface/frontend_sections/administration/scripts)

[comment]: # ({/new-ddd381e9})
