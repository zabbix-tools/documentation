[comment]: # translation:outdated

[comment]: # ({new-17a30eba})
# 14 Обработка ошибок базы данных

Если Zabbix обнаруживает, что база данных недоступна, он отправляет
сообщение и продолжает попытки подключения к базе данных. Для некоторых
механизмов баз данных, распознаются определенные коды ошибок.

[comment]: # ({/new-17a30eba})

[comment]: # ({new-537463f7})
#### MySQL

-   CR\_CONN\_HOST\_ERROR
-   CR\_SERVER\_GONE\_ERROR
-   CR\_CONNECTION\_ERROR
-   CR\_SERVER\_LOST
-   CR\_UNKNOWN\_HOST
-   ER\_SERVER\_SHUTDOWN
-   ER\_ACCESS\_DENIED\_ERROR
-   ER\_ILLEGAL\_GRANT\_FOR\_TABLE
-   ER\_TABLEACCESS\_DENIED\_ERROR
-   ER\_UNKNOWN\_ERROR

[comment]: # ({/new-537463f7})
