<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="ru" datatype="plaintext" original="manual/appendix/install/locales.md">
    <body>
      <trans-unit id="2dc59dbb" xml:space="preserve">
        <source># 15 Additional frontend languages</source>
      </trans-unit>
      <trans-unit id="50c452f4" xml:space="preserve">
        <source>#### Overview

In order to use any other language than English in Zabbix web interface,
its locale should be installed on the web server. Additionally, the PHP
gettext extension is required for the translations to work.</source>
      </trans-unit>
      <trans-unit id="bb6c0b89" xml:space="preserve">
        <source>#### Installing locales

To list all installed languages, run:

    locale -a

If some languages that are needed are not listed, open the
*/etc/locale.gen* file and uncomment the required locales. Since Zabbix
uses UTF-8 encoding, you need to select locales with UTF-8 charset.

Now run:

    locale-gen 

Restart the web server.

The locales should now be installed. It may be required to reload Zabbix
frontend page in browser using Ctrl + F5 for new languages to appear.</source>
      </trans-unit>
      <trans-unit id="b0831966" xml:space="preserve">
        <source>#### Installing Zabbix

If installing Zabbix directly from [Zabbix git
repository](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse),
translation files should be generated manually. To generate translation
files, run:

    make gettext
    locale/make_mo.sh

This step is not needed when installing Zabbix from packages or source
tar.gz files.</source>
      </trans-unit>
      <trans-unit id="5dccdfbc" xml:space="preserve">
        <source>#### Selecting a language

There are several ways to select a language in Zabbix web interface:

-   When installing web interface - in the frontend [installation
    wizard](/manual/installation/frontend#welcome_screen). Selected
    language will be set as system default.
-   After the installation, system default language can be changed in
    the *Administration→General→GUI* [menu
    section](/manual/web_interface/frontend_sections/administration/general#gui).
-   Language for a particular user can be changed in the [user
    profile](/manual/web_interface/user_profile#user_profile).

If a locale for a language is not installed on the machine, this
language will be greyed out in Zabbix language selector. A red icon is
displayed next to the language selector if at least one locale is
missing. Upon pressing on this icon the following message will be
displayed: "You are not able to choose some of the languages, because
locales for them are not installed on the web server."

![locale\_warning.png](../../../../assets/en/manual/appendix/install/locale_warning.png){width="600"}</source>
      </trans-unit>
    </body>
  </file>
</xliff>
