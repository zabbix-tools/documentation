[comment]: # translation:outdated

[comment]: # ({d567078e-7af1bbaa})
# 1 Создание базы данных

[comment]: # ({/d567078e-7af1bbaa})

[comment]: # ({f4682f89-fab90562})
#### Обзор

База данных Zabbix должна быть создана в процессе установки Zabbix сервера или прокси.

Этот раздел предоставляет инструкции для создания базы данных Zabbix. 
Отдельный набор инструкций доступен для каждой из поддерживаемых баз данных.

UTF-8 - единственная кодировка поддерживаемая Zabbix. Так же известная тем, что работает без каких-либо уязвимостей в безопасности. Пользователи должны осознавать возможное наличии уязвимостей при использовании некоторых других кодировок.

[comment]: # ({/f4682f89-fab90562})

[comment]: # ({e68f571a-fad527fc})

::: noteclassic
При установке из [Zabbix Git
репозитория](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse), Вам необходимо выполнить команду:

`$ make dbschema` 

прежде, чем приступать к следующим шагам. 
:::

[comment]: # ({/e68f571a-fad527fc})

[comment]: # ({45cd0315-1e36e539})
#### MySQL

Набор символов utf8 (так же utf8mb3) и utf8mb4 поддерживаются (с сопоставлениями
utf8\_bin и utf8mb4\_bin соответственно) для корректной работы Zabbix
сервера/прокси с базой данных MySQL. Для новых установок рекоммендуется 
использовать utf8mb4.

    shell> mysql -uroot -p<password>
    mysql> create database zabbix character set utf8mb4 collate utf8mb4_bin;
    mysql> create user 'zabbix'@'localhost' identified by '<password>';
    mysql> grant all privileges on zabbix.* to 'zabbix'@'localhost';
    mysql> quit;

::: notewarning
Если Вы устанавливаете Zabbix из **пакетов**, останитесь на этом месте
и продолжите используя инструкции для
[RHEL/CentOS](https://www.zabbix.com/download?zabbix=5.0&os_distribution=red_hat_enterprise_linux&os_version=8&db=mysql)
или
[Debian/Ubuntu](https://www.zabbix.com/download?zabbix=5.0&os_distribution=debian&os_version=10_buster&db=mysql)
чтобы импортировать данные в базу данных.
:::

Если Вы устанавливаете Zabbix из исходных кодов, перейдите к импортированию данных в базу данных.
Для базы данных Zabbix прокси, нужно импортировать только `schema.sql` (без images.sql или data.sql):

    shell> cd database/mysql
    shell> mysql -uzabbix -p<password> zabbix < schema.sql
    # остановитесь здесь, если вы создаете базу данных для Zabbix прокси
    shell> mysql -uzabbix -p<password> zabbix < images.sql
    shell> mysql -uzabbix -p<password> zabbix < data.sql

[comment]: # ({/45cd0315-1e36e539})

[comment]: # ({96b03735-61d6043c})
#### PostgreSQL

У Вас должен быть создан пользователь базы данных с правами на создание обьектов базы данных. 
Следующая команда создаст пользователя `zabbix`. Задайте
пароль при появлении запроса и затем введите пароль повторно (обратите внимание, что в начале
может быть запрошен `sudo` пароль):

    shell> sudo -u postgres createuser --pwprompt zabbix

Теперь мы создадим базу данных `zabbix` (последний параметр) с раннее созданным пользователем
в качестве владельца (`-O zabbix`).

    shell> sudo -u postgres createdb -O zabbix -E Unicode -T template0 zabbix

::: notewarning
Если Вы устанавливаете Zabbix из **пакетов**, остановитесь
здесь и далее следуйте инструкциям для
[RHEL/CentOS](https://www.zabbix.com/download?zabbix=5.0&os_distribution=red_hat_enterprise_linux&os_version=8&db=postgresql)
или
[Debian/Ubuntu](https://www.zabbix.com/download?zabbix=5.0&os_distribution=debian&os_version=10_buster&db=postgresql)
для импорта схемы и данных в базу данных.
:::

Если Вы устанавливаете Zabbix из исходных кодов, переходите к импорту
схемы и данных (предпологается, что вы находитесь в корневой директории исходных кодов Zabbix).
Для базы данных Zabbix прокси, необходимо импортировать только `schema.sql` (без images.sql и без data.sql).

    shell> cd database/postgresql
    shell> cat schema.sql | sudo -u zabbix psql zabbix
    # остановитесь здесь, если Вы создаёте базу данных для Zabbix прокси
    shell> cat images.sql | sudo -u zabbix psql zabbix
    shell> cat data.sql | sudo -u zabbix psql zabbix

::: noteimportant
Приведенные выше команды предоставлены как пример, который будет работать на большинстве GNU/Linux инсталляций.
Вы можете использовать и другие команды, например, "psql -U <username>" в зависимости от того,
каким образом настроена Ваша система/база данных. Если у Вас возникли трудности с настройкой
базы данных, пожалуйста, проконсультируйтесь с Вашим администратором баз данных.
:::

[comment]: # ({/96b03735-61d6043c})

[comment]: # ({31815407-cc68ca58})
#### TimescaleDB

Инструкции по созданию и настройке TimescaleDB приведены в отдельном [разделе](/manual/appendix/install/timescaledb).

[comment]: # ({/31815407-cc68ca58})

[comment]: # ({3199abec-7b4d56a7})
#### Oracle

Инструкции по созданию и настройке базы данных Oracle приведены в отдельном [разделе](/manual/appendix/install/oracle).

[comment]: # ({/3199abec-7b4d56a7})


[comment]: # ({8b805ae0-02d49e4f})
#### SQLite

Использование SQLite поддерживается только для **Zabbix прокси**!

::: noteclassic
Если Вы используете SQLite для Zabbix прокс, база данных
будет создана автоматически, в случае если она не существует
:::

    shell> cd database/sqlite3
    shell> sqlite3 /var/lib/sqlite/zabbix.db < schema.sql

Вернуться в [раздел установки](/manual/installation/install).

[comment]: # ({/8b805ae0-02d49e4f})
