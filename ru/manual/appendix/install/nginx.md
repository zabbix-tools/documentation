[comment]: # translation:outdated

[comment]: # ({new-840eea1a})
# 7 Примечания по настройке Nginx для Zabbix на SLES дистрибутивах

[comment]: # ({/new-840eea1a})

[comment]: # ({new-4b966990})
#### SLES 12

В SUSE Linux Enterprise Server 12 вам потребуется добавить Nginx
репозиторий, до установки Nginx:

    zypper addrepo -G -t yum -c 'http://nginx.org/packages/sles/12' nginx

Вам также потребуется настроить `php-fpm`:

    cp /etc/php5/fpm/php-fpm.conf{.default,}
    sed -i 's/user = nobody/user = wwwrun/; s/group = nobody/group = www/' /etc/php5/fpm/php-fpm.conf

[comment]: # ({/new-4b966990})

[comment]: # ({new-fb2c0b94})
#### SLES 15

В SUSE Linux Enterprise Server 15 вам потребуется настроить `php-fpm`:

    cp /etc/php7/fpm/php-fpm.conf{.default,}
    cp /etc/php7/fpm/php-fpm.d/www.conf{.default,}
    sed -i 's/user = nobody/user = wwwrun/; s/group = nobody/group = www/' /etc/php7/fpm/php-fpm.d/www.conf

[comment]: # ({/new-fb2c0b94})


[comment]: # ({new-31bd54eb})
#### SLES 15

In SUSE Linux Enterprise Server 15 you need to configure `php-fpm`:

    cp /etc/php7/fpm/php-fpm.conf{.default,}
    cp /etc/php7/fpm/php-fpm.d/www.conf{.default,}
    sed -i 's/user = nobody/user = wwwrun/; s/group = nobody/group = www/' /etc/php7/fpm/php-fpm.d/www.conf

[comment]: # ({/new-31bd54eb})
