[comment]: # translation:outdated

[comment]: # ({new-b19c703a})
# 12 Setting up scheduled reports

[comment]: # ({/new-b19c703a})

[comment]: # ({new-bb42dbf1})
#### Overview

This section provides instructions on installing Zabbix web service and
configuring Zabbix to enable generation of scheduled reports.

::: noteimportant
 Currently the support of scheduled reports is
experimental. 
:::

[comment]: # ({/new-bb42dbf1})

[comment]: # ({new-87c4cf08})
#### Installation

A new [Zabbix web service](/manual/concepts/web_service) process and
Google Chrome browser should be installed to enable generation of
scheduled reports. The web service may be installed on the same machine
where the Zabbix server is installed or on a different machine. Google
Chrome browser should be installed on the same machine, where the web
service is installed.

[comment]: # ({/new-87c4cf08})


[comment]: # ({new-b0d2832d})
To compile Zabbix web service from sources, see [Installing Zabbix web
service](/manual/installation/install#installing_zabbix_web_service).

After the installation, run zabbix\_web\_service on the machine, where the web service is installed:

    shell> zabbix_web_service
    

[comment]: # ({/new-b0d2832d})

[comment]: # ({new-1fd5a212})
#### Configuration

To ensure proper communication between all elements involved make sure
server configuration file and frontend configuration parameters are
properly configured.

[comment]: # ({/new-1fd5a212})

[comment]: # ({new-064f3be0})
##### Zabbix server

The following parameters in Zabbix server configuration file need to be
updated: *WebServiceURL* and *StartReportWriters*.

**WebServiceURL**

This parameter is required to enable communication with the web service.
The URL should be in the format `<host:port>/report`.

-   By default, the web service listens on port 10053. A different port
    can be specified in the web service configuration file.
-   Specifying the `/report` path is mandatory (the path is hardcoded
    and cannot be changed).

Example:

    WebServiceURL=http://localhost:10053/report

**StartReportWriters**

This parameter determines how many report writer processes should be
started. If it is not set or equals 0, report generation is disabled.
Based on the number and frequency of reports required, it is possible to
enable from 1 to 100 report writer processes.

Example:

    StartReportWriters=3

[comment]: # ({/new-064f3be0})

[comment]: # ({new-613109e8})
##### Zabbix frontend

A *Frontend URL* parameter should be set to enable communication between
Zabbix frontend and Zabbix web service:

-   Proceed to the *Administration → General → Other parameters*
    frontend menu section
-   Specify the full URL of the Zabbix web interface in the *Frontend
    URL* parameter.

![frontend\_url.png](../../../../assets/en/manual/appendix/install/frontend_url.png)

::: notetip
 Once the setup procedure is completed, you may want to
configure and send a [test report](/manual/config/reports#configuration)
to make sure everything works correctly. 
:::

[comment]: # ({/new-613109e8})

[comment]: # ({new-f2aa9786})
The official zabbix-web-service package is available in the [Zabbix
repository](http://repo.zabbix.com/). Google Chrome browser is not
included into these packages and has to be installed separately.

[comment]: # ({/new-f2aa9786})
