[comment]: # translation:outdated

[comment]: # ({new-6f1ad3f2})
# 8 Запуск агента из под root

Начиная с версии **5.0.0** файл сервиса systemd для Zabbix агента в
[официальных
пакетах](https://www.zabbix.com/ru/download?zabbix=5.0&os_distribution=red_hat_enterprise_linux&os_version=8&db=mysql)был
обновлен и теперь явно включает директивы для `User` и `Group`. Обе
директивы заданы значением `zabbix`.

Это означает, что старая функциональность настройки из под какого
пользователя Zabbix агент запускается через `zabbix_agentd.conf` файл
замещена и агент будет запускаться всегда из под пользователя, который
указывается в файле сервиса systemd.

Чтобы переопределить это новое поведение, создайте файл
`/etc/systemd/system/zabbix-agent.service.d/override.conf` со следующим
содержимым.

    [Service]
    User=root
    Group=root

Перезагрузите демонов и перезапустите сервис zabbix-agent.

    systemctl daemon-reload
    systemctl restart zabbix-agent

Для **агент2** такой подход полностью определяет пользователя, из под
которого он будет запускаться.

Для старого **агента** этот подход лишь включает повторно
функциональность настройки пользователя в файле `zabbix_agentd.conf`.
Поэтому, чтобы запустить Zabbix агента из под пользователя root, вам
также необходимо изменить[файл
конфигурации](/ru/manual/appendix/config/zabbix_agentd) агента и указать
опции `User=root`, как и `AllowRoot=1`.

[comment]: # ({/new-6f1ad3f2})

[comment]: # ({new-2f6d2fe8})

### Zabbix agent

To override the default user and group for Zabbix agent, run:

    systemctl edit zabbix-agent

Then, add the following content:

    [Service]
    User=root
    Group=root

Reload daemons and restart the zabbix-agent service:

    systemctl daemon-reload
    systemctl restart zabbix-agent

For **Zabbix agent** this re-enables the functionality of configuring user in the `zabbix_agentd.conf` file.
Now you need to set `User=root` and `AllowRoot=1` configuration parameters in the agent [configuration file](/manual/appendix/config/zabbix_agentd).

[comment]: # ({/new-2f6d2fe8})

[comment]: # ({new-f2ff86e2})

### Zabbix agent 2

To override the default user and group for Zabbix agent 2, run:

    systemctl edit zabbix-agent2

Then, add the following content:

    [Service]
    User=root
    Group=root

Reload daemons and restart the zabbix-agent service:

    systemctl daemon-reload
    systemctl restart zabbix-agent2

For **Zabbix agent2** this completely determines the user that it runs as.
No additional modifications are required.

[comment]: # ({/new-f2ff86e2})
