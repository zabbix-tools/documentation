[comment]: # translation:outdated

[comment]: # ({new-5407a56a})
# 3 Безопасные соединения с базой данных

[comment]: # ({/new-5407a56a})

[comment]: # ({new-7cadd09e})
#### Обзор

Можно настроить безопасные соединения TLS с базами данных MySQL и
PostgreSQL из:

-   Веб-интерфейса Zabbix
-   Zabbix сервера или прокси

[comment]: # ({/new-7cadd09e})

[comment]: # ({new-5af34b43})
#### Настройка веб-интерфейса

Опции для настройки безопасных соединений с базой данных становятся
доступными, когда флажок *TLS шифрование* отмечен на шаге [Настроить
соединение с БД](/manual/installation/install#step_3) при установке
веб-интерфейса Zabbix.

![](../../../../assets/en/manual/appendix/install/install_db_encrypt.png){width="600"}

|Параметр|<|Описание|
|----------------|-|----------------|
|*Шифрование TLS*|<|Установите этот флажок, чтобы активировать шифрование для соединений с базой данных Zabbix.<br>Даже если другие параметры не заполнены, соединения будут зашифрованы TLS, если этот флажок установлен.|
|*Параметры TLS*|<|<|
|<|*Файл TLS ключа*|Укажите полный путь к действительному файлу TLS ключа.|
|^|*Файл TLS сертификата*|Укажите полный путь к действительному файлу TLS сертификата.|
|^|//Файл TLS источника сертификатов //|Укажите полный путь к действительному файлу источника сертификации TLS.|
|^|*С проверкой хоста*|Установите этот флажок, чтобы активировать проверку узла сети.|
|^|*Список TLS шифров*|Укажите собственный список допустимых шифров. Формат списка шифров должен соответствовать стандарту OpenSSL.<br>Это поле доступно только для MySQL.|

::: noteimportant
Параметры TLS должны корректно указывать на файлы.
Если они указывают на несуществующие или недействительные файлы, будет
отображаться ошибка соединения. Если параметры TLS указывают на файлы,
которые открыты для записи, веб-интерфейс сгенерирует предупреждение в
отчете [Информация о
системе](/manual/web_interface/frontend_sections/reports/status_of_zabbix)
о том, что "Файлы сертификата TLS должны быть доступны только для
чтения." 
:::

[comment]: # ({/new-5af34b43})

[comment]: # ({new-9801ac5b})
##### Примеры использования

|Настройка|Результат|
|------------------|------------------|
|Нет (оставьте *Шифрование TLS* неотмеченным)|Подключение к базе данных без шифрования.|
|1\. Установите только флажок *Шифрование TLS*|Безопасное соединение TLS с базой данных.|
|1\. Установите флажок *Шифрование TLS*<br>2. Укажите файл источника TLS сертификации|Безопасное соединение TLS с базой данных;<br>Сертификат сервера базы данных проверен и подтвержден тем,<br>что он подписан доверенным центром.|
|1\. Установите флажок *Шифрование TLS*<br>2. Укажите файл источника TLS сертификации<br>3. Установите *С проверкой хоста*<br>4. Укажите список шифров TLS (необязательно)|Безопасное соединение TLS с базой данных;<br>Сертификат сервера базы данных проверяется путем сравнения имени узла сети,<br>указанного в сертификате, с именем хоста, к которому он подключен;<br>Подтверждено, что сертификат подписан доверенным центром.|
|1\. Установите флажок *Шифрование TLS*<br>2. Укажите файл TLS ключа<br>3. Укажите файл TLS сертификата<br>4. Укажите файл источника TLS сертификации<br>5. Установите *С проверкой хоста*<br>6. Укажите список шифров TLS (необязательно)|Защищенные TLS соединения с базой данных устанавливаются с максимальной безопасностью.<br>Требование к клиентской части представить свои сертификаты настраивается на стороне сервера.|

[comment]: # ({/new-9801ac5b})

[comment]: # ({new-84f30836})
#### Настройка Zabbix сервера/прокси

Защищенные соединения с базой данных можно настроить с помощью
соответствующих параметров в файле конфигурации Zabbix
[сервера](/ru/manual/appendix/config/zabbix_server) и/или
[прокси](/manual/appendix/config/zabbix_proxy).

|Настройка|Результат|
|------------------|------------------|
|Нет|Соединение с базой данных без шифрования.|
|1\. Установите DBTLSConnect=required|Сервер/прокси устанавливают TLS-соединение с базой данных. Незашифрованное соединение не допускается.|
|1\. Установите DBTLSConnect=verify\_ca<br>2. В DBTLSCAFile укажите файл центра сертификации TLS|Сервер/прокси устанавливает TLS-соединение с базой данных после проверки сертификата базы данных.|
|1\. Установите DBTLSConnect=verify\_full<br>2. В DBTLSCAFile укажите файл центра сертификации TLS|Сервер / прокси-сервер устанавливает TLS-соединение с базой данных после проверки сертификата базы данных и идентификации хоста базы данных.|
|1\. В DBTLSCAFile укажите файл центра сертификации TLS<br>2. В DBTLSCertFile укажите файл сертификата открытого ключа клиента<br>3. В DBTLSKeyFile указать файл закрытого ключа клиента|Сервер / прокси предоставляют клиентский сертификат при подключении к базе данных.|
|1\. В DBTLSCipher укажите список шифровальных шифров, которые клиент разрешает для соединений, использующих протоколы TLS до TLS 1.2<br><br>или в DBTLSCipher13 - список шифровальных шифров, которые клиент разрешает для соединений по протоколу TLS 1.3.|(MySQL) TLS-соединение осуществляется с использованием шифра из предоставленного списка.<br>(PostgreSQL) Установка этой опции будет считаться ошибкой.|

[comment]: # ({/new-84f30836})




[comment]: # ({new-b1071a8a})
##### Frontend to the database

A secure connection to the database can be configured during frontend
installation:

-   Mark the *Database TLS encryption* checkbox in the [Configure DB
    connection](/manual/installation/frontend#configure_db_connection)
    step to enable transport encryption.
-   Mark the *Verify database certificate* checkbox that appears when
    *TLS encryption* field is checked to enable encryption with
    certificates.

::: noteclassic
 For MySQL, the *Database TLS encryption* checkbox is
disabled, if *Database host* is set to localhost, because connection
that uses a socket file (on Unix) or shared memory (on Windows) cannot
be encrypted.\
For PostgreSQL, the *TLS encryption* checkbox is disabled, if the value
of the *Database host* field begins with a slash or the field is empty.

:::

The following parameters become available in the TLS encryption in
certificates mode (if both checkboxes are marked):

|Parameter|Description|
|---------|-----------|
|*Database TLS CA file*|Specify the full path to a valid TLS certificate authority (CA) file.|
|*Database TLS key file*|Specify the full path to a valid TLS key file.|
|*Database TLS certificate file*|Specify the full path to a valid TLS certificate file.|
|*Database host verification*|Mark this checkbox to activate host verification.<br>Disabled for MYSQL, because PHP MySQL library does not allow to skip the peer certificate validation step.|
|*Database TLS cipher list*|Specify a custom list of valid ciphers. The format of the cipher list must conform to the OpenSSL standard.<br>Available for MySQL only.|

::: noteimportant
TLS parameters must point to valid files. If they
point to non-existent or invalid files, it will lead to the
authorization error.\
If certificate files are writable, the frontend generates a warning in
the [System
information](/manual/web_interface/frontend_sections/reports/status_of_zabbix)
report that "TLS certificate files must be read-only." (displayed only
if the PHP user is the owner of the certificate).\
\
Certificates protected by passwords are not supported. 
:::

[comment]: # ({/new-b1071a8a})

[comment]: # ({new-d4fd87a2})
##### Use cases

Zabbix frontend uses GUI interface to define possible options: required,
verify\_ca, verify\_full. Specify required options in the installation
wizard step *Configure DB connections*. These options are mapped to the
configuration file (zabbix.conf.php) in the following manner:

|GUI settings|Configuration file|Description|Result|
|------------|------------------|-----------|------|
|![](../../../../assets/en/manual/appendix/install/encrypt_db_transport.png)|...<br>// Used for TLS connection.<br>$DB\['ENCRYPTION'\] = true;<br>$DB\['KEY\_FILE'\] = '';<br>$DB\['CERT\_FILE'\] = '';<br>$DB\['CA\_FILE'\] = '';<br>$DB\['VERIFY\_HOST'\] = false;<br>$DB\['CIPHER\_LIST'\] = '';<br>...|Check *Database TLS encryption*<br>Leave *Verify database certificate* unchecked|Enable 'required' mode.|
|![](../../../../assets/en/manual/appendix/install/encrypt_db_verify_ca.png)|...<br>$DB\['ENCRYPTION'\] = true;\\\\ $DB\['KEY\_FILE'\] = '';<br>$DB\['CERT\_FILE'\] = '';<br>$DB\['CA\_FILE'\] = '/etc/ssl/mysql/ca.pem';<br>$DB\['VERIFY\_HOST'\] = false;<br>$DB\['CIPHER\_LIST'\] = '';<br>...|1\. Check *Database TLS encryption* and *Verify database certificate*<br>2. Specify path to *Database TLS CA file*|Enable 'verify\_ca' mode.|
|![](../../../../assets/en/manual/appendix/install/encrypt_db_verify_full1.png)|...<br>// Used for TLS connection with strictly defined Cipher list.<br>$DB\['ENCRYPTION'\] = true;<br>$DB\['KEY\_FILE'\] = '<key\_file\_path>';<br>$DB\['CERT\_FILE'\] = '<key\_file\_path>';<br>$DB\['CA\_FILE'\] = '<key\_file\_path>';<br>$DB\['VERIFY\_HOST'\] = true;<br>$DB\['CIPHER\_LIST'\] = '<cipher\_list>';<br>...<br><br>Or:<br><br>...<br>// Used for TLS connection without Cipher list defined - selected by MySQL server<br>$DB\['ENCRYPTION'\] = true;<br>$DB\['KEY\_FILE'\] = '<key\_file\_path>';<br>$DB\['CERT\_FILE'\] = '<key\_file\_path>';<br>$DB\['CA\_FILE'\] = '<key\_file\_path>';<br>$DB\['VERIFY\_HOST'\] = true;<br>$DB\['CIPHER\_LIST'\] = '';<br>...|1\. Check *Database TLS encryption* and *Verify database certificate*<br>2. Specify path to *Database TLS key file*<br>3. Specify path to *Database TLS CA file*<br>4. Specify path to *Database TLS certificate file*<br>6. Specify TLS cipher list (optional)|Enable 'verify\_full' mode for MySQL.|
|![](../../../../assets/en/manual/appendix/install/encrypt_db_verify_full2.png)|...<br>$DB\['ENCRYPTION'\] = true;<br>$DB\['KEY\_FILE'\] = '<key\_file\_path>';<br>$DB\['CERT\_FILE'\] = '<key\_file\_path>';<br>$DB\['CA\_FILE'\] = '<key\_file\_path>';<br>$DB\['VERIFY\_HOST'\] = true;<br>$DB\['CIPHER\_LIST'\] = ' ';<br>...<br>|1\. Check *Database TLS encryption* and *Verify database certificate*<br>2. Specify path to *Database TLS key file*<br>3. Specify path to *Database TLS CA file*<br>4. Specify path to *Database TLS certificate file*<br>6. Check *Database host verification*|Enable 'verify\_full' mode for PostgreSQL.|

**See also:** [Encryption configuration examples for
MySQL](/manual/appendix/install/db_encrypt/mysql), [Encryption
configuration examples for
PostgreSQL](/manual/appendix/install/db_encrypt/postgres).

[comment]: # ({/new-d4fd87a2})

[comment]: # ({new-9cfb91d1})
#### Zabbix server/proxy configuration

Secure connections to the database can be configured with the respective
parameters in the Zabbix [server](/manual/appendix/config/zabbix_server)
and/or [proxy](/manual/appendix/config/zabbix_proxy) configuration file.

|Configuration|Result|
|-------------|------|
|None|Connection to the database without encryption.|
|1\. Set DBTLSConnect=required|Server/proxy make a TLS connection to the database. An unencrypted connection is not allowed.|
|1\. Set DBTLSConnect=verify\_ca<br>2. Set DBTLSCAFile - specify the TLS certificate authority file|Server/proxy make a TLS connection to the database after verifying the database certificate.|
|1\. Set DBTLSConnect=verify\_full<br>2. Set DBTLSCAFile - specify TLS certificate authority file|Server/proxy make a TLS connection to the database after verifying the database certificate and the database host identity.|
|1\. Set DBTLSCAFile - specify TLS certificate authority file<br>2. Set DBTLSCertFile - specify the client public key certificate file<br>3. Set DBTLSKeyFile - specify the client private key file|Server/proxy provide a client certificate while connecting to the database.|
|1\. Set DBTLSCipher - the list of encryption ciphers that the client permits for connections using TLS protocols up to TLS 1.2<br><br>or DBTLSCipher13 - the list of encryption ciphers that the client permits for connections using TLS 1.3 protocol|(MySQL) TLS connection is made using a cipher from the provided list.<br>(PostgreSQL) Setting this option will be considered as an error.|

[comment]: # ({/new-9cfb91d1})
