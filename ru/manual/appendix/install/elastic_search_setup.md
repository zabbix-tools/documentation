[comment]: # translation:outdated

[comment]: # ({new-4ff8e217})
# 5 Установка Elasticsearch

::: noteimportant
Поддержка Elasticsearch экспериментальная!

:::

Zabbix недавно начал поддерживать хранение данных истории в
Elasticsearch вместо базы данных. Теперь пользователям предоставляется
возможность выбора места хранения данных истории между совместимой базой
данных и Elasticsearch. Процедура установки, которая рассматривается в
этом разделе, применима к следующим версиям Elasticsearch: **7.x**. В
случае, если используется более старая или более новая версия
Elasticsearch, некоторый функционал может не работать должным образом.

::: notewarning
 Если все данные истории хранятся в Elasticsearch,
тренды не рассчитываются и не сохраняются в базе данных. Если тренды не
рассчитываются и не сохраняются, период хранения истории может
потребоваться продлить.
:::

[comment]: # ({/new-4ff8e217})

[comment]: # ({new-0b6587bd})
#### Настройка

Для обеспечения надлежащей связи между всеми участвующими элементами
убедитесь, что параметры файла конфигурации сервера и файл конфигурации
веб-интерфейса настроены должным образом.

[comment]: # ({/new-0b6587bd})

[comment]: # ({new-293ef64c})
##### Zabbix сервер и веб-интерфейс

Выдержка из файла конфигурации Zabbix сервера с параметрами, которые
необходимо обновить:

    ### Option: HistoryStorageURL
    # History storage HTTP[S] URL.
    #
    # Mandatory: no
    # Default:
    # HistoryStorageURL= 
    ### Option: HistoryStorageTypes
    # Comma separated list of value types to be sent to the history storage.
    #
    # Mandatory: no
    # Default:
    # HistoryStorageTypes=uint,dbl,str,log,text

Пример значений параметров, которые требуется указать в файле
конфигурации Zabbix сервера:

    HistoryStorageURL=http://test.elasticsearch.lan:9200
    HistoryStorageTypes=str,log,text

Подобная конфигурация заставит Zabbix сервер хранить значения истории
числовых типов в соответствующей базе данных и текстовых данных истории
в Elasticsearch.

Elasticsearch поддерживает следующие типы элементов данных:

    uint,dbl,str,log,text

Разъяснение поддерживаемых типов элементов данных:

|   |   |   |
|---|---|---|
|**Тип значения элемента данных**|**Таблица в базе данных**|**Тип в Elasticsearch**|
|Числовой (целое положительное)|history\_uint|uint|
|Числовой (с плавающей точкой)|history|dbl|
|Символ|history\_str|str|
|Журнал (лог)|history\_log|log|
|Текст|history\_text|text|

Выдержка из файла конфигурации Zabbix веб-интерфейса
(`conf/zabbix.conf.php`) с параметрами, которые необходимо обновить:

    // Elasticsearch url (can be string if same url is used for all types).
    $HISTORY['url']   = [
          'uint' => 'http://localhost:9200',
          'text' => 'http://localhost:9200'
    ];
    // Value types stored in Elasticsearch.
    $HISTORY['types'] = ['uint', 'text'];

Пример значений параметров, которые требуется указать в файле
конфигурации Zabbix веб-интерфейса:

    $HISTORY['url']   = 'http://test.elasticsearch.lan:9200';
    $HISTORY['types'] = ['str', 'text', 'log'];

Подобная конфигурация заставит хранить значения истории `Текст`,
`Символ` и `Журнал (лог)` в Elasticsearch.

Также требуется сделать $HISTORY глобальной переменной в
`conf/zabbix.conf.php`, чтобы убедиться, что все работает должным
образом (смотрите файл `conf/zabbix.conf.php.example` как это сделать):

    // Zabbix GUI configuration file.
    global $DB, $HISTORY;

[comment]: # ({/new-293ef64c})

[comment]: # ({new-e9a9c5bd})
##### Установка Elasticsearch и создание сопоставления

Последними двумя шагами, чтобы все заработало, нужно установить сам
Elasticsearch и создать процесс сопоставления.

Чтобы установить Elasticsearch, пожалуйста, обратитесь к [инструкции по
установке
Elasticsearch](https://www.elastic.co/guide/en/elasticsearch/reference/current/setup.html)
\[en\].

::: noteclassic
Сопоставление является структурой данных в Elasticsearch
(аналогично таблице в базе данных). Сопоставления по всем типам данных
истории доступны здесь:
`database/elasticsearch/elasticsearch.map`.
:::

::: notewarning
Создание сопоставления обязательно. Некоторый
функционал будет поломан, если сопоставление не создано согласно этой
инструкции.
:::

Для создания сопоставления для типа `text` отправьте следующий запрос в
Elasticsearch:

``` {.java}
curl -X PUT \
 http://your-elasticsearch.here:9200/text \
 -H 'content-type:application/json' \
 -d '{
   "settings": {
      "index": {
         "number_of_replicas": 1,
         "number_of_shards": 5
      }
   },
   "mappings": {
      "properties": {
         "itemid": {
            "type": "long"
         },
         "clock": {
            "format": "epoch_second",
            "type": "date"
         },
         "value": {
            "fields": {
               "analyzed": {
                  "index": true,
                  "type": "text",
                  "analyzer": "standard"
               }
            },
            "index": false,
            "type": "text"
         }
      }
   }
}'
```

Похожий запрос требуется выполнить для создания сопоставлений по
значениям истории `Символ` и `Журнал (лог)` с соответствующим
исправлением типа.

::: noteclassic
Чтобы работать с Elasticsearch, пожалуйста, обратитесь к
[странице требований](/ru/manual/installation/requirements#сервер) для
получения более подробной информации.
:::

::: noteclassic
[Очистка истории](/ru/manual/installation/requirements) не
удаляет никакие данные из Elasticsearch.
:::

[comment]: # ({/new-e9a9c5bd})

[comment]: # ({new-8451a0a2})
#### Запись данных истории в несколько индексов на основе даты

В этом разделе описываются дополнительные шаги, которые необходимы для
работы с конвейерами и узлами ingest.

Для начала вам нужно создать шаблоны для индексов. Следующий пример
показывает запрос на создание uint шаблона:

``` {.java}
curl -X PUT \
 http://your-elasticsearch.here:9200/_template/uint_template \
 -H 'content-type:application/json' \
 -d '{
   "index_patterns": [
      "uint*"
   ],
   "settings": {
      "index": {
         "number_of_replicas": 1,
         "number_of_shards": 5
      }
   },
   "mappings": {
      "properties": {
         "itemid": {
            "type": "long"
         },
         "clock": {
            "format": "epoch_second",
            "type": "date"
         },
         "value": {
            "type": "long"
         }
      }
   }
}'
```

Для создания остальных шаблонов пользователю нужно изменить URL
(последняя часть имени шаблона), изменить поля "template" и
"index\_patterns", чтобы имя индекса совпадало, и указать корректное
сопоставление, которое можно взять с
`database/elasticsearch/elasticsearch.map`. Например, можно использовать
следующую команду, чтобы создать шаблон для text индекса:

``` {.java}
curl -X PUT \
 http://your-elasticsearch.here:9200/_template/text_template \
 -H 'content-type:application/json' \
 -d '{
   "index_patterns": [
      "text*"
   ],
   "settings": {
      "index": {
         "number_of_replicas": 1,
         "number_of_shards": 5
      }
   },
   "mappings": {
      "properties": {
         "itemid": {
            "type": "long"
         },
         "clock": {
            "format": "epoch_second",
            "type": "date"
         },
         "value": {
            "fields": {
               "analyzed": {
                  "index": true,
                  "type": "text",
                  "analyzer": "standard"
               }
            },
            "index": false,
            "type": "text"
         }
      }
   }
}'
```

Этот шаг необходим, чтобы позволить Elasticsearch задавать корректные
сопоставления для индексов, которые создаются автоматически. Затем это
требуется для создания определения конвейера. Конвейером является своего
рода предварительная обработка данных перед тем как поместить их в
индексы. Можно использовать следующую команду, чтобы создать конвейер
для uint индекса:

``` {.java}
curl -X PUT \
 http://your-elasticsearch.here:9200/_ingest/pipeline/uint-pipeline \
 -H 'content-type:application/json' \
 -d '{
   "description": "daily uint index naming",
   "processors": [
      {
         "date_index_name": {
            "field": "clock",
            "date_formats": [
               "UNIX"
            ],
            "index_name_prefix": "uint-",
            "date_rounding": "d"
         }
      }
   ]
}'
```

Пользователь может изменить параметр округления ("date\_rounding"),
чтобы задать спефицичный период ротации индекса. Для создания остальных
конвейером пользователю необходимо изменить URL (последняя часть имени
конвейера) и изменить поле "index\_name\_prefix", чтобы оно
соответствовало имени индекса.

Смотрите также документацию
[Elasticsearch](https://www.elastic.co/guide/en/elasticsearch/reference/master/date-index-name-processor.html)
\[en\].

Кроме того, запись данных истории в несколько индексов на основе даты
необходимо также включить в новом параметре в конфигурации Zabbix
сервера:

    ### Option: HistoryStorageDateIndex
    # Enable preprocessing of history values in history storage to store values in different indices based on date.
    # 0 - disable
    # 1 - enable
    #
    # Mandatory: no
    # Default:
    # HistoryStorageDateIndex=0

[comment]: # ({/new-8451a0a2})

[comment]: # ({new-5c4a5e3c})
#### Решение проблем

Следующие шаги могут помочь вам в решении проблем с установкой
Elasticsearch:

1.  Проверьте, что сопоставления корректны (требуется запрос GET к URL
    индекса, примерно такой: `http://localhost:9200/uint`).
2.  Проверьте, что шарды не находятся в состоянии ошибки (должен помочь
    перезапуск Elasticsearch).
3.  Проверьте конфигурацию Elasticsearch. Конфигурация должна разрешать
    доступ с хоста Zabbix веб-интерфейса и хоста Zabbix сервера.
4.  Проверьте журналы Elasticsearch.

Если у вас по-прежнему возникают проблемы с вашей инсталляцией,
пожалуйста, создайте отчет об ошибке со всей информацией из этого списка
(сопоставления, журналы ошибок, настройка, версия и так далее)

[comment]: # ({/new-5c4a5e3c})
