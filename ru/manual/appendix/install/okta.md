[comment]: # translation:outdated

[comment]: # ({new-e09cf279})
# 10 Настройка SAML с Okta

В этом разделе описывается, как настроить Okta для включения
аутентификации SAML 2.0 для Zabbix.

[comment]: # ({/new-e09cf279})

[comment]: # ({new-c8df5f13})
#### Настройка Okta

1\. Перейдите на <https://okta.com> и зарегистрируйтесь или войдите в
свою учетную запись.

2\. В веб-интерфейсе Okta перейдите в *Приложения → Приложения* и
нажмите кнопку "Добавить приложение".
(![](../../../../assets/en/manual/appendix/install/okta_add_app.png)).

3\. Нажмите кнопку "Создать новое
приложение".(![](../../../../assets/en/manual/appendix/install/okta_create_app.png)).
Во всплывающем окне выберите *Платформа:* Веб, *Метод входа:* SAML 2.0 и
нажмите кнопку "Создать".

![](../../../../assets/en/manual/appendix/install/okta_app1.png){width="600"}

4\. Заполните поля на вкладке *Общие настройки* (первая открывшаяся
вкладка) в соответствии с вашими предпочтениями и нажмите "Далее".

5\. На вкладке *Настроить SAML* введите значения, указанные ниже, затем
нажмите «Далее».

-   В разделе **ОБЩИЕ**:
    -   *URL-адрес для единого входа*:
        https://<your-zabbix-url>/ui/index\_sso.php?acs\
        Флажок *Использовать для URL-адреса получателя...* должен быть
        отмечен.
    -   *Код URI аудитории (идентификатор сущности SP)*: zabbix\
        Обратите внимание, что это значение будет использоваться в
        утверждении SAML как уникальный идентификатор поставщика услуг
        (если не совпадает, операция будет отклонена). В этом поле можно
        указать URL-адрес или любую строку данных.
    -   *Cостояние реле по умолчанию*:\
        Оставьте это поле пустым; если требуется настраиваемый редирект,
        его можно добавить в Zabbix в настройках *Администрирование →
        Пользователи*.
    -   Заполните остальные поля в соответствии с вашими предпочтениями.

![](../../../../assets/en/manual/appendix/install/okta_app2.png)

::: noteclassic
Если вы планируете использовать зашифрованное соединение,
сгенерируйте частный и публичный сертификаты шифрования, а затем
загрузите публичный сертификат в Okta. Форма загрузки сертификата
появляется, когда *Шифрование утверждения* установлено на Зашифрованное
(нажмите *Показать дополнительные настройки*, чтобы найти этот
параметр). 
:::

-   В разделе **Операторы атрибутов** добавьте выражение с параметрами:
    -   *Name:* usrEmail
    -   *Name format:* Unspecified
    -   *Value:* user.email

![](../../../../assets/en/manual/appendix/install/okta_app3.png)

6\. На следующей вкладке выберите "Я поставщик программного обеспечения.
Я хочу интегрировать свое приложение с Okta" и нажмите "Готово".

7\. Теперь перейдите на вкладку *Пользователи* и нажмите кнопку
"Назначить", затем выберите *Назначить людям* из раскрывающегося списка.
![](../../../../assets/en/manual/appendix/install/okta_assign.png)

8\. Во всплывающем окне назначьте созданное приложение людям, которые
будут использовать SAML 2.0 для аутентификации в Zabbix, затем нажмите
"Сохранить и вернуться".

9\. Перейдите в раздел "Единый вход" → "Просмотр инструкций по
настройке". Инструкции по установке будут отображены в новой вкладке;
держите эту вкладку открытой при настройке Zabbix.

![](../../../../assets/en/manual/appendix/install/okta_setup.png)

[comment]: # ({/new-c8df5f13})

[comment]: # ({new-bdad4ea8})
#### Настойка Zabbix

1\. В Zabbix, перейдите в настройки SAML в разделе *Администрирование →
Аутентификация* и скопируйте информацию из инструкций по настройке Okta
в соответствующие поля:

-   Identity Provider Single Sign-On URL → SSO service URL
-   Identity Provider Issuer → IdP entity ID
-   Username attribute → Attribute name (usrEmail)
-   SP entity ID → Audience URI

2\. Загрузите сертификат, указанный на странице инструкций по настройке
Okta, в папку *ui/conf/certs* как idp.crt и установите разрешение 644,
выполнив:

    chmod 644 idp.crt

Обратите внимание, что если вы обновились до Zabbix 5.0 из более старой
версии, вам также необходимо вручную добавить эти строки в файл
zabbix.conf.php (расположенный в каталоге *ui/conf*/):

    // Used for SAML authentication.
    $SSO['SP_KEY'] = 'conf/certs/sp.key'; // Path to your private key.
    $SSO['SP_CERT'] = 'conf/certs/sp.crt'; // Path to your public key.
    $SSO['IDP_CERT'] = 'conf/certs/idp.crt'; // Path to IdP public key.
    $SSO['SETTINGS'] = []; // Additional settings

См. общие инструкции [Аутентификация
SAML](/ru/manual/web_interface/frontend_sections/Administration/authentication#saml_authentication)
для получения дополнительных сведений. 3. Если *Шифрование утверждений*
было установлено на Шифрование в Okta, флажок «Утверждения» параметра
*Шифрование* также должен быть отмечен в Zabbix.

![](../../../../assets/en/manual/appendix/install/okta_zabbix.png)

4\. Нажмите кнопку "Обновить", чтобы сохранить эти настройки.

::: noteclassic
 Для входа с помощью SAML псевдоним пользователя в Zabbix
должен соответствовать его электронной почте Okta. Эти настройки можно
изменить в разделе *Администрирование → Пользователи* веб-интерфейса
Zabbix. 
:::

[comment]: # ({/new-bdad4ea8})

[comment]: # ({new-ff17ddca})

#### SCIM provisioning

1\. To turn on SCIM provisioning, go to "General" -> "App Settings" of the application in Okta. 

Mark the *Enable SCIM provisioning* checkbox. As a result, a new *Provisioning* tab appears.

2\. Go to the "Provisioning" tab to set up a SCIM connection:

-   In *SCIM connector base URL* specify the path to the Zabbix frontend and append `api_scim.php` to it, i.e.:\
    `https://<your-zabbix-url>/zabbix/api_scim.php`
-   *Unique identifier field for users*: `email`
-   *Authentication mode*: `HTTP header`
-   In *Authorization* enter a valid API token with Super admin rights

![](../../../../assets/en/manual/appendix/install/okta_scim_conn.png){width="600"}

::: noteimportant
If you are using Apache, you may need to change the default Apache configuration in `/etc/apache2/apache2.conf` by adding the following line:

    SetEnvIf Authorization "(.*)" HTTP_AUTHORIZATION=$1

Otherwise Apache does not send the Authorization header in request.
:::

3\. Click on *Test Connector Configuration* to test the connection. If all is correct a success message will be displayed.

4\. In "Provisioning" -> "To App", make sure to mark the following checkboxes:

-    Create Users
-    Update User Attributes
-    Deactivate Users

This will make sure that these request types will be sent to Zabbix.

5\. Make sure that all attributes defined in SAML are defined in SCIM. You can access the profile editor for your app in "Provisioning" -> "To App", by clicking on *Go to Profile Editor*.

Click on *Add Attribute*. Fill the values for *Display name*, *Variable name*, *External name* with the SAML attribute name, for example, `user_name`.

![](../../../../assets/en/manual/appendix/install/okta_add_attr.png)

*Extenal namespace* should be the same as user schema: `urn:ietf:params:scim:schemas:core:2.0:User`

6\. Go to "Provisioning" -> "To App" -> "Attribute Mappings" of your application. Click on *Show Unmapped Attributes* at the bottom. Newly added attributes appear.

7\. Map each added attribute.

![](../../../../assets/en/manual/appendix/install/okta_map_attr.png)

8\. Add users in the "Assignments" tab. The users previously need to be added in *Directory* -> *People*. All these assignments will be sent as requests to Zabbix.

9\. Add groups in the "Push Groups" tab. The user group mapping pattern in Zabbix SAML settings must match a group specified here. If there is no match, the user cannot be created in Zabbix.

Information about group members is sent every time when some change is made.

[comment]: # ({/new-ff17ddca})
