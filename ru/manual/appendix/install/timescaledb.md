[comment]: # translation:outdated

[comment]: # ({new-98c082ce})
# 4 Настройка TimescaleDB setup

[comment]: # ({/new-98c082ce})

[comment]: # ({new-acb21280})
#### Обзор

Zabbix поддерживает TimescaleDB, решения на основе PostgreSQL базы
данных для автоматического партиционирования данных на части на основе
времени для поддержки большего уровня производительности.

::: notewarning
В настоящее время TimescaleDB не поддерживается
Zabbix прокси.
:::

В этом разделе представлены необходимые шаги для миграции с существующих
таблиц в PostgreSQL на TimescaleDB.

[comment]: # ({/new-acb21280})

[comment]: # ({new-f731725c})
#### Настройка

Мы предполагаем, что TimescaleDB расширение уже установлено в базе
данных (смотрите [инструкции по
инсталляции](https://docs.timescale.com/latest/getting-started/installation)).

TimescaleDB расширение также необходимо включить для отдельной БД,
выполнив:

    echo "CREATE EXTENSION IF NOT EXISTS timescaledb CASCADE;" | sudo -u postgres psql zabbix

Для выполнения этой команды потребуются привилегии администратора базы
данных.

::: noteclassic
 Если вы используете схему базы данных, отличную от
'public', вам необходимо добавить SCHEMA к приведенной выше команде.
E.g.:\
`echo "CREATE EXTENSION IF NOT EXISTS timescaledb SCHEMA yourschema CASCADE;" | sudo -u postgres psql zabbix`
:::

Затем выполните `timescaledb.sql` скрипт, который находится в
database/postgresql. Для новых установок скрипт должен запускаться после
того, как обычная база данных PostgreSQL была создана с исходной
схемой/данными (см. [создание базы
данных](/ru/manual/appendix/install/db_scripts)):

    cat timescaledb.sql | sudo -u zabbix psql zabbix

Миграция существующих данных истории и динамики изменений потребует
много времени. Zabbix сервер и веб-интерфейс должны быть остановлены в
процессе миграции.

Скрипт `timescaledb.sql` задает следующие параметры процессу очистки
истории (с *Администрирование* → *Общие* → *Очистка истории*):

-   Переопределить период хранения истории элементов данных
-   Переопределить период хранения динамики изменения элементов данных

В случае использования очистки истории с партиционированием для истории
и динамики изменений обе эти опции необходимо активировать. Имеется
возможность использования TimescaleDB партиционирования только для
динамики изменений (установив настройку *Переопределить период хранения
динамики изменения элементов данных*) или только для истории
(*Переопределить период хранения истории элементов данных*).

Для PostgreSQL версии 10.2 или выше и TimescaleDB версии 1.5 или выше
скрипт timescaledb.sql устанавливает два дополнительных параметра:

-   Активировать сжатие
-   Сжимать записи старше 7 дней

Все эти параметры можно изменить в *Администрирование*→*Общие*→*Очистка
истории* после установки.

::: notetip
Вы можете запустить инструмент timescaledb-tune,
предоставляемый TimescaleDB, чтобы оптимизировать параметры конфигурации
PostgreSQL в вашем '' postgresql.conf ''.
:::

[comment]: # ({/new-f731725c})

[comment]: # ({new-60e46db2})

Compression can be used only if both *Override item history period*
and *Override item trend period* options are enabled.

[comment]: # ({/new-60e46db2})

[comment]: # ({new-8c3a80a9})

All of these parameters can be changed in *Administration* →
*Housekeeping* after the installation.

::: notetip
You may want to run the timescaledb-tune tool provided
by TimescaleDB to optimize PostgreSQL configuration parameters in your
`postgresql.conf`.
:::

[comment]: # ({/new-8c3a80a9})

[comment]: # ({new-2012f0a6})
#### Сжатие TimescaleDB

Собственное сжатие TimescaleDB поддерживается начиная с Zabbix 5.0 для
PostgreSQL версии 10.2 или выше и TimescaleDB версии 1.5 или выше для
всех таблиц Zabbix, которыми управляет TimescaleDB. Во время обновления
или миграции на TimescaleDB первоначальное сжатие больших таблиц может
занять много времени.

::: notetip
Пользователям рекомендуется ознакомиться с документацией
по сжатию
[TimescaleDB](https://docs.timescale.com/latest/using-timescaledb/compression)
перед использованием сжатия.
:::

Обратите внимание, что на сжатие накладываются определенные ограничения,
а именно:

-   Изменения сжатых фрагментов (вставки, удаления, обновления) не
    допускаются.
-   Изменения схемы для сжатых таблиц не допускаются.

Настройки сжатия можно изменить в блоке *Сжатие истории и трендов* в
разделе *Администрирование* → *Общие* → *Очистка истории* веб-интерфейса
Zabbix.

|Параметр|По умолчанию|Комментарии|
|----------------|-----------------------|----------------------|
|*Включить сжатие*|Включено|Установка или снятие флажка не активирует/деактивирует сжатие немедленно. Так как сжатием занимается процесс очистки истории (housekeeper), изменения вступят в силу в течение максимум двух периодов времени, заданных в параметре `HousekeepingFrequency` (в файле конфигурации сервера [zabbix\_server.conf](ru/manual/appendix/config/zabbix_server))<br><br>После отключения сжатия новые фрагменты, попавшие в период сжатия, не будут сжиматься. Однако все ранее сжатые данные останутся сжатыми. Чтобы распаковать ранее сжатые фрагменты, следуйте инструкциям в документации [TimescaleDB](https://docs.timescale.com/latest/using-timescaledb/compression) \[en\].<br><br>При обновлении старых версий Zabbix с поддержкой TimescaleDB сжатие не будет включено по умолчанию.|
|*Compress records older than* (*Сжимать записи старше, чем*)|7d|Этот параметр не может быть меньше 7 дней.<br><br>Из-за неизменности сжатых фрагментов все запоздавшие данные (например, данные, задержанные прокси), которые старше этого значения, будут отброшены.|

[comment]: # ({/new-2012f0a6})
