[comment]: # translation:outdated

[comment]: # ({baafd79f-609a8e9d})
# 2 Восстановление набора символов и сопоставления базы данных Zabbix

[comment]: # ({/baafd79f-609a8e9d})

[comment]: # ({new-25eef83a})
#### MySQL/MariaDB

**1.** Проверьте набор символов базы данных и сопоставление.

Например:

    mysql> SELECT @@character_set_database, @@collation_database;
    +--------------------------+----------------------+
    | @@character_set_database | @@collation_database |
    +--------------------------+----------------------+
    | latin2                   | latin2 _general_ci   |
    +--------------------------+----------------------+

Как мы видим, набор символов в выводе - не 'utf8', а сопоставление -не 'utf8\_bin',
поэтому нам нужно их исправить.

**2.** Остановите Zabbix.

**3.** Создайте резервную копию базы данных!

**4.** Исправьте набор символов и сопоставление на уровне базы данных:

    alter database <your DB name> character set utf8 collate utf8_bin;

Исправленные значения:

    mysql> SELECT @@character_set_database, @@collation_database;
    +--------------------------+----------------------+
    | @@character_set_database | @@collation_database |
    +--------------------------+----------------------+
    | utf8                     | utf8_bin             |
    +--------------------------+----------------------+ 

**5.** Загрузите [скрипт](https://support.zabbix.com/secure/attachment/113858/113858_utf8_convert.sql), чтобы исправить набор символов и сопоставление на уровне таблицы и столбца:

    mysql <your DB name> < utf8_convert.sql

**6.** Выполните скрипт:

                   SET @ZABBIX_DATABASE = '<your DB name>';
    If MariaDB →  set innodb_strict_mode = OFF;        
                   CALL zbx_convert_utf8();
    If MariaDB →  set innodb_strict_mode = ON;   
                   drop procedure zbx_convert_utf8;

Обратите внимание, что кодировка данных на диске будет изменена. Например, при преобразовании таких символов, как Æ, Ñ, Ö из 'latin1' в 'utf8' они перейдут с 1 байта на 2 байта. Таким образом, для исправленной базы данных может потребоваться больше места, чем раньше.

**7.** Если ошибок нет - вы можете создать резервную копию базы данных с исправленной базой данных.

**8.** Запустите Zabbix.

[comment]: # ({/new-25eef83a})
