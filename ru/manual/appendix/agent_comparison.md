[comment]: # translation:outdated

[comment]: # ({new-28165f87})
# 18 Отличия между Zabbix агентом и Zabbix агентом 2

В этом разделе описываются различия между Zabbix агентом и Zabbix
агентом 2.

|Параметр|Zabbix агент|Zabbix агент 2|
|----------------|-----------------|-------------------|
|Язык программирования|C|Go с некоторыми частями в C|
|Демон|X|\-|
|Возможности расширения|Пользовательские [загружаемые модули](/ru/manual/config/items/loadablemodules) на языке C.|Пользовательские [плагины](/ru/manual/config/items/plugins) на языке Go.|
|Требования|<|<|
|Поддерживаемые платформы|Linux, IBM AIX, FreeBSD, NetBSD, OpenBSD, HP-UX, Mac OS X, Solaris: 9, 10, 11, Windows: все настольные и серверные версии, начиная с XP|Linux, Windows: все настольные и серверные версии, начиная с XP.|
|Поддерживаемые крипто библиотеки|GnuTLS 3.1.18 и выше<br>OpenSSL 1.0.1, 1.0.2, 1.1.0, 1.1.1<br>LibreSSL - протестировано с версиями 2.7.4, 2.8.2 (применяются определенные ограничения, см. страницу [Шифрование](/ru/manual/encryption#compiling_zabbix_with_encryption_support) для информации).|Linux: OpenSSL 1.0.1 и выше поддерживается с Zabbix 4.4.8.<br>MS Windows: OpenSSL 1.1.1 и выше.<br>В библиотеке OpenSSL должна быть включена поддержка PSK. LibreSSL не поддерживается.|
|Процессы мониторинга|<|<|
|Процессы|Для каждой записи сервера/прокси отдельный активный процесс проверки.|Единый процесс с автоматически созданными потоками.<br>Максимальное количество потоков определяется переменной среды GOMAXPROCS.|
|Метрики|**UNIX**: см. список поддерживаемых [элементов данных](/ru/manual/config/items/itemtypes/zabbix_agent).<br><br>**Windows**: см. список дополнительных элементов данных, специфичных для Windows [элементов данных](/ru/manual/config/items/itemtypes/zabbix_agent/win_keys).|**UNIX**: все метрики, поддерживаемые Zabbix агентом.<br>Кроме того, агент 2 предоставляет родное решение Zabbix для мониторинга: Docker, Memcached, MySQL, PostgreSQL, Redis, systemd (см. список [элементов данных](/ru/manual/config/items/itemtypes/zabbix_agent/zabbix_agent2), специфичных для агента 2).<br><br>**Windows**: все метрики, поддерживаемые Zabbix агентом, а также (с Zabbix 5.0.3) net.tcp.service\* проверки HTTPS, LDAP.|
|Согласованность|Проверки выполняются последовательно.|Проверки из разных плагинов или множественные проверки в одном плагине могут выполняться одновременно.|
|Пользовательские интервалы|Поддерживаются только для пассивных проверок.|Поддерживаются для пассивных и активных проверок.|
|Сторонние трапы|\-|X|
|Другие функции|<|<|
|Постоянная память|\-|X|
|Настройки таймаута|Задается на уровне агента.|Таймаут плагина может переопределить таймаут, заданный на уровне агента.|
|Отбросить пользовательские привилегии|X (только Unix)|\-|
|Настраиваемые пользователем шифры|X|\-|

**См. также:**

-   *Описание процессов*: [Zabbix agent](/ru/manual/concepts/agent),
    [Zabbix agent 2](/ru/manual/concepts/agent2)
-   *Параметры настройки*: Zabbix агент
    [UNIX](/ru/manual/appendix/config/zabbix_agentd) /
    [Windows](/ru/manual/appendix/config/zabbix_agentd_win), Zabbix
    агент 2 [UNIX](/ru/manual/appendix/config/zabbix_agent2) /
    [Windows](/ru/manual/appendix/config/zabbix_agent2_win)

[comment]: # ({/new-28165f87})
