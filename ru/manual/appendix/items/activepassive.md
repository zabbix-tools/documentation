[comment]: # translation:outdated

[comment]: # ({new-a6d31bd3})
# 3 Пассивные и активные проверки агента

[comment]: # ({/new-a6d31bd3})

[comment]: # ({d4a6d70c-7bd0dd61})
#### Обзор

Этот раздел детальн описывает пассивные и активные проверки, которые выполняются [Zabbix агентом](/manual/config/items/itemtypes/zabbix_agent).

Zabbix использует протокол на основе JSON для взаимодействия с Zabbix агентом.

[comment]: # ({/d4a6d70c-7bd0dd61})

[comment]: # ({cb0dbe48-e6db8dd6})
#### Пассивные проверки

Пассивная проверка это простой запрос данных. Zabbix сервер или прокси запрашивает какие-либо данныеor proxy (например, загрузку CPU) и Zabbix агент отправляет результат обратно серверу.

**Запрос сервера**

Для получения более подробных сведений об определении длины заголовка и данных, пожалуйста, обратитесь к [деталям протокола](/manual/appendix/protocols/header_datalen).

    <ключ элемента данных>

**Ответ агента**

    <ДАННЫЕ>[\0<ОШИБКА>]

Часть в квадратных скобках выше опциональна и отправляется только по неподдерживаемым элементам данных.

Например, для поддерживаемых элементов данных:

1.  Сервер открывает TCP соединение
2.  Сервер отправляет **<ЗАГОЛОВОК><ДЛИНАДАННЫХ>agent.ping**
3.  Агент читает запрос и отвечает с **<ЗАГОЛОВОК><ДЛИНАДАННЫХ>1**
4.  Сервер обрабатывает данные, чтобы извлечь значение, в нашем случае '1'
5.  TCP соединение закрывается

Для неподдерживаемых элементов данных:

1.  Сервер открывает TCP соединение
2.  Сервер отправляет **<ЗАГОЛОВОК><ДЛИНАДАННЫХ>vfs.fs.size\[/nono\]**
3.  Агент читает запрос и отвечает с **<ЗАГОЛОВОК><ДЛИНАДАННЫХ>ZBX\_NOTSUPPORTED\\0Cannot obtain filesystem information: \[2\] No such file or directory**
4.  Сервер обрабатывает данные, меняет состояние элемента данных на неподдерживаемое с приведённым сообщением об ошибке
5.  TCP соединение закрывается

[comment]: # ({/cb0dbe48-e6db8dd6})

[comment]: # ({795db5ff-ecdd6e8e})
#### Активные проверки

Активные проверки требуют более сложной обработки. Агент сначала должен получить с сервера список элементов данных для независимой обработки.

Сервера, для получения активных проверок, перечислены в параметре 'ServerActive'  [файла конфигурации](/manual/appendix/config/zabbix_agentd) агента. Частота запросов данных проверок настраивается параметром 'RefreshActiveChecks' в этом же файле конфигурации. Однако, если обновление активных проверок завершится с ошибкой, запрос повторится через, жестко заданные в коде, 60 секунд.

Затем агент периодически отправляет новые значения на сервер(а).

::: notetip
Если агент находится за брандмауэром, вы можете рассмотреть возможность использования исключительно Активных проверок, т.к. в этом случае, вам не понадобится менять настройки брандмауэра для разрешения начальных входящих соединений
:::

[comment]: # ({/795db5ff-ecdd6e8e})

[comment]: # ({new-4c14cd28})
::: notetip
In order to decrease network traffic and resources usage Zabbix server or Zabbix proxy will provide configuration only if Zabbix agent still hasn't received configuration or if something has changed in host configuration, global macros or global regular expressions.
:::

[comment]: # ({/new-4c14cd28})

[comment]: # ({new-2f80420a})
The agent then periodically sends the new values to the server(s).

::: notetip
If an agent is behind the firewall you might consider
using only Active checks because in this case you wouldn't need to
modify the firewall to allow initial incoming connections.
:::

[comment]: # ({/new-2f80420a})

[comment]: # ({d5566876-d898e135})
##### Получение списка элементов данных

**Запрос агента**

``` {.javascript}
{
    "request":"active checks",
    "host":"<hostname>"
}
```

**Ответ сервера**

``` {.javascript}
{
    "response":"success",
    "data":[
        {
            "key":"log[/home/zabbix/logs/zabbix_agentd.log]",
            "delay":30,
            "lastlogsize":0,
            "mtime":0
        },
        {
            "key":"agent.version",
            "delay":600,
            "lastlogsize":0,
            "mtime":0
        },
        {
            "key":"vfs.fs.size[/nono]",
            "delay":600,
            "lastlogsize":0,
            "mtime":0
        }
    ]
}
```

Ответ сервера должен быть успешным. У каждого полученного элемента данных обязательно должны быть указаны свойства **key**, **delay**, **lastlogsize** и **mtime**, независимо от того является элемент данных журнальным или нет.

Например:

1.  Агент открывает TCP соединение
2.  Агент запрашивает список проверок
3.  Сервер отвечает списком элементов данных (ключ элемента данных, интервал обновления)
4.  Агент анализирует ответ
5.  TCP соединение закрывается
6.  Агент начинает периодический сбор данных

::: noteimportant
 Возьмите на заметку, что (чувствительные) данные конфигурации могут стать доступными лицам, имеющим доступ к порту траппера Zabbix сервера, при использовании активных проверок. Это возможно так как любой может представиться активным агентом и запросить данные конфигурации элементов данных; аутентификация не производится, если вы не используете опции [шифрования](/manual/encryption).
:::

[comment]: # ({/d5566876-d898e135})

[comment]: # ({dd14368c-8c5ecfe1})
##### Отправка собранных данных

**Агент отправляет**

``` {.javascript}
{
    "request":"agent data",
    "session": "12345678901234567890123456789012",
    "data":[
        {
            "host":"<hostname>",
            "key":"agent.version",
            "value":"2.4.0",
            "id": 1,
            "clock":1400675595,            
            "ns":76808644
        },
        {
            "host":"<hostname>",
            "key":"log[/home/zabbix/logs/zabbix_agentd.log]",
            "lastlogsize":112,
            "value":" 19845:20140621:141708.521 Starting Zabbix Agent [<hostname>]. Zabbix 2.4.0 (revision 50000).",
            "id": 2,
            "clock":1400675595,            
            "ns":77053975
        },
        {
            "host":"<hostname>",
            "key":"vfs.fs.size[/nono]",
            "state":1,
            "value":"Cannot obtain filesystem information: [2] No such file or directory",
            "id": 3,
            "clock":1400675595,            
            "ns":78154128
        }
    ],
    "clock": 1400675595,
    "ns": 78211329
}
```

Виртуальный ID назначается каждому значению. Значением ID является простой счётчик с инкрементом, уникальный в пределах одной сессии передачи данных (идентифицируется при помощи токена сессии). Этот ID используется, чтобы отбрасывать дубликаты значений, которые могут быть отправлены в средах с плохой связью.

**Ответ сервера**

``` {.javascript}
{
    "response":"success",
    "info":"processed: 3; failed: 0; total: 3; seconds spent: 0.003534"
}
```

::: noteimportant
Если отправка некоторых значений завершилась неудачей на сервере (например, по причине того, что узел сети или элемент данных был деактивирован или удален), агент не будет повторять отправку этих элементов данных
:::

Например:

1.  Агент открывает TCP соединение
2.  Агент отправляет список значений
3.  Сервер обрабатывает данные и отправляет обратно статус
4.  TCP соединение закрывается

Обратите внимание на то, как в примере выше указывается неподдерживаемое состояние для vfs.fs.size\[/nono\] при помощи значения "state" равного 1 и сообщения об ошибке в свойстве "value".

::: noteimportant
Сообщение об ошибке будет обрезано до 2048 символов на стороне сервера.
:::

[comment]: # ({/dd14368c-8c5ecfe1})

[comment]: # ({new-ee2b7a2a})

##### Heartbeat message

The heartbeat message is sent by an active agent to Zabbix server/proxy 
every HeartbeatFrequency seconds (configured in the Zabbix agent 
[configuration file](/manual/appendix/config/zabbix_agentd)). 

It is used to monitor the availability of active checks.

```json
{
  "request": "active check heartbeat",
  "host": "Zabbix server",
  "heartbeat_freq": 60
}
```

| Field | Type | Mandatory | Value |
|-|-|-|--------|
| request | _string_ | yes | `active check heartbeat` |
| host | _string_ | yes | The host name. |
| heartbeat_freq | _number_ | yes | The agent heartbeat frequency (HeartbeatFrequency configuration parameter). |

[comment]: # ({/new-ee2b7a2a})

[comment]: # ({72c60b35-e66043c4})
#### Более старый XML протокол

::: noteclassic
Zabbix воспринимает до 16 МВ XML данных закодированных в Base64, но одиночное декодированное значение не должно быть длиннее 64 КБ, в противном случае значение будет обрезано до 64 КБ в процессе декодирования.
:::

[comment]: # ({/72c60b35-e66043c4})
