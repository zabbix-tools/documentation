[comment]: # translation:outdated

[comment]: # ({new-3aa2cead})
# 13 Удаленный мониторинг статистики Zabbix

[comment]: # ({/new-3aa2cead})

[comment]: # ({598f1caa-22debe41})
#### Обзор

Возможно сделать некоторые внутренние метрики Zabbix сервера и прокси доступными удаленно другому Zabbix экземпляру или сторонним утилитам. Это может быть полезно поставщикам поддержки/сервисов, предоставив возможность мониторить удаленно Zabbix сервера/прокси своих клиентов или в организациях, где Zabbix не основной
инструмент мониторинга, внутренние метрики Zabbix можно мониторить сторонней системой при использовании зонтичного мониторинга.

Внутренняя статистика Zabbix открыта для настраиваемого списка адресов, которые указываются в новом параметре 'StatsAllowedIP' [сервера](/manual/appendix/config/zabbix_server)/[прокси](/manual/appendix/config/zabbix_proxy). Запросы будут приниматься только с этих адресов.

[comment]: # ({/598f1caa-22debe41})

[comment]: # ({03e7ec75-a506f2e2})

#### Элементы данных

Для настройки запроса внутренней статистики с другого экземпляра Zabbix вы можете использовать два элемента данных:

-   Внутренний элемент данных `zabbix[stats,<ip>,<port>]` - для прямых удаленных запросов к Zabbix серверу/прокси. <ip> и <порт> используются для идентификации необходимого экземпляра.
-   Элемент данных агента `zabbix.stats[<ip>,<port>]` для удаленных запросов к Zabbix серверу/прокси через агента. <ip> и <порт> используются для идентификации необходимого экземпляра.

Смотрите также: [Внутренние элементы данных](/manual/config/items/itemtypes/internal), [Элементы данных Zabbix агента](/manual/config/items/itemtypes/zabbix_agent)

Следующая диаграмма иллюстрирует использование обоих элементов данных в зависимости от контекста.

![](../../../../assets/en/manual/appendix/items/ext_stats.png)

-   ![](../../../../assets/en/manual/appendix/items/green.png) - Сервер →  внешний экземпляр Zabbix (`zabbix[stats,<ip>,<port>]`)
-   ![](../../../../assets/en/manual/appendix/items/pink.png) - Сервер → прокси → внешний экземпляр Zabbix (`zabbix[stats,<ip>,<port>]`)
-   ![](../../../../assets/en/manual/appendix/items/blue.png) - Сервер → агент → внешний экземпляр Zabbix (`zabbix.stats[<ip>,<port>]`)
-   ![](../../../../assets/en/manual/appendix/items/red.png) - Сервер → прокси → агент → внешний экземпляр Zabbix (`zabbix.stats[<ip>,<port>]`)

Чтобы убедиться, что целевой экземпляр разрешает запросы к себе с внешнего экземпляра, укажите адрес внешнего экземпляра в параметре 'StatsAllowedIP' на целевом экземпляре.

[comment]: # ({/03e7ec75-a506f2e2})

[comment]: # ({f8b4fc9f-ae65d17e})
#### Предоставляемые метрики

Элементы данных собирают статистику одним запросом и возвращают JSON, который является основой зависимых элементов данных и из которого они получают данные. Любым из двух элементов данных возвращаются следующие [внутренние метрики](/ru/manual/config/items/itemtypes/internal):

-   `zabbix[boottime]`
-   `zabbix[hosts]`
-   `zabbix[items]`
-   `zabbix[items_unsupported]`
-   `zabbix[preprocessing_queue]` (только сервер)
-   `zabbix[process,<тип>,<режим>,<состояние>]` (статистика только на основе типов процессов)
-   `zabbix[rcache,<кэш>,<режим>]`
-   `zabbix[requiredperformance]`
-   `zabbix[triggers]` (только сервер)
-   `zabbix[uptime]`
-   `zabbix[vcache,buffer,<режим>]` (только сервер)
-   `zabbix[vcache,cache,<параметр>]`
-   `zabbix[vmware,buffer,<режим>]`
-   `zabbix[wcache,<кэш>,<режим>]` (тип кэша 'trends' только сервер)

[comment]: # ({/f8b4fc9f-ae65d17e})

[comment]: # ({3fe58cc5-33cbf401})
#### Шаблоны

Для [удаленного мониторинга](/ru/manual/appendix/items/remote_stats) внутренних метрик Zabbix сервера или прокси с внешнего экземпляра доступные шаблоны:

-   Template App Remote Zabbix server
-   Template App Remote Zabbix proxy

Обратите внимание, что для использования шаблонов удаленного мониторинга нескольких внешних экземпляров, для мониторинга каждого внешнего экземпляра потребуется отдельный узел сети.

[comment]: # ({/3fe58cc5-33cbf401})

[comment]: # ({a3055303-89428848})

#### Процесс траппер

Получение запросов внутренних метрик с внешнего экземпляра Zabbix обрабатывается процессом траппер, который проверяет запрос, собирает метрики, создает буфер данных JSON и отправляет назад подготовленный JSON, например, с сервера:

``` {.java}
{
  "response": "success",
  "data": {
    "boottime": N,
    "uptime": N,
    "hosts": N,
    "items": N,
    "items_unsupported": N,
    "preprocessing_queue": N,
    "process": {
      "alert manager": {
        "busy": {
          "avg": N,
          "max": N,
          "min": N
        },
        "idle": {
          "avg": N,
          "max": N,
          "min": N
        },
        "count": N
      },
...
    },
    "queue": N,
    "rcache": {
      "total": N,
      "free": N,
      "pfree": N,
      "used": N,
      "pused": N
    },
    "requiredperformance": N,
    "triggers": N,
    "uptime": N,
    "vcache": {
      "buffer": {
        "total": N,
        "free": N,
        "pfree": N,
        "used": N,
        "pused": N
      },
      "cache": {
        "requests": N,
        "hits": N,
        "misses": N,
        "mode": N
      }
    },
    "vmware": {
      "total": N,
      "free": N,
      "pfree": N,
      "used": N,
      "pused": N
    },
    "version": "N",
    "wcache": {
      "values": {
        "all": N,
        "float": N,
        "uint": N,
        "str": N,
        "log": N,
        "text": N,
        "not supported": N
      },
      "history": {
        "pfree": N,
        "free": N,
        "total": N,
        "used": N,
        "pused": N
      },
      "index": {
        "pfree": N,
        "free": N,
        "total": N,
        "used": N,
        "pused": N
      },
      "trend": {
        "pfree": N,
        "free": N,
        "total": N,
        "used": N,
        "pused": N
      }
    }
  }
}
```

[comment]: # ({/a3055303-89428848})

[comment]: # ({07d5b0dc-c668697f})
####  Элементы данных внутренней очереди

Также имеются два других элемента данных, которые позволяют получить внутреннюю статистику очереди удаленным запросом с другого экземпляра Zabbix:

-   Внутренний элемент данных `zabbix[stats,<ip>,<port>,queue,<from>,<to>]` - для прямых запросов внутренней очереди с удаленных Zabbix сервера/прокси
-   Элемент данных агента `zabbix.stats[<ip>,<port>,queue,<from>,<to>]` - для запросов внутренней очереди удаленных Zabbix сервера/прокси через агента

Смотрите также: [Внутренние элементы данных](/ru/manual/config/items/itemtypes/internal), [Элементы данных Zabbix агента](/ru/manual/config/items/itemtypes/zabbix_agent)

[comment]: # ({/07d5b0dc-c668697f})
