[comment]: # translation:outdated

[comment]: # ({new-098d0431})
# 9 Заметки о параметре типа памяти в элементах данных proc.mem

[comment]: # ({/new-098d0431})

[comment]: # ({83f00410-2840c113})
#### Обзор

Параметр **тип памяти** поддерживается на платформах Linux, AIX, FreeBSD и Solaris.

Три общих значения 'типа памяти' поддерживаются на всех этих платформах: `pmem`, `rss` и `vsize`. В дополнение, для некоторых платформ поддерживаются специфичные для этих платформ значения 'типа памяти'.

[comment]: # ({/83f00410-2840c113})

[comment]: # ({67b83bde-04a03696})
#### AIX

Смотри значения поддерживаемые параметром 'типа памяти' на AIX в таблице.

|Поддерживаемое значение|Описание|Источник в структуре procentry64|Пытается быть совместимым с|
|---------------|-----------|-------------------------------|---------------------------|
|vsize ^[1](proc_mem_notes#footnotes)^|Размер виртуальной памяти|pi\_size|<|
|pmem|Процент физической памяти|pi\_prm|ps -o pmem|
|rss|Резидентный размер набора|pi\_trss + pi\_drss|ps -o rssize|
|size|Размер процесса (код + данные)|pi\_dvm|"ps gvw" колонка SIZE|
|dsize|Размер данных|pi\_dsize|<|
|tsize|Размер текста (кода)|pi\_tsize|"ps gvw" колонка TSIZ|
|sdsize|Размер данных из разделяемой библиотеки|pi\_sdsize|<|
|drss|Резидентный размер набора данных|pi\_drss|<|
|trss|Резидентный размер набора текста|pi\_trss|<|

[comment]: # ({/67b83bde-04a03696})

[comment]: # ({new-cc0df248})

Notes for AIX:

1. When choosing parameters for proc.mem[] item key on AIX, try to specify narrow process selection criteria. Otherwise there is a risk of getting unwanted processes counted into proc.mem[] result.

Example:
```
\$ zabbix_agentd -t proc.mem[,,,NonExistingProcess,rss]
proc.mem[,,,NonExistingProcess,rss]           [u|2879488]
```

This example shows how specifying only command line (regular expression to match) parameter results in Zabbix agent self-accounting - probably not what you want.

[comment]: # ({/new-cc0df248})

[comment]: # ({new-14c4cc2b})
2. Do not use "ps -ef" to browse processes - it shows only non-kernel processes. Use "ps -Af" to see all processes which will be seen by Zabbix agent.

3. Let's go through example of 'topasrec' how Zabbix agent proc.mem[] selects processes.

```
\$ ps -Af | grep topasrec
root 10747984        1   0   Mar 16      -  0:00 /usr/bin/topasrec  -L -s 300 -R 1 -r 6 -o /var/perf daily/ -ypersistent=1 -O type=bin -ystart_time=04:08:54,Mar16,2023
```

proc.mem[] has arguments:

```
proc.mem[<name>,<user>,<mode>,<cmdline>,<memtype>]
```

[comment]: # ({/new-14c4cc2b})

[comment]: # ({new-0be32659})

The 1st criterion is a process name (argument <name>). In our example Zabbix agent will see it as 'topasrec'. In order to match, you need to either specify 'topasrec' or to leave it empty.
The 2nd criterion is a user name (argument <user>). To match, you need to either specify 'root' or to leave it empty.
The 3rd criterion used in process selection is an argument <cmdline>. Zabbix agent will see its value as '/usr/bin/topasrec -L -s 300 -R 1 -r 6 -o /var/perf/daily/ -ypersistent=1 -O type=bin -ystart_time=04:08:54,Mar16,2023'. To match, you need to either specify a regular expression which matches this string or to leave it empty.

Arguments <mode> and <memtype> are applied after using the three criteria mentioned above. 

[comment]: # ({/new-0be32659})

[comment]: # ({9029cf32-8a700330})
#### FreeBSD

Смотрите значения поддерживаемые параметром 'типа памяти' на FreeBSD в таблице.

|Поддерживаемое значение|Описание|Источник в структуре kinfo\_proc|Пытается быть совместимым с|
|---------------------------------------------|----------------|--------------------------------------------------|---------------------------------------------------|
|vsize|Размер виртуальной памяти|kp\_eproc.e\_vm.vm\_map.size или ki\_size|ps -o vsz|
|pmem|Процент физической памяти|вычисляется из rss|ps -o pmem|
|rss|Резидентный размер набора|kp\_eproc.e\_vm.vm\_rssize или ki\_rssize|ps -o rss|
|size (( - значение по умолчанию))|Размер процесса (код + данные + стэк)|tsize + dsize + ssize|<|
|tsize|Размер текста (кода)|kp\_eproc.e\_vm.vm\_tsize или ki\_tsize|ps -o tsiz|
|dsize|Размер данных|kp\_eproc.e\_vm.vm\_dsize или ki\_dsize|ps -o dsiz|
|ssize|Размер стэка|kp\_eproc.e\_vm.vm\_ssize или ki\_ssize|ps -o ssiz|

[comment]: # ({/9029cf32-8a700330})

[comment]: # ({ad02053c-f43d1dfd})
#### Linux

Смотрите значения поддерживаемые параметром 'типа памяти' на Linux в таблице.

|Поддерживаемое значение|Описание|Источник из /proc/<pid>/status файла|
|---------------|-----------|---------------------------------------|
|vsize ^[1](proc_mem_notes#footnotes)^|Размер виртуальной памяти|VmSize|
|pmem|Процент физической памяти|(VmRSS/total\_memory) \* 100|
|rss|Резидентный размер набора|VmRSS|
|data|Размер сегмента данных|VmData|
|exe|Размер сегмента кода|VmExe|
|hwm|Пиковый резидентный размер набора|VmHWM|
|lck|Размер заблокированной памяти|VmLck|
|lib|Размер разделяемых библиотек|VmLib|
|peak|Пиковый размер виртуальной памяти|VmPeak|
|pin|Размер закрепленных страниц|VmPin|
|pte|Размер страниц записей таблицы|VmPTE|
|size|Размер сегментов кода + данных + стэка|VmExe + VmData + VmStk|
|stk|Размер сегмента стэка|VmStk|
|swap|Размер используемого места в разделе подкачки|VmSwap|

Заметки по Linux:

1.  Не все значения 'типа памяти' поддерживаются старыми версиями ядра Linux. Например, ядра Linux 2.4 не поддерживают значения `hwm`, `pin`, `peak`, `pte` и `swap`.
2.  Мы заметили, что процесс само-диагностики активных проверок Zabbix агента с `proc.mem[...,...,...,...,data]` отображает значение, которое на 4 КБ больше, чем отдает строка `VmData` в файле агента /proc/<pid>/status. Во время самостоятельного измерения сегмент данных агента увеличивается на 4 КБ и затем возвращается к предыдущему значению.

[comment]: # ({/ad02053c-f43d1dfd})

[comment]: # ({5c537a8b-96fd9f28})
#### Solaris

Смотрите значения поддерживаемые параметром 'типа памяти' на Solaris в таблице 

|Поддерживаемое значение|Описание|Источник в структуре psinfo|Пытается быть совместимым с|
|---------------------------------------------|----------------|---------------------------------------------|---------------------------------------------------|
|vsize (( - значение по умолчанию))|Размер образа процесса|pr\_size|ps -o vsz|
|pmem|Процент физической памяти|pr\_pctmem|ps -o pmem|
|rss|Резидентный размер набора<br>Он может быть недооценен - см. описание rss в "man ps".|pr\_rssize|ps -o rss|

[comment]: # ({/5c537a8b-96fd9f28})


[comment]: # ({244c287f-e0ce8129})
##### Сноски

^**1**^ Значение по умолчанию.

[comment]: # ({/244c287f-e0ce8129})
