[comment]: # translation:outdated

[comment]: # ({new-930fae3b})
# 14 Настройка Kerberos с Zabbix

[comment]: # ({/new-930fae3b})

[comment]: # ({40070a2e-5be9f534})
#### Обзор

Kerberos аутентификацию можно использовать в веб-мониторинге и HTTP элементах данных в Zabbix начиная с версии 4.4.0.

Этот раздел приводит пример настройки Kerberos с Zabbix сервером для выполнения мониторинга `www.example.com` под пользователем 'zabbix'.

[comment]: # ({/40070a2e-5be9f534})

[comment]: # ({new-edb96e72})
#### Шаги

[comment]: # ({/new-edb96e72})

[comment]: # ({0a1ecdac-33108ef2})
##### Шаг 1

Установите пакет Kerberos.

Для Debian/Ubuntu:

    apt install krb5-user

Для RHEL/CentOS:

    yum install krb5-workstation

[comment]: # ({/0a1ecdac-33108ef2})

[comment]: # ({3d938584-b841f368})
##### Шаг 2

Настройте файл конфигурации Kerberos (смотрите документацию MIT для получения более подробной информации)

``` {.java}
cat /etc/krb5.conf 
[libdefaults]
    default_realm = EXAMPLE.COM

[comment]: # ({/3d938584-b841f368})

[comment]: # ({1861e35a-90a52eb3})
# Следующие переменные krb5.conf предназначены только для MIT Kerberos.
    kdc_timesync = 1
    ccache_type = 4
    forwardable = true
    proxiable = true

    [realms]
    EXAMPLE.COM = {
    }

    [domain_realm]
    .example.com=EXAMPLE.COM
    example.com=EXAMPLE.COM

    ```

[comment]: # ({/1861e35a-90a52eb3})

[comment]: # ({a3f9ba6c-ffbcf49a})
##### Шаг 3

Создайте билет Kerberos для пользователя *zabbix*. Выполните следующую команду под пользователем *zabbix*:

    kinit zabbix

::: noteimportant
Важно выполнить команду выше под пользователем *zabbix*. Если вы выполните её под *root* аутентификация не будет работать.
:::

[comment]: # ({/a3f9ba6c-ffbcf49a})

[comment]: # ({23c3199b-01fcf461})
##### Шаг 4

Создайте веб-сценарий или элемента данных HTTP агент с типом аутентификации Kerberos.

Опционально можно протестировать при помощи следующей команды:

    curl -v --negotiate -u : http://example.com

Обратите внимание, что для длительного веб-мониторинга потребуется позаботиться о продлении срока действия Kerberos билета. По умолчанию время истечения срока билета 10ч.

[comment]: # ({/23c3199b-01fcf461})
