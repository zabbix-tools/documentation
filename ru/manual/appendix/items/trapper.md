[comment]: # translation:outdated

[comment]: # ({new-1602be12})
# 4 Траппер элементы данных

[comment]: # ({/new-1602be12})

[comment]: # ({new-ea58f0b6})
#### Обзор

Zabbix сервер использует протокол коммуникации на основе JSON для
получения данных от Zabbix sender в случае с [траппер элементами
данных](/ru/manual/config/items/itemtypes/trapper).

Сообщения запроса и ответа должны начинаться с [заголовка и длины
данныхheader and data
length](/ru/manual/appendix/protocols/header_datalen).

[comment]: # ({/new-ea58f0b6})

[comment]: # ({new-51cd3761})
#### Запрос Zabbix sender

``` {.javascript}
{
    "request":"sender data",
    "data":[
        {
            "host":"<имя хоста>",
            "key":"trap",
            "value":"тестовое значение"
        }
    ]
}
```

[comment]: # ({/new-51cd3761})

[comment]: # ({new-c4e3e287})
#### Ответ Zabbix сервера

``` {.javascript}
{
    "response":"success",
    "info":"processed: 1; failed: 0; total: 1; seconds spent: 0.060753"
}
```

[comment]: # ({/new-c4e3e287})

[comment]: # ({new-64bfec01})
#### Также Zabbix sender может отправлять запрос со штампом времени

``` {.javascript}
{
    "request":"sender data",
    "data":[
        {
            "host":"<имя хоста>",
            "key":"trap",
            "value":"тестовое значение",
            "clock":1516710794
        },
        {
            "host":"<имя хоста>",
            "key":"trap",
            "value":"тестовое значение",
            "clock":1516710795
        }
    ],
    "clock":1516712029,
    "ns":873386094
}
```

[comment]: # ({/new-64bfec01})

[comment]: # ({new-c71e91fd})
#### Ответ Zabbix сервера

``` {.javascript}
{
    "response":"success",
    "info":"processed: 2; failed: 0; total: 2; seconds spent: 0.060904"
}
```

[comment]: # ({/new-c71e91fd})
