[comment]: # translation:outdated

[comment]: # ({new-f9e8f670})
# 11 proc.get parameters

[comment]: # ({/new-f9e8f670})

[comment]: # ({new-b5ff62ed})
#### Overview

The item **proc.get**\[<name>,<user>,<cmdline>,<mode>\] is supported on Linux, Windows, FreeBSD, OpenBSD, and NetBSD.

List of process parameters returned by the item varies depending on the operating system and 'mode' argument value. 
 

[comment]: # ({/new-b5ff62ed})

[comment]: # ({new-f5f4da9b})
#### Linux

The following process parameters are returned on Linux for each mode:

|mode=process|mode=thread|mode=summary|
|---------------|-----------|-------------------------------|
|pid: PID |pid: PID|name: process name |
|ppid: parent PID|ppid: parent PID| processes: number of processes |
|name: process name|name: process name | vsize: virtual memory size |
|cmdline: command with arguments |tid: thread ID|pmem: percentage of real memory |
|vsize: virtual memory size | tname: thread name |rss: resident set size |
|pmem: percentage of real memory |cputime_user: total CPU seconds (user)|data: size of data segment |
|rss: resident set size |cputime_system: total CPU seconds (system) |exe: size of code segment |
|data: size of data segment |state: thread state |lck: size of locked memory |
|exe: size of code segment |ctx_switches: number of context switches |lib: size of shared libraries |
|hwm: peak resident set size | page_faults: number of page faults |pin: size of pinned pages |
|lck: size of locked memory | |pte: size of page table entries |
|lib: size of shared libraries| |size: size of process code + data + stack segments |
|peak: peak virtual memory size| |stk: size of stack segment |
|pin: size of pinned pages| |swap: size of swap space used |
|pte: size of page table entries| |cputime_user: total CPU seconds (user) |
|size: size of process code + data + stack segments| |cputime_system: total CPU seconds (system) |
|stk: size of stack segment| |ctx_switches: number of context switches |
|swap: size of swap space used| |threads: number of threads |
|cputime_user: total CPU seconds (user)| |page_faults: number of page faults |
|cputime_system: total CPU seconds (system)| | |
|state: process state (transparently retrieved from procfs, long form)| | |
|ctx_switches: number of context switches| | |
|threads: number of threads| | |
|page_faults: number of page faults| | |

[comment]: # ({/new-f5f4da9b})

[comment]: # ({new-306ee774})
#### BSD-based OS

The following process parameters are returned on FreeBSD, OpenBSD, and NetBSD for each mode:

|mode=process|mode=thread|mode=summary|
|---------------|-----------|-------------------------------|
|pid: PID |pid: PID|name: process name |
|ppid: parent PID|ppid: parent PID| processes: number of processes |
|jid: ID of jail (FreeBSD only)|jid: ID of jail (FreeBSD only)|vsize: virtual memory size |
|name: process name|name: process name |pmem: percentage of real memory (FreeBSD only) |
|cmdline: command with arguments |tid: thread ID|rss: resident set size |
|vsize: virtual memory size | tname: thread name |size: size of process (code + data + stack) |
|pmem: percentage of real memory (FreeBSD only)|cputime_user: total CPU seconds (user)|tsize: text (code) size |
|rss: resident set size |cputime_system: total CPU seconds (system) |dsize: data size |
|size: size of process (code + data + stack) |state: thread state |ssize: stack size |
|tsize: text (code) size |ctx_switches: number of context switches |cputime_user: total CPU seconds (user) |
|dsize: data size |io_read_op: number of times the system had to perform input |cputime_system: total CPU seconds (system) |
|ssize: stack size |io_write_op: number of times the system had to perform output |ctx_switches: number of context switches |
|cputime_user: total CPU seconds (user)| |threads: number of threads (not supported for NetBSD) |
|cputime_system: total CPU seconds (system)| |stk: size of stack segment |
|state: process state (disk sleep/running/sleeping/tracing stop/zombie/other)| |page_faults: number of page faults |
|ctx_switches: number of context switches| |fds: number of file descriptors (OpenBSD only) |
|threads: number of threads (not supported for NetBSD)| |swap: size of swap space used |
|page_faults: number of page faults| |io_read_op: number of times the system had to perform input |
|fds: number of file descriptors (OpenBSD only) | |io_write_op: number of times the system had to perform output |
|swap: size of swap space used| | |
|io_read_op: number of times the system had to perform input| | |
|io_write_op: number of times the system had to perform output| | |


[comment]: # ({/new-306ee774})

[comment]: # ({new-fdd0d9d4})
#### Windows

The following process parameters are returned on Windows for each mode:

|mode=process|mode=thread|mode=summary|
|---------------|-----------|-------------------------------|
|pid: PID |pid: PID |name: process name |
|ppid: parent PID |ppid: parent PID |processes: number of processes |
|name: process name |name: process name |vmsize: virtual memory size |
|vmsize: virtual memory size |tid: thread ID |wkset: size of process working set |
|wkset: size of process working set | |cputime_user: total CPU seconds (user) |
|cputime_user: total CPU seconds (user) | |cputime_system: total CPU seconds (system) |
|cputime_system: total CPU seconds (system) | |threads: number of threads |
|threads: number of threads | |page_faults: number of page faults |
|page_faults: number of page faults | |handles: number of handles |
|handles: number of handles | |io_read_b: IO bytes read |
|io_read_b: IO bytes read | |io_write_b: IO bytes written |
|io_write_b: IO bytes written | |io_read_op: IO read operations |
|io_read_op: IO read operations | |io_write_op: IO write operations |
|io_write_op: IO write operations | |io_other_b: IO bytes transferred, other than read and write operations |
|io_other_b: IO bytes transferred, other than read and write operations | |io_other_op: IO operations, other than read and write operations |
|io_other_op: IO operations, other than read and write operations | | |

[comment]: # ({/new-fdd0d9d4})
