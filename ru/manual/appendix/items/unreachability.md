[comment]: # translation:outdated

[comment]: # ({new-02f012f5})
# 12 Настройки недостижимости/недоступности хостов

[comment]: # ({/new-02f012f5})

[comment]: # ({new-b32082d6})
#### Обзор

Несколько [параметров](/ru/manual/appendix/config/zabbix_server)
конфигурации определяют каким образом Zabbix сервер должен вести себя,
если агентские проверки (Zabbix, SNMP, IPMI, JMX) завершаются с ошибками
и хост становится недостижимым.

[comment]: # ({/new-b32082d6})

[comment]: # ({new-e56dd03b})
#### Недостижимый узел сети

Узел сети считается недостижимым после ошибки при проверке (сетевая
ошибка, превышение времени ожидания) при помощи Zabbix, SNMP, IPMI или
JMX агентов. Возьмите на заметку, что активные проверки Zabbix агента
никак не влияют на доступность узла сети.

С этого момента **UnreachableDelay** определяет как часто узел сети
будет повторно проверяться, используя один из элементов данных (включая
LLD правила), в ситуации недостижимости и такие повторные проверки
выполняются уже с помощью unreachable поллеров (или IPMI поллеров для
IPMI проверок). По умолчанию 15 секунд до следующей проверки.

В журнале Zabbix сервера недостижимость записывается сообщениями
подобными следующим:

    Zabbix agent item [system.cpu.load[percpu,avg1]] on host [New host] failed: first network error, wait for 15 seconds
    Zabbix agent item [system.cpu.load[percpu,avg15]] on host [New host] failed: another network error, wait for 15 seconds

Обратите внимание, что указывается какой точно элемент данных выполнился
с ошибкой и тип этого элемента данных (Zabbix агент).

::: noteclassic
Параметр *Timeout* также влияет на то, как быстро элемент
данных будет проверен повторно во течении периода недостижимости. Если
время ожидания 20 секунд и UnreachableDelay 30 секунд, следующая
проверка будет через 50 секунд после первой попытки.
:::

Параметр **UnreachablePeriod** определяет общую длительность периода
недостижимости. По умолчанию, UnreachablePeriod 45 секунд.
UnreachablePeriod должен быть в несколько раз больше, чем
UnreachableDelay, так чтобы элементы данных проверялись повторно более
одного раза до того момента, как узлы сети станут недоступными.

Если нежостижимый узел сети заработает, наблюдение за узлом сети
вернется к нормальному режиму автоматически:

    resuming Zabbix agent checks on host "New host": connection restored

[comment]: # ({/new-e56dd03b})

[comment]: # ({new-ba32db14})
#### Недоступный узел сети

После того как UnreachablePeriod завершится и узел сети не стал
доступным, такой узел сети считается недоступным.

В журнал Zabbix сервера подобное записывается примерно следующим
сообщением:

    temporarily disabling Zabbix agent checks on host [New host]: host unavailable

и в
[веб-интерфейсе](/ru/manual/web_interface/frontend_sections/configuration/hosts)
иконка доступности узла сети меняет свой цвет с зеленого (или серого) на
красный (обратите внимание, что при наведении указателя мыши отобразится
подсказка с описанием ошибки):

![](../../../../assets/en/manual/config/unavailable.png)

Параметр **UnavailableDelay** определяет, как часто элементы данных
будут проверяться повторно пока узел сети недоступен.

По умолчанию - раз в 60 секунд (таким образом, в этом случае "временное
отключение" из файла журнала выше будет означать деактивацию проверок
сроком на одну минуту).

Когда соединение c узлом сети будет восстановлено, наблюдение за узлом
сети вернется к нормальному режиму автоматически:

    enabling Zabbix agent checks on host [New host]: host became available

[comment]: # ({/new-ba32db14})


[comment]: # ({new-36969217})
#### Unavailable interface

After the UnreachablePeriod ends and the interface has not reappeared,
the interface is treated as unavailable.

In the server log it is indicated by messages like these:

    temporarily disabling Zabbix agent checks on host "New host": interface unavailable

and in the
[frontend](/manual/web_interface/frontend_sections/configuration/hosts)
the host availability icon goes from green/gray to yellow/red (the
unreachable interface details can be seen in the hint box that is
displayed when a mouse is positioned on the host availability icon):

![](../../../../assets/en/manual/config/unavailable.png)

The **UnavailableDelay** parameter defines how often an interface is
checked during interface unavailability.

By default it is 60 seconds (so in this case "temporarily disabling",
from the log message above, will mean disabling checks for one minute).

When the connection to the interface is restored, the monitoring returns
to normal automatically, too:

    enabling Zabbix agent checks on host "New host": interface became available

[comment]: # ({/new-36969217})
