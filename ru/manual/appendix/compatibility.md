[comment]: # translation:outdated

[comment]: # ({new-d25f4c9e})
# 13 Совместимость версий

[comment]: # ({/new-d25f4c9e})

[comment]: # ({new-eefa9a73})
#### Поддерживаемые агенты

Zabbix агенты начиная с 1.4 версии совместимы с Zabbix 4.0. Однако,
возможно вам потребуется пересмотреть конфигурацию старых агентов, так
как некоторые параметры могли измениться, например, параметры связанные
с
[журналированием](https://www.zabbix.com/documentation/3.0/ru/manual/installation/upgrade_notes_300#изменения_в_параметрах_конфигурации_связанные_с_журналированием)
по сравнению с версиями до 3.0.

Чтобы воспользоваться всеми преимуществами новых и улучшенных элементов
данных, улучшенной производительности и уменьшенным использованием
памяти, используйте последнюю версию агента 4.0.

[comment]: # ({/new-eefa9a73})

[comment]: # ({new-1a5df00a})
#### Поддерживаемые Zabbix прокси

Zabbix 4.0.x сервер может работать только с Zabbix 4.0.x прокси. Zabbix
4.0.x прокси могут работать только с Zabbix 4.0.x сервером.

::: noteimportant
Известно, что имеется возможность запустить
обновленный сервер с ещё не обновленными прокси, которые будут
отправлять данные на новый сервер (хотя прокси и не смогут обновлять
свою конфигурацию). Однако, такой подход не рекомендуется и не
поддерживается Zabbix, его выбор исключительно на ваш страх и риск. Для
получения более подробных сведений, смотрите [процедуру
обновления](/ru/manual/installation/upgrade).
:::

[comment]: # ({/new-1a5df00a})

[comment]: # ({new-5be689b0})
#### Поддержка XML файлов

XML файлы, экспортированные в версиях 1.8, 2.0, 2.2, 2.4, 3.0, 3.2 и 3.4
поддерживаются при импорте в версию Zabbix 4.0.

::: noteimportant
В XML формате Zabbix 1.8 при экспорте, зависимости
триггеров сортируются только по названию. Если имеется несколько
триггеров с одинаковым именем (например, имеющие разные важности и
выражения), которые имеют заданную зависимость между собой, то их
невозможно импортировать. Такие зависимости должны быть удалены вручную
из файла XML и добавлены после импорта.
:::

[comment]: # ({/new-5be689b0})

[comment]: # ({new-78c6278a})

In relation to Zabbix server, proxies can be:
-   *Current* (proxy and server have the same major version);
-   *Outdated* (proxy version is older than server version, but is partially supported);
-   *Unsupported* (proxy version is older than server previous LTS release version *or* proxy version is newer than
server major version).

Examples:

|Server version|*Current* proxy version|*Outdated* proxy version|*Unsupported* proxy version|
|--------------|-----------------------|------------------------|---------------------------|
|6.4|6.4|6.0, 6.2|Older than 6.0; newer than 6.4|
|7.0|7.0|6.0, 6.2, 6.4|Older than 6.0; newer than 7.0|
|7.2|7.2|7.0|Older than 7.0; newer than 7.2|

[comment]: # ({/new-78c6278a})

[comment]: # ({new-9f9fce98})

Functionality supported by proxies:

|Proxy version|Data update|Configuration update|Tasks|
|--|--|--|----|
|*Current*|Yes|Yes|Yes|
|*Outdated*|Yes|No|[Remote commands](/manual/config/notifications/action/operation/remote_command) (e.g., shell scripts);<br>Immediate item value checks (i.e., *[Execute now](/manual/config/items/check_now)*);<br>Note: Preprocessing [tests with a real value](/manual/config/items/preprocessing#testing-real-value) are not supported.|
|*Unsupported*|No|No|No|

[comment]: # ({/new-9f9fce98})

[comment]: # ({new-f9a2762b})

Warnings about using incompatible Zabbix daemon versions are logged.

[comment]: # ({/new-f9a2762b})


[comment]: # ({new-5f9e54ca})
#### Supported XML files

XML files not older than version 1.8 are supported for import in Zabbix
6.0.

::: noteimportant
In the XML export format, trigger dependencies are
stored by name only. If there are several triggers with the same name
(for example, having different severities and expressions) that have a
dependency defined between them, it is not possible to import them. Such
dependencies must be manually removed from the XML file and re-added
after import.
:::

[comment]: # ({/new-5f9e54ca})
