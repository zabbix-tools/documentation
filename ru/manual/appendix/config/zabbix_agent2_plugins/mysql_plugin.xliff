<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="ru" datatype="plaintext" original="manual/appendix/config/zabbix_agent2_plugins/mysql_plugin.md">
    <body>
      <trans-unit id="f957ad95" xml:space="preserve">
        <source># 7 MySQL plugin</source>
      </trans-unit>
      <trans-unit id="7e8aa5c3" xml:space="preserve">
        <source>#### Overview

This section lists parameters supported in the MySQL Zabbix agent 2
plugin configuration file (mysql.conf). 

Note that:

-   The default values reflect process defaults, not the values in the
    shipped configuration files;
-   Zabbix supports configuration files only in UTF-8 encoding without
    [BOM](https://en.wikipedia.org/wiki/Byte_order_mark);
-   Comments starting with "\#" are only supported at the beginning of
    the line.</source>
      </trans-unit>
      <trans-unit id="b9c98913" xml:space="preserve">
        <source>#### Parameters

|Parameter|Mandatory|Range|Default|Description|
|--|--|--|--|----------|
|Plugins.Mysql.CallTimeout|no|1-30|global timeout|The maximum amount of time in seconds to wait for a request to be done.|
|Plugins.Mysql.Default.Password|no| | |Default password for connecting to MySQL; used if no value is specified in an item key or named session.|
|Plugins.Mysql.Default.TLSCAFile|no&lt;br&gt;(yes, if Plugins.Mysql.Default.TLSConnect is set to one of: verify_ca, verify_full)| | |Full pathname of a file containing the top-level CA(s) certificates for peer certificate verification for encrypted communications between Zabbix agent 2 and monitored databases; used if no value is specified in a named session.|
|Plugins.Mysql.Default.TLSCertFile|no&lt;br&gt;(yes, if Plugins.Mysql.Default.TLSConnect is set to one of: verify_ca, verify_full)| | |Full pathname of a file containing the agent certificate or certificate chain for encrypted communications between Zabbix agent 2 and monitored databases; used if no value is specified in a named session.|
|Plugins.Mysql.Default.TLSConnect|no| | |Encryption type for communications between Zabbix agent 2 and monitored databases; used if no value is specified in a named session.&lt;br&gt;&lt;br&gt;Supported values:&lt;br&gt;*required* - require TLS connection;&lt;br&gt;*verify\_ca* - verify certificates;&lt;br&gt;*verify\_full* - verify certificates and IP address.|
|Plugins.Mysql.Default.TLSKeyFile|no&lt;br&gt;(yes, if Plugins.Mysql.Default.TLSConnect is set to one of: verify_ca, verify_full)| | |Full pathname of a file containing the database private key for encrypted communications between Zabbix agent 2 and monitored databases; used if no value is specified in a named session.|
|Plugins.Mysql.Default.Uri|no| |tcp://localhost:3306|Default URI for connecting to MySQL; used if no value is specified in an item key or named session. &lt;br&gt;&lt;br&gt;Should not include embedded credentials (they will be ignored).&lt;br&gt;Must match the URI format.&lt;br&gt;Supported schemes: `tcp`, `unix`; a scheme can be omitted.&lt;br&gt;A port can be omitted (default=3306).&lt;br&gt;Examples: `tcp://localhost:3306`&lt;br&gt;`localhost`&lt;br&gt;`unix:/var/run/mysql.sock`|
|Plugins.Mysql.Default.User|no| | |Default username for connecting to MySQL; used if no value is specified in an item key or named session.|
|Plugins.Mysql.KeepAlive|no|60-900|300|The maximum time of waiting (in seconds) before unused plugin connections are closed.|
|Plugins.Mysql.Sessions.&lt;SessionName&gt;.Password|no| | |Named session password.&lt;br&gt;**&lt;SessionName&gt;** - define name of a session for using in item keys.|
|Plugins.Mysql.Sessions.&lt;SessionName&gt;.TLSCAFile|no&lt;br&gt;(yes, if Plugins.Mysql.Sessions.&lt;SessionName&gt;.TLSConnect is set to one of: verify_ca, verify_full)| | |Full pathname of a file containing the top-level CA(s) certificates for peer certificate verification, used for encrypted communications between Zabbix agent 2 and monitored databases.&lt;br&gt;**&lt;SessionName&gt;** - define name of a session for using in item keys.|
|Plugins.Mysql.Sessions.&lt;SessionName&gt;.TLSCertFile|no&lt;br&gt;(yes, if Plugins.Mysql.Sessions.&lt;SessionName&gt;.TLSConnect is set to one of: verify_ca, verify_full)| | |Full pathname of a file containing the agent certificate or certificate chain, used for encrypted communications between Zabbix agent 2 and monitored databases.&lt;br&gt;**&lt;SessionName&gt;** - define name of a session for using in item keys.|
|Plugins.Mysql.Sessions.&lt;SessionName&gt;.TLSConnect|no| | |Encryption type for communications between Zabbix agent 2 and monitored databases.&lt;br&gt;**&lt;SessionName&gt;** - define name of a session for using in item keys.&lt;br&gt;&lt;br&gt;Supported values:&lt;br&gt;*required* - require TLS connection;&lt;br&gt;*verify\_ca* - verify certificates;&lt;br&gt;*verify\_full* - verify certificates and IP address.|
|Plugins.Mysql.Sessions.&lt;SessionName&gt;.TLSKeyFile|no&lt;br&gt;(yes, if Plugins.Mysql.Sessions.&lt;SessionName&gt;.TLSConnect is set to one of: verify_ca, verify_full)| | |Full pathname of a file containing the database private key used for encrypted communications between Zabbix agent 2 and monitored databases.&lt;br&gt;**&lt;SessionName&gt;** - define name of a session for using in item keys.|
|Plugins.Mysql.Sessions.&lt;SessionName&gt;.Uri|no| | |Connection string of a named session.&lt;br&gt;**&lt;SessionName&gt;** - define name of a session for using in item keys.&lt;br&gt;&lt;br&gt;Should not include embedded credentials (they will be ignored).&lt;br&gt;Must match the URI format.&lt;br&gt;Supported schemes: `tcp`, `unix`; a scheme can be omitted.&lt;br&gt;A port can be omitted (default=3306).&lt;br&gt;Examples: `tcp://localhost:3306`&lt;br&gt;`localhost`&lt;br&gt;`unix:/var/run/mysql.sock`|
|Plugins.Mysql.Sessions.&lt;SessionName&gt;.User|no| | |Named session username.&lt;br&gt;**&lt;SessionName&gt;** - define name of a session for using in item keys.|
|Plugins.Mysql.Timeout|no|1-30|global timeout|Request execution timeout (how long to wait for a request to complete before shutting it down).|

See also:

-   Description of general Zabbix agent 2 configuration parameters:
    [Zabbix agent 2 (UNIX)](/manual/appendix/config/zabbix_agent2) /
    [Zabbix agent 2
    (Windows)](/manual/appendix/config/zabbix_agent2_win)
-   Instructions for configuring [plugins](/manual/extensions/plugins)</source>
      </trans-unit>
    </body>
  </file>
</xliff>
