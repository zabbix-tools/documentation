[comment]: # translation:outdated

[comment]: # ({new-7549d0ca})
# 1 Ceph plugin

[comment]: # ({/new-7549d0ca})

[comment]: # ({new-e123c786})
#### Overview

This section lists parameters supported in the Ceph Zabbix agent 2
plugin configuration file (ceph.conf). Note that:

-   The default values reflect process defaults, not the values in the
    shipped configuration files;
-   Zabbix supports configuration files only in UTF-8 encoding without
    [BOM](https://en.wikipedia.org/wiki/Byte_order_mark);
-   Comments starting with "\#" are only supported at the beginning of
    the line.

[comment]: # ({/new-e123c786})

[comment]: # ({new-30c690c4})
#### Parameters

|Parameter|Mandatory|Range|Default|Description|
|---------|---------|-----|-------|-----------|
|Plugins.Ceph.InsecureSkipVerify|no|false / true|false|Determines whether an http client should verify the server's certificate chain and host name.<br>If *true*, TLS accepts any certificate presented by the server and any host name in that certificate. In this mode, TLS is susceptible to man-in-the-middle attacks (should be used only for testing).|
|Plugins.Ceph.KeepAlive|no|60-900|300|The maximum time of waiting (in seconds) before unused plugin connections are closed.|
|Plugins.Ceph.Sessions.<SessionName>.ApiKey|no|<|<|Named session API key.<br>**<SessionName>** - name of a session for using in item keys.|
|Plugins.Ceph.Sessions.<SessionName>.User|no|<|<|Named session username.<br>**<SessionName>** - name of a session for using in item keys.|
|Plugins.Ceph.Sessions.<SessionName>.Uri|no|<|https://localhost:8003|Connection string of a named session.<br>**<SessionName>** - name of a session for using in item keys.<br><br>Should not include embedded credentials (they will be ignored).<br>Must match the URI format.<br>Only `https` scheme is supported; a scheme can be omitted (since version 5.2.3).<br>A port can be omitted (default=8003).<br>Examples: `https://127.0.0.1:8003`<br>`localhost`|
|Plugins.Ceph.Timeout|no|1-30|global timeout|Request execution timeout (how long to wait for a request to complete before shutting it down).|

See also:

-   Description of general Zabbix agent 2 configuration parameters:
    [Zabbix agent 2 (UNIX)](/manual/appendix/config/zabbix_agent2) /
    [Zabbix agent 2
    (Windows)](/manual/appendix/config/zabbix_agent2_win)
-   Instructions for configuring [plugins](/manual/config/items/plugins)

[comment]: # ({/new-30c690c4})
