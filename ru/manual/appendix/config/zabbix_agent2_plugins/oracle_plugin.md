[comment]: # translation:outdated

[comment]: # ({new-5c4b1cd4})
# 8 Oracle plugin

[comment]: # ({/new-5c4b1cd4})

[comment]: # ({new-e123c786})
#### Overview

This section lists parameters supported in the Ceph Zabbix agent 2
plugin configuration file (ceph.conf). Note that:

-   The default values reflect process defaults, not the values in the
    shipped configuration files;
-   Zabbix supports configuration files only in UTF-8 encoding without
    [BOM](https://en.wikipedia.org/wiki/Byte_order_mark);
-   Comments starting with "\#" are only supported at the beginning of
    the line.

[comment]: # ({/new-e123c786})

[comment]: # ({new-a8434132})
#### Parameters

|Parameter|Mandatory|Range|Default|Description|
|---------|---------|-----|-------|-----------|
|Plugins.Oracle.CallTimeout|no|1-30|global timeout|The maximum wait time in seconds for a request to be completed.|
|Plugins.Oracle.ConnectTimeout|no|1-30|global timeout|The maximum wait time in seconds for a connection to be established.|
|Plugins.Oracle.CustomQueriesPath|no|<|<|Full pathname of a directory containing .sql files with custom queries.<br>Disabled by default.<br>Example: `/etc/zabbix/oracle/sql`|
|Plugins.Oracle.KeepAlive|no|60-900|300|The maximum time of waiting (in seconds) before unused plugin connections are closed.|
|Plugins.Oracle.Sessions.<SessionName>.Password|no|<|<|Named session password.<br>**<SessionName>** - name of a session for using in item keys.|
|Plugins.Oracle.Sessions.<SessionName>.Service|no|<|<|Named session service name to be used for connection (SID is not supported).<br>Supported for: *Oracle*.<br>**<PluginName>** - name of the plugin.<br>**<SessionName>** -name of a session for using in item keys.|
|Plugins.Oracle.Sessions.<SessionName>.Uri|no|<|tcp://localhost:1521|Named session connection string for Oracle.<br>**<SessionName>** - name of a session for using in item keys.<br><br>Should not include embedded credentials (they will be ignored).<br>Must match the URI format.<br>Only `tcp` scheme is supported; a scheme can be omitted (since version 5.2.3).<br>A port can be omitted (default=1521).<br>Examples: `tcp://127.0.0.1:1521`<br>`localhost`|
|Plugins.Oracle.Sessions.<SessionName>.User|no|<|<|Named session username.<br>**<SessionName>** - name of a session for using in item keys.|

See also:

-   Description of general Zabbix agent 2 configuration parameters:
    [Zabbix agent 2 (UNIX)](/manual/appendix/config/zabbix_agent2) /
    [Zabbix agent 2
    (Windows)](/manual/appendix/config/zabbix_agent2_win)
-   Instructions for configuring [plugins](/manual/config/items/plugins)

[comment]: # ({/new-a8434132})
