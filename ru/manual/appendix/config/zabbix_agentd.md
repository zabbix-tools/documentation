[comment]: # attributes: notoc

[comment]: # translation:outdated

[comment]: # ({new-c52b2df3})
# 3 Zabbix агент (UNIX)

::: noteclassic
Значения по умолчанию являются умолчаниями демона, но не
значениями которые указаны в поставляемых файлах
конфигурации.
:::

Поддерживаемые параметры в файле конфигурации Zabbix агента
(*zabbix\_agentd.conf*):

|Параметр|Обязательный|Диапазон|Умолчание|Описание|
|----------------|------------------------|----------------|------------------|----------------|
|Alias|нет|<|<|Задает алиас ключу элемента данных. Его можно использовать для замены длинных и сложных ключей элементов данных на более простые и короткие.<br>Можно добавлять несколько параметров *Alias*. Разрешено указывать несколько параметров с одинаковым ключем *Alias*.<br>Несколько ключей *Alias* могут ссылаться на один и тот же ключ.<br>Алиасы можно использовать в *HostMetadataItem*, но нельзя в *HostnameItem* параметрах.<br><br>Примеры:<br><br>1. Получение ID пользователя 'zabbix'.<br>Alias=zabbix.userid:vfs.file.regexp\[/etc/passwd,"\^zabbix:.:(\[0-9\]+)",,,,\\1\]<br>Теперь можно использовать сокращенный ключ **zabbix.userid**, чтобы получать данные.<br><br>2. Получение утилизации CPU с параметрами по умолчанию и с пользовательскими параметрами.<br>Alias=cpu.util:system.cpu.util<br>Alias=cpu.util\[\*\]:system.cpu.util\[\*\]<br>Такая запись позволяет использовать **cpu.util** ключ для получения утилизации CPU в процентах с параметрами по умолчанию, а также использовать **cpu.util\[all, idle, avg15\]** для получения конкретных данных об утилизации CPU.<br><br>3. Выполнение нескольких правил [низкоуровневого обнаружения](ru/manual/discovery/low_level_discovery), которые обрабатывают одни и те же элементы данных обнаружения.<br>Alias=vfs.fs.discovery\[\*\]:vfs.fs.discovery<br>Теперь имеется возможность добавить несколько правил обнаружения, используя **vfs.fs.discovery** с разными параметрами для каждого правила, например, **vfs.fs.discovery\[foo\]**, **vfs.fs.discovery\[bar\]**, и т.д.|
|AllowRoot|нет|<|0|Разрешение агенту запускаться под 'root'. Если отключено и агент запускается из под 'root', то агент попытается переключиться на пользователя 'zabbix'. Не имеет смысла, если агент запускается под обычным пользователем.<br>0 - не разрешать<br>1 - разрешать|
|BufferSend|нет|1-3600|5|Не хранить данные в буфере дольше N секунд.|
|BufferSize|нет|2-65535|100|Максимальное количество значений в буфере памяти. Агент будет отправлять<br>все собранные данные Zabbix серверу или прокси при заполнении буфера.|
|DebugLevel|нет|0-5|3|Задает уровень журналирования:<br>0 - основная информация о запуске и остановки процессов Zabbix<br>1 - критичная информация<br>2 - информация об ошибках<br>3 - предупреждения<br>4 - для отладки (записывается очень много информации)<br>5 - расширенная отладка (записывается еще больше информации)|
|EnableRemoteCommands|нет|<|0|Разрешены ли удаленные команды с Zabbix сервера.<br>0 - не разрешены<br>1 - разрешены|
|HostMetadata|нет|0-255 символов|<|Опциональный параметр, который задает метаданные узла сети. Метаданные узла сети используются только в процессе автоматической регистрации узлов сети (активный агент).<br>Если не определено, то значение берётся от HostMetadataItem.<br>Агент выдаст ошибку и не запустится, если указанное значение выходит за лимит длины строки или не является UTF-8 строкой.<br>Этот параметр поддерживается с версии 2.2.0 и выше.|
|HostMetadataItem|нет|<|<|Опциональный параметр, который задает элемент данных *Zabbix агент*, который используется для получения метаданных узла сети. Этот параметр используется только, если HostMetadata не определен.<br>Поддерживаются UserParameters и алиасы. Поддерживается *system.run\[\]* независимо от значения *EnableRemoteCommands*.<br>Метаданные узла сети используются только в процессе автоматической регистрации узлов сети (активный агент).<br>В процессе запроса авторегистрации агент запишет в журнал предупреждающее сообщение, если полученное значение от указанного элемента данных выходит за лимит в 255 символов.<br>Значение полученное от указанного элемента данных должно являться UTF-8 строкой, в противном случае оно будет игнорироваться.<br>Этот параметр поддерживается с версии 2.2.0 и выше.|
|Hostname|нет|<|Задается HostnameItem'ом|Уникальное, регистрозависимое имя хоста.<br>Требуется для активных проверок и должно совпадать с именем узла сети указанном на сервере.<br>Допустимые символы: буквенно-цифровые, '.', ' ', '\_' и '-'.<br>Максимальная длина: 64|
|HostnameItem|нет|<|system.hostname|Опциональный параметр, который задает элемент данных *Zabbix агент*, который используется для получения имени хоста. Этот параметр используется только, если Hostname не определен.<br>Не поддерживает UserParameters, счетчики производительности и алиасы, но поддерживает system.run\[\], независимо от значения EnableRemoteCommands.<br>Этот параметр поддерживается с версии 1.8.6 и более новыми.|
|Include|нет|<|<|Вы можете включить отдельные файлы или все файлы из папки с файлом конфигурации.<br>Для включения только необходимых файлов из указанной папки, поддерживается символ звездочки для поиска совпадения по маске. Например: `/абсолютный/путь/к/файлам/конфигурации/*.conf`. Совпадение с маской поддерживается начиная с Zabbix 2.4.0.<br>Смотрите [специальные заметки](special_notes_include) по поводу ограничений.|
|ListenIP|нет|<|0.0.0.0|Список IP адресов разделенных запятыми, которые должен слушать агент.<br>Список из нескольких IP адресов поддерживается с версии 1.8.3 и выше.|
|ListenPort|нет|1024-32767|10050|Агент будет слушать этот порт для подключений с сервера.|
|LoadModule|нет|<|<|Модули, которые загружаются во время старта. Модули используются для расширения возможностей сервера.<br>Формат: Loadmodule=<module.so><br>Модули должны находиться в папке указанной в параметре LoadModulePath.<br>Допускается добавлять несколько параметров LoadModule.|
|LoadModulePath|нет|<|<|Абсолютный путь к папке с модулями агента.<br>По умолчанию зависит от опций компиляции.|
|LogFile|Да, если LogType задан как *file*, иначе нет.|<|<|Имя файла журнала.|
|LogFileSize|нет|0-1024|1|Максимальный размер файла журнала в МБ.<br>0 - отключение автоматической ротации журнала.<br>*Примечание*: Если лимит достигнут и ротация не удалась, по каким-либо причинам, существующий файл журнала очищается и начинается новый.|
|LogType|нет|<|file|Тип вывода журнала:<br>*file* - запись журнала в файл указанный в LogFile параметре,<br>*system* - запись журнала в syslog,<br>*console* - вывод журнала в стандартный вывод.<br>Этот параметр поддерживается начиная с Zabbix 3.0.0.|
|LogRemoteCommands|нет|<|0|Включение журналирования выполняемых shell команд как предупреждений.<br>0 - отключено<br>1 - включено|
|MaxLinesPerSecond|нет|1-1000|20|Максимальное количество новых строк в секунду, которые агент будет отправлять серверу или прокси при обработке активных проверок 'log' и 'eventlog'.<br>Указаное значение будет перезаписано параметром 'maxlines',<br>указанное в ключах элементов данных 'log' и 'eventlog'.<br>*Обратите внимание*: Zabbix будет обрабатывать в 10 раз больше новых строк, чем указано в *MaxLinesPerSecond* при поиске требуемой строки в элементах данных журналов.|
|PidFile|нет|<|/tmp/zabbix\_agentd.pid|Имя PID файла.|
|RefreshActiveChecks|нет|60-3600|120|Как часто обновлять список активных проверок, в секундах.<br>Обратите внимание, что после неуспешного обновления активных проверок, следующая попытка будет предпринята через 60 секунд.|
|Server|да, если StartAgents задано значением 0 явно|<|<|Список разделенных запятой IP адресов, опционально в CIDR нотации, или имен хостов Zabbix серверов и Zabbix прокси.<br>Входящие соединения будут приниматься только с хостов указанных в этом списке.<br>Если включена поддержка IPv6, то '127.0.0.1', '::127.0.0.1', '::ffff:127.0.0.1' обрабатываются одинаково и '::/0' разрешает все IPv4 и IPv6 адреса.<br>'0.0.0.0/0' можно использовать, чтобы разрешить любой IPv4 адрес.<br>Обратите внимание, что "IPv4-совместимые IPv6 адреса" (0000::/96 prefix) поддерживаются, но являются устаревшими согласно [RFC4291](https://tools.ietf.org/html/rfc4291#section-2.5.5) \[en\].<br>Пример: Server=127.0.0.1,192.168.1.0/24,::1,2001:db8::/32,zabbix.domain<br>Пробелы разрешены начиная с версии Zabbix 2.2.|
|ServerActive|нет|<|<|Список пар IP:порт (или имя хоста:порт) Zabbix серверов или Zabbix прокси для активных проверок.<br>Можно указывать несколько адресов разделенных запятыми, чтобы параллельно использовать несколько независимых Zabbix серверов. Пробелы не допустимы.<br>Если порт не указан, то используется порт по умолчанию.<br>IPv6 адреса должны быть заключены в квадратные скобки, если для хоста указывается порт.<br>Если порт порт не указан, то квадратные скобки для IPv6 адресов опциональны.<br>Если параметр не указан, активные проверки отключены.|
|SourceIP|нет|<|<|Локальный IP адрес для исходящих подключений.|
|StartAgents|нет|0-100|3|Количество пре-форков экземпляров zabbix\_agentd, которые обрабатывают пассивные проверки.<br>Если указано значение равное 0, то пассивные проверки будут отключены и агент не будет слушать какой-либо TCP порт.<br>Максимальное количество 16 до версии 1.8.5.|
|Timeout|нет|1-30|3|Тратить не более Timeout секунд при обработке|
|TLSAccept|да, если заданы TLS сертификат или параметры PSK (даже при *незашифрованном* соединении), в противном случае - нет|<|<|Какие принимаются входящие подключения. Используется пассивными проверками. Можно указывать несколько значений, разделенных запятой:<br>*unencrypted* - принимать подключения без шифрования (по умолчанию)<br>*psk* - принимать подключения с TLS и pre-shared ключем (PSK)<br>*cert* - принимать подключения с TLS и сертификатом<br>Этот параметр поддерживается начиная с Zabbix 3.0.0.|
|TLSCAFile|нет|<|<|Абсолютный путь к файлу, который содержит сертификаты верхнего уровня CA(и) для верификации сертификата узла, используется для зашифрованных соединений между Zabbix компонентами.<br>Этот параметр поддерживается начиная с Zabbix 3.0.0.|
|TLSCertFile|нет|<|<|Абсолютный путь к файлу, который содержит сертификат или цепочку сертификатов, используется для зашифрованных соединений между Zabbix компонентами.<br>Этот параметр поддерживается начиная с Zabbix 3.0.0.|
|TLSConnect|да, если заданы TLS сертификат или параметры PSK (даже при *незашифрованном* соединении), в противном случае - нет|<|<|Как агент должен соединяться с Zabbix сервером или прокси. Используется активными проверками. Можно указать только одно значение:<br>*unencrypted* - подключаться без шифрования (по умолчанию)<br>*psk* - подключаться, используя TLS и pre-shared ключем (PSK)<br>*cert* - подключаться, используя TLS и сертификат<br>Этот параметр поддерживается начиная с Zabbix 3.0.0.|
|TLSCRLFile|нет|<|<|Абсолютный путь к файлу, который содержит отозванные сертификаты. Этот параметр используется для зашифрованных соединений между Zabbix компонентами.<br>Этот параметр поддерживается начиная с Zabbix 3.0.0.|
|TLSKeyFile|нет|<|<|Абсолютный путь к файлу, который содержит приватный ключ агента, используется для зашифрованных соединений между Zabbix компонентами.<br>Этот параметр поддерживается начиная с Zabbix 3.0.0.|
|TLSPSKFile|нет|<|<|Абсолютный путь к файлу, который содержит pre-shared ключ агента, используется для зашифрованных соединений с Zabbix сервером.<br>Этот параметр поддерживается начиная с Zabbix 3.0.0.|
|TLSPSKIdentity|нет|<|<|Строка идентификатор pre-shared ключа, используется для зашифрованных соединений с Zabbix сервером.<br>Этот параметр поддерживается начиная с Zabbix 3.0.0.|
|TLSServerCertIssuer|нет|<|<|Разрешенный эмитент сертификата сервера (прокси).<br>Этот параметр поддерживается начиная с Zabbix 3.0.0.|
|TLSServerCertSubject|нет|<|<|Разрешенная тема сертификата сервера (прокси).<br>Этот параметр поддерживается начиная с Zabbix 3.0.0.|
|UnsafeUserParameters|нет|0,1|0|Разрешить все символы, которые можно передать аргументами в пользовательские параметры. Поддерживается начиная с Zabbix 1.8.2.<br>Не разрешены следующие символы:<br>\\ ' " \` \* ? \[ \] { } \~ $ ! & ; ( ) < > \| \# @<br>Кроме того, не разрешены символы новой строки.|
|User|нет|<|zabbix|Использование привилегий указанного, существующего пользователя системы.<br>Имеет эффект только, если запускается под 'root' и AllowRoot отключен.<br>Этот параметр поддерживается начиная с Zabbix 2.4.0.|
|UserParameter|нет|<|<|Пользовательский параметр для мониторинга. Можно указать нескольких пользовательских параметров.<br>Формат: UserParameter=<ключ>,<shell команда><br>Обратите внимание, что команда не должна возвращать только пустую строку или EOL.<br>Например: UserParameter=system.test,who\|wc -l|

::: noteclassic
В Zabbix агенте версии 2.0.0 параметры конфигурации
связанные с активными и пассивными проверками изменены.\
Смотрите раздел ["Смотрите также"](#see_also) внизу этой страницы для
получения более подробных сведений об эти изменениях.
:::

::: noteclassic
Zabbix поддерживает файлы конфигурации только в кодировке
UTF-8 без
[BOM](https://ru.wikipedia.org/wiki/Маркер_последовательности_байтов).\
\
Комментарии, начинающиеся с "\#", поддерживаются только в начале
строки.
:::

[comment]: # ({/new-c52b2df3})

[comment]: # ({new-2773fc24})
### Смотрите также

1.  [Различия в конфигурациях Zabbix агента активных и пассивных
    проверок начиная с версии 2.0.0
    \[en\]](http://blog.zabbix.com/multiple-servers-for-active-agent-sure/858)

[comment]: # ({/new-2773fc24})

[comment]: # ({new-bb271b3c})
### Parameter details

[comment]: # ({/new-bb271b3c})




[comment]: # ({new-20d32ed1})
##### `Alias`
Sets an alias for an item key. It can be used to substitute a long and complex item key with a shorter and simpler one. Multiple *Alias* parameters with the same *Alias* key may be present.<br>Different *Alias* keys may reference the same item key.<br>Aliases can be used in *HostMetadataItem* but not in *HostnameItem* parameters.

Example 1: Retrieving the ID of user 'zabbix'.

    Alias=zabbix.userid:vfs.file.regexp\[/etc/passwd,"\^zabbix:.:(\[0-9\]+)",,,,\\1\]
    
Now the **zabbix.userid** shorthand key may be used to retrieve data.

Example 2: Getting CPU utilization with default and custom parameters.

    Alias=cpu.util:system.cpu.util
    Alias=cpu.util\[\*\]:system.cpu.util\[\*\]

This allows use the **cpu.util** key to get CPU utilization percentage with default parameters as well as use **cpu.util\[all, idle, avg15\]** to get specific data about CPU utilization.

Example 3: Running multiple [low-level discovery](/manual/discovery/low_level_discovery) rules processing the same discovery items.

    Alias=vfs.fs.discovery\[\*\]:vfs.fs.discovery

Now it is possible to set up several discovery rules using **vfs.fs.discovery** with different parameters for each rule, e.g., **vfs.fs.discovery\[foo\]**, **vfs.fs.discovery\[bar\]**, etc.

[comment]: # ({/new-20d32ed1})

[comment]: # ({new-f52682cc})
##### `AllowKey`
Allow execution of those item keys that match a pattern. The key pattern is a wildcard expression that supports the "\*" character to match any number of any characters.<br>Multiple key matching rules may be defined in combination with DenyKey. The parameters are processed one by one according to their appearance order. See also: [Restricting agent checks](/manual/config/items/restrict_checks).

[comment]: # ({/new-f52682cc})

[comment]: # ({new-a02d0dc0})
##### `AllowRoot`
Allow the agent to run as 'root'. If disabled and the agent is started by 'root', the agent will try to switch to user 'zabbix' instead. Has no effect if started under a regular user.

Default: `0` | Values: 0 - do not allow; 1 - allow

[comment]: # ({/new-a02d0dc0})

[comment]: # ({new-12f60552})
##### `BufferSend`
Do not keep data longer than N seconds in buffer.

Default: `5` | Range: 1-3600

[comment]: # ({/new-12f60552})

[comment]: # ({new-397065ee})
##### `BufferSize`
The maximum number of values in the memory buffer. The agent will send all collected data to the Zabbix server or proxy if the buffer is full.

Default: `100` | Range: 2-65535

[comment]: # ({/new-397065ee})

[comment]: # ({new-e5288ea7})
##### `DebugLevel`
Specify the debug level:<br>*0* - basic information about starting and stopping of Zabbix processes<br>*1* - critical information;<br>*2* - error information;<br>*3* - warnings;<br>*4* - for debugging (produces lots of information);<br>*5* - extended debugging (produces even more information).

Default: `3` | Range: 0-5

[comment]: # ({/new-e5288ea7})

[comment]: # ({new-ef1d8b77})
##### `DenyKey`
Deny execution of those item keys that match a pattern. The key pattern is a wildcard expression that supports the "\*" character to match any number of any characters.<br>Multiple key matching rules may be defined in combination with AllowKey. The parameters are processed one by one according to their appearance order. See also: [Restricting agent checks](/manual/config/items/restrict_checks).

[comment]: # ({/new-ef1d8b77})

[comment]: # ({new-50dcbfc8})
##### `EnableRemoteCommands`
Whether remote commands from Zabbix server are allowed. This parameter is **deprecated**, use AllowKey=system.run\[\*\] or DenyKey=system.run\[\*\] instead.<br>It is an internal alias for AllowKey/DenyKey parameters depending on value:<br>0 - DenyKey=system.run\[\*\]<br>1 - AllowKey=system.run\[\*\]

Default: `0` | Values: 0 - do not allow, 1 - allow

[comment]: # ({/new-50dcbfc8})

[comment]: # ({new-764c3d3c})
##### `HeartbeatFrequency`
The frequency of heartbeat messages in seconds. Used for monitoring the availability of active checks.<br>0 - heartbeat messages disabled.

Default: `60` | Range: 0-3600

[comment]: # ({/new-764c3d3c})

[comment]: # ({new-e514f4a5})
##### `HostInterface`
An optional parameter that defines the host interface. The host interface is used at host [autoregistration](/manual/discovery/auto_registration#using_dns_as_default_interface) process. If not defined, the value will be acquired from HostInterfaceItem.<br>The agent will issue an error and not start if the value is over the limit of 255 characters.

Range: 0-255 characters

[comment]: # ({/new-e514f4a5})

[comment]: # ({new-4965ac2b})
##### `HostInterfaceItem`
An optional parameter that defines an item used for getting the host interface.<br>Host interface is used at host [autoregistration](/manual/discovery/auto_registration#using_dns_as_default_interface) process.<br>During an autoregistration request the agent will log a warning message if the value returned by the specified item is over the limit of 255 characters.<br>This option is only used when HostInterface is not defined.

[comment]: # ({/new-4965ac2b})

[comment]: # ({new-42990e42})
##### `HostMetadata`
An optional parameter that defines host metadata. Host metadata is used only at host autoregistration process (active agent). If not defined, the value will be acquired from HostMetadataItem.<br>The agent will issue an error and not start if the specified value is over the limit of 2034 bytes or a non-UTF-8 string.

Range: 0-2034 bytes

[comment]: # ({/new-42990e42})

[comment]: # ({new-953ee255})
##### `HostMetadataItem`
An optional parameter that defines a Zabbix agent item used for getting host metadata. This option is only used when HostMetadata is not defined.User parameters and aliases are supported. The system.run\[\] item is supported regardless of AllowKey/DenyKey values.<br>The HostMetadataItem value is retrieved on each autoregistration attempt and is used only at host autoregistration process (active agent).<br>During an autoregistration request the agent will log a warning message if the value returned by the specified item is over the limit of 65535 UTF-8 code points. The value returned by the item must be a UTF-8 string otherwise it will be ignored.

[comment]: # ({/new-953ee255})

[comment]: # ({new-79d09548})
##### `Hostname`
A list of comma-delimited, unique, case-sensitive hostnames. Required for active checks and must match hostnames as configured on the server. The value is acquired from HostnameItem if undefined.<br>Allowed characters: alphanumeric, '.', ' ', '\_' and '-'. Maximum length: 128 characters per hostname, 2048 characters for the entire line.

Default: Set by HostnameItem

[comment]: # ({/new-79d09548})

[comment]: # ({new-14aaef49})
##### `HostnameItem`
An optional parameter that defines a Zabbix agent item used for getting the host name. This option is only used when Hostname is not defined. User parameters or aliases are not supported, but the system.run\[\] item is supported regardless of AllowKey/DenyKey values.<br>The output length is limited to 512KB.

Default: `system.hostname`

[comment]: # ({/new-14aaef49})

[comment]: # ({new-1e31f1b3})
##### `Include`
You may include individual files or all files in a directory in the configuration file. To only include relevant files in the specified directory, the asterisk wildcard character is supported for pattern matching.<br>See [special notes](special_notes_include) about limitations.

Example:

    Include=/absolute/path/to/config/files/*.conf

[comment]: # ({/new-1e31f1b3})

[comment]: # ({new-d49e31a2})
##### `ListenBacklog`
The maximum number of pending connections in the TCP queue.<br>The default value is a hard-coded constant, which depends on the system.<br>The maximum supported value depends on the system, too high values may be silently truncated to the 'implementation-specified maximum'.

Default: `SOMAXCONN` | Range: 0 - INT\_MAX

[comment]: # ({/new-d49e31a2})

[comment]: # ({new-62349c68})
##### `ListenIP`
A list of comma-delimited IP addresses that the agent should listen on.

Default: `0.0.0.0`

[comment]: # ({/new-62349c68})

[comment]: # ({new-e99b72c0})
##### `ListenPort`
The agent will listen on this port for connections from the server.

Default: `10050` | Range: 1024-32767

[comment]: # ({/new-e99b72c0})

[comment]: # ({new-fb75d239})
##### `LoadModule`
The module to load at agent startup. Modules are used to extend the functionality of the agent. The module must be located in the directory specified by LoadModulePath or the path must precede the module name. If the preceding path is absolute (starts with '/') then LoadModulePath is ignored.<br>Formats:<br>LoadModule=<module.so><br>LoadModule=<path/module.so><br>LoadModule=</abs\_path/module.so><br>It is allowed to include multiple LoadModule parameters.

[comment]: # ({/new-fb75d239})

[comment]: # ({new-bd604c99})
##### `LoadModulePath`
Full path to the location of agent modules. The default depends on compilation options.

[comment]: # ({/new-bd604c99})

[comment]: # ({new-bb5252d8})
##### `LogFile`
Name of the log file.

Mandatory: Yes, if LogType is set to *file*; otherwise no

[comment]: # ({/new-bb5252d8})

[comment]: # ({new-778f1edc})
##### `LogFileSize`
The maximum size of a log file in MB.<br>0 - disable automatic log rotation.<br>*Note*: If the log file size limit is reached and file rotation fails, for whatever reason, the existing log file is truncated and started anew.

Default: `1` | Range: 0-1024

[comment]: # ({/new-778f1edc})

[comment]: # ({new-3d97cc10})
##### `LogRemoteCommands`
Enable logging of the executed shell commands as warnings. Commands will be logged only if executed remotely. Log entries will not be created if system.run\[\] is launched locally by HostMetadataItem, HostInterfaceItem or HostnameItem parameters.

Default: `0` | Values: 0 - disabled, 1 - enabled

[comment]: # ({/new-3d97cc10})

[comment]: # ({new-9d26f327})
##### `LogType`
Type of the log output:<br>*file* - write log to the file specified by LogFile parameter;<br>*system* - write log to syslog;<br>*console* - write log to standard output.

Default: `file`

[comment]: # ({/new-9d26f327})

[comment]: # ({new-7d39adb8})
##### `MaxLinesPerSecond`
The maximum number of new lines the agent will send per second to Zabbix server or proxy when processing 'log' and 'logrt' active checks. The provided value will be overridden by the 'maxlines' parameter, provided in the 'log' or 'logrt' item key.<br>*Note*: Zabbix will process 10 times more new lines than set in *MaxLinesPerSecond* to seek the required string in log items.

Default: `20` | Range: 1-1000

[comment]: # ({/new-7d39adb8})

[comment]: # ({new-8a5c9259})
##### `PidFile`
Name of the PID file.

Default: `/tmp/zabbix\_agentd.pid`

[comment]: # ({/new-8a5c9259})

[comment]: # ({new-b62afa60})
##### `RefreshActiveChecks`
How often the list of active checks is refreshed, in seconds. Note that after failing to refresh active checks the next refresh will be attempted in 60 seconds.

Default: `5` | Range: 1-86400

[comment]: # ({/new-b62afa60})

[comment]: # ({new-46329efe})
##### `Server`
A list of comma-delimited IP addresses, optionally in CIDR notation, or hostnames of Zabbix servers and Zabbix proxies. Incoming connections will be accepted only from the hosts listed here. If IPv6 support is enabled then '127.0.0.1', '::127.0.0.1', '::ffff:127.0.0.1' are treated equally and '::/0' will allow any IPv4 or IPv6 address. '0.0.0.0/0' can be used to allow any IPv4 address. Note, that "IPv4-compatible IPv6 addresses" (0000::/96 prefix) are supported but deprecated by [RFC4291](https://tools.ietf.org/html/rfc4291#section-2.5.5). Spaces are allowed.

Example: 

    Server=127.0.0.1,192.168.1.0/24,::1,2001:db8::/32,zabbix.domain

Mandatory: yes, if StartAgents is not explicitly set to 0

[comment]: # ({/new-46329efe})

[comment]: # ({new-d6a09b20})
##### `ServerActive`
Zabbix server/proxy address or cluster configuration to get active checks from. The server/proxy address is an IP address or DNS name and optional port separated by colon.<br>Cluster configuration is one or more server addresses separated by semicolon. Multiple Zabbix servers/clusters and Zabbix proxies can be specified, separated by comma. More than one Zabbix proxy should not be specified from each Zabbix server/cluster. If Zabbix proxy is specified then Zabbix server/cluster for that proxy should not be specified.<br>Multiple comma-delimited addresses can be provided to use several independent Zabbix servers in parallel. Spaces are allowed.<br>If the port is not specified, default port is used.<br>IPv6 addresses must be enclosed in square brackets if port for that host is specified. If port is not specified, square brackets for IPv6 addresses are optional.<br>If this parameter is not specified, active checks are disabled.

Example for Zabbix proxy: 

    ServerActive=127.0.0.1:10051

Example for multiple servers: 

    ServerActive=127.0.0.1:20051,zabbix.domain,\[::1\]:30051,::1,\[12fc::1\]

Example for high availability:

    ServerActive=zabbix.cluster.node1;zabbix.cluster.node2:20051;zabbix.cluster.node3

Example for high availability with two clusters and one server:

    ServerActive=zabbix.cluster.node1;zabbix.cluster.node2:20051,zabbix.cluster2.node1;zabbix.cluster2.node2,zabbix.domain

[comment]: # ({/new-d6a09b20})

[comment]: # ({new-020cc066})
##### `SourceIP`
The source IP address for:<br>- outgoing connections to Zabbix server or Zabbix proxy;<br>- making connections while executing some items (web.page.get, net.tcp.port, etc.).

[comment]: # ({/new-020cc066})

[comment]: # ({new-6e2d3567})
##### `StartAgents`
The number of pre-forked instances of zabbix\_agentd that process passive checks. If set to 0, passive checks are disabled and the agent will not listen on any TCP port.

Default: `3` | Range: 0-100

[comment]: # ({/new-6e2d3567})

[comment]: # ({new-ee64bcdf})
##### `Timeout`
Spend no more than Timeout seconds on processing.

Default: `3` | Range: 1-30

[comment]: # ({/new-ee64bcdf})

[comment]: # ({new-849f8496})
##### `TLSAccept`
What incoming connections to accept. Used for a passive checks. Multiple values can be specified, separated by comma:<br>*unencrypted* - accept connections without encryption (default)<br>*psk* - accept connections with TLS and a pre-shared key (PSK)<br>*cert* - accept connections with TLS and a certificate

Mandatory: yes, if TLS certificate or PSK parameters are defined (even for *unencrypted* connection); otherwise no

[comment]: # ({/new-849f8496})

[comment]: # ({new-336a620d})
##### `TLSCAFile`
The full pathname of the file containing the top-level CA(s) certificates for peer certificate verification, used for encrypted communications between Zabbix components.

[comment]: # ({/new-336a620d})

[comment]: # ({new-185ad3c6})
##### `TLSCertFile`
The full pathname of the file containing the agent certificate or certificate chain, used for encrypted communications with Zabbix components.

[comment]: # ({/new-185ad3c6})

[comment]: # ({new-3e9bf2b8})
##### `TLSCipherAll`
GnuTLS priority string or OpenSSL (TLS 1.2) cipher string. Override the default ciphersuite selection criteria for certificate- and PSK-based encryption.

Example:

    TLS\_AES\_256\_GCM\_SHA384:TLS\_CHACHA20\_POLY1305\_SHA256:TLS\_AES\_128\_GCM\_SHA256

[comment]: # ({/new-3e9bf2b8})

[comment]: # ({new-03e7f520})
##### `TLSCipherAll13`
Cipher string for OpenSSL 1.1.1 or newer in TLS 1.3. Override the default ciphersuite selection criteria for certificate- and PSK-based encryption.

Example for GnuTLS: 

    NONE:+VERS-TLS1.2:+ECDHE-RSA:+RSA:+ECDHE-PSK:+PSK:+AES-128-GCM:+AES-128-CBC:+AEAD:+SHA256:+SHA1:+CURVE-ALL:+COMP-NULL::+SIGN-ALL:+CTYPE-X.509

Example for OpenSSL: 

    EECDH+aRSA+AES128:RSA+aRSA+AES128:kECDHEPSK+AES128:kPSK+AES128

[comment]: # ({/new-03e7f520})

[comment]: # ({new-f7468437})
##### `TLSCipherCert`
GnuTLS priority string or OpenSSL (TLS 1.2) cipher string. Override the default ciphersuite selection criteria for certificate-based encryption.

Example for GnuTLS: 

    NONE:+VERS-TLS1.2:+ECDHE-RSA:+RSA:+AES-128-GCM:+AES-128-CBC:+AEAD:+SHA256:+SHA1:+CURVE-ALL:+COMP-NULL:+SIGN-ALL:+CTYPE-X.509

Example for OpenSSL: 

    EECDH+aRSA+AES128:RSA+aRSA+AES128

[comment]: # ({/new-f7468437})

[comment]: # ({new-717d9f51})
##### `TLSCipherCert13`
Cipher string for OpenSSL 1.1.1 or newer in TLS 1.3. Override the default ciphersuite selection criteria for certificate-based encryption.

[comment]: # ({/new-717d9f51})

[comment]: # ({new-3a633a0a})
##### `TLSCipherPSK`
GnuTLS priority string or OpenSSL (TLS 1.2) cipher string. Override the default ciphersuite selection criteria for PSK-based encryption.

Example for GnuTLS: 

    NONE:+VERS-TLS1.2:+ECDHE-PSK:+PSK:+AES-128-GCM:+AES-128-CBC:+AEAD:+SHA256:+SHA1:+CURVE-ALL:+COMP-NULL:+SIGN-ALL

Example for OpenSSL: 

    kECDHEPSK+AES128:kPSK+AES128

[comment]: # ({/new-3a633a0a})

[comment]: # ({new-8208c770})
##### `TLSCipherPSK13`
Cipher string for OpenSSL 1.1.1 or newer in TLS 1.3. Override the default ciphersuite selection criteria for PSK-based encryption.

Example:

    TLS_CHACHA20_POLY1305_SHA256:TLS_AES_128_GCM_SHA256

[comment]: # ({/new-8208c770})

[comment]: # ({new-b1151069})
##### `TLSConnect`
How the agent should connect to Zabbix server or proxy. Used for active checks. Only one value can be specified:<br>*unencrypted* - connect without encryption (default)<br>*psk* - connect using TLS and a pre-shared key (PSK)<br>*cert* - connect using TLS and a certificate

Mandatory: yes, if TLS certificate or PSK parameters are defined (even for *unencrypted* connection); otherwise no

[comment]: # ({/new-b1151069})

[comment]: # ({new-28aef301})
##### `TLSCRLFile`
The full pathname of the file containing revoked certificates. This parameter is used for encrypted communications between Zabbix components.

[comment]: # ({/new-28aef301})

[comment]: # ({new-cbcbba0a})
##### `TLSKeyFile`
The full pathname of the file containing the agent private key, used for encrypted communications between Zabbix components.

[comment]: # ({/new-cbcbba0a})

[comment]: # ({new-845f5b70})
##### `TLSPSKFile`
The full pathname of the file containing the agent pre-shared key, used for encrypted communications with Zabbix server.

[comment]: # ({/new-845f5b70})

[comment]: # ({new-b02e4942})
##### `TLSPSKIdentity`
The pre-shared key identity string, used for encrypted communications with Zabbix server.

[comment]: # ({/new-b02e4942})

[comment]: # ({new-7a78525c})
##### `TLSServerCertIssuer`
The allowed server (proxy) certificate issuer.

[comment]: # ({/new-7a78525c})

[comment]: # ({new-c8d1ac8b})
##### `TLSServerCertSubject`
The allowed server (proxy) certificate subject.

[comment]: # ({/new-c8d1ac8b})

[comment]: # ({new-676eb24d})
##### `UnsafeUserParameters`
Allow all characters to be passed in arguments to user-defined parameters. The following characters are not allowed: \\ ' " \` \* ? \[ \] { } \~ $ ! & ; ( ) < > \| \# @<br>Additionally, newline characters are not allowed.

Default: `0` | Values: 0 - do not allow, 1 - allow

[comment]: # ({/new-676eb24d})

[comment]: # ({new-e9779b31})
##### `User`
Drop privileges to a specific, existing user on the system.<br>Only has effect if run as 'root' and AllowRoot is disabled.

Default: `zabbix`

[comment]: # ({/new-e9779b31})

[comment]: # ({new-1591e129})
##### `UserParameter`
A user-defined parameter to monitor. There can be several user-defined parameters.<br>Format: UserParameter=<key>,<shell command><br>Note that the shell command must not return empty string or EOL only. Shell commands may have relative paths, if the UserParameterDir parameter is specified.

Example:

    UserParameter=system.test,who\|wc -l
    UserParameter=check\_cpu,./custom\_script.sh

[comment]: # ({/new-1591e129})

[comment]: # ({new-493da8fd})
##### `UserParameterDir`
The default search path for UserParameter commands. If used, the agent will change its working directory to the one specified here before executing a command. Thereby, UserParameter commands can have a relative `./` prefix instead of a full path.<br>Only one entry is allowed.

Example:

    UserParameterDir=/opt/myscripts

[comment]: # ({/new-493da8fd})

[comment]: # ({new-9170c364})
#### See also

1.  [Differences in the Zabbix agent configuration for active and
    passive checks starting from version
    2.0.0](http://blog.zabbix.com/multiple-servers-for-active-agent-sure/858)

[comment]: # ({/new-9170c364})
