[comment]: # attributes: notoc

[comment]: # translation:outdated

[comment]: # ({new-dbb2a1f5})
# 1 Zabbix сервер

::: noteclassic
Значения по умолчанию являются умолчаниями демона, но не
значениями которые указаны в поставляемых файлах
конфигурации.
:::

Поддерживаемые параметры в файле конфигурации Zabbix сервера:

|Параметр|Обязательный|Диапазон|Умолчание|Описание|
|----------------|------------------------|----------------|------------------|----------------|
|AlertScriptsPath|нет|<|/usr/local/share/zabbix/alertscripts|Размещение пользовательских скриптов оповещений (зависит от указанной при компиляции переменной *datadir*).|
|AllowRoot|нет|<|0|Разрешение серверу запускаться под "root". Если отключено и сервер запускается из под "root", сервер попробует переключиться на пользователя "zabbix". Не имеет смысла, если сервер запускается под обычным пользователем.<br>0 - не разрешено<br>1 - разрешено<br>Данный параметр поддерживается с начиная с Zabbix 2.2.0.|
|CacheSize|нет|128K-8G|8М|Размер кэша конфигурации, в байтах.<br>Размер распределенной памяти для хранения данных узлов сети, элементов данных и триггеров.<br>Максимальный предел был 2G до Zabbix 2.2.3.|
|CacheUpdateFrequency|нет|1-3600|60|Как часто Zabbix будет выполнять процедуру обновления кэша конфигурации, в секундах.<br>Смотрите также опции [административных функций](/ru/manual/concepts/server#процесс_сервера).|
|DBHost|нет|<|localhost|Имя хоста базы данных.<br>В случае localhost или пустой строки для MySQL, будет использоваться сокет. В случае PostgreSQL<br>только при пустой строке будет произведена попытка использовать сокет.|
|DBName|да|<|<|Имя базы данных.|
|DBPassword|нет|<|<|Пароль к базе данных.<br>Закомментируйте эту строку, если пароль не используется.|
|DBPort|нет|1024-65535|<|Порт базы данных, когда не используется локальный сокет.|
|DBSchema|нет|<|<|Имя схемы. Используется для IBM DB2 и PostgreSQL.|
|DBSocket|нет|<|<|Путь к сокету MySQL.|
|DBUser|нет|<|<|Пользователь базы данных.|
|DebugLevel|нет|0-5|3|Задает уровень журналирования:<br>0 - основная информация о запуске и остановки процессов Zabbix<br>1 - критичная информация<br>2 - информация об ошибках<br>3 - предупреждения<br>4 - для отладки (записывается очень много информации)<br>5 - расширенная отладка (записывается еще больше информации).<br>Смотрите также опции [административных функций](/ru/manual/concepts/server#процесс_сервера).|
|ExportDir|нет|<|<|Директория для [экспорта в режиме реального времени](/ru/manual/appendix/install/real_time_export) событий, истории и динамики изменений в JSON формате с разделением новой строкой. Если задано, активируется экспорт в режиме реального времени.<br>Этот параметр поддерживается начиная с Zabbix 4.0.0.|
|ExportFileSize|нет|1M-1G|1G|Максимальный размер каждого файла с экспортированными данными в байтах. Используется только для ротации, если задан параметр ExportDir.<br>Этот параметр поддерживается начиная с Zabbix 4.0.0.|
|ExternalScripts|нет|<|/usr/local/share/zabbix/externalscripts|Размещение внешних скриптов (зависит от указанной при компиляции переменной *datadir*).|
|Fping6Location|нет|<|/usr/sbin/fping6|Размещение fping6.<br>Убедитесь, что владельцем бинарного файла fping6 является root и флаг SUID установлен.<br>Оставьте пустым ("Fping6Location="), если ваша утилита fping поддерживает обработку IPv6 адресов.|
|FpingLocation|нет|<|/usr/sbin/fping|Размещение fping.<br>Убедитесь, что владельцем бинарного файла fping является root и флаг SUID установлен!|
|HistoryCacheSize|нет|128K-2G|16M|Размер кэша истории, в байтах.<br>Размер разделяемой памяти для хранения данных истории.|
|HistoryIndexCacheSize|нет|128K-2G|4M|Размер кэша индекса истории, в байтах.<br>Размер разделяемой памяти для индексации данных записываемой истории в кэш истории.<br>Кэшу индекса необходимо около 100 байт на кэширование одного элемента данных.<br>Этот параметр поддерживается начиная с Zabbix 3.0.0.|
|HistoryStorageDateIndex|нет|<|0|Включение предварительной обработки значений истории в хранилище истории для сохранения значений в разных индексах на основе даты:<br>0 - включение<br>1 - выключение|
|HistoryStorageURL|нет|<|<|HTTP\[S\] URL хранилища истории.<br>Этот параметр используется для установки [Elasticsearch](/ru/manual/appendix/install/elastic_search_setup).|
|HistoryStorageTypes|нет|<|uint,dbl,str,log,text|Список, разделенный запятыми, типов значений для отправки в хранилище истории.<br>Этот параметр используется для установки [Elasticsearch](/ru/manual/appendix/install/elastic_search_setup).|
|HousekeepingFrequency|no|0-24|1|Как часто Zabbix будет выполнять процедуру очистки базы (в часах).<br>Автоматическая очистка базы данных удаляет устаревшую информацию из базы данных.<br>*Обратите внимание*: Для предотвращения перегрузки функции очистки (к примеру, когда периоды хранения данных истории и динамики изменения сильно занижены), не более чем 4 периода кратных HousekeepingFrequency часов устаревшей истории будет удалено за один цикл очистки по каждому элементу данных. Таким образом, если HousekeepingFrequency равен 1 часу, то за один цикл будет удалено не более чем 4 часа устаревшей истории (начиная с самой старейшей записи).<br>*На заметку*: Для снижения нагрузки на сервер запуск процессе очистки отложен на 30 минут после запуска сервера. Таким образом, если HousekeepingFrequency равен 1 часу, самая первая процедура очистки запустится через 30 минут после запуска сервера, и затем повторится с задержкой в один час. Такое поведение отсрочки введено начиная с Zabbix 2.4.0.<br>Начиная с Zabbix 3.0.0 имеется возможность отключить автоматическую очистку истории, указав HousekeepingFrequency равным 0. В этом случае процедуру очистки истории можно запустить только с помощью опции контроля управления *housekeeper\_execute* и периодом удаления устаревшей информации является 4 кратный период начиная с последнего цикла удаления истории, но не менее чем 4 часа и не более 4 дней.<br>Смотрите также опции [административных функций](/ru/manual/concepts/server#процесс_сервера).|
|Include|нет|<|<|Вы можете включить отдельные файлы или все файлы из папки с файлом конфигурации.<br>Для включения только необходимых файлов из указанной папки, поддерживается символ звездочки для поиска совпадения по маске. Например: `/абсолютный/путь/к/файлам/конфигурации/*.conf`. Совпадение с маской поддерживается начиная с Zabbix 2.4.0.<br>Смотрите [специальные заметки](special_notes_include) по поводу ограничений.|
|JavaGateway|нет|<|<|IP адрес (или имя хоста) Zabbix Java gateway.<br>Требуется только, если запущены Java поллеры.<br>Этот параметр поддерживается начиная с Zabbix 2.0.0.|
|JavaGatewayPort|нет|1024-32767|10052|Порт, который слушает Zabbix Java gateway.<br>Этот параметр поддерживается начиная с Zabbix 2.0.0.|
|ListenIP|no|<|0.0.0.0|Список IP адресов разделенных запятыми, которые должен слушать траппер.<br>Траппер будет слушать все сетевые интерфейсы, если этот параметр не указан.<br>Список из нескольких IP адресов поддерживается начиная с Zabbix 1.8.3.|
|ListenPort|нет|1024-32767|10051|Порт, который слушает траппер.|
|LoadModule|нет|<|<|Модули, которые загружаются во время старта. Модули используются для расширения возможностей сервера.<br>Формат: Loadmodule=<module.so><br>Модули должны находиться в папке указанной в параметре LoadModulePath.<br>Допускается добавлять несколько параметров LoadModule.|
|LoadModulePath|нет|<|<|Абсолютный путь к папке с серверными модулями.<br>По умолчанию зависит от опций компиляции.|
|LogFile|да, если LogType задан как *file*, иначе<br>нет.|<|<|Имя файла журнала.|
|LogFileSize|нет|0-1024|1|Максимальный размер файла журнала в МБ.<br>0 - отключение автоматической ротации журнала.<br>*Примечание*: Если лимит достигнут и ротация не удалась, по каким-либо причинам, существующий файл журнала очищается и начинается новый.|
|LogType|нет|<|file|Тип вывода журнала:<br>*file* - запись журнала в файл указанный в LogFile параметре,<br>*system* - запись журнала в syslog,<br>*console* - вывод журнала в стандартный вывод.<br>Этот параметр поддерживается начиная с Zabbix 3.0.0.|
|LogSlowQueries|нет|0-3600000|0|Как долго могут выполняться запросы в базу данных до того как они запишутся в журнал (в миллисекундах).<br>0 - не журналировать медленные запросы.<br>Эта опция активируется начиная с DebugLevel=3.<br>Этот параметр поддерживается начиная с Zabbix 1.8.2.|
|MaxHousekeeperDelete|нет|0-1000000|5000|Удалять не более 'MaxHousekeeperDelete' строк (в соответствии с \[имя\_таблицы\], \[поле\], \[значение\]) за одну задачу за один цикл автоматической очистки базы данных.<br>Если значение равно 0, то тогда ограничение не используется вовсе. В этом случае вы должны знать, что вы делаете!<br>Этот параметр поддерживается начиная с Zabbix 1.8.2 и применяется только для удаления данных истории и динамики изменений уже удаленных элементов данных.|
|PidFile|нет|<|/tmp/zabbix\_server.pid|Имя PID файла.|
|ProxyConfigFrequency|нет|1-604800|3600|Как часто Zabbix сервер будет отправлять данные конфигурации на Zabbix прокси в секундах. Используется только с прокси, который работает в пассивном режиме.<br>Этот параметр поддерживается начиная с Zabbix 1.8.3.|
|ProxyDataFrequency|нет|1-3600|1|Как часто Zabbix сервер будет запрашивать данные истории с Zabbix прокси в секундах. Используется только с прокси, который работает в пассивном режиме.<br>Этот параметр поддерживается начиная с Zabbix 1.8.3.|
|SNMPTrapperFile|нет|<|/tmp/zabbix\_traps.tmp|Временный файл, используемый для передачи данных серверу от демона SNMP trap.<br>Должен быть таким же, как и в zabbix\_trap\_receiver.pl или в файле конфигурации SNMPTT.<br>Этот параметр поддерживается начиная с Zabbix 2.0.0.|
|SocketDir|нет|<|/tmp|Папка для хранения IPC сокетов, которые используются внутренними сервисами Zabbix.<br>Этот параметр поддерживается начиная с Zabbix 3.4.0.|
|SourceIP|нет|<|<|Локальный IP адрес для исходящих подключений.|
|SSHKeyLocation|нет|<|<|Размещение публичных и приватных ключей для SSH проверок и действий|
|SSLCertLocation|нет|<|<|Размещение файлов клиентских SSL сертификатов для аутентификации клиента.<br>Этот параметр используется только в веб-мониторинге и поддерживается начиная с Zabbix 2.4.|
|SSLKeyLocation|нет|<|<|Размещение файлов приватных SSL ключей для аутентификации клиента.<br>Этот параметр используется только в веб-мониторинге и поддерживается начиная с Zabbix 2.4.|
|SSLCALocation|нет|<|<|Переопределение расположения файлов центра сертификации (CA) для верификации SSL сертификатов сервера. Если не задано, будет использоваться общесистемная папка.<br>Обратите внимание, что значение этого параметра задаст CURLOPT\_CAPATH опцию libcurl. Для libcurl версий до 7.42.0, эта опция имеет эффект только, если libcurl скомпилирован для использования OpenSSL. Для получения более подробной информации смотрите [веб-страницу cURL](http://curl.haxx.se/libcurl/c/CURLOPT_CAPATH.html).<br>Этот параметр используется в веб-мониторинге начиная с Zabbix 2.4.0 и при аутентификации SMTP начиная с Zabbix 3.0.0.|
|StartDBSyncers|нет|1-100|4|Количество экземпляров пре-форков DB Syncers.<br>Максимальное количество 64 до версии 1.8.5.<br>Этот параметр поддерживается начиная с Zabbix 1.8.3.|
|StartAlerters|no|1-100|3|Количество экземпляров пре-форков процессов оповещений.<br>Этот параметр поддерживается начиная с Zabbix 3.4.0.|
|StartDiscoverers|нет|0-250|1|Количество экземпляров пре-форков автообнаружения.<br>Максимальное количество 255 до версии 1.8.5.|
|StartEscalators|нет|1-100|1|Количество экземпляров пре-форков эскалаторов.<br>Этот параметр поддерживается начиная с Zabbix 3.0.0.|
|StartHTTPPollers|нет|0-1000|1|Количество экземпляров пре-форков HTTP поллеров**^[1](zabbix_server#примечания)^**.<br>Максимальное количество 255 до версии 1.8.5.|
|StartIPMIPollers|нет|0-1000|0|Количество экземпляров пре-форков IPMI поллеров.<br>Максимальное количество 255 до версии 1.8.5.|
|StartJavaPollers|нет|0-1000|0|Количество экземпляров пре-форков Java поллеров**^[1](zabbix_server#примечания)^**.<br>Этот параметр поддерживается начиная с Zabbix 2.0.0.|
|StartPingers|нет|0-1000|1|Количество экземпляров пре-форков ICMP pingers**^[1](zabbix_server#примечания)^**.<br>Максимальное количество 255 до версии 1.8.5.|
|StartPollersUnreachable|no|0-1000|1|Количество экземпляров пре-форков поллеров для недоступных узлов сети (включая IPMI и Java)**^[1](zabbix_server#примечания)^**.<br>Начиная с Zabbix 2.4.0, по крайне один поллер для недоступных хостов должен быть запущен, если обычные поллеры, IPMI или Java запускаются.<br>Максимальное количество 255 до версии 1.8.5.<br>Этот параметр опущен в версии 1.8.3.|
|StartPollers|нет|0-1000|5|Количество экземпляров пре-форков поллеров**^[1](zabbix_server#примечания)^**.<br>*Обратите внимание*, что ненулевое значение требуется для работы внутренних, агрегированных и вычисляемых элементов данных.|
|StartPreprocessors|нет|1-1000|3|Количество экземпляров пре-форков "worker" предварительной обработки**^[1](zabbix_server#примечания)^**.<br>Менеджер предобработки запускается автоматически, когда запускается "worker" предварительной обработки.<br>Этот параметр поддерживается начиная с Zabbix 3.4.0.|
|StartProxyPollers|нет|0-250|1|Количество экземпляров пре-форков поллеров для работы с пассивными прокси**^[1](zabbix_server#примечания)^**.<br>Максимальное количество 255 до версии 1.8.5.<br>Этот параметр поддерживается начиная с Zabbix 1.8.3.|
|StartSNMPTrapper|нет|0-1|0|Если значение равно 1, будет запущен процесс SNMP траппера.<br>Этот параметр поддерживается начиная с Zabbix 2.0.0.|
|StartTimers|нет|1-1000|1|Количество экземпляров пре-форков таймера.<br>Таймеры обрабатывают периоды обслуживания.<br>Этот параметр поддерживается начиная с Zabbix 2.2.0.|
|StartTrappers|нет|0-1000|5|Количество экземпляров пре-форков трапперов**^[1](zabbix_server#примечания)^**.<br>Трапперы принимают входящие подключения от Zabbix sender, активных агентов, активных прокси и дочерних нод.<br>По крайней мере один процесс траппера должен быть запущен для отображения доступности сервера и просмотра очереди в веб-интерфейсе.<br>Максимальное количество 255 до версии 1.8.5.|
|StartVMwareCollectors|нет|0-250|0|Количество экземпляров пре-форков vmware коллекторов.<br>Этот параметр поддерживается начиная с версии Zabbix 2.2.0.|
|Timeout|нет|1-30|3|Указывает как долго мы ждем ответа от агента, SNMP устройства или внешней проверки (в секундах).|
|TLSCAFile|нет|<|<|Абсолютный путь к файлу, который содержит сертификаты верхнего уровня CA(и) для верификации сертификата узла, используется для зашифрованных соединений между Zabbix компонентами.<br>Этот параметр поддерживается начиная с Zabbix 3.0.0.|
|TLSCertFile|нет|<|<|Абсолютный путь к файлу, который содержит сертификат или цепочку сертификатов, используется для зашифрованных соединений между Zabbix компонентами.<br>Этот параметр поддерживается начиная с Zabbix 3.0.0.|
|TLSCRLFile|нет|<|<|Абсолютный путь к файлу, который содержит отозванные сертификаты. Этот параметр используется для зашифрованных соединений между Zabbix компонентами.<br>Этот параметр поддерживается начиная с Zabbix 3.0.0.|
|TLSKeyFile|нет|<|<|Абсолютный путь к файлу, который содержит приватный ключ сервера, используется для зашифрованных соединений между Zabbix компонентами.<br>Этот параметр поддерживается начиная с Zabbix 3.0.0.|
|TmpDir|нет|<|/tmp|Папка с временными данными.|
|TrapperTimeout|нет|1-300|300|Указывает, как много секунд траппер может потратить на обработку новых данных.|
|TrendCacheSize|нет|128K-2G|4M|Размер кэша динамики изменений, в байтах.<br>Размер разделяемой памяти для хранения данных динамики изменений.|
|UnavailableDelay|нет|1-3600|60|Как часто узел сети будет проверяться на доступность в период его [недоступности](/ru/manual/appendix/items/unreachability#недоступный_узел_сети), в секундах.|
|UnreachableDelay|нет|1-3600|15|Как часто узел сети будет проверяться на доступность в период его [недостижимости](/ru/manual/appendix/items/unreachability#недостижимый_узел_сети), в секундах.|
|UnreachablePeriod|нет|1-3600|45|Через сколько секунд [недостижимости](/ru/manual/appendix/items/unreachability#недостижимый_узел_сети) узел сети считается недоступным.|
|User|нет|<|zabbix|Использование привилегий указанного, существующего пользователя системы.<br>Имеет эффект только, если запускается под 'root' и AllowRoot отключен.<br>Этот параметр поддерживается начиная с Zabbix 2.4.0.|
|ValueCacheSize|нет|0,128К - 64G|8M|Размер кэша для хранения истории значений в байтах.<br>Разделяемая память для кэширования запросов к данным истории элементов данных.<br>Значение 0 отключит кэш значений (не рекомендуется).<br>В случае, если кэш значений исчерпает разделяемую память, предупреждающее сообщение будет записано в журнал сервера каждые 5 минут.<br>Этот параметр поддерживается начиная с Zabbix 2.2.0.|
|VMwareCacheSize|нет|256K - 2G|8M|Разделяемая память для хранения данных VMware.<br>Можно использовать внутренние проверки VMware - zabbix\[vmware,buffer,…\] для мониторинга использования кэша VMware (смотрите раздел о [внутренних проверках](/ru/manual/config/items/itemtypes/internal))<br>Заметьте, что разделяемая память не выделяется, если экземпляры VMware коллекторов не настроены на запуск.<br>Этот параметр поддерживается начиная с Zabbix 2.2.0.|
|VMwareFrequency|нет|10-86400|60|Задержка в секундах между сбором данных с одной службы VMware.<br>Это значение необходимо установить наименьшим значением интервала обновления у любого элемента данных VMware мониторинга.<br>Этот параметр поддерживается начиная с Zabbix 2.2.0.|
|VMwarePerfFrequency|нет|10-86400|60|Задержка в секундах между получением статистики по счётчикам производительности с одной службы VMware.<br>Это значение необходимо установить наименьшим значением интервала обновления у любого [элемента данных](/ru/manual/config/items/itemtypes/simple_checks/vmware_keys#примечания) VMware мониторинга, который использует счётчики производительности VMware.<br>Этот параметр поддерживается начиная с Zabbix 2.2.9, 2.4.4.|
|VMwareTimeout|нет|1-300|10|Максимальное количество секунд, которое коллектор vmware потратит на ожидание ответа со службы VMware (vCenter или ESX гипервизора).<br>Этот параметр поддерживается начиная с Zabbix 2.2.9, 2.4.4.|

[comment]: # ({/new-dbb2a1f5})

[comment]: # ({new-16f1be5f})
##### Примечания

::: notewarning
**(1)** Обратите внимание, что слишком большое
количество процессов обработки данных (поллеров, поллеров недоступных
устройств, HTTP поллеров, Java поллеров, пингеров, трапперов, прокси
поллеров) вместе с IPMI менеджером, SNMP траппером и "workers"
предварительной обработки, могут исчерпать ограничение количества
файловых дескрипторов по отдельным процессам для менеджера
предварительной обработки. Что заставит Zabbix сервер остановиться
(обычно в течении короткого периода времени после запуска, но иногда
может занять более длительное время). Файл конфигурации необходимо
пересмотреть или лимит должен быть увеличен, чтобы избежать подобной
ситуации.
:::

::: noteclassic
Zabbix поддерживает файлы конфигурации только в кодировке
UTF-8 без
[BOM](https://ru.wikipedia.org/wiki/Маркер_последовательности_байтов).\
\
Комментарии, начинающиеся с "\#", поддерживаются только в начале
строки.
:::

[comment]: # ({/new-16f1be5f})

[comment]: # ({new-6428a44e})
### Parameter details

[comment]: # ({/new-6428a44e})




[comment]: # ({new-e273f2ed})
##### `AlertScriptsPath`
The location of custom alert scripts (depends on the *datadir* compile-time installation variable).

Default: `/usr/local/share/zabbix/alertscripts`

[comment]: # ({/new-e273f2ed})

[comment]: # ({new-1edb700a})
##### `AllowRoot`
Allow the server to run as 'root'. If disabled and the server is started by 'root', the server will try to switch to the 'zabbix' user instead. Has no effect if started under a regular user.

Default: `0` | Values: 0 - do not allow; 1 - allow

[comment]: # ({/new-1edb700a})

[comment]: # ({new-37f8828a})
##### `AllowUnsupportedDBVersions`
Allow the server to work with unsupported database versions.

Default: `0` | Values: 0 - do not allow; 1 - allow

[comment]: # ({/new-37f8828a})

[comment]: # ({new-6c704994})
##### `CacheSize`
The size of the configuration cache, in bytes. The shared memory size for storing host, item and trigger data.

Default: `32M` | Range: 128K-64G

[comment]: # ({/new-6c704994})

[comment]: # ({new-2c956137})
##### `CacheUpdateFrequency`
This parameter determines how often Zabbix will perform the configuration cache update in seconds. See also [runtime control](/manual/concepts/server#runtime-control) options.

Default: `10` | Range: 1-3600

[comment]: # ({/new-2c956137})

[comment]: # ({new-d92a7b2b})
##### `DBHost`
The database host name.<br>With MySQL `localhost` or empty string results in using a socket. With PostgreSQL only empty string results in attempt to use socket. With [Oracle](/manual/appendix/install/oracle#connection_set_up) empty string results in using the Net Service Name connection method; in this case consider using the TNS\_ADMIN environment variable to specify the directory of the tnsnames.ora file.

Default: `localhost`

[comment]: # ({/new-d92a7b2b})

[comment]: # ({new-1058c650})
##### `DBName`
The database name.<br>With [Oracle](/manual/appendix/install/oracle#connection_set_up), if the Net Service Name connection method is used, specify the service name from tnsnames.ora or set to empty string; set the TWO\_TASK environment variable if DBName is set to empty string.

Mandatory: Yes

[comment]: # ({/new-1058c650})

[comment]: # ({new-479ee610})
##### `DBPassword`
The database password. Comment this line if no password is used.

[comment]: # ({/new-479ee610})

[comment]: # ({new-1b941880})
##### `DBPort`
The database port when not using local socket.<br>With [Oracle](/manual/appendix/install/oracle#connection_set_up), if the Net Service Name connection method is used, this parameter will be ignored; the port number from the tnsnames.ora file will be used instead.

Range: 1024-65535

[comment]: # ({/new-1b941880})

[comment]: # ({new-abcff68d})
##### `DBSchema`
The database schema name. Used for PostgreSQL.

[comment]: # ({/new-abcff68d})

[comment]: # ({new-3a14461c})
##### `DBSocket`
The path to the MySQL socket file.

[comment]: # ({/new-3a14461c})

[comment]: # ({new-94782f63})
##### `DBUser`
The database user.

[comment]: # ({/new-94782f63})

[comment]: # ({new-500737d5})
##### `DBTLSConnect`
Setting this option to the following values enforces to use a TLS connection to the database:<br>*required* - connect using TLS<br>*verify\_ca* - connect using TLS and verify certificate<br>*verify\_full* - connect using TLS, verify certificate and verify that database identity specified by DBHost matches its certificate<br><br>With MySQL, starting from 5.7.11, and PostgreSQL the following values are supported: `required`, `verify\_ca`, `verify\_full`.<br>With MariaDB, starting from version 10.2.6, the `required` and `verify\_full` values are supported.<br>By default not set to any option and the behavior depends on database configuration.

[comment]: # ({/new-500737d5})

[comment]: # ({new-fc356178})
##### `DBTLSCAFile`
The full pathname of a file containing the top-level CA(s) certificates for database certificate verification.

Mandatory: no (yes, if DBTLSConnect set to *verify\_ca* or *verify\_full*)

[comment]: # ({/new-fc356178})

[comment]: # ({new-5f286da4})
##### `DBTLSCertFile`
The full pathname of a file containing the Zabbix server certificate for authenticating to database.

[comment]: # ({/new-5f286da4})

[comment]: # ({new-dd8b56f5})
##### `DBTLSKeyFile`
The full pathname of a file containing the private key for authenticating to database.

[comment]: # ({/new-dd8b56f5})

[comment]: # ({new-d8056d7a})
##### `DBTLSCipher`
The list of encryption ciphers that Zabbix server permits for TLS protocols up through TLS v1.2. Supported only for MySQL.

[comment]: # ({/new-d8056d7a})

[comment]: # ({new-a327c2f2})
##### `DBTLSCipher13`
The list of encryption ciphersuites that Zabbix server permits for the TLS v1.3 protocol. Supported only for MySQL, starting from version 8.0.16.

[comment]: # ({/new-a327c2f2})

[comment]: # ({new-d82f69fb})
##### `DebugLevel`
Specify the debug level:<br>*0* - basic information about starting and stopping of Zabbix processes<br>*1* - critical information;<br>*2* - error information;<br>*3* - warnings;<br>*4* - for debugging (produces lots of information);<br>*5* - extended debugging (produces even more information).<br>See also [runtime control](/manual/concepts/server#server_process) options.

Default: `3` | Range: 0-5

[comment]: # ({/new-d82f69fb})

[comment]: # ({new-99815de4})
##### `ExportDir`
The directory for [real-time export](/manual/appendix/install/real_time_export) of events, history and trends in newline-delimited JSON format. If set, enables the real-time export.

[comment]: # ({/new-99815de4})

[comment]: # ({new-965e757e})
##### `ExportFileSize`
The maximum size per export file in bytes. Used for rotation if `ExportDir` is set.

Default: `1G` | Range: 1M-1G

[comment]: # ({/new-965e757e})

[comment]: # ({new-5447304d})
##### `ExportType`
The list of comma-delimited entity types (events, history, trends) for [real-time export](/manual/appendix/install/real_time_export) (all types by default). Valid only if ExportDir is set.<br>*Note* that if ExportType is specified, but ExportDir is not, then this is a configuration error and the server will not start.

Example for history and trends export:

    ExportType=history,trends

Example for event export only:

    ExportType=events

[comment]: # ({/new-5447304d})

[comment]: # ({new-d0a46edf})
##### `ExternalScripts`
The location of external scripts (depends on the `datadir` compile-time installation variable).

Default: `/usr/local/share/zabbix/externalscripts`

[comment]: # ({/new-d0a46edf})

[comment]: # ({new-bdd6796f})
##### `Fping6Location`
The location of fping6. Make sure that the fping6 binary has root ownership and the SUID flag set. Make empty ("Fping6Location=") if your fping utility is capable to process IPv6 addresses.

Default: `/usr/sbin/fping6`

[comment]: # ({/new-bdd6796f})

[comment]: # ({new-025648b5})
##### `FpingLocation`
The location of fping. Make sure that the fping binary has root ownership and the SUID flag set.

Default: `/usr/sbin/fping`

[comment]: # ({/new-025648b5})

[comment]: # ({new-8bb05e99})
##### `HANodeName`
The high availability cluster node name. When empty the server is working in standalone mode and a node with empty name is created.

[comment]: # ({/new-8bb05e99})

[comment]: # ({new-c802a26c})
##### `HistoryCacheSize`
The size of the history cache, in bytes. The shared memory size for storing history data.

Default: `16M` | Range: 128K-2G

[comment]: # ({/new-c802a26c})

[comment]: # ({new-2753befa})
##### `HistoryIndexCacheSize`
The size of the history index cache, in bytes. The shared memory size for indexing the history data stored in history cache. The index cache size needs roughly 100 bytes to cache one item.

Default: `4M` | Range: 128K-2G

[comment]: # ({/new-2753befa})

[comment]: # ({new-b5dc4ecc})
##### `HistoryStorageDateIndex`
Enable preprocessing of history values in history storage to store values in different indices based on date.

Default: `0` | Values: 0 - disable; 1 - enable

[comment]: # ({/new-b5dc4ecc})

[comment]: # ({new-08949761})
##### `HistoryStorageURL`
The history storage HTTP\[S\] URL. This parameter is used for [Elasticsearch](/manual/appendix/install/elastic_search_setup) setup.

[comment]: # ({/new-08949761})

[comment]: # ({new-131c3d46})
##### `HistoryStorageTypes`
A comma-separated list of value types to be sent to the history storage. This parameter is used for [Elasticsearch](/manual/appendix/install/elastic_search_setup) setup.

Default: `uint,dbl,str,log,text`

[comment]: # ({/new-131c3d46})

[comment]: # ({new-42928b03})
##### `HousekeepingFrequency`
This parameter determines how often Zabbix will perform the housekeeping procedure in hours. Housekeeping is removing outdated information from the database.<br>*Note*: To prevent housekeeper from being overloaded (for example, when history and trend periods are greatly reduced), no more than 4 times HousekeepingFrequency hours of outdated information are deleted in one housekeeping cyc,le, for each item. Thus, if HousekeepingFrequency is 1, no more than 4 hours of outdated information (starting from the oldest entry) will be deleted per cycle.<br>*Note*: To lower load on server startup housekeeping is postponed for 30 minutes after server start. Thus, if HousekeepingFrequency is 1, the very first housekeeping procedure after server start will run after 30 minutes, and will repeat with one hour delay thereafter.<br>It is possible to disable automatic housekeeping by setting HousekeepingFrequency to 0. In this case the housekeeping procedure can only be started by *housekeeper\_execute* runtime control option and the period of outdated information deleted in one housekeeping cycle is 4 times the period since the last housekeeping cycle, but not less than 4 hours and not greater than 4 days.<br>See also [runtime control](/manual/concepts/server#server_process) options.

Default: `1` | Range: 0-24

[comment]: # ({/new-42928b03})

[comment]: # ({new-5836e8ba})
##### `Include`
You may include individual files or all files in a directory in the configuration file. To only include relevant files in the specified directory, the asterisk wildcard character is supported for pattern matching. See [special notes](special_notes_include) about limitations.

Example:

    Include=/absolute/path/to/config/files/*.conf

[comment]: # ({/new-5836e8ba})

[comment]: # ({new-97c00865})
##### `JavaGateway`
The IP address (or hostname) of Zabbix Java gateway. Only required if Java pollers are started.

[comment]: # ({/new-97c00865})

[comment]: # ({new-fc4d246a})
##### `JavaGatewayPort`
The port that Zabbix Java gateway listens on.

Default: `10052` | Range: 1024-32767

[comment]: # ({/new-fc4d246a})

[comment]: # ({new-22241946})
##### `ListenBacklog`
The maximum number of pending connections in the TCP queue.<br>The default value is a hard-coded constant, which depends on the system.<br>The maximum supported value depends on the system, too high values may be silently truncated to the 'implementation-specified maximum'.

Default: `SOMAXCONN` | Range: 0 - INT\_MAX

[comment]: # ({/new-22241946})

[comment]: # ({new-33f1aea9})
##### `ListenIP`
A list of comma-delimited IP addresses that the trapper should listen on.<br>Trapper will listen on all network interfaces if this parameter is missing.

Default: `0.0.0.0`

[comment]: # ({/new-33f1aea9})

[comment]: # ({new-98e0be1b})
##### `ListenPort`
The listen port for trapper.

Default: `10051` | Range: 1024-32767

[comment]: # ({/new-98e0be1b})

[comment]: # ({new-55309e9c})
##### `LoadModule`
The module to load at server startup. Modules are used to extend the functionality of the server.  The module must be located in the directory specified by LoadModulePath or the path must precede the module name. If the preceding path is absolute (starts with '/') then LoadModulePath is ignored.<br>Formats:<br>LoadModule=<module.so><br>LoadModule=<path/module.so><br>LoadModule=</abs\_path/module.so><br>It is allowed to include multiple LoadModule parameters.

[comment]: # ({/new-55309e9c})

[comment]: # ({new-0c0c0723})
##### `LoadModulePath`
Full path to location of server modules.<br>Default depends on compilation options.

[comment]: # ({/new-0c0c0723})

[comment]: # ({new-c650f3c9})
##### `LogFile`
Name of the log file.

Mandatory: Yes, if LogType is set to *file*; otherwise no

[comment]: # ({/new-c650f3c9})

[comment]: # ({new-9c05648a})
##### `LogFileSize`
Maximum size of the log file in MB.<br>0 - disable automatic log rotation.<br>*Note*: If the log file size limit is reached and file rotation fails, for whatever reason, the existing log file is truncated and started anew.

Default: `1` | Range: 0-1024 | Mandatory: Yes, if LogType is set to *file*; otherwise no

[comment]: # ({/new-9c05648a})

[comment]: # ({new-2f20d22f})
##### `LogSlowQueries`
Determines how long a database query may take before being logged in milliseconds.<br>0 - don't log slow queries.<br>This option becomes enabled starting with DebugLevel=3.

Default: `0` | Range: 0-3600000

[comment]: # ({/new-2f20d22f})

[comment]: # ({new-926c45e2})
##### `LogType`
Type of the log output:<br>*file* - write log to file specified by LogFile parameter;<br>*system* - write log to syslog;<br>*console* - write log to standard output.

Default: `file`

[comment]: # ({/new-926c45e2})

[comment]: # ({new-124ae689})
##### `MaxHousekeeperDelete`
No more than 'MaxHousekeeperDelete' rows (corresponding to \[tablename\], \[field\], \[value\]) will be deleted per one task in one housekeeping cycle.<br>If set to 0 then no limit is used at all. In this case you must know what you are doing, so as not to [overload the database!](zabbix_server#footnotes) **^[2](zabbix_server#footnotes)^**<br>This parameter applies only to deleting history and trends of already deleted items.

Default: `5000` | Range: 0-1000000

[comment]: # ({/new-124ae689})

[comment]: # ({new-9cfb79d4})
##### `NodeAddress`
IP or hostname with optional port to override how the frontend should connect to the server.<br>Format: \<address>\[:\<port>\]<br><br> The priority of addresses used by the frontend to specify the server **address** is:<br>- the address specified in NodeAddress (1)<br>- ListenIP (if not 0.0.0.0 or ::) (2)<br>- localhost (default) (3)<br>The priority of ports used by the frontend to specify the server **port** is:<br>- the port specified in NodeAddress (1)<br>- ListenPort (2)<br>- 10051 (default) (3)<br>See also: [HANodeName](#hanodename) parameter; [Enabling high availability](/manual/concepts/server/ha#enabling-high-availability).

Default: 'localhost:10051'

[comment]: # ({/new-9cfb79d4})

[comment]: # ({new-8153735c})
##### `PidFile`
Name of the PID file.

Default: `/tmp/zabbix\_server.pid`

[comment]: # ({/new-8153735c})

[comment]: # ({new-b0d337ac})
##### `ProblemHousekeepingFrequency`
Determines how often Zabbix will delete problems for deleted triggers in seconds.

Default: `60` | Range: 1-3600

[comment]: # ({/new-b0d337ac})

[comment]: # ({new-b3642a43})
##### `ProxyConfigFrequency`
Determines how often Zabbix server sends configuration data to a Zabbix proxy in seconds. Used only for proxies in a passive mode.

Default: `10` | Range: 1-604800

[comment]: # ({/new-b3642a43})

[comment]: # ({new-3bb5827f})
##### `ProxyDataFrequency`
Determines how often Zabbix server requests history data from a Zabbix proxy in seconds. Used only for proxies in the passive mode.

Default: `1` | Range: 1-3600

[comment]: # ({/new-3bb5827f})

[comment]: # ({new-7df5a0e9})
##### `ServiceManagerSyncFrequency`
Determines how often Zabbix will synchronize the configuration of a service manager in seconds.

Default: `60` | Range: 1-3600

[comment]: # ({/new-7df5a0e9})

[comment]: # ({new-9c385986})
##### `SNMPTrapperFile`
Temporary file used for passing data from the SNMP trap daemon to the server.<br>Must be the same as in zabbix\_trap\_receiver.pl or SNMPTT configuration file.

Default: `/tmp/zabbix\_traps.tmp`

[comment]: # ({/new-9c385986})

[comment]: # ({new-cd750f09})
##### `SocketDir`
Directory to store IPC sockets used by internal Zabbix services.

Default: `/tmp`

[comment]: # ({/new-cd750f09})

[comment]: # ({new-bf3120f2})
##### `SourceIP`
Source IP address for:<br>- outgoing connections to Zabbix proxy and Zabbix agent;<br>- agentless connections (VMware, SSH, JMX, SNMP, Telnet and simple checks);<br>- HTTP agent connections;<br>- script item JavaScript HTTP requests;<br>- preprocessing JavaScript HTTP requests;<br>- sending notification emails (connections to SMTP server);<br>- webhook notifications (JavaScript HTTP connections);<br>- connections to the Vault

[comment]: # ({/new-bf3120f2})

[comment]: # ({new-8edbdae2})
##### `SSHKeyLocation`
Location of public and private keys for SSH checks and actions.

[comment]: # ({/new-8edbdae2})

[comment]: # ({new-f14a0fc6})
##### `SSLCertLocation`
Location of SSL client certificate files for client authentication.<br>This parameter is used in web monitoring only.

[comment]: # ({/new-f14a0fc6})

[comment]: # ({new-d38d629d})
##### `SSLKeyLocation`
Location of SSL private key files for client authentication.<br>This parameter is used in web monitoring only.

[comment]: # ({/new-d38d629d})

[comment]: # ({new-d4ecaa9a})
##### `SSLCALocation`
Override the location of certificate authority (CA) files for SSL server certificate verification. If not set, system-wide directory will be used.<br>Note that the value of this parameter will be set as libcurl option CURLOPT\_CAPATH. For libcurl versions before 7.42.0, this only has effect if libcurl was compiled to use OpenSSL. For more information see [cURL web page](http://curl.haxx.se/libcurl/c/CURLOPT_CAPATH.html).<br>This parameter is used in web monitoring and in SMTP authentication.

[comment]: # ({/new-d4ecaa9a})

[comment]: # ({new-49587d38})
##### `StartAlerters`
The number of pre-forked instances of [alerters](/manual/concepts/server#server_process_types).

Default: `3` | Range: 1-100

[comment]: # ({/new-49587d38})

[comment]: # ({new-4404a76d})
##### `StartConnectors`
The number of pre-forked instances of [connector workers](/manual/concepts/server#server_process_types). The connector manager process is started automatically when a connector worker is started.

Range: 0-1000

[comment]: # ({/new-4404a76d})

[comment]: # ({new-25c85aff})
##### `StartDBSyncers`
The number of pre-forked instances of [history syncers](/manual/concepts/server#server_process_types).<br>*Note*: Be careful when changing this value, increasing it may do more harm than good. Roughly, the default value should be enough to handle up to 4000 NVPS.

Default: `4` | Range: 1-100

[comment]: # ({/new-25c85aff})

[comment]: # ({new-40a86c5a})
##### `StartDiscoverers`
The number of pre-forked instances of [discoverers](/manual/concepts/server#server_process_types).

Default: `1` | Range: 0-250

[comment]: # ({/new-40a86c5a})

[comment]: # ({new-5a8be3fd})
##### `StartEscalators`
The number of pre-forked instances of [escalators](/manual/concepts/server#server_process_types).

Default: `1` | Range: 1-100

[comment]: # ({/new-5a8be3fd})

[comment]: # ({new-97ad4fb5})
##### `StartHistoryPollers`
The number of pre-forked instances of [history pollers](/manual/concepts/server#server_process_types).<br>Only required for calculated checks.

Default: `5` | Range: 0-1000

[comment]: # ({/new-97ad4fb5})

[comment]: # ({new-565030c6})
##### `StartHTTPPollers`
The number of pre-forked instances of [HTTP pollers](/manual/concepts/server#server_process_types)**^[1](zabbix_server#footnotes)^**.

Default: `1` | Range: 0-1000

[comment]: # ({/new-565030c6})

[comment]: # ({new-70082588})
##### `StartIPMIPollers`
The number of pre-forked instances of [IPMI pollers](/manual/concepts/server#server_process_types).

Default: `0` | Range: 0-1000

[comment]: # ({/new-70082588})

[comment]: # ({new-5dbd1f62})
##### `StartJavaPollers`
The number of pre-forked instances of [Java pollers](/manual/concepts/server#server_process_types)**^[1](zabbix_server#footnotes)^**.

Default: `0` | Range: 0-1000

[comment]: # ({/new-5dbd1f62})

[comment]: # ({new-d9d80e3e})
##### `StartLLDProcessors`
The number of pre-forked instances of low-level discovery (LLD) [workers](/manual/concepts/server#server_process_types)**^[1](zabbix_server#footnotes)^**.<br>The LLD manager process is automatically started when an LLD worker is started.

Default: `2` | Range: 0-100

[comment]: # ({/new-d9d80e3e})

[comment]: # ({new-79d5cf2e})
##### `StartODBCPollers`
The number of pre-forked instances of [ODBC pollers](/manual/concepts/server#server_process_types)**^[1](zabbix_server#footnotes)^**.

Default: `1` | Range: 0-1000

[comment]: # ({/new-79d5cf2e})

[comment]: # ({new-e788444b})
##### `StartPingers`
The number of pre-forked instances of [ICMP pingers](/manual/concepts/server#server_process_types)**^[1](zabbix_server#footnotes)^**.

Default: `1` | Range: 0-1000

[comment]: # ({/new-e788444b})

[comment]: # ({new-a12742d5})
##### `StartPollersUnreachable`
The number of pre-forked instances of [pollers for unreachable hosts](/manual/concepts/server#server_process_types) (including IPMI and Java)**^[1](zabbix_server#footnotes)^**.<br>At least one poller for unreachable hosts must be running if regular, IPMI or Java pollers are started.

Default: `1` | Range: 0-1000

[comment]: # ({/new-a12742d5})

[comment]: # ({new-5a731224})
##### `StartPollers`
The number of pre-forked instances of [pollers](/manual/concepts/server#server_process_types)**^[1](zabbix_server#footnotes)^**.

Default: `5` | Range: 0-1000

[comment]: # ({/new-5a731224})

[comment]: # ({new-bdcb0d21})
##### `StartPreprocessors`
The number of pre-forked instances of preprocessing [workers](/manual/concepts/server#server_process_types)**^[1](zabbix_server#footnotes)^**.<br>The preprocessing manager process is automatically started when a preprocessor worker is started.

Default: `3` | Range: 1-1000

[comment]: # ({/new-bdcb0d21})

[comment]: # ({new-d6d62537})
##### `StartProxyPollers`
The number of pre-forked instances of [pollers for passive proxies](/manual/concepts/server#server_process_types)**^[1](zabbix_server#footnotes)^**.

Default: `1` | Range: 0-250

[comment]: # ({/new-d6d62537})

[comment]: # ({new-469b0927})
##### `StartReportWriters`
The number of pre-forked instances of [report writers](/manual/concepts/server#server_process_types).<br>If set to 0, scheduled report generation is disabled.<br>The report manager process is automatically started when a report writer is started.

Default: `0` | Range: 0-100

[comment]: # ({/new-469b0927})

[comment]: # ({new-9cccc249})
##### `StartSNMPTrapper`
If set to 1, an [SNMP trapper](/manual/concepts/server#server_process_types) process will be started.

Default: `0` | Range: 0-1

[comment]: # ({/new-9cccc249})

[comment]: # ({new-f1f30b44})
##### `StartTimers`
The number of pre-forked instances of [timers](/manual/concepts/server#server_process_types).<br>Timers process maintenance periods.

Default: `1` | Range: 1-1000

[comment]: # ({/new-f1f30b44})

[comment]: # ({new-368a443a})
##### `StartTrappers`
The number of pre-forked instances of [trappers](/manual/concepts/server#server_process_types)**^[1](zabbix_server#footnotes)^**.<br>Trappers accept incoming connections from Zabbix sender, active agents and active proxies.

Default: `5` | Range: 1-1000

[comment]: # ({/new-368a443a})

[comment]: # ({new-16e17139})
##### `StartVMwareCollectors`
The number of pre-forked [VMware collector](/manual/concepts/server#server_process_types) instances.

Default: `0` | Range: 0-250

[comment]: # ({/new-16e17139})

[comment]: # ({new-0da7c397})
##### `StatsAllowedIP`
List of comma delimited IP addresses, optionally in CIDR notation, or DNS names of external Zabbix instances. Stats request will be accepted only from the addresses listed here. If this parameter is not set no stats requests will be accepted.<br>If IPv6 support is enabled then '127.0.0.1', '::127.0.0.1', '::ffff:127.0.0.1' are treated equally and '::/0' will allow any IPv4 or IPv6 address. '0.0.0.0/0' can be used to allow any IPv4 address.

Example:

    StatsAllowedIP=127.0.0.1,192.168.1.0/24,::1,2001:db8::/32,zabbix.example.com

[comment]: # ({/new-0da7c397})

[comment]: # ({new-db780aaa})
##### `Timeout`
Specifies how long we wait for agent, SNMP device or external check in seconds.

Default: `3` | Range: 1-30

[comment]: # ({/new-db780aaa})

[comment]: # ({new-68df390b})
##### `TLSCAFile`
Full pathname of a file containing the top-level CA(s) certificates for peer certificate verification, used for encrypted communications between Zabbix components.

[comment]: # ({/new-68df390b})

[comment]: # ({new-24534d6d})
##### `TLSCertFile`
Full pathname of a file containing the server certificate or certificate chain, used for encrypted communications between Zabbix components.

[comment]: # ({/new-24534d6d})

[comment]: # ({new-0c9251b8})
##### `TLSCipherAll`
GnuTLS priority string or OpenSSL (TLS 1.2) cipher string. Override the default ciphersuite selection criteria for certificate- and PSK-based encryption.

Example:

    TLS\_AES\_256\_GCM\_SHA384:TLS\_CHACHA20\_POLY1305\_SHA256:TLS\_AES\_128\_GCM\_SHA256

[comment]: # ({/new-0c9251b8})

[comment]: # ({new-74bef172})
##### `TLSCipherAll13`
Cipher string for OpenSSL 1.1.1 or newer in TLS 1.3. Override the default ciphersuite selection criteria for certificate- and PSK-based encryption.

Example for GnuTLS: 

    NONE:+VERS-TLS1.2:+ECDHE-RSA:+RSA:+ECDHE-PSK:+PSK:+AES-128-GCM:+AES-128-CBC:+AEAD:+SHA256:+SHA1:+CURVE-ALL:+COMP-NULL::+SIGN-ALL:+CTYPE-X.509

Example for OpenSSL: 

    EECDH+aRSA+AES128:RSA+aRSA+AES128:kECDHEPSK+AES128:kPSK+AES128

[comment]: # ({/new-74bef172})

[comment]: # ({new-a0eeb337})
##### `TLSCipherCert`
GnuTLS priority string or OpenSSL (TLS 1.2) cipher string. Override the default ciphersuite selection criteria for certificate-based encryption.

Example for GnuTLS: 

    NONE:+VERS-TLS1.2:+ECDHE-RSA:+RSA:+AES-128-GCM:+AES-128-CBC:+AEAD:+SHA256:+SHA1:+CURVE-ALL:+COMP-NULL:+SIGN-ALL:+CTYPE-X.509

Example for OpenSSL: 

    EECDH+aRSA+AES128:RSA+aRSA+AES128

[comment]: # ({/new-a0eeb337})

[comment]: # ({new-07722feb})
##### `TLSCipherCert13`
Cipher string for OpenSSL 1.1.1 or newer in TLS 1.3. Override the default ciphersuite selection criteria for certificate-based encryption.

[comment]: # ({/new-07722feb})

[comment]: # ({new-b3e6744e})
##### `TLSCipherPSK`
GnuTLS priority string or OpenSSL (TLS 1.2) cipher string. Override the default ciphersuite selection criteria for PSK-based encryption.

Example for GnuTLS: 

    NONE:+VERS-TLS1.2:+ECDHE-PSK:+PSK:+AES-128-GCM:+AES-128-CBC:+AEAD:+SHA256:+SHA1:+CURVE-ALL:+COMP-NULL:+SIGN-ALL

Example for OpenSSL: 

    kECDHEPSK+AES128:kPSK+AES128

[comment]: # ({/new-b3e6744e})

[comment]: # ({new-a67b06d2})
##### `TLSCipherPSK13`
Cipher string for OpenSSL 1.1.1 or newer in TLS 1.3. Override the default ciphersuite selection criteria for PSK-based encryption.

Example:

    TLS_CHACHA20_POLY1305_SHA256:TLS_AES_128_GCM_SHA256

[comment]: # ({/new-a67b06d2})

[comment]: # ({new-7b53bee4})
##### `TLSCRLFile`
Full pathname of a file containing revoked certificates. This parameter is used for encrypted communications between Zabbix components.

[comment]: # ({/new-7b53bee4})

[comment]: # ({new-e236df69})
##### `TLSKeyFile`
Full pathname of a file containing the server private key, used for encrypted communications between Zabbix components.

[comment]: # ({/new-e236df69})

[comment]: # ({new-0b008346})
##### `TmpDir`
Temporary directory.

Default: `/tmp`

[comment]: # ({/new-0b008346})

[comment]: # ({new-621f6e9f})
##### `TrapperTimeout`
Specifies how many seconds the trapper may spend processing new data.

Default: `300` | Range: 1-300

[comment]: # ({/new-621f6e9f})

[comment]: # ({new-02c73c1a})
##### `TrendCacheSize`
Size of the trend cache, in bytes.<br>The shared memory size for storing trends data.

Default: `4M` | Range: 128K-2G

[comment]: # ({/new-02c73c1a})

[comment]: # ({new-5eeafa8d})
##### `TrendFunctionCacheSize`
Size of the trend function cache, in bytes.<br>The shared memory size for caching calculated trend function data.

Default: `4M` | Range: 128K-2G

[comment]: # ({/new-5eeafa8d})

[comment]: # ({new-f61cf9e1})
##### `UnavailableDelay`
Determines how often host is checked for availability during the [unavailability](/manual/appendix/items/unreachability#unavailable_host) period in seconds.

Default: `60` | Range: 1-3600

[comment]: # ({/new-f61cf9e1})

[comment]: # ({new-98b994ea})
##### `UnreachableDelay`
Determines how often host is checked for availability during the [unreachability](/manual/appendix/items/unreachability#unreachable_host) period in seconds.

Default: `15` | Range: 1-3600

[comment]: # ({/new-98b994ea})

[comment]: # ({new-df350752})
##### `UnreachablePeriod`
Determines after how many seconds of [unreachability](/manual/appendix/items/unreachability#unreachable_host) treats a host as unavailable.

Default: `45` | Range: 1-3600

[comment]: # ({/new-df350752})

[comment]: # ({new-a9a2ec9d})
##### `User`
Drop privileges to a specific, existing user on the system.<br>Only has effect if run as 'root' and AllowRoot is disabled.

Default: `zabbix`

[comment]: # ({/new-a9a2ec9d})

[comment]: # ({new-20ddb562})
##### `ValueCacheSize`
Size of the history value cache, in bytes.<br>The shared memory size for caching item history data requests.<br>Setting to 0 disables the value cache (not recommended).<br>When the value cache runs out of the shared memory a warning message is written to the server log every 5 minutes.

Default: `8M` | Range: 0,128K-64G

[comment]: # ({/new-20ddb562})

[comment]: # ({new-35521bcc})
##### `Vault`
Specifies the vault provider:<br>*HashiCorp* - HashiCorp KV Secrets Engine version 2<br>*CyberArk*  - CyberArk Central Credential Provider<br>Must match the vault provider set in the frontend.

Default: `HashiCorp`

[comment]: # ({/new-35521bcc})

[comment]: # ({new-664924ee})
##### `VaultDBPath`
Specifies a location, from where database credentials should be retrieved by keys. Depending on the Vault, can be vault path or query.<br> The keys used for HashiCorp are 'password' and 'username'. 

Example: 

    secret/zabbix/database

The keys used for CyberArk are 'Content' and 'UserName'.

Example: 

    AppID=zabbix_server&Query=Safe=passwordSafe;Object=zabbix_proxy_database

This option can only be used if DBUser and DBPassword are not specified.

[comment]: # ({/new-664924ee})

[comment]: # ({new-778803e5})
##### `VaultTLSCertFile`
Name of the SSL certificate file used for client authentication<br> The certificate file must be in PEM1 format. <br> If the certificate file contains also the private key, leave the SSL key file field empty. <br> The directory containing this file is specified by the configuration parameter SSLCertLocation.<br>This option can be omitted but is recommended for CyberArkCCP vault.

[comment]: # ({/new-778803e5})

[comment]: # ({new-755d874e})
##### `VaultTLSKeyFile`
Name of the SSL private key file used for client authentication. <br> The private key file must be in PEM1 format. <br> The directory containing this file is specified by the configuration parameter SSLKeyLocation. <br>This option can be omitted but is recommended for CyberArkCCP vault.

[comment]: # ({/new-755d874e})

[comment]: # ({new-cc25a6c8})
##### `VaultToken`
HashiCorp Vault authentication token that should have been generated exclusively for Zabbix server with read-only permission to the paths specified in [Vault macros](/manual/config/macros/user_macros#configuration) and read-only permission to the path specified in the optional VaultDBPath configuration parameter.<br>It is an error if VaultToken and VAULT\_TOKEN environment variable are defined at the same time.

Mandatory: Yes, if Vault is set to *HashiCorp*; otherwise no

[comment]: # ({/new-cc25a6c8})

[comment]: # ({new-f46a0763})
##### `VaultURL`
Vault server HTTP\[S\] URL. The system-wide CA certificates directory will be used if SSLCALocation is not specified.

Default: `https://127.0.0.1:8200`

[comment]: # ({/new-f46a0763})

[comment]: # ({new-854dfe73})
##### `VMwareCacheSize`
Shared memory size for storing VMware data.<br>A VMware internal check zabbix\[vmware,buffer,...\] can be used to monitor the VMware cache usage (see [Internal checks](/manual/config/items/itemtypes/internal)).<br>Note that shared memory is not allocated if there are no vmware collector instances configured to start.

Default: `8M` | Range: 256K-2G

[comment]: # ({/new-854dfe73})

[comment]: # ({new-2a646d43})
##### `VMwareFrequency`
Delay in seconds between data gathering from a single VMware service.<br>This delay should be set to the least update interval of any VMware monitoring item.

Default: `60` | Range: 10-86400

[comment]: # ({/new-2a646d43})

[comment]: # ({new-cf5957ec})
##### `VMwarePerfFrequency`
Delay in seconds between performance counter statistics retrieval from a single VMware service.<br>This delay should be set to the least update interval of any VMware monitoring [item](/manual/config/items/itemtypes/simple_checks/vmware_keys#footnotes) that uses VMware performance counters.

Default: `60` | Range: 10-86400

[comment]: # ({/new-cf5957ec})

[comment]: # ({new-eefd2428})
##### `VMwareTimeout`
The maximum number of seconds a vmware collector will wait for a response from VMware service (vCenter or ESX hypervisor).

Default: `10` | Range: 1-300

[comment]: # ({/new-eefd2428})

[comment]: # ({new-738954e5})
##### `WebServiceURL`
HTTP\[S\] URL to Zabbix web service in the format `<host:port>/report`. 

Example: 

    WebServiceURL=http://localhost:10053/report

[comment]: # ({/new-738954e5})

[comment]: # ({new-2ab44494})
##### Footnotes

^**1**^ Note that too many data gathering processes (pollers,
unreachable pollers, HTTP pollers, Java pollers, pingers, trappers,
proxypollers) together with IPMI manager, SNMP trapper and preprocessing
workers can **exhaust** the per-process file descriptor limit for the
preprocessing manager.

::: notewarning
This will cause Zabbix server to stop (usually
shortly after the start, but sometimes it can take more time). The
configuration file should be revised or the limit should be raised to
avoid this situation.
:::

^**2**^ When a lot of items are deleted it increases the load to the
database, because the housekeeper will need to remove all the history
data that these items had. For example, if we only have to remove 1 item
prototype, but this prototype is linked to 50 hosts and for every host
the prototype is expanded to 100 real items, 5000 items in total have to
be removed (1\*50\*100). If 500 is set for MaxHousekeeperDelete
(MaxHousekeeperDelete=500), the housekeeper process will have to remove
up to 2500000 values (5000\*500) for the deleted items from history and
trends tables in one cycle.

[comment]: # ({/new-2ab44494})
