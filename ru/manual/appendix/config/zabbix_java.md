[comment]: # attributes: notoc

[comment]: # translation:outdated

[comment]: # ({new-8bf9c5fc})
# 5 Zabbix Java gateway

Если вы используете скрипты `startup.sh` и `shutdown.sh` для запуска
[Zabbix Java gateway](/ru/manual/concepts/java), вы можете указать
необходимые параметры конфигурации в файле `settings.sh`. Скрипты
запуска и остановки являются источником файла настроек и заботятся о
конвертации shell переменных (приведенные в первой колонке) в свойства
Java (приведенные во второй колонке).

Если вы запускаете Zabbix Java gateway вручную, непосредственно с
помощью `java`, тогда вы можете указать соответствующие свойства Java в
командной строке.

|Параметр|Свойство|Обязательный|Диапазон|Умолчание|Описание|
|----------------|----------------|------------------------|----------------|------------------|----------------|
|LISTEN\_IP|zabbix.listenIP|нет|<|0.0.0.0|IP адрес, который слушает Zabbix Java Gateway.|
|LISTEN\_PORT|zabbix.listenPort|нет|1024-32767|10052|Порт, который слушает Zabbix Java Gateway.|
|PID\_FILE|zabbix.pidFile|нет|<|/tmp/zabbix\_java.pid|Имя PID файла. Если не задано, Zabbix Java Gateway запустится консольным приложением.|
|START\_POLLERS|zabbix.startPollers|нет|1-1000|5|Количество запускаемых потоков worker.|
|TIMEOUT|zabbix.timeout|нет|1-30|3|Как долго ждать завершения сетевых операций. Этот параметр поддерживается с Zabbix 2.0.15, 2.2.10 и 2.4.5.|

::: notewarning
Порт 10052 не [зарегистрирован в
IANA](http://www.iana.org/assignments/service-names-port-numbers/service-names-port-numbers.txt).
:::

[comment]: # ({/new-8bf9c5fc})
