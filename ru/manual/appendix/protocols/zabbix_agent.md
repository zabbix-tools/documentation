[comment]: # translation:outdated

[comment]: # ({new-a175d7cd})
# 2 Протокол Zabbix агента

Пожалуйста, обратитесь к странице [Пассивные и активные проверки
агента](/ru/manual/appendix/items/activepassive) для получения более
подробной информации.

[comment]: # ({/new-a175d7cd})
