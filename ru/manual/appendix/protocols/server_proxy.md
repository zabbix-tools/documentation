[comment]: # translation:outdated

[comment]: # ({new-7669b1c2})
# 1 Протокол обмена данными сервер-прокси

[comment]: # ({/new-7669b1c2})

[comment]: # ({new-7382efcd})
#### Обзор

Обмен данными сервер - прокси основывается на JSON формате.

Сообщения запроса и ответа должны начинаться с [длины заголовка и
данных](/ru/manual/appendix/protocols/header_datalen).

[comment]: # ({/new-7382efcd})

[comment]: # ({new-14661e11})
#### Пассивный прокси

[comment]: # ({/new-14661e11})

[comment]: # ({new-fac082ad})
##### Запрос конфигурации прокси

Запрос `proxy config` отправляется сервером, чтобы обеспечить прокси
данными конфигурации. Этот запрос отправляется каждые
`ProxyConfigFrequency` (параметр конфигурации сервера) секунд.

|имя|<|<|<|тип значения|описание|
|------|-|-|-|-----------------------|----------------|
|сервер→прокси:|<|<|<|<|<|
|**request**|<|<|<|*строка*|'proxy config'|
|**<таблица>**|<|<|<|*объект*|один или несколько объектов с <таблица> данными|
|<|**поля**|<|<|*массив*|массив имен полей|
|<|<|\-|<|*строка*|имя поля|
|<|**данные**|<|<|*массив*|массив строк|
|<|<|\-|<|*массив*|массив колонок|
|<|<|<|\-|*строка*,*число*|значение колонки с типом, зависящим от типа колонки в схеме базы данных|
|прокси→сервер:|<|<|<|<|<|
|**response**|<|<|<|*строка*|информация об успешности запроса ('success' или 'failed')|
|**version**|<|<|<|*строка*|версия прокси (<мажорная>.<минорная>.<номер сборки>)|

Пример:

сервер→прокси:

``` {.javascript}
{
    "request": "proxy config",
    "globalmacro":{
        "fields":[
            "globalmacroid",
            "macro",
            "value"
        ],
        "data":[
            [
                2,
                "{$SNMP_COMMUNITY}",
                "public"
            ]
        ]
    },
    "hosts":{
        "fields":[
            "hostid",
            "host",
            "status",
            "ipmi_authtype",
            "ipmi_privilege",
            "ipmi_username",
            "ipmi_password",
            "name",
            "tls_connect",
            "tls_accept",
            "tls_issuer",
            "tls_subject",
            "tls_psk_identity",
            "tls_psk"
        ],
        "data":[
            [
                10001,
                "Template OS Linux",
                3,
                -1,
                2,
                "",
                "",
                "Template OS Linux",
                1,
                1,
                "",
                "",
                "",
                ""
            ],
            [
                10050,
                "Template App Zabbix Agent",
                3,
                -1,
                2,
                "",
                "",
                "Template App Zabbix Agent",
                1,
                1,
                "",
                "",
                "",
                ""
            ],
            [
                10105,
                "Logger",
                0,
                -1,
                2,
                "",
                "",
                "Logger",
                1,
                1,
                "",
                "",
                "",
                ""
            ]
        ]
    },
    "interface":{
        "fields":[
            "interfaceid",
            "hostid",
            "main",
            "type",
            "useip",
            "ip",
            "dns",
            "port",
            "bulk"
        ],
        "data":[
            [
                2,
                10105,
                1,
                1,
                1,
                "127.0.0.1",
                "",
                "10050",
                1
            ]
        ]
    },
    ...
}
```

прокси→сервер:

``` {.javascript}
{
  "response": "success",
  "version": "3.4.0"
}
```

[comment]: # ({/new-fac082ad})

[comment]: # ({new-c348e5d9})
##### Запрос прокси

Запрос `proxy data` используется для получения данных доступности узлов
сети, истории, обнаружения и авторегистрации с прокси. Этот запрос
отправляется каждые `ProxyDataFrequency` (параметр конфигурации сервера)
секунд.

|имя|<|тип значения|описание|
|------|-|-----------------------|----------------|
|сервер→прокси:|<|<|<|
|**request**|<|*строка*|'proxy data'|
|прокси→сервер:|<|<|<|
|**session**|<|*строка*|Токен сеанса передачи данных|
|**host availability**|<|*массив*|*(опционально)* массив объектов с данными о доступности узлов сети|
|<|**hostid**|*число*|идентификатор узла сети|
|<|**available**|*число*|доступность Zabbix агента<br><br>**0**, *HOST\_AVAILABLE\_UNKNOWN* - неизвестно<br>**1**, *HOST\_AVAILABLE\_TRUE* - доступен<br>**2**, *HOST\_AVAILABLE\_FALSE* - недоступен|
|<|**error**|*строка*|сообщение об ошибке Zabbix агента или пустая строка|
|<|**snmp\_available**|*число*|доступность SNMP агента<br><br>**0**, *HOST\_AVAILABLE\_UNKNOWN* - неизвестно<br>**1**, *HOST\_AVAILABLE\_TRUE* - доступен<br>**2**, *HOST\_AVAILABLE\_FALSE* - недоступен|
|<|**snmp\_error**|*строка*|сообщение об ошибке SNMP агента или пустая строка|
|<|**ipmi\_available**|*число*|доступность IPMI агента<br><br>**0**, *HOST\_AVAILABLE\_UNKNOWN* - неизвестно<br>**1**, *HOST\_AVAILABLE\_TRUE* - доступен<br>**2**, *HOST\_AVAILABLE\_FALSE* - недоступен|
|<|**ipmi\_error**|*строка*|сообщение об ошибке IPMI агента или пустая строка|
|<|**jmx\_available**|*число*|доступность JMX агента<br><br>**0**, *HOST\_AVAILABLE\_UNKNOWN* - неизвестно<br>**1**, *HOST\_AVAILABLE\_TRUE* - доступен<br>**2**, *HOST\_AVAILABLE\_FALSE* - недоступен|
|<|**jmx\_error**|*строка*|сообщение об ошибке JMX агента или пустая строка|
|**history data**|<|*массив*|*(опционально)* массив объектов с данными истории|
|<|**itemid**|*число*|идентификатор элемента данных|
|<|**clock**|*число*|штамп времени значения элемента данных (секунды)|
|<|**ns**|*число*|штамп времени значения элемента данных (наносекунды)|
|<|**value**|*строка*|*(опционально)* значение элемента данных|
|<|**id**|*число*|идентификатор числа (возрастающий счётчик, уникальный в пределах одного сеанса передачи данных)|
|<|**timestamp**|*число*|*(опционально)* штамп времени у элементов данных с типом журнал (лог)|
|<|**source**|*строка*|*(опционально)* значение источника элемента данных журнала событий|
|<|**severity**|*число*|*(опционально)* значение важности элемента данных журнала событий|
|<|**eventid**|*число*|*(опционально)* значение eventid элемента данных журнала событий|
|<|**state**|*строка*|*(опционально)* статус элемента данных<br>**0**, *ITEM\_STATE\_NORMAL*<br>**1**, *ITEM\_STATE\_NOTSUPPORTED*|
|<|**lastlogsize**|*число*|*(опционально)* последний размер журнала у элементов данных с типом журнал (лог)|
|<|**mtime**|*число*|*(опционально)* время модификации журнала у элементов данных с типом журнал (лог)|
|**discovery data**|<|*массив*|*(опционально)* массив объектов с данными обнаружения|
|<|**clock**|*число*|штамп времени данных обнаружения|
|<|**druleid**|*число*|идентификатор правила обнаружения|
|<|**dcheckid**|*число*|идентификатор проверки обнаружения или null для данных правила обнаружения|
|<|**type**|*число*|тип проверки обнаружения:<br><br>**-1** данные правила обнаружения<br>**0**, *SVC\_SSH* - проверка SSH сервиса<br>**1**, *SVC\_LDAP* - проверка LDAP сервиса<br>**2**, *SVC\_SMTP* - проверка SMTP сервиса<br>**3**, *SVC\_FTP* - проверка FTP сервиса<br>**4**, *SVC\_HTTP* - проверка HTTP сервиса<br>**5**, *SVC\_POP* - проверка POP сервиса<br>**6**, *SVC\_NNTP* - проверка NNTP сервиса<br>**7**, *SVC\_IMAP* - проверка IMAP сервиса<br>**8**, *SVC\_TCP* - проверка доступности TCP порта<br>**9**, *SVC\_AGENT* - Zabbix агент<br>**10**, *SVC\_SNMPv1* - SNMPv1 агент<br>**11**, *SVC\_SNMPv2* - SNMPv2 агент<br>**12**, *SVC\_ICMPPING* - ICMP пинг<br>**13**, *SVC\_SNMPv3* - SNMPv3 агент<br>**14**, *SVC\_HTTPS* - проверка HTTPS сервиса<br>**15**, *SVC\_TELNET* - проверка доступности Telnet|
|<|**ip**|*строка*|IP адрес хоста|
|<|**dns**|*строка*|DNS имя хоста|
|<|**port**|*число*|*(опционально)* номер порта сервиса|
|<|**key\_**|*строка*|*(опционально)* ключ элемента данных для проверки обнаружения с типом **9** *SVC\_AGENT*|
|<|**value**|*строка*|*(опционально)* полученное значение от сервиса, может быть пустым по большинству сервисов|
|<|**status**|*число*|*(опционально)* состояние сервиса:<br><br>**0**, *DOBJECT\_STATUS\_UP* - Сервис ДОСТУПЕН<br>**1**, *DOBJECT\_STATUS\_DOWN* - Сервис НЕДОСТУПЕН|
|**auto registration**|<|*массив*|*(опционально)* массив объектов с данными авторегистрации|
|<|**clock**|*число*|штамп времени данных авторегистрации|
|<|**host**|*строка*|имя хоста|
|<|**ip**|*строка*|*(опционально)* IP адрес хоста|
|<|**dns**|*строка*|*(опционально)* разрешенный DNS имя с IP адреса|
|<|**port**|*строка*|*(опционально)* порт хоста|
|<|**host\_metadata**|*строка*|*(опционально)* метаданные хоста, которые отправил агент (на основе параметров конфигурации агента HostMetadata или HostMetadataItem)|
|**tasks**|<|*массив*|*(опционально)* массив задач|
|<|**type**|*число*|тип задачи:<br><br>**0**, *ZBX\_TM\_TASK\_PROCESS\_REMOTE\_COMMAND\_RESULT* - результат удаленной команды|
|<|**status**|*число*|состояние выполнения удаленной команды:<br><br>**0**, *ZBX\_TM\_REMOTE\_COMMAND\_COMPLETED* - удаленная команда завершена успешно<br>**1**, *ZBX\_TM\_REMOTE\_COMMAND\_FAILED* - удаленная команда завершилась с ошибкой|
|<|**error**|*строка*|*(опционально)* сообщение об ошибке|
|<|**parent\_taskid**|*число*|id родительской задачи|
|**more**|<|*число*|*(опционально)* 1 - имеются еще данные истории к отправке|
|**clock**|<|*число*|*(опционально)* штамп времени передачи данных (секунды)|
|**ns**|<|*число*|*(опционально)* штамп времени передачи данных (наносекунды)|
|**version**|<|*строка*|версия прокси (<мажорная>.<минорная>.<номер сборки>)|
|сервер→прокси:|<|<|<|
|**response**|<|*строка*|информация об успешности запроса ('success' или 'failed')|
|**tasks**|<|*массив*|*(опционально)* array of tasks|
|<|**type**|*число*|тип задачи:<br><br>**1**, *ZBX\_TM\_TASK\_PROCESS\_REMOTE\_COMMAND* - удаленная команда|
|<|**clock**|*число*|время создания задачи|
|<|**ttl**|*число*|время в секундах после которого срок задачи истекает|
|<|**commandtype**|*число*|тип удаленной команды:<br><br>**0**, *ZBX\_SCRIPT\_TYPE\_CUSTOM\_SCRIPT* - использование пользовательского скрипта<br>**1**, *ZBX\_SCRIPT\_TYPE\_IPMI* - использование IPMI<br>**2**, *ZBX\_SCRIPT\_TYPE\_SSH* - использование SSH<br>**3**, *ZBX\_SCRIPT\_TYPE\_TELNET* - использование Telnet<br>**4**, *ZBX\_SCRIPT\_TYPE\_GLOBAL\_SCRIPT* - использование глобального скрипта (в настоящее время возможности эквивалентны пользовательской команде)|
|<|**command**|*строка*|выполняемая удаленная команда|
|<|**execute\_on**|*число*|цель выполнения для пользовательских скриптов:<br><br>**0**, *ZBX\_SCRIPT\_EXECUTE\_ON\_AGENT* - выполнение скрипта на стороне агента<br>**1**, *ZBX\_SCRIPT\_EXECUTE\_ON\_SERVER* - выполнение скрипта на стороне сервера<br>**2**, *ZBX\_SCRIPT\_EXECUTE\_ON\_PROXY* - выполнение скрипта на стороне прокси|
|<|**port**|*число*|*(опционально)* порт для telnet и ssh команд|
|<|**authtype**|*число*|*(опционально)* тип аутентификации для ssh команд|
|<|**username**|*строка*|*(опционально)* имя пользователя для telnet и ssh команд|
|<|**password**|*строка*|*(опционально)* пароль для telnet и ssh команд|
|<|**publickey**|*строка*|*(опционально)* публичный ключ для ssh команд|
|<|**privatekey**|*строка*|*(опционально)* приватный ключ для ssh команд|
|<|**parent\_taskid**|*число*|id родительской задачи|
|<|**hostid**|*число*|hostid цели|

Пример:

сервер→прокси:

``` {.javascript}
{
  "request": "proxy data"
}
```

прокси→сервер:

``` {.javascript}
{
    "session": "12345678901234567890123456789012"
    "host availability":[
        {
            "hostid":10106,
            "available":1,
            "error":"",
            "snmp_available":0,
            "snmp_error":"",
            "ipmi_available":0,
            "ipmi_error":"",
            "jmx_available":0,
            "jmx_error":""
        },
        {
            "hostid":10107,
            "available":1,
            "error":"",
            "snmp_available":0,
            "snmp_error":"",
            "ipmi_available":0,
            "ipmi_error":"",
            "jmx_available":0,
            "jmx_error":""
        }
    ],
    "history data":[
        {
            "itemid":"12345",
            "clock":1478609647,
            "ns":332510044,
            "value":"52956612",
            "id": 1
        },
        {
            "itemid":"12346",
            "clock":1478609647,
            "ns":330690279,
            "state":1,
            "value":"Cannot find information for this network interface in /proc/net/dev.",
            "id": 2
        }
    ],
    "discovery data":[
        {
            "clock":1478608764,
            "drule":2,
            "dcheck":3,
            "type":12,
            "ip":"10.3.0.10",
            "dns":"vdebian",
            "status":1
        },
        {
            "clock":1478608764,
            "drule":2,
            "dcheck":null,
            "type":-1,
            "ip":"10.3.0.10",
            "dns":"vdebian",
            "status":1
        }
    ],
    "auto registration":[
        {
            "clock":1478608371,
            "host":"Logger1",
            "ip":"10.3.0.1",
            "dns":"localhost",
            "port":"10050"
        },
        {
            "clock":1478608381,
            "host":"Logger2",
            "ip":"10.3.0.2",
            "dns":"localhost",
            "port":"10050"
        }
    ],
    "tasks":[
        {
            "type": 0,
            "status": 0,
            "parent_taskid": 10
        },
        {
            "type": 0,
            "status": 1,
            "error": "No permissions to execute task.",
            "parent_taskid": 20
        }
    ],    
    "version":"3.4.0"
}
```

сервер→прокси:

``` {.javascript}
{
  "response": "success",
  "tasks":[
      {
         "type": 1,
         "clock": 1478608371,
         "ttl": 600,
         "commandtype": 2,
         "command": "restart_service1.sh",
         "execute_on": 2,
         "port": 80,
         "authtype": 0,
         "username": "userA",
         "password": "password1",
         "publickey": "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCqGKukO1De7zhZj6+H0qtjTkVxwTCpvKe",
         "privatekey": "lsuusFncCzWBQ7RKNUSesmQRMSGkVb1/3j+skZ6UtW+5u09lHNsj6tQ5QCqGKukO1De7zhd",
         "parent_taskid": 10,
         "hostid": 10070
      },
      {
         "type": 1,
         "clock": 1478608381,
         "ttl": 600,
         "commandtype": 1,
         "command": "restart_service2.sh",
         "execute_on": 0,
         "authtype": 0,
         "username": "",
         "password": "",
         "publickey": "",
         "privatekey": "",
         "parent_taskid": 20,
         "hostid": 10084
      }
  ]
}
```

[comment]: # ({/new-c348e5d9})

[comment]: # ({new-967ea060})
#### Активный прокси

[comment]: # ({/new-967ea060})


[comment]: # ({new-bed30a3b})
##### Запрос конфигурации прокси

Запрос `proxy config` отправляется прокси, чтобы получить данные
конфигурации прокси. Запрос отправляется каждые `ConfigFrequency`
(параметр конфигурации прокси) секунд.

|имя|<|<|<|тип значения|описание|
|------|-|-|-|-----------------------|----------------|
|прокси→сервер:|<|<|<|<|<|
|**request**|<|<|<|*строка*|'proxy config'|
|**host**|<|<|<|*строка*|имя прокси|
|**version**|<|<|<|*строка*|версия прокси (<мажорная>.<минорная>.<номер сборки>)|
|сервер→прокси:|<|<|<|<|<|
|**request**|<|<|<|*строка*|'proxy config'|
|**<table>**|<|<|<|*объект*|один или несколько объектов с <таблица> данными|
|<|**fields**|<|<|*массив*|массив имен полей|
|<|<|\-|<|*строка*|имя поля|
|<|**data**|<|<|*массив*|массив строк|
|<|<|\-|<|*массив*|массив колонок|
|<|<|<|\-|*строка*,*число*|значение колонки с типом, зависящим от типа колонки в схеме базы данных|
|прокси→сервер:|<|<|<|<|<|
|**response**|<|<|<|*строка*|информация об успешности запроса ('success' или 'failed')|

Пример:

прокси→сервер:

``` {.javascript}
{
  "request": "proxy config",
  "host": "Proxy #12",
  "version":"3.4.0"
}
```

сервер→прокси:

``` {.javascript}
{
    "globalmacro":{
        "fields":[
            "globalmacroid",
            "macro",
            "value"
        ],
        "data":[
            [
                2,
                "{$SNMP_COMMUNITY}",
                "public"
            ]
        ]
    },
    "hosts":{
        "fields":[
            "hostid",
            "host",
            "status",
            "ipmi_authtype",
            "ipmi_privilege",
            "ipmi_username",
            "ipmi_password",
            "name",
            "tls_connect",
            "tls_accept",
            "tls_issuer",
            "tls_subject",
            "tls_psk_identity",
            "tls_psk"
        ],
        "data":[
            [
                10001,
                "Template OS Linux",
                3,
                -1,
                2,
                "",
                "",
                "Template OS Linux",
                1,
                1,
                "",
                "",
                "",
                ""
            ],
            [
                10050,
                "Template App Zabbix Agent",
                3,
                -1,
                2,
                "",
                "",
                "Template App Zabbix Agent",
                1,
                1,
                "",
                "",
                "",
                ""
            ],
            [
                10105,
                "Logger",
                0,
                -1,
                2,
                "",
                "",
                "Logger",
                1,
                1,
                "",
                "",
                "",
                ""
            ]
        ]
    },
    "interface":{
        "fields":[
            "interfaceid",
            "hostid",
            "main",
            "type",
            "useip",
            "ip",
            "dns",
            "port",
            "bulk"
        ],
        "data":[
            [
                2,
                10105,
                1,
                1,
                1,
                "127.0.0.1",
                "",
                "10050",
                1
            ]
        ]
    },
    ...
}
```

прокси→сервер:

``` {.javascript}
{
  "response": "success"
}
```

[comment]: # ({/new-bed30a3b})

[comment]: # ({new-ef0ba621})
##### Запрос данных прокси

Запрос `proxy data` отправляется прокси, чтобы предоставить данные
доступности узлов сети, истории, обнаружения и авторегистрации. Этот
запрос отправляется каждые `DataSenderFrequency` (параметр конфигурации
прокси) секунд.

|имя|<|тип значения|описание|
|------|-|-----------------------|----------------|
|прокси→сервер:|<|<|<|
|**request**|<|*строка*|'proxy data'|
|**host**|<|*строка*|имя прокси|
|**session**|<|*строка*|Токен сеанса передачи данных|
|**host availability**|<|*массив*|*(опционально)* массив объектов с данными о доступности узлов сети|
|<|**hostid**|*число*|идентификатор узла сети|
|<|**available**|*число*|доступность Zabbix агента<br><br>**0**, *HOST\_AVAILABLE\_UNKNOWN* - неизвестно<br>**1**, *HOST\_AVAILABLE\_TRUE* - доступен<br>**2**, *HOST\_AVAILABLE\_FALSE* - недоступен|
|<|**error**|*строка*|сообщение об ошибке Zabbix агента или пустая строка|
|<|**snmp\_available**|*число*|доступность SNMP агента<br><br>**0**, *HOST\_AVAILABLE\_UNKNOWN* - неизвестно<br>**1**, *HOST\_AVAILABLE\_TRUE* - доступен<br>**2**, *HOST\_AVAILABLE\_FALSE* - недоступен|
|<|**snmp\_error**|*строка*|сообщение об ошибке SNMP агента или пустая строка|
|<|**ipmi\_available**|*число*|доступность IPMI агента<br><br>**0**, *HOST\_AVAILABLE\_UNKNOWN* - неизвестно<br>**1**, *HOST\_AVAILABLE\_TRUE* - доступен<br>**2**, *HOST\_AVAILABLE\_FALSE* - недоступен|
|<|**ipmi\_error**|*строка*|сообщение об ошибке IPMI агента или пустая строка|
|<|**jmx\_available**|*число*|доступность JMX агента<br><br>**0**, *HOST\_AVAILABLE\_UNKNOWN* - неизвестно<br>**1**, *HOST\_AVAILABLE\_TRUE* - доступен<br>**2**, *HOST\_AVAILABLE\_FALSE* - недоступен|
|<|**jmx\_error**|*строка*|сообщение об ошибке JMX агента или пустая строка|
|**history data**|<|*массив*|*(опционально)* массив объектов с данными истории|
|<|**itemid**|*число*|идентификатор элемента данных|
|<|**clock**|*число*|штамп времени значения элемента данных (секунды)|
|<|**ns**|*число*|штамп времени значения элемента данных (наносекунды)|
|<|**value**|*строка*|*(опционально)* значение элемента данных|
|<|**id**|*число*|идентификатор числа (возрастающий счётчик, уникальный в пределах одного сеанса передачи данных)|
|<|**timestamp**|*число*|*(опционально)* штамп времени у элементов данных с типом журнал (лог)|
|<|**source**|*строка*|*(опционально)* значение источника элемента данных журнала событий|
|<|**severity**|*число*|*(опционально)* значение важности элемента данных журнала событий|
|<|**eventid**|*число*|*(опционально)* значение eventid элемента данных журнала событий|
|<|**state**|*строка*|*(опционально)* статус элемента данных<br>**0**, *ITEM\_STATE\_NORMAL*<br>**1**, *ITEM\_STATE\_NOTSUPPORTED*|
|<|**lastlogsize**|*число*|*(опционально)* последний размер журнала у элементов данных с типом журнал (лог)|
|<|**mtime**|*число*|*(опционально)* время модификации журнала у элементов данных с типом журнал (лог)|
|**discovery data**|<|*массив*|*(опционально)* массив объектов с данными обнаружения|
|<|**clock**|*число*|штамп времени данных обнаружения|
|<|**druleid**|*число*|идентификатор правила обнаружения|
|<|**dcheckid**|*число*|идентификатор проверки обнаружения или null для данных правила обнаружения|
|<|**type**|*число*|тип проверки обнаружения:<br><br>**-1** данные правила обнаружения<br>**0**, *SVC\_SSH* - проверка SSH сервиса<br>**1**, *SVC\_LDAP* - проверка LDAP сервиса<br>**2**, *SVC\_SMTP* - проверка SMTP сервиса<br>**3**, *SVC\_FTP* - проверка FTP сервиса<br>**4**, *SVC\_HTTP* - проверка HTTP сервиса<br>**5**, *SVC\_POP* - проверка POP сервиса<br>**6**, *SVC\_NNTP* - проверка NNTP сервиса<br>**7**, *SVC\_IMAP* - проверка IMAP сервиса<br>**8**, *SVC\_TCP* - проверка доступности TCP порта<br>**9**, *SVC\_AGENT* - Zabbix агент<br>**10**, *SVC\_SNMPv1* - SNMPv1 агент<br>**11**, *SVC\_SNMPv2* - SNMPv2 агент<br>**12**, *SVC\_ICMPPING* - ICMP пинг<br>**13**, *SVC\_SNMPv3* - SNMPv3 агент<br>**14**, *SVC\_HTTPS* - проверка HTTPS сервиса<br>**15**, *SVC\_TELNET* - проверка доступности Telnet|
|<|**ip**|*строка*|IP адрес хоста|
|<|**dns**|*строка*|DNS имя хоста|
|<|**port**|*число*|*(опционально)* номер порта сервиса|
|<|**key\_**|*строка*|*(опционально)* ключ элемента данных для проверки обнаружения с типом **9** *SVC\_AGENT*|
|<|**value**|*строка*|*(опционально)* полученное значение от сервиса, может быть пустым по большинству сервисов|
|<|**status**|*число*|*(опционально)* состояние сервиса:<br><br>**0**, *DOBJECT\_STATUS\_UP* - Сервис ДОСТУПЕН<br>**1**, *DOBJECT\_STATUS\_DOWN* - Сервис НЕДОСТУПЕН|
|**auto registration**|<|*массив*|*(опционально)* массив объектов с данными авторегистрации|
|<|**clock**|*число*|штамп времени данных авторегистрации|
|<|**host**|*строка*|имя хоста|
|<|**ip**|*строка*|*(опционально)* IP адрес хоста|
|<|**dns**|*строка*|*(опционально)* разрешенный DNS имя с IP адреса|
|<|**port**|*строка*|*(опционально)* порт хоста|
|<|**host\_metadata**|*строка*|*(опционально)* метаданные хоста, которые отправил агент (на основе параметров конфигурации агента HostMetadata или HostMetadataItem)|
|**tasks**|<|*массив*|*(опционально)* массив задач|
|<|**type**|*число*|тип задачи:<br><br>**0**, *ZBX\_TM\_TASK\_PROCESS\_REMOTE\_COMMAND\_RESULT* - результат удаленной команды|
|<|**status**|*число*|состояние выполнения удаленной команды:<br><br>**0**, *ZBX\_TM\_REMOTE\_COMMAND\_COMPLETED* - удаленная команда завершена успешно<br>**1**, *ZBX\_TM\_REMOTE\_COMMAND\_FAILED* - удаленная команда завершилась с ошибкой|
|<|**error**|*строка*|*(опционально)* сообщение об ошибке|
|<|**parent\_taskid**|*число*|id родительской задачи|
|**more**|<|*число*|*(опционально)* 1 - имеются еще данные истории к отправке|
|**clock**|<|*число*|*(опционально)* штамп времени передачи данных (секунды)|
|**ns**|<|*число*|*(опционально)* штамп времени передачи данных (наносекунды)|
|**version**|<|*строка*|версия прокси (<мажорная>.<минорная>.<номер сборки>)|
|сервер→прокси:|<|<|<|
|**response**|<|*строка*|информация об успешности запроса ('success' или 'failed')|
|**tasks**|<|*массив*|*(опционально)* array of tasks|
|<|**type**|*число*|тип задачи:<br><br>**1**, *ZBX\_TM\_TASK\_PROCESS\_REMOTE\_COMMAND* - удаленная команда|
|<|**clock**|*число*|время создания задачи|
|<|**ttl**|*число*|время в секундах после которого срок задачи истекает|
|<|**commandtype**|*число*|тип удаленной команды:<br><br>**0**, *ZBX\_SCRIPT\_TYPE\_CUSTOM\_SCRIPT* - использование пользовательского скрипта<br>**1**, *ZBX\_SCRIPT\_TYPE\_IPMI* - использование IPMI<br>**2**, *ZBX\_SCRIPT\_TYPE\_SSH* - использование SSH<br>**3**, *ZBX\_SCRIPT\_TYPE\_TELNET* - использование Telnet<br>**4**, *ZBX\_SCRIPT\_TYPE\_GLOBAL\_SCRIPT* - использование глобального скрипта (в настоящее время возможности эквивалентны пользовательской команде)|
|<|**command**|*строка*|выполняемая удаленная команда|
|<|**execute\_on**|*число*|цель выполнения для пользовательских скриптов:<br><br>**0**, *ZBX\_SCRIPT\_EXECUTE\_ON\_AGENT* - выполнение скрипта на стороне агента<br>**1**, *ZBX\_SCRIPT\_EXECUTE\_ON\_SERVER* - выполнение скрипта на стороне сервера<br>**2**, *ZBX\_SCRIPT\_EXECUTE\_ON\_PROXY* - выполнение скрипта на стороне прокси|
|<|**port**|*число*|*(опционально)* порт для telnet и ssh команд|
|<|**authtype**|*число*|*(опционально)* тип аутентификации для ssh команд|
|<|**username**|*строка*|*(опционально)* имя пользователя для telnet и ssh команд|
|<|**password**|*строка*|*(опционально)* пароль для telnet и ssh команд|
|<|**publickey**|*строка*|*(опционально)* публичный ключ для ssh команд|
|<|**privatekey**|*строка*|*(опционально)* приватный ключ для ssh команд|
|<|**parent\_taskid**|*число*|id родительской задачи|
|<|**hostid**|*число*|hostid цели|

Пример:

пркси→сервер:

``` {.javascript}
{
  "request": "proxy data",
  "host": "Proxy #12", 
  "session": "12345678901234567890123456789012",
    "host availability":[
        {
            "hostid":10106,
            "available":1,
            "error":"",
            "snmp_available":0,
            "snmp_error":"",
            "ipmi_available":0,
            "ipmi_error":"",
            "jmx_available":0,
            "jmx_error":""
        },
        {
            "hostid":10107,
            "available":1,
            "error":"",
            "snmp_available":0,
            "snmp_error":"",
            "ipmi_available":0,
            "ipmi_error":"",
            "jmx_available":0,
            "jmx_error":""
        }
    ],
    "history data":[
        {
            "itemid":"12345",
            "clock":1478609647,
            "ns":332510044,
            "value":"52956612",
            "id": 1
        },
        {
            "itemid":"12346",
            "clock":1478609647,
            "ns":330690279,
            "state":1,
            "value":"Cannot find information for this network interface in /proc/net/dev.",
            "id": 2
        }
    ],
    "discovery data":[
        {
            "clock":1478608764,
            "drule":2,
            "dcheck":3,
            "type":12,
            "ip":"10.3.0.10",
            "dns":"vdebian",
            "status":1
        },
        {
            "clock":1478608764,
            "drule":2,
            "dcheck":null,
            "type":-1,
            "ip":"10.3.0.10",
            "dns":"vdebian",
            "status":1
        }
    ],
    "auto registration":[
        {
            "clock":1478608371,
            "host":"Logger1",
            "ip":"10.3.0.1",
            "dns":"localhost",
            "port":"10050"
        },
        {
            "clock":1478608381,
            "host":"Logger2",
            "ip":"10.3.0.2",
            "dns":"localhost",
            "port":"10050"
        }
    ],
    "tasks":[
        {
            "type": 2,
            "clock":1478608371,
            "ttl": 600,
            "commandtype": 2,
            "command": "restart_service1.sh",
            "execute_on": 2,
            "port": 80,
            "authtype": 0,
            "username": "userA",
            "password": "password1",
            "publickey": "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCqGKukO1De7zhZj6+H0qtjTkVxwTCpvKe",
            "privatekey": "lsuusFncCzWBQ7RKNUSesmQRMSGkVb1/3j+skZ6UtW+5u09lHNsj6tQ5QCqGKukO1De7zhd",
            "parent_taskid": 10,
            "hostid": 10070
        },
        {
            "type": 2,
            "clock":1478608381,
            "ttl": 600,
            "commandtype": 1,
            "command": "restart_service2.sh",
            "execute_on": 0,
            "authtype": 0,
            "username": "",
            "password": "",
            "publickey": "",
            "privatekey": "",
            "parent_taskid": 20,
            "hostid": 10084
        }
    ],
    "tasks":[
        {
            "type": 0,
            "status": 0,
            "parent_taskid": 10
        },
        {
            "type": 0,
            "status": 1,
            "error": "No permissions to execute task.",
            "parent_taskid": 20
        }
    ], 
    "version":"3.4.0"
}
```

сервер→прокси:

``` {.javascript}
{
  "response": "success",
  "tasks":[
      {
         "type": 1,
         "clock": 1478608371,
         "ttl": 600,
         "commandtype": 2,
         "command": "restart_service1.sh",
         "execute_on": 2,
         "port": 80,
         "authtype": 0,
         "username": "userA",
         "password": "password1",
         "publickey": "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCqGKukO1De7zhZj6+H0qtjTkVxwTCpvKe",
         "privatekey": "lsuusFncCzWBQ7RKNUSesmQRMSGkVb1/3j+skZ6UtW+5u09lHNsj6tQ5QCqGKukO1De7zhd",
         "parent_taskid": 10,
         "hostid": 10070
      },
      {
         "type": 1,
         "clock": 1478608381,
         "ttl": 600,
         "commandtype": 1,
         "command": "restart_service2.sh",
         "execute_on": 0,
         "authtype": 0,
         "username": "",
         "password": "",
         "publickey": "",
         "privatekey": "",
         "parent_taskid": 20,
         "hostid": 10084
      }
  ]
}
```

#### Обратная совместимость

Сервер поддерживает частичную обратную совместимость, принимая старые
запросы `host availability`, `history data`, `discovery data` и
`auto registration`.

[comment]: # ({/new-ef0ba621})
