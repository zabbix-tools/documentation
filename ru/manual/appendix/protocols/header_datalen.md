[comment]: # translation:outdated

[comment]: # ({new-9153def3})
# 4 Длина заголовка и данных

[comment]: # ({/new-9153def3})

[comment]: # ({new-faa3c87b})
#### Обзор

Заголовок и длина данных присутствуют в сообщениях ответа и запроса
между компонентами Zabbix. Это требуется для определения длины
сообщения.

    <HEADER> - "ZBXD\x01" (5 байт)
    <DATALEN> - размер данных (8 байт). число 1 отформатируется в 01/00/00/00/00/00/00/00 (восемь байт, 64-битное число в little-endian формате)

Чтобы не израсходовать память полностью (в теории) Zabbix протокол
ограничен на прием данных только 128МБ за одно соединение.

[comment]: # ({/new-faa3c87b})

[comment]: # ({new-409c40cc})

#### Structure

The header consists of four fields. All numbers in the header are formatted as little-endian.

|Field|Size|Size<br>(large packet)|Description|
|--|-|-|------|
|`<PROTOCOL>`|4|4|`"ZBXD"` or `5A 42 58 44`|
|`<FLAGS>`|1|1|Protocol flags:<br>`0x01` - Zabbix communications protocol<br>`0x02` - compression<br>`0x04` - large packet|
|`<DATALEN>`|4|8|Data length.|
|`<RESERVED>`|4|8|When compression is used (`0x02` flag) - the length of uncompressed data<br>When compression is not used - `00 00 00 00`|

[comment]: # ({/new-409c40cc})

[comment]: # ({new-8698de59})
#### Реализация

Здесь представлены выдержки кода, которые показывают как добавить
заголовок Zabbix протокола к `data`, которые вы *хотите* отправить,
чтобы получить `packet` вам *необходимо* отправлять на Zabbix, так чтобы
он интерпретировался корректным образом.

|Язык|Код|
|--------|------|
|bash|`printf -v LENGTH '%016x' "${#DATA}"PACK=""for i in {14..0..-2}; do PACK="$PACK\\x${LENGTH:$i:2}"; doneprintf "ZBXD\1$PACK%s" $DATA`{.bash}|
|Java|`byte[] header = new byte[] {'Z', 'B', 'X', 'D', '\1',(byte)(data.length & 0xFF),(byte)((data.length >> 8) & 0xFF),(byte)((data.length >> 16) & 0xFF),(byte)((data.length >> 24) & 0xFF),'\0', '\0', '\0', '\0'};|
|<|byte[] packet = new byte[header.length + data.length];System.arraycopy(header, 0, packet, 0, header.length);System.arraycopy(data, 0, packet, header.length, data.length);`{.Java}|
|PHP|`$packet = "ZBXD\1" . pack('P', strlen($data)) . $data;`{.PHP} или `$packet = "ZBXD\1" . pack('V', strlen($data)) . "\0\0\0\0" . $data;`{.PHP}|
|Perl|`my $packet = "ZBXD\1" . pack('<Q', length($data)) . $data;`{.Perl}или`my $packet = "ZBXD\1" . pack('V', length($data)) . "\0\0\0\0" . $data;`{.Perl}|
|Python|`packet = "ZBXD\1" + struct.pack('<Q', len(data)) + data`{.Python}|

[comment]: # ({/new-8698de59})
