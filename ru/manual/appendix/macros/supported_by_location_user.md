[comment]: # translation:outdated

[comment]: # ({new-d428738a})
# 2 Пользовательские макросы поддерживаемые по назначению

[comment]: # ({/new-d428738a})

[comment]: # ({new-f0afb8e0})
#### Обзор

[Определяемые пользователями](/ru/manual/config/macros/usermacros)
макросы поддерживаются в следующих местах:

-   Узлы сети
    -   IP/DNS интерфейса
    -   Порт интерфейса

```{=html}
<!-- -->
```
-   Пассивные прокси
    -   Порт интерфейса

```{=html}
<!-- -->
```
-   Элементы данных и прототипы элементов данных
    -   Имя (*устарело*)
    -   Параметры ключей
    -   Интервал обновления
    -   Пользовательские интервалы
    -   Период хранения истории
    -   Период хранения динамики изменений
    -   Имя контекста SNMPv3
    -   Имя безопасности SNMPv3
    -   Пароль аутентификации SNMPv3
    -   Ключевая фраза безопасности SNMPv3
    -   SNMPv1/v2 community
    -   SNMP OID
    -   SNMP порт
    -   Имя пользователя SSH
    -   Публичный ключ SSH
    -   Приватный ключ SSH
    -   Пароль к SSH
    -   SSH скрипт
    -   Имя пользователя к Telnet
    -   Пароль к Telnet
    -   Telnet скрипт
    -   [Формула](/ru/manual/config/items/itemtypes/calculated#настраиваемые_поля)
        вычисляемого элемента данных
    -   Поле "Разрешенные узлы сети" траппер элемента данных
    -   Дополнительные параметры мониторинга баз данных
    -   Поле элемента данных JMX endpoint
    -   начиная с Zabbix 4.0 также в:
        -   шагах предобработки значений элементов данных
        -   поле URL HTTP агента
        -   HTTP полях запросов HTTP агента
        -   поле тела запроса HTTP агента
        -   поле кодов состояний HTTP агента
        -   поле заголовков ключа и значения HTTP агента
        -   поле имени пользователя HTTP аутентификации HTTP агента
        -   поле пароля HTTP аутентификации HTTP агента
        -   поле HTTP прокси HTTP агента
        -   поле файла SSL сертификата HTTP агента
        -   поле файла SSL ключа HTTP агента
        -   поле пароля к SSL ключу HTTP агента
        -   поле HTTP времени ожидания HTTP агента
        -   поле HTTP разрешенных хостов HTTP агента

```{=html}
<!-- -->
```
-   Обнаружения

```{=html}
<!-- -->
```
        * Интервал обновления
        * Имя контекста SNMPv3
        * Имя безопасности SNMPv3
        * Пароль аутентификации SNMPv3
        * Ключевая фраза безопасности SNMPv3
        * SNMPv1/v2 community
        * SNMP OID

-   Правило низкоуровневого обнаружения

```{=html}
<!-- -->
```
        * Имя
        * Параметры ключей
        * Интервал обновления
        * Пользовательские интервалы
        * Имя контекста SNMPv3
        * Имя безопасности SNMPv3
        * Пароль аутентификации SNMPv3
        * Ключевая фраза безопасности SNMPv3
        * SNMPv1/v2 community
        * SNMP OID
        * SNMP порт
        * Имя пользователя SSH
        * Публичный ключ SSH
        * Приватный ключ SSH
        * Пароль к SSH
        * SSH скрипт
        * Имя пользователя к Telnet
        * Пароль к Telnet
        * Telnet скрипт
        * Поле "Разрешенные узлы сети" траппер элемента данных
        * Дополнительные параметры мониторинга баз данных
        * Поле элемента данных JMX endpoint
        * Период хранения потерянных ресурсов
        * Регулярные выражения в фильтре
        * начиная с Zabbix 4.0 также в:
          * поле URL HTTP агента
          * HTTP полях запросов HTTP агента
          * поле запроса тела HTTP агента
          * поле требуемых кодов состояний HTTP агента
          * поле заголовков ключа и значения HTTP агента
          * поле имени пользователя HTTP аутентификации HTTP агента
          * поле пароля HTTP аутентификации HTTP агента
          * поле HTTP времени ожидания HTTP агента

    * Веб-сценарий
        * Имя
        * Интервал обновления
        * Агент
        * HTTP прокси
        * Переменные
        * Заголовки
        * Имя шага
        * URL шага
        * Post переменные шага
        * Заголовки шага
        * Время ожидания шага
        * Требуемая строка
        * Требуемые коды состояний
        * Аутентификация
        * Файл сертификата SSL
        * Файл ключа SSL
        * Пароль к ключу SSL
       
    * Триггеры
        * Имя
        * Выражение (только в константах и в параметрах функций)
        * Описание
        * URL

-   Оповещения, основанные на триггерах
-   Оповещения о внутренних событиях, основанные на триггерах
-   Оповещения обновления проблем

```{=html}
<!-- -->
```
-   Теги событий

```{=html}
<!-- -->
```
        * Имя тега
        * Значение тега
        * Тег для поиска совпадений

-   Операции действий
    -   Длительность шага операции по умолчанию
    -   Длительность шага

```{=html}
<!-- -->
```
-   Условия действий
    -   Условие периода времени

```{=html}
<!-- -->
```
-   Глобальные скрипты (включая текст подтверждения)

```{=html}
<!-- -->
```
-   Поле URL динамического элемента комплексного экрана URL

```{=html}
<!-- -->
```
-   Администрирование → Пользователи → Оповещения: поле 'Когда активно'
-   Администрирование → Общие → Рабочее время: поле 'Рабочее время'

Полный список всех поддерживаемых макросов в Zabbix смотрите [макросы
поддерживаемые по
назначению](/ru/manual/appendix/macros/supported_by_location).

[comment]: # ({/new-f0afb8e0})












[comment]: # ({new-7cb4c86b})
#### Actions

In [actions](/manual/config/notifications/action), user macros can be
used in the following fields:

|Location|<|Multiple macros/mix with text^[1](supported_by_location_user#footnotes)^|
|--------|-|------------------------------------------------------------------------|
|Trigger-based notifications and commands|<|yes|
|Trigger-based internal notifications|<|yes|
|Problem update notifications|<|yes|
|Service-based notifications and commands|<|yes|
|Service update notifications|<|yes|
|Time period condition|<|no|
|*Operations*|<|<|
|<|Default operation step duration|no|
|^|Step duration|no|

[comment]: # ({/new-7cb4c86b})

[comment]: # ({new-f30645e0})
#### Hosts/host prototypes

In a [host](/manual/config/hosts/host) and [host
prototype](/manual/vm_monitoring#host_prototypes) configuration, user
macros can be used in the following fields:

|Location|<|Multiple macros/mix with text^[1](supported_by_location_user#footnotes)^|
|--------|-|------------------------------------------------------------------------|
|Interface IP/DNS|<|DNS only|
|Interface port|<|no|
|*SNMP v1, v2*|<|<|
|<|SNMP community|yes|
|*SNMP v3*|<|<|
|<|Context name|yes|
|^|Security name|yes|
|^|Authentication passphrase|yes|
|^|Privacy passphrase|yes|
|*IPMI*|<|<|
|<|Username|yes|
|^|Password|yes|
|//Tags //|<|<|
|<|Tag names|yes|
|^|Tag values|yes|

[comment]: # ({/new-f30645e0})

[comment]: # ({new-d51563d6})
#### Items / item prototypes

In an [item](/manual/config/items/item) or an [item
prototype](/manual/discovery/low_level_discovery#item_prototypes)
configuration, user macros can be used in the following fields:

|Location|<|Multiple macros/mix with text^[1](supported_by_location_user#footnotes)^|
|--------|-|------------------------------------------------------------------------|
|Name (deprecated)|<|yes|
|Item key parameters|<|yes|
|Update interval|<|no|
|Custom intervals|<|no|
|History storage period|<|no|
|Trend storage period|<|no|
|Description|<|yes|
|*Calculated item*|<|<|
|<|Formula|yes|
|*Database monitor*|<|<|
|<|Username|yes|
|^|Password|yes|
|^|SQL query|yes|
|*HTTP agent*|<|<|
|<|URL^[2](supported_by_location_user#footnotes)^|yes|
|^|Query fields|yes|
|^|Timeout|no|
|^|Request body|yes|
|^|Headers (names and values)|yes|
|^|Required status codes|yes|
|^|HTTP proxy|yes|
|^|HTTP authentication username|yes|
|^|HTTP authentication password|yes|
|^|SSl certificate file|yes|
|^|SSl key file|yes|
|^|SSl key password|yes|
|^|Allowed hosts|yes|
|*JMX agent*|<|<|
|<|JMX endpoint|yes|
|*Script item*|<|<|
|<|Parameter names and values|yes|
|*SNMP agent*|<|<|
|<|SNMP OID|yes|
|*SSH agent*|<|<|
|<|Username|yes|
|^|Public key file|yes|
|^|Private key file|yes|
|^|Password|yes|
|^|Script|yes|
|*TELNET agent*|<|<|
|<|Username|yes|
|^|Password|yes|
|^|Script|yes|
|*Zabbix trapper*|<|<|
|<|Allowed hosts|yes|
|*Tags*|<|<|
|<|Tag names|yes|
|^|Tag values|yes|
|*Preprocessing*|<|<|
|<|Step parameters (including custom scripts)|yes|

[comment]: # ({/new-d51563d6})

[comment]: # ({new-747bd0ca})
#### Low-level discovery

In a [low-level discovery
rule](/manual/discovery/low_level_discovery#configuring_low-level_discovery),
user macros can be used in the following fields:

|Location|<|Multiple macros/mix with text^[1](supported_by_location_user#footnotes)^|
|--------|-|------------------------------------------------------------------------|
|Name|<|yes|
|Key parameters|<|yes|
|Update interval|<|no|
|Custom interval|<|no|
|Keep lost resources period|<|no|
|Description|<|yes|
|*SNMP agent*|<|<|
|<|SNMP OID|yes|
|*SSH agent*|<|<|
|<|Username|yes|
|^|Public key file|yes|
|^|Private key file|yes|
|^|Password|yes|
|^|Script|yes|
|*TELNET agent*|<|<|
|<|Username|yes|
|^|Password|yes|
|^|Script|yes|
|*Zabbix trapper*|<|<|
|<|Allowed hosts|yes|
|*Database monitor*|<|<|
|<|Additional parameters|yes|
|*JMX agent*|<|<|
|<|JMX endpoint|yes|
|*HTTP agent*|<|<|
|<|URL^[2](supported_by_location_user#footnotes)^|yes|
|^|Query fields|yes|
|^|Timeout|no|
|^|Request body|yes|
|^|Headers (names and values)|yes|
|^|Required status codes|yes|
|^|HTTP authentication username|yes|
|^|HTTP authentication password|yes|
|*Filters*|<|<|
|<|Regular expression|yes|
|*Overrides*|<|<|
|<|Filters: regular expression|yes|
|^|Operations: update interval (for item prototypes)|no|
|^|Operations: history storage period (for item prototypes)|no|
|^|Operations: trend storage period (for item prototypes)|no|

[comment]: # ({/new-747bd0ca})

[comment]: # ({new-95f2f372})
#### Network discovery

In a [network discovery rule](/manual/discovery/network_discovery/rule),
user macros can be used in the following fields:

|Location|<|Multiple macros/mix with text^[1](supported_by_location_user#footnotes)^|
|--------|-|------------------------------------------------------------------------|
|Update interval|<|no|
|*SNMP v1, v2*|<|<|
|<|SNMP community|yes|
|^|SNMP OID|yes|
|*SNMP v3*|<|<|
|<|Context name|yes|
|^|Security name|yes|
|^|Authentication passphrase|yes|
|^|Privacy passphrase|yes|
|^|SNMP OID|yes|

[comment]: # ({/new-95f2f372})

[comment]: # ({new-575d88fc})
#### Proxies

In a [proxy](/manual/distributed_monitoring/proxies#configuration)
configuration, user macros can be used in the following field:

|Location|<|Multiple macros/mix with text^[1](supported_by_location_user#footnotes)^|
|--------|-|------------------------------------------------------------------------|
|Interface port (for passive proxy)|<|no|

[comment]: # ({/new-575d88fc})

[comment]: # ({new-32b6aef1})
#### Templates

In a [template](/manual/config/templates/template) configuration, user
macros can be used in the following fields:

|Location|<|Multiple macros/mix with text^[1](supported_by_location_user#footnotes)^|
|--------|-|------------------------------------------------------------------------|
|*Tags*|<|<|
|<|Tag names|yes|
|^|Tag values|yes|

[comment]: # ({/new-32b6aef1})

[comment]: # ({new-c78b04ef})
#### Triggers

In a [trigger](/manual/config/triggers/trigger) configuration, user
macros can be used in the following fields:

|Location|<|Multiple macros/mix with text^[1](supported_by_location_user#footnotes)^|
|--------|-|------------------------------------------------------------------------|
|Name|<|yes|
|Operational data|<|yes|
|Expression (only in constants and function parameters; secret macros are not supported).|<|yes|
|Description|<|yes|
|URL^[2](supported_by_location_user#footnotes)^|<|yes|
|Tag for matching|<|yes|
|*Tags*|<|<|
|<|Tag names|yes|
|^|Tag values|yes|

[comment]: # ({/new-c78b04ef})

[comment]: # ({new-78f1f511})
#### Web scenario

In a [web scenario](/manual/web_monitoring) configuration, user macros
can be used in the following fields:

|Location|<|Multiple macros/mix with text^[1](supported_by_location_user#footnotes)^|
|--------|-|------------------------------------------------------------------------|
|Name|<|yes|
|Update interval|<|no|
|Agent|<|yes|
|HTTP proxy|<|yes|
|Variables (values only)|<|yes|
|Headers (names and values)|<|yes|
|*Steps*|<|<|
|<|Name|yes|
|^|URL^[2](supported_by_location_user#footnotes)^|yes|
|^|Variables (values only)|yes|
|^|Headers (names and values)|yes|
|^|Timeout|no|
|^|Required string|yes|
|^|Required status codes|no|
|*Authentication*|<|<|
|<|User|yes|
|^|Password|yes|
|^|SSL certificate|yes|
|^|SSL key file|yes|
|^|SSL key password|yes|
|*Tags*|<|<|
|<|Tag names|yes|
|^|Tag values|yes|

[comment]: # ({/new-78f1f511})

[comment]: # ({new-efe84e1d})
#### Other locations

In addition to the locations listed here, user macros can be used in the
following fields:

|Location|<|Multiple macros/mix with text^[1](supported_by_location_user#footnotes)^|
|--------|-|------------------------------------------------------------------------|
|Global scripts (script, SSH, Telnet, IPMI), including confirmation text|<|yes|
|Webhooks|<|<|
|<|JavaScript script|no|
|<|JavaScript script parameter name|no|
|<|JavaScript script parameter value|yes|
|*Monitoring → Dashboards*|<|<|
|<|URL^[2](supported_by_location_user#footnotes)^ field of *dynamic URL* dashboard widget|yes|
|*Administration → Users → Media*|<|<|
|<|When active|no|
|*Administration → General → GUI*|<|<|
|<|Working time|no|
|*Administration → Media types → Message templates*|<|<|
|<|Subject|yes|
|^|Message|yes|

For a complete list of all macros supported in Zabbix, see [supported
macros](/manual/appendix/macros/supported_by_location).

[comment]: # ({/new-efe84e1d})

[comment]: # ({new-5a370e78})
##### Footnotes

^**1**^ If multiple macros in a field or macros mixed with text are not
supported for the location, a single macro has to fill the whole field.

^**2**^ URLs that contain a [secret
macro](/manual/config/macros/user_macros#configuration) will not work,
as the macro in them will be resolved as "\*\*\*\*\*\*".

[comment]: # ({/new-5a370e78})
