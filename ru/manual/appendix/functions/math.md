[comment]: # attributes: notoc

[comment]: # translation:outdated

[comment]: # ({new-ef4bef6c})
FIXME **This page is not fully translated, yet. Please help completing
the translation.**\
*(remove this paragraph once the translation is finished)*

# 5 Mathematical functions

All functions listed here are supported in:

-   [Trigger expressions](/manual/config/triggers/expression)
-   [Calculated items](/manual/config/items/itemtypes/calculated)

Mathematical functions are supported with float and integer value types,
unless stated otherwise.

Some general notes on function parameters:

-   Function parameters are separated by a comma
-   Expressions are accepted as parameters
-   Optional function parameters (or parameter parts) are indicated by
    `<` `>`

|FUNCTION|<|<|<|
|--------|-|-|-|
|<|**Description**|**Function-specific parameters**|**Comments**|
|**abs**|<|<|<|
|<|The amount of absolute difference between the previous and latest value.<br><br>This function references **change()** to obtain the absolute difference (see example).|<|Supported value types: float, int, str, text, log<br><br>For strings returns:<br>0 - values are equal<br>1 - values differ<br><br>Example:<br>=> **abs**(change(/host/key))>10<br><br>Absolute numeric difference will be calculated, as seen with these incoming example values ('previous' and 'latest' value = absolute difference):<br>'1' and '5' = `4`<br>'3' and '1' = `2`<br>'0' and '-2.5' = `2.5`|
|**acos**|<|<|<|
|<|The arccosine of a value as an angle, expressed in radians.|<|The value must be between -1 and 1.<br><br>For example, the arccosine of a value '0.5' will be '2.0943951'.<br><br>Example:<br>=> **acos**(last(/host/key))|
|**asin**|<|<|<|
|<|The arcsine of a value as an angle, expressed in radians.|<|The value must be between -1 and 1.<br><br>For example, the arcsine of a value '0.5' will be '-0.523598776'.<br><br>Example:<br>=> **asin**(last(/host/key))|
|**atan**|<|<|<|
|<|The arctangent of a value as an angle, expressed in radians.|<|For example, the arctangent of a value '1' will be '0.785398163'.<br><br>Example:<br>=> **atan**(last(/host/key))|
|**atan2** (abscissa)|<|<|<|
|<|The arctangent of the ordinate (exprue) and abscissa coordinates specified as an angle, expressed in radians.|<|For example, the arctangent of the ordinate and abscissa coordinates of a value '1' will be '2.21429744'.<br><br>Example:<br>=> **atan2**(last(/host/key),2)|
|**avg** (<value1>,<value2>,...)|<|<|<|
|<|Average value of the referenced item values.|**valueX** - value returned by one of history functions|Example:<br>=> **avg**(avg(/host/key),avg(/host2/key2))|
|**cbrt**|<|<|<|
|<|Cube root of a value.|<|For example, the cube root of '64' will be '4', of '63' will be '3.97905721'.<br><br>Example:<br>=> **cbrt**(last(/host/key))|
|**ceil**|<|<|<|
|<|Round the value up to the nearest greater integer.|<|For example, '2.4' will be rounded up to '3'.<br><br>Example:<br>=> **ceil**(last(/host/key))<br><br>See also floor().|
|**cos**|<|<|<|
|<|The cosine of a value, where the value is an angle expressed in radians.|<|For example, the cosine of a value '1' will be '0.54030230586'.<br><br>Example:<br>=> **cos**(last(/host/key))|
|**cosh**|<|<|<|
|<|The hyperbolic cosine of a value.|<|For example, the hyperbolic cosine of a value '1' will be '1.54308063482'.<br><br>Returns value as a real number, not as scientific notation.<br><br>Example:<br>=> **cosh**(last(/host/key))|
|**cot**|<|<|<|
|<|The cotangent of a value, where the value is an angle, expressed in radians.|<|For example, the cotangent of a value '1' will be '0.54030230586'.<br><br>Example:<br>=> **cot**(last(/host/key))|
|**degrees**|<|<|<|
|<|Converts a value from radians to degrees.|<|For example, a value '1' converted to degrees will be '57.2957795'.<br><br>Example:<br>=> **degrees**(last(/host/key))|
|**e**|<|<|<|
|<|Euler's number (2.718281828459045).|<|Example:<br>=> **e**()|
|**exp**|<|<|<|
|<|Euler's number at a power of a value.|<|For example, Euler's number at a power of a value '2' will be '7.38905609893065'.<br><br>Example:<br>=> **exp**(last(/host/key))|
|**expm1**|<|<|<|
|<|Euler's number at a power of a value minus 1.|<|For example, Euler's number at a power of a value '2' minus 1 will be '6.38905609893065'.<br><br>Example:<br>=> **expm1**(last(/host/key))|
|**floor**|<|<|<|
|<|Round the value down to the nearest smaller integer.|<|For example, '2.6' will be rounded down to '2'.<br><br>Example:<br>=> **floor**(last(/host/key))<br><br>See also ceil().|
|**log**|<|<|<|
|<|Natural logarithm.|<|For example, the natural logarithm of a value '2' will be '0.69314718055994529'.<br><br>Example:<br>=> **log**(last(/host/key))|
|**log10**|<|<|<|
|<|Decimal logarithm.|<|For example, the decimal logarithm of a value '5' will be '0.69897000433'.<br><br>Example:<br>=> **log10**(last(/host/key))|
|**max** (<value1>,<value2>,...)|<|<|<|
|<|Highest value of the referenced item values.|**valueX** - value returned by one of history functions|Example:<br>=> **max**(avg(/host/key),avg(/host2/key2))|
|**min** (<value1>,<value2>,...)|<|<|<|
|<|Lowest value of the referenced item values.|**valueX** - value returned by one of history functions|Example:<br>=> **min**(avg(/host/key),avg(/host2/key2))|
|**mod** (denominator)|<|<|<|
|<|Division remainder.|**denominator** - division denominator|For example, division remainder of a value '5' with division denominator '2' will be '1'.<br><br>Example:<br>=> **mod**(last(/host/key),2)|
|**pi**|<|<|<|
|<|Pi constant (3.14159265358979).|<|Example:<br>=> **pi**()|
|**power** (power value)|<|<|<|
|<|The power of a value.|**power value** - the Nth power to use|For example, the 3rd power of a value '2' will be '8'.<br><br>Example:<br>=> **power**(last(/host/key),3)|
|**radians**|<|<|<|
|<|Convert a value from degrees to radians.|<|For example, a value '1' converted to radians will be '0.0174532925'.<br><br>Example:<br>=> **radians**(last(/host/key))|
|**rand**|<|<|<|
|<|Return a random integer value.|<|A pseudo-random generated number using time as seed (enough for mathematical purposes, but not cryptography).<br><br>Example:<br>=> **rand**()|
|**round** (decimal places)|<|<|<|
|<|Round the value to decimal places.|**decimal places** - specify decimal places for rounding (0 is also possible)|For example, a value '2.5482' rounded to 2 decimal places will be '2.55'.<br><br>Example:<br>=> **round**(last(/host/key),2)|
|**signum**|<|<|<|
|<|Returns '-1' if a value is negative, '0' if a value is zero, '1' if a value is positive.|<|Example:<br>=> **signum**(last(/host/key))|
|**sin**|<|<|<|
|<|The sine of a value, where the value is an angle expressed in radians.|<|For example, the sine of a value '1' will be '0.8414709848'.<br><br>Example:<br>=> **sin**(last(/host/key))|
|**sinh**|<|<|<|
|<|The hyperbolical sine of a value.|<|For example, the hyperbolical sine of a value '1' will be '1.17520119364'.<br><br>Example:<br>=> **sinh**(last(/host/key))|
|**sqrt**|<|<|<|
|<|Square root of a value.|<|This function will fail with a negative value.<br><br>For example, the square root of a value '3.5' will be '1.87082869339'.<br><br>Example:<br>=> **sqrt**(last(/host/key))|
|**sum** (<value1>,<value2>,...)|<|<|<|
|<|Sum of the referenced item values.|**valueX** - value returned by one of history functions|Example:<br>=> **sum**(avg(/host/key),avg(/host2/key2))|
|**tan**|<|<|<|
|<|The tangent of a value.|<|For example, the tangent of a value '1' will be '1.55740772465'.<br><br>Example:<br>=> **tan**(last(/host/key))|
|**truncate** (decimal places)|<|<|<|
|<|Truncate the value to decimal places.|**decimal places** - specify decimal places for truncating (0 is also possible)|Example:<br>=> **truncate**(last(/host/key),2)|

[comment]: # ({/new-ef4bef6c})

[comment]: # ({new-f932d69d})
### Function details

Some general notes on function parameters:

-   Function parameters are separated by a comma
-   Expressions are accepted as parameters
-   Optional function parameters (or parameter parts) are indicated by
    `<` `>`

[comment]: # ({/new-f932d69d})

[comment]: # ({new-89cbc486})

##### abs(value) {#abs}

The absolute value of a value.<br>
Supported value types: *Float*, *Integer*, *String*, *Text*, *Log*.<br>
For strings returns: 0 - the values are equal; 1 - the values differ.

Parameter: 

-   **value** - the value to check

The absolute numeric difference will be calculated, as seen with these incoming example values ('previous' and 'latest' value = absolute difference): '1' and '5' = `4`; '3' and '1' = `2`; '0' and '-2.5' = `2.5`

Example:

    abs(last(/host/key))>10

[comment]: # ({/new-89cbc486})

[comment]: # ({new-f40a2e61})

##### acos(value) {#acos}

The arccosine of a value as an angle, expressed in radians.

Parameter: 

-   **value** - the value to check

The value must be between -1 and 1. For example, the arccosine of a value '0.5' will be '2.0943951'.

Example:

    acos(last(/host/key))

[comment]: # ({/new-f40a2e61})

[comment]: # ({new-b44dbca5})

##### asin(value) {#asin}

The arcsine of a value as an angle, expressed in radians.

Parameter: 

-   **value** - the value to check

The value must be between -1 and 1. For example, the arcsine of a value '0.5' will be '-0.523598776'.

Example:

    asin(last(/host/key)) 

[comment]: # ({/new-b44dbca5})

[comment]: # ({new-08e0843c})

##### atan(value) {#atan}

The arctangent of a value as an angle, expressed in radians.

Parameter: 

-   **value** - the value to check

The value must be between -1 and 1. For example, the arctangent of a value '1' will be '0.785398163'.

Example:

    atan(last(/host/key))

[comment]: # ({/new-08e0843c})

[comment]: # ({new-6df7b343})

##### atan2(value,abscissa) {#atan2}

The arctangent of the ordinate (exprue) and abscissa coordinates specified as an angle, expressed in radians.

Parameter: 

-   **value** - the value to check;
-   **abscissa** - the abscissa value.

For example, the arctangent of the ordinate and abscissa coordinates of a value '1' will be '2.21429744'.

Example:

    atan(last(/host/key),2)

[comment]: # ({/new-6df7b343})

[comment]: # ({new-6d42ceda})

##### avg(<value1>,<value2>,...) {#avg}

The average value of the referenced item values.

Parameter: 

-   **valueX** - the value returned by one of the history functions.

Example:

    avg(avg(/host/key),avg(/host2/key2))

[comment]: # ({/new-6d42ceda})

[comment]: # ({new-65742fe9})

##### cbrt(value) {#cbrt}

The cube root of a value.

Parameter: 

-   **value** - the value to check

For example, the cube root of '64' will be '4', of '63' will be '3.97905721'.

Example:

    cbrt(last(/host/key))

[comment]: # ({/new-65742fe9})

[comment]: # ({new-4290a457})

##### ceil(value) {#ceil}

Round the value up to the nearest greater or equal integer.

Parameter: 

-   **value** - the value to check

For example, '2.4' will be rounded up to '3'. See also [floor()](#floor).

Example:

    ceil(last(/host/key))

[comment]: # ({/new-4290a457})

[comment]: # ({new-b07f5dfa})

##### cos(value) {#cos}

The cosine of a value, where the value is an angle expressed in radians.

Parameter: 

-   **value** - the value to check

For example, the cosine of a value '1' will be '0.54030230586'.

Example:

    cos(last(/host/key))

[comment]: # ({/new-b07f5dfa})

[comment]: # ({new-4df693f6})

##### cosh(value) {#cosh}

The hyperbolic cosine of a value.
Returns the value as a real number, not as scientific notation.

Parameter: 

-   **value** - the value to check

For example, the hyperbolic cosine of a value '1' will be '1.54308063482'.

Example:

    cosh(last(/host/key))

[comment]: # ({/new-4df693f6})

[comment]: # ({new-7b0480bb})

##### cot(value) {#cot}

The cotangent of a value, where the value is an angle expressed in radians.

Parameter: 

-   **value** - the value to check

For example, the cotangent of a value '1' will be '0.54030230586'.

Example:

    cot(last(/host/key))

[comment]: # ({/new-7b0480bb})

[comment]: # ({new-552d8ac9})

##### degrees(value) {#degrees}

Converts a value from radians to degrees.

Parameter: 

-   **value** - the value to check

For example, a value '1' converted to degrees will be '57.2957795'.

Example:

    degrees(last(/host/key))

[comment]: # ({/new-552d8ac9})

[comment]: # ({new-790837bf})

##### e {#e}

The Euler's number (2.718281828459045).

Example:

    e()

[comment]: # ({/new-790837bf})

[comment]: # ({new-5f0104cf})

##### exp(value) {#exp}

The Euler's number at a power of a value.

Parameter: 

-   **value** - the value to check

For example, Euler's number at a power of a value '2' will be '7.38905609893065'.

Example:

    exp(last(/host/key))

[comment]: # ({/new-5f0104cf})

[comment]: # ({new-fa7bce72})

##### expm1(value) {#expm1}

The Euler's number at a power of a value minus 1.

Parameter: 

-   **value** - the value to check

For example, Euler's number at a power of a value '2' minus 1 will be '6.38905609893065'.

Example:

    expm1(last(/host/key))

[comment]: # ({/new-fa7bce72})

[comment]: # ({new-367cea41})

##### floor(value) {#floor}

Round the value down to the nearest smaller or equal integer.

Parameter: 

-   **value** - the value to check

For example, '2.6' will be rounded down to '2'. See also [ceil()](#ceil).

Example:

    floor(last(/host/key))

[comment]: # ({/new-367cea41})

[comment]: # ({new-02960b00})

##### log(value) {#log}

The natural logarithm.

Parameter: 

-   **value** - the value to check

For example, the natural logarithm of a value '2' will be '0.69314718055994529'.

Example:

    log(last(/host/key))

[comment]: # ({/new-02960b00})

[comment]: # ({new-9eb02241})

##### log10(value) {#log10}

The decimal logarithm.

Parameter: 

-   **value** - the value to check

For example, the decimal logarithm of a value '5' will be '0.69897000433'.

Example:

    log10(last(/host/key))

[comment]: # ({/new-9eb02241})

[comment]: # ({new-9b8efc17})

##### max(<value1>,<value2>,...) {#max}

The highest value of the referenced item values.

Parameter: 

-   **valueX** - the value returned by one of the history functions.

Example:

    max(avg(/host/key),avg(/host2/key2))

[comment]: # ({/new-9b8efc17})

[comment]: # ({new-5e82e822})

##### min(<value1>,<value2>,...) {#min}

The lowest value of the referenced item values.

Parameter: 

-   **valueX** - the value returned by one of the history functions.

Example:

    min(avg(/host/key),avg(/host2/key2))

[comment]: # ({/new-5e82e822})

[comment]: # ({new-120dd48b})

##### mod(value,denominator) {#mod}

The division remainder.

Parameter: 

-   **value** - the value to check;
-   **denominator** - the division denominator.

For example, division remainder of a value '5' with division denominator '2' will be '1'.

Example:

    mod(last(/host/key),2)

[comment]: # ({/new-120dd48b})

[comment]: # ({new-d8cd6290})

##### pi {#pi}

The Pi constant (3.14159265358979).

Example:

    pi()

[comment]: # ({/new-d8cd6290})

[comment]: # ({new-617a0fc5})

##### power(value,power value) {#power}

The power of a value.

Parameter: 

-   **value** - the value to check;
-   **power value** - the Nth power to use.

For example, the 3rd power of a value '2' will be '8'.

Example:

    power(last(/host/key),3)

[comment]: # ({/new-617a0fc5})

[comment]: # ({new-c94a9d57})

##### radians(value) {#radians}

Converts a value from degrees to radians.

Parameter: 

-   **value** - the value to check

For example, a value '1' converted to radians will be '0.0174532925'.

Example:

    radians(last(/host/key))

[comment]: # ({/new-c94a9d57})

[comment]: # ({new-16f1713f})

##### rand {#rand}

Return a random integer value. A pseudo-random generated number using time as seed (enough for mathematical purposes, but not cryptography).

Example:

    rand()

[comment]: # ({/new-16f1713f})

[comment]: # ({new-e19fd7cb})

##### round(value,decimal places) {#round}

Round the value to decimal places.

Parameter: 

-   **value** - the value to check;
-   **decimal places** - specify decimal places for rounding (0 is also possible).

For example, a value '2.5482' rounded to 2 decimal places will be '2.55'.

Example:

    round(last(/host/key),2)

[comment]: # ({/new-e19fd7cb})

[comment]: # ({new-9668b2dc})

##### signum(value) {#signum}

Returns '-1' if a value is negative, '0' if a value is zero, '1' if a value is positive.

Parameter: 

-   **value** - the value to check.

For example, a value '2.5482' rounded to 2 decimal places will be '2.55'.

Example:

    signum(last(/host/key))

[comment]: # ({/new-9668b2dc})

[comment]: # ({new-1c54e7c1})

##### sin(value) {#sin}

The sine of a value, where the value is an angle expressed in radians.

Parameter: 

-   **value** - the value to check

For example, the sine of a value '1' will be '0.8414709848'.

Example:

    sin(last(/host/key))

[comment]: # ({/new-1c54e7c1})

[comment]: # ({new-f2223e46})

##### sinh(value) {#sinh}

The hyperbolical sine of a value, where the value is an angle expressed in radians.

Parameter: 

-   **value** - the value to check

For example, the hyperbolical sine of a value '1' will be '1.17520119364'.

Example:

    sinh(last(/host/key))

[comment]: # ({/new-f2223e46})

[comment]: # ({new-a732b60e})

##### sqrt(value) {#sqrt}

The square root of a value.<br>
This function will fail with a negative value.

Parameter: 

-   **value** - the value to check

For example, the square root of a value '3.5' will be '1.87082869339'.

Example:

    sqrt(last(/host/key))

[comment]: # ({/new-a732b60e})

[comment]: # ({new-0bbc007d})

##### sum(<value1>,<value2>,...) {#sum}

The sum of the referenced item values.

Parameter: 

-   **valueX** - the value returned by one of the history functions.

Example:

    sum(avg(/host/key),avg(/host2/key2))

[comment]: # ({/new-0bbc007d})

[comment]: # ({new-49e10a41})

##### tan(value) {#tan}

The tangent of a value.

Parameter: 

-   **value** - the value to check

For example, the tangent of a value '1' will be '1.55740772465'.

Example:

    tan(last(/host/key))

[comment]: # ({/new-49e10a41})

[comment]: # ({new-53de5a0f})

##### truncate(value,decimal places) {#truncate}

Truncate the value to decimal places.

Parameter: 

-   **value** - the value to check;
-   **decimal places** - specify decimal places for truncating (0 is also possible).

For example, a value '2.5482' truncated to 2 decimal places will be '2.54'.

Example:

    truncate(last(/host/key),2)

[comment]: # ({/new-53de5a0f})

[comment]: # ({new-92620b8c})

See [all supported functions](/manual/appendix/functions).  

[comment]: # ({/new-92620b8c})
