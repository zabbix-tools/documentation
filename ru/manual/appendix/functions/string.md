[comment]: # attributes: notoc

[comment]: # translation:outdated

[comment]: # ({new-b12f316b})
FIXME **This page is not fully translated, yet. Please help completing
the translation.**\
*(remove this paragraph once the translation is finished)*

# 8 String functions

All functions listed here are supported in:

-   [Trigger expressions](/manual/config/triggers/expression)
-   [Calculated items](/manual/config/items/itemtypes/calculated)

Some general notes on function parameters:

-   Function parameters are separated by a comma
-   Expressions are accepted as parameters
-   String parameters must be double-quoted; otherwise they might get
    misinterpreted
-   Optional function parameters (or parameter parts) are indicated by
    `<` `>`

|FUNCTION|<|<|<|
|--------|-|-|-|
|<|**Description**|**Function-specific parameters**|**Comments**|
|**ascii**|<|<|<|
|<|The ASCII code of the leftmost character of the value.|<|Supported value types: string, text, log<br><br>For example, a value like 'Abc' will return '65' (ASCII code for 'A').<br><br>Example:<br>=> **ascii**(last(/host/key))|
|**bitlength**|<|<|<|
|<|The length of value in bits.|<|Supported value types: string, text, log, integer<br><br>Example:<br>=> **bitlength**(last(/host/key))|
|**bytelength**|<|<|<|
|<|The length of value in bytes.|<|Supported value types: string, text, log, integer<br><br>Example:<br>=> **bytelength**(last(/host/key))|
|**char**|<|<|<|
|<|Return the character by interpreting the value as ASCII code.|<|Supported value types: integer<br><br>The value must be in the 0-255 range. For example, a value like '65' (interpreted as ASCII code) will return 'A'.<br><br>Example:<br>=> **char**(last(/host/key))|
|**concat** (string)|<|<|<|
|<|The string resulting from concatenating the value to the specified string.|**string** - the string to concatenate to|Supported value types: string, text, log<br><br>For example, a value like 'Zab' concatenated to 'bix' (the specified string) will return 'Zabbix'.<br><br>Example:<br>=> **concat**(last(/host/key),**"bix"**)|
|**insert** (start,length,replacement)|<|<|<|
|<|Insert specified characters or spaces into the character string beginning at the specified position in the string.|**start** - start position<br>**length** - positions to replace<br>**replacement** - replacement string|Supported value types: string, text, log<br><br>For example, a value like 'Zabbbix' will be replaced by 'Zabbix' if 'bb' (starting position 3, positions to replace 2) is replaced by 'b'.<br><br>Example:<br>=> **insert**(last(/host/key),**3**,**2**,**"b"**)|
|**left** (count)|<|<|<|
|<|The leftmost characters of the value.|**count** - number of characters to return|Supported value types: string, text, log<br><br>For example, you may return 'Zab' from 'Zabbix' by specifying 3 leftmost characters to return.<br><br>Example:<br>=> **left**(last(/host/key),**3**) - return three leftmost characters<br><br>See also right().|
|**length**|<|<|<|
|<|The length of value in characters.|<|Supported value types: str, text, log<br><br>Example:<br>=> **length**(last(/host/key)) → length of the latest value<br>=> **length**(last(/host/key,\#3)) → length of the third most recent value<br>=> **length**(last(/host/key,\#1:now-1d)) → length of the most recent value one day ago|
|**ltrim** (<chars>)|<|<|<|
|<|Remove specified characters from the beginning of string.|**chars** - (optional) specify characters to remove<br><br>Whitespace is left-trimmed by default (if no optional characters are specified).|Supported value types: string, text, log<br><br>Example:<br>=> **ltrim**(last(/host/key)) - remove whitespace from the beginning of string<br>=> **ltrim**(last(/host/key),**"Z"**) - remove any 'Z' from the beginning of string<br>=> **ltrim**(last(/host/key),**" Z"**) - remove any space and 'Z' from the beginning of string<br><br>See also: rtrim(), trim()|
|**mid** (start,length)|<|<|<|
|<|Return a substring of N characters beginning at the character position specified by 'start'.|**start** - start position of substring<br>**length** - positions to return in substring|Supported value types: string, text, log<br><br>For example, it is possible return 'abbi' from a value like 'Zabbix' if starting position is 2, and positions to return is 4).<br><br>Example:<br>=> **mid**(last(/host/key),**2**,**4**)="abbi"|
|**repeat** (count)|<|<|<|
|<|Repeat a string.|**count** - number of times to repeat|Supported value types: string, text, log<br><br>Example:<br>=> **repeat**(last(/host/key),**2**) - repeat the value two times|
|**replace** (pattern,replacement)|<|<|<|
|<|Find pattern in the value and replace with replacement. All occurrences of the pattern will be replaced.|**pattern** - pattern to find<br>**replacement** - string to replace the pattern with|Supported value types: string, text, log<br><br>Example:<br>=> **replace**(last(/host/key),**"ibb"**,**"abb"**) - replace all 'ibb' with 'abb'|
|**right** (count)|<|<|<|
|<|The rightmost characters of the value.|**count** - number of characters to return|Supported value types: string, text, log<br><br>For example, you may return 'bix' from 'Zabbix' by specifying 3 rightmost characters to return.<br><br>Example:<br>=> **right**(last(/host/key),**3**) - return three rightmost characters<br><br>See also left().|
|**rtrim** (<chars>)|<|<|<|
|<|Remove specified characters from the end of string.|**chars** - (optional) specify characters to remove<br><br>Whitespace is right-trimmed by default (if no optional characters are specified).|Supported value types: string, text, log<br><br>Example:<br>=> **rtrim**(last(/host/key)) - remove whitespace from the end of string<br>=> **rtrim**(last(/host/key),**"x"**) - remove any 'x' from the end of string<br>=> **rtrim**(last(/host/key),**"x "**) - remove any 'x' or space from the end of string<br><br>See also: ltrim(), trim()|
|**trim** (<chars>)|<|<|<|
|<|Remove specified characters from the beginning and end of string.|**chars** - (optional) specify characters to remove<br><br>Whitespace is trimmed from both sides by default (if no optional characters are specified).|Supported value types: string, text, log<br><br>Example:<br>=> **trim**(last(/host/key)) - remove whitespace from the beginning and end of string<br>=> **trim**(last(/host/key),**"\_"**) - remove '\_' from the beginning and end of string<br><br>See also: ltrim(), rtrim()|

[comment]: # ({/new-b12f316b})

[comment]: # ({new-e91f4bca})
### Function details

Some general notes on function parameters:

-   Function parameters are separated by a comma
-   Expressions are accepted as parameters
-   String parameters must be double-quoted; otherwise they might get
    misinterpreted
-   Optional function parameters (or parameter parts) are indicated by
    `<` `>`

[comment]: # ({/new-e91f4bca})

[comment]: # ({new-6bdf63a4})

##### ascii(value) {#ascii}

The ASCII code of the leftmost character of the value.<br>
Supported value types: *String*, *Text*, *Log*.

Parameter: 

-   **value** - the value to check

For example, a value like 'Abc' will return '65' (ASCII code for 'A').

Example:

    ascii(last(/host/key))

[comment]: # ({/new-6bdf63a4})

[comment]: # ({new-30f96a52})

##### bitlength(value) {#bitlength}

The length of value in bits.<br>
Supported value types: *String*, *Text*, *Log*, *Integer*.

Parameter: 

-   **value** - the value to check

Example:

    bitlength(last(/host/key))

[comment]: # ({/new-30f96a52})

[comment]: # ({new-d58806ed})

##### bytelength(value) {#bytelength}

The length of value in bytes.<br>
Supported value types: *String*, *Text*, *Log*, *Integer*.

Parameter: 

-   **value** - the value to check

Example:

    bytelength(last(/host/key))

[comment]: # ({/new-d58806ed})

[comment]: # ({new-e35a326a})

##### char(value) {#char}

Return the character by interpreting the value as ASCII code.<br>
Supported value types: *Integer*.

Parameter: 

-   **value** - the value to check

The value must be in the 0-255 range. For example, a value like '65' (interpreted as ASCII code) will return 'A'.

Example:

    char(last(/host/key))

[comment]: # ({/new-e35a326a})

[comment]: # ({new-5e2f76e5})

##### concat(<value1>,<value2>,...) {#concat}

The string resulting from concatenating the referenced item values or constant values.<br>
Supported value types: *String*, *Text*, *Log*, *Float*, *Integer*.

Parameter: 

-   **valueX** - the value returned by one of the history functions or a constant value (string, integer, or float number). Must contain at least two parameters.

For example, a value like 'Zab' concatenated to 'bix' (the constant string) will return 'Zabbix'.

Examples:

    concat(last(/host/key),"bix")
    concat("1 min: ",last(/host/system.cpu.load[all,avg1]),", 15 min: ",last(/host/system.cpu.load[all,avg15]))

[comment]: # ({/new-5e2f76e5})

[comment]: # ({new-06698de3})

##### insert(value,start,length,replacement) {#insert}

Insert specified characters or spaces into the character string beginning at the specified position in the string.<br>
Supported value types: *String*, *Text*, *Log*.

Parameter: 

-   **value** - the value to check;<br>
-   **start** - start position;<br>
-   **length** - positions to replace;<br>
-   **replacement** - replacement string.

For example, a value like 'Zabbbix' will be replaced by 'Zabbix' if 'bb' (starting position 3, positions to replace 2) is replaced by 'b'.

Example:

    insert(last(/host/key),3,2,"b")

[comment]: # ({/new-06698de3})

[comment]: # ({new-16f928ed})

##### left(value,count) {#left}

Return the leftmost characters of the value.<br>
Supported value types: *String*, *Text*, *Log*.

Parameter: 

-   **value** - the value to check;<br>
-   **count** - the number of characters to return.

For example, you may return 'Zab' from 'Zabbix' by specifying 3 leftmost characters to return. See also [right()](#right).

Example:

    left(last(/host/key),3) #return three leftmost characters

[comment]: # ({/new-16f928ed})

[comment]: # ({new-b8f86065})

##### length(value) {#length}

The length of value in characters.<br>
Supported value types: *String*, *Text*, *Log*.

Parameter: 

-   **value** - the value to check.

Examples:

    length(last(/host/key)) #the length of the latest value
    length(last(/host/key,#3)) #the length of the third most recent value
    length(last(/host/key,#1:now-1d)) #the length of the most recent value one day ago

[comment]: # ({/new-b8f86065})

[comment]: # ({new-01a5023c})

##### ltrim(value,<chars>) {#ltrim}

Remove specified characters from the beginning of string.<br>
Supported value types: *String*, *Text*, *Log*.

Parameter: 

-   **value** - the value to check;<br>
-   **chars** (optional) - specify the characters to remove.

Whitespace is left-trimmed by default (if no optional characters are specified). See also: [rtrim()](#rtrim), [trim()](#trim).

Examples:

    ltrim(last(/host/key)) #remove whitespace from the beginning of string
    ltrim(last(/host/key),"Z") #remove any 'Z' from the beginning of string
    ltrim(last(/host/key)," Z") #remove any space and 'Z' from the beginning of string

[comment]: # ({/new-01a5023c})

[comment]: # ({new-de3605ed})

##### mid(value,start,length) {#mid}

Return a substring of N characters beginning at the character position specified by 'start'.<br>
Supported value types: *String*, *Text*, *Log*.

Parameter: 

-   **value** - the value to check;<br>
-   **start** - start position of the substring;<br>
-   **length** - positions to return in substring.

For example, it is possible return 'abbi' from a value like 'Zabbix' if starting position is 2, and positions to return is 4.

Example:

    mid(last(/host/key),2,4)="abbi"

[comment]: # ({/new-de3605ed})

[comment]: # ({new-9e3e42f7})

##### repeat(value,count) {#repeat}

Repeat a string.<br>
Supported value types: *String*, *Text*, *Log*.

Parameter: 

-   **value** - the value to check;<br>
-   **count** - the number of times to repeat.

Example:

    repeat(last(/host/key),2) #repeat the value two times

[comment]: # ({/new-9e3e42f7})

[comment]: # ({new-6aa607aa})

##### replace(value,pattern,replacement) {#replace}

Find the pattern in the value and replace with replacement. All occurrences of the pattern will be replaced.<br>
Supported value types: *String*, *Text*, *Log*.

Parameter: 

-   **value** - the value to check;<br>
-   **pattern** - the pattern to find;<br>
-   **replacement** - the string to replace the pattern with.

Example:

    replace(last(/host/key),"ibb","abb") - replace all 'ibb' with 'abb'

[comment]: # ({/new-6aa607aa})

[comment]: # ({new-ccefcd9e})

##### right(value,count) {#right}

Return the rightmost characters of the value.<br>
Supported value types: *String*, *Text*, *Log*.

Parameter: 

-   **value** - the value to check;<br>
-   **count** - the number of characters to return.

For example, you may return 'bix' from 'Zabbix' by specifying 3 rightmost characters to return. See also [left()](#left).

Example:

    right(last(/host/key),3) #return three rightmost characters

[comment]: # ({/new-ccefcd9e})

[comment]: # ({new-6e9972ef})

##### rtrim(value,<chars>) {#rtrim}

Remove specified characters from the end of string.<br>
Supported value types: *String*, *Text*, *Log*.

Parameter: 

-   **value** - the value to check;<br>
-   **chars** (optional) - specify the characters to remove.

Whitespace is right-trimmed by default (if no optional characters are specified). See also: [ltrim()](#ltrim), [trim()](#trim).

Examples:

    rtrim(last(/host/key)) #remove whitespace from the end of string
    rtrim(last(/host/key),"x") #remove any 'x' from the end of string
    rtrim(last(/host/key),"x ") #remove any 'x' and space from the end of string

[comment]: # ({/new-6e9972ef})

[comment]: # ({new-97fb4186})

##### trim(value,<chars>) {#trim}

Remove specified characters from the beginning and end of string.<br>
Supported value types: *String*, *Text*, *Log*.

Parameter: 

-   **value** - the value to check;<br>
-   **chars** (optional) - specify the characters to remove.

Whitespace is trimmed from both sides by default (if no optional characters are specified). See also: [ltrim()](#ltrim), [rtrim()](#rtrim).

Examples:

    trim(last(/host/key)) - remove whitespace from the beginning and end of string
    trim(last(/host/key),"_") - remove '_' from the beginning and end of string

[comment]: # ({/new-97fb4186})

[comment]: # ({new-b8eeecec})

See [all supported functions](/manual/appendix/functions).

[comment]: # ({/new-b8eeecec})
