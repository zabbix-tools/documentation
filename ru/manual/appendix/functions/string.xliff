<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="ru" datatype="plaintext" original="manual/appendix/functions/string.md">
    <body>
      <trans-unit id="b12f316b" xml:space="preserve">
        <source>
# 9 String functions

All functions listed here are supported in:

-   [Trigger expressions](/manual/config/triggers/expression)
-   [Calculated items](/manual/config/items/itemtypes/calculated)

The functions are listed without additional information. Click on the function to see the full details.

|Function|Description|
|--|--------|
|[ascii](#ascii)|The ASCII code of the leftmost character of the value.|
|[bitlength](#bitlength)|The length of value in bits.|
|[bytelength](#bytelength)|The length of value in bytes.|
|[char](#char)|Return the character by interpreting the value as ASCII code.|
|[concat](#concat)|The string resulting from concatenating the referenced item values or constant values.|
|[insert](#insert)|Insert specified characters or spaces into the character string beginning at the specified position in the string.|
|[left](#left)|Return the leftmost characters of the value.|
|[length](#length)|The length of value in characters.|
|[ltrim](#ltrim)|Remove specified characters from the beginning of string.|
|[mid](#mid)|Return a substring of N characters beginning at the character position specified by 'start'.|
|[repeat](#repeat)|Repeat a string.|
|[replace](#replace)|Find the pattern in the value and replace with replacement.|
|[right](#right)|Return the rightmost characters of the value.|
|[rtrim](#rtrim)|Remove specified characters from the end of string.|
|[trim](#trim)|Remove specified characters from the beginning and end of string.|</source>
      </trans-unit>
      <trans-unit id="e91f4bca" xml:space="preserve">
        <source>### Function details

Some general notes on function parameters:

-   Function parameters are separated by a comma
-   Expressions are accepted as parameters
-   String parameters must be double-quoted; otherwise they might get
    misinterpreted
-   Optional function parameters (or parameter parts) are indicated by
    `&lt;` `&gt;`</source>
      </trans-unit>
      <trans-unit id="6bdf63a4" xml:space="preserve">
        <source>
##### ascii(value) {#ascii}

The ASCII code of the leftmost character of the value.&lt;br&gt;
Supported value types: *String*, *Text*, *Log*.

Parameter: 

-   **value** - the value to check

For example, a value like 'Abc' will return '65' (ASCII code for 'A').

Example:

    ascii(last(/host/key))</source>
      </trans-unit>
      <trans-unit id="30f96a52" xml:space="preserve">
        <source>
##### bitlength(value) {#bitlength}

The length of value in bits.&lt;br&gt;
Supported value types: *String*, *Text*, *Log*, *Integer*.

Parameter: 

-   **value** - the value to check

Example:

    bitlength(last(/host/key))</source>
      </trans-unit>
      <trans-unit id="d58806ed" xml:space="preserve">
        <source>
##### bytelength(value) {#bytelength}

The length of value in bytes.&lt;br&gt;
Supported value types: *String*, *Text*, *Log*, *Integer*.

Parameter: 

-   **value** - the value to check

Example:

    bytelength(last(/host/key))</source>
      </trans-unit>
      <trans-unit id="e35a326a" xml:space="preserve">
        <source>
##### char(value) {#char}

Return the character by interpreting the value as ASCII code.&lt;br&gt;
Supported value types: *Integer*.

Parameter: 

-   **value** - the value to check

The value must be in the 0-255 range. For example, a value like '65' (interpreted as ASCII code) will return 'A'.

Example:

    char(last(/host/key))</source>
      </trans-unit>
      <trans-unit id="5e2f76e5" xml:space="preserve">
        <source>
##### concat(&lt;value1&gt;,&lt;value2&gt;,...) {#concat}

The string resulting from concatenating the referenced item values or constant values.&lt;br&gt;
Supported value types: *String*, *Text*, *Log*, *Float*, *Integer*.

Parameter: 

-   **valueX** - the value returned by one of the history functions or a constant value (string, integer, or float number). Must contain at least two parameters.

For example, a value like 'Zab' concatenated to 'bix' (the constant string) will return 'Zabbix'.

Examples:

    concat(last(/host/key),"bix")
    concat("1 min: ",last(/host/system.cpu.load[all,avg1]),", 15 min: ",last(/host/system.cpu.load[all,avg15]))</source>
      </trans-unit>
      <trans-unit id="06698de3" xml:space="preserve">
        <source>
##### insert(value,start,length,replacement) {#insert}

Insert specified characters or spaces into the character string beginning at the specified position in the string.&lt;br&gt;
Supported value types: *String*, *Text*, *Log*.

Parameter: 

-   **value** - the value to check;&lt;br&gt;
-   **start** - start position;&lt;br&gt;
-   **length** - positions to replace;&lt;br&gt;
-   **replacement** - replacement string.

For example, a value like 'Zabbbix' will be replaced by 'Zabbix' if 'bb' (starting position 3, positions to replace 2) is replaced by 'b'.

Example:

    insert(last(/host/key),3,2,"b")</source>
      </trans-unit>
      <trans-unit id="16f928ed" xml:space="preserve">
        <source>
##### left(value,count) {#left}

Return the leftmost characters of the value.&lt;br&gt;
Supported value types: *String*, *Text*, *Log*.

Parameter: 

-   **value** - the value to check;&lt;br&gt;
-   **count** - the number of characters to return.

For example, you may return 'Zab' from 'Zabbix' by specifying 3 leftmost characters to return. See also [right()](#right).

Example:

    left(last(/host/key),3) #return three leftmost characters</source>
      </trans-unit>
      <trans-unit id="b8f86065" xml:space="preserve">
        <source>
##### length(value) {#length}

The length of value in characters.&lt;br&gt;
Supported value types: *String*, *Text*, *Log*.

Parameter: 

-   **value** - the value to check.

Examples:

    length(last(/host/key)) #the length of the latest value
    length(last(/host/key,#3)) #the length of the third most recent value
    length(last(/host/key,#1:now-1d)) #the length of the most recent value one day ago</source>
      </trans-unit>
      <trans-unit id="01a5023c" xml:space="preserve">
        <source>
##### ltrim(value,&lt;chars&gt;) {#ltrim}

Remove specified characters from the beginning of string.&lt;br&gt;
Supported value types: *String*, *Text*, *Log*.

Parameter: 

-   **value** - the value to check;&lt;br&gt;
-   **chars** (optional) - specify the characters to remove.

Whitespace is left-trimmed by default (if no optional characters are specified). See also: [rtrim()](#rtrim), [trim()](#trim).

Examples:

    ltrim(last(/host/key)) #remove whitespace from the beginning of string
    ltrim(last(/host/key),"Z") #remove any 'Z' from the beginning of string
    ltrim(last(/host/key)," Z") #remove any space and 'Z' from the beginning of string</source>
      </trans-unit>
      <trans-unit id="de3605ed" xml:space="preserve">
        <source>
##### mid(value,start,length) {#mid}

Return a substring of N characters beginning at the character position specified by 'start'.&lt;br&gt;
Supported value types: *String*, *Text*, *Log*.

Parameter: 

-   **value** - the value to check;&lt;br&gt;
-   **start** - start position of the substring;&lt;br&gt;
-   **length** - positions to return in substring.

For example, it is possible return 'abbi' from a value like 'Zabbix' if starting position is 2, and positions to return is 4.

Example:

    mid(last(/host/key),2,4)="abbi"</source>
      </trans-unit>
      <trans-unit id="9e3e42f7" xml:space="preserve">
        <source>
##### repeat(value,count) {#repeat}

Repeat a string.&lt;br&gt;
Supported value types: *String*, *Text*, *Log*.

Parameter: 

-   **value** - the value to check;&lt;br&gt;
-   **count** - the number of times to repeat.

Example:

    repeat(last(/host/key),2) #repeat the value two times</source>
      </trans-unit>
      <trans-unit id="6aa607aa" xml:space="preserve">
        <source>
##### replace(value,pattern,replacement) {#replace}

Find the pattern in the value and replace with replacement. All occurrences of the pattern will be replaced.&lt;br&gt;
Supported value types: *String*, *Text*, *Log*.

Parameter: 

-   **value** - the value to check;&lt;br&gt;
-   **pattern** - the pattern to find;&lt;br&gt;
-   **replacement** - the string to replace the pattern with.

Example:

    replace(last(/host/key),"ibb","abb") - replace all 'ibb' with 'abb'</source>
      </trans-unit>
      <trans-unit id="ccefcd9e" xml:space="preserve">
        <source>
##### right(value,count) {#right}

Return the rightmost characters of the value.&lt;br&gt;
Supported value types: *String*, *Text*, *Log*.

Parameter: 

-   **value** - the value to check;&lt;br&gt;
-   **count** - the number of characters to return.

For example, you may return 'bix' from 'Zabbix' by specifying 3 rightmost characters to return. See also [left()](#left).

Example:

    right(last(/host/key),3) #return three rightmost characters</source>
      </trans-unit>
      <trans-unit id="6e9972ef" xml:space="preserve">
        <source>
##### rtrim(value,&lt;chars&gt;) {#rtrim}

Remove specified characters from the end of string.&lt;br&gt;
Supported value types: *String*, *Text*, *Log*.

Parameter: 

-   **value** - the value to check;&lt;br&gt;
-   **chars** (optional) - specify the characters to remove.

Whitespace is right-trimmed by default (if no optional characters are specified). See also: [ltrim()](#ltrim), [trim()](#trim).

Examples:

    rtrim(last(/host/key)) #remove whitespace from the end of string
    rtrim(last(/host/key),"x") #remove any 'x' from the end of string
    rtrim(last(/host/key),"x ") #remove any 'x' and space from the end of string</source>
      </trans-unit>
      <trans-unit id="97fb4186" xml:space="preserve">
        <source>
##### trim(value,&lt;chars&gt;) {#trim}

Remove specified characters from the beginning and end of string.&lt;br&gt;
Supported value types: *String*, *Text*, *Log*.

Parameter: 

-   **value** - the value to check;&lt;br&gt;
-   **chars** (optional) - specify the characters to remove.

Whitespace is trimmed from both sides by default (if no optional characters are specified). See also: [ltrim()](#ltrim), [rtrim()](#rtrim).

Examples:

    trim(last(/host/key)) - remove whitespace from the beginning and end of string
    trim(last(/host/key),"_") - remove '_' from the beginning and end of string</source>
      </trans-unit>
      <trans-unit id="b8eeecec" xml:space="preserve">
        <source>
See [all supported functions](/manual/appendix/functions).</source>
      </trans-unit>
    </body>
  </file>
</xliff>
