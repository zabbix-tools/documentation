[comment]: # translation:outdated

[comment]: # ({new-e0e52cee})
# 1 Генерация событий на триггеры

[comment]: # ({/new-e0e52cee})

[comment]: # ({new-5a424184})
#### Обзор

Изменение состояния триггера является наиболее частым и наиболее важным
источником событий. Каждый раз, когда триггер меняет свое состояние,
генерируется событие. Событие содержит подробную информацию о изменении
состояния триггера - когда это случилось, и какое сейчас новое
состояние.

Триггерами создаются два типа событий - Проблема и ОК.

[comment]: # ({/new-5a424184})

[comment]: # ({new-b13ee7a9})
#### События проблемы

Событие проблемы создается:

-   когда выражение триггера вычисляется как ПРАВДА, если триггер в
    состоянии ОК;
-   каждый раз выражение триггера вычисляется как ПРАВДА, если у
    триггера активировано множественная генерация событий проблема.

[comment]: # ({/new-b13ee7a9})

[comment]: # ({new-6f89a5a0})
#### События OK

Событие OK закрывает связанные событие(ия) о проблеме и может быть
создано тремя компонентами:

-   триггеры - основываясь на настройках 'Генерация ОК событий' и 'ОК
    событие закрывает';
-   корреляция событий
-   менеджер задач – когда событие [закрывается
    вручную](/ru/manual/config/events/manual_close)

[comment]: # ({/new-6f89a5a0})

[comment]: # ({new-8e1a9fc3})
##### Триггеры

У триггеров имеется настройка 'Формирование ОК событий', которая
определяет, как генерируются ОК события:

-   *Выражение* - событие OK генерируется триггеру в состоянии проблема,
    когда выражение этого триггера вычисляется как ЛОЖЬ. Это самая
    простая настройка, активированная по умолчанию.
-   *Выражение восстановления* - событие OK генерирутеся триггеру в
    состоянии проблема, когда выражение этого триггера вычисляется как
    ЛОЖЬ и выражение восстановления вычисляется как ПРАВДА. Эту опцию
    можно использовать, если критерий восстановления триггера отличается
    от критерия проблемы.
-   *Нет* - событие OK никогда не генерируется. Эту опцию можно
    использовать в сочетании с множественной генерацией проблем для
    простой отправки оповещений, когда что-то происходит.

Дополнительно, у триггеров имеется настройка 'ОК событие закрывает',
которая определяет какие события о проблемах закрываются:

-   *Все проблемы* - событие OK закрывает все открытые проблемы, которые
    созданы этим триггером
-   *Все проблемы, если значение тега совпадает* - событие OK закрывает
    все открытые проблемы, которые созданы этим триггером, и имеющие по
    крайней мере одно совпадающее значение тега. Тег задается настройкой
    триггера 'Теги для совпадения'. Если нет никаких событий о проблемах
    для закрытия, тогда событие ОК не генерируется. Эту опцию часто
    называют корреляция событий на уровне триггеров.

[comment]: # ({/new-8e1a9fc3})

[comment]: # ({new-ad7b6e93})
##### Корреляция событий

Корреляция событий (также называемая глобальной корреляцией событий)
является способом настройки пользовательских правил закрытия (в
результате чего генерируются ОК события).

Правила определяют каким образом новые события о проблемах соотносятся с
существующими событиями о проблемах и позволяют закрыть новое событие
или совпадающие события при помощи генерации ОК событий.

Однако, корреляцию событий необходимо настраивать очень осторожно, так
как она может негативно повлиять на производительность обработки событий
или, при неправильной настройке, закрыть больше событий, чем требуется
(в худшем случае могут быть закрыты вообще все события о проблемах).
Несколько советов по настройке:

1.  всегда старайтесь уменьшить масштаб корреляции, указав уникальный
    тег для контрольного события (парное событие со старыми событиями) и
    использовав условие корреляции 'тег нового события'
2.  не забывайте добавить условие на основе старого события, если
    используется операция 'закрыть старые события', или все существующие
    проблемы могут быть закрыты
3.  избегайте использования распространенных имен тегов, которые
    используются различными настройками корреляции

[comment]: # ({/new-ad7b6e93})

[comment]: # ({new-00be2f5b})
##### Менеджер задач

Если у триггера активирована настройка 'Разрешить закрывать вручную',
тогда появлется возможность закрывать события о проблемах, которые
созданы этим триггером, вручную. Это можно сделать в веб-интерфейсе при
[обновлении проблемы](/ru/manual/acknowledges#обновление_проблем).
Событие не закрывается напрямую – вместо 'закрытия события' создается
задача, которая будет обработана менеджером задач в самое короткое
время. Диспетчер задач создаст соответствующее ОК событие и событие о
проблеме закроется.

[comment]: # ({/new-00be2f5b})
