[comment]: # translation:outdated

[comment]: # ({new-ad0fbcd3})
# 3 Закрытие проблем вручную

[comment]: # ({/new-ad0fbcd3})

[comment]: # ({new-0eef1445})
#### Обзор

Хотя в целом события о проблемах решаются автоматически, когда состояние
триггера переходит из 'Проблема' в 'ОК', могут быть случаи когда трудно
определить при помощи выражения триггера что проблема решена. В таких
случаях проблема должна быть решена вручную.

Например, *syslog* может сообщать о том, что для оптимальной
производительности необходимо отрегулировать некоторые параметры ядра. В
это случае информация о проблеме сообщается Linux администраторам, они
исправляют её и затем вручную закрывают эту проблему.

Проблемы можно закрывать вручную только у триггеров, у которых включена
опция *Разрешить закрывать вручную*.

Когда проблема "закрыта вручную", Zabbix генерирует новую внутреннюю
задачу для Zabbix сервера. Затем процесс *task manager* выполняет эту
задачу и генерирует OK событие, следовательно закрывает событие о
проблеме.

Вручную закрытая проблема не означает, что основной триггер никогда
больше не перейдет в состояние 'Проблема'. При поступлении новых данных
от любого элемента данных, которые включены в выражение триггера,
произойдет повторное вычисление всего выражения, что может привести
снова к проблеме. Также триггер вычисляется повторно, когда в его
выражении используются функции связанные со временем. Полный список
функций связанных со временем можно найти на [странице с
Триггерами](/ru/manual/config/triggers/).

[comment]: # ({/new-0eef1445})

[comment]: # ({new-ece5ae52})
#### Настройка

Необходимо выполнить два шага для закрытия проблем вручную.

[comment]: # ({/new-ece5ae52})

[comment]: # ({new-8e977ac6})
##### Настройка триггера

В настройке триггера включите опцию *Разрешить закрывать вручную*.

![](../../../../assets/en/manual/config/manual_close_conf1.png)

[comment]: # ({/new-8e977ac6})

[comment]: # ({new-229474e2})
##### Экран обновления проблемы

Если возникает проблема у триггера с флагом *Закрыть вручную*, вы можете
перейти на экран [обновления
проблемы](/ru/manual/acknowledges#обновление_проблемы) этого триггера и
закрыть проблему вручную.

Чтобы закрыть проблему, проверьте наличие опции *Закрыть проблему* в
диалоге и нажмите на *Обновить*.

![](../../../../assets/en/manual/config/close_problem.png)

Все обязательные поля ввода отмечены красной звёздочкой.

Запрос выполняется Zabbix сервером. Обычно такая процедура занимает
несколько секунд до закрытия проблемы. В процессе этой процедуры в
*Мониторинг* → *Проблемы* будет отображаться *ЗАКРЫТИЕ* как состояние
проблемы.

[comment]: # ({/new-229474e2})

[comment]: # ({new-a2933506})
#### Проверка

Можно проверить что проблема была закрыты вручную:

-   в деталях события, доступных через *Мониторинг* → *Проблемы*;
-   используя макрос {EVENT.UPDATE.HISTORY} в сообщениях оповещения,
    который предоставит такую информацию.

[comment]: # ({/new-a2933506})
