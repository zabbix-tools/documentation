[comment]: # translation:outdated

[comment]: # ({new-e9a8ab7a})
# 2 Другие источники событий

[comment]: # ({/new-e9a8ab7a})

[comment]: # ({new-071dda8a})
#### События на обнаружения

Zabbix периодически сканирует диапазоны IP адресов заданные в правилах
сетевого обнаружения. Частота этой проверки настраивается индивидуально
для каждого правила. После того, как узел сети или сервис обнаружен,
генерируется событие (или несколько событий) на обнаружение.

Zabbix генерирует следующие события:

|Событие|Когда генерируется|
|--------------|-----------------------------------|
|Сервис доступен|Каждый раз когда Zabbix обнаруживает активный сервис.|
|Сервис недоступен|Каждый раз когда Zabbix не может обнаружить сервис.|
|Узел сети доступен|Если хотя бы один сервис доступен для IP.|
|Узел сети недоступен|Если все сервисы не доступны.|
|Сервис обнаружен|Если сервис стал доступен после его недоступности или обнаружен впервые.|
|Сервис потерян|Если сервис потерян после того как был доступен.|
|Узел сети обнаружен|Если узел сети стал доступен после его недоступности или обнаружен впервые.|
|Узел сети потерян|Если узел сети потерян после того как был доступен.|

[comment]: # ({/new-071dda8a})

[comment]: # ({new-3ae80f24})
#### События на авторегистрацию активных агентов

Авторегистрация активных агентов создает события в Zabbix.

Если настроено, событие на авторегистрацию активного агента может
создаваться, если ранее неизвестный активный агент запрашивает свои
проверки или, если изменились метаданные узла сети. Сервер добавляет
новый автоматически зарегистрированный узел сети, используя полученные
IP адрес и порт от агента.

Для получения более подробной информации, смотрите страницу об
[автоматической регистрации активных
агентов](/ru/manual/discovery/auto_registration).

[comment]: # ({/new-3ae80f24})

[comment]: # ({new-5d514e59})
#### Внутренние события

Внутренние события возникают, когда:

-   элемент данных меняет свое состояние с "нормального" на
    "неподдерживается"
-   элемент данных меняет свое состояние с "неподдерживается" на
    "нормальное"
-   правило низкоуровневого обнаружения меняет свое состояние с
    "нормального" на "неподдерживается"
-   правило низкоуровневого обнаружения меняет свое состояние с
    "неподдерживается" на "нормальный"
-   триггер меняет свое состояние с "нормального" на "неизвестное"
-   триггер меняет свое состояние с "неизвестного" на "нормальное"

Внутренние события поддерживаются начиная с Zabbix 2.2. Целью введения
внутренних событий - дать знать пользователям, когда происходят
некоторые внутренние события, например, элемент данных становиться
неподдерживаемым и перестает собирать данные.

[comment]: # ({/new-5d514e59})


[comment]: # ({new-d156b0a7})
#### Internal events

Internal events happen when:

-   an item changes state from 'normal' to 'unsupported'
-   an item changes state from 'unsupported' to 'normal'
-   a low-level discovery rule changes state from 'normal' to
    'unsupported'
-   a low-level discovery rule changes state from 'unsupported' to
    'normal'
-   a trigger changes state from 'normal' to 'unknown'
-   a trigger changes state from 'unknown' to 'normal'

Internal events are supported since Zabbix 2.2. The aim of introducing
internal events is to allow users to be notified when any internal event
takes place, for example, an item becomes unsupported and stops
gathering data.

Internal events are only created when internal actions for these events
are enabled. To stop generation of internal events (for example, for
items becoming unsupported), disable all actions for internal events in
Configuration → Actions → Internal actions.

::: noteclassic
 If internal actions are disabled, while an object is in the
'unsupported' state, recovery event for this object will still be
created.\
\
If internal actions are enabled, while an object is in the 'unsupported'
state, recovery event for this object will be created, even though
'problem event' has not been created for the object. 
:::

[comment]: # ({/new-d156b0a7})
