[comment]: # translation:outdated

[comment]: # ({new-56bf7160})
# 4 Вебхук

[comment]: # ({/new-56bf7160})

[comment]: # ({new-6584b266})
#### Обзор

Этот тип оповещений полезен для выполнения HTTP-вызовов с использованием
пользовательского кода JavaScript для прямой интеграции с внешними
системами, такими как системы поддержки, чаты или мессенджеры.

Чтобы использовать вебхук в качестве механизма уведомления, необходимо
сначала создать соответствующий тип оповещений, а затем в профиле
пользователя указать конкретные адреса для отправки уведомлений. Затем
этот тип оповещений можно будет выбирать в настройках действия.

Вы можете импортировать предварительно настроенный вебхук или создать
свой собственный.

[comment]: # ({/new-6584b266})

[comment]: # ({new-27a06b1f})
#### Интеграции

Zabbix предоставляет готовые решения для интеграции с рядом популярных
сервисов. На данный момент доступны предварительно настроенные вебхуки
для отправки уведомлений Zabbix в:

-   [Discord](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/discord)
-   [Jira](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/jira)
-   [Jira Service
    Desk](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/jira_servicedesk)
-   [Mattermost](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/mattermost)
-   [Microsoft
    Teams](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/msteams)
-   [Opsgenie](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/opsgenie)
-   [OTRS](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/otrs)
-   [Pagerduty](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/pagerduty)
-   [Pushover](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/pushover)
-   [Redmine](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/redmine)
-   [ServiceNow](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/servicenow)
-   [SIGNL4](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/signl4)
-   [Slack](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/slack)
-   [Telegram](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/telegram)
-   [Zammad](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/zammad)
-   [Zendesk](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/zendesk)

::: notetip
 В дополнение к перечисленным сервисам, Zabbix может
быть интегрирован со **Spiceworks** - для этой интеграции вебхук не
требуется. Чтобы преобразовать уведомления Zabbix в тикеты Spiceworks,
используйте тип оповещений
[E-mail](/ru/manual/config/notifications/media/email) и введите адрес
электронной почты службы поддержки Spiceworks (например,
help\@zabbix.on.spiceworks.com) в настройках специально созданного или
существующего пользователя Zabbix (для отправки уведомлений,
пользователь должен иметь доступ к необходимым узлам сети).

:::

[comment]: # ({/new-27a06b1f})

[comment]: # ({new-d84f1038})
#### Настройка

Чтобы настроить вебхук в качестве типа оповещений:

-   Перейдите в раздел *Администрация → Типы оповещений*
-   Нажмите *Создать тип оповещений*

Вкладка **тип оповещений** содержит различные атрибуты, специфичные для
выбранного типа:

![](../../../../../assets/en/manual/config/webhook_jira1.png){width="600"}

Все обязательные поля ввода отмечены красной звездочкой.

Следующие параметры относятся к типу оповещений вебхук:

|Параметр|Описание|
|----------------|----------------|
|*Параметры*|Укажите переменные webhook как пары атрибутов и значений.<br>Некоторые переменные включены по умолчанию (URL: <пусто>, HTTPProxy: <пусто>, Кому: {ALERT.SENDTO}, Тема: {ALERT.SUBJECT}, Сообщение: { ALERT.MESSAGE}), вы можете сохранить или удалить их.<br>Значения кодируются в URL автоматически. Использованные макросы раскрываются, а затем значения автоматически кодируются в URL.<br>Все макросы, которые поддерживаются в уведомлениях о проблемах, поддерживаются в параметрах.<br>Если вы указываете прокси-сервер HTTP, поле поддерживает те же функции, что и в поле конфигурации элемента [HTTP прокси](/ru/manual/config/items/itemtypes/http#configuration). Строка прокси может иметь префикс `[scheme]://` указать, какой тип прокси используется (например, https, socks4, socks5; см. [документацию](https://curl.haxx.se/libcurl/c/CURLOPT_PROXY.html)).|
|*Скрипт*|Введите код JavaScript в блоке, который появляется при нажатии в поле параметра (или рядом с кнопкой просмотра / редактирования). Этот код выполнит операцию вебух (см. [примеры](#example_scripts)).<br>Код имеет доступ ко всем параметрам, он может выполнять запросы HTTP GET, POST, PUT и DELETE и управлять HTTP заголовками и телом запроса. Код может возвращать статус OK вместе с необязательным списком тегов и значений тегов.(например, Jira ID: PROD-1234, Responsible: John Smith, Processed:<no value>, и т.д.) или строку ошибки.<br>См. также: [Дополнительные объекты Javascript](/ru/manual/config/items/preprocessing/javascript/javascript_objects).|
|*Таймаут*|Тайм-аут выполнения JavaScript (1-60 с, по умолчанию 30 с).<br>Поддерживаются суффиксы времени, например 30s, 1m.|
|*Обработка тегов*|Установите флажок для обработки возвращаемых значений свойств JSON как тегов. Эти теги добавляются к уже существующим (если есть) тегам проблемных событий в Zabbix.|
|*Включить запись меню события*|Установите флажок, чтобы включить запись в [меню события](/ru/manual/web_interface/frontend_sections/monitoring/problems#event_menu) связанную с созданным внешним тикетом.|
|*Имя записи меню*|Укажите название пункта меню.<br>Поддерживается макрос {EVENT.TAGS.<tag name>}.<br>Это поле является обязательным, только если выбрана опция *Включить запись меню события*.|
|*URL записи меню*|Укажите основной URL-адрес пункта меню.<br>Поддерживается макрос {EVENT.TAGS.<tag name>}.<br>Это поле является обязательным, только если выбрана опция *Включить запись меню события*.|

См. [общие параметры способов
оповещений](/manual/config/notifications/media#common_parameters) для
получения подробных сведений о настройке сообщений по умолчанию и
параметров обработки предупреждений.

::: notewarning
 Даже если вебхук не использует сообщения по
умолчанию, шаблоны сообщений для типов операций, используемых этим
вебхуком, все равно должны быть прописаны, иначе уведомление не будет
отправлено. 
:::

[comment]: # ({/new-d84f1038})

[comment]: # ({new-e5e73ea8})

#### Media type testing

To test a configured webhook media type:

-   Locate the relevant webhook in the [list](/manual/config/notifications/media#overview) of media types.
-   Click on *Test* in the last column of the list (a testing window
    will open).
-   Edit the webhook parameter values, if needed.
-   Click on *Test*.

By default, webhook tests are performed with parameters entered during
configuration. However, it is possible to change attribute values for
testing. Replacing or deleting values in the testing window affects the
test procedure only, the actual webhook attribute values will remain
unchanged.

![](../../../../../assets/en/manual/config/webhook_test1.png){width="600"}

To view media type test log entries without leaving the test window, click on *Open log* (a new popup window will open).

![](../../../../../assets/en/manual/config/mediatype_test2.png){width="600"}

[comment]: # ({/new-e5e73ea8})

[comment]: # ({new-b2974109})
**If the webhook test is successful:**

-   *"Media type test successful."* message is displayed
-   Server response appears in the gray *Response* field
-   Response type (JSON or String) is specified below the *Response*
    field

[comment]: # ({/new-b2974109})

[comment]: # ({new-abfbc807})
**If the webhook test fails:**

-   *"Media type test failed."* message is displayed, followed by
    additional failure details.

[comment]: # ({/new-abfbc807})

[comment]: # ({new-6eeec89a})
##### Примеры скриптов

Создать задачу в JIRA и вернуть некоторую информацию о созданной
проблеме.

``` {.java}
try {
    Zabbix.Log(127, 'jira webhook script value='+value);

    var result = {
        'tags': {
            'endpoint': 'jira'
        }
    },
    params = JSON.parse(value),
    req = new CurlHttpRequest(),
        proxy = params.HTTPProxy;
        req.SetProxy(proxy);
    fields = {},
    resp;

    req.AddHeader('Content-Type: application/json');
    req.AddHeader('Authorization: Basic '+params.authentication);

    fields.summary = params.summary;
    fields.description = params.description;
    fields.project = {"key": params.project_key};
    fields.issuetype = {"id": params.issue_id};
    resp = req.Post('https://tsupport.zabbix.lan/rest/api/2/issue/',
        JSON.stringify({"fields": fields})
    );

    if (req.Status() != 201) {
        throw 'Response code: '+req.Status();
    }

    resp = JSON.parse(resp);
    result.tags.issue_id = resp.id;
    result.tags.issue_key = resp.key;
} catch (error) {
    Zabbix.Log(127, 'jira issue creation failed json : '+JSON.stringify({"fields": fields}));
    Zabbix.Log(127, 'jira issue creation failed : '+error);

    result = {};
}

return JSON.stringify(result);
```

Отправить уведомление в заданный канал Slack.

![](/manual/config/webhook_slack1.png)

``` {.java}
var req = new CurlHttpRequest();
params = JSON.parse(value);
proxy = params.HTTPProxy;
req.SetProxy(proxy);
req.AddHeader('Content-Type: application/x-www-form-urlencoded');

Zabbix.Log(127, 'webhook request value='+value);

req.Post('https://hooks.slack.com/services/KLFDEI9KNL/ZNA76HGCF/h9MLKJMWoUcEAz8n',
  'payload='+value
);

Zabbix.Log(127, 'response code: '+req.Status());

return JSON.stringify({
  'tags': {
    'delivered': 'slack'
  }
});
```

[comment]: # ({/new-6eeec89a})

[comment]: # ({new-ac89791d})
#### Оповещения пользователей

После настройки способа оповещений Вебхук, перейдите в раздел
*Администрирование → Пользователи*. В настройках профиля пользователя на
вкладке **Оповещения** добавьте оповещения нужного типа и адрес, куда
отправлять уведомления. Шаги по настройке пользовательских каналов
оповещений, общие для всех способов, описаны на странице [способы
оповещений](/ru/manual/config/notifications/media#user_media).

Обратите внимание, что поле *Отправлять на* должно быть заполнено. Если
вебхук не использует это поле, введите любую комбинацию допустимых
символов.

[comment]: # ({/new-ac89791d})
