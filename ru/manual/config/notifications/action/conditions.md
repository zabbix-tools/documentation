[comment]: # translation:outdated

[comment]: # ({new-aa1f9956})
# 1 Условия

[comment]: # ({/new-aa1f9956})

[comment]: # ({new-6ef58449})
#### Обзор

Действие будет выполнено только в случае, если событие удовлетворяет
заданному набору условий. Условия задаются при настройке
[действия](/ru/manual/config/notifications/action#настройка_действия).

Следующие условия можно задать для действий основанных на триггерах:

|Тип условия|Поддерживаемые операторы|Описание|
|---------------------|-----------------------------------------------|----------------|
|*Группа элементов данных*|=<br>содержит<br>не содержит|Укажите группу элементов данных или исключение группы элементов данных.<br>**=** - событие относится к триггеру, элемент данных которого принадлежит указанной группе элементов данных.<br>**содержит** - событие относится к триггеру, элемент данных которого принадлежит группе элементов данных, содержащей указанную строку.<br>**не содержит** - событие относится к триггеру, элемент данных которого принадлежит группе элементов данных, не содержащей указанную строку.|
|*Группа узлов сети*|=<br><>|Укажите группу узлов сети или исключение группы узлов сети.<br>**=** - событие относится к указанной группе узлов сети.<br>**<>** - событие не относится к указанной группе узлов сети.<br>Указав родительскую группу узлов сети косвенным образом будут выбраны все вложенные группы узлов сети. Чтобы выбрать только родительскую группу, все вложенные группы необходимо дополнительно указать с оператором **<>**.|
|*Шаблон*|=<br><>|Укажите шаблон или исключение шаблона.<br>**=** - событие относится к триггеру унаследованному из указанного шаблона.<br>**<>** - событие не относится к триггеру унаследованному из указанного шаблона.|
|*Узел сети*|=<br><>|Укажите узел сети или исключение узла сети.<br>**=** - событие относится к указанному узлу сети.<br>**<>** - событие не относится к указанному узлу сети.|
|*Тег*|=<br><><br>содержит<br>не содержит|Уажите тег события или исключение тега события.<br>**=** - событие имеет указанный тег<br>**<>** - событие не имеет указанный тег<br>**содержит** - событие имеет тег, который содержит указанную строку<br>**не содержит** - событие имеет тег, который не содержит указанную строку|
|*Значение тега*|=<br><><br>содержит<br>не содержит|Укажите комбинацию тега события и его значение или исключение комбинации тега и значения.<br>**=** - событие имеет указанные тег и значение<br>**<>** - событие не имеет указанные тег и значение<br>**содержит** - событие содержит указанные строки тега и значения<br>**не содержит** - событие не содержит указанные строки тега и значения|
|*Триггер*|=<br><>|Укажите триггер или исключение триггера.<br>**=** - событие сгенерировано указанным триггером.<br>**<>** - событие сгенерировано любым триггером отличным от указанного.|
|*Имя триггера*|содержит<br>не содержит|Укажите строку из имени триггера или исключение строки.<br>**содержит** - событие произошло от триггера, который содержит в имени указанную строку. Регистрозависимое.<br>**не содержит** - указанная строка не содержится в имени триггера. Регистрозависимое.<br>*Обратите внимание*: Заданное значение будет сравниваться с именем триггера, со всеми раскрытыми макросами.|
|*Важность триггера*|=<br><><br>>=<br><=|Укажите важность триггера.<br>**=** - совпадает с указанной важностью триггера<br>**<>** - не совпадает с указанной важностью триггера<br>**>=** - выше или совпадает указанной важности триггера<br>**<=** - ниже или совпадает указанной важности триггера|
|*Период времени*|в<br>не в|Укажите период времени или исключение периода времени.<br>**в** - время события в течении указанного периода времени.<br>**не в** - время события не входит в указанный период времени.<br>Смотрите страницу [Спецификации периодов времени](/ru/manual/appendix/time_period) для получения более подробных сведений об этом формате.<br>Поддерживаются [пользовательские макросы](/ru/manual/config/macros/usermacros) начиная с Zabbix 3.4.0.|
|*Состояние обслуживания*|в<br>не в|Укажите должен быть узел сети находиться в обслуживании или нет.<br>**в** - узел сети в режиме обслуживания.<br>**не в** - узел сети не в режиме обслуживания.<br>*Обратите внимание*: Если в выражении триггера указано несколько узлов сети, условие выполняется, если по крайней мере один из узлов сети не/в режиме обслуживания.|
|*Проблема подавлена*|нет<br>да|Укажите, если проблема подавлена (не отображается) по причине обслуживания узла сети.<br>**нет** - проблема не подавлена.<br>**да** - проблема подавлена.<br>|

Следующие условия можно задать для действий основанных на обнаружении:

|Тип условия|Поддерживаемые операторы|Описание|
|---------------------|-----------------------------------------------|----------------|
|*IP узла сети*|=<br><>|Укажите диапазон IP адресов или исключение диапазона у обнаруженного узла сети.<br>**=** - IP адрес узла сети в указанном диапазоне.<br>**<>** - IP адрес узла сети не из указанного диапазона.<br>Доступны следующие форматы: :<br>Одиночный IP: 192.168.1.33<br>Диапазон IP адресов:<br>192.168.1.1-254 (до Zabbix 2.4.4)<br>192.168.1-10.1-254 (начиная с Zabbix 2.4.4)<br>маска IP: 192.168.4.0/24<br>Список диапазонов: 192.168.1.1-254, 192.168.2.1-100, 192.168.2.200, 192.168.4.0/24<br>Поддержка пробелов в формате списка имеется начиная с Zabbix 3.0.0.|
|*Тип сервиса*|=<br><>|Укажите тип сервиса обнаруженного сервиса или исключение типа сервиса.<br>**=** - совпадает с обнаруженным сервисом.<br>**<>** - не совпадает с обнаруженным сервисом.<br>Доступные типы сервисов: SSH, LDAP, SMTP, FTP, HTTP, HTTPS (доступно с Zabbix 2.2), POP, NNTP, IMAP, TCP, Zabbix агент, SNMPv1 агент, SNMPv2 агент, SNMPv3 агент, ICMP пинг, telnet (доступно начиная с версии Zabbix 2.2)|
|*Порт сервиса*|=<br><>|Укажите диапазон TCP портов или исключение диапазона у обнаруженного узла сети.\\<br>**=** - порт сервиса из указанного диапазона.<br>**<>** - порт сервиса не из указанного диапазона.|
|*Правило обнаружения*|=<br><>|Укажите правило обнаружения или исключение правила обнаружения.<br>**=** - использование указанного правила обнаружения.<br>**<>** - использование любого другого правила обнаружения, отличного от указанного.|
|*Проверка обнаружения*|=<br><>|Укажите проверку обнаружения или исключение проверки обнаружения.<br>**=** - использование указанной проверки обнаружения.<br>**<>** - использование любой другой проверки обнаружения, отличной от указанной.|
|*Обнаруженный объект*|=|Укажите обнаруженный объект.<br>**=** - совпадает с обнаруженным объектом (устройством или сервисом).|
|*Состояние обнаружения*|=|**Доступен** - совпадает с событиями 'Узел сети доступен' и 'Сервис доступен'<br>**Недоступен** - совпадает с событиями 'Узел сети недоступен' и 'Сервис недоступен'<br>**Обнаружен** - совпадает с событиями 'Узел сети обнаружен' и 'Сервис обнаружен'<br>**Потерян** - совпадает с событиями 'Узел сети потерян' и 'Сервис потерян'|
|*Доступен/Недоступен*|>=<br><=|Время доступности для событий 'Узел сети доступен' и 'Сервис доступен'. Время недоступности для событий 'Узел сети недоступен' и 'Сервис недоступен'.<br>**>=** - больше или совпадает. Параметр задается в секундах.<br>**<=** - меньше или совпадает. Параметр задается в секундах.|
|*Полученное значение*|=<br><><br>>=<br><=<br>содержит<br>не содержит|Укажите полученное значение от проверки агента (Zabbix, SNMP) в правиле обнаружения. Регистрозависимое сравнение строк. Если в правиле заданы несколько проверок Zabbix агента или SNMP, проверяется каждое полученное от них значения (каждая проверка генерирует новое событие, которое сопоставляется со всеми условиями).<br>**=** - совпадает с указанным значением.<br>**<>** - не совпадает с указанным значением.<br>**>=** - больше или совпадает с указанным значением.<br>**<=** - меньше или совпадает с указанным значением.<br>**содержит** - содержит указанную подстроку. Параметр задается в виде строки.<br>**не содержит** - не содержит указанную подстроку. Параметр задается в виде строки.|
|*Прокси*|=<br><>|Укажите прокси или исключение прокси.<br>**=** - используется указанный прокси.<br>**<>** - используется любой другой прокси отличный от указанного.|

::: noteclassic
Проверки сервисов в правиле обнаружения, которые приводят к
событиям обнаружения, не выполняются одновременно. Поэтому, если
настроено **несколько** значений для `Тип сервиса`, `Порт сервиса` или
`Полученное значение` условий в действии, они будут сравниваться с одним
событием обнаружения за раз, но **не** сравнивается с несколькими
событиями одновременно. В результате действия с несколькими значениями
по одним и тем же типам проверок могут быть выполнены
некорректно.
:::

Следующие условия можно задать для действий основанных на
авто-регистрации активного агента:

|Тип условия|Поддерживаемые операции|Описание|
|---------------------|---------------------------------------------|----------------|
|Метаданные узла сети|содержит<br>не содержит|Укажите метаданные узла сети или метаданные узла сети для исключения.<br>**содержит** - метаданные узла сети содержат указанную строку<br>**не содержит** - метаданные узла сети не содержат указанную строку. Метаданные узла сети можно задать в [файле конфигурации агента](/ru/manual/appendix/config/zabbix_agentd)|
|Имя узла сети|содержит<br>не содержит|Укажите имя узла сети или имя узла сети для исключения.<br>**содержит** - имя узла сети содержит указанную строку<br>**не содержит** - имя узла сети не содержит указанную строку.|
|Прокси|=<br><>|Укажите прокси или прокси для исклюения:<br>**=** - используется указанный прокси.<br>**<>** - используется любой другой прокси отличный от указанного.|

Следующие условия можно задать для действий основанных на внутренних
событиях:

|Тип условия|Поддерживаемые операции|Описание|
|---------------------|---------------------------------------------|----------------|
|Группа элемента данных|=<br>содержит<br>не содержит|Укажите группу элементов данных или группу элементов данных для исключения.<br>**=** - событие относиться к элементу данных, который принадлежит указанной группе элементов данных.<br>**содержит** - событие относиться к элементу данных, принадлежит указанной группе элементов данных содержащей указанную строку<br>**не содержит** - событие относиться к элементу данных, который принадлежит указанной группе элементов данных не содержащей указанную строку|
|Тип события|=|**Элемент данных в состоянии "не поддерживается"** - соответствует событию, когда элемент данных переходит из состояния 'активировано' в состояние 'не поддерживается'<br>**Правило низкоуровневого обнаружения в состоянии "не поддерживается"** - соответствует событию, когда правило низкоуровневого обнаружения переходит из состояния 'активировано' в состояние 'не поддерживается'<br>**Триггер в состоянии "неизвестно"** - соответствует событию, когда триггер переходит из состояния 'активировано' в состояние 'неизвестно'|
|Группа узла сети|=<br><>|Укажите группу узла сети или группу узла сети для исключения.<br>**=** - событие относится к указанной группе узлов сети.<br>**<>** - событие не относится к указанной группе узлов сети.|
|Шаблон|=<br><>|Укажите шаблон или шаблон для исключения.<br>**=** - событие относится к элементу данных/триггеру/правилу низкоуровневого обнаружения унаследованных из указанного шаблона.<br>**<>** - событие не относиться к элементу данных/триггеру/правилу низкоуровневого обнаружения унаследованных из указанного шаблона.|
|Узел сети|=<br><>|Укажите узел сети или узел сети для исключения.<br>**=** - событие относиться к указанному узлу сети.<br>**<>** - событие не относиться к указанному узлу сети.|

[comment]: # ({/new-6ef58449})

[comment]: # ({new-6e7e1ac8})
##### Тип вычисления

Для вычисления условий доступны следующие опции:

-   **И** - должны быть выполнены все условия

Обратите внимание, что вычисление "И" не нужно использовать между
несколькими триггерами, когда они добавлены с условием `​Триггер=`.
Действия могут выполняться, основываясь на событии только одного
триггера.

-   **Или** - достаточно выполнения одного условия
-   **И/Или** - комбинация из двух опций: И с различными типами условий
    и ИЛИ с одинаковым типом условий, например:

*Группа узелов сети* = Oracle сервера\
*Группа узлов сети* = MySQL сервера\
*Имя триггера* содержит 'База данных не работает'\
*Имя триггера* содержит 'База данных недоступна'

вычисляется как

**(**Группа узлов сети = Oracle сервера **или** Группа узлов сети =
MySQL сервера**)** **и** **(**Имя триггера содержит 'База данных не
работает' **или** Имя триггера содержит 'База данных недоступна'**)**

-   **Пользовательское выражение** - формула вычисления, введенная
    пользователем, для оценки условий действия. Она должна включать в
    себя все условия (представленные в виде прописных букв A, B, C, ...)
    и может включать пробелы, символы табуляции, скобки ( ), **and** (с
    учетом регистра), **or** (с учетом регистра), **not** (с учетом
    регистра).

Тогда как прерыдущий пример с `И/Или` был бы представлен в виде (A или
B) и (C или D), в пользовательском выражении вы также можете
использовать несколько других методов вычисления:

(A и B) и (C или D)\
(A и B) или (C и D)\
((A или B) и C) или D\
(not (A or B) and C) or not D\
и так далее.

[comment]: # ({/new-6e7e1ac8})

[comment]: # ({new-79be7a27})
#### Service actions

The following conditions can be used in service actions:

|Condition type|Supported operators|Description|
|--|--|------|
|*Service*|equals<br>does not equal|Specify a service or a service to exclude.<br>**equals** - event belongs to this service.<br>**does not equal** - event does not belong to this service.<br>Specifying a parent service implicitly selects all child services. To specify the parent service only, all nested services have to be additionally set with the **does not equal** operator.|
|*Service name*|contains<br>does not contain|Specify a string in the service name or a string to exclude.<br>**contains** - event is generated by a service, containing this string in the name.<br>**does not contain** - this string cannot be found in the service name.|
|*Service tag name*|equals<br>does not equal<br>contains<br>does not contain|Specify an event tag or an event tag to exclude. Service event tags can be defined in the service configuration section *Tags*.<br>**equals** - event has this tag<br>**does not equal** - event does not have this tag<br>**contains** - event has a tag containing this string<br>**does not contain** - event does not have a tag containing this string.|
|*Service tag value*|equals<br>does not equal<br>contains<br>does not contain|Specify an event tag and value combination or a tag and value combination to exclude. Service event tags can be defined in the service configuration section *Tags*.<br>**equals** - event has this tag and value<br>**does not equal** - event does not have this tag and value<br>**contains** - event has a tag and value containing these strings<br>**does not contain** - event does not have a tag and value containing these strings.|

:::noteimportant
Make sure to define [message templates](/manual/config/notifications/media#overview) for Service actions in the *Alerts -> Media types* menu. Otherwise, the notifications will not be sent. 
:::

[comment]: # ({/new-79be7a27})







[comment]: # ({new-6df27208})
#### Discovery actions

The following conditions can be set for discovery-based events:

|Condition type|Supported operators|Description|
|--------------|-------------------|-----------|
|*Host IP*|equals<br>does not equal|Specify an IP address range or a range to exclude for a discovered host.<br>**equals** - host IP is in the range.<br>**does not equal** - host IP is not in the range.<br>It may have the following formats:<br>Single IP: 192.168.1.33<br>Range of IP addresses: 192.168.1-10.1-254<br>IP mask: 192.168.4.0/24<br>List: 192.168.1.1-254, 192.168.2.1-100, 192.168.2.200, 192.168.4.0/24<br>Support for spaces in the list format is provided since Zabbix 3.0.0.|
|*Service type*|equals<br>does not equal|Specify a service type of a discovered service or a service type to exclude.<br>**equals** - matches the discovered service.<br>**does not equal** - does not match the discovered service.<br>Available service types: SSH, LDAP, SMTP, FTP, HTTP, HTTPS *(available since Zabbix 2.2 version)*, POP, NNTP, IMAP, TCP, Zabbix agent, SNMPv1 agent, SNMPv2 agent, SNMPv3 agent, ICMP ping, telnet *(available since Zabbix 2.2 version)*.|
|*Service port*|equals<br>does not equal|Specify a TCP port range of a discovered service or a range to exclude.<br>**equals** - service port is in the range.<br>**does not equal** - service port is not in the range.|
|*Discovery rule*|equals<br>does not equal|Specify a discovery rule or a discovery rule to exclude.<br>**equals** - using this discovery rule.<br>**does not equal** - using any other discovery rule, except this one.|
|*Discovery check*|equals<br>does not equal|Specify a discovery check or a discovery check to exclude.<br>**equals** - using this discovery check.<br>**does not equal** - using any other discovery check, except this one.|
|*Discovery object*|equals|Specify the discovered object.<br>**equals** - equal to discovered object (a device or a service).|
|*Discovery status*|equals|**Up** - matches 'Host Up' and 'Service Up' events<br>**Down** - matches 'Host Down' and 'Service Down' events<br>**Discovered** - matches 'Host Discovered' and 'Service Discovered' events<br>**Lost** - matches 'Host Lost' and 'Service Lost' events|
|*Uptime/Downtime*|is greater than or equals<br>is less than or equals|Uptime for 'Host Up' and 'Service Up' events. Downtime for 'Host Down' and 'Service Down' events.<br>**is greater than or equals** - is more or equal to. Parameter is given in seconds.<br>**is less than or equals** - is less or equal to. Parameter is given in seconds.|
|*Received value*|equals<br>does not equal<br>is greater than or equals<br>is less than or equals<br>contains<br>does not contain|Specify the value received from an agent (Zabbix, SNMP) check in a discovery rule. String comparison. If several Zabbix agent or SNMP checks are configured for a rule, received values for each of them are checked (each check generates a new event which is matched against all conditions).<br>**equals** - equal to the value.<br>**does not equal** - not equal to the value.<br>**is greater than or equals** - more or equal to the value.<br>**is less than or equals** - less or equal to the value.<br>**contains** - contains the substring. Parameter is given as a string.<br>**does not contain** - does not contain the substring. Parameter is given as a string.|
|*Proxy*|equals<br>does not equal|Specify a proxy or a proxy to exclude.<br>**equals** - using this proxy.<br>**does not equal** - using any other proxy except this one.|

::: noteclassic
Service checks in a discovery rule, which result in
discovery events, do not take place simultaneously. Therefore, if
**multiple** values are configured for `Service type`, `Service port` or
`Received value` conditions in the action, they will be compared to one
discovery event at a time, but **not** to several events simultaneously.
As a result, actions with multiple values for the same check types may
not be executed correctly.
:::

[comment]: # ({/new-6df27208})

[comment]: # ({new-e9b18b74})
#### Autoregistration actions

The following conditions can be set for actions based on active agent
autoregistration:

|Condition type|Supported operators|Description|
|--------------|-------------------|-----------|
|*Host metadata*|contains<br>does not contain<br>matches<br>does not match|Specify host metadata or host metadata to exclude.<br>**contains** - host metadata contains the string.<br>**does not contain** - host metadata does not contain the string.<br>Host metadata can be specified in an [agent configuration file](/manual/appendix/config/zabbix_agentd).<br>**matches** - host metadata matches regular expression.<br>**does not match** - host metadata does not match regular expression.|
|*Host name*|contains<br>does not contain<br>matches<br>does not match|Specify a host name or a host name to exclude.<br>**contains** - host name contains the string.<br>**does not contain** - host name does not contain the string.<br>**matches** - host name matches regular expression.<br>**does not match** - host name does not match regular expression.|
|*Proxy*|equals<br>does not equal|Specify a proxy or a proxy to exclude.<br>**equals** - using this proxy.<br>**does not equal** - using any other proxy except this one.|

[comment]: # ({/new-e9b18b74})

[comment]: # ({new-133d6ae1})
#### Internal event actions

The following conditions can be set for actions based on internal
events:

|Condition type|Supported operators|Description|
|--------------|-------------------|-----------|
|*Event type*|equals|**Item in "not supported" state** - matches events where an item goes from a 'normal' to 'not supported' state<br>**Low-level discovery rule in "not supported" state** - matches events where a low-level discovery rule goes from a 'normal' to 'not supported' state<br>**Trigger in "unknown" state** - matches events where a trigger goes from a 'normal' to 'unknown' state|
|*Host group*|equals<br>does not equal|Specify host groups or host groups to exclude.<br>**equals** - event belongs to this host group.<br>**does not equal** - event does not belong to this host group.|
|*Tag name*|equals<br>does not equal<br>contains<br>does not contain|Specify event tag or event tag to exclude.<br>**equals** - event has this tag<br>**does not equal** - event does not have this tag<br>**contains** - event has a tag containing this string<br>**does not contain** - event does not have a tag containing this string|
|*Tag value*|equals<br>does not equal<br>contains<br>does not contain|Specify event tag and value combination or tag and value combination to exclude.<br>**equals** - event has this tag and value<br>**does not equal** - event does not have this tag and value<br>**contains** - event has a tag and value containing these strings<br>**does not contain** - event does not have a tag and value containing these strings|
|*Template*|equals<br>does not equal|Specify templates or templates to exclude.<br>**equals** - event belongs to an item/trigger/low-level discovery rule inherited from this template.<br>**does not equal** - event does not belong to an item/trigger/low-level discovery rule inherited from this template.|
|*Host*|equals<br>does not equal|Specify hosts or hosts to exclude.<br>**equals** - event belongs to this host.<br>**does not equal** - event does not belong to this host.|

[comment]: # ({/new-133d6ae1})

[comment]: # ({new-8c003f8b})
#### Type of calculation

The following options of calculating conditions are available:

-   **And** - all conditions must be met

Note that using "And" calculation is disallowed between several triggers
when they are selected as a `Trigger=` condition. Actions can only be
executed based on the event of one trigger.

-   **Or** - enough if one condition is met
-   **And/Or** - combination of the two: AND with different condition
    types and OR with the same condition type, for example:

*Host group* equals Oracle servers\
*Host group* equals MySQL servers\
*Trigger name* contains 'Database is down'\
*Trigger name* contains 'Database is unavailable'

is evaluated as

**(**Host group equals Oracle servers **or** Host group equals MySQL
servers**)** **and** **(**Trigger name contains 'Database is down'
**or** Trigger name contains 'Database is unavailable'**)**

-   **Custom expression** - a user-defined calculation formula for
    evaluating action conditions. It must include all conditions
    (represented as uppercase letters A, B, C, ...) and may include
    spaces, tabs, brackets ( ), **and** (case sensitive), **or** (case
    sensitive), **not** (case sensitive).

While the previous example with `And/Or` would be represented as (A or
B) and (C or D), in a custom expression you may as well have multiple
other ways of calculation:

(A and B) and (C or D)\
(A and B) or (C and D)\
((A or B) and C) or D\
(not (A or B) and C) or not D\
etc.

[comment]: # ({/new-8c003f8b})

[comment]: # ({new-fca3d6de})
#### Actions disabled due to deleted objects

If a certain object (host, template, trigger, etc.) used in an action
condition/operation is deleted, the condition/operation is removed and
the action is disabled to avoid incorrect execution of the action. The
action can be re-enabled by the user.

This behavior takes place when deleting:

-   host groups ("host group" condition, "remote command" operation on a
    specific host group);
-   hosts ("host" condition, "remote command" operation on a specific
    host);
-   templates ("template" condition, "link to template" and "unlink from
    template" operations);
-   triggers ("trigger" condition);
-   discovery rules (when using "discovery rule" and "discovery check"
    conditions).

*Note*: If a remote command has many target hosts, and we delete one of
them, only this host will be removed from the target list, the operation
itself will remain. But, if it's the only host, the operation will be
removed, too. The same goes for "link to template" and "unlink from
template" operations.

Actions are not disabled when deleting a user or user group used in a
"send message" operation.

[comment]: # ({/new-fca3d6de})
