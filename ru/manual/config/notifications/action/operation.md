[comment]: # translation:outdated

[comment]: # ({new-531c9a02})
# 2 Операции

[comment]: # ({/new-531c9a02})

[comment]: # ({new-49564975})
#### Обзор

Вы можете указать следующие операции для всех источников:

-   отправка сообщений
-   выполнение удаленных команд (включая IPMI)

::: noteimportant
Zabbix сервер не создает оповещения, если доступ к
узлу сети "запрещен" пользователю явным образом, который задан в
операции получателем, или, если пользователь вовсе не имеет заданных
прав доступа к этому узлу сети.
:::

Для событий обнаружения и авторегистрации, также доступны дополнительные
операции:

-   [добавление узла
    сети](/ru/manual/config/notifications/action/operation/other#добавление_узла_сети)
-   добавление узла сети
-   удаление узла сети
-   активация узла сети
-   деактивация узла сети
-   добавление в группу узлов сети
-   удаление из группы узлов сети
-   присоединение шаблона
-   отсоединение шаблона
-   установка режима инвентарных данных узлам сети

[comment]: # ({/new-49564975})

[comment]: # ({new-4b8a4b65})
#### Настройка операции

Для настройки операции, в диалоге
[настройки](/ru/manual/config/notifications/action) действия перейдите
на вкладку *Операции* и в блоке Операции нажмите на *Новая*. Измените
шаг операции и нажмите *Добавить* для добавления в список *Операции*.

Атрибуты операции:

![](../../../../../assets/en/manual/config/action_operation2.png){width="600"}

Все обязательные поля ввода отмечены красной звёздочкой.

|Параметр|<|<|<|<|Описание|
|----------------|-|-|-|-|----------------|
|*Длительность шага операции по умолчанию*|<|<|<|<|Длительность одного шага операции по умолчанию (от 60 секунд до 1 недели).<br>Например, длительность шага в один час означает, что если эта операция выполняется, то следующая операция начнется через один час.<br>Начиная с Zabbix 3.4.0, поддерживаются [суффиксы времени](/ru/manual/appendix/suffixes), например, 60s, 1m, 2h, 1d.<br>Поддерживаются [пользовательские макросы](/ru/manual/config/macros/usermacros) начиная с 3.4.0.|
|*Тема по умолчанию*|<|<|<|<|Тема сообщений по умолчанию для оповещений. Тема может содержать [макросы](/ru/manual/config/notifications/action/operation/macros). Это поле ограничено длиной в 255 символов.|
|*Сообщение по умолчанию*|<|<|<|<|Сообщение по умолчанию для оповещений. Сообщение может содержать [макросы](/ru/manual/config/notifications/action/operation/macros). Это поле ограничено некоторым количеством символов, которое зависит от типа базы данных (для получения более подробных сведений смотрите [Отправка сообщения](/ru/manual/config/notifications/action/operation/message)).|
|*Приостановить операции для подавленных проблем*|<|<|<|<|Отметьте эту опцию для задержки начала операций в течении периода обслуживания. Когда операции начнутся, после периода обслуживания, выполнятся все операции, включая те, которые для событий во время обслуживания.<br>Если вы не отметите, операции будут выполняться без задержки даже в процессе периода обслуживания.<br>Эта опция поддерживается начиная с Zabbix 3.2.0.|
|*Операции*|<|<|<|<|Операции действия отображаются со следующими деталями:<br>**Шаги** - шаг(и) эскалации к которым назначаются операции<br>**Детали** - тип операции и ее получатель/цель.<br>Начиная с Zabbix 2.2, список операций также отражает способ оповещений (e-mail, SMS, Jabber и т.п.), используемый для отправки сообщения, а также имя и фамилию (в круглых скобках после псевдонима) получателя оповещения.<br>**Начинать в** - через какое время после события выполнится эта операция<br>**Длительность (сек)** - отображение длительности шага. Отображается *по умолчанию*, если шаг имеет длительность по умолчанию, и отображается время, если используется указанное время.<br>**Действие** - отображаются ссылки для изменения и удаления операции.<br>Для добавления новой операции, нажмите на *Новый*.|
|Детали операций|<|<|<|<|Этот блок используется для настройки параметров операции.|
|<|*Шаги*|<|<|<|Выберите шаг(и) для назначения операции в расписание [эскалаций](escalations):<br>**От** - выполнять, начиная с этого шага<br>**До** - выполнять, до этого шага (0=бесконечно, выполнение не будет ограничено)|
|^|*Длительность шага*|<|<|<|Пользовательская длительность для этих шагов (0=использовать длительность шага по умолчанию).<br>Начиная с Zabbix 3.4.0, поддерживаются [суффиксы времени](/ru/manual/appendix/suffixes), например, 60s, 1m, 2h, 1d.<br>Поддерживаются [пользовательские макросы](/ru/manual/config/macros/usermacros) начиная с 3.4.0.<br>На один шаг можно назначить несколько операций. Если такие операции будут иметь разную длительность шага, то учитывается самая короткая и она будет применена к шагу.|
|^|*Тип операции*|<|<|<|Для всех событий доступно два типа операций:<br>**Отправлять сообщение** - отправление сообщения пользователю<br>**Удаленная команда** - выполнение удаленной команды<br>Остальные операции доступны для событий основанных на обнаружении и автоматической регистрации (см. выше).|
|^|<|Тип операции: [отправка сообщений](/ru/manual/config/notifications/action/operation/message)|<|<|<|
|^|^|*Отправлять группам<br>пользователей*|<|<|Нажмите на *Добавить* для выбора групп пользователей, которым будет отправляться сообщение.<br>Группа пользователей должна иметь по крайней мере [права доступа](/ru/manual/config/users_and_usergroups/permissions) на "чтение" узла сети, чтобы получить уведомления.|
|^|^|*Отправлять пользователям*|<|<|Нажмите на *Добавить* для выбора пользователей, которым будет отправляться сообщение.<br>Пользователь должен иметь по крайней мере [права доступа](/ru/manual/config/users_and_usergroups/permissions) на "чтение" узла сети, чтобы получить уведомления.|
|^|^|*Отправлять только через*|<|<|Отправка сообщения по всем заданным способам оповещений или только по выбранному.|
|^|^|*Сообщение по умолчанию*|<|<|Если выбрано, будет использовано сообщение по умолчанию (смотрите выше).|
|^|^|*Тема*|<|<|Тема пользовательского сообщения. Тема может содержать макросы. Это поле ограничено длиной в 255 символов.|
|^|^|*Сообщение*|<|<|Пользовательское сообщение. Сообщение может содержать макросы. Это поле ограничено некоторым количеством символов, которое зависит от типа базы данных (для получения более подробных сведений смотрите [Отправка сообщения](/ru/manual/config/notifications/action/operation/message)).|
|^|^|Тип операции: [удаленная команда](/ru/manual/config/notifications/action/operation/remote_command)|<|<|<|
|^|^|*Список целей*|<|<|Выберите цели, на которых будет выполняться команда:<br>**Текущий узел сети** - команда будет выполнена на узле сети триггер которого вызвал это событие о проблеме. Эта опция не будет работать, если триггер содержит несколько узлов сети.<br>**Узел сети** - выберите узел/узлы сети, на котором команда будет выполнена.<br>**Группа узлов сети** - выберите группу(ы) узлов сети, на котором команда будет выполнена. Указав родительскую группу узлов сети косвенным образом будут выбраны все вложенные группы узлов сети. Таким образом, удаленная команда будет также выполнена на узлах сети из вложенных групп.<br>Команда выполняется на узле сети только один раз, независимо от того совпадает ли узел сети более одного раза (например, входит в несколько групп узлов сети; указан отдельно и входит в группу узлов сети).<br>Список целей не имеет смысла, если пользовательский скрипт выполняется на Zabbix сервере. В этом случае выбор нескольких целей приведет только к тому, что скрипт выполнится на сервере несколько раз.<br>Обратите внимание, выбор цели также зависит от настройки *Группа пользователей* в [конфигурации](/ru/manual/web_interface/frontend_sections/administration/scripts#настройка_глобального_скрипта) глобального скрипта.|
|^|^|*Тип*|<|<|Выберите тип команды:<br>**IPMI** - выполнение [IPMI команды](/ru/manual/config/notifications/action/operation/remote_command#удаленные_команды_ipmi)<br>**Пользовательский скрипт** - выполнение пользовательского набора команд.<br>**SSH** - выполнение SSH команды<br>**Telnet** - выполнение Telnet команды<br>**Глобальный скрипт** - выполнение одного из глобальных скриптов определенных в *Администрирование→Скрипты*.|
|^|^|*Выполнить на*|<|<|Выполнение пользовательского скрипта на:<br>**Zabbix агент** - скрипт будет выполнен Zabbix агентом на хосте<br>**Zabbix сервер (прокси)** - скрипт будет выполнен Zabbix сервером или прокси - в зависимости от того кем наблюдается узел сети сервером или прокси<br>**Zabbix сервер** - скрипт будет выполнен только на стороне Zabbix сервера<br>Для выполнения скриптов на стороне агента необходимо [настроить](/ru/manual/appendix/config/zabbix_agentd) (включён параметр *EnableRemoteCommands*) разрешение выполнения удаленных команд с сервера.<br>Для выполнения скриптов на стороне прокси необходимо [настроить](/manual/appendix/config/zabbix_proxy) (включён параметр *EnableRemoteCommands*) разрешение выполнения удаленных команд с сервера.<br>Это поле доступно, если как *Тип* выбран 'Пользовательский скрипт'.|
|^|^|*Команды*|<|<|Введите команду(ы).<br>Поддерживаемые макросы будут раскрыты на основании выражения триггера, которое вызвало событие. Например, макрос узла сети будет раскрыт в узлы сети из выражения триггера (не в узлы сети из списка целей).|
|<|*Условия*|<|<|<|Условие выполнения операции:<br>**Не подтверждено** - только, если событие не подтверждено<br>**Подтверждено** - только, если событие подтверждено|

[comment]: # ({/new-4b8a4b65})


[comment]: # ({new-4a37f720})
#### Operation details

![](../../../../../assets/en/manual/config/operation_details.png)

|Parameter|<|<|<|Description|
|---------|-|-|-|-----------|
|*Operation*|<|<|<|Select the operation:<br>**Send message** - send message to user<br>**<remote command name>** - execute a remote command. Commands are available for execution if previously defined in [global scripts](/manual/web_interface/frontend_sections/administration/scripts#configuring_a_global_script) with *Action operation* selected as its scope.<br>More operations are available for discovery and autoregistration based events (see above).|
|*Steps*|<|<|<|Select the step(s) to assign the operation to in an [escalation](escalations) schedule:<br>**From** - execute starting with this step<br>**To** - execute until this step (0=infinity, execution will not be limited)|
|*Step duration*|<|<|<|Custom duration for these steps (0=use default step duration).<br>[Time suffixes](/manual/appendix/suffixes) are supported, e.g. 60s, 1m, 2h, 1d, since Zabbix 3.4.0.<br>[User macros](/manual/config/macros/user_macros) are supported, since Zabbix 3.4.0.<br>Several operations can be assigned to the same step. If these operations have different step duration defined, the shortest one is taken into account and applied to the step.|
|<|Operation type: [send message](/manual/config/notifications/action/operation/message)|<|<|<|
|^|*Send to user groups*|<|<|Click on *Add* to select user groups to send the message to.<br>The user group must have at least "read" [permissions](/manual/config/users_and_usergroups/permissions) to the host in order to be notified.|
|^|*Send to users*|<|<|Click on *Add* to select users to send the message to.<br>The user must have at least "read" [permissions](/manual/config/users_and_usergroups/permissions) to the host in order to be notified.|
|^|*Send only to*|<|<|Send message to all defined media types or a selected one only.|
|^|*Custom message*|<|<|If selected, the custom message can be configured.<br>For notifications about internal events via [webhooks](/manual/config/notifications/media/webhook), custom message is mandatory.|
|^|*Subject*|<|<|Subject of the custom message. The subject may contain macros. It is limited to 255 characters.|
|^|*Message*|<|<|The custom message. The message may contain macros. It is limited to certain amount of characters depending on the type of database (see [Sending message](/manual/config/notifications/action/operation/message) for more information).|
|^|Operation type: [remote command](/manual/config/notifications/action/operation/remote_command)|<|<|<|
|^|*Target list*|<|<|Select targets to execute the command on:<br>**Current host** - command is executed on the host of the trigger that caused the problem event. This option will not work if there are multiple hosts in the trigger.<br>**Host** - select host(s) to execute the command on.<br>**Host group** - select host group(s) to execute the command on. Specifying a parent host group implicitly selects all nested host groups. Thus the remote command will also be executed on hosts from nested groups.<br>A command on a host is executed only once, even if the host matches more than once (e.g. from several host groups; individually and from a host group).<br>The target list is meaningless if a custom script is executed on Zabbix server. Selecting more targets in this case only results in the script being executed on the server more times.<br>Note that for global scripts, the target selection also depends on the *Host group* setting in global script [configuration](/manual/web_interface/frontend_sections/administration/scripts#configuring_a_global_script).<br>*Target list* option is not available for *Service actions* because in this case remote commands are always executed on Zabbix server.|
|*Conditions*|<|<|<|Condition for performing the operation:<br>**Not ack** - only when the event is unacknowledged<br>**Ack** - only when the event is acknowledged.<br>*Conditions* option is not available for *Service actions*.|

When done, click on *Add* to add the operation to the list of
*Operations*.

[comment]: # ({/new-4a37f720})
