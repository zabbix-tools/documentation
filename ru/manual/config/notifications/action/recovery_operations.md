[comment]: # translation:outdated

[comment]: # ({new-10ba847f})
# 3 Операции восстановления

[comment]: # ({/new-10ba847f})

[comment]: # ({new-e5432fa9})
#### Обзор

Операции восстановления позволяют вам получать оповещения когда проблемы
решены.

В операциях восстановления поддерживаются как сообщения, так и удаленные
команды. Операции восстановления не поддерживают эскалации - все
операции назначаются на единственный шаг.

[comment]: # ({/new-e5432fa9})

[comment]: # ({new-960d3247})
#### Сценарии применения

Некоторые сценарии использования операций восстановления заключаются в
следующем:

1.  Оповещение всех пользователей, которые были оповещены о проблеме

```{=html}
<!-- -->
```
       * Выберите типом операции 'Отправка сообщения о восстановлении'
    - Наличие нескольких операций по восстановлению: отправка оповещений и удаленная команда
       * Добавьте типы операций для отправки сообщения и выполнения команды
    - Открытие задачи во внешей системе поддержки клиентов/системе задач и закрытие этих задач при решении проблемы
       * Создайте внешний скрипт, который будет взаимодействует с системой поддержки клиентов
       * Создайте действие, которое имеет операцию, которая выполняет этот скрипт и таким образом открывает задачу
       * Добавьте операцию восстановления, которая выполнит этот скрипт с другими параметрами и закроет задачу
       * Используйте макрос {EVENT.ID} как ссылку на оригинальную проблему

[comment]: # ({/new-960d3247})

[comment]: # ({new-82501aec})
#### Настройка операции восстановления

Для настройки операции восстановления:

-   Перейдите на вкладку *Операции восстановления* в
    [настройках](/ru/manual/config/notifications/action) действия
-   Нажмите на *Новый* в блоке Операций
-   Измените детали операции и нажмите на *Добавить*

Можно добавить несколько операций.

Атрибуты операции восстановления:

![](../../../../../assets/en/manual/config/recovery_operation1.png){width="600"}

Все обязательные поля ввода отмечены красной звёздочкой.

|Параметр|<|<|<|<|Описание|
|----------------|-|-|-|-|----------------|
|*Тема по умолчанию*|<|<|<|<|Тема сообщений по умолчанию для оповещений о восстановлении. Тема может содержать [макросы](/ru/manual/config/notifications/action/operation/macros).|
|*Сообщение по умолчанию*|<|<|<|<|Сообщение по умолчанию для оповещений о восстановлении. Сообщение может содержать [макросы](/ru/manual/config/notifications/action/operation/macros).|
|*Операции*|<|<|<|<|Детали операции восстановления.<br>Для настройки новой операции восстановления, нажмите на *Новая*.<br>|
|Детали операций|<|<|<|<|Этот блок используется для настройки деталей операции восстановления.|
|<|*Тип операции*|<|<|<|Для событий восстановления доступны три типа операций:<br>**Отправлять сообщение** - отправление сообщения о восстановлении конкретному пользователю<br>**Удаленная команда** - выполнение удаленной команды<br>**Оповещать всех участников** - отправка сообщения о восстановлении всем пользователя, которые ранее получали оповещения о событии проблем<br>Обратите внимание что, если в нескольких типах операций задан один и тот же получатель с не изменёнными темой/сообщением по умолчанию, тогда дубликаты оповещений отправлены не будут.|
|^|<|Тип операции: [отправлять сообщение](/ru/manual/config/notifications/action/operation/message)|<|<|<|
|^|^|*Отправлять группам<br>пользователей*|<|<|Нажмите на *Добавить* для выбора групп пользователей, которым будет отправляться сообщение о восстановлении.<br>Группа пользователей должна иметь по крайней мере [права доступа](/ru/manual/config/users_and_usergroups/permissions) на "чтение" узла сети, чтобы получить уведомления.|
|^|^|*Отправлять пользователям*|<|<|Нажмите на *Добавить* для выбора пользователей, которым будет отправляться сообщение о восстановлении.<br>Пользователь должен иметь по крайней мере [права доступа](/ru/manual/config/users_and_usergroups/permissions) на "чтение" узла сети, чтобы получить уведомления.|
|^|^|*Отправлять только через*|<|<|Отправка сообщения о восстановлении по всем заданным способам оповещений или только по выбранному.|
|^|^|*Сообщение по умолчанию*|<|<|Если выбрано, будет использовано сообщение по умолчанию (смотрите выше).|
|^|^|*Тема*|<|<|Тема пользовательского сообщения. Тема может содержать макросы.|
|^|^|*Сообщение*|<|<|Пользовательское сообщение. Сообщение может содержать макросы.|
|^|^|Тип операции: [удаленная команда](/ru/manual/config/notifications/action/operation/remote_command)|<|<|<|
|^|^|*Список целей*|<|<|Выберите цели для выполнения команды:<br>**Текущий узел сети** - команда выполнится на узле сети триггера, который вызвал событие о проблеме. Эта опция не будет работать, если в выражении триггера имеется несколько узлов сети.<br>**Узел сети** - выберите узел(ы) сети для выполнения команды.<br>**Группа узлов сети** - выберите группу(ы) узлов сети для выполнения команды. Указав родительскую группу узлов сети косвенным образом будут выбраны все вложенные группы узлов сети. Таким образом команда будет выполнена на узлах сети из вложенных групп.<br>Команда на узле сети выполняется только один раз даже, если узел сети соответствует боле одного раза (например, из нескольких групп узлов сети; отдельно и из группы узлов сети).<br>Список целей не имеет смысла, если команда выполняется на стороне Zabbix сервера. В этом случае выбор нескольких целей приведет только к тому, что команда выполнится на сервере несколько раз.<br>Обратите внимание, что в случае глобальных скриптов выбор цели также зависит от настройки *Группа узлов сети* в [настройках](/ru/manual/web_interface/frontend_sections/administration/scripts#настройка_глобального_скрипта) глобального скрипта.|
|^|^|*Тип*|<|<|Выберите тип команды:<br>**IPMI** - выполнение [IPMI команды](/ru/manual/config/notifications/action/operation/remote_command#удаленные_команды_через_ipmi)<br>**Пользовательский скрипт** - выполнение пользовательского набора команд.<br>**SSH** - выполнение SSH команды<br>**Telnet** - выполнение Telnet команды<br>**Глобальный скрипт** - выполнение одного из глобальных скриптов определенных в *Администрирование→Скрипты*.|
|^|^|*Выполнить на*|<|<|Выполнение пользовательского скрипта на:<br>**Zabbix агент** - скрипт будет выполнен на узле сети Zabbix агентом<br>**Zabbix сервер (прокси)** - скрипт будет выполнен Zabbix сервером или прокси - в зависимости от того кем наблюдается узел сети сервером или прокси<br>**Zabbix сервер** - скрипт будет выполнен только на стороне Zabbix сервера<br>Для выполнения скриптов на стороне агента необходимо [настроить](/ru/manual/appendix/config/zabbix_agentd) разрешение выполнения удаленных команд с сервера.<br>Это поле доступно, если как *Тип* выбран 'Пользовательский скрипт'.|
|^|^|*Команды*|<|<|Введите команду(ы).<br>Поддерживаемые макросы будут раскрыты на основании выражения триггера, которое вызвало событие. Например, макрос узла сети будет раскрыт в узлы сети из выражения триггера (не в узлы сети из списка целей).|
|^|<|Тип операции: оповещать всех участников|<|<|<|
|^|^|*Сообщение по умолчанию*|<|<|Если выбрано, будет использовано сообщение по умолчанию (смотрите выше).|
|^|^|*Тема*|<|<|Тема пользовательского сообщения. Тема может содержать макросы.|
|^|^|*Сообщение*|<|<|Пользовательское сообщение. Сообщение может содержать макросы.|

[comment]: # ({/new-82501aec})


[comment]: # ({new-f2127d8b})
#### Recovery operation details

![](../../../../../assets/en/manual/config/recovery_operation_details.png)

|Parameter|<|<|<|Description|
|---------|-|-|-|-----------|
|*Operation*|<|<|<|Three operation types are available for recovery events:<br>**Send message** - send recovery message to specified user<br>**Notify all involved** - send recovery message to all users who were notified on the problem event<br>**<remote command name>** - execute a remote command. Commands are available for execution if previously defined in [global scripts](/manual/web_interface/frontend_sections/administration/scripts#configuring_a_global_script) with *Action operation* selected as its scope.<br>Note that if the same recipient is defined in several operation types without specified *Custom message*, duplicate notifications are not sent.|
|<|Operation type: [send message](/manual/config/notifications/action/operation/message)|<|<|<|
|^|*Send to user groups*|<|<|Click on *Add* to select user groups to send the recovery message to.<br>The user group must have at least "read" [permissions](/manual/config/users_and_usergroups/permissions) to the host in order to be notified.|
|^|*Send to users*|<|<|Click on *Add* to select users to send the recovery message to.<br>The user must have at least "read" [permissions](/manual/config/users_and_usergroups/permissions) to the host in order to be notified.|
|^|*Send only to*|<|<|Send default recovery message to all defined media types or a selected one only.|
|^|*Custom message*|<|<|If selected, a custom message can be defined.|
|^|*Subject*|<|<|Subject of the custom message. The subject may contain macros.|
|^|*Message*|<|<|The custom message. The message may contain macros.|
|^|Operation type: [remote command](/manual/config/notifications/action/operation/remote_command)|<|<|<|
|^|*Target list*|<|<|Select targets to execute the command on:<br>**Current host** - command is executed on the host of the trigger that caused the problem event. This option will not work if there are multiple hosts in the trigger.<br>**Host** - select host(s) to execute the command on.<br>**Host group** - select host group(s) to execute the command on. Specifying a parent host group implicitly selects all nested host groups. Thus the remote command will also be executed on hosts from nested groups.<br>A command on a host is executed only once, even if the host matches more than once (e.g. from several host groups; individually and from a host group).<br>The target list is meaningless if the command is executed on Zabbix server. Selecting more targets in this case only results in the command being executed on the server more times.<br>Note that for global scripts, the target selection also depends on the *Host group* setting in global script [configuration](/manual/web_interface/frontend_sections/administration/scripts#configuring_a_global_script).|
|<|Operation type: notify all involved|<|<|<|
|^|*Custom message*|<|<|If selected, a custom message can be defined.|
|^|*Subject*|<|<|Subject of the custom message. The subject may contain macros.|
|^|*Message*|<|<|The custom message. The message may contain macros.|

All mandatory input fields are marked with a red asterisk. When done,
click on *Add* to add operation to the list of *Recovery operations*.

[comment]: # ({/new-f2127d8b})

[comment]: # ({new-7dbd8d4d})

#### Operation type: [send message](/manual/config/notifications/action/operation/message)

|Parameter|Description|
|--|--------|
|*Send to user groups*|Click on *Add* to select user groups to send the recovery message to.<br>The user group must have at least "read" [permissions](/manual/config/users_and_usergroups/permissions) to the host in order to be notified.|
|*Send to users*|Click on *Add* to select users to send the recovery message to.<br>The user must have at least "read" [permissions](/manual/config/users_and_usergroups/permissions) to the host in order to be notified.|
|*Send only to*|Send default recovery message to all defined media types or a selected one only.|
|*Custom message*|If selected, a custom message can be defined.|
|*Subject*|Subject of the custom message. The subject may contain macros.|
|*Message*|The custom message. The message may contain macros.|

[comment]: # ({/new-7dbd8d4d})

[comment]: # ({new-37fc532b})

#### Operation type: [remote command](/manual/config/notifications/action/operation/remote_command)

|Parameter|Description|
|--|--------|
|*Target list*|Select targets to execute the command on:<br>**Current host** - command is executed on the host of the trigger that caused the problem event. This option will not work if there are multiple hosts in the trigger.<br>**Host** - select host(s) to execute the command on.<br>**Host group** - select host group(s) to execute the command on. Specifying a parent host group implicitly selects all nested host groups. Thus the remote command will also be executed on hosts from nested groups.<br>A command on a host is executed only once, even if the host matches more than once (e.g. from several host groups; individually and from a host group).<br>The target list is meaningless if the command is executed on Zabbix server. Selecting more targets in this case only results in the command being executed on the server more times.<br>Note that for global scripts, the target selection also depends on the *Host group* setting in global script [configuration](/manual/web_interface/frontend_sections/administration/scripts#configuring_a_global_script).|

[comment]: # ({/new-37fc532b})

[comment]: # ({new-945f9a49})

#### Operation type: notify all involved

|Parameter|Description|
|--|--------|
|*Custom message*|If selected, a custom message can be defined.|
|*Subject*|Subject of the custom message. The subject may contain macros.|
|*Message*|The custom message. The message may contain macros.|


[comment]: # ({/new-945f9a49})
