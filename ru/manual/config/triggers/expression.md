[comment]: # translation:outdated

[comment]: # ({new-510ac9cc})
# 2 Выражение триггера

[comment]: # ({/new-510ac9cc})

[comment]: # ({new-473bcd5e})
#### Обзор

Используемые в триггерах выражения являются очень гибкими. Вы можете
использовать их для создания сложных логических тестов, учитывая
статистику по мониторингу.

Простое полезное выражение может выглядеть примерно так:

    {<сервер>:<ключ>.<функция>(<параметр>)}<оператор><константа>

[comment]: # ({/new-473bcd5e})

[comment]: # ({new-d4e5ef03})
#### Функции

Функции триггеров позволяют ссылаться на собранные значения, текущее
время и другие факторы.

Имеется полный список [поддерживаемых
функций](/ru/manual/appendix/triggers/functions).

[comment]: # ({/new-d4e5ef03})

[comment]: # ({new-b255de16})
#### Параметры функций

Большинство числовых функций принимают количество секунд в качестве
параметра.

Вы можете использовать префикс **\#**, чтобы указать что этот параметр
должен иметь другой смысл:

|ВЫЗОВ ФУНКЦИИ|СМЫСЛ|
|-------------------------|----------|
|**sum(600)**|Сумма всех значений за 600 секунд|
|**sum(\#5)**|Сумма последних 5 значений|

Функция **last** использует другой смысл для значений, когда начинается
с решетки - она дает выбрать n-ое предыдущее значение, так что с учетом
значений 3, 7, 2, 6, 5 (от наиболее нового до наиболее старого), при
**last(\#2)** вернется *7* и при **last(\#5)** вернется *5*.

Несколько функций поддерживают дополнительный, второй параметр
`сдвиг_времени`. Этот параметр позволят ссылаться на данные из периода
времени в прошлом. Например, для **avg(1h,1d)** будет возвращено среднее
значение за час днем ранее.

Вы можете использовать поддерживаемые [суффиксы
преобразований](ru/manual/appendix/suffixes) в выражениях триггеров,
например, '5m' (минут) вместо '300' секунд или '1d' (день) вместо
'86400' секунд. '1K' будет состоять из '1024' байт.

[comment]: # ({/new-b255de16})

[comment]: # ({new-cf9e09bb})
#### Операторы

Следующие операторы поддерживаются для триггеров **(представлены по
убыванию приоритета выполнения)**:

|ПРИОРИТЕТ|ОПЕРАТОР|ОПРЕДЕЛЕНИЕ|**Заметки по [неизвестным значениям](/ru/manual/config/triggers/expression#выражения_с_неподдерживаемыми_элементами_данных_и_неизвестными_значениями)**|
|------------------|----------------|----------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|**1**|**-**|Унарный минус|**-**Неизвестно → Неизвестно|
|**2**|**not**|Логическое НЕ|**not** Неизвестно → Неизвестно|
|**3**|**\***|Умножение|0 **\*** Неизвестно → Неизвестно<br>(да, Неизвестно, не 0 - чтобы не потерять<br>Неизвестно в арифметических операциях)<br>1.2 **\*** Неизвестно → Неизвестно|
|<|**/**|Деление|Неизвестно **/** 0 → ошибка<br>Неизвестно **/** 1.2 → Неизвестно<br>0.0 **/** Неизвестно → Неизвестно|
|**4**|**+**|Арифметический плюс|1.2 **+** Неизвестно → Неизвестно|
|<|**-**|Арифметический минус|1.2 **-** Неизвестно → Неизвестно|
|**5**|**<**|Менее чем. Этот оператор может быть представлен в виде:<br><br>A<B ⇔ (A<B-0.000001)|1.2 **<** Неизвестно → Неизвестно|
|<|**<=**|Менее чем или равно.Этот оператор может быть представлен в виде:<br><br>A<=B ⇔ (A≤B+0.000001)|Неизвестно **<=** Неизвестно → Неизвестно|
|<|**>**|Более чем. Этот оператор может быть представлен в виде:<br><br>A>B ⇔ (A>B+0.000001)|<|
|<|**>=**|Более чем или равно. Этот оператор может быть представлен в виде:<br><br>A>=B ⇔ (A≥B-0.000001)|<|
|**6**|**=**|Равенство. Этот оператор может быть представлен в виде:<br><br>A=B ⇔ (A≥B-0.000001) и (A≤B+0.000001)|<|
|<|**<>**|Не равно. Этот оператор может быть представлен в виде:<br><br>A<>B ⇔ (A<B-0.000001) или (A>B+0.000001)|<|
|**7**|**and**|Логическое И|0 **and** Неизвестно → 0<br>1 **and** Неизвестно → Неизвестно<br>Неизвестно **and** Неизвестно → Неизвестно|
|**8**|**or**|Логическое ИЛИ|1 **or** Неизвестно → 1<br>0 **or** Неизвестно → Неизвестно<br>Неизвестно **or** Неизвестно → Неизвестно|

Операторы **not**, **and** and **or** регистрозависимы и должны быть в
нижнем регистре. Они также должны быть окружены символами пробелов или
круглыми скобками.

Все операторы, кроме унарных **-** и **not**, имеют ассоциативность
слева на право. Унарные **-** и **not** не ассоциативны (имеется в виду
необходимо использовать **-(-1)** и **not (not 1)** вместо **--1** и
**not not 1**).

Результат вычисления:

-   Операторы **<**, **<=**, **>**, **>=**, **=**,
    **<>** должны давать '1' в выражении триггера, если указанное
    соотношение правдиво и '0', если оно ложно. Если по крайней мере
    один операнд Неизвестен, то и результат будет Неизвестно;
-   **and** по известным операндам должно давать '1', если оба из этих
    операндов сравнения не равны '0'; в противном случае, будет давать
    '0'; для неизвестных операндов **and** даст '0' только, если один из
    операндов сравнения равен '0'; в противном случае, он даст
    'Неизвестно';
-   **or** по известным операндам должно давать '1', если какой-либо из
    этих операндов сравнения не равен '0'; в противном случае, будет
    давать '0'; для неизвестных операндов **or** даст '1' только, если
    один из операндов сравнения не равен '0'; в противном случае, он
    даст 'Неизвестно';
-   Результат логического операнда отрицания **not** для известного
    операнда равен '0', если значение этого операнда сравнения не равно
    '0'; '1', если значение его операнда сравнения равно '0'. Для
    неизвестных операндов **not** даст 'Неизвестно'.

[comment]: # ({/new-cf9e09bb})

[comment]: # ({new-6699e631})
#### Кэширование значений

Значения, которые требуются для вычисления триггеров, кэшируются Zabbix
сервером. По этой причине такое вычисление триггеров на некоторое время
приводит к более высокой загрузке базы данных после перезапуска сервера.
Кэш значений не очищается, когда значения истории элементов данных
удаляются (либо вручную, либо при помощи автоматической очистки
истории), поэтому сервер будет использовать кэшированные значения пока
они не станут старше, чем периоды времени, которые заданы в функциях
триггеров, либо пока сервер не будет перезапущен.

[comment]: # ({/new-6699e631})

[comment]: # ({new-c5d3ec54})
#### Примеры триггеров

[comment]: # ({/new-c5d3ec54})

[comment]: # ({new-10bc91a2})
##### Пример 1

Высокая загрузка процессора на www.zabbix.com.

    {www.zabbix.com:system.cpu.load[all,avg1].last()}>5

'www.zabbix.com:system.cpu.load\[all,avg1\]' представляет короткое имя
наблюдаемого параметра. Эта строка указывает, что сервер -
'www.zabbix.com' и наблюдаемый ключ - 'system.cpu.load\[all,avg1\]'.
Используя функцию 'last()', мы ссылаемся на самое последнее значение. И
наконец '>5' означает, что триггер перейдет в состояние ПРОБЛЕМА
всякий раз, когда самое новое измерение загрузки процессора на сервере
www.zabbix.com будет превышать 5.

[comment]: # ({/new-10bc91a2})

[comment]: # ({new-195c3f4f})
##### Пример 2

www.zabbix.com перегружен

    {www.zabbix.com:system.cpu.load[all,avg1].last()}>5 or {www.zabbix.com:system.cpu.load[all,avg1].min(10m)}>2 

Это выражение будет истинным, когда либо текущая загрузка процессора
станет более 5, либо загрузка процессора больше значения 2 за последние
10 минут.

[comment]: # ({/new-195c3f4f})

[comment]: # ({new-abac99e3})
##### Пример 3

/etc/passwd был изменен

Используется функция diff:

    {www.zabbix.com:vfs.file.cksum[/etc/passwd].diff()}=1

Это выражение будет истинным, когда предыдущее значение контрольной
суммы файла /etc/passwd отличается от самого нового значения.

Аналогичные выражения могут быть полезны для мониторинга изменений в
важных файлах, таких как /etc/passwd, /etc/inetd.conf, /kernel и других.

[comment]: # ({/new-abac99e3})

[comment]: # ({new-17b607dc})
##### Пример 4

Кто-то скачивает большой файл из Интернет

Используется функция min:

    {www.zabbix.com:net.if.in[eth0,bytes].min(5m)}>100K

Это выражение будет истинным, когда количество полученных байт на eth0
превышает 100 КБ за последних 5 минут.

[comment]: # ({/new-17b607dc})

[comment]: # ({new-9a129732})
##### Пример 5

Оба узла кластера SMTP серверов недоступны

Примечание, в выражении используются два разных узла сети:

    {smtp1.zabbix.com:net.tcp.service[smtp].last()}=0 and {smtp2.zabbix.com:net.tcp.service[smtp].last()}=0

Это выражение будет истинным, когда оба SMTP сервера недоступны на обоих
smtp1.zabbix.com и smtp2.zabbix.com.

[comment]: # ({/new-9a129732})

[comment]: # ({new-49592749})
##### Пример 6

Zabbix агент нуждается в обновлении

Используется функция str():

    {zabbix.zabbix.com:agent.version.str("beta8")}=1

Это выражение будет истинным, когда версия Zabbix агента содержит в себе
'beta8' (возможно 1.0beta8).

[comment]: # ({/new-49592749})

[comment]: # ({new-46e72843})
##### Пример 7

Сервер недоступен

    {zabbix.zabbix.com:icmpping.count(30m,0)}>5

Это выражение будет истинным, если узел сети “zabbix.zabbix.com"
недоступен более 5 раз за последние 30 минут.

[comment]: # ({/new-46e72843})

[comment]: # ({new-7956e04e})
##### Пример 8

Нет данных за последние 3 минуты

Используется функцию nodata():

    {zabbix.zabbix.com:tick.nodata(3m)}=1

Для того, чтобы этот триггер заработал, элемент данных ‘tick’ должен
быть задан как элемент данных типа Zabbix
[траппер](/ru/manual/config/items/itemtypes/trapper). Узел сети должен
периодически отправлять данные этому элементу данных, используя
zabbix\_sender. Если не было получено данных за последние 180 секунд,
значением триггера станет ПРОБЛЕМА.

*Обратие внимание*, что 'nodata' можно использовать с любым типом
элементов данных.

[comment]: # ({/new-7956e04e})

[comment]: # ({new-62cfb014})
##### Пример 9

Активность CPU в ночное время

Используется функция time():

    {zabbix:system.cpu.load[all,avg1].min(5m)}>2 and {zabbix:system.cpu.load[all,avg1].time()}>000000 and {zabbix:system.cpu.load[all,avg1].time()}<060000

Триггер может изменить свое состояние в истинное только в ночное время
(00:00-06:00).

[comment]: # ({/new-62cfb014})

[comment]: # ({new-567b88a9})
##### Пример 10

Проверка синхронизации времени на клиенте со временем на Zabbix сервере

Используется функция fuzzytime():

    {MySQL_DB:system.localtime.fuzzytime(10)}=0

Триггер изменит состояние на проблему тогда, когда локальное время на
сервере MySQL\_DB и Zabbix сервере различаются более чем на 10 секунд.

[comment]: # ({/new-567b88a9})

[comment]: # ({new-2f13cb92})
##### Пример 11

Сравнение средней загрузки сегодня со средним значением загрузки за это
же время вчера (использование второго параметра `сдвиг_времени`).

    {server:system.cpu.load.avg(1h)}/{server:system.cpu.load.avg(1h,1d)}>2

Триггер изменит свое состояние на проблему, если средняя загрузка за
последний час будет в два раза больше чем за аналогичный период времени
вчера.

[comment]: # ({/new-2f13cb92})

[comment]: # ({new-2c48905a})
##### Пример 12

Использование значение другого элемента данных в качестве порогового
значения триггера:

    {Template PfSense:hrStorageFree[{#SNMPVALUE}].last()}<{Template PfSense:hrStorageSize[{#SNMPVALUE}].last()}*0.1

Триггер изменит свое состояние на проблему, если свободное пространство
на диски упадет ниже 10 процентов.

[comment]: # ({/new-2c48905a})

[comment]: # ({new-7e0ba13a})
##### Example 10

CPU activity at any time with exception.

Use of function time() and **not** operator:

    min(/zabbix/system.cpu.load[all,avg1],5m)>2
    and not (dayofweek()=7 and time()>230000)
    and not (dayofweek()=1 and time()<010000)

The trigger may change its state to true at any time,
except for 2 hours on a week change (Sunday, 23:00 - Monday, 01:00).

[comment]: # ({/new-7e0ba13a})

[comment]: # ({new-0e6bfc51})
##### Пример 13

Использование [результата вычисления](#оператры) для получения
количества триггеров больше порога:

    ({server1:system.cpu.load[all,avg1].last()}>5) + ({server2:system.cpu.load[all,avg1].last()}>5) + ({server3:system.cpu.load[all,avg1].last()}>5)>=2

Триггер изменит свое состояние на проблему, если по крайней мере два
триггера из выражения будут больше 5.

[comment]: # ({/new-0e6bfc51})

[comment]: # ({new-47b2e2fd})
#### Гистерезис

Порой нам необходим интервал между состояниями ОК и Проблема, а не
просто порог. Например, мы бы хотели задать триггер, который переходит в
состояние Проблема, когда температура в серверной комнате становится
больше 20C и мы бы хотели чтобы он оставался в этом состоянии пока
температура не опустится ниже 15C.

Чтобы это сделать, сначала мы зададим выражение триггера для события о
проблеме. Затем выберем 'Выражение восстановления' в *Формирование ОК
события* и укажем выражение восстановления для ОК события.

Обратите внимание, что выражение восстановления будет вычислено только
при первом решении события о проблеме. Невозможно решить проблему при
помощи выражения восстановления, если условие проблемы всё еще
присутствует.

[comment]: # ({/new-47b2e2fd})

[comment]: # ({new-bb0f5278})
##### Пример 1

Температура в серверной комнате слишком высокая.

Выражение проблемы:

    {server:temp.last()}>20

Выражение восстановления:

    {server:temp.last()}<=15

[comment]: # ({/new-bb0f5278})

[comment]: # ({new-0cc34ec7})
##### Пример 2

Очень мало свободного места на диске

Выражение проблемы: если меньше 10ГБ за последние 5 минут

    {server:vfs.fs.size[/,free].max(5m)}<10G

Выражение восстановления: если больше 40ГБ за последние 10 минут

    {server:vfs.fs.size[/,free].min(10m)}>40G

[comment]: # ({/new-0cc34ec7})

[comment]: # ({new-adfc6b9d})
#### Выражения с неподдерживаемыми элементами данных и неизвестными значениями

Версии до Zabbix 3.2 очень строго относились к неподдерживаемым
элементам данных в выражениях триггеров. Любой неподдерживаемый элемент
данных в выражении незамедлительно менял значение триггера на
`Неизвестно`.

Начиная с Zabbix 3.2 существует более гибкий подход к неподдерживаемым
элементам данных, допуская неизвестные значения при вычислении
выражений:

-   У некоторых функций их значения не зависят от того поддерживается ли
    элемент данных или нет. Такие функции теперь вычисляются даже, если
    ссылаются на неподдерживаемые элементы данных. Смотрите список в
    разделе [функции и неподдерживаемые элементы
    данных](/ru/manual/appendix/triggers/functions#функции_и_неподдерживаемые_элементы_данных).
-   Логические выражения с ИЛИ и И могут быть вычислены для известных
    значений в двух случаях независимо от неизвестных операндов:
    -   "1 `or` Неподдерживаемый\_элемент\_данных1.некая\_функция() `or`
        Неподдерживаемый\_элемент\_данных2. некая\_функция() `or` ..."
        может быть вычислена как '1' (Правда),
    -   "0 `and` Неподдерживаемый\_элемент\_данных1. некая\_функция()
        `and` Неподдерживаемый\_элемент\_данных2. некая\_функция() `and`
        ..." может быть вычислена как '0' (Ложь).\
        Zabbix пытается вычислить логические выражения принимая
        неподдерживаемые элементы данных как `Неизвестные` значения. В
        двух случаях, упомянутых выше, будет приниматься известное
        значение; в остальных случаях значением триггера будет
        `Неизвестно`.
-   Если вычисление триггера по поддерживаемому элементу данных приведет
    к ошибке, значением функции будет `Неизвестно` и оно будет частью
    дальнейшего вычисления выражения.

Обратите внимение на то, что неизвестные значения могут "исчезать"
только в логических выражениях описанных выше. В арифметических
выражениях неизвестные `Неизвестному` результату (за исключением деления
на 0).

Если выражение триггера с несколькими неподдерживаемыми элементами
данных вычисляется как `Неизвестное`, сообщение об ошибке в
веб-интерфейсе ссылается на последний вычисленный неподдерживаемый
элемент данных.

[comment]: # ({/new-adfc6b9d})






[comment]: # ({new-c2b4949f})
##### Example 2

Free disk space is too low.

Problem expression: it is less than 10GB for last 5 minutes

    max(/server/vfs.fs.size[/,free],5m)<10G

Recovery expression: it is more than 40GB for last 10 minutes

    min(/server/vfs.fs.size[/,free],10m)>40G

[comment]: # ({/new-c2b4949f})

[comment]: # ({new-60fad4fc})
#### Expressions with unsupported items and unknown values

Versions before Zabbix 3.2 are very strict about unsupported items in a
trigger expression. Any unsupported item in the expression immediately
renders trigger value to `Unknown`.

Since Zabbix 3.2 there is a more flexible approach to unsupported items
by admitting unknown values into expression evaluation:

-   For the nodata() function, the values are not affected by whether an
    item is supported or unsupported. The function is evaluated even if
    it refers to an unsupported item.
-   Logical expressions with OR and AND can be evaluated to known values
    in two cases regardless of unknown operands:
    -   "1 `or` Unsuported\_item1.some\_function() `or`
        Unsuported\_item2.some\_function() `or` ..." can be evaluated to
        '1' (True),
    -   "0 `and` Unsuported\_item1.some\_function() `and`
        Unsuported\_item2.some\_function() `and` ..." can be evaluated
        to '0' (False).\
        Zabbix tries to evaluate logical expressions taking unsupported
        items as `Unknown` values. In the two cases mentioned above a
        known value will be produced; in other cases trigger value will
        be `Unknown`.
-   If a function evaluation for supported item results in error, the
    function value is `Unknown` and it takes part in further expression
    evaluation.

Note that unknown values may "disappear" only in logical expressions as
described above. In arithmetic expressions unknown values always lead to
result `Unknown` (except division by 0).

If a trigger expression with several unsupported items evaluates to
`Unknown` the error message in the frontend refers to the last
unsupported item evaluated.

[comment]: # ({/new-60fad4fc})

[comment]: # ({new-6906462b})
##### Example 15

Comparing two string values - operands are:

-   a function that returns a string
-   a combination of macros and strings

Problem: detect changes in the DNS query

The item key is:

    net.dns.record[8.8.8.8,{$WEBSITE_NAME},{$DNS_RESOURCE_RECORD_TYPE},2,1]

with macros defined as

    {$WEBSITE_NAME} = example.com
    {$DNS_RESOURCE_RECORD_TYPE} = MX

and normally returns:

    example.com           MX       0 mail.example.com

So our trigger expression to detect if the DNS query result deviated
from the expected result is:

    last(/Zabbix server/net.dns.record[8.8.8.8,{$WEBSITE_NAME},{$DNS_RESOURCE_RECORD_TYPE},2,1])<>"{$WEBSITE_NAME}           {$DNS_RESOURCE_RECORD_TYPE}       0 mail.{$WEBSITE_NAME}"

Notice the quotes around the second operand.

[comment]: # ({/new-6906462b})

[comment]: # ({new-3f1b1c81})
##### Example 16

Comparing two string values - operands are:

-   a function that returns a string
-   a string constant with special characters \\ and "

Problem: detect if the `/tmp/hello` file content is equal to:

    \" //hello ?\"

Option 1) write the string directly

    last(/Zabbix server/vfs.file.contents[/tmp/hello])="\\\" //hello ?\\\""

Notice how \\ and " characters are escaped when the string gets compared
directly.

Option 2) use a macro

    {$HELLO_MACRO} = \" //hello ?\"

in the expression:

    last(/Zabbix server/vfs.file.contents[/tmp/hello])={$HELLO_MACRO}

[comment]: # ({/new-3f1b1c81})

[comment]: # ({new-942a7e0e})
##### Example 17

Comparing long-term periods.

Problem: Load of Exchange server increased by more than 10% last month

    trendavg(/Exchange/system.cpu.load,1M:now/M)>1.1*trendavg(/Exchange/system.cpu.load,1M:now/M-1M)

You may also use the [Event
name](/manual/config/triggers/trigger#configuration) field in trigger
configuration to build a meaningful alert message, for example to
receive something like

`"Load of Exchange server increased by 24% in July (0.69) comparing to June (0.56)"`

the event name must be defined as:

    Load of {HOST.HOST} server increased by {{?100*trendavg(//system.cpu.load,1M:now/M)/trendavg(//system.cpu.load,1M:now/M-1M)}.fmtnum(0)}% in {{TIME}.fmttime(%B,-1M)} ({{?trendavg(//system.cpu.load,1M:now/M)}.fmtnum(2)}) comparing to {{TIME}.fmttime(%B,-2M)} ({{?trendavg(//system.cpu.load,1M:now/M-1M)}.fmtnum(2)})

It is also useful to allow manual closing in trigger configuration for
this kind of problem.

[comment]: # ({/new-942a7e0e})

[comment]: # ({new-bddf0717})
#### Hysteresis

Sometimes an interval is needed between problem and recovery states,
rather than a simple threshold. For example, if we want to define a
trigger that reports a problem when server room temperature goes above
20°C and we want it to stay in the problem state until the temperature
drops below 15°C, a simple trigger threshold at 20°C will not be enough.

Instead, we need to define a trigger expression for the problem event
first (temperature above 20°C). Then we need to define an additional
recovery condition (temperature below 15°C). This is done by defining an
additional *Recovery expression* parameter when
[defining](/manual/config/triggers/trigger) a trigger.

In this case, problem recovery will take place in two steps:

-   First, the problem expression (temperature above 20°C) will have to
    evaluate to FALSE
-   Second, the recovery expression (temperature below 15°C) will have
    to evaluate to TRUE

The recovery expression will be evaluated only when the problem event is
resolved first.

::: notewarning
The recovery expression being TRUE alone does not
resolve a problem if the problem expression is still TRUE!
:::

[comment]: # ({/new-bddf0717})

[comment]: # ({new-6fc2eb1c})
##### Example 1

Temperature in server room is too high.

Problem expression:

    last(/server/temp)>20

Recovery expression:

    last(/server/temp)<=15

[comment]: # ({/new-6fc2eb1c})

[comment]: # ({note-contribute})

::: note-contribute
Располагаете примером, который может пригодиться другим? Используйте [Форму предложения примера](#report-example), чтобы отправить пример разработчикам Zabbix.
:::

[comment]: # ({/note-contribute})
