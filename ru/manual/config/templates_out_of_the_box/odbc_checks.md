[comment]: # translation:outdated

[comment]: # ({new-69d56449})
# Настройка шаблонов ODBC

Шаги для обеспечения правильной работы шаблонов, которые собирают
метрики через [мониторинг
ODBC](/ru/manual/config/items/itemtypes/odbc_checks):

1\. Убедитесь, что необходимый драйвер ODBC установлен на Zabbix сервере
или прокси.\
2. [Привяжите](/ru/manual/config/templates/linking#linking_a_template)
шаблон на целевой узел сети (если шаблон недоступен в вашей установке
Zabbix, вам может потребоваться сначала импортировать файл .xml шаблона
- см. инструкции в разделе [Готовые
шаблоны](/ru/manual/config/templates_out_of_the_box)).\
3. При необходимости измените значения обязательных макросов.\
4. Настройте объект для мониторинга так, чтобы разрешить обмен данными с
Zabbix - см. инструкции в столбце *Дополнительные шаги/комментарии*.

::: notetip
 Эта страница содержит только минимальный набор макросов
и шагов настройки, необходимых для правильной работы шаблона. Подробное
описание шаблона, включая полный список макросов, элементов и триггеров,
доступно в файле Readme.md шаблона (доступном при нажатии на имя
шаблона). 
:::

|Шаблон|Обязательные макросы|Дополнительные шаги/комментарии|
|------------|---------------------------------------|------------------------------------------------------------|
|[Template DB MSSQL by ODBC](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/db/mssql_odbc/README.md)|**{$MSSQL.DSN}** - имя системного источника данных (по умолчанию: <Укажите свой DSN>)<br>**{$MSSQL.PORT}** - TCP-порт Microsoft SQL Server (по умолчанию:1433)<br>**{$MSSQL.USER}, {$MSSQL.PASSWORD}** - учетные данные Microsoft SQL (по умолчанию: не заданы)|Создайте пользователя Microsoft SQL для мониторинга и предоставьте ему следующие разрешения: View Server State (Просмотр состояния сервера); View Any Definition (Просмотр любого определения); см. подробнее в документации [Microsoft SQL](https://docs.microsoft.com/en-us/sql/relational-databases/security/authentication-access/grant-a-permission-to-a-principal?view=sql -server-ver15)).<br><br>Элемент данных "Service's TCP port state" использует макросы {HOST.CONN} и {$MSSQL.PORT}, чтобы проверить доступность Microsoft SQL.|
|[Template DB MySQL by ODBC](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/db/mysql_odbc/README.md)|**{$MYSQL.DSN}** - имя системного источника данных (по умолчанию: <Укажите свой DSN>)<br>**{$MYSQL.USER}, {$MYSQL.PASSWORD}** - учетные данные MySQL; пароль может быть пустым (по умолчанию: не заданы)|Чтобы предоставить пользователю MySQL, который будет использоваться для мониторинга, необходимые привилегии, выполните:<br>`GRANT USAGE,REPLICATION CLIENT,PROCESS,SHOW DATABASES,SHOW VIEW ON %% *.* TO '<username>'@'%';%%`<br><br>см. подробнее в документации [MYSQL](https://dev.mysql.com/doc/refman/8.0/en/grant.html).|
|[Template DB Oracle by ODBC](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/db/oracle_odbc/README.md)|**{$ORACLE.DSN}** - имя системного источника данных (по умолчанию: <Укажите свой DSN>)<br>**{$ORACLE.PORT}** - TCP-порт Oracle DB (по умолчанию: 1521)<br>**{$ORACLE.USER}, {$ORACLE.PASSWORD}** - учетные данные Oracle (по умолчанию: не заданы)|1\. Чтобы создать пользователя Oracle для мониторинга, выполните:<br>`CREATE USER zabbix_mon IDENTIFIED BY <PASSWORD>;`<br>`-- Grant access to the zabbix_mon user.`<br>`GRANT CONNECT, CREATE SESSION TO zabbix_mon;`<br>`GRANT SELECT ON V_$instance TO zabbix_mon;`<br>`GRANT SELECT ON V_$database TO zabbix_mon;`<br>`GRANT SELECT ON v_$sysmetric TO zabbix_mon;`<br>`GRANT SELECT ON v$recovery_file_dest TO zabbix_mon;`<br>`GRANT SELECT ON v$active_session_history TO zabbix_mon;`<br>`GRANT SELECT ON v$osstat TO zabbix_mon;`<br>`GRANT SELECT ON v$restore_point TO zabbix_mon;`<br>`GRANT SELECT ON v$process TO zabbix_mon;`<br>`GRANT SELECT ON v$datafile TO zabbix_mon;`<br>`GRANT SELECT ON v$pgastat TO zabbix_mon;`<br>`GRANT SELECT ON v$sgastat TO zabbix_mon;`<br>`GRANT SELECT ON v$log TO zabbix_mon;`<br>`GRANT SELECT ON v$archive_dest TO zabbix_mon;`<br>`GRANT SELECT ON v$asm_diskgroup TO zabbix_mon;`<br>`GRANT SELECT ON sys.dba_data_files TO zabbix_mon;`<br>`GRANT SELECT ON DBA_TABLESPACES TO zabbix_mon;`<br>`GRANT SELECT ON DBA_TABLESPACE_USAGE_METRICS TO zabbix_mon;`<br>`GRANT SELECT ON DBA_USERS TO zabbix_mon;`<br><br>2. Убедитесь, что ODBC подключается к Oracle с параметром сессии `NLS_NUMERIC_CHARACTERS= '.,'`<br><br>3. Добавьте новую запись в odbc.ini:<br>`[$ORACLE.DSN]`<br>`Driver = Oracle 19 ODBC driver`<br>`Servername = $ORACLE.DSN`<br>`DSN = $ORACLE.DSN`<br><br>4. Проверьте соединение через isql:<br>`isql $TNS_NAME $DB_USER $DB_PASSWORD`<br><br>5.Настройте Zabbix сервер или Zabbix прокси для использования Oracle ENV. Отредактируйте или добавьте новый файл: */etc/sysconfig/zabbix-server*, или для прокси: */etc/sysconfig/zabbix-proxy*. Затем добавьте в файл следующие строки:<br>`export ORACLE_HOME=/usr/lib/oracle/19.6/client64`<br>`export PATH=$PATH:$ORACLE_HOME/bin`<br>`export LD_LIBRARY_PATH=$ORACLE_HOME/lib:/usr/lib64:/usr/lib:$ORACLE_HOME/bin`<br>`export TNS_ADMIN=$ORACLE_HOME/network/admin`<br><br>6. Перезапустите Zabbix сервер или прокси.|

[comment]: # ({/new-69d56449})
