[comment]: # translation:outdated

[comment]: # ({new-ebdbb780})
# Настройка шаблонов HTTP

Шаги для обеспечения правильной работы шаблонов, которые собирают
метрики через [HTTP агент](/ru/manual/config/items/itemtypes/http):

1\. Создайте хост в Zabbix и укажите IP-адрес или DNS-имя цели
мониторинга в качестве основного интерфейса. Это необходимо для
правильного разрешения макроса {HOST.CONN} в элементах шаблона.\
2. [Привяжите](/ru/manual/config/templates/linking#linking_a_template)
шаблон к узлу сети, созданному в предыдущем шаге (если шаблон недоступен
в вашей установке Zabbix, вам может потребоваться сначала импортировать
файл .xml шаблона - см. инструкции в разделе [Готовые
шаблоны](/ru/manual/config/templates_out_of_the_box)).\
3. При необходимости измените значения обязательных макросов.\
4. Настройте объект для мониторинга так, чтобы разрешить обмен данными с
Zabbix - см. инструкции в столбце *Дополнительные шаги/комментарии*.

::: notetip
 Эта страница содержит только минимальный набор макросов
и шагов настройки, необходимых для правильной работы шаблона. Подробное
описание шаблона, включая полный список макросов, элементов и триггеров,
доступно в файле Readme.md шаблона (доступном при нажатии на имя
шаблона). 
:::

|Шаблон|Обязательные макросы|Дополнительные шаги/комментарии|
|------------|---------------------------------------|------------------------------------------------------------|
|[Template App Apache by HTTP](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/app/apache_http)|**{$APACHE.STATUS.HOST}** - имя хоста или IP-адрес страницы статуса Apache (по учолчанию: 127.0.0.1)<br>**{$APACHE.STATUS.PATH}** - путь URL (по умолчанию: server-status?auto)<br>**{$APACHE.STATUS.PORT}** - порт страницы статуса Apache (по умолчанию: 80)<br>**{$APACHE.STATUS.SCHEME}** - схема запроса. Поддерживается: http (по умолчанию), https|Должен быть установлен модуль Apache mod\_status (см. подробнее в документации [Apache](https://httpd.apache.org/docs/current/mod/mod_status.html)).<br>Чтобы проверить доступность, выполните:<br>`httpd -M 2>/dev/null \| grep status_module`<br><br>Пример конфигурации Apache:<br>`<Location "/server-status">`<br>`SetHandler server-status`<br>`Require host example.com`<br>`</Location>`|
|[Template App Elasticsearch Cluster by HTTP](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/app/elasticsearch_http/README.md)|**{$ELASTICSEARCH.PORT}** - порт узла сети Elasticsearch (по умолчанию: 9200)<br>**{$ELASTICSEARCH.SCHEME}** - схема запроса. Поддерживаемые: http (по умолчанию), https<br>**{$ELASTICSEARCH.USERNAME}**, **{$ELASTICSEARCH.PASSWORD}** - учетные данные для входа, требуются, только если они используются для аутентификации Elasticsearch.|\-|
|[Template App Etcd by HTTP](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/app/etcd_http/README.md)|**{$ETCD.PORT}**- порт, используемый конечной точкой API Etcd (по умолчанию: 2379)<br>**{$ETCD.SCHEME}** - схема запроса. Поддерживается: http (по умолчанию), https<br>**{$ETCD.USER}**, **{$ETCD.PASSWORD}** - учетные данные для входа, обязательны только если используются для аутентификации Etcd|Метрики собираются из конечной точки /metrics; для указания местоположения конечной точки используйте опцию `--listen-metrics-urls` (см. подробнее в документации [Etcd](https://github.com/etcd-io/website/blob/master/content/docs/v3.4.0/op-guide/configuration.md#--listen-metrics-urls)).<br><br>Чтобы проверить, настроен ли Etcd для разрешения сбора метрик, выполните:<br>`curl -L http://localhost:2379/metrics`<br><br>Чтобы проверить, доступен ли Etcd с прокси Zabbix или с сервера Zabbix, запустите:<br>`curl -L http:%%//<etcd_node_adress>:2379/metrics%%`<br><br>Шаблон должен быть добавлен к каждому узлу с Etcd.|
|[Template App HAProxy by HTTP](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/app/haproxy_http/README.md)|**{$HAPROXY.STATS.PATH}** - путь к странице HAProxy Stats (по умолчанию: stats)<br>**{$HAPROXY.STATS.PORT}** - порт узла сети или контейнера HAProxy Stats (по умолчанию: 8404)<br>**{$HAPROXY.STATS.SCHEME}** - схема запроса. Поддерживается: http (по умолчанию), https|страница HAProxy Stats должна быть настроена (см. подробнее о HAProxy Stats в блоге [HAProxy](https://www.haproxy.com/blog/exploring-the-haproxy-stats-page/); в Readme.md шаблона Zabbix есть готовый пример конфигурации).|
|[Template App Nginx by HTTP](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/app/nginx_http/README.md)|**{$NGINX.STUB\_STATUS.HOST}** - имя узла сети или IP-адрес узла сети или контейнера Nginx stub\_status (по умолчанию: localhost).<br>**{$NGINX.STUB\_STATUS.PATH}** - путь к странице Nginx stub\_status (по умолчанию: basic\_status).<br>**{$NGINX.STUB\_STATUS.PORT}** - порт хоста или контейнера Nginx stub\_status (по умолчанию: 80).<br>**{$NGINX.STUB\_STATUS.SCHEME}** - схема запроса. Поддерживается: http (по умолчанию), https|`'ngx_http_stub_status_module` должен быть создан (см. подробнее в документации [Nginx](https://nginx.org/en/docs/http/ngx_http_stub_status_module.html); в Readme.md шаблона Zabbix есть готовый пример конфигурации).<br>Чтобы проверить доступность, выполните:<br>`nginx -V 2>&1 \| grep -o with-http_stub_status_module`|
|[Template App RabbitMQ cluster by HTTP](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/app/rabbitmq_http/README.md)|**{$RABBITMQ.API.CLUSTER\_HOST}** - имя хоста или IP-адрес конечной точки API кластера RabbitMQ (по умолчанию: 127.0.0.1)<br>**{$RABBITMQ.API.SCHEME}** - схема запроса. Поддерживается: http (по умолчанию), https<br>**{$RABBITMQ.API.USER}**, **{$RABBITMQ.API.PASSWORD}** -учетные данные RabbitMQ (по умолчанию имя пользователя: zbx\_monitor, пароль: zabbix).|Включить плагин управления RabbitMQ (см. документацию [RabbitMQ](https://www.rabbitmq.com/management.html)).<br><br>Чтобы создать пользователя RabbitMQ с необходимыми разрешениями для мониторинга, выполните:<br>'' rabbitmqctl add\_user zbx\_monitor <PASSWORD> ''<br>`rabbitmqctl set_permissions -p / zbx_monitor %% "" "" ".*"%%`<br>`rabbitmqctl set_user_tags zbx_monitor monitoring`<br><br>Если кластер состоит из нескольких узлов, рекомендуется назначить шаблон мониторинга кластера отдельному балансирующему узлу сети. В случае мониторинга кластера с одним узлом, шаблон кластера можно назначить узлу сети с шаблоном узла.|
|[Template DB ClickHouse by HTTP](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/db/clickhouse_http/README.md)|**{$CLICKHOUSE.PORT}** - порт конечной точки HTTP ClickHouse (по умолчанию: 8123)<br>**{$CLICKHOUSE.SCHEME}** - схема запроса. Поддерживается: http (по умолчанию), https<br>**{$CLICKHOUSE.USER}**, **{$CLICKHOUSE.PASSWORD}** - учетные данные ClickHouse (по умолчанию имя ползователя: zabbix, пароль: zabbix\_pass).<br>Если вам не нужна аутентификация, удалите заголовки из элементов типа агента HTTP.|Создайте пользователя ClickHouse с "веб-профилем" и разрешением на просмотр баз данных (см. подробнее в документации [ClickHouse](https://clickhouse.tech/docs/en/operations/settings/settings-users/)).<br><br>См. файл шаблона *Readme.md* для готовой к использованию конфигурации файла zabbix.xml.|

[comment]: # ({/new-ebdbb780})
