[comment]: # translation:outdated

[comment]: # ({new-7a75c626})
# 1 Стандартизованные шаблоны для сетевых устройств

[comment]: # ({/new-7a75c626})

[comment]: # ({new-1dc52a1f})
#### Обзор

Чтобы предоставлять мониторинг сетевых устройств, таких как коммутаторы
и роутеры, мы создали две так называемые модели: для самого сетевого
устройства (в основном его шасси) и для сетевого интерфейса.

Начиная с Zabbix 3.4, поставляются шаблоны для многих семейств сетевых
устройств. Все шаблоны охватывают (где возможно получить эти элементы
данных с устройства):

-   Мониторинг ошибок шасси (источники питания, вентиляторы и
    температура, общее состояние)
-   Мониторинг производительности шасси (элементы данных CPU и памяти)
-   Сбор инвентарных данных шасси (серийные номера, имя модели, версия
    прошивки)
-   Мониторинг сетевых интерфейсов при помощи IF-MIB и EtherLike-MIB
    (состояние интерфейса, загрузка трафика на порту, состояние дуплекса
    для Ethernet)

Эти шаблоны доступны:

-   В *Настройка* → *Шаблоны* в новых инсталляциях;
-   В [официальном Zabbix репозитории
    шаблонов](https://www.zabbix.org/wiki/Zabbix_Templates/Official_Templates).
    Если вы обновляетесь с версии pre-3.4 Zabbix, вы можете
    импортировать эти шаблоны из XML.

Если вы импортируете новые готовые шаблоны, вы возможно захотите
обновить глобальное регулярное выражение
`@Network interfaces for discovery` на:

    Результат ЛОЖЬ: ^Software Loopback Interface
    Результат ЛОЖЬ: ^(In)?[lL]oop[bB]ack[0-9._]*$
    Результат ЛОЖЬ: ^NULL[0-9.]*$
    Результат ЛОЖЬ: ^[lL]o[0-9.]*$
    Результат ЛОЖЬ: ^[sS]ystem$
    Результат ЛОЖЬ: ^Nu[0-9.]*$

для фильтрации loopback и нулевых интерфейсов с большинства систем.

[comment]: # ({/new-1dc52a1f})

[comment]: # ({new-b5f67cef})
#### Устройства

Список семейств устройств, для которых доступны шаблоны:

|Имя шаблона|Производитель|Семейство устройств|Известные модели|ОС|Использованные MIB|**[Теги](/ru/manual/config/templates_out_of_the_box/network_devices#теги)**|
|---------------------|--------------------------|-------------------------------------|-------------------------------|----|--------------------------------|-----------------------------------------------------------------------------------|
|*Template Net Alcatel Timetra TiMOS SNMPv2*|Alcatel|Alcatel Timetra|ALCATEL SR 7750|TiMOS|TIMETRA-SYSTEM-MIB,TIMETRA-CHASSIS-MIB|Сертифицированный|
|*Template Net Brocade FC SNMPv2*|Brocade|Коммутаторы Brocade FC|Brocade 300 SAN Switch-|\-|SW-MIB,ENTITY-MIB|Производительность, Ошибки|
|*Template Net Brocade\_Foundry Stackable SNMPv2*|Brocade|Brocade ICX|Brocade ICX6610, Brocade ICX7250-48, Brocade ICX7450-48F|<|FOUNDRY-SN-AGENT-MIB, FOUNDRY-SN-STACKING-MIB|Сертифицированный|
|*Template Net Brocade\_Foundry Nonstackable SNMPv2*|Brocade, Foundry|Brocade MLX, Foundry|Brocade MLXe, Foundry FLS648, Foundry FWSX424|<|FOUNDRY-SN-AGENT-MIB|Сертифицированный, Ошибки|
|*Template Net Cisco IOS SNMPv2*|Cisco|Cisco IOS версии > 12.2 3.5|Cisco C2950|IOS|CISCO-PROCESS-MIB,CISCO-MEMORY-POOL-MIB,CISCO-ENVMON-MIB|Сертифицированный|
|*Template Net Cisco releases later than 12.0\_3\_T and prior to 12.2\_3.5\_ SNMPv2*|Cisco|Cisco IOS > 12.0 3 T и < 12.2 3.5|\-|IOS|CISCO-PROCESS-MIB,CISCO-MEMORY-POOL-MIB,CISCO-ENVMON-MIB|Сертифицированный|
|*Template Net Cisco releases prior to 12.0\_3\_T SNMPv2*|Cisco|Cisco IOS < 12.0 3 T|\-|IOS|OLD-CISCO-CPU-MIB,CISCO-MEMORY-POOL-MIB|Сертифицированный|
|*Template Net D-Link DES\_DGS Switch SNMPv2*|D-Link|Коммутаторы DES/DGX|D-Link DES-xxxx/DGS-xxxx,DLINK DGS-3420-26SC|\-|DLINK-AGENT-MIB,EQUIPMENT-MIB,ENTITY-MIB|Сертифицированный|
|*Template Net D-Link DES 7200 SNMPv2*|D-Link|DES-7xxx|D-Link DES 7206|\-|ENTITY-MIB,MY-SYSTEM-MIB,MY-PROCESS-MIB,MY-MEMORY-MIB|Производительность Ошибки Интерфейсы|
|*Template Net Dell Force S-Series SNMPv2*|Dell|Dell Force S-Series|S4810|<|F10-S-SERIES-CHASSIS-MIB|Сертифицированный|
|*Template Net Extreme Exos SNMPv2*|Extreme|Extreme EXOS|X670V-48x|EXOS|EXTREME-SYSTEM-MIB,EXTREME-SOFTWARE-MONITOR-MIB|Сертифицированный|
|*Template Net Huawei VRP SNMPv2*|Huawei|Huawei VRP|S2352P-EI|\-|ENTITY-MIB,HUAWEI-ENTITY-EXTENT-MIB|Сертифицированный|
|*Template Net Intel\_Qlogic Infiniband SNMPv2*|Intel/QLogic|Устройства Intel/QLogic Infiniband|Infiniband 12300|<|ICS-CHASSIS-MIB|Ошибки Инвентарь|
|*Template Net Juniper SNMPv2*|Juniper|Модели MX,SRX,EX|Juniper MX240, Juniper EX4200-24F|JunOS|JUNIPER-MIB|Сертифицированный|
|*Template Net Mellanox SNMPv2*|Mellanox|Устройства Mellanox Infiniband|SX1036|MLNX-OS|HOST-RESOURCES-MIB,ENTITY-MIB,ENTITY-SENSOR-MIB,MELLANOX-MIB|Сертифицированный|
|*Template Net Mikrotik SNMPv2*|Mikrotik|Mikrotik RouterOS devices|Mikrotik CCR1016-12G, Mikrotik RB2011UAS-2HnD, Mikrotik 912UAG-5HPnD, Mikrotik 941-2nD, Mikrotik 951G-2HnD, Mikrotik 1100AHx2|RouterOS|MIKROTIK-MIB,HOST-RESOURCES-MIB|Сертифицированный|
|*Template Net QTech QSW SNMPv2*|QTech|Устройства Qtech|Qtech QSW-2800-28T|\-|QTECH-MIB,ENTITY-MIB|Производительность Инвентарь|
|*Template Net Ubiquiti AirOS SNMPv1*|Ubiquiti|Беспроводные устройства Ubiquiti AirOS|NanoBridge,NanoStation,Unifi|AirOS|FROGFOOT-RESOURCES-MIB,IEEE802dot11-MIB|Производительность|
|*Template Net HP Comware HH3C SNMPv2*|HP|HP (H3C) Comware|HP A5500-24G-4SFP HI Switch|<|HH3C-ENTITY-EXT-MIB,ENTITY-MIB|Сертифицированный|
|*Template Net HP Enterprise Switch SNMPv2*|HP|HP Enterprise Switch|HP ProCurve J4900B Switch 2626, HP J9728A 2920-48G Switch|<|STATISTICS-MIB,NETSWITCH-MIB,HP-ICF-CHASSIS,ENTITY-MIB,SEMI-MIB|Сертифицированный|
|*Template Net TP-LINK SNMPv2*|TP-LINK|TP-LINK|T2600G-28TS v2.0|<|TPLINK-SYSMONITOR-MIB,TPLINK-SYSINFO-MIB|Производительность Инвентарь|
|*Template Net Netgear Fastpath SNMPv2*|Netgear|Netgear Fastpath|M5300-28G|<|FASTPATH-SWITCHING-MIB,FASTPATH-BOXSERVICES-PRIVATE-MIB|Ошибки Инвентарь|

[comment]: # ({/new-b5f67cef})

[comment]: # ({new-07c98c5a})
#### Дизайн шаблонов

Шаблоны разработаны с учетом следующего:

-   Пользовательские макросы используются где только возможно, поэтому
    пользователь может отрегулировать триггеры
-   Низкоуровневые обнаружения используются где только возможно, чтобы
    минимизировать количество неподдерживаемых элементов данных
-   Шаблоны поставляются для SNMPv2. SNMPv1 используется, если известно
    что большинство устройств не поддерживают SNMPv2.
-   Все шаблоны зависят от Template ICMP Ping, таким образом все
    устройства так же проверяются по протоколу ICMP
-   Элементы данных не используют MIB - в элементах данных и правилах
    обнаружения используются только SNMP OID. Поэтому для работы
    шаблонов нет необходимости загружать какие-либо MIB в Zabbix.
-   Сетевые интерфейсы loopback фильтруются при обнаружения, также как и
    интерфейсы с ifAdminStatus = down(2)
-   Используются 64-битные счетчики из IF-MIB::ifXTable, где это
    возможно. Если они не поддерживаются, вместо них используются
    32-битные счетчики по умолчанию.
-   У всех обнаруженных сетевых интерфейсов имеется триггер, который
    наблюдает за его рабочим состоянием (канал связи).
    -   Если вы не хотите контроллировать это состояние по какому-либо
        конкретному интерфейсу, создайте контекст пользовательского
        макроса со значением 0. Например:

![](../../../../assets/en/manual/config/template_ifcontrol.png)

где Gi0/0 является {\#IFNAME}. Таким образом, триггер больше не будет
использоваться для этого конкретного интерфейса.

      * Вы также можете изменить поведение по умолчанию по всем триггерам (не срабатывать) и активировать только триггер только ограниченному количеству интерфейсов, например приходящим каналам связи

![](../../../../assets/en/manual/config/template_ifcontrol2.png)

[comment]: # ({/new-07c98c5a})

[comment]: # ({new-6d84c190})
#### Теги

-   Прозводительность – MIB'ы семейства устройств дают возможность
    мониторинга элементов данных CPU и памяти;
-   Ошибки - MIB'ы семейства устройств дают возможность мониторинга по
    крайней мере одного сенсора температуры;
-   Инвентарь – MIB'ы семейства устройств дают возможность сбора по
    крайней мене серийного номера устройства и его наименование модели;
-   Сертифицированный – охватываются все три основные категории выше.

[comment]: # ({/new-6d84c190})
