[comment]: # translation:outdated

[comment]: # ({new-7f0ac7a3})
# Настройка шаблонов Zabbix агента 2

Шаги по обеспечению корректной работы шаблонов, собирающих метрики с
[Zabbix агент 2](/manual/concepts/agent2):

1\. Убедитесь, что на хосте установлен агент 2 и что установленная
версия содержит необходимый плагин.\
2. [Привяжите](/ru/manual/config/templates/linking#linking_a_template)
шаблон к целевому узлу сети (если шаблон недоступен в вашей установке
Zabbix, вам может потребоваться сначала импортировать файл .xml шаблона
- см. инструкции в разделе [Готовые
шаблоны](/ru/manual/config/templates_out_of_the_box)).\
3. При необходимости измените значения обязательных макросов. Обратите
внимание, что пользовательские макросы могут использоваться для
переопределения параметров конфигурации.\
4. Настройте объект для мониторинга так, чтобы разрешить обмен данными с
Zabbix - см. инструкции в столбце *Дополнительные шаги/комментарии*.

::: noteclassic
 Шаблоны Zabbix агента 2 работают вместе с плагинами. В то
время как базовая конфигурация может быть выполнена путем простой
настройки пользовательских макросов, более глубокая настройка может быть
достигнута путем [настройки
плагина](/ru/manual/config/items/plugins#именованные_сессии). Например,
если плагин поддерживает именованные сессии, можно отслеживать несколько
объектов одного типа (например, MySQL1 и MySQL2), указав для каждого
именованную сессию с собственным URI, именем пользователя и паролем в
файле конфигурации. 
:::

::: notetip
 Эта страница содержит только минимальный набор макросов
и шагов настройки, необходимых для правильной работы шаблона. Подробное
описание шаблона, включая полный список макросов, элементов и триггеров,
доступно в файле Readme.md шаблона (доступном при нажатии на имя
шаблона). 
:::

|Имя шаблона|Обязательные макросы|Дополнительные шаги/комментарии|
|---------------------|---------------------------------------|------------------------------------------------------------|
|[Template App Docker](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/app/docker/README.md)|\-|Работает с плагином *Docker*; именованные сессии не поддерживаются.<br><br>Чтобы указать путь к конечной точке Docker API, отредактируйте параметр Plugins.Docker.Endpoint в [файле конфигурации](/ru/manual/appendix/config/zabbix_agent2) агента 2. (по умолчанию: `Plugins.Docker.Endpoint=unix:///var/run/docker.sock`).<br><br>Чтобы проверить доступность, запустите:<br>`zabbix_get -s docker-host -k docker.info`|
|[Template App Memcached](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/app/memcached/README.md)|**{$MEMCACHED.CONN.URI}** - строка подключения в формате URI; порт не является обязательным; пароль не используется.<br>Если не установлен, используется значение плагина по умолчанию: tcp://localhost:11211.<br>Примеры: tcp://127.0.0.1:11211, tcp://localhost, unix:/var/run/memcached.sock.|Работает с плагином *Memcached*; именованные сессии поддерживаются.<br><br>Чтобы проверить доступность, запустите:<br>`zabbix_get -s memcached-host -k memcached.ping`|
|[Template DB MySQL by Zabbix agent 2](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/db/mysql_agent2/README.md)|**{$MYSQL.DSN}** - имя системного источника данных экземпляра MySQL (по умолчанию: <Укажите свой DSN>).<br>Может быть именем сеанса или URI, определенным в следующем формате: <протокол(узел сети:порт или /путь/к/сокету)/><br>Для URI поддерживаются только схемы TCP и Unix.<br>Примеры: MySQL1, tcp://localhost:3306, tcp://172.16.0.10, unix:/var/run/mysql.sock<br>**{$MYSQL.USER}**, **{$MYSQL.PASSWORD}** - учетные данные MySQL (по умолчанию: не заданы). Обязательно, если в {$MYSQL.DSN} указан URI.<br>Должны быть пустыми, если в {$MYSQL.DSN} указано название именованной сессии.|Работает с плагином *MySQL*; именованные сессии поддерживаются.<br><br>Чтобы предоставить пользователю MySQL необходимые привилегии, которые будут использоваться для мониторинга, запустите:<br>`GRANT USAGE,REPLICATION CLIENT,PROCESS,SHOW DATABASES,SHOW VIEW ON *.* TO '<username>'@'%';`<br><br>См. документацию MySQL для получения информации о [привилегиях пользователя](https://dev.mysql.com/doc/refman/8.0/en/grant.html) и [сокетах Unix](https://dev.mysql.com/doc/refman/8.0/en/problems-with-mysql-sock.html).|
|[Template DB PostgreSQL by Zabbix agent 2](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/db/postgresql_agent2)|**{$PG.URI}** - строка подключения; может быть именем сеанса или URI, определенным в следующем формате: <протокол(узел сети:порт или /путь/к/сокету)/>. Для URI поддерживаются только схемы TCP и Unix.<br>Примеры: Postgres1, tcp://localhost:5432, tcp://172.16.0.10<br>**{$PG.USER}**, **{$PG.PASSWORD}** - учетные данные PostgreSQL (по умолчанию имя пользователя: postgres, пароль:postgres).<br>Обязательно, если {$PG.URI} - это URI. Должен быть пустым, если {$PG.URI} - это название именованной сессии.|Работает с плагином *PostgreSQL*; именованные сессии поддерживаются.<br><br>Чтобы создать пользователя с необходимыми привилегиями, для PostgreSQL 10 и новее выполните:<br>`CREATE USER 'zbx_monitor' IDENTIFIED BY '<password>';`<br>`GRANT EXECUTE ON FUNCTION pg_catalog.pg_ls_dir(text) TO zbx_monitor;`\\\\`GRANT EXECUTE ON FUNCTION pg_catalog.pg_stat_file(text) TO zbx_monitor;`<br><br>Отредактируйте pg\_hba.conf, чтобы разрешить соединения от Zabbix агента (подробности см. в [документации PostgreSQL](https://www.postgresql.org/docs/current/auth-pg-hba-conf.html)).|
|[Template DB Redis](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/db/redis/README.md)|**{$REDIS.CONN.URI}** -строка подключения в формате URI; порт не является обязательным; пароль не используется.<br>Если не установлен, используется значение плагина по умолчанию: tcp://localhost:6379|Работает с плагином *Redis*; именованные сессии поддерживаются.<br><br>Чтобы проверить доступность, выполните:<br>`zabbix_get -s redis-master -k redis.ping`|

[comment]: # ({/new-7f0ac7a3})
