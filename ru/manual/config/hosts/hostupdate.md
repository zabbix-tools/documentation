[comment]: # translation:outdated

[comment]: # ({new-1c6301ba})
# -\#3 Массовое обновление

[comment]: # ({/new-1c6301ba})

[comment]: # ({new-7205743f})
#### Обзор

Порой вы можете захотеть изменить какие-нибудь атрибуты для нескольких
узлов сети одновременно. Вместо открытия каждого узла сети для
редактирования, вы можете воспользоваться функцией массового обновления.

[comment]: # ({/new-7205743f})

[comment]: # ({new-929367ac})
#### Использование массового обновления

Для массового обновления каких-либо узлов сети, выполните следующее:

-   Отметьте узлы сети из списка, которые вы хотите обновить, из списка
    [узлов
    сети](/ru/manual/web_interface/frontend_sections/configuration/hosts)
-   Нажмите на *Массовое обновление* ниже списка
-   Перейдите в необходимую вкладку атрибутов (*Узел сети*, *Шаблоны*,
    *IPMI*, *Инвентаризация* или *Шифрование*)
-   Отметьте атрибуты для обновления и введите для них новые значения

![host\_mass\_update.png](../../../../assets/en/manual/config/host_mass_update.png)

*Заменить группы узла сети* уберет узел сети из текущих групп и заменит
на указанную(ые) в данном поле.

*Добавить новые или существующие группы узлов сети* позволяет выбрать
дополнительные группы узла сети из существующих или назначить совершенно
новые группы узлов сети для выбранных узлов сети.

*Удалить группы узлов сети* позволяет удалить выбранные группы узлов
сети с узлов сети. В случае, когда узлы сети уже находятся в выбранных
группах, тогда узлы сети будут удалены из этих групп. В случае, когда
узлы сети не находятся в выбранных группах, тогда ничего не будет
добавлено или удалено. В случае, когда одни и те же группы узлов сети
заменяются и удаляются одновременно, узлы сети фактически остаются без
групп.

Эти поля имеют функцию авто-дополнения - начните набирать и в них
появится выпадающий список совпадающих групп узлов сети. Если группа
узла сети новая, она также появиться в выпадающем списке и будет
отмечена в конце строки как *(новая)*. Просто прокрутите вниз для
выбора.

![host\_mass\_update2.png](../../../../assets/en/manual/config/host_mass_update2.png){width="600"}

Для обновления присоединенных шаблонов на вкладке **Шаблоны**, отметьте
*Присоединить шаблоны* и начните вводить имя шаблона в поле с
автодополнением до появления выпадающего списка предлагающего
совпадающие доступные шаблоны. Просто используйте прокрутку для выбора
присоединяемого шаблона.

Опция *Заменить* позволяет добавить новый шаблон к узлу сети взамен
любых других шаблонов соединенных с узлам сети ранее. Опция *Очистить
при отсоединении* позволяет не только отсоединить какие-либо ранее
присоединенные шаблоны, но также удалить все элементы унаследованные от
них (элементы данных, триггеры, и т.д.).

![host\_mass\_update3.png](../../../../assets/en/manual/config/host_mass_update3.png){width="500"}

![host\_mass\_update4.png](../../../../assets/en/manual/config/host_mass_update4.png){width="500"}

Чтобы появилась возможность массового обновления полей инвентарных
данных, *Режим инвентарных данных* должен быть задан значениями
'Вручную' или 'Автоматически'.

![](../../../../assets/en/manual/config/host_mass_update5.png)

Когда сделаете все необходимые изменения, нажмите на кнопку *Обновить*.
Атрибуты будут обновлены соответственно всем выбранным узлам сети.

[comment]: # ({/new-929367ac})
