[comment]: # translation:outdated

[comment]: # ({new-ba72dbf1})
# 2 Права доступа

[comment]: # ({/new-ba72dbf1})

[comment]: # ({new-a9157075})
#### Обзор

Вы можете разграничить права доступа пользователей в Zabbix, задав
соответствующий тип пользователя и затем включив непривилегированных
пользователей в группы пользователей, которые имеют доступ к данным
группы узлов сети.

[comment]: # ({/new-a9157075})

[comment]: # ({new-ac897fff})

#### User types

Permissions in Zabbix depend, primarily, on the user type:

   - *User* - has limited access rights to menu sections (see below) and no access to any resources by default. Any permissions to host or template groups must be explicitly assigned;
   - *Admin* - has incomplete access rights to menu sections (see below). The user has no access to any host groups by default. Any permissions to host or template groups must be explicitly given;
   - *Super admin* - has access to all menu sections. The user has a read-write access to all host and template groups. Permissions cannot be revoked by denying access to specific groups.

**Menu access**

The following table illustrates access to Zabbix menu sections per user type:

|Menu section|<|User|Admin|Super admin|
|-|-------------|------|------|------|
|**Dashboards**|<|+|+|+|
|**Monitoring**|<|+|+|+|
| |*Problems*|+|+|+|
|^|*Hosts*|+|+|+|
|^|*Latest data*|+|+|+|
|^|*Maps*|+|+|+|
|^|*Discovery*| |+|+|
|**Services**|<|+|+|+|
| |*Services*|+|+|+|
|^|*SLA*| |+|+|
|^|*SLA report*|+|+|+|
|**Inventory**|<|+|+|+|
| |*Overview*|+|+|+|
|^|*Hosts*|+|+|+|
|**Reports**|<|+|+|+|
| |*System information*| | |+|
|^|*Scheduled reports*| |+|+|
|^|*Availability report*|+|+|+|
|^|*Triggers top 100*|+|+|+|
|^|*Audit log*| | |+|
|^|*Action log*| | |+|
|^|*Notifications*| |+|+|
|**Data collection**|<| |+|+|
| |*Template groups*| |+|+|
|^|*Host groups*| |+|+|
|^|*Templates*| |+|+|
|^|*Hosts*| |+|+|
|^|*Maintenance*| |+|+|
|^|*Event correlation*| | |+|
|^|*Discovery*| |+|+|
|**Alerts**|<| |+|+|
| |*Trigger actions*| |+|+|
| |*Service actions*| |+|+|
| |*Discovery actions*| |+|+|
| |*Autoregistration actions*| |+|+|
| |*Internal actions*| |+|+|
|^|*Media types*| | |+|
|^|*Scripts*| | |+|
|**Users**|<| | |+|
| |*User groups*| | |+|
|^|*User roles*| | |+|
|^|*Users*| | |+|
|^|*API tokens*| | |+|
|^|*Authentication*| | |+|
|**Administration**|<| | |+|
| |*General*| | |+|
|^|*Audit log*| | |+|
|^|*Housekeeping*| | |+|
|^|*Proxies*| | |+|
|^|*Macros*| | |+|
|^|*Queue*| | |+|

[comment]: # ({/new-ac897fff})

[comment]: # ({new-a852da1b})
#### Тип пользователя

Тип пользователя определяет уровень прав к административным меню и
уровень прав к данным групп узлов сети.

|Тип пользователя|Описание|
|-------------------------------|----------------|
|*Zabbix Пользователь*|Пользователь имеет доступ к меню Мониторинг. По умолчанию пользователь не имеет прав доступа к каким-либо ресурсам. Права доступа на группу узлов сети должны быть заданы явно.|
|*Zabbix Администратор*|Пользователь имеет доступ в меню Мониторинг и в меню Настройка. По умолчанию пользователь не имеет прав доступа к каким-либо ресурсам. Права доступа на группу узлов сети должны быть заданы явно.|
|*Zabbix Супер-Администратор*|Пользователь имеет доступ ко всему: Мониторинг, Настройка и Администрирование. Пользователь имеет права Чтения-Записи ко всем группам узлов сети. Запрет доступа к каким-либо группам узлов сети не влияет на права доступа пользователя.|

[comment]: # ({/new-a852da1b})

[comment]: # ({new-f38cb50e})
#### Права доступа к группам узлов сети

Доступ к любым данным узлов сети в Zabbix гарантирован [группам
пользователей](/ru/manual/config/users_and_usergroups/usergroup) только
на уровне группы узлов сети.

Это означает, что отдельному пользователю не может быть напрямую
назначено разрешение на доступ к узлу сети (или к группе узлов сети).
Пользователь может получить доступ к узлу сети, только будучи частью
группы пользователей, которая имеет доступ к группе узлов сети, которая
содержит необходимый узел сети.

[comment]: # ({/new-f38cb50e})
