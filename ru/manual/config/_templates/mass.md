[comment]: # translation:outdated

[comment]: # ({new-bfcc3bc2})
# 4 Массовое обновление

[comment]: # ({/new-bfcc3bc2})

[comment]: # ({new-cb0c0890})
#### Обзор

Иногда вы можете захотеть изменить какой-либо атрибут для нескольких
шаблонов одновременно. Вместо того, чтобы открывать каждый отдельный
шаблон для редактирования, вы можете использовать для этого функцию
массового обновления.

[comment]: # ({/new-cb0c0890})

[comment]: # ({new-b2a07f91})
#### Использование массового обновления

Чтобы массово обновить некоторые шаблоны, сделайте следующее:

-   Установите флажки перед шаблонами, которые вы хотите обновить, в
    [списке
    шаблонов](/ru/manual/web_interface/frontend_sections/configuration/templates)
-   Нажмите на *Массовое обновление* под списком
-   Перейдите на вкладку с необходимыми атрибутами (*Шаблон*, *Связанные
    шаблоны* или *Теги*)
-   Установите флажки для атрибутов, которые вы хотите обновить, и
    введите для них новое значение

![](../../../../assets/en/manual/config/templ_mass_a1.png)

На вкладке **Шаблон** при выборе кнопки для обновления группы узлов сети
доступны следующие параметры:

-   *Добавить* - добавить шаблоны в другие группы узлов сети в
    дополнение к текущим группам (можно выбрать существующие группы из
    списка или создать новые группы узлов сети для шаблонов).
-   *Замена* - удалить шаблон из всех существующих групп узлов сети и
    заменить их на те, которые указаны в этом поле (существующие или
    новые группы узлов сети).
-   *Удалить* - удалить шаблоны из указанных групп узлов сети.

Эти поля заполняются автоматически - после начала ввода в них появляется
раскрывающийся список подходящих групп узлов сети. Если группа узлов
сети новая, она также отображается в раскрывающемся списке со знаком
*(новая)* после строки. Чтобы найти нужную группу, прокрутите список
вниз или напечатайте еще несколько символов, чтобы сократить число
подходящих групп.

![](../../../../assets/en/manual/config/templ_mass_b0.png)

На вкладке **Присоединенные шаблоны** при выборе соответствующей кнопки
для обновления связей шаблонов доступны следующие параметры:

-   *Связь* - присоединить дополнительные шаблоны.
-   *Замена* - отсоединить все привязанные шаблоны и присоединить новые,
    указанные в этой поле.
-   *Отсоединить* - отсоединить шаблоны, указанные в этом поле.

Чтобы указать шаблоны для связи/отсоединения, начните вводить имя
шаблона в поле автозаполнения, пока не появится раскрывающийся список,
предлагающий соответствующие шаблоны. Просто прокрутите вниз, чтобы
выбрать нужный шаблон.

Опция *Очистить при отсоединении* позволит не только отсоединить любые
ранее связанные шаблоны, но и удалить все унаследованные от них элементы
(элементы данных, триггеры и т. д.).
![](../../../../assets/en/manual/config/templ_mass_c1.png)

На вкладке *Теги* при выборе кнопки для обновления группы узлов сети
доступны следующие параметры:

-   *Добавить* - добавить новые теги в дополнение к текущим тегам.
-   *Замена* - удалить существующие теги и заменить их на те, которые
    указаны в этом поле.
-   *Удалить* - удалить указанные теги.

В тегах поддерживаются пользовательские макросы, макросы {INVENTORY.\*},
макросы {HOST.HOST}, {HOST.NAME}, {HOST.CONN}, {HOST.DNS}, {HOST.IP},
{HOST.PORT} и {HOST.ID}. Обратите внимание, что теги с одинаковыми
именами, но разными значениями не считаются «дубликатами» и могут быть
добавлены в один и тот же шаблон.

![](../../../../assets/en/manual/config/templ_mass_d.png)

При выборе соответствующей кнопки для обновления макросов доступны
следующие параметры:

-   *Добавить* - указать дополнительные пользовательские макросы для
    шаблонов. Если установлен флажок *Обновить существующее*, значение,
    тип и описание для указанного имени макроса будут обновлены. Если
    флажок не установлен, если макрос с таким именем уже существует в
    шаблоне (шаблонах), он не будет обновляться.
-   *Обновить* - заменить значения, типы и описания макросов, указанных
    в этом списке. Если установлен флажок *Добавить отсутствующее*,
    макрос, который ранее не существовал в шаблоне, будет добавлен как
    новый макрос. Если этот флажок не установлен, будут обновлены только
    макросы, которые уже существуют в шаблоне.
-   *Удалить* - удалить указанные макросы из шаблонов. Если установлен
    флажок *Исключая выбранные*, все макросы, кроме указанных в списке,
    будут удалены. Если флажок не установлен, будут удалены только
    макросы, указанные в списке.
-   *Удалить все* -удалить все пользовательские макросы из шаблонов.
    Если не установлен флажок *Я подтверждаю удаление всех макросов*,
    откроется новое всплывающее окно с просьбой подтвердить удаление
    всех макросов.

Когда все необходимые изменения будут сделаны, нажмите *Обновить*.
Атрибуты будут обновлены соответственно для всех выбранных шаблонов.

[comment]: # ({/new-b2a07f91})

[comment]: # ({new-be3b0440})

The **Template** tab contains general template mass update options.

![](../../../../assets/en/manual/config/templates/templ_mass.png){width=600}

The following options are available when selecting the respective button
for the *Template groups* update:

-   *Add* - allows to specify additional template groups from the existing ones or enter completely new template groups for the templates;
-   *Replace* - will remove the template from any existing template groups
    and replace them with the one(s) specified in this field (existing or new template groups);
-   *Remove* - will remove specific template groups from templates.

These fields are auto-complete - starting to type in them offers a
dropdown of matching template groups. If the template group is new, it also
appears in the dropdown and it is indicated by *(new)* after the string.
Just scroll down to select.

[comment]: # ({/new-be3b0440})

[comment]: # ({new-fd5d684f})

The **Tags** tab allows you to mass update template-level tags.

![](../../../../assets/en/manual/config/templates/templ_mass_c.png){width=600}

User macros, {INVENTORY.\*} macros, {HOST.HOST}, {HOST.NAME},
{HOST.CONN}, {HOST.DNS}, {HOST.IP}, {HOST.PORT} and {HOST.ID} macros are
supported in tags. Note that tags with the same name, but different
values are not considered 'duplicates' and can be added to the same
template.

[comment]: # ({/new-fd5d684f})

[comment]: # ({new-62e49505})

The **Macros** tab allows you to mass update template-level macros.

![](../../../../assets/en/manual/config/templates/templ_mass_d.png){width=600}

The following options are available when selecting the respective button
for macros update:

-   *Add* - allows to specify additional user macros for the templates.
    If *Update existing* checkbox is checked, value, type and
    description for the specified macro name will be updated. If
    unchecked, if a macro with that name already exist on the
    template(s), it will not be updated.
-   *Update* - will replace values, types and descriptions of macros
    specified in this list. If *Add missing* checkbox is checked, macro
    that didn't previously exist on a template will be added as new
    macro. If unchecked, only macros that already exist on a template
    will be updated.
-   *Remove* - will remove specified macros from templates. If *Except
    selected* box is checked, all macros except specified in the list
    will be removed. If unchecked, only macros specified in the list
    will be removed.
-   *Remove all* - will remove all user macros from templates. If *I
    confirm to remove all macros* checkbox is not checked, a new popup
    window will open asking to confirm removal of all macros.

[comment]: # ({/new-62e49505})

[comment]: # ({new-40ab4576})

The **Value mapping** tab allows you to mass update [value mappings](/manual/config/items/mapping).

![](../../../../assets/en/manual/config/templates/templ_mass_e.png){width=600}

Buttons with the following options are available for value map update:

-   *Add* - add value maps to the templates. If you mark *Update
    existing*, all properties of the value map with this name will be
    updated. Otherwise, if a value map with that name already exists, it
    will not be updated.
-   *Update* - update existing value maps. If you mark *Add missing*, a
    value map that didn't previously exist on a template will be added
    as a new value map. Otherwise only the value maps that already exist
    on a template will be updated.
-   *Rename* - give new name to an existing value map.
-   *Remove* - remove the specified value maps from the templates. If
    you mark *Except selected*, all value maps will be removed
    **except** the ones that are specified.
-   *Remove all* - remove all value maps from the templates. If the *I
    confirm to remove all value maps* checkbox is not marked, a new
    popup window will open asking to confirm the removal.

*Add from template* and *Add from host* options are available for value mapping add/update operations. 
They allow to select value mappings from a template or a host respectively.

[comment]: # ({/new-40ab4576})

[comment]: # ({new-84e57bb5})

When done with all required changes, click on *Update*. The attributes
will be updated accordingly for all the selected templates.

[comment]: # ({/new-84e57bb5})
