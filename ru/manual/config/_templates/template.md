[comment]: # translation:outdated

[comment]: # ({new-3222767b})
# 1 Настройка шаблона

[comment]: # ({/new-3222767b})

[comment]: # ({new-630aea00})
#### Обзор

Для настройки шаблона необходимо сначала создать его, указав общие
параметры, и только затем добавлять объекты (элементы данных, триггеры,
графики и прочее) к этому шаблону.

[comment]: # ({/new-630aea00})

[comment]: # ({new-de7b2ff6})
#### Создание шаблона

Для создания шаблона, выполните следующее:

-   Перейдите в *Настройка → Шаблоны*
-   Нажмите на *Создать шаблон*
-   Измените атрибуты шаблона

Вкладка **Шаблон** содержит общие атрибуты шаблона.

![](../../../../assets/en/manual/config/template.png){width="700"}

Все обязательные поля ввода отмечены красной звёздочкой.

Атрибуты шаблонов:

|Параметр|Описание|
|----------------|----------------|
|*Имя шаблона*|Уникальное имя шаблона.|
|*Видимое имя*|Если вы укажите это имя, то только оно будет видимо в списках, картах и прочем.|
|*Группы*|Группы узлов сети/шаблонов к которым принадлежит этот шаблон.|
|*Описание*|Введите описание шаблона.|

Вкладка **Соединенные шаблоны** позволяет вам присоединить один и более
"вложенных" шаблонов к этому шаблону. Все объекты (элементы данных,
триггеры, графики и прочее) будут унаследованы из присоединенных
шаблонов.

Для присоединения нового шаблона, начните печатать в поле *Присоединить
новые шаблоны* до появления списка шаблонов соответствующих введенной
букве (буквам). Прокрутите список и выберите. Когда все шаблоны будут
присоединены, нажмите на *Добавить*.

Для отсоединения шаблона, воспользуйтесь одной из двух опций в блоке
*Присоединенных шаблонов*:

-   *Отсоединить* - отсоединить шаблон, но оставить его элементы данных,
    триггеры и графики
-   *Отсоединить и очистить* - отсоединить шаблон и удалить все его
    элементы данных, триггеры и графики

Вкладка **Макросы** позволяет вам определить [пользовательские
макросы](/ru/manual/config/macros/usermacros) уровня шаблона. Вы также
можете здесь просмотреть макросы из присоединенных шаблонов и глобальные
макросы, если вы выберите опцию *Унаследованные и макросы из шаблонов*.
Это то место, где отображаются все определенные пользовательские макросы
для этого шаблона со своими раскрытыми значениями, а также информация о
том откуда эти макросы.

![](../../../../assets/en/manual/config/template_c.png){width="600"}

Для удобства имеются ссылки на настройку соответствующих шаблонов и
глобальных макросов. Также имеется возможность изменить макрос уровня
шаблона/глобальный на уровне этого шаблона, фактически создав копию
этого макроса у шаблона.

Кнопки:

|   |   |
|---|---|
|![](../../../../assets/en/manual/config/button_add.png)|Добавление шаблона. Добавленный шаблон должен появиться в списке.|
|![](../../../../assets/en/manual/config/button_update.png)|Обновление свойств существующего шаблона.|
|![](../../../../assets/en/manual/config/button_clone.png)|Создание другого шаблона основанного на свойствах текущего шаблона, включая все объекты (элементы данных, триггеры и прочее) унаследованные от присоединенных шаблонов.|
|![](../../../../assets/en/manual/config/button_full.png)|Создание другого шаблона основанного на свойствах текущего шаблона, включая все объекты (элементы данных, триггеры и прочее) как унаследованные от присоединенных шаблонов, так и напрямую присоединенные к текущему шаблону.|
|![](../../../../assets/en/manual/config/button_delete.png)|Удаление шаблона; объекты из шаблона (элементы данных, триггеры и прочее) останутся присоединенными к узлам сети.|
|![](../../../../assets/en/manual/config/button_clear.png)|Удаление шаблона и всех его объектов из присоединенных узлов сети.|
|![](../../../../assets/en/manual/config/button_cancel.png)|Отмена изменения свойств шаблона.|

Когда шаблон создан, самое время добавить в него какие-нибудь объекты.

::: noteimportant
Элементы данных должны быть добавлены первыми в
шаблон. Триггеры и графики нельзя добавить без наличия соответствующих
элементов данных.
:::

[comment]: # ({/new-de7b2ff6})

[comment]: # ({new-0d973911})
#### Добавление элементов данных, триггеров, графиков

Для добавления элементов данных в шаблон, сделайте следующее:

-   Перейдите в *Настройка → Узлы сети* (или *Шаблоны*)
-   Нажмите на *Элементы данных* в строке с требуемым узлом
    сети/шаблоном
-   Отметьте элементы данных которые вы хотите добавить в шаблон
-   Выберите *Копировать выбранное в...* ниже списка
-   Выберите шаблон (или группу шаблонов) в который необходимо
    скопировать элементы данных и нажмите на *Копировать*

Все выбранные элементы данных должны будут скопироваться в шаблон.

Добавление триггеров и графиков осуществляется похожим образом (из
списка триггеров и графиков соответственно), опять же, имейте ввиду, что
они могут быть добавлены только после того, как сначала будут добавлены
требуемые элементы данных.

[comment]: # ({/new-0d973911})

[comment]: # ({new-b78a3626})
#### Добавление комплексных экранов

Для добавления комплексных экранов в шаблон из *Настройка → Шаблоны*,
сделайте следующее:

-   Нажмите на *Комплексные экраны* в строке с шаблоном
-   Настройте комплексный экран обычным способом [настройки комплексных
    экранов](/ru/manual/config/visualisation/screens)

::: noteimportant
Элементы, которые можно добавить в комплексные
экраны шаблонов: простые графики, пользовательские графики, часы,
простой текст, URL.
:::

::: notetip
Для получения информации касательно доступа к
комплексным экранам узлов сети, которые создаются с шаблонных
комплексных экранов, смотрите раздел [комплексный экран узла
сети](/ru/manual/config/visualisation/host_screens#доступ_к_комплексным_экранам_узлов_сети).
:::

[comment]: # ({/new-b78a3626})

[comment]: # ({new-513e93e5})
#### Настройка правил низкоуровневого обнаружения

Смотрите раздел по [низкоуровневому
обнаружению](/ru/manual/discovery/low_level_discovery) в этом
руководстве.

[comment]: # ({/new-513e93e5})

[comment]: # ({new-e3935a9d})
#### Добавление веб-сценариев

Чтобы добавить веб-сценарии в шаблон в *Настройка → Шаблоны*, сделайте
следующее:

-   Нажмите на *Веб* в строке с шаблоном
-   Настройте веб-сценарии обычным способом [настройки
    веб-сценариев](/ru/manual/web_monitoring)

[comment]: # ({/new-e3935a9d})

[comment]: # ({new-d357b380})
#### Creating a template group

::: noteimportant
Only Super Admin users can create template groups.
:::

To create a template group in Zabbix frontend, do the following:

-   Go to: *Configuration → Template groups*
-   Click on *Create template group* in the upper right corner of the screen
-   Enter the group name in the form

![](../../../../assets/en/manual/config/template_group.png)

To create a nested template group, use the '/' forward slash separator, for example `Linux servers/Databases/MySQL`. You can create this group even if none of the two parent template groups (`Linux servers/Databases/`) exist. In this case creating these parent template groups is up to the user; they will not be created automatically.<br>Leading and trailing slashes, several slashes in a row are not allowed. Escaping of '/' is not supported.

[comment]: # ({/new-d357b380})

[comment]: # ({new-ed4ea073})

Once the group is created, you can click on the group name in the list to edit group name, clone the group or set additional option:

![](../../../../assets/en/manual/config/template_group2.png)

*Apply permissions to all subgroups* - mark this checkbox and click on *Update* to apply the same level of permissions to all nested template groups. For user groups that may have had differing [permissions](/manual/config/users_and_usergroups/usergroup#configuration) assigned to nested template groups, the permission level of the parent template group will be enforced on the nested groups. This is a one-time option that is not saved in the database.

**Permissions to nested template groups**

-   When creating a child template group to an existing parent template group,
    [user group](/manual/config/users_and_usergroups/usergroup)
    permissions to the child are inherited from the parent (for example,
    when creating `Databases/MySQL` if `Databases` already exists)
-   When creating a parent template group to an existing child template group,
    no permissions to the parent are set (for example, when creating
    `Databases` if `Databases/MySQL` already exists)

[comment]: # ({/new-ed4ea073})
