[comment]: # translation:outdated

[comment]: # ({new-69aa4121})
# 1 Корреляция событий на основе триггеров

[comment]: # ({/new-69aa4121})

[comment]: # ({new-f01b1f93})
#### Обзор

Корреляция событий на основе триггеров позволяет сопоставлять отдельные
проблемы, о которых сообщает один триггер.

В то время как ОК события закрывают в Zabbix все события о проблеме,
бывают случаи, когда необходим более обстоятельный подход. Например, при
мониторинге файлов журналов вы можете захотеть обнаруживать некоторые
проблемы в файле журнала и закрывать их по отдельности, а не все разом.

Это тот случай, когда триггеры имеют активированную опцию *Формирование
множественных Проблема событий*. Такие триггеры обычно используются для
мониторинга журналов, обработки трапов и т.п.

В Zabbix имеется возможность сопоставить события о проблемах,
основываясь на [тегах
событий](/ru/manual/config/event_correlation/trigger/event_tags). Теги
используются для извлечения значений и создания метки по событиям о
проблеме. Используя преимущества такого подхода, проблемы могут быть
закрыты отдельно на основании совпадения тега.

Другими словами, один триггер может создавать отдельные события, которые
идентифицируются при помощи тега событий. Следовательно, события о
проблемах могут быть выявлены одно за другим и закрыты отдельно на
основании метки при помощи тега событий.

[comment]: # ({/new-f01b1f93})

[comment]: # ({new-12f3de38})
#### Как это работает

В мониторинге журналов вы можете встретиться со строками как эти:

    Строка1: Приложение 1 остановлено
    Строка2: Приложение 2 остановлено
    Строка3: Приложение 1 перезапущено
    Строка4: Приложение 2 перезапущено

Идея корреляции событий состоит в том, чтобы была возможность
сопоставить событие о проблеме со Строки1 с решением со Строки3 и
событие о проблеме со Строки2 с решением со Строки4 и закрыть эти
проблемы одну за другой:

    Строка1: Приложение 1 остановлено
    Строка3: Приложение 1 перезапущено #проблема со Строки 1 закрыта

    Строка2: Приложение 2 остановлено
    Строка4: Приложение 2 перезапущено #проблема со Строки 2 закрыта

Чтобы сделать подобное вам необходимо отметить эти связанные события
как, например, "Приложение 1" и "Приложение 2". Это можно сделать
применив регулярное выражение к строке из файла журнала, чтобы извлечь
значение тега. Затем, когда события будут созданы, они будут
промаркированы как "Приложение 1" и "Приложение 2" соответственно и
проблема может быть сопоставлена решению.

[comment]: # ({/new-12f3de38})

[comment]: # ({new-5d32b87c})
##### Item

To begin with, you may want to set up an item that monitors a log file,
for example:

    log[/var/log/syslog]

![](../../../../assets/en/manual/config/event_correlation/correlation_item.png)

With the item set up, wait a minute for the configuration changes to be
picked up and then go to [Latest
data](/manual/web_interface/frontend_sections/monitoring/latest_data) to
make sure that the item has started collecting data.

[comment]: # ({/new-5d32b87c})

[comment]: # ({new-8d1971f2})
#### Настройка

Для настройки корреляции на уровне триггера:

-   перейдите в [диалог настройки](/ru/manual/config/triggers/trigger)
    триггера

![](../../../../assets/en/manual/config/correlation_trigger.png){width="600"}

Все обязательные поля ввода отмечены красной звёздочкой.

-   выберите 'Режим формирования Проблема событий' значение
    *Множественный*
-   в 'ОК событие закрывает' выберите *Все проблемы, если значения тегов
    совпадают*
-   введите имя тега для сопоставления событий
-   настройте
    [теги](/ru/manual/config/event_correlation/trigger/event_tags) для
    извлечения значений тега со строк журнала

При успешной настройке у вас будет возможность видеть события о
проблемах промаркированные приложением и сопоставленные с их решением в
*Мониторинг* → *Проблемы*.

![](../../../../assets/en/manual/config/matched_problems.png){width="600"}

::: notewarning
Посколько возможна некорректная настройка, когда
похожие теги событий могут быть созданы по **нерешаемым** проблемам,
пожалуйста, рассмотрите указанные ниже подобные случаи!
:::

-   При наличии двух приложений, которые записывают сообщениях об
    ошибках и восстановлениях в один и тот же файл журнала, пользователь
    может решить использование двух *Приложение* тегов в одном триггере
    с различными значениями тегов, используя разные регулярные выражения
    в значениях тегов для извлечения имен, так сказать приложение А и
    приложение В из {ITEM.VALUE} макроса (например, если форматы
    сообщений различаются). Однако, такой подход может не сработать как
    задумано, если регулярным выражениям будут отсутствовать
    соответствия. Несоответствующие регулярные выражения могут выдать
    пустые значения тегов и одного пустого значения тега как по событиям
    о проблеме, так и по ОК событиям, будет достаточно чтобы их
    соотнести. Таким образом сообщение о восстановлении с приложения А
    может случайно закрыть сообщение об ошибке с приложения В.

```{=html}
<!-- -->
```
-   Реальные теги и значения тегов становятся видимы только когда
    срабатывает триггер. Если используемое регулярное выражение
    ошибочно, оно автоматически заменяется на \*НЕИЗВЕСТНО\* строку.
    Если изначальное событие о проблеме со значением тега \*НЕИЗВЕСТНО\*
    отсутствует, тогда могут появиться последующие ОК события с тем же
    значением тега \*НЕИЗВЕСТНО\*, которые могут закрыть события о
    проблемах, которые они не должны закрывать.

```{=html}
<!-- -->
```
-   Если пользователь использует {ITEM.VALUE} макрос без функций макроса
    в качестве значения тега, тогда будет применяться ограничение по
    длине строки в 255-символ. Когда в журнале имеются длинные сообщения
    и первые 255 символов не конкретизируют проблему, может привести к
    идентичным тегам событий по нерешаемым проблемам.

[comment]: # ({/new-8d1971f2})



[comment]: # ({new-57e60c4e})
##### Trigger

With the item working you need to configure the
[trigger](/manual/config/triggers/trigger). It's important to decide
what entries in the log file are worth paying attention to. For example,
the following trigger expression will search for a string like
'Stopping' to signal potential problems:

    find(/My host/log[/var/log/syslog],,"regexp","Stopping")=1 

::: noteimportant
To make sure that each line containing the string
"Stopping" is considered a problem also set the *Problem event
generation mode* in trigger configuration to 'Multiple'.
:::

Then define a recovery expression. The following recovery expression
will resolve all problems if a log line is found containing the string
"Starting":

    find(/My host/log[/var/log/syslog],,"regexp","Starting")=1 

Since we do not want that it's important to make sure somehow that the
corresponding root problems are closed, not just all problems. That's
where tagging can help.

Problems and resolutions can be matched by specifying a tag in the
trigger configuration. The following settings have to be made:

-   *Problem event generation mode*: Multiple
-   *OK event closes*: All problems if tag values match
-   Enter the name of the tag for event matching

![](../../../../assets/en/manual/config/event_correlation/correlation_trigger.png)

-   configure the [tags](/manual/config/tagging) to extract tag values
    from log lines

![](../../../../assets/en/manual/config/event_correlation/correlation_trigger2.png)

If configured successfully you will be able to see problem events tagged
by application and matched to their resolution in *Monitoring* →
*Problems*.

![](../../../../assets/en/manual/config/matched_problems.png){width="600"}

::: notewarning
Because misconfiguration is possible, when similar
event tags may be created for **unrelated** problems, please review the
cases outlined below!
:::

-   With two applications writing error and recovery messages to the
    same log file a user may decide to use two *Application* tags in the
    same trigger with different tag values by using separate regular
    expressions in the tag values to extract the names of, say,
    application A and application B from the {ITEM.VALUE} macro (e.g.
    when the message formats differ). However, this may not work as
    planned if there is no match to the regular expressions.
    Non-matching regexps will yield empty tag values and a single empty
    tag value in both problem and OK events is enough to correlate them.
    So a recovery message from application A may accidentally close an
    error message from application B.

```{=html}
<!-- -->
```
-   Actual tags and tag values only become visible when a trigger fires.
    If the regular expression used is invalid, it is silently replaced
    with an \*UNKNOWN\* string. If the initial problem event with an
    \*UNKNOWN\* tag value is missed, there may appear subsequent OK
    events with the same \*UNKNOWN\* tag value that may close problem
    events which they shouldn't have closed.

```{=html}
<!-- -->
```
-   If a user uses the {ITEM.VALUE} macro without macro functions as the
    tag value, the 255-character limitation applies. When log messages
    are long and the first 255 characters are non-specific, this may
    also result in similar event tags for unrelated problems.

[comment]: # ({/new-57e60c4e})
