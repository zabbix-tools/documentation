[comment]: # translation:outdated

[comment]: # ({new-b4b0e364})
# - \#4 События

[comment]: # ({/new-b4b0e364})

[comment]: # ({new-3825c91b})
#### Обзор

События в Zabbix генерируются несколькими источниками:

-   события на триггеры - всякий раз, когда триггер меняет свое
    состояние(ОК→ПРОБЛЕМА→ОК)
-   события на обнаружение - при обнаружении узлов сети или сервисов
-   события на авторегистрацию - когда активные агенты автоматически
    регистрируются сервером
-   внутренние события - когда элементы данных/правила низкоуровневого
    обнаружения становятся не поддерживаемыми или триггер переходит в
    состояние неизвестно

::: noteclassic
Внутренние события поддерживаются начиная с Zabbix
2.2.
:::

События имеют штамп времени и могут быть основой для действий, таких как
отправка оповещений по почте и т.п.

Для просмотра деталей событий в веб-интерфейсе, перейдите в *Мониторинг*
→ *События*. Здесь вы можете нажать на дату и время события и
просмотреть его детали.

Более подробная информация доступна в:

-   [события на триггеры](/ru/manual/config/events/trigger_events)
-   [другие источники событий](/ru/manual/config/events/sources)

[comment]: # ({/new-3825c91b})
