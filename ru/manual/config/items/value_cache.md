[comment]: # translation:outdated

[comment]: # ({new-23de852a})
# 11 Кэш значений

[comment]: # ({/new-23de852a})

[comment]: # ({new-f73e0537})
#### Обзор

Чтобы сделать вычисления выражений триггеров, вычисляемых/агрегированных
элементов данных и некоторых макросов, начиная с Zabbix 2.2 в Zabbix
сервере поддерживается опция кэша значений.

Этот кэш находящийся в оперативной памяти используется для доступа к
данным истории, вместо выполнения прямых запросов SQL в базу данных.
Если данные истории не присутствуют в кэше, недостающие значения
запрашиваются из базы данных и, соответственно, кэш обновляется.

Для включения фунционала кэша значений, поддерживается дополнительный
параметр **ValueCacheSize** в [файле
конфигурации](/ru/manual/appendix/config/zabbix_server) Zabbix server.

Два внутренних элемента данных поддерживаются для наблюдения за кэшем
значений **zabbix\[vcache,buffer,<режим>\]** и
**zabbix\[vcache,cache,<параметр>\]**. Для получения более
детальных сведений смотрите [внутренние элементы
данных](/ru/manual/config/items/itemtypes/internal).

[comment]: # ({/new-f73e0537})

[comment]: # ({new-3add2a5c})
Item values remain in value cache either until:

-   the item is deleted (cached values are deleted after the next configuration sync);
-   the item value is outside the time or count range specified in the trigger/calculated item expression
    (cached value is removed when a new value is received);
-   the time or count range specified in the trigger/calculated item expression is changed
    so that less data is required for calculation (unnecessary cached values are removed after 24 hours).

::: notetip
Value cache status can be observed by using the server [runtime control](/manual/concepts/server#runtime-control) option
`diaginfo` (or `diaginfo=valuecache`) and inspecting the section for value cache diagnostic information.
This can be useful for determining misconfigured triggers or calculated items.
:::

[comment]: # ({/new-3add2a5c})

[comment]: # ({new-1fb59f91})
To enable the value cache functionality, an optional **ValueCacheSize**
parameter is supported by the Zabbix server
[configuration](/manual/appendix/config/zabbix_server) file.

Two internal items are supported for monitoring the value cache:
**zabbix\[vcache,buffer,<mode>\]** and
**zabbix\[vcache,cache,<parameter>\]**. See more details with
[internal items](/manual/config/items/itemtypes/internal).

[comment]: # ({/new-1fb59f91})
