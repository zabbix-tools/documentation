[comment]: # translation:outdated

[comment]: # ({new-d3accdff})
# Дополнительные Javascript объекты

[comment]: # ({/new-d3accdff})

[comment]: # ({new-ab8bd378})
#### Обзор

Этот раздел описывает Zabbix дополнения в Javascript язык, который
реализован при помощи Duktape.

[comment]: # ({/new-ab8bd378})

[comment]: # ({new-c73f2449})
#### Встроенные объекты

**Zabbix**

Zabbix объект даёт возможность взаимодействия с внутренней
функциональностью Zabbix.

|Метод|Описание|
|----------|----------------|
|`Log(уровень_журнала, сообщение)`|Записывает <сообщение> в журнал Zabbix с использованием <уровень\_журнала> уровня журнала (смотрите DebugLevel параметр в файле конфигурации).|

Пример:

    Zabbix.Log(3, "this is a log entry written with 'Warning' log level")

**CurlHttpRequest**

Этот объект инкаспулирует cURL дескриптор, который позволяет выполнять
простые HTTP запросы. Ошибки сообщаются в вызове исключений.

|Метод|Описание|
|----------|----------------|
|`AddHeader(имя, значение)`|Добавление поля HTTP заголовка. Это поле используется для всех последующих вызовов, до момента очистки при помощи ClearHeader() метода.|
|`ClearHeader()`|Очистка HTTP заголовка. если заданные поля заголовков отсутствуют, CurlHttpRequest задаст Content-Type значением application/json, если публикуемые данные отформатированы в виде json и text/plain в противном случае.|
|`Get(url, данные)`|Отправка HTTP GET запроса на URL с необязательным информационным дополнением *data* и получение ответа.|
|`Put(url, данные)`|Отправка HTTP PUT запроса на URL с необязательным информационным дополнением *data* и получение ответа.|
|`Post(url, данные)`|Отправка HTTP POST запроса на URL с необязательным информационным дополнением *data* и получение ответа.|
|`Delete(url, данные)`|Отправка HTTP DELETE запроса на URL с необязательным информационным дополнением *data* и получение ответа.|
|`Status()`|Возврат кода состояния последнего HTTP запроса.|

Пример:

``` {.java}
try {
    Zabbix.Log(4, 'jira webhook script value='+value);
  
    var result = {
        'tags': {
            'endpoint': 'jira'
        }
    },
    params = JSON.parse(value),
    req = new CurlHttpRequest(),
    fields = {},
    resp;
  
    req.AddHeader('Content-Type: application/json');
    req.AddHeader('Authorization: Basic '+params.authentication);
  
    fields.summary = params.summary;
    fields.description = params.description;
    fields.project = {"key": params.project_key};
    fields.issuetype = {"id": params.issue_id};
    resp = req.Post('https://tsupport.zabbix.lan/rest/api/2/issue/',
        JSON.stringify({"fields": fields})
    );
  
    if (req.Status() != 201) {
        throw 'Response code: '+req.Status();
    }
  
    resp = JSON.parse(resp);
    result.tags.issue_id = resp.id;
    result.tags.issue_key = resp.key;
} catch (error) {
    Zabbix.Log(4, 'jira issue creation failed json : '+JSON.stringify({"fields": fields}));
    Zabbix.Log(4, 'jira issue creation failed : '+error);
  
    result = {};
}
  
return JSON.stringify(result);
```

[comment]: # ({/new-c73f2449})






[comment]: # ({new-e5dc8dd0})
#### Zabbix

The Zabbix object provides interaction with the internal Zabbix
functionality.

|Method|Description|
|------|-----------|
|`log(loglevel, message)`|Writes <message> into Zabbix log using <loglevel> log level (see configuration file DebugLevel parameter).|

Example:

    Zabbix.log(3, "this is a log entry written with 'Warning' log level")

You may use the following aliases:

|Alias|Alias to|
|-----|--------|
|console.log(object)|Zabbix.log(4, JSON.stringify(object))|
|console.warn(object)|Zabbix.log(3, JSON.stringify(object))|
|console.error(object)|Zabbix.log(2, JSON.stringify(object))|

[comment]: # ({/new-e5dc8dd0})

[comment]: # ({new-413974f8})
#### HttpRequest

This object encapsulates cURL handle allowing to make simple HTTP
requests. Errors are thrown as exceptions.

::: noteimportant
HttpRequest is a new name for this object since
Zabbix 5.4. Previously it used to be called CurlHttpRequest. Method
names have also been changed in Zabbix 5.4. The old object/method names
are now deprecated and their support will be discontinued after Zabbix
6.0.
:::

|Method|Description|
|------|-----------|
|`addHeader(name, value)`|Adds HTTP header field. This field is used for all following requests until cleared with the clearHeader() method.|
|`clearHeader()`|Clears HTTP header. If no header fields are set, HttpRequest will set Content-Type to application/json if the data being posted is JSON-formatted; text/plain otherwise.|
|`connect(url)`|Sends HTTP CONNECT request to the URL and returns the response.|
|`customRequest(method, url, data)`|Allows to specify any HTTP method in the first parameter. Sends the method request to the URL with optional *data* payload and returns the response.|
|`delete(url, data)`|Sends HTTP DELETE request to the URL with optional *data* payload and returns the response.|
|`getHeaders()`|Returns object of received HTTP header fields.|
|`get(url, data)`|Sends HTTP GET request to the URL with optional *data* payload and returns the response.|
|`head(url)`|Sends HTTP HEAD request to the URL and returns the response.|
|`options(url)`|Sends HTTP OPTIONS request to the URL and returns the response.|
|`patch(url, data)`|Sends HTTP PATCH request to the URL with optional *data* payload and returns the response.|
|`put(url, data)`|Sends HTTP PUT request to the URL with optional *data* payload and returns the response.|
|`post(url, data)`|Sends HTTP POST request to the URL with optional *data* payload and returns the response.|
|`getStatus()`|Returns the status code of the last HTTP request.|
|`setProxy(proxy)`|Sets HTTP proxy to "proxy" value. If this parameter is empty then no proxy is used.|
|`setHttpAuth(bitmask, username, password)`|Sets enabled HTTP authentication methods (HTTPAUTH\_BASIC, HTTPAUTH\_DIGEST, HTTPAUTH\_NEGOTIATE, HTTPAUTH\_NTLM, HTTPAUTH\_NONE) in the 'bitmask' parameter.<br>The HTTPAUTH\_NONE flag allows to disable HTTP authentication.<br>Examples:<br>`request.setHttpAuth(HTTPAUTH_NTLM \| HTTPAUTH_BASIC, username, password)`<br>`request.setHttpAuth(HTTPAUTH_NONE)`|
|`trace(url, data)`|Sends HTTP TRACE request to the URL with optional *data* payload and returns the response.|

Example:

``` {.java}
try {
    Zabbix.log(4, 'jira webhook script value='+value);
  
    var result = {
        'tags': {
            'endpoint': 'jira'
        }
    },
    params = JSON.parse(value),
    req = new HttpRequest(),
    fields = {},
    resp;
  
    req.addHeader('Content-Type: application/json');
    req.addHeader('Authorization: Basic '+params.authentication);
  
    fields.summary = params.summary;
    fields.description = params.description;
    fields.project = {"key": params.project_key};
    fields.issuetype = {"id": params.issue_id};
    resp = req.post('https://tsupport.zabbix.lan/rest/api/2/issue/',
        JSON.stringify({"fields": fields})
    );
  
    if (req.getStatus() != 201) {
        throw 'Response code: '+req.getStatus();
    }
  
    resp = JSON.parse(resp);
    result.tags.issue_id = resp.id;
    result.tags.issue_key = resp.key;
} catch (error) {
    Zabbix.log(4, 'jira issue creation failed json : '+JSON.stringify({"fields": fields}));
    Zabbix.log(4, 'jira issue creation failed : '+error);
  
    result = {};
}
  
return JSON.stringify(result);
```

[comment]: # ({/new-413974f8})

[comment]: # ({new-06cac1ca})
#### XML

The XML object allows the processing of XML data in the item and
low-level discovery preprocessing and webhooks.

::: noteimportant
 In order to use XML object, server/proxy must be
compiled with libxml2 support.
:::

|Method|Description|
|------|-----------|
|`XML.query(expression, data)`|Retrieves node content using XPath. Returns null if node is not found.<br>**expression** - an XPath expression;<br>**data** - XML data as a string.|
|`XML.toJson(data)`|Converts data in XML format to JSON.|
|`XML.fromJson(object)`|Converts data in JSON format to XML.|

Example:

*Input:*

    <menu>
        <food type = "breakfast">
            <name>Chocolate</name>
            <price>$5.95</price>
            <description></description>
            <calories>650</calories>
        </food>
    </menu>

*Output:*

``` {.java}
{
    "menu": {
        "food": {
            "@type": "breakfast",
            "name": "Chocolate",
            "price": "$5.95",
            "description": null,
            "calories": "650"
        }
    }
}
```

[comment]: # ({/new-06cac1ca})

[comment]: # ({new-a12fbdc2})
##### Serialization rules

XML to JSON conversion will be processed according to the following
rules (for JSON to XML conversions reversed rules are applied):

1\. XML attributes will be converted to keys that have their names
prepended with '@'.

Example:

*Input:*

     <xml foo="FOO">
       <bar>
         <baz>BAZ</baz>
       </bar>
     </xml>

*Output:*

``` {.java}
 {
   "xml": {
     "@foo": "FOO",
     "bar": {
       "baz": "BAZ"
     }
   }
 }
```

2\. Self-closing elements (<foo/>) will be converted as having
'null' value.

Example:

*Input:*

    <xml>
      <foo/>
    </xml>

*Output:*

``` {.java}
{
  "xml": {
    "foo": null
  }
}
```

3\. Empty attributes (with "" value) will be converted as having empty
string ('') value.

Example:

*Input:*

    <xml>
      <foo bar="" />
    </xml>

*Output:*

``` {.java}
{
  "xml": {
    "foo": {
      "@bar": ""
    }
  }
}
```

4\. Multiple child nodes with the same element name will be converted to
a single key that has an array of values as its value.

Example:

*Input:*

    <xml>
      <foo>BAR</foo>
      <foo>BAZ</foo>
      <foo>QUX</foo>
    </xml>

*Output:*

``` {.java}
{
  "xml": {
    "foo": ["BAR", "BAZ", "QUX"]
  }
}
```

5\. If a text element has no attributes and no children, it will be
converted as a string.

Example:

*Input:*

    <xml>
        <foo>BAZ</foo>
    </xml>

*Output:*

``` {.java}
{
  "xml": {
    "foo": "BAZ"
   }
}
```

6\. If a text element has no children, but has attributes: text content
will be converted to an element with the key '\#text' and content as a
value; attributes will be converted as described in the serialization
rule 1.

Example:

*Input:*

    <xml>
      <foo bar="BAR">
        BAZ
      </foo>
    </xml>

*Output:*

``` {.java}
{
  "xml": {
    "foo": {
      "@bar": "BAR",
      "#text": "BAZ"
    }
  }
}
```

[comment]: # ({/new-a12fbdc2})

[comment]: # ({new-1368a2b0})
### Global JavaScript functions

Additional global JavaScript functions have been implemented with
Duktape:

-   btoa(string) - encodes string to base64 string
-   atob(base64\_string) - decodes base64 string

``` {.java}
try {
    b64 = btoa("utf8 string");
    utf8 = atob(b64);
} 
catch (error) {
    return {'error.name' : error.name, 'error.message' : error.message}
}
```

-   md5(string) - calculates the MD5 hash of a string
-   sha256(string) - calculates the SHA256 hash of a string

[comment]: # ({/new-1368a2b0})
