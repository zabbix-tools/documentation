[comment]: # translation:outdated

[comment]: # ({new-06812ee1})
# 2 Типы элементов данных

[comment]: # ({/new-06812ee1})

[comment]: # ({new-f03e2e9a})
#### Обзор

Типы элементов данных охватывают различные методы получения данных с
вашей системы. Каждый тип элемента данных поставляется со своим
собственным набором поддерживаемых ключей элементов данных и требуемых
параметров.

В настоящее время Zabbix предлагает следующие типы элементов данных:

-   [Zabbix агент
    проверки](/ru/manual/config/items/itemtypes/zabbix_agent)
-   [SNMP агент проверки](/ru/manual/config/items/itemtypes/snmp)
-   [SNMP трапы](/ru/manual/config/items/itemtypes/snmptrap)
-   [IPMI проверки](/ru/manual/config/items/itemtypes/ipmi)
-   [Простые проверки](/ru/manual/config/items/itemtypes/simple_checks)
    -   [VMware
        мониторинг](/ru/manual/config/items/itemtypes/simple_checks/vmware_keys)
-   [Мониторинг файлов
    журналов](/ru/manual/config/items/itemtypes/log_items)
-   [Вычисляемые элементы
    данных](/ru/manual/config/items/itemtypes/calculated)
    -   [Агрегированные](/ru/manual/config/items/itemtypes/calculated/aggregate)
-   [Внутренние проверки
    Zabbix](/ru/manual/config/items/itemtypes/internal)
-   [SSH проверки](/ru/manual/config/items/itemtypes/ssh_checks)
-   [Telnet проверки](/ru/manual/config/items/itemtypes/telnet_checks)
-   [Внешние проверки](/ru/manual/config/items/itemtypes/external)
-   [Траппер элементы данных](/ru/manual/config/items/itemtypes/trapper)
-   [JMX мониторинг](/ru/manual/config/items/itemtypes/jmx_monitoring)
-   [ODBC проверки](/ru/manual/config/items/itemtypes/odbc_checks)
-   [Зависимые элементы
    данных](/ru/manual/config/items/itemtypes/dependent_items)
-   [HTTP проверки](/ru/manual/config/items/itemtypes/http)

Детальная информация по всем типам элементов данных включена в
подстраницы этого раздела. Несмотря на это, некоторые типы элементов
данных предлагают большее количество опций по сбору данных,
дополнительные опции через [пользовательские
параметры](/ru/manual/config/items/userparameters) или [подгружаемые
модули](/ru/manual/config/items/loadablemodules).

Некоторые проверки выполняются Zabbix сервером в одиночку (так
называемый безагентный мониторинг) в то время как остальные требуют
Zabbix агента или даже Zabbix Java gateway (с JMX мониторингом).

::: noteimportant
Если отдельный тип элемента данных требует
определенный интерфейс (например, IPMI проверка требует IPMI интерфейс
на узле сети), то этот интерфейс должен существовать в определении этого
узла сети.
:::

Можно задвать несколько интерфейсов в определении узла сети: Zabbix
агент, SNMP агент, JMX и IPMI. Если элемент данных может использовать
более чем один интерфейс, он будет искать доступные интерфейсы у узла
сети (в следующем порядке: Агент→SNMP→JMX→IPMI) и будет связан с первым
подходящим ему интерфейсом.

Все элементы данных которые возвращают текст (символ, журнал, текстовый
типы информации) теперь могут возвращать только пробелы (при
необходимости), при этом возвращенное значение становится пустой строкой
(поддерживается начиная с 2.0).

[comment]: # ({/new-f03e2e9a})
