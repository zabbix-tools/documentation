[comment]: # attributes: notoc

[comment]: # translation:outdated

[comment]: # ({new-be4ab03d})
# 5 Простые проверки

[comment]: # ({/new-be4ab03d})

[comment]: # ({new-76023448})
#### Обзор

Простые проверки в основном используются для удаленных безагентных
проверок сервисов.

Обратите внимание, что для простых проверок Zabbix агент не требуется.
За обработку (созданием внешних подключений и т.д.) простых проверок
отвечает Zabbix сервер/прокси.

Примеры использования простых проверок:

    net.tcp.service[ftp,,155]
    net.tcp.service[http]
    net.tcp.service.perf[http,,8080]
    net.udp.service.perf[ntp]

::: noteclassic
Поля *Имя пользователя* и *пароль* в конфигурации простых
элементов данных используются для элементов данных мониторинга VMware;
иначе игнорируются.
:::

[comment]: # ({/new-76023448})

[comment]: # ({new-f59a5ccc})
#### Поддерживаемые простые проверки

Список поддерживаемых простых проверок:

Смотрите также:

-   [Ключи элементов данных для мониторинга
    VMware](/ru/manual/config/items/itemtypes/simple_checks/vmware_keys)

|Ключ|<|<|<|<|
|--------|-|-|-|-|
|▲|**Описание**|**Возвращаемое значение**|**Параметры**|**Комментарии**|
|icmpping\[<цель>,<пакеты>,<интервал>,<размер>,<время ожидания>\]|<|<|<|<|
|<|Доступность хоста через пинг по ICMP.|0 - ошибка при пинге по ICMP<br>1 - успешный пинг по ICMP|**цель** - IP хоста или DNS имя<br>**пакеты** - количество пакетов<br>**интервал** - время между успешными пакетами в миллисекундах<br>**размер** - размер пакета в байтах<br>**время ожидания** - время ожидания в миллисекундах|Пример:<br>=> **icmpping\[,4\]** → если по крайней мере один пакет из четырех вернется, элемент данных возвратит 1.<br><br>Смотрите также таблицу [со значениями по умолчанию](/ru/manual/config/items/itemtypes/simple_checks#icmp_пинг).|
|icmppingloss\[<цель>,<пакеты>,<интервал>,<размер>,<время ожидания>\]|<|<|<|<|
|<|Процентное отношение потерянных пакетов.|Число с плавающей точкой.|**цель** - IP хоста или DNS имя<br>**пакеты** - количество пакетов<br>**интервал** - время между успешными пакетами в миллисекундах<br>**размер** - размер пакета в байтах<br>**время ожидания** - время ожидания в миллисекундах|Смотрите также таблицу [со значениями по умолчанию](/ru/manual/config/items/itemtypes/simple_checks#icmp_пинг).|
|icmppingsec\[<цель>,<пакеты>,<интервал>,<размер>,<время ожидания>,<режим>\]|<|<|<|<|
|<|Время ответа на пинг по ICMP (в секундах).|Число с плавающей точкой|**цель** - IP хоста или DNS имя<br>**пакеты** - количество пакетов<br>**интервал** - время между успешными пакетами в миллисекундах<br>**размер** - размер пакета в байтах<br>**время ожидания** - время ожидания в миллисекундах<br>**режим** - один из min, max, avg (по умолчанию)|Если хост недоступен (превышено время ожидания), элемент данных вернет 0.<br>Если элемент данных "icmppingsec" вернет значение меньше 0.0001 секунд, значение будет равно 0.0001 секунд.<br><br>Смотрите также таблицу [со значениями по умолчанию](/ru/manual/config/items/itemtypes/simple_checks#icmp_пинг).|
|net.tcp.service\[сервис,<ip>,<порт>\]|<|<|<|<|
|<|Проверка запущен ли сервис и отвечает ли на TCP подключения.|0 - сервис недоступен<br>1 - сервис работает|**сервис** - один из ssh, ntp, ldap, smtp, ftp, http, pop, nntp, imap, tcp, https, telnet (смотри [детали](/ru/manual/appendix/items/service_check_details))<br>**ip** - IP адрес или DNS имя (по умолчанию, используется IP/DNS узла сети)<br>**порт** - номер порта (по умолчанию для сервиса используется стандартный номер порта).|Пример:<br>=> net.tcp.service\[ftp,,45\] → можно использовать для проверки доступности FTP сервера на 45 порту TCP.<br><br>Обратите внимание, для сервиса *tcp* обязательно нужно указывать порт.<br>Эти проверки могут привести к дополнительным записям в системных лог файлах (обычно сессии SMTP и SSH журналируются).<br>Проверка шифрованных протоколов (таких как IMAP на 993 порту или POP на 995 порту) в настоящее время не поддерживается. Как решение, пожалуйста, для подобных проверок используйте net.tcp.service\[tcp,<ip>,порт\].<br>Сервисы *https* и *telnet* поддерживаются Zabbix начиная с версии 2.0.|
|net.tcp.service.perf\[сервис,<ip>,<порт>\]|<|<|<|<|
|<|Проверка производительности сервиса.|Число с плавающей точкой.<br><br>0.000000 - сервис недоступен<br><br>сек - количество секунд потребовавшихся для подключения к сервису|**сервис** - один из ssh, ntp, ldap, smtp, ftp, http, pop, nntp, imap, tcp, https, telnet (смотри [детали](/ru/manual/appendix/items/service_check_details))<br>**ip** - IP адрес или DNS имя (по умолчанию, используется IP/DNS узла сети)<br>**порт** - номер порта (по умолчанию для сервиса используется стандартный номер порта).|Пример ключа:<br>=> net.tcp.service.perf\[ssh\] → можно использовать для проверки скорости начального ответа от SSH сервера.<br><br>Обратите внимание, для сервиса *tcp* обязательно нужно указывать порт.<br>Проверка шифрованных протоколов (таких как IMAP на 993 порту или POP на 995 порту) в настоящее время не поддерживается. Как решение, пожалуйста, для подобных проверок используйте net.tcp.service.perf\[tcp,<ip>,порт\].<br>Сервисы *https* и *telnet* поддерживаются Zabbix начиная с версии 2.0.<br>Назывался tcp\_perf до Zabbix 2.0.|
|net.udp.service\[сервис,<ip>,<порт>\]|<|<|<|<|
|<|Проверка запущен ли сервис и отвечает ли на UDP подключения.|0 - сервис недоступен<br>1 - сервис работает|**сервис** - возможные значения: *ntp* (смотри [детали](/ru/manual/appendix/items/service_check_details))<br>**ip** - IP адрес или DNS имя (по умолчанию, используется IP/DNS узла сети)<br>**порт** - номер порта (по умолчанию для сервиса используется стандартный номер порта).|Пример:<br>=> net.udp.service\[ntp,,45\] → можно использовать для тестирования доступности NTP сервиса на 45 порту UDP.<br><br>Этот элемент данных поддерживается начиная с Zabbix 3.0, но *ntp* сервис был доступен в net.tcp.service\[\] элементе данных и в предыдущих версиях.|
|net.udp.service.perf\[service,<ip>,<port>\]|<|<|<|<|
|<|Проверка производительности UDP сервиса.|Число с плавающей точкой.<br><br>0.000000 - сервис недоступен<br><br>секунды - количество секунд прошедшее на ожидания ответа от сервиса|**сервис** - возможные значения: *ntp* (смотри [детали](/ru/manual/appendix/items/service_check_details))<br>**ip** - IP адрес или DNS имя (по умолчанию, используется IP/DNS узла сети)<br>**порт** - номер порта (по умолчанию для сервиса используется стандартный номер порта).|Пример:<br>=> net.udp.service.perf\[ntp\] → можно использовать для тестирования времени ответа от NTP сервиса.<br><br>Этот элемент данных поддерживается начиная с Zabbix 3.0, но *ntp* сервис был доступен в net.tcp.service\[\] элементе данных и в предыдущих версиях.|

[comment]: # ({/new-f59a5ccc})

[comment]: # ({new-23d7246f})

### Item key details

[comment]: # ({/new-23d7246f})

[comment]: # ({new-7d8dff2e})

##### icmpping\[<target>,<packets>,<interval>,<size>,<timeout>,<options>\] {#icmpping}

<br>
The host accessibility by ICMP ping.<br>
Return value: *0* - ICMP ping fails; *1* - ICMP ping successful.

Parameters:

-   **target** - the host IP or DNS name;
-   **packets** - the number of packets;
-   **interval** - the time between successive packets in milliseconds;
-   **size** - the packet size in bytes;
-   **timeout** - the timeout in milliseconds;
-   **options** - used for allowing redirect: if empty (default value), redirected responses are treated as target host down; if set to *allow_redirect*, redirected responses are treated as target host up.

See also the table of [default values](#default-values).

Example:

    icmpping[,4] #If at least one packet of the four is returned, the item will return 1.

[comment]: # ({/new-7d8dff2e})

[comment]: # ({new-6cd0f1b1})

##### icmppingloss\[<target>,<packets>,<interval>,<size>,<timeout>,<options>\] {#icmppingloss}

<br>
The percentage of lost packets.<br>
Return value: *Float*.

Parameters:

-   **target** - the host IP or DNS name;
-   **packets** - the number of packets;
-   **interval** - the time between successive packets in milliseconds;
-   **size** - the packet size in bytes;
-   **timeout** - the timeout in milliseconds;
-   **options** - used for allowing redirect: if empty (default value), redirected responses are treated as target host down; if set to *allow_redirect*, redirected responses are treated as target host up.

See also the table of [default values](#default-values).

[comment]: # ({/new-6cd0f1b1})

[comment]: # ({new-b5636838})

##### icmppingsec\[<target>,<packets>,<interval>,<size>,<timeout>,<mode>,<options>\] {#icmppingsec}

<br>
The ICMP ping response time (in seconds).<br>
Return value: *Float*.

Parameters:

-   **target** - the host IP or DNS name;
-   **packets** - the number of packets;
-   **interval** - the time between successive packets in milliseconds;
-   **size** - the packet size in bytes;
-   **timeout** - the timeout in milliseconds;
-   **mode** - possible values: *min*, *max*, or *avg* (default);
-   **options** - used for allowing redirect: if empty (default value), redirected responses are treated as target host down; if set to *allow_redirect*, redirected responses are treated as target host up.

Comments:

-   Packets which are lost or timed out are not used in the calculation;
-   If the host is not available (timeout reached), the item will return 0;
-   If the return value is less than 0.0001 seconds, the value will be set to 0.0001 seconds;
-   See also the table of [default values](#default-values).

[comment]: # ({/new-b5636838})

[comment]: # ({new-b0a71170})

##### net.tcp.service[service,<ip>,<port>] {#nettcpservice}

<br>
Checks if a service is running and accepting TCP connections.<br>
Return value: *0* - the service is down; *1* - the service is running.

Parameters:

-   **service** - possible values: *ssh*, *ldap*, *smtp*, *ftp*, *http*, *pop*, *nntp*, *imap*, *tcp*, *https*, *telnet* (see [details](/manual/appendix/items/service_check_details));
-   **ip** - the IP address or DNS name (by default the host IP/DNS is used);
-   **port** - the port number (by default the standard service port number is used).

Comments:

-   Note that with *tcp* service indicating the port is mandatory;
-   These checks may result in additional messages in system daemon logfiles (SMTP and SSH sessions being logged usually);
-   Checking of encrypted protocols (like IMAP on port 993 or POP on port 995) is currently not supported. As a workaround, please use `net.tcp.service[tcp,<ip>,port]` for checks like these.

Example:

    net.tcp.service[ftp,,45] #This item can be used to test the availability of FTP server on TCP port 45.

[comment]: # ({/new-b0a71170})

[comment]: # ({new-946385b4})

##### net.tcp.service.perf[service,<ip>,<port>] {#nettcpserviceperf}

<br>
Checks the performance of a TCP service.<br>
Return value: *Float*: *0.000000* - the service is down; *seconds* - the number of seconds spent while connecting to the service.

Parameters:

-   **service** - possible values: *ssh*, *ldap*, *smtp*, *ftp*, *http*, *pop*, *nntp*, *imap*, *tcp*, *https*, *telnet* (see [details](/manual/appendix/items/service_check_details));
-   **ip** - the IP address or DNS name (by default the host IP/DNS is used);
-   **port** - the port number (by default the standard service port number is used).

Comments:

-   Note that with *tcp* service indicating the port is mandatory;
-   Checking of encrypted protocols (like IMAP on port 993 or POP on port 995) is currently not supported. As a workaround, please use `net.tcp.service[tcp,<ip>,port]` for checks like these.

Example:

    net.tcp.service.perf[ssh] #This item can be used to test the speed of initial response from SSH server.

[comment]: # ({/new-946385b4})

[comment]: # ({new-2859223a})

##### net.udp.service[service,<ip>,<port>] {#netudpservice}

<br>
Checks if a service is running and responding to UDP requests.<br>
Return value: *0* - the service is down; *1* - the service is running.

Parameters:

-   **service** - possible values: *ntp* (see [details](/manual/appendix/items/service_check_details));
-   **ip** - the IP address or DNS name (by default the host IP/DNS is used);
-   **port** - the port number (by default the standard service port number is used).

Example:

    net.udp.service[ntp,,45] #This item can be used to test the availability of NTP service on UDP port 45.

[comment]: # ({/new-2859223a})

[comment]: # ({new-9cf5922c})

##### net.udp.service.perf[service,<ip>,<port>] {#netudpserviceperf}

<br>
Checks the performance of a UDP service.<br>
Return value: *Float*: *0.000000* - the service is down; *seconds* - the number of seconds spent waiting for response from the service.

Parameters:

-   **service** - possible values: *ntp* (see [details](/manual/appendix/items/service_check_details));
-   **ip** - the IP address or DNS name (by default the host IP/DNS is used);
-   **port** - the port number (by default the standard service port number is used).

Example:

    net.udp.service.perf[ntp] #This item can be used to test the response time from NTP service.

[comment]: # ({/new-9cf5922c})

[comment]: # ({new-85612a2b})
  
*Note* that for SourceIP support in LDAP simple checks, OpenLDAP version 2.6.1 or above is required.

[comment]: # ({/new-85612a2b})

[comment]: # ({new-92a6c4ae})
##### Обработка времени ожидания

Zabbix не будет обрабатывать простую проверку дольше Timeout (времени
ожидания) секунд, заданных в файле конфигурации Zabbix сервера/прокси.

[comment]: # ({/new-92a6c4ae})

[comment]: # ({new-b08a4508})
#### ICMP пинг

Для обработки ICMP пинг Zabbix использует внешнюю утилиту **fping**.

Эта утилита не является частью дистрибутива Zabbix и должна быть
установлена дополнительно. Если утилиты нет, у нее выставлены неверные
разрешения и её размещение не совпадает с размещением заданным в файле
конфигурации Zabbix сервера/прокси (параметры 'FpingLocation'), ICMP
пинг (**icmpping**, **icmppingloss**, **icmppingsec**) не будет
обрабатываться.

Смотрите также: [известные
проблемы](/ru/manual/installation/known_issues#простые_проверки)

**fping** должен быть выполняемым под пользователем Zabbix демонов и
должен иметь setuid root. Выполните эти команды из под **root** для
выставления корректных разрешений:

    shell> chown root:zabbix /usr/sbin/fping
    shell> chmod 4710 /usr/sbin/fping

После выполнения этих двух команд выше проверьте владельца исполняемого
файла **fping**. В некоторых случаях владелец может сброситься при
выполнении chmod команды.

Также проверьте, принадлежит ли пользователь zabbix к группе zabbix,
запустив команду:

    shell> groups zabbix

и если нет добавьте следующей командой:

    shell> usermod -a -G zabbix zabbix

Значения по умолчанию, ограничения и описания значений для параметров
ICMP проверок:

|Параметр|Ед. изм|Описание|Флаг у fping|Значения по умолчанию у|<|Разрешенные ограничения<br>в Zabbix|<|
|----------------|------------|----------------|-----------------|-------------------------------------------|-|----------------------------------------------------------|-|

::: notewarning
Предупреждение: Значения по умолчанию для fping
могут различаться в зависимости от платформы и версии - если
сомневаетесь, проверьте документацию по fping.
:::

Zabbix записывает проверяемые IP адреса во временный файл по всем трем
*icmpping\** ключам, который затем передается утилите **fping**. Если
элементы данных имеют различные параметры ключа, то только элементы
данных с идентичными параметрами ключа записываются в один файл.\
Все записанные в один файл IP адреса проверяются fping утилитой в
параллельном режиме, таким образом процесс Zabbix icmp pinger тратит
фиксированное время вне зависимости от количества IP адресов в файле.

[comment]: # ({/new-b08a4508})

[comment]: # ({new-e3dd9826})

##### Installation

fping is not included with Zabbix and needs to be installed separately:

- Various Unix-based platforms have the fping package in their default repositories, but it is not pre-installed. In this case you can use the package manager to install fping.

- Zabbix provides [fping packages](http://repo.zabbix.com/non-supported/rhel/) for RHEL. Please note that these packages are provided without official support.

- fping can also be compiled [from source](https://github.com/schweikert/fping/blob/develop/README.md#installation).

[comment]: # ({/new-e3dd9826})

[comment]: # ({new-9ced6345})

##### Configuration

Specify fping location in the *[FpingLocation](/manual/appendix/config/zabbix_server#fpinglocation)* parameter of Zabbix server/proxy configuration file 
(or *[Fping6Location](/manual/appendix/config/zabbix_server#fping6location)* parameter for using IPv6 addresses).

fping should be executable by the user Zabbix server/proxy run as and this user should have sufficient rights.

See also: [Known issues](/manual/installation/known_issues#simple_checks) for processing simple checks with fping versions below 3.10.

[comment]: # ({/new-9ced6345})

[comment]: # ({new-3c21487d})

##### Default values

Defaults, limits and description of values for ICMP check parameters:

|Parameter|Unit|Description|Fping's flag|Defaults set by|<|Allowed limits<br>by Zabbix|<|
|--|--|--------|-|--|--|--|--|
|||||**fping**|**Zabbix**|**min**|**max**|
|packets|number|number of request packets sent to a target|-C||3|1|10000|
|interval|milliseconds|time to wait between successive packets to an individual target|-p|1000||20|unlimited|
|size|bytes|packet size in bytes<br>56 bytes on x86, 68 bytes on x86_64|-b|56 or 68||24|65507|
|timeout|milliseconds|**fping v3.x** - timeout to wait after last packet sent, affected by *-C* flag<br> **fping v4.x** - individual timeout for each packet|-t|**fping v3.x** - 500<br>**fping v4.x** and newer - inherited from *-p* flag, but not more than 2000||50|unlimited|

The defaults may differ slightly depending on the platform and version.

In addition, Zabbix uses fping options *-i interval ms* (do not mix up with the item parameter *interval* mentioned in the table above,
which corresponds to fping option *-p*) and *-S source IP address* (or *-I* in older fping versions).
These options are auto-detected by running checks with different option combinations.
Zabbix tries to detect the minimal value in milliseconds that fping allows to use with *-i* by trying 3 values: 0, 1 and 10.
The value that first succeeds is then used for subsequent ICMP checks.
This process is done by each [ICMP pinger](/manual/concepts/server#server_process_types) process individually.

Auto-detected fping options are invalidated every hour and detected again on the next attempt to perform ICMP check.
Set [DebugLevel](/manual/appendix/config/zabbix_server#debuglevel)>=4 in order to view details of this process in the server or proxy log file.

Zabbix writes IP addresses to be checked by any of the three *icmpping\** keys to a temporary file, which is then passed to fping.
If items have different key parameters, only the ones with identical key parameters are written to a single file.
All IP addresses written to the single file will be checked by fping in parallel,
so Zabbix ICMP pinger process will spend fixed amount of time disregarding the number of IP addresses in the file.

[comment]: # ({/new-3c21487d})
