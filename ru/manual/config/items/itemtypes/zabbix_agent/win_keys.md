[comment]: # attributes: notoc

[comment]: # translation:outdated

[comment]: # ({new-071dd107})
# Специфичные ключи элементов данных для Windows

[comment]: # ({/new-071dd107})

[comment]: # ({new-2356bb2e})

### Windows-specific items

The table provides details on the item keys that are supported **only** 
by Zabbix Windows agent.

|Key|<|<|<|<|
|---|-|-|-|-|
|<|Description|Return value|Parameters|Comments|
|eventlog\[name,<regexp>,<severity>,<source>,<eventid>,<maxlines>,<mode>\]|<|<|<|<|
|<|Event log monitoring.|Log|**name** - name of event log<br>**regexp** - regular expression describing the required pattern<br>**severity** - regular expression describing severity<br>This parameter accepts the following values: "Information", "Warning", "Error", "Critical", "Verbose" (since Zabbix 2.2.0 running on Windows Vista or newer)<br>**source** - regular expression describing source identifier (regular expression is supported since Zabbix 2.2.0)<br>**eventid** - regular expression describing the event identifier(s)<br>**maxlines** - maximum number of new lines per second the agent will send to Zabbix server or proxy. This parameter overrides the value of 'MaxLinesPerSecond' in [zabbix\_agentd.win.conf](/manual/appendix/config/zabbix_agentd_win)<br>**mode** - possible values:<br>*all* (default), *skip* - skip processing of older data (affects only newly created items).|The item must be configured as an [active check](/manual/appendix/items/activepassive#active_checks).<br><br>Examples:<br>=> eventlog\[Application\]<br>=> eventlog\[Security,,"Failure Audit",,\^(529\|680)$\]<br>=> eventlog\[System,,"Warning\|Error"\]<br>=> eventlog\[System,,,,\^1$\]<br>=> eventlog\[System,,,,\@TWOSHORT\] - here a [custom regular expression](/manual/regular_expressions) named `TWOSHORT` is referenced (defined as a *Result is TRUE* type, the expression itself being `^1$\|^70$`).<br><br>*Note* that the agent is unable to send in events from the "Forwarded events" log.<br><br>The `mode` parameter is supported since Zabbix 2.0.0.<br>"Windows Eventing 6.0" is supported since Zabbix 2.2.0.<br><br>Note that selecting a non-Log [type of information](/manual/config/items/item#configuration) for this item will lead to the loss of local timestamp, as well as log severity and source information.<br><br>See also additional information on [log monitoring](/manual/config/items/itemtypes/log_items).|
|net.if.list|<|<|<|<|
|<|Network interface list (includes interface type, status, IPv4 address, description).|Text|<|Supported since Zabbix agent version 1.8.1. Multi-byte interface names supported since Zabbix agent version 1.8.6. Disabled interfaces are not listed.<br><br>Note that enabling/disabling some components may change their ordering in the Windows interface name.<br><br>Some Windows versions (for example, Server 2008) might require the latest updates installed to support non-ASCII characters in interface names.|
|perf\_counter\[counter,<interval>\]|<|<|<|<|
|<|Value of any Windows performance counter.|Integer, float, string or text (depending on the request)|**counter** - path to the counter<br>**interval** - last N seconds for storing the average value.<br>The `interval` must be between 1 and 900 seconds (included) and the default value is 1.|Performance Monitor can be used to obtain list of available counters. Until version 1.6 this parameter will return correct value only for counters that require just one sample (like \\System\\Threads). It will not work as expected for counters that require more than one sample - like CPU utilization. Since 1.6, `interval` is used, so the check returns an average value for last "interval" seconds every time.<br><br>See also: [Windows performance counters](/manual/config/items/perfcounters).|
|perf\_counter\_en\[counter,<interval>\]|<|<|<|<|
|<|Value of any Windows performance counter in English.|Integer, float, string or text (depending on the request)|**counter** - path to the counter in English<br>**interval** - last N seconds for storing the average value.<br>The `interval` must be between 1 and 900 seconds (included) and the default value is 1.|This item is only supported on **Windows Server 2008/Vista** and above.<br><br>You can find the list of English strings by viewing the following registry key: `HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Perflib\009`.<br><br>Supported since Zabbix agent versions 4.0.13 and 4.2.7.|
|perf\_instance.discovery\[object\]|<|<|<|<|
|<|List of object instances of Windows performance counters. Used for [low-level discovery](/manual/discovery/low_level_discovery/examples/windows_perf_instances).|JSON object|**object** - object name (localized)|Supported since Zabbix agent version 5.0.1.|
|perf\_instance\_en.discovery\[object\]|<|<|<|<|
|<|List of object instances of Windows performance counters, discovered using object names in English. Used for [low-level discovery](/manual/discovery/low_level_discovery/examples/windows_perf_instances).|JSON object|**object** - object name (in English)|Supported since Zabbix agent version 5.0.1.|
|proc\_info\[process,<attribute>,<type>\]|<|<|<|<|
|<|Various information about specific process(es).|Float|**process** - process name<br>**attribute** - requested process attribute<br>**type** - representation type (meaningful when more than one process with the same name exists)|The following `attributes` are supported:<br>*vmsize* (default) - size of process virtual memory in Kbytes<br>*wkset* - size of process working set (amount of physical memory used by process) in Kbytes<br>*pf* - number of page faults<br>*ktime* - process kernel time in milliseconds<br>*utime* - process user time in milliseconds<br>*io\_read\_b* - number of bytes read by process during I/O operations<br>*io\_read\_op* - number of read operation performed by process<br>*io\_write\_b* - number of bytes written by process during I/O operations<br>*io\_write\_op* - number of write operation performed by process<br>*io\_other\_b* - number of bytes transferred by process during operations other than read and write operations<br>*io\_other\_op* - number of I/O operations performed by process, other than read and write operations<br>*gdiobj* - number of GDI objects used by process<br>*userobj* - number of USER objects used by process<br><br>Valid `types` are:<br>*avg* (default) - average value for all processes named <process><br>*min* - minimum value among all processes named <process><br>*max* - maximum value among all processes named <process><br>*sum* - sum of values for all processes named <process><br><br>Examples:<br>=> proc\_info\[iexplore.exe,wkset,sum\] - to get the amount of physical memory taken by all Internet Explorer processes<br>=> proc\_info\[iexplore.exe,pf,avg\] - to get the average number of page faults for Internet Explorer processes<br><br>Note that on a 64-bit system, a 64-bit Zabbix agent is required for this item to work correctly.<br><br>Note: *io\_\**, *gdiobj* and *userobj* attributes are available only on Windows 2000 and later versions of Windows, not on Windows NT 4.0.|
|service.discovery|<|<|<|<|
|<|List of Windows services. Used for [low-level discovery](/manual/discovery/low_level_discovery/examples/windows_services).|JSON object|<|Supported since Zabbix agent version 3.0.|
|service.info\[service,<param>\]|<|<|<|<|
|<|Information about a service.|Integer - with `param` as *state*, *startup*<br><br>String - with `param` as *displayname*, *path*, *user*<br><br>Text - with `param` as *description*<br><br>Specifically for *state*:<br>0 - running,<br>1 - paused,<br>2 - start pending,<br>3 - pause pending,<br>4 - continue pending,<br>5 - stop pending,<br>6 - stopped,<br>7 - unknown,<br>255 - no such service<br><br>Specifically for *startup*:<br>0 - automatic,<br>1 - automatic delayed,<br>2 - manual,<br>3 - disabled,<br>4 - unknown,<br>5 - automatic trigger start,<br>6 - automatic delayed trigger start,<br>7 - manual trigger start|**service** - a real service name or its display name as seen in MMC Services snap-in<br>**param** - *state* (default), *displayname*, *path*, *user*, *startup* or *description*|Examples:<br>=> service.info\[SNMPTRAP\] - state of the SNMPTRAP service<br>=> service.info\[SNMP Trap\] - state of the same service, but with display name specified<br>=> service.info\[EventLog,startup\] - startup type of the EventLog service<br><br>Items service.info\[service,state\] and service.info\[service\] will return the same information.<br><br>Note that only with `param` as *state* this item returns a value for non-existing services (255).<br><br>This item is supported since Zabbix 3.0.0. It should be used instead of the deprecated service\_state\[service\] item.|
|services\[<type>,<state>,<exclude>\]|<|<|<|<|
|<|Listing of services.|0 - if empty<br><br>Text - list of services separated by a newline|**type** - *all* (default), *automatic*, *manual* or *disabled*<br>**state** - *all* (default), *stopped*, *started*, *start\_pending*, *stop\_pending*, *running*, *continue\_pending*, *pause\_pending* or *paused*<br>**exclude** - services to exclude from the result. Excluded services should be listed in double quotes, separated by comma, without spaces.|Examples:<br>=> services\[,started\] - list of started services<br>=> services\[automatic, stopped\] - list of stopped services, that should be run<br>=> services\[automatic, stopped, "service1,service2,service3"\] - list of stopped services, that should be run, excluding services with names service1, service2 and service3<br><br>The `exclude` parameter is supported since Zabbix 1.8.1.|
|wmi.get\[<namespace>,<query>\]|<|<|<|<|
|<|Execute WMI query and return the first selected object.|Integer, float, string or text (depending on the request)|**namespace** - WMI namespace<br>**query** - WMI query returning a single object<br>|WMI queries are performed with [WQL](https://en.wikipedia.org/wiki/WQL).<br><br>Example:<br>=> wmi.get\[root\\cimv2,select status from Win32\_DiskDrive where Name like '%PHYSICALDRIVE0%'\] - returns the status of the first physical disk<br><br>This key is supported since Zabbix 2.2.0.|
|wmi.getall\[<namespace>,<query>\]|<|<|<|<|
|<|Execute WMI query and return the whole response.<br><br>Can be used for [low-level discovery](/manual/discovery/low_level_discovery/examples/wmi).|JSON object|**namespace** - WMI namespace<br>**query** - WMI query<br>|WMI queries are performed with [WQL](https://en.wikipedia.org/wiki/WQL).<br><br>Example:<br>=> wmi.getall\[root\\cimv2,select \* from Win32\_DiskDrive where Name like '%PHYSICALDRIVE%'\] - returns status information of physical disks<br><br>JSONPath [preprocessing](/manual/config/items/item#item_value_preprocessing) can be used to point to more specific values in the returned JSON.<br><br>This key is supported since Zabbix 4.4.0.|
|vm.vmemory.size\[<type>\]|<|<|<|<|
|<|Virtual memory size in bytes or in percentage from total.|Integer - for bytes<br><br>Float - for percentage|**type** - possible values:<br>*available* (available virtual memory), *pavailable* (available virtual memory, in percent), *pused* (used virtual memory, in percent), *total* (total virtual memory, default), *used* (used virtual memory)|Example:<br>=> vm.vmemory.size\[pavailable\] → available virtual memory, in percentage<br><br>Monitoring of virtual memory statistics is based on:<br>\* Total virtual memory on Windows (total physical + page file size);<br>\* The maximum amount of memory Zabbix agent can commit;<br>\* The current committed memory limit for the system or Zabbix agent, whichever is smaller.<br><br>This key is supported since Zabbix 3.0.7 and 3.2.3.|

[comment]: # ({/new-2356bb2e})

[comment]: # ({new-3077d649})
### Item key details

Parameters without angle brackets are mandatory. Parameters marked with angle brackets **<** **>** are optional.

[comment]: # ({/new-3077d649})

[comment]: # ({new-ea4d4b73})

##### eventlog[name,<regexp>,<severity>,<source>,<eventid>,<maxlines>,<mode>]

<br>
The event log monitoring.<br>
Return value: *Log*.

Parameters:

-   **name** - the name of the event log;<br>
-   **regexp** - a regular [expression](/manual/regular_expressions#overview) describing the required pattern (case sensitive);<br>
-   **severity** - a regular expression describing severity (case insensitive). This parameter accepts the following values: "Information", "Warning", "Error", "Critical", "Verbose" (running on Windows Vista or newer).<br>
-   **source** - a regular expression describing the source identifier (case insensitive);<br>
-   **eventid** - a regular expression describing the event identifier(s) (case sensitive);<br>
-   **maxlines** - the maximum number of new lines per second the agent will send to Zabbix server or proxy. This parameter overrides the value of 'MaxLinesPerSecond' in [zabbix\_agentd.win.conf](/manual/appendix/config/zabbix_agentd_win).<br>
-   **mode** - possible values: *all* (default) or *skip* - skip the processing of older data (affects only newly created items).

Comments:

-   The item must be configured as an [active check](/manual/appendix/items/activepassive#active_checks);
-   The agent is unable to send in events from the "Forwarded events" log;
-   Windows Eventing 6.0 is supported;
-   Selecting a non-Log [type of information](/manual/config/items/item#configuration) for this item will lead to the loss of local timestamp, as well as log severity and source information;
-   See also additional information on [log monitoring](/manual/config/items/itemtypes/log_items).

Examples:

    eventlog[Application]
    eventlog[Security,,"Failure Audit",,^(529|680)$]
    eventlog[System,,"Warning|Error"]
    eventlog[System,,,,^1$]
    eventlog[System,,,,@TWOSHORT] #here a custom regular expression named `TWOSHORT` is referenced (defined as a *Result is TRUE* type, the expression itself being `^1$|^70$`).
    

[comment]: # ({/new-ea4d4b73})

[comment]: # ({new-2304e84f})

##### net.if.list

<br>
The network interface list (includes interface type, status, IPv4 address, description).<br>
Return value: *Text*.

Comments:

-   Multi-byte interface names supported;
-   Disabled interfaces are not listed;
-   Enabling/disabling some components may change their ordering in the Windows interface name;
-   Some Windows versions (for example, Server 2008) might require the latest updates installed to support non-ASCII characters in interface names.

[comment]: # ({/new-2304e84f})

[comment]: # ({new-9a990f49})

##### perf_counter[counter,<interval>]

<br>
The value of any Windows performance counter.<br>
Return value: *Integer*, *float*, *string* or *text* (depending on the request).

Parameters:

-   **counter** - the path to the counter;<br>
-   **interval** - the last N seconds for storing the average value. The `interval` must be between 1 and 900 seconds (included) and the default value is 1.

Comments:

-   `interval` is used for counters that require more than one sample (like CPU utilization), so the check returns an average value for last "interval" seconds every time;
-   Performance Monitor can be used to obtain the list of available counters. 
-   See also: [Windows performance counters](/manual/config/items/perfcounters).

[comment]: # ({/new-9a990f49})

[comment]: # ({new-68dc3c35})

##### perf_counter_en[counter,<interval>]

<br>
The value of any Windows performance counter in English.<br>
Return value: *Integer*, *float*, *string* or *text* (depending on the request).

Parameters:

-   **counter** - the path to the counter in English;<br>
-   **interval** - the last N seconds for storing the average value. The `interval` must be between 1 and 900 seconds (included) and the default value is 1.

Comments:

-   `interval` is used for counters that require more than one sample (like CPU utilization), so the check returns an average value for last "interval" seconds every time;
-   This item is only supported on **Windows Server 2008/Vista** and above;
-   You can find the list of English strings by viewing the following registry key: `HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Perflib\009`.

[comment]: # ({/new-68dc3c35})

[comment]: # ({new-f4ce8a45})

##### perf_instance.discovery[object]

<br>
The list of object instances of Windows performance counters. Used for [low-level discovery](/manual/discovery/low_level_discovery/examples/windows_perf_instances).<br>
Return value: *JSON object*.

Parameter:

-   **object** - the object name (localized).

[comment]: # ({/new-f4ce8a45})

[comment]: # ({new-817f7111})

##### perf_instance_en.discovery[object]

<br>
The list of object instances of Windows performance counters, discovered using the object names in English. Used for [low-level discovery](/manual/discovery/low_level_discovery/examples/windows_perf_instances).<br>
Return value: *JSON object*.

Parameter:

-   **object** - the object name (in English).

[comment]: # ({/new-817f7111})

[comment]: # ({new-36e50fdd})

##### proc_info[process,<attribute>,<type>]

<br>
Various information about specific process(es).<br>
Return value: *Float*.

Parameters:

-   **process** - the process name;<br>
-   **attribute** - the requested process attribute;<br>
-   **type** - the representation type (meaningful when more than one process with the same name exists)

Comments:

-   The following `attributes` are supported:<br>*vmsize* (default) - size of process virtual memory in Kbytes<br>*wkset* - size of process working set (amount of physical memory used by process) in Kbytes<br>*pf* - number of page faults<br>*ktime* - process kernel time in milliseconds<br>*utime* - process user time in milliseconds<br>*io\_read\_b* - number of bytes read by process during I/O operations<br>*io\_read\_op* - number of read operation performed by process<br>*io\_write\_b* - number of bytes written by process during I/O operations<br>*io\_write\_op* - number of write operation performed by process<br>*io\_other\_b* - number of bytes transferred by process during operations other than read and write operations<br>*io\_other\_op* - number of I/O operations performed by process, other than read and write operations<br>*gdiobj* - number of GDI objects used by process<br>*userobj* - number of USER objects used by process;<br>
-   Valid `types` are:<br>*avg* (default) - average value for all processes named <process><br>*min* - minimum value among all processes named <process><br>*max* - maximum value among all processes named <process><br>*sum* - sum of values for all processes named <process>;
-   *io\_\**, *gdiobj* and *userobj* attributes are available only on Windows 2000 and later versions of Windows, not on Windows NT 4.0;
-   On a 64-bit system, a 64-bit Zabbix agent is required for this item to work correctly.<br>

Examples:

    proc_info[iexplore.exe,wkset,sum] #retrieve the amount of physical memory taken by all Internet Explorer processes
    proc_info[iexplore.exe,pf,avg] #retrieve the average number of page faults for Internet Explorer processes

[comment]: # ({/new-36e50fdd})

[comment]: # ({new-ff3c15dc})

##### registry.data[key,<value name>]

<br>
Return data for the specified value name in the Windows Registry key.<br>
Return value: *Integer*, *string* or *text* (depending on the value type)

Parameters:

-   **key** - the registry key including the root key; root abbreviations (e.g. HKLM) are allowed;
-   **value name** - the registry value name in the key (empty string "" by default). The default value is returned if the value name is not supplied.

Comments:

-   Supported root abbreviations:<br>HKCR - HKEY_CLASSES_ROOT<br>HKCC - HKEY_CURRENT_CONFIG<br>HKCU - HKEY_CURRENT_USER<br>HKCULS - HKEY_CURRENT_USER_LOCAL_SETTINGS<br>HKLM - HKEY_LOCAL_MACHINE<br>HKPD - HKEY_PERFORMANCE_DATA<br>HKPN - HKEY_PERFORMANCE_NLSTEXT<br>HKPT - HKEY_PERFORMANCE_TEXT<br>HKU - HKEY_USERS<br>
-   Keys with spaces must be double-quoted.

Examples:

    registry.data["HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\Windows Error Reporting"] #return the data of the default value of this key
    registry.data["HKLM\SOFTWARE\Microsoft\Windows\Windows Error Reporting","EnableZip"] #return the data of the value named "Enable Zip" in this key

[comment]: # ({/new-ff3c15dc})

[comment]: # ({new-e1a87947})

##### registry.get[key,<mode>,<name regexp>]

<br>
The list of Windows Registry values or keys located at given key.<br>
Return value: *JSON object*.

Parameters:

-   **key** - the registry key including the root key; root abbreviations (e.g. HKLM) are allowed (see comments for registry.data\[\] to see full list of abbreviations);<br>
-   **mode** - possible values:<br>*values* (default) or *keys*;<br>
-   **name regexp** - only discover values with names that match the regexp (default - discover all values). Allowed only with *values* as `mode`.

Keys with spaces must be double-quoted.

Examples:

    registry.get[HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall,values,"^DisplayName|DisplayVersion$"] #return the data of the values named "DisplayName" or "DisplayValue" in this key. The JSON will include details of the key, last subkey, value name, value type and value data.
    registry.get[HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall,values] #return the data of the all values in this key. The JSON will include details of the key, last subkey, value name, value type and value data.
    registry.get[HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall,keys] #return all subkeys of this key. The JSON will include details of the key and last subkey.

[comment]: # ({/new-e1a87947})

[comment]: # ({new-e7b67f01})

##### service.discovery

<br>
The list of Windows services. Used for [low-level discovery](/manual/discovery/low_level_discovery/examples/windows_services).<br>
Return value: *JSON object*.

[comment]: # ({/new-e7b67f01})

[comment]: # ({new-f01b9c46})

##### service.info[service,<param>]

<br>
Information about a service.<br>
Return value: *Integer* - with `param` as *state*, *startup*; *String* - with `param` as *displayname*, *path*, *user*; *Text* - with `param` as *description*<br>Specifically for *state*: 0 - running, 1 - paused, 2 - start pending, 3 - pause pending, 4 - continue pending, 5 - stop pending, 6 - stopped, 7 - unknown, 255 - no such service<br>Specifically for *startup*: 0 - automatic, 1 - automatic delayed, 2 - manual, 3 - disabled, 4 - unknown, 5 - automatic trigger start, 6 - automatic delayed trigger start, 7 - manual trigger start

Parameters:

-   **service** - a real service name or its display name as seen in the MMC Services snap-in;
-   **param** - *state* (default), *displayname*, *path*, *user*, *startup*, or *description*.

Comments:

-   Items like `service.info[service,state]` and `service.info[service]` will return the same information;
-   Only with `param` as *state* this item returns a value for non-existing services (255).

Examples:

    service.info[SNMPTRAP] - state of the SNMPTRAP service;
    service.info[SNMP Trap] - state of the same service, but with the display name specified;
    service.info[EventLog,startup] - the startup type of the EventLog service

[comment]: # ({/new-f01b9c46})

[comment]: # ({new-2d4eec00})

##### services[<type>,<state>,<exclude>]

<br>
The listing of services.<br>
Return value: *0* - if empty; *Text* - the list of services separated by a newline.

Parameters:

-   **type** - *all* (default), *automatic*, *manual*, or *disabled*;
-   **state** - *all* (default), *stopped*, *started*, *start_pending*, *stop_pending*, *running*, *continue_pending*, *pause_pending*, or *paused*;
-   **exclude** - the services to exclude from the result. Excluded services should be listed in double quotes, separated by comma, without spaces.

Examples:

    services[,started] #returns the list of started services;
    services[automatic, stopped] #returns the list of stopped services that should be running;
    services[automatic, stopped, "service1,service2,service3"] #returns the list of stopped services that should be running, excluding services named "service1", "service2" and "service3"

[comment]: # ({/new-2d4eec00})

[comment]: # ({new-ef131dd0})

##### vm.vmemory.size[<type>]

<br>
The virtual memory size in bytes or in percentage from the total.<br>
Return value: *Integer* - for bytes; *float* - for percentage.

Parameter:

-   **type** - possible values: *available* (available virtual memory), *pavailable* (available virtual memory, in percent), *pused* (used virtual memory, in percent), *total* (total virtual memory, default), or *used* (used virtual memory)

Comments:

-   The monitoring of virtual memory statistics is based on:<br>
    -   Total virtual memory on Windows (total physical + page file size);<br>
    -   The maximum amount of memory Zabbix agent can commit;<br>
    -   The current committed memory limit for the system or Zabbix agent, whichever is smaller.

Example:

    vm.vmemory.size[pavailable] #return the available virtual memory, in percentage

[comment]: # ({/new-ef131dd0})

[comment]: # ({new-a80553ac})

##### wmi.get[<namespace>,<query>]

<br>
Execute a WMI query and return the first selected object.<br>
Return value: *Integer*, *float*, *string* or *text* (depending on the request).

Parameters:

-   **namespace** - the WMI namespace;<br>
-   **query** - the WMI query returning a single object.

WMI queries are performed with [WQL](https://en.wikipedia.org/wiki/WQL).

Example:

    wmi.get[root\cimv2,select status from Win32_DiskDrive where Name like '%PHYSICALDRIVE0%'] #returns the status of the first physical disk

[comment]: # ({/new-a80553ac})

[comment]: # ({new-8c19f9d3})

##### wmi.getall[<namespace>,<query>]

<br>
Execute a WMI query and return the whole response. Can be used for [low-level discovery](/manual/discovery/low_level_discovery/examples/wmi).<br>
Return value: *JSON object*

Parameters:

-   **namespace** - the WMI namespace;<br>
-   **query** - the WMI query.

Comments:

-   WMI queries are performed with [WQL](https://en.wikipedia.org/wiki/WQL).
-   JSONPath [preprocessing](/manual/config/items/item#item_value_preprocessing) can be used to point to more specific values in the returned JSON.

Example:

    wmi.getall[root\cimv2,select * from Win32_DiskDrive where Name like '%PHYSICALDRIVE%'] #returns status information of physical disks

[comment]: # ({/new-8c19f9d3})

[comment]: # ({new-c2b4f623})
#### Мониторинг служб Windows

Это руководство содержит пошаговые инструкции по настройке мониторинга
служб Windows. Предполагается, что Zabbix сервер и агент уже настроены и
работают.

[comment]: # ({/new-c2b4f623})

[comment]: # ({new-110cbb70})
##### Шаг 1

Узнайте имя службы.

Вы можете получить имя, перейдя в оснастку MMC Службы и открыв свойства
службы. На вкладке Общие вы должны увидеть поле называемое 'Имя службы'.
Значение которого и будет именем желаемой службы, которое вы будете
использовать при настройке элемента данных для наблюдения.

Например, если вы хотите наблюдать службу "workstation", то ваша служба
скорее всего будет: **lanmanworkstation**.

[comment]: # ({/new-110cbb70})

[comment]: # ({new-d5dd9159})
##### Шаг 2

[Настройте элемент данных](/ru/manual/config/items/item) для наблюдения
за службой.

Элемент данных service.info\[служба,<парам>\] возвращает
информацию о указанной службе. В зависимости от требемой вам информации,
укажите опцию *парам*, которая принимает следующие значения:
*displayname*, *state*, *path*, *user*, *startup* или *description*.
Значением по умолчанию является *state*, если *парам* не указан
(service.info\[служба\]).

Тип возвращаемого значения зависит от выбранного *парам*: целое число
при *state* и *startup*; строка символов при *displayname*, *path* и
*user*; текст при *description*.

Пример:

-   *Ключ*: serfice.info\[lanmanworkstation\]
-   *Тип информации*: Целочисленное (положительное)
-   *Отображение значений*: выберите преобразование значений *Windows
    service state*

Имеется два преобразования значений *Windows service state* и *Windows
service startup type*, которые сопоставляют числовое значение в
веб-интерфейсе его текстовому представлению.

[comment]: # ({/new-d5dd9159})

[comment]: # ({new-bee949c4})
#### Обнаружение служб Windows

[Низкоуровневое обнаружение](/ru/manual/discovery/low_level_discovery)
дает возможность автоматического создания элементов данных, триггеров и
графиков по различных объектам на компьютере. Zabbix может автоматически
начать наблюдение за службами Windows на вашей машине, без необходимости
знания точного имени службы или создания элементов данных по каждой
службе вручную. Можно использовать фильтр для генерирования реальных
элементов данных, триггеров и графиков только по интересующим службам.

[comment]: # ({/new-bee949c4})

[comment]: # ({new-bfb617f2})

### Overview

This section provides details on the Zabbix agent items that are 
supported on Windows. The supported items are presented in two tables:

-   [Agent items](agent-items) - the agent item keys **also** supported on Windows
-   [Windows-specific items](windows-specific-items) - the item keys that are supported **only** on Windows:
   -    eventlog/[/]
   -    net.if.list
   -    perf_counter/[/]
   -    perf_counter_en/[/]
   -    perf_instance.discovery/[/]
   -    proc_info/[/]
   -    service.discovery
   -    service.info/[/]
   -    services
   -    wmi.get/[/]
   -    wmi.getall/[/]
   -    vm.vmemory.size/[/]

Windows-specific items sometimes are an approximate counterpart of a similar 
agent item, for example `proc_info`, supported on Windows, roughly corresponds 
to the `proc.mem` item, not supported on Windows.

Note that all item keys supported by Zabbix agent on Windows are also supported by 
Zabbix agent 2. See [item keys supported by Zabbix agent 2](/manual/config/items/itemtypes/zabbix_agent/zabbix_agent2) 
for additional item keys that you can use with the agent 2 only.

See also: [Minimum permissions for Windows
items](/manual/appendix/items/win_permissions)

[comment]: # ({/new-bfb617f2})

[comment]: # ({new-52f3ae09})

### Agent items

The table below lists Zabbix agent items that are supported on Windows:

-   The item key is a link to full item details in the corresponding agent item group
-   The item key signature includes only those parameters that are supported on Windows
-   Windows-relevant item comments are included

|Key|Comments|
|---|--------|
|[agent.hostmetadata](/manual/config/items/itemtypes/zabbix_agent#zabbix-metrics) | |
|[agent.hostname](/manual/config/items/itemtypes/zabbix_agent#zabbix-metrics) | |
|[agent.ping](/manual/config/items/itemtypes/zabbix_agent#zabbix-metrics) | |
|[agent.variant](/manual/config/items/itemtypes/zabbix_agent#zabbix-metrics) | |
|[agent.version](/manual/config/items/itemtypes/zabbix_agent#zabbix-metrics) | |
|[log\[file,<regexp>,<encoding>,<maxlines>,<mode>,<output>,<maxdelay>,<options>\]](/manual/config/items/itemtypes/zabbix_agent#log-data) |This item is not supported for Windows Event Log.<br>The `persistent_dir` parameter is not supported on Windows. |
|[log.count\[file,<regexp>,<encoding>,<maxproclines>,<mode>,<maxdelay>,<options>\]](/manual/config/items/itemtypes/zabbix_agent#log-data) |This item is not supported for Windows Event Log.<br>The `persistent_dir` parameter is not supported on Windows. |
|[logrt\[file_regexp,<regexp>,<encoding>,<maxlines>,<mode>,<output>,<maxdelay>,<options>\]](/manual/config/items/itemtypes/zabbix_agent#log-data) |This item is not supported for Windows Event Log.<br>The `persistent_dir` parameter is not supported on Windows. |
|[logrt.count\[file_regexp,<regexp>,<encoding>,<maxproclines>,<mode>,<maxdelay>,<options>\]](/manual/config/items/itemtypes/zabbix_agent#log-data) |This item is not supported for Windows Event Log.<br>The `persistent_dir` parameter is not supported on Windows. |
|[modbus.get\[endpoint,<slave id>,<function>,<address>,<count>,<type>,<endianness>,<offset>\]](/manual/config/items/itemtypes/zabbix_agent#modbus-data) | |
|[net.dns\[<ip>,name,<type>,<timeout>,<count>,<protocol>\]](/manual/config/items/itemtypes/zabbix_agent#network-data) |The `ip`, `timeout` and `count` parameters are ignored on Windows. |
|[net.dns.record\[<ip>,name,<type>,<timeout>,<count>,<protocol>\]](/manual/config/items/itemtypes/zabbix_agent#network-data) |The `ip`, `timeout` and `count` parameters are ignored on Windows. |
|[net.if.discovery](/manual/config/items/itemtypes/zabbix_agent#network-data) |Some Windows versions (for example, Server 2008) might require the latest updates installed to support non-ASCII characters in interface names. |
|[net.if.in\[if,<mode>\]](/manual/config/items/itemtypes/zabbix_agent#network-data) |On Windows, the item gets values from 64-bit counters if available. 64-bit interface statistic counters were introduced in Windows Vista and Windows Server 2008. If 64-bit counters are not available, the agent uses 32-bit counters.<br><br>Multi-byte interface names on Windows are supported.<br><br>You may obtain network interface descriptions on Windows with net.if.discovery or net.if.list items. |
|[net.if.out\[if,<mode>\]](/manual/config/items/itemtypes/zabbix_agent#network-data) |On Windows, the item gets values from 64-bit counters if available. 64-bit interface statistic counters were introduced in Windows Vista and Windows Server 2008. If 64-bit counters are not available, the agent uses 32-bit counters.<br><br>Multi-byte interface names on Windows are supported.<br><br>You may obtain network interface descriptions on Windows with net.if.discovery or net.if.list items. |
|[net.if.total\[if,<mode>\]](/manual/config/items/itemtypes/zabbix_agent#network-data) |On Windows, the item gets values from 64-bit counters if available. 64-bit interface statistic counters were introduced in Windows Vista and Windows Server 2008. If 64-bit counters are not available, the agent uses 32-bit counters.<br><br>You may obtain network interface descriptions on Windows with net.if.discovery or net.if.list items. |
|[net.tcp.listen\[port\]](/manual/config/items/itemtypes/zabbix_agent#network-data) | |
|[net.tcp.port\[<ip>,port\]](/manual/config/items/itemtypes/zabbix_agent#network-data) | |
|[net.tcp.service\[service,<ip>,<port>\]](/manual/config/items/itemtypes/zabbix_agent#network-data) |Checking of LDAP and HTTPS on Windows is only supported by Zabbix agent 2. |
|[net.tcp.service.perf\[service,<ip>,<port>\]](/manual/config/items/itemtypes/zabbix_agent#network-data) |Checking of LDAP and HTTPS on Windows is only supported by Zabbix agent 2. |
|[net.tcp.socket.count\[<laddr>,<lport>,<raddr>,<rport>,<state>\]](/manual/config/items/itemtypes/zabbix_agent#network-data) |This item is supported on Linux by Zabbix agent, but on Windows it is supported only by [Zabbix agent 2](/manual/config/items/itemtypes/zabbix_agent/zabbix_agent2) on 64-bit Windows. |
|[net.udp.service\[service,<ip>,<port>\]](/manual/config/items/itemtypes/zabbix_agent#network-data) | |
|[net.udp.service.perf\[service,<ip>,<port>\]](/manual/config/items/itemtypes/zabbix_agent#network-data) | |
|[net.udp.socket.count\[<laddr>,<lport>,<raddr>,<rport>,<state>\]](/manual/config/items/itemtypes/zabbix_agent#network-data) |This item is supported on Linux by Zabbix agent, but on Windows it is supported only by [Zabbix agent 2](/manual/config/items/itemtypes/zabbix_agent/zabbix_agent2) on 64-bit Windows. |
|[proc.num\[<name>,<user>\]](/manual/config/items/itemtypes/zabbix_agent#process-data) |On Windows, only the `name` and `user` parameters are supported. |
|[system.cpu.discovery](/manual/config/items/itemtypes/zabbix_agent#system-data) | |
|[system.cpu.load\[<cpu>,<mode>\]](/manual/config/items/itemtypes/zabbix_agent#system-data) | |
|[system.cpu.num\[<type>\]](/manual/config/items/itemtypes/zabbix_agent#system-data) | |
|[system.cpu.util\[<cpu>,<type>,<mode>\]](/manual/config/items/itemtypes/zabbix_agent#system-data) |*system* is the only `type` parameter supported on Windows. |
|[system.hostname\[<type>, <transform>\]](/manual/config/items/itemtypes/zabbix_agent#system-data) |The value is acquired by either GetComputerName() (for **netbios**) or gethostname() (for **host**) functions on Windows.<br><br>Examples of returned values:<br>=> system.hostname → WIN-SERV2008-I6<br>=> system.hostname\[host\] → Win-Serv2008-I6LonG<br>=> system.hostname\[host,lower\] → win-serv2008-i6long<br><br>See also a [more detailed description](/manual/appendix/install/windows_agent#configuration).|
|[system.localtime\[<type>\]](/manual/config/items/itemtypes/zabbix_agent#system-data) | |
|[system.run\[command,<mode>\]](/manual/config/items/itemtypes/zabbix_agent#system-data) | |
|[system.sw.arch](/manual/config/items/itemtypes/zabbix_agent#system-data) | |
|[system.swap.size\[<device>,<type>\]](/manual/config/items/itemtypes/zabbix_agent#system-data) |The `pused` type parameter is supported on Linux by Zabbix agent, but on Windows it is supported only by [Zabbix agent 2](/manual/config/items/itemtypes/zabbix_agent/zabbix_agent2).<br>Note that this key might report incorrect swap space size/percentage on virtualized (VMware ESXi, VirtualBox) Windows platforms. In this case you may use the `perf_counter[\700(_Total)\702]` key to obtain correct swap space percentage. |
|[system.uname](/manual/config/items/itemtypes/zabbix_agent#system-data) |Example of returned value:<br>Windows ZABBIX-WIN 6.0.6001 Microsoft® Windows Server® 2008 Standard Service Pack 1 x86<br><br>On Windows the value for this item is obtained from Win32\_OperatingSystem and Win32\_Processor WMI classes. The OS name (including edition) might be translated to the user's display language. On some versions of Windows it contains trademark symbols and extra spaces. |
|[system.uptime](/manual/config/items/itemtypes/zabbix_agent#system-data) | |
|[vfs.dir.count\[dir,<regex_incl>,<regex_excl>,<types_incl>,<types_excl>,<max_depth>,<min_size>,<max_size>,<min_age>,<max_age>,<regex_excl_dir>\]](/manual/config/items/itemtypes/zabbix_agent#virtual-file-system-data) |On Windows, directory symlinks are skipped and hard links are counted only once.<br><br>Example:<br>⇒ vfs.dir.count\["C:\\Users\\ADMINI\~1\\AppData\\Local\\Temp"\] - monitors the number of files in temporary directory|
|[vfs.dir.get\[dir,<regex_incl>,<regex_excl>,<types_incl>,<types_excl>,<max_depth>,<min_size>,<max_size>,<min_age>,<max_age>,<regex_excl_dir>\]](/manual/config/items/itemtypes/zabbix_agent#virtual-file-system-data) |On Windows, directory symlinks are skipped and hard links are counted only once.<br><br>Example:<br>⇒ vfs.dir.get\["C:\\Users\\ADMINI\~1\\AppData\\Local\\Temp"\] - retrieves the file list in temporary directory|
|[vfs.dir.size\[dir,<regex_incl>,<regex_excl>,<mode>,<max_depth>,<regex_excl_dir>\]](/manual/config/items/itemtypes/zabbix_agent#virtual-file-system-data) |On Windows any symlink is skipped and hard links are taken into account only once.|
|[vfs.file.cksum\[file,<mode>\]](/manual/config/items/itemtypes/zabbix_agent#virtual-file-system-data) | |
|[vfs.file.contents\[file,<encoding>\]](/manual/config/items/itemtypes/zabbix_agent#virtual-file-system-data) | |
|[vfs.file.exists\[file,<types_incl>,<types_excl>\]](/manual/config/items/itemtypes/zabbix_agent#virtual-file-system-data) |On Windows the double quotes have to be backslash '\\' escaped and the whole item key enclosed in double quotes when using the command line utility for calling zabbix\_get.exe or agent2.<br><br>Note that the item may turn unsupported on Windows if a directory is searched within a non-existing directory, e.g. vfs.file.exists\[C:\\no\\dir,dir\] (where 'no' does not exist).|
|[vfs.file.get\[file\]](/manual/config/items/itemtypes/zabbix_agent#virtual-file-system-data) |Supported file types on Windows: regular file, directory, symbolic link |
|[vfs.file.md5sum\[file\]](/manual/config/items/itemtypes/zabbix_agent#virtual-file-system-data) | |
|[vfs.file.owner\[file,<ownertype>,<resulttype>\]](/manual/config/items/itemtypes/zabbix_agent#virtual-file-system-data) | |
|[vfs.file.regexp\[\file,regexp,<encoding>,<start line>,<end line>,<output>\]](/manual/config/items/itemtypes/zabbix_agent#virtual-file-system-data) | |
|[vfs.file.regmatch\[file,regexp,<encoding>,<start line>,<end line>\]](/manual/config/items/itemtypes/zabbix_agent#virtual-file-system-data) | |
|[vfs.file.size\[file,<mode>\]](/manual/config/items/itemtypes/zabbix_agent#virtual-file-system-data) | |
|[vfs.file.time\[file,<mode>\]](/manual/config/items/itemtypes/zabbix_agent#virtual-file-system-data) |On Windows XP vfs.file.time[file,change] may be equal to vfs.file.time[file,access]. |
|[vfs.fs.discovery](/manual/config/items/itemtypes/zabbix_agent#virtual-file-system-data) |The {#FSLABEL} macro is supported on Windows since Zabbix 6.0.|
|[vfs.fs.get](/manual/config/items/itemtypes/zabbix_agent#virtual-file-system-data) |The {#FSLABEL} macro is supported on Windows since Zabbix 6.0.|
|[vfs.fs.size\[fs,<mode>\]](/manual/config/items/itemtypes/zabbix_agent#virtual-file-system-data) | |
|[vm.memory.size\[<mode>\]](/manual/config/items/itemtypes/zabbix_agent#virtual-machine-data) | |
|[web.page.get\[host,<path>,<port>\]](/manual/config/items/itemtypes/zabbix_agent#web-monitoring-data) | |
|[web.page.perf\[host,<path>,<port>\]](/manual/config/items/itemtypes/zabbix_agent#web-monitoring-data) | |
|[web.page.regexp\[host,<path>,<port>,regexp,<length>,<output>\]](/manual/config/items/itemtypes/zabbix_agent#web-monitoring-data) | |
|[zabbix.stats\[<ip>,<port>\]](/manual/config/items/itemtypes/zabbix_agent#zabbix-metrics) | |
|[zabbix.stats\[<ip>,<port>,queue,<from>,<to>\]](/manual/config/items/itemtypes/zabbix_agent#zabbix-metrics) | |

[comment]: # ({/new-52f3ae09})
