[comment]: # translation:outdated

[comment]: # ({new-6fa4a853})
# 4 Рекомендуемые настройки UnixODBC для MSSQL

[comment]: # ({/new-6fa4a853})

[comment]: # ({new-29c905c8})
#### Установка

    *** Red Hat Enterprise Linux/CentOS**:

    # yum -y install freetds unixODBC 

    ***Debian/Ubuntu**:

Пожалуйста, обратитесь к [документации пользователя
FreeTDS](http://www.freetds.org/userguide/), чтобы загрузить необходимый
драйвер базы данных для соответствующей платформы.

Для получения дополнительной информации, пожалуйста, обратитесь к:
[Установка unixODBC](/ru/manual/config/items/itemtypes/odbc_checks).

[comment]: # ({/new-29c905c8})

[comment]: # ({new-aa4d8324})
#### Настройка

Настройка ODBC выполняется изменением **odbcinst.ini** и **odbc.ini**
файлов. Эти файлы конфигурации можно найти в */etc* папке. Файл
**odbcinst.ini** может отсутствовать и в этом случае его необходимо
создать вручную.

Пожалуйста, обратите внимание на следующие примеры:

**odbcinst.ini**

    $ vi /etc/odbcinst.ini
    [FreeTDS]
    Driver = /usr/lib64/libtdsodbc.so.0

**odbc.ini**

    $ vi /etc/odbc.ini
    [sql1]
    Driver = FreeTDS
    Server = <SQL сервер 1 IP>
    PORT = 1433
    TDS_Version = 8.0

[comment]: # ({/new-aa4d8324})
