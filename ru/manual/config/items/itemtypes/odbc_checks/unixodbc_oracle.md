[comment]: # translation:outdated

[comment]: # ({new-596937b2})
# 3 Рекомендуемые настройки UnixODBC для Oracle

[comment]: # ({/new-596937b2})

[comment]: # ({new-4bfd360f})
#### Установка

Пожалуйста, для получения всех требуемых инструкций обратитесь к
[документации
Oracle](https://docs.oracle.com/database/121/ADFNS/adfns_odbc.htm).

Для получения дополнительной информации, пожалуйста, обратитесь к:
[Установка unixODBC](/ru/manual/config/items/itemtypes/odbc_checks).

[comment]: # ({/new-4bfd360f})
