[comment]: # translation:outdated

[comment]: # ({new-e966b3a3})
# 1 Рекомендуемые настройки UnixODBC для MySQL

[comment]: # ({/new-e966b3a3})

[comment]: # ({new-e5f46c02})
#### Установка

    *** Red Hat Enterprise Linux/CentOS**:

    # yum install mysql-connector-odbc

    ***Debian/Ubuntu**:

Пожалуйста, обратитесь к [документации
MySQL](https://dev.mysql.com/downloads/connector/odbc/), чтобы загрузить
необходимый драйвер базы данных для соответствующей платформы.

Для получения дополнительной информации, пожалуйста, обратитесь к:
[Установка unixODBC](/ru/manual/config/items/itemtypes/odbc_checks).

[comment]: # ({/new-e5f46c02})

[comment]: # ({new-dcea6601})
#### Настройка

Настройка ODBC выполняется изменением **odbcinst.ini** и **odbc.ini**
файлов. Эти файлы конфигурации можно найти в */etc* папке. Файл
**odbcinst.ini** может отсутствовать и в этом случае его необходимо
создать вручную.

**odbcinst.ini**

    [mysql]
    Description = General ODBC for MySQL
    Driver      = /usr/lib64/libmyodbc5.so
    Setup       = /usr/lib64/libodbcmyS.so 
    FileUsage   = 1

Пожалуйста, обратите внимание на следующие примеры параметров
конфигурации **odbc.ini**.

-   Пример подключения по IP адресу:

```{=html}
<!-- -->
```
    [TEST_MYSQL]                                                     
    Description = MySQL database 1                                   
    Driver  = mysql                                                  
    Port = 3306                                                      
    Server = 127.0.0.1

-   Пример подключения по IP адресу и с использованием учетной записи.
    По умолчанию используется база данных zabbix:

```{=html}
<!-- -->
```
    [TEST_MYSQL_FILLED_CRED]                       
    Description = MySQL database 2                 
    Driver  = mysql                                
    User = root                                    
    Port = 3306                                    
    Password = zabbix                           
    Database = zabbix                             
    Server = 127.0.0.1                             

-   Пример подключения через сокет и с использованием учетной записи. По
    умолчанию используется база данных zabbix:

```{=html}
<!-- -->
```
    [TEST_MYSQL_FILLED_CRED_SOCK]                  
    Description = MySQL database 3                 
    Driver  = mysql                                
    User = root                                    
    Password = zabbix                           
    Socket = /var/run/mysqld/mysqld.sock           
    Database = zabbix

Все остальные возможные опции параметров конфигурации можно найти на
странице [официальной документации
MySQL](https://dev.mysql.com/doc/connector-odbc/en/connector-odbc-configuration-connection-parameters.html)
\[en\].

[comment]: # ({/new-dcea6601})
