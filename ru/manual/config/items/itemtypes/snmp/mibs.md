[comment]: # translation:outdated

[comment]: # ({new-8afcd82f})
# 3 MIB файлы

[comment]: # ({/new-8afcd82f})

[comment]: # ({new-f224bb08})
#### Введение

MIB обозначает Базу Управляющей Информации. MIB файлы позволяют вам
использовать текстовое представление OID'а (Идентификатор Объекта).

Например,

    ifHCOutOctets

является текстовым представлением OID'а

    1.3.6.1.2.1.31.1.1.1.10

Вы можете использовать оба варианта при мониторинге SNMP устройств с
Zabbix, но если вам удобнее пользоваться текстовым представлением, вам
нужно установить MIB файлы.

[comment]: # ({/new-f224bb08})

[comment]: # ({new-5aed9e1b})
#### Установка MIB файлов

На системах на основе Debian:

    # apt install snmp-mibs-downloader
    # download-mibs

На системах на основе RedHat:

    # yum install net-snmp-libs

[comment]: # ({/new-5aed9e1b})

[comment]: # ({new-5346f669})
#### Включение MIB файлов

На системах на основе RedHat mib файлы должны быть включены по
умолчанию. На системах на основе Debian вам нужно отредактировать файл
`/etc/snmp/snmp.conf` и закомментировать строку, которая содержит
`mibs`:

    # As the snmp packages come without MIB files due to license reasons, loading
    # of MIBs is disabled by default. If you added the MIBs you can reenable
    # loading them by commenting out the following line.
    #mibs :

[comment]: # ({/new-5346f669})

[comment]: # ({new-bb4c3c1f})
#### Тестирование MIB файлов

Тестирование snmp MIB можно выполнить с использованием `snmpwalk`
утилиты. Если у вас эта утилита не установлена, вы можете использовать
следующие инструкции.

На системах на основе Debian:

    # apt install snmp

На системах на основе RedHat:

    # yum install net-snmp-utils

После чего следующая команда не выдаст ошибку при выполнении запроса к
сетевому устройству:

    $ snmpwalk -v 2c -c public <IP СЕТЕВОГО УСТРОЙСТВА> ifInOctets
    IF-MIB::ifInOctets.1 = Counter32: 176137634
    IF-MIB::ifInOctets.2 = Counter32: 0
    IF-MIB::ifInOctets.3 = Counter32: 240375057
    IF-MIB::ifInOctets.4 = Counter32: 220893420
    [...]

[comment]: # ({/new-bb4c3c1f})

[comment]: # ({new-87dbd7d4})
#### Использование MIB в Zabbix

Самое главное - иметь в виду, что процессы Zabbix не знают об
изменениях, которые сделаны в MIB файлы. Поэтому после каждого изменения
вам необходимо перезапустить Zabbix сервер или прокси, например:

    # service zabbix-server restart

После выполнения этой команды изменения сделанные в MIB файлах вступят в
силу.

[comment]: # ({/new-87dbd7d4})

[comment]: # ({new-efd836b8})
#### Использование пользовательских MIB файлов

Имеются стандартные MIB файлы, которые поставляются с каждым GNU/Linux
дистрибутивом. Но некоторые производители устройств поставляют свои
собственные файлы.

Скажем, если вы хотите использовать
[CISCO-SMI](ftp://ftp.cisco.com/pub/mibs/v2/CISCO-SMI.my) MIB файл.
Следующие инструкции загрузят и установят этот файл:

    # wget ftp://ftp.cisco.com/pub/mibs/v2/CISCO-SMI.my -P /tmp
    # mkdir -p /usr/local/share/snmp/mibs
    # grep -q '^mibdirs +/usr/local/share/snmp/mibs' /etc/snmp/snmp.conf 2>/dev/null || echo "mibdirs +/usr/local/share/snmp/mibs" >> /etc/snmp/snmp.conf
    # cp /tmp/CISCO-SMI.my /usr/local/share/snmp/mibs

Теперь вы сможете использовать его. Попытайтесь перевести имя объекта
*ciscoProducts* из MIB файла в OID:

    # snmptranslate -IR -On CISCO-SMI::ciscoProducts
    .1.3.6.1.4.1.9.1

Если вы получаете ошибки вместо OID, удостоверьтесь, что все предыдущие
команды не вернули никаких ошибок.

Когда перевод имени объекта заработает, вы будете готовы использовать
пользовательский MIB файл. Обратите внимание, что в запросе используется
MIB префикс имени (*CISCO-SMI::*). Вам потребуется его указывать, когда
используете инструменты командной строки, а также Zabbix.

Не забудьте перезапустить Zabbix сервер/прокси перед использованием
этого MIB файла в Zabbix.

::: noteimportant
Имейте в виду, что у MIB файлов могут быть
зависимости. То есть, одному MIB может требоваться другой. Чтобы
удовлетворить эти зависимости, вам нужно установить все необходимые MIB
файлы.
:::

[comment]: # ({/new-efd836b8})
