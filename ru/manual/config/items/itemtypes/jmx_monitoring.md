[comment]: # translation:outdated

[comment]: # ({new-627e4087})
# 13 JMX мониторинг

[comment]: # ({/new-627e4087})

[comment]: # ({new-e3fddc4c})
#### Обзор

Мониторинг JMX можно использовать для наблюдения за счетчиками JMX в
Java приложениях.

В Zabbix 2.0 добавлена встроенная поддержка мониторинга JMX, был выпущен
новый Zabbix демон, так называемый "Zabbix Java gateway".

Когда Zabbix сервер хочет узнать значение конкретного счетчика JMX у
узла сети, он опрашивает Zabbix **Java gateway**, который в свою очередь
используя [API управление
JMX](http://java.sun.com/javase/technologies/core/mntr-mgmt/javamanagement/),
удаленно опрашивает интересующее приложение.

Для получения более подробных сведений, включая где можно взять Zabbix
Java gateway и как его настроить, смотрите [этот
раздел](/ru/manual/concepts/java) руководства.

::: notewarning
Связь между Java gateway и наблюдаемым JMX
приложением не должна быть закрыта брандмауэром.
:::

[comment]: # ({/new-e3fddc4c})

[comment]: # ({new-7feef73e})
#### Включение удаленного JMX мониторинга для Java приложений

Приложению Java не требуется какое-либо дополнительно установленное
программное обеспечение, но для поддержки удаленного мониторинга JMX
приложение должно быть запущено с указанными ниже параметрами командной
строки.

Как минимум, если вы просто хотите начать наблюдение за простым
приложением Java на локальном хосте без каких либо защиты, запустите его
со следующими опциями:

    java \
    -Dcom.sun.management.jmxremote \
    -Dcom.sun.management.jmxremote.port=12345 \
    -Dcom.sun.management.jmxremote.authenticate=false \
    -Dcom.sun.management.jmxremote.ssl=false \
    -jar /usr/share/doc/openjdk-6-jre-headless/demo/jfc/Notepad/Notepad.jar

С этими аргументами Java будет слушать входящие соединения JMX на порту
12345, только с локальных хостов, без обязательных аутентификации или
SSL.

Если вы хотите разрешить подключения с другого интерфейса, укажите
параметр -Djava.rmi.server.hostname равным IP адресу этого интерфейса.

Если вы хотите иметь более строгую проверку в плане безопасности, есть
много других опций в Java, которые вам доступны. Например, следующая
иллюстрация запускает приложение с более универсальным набором опций и
открывает это приложение для более широкой сети, не только для
локального компьютера.

    java \
    -Djava.rmi.server.hostname=192.168.3.14 \
    -Dcom.sun.management.jmxremote \
    -Dcom.sun.management.jmxremote.port=12345 \
    -Dcom.sun.management.jmxremote.authenticate=true \
    -Dcom.sun.management.jmxremote.password.file=/etc/java-6-openjdk/management/jmxremote.password \
    -Dcom.sun.management.jmxremote.access.file=/etc/java-6-openjdk/management/jmxremote.access \
    -Dcom.sun.management.jmxremote.ssl=true \
    -Djavax.net.ssl.keyStore=$ВАШЕ_ХРАНИЛИЩЕ_КЛЮЧЕЙ \
    -Djavax.net.ssl.keyStorePassword=$ВАШ_ПАРОЛЬ_К_ХРАНИЛИЩУ_КЛЮЧЕЙ \
    -Djavax.net.ssl.trustStore=$ВАШЕ_ДОВЕРЕННОЕ_ХРАНИЛИЩЕ \
    -Djavax.net.ssl.trustStorePassword=$ВАШ_ПАРОЛЬ_К_ДОВЕРЕННОМУ_ХРАНИЛИЩУ \
    -Dcom.sun.management.jmxremote.ssl.need.client.auth=true \
    -jar /usr/share/doc/openjdk-6-jre-headless/demo/jfc/Notepad/Notepad.jar

Значительное количество (если не все) этих настроек можно указать в
/etc/java-6-openjdk/management/management.properties (или там, где этот
файл расположен на вашем компьютере).

Обратите внимание, если вы желаете использовать SSL, то вы должны
изменить startup.sh скрипт Java gateway, добавив в него опции
`-Djavax.net.ssl.*` так, чтобы он знал где искать хранилище ключей и
доверенное хранилище.

Смотрите [Мониторинг и Управление с использованием JMX
\[en\]](http://download.oracle.com/javase/1.5.0/docs/guide/management/agent.html)
для получения более подробной информации.

[comment]: # ({/new-7feef73e})

[comment]: # ({new-4b8fd32c})
#### Настройка JMX интерфейсов и элементов данных в веб-интерфейсе Zabbix

Когда Java Gateway запущен, сервер знает где его искать и Java
приложение запущено с поддержкой удаленного JMX мониторинга, самое время
настроить интерфейсы и элементы данных в Веб-интерфейсе Zabbix.

[comment]: # ({/new-4b8fd32c})

[comment]: # ({new-01c1becc})
##### Настройка JMX интерфейса

Начнем с создания интерфейса JMX-типа у интересующего узла сети.\
![](../../../../../assets/en/manual/config/items/itemtypes/jmx_interface.png)

[comment]: # ({/new-01c1becc})

[comment]: # ({new-f55573f5})
##### Добавление элемента данных JMX агента

Для каждого интересующего вас счетчика JMX вам необходимо добавить
элемент данных с типом **JMX агент** присоединенный к этому интерфейсу.\
Ключ на снимке экрана ниже имеет следующий вид
`jmx["java.lang:type=Memory","HeapMemoryUsage.used"]`.

![](../../../../../assets/en/manual/config/items/itemtypes/jmx_item.png){width="600"}

Все обязательные поля ввода отмечены красной звёздочкой.

Поля, требующие специфичной информации для JMX элементов данных:

|   |   |
|---|---|
|*Тип*|Укажите здесь **JMX агент**.|
|*Ключ*|Ключ элемента данных `jmx[]` состоит из двух параметров:<br>**имя объекта** - имя объекта MBean;<br>**имя атрибута** - имя атрибута MBean с опциональными составными данными имен полей, разделенных точками.<br>Смотрите ниже для получения более подробных сведений о ключах элементов данных JMX.<br>Начиная с Zabbix 3.4, вы можете обнаруживать MBeans и MBean атрибуты, используя элемент данных `jmx.discovery[]` [низкоуровневого обнаружения](/ru/manual/discovery/low_level_discovery/jmx).|
|*JMX endpoint*|Вы можете указать пользовательский JMX endpoint. Убедитесь, что параметры подключения JMX endpoint совпадают с JMX интерфейсом. Это можно сделать при помощи макросов {HOST.\*}, как это сделано в JMX endpoint по умолчанию.<br>Это поле поддерживается начиная с 3.4.0. Поддерживаются [макросы](/ru/manual/appendix/macros/supported_by_location) {HOST.\*} и пользовательские макросы.|
|*Имя пользователя*|Укажите имя пользователя, если вы настроили аутентификацию у вашего Java приложения.<br>Поддерживаются пользовательские макросы.|
|*Пароль*|Укажите пароль, если вы настроили аутентификацию у вашего Java приложения.<br>Поддерживаются пользовательские макросы.|

Если вы хотите наблюдать за Логическим счетчиком, который может быть
"true" или "false", вы должны указать тип информации "Числовой (целое
положительное)" и "Логический" тип данных. Сервер будет записывать
Логические значения как 1 или 0, соответственно.

[comment]: # ({/new-f55573f5})

[comment]: # ({new-9430cb1d})
#### Детальная информация о ключах JMX элементов данных

[comment]: # ({/new-9430cb1d})

[comment]: # ({new-105cc785})
##### Простые атрибуты

Имя объекта MBean неважно, кроме строки, которую вы определили в вашем
Java приложении. Имя атрибута, с другой стороны, может быть более
сложным. В случае, если атрибут возвращает простой тип данных (число,
строку и т.п.), то не стоит волноваться об этом, ключ будет выглядеть
примерно так:

    jmx[com.example:Type=Hello,weight]

В этом примере именем объекта является "com.<example:Type=Hello>",
именем атрибута будет являться "weight" и, скорее всего, тип
возвращаемого значения должен быть "Числовой (с плавающей точкой)".

[comment]: # ({/new-105cc785})

[comment]: # ({new-00e3fc9b})
##### Атрибуты возвращающие составные данные

Ключ становится более сложным, когда ваш атрибут возвращает составные
данные. Например: именем вашего атрибута является "apple" и он
возвращает хэш представляющих его параметров, таких как "weight",
"color" и прочее. Тогда ваш ключ может выглядеть примерно так:

    jmx[com.example:Type=Hello,apple.weight]

Этот пример показывает как разделяются с помощью точки имя атрибута и
ключ хэша. Точно также, если атрибут возвращает часть вложенных
составных данных, их нужно снова разделить точкой:

    jmx[com.example:Type=Hello,fruits.apple.weight]

[comment]: # ({/new-00e3fc9b})

[comment]: # ({new-ae61b0e7})
##### Проблема с точками

Пока все хорошо. Но что, если имя атрибута или ключ хэша содержит символ
точки? Вот пример:

    jmx[com.example:Type=Hello,all.fruits.apple.weight]

Это проблема. Как сказать Zabbix'у, что имя атрибута "all.fruits", а не
просто "all"? Как отличить точку, которая является частью имени, от
точки которая разделяет имя атрибута и ключи хэшей?

До **2.0.4** Zabbix Java gateway был не способен справится с такими
ситуациями и пользователи оставались с НЕПОДДЕРЖИВАЕМЫМИ элементами
данных. Начиная с 2.0.4 проблема была исправлена, все что вам требуется
сделать - экранировать точки, которые являются частью имени, обратной
косой чертой:

    jmx[com.example:Type=Hello,all\.fruits.apple.weight]

Аналогично, если ваш ключ хэша содержит точку вам необходимо её
экранировать:

    jmx[com.example:Type=Hello,all\.fruits.apple.total\.weight]

[comment]: # ({/new-ae61b0e7})

[comment]: # ({new-3a5802d2})
##### Другие проблемы

Символ обратной косой черты тоже должен быть экранирован:

    jmx[com.example:type=Hello,c:\\documents]

Для обработки любых других символов в ключе JMX элемента данных,
пожалуйста, смотрите
[раздел](ru/manual/config/items/item/key#параметр_-_строка_заключенная_в_кавычки)
формата ключа элементов данных.

На самом деле это все, что нужно сделать. Успешного мониторинга JMX!

[comment]: # ({/new-3a5802d2})

[comment]: # ({new-80aedcd9})
##### Непримитивные типы данных

Начиная с Zabbix 4.0.0 имеется возможность работы с пользовательскими
MBean, которые возвращают непримитивные типы данных, которые
переопределяют метод **toString()**.

[comment]: # ({/new-80aedcd9})

[comment]: # ({new-1813b2fc})
#### Пример пользовательского endpoint с JBoss EAP 6.4

Пользовательские endpoint позволяют работать с различными транспортными
протоколами, которые отличаются от протокола по умолчанию RMI.

Для иллюстрации этой возможности в качестве примера давайте попытаемся
настроить JBoss EAP 6.4. Во-первых, давайте сделаем некоторые
предположения:

-   У вас уже имеется установленный Zabbix Java gateway. Если нет, тогда
    вам нужно сделать это в соответствии с
    [документацией](/ru/manual/concepts/java).
-   Zabbix сервер и Java gateway установлены с префиксом /usr/local/.
-   JBoss уже установлен в /opt/jboss-eap-6.4/ и запущен в автономном
    режиме.
-   Мы будем считать, что все эти компоненты работают на одном и том же
    хосте.
-   Брандмауэр и SELinux отключены (или настроены соответствующим
    образом).

Давайте выполним некоторые простые настройки в zabbix\_server.conf:

    JavaGateway=127.0.0.1
    StartJavaPollers=5

И в файле конфигурации zabbix\_java/settings.sh (или
zabbix\_java\_gateway.conf):

    START_POLLERS=5

Проверьте, что JBoss слушает свой стандартный порт управления:

    $ netstat -natp | grep 9999
    tcp        0      0 127.0.0.1:9999          0.0.0.0:*               LISTEN      10148/java

Теперь давайте создадим в Zabbix узел сети с JMX интерфейсом
127.0.0.1:9999.

![](../../../../../assets/en/manual/config/items/itemtypes/jmx_jboss_example.png){width="600"}

Как мы знаем эта версия JBoss использует протокол JBoss Remoting всесто
RMI, мы можем использовать массовое обновление параметра JMX endpoint в
нашем шаблоне JMX в соответствии:

    service:jmx:remoting-jmx://{HOST.CONN}:{HOST.PORT}

![](../../../../../assets/en/manual/config/items/itemtypes/jmx_jboss_example3.png){width="600"}

Давайте обновим кэш конфигурации:

    $ /usr/local/sbin/zabbix_server -R config_cache_reload

Обратите внимание, что сначала может возникнуть ошибка.

![](../../../../../assets/en/manual/config/items/itemtypes/jmx_jboss_example4.png){width="600"}

"Unsupported protocol: remoting-jmx" означает, что Java gateway не знает
как работать с указанным протоколом. Эту ошибку можно исправить создав
файл \~/needed\_modules.txt со следующим содержимым:

    jboss-as-remoting
    jboss-logging
    jboss-logmanager
    jboss-marshalling
    jboss-remoting
    jboss-sasl
    jcl-over-slf4j
    jul-to-slf4j-stub
    log4j-jboss-logmanager
    remoting-jmx
    slf4j-api
    xnio-api
    xnio-nio</pre>

и затем выполнив эту команду:

    $ for i in $(cat ~/needed_modules.txt); do find /opt/jboss-eap-6.4 -iname ${i}*.jar -exec cp {} /usr/local/sbin/zabbix_java/lib/ \; ; done

Таким образом, у Java gateway будут в наличии все необходимые модули для
работы с jmx-remoting. Осталось только перезапустить Java gateway,
немного подождать и, если вы все сделали правильно, вы увидите, что эти
данные JMX мониторинга начинают поступать в Zabbix:

![](../../../../../assets/en/manual/config/items/itemtypes/jmx_jboss_example5.png){width="600"}

[comment]: # ({/new-1813b2fc})


[comment]: # ({new-ffdec0f9})
#### Using custom endpoint with JBoss EAP 6.4

Custom endpoints allow working with different transport protocols other
than the default RMI.

To illustrate this possibility, let's try to configure JBoss EAP 6.4
monitoring as an example. First, let's make some assumptions:

-   You have already installed Zabbix Java gateway. If not, then you can
    do it in accordance with the [documentation](/manual/concepts/java).
-   Zabbix server and Java gateway are installed with the prefix
    /usr/local/
-   JBoss is already installed in /opt/jboss-eap-6.4/ and is running in
    standalone mode
-   We shall assume that all these components work on the same host
-   Firewall and SELinux are disabled (or configured accordingly)

Let's make some simple settings in zabbix\_server.conf:

    JavaGateway=127.0.0.1
    StartJavaPollers=5

And in the zabbix\_java/settings.sh configuration file (or
zabbix\_java\_gateway.conf):

    START_POLLERS=5

Check that JBoss listens to its standard management port:

    $ netstat -natp | grep 9999
    tcp        0      0 127.0.0.1:9999          0.0.0.0:*               LISTEN      10148/java

Now let's create a host with JMX interface 127.0.0.1:9999 in Zabbix.

![](../../../../../assets/en/manual/config/items/itemtypes/jmx_jboss_example.png){width="600"}

As we know that this version of JBoss uses the the JBoss Remoting
protocol instead of RMI, we may mass update the JMX endpoint parameter
for items in our JMX template accordingly:

    service:jmx:remoting-jmx://{HOST.CONN}:{HOST.PORT}

![](../../../../../assets/en/manual/config/items/itemtypes/jmx_jboss_example_b.png)

Let's update the configuration cache:

    $ /usr/local/sbin/zabbix_server -R config_cache_reload

Note that you may encounter an error first.

![](../../../../../assets/en/manual/config/items/itemtypes/jmx_jboss_example4.png){width="600"}

"Unsupported protocol: remoting-jmx" means that Java gateway does not
know how to work with the specified protocol. That can be fixed by
creating a \~/needed\_modules.txt file with the following content:

    jboss-as-remoting
    jboss-logging
    jboss-logmanager
    jboss-marshalling
    jboss-remoting
    jboss-sasl
    jcl-over-slf4j
    jul-to-slf4j-stub
    log4j-jboss-logmanager
    remoting-jmx
    slf4j-api
    xnio-api
    xnio-nio</pre>

and then executing the command:

    $ for i in $(cat ~/needed_modules.txt); do find /opt/jboss-eap-6.4 -iname ${i}*.jar -exec cp {} /usr/local/sbin/zabbix_java/lib/ \; ; done

Thus, Java gateway will have all the necessary modules for working with
jmx-remoting. What's left is to restart the Java gateway, wait a bit and
if you did everything right, see that JMX monitoring data begin to
arrive in Zabbix (see also: [Latest
data](/[[/manual/web_interface/frontend_sections/monitoring/latest_data)).

[comment]: # ({/new-ffdec0f9})
