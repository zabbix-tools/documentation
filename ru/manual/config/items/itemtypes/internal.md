[comment]: # attributes: notoc

[comment]: # translation:outdated

[comment]: # ({new-8981e17d})
# 8 Внутренние проверки

[comment]: # ({/new-8981e17d})

[comment]: # ({new-34928067})
#### Обзор

Внутренние проверки позволяют наблюдать за внутренним процессами Zabbix.
Другими словами, вы можете наблюдать что происходит с Zabbix сервером
или Zabbix прокси.

Внутренние проверки вычисляются:

-   на Zabbix сервере - если узел сети наблюдается через сервер
-   на Zabbix прокси - если узел сети наблюдается через прокси

Внутренние проверки обрабатываются сервером или прокси вне зависимости
от состояния обслуживания узла сети (начиная с Zabbix 2.4.0).

Для использования этого элемента данных выберите тип элемента данных
**Zabbix внутренний**.

::: notetip
Внутренние проверки обрабатываются Zabbix
поллерами.
:::

[comment]: # ({/new-34928067})

[comment]: # ({new-5d8f3b63})
#### Performance

Using some internal items may negatively affect performance. These items are:

-   `zabbix[host,,items]`
-   `zabbix[host,,items_unsupported]`
-   `zabbix[hosts]`
-   `zabbix[items]`
-   `zabbix[items_unsupported]`
-   `zabbix[queue]`
-   `zabbix[required_performance]`
-   `zabbix[stats,,,queue]`
-   `zabbix[triggers]`

The [System information](/manual/web_interface/frontend_sections/reports/status_of_zabbix) and [Queue](/manual/web_interface/frontend_sections/administration/queue) 
frontend sections are also affected.

[comment]: # ({/new-5d8f3b63})

[comment]: # ({new-74b446f4})
#### Поддерживаемые ключи

-   Параметры без угловых скобок являются константами - например, 'host'
    и 'available' в `zabbix[host,<тип>,available]`. Используйте их в
    ключе элемента данных без изменения (*как есть*).
-   Значения элементов данных и параметры элементов данных, которые "не
    поддерживаются на прокси" можно собирать только на узлах сети,
    которые наблюдаются через сервер. И наоборот, значения "не
    поддерживается на сервере" можно собирать только, если узел сети
    наблюдается через прокси.

|Ключ|<|<|<|<|<|
|--------|-|-|-|-|-|
|▲|Описание|<|<|Возвращаемое значение|Комментарии|
|zabbix\[boottime\]|<|<|<|<|<|
|<|Время запуска процесса Zabbix сервера в секундах.|<|<|Целое число.|<|
|zabbix\[history\]|<|<|<|<|<|
|<|Количество значений хранимых в таблице HISTORY|<|<|Целое число.|Не используйте с MySQL InnoDB, Oracle или PostgreSQL!<br>*(не поддерживается прокси)*|
|zabbix\[history\_log\]|<|<|<|<|<|
|<|Количество значений хранимых в таблице HISTORY\_LOG|<|<|Целое число.|Не используйте с MySQL InnoDB, Oracle или PostgreSQL!<br>Этот элемент данных поддерживается начиная с версии **1.8.3**.<br>*(не поддерживается прокси)*|
|zabbix\[history\_str\]|<|<|<|<|<|
|<|Количество значений хранимых в таблице HISTORY\_STR|<|<|Целое число.|Не используйте с MySQL InnoDB, Oracle или PostgreSQL!<br>*(не поддерживается прокси)*|
|zabbix\[history\_text\]|<|<|<|<|<|
|<|Количество значений хранимых в таблице HISTORY\_TEXT|<|<|Целое число.|Не используйте с MySQL InnoDB, Oracle или PostgreSQL!<br>Этот элемент данных поддерживается начиная с версии **1.8.3**.<br>*(не поддерживается прокси)*|
|zabbix\[history\_uint\]|<|<|<|<|<|
|<|Количество значений хранимых в таблице HISTORY\_UINT|<|<|Целое число.|Не используйте на MySQL InnoDB, Oracle или PostgreSQL!<br>Этот элемент данных поддерживается начиная с версии **1.8.3**.<br>*(не поддерживается прокси)*|
|zabbix\[host,,items\]|<|<|<|<|<|
|<|Количество активированных элементов данных (поддерживаемых и неподдерживаемых) у узла сети.|<|<|Целое число.|Этот элемент данных поддерживается начиная с Zabbix **3.0.0.**|
|zabbix\[host,,items\_unsupported\]|<|<|<|<|<|
|<|Количество активированных неподдерживаемых элементов данных у узла сети.|<|<|Целое число.|Этот элемент данных поддерживается начиная с Zabbix **3.0.0.**|
|zabbix\[host,,maintenance\]|<|<|<|<|<|
|<|Возвращает текущее состояние обслуживания узла сети.|<|<|0 - узел сети в нормальном состоянии,<br>1 - узел сети в обслуживании со сбором данных,<br>2 - узел сети в обслуживании без сбора данных.|Данный элемент данных всегда обрабатывается Zabbix сервером вне зависимости от настроек узла сети (мониторится через сервер или прокси). Прокси не получает этот элемент данных при получении своей конфигурации.<br>Второй параметр должен быть пустым и зарезервирован для использования в будущем.<br>Данный элемент данных поддерживается начиная с Zabbix **2.4.0.**<br>|
|zabbix\[host,discovery,interfaces\]|<|<|<|<|<|
|<|Детали по всем добавленным интерфейсам к узлу сети в веб-интерфейсе Zabbix.|<|<|JSON объект.|Этот элемент данных можно использовать в [низкоуровневом обнаружении](/ru/manual/discovery/low_level_discovery/host_interfaces).<br>Этот элемент данных поддерживается начиная с Zabbix **3.4.0.**<br>*(не поддерживается прокси)*|
|zabbix\[host,<тип>,available\]|<|<|<|<|<|
|<|Доступность определенного типа проверок на узле сети. Значение этого элемента соответствует иконкам доступности в списке узлов сети.|<|<|0 - недоступен, 1 - доступен, 2 - неизвестно.|Допустимые типы: **agent**, **snmp**, **ipmi**, **jmx**.<br><br>Значение элемента данных вычисляется согласно параметрам конфигурации [доступности/недоступности](/ru/manual/appendix/items/unreachability) соответствующего узла сети.<br><br>Этот элемент данных поддерживается начиная с Zabbix **2.0.0**.|
|zabbix\[hosts\]|<|<|<|<|<|
|<|Количество наблюдаемых узлов сети|<|<|Целое число.|Данный элемент данных поддерживается начиная с Zabbix **2.2.0**.|
|zabbix\[items\]|<|<|<|<|<|
|<|Количество активированных элементов данных (поддерживаемых и не поддерживаемых)|<|<|Целое число.|<|
|zabbix\[items\_unsupported\]|<|<|<|<|<|
|<|Количество не поддерживаемых элементов данных|<|<|Целое число.|<|
|zabbix\[java,,<параметр>\]|<|<|<|<|<|
|<|Получение информации связанной с Zabbix Java gateway.|<|<|Если <параметром> является **ping**, возвращается "1". Можно использовать для проверки доступности Java gateway, используя функцию триггера nodata().<br><br>Если <параметром> является **version**, возвращается версия Java gateway. Пример: "2.0.0".|Второй параметр должен быть пустым, т.к. зарезервирован для использования в будущем.<br><br>Этот элемент данных поддерживается начиная с Zabbix **2.0.0**.|
|zabbix\[preprocessing\_queue\]|<|<|<|<|<|
|<|Количество значений, помещенных в очередь предварительной обработки.|<|<|Целое число.|Этот элемент данных можно использовать для мониторинга размера очереди предварительной обработки.<br><br>Этот элемент данных поддерживается начиная с **3.4.0.**|
|zabbix\[process,<тип>,<режим>,<состояние>\]|<|<|<|<|<|
|<|Время конкретного процесса Zabbix или группы процессов (указываются с помощью <тип> и <режим>), потраченное на <состояние> в процентах. Расчет производится только за последнюю минуту.<br><br>Если в поле <режим> указан номер процесса Zabbix, который не запущен (например, при 5 запущенных поллерах, в <режиме> указано 6), такой элемент данных переходит в состояние *не поддерживается*.<br>Минимум и максимум относится к процентному использованию одним процессом. Таким образом, если в группе из 3 поллеров процентное использование составило 2, 18 и 66, тогда min должен вернуть 2, а max должен вернуть 66.<br>Процессы сообщают то, что они делают, используя разделяемую память, и процесс само-диагностики резюмирует эти данные каждую секунду. Изменения состояний (busy/idle) регистрируются при изменении - таким образом, процессы, которые становятся занятыми, регистрируют это состояние и не меняют и не обновляют его пока процесс не станет свободным. Такое поведение гарантирует, что даже полностью зависшие процессы будут корректно зарегистрированы как 100% занятые.<br>В настоящее время, "busy" означает "не спит", но в будущем могут добавиться дополнительные состояния - ожидание блокировки, выполнение запросов в базу данных, и т.д.<br>В Linux и в большинстве других систем, точность составляет 1/100 секунды.|<|<|Процент времени.<br>Целое с плавающей точкой.|На данный момент поддерживаются следующие типы процессов:<br>**alerter** - процесс отправки уведомлений (*не поддерживается прокси*)<br>**alert manager** - менеджер задач оповещения<br>**configuration syncer** - процесс управления кэшем данных конфигурации в оперативной памяти<br>**data sender** - процесс отправки данных с прокси (*не поддерживается сервером*)<br>**discoverer** - процесс обнаружения устройств<br>**escalator** - процесс эскалации действий (*не поддерживается прокси*)<br>**heartbeat sender** - процесс уведомления сервера прокси сервером о состоянии прокси (*не поддерживается сервером*)<br>**history syncer** - процесс, который записывает историю в БД<br>**housekeeper** - процесс удаления старых данных истории<br>**http poller** - поллер веб-мониторинга<br>**icmp pinger** - поллер проверок icmpping<br>**ipmi manager** - менеджер IPMI поллеров<br>**ipmi poller** - поллер для проверок по IPMI<br>**java poller** - поллер для Java проверок<br>**poller** - обычный поллер для пассивных проверок<br>**preprocessing manager** - менеджер задач предобработки<br>**preprocessing worker** - процесс предобработки данных<br>**proxy poller** - поллер для пассивных прокси (*не поддерживается прокси*)<br>**self-monitoring** - процесс сбора внутренней статистики сервера<br>**snmp trapper** - траппер сбора/обработки SNMP трапов<br>**task manager** - процесс для удаленного выполнения задач, которые запрашиваются другими компонентами (например, возможности закрытия проблемы, подтверждения проблемы, принудительной проверки значения элемента данных, удаленной команды)<br>**timer** - процесс обработки обслуживаний<br>**trapper** - процесс-улавливатель для активных проверок, трапов, связей между нодами и прокси<br>**unreachable poller** - поллер недоступных устройств<br>**vmware collector** - коллектор данных VMware, ответственный за сбор данных со служб Vmware<br><br>Примечание: Вы также можете увидеть все эти типы процессов в файле журнала сервера.<br><br>Допустимые режимы:<br>**avg** - среднее значение по всем процессам указанного типа (по умолчанию)<br>**count** - количество форков указанного типа процесса, **<состояние>** не должно быть указано<br>**max** - максимальное значение<br>**min** - минимальное значение<br>**<номер процесса>** - номер процесса (от 1 до количества префорк процессов). Например, если запущенно 4 траппера, тогда значение от 1 до 4.<br><br>Допустимые состояния:<br>**busy** - процесс в занятом состоянии, например, обработка запроса (по умолчанию).<br>**idle** - процесс в свободном состоянии, ничего не делающий.<br><br>Примеры:<br>=> zabbix\[process,poller,avg,busy\] → среднее время, потраченное процессами поллеров, которые что-либо делали за последнюю минуту<br>=> zabbix\[process,"icmp pinger",max,busy\] → максимальное время, потраченное любыми процессами ICMP pinger, которые что-либо делали за последнюю минуту<br>=> zabbix\[process,"history syncer",2,busy\] → время, потраченное на что-то процессом синхронизации истории номер 2 в течении последней минуты<br>=> zabbix\[process,trapper,count\] → общее количество запущенных процессов trapper<br><br>Этот элемент данных поддерживается начиная с Zabbix **1.8.5**.|
|zabbix\[proxy,<имя>,<параметр>\]|<|<|<|<|<|
|<|Информация доступности по Zabbix прокси.|<|<|Целое число.|<имя> - Имя прокси<br>Список поддерживаемых параметров (<параметр>):<br>lastaccess – штамп времени последнего сообщения о доступности от прокси<br><br>Например, => zabbix\[proxy,"Germany",lastaccess\]<br><br>Можно использовать [функцию триггеров](/ru/manual/appendix/triggers/functions) fuzzytime() для проверки доступности прокси.<br>Начиная с Zabbix 2.4.0 этот элемент данных всегда обрабатывается Zabbix сервером независимо от настроек узла сети (наблюдается через сервер или прокси).|
|zabbix\[proxy\_history\]|<|<|<|<|<|
|<|Количество значений в таблице истории прокси ожидающих отправку на сервер|<|<|Целое число.|Данный элемент данных поддерживается начиная с Zabbix **2.2.0**<br>*(не поддерживается сервером)*|
|zabbix\[queue,<от>,<до>\]|<|<|<|<|<|
|<|Количество наблюдаемых элементов данных в очереди, которые задерживаются <от> и <до> секунд включительно.|<|<|Целое число.|<от> - по умолчанию: 6 секунд<br><до> - по умолчанию: бесконечно<br>Для этих параметров поддерживаются [символы времени](ru/manual/appendix/suffixes) (s,m,h,d,w).<br>Параметры `от` и `до` поддерживаются начиная с Zabbix **1.8.3**.|
|zabbix\[rcache,<кэш>,<режим>\]|<|<|<|<|<|
|<|Статистика доступности кэша кофигурации|<|<|Целое число (для размера):<br>Целое с плавающей точкой (для процентов).|Кэш: **buffer**<br>Режим:<br>**total** - полный размер буфера<br>**free** - размер свободного места в буфере<br>**pfree** - процент свободного места в буфере<br>**used** - размер использованного места в буфере|
|zabbix\[requiredperformance\]|<|<|<|<|<|
|<|Требуемое быстродействие Zabbix сервера, имеется ввиду новые значения в секунду.|<|<|Целое с плавающей точкой.|Приблизительно соответствует значению "Требуемое быстродействие сервера, новые значения в секунду" со страницы *Отчеты → [Информация о системе](/ru/manual/web_interface/frontend_sections/reports/status_of_zabbix)*. Поддерживается начиная с Zabbix **1.6.2**.|
|zabbix\[trends\]|<|<|<|<|<|
|<|Количество значений хранимых в таблице TRENDS|<|<|Целое число.|Не используйте c MySQL InnoDB, Oracle или PostgreSQL!<br>*(не поддерживается прокси)*|
|zabbix\[trends\_uint\]|<|<|<|<|<|
|<|Количество значений хранимых в таблице TRENDS\_UINT|<|<|Целое число.|Не используйте c MySQL InnoDB, Oracle или PostgreSQL!<br>Этот элемент данных поддерживается начиная с Zabbix **1.8.3**.<br>*(не поддерживается прокси)*|
|zabbix\[triggers\]|<|<|<|<|<|
|<|Количество активированных триггеров в базе данных Zabbix, со всеми активированными элементами данных, которые упомянуты в выражениях триггеров на активированных узлах сети.|<|<|Целое число.|*(не поддерживается прокси)*|
|zabbix\[uptime\]|<|<|<|<|<|
|<|Время непрерывной работы процесса Zabbix сервера в секундах.|<|<|Целое число.|<|
|zabbix\[vcache,buffer,<режим>\]|<|<|<|<|<|
|<|Статистика доступности кэша значений.|<|<|Целое число (для размера);<br>Целое с плавающей точкой (для процентов).|Режим:<br>**total** - полный размер буфера<br>**free** - размер свободного места в буфере<br>**pfree** - процент свободного места в буфере<br>**used** - размер использованного места в буфере<br>**pused** - процент использованного места в буфере<br><br>Данный элемент данных поддерживается с Zabbix **2.2.0**. *(не поддерживается прокси)*|
|zabbix\[vcache,cache,<параметр>\]|<|<|<|<|<|
|<|Статистика эффективности кэша значений Zabbix.|<|<|Целое число.<br><br>С параметром равным **mode**:<br>0 - нормальный режим,<br>1 - режим низкой памяти|Параметр:<br>**requests** - общее количество запросов<br>**hits** - количество попаданий в кэш (история берется из кэша)<br>**phits** - процент попаданий в кэш<br>**misses** - количество непопаданий в кэш (история берется из базы данных)<br>**mode** - режим работы кэша значений<br><br>Данный элемент данных поддерживается с Zabbix **2.2.0** и параметр **mode** поддерживается начиная с Zabbix **3.0.0**. *(не поддерживается прокси)*<br><br>Вы можете использовать этот ключ с шагом предобработки *Изменение в секунду* при необходимости получения статистики значений в секунду.|
|zabbix\[vmware,buffer,<режим>\]|<|<|<|<|<|
|<|Статистика доступности кэша VMware.|<|<|Целое число (для размера);<br>Целое с плавающей точкой (для процентов).|Режим:<br>**total** - полный размер буфера<br>**free** - размер свободного места в буфере<br>**pfree** - процент свободного места в буфере<br>**used** - размер использованного метса в буфере<br>**pused** - процент использованного места в буфере<br><br>Данный элемент данных поддерживается с Zabbix **2.2.0**.|
|zabbix\[wcache,<кэш>,<режим>\]|<|<|<|<|<|
|<|Статистика и доступность кэша записи Zabbix.|<|<|<|Требуется обязательно указывать <кэш>.|
|<|**Кэш**|**Режим**|<|<|<|
|^|values|all<br>*(по умолчанию)*|Количество значений обрабатываемых Zabbix сервером или Zabbix прокси, исключая не поддерживаемые|Целое число.|Счетчик.<br>Вы можете использовать этот ключ с шагом предобработки *Изменение в секунду* при необходимости получения статистики значений в секунду.|
|^|^|float|Количество обработанных значений с плавающей точкой.|Целое число.|Счетчик.|
|^|^|uint|Количество обработанных целочисленных значений.|Целое число.|Счетчик.|
|^|^|str|Количество обработанных символьных/строковых значений.|Целое число.|Счетчик.|
|^|^|log|Количество обработанных значений из файлов журналов.|Целое число.|Счетчик.|
|^|^|text|Количество обработанных текстовых значений.|Целое число.|Счетчик.|
|^|^|not supported|Количество неподдерживаемых элементов данных.|Целое число.|Счетчик.<br>Режим *not supported* поддерживается начиная с Zabbix **1.8.6**.|
|^|history|pfree<br>*(по умолчанию)*|Свободное место в буфере истории в процентах.|Число с плавающей точкой.|Кэш истории используется для хранения значений элементов данных. Низкое количество свободного места отражает проблемы с производительностью на стороне базы данных.|
|^|^|free|Размер свободного места в буфере истории.|Целое число.|<|
|^|^|total|Полный размер буфера.|Целое число.|<|
|^|^|used|Размер занятого места в буфере истории.|Целое число.|<|
|^|index|pfree<br>*(по умолчанию)*|Свободное место в буфере индексов истории.|Целое с плавающей точкой.|Кэш индексов истории используется для индексирования значений записываемых в кэш истории.<br>Кэш *индекса* поддерживается начиная с Zabbix **3.0.0**.|
|^|^|free|Свободное место в буфере индексов истории.|Целое число.|<|
|^|^|total|Полный размер буфера индексов истории.|Целое число.|<|
|^|^|used|Размер занятого места в буфере индексов истории.|Целое число.|<|
|^|trend|pfree<br>*(по умолчанию)*|Свободное место в буфере динамики изменений в процентах.|Число с плавающей точкой.|Кэш динамики изменений аггрегируется за текущий час для всех элементов данных, которые получают данные.<br>*(не поддерживается прокси)*|
|^|^|free|Свободное место в буфере динамики изменений.|Целое число.|*(не поддерживается прокси)*|
|^|^|total|Полный размер буфера динамики изменений.|Целое число.|*(не поддерживается прокси)*|
|^|^|used|Размер занятого места в буфере динамики изменений.|Целое число|*(не поддерживается прокси)*|

[comment]: # ({/new-74b446f4})

[comment]: # ({new-0f6066ab})

### Item key details

-   Parameters without angle brackets are constants - for example,
    'host' and 'available' in `zabbix[host,<type>,available]`. Use them in the item key *as is*.
-   Values for items and item parameters that are "not supported on proxy" can only be retrieved if the host is monitored by server. And vice versa, values "not supported on server" can only be retrieved if the host is monitored by proxy.

[comment]: # ({/new-0f6066ab})

[comment]: # ({new-e52bbd1a})

##### zabbix[boottime] {#boottime}

<br>
The startup time of Zabbix server or Zabbix proxy process in seconds.<br>
Return value: *Integer*.

[comment]: # ({/new-e52bbd1a})

[comment]: # ({new-d1cde038})

##### zabbix[cluster,discovery,nodes] {#cluster.discovery}

<br>
Discovers the [high availability cluster](/manual/concepts/server/ha) nodes.<br>
Return value: *JSON object*.

This item can be used in low-level discovery.

[comment]: # ({/new-d1cde038})

[comment]: # ({new-09c4f0cf})

##### zabbix[connector_queue] {#connector.queue}

<br>
The count of values enqueued in the connector queue.<br>
Return value: *Integer*.

This item is supported since Zabbix 6.4.0.

[comment]: # ({/new-09c4f0cf})

[comment]: # ({new-fcd989ee})

##### zabbix[discovery_queue] {#discovery.queue}

<br>
The count of network checks enqueued in the discovery queue.<br>
Return value: *Integer*.

[comment]: # ({/new-fcd989ee})

[comment]: # ({new-f065c85f})

##### zabbix[host,,items] {#host.items}

<br>
The number of enabled items (supported and not supported) on the host.<br>
Return value: *Integer*.

[comment]: # ({/new-f065c85f})

[comment]: # ({new-31a0bdcc})

##### zabbix[host,,items_unsupported] {#host.items.unsupported}

<br>
The number of enabled unsupported items on the host.<br>
Return value: *Integer*.

[comment]: # ({/new-31a0bdcc})

[comment]: # ({new-6220db31})

##### zabbix[host,,maintenance] {#maintenance}

<br>
The current maintenance status of the host.<br>
Return values: *0* - normal state; *1* - maintenance with data collection; *2* - maintenance without data collection.

Comments:

-   This item is always processed by Zabbix server regardless of the host location (on server or proxy). The proxy will not receive this item with configuration data. 
-   The second parameter must be empty and is reserved for future use.

[comment]: # ({/new-6220db31})

[comment]: # ({new-ec7187d4})

##### zabbix[host,active_agent,available] {#active.available}

<br>
The availability of active agent checks on the host.<br>
Return values: *0* - unknown; *1* - available; *2* - not available.

[comment]: # ({/new-ec7187d4})

[comment]: # ({new-48ad1e26})

##### zabbix[host,discovery,interfaces] {#discovery.interfaces}

<br>
The details of all configured interfaces of the host in Zabbix frontend.<br>
Return value: *JSON object*.

Comments:

-   This item can be used in [low-level discovery](/manual/discovery/low_level_discovery/examples/host_interfaces);
-   This item is not supported on Zabbix proxy.

[comment]: # ({/new-48ad1e26})

[comment]: # ({new-9752a684})

##### zabbix[host,<type>,available] {#host.available}

<br>
The availability of the main interface of a particular type of checks on the host.<br>
Return values: *0* - not available; *1* - available; *2* - unknown.

Comments:

-   Valid **types** are: *agent*, *snmp*, *ipmi*, *jmx*;
-   The item value is calculated according to the configuration parameters regarding host [unreachability/unavailability](/manual/appendix/items/unreachability).

[comment]: # ({/new-9752a684})

[comment]: # ({new-a65ef5bf})

##### zabbix[hosts] {#hosts}

<br>
The number of monitored hosts.<br>
Return value: *Integer*.

[comment]: # ({/new-a65ef5bf})

[comment]: # ({new-f291ac50})

##### zabbix[items] {#items}

<br>
The number of enabled items (supported and not supported).<br>
Return value: *Integer*.

[comment]: # ({/new-f291ac50})

[comment]: # ({new-53295b6d})

##### zabbix[items_unsupported] {#items.unsupported}

<br>
The number of unsupported items.<br>
Return value: *Integer*.

[comment]: # ({/new-53295b6d})

[comment]: # ({new-ba37fff4})

##### zabbix[java,,<param>] {#java}

<br>
The information about Zabbix Java gateway.<br>
Return values: *1* - if <param> is *ping*; *Java gateway version* -  if <param> is *version* (for example: "2.0.0").

Comments:

-   Valid values for **param** are: *ping*, *version*;
-   This item can be used to check Java gateway availability using the `nodata()` trigger function;
-   The second parameter must be empty and is reserved for future use.

[comment]: # ({/new-ba37fff4})

[comment]: # ({new-8f8235d6})

##### zabbix[lld_queue] {#lld.queue}

<br>
The count of values enqueued in the low-level discovery processing queue.<br>
Return value: *Integer*.

This item can be used to monitor the low-level discovery processing queue length.

[comment]: # ({/new-8f8235d6})

[comment]: # ({new-3e07a93d})

##### zabbix[preprocessing_queue] {#preprocessing.queue}

<br>
The count of values enqueued in the preprocessing queue.<br>
Return value: *Integer*.

This item can be used to monitor the preprocessing queue length.

[comment]: # ({/new-3e07a93d})

[comment]: # ({new-b74b9537})

##### zabbix[process,<type>,<mode>,<state>] {#process}

<br>
The percentage of time a particular Zabbix process or a group of processes (identified by \<type\> and \<mode\>) spent in \<state\>. It is calculated for the last minute only.<br>
Return value: *Float*.

Parameters:

-   **type** - for [server processes](/manual/concepts/server#server_process_types): *alert manager*, *alert syncer*, *alerter*, *availability manager*, *configuration syncer*, *connector manager*, *connector worker*, *discoverer*, *escalator*, *history poller*, *history syncer*, *housekeeper*, *http poller*, *icmp pinger*, *ipmi manager*, *ipmi poller*, *java poller*, *lld manager*, *lld worker*, *odbc poller*, *poller*, *preprocessing manager*, *preprocessing worker*, *proxy poller*, *self-monitoring*, *snmp trapper*, *task manager*, *timer*, *trapper*, *unreachable poller*, *vmware collector*;<br>for [proxy processes](/manual/concepts/proxy#proxy_process_types): *availability manager*, *configuration syncer*, *data sender*, *discoverer*, *history syncer*, *housekeeper*, *http poller*, *icmp pinger*, *ipmi manager*, *ipmi poller*, *java poller*, *odbc poller*, *poller*, *preprocessing manager*, *preprocessing worker*, *self-monitoring*, *snmp trapper*, *task manager*, *trapper*, *unreachable poller*, *vmware collector*
-   **mode** - *avg* - average value for all processes of a given type (default)<br>*count* - returns number of forks for a given process type, <state> should not be specified<br>*max* - maximum value<br>*min* - minimum value<br>*<process number>* - process number (between 1 and the number of pre-forked instances). For example, if 4 trappers are running, the value is between 1 and 4.
-   **state** - *busy* - process is in busy state, for example, the processing request (default); *idle* - process is in idle state doing nothing.

Comments:

-   If \<mode\> is a Zabbix process number that is not running (for example, with 5 pollers running the \<mode\> is specified to be 6), such an item will turn unsupported;
-   Minimum and maximum refers to the usage percentage for a single process. So if in a group of 3 pollers usage percentages per process were 2, 18 and 66, min would return 2 and max would return 66.
-   Processes report what they are doing in shared memory and the self-monitoring process summarizes that data each second. State changes (busy/idle) are registered upon change - thus a process that becomes busy registers as such and doesn't change or update the state until it becomes idle. This ensures that even fully hung processes will be correctly registered as 100% busy.
-   Currently, "busy" means "not sleeping", but in the future additional states might be introduced - waiting for locks, performing database queries, etc.
-   On Linux and most other systems, resolution is 1/100 of a second.

Examples:

    zabbix[process,poller,avg,busy] #the average time of poller processes spent doing something during the last minute
    zabbix[process,"icmp pinger",max,busy] #the maximum time spent doing something by any ICMP pinger process during the last minute
    zabbix[process,"history syncer",2,busy] #the time spent doing something by history syncer number 2 during the last minute
    zabbix[process,trapper,count] #the amount of currently running trapper processes

[comment]: # ({/new-b74b9537})

[comment]: # ({new-d90bc125})

##### zabbix[proxy,<name>,<param>] {#proxy}

<br>
The information about Zabbix proxy.<br>
Return value: *Integer*.

Parameters:

-   **name** - the proxy name;
-   **param** - *delay* - how long the collected values are unsent, calculated as "proxy delay" (difference between the current proxy time and the timestamp of the oldest unsent value on proxy) + ("current server time" - "proxy lastaccess").

[comment]: # ({/new-d90bc125})

[comment]: # ({new-fd9ea3f4})

##### zabbix[proxy,discovery] {#proxy.discovery}

<br>
The list of Zabbix proxies with name, mode, encryption, compression, version, last seen, host count, item count, required values per second (vps) and version status (current/outdated/unsupported).<br>
Return value: *JSON object*.

[comment]: # ({/new-fd9ea3f4})

[comment]: # ({new-5b463b30})

##### zabbix[proxy_history] {#proxy.history}

<br>
The number of values in the proxy history table waiting to be sent to the server.<br>
Return values: *Integer*.

This item is not supported on Zabbix server.

[comment]: # ({/new-5b463b30})

[comment]: # ({new-ebe20dec})

##### zabbix[queue,<from>,<to>] {#queue}

<br>
The number of monitored items in the queue which are delayed at least by \<from\> seconds, but less than \<to\> seconds.<br>
Return value: *Integer*.

Parameters:

-   **from** - default: 6 seconds;
-   **to** - default: infinity.

[Time-unit symbols](/manual/appendix/suffixes) (s,m,h,d,w) are supported in the parameters.

[comment]: # ({/new-ebe20dec})

[comment]: # ({new-04aafd0c})

##### zabbix[rcache,<cache>,<mode>] {#rcache}

<br>
The availability statistics of the Zabbix configuration cache.<br>
Return values: *Integer* (for size); *Float* (for percentage).

Parameters:

-   **cache** - *buffer*;
-   **mode** - *total* - the total size of buffer<br>*free* - the size of free buffer<br>*pfree* - the percentage of free buffer<br>*used* - the size of used buffer<br>*pused* - the percentage of used buffer

[comment]: # ({/new-04aafd0c})

[comment]: # ({new-f2b37a90})

##### zabbix[requiredperformance] {#required.performance}

<br>
The required performance of Zabbix server or Zabbix proxy, in new values per second expected.<br>
Return value: *Float*.

Approximately correlates with "Required server performance, new values per second" in *Reports → [System information](/manual/web_interface/frontend_sections/reports/status_of_zabbix)*.

[comment]: # ({/new-f2b37a90})

[comment]: # ({new-8054c8ab})

##### zabbix[stats,<ip>,<port>] {#stats}

<br>
The internal metrics of a remote Zabbix server or proxy.<br>
Return values: *JSON object*.

Parameters:

-   **ip** - the IP/DNS/network mask list of servers/proxies to be remotely queried (default is 127.0.0.1);
-   **port** - the port of server/proxy to be remotely queried (default is 10051).

Comments:

-   The stats request will only be accepted from the addresses listed in the 'StatsAllowedIP' [server](/manual/appendix/config/zabbix_server)/[proxy](/manual/appendix/config/zabbix_proxy) parameter on the target instance;
-   A selected set of internal metrics is returned by this item. For details, see [Remote monitoring of Zabbix stats](/manual/appendix/items/remote_stats#exposed_metrics).

[comment]: # ({/new-8054c8ab})

[comment]: # ({new-d7c2c9be})

##### zabbix[stats,<ip>,<port>,queue,<from>,<to>] {#stats.queue}

<br>
The internal queue metrics (see `zabbix[queue,<from>,<to>]`) of a remote Zabbix server or proxy.<br>
Return values: *JSON object*.

Parameters:

-   **ip** - the IP/DNS/network mask list of servers/proxies to be remotely queried (default is 127.0.0.1);
-   **port** - the port of server/proxy to be remotely queried (default is 10051);
-   **from** - delayed by at least (default is 6 seconds);
-   **to** - delayed by at most (default is infinity).

Comments:

-   The stats request will only be accepted from the addresses listed in the 'StatsAllowedIP' [server](/manual/appendix/config/zabbix_server)/[proxy](/manual/appendix/config/zabbix_proxy) parameter on the target instance;
-   A selected set of internal metrics is returned by this item. For details, see [Remote monitoring of Zabbix stats](/manual/appendix/items/remote_stats#exposed_metrics).

[comment]: # ({/new-d7c2c9be})

[comment]: # ({new-c688d590})

##### zabbix[tcache,<cache>,<parameter>] {#tcache}

<br>
The effectiveness statistics of the Zabbix trend function cache.<br>
Return values: *Integer* (for size); *Float* (for percentage).

Parameters:

-   **cache** - *buffer*;
-   **mode** - **all* - total cache requests (default)<br>*hits* - cache hits<br>*phits* - percentage of cache hits<br>*misses* - cache misses<br>*pmisses* - percentage of cache misses<br>*items* - the number of cached items<br>*requests* - the number of cached requests<br>*pitems* - percentage of cached items from cached items + requests. Low percentage most likely means that the cache size can be reduced.

This item is not supported on Zabbix proxy.

[comment]: # ({/new-c688d590})

[comment]: # ({new-9699e4ca})

##### zabbix[triggers] {#triggers}

<br>
The number of enabled triggers in Zabbix database, with all items enabled on enabled hosts.<br>
Return value: *Integer*.

This item is not supported on Zabbix proxy.

[comment]: # ({/new-9699e4ca})

[comment]: # ({new-a5c8cdec})

##### zabbix[uptime] {#uptime}

<br>
The uptime of the Zabbix server or proxy process in seconds.<br>
Return value: *Integer*.

[comment]: # ({/new-a5c8cdec})

[comment]: # ({new-304cb09c})

##### zabbix[vcache,buffer,<mode>] {#vcache}

<br>
The availability statistics of the Zabbix value cache.<br>
Return values: *Integer* (for size); *Float* (for percentage).

Parameter:

-   **mode** - *total* - the total size of buffer<br>*free* - the size of free buffer<br>*pfree* - the percentage of free buffer<br>*used* - the size of used buffer<br>*pused* - the percentage of used buffer

This item is not supported on Zabbix proxy.

[comment]: # ({/new-304cb09c})

[comment]: # ({new-1454b994})

##### zabbix[vcache,cache,<parameter>] {#vcache.parameter}

<br>
The effectiveness statistics of the Zabbix value cache.<br>
Return values: *Integer*. With the *mode* parameter returns: 0 - normal mode; 1 - low memory mode.

Parameters:

-   **parameter** - *requests* - the total number of requests<br>*hits* - the number of cache hits (history values taken from the cache)<br>*misses* - the number of cache misses (history values taken from the database)<br>*mode* - the value cache operating mode

Comments:

-   Once the low-memory mode has been switched on, the value cache will remain in this state for 24 hours, even if the problem that triggered this mode is resolved sooner;
-   You may use this key with the *Change per second* preprocessing step in order to get values-per-second statistics;
-   This item is not supported on Zabbix proxy.

[comment]: # ({/new-1454b994})

[comment]: # ({new-ddde46a0})

##### zabbix[version] {#version}

<br>
The version of Zabbix server or proxy.<br>
Return value: *String*. For example: `6.0.0beta1`.

[comment]: # ({/new-ddde46a0})

[comment]: # ({new-dda0c008})

##### zabbix[vmware,buffer,<mode>] {#vmware}

<br>
The availability statistics of the Zabbix vmware cache.<br>
Return values: *Integer* (for size); *Float* (for percentage).

Parameters:

-   **mode** - *total* - the total size of buffer<br>*free* - the size of free buffer<br>*pfree* - the percentage of free buffer<br>*used* - the size of used buffer<br>*pused* - the percentage of used buffer

[comment]: # ({/new-dda0c008})

[comment]: # ({new-9fa76fcd})

##### zabbix[wcache,<cache>,<mode>] {#wcache}

<br>
The statistics and availability of the Zabbix write cache.<br>
Return values: *Integer* (for number/size); *Float* (for percentage).

Parameters:

-   **cache** - *values*, *history*, *index*, or *trend*;
-   **mode** - (with *values*) *all* (default) - the total number of values processed by Zabbix server/proxy, except unsupported items (counter)<br>*float* - the number of processed float values (counter)<br>*uint* - the number of processed unsigned integer values (counter)<br>*str* - the number of processed character/string values (counter)<br>*log* - the number of processed log values (counter)<br>*text* - the number of processed text values (counter)<br>*not supported* - the number of times item processing resulted in item becoming unsupported or keeping that state (counter)<br>(with *history*, *index*, *trend* cache) *pfree* (default) - the percentage of free buffer<br>*total* - the total size of buffer<br>*free* - the size of free buffer<br>*used* - the size of used buffer<br>*pused* - the percentage of used buffer

Comments:

-   Specifying \<cache\> is mandatory. The `trend` cache parameter is not supported with Zabbix proxy.
-   The history cache is used to store item values. A low number indicates performance problems on the database side.
-   The history index cache is used to index the values stored in the history cache;
-   The trend cache stores the aggregate for the current hour for all items that receive data;
-   You may use the zabbix[wcache,values] key with the *Change per second* preprocessing step in order to get values-per-second statistics.

[comment]: # ({/new-9fa76fcd})
