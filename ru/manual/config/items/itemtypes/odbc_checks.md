[comment]: # translation:outdated

[comment]: # ({new-68c88a1c})
# 14 ODBC мониторинг

[comment]: # ({/new-68c88a1c})

[comment]: # ({new-b5170fd3})
#### Обзор

ODBC мониторинг соответствует типу элемента данных *Монитор баз данных*
в веб-интерфейсе Zabbix.

ODBC - язык программирования на C, промежуточная прослойка API для
доступа к системам управления баз данных (DBMS). Концепт ODBC был
разработан Microsoft и в дальнейшем портирован на другие платформы.

Zabbix может выполнять запросы к любой базе данных, которая
поддерживается ODBC. Чтобы это сделать, Zabbix не подключается напрямую
к базам данных, он использует интерфейс ODBC и драйвера установленные в
ODBC. Эта функция позволяет мониторить различные базы данных с
различными целями с большей эффективностью - например, проверка
специфичных запросов к базе данных, статистика использования и прочее.
Zabbix поддерживает unixODBC, которая наиболее часто используются в
реализациях ODBC API с открытым исходным кодом.

[comment]: # ({/new-b5170fd3})

[comment]: # ({new-bf6fd8ba})
#### Установка unixODBC

Предлагаемый вариант установки unixODBC состоит из использования
репозитариев пакетов по умолчанию в Linux операционной системы. В
наиболее популярные дистрибутивы Linux unixODBC включен в репозитарии
пакетов по умолчанию. Если он недоступен, вы можете обратиться к
домашней странице UnixODBC: <http://www.unixodbc.org/download.html>.

Установка unixODBC на системы на базе RedHat/Fedora с использованием
менеджера пакетов *yum*:

    shell> yum -y install unixODBC unixODBC-devel

Установка unixODBC на системы на базе SUSE с использованием менеджера
пакетов *zypper*:

    # zypper in unixODBC-devel

::: noteclassic
Пакет unixODBC-devel требуется для компиляции Zabbix с
поддержкой unixODBC.
:::

[comment]: # ({/new-bf6fd8ba})

[comment]: # ({new-eea09ed7})
#### Установка драйверов unixODBC

Драйвер unixODBC базы данных должен быть установлен для базы данных,
которая будет наблюдаться. unixODBC имеет список поддерживаемых баз
данных и драйверов: <http://www.unixodbc.org/drivers.html>. В некоторых
дистрибутивах Linux драйвера баз данных включены в репозитарии пакетов.
Драйвера MySQL базы данных на системы на базе RedHat/Fedora можно
установить с помощью менеджера пакетов *yum*:

    shell> yum install mysql-connector-odbc

Установка MySQL драйвера на системы на базе SUSE с использованием
менеджера пакетов *zypper*:

    # zypper in MyODBC-unicODBC

[comment]: # ({/new-eea09ed7})

[comment]: # ({new-b15e771b})
#### Настройка unixODBC

Настройка ODBC выполняется редактированием файлов **odbcinst.ini** и
**odbc.ini**. Для проверки размещения этих файлов введите:

    shell> odbcinst -j

**odbcinst.ini** используется для перечисления установленных драйверов
баз данных ODBC:

    [mysql]
    Description = ODBC for MySQL
    Driver      = /usr/lib/libmyodbc5.so

Подробная информация:

|Атрибут|Описание|
|--------------|----------------|
|*mysql*|Имя драйвера базы данных.|
|*Description*|Описание драйвера базы данных.|
|*Driver*|Размещение библиотеки драйвера базы данных.|

**odbc.ini** используется для определения источников данных:

    [test]
    Description = MySQL test database
    Driver      = mysql
    Server      = 127.0.0.1
    User        = root
    Password    =
    Port        = 3306
    Database    = zabbix

Подробная информация:

|Атрибут|Описание|
|--------------|----------------|
|*test*|Имя источника данных (DSN).|
|*Description*|Описание источника данных.|
|*Driver*|Имя драйвера базы данных - как указано в odbcinst.ini|
|*Server*|IP/DNS сервера базы данных.|
|*User*|Пользователь базы данных для подключения.|
|*Password*|Пароль к базе данных.|
|*Port*|Порт подключения к базе данных.|
|*Database*|Имя базы данных.|

Для проверки работает ли соединение ODBC корректно, подключение к базе
данных необходимо протестировать. Для этого можно воспользоваться
утилитой **isql** (включена в пакет unixODBC):

    shell> isql test
    +---------------------------------------+
    | Connected!                            |
    |                                       |
    | sql-statement                         |
    | help [tablename]                      |
    | quit                                  |
    |                                       |
    +---------------------------------------+
    SQL>

[comment]: # ({/new-b15e771b})

[comment]: # ({new-9f87c9b6})
#### Компиляция Zabbix с поддержкой ODBC

Для включения поддержки ODBC, Zabbix должен быть скомпилирован со
следующим флагом:

      --with-unixodbc[=ARG]   use odbc driver against unixODBC package

::: noteclassic
Смотрите более подробную информацию о установке Zabbix из
[исходных кодов](/ru/manual/installation/install).
:::

[comment]: # ({/new-9f87c9b6})

[comment]: # ({new-ca784f43})
#### Настройка элемента данных в веб-интерфейсе Zabbix

Настройка [элемента данных](/ru/manual/config/items/item#обзор) для
мониторинга базы данных.

![](../../../../../assets/en/manual/config/items/itemtypes/odbc.png)

Все обязательные поля ввода отмечены красной звёздочкой.

Специально для элементов данных мониторинга баз данных вы должны
указать:

|   |   |
|---|---|
|*Тип*|Выберите здесь *Монитор баз данных*.|
|*Ключ*|Введите **db.odbc.select**\[уникальное\_описание,имя\_источника\_данных\]<br>Уникальное описание будет служить идентификатором элемента данных в триггерах и тому подобном.<br>Имя источника данных (DSN) должно быть указано как в настройках odbc.ini.|
|*Имя пользователя*|Введите имя пользователя для доступа к базе данных (опционально, если пользователь указан в odbc.ini)|
|*Пароль*|Введите пароль пользователя для доступа к базе данных (опционально, если пароль указан в odbc.ini)|
|*SQL запрос*|Введите необходимый SQL запрос|
|*Тип информации*|Очень важно знать какой тип информации будет возвращаться указанным запросом, то есть выберите корректный тип информации здесь. С некорректным *типом информации* элемент данных станет неподдерживаемым.|

[comment]: # ({/new-ca784f43})

[comment]: # ({new-0188aaa2})
#### Item key details

Parameters without angle brackets are mandatory. Parameters marked with angle brackets **<** **>** are optional.

[comment]: # ({/new-0188aaa2})

[comment]: # ({new-13edfcd4})

##### db.odbc.select[<unique short description>,<dsn>,<connection string>] {#db.odbc.select}

<br>
Returns one value, i.e. the first column of the first row of the SQL query result.
Return value: depending on the SQL query.

Parameters:

-   **unique short description** - a unique short description to identify the item (for use in triggers, etc);
-   **dsn** - the data source name (as specified in odbc.ini);
-   **connection string** - the connection string (may contain driver-specific arguments).

Comments:

-   Although `dsn` and `connection string` are optional parameters, at least one of them must be present. If both data source name (DSN) and connection string are defined, the DSN will be ignored.
-   If a query returns more than one column, only the first column is read. If a query returns more than one line, only the first line is read.

[comment]: # ({/new-13edfcd4})

[comment]: # ({new-ebf5d449})

##### db.odbc.get[<unique short description>,<dsn>,<connection string>] {#db.odbc.get}

<br>
Transforms the SQL query result into a JSON array.<br>
Return value: *JSON object*.

Parameters:

-   **unique short description** - a unique short description to identify the item (for use in triggers, etc);
-   **dsn** - the data source name (as specified in odbc.ini);
-   **connection string** - the connection string (may contain driver-specific arguments).

Comments:

-   Although `dsn` and `connection string` are optional parameters, at least one of them must be present. If both data source name (DSN) and connection string are defined, the DSN will be ignored.
-   Multiple rows/columns in JSON format may be returned. This item may be used as a master item that collects all data in one system call, while JSONPath preprocessing may be used in dependent items to extract individual values. For more information, see an [example](/manual/discovery/low_level_discovery/examples/sql_queries#using-db.odbc.get) of the returned format, used in low-level discovery.

Example:

    db.odbc.get[MySQL example,,"Driver=/usr/local/lib/libmyodbc5a.so;Database=master;Server=127.0.0.1;Port=3306"] #connection for MySQL ODBC driver 5

[comment]: # ({/new-ebf5d449})

[comment]: # ({new-c8148c95})

##### db.odbc.discovery[<unique short description>,<dsn>,<connection string>] {#db.odbc.discovery}

<br>
Transforms the SQL query result into a JSON array, used for [low-level disovery](/manual/discovery/low_level_discovery/examples/sql_queries). 
The column names from the query result are turned into low-level discovery macro 
names paired with the discovered field values. These macros can be used in creating 
item, trigger, etc prototypes.<br>
Return value: *JSON object*.

Parameters:

-   **unique short description** - a unique short description to identify the item (for use in triggers, etc);
-   **dsn** - the data source name (as specified in odbc.ini);
-   **connection string** - the connection string (may contain driver-specific arguments).

Comments:

-   Although `dsn` and `connection string` are optional parameters, at least one of them must be present. If both data source name (DSN) and connection string are defined, the DSN will be ignored.

[comment]: # ({/new-c8148c95})

[comment]: # ({new-06f9d2eb})
#### Важные замечания

-   Zabbix не ограничивает время выполнения запроса. Пользователь вправе
    выбирать запросы, которые могут быть выполнены в разумное время.
-   Значение параметра
    [Timeout](/ru/manual/appendix/config/zabbix_server) с Zabbix сервера
    используется как время ожидания подключения ODBC (обратите внимание,
    в зависимости от драйвера ODBC время ожидания подключения может быть
    проигнорировано).
-   Запрос должен возвращать только одно значение.
-   Если запрос возвращает более чем одну колонку, будет прочитана
    только первая колонка.
-   Если запрос возвращает более чем одну строку, будет прочитана только
    первая строка.
-   Команда SQL должна начинаться с `select`.
-   Команда SQL не должна включать в себя переводы строк.
-   Смотрите также [известные
    проблемы](/ru/manual/installation/known_issues#odbc_проверки) по
    ODBC проверкам

[comment]: # ({/new-06f9d2eb})

[comment]: # ({new-718edfdc})
#### Сообщения об ошибках

Сообщения об ошибках ODBC скомпонованы в поля для предоствления
подробной информации. Например:

    Cannot execute ODBC query: [SQL_ERROR]:[42601][7][ERROR: syntax error at or near ";"; Error while executing the query]
    └───────────┬───────────┘  └────┬────┘ └──┬──┘└┬┘└─────────────────────────────┬─────────────────────────────────────┘
                │                   │         │    └─ Родной код ошибки            └─ Родное сообщение об ошибке
                │                   │         └─ SQLState
                └─ Zabbix сообщение └─ ODBC возвращаемый код

Обратите внимание, что длина сообщения об ошибке ограничена 2048
байтами, поэтому сообщение может быть укорочено. Если есть более одной
ODBC диагностической записи, Zabbix пытается их скомпоновать (разделяя
символом `|`) на сколько позволяет максимальная длина сообщения.

[comment]: # ({/new-718edfdc})
