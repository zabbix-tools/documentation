[comment]: # translation:outdated

[comment]: # ({new-d1352224})
# 17 Проверки Prometheus

[comment]: # ({/new-d1352224})

[comment]: # ({new-06c90457})
#### Обзор

Zabbix может запрашивать метрики, представленные в формате строки
Prometheus.

Чтобы начать сбор данных Prometheus, необходимо настроить два элемента
данных:

-   основной [элемент данных HTTP](/manual/config/items/itemtypes/http),
    указывающий на соответствующую конечную точку данных, например,
    `https://<prometheus host>/metrics`
-   зависимые элементы данных, использующие опцию предварительной
    обработки Prometheus для запроса необходимых данных из метрик,
    собранных основным элементом данных

Существует два варианта предварительной обработки данных Prometheus:

-   *Шаблон Prometheus* - используется в обычных элементах данных для
    запроса метрик Prometheus
-   *Prometheus в JSON* - используется в обычных элементах данных и для
    низкоуровневого обнаружения. В этом случае запрашиваемые данные
    Prometheus возвращаются в формате JSON.

[comment]: # ({/new-06c90457})

[comment]: # ({new-4473dc29})

##### Bulk processing

Bulk processing is supported for dependent items. To enable caching and indexing, 
the *Prometheus pattern* preprocessing must be the **first** preprocessing step. 
When *Prometheus pattern* is first preprocessing step then the parsed Prometheus 
data is cached and indexed by the first `<label>==<value>` condition in the 
*Prometheus pattern* preprocessing step. This cache is reused when processing 
other dependent items in this batch. For optimal performance, the first label 
should be the one with most different values.

If there is other preprocessing to be done before the first step, it should be 
moved either to the master item or to a new dependent item which would be used as 
the master item for the dependent items.

[comment]: # ({/new-4473dc29})

[comment]: # ({new-73ac7ad6})
#### Настройка

Если у вас настроен основной элемент данных HTTP, вам нужно создать
[зависимый элемент
данных](/ru/manual/config/items/itemtypes/dependent_items), который
использует шаг предобработки Prometheus:

-   введите общие параметры зависимого элемента данных в форме
    конфигурации
-   перейдите на вкладку **Предобработка**
-   выберите шаг предобработки *Prometheus* (*Шаблон Prometheus* или
    *Prometheus в JSON*)

![](../../../../../assets/en/manual/config/items/itemtypes/prometheus_item1.png){width="600"}

|Параметр|Описание|Пример|
|----------------|----------------|------------|
|*Шаблон*|Для определения необходимого шаблона данных вы можете использовать язык запросов, который похож на язык запросов Prometheus (см. [таблицу сравнения](#сравнение_языка_запросов)), например:<br><metric name> - выбрать по названию метрики<br>{\_\_name\_\_="<metric name>"} - выбрать по названию метрики<br>{\_\_name\_\_=\~"<regex>"} - выбрать по названию метрики, совпадающему с регулярным выражением<br>{<label name>="<label value>",...} - выбрать по названию метки<br>{<label name>=\~"<regex>",...} - выбрать по имени метки, совпадающему с регулярным выражением<br>{\_\_name\_\_=\~".\*"}==<value> - выбрать по значению метрики<br>Или сочетание вышеперечисленного:<br><metric name>{<label1 name>="<label1 value>",<label2 name>=\~"<regex>",...}==<value><br><br>Значением метки может быть любая последовательность символов UTF-8, но символы обратной косой черты, двойных кавычек и перевода строки должны быть экранированы как `\\`, `\"` и `\n` соответственно; другие символы не должны быть экранированы.|*wmi\_os\_physical\_memory\_free\_bytes*<br>*cpu\_usage\_system{cpu="cpu-total"}*<br>*cpu\_usage\_system{cpu=\~".\*"}*<br>*cpu\_usage\_system{cpu="cpu-total",host=\~".\*"}*<br>*wmi\_service\_state{name="dhcp"}==1*<br>*wmi\_os\_timezone{timezone=\~".\*"}==1*|
|*Вывод*|Определите название метки (необязательно). В этом случае возвращается значение, соответствующее названию метки.<br>Это поле доступно только для шага //Шаблон Prometheus //.|<|

[comment]: # ({/new-73ac7ad6})

[comment]: # ({new-8902eb84})
#### Prometheus в JSON

Данные из Prometheus могут быть использованы для низкоуровневого
обнаружения. В этом случае необходимы данные в формате JSON, и опция
предобработки *Prometheus в JSON* отформатирует их именно таким образом.

См. также: [Обнаружение с использованием данных
Prometheus](/ru/manual/discovery/low_level_discovery/prometheus).

[comment]: # ({/new-8902eb84})

[comment]: # ({new-2fbe720b})
#### Сравнение языка запросов

В следующей таблице перечислены различия и сходства между PromQL и
языком запросов предобработки Prometheus в Zabbix.

|<|[Мгновенный векторный селектор PromQL](https://prometheus.io/docs/prometheus/latest/querying/basics/#instant-vector-selectors)|Предобработка Prometheus в Zabbix|
|-|---------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------|
|**Различия**|<|<|
|Цель запроса|сервер Prometheus|Простой текст в формате экспозиции Prometheus|
|Возвращает|Мгновенный вектор|Значение метрики или метки (шаблон Prometheus)<br>Массив метрик для отдельного значения в JSON (Prometheus в JSON)|
|Операторы соответствия меток|**=**, **!=**, **=\~**, **!\~**|**=**, **=\~**|
|Регулярное выражение, используемое в сопоставлении названия метки или метрики|RE2|PCRE|
|Операторы сравнения|См. [список](https://prometheus.io/docs/prometheus/latest/querying/operators/#comparison-binary-operators)|Только **==** (равно) поддерживается для фильтрации значений|
|**Сходства**|<|<|
|Выбор по названию метрики, равному строке|<metric name> или {\_\_name\_\_="<metric name>"}|<metric name> или {\_\_name\_\_="<metric name>"}|
|Выбор по имени метрики, которое соответствует регулярному выражению|{\_\_name\_\_=\~"<regex>"}|{\_\_name\_\_=\~"<regex>"}|
|Выбор по значению <label name>, равному строке|{<label name>="<label value>",...}|{<label name>="<label value>",...}|
|Выбор по значению <label name>, которое соответствует регулярному выражению|{<label name>=\~"<regex>",...}|{<label name>=\~"<regex>",...}|
|Выбор по значению, равному строке|{\_\_name\_\_=\~".\*"} == <value>|{\_\_name\_\_=\~".\*"} == <value>|

[comment]: # ({/new-2fbe720b})
