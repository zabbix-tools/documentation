[comment]: # translation:outdated

[comment]: # ({new-780b1aa2})
# 15 Ограничение проверок агента

[comment]: # ({/new-780b1aa2})

[comment]: # ({new-9de656b2})
#### Обзор

Можно ограничить проверки на стороне агента, создав черный список
элементов, белый список или комбинацию белого/черного списка.

Для этого используйте комбинацию из двух параметров
[конфигурации](/ru/manual/appendix/config/zabbix_agentd) агента:

-   `AllowKey=<шаблон>` - какие проверки разрешены; <шаблон>
    указывается с использованием выражения с подстановочным знаком (\*)
-   `DenyKey=<шаблон>` - какие проверки запрещены; <шаблон>
    указывается с использованием выражения с подстановочным знаком (\*)

Обратите внимание, что:

-   Все элементы данных `system.run[*]` (скрипты, удаленные команды) по
    умолчанию отключены, даже если не указаны запрещающие ключи;
-   Начиная с Zabbix 5.0.2 параметр агента EnableRemoteCommands:

```{=html}
<!-- -->
```
       * признан устаревшим для Zabbix агента
       * не поддерживается для Zabbix агента 2

Поэтому, чтобы разрешить все удаленные команды, укажите параметр
AllowKey=system.run\[\*\]. (До Zabbix 5.0.2, в конфигурации агента также
требуется указать EnableRemoteCommands=1.)

Чтобы разрешить только некоторые удаленные команды, создайте белый
список более конкретных параметров AllowKey\[\]. Чтобы запретить
определенные удаленные команды, добавьте параметры DenyKey перед
параметром AllowKey=system.run\[\*\].

[comment]: # ({/new-9de656b2})

[comment]: # ({new-3145c750})
#### Важные правила

-   Белый список без правила запрещения ключей разрешен только для
    элементов данных system.run \[\*\]. Для всех остальных элементов
    параметры AllowKey не допускаются без параметра DenyKey; в этом
    случае Zabbix агент **не запустится** только с параметрами AllowKey.
-   Очередность имеет значение. Указанные параметры проверяются по
    очереди в соответствии с порядком их появления в файле конфигурации:
    -   Как только ключ элемента данных соответствует
        разрешающему/запрещающему правилу, элемент разрешается или
        запрещается; и проверка правил прекращается. Таким образом, если
        элемент соответствует и разрешающему правилу, и запрещающему
        правилу, результат будет зависеть от того, какое правило будет
        первым.

```{=html}
<!-- -->
```
      *Порядок влияет также на параметр EnableRemoteCommands (если используется).
    * Поддерживается неограниченное количество параметров AllowKey/DenyKey.
    * Правила AllowKey, DenyKey не влияют на параметры конфигурации HostnameItem, HostMetadataItem, HostInterfaceItem.
    * Шаблон ключа - это выражение с подстановочным знаком, в котором подстановочный знак (*) соответствует любому количеству любых символов в определенной позиции. Его можно использовать как в имени ключа, так и в параметрах.
    * Если конкретный ключ элемента запрещен в конфигурации агента, элемент будет отмечен как неподдерживаемый (без указания причины);
    * Агент Zabbix с параметром командной строки --print (-p) не будет показывать ключи, которые не разрешены конфигурацией;
    * Агент Zabbix с параметром командной строки --test (-t) вернет статус %%"%% Unsupported item key. %%"%% (Неподдерживаемый ключ элемента данных) для ключей, которые не разрешены конфигурацией;
    * Отклоненные удаленные команды не будут записаны в журнал агента (если LogRemoteCommands = 1).

[comment]: # ({/new-3145c750})

[comment]: # ({new-253ad144})
#### Примеры использования

[comment]: # ({/new-253ad144})

[comment]: # ({new-866af1fd})
##### Запретить конкретную проверку

-   Добавить в черный список конкретную проверку с параметром DenyKey.
    Соответствующие ключи будут запрещены. Разрешены все несовпадающие
    ключи, кроме элементов system.run\[\].

Например:

    # Deny secure data access
    DenyKey=vfs.file.contents[/etc/passwd,*]

::: noteimportant
Черный список может быть не лучшим выбором, потому
что новая версия Zabbix может иметь новые ключи, которые не полностью
ограничиваются существующей конфигурацией. Это может вызвать брешь в
безопасности.
:::

[comment]: # ({/new-866af1fd})

[comment]: # ({new-10d0a182})
##### Запретить конкретную команду, разрешить другие

-   Добавить в черный список конкретную команду с параметром DenyKey.
    Добавьте в белый список все остальные команды с помощью параметра
    AllowKey.

```{=html}
<!-- -->
```
    # Disallow specific command
    DenyKey=system.run[ls -l /]
     
    # Allow other scripts
    AllowKey=system.run[*]

[comment]: # ({/new-10d0a182})

[comment]: # ({new-83edb1ef})
##### Разрешить конкретную проверку, запретить другие

-   Внесите определенные проверки в белый список с параметрами AllowKey,
    запретите другие с помощью `DenyKey = *`

Например:

    # Allow reading logs:
    AllowKey=vfs.file.*[/var/log/*]

    # Allow localtime checks
    AllowKey=system.localtime[*]

    # Deny all other keys
    DenyKey=*

[comment]: # ({/new-83edb1ef})

[comment]: # ({new-16bccaff})
#### Примеры шаблонов

|Шаблон|Описание|Соответствует|Не соответствует|
|------------|----------------|--------------------------|-------------------------------|
|*\**|Соответствует всем возможным ключам с параметрами или без них.|Любой|Нет|
|*vfs.file.contents*|Соответствует `vfs.file.contents` без параметров.|vfs.file.contents|vfs.file.contents\[/etc/passwd\]|
|*vfs.file.contents\[\]*|Соответствует `vfs.file.contents` с пустыми параметрами.|vfs.file.contents\[\]|vfs.file.contents|
|*vfs.file.contents\[\*\]*|Соответствует `vfs.file.contents` с любыми параметрами; не будет соответствовать `vfs.file.contents` без квадратных скобок.|vfs.file.contents\[\]<br>vfs.file.contents\[/path/to/file\]|vfs.file.contents|
|*vfs.file.contents\[/etc/passwd,\*\]*|Соответствует `vfs.file.contents` с первыми параметрами, соответствующими \*/etc/passwd\*, и всеми другими параметрами, имеющими любое значение (в том числе пустыми).|vfs.file.contents\[/etc/passwd,\]<br>vfs.file.contents\[/etc/passwd,utf8\]|vfs.file.contents\[/etc/passwd\]<br>vfs.file.contents\[/var/log/zabbix\_server.log\]<br>vfs.file.contents\[\]|
|*vfs.file.contents\[\*passwd\*\]*|Соответствует `vfs.file.contents` с первым параметром, совпадающим с \*passwd\*, и без других параметров.|vfs.file.contents\[/etc/passwd\]|vfs.file.contents\[/etc/passwd,\]<br>vfs.file.contents\[/etc/passwd, utf8\]|
|*vfs.file.contents\[\*passwd\*,\*\]*|Соответствует `vfs.file.contents` только с первым параметром, совпадающим с \*passwd\*, и всеми последующими параметрами, имеющими любое значение (также пустое).|vfs.file.contents\[/etc/passwd,\]<br>vfs.file.contents\[/etc/passwd, utf8\]|vfs.file.contents\[/etc/passwd\]<br>vfs.file.contents\[/tmp/test\]|
|*vfs.file.contents\[/var/log/zabbix\_server.log,\*,abc\]*|Соответствует `vfs.file.contents` с первым параметром, совпадающим с /var/log/zabbix\_server.log, третьим параметром, совпадающим с 'abc' и любым (втом числе пустым) вторым параметром.|vfs.file.contents\[/var/log/zabbix\_server.log,,abc\]<br>vfs.file.contents\[/var/log/zabbix\_server.log,utf8,abc\]|vfs.file.contents\[/var/log/zabbix\_server.log,,abc,def\]|
|*vfs.file.contents\[/etc/passwd,utf8\]*|Соответствует `vfs.file.contents` с первым параметром, соответствующим /etc/passwd, вторым параметром, совпадающим с 'utf8', и без других аргументов.|vfs.file.contents\[/etc/passwd,utf8\]|vfs.file.contents\[/etc/passwd,\]<br>vfs.file.contents\[/etc/passwd,utf16\]|
|*vfs.file.\**|Соответствует любым ключам, начинающимся с `vfs.file.` без каких-либо параметров.|vfs.file.contents<br>vfs.file.size|vfs.file.contents\[\]<br>vfs.file.size\[/var/log/zabbix\_server.log\]|
|*vfs.file.\*\[\*\]*|Соответствует любым ключам, начинающимся с `vfs.file.`, с любыми параметрами.|vfs.file.size.bytes\[\]<br>vfs.file.size\[/var/log/zabbix\_server.log, utf8\]|vfs.file.size.bytes|
|*vfs.\*.contents*|Соответствует любому ключу, начинающемуся с `vfs.` и заканчивающемуся `.contents` без каких-либо параметров.|vfs.mount.point.file.contents<br>vfs..contents|vfs.contents|

[comment]: # ({/new-16bccaff})


[comment]: # ({new-9df9c213})
#### system.run and AllowKey

A hypothetical script like 'myscript.sh' may be executed on a host via
Zabbix agent in several ways:

1\. As an item key in a passive or active check, for example:

-   system.run\[myscript.sh\]
-   system.run\[myscript.sh,wait\]
-   system.run\[myscript.sh.nowait\]

Here the user may add "wait", "nowait" or omit the 2nd argument to use
its default value in system.run\[\].

2\. As a global script (initiated by user in frontend or API).

A user configures this script in *Administration* → *Scripts*, sets
"Execute on: Zabbix agent" and puts "myscript.sh" into the script's
"Commands" input field. When invoked from frontend or API the Zabbix
server sends to agent:

-   system.run\[myscript.sh,wait\] - up to Zabbix 5.0.4
-   system.run\[myscript.sh\] - since 5.0.5

Here the user does not control the "wait"/"nowait" parameters.

3\. As a remote command from an action. The Zabbix server sends to
agent:

-   system.run\[myscript.sh,nowait\]

Here again the user does not control the "wait"/"nowait" parameters.

What that means is if we set AllowKey like:

    AllowKey=system.run[myscript.sh]

then

-   system.run\[myscript.sh\] - will be allowed
-   system.run\[myscript.sh,wait\], system.run\[myscript.sh,nowait\]
    will not be allowed - the script will not be run if invoked as a
    step of action

To allow all described variants you may add:

    AllowKey=system.run[myscript.sh,*] 
    DenyKey=system.run[*]

to the agent/agent2 parameters.

[comment]: # ({/new-9df9c213})
