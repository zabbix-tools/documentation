[comment]: # translation:outdated

[comment]: # ({65cecc37-09ce3b2e})
# 3 Возможности Zabbix

[comment]: # ({/65cecc37-09ce3b2e})

[comment]: # ({d712dc62-a70701e6})
#### Обзор

Zabbix - высоко интегрированное решение мониторинга сети, которое предлагает множество функций в одном пакете.

**[Сбор данных](/manual/config/items)**

-   проверки доступности и производительности
-   поддержка мониторинга с использованием SNMP (и трапперы, и поллеры), IPMI, JMX, VMware
-   пользовательские проверки
-   сбор желаемых данных с использованием пользовательских интервалов
-   выполняются сервером/прокси и агентами

**[Гибкие определения порогов](/manual/config/triggers)**

-   вы можете задавать очень гибкие пороги проблем, называемые триггерами, ссылаясь на значения из базы данных

**[Множество настроек оповещений](/manual/config/notifications)**

-   отправка оповещений может быть настроена с использованием расписания эскалаций, получателей, типов оповещений
-   оповещения можно сделать информативными и полезными с использованием переменных макросов
-   автоматические действия, включающие в себя удаленные команды

**[Построение графиков в режиме реального времени](/manual/config/visualization/graphs/simple)**

-   при помощи встроенного функционала построения графиков сразу же доступны графики по наблюдаемым элементам данных

**[Возможности веб-мониторинга](/manual/web_monitoring)**

-   Zabbix может имитировать нажатия мышкой на веб-сайте, проверить функционал и время ответа

**[Широкие возможности визуализации](/manual/config/visualization)**

-   возможность создавать пользовательские графики, что позволяет комбинировать множество элементов данных в одном месте
-   карты сети
-   пользовательские комплексные экраны и слайд-шоу наподобие внешнего вида ПАНЕЛИ
-   отчеты
-   высокоуровневое (бизнес) представление наблюдаемых ресурсов

**[Хранение данных истории](/manual/installation/requirements#database_size)**

-   запись данных в базу данных
-   настраиваемая история
-   встроенная процедура очистки истории

**[Простая настройка](/manual/config/hosts)**

-   добавление наблюдаемых устройств в виде узлов сети
-   как только узлы сети появляются в базе данных, они сразу готовы к мониторингу
-   применение шаблонов к наблюдаемым устройствам

**[Использование шаблонов](/manual/config/templates)**

-   группировка проверок в шаблоны
-   шаблоны могут наследоваться от других шаблонов

**[Сетевое обнаружение](/manual/discovery)**

-   автоматическое обнаружение сетевых устройств
-   автоматическая регистрация агентов
-   обнаружение файловых систем, сетевых устройств и SNMP OID'ов

**[Быстрый веб-итерфейс](/manual/web_interface)**

-   веб-интерфейс, основанный на языке PHP
-   доступен из любого места
-   удобная навигация
-   журнал аудита

**[Zabbix API](/manual/api)**

-   Zabbix API предоставляет программируемый интерфейс к Zabbix для массовых манипуляций, интеграции стороннего программного обеспечения и других целей.

**[Система прав доступа](/manual/config/users_and_usergroups)**

-   безопасная аутентификация пользователей
-   возможность ограничения доступа отдельным пользователям к конкретным страницам

**[Полнофункциональный и легко расширяемый агент](/manual/concepts/agent)**

-   разворачивается на наблюдаемых машинах
-   можно развернуть как на Linux, так и на Windows

**[Бинарные демоны](/manual/concepts/server)**

-   написаны на C, имеют высокую производительность и используют небольшой объем памяти
-   легко переносимы

**[Готовность к сложным средам](/manual/distributed_monitoring)**

-   простой удаленный мониторинг с использованием Zabbix прокси

[comment]: # ({/d712dc62-a70701e6})
