[comment]: # translation:outdated

[comment]: # ({2d97db26-761c4a6f})
# 2 Что такое Zabbix

[comment]: # ({/2d97db26-761c4a6f})

[comment]: # ({new-319fca18})
#### Обзор

Zabbix создан Алексеем Владышевым, и в настоящее время активно разрабатывается и поддерживается компанией Zabbix SIA.

Zabbix - это решение распределенного мониторинга корпоративного класса с открытыми исходными кодами.

Zabbix - это программное обеспечение для мониторинга многочисленных параметров сети, жизнеспособности и целостности серверов, виртуальных машин, приложений, сервисов, баз данных, веб-сайтов, облачных сред и многого другого. Zabbix использует гибкий механизм оповещений, что позволяет пользователям настраивать уведомления основанные на e-mail практически на любое событие. Такой подход позволяет быстро реагировать на проблемы с серверами. Zabbix предлагает отличные функции отчетности и визуализации данных основанные на данных истории. Что делает Zabbix идеальным при планировании мощностей.

Zabbix поддерживает как поллеры, так и трапперы. Все отчеты и статистика Zabbix, так же как и параметры настройки, доступны через Веб-интерфейс. Веб-интерфейс обеспечивает доступ к информации о состоянии вашей сети и жизнеспособности ваших серверов из любого места. Корректно настроенный, Zabbix может сыграть важную роль в мониторинге ИТ инфраструктуры. Это верно и для маленьких организаций с несколькими серверами и для больших организаций со множеством серверов.

Zabbix бесплатен. Zabbix написан и распространяется под лицензией GPL General Public License версии 2. Это означает, что его исходный код свободно распространяется и доступен для неограниченного круга лиц.

[Коммерческая поддержка](http://www.zabbix.com/support.php) доступна и осуществляется как самой компанией Zabbix Company, так и партнерами компании по всему миру.

Узнайте больше о [возможностях Zabbix](features).

[comment]: # ({/new-319fca18})

[comment]: # ({77fd96fe-46ee774b})
#### Пользователи Zabbix

Множество организаций разных размеров по всему миру полагаются на Zabbix, как на основную платформу мониторинга.

[comment]: # ({/77fd96fe-46ee774b})
