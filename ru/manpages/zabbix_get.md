[comment]: # translation:outdated

[comment]: # ({new-dabb0bd5})
# zabbix\_get

Раздел: Команды обслуживания (8)\
Обновлено: 2015-08-06\
[Оглавление](#index) [Вернуться к основному
содержимому](/documentation/3.0/ru/manpages)

------------------------------------------------------------------------

[ ]{#lbAB}

[comment]: # ({/new-dabb0bd5})

[comment]: # ({new-0ba1ce53})
## НАИМЕНОВАНИЕ

zabbix\_get - утилита Zabbix get [ ]{#lbAC}

[comment]: # ({/new-0ba1ce53})

[comment]: # ({new-60e7e93b})
## ОБЗОР

**zabbix\_get -s** *имя-хоста-или-IP* \[**-p** *номер-порта*\] \[**-I**
*IP-адрес*\] **-k** *ключ-элемента-данных*\
**zabbix\_get -s** *имя-хоста-или-IP* \[**-p** *номер-порта*\] \[**-I**
*IP-адрес*\] **--tls-connect** **cert** **--tls-ca-file** *CA-файл*
\[**--tls-crl-file** *CRL-файл*\] \[**--tls-agent-cert-issuer**
*эмитент-сертификата*\] \[**--tls-agent-cert-subject**
*тема-сертификата*\] **--tls-cert-file** *файл-сертификата*
**--tls-key-file** *файл-ключа* **-k** *ключ-элемента данных*\
**zabbix\_get -s** *имя-хоста-или-IP* \[**-p** *номер-порта*\] \[**-I**
*IP-адрес*\] **--tls-connect** **psk** **--tls-psk-identity**
*идентификатор-PSK* **--tls-psk-file** *PSK-файл* **-k**
*ключ-элемента-данных*\
**zabbix\_get -h**\
**zabbix\_get -V** [ ]{#lbAD}

[comment]: # ({/new-60e7e93b})

[comment]: # ({a9c20a9a-da08bc1a})
## ОПИСАНИЕ

**zabbix\_get**  - это утилита командной строки, которая используется для получения данных
от агента Zabbix. [  ]{#lbAE}

[comment]: # ({/a9c20a9a-da08bc1a})

[comment]: # ({new-4ab1a759})
### ПАРАМЕТРЫ

**-s**, **--host** *имя-хоста-или-IP*  
Необходимо указать имя хоста или IP адреса хоста.

**-p**, **--port** *номер-порта*  
Необходимо указать номер порта, запущенного агента на хосте. По
умолчанию 10050.

**-I**, **--source-address** *IP-адрес*  
Необходимо указать локальный IP адрес для исходящих подключений.

**-k**, **--key** *ключ-элемента-данных*  
Необходимо указать ключ элемента данных, с которого необходимо получить
значение.

**--tls-connect** *значение*  
Каким образом подключаться к агенту. Значения:

[ ]{#lbAF}

[comment]: # ({/new-4ab1a759})

[comment]: # ({new-465bc4c8})
### 

  
**unencrypted**  
подключение без шифрования

```{=html}
<!-- -->
```
  
**psk**  
подключение с использованием TLS и pre-shared ключа

```{=html}
<!-- -->
```
  
**cert**  
подключение с использованием TLS и сертификата

```{=html}
<!-- -->
```
**--tls-ca-file** *CA-файл*  
Абсолютный путь к файлу содержащему CA сертификаты верхнего уровня для
проверки сертификата узла.

**--tls-crl-file** *CRL-файл*  
Абсолютный путь к файлу содержащему отозванные сертификаты.

**--tls-agent-cert-issuer** *эмитент-сертификата*  
Разрешенный эмитент сертификата агента.

**--tls-agent-cert-subject** *тема-сертификата*  
Разрешенная тема сертификата агента.

**--tls-cert-file** *файл-сертификата*  
Абсолютный путь к файлу содержащему сертификат или цепочку сертификатов.

**--tls-key-file** *файл-ключа*  
Абсолютный путь к файлу содержащему приватный ключ.

**--tls-psk-identity** *идентификатор-PSK*  
Строка идентификатор PSK.

**--tls-psk-file** *PSK-файл*  
Абсолютный путь к файлу содержащему pre-shared ключ.

**-h**, **--help**  
Вывод этой справки и выход.

**-V**, **--version**  
Вывод информации о версии и выход.

[ ]{#lbAF}

[comment]: # ({/new-465bc4c8})

[comment]: # ({new-c5e4d00b})
## ПРИМЕРЫ

**zabbix\_get -s 127.0.0.1 -p 10050 -k "system.cpu.load\[all,avg1\]"**\
**zabbix\_get -s 127.0.0.1 -p 10050 -k "system.cpu.load\[all,avg1\]"
--tls-connect cert --tls-ca-file /home/zabbix/zabbix\_ca\_file
--tls-agent-cert-issuer "CN=Signing CA,OU=IT operations,O=Example
Corp,DC=example,DC=com" --tls-agent-cert-subject "CN=server1,OU=IT
operations,O=Example Corp,DC=example,DC=com" --tls-cert-file
/home/zabbix/zabbix\_get.crt --tls-key-file
/home/zabbix/zabbix\_get.key\
zabbix\_get -s 127.0.0.1 -p 10050 -k "system.cpu.load\[all,avg1\]"
--tls-connect psk --tls-psk-identity "PSK ID Zabbix agentd"
--tls-psk-file /home/zabbix/zabbix\_agentd.psk** [ ]{#lbAH}

[comment]: # ({/new-c5e4d00b})

[comment]: # ({new-0ed7cca7})
## СМОТРИ ТАКЖЕ

**[zabbix\_agentd](zabbix_agentd)**(8),
**[zabbix\_proxy](zabbix_proxy)**(8),
**[zabbix\_sender](zabbix_sender)**(8),
**[zabbix\_server](zabbix_server)**(8) [ ]{#lbAH}

[comment]: # ({/new-0ed7cca7})

[comment]: # ({new-aaed5529})
## АВТОР

Алексей Владышев <<alex@zabbix.com>>

------------------------------------------------------------------------

[ ]{#index}

[comment]: # ({/new-aaed5529})

[comment]: # ({new-bdb17437})
## Оглавление

[НАИМЕНОВАНИЕ](#lbAB)  

[ОБЗОР](#lbAC)  

[ОПИСАНИЕ](#lbAD)  
[Параметры](#lbAE)  

[ПРИМЕРЫ](#lbAF)  

[СМОТРИ ТАКЖЕ](#lbAG)  

[АВТОР](#lbAH)  

------------------------------------------------------------------------

This document was created by man2html, using the manual pages.\
Time: 09:21:04 GMT, January 08, 2016

[comment]: # ({/new-bdb17437})
