[comment]: # translation:outdated

[comment]: # ({new-fdbd84bb})
# zabbix\_server

Раздел: Команды обслуживания (8)\
Обновлено: 2016-01-13\
[Оглавление](#index) [Вернуться к основному
содержимому](/documentation/3.0/ru/manpages)

------------------------------------------------------------------------

[ ]{#lbAB}

[comment]: # ({/new-fdbd84bb})

[comment]: # ({new-1090a00e})
## НАИМЕНОВАНИЕ

zabbix\_server - демон Zabbix сервера [ ]{#lbAC}

[comment]: # ({/new-1090a00e})

[comment]: # ({new-2a5507db})
## ОБЗОР

**zabbix\_server** \[**-c** *файл-конфигурации*\]\
**zabbix\_server** \[**-c** *файл-конфигурации*\] **-R**
*опция-контроля*\
**zabbix\_server -h**\
**zabbix\_server -V** [ ]{#lbAD}

[comment]: # ({/new-2a5507db})

[comment]: # ({new-f39aa640})
## ОПИСАНИЕ

**zabbix\_server** это основной демон программного обеспечения Zabbix.
[ ]{#lbAE}

[comment]: # ({/new-f39aa640})

[comment]: # ({new-455125db})
### Параметры

-c, --config *<файл-конфигурации>*  
Использование альтернативного *файла-конфигурации* вместо файла по
умолчанию. Необходимо указать путь к файлу конфигурации.

**-f**, **--foreground**  
Запуск Zabbix сервера в приоритетном режиме.

-R, --runtime-control *<опция>*  
Выполнение административных функций в соотвествии с *опцией*.

[ ]{#lbAF}

[comment]: # ({/new-455125db})

[comment]: # ({new-d7d7f728})
### 

  
Опции выполнения административных функций

  
config\_cache\_reload  
Обновление кэша конфигурации. Игнорируется, если кэш уже в процессе
загрузки. Для поиска PID файла используется файл конфигурации по
умолчанию (если **-c** опция не была указана), сигнал будет отправлен
тому процессу, который указан в PID файле.

  
**housekeeper\_execute**  
Выполнение процесса очистки истории. Игнорируется, если процесс очистки
истории уже выполняется.

  
log\_level\_increase\[=<цель>\]  
Увеличение уровня журналирования, действует на все процессы, если цель
не указана

  
log\_level\_decrease\[=<цель>\]  
Уменьшение уровня журналирования, действует на все процессы, если цель
не указана.

[ ]{#lbAG}

[comment]: # ({/new-d7d7f728})

[comment]: # ({new-800a1525})

**ha\_remove\_node**\[=*target*\]  
Remove the high availability (HA) node specified by its name or ID.
Note that active/standby nodes cannot be removed.

**ha\_set\_failover\_delay**\[=*delay*\]  
Set high availability (HA) failover delay.
Time suffixes are supported, e.g. 10s, 1m.

**secrets\_reload**  
Reload secrets from Vault.

**service\_cache\_reload**  
Reload the service manager cache.

**snmp\_cache\_reload**  
Reload SNMP cache, clear the SNMP properties (engine time, engine boots, engine id, credentials) for all hosts.

**prof\_enable**\[=*target*\]  
Enable profiling.
Affects all processes if target is not specified.
Enabled profiling provides details of all rwlocks/mutexes by function name.
Supported since Zabbix 6.0.13.

**prof\_disable**\[=*target*\]  
Disable profiling.
Affects all processes if target is not specified.
Supported since Zabbix 6.0.13.
  
**log\_level\_increase**\[=*target*\]  
Increase log level, affects all processes if target is not specified

  
**log\_level\_decrease**\[=*target*\]  
Decrease log level, affects all processes if target is not specified

[ ]{#lbAG}

[comment]: # ({/new-800a1525})

[comment]: # ({new-858c64d1})
### 

  
Цели контроля уровня журналирования

  
<pid>  
Идентификатор процесса

  
<тип процесса>  
Все процессы указанного типа (например, poller)

  
<тип процесса>,N  
Тип процесса и его номер (например, poller,3)

```{=html}
<!-- -->
```
-h, --help  
Вывод этой справки и выход.

-V, --version  
Вывод информации о версии и выход.

[ ]{#lbAG}

[comment]: # ({/new-858c64d1})

[comment]: # ({54933121-00ee0d1e})
## ФАЙЛЫ

*/usr/local/etc/zabbix\_server.conf*  
Расположение файла конфигурации Zabbix сервера по умолчанию (если не изменено
во время компиляции).

[ ]{#lbAI}

[comment]: # ({/54933121-00ee0d1e})

[comment]: # ({new-d095490c})
## СМОТРИ ТАКЖЕ

**[zabbix\_agentd](zabbix_agentd)**(8),
**[zabbix\_get](zabbix_get)**(8), **[zabbix\_proxy](zabbix_proxy)**(8),
**[zabbix\_sender](zabbix_sender)**(8) [ ]{#lbAI}

[comment]: # ({/new-d095490c})

[comment]: # ({new-e55ed07e})
## АВТОР

Алексей Владышев <<alex@zabbix.com>>

------------------------------------------------------------------------

[ ]{#index}

[comment]: # ({/new-e55ed07e})

[comment]: # ({new-b105a0f1})
## Оглавление

[НАИМЕНОВАНИЕ](#lbAB)

[ОБЗОР](#lbAC)

[ОПИСАНИЕ](#lbAD)

[ОПЦИИ](#lbAE)

[](#lbAF)

[](#lbAG)

  

[ФАЙЛЫ](#lbAG)

[СМОТРИ ТАКЖЕ](#lbAH)

[АВТОР](#lbAI)

------------------------------------------------------------------------

This document was created by man2html, using the manual pages.\
Time: 09:11:11 GMT, January 19, 2016

[comment]: # ({/new-b105a0f1})
