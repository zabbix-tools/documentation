<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="ua" datatype="plaintext" original="manual/appliance.md">
    <body>
      <trans-unit id="8c911baa" xml:space="preserve">
        <source># 6. Zabbix appliance</source>
      </trans-unit>
      <trans-unit id="cfa1a361" xml:space="preserve">
        <source>#### Overview

As an alternative to setting up manually or reusing an existing server
for Zabbix, users may
[download](http://www.zabbix.com/download_appliance) a Zabbix appliance
or a Zabbix appliance installation CD image.

Zabbix appliance and installation CD versions are based on AlmaLinux 8
(x86\_64).

Zabbix appliance installation CD can be used for instant deployment of
Zabbix server (MySQL).

::: noteimportant
 You can use this Appliance to evaluate Zabbix.
The Appliance is not intended for serious production use. 
:::</source>
      </trans-unit>
      <trans-unit id="cffa82a3" xml:space="preserve">
        <source>##### System requirements:

-   *RAM*: 1.5 GB
-   *Disk space*: at least 8 GB should be allocated for the virtual
    machine
-   *CPU*: 2 cores minimum 

Zabbix installation CD/DVD boot menu:

![](../../assets/en/manual/installation_cd_boot_menu1.png){width="600"}

Zabbix appliance contains a Zabbix server (configured and running on
MySQL) and a frontend.

Zabbix virtual appliance is available in the following formats:

-   VMware (.vmx)
-   Open virtualization format (.ovf)
-   Microsoft Hyper-V 2012 (.vhdx)
-   Microsoft Hyper-V 2008 (.vhd)
-   KVM, Parallels, QEMU, USB stick, VirtualBox, Xen (.raw)
-   KVM, QEMU (.qcow2)

To get started, boot the appliance and point a browser at the IP the
appliance has received over DHCP.

::: noteimportant
 DHCP must be enabled on the host. 
:::

To get the IP address from inside the virtual machine run:

    ip addr show

To access Zabbix frontend, go to **http://&lt;host\_ip&gt;** (for access
from the host's browser bridged mode should be enabled in the VM network
settings).

::: notetip
If the appliance fails to start up in Hyper-V, you may
want to press `Ctrl+Alt+F2` to switch tty sessions.
:::</source>
      </trans-unit>
      <trans-unit id="589fd5e2" xml:space="preserve">
        <source>#### - Changes to AlmaLinux 8 configuration

The appliance is based on AlmaLinux 8. There are some changes applied to
the base AlmaLinux configuration.</source>
      </trans-unit>
      <trans-unit id="193d3b23" xml:space="preserve">
        <source>##### - Repositories

Official Zabbix
[repository](/manual/installation/install_from_packages/rhel) has
been added to */etc/yum.repos.d*:

    [zabbix]
    name=Zabbix Official Repository - $basearch
    baseurl=http://repo.zabbix.com/zabbix/5.2/rhel/8/$basearch/
    enabled=1
    gpgcheck=1
    gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-ZABBIX-A14FE591</source>
      </trans-unit>
      <trans-unit id="589dc798" xml:space="preserve">
        <source>##### - Firewall configuration

The appliance uses iptables firewall with predefined rules:

-   Opened SSH port (22 TCP);
-   Opened Zabbix agent (10050 TCP) and Zabbix trapper (10051 TCP)
    ports;
-   Opened HTTP (80 TCP) and HTTPS (443 TCP) ports;
-   Opened SNMP trap port (162 UDP);
-   Opened outgoing connections to NTP port (53 UDP);
-   ICMP packets limited to 5 packets per second;
-   All other incoming connections are dropped.</source>
      </trans-unit>
      <trans-unit id="b2283a9a" xml:space="preserve">
        <source>##### - Using a static IP address

By default the appliance uses DHCP to obtain the IP address. To specify
a static IP address:

-   Log in as root user;
-   Open */etc/sysconfig/network-scripts/ifcfg-eth0* file;
-   Replace *BOOTPROTO=dhcp* with *BOOTPROTO=none*
-   Add the following lines:
    -   *IPADDR=&lt;IP address of the appliance&gt;*
    -   *PREFIX=&lt;CIDR prefix&gt;*
    -   *GATEWAY=&lt;gateway IP address&gt;*
    -   *DNS1=&lt;DNS server IP address&gt;*
-   Run **systemctl restart network** command.

Consult the official Red Hat
[documentation](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/deployment_guide/s1-networkscripts-interfaces)
if needed.</source>
      </trans-unit>
      <trans-unit id="acf06935" xml:space="preserve">
        <source>##### - Changing time zone

By default the appliance uses UTC for the system clock. To change the
time zone, copy the appropriate file from */usr/share/zoneinfo* to
*/etc/localtime*, for example:

    cp /usr/share/zoneinfo/Europe/Riga /etc/localtime</source>
      </trans-unit>
      <trans-unit id="2c378c8d" xml:space="preserve">
        <source>#### - Zabbix configuration

Zabbix appliance setup has the following passwords and configuration
changes:</source>
      </trans-unit>
      <trans-unit id="d39b5151" xml:space="preserve">
        <source>##### - Credentials (login:password)

System:

-   root:zabbix

Zabbix frontend:

-   Admin:zabbix

Database:

-   root:&lt;random&gt;
-   zabbix:&lt;random&gt;

::: noteclassic
Database passwords are randomly generated during the
installation process.\
Root password is stored inside the /root/.my.cnf file. It is not
required to input a password under the "root" account.
:::

To change the database user password, changes have to be made in the
following locations:

-   MySQL;
-   /etc/zabbix/zabbix\_server.conf;
-   /etc/zabbix/web/zabbix.conf.php.

::: noteclassic
 Separate users `zabbix_srv` and `zabbix_web` are defined
for the server and the frontend respectively. 
:::</source>
      </trans-unit>
      <trans-unit id="f11ee379" xml:space="preserve">
        <source>##### - File locations

-   Configuration files are located in **/etc/zabbix**.
-   Zabbix server, proxy and agent logfiles are located in
    **/var/log/zabbix**.
-   Zabbix frontend is located in **/usr/share/zabbix**.
-   Home directory for the user **zabbix** is **/var/lib/zabbix**.</source>
      </trans-unit>
      <trans-unit id="e00773c5" xml:space="preserve">
        <source>##### - Changes to Zabbix configuration

-   Frontend timezone is set to Europe/Riga (this can be modified in
    **/etc/php-fpm.d/zabbix.conf**);</source>
      </trans-unit>
      <trans-unit id="2c4e4239" xml:space="preserve">
        <source>#### - Frontend access

By default, access to the frontend is allowed from anywhere.

The frontend can be accessed at *http://&lt;host&gt;*.

This can be customized in **/etc/nginx/conf.d/zabbix.conf**. Nginx has
to be restarted after modifying this file. To do so, log in using SSH as
**root** user and execute:

    systemctl restart nginx</source>
      </trans-unit>
      <trans-unit id="4fbc391b" xml:space="preserve">
        <source>#### - Firewall

By default, only the ports listed in the [configuration
changes](#firewall_configuration) above are open. To open additional
ports, modify "*/etc/sysconfig/iptables*" file and reload firewall
rules:

    systemctl reload iptables</source>
      </trans-unit>
      <trans-unit id="bf60554b" xml:space="preserve">
        <source>#### - Upgrading

The Zabbix appliance packages may be upgraded. To do so, run:

    dnf update zabbix*</source>
      </trans-unit>
      <trans-unit id="d167c766" xml:space="preserve">
        <source>#### - System Services

Systemd services are available:

    systemctl list-units zabbix*</source>
      </trans-unit>
      <trans-unit id="a582d1bf" xml:space="preserve">
        <source>#### - Format-specific notes</source>
      </trans-unit>
      <trans-unit id="a45df55b" xml:space="preserve">
        <source>##### - VMware

The images in *vmdk* format are usable directly in VMware Player, Server
and Workstation products. For use in ESX, ESXi and vSphere they must be
converted using [VMware
converter](http://www.vmware.com/products/converter/).
If you use VMWare Converter, you may encounter issues with the hybrid network adapter. In that case, you can try
specifying the E1000 adapter during the conversion process. Alternatively, after the conversion is complete,
you can delete the existing adapter and add an E1000 adapter.</source>
      </trans-unit>
      <trans-unit id="7d1c1440" xml:space="preserve">
        <source>##### - HDD/flash image (raw)

    dd if=./zabbix_appliance_5.2.0.raw of=/dev/sdc bs=4k conv=fdatasync

Replace */dev/sdc* with your Flash/HDD disk device.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
