[comment]: # translation:outdated

[comment]: # ({new-ea7b20e9})
# 2 Plugins

[comment]: # ({/new-ea7b20e9})

[comment]: # ({new-84502826})
#### Overview

Plugins provide an option to extend the monitoring capabilities of Zabbix. Plugins are written in Go programming
language and are supported by Zabbix agent 2 only.
Plugins provide an alternative to [loadable modules](/manual/extensions/loadablemodules) (written in C),
and [other methods](/manual/extensions) for extending Zabbix functionality.

The following features are specific to agent 2 and its plugins:

-   support of scheduled and flexible intervals for both passive and
    active checks;
-   task queue management with respect to schedule and task concurrency;
-   plugin-level timeouts;
-   compatibility check of Zabbix agent 2 and its plugins on start up.

Since Zabbix 6.0.0, plugins don't have to be integrated into the agent 2 directly and can be added as loadable plugins,
thus making the creation process of additional plugins for gathering new monitoring metrics easier.

This page lists Zabbix native and loadable plugins, and describes plugin configuration principles from the user
perspective. For instructions about writing your own plugins, please see
[Plugin development guidelines](https://www.zabbix.com/documentation/guidelines/en/plugins).
[Metrics collection process](#metrics-collection-process) provides a detailed description of the entire metrics
collection process including the methods applied and the connection diagram.

[comment]: # ({/new-84502826})

[comment]: # ({new-ff9223df})
#### Configuring plugins

This section provides common plugin configuration principles and best practices.

All plugins are configured using *Plugins.\** parameter, which can either be part of the Zabbix agent 2 [configuration
file](/manual/appendix/config/zabbix_agent2) or a plugin's own
[configuration file](/manual/appendix/config/zabbix_agent2_plugins). If a plugin uses a separate configuration file,
path to this file should be specified in the Include parameter of Zabbix agent 2 configuration file.

Each plugin parameter should have the following structure:

*Plugins.<PluginName>.<Parameter>=<Value>*

Parameter names should adhere to the following requirements:

-   it is recommended to capitalize the names of your plugins;
-   the parameter should be capitalized;
-   special characters are not allowed;
-   nesting isn’t limited by a maximum level;
-   the number of parameters is not limited.

[comment]: # ({/new-ff9223df})

[comment]: # ({new-36aaae86})
##### Named sessions

Named sessions represent an additional level of plugin parameters and can be used to define separate sets of
authentication parameters for each of the instances being monitored. Each named session parameter should have the
following structure:

Plugins.<PluginName>.<SessionName>.<Parameter>=<Value>

A session name can be used as a connString item key parameter instead of specifying a URI, username, and password
separately. In item keys, the first parameter can be either a connString or a Uri. If the first key parameter matches a
session name specified in the configuration file, the check will be executed using named session parameters. If the first
key parameter doesn't match any session name, it will be treated as a Uri.

Note, that:

-   when providing a connString (session name) in key parameters, key
    parameters for username and password must be empty;
-   passing embedded URI credentials is not supported, consider using
    named sessions instead;
-   in case an authentication parameter is not specified for the named
    session, a hardcoded default value will be used.

The list of available named session parameters depends on the plugin, see individual plugin
[configuration files](/manual/appendix/config/zabbix_agent2_plugins) for details.

**Example:** Monitoring of two instances “MySQL1” and “MySQL2” can be configured in the following way:

    Plugins.Mysql.Sessions.MySQL1.Uri=tcp://127.0.0.1:3306
    Plugins.Mysql.Sessions.MySQL1.User=<UsernameForMySQL1>
    Plugins.Mysql.Sessions.MySQL1.Password=<PasswordForMySQL1>    
    Plugins.Mysql.Sessions.MySQL2.Uri=tcp://127.0.0.1:3307   
    Plugins.Mysql.Sessions.MySQL2.User=<UsernameForMySQL2>
    Plugins.Mysql.Sessions.MySQL2.Password=<PasswordForMySQL2>

Now, these names may be used as connStrings in keys instead of URIs:

    mysql.ping[MySQL1]
    mysql.ping[MySQL2]

[comment]: # ({/new-36aaae86})

[comment]: # ({new-62cf279d})

##### Parameter priority

*Since version 6.4.3*, Zabbix agent 2 plugins search for connection-related parameter values in the following order: 

![](../../../assets/en/diagrams/agent2_parameters.png)

1. The first item key parameter is compared to session names. If no match is found it is treated as an actual value; in this case, step 3 will be skipped. If a match is found, the parameter value (usually, a URI) must be defined in the named session.
2. Other parameters will be taken from the item key if defined.
3. If an item key parameter (for example, password) is empty, plugin will look for the corresponding named session parameter.
4. If the session parameter is also not specified, the value defined in the corresponding [default parameter](#default-values) will be used.
5. If all else fails, the plugin will use the hardcoded default value.

[comment]: # ({/new-62cf279d})

[comment]: # ({new-038eec44})

##### Example 1

Monitoring of two instances “MySQL1” and “MySQL2”.

Configuration parameters:

```bash
Plugins.Mysql.Sessions.MySQL1.Uri=tcp://127.0.0.1:3306
Plugins.Mysql.Sessions.MySQL1.User=mysql1_user
Plugins.Mysql.Sessions.MySQL1.Password=unique_password
Plugins.Mysql.Sessions.MySQL2.Uri=tcp://192.0.2.0:3306
Plugins.Mysql.Sessions.MySQL2.User=mysql2_user
Plugins.Mysql.Sessions.MySQL2.Password=different_password
```

[Item keys](/manual/config/items/itemtypes/zabbix_agent/zabbix_agent2): `mysql.ping[MySQL1]`, `mysql.ping[MySQL2]`

[comment]: # ({/new-038eec44})

[comment]: # ({new-c0e9a7f9})

##### Example 2

Providing some of the parameters in the item key (supported since Zabbix 6.4.2).

Configuration parameters:

```bash
Plugins.Postgres.Sessions.Session1.Uri=tcp://192.0.2.234:5432
Plugins.Postgres.Sessions.Session1.User=old_username
Plugins.Postgres.Sessions.Session1.Password=session_password
```

[Item key](/manual/config/items/itemtypes/zabbix_agent/zabbix_agent2): `pgsql.ping[session1,new_username,,postgres]`

As a result of this configuration, the agent will connect to PostgreSQL using the following parameters:

- URI from session parameter: *192.0.2.234:5432*
- Username from the item key: *new_username*
- Password from session parameter (since it is omitted in the item key): *session_password*
- Database name from the item key: *postgres*

[comment]: # ({/new-c0e9a7f9})

[comment]: # ({new-414321c1})

##### Example 3

Collecting a metric using default configuration parameters.

Configuration parameters:

```bash
Plugins.Postgres.Default.Uri=tcp://192.0.2.234:5432
Plugins.Postgres.Default.User=zabbix
Plugins.Postgres.Default.Password=password
```

[Item key](/manual/config/items/itemtypes/zabbix_agent/zabbix_agent2): `pgsql.ping[,,,postgres]`

As a result of this configuration, the agent will connect to PostgreSQL using the parameters:

- Default URI: *192.0.2.234:5432*
- Default username: *zabbix*
- Default password: *password*
- Database name from the item key: *postgres*

[comment]: # ({/new-414321c1})



[comment]: # ({new-bebf9145})
##### Hardcoded defaults

If a parameter required for authentication is not provided in an item key or in the named session parameters, the plugin
will use a hardcoded default value.

[comment]: # ({/new-bebf9145})

[comment]: # ({new-8385f300})
##### Connections

Some plugins support gathering metrics from multiple instances simultaneously. Both local and remote instances can be
monitored. TCP and Unix-socket connections are supported.

It is recommended to configure plugins to keep connections to instances in an open state. The benefits are reduced
network congestion, latency, and CPU and memory usage due to the lower number of connections. The client library takes
care of this.

::: notetip
Time period for which unused connections should remain open can be determined by *Plugins.<PluginName>.KeepAlive*
parameter. Example: *Plugins.Memcached.KeepAlive*
:::

[comment]: # ({/new-8385f300})

[comment]: # ({new-4c4f3ca2})
#### Plugins supplied out-of-the-box

All metrics supported by Zabbix agent 2 are collected by plugins.

::: notetip
All the loadable plugins, for instance MongoDB, when launched with:
-  *-V version* - print plugin version and license information;
-  *-h help* - print help information.
:::

The following plugins for Zabbix agent 2 are available out-of-the-box:

|Plugin name|Description|Supported item keys|Comments|
|--|--|------|------|
|Agent|Metrics of the Zabbix agent being used.|agent.hostname, agent.ping, agent.version|Supported keys have the same parameters as Zabbix agent [keys](/manual/config/items/itemtypes/zabbix_agent).<br><br> See the [Agent plugin source code](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/internal/agent/plugin_agent.go) here.|
|Ceph|Ceph monitoring.|ceph.df.details, ceph.osd.stats, ceph.osd.discovery, ceph.osd.dump,<br>ceph.ping, ceph.pool.discovery, ceph.status|Supported [keys](/manual/config/items/itemtypes/zabbix_agent/zabbix_agent2) can be used with Zabbix agent 2 only.<br><br>See also:<br>- [Plugin documentation](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/ceph/README.md)<br>- [Configuration parameters](/manual/appendix/config/zabbix_agent2_plugins/ceph_plugin).|
|CPU|System CPU monitoring (number of CPUs/CPU cores, discovered CPUs, utilization percentage).|system.cpu.discovery, system.cpu.num, system.cpu.util|Supported keys have the same parameters as Zabbix agent [keys](/manual/config/items/itemtypes/zabbix_agent).<br><br> See the [CPU plugin source code](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/system/cpu) here.|
|Docker|Monitoring of Docker containers.|docker.container\_info, docker.container\_stats, docker.containers, docker.containers.discovery,<br>docker.data\_usage, docker.images, docker.images.discovery, docker.info, docker.ping|Supported [keys](/manual/config/items/itemtypes/zabbix_agent/zabbix_agent2) can be used with Zabbix agent 2 only.<br><br>See also:<br>[Configuration parameters](/manual/appendix/config/zabbix_agent2_plugins/d_plugin).<br><br> See the [Docker plugin source code](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/docker) here.|
|File|File metrics collection.|vfs.file.cksum, vfs.file.contents, vfs.file.exists, vfs.file.md5sum,<br>vfs.file.regexp, vfs.file.regmatch, vfs.file.size, vfs.file.time|Supported keys have the same parameters as Zabbix agent [keys](/manual/config/items/itemtypes/zabbix_agent).<br><br> See the [File plugin source code](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/vfs/file) here.|
|Kernel|Kernel monitoring.|kernel.maxfiles, kernel.maxproc|Supported keys have the same parameters as Zabbix agent [keys](/manual/config/items/itemtypes/zabbix_agent).<br><br> See the [Kernel plugin source code](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/kernel) here.|
|Log|Log file monitoring.|log, log.count, logrt, logrt.count|Supported keys have the same parameters as Zabbix agent [keys](/manual/config/items/itemtypes/zabbix_agent).<br><br>See also:<br>Plugin configuration parameters ([Unix](/manual/appendix/config/zabbix_agent2)/[Windows](/manual/appendix/config/zabbix_agent2_win))<br><br> See the [Log plugin source code](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/log) here.|
|Memcached|Memcached server monitoring.|memcached.ping, memchached.stats|Supported [keys](/manual/config/items/itemtypes/zabbix_agent/zabbix_agent2) can be used with Zabbix agent 2 only.<br><br>See also:<br>- [Plugin documentation](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/memcached/README.md)<br>- [Configuration parameters](/manual/appendix/config/zabbix_agent2_plugins/memcached_plugin)|
|Modbus|Reads Modbus data.|modbus.get|Supported keys have the same parameters as Zabbix agent [keys](/manual/config/items/itemtypes/zabbix_agent).<br><br>See also:<br>- [Plugin documentation](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/modbus/README.md)<br>- [Configuration parameters](/manual/appendix/config/zabbix_agent2_plugins/modbus_plugin)|
|MongoDB|Monitoring of MongoDB servers and clusters (document-based, distributed database).|mongodb.collection.stats, mongodb.collections.discovery, mongodb.collections.usage, mongodb.connpool.stats,<br>mongodb.db.stats, mongodb.db.discovery, mongodb.jumbo\_chunks.count, mongodb.oplog.stats,<br>mongodb.ping, mongodb.rs.config, mongodb.rs.status, mongodb.server.status,<br>mongodb.sh.discovery|Supported MongoDB versions: 2.6-5.3<br><br>Supported [keys](/manual/config/items/itemtypes/zabbix_agent/zabbix_agent2) can be used with Zabbix agent 2 only.<br><br>*Plugins.MongoDB.System.Path* variable needs to be set in Zabbix agent 2 configuration file with the path to the MongoDB plugin executable. <br><br> See also:<br>- [Plugin documentation](https://git.zabbix.com/projects/AP/repos/mongodb/browse/README.md)<br>- [Configuration parameters](/manual/appendix/config/zabbix_agent2_plugins/mongodb_plugin)|
|MQTT|Receives published values of MQTT topics.|mqtt.get|Supported [keys](/manual/config/items/itemtypes/zabbix_agent/zabbix_agent2) can be used with Zabbix agent 2 only.<br><br>See also:<br>- [Plugin documentation](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/mqtt/README.md)<br>- [Configuration parameters](/manual/appendix/config/zabbix_agent2_plugins/mqtt_plugin)|
|MySQL|Monitoring of MySQL and its forks.|mysql.db.discovery, mysql.db.size, mysql.get\_status\_variables,<br>mysql.ping, mysql.replication.discovery, mysql.replication.get\_slave\_status, mysql.version|To configure encrypted connection to the database, use [named sessions](#named_sessions) and specify TLS parameters for the named session in the agent configuration file. Currently, TLS parameters cannot be passed as item key parameters.<br><br>Supported [keys](/manual/config/items/itemtypes/zabbix_agent/zabbix_agent2) can be used with Zabbix agent 2 only.<br><br>See also:<br>- [Plugin documentation](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/mysql/README.md)<br>- [Configuration parameters](/manual/appendix/config/zabbix_agent2_plugins/mysql_plugin)|
|NetIf|Monitoring of network interfaces.|net.if.collisions, net.if.discovery, net.if.in, net.if.out, net.if.total|Supported keys have the same parameters as Zabbix agent [keys](/manual/config/items/itemtypes/zabbix_agent).<br><br> See the [NetIf plugin source code](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/net/netif) here.|
|Oracle|Oracle Database monitoring.|oracle.diskgroups.stats, oracle.diskgroups.discovery, oracle.archive.info, oracle.archive.discovery,<br>oracle.cdb.info, oracle.custom.query, oracle.datafiles.stats, oracle.db.discovery,<br>oracle.fra.stats, oracle.instance.info, oracle.pdb.info, oracle.pdb.discovery,<br>oracle.pga.stats, oracle.ping, oracle.proc.stats, oracle.redolog.info,<br>oracle.sga.stats, oracle.sessions.stats, oracle.sys.metrics, oracle.sys.params,<br>oracle.ts.stats, oracle.ts.discovery, oracle.user.info|Install the [Oracle Instant Client](https://www.oracle.com/database/technologies/instant-client/downloads.html) before using the plugin.<br><br>Supported [keys](/manual/config/items/itemtypes/zabbix_agent/zabbix_agent2) can be used with Zabbix agent 2 only.<br><br>See also:<br>- [Plugin documentation](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/oracle/README.md)<br>- [Configuration parameters](/manual/appendix/config/zabbix_agent2_plugins/oracle_plugin)|
|PostgreSQL|Monitoring of PostgreSQL and its forks.|pgsql.autovacuum.count, pgsql.archive, pgsql.bgwriter, pgsql.cache.hit, pgsql.connections,<br> pgsql.custom.query, pgsql.dbstat, pgsql.dbstat.sum, pgsql.db.age, pgsql.db.bloating\_tables, <br> pgsql.db.discovery, pgsql.db.size, pgsql.locks, pgsql.oldest.xid, pgsql.ping, pgsql.queries, <br> pgsql.replication.count, pgsql.replication.process, pgsql.replication.process.discovery, pgsql.replication.recovery\_role, pgsql.replication.status, <br> pgsql.replication\_lag.b, pgsql.replication\_lag.sec, pgsql.uptime, pgsql.wal.stat |To configure encrypted connection to the database, use [named sessions](#named_sessions) and specify TLS parameters for the named session in the agent configuration file. Currently, TLS parameters cannot be passed as item key parameters.<br><br>Supported [keys](/manual/config/items/itemtypes/zabbix_agent/zabbix_agent2) can be used with Zabbix agent 2 only.<br><br>It is possible to extend functionality of the plugin with user-defined queries - see [Plugin documentation](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/postgres/README.md) for details.<br><br> See also: [Configuration parameters](/manual/appendix/config/zabbix_agent2_plugins/postgres_plugin)|
|Proc|Process CPU utilization percentage.|proc.cpu.util|Supported key has the same parameters as Zabbix agent [key](/manual/config/items/itemtypes/zabbix_agent).<br><br> See the [Proc plugin source code]( https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/proc) here.|
|Redis|Redis server monitoring.|redis.config, redis.info, redis.ping, redis.slowlog.count|Supported [keys](/manual/config/items/itemtypes/zabbix_agent/zabbix_agent2) can be used with Zabbix agent 2 only.<br><br>See also:<br>- [Plugin documentation](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/redis/README.md)<br>- [Configuration parameters](/manual/appendix/config/zabbix_agent2_plugins/redis_plugin)|
|Smart|S.M.A.R.T. monitoring.|smart.attribute.discovery, smart.disk.discovery, smart.disk.get|Sudo/root access rights to smartctl are required for the user executing Zabbix agent 2. The minimum required smartctl version is 7.1.<br><br>Supported [keys](/manual/config/items/itemtypes/zabbix_agent/zabbix_agent2) can be used with Zabbix agent 2 only on Linux/Windows, both as a passive and active check.<br>See also:<br>[Configuration parameters](/manual/appendix/config/zabbix_agent2_plugins/smart_plugin)<br><br> See the [Smart plugin source code](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/smart) here.|
|Swap|Swap space size in bytes/percentage.|system.swap.size|Supported key has the same parameters as Zabbix agent [key](/manual/config/items/itemtypes/zabbix_agent).<br><br> See the [Swap plugin source code](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/system/swap) here.|
|SystemRun|Runs specified command.|system.run|Supported key has the same parameters as Zabbix agent [key](/manual/config/items/itemtypes/zabbix_agent).<br><br>See also:<br>Plugin configuration parameters ([Unix](/manual/appendix/config/zabbix_agent2)/[Windows](/manual/appendix/config/zabbix_agent2_win))|
|Systemd|Monitoring of systemd services.|systemd.unit.discovery, systemd.unit.get, systemd.unit.info|Supported [keys](/manual/config/items/itemtypes/zabbix_agent/zabbix_agent2) can be used with Zabbix agent 2 only.<br><br> See the [Systemd plugin source code](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/systemd) here.|
|TCP|TCP connection availability check.|net.tcp.port|Supported key has the same parameters as Zabbix agent [key](/manual/config/items/itemtypes/zabbix_agent).<br><br> See the [TCP plugin source code](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/net/tcp) here.|
|UDP|Monitoring of the UDP services availability and performance.|net.udp.service, net.udp.service.perf|Supported keys have the same parameters as Zabbix agent [keys](/manual/config/items/itemtypes/zabbix_agent).<br><br> See the [UDP plugin source code](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/net/udp) here.|
|Uname|Retrieval of information about the system.|system.hostname, system.sw.arch, system.uname|Supported keys have the same parameters as Zabbix agent [keys](/manual/config/items/itemtypes/zabbix_agent).<br><br> See the [Uname plugin source code](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/system/uname) here.|
|Uptime|System uptime metrics collection.|system.uptime|Supported key has the same parameters as Zabbix agent [key](/manual/config/items/itemtypes/zabbix_agent).<br><br> See the [Uptime plugin source code](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/system/uptime) here.|
|VFSDev|VFS metrics collection.|vfs.dev.discovery, vfs.dev.read, vfs.dev.write|Supported keys have the same parameters as Zabbix agent [keys](/manual/config/items/itemtypes/zabbix_agent).<br><br> See the [VFSDEv plugin source code](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/vfs/dev) here.|
|WebCertificate|Monitoring of TLS/SSL website certificates.|web.certificate.get|Supported [key](/manual/config/items/itemtypes/zabbix_agent/zabbix_agent2) can be used with Zabbix agent 2 only.<br><br> See the [WebCertificate plugin source code](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/web/certificate) here.|
|WebPage|Web page monitoring.|web.page.get, web.page.perf, web.page.regexp|Supported keys have the same parameters as Zabbix agent [keys](/manual/config/items/itemtypes/zabbix_agent).<br><br> See the [WebPage plugin source code](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/web/page) here.|
|ZabbixAsync|Asynchronous metrics collection.|net.tcp.listen, net.udp.listen, sensor, system.boottime, system.cpu.intr, system.cpu.load,<br>system.cpu.switches, system.hw.cpu, system.hw.macaddr, system.localtime, system.sw.os,<br>system.swap.in, system.swap.out, vfs.fs.discovery|Supported keys have the same parameters as Zabbix agent [keys](/manual/config/items/itemtypes/zabbix_agent).<br><br> See the [ZabbixAsync plugin source code](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/zabbix/async) here.|
|ZabbixStats|Zabbix server/proxy internal metrics or number of delayed items in a queue.|zabbix.stats|Supported keys have the same parameters as Zabbix agent [keys](/manual/config/items/itemtypes/zabbix_agent).<br><br> See the [ZabbixStats plugin source code](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/zabbix/stats) here.|
|ZabbixSync|Synchronous metrics collection.|net.dns, net.dns.record, net.tcp.service, net.tcp.service.perf, proc.mem,<br>proc.num, system.hw.chassis, system.hw.devices, system.sw.packages,<br>system.users.num, vfs.dir.count, vfs.dir.size, vfs.fs.get, vfs.fs.inode,<br>vfs.fs.size, vm.memory.size.|Supported keys have the same parameters as Zabbix agent [keys](/manual/config/items/itemtypes/zabbix_agent).<br><br> See the [ZabbixSync plugin source code](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/zabbix/sync) here.|

[comment]: # ({/new-4c4f3ca2})

[comment]: # ({new-ebdf4ed5})

##### Built-in

The following plugins for Zabbix agent 2 are available out-of-the-box. Click on the plugin name to go to the plugin repository with additional information.

|Plugin name|Description|[Supported item keys](/manual/config/items/itemtypes/zabbix_agent/zabbix_agent2)|Comments|
|--|--|--|----|
|Agent|Metrics of the Zabbix agent being used.|agent.hostname, agent.ping, agent.version|Supported keys have the same parameters as Zabbix agent [keys](/manual/config/items/itemtypes/zabbix_agent).|
|[Ceph](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/ceph/README.md)|Ceph monitoring.|ceph.df.details, ceph.osd.stats, ceph.osd.discovery, ceph.osd.dump,<br>ceph.ping, ceph.pool.discovery, ceph.status| |
|[CPU](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/system/cpu)|System CPU monitoring (number of CPUs/CPU cores, discovered CPUs, utilization percentage).|system.cpu.discovery, system.cpu.num, system.cpu.util|Supported keys have the same parameters as Zabbix agent [keys](/manual/config/items/itemtypes/zabbix_agent).|
|[Docker](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/docker)|Monitoring of Docker containers.|docker.container\_info, docker.container\_stats, docker.containers, docker.containers.discovery,<br>docker.data\_usage, docker.images, docker.images.discovery, docker.info, docker.ping|See also:<br>[Configuration parameters](/manual/appendix/config/zabbix_agent2_plugins/d_plugin)|
|File|File metrics collection.|vfs.file.cksum, vfs.file.contents, vfs.file.exists, vfs.file.md5sum,<br>vfs.file.regexp, vfs.file.regmatch, vfs.file.size, vfs.file.time|Supported keys have the same parameters as Zabbix agent [keys](/manual/config/items/itemtypes/zabbix_agent).|
|[Kernel](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/kernel)|Kernel monitoring.|kernel.maxfiles, kernel.maxproc|Supported keys have the same parameters as Zabbix agent [keys](/manual/config/items/itemtypes/zabbix_agent).|
|[Log](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/log)|Log file monitoring.|log, log.count, logrt, logrt.count|Supported keys have the same parameters as Zabbix agent [keys](/manual/config/items/itemtypes/zabbix_agent).<br><br>See also:<br>Plugin configuration parameters ([Unix](/manual/appendix/config/zabbix_agent2)/[Windows](/manual/appendix/config/zabbix_agent2_win))|
|[Memcached](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/memcached/README.md)|Memcached server monitoring.|memcached.ping, memchached.stats| |
|[Modbus](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/modbus/README.md)|Reads Modbus data.|modbus.get|Supported keys have the same parameters as Zabbix agent [keys](/manual/config/items/itemtypes/zabbix_agent).|
|[MQTT](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/mqtt/README.md)|Receives published values of MQTT topics.|mqtt.get| |
|[MySQL](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/mysql/README.md)|Monitoring of MySQL and its forks.|mysql.db.discovery, mysql.db.size, mysql.get\_status\_variables,<br>mysql.ping, mysql.replication.discovery, mysql.replication.get\_slave\_status, mysql.version|To configure encrypted connection to the database, use [named sessions](#named_sessions) and specify the TLS parameters for the named session in the agent configuration file. Currently, TLS parameters cannot be passed as item key parameters.|
|[NetIf](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/net/netif)|Monitoring of network interfaces.|net.if.collisions, net.if.discovery, net.if.in, net.if.out, net.if.total|Supported keys have the same parameters as Zabbix agent [keys](/manual/config/items/itemtypes/zabbix_agent).|
|[Oracle](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/oracle/README.md)|Oracle Database monitoring.|oracle.diskgroups.stats, oracle.diskgroups.discovery, oracle.archive.info, oracle.archive.discovery,<br>oracle.cdb.info, oracle.custom.query, oracle.datafiles.stats, oracle.db.discovery,<br>oracle.fra.stats, oracle.instance.info, oracle.pdb.info, oracle.pdb.discovery,<br>oracle.pga.stats, oracle.ping, oracle.proc.stats, oracle.redolog.info,<br>oracle.sga.stats, oracle.sessions.stats, oracle.sys.metrics, oracle.sys.params,<br>oracle.ts.stats, oracle.ts.discovery, oracle.user.info|Install the [Oracle Instant Client](https://www.oracle.com/database/technologies/instant-client/downloads.html) before using the plugin.|
|[Proc](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/proc)|Process CPU utilization percentage.|proc.cpu.util|Supported key has the same parameters as Zabbix agent [key](/manual/config/items/itemtypes/zabbix_agent).|
|[Redis](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/redis/README.md)|Redis server monitoring.|redis.config, redis.info, redis.ping, redis.slowlog.count| |
|[Smart](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/smart)|S.M.A.R.T. monitoring.|smart.attribute.discovery, smart.disk.discovery, smart.disk.get|Sudo/root access rights to smartctl are required for the user executing Zabbix agent 2. The minimum required smartctl version is 7.1.<br><br>Supported [keys](/manual/config/items/itemtypes/zabbix_agent/zabbix_agent2) can be used with Zabbix agent 2 only on Linux/Windows, both as a passive and active check.<br>See also:<br>[Configuration parameters](/manual/appendix/config/zabbix_agent2_plugins/smart_plugin)|
|[Swap](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/system/swap)|Swap space size in bytes/percentage.|system.swap.size|Supported key has the same parameters as Zabbix agent [key](/manual/config/items/itemtypes/zabbix_agent).|
|SystemRun|Runs specified command.|system.run|Supported key has the same parameters as Zabbix agent [key](/manual/config/items/itemtypes/zabbix_agent).<br><br>See also:<br>Plugin configuration parameters ([Unix](/manual/appendix/config/zabbix_agent2)/[Windows](/manual/appendix/config/zabbix_agent2_win))|
|Systemd|Monitoring of systemd services.|systemd.unit.discovery, systemd.unit.get, systemd.unit.info| |
|[TCP](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/net/tcp)|TCP connection availability check.|net.tcp.port|Supported key has the same parameters as Zabbix agent [key](/manual/config/items/itemtypes/zabbix_agent).|
|[UDP](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/net/udp)|Monitoring of the UDP services availability and performance.|net.udp.service, net.udp.service.perf|Supported keys have the same parameters as Zabbix agent [keys](/manual/config/items/itemtypes/zabbix_agent).|
|[Uname](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/system/uname)|Retrieval of information about the system.|system.hostname, system.sw.arch, system.uname|Supported keys have the same parameters as Zabbix agent [keys](/manual/config/items/itemtypes/zabbix_agent).|
|[Uptime](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/system/uptime)|System uptime metrics collection.|system.uptime|Supported key has the same parameters as Zabbix agent [key](/manual/config/items/itemtypes/zabbix_agent).|
|[VFSDev](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/vfs/dev)|VFS metrics collection.|vfs.dev.discovery, vfs.dev.read, vfs.dev.write|Supported keys have the same parameters as Zabbix agent [keys](/manual/config/items/itemtypes/zabbix_agent).|
|[WebCertificate](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/web/certificate)|Monitoring of TLS/SSL website certificates.|web.certificate.get| |
|[WebPage](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/web/page)|Web page monitoring.|web.page.get, web.page.perf, web.page.regexp|Supported keys have the same parameters as Zabbix agent [keys](/manual/config/items/itemtypes/zabbix_agent).|
|[ZabbixAsync](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/zabbix/async)|Asynchronous metrics collection.|net.tcp.listen, net.udp.listen, sensor, system.boottime, system.cpu.intr, system.cpu.load,<br>system.cpu.switches, system.hw.cpu, system.hw.macaddr, system.localtime, system.sw.os,<br>system.swap.in, system.swap.out, vfs.fs.discovery|Supported keys have the same parameters as Zabbix agent [keys](/manual/config/items/itemtypes/zabbix_agent).|
|[ZabbixStats](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/zabbix/stats)|Zabbix server/proxy internal metrics or number of delayed items in a queue.|zabbix.stats|Supported keys have the same parameters as Zabbix agent [keys](/manual/config/items/itemtypes/zabbix_agent).|
|[ZabbixSync](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/zabbix/sync)|Synchronous metrics collection.|net.dns, net.dns.record, net.tcp.service, net.tcp.service.perf, proc.mem,<br>proc.num, system.hw.chassis, system.hw.devices, system.sw.packages,<br>system.users.num, vfs.dir.count, vfs.dir.size, vfs.fs.get, vfs.fs.inode,<br>vfs.fs.size, vm.memory.size.|Supported keys have the same parameters as Zabbix agent [keys](/manual/config/items/itemtypes/zabbix_agent).|

[comment]: # ({/new-ebdf4ed5})

[comment]: # ({new-9ecbdda6})

##### Loadable

::: notetip
Loadable plugins, when launched with:<br>
-   *-V --version* - print plugin version and license information;<br>
-   *-h --help* - print help information.
:::

Click on the plugin name to go to the plugin repository with additional information.

|Plugin name|Description|Supported item keys|Comments|
|--|--|--|----|
|[MongoDB](https://git.zabbix.com/projects/AP/repos/mongodb/browse/README.md)|Monitoring of MongoDB servers and clusters (document-based, distributed database).|mongodb.collection.stats, mongodb.collections.discovery, mongodb.collections.usage, mongodb.connpool.stats,<br>mongodb.db.stats, mongodb.db.discovery, mongodb.jumbo\_chunks.count, mongodb.oplog.stats,<br>mongodb.ping, mongodb.rs.config, mongodb.rs.status, mongodb.server.status,<br>mongodb.sh.discovery| |
|[PostgreSQL](https://git.zabbix.com/projects/AP/repos/postgresql/browse/README.md)|Monitoring of PostgreSQL and its forks.|pgsql.autovacuum.count, pgsql.archive, pgsql.bgwriter, pgsql.cache.hit, pgsql.connections,<br> pgsql.custom.query, pgsql.dbstat, pgsql.dbstat.sum, pgsql.db.age, pgsql.db.bloating\_tables, <br> pgsql.db.discovery, pgsql.db.size, pgsql.locks, pgsql.oldest.xid, pgsql.ping, pgsql.queries, <br> pgsql.replication.count, pgsql.replication.process, pgsql.replication.process.discovery, pgsql.replication.recovery\_role, pgsql.replication.status, <br> pgsql.replication\_lag.b, pgsql.replication\_lag.sec, pgsql.uptime, pgsql.wal.stat |This plugin is loadable since Zabbix 6.0.10 (built-in previously).<br><br>To configure encrypted connections to the database, use [named sessions](#named_sessions) and specify the TLS parameters for the named session in the agent configuration file. Currently, TLS parameters cannot be passed as item key parameters.|

See also: [Building loadable plugins](/manual/config/items/plugins/build).

[comment]: # ({/new-9ecbdda6})
