[comment]: # translation:outdated

[comment]: # ({new-5e482050})
# 12 Trapper items

[comment]: # ({/new-5e482050})

[comment]: # ({new-03e7aedb})
#### Overview

Trapper items accept incoming data instead of querying for it.

It is useful for any data you might want to "push" into Zabbix.

To use a trapper item you must:

-   have a trapper item set up in Zabbix
-   send in the data into Zabbix

[comment]: # ({/new-03e7aedb})

[comment]: # ({new-5d32b87c})
#### Configuration

[comment]: # ({/new-5d32b87c})

[comment]: # ({new-62c22fbb})
##### Item configuration

To configure a trapper item:

-   Go to: *Configuration* → *Hosts*
-   Click on *Items* in the row of the host
-   Click on *Create item*
-   Enter parameters of the item in the form

![](../../../../../assets/en/manual/config/items/itemtypes/trapper_item.png)

All mandatory input fields are marked with a red asterisk.

The fields that require specific information for trapper items are:

|   |   |
|---|---|
|*Type*|Select **Zabbix trapper** here.|
|*Key*|Enter a key that will be used to recognize the item when sending in data.|
|*Type of information*|Select the type of information that will correspond the format of data that will be sent in.|
|*Allowed hosts*|List of comma delimited IP addresses, optionally in CIDR notation, or hostnames.<br>If specified, incoming connections will be accepted only from the hosts listed here.<br>If IPv6 support is enabled then '127.0.0.1', '::127.0.0.1', '::ffff:127.0.0.1' are treated equally and '::/0' will allow any IPv4 or IPv6 address.<br>'0.0.0.0/0' can be used to allow any IPv4 address.<br>Note, that "IPv4-compatible IPv6 addresses" (0000::/96 prefix) are supported but deprecated by [RFC4291](https://tools.ietf.org/html/rfc4291#section-2.5.5).<br>Example: Server=127.0.0.1, 192.168.1.0/24, 192.168.3.1-255, 192.168.1-10.1-255, ::1,2001:db8::/32, zabbix.domain<br>Spaces and [user macros](/manual/config/macros/user_macros) are allowed in this field since Zabbix 2.2.0.<br>Host macros: {HOST.HOST}, {HOST.NAME}, {HOST.IP}, {HOST.DNS}, {HOST.CONN} are allowed in this field since Zabbix 4.0.2.|

::: noteclassic
You may have to wait up to 60 seconds after saving the item
until the server picks up the changes from a configuration cache update,
before you can send in values.
:::

[comment]: # ({/new-62c22fbb})

[comment]: # ({new-5f246209})
##### Sending in data

In the simplest of cases, we may use
[zabbix\_sender](/manual/concepts/sender) utility to send in some 'test
value':

    zabbix_sender -z <server IP address> -p 10051 -s "New host" -k trap -o "test value"

To send in the value we use these keys:

*-z* - to specify Zabbix server IP address

*-p* - to specify Zabbix server port number (10051 by default)

*-s* - to specify the host (make sure to use the 'technical' [host
name](/manual/config/hosts/host#configuration) here, instead of the
'visible' name)

*-k* - to specify the key of the item we just defined

*-o* - to specify the actual value to send

::: noteimportant
Zabbix trapper process does not expand macros used
in the item key in attempt to check corresponding item key existence for
targeted host.
:::

[comment]: # ({/new-5f246209})

[comment]: # ({new-1e9d760a})
##### Display

This is the result in *Monitoring → Latest data*:

![](../../../../../assets/en/manual/config/items/itemtypes/trapped_data.png){width="600"}

Note that if a single numeric value is sent in, the data graph will show
a horizontal line to the left and to the right of the time point of the
value.

[comment]: # ({/new-1e9d760a})
