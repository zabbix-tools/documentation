[comment]: # attributes: notoc

[comment]: # ({new-871f8735})
# Item keys specific to agent 2

[comment]: # ({/new-871f8735})

[comment]: # translation:outdated


[comment]: # ({new-3728d5da})

Zabbix agent 2 supports all item keys supported for Zabbix agent on [Unix](/manual/config/items/itemtypes/zabbix_agent) and [Windows](manual/config/items/itemtypes/zabbix_agent/win_keys). This page provides details on the additional item keys, which you can use with Zabbix
agent 2 only, grouped by the plugin they belong to.

See also: [Plugins supplied
out-of-the-box](/manual/config/items/plugins#plugins_supplied_out-of-the-box)

::: noteclassic
 Parameters without angle brackets are mandatory. Parameters
marked with angle brackets **<** **>** are optional.
:::

[comment]: # ({/new-3728d5da})

[comment]: # ({new-3077d649})
### Item key details

Parameters without angle brackets are mandatory. Parameters marked with angle brackets **<** **>** are optional.

[comment]: # ({/new-3077d649})

[comment]: # ({new-e443ac84})

##### ceph.df.details[connString,<user>,<apikey>]

<br>
The cluster’s data usage and distribution among pools.<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the Ceph login credentials.<br>

[comment]: # ({/new-e443ac84})

[comment]: # ({new-8b08b33c})

##### ceph.osd.stats[connString,<user>,<apikey>]

<br>
Aggregated and per OSD statistics.<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the Ceph login credentials.<br>

[comment]: # ({/new-8b08b33c})

[comment]: # ({new-07bfdcb6})

##### ceph.osd.discovery[connString,<user>,<apikey>]

<br>
The list of discovered OSDs. Used for [low-level discovery](/manual/discovery/low_level_discovery/examples/systemd).<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the Ceph login credentials.<br>

[comment]: # ({/new-07bfdcb6})

[comment]: # ({new-236454b9})

##### ceph.osd.dump[connString,<user>,<apikey>]

<br>
The usage thresholds and statuses of OSDs.<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the Ceph login credentials.<br>

[comment]: # ({/new-236454b9})

[comment]: # ({new-04750a8f})

##### ceph.ping[connString,<user>,<apikey>]

<br>
Tests whether a connection to Ceph can be established.<br>
Return value: *0* - connection is broken (if there is any error presented including AUTH and configuration issues); *1* - connection is successful.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the Ceph login credentials.<br>

[comment]: # ({/new-04750a8f})

[comment]: # ({new-5930838a})

##### ceph.pool.discovery[connString,<user>,<apikey>]

<br>
The list of discovered pools. Used for [low-level discovery](/manual/discovery/low_level_discovery/examples/systemd).<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the Ceph login credentials.<br>

[comment]: # ({/new-5930838a})

[comment]: # ({new-d2e29bd4})

##### ceph.status[connString,<user>,<apikey>]

<br>
The overall cluster's status.<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the Ceph login credentials.<br>

[comment]: # ({/new-d2e29bd4})

[comment]: # ({new-0f9ad2d9})

##### docker.container_info[<ID>,<info>]

<br>
Low-level information about a container.<br>
Return value: The output of the [ContainerInspect](https://docs.docker.com/engine/api/v1.28/#operation/ContainerInspect) API call serialized as JSON.

Parameters:

-   **ID** - the ID or name of the container;<br>
-   **info** - the amount of information returned. Supported values: *short* (default) or *full*.

The Agent 2 user ('zabbix') must be added to the 'docker' [group](https://docs.docker.com/engine/install/linux-postinstall/#manage-docker-as-a-non-root-user) for sufficient privileges. Otherwise the check will fail.

[comment]: # ({/new-0f9ad2d9})

[comment]: # ({new-9e9006b3})

##### docker.container_stats[<ID>]

<br>
The container resource usage statistics.<br>
Return value: The output of the [ContainerStats](https://docs.docker.com/engine/api/v1.28/#operation/ContainerStats) API call and CPU usage percentage serialized as JSON.

Parameter:

-   **ID** - the ID or name of the container.

The Agent 2 user ('zabbix') must be added to the 'docker' [group](https://docs.docker.com/engine/install/linux-postinstall/#manage-docker-as-a-non-root-user) for sufficient privileges. Otherwise the check will fail.

[comment]: # ({/new-9e9006b3})

[comment]: # ({new-30610c8f})

##### docker.containers

<br>
The list of containers.<br>
Return value: The output of the [ContainerList](https://docs.docker.com/engine/api/v1.28/#operation/ContainerList) API call serialized as JSON.

The Agent 2 user ('zabbix') must be added to the 'docker' [group](https://docs.docker.com/engine/install/linux-postinstall/#manage-docker-as-a-non-root-user) for sufficient privileges. Otherwise the check will fail.

[comment]: # ({/new-30610c8f})

[comment]: # ({new-39cceff0})

##### docker.containers.discovery[<options>]

<br>
Returns the list of containers. Used for [low-level discovery](/manual/discovery/low_level_discovery/).<br>
Return value: *JSON object*.

Parameter:

-   **options** - specify whether all or only running containers should be discovered. Supported values: *true* - return all containers; *false* - return only running containers (default).

The Agent 2 user ('zabbix') must be added to the 'docker' [group](https://docs.docker.com/engine/install/linux-postinstall/#manage-docker-as-a-non-root-user) for sufficient privileges. Otherwise the check will fail.

[comment]: # ({/new-39cceff0})

[comment]: # ({new-7a99eb4b})

##### docker.data.usage

<br>
Information about the current data usage.<br>
Return value: The output of the [SystemDataUsage](https://docs.docker.com/engine/api/v1.28/#operation/SystemDataUsage) API call serialized as JSON.

The Agent 2 user ('zabbix') must be added to the 'docker' [group](https://docs.docker.com/engine/install/linux-postinstall/#manage-docker-as-a-non-root-user) for sufficient privileges. Otherwise the check will fail.

[comment]: # ({/new-7a99eb4b})

[comment]: # ({new-6c71011c})

##### docker.images

<br>
Returns the list of images.<br>
Return value: The output of the [ImageList](https://docs.docker.com/engine/api/v1.28/#operation/ImageList) API call serialized as JSON.

The Agent 2 user ('zabbix') must be added to the 'docker' [group](https://docs.docker.com/engine/install/linux-postinstall/#manage-docker-as-a-non-root-user) for sufficient privileges. Otherwise the check will fail.

[comment]: # ({/new-6c71011c})

[comment]: # ({new-1c03f201})

##### docker.images.discovery

<br>
Returns the list of images. Used for [low-level discovery](/manual/discovery/low_level_discovery/).<br>
Return value: *JSON object*.

The Agent 2 user ('zabbix') must be added to the 'docker' [group](https://docs.docker.com/engine/install/linux-postinstall/#manage-docker-as-a-non-root-user) for sufficient privileges. Otherwise the check will fail.

[comment]: # ({/new-1c03f201})

[comment]: # ({new-d549fed5})

##### docker.info

<br>
The system information.<br>
Return value: The output of the [SystemInfo](https://docs.docker.com/engine/api/v1.28/#operation/SystemInfo) API call serialized as JSON.

The Agent 2 user ('zabbix') must be added to the 'docker' [group](https://docs.docker.com/engine/install/linux-postinstall/#manage-docker-as-a-non-root-user) for sufficient privileges. Otherwise the check will fail.

[comment]: # ({/new-d549fed5})

[comment]: # ({new-e8a64d82})

##### docker.ping

<br>
Test if the Docker daemon is alive or not.<br>
Return value: *1* - the connection is alive; *0* - the connection is broken.

The Agent 2 user ('zabbix') must be added to the 'docker' [group](https://docs.docker.com/engine/install/linux-postinstall/#manage-docker-as-a-non-root-user) for sufficient privileges. Otherwise the check will fail.

[comment]: # ({/new-e8a64d82})

[comment]: # ({new-d4eb298e})

##### memcached.ping[connString,<user>,<password>]

<br>
Test if a connection is alive or not.<br>
Return value: *1* - the connection is alive; *0* - the connection is broken (if there is any error presented including AUTH and configuration issues).

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the Memcached login credentials.<br>

[comment]: # ({/new-d4eb298e})

[comment]: # ({new-ceb374d4})

##### memcached.stats[connString,<user>,<password>,<type>]

<br>
Gets the output of the STATS command.<br>
Return value: *JSON* - the output is serialized as JSON.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the Memcached login credentials;<br>
-   **type** - stat type to be returned: *items*, *sizes*, *slabs* or *settings* (empty by default, returns general statistics).

[comment]: # ({/new-ceb374d4})

[comment]: # ({new-ba10c6a3})

##### mongodb.collection.stats[connString,<user>,<password>,<database>,collection]

<br>
Returns a variety of storage statistics for a given collection.<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the MongoDB login credentials;<br>
-   **database** - the database name (default: admin);<br>
-   **collection** - the collection name.

[comment]: # ({/new-ba10c6a3})

[comment]: # ({new-d3f908ab})

##### mongodb.collections.discovery[connString,<user>,<password>]

<br>
Returns a list of discovered collections. Used for [low-level discovery](/manual/discovery/low_level_discovery/examples/systemd).<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the MongoDB login credentials.<br>

[comment]: # ({/new-d3f908ab})

[comment]: # ({new-d9af9de6})

##### mongodb.collections.usage[connString,<user>,<password>]

<br>
Returns the usage statistics for collections.<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the MongoDB login credentials.<br>

[comment]: # ({/new-d9af9de6})

[comment]: # ({new-20da93d0})

##### mongodb.connpool.stats[connString,<user>,<password>]

<br>
Returns information regarding the open outgoing connections from the current database instance to other members of the sharded cluster or replica set.<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the MongoDB login credentials;<br>
-   **database** - the database name (default: admin);<br>
-   **collection** - the collection name.

[comment]: # ({/new-20da93d0})

[comment]: # ({new-52b800b7})

##### mongodb.db.stats[connString,<user>,<password>,<database>]

<br>
Returns the statistics reflecting a given database system state.<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the MongoDB login credentials;<br>
-   **database** - the database name (default: admin).<br>

[comment]: # ({/new-52b800b7})

[comment]: # ({new-e12a4df1})

##### mongodb.db.discovery[connString,<user>,<password>]

<br>
Returns a list of discovered databases. Used for [low-level discovery](/manual/discovery/low_level_discovery/examples/systemd).<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the MongoDB login credentials.<br>

[comment]: # ({/new-e12a4df1})

[comment]: # ({new-2d0db89c})

##### mongodb.jumbo_chunks.count[connString,<user>,<password>]

<br>
Returns the count of jumbo chunks.<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the MongoDB login credentials.<br>

[comment]: # ({/new-2d0db89c})

[comment]: # ({new-5bb28367})

##### mongodb.oplog.stats[connString,<user>,<password>]

<br>
Returns the status of the replica set, using data polled from the oplog.<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the MongoDB login credentials.<br>

[comment]: # ({/new-5bb28367})

[comment]: # ({new-57fd60cc})

##### mongodb.ping[connString,<user>,<password>]

<br>
Test if a connection is alive or not.<br>
Return value: *1* - the connection is alive; *0* - the connection is broken (if there is any error presented including AUTH and configuration issues).

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the MongoDB login credentials.<br>

[comment]: # ({/new-57fd60cc})

[comment]: # ({new-7bd73d75})

##### mongodb.rs.config[connString,<user>,<password>]

<br>
Returns the current configuration of the replica set.<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the MongoDB login credentials.<br>

[comment]: # ({/new-7bd73d75})

[comment]: # ({new-fbfe0a2b})

##### mongodb.rs.status[connString,<user>,<password>]

<br>
Returns the replica set status from the point of view of the member where the method is run.<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the MongoDB login credentials.<br>

[comment]: # ({/new-fbfe0a2b})

[comment]: # ({new-df1e29bd})

##### mongodb.server.status[connString,<user>,<password>]

<br>
Returns the database state.<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the MongoDB login credentials.<br>

[comment]: # ({/new-df1e29bd})

[comment]: # ({new-989959f8})

##### mongodb.sh.discovery[connString,<user>,<password>]

<br>
Returns the list of discovered shards present in the cluster.<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the MongoDB login credentials.<br>

[comment]: # ({/new-989959f8})

[comment]: # ({new-d1bb976a})

##### mqtt.get[<broker url>,topic,<username>,<password>]

<br>
Subscribes to a specific topic or topics (with wildcards) of the provided broker and waits for publications.<br>
Return value: Depending on topic content. If wildcards are used, returns topic content as JSON.

Parameters:

-   **broker url** - the MQTT broker URL (if empty, *localhost* with port 1883 is used);<br>
-   **topic** - the MQTT topic (mandatory). Wildcards (+,\#) are supported;<br>
-   **user, password** - the authentication credentials (if required).<br>

Comments:

-   The item must be configured as an [active check](/manual/appendix/items/activepassive#active_checks) ('Zabbix agent (active)' item type);
-   TLS encryption certificates can be used by saving them into a default location (e.g. `/etc/ssl/certs/` directory for Ubuntu). For TLS, use the `tls://` scheme.

[comment]: # ({/new-d1bb976a})

[comment]: # ({new-87221485})

##### mysql.db.discovery[connString,<user>,<password>]

<br>
Returns the list of MySQL databases. Used for [low-level discovery](/manual/discovery/low_level_discovery/examples/systemd).<br>
Return value: The result of the "show databases" SQL query in LLD JSON format.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the MySQL login credentials.<br>

[comment]: # ({/new-87221485})

[comment]: # ({new-661ad031})

##### mysql.db.size[connString,<user>,<password>,<database name>]

<br>
The database size in bytes.<br>
Return value: Result of the "select coalesce(sum(data_length + index_length),0) as size from information_schema.tables where table_schema=?" SQL query for specific database in bytes.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the MySQL login credentials;<br>
-   **database name** - the database name.

[comment]: # ({/new-661ad031})

[comment]: # ({new-c135cdf1})

##### mysql.get_status_variables[connString,<user>,<password>]

<br>
Values of the global status variables.<br>
Return value: Result of the "show global status" SQL query in JSON format.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the MySQL login credentials.<br>

[comment]: # ({/new-c135cdf1})

[comment]: # ({new-353c20ea})

##### mysql.ping[connString,<user>,<password>]

<br>
Test if a connection is alive or not.<br>
Return value: *1* - the connection is alive; *0* - the connection is broken (if there is any error presented including AUTH and configuration issues).

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the MySQL login credentials.<br>

[comment]: # ({/new-353c20ea})

[comment]: # ({new-baf67c1f})

##### mysql.replication.discovery[connString,<user>,<password>]

<br>
Returns the list of MySQL replications. Used for [low-level discovery](/manual/discovery/low_level_discovery/examples/systemd).<br>
Return value: The result of the "show slave status" SQL query in LLD JSON format.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the MySQL login credentials.<br>

[comment]: # ({/new-baf67c1f})

[comment]: # ({new-90f6ce4d})

##### mysql.replication.get_slave_status[connString,<user>,<password>,<master host>]

<br>
The replication status.<br>
Return value: Result of the "show slave status" SQL query in JSON format.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the MySQL login credentials;<br>
-   **master host** - the replication master host name.<br>

[comment]: # ({/new-90f6ce4d})

[comment]: # ({new-f55a22aa})

##### mysql.version[connString,<user>,<password>]

<br>
The MySQL version.<br>
Return value: *String* (with the MySQL instance version).

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the MySQL login credentials.<br>

[comment]: # ({/new-f55a22aa})

[comment]: # ({new-9773f5ca})

##### oracle.diskgroups.stats[connString,<user>,<password>,<service>,<diskgroup>]

<br>
Returns the Automatic Storage Management (ASM) disk groups statistics.<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the Oracle login credentials;<br>
-   **service** - the Oracle service name;<br>
-   **diskgroup** - the name of the ASM disk group to query.

[comment]: # ({/new-9773f5ca})

[comment]: # ({new-ecd5f683})

##### oracle.diskgroups.discovery[connString,<user>,<password>,<service>]

<br>
Returns the list of ASM disk groups. Used for [low-level discovery](/manual/discovery/low_level_discovery/examples/systemd).<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the Oracle login credentials;<br>
-   **service** - the Oracle service name.<br>

[comment]: # ({/new-ecd5f683})

[comment]: # ({new-dcf5ffe2})

##### oracle.archive.info[connString,<user>,<password>,<service>,<destination>]

<br>
The archive logs statistics.<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the Oracle login credentials;<br>
-   **service** - the Oracle service name;<br>
-   **destination** - the name of the destination to query.

[comment]: # ({/new-dcf5ffe2})

[comment]: # ({new-a1afc726})

##### oracle.cdb.info[connString,<user>,<password>,<service>,<database>]

<br>
The Container Databases (CDBs) information.<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the Oracle login credentials;<br>
-   **service** - the Oracle service name;<br>
-   **destination** - the name of the database to query.

[comment]: # ({/new-a1afc726})

[comment]: # ({new-83723c39})

##### oracle.custom.query[connString,<user>,<password>,<service>,queryName,<args...>]

<br>
The result of a custom query.<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the Oracle login credentials;<br>
-   **service** - the Oracle service name;<br>
-   **queryName** — the name of a custom query (must be equal to the name of an SQL file without an extension);
-   **args...** — one or several comma-separated arguments to pass to the query.

[comment]: # ({/new-83723c39})

[comment]: # ({new-ef413040})

##### oracle.datafiles.stats[connString,<user>,<password>,<service>]

<br>
Returns the data files statistics.<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the Oracle login credentials;<br>
-   **service** - the Oracle service name;<br>
-   **diskgroup** - the name of the ASM disk group to query.

[comment]: # ({/new-ef413040})

[comment]: # ({new-3aee96da})

##### oracle.db.discovery[connString,<user>,<password>,<service>]

<br>
Returns the list of databases. Used for [low-level discovery](/manual/discovery/low_level_discovery/examples/systemd).<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the Oracle login credentials;<br>
-   **service** - the Oracle service name.<br>

[comment]: # ({/new-3aee96da})

[comment]: # ({new-d1925e26})

##### oracle.fra.stats[connString,<user>,<password>,<service>]

<br>
Returns the Fast Recovery Area (FRA) statistics.<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the Oracle login credentials;<br>
-   **service** - the Oracle service name.<br>

[comment]: # ({/new-d1925e26})

[comment]: # ({new-d0b09033})

##### oracle.instance.info[connString,<user>,<password>,<service>]

<br>
The instance statistics.<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the Oracle login credentials;<br>
-   **service** - the Oracle service name.<br>

[comment]: # ({/new-d0b09033})

[comment]: # ({new-e6cb70e2})

##### oracle.pdb.info[connString,<user>,<password>,<service>,<database>]

<br>
The Pluggable Databases (PDBs) information.<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the Oracle login credentials;<br>
-   **service** - the Oracle service name;<br>
-   **destination** - the name of the database to query.

[comment]: # ({/new-e6cb70e2})

[comment]: # ({new-7cabfeb8})

##### oracle.pdb.discovery[connString,<user>,<password>,<service>]

<br>
Returns the list of PDBs. Used for [low-level discovery](/manual/discovery/low_level_discovery/examples/systemd).<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the Oracle login credentials;<br>
-   **service** - the Oracle service name.<br>

[comment]: # ({/new-7cabfeb8})

[comment]: # ({new-2ca3eeaf})

##### oracle.pga.stats[connString,<user>,<password>,<service>]

<br>
Returns the Program Global Area (PGA) statistics.<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the Oracle login credentials;<br>
-   **service** - the Oracle service name.<br>

[comment]: # ({/new-2ca3eeaf})

[comment]: # ({new-e13b9288})

##### oracle.ping[connString,<user>,<password>,<service>]

<br>
Test whether a connection to Oracle can be established.<br>
Return value: *1* - the connection is successful; *0* - the connection is broken (if there is any error presented including AUTH and configuration issues).

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the Oracle login credentials;<br>
-   **service** - the Oracle service name.<br>

[comment]: # ({/new-e13b9288})

[comment]: # ({new-77cd7d3d})

##### oracle.proc.stats[connString,<user>,<password>,<service>]

<br>
Returns the processes statistics.<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the Oracle login credentials;<br>
-   **service** - the Oracle service name.<br>

[comment]: # ({/new-77cd7d3d})

[comment]: # ({new-f219cf27})

##### oracle.redolog.info[connString,<user>,<password>,<service>]

<br>
The log file information from the control file.<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the Oracle login credentials;<br>
-   **service** - the Oracle service name.<br>

[comment]: # ({/new-f219cf27})

[comment]: # ({new-2008672b})

##### oracle.sga.stats[connString,<user>,<password>,<service>]

<br>
Returns the System Global Area (SGA) statistics.<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the Oracle login credentials;<br>
-   **service** - the Oracle service name.<br>

[comment]: # ({/new-2008672b})

[comment]: # ({new-2a343448})

##### oracle.sessions.stats[connString,<user>,<password>,<service>,<lockMaxTime>]

<br>
Returns the sessions statistics.<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the Oracle login credentials;<br>
-   **service** - the Oracle service name;<br>
-   **lockMaxTime** - the maximum session lock duration in seconds to count the session as a prolongedly locked. Default: 600 seconds.

[comment]: # ({/new-2a343448})

[comment]: # ({new-585ccbd4})

##### oracle.sys.metrics[connString,<user>,<password>,<service>,<duration>]

<br>
Returns a set of system metric values.<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the Oracle login credentials;<br>
-   **service** - the Oracle service name;<br>
-   **duration** - the capturing interval (in seconds) of system metric values. Possible values: *60* — long duration (default), *15* — short duration.

[comment]: # ({/new-585ccbd4})

[comment]: # ({new-4ad20624})

##### oracle.sys.params[connString,<user>,<password>,<service>]

<br>
Returns a set of system parameter values.<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the Oracle login credentials;<br>
-   **service** - the Oracle service name.<br>

[comment]: # ({/new-4ad20624})

[comment]: # ({new-fd9b5b8e})

##### oracle.ts.stats[connString,<user>,<password>,<service>,<tablespace>,<type>]

<br>
Returns the tablespaces statistics.<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the Oracle login credentials;<br>
-   **service** - the Oracle service name;<br>
-   **tablespace** - name of the tablespace to query. Default (if left empty and `type` is set):<br>- "TEMP" (if `type` is set to "TEMPORARY");<br>- "USERS" (if `type` is set to "PERMANENT").
-   **type** - the type of the tablespace to query. Default (if `tablespace` is set): "PERMANENT".

[comment]: # ({/new-fd9b5b8e})

[comment]: # ({new-836285d4})

##### oracle.ts.discovery[connString,<user>,<password>,<service>]

<br>
Returns a list of tablespaces. Used for [low-level discovery](/manual/discovery/low_level_discovery/examples/systemd).<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the Oracle login credentials;<br>
-   **service** - the Oracle service name.

[comment]: # ({/new-836285d4})

[comment]: # ({new-213a30fa})

##### oracle.user.info[connString,<user>,<password>,<service>,<username>]

<br>
Returns Oracle user information.<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the Oracle login credentials;<br>
-   **service** - the Oracle service name;<br>
-   **username** - the username for which the information is needed. Lowercase usernames are not supported. Default: current user.

[comment]: # ({/new-213a30fa})

[comment]: # ({new-681f20c2})

##### pgsql.autovacuum.count[uri,<username>,<password>,<database name>]

<br>
The number of autovacuum workers.<br>
Return value: *Integer*.

Parameters:

-   **uri** - the URI or session name;<br>
-   **username, password** - the PostgreSQL credentials;<br>
-   **database name** - the database name.<br>

[comment]: # ({/new-681f20c2})

[comment]: # ({new-d00cb61c})

##### pgsql.archive[uri,<username>,<password>,<database name>]

<br>
The information about archived files.<br>
Return value: *JSON object*.

Parameters:

-   **uri** - the URI or session name;<br>
-   **username, password** - the PostgreSQL credentials;<br>
-   **database name** - the database name.<br>

The returned data are processed by dependent items:

-   **pgsql.archive.count_archived_files** - the number of WAL files that have been successfully archived;<br>
-   **pgsql.archive.failed_trying_to_archive** - the number of failed attempts for archiving WAL files;<br>
-   **pgsql.archive.count_files_to_archive** - the number of files to archive;<br>
-   **pgsql.archive.size_files_to_archive** - the size of files to archive.

[comment]: # ({/new-d00cb61c})

[comment]: # ({new-00011d59})

##### pgsql.bgwriter[uri,<username>,<password>,<database name>]

<br>
The combined number of checkpoints for the database cluster, broken down by checkpoint type.<br>
Return value: *JSON object*.

Parameters:

-   **uri** - the URI or session name;<br>
-   **username, password** - the PostgreSQL credentials;<br>
-   **database name** - the database name.<br>

The returned data are processed by dependent items:

-   **pgsql.bgwriter.buffers_alloc** - the number of buffers allocated;<br>
-   **pgsql.bgwriter.buffers_backend** - the number of buffers written directly by a backend;<br>
-   **pgsql.bgwriter.maxwritten_clean** - the number of times the background writer stopped a cleaning scan, because it had written too many buffers;<br>
-   **pgsql.bgwriter.buffers_backend_fsync** -the number of times a backend had to execute its own fsync call instead of the background writer;
-   **pgsql.bgwriter.buffers_clean** - the number of buffers written by the background writer;
-   **pgsql.bgwriter.buffers_checkpoint** - the number of buffers written during checkpoints;
-   **pgsql.bgwriter.checkpoints_timed** - the number of scheduled checkpoints that have been performed;
-   **pgsql.bgwriter.checkpoints_req** - the number of requested checkpoints that have been performed;
-   **pgsql.bgwriter.checkpoint_write_time** - the total amount of time spent in the portion of checkpoint processing where files are written to disk, in milliseconds;
-   **pgsql.bgwriter.sync_time** - the total amount of time spent in the portion of checkpoint processing where files are synchronized with disk.

[comment]: # ({/new-00011d59})

[comment]: # ({new-00ab276d})

##### pgsql.cache.hit[uri,<username>,<password>,<database name>]

<br>
The PostgreSQL buffer cache hit rate.<br>
Return value: *Float*.

Parameters:

-   **uri** - the URI or session name;<br>
-   **username, password** - the PostgreSQL credentials;<br>
-   **database name** - the database name.<br>

[comment]: # ({/new-00ab276d})

[comment]: # ({new-32909ece})

##### pgsql.connections[uri,<username>,<password>,<database name>]

<br>
Returns connections by type.<br>
Return value: *JSON object*.

Parameters:

-   **uri** - the URI or session name;<br>
-   **username, password** - the PostgreSQL credentials;<br>
-   **database name** - the database name.<br>

The returned data are processed by dependent items:

-   **pgsql.connections.active** - the backend is executing a query;<br>
-   **pgsql.connections.fastpath_function_call** - the backend is executing a fast-path function;<br>
-   **pgsql.connections.idle** - the backend is waiting for a new client command;<br>
-   **pgsql.connections.idle_in_transaction** - the backend is in a transaction, but is not currently executing a query;<br>
-   **pgsql.connections.prepared** - the number of prepared connections;<br>
-   **pgsql.connections.total** - the total number of connections;<br>
-   **pgsql.connections.total_pct** - percantange of total connections in respect to ‘max\_connections’ setting of the PostgreSQL server;<br>
-   **pgsql.connections.waiting** - number of connections in a query;<br>
-   **pgsql.connections.idle_in_transaction_aborted** - the backend is in a transaction, but is not currently executing a query and one of the statements in the transaction caused an error.

[comment]: # ({/new-32909ece})

[comment]: # ({new-21431ffe})

##### pgsql.custom.query[uri,<username>,<password>,queryName[,args...]]

<br>
Returns the result of a custom query.<br>
Return value: *JSON object*.

Parameters:

-   **uri** - the URI or session name;<br>
-   **username, password** - the PostgreSQL credentials;<br>
-   **queryName** - the name of a custom query, must match the SQL file name without an extension;<br>
-   **args**(optional) - the arguments to pass to the query.

[comment]: # ({/new-21431ffe})

[comment]: # ({new-11f6be08})

##### pgsql.db.age[uri,<username>,<password>,<database name>]

<br>
The age of the oldest FrozenXID of the database. Used for [low-level discovery](/manual/discovery/low_level_discovery/examples/systemd).<br>
Return value: *Integer*.

Parameters:

-   **uri** - the URI or session name;<br>
-   **username, password** - the PostgreSQL credentials;<br>
-   **database name** - the database name.<br>

[comment]: # ({/new-11f6be08})

[comment]: # ({new-ae9c9740})

##### pgsql.db.bloating_tables[uri,<username>,<password>,<database name>]

<br>
The number of bloating tables per database. Used for [low-level discovery](/manual/discovery/low_level_discovery/examples/systemd).<br>
Return value: *Integer*.

Parameters:

-   **uri** - the URI or session name;<br>
-   **username, password** - the PostgreSQL credentials;<br>
-   **database name** - the database name.<br>

[comment]: # ({/new-ae9c9740})

[comment]: # ({new-bead66f0})

##### pgsql.db.discovery[uri,<username>,<password>,<database name>]

<br>
The list of PostgreSQL databases. Used for [low-level discovery](/manual/discovery/low_level_discovery/examples/systemd).<br>
Return value: *JSON object*.

Parameters:

-   **uri** - the URI or session name;<br>
-   **username, password** - the PostgreSQL credentials;<br>
-   **database name** - the database name.<br>

[comment]: # ({/new-bead66f0})

[comment]: # ({new-a209d73f})

##### pgsql.db.size[uri,<username>,<password>,<database name>]

<br>
The database size in bytes. Used for [low-level discovery](/manual/discovery/low_level_discovery/examples/systemd).<br>
Return value: *Integer*.

Parameters:

-   **uri** - the URI or session name;<br>
-   **username, password** - the PostgreSQL credentials;<br>
-   **database name** - the database name.<br>

[comment]: # ({/new-a209d73f})

[comment]: # ({new-2a2e3c05})

##### pgsql.dbstat[uri,<username>,<password>,<database name>]

<br>
Collects the statistics per database. Used for [low-level discovery](/manual/discovery/low_level_discovery/examples/systemd).<br>
Return value: *JSON object*.

Parameters:

-   **uri** - the URI or session name;<br>
-   **username, password** - the PostgreSQL credentials;<br>
-   **database name** - the database name.<br>

The returned data are processed by dependent items:

-   **pgsql.dbstat.numbackends["{#DBNAME}"]** - the time spent reading data file blocks by backends in this database, in milliseconds;<br>
-   **pgsql.dbstat.sum.blk_read_time["{#DBNAME}"]** - the time spent reading data file blocks by backends in this database, in milliseconds;<br>
-   **pgsql.dbstat.sum.blk_write_time["{#DBNAME}"]** - the time spent writing data file blocks by backends in this database, in milliseconds;<br>
-   **pgsql.dbstat.sum.checksum_failures["{#DBNAME}"]** - the number of data page checksum failures detected (or on a shared object), or NULL if data checksums are not enabled (PostgreSQL version 12 only);<br>
-   **pgsql.dbstat.blks_read.rate["{#DBNAME}"]** - the number of disk blocks read in this database;<br>
-   **pgsql.dbstat.deadlocks.rate["{#DBNAME}"]** - the number of deadlocks detected in this database;<br>
-   **pgsql.dbstat.blks_hit.rate["{#DBNAME}"]** - the number of times disk blocks were found already in the buffer cache, so that a read was not necessary (this only includes hits in the PostgreSQL Pro buffer cache, not the operating system's file system cache);<br>
-   **pgsql.dbstat.xact_rollback.rate["{#DBNAME}"]** - the number of transactions in this database that have been rolled back;<br>
-   **pgsql.dbstat.xact_commit.rate["{#DBNAME}"]** - the number of transactions in this database that have been committed;<br>
-   **pgsql.dbstat.tup_updated.rate["{#DBNAME}"]** - the number of rows updated by queries in this database;<br>
-   **pgsql.dbstat.tup_returned.rate["{#DBNAME}"]** - the number of rows returned by queries in this database;<br>
-   **pgsql.dbstat.tup_inserted.rate["{#DBNAME}"]** - the number of rows inserted by queries in this database;<br>
-   **pgsql.dbstat.tup_fetched.rate["{#DBNAME}"]** - the number of rows fetched by queries in this database;<br>
-   **pgsql.dbstat.tup_deleted.rate["{#DBNAME}"]** - the number of rows deleted by queries in this database;<br>
-   **pgsql.dbstat.conflicts.rate["{#DBNAME}"]** - the number of queries canceled due to conflicts with recovery in this database (the conflicts occur only on standby servers);<br>
-   **pgsql.dbstat.temp_files.rate["{#DBNAME}"]** - the number of temporary files created by queries in this database. All temporary files are counted, regardless of the log_temp_files settings and reasons for which the temporary file was created (e.g., sorting or hashing);<br>
-   **pgsql.dbstat.temp_bytes.rate["{#DBNAME}"]** - the total amount of data written to temporary files by queries in this database. Includes data from all temporary files, regardless of the log_temp_files settings and reasons for which the temporary file was created (e.g., sorting or hashing).

[comment]: # ({/new-2a2e3c05})

[comment]: # ({new-96f0fa06})

##### pgsql.dbstat.sum[uri,<username>,<password>,<database name>]

<br>
The summarized data for all databases in a cluster.<br>
Return value: *JSON object*.

Parameters:

-   **uri** - the URI or session name;<br>
-   **username, password** - the PostgreSQL credentials;<br>
-   **database name** - the database name.<br>

The returned data are processed by dependent items:

-   **pgsql.dbstat.numbackends** - the number of backends currently connected to this database;<br>
-   **pgsql.dbstat.sum.blk\_read\_time** - time spent reading data file blocks by backends in this database, in milliseconds;<br>
-   **pgsql.dbstat.sum.blk\_write\_time** - time spent writing data file blocks by backends in this database, in milliseconds;<br>
-   **pgsql.dbstat.sum.checksum\_failures** - the number of data page checksum failures detected (or on a shared object), or NULL if data checksums are not enabled (PostgreSQL version 12 only);<br>
-   **pgsql.dbstat.sum.xact\_commit** - the number of transactions in this database that have been committed;<br>
-   **pgsql.dbstat.sum.conflicts** - database statistics about query cancels due to conflict with recovery on standby servers;<br>
-   **pgsql.dbstat.sum.deadlocks** - the number of deadlocks detected in this database;<br>
-   **pgsql.dbstat.sum.blks\_read** - the number of disk blocks read in this database;<br>
-   **pgsql.dbstat.sum.blks\_hit** - the number of times disk blocks were found already in the buffer cache, so a read was not necessary (only hits in the PostgreSQL Pro buffer cache are included);<br>
-   **pgsql.dbstat.sum.temp\_bytes** - the total amount of data written to temporary files by queries in this database. Includes data from all temporary files, regardless of the log\_temp\_files settings and reasons for which the temporary file was created (e.g., sorting or hashing);<br>
-   **pgsql.dbstat.sum.temp_files** - the number of temporary files created by queries in this database. All temporary files are counted, regardless of the log\_temp\_files settings and reasons for which the temporary file was created (e.g., sorting or hashing);<br>
-   **pgsql.dbstat.sum.xact_rollback** - the number of rolled-back transactions in this database;<br>
-   **pgsql.dbstat.sum.tup_deleted** - the number of rows deleted by queries in this database;<br>
-   **pgsql.dbstat.sum.tup_fetched** - the number of rows fetched by queries in this database;<br>
-   **pgsql.dbstat.sum.tup_inserted** - the number of rows inserted by queries in this database;<br>
-   **pgsql.dbstat.sum.tup_returned** - the number of rows returned by queries in this database;<br>
-   **pgsql.dbstat.sum.tup_updated** - the number of rows updated by queries in this database.

[comment]: # ({/new-96f0fa06})

[comment]: # ({new-14ca0aec})

##### pgsql.locks[uri,<username>,<password>,<database name>]

<br>
The information about granted locks per database. Used for [low-level discovery](/manual/discovery/low_level_discovery/examples/systemd).<br>
Return value: *JSON object*.

Parameters:

-   **uri** - the URI or session name;<br>
-   **username, password** - the PostgreSQL credentials;<br>
-   **database name** - the database name.<br>

The returned data are processed by dependent items:

-   **pgsql.locks.shareupdateexclusive["{#DBNAME}"]** - the number of share update exclusive locks;<br>
-   **pgsql.locks.accessexclusive["{#DBNAME}"]** - the number of access exclusive locks;<br>
-   **pgsql.locks.accessshare["{#DBNAME}"]** - the number of access share locks;<br>
-   **pgsql.locks.exclusive["{#DBNAME}"]** - the number of exclusive locks;<br>
-   **pgsql.locks.rowexclusive["{#DBNAME}"]** - the number of row exclusive locks;<br>
-   **pgsql.locks.rowshare["{#DBNAME}"]** - the number of row share locks;<br>
-   **pgsql.locks.share["{#DBNAME}"]** - the number of shared locks;<br>
-   **pgsql.locks.sharerowexclusive["{#DBNAME}"]** - the number of share row exclusive locks.

[comment]: # ({/new-14ca0aec})

[comment]: # ({new-f37282e1})

##### pgsql.oldest.xid[uri,<username>,<password>,<database name>]

<br>
The age of the oldest XID.<br>
Return value: *Integer*.

Parameters:

-   **uri** - the URI or session name;<br>
-   **username, password** - the PostgreSQL credentials;<br>
-   **database name** - the database name.<br>

[comment]: # ({/new-f37282e1})

[comment]: # ({new-a61feaf8})

##### pgsql.ping[uri,<username>,<password>,<database name>]

<br>
Tests whether a connection is alive or not.<br>
Return value: *1* - the connection is alive; *0* - the connection is broken (if there is any error presented including AUTH and configuration issues).

Parameters:

-   **uri** - the URI or session name;<br>
-   **username, password** - the PostgreSQL credentials;<br>
-   **database name** - the database name.<br>

[comment]: # ({/new-a61feaf8})

[comment]: # ({new-1240f49d})

##### pgsql.queries[uri,<username>,<password>,<database name>,<time period>]

<br>
Queries metrics by execution time.<br>
Return value: *JSON object*.

Parameters:

-   **uri** - the URI or session name;<br>
-   **username, password** - the PostgreSQL credentials;<br>
-   **database name** - the database name;<br>
-   **timePeriod** - the execution time limit for the count of slow queries (must be a positive integer).

The returned data are processed by dependent items:

-   **pgsql.queries.mro.time_max["{#DBNAME}"]** - the max maintenance query time;<br>
-   **pgsql.queries.query.time_max["{#DBNAME}"]** - the max query time;<br>
-   **pgsql.queries.tx.time_max["{#DBNAME}"]** - the max transaction query time;<br>
-   **pgsql.queries.mro.slow_count["{#DBNAME}"]** - the slow maintenance query count;<br>
-   **pgsql.queries.query.slow_count["{#DBNAME}"]** - the slow query count;<br>
-   **pgsql.queries.tx.slow_count["{#DBNAME}"]** - the slow transaction query count;<br>
-   **pgsql.queries.mro.time_sum["{#DBNAME}"]** - the sum maintenance query time;<br>
-   **pgsql.queries.query.time_sum["{#DBNAME}"]** - the sum query time;<br>
-   **pgsql.queries.tx.time_sum["{#DBNAME}"]** - the sum transaction query time.

[comment]: # ({/new-1240f49d})

[comment]: # ({new-7da2d7a1})

##### pgsql.replication.count[uri,<username>,<password>]

<br>
The number of standby servers.<br>
Return value: *Integer*.

Parameters:

-   **uri** - the URI or session name;<br>
-   **username, password** - the PostgreSQL credentials.

[comment]: # ({/new-7da2d7a1})

[comment]: # ({new-46c214a7})

##### pgsql.replication.process[uri,<username>,<password>]

<br>
The flush lag, write lag and replay lag per each sender process.<br>
Return value: *JSON object*.

Parameters:

-   **uri** - the URI or session name;<br>
-   **username, password** - the PostgreSQL credentials.

[comment]: # ({/new-46c214a7})

[comment]: # ({new-4e97798b})

##### pgsql.replication.process.discovery[uri,<username>,<password>]

<br>
The replication process name discovery.<br>
Return value: *JSON object*.

Parameters:

-   **uri** - the URI or session name;<br>
-   **username, password** - the PostgreSQL credentials.

[comment]: # ({/new-4e97798b})

[comment]: # ({new-2db43ebf})

##### pgsql.replication.recovery_role[uri,<username>,<password>]

<br>
The recovery status.<br>
Return value: *0* - master mode; *1* - recovery is still in progress (standby mode).

Parameters:

-   **uri** - the URI or session name;<br>
-   **username, password** - the PostgreSQL credentials.

[comment]: # ({/new-2db43ebf})

[comment]: # ({new-0b42de61})

##### pgsql.replication.status[uri,<username>,<password>]

<br>
The status of replication.<br>
Return value: *0* - streaming is down; *1* - streaming is up; *2* - master mode.

Parameters:

-   **uri** - the URI or session name;<br>
-   **username, password** - the PostgreSQL credentials.

[comment]: # ({/new-0b42de61})

[comment]: # ({new-dce31b13})

##### pgsql.replication_lag.b[uri,<username>,<password>]

<br>
The replication lag in bytes.<br>
Return value: *Integer*.

Parameters:

-   **uri** - the URI or session name;<br>
-   **username, password** - the PostgreSQL credentials.

[comment]: # ({/new-dce31b13})

[comment]: # ({new-911ce6f3})

##### pgsql.replication_lag.sec[uri,<username>,<password>]

<br>
The replication lag in seconds.<br>
Return value: *Integer*.

Parameters:

-   **uri** - the URI or session name;<br>
-   **username, password** - the PostgreSQL credentials.

[comment]: # ({/new-911ce6f3})

[comment]: # ({new-a81f90e6})

##### pgsql.uptime[uri,<username>,<password>,<database name>]

<br>
The PostgreSQL uptime in milliseconds.<br>
Return value: *Float*.

Parameters:

-   **uri** - the URI or session name;<br>
-   **username, password** - the PostgreSQL credentials;<br>
-   **database name** - the database name.<br>

[comment]: # ({/new-a81f90e6})

[comment]: # ({new-3a4ac3ad})

##### pgsql.wal.stat[uri,<username>,<password>,<database name>]

<br>
The WAL statistics.<br>
Return value: *JSON object*.

Parameters:

-   **uri** - the URI or session name;<br>
-   **username, password** - the PostgreSQL credentials;<br>
-   **database name** - the database name.<br>

The returned data are processed by dependent items:

-   **pgsql.wal.count** — the number of WAL files;<br>
-   **pgsql.wal.write** - the WAL lsn used (in bytes).

[comment]: # ({/new-3a4ac3ad})

[comment]: # ({new-fb03aca5})

##### redis.config[connString,<password>,<pattern>]

<br>
Gets the configuration parameters of a Redis instance that match the pattern.<br>
Return value: *JSON* - if a glob-style pattern was used; single value - if a pattern did not contain any wildcard character.

Parameters:

-   **connString** - the URI or session name;<br>
-   **password** - the Redis password;<br>
-   **pattern** - a glob-style pattern (*\** by default).

[comment]: # ({/new-fb03aca5})

[comment]: # ({new-8c5408f6})

##### redis.info[connString,<password>,<section>]

<br>
Gets the output of the INFO command.<br>
Return value: *JSON* - the output is serialized as JSON.

Parameters:

-   **connString** - the URI or session name;<br>
-   **password** - the Redis password;<br>
-   **section** - the [section](https://redis.io/commands/info) of information (*default* by default).<br>

[comment]: # ({/new-8c5408f6})

[comment]: # ({new-7952fb43})

##### redis.ping[connString,<password>]

<br>
Test if a connection is alive or not.<br>
Return value: *1* - the connection is alive; *0* - the connection is broken (if there is any error presented including AUTH and configuration issues).

Parameters:

-   **connString** - the URI or session name;<br>
-   **password** - the Redis password.<br>

[comment]: # ({/new-7952fb43})

[comment]: # ({new-e6d86f50})

##### redis.slowlog.count[connString,<password>]

<br>
The number of slow log entries since Redis was started.<br>
Return value: *Integer*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **password** - the Redis password.<br>

[comment]: # ({/new-e6d86f50})

[comment]: # ({new-74faa2be})

##### smart.attribute.discovery

<br>
Returns a list of S.M.A.R.T. device attributes.<br>
Return value: *JSON object*.

Comments:

-   The following macros and their values are returned: {\#NAME}, {\#DISKTYPE}, {\#ID}, {\#ATTRNAME}, {\#THRESH};
-   HDD, SSD and NVME drive types are supported. Drives can be alone or combined in a RAID. {\#NAME} will have an add-on in case of RAID, e.g: {"{\#NAME}": "/dev/sda cciss,2"}.

[comment]: # ({/new-74faa2be})

[comment]: # ({new-c0ca5d9d})

##### smart.disk.discovery

<br>
Returns a list of S.M.A.R.T. devices.<br>
Return value: *JSON object*.

Comments:

-   The following macros and their values are returned: {\#NAME}, {\#DISKTYPE}, {\#MODEL}, {\#SN}, {\#PATH}, {\#ATTRIBUTES}, {\#RAIDTYPE};
-   HDD, SSD and NVME drive types are supported. If a drive does not belong to a RAID, the {\#RAIDTYPE} will be empty. {\#NAME} will have an add-on in case of RAID, e.g: {"{\#NAME}": "/dev/sda cciss,2"}.

[comment]: # ({/new-c0ca5d9d})

[comment]: # ({new-85fda450})

##### smart.disk.get[<path>,<raid type>]

<br>
Returns all available properties of S.M.A.R.T. devices.<br>
Return value: *JSON object*.

Parameters:

-   **path** - the disk path, the {\#PATH} macro may be used as a value;<br>
-   **raid_type** - the RAID type, the {\#RAID} macro may be used as a value

Comments:

-   HDD, SSD and NVME drive types are supported. Drives can be alone or combined in a RAID;<br>
-   The data includes smartctl version and call arguments, and additional fields:<br>*disk\_name* - holds the name with the required add-ons for RAID discovery, e.g: {"disk\_name": "/dev/sda cciss,2"}<br>*disk\_type* - holds the disk type HDD, SSD, or NVME, e.g: {"disk\_type": "ssd"};<br>
-   If no parameters are specified, the item will return information about all disks.

[comment]: # ({/new-85fda450})

[comment]: # ({new-a2e1203a})

##### systemd.unit.get[unit name,<interface>]

<br>
Returns all properties of a systemd unit.<br>
Return value: *JSON object*.

Parameters:

-   **unit name** - the unit name (you may want to use the {\#UNIT.NAME} macro in item prototype to discover the name);<br>
-   **interface** - the unit interface type, possible values: *Unit* (default), *Service*, *Socket*, *Device*, *Mount*, *Automount*, *Swap*, *Target*, *Path*.

Comments:

-   This item is supported on Linux platform only;
-   LoadState, ActiveState and UnitFileState for Unit interface are returned as text and integer: `"ActiveState":{"state":1,"text":"active"}`.

[comment]: # ({/new-a2e1203a})

[comment]: # ({new-04d19e13})

##### systemd.unit.info[unit name,<property>,<interface>]

<br>
A systemd unit information.<br>
Return value: *String*.

Parameters:

-   **unit name** - the unit name (you may want to use the {\#UNIT.NAME} macro in item prototype to discover the name);<br>
-   **property** - unit property (e.g. ActiveState (default), LoadState, Description);
-   **interface** - the unit interface type (e.g. Unit (default), Socket, Service).

Comments:

-   This item is supported on Linux platform only;
-   This item allows to retrieve a specific property from specific type of interface as described in [dbus API](https://www.freedesktop.org/wiki/Software/systemd/dbus/).

Examples:

    systemd.unit.info["{#UNIT.NAME}"] #collect active state (active, reloading, inactive, failed, activating, deactivating) info on discovered systemd units
    systemd.unit.info["{#UNIT.NAME}",LoadState] #collect load state info on discovered systemd units
    systemd.unit.info[mysqld.service,Id] #retrieve the service technical name (mysqld.service)
    systemd.unit.info[mysqld.service,Description] #retrieve the service description (MySQL Server)
    systemd.unit.info[mysqld.service,ActiveEnterTimestamp] #retrieve the last time the service entered the active state (1562565036283903)
    systemd.unit.info[dbus.socket,NConnections,Socket] #collect the number of connections from this socket unit

[comment]: # ({/new-04d19e13})

[comment]: # ({new-28908462})

##### systemd.unit.discovery[<type>]

<br>
List of systemd units and their details. Used for [low-level discovery](/manual/discovery/low_level_discovery/examples/systemd).<br>
Return value: *JSON object*.

Parameter:

-   **type** - possible values: *all*, *automount*, *device*, *mount*, *path*, *service* (default), *socket*, *swap*, *target*.

This item is supported on Linux platform only.

[comment]: # ({/new-28908462})

[comment]: # ({new-9a731445})

##### web.certificate.get[hostname,<port>,<address>]

<br>
Validates the certificates and returns certificate details.<br>
Return value: *JSON object*.

Parameter:

-   **hostname** - can be either IP or DNS.<br>May contain the URL scheme (*https* only), path (it will be ignored), and port.<br>If a port is provided in both the first and the second parameters, their values must match.<br>If address (the 3rd parameter) is specified, the hostname is only used for SNI and hostname verification;<br>
-   **port** - the port number (default is 443 for HTTPS);<br>
-   **address** - can be either IP or DNS. If specified, it will be used for connection, and hostname (the 1st parameter) will be used for SNI, and host verification. In case, the 1st parameter is an IP and the 3rd parameter is DNS, the 1st parameter will be used for connection, and the 3rd parameter will be used for SNI and host verification.

Comments:

-   This item turns unsupported if the resource specified in `host` does not exist or is unavailable or if TLS handshake fails with any error except an invalid certificate;<br>
-   Currently, AIA (Authority Information Access) X.509 extension, CRLs and OCSP (including OCSP stapling), Certificate Transparency, and custom CA trust store are not supported.

[comment]: # ({/new-9a731445})












