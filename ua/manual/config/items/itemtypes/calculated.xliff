<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="ua" datatype="plaintext" original="manual/config/items/itemtypes/calculated.md">
    <body>
      <trans-unit id="b8c65ad1" xml:space="preserve">
        <source># 7 Calculated items</source>
      </trans-unit>
      <trans-unit id="43ebe58c" xml:space="preserve">
        <source>#### Overview

A calculated item allows to create a calculation based on the
values of some existing items. For example, you may want to 
calculate the hourly average of some item value or to calculate 
the total value for a group of items. That is what a calculated 
item is for.

Calculations may use both:

-   single values of individual items
-   a complex filter to select multiple items for aggregation (see
    [aggregate calculations](/manual/config/items/itemtypes/calculated/aggregate)
    for details)

Calculated items are a way of creating virtual data sources. All
calculations are done by Zabbix server only. The values are periodically
calculated based on the arithmetical expression used.

The resulting data is stored in the Zabbix database as for any other
item; both history and trend values are stored and graphs can be
generated.

::: noteclassic
If the calculation result is a float value it will be
trimmed to an integer if the calculated item type of information is
*Numeric (unsigned)*.
:::

Calculated items share their syntax with trigger
[expressions](/manual/config/triggers/expression). Comparison to strings
is allowed in calculated items. Calculated items may be referenced by
macros or other entities same as any other item type.

To use calculated items, choose the item type **Calculated**.</source>
      </trans-unit>
      <trans-unit id="ddc35784" xml:space="preserve">
        <source>#### Configurable fields

The **key** is a unique item identifier (per host). You can create any
key name using supported symbols.

The calculation definition should be entered in the **Formula** field. There
is no connection between the formula and the key. The key parameters 
are not used in the formula in any way.

The syntax of a simple formula is:

    function(/host/key,&lt;parameter1&gt;,&lt;parameter2&gt;,...)

where:

|   |   |
|--|--------|
|*function*|One of the [supported functions](/manual/appendix/functions): last, min, max, avg, count, etc|
|*host*|Host of the item that is used for calculation.&lt;br&gt;The current host can be omitted (i.e. as in `function(//key,parameter,...)`).|
|*key*|Key of the item that is used for calculation.|
|*parameter(s)*|Parameters of the function, if required.|

::: noteimportant
[User macros](/manual/config/macros/user_macros)
in the formula will be expanded if used to reference a function
parameter, item filter parameter, or a constant. User macros will 
NOT be expanded if referencing a function, host name, item key, 
item key parameter or operator.
:::

A more complex formula may use a combination of functions, operators and
brackets. You can use all functions and [operators](/manual/config/triggers/expression#operators) supported in trigger expressions. The logic and operator 
precedence are exactly the same.

Unlike trigger expressions, Zabbix processes calculated items according
to the item update interval, not upon receiving a new value.

All items that are referenced by history functions in the calculated
item formula must exist and be collecting data. Also, if you change the
item key of a referenced item, you have to manually update any formulas
using that key.

A calculated item may become unsupported in several cases:

-   referenced item(s)
    -   is not found
    -   is disabled
    -   belongs to a disabled host
    -   is not supported (except with nodata() function and
        [operators](/manual/config/triggers/expression#operators) with
        unknown values)
-   no data to calculate a function
-   division by zero
-   incorrect syntax used</source>
      </trans-unit>
      <trans-unit id="82c22f4f" xml:space="preserve">
        <source>#### Usage examples</source>
      </trans-unit>
      <trans-unit id="426ab71a" xml:space="preserve">
        <source>##### Example 1

Calculating percentage of free disk space on '/'.

Use of function **last**:

    100*last(//vfs.fs.size[/,free])/last(//vfs.fs.size[/,total])

Zabbix will take the latest values for free and total disk spaces and
calculate percentage according to the given formula.</source>
      </trans-unit>
      <trans-unit id="77bcdfd9" xml:space="preserve">
        <source>##### Example 2

Calculating a 10-minute average of the number of values processed by
Zabbix.

Use of function **avg**:

    avg(/Zabbix Server/zabbix[wcache,values],10m)

Note that extensive use of calculated items with long time periods may
affect performance of Zabbix server.</source>
      </trans-unit>
      <trans-unit id="5d068733" xml:space="preserve">
        <source>##### Example 3

Calculating total bandwidth on eth0.

Sum of two functions:

    last(//net.if.in[eth0,bytes])+last(//net.if.out[eth0,bytes])</source>
      </trans-unit>
      <trans-unit id="f8b53af1" xml:space="preserve">
        <source>##### Example 4

Calculating percentage of incoming traffic.

More complex expression:

    100*last(//net.if.in[eth0,bytes])/(last(//net.if.in[eth0,bytes])+last(//net.if.out[eth0,bytes]))

See also: [Examples of aggregate
calculations](/manual/config/items/itemtypes/calculated/aggregate#usage_examples)</source>
      </trans-unit>
    </body>
  </file>
</xliff>
