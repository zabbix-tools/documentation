[comment]: # translation:outdated

[comment]: # ({new-5479dd79})
# VMware monitoring item keys

[comment]: # ({/new-5479dd79})

[comment]: # ({new-98337d09})
#### Item keys

The table provides details on the simple checks that can be used to
monitor [VMware environments](/manual/vm_monitoring).

|Key|<|<|<|<|
|---|-|-|-|-|
|<|Description|Return value|Parameters|Comments|
|vmware.cl.perfcounter\[<url>,<id>,<path>,<instance>\]|<|<|<|<|
|<|VMware cluster performance counter metrics.|Integer|**url** - VMware service URL<br>**id** - VMware cluster ID<br>**path** - performance counter path<br>**instance** - performance counter instance|`id` can be received from vmware.cluster.discovery\[\] as {\#CLUSTER.ID}|
|vmware.cluster.discovery\[<url>\]|<|<|<|<|
|<|Discovery of VMware clusters.|JSON object|**url** - VMware service URL|<|
|vmware.cluster.status\[<url>, <name>\]|<|<|<|<|
|<|VMware cluster status.|Integer:<br>0 - gray;<br>1 - green;<br>2 - yellow;<br>3 - red|**url** - VMware service URL<br>**name** - VMware cluster name|<|
|vmware.datastore.discovery\[<url>\]|<|<|<|<|
|<|Discovery of VMware datastores.|JSON object|**url** - VMware service URL|<|
|vmware.datastore.hv.list\[<url>,<datastore>\]|<|<|<|<|
|<|List of datastore hypervisors.|JSON object|**url** - VMware service URL<br>**datastore** - datastore name|<|
|vmware.datastore.read\[<url>,<datastore>,<mode>\]|<|<|<|<|
|<|Amount of time for a read operation from the datastore (milliseconds).|Integer ^**[2](vmware_keys#footnotes)**^|**url** - VMware service URL<br>**datastore** - datastore name<br>**mode** - latency (average value, default), maxlatency (maximum value)|<|
|vmware.datastore.size\[<url>,<datastore>,<mode>\]|<|<|<|<|
|<|VMware datastore space in bytes or in percentage from total.|Integer - for bytes<br>Float - for percentage|**url** - VMware service URL<br>**datastore** - datastore name<br>**mode** - possible values:<br>total (default), free, pfree (free, percentage), uncommitted|<|
|vmware.datastore.write\[<url>,<datastore>,<mode>\]|<|<|<|<|
|<|Amount of time for a write operation to the datastore (milliseconds).|Integer ^**[2](vmware_keys#footnotes)**^|**url** - VMware service URL<br>**datastore** - datastore name<br>**mode** - latency (average value, default), maxlatency (maximum value)|<|
|vmware.dc.discovery\[<url>\]|<|<|<|<|
|<|Discovery of VMware datacenters.|JSON object|**url** - VMware service URL|<|
|vmware.eventlog\[<url>,<mode>\]|<|<|<|<|
|<|VMware event log.|Log|**url** - VMware service URL<br>**mode** - *all* (default), *skip* - skip processing of older data|There must be only one vmware.eventlog\[\] item key per URL.<br><br>See also: [example of filtering](/manual/config/items/preprocessing/examples#filtering_vmware_event_log_records) VMware event log records.|
|vmware.fullname\[<url>\]|<|<|<|<|
|<|VMware service full name.|String|**url** - VMware service URL|<|
|vmware.hv.cluster.name\[<url>,<uuid>\]|<|<|<|<|
|<|VMware hypervisor cluster name.|String|**url** - VMware service URL<br>**uuid** - VMware hypervisor host name|<|
|vmware.hv.cpu.usage\[<url>,<uuid>\]|<|<|<|<|
|<|VMware hypervisor processor usage (Hz).|Integer|**url** - VMware service URL<br>**uuid** - VMware hypervisor host name|<|
|vmware.hv.cpu.usage.perf\[<url>,<uuid>\]|<|<|<|<|
|<|VMware hypervisor processor usage as a percentage during the interval.|Float|**url** - VMware service URL<br>**uuid** - VMware hypervisor host name|<|
|vmware.hv.cpu.utilization\[<url>,<uuid>\]|<|<|<|<|
|<|VMware hypervisor processor usage as a percentage during the interval, depends on power management or HT.|Float|**url** - VMware service URL<br>**uuid** - VMware hypervisor host name|<|
|vmware.hv.datacenter.name\[<url>,<uuid>\]|<|<|<|<|
|<|VMware hypervisor datacenter name.|String|**url** - VMware service URL<br>**uuid** - VMware hypervisor host name|<|
|vmware.hv.datastore.discovery\[<url>,<uuid>\]|<|<|<|<|
|<|Discovery of VMware hypervisor datastores.|JSON object|**url** - VMware service URL<br>**uuid** - VMware hypervisor host name|<|
|vmware.hv.datastore.list\[<url>,<uuid>\]|<|<|<|<|
|<|List of VMware hypervisor datastores.|JSON object|**url** - VMware service URL<br>**uuid** - VMware hypervisor host name|<|
|vmware.hv.datastore.multipath\[<url>,<uuid>,<datastore>,<partitionid>\]|<|<|<|<|
|<|Number of available datastore paths.|Integer|**url** - VMware service URL<br>**uuid** - VMware hypervisor host name<br>**datastore** - datastore name<br>**partitionid** - internal ID of physical device from vmware.hv.datastore.discovery|<|
|vmware.hv.datastore.read\[<url>,<uuid>,<datastore>,<mode>\]|<|<|<|<|
|<|Average amount of time for a read operation from the datastore (milliseconds).|Integer ^**[2](vmware_keys#footnotes)**^|**url** - VMware service URL<br>**uuid** - VMware hypervisor host name<br>**datastore** - datastore name<br>**mode** - latency (default)|<|
|vmware.hv.datastore.size\[<url>,<uuid>,<datastore>,<mode>\]|<|<|<|<|
|<|VMware datastore space in bytes or in percentage from total.|Integer - for bytes<br>Float - for percentage|**url** - VMware service URL<br>**uuid** - VMware hypervisor host name<br>**datastore** - datastore name<br>**mode** - possible values:<br>total (default), free, pfree (free, percentage), uncommitted|<|
|vmware.hv.datastore.write\[<url>,<uuid>,<datastore>,<mode>\]|<|<|<|<|
|<|Average amount of time for a write operation to the datastore (milliseconds).|Integer ^**[2](vmware_keys#footnotes)**^|**url** - VMware service URL<br>**uuid** - VMware hypervisor host name<br>**datastore** - datastore name<br>**mode** - latency (default)|<|
|vmware.hv.discovery\[<url>\]|<|<|<|<|
|<|Discovery of VMware hypervisors.|JSON object|**url** - VMware service URL|<|
|vmware.hv.fullname\[<url>,<uuid>\]|<|<|<|<|
|<|VMware hypervisor name.|String|**url** - VMware service URL<br>**uuid** - VMware hypervisor host name|<|
|vmware.hv.hw.cpu.freq\[<url>,<uuid>\]|<|<|<|<|
|<|VMware hypervisor processor frequency (Hz).|Integer|**url** - VMware service URL<br>**uuid** - VMware hypervisor host name|<|
|vmware.hv.hw.cpu.model\[<url>,<uuid>\]|<|<|<|<|
|<|VMware hypervisor processor model.|String|**url** - VMware service URL<br>**uuid** - VMware hypervisor host name|<|
|vmware.hv.hw.cpu.num\[<url>,<uuid>\]|<|<|<|<|
|<|Number of processor cores on VMware hypervisor.|Integer|**url** - VMware service URL<br>**uuid** - VMware hypervisor host name|<|
|vmware.hv.hw.cpu.threads\[<url>,<uuid>\]|<|<|<|<|
|<|Number of processor threads on VMware hypervisor.|Integer|**url** - VMware service URL<br>**uuid** - VMware hypervisor host name|<|
|vmware.hv.hw.memory\[<url>,<uuid>\]|<|<|<|<|
|<|VMware hypervisor total memory size (bytes).|Integer|**url** - VMware service URL<br>**uuid** - VMware hypervisor host name|<|
|vmware.hv.hw.model\[<url>,<uuid>\]|<|<|<|<|
|<|VMware hypervisor model.|String|**url** - VMware service URL<br>**uuid** - VMware hypervisor host name|<|
|vmware.hv.hw.uuid\[<url>,<uuid>\]|<|<|<|<|
|<|VMware hypervisor BIOS UUID.|String|**url** - VMware service URL<br>**uuid** - VMware hypervisor host name|<|
|vmware.hv.hw.vendor\[<url>,<uuid>\]|<|<|<|<|
|<|VMware hypervisor vendor name.|String|**url** - VMware service URL<br>**uuid** - VMware hypervisor host name|<|
|vmware.hv.maintenance\[<url>,<uuid>\]|<|<|<|<|
|<|VMware hypervisor maintenance status.|Integer|**url** - VMware service URL<br>**uuid** - VMware hypervisor host name|Returns '0' - not in maintenance or '1' - in maintenance|
|vmware.hv.memory.size.ballooned\[<url>,<uuid>\]|<|<|<|<|
|<|VMware hypervisor ballooned memory size (bytes).|Integer|**url** - VMware service URL<br>**uuid** - VMware hypervisor host name|<|
|vmware.hv.memory.used\[<url>,<uuid>\]|<|<|<|<|
|<|VMware hypervisor used memory size (bytes).|Integer|**url** - VMware service URL<br>**uuid** - VMware hypervisor host name|<|
|vmware.hv.network.in\[<url>,<uuid>,<mode>\]|<|<|<|<|
|<|VMware hypervisor network input statistics (bytes per second).|Integer ^**[2](vmware_keys#footnotes)**^|**url** - VMware service URL<br>**uuid** - VMware hypervisor host name<br>**mode** - bps (default)|<|
|vmware.hv.network.out\[<url>,<uuid>,<mode>\]|<|<|<|<|
|<|VMware hypervisor network output statistics (bytes per second).|Integer ^**[2](vmware_keys#footnotes)**^|**url** - VMware service URL<br>**uuid** - VMware hypervisor host name<br>**mode** - bps (default)|<|
|vmware.hv.perfcounter\[<url>,<uuid>,<path>,<instance>\]|<|<|<|<|
|<|VMware hypervisor performance counter value.|Integer ^**[2](vmware_keys#footnotes)**^|**url** - VMware service URL<br>**uuid** - VMware hypervisor host name<br>**path** - performance counter path ^**[1](vmware_keys#footnotes)**^<br>**instance** - performance counter instance. Use empty instance for aggregate values (default)|<|
|vmware.hv.power\[<url>,<uuid>,<max>\]|<|<|<|<|
|<|VMware hypervisor power usage (W).|Integer|**url** - VMware service URL<br>**uuid** - VMware hypervisor host name<br>**max** - maximum allowed power usage|<|
|vmware.hv.sensor.health.state\[<url>,<uuid>\]|<|<|<|<|
|<|VMware hypervisor health state rollup sensor.|Integer:<br>0 - gray;<br>1 - green;<br>2 - yellow;<br>3 - red|**url** - VMware service URL<br>**uuid** - VMware hypervisor host name|<|
|vmware.hv.sensors.get\[<url>,<uuid>\]|<|<|<|<|
|<|VMware hypervisor HW vendor state sensors.|JSON|**url** - VMware service URL<br>**uuid** - VMware hypervisor host name|<|
|vmware.hv.status\[<url>,<uuid>\]|<|<|<|<|
|<|VMware hypervisor status.|Integer:<br>0 - gray;<br>1 - green;<br>2 - yellow;<br>3 - red|**url** - VMware service URL<br>**uuid** - VMware hypervisor host name|Uses the host system overall status property.|
|vmware.hv.uptime\[<url>,<uuid>\]|<|<|<|<|
|<|VMware hypervisor uptime (seconds).|Integer|**url** - VMware service URL<br>**uuid** - VMware hypervisor host name|<|
|vmware.hv.version\[<url>,<uuid>\]|<|<|<|<|
|<|VMware hypervisor version.|String|**url** - VMware service URL<br>**uuid** - VMware hypervisor host name|<|
|vmware.hv.vm.num\[<url>,<uuid>\]|<|<|<|<|
|<|Number of virtual machines on VMware hypervisor.|Integer|**url** - VMware service URL<br>**uuid** - VMware hypervisor host name|<|
|vmware.version\[<url>\]|<|<|<|<|
|<|VMware service version.|String|**url** - VMware service URL|<|
|vmware.vm.cluster.name\[<url>,<uuid>\]|<|<|<|<|
|<|VMware virtual machine name.|String|**url** - VMware service URL<br>**uuid** - VMware virtual machine host name|<|
|vmware.vm.cpu.latency\[<url>,<uuid>\]|<|<|<|<|
|<|Percentage of time the virtual machine is unable to run because it is contending for access to the physical CPU(s).|Float|**url** - VMware service URL<br>**uuid** - VMware virtual machine host name|<|
|vmware.vm.cpu.num\[<url>,<uuid>\]|<|<|<|<|
|<|Number of processors on VMware virtual machine.|Integer|**url** - VMware service URL<br>**uuid** - VMware virtual machine host name|<|
|vmware.vm.cpu.readiness\[<url>,<uuid>,<instance>\]|<|<|<|<|
|<|Percentage of time that the virtual machine was ready, but could not get scheduled to run on the physical CPU.|Float|**url** - VMware service URL<br>**uuid** - VMware virtual machine host name<br>**instance** - CPU instance|<|
|vmware.vm.cpu.ready\[<url>,<uuid>\]|<|<|<|<|
|<|Time (in milliseconds) that the virtual machine was ready, but could not get scheduled to run on the physical CPU. CPU ready time is dependent on the number of virtual machines on the host and their CPU loads (%).|Integer ^**[2](vmware_keys#footnotes)**^|**url** - VMware service URL<br>**uuid** - VMware virtual machine host name|<|
|vmware.vm.cpu.swapwait\[<url>,<uuid>,<instance>\]|<|<|<|<|
|<|Percentage of CPU time spent waiting for swap-in.|Float|**url** - VMware service URL<br>**uuid** - VMware virtual machine host name<br>**instance** - CPU instance|<|
|vmware.vm.cpu.usage\[<url>,<uuid>\]|<|<|<|<|
|<|VMware virtual machine processor usage (Hz).|Integer|**url** - VMware service URL<br>**uuid** - VMware virtual machine host name|<|
|vmware.vm.cpu.usage.perf\[<url>,<uuid>\]|<|<|<|<|
|<|VMware virtual machine processor usage as a percentage during the interval.|Float|**url** - VMware service URL<br>**uuid** - VMware virtual machine host name|<|
|vmware.vm.datacenter.name\[<url>,<uuid>\]|<|<|<|<|
|<|VMware virtual machine datacenter name.|String|**url** - VMware service URL<br>**uuid** - VMware virtual machine host name|<|
|vmware.vm.discovery\[<url>\]|<|<|<|<|
|<|Discovery of VMware virtual machines.|JSON object|**url** - VMware service URL|<|
|vmware.vm.guest.memory.size.swapped\[<url>,<uuid>\]|<|<|<|<|
|<|Amount of guest physical memory that is swapped out to the swap space (KB).|Integer|**url** - VMware service URL<br>**uuid** - VMware virtual machine host name|<|
|vmware.vm.guest.osuptime\[<url>,<uuid>\]|<|<|<|<|
|<|Total time elapsed since the last operating system boot-up (in seconds).|Integer|**url** - VMware service URL<br>**uuid** - VMware virtual machine host name|<|
|vmware.vm.hv.name\[<url>,<uuid>\]|<|<|<|<|
|<|VMware virtual machine hypervisor name.|String|**url** - VMware service URL<br>**uuid** - VMware virtual machine host name|<|
|vmware.vm.memory.size\[<url>,<uuid>\]|<|<|<|<|
|<|VMware virtual machine total memory size (bytes).|Integer|**url** - VMware service URL<br>**uuid** - VMware virtual machine host name|<|
|vmware.vm.memory.size.ballooned\[<url>,<uuid>\]|<|<|<|<|
|<|VMware virtual machine ballooned memory size (bytes).|Integer|**url** - VMware service URL<br>**uuid** - VMware virtual machine host name|<|
|vmware.vm.memory.size.compressed\[<url>,<uuid>\]|<|<|<|<|
|<|VMware virtual machine compressed memory size (bytes).|Integer|**url** - VMware service URL<br>**uuid** - VMware virtual machine host name|<|
|vmware.vm.memory.size.consumed\[<url>,<uuid>\]|<|<|<|<|
|<|Amount of host physical memory consumed for backing up guest physical memory pages (KB).|Integer|**url** - VMware service URL<br>**uuid** - VMware virtual machine host name|<|
|vmware.vm.memory.size.private\[<url>,<uuid>\]|<|<|<|<|
|<|VMware virtual machine private memory size (bytes).|Integer|**url** - VMware service URL<br>**uuid** - VMware virtual machine host name|<|
|vmware.vm.memory.size.shared\[<url>,<uuid>\]|<|<|<|<|
|<|VMware virtual machine shared memory size (bytes).|Integer|**url** - VMware service URL<br>**uuid** - VMware virtual machine host name|<|
|vmware.vm.memory.size.swapped\[<url>,<uuid>\]|<|<|<|<|
|<|VMware virtual machine swapped memory size (bytes).|Integer|**url** - VMware service URL<br>**uuid** - VMware virtual machine host name|<|
|vmware.vm.memory.size.usage.guest\[<url>,<uuid>\]|<|<|<|<|
|<|VMware virtual machine guest memory usage (bytes).|Integer|**url** - VMware service URL<br>**uuid** - VMware virtual machine host name|<|
|vmware.vm.memory.size.usage.host\[<url>,<uuid>\]|<|<|<|<|
|<|VMware virtual machine host memory usage (bytes).|Integer|**url** - VMware service URL<br>**uuid** - VMware virtual machine host name|<|
|vmware.vm.memory.usage\[<url>,<uuid>\]|<|<|<|<|
|<|Percentage of host physical memory that has been consumed.|Float|**url** - VMware service URL<br>**uuid** - VMware virtual machine host name|<|
|vmware.vm.net.if.discovery\[<url>,<uuid>\]|<|<|<|<|
|<|Discovery of VMware virtual machine network interfaces.|JSON object|**url** - VMware service URL<br>**uuid** - VMware virtual machine host name|<|
|vmware.vm.net.if.in\[<url>,<uuid>,<instance>,<mode>\]|<|<|<|<|
|<|VMware virtual machine network interface input statistics (bytes/packets per second).|Integer ^**[2](vmware_keys#footnotes)**^|**url** - VMware service URL<br>**uuid** - VMware virtual machine host name<br>**instance** - network interface instance<br>**mode** - bps (default)/pps - bytes/packets per second|<|
|vmware.vm.net.if.out\[<url>,<uuid>,<instance>,<mode>\]|<|<|<|<|
|<|VMware virtual machine network interface output statistics (bytes/packets per second).|Integer ^**[2](vmware_keys#footnotes)**^|**url** - VMware service URL<br>**uuid** - VMware virtual machine host name<br>**instance** - network interface instance<br>**mode** - bps (default)/pps - bytes/packets per second|<|
|vmware.vm.net.if.usage\[<url>,<uuid>,<instance>\]|<|<|<|<|
|<|VMware virtual machine network utilization (combined transmit-rates and receive-rates) during the interval (KBps).|Integer|**url** - VMware service URL<br>**uuid** - VMware virtual machine host name<br>**instance** - network interface instance|<|
|vmware.vm.perfcounter\[<url>,<uuid>,<path>,<instance>\]|<|<|<|<|
|<|VMware virtual machine performance counter value.|Integer ^**[2](vmware_keys#footnotes)**^|**url** - VMware service URL<br>**uuid** - VMware virtual machine host name<br>**path** - performance counter path ^**[1](vmware_keys#footnotes)**^<br>**instance** - performance counter instance. Use empty instance for aggregate values (default)|<|
|vmware.vm.powerstate\[<url>,<uuid>\]|<|<|<|<|
|<|VMware virtual machine power state.|Integer:<br>0 - poweredOff;<br>1 - poweredOn;<br>2 - suspended|**url** - VMware service URL<br>**uuid** - VMware virtual machine host name|<|
|vmware.vm.storage.committed\[<url>,<uuid>\]|<|<|<|<|
|<|VMware virtual machine committed storage space (bytes).|Integer|**url** - VMware service URL<br>**uuid** - VMware virtual machine host name|<|
|vmware.vm.storage.readoio\[<url>,<uuid>,<instance>\]|<|<|<|<|
|<|Average number of outstanding read requests to the virtual disk during the collection interval.|Integer|**url** - VMware service URL<br>**uuid** - VMware virtual machine host name<br>**instance** - disk device instance (mandatory)|<|
|vmware.vm.storage.totalreadlatency\[<url>,<uuid>,<instance>\]|<|<|<|<|
|<|The average time a read from the virtual disk takes (milliseconds).|Integer|**url** - VMware service URL<br>**uuid** - VMware virtual machine host name<br>**instance** - disk device instance (mandatory)|<|
|vmware.vm.storage.totalwritelatency\[<url>,<uuid>,<instance>\]|<|<|<|<|
|<|The average time a write to the virtual disk takes (milliseconds).|Integer|**url** - VMware service URL<br>**uuid** - VMware virtual machine host name<br>**instance** - disk device instance (mandatory)|<|
|vmware.vm.storage.uncommitted\[<url>,<uuid>\]|<|<|<|<|
|<|VMware virtual machine uncommitted storage space (bytes).|Integer|**url** - VMware service URL<br>**uuid** - VMware virtual machine host name|<|
|vmware.vm.storage.unshared\[<url>,<uuid>\]|<|<|<|<|
|<|VMware virtual machine unshared storage space (bytes).|Integer|**url** - VMware service URL<br>**uuid** - VMware virtual machine host name|<|
|vmware.vm.storage.writeoio\[<url>,<uuid>,<instance>\]|<|<|<|<|
|<|Average number of outstanding write requests to the virtual disk during the collection interval.|Integer|**url** - VMware service URL<br>**uuid** - VMware virtual machine host name<br>**instance** - disk device instance (mandatory)|<|
|vmware.vm.uptime\[<url>,<uuid>\]|<|<|<|<|
|<|VMware virtual machine uptime (seconds).|Integer|**url** - VMware service URL<br>**uuid** - VMware virtual machine host name|<|
|vmware.vm.vfs.dev.discovery\[<url>,<uuid>\]|<|<|<|<|
|<|Discovery of VMware virtual machine disk devices.|JSON object|**url** - VMware service URL<br>**uuid** - VMware virtual machine host name|<|
|vmware.vm.vfs.dev.read\[<url>,<uuid>,<instance>,<mode>\]|<|<|<|<|
|<|VMware virtual machine disk device read statistics (bytes/operations per second).|Integer ^**[2](vmware_keys#footnotes)**^|**url** - VMware service URL<br>**uuid** - VMware virtual machine host name<br>**instance** - disk device instance<br>**mode** - bps (default)/ops - bytes/operations per second|<|
|vmware.vm.vfs.dev.write\[<url>,<uuid>,<instance>,<mode>\]|<|<|<|<|
|<|VMware virtual machine disk device write statistics (bytes/operations per second).|Integer ^**[2](vmware_keys#footnotes)**^|**url** - VMware service URL<br>**uuid** - VMware virtual machine host name<br>**instance** - disk device instance<br>**mode** - bps (default)/ops - bytes/operations per second|<|
|vmware.vm.vfs.fs.discovery\[<url>,<uuid>\]|<|<|<|<|
|<|Discovery of VMware virtual machine file systems.|JSON object|**url** - VMware service URL<br>**uuid** - VMware virtual machine host name|VMware Tools must be installed on the guest virtual machine.|
|vmware.vm.vfs.fs.size\[<url>,<uuid>,<fsname>,<mode>\]|<|<|<|<|
|<|VMware virtual machine file system statistics (bytes/percentages).|Integer|**url** - VMware service URL<br>**uuid** - VMware virtual machine host name<br>**fsname** - file system name<br>**mode** - total/free/used/pfree/pused|VMware Tools must be installed on the guest virtual machine.|

[comment]: # ({/new-98337d09})


