[comment]: # translation:outdated

[comment]: # ({new-5ae18af3})
# JMX template operation

Steps to ensure correct operation of templates that collect metrics by
[JMX](/manual/config/items/itemtypes/jmx_monitoring):

1\. Make sure Zabbix [Java
gateway](/manual/concepts/java#getting_java_gateway) is installed and
set up properly.\
2. [Link](/manual/config/templates/linking#linking_a_template) the
template to the target host. The host should have JMX interface set up.\
If the template is not available in your Zabbix installation, you may
need to import the template's .xml file first - see [Templates
out-of-the-box](/manual/config/templates_out_of_the_box) section for
instructions.\
3. Adjust the values of mandatory macros as needed.\
4. Configure the instance being monitored to allow sharing data with
Zabbix - see instructions in the *Additional steps/comments* column.

::: notetip
 This page contains only a minimum set of macros and
setup steps that are required for proper template operation. A detailed
description of a template, including the full list of macros, items, and
triggers, is available in the template's Readme.md file (accessible by
clicking on a template name). 
:::

|Template|Mandatory macros|Additional steps/comments|
|--------|----------------|-------------------------|
|[Apache ActiveMQ by JMX](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/app/activemq_jmx/README.md)|**{$ACTIVEMQ.PORT}** - port for JMX (default: 1099).<br>**{$ACTIVEMQ.USERNAME}**, **{$ACTIVEMQ.PASSWORD}** - login credentials for JMX<br>(default username: admin, password: activemq).|JMX access to Apache ActiveMQ should be enabled and configured per instructions in [the official documentation](https://activemq.apache.org/jmx.html).|
|[Apache Cassandra by JMX](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/db/cassandra_jmx/README.md)|**{$CASSANDRA.USER}**, **{$CASSANDRA.PASSWORD}** - Apache Cassandra login credentials<br>(default username: zabbix, password: zabbix)|JMX access to Apache Cassandra should be enabled and configured per instructions in [the official documentation](https://cassandra.apache.org/doc/latest/operating/security.html#jmx-access).|
|[Apache Kafka by JMX](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/app/kafka_jmx/README.md)|**{$KAFKA.USER}**, **{$KAFKA.PASSWORD}** - Apache Kafka login credentials<br>(default username: zabbix, password: zabbix)|JMX access to Apache Kafka should be enabled and configured per instructions in [the official documentation](https://kafka.apache.org/documentation/#remote_jmx).|
|[Apache Tomcat by JMX](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/app/tomcat_jmx/README.md)|**{$TOMCAT.USER}**, **{$TOMCAT.PASSWORD}** - Apache Tomcat login credentials; leave blank if Tomcat installation does not require authentication<br>(default: not set).|JMX access to Apache Tomcat should be enabled and configured per instructions in [the official documentation](https://tomcat.apache.org/tomcat-10.0-doc/monitoring.html#Enabling_JMX_Remote) (choose the correct version).|
|[GridGain by JMX](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/db/gridgain_jmx/README.md)|**{$GRIDGAIN.USER}**, **{$GRIDGAIN.PASSWORD}** - GridGain login credentials (default username: zabbix, password: <secret>).|JMX access to GridGain In-Memory Computing Platform should be enabled and configured per instructions in [the documentation](https://docs.oracle.com/javase/8/docs/technotes/guides/management/agent.html).|
|[Ignite by JMX](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/db/ignite_jmx/README.md)|**{$IGNITE.USER}**, **{$IGNITE.PASSWORD}** - Apache Ignite login credentials (default username: zabbix, password: <secret>).|Enable and configure JMX access to Apache Ignite.<br><br>JMX tree hierarchy contains ClassLoader by default. Adding the following Java Virtual Machine option `-DIGNITE_MBEAN_APPEND_CLASS_LOADER_ID=false` will exclude one level with ClassLoader name.<br><br>Cache and Data Region metrics can be configured as needed - see [Ignite documentation](https://ignite.apache.org/docs/latest/monitoring-metrics/configuring-metrics) for details.<br><br>See also: [Monitoring and Management Using JMX Technology](https://docs.oracle.com/javase/8/docs/technotes/guides/management/agent.html)|
|[WildFly Domain by JMX](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/app/wildfly_domain_jmx/README.md)|**{$WILDFLY.JMX.PROTOCOL}** - JMX scheme (default: remote+http)<br>**{$WILDFLY.USER}**, **{$WILDFLY.PASSWORD}** - WildFly login credentials (default username: zabbix, password: zabbix).|1\. Enable and configure JMX access to WildFly according to instructions in [the official documentation](https://docs.wildfly.org/23/Admin_Guide.html#JMX).<br><br>2. Copy *jboss-client.jar* from `/(wildfly,EAP,Jboss,AS)/bin/client` into directory `/usr/share/zabbix-java-gateway/lib`.<br><br>3. Restart Zabbix Java gateway.|
|[WildFly Server by JMX](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/app/wildfly_server_jmx/README.md)|**{$WILDFLY.JMX.PROTOCOL}** - JMX scheme (default: remote+http)<br>**{$WILDFLY.USER}**, **{$WILDFLY.PASSWORD}** - WildFly login credentials (default username: zabbix, password: zabbix).|1\. Enable and configure JMX access to WildFly according to instructions in [the official documentation](https://docs.wildfly.org/23/Admin_Guide.html#JMX).<br><br>2. Copy *jboss-client.jar* from `/(wildfly,EAP,Jboss,AS)/bin/client` into directory `/usr/share/zabbix-java-gateway/lib`.<br><br>3. Restart Zabbix Java gateway.|

[comment]: # ({/new-5ae18af3})
