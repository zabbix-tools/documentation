[comment]: # translation:outdated

[comment]: # ({new-d5d0048d})
# 3 Trigger dependencies

[comment]: # ({/new-d5d0048d})

[comment]: # ({new-26bda053})
#### Overview

Sometimes the availability of one host depends on another. A server that
is behind some router will become unreachable if the router goes down.
With triggers configured for both, you might get notifications about two
hosts down - while only the router was the guilty party.

This is where some dependency between hosts might be useful. With
dependency set notifications of the dependents could be withheld and
only the notification for the root problem sent.

While Zabbix does not support dependencies between hosts directly, they
may be defined with another, more flexible method - trigger
dependencies. A trigger may have one or more triggers it depends on.

So in our simple example we open the server trigger configuration form
and set that it depends on the respective trigger of the router. With
such dependency the server trigger will not change state as long as the
trigger it depends on is in 'PROBLEM' state - and thus no dependent
actions will be taken and no notifications sent.

If both the server and the router are down and dependency is there,
Zabbix will not execute actions for the dependent trigger.

Actions on dependent triggers will not be executed if the trigger they
depend on:

-   changes its state from 'PROBLEM' to 'UNKNOWN'
-   is closed manually, by correlation or with the help of time- based
    functions
-   is resolved by a value of an item not involved in dependent trigger
-   is disabled, has disabled item or disabled item host

Note that "secondary" (dependent) trigger in the above-mentioned cases
will not be immediately updated. While parent trigger is in PROBLEM
state, it's dependents may report values, which we cannot trust. Thus,
dependent trigger will only be re-evaluated, and change it's state, only
after parent trigger is in OK state and we have received trusty metrics.

Also:

-   Trigger dependency may be added from any host trigger to any other
    host trigger, as long as it wouldn't result in a circular
    dependency.
-   Trigger dependency may be added from a template to a template. If a
    trigger from template A depends on a trigger from template B,
    template A may only be linked to a host (or another template)
    together with template B, but template B may be linked to a host (or
    another template) alone.
-   Trigger dependency may be added from template trigger to a host
    trigger. In this case, linking such a template to a host will create
    a host trigger that depends on the same trigger template trigger was
    depending on. This allows to, for example, have a template where
    some triggers depend on router (host) triggers. All hosts linked to
    this template will depend on that specific router.
-   Trigger dependency from a host trigger to a template trigger may not
    be added.
-   Trigger dependency may be added from a trigger prototype to another
    trigger prototype (within the same low-level discovery rule) or a
    real trigger. A trigger prototype may not depend on a trigger
    prototype from a different LLD rule or on a trigger created from
    trigger prototype. Host trigger prototype cannot depend on a trigger
    from a template.

[comment]: # ({/new-26bda053})

[comment]: # ({new-c4578b73})
#### Configuration

To define a dependency, open the Dependencies tab in a trigger
[configuration form](trigger#configuration). Click on *Add* in the
'Dependencies' block and select one or more triggers that our trigger
will depend on.

![](../../../../assets/en/manual/config/triggers/dependency.png)

Click *Update*. Now the trigger has an indication of its dependency in
the list.

![](../../../../assets/en/manual/config/triggers/dependency_list.png)

[comment]: # ({/new-c4578b73})

[comment]: # ({new-4dc2ce6b})
##### Example of several dependencies

For example, a Host is behind a Router2 and the Router2 is behind a
Router1.

    Zabbix - Router1 - Router2 - Host

If Router1 is down, then obviously Host and Router2 are also unreachable
yet we don't want to receive three notifications about Host, Router1 and
Router2 all being down.

So in this case we define two dependencies:

    'Host is down' trigger depends on 'Router2 is down' trigger
    'Router2 is down' trigger depends on 'Router1 is down' trigger

Before changing the status of the 'Host is down' trigger, Zabbix will
check for corresponding trigger dependencies. If found, and one of those
triggers is in 'Problem' state, then the trigger status will not be
changed and thus actions will not be executed and notifications will not
be sent.

Zabbix performs this check recursively. If Router1 or Router2 is
unreachable, the Host trigger won't be updated.

[comment]: # ({/new-4dc2ce6b})
