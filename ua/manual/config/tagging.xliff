<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="ua" datatype="plaintext" original="manual/config/tagging.md">
    <body>
      <trans-unit id="12728bfa" xml:space="preserve">
        <source># 6 Tagging</source>
      </trans-unit>
      <trans-unit id="700f39aa" xml:space="preserve">
        <source>#### Overview

There is an option to tag various entities in Zabbix. Tags can be
defined for:

-   templates
-   hosts
-   items
-   web scenarios
-   triggers
-   services
-   template items and triggers
-   host, item and trigger prototypes

Tags have several uses, most notably, to mark events. If entities are
tagged, the corresponding new events get marked accordingly:

-   with tagged templates - any host problems created by relevant
    entities (items, triggers, etc) from this template will be marked
-   with tagged hosts - any problem of the host will be marked
-   with tagged items, web scenarios - any data/problem of this item or
    web scenario will be marked
-   with tagged triggers - any problem of this trigger will be marked

A problem event inherits all tags from the whole chain of templates,
hosts, items, web scenarios, triggers. Completely identical `tag:value`
combinations (after resolved macros) are merged into one rather than
being duplicated, when marking the event.

Having custom event tags allows for more flexibility. Importantly,
events can be [correlated](/manual/config/event_correlation) based on
event tags. In other uses, actions can be defined based on tagged
events. Item problems can be grouped based on tags. Problem tags can also be used to map problems to [services](/manual/it_services/service_tree#problem-tags).

Tagging is realized as a pair of *tag name* and *value*. You can use
only the name or pair it with a value:

    MySQL, Service:MySQL, Services, Services:Customer, Applications, Application:Java, Priority:High 

An entity (template, host, item, web scenario, trigger or event) may be
tagged with the same name, but different values - these tags will not be
considered 'duplicates'. Similarly, a tag without value and the same tag
with value can be used simultaneously.</source>
      </trans-unit>
      <trans-unit id="1a2fe757" xml:space="preserve">
        <source>#### Use cases

Some use cases for this functionality are as follows:

1.  Mark trigger events in the frontend:
    -   Define a tag at the trigger level, for example `scope:performance`;
    -   All problems created by this trigger will be marked with this tag.
2.  Mark all template-inherited problems:
    -   Define a tag at the template level, for example `target:MySQL`;
    -   All host problems created by triggers from this template will be marked with this tag.
3.  Mark all host problems:
    -   Define a tag at the host level, for example `service:Jira`;
    -   All problems of the host triggers will be marked with this tag.
4.  Group related items:
    -   Define a tag at the item level, for example `component:cpu`;
    -   In the *Latest data* section, use the tag filter to view all items tagged as `component:cpu`.
5.  Identify problems in a log file and close them separately:
    -   Define tags in the log trigger that will identify events using value extraction by the `{{ITEM.VALUE&lt;N&gt;}.regsub()}` macro;
    -   In the trigger configuration, set multiple problem event generation mode;
    -   In the trigger configuration, use [event correlation](/manual/config/event_correlation): 
        select the option that OK event closes only matching events and choose the tag for matching;
    -   See problem events created with a tag and closed individually.
6.  Use it to filter notifications:
    -   Define tags at the trigger level to mark events by different tags;
    -   Use tag filtering in action conditions to receive notifications only on the events that match tag data.
7.  Use information extracted from item value as tag value:
    -   Use an `{{ITEM.VALUE&lt;N&gt;}.regsub()}` macro in the tag value;
    -   See tag values in *Monitoring* → *Problems* as extracted data from the item value.
8.  Identify problems better in notifications:
    -   Define tags at the trigger level;
    -   Use an {EVENT.TAGS} macro in the problem notification;
    -   Easier identify which application/service the notification belongs to.
9.  Simplify configuration tasks by using tags at the template level:
    -   Define tags at the template trigger level;
    -   See these tags on all triggers created from template triggers.
10. Create triggers with tags from low-level discovery (LLD):
    -   Define tags on trigger prototypes;
    -   Use LLD macros in the tag name or value;
    -   See these tags on all triggers created from trigger prototypes.
11. Match services using [service tags](/manual/it_services/service_tree#service-tags):
    -   Define [service actions](/manual/it_services/service_actions#conditions) for services with matching tags;
    -   Use service tags to map a service to an SLA for [SLA calculations](/manual/it_services/sla#configuration).
12. Map services to problems using [problem tags](/manual/it_services/service_tree#service-configuration):
    -   In the service configuration, specify [problem tag](/manual/it_services/service_tree#problem-tags), for example `target:MySQL`;
    -   Problems with the matching tag will be automatically correlated to the service;
    -   Service status will change to the status of the problem with the highest severity. 
13. Suppress problems when a host is in maintenance mode:
    -   Define tags in *[Maintenance periods](/manual/maintenance#configuration)* to suppress only problems with matching tags.
14. Grant access to user groups:
    -   Specify tags in the [user group](/manual/config/users_and_usergroups/usergroup#configuration) configuration to allow viewing only problems with matching tags.</source>
      </trans-unit>
      <trans-unit id="4793fc2c" xml:space="preserve">
        <source>#### Configuration

Tags can be entered in a dedicated tab, for example, in trigger
configuration:

![](../../../assets/en/manual/config/tags_trigger.png)</source>
      </trans-unit>
      <trans-unit id="aace9fc0" xml:space="preserve">
        <source>#### Macro support

Built-in and user macros in tags are resolved at the time of the event. Until the event has occurred these macros will be shown in Zabbix frontend unresolved. Low-level discovery macros are resolved during discovery process.

The following macros may be used in trigger tags:

-   {ITEM.VALUE}, {ITEM.LASTVALUE}, {HOST.HOST}, {HOST.NAME}, {HOST.CONN}, {HOST.DNS}, {HOST.IP}, {HOST.PORT} and {HOST.ID}
    macros can be used to populate the tag name or tag value.
-   {INVENTORY.\*} [macros](/manual/appendix/macros/supported_by_location) can be used
    to reference host inventory values from one or several hosts in a trigger expression.
-   [User macros](/manual/config/macros/user_macros) and user macros with context are supported for the tag name/value;
    context may include low-level discovery macros.
-   Low-level discovery macros can be used for the tag name/value in
    trigger prototypes.

The following macros may be used in trigger-based notifications:

-   {EVENT.TAGS} and {EVENT.RECOVERY.TAGS} macros will resolve to a
    comma separated list of event tags or recovery event tags
-   {EVENT.TAGSJSON} and {EVENT.RECOVERY.TAGSJSON} macros will resolve
    to a JSON array containing event tag
    [objects](/manual/api/reference/event/object#event_tag) or recovery
    event tag objects

The following macros may be used in template, host, item and web
scenario tags:

-   {HOST.HOST}, {HOST.NAME}, {HOST.CONN}, {HOST.DNS}, {HOST.IP},
    {HOST.PORT} and {HOST.ID} macros
-   {INVENTORY.\*}
    [macros](/manual/appendix/macros/supported_by_location)
-   [User macros](/manual/config/macros/user_macros)
-   Low-level discovery macros can be used in item prototype tags

The following macros may be used in host prototype tags:

-   {HOST.HOST}, {HOST.NAME}, {HOST.CONN}, {HOST.DNS}, {HOST.IP},
    {HOST.PORT} and {HOST.ID} macros
-   {INVENTORY.\*}
    [macros](/manual/appendix/macros/supported_by_location)
-   [User macros](/manual/config/macros/user_macros)
-   [Low-level discovery macros](/manual/config/macros/lld_macros) will
    be resolved during discovery process and then added to the
    discovered host</source>
      </trans-unit>
      <trans-unit id="20ae36ff" xml:space="preserve">
        <source>##### Substring extraction in trigger tags

Substring extraction is supported for populating the tag name or tag
value, using a macro [function](/manual/config/macros/macro_functions) -
applying a regular expression to the value obtained by the {ITEM.VALUE},
{ITEM.LASTVALUE} macro or a low-level discovery macro. For example:

    {{ITEM.VALUE}.regsub(pattern, output)}
    {{ITEM.VALUE}.iregsub(pattern, output)}

    {{#LLDMACRO}.regsub(pattern, output)}
    {{#LLDMACRO}.iregsub(pattern, output)}

Tag name and value will be cut to 255 characters if their length exceeds
255 characters after macro resolution.

See also: Using macro functions in [low-level discovery
macros](/manual/config/macros/lld_macros#using_macro_functions) for
event tagging.</source>
      </trans-unit>
      <trans-unit id="048b2b9c" xml:space="preserve">
        <source>#### Viewing event tags

Tagging, if defined, can be seen with new events in:

-   *Monitoring* → *Problems*
-   *Monitoring* → *Problems* → *Event details*
-   *Dashboards* → *Problems* widget

![](../../../assets/en/manual/config/triggers/event_tags_view.png){width="600"}

Only the first three tag entries can be displayed. If there are more than
three tag entries, it is indicated by three dots. If you roll your mouse
over these three dots, all tag entries are displayed in a pop-up window.

Note that the order in which tags are displayed is affected by tag
filtering and the *Tag display priority* option in the filter of
*Monitoring* → *Problems* or the *Problems* dashboard widget.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
