[comment]: # translation:outdated

[comment]: # ({new-e38500bf})
# 3 Dashboards

[Dashboards](/manual/web_interface/frontend_sections/monitoring/dashboard)
and their widgets provide a strong visualization platform with such
tools as modern graphs, maps, slideshows, and many more.

![](../../../../assets/en/manual/config/visualization/dashboard.png){width="600"}

[comment]: # ({/new-e38500bf})
