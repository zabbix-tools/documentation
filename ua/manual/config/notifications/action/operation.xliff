<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="ua" datatype="plaintext" original="manual/config/notifications/action/operation.md">
    <body>
      <trans-unit id="531c9a02" xml:space="preserve">
        <source># 2 Operations</source>
      </trans-unit>
      <trans-unit id="49564975" xml:space="preserve">
        <source>#### Overview

You can define the following operations for all events:

-   Send a message
-   Execute a remote command

::: noteimportant
Zabbix server does not create alerts if access to
the host is explicitly "denied" for the user defined as action operation
recipient or if the user has no rights defined to the host at
all.
:::

For discovery and autoregistration events, additional operations are available:

-   [Add host](/manual/config/notifications/action/operation/other#adding_host)
-   Remove host
-   Enable host
-   Disable host
-   Add to host group
-   Remove from host group
-   Link template
-   Unlink template
-   Set host inventory mode</source>
      </trans-unit>
      <trans-unit id="4b8a4b65" xml:space="preserve">
        <source>#### Configuring an operation

To configure an operation, go to the *Operations* tab in
[action](/manual/config/notifications/action) configuration.

![](../../../../../assets/en/manual/config/notifications/action_operation2.png){width="600"}

General operation attributes:

|Parameter|Description|
|--|-----------|
|*Default operation step duration*|Duration of one operation step by default (60 seconds to 1 week).&lt;br&gt;For example, an hour-long step duration means that if an operation is carried out, an hour will pass before the next step.&lt;br&gt;[Time suffixes](/manual/appendix/suffixes) are supported, e.g. 60s, 1m, 2h, 1d, since Zabbix 3.4.0.&lt;br&gt;[User macros](/manual/config/macros/user_macros) are supported, since Zabbix 3.4.0.|
|*Operations*|Action operations (if any) are displayed, with these details:&lt;br&gt;**Steps** - escalation step(s) to which the operation is assigned.&lt;br&gt;**Details** - type of operation and its recipient/target.&lt;br&gt;The operation list also displays the media type (email, SMS or script) used as well as the name and surname (in parentheses after the username) of a notification recipient.&lt;br&gt;**Start in** - how long after an event the operation is performed.&lt;br&gt;**Duration (sec)** - step duration is displayed. *Default* is displayed if the step uses default duration, and a time is displayed if custom duration is used.&lt;br&gt;**Action** - links for editing and removing an operation are displayed.|
|*Recovery operations*|Action operations (if any) are displayed, with these details:&lt;br&gt;**Details** - type of operation and its recipient/target.&lt;br&gt;The operation list also displays the media type (email, SMS or script) used as well as the name and surname (in parentheses after the username) of a notification recipient.&lt;br&gt;**Action** - links for editing and removing an operation are displayed.|
|*Update operations*|Action operations (if any) are displayed, with these details:&lt;br&gt;**Details** - type of operation and its recipient/target.&lt;br&gt;The operation list also displays the media type (email, SMS or script) used as well as the name and surname (in parentheses after the username) of a notification recipient.&lt;br&gt;**Action** - links for editing and removing an operation are displayed.|
|*Pause operations for symptom problems*|Mark this checkbox to pause operations (after the first operation) for symptom problems.&lt;br&gt;Note that this setting affects only problem escalations; recovery and update operations will not be affected.&lt;br&gt;This option is available for *Trigger actions* only.|
|*Pause operations for suppressed problems*|Mark this checkbox to delay the start of operations for the duration of a maintenance period. When operations are started, after the maintenance, all operations are performed including those for the events during the maintenance.&lt;br&gt;Note that this setting affects only problem escalations; recovery and update operations will not be affected.&lt;br&gt;If you unmark this checkbox, operations will be executed without delay even during a maintenance period.&lt;br&gt;This option is not available for *Service actions*.|
|*Notify about canceled escalations*|Unmark this checkbox to disable notifications about canceled escalations (when host, item, trigger or action is disabled).|

All mandatory input fields are marked with a red asterisk.

To configure details of a new operation, click on
![](../../../../../assets/en/manual/config/add_link.png) in the
*Operations* block. To edit an existing operation, click on
![](../../../../../assets/en/manual/config/edit_link.png) next to the operation.
A pop-up window will open where you can edit the operation step details.</source>
      </trans-unit>
      <trans-unit id="4a37f720" xml:space="preserve">
        <source>#### Operation details

![](../../../../../assets/en/manual/config/operation_details.png){width="600"}

|Parameter|&lt;|&lt;|Description|
|-|-|----------|--------------------------------------|
|*Operation*|&lt;|&lt;|Select the operation:&lt;br&gt;**Send message** - send message to user.&lt;br&gt;**&lt;remote command name&gt;** - execute a remote command. Commands are available for execution if previously defined in [global scripts](/manual/web_interface/frontend_sections/alerts/scripts#configuring_a_global_script) with *Action operation* selected as its scope.&lt;br&gt;More operations are available for discovery and autoregistration based events (see above).|
|*Steps*|&lt;|&lt;|Select the step(s) to assign the operation to in an [escalation](escalations) schedule:&lt;br&gt;**From** - execute starting with this step.&lt;br&gt;**To** - execute until this step (0=infinity, execution will not be limited).|
|*Step duration*|&lt;|&lt;|Custom duration for these steps (0=use default step duration).&lt;br&gt;[Time suffixes](/manual/appendix/suffixes) are supported, e.g. 60s, 1m, 2h, 1d, since Zabbix 3.4.0.&lt;br&gt;[User macros](/manual/config/macros/user_macros) are supported, since Zabbix 3.4.0.&lt;br&gt;Several operations can be assigned to the same step. If these operations have different step duration defined, the shortest one is taken into account and applied to the step.|
|Operation type: [send message](/manual/config/notifications/action/operation/message)|&lt;|&lt;|&lt;|
| |*Send to user groups*|&lt;|Select user groups to send the message to.&lt;br&gt;The user group must have at least "read" [permissions](/manual/config/users_and_usergroups/permissions) to the host in order to be notified.|
|^|*Send to users*|&lt;|Select users to send the message to.&lt;br&gt;The user must have at least "read" [permissions](/manual/config/users_and_usergroups/permissions) to the host in order to be notified.|
|^|*Send only to*|&lt;|Send message to all defined media types or a selected one only.|
|^|*Custom message*|&lt;|If selected, the custom message can be configured.&lt;br&gt;For notifications about internal events via [webhooks](/manual/config/notifications/media/webhook), custom message is mandatory.|
|^| |*Subject*|Subject of the custom message. The subject may contain macros. It is limited to 255 characters.|
|^|^|*Message*|The custom message. The message may contain macros. It is limited to certain amount of characters depending on the type of database (see [Sending message](/manual/config/notifications/action/operation/message) for more information).|
|Operation type: [remote command](/manual/config/notifications/action/operation/remote_command)|&lt;|&lt;|&lt;|
| |*Target list*|&lt;|Select targets to execute the command on:&lt;br&gt;**Current host** - command is executed on the host of the trigger that caused the problem event. This option will not work if there are multiple hosts in the trigger.&lt;br&gt;**Host** - select host(s) to execute the command on.&lt;br&gt;**Host group** - select host group(s) to execute the command on. Specifying a parent host group implicitly selects all nested host groups. Thus the remote command will also be executed on hosts from nested groups.&lt;br&gt;A command on a host is executed only once, even if the host matches more than once (e.g. from several host groups; individually and from a host group).&lt;br&gt;The target list is meaningless if a custom script is executed on Zabbix server. Selecting more targets in this case only results in the script being executed on the server more times.&lt;br&gt;Note that for global scripts, the target selection also depends on the *Host group* setting in global script [configuration](/manual/web_interface/frontend_sections/alerts/scripts#configuring_a_global_script).&lt;br&gt;*Target list* option is not available for *Service actions* because in this case remote commands are always executed on Zabbix server.|
|*Conditions*|&lt;|&lt;|Condition for performing the operation:&lt;br&gt;**Event is not acknowledged** - only when the event is unacknowledged.&lt;br&gt;**Event is acknowledged** - only when the event is acknowledged.&lt;br&gt;*Conditions* option is only available for *Trigger actions*.|

When done, click *Add* to add the operation to the list of *Operations*.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
