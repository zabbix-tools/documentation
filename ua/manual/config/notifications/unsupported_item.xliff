<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="ua" datatype="plaintext" original="manual/config/notifications/unsupported_item.md">
    <body>
      <trans-unit id="931a392a" xml:space="preserve">
        <source># 3 Receiving notification on unsupported items</source>
      </trans-unit>
      <trans-unit id="436ff6e8" xml:space="preserve">
        <source>#### Overview

Receiving notifications on unsupported items is supported since Zabbix
2.2.

It is part of the concept of internal events in Zabbix, allowing users
to be notified on these occasions. [Internal events](/manual/config/events/sources#internal-events) reflect a change of
state:

-   when items go from 'normal' to 'unsupported' (and back);
-   when triggers go from 'normal' to 'unknown' (and back);
-   when low-level discovery rules go from 'normal' to 'unsupported' (and back).

This section presents a how-to for **receiving notification** when an
item turns unsupported.</source>
      </trans-unit>
      <trans-unit id="29f60fbd" xml:space="preserve">
        <source>#### Configuration

Overall, the process of setting up the notification should feel familiar
to those who have set up alerts in Zabbix before.</source>
      </trans-unit>
      <trans-unit id="6b217825" xml:space="preserve">
        <source>##### Step 1

Configure [some media](media), such as email, SMS, or script to use for
the notifications. Refer to the corresponding sections of the manual to
perform this task.

::: noteimportant
For notifying on internal events the default
severity ('Not classified') is used, so leave it checked when
configuring [user
media](/manual/config/notifications/media/email#user_media) if you want
to receive notifications for internal events.
:::</source>
      </trans-unit>
      <trans-unit id="88b88d25" xml:space="preserve">
        <source>##### Step 2

Go to *Alerts → Actions* → *Internal actions*.

Click on *Create action* at the top right corner of the page to open an action configuration form.</source>
      </trans-unit>
      <trans-unit id="9e5f70bb" xml:space="preserve">
        <source>##### Step 3

In the *Action* tab enter a name for the action.
Then click on *Add* in the *Conditions* block to add a new condition.

![](../../../../assets/en/manual/config/notifications/report_items_actions.png){width="600"}

In the *New condition* pop-up window select "Event type" as the condition type and then select "Item in 'not supported' state" as the event type.

![](../../../../assets/en/manual/config/notifications/report_items_actions_details.png){width="600"}

Don't forget to click on *Add* to actually list the condition in the *Conditions* block.</source>
      </trans-unit>
      <trans-unit id="395fb8cf" xml:space="preserve">
        <source>##### Step 4

In the *Operations* tab, click on *Add* in the *Operations* block to add a new operation.

![](../../../../assets/en/manual/config/notifications/report_items_operations1.png){width="600"}

Select some recipients of the message (user groups/users) and the media type (or "All") to use for delivery.
Check the *Custom message* checkbox if you wish to enter the custom subject/content of the problem message.

![](../../../../assets/en/manual/config/notifications/report_items_operations1_details.png){width="600"}

Click on *Add* to actually list the operation in the *Operations* block.

If you wish to receive more than one notification, set the operation step duration (interval between messages sent) and add another step.</source>
      </trans-unit>
      <trans-unit id="921fcf26" xml:space="preserve">
        <source>##### Step 5

The *Recovery operations* block allows to configure a recovery notification when an item goes back to the normal state.
Click on *Add* in the *Recovery operations* block to add a new recovery operation.

![](../../../../assets/en/manual/config/notifications/report_items_operations2.png){width="600"}

Select the operation type "Notify all involved".
Select *Custom message* checkbox if you wish to enter the custom subject/content of the problem message.

![](../../../../assets/en/manual/config/notifications/report_items_operations2_details.png){width="600"}

Click on *Add* in the *Operation details* pop-up window to actually list the operation in the *Recovery operations* block.</source>
      </trans-unit>
      <trans-unit id="9035a0ae" xml:space="preserve">
        <source>##### Step 6

When finished, click on the *Add* button at the bottom of the form.

![](../../../../assets/en/manual/config/notifications/report_items_operations3.png){width="600"}

And that's it, you're done!
Now you can look forward to receiving your first notification from Zabbix if some item turns unsupported.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
