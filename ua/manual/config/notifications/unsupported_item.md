[comment]: # translation:outdated

[comment]: # ({new-931a392a})
# 3 Receiving notification on unsupported items

[comment]: # ({/new-931a392a})

[comment]: # ({new-436ff6e8})
#### Overview

Receiving notifications on unsupported items is supported since Zabbix
2.2.

It is part of the concept of internal events in Zabbix, allowing users
to be notified on these occasions. Internal events reflect a change of
state:

-   when items go from 'normal' to 'unsupported' (and back)
-   when triggers go from 'normal' to 'unknown' (and back)
-   when low-level discovery rules go from 'normal' to 'unsupported'
    (and back)

This section presents a how-to for **receiving notification** when an
item turns unsupported.

[comment]: # ({/new-436ff6e8})

[comment]: # ({new-29f60fbd})
#### Configuration

Overall, the process of setting up the notification should feel familiar
to those who have set up alerts in Zabbix before.

[comment]: # ({/new-29f60fbd})

[comment]: # ({new-6b217825})
##### Step 1

Configure [some media](media), such as e-mail, SMS, or script to use for
the notifications. Refer to the corresponding sections of the manual to
perform this task.

::: noteimportant
For notifying on internal events the default
severity ('Not classified') is used, so leave it checked when
configuring [user
media](/manual/config/notifications/media/email#user_media) if you want
to receive notifications for internal events.
:::

[comment]: # ({/new-6b217825})

[comment]: # ({new-88b88d25})
##### Step 2

Go to *Configuration → Actions* and select *Internal actions* from the
page title dropdown.

![](../../../../assets/en/manual/config/internal_actions.png){width="600"}

Click on *Create action* to the right to open an action configuration
form.

[comment]: # ({/new-88b88d25})

[comment]: # ({new-9e5f70bb})
##### Step 3

In the **Action** tab enter a name for the action. Then click on *Add*
in the condition block to add a new condition.

In the new condition popup window select *Event type* as the condition
type and then select *Item in "not supported" state* as the event type
value.

![](../../../../assets/en/manual/config/notifications/action/report_unsupported_cond.png)

Don't forget to click on *Add* to actually list the condition in the
*Conditions* block.

[comment]: # ({/new-9e5f70bb})

[comment]: # ({new-395fb8cf})
##### Step 4

In the **Operations** tab, click on *Add* in the *Operations* block and
select some recipients of the message (user groups/users) and the media
types (or 'All') to use for delivery.

Select *Custom message* checkbox if you wish to enter the custom
subject/content of the problem message.

![](../../../../assets/en/manual/config/unsupp_operation.png)

![](../../../../assets/en/manual/config/unsupp_operation_b.png)

Click on *Add* to actually list the operation in the *Operations* block.

If you wish to receive more than one notification, set the operation
step duration (interval between messages sent) and add another step.

[comment]: # ({/new-395fb8cf})

[comment]: # ({new-921fcf26})
##### Step 5

The **Recovery operations** block allows to configure a recovery
notification when an item goes back to the normal state. Click on *Add*
in the *Recovery operations* block, select the operation type, the
recipients of the message (user groups/users) and the media types (or
'All') to use for delivery.

Select *Custom message* checkbox if you wish to enter the custom
subject/content of the problem message.

![](../../../../assets/en/manual/config/unsupp_recovery.png)

Click on *Add* in the *Operation details* popup window to actually list
the operation in the *Recovery operations* block.

[comment]: # ({/new-921fcf26})

[comment]: # ({new-9035a0ae})
##### Step 6

When finished, click on the **Add** button at the bottom of the form.

And that's it, you're done! Now you can look forward to receiving your
first notification from Zabbix if some item turns unsupported.

[comment]: # ({/new-9035a0ae})
