[comment]: # translation:outdated

[comment]: # ({new-1c2455b7})
# 2 Active agent autoregistration

[comment]: # ({/new-1c2455b7})

[comment]: # ({new-81edbbe2})
#### Overview

It is possible to allow active Zabbix agent autoregistration, after
which the server can start monitoring them. This way new hosts can be
added for monitoring without configuring them manually on the server.

Autoregistration can happen when a previously unknown active agent asks
for checks.

The feature might be very handy for automatic monitoring of new Cloud
nodes. As soon as you have a new node in the Cloud Zabbix will
automatically start the collection of performance and availability data
of the host.

Active agent autoregistration also supports the monitoring of added
hosts with passive checks. When the active agent asks for checks,
providing it has the 'ListenIP' or 'ListenPort' configuration parameters
defined in the configuration file, these are sent along to the server.
(If multiple IP addresses are specified, the first one is sent to the
server.)

Server, when adding the new autoregistered host, uses the received IP
address and port to configure the agent. If no IP address value is
received, the one used for the incoming connection is used. If no port
value is received, 10050 is used.

It is possible to specify that the host should be autoregistered with a
[DNS name](#using_dns_as_default_interface) as the default agent
interface.

Autoregistration is rerun:

-   if host [metadata](#using_host_metadata) information changes:
    -   due to HostMetadata changed and agent restarted
    -   due to value returned by HostMetadataItem changed
-   for manually created hosts with metadata missing
-   if a host is manually changed to be monitored by another Zabbix
    proxy
-   if autoregistration for the same host comes from a new Zabbix proxy

[comment]: # ({/new-81edbbe2})

[comment]: # ({new-5d32b87c})
#### Configuration

[comment]: # ({/new-5d32b87c})

[comment]: # ({new-bf3762f0})
##### Specify server

Make sure you have the Zabbix server identified in the agent
[configuration file](/manual/appendix/config/zabbix_agentd) -
zabbix\_agentd.conf

    ServerActive=10.0.0.1

Unless you specifically define a *Hostname* in zabbix\_agentd.conf, the
system hostname of agent location will be used by server for naming the
host. The system hostname in Linux can be obtained by running the
'hostname' command.

If *Hostname* is defined in Zabbix agent configuration as a
comma-delimited list of hosts, hosts will be created for all listed
hostnames.

Restart the agent after making any changes to the configuration file.

[comment]: # ({/new-bf3762f0})

[comment]: # ({new-7b103292})
##### Action for active agent autoregistration

When server receives an autoregistration request from an agent it calls
an [action](/manual/config/notifications/action). An action of event
source "Autoregistration" must be configured for agent autoregistration.

::: noteclassic
Setting up [network discovery](network_discovery) is not
required to have active agents autoregister.
:::

In the Zabbix frontend, go to *Configuration → Actions*, select
*Autoregistration* as the event source and click on *Create action*:

-   In the Action tab, give your action a name
-   Optionally specify
    [conditions](/manual/config/notifications/action/conditions#autoregistration_actions).
    You can do a substring match or regular expression match in the
    conditions for host name/host metadata. If you are going to use the
    "Host metadata" condition, see the next section.
-   In the Operations tab, add relevant operations, such as - 'Add
    host', 'Add to host group' (for example, *Discovered hosts*), 'Link
    to templates', etc.

::: notetip
If the hosts that will be autoregistering are likely to
be supported for active monitoring only (such as hosts that are
firewalled from your Zabbix server) then you might want to create a
specific template like *Template\_Linux-active* to link to.
:::

Created hosts are added to the *Discovered hosts* group (by default,
configurable in *Administration* → *General* →
*[Other](/manual/web_interface/frontend_sections/administration/general#other_parameters)*).
If you wish hosts to be added to another group, add a *Remove from host
group* operation (specifying "Discovered hosts") and also add an *Add to
host group* operation (specifying another host group), because a host
must belong to a host group.

[comment]: # ({/new-7b103292})

[comment]: # ({new-66cac4ef})
#### Secure autoregistration

A secure way of autoregistration is possible by configuring PSK-based
authentication with encrypted connections.

The level of encryption is configured globally in *Administration* →
*[General](/manual/web_interface/frontend_sections/administration/general)*,
in the Autoregistration section accessible through the dropdown to the
right. It is possible to select no encryption, TLS encryption with PSK
authentication or both (so that some hosts may register without
encryption while others through encryption).

Authentication by PSK is verified by Zabbix server before adding a host.
If successful, the host is added and *[Connections from/to
host](/manual/config/hosts/host#encryption)* are set to 'PSK' only with
identity/pre-shared key the same as in the global autoregistration
setting.

::: noteimportant
To ensure security of autoregistration on
installations using proxies, encryption between Zabbix server and proxy
should be enabled.
:::

[comment]: # ({/new-66cac4ef})

[comment]: # ({new-9c275675})
#### Using DNS as default interface

HostInterface and HostInterfaceItem [configuration
parameters](/manual/appendix/config/zabbix_agentd) allow to specify a
custom value for the host interface during autoregistration.

More specifically, they are useful if the host should be autoregistered
with a DNS name as the default agent interface rather than its IP
address. In that case the DNS name should be specified or returned as
the value of either HostInterface or HostInterfaceItem parameters. Note
that if the value of one of the two parameters changes, the
autoregistered host interface is updated. So it is possible to update
the default interface to another DNS name or update it to an IP address.
For the changes to take effect though, the agent has to be restarted.

::: noteclassic
If HostInterface or HostInterfaceItem parameters are not
configured, the listen\_dns parameter is resolved from the IP address.
If such resolving is configured incorrectly, it may break
autoregistration because of invalid hostname.
:::

[comment]: # ({/new-9c275675})

[comment]: # ({new-af247ed6})
#### Using host metadata

When agent is sending an autoregistration request to the server it sends
its hostname. In some cases (for example, Amazon cloud nodes) a hostname
is not enough for Zabbix server to differentiate discovered hosts. Host
metadata can be optionally used to send other information from an agent
to the server.

Host metadata is configured in the agent [configuration
file](/manual/appendix/config/zabbix_agentd) - zabbix\_agentd.conf.
There are 2 ways of specifying host metadata in the configuration file:

    HostMetadata
    HostMetadataItem

See the description of the options in the link above.

::: noteimportant
An autoregistration attempt happens every time an
active agent sends a request to refresh active checks to the server. The
delay between requests is specified in the
[RefreshActiveChecks](/manual/appendix/config/zabbix_agentd) parameter
of the agent. The first request is sent immediately after the agent is
restarted.
:::

[comment]: # ({/new-af247ed6})

[comment]: # ({new-070aaeb4})
##### Example 1

Using host metadata to distinguish between Linux and Windows hosts.

Say you would like the hosts to be autoregistered by the Zabbix server.
You have active Zabbix agents (see "Configuration" section above) on
your network. There are Windows hosts and Linux hosts on your network
and you have "Linux by Zabbix agent" and "Windows by Zabbix agent"
templates available in your Zabbix frontend. So at host registration,
you would like the appropriate Linux/Windows template to be applied to
the host being registered. By default, only the hostname is sent to the
server at autoregistration, which might not be enough. In order to make
sure the proper template is applied to the host you should use host
metadata.

[comment]: # ({/new-070aaeb4})

[comment]: # ({new-9543f562})
##### Frontend configuration

The first thing to do is to configure the frontend. Create 2 actions.
The first action:

-   Name: Linux host autoregistration
-   Conditions: Host metadata contains *Linux*
-   Operations: Link to templates: Linux

::: noteclassic
You can skip an "Add host" operation in this case. Linking
to a template requires adding a host first so the server will do that
automatically.
:::

The second action:

-   Name: Windows host autoregistration
-   Conditions: Host metadata contains *Windows*
-   Operations: Link to templates: Windows

[comment]: # ({/new-9543f562})

[comment]: # ({new-110dbc2b})
##### Agent configuration

Now you need to configure the agents. Add the next line to the agent
configuration files:

    HostMetadataItem=system.uname

This way you make sure host metadata will contain "Linux" or "Windows"
depending on the host an agent is running on. An example of host
metadata in this case:

    Linux: Linux server3 3.2.0-4-686-pae #1 SMP Debian 3.2.41-2 i686 GNU/Linux
    Windows: Windows WIN-0PXGGSTYNHO 6.0.6001 Windows Server 2008 Service Pack 1 Intel IA-32

Do not forget to restart the agent after making any changes to the
configuration file.

[comment]: # ({/new-110dbc2b})

[comment]: # ({new-85f695d6})
##### Example 2

*Step 1*

Using host metadata to allow some basic protection against unwanted
hosts registering.

[comment]: # ({/new-85f695d6})

[comment]: # ({new-c5ffa817})
##### Frontend configuration

Create an action in the frontend, using some hard-to-guess secret code
to disallow unwanted hosts:

-   Name: Autoregistration action Linux
-   Conditions:

```{=html}
<!-- -->
```
        * Type of calculation: AND
        * Condition (A): Host metadata contains //Linux//
        * Condition (B): Host metadata contains //21df83bf21bf0be663090bb8d4128558ab9b95fba66a6dbf834f8b91ae5e08ae//
    * Operations: 
        * Send message to users: Admin via all media
        * Add to host groups: Linux servers
        * Link to templates: Linux

Please note that this method alone does not provide strong protection
because data is transmitted in plain text. Configuration cache reload is
required for changes to have an immediate effect.

[comment]: # ({/new-c5ffa817})

[comment]: # ({new-9fca761d})
##### Agent configuration

Add the next line to the agent configuration file:

    HostMetadata=Linux    21df83bf21bf0be663090bb8d4128558ab9b95fba66a6dbf834f8b91ae5e08ae

where "Linux" is a platform, and the rest of the string is the
hard-to-guess secret text.

Do not forget to restart the agent after making any changes to the
configuration file.

*Step 2*

It is possible to add additional monitoring for an already registered
host.

[comment]: # ({/new-9fca761d})

[comment]: # ({new-6eb51339})
##### Frontend configuration

Update the action in the frontend:

-   Name: Autoregistration action Linux
-   Conditions:

```{=html}
<!-- -->
```
        * Type of calculation: AND
        * Condition (A): Host metadata contains Linux
        * Condition (B): Host metadata contains 21df83bf21bf0be663090bb8d4128558ab9b95fba66a6dbf834f8b91ae5e08ae
    * Operations:
        * Send message to users: Admin via all media
        * Add to host groups: Linux servers
        * Link to templates: Linux
        * Link to templates: MySQL by Zabbix Agent

[comment]: # ({/new-6eb51339})

[comment]: # ({new-15c86816})
##### Agent configuration

Update the next line in the agent configuration file:

    HostMetadata=MySQL on Linux 21df83bf21bf0be663090bb8d4128558ab9b95fba66a6dbf834f8b91ae5e08ae

Do not forget to restart the agent after making any changes to the
configuration file.

[comment]: # ({/new-15c86816})
