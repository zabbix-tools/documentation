[comment]: # translation:outdated

[comment]: # ({new-06931244})
# 5 What's new in Zabbix 7.0.0

[comment]: # ({/new-06931244})

[comment]: # ({new-e926bfb2})

### Breaking changes

Zabbix 7.0.0 is in development and does not have any breaking changes yet.

[comment]: # ({/new-e926bfb2})

[comment]: # ({new-dc0a44cf})

### Concurrency in network discovery

Previously each network discovery rule was processed by one discoverer process. Thus all service checks within the rule would be performed 
sequentially. 

In the new version the network discovery process has been reworked to allow concurrency between service checks. 
A new discovery manager process has been added along with a configurable number of discovery workers (or threads). The discovery manager 
processes discovery rules and creates a discovery job per each rule with tasks (service checks). The service checks are picked up and 
performed by the discovery workers. Only those checks that have the same IP and port are scheduled sequentially because 
some devices may not allow concurrent connections on the same port. A new zabbix[discovery_queue] internal item allows to monitor the number of discovery checks in the queue.

The [StartDiscoverers](/manual/appendix/config/zabbix_server#startdiscoverers) parameter now determines the total number of available discovery workers for discovery. The default number of StartDiscoverers has been upped from 1 to 5, and the range from 0-250 to 0-1000. The `discoverer` processes from previous Zabbix versions have been dropped.

Additionally, the number of available workers per each rule is now configurable in the [frontend](/manual/discovery/network_discovery/rule). This parameter is optional.

[comment]: # ({/new-dc0a44cf})

[comment]: # ({new-b6cdcf96})

### Items

The [item](/manual/config/items/itemtypes/zabbix_agent#system.hostnametypetransform) **system.hostname[]** can now return a Fully Qualified Domain Name,
if *fqdn* option is specified in the **type** item key parameter.

[comment]: # ({/new-b6cdcf96})

[comment]: # ({new-6b2dd9f4})
### Functions

[comment]: # ({/new-6b2dd9f4})

[comment]: # ({new-45142bff})
##### Aggregate calculations

Several aggregate functions have been updated. Now:

-   Aggregate functions **[count](/manual/appendix/functions/aggregate#count)** and **[count_foreach](/manual/appendix/functions/aggregate/foreach#additional-parameters)** support optional parameters *operator* and *pattern*, which can be used to fine-tune item filtering and only count values that match given criteria.
-   All [foreach functions](/appendix/functions/aggregate/foreach) no longer include unsupported items in the count.
-   The function **[last_foreach](/appendix/functions/aggregate/foreach#time-period)**, previously configured to ignore the time period argument, accepts it as an optional parameter.

[comment]: # ({/new-45142bff})

[comment]: # ({new-5f91b819})
### Remote commands on active agents

Starting with Zabbix agent 7.0, it is now possible to execute [remote commands](/manual/config/notifications/action/operation/remote_command) on an agent that is operating in active mode.
Once the execution of a remote command is triggered by an action [operation](/manual/config/notifications/action/operation#operations) or manual [script](/manual/web_interface/frontend_sections/alerts/scripts) execution,
the command will be included in the active checks configuration and executed once the active agent receives it.
Note that older active agents will ignore any remote commands included in the active checks configuration.
For more information, see [*Passive and active agent checks*](/manual/appendix/items/activepassive#active-checks).

[comment]: # ({/new-5f91b819})

[comment]: # ({new-1bc98669})

### Increased character limit for URL fields

The character limit for all URL fields is now 2048 characters.
This now includes: *Tile URL* for settings related to [geographical maps](/manual/web_interface/frontend_sections/administration/general#geographical-maps),
*Frontend URL* for configuring miscellaneous [frontend parameters](/manual/web_interface/frontend_sections/administration/general#other-parameters),
*URLs* for [network maps](/manual/config/visualization/maps/map#creating-a-map) and [network map elements](/manual/config/visualization/maps/map#adding-elements),
*URL A-C* for [host inventory](/manual/api/reference/host/object#host-inventory) fields,
and *URL* for the [URL dashboard widget](/manual/web_interface/frontend_sections/dashboards/widgets/url#url).

[comment]: # ({/new-1bc98669})

[comment]: # ({new-8a0c4fd0})
### Modal forms

The form for [module details](/manual/web_interface/frontend_sections/administration/general#modules) is now opened in a modal (pop-up) window.

[comment]: # ({/new-8a0c4fd0})

[comment]: # ({new-numeric})
### Support for the old numeric type dropped

The old style of floating point values, previously deprecated, is no longer supported, as numeric values of extended range are used.

[comment]: # ({/new-numeric})

[comment]: # ({new-7389886f})
### Cloning simplified

Previously it was possible to *Clone* and *Full clone* [hosts](/manual/config/hosts/host#overview), [templates](/manual/config/templates/template#creating-a-template) and [maps](/manual/config/visualization/maps/map#creating-a-map).

Now the option *Clone* has been removed, and the option *Full clone* has been renamed to *Clone* while still preserving all of the previous functionality of *Full clone*.

[comment]: # ({/new-7389886f})

[comment]: # ({new-7d92d0f0})
### Versions displayed

Zabbix frontend and Zabbix server version numbers are now viewable. Visit [System information page](/manual/web_interface/frontend_sections/reports/status_of_zabbix) to find out more.

[comment]: # ({/new-7d92d0f0})

[comment]: # ({new-4f802bf8})
### Expanded widget availability on template dashboards

Previously, on a [template dashboard](/manual/config/templates/template#adding-dashboards), you could create only the following widgets: *Clock*, *Graph (classic)*, *Graph prototype*, *Plain text*, *URL*.

Now template dashboards support the creation of all widgets.

[comment]: # ({/new-4f802bf8})

[comment]: # ({new-8442e72a})
### Collapsible advanced configuration

The *Advanced configuration* checkboxes, responsible for displaying advanced configuration options, have been replaced with collapsible blocks
(see, for example, [Connector configuration](/manual/config/export/streaming#configuration), [Service configuration](/manual/it_services/service_tree#service-configuration), [*Clock* widget configuration](/manual/web_interface/frontend_sections/dashboards/widgets/clock#configuration), etc.).
This improves user experience, as collapsing these blocks and saving the configuration will no longer reset the configured advanced options to their default values.

[comment]: # ({/new-8442e72a})

[comment]: # ({new-35832a7c})
### Frontend

[comment]: # ({/new-35832a7c})

[comment]: # ({new-347ec551})
##### Documentation link for each standard item

Each standard item now has a direct link from the frontend to its documentation page. 
The links are under the question mark icon, when opening the item helper from the item 
configuration form:

![](../../../assets/en/manual/introduction/item_helper.png)

[comment]: # ({/new-347ec551})

[comment]: # ({new-e4dba1ee})
##### Icons replaced by fonts

All icons in the frontend have been replaced by fonts.

[comment]: # ({/new-e4dba1ee})

[comment]: # ({new-f355ddd7})
### Miscellaneous

Default value of the [BufferSize](/manual/appendix/config/zabbix_agent2) configuration parameter for Zabbix agent 2 has been increased from 100 to 1000.

[comment]: # ({/new-f355ddd7})

[comment]: # ({new-325187ab})
New filtering option has been added to [Latest data](manual/web_interface/frontend_sections/monitoring/latest_data) section: it now allows you to filter items by their state (supported/unsupported).

[comment]: # ({/new-325187ab})


