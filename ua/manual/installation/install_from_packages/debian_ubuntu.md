[comment]: # translation:outdated

[comment]: # ({new-d96d3275})
# 2 Debian/Ubuntu/Raspbian

[comment]: # ({/new-d96d3275})


[comment]: # ({new-ae631a63})
### Notes on installation

See the [installation
instructions](https://www.zabbix.com/download?zabbix=6.0&os_distribution=debian&os_version=10_buster&db=mysql)
per platform in the download page for:

-   installing the repository
-   installing server/agent/frontend
-   creating initial database, importing initial data
-   configuring database for Zabbix server
-   configuring PHP for Zabbix frontend
-   starting server/agent processes
-   configuring Zabbix frontend

If you want to run Zabbix agent as root, see [running agent as
root](/manual/appendix/install/run_agent_as_root).

Zabbix web service process, which is used for [scheduled report
generation](/manual/web_interface/frontend_sections/reports/scheduled),
requires Google Chrome browser. The browser is not included into
packages and has to be installed manually.

[comment]: # ({/new-ae631a63})

[comment]: # ({new-99a402fa})
#### Importing data with Timescale DB

With TimescaleDB, in addition to the import command for PostgreSQL, also
run:

    # zcat /usr/share/doc/zabbix-sql-scripts/postgresql/timescaledb.sql.gz | sudo -u zabbix psql zabbix

::: notewarning
TimescaleDB is supported with Zabbix server
only.
:::

[comment]: # ({/new-99a402fa})


[comment]: # ({new-fa73411a})
#### SELinux configuration

See [SELinux
configuration](/manual/installation/install_from_packages/rhel_centos#selinux_configuration)
for RHEL/CentOS.

After the frontend and SELinux configuration is done, restart the Apache
web server:

    # service apache2 restart

[comment]: # ({/new-fa73411a})

[comment]: # ({new-a12da003})
### Proxy installation

Once the required repository is added, you can install Zabbix proxy by
running:

    # apt install zabbix-proxy-mysql

Substitute 'mysql' in the command with 'pgsql' to use PostgreSQL, or
with 'sqlite3' to use SQLite3.

[comment]: # ({/new-a12da003})

[comment]: # ({new-fe6abb8e})
##### Creating database

[Create](/manual/appendix/install/db_scripts) a separate database for
Zabbix proxy.

Zabbix server and Zabbix proxy cannot use the same database. If they are
installed on the same host, the proxy database must have a different
name.

[comment]: # ({/new-fe6abb8e})

[comment]: # ({new-2ab835d7})
##### Importing data

Import initial schema:

    # zcat /usr/share/doc/zabbix-sql-scripts/mysql/schema.sql.gz | mysql -uzabbix -p zabbix

For proxy with PostgreSQL (or SQLite):

    # zcat /usr/share/doc/zabbix-sql-scripts/postgresql/schema.sql.gz | sudo -u zabbix psql zabbix
    # zcat /usr/share/doc/zabbix-sql-scripts/sqlite3/schema.sql.gz | sqlite3 zabbix.db

[comment]: # ({/new-2ab835d7})

[comment]: # ({new-d0a225c7})
##### Configure database for Zabbix proxy

Edit zabbix\_proxy.conf:

    # vi /etc/zabbix/zabbix_proxy.conf
    DBHost=localhost
    DBName=zabbix
    DBUser=zabbix
    DBPassword=<password>

In DBName for Zabbix proxy use a separate database from Zabbix server.

In DBPassword use Zabbix database password for MySQL; PosgreSQL user
password for PosgreSQL.

Use `DBHost=` with PostgreSQL. You might want to keep the default
setting `DBHost=localhost` (or an IP address), but this would make
PostgreSQL use a network socket for connecting to Zabbix. Refer to the
[respective
section](/manual/installation/install_from_packages/rhel_centos#selinux_configuration)
for RHEL/CentOS for instructions.

[comment]: # ({/new-d0a225c7})

[comment]: # ({new-27de2ced})
##### Starting Zabbix proxy process

To start a Zabbix proxy process and make it start at system boot:

    # systemctl restart zabbix-proxy
    # systemctl enable zabbix-proxy

[comment]: # ({/new-27de2ced})

[comment]: # ({new-871a973b})
##### Frontend configuration

A Zabbix proxy does not have a frontend; it communicates with Zabbix
server only.

[comment]: # ({/new-871a973b})

[comment]: # ({new-cd9340bd})
### Java gateway installation

It is required to install [Java gateway](/manual/concepts/java) only if
you want to monitor JMX applications. Java gateway is lightweight and
does not require a database.

Once the required repository is added, you can install Zabbix Java
gateway by running:

    # apt install zabbix-java-gateway

Proceed to [setup](/manual/concepts/java/from_debian_ubuntu) for more
details on configuring and running Java gateway.

[comment]: # ({/new-cd9340bd})

[comment]: # ({new-9df568e9})
### Overview

Official Zabbix packages are available for:

|   |   |
|---|---|
|Debian 11 (Bullseye)|[Download](https://www.zabbix.com/download?zabbix=6.0&os_distribution=debian&os_version=11_bullseye&db=mysql)|
|Debian 10 (Buster)|[Download](https://www.zabbix.com/download?zabbix=6.0&os_distribution=debian&os_version=10_buster&db=mysql)|
|Debian 9 (Stretch)|[Download](https://www.zabbix.com/download?zabbix=6.0&os_distribution=debian&os_version=9_stretch&db=mysql)|
|Ubuntu 20.04 (Focal Fossa) LTS|[Download](https://www.zabbix.com/download?zabbix=6.0&os_distribution=ubuntu&os_version=20.04_focal&db=mysql)|
|Ubuntu 18.04 (Bionic Beaver) LTS|[Download](https://www.zabbix.com/download?zabbix=6.0&os_distribution=ubuntu&os_version=18.04_bionic&db=mysql)|
|Ubuntu 16.04 (Xenial Xerus) LTS|[Download](https://www.zabbix.com/download?zabbix=6.0&os_distribution=ubuntu&os_version=16.04_xenial&db=mysql)|
|Ubuntu 14.04 (Trusty Tahr) LTS|[Download](https://www.zabbix.com/download?zabbix=6.0&os_distribution=ubuntu&os_version=14.04_trusty&db=mysql)|
|Raspbian 11 (Bullseye)|[Download](https://www.zabbix.com/download?zabbix=6.0&os_distribution=raspberry_pi_os&os_version=11_bullseye&db=mysql)|
|Raspbian 10 (Buster)|[Download](https://www.zabbix.com/download?zabbix=6.0&os_distribution=raspberry_pi_os&os_version=10_buster&db=mysql)|
|Raspbian 9 (Stretch)|[Download](https://www.zabbix.com/download?zabbix=6.0&os_distribution=raspberry_pi_os&os_version=9_stretch&db=mysql)|

Packages are available with either MySQL/PostgreSQL database and
Apache/Nginx webserver support.

[comment]: # ({/new-9df568e9})
