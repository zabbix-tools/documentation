[comment]: # translation:outdated

[comment]: # ({new-51abb0f6})
# 3 SUSE Linux Enterprise Server

[comment]: # ({/new-51abb0f6})

[comment]: # ({new-ee73b6da})
### Overview

Official Zabbix packages are available for:

|   |   |
|---|---|
|SUSE Linux Enterprise Server 15|[Download](https://www.zabbix.com/download?zabbix=6.0&os_distribution=suse_linux_enterprise_server&os_version=15&db=mysql)|
|SUSE Linux Enterprise Server 12|[Download](https://www.zabbix.com/download?zabbix=6.0&os_distribution=suse_linux_enterprise_server&os_version=12&db=mysql)|

::: noteclassic
 *Verify CA* [encryption
mode](/manual/appendix/install/db_encrypt/mysql) doesn't work on SLES 12
(all minor OS versions) with MySQL due to older MySQL libraries.

:::

[comment]: # ({/new-ee73b6da})

[comment]: # ({new-37fbf2db})
### Adding Zabbix repository

Install the repository configuration package. This package contains yum
(software package manager) configuration files.

SLES 15:

    # rpm -Uvh --nosignature https://repo.zabbix.com/zabbix/6.0/sles/15/x86_64/zabbix-release-6.0-1.sles15.noarch.rpm
    # zypper --gpg-auto-import-keys refresh 'Zabbix Official Repository' 

SLES 12:

    # rpm -Uvh --nosignature https://repo.zabbix.com/zabbix/6.0/sles/12/x86_64/zabbix-release-6.0-1.sles12.noarch.rpm
    # zypper --gpg-auto-import-keys refresh 'Zabbix Official Repository' 

Please note, that Zabbix web service process, which is used for
[scheduled report
generation](/manual/web_interface/frontend_sections/reports/scheduled),
requires Google Chrome browser. The browser is not included into
packages and has to be installed manually.

[comment]: # ({/new-37fbf2db})

[comment]: # ({new-ef4f3e50})
### Server/frontend/agent installation

To install Zabbix server/frontend/agent with MySQL support:

    # zypper install zabbix-server-mysql zabbix-web-mysql zabbix-apache-conf zabbix-agent

Substitute 'apache' in the command with 'nginx' if using the package for
Nginx web server. See also: [Nginx setup for Zabbix on SLES
12/15](/manual/appendix/install/nginx).

Substitute 'zabbix-agent' with 'zabbix-agent2' in these commands if
using Zabbix agent 2 (only SLES 15 SP1+).

To install Zabbix proxy with MySQL support:

    # zypper install zabbix-proxy-mysql

Substitute 'mysql' in the commands with 'pgsql' to use PostgreSQL.

[comment]: # ({/new-ef4f3e50})

[comment]: # ({new-c573e862})
#### Creating database

For Zabbix [server](/manual/concepts/server) and
[proxy](/manual/concepts/proxy) daemons a database is required. It is
not needed to run Zabbix [agent](/manual/concepts/agent).

::: notewarning
Separate databases are needed for Zabbix server and
Zabbix proxy; they cannot use the same database. Therefore, if they are
installed on the same host, their databases must be created with
different names!
:::

Create the database using the provided instructions for
[MySQL](/manual/appendix/install/db_scripts#mysql) or
[PostgreSQL](/manual/appendix/install/db_scripts#postgresql).

[comment]: # ({/new-c573e862})

[comment]: # ({new-35bc057d})
#### Importing data

Now import initial schema and data for the **server** with MySQL:

    # zcat /usr/share/doc/packages/zabbix-sql-scripts/mysql/create.sql.gz | mysql -uzabbix -p zabbix

You will be prompted to enter your newly created database password.

With PostgreSQL:

    # zcat /usr/share/doc/packages/zabbix-sql-scripts/postgresql/create.sql.gz | sudo -u zabbix psql zabbix

With TimescaleDB, in addition to the previous command, also run:

    # zcat /usr/share/doc/packages/zabbix-sql-scripts/postgresql/timescaledb.sql.gz | sudo -u <username> psql zabbix

::: notewarning
TimescaleDB is supported with Zabbix server
only.
:::

For proxy, import initial schema:

    # zcat /usr/share/doc/packages/zabbix-sql-scripts/mysql/schema.sql.gz | mysql -uzabbix -p zabbix

For proxy with PostgreSQL:

    # zcat /usr/share/doc/packages/zabbix-sql-scripts/postgresql/schema.sql.gz | sudo -u zabbix psql zabbix

[comment]: # ({/new-35bc057d})

[comment]: # ({new-0ea127cd})
#### Configure database for Zabbix server/proxy

Edit /etc/zabbix/zabbix\_server.conf (and zabbix\_proxy.conf) to use
their respective databases. For example:

    # vi /etc/zabbix/zabbix_server.conf
    DBHost=localhost
    DBName=zabbix
    DBUser=zabbix
    DBPassword=<password>

In DBPassword use Zabbix database password for MySQL; PosgreSQL user
password for PosgreSQL.

Use `DBHost=` with PostgreSQL. You might want to keep the default
setting `DBHost=localhost` (or an IP address), but this would make
PostgreSQL use a network socket for connecting to Zabbix.

[comment]: # ({/new-0ea127cd})

[comment]: # ({new-d162545b})
#### Zabbix frontend configuration

Depending on the web server used (Apache/Nginx) edit the corresponding
configuration file for Zabbix frontend:

-   For Apache the configuration file is located in
    `/etc/apache2/conf.d/zabbix.conf`. Some PHP settings are already
    configured. But it's necessary to uncomment the "date.timezone"
    setting and [set the right
    timezone](http://php.net/manual/en/timezones.php) for you.

```{=html}
<!-- -->
```
    php_value max_execution_time 300
    php_value memory_limit 128M
    php_value post_max_size 16M
    php_value upload_max_filesize 2M
    php_value max_input_time 300
    php_value max_input_vars 10000
    php_value always_populate_raw_post_data -1
    # php_value date.timezone Europe/Riga

-   The zabbix-nginx-conf package installs a separate Nginx server for
    Zabbix frontend. Its configuration file is located in
    `/etc/nginx/conf.d/zabbix.conf`. For Zabbix frontend to work, it's
    necessary to uncomment and set `listen` and/or `server_name`
    directives.

```{=html}
<!-- -->
```
    # listen 80;
    # server_name example.com;

-   Zabbix uses its own dedicated php-fpm connection pool with Nginx:

Its configuration file is located in
`/etc/php7/fpm/php-fpm.d/zabbix.conf`. Some PHP settings are already
configured. But it's necessary to set the right
[date.timezone](http://php.net/manual/en/timezones.php) setting for you.

    php_value[max_execution_time] = 300
    php_value[memory_limit] = 128M
    php_value[post_max_size] = 16M
    php_value[upload_max_filesize] = 2M
    php_value[max_input_time] = 300
    php_value[max_input_vars] = 10000
    ; php_value[date.timezone] = Europe/Riga

Now you are ready to proceed with [frontend installation
steps](/manual/installation/install#installing_frontend) which will
allow you to access your newly installed Zabbix.

Note that a Zabbix proxy does not have a frontend; it communicates with
Zabbix server only.

[comment]: # ({/new-d162545b})

[comment]: # ({new-71fda19d})
#### Starting Zabbix server/agent process

Start Zabbix server and agent processes and make it start at system
boot.

With Apache web server:

    # systemctl restart zabbix-server zabbix-agent apache2 php-fpm
    # systemctl enable zabbix-server zabbix-agent apache2 php-fpm

Substitute 'apache2' with 'nginx' for Nginx web server.

[comment]: # ({/new-71fda19d})

[comment]: # ({new-8e553867})
### Installing debuginfo packages

To enable debuginfo repository edit */etc/zypp/repos.d/zabbix.repo*
file. Change `enabled=0` to `enabled=1` for zabbix-debuginfo repository.

    [zabbix-debuginfo]
    name=Zabbix Official Repository debuginfo
    type=rpm-md
    baseurl=http://repo.zabbix.com/zabbix/4.5/sles/15/x86_64/debuginfo/
    gpgcheck=1
    gpgkey=http://repo.zabbix.com/zabbix/4.5/sles/15/x86_64/debuginfo/repodata/repomd.xml.key
    enabled=0
    update=1

This will allow you to install zabbix-***<component>***-debuginfo
packages.

[comment]: # ({/new-8e553867})
