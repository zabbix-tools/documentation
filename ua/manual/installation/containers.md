[comment]: # translation:outdated

[comment]: # ({new-e62e1497})
# 5 Installation from containers

[comment]: # ({/new-e62e1497})

[comment]: # ({new-a3ac452e})

### Overview

This section describes how to deploy Zabbix with [Docker](#docker) or [Docker Compose](#docker-compose).

Zabbix officially provides:

- Separate Docker images for each Zabbix
component to run as portable and self-sufficient containers.
- Compose files for defining and running
multi-container Zabbix components in Docker.

[comment]: # ({/new-a3ac452e})

[comment]: # ({new-f9acea87})
### Docker

Zabbix provides [Docker](https://www.docker.com) images for each Zabbix
component as portable and self-sufficient containers to speed up
deployment and update procedure.

Zabbix components come with MySQL and PostgreSQL database support,
Apache2 and Nginx web server support. These images are separated into
different images.

[comment]: # ({/new-f9acea87})

[comment]: # ({new-e498c2aa})

#### Tags

Official Zabbix component images may contain the following tags:

|Tag|Description|Example|
|--|----------|----|
|latest            | The latest stable version of a Zabbix component based on Alpine Linux image. |zabbix-agent:latest |
|<OS>-trunk | The latest nightly build of the Zabbix version that is currently being developed on a specific operating system. <br> <br> **<OS>** - the base operating system. Supported values: <br> *alpine* - Alpine Linux; <br> *ltsc2019* -  Windows 10 LTSC 2019 (agent only); <br> *ol* - Oracle Linux; <br> *ltsc2022* - Windows 11 LTSC 2022 (agent only); <br> *ubuntu* - Ubuntu  |zabbix agent:ubuntu-trunk |
|<OS>-latest     | The latest stable version of a Zabbix component on a specific operating system. <br> <br> **<OS>** - the base operating system. Supported values:  <br> *alpine* - Alpine Linux; <br> *ltsc2019* -  Windows 10 LTSC 2019 (agent only); <br> *ol* - Oracle Linux; <br> *ltsc2022* - Windows 11 LTSC 2022 (agent only); <br> *ubuntu* - Ubuntu|zabbix-agent:ol-latest |
|<OS>-X.X-latest |The latest minor version of a Zabbix component of a specific major version and operating system. <br> <br> **<OS>** - the base operating system. Supported values:  <br> *alpine* - Alpine Linux; <br> *centos* - CentOS (only for Zabbix 4.0) <br> *ltsc2019* -  Windows 10 LTSC 2019 (agent only); <br> *ol* - Oracle Linux; <br> *ltsc2022* - Windows 11 LTSC 2022 (agent only); <br> *ubuntu* - Ubuntu<br><br>**X.X** - the major Zabbix version (supported: *4.0*, *5.0*, *6.0*, *6.2*). |zabbix-agent:alpine-6.0-latest |
|<OS>-X.X.*      |The latest minor version of a Zabbix component of a specific major version and operating system. <br> <br> **<OS>** - the base operating system. Supported values:  <br> *alpine* - Alpine Linux; <br> *centos* - CentOS (only for Zabbix 4.0) <br> *ltsc2019* -  Windows 10 LTSC 2019 (agent only); <br> *ol* - Oracle Linux; <br> *ltsc2022* - Windows 11 LTSC 2022 (agent only); <br> *ubuntu* - Ubuntu<br><br>**X.X** - the major Zabbix version (supported: *4.0*, *5.0*, *6.0*, *6.2*). <br><br> **\*** - the Zabbix minor version  |zabbix-agent:alpine-6.4.1|

[comment]: # ({/new-e498c2aa})


[comment]: # ({new-fba82ded})
#### Docker file sources

Everyone can follow Docker file changes using the Zabbix [official
repository](https://github.com/zabbix/zabbix-docker) on
[github.com](https://github.com/). You can fork the project or make your
own images based on official Docker files.

[comment]: # ({/new-fba82ded})



[comment]: # ({new-0b4b68c7})
#### Usage

[comment]: # ({/new-0b4b68c7})

[comment]: # ({new-de9f41d4})
##### Environment variables

All Zabbix component images provide environment variables to control
configuration. These environment variables are listed in each component
repository. These environment variables are options from Zabbix
configuration files, but with different naming method. For example,
`ZBX_LOGSLOWQUERIES` is equal to `LogSlowQueries` from Zabbix server and
Zabbix proxy configuration files.

::: noteimportant
 Some of configuration options are not allowed to
change. For example, `PIDFile` and `LogType`.
:::

Some of components have specific environment variables, which do not
exist in official Zabbix configuration files:

|   |   |   |
|---|---|---|
|**Variable**|**Components**|**Description**|
|`DB_SERVER_HOST`|Server<br>Proxy<br>Web interface|This variable is IP or DNS name of MySQL or PostgreSQL server.<br>By default, value is `mysql-server` or `postgres-server` for MySQL or PostgreSQL respectively|
|`DB_SERVER_PORT`|Server<br>Proxy<br>Web interface|This variable is port of MySQL or PostgreSQL server.<br>By default, value is '3306' or '5432' respectively.|
|`MYSQL_USER`|Server<br>Proxy<br>Web-interface|MySQL database user.<br>By default, value is 'zabbix'.|
|`MYSQL_PASSWORD`|Server<br>Proxy<br>Web interface|MySQL database password.<br>By default, value is 'zabbix'.|
|`MYSQL_DATABASE`|Server<br>Proxy<br>Web interface|Zabbix database name.<br>By default, value is 'zabbix' for Zabbix server and 'zabbix\_proxy' for Zabbix proxy.|
|`POSTGRES_USER`|Server<br>Web interface|PostgreSQL database user.<br>By default, value is 'zabbix'.|
|`POSTGRES_PASSWORD`|Server<br>Web interface|PostgreSQL database password.<br>By default, value is 'zabbix'.|
|`POSTGRES_DB`|Server<br>Web interface|Zabbix database name.<br>By default, value is 'zabbix' for Zabbix server and 'zabbix\_proxy' for Zabbix proxy.|
|`PHP_TZ`|Web-interface|Timezone in PHP format. Full list of supported timezones are available on [php.net](http://php.net/manual/en/timezones.php).<br>By default, value is 'Europe/Riga'.|
|`ZBX_SERVER_NAME`|Web interface|Visible Zabbix installation name in right top corner of the web interface.<br>By default, value is 'Zabbix Docker'|
|`ZBX_JAVAGATEWAY_ENABLE`|Server<br>Proxy|Enables communication with Zabbix Java gateway to collect Java related checks.<br>By default, value is "false"|
|`ZBX_ENABLE_SNMP_TRAPS`|Server<br>Proxy|Enables SNMP trap feature. It requires **zabbix-snmptraps** instance and shared volume */var/lib/zabbix/snmptraps* to Zabbix server or Zabbix proxy.|

[comment]: # ({/new-de9f41d4})

[comment]: # ({new-7d10f7dc})
##### Volumes

The images allow to use some mount points. These mount points are
different and depend on Zabbix component type:

|   |   |
|---|---|
|**Volume**|**Description**|
|**Zabbix agent**|<|
|*/etc/zabbix/zabbix\_agentd.d*|The volume allows to include *\*.conf* files and extend Zabbix agent using the `UserParameter` feature|
|*/var/lib/zabbix/modules*|The volume allows to load additional modules and extend Zabbix agent using the [LoadModule](/manual/config/items/loadablemodules) feature|
|*/var/lib/zabbix/enc*|The volume is used to store TLS-related files. These file names are specified using `ZBX_TLSCAFILE`, `ZBX_TLSCRLFILE`, `ZBX_TLSKEY_FILE` and `ZBX_TLSPSKFILE` environment variables|
|**Zabbix server**|<|
|*/usr/lib/zabbix/alertscripts*|The volume is used for custom alert scripts. It is the `AlertScriptsPath` parameter in [zabbix\_server.conf](/manual/appendix/config/zabbix_server)|
|*/usr/lib/zabbix/externalscripts*|The volume is used by [external checks](/manual/config/items/itemtypes/external). It is the `ExternalScripts` parameter in [zabbix\_server.conf](/manual/appendix/config/zabbix_server)|
|*/var/lib/zabbix/modules*|The volume allows to load additional modules and extend Zabbix server using the [LoadModule](/manual/config/items/loadablemodules) feature|
|*/var/lib/zabbix/enc*|The volume is used to store TLS related files. These file names are specified using `ZBX_TLSCAFILE`, `ZBX_TLSCRLFILE`, `ZBX_TLSKEY_FILE` and `ZBX_TLSPSKFILE` environment variables|
|*/var/lib/zabbix/ssl/certs*|The volume is used as location of SSL client certificate files for client authentication. It is the `SSLCertLocation` parameter in zabbix\_server.conf|
|*/var/lib/zabbix/ssl/keys*|The volume is used as location of SSL private key files for client authentication. It is the `SSLKeyLocation` parameter in [zabbix\_server.conf](/manual/appendix/config/zabbix_server)|
|*/var/lib/zabbix/ssl/ssl\_ca*|The volume is used as location of certificate authority (CA) files for SSL server certificate verification. It is the `SSLCALocation` parameter in [zabbix\_server.conf](/manual/appendix/config/zabbix_server)|
|*/var/lib/zabbix/snmptraps*|The volume is used as location of snmptraps.log file. It could be shared by zabbix-snmptraps container and inherited using the volumes\_from Docker option while creating a new instance of Zabbix server. SNMP trap processing feature could be enabled by using shared volume and switching the `ZBX_ENABLE_SNMP_TRAPS` environment variable to 'true'|
|*/var/lib/zabbix/mibs*|The volume allows to add new MIB files. It does not support subdirectories, all MIBs must be placed in `/var/lib/zabbix/mibs`|
|**Zabbix proxy**|<|
|*/usr/lib/zabbix/externalscripts*|The volume is used by [external checks](/manual/config/items/itemtypes/external). It is the `ExternalScripts` parameter in [zabbix\_proxy.conf](/manual/appendix/config/zabbix_proxy)|
|*/var/lib/zabbix/modules*|The volume allows to load additional modules and extend Zabbix server using the [LoadModule](/manual/config/items/loadablemodules) feature|
|*/var/lib/zabbix/enc*|The volume is used to store TLS related files. These file names are specified using `ZBX_TLSCAFILE`, `ZBX_TLSCRLFILE`, `ZBX_TLSKEY_FILE` and `ZBX_TLSPSKFILE` environment variables|
|*/var/lib/zabbix/ssl/certs*|The volume is used as location of SSL client certificate files for client authentication. It is the `SSLCertLocation` parameter in [zabbix\_proxy.conf](/manual/appendix/config/zabbix_proxy)|
|*/var/lib/zabbix/ssl/keys*|The volume is used as location of SSL private key files for client authentication. It is the `SSLKeyLocation` parameter in [zabbix\_proxy.conf](/manual/appendix/config/zabbix_proxy)|
|*/var/lib/zabbix/ssl/ssl\_ca*|The volume is used as location of certificate authority (CA) files for SSL server certificate verification. It is the `SSLCALocation` parameter in [zabbix\_proxy.conf](/manual/appendix/config/zabbix_proxy)|
|*/var/lib/zabbix/snmptraps*|The volume is used as location of snmptraps.log file. It could be shared by the zabbix-snmptraps container and inherited using the volumes\_from Docker option while creating a new instance of Zabbix server. SNMP trap processing feature could be enabled by using shared volume and switching the `ZBX_ENABLE_SNMP_TRAPS` environment variable to 'true'|
|*/var/lib/zabbix/mibs*|The volume allows to add new MIB files. It does not support subdirectories, all MIBs must be placed in `/var/lib/zabbix/mibs`|
|**Zabbix web interface based on Apache2 web server**|<|
|*/etc/ssl/apache2*|The volume allows to enable HTTPS for Zabbix web interface. The volume must contain the two `ssl.crt` and `ssl.key` files prepared for Apache2 SSL connections|
|**Zabbix web interface based on Nginx web server**|<|
|*/etc/ssl/nginx*|The volume allows to enable HTTPS for Zabbix web interface. The volume must contain the two `ssl.crt`, `ssl.key` files and `dhparam.pem` prepared for Nginx SSL connections|
|**Zabbix snmptraps**|<|
|*/var/lib/zabbix/snmptraps*|The volume contains the `snmptraps.log` log file named with received SNMP traps|
|*/var/lib/zabbix/mibs*|The volume allows to add new MIB files. It does not support subdirectories, all MIBs must be placed in `/var/lib/zabbix/mibs`|

For additional information use Zabbix official repositories in Docker
Hub.

[comment]: # ({/new-7d10f7dc})

[comment]: # ({new-492bd3ba})
##### Usage examples

**Example 1**

The example demonstrates how to run Zabbix server with MySQL database
support, Zabbix web interface based on the Nginx web server and Zabbix
Java gateway.

1\. Create network dedicated for Zabbix component containers:

    # docker network create --subnet 172.20.0.0/16 --ip-range 172.20.240.0/20 zabbix-net

2\. Start empty MySQL server instance

    # docker run --name mysql-server -t \
          -e MYSQL_DATABASE="zabbix" \
          -e MYSQL_USER="zabbix" \
          -e MYSQL_PASSWORD="zabbix_pwd" \
          -e MYSQL_ROOT_PASSWORD="root_pwd" \
          --network=zabbix-net \
          -d mysql:8.0 \
          --restart unless-stopped \
          --character-set-server=utf8 --collation-server=utf8_bin \
          --default-authentication-plugin=mysql_native_password

3\. Start Zabbix Java gateway instance

    # docker run --name zabbix-java-gateway -t \
          --network=zabbix-net \
          --restart unless-stopped \
          -d zabbix/zabbix-java-gateway:alpine-5.4-latest

4\. Start Zabbix server instance and link the instance with created
MySQL server instance

    # docker run --name zabbix-server-mysql -t \
          -e DB_SERVER_HOST="mysql-server" \
          -e MYSQL_DATABASE="zabbix" \
          -e MYSQL_USER="zabbix" \
          -e MYSQL_PASSWORD="zabbix_pwd" \
          -e MYSQL_ROOT_PASSWORD="root_pwd" \
          -e ZBX_JAVAGATEWAY="zabbix-java-gateway" \
          --network=zabbix-net \
          -p 10051:10051 \
          --restart unless-stopped \
          -d zabbix/zabbix-server-mysql:alpine-5.4-latest

::: noteclassic
Zabbix server instance exposes 10051/TCP port (Zabbix
trapper) to host machine.
:::

5\. Start Zabbix web interface and link the instance with created MySQL
server and Zabbix server instances

    # docker run --name zabbix-web-nginx-mysql -t \
          -e ZBX_SERVER_HOST="zabbix-server-mysql" \
          -e DB_SERVER_HOST="mysql-server" \
          -e MYSQL_DATABASE="zabbix" \
          -e MYSQL_USER="zabbix" \
          -e MYSQL_PASSWORD="zabbix_pwd" \
          -e MYSQL_ROOT_PASSWORD="root_pwd" \
          --network=zabbix-net \
          -p 80:8080 \
          --restart unless-stopped \
          -d zabbix/zabbix-web-nginx-mysql:alpine-5.4-latest

::: noteclassic
Zabbix web interface instance exposes 80/TCP port (HTTP) to
host machine.
:::

**Example 2**

The example demonstrates how to run Zabbix server with PostgreSQL
database support, Zabbix web interface based on the Nginx web server and
SNMP trap feature.

1\. Create network dedicated for Zabbix component containers:

    # docker network create --subnet 172.20.0.0/16 --ip-range 172.20.240.0/20 zabbix-net

2\. Start empty PostgreSQL server instance

    # docker run --name postgres-server -t \
          -e POSTGRES_USER="zabbix" \
          -e POSTGRES_PASSWORD="zabbix_pwd" \
          -e POSTGRES_DB="zabbix" \
          --network=zabbix-net \
          --restart unless-stopped \
          -d postgres:latest

3\. Start Zabbix snmptraps instance

    # docker run --name zabbix-snmptraps -t \
          -v /zbx_instance/snmptraps:/var/lib/zabbix/snmptraps:rw \
          -v /var/lib/zabbix/mibs:/usr/share/snmp/mibs:ro \
          --network=zabbix-net \
          -p 162:1162/udp \
          --restart unless-stopped \
          -d zabbix/zabbix-snmptraps:alpine-5.4-latest

::: noteclassic
Zabbix snmptrap instance exposes the 162/UDP port (SNMP
traps) to host machine.
:::

4\. Start Zabbix server instance and link the instance with created
PostgreSQL server instance

    # docker run --name zabbix-server-pgsql -t \
          -e DB_SERVER_HOST="postgres-server" \
          -e POSTGRES_USER="zabbix" \
          -e POSTGRES_PASSWORD="zabbix_pwd" \
          -e POSTGRES_DB="zabbix" \
          -e ZBX_ENABLE_SNMP_TRAPS="true" \
          --network=zabbix-net \
          -p 10051:10051 \
          --volumes-from zabbix-snmptraps \
          --restart unless-stopped \
          -d zabbix/zabbix-server-pgsql:alpine-5.4-latest

::: noteclassic
Zabbix server instance exposes the 10051/TCP port (Zabbix
trapper) to host machine.
:::

5\. Start Zabbix web interface and link the instance with created
PostgreSQL server and Zabbix server instances

    # docker run --name zabbix-web-nginx-pgsql -t \
          -e ZBX_SERVER_HOST="zabbix-server-pgsql" \
          -e DB_SERVER_HOST="postgres-server" \
          -e POSTGRES_USER="zabbix" \
          -e POSTGRES_PASSWORD="zabbix_pwd" \
          -e POSTGRES_DB="zabbix" \
          --network=zabbix-net \
          -p 443:8443 \
          -p 80:8080 \
          -v /etc/ssl/nginx:/etc/ssl/nginx:ro \
          --restart unless-stopped \
          -d zabbix/zabbix-web-nginx-pgsql:alpine-5.4-latest

::: noteclassic
Zabbix web interface instance exposes the 443/TCP port
(HTTPS) to host machine.\
Directory */etc/ssl/nginx* must contain certificate with required
name.
:::

**Example 3**

The example demonstrates how to run Zabbix server with MySQL database
support, Zabbix web interface based on the Nginx web server and Zabbix
Java gateway using `podman` on Red Hat 8.

1\. Create new pod with name `zabbix` and exposed ports (web-interface,
Zabbix server trapper):

    podman pod create --name zabbix -p 80:8080 -p 10051:10051

2\. (optional) Start Zabbix agent container in `zabbix` pod location:

    podman run --name zabbix-agent \
        -eZBX_SERVER_HOST="127.0.0.1,localhost" \
        --restart=always \
        --pod=zabbix \
        -d registry.connect.redhat.com/zabbix/zabbix-agent-50:latest

3\. Create `./mysql/` directory on host and start Oracle MySQL server
8.0:

    podman run --name mysql-server -t \
          -e MYSQL_DATABASE="zabbix" \
          -e MYSQL_USER="zabbix" \
          -e MYSQL_PASSWORD="zabbix_pwd" \
          -e MYSQL_ROOT_PASSWORD="root_pwd" \
          -v ./mysql/:/var/lib/mysql/:Z \
          --restart=always \
          --pod=zabbix \
          -d mysql:8.0 \
          --character-set-server=utf8 --collation-server=utf8_bin \
          --default-authentication-plugin=mysql_native_password

3\. Start Zabbix server container:

    podman run --name zabbix-server-mysql -t \
                      -e DB_SERVER_HOST="127.0.0.1" \
                      -e MYSQL_DATABASE="zabbix" \
                      -e MYSQL_USER="zabbix" \
                      -e MYSQL_PASSWORD="zabbix_pwd" \
                      -e MYSQL_ROOT_PASSWORD="root_pwd" \
                      -e ZBX_JAVAGATEWAY="127.0.0.1" \
                      --restart=always \
                      --pod=zabbix \
                      -d registry.connect.redhat.com/zabbix/zabbix-server-mysql-50

4\. Start Zabbix Java Gateway container:

    podman run --name zabbix-java-gateway -t \
          --restart=always \
          --pod=zabbix \
          -d registry.connect.redhat.com/zabbix/zabbix-java-gateway-50

5\. Start Zabbix web-interface container:

    podman run --name zabbix-web-mysql -t \
                      -e ZBX_SERVER_HOST="127.0.0.1" \
                      -e DB_SERVER_HOST="127.0.0.1" \
                      -e MYSQL_DATABASE="zabbix" \
                      -e MYSQL_USER="zabbix" \
                      -e MYSQL_PASSWORD="zabbix_pwd" \
                      -e MYSQL_ROOT_PASSWORD="root_pwd" \
                      --restart=always \
                      --pod=zabbix \
                      -d registry.connect.redhat.com/zabbix/zabbix-web-mysql-50

::: noteclassic
Pod `zabbix` exposes 80/TCP port (HTTP) to host machine from
8080/TCP of `zabbix-web-mysql` container.
:::

[comment]: # ({/new-492bd3ba})

[comment]: # ({new-c443c22e})
### Docker Compose

Zabbix provides compose files also for defining and running
multi-container Zabbix components in Docker. These compose files are
available in Zabbix docker official repository on github.com:
<https://github.com/zabbix/zabbix-docker>. These compose files are added
as examples, they are overloaded. For example, they contain proxies with
MySQL and SQLite3 support.

There are a few different versions of compose files:

|   |   |
|---|---|
|**File name**|**Description**|
|`docker-compose_v3_alpine_mysql_latest.yaml`|The compose file runs the latest version of Zabbix 5.4 components on Alpine Linux with MySQL database support.|
|`docker-compose_v3_alpine_mysql_local.yaml`|The compose file locally builds the latest version of Zabbix 5.4 and runs Zabbix components on Alpine Linux with MySQL database support.|
|`docker-compose_v3_alpine_pgsql_latest.yaml`|The compose file runs the latest version of Zabbix 5.4 components on Alpine Linux with PostgreSQL database support.|
|`docker-compose_v3_alpine_pgsql_local.yaml`|The compose file locally builds the latest version of Zabbix 5.4 and runs Zabbix components on Alpine Linux with PostgreSQL database support.|
|`docker-compose_v3_centos_mysql_latest.yaml`|The compose file runs the latest version of Zabbix 5.4 components on CentOS 8 with MySQL database support.|
|`docker-compose_v3_centos_mysql_local.yaml`|The compose file locally builds the latest version of Zabbix 5.4 and runs Zabbix components on CentOS 8 with MySQL database support.|
|`docker-compose_v3_centos_pgsql_latest.yaml`|The compose file runs the latest version of Zabbix 5.4 components on CentOS 8 with PostgreSQL database support.|
|`docker-compose_v3_centos_pgsql_local.yaml`|The compose file locally builds the latest version of Zabbix 5.4 and runs Zabbix components on CentOS 8 with PostgreSQL database support.|
|`docker-compose_v3_ubuntu_mysql_latest.yaml`|The compose file runs the latest version of Zabbix 5.4 components on Ubuntu 20.04 with MySQL database support.|
|`docker-compose_v3_ubuntu_mysql_local.yaml`|The compose file locally builds the latest version of Zabbix 5.4 and runs Zabbix components on Ubuntu 20.04 with MySQL database support.|
|`docker-compose_v3_ubuntu_pgsql_latest.yaml`|The compose file runs the latest version of Zabbix 5.4 components on Ubuntu 20.04 with PostgreSQL database support.|
|`docker-compose_v3_ubuntu_pgsql_local.yaml`|The compose file locally builds the latest version of Zabbix 5.4 and runs Zabbix components on Ubuntu 20.04 with PostgreSQL database support.|

::: noteimportant
Available Docker compose files support version 3
of Docker Compose.
:::

[comment]: # ({/new-c443c22e})

[comment]: # ({new-52e39127})
#### Storage

Compose files are configured to support local storage on a host machine.
Docker Compose will create a `zbx_env` directory in the folder with the
compose file when you run Zabbix components using the compose file. The
directory will contain the same structure as described above in the
[Volumes](#Volumes) section and directory for database storage.

There are also volumes in read-only mode for `/etc/localtime` and
`/etc/timezone` files.

[comment]: # ({/new-52e39127})

[comment]: # ({new-0be3c140})
#### Environment files

In the same directory with compose files on github.com you can find
files with default environment variables for each component in compose
file. These environment files are named like `.env_<type of component>`.

[comment]: # ({/new-0be3c140})

[comment]: # ({new-ad55959c})
#### Examples

**Example 1**

    # git checkout 5.4
    # docker-compose -f ./docker-compose_v3_alpine_mysql_latest.yaml up -d

The command will download latest Zabbix 5.4 images for each Zabbix
component and run them in detach mode.

::: noteimportant
Do not forget to download
`.env_<type of component>` files from github.com official Zabbix
repository with compose files.
:::

**Example 2**

    # git checkout 5.4
    # docker-compose -f ./docker-compose_v3_ubuntu_mysql_local.yaml up -d

The command will download base image Ubuntu 20.04 (focal), then build
Zabbix 5.4 components locally and run them in detach mode.

[comment]: # ({/new-ad55959c})
