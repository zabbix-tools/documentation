[comment]: # translation:outdated

[comment]: # ({new-91aab3e5})
# 2 Debian/Ubuntu

[comment]: # ({/new-91aab3e5})

[comment]: # ({new-d02acea9})
#### Overview

This section provides the steps required for a successful
[upgrade](/manual/installation/upgrade) from Zabbix **5.4**.x to Zabbix
**6.0**.x using official Zabbix packages for Debian/Ubuntu.

While upgrading Zabbix agents is not mandatory (but recommended), Zabbix
server and proxies must be of the [same major
version](/manual/appendix/compatibility). Therefore, in a server-proxy
setup, Zabbix server and all proxies have to be stopped and upgraded.
Keeping proxies running during server upgrade no longer will bring any
benefit as during proxy upgrade their old data will be discarded and no
new data will be gathered until proxy configuration is synced with
server.

Note that with SQLite database on proxies, history data from proxies
before the upgrade will be lost, because SQLite database upgrade is not
supported and the SQLite database file has to be manually removed. When
proxy is started for the first time and the SQLite database file is
missing, proxy creates it automatically.

Depending on database size the database upgrade to version 6.0 may take
a long time.

::: notewarning
Before the upgrade make sure to read the relevant
**upgrade notes!**
:::

The following upgrade notes are available:

|Upgrade from|Read full upgrade notes|Most important changes between versions|
|------------|-----------------------|---------------------------------------|
|5.4.x|For:<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|<|
|5.2.x|For:<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|Minimum required database versions upped;<br>Aggregate items removed as a separate type.|
|5.0.x|For:<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|Minimum required PHP version upped from 7.2.0 to 7.2.5.|
|4.4.x|For:<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)|Support of IBM DB2 dropped;<br>Minimum required PHP version upped from 5.4.0 to 7.2.0;<br>Minimum required database versions upped;<br>Changed Zabbix PHP file directory.|
|4.2.x|For:<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.4/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|Jabber, Ez Texting media types removed.|
|4.0.x LTS|For:<br>Zabbix [4.2](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_420)<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.4/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|Older proxies no longer can report data to an upgraded server;<br>Newer agents no longer will be able to work with an older Zabbix server.|
|3.4.x|For:<br>Zabbix [4.0](https://www.zabbix.com/documentation/4.0/manual/installation/upgrade_notes_400)<br>Zabbix [4.2](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_420)<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.4/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|'libpthread' and 'zlib' libraries now mandatory;<br>Support for plain text protocol dropped and header is mandatory;<br>Pre-1.4 version Zabbix agents are no longer supported;<br>The Server parameter in passive proxy configuration now mandatory.|
|3.2.x|For:<br>Zabbix [3.4](https://www.zabbix.com/documentation/3.4/manual/installation/upgrade_notes_340)<br>Zabbix [4.0](https://www.zabbix.com/documentation/4.0/manual/installation/upgrade_notes_400)<br>Zabbix [4.2](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_420)<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.4/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|SQLite support as backend database dropped for Zabbix server/frontend;<br>Perl Compatible Regular Expressions (PCRE) supported instead of POSIX extended;<br>'libpcre' and 'libevent' libraries mandatory for Zabbix server;<br>Exit code checks added for user parameters, remote commands and system.run\[\] items without the 'nowait' flag as well as Zabbix server executed scripts;<br>Zabbix Java gateway has to be upgraded to support new functionality.|
|3.0.x LTS|For:<br>Zabbix [3.2](https://www.zabbix.com/documentation/3.2/manual/installation/upgrade_notes_320)<br>Zabbix [3.4](https://www.zabbix.com/documentation/3.4/manual/installation/upgrade_notes_340)<br>Zabbix [4.0](https://www.zabbix.com/documentation/4.0/manual/installation/upgrade_notes_400)<br>Zabbix [4.2](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_420)<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.4/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|Database upgrade may be slow, depending on the history table size.|
|2.4.x|For:<br>Zabbix [3.0](https://www.zabbix.com/documentation/3.0/manual/installation/upgrade_notes_300)<br>Zabbix [3.2](https://www.zabbix.com/documentation/3.2/manual/installation/upgrade_notes_320)<br>Zabbix [3.4](https://www.zabbix.com/documentation/3.4/manual/installation/upgrade_notes_340)<br>Zabbix [4.0](https://www.zabbix.com/documentation/4.0/manual/installation/upgrade_notes_400)<br>Zabbix [4.2](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_420)<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|Minimum required PHP version upped from 5.3.0 to 5.4.0<br>LogFile agent parameter must be specified|
|2.2.x LTS|For:<br>Zabbix [2.4](https://www.zabbix.com/documentation/2.4/manual/installation/upgrade_notes_240)<br>Zabbix [3.0](https://www.zabbix.com/documentation/3.0/manual/installation/upgrade_notes_300)<br>Zabbix [3.2](https://www.zabbix.com/documentation/3.2/manual/installation/upgrade_notes_320)<br>Zabbix [3.4](https://www.zabbix.com/documentation/3.4/manual/installation/upgrade_notes_340)<br>Zabbix [4.0](https://www.zabbix.com/documentation/4.0/manual/installation/upgrade_notes_400)<br>Zabbix [4.2](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_420)<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|Node-based distributed monitoring removed|
|2.0.x|For:<br>Zabbix [2.2](https://www.zabbix.com/documentation/2.2/manual/installation/upgrade_notes_220)<br>Zabbix [2.4](https://www.zabbix.com/documentation/2.4/manual/installation/upgrade_notes_240)<br>Zabbix [3.0](https://www.zabbix.com/documentation/3.0/manual/installation/upgrade_notes_300)<br>Zabbix [3.2](https://www.zabbix.com/documentation/3.2/manual/installation/upgrade_notes_320)<br>Zabbix [3.4](https://www.zabbix.com/documentation/3.4/manual/installation/upgrade_notes_340)<br>Zabbix [4.0](https://www.zabbix.com/documentation/4.0/manual/installation/upgrade_notes_400)<br>Zabbix [4.2](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_420)<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|Minimum required PHP version upped from 5.1.6 to 5.3.0;<br>Case-sensitive MySQL database required for proper server work; character set utf8 and utf8\_bin collation is required for Zabbix server to work properly with MySQL database. See [database creation scripts](/manual/appendix/install/db_scripts#mysql).<br>'mysqli' PHP extension required instead of 'mysql'|

You may also want to check the
[requirements](/manual/installation/requirements) for 6.0.

::: notetip
It may be handy to run two parallel SSH sessions during
the upgrade, executing the upgrade steps in one and monitoring the
server/proxy logs in another. For example, run
`tail -f zabbix_server.log` or `tail -f zabbix_proxy.log` in the second
SSH session showing you the latest log file entries and possible errors
in real time. This can be critical for production
instances.
:::

[comment]: # ({/new-d02acea9})

[comment]: # ({new-93ff8b03})
#### Upgrade procedure

[comment]: # ({/new-93ff8b03})

[comment]: # ({new-f8102233})
##### 1 Stop Zabbix processes

Stop Zabbix server to make sure that no new data is inserted into
database.

    # service zabbix-server stop

If upgrading Zabbix proxy, stop proxy too.

    # service zabbix-proxy stop

[comment]: # ({/new-f8102233})

[comment]: # ({new-ab13a6a4})
##### 2 Back up the existing Zabbix database

This is a very important step. Make sure that you have a backup of your
database. It will help if the upgrade procedure fails (lack of disk
space, power off, any unexpected problem).

[comment]: # ({/new-ab13a6a4})

[comment]: # ({new-6c141723})
##### 3 Back up configuration files, PHP files and Zabbix binaries

Make a backup copy of Zabbix binaries, configuration files and the PHP
file directory.

Configuration files:

    # mkdir /opt/zabbix-backup/
    # cp /etc/zabbix/zabbix_server.conf /opt/zabbix-backup/
    # cp /etc/apache2/conf-enabled/zabbix.conf /opt/zabbix-backup/

PHP files and Zabbix binaries:

    # cp -R /usr/share/zabbix/ /opt/zabbix-backup/
    # cp -R /usr/share/doc/zabbix-* /opt/zabbix-backup/

[comment]: # ({/new-6c141723})

[comment]: # ({new-31c75b55})
##### 4 Update repository configuration package

To proceed with the update your current repository package has to be
uninstalled.

    # rm -Rf /etc/apt/sources.list.d/zabbix.list

Then install the new repository configuration package.

On **Debian 10** run:

    # wget https://repo.zabbix.com/zabbix/6.0/debian/pool/main/z/zabbix-release/zabbix-release_6.0-1+debian10_all.deb
    # dpkg -i zabbix-release_5.4-1+debian10_all.deb

On **Debian 9** run:

    # wget https://repo.zabbix.com/zabbix/6.0/debian/pool/main/z/zabbix-release/zabbix-release_6.0-1+debian9_all.deb
    # dpkg -i zabbix-release_5.4-1+debian9_all.deb

On **Debian 8** run:

    # wget https://repo.zabbix.com/zabbix/6.0/debian/pool/main/z/zabbix-release/zabbix-release_6.0-1+debian8_all.deb
    # dpkg -i zabbix-release_5.4-1+debian8_all.deb

On **Ubuntu 20.04** run:

    # wget https://repo.zabbix.com/zabbix/5.4/ubuntu/pool/main/z/zabbix-release/zabbix-release_6.0-1+ubuntu20.04_all.deb
    # dpkg -i zabbix-release_5.4-1+ubuntu20.04_all.deb

On **Ubuntu 18.04** run:

    # wget https://repo.zabbix.com/zabbix/5.4/ubuntu/pool/main/z/zabbix-release/zabbix-release_6.0-1+ubuntu18.04_all.deb
    # dpkg -i zabbix-release_6.0-1+ubuntu18.04_all.deb

On **Ubuntu 16.04** run:

    # wget https://repo.zabbix.com/zabbix/5.4/ubuntu/pool/main/z/zabbix-release/zabbix-release_6.0-1+ubuntu16.04_all.deb
    # dpkg -i zabbix-release_5.4-1+ubuntu16.04_all.deb

On **Ubuntu 14.04** run:

    # wget https://repo.zabbix.com/zabbix/5.4/ubuntu/pool/main/z/zabbix-release/zabbix-release_6.0-1+ubuntu14.04_all.deb
    # dpkg -i zabbix-release_6.0-1+ubuntu14.04_all.deb

Update the repository information.

    # apt-get update

[comment]: # ({/new-31c75b55})

[comment]: # ({new-08c7383c})
##### 5 Upgrade Zabbix components

To upgrade Zabbix components you may run something like:

    # apt-get install --only-upgrade zabbix-server-mysql zabbix-frontend-php zabbix-agent

If using PostgreSQL, substitute `mysql` with `pgsql` in the command. If
upgrading the proxy, substitute `server` with `proxy` in the command. If
upgrading the Zabbix agent 2, substitute `zabbix-agent`with
`zabbix-agent2`in the command.

Then, to upgrade the web frontend with Apache correctly, also run:

    # apt-get install zabbix-apache-conf

Distributions **prior to Debian 10 (buster) / Ubuntu 18.04 (bionic) /
Raspbian 10 (buster)** do not provide PHP 7.2 or newer, which is
required for Zabbix frontend 5.0. See
[information](/manual/installation/frontend/frontend_on_rhel7) about
installing Zabbix frontend on older distributions.

[comment]: # ({/new-08c7383c})

[comment]: # ({new-455f4e97})
##### 6 Review component configuration parameters

See the upgrade notes for details on [mandatory
changes](/manual/installation/upgrade_notes_600#configuration_parameters)
(if any).

For new optional parameters, see the [What's
new](/manual/introduction/whatsnew600#configuration_parameters) section.

[comment]: # ({/new-455f4e97})

[comment]: # ({new-624a8fc7})
##### 7 Start Zabbix processes

Start the updated Zabbix components.

    # service zabbix-server start
    # service zabbix-proxy start
    # service zabbix-agent start
    # service zabbix-agent2 start

[comment]: # ({/new-624a8fc7})

[comment]: # ({new-49e4f43e})
##### 8 Clear web browser cookies and cache

After the upgrade you may need to clear web browser cookies and web
browser cache for the Zabbix web interface to work properly.

[comment]: # ({/new-49e4f43e})

[comment]: # ({new-9bab02f2})
#### Upgrade between minor versions

It is possible to upgrade minor versions of 6.0.x (for example, from
6.0.1 to 6.0.3). It is easy.

To upgrade Zabbix minor version please run:

    $ sudo apt install --only-upgrade 'zabbix.*'

To upgrade Zabbix server minor version please run:

    $ sudo apt install --only-upgrade 'zabbix-server.*'

To upgrade Zabbix agent minor version please run:

    $ sudo apt install --only-upgrade 'zabbix-agent.*'

or, for Zabbix agent 2:

    $ sudo apt install --only-upgrade 'zabbix-agent2.*'

[comment]: # ({/new-9bab02f2})
