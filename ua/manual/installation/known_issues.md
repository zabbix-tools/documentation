[comment]: # translation:outdated

[comment]: # ({new-97796c37})
# 8 Known issues

[comment]: # ({/new-97796c37})

[comment]: # ({new-e338613a})
#### Proxy startup with MySQL 8.0.0-8.0.17

zabbix\_proxy on MySQL versions 8.0.0-8.0.17 fails with the following
"access denied" error:

    [Z3001] connection to database 'zabbix' failed: [1227] Access denied; you need (at least one of) the SUPER, SYSTEM_VARIABLES_ADMIN or SESSION_VARIABLES_ADMIN privilege(s) for this operation

That is due to MySQL 8.0.0 starting to enforce special permissions for
setting session variables. However, in 8.0.18 this behavior was removed:
[As of MySQL 8.0.18, setting the session value of this system variable
is no longer a restricted
operation.](https://dev.mysql.com/doc/refman/8.0/en/server-system-variables.html)

The workaround is based on granting additional privileges to the
`zabbix` user:

For MySQL versions 8.0.14 - 8.0.17:

    grant SESSION_VARIABLES_ADMIN on *.* to 'zabbix'@'localhost';

For MySQL versions 8.0.0 - 8.0.13:

    grant SYSTEM_VARIABLES_ADMIN on *.* to 'zabbix'@'localhost';

[comment]: # ({/new-e338613a})


[comment]: # ({new-68c1ee03})
#### Timescale DB

PostgreSQL versions 9.6-12 use too much memory when updating tables with
a large number of partitions ([see problem
report](https://www.postgresql-archive.org/memory-problems-and-crash-of-db-when-deleting-data-from-table-with-thousands-of-partitions-td6108612.html)).
This issue manifests itself when Zabbix updates trends on systems with
TimescaleDB if trends are split into relatively small (e.g. 1 day)
chunks. This leads to hundreds of chunks present in the trends tables
with default housekeeping settings - the condition where PostgreSQL is
likely to run out of memory.

The issue has been resolved since Zabbix 5.0.1 for new installations
with TimescaleDB, but if TimescaleDB was set up with Zabbix before that,
please see
[ZBX-16347](https://support.zabbix.com/browse/ZBX-16347?focusedCommentId=430816&page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel#comment-430816)
for the migration notes.

[comment]: # ({/new-68c1ee03})

[comment]: # ({new-5c2a7ea7})
#### Upgrade with MariaDB 10.2.1 and before

Upgrading Zabbix may fail if database tables were created with MariaDB
10.2.1 and before, because in those versions the default row format is
compact. This can be fixed by changing the row format to dynamic (see
also [ZBX-17690](https://support.zabbix.com/browse/ZBX-17690)).

[comment]: # ({/new-5c2a7ea7})

[comment]: # ({new-40e33d04})
#### Database TLS connection with MariaDB

Database TLS connection is not supported with the 'verify\_ca' option
for the DBTLSConnect [parameter](/manual/appendix/config/zabbix_server)
if MariaDB is used.

[comment]: # ({/new-40e33d04})

[comment]: # ({new-40ea4656})

#### Possible deadlocks with MySQL/MariaDB

When running under high load, and with more than one LLD worker involved, it is possible to run into a deadlock caused by an InnoDB error related to the row-locking strategy (see [upstream bug](https://github.com/mysql/mysql-server/commit/7037a0bdc83196755a3bf3e935cfb3c0127715d5)). The error has been fixed in MySQL since 8.0.29, but not in MariaDB. For more details, see [ZBX-21506](https://support.zabbix.com/browse/ZBX-21506).

[comment]: # ({/new-40ea4656})

[comment]: # ({new-70c19e71})
#### Global event correlation

Events may not get correlated correctly if the time interval between the
first and second event is very small, i.e. half a second and less.

[comment]: # ({/new-70c19e71})

[comment]: # ({new-215c95a7})
#### Numeric (float) data type range with PostgreSQL 11 and earlier

PostgreSQL 11 and earlier versions only support floating point value
range of approximately -1.34E-154 to 1.34E+154.

[comment]: # ({/new-215c95a7})

[comment]: # ({new-dfc40df7})
#### NetBSD 8.0 and newer

Various Zabbix processes may randomly crash on startup on the NetBSD
versions 8.X and 9.X. That is due to the too small default stack size
(4MB), which must be increased by running:

    ulimit -s 10240

For more information, please see the related problem report:
[ZBX-18275](https://support.zabbix.com/browse/ZBX-18275).

[comment]: # ({/new-dfc40df7})

[comment]: # ({new-3cf04fe3})
#### IPMI checks

IPMI checks will not work with the standard OpenIPMI library package on
Debian prior to 9 (stretch) and Ubuntu prior to 16.04 (xenial). To fix
that, recompile OpenIPMI library with OpenSSL enabled as discussed in
[ZBX-6139](https://support.zabbix.com/browse/ZBX-6139).

[comment]: # ({/new-3cf04fe3})

[comment]: # ({new-8c5cdd23})
#### SSH checks

Some Linux distributions like Debian, Ubuntu do not support encrypted
private keys (with passphrase) if the libssh2 library is installed from
packages. Please see
[ZBX-4850](https://support.zabbix.com/browse/ZBX-4850) for more details.

When using libssh 0.9.x on CentOS 8 with OpenSSH 8 SSH checks may
occasionally report "Cannot read data from SSH server". This is caused
by a libssh
[issue](https://gitlab.com/libssh/libssh-mirror/-/merge_requests/101)
([more detailed report](https://bugs.libssh.org/T231)). The error is
expected to have been fixed by a stable libssh 0.9.5 release. See also
[ZBX-17756](https://support.zabbix.com/browse/ZBX-17756) for details.

[comment]: # ({/new-8c5cdd23})

[comment]: # ({new-0c2fc2b9})
#### ODBC checks

-   MySQL unixODBC driver should not be used with Zabbix server or
    Zabbix proxy compiled against MariaDB connector library and vice
    versa, if possible it is also better to avoid using the same
    connector as the driver due to an [upstream
    bug](https://bugs.mysql.com/bug.php?id=73709). Suggested setup:

```{=html}
<!-- -->
```
    PostgreSQL, SQLite or Oracle connector → MariaDB or MySQL unixODBC driver
    MariaDB connector → MariaDB unixODBC driver
    MySQL connector → MySQL unixODBC driver

Please see [ZBX-7665](https://support.zabbix.com/browse/ZBX-7665) for
more information and available workarounds.

-   XML data queried from Microsoft SQL Server may get truncated in
    various ways on Linux and UNIX systems.

```{=html}
<!-- -->
```
-   It has been observed that using ODBC checks on CentOS 8 for
    monitoring Oracle databases using Oracle Instant Client for Linux
    11.2.0.4.0 causes the Zabbix server to crash. The issue can be
    solved by upgrading Oracle Instant Client to 12.1.0.2.0, 12.2.0.1.0,
    18.5.0.0.0 or 19. See also
    [ZBX-18402](https://support.zabbix.com/browse/ZBX-18402).

[comment]: # ({/new-0c2fc2b9})

[comment]: # ({new-1db730d3})
#### Incorrect request method parameter in items

The request method parameter, used only in HTTP checks, may be
incorrectly set to '1', a non-default value for all items as a result of
upgrade from a pre-4.0 Zabbix version. For details on how to fix this
situation, see [ZBX-19308](https://support.zabbix.com/browse/ZBX-19308).

[comment]: # ({/new-1db730d3})


[comment]: # ({new-4713dff4})
#### Web monitoring and HTTP agent

Zabbix server leaks memory on CentOS 6, CentOS 7 and possibly other
related Linux distributions due to an [upstream
bug](https://bugzilla.redhat.com/show_bug.cgi?id=1057388) when "SSL
verify peer" is enabled in web scenarios or HTTP agent. Please see
[ZBX-10486](https://support.zabbix.com/browse/ZBX-10486) for more
information and available workarounds.

[comment]: # ({/new-4713dff4})

[comment]: # ({new-9cd7efe1})
#### Simple checks

There is a bug in **fping** versions earlier than v3.10 that mishandles
duplicate echo replay packets. This may cause unexpected results for
`icmpping`, `icmppingloss`, `icmppingsec` items. It is recommended to
use the latest version of **fping**. Please see
[ZBX-11726](https://support.zabbix.com/browse/ZBX-11726) for more
details.

[comment]: # ({/new-9cd7efe1})

[comment]: # ({new-a4574c73})
#### SNMP checks

If the OpenBSD operating system is used, a use-after-free bug in the
Net-SNMP library up to the 5.7.3 version can cause a crash of Zabbix
server if the SourceIP parameter is set in the Zabbix server
configuration file. As a workaround, please do not set the SourceIP
parameter. The same problem applies also for Linux, but it does not
cause Zabbix server to stop working. A local patch for the net-snmp
package on OpenBSD was applied and will be released with OpenBSD 6.3.

[comment]: # ({/new-a4574c73})

[comment]: # ({new-d699f9d6})
#### SNMP data spikes

Spikes in SNMP data have been observed that may be related to certain
physical factors like voltage spikes in the mains. See
[ZBX-14318](https://support.zabbix.com/browse/ZBX-14318) more details.

[comment]: # ({/new-d699f9d6})

[comment]: # ({new-7aeb682d})
#### SNMP traps

The "net-snmp-perl" package, needed for SNMP traps, has been removed in
RHEL/CentOS 8.0-8.2; re-added in RHEL 8.3.

So if you are using RHEL 8.0-8.2, the best solution is to upgrade to
RHEL 8.3; if you are using CentOS 8.0-8.2, you may wait for CentOS 8.3
or use a package from EPEL.

Please also see [ZBX-17192](https://support.zabbix.com/browse/ZBX-17192)
for more information.

[comment]: # ({/new-7aeb682d})

[comment]: # ({new-f46cb486})
#### Alerter process crash in Centos/RHEL 7

Instances of a Zabbix server alerter process crash have been encountered
in Centos/RHEL 7. Please see
[ZBX-10461](https://support.zabbix.com/browse/ZBX-10461) for details.

[comment]: # ({/new-f46cb486})

[comment]: # ({new-8ffad918})
#### Compiling Zabbix agent on HP-UX

If you install the PCRE library from a popular HP-UX package site
<http://hpux.connect.org.uk>, for example from file
`pcre-8.42-ia64_64-11.31.depot`, you get only the 64-bit version of the
library installed in the /usr/local/lib/hpux64 directory.

In this case, for successful agent compilation customized options need
to be used for the "configure" script, e.g.:

    CFLAGS="+DD64" ./configure --enable-agent --with-libpcre-include=/usr/local/include --with-libpcre-lib=/usr/local/lib/hpux64

[comment]: # ({/new-8ffad918})

[comment]: # ({new-6e1fb8fe})
#### Flipping frontend locales

It has been observed that frontend locales may flip without apparent
logic, i. e. some pages (or parts of pages) are displayed in one
language while other pages (or parts of pages) in a different language.
Typically the problem may appear when there are several users, some of
whom use one locale, while others use another.

A known workaround to this is to disable multithreading in PHP and
Apache.

The problem is related to how setting the locale works [in
PHP](https://www.php.net/manual/en/function.setlocale): locale
information is maintained per process, not per thread. So in a
multi-thread environment, when there are several projects run by same
Apache process, it is possible that the locale gets changed in another
thread and that changes how data can be processed in the Zabbix thread.

For more information, please see related problem reports:

-   [ZBX-10911](https://support.zabbix.com/browse/ZBX-10911) (Problem
    with flipping frontend locales)
-   [ZBX-16297](https://support.zabbix.com/browse/ZBX-16297) (Problem
    with number processing in graphs using the `bcdiv` function of BC
    Math functions)

[comment]: # ({/new-6e1fb8fe})

[comment]: # ({new-81fe18ae})
#### PHP 7.3 opcache configuration

If "opcache" is enabled in the PHP 7.3 configuration, Zabbix frontend
may show a blank screen when loaded for the first time. This is a
registered [PHP bug](https://bugs.php.net/bug.php?id=78015). To work
around this, please set the "opcache.optimization\_level" parameter to
`0x7FFFBFDF` in the PHP configuration (php.ini file).

[comment]: # ({/new-81fe18ae})

[comment]: # ({new-4f3b73ce})
#### Graphs

Changes to Daylight Saving Time (DST) result in irregularities when
displaying X axis labels (date duplication, date missing, etc).

[comment]: # ({/new-4f3b73ce})

[comment]: # ({new-357fdb5b})
#### Log file monitoring

`log[]` and `logrt[]` items repeatedly reread log file from the
beginning if file system is 100% full and the log file is being appended
(see [ZBX-10884](https://support.zabbix.com/browse/ZBX-10884) for more
information).

[comment]: # ({/new-357fdb5b})

[comment]: # ({new-82ff58c2})
#### Slow MySQL queries

Zabbix server generates slow select queries in case of non-existing
values for items. This is caused by a known
[issue](https://bugs.mysql.com/bug.php?id=74602) in MySQL 5.6/5.7
versions. A workaround to this is disabling the
index\_condition\_pushdown optimizer in MySQL. For an extended
discussion, see
[ZBX-10652](https://support.zabbix.com/browse/ZBX-10652).

[comment]: # ({/new-82ff58c2})


[comment]: # ({new-7ed39efb})
#### Slow configuration sync with Oracle

Configuration sync might be slow in Zabbix 6.0 installations with Oracle DB that have high number of items and item preprocessing steps.
This is caused by the Oracle database engine speed processing *nclob* type fields.

To improve performance, you can convert the field types from *nclob* to *nvarchar2* by manually applying the database patch [items_nvarchar_prepare.sql](/../assets/en/manual/installation/items_nvarchar_prepare.sql).
Note that this conversion will reduce the maximum field size limit from 65535 bytes to 4000 bytes
for item preprocessing parameters and item parameters such as *Description*, Script item's field *Script*,
HTTP agent item's fields *Request body* and *Headers*, Database monitor item's field *SQL query*.
Queries to determine template names that need to be deleted before applying the patch are provided in the patch as a comment. Alternatively, if MAX_STRING_SIZE is set you can change *nvarchar2(4000)* to *nvarchar2(32767)* in the patch queries to set the 32767 bytes field size limit.

For an extended discussion, see [ZBX-22363](https://support.zabbix.com/browse/ZBX-22363).

[comment]: # ({/new-7ed39efb})

[comment]: # ({new-b393528e})
#### API login

A large number of open user sessions can be created when using custom
scripts with the `user.login` [method](/manual/api/reference/user/login)
without a following `user.logout`.

[comment]: # ({/new-b393528e})

[comment]: # ({new-17c4463f})
#### IPv6 address issue in SNMPv3 traps

Due to a net-snmp bug, IPv6 address may not be correctly displayed when
using SNMPv3 in SNMP traps. For more details and a possible workaround,
see [ZBX-14541](https://support.zabbix.com/browse/ZBX-14541).

[comment]: # ({/new-17c4463f})

[comment]: # ({new-d77627ce})
#### Trimmed long IPv6 IP address in failed login information

A failed login attempt message will display only the first 39 characters
of a stored IP address as that's the character limit in the database
field. That means that IPv6 IP addresses longer than 39 characters will
be shown incompletely.

[comment]: # ({/new-d77627ce})

[comment]: # ({new-57420738})
#### Zabbix agent checks on Windows

Non-existing DNS entries in a `Server` parameter of Zabbix agent
configuration file (zabbix\_agentd.conf) may increase Zabbix agent
response time on Windows. This happens because Windows DNS caching
daemon doesn't cache negative responses for IPv4 addresses. However, for
IPv6 addresses negative responses are cached, so a possible workaround
to this is disabling IPv4 on the host.

[comment]: # ({/new-57420738})

[comment]: # ({new-40001075})
#### YAML export/import

There are some known issues with YAML
[export/import](/manual/xml_export_import):

-   Error messages are not translatable;
-   Valid JSON with a .yaml file extension sometimes cannot be imported;
-   Unquoted human-readable dates are automatically converted to Unix
    timestamps.

[comment]: # ({/new-40001075})

[comment]: # ({new-fcbf4bce})
#### Setup wizard on SUSE with NGINX and php-fpm

Frontend setup wizard cannot save configuration file on SUSE with NGINX
+ php-fpm. This is caused by a setting in
/usr/lib/systemd/system/php-fpm.service unit, which prevents Zabbix from
writing to /etc. (introduced in [PHP
7.4](https://bugs.php.net/bug.php?id=72510)).

There are two workaround options available:

-   Set the
    [ProtectSystem](https://www.freedesktop.org/software/systemd/man/systemd.exec.html#ProtectSystem=)
    option to 'true' instead of 'full' in the php-fpm systemd unit.
-   Manually save /etc/zabbix/web/zabbix.conf.php file.

[comment]: # ({/new-fcbf4bce})

[comment]: # ({new-8009b04b})
#### Chromium for Zabbix web service on Ubuntu 20

Though in most cases, Zabbix web service can run with Chromium, on
Ubuntu 20.04 using Chromium causes the following error:

    Cannot fetch data: chrome failed to start:cmd_run.go:994:
    WARNING: cannot create user data directory: cannot create 
    "/var/lib/zabbix/snap/chromium/1564": mkdir /var/lib/zabbix: permission denied
    Sorry, home directories outside of /home are not currently supported. See https://forum.snapcraft.io/t/11209 for details.

This error occurs because `/var/lib/zabbix` is used as a home directory
of user 'zabbix'.

[comment]: # ({/new-8009b04b})

[comment]: # ({new-1f99c5d8})
#### MySQL custom error codes

If Zabbix is used with MySQL installation on Azure, an unclear error
message *\[9002\] Some errors occurred* may appear in Zabbix logs. This
generic error text is sent to Zabbix server or proxy by the database. To
get more information about the cause of the error, check Azure logs.

[comment]: # ({/new-1f99c5d8})


[comment]: # ({new-eb422070})
#### Invalid regular expressions after switching to PCRE2 
In Zabbix 6.0 support for PCRE2 has been added. Even though PCRE is still supported, Zabbix installation packages for RHEL/CentOS 7 and newer, SLES (all versions), Debian 9 and newer, Ubuntu 16.04 and newer have been updated to use PCRE2. While providing many benefits, switching to PCRE2 may cause certain existing PCRE regexp patterns becoming invalid or behaving differently. In particular, this affects the pattern *\^[\\w-\\.]*. In order to make this regexp valid again without affecting semantics, change the expression to *\^[-\\w\\.]* . This happens due to the fact that PCRE2 treats the dash sign as a delimiter, creating a range inside a character class.
The following Zabbix installation packages have been updated and now use PCRE2:  RHEL/CentOS 7 and newer, SLES (all versions), Debian 9 and newer, Ubuntu 16.04 and newer.

[comment]: # ({/new-eb422070})

[comment]: # ({new-093b78e2})
#### Geomap widget error 

The maps in the Geomap widget may not load correctly, if you have upgraded from an older Zabbix version with NGINX and didn't switch to the new NGINX configuration file during the upgrade. 

To fix the issue, you can  discard the old configuration file, use the configuration file from the current version package and reconfigure it as described in the [download instructions](https://www.zabbix.com/download?zabbix=6.0&os_distribution=red_hat_enterprise_linux&os_version=8&db=mysql&ws=nginx) in section *e. Configure PHP for Zabbix frontend*.

Alternatively, you can manually edit an existing NGINX configuration file (typically, */etc/zabbix/nginx.conf*). To do so, open the file and locate the following block: 

    location ~ /(api\/|conf[^\.]|include|locale|vendor) {
            deny            all;
            return          404;
    }

Then, replace this block with: 

    location ~ /(api\/|conf[^\.]|include|locale) {
            deny            all;
            return          404;
    }

    location /vendor {
            deny            all;
            return          404;
    }


[comment]: # ({/new-093b78e2})
