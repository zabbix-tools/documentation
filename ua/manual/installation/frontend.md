[comment]: # translation:outdated

[comment]: # ({new-9cceae9f})
# 6 Web interface installation

This section provides step-by-step instructions for installing Zabbix
web interface. Zabbix frontend is written in PHP, so to run it a PHP
supported webserver is needed.

[comment]: # ({/new-9cceae9f})

[comment]: # ({new-0c649c0e})
:::notetip
You can find out more about setting up SSL for Zabbix frontend by referring to these [best practices](/manual/installation/requirements/best_practices).
:::

[comment]: # ({/new-0c649c0e})

[comment]: # ({new-6b173526})
#### Welcome screen

Open Zabbix frontend URL in the browser. If you have installed Zabbix
from packages, the URL is:

-   for Apache: *http://<server\_ip\_or\_name>/zabbix*
-   for Nginx: *http://<server\_ip\_or\_name>*

You should see the first screen of the frontend installation wizard.

Use the *Default language* drop-down menu to change system default
language and continue the installation process in the selected language
(optional). For more information, see [Installation of additional
frontend languages](/manual/appendix/install/locales).

![](../../../assets/en/manual/installation/install_1.png){width="550"}

[comment]: # ({/new-6b173526})

[comment]: # ({new-2458c643})
#### Check of pre-requisites

Make sure that all software prerequisites are met.

![](../../../assets/en/manual/installation/install_2.png){width="550"}

|Pre-requisite|Minimum value|Description|
|-------------|-------------|-----------|
|*PHP version*|7.2.5|<|
|*PHP memory\_limit option*|128MB|In php.ini:<br>memory\_limit = 128M|
|*PHP post\_max\_size option*|16MB|In php.ini:<br>post\_max\_size = 16M|
|*PHP upload\_max\_filesize option*|2MB|In php.ini:<br>upload\_max\_filesize = 2M|
|*PHP max\_execution\_time option*|300 seconds (values 0 and -1 are allowed)|In php.ini:<br>max\_execution\_time = 300|
|*PHP max\_input\_time option*|300 seconds (values 0 and -1 are allowed)|In php.ini:<br>max\_input\_time = 300|
|*PHP session.auto\_start option*|must be disabled|In php.ini:<br>session.auto\_start = 0|
|*Database support*|One of: MySQL, Oracle, PostgreSQL.|One of the following modules must be installed:<br>mysql, oci8, pgsql|
|*bcmath*|<|php-bcmath|
|*mbstring*|<|php-mbstring|
|*PHP mbstring.func\_overload option*|must be disabled|In php.ini:<br>mbstring.func\_overload = 0|
|*sockets*|<|php-net-socket. Required for user script support.|
|*gd*|2.0.28|php-gd. PHP GD extension must support PNG images (*--with-png-dir*), JPEG (*--with-jpeg-dir*) images and FreeType 2 (*--with-freetype-dir*).|
|*libxml*|2.6.15|php-xml|
|*xmlwriter*|<|php-xmlwriter|
|*xmlreader*|<|php-xmlreader|
|*ctype*|<|php-ctype|
|*session*|<|php-session|
|*gettext*|<|php-gettext<br>Since Zabbix 2.2.1, the PHP gettext extension is not a mandatory requirement for installing Zabbix. If gettext is not installed, the frontend will work as usual, however, the translations will not be available.|

Optional pre-requisites may also be present in the list. A failed
optional prerequisite is displayed in orange and has a *Warning* status.
With a failed optional pre-requisite, the setup may continue.

::: noteimportant
If there is a need to change the Apache user or
user group, permissions to the session folder must be verified.
Otherwise Zabbix setup may be unable to continue.
:::

[comment]: # ({/new-2458c643})

[comment]: # ({new-879dc06f})
#### Configure DB connection

Enter details for connecting to the database. Zabbix database must
already be created.

![](../../../assets/en/manual/installation/install_3a.png){width="550"}

If the *Database TLS encryption* option is checked, then additional
fields for [configuring the TLS
connection](/manual/appendix/install/db_encrypt) to the database appear
in the form (MySQL or PostgreSQL only).

If HashiCorp Vault option is selected for storing credentials,
additional fields are available for specifying the Vault API endpoint,
secret path and authentication token:

![](../../../assets/en/manual/installation/install_3b.png){width="550"}

[comment]: # ({/new-879dc06f})

[comment]: # ({new-98277238})
#### Settings

Entering a name for Zabbix server is optional, however, if submitted, it
will be displayed in the menu bar and page titles.

Set the default [time zone](/manual/web_interface/time_zone#overview)
and theme for the frontend.

![](../../../assets/en/manual/installation/install_4.png){width="550"}

[comment]: # ({/new-98277238})

[comment]: # ({new-42398398})
#### Pre-installation summary

Review a summary of settings.

![](../../../assets/en/manual/installation/install_5.png){width="550"}

[comment]: # ({/new-42398398})

[comment]: # ({new-1124dd9e})
#### Install

If installing Zabbix from sources, download the configuration file and
place it under conf/ in the webserver HTML documents subdirectory where
you copied Zabbix PHP files to.

![](../../../assets/en/manual/installation/install_6.png){width="550"}

![](../../../assets/en/manual/installation/saving_zabbix_conf.png){width="350"}

::: notetip
Providing the webserver user has write access to conf/
directory the configuration file would be saved automatically and it
would be possible to proceed to the next step right away.
:::

Finish the installation.

![](../../../assets/en/manual/installation/install_7.png){width="550"}

[comment]: # ({/new-1124dd9e})

[comment]: # ({new-d59dc4b9})
#### Log in

Zabbix frontend is ready! The default user name is **Admin**, password
**zabbix**.

![](../../../assets/en/manual/quickstart/login.png){width="350"}

Proceed to [getting started with Zabbix](/manual/quickstart/login).

[comment]: # ({/new-d59dc4b9})
