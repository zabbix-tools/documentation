<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="ua" datatype="plaintext" original="manual/appendix/config/zabbix_agent2_plugins/mqtt_plugin.md">
    <body>
      <trans-unit id="e0afbd96" xml:space="preserve">
        <source># 6 MQTT plugin</source>
      </trans-unit>
      <trans-unit id="88abc321" xml:space="preserve">
        <source>#### Overview

This section lists parameters supported in the MQTT Zabbix agent 2
plugin configuration file (mqtt.conf). 

Note that:

-   The default values reflect process defaults, not the values in the
    shipped configuration files;
-   Zabbix supports configuration files only in UTF-8 encoding without
    [BOM](https://en.wikipedia.org/wiki/Byte_order_mark);
-   Comments starting with "\#" are only supported at the beginning of
    the line.</source>
      </trans-unit>
      <trans-unit id="97d87248" xml:space="preserve">
        <source>#### Parameters

|Parameter|Mandatory|Range|Default|Description|
|--|--|--|--|----------|
|Plugins.MQTT.Default.Password|no| | |Default password for connecting to MQTT; used if no value is specified in an item key or named session.&lt;br&gt; Supported since version 6.4.4|
|Plugins.MQTT.Default.TLSCAFile|no| | |Full pathname of a file containing the top-level CA(s) certificates for peer certificate verification for encrypted communications between Zabbix agent 2 and MQTT broker; used if no value is specified in a named session.&lt;br&gt; Supported since version 6.4.4|
|Plugins.MQTT.Default.TLSCertFile|no| | |Full pathname of a file containing the agent certificate or certificate chain for encrypted communications between Zabbix agent 2 and MQTT broker; used if no value is specified in a named session.&lt;br&gt; Supported since version 6.4.4|
|Plugins.MQTT.Default.TLSKeyFile|no| | |Full pathname of a file containing the MQTT private key for encrypted communications between Zabbix agent 2 and MQTT broker; used if no value is specified in a named session.&lt;br&gt; Supported since version 6.4.4|
|Plugins.MQTT.Default.Topic|no| | |Default topic for MQTT subscription; used if no value is specified in an item key or named session.&lt;br&gt;&lt;br&gt;The topic may contain wildcards ("+","#")&lt;br&gt;Examples: `path/to/file`&lt;br&gt;`path/to/#`&lt;br&gt;`path/+/topic`&lt;br&gt;Supported since version 6.4.4|
|Plugins.MQTT.Default.Url|no| |tcp://localhost:1883|Default MQTT broker connection string; used if no value is specified in an item key or named session.&lt;br&gt;&lt;br&gt;Should not include query parameters.&lt;br&gt;Must match the URL format.&lt;br&gt;Supported schemes: `tcp` (default), `ws`, `tls`; a scheme can be omitted.&lt;br&gt;A port can be omitted (default=1883).&lt;br&gt;Examples: `tcp://host:1883`&lt;br&gt;`localhost`&lt;br&gt;`ws://host:8080`&lt;br&gt;Supported since version 6.4.4|
|Plugins.MQTT.Default.User|no| | |Default username for connecting to MQTT; used if no value is specified in an item key or named session.&lt;br&gt;Supported since version 6.4.4|
|Plugins.MQTT.Sessions.&lt;SessionName&gt;.Password|no| | |Named session password.&lt;br&gt;**&lt;SessionName&gt;** - define name of a session for using in item keys.&lt;br&gt;Supported since version 6.4.4|
|Plugins.MQTT.Sessions.&lt;SessionName&gt;.TLSCAFile|no| | |Full pathname of a file containing the top-level CA(s) certificates for peer certificate verification, used for encrypted communications between Zabbix agent 2 and MQTT broker.&lt;br&gt;**&lt;SessionName&gt;** - define name of a session for using in item keys.&lt;br&gt;Supported since version 6.4.4|
|Plugins.MQTT.Sessions.&lt;SessionName&gt;.TLSCertFile|no| | |Full pathname of a file containing the agent certificate or certificate chain, used for encrypted communications between Zabbix agent 2 and MQTT broker.&lt;br&gt;**&lt;SessionName&gt;** - define name of a session for using in item keys.&lt;br&gt;Supported since version 6.4.4|
|Plugins.MQTT.Sessions.&lt;SessionName&gt;.TLSKeyFile|no| | |Full pathname of a file containing the MQTT private key used for encrypted communications between Zabbix agent 2 and MQTT broker.&lt;br&gt;**&lt;SessionName&gt;** - define name of a session for using in item keys.&lt;br&gt;Supported since version 6.4.4|
|Plugins.MQTT.Sessions.&lt;SessionName&gt;.Topic|no| | |Named session topic for MQTT subscription.&lt;br&gt;**&lt;SessionName&gt;** - define name of a session for using in item keys.&lt;br&gt;&lt;br&gt;The topic may contain wildcards ("+","#")&lt;br&gt;Examples: `path/to/file`&lt;br&gt;`path/to/#`&lt;br&gt;`path/+/topic`&lt;br&gt;Supported since version 6.4.4|
|Plugins.MQTT.Sessions.&lt;SessionName&gt;.Url|no| | |Connection string of a named session.&lt;br&gt;**&lt;SessionName&gt;** - define name of a session for using in item keys.&lt;br&gt;&lt;br&gt;Should not include query parameters.&lt;br&gt;Must match the URL format.&lt;br&gt;Supported schemes: `tcp` (default), `ws`, `tls`; a scheme can be omitted.&lt;br&gt;A port can be omitted (default=1883).&lt;br&gt;Examples: `tcp://host:1883`&lt;br&gt;`localhost`&lt;br&gt;`ws://host:8080`&lt;br&gt;Supported since version 6.4.4|
|Plugins.MQTT.Sessions.&lt;SessionName&gt;.User|no| | |Named session username.&lt;br&gt;**&lt;SessionName&gt;** - define name of a session for using in item keys.&lt;br&gt;Supported since version 6.4.4|
|Plugins.MQTT.Timeout|no|1-30|global timeout|Request execution timeout (how long to wait for a request to complete before shutting it down).|

See also:

-   Description of general Zabbix agent 2 configuration parameters:
    [Zabbix agent 2 (UNIX)](/manual/appendix/config/zabbix_agent2) /
    [Zabbix agent 2
    (Windows)](/manual/appendix/config/zabbix_agent2_win)
-   Instructions for configuring [plugins](/manual/extensions/plugins)</source>
      </trans-unit>
    </body>
  </file>
</xliff>
