[comment]: # translation:outdated

[comment]: # ({new-9778afa5})
# 4 Modbus plugin

[comment]: # ({/new-9778afa5})

[comment]: # ({new-f2cac608})
#### Overview

This section lists parameters supported in the Modbus Zabbix agent 2
plugin configuration file (modbus.conf). Note that:

-   The default values reflect process defaults, not the values in the
    shipped configuration files;
-   Zabbix supports configuration files only in UTF-8 encoding without
    [BOM](https://en.wikipedia.org/wiki/Byte_order_mark);
-   Comments starting with "\#" are only supported at the beginning of
    the line.

[comment]: # ({/new-f2cac608})

[comment]: # ({new-c69b0a6d})
#### Parameters

|Parameter|Mandatory|Range|Default|Description|
|---------|---------|-----|-------|-----------|
|Plugins.Modbus.Sessions.<SessionName>.Endpoint|no|<|<|Endpoint is a connection string consisting of a protocol scheme, a host address and a port or seral port name and attributes.<br>**<SessionName>** - name of a session for using in item keys.|
|Plugins.Modbus.Sessions.<SessionName>.SlaveID|no|<|<|Slave ID of a named session.<br>**<SessionName>** - name of a session for using in item keys.<br>Example: `Plugins.Modbus.Sessions.MB1.SlaveID=20`<br>*Note* that this named session parameter is checked only if the value provided in the [item key](/manual/config/items/itemtypes/zabbix_agent) slave ID parameter is empty.|
|Plugins.Modbus.Sessions.<SessionName>.Timeout|no|<|<|Timeout of a named session.<br>**<SessionName>** - name of a session for using in item keys.<br>Example: `Plugins.Modbus.Sessions.MB1.Timeout=2`|
|Plugins.Modbus.Timeout|no|1-30|global timeout|Request execution timeout (how long to wait for a request to complete before shutting it down).|

See also:

-   Description of general Zabbix agent 2 configuration parameters:
    [Zabbix agent 2 (UNIX)](/manual/appendix/config/zabbix_agent2) /
    [Zabbix agent 2
    (Windows)](/manual/appendix/config/zabbix_agent2_win)
-   Instructions for configuring [plugins](/manual/config/items/plugins)

[comment]: # ({/new-c69b0a6d})
