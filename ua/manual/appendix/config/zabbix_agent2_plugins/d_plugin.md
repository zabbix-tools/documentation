[comment]: # translation:outdated

[comment]: # ({new-aed74d95})
# 2 Docker plugin

[comment]: # ({/new-aed74d95})

[comment]: # ({new-d38e50a7})
#### Overview

This section lists parameters supported in the Docker Zabbix agent 2
plugin configuration file (docker.conf). Note that:

-   The default values reflect process defaults, not the values in the
    shipped configuration files;
-   Zabbix supports configuration files only in UTF-8 encoding without
    [BOM](https://en.wikipedia.org/wiki/Byte_order_mark);
-   Comments starting with "\#" are only supported at the beginning of
    the line.

[comment]: # ({/new-d38e50a7})

[comment]: # ({new-de9824e9})
#### Parameters

|Parameter|Mandatory|Range|Default|Description|
|---------|---------|-----|-------|-----------|
|Plugins.Docker.Endpoint|no|<|unix:///var/run/docker.sock|Docker daemon unix-socket location.<br>Must contain a scheme (only `unix://` is supported).|
|Plugins.Docker.Timeout|no|1-30|global timeout|Request execution timeout (how long to wait for a request to complete before shutting it down).|

See also:

-   Description of general Zabbix agent 2 configuration parameters:
    [Zabbix agent 2 (UNIX)](/manual/appendix/config/zabbix_agent2) /
    [Zabbix agent 2
    (Windows)](/manual/appendix/config/zabbix_agent2_win)
-   Instructions for configuring [plugins](/manual/config/items/plugins)

[comment]: # ({/new-de9824e9})
