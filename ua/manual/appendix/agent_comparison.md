[comment]: # translation:outdated

[comment]: # ({new-28165f87})
# 18 Agent vs agent 2 comparison

This section describes the differences between the Zabbix agent and the
Zabbix agent 2.

|Parameter|Zabbix agent|Zabbix agent 2|
|---------|------------|--------------|
|Programming language|C|Go with some parts in C|
|Daemonization|yes|by systemd only (yes on Windows)|
|Supported extensions|Custom [loadable modules](/manual/config/items/loadablemodules) in C.|Custom [plugins](/manual/config/items/plugins) in Go.|
|Requirements|<|<|
|Supported platforms|Linux, IBM AIX, FreeBSD, NetBSD, OpenBSD, HP-UX, Mac OS X, Solaris: 9, 10, 11, Windows: all desktop and server versions since XP|Linux, Windows: all desktop and server versions since XP.|
|Supported crypto libraries|GnuTLS 3.1.18 and newer<br>OpenSSL 1.0.1, 1.0.2, 1.1.0, 1.1.1<br>LibreSSL - tested with versions 2.7.4, 2.8.2 (certain limitations apply, see the [Encryption](/manual/encryption#compiling_zabbix_with_encryption_support) page for details).|Linux: OpenSSL 1.0.1 and later is supported since Zabbix 4.4.8.<br>MS Windows: OpenSSL 1.1.1 or later.<br>The OpenSSL library must have PSK support enabled. LibreSSL is not supported.|
|Monitoring processes|<|<|
|Processes|A separate active check process for each server/proxy record.|Single process with automatically created threads.<br>The maximum number of threads is determined by the GOMAXPROCS environment variable.|
|Metrics|**UNIX**: see a list of supported [items](/manual/config/items/itemtypes/zabbix_agent).<br><br>**Windows**: see a list of additional Windows-specific [items](/manual/config/items/itemtypes/zabbix_agent/win_keys).|**UNIX**: All metrics supported by Zabbix agent.<br>Additionally, the agent 2 provides Zabbix-native monitoring solution for: Docker, Memcached, MySQL, PostgreSQL, Redis, systemd, and other monitoring targets - see a full list of agent 2 specific [items](/manual/config/items/itemtypes/zabbix_agent/zabbix_agent2).<br><br>**Windows**: All metrics supported by Zabbix agent, and also net.tcp.service\* checks of HTTPS, LDAP.<br>Additionally, the agent 2 provides Zabbix-native monitoring solution for: PostgreSQL, Redis.|
|Concurrency|Active checks for single server are executed sequentially.|Checks from different plugins or multiple checks within one plugin can be executed concurrently.|
|Scheduled/flexible intervals|Supported for passive checks only.|Supported for passive and active checks.|
|Third party traps|no|yes|
|Additional features|<|<|
|Persistent storage|no|yes|
|Persistent files for log\*\[\] metrics|yes (only on Unix)|no|
|Timeout settings|Defined on an agent level only.|Plugin timeout can override the timeout defined on an agent level.|
|Changes user at runtime|yes (Unix-like systems only)|no (controlled by systemd)|
|User-configurable ciphersuites|yes|no|

**See also:**

-   *Zabbix processes description*: [Zabbix
    agent](/manual/concepts/agent), [Zabbix agent
    2](/manual/concepts/agent2)
-   *Configuration parameters*: Zabbix agent
    [UNIX](/manual/appendix/config/zabbix_agentd) /
    [Windows](/manual/appendix/config/zabbix_agentd_win), Zabbix agent 2
    [UNIX](/manual/appendix/config/zabbix_agent2) /
    [Windows](/manual/appendix/config/zabbix_agent2_win)

[comment]: # ({/new-28165f87})
