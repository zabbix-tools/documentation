[comment]: # translation:outdated

[comment]: # ({new-9153def3})
# 4 Header

[comment]: # ({/new-9153def3})

[comment]: # ({new-faa3c87b})
#### Overview

The header is present in response and request messages between Zabbix
components. It is required to determine the length of message, if it is
compressed or not and the format of message length fields. The header
consists of:

    <PROTOCOL> - "ZBXD" (4 bytes).
    <FLAGS> - the protocol flags, (1 byte). 0x01 - Zabbix communications protocol, 0x02 - compression, 0x04 - large packet).
    <DATALEN> - data length (4 bytes or 8 bytes for large packet). 1 will be formatted as 01/00/00/00 (four bytes, 32 bit number in little-endian format) or 01/00/00/00/00/00/00/00 (eight bytes, 64 bit number in little-endian format) for large packet.
    <RESERVED> - uncompressed data length (4 bytes or 8 bytes for large packet). 1 will be formatted as 01/00/00/00 (four bytes, 32 bit number in little-endian format) or 01/00/00/00/00/00/00/00 (eight bytes, 64 bit number in little-endian format) for large packet.

When compression is enabled (0x02 flag) the <RESERVED> bytes
contains uncompressed data size.

Zabbix protocol has 1GB packet size limit per connection. The limit of
1GB is applied for received packet data length and for uncompressed data
length, however, when large packet is enabled (0x04 flag) it is possible
for Zabbix proxy to receive configuration with size up to 16GB; note
that large packet can only be used for Zabbix proxy configuration, and
Zabbix server will automatically set (0x04 flag) and send length fields
as 8 bytes each when data length before compression exceeds 4GB.

[comment]: # ({/new-faa3c87b})

[comment]: # ({new-409c40cc})

#### Structure

The header consists of four fields. All numbers in the header are formatted as little-endian.

|Field|Size|Size<br>(large packet)|Description|
|--|-|-|------|
|`<PROTOCOL>`|4|4|`"ZBXD"` or `5A 42 58 44`|
|`<FLAGS>`|1|1|Protocol flags:<br>`0x01` - Zabbix communications protocol<br>`0x02` - compression<br>`0x04` - large packet|
|`<DATALEN>`|4|8|Data length.|
|`<RESERVED>`|4|8|When compression is used (`0x02` flag) - the length of uncompressed data<br>When compression is not used - `00 00 00 00`|

[comment]: # ({/new-409c40cc})

[comment]: # ({new-8698de59})
#### Implementation

Here are code snippets showing how to add Zabbix protocol header to the
`data` you *want* to send in order to obtain `packet` you *should* send
to Zabbix so it is interpreted correctly.

|Language|Code|
|--------|----|
|bash|`printf -v LENGTH '%016x' "${#DATA}"PACK=""for (( i=14; i>=0; i-=2 )); do PACK="$PACK\\x${LENGTH:$i:2}"; doneprintf "ZBXD\1$PACK%s" "$DATA"`{.bash}|
|Java|`byte[] header = new byte[] {'Z', 'B', 'X', 'D', '\1',(byte)(data.length & 0xFF),(byte)((data.length >> 8) & 0xFF),(byte)((data.length >> 16) & 0xFF),(byte)((data.length >> 24) & 0xFF),'\0', '\0', '\0', '\0'};|
|<|byte[] packet = new byte[header.length + data.length];System.arraycopy(header, 0, packet, 0, header.length);System.arraycopy(data, 0, packet, header.length, data.length);`{.Java}|
|PHP|`$packet = "ZBXD\1" . pack('P', strlen($data)) . $data;`{.PHP}or`$packet = "ZBXD\1" . pack('V', strlen($data)) . "\0\0\0\0" . $data;`{.PHP}|
|Perl|`my $packet = "ZBXD\1" . pack('<Q', length($data)) . $data;`{.Perl}or`my $packet = "ZBXD\1" . pack('V', length($data)) . "\0\0\0\0" . $data;`{.Perl}|
|Python|`packet = "ZBXD\1" + struct.pack('<Q', len(data)) + data`{.Python}|

[comment]: # ({/new-8698de59})
