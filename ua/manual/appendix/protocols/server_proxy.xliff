<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="ua" datatype="plaintext" original="manual/appendix/protocols/server_proxy.md">
    <body>
      <trans-unit id="7669b1c2" xml:space="preserve">
        <source># 1 Server-proxy data exchange protocol</source>
      </trans-unit>
      <trans-unit id="7382efcd" xml:space="preserve">
        <source>#### Overview

Server - proxy data exchange is based on JSON format.

Request and response messages must begin with [header and data
length](/manual/appendix/protocols/header_datalen).</source>
      </trans-unit>
      <trans-unit id="14661e11" xml:space="preserve">
        <source>#### Passive proxy</source>
      </trans-unit>
      <trans-unit id="fac082ad" xml:space="preserve">
        <source>##### Configuration request

The server will first send an empty `proxy config` request. This request is sent every `ProxyConfigFrequency`
(server configuration parameter) seconds.

The proxy responds with the current proxy version, session token and configuration revision. The server responds with the configuration data that need to be updated.

| name | &lt;   | &lt;   | &lt;   | &lt;   | value type | description |
|-|-|-|-|----------|----------|----------------------------------------|
| server→proxy: | &lt;   | &lt;   | &lt;   | &lt;   | &lt;   | &lt;   |
| **request** | &lt;   | &lt;   | &lt;   | &lt;   | *string* | 'proxy config' |
| |&lt;|&lt;|&lt;|&lt;|&lt;|&lt;|
| proxy→server: | &lt;   | &lt;   | &lt;   | &lt;   | &lt;   | &lt;   |
| **version** | &lt;   | &lt;   | &lt;   | &lt;   | *string* | Proxy version (\&lt;major&gt;.\&lt;minor&gt;.\&lt;build&gt;). |
| **session** | &lt;   | &lt;   | &lt;   | &lt;   | *string* | Proxy configuration session token. |
| **config_revision** | &lt;   | &lt;   | &lt;   | &lt;   | *number* | Proxy configuration revision. |
| |&lt;|&lt;|&lt;|&lt;|&lt;|&lt;|
| server→proxy: | &lt;   | &lt;   | &lt;   | &lt;   | &lt;   | &lt;   |
| **full_sync** | &lt;   | &lt;   | &lt;   | &lt;   | *number* | 1 - if full configuration data is sent; absent - otherwise (optional). |
| **data** | &lt;   | &lt;   | &lt;   | &lt;   | *array* | Object of table data. Absent if configuration has not been changed (optional). |
|     | **\&lt;table&gt;** | &lt;   | &lt;   | &lt;   | *object* | One or more objects with \&lt;table&gt; data (optional, depending on changes). |
| ^   |     | **fields** | &lt;   | &lt;   | *array* | Array of field names. |
| ^   | ^   |     | -   | &lt;   | *string* | Field name. |
| ^   | ^   | **data** | &lt;   | &lt;   | *array* | Array of rows. |
| ^   | ^   |     | -   | &lt;   | *array* | Array of columns. |
| ^   | ^   | ^   |     | -   | *string*,*number* | Column value with type depending on column type in database schema. |
|     | &lt;   | &lt;   | &lt;   | &lt;   | &lt;   | &lt;   |
| **macro.secrets** | &lt;   | &lt;   | &lt;   | &lt;  | *object* | Secret macro information, absent if there are no changes in vault macros  (optional). |
| **config_revision** | &lt;   | &lt;   | &lt;   | &lt;   | *number* | Configuration cache revision - sent with configuration data (optional). |
| **del_hostids** | &lt;   | &lt;   | &lt;   | &lt;   | *array* | Array of removed hostids (optional). |
|     | -   | &lt;   | &lt;   | &lt;   | *number* | Host identifier. |
| **del_macro_hostids** | &lt;   | &lt;   | &lt;   | &lt;   | *array* | Array of hostids with all macros removed (optional). |
|     | -   | &lt;   | &lt;   | &lt;   | *number* | Host identifier. |
|     | &lt;   | &lt;   | &lt;   | &lt;   | &lt;   | &lt;   |
| proxy→server: | &lt;   | &lt;   | &lt;   | &lt;   | &lt;   | &lt;   |
| **response** | &lt;   | &lt;   | &lt;   | &lt;   | *string* | Request success information ('success' or 'failed'). |
| **version** | &lt;   | &lt;   | &lt;   | &lt;   | *string* | Proxy version (\&lt;major&gt;.\&lt;minor&gt;.\&lt;build&gt;). |

Example:

server→proxy:

server→proxy:

```{.javascript}
{
  "request":"proxy config"
} 
```

proxy→server:

```{.javascript}
{
  "version": "6.4.0",
  "session": "0033124949800811e5686dbfd9bcea98",
  "config_revision": 0
}
```

server→proxy:

```{.javascript}
{
	"full_sync": 1,
	"data": {
		"hosts": {
			"fields": ["hostid", "host", "status", "ipmi_authtype", "ipmi_privilege", "ipmi_username", "ipmi_password", "name", "tls_connect", "tls_accept", "tls_issuer", "tls_subject", "tls_psk_identity", "tls_psk"],
			"data": [
				[10084, "Zabbix server", 0, -1, 2, "", "", "Zabbix server", 1, 1, "", "", "", ""]
			]
		},
		"interface": {
			"fields": ["interfaceid", "hostid", "main", "type", "useip", "ip", "dns", "port", "available"],
			"data": [
				[1, 10084, 1, 1, 1, "127.0.0.1", "", "10053", 1]
			]
		},
		"interface_snmp": {
			"fields": ["interfaceid", "version", "bulk", "community", "securityname", "securitylevel", "authpassphrase", "privpassphrase", "authprotocol", "privprotocol", "contextname"],
			"data": []
		},
		"host_inventory": {
			"fields": ["hostid", "type", "type_full", "name", "alias", "os", "os_full", "os_short", "serialno_a", "serialno_b", "tag", "asset_tag", "macaddress_a", "macaddress_b", "hardware", "hardware_full", "software", "software_full", "software_app_a", "software_app_b", "software_app_c", "software_app_d", "software_app_e", "contact", "location", "location_lat", "location_lon", "notes", "chassis", "model", "hw_arch", "vendor", "contract_number", "installer_name", "deployment_status", "url_a", "url_b", "url_c", "host_networks", "host_netmask", "host_router", "oob_ip", "oob_netmask", "oob_router", "date_hw_purchase", "date_hw_install", "date_hw_expiry", "date_hw_decomm", "site_address_a", "site_address_b", "site_address_c", "site_city", "site_state", "site_country", "site_zip", "site_rack", "site_notes", "poc_1_name", "poc_1_email", "poc_1_phone_a", "poc_1_phone_b", "poc_1_cell", "poc_1_screen", "poc_1_notes", "poc_2_name", "poc_2_email", "poc_2_phone_a", "poc_2_phone_b", "poc_2_cell", "poc_2_screen", "poc_2_notes"],
			"data": [
				[10084, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "56.95387", "24.22067", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""]
			]
		},
		"items": {
			"fields": ["itemid", "type", "snmp_oid", "hostid", "key_", "delay", "history", "status", "value_type", "trapper_hosts", "logtimefmt", "params", "ipmi_sensor", "authtype", "username", "password", "publickey", "privatekey", "flags", "interfaceid", "inventory_link", "jmx_endpoint", "master_itemid", "timeout", "url", "query_fields", "posts", "status_codes", "follow_redirects", "post_type", "http_proxy", "headers", "retrieve_mode", "request_method", "output_format", "ssl_cert_file", "ssl_key_file", "ssl_key_password", "verify_peer", "verify_host", "allow_traps"],
			"data": [
				[44161, 7, "", 10084, "agent.hostmetadata", "10s", "90d", 0, 1, "", "", "", "", 0, "", "", "", "", 0, null, 0, "", null, "3s", "", "", "", "200", 1, 0, "", "", 0, 0, 0, "", "", "", 0, 0, 0],
				[44162, 0, "", 10084, "agent.ping", "10s", "90d", 0, 3, "", "", "", "", 0, "", "", "", "", 0, 1, 0, "", null, "3s", "", "", "", "200", 1, 0, "", "", 0, 0, 0, "", "", "", 0, 0, 0]
			]
		},
		"item_rtdata": {
			"fields": ["itemid", "lastlogsize", "mtime"],
			"data": [
				[44161, 0, 0],
				[44162, 0, 0]
			]
		},
		"item_preproc": {
			"fields": ["item_preprocid", "itemid", "step", "type", "params", "error_handler", "error_handler_params"],
			"data": []
		},
		"item_parameter": {
			"fields": ["item_parameterid", "itemid", "name", "value"],
			"data": []
		},
		"globalmacro": {
			"fields": ["globalmacroid", "macro", "value", "type"],
			"data": [
				[2, "{$SNMP_COMMUNITY}", "public", 0]
			]
		},
		"hosts_templates": {
			"fields": ["hosttemplateid", "hostid", "templateid", "link_type"],
			"data": []
		},
		"hostmacro": {
			"fields": ["hostmacroid", "hostid", "macro", "value", "type", "automatic"],
			"data": [
				[5676, 10084, "{$M}", "AppID=zabbix_server&amp;Query=Safe=passwordSafe;Object=zabbix:Content", 2, 0]
			]
		},
		"drules": {
			"fields": ["druleid", "name", "iprange", "delay"],
			"data": [
				[2, "Local network", "127.0.0.1", "10s"]
			]
		},
		"dchecks": {
			"fields": ["dcheckid", "druleid", "type", "key_", "snmp_community", "ports", "snmpv3_securityname", "snmpv3_securitylevel", "snmpv3_authpassphrase", "snmpv3_privpassphrase", "uniq", "snmpv3_authprotocol", "snmpv3_privprotocol", "snmpv3_contextname", "host_source", "name_source"],
			"data": [
				[2, 2, 9, "system.uname", "", "10052", "", 0, "", "", 0, 0, 0, "", 1, 0]
			]
		},
		"regexps": {
			"fields": ["regexpid", "name"],
			"data": [
				[1, "File systems for discovery"],
				[2, "Network interfaces for discovery"],
				[3, "Storage devices for SNMP discovery"],
				[4, "Windows service names for discovery"],
				[5, "Windows service startup states for discovery"]
			]
		},
		"expressions": {
			"fields": ["expressionid", "regexpid", "expression", "expression_type", "exp_delimiter", "case_sensitive"],
			"data": [
				[1, 1, "^(btrfs|ext2|ext3|ext4|reiser|xfs|ffs|ufs|jfs|jfs2|vxfs|hfs|apfs|refs|ntfs|fat32|zfs)$", 3, ",", 0],
				[3, 3, "^(Physical memory|Virtual memory|Memory buffers|Cached memory|Swap space)$", 4, ",", 1],
				[5, 4, "^(MMCSS|gupdate|SysmonLog|clr_optimization_v2.0.50727_32|clr_optimization_v4.0.30319_32)$", 4, ",", 1],
				[6, 5, "^(automatic|automatic delayed)$", 3, ",", 1],
				[7, 2, "^Software Loopback Interface", 4, ",", 1],
				[8, 2, "^(In)?[Ll]oop[Bb]ack[0-9._]*$", 4, ",", 1],
				[9, 2, "^NULL[0-9.]*$", 4, ",", 1],
				[10, 2, "^[Ll]o[0-9.]*$", 4, ",", 1],
				[11, 2, "^[Ss]ystem$", 4, ",", 1],
				[12, 2, "^Nu[0-9.]*$", 4, ",", 1]
			]
		},
		"config": {
			"fields": ["configid", "snmptrap_logging", "hk_history_global", "hk_history", "autoreg_tls_accept"],
			"data": [
				[1, 1, 0, "90d", 1]
			]
		},
		"httptest": {
			"fields": ["httptestid", "name", "delay", "agent", "authentication", "http_user", "http_password", "hostid", "http_proxy", "retries", "ssl_cert_file", "ssl_key_file", "ssl_key_password", "verify_peer", "verify_host"],
			"data": []
		},
		"httptestitem": {
			"fields": ["httptestitemid", "httptestid", "itemid", "type"],
			"data": []
		},
		"httptest_field": {
			"fields": ["httptest_fieldid", "httptestid", "type", "name", "value"],
			"data": []
		},
		"httpstep": {
			"fields": ["httpstepid", "httptestid", "name", "no", "url", "timeout", "posts", "required", "status_codes", "follow_redirects", "retrieve_mode", "post_type"],
			"data": []
		},
		"httpstepitem": {
			"fields": ["httpstepitemid", "httpstepid", "itemid", "type"],
			"data": []
		},
		"httpstep_field": {
			"fields": ["httpstep_fieldid", "httpstepid", "type", "name", "value"],
			"data": []
		},
		"config_autoreg_tls": {
			"fields": ["autoreg_tlsid", "tls_psk_identity", "tls_psk"],
			"data": [
				[1, "", ""]
			]
		}
	},
	"macro.secrets": {
		"AppID=zabbix_server&amp;Query=Safe=passwordSafe;Object=zabbix": {
			"Content": "738"
		}
	},
	"config_revision": 2
}
```

proxy→server:

```{.javascript}
{
  "response": "success",
  "version": "6.4.0"
}
```</source>
      </trans-unit>
      <trans-unit id="c348e5d9" xml:space="preserve">
        <source>##### Data request

The `proxy data` request is used to obtain host interface availability,
historical, discovery and autoregistration data from proxy. This request
is sent every `ProxyDataFrequency` (server configuration parameter)
seconds.

|name|&lt;|value type|description|
|-|----------|----------|----------------------------------------|
|server→proxy:|&lt;|&lt;|&lt;|
|**request**|&lt;|*string*|'proxy data'|
|proxy→server:|&lt;|&lt;|&lt;|
|**session**|&lt;|*string*|Data session token.|
|**interface availability**|&lt;|*array*|*(optional)* Array of interface availability data objects.|
| |**interfaceid**|*number*|Interface identifier.|
|^|**available**|*number*|Interface availability:&lt;br&gt;&lt;br&gt;**0**, *INTERFACE\_AVAILABLE\_UNKNOWN* - unknown&lt;br&gt;**1**, *INTERFACE\_AVAILABLE\_TRUE* - available&lt;br&gt;**2**, *INTERFACE\_AVAILABLE\_FALSE* - unavailable|
|^|**error**|*string*|Interface error message or empty string.|
|**history data**|&lt;|*array*|*(optional)* Array of history data objects.|
| |**itemid**|*number*|Item identifier.|
|^|**clock**|*number*|Item value timestamp (seconds).|
|^|**ns**|*number*|Item value timestamp (nanoseconds).|
|^|**value**|*string*|*(optional)* Item value.|
|^|**id**|*number*|Value identifier (ascending counter, unique within one data session).|
|^|**timestamp**|*number*|*(optional)* Timestamp of log type items.|
|^|**source**|*string*|*(optional)* Eventlog item source value.|
|^|**severity**|*number*|*(optional)* Eventlog item severity value.|
|^|**eventid**|*number*|*(optional)* Eventlog item eventid value.|
|^|**state**|*string*|*(optional)* Item state:&lt;br&gt;**0**, *ITEM\_STATE\_NORMAL*&lt;br&gt;**1**, *ITEM\_STATE\_NOTSUPPORTED*|
|^|**lastlogsize**|*number*|*(optional)* Last log size of log type items.|
|^|**mtime**|*number*|*(optional)* Modification time of log type items.|
|**discovery data**|&lt;|*array*|*(optional)* Array of discovery data objects.|
| |**clock**|*number*|Discovery data timestamp.|
|^|**druleid**|*number*|Discovery rule identifier.|
|^|**dcheckid**|*number*|Discovery check identifier or null for discovery rule data.|
|^|**type**|*number*|Discovery check type:&lt;br&gt;&lt;br&gt;**-1** discovery rule data&lt;br&gt;**0**, *SVC\_SSH* - SSH service check&lt;br&gt;**1**, *SVC\_LDAP* - LDAP service check&lt;br&gt;**2**, *SVC\_SMTP* - SMTP service check&lt;br&gt;**3**, *SVC\_FTP* - FTP service check&lt;br&gt;**4**, *SVC\_HTTP* - HTTP service check&lt;br&gt;**5**, *SVC\_POP* - POP service check&lt;br&gt;**6**, *SVC\_NNTP* - NNTP service check&lt;br&gt;**7**, *SVC\_IMAP* - IMAP service check&lt;br&gt;**8**, *SVC\_TCP* - TCP port availability check&lt;br&gt;**9**, *SVC\_AGENT* - Zabbix agent&lt;br&gt;**10**, *SVC\_SNMPv1* - SNMPv1 agent&lt;br&gt;**11**, *SVC\_SNMPv2* - SNMPv2 agent&lt;br&gt;**12**, *SVC\_ICMPPING* - ICMP ping&lt;br&gt;**13**, *SVC\_SNMPv3* - SNMPv3 agent&lt;br&gt;**14**, *SVC\_HTTPS* - HTTPS service check&lt;br&gt;**15**, *SVC\_TELNET* - Telnet availability check|
|^|**ip**|*string*|Host IP address.|
|^|**dns**|*string*|Host DNS name.|
|^|**port**|*number*|*(optional)* Service port number.|
|^|**key\_**|*string*|*(optional)* Item key for discovery check of type **9** *SVC\_AGENT*|
|^|**value**|*string*|*(optional)* Value received from the service, can be empty for most of services.|
|^|**status**|*number*|*(optional)* Service status:&lt;br&gt;&lt;br&gt;**0**, *DOBJECT\_STATUS\_UP* - Service UP&lt;br&gt;**1**, *DOBJECT\_STATUS\_DOWN* - Service DOWN|
|**auto registration**|&lt;|*array*|*(optional)* Array of autoregistration data objects.|
| |**clock**|*number*|Autoregistration data timestamp.|
|^|**host**|*string*|Host name.|
|^|**ip**|*string*|*(optional)* Host IP address.|
|^|**dns**|*string*|*(optional)* Resolved DNS name from IP address.|
|^|**port**|*string*|*(optional)* Host port.|
|^|**host\_metadata**|*string*|*(optional)* Host metadata sent by agent (based on HostMetadata or HostMetadataItem agent configuration parameter).|
|**tasks**|&lt;|*array*|*(optional)* Array of tasks.|
| |**type**|*number*|Task type:&lt;br&gt;&lt;br&gt;**0**, *ZBX\_TM\_TASK\_PROCESS\_REMOTE\_COMMAND\_RESULT* - remote command result|
|^|**status**|*number*|Remote-command execution status:&lt;br&gt;&lt;br&gt;**0**, *ZBX\_TM\_REMOTE\_COMMAND\_COMPLETED* - remote command completed successfully&lt;br&gt;**1**, *ZBX\_TM\_REMOTE\_COMMAND\_FAILED* - remote command failed|
|^|**error**|*string*|*(optional)* Error message.|
|^|**parent\_taskid**|*number*|Parent task ID.|
|**more**|&lt;|*number*|*(optional)* 1 - there are more history data to send.|
|**clock**|&lt;|*number*|*(optional)* Data transfer timestamp (seconds).|
|**ns**|&lt;|*number*|*(optional)* Data transfer timestamp (nanoseconds).|
|**version**|&lt;|*string*|Proxy version (&lt;major&gt;.&lt;minor&gt;.&lt;build&gt;).|
|server→proxy:|&lt;|&lt;|&lt;|
|**response**|&lt;|*string*|Request success information ('success' or 'failed').|
|**tasks**|&lt;|*array*|*(optional)* Array of tasks.|
| |**type**|*number*|Task type:&lt;br&gt;&lt;br&gt;**1**, *ZBX\_TM\_TASK\_PROCESS\_REMOTE\_COMMAND* - remote command|
|^|**clock**|*number*|Task creation time.|
|^|**ttl**|*number*|Time in seconds after which the task expires.|
|^|**commandtype**|*number*|Remote-command type:&lt;br&gt;&lt;br&gt;**0**, *ZBX\_SCRIPT\_TYPE\_CUSTOM\_SCRIPT* - use custom script&lt;br&gt;**1**, *ZBX\_SCRIPT\_TYPE\_IPMI* - use IPMI&lt;br&gt;**2**, *ZBX\_SCRIPT\_TYPE\_SSH* - use SSH&lt;br&gt;**3**, *ZBX\_SCRIPT\_TYPE\_TELNET* - use Telnet&lt;br&gt;**4**, *ZBX\_SCRIPT\_TYPE\_GLOBAL\_SCRIPT* - use global script (currently functionally equivalent to custom script)|
|^|**command**|*string*|Remote command to execute.|
|^|**execute\_on**|*number*|Execution target for custom scripts:&lt;br&gt;&lt;br&gt;**0**, *ZBX\_SCRIPT\_EXECUTE\_ON\_AGENT* - execute script on agent&lt;br&gt;**1**, *ZBX\_SCRIPT\_EXECUTE\_ON\_SERVER* - execute script on server&lt;br&gt;**2**, *ZBX\_SCRIPT\_EXECUTE\_ON\_PROXY* - execute script on proxy|
|^|**port**|*number*|*(optional)* Port for Telnet and SSH commands.|
|^|**authtype**|*number*|*(optional)* Authentication type for SSH commands.|
|^|**username**|*string*|*(optional)* User name for Telnet and SSH commands.|
|^|**password**|*string*|*(optional)* Password for Telnet and SSH commands.|
|^|**publickey**|*string*|*(optional)* Public key for SSH commands.|
|^|**privatekey**|*string*|*(optional)* Private key for SSH commands.|
|^|**parent\_taskid**|*number*|Parent task ID.|
|^|**hostid**|*number*|Target host ID.|

Example:

server→proxy:

``` {.javascript}
{
  "request": "proxy data"
}
```

proxy→server:

``` {.javascript}
{
    "session": "12345678901234567890123456789012"
    "interface availability": [
        {
            "interfaceid": 1,
            "available": 1,
            "error": ""
    },
        {
            "interfaceid": 2,
            "available": 2,
            "error": "Get value from agent failed: cannot connect to [[127.0.0.1]:10049]: [111] Connection refused"
    },
        {
            "interfaceid": 3,
            "available": 1,
            "error": ""
    },
        {
            "interfaceid": 4,
            "available": 1,
            "error": ""
    }
    ],
    "history data":[
        {
            "itemid":"12345",
            "clock":1478609647,
            "ns":332510044,
            "value":"52956612",
            "id": 1
        },
        {
            "itemid":"12346",
            "clock":1478609647,
            "ns":330690279,
            "state":1,
            "value":"Cannot find information for this network interface in /proc/net/dev.",
            "id": 2
        }
    ],
    "discovery data":[
        {
            "clock":1478608764,
            "drule":2,
            "dcheck":3,
            "type":12,
            "ip":"10.3.0.10",
            "dns":"vdebian",
            "status":1
        },
        {
            "clock":1478608764,
            "drule":2,
            "dcheck":null,
            "type":-1,
            "ip":"10.3.0.10",
            "dns":"vdebian",
            "status":1
        }
    ],
    "auto registration":[
        {
            "clock":1478608371,
            "host":"Logger1",
            "ip":"10.3.0.1",
            "dns":"localhost",
            "port":"10050"
        },
        {
            "clock":1478608381,
            "host":"Logger2",
            "ip":"10.3.0.2",
            "dns":"localhost",
            "port":"10050"
        }
    ],
    "tasks":[
        {
            "type": 0,
            "status": 0,
            "parent_taskid": 10
        },
        {
            "type": 0,
            "status": 1,
            "error": "No permissions to execute task.",
            "parent_taskid": 20
        }
    ],
    "version":"6.4.0"
}
```

server→proxy:

``` {.javascript}
{
  "response": "success",
  "tasks":[
      {
         "type": 1,
         "clock": 1478608371,
         "ttl": 600,
         "commandtype": 2,
         "command": "restart_service1.sh",
         "execute_on": 2,
         "port": 80,
         "authtype": 0,
         "username": "userA",
         "password": "password1",
         "publickey": "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCqGKukO1De7zhZj6+H0qtjTkVxwTCpvKe",
         "privatekey": "lsuusFncCzWBQ7RKNUSesmQRMSGkVb1/3j+skZ6UtW+5u09lHNsj6tQ5QCqGKukO1De7zhd",
         "parent_taskid": 10,
         "hostid": 10070
      },
      {
         "type": 1,
         "clock": 1478608381,
         "ttl": 600,
         "commandtype": 1,
         "command": "restart_service2.sh",
         "execute_on": 0,
         "authtype": 0,
         "username": "",
         "password": "",
         "publickey": "",
         "privatekey": "",
         "parent_taskid": 20,
         "hostid": 10084
      }
  ]
}
```</source>
      </trans-unit>
      <trans-unit id="967ea060" xml:space="preserve">
        <source>#### Active proxy</source>
      </trans-unit>
      <trans-unit id="bed30a3b" xml:space="preserve">
        <source>##### Configuration request

The `proxy config` request is sent by active proxy to obtain proxy
configuration data. This request is sent every `ProxyConfigFrequency` (proxy
configuration parameter) seconds.

| name | &lt;   | &lt;   | &lt;   | &lt;   | value type | description |
|-|-|-|-|----------|----------|----------------------------------------|
| proxy→server: | &lt;   | &lt;   | &lt;   | &lt;   | &lt;   | &lt;   |
| **request** | &lt;   | &lt;   | &lt;   | &lt;   | *string* | 'proxy config' |
| **host** | &lt;   | &lt;   | &lt;   | &lt;   | *string&lt;br&gt;* | Proxy name. |
| **version** | &lt;   | &lt;   | &lt;   | &lt;   | *string* | Proxy version (\&lt;major&gt;.\&lt;minor&gt;.\&lt;build&gt;). |
| **session** | &lt;   | &lt;   | &lt;   | &lt;   | *string* | Proxy configuration session token. |
| **config_revision** | &lt;   | &lt;   | &lt;   | &lt;   | *number* | Proxy configuration revision. |
|     | &lt;   | &lt;   | &lt;   | &lt;   | &lt;   | &lt;   |
| server→proxy: | &lt;   | &lt;   | &lt;   | &lt;   | &lt;   | &lt;   |
| **fullsync** | &lt;   | &lt;   | &lt;   | &lt;   | *number* | 1 - if full configuration data is sent, absent otherwise (optional). |
| **data** | &lt;   | &lt;   | &lt;   | &lt;   | *array* | Object of table data. Absent if configuration has not been changed (optional). |
|     | **\&lt;table&gt;** | &lt;   | &lt;   | &lt;   | *object* | One or more objects with \&lt;table&gt; data (optional, depending on changes). |
| ^   |     | **fields** | &lt;   | &lt;   | *array* | Array of field names. |
| ^   | ^   |     | -   | &lt;   | *string* | Field name. |
| ^   | ^   | **data** | &lt;   | &lt;   | *array* | Array of rows. |
| ^   | ^   |     | -   | &lt;   | *array* | Array of columns. |
| ^   | ^   | ^   |     | -   | *string*,*number* | Column value with type depending on column type in database schema. |
|     | &lt;   | &lt;   | &lt;   | &lt;   | &lt;   | &lt;   |
| **macro.secrets** | &lt;   | &lt;   | &lt;   | &lt;  | *object* | Secret macro information, absent if there are no changes in vault macros  (optional). |
| **config_revision** | &lt;   | &lt;   | &lt;   | &lt;   | *number* | Configuration cache revision - sent with configuration data (optional). |
| **del_hostids** | &lt;   | &lt;   | &lt;   | &lt;   | *array* | Array of removed hostids (optional). |
|     | -   | &lt;   | &lt;   | &lt;   | *number* | Host identifier. |
| **del_macro_hostids** | &lt;   | &lt;   | &lt;   | &lt;   | *array* | Array of hostids with all macros removed (optional). |
|     | -   | &lt;   | &lt;   | &lt;   | *number* | Host identifier. |

Example:

proxy→server:

```{.javascript}
{
  "request": "proxy config",
  "host": "Zabbix proxy",
  "version":"6.4.0",
  "session": "fd59a09ff4e9d1fb447de1f04599bcf6",
  "config_revision": 0
}
```

server→proxy:

```{.javascript}
{
	"full_sync": 1,
	"data": {
		"hosts": {
			"fields": ["hostid", "host", "status", "ipmi_authtype", "ipmi_privilege", "ipmi_username", "ipmi_password", "name", "tls_connect", "tls_accept", "tls_issuer", "tls_subject", "tls_psk_identity", "tls_psk"],
			"data": [
				[10084, "Zabbix server", 0, -1, 2, "", "", "Zabbix server", 1, 1, "", "", "", ""]
			]
		},
		"interface": {
			"fields": ["interfaceid", "hostid", "main", "type", "useip", "ip", "dns", "port", "available"],
			"data": [
				[1, 10084, 1, 1, 1, "127.0.0.1", "", "10053", 1]
			]
		},
		"interface_snmp": {
			"fields": ["interfaceid", "version", "bulk", "community", "securityname", "securitylevel", "authpassphrase", "privpassphrase", "authprotocol", "privprotocol", "contextname"],
			"data": []
		},
		"host_inventory": {
			"fields": ["hostid", "type", "type_full", "name", "alias", "os", "os_full", "os_short", "serialno_a", "serialno_b", "tag", "asset_tag", "macaddress_a", "macaddress_b", "hardware", "hardware_full", "software", "software_full", "software_app_a", "software_app_b", "software_app_c", "software_app_d", "software_app_e", "contact", "location", "location_lat", "location_lon", "notes", "chassis", "model", "hw_arch", "vendor", "contract_number", "installer_name", "deployment_status", "url_a", "url_b", "url_c", "host_networks", "host_netmask", "host_router", "oob_ip", "oob_netmask", "oob_router", "date_hw_purchase", "date_hw_install", "date_hw_expiry", "date_hw_decomm", "site_address_a", "site_address_b", "site_address_c", "site_city", "site_state", "site_country", "site_zip", "site_rack", "site_notes", "poc_1_name", "poc_1_email", "poc_1_phone_a", "poc_1_phone_b", "poc_1_cell", "poc_1_screen", "poc_1_notes", "poc_2_name", "poc_2_email", "poc_2_phone_a", "poc_2_phone_b", "poc_2_cell", "poc_2_screen", "poc_2_notes"],
			"data": [
				[10084, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "56.95387", "24.22067", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""]
			]
		},
		"items": {
			"fields": ["itemid", "type", "snmp_oid", "hostid", "key_", "delay", "history", "status", "value_type", "trapper_hosts", "logtimefmt", "params", "ipmi_sensor", "authtype", "username", "password", "publickey", "privatekey", "flags", "interfaceid", "inventory_link", "jmx_endpoint", "master_itemid", "timeout", "url", "query_fields", "posts", "status_codes", "follow_redirects", "post_type", "http_proxy", "headers", "retrieve_mode", "request_method", "output_format", "ssl_cert_file", "ssl_key_file", "ssl_key_password", "verify_peer", "verify_host", "allow_traps"],
			"data": [
				[44161, 7, "", 10084, "agent.hostmetadata", "10s", "90d", 0, 1, "", "", "", "", 0, "", "", "", "", 0, null, 0, "", null, "3s", "", "", "", "200", 1, 0, "", "", 0, 0, 0, "", "", "", 0, 0, 0],
				[44162, 0, "", 10084, "agent.ping", "10s", "90d", 0, 3, "", "", "", "", 0, "", "", "", "", 0, 1, 0, "", null, "3s", "", "", "", "200", 1, 0, "", "", 0, 0, 0, "", "", "", 0, 0, 0]
			]
		},
		"item_rtdata": {
			"fields": ["itemid", "lastlogsize", "mtime"],
			"data": [
				[44161, 0, 0],
				[44162, 0, 0]
			]
		},
		"item_preproc": {
			"fields": ["item_preprocid", "itemid", "step", "type", "params", "error_handler", "error_handler_params"],
			"data": []
		},
		"item_parameter": {
			"fields": ["item_parameterid", "itemid", "name", "value"],
			"data": []
		},
		"globalmacro": {
			"fields": ["globalmacroid", "macro", "value", "type"],
			"data": [
				[2, "{$SNMP_COMMUNITY}", "public", 0]
			]
		},
		"hosts_templates": {
			"fields": ["hosttemplateid", "hostid", "templateid", "link_type"],
			"data": []
		},
		"hostmacro": {
			"fields": ["hostmacroid", "hostid", "macro", "value", "type", "automatic"],
			"data": [
				[5676, 10084, "{$M}", "AppID=zabbix_server&amp;Query=Safe=passwordSafe;Object=zabbix:Content", 2, 0]
			]
		},
		"drules": {
			"fields": ["druleid", "name", "iprange", "delay"],
			"data": [
				[2, "Local network", "127.0.0.1", "10s"]
			]
		},
		"dchecks": {
			"fields": ["dcheckid", "druleid", "type", "key_", "snmp_community", "ports", "snmpv3_securityname", "snmpv3_securitylevel", "snmpv3_authpassphrase", "snmpv3_privpassphrase", "uniq", "snmpv3_authprotocol", "snmpv3_privprotocol", "snmpv3_contextname", "host_source", "name_source"],
			"data": [
				[2, 2, 9, "system.uname", "", "10052", "", 0, "", "", 0, 0, 0, "", 1, 0]
			]
		},
		"regexps": {
			"fields": ["regexpid", "name"],
			"data": [
				[1, "File systems for discovery"],
				[2, "Network interfaces for discovery"],
				[3, "Storage devices for SNMP discovery"],
				[4, "Windows service names for discovery"],
				[5, "Windows service startup states for discovery"]
			]
		},
		"expressions": {
			"fields": ["expressionid", "regexpid", "expression", "expression_type", "exp_delimiter", "case_sensitive"],
			"data": [
				[1, 1, "^(btrfs|ext2|ext3|ext4|reiser|xfs|ffs|ufs|jfs|jfs2|vxfs|hfs|apfs|refs|ntfs|fat32|zfs)$", 3, ",", 0],
				[3, 3, "^(Physical memory|Virtual memory|Memory buffers|Cached memory|Swap space)$", 4, ",", 1],
				[5, 4, "^(MMCSS|gupdate|SysmonLog|clr_optimization_v2.0.50727_32|clr_optimization_v4.0.30319_32)$", 4, ",", 1],
				[6, 5, "^(automatic|automatic delayed)$", 3, ",", 1],
				[7, 2, "^Software Loopback Interface", 4, ",", 1],
				[8, 2, "^(In)?[Ll]oop[Bb]ack[0-9._]*$", 4, ",", 1],
				[9, 2, "^NULL[0-9.]*$", 4, ",", 1],
				[10, 2, "^[Ll]o[0-9.]*$", 4, ",", 1],
				[11, 2, "^[Ss]ystem$", 4, ",", 1],
				[12, 2, "^Nu[0-9.]*$", 4, ",", 1]
			]
		},
		"config": {
			"fields": ["configid", "snmptrap_logging", "hk_history_global", "hk_history", "autoreg_tls_accept"],
			"data": [
				[1, 1, 0, "90d", 1]
			]
		},
		"httptest": {
			"fields": ["httptestid", "name", "delay", "agent", "authentication", "http_user", "http_password", "hostid", "http_proxy", "retries", "ssl_cert_file", "ssl_key_file", "ssl_key_password", "verify_peer", "verify_host"],
			"data": []
		},
		"httptestitem": {
			"fields": ["httptestitemid", "httptestid", "itemid", "type"],
			"data": []
		},
		"httptest_field": {
			"fields": ["httptest_fieldid", "httptestid", "type", "name", "value"],
			"data": []
		},
		"httpstep": {
			"fields": ["httpstepid", "httptestid", "name", "no", "url", "timeout", "posts", "required", "status_codes", "follow_redirects", "retrieve_mode", "post_type"],
			"data": []
		},
		"httpstepitem": {
			"fields": ["httpstepitemid", "httpstepid", "itemid", "type"],
			"data": []
		},
		"httpstep_field": {
			"fields": ["httpstep_fieldid", "httpstepid", "type", "name", "value"],
			"data": []
		},
		"config_autoreg_tls": {
			"fields": ["autoreg_tlsid", "tls_psk_identity", "tls_psk"],
			"data": [
				[1, "", ""]
			]
		}
	},
	"macro.secrets": {
		"AppID=zabbix_server&amp;Query=Safe=passwordSafe;Object=zabbix": {
			"Content": "738"
		}
	},
	"config_revision": 2
}
```</source>
      </trans-unit>
      <trans-unit id="ef0ba621" xml:space="preserve">
        <source>##### Data request

The `proxy data` request is sent by proxy to provide host interface availability, history, discovery and autoregistration data.
This request is sent every `DataSenderFrequency` (proxy configuration parameter) seconds.
Note that active proxy will still poll Zabbix server every second for remote command tasks (with an empty `proxy data` request).

|name|&lt;|value type|description|
|-|----------|----------|----------------------------------------|
|proxy→server:|&lt;|&lt;|&lt;|
|**request**|&lt;|*string*|'proxy data'|
|**host**|&lt;|*string*|Proxy name.|
|**session**|&lt;|*string*|Data session token.|
|**interface availability**|&lt;|*array*|*(optional)* Array of interface availability data objects.|
| |**interfaceid**|*number*|Interface identifier.|
|^|**available**|*number*|Interface availability:&lt;br&gt;&lt;br&gt;**0**, *INTERFACE\_AVAILABLE\_UNKNOWN* - unknown&lt;br&gt;**1**, *INTERFACE\_AVAILABLE\_TRUE* - available&lt;br&gt;**2**, *INTERFACE\_AVAILABLE\_FALSE* - unavailable|
|^|**error**|*string*|Interface error message or empty string.|
|**history data**|&lt;|*array*|*(optional)* Array of history data objects.|
| |**itemid**|*number*|Item identifier.|
|^|**clock**|*number*|Item value timestamp (seconds).|
|^|**ns**|*number*|Item value timestamp (nanoseconds).|
|^|**value**|*string*|*(optional)* Item value.|
|^|**id**|*number*|Value identifier (ascending counter, unique within one data session).|
|^|**timestamp**|*number*|*(optional)* Timestamp of log type items.|
|^|**source**|*string*|*(optional)* Eventlog item source value.|
|^|**severity**|*number*|*(optional)* Eventlog item severity value.|
|^|**eventid**|*number*|*(optional)* Eventlog item eventid value.|
|^|**state**|*string*|*(optional)* Item state:&lt;br&gt;**0**, *ITEM\_STATE\_NORMAL*&lt;br&gt;**1**, *ITEM\_STATE\_NOTSUPPORTED*|
|^|**lastlogsize**|*number*|*(optional)* Last log size of log type items.|
|^|**mtime**|*number*|*(optional)* Modification time of log type items.|
|**discovery data**|&lt;|*array*|*(optional)* Array of discovery data objects.|
| |**clock**|*number*|Discovery data timestamp.|
|^|**druleid**|*number*|Discovery rule identifier.|
|^|**dcheckid**|*number*|Discovery check identifier or null for discovery rule data.|
|^|**type**|*number*|Discovery check type:&lt;br&gt;&lt;br&gt;**-1** discovery rule data&lt;br&gt;**0**, *SVC\_SSH* - SSH service check&lt;br&gt;**1**, *SVC\_LDAP* - LDAP service check&lt;br&gt;**2**, *SVC\_SMTP* - SMTP service check&lt;br&gt;**3**, *SVC\_FTP* - FTP service check&lt;br&gt;**4**, *SVC\_HTTP* - HTTP service check&lt;br&gt;**5**, *SVC\_POP* - POP service check&lt;br&gt;**6**, *SVC\_NNTP* - NNTP service check&lt;br&gt;**7**, *SVC\_IMAP* - IMAP service check&lt;br&gt;**8**, *SVC\_TCP* - TCP port availability check&lt;br&gt;**9**, *SVC\_AGENT* - Zabbix agent&lt;br&gt;**10**, *SVC\_SNMPv1* - SNMPv1 agent&lt;br&gt;**11**, *SVC\_SNMPv2* - SNMPv2 agent&lt;br&gt;**12**, *SVC\_ICMPPING* - ICMP ping&lt;br&gt;**13**, *SVC\_SNMPv3* - SNMPv3 agent&lt;br&gt;**14**, *SVC\_HTTPS* - HTTPS service check&lt;br&gt;**15**, *SVC\_TELNET* - Telnet availability check|
|^|**ip**|*string*|Host IP address.|
|^|**dns**|*string*|Host DNS name.|
|^|**port**|*number*|*(optional)* Service port number.|
|^|**key\_**|*string*|*(optional)* Item key for discovery check of type **9** *SVC\_AGENT*|
|^|**value**|*string*|*(optional)* Value received from the service, can be empty for most services.|
|^|**status**|*number*|*(optional)* Service status:&lt;br&gt;&lt;br&gt;**0**, *DOBJECT\_STATUS\_UP* - Service UP&lt;br&gt;**1**, *DOBJECT\_STATUS\_DOWN* - Service DOWN|
|**autoregistration**|&lt;|*array*|*(optional)* Array of autoregistration data objects.|
| |**clock**|*number*|Autoregistration data timestamp.|
|^|**host**|*string*|Host name.|
|^|**ip**|*string*|*(optional)* Host IP address.|
|^|**dns**|*string*|*(optional)* Resolved DNS name from IP address.|
|^|**port**|*string*|*(optional)* Host port.|
|^|**host\_metadata**|*string*|*(optional)* Host metadata sent by agent (based on HostMetadata or HostMetadataItem agent configuration parameter).|
|**tasks**|&lt;|*array*|*(optional)* Array of tasks.|
| |**type**|*number*|Task type:&lt;br&gt;&lt;br&gt;**0**, *ZBX\_TM\_TASK\_PROCESS\_REMOTE\_COMMAND\_RESULT* - remote command result|
|^|**status**|*number*|Remote-command execution status:&lt;br&gt;&lt;br&gt;**0**, *ZBX\_TM\_REMOTE\_COMMAND\_COMPLETED* - remote command completed successfully&lt;br&gt;**1**, *ZBX\_TM\_REMOTE\_COMMAND\_FAILED* - remote command failed|
|^|**error**|*string*|*(optional)* Error message.|
|^|**parent\_taskid**|*number*|Parent task ID.|
|**more**|&lt;|*number*|*(optional)* 1 - there are more history data to send|
|**clock**|&lt;|*number*|*(optional)* Data transfer timestamp (seconds).|
|**ns**|&lt;|*number*|*(optional)* Data transfer timestamp (nanoseconds).|
|**version**|&lt;|*string*|Proxy version (&lt;major&gt;.&lt;minor&gt;.&lt;build&gt;).|
|server→proxy:|&lt;|&lt;|&lt;|
|**response**|&lt;|*string*|Request success information ('success' or 'failed').|
|**upload**|&lt;|*string*|Upload control for historical data (history, autoregistration, host availability, network discovery).&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;**enabled** - normal operation&lt;br&gt;**disabled** - server is not accepting data (possibly due to internal cache over limit)|
|**tasks**|&lt;|*array*|*(optional)* Array of tasks.|
| |**type**|*number*|Task type:&lt;br&gt;&lt;br&gt;**1**, *ZBX\_TM\_TASK\_PROCESS\_REMOTE\_COMMAND* - remote command|
|^|**clock**|*number*|Task creation time.|
|^|**ttl**|*number*|Time in seconds after which the task expires.|
|^|**commandtype**|*number*|Remote-command type:&lt;br&gt;&lt;br&gt;**0**, *ZBX\_SCRIPT\_TYPE\_CUSTOM\_SCRIPT* - use custom script&lt;br&gt;**1**, *ZBX\_SCRIPT\_TYPE\_IPMI* - use IPMI&lt;br&gt;**2**, *ZBX\_SCRIPT\_TYPE\_SSH* - use SSH&lt;br&gt;**3**, *ZBX\_SCRIPT\_TYPE\_TELNET* - use Telnet&lt;br&gt;**4**, *ZBX\_SCRIPT\_TYPE\_GLOBAL\_SCRIPT* - use global script (currently functionally equivalent to custom script)|
|^|**command**|*string*|Remote command to execute.|
|^|**execute\_on**|*number*|Execution target for custom scripts:&lt;br&gt;&lt;br&gt;**0**, *ZBX\_SCRIPT\_EXECUTE\_ON\_AGENT* - execute script on agent&lt;br&gt;**1**, *ZBX\_SCRIPT\_EXECUTE\_ON\_SERVER* - execute script on server&lt;br&gt;**2**, *ZBX\_SCRIPT\_EXECUTE\_ON\_PROXY* - execute script on proxy|
|^|**port**|*number*|*(optional)* Port for Telnet and SSH commands.|
|^|**authtype**|*number*|*(optional)* Authentication type for SSH commands.|
|^|**username**|*string*|*(optional)* User name for Telnet and SSH commands.|
|^|**password**|*string*|*(optional)* Password for Telnet and SSH commands.|
|^|**publickey**|*string*|*(optional)* Public key for SSH commands.|
|^|**privatekey**|*string*|*(optional)* Private key for SSH commands.|
|^|**parent\_taskid**|*number*|Parent task ID.|
|^|**hostid**|*number*|Target host ID.|

Example:

proxy→server:

``` {.javascript}
{
	"request": "proxy data",
	"host": "Zabbix proxy",
	"session": "818cdd1b537bdc5e50c09ed4969235b6",
	"interface availability": [{
		"interfaceid": 1,
		"available": 1,
		"error": ""
	}],
	"history data": [{
		"id": 1114,
		"itemid": 44162,
		"clock": 1665730632,
		"ns": 798953105,
		"value": "1"
	}, {
		"id": 1115,
		"itemid": 44161,
		"clock": 1665730633,
		"ns": 811684663,
		"value": "58"
	}],
	"auto registration": [{
		"clock": 1665730633,
		"host": "Zabbix server",
		"ip": "127.0.0.1",
		"dns": "localhost",
		"port": "10053",
		"host_metadata": "58",
		"tls_accepted": 1
	}],
	"discovery data": [{
		"clock": 1665732232,
		"drule": 2,
		"dcheck": 2,
		"ip": "127.0.0.1",
		"dns": "localhost",
		"port": 10052,
		"status": 1
	}, {
		"clock": 1665732232,
		"drule": 2,
		"dcheck": null,
		"ip": "127.0.0.1",
		"dns": "localhost",
		"status": 1
	}],
	"host data": [{
		"hostid": 10084,
		"active_status": 1
	}],
	"tasks": [{
		"type": 3,
		"clock": 1665730985,
		"ttl": 0,
		"status": -1,
		"info": "Remote commands are not enabled",
		"parent_taskid": 3
	}],
	"version": "6.4.0",
	"clock": 1665730643,
	"ns": 65389964
}
```

server→proxy:

``` {.javascript}
{
	"upload": "enabled",
	"response": "success",
	"tasks": [{
		"type": 2,
		"clock": 1665730986,
		"ttl": 600,
		"commandtype": 0,
		"command": "ping -c 3 127.0.0.1; case $? in [01]) true;; *) false;; esac",
		"execute_on": 2,
		"port": 0,
		"authtype": 0,
		"username": "",
		"password": "",
		"publickey": "",
		"privatekey": "",
		"alertid": 0,
		"parent_taskid": 4,
		"hostid": 10084
	}]
}
```</source>
      </trans-unit>
    </body>
  </file>
</xliff>
