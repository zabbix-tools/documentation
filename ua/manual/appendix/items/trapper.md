[comment]: # translation:outdated

[comment]: # ({new-1602be12})
# 4 Trapper items

[comment]: # ({/new-1602be12})

[comment]: # ({new-ea58f0b6})
#### Overview

Zabbix server uses a JSON- based communication protocol for receiving
data from Zabbix sender with the help of [trapper
item](/manual/config/items/itemtypes/trapper).

Request and response messages must begin with [header and data
length](/manual/appendix/protocols/header_datalen).

[comment]: # ({/new-ea58f0b6})

[comment]: # ({new-51cd3761})
#### Zabbix sender request

``` {.javascript}
{
    "request":"sender data",
    "data":[
        {
            "host":"<hostname>",
            "key":"trap",
            "value":"test value"
        }
    ]
}
```

[comment]: # ({/new-51cd3761})

[comment]: # ({new-c4e3e287})
#### Zabbix server response

``` {.javascript}
{
    "response":"success",
    "info":"processed: 1; failed: 0; total: 1; seconds spent: 0.060753"
}
```

[comment]: # ({/new-c4e3e287})

[comment]: # ({new-64bfec01})
#### Zabbix sender request with a timestamp

Alternatively Zabbix sender can send a request with a timestamp and
nanoseconds.

``` {.javascript}
{
    "request":"sender data",
    "data":[
        {
            "host":"<hostname>",
            "key":"trap",
            "value":"test value",
            "clock":1516710794,
            "ns":592397170
        },
        {
            "host":"<hostname>",
            "key":"trap",
            "value":"test value",
            "clock":1516710795,
            "ns":192399456
        }
    ],
    "clock":1516712029,
    "ns":873386094
}
```

[comment]: # ({/new-64bfec01})

[comment]: # ({new-c71e91fd})
#### Zabbix server response

``` {.javascript}
{
    "response":"success",
    "info":"processed: 2; failed: 0; total: 2; seconds spent: 0.060904"
}
```

[comment]: # ({/new-c71e91fd})
