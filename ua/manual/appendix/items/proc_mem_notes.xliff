<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="ua" datatype="plaintext" original="manual/appendix/items/proc_mem_notes.md">
    <body>
      <trans-unit id="098d0431" xml:space="preserve">
        <source># 8 Notes on memtype parameter in proc.mem items</source>
      </trans-unit>
      <trans-unit id="2840c113" xml:space="preserve">
        <source>#### Overview

The **memtype** parameter is supported on Linux, AIX, FreeBSD, and
Solaris platforms.

Three common values of 'memtype' are supported on all of these
platforms: `pmem`, `rss` and `vsize`. Additionally, platform-specific
'memtype' values are supported on some platforms.</source>
      </trans-unit>
      <trans-unit id="04a03696" xml:space="preserve">
        <source>#### AIX

See values supported for 'memtype' parameter on AIX in the table.

|Supported value|Description|Source in procentry64 structure|Tries to be compatible with|
|--|------|--|--|
|vsize ^[1](proc_mem_notes#footnotes)^|Virtual memory size|pi\_size| |
|pmem|Percentage of real memory|pi\_prm|ps -o pmem|
|rss|Resident set size|pi\_trss + pi\_drss|ps -o rssize|
|size|Size of process (code + data)|pi\_dvm|"ps gvw" SIZE column|
|dsize|Data size|pi\_dsize|&lt;|
|tsize|Text (code) size|pi\_tsize|"ps gvw" TSIZ column|
|sdsize|Data size from shared library|pi\_sdsize|&lt;|
|drss|Data resident set size|pi\_drss| |
|trss|Text resident set size|pi\_trss| |</source>
      </trans-unit>
      <trans-unit id="cc0df248" xml:space="preserve">
        <source>
Notes for AIX:

1. When choosing parameters for proc.mem[] item key on AIX, try to specify narrow process selection criteria. Otherwise there is a risk of getting unwanted processes counted into proc.mem[] result.

Example:
```
$ zabbix_agentd -t proc.mem[,,,NonExistingProcess,rss]
proc.mem[,,,NonExistingProcess,rss]           [u|2879488]
```

This example shows how specifying only command line (regular expression to match) parameter results in Zabbix agent self-accounting - probably not what you want.</source>
      </trans-unit>
      <trans-unit id="14c4cc2b" xml:space="preserve">
        <source>2. Do not use "ps -ef" to browse processes - it shows only non-kernel processes. Use "ps -Af" to see all processes which will be seen by Zabbix agent.

3. Let's go through example of 'topasrec' how Zabbix agent proc.mem[] selects processes.

```
$ ps -Af | grep topasrec
root 10747984        1   0   Mar 16      -  0:00 /usr/bin/topasrec  -L -s 300 -R 1 -r 6 -o /var/perf daily/ -ypersistent=1 -O type=bin -ystart_time=04:08:54,Mar16,2023
```

proc.mem[] has arguments:

```
proc.mem[&lt;name&gt;,&lt;user&gt;,&lt;mode&gt;,&lt;cmdline&gt;,&lt;memtype&gt;]
```</source>
      </trans-unit>
      <trans-unit id="0be32659" xml:space="preserve">
        <source>
The 1st criterion is a process name (argument &lt;name&gt;). In our example Zabbix agent will see it as 'topasrec'. In order to match, you need to either specify 'topasrec' or to leave it empty.
The 2nd criterion is a user name (argument &lt;user&gt;). To match, you need to either specify 'root' or to leave it empty.
The 3rd criterion used in process selection is an argument &lt;cmdline&gt;. Zabbix agent will see its value as '/usr/bin/topasrec -L -s 300 -R 1 -r 6 -o /var/perf/daily/ -ypersistent=1 -O type=bin -ystart_time=04:08:54,Mar16,2023'. To match, you need to either specify a regular expression which matches this string or to leave it empty.

Arguments &lt;mode&gt; and &lt;memtype&gt; are applied after using the three criteria mentioned above. </source>
      </trans-unit>
      <trans-unit id="8a700330" xml:space="preserve">
        <source>#### FreeBSD

See values supported for 'memtype' parameter on FreeBSD in the table.

|Supported value|Description|Source in kinfo\_proc structure|Tries to be compatible with|
|--|------|--|--|
|vsize|Virtual memory size|kp\_eproc.e\_vm.vm\_map.size or ki\_size|ps -o vsz|
|pmem|Percentage of real memory|calculated from rss|ps -o pmem|
|rss|Resident set size|kp\_eproc.e\_vm.vm\_rssize or ki\_rssize|ps -o rss|
|size ^[1](proc_mem_notes#footnotes)^|Size of process (code + data + stack)|tsize + dsize + ssize| |
|tsize|Text (code) size|kp\_eproc.e\_vm.vm\_tsize or ki\_tsize|ps -o tsiz|
|dsize|Data size|kp\_eproc.e\_vm.vm\_dsize or ki\_dsize|ps -o dsiz|
|ssize|Stack size|kp\_eproc.e\_vm.vm\_ssize or ki\_ssize|ps -o ssiz|</source>
      </trans-unit>
      <trans-unit id="f43d1dfd" xml:space="preserve">
        <source>#### Linux

See values supported for 'memtype' parameter on Linux in the table.

|Supported value|Description|Source in /proc/&lt;pid&gt;/status file|
|--|------|--|
|vsize ^[1](proc_mem_notes#footnotes)^|Virtual memory size|VmSize|
|pmem|Percentage of real memory|(VmRSS/total\_memory) \* 100|
|rss|Resident set size|VmRSS|
|data|Size of data segment|VmData|
|exe|Size of code segment|VmExe|
|hwm|Peak resident set size|VmHWM|
|lck|Size of locked memory|VmLck|
|lib|Size of shared libraries|VmLib|
|peak|Peak virtual memory size|VmPeak|
|pin|Size of pinned pages|VmPin|
|pte|Size of page table entries|VmPTE|
|size|Size of process code + data + stack segments|VmExe + VmData + VmStk|
|stk|Size of stack segment|VmStk|
|swap|Size of swap space used|VmSwap|

Notes for Linux:

1.  Not all 'memtype' values are supported by older Linux kernels. For
    example, Linux 2.4 kernels do not support `hwm`, `pin`, `peak`,
    `pte` and `swap` values.
2.  We have noticed that self-monitoring of the Zabbix agent active
    check process with `proc.mem[...,...,...,...,data]` shows a value
    that is 4 kB larger than reported by `VmData` line in the agent's
    /proc/&lt;pid&gt;/status file. At the time of self-measurement the
    agent's data segment increases by 4 kB and then returns to the
    previous size.</source>
      </trans-unit>
      <trans-unit id="96fd9f28" xml:space="preserve">
        <source>#### Solaris

See values supported for 'memtype' parameter on Solaris in the table.

|Supported value|Description|Source in psinfo structure|Tries to be compatible with|
|--|------|--|--|
|vsize ^[1](proc_mem_notes#footnotes)^|Size of process image|pr\_size|ps -o vsz|
|pmem|Percentage of real memory|pr\_pctmem|ps -o pmem|
|rss|Resident set size&lt;br&gt;It may be underestimated - see rss description in "man ps".|pr\_rssize|ps -o rss|</source>
      </trans-unit>
      <trans-unit id="e0ce8129" xml:space="preserve">
        <source>##### Footnotes

^**1**^ Default value.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
