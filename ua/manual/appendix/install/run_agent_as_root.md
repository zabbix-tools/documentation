[comment]: # translation:outdated

[comment]: # ({new-6f1ad3f2})
# 9 Running agent as root

Starting with version **5.0.0** the systemd service file for Zabbix
agent in [official
packages](https://www.zabbix.com/download?zabbix=5.0&os_distribution=red_hat_enterprise_linux&os_version=8&db=mysql)
was updated to explicitly include directives for `User` and `Group`.
Both are set to `zabbix`.

This means that the old functionality of configuring which user Zabbix
agent runs as via `zabbix_agentd.conf` file is bypassed and agent will
always run as the user specified in the systemd service file.

To override this new behavior create a
`/etc/systemd/system/zabbix-agent.service.d/override.conf` file with the
following content:

    [Service]
    User=root
    Group=root

Reload daemons and restart the zabbix-agent service:

    systemctl daemon-reload
    systemctl restart zabbix-agent

For **Zabbix agent2** this completely determines the user that it runs
as.

For old **agent** this only re-enables the functionality of configuring
user in the `zabbix_agentd.conf` file. Therefore in order to run zabbix
agent as root you still have to edit the agent [configuration
file](/manual/appendix/config/zabbix_agentd) and specify `User=root` as
well as `AllowRoot=1` options.

[comment]: # ({/new-6f1ad3f2})

[comment]: # ({new-2f6d2fe8})

### Zabbix agent

To override the default user and group for Zabbix agent, run:

    systemctl edit zabbix-agent

Then, add the following content:

    [Service]
    User=root
    Group=root

Reload daemons and restart the zabbix-agent service:

    systemctl daemon-reload
    systemctl restart zabbix-agent

For **Zabbix agent** this re-enables the functionality of configuring user in the `zabbix_agentd.conf` file.
Now you need to set `User=root` and `AllowRoot=1` configuration parameters in the agent [configuration file](/manual/appendix/config/zabbix_agentd).

[comment]: # ({/new-2f6d2fe8})

[comment]: # ({new-f2ff86e2})

### Zabbix agent 2

To override the default user and group for Zabbix agent 2, run:

    systemctl edit zabbix-agent2

Then, add the following content:

    [Service]
    User=root
    Group=root

Reload daemons and restart the zabbix-agent service:

    systemctl daemon-reload
    systemctl restart zabbix-agent2

For **Zabbix agent2** this completely determines the user that it runs as.
No additional modifications are required.

[comment]: # ({/new-f2ff86e2})
