[comment]: # translation:outdated

[comment]: # ({new-7af1bbaa})
# 1 Database creation

[comment]: # ({/new-7af1bbaa})

[comment]: # ({new-fab90562})
#### Overview

A Zabbix database must be created during the installation of Zabbix
server or proxy.

This section provides instructions for creating a Zabbix database. A
separate set of instructions is available for each supported database.

UTF-8 is the only encoding supported by Zabbix. It is known to work
without any security flaws. Users should be aware that there are known
security issues if using some of the other encodings.

[comment]: # ({/new-fab90562})

[comment]: # ({new-fad527fc})

::: noteclassic
If installing from [Zabbix Git
repository](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse), you need to run:

`$ make dbschema` 

prior to proceeding to the next steps. 
:::

[comment]: # ({/new-fad527fc})

[comment]: # ({new-1e36e539})
#### MySQL

Character sets utf8 (aka utf8mb3) and utf8mb4 are supported (with
utf8\_bin and utf8mb4\_bin collation respectively) for Zabbix
server/proxy to work properly with MySQL database. It is recommended to
use utf8mb4 for new installations.

    shell> mysql -uroot -p<password>
    mysql> create database zabbix character set utf8mb4 collate utf8mb4_bin;
    mysql> create user 'zabbix'@'localhost' identified by '<password>';
    mysql> grant all privileges on zabbix.* to 'zabbix'@'localhost';
    mysql> quit;

::: notewarning
If you are installing from Zabbix **packages**, stop
here and continue with instructions for
[RHEL/CentOS](https://www.zabbix.com/download?zabbix=5.0&os_distribution=red_hat_enterprise_linux&os_version=8&db=mysql)
or
[Debian/Ubuntu](https://www.zabbix.com/download?zabbix=5.0&os_distribution=debian&os_version=10_buster&db=mysql)
to import the data into the database.
:::

If you are installing Zabbix from sources, proceed to import the data
into the database. For a Zabbix proxy database, only `schema.sql` should
be imported (no images.sql nor data.sql):

    shell> cd database/mysql
    shell> mysql -uzabbix -p<password> zabbix < schema.sql
    # stop here if you are creating database for Zabbix proxy
    shell> mysql -uzabbix -p<password> zabbix < images.sql
    shell> mysql -uzabbix -p<password> zabbix < data.sql

[comment]: # ({/new-1e36e539})

[comment]: # ({new-61d6043c})
#### PostgreSQL

You need to have database user with permissions to create database
objects. The following shell command will create user `zabbix`. Specify
password when prompted and repeat password (note, you may first be asked
for `sudo` password):

    shell> sudo -u postgres createuser --pwprompt zabbix

Now we will set up the database `zabbix` (last parameter) with the
previously created user as the owner (`-O zabbix`).

    shell> sudo -u postgres createdb -O zabbix -E Unicode -T template0 zabbix

::: notewarning
If you are installing from Zabbix **packages**, stop
here and continue with instructions for
[RHEL/CentOS](https://www.zabbix.com/download?zabbix=5.0&os_distribution=red_hat_enterprise_linux&os_version=8&db=postgresql)
or
[Debian/Ubuntu](https://www.zabbix.com/download?zabbix=5.0&os_distribution=debian&os_version=10_buster&db=postgresql)
to import the initial schema and data into the database.
:::

If you are installing Zabbix from sources, proceed to import the initial
schema and data (assuming you are in the root directory of Zabbix
sources). For a Zabbix proxy database, only `schema.sql` should be
imported (no images.sql nor data.sql).

    shell> cd database/postgresql
    shell> cat schema.sql | sudo -u zabbix psql zabbix
    # stop here if you are creating database for Zabbix proxy
    shell> cat images.sql | sudo -u zabbix psql zabbix
    shell> cat data.sql | sudo -u zabbix psql zabbix

::: noteimportant
The above commands are provided as an example that
will work in most of GNU/Linux installations. You can use different
commands, e. g. "psql -U <username>" depending on how your
system/database are configured. If you have troubles setting up the
database please consult your Database administrator. 
:::

[comment]: # ({/new-61d6043c})

[comment]: # ({new-cc68ca58})
#### TimescaleDB

Instructions for creating and configuring TimescaleDB are provided in a
separate [section](/manual/appendix/install/timescaledb).

[comment]: # ({/new-cc68ca58})

[comment]: # ({new-7b4d56a7})
#### Oracle

Instructions for creating and configuring Oracle database are provided
in a separate [section](/manual/appendix/install/oracle).

[comment]: # ({/new-7b4d56a7})

[comment]: # ({new-02d49e4f})
#### SQLite

Using SQLite is supported for **Zabbix proxy** only!

::: noteclassic
If using SQLite with Zabbix proxy, database will be
automatically created if it does not exist.
:::

    shell> cd database/sqlite3
    shell> sqlite3 /var/lib/sqlite/zabbix.db < schema.sql

Return to the [installation section](/manual/installation/install).

[comment]: # ({/new-02d49e4f})
