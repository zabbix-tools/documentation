[comment]: # translation:outdated

[comment]: # ({new-cd4a930c})
# service.update

[comment]: # ({/new-cd4a930c})

[comment]: # ({new-79afe9b7})
### Description

`object service.update(object/array services)`

This method allows to update existing services.

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-79afe9b7})

[comment]: # ({new-5fb5ebb3})
### Parameters

`(object/array)` service properties to be updated.

The `serviceid` property must be defined for each service, all other
properties are optional. Only the passed properties will be updated, all
others will remain unchanged.

Additionally to the [standard service properties](object#service), the
method accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|children|array|Child services to replace the current service children.<br><br>The children must have the `serviceid` property defined.|
|parents|array|Parent services to replace the current service parents.<br><br>The parents must have the `serviceid` property defined.|
|tags|array|Service [tags](/manual/api/reference/service/object#service_tag) to replace the current service tags.|
|times|array|Service [times](/manual/api/reference/service/object#service_time) to replace the current service times.|
|problem\_tags|array|[Problem tags](/manual/api/reference/service/object#problem_tag) to replace the current problem tags.|
|status\_rules|array|[Status rules](/manual/api/reference/service/object#status_rule) to replace the current status rules.|

[comment]: # ({/new-5fb5ebb3})

[comment]: # ({new-9465ee4d})
### Return values

`(object)` Returns an object containing the IDs of the updated services
under the `serviceids` property.

[comment]: # ({/new-9465ee4d})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-b9918036})
#### Setting the parent for a service

Make service with ID "3" to be the parent for service with ID "5".

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "service.update",
    "params": {
        "serviceid": "5",
        "parents": [
            {
                "serviceid": "3"
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "serviceids": [
            "5"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-b9918036})

[comment]: # ({new-263b1baa})
#### Adding a scheduled downtime

Add a downtime for service with ID "4" scheduled weekly from Monday
22:00 till Tuesday 10:00.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "service.update",
    "params": {
        "serviceid": "4",
        "times": [
            {
                "type": "1",
                "ts_from": "165600",
                "ts_to": "201600"
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "serviceids": [
            "4"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-263b1baa})

[comment]: # ({new-fd1b6894})
### Source

CService::update() in *ui/include/classes/api/services/CService.php*.

[comment]: # ({/new-fd1b6894})
