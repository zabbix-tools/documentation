[comment]: # translation:outdated

[comment]: # ({new-19701d17})
# user.login

[comment]: # ({/new-19701d17})

[comment]: # ({new-c647dda4})
### Description

`string/object user.login(object parameters)`

This method allows to log in to the API and generate an authentication
token.

::: notewarning
When using this method, you also need to do
[user.logout](/manual/api/reference/user/logout) to prevent the
generation of a large number of open session records.
:::

::: noteimportant
This method is only available to unauthenticated
users and must be called without the `auth` parameter in the JSON-RPC
request.
:::

[comment]: # ({/new-c647dda4})

[comment]: # ({new-68f0a066})
### Parameters

`(object)` Parameters containing the user name and password.

The method accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|**password**<br>(required)|string|User password.|
|**username**<br>(required)|string|User name.|
|userData|flag|Return information about the authenticated user.|

[comment]: # ({/new-68f0a066})

[comment]: # ({new-bc924c62})
### Return values

`(string/object)` If the `userData` parameter is used, returns an object
containing information about the authenticated user.

Additionally to the [standard user properties](object#user), the
following information is returned:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|debug\_mode|boolean|Whether debug mode is enabled for the user.|
|gui\_access|integer|User's authentication method to the frontend.<br><br>Refer to the `gui_access` property of the [user group object](/manual/api/reference/usergroup/object#user_group) for a list of possible values.|
|sessionid|string|Authentication token, which must be used in the following API requests.|
|userip|string|IP address of the user.|

::: notetip
If a user has been successfully authenticated after one
or more failed attempts, the method will return the current values for
the `attempt_clock`, `attempt_failed` and `attempt_ip` properties and
then reset them.
:::

If the `userData` parameter is not used, the method returns an
authentication token.

::: notetip
The generated authentication token should be remembered
and used in the `auth` parameter of the following JSON-RPC requests. It
is also required when using HTTP authentication.
:::

[comment]: # ({/new-bc924c62})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-46abbe5a})
#### Authenticating a user

Authenticate a user.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "user.login",
    "params": {
        "username": "Admin",
        "password": "zabbix"
    },
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": "0424bd59b807674191e7d77572075f33",
    "id": 1
}
```

[comment]: # ({/new-46abbe5a})

[comment]: # ({new-8448c8ab})
#### Requesting authenticated user's information

Authenticate and return additional information about the user.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "user.login",
    "params": {
        "username": "Admin",
        "password": "zabbix",
        "userData": true
    },
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "userid": "1",
        "username": "Admin",
        "name": "Zabbix",
        "surname": "Administrator",
        "url": "",
        "autologin": "1",
        "autologout": "0",
        "lang": "ru_RU",
        "refresh": "0",
        "theme": "default",
        "attempt_failed": "0",
        "attempt_ip": "127.0.0.1",
        "attempt_clock": "1355919038",
        "rows_per_page": "50",
        "timezone": "Europe/Riga",
        "roleid": "3",
        "type": 3,
        "debug_mode": 0,
        "userip": "127.0.0.1",
        "gui_access": "0",
        "sessionid": "5b56eee8be445e98f0bd42b435736e42"
    },
    "id": 1
}
```

[comment]: # ({/new-8448c8ab})

[comment]: # ({new-22a14fca})
### See also

-   [user.logout](logout)

[comment]: # ({/new-22a14fca})

[comment]: # ({new-7fd9f53c})
### Source

CUser::login() in *ui/include/classes/api/services/CUser.php*.

[comment]: # ({/new-7fd9f53c})
