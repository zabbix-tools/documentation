[comment]: # translation:outdated

[comment]: # ({new-5d6f5a54})
# Template dashboard

This class is designed to work with template dashboards.

Object references:\

-   [Template dashboard](/manual/api/reference/templatedashboard/object)
-   [Template dashboard
    page](/manual/api/reference/templatedashboard/object#template_dashboard_page)
-   [Template dashboard
    widget](/manual/api/reference/templatedashboard/object#template_dashboard_widget)
-   [Template dashboard widget
    field](/manual/api/reference/templatedashboard/object#template_dashboard_widget_field)

Available methods:\

-   [templatedashboard.create](/manual/api/reference/templatedashboard/create) -
    creating new template dashboards
-   [templatedashboard.delete](/manual/api/reference/templatedashboard/delete) -
    deleting template dashboards
-   [templatedashboard.get](/manual/api/reference/templatedashboard/get) -
    retrieving template dashboards
-   [templatedashboard.update](/manual/api/reference/templatedashboard/update) -
    updating template dashboards

[comment]: # ({/new-5d6f5a54})
