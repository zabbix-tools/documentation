[comment]: # translation:outdated

[comment]: # ({new-09ff1eef})
# Value map

This class is designed to work with value maps.

Object references:\

-   [Value map](/manual/api/reference/valuemap/object#value_map)

Available methods:\

-   [valuemap.create](/manual/api/reference/valuemap/create) - creating
    new value maps
-   [valuemap.delete](/manual/api/reference/valuemap/delete) - deleting
    value maps
-   [valuemap.get](/manual/api/reference/valuemap/get) - retrieving
    value maps
-   [valuemap.update](/manual/api/reference/valuemap/update) - updating
    value maps

[comment]: # ({/new-09ff1eef})
