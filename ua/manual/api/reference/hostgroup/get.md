[comment]: # translation:outdated

[comment]: # ({new-f8d4c2e5})
# hostgroup.get

[comment]: # ({/new-f8d4c2e5})

[comment]: # ({new-08ff22f2})
### Description

`integer/array hostgroup.get(object parameters)`

The method allows to retrieve host groups according to the given
parameters.

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-08ff22f2})

[comment]: # ({new-7f042696})
### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|graphids|string/array|Return only host groups that contain hosts or templates with the given graphs.|
|groupids|string/array|Return only host groups with the given host group IDs.|
|hostids|string/array|Return only host groups that contain the given hosts.|
|maintenanceids|string/array|Return only host groups that are affected by the given maintenances.|
|monitored\_hosts|flag|Return only host groups that contain monitored hosts.|
|real\_hosts|flag|Return only host groups that contain hosts.|
|templated\_hosts|flag|Return only host groups that contain templates.|
|templateids|string/array|Return only host groups that contain the given templates.|
|triggerids|string/array|Return only host groups that contain hosts or templates with the given triggers.|
|with\_graphs|flag|Return only host groups that contain hosts with graphs.|
|with\_graph\_prototypes|flag|Return only host groups that contain hosts with graph prototypes.|
|with\_hosts\_and\_templates|flag|Return only host groups that contain hosts *or* templates.|
|with\_httptests|flag|Return only host groups that contain hosts with web checks.<br><br>Overrides the `with_monitored_httptests` parameter.|
|with\_items|flag|Return only host groups that contain hosts or templates with items.<br><br>Overrides the `with_monitored_items` and`with_simple_graph_items` parameters.|
|with\_item\_prototypes|flag|Return only host groups that contain hosts with item prototypes.<br><br>Overrides the `with_simple_graph_item_prototypes` parameter.|
|with\_simple\_graph\_item\_prototypes|flag|Return only host groups that contain hosts with item prototypes, which are enabled for creation and have numeric type of information.|
|with\_monitored\_httptests|flag|Return only host groups that contain hosts with enabled web checks.|
|with\_monitored\_items|flag|Return only host groups that contain hosts or templates with enabled items.<br><br>Overrides the `with_simple_graph_items` parameter.|
|with\_monitored\_triggers|flag|Return only host groups that contain hosts with enabled triggers. All of the items used in the trigger must also be enabled.|
|with\_simple\_graph\_items|flag|Return only host groups that contain hosts with numeric items.|
|with\_triggers|flag|Return only host groups that contain hosts with triggers.<br><br>Overrides the `with_monitored_triggers` parameter.|
|selectDiscoveryRule|query|Return a [discoveryRule](/manual/api/reference/drule/object) property with the LLD rule that created the host group.|
|selectGroupDiscovery|query|Return a `groupDiscovery` property with the host group discovery object.<br><br>The host group discovery object links a discovered host group to a host group prototype and has the following properties:<br>`groupid` - `(string)` ID of the discovered host group;<br>`lastcheck` - `(timestamp)` time when the host group was last discovered;<br>`name` - `(string)` name of the host goup prototype;<br>`parent_group_prototypeid` - `(string)` ID of the host group prototype from which the host group has been created;<br>`ts_delete` - `(timestamp)` time when a host group that is no longer discovered will be deleted.|
|selectHosts|query|Return a [hosts](/manual/api/reference/host/object) property with the hosts that belong to the host group.<br><br>Supports `count`.|
|selectTemplates|query|Return a [templates](/manual/api/reference/template/object) property with the templates that belong to the host group.<br><br>Supports `count`.|
|limitSelects|integer|Limits the number of records returned by subselects.<br><br>Applies to the following subselects:<br>`selectHosts` - results will be sorted by `host`;<br>`selectTemplates` - results will be sorted by `host`.|
|sortfield|string/array|Sort the result by the given properties.<br><br>Possible values are: `groupid`, `name`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters) page.|
|editable|boolean|^|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

[comment]: # ({/new-7f042696})

[comment]: # ({new-7223bab1})
### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-8ed2756a})
#### Retrieving data by name

Retrieve all data about two host groups named "Zabbix servers" and
"Linux servers".

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "hostgroup.get",
    "params": {
        "output": "extend",
        "filter": {
            "name": [
                "Zabbix servers",
                "Linux servers"
            ]
        }
    },
    "auth": "6f38cddc44cfbb6c1bd186f9a220b5a0",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "groupid": "2",
            "name": "Linux servers",
            "internal": "0"
        },
        {
            "groupid": "4",
            "name": "Zabbix servers",
            "internal": "0"
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-8ed2756a})

[comment]: # ({new-e5f0abe1})
### See also

-   [Host](/manual/api/reference/host/object#host)
-   [Template](/manual/api/reference/template/object#template)

[comment]: # ({/new-e5f0abe1})

[comment]: # ({new-1e72ea39})
### Source

CHostGroup::get() in *ui/include/classes/api/services/CHostGroup.php*.

[comment]: # ({/new-1e72ea39})
