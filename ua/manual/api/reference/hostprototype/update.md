[comment]: # translation:outdated

[comment]: # ({new-c0df6427})
# hostprototype.update

[comment]: # ({/new-c0df6427})

[comment]: # ({new-796670ab})
### Description

`object hostprototype.update(object/array hostPrototypes)`

This method allows to update existing host prototypes.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-796670ab})

[comment]: # ({new-50727275})
### Parameters

`(object/array)` Host prototype properties to be updated.

The `hostid` property must be defined for each host prototype, all other
properties are optional. Only the passed properties will be updated, all
others will remain unchanged.

Additionally to the [standard host prototype
properties](object#host_prototype), the method accepts the following
parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|groupLinks|array|Group [links](/manual/api/reference/hostprototype/object#group_link) to replace the current group links on the host prototype.|
|groupPrototypes|array|Group [prototypes](/manual/api/reference/hostprototype/object#group_prototype) to replace the existing group prototypes on the host prototype.|
|macros|object/array|[User macros](/manual/api/reference/usermacro/object) to replace the current user macros.<br><br>All macros that are not listed in the request will be removed.|
|tags|object/array|Host prototype [tags](/manual/api/reference/hostprototype/object#host_prototype_tag) to replace the current tags.<br><br>All tags that are not listed in the request will be removed.|
|interfaces|object/array|Host prototype [custom interfaces](/manual/api/reference/hostprototype/object#custom_interface) to replace the current interfaces.<br><br>Custom interface object should contain all its parameters.<br>All interfaces that are not listed in the request will be removed.|
|templates|object/array|[Templates](/manual/api/reference/template/object) to replace the currently linked templates.<br><br>The templates must have the `templateid` property defined.|

[comment]: # ({/new-50727275})

[comment]: # ({new-c97defa2})
### Return values

`(object)` Returns an object containing the IDs of the updated host
prototypes under the `hostids` property.

[comment]: # ({/new-c97defa2})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-9e34a671})
#### Disabling a host prototype

Disable a host prototype, that is, set its status to 1.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "hostprototype.update",
    "params": {
        "hostid": "10092",
        "status": 1
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "hostids": [
            "10092"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-9e34a671})

[comment]: # ({new-440003e2})
#### Updating host prototype tags

Replace host prototype tags with new ones.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "hostprototype.update",
    "params": {
        "hostid": "10092",
        "tags": [
            {
                "tag": "Datacenter",
                "value": "{#DATACENTER.NAME}"
            },
            {
                "tag": "Instance type",
                "value": "{#INSTANCE_TYPE}"
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "hostids": [
            "10092"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-440003e2})

[comment]: # ({new-7a7b18a4})
#### Updating host prototype custom interfaces

Replace inherited interfaces with host prototype custom interfaces.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "hostprototype.update",
    "params": {
        "hostid": "10092",
        "custom_interfaces": "1",
        "interfaces": [
            {
                "main": "1",
                "type": "2",
                "useip": "1",
                "ip": "127.0.0.1",
                "dns": "",
                "port": "161",
                "details": {
                    "version": "2",
                    "bulk": "1",
                    "community": "{$SNMP_COMMUNITY}"
                }
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "hostids": [
            "10092"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-7a7b18a4})

[comment]: # ({new-c4a38bf3})
### See also

-   [Group link](object#group_link)
-   [Group prototype](object#group_prototype)
-   [Host prototype tag](object#host_prototype_tag)
-   [Custom interface](object#custom_interface)
-   [User
    macro](/manual/api/reference/usermacro/object#hosttemplate_level_macro)

[comment]: # ({/new-c4a38bf3})

[comment]: # ({new-a380e3fb})
### Source

CHostPrototype::update() in
*ui/include/classes/api/services/CHostPrototype.php*.

[comment]: # ({/new-a380e3fb})
