[comment]: # translation:outdated


[comment]: # ({new-03e77d6d})
# Dashboard

This class is designed to work with dashboards.

Object references:\

-   [Dashboard](/manual/api/reference/dashboard/object#dashboard)
-   [Dashboard
    page](/manual/api/reference/dashboard/object#dashboard_page)
-   [Dashboard
    widget](/manual/api/reference/dashboard/object#dashboard_widget)
-   [Dashboard widget
    field](/manual/api/reference/dashboard/object#dashboard_widget_field)
-   [Dashboard
    user](/manual/api/reference/dashboard/object#dashboard_user)
-   [Dashboard user
    group](/manual/api/reference/dashboard/object#dashboard_user_group)

Available methods:\

-   [dashboard.create](/manual/api/reference/dashboard/create) -
    creating new dashboards
-   [dashboard.delete](/manual/api/reference/dashboard/delete) -
    deleting dashboards
-   [dashboard.get](/manual/api/reference/dashboard/get) - retrieving
    dashboards
-   [dashboard.update](/manual/api/reference/dashboard/update) -
    updating dashboards

[comment]: # ({/new-03e77d6d})
