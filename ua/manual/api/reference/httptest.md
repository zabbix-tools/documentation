[comment]: # translation:outdated

[comment]: # ({new-7aa664d0})
# Web scenario

This class is designed to work with web scenarios.

Object references:\

-   [Web scenario](/manual/api/reference/httptest/object#web_scenario)
-   [Scenario step](/manual/api/reference/httptest/object#scenario_step)

Available methods:\

-   [httptest.create](/manual/api/reference/httptest/create) - creating
    new web scenarios
-   [httptest.delete](/manual/api/reference/httptest/delete) - deleting
    web scenarios
-   [httptest.get](/manual/api/reference/httptest/get) - retrieving web
    scenarios
-   [httptest.update](/manual/api/reference/httptest/update) - updating
    web scenarios

[comment]: # ({/new-7aa664d0})
