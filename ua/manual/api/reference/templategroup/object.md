[comment]: # translation:outdated

[comment]: # ({new-f9e8f670})
# > Template group object

The following objects are directly related to the `templategroup` API.

[comment]: # ({/new-f9e8f670})

[comment]: # ({new-ad144502})
### Template group

The template group object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|groupid|string|*(readonly)* ID of the template group.|
|**name**<br>(required)|string|Name of the template group.|
|uuid|string|Universal unique identifier, used for linking imported template groups to already existing ones. Auto-generated, if not given.<br><br>For update operations this field is *readonly*.|

[comment]: # ({/new-ad144502})
