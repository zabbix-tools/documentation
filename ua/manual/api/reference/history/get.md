[comment]: # translation:outdated

[comment]: # ({new-ad84ea6b})
# history.get

[comment]: # ({/new-ad84ea6b})

[comment]: # ({new-385acf3d})
### Description

`integer/array history.get(object parameters)`

The method allows to retrieve history data according to the given
parameters.

See also: [known issues](/manual/installation/known_issues#api)

::: noteimportant
This method may return historical data of a
deleted entity if this data has not been removed by the housekeeper
yet.
:::

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-385acf3d})

[comment]: # ({new-ddac6e9c})
### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|history|integer|History object types to return.<br><br>Possible values:<br>0 - numeric float;<br>1 - character;<br>2 - log;<br>3 - numeric unsigned;<br>4 - text.<br><br>Default: 3.|
|hostids|string/array|Return only history from the given hosts.|
|itemids|string/array|Return only history from the given items.|
|time\_from|timestamp|Return only values that have been received after or at the given time.|
|time\_till|timestamp|Return only values that have been received before or at the given time.|
|sortfield|string/array|Sort the result by the given properties.<br><br>Possible values are: `itemid` and `clock`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters) page.|
|editable|boolean|^|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|integer|^|
|output|query|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

[comment]: # ({/new-ddac6e9c})

[comment]: # ({new-7223bab1})
### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-cf7bb886})
#### Retrieving item history data

Return 10 latest values received from a numeric(float) item.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "history.get",
    "params": {
        "output": "extend",
        "history": 0,
        "itemids": "23296",
        "sortfield": "clock",
        "sortorder": "DESC",
        "limit": 10
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "itemid": "23296",
            "clock": "1351090996",
            "value": "0.085",
            "ns": "563157632"
        },
        {
            "itemid": "23296",
            "clock": "1351090936",
            "value": "0.16",
            "ns": "549216402"
        },
        {
            "itemid": "23296",
            "clock": "1351090876",
            "value": "0.18",
            "ns": "537418114"
        },
        {
            "itemid": "23296",
            "clock": "1351090816",
            "value": "0.21",
            "ns": "522659528"
        },
        {
            "itemid": "23296",
            "clock": "1351090756",
            "value": "0.215",
            "ns": "507809457"
        },
        {
            "itemid": "23296",
            "clock": "1351090696",
            "value": "0.255",
            "ns": "495509699"
        },
        {
            "itemid": "23296",
            "clock": "1351090636",
            "value": "0.36",
            "ns": "477708209"
        },
        {
            "itemid": "23296",
            "clock": "1351090576",
            "value": "0.375",
            "ns": "463251343"
        },
        {
            "itemid": "23296",
            "clock": "1351090516",
            "value": "0.315",
            "ns": "447947017"
        },
        {
            "itemid": "23296",
            "clock": "1351090456",
            "value": "0.275",
            "ns": "435307141"
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-cf7bb886})

[comment]: # ({new-621f421c})
### Source

CHistory::get() in *ui/include/classes/api/services/CHistory.php*.

[comment]: # ({/new-621f421c})
