[comment]: # translation:outdated

[comment]: # ({new-830462e8})
# Service

This class is designed to work with IT infrastructure/business services.

Object references:\

-   [Service](/manual/api/reference/service/object#service)
-   [Status rule](/manual/api/reference/service/object#status_rule)
-   [Service tag](/manual/api/reference/service/object#service_tag)
-   [Service time](/manual/api/reference/service/object#service_time)
-   [Service alarm](/manual/api/reference/service/object#service_alarm)
-   [Problem tag](/manual/api/reference/service/object#problem_tag)

Available methods:\

-   [service.create](/manual/api/reference/service/create) - creating
    new services
-   [service.delete](/manual/api/reference/service/delete) - deleting
    services
-   [service.get](/manual/api/reference/service/get) - retrieving
    services
-   [service.getsla](/manual/api/reference/service/getsla) - retrieving
    availability information about services
-   [service.update](/manual/api/reference/service/update) - updating
    services

[comment]: # ({/new-830462e8})
