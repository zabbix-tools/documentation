<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="ua" datatype="plaintext" original="manual/api/reference/trigger/create.md">
    <body>
      <trans-unit id="56486046" xml:space="preserve">
        <source># trigger.create</source>
      </trans-unit>
      <trans-unit id="8efa90c6" xml:space="preserve">
        <source>### Description

`object trigger.create(object/array triggers)`

This method allows to create new triggers.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::</source>
      </trans-unit>
      <trans-unit id="c2c3e90f" xml:space="preserve">
        <source>### Parameters

`(object/array)` Triggers to create.

Additionally to the [standard trigger properties](object#trigger) the
method accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|dependencies|array|Triggers that the trigger is dependent on.&lt;br&gt;&lt;br&gt;The triggers must have the `triggerid` property defined.|
|tags|array|Trigger [tags.](/manual/api/reference/trigger/object#trigger_tag)|

::: noteimportant
The trigger expression has to be given in its
expanded form.
:::</source>
      </trans-unit>
      <trans-unit id="4938d22d" xml:space="preserve">
        <source>### Return values

`(object)` Returns an object containing the IDs of the created triggers
under the `triggerids` property. The order of the returned IDs matches
the order of the passed triggers.</source>
      </trans-unit>
      <trans-unit id="b41637d2" xml:space="preserve">
        <source>### Examples</source>
      </trans-unit>
      <trans-unit id="f57689d3" xml:space="preserve">
        <source>#### Creating a trigger

Create a trigger with a single trigger dependency.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "trigger.create",
    "params": [
        {
            "description": "Processor load is too high on {HOST.NAME}",
            "expression": "last(/Linux server/system.cpu.load[percpu,avg1])&gt;5",
            "dependencies": [
                {
                    "triggerid": "17367"
                }
            ]
        },
        {
            "description": "Service status",
            "expression": "length(last(/Linux server/log[/var/log/system,Service .* has stopped]))&lt;&gt;0",
            "dependencies": [
                {
                    "triggerid": "17368"
                }
            ],
            "tags": [
                {
                    "tag": "service",
                    "value": "{{ITEM.VALUE}.regsub(\"Service (.*) has stopped\", \"\\1\")}"
                },
                {
                    "tag": "error",
                    "value": ""
                }
            ]
        }
    ],
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "triggerids": [
            "17369",
            "17370"
        ]
    },
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="7d49da78" xml:space="preserve">
        <source>### Source

CTrigger::create() in *ui/include/classes/api/services/CTrigger.php*.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
