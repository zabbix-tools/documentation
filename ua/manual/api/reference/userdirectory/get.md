[comment]: # translation:outdated

[comment]: # ({new-f9e8f670})
# userdirectory.get

[comment]: # ({/new-f9e8f670})

[comment]: # ({new-b5ff62ed})
### Description

`integer/array userdirectory.get(object parameters)`

The method allows to retrieve user directories according to the given parameters.

::: noteclassic
This method is only available to *Super admin* user types.
:::

[comment]: # ({/new-b5ff62ed})

[comment]: # ({new-d99c84a0})
### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|userdirectoryids       |string/array   |Return only user directory with the given IDs. |
|selectUsrgrps          |query          |Return a `usrgrps` property with [user groups](/manual/api/reference/usergroup/object) associated with user directory.<br><br>Supports `count`. |
|sortfield              |string/array   |Sort the result by the given properties.<br><br>Possible values are: `name`, `host`. |
|filter                 |object         |Return only those results that exactly match the given filter.<br><br>Possible values are: `userdirectoryid`, `host`, `name`.|
|search                 |object         |Return results that match the given wildcard search (case-insensitive).<br><br>Possible values are: `base_dn`, `bind_dn`, `description`, `host`, `name`, `search_attribute`, `search_filter`.|
|countOutput            |boolean        |These parameters being common for all `get` methods are described in detail in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters).|
|excludeSearch          |boolean        |^|
|limit                  |integer        |^|
|output                 |query          |^|
|preservekeys           |boolean        |^|
|searchByAny            |boolean        |^|
|searchWildcardsEnabled |boolean        |^|
|sortorder              |string/array   |^|
|startSearch            |boolean        |^|

[comment]: # ({/new-d99c84a0})

[comment]: # ({new-a18ff82b})
### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.

[comment]: # ({/new-a18ff82b})

[comment]: # ({new-ba1c8d4c})
### Examples

[comment]: # ({/new-ba1c8d4c})

[comment]: # ({new-6acef378})
#### Retrieving user directories

Retrieve all user directories with additional property with count of user groups where user directory is used.

Request:

```json
{
    "jsonrpc": "2.0",
    "method": "userdirectory.get",
    "params": {
        "output": "extend",
        "selectUsrgrps": "count"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": [
        {
            "userdirectoryid": "2",
            "name": "API user directory #1",
            "description": "",
            "host": "127.0.0.1",
            "port": "389",
            "base_dn": "ou=Users,dc=example,dc=org",
            "bind_dn": "cn=ldap_search,dc=example,dc=org",
            "search_attribute": "uid",
            "start_tls": "0",
            "search_filter": "",
            "usrgrps": "5"
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-6acef378})

[comment]: # ({new-5f3bc200})
### See also

-   [User group](/manual/api/reference/usergroup/object#user)

[comment]: # ({/new-5f3bc200})

[comment]: # ({new-392498dd})
### Source

CUserDirectory::get() in *ui/include/classes/api/services/CUserDirectory.php*.

[comment]: # ({/new-392498dd})
