[comment]: # translation:outdated

[comment]: # ({new-f9e8f670})
# userdirectory.delete

[comment]: # ({/new-f9e8f670})

[comment]: # ({new-b5ff62ed})
### Description

`object userdirectory.delete(array userDirectoryIds)`

This method allows to delete user directories. User directory cannot be deleted when it is directly used for at least one user group.\
Default LDAP user directory cannot be deleted when `authentication.ldap_configured` is set to 1 or when there are more user directories left.

::: noteclassic
This method is only available to *Super admin* user type.
:::

[comment]: # ({/new-b5ff62ed})

[comment]: # ({new-90e6a45f})
### Parameters

`(array)` IDs of the user directories to delete.

[comment]: # ({/new-90e6a45f})

[comment]: # ({new-e48d17a6})
### Return values

`(object)` Returns an object containing the IDs of the deleted user directories under the `userdirectoryids` property.

[comment]: # ({/new-e48d17a6})

[comment]: # ({new-94dc6263})
### Examples

[comment]: # ({/new-94dc6263})

[comment]: # ({new-ec4f71a7})
#### Deleting multiple user directories

Delete two user directories.

Request:

```json
{
    "jsonrpc": "2.0",
    "method": "userdirectory.delete",
    "params": [
        "2",
        "12"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "userdirectoryids": [
            "2",
            "12"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-ec4f71a7})

[comment]: # ({new-1eee2f52})
### Source

CUserDirectory::delete() in *ui/include/classes/api/services/CUserDirectory.php*.

[comment]: # ({/new-1eee2f52})
