<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="ua" datatype="plaintext" original="manual/api/reference/proxy/create.md">
    <body>
      <trans-unit id="07099686" xml:space="preserve">
        <source># proxy.create</source>
      </trans-unit>
      <trans-unit id="e0abed38" xml:space="preserve">
        <source>### Description

`object proxy.create(object/array proxies)`

This method allows to create new proxies.

::: noteclassic
This method is only available to *Super admin* user type. Permissions to call the method can be revoked in user role
settings. See [User roles](/manual/web_interface/frontend_sections/users/user_roles) for more information.
:::</source>
      </trans-unit>
      <trans-unit id="fc26a415" xml:space="preserve">
        <source>### Parameters

`(object/array)` Proxies to create.

Additionally to the [standard proxy properties](object#proxy), the method accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|hosts|array|[Hosts](/manual/api/reference/host/object) to be monitored by the proxy. If a host is already monitored by a different proxy, it will be reassigned to the current proxy.&lt;br&gt;&lt;br&gt;The hosts must have the `hostid` property defined.|
|interface|object|Host [interface](/manual/api/reference/hostinterface/object) to be created for the passive proxy.&lt;br&gt;&lt;br&gt;[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):&lt;br&gt;- *required* if `status` of [Proxy object](object#proxy) is set to "passive proxy"|</source>
      </trans-unit>
      <trans-unit id="606a1465" xml:space="preserve">
        <source>### Return values

`(object)` Returns an object containing the IDs of the created proxies under the `proxyids` property. The order of the
returned IDs matches the order of the passed proxies.</source>
      </trans-unit>
      <trans-unit id="b41637d2" xml:space="preserve">
        <source>### Examples</source>
      </trans-unit>
      <trans-unit id="86fb9238" xml:space="preserve">
        <source>#### Create an active proxy

Create an action proxy "Active proxy" and assign a host to be monitored by it.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "proxy.create",
    "params": {
        "host": "Active proxy",
        "status": "5",
        "hosts": [
            {
                "hostid": "10279"
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "proxyids": [
            "10280"
        ]
    },
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="8e1098f9" xml:space="preserve">
        <source>#### Create a passive proxy

Create a passive proxy "Passive proxy" and assign two hosts to be monitored by it.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "proxy.create",
    "params": {
        "host": "Passive proxy",
        "status": "6",
        "interface": {
            "ip": "127.0.0.1",
            "dns": "",
            "useip": "1",
            "port": "10051"
        },
        "hosts": [
            {
                "hostid": "10192"
            },
            {
                "hostid": "10139"
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "proxyids": [
            "10284"
        ]
    },
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="273e0fd8" xml:space="preserve">
        <source>### See also

-   [Host](/manual/api/reference/host/object#host)
-   [Proxy interface](object#proxy_interface)</source>
      </trans-unit>
      <trans-unit id="453e4545" xml:space="preserve">
        <source>### Source

CProxy::create() in *ui/include/classes/api/services/CProxy.php*.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
