<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="ua" datatype="plaintext" original="manual/api/reference/auditlog/get.md">
    <body>
      <trans-unit id="668456d1" xml:space="preserve">
        <source># auditlog.get</source>
      </trans-unit>
      <trans-unit id="002826bb" xml:space="preserve">
        <source>### Description

`integer/array auditlog.get(object parameters)`

The method allows to retrieve audit log records according to the given
parameters.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::</source>
      </trans-unit>
      <trans-unit id="c9891e0d" xml:space="preserve">
        <source>### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|auditids|string/array|Return only audit log with the given IDs.|
|userids|string/array|Return only audit log that were created by the given users.|
|time\_from|timestamp|Returns only audit log entries that have been created after or at the given time.|
|time\_till|timestamp|Returns only audit log entries that have been created before or at the given time.|
|sortfield|string/array|Sort the result by the given properties.&lt;br&gt;&lt;br&gt;Possible values: `auditid`, `userid`, `clock`.|
|filter|object|Return only results that exactly match the given filter.&lt;br&gt;&lt;br&gt;Accepts an array, where the keys are property names, and the values are either a single value or an array of values to match against.|
|search|object|Case insensitive sub-string search in content of fields: `username`, `ip`, `resourcename`, `details`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters).|
|excludeSearch|boolean|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|</source>
      </trans-unit>
      <trans-unit id="7223bab1" xml:space="preserve">
        <source>### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.</source>
      </trans-unit>
      <trans-unit id="b41637d2" xml:space="preserve">
        <source>### Examples</source>
      </trans-unit>
      <trans-unit id="66670b26" xml:space="preserve">
        <source>#### Retrieve audit log

Retrieve two latest audit log records.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "auditlog.get",
    "params": {
        "output": "extend",
        "sortfield": "clock",
        "sortorder": "DESC",
        "limit": 2
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": [
        {
            "auditid": "cksstgfam0001yhdcc41y20q2",
            "userid": "1",
            "username": "Admin",
            "clock": "1629975715",
            "ip": "127.0.0.1",
            "action": "1",
            "resourcetype": "0",
            "resourceid": "0",
            "resourcename": "Jim",
            "recordsetid": "cksstgfal0000yhdcso67ondl",
            "details": "{\"user.name\":[\"update\",\"Jim\",\"\"],\"user.medias[37]\":[\"add\"],\"user.medias[37].\":[\"add\"],\"user.medias[37].mediatypeid\":[\"add\",\"1\"],\"user.medias[37].sendto\":[\"add\",\"support123@company.com\"]}"
        },
        {
            "auditid": "ckssofl0p0001yhdcqxclsg8r",
            "userid": "1",
            "username": "Admin",
            "clock": "1629967278",
            "ip": "127.0.0.1",
            "action": "0",
            "resourcetype": "0",
            "resourceid": "20",
            "resourcename": "John",
            "recordsetid": "ckssofl0p0000yhdcpxyo1jgo",
            "details": "{\"user.username\":[\"add\",\"John\"], \"user.userid:\":[\"add\",\"20\"],\"user.usrgrps[28]\":[\"add\"],\"user.usrgrps[28].usrgrpid\":[\"add\", \"7\"]}"
        }
    ],
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="e2b9a859" xml:space="preserve">
        <source>### See also

-   [Audit log object](/manual/api/reference/auditlog/object)</source>
      </trans-unit>
      <trans-unit id="11b31975" xml:space="preserve">
        <source>### Source

CAuditLog::get() in *ui/include/classes/api/services/CAuditLog.php*.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
