<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="ua" datatype="plaintext" original="manual/api/reference/discoveryrule/object.md">
    <body>
      <trans-unit id="c5a9e121" xml:space="preserve">
        <source># &gt; LLD rule object

The following objects are directly related to the `discoveryrule` API.</source>
      </trans-unit>
      <trans-unit id="c17fa830" xml:space="preserve">
        <source>### LLD rule

The low-level discovery rule object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|itemid|string|ID of the LLD rule.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *read-only*&lt;br&gt;- *required* for update operations|
|delay|string|Update interval of the LLD rule. Accepts seconds or time unit with suffix and with or without one or more [custom intervals](/manual/config/items/item/custom_intervals) that consist of either flexible intervals and scheduling intervals as serialized strings. Also accepts user macros. Flexible intervals could be written as two macros separated by a forward slash. Intervals are separated by a semicolon.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* if `type` is set to "Zabbix agent" (0), "Simple check" (3), "Zabbix internal" (5), "External check" (10), "Database monitor" (11), "IPMI agent" (12), "SSH agent" (13), "TELNET agent" (14), "JMX agent" (16), "HTTP agent" (19), "SNMP agent" (20), "Script" (21), or if `type` is set to "Zabbix agent (active)" (7) and `key_` does not contain "mqtt.get"|
|hostid|string|ID of the host that the LLD rule belongs to.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *constant*&lt;br&gt;- *required* for create operations|
|interfaceid|string|ID of the LLD rule's host interface.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* if LLD rule belongs to host and `type` is set to "Zabbix agent", "IPMI agent", "JMX agent", or "SNMP agent"&lt;br&gt;- *supported* if LLD rule belongs to host and `type` is set to "Simple check", "External check", "SSH agent", "TELNET agent", or "HTTP agent"|
|key\_|string|LLD rule key.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* for create operations&lt;br&gt;- *read-only* for inherited objects|
|name|string|Name of the LLD rule.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* for create operations&lt;br&gt;- *read-only* for inherited objects|
|type|integer|Type of the LLD rule.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - Zabbix agent;&lt;br&gt;2 - Zabbix trapper;&lt;br&gt;3 - Simple check;&lt;br&gt;5 - Zabbix internal;&lt;br&gt;7 - Zabbix agent (active);&lt;br&gt;10 - External check;&lt;br&gt;11 - Database monitor;&lt;br&gt;12 - IPMI agent;&lt;br&gt;13 - SSH agent;&lt;br&gt;14 - TELNET agent;&lt;br&gt;16 - JMX agent;&lt;br&gt;18 - Dependent item;&lt;br&gt;19 - HTTP agent;&lt;br&gt;20 - SNMP agent;&lt;br&gt;21 - Script.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* for create operations&lt;br&gt;- *read-only* for inherited objects|
|url|string|URL string.&lt;br&gt;Supports user macros, {HOST.IP}, {HOST.CONN}, {HOST.DNS}, {HOST.HOST}, {HOST.NAME}, {ITEM.ID}, {ITEM.KEY}.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* if `type` is set to "HTTP agent"&lt;br&gt;- *read-only* for inherited objects|
|allow\_traps|integer|Allow to populate value similarly to the trapper item.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* Do not allow to accept incoming data;&lt;br&gt;1 - Allow to accept incoming data.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `type` is set to "HTTP agent"|
|authtype|integer|Authentication method.&lt;br&gt;&lt;br&gt;Possible values if `type` is set to "SSH agent":&lt;br&gt;0 - *(default)* password;&lt;br&gt;1 - public key.&lt;br&gt;&lt;br&gt;Possible values if `type` is set to "HTTP agent":&lt;br&gt;0 - *(default)* none;&lt;br&gt;1 - basic;&lt;br&gt;2 - NTLM.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `type` is set to "SSH agent" or "HTTP agent"&lt;br&gt;- *read-only* for inherited objects (if `type` is set to "HTTP agent")|
|description|string|Description of the LLD rule.|
|error|string|Error text if there are problems updating the LLD rule.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *read-only*|
|follow\_redirects|integer|Follow response redirects while pooling data.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - Do not follow redirects;&lt;br&gt;1 - *(default)* Follow redirects.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `type` is set to "HTTP agent"&lt;br&gt;- *read-only* for inherited objects|
|headers|object|Object with HTTP(S) request headers, where header name is used as key and header value as value.&lt;br&gt;&lt;br&gt;Example: { "User-Agent": "Zabbix" }&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `type` is set to "HTTP agent"&lt;br&gt;- *read-only* for inherited objects|
|http\_proxy|string|HTTP(S) proxy connection string.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `type` is set to "HTTP agent"&lt;br&gt;- *read-only* for inherited objects|
|ipmi\_sensor|string|IPMI sensor.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* if `type` is set to "IPMI agent" and `key_` is not set to "ipmi.get"&lt;br&gt;- *supported* if `type` is set to "IPMI agent"&lt;br&gt;- *read-only* for inherited objects|
|jmx\_endpoint|string|JMX agent custom connection string.&lt;br&gt;&lt;br&gt;Default: service:jmx:rmi:///jndi/rmi://{HOST.CONN}:{HOST.PORT}/jmxrmi&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `type` is set to "JMX agent"|
|lifetime|string|Time period after which items that are no longer discovered will be deleted. Accepts seconds, time unit with suffix, or a user macro.&lt;br&gt;&lt;br&gt;Default: `30d`.|
|master\_itemid|integer|Master item ID.&lt;br&gt;Recursion up to 3 dependent items and maximum count of dependent items equal to 999 are allowed.&lt;br&gt;Discovery rule cannot be master item for another discovery rule.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* if `type` is set to "Dependent item"&lt;br&gt;- *read-only* for inherited objects|
|output\_format|integer|Should the response be converted to JSON.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* Store raw;&lt;br&gt;1 - Convert to JSON.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `type` is set to "HTTP agent"&lt;br&gt;- *read-only* for inherited objects|
|params|string|Additional parameters depending on the type of the LLD rule:&lt;br&gt;- executed script for SSH and Telnet LLD rules;&lt;br&gt;- SQL query for database monitor LLD rules;&lt;br&gt;- formula for calculated LLD rules.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* if `type` is set to "Database monitor", "SSH agent", "TELNET agent", or "Script"&lt;br&gt;- *read-only* for inherited objects (if `type` is set to "Script")|
|parameters|array|Additional parameters if `type` is set to "Script".&lt;br&gt;Array of objects with `name` and `value` properties, where `name` must be unique.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `type` is set to "Script"&lt;br&gt;- *read-only* for inherited objects|
|password|string|Password for authentication.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* if `type` is set to "JMX agent" and `username` is set&lt;br&gt;- *supported* if `type` is set to "Simple check", "Database monitor", "SSH agent", "TELNET agent", or "HTTP agent"&lt;br&gt;- *read-only* for inherited objects (if `type` is set to "HTTP agent")|
|post\_type|integer|Type of post data body stored in `posts` property.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* Raw data;&lt;br&gt;2 - JSON data;&lt;br&gt;3 - XML data.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `type` is set to "HTTP agent"&lt;br&gt;- *read-only* for inherited objects|
|posts|string|HTTP(S) request body data.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* if `type` is set to "HTTP agent" and `post_type` is set to "JSON data" or "XML data"&lt;br&gt;- *supported* if `type` is set to "HTTP agent" and `post_type` is set to "Raw data"&lt;br&gt;- *read-only* for inherited objects|
|privatekey|string|Name of the private key file.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* if `type` is set to "SSH agent" and `authtype` is set to "public key"|
|publickey|string|Name of the public key file.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* if `type` is set to "SSH agent" and `authtype` is set to "public key"|
|query\_fields|array|Query parameters.&lt;br&gt;Array of objects with `key`:`value` pairs, where `value` can be an empty string.|
|request\_method|integer|Type of request method.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* GET;&lt;br&gt;1 - POST;&lt;br&gt;2 - PUT;&lt;br&gt;3 - HEAD.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `type` is set to "HTTP agent"&lt;br&gt;- *read-only* for inherited objects|
|retrieve\_mode|integer|What part of response should be stored.&lt;br&gt;&lt;br&gt;Possible values if `request_method` is set to "GET", "POST", or "PUT":&lt;br&gt;0 - *(default)* Body;&lt;br&gt;1 - Headers;&lt;br&gt;2 - Both body and headers will be stored.&lt;br&gt;&lt;br&gt;Possible values if `request_method` is set to "HEAD":&lt;br&gt;1 - Headers.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `type` is set to "HTTP agent"&lt;br&gt;- *read-only* for inherited objects|
|snmp\_oid|string|SNMP OID.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* if `type` is set to "SNMP agent"&lt;br&gt;- *read-only* for inherited objects|
|ssl\_cert\_file|string|Public SSL Key file path.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `type` is set to "HTTP agent"&lt;br&gt;- *read-only* for inherited objects|
|ssl\_key\_file|string|Private SSL Key file path.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `type` is set to "HTTP agent"&lt;br&gt;- *read-only* for inherited objects|
|ssl\_key\_password|string|Password for SSL Key file.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `type` is set to "HTTP agent"&lt;br&gt;- *read-only* for inherited objects|
|state|integer|State of the LLD rule.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* normal;&lt;br&gt;1 - not supported.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *read-only*|
|status|integer|Status of the LLD rule.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* enabled LLD rule;&lt;br&gt;1 - disabled LLD rule.|
|status\_codes|string|Ranges of required HTTP status codes, separated by commas. Also supports user macros as part of comma separated list.&lt;br&gt;&lt;br&gt;Example: 200,200-{$M},{$M},200-400&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `type` is set to "HTTP agent"&lt;br&gt;- *read-only* for inherited objects|
|templateid|string|ID of the parent template LLD rule.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *read-only*|
|timeout|string|Item data polling request timeout. Supports user macros.&lt;br&gt;&lt;br&gt;Default: `3s`.&lt;br&gt;Maximum value: `60s`.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `type` is set to "HTTP agent" or "Script"&lt;br&gt;- *read-only* for inherited objects|
|trapper\_hosts|string|Allowed hosts.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `type` is set to "Zabbix trapper", or if `type` is set to "HTTP agent" and `allow_traps` is set to "Allow to accept incoming data"|
|username|string|Username for authentication.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* if `type` is set to "SSH agent", "TELNET agent", or if `type` is set to "JMX agent" and `password` is set&lt;br&gt;- *supported* if `type` is set to "Simple check", "Database monitor", or "HTTP agent"&lt;br&gt;- *read-only* for inherited objects (if `type` is set to "HTTP agent")|
|uuid|string|Universal unique identifier, used for linking imported LLD rules to already existing ones. Auto-generated, if not given.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if the LLD rule belongs to a template|
|verify\_host|integer|Validate host name in URL is in Common Name field or a Subject Alternate Name field of host certificate.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* Do not validate;&lt;br&gt;1 - Validate.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `type` is set to "HTTP agent"&lt;br&gt;- *read-only* for inherited objects|
|verify\_peer|integer|Validate is host certificate authentic.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* Do not validate;&lt;br&gt;1 - Validate.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `type` is set to "HTTP agent"&lt;br&gt;- *read-only* for inherited objects|</source>
      </trans-unit>
      <trans-unit id="b0412737" xml:space="preserve">
        <source>### LLD rule filter

The LLD rule filter object defines a set of conditions that can be used
to filter discovered objects. It has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|conditions|array|Set of filter conditions to use for filtering results.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|
|evaltype|integer|Filter condition evaluation method.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - and/or;&lt;br&gt;1 - and;&lt;br&gt;2 - or;&lt;br&gt;3 - custom expression.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|
|eval\_formula|string|Generated expression that will be used for evaluating filter conditions. The expression contains IDs that reference specific filter conditions by its `formulaid`. The value of `eval_formula` is equal to the value of `formula` for filters with a custom expression.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *read-only*|
|formula|string|User-defined expression to be used for evaluating conditions of filters with a custom expression. The expression must contain IDs that reference specific filter conditions by its `formulaid`. The IDs used in the expression must exactly match the ones defined in the filter conditions: no condition can remain unused or omitted.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* if `evaltype` is set to "custom expression"|</source>
      </trans-unit>
      <trans-unit id="be83ef5c" xml:space="preserve">
        <source>#### LLD rule filter condition

The LLD rule filter condition object defines a separate check to perform
on the value of an LLD macro. It has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|macro|string|LLD macro to perform the check on.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|
|value|string|Value to compare with.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|
|formulaid|string|Arbitrary unique ID that is used to reference the condition from a custom expression. Can only contain capital-case letters. The ID must be defined by the user when modifying filter conditions, but will be generated anew when requesting them afterward.|
|operator|integer|Condition operator.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;8 - *(default)* matches regular expression;&lt;br&gt;9 - does not match regular expression;&lt;br&gt;12 - exists;&lt;br&gt;13 - does not exist.|

::: notetip
To better understand how to use filters with various
types of expressions, see examples on the
[discoveryrule.get](get#retrieving_filter_conditions) and
[discoveryrule.create](create#using_a_custom_expression_filter) method
pages.
:::</source>
      </trans-unit>
      <trans-unit id="4a2b52b9" xml:space="preserve">
        <source>### LLD macro path

The LLD macro path has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|lld\_macro|string|LLD macro.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|
|path|string|Selector for value which will be assigned to corresponding macro.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|</source>
      </trans-unit>
      <trans-unit id="14dc55c1" xml:space="preserve">
        <source>### LLD rule preprocessing

The LLD rule preprocessing object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|type|integer|The preprocessing option type.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;5 - Regular expression matching;&lt;br&gt;11 - XML XPath;&lt;br&gt;12 - JSONPath;&lt;br&gt;15 - Does not match regular expression;&lt;br&gt;16 - Check for error in JSON;&lt;br&gt;17 - Check for error in XML;&lt;br&gt;20 - Discard unchanged with heartbeat;&lt;br&gt;23 - Prometheus to JSON;&lt;br&gt;24 - CSV to JSON;&lt;br&gt;25 - Replace;&lt;br&gt;27 - XML to JSON;&lt;br&gt;28 - SNMP walk value;&lt;br&gt;29 - SNMP walk to JSON.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|
|params|string|Additional parameters used by preprocessing option. Multiple parameters are separated by the newline (\\n) character.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* if `type` is set to "Regular expression matching" (5), "XML XPath" (11), "JSONPath" (12), "Does not match regular expression" (15), "Check for error in JSON" (16), "Check for error in XML" (17), "Discard unchanged with heartbeat" (20), "Prometheus to JSON" (23), "CSV to JSON" (24), "Replace" (25), "SNMP walk value" (28), or "SNMP walk to JSON" (29)|
|error\_handler|integer|Action type used in case of preprocessing step failure.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - Error message is set by Zabbix server;&lt;br&gt;1 - Discard value;&lt;br&gt;2 - Set custom value;&lt;br&gt;3 - Set custom error message.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* if `type` is set to "Regular expression matching" (5), "XML XPath" (11), "JSONPath" (12), "Does not match regular expression" (15), "Check for error in JSON" (16), "Check for error in XML" (17), "Prometheus to JSON" (23), "CSV to JSON" (24), "XML to JSON" (27), "SNMP walk value" (28), or "SNMP walk to JSON" (29)|
|error\_handler\_params|string|Error handler parameters.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* if `error_handler` is set to "Set custom value" or "Set custom error message"|

The following parameters and error handlers are supported for each preprocessing type.

|Preprocessing type|Name|Parameter 1|Parameter 2|Parameter 3|Supported error handlers|
|------------------|----|-----------|-----------|-----------|------------------------|
|5|Regular expression|pattern^1^|output^2^|&lt;|0, 1, 2, 3|
|11|XML XPath|path^3^|&lt;|&lt;|0, 1, 2, 3|
|12|JSONPath|path^3^|&lt;|&lt;|0, 1, 2, 3|
|15|Does not match regular expression|pattern^1^|&lt;|&lt;|0, 1, 2, 3|
|16|Check for error in JSON|path^3^|&lt;|&lt;|0, 1, 2, 3|
|17|Check for error in XML|path^3^|&lt;|&lt;|0, 1, 2, 3|
|20|Discard unchanged with heartbeat|seconds^4,\ 5,\ 6^|&lt;|&lt;|&lt;|
|23|Prometheus to JSON|pattern^5,\ 7^|&lt;|&lt;|0, 1, 2, 3|
|24|CSV to JSON|character^2^|character^2^|0,1|0, 1, 2, 3|
|25|Replace|search string^2^|replacement^2^|&lt;|&lt;|
|27|XML to JSON|&lt;|&lt;|&lt;|0, 1, 2, 3|
|28|SNMP walk value|OID^2^|Format:&lt;br&gt;0 - Unchanged&lt;br&gt;1 - UTF-8 from Hex-STRING&lt;br&gt;2 - MAC from Hex-STRING&lt;br&gt;3 - Integer from BITS|&lt;|0, 1, 2, 3|
|29|SNMP walk to JSON^9^|Field name^2^|OID prefix^2^|Format:&lt;br&gt;0 - Unchanged&lt;br&gt;1 - UTF-8 from Hex-STRING&lt;br&gt;2 - MAC from Hex-STRING&lt;br&gt;3 - Integer from BITS|0, 1, 2, 3|

^1^ regular expression\
^2^ string\
^3^ JSONPath or XML XPath\
^4^ positive integer (with support of time suffixes, e.g. 30s, 1m, 2h,
1d)\
^5^ user macro\
^6^ LLD macro\
^7^ Prometheus pattern following the syntax:
`&lt;metric name&gt;{&lt;label name&gt;="&lt;label value&gt;", ...} == &lt;value&gt;`. Each
Prometheus pattern component (metric, label name, label value and metric
value) can be user macro.\
^8^ Prometheus output following the syntax: `&lt;label name&gt;`.
^9^ Supports multiple "Field name,OID prefix,Format records" records delimited by a new line character.</source>
      </trans-unit>
      <trans-unit id="2f32e5f0" xml:space="preserve">
        <source>### LLD rule overrides

The LLD rule overrides object defines a set of rules (filters,
conditions and operations) that are used to override properties of
different prototype objects. It has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|name|string|Unique override name.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|
|step|integer|Unique order number of the override.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|
|stop|integer|Stop processing next overrides if matches.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* don't stop processing overrides;&lt;br&gt;1 - stop processing overrides if filter matches.|
|filter|object|Override filter.|
|operations|array|Override operations.|</source>
      </trans-unit>
      <trans-unit id="573b52c4" xml:space="preserve">
        <source>#### LLD rule override filter

The LLD rule override filter object defines a set of conditions that if
they match the discovered object the override is applied. It has the
following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|evaltype|integer|Override filter condition evaluation method.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - and/or;&lt;br&gt;1 - and;&lt;br&gt;2 - or;&lt;br&gt;3 - custom expression.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|
|conditions|array|Set of override filter conditions to use for matching the discovered objects.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|
|eval\_formula|string|Generated expression that will be used for evaluating override filter conditions. The expression contains IDs that reference specific override filter conditions by its `formulaid`. The value of `eval_formula` is equal to the value of `formula` for filters with a custom expression.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *read-only*|
|formula|string|User-defined expression to be used for evaluating conditions of override filters with a custom expression. The expression must contain IDs that reference specific override filter conditions by its `formulaid`. The IDs used in the expression must exactly match the ones defined in the override filter conditions: no condition can remain unused or omitted.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* if `evaltype` is set to "custom expression"|</source>
      </trans-unit>
      <trans-unit id="e47f6c6a" xml:space="preserve">
        <source>#### LLD rule override filter condition

The LLD rule override filter condition object defines a separate check
to perform on the value of an LLD macro. It has the following
properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|macro|string|LLD macro to perform the check on.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|
|value|string|Value to compare with.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|
|formulaid|string|Arbitrary unique ID that is used to reference the condition from a custom expression. Can only contain capital-case letters. The ID must be defined by the user when modifying filter conditions, but will be generated anew when requesting them afterward.|
|operator|integer|Condition operator.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;8 - *(default)* matches regular expression;&lt;br&gt;9 - does not match regular expression;&lt;br&gt;12 - exists;&lt;br&gt;13 - does not exist.|</source>
      </trans-unit>
      <trans-unit id="e7b08f3c" xml:space="preserve">
        <source>#### LLD rule override operation

The LLD rule override operation is combination of conditions and actions
to perform on the prototype object. It has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|operationobject|integer|Type of discovered object to perform the action.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - Item prototype;&lt;br&gt;1 - Trigger prototype;&lt;br&gt;2 - Graph prototype;&lt;br&gt;3 - Host prototype.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|
|operator|integer|Override condition operator.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* equals;&lt;br&gt;1 - does not equal;&lt;br&gt;2 - contains;&lt;br&gt;3 - does not contain;&lt;br&gt;8 - matches;&lt;br&gt;9 - does not match.|
|value|string|Pattern to match item, trigger, graph or host prototype name depending on selected object.|
|opstatus|object|Override operation status object for item, trigger and host prototype objects.|
|opdiscover|object|Override operation discover status object (all object types).|
|opperiod|object|Override operation period (update interval) object for item prototype object.|
|ophistory|object|Override operation history object for item prototype object.|
|optrends|object|Override operation trends object for item prototype object.|
|opseverity|object|Override operation severity object for trigger prototype object.|
|optag|array|Override operation tag object for trigger and host prototype objects.|
|optemplate|array|Override operation template object for host prototype object.|
|opinventory|object|Override operation inventory object for host prototype object.|</source>
      </trans-unit>
      <trans-unit id="52d3b579" xml:space="preserve">
        <source>##### LLD rule override operation status

LLD rule override operation status that is set to discovered object. It
has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|status|integer|Override the status for selected object.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - Create enabled;&lt;br&gt;1 - Create disabled.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|</source>
      </trans-unit>
      <trans-unit id="ff620350" xml:space="preserve">
        <source>##### LLD rule override operation discover

LLD rule override operation discover status that is set to discovered
object. It has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|discover|integer|Override the discover status for selected object.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - Yes, continue discovering the objects;&lt;br&gt;1 - No, new objects will not be discovered and existing ones will be marked as lost.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|</source>
      </trans-unit>
      <trans-unit id="6e469c1b" xml:space="preserve">
        <source>##### LLD rule override operation period

LLD rule override operation period is an update interval value (supports
custom intervals) that is set to discovered item. It has the following
properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|delay|string|Override the update interval of the item prototype. Accepts seconds or a time unit with suffix (30s,1m,2h,1d) as well as flexible and scheduling intervals and user macros or LLD macros. Multiple intervals are separated by a semicolon.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|</source>
      </trans-unit>
      <trans-unit id="ff97489f" xml:space="preserve">
        <source>##### LLD rule override operation history

LLD rule override operation history value that is set to discovered
item. It has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|history|string|Override the history of item prototype which is a time unit of how long the history data should be stored. Also accepts user macro and LLD macro.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|</source>
      </trans-unit>
      <trans-unit id="10b52601" xml:space="preserve">
        <source>##### LLD rule override operation trends

LLD rule override operation trends value that is set to discovered item.
It has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|trends|string|Override the trends of item prototype which is a time unit of how long the trends data should be stored. Also accepts user macro and LLD macro.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|</source>
      </trans-unit>
      <trans-unit id="991f3bdc" xml:space="preserve">
        <source>##### LLD rule override operation severity

LLD rule override operation severity value that is set to discovered
trigger. It has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|severity|integer|Override the severity of trigger prototype.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* not classified;&lt;br&gt;1 - information;&lt;br&gt;2 - warning;&lt;br&gt;3 - average;&lt;br&gt;4 - high;&lt;br&gt;5 - disaster.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|</source>
      </trans-unit>
      <trans-unit id="0693b8f3" xml:space="preserve">
        <source>##### LLD rule override operation tag

LLD rule override operation tag object contains tag name and value that
are set to discovered object. It has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|tag|string|New tag name.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|
|value|string|New tag value.|</source>
      </trans-unit>
      <trans-unit id="5eebc05a" xml:space="preserve">
        <source>##### LLD rule override operation template

LLD rule override operation template object that is linked to discovered
host. It has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|templateid|string|Override the template of host prototype linked templates.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|</source>
      </trans-unit>
      <trans-unit id="9c5cc3d1" xml:space="preserve">
        <source>##### LLD rule override operation inventory

LLD rule override operation inventory mode value that is set to
discovered host. It has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|inventory\_mode|integer|Override the host prototype inventory mode.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;-1 - disabled;&lt;br&gt;0 - *(default)* manual;&lt;br&gt;1 - automatic.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|</source>
      </trans-unit>
    </body>
  </file>
</xliff>
