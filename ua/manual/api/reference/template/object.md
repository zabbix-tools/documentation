[comment]: # translation:outdated

[comment]: # ({new-ac6966b9})
# > Template object

The following objects are directly related to the `template` API.

[comment]: # ({/new-ac6966b9})

[comment]: # ({new-ccfaad06})
### Template

The template object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|templateid|string|*(readonly)* ID of the template.|
|**host**<br>(required)|string|Technical name of the template.|
|description|text|Description of the template.|
|name|string|Visible name of the template.<br><br>Default: `host` property value.|
|uuid|string|Universal unique identifier, used for linking imported templates to already existing ones. Auto-generated, if not given.<br><br>For update operations this field is *readonly*.|

[comment]: # ({/new-ccfaad06})

[comment]: # ({new-2248aef2})
### Template tag

The template tag object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**tag**<br>(required)|string|Template tag name.|
|value|string|Template tag value.|

[comment]: # ({/new-2248aef2})
