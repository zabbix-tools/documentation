[comment]: # translation:outdated

[comment]: # ({new-8c618db1})
# template.massadd

[comment]: # ({/new-8c618db1})

[comment]: # ({new-399b9834})
### Description

`object template.massadd(object parameters)`

This method allows to simultaneously add multiple related objects to the
given templates.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-399b9834})

[comment]: # ({new-4167b841})
### Parameters

`(object)` Parameters containing the IDs of the templates to update and
the objects to add to the templates.

The method accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|**templates**<br>(required)|object/array|Templates to be updated.<br><br>The templates must have the `templateid` property defined.|
|groups|object/array|Host groups to add the given templates to.<br><br>The host groups must have the `groupid` property defined.|
|macros|object/array|User macros to be created for the given templates.|
|templates\_link|object/array|Templates to link to the given templates.<br><br>The templates must have the `templateid` property defined.|

[comment]: # ({/new-4167b841})

[comment]: # ({new-dcba01c8})
### Return values

`(object)` Returns an object containing the IDs of the updated templates
under the `templateids` property.

[comment]: # ({/new-dcba01c8})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-f8fe44e4})
#### Link a group to a templates

Add host group "2" to a two templates.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "template.massadd",
    "params": {
        "templates": [
            {
                "templateid": "10085"
            },
            {
                "templateid": "10086"
            }
        ],
        "groups": [
            {
                "groupid": "2"
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "templateids": [
            "10085",
            "10086"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-f8fe44e4})

[comment]: # ({new-7dbed7e5})
#### Link two templates to a template

Link templates "10106" and "10104" to template "10073".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "template.massadd",
    "params": {
        "templates": [
            {
                "templateid": "10073"
            }
        ],
        "templates_link": [
            {
                "templateid": "10106"
            },
            {
                "templateid": "10104"
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "templateids": [
            "10073"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-7dbed7e5})


[comment]: # ({new-bd8dd370})
### See also

-   [template.update](update)
-   [Host](/manual/api/reference/host/object#host)
-   [Host group](/manual/api/reference/hostgroup/object#host_group)
-   [User macro](/manual/api/reference/usermacro/object#hosttemplate_level_macro)

[comment]: # ({/new-bd8dd370})

[comment]: # ({new-2f7abcb1})
### Source

CTemplate::massAdd() in *ui/include/classes/api/services/CTemplate.php*.

[comment]: # ({/new-2f7abcb1})
