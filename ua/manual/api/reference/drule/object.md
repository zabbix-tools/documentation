[comment]: # translation:outdated

[comment]: # ({new-141f308b})
# > Discovery rule object

The following objects are directly related to the `drule` API.

[comment]: # ({/new-141f308b})

[comment]: # ({new-8884a1d1})
### Discovery rule

The discovery rule object defines a network discovery rule. It has the
following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|druleid|string|*(readonly)* ID of the discovery rule.|
|**iprange**<br>(required)|string|One or several IP ranges to check separated by commas.<br><br>Refer to the [network discovery configuration](/manual/discovery/network_discovery/rule) section for more information on supported formats of IP ranges.|
|**name**<br>(required)|string|Name of the discovery rule.|
|delay|string|Execution interval of the discovery rule. Accepts seconds, time unit with suffix and user macro.<br><br>Default: 1h.|
|nextcheck|timestamp|*(readonly)* Time when the discovery rule will be executed next.|
|proxy\_hostid|string|ID of the proxy used for discovery.|
|status|integer|Whether the discovery rule is enabled.<br><br>Possible values:<br>0 - *(default)* enabled;<br>1 - disabled.|

[comment]: # ({/new-8884a1d1})
