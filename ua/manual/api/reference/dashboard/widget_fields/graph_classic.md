[comment]: # translation:outdated

[comment]: # ({new-a55a0f20})
# 9 Graph (classic)

[comment]: # ({/new-a55a0f20})

[comment]: # ({new-028866a6})
### Description

These parameters and the possible property values for the respective dashboard widget field objects allow to configure
the [*Graph (classic)*](/manual/web_interface/frontend_sections/dashboards/widgets/graph_classic) widget in `dashboard.create` and `dashboard.update` methods.

[comment]: # ({/new-028866a6})

[comment]: # ({new-f319d6dc})
### Parameters

The following parameters are supported for the *Graph (classic)* widget.

|Parameter|[type](/manual/api/reference/dashboard/object#dashboard-widget-field)|name|value|
|-----|-|-----|-------------------|
|*Refresh interval*|0|rf_rate|0 - No refresh;<br>10 - 10 seconds;<br>30 - 30 seconds;<br>60 - *(default)* 1 minute;<br>120 - 2 minutes;<br>600 - 10 minutes;<br>900 - 15 minutes.|
|*Source*|0|source_type|0 - *(default)* Graph;<br>1 - Simple graph.|
|*Graph*|6|graphid|[Graph](/manual/api/reference/graph/get) ID.<br><br>Parameter *Graph* required if *Source* is set to "Graph".|
|*Item*|4|itemid|[Item](/manual/api/reference/item/get) ID.<br><br>Parameter *Item* required if *Source* is set to "Simple graph".|
|*Show legend*|0|show_legend|0 - Disabled;<br>1 - *(default)* Enabled.|
|*Enable host selection*|0|dynamic|0 - *(default)* Disabled;<br>1 - Enabled.|

[comment]: # ({/new-f319d6dc})

[comment]: # ({new-313ae2b7})
### Examples

The following examples aim to only describe the configuration of the dashboard widget field objects for the *Graph (classic)* widget.
For more information on configuring a dashboard, see [`dashboard.create`](/manual/api/reference/dashboard/create).

[comment]: # ({/new-313ae2b7})

[comment]: # ({new-a6ab08a1})
#### Configuring a *Graph (classic)* widget

Configure a *Graph (classic)* widget that displays a simple graph for the item "42269".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "dashboard.create",
    "params": {
        "name": "My dashboard",
        "display_period": 30,
        "auto_start": 1,
        "pages": [
            {
                "widgets": [
                    {
                        "type": "graph",
                        "name": "Graph (classic)",
                        "x": 0,
                        "y": 0,
                        "width": 12,
                        "height": 5,
                        "view_mode": 0,
                        "fields": [
                            {
                                "type": 0,
                                "name": "source_type",
                                "value": 1
                            },
                            {
                                "type": 4,
                                "name": "itemid",
                                "value": 42269
                            }
                        ]
                    }
                ]
            }
        ],
        "userGroups": [
            {
                "usrgrpid": 7,
                "permission": 2
            }
        ],
        "users": [
            {
                "userid": 1,
                "permission": 3
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "dashboardids": [
            "3"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-a6ab08a1})

[comment]: # ({new-c57c98c8})
### See also

-   [Dashboard widget field](/manual/api/reference/dashboard/object#dashboard-widget-field)
-   [`dashboard.create`](/manual/api/reference/dashboard/create)
-   [`dashboard.update`](/manual/api/reference/dashboard/update)

[comment]: # ({/new-c57c98c8})
