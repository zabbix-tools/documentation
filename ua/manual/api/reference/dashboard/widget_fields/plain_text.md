[comment]: # translation:outdated

[comment]: # ({new-8203455f})
# 15 Plain text

[comment]: # ({/new-8203455f})

[comment]: # ({new-4f35bb7d})
### Description

These parameters and the possible property values for the respective dashboard widget field objects allow to configure
the [*Plain text*](/manual/web_interface/frontend_sections/dashboards/widgets/plain_text) widget in `dashboard.create` and `dashboard.update` methods.

[comment]: # ({/new-4f35bb7d})

[comment]: # ({new-fc685d34})
### Parameters

The following parameters are supported for the *Plain text* widget.

|Parameter|[type](/manual/api/reference/dashboard/object#dashboard-widget-field)|name|value|
|-----|-|-----|-------------------|
|*Refresh interval*|0|rf_rate|0 - No refresh;<br>10 - 10 seconds;<br>30 - 30 seconds;<br>60 - *(default)* 1 minute;<br>120 - 2 minutes;<br>600 - 10 minutes;<br>900 - 15 minutes.|
|***Items***<br>(required)|4|itemids|[Item](/manual/api/reference/item/get) ID.<br><br>Note: To configure multiple items, create a dashboard widget field object for each item.|
|*Items location*|0|style|0 - *(default)* Left;<br>1 - Top.|
|*Show lines*|0|show_lines|Valid values range from 1-100.<br><br>Default: 25.|
|*Show text as HTML*|0|show_as_html|0 - *(default)* Disabled;<br>1 - Enabled.|
|*Enable host selection*|0|dynamic|0 - *(default)* Disabled;<br>1 - Enabled.|

[comment]: # ({/new-fc685d34})

[comment]: # ({new-08d24b15})
### Examples

The following examples aim to only describe the configuration of the dashboard widget field objects for the *Plain text* widget.
For more information on configuring a dashboard, see [`dashboard.create`](/manual/api/reference/dashboard/create).

[comment]: # ({/new-08d24b15})

[comment]: # ({new-753680ea})
#### Configuring a *Plain text* widget

Configure a *Plain text* widget that displays latest data for items "42269" and "42253".
In addition, configure the item names to be located at the top of the data columns, and only 15 lines of data to be displayed.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "dashboard.create",
    "params": {
        "name": "My dashboard",
        "display_period": 30,
        "auto_start": 1,
        "pages": [
            {
                "widgets": [
                    {
                        "type": "plaintext",
                        "name": "Plain text",
                        "x": 0,
                        "y": 0,
                        "width": 6,
                        "height": 3,
                        "view_mode": 0,
                        "fields": [
                            {
                                "type": 4,
                                "name": "itemids",
                                "value": 42269
                            },
                            {
                                "type": 4,
                                "name": "itemids",
                                "value": 42253
                            },
                            {
                                "type": 0,
                                "name": "style",
                                "value": 1
                            },
                            {
                                "type": 0,
                                "name": "show_lines",
                                "value": 15
                            }
                        ]
                    }
                ]
            }
        ],
        "userGroups": [
            {
                "usrgrpid": 7,
                "permission": 2
            }
        ],
        "users": [
            {
                "userid": 1,
                "permission": 3
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "dashboardids": [
            "3"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-753680ea})

[comment]: # ({new-090ccc38})
### See also

-   [Dashboard widget field](/manual/api/reference/dashboard/object#dashboard-widget-field)
-   [`dashboard.create`](/manual/api/reference/dashboard/create)
-   [`dashboard.update`](/manual/api/reference/dashboard/update)

[comment]: # ({/new-090ccc38})
