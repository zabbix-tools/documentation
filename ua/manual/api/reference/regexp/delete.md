[comment]: # translation:outdated

[comment]: # ({new-c073dc7d})
# regexp.delete

[comment]: # ({/new-c073dc7d})

[comment]: # ({new-702ce926})
### Description

`object regexp.delete(array regexpids)`

This method allows to delete global regular expressions.

::: noteclassic
This method is only available to *Super admin* user types.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-702ce926})

[comment]: # ({new-99c30c43})
### Parameters

`(array)` IDs of the regular expressions to delete.

[comment]: # ({/new-99c30c43})

[comment]: # ({new-b583665a})
### Return values

`(object)` Returns an object containing the IDs of the deleted regular
expressions under the `regexpids` property.

[comment]: # ({/new-b583665a})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-13da0716})
#### Deleting multiple global regular expressions.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "regexp.delete",
    "params": [
        "16",
        "17"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "regexpids": [
            "16",
            "17"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-13da0716})

[comment]: # ({new-e0390136})
### Source

CRegexp::delete() in *ui/include/classes/api/services/CRegexp.php*.

[comment]: # ({/new-e0390136})
