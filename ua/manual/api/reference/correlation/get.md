[comment]: # translation:outdated

[comment]: # ({new-7416ad19})
# correlation.get

[comment]: # ({/new-7416ad19})

[comment]: # ({new-858936aa})
### Description

`integer/array correlation.get(object parameters)`

The method allows to retrieve correlations according to the given
parameters.

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-858936aa})

[comment]: # ({new-06a650a0})
### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|correlationids|string/array|Return only correlations with the given IDs.|
|selectFilter|query|Return a [filter](/manual/api/reference/correlation/object#correlation_filter) property with the correlation conditions.|
|selectOperations|query|Return an [operations](/manual/api/reference/correlation/object#correlation_operation) property with the correlation operations.|
|sortfield|string/array|Sort the result by the given properties.<br><br>Possible values are: `correlationid`, `name` and `status`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters).|
|editable|boolean|^|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

[comment]: # ({/new-06a650a0})

[comment]: # ({new-7223bab1})
### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-d14c4f20})
#### Retrieve correlations

Retrieve all configured correlations together with correlation
conditions and operations. The filter uses the "and/or" evaluation type,
so the `formula` property is empty and `eval_formula` is generated
automatically.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "correlation.get",
    "params": {
        "output": "extend",
        "selectOperations": "extend",
        "selectFilter": "extend"
    },
    "auth": "343baad4f88b4106b9b5961e77437688",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "correlationid": "1",
            "name": "Correlation 1",
            "description": "",
            "status": "0",
            "filter": {
                "evaltype": "0",
                "formula": "",
                "conditions": [
                    {
                        "type": "3",
                        "oldtag": "error",
                        "newtag": "ok",
                        "formulaid": "A"
                    }
                ],
                "eval_formula": "A"
            },
            "operations": [
                {
                    "type": "0"
                }
            ]
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-d14c4f20})

[comment]: # ({new-e7d15410})
### See also

-   [Correlation filter](object#correlation_filter)
-   [Correlation operation](object#correlation_operation)

[comment]: # ({/new-e7d15410})

[comment]: # ({new-bbab04bd})
### Source

CCorrelation::get() in
*ui/include/classes/api/services/CCorrelation.php*.

[comment]: # ({/new-bbab04bd})
