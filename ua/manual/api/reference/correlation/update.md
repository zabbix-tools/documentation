[comment]: # translation:outdated

[comment]: # ({new-c71a2e54})
# correlation.update

[comment]: # ({/new-c71a2e54})

[comment]: # ({new-b9562236})
### Description

`object correlation.update(object/array correlations)`

This method allows to update existing correlations.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-b9562236})

[comment]: # ({new-326bc80e})
### Parameters

`(object/array)` Correlation properties to be updated.

The `correlationid` property must be defined for each correlation, all
other properties are optional. Only the passed properties will be
updated, all others will remain unchanged.

Additionally to the [standard correlation
properties](object#correlation), the method accepts the following
parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|filter|object|Correlation [filter](/manual/api/reference/correlation/object#correlation_filter) object to replace the current filter.|
|operations|array|Correlation [operations](/manual/api/reference/correlation/object#correlation_operation) to replace existing operations.|

[comment]: # ({/new-326bc80e})

[comment]: # ({new-13e1dc13})
### Return values

`(object)` Returns an object containing the IDs of the updated
correlations under the `correlationids` property.

[comment]: # ({/new-13e1dc13})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-67bb2dfb})
#### Disable correlation

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "correlation.update",
    "params": {
        "correlationid": "1",
        "status": "1"
    },
    "auth": "343baad4f88b4106b9b5961e77437688",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "correlationids": [
            "1"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-67bb2dfb})

[comment]: # ({new-d6ef1778})
#### Replace conditions, but keep the evaluation method

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "correlation.update",
    "params": {
        "correlationid": "1",
        "filter": {
            "conditions": [
                {
                    "type": 3,
                    "oldtag": "error",
                    "newtag": "ok"
                }
            ]
        }
    },
    "auth": "343baad4f88b4106b9b5961e77437688",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "correlationids": [
            "1"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-d6ef1778})

[comment]: # ({new-e7d15410})
### See also

-   [Correlation filter](object#correlation_filter)
-   [Correlation operation](object#correlation_operation)

[comment]: # ({/new-e7d15410})

[comment]: # ({new-40fa665f})
### Source

CCorrelation::update() in
*ui/include/classes/api/services/CCorrelation.php*.

[comment]: # ({/new-40fa665f})
