[comment]: # translation:outdated

[comment]: # ({new-c28978f7})
# token.create

[comment]: # ({/new-c28978f7})

[comment]: # ({new-3cdde1b6})
### Description

`object token.create(object/array tokens)`

This method allows to create new tokens.

::: noteclassic
Only *Super admin* user type is allowed to manage tokens for
other users.
::: 

::: noteclassic
A token created by this method has
to be [generated](generate) before it is usable.
:::

[comment]: # ({/new-3cdde1b6})

[comment]: # ({new-b3691541})
### Parameters

`(object/array)` Tokens to create.

The method accepts tokens with the [standard token properties](object).

[comment]: # ({/new-b3691541})

[comment]: # ({new-da7805eb})
### Return values

`(object)` Returns an object containing the IDs of the created tokens
under the `tokenids` property. The order of the returned IDs matches the
order of the passed tokens.

[comment]: # ({/new-da7805eb})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-119ef871})
#### Create a token

Create an enabled token that never expires and authenticates user of ID
2.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "token.create",
    "params": {
        "name": "Your token",
        "userid": "2"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "tokenids": [
            "188"
        ]
    },
    "id": 1
}
```

Create a disabled token that expires at January 21st, 2021. This token
will authenticate current user.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "token.create",
    "params": {
        "name": "Your token",
        "status": "1",
        "expires_at": "1611238072"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "tokenids": [
            "189"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-119ef871})

[comment]: # ({new-5c90c156})
### Source

CToken::create() in *ui/include/classes/api/services/CToken.php*.

[comment]: # ({/new-5c90c156})
