[comment]: # translation:outdated

[comment]: # ({new-77549553})
# connector.delete

[comment]: # ({/new-77549553})

[comment]: # ({new-1e18cf5c})
### Description

`object connector.delete(array connectorids)`

This method allows to delete connector entries.

::: noteclassic
This method is only available to *Super admin* user type. Permissions to call the method can be revoked in user role settings.
See [User roles](/manual/web_interface/frontend_sections/users/user_roles) for more information.
:::

[comment]: # ({/new-1e18cf5c})

[comment]: # ({new-55d11efd})
### Parameters

`(array)` IDs of the connectors to delete.

[comment]: # ({/new-55d11efd})

[comment]: # ({new-5f42b70a})
### Return values

`(object)` Returns an object containing the IDs of the deleted connectors under the `connectorids` property.

[comment]: # ({/new-5f42b70a})

[comment]: # ({new-931b87dc})
### Examples

[comment]: # ({/new-931b87dc})

[comment]: # ({new-d07c0442})
#### Deleting multiple connectors

Delete two connector entries.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "connector.delete",
    "params": [
        3,
        5
    ],
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "connectorids": [
            "3",
            "5"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-d07c0442})

[comment]: # ({new-b3ee848b})
### Source

CConnector::delete() in *ui/include/classes/api/services/CConnector.php*.

[comment]: # ({/new-b3ee848b})
