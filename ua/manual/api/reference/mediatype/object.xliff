<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="ua" datatype="plaintext" original="manual/api/reference/mediatype/object.md">
    <body>
      <trans-unit id="2fbbc9a6" xml:space="preserve">
        <source># &gt; Media type object

The following objects are directly related to the `mediatype` API.</source>
      </trans-unit>
      <trans-unit id="4630c87a" xml:space="preserve">
        <source>### Media type

The media type object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|mediatypeid|string|ID of the media type.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *read-only*&lt;br&gt;- *required* for update operations|
|name|string|Name of the media type.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* for create operations|
|type|integer|Transport used by the media type.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - Email;&lt;br&gt;1 - Script;&lt;br&gt;2 - SMS;&lt;br&gt;4 - Webhook.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* for create operations|
|exec\_path|string|For script media types `exec_path` contains the name of the executed script.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* if `type` is set to "Script"|
|gsm\_modem|string|Serial device name of the GSM modem.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* if `type` is set to "SMS"|
|passwd|string|Authentication password.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `type` is set to "Email"|
|smtp\_email|string|Email address from which notifications will be sent.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* if `type` is set to "Email"|
|smtp\_helo|string|SMTP HELO.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* if `type` is set to "Email"|
|smtp\_server|string|SMTP server.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* if `type` is set to "Email"|
|smtp\_port|integer|SMTP server port to connect to.|
|smtp\_security|integer|SMTP connection security level to use.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - None;&lt;br&gt;1 - STARTTLS;&lt;br&gt;2 - SSL/TLS.|
|smtp\_verify\_host|integer|SSL verify host for SMTP.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - No;&lt;br&gt;1 - Yes.|
|smtp\_verify\_peer|integer|SSL verify peer for SMTP.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - No;&lt;br&gt;1 - Yes.|
|smtp\_authentication|integer|SMTP authentication method to use.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - None;&lt;br&gt;1 - Normal password.|
|status|integer|Whether the media type is enabled.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* Enabled;&lt;br&gt;1 - Disabled.|
|username|string|User name.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `type` is set to "Email"|
|maxsessions|integer|The maximum number of alerts that can be processed in parallel.&lt;br&gt;&lt;br&gt;Possible values if `type` is set to "SMS": *(default)* 1.&lt;br&gt;&lt;br&gt;Possible values if `type` is set to "Email", "Script", or "Webhook": 0-100.|
|maxattempts|integer|The maximum number of attempts to send an alert.&lt;br&gt;&lt;br&gt;Possible values: 1-100.&lt;br&gt;&lt;br&gt;Default value: 3.|
|attempt\_interval|string|The interval between retry attempts. Accepts seconds and time unit with suffix.&lt;br&gt;&lt;br&gt;Possible values: 0-1h.&lt;br&gt;&lt;br&gt;Default value: 10s.|
|content\_type|integer|Message format.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - plain text;&lt;br&gt;1 - *(default)* html.|
|script|string|Media type webhook script javascript body.|
|timeout|string|Media type webhook script timeout.&lt;br&gt;Accepts seconds and time unit with suffix.&lt;br&gt;&lt;br&gt;Possible values: 1-60s.&lt;br&gt;&lt;br&gt;Default: 30s.|
|process\_tags|integer|Defines should the webhook script response to be interpreted as tags and these tags should be added to associated event.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* Ignore webhook script response;&lt;br&gt;1 - Process webhook script response as tags.|
|show\_event\_menu|integer|Show media type entry in `problem.get` and `event.get` property `urls`.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* Do not add `urls` entry;&lt;br&gt;1 - Add media type to `urls` property.|
|event\_menu\_url|string|Define `url` property of media type entry in `urls` property of `problem.get` and `event.get`.|
|event\_menu\_name|string|Define `name` property of media type entry in `urls` property of `problem.get` and `event.get`.|
|parameters|array|Array of [webhook](#webhook-parameters) or [script](#script-parameters) input parameters.|
|description|string|Media type description.|</source>
      </trans-unit>
      <trans-unit id="46e1e59a" xml:space="preserve">
        <source>### Webhook parameters

Parameters passed to a webhook script when it is being called have the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|name|string|Parameter name.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|
|value|string|Parameter value, supports macros.&lt;br&gt;Supported macros are described on the [Supported macros](/manual/appendix/macros/supported_by_location) page.|</source>
      </trans-unit>
      <trans-unit id="parameters" xml:space="preserve">
        <source>### Script parameters

Parameters passed to a script when it is being called have the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|**sortorder**&lt;br&gt;(required)|integer|The order in which the parameters will be passed to the script as command-line arguments. Starting with 0 as the first one.|
|value|string|Parameter value, supports macros.&lt;br&gt;Supported macros are described on the [Supported macros](/manual/appendix/macros/supported_by_location) page.|</source>
      </trans-unit>
      <trans-unit id="9c81491f" xml:space="preserve">
        <source>### Message template

The message template object defines a template that will be used as a
default message for action operations to send a notification. It has the
following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|eventsource|integer|Event source.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - triggers;&lt;br&gt;1 - discovery;&lt;br&gt;2 - autoregistration;&lt;br&gt;3 - internal;&lt;br&gt;4 - services.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|
|recovery|integer|Operation mode.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - operations;&lt;br&gt;1 - recovery operations;&lt;br&gt;2 - update operations.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|
|subject|string|Message subject.|
|message|string|Message text.|</source>
      </trans-unit>
    </body>
  </file>
</xliff>
