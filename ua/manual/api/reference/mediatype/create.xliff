<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="ua" datatype="plaintext" original="manual/api/reference/mediatype/create.md">
    <body>
      <trans-unit id="fb5f6958" xml:space="preserve">
        <source># mediatype.create</source>
      </trans-unit>
      <trans-unit id="34dd1ecc" xml:space="preserve">
        <source>### Description

`object mediatype.create(object/array mediaTypes)`

This method allows to create new media types.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::</source>
      </trans-unit>
      <trans-unit id="dbb8bb8d" xml:space="preserve">
        <source>### Parameters

`(object/array)` Media types to create.

Additionally to the [standard media type properties](object#media_type),
the method accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|parameters|array|[Script](object#script_parameters) or [webhook](object#webhook_parameters) parameters to be created for the media type.|
|message\_templates|array|[Message templates](object#message_template) to be created for the media type.|</source>
      </trans-unit>
      <trans-unit id="b1eed639" xml:space="preserve">
        <source>### Return values

`(object)` Returns an object containing the IDs of the created media
types under the `mediatypeids` property. The order of the returned IDs
matches the order of the passed media types.</source>
      </trans-unit>
      <trans-unit id="b41637d2" xml:space="preserve">
        <source>### Examples</source>
      </trans-unit>
      <trans-unit id="e61703a8" xml:space="preserve">
        <source>#### Creating an email media type

Create a new email media type with a custom SMTP port and message
templates.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "mediatype.create",
    "params": {
        "type": "0",
        "name": "Email",
        "smtp_server": "mail.example.com",
        "smtp_helo": "example.com",
        "smtp_email": "zabbix@example.com",
        "smtp_port": "587",
        "content_type": "1",
        "message_templates": [
            {
                "eventsource": "0",
                "recovery": "0",
                "subject": "Problem: {EVENT.NAME}",
                "message": "Problem \"{EVENT.NAME}\" on host \"{HOST.NAME}\" started at {EVENT.TIME}."
            },
            {
                "eventsource": "0",
                "recovery": "1",
                "subject": "Resolved in {EVENT.DURATION}: {EVENT.NAME}",
                "message": "Problem \"{EVENT.NAME}\" on host \"{HOST.NAME}\" has been resolved at {EVENT.RECOVERY.TIME} on {EVENT.RECOVERY.DATE}."
            },
            {
                "eventsource": "0",
                "recovery": "2",
                "subject": "Updated problem in {EVENT.AGE}: {EVENT.NAME}",
                "message": "{USER.FULLNAME} {EVENT.UPDATE.ACTION} problem \"{EVENT.NAME}\" on host \"{HOST.NAME}\" at {EVENT.UPDATE.DATE} {EVENT.UPDATE.TIME}."
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "mediatypeids": [
            "7"
        ]
    },
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="1711e041" xml:space="preserve">
        <source>#### Creating a script media type

Create a new script media type with a custom value for the number of
attempts and the interval between them.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "mediatype.create",
    "params": {
        "type": "1",
        "name": "Push notifications",
        "exec_path": "push-notification.sh",
        "maxattempts": "5",
        "attempt_interval": "11s",
        "parameters": [
            {
                "sortorder": "0",
                "value": "{ALERT.SENDTO}"
            },
            {
                "sortorder": "1",
                "value": "{ALERT.SUBJECT}"
            },
            {
                "sortorder": "2",
                "value": "{ALERT.MESSAGE}"
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "mediatypeids": [
            "8"
        ]
    },
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="9932dd76" xml:space="preserve">
        <source>#### Creating a webhook media type

Create a new webhook media type.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "mediatype.create",
    "params": {
        "type": "4",
        "name": "Webhook",
        "script": "var Webhook = {\r\n    token: null,\r\n    to: null,\r\n    subject: null,\r\n    message: null,\r\n\r\n    sendMessage: function() {\r\n        // some logic\r\n    }\r\n}",
        "parameters": [
            {
                "name": "Message",
                "value": "{ALERT.MESSAGE}"
            },
            {
                "name": "Subject",
                "value": "{ALERT.SUBJECT}"
            },
            {
                "name": "To",
                "value": "{ALERT.SENDTO}"
            },
            {
                "name": "Token",
                "value": "&lt;Token&gt;"
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "mediatypeids": [
            "9"
        ]
    },
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="06979650" xml:space="preserve">
        <source>### Source

CMediaType::create() in
*ui/include/classes/api/services/CMediaType.php*.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
