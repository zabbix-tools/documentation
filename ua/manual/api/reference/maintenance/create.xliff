<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="ua" datatype="plaintext" original="manual/api/reference/maintenance/create.md">
    <body>
      <trans-unit id="f47b150e" xml:space="preserve">
        <source># maintenance.create</source>
      </trans-unit>
      <trans-unit id="e8498ebc" xml:space="preserve">
        <source>### Description

`object maintenance.create(object/array maintenances)`

This method allows to create new maintenances.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::</source>
      </trans-unit>
      <trans-unit id="6c14daac" xml:space="preserve">
        <source>### Parameters

`(object/array)` Maintenances to create.

Additionally to the [standard maintenance
properties](object#maintenance), the method accepts the following
parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|groups|object/array|Host [groups](/manual/api/reference/hostgroup/object) that will undergo maintenance.&lt;br&gt;&lt;br&gt;The host groups must have the `groupid` property defined.&lt;br&gt;&lt;br&gt;[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):&lt;br&gt;- *required* if `hosts` is not set|
|hosts|object/array|[Hosts](/manual/api/reference/host/object) that will undergo maintenance.&lt;br&gt;&lt;br&gt;The hosts must have only the `hostid` property defined.&lt;br&gt;&lt;br&gt;[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):&lt;br&gt;- *required* if `groups` is not set|
|timeperiods|object/array|Maintenance [time periods](/manual/api/reference/maintenance/object#time_period).&lt;br&gt;&lt;br&gt;[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):&lt;br&gt;- *required*|
|tags|object/array|[Problem tags](/manual/api/reference/maintenance/object#problem_tag).&lt;br&gt;&lt;br&gt;Define what problems must be suppressed.&lt;br&gt;If no tags are given, all active maintenance host problems will be suppressed.&lt;br&gt;&lt;br&gt;[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):&lt;br&gt;- *supported* if `maintenance_type` of [Maintenance object](object#maintenance) is set to "with data collection"|
|groupids&lt;br&gt;(deprecated)|array|This parameter is deprecated, please use `groups` instead.&lt;br&gt;IDs of the host groups that will undergo maintenance.|
|hostids&lt;br&gt;(deprecated)|array|This parameter is deprecated, please use `hosts` instead.&lt;br&gt;IDs of the hosts that will undergo maintenance.|</source>
      </trans-unit>
      <trans-unit id="48c4869c" xml:space="preserve">
        <source>### Return values

`(object)` Returns an object containing the IDs of the created
maintenances under the `maintenanceids` property. The order of the
returned IDs matches the order of the passed maintenances.</source>
      </trans-unit>
      <trans-unit id="b41637d2" xml:space="preserve">
        <source>### Examples</source>
      </trans-unit>
      <trans-unit id="bb812c67" xml:space="preserve">
        <source>#### Creating a maintenance

Create a maintenance with data collection for host group with ID "2" and with
problem tags **service:mysqld** and **error**. It must be active from
22.01.2013 till 22.01.2014, come in effect each Sunday at 18:00 and last
for one hour.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "maintenance.create",
    "params": {
        "name": "Sunday maintenance",
        "active_since": 1358844540,
        "active_till": 1390466940,
        "tags_evaltype": 0,
        "groups": [
            {"groupid": "2"}
        ],
        "timeperiods": [
            {
                "period": 3600,
                "timeperiod_type": 3,
                "start_time": 64800,
                "every": 1,
                "dayofweek": 64
            }
        ],
        "tags": [
            {
                "tag": "service",
                "operator": "0",
                "value": "mysqld"
            },
            {
                "tag": "error",
                "operator": "2",
                "value": ""
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "maintenanceids": [
            "3"
        ]
    },
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="82b3ef0e" xml:space="preserve">
        <source>### See also

-   [Time period](object#time_period)</source>
      </trans-unit>
      <trans-unit id="3e3a556f" xml:space="preserve">
        <source>### Source

CMaintenance::create() in
*ui/include/classes/api/services/CMaintenance.php*.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
