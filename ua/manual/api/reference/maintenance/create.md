[comment]: # translation:outdated

[comment]: # ({new-f47b150e})
# maintenance.create

[comment]: # ({/new-f47b150e})

[comment]: # ({new-e8498ebc})
### Description

`object maintenance.create(object/array maintenances)`

This method allows to create new maintenances.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-e8498ebc})

[comment]: # ({new-6c14daac})
### Parameters

`(object/array)` Maintenances to create.

Additionally to the [standard maintenance
properties](object#maintenance), the method accepts the following
parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|groups|object/array|Host [groups](/manual/api/reference/hostgroup/object) that will undergo maintenance.<br><br>The host groups must have the `groupid` property defined.<br><br>At least one object of `groups` or `hosts` must be specified.|
|hosts|object/array|[Hosts](/manual/api/reference/host/object) that will undergo maintenance.<br><br>The hosts must have the `hostid` property defined.<br><br>At least one object of `groups` or `hosts` must be specified.|
|**timeperiods**<br>(required)|object/array|Maintenance [time periods](/manual/api/reference/maintenance/object#time_period).|
|tags|object/array|[Problem tags](/manual/api/reference/maintenance/object#problem_tag).<br><br>Define what problems must be suppressed.<br>If no tags are given, all active maintenance host problems will be suppressed.|

[comment]: # ({/new-6c14daac})

[comment]: # ({new-48c4869c})
### Return values

`(object)` Returns an object containing the IDs of the created
maintenances under the `maintenanceids` property. The order of the
returned IDs matches the order of the passed maintenances.

[comment]: # ({/new-48c4869c})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-bb812c67})
#### Creating a maintenance

Create a maintenance with data collection for host group with ID "2" and with
problem tags **service:mysqld** and **error**. It must be active from
22.01.2013 till 22.01.2014, come in effect each Sunday at 18:00 and last
for one hour.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "maintenance.create",
    "params": {
        "name": "Sunday maintenance",
        "active_since": 1358844540,
        "active_till": 1390466940,
        "tags_evaltype": 0,
        "groups": [
            {"groupid": "2"}
        ],
        "timeperiods": [
            {
                "period": 3600,
                "timeperiod_type": 3,
                "start_time": 64800,
                "every": 1,
                "dayofweek": 64
            }
        ],
        "tags": [
            {
                "tag": "service",
                "operator": "0",
                "value": "mysqld"
            },
            {
                "tag": "error",
                "operator": "2",
                "value": ""
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "maintenanceids": [
            "3"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-bb812c67})

[comment]: # ({new-82b3ef0e})
### See also

-   [Time period](object#time_period)

[comment]: # ({/new-82b3ef0e})

[comment]: # ({new-3e3a556f})
### Source

CMaintenance::create() in
*ui/include/classes/api/services/CMaintenance.php*.

[comment]: # ({/new-3e3a556f})
