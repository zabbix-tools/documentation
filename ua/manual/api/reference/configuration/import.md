[comment]: # translation:outdated

[comment]: # ({new-1359fe15})
# configuration.import

[comment]: # ({/new-1359fe15})

[comment]: # ({new-fe885a4a})
### Description

`boolean configuration.import(object parameters)`

This method allows to import configuration data from a serialized
string.

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-fe885a4a})

[comment]: # ({new-147fd272})
### Parameters

`(object)` Parameters containing the data to import and rules how the
data should be handled.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|**format**<br>(required)|string|Format of the serialized string.<br><br>Possible values:<br>`yaml` - YAML;<br>`xml` - XML;<br>`json` - JSON.|
|**source**<br>(required)|string|Serialized string containing the configuration data.|
|**rules**<br>(required)|object|Rules on how new and existing objects should be imported.<br><br>The `rules` parameter is described in detail in the table below.|

::: notetip
If no rules are given, the configuration will not be
updated.
:::

The `rules` object supports the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|discoveryRules|object|Rules on how to import LLD rules.<br><br>Supported parameters:<br>`createMissing` - `(boolean)` if set to `true`, new LLD rules will be created; default: `false`;<br>`updateExisting` - `(boolean)` if set to `true`, existing LLD rules will be updated; default: `false`;<br>`deleteMissing` - `(boolean)` if set to `true`, LLD rules not present in the imported data will be deleted from the database; default: `false`.|
|graphs|object|Rules on how to import graphs.<br><br>Supported parameters:<br>`createMissing` - `(boolean)` if set to `true`, new graphs will be created; default: `false`;<br>`updateExisting` - `(boolean)` if set to `true`, existing graphs will be updated; default: `false`;<br>`deleteMissing` - `(boolean)` if set to `true`, graphs not present in the imported data will be deleted from the database; default: `false`.|
|groups|object|Rules on how to import host groups.<br><br>Supported parameters:<br>`createMissing` - `(boolean)` if set to `true`, new host groups will be created; default: `false`;<br>`updateExisting` - `(boolean)` if set to `true`, existing host groups will be updated; default: `false`.|
|hosts|object|Rules on how to import hosts.<br><br>Supported parameters:<br>`createMissing` - `(boolean)` if set to `true`, new hosts will be created; default: `false`;<br>`updateExisting` - `(boolean)` if set to `true`, existing hosts will be updated; default: `false`.|
|httptests|object|Rules on how to import web scenarios.<br><br>Supported parameters:<br>`createMissing` - `(boolean)` if set to `true`, new web scenarios will be created; default: `false`;<br>`updateExisting` - `(boolean)` if set to `true`, existing web scenarios will be updated; default: `false`;<br>`deleteMissing` - `(boolean)` if set to `true`, web scenarios not present in the imported data will be deleted from the database; default: `false`.|
|images|object|Rules on how to import images.<br><br>Supported parameters:<br>`createMissing` - `(boolean)` if set to `true`, new images will be created; default: `false`;<br>`updateExisting` - `(boolean)` if set to `true`, existing images will be updated; default: `false`.|
|items|object|Rules on how to import items.<br><br>Supported parameters:<br>`createMissing` - `(boolean)` if set to `true`, new items will be created; default: `false`;<br>`updateExisting` - `(boolean)` if set to `true`, existing items will be updated; default: `false`;<br>`deleteMissing` - `(boolean)` if set to `true`, items not present in the imported data will be deleted from the database; default: `false`.|
|maps|object|Rules on how to import maps.<br><br>Supported parameters:<br>`createMissing` - `(boolean)` if set to `true`, new maps will be created; default: `false`;<br>`updateExisting` - `(boolean)` if set to `true`, existing maps will be updated; default: `false`.|
|mediaTypes|object|Rules on how to import media types.<br><br>Supported parameters:<br>`createMissing` - `(boolean)` if set to `true`, new media types will be created; default: `false`;<br>`updateExisting` - `(boolean)` if set to `true`, existing media types will be updated; default: `false`.|
|templateLinkage|object|Rules on how to import template links.<br><br>Supported parameters:<br>`createMissing` - `(boolean)` if set to `true`, new links between templates and host will be created; default: `false`;<br>`deleteMissing` - `(boolean)` if set to `true`, template links not present in the imported data will be deleted from the database; default: `false`.|
|templates|object|Rules on how to import templates.<br><br>Supported parameters:<br>`createMissing` - `(boolean)` if set to `true`, new templates will be created; default: `false`;<br>`updateExisting` - `(boolean)` if set to `true`, existing templates will be updated; default: `false`.|
|templateDashboards|object|Rules on how to import template dashboards.<br><br>Supported parameters:<br>`createMissing` - `(boolean)` if set to `true`, new template dashboards will be created; default: `false`;<br>`updateExisting` - `(boolean)` if set to `true`, existing template dashboards will be updated; default: `false`;<br>`deleteMissing` - `(boolean)` if set to `true`, template dashboards not present in the imported data will be deleted from the database; default: `false`.|
|triggers|object|Rules on how to import triggers.<br><br>Supported parameters:<br>`createMissing` - `(boolean)` if set to `true`, new triggers will be created; default: `false`;<br>`updateExisting` - `(boolean)` if set to `true`, existing triggers will be updated; default: `false`;<br>`deleteMissing` - `(boolean)` if set to `true`, triggers not present in the imported data will be deleted from the database; default: `false`.|
|valueMaps|object|Rules on how to import host or template value maps.<br><br>Supported parameters:<br>`createMissing` - `(boolean)` if set to `true`, new value maps will be created; default: `false`;<br>`updateExisting` - `(boolean)` if set to `true`, existing value maps will be updated; default: `false`;<br>`deleteMissing` - `(boolean)` if set to `true`, value maps not present in the imported data will be deleted from the database; default: `false`.|

[comment]: # ({/new-147fd272})

[comment]: # ({new-08d02880})
### Return values

`(boolean)` Returns `true` if importing has been successful.

[comment]: # ({/new-08d02880})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-0801380e})
#### Importing hosts and items

Import the host and items contained in the XML string. If any items in
XML are missing, they will be deleted from the database, and everything
else will be left unchanged.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "configuration.import",
    "params": {
        "format": "xml",
        "rules": {
            "valueMaps": {
                "createMissing": true,
                "updateExisting": false
            },
            "hosts": {
                "createMissing": true,
                "updateExisting": true
            },
            "items": {
                "createMissing": true,
                "updateExisting": true,
                "deleteMissing": true
            }
        },
        "source": "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<zabbix_export><version>5.4</version><date>2020-03-13T15:31:45Z</date><groups><group><uuid>6f6799aa69e844b4b3918f779f2abf08</uuid><name>Zabbix servers</name></group></groups><hosts><host><host>Export host</host><name>Export host</name><groups><group><name>Zabbix servers</name></group></groups><interfaces><interface><interface_ref>if1</interface_ref></interface></interfaces><items><item><name>Item</name><key>item.key</key><delay>30s</delay><valuemap><name>Host status</name></valuemap><interface_ref>if1</interface_ref><request_method>POST</request_method></item></items><valuemaps><valuemap><name>Host status</name><mappings><mapping><value>0</value><newvalue>Up</newvalue></mapping><mapping><value>2</value><newvalue>Unreachable</newvalue></mapping></mappings></valuemap></valuemaps></host></hosts></zabbix_export>"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": true,
    "id": 1
}
```

[comment]: # ({/new-0801380e})

[comment]: # ({new-c5744b74})
### Source

CConfiguration::import() in
*ui/include/classes/api/services/CConfiguration.php*.

[comment]: # ({/new-c5744b74})
