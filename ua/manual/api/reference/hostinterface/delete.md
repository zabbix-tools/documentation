[comment]: # translation:outdated

[comment]: # ({new-fc20d2a9})
# hostinterface.delete

[comment]: # ({/new-fc20d2a9})

[comment]: # ({new-6a90d1d2})
### Description

`object hostinterface.delete(array hostInterfaceIds)`

This method allows to delete host interfaces.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-6a90d1d2})

[comment]: # ({new-617ba57c})
### Parameters

`(array)` IDs of the host interfaces to delete.

[comment]: # ({/new-617ba57c})

[comment]: # ({new-61a8b976})
### Return values

`(object)` Returns an object containing the IDs of the deleted host
interfaces under the `interfaceids` property.

[comment]: # ({/new-61a8b976})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-91934c15})
#### Delete a host interface

Delete the host interface with ID 30062.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "hostinterface.delete",
    "params": [
        "30062"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "interfaceids": [
            "30062"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-91934c15})

[comment]: # ({new-a3d1e725})
### See also

-   [hostinterface.massremove](massremove)
-   [host.massremove](/manual/api/reference/host/massremove)

[comment]: # ({/new-a3d1e725})

[comment]: # ({new-cf54c99c})
### Source

CHostInterface::delete() in
*ui/include/classes/api/services/CHostInterface.php*.

[comment]: # ({/new-cf54c99c})
