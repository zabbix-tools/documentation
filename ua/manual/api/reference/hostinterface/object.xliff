<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="ua" datatype="plaintext" original="manual/api/reference/hostinterface/object.md">
    <body>
      <trans-unit id="7ed888d2" xml:space="preserve">
        <source># &gt; Host interface object

The following objects are directly related to the `hostinterface` API.</source>
      </trans-unit>
      <trans-unit id="ce77a3b0" xml:space="preserve">
        <source>### Host interface

The host interface object has the following properties.

::: noteimportant
Note that both `ip` and `dns` properties are *required* for create operations.
If you do not want to use DNS, set it to an empty string.
:::

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|interfaceid|string|ID of the interface.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *read-only*&lt;br&gt;- *required* for update operations|
|available|integer|Availability of host interface.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* unknown;&lt;br&gt;1 - available;&lt;br&gt;2 - unavailable.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *read-only*|
|hostid|string|ID of the host that the interface belongs to.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *constant*&lt;br&gt;- *required* for create operations|
|type|integer|Interface type.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;1 - Agent;&lt;br&gt;2 - SNMP;&lt;br&gt;3 - IPMI;&lt;br&gt;4 - JMX.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* for create operations|
|ip|string|IP address used by the interface.&lt;br&gt;&lt;br&gt;Can be empty if the connection is made via DNS.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* for create operations|
|dns|string|DNS name used by the interface.&lt;br&gt;&lt;br&gt;Can be empty if the connection is made via IP.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* for create operations|
|port|string|Port number used by the interface.&lt;br&gt;Can contain user macros.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* for create operations|
|useip|integer|Whether the connection should be made via IP.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - connect using host DNS name;&lt;br&gt;1 - connect using host IP address.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* for create operations|
|main|integer|Whether the interface is used as default on the host. Only one interface of some type can be set as default on a host.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - not default;&lt;br&gt;1 - default.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* for create operations|
|details|array|Additional [details](#details) object for interface.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* if `type` is set to "SNMP"|
|disable\_until|timestamp|The next polling time of an unavailable host interface.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *read-only*|
|error|string|Error text if host interface is unavailable.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *read-only*|
|errors\_from|timestamp|Time when host interface became unavailable.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *read-only*|</source>
      </trans-unit>
      <trans-unit id="3bb71fcc" xml:space="preserve">
        <source>### Details

The details object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|version|integer|SNMP interface version.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;1 - SNMPv1;&lt;br&gt;2 - SNMPv2c;&lt;br&gt;3 - SNMPv3.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|
|bulk|integer|Whether to use bulk SNMP requests.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - don't use bulk requests;&lt;br&gt;1 - (default) - use bulk requests.|
|community|string|SNMP community. Used only by SNMPv1 and SNMPv2 interfaces.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* if `version` is set to "SNMPv1" or "SNMPv2c"|
|securityname|string|SNMPv3 security name. Used only by SNMPv3 interfaces.|
|securitylevel|integer|SNMPv3 security level. Used only by SNMPv3 interfaces.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - (default) - noAuthNoPriv;&lt;br&gt;1 - authNoPriv;&lt;br&gt;2 - authPriv.|
|authpassphrase|string|SNMPv3 authentication passphrase. Used only by SNMPv3 interfaces.|
|privpassphrase|string|SNMPv3 privacy passphrase. Used only by SNMPv3 interfaces.|
|authprotocol|integer|SNMPv3 authentication protocol. Used only by SNMPv3 interfaces.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - (default) - MD5;&lt;br&gt;1 - SHA1;&lt;br&gt;2 - SHA224;&lt;br&gt;3 - SHA256;&lt;br&gt;4 - SHA384;&lt;br&gt;5 - SHA512.|
|privprotocol|integer|SNMPv3 privacy protocol. Used only by SNMPv3 interfaces.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - (default) - DES;&lt;br&gt;1 - AES128;&lt;br&gt;2 - AES192;&lt;br&gt;3 - AES256;&lt;br&gt;4 - AES192C;&lt;br&gt;5 - AES256C.|
|contextname|string|SNMPv3 context name. Used only by SNMPv3 interfaces.|</source>
      </trans-unit>
    </body>
  </file>
</xliff>
