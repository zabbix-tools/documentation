[comment]: # ({new-449ef1ba})
# Appendix 2. Changes from 6.4 to 7.0

[comment]: # ({/new-449ef1ba})

[comment]: # translation:outdated


[comment]: # ({new-31b2151e})
### Backward incompatible changes

[comment]: # ({/new-31b2151e})

[comment]: # ({new-95751b30})
#### dashboard
[ZBXNEXT-2299](https://support.zabbix.com/browse/ZBXNEXT-2299) Replaced [dashboard problem widget field](/manual/api/reference/dashboard/widget_fields/problems#parameters) `unacknowledged` with two new fields `acknowledgement_status` and `acknowledged_by_me`.\

[comment]: # ({/new-95751b30})

[comment]: # ({new-11e46ab0})
#### dashboard

[ZBXNEXT-8245](https://support.zabbix.com/browse/ZBXNEXT-8245) Removed possible value combinations of the [dashboard widget field](/manual/api/reference/dashboard/object#dashboard-widget-field) object properties for widget types [`clock`](/manual/api/reference/dashboard/widget_fields/clock) and [`item`](/manual/api/reference/dashboard/widget_fields/item_value): `"type": 0, "name": "adv_conf", "value": <0 - Disabled; 1 - Enabled>`.\

[comment]: # ({/new-11e46ab0})













[comment]: # ({new-5b2db1cc})
### Other changes and bug fixes

[comment]: # ({/new-5b2db1cc})

[comment]: # ({new-52e7c953})
#### dcheck

[ZBXNEXT-8079](https://support.zabbix.com/browse/ZBXNEXT-8079) Added new property `allow_redirect`.\

[comment]: # ({/new-52e7c953})

[comment]: # ({new-47d4e279})
##### event

[ZBXNEXT-2299](https://support.zabbix.com/browse/ZBXNEXT-2299) `event.get`: Added new filtering options `action` and `action_userid`.\

[comment]: # ({/new-47d4e279})

[comment]: # ({new-18338e51})
##### problem

[ZBXNEXT-2299](https://support.zabbix.com/browse/ZBXNEXT-2299) `problem.get`: Added new filtering options `action` and `action_userid`.\

[comment]: # ({/new-18338e51})

[comment]: # ({new-5ef3e354})
##### templatedashboard

[ZBXNEXT-8086](https://support.zabbix.com/browse/ZBXNEXT-8079) Added new [template dashboard widget](/manual/api/reference/templatedashboard/object#template-dashboard-widget) types (actionlog, dataover, discovery, favgraphs, favmaps, hostavail, map, navtree, problemhosts, problems, problemsbysv, slareport, svggraph, systeminfo, tophosts, trigover, web).\
[ZBXNEXT-8086](https://support.zabbix.com/browse/ZBXNEXT-8079) Added new [template dashboard widget field](/manual/api/reference/templatedashboard/object#template-dashboard-widget-field) types (8 - Map, 9 - Service, 10 - SLA, 11 - User, 12 - Action, 13 - Media type).\

[comment]: # ({/new-5ef3e354})

[comment]: # ({new-ab579aa8})
##### drule

[ZBXNEXT-2732](https://support.zabbix.com/browse/ZBXNEXT-2732) Added new property `concurrency_max`.\

[comment]: # ({/new-ab579aa8})

















