<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="ua" datatype="plaintext" original="manual/guides/monitor_windows.md">
    <body>
      <trans-unit id="3fc5b143" xml:space="preserve">
        <source># 2. Monitor Windows with Zabbix agent</source>
      </trans-unit>
      <trans-unit id="fd7b4328" xml:space="preserve">
        <source>## Introduction

This page walks you through the steps required to start basic monitoring of Windows machines with Zabbix.

**Who this guide is for**

This guide is designed for new Zabbix users and contains the minimum set 
of steps required to enable basic monitoring of your Windows machine. 
If you are looking for deep customization options or require more advanced 
configuration, see  [Configuration](/manual/config) section of Zabbix 
manual.  

**Prerequisites**

Before proceeding with this installation guide, you must [download and install](https://www.zabbix.com/download) Zabbix 
server and Zabbix frontend according to instructions for your OS.</source>
      </trans-unit>
      <trans-unit id="7cec4ce2" xml:space="preserve">
        <source>
## Install Zabbix agent

Zabbix agent is the process responsible for gathering data. You need to install it on the Windows machine that you want 
to monitor. Follow Zabbix agent installation instructions for [Windows](/installation/install_from_packages/win_msi).

![](../../../assets/en/manual/installation/install_from_packages/msi0_b.png)</source>
      </trans-unit>
      <trans-unit id="c5ab18d1" xml:space="preserve">
        <source>
## Configure Zabbix for monitoring

Zabbix agent can collect metrics in active or passive mode (simultaneously). 

::: noteclassic
A passive check is a simple data request. Zabbix server or proxy asks for some data (for example, CPU load) and Zabbix 
agent sends back the result to the server. Active checks require more complex processing. The agent must first retrieve 
from the server(s) a list of items for independent processing and then bulk send the data back. See 
[Passive and active agent checks](/manual/appendix/items/activepassive) for more info.
:::

Monitoring templates provided by Zabbix usually offer two alternatives - a template for Zabbix agent and a template for 
Zabbix agent (active). With the first option, the agent will collect metrics in passive mode. Such templates will deliver 
identical monitoring results, but using different communication protocols. 

Further Zabbix configuration depends on whether you select a template for [active](#active-checks) or 
[passive](#passive-checks) Zabbix agent checks.</source>
      </trans-unit>
      <trans-unit id="76c0a12c" xml:space="preserve">
        <source>### Passive checks</source>
      </trans-unit>
      <trans-unit id="854b1e0b" xml:space="preserve">
        <source>
#### Zabbix frontend

1\. Log in to Zabbix frontend.

2\. [Create a host](/manual/config/hosts/host) in Zabbix web interface. 

This host will represent your Windows machine. 

3\. In the *Interfaces* parameter, add *Agent* interface and specify the IP 
address or DNS name of the Windows machine where the agent is installed. 

4\. In the *Templates* parameter, type or select *Windows by Zabbix agent*. 

![](../../../assets/en/manual/guides/win_host_passive.png){width="600"}</source>
      </trans-unit>
      <trans-unit id="17dcee1f" xml:space="preserve">
        <source>#### Zabbix agent

For passive checks Zabbix agent needs to know the IP address or DNS name of Zabbix server. If you 
have provided correct information during the agent installation, the configuration file is already updated. Otherwise, you 
need to manually specify it. Go to the `C:\Program files\Zabbix Agent` folder, open the file 
*zabbix_agent.conf* and add the IP/DNS of your Zabbix server to the *Server* parameter.

Example:

    Server=192.0.2.22</source>
      </trans-unit>
      <trans-unit id="287beca2" xml:space="preserve">
        <source>### Active checks</source>
      </trans-unit>
      <trans-unit id="9d58b1d3" xml:space="preserve">
        <source>#### Zabbix frontend

1\. Log in to Zabbix frontend.

2\. [Create a host](/manual/config/hosts/host) in Zabbix web interface. 

This host will represent your Windows machine. 

3\. In the *Templates* parameter, type or select  *Windows by Zabbix agent active*.

![](../../../assets/en/manual/guides/win_host_active.png){width="600"}</source>
      </trans-unit>
      <trans-unit id="ec395430" xml:space="preserve">
        <source>#### Zabbix agent

In the `C:\Program files\Zabbix Agent` folder open the file *zabbix_agent.conf* and add:

- The name of the host you created in Zabbix web interface to the *Hostname* parameter.
- The IP address or DNS name of your Zabbix server machine to the *ServerActive* parameter (might be prefilled if you have provided it during Zabbix agent setup).

Example:
  
    ServerActive= 192.0.2.22
    Hostname=Windows workstation</source>
      </trans-unit>
      <trans-unit id="2bb9a9d4" xml:space="preserve">
        <source>## View collected metrics

Congratulations! At this point, Zabbix is already monitoring your Windows machine. 

To view collected metrics, open the *Monitoring-&gt;Hosts* 
[menu section](/manual/web_interface/frontend_sections/monitoring/hosts) and click on 
the *Latest data* next to the host. 

![](../../../assets/en/manual/guides/win_latest_data.png){width="600"}</source>
      </trans-unit>
      <trans-unit id="eaf2b29b" xml:space="preserve">
        <source>## Set up problem alerts

Zabbix can notify you about a problem with your infrastructure using a variety of methods. This guide provides 
configuration steps for sending email alerts. 

1\. Go to the *User settings -&gt; Profile*, switch to the tab *Media* and [add your email](/manual/quickstart/login#adding-user).

![](../../../assets/en/manual/quickstart/new_media.png){width="600"}

2\. Follow the guide for [Receiving problem notification](/manual/quickstart/notification).

Next time, when Zabbix detects a problem you should receive an alert via email.

:::notetip
On Windows, you can use [CpuStres](https://docs.microsoft.com/en-us/sysinternals/downloads/cpustres) utility to simulate 
high CPU load and as a result receive a problem alert. 
:::

**See also:**

- [Creating an item](/manual/config/items/item) - how to start monitoring additional metrics (custom monitoring without templates).
- [Zabbix agent items](/manual/config/items/itemtypes/zabbix_agent), [Zabbix agent items for Windows](/manual/config/items/itemtypes/zabbix_agent/win_keys) - full list of metrics you can monitor using Zabbix agent on Windows.
- [Problem escalations](/manual/config/notifications/action/escalations) - how to create multi-step alert scenarios (e.g., first send message to the system administrator, then, if a problem is not resolved in 45 minutes, send message to the data center manager).</source>
      </trans-unit>
    </body>
  </file>
</xliff>
