[comment]: # translation:outdated

[comment]: # ({new-6705152d})
# 2 User roles

[comment]: # ({/new-6705152d})

[comment]: # ({new-58a614ec})
#### Overview

In the *Users → User roles* section you may create user roles. 

User roles allow to create fine-grained permissions based on 
the initially selected user type (*User*, *Admin*, *Super admin*).

Upon selecting a user type, all available permissions for this user 
type are granted (checked by default).

Permissions can only be revoked from the subset that is available for 
the user type; they cannot be extended beyond what is available for the 
user type. 

Checkboxes for unavailable permissions are grayed out; users will not 
be able to access the element even by entering a direct URL to 
this element into the browser.

User roles can be assigned to system users. Each user may have only one role assigned.

[comment]: # ({/new-58a614ec})

[comment]: # ({new-70ecdd62})
::: noteclassic
A new user cannot be created without assigning a user role to them in 6.4.4 and later Zabbix versions.
Previously created users may still be edited without assigning a user role to them, but when a user role is assigned, the field becomes mandatory, and a user role cannot be deleted anymore. You can always change the user role, though.
:::

[comment]: # ({/new-70ecdd62})

[comment]: # ({new-61e00095})
#### Default user roles

By default, Zabbix is configured with four user roles, which have a
pre-defined set of permissions:

-   Guest role
-   User role
-   Admin role
-   Super admin role

![](../../../../../assets/en/manual/web_interface/user_roles.png){width="600"}

These are based on the main user types in Zabbix. The *Guest role* is a 
user-type role with the only permissions to view some frontend sections.

::: noteclassic
The default *Super admin role* cannot be modified or deleted,
because at least one Super admin user with unlimited privileges must
exist in Zabbix. Users of type *Super admin* can modify settings 
of their own role, but not the user type. 
:::

[comment]: # ({/new-61e00095})

[comment]: # ({new-61ab39c3})

#### Configuration

To create a new role, click on the *Create user role* button at the top
right corner. To update an existing role, click on the role name to open
the configuration form.

![](../../../../../assets/en/manual/web_interface/user_role.png)

Available permissions are displayed. To revoke a certain permission, 
unmark its checkbox. 

Available permissions along with the defaults for each pre-configured 
user role in Zabbix are described below.

[comment]: # ({/new-61ab39c3})

[comment]: # ({new-a18513bb})

#### Default permissions

**Access to UI elements**

The default access to menu sections depends on the user type. See 
the Permissions page for [details](/manual/config/users_and_usergroups/permissions#user_types).

**Access to other options**

| | | | | | | |
|-|----------|-----------------------------|----|----|----|----|
|**Parameter**|<|**Description**|**Default user roles**|<|<|<|
| |<|<|*Super admin role*|*Admin role*|*User role*|*Guest role*|
|*Default access to new UI elements*|<|Enable/disable access to the custom UI elements. Modules, if present, will be listed below.|Yes|Yes|Yes|Yes|
|**Access to services**|<|<|<|<|<|<|
| |Read-write access to services|Select read-write access to services:<br>**None** - no access at all<br>**All** - access to all services is read-write<br>**Service list** - select services for read-write access<br><br>The read-write access, if granted, takes precedence over the read-only access settings and is dynamically inherited by the child services.|All|All|None|None|
|^|Read-write access to services with tag|Specify tag name and, optionally, value to additionally grant read-write access to services matching the tag.<br>This option is available if 'Service list' is selected in the *Read-write access to services* parameter.<br>The read-write access, if granted, takes precedence over the read-only access settings and is dynamically inherited by the child services.|^|^|^|^|
|^|Read-only access to services|Select read-only access to services:<br>**None** - no access at all<br>**All** - access to all services is read-only<br>**Service list** - select services for read-only access<br><br>The read-only access does not take precedence over the read-write access and is dynamically inherited by the child services.|^|^|All|All|
|^|Read-only access to services with tag|Specify tag name and, optionally, value to additionally grant read-only access to services matching the tag.<br>This option is available if 'Service list' is selected in the *Read-only access to services* parameter.<br>The read-only access does not take precedence over the read-write access and is dynamically inherited by the child services.|^|^|^|^|
|**Access to modules**|<|<|<|<|<|<|
| |<Module name>|Allow/deny access to a specific module. Only enabled modules are shown in this section. It is not possible to grant or restrict access to a module that is currently disabled.|Yes|Yes|Yes|Yes|
|^|*Default access to new modules*|Enable/disable access to modules that may be added in the future.|^|^|^|^|
|**Access to API**|<|<|<|<|<|<|
| |*Enabled*|Enable/disable access to API.|Yes|Yes|Yes|No|
|^|*API methods*|Select *Allow list* to allow only specified API methods or *Deny list* to restrict only specified API methods.<br><br>In the search field, start typing the method name, then select the method from the auto-complete list.<br>You can also press the Select button and select methods from the full list available for this user type. Note, that if certain action from the Access to actions block is unchecked, users will not be able to use API methods related to this action.<br><br>Wildcards are supported. Examples: `dashboard.*` (all methods of 'dashboard.' API service) `*` (any method), `*.export` (methods with '.export' name from all API services).<br><br>If no methods have been specified the Allow/Deny list rule will be ignored.|^|^|^|^|
|**Access to actions**|<|<|<|<|<|<|
| |Create and edit dashboards|Clearing this checkbox will also revoke the rights to use `.create`, `.update` and `.delete` API methods for the corresponding elements.|Yes|Yes|Yes|No|
|^|Create and edit maps| |^|^|^|^|
|^|Create and edit maintenance|^|^|^|No|^|
|^|Add problem comments|Clearing this checkbox will also revoke the rights to perform corresponding action via `event.acknowledge` API method.|^|^|Yes|^|
|^|Change severity| |^|^|^|^|
|^|Acknowledge problems|^|^|^|^|^|
|^|Suppress problems|^|^|^|^|^|
|^|Close problems|^|^|^|^|^|
|^|Execute scripts|Clearing this checkbox will also revoke the rights to use the `script.execute` API method.|^|^|^|^|
|^|Manage API tokens|Clearing this checkbox will also revoke the rights to use all `token.` API methods.|^|^|^|^|
|^|Manage scheduled reports|Clearing this checkbox will also revoke the rights to use all `report.` API methods.|^|^|No|^|
|^|Manage SLA|Enable/disable the rights to manage [SLA](/manual/web_interface/frontend_sections/services/sla).|^|^|^|^|
|^|Invoke "Execute now" on read-only hosts|Allow to use the "Execute now" option in latest data for items of read-only hosts.|^|^|Yes|^|
|^|Default access to new actions|Enable/disable access to new actions.|^|^|^|^|

See also:

-   [Configuring a user](/manual/config/users_and_usergroups/user)

[comment]: # ({/new-a18513bb})
