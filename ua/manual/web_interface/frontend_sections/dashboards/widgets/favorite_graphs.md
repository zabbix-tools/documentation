[comment]: # translation:outdated

[comment]: # ({new-1b514b16})
# 5 Favorite graphs

[comment]: # ({/new-1b514b16})

[comment]: # ({new-9ef1ebfc})
#### Overview

This widget contains shortcuts to the most needed graphs, sorted
alphabetically.

The list of shortcuts is populated when you
[view](/manual/web_interface/frontend_sections/monitoring/hosts/graphs)
a graph and then click on its
![](../../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/button_add_fav.png)
*Add to favorites* button.

All configuration parameters are [common](/manual/web_interface/frontend_sections/dashboards/widgets#common-parameters) 
for all widgets.

[comment]: # ({/new-9ef1ebfc})
