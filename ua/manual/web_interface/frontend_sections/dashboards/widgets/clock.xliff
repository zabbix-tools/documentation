<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="ua" datatype="plaintext" original="manual/web_interface/frontend_sections/dashboards/widgets/clock.md">
    <body>
      <trans-unit id="7074b2d8" xml:space="preserve">
        <source># 2 Clock</source>
      </trans-unit>
      <trans-unit id="07a1c7da" xml:space="preserve">
        <source>
#### Overview

In the clock widget, you may display local, server, or specified host
time.

Both analog and digital clocks can be displayed:

![](../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/clock_analog.png)

![](../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/clock_digital.png)</source>
      </trans-unit>
      <trans-unit id="009b8658" xml:space="preserve">
        <source>#### Configuration

To configure, select *Clock* as type:

![](../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/clock.png){width=600}

In addition to the parameters that are [common](/manual/web_interface/frontend_sections/dashboards/widgets#common-parameters) 
for all widgets, you may set the following specific options:

|   |   |
|--|--------|
|*Time type*|Select local, server, or specified host time.&lt;br&gt;Server time will be identical to the time zone set globally or for the Zabbix user.|
|*Item*|Select the item for displaying time. To display host time, use the `system.localtime[local]` [item](/manual/config/items/itemtypes/zabbix_agent). This item must exist on the host.&lt;br&gt;This field is available only when *Host time* is selected.|
|*Clock type*|Select clock type:&lt;br&gt;**Analog** - analog clock&lt;br&gt;**Digital** - digital clock|
|*Show*|Select information units to display in the digital clock (date, time, time zone).&lt;br&gt;This field is available only if "Digital" is selected in the *Clock type* field.|
|*Advanced configuration*|Click on the *Advanced configuration* label to display [advanced configuration](#advanced-configuration) options for the digital clock.&lt;br&gt;This section is available only if "Digital" is selected in the *Clock type* field.|</source>
      </trans-unit>
      <trans-unit id="5e3f567f" xml:space="preserve">
        <source>#### Advanced configuration

Advanced configuration options are available in the collapsible *Advanced configuration* section,
and only for those elements that are selected in the *Show* field (see above).

Additionally, advanced configuration allows to change the background 
color for the whole widget.

![](../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/clock_advanced.png){width=600}

|   |   |
|--|--------|
|*Background color* |Select the background color from the color picker.&lt;br&gt;`D` stands for default color (depends on the frontend theme). To return to the default value, click the *Use default* button in the color picker. |
|_**Date**_ ||
|*Size* |Enter font size height for the date (in percent relative to total widget height). |
|*Bold* |Mark the checkbox to display date in bold type. |
|*Color* |Select the date color from the color picker.&lt;br&gt;`D` stands for default color (depends on the frontend theme). To return to the default value, click the *Use default* button in the color picker. |
|_**Time**_ ||
|*Size* |Enter font size height for the time (in percent relative to total widget height). |
|*Bold* |Mark the checkbox to display time in bold type. |
|*Color* |Select the time color from the color picker.&lt;br&gt;`D` stands for default color (depends on the frontend theme). To return to the default value, click the *Use default* button in the color picker. |
|*Seconds* |Mark the checkbox to display seconds. Otherwise only hours and minutes will be displayed.|
|*Format* |Select to display a 24-hour or 12-hour time. |
|_**Time zone**_ ||
|*Size* |Enter font size height for the time zone (in percent relative to total widget height). |
|*Bold* |Mark the checkbox to display time zone in bold type. |
|*Color* |Select the time zone color from the color picker.&lt;br&gt;`D` stands for default color (depends on the frontend theme). To return to the default value, click the *Use default* button in the color picker. |
|*Time zone*|Select the time zone.|
|*Format* |Select to display time zone in short format (e.g. `New York`) or full format (e.g.`(UTC-04:00) America/New York`). |</source>
      </trans-unit>
    </body>
  </file>
</xliff>
