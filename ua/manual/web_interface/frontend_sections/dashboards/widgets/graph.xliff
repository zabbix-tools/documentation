<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="ua" datatype="plaintext" original="manual/web_interface/frontend_sections/dashboards/widgets/graph.md">
    <body>
      <trans-unit id="167707a1" xml:space="preserve">
        <source># 8 Graph</source>
      </trans-unit>
      <trans-unit id="21bacb3e" xml:space="preserve">
        <source>#### Overview

The graph widget provides a modern and versatile way of visualizing data
collected by Zabbix using a vector image drawing technique. This graph
widget is supported since Zabbix 4.0. Note that the graph widget
supported before Zabbix 4.0 can still be used as [Graph
(classic)](/manual/web_interface/frontend_sections/dashboards/widgets/graph_classic).
See also [*Adding widgets*](/manual/web_interface/frontend_sections/dashboards#adding-widgets)
section on *Dashboards* page for more details.</source>
      </trans-unit>
      <trans-unit id="b1257d27" xml:space="preserve">
        <source>#### Configuration

To configure, select *Graph* as type:

![](../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/graph_dataset.png){width="600"}</source>
      </trans-unit>
      <trans-unit id="d1f705fb" xml:space="preserve">
        <source>#### Data set

The **Data set** tab allows to add data sets and define their visual
representation:

|   |   |   |
|-|----------|----------------------------------------|
|*Data set*|&lt;|Enter the host and item patterns; data of items that match the entered patterns is displayed on the graph; up to 50 items may be displayed. Host pattern and item pattern parameters are mandatory.&lt;br&gt;&lt;br&gt;Wildcard patterns may be used for selection (for example, `*` will return results that match zero or more characters).&lt;br&gt;To specify a wildcard pattern, just enter the string manually and press *Enter*.&lt;br&gt;The wildcard symbol is always interpreted, therefore, it is not possible to add, for example, an item named *item\** individually, if there are other matching items (e.g., item2, item3). See also: [Data set configuration details](#data-set-configuration-details).&lt;br&gt;&lt;br&gt;**Alternatively** to specifying item patterns, you may select a list of items if the data set has been added with the *Item list* option (see the description of the *Add new data set* button).&lt;br&gt;&lt;br&gt;When configuring the widget on a [template dashboard](/manual/config/templates/template#adding-dashboards), the parameter for specifying host patterns is not available, and the parameter for specifying an item list allows to select only [items configured on the template](/manual/config/templates/template#adding-items-triggers-graphs).|
| |*Draw*|Choose the draw type of the metric.&lt;br&gt;Possible draw types: *Line* (set by default), *Points*, *Staircase*, and *Bar*.&lt;br&gt;Note that if there is only one data point in the line/staircase graph, it is drawn as a point regardless of the draw type. The point size is calculated from the line width, but it cannot be smaller than 3 pixels, even if the line width is less.|
|^|*Stacked*|Mark the checkbox to display data as stacked (filled areas displayed).&lt;br&gt;This option is disabled when *Points* draw type is selected.|
|^|*Width*|Set the line width.&lt;br&gt;This option is available when *Line* or *Staircase* draw type is selected.|
|^|*Point size*|Set the point size.&lt;br&gt;This option is available when *Points* draw type is selected.|
|^|*Transparency*|Set the transparency level.|
|^|*Fill*|Set the fill level.&lt;br&gt;This option is available when *Line* or *Staircase* draw type is selected.|
|^|*Missing data*|Select the option for displaying missing data:&lt;br&gt;**None** - the gap is left empty;&lt;br&gt;**Connected** - two border values are connected;&lt;br&gt;**Treat as 0** - the missing data is displayed as 0 values;&lt;br&gt;**Last known** - the missing data is displayed with the same value as the last known value; not applicable for the *Points* and *Bar* draw type.|
|^|*Y-axis*|Select the side of the graph where the Y-axis will be displayed.|
|^|*Time shift*|Specify time shift if required.&lt;br&gt;You may use [time suffixes](/manual/appendix/suffixes#time_suffixes) in this field. Negative values are allowed.|
|^|*Aggregation function*|Specify which aggregation function to use:&lt;br&gt;**min** - display the smallest value;&lt;br&gt;**max** - display the largest value;&lt;br&gt;**avg** - display the average value;&lt;br&gt;**sum** - display the sum of values;&lt;br&gt;**count** - display the count of values;&lt;br&gt;**first** - display the first value;&lt;br&gt;**last** - display the last value;&lt;br&gt;**none** - display all values (no aggregation).&lt;br&gt;&lt;br&gt;Aggregation allows to display an aggregated value for the chosen interval (5 minutes, an hour, a day), instead of all values. See also: [Aggregation in graphs](/manual/config/visualization/graphs/aggregate).|
|^|*Aggregation interval*|Specify the interval for aggregating values.&lt;br&gt;You may use [time suffixes](/manual/appendix/suffixes#time_suffixes) in this field. A numeric value without a suffix will be regarded as seconds.|
|^|*Aggregate*|Specify whether to aggregate:&lt;br&gt;**Each item** - each item in the dataset will be aggregated and displayed separately;&lt;br&gt;**Data set** - all dataset items will be aggregated and displayed as one value.|
|^|*Approximation*|Specify what value to display when more than one value exists per vertical graph pixel:&lt;br&gt;**all** - display the smallest, the largest and the average values;&lt;br&gt;**min** - display the smallest value;&lt;br&gt;**max** - display the largest value;&lt;br&gt;**avg** - display the average value.&lt;br&gt;&lt;br&gt;This setting is useful when displaying a graph for a large time period with frequent update interval (such as one year of values collected every 10 minutes).|
|^|*Data set label*|Specify the data set label that is displayed in graph *Data set* configuration and in graph *Legend* (for aggregated data sets).&lt;br&gt;All data sets are numbered including those with a specified *Data set label*. If no label is specified, the data set will be labeled automatically according to its number (e.g. "Data set #2", "Data set #3", etc.). Data set numbering ir recalculated after reordering/dragging data sets.&lt;br&gt;Data set labels that are too long will be shortened to fit where displayed (e.g. "Number of proc...").|</source>
      </trans-unit>
      <trans-unit id="63c8cd81" xml:space="preserve">
        <source>
Existing data sets are displayed in a list. You may:

-   ![](../../../../../../assets/en/manual/web_interface/frontend_sections/dashboards/move_icon.png) -
    click on the move icon and drag a data set to a new place in the
    list.
-   ![](../../../../../../assets/en/manual/web_interface/frontend_sections/dashboards/expand_icon.png) -
    click on the expand icon to expand data set details. When expanded, this icon turns into a collapse icon.
-   ![](../../../../../../assets/en/manual/web_interface/frontend_sections/dashboards/color_icon.png) -
    click on the color icon to change the base color, either from the color picker or manually. The base color is used to calculate different colors for each item of the data set.
-   ![](../../../../../../assets/en/manual/web_interface/frontend_sections/dashboards/add_data_set.png) -
    click on this button to add an empty data set allowing to select the host/item pattern.
    -   If you click on the downward pointing icon next to the *Add new data set* button, a drop-down menu appears allowing to add a new data set with item pattern/item list or by cloning the currently open data set. If all data sets are collapsed, the *Clone* option is not available.

![](../../../../../../assets/en/manual/web_interface/frontend_sections/dashboards/graph_dataset_add.png)</source>
      </trans-unit>
      <trans-unit id="4bf7d5e9" xml:space="preserve">
        <source>##### Data set configuration details

The *host pattern* and *item pattern* fields in the *Data set* tab both recognize full names
or patterns containing a wildcard symbol (\*).
This functionality enables to select all the host names and item names containing the selected pattern.
Most importantly, while typing the item name or item pattern in the *item pattern* field,
only the items that belong to the selected host name(s) are displayed on a drop-down list.
For example, having typed a pattern **z\*** in the *host pattern* field,
the drop-down list displays all the host names containing this pattern: z\*, Zabbix server, and Zabbix proxy.
After pressing *Enter*, this pattern is accepted and is displayed as **z\***.
Similarly, the pattern can be created in the *item pattern* field.
For example, having typed the pattern **a\*** in the *item pattern* field,
the drop-down list displays all the item names containing this pattern:
a\*, Available memory, Available memory in %.

See the image of the *Data set* tab below.

![](../../../../../../assets/en/manual/web_interface/frontend_sections/dashboards/widgets/graph_widget_it_host_pattern_1.png){width="600"}

After pressing *Enter*, this pattern is accepted and is displayed as **a\***,
and all the selected items that belong to the selected host name(s) are displayed above the *Data set* tab.
See the image of the *Data set* tab below.

![](../../../../../../assets/en/manual/web_interface/frontend_sections/dashboards/widgets/graph_widget_it_host_pattern_2.png){width="600"}</source>
      </trans-unit>
      <trans-unit id="efef79c8" xml:space="preserve">
        <source>#### Displaying options

The **Displaying options** tab allows to define history data selection:

![](../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/graph_displaying_options.png)

|   |   |
|--|--------|
|*History data selection*|Set the source of graph data:&lt;br&gt;**Auto** - data are sourced according to the classic graph [algorithm](/manual/config/visualization/graphs/simple#generating_from_historytrends) (default);&lt;br&gt;**History** - data from history;&lt;br&gt;**Trends** - data from trends.|
|*Simple triggers*|Mark the checkbox to show simple triggers as lines with black dashes over the trigger severity color.|
|*Working time*|Mark the checkbox to show working time on the graph. Working time (working days) is displayed in graphs as a white background, while non-working time is displayed in gray (with the *Original blue* default frontend theme).|
|*Percentile line (left)*|Mark the checkbox and enter the percentile value to show the specified percentile as a line on the left Y-axis of the graph.&lt;br&gt;If, for example, a 95% percentile is set, then the percentile line will be at the level where 95 percent of the values fall under.|
|*Percentile line (right)*|Mark the checkbox and enter the percentile value to show the specified percentile as a line on the right Y-axis of the graph.&lt;br&gt;If, for example, a 95% percentile is set, then the percentile line will be at the level where 95 percent of the values fall under.|</source>
      </trans-unit>
      <trans-unit id="a747b952" xml:space="preserve">
        <source>#### Time period

The **Time period** tab allows to set a custom time period:

![](../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/graph_time_period.png)

|   |   |
|--|--------|
|*Set custom time period*|Mark this checkbox to set the custom time period for the graph (unmarked by default).|
|*From*|Set the start time of the custom time period for the graph.|
|*To*|Set the end time of the custom time period for the graph.|</source>
      </trans-unit>
      <trans-unit id="ec3778fa" xml:space="preserve">
        <source>#### Axes
The **Axes** tab allows to customize how axes are displayed:

![](../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/graph_axes.png)

|   |   |
|--|--------|
|*Left Y*|Mark this checkbox to make left Y-axis visible.&lt;br&gt;The checkbox may be disabled if unselected either in *Data set* or in *Overrides* tab.|
|*Right Y*|Mark this checkbox to make right Y-axis visible.&lt;br&gt;The checkbox may be disabled if unselected either in *Data set* or in *Overrides* tab.|
|*X-Axis*|Unmark this checkbox to hide X-axis (marked by default).|
|*Min*|Set the minimum value of the corresponding axis.&lt;br&gt;Visible range minimum value of Y-axis is specified.|
|*Max*|Set the maximum value of the corresponding axis.&lt;br&gt;Visible range maximum value of Y-axis is specified.|
|*Units*|Choose the unit for the graph axis values from the dropdown.&lt;br&gt;If the *Auto* option is chosen, axis values are displayed using units of the first item of the corresponding axis.&lt;br&gt;*Static* option allows you to assign the corresponding axis' custom name. If the *Static* option is chosen and the *value* input field left blank the corresponding axis' name will only consist of a numeric value.|</source>
      </trans-unit>
      <trans-unit id="b12a63bc" xml:space="preserve">
        <source>#### Legend

The **Legend** tab allows to customize the graph legend:

![](../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/graph_legend.png)

|   |   |
|--|--------|
|*Show legend*|Unmark this checkbox to hide the legend on the graph (marked by default).|
|*Display min/max/avg*|Mark this checkbox to display the minimum, maximum and average values of the item in the legend.|
|*Number of rows*|Set the number of legend rows to be displayed.|
|*Number of columns*|Set the number of legend columns to be displayed.|</source>
      </trans-unit>
      <trans-unit id="07ed8642" xml:space="preserve">
        <source>#### Problems

The **Problems** tab allows to customize the problem display:

![](../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/graph_problems.png){width="600"}

|   |   |
|--|--------|
|*Show problems*|Mark this checkbox to enable problem displaying on the graph (unmarked, i.e., disabled by default).|
|*Selected items only*|Mark this checkbox to include problems for the selected items only to be displayed on the graph.|
|*Problem hosts*|Select the problem hosts to be displayed on the graph.&lt;br&gt;&lt;br&gt;Wildcard patterns may be used (for example, `*` will return results that match zero or more characters).&lt;br&gt;To specify a wildcard pattern, just enter the string manually and press *Enter*.&lt;br&gt;While you are typing, note how all matching hosts are displayed in the dropdown.&lt;br&gt;&lt;br&gt;This parameter is not available when configuring the widget on a [template dashboard](/manual/config/templates/template#adding-dashboards).|
|*Severity*|Mark the problem severities to be displayed on the graph.|
|*Problem*|Specify the problem's name to be displayed on the graph.|
|*Problem tags*|Specify problem tags to limit the number of problems displayed in the widget.&lt;br&gt;It is possible to include as well as exclude specific tags and tag values. Several conditions can be set. Tag name matching is always case-sensitive.&lt;br&gt;&lt;br&gt;There are several operators available for each condition:&lt;br&gt;**Exists** - include the specified tag names;&lt;br&gt;**Equals** - include the specified tag names and values (case-sensitive);&lt;br&gt;**Contains** - include the specified tag names where the tag values contain the entered string (substring match, case-insensitive);&lt;br&gt;**Does not exist** - exclude the specified tag names;&lt;br&gt;**Does not equal** - exclude the specified tag names and values (case-sensitive);&lt;br&gt;**Does not contain** - exclude the specified tag names where the tag values contain the entered string (substring match, case-insensitive).&lt;br&gt;&lt;br&gt;There are two calculation types for conditions:&lt;br&gt;**And/Or** - all conditions must be met, conditions having the same tag name will be grouped by the *Or* condition;&lt;br&gt;**Or** - enough if one condition is met.|</source>
      </trans-unit>
      <trans-unit id="bb195e90" xml:space="preserve">
        <source>#### Overrides

The **Overrides** tab allows to add custom overrides for data sets:

![](../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/graph_overrides.png){width="600"}

Overrides are useful when several items are selected for a data set
using the `*` wildcard and you want to change how the items are
displayed by default (e.g. default base color or any other property).

Existing overrides (if any) are displayed in a list. To add a new
override:

-   Click on the
    ![](../../../../../../assets/en/manual/web_interface/frontend_sections/dashboards/add_override.png)
    button
-   Select hosts and items for the override. Alternatively, you may
    enter host and item patterns. Wildcard patterns may be used (for
    example, `*` will return results that match zero or more
    characters). To specify a wildcard pattern, just enter the string
    manually and press *Enter*. While you are typing, note how all
    matching hosts are displayed in the dropdown. The wildcard symbol is
    always interpreted, therefore it is not possible to add, for
    example, an item named "item\*" individually if there are other
    matching items (e.g. item2, item3). Host pattern and item pattern
    parameters are mandatory.
    The parameter for specifying host patterns is not available when configuring the widget on a [template dashboard](/manual/config/templates/template#adding-dashboards).
    The parameter for specifying an item list allows to select only [items configured on the template](/manual/config/templates/template#adding-items-triggers-graphs) when configuring the widget on a [template dashboard](/manual/config/templates/template#adding-dashboards).
-   Click on
    ![](../../../../../../assets/en/manual/web_interface/frontend_sections/dashboards/add_override2.png),
    to select override parameters. At least one override parameter
    should be selected. For parameter descriptions, see the *Data set*
    tab above.

Information displayed by the graph widget can be downloaded as a .png
image using the [widget
menu](/manual/web_interface/frontend_sections/dashboards#widget_menu):

![](../../../../../../assets/en/manual/web_interface/frontend_sections/dashboards/graph_widget_as_png.png)

A screenshot of the widget will be saved to the Downloads folder.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
