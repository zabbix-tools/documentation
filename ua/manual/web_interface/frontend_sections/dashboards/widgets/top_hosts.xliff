<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="ua" datatype="plaintext" original="manual/web_interface/frontend_sections/dashboards/widgets/top_hosts.md">
    <body>
      <trans-unit id="11ceac39" xml:space="preserve">
        <source># 21 Top hosts</source>
      </trans-unit>
      <trans-unit id="00820133" xml:space="preserve">
        <source>#### Overview

This widget provides a way to create custom tables for displaying the data situation, 
allowing to display *Top N*-like reports and progress-bar reports useful for capacity planning. 

The maximum number of hosts that can be displayed is 100.

![](../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/top_hosts.png){width="600"}

![](../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/top_hosts2.png){width="600"}</source>
      </trans-unit>
      <trans-unit id="1a44cc5c" xml:space="preserve">
        <source>#### Configuration

To configure, select *Top hosts* as type:

![](../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/top_hosts.1.png){width="600"}</source>
      </trans-unit>
      <trans-unit id="de161855" xml:space="preserve">
        <source>In addition to the parameters that are [common](/manual/web_interface/frontend_sections/dashboards/widgets#common-parameters) 
for all widgets, you may set the following specific options:

|   |   |
|--|--------|
|*Host groups*|Host groups to display data for.&lt;br&gt;This parameter is not available when configuring the widget on a [template dashboard](/manual/config/templates/template#adding-dashboards).|
|*Hosts*|Hosts to display data for.&lt;br&gt;This parameter is not available when configuring the widget on a [template dashboard](/manual/config/templates/template#adding-dashboards).|
|*Host tags*|Specify tags to limit the number of hosts displayed in the widget.&lt;br&gt;It is possible to include as well as exclude specific tags and tag values. Several conditions can be set. Tag name matching is always case-sensitive.&lt;br&gt;&lt;br&gt;There are several operators available for each condition:&lt;br&gt;**Exists** - include the specified tag names;&lt;br&gt;**Equals** - include the specified tag names and values (case-sensitive);&lt;br&gt;**Contains** - include the specified tag names where the tag values contain the entered string (substring match, case-insensitive);&lt;br&gt;**Does not exist** - exclude the specified tag names;&lt;br&gt;**Does not equal** - exclude the specified tag names and values (case-sensitive);&lt;br&gt;**Does not contain** - exclude the specified tag names where the tag values contain the entered string (substring match, case-insensitive).&lt;br&gt;&lt;br&gt;There are two calculation types for conditions:&lt;br&gt;**And/Or** - all conditions must be met, conditions having the same tag name will be grouped by the Or condition;&lt;br&gt;**Or** - enough if one condition is met.&lt;br&gt;&lt;br&gt;This parameter is not available when configuring the widget on a [template dashboard](/manual/config/templates/template#adding-dashboards).|
|*Columns*|Add data [columns](#columns) to display.&lt;br&gt;The column order determines their display from left to right.&lt;br&gt;Columns can be reordered by dragging up and down by the handle before the column name.|
|*Order*|Specify the ordering of rows:&lt;br&gt;**Top N** - in descending order by the *Order column* aggregated value;&lt;br&gt;**Bottom N** - in ascending order by the *Order column* aggregated value.|
|*Order column*|Specify the column from the defined *Columns* list to use for *Top N* or *Bottom N* ordering.|
|*Host count*|Count of host rows to be shown (1-100).&lt;br&gt;This parameter is not available when configuring the widget on a [template dashboard](/manual/config/templates/template#adding-dashboards).|</source>
      </trans-unit>
      <trans-unit id="54c7e604" xml:space="preserve">
        <source>##### Column configuration

![](../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/top_hosts.2.png){width="600"}</source>
      </trans-unit>
      <trans-unit id="ded10671" xml:space="preserve">
        <source>Common column parameters:

|   |   |
|--|--------|
|*Name*|Name of the column.|
|*Data*|Data type to display in the column:&lt;br&gt;**Item value** - value of the specified item;&lt;br&gt;**Host name** - host name of the item specified in the *Item value* column;&lt;br&gt;**Text** - static text string.|
|*Base color*|Background color of the column; fill color if *Item value* data is displayed as bar/indicators.&lt;br&gt;For *Item value* data the default color can be overridden by custom color, if the item value is over one of the specified thresholds.|</source>
      </trans-unit>
      <trans-unit id="c994be1c" xml:space="preserve">
        <source>Specific parameters for item value columns:

|   |   |
|--|--------|
|*Item*|Select the item.&lt;br&gt;When configuring the widget on a [template dashboard](/manual/config/templates/template#adding-dashboards), only [items configured on the template](/manual/config/templates/template#adding-items-triggers-graphs) can be selected.&lt;br&gt;Selecting items with binary values is not supported.|
|*Time shift*|Specify time shift if required.&lt;br&gt;You may use [time suffixes](/manual/appendix/suffixes#time_suffixes) in this field. Negative values are allowed.|
|*Aggregation function*|Specify which aggregation function to use:&lt;br&gt;**min** - display the smallest value;&lt;br&gt;**max** - display the largest value;&lt;br&gt;**avg** - display the average value;&lt;br&gt;**sum** - display the sum of values;&lt;br&gt;**count** - display the count of values;&lt;br&gt;**first** - display the first value;&lt;br&gt;**last** - display the last value;&lt;br&gt;**none** - display all values (no aggregation).&lt;br&gt;&lt;br&gt;Aggregation allows to display an aggregated value for the chosen interval (5 minutes, an hour, a day), instead of all values.&lt;br&gt;Note that only numeric items can be displayed in this column if this setting is not "none".|
|*Aggregation interval*|Specify the interval for aggregating values.&lt;br&gt;You may use [time suffixes](/manual/appendix/suffixes#time_suffixes) in this field. A numeric value without a suffix will be regarded as seconds.&lt;br&gt;This parameter will not be displayed if *Aggregation function* is set to "none".|
|*Display*|Define how the value should be displayed:&lt;br&gt;**As is** - as regular text;&lt;br&gt;**Bar** - as solid, color-filled bar;&lt;br&gt;**Indicators** - as segmented, color-filled bar.&lt;br&gt;&lt;br&gt;Note that only numeric items can be displayed in this column if this setting is not "as is".|
|*History*|Take data from history or trends:&lt;br&gt;**Auto** - automatic selection;&lt;br&gt;**History** - take history data;&lt;br&gt;**Trends** - take trend data.&lt;br&gt;&lt;br&gt;This setting applies only to numeric data. Non-numeric data will always be taken from history.|
|*Min*|Minimum value for bar/indicators.|
|*Max*|Maximum value for bar/indicators.|
|*Decimal places*|Specify how many decimal places will be displayed with the value.&lt;br&gt;This setting applies only to numeric data.|
|*Thresholds*|Specify threshold values when the background/fill color should change.&lt;br&gt;The list will be sorted in ascending order when saved.&lt;br&gt;Note that only numeric items can be displayed in this column if thresholds are used.|</source>
      </trans-unit>
      <trans-unit id="b7e22215" xml:space="preserve">
        <source>Specific parameters for text columns:

|   |   |
|--|--------|
|*Text*|Enter the string to display.&lt;br&gt;May contain host and inventory [macros](/manual/appendix/macros/supported_by_location).|</source>
      </trans-unit>
    </body>
  </file>
</xliff>
