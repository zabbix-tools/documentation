[comment]: # translation:outdated

[comment]: # ({new-ae6d2ebd})
# 3 Hosts

[comment]: # ({/new-ae6d2ebd})

[comment]: # ({new-26f6fa20})
#### Overview

The *Monitoring → Hosts* section displays a full list of monitored hosts
with detailed information about host interface, availability, tags,
current problems, status (enabled/disabled), and links to easily
navigate to the host's latest data, problem history, graphs, dashboards
and web scenarios.

![](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/monitoring_hosts.png){width="600"}

|Column|Description|
|------|-----------|
|*Name*|The visible host name. Clicking on the name brings up the [host menu](/manual/web_interface/frontend_sections/monitoring/dashboard#host_menu).<br>An orange wrench icon ![](../../../../../assets/en/manual/web_interface/frontend_sections/configuration/maintenance_wrench_icon.png) after the name indicates that this host is in maintenance.<br>Click on the column header to sort hosts by name in ascending or descending order.|
|*Interface*|The main interface of the host is displayed.|
|*Availability*|Host [availability](/manual/web_interface/frontend_sections/configuration/hosts#reading_host_availability) per configured interface.<br>Icons represent only those interface types (Zabbix agent, SNMP, IPMI, JMX) that are configured. If you position the mouse on the icon, a popup list of all interfaces of this type appears with each interface details, status and errors.<br>The column is empty for hosts with no interfaces.<br>The current status of all interfaces of one type is displayed by the respective icon color:<br>**Green** - all interfaces available<br>**Yellow** - at least one interface available and at least one unavailable; others can have any value including 'unknown'<br>**Red** - no interfaces available<br>**Gray** - at least one interface unknown (none unavailable)<br>Note that active Zabbix agent items do not affect host availability.|
|*Tags*|[Tags](/manual/config/tagging) of the host and all linked templates, with macros unresolved.|
|*Status*|Host status - *Enabled* or *Disabled*.<br>Click on the column header to sort hosts by status in ascending or descending order.|
|*Latest data*|Clicking on the link will open *Monitoring - Latest data* page with all the latest data collected from the host.|
|*Problems*| The number of open host problems sorted by severity. The color of the square indicates problem severity. The number on the square means the number of problems for the given severity.<br>Clicking on the icon will open *Monitoring - Problems* page for the current host. <br>If a host has no problems, a link to the Problems section for this host is displayed as text. <br>Use the filter to select whether suppressed problems should be included (not included by default).|
|*Graphs*|Clicking on the link will display graphs configured for the host. The number of graphs is displayed in gray.<br>If a host has no graphs, the link is disabled (gray text) and no number is displayed.|
|*Dashboards*|Clicking on the link will display dashboards configured for the host. The number of dashboards is displayed in gray.<br>If a host has no dashboards, the link is disabled (gray text) and no number is displayed.|
|*Web*|Clicking on the link will display web scenarios configured for the host. The number of web scenarios is displayed in gray.<br>If a host has no web scenarios, the link is disabled (gray text) and no number is displayed.|

[comment]: # ({/new-26f6fa20})

[comment]: # ({new-bd285dea})
##### Buttons

*Create host* allows to create a [new host](/manual/config/hosts/host).
This button is available for Admin and Super Admin users only.

View mode buttons being common for all sections are described on the
[Monitoring](/manual/web_interface/frontend_sections/monitoring#view_mode_buttons)
page.

[comment]: # ({/new-bd285dea})

[comment]: # ({new-159cadac})
#### Using filter

You can use the filter to display only the hosts you are interested in.
For better search performance, data is searched with macros unresolved.

The filter is located above the table. It is possible to filter hosts by
name, host group, IP or DNS, interface port, tags, problem severity,
status (enabled/disabled/any); you can also select whether to display
suppressed problems and hosts that are currently in maintenance.
Favorite filter settings can be saved as tabs and then quickly accessed
by clicking on the [tabs above the
filter](/manual/web_interface/frontend_sections/monitoring/problems#tabs_for_favorite_filters).

![](../../../../../assets/en/manual/web_interface/monitoring_hosts_filter.png){width="600"}

|Parameter|Description|
|---------|-----------|
|*Name*|Filter by visible host name.|
|*Host groups*|Filter by one or more host groups.<br>Specifying a parent host group implicitly selects all nested host groups.|
|*IP*|Filter by IP address.|
|*DNS*|Filter by DNS name.|
|*Port*|Filter by port number.|
|*Severity*|Filter by problem severity. By default problems of all severities are displayed. Problems are displayed if not suppressed.|
|*Status*|Filter by host status.|
|*Tags*|Filter by host tag name and value. Hosts can be filtered by host-level tags as well as tags from all linked templates, including parent templates.<br>It is possible to include as well as exclude specific tags and tag values. Several conditions can be set. Tag name matching is always case-sensitive.<br>There are several operators available for each condition:<br>**Exists** - include the specified tag names<br>**Equals** - include the specified tag names and values (case-sensitive)<br>**Contains** - include the specified tag names where the tag values contain the entered string (substring match, case-insensitive)<br>**Does not exist** - exclude the specified tag names<br>**Does not equal** - exclude the specified tag names and values (case-sensitive)<br>**Does not contain** - exclude the specified tag names where the tag values contain the entered string (substring match, case-insensitive)<br>There are two calculation types for conditions:<br>**And/Or** - all conditions must be met, conditions having the same tag name will be grouped by the Or condition<br>**Or** - enough if one condition is met|
|*Show hosts in maintenance*|Mark the checkbox to display hosts that are in maintenance (displayed by default).|
|*Show suppressed problems*|Mark the checkbox to display problems that would otherwise be suppressed (not shown) because of host maintenance.|

[comment]: # ({/new-159cadac})
