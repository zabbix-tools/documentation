<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="ua" datatype="plaintext" original="manual/web_interface/frontend_sections/data_collection/hosts.md">
    <body>
      <trans-unit id="ae6d2ebd" xml:space="preserve">
        <source># 4 Hosts</source>
      </trans-unit>
      <trans-unit id="a0b0a26b" xml:space="preserve">
        <source>#### Overview

In the *Data collection → Hosts* section users can configure and maintain
hosts.

A listing of existing hosts with their details is displayed.

![](../../../../../assets/en/manual/web_interface/frontend_sections/configuration/hosts.png){width="600"}

Displayed data:

|Column|Description|
|--|--------|
|*Name*|Name of the host.&lt;br&gt;Clicking on the host name opens the host [configuration form](/manual/config/hosts/host#configuring_a_host).|
|*Entities (Items, Triggers, Graphs, Discovery, Web)*|Clicking on the entity name will display items, triggers etc. of the host.&lt;br&gt;The number of the respective entities is displayed in gray.|
|*Interface*|The main interface of the host is displayed.|
|*Proxy*|Proxy name is displayed, if the host is monitored by a proxy.&lt;br&gt;This column is only displayed if the *Monitored by* filter option is set to 'Any' or 'Proxy'.|
|*Templates*|The templates linked to the host are displayed.&lt;br&gt;If other templates are contained in the linked template, those are displayed in parentheses, separated by a comma.&lt;br&gt;Clicking on a template name will open its configuration form.|
|*Status*|Host status is displayed - *Enabled* or *Disabled*.&lt;br&gt;By clicking on the status you can change it.&lt;br&gt;An orange wrench icon ![](../../../../../assets/en/manual/web_interface/frontend_sections/configuration/maintenance_wrench_icon.png) before the host status indicates that this host is in maintenance. Maintenance details are displayed when the mouse pointer is positioned on the icon.|
|*Availability*|Host [availability](/manual/web_interface/frontend_sections/data_collection/hosts#reading_host_availability) per configured interface is displayed.&lt;br&gt;&lt;br&gt;Availability icons represent only those interface types (Zabbix agent, SNMP, IPMI, JMX) that are configured. If you position the mouse pointer on the icon, a pop-up list appears listing all interfaces of this type with details, status and errors (for the agent interface, availability of active checks is also listed).&lt;br&gt;The column is empty for hosts with no interfaces.&lt;br&gt;The current status of all interfaces of one type is displayed by the respective icon color:&lt;br&gt;**Green** - all interfaces are available;&lt;br&gt;**Yellow** - at least one interface is available and at least one is unavailable; others can have any status, including 'unknown';&lt;br&gt;**Red** - no interfaces are available;&lt;br&gt;**Gray** - at least one interface is unknown (none unavailable).&lt;br&gt;&lt;br&gt;**Active check availability.** Since Zabbix 6.2 active checks also affect host availability, if there is at least one enabled active check on the host. To determine active check availability, heartbeat messages are sent in the agent active check thread. The frequency of the heartbeat messages is set by the `HeartbeatFrequency` parameter in Zabbix [agent](/manual/appendix/config/zabbix_agentd) and [agent 2](/manual/appendix/config/zabbix_agent2) configurations (60 seconds by default, 0-3600 range). Active checks are considered unavailable when the active check heartbeat is older than 2 x HeartbeatFrequency seconds.&lt;br&gt;**Note:** If Zabbix agents older than 6.2.x are used, they are not sending any active check heartbeats, so the availability of their hosts will remain unknown.&lt;br&gt;Active agent availability is counted towards the total Zabbix agent availability in the same way as a passive interface is. For example, if a passive interface is available while the active checks are unknown, the total agent availability is set to gray (unknown).|
|*Agent encryption*|Encryption status for connections to the host is displayed:&lt;br&gt;**None** - no encryption;&lt;br&gt;**PSK** - using pre-shared key;&lt;br&gt;**Cert** - using certificate.|
|*Info*|Error information (if any) regarding the host is displayed.|
|*Tags*|[Tags](/manual/config/tagging) of the host with macros unresolved.|

To configure a new host, click on the *Create host* button in the top
right-hand corner. To import a host from a YAML, XML, or JSON file,
click on the *Import* button in the top right-hand corner.</source>
      </trans-unit>
      <trans-unit id="979498d5" xml:space="preserve">
        <source>##### Mass editing options

Buttons below the list offer some mass editing options:

-   *Enable* - change host status to *Monitored*;
-   *Disable* - change host status to *Not monitored*;
-   *Export* - export the hosts to a YAML, XML or JSON file;
-   *Mass update* - [update several properties](/manual/config/hosts/hostupdate) for a number of hosts at once;
-   *Delete* - delete the hosts.

To use these options, mark the checkboxes before the respective hosts,
then click on the required button.</source>
      </trans-unit>
      <trans-unit id="caf065ae" xml:space="preserve">
        <source>#### Using filter

You can use the filter to display only the hosts you are interested in.
For better search performance, data is searched with macros unresolved.

The *Filter* icon is available at the top right corner.
Clicking on it will open a filter where you can specify the desired filtering criteria.

![](../../../../../assets/en/manual/web_interface/host_filter.png){width="600"}

|Parameter|Description|
|--|--------|
|*Host groups*|Filter by one or more host groups.&lt;br&gt;Specifying a parent host group implicitly selects all nested host groups.|
|*Templates*|Filter by linked templates.|
|*Name*|Filter by visible host name.|
|*DNS*|Filter by DNS name.|
|*IP*|Filter by IP address.|
|*Port*|Filter by port number.|
|*Status*|Filter by host status.|
|*Monitored by*|Filter hosts that are monitored by server only, proxy only or both.|
|*Proxy*|Filter hosts that are monitored by the proxy specified here.|
|*Tags*|Filter by host tag name and value.&lt;br&gt;It is possible to include as well as exclude specific tags and tag values. Several conditions can be set. Tag name matching is always case-sensitive.&lt;br&gt;&lt;br&gt;There are several operators available for each condition:&lt;br&gt;**Exists** - include the specified tag names;&lt;br&gt;**Equals** - include the specified tag names and values (case-sensitive);&lt;br&gt;**Contains** - include the specified tag names where the tag values contain the entered string (substring match, case-insensitive);&lt;br&gt;**Does not exist** - exclude the specified tag names;&lt;br&gt;**Does not equal** - exclude the specified tag names and values (case-sensitive);&lt;br&gt;**Does not contain** - exclude the specified tag names where the tag values contain the entered string (substring match, case-insensitive).&lt;br&gt;&lt;br&gt;There are two calculation types for conditions:&lt;br&gt;**And/Or** - all conditions must be met, conditions having the same tag name will be grouped by the Or condition;&lt;br&gt;**Or** - enough if one condition is met.|</source>
      </trans-unit>
      <trans-unit id="aafce980" xml:space="preserve">
        <source>#### Reading host availability

Host availability icons reflect the current host interface status on
Zabbix server. Therefore, in the frontend:

-   If you disable a host, availability icons will not immediately turn
    gray (unknown status), because the server has to synchronize the
    configuration changes first.
-   If you enable a host, availability icons will not immediately turn
    green (available), because the server has to synchronize the
    configuration changes and start polling the host first.</source>
      </trans-unit>
      <trans-unit id="4dfcee51" xml:space="preserve">
        <source>#### Unknown interface status

Zabbix server determines an 'unknown' status for the corresponding agent
interface (Zabbix, SNMP, IPMI, JMX) in the following cases:

-   There are no enabled items on the interface (they were removed or
    disabled).
-   There are only active Zabbix agent items.
-   There are no pollers for that type of the interface (e.g.
    StartPollers=0).
-   Host is disabled.
-   Host is set to be monitored by proxy, a different proxy or by server
    if it was monitored by proxy.
-   Host is monitored by a proxy that appears to be offline (no updates
    received from the proxy during the maximum heartbeat interval - 1
    hour).

Setting interface availability to 'unknown' is done after server
configuration cache synchronization. Restoring interface availability
(available/unavailable) on hosts monitored by proxies is done after
proxy configuration cache synchronization.

For more details about host interface unreachability, see [Unreachable/unavailable host interface settings](/manual/appendix/items/unreachability).</source>
      </trans-unit>
    </body>
  </file>
</xliff>
