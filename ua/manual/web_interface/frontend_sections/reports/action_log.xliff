<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="ua" datatype="plaintext" original="manual/web_interface/frontend_sections/reports/action_log.md">
    <body>
      <trans-unit id="7d551978" xml:space="preserve">
        <source># 6 Action log</source>
      </trans-unit>
      <trans-unit id="8be899f9" xml:space="preserve">
        <source>#### Overview

In the *Reports → Action log* section users can view details of operations (notifications, remote commands) executed within an action.

![](../../../../../assets/en/manual/web_interface/action_log2.png){width="600"}

Displayed data:

|Column|Description|
|--|--------|
|*Time*|Timestamp of the operation.|
|*Action*|Name of the action causing operations.|
|*Media type*|Media type (e.g. Email, Jira, etc.) used for sending a notification.&lt;br&gt;For operations that executed remote commands, this column will be empty.|
|*Recipient*|Information about the notification recipient - username, name and surname (in parentheses), and additional information depending on the media type (email, username, etc.).&lt;br&gt;For operations that executed remote commands, this column will be empty.|
|*Message*|The content of the message/remote command.&lt;br&gt;A remote command is separated from the target host with a colon symbol: `&lt;host&gt;:&lt;command&gt;`. For example, if the remote command was executed on Zabbix server, then the information will have the following format: `Zabbix server:&lt;command&gt;`.|
|*Status*|Operation status:&lt;br&gt;*In progress* - operation for sending a notification is in progress (the remaining number of times the server will try to send the notification is also displayed)&lt;br&gt;*Sent* - notification has been sent&lt;br&gt;*Executed* - remote command has been executed&lt;br&gt;*Failed* - operation has not been completed|
|*Info*|Error information (if any) regarding the operation execution.|</source>
      </trans-unit>
      <trans-unit id="d84e7bb0" xml:space="preserve">
        <source>#### Buttons

The button at the top right corner of the page offers the following option:

|   |   |
|--|--------|
|![](../../../../../assets/en/manual/web_interface/button_csv.png)|Export action log records from all pages to a CSV file. If a filter is applied, only the filtered records will be exported.&lt;br&gt;In the exported CSV file the columns "Recipient" and "Message" are divided into several columns - "Recipient's Zabbix username", "Recipient's name", "Recipient's surname", "Recipient", and "Subject", "Message", "Command".|</source>
      </trans-unit>
      <trans-unit id="accea306" xml:space="preserve">
        <source>#### Using filter

The filter is located below the *Action log* bar.
It can be opened and collapsed by clicking on the *Filter* tab at the top right corner of the page.

![](../../../../../assets/en/manual/web_interface/action_log_filter.png){width="600"}

You may use the filter to narrow down the records by notification recipients, actions, media types, status,
or by the message/remote command content (*Search string*).
For better search performance, data is searched with macros unresolved.</source>
      </trans-unit>
      <trans-unit id="3eac5e48" xml:space="preserve">
        <source>#### Time period selector

The [time period selector](/manual/config/visualization/graphs/simple#time_period_selector) allows to select often required periods with one mouse click.
The time period selector can be opened by clicking on the time period tab next to the filter.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
