[comment]: # translation:outdated

[comment]: # ({new-764560a7})
# 1 Menu

[comment]: # ({/new-764560a7})

[comment]: # ({new-4edaa9a7})
#### Overview

A vertical menu in a sidebar provides access to various Zabbix frontend
sections.

The menu is dark blue in the default theme.

![](../../../assets/en/manual/introduction/vertical_menu.png){width="600"}

[comment]: # ({/new-4edaa9a7})

[comment]: # ({new-6ddbf7a4})
#### Working with the menu

A [global search](/manual/web_interface/global_search) box is located
below the Zabbix logo.

The menu can be collapsed or hidden completely:

-   To collapse, click on
    ![](../../../assets/en/manual/web_interface/button_collapse.png)
    next to Zabbix logo
-   To hide, click on
    ![](../../../assets/en/manual/web_interface/button_hide.png) next to
    Zabbix logo

|   |   |
|---|---|
|![](../../../assets/en/manual/web_interface/collapsed_menu.png)|![](../../../assets/en/manual/web_interface/hidden_menu.png)|
|Collapsed menu with only the icons visible.|Hidden menu.|

[comment]: # ({/new-6ddbf7a4})

[comment]: # ({new-3086e8b2})
##### Collapsed menu

When the menu is collapsed to icons only, a full menu reappears as soon
as the mouse cursor is placed upon it. Note that it reappears over page
content; to move page content to the right you have to click on the
expand button. If the mouse cursor again is placed outside the full
menu, the menu will collapse again after two seconds.

You can also make a collapsed menu reappear fully by hitting the Tab
key. Hitting the Tab key repeatedly will allow to focus on the next menu
element.

[comment]: # ({/new-3086e8b2})

[comment]: # ({new-7dd192ee})
##### Hidden menu

Even when the menu is hidden completely, a full menu is just one mouse
click away, by clicking on the burger icon. Note that it reappears over
page content; to move page content to the right you have to unhide the
menu by clicking on the show sidebar button.

[comment]: # ({/new-7dd192ee})

[comment]: # ({new-0d46829f})

##### Menu levels

There are up to three levels in the menu.

![](../../../assets/en/manual/web_interface/menu_levels.png)

[comment]: # ({/new-0d46829f})

[comment]: # ({new-5ec4ea88})
### Context menu

In addition to the main menu, Zabbix provides [host menu](/manual/web_interface/menu/host_menu) and 
[event menu](/manual/web_interface/menu/event_menu) for convenient access to frequently used elements, such as
configuration window, related scripts or external links. The context menus are accessible by clicking on the host or
problem/trigger name in supported locations.

Similarly, in the *Monitoring->Latest data* section clicking on the item name brings up the 
[item menu](/manual/web_interface/frontend_sections/monitoring/latest_data#item-menu).

[comment]: # ({/new-5ec4ea88})
