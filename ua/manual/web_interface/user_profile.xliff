<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="ua" datatype="plaintext" original="manual/web_interface/user_profile.md">
    <body>
      <trans-unit id="bba949fc" xml:space="preserve">
        <source># 3 User settings</source>
      </trans-unit>
      <trans-unit id="2f38d1c3" xml:space="preserve">
        <source>#### Overview

Depending on user role permissions, the *User settings* section may
contain the following pages:

-   *Profile* or *User profile* - for customizing certain Zabbix frontend features.
-   *API tokens* - for managing API tokens assigned to the current user.

The list of available pages appears upon pressing on the
![](../../../assets/en/manual/web_interface/user_profile.png) user icon
near the bottom of the Zabbix menu (not available for a guest user). It
is also possible to switch between pages by using a title dropdown in
the top left corner.

|   |   |
|---|---|
|![](../../../assets/en/manual/web_interface/user_settings_menu.png){width="300"}|![](../../../assets/en/manual/web_interface/user_settings_selector.png){width="300"}|
|Third-level menu.|Title dropdown.|</source>
      </trans-unit>
      <trans-unit id="8fcf9738" xml:space="preserve">
        <source>#### User profile

The **User profile** section provides options to set custom interface
language, color theme, number of rows displayed in the lists, etc. The
changes made here will be applied to the current user only.

The **User** tab allows you to set various user preferences.

![profile.png](../../../assets/en/manual/web_interface/profile.png)

|Parameter|Description|
|--|--------|
|*Password*|Click on the *Change password* button to open three fields: *Old password*, *New password*, *New password (once again)*.&lt;br&gt;On a successful password change, the user will be logged out of all active sessions.&lt;br&gt;Note that the password can only be changed for users using Zabbix [internal authentication](/manual/web_interface/frontend_sections/users/authentication#default-authentication).|
|*Language*|Select the interface language of your choice or select **System default** to use default system settings.&lt;br&gt;For more information, see [Installation of additional frontend languages](/manual/appendix/install/locales).|
|*Time zone*|Select the time zone to override global [time zone](/manual/web_interface/time_zone#overview) on user level or select **System default** to use global time zone settings.|
|*Theme*|Select a color theme specifically for your profile:&lt;br&gt;**System default** - use default system settings&lt;br&gt;**Blue** - standard blue theme&lt;br&gt;**Dark** - alternative dark theme&lt;br&gt;**High-contrast light** - light theme with high contrast&lt;br&gt;**High-contrast dark** - dark theme with high contrast|
|*Auto-login*|Mark this checkbox to make Zabbix remember you and log you in automatically for 30 days. Browser cookies are used for this.|
|*Auto-logout*|With this checkbox marked you will be logged out automatically, after the set amount of seconds (minimum 90 seconds, maximum 1 day).&lt;br&gt;[Time suffixes](/manual/appendix/suffixes) are supported, for example: 90s, 5m, 2h, 1d.&lt;br&gt;Note that this option will not work:&lt;br&gt;\* When Monitoring menu pages perform background information refreshes. In case pages refreshing data in a specific time interval (dashboards, graphs, latest data, etc.) are left open session lifetime is extended, respectively disabling auto-logout feature.&lt;br&gt;\* If logging in with the *Remember me for 30 days* option checked.&lt;br&gt;*Auto-logout* can also accept "0", meaning that auto-logout feature becomes disabled after profile settings update.|
|*Refresh*|You can set how often the information in the pages will be refreshed on the Monitoring menu.&lt;br&gt;[Time suffixes](/manual/appendix/suffixes) are supported, for example: 30s, 5m, 2h, 1d.|
|*Rows per page*|You can set how many rows will be displayed per page in the lists. Fewer rows (and fewer records to display) mean faster loading times.|
|*URL (after login)*|You can set a specific URL to be displayed after the login. Instead of the default *Dashboards* it can be, for example, the URL of *Monitoring* → *Triggers*.|

The **Media** tab allows you to specify the [media details](/manual/config/notifications/media#user_media) for the user,
such as media types and addresses to use and when to use them to deliver notifications.

![profile\_b.png](../../../assets/en/manual/web_interface/profile_b.png)

If the media type has been disabled:

-   A yellow info icon is displayed after the name.
-   *Disabled* is displayed in the Status column.

::: noteclassic
Only [admin
level](/manual/config/users_and_usergroups/permissions) users (*Admin* and
*Super admin*) can change their own media details.
:::

The **Messaging** tab allows you to set [global
notifications](/manual/web_interface/user_profile/global_notifications).</source>
      </trans-unit>
      <trans-unit id="960ba61e" xml:space="preserve">
        <source>#### API tokens

API tokens section allows to view tokens assigned to the user, edit
token details and [create new
tokens](/manual/web_interface/frontend_sections/users/api_tokens).
This section is only available to a user if *Manage API tokens* action
is allowed in the [user
role](/manual/web_interface/frontend_sections/users/user_roles)
settings.

![](../../../assets/en/manual/web_interface/user_tokens.png){width="600"}

You may filter API tokens by name, expiry date, or status
(*Enabled*/*Disabled*). Click on the token status in the list to quickly
enable/disable a token. You may also mass enable/disable tokens by
selecting them in the list and then clicking on the *Enable*/*Disable*
buttons below the list. 

::: noteimportant
Users cannot view *Auth
token* value of the tokens assigned to them in Zabbix. *Auth token*
value is displayed only once - immediately after creating a token. If it
has been lost, the token has to be regenerated. 
:::</source>
      </trans-unit>
    </body>
  </file>
</xliff>
