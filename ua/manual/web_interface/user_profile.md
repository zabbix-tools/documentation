[comment]: # translation:outdated

[comment]: # ({new-bba949fc})
# 3 User settings

[comment]: # ({/new-bba949fc})

[comment]: # ({new-2f38d1c3})
#### Overview

Depending on user role permissions, the *User settings* section may
contain the following pages:

-   *User profile* - for customizing certain Zabbix frontend features;
-   *API tokens* - for managing API tokens assigned to the current user.

The list of available pages appears upon pressing on the
![](../../../assets/en/manual/web_interface/user_profile.png) user icon
near the bottom of the Zabbix menu (not available for a guest user). It
is also possible to switch between pages by using a title dropdown in
the top left corner.

|   |   |
|---|---|
|![](../../../assets/en/manual/web_interface/user_menu.png){width="300"}|![](../../../assets/en/manual/web_interface/user_selector.png){width="300"}|
|Third-level menu.|Title dropdown.|

[comment]: # ({/new-2f38d1c3})

[comment]: # ({new-8fcf9738})
#### - User profile

The **User profile** section provides options to set custom interface
language, color theme, number of rows displayed in the lists, etc. The
changes made here will be applied to the current user only.

The **User** tab allows you to set various user preferences.

![profile.png](../../../assets/en/manual/web_interface/profile.png)

|Parameter|Description|
|---------|-----------|
|*Password*|Click on the link to display two fields for entering a new password.|
|*Language*|Select the interface language of your choice or select **System default** to use default system settings.<br>For more information, see [Installation of additional frontend languages](/manual/appendix/install/locales).|
|*Time zone*|Select the time zone to override global [time zone](/manual/web_interface/time_zone#overview) on user level or select **System default** to use global time zone settings.|
|*Theme*|Select a color theme specifically for your profile:<br>**System default** - use default system settings<br>**Blue** - standard blue theme<br>**Dark** - alternative dark theme<br>**High-contrast light** - light theme with high contrast<br>**High-contrast dark** - dark theme with high contrast|
|*Auto-login*|Mark this checkbox to make Zabbix remember you and log you in automatically for 30 days. Browser cookies are used for this.|
|*Auto-logout*|With this checkbox marked you will be logged out automatically, after the set amount of seconds (minimum 90 seconds, maximum 1 day).<br>[Time suffixes](/manual/appendix/suffixes) are supported, e.g. 90s, 5m, 2h, 1d.<br>Note that this option will not work:<br>\* When Monitoring menu pages perform background information refreshes. In case pages refreshing data in a specific time interval (dashboards, graphs, latest data, etc.) are left open session lifetime is extended, respectively disabling auto-logout feature;<br>\* If logging in with the *Remember me for 30 days* option checked.<br>Auto-logout can accept 0, meaning that Auto-logout becomes disabled after profile settings update.|
|*Refresh*|You can set how often the information in the pages will be refreshed on the Monitoring menu, except for Dashboard, which uses its own refresh parameters for every widget.<br>[Time suffixes](/manual/appendix/suffixes) are supported, e.g. 30s, 5m, 2h, 1d.|
|*Rows per page*|You can set how many rows will be displayed per page in the lists. Fewer rows (and fewer records to display) mean faster loading times.|
|*URL (after login)*|You can set a specific URL to be displayed after the login. Instead of the default *Monitoring* → *Dashboard* it can be, for example, the URL of *Monitoring* → *Triggers*.|

The **Media** tab allows you to specify the [media
details](/manual/config/notifications/media#user_media) for the user,
such as the types, the addresses to use and when to use them to deliver
notifications.

![profile\_b.png](../../../assets/en/manual/web_interface/profile_b.png)

::: noteclassic
Only [admin
level](/manual/config/users_and_usergroups/permissions) users (Admin and
Super admin) can change their own media details.
:::

The **Messaging** tab allows you to set [global
notifications](/manual/web_interface/user_profile/global_notifications).

[comment]: # ({/new-8fcf9738})

[comment]: # ({new-960ba61e})
#### - API tokens

API tokens section allows to view tokens assigned to the user, edit
token details and [create new
tokens](/manual/web_interface/frontend_sections/administration/general#api_tokens).
This section is only available to a user if *Manage API tokens* action
is allowed in the [user
role](/manual/web_interface/frontend_sections/administration/user_roles)
settings.

![](../../../assets/en/manual/web_interface/user_tokens.png){width="600"}

You may filter API tokens by name, expiry date, or status
(enabled/disabled). Click on the token status in the list to quickly
enable/disable a token. You may also mass enable/disable tokens by
selecting them in the list and then clicking on the Enable/Disable
buttons below the list. 

::: noteimportant
 Users cannot view *Auth
token* value of the tokens assigned to them in Zabbix. *Auth token*
value is displayed only once - immediately after creating a token. If it
has been lost, the token has to be regenerated. 
:::

[comment]: # ({/new-960ba61e})
