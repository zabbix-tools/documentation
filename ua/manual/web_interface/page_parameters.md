[comment]: # translation:outdated

[comment]: # ({new-3043f46c})
# 6 Page parameters

[comment]: # ({/new-3043f46c})

[comment]: # ({new-79688fd8})
#### Overview

Most Zabbix web interface pages support various HTTP GET parameters that
control what will be displayed. They may be passed by specifying
parameter=value pairs after the URL, separated from the URL by a
question mark (?) and from each other by ampersands (&).

[comment]: # ({/new-79688fd8})

[comment]: # ({new-46634552})
#### Monitoring → Problems

The following parameters are supported:

-   `show` - filter option "Show": 1 - recent problems, 2 - all, 3 - in
    problem state
-   `name` - filter option "Problem": freeform string
-   `severities` - filter option "Severity": array of selected
    severities in a format 'severities\[\*\]=\*' (replace \* with
    severity level): 0 - not classified, 1 - information, 2 - warning,
    3 - average, 4 - high, 5 - disaster
-   `inventory` - filter option "Host inventory": array of inventory
    fields: \[field\], \[value\]
-   `evaltype` - filter option "Tags", tag filtering strategy: 0 -
    And/Or, 2 - Or
-   `tags` - filter option "Tags": array of defined tags: \[tag\],
    \[operator\], \[value\]
-   `show_tags` - filter option "Show tags": 0 - none, 1 - one, 2 - two,
    3 - three
-   `tag_name_format` - filter option "Tag name": 0 - full name, 1 -
    shortened, 2 - none
-   `tag_priority` - filter option "Tag display priority":
    comma-separated string of tag display priority
-   `show_suppressed` - filter option "Show suppressed problems": should
    be 'show\_suppressed=1' to show
-   `unacknowledged` - filter option "Show unacknowledged only": should
    be 'unacknowledged=1' to show
-   `compact_view` - filter option "Compact view": should be
    'compact\_view=1' to show
-   `highlight_row` - filter option "Highlight whole row" (use problem
    color as background color for every problem row): should be '1' to
    highlight; can be set only when 'compact\_view' is set
-   `filter_name` - filter properties option "Name": freeform string
-   `filter_show_counter` - filter properties option "Show number of
    records": 1 - show, 0 - do not show
-   `filter_custom_time` - filter properties option "Set custom time
    period": 1 - set, 0 - do not set
-   `sort` - sort column: clock, host, severity, name
-   `sortorder` - sort order or results: DESC - descending, ASC -
    ascending
-   `age_state` - filter option "Age less than": should be
    'age\_state=1' to enable 'age'. Is used only when 'show' equals 3.
-   `age` - filter option "Age less than": days
-   `groupids` - filter option "Host groups": array of host groups IDs
-   `hostids` - filter option "Hosts": array of host IDs
-   `triggerids` - filter option "Triggers": array of trigger IDs
-   `show_timeline` - filter option "Show timeline": should be
    'show\_timeline=1' to show
-   `details` - filter option "Show details": should be 'details=1' to
    show
-   `from` - date range start, can be 'relative' (e.g.: now-1m). Is used
    only when 'filter\_custom\_time' equals 1.
-   `to` - date range end, can be 'relative' (e.g.: now-1m). Is used
    only when 'filter\_custom\_time' equals 1.

[comment]: # ({/new-46634552})

[comment]: # ({new-3af4f65f})
#### Kiosk mode

The kiosk mode in supported frontend pages can be activated using URL
parameters. For example, in dashboards:

-   `/zabbix.php?action=dashboard.view&kiosk=1` - activate kiosk mode
-   `/zabbix.php?action=dashboard.view&kiosk=0` - activate normal mode

[comment]: # ({/new-3af4f65f})

[comment]: # ({new-0cb5a83c})
#### Slideshow

It is possible to activate a slideshow in the dashboard:

-   `/zabbix.php?action=dashboard.view&slideshow=1` - activate slideshow

[comment]: # ({/new-0cb5a83c})
