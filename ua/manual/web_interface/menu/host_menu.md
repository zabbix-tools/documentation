[comment]: # translation:outdated

[comment]: # ({new-5bf78e6f})

# 1 Host menu

[comment]: # ({/new-5bf78e6f})

[comment]: # ({new-5b802363})

## Overview

The host menu is accessible by clicking on the host name in [supported locations](#supported-locations).

![](../../../../assets/en/manual/web_interface/host_menu.png)

[comment]: # ({/new-5b802363})

[comment]: # ({new-60febe87})

## Content

The host menu allows to:

- quickly switch to host inventory, latest data, problems, graphs, dashboards, web scenarios and host configuration.
  Note that host configuration is available for Admin and Super admin users only.
- execute [global scripts](/manual/web_interface/frontend_sections/alerts/scripts) configured for the current host.
  These scripts need to have their scope defined as 'Manual host action' to be available in the host menu.
- add custom links. To add a URL, you need to create a global
  [script](/manual/web_interface/frontend_sections/alerts/scripts) with scope 'Manual host action' and type 'URL'.
  Click on the script name to open the URL.

[comment]: # ({/new-60febe87})

[comment]: # ({new-3ad956d9})

## Supported locations

The host menu is accessible by clicking on a host in various frontend sections, for example:

- Dashboards [widgets](/manual/web_interface/frontend_sections/dashboards/widgets), such as Data overview, Trigger
  overview, Problems, etc.
- Monitoring → [Problems](/manual/web_interface/frontend_sections/monitoring/problems)
- Monitoring → [Problems](/manual/web_interface/frontend_sections/monitoring/problems) → Event details
- Monitoring → [Hosts](/manual/web_interface/frontend_sections/monitoring/hosts)
- Monitoring → Hosts → [Web Monitoring](/manual/web_interface/frontend_sections/monitoring/hosts/web)
- Monitoring → [Latest data](/manual/web_interface/frontend_sections/monitoring/latest_data)
- Monitoring → [Maps](/manual/web_interface/frontend_sections/monitoring/maps)
- Inventory → [Hosts](/manual/web_interface/frontend_sections/inventory/hosts)
- Reports → [Triggers top 100](/manual/web_interface/frontend_sections/reports/triggers_top)

[comment]: # ({/new-3ad956d9})
