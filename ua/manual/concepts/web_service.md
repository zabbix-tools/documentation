[comment]: # translation:outdated

[comment]: # ({new-8e572e45})
# 9 Web service

[comment]: # ({/new-8e572e45})

[comment]: # ({new-cac50ff6})
#### Overview

Zabbix web service is a process that is used for communication with
external web services. Currently, Zabbix web service is used for
generating and sending scheduled reports with plans to add additional
functionality in the future.

Zabbix server connects to the web service via HTTP(S). Zabbix web
service requires Google Chrome to be installed on the same host; on some
distributions the service may also work with Chromium (see [known
issues](/manual/installation/known_issues#chromium_for_zabbix_web_service_on_ubuntu_20))
.

[comment]: # ({/new-cac50ff6})

[comment]: # ({new-704910f2})
#### Installation

Zabbix web service is available in pre-compiled Zabbix packages
available for download at [Zabbix
website](http://www.zabbix.com/download.php). To compile [Zabbix web
service](/manual/installation/install#installing_zabbix_web_service)
from sources, specify the `--enable-webservice` configure option.

**See also:**

-   Configuration file options for
    [zabbix\_web\_service](/manual/appendix/config/zabbix_web_service);
-   [Setting up scheduled reports](/manual/appendix/install/web_service)

[comment]: # ({/new-704910f2})
