<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="ua" datatype="plaintext" original="manual/concepts/server.md">
    <body>
      <trans-unit id="cbb57ea2" xml:space="preserve">
        <source># 1 Server</source>
      </trans-unit>
      <trans-unit id="0e549571" xml:space="preserve">
        <source>#### Overview

Zabbix server is the central process of Zabbix software.

The server performs the polling and trapping of data, it calculates
triggers, sends notifications to users. It is the central component to
which Zabbix agents and proxies report data on availability and
integrity of systems. The server can itself remotely check networked
services (such as web servers and mail servers) using simple service
checks.

The server is the central repository in which all configuration,
statistical and operational data is stored, and it is the entity in
Zabbix that will actively alert administrators when problems arise in
any of the monitored systems.

The functioning of a basic Zabbix server is broken into three distinct
components; they are: Zabbix server, web frontend and database storage.

All of the configuration information for Zabbix is stored in the
database, which both the server and the web frontend interact with. For
example, when you create a new item using the web frontend (or API) it
is added to the items table in the database. Then, about once a minute
Zabbix server will query the items table for a list of the items which
are active that is then stored in a cache within the Zabbix server. This
is why it can take up to two minutes for any changes made in Zabbix
frontend to show up in the latest data section.</source>
      </trans-unit>
      <trans-unit id="c20247df" xml:space="preserve">
        <source>#### Running server</source>
      </trans-unit>
      <trans-unit id="1314fd6f" xml:space="preserve">
        <source>##### If installed as package

Zabbix server runs as a daemon process. The server can be started by
executing:

    service zabbix-server start

This will work on most of GNU/Linux systems. On other systems you may
need to run:

    /etc/init.d/zabbix-server start

Similarly, for stopping/restarting/viewing status, use the following
commands:

    service zabbix-server stop
    service zabbix-server restart
    service zabbix-server status</source>
      </trans-unit>
      <trans-unit id="a339702b" xml:space="preserve">
        <source>##### Runtime control

Runtime control options:

|Option|Description|Target|
|--|------|------|
|config\_cache\_reload|Reload configuration cache. Ignored if cache is being currently loaded.| |
|diaginfo\[=&lt;**section**&gt;\]|Gather diagnostic information in the server log file.|**historycache** - history cache statistics&lt;br&gt;**valuecache** - value cache statistics&lt;br&gt;**preprocessing** - preprocessing manager statistics&lt;br&gt;**alerting** - alert manager statistics&lt;br&gt;**lld** - LLD manager statistics&lt;br&gt;**locks** - list of mutexes (is empty on **BSD** systems)&lt;br&gt;**connector** - statistics for connectors with the largest queue|
|ha\_status|Log high availability (HA) cluster status.| |
|ha\_remove\_node=target|Remove the high availability (HA) node specified by its name or ID.&lt;br&gt;Note that active/standby nodes cannot be removed.|**target** - name or ID of the node (can be obtained by running ha\_status)|
|ha\_set\_failover\_delay=delay|Set high availability (HA) failover delay.&lt;br&gt;Time suffixes are supported, e.g. 10s, 1m.| |
|proxy\_config\_cache\_reload[=&lt;**target**&gt;]|Reload proxy configuration cache.|**target** - comma-delimited list of proxy names&lt;br&gt;If no target is specified, reload configuration for all proxies|
|secrets\_reload|Reload secrets from Vault.| |
|service\_cache\_reload|Reload the service manager cache.| |
|snmp\_cache\_reload|Reload SNMP cache, clear the SNMP properties (engine time, engine boots, engine id, credentials) for all hosts.| |
|housekeeper\_execute|Start the housekeeping procedure. Ignored if the housekeeping procedure is currently in progress.| |
|trigger\_housekeeper\_execute|Start the trigger housekeeping procedure. Ignored if the trigger housekeeping procedure is currently in progress.| |
|log\_level\_increase\[=&lt;**target**&gt;\]|Increase log level, affects all processes if target is not specified.&lt;br&gt;Not supported on *BSD* systems.|**process type** - All processes of specified type (e.g., poller)&lt;br&gt;See all [server process types](#server_process_types).&lt;br&gt;**process type,N** - Process type and number (e.g., poller,3)&lt;br&gt;**pid** - Process identifier (1 to 65535). For larger values specify target as 'process type,N'.|
|log\_level\_decrease\[=&lt;**target**&gt;\]|Decrease log level, affects all processes if target is not specified.&lt;br&gt;Not supported on *BSD* systems.|^|
|prof\_enable\[=&lt;**target**&gt;\]|Enable profiling.&lt;br&gt;Affects all processes if target is not specified.&lt;br&gt;Enabled profiling provides details of all rwlocks/mutexes by function name.|**process type** - All processes of specified type (e.g. history syncer)&lt;br&gt;Supported process types as profiling targets: alerter, alert manager, availability manager, configuration syncer, discovery manager, escalator, history poller, history syncer, housekeeper, http poller, icmp pinger, ipmi manager, ipmi poller, java poller, lld manager, lld worker, odbc poller, poller, preprocessing manager, preprocessing worker, proxy poller, self-monitoring, service manager, snmp trapper, task manager, timer, trapper, unreachable poller, vmware collector&lt;br&gt;**process type,N** - Process type and number (e.g., history syncer,1)&lt;br&gt;**pid** - Process identifier (1 to 65535). For larger values specify target as 'process type,N'.&lt;br&gt;**scope** - `rwlock`, `mutex`, `processing` can be used with the process type and number (e.g., history syncer,1,processing) or all processes of type (e.g., history syncer,rwlock)|
|prof\_disable\[=&lt;**target**&gt;\]|Disable profiling.&lt;br&gt;Affects all processes if target is not specified.|**process type** - All processes of specified type (e.g. history syncer)&lt;br&gt;Supported process types as profiling targets: see `prof_enable`&lt;br&gt;**process type,N** - Process type and number (e.g., history syncer,1)&lt;br&gt;**pid** - Process identifier (1 to 65535). For larger values specify target as 'process type,N'.|</source>
      </trans-unit>
      <trans-unit id="1cb7f51c" xml:space="preserve">
        <source>Example of using runtime control to reload the server configuration
cache:

    zabbix_server -c /usr/local/etc/zabbix_server.conf -R config_cache_reload

Examples of using runtime control to reload the proxy configuration:

    # Reload configuration of all proxies:
    zabbix_server -R proxy_config_cache_reload
    
    # Reload configuration of Proxy1 and Proxy2:
    zabbix_server -R proxy_config_cache_reload=Proxy1,Proxy2


Examples of using runtime control to gather diagnostic information:

    # Gather all available diagnostic information in the server log file:
    zabbix_server -R diaginfo

    # Gather history cache statistics in the server log file:
    zabbix_server -R diaginfo=historycache

Example of using runtime control to reload the SNMP cache:

    zabbix_server -R snmp_cache_reload  

Example of using runtime control to trigger execution of housekeeper:

    zabbix_server -c /usr/local/etc/zabbix_server.conf -R housekeeper_execute

Examples of using runtime control to change log level:

    # Increase log level of all processes:
    zabbix_server -c /usr/local/etc/zabbix_server.conf -R log_level_increase

    # Increase log level of second poller process:
    zabbix_server -c /usr/local/etc/zabbix_server.conf -R log_level_increase=poller,2

    # Increase log level of process with PID 1234:
    zabbix_server -c /usr/local/etc/zabbix_server.conf -R log_level_increase=1234

    # Decrease log level of all http poller processes:
    zabbix_server -c /usr/local/etc/zabbix_server.conf -R log_level_decrease="http poller"

Example of setting the HA failover delay to the minimum of 10 seconds:

    zabbix_server -R ha_set_failover_delay=10s</source>
      </trans-unit>
      <trans-unit id="b4f10179" xml:space="preserve">
        <source>##### Process user

Zabbix server is designed to run as a non-root user. It will run as
whatever non-root user it is started as. So you can run server as any
non-root user without any issues.

If you will try to run it as 'root', it will switch to a hardcoded
'zabbix' user, which must be [present](/manual/installation/install) on
your system. You can only run server as 'root' if you modify the
'AllowRoot' parameter in the server configuration file accordingly.

If Zabbix server and [agent](agent) are run on the same machine it is
recommended to use a different user for running the server than for
running the agent. Otherwise, if both are run as the same user, the
agent can access the server configuration file and any Admin level user
in Zabbix can quite easily retrieve, for example, the database password.</source>
      </trans-unit>
      <trans-unit id="0a81f475" xml:space="preserve">
        <source>##### Configuration file

See the [configuration file](/manual/appendix/config/zabbix_server)
options for details on configuring zabbix\_server.</source>
      </trans-unit>
      <trans-unit id="49247ffc" xml:space="preserve">
        <source>##### Start-up scripts

The scripts are used to automatically start/stop Zabbix processes during
system's start-up/shutdown. The scripts are located under directory
misc/init.d.</source>
      </trans-unit>
      <trans-unit id="9f05badb" xml:space="preserve">
        <source>#### Server process types

-   `alert manager` - alert queue manager
-   `alert syncer` - alert DB writer
-   `alerter` - process for sending notifications
-   `availability manager` - process for host availability updates
-   `configuration syncer` - process for managing in-memory cache of
    configuration data
-   `connector manager` - manager process for connectors
-   `connector worker` - process for handling requests from the connector manager
-   `discovery manager` - manager process for discovery of devices
-   `discovery worker` - process for handling discovery tasks from the discovery manager
-   `escalator` - process for escalation of actions
-   `ha manager` - process for managing high availability
-   `history poller` - process for handling calculated checks requiring a database connection
-   `history syncer` - history DB writer
-   `housekeeper` - process for removal of old historical data
-   `http poller` - web monitoring poller
-   `icmp pinger` - poller for icmpping checks
-   `ipmi manager` - IPMI poller manager
-   `ipmi poller` - poller for IPMI checks
-   `java poller` - poller for Java checks
-   `lld manager` - manager process of low-level discovery tasks
-   `lld worker` - worker process of low-level discovery tasks
-   `odbc poller` - poller for ODBC checks
-   `poller` - normal poller for passive checks
-   `preprocessing manager` - manager of preprocessing tasks
-   `preprocessing worker` - process for data preprocessing
-   `proxy poller` - poller for passive proxies
-   `report manager`- manager of scheduled report generation tasks
-   `report writer` - process for generating scheduled reports
-   `self-monitoring` - process for collecting internal server
    statistics
-   `service manager` - process for managing services by receiving information
    about problems, problem tags, and problem recovery from history syncer, task manager, and alert manager
-   `snmp trapper` - trapper for SNMP traps
-   `task manager` - process for remote execution of tasks requested by
    other components (e.g., close problem, acknowledge problem, check
    item value now, remote command functionality)
-   `timer` - timer for processing maintenances
-   `trapper` - trapper for active checks, traps, proxy communication
-   `trigger housekeeper` - process for removing problems generated by triggers that have been deleted
-   `unreachable poller` - poller for unreachable devices
-   `vmware collector` - VMware data collector responsible for data
    gathering from VMware services

The server log file can be used to observe these process types.

Various types of Zabbix server processes can be monitored using the
**zabbix\[process,&lt;type&gt;,&lt;mode&gt;,&lt;state&gt;\]** internal
[item](/manual/config/items/itemtypes/internal).</source>
      </trans-unit>
      <trans-unit id="cdd68340" xml:space="preserve">
        <source>#### Supported platforms

Due to the security requirements and mission-critical nature of server
operation, UNIX is the only operating system that can consistently
deliver the necessary performance, fault tolerance and resilience.
Zabbix operates on market leading versions.

Zabbix server is tested on the following platforms:

-   Linux
-   Solaris
-   AIX
-   HP-UX
-   Mac OS X
-   FreeBSD
-   OpenBSD
-   NetBSD
-   SCO Open Server
-   Tru64/OSF1

::: noteclassic
Zabbix may work on other Unix-like operating systems as
well.
:::</source>
      </trans-unit>
      <trans-unit id="982e2546" xml:space="preserve">
        <source>#### Locale

Note that the server requires a UTF-8 locale so that some textual items
can be interpreted correctly. Most modern Unix-like systems have a UTF-8
locale as default, however, there are some systems where that may need
to be set specifically.</source>
      </trans-unit>
      <trans-unit id="bbb4c5d8" xml:space="preserve">
        <source>##### Start up manually

If the above does not work you have to start it manually. Find the path
to the zabbix\_server binary and execute:

    zabbix_server

You can use the following command line parameters with Zabbix server:

    -c --config &lt;file&gt;              path to the configuration file (default is /usr/local/etc/zabbix_server.conf)
    -f --foreground                 run Zabbix server in foreground
    -R --runtime-control &lt;option&gt;   perform administrative functions
    -h --help                       give this help
    -V --version                    display version number

Examples of running Zabbix server with command line parameters:

    zabbix_server -c /usr/local/etc/zabbix_server.conf
    zabbix_server --help
    zabbix_server -V</source>
      </trans-unit>
    </body>
  </file>
</xliff>
