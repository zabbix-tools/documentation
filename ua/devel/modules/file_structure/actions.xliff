<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="ua" datatype="plaintext" original="devel/modules/file_structure/actions.md">
    <body>
      <trans-unit id="e31d2e06" xml:space="preserve">
        <source># Actions

Actions are responsible for 'business logic' of the module. An action usually consists of a [controller](#controller) and an [action view](views#action-view). 

A module can: 

- Call actions that are already defined in Zabbix frontend.
- Override default actions with custom actions.
- Define completely new actions.

To override a default action behavior with some custom behavior, define an action with the same name in the module configuration. 
When the action is called, the module action will be executed instead of the default Zabbix action. 

Action files should be stored in the *actions* folder. The actions need to be specified in the [manifest.json](manifest).</source>
      </trans-unit>
      <trans-unit id="348a3343" xml:space="preserve">
        <source>### Controller

Action controller workflow:

1) Check that all parameters passed in an HTTP request are valid:
- Call the controller's *checkInput()* method
- Use validation rules defined in CNewValidator.php
- Call *validateInput()* method

2) Check user permissions.

3) Prepare the data according to passed parameters: if *checkInput()* returns true, Zabbix calls the controller's *doAction()* method.

4) Prepare the **$data** array for the view. Use *CControllerResponseData* and *setResponse()* method to store response in the **$data** array.

Example:

````php
/**
 * Validate input parameters.
 *
 * @return bool
 */
protected function checkInput(): bool {
    $ret = $this-&gt;validateInput([
        'status' =&gt; 'in '.implode(',', [HOST_STATUS_MONITORED, HOST_STATUS_NOT_MONITORED])
    ]);

    if (!$ret) {
        $this-&gt;setResponse(new CControllerResponseFatal());
    }

    return $ret;
}

/**
 * Check user permissions.
 *
 * @return bool
 */
protected function checkPermissions() {
    return $this-&gt;getUserType() &gt;= USER_TYPE_ZABBIX_ADMIN;
}

/**
 * Execute action and generate response object.
 */
protected function do Action(): void {
    $data = [ 
        'hosts_count' =&gt; API::Host()-&gt;get([
            'countOutput' =&gt; true,
            'filter' =&gt; [
                'status' =&gt; $this-&gt;getInput('status')
            ]
        ]) 
    ];
    
    $this-&gt;setResponse(new CControllerResponseData($data));
}
````

::: notetip
You can view the full list of available controller classes in Zabbix [source code](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/ui/app/controllers).
:::</source>
      </trans-unit>
    </body>
  </file>
</xliff>
