[comment]: # translation:outdated

[comment]: # ({new-dabb0bd5})
# zabbix\_get

Section: User Commands (1)\
Updated: 2021-06-01\
[Index](#index) [Return to Main Contents](/documentation/6.0/manpages)

------------------------------------------------------------------------

[ ]{#lbAB}

[comment]: # ({/new-dabb0bd5})

[comment]: # ({new-0ba1ce53})
## NAME

zabbix\_get - Zabbix get utility [ ]{#lbAC}

[comment]: # ({/new-0ba1ce53})

[comment]: # ({new-60e7e93b})
## SYNOPSIS

**zabbix\_get -s** *host-name-or-IP* \[**-p** *port-number*\] \[**-I**
*IP-address*\] \[**-t** *timeout*\] **-k** *item-key*\
**zabbix\_get -s** *host-name-or-IP* \[**-p** *port-number*\] \[**-I**
*IP-address*\] \[**-t** *timeout*\] **--tls-connect** **cert**
**--tls-ca-file** *CA-file* \[**--tls-crl-file** *CRL-file*\]
\[**--tls-agent-cert-issuer** *cert-issuer*\]
\[**--tls-agent-cert-subject** *cert-subject*\] **--tls-cert-file**
*cert-file* **--tls-key-file** *key-file* \[**--tls-cipher13**
*cipher-string*\] \[**--tls-cipher** *cipher-string*\] **-k**
*item-key*\
**zabbix\_get -s** *host-name-or-IP* \[**-p** *port-number*\] \[**-I**
*IP-address*\] \[**-t** *timeout*\] **--tls-connect** **psk**
**--tls-psk-identity** *PSK-identity* **--tls-psk-file** *PSK-file*
\[**--tls-cipher13** *cipher-string*\] \[**--tls-cipher**
*cipher-string*\] **-k** *item-key*\
**zabbix\_get -h**\
**zabbix\_get -V** [ ]{#lbAD}

[comment]: # ({/new-60e7e93b})

[comment]: # ({new-da08bc1a})
## DESCRIPTION

**zabbix\_get** is a command line utility for getting data from Zabbix
agent. [ ]{#lbAE}

[comment]: # ({/new-da08bc1a})

[comment]: # ({new-4ab1a759})
## OPTIONS

**-s**, **--host** *host-name-or-IP*  
Specify host name or IP address of a host.

**-p**, **--port** *port-number*  
Specify port number of agent running on the host. Default is 10050.

**-I**, **--source-address** *IP-address*  
Specify source IP address.

**-t**, **--timeout** *seconds*  
Specify timeout. Valid range: 1-30 seconds (default: 30)

**-k**, **--key** *item-key*  
Specify key of item to retrieve value for.

**--tls-connect** *value*  
How to connect to agent. Values:

[ ]{#lbAF}

[comment]: # ({/new-4ab1a759})

[comment]: # ({new-465bc4c8})
### 

  
**unencrypted**  
connect without encryption (default)

```{=html}
<!-- -->
```
  
**psk**  
connect using TLS and a pre-shared key

```{=html}
<!-- -->
```
  
**cert**  
connect using TLS and a certificate

```{=html}
<!-- -->
```
**--tls-ca-file** *CA-file*  
Full pathname of a file containing the top-level CA(s) certificates for
peer certificate verification.

**--tls-crl-file** *CRL-file*  
Full pathname of a file containing revoked certificates.

**--tls-agent-cert-issuer** *cert-issuer*  
Allowed agent certificate issuer.

**--tls-agent-cert-subject** *cert-subject*  
Allowed agent certificate subject.

**--tls-cert-file** *cert-file*  
Full pathname of a file containing the certificate or certificate chain.

**--tls-key-file** *key-file*  
Full pathname of a file containing the private key.

**--tls-psk-identity** *PSK-identity*  
PSK-identity string.

**--tls-psk-file** *PSK-file*  
Full pathname of a file containing the pre-shared key.

**--tls-cipher13** *cipher-string*  
Cipher string for OpenSSL 1.1.1 or newer for TLS 1.3. Override the
default ciphersuite selection criteria. This option is not available if
OpenSSL version is less than 1.1.1.

**--tls-cipher** *cipher-string*  
GnuTLS priority string (for TLS 1.2 and up) or OpenSSL cipher string
(only for TLS 1.2). Override the default ciphersuite selection criteria.

**-h**, **--help**  
Display this help and exit.

**-V**, **--version**  
Output version information and exit.

[ ]{#lbAG}

[comment]: # ({/new-465bc4c8})

[comment]: # ({new-c5e4d00b})
## EXAMPLES

**zabbix\_get -s 127.0.0.1 -p 10050 -k "system.cpu.load\[all,avg1\]"**\
**zabbix\_get -s 127.0.0.1 -p 10050 -k "system.cpu.load\[all,avg1\]"
--tls-connect cert --tls-ca-file /home/zabbix/zabbix\_ca\_file
--tls-agent-cert-issuer "CN=Signing CA,OU=IT operations,O=Example
Corp,DC=example,DC=com" --tls-agent-cert-subject "CN=server1,OU=IT
operations,O=Example Corp,DC=example,DC=com" --tls-cert-file
/home/zabbix/zabbix\_get.crt --tls-key-file
/home/zabbix/zabbix\_get.key\
zabbix\_get -s 127.0.0.1 -p 10050 -k "system.cpu.load\[all,avg1\]"
--tls-connect psk --tls-psk-identity "PSK ID Zabbix agentd"
--tls-psk-file /home/zabbix/zabbix\_agentd.psk** [ ]{#lbAH}

[comment]: # ({/new-c5e4d00b})

[comment]: # ({new-0ed7cca7})
## SEE ALSO

Documentation <https://www.zabbix.com/manuals>

**[zabbix\_agentd](zabbix_agentd)**(8),
**[zabbix\_proxy](zabbix_proxy)**(8),
**[zabbix\_sender](zabbix_sender)**(1),
**[zabbix\_server](zabbix_server)**(8), **[zabbix\_js](zabbix_js)**(1),
**[zabbix\_agent2](zabbix_agent2)**(8),
**[zabbix\_web\_service](zabbix_web_service)**(8) [ ]{#lbAI}

[comment]: # ({/new-0ed7cca7})

[comment]: # ({new-aaed5529})
## AUTHOR

Alexei Vladishev <[[\[email protected\]]{.__cf_email__
cfemail="254449405d655f4447474c5d0b464a48"}](/cdn-cgi/l/email-protection#eb8a878e93ab918a89898293c5888486)>

------------------------------------------------------------------------

[ ]{#index}

[comment]: # ({/new-aaed5529})

[comment]: # ({new-bdb17437})
## Index

[NAME](#lbAB)

[SYNOPSIS](#lbAC)

[DESCRIPTION](#lbAD)

[OPTIONS](#lbAE)

[](#lbAF)

  

[EXAMPLES](#lbAG)

[SEE ALSO](#lbAH)

[AUTHOR](#lbAI)

------------------------------------------------------------------------

This document was created by [man2html](/cgi-bin/man/man2html), using
the manual pages.\
Time: 08:42:29 GMT, June 11, 2021

[comment]: # ({/new-bdb17437})
