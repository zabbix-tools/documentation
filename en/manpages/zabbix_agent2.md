[comment]: # ({d8f864a5-9beb7cfa})
# zabbix\_agent2

Section: Maintenance Commands (8)\
Updated: 2019-01-29\
[Index](#index) [Return to Main Contents](/manpages)

------------------------------------------------------------------------

[ ]{#lbAB}

[comment]: # ({/d8f864a5-9beb7cfa})

[comment]: # ({942e0d82-942e0d82})
## NAME

zabbix\_agent2 - Zabbix agent 2\
\
[ ]{#lbAC}

[comment]: # ({/942e0d82-942e0d82})

[comment]: # ({5535a811-5535a811})
## SYNOPSIS

**zabbix\_agent2** \[**-c** *config-file*\]\
**zabbix\_agent2** \[**-c** *config-file*\] **-p**\
**zabbix\_agent2** \[**-c** *config-file*\] **-t** *item-key*\
**zabbix\_agent2** \[**-c** *config-file*\] **-R** *runtime-option*\
**zabbix\_agent2 -h**\
**zabbix\_agent2 -V**\
\
[ ]{#lbAD}

[comment]: # ({/5535a811-5535a811})

[comment]: # ({5b789849-5b789849})
## DESCRIPTION

**zabbix\_agent2** is an application for monitoring parameters of
various services.\
\
[ ]{#lbAE}

[comment]: # ({/5b789849-5b789849})

[comment]: # ({7197fda6-7197fda6})
## OPTIONS

**-c**, **--config** *config-file*  
Use the alternate *config-file* instead of the default one.

**-R**, **--runtime-control** *runtime-option*  
Perform administrative functions according to *runtime-option*.

[comment]: # ({/7197fda6-7197fda6})

[comment]: # ({c754f1e2-c754f1e2})
### 

  

[comment]: # ({/c754f1e2-c754f1e2})

[comment]: # ({424c39c0-c730d427})
#### Runtime control options:

  
**userparameter reload**  
Reload values of the *UserParameter* and *Include* options from the current configuration file

  
**loglevel increase**  
Increase log level

  
**loglevel decrease**  
Decrease log level

  
**help**  
List available runtime control options

  
**metrics**  
List available metrics

  
**version**  
Display version

```{=html}
<!-- -->
```
**-p**, **--print**  
Print known items and exit. For each item either generic defaults are
used, or specific defaults for testing are supplied. These defaults are
listed in square brackets as item key parameters. Returned values are
enclosed in square brackets and prefixed with the type of the returned
value, separated by a pipe character. For user parameters type is always
**t**, as the agent can not determine all possible return values. Items,
displayed as working, are not guaranteed to work from the Zabbix server
or zabbix\_get when querying a running agent daemon as permissions or
environment may be different. Returned value types are:

  
d  
Number with a decimal part.

  
m  
Not supported. This could be caused by querying an item that only works
in the active mode like a log monitoring item or an item that requires
multiple collected values. Permission issues or incorrect user
parameters could also result in the not supported state.

  
s  
Text. Maximum length not limited.

  
t  
Text. Same as **s**.

  
u  
Unsigned integer.

\

**-t**, **--test** *item-key*  
Test single item and exit. See **--print** for output description.

**-h**, **--help**  
Display this help and exit.

**-V**, **--version**  
Output version information and exit.

[ ]{#lbAG}

[comment]: # ({/424c39c0-c730d427})

[comment]: # ({37997f11-37997f11})
## FILES

*/usr/local/etc/zabbix\_agent2.conf*  
Default location of Zabbix agent 2 configuration file (if not modified
during compile time).

[ ]{#lbAH}

[comment]: # ({/37997f11-37997f11})

[comment]: # ({ad1bba15-ad1bba15})
## SEE ALSO

Documentation <https://www.zabbix.com/manuals>

**[zabbix\_agentd](zabbix_agentd)**(8),
**[zabbix\_get](zabbix_get)**(8), **[zabbix\_js](zabbix_js)**(8),
**[zabbix\_proxy](zabbix_proxy)**(8),
**[zabbix\_sender](zabbix_sender)**(8),
**[zabbix\_server](zabbix_server)**(8) [ ]{#lbAI}

[comment]: # ({/ad1bba15-ad1bba15})

[comment]: # ({96162c7c-96162c7c})
## AUTHOR

Zabbix LLC

------------------------------------------------------------------------

[ ]{#index}

[comment]: # ({/96162c7c-96162c7c})

[comment]: # ({fdcb7ffe-8d9d9852})
## Index

[NAME](#lbAB)

[SYNOPSIS](#lbAC)

[DESCRIPTION](#lbAD)

[OPTIONS](#lbAE)

[](#lbAF)

  

[FILES](#lbAG)

[SEE ALSO](#lbAH)

[AUTHOR](#lbAI)

------------------------------------------------------------------------

This document was created on: 14:07:57 GMT, November 22, 2021

[comment]: # ({/fdcb7ffe-8d9d9852})
