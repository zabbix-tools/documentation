[comment]: # ({cafabed0-2c290a27})
# zabbix\_agentd

Section: Maintenance Commands (8)\
Updated: 2019-01-29\
[Index](#index) [Return to Main Contents](/manpages)

------------------------------------------------------------------------

[ ]{#lbAB}

[comment]: # ({/cafabed0-2c290a27})

[comment]: # ({75de926e-75de926e})
## NAME

zabbix\_agentd - Zabbix agent daemon [ ]{#lbAC}

[comment]: # ({/75de926e-75de926e})

[comment]: # ({b8c27067-b8c27067})
## SYNOPSIS

**zabbix\_agentd** \[**-c** *config-file*\]\
**zabbix\_agentd** \[**-c** *config-file*\] **-p**\
**zabbix\_agentd** \[**-c** *config-file*\] **-t** *item-key*\
**zabbix\_agentd** \[**-c** *config-file*\] **-R** *runtime-option*\
**zabbix\_agentd -h**\
**zabbix\_agentd -V** [ ]{#lbAD}

[comment]: # ({/b8c27067-b8c27067})

[comment]: # ({ecdda52c-ecdda52c})
## DESCRIPTION

**zabbix\_agentd** is a daemon for monitoring various server parameters.
[ ]{#lbAE}

[comment]: # ({/ecdda52c-ecdda52c})

[comment]: # ({63567f56-63567f56})
## OPTIONS

**-c**, **--config** *config-file*  
Use the alternate *config-file* instead of the default one.

**-f**, **--foreground**  
Run Zabbix agent in foreground.

**-R**, **--runtime-control** *runtime-option*  
Perform administrative functions according to *runtime-option*.

[ ]{#lbAF}

[comment]: # ({/63567f56-63567f56})

[comment]: # ({943ad149-5333e0f4})
### 

  
#### Runtime control options

  
**userparameter\_reload**\[=*target*\]  
Reload values of the *UserParameter* and *Include* options from the current configuration file

  
**log\_level\_increase**\[=*target*\]  
Increase log level, affects all processes if target is not specified

  
**log\_level\_decrease**\[=*target*\]  
Decrease log level, affects all processes if target is not specified

[ ]{#lbAG}

[comment]: # ({/943ad149-5333e0f4})

[comment]: # ({199def5e-199def5e})
### 

  
Log level control targets

  
*process-type*  
All processes of specified type (active checks, collector, listener)

  
*process-type,N*  
Process type and number (e.g., listener,3)

  
*pid*  
Process identifier, up to 65535. For larger values specify target as
"process-type,N"

```{=html}
<!-- -->
```
**-p**, **--print**  
Print known items and exit. For each item either generic defaults are
used, or specific defaults for testing are supplied. These defaults are
listed in square brackets as item key parameters. Returned values are
enclosed in square brackets and prefixed with the type of the returned
value, separated by a pipe character. For user parameters type is always
**t**, as the agent can not determine all possible return values. Items,
displayed as working, are not guaranteed to work from the Zabbix server
or zabbix\_get when querying a running agent daemon as permissions or
environment may be different. Returned value types are:

  
d  
Number with a decimal part.

  
m  
Not supported. This could be caused by querying an item that only works
in the active mode like a log monitoring item or an item that requires
multiple collected values. Permission issues or incorrect user
parameters could also result in the not supported state.

  
s  
Text. Maximum length not limited.

  
t  
Text. Same as **s**.

  
u  
Unsigned integer.

**-t**, **--test** *item-key*  
Test single item and exit. See **--print** for output description.

**-h**, **--help**  
Display this help and exit.

**-V**, **--version**  
Output version information and exit.

[ ]{#lbAH}

[comment]: # ({/199def5e-199def5e})

[comment]: # ({fcfd4796-fcfd4796})
## FILES

*/usr/local/etc/zabbix\_agentd.conf*  
Default location of Zabbix agent configuration file (if not modified
during compile time).

[ ]{#lbAI}

[comment]: # ({/fcfd4796-fcfd4796})

[comment]: # ({539868a7-539868a7})
## SEE ALSO

Documentation <https://www.zabbix.com/manuals>

**[zabbix\_agent2](zabbix_agent2)**(8),
**[zabbix\_get](zabbix_get)**(1), **[zabbix\_js](zabbix_js)**(1),
**[zabbix\_proxy](zabbix_proxy)**(8),
**[zabbix\_sender](zabbix_sender)**(1),
**[zabbix\_server](zabbix_server)**(8) [ ]{#lbAJ}

[comment]: # ({/539868a7-539868a7})

[comment]: # ({e55ed07e-e55ed07e})
## AUTHOR

Alexei Vladishev <<alex@zabbix.com>>

------------------------------------------------------------------------

[ ]{#index}

[comment]: # ({/e55ed07e-e55ed07e})

[comment]: # ({b1144691-6ea4e50b})
## Index

[NAME](#lbAB)

[SYNOPSIS](#lbAC)

[DESCRIPTION](#lbAD)

[OPTIONS](#lbAE)

[](#lbAF)

[](#lbAG)

  

[FILES](#lbAH)

[SEE ALSO](#lbAI)

[AUTHOR](#lbAJ)

------------------------------------------------------------------------

This document was created on: 20:50:13 GMT, November 22, 2021

[comment]: # ({/b1144691-6ea4e50b})
