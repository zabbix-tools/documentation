[comment]: # ({be024674-50528bfc})
# zabbix\_js

Section: User Commands (1)\
Updated: 2019-01-29\
[Index](#index) [Return to Main Contents](/manpages)

------------------------------------------------------------------------

[ ]{#lbAB}

[comment]: # ({/be024674-50528bfc})

[comment]: # ({42249a0d-42249a0d})
## NAME

zabbix\_js - Zabbix JS utility [ ]{#lbAC}

[comment]: # ({/42249a0d-42249a0d})

[comment]: # ({8c5d99cc-8c5d99cc})
## SYNOPSIS

**zabbix\_js -s** *script-file* **-p** *input-param* \[**-l**
*log-level*\] \[**-t** *timeout*\]\
**zabbix\_js -s** *script-file* **-i** *input-file* \[**-l**
*log-level*\] \[**-t** *timeout*\]\
**zabbix\_js -h**\
**zabbix\_js -V** [ ]{#lbAD}

[comment]: # ({/8c5d99cc-8c5d99cc})

[comment]: # ({b7046c11-b7046c11})
## DESCRIPTION

**zabbix\_js** is a command line utility that can be used for embedded
script testing. [ ]{#lbAE}

[comment]: # ({/b7046c11-b7046c11})

[comment]: # ({912215c0-02064894})
## OPTIONS

**-s**, **--script** *script-file*  
Specify the file name of the script to execute. If '-' is specified as
file name, the script will be read from stdin.

**-p**, **--param** *input-param*  
Specify the input parameter.

**-i**, **--input** *input-file*  
Specify the file name of the input parameter. If '-' is specified as
file name, the input will be read from stdin.

**-l**, **--loglevel** *log-level*  
Specify the log level.

**-t**, **--timeout** *timeout*  
Specify the timeout in seconds. Valid range: 1\-60 seconds (default: 10)

**-h**, **--help**  
Display this help and exit.

**-V**, **--version**  
Output version information and exit.

[ ]{#lbAF}

[comment]: # ({/912215c0-02064894})

[comment]: # ({74bea5ed-74bea5ed})
## EXAMPLES

**zabbix\_js -s script-file.js -p example** [ ]{#lbAG}

[comment]: # ({/74bea5ed-74bea5ed})

[comment]: # ({f909573a-f909573a})
## SEE ALSO

Documentation <https://www.zabbix.com/manuals>

**[zabbix\_agent2](zabbix_agent2)**(8),
**[zabbix\_agentd](zabbix_agentd)**(8),
**[zabbix\_get](zabbix_get)**(1), **[zabbix\_proxy](zabbix_proxy)**(8),
**[zabbix\_sender](zabbix_sender)**(1),
**[zabbix\_server](zabbix_server)**(8)

------------------------------------------------------------------------

[ ]{#index}

[comment]: # ({/f909573a-f909573a})

[comment]: # ({b43e383d-af76d2f6})
## Index

[NAME](#lbAB)  

[SYNOPSIS](#lbAC)  

[DESCRIPTION](#lbAD)  

[OPTIONS](#lbAE)  

[EXAMPLES](#lbAF)  

[SEE ALSO](#lbAG)  

------------------------------------------------------------------------

This document was created on: 21:23:35 GMT, March 18, 2020

[comment]: # ({/b43e383d-af76d2f6})
