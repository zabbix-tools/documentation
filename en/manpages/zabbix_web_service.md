[comment]: # ({d73d9ea7-d1b7141f})
# zabbix\_web\_service

Section: Maintenance Commands (8)\
Updated: 2019-01-29\
[Index](#index) [Return to Main Contents](/manpages)

------------------------------------------------------------------------

[ ]{#lbAB}

[comment]: # ({/d73d9ea7-d1b7141f})

[comment]: # ({6319fb62-6319fb62})
## NAME

zabbix\_web\_service - Zabbix web service [ ]{#lbAC}

[comment]: # ({/6319fb62-6319fb62})

[comment]: # ({93a3222a-93a3222a})
## SYNOPSIS

**zabbix\_web\_service** \[**-c** *config-file*\]\
**zabbix\_web\_service -h**\
**zabbix\_web\_service -V** [ ]{#lbAD}

[comment]: # ({/93a3222a-93a3222a})

[comment]: # ({c1ed92af-c1ed92af})
## DESCRIPTION

**zabbix\_web\_service** is an application for providing web services to
Zabbix components. [ ]{#lbAE}

[comment]: # ({/c1ed92af-c1ed92af})

[comment]: # ({98c2799b-98c2799b})
## OPTIONS

**-c**, **--config** *config-file*  
Use the alternate *config-file* instead of the default one.

**-h**, **--help**  
Display this help and exit.

**-V**, **--version**  
Output version information and exit.

[ ]{#lbAF}

[comment]: # ({/98c2799b-98c2799b})

[comment]: # ({440875a7-440875a7})
## FILES

*/usr/local/etc/zabbix\_web\_service.conf*  
Default location of Zabbix web service configuration file (if not
modified during compile time).

[ ]{#lbAG}

[comment]: # ({/440875a7-440875a7})

[comment]: # ({4f67430a-4f67430a})
## SEE ALSO

Documentation <https://www.zabbix.com/manuals>

**[zabbix\_agentd](zabbix_agentd)**(8),
**[zabbix\_get](zabbix_get)**(1), **[zabbix\_proxy](zabbix_proxy)**(8),
**[zabbix\_sender](zabbix_sender)**(1),
**[zabbix\_server](zabbix_server)**(8), **[zabbix\_js](zabbix_js)**(1),
**[zabbix\_agent2](zabbix_agent2)**(8) [ ]{#lbAH}

[comment]: # ({/4f67430a-4f67430a})

[comment]: # ({96162c7c-96162c7c})
## AUTHOR

Zabbix LLC

------------------------------------------------------------------------

[ ]{#index}

[comment]: # ({/96162c7c-96162c7c})

[comment]: # ({d5908c14-ea1c0f43})
## Index

[NAME](#lbAB)  

[SYNOPSIS](#lbAC)  

[DESCRIPTION](#lbAD)  

[OPTIONS](#lbAE)  

[FILES](#lbAF)  

[SEE ALSO](#lbAG)  

[AUTHOR](#lbAH)  

------------------------------------------------------------------------

This document was created on: 12:58:30 GMT, June 11, 2021

[comment]: # ({/d5908c14-ea1c0f43})
