[comment]: # ({eb3eba33-e0cabf53})
# zabbix\_proxy

Section: Maintenance Commands (8)\
Updated: 2020-09-04\
[Index](#index) [Return to Main Contents](/manpages)

------------------------------------------------------------------------

[ ]{#lbAB}

[comment]: # ({/eb3eba33-e0cabf53})

[comment]: # ({c7ba44c5-c7ba44c5})
## NAME

zabbix\_proxy - Zabbix proxy daemon [ ]{#lbAC}

[comment]: # ({/c7ba44c5-c7ba44c5})

[comment]: # ({6590818d-6590818d})
## SYNOPSIS

**zabbix\_proxy** \[**-c** *config-file*\]\
**zabbix\_proxy** \[**-c** *config-file*\] **-R** *runtime-option*\
**zabbix\_proxy -h**\
**zabbix\_proxy -V** [ ]{#lbAD}

[comment]: # ({/6590818d-6590818d})

[comment]: # ({5c6b9587-5c6b9587})
## DESCRIPTION

**zabbix\_proxy** is a daemon that collects monitoring data from devices
and sends it to Zabbix server. [ ]{#lbAE}

[comment]: # ({/5c6b9587-5c6b9587})

[comment]: # ({daee5c55-daee5c55})
## OPTIONS

**-c**, **--config** *config-file*  
Use the alternate *config-file* instead of the default one.

**-f**, **--foreground**  
Run Zabbix proxy in foreground.

**-R**, **--runtime-control** *runtime-option*  
Perform administrative functions according to *runtime-option*.

[ ]{#lbAF}

[comment]: # ({/daee5c55-daee5c55})

[comment]: # ({045894c6-045894c6})
### 

  
Runtime control options

  
**config\_cache\_reload**  
Reload configuration cache. Ignored if cache is being currently loaded.
Active Zabbix proxy will connect to the Zabbix server and request
configuration data. Default configuration file (unless **-c** option is
specified) will be used to find PID file and signal will be sent to
process, listed in PID file.

  
**snmp\_cache\_reload**  
Reload SNMP cache.

  
**housekeeper\_execute**  
Execute the housekeeper. Ignored if housekeeper is being currently
executed.

  
**diaginfo**\[=*section*\]  
Log internal diagnostic information of the specified section. Section
can be *historycache*, *preprocessing*. By default diagnostic
information of all sections is logged.

  
**log\_level\_increase**\[=*target*\]  
Increase log level, affects all processes if target is not specified.

  
**log\_level\_decrease**\[=*target*\]  
Decrease log level, affects all processes if target is not specified.

[ ]{#lbAG}

[comment]: # ({/045894c6-045894c6})

[comment]: # ({89a1f628-b863520e})
### 

  
Log level control targets

  
*process-type*  
All processes of specified type (configuration syncer, data sender,
discoverer, history syncer, housekeeper, http poller,
icmp pinger, ipmi manager, ipmi poller, java poller, poller,
self-monitoring, snmp trapper, task manager, trapper,
unreachable poller, vmware collector)

  
*process-type,N*  
Process type and number (e.g., poller,3)

  
*pid*  
Process identifier, up to 65535. For larger values specify target as
"process-type,N"

```{=html}
<!-- -->
```
**-h**, **--help**  
Display this help and exit.

**-V**, **--version**  
Output version information and exit.

[ ]{#lbAH}

[comment]: # ({/89a1f628-b863520e})

[comment]: # ({583a1725-583a1725})
## FILES

*/usr/local/etc/zabbix\_proxy.conf*  
Default location of Zabbix proxy configuration file (if not modified
during compile time).

[ ]{#lbAI}

[comment]: # ({/583a1725-583a1725})

[comment]: # ({41feb9bb-41feb9bb})
## SEE ALSO

Documentation <https://www.zabbix.com/manuals>

**[zabbix\_agentd](zabbix_agentd)**(8),
**[zabbix\_get](zabbix_get)**(1),
**[zabbix\_sender](zabbix_sender)**(1),
**[zabbix\_server](zabbix_server)**(8), **[zabbix\_js](zabbix_js)**(1),
**[zabbix\_agent2](zabbix_agent2)**(8) [ ]{#lbAJ}

[comment]: # ({/41feb9bb-41feb9bb})

[comment]: # ({e55ed07e-e55ed07e})
## AUTHOR

Alexei Vladishev <<alex@zabbix.com>>

------------------------------------------------------------------------

[ ]{#index}

[comment]: # ({/e55ed07e-e55ed07e})

[comment]: # ({454812dd-9f3a9d2a})
## Index

[NAME](#lbAB)

[SYNOPSIS](#lbAC)

[DESCRIPTION](#lbAD)

[OPTIONS](#lbAE)

[](#lbAF)

[](#lbAG)

  

[FILES](#lbAH)

[SEE ALSO](#lbAI)

[AUTHOR](#lbAJ)

------------------------------------------------------------------------

This document was created on: 16:12:22 GMT, September 04, 2020

[comment]: # ({/454812dd-9f3a9d2a})
