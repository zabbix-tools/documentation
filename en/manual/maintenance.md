[comment]: # ({c86bb2b9-c86bb2b9})
# 11. Maintenance

[comment]: # ({/c86bb2b9-c86bb2b9})

[comment]: # ({89ef1c2a-e70ea305})
#### Overview

You can define maintenance periods for host groups, hosts and specific
triggers/services in Zabbix.

There are two maintenance types - with data collection and with no data
collection.

During a maintenance "with data collection" triggers are processed as
usual and events are created when required. However, problem escalations
are paused for hosts/triggers in maintenance, if the *Pause operations
for suppressed problems* option is checked in action configuration. In
this case, escalation steps that may include sending notifications or
remote commands will be ignored for as long as the maintenance period
lasts. Note that problem recovery and update operations are not
suppressed during maintenance, only escalations.

For example, if escalation steps are scheduled at 0, 30 and 60 minutes
after a problem start, and there is a half-hour long maintenance lasting
from 10 minutes to 40 minutes after a real problem arises, steps two and
three will be executed a half-hour later, or at 60 minutes and 90
minutes (providing the problem still exists). Similarly, if a problem
arises during the maintenance, the escalation will start after the
maintenance.

To receive problem notifications during the maintenance normally
(without delay), you have to uncheck the *Pause operations for
suppressed problems* option in action configuration.

::: noteclassic
If at least one host (used in the trigger expression) is not
in maintenance mode, Zabbix will send a problem
notification.
:::

Zabbix server must be running during maintenance. Timer processes are
responsible for switching host status to/from maintenance at 0 seconds
of every minute. Note that when a host enters maintenance, Zabbix server
timer processes will read all open problems to check if it is required
to suppress those. This may have a performance impact if there are many
open problems. Zabbix server will also read all open problems upon
startup, even if there are no maintenances configured at the time.

Note that the Zabbix server (or proxy) always collects data regardless of the maintenance type
(including "no data" maintenance). The data is later ignored by the
server if 'no data collection' is set.

When "no data" maintenance ends, triggers using nodata() function will
not fire before the next check during the period they are checking.

If a log item is added while a host is in maintenance and the
maintenance ends, only new logfile entries since the end of the
maintenance will be gathered.

If a timestamped value is sent for a host that is in a “no data”
maintenance type (e.g. using [Zabbix sender](/manpages/zabbix_sender))
then this value will be dropped however it is possible to send a
timestamped value in for an expired maintenance period and it will be
accepted.

If maintenance period, hosts, groups or tags are changed by the user,
the changes will only take effect after configuration cache
synchronization.

[comment]: # ({/89ef1c2a-e70ea305})

[comment]: # ({2d466061-6ef20399})
#### Configuration

To configure a maintenance period:

1. Go to: *Data collection → Maintenance*.
2. Click on *Create maintenance period* (or on the name of an existing maintenance period).
3. Enter maintenance parameters in the form.

![](../../assets/en/manual/maintenance/maintenance.png){width="600"}

All mandatory input fields are marked with a red asterisk.

|Parameter|Description|
|--|--------|
|*Name*|Name of the maintenance period.|
|*Maintenance type*|Two types of maintenance can be set:<br>**With data collection** - data will be collected by the server during maintenance, triggers will be processed;<br>**No data collection** - data will not be collected by the server during maintenance.|
|*Active since*|The date and time when executing maintenance periods becomes active.<br>*Note:* Setting this time alone does not activate a maintenance period; for that go to the *Periods* tab.|
|*Active till*|The date and time when executing maintenance periods stops being active.|
|*Periods*|This block allows you to define the exact days and hours when the maintenance takes place. Clicking on ![](../../assets/en/manual/maintenance/add_link.png) opens a popup window with a flexible *Maintenance period* form where you can define maintenance schedule. See [Maintenance periods](#maintenance_periods) for a detailed description.|
|*Host groups*|Select host groups that the maintenance will be activated for. The maintenance will be activated for all hosts from the specified host group(s). This field is auto-complete, so starting to type in it will display a dropdown of all available host groups.<br>Specifying a parent host group implicitly selects all nested host groups. Thus the maintenance will also be activated on hosts from nested groups.|
|*Hosts*|Select hosts that the maintenance will be activated for. This field is auto-complete, so starting to type in it will display a dropdown of all available hosts.<br>|
|*Tags*|If maintenance tags are specified, maintenance for the selected hosts will be activated, but only problems with matching tags will be suppressed (that is, no actions will be taken).<br><br>In case of multiple tags, they are calculated as follows:<br>**And/Or** - all tags must correspond; however tags with the same tag name are calculated by the Or condition;<br>**Or** - enough if one tag corresponds.<br><br>There are two ways of matching the tag value:<br>**Contains** - case-sensitive substring match (tag value contains the entered string);<br>**Equals** - case-sensitive string match (tag value equals the entered string).<br><br>Tags can be specified only if *With data collection* mode is selected.|
|*Description*|Description of maintenance period.|

[comment]: # ({/2d466061-6ef20399})

[comment]: # ({8d89857e-4dc2aa1c})
##### Maintenance periods

The maintenance period window is for scheduling time for a recurring or
a one-time maintenance. The form is dynamic with available fields
changing based on the *Period type* selected.

![](../../assets/en/manual/maintenance/maintenance_period.png){width="600"}

|Period type|Description|
|--|--------|
|*One time only*|Configure a one time only maintenance period:<br>*Date* - date and time of the maintenance period;<br>*Maintenance period length* - for how long the maintenance will be active.|
|*Daily*|Configure a daily maintenance period:<br>*Every day(s)* - maintenance frequency (1 - *(default)* every day, 2 - every two days, etc.);<br>*At (hour:minute)* - time of the day when maintenance starts;<br>*Maintenance period length* - for how long the maintenance will be active.<br><br>When *Every day(s)* parameter is greater than "1", the starting day is the day that the *Active since* time falls on. Examples:<br>- if *Active since* is set to "2021-01-01 12:00", *Every day(s)* is set to "2", and *At (hour:minute)* is set to "23:00", then the first maintenance period will start on January 1 at 23:00, while the second maintenance period will start on January 3 at 23:00;<br>- if *Active since* is set to "2021-01-01 12:00", *Every day(s)* is set to "2", and *At (hour:minute)* is set to "01:00", then the first maintenance period will start on January 3 at 01:00, while the second maintenance period will start on January 5 at 01:00.|
|*Weekly*|Configure a weekly maintenance period:<br>*Every week(s)* - maintenance frequency (1 - *(default)* every week, 2 - every two weeks, etc.);<br>*Day of week* - on which day the maintenance should take place;<br>*At (hour:minute)* - time of the day when maintenance starts;<br>*Maintenance period length* - for how long the maintenance will be active.<br><br>When *Every week(s)* parameter is greater than "1", the starting week is the week that the *Active since* time falls on. For examples, see parameter *Daily* description above.|
|*Monthly*|Configure a monthly maintenance period:<br>*Month* - select all months during which the regular maintenance is carried out;<br>*Date: **Day of month*** - select this option if the maintenance should take place on the same date each month (for example, every 1st day of the month), and then select the required day in the field *Day of month* that appears;<br>*Date: **Day of week*** - select this option if the maintenance should take place only on certain days (for example, every first Monday of the month), then select (in the drop-down) the required week of the month (first, second, third, fourth, or last), and then mark the checkboxes for maintenance day(s);<br>*At (hour:minute)* - time of the day when maintenance starts;<br>*Maintenance period length* - for how long the maintenance will be active.|

[comment]: # ({/8d89857e-4dc2aa1c})

[comment]: # ({7d918711-6aeb0a4c})

::: noteimportant
When creating a maintenance period, the [time zone](/manual/web_interface/time_zone) of the user who creates it is used.
However, when recurring maintenance periods (*Daily*, *Weekly*, *Monthly*) are scheduled, the time zone of the Zabbix server is used.
To ensure predictable behavior of recurring maintenance periods, it is required to use a common time zone for all parts of Zabbix.
:::

[comment]: # ({/7d918711-6aeb0a4c})

[comment]: # ({5aa2e600-3b17f2f3})

When done, press *Add* to add the maintenance period to the *Periods* block.

Note that Daylight Saving Time (DST) changes do not affect how long the maintenance will be.
For example, let's say that we have a two-hour maintenance configured that usually starts at 01:00 and finishes at 03:00:

-   if after one hour of maintenance (at 02:00) a DST change happens and current time changes from 02:00 to 03:00, the maintenance will continue for one more hour (till 04:00);
-   if after two hours of maintenance (at 03:00) a DST change happens and current time changes from 03:00 to 02:00, the maintenance will stop, because two hours have passed;
-   if a maintenance period starts during the hour that is skipped by a DST change, then the maintenance will not start.

If a maintenance period is set to "1 day" (the actual period of the maintenance is 24 hours, since Zabbix calculates days in hours), starts at 00:00 and finishes at 00:00 the next day:

-   the maintenance will stop at 01:00 the next day if current time changes forward one hour;
-   the maintenance will stop at 23:00 that day if current time changes back one hour.

[comment]: # ({/5aa2e600-3b17f2f3})

[comment]: # ({924e8c68-924e8c68})
#### Display

[comment]: # ({/924e8c68-924e8c68})

[comment]: # ({1daf7551-0e33672d})
##### Displaying hosts in maintenance

An orange wrench icon
![](../../assets/en/manual/web_interface/frontend_sections/configuration/maintenance_wrench_icon.png)
next to the host name indicates that this host is in maintenance in:

-   *Dashboards*
-   *Monitoring → Problems*
-   *Inventory → Hosts → Host inventory details*
-   *Data collection → Hosts* (See 'Status' column)

![](../../assets/en/manual/maintenance/maintenance_icon.png)

Maintenance details are displayed when the mouse pointer is positioned
over the icon.

Additionally, hosts in maintenance get an orange background in
*Monitoring → Maps*.

[comment]: # ({/1daf7551-0e33672d})

[comment]: # ({7dd10abd-a923614c})
##### Displaying suppressed problems

Normally problems for hosts in maintenance are suppressed, i.e. not
displayed in the frontend. However, it is also possible to configure
that suppressed problems are shown, by selecting the *Show suppressed
problems* option in these locations:

-   *Dashboards* (in *Problem hosts*, *Problems*,
    *Problems by severity*, *Trigger overview* widget configuration)
-   *Monitoring* → *Problems* (in the filter)
-   *Monitoring* → *Maps* (in map configuration)
-   Global
    [notifications](/manual/web_interface/user_profile/global_notifications)
    (in user profile configuration)

When suppressed problems are displayed, the following icon is displayed:
![](../../assets/en/manual/web_interface/icon_suppressed.png). Rolling a
mouse over the icon displays more details.

![](../../assets/en/manual/web_interface/info_suppressed2.png)

[comment]: # ({/7dd10abd-a923614c})
