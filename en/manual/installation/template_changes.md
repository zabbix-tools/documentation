[comment]: # ({9b16a969-0a5cc230})
# 9 Template changes

This page lists all changes to the stock templates that are shipped with
Zabbix.

Note that upgrading to the latest Zabbix version will not automatically
upgrade the templates used. It is suggested to modify the templates in
existing installations by:

-   Downloading the latest templates from the [Zabbix Git
    repository](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates);
-   Then, while in *Data collection* → *Templates* you can import them
    manually into Zabbix. If templates with the same names already
    exist, the *Delete missing* options should be checked when importing
    to achieve a clean import. This way the old items that are no longer
    in the updated template will be removed (note that it will mean
    losing history of these old items).

[comment]: # ({/9b16a969-0a5cc230})

[comment]: # ({1b30ec92-94950308})
### CHANGES IN 7.0.0

[comment]: # ({/1b30ec92-94950308})

[comment]: # ({6bb95db4-651539db})
#### Updated templates

**Zabbix server/proxy health**

Templates for Zabbix server/proxy health have been updated according to the [changes](/manual/introduction/whatsnew700#concurrency-in-network-discovery) 
in network discovery. The item/trigger for monitoring the discoverer process (now removed) have been replaced by 
items/triggers to measure the process utilization of discovery manager and discovery worker respectively. 
A new internal item has been added for monitoring the discovery queue. 

[comment]: # ({/6bb95db4-651539db})
