[comment]: # ({365934f1-365934f1})
# 7 Upgrade procedure

[comment]: # ({/365934f1-365934f1})

[comment]: # ({58c5428c-3f635361})
#### Overview

This section provides upgrade information for Zabbix **7.0**:

-   using packages:
    -   for [Red Hat Enterprise
        Linux](/manual/installation/upgrade/packages/rhel)
    -   for
        [Debian/Ubuntu](/manual/installation/upgrade/packages/debian_ubuntu)
-   using [sources](/manual/installation/upgrade/sources)

See also [upgrade instructions](/manual/concepts/server/ha#upgrading-ha-cluster) for servers in a **high-availability** (HA) cluster.

[comment]: # ({/58c5428c-3f635361})

[comment]: # ({8fe6ecf8-54ac8cd9})

Upgrading Zabbix agents is recommended but not mandatory. 

Upgrading Zabbix proxies is highly recommended. Zabbix server fully supports proxies that are of the same major version
as the server. Zabbix server also supports proxies that are **no older** than Zabbix server previous LTS release
version, but with limited functionality (data collection, execution of
[remote commands](/manual/config/notifications/action/operation/remote_command),
[immediate item value checks](/manual/config/items/check_now)). Configuration update is also disabled, and
[outdated](/manual/appendix/compatibility#supported-zabbix-proxies) proxies will only work with old configuration.

[comment]: # ({/8fe6ecf8-54ac8cd9})

[comment]: # ({2210de30-7a93c7ad})

::: noteimportant
Proxies that are older than Zabbix server previous LTS release version or newer than Zabbix server major version are not
supported. Zabbix server will ignore data from unsupported proxies and all communication with Zabbix server will fail
with a warning. For more information, see
[Version compatibility](/manual/appendix/compatibility#supported-zabbix-proxies).
:::

To minimize downtime and data loss during the upgrade, it is recommended to stop, upgrade, and start Zabbix server and
then stop, upgrade, and start Zabbix proxies one after another. During server downtime, running proxies will continue
data collection. Once the server is up and running, [outdated](/manual/appendix/compatibility#supported-zabbix-proxies)
proxies will send the data to the newer server (proxy configuration will not be updated though) and will remain partly
functional. Any notifications for problems during Zabbix server downtime will be generated only after the upgraded
server is started.

[comment]: # ({/2210de30-7a93c7ad})

[comment]: # ({ec768c51-e649918a})

If Zabbix proxy is started for the first time and the SQLite database file is missing, proxy creates it automatically.
**Note that starting with Zabbix 6.4.0, if Zabbix proxy uses SQLite3 and on startup detects that existing database file
version is older than required, it will delete the database file automatically and create a new one**. Therefore,
history data stored in the SQLite database file will be lost. If Zabbix proxy's version is older than the database file
version, Zabbix will log an error and exit.

Depending on the database size, the database upgrade to version 7.0 may take a long time.

[comment]: # ({/ec768c51-e649918a})

[comment]: # ({9c2ae197-a45a4e79})

Direct upgrade to Zabbix 7.0.x is possible from Zabbix **6.4**.x, **6.2**.x, **6.0**.x, **5.4**.x,
**5.2**.x, **5.0**.x, **4.4**.x, **4.2**.x, **4.0**.x, **3.4**.x, **3.2**.x,
**3.0**.x, **2.4**.x, **2.2**.x and **2.0**.x.
For upgrading from earlier versions consult Zabbix documentation for 2.0 and earlier.

::: noteclassic
Please be aware that after upgrading some third-party software integrations in Zabbix might be affected, if the
external software is not compatible with the upgraded Zabbix version.
:::

[comment]: # ({/9c2ae197-a45a4e79})

[comment]: # ({39c8c309-00dcc2d4})

The following upgrade notes are available:

|Upgrade from|Read full upgrade notes|Most important changes between versions|
|--|--|------|
|6.4.x    |For:<br>Zabbix [7.0](/manual/installation/upgrade_notes_700)| |
|6.2.x    |For:<br>Zabbix [6.4](https://www.zabbix.com/documentation/6.4/manual/installation/upgrade_notes_640)<br>Zabbix [7.0](/manual/installation/upgrade_notes_700)|Minimum required MySQL version raised from 8.0.0 to 8.0.30.<br>'libevent_pthreads' library is required for Zabbix server/proxy.<br>Upon the first launch after an upgrade, Zabbix proxy with SQLite3 automatically drops the old version of the database (with all the history) and creates a new one. |
|6.0.x LTS|For:<br>Zabbix [6.2](https://www.zabbix.com/documentation/6.2/manual/installation/upgrade_notes_620)<br>Zabbix [6.4](https://www.zabbix.com/documentation/6.4/manual/installation/upgrade_notes_640)<br>Zabbix [7.0](/manual/installation/upgrade_notes_700)|Minimum required PHP version upped from 7.2.5 to 7.4.0. <br> Deterministic triggers need to be created during the upgrade. If binary logging is enabled for MySQL/MariaDB, this requires superuser privileges or setting the variable/configuration parameter  *log_bin_trust_function_creators = 1*. See [Database creation scripts](/manual/appendix/install/db_scripts#mysql) for instructions how to set the variable.|
|5.4.x    |For:<br>Zabbix [6.0](https://www.zabbix.com/documentation/6.0/manual/installation/upgrade_notes_600)<br>Zabbix [6.2](https://www.zabbix.com/documentation/6.2/manual/installation/upgrade_notes_620)<br>Zabbix [6.4](https://www.zabbix.com/documentation/6.4/manual/installation/upgrade_notes_640)<br>Zabbix [7.0](/manual/installation/upgrade_notes_700)|Minimum required database versions upped.<br>Server/proxy will not start if outdated database.<br>Audit log records lost because of database structure change. |
|5.2.x    |For:<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](https://www.zabbix.com/documentation/6.0/manual/installation/upgrade_notes_600)<br>Zabbix [6.2](https://www.zabbix.com/documentation/6.2/manual/installation/upgrade_notes_620)<br>Zabbix [6.4](https://www.zabbix.com/documentation/6.4/manual/installation/upgrade_notes_640)<br>Zabbix [7.0](/manual/installation/upgrade_notes_700)|Minimum required database versions upped.<br>Aggregate items removed as a separate type.|
|5.0.x LTS|For:<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](https://www.zabbix.com/documentation/6.0/manual/installation/upgrade_notes_600)<br>Zabbix [6.2](https://www.zabbix.com/documentation/6.2/manual/installation/upgrade_notes_620)<br>Zabbix [6.4](https://www.zabbix.com/documentation/6.4/manual/installation/upgrade_notes_640)<br>Zabbix [7.0](/manual/installation/upgrade_notes_700)|Minimum required PHP version upped from 7.2.0 to 7.2.5.<br> Password hashing algorithm changed from MD5 to bcrypt. |
|4.4.x    |For:<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](https://www.zabbix.com/documentation/6.0/manual/installation/upgrade_notes_600)<br>Zabbix [6.2](https://www.zabbix.com/documentation/6.2/manual/installation/upgrade_notes_620)<br>Zabbix [6.4](https://www.zabbix.com/documentation/6.4/manual/installation/upgrade_notes_640)<br>Zabbix [7.0](/manual/installation/upgrade_notes_700)|Support of IBM DB2 dropped.<br>Minimum required PHP version upped from 5.4.0 to 7.2.0.<br>Minimum required database versions upped.<br>Changed Zabbix PHP file directory.|
|4.2.x    |For:<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.4/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](https://www.zabbix.com/documentation/6.0/manual/installation/upgrade_notes_600)<br>Zabbix [6.2](https://www.zabbix.com/documentation/6.2/manual/installation/upgrade_notes_620)<br>Zabbix [6.4](https://www.zabbix.com/documentation/6.4/manual/installation/upgrade_notes_640)<br>Zabbix [7.0](/manual/installation/upgrade_notes_700)|Jabber, Ez Texting media types removed.|
|4.0.x LTS|For:<br>Zabbix [4.2](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_420)<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.4/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](https://www.zabbix.com/documentation/6.0/manual/installation/upgrade_notes_600)<br>Zabbix [6.2](https://www.zabbix.com/documentation/6.2/manual/installation/upgrade_notes_620)<br>Zabbix [6.4](https://www.zabbix.com/documentation/6.4/manual/installation/upgrade_notes_640)<br>Zabbix [7.0](/manual/installation/upgrade_notes_700)|Older proxies no longer can report data to an upgraded server.<br>Newer agents no longer will be able to work with an older Zabbix server.|
|3.4.x    |For:<br>Zabbix [4.0](https://www.zabbix.com/documentation/4.0/manual/installation/upgrade_notes_400)<br>Zabbix [4.2](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_420)<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.4/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](https://www.zabbix.com/documentation/6.0/manual/installation/upgrade_notes_600)<br>Zabbix [6.2](https://www.zabbix.com/documentation/6.2/manual/installation/upgrade_notes_620)<br>Zabbix [6.4](https://www.zabbix.com/documentation/6.4/manual/installation/upgrade_notes_640)<br>Zabbix [7.0](/manual/installation/upgrade_notes_700)|'libpthread' and 'zlib' libraries now mandatory.<br>Support for plain text protocol dropped and header is mandatory.<br>Pre-1.4 version Zabbix agents are no longer supported.<br>The Server parameter in passive proxy configuration now mandatory.|
|3.2.x    |For:<br>Zabbix [3.4](https://www.zabbix.com/documentation/3.4/manual/installation/upgrade_notes_340)<br>Zabbix [4.0](https://www.zabbix.com/documentation/4.0/manual/installation/upgrade_notes_400)<br>Zabbix [4.2](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_420)<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.4/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](https://www.zabbix.com/documentation/6.0/manual/installation/upgrade_notes_600)<br>Zabbix [6.2](https://www.zabbix.com/documentation/6.2/manual/installation/upgrade_notes_620)<br>Zabbix [6.4](https://www.zabbix.com/documentation/6.4/manual/installation/upgrade_notes_640)<br>Zabbix [7.0](/manual/installation/upgrade_notes_700)|SQLite support as backend database dropped for Zabbix server/frontend.<br>Perl Compatible Regular Expressions (PCRE) supported instead of POSIX extended.<br>'libpcre' and 'libevent' libraries mandatory for Zabbix server.<br>Exit code checks added for user parameters, remote commands and system.run\[\] items without the 'nowait' flag as well as Zabbix server executed scripts.<br>Zabbix Java gateway has to be upgraded to support new functionality.|
|3.0.x LTS|For:<br>Zabbix [3.2](https://www.zabbix.com/documentation/3.2/manual/installation/upgrade_notes_320)<br>Zabbix [3.4](https://www.zabbix.com/documentation/3.4/manual/installation/upgrade_notes_340)<br>Zabbix [4.0](https://www.zabbix.com/documentation/4.0/manual/installation/upgrade_notes_400)<br>Zabbix [4.2](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_420)<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.4/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](https://www.zabbix.com/documentation/6.0/manual/installation/upgrade_notes_600)<br>Zabbix [6.2](https://www.zabbix.com/documentation/6.2/manual/installation/upgrade_notes_620)<br>Zabbix [6.4](https://www.zabbix.com/documentation/6.4/manual/installation/upgrade_notes_640)<br>Zabbix [7.0](/manual/installation/upgrade_notes_700)|Database upgrade may be slow, depending on the history table size.|
|2.4.x    |For:<br>Zabbix [3.0](https://www.zabbix.com/documentation/3.0/manual/installation/upgrade_notes_300)<br>Zabbix [3.2](https://www.zabbix.com/documentation/3.2/manual/installation/upgrade_notes_320)<br>Zabbix [3.4](https://www.zabbix.com/documentation/3.4/manual/installation/upgrade_notes_340)<br>Zabbix [4.0](https://www.zabbix.com/documentation/4.0/manual/installation/upgrade_notes_400)<br>Zabbix [4.2](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_420)<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](https://www.zabbix.com/documentation/6.0/manual/installation/upgrade_notes_600)<br>Zabbix [6.2](https://www.zabbix.com/documentation/6.2/manual/installation/upgrade_notes_620)<br>Zabbix [6.4](https://www.zabbix.com/documentation/6.4/manual/installation/upgrade_notes_640)<br>Zabbix [7.0](/manual/installation/upgrade_notes_700)|Minimum required PHP version upped from 5.3.0 to 5.4.0.<br>LogFile agent parameter must be specified.|
|2.2.x LTS|For:<br>Zabbix [2.4](https://www.zabbix.com/documentation/2.4/manual/installation/upgrade_notes_240)<br>Zabbix [3.0](https://www.zabbix.com/documentation/3.0/manual/installation/upgrade_notes_300)<br>Zabbix [3.2](https://www.zabbix.com/documentation/3.2/manual/installation/upgrade_notes_320)<br>Zabbix [3.4](https://www.zabbix.com/documentation/3.4/manual/installation/upgrade_notes_340)<br>Zabbix [4.0](https://www.zabbix.com/documentation/4.0/manual/installation/upgrade_notes_400)<br>Zabbix [4.2](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_420)<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](https://www.zabbix.com/documentation/6.0/manual/installation/upgrade_notes_600)<br>Zabbix [6.2](https://www.zabbix.com/documentation/6.2/manual/installation/upgrade_notes_620)<br>Zabbix [6.4](https://www.zabbix.com/documentation/6.4/manual/installation/upgrade_notes_640)<br>Zabbix [7.0](/manual/installation/upgrade_notes_700)|Node-based distributed monitoring removed.|
|2.0.x    |For:<br>Zabbix [2.2](https://www.zabbix.com/documentation/2.2/manual/installation/upgrade_notes_220)<br>Zabbix [2.4](https://www.zabbix.com/documentation/2.4/manual/installation/upgrade_notes_240)<br>Zabbix [3.0](https://www.zabbix.com/documentation/3.0/manual/installation/upgrade_notes_300)<br>Zabbix [3.2](https://www.zabbix.com/documentation/3.2/manual/installation/upgrade_notes_320)<br>Zabbix [3.4](https://www.zabbix.com/documentation/3.4/manual/installation/upgrade_notes_340)<br>Zabbix [4.0](https://www.zabbix.com/documentation/4.0/manual/installation/upgrade_notes_400)<br>Zabbix [4.2](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_420)<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](https://www.zabbix.com/documentation/6.0/manual/installation/upgrade_notes_600)<br>Zabbix [6.2](https://www.zabbix.com/documentation/6.2/manual/installation/upgrade_notes_620)<br>Zabbix [6.4](https://www.zabbix.com/documentation/6.4/manual/installation/upgrade_notes_640)<br>Zabbix [7.0](/manual/installation/upgrade_notes_700)|Minimum required PHP version upped from 5.1.6 to 5.3.0.<br>Case-sensitive MySQL database required for proper server work; character set utf8 and utf8\_bin collation is required for Zabbix server to work properly with MySQL database. See [database creation scripts](/manual/appendix/install/db_scripts#mysql).<br>'mysqli' PHP extension required instead of 'mysql'.|

[comment]: # ({/39c8c309-00dcc2d4})
