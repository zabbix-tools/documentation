[comment]: # ({d96d3275-d96d3275})
# 2 Debian/Ubuntu/Raspbian

[comment]: # ({/d96d3275-d96d3275})

[comment]: # ({1479698b-9df568e9})
### Overview

Official Zabbix 6.5 PRE-RELEASE packages for Debian, Ubuntu, and Raspberry Pi OS (Raspbian) are available on
[Zabbix website](https://www.zabbix.com/download). Packages for Zabbix 7.0 will be available upon its release.

Packages are available with either MySQL/PostgreSQL database and Apache/Nginx web server support.

[comment]: # ({/1479698b-9df568e9})

[comment]: # ({0adee734-ae631a63})
### Notes on installation

See the [installation
instructions](https://www.zabbix.com/download?zabbix=7.0&os_distribution=ubuntu&os_version=22.04&components=server_frontend_agent&db=mysql&ws=apache)
per platform in the download page for:

-   installing the repository
-   installing server/agent/frontend
-   creating initial database, importing initial data
-   configuring database for Zabbix server
-   configuring PHP for Zabbix frontend
-   starting server/agent processes
-   configuring Zabbix frontend

If you want to run Zabbix agent as root, see [running agent as
root](/manual/appendix/install/run_agent_as_root).

Zabbix web service process, which is used for [scheduled report
generation](/manual/web_interface/frontend_sections/reports/scheduled),
requires Google Chrome browser. The browser is not included into
packages and has to be installed manually.

[comment]: # ({/0adee734-ae631a63})

[comment]: # ({e53454de-99a402fa})
#### Importing data with Timescale DB

With TimescaleDB, in addition to the import command for PostgreSQL, also
run:

    cat /usr/share/zabbix-sql-scripts/postgresql/timescaledb.sql | sudo -u zabbix psql zabbix

::: notewarning
TimescaleDB is supported with Zabbix server
only.
:::

[comment]: # ({/e53454de-99a402fa})


[comment]: # ({89d36800-fa73411a})
#### SELinux configuration

See [SELinux
configuration](/manual/installation/install_from_packages/rhel#selinux_configuration)
for RHEL.

After the frontend and SELinux configuration is done, restart the Apache
web server:

    service apache2 restart

[comment]: # ({/89d36800-fa73411a})

[comment]: # ({7d2a10a7-a12da003})
### Proxy installation

Once the required repository is added, you can install Zabbix proxy by
running:

    apt install zabbix-proxy-mysql zabbix-sql-scripts

Substitute 'mysql' in the command with 'pgsql' to use PostgreSQL, or
with 'sqlite3' to use SQLite3.

The package 'zabbix-sql-scripts' contains database schemas for all supported database management systems for both Zabbix server and Zabbix proxy and will be used for data import.

[comment]: # ({/7d2a10a7-a12da003})

[comment]: # ({fe6abb8e-fe6abb8e})
##### Creating database

[Create](/manual/appendix/install/db_scripts) a separate database for
Zabbix proxy.

Zabbix server and Zabbix proxy cannot use the same database. If they are
installed on the same host, the proxy database must have a different
name.

[comment]: # ({/fe6abb8e-fe6abb8e})

[comment]: # ({fa48c063-2ab835d7})
##### Importing data

Import initial schema:

    cat /usr/share/zabbix-sql-scripts/mysql/proxy.sql | mysql -uzabbix -p zabbix

For proxy with PostgreSQL (or SQLite):

    cat /usr/share/zabbix-sql-scripts/postgresql/proxy.sql | sudo -u zabbix psql zabbix
    cat /usr/share/zabbix-sql-scripts/sqlite3/proxy.sql | sqlite3 zabbix.db

[comment]: # ({/fa48c063-2ab835d7})

[comment]: # ({12396295-d0a225c7})
##### Configure database for Zabbix proxy

Edit zabbix\_proxy.conf:

    vi /etc/zabbix/zabbix_proxy.conf
    DBHost=localhost
    DBName=zabbix
    DBUser=zabbix
    DBPassword=<password>

In DBName for Zabbix proxy use a separate database from Zabbix server.

In DBPassword use Zabbix database password for MySQL; PostgreSQL user
password for PostgreSQL.

Use `DBHost=` with PostgreSQL. You might want to keep the default
setting `DBHost=localhost` (or an IP address), but this would make
PostgreSQL use a network socket for connecting to Zabbix. Refer to the
[respective
section](/manual/installation/install_from_packages/rhel#selinux_configuration)
for RHEL for instructions.

[comment]: # ({/12396295-d0a225c7})

[comment]: # ({72cdd634-27de2ced})
##### Starting Zabbix proxy process

To start a Zabbix proxy process and make it start at system boot:

    systemctl restart zabbix-proxy
    systemctl enable zabbix-proxy

[comment]: # ({/72cdd634-27de2ced})

[comment]: # ({871a973b-871a973b})
##### Frontend configuration

A Zabbix proxy does not have a frontend; it communicates with Zabbix
server only.

[comment]: # ({/871a973b-871a973b})

[comment]: # ({5f1bdfd7-cd9340bd})
### Java gateway installation

It is required to install [Java gateway](/manual/concepts/java) only if
you want to monitor JMX applications. Java gateway is lightweight and
does not require a database.

Once the required repository is added, you can install Zabbix Java
gateway by running:

    apt install zabbix-java-gateway

Proceed to [setup](/manual/concepts/java/from_debian_ubuntu) for more
details on configuring and running Java gateway.

[comment]: # ({/5f1bdfd7-cd9340bd})
