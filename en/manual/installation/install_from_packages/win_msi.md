[comment]: # ({7050624a-7050624a})
# 4 Windows agent installation from MSI

[comment]: # ({/7050624a-7050624a})

[comment]: # ({7428054e-04777b60})
### Overview

Zabbix Windows agent can be installed from Windows MSI installer
packages (32-bit or 64-bit) available for
[download](https://www.zabbix.com/download_agents#tab:44).

A 32-bit package cannot be installed on a 64-bit Windows.

The minimum requirement for MSI installation is:

- Windows XP x64 and Server 2003 for Zabbix agent;
- Windows 7 x32 for Zabbix agent 2.

The Zabbix get and sender utilities can also be installed, either together with Zabbix agent/agent 2 or separately.

All packages come with TLS support, however, configuring TLS is
optional.

Both UI and command-line based installation is supported.

::: noteclassic
Although Zabbix installation from MSI installer packages is fully supported,
it is recommended to install at least *Microsoft .NET Framework 2* for proper error handling.
See [Microsoft Download .NET Framework](https://dotnet.microsoft.com/en-us/download/dotnet-framework).
:::

::: noteimportant
It is recommended to use default paths provided by the installer as using custom paths without proper permissions could compromise the security of the installation.
:::

[comment]: # ({/7428054e-04777b60})

[comment]: # ({3d7cd403-0fc3b21c})
### Installation steps

To install, double-click the downloaded MSI file.

![](../../../../assets/en/manual/installation/install_from_packages/msi0_b.png)

![](../../../../assets/en/manual/installation/install_from_packages/msi0_c.png)

Accept the license to proceed to the next step.

![](../../../../assets/en/manual/installation/install_from_packages/msi0_d.png)

Specify the following parameters.

|Parameter|Description|
|--|--------|
|*Host name*|Specify host name.|
|*Zabbix server IP/DNS*|Specify IP/DNS of Zabbix server.|
|*Agent listen port*|Specify agent listen port (10050 by default).|
|*Server or Proxy for active checks*|Specify IP/DNS of Zabbix server/proxy for active agent checks.|
|*Enable PSK*|Mark the checkbox to enable TLS support via pre-shared keys.|
|*Add agent location to the PATH*|Add agent location to the PATH variable.|

![](../../../../assets/en/manual/installation/install_from_packages/msi0_e.png)

Enter pre-shared key identity and value. This step is only available if
you checked *Enable PSK* in the previous step.

![](../../../../assets/en/manual/installation/install_from_packages/msi0_f.png)

Select Zabbix components to install - [Zabbix agent
daemon](/manual/concepts/agent), [Zabbix
sender](/manual/concepts/sender), [Zabbix get](/manual/concepts/get).

![](../../../../assets/en/manual/installation/install_from_packages/msi0_g.png)

Zabbix components along with the configuration file will be installed in
a *Zabbix Agent* folder in Program Files. zabbix\_agentd.exe will be set
up as Windows service with automatic startup.

![](../../../../assets/en/manual/installation/install_from_packages/msi0_h.png)

[comment]: # ({/3d7cd403-0fc3b21c})

[comment]: # ({e632e39b-1ca3dd90})
### Command-line based installation

[comment]: # ({/e632e39b-1ca3dd90})

[comment]: # ({8a7a0174-89028c66})
##### Supported parameters

The following parameters are supported by created MSIs.

|Parameter|Description|
|--|--------|
|ADDDEFAULT|A comma-delimited list of programs to install.<br>Possible values: `AgentProgram`, `GetProgram`, `SenderProgram`, `ALL`.<br>Example: `ADDDEFAULT=AgentProgram,GetProgram`|
|ADDLOCAL|A comma-delimited list of programs to install.<br>Possible values: `AgentProgram`,  `GetProgram`, `SenderProgram`, `ALL`.<br>Example: `ADDLOCAL=AgentProgram,SenderProgram`|
|ALLOWDENYKEY|Sequence of "AllowKey" and "DenyKey" [parameters](/manual/config/items/restrict_checks) separated by `;`<br>Use `\\;` to escape the delimiter.<br>Example: `ALLOWDENYKEY="AllowKey=system.run[type c:\windows\system32\drivers\etc\hosts];DenyKey=system.run[*]"`|
|CONF|The full pathname to a custom configuration file.<br>Example: `CONF=c:\full\path\to\user.conf`|
|ENABLEPATH|Add agent location to the PATH variable.|
|[ENABLEPERSISTENTBUFFER](/manual/appendix/config/zabbix_agent2_win#enablepersistentbuffer)|Zabbix agent 2 only. Enable the usage of local persistent storage for active items.|
|[HOSTINTERFACE](/manual/appendix/config/zabbix_agentd_win#hostinterface)|An optional parameter that defines the host interface.|
|[HOSTMETADATA](/manual/appendix/config/zabbix_agentd_win#hostmetadata)|An optional parameter that defines the host metadata.|
|[HOSTMETADATAITEM](/manual/appendix/config/zabbix_agentd_win#hostmetadataitem)|An optional parameter that defines a Zabbix agent item used for getting the host metadata.|
|[HOSTNAME](/manual/appendix/config/zabbix_agentd_win#hostname)|An optional parameter that defines the hostname.|
|INCLUDE|Sequence of [includes](/manual/appendix/config/special_notes_include) separated by `;`|
|INSTALLFOLDER|The full pathname of the folder in which Zabbix components along with the configuration file will be installed.|
|[LISTENIP](/manual/appendix/config/zabbix_agentd_win#listenip)|A list of comma-delimited IP addresses that the agent should listen on.|
|[LISTENPORT](/manual/appendix/config/zabbix_agentd_win#listenport)|The agent will listen on this port for connections from the server.|
|[LOGFILE](/manual/appendix/config/zabbix_agentd_win#logfile)|The name of the log file.|
|[LOGTYPE](/manual/appendix/config/zabbix_agentd_win#logtype)|The type of the log output.|
|[PERSISTENTBUFFERFILE](/manual/appendix/config/zabbix_agent2_win#persistentbufferfile)|Zabbix agent 2 only. The file where Zabbix agent 2 should keep the SQLite database.|
|[PERSISTENTBUFFERPERIOD](/manual/appendix/config/zabbix_agent2_win#persistentbufferperiod)|Zabbix agent 2 only. The time period for which data should be stored when there is no connection to the server or proxy.|
|[SERVER](/manual/appendix/config/zabbix_agentd_win#server)|A list of comma-delimited IP addresses, optionally in CIDR notation, or DNS names of Zabbix servers and Zabbix proxies.|
|[SERVERACTIVE](/manual/appendix/config/zabbix_agentd_win#serveractive)|The Zabbix server/proxy address or cluster configuration to get active checks from.|
|SKIP|`SKIP=fw` - do not install the firewall exception rule.|
|[STATUSPORT](/manual/appendix/config/zabbix_agent2_win#statusport)|Zabbix agent 2 only. If set, the agent will listen on this port for HTTP status requests (http://localhost:<port>/status).|
|[TIMEOUT](/manual/appendix/config/zabbix_agentd_win#timeout)|Spend no more than Timeout seconds on processing.|
|[TLSACCEPT](/manual/appendix/config/zabbix_agentd_win#tlsaccept)|What incoming connections to accept.|
|[TLSCAFILE](/manual/appendix/config/zabbix_agentd_win#tlscafile)|The full pathname of a file containing the top-level CA(s) certificates for peer certificate verification, used for encrypted communications between Zabbix components.|
|[TLSCERTFILE](/manual/appendix/config/zabbix_agentd_win#tlscertfile)|The full pathname of a file containing the agent certificate or certificate chain, used for encrypted communications between Zabbix components.|
|[TLSCONNECT](/manual/appendix/config/zabbix_agentd_win#tlsconnect)|How the agent should connect to Zabbix server or proxy.|
|[TLSCRLFILE](/manual/appendix/config/zabbix_agentd_win#tlscrlfile)|The full pathname of a file containing revoked certificates. This parameter is used for encrypted communications between Zabbix components.|
|[TLSKEYFILE](/manual/appendix/config/zabbix_agentd_win#tlskeyfile)|The full pathname of a file containing the agent private key, used for encrypted communications between Zabbix components.|
|[TLSPSKFILE](/manual/appendix/config/zabbix_agentd_win#tlspskfile)|The full pathname of a file containing the agent [pre-shared key](/manual/encryption/using_pre_shared_keys), used for encrypted communications with Zabbix server.|
|[TLSPSKIDENTITY](/manual/appendix/config/zabbix_agentd_win#tlspskidentity)|The [pre-shared key](/manual/encryption/using_pre_shared_keys) identity string, used for encrypted communications with Zabbix server.|
|TLSPSKVALUE|The [pre-shared key](/manual/encryption/using_pre_shared_keys) string value, used for encrypted communications with Zabbix server.|
|[TLSSERVERCERTISSUER](/manual/appendix/config/zabbix_agentd_win#tlsservercertissuer)|The allowed server (proxy) certificate issuer.|
|[TLSSERVERCERTSUBJECT](/manual/appendix/config/zabbix_agentd_win#tlsservercertsubject)|The allowed server (proxy) certificate subject.|

[comment]: # ({/8a7a0174-89028c66})

[comment]: # ({160b6886-bfabef9c})
##### Examples

To install Zabbix Windows agent from the command-line, you may run, for example:

    SET INSTALLFOLDER=C:\Program Files\Zabbix Agent

    msiexec /l*v log.txt /i zabbix_agent-7.0.0-x86.msi /qn^
     LOGTYPE=file^
     LOGFILE="%INSTALLFOLDER%\zabbix_agentd.log"^
     SERVER=192.168.6.76^
     LISTENPORT=12345^
     SERVERACTIVE=::1^
     HOSTNAME=myHost^
     TLSCONNECT=psk^
     TLSACCEPT=psk^
     TLSPSKIDENTITY=MyPSKID^
     TLSPSKFILE="%INSTALLFOLDER%\mykey.psk"^
     TLSCAFILE="c:\temp\f.txt1"^
     TLSCRLFILE="c:\temp\f.txt2"^
     TLSSERVERCERTISSUER="My CA"^
     TLSSERVERCERTSUBJECT="My Cert"^
     TLSCERTFILE="c:\temp\f.txt5"^
     TLSKEYFILE="c:\temp\f.txt6"^
     ENABLEPATH=1^
     INSTALLFOLDER="%INSTALLFOLDER%"^
     SKIP=fw^
     ALLOWDENYKEY="DenyKey=vfs.file.contents[/etc/passwd]"

You may also run, for example:

    msiexec /l*v log.txt /i zabbix_agent-7.0.0-x86.msi /qn^
     SERVER=192.168.6.76^
     TLSCONNECT=psk^
     TLSACCEPT=psk^
     TLSPSKIDENTITY=MyPSKID^
     TLSPSKVALUE=1f87b595725ac58dd977beef14b97461a7c1045b9a1c963065002c5473194952

::: noteclassic
If both TLSPSKFILE and TLSPSKVALUE are passed, then TLSPSKVALUE will be written to TLSPSKFILE.
:::

[comment]: # ({/160b6886-bfabef9c})
