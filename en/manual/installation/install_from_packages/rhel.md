[comment]: # ({c3117020-83c2fbfb})
# 1 Red Hat Enterprise Linux

[comment]: # ({/c3117020-83c2fbfb})

[comment]: # ({23cab29a-e0fb2295})
### Overview

Official Zabbix 6.5 PRE-RELEASE packages for Red Hat Enterprise Linux and Oracle Linux versions 6, 7, 8, and 9 are available
on [Zabbix website](https://www.zabbix.com/download). Packages for Zabbix 7.0 will be available upon its release.

Packages are available with:

-   MySQL or PostgreSQL database
-   Apache or Nginx web server support

*Zabbix agent* packages, and utilities *Zabbix get* and *Zabbix sender* for RHEL [6](https://repo.zabbix.com/zabbix/6.5/rhel/6/x86_64/), 
[7](https://repo.zabbix.com/zabbix/6.5/rhel/7/x86_64/), [8](https://repo.zabbix.com/zabbix/6.5/rhel/8/x86_64/), and [9](https://repo.zabbix.com/zabbix/6.5/rhel/9/x86_64/)
are also available in Zabbix Official Repository as 6.5 pre-release versions.

The official Zabbix repository provides `fping`, `iksemel` and `libssh2` packages as well. These packages are located in the
[non-supported](https://repo.zabbix.com/non-supported/) directory.

::: noteimportant
The EPEL repository for EL9 also provides Zabbix packages. If both the official Zabbix repository and EPEL repositories are installed, 
then the Zabbix packages in EPEL **must be** excluded by adding the following clause to the EPEL repo configuration file under `/etc/yum.repos.d/`:

    [epel]
    ...
    excludepkgs=zabbix*
:::

[comment]: # ({/23cab29a-e0fb2295})

[comment]: # ({e938a4a8-0d2df460})
### Notes on installation

See [installation instructions](https://www.zabbix.com/download?zabbix=7.0&os_distribution=red_hat_enterprise_linux&os_version=9&components=server_frontend_agent&db=mysql&ws=apache)
per platform in the download page for:

-   installing the repository
-   installing server/agent/frontend
-   creating initial database, importing initial data
-   configuring database for Zabbix server
-   configuring PHP for Zabbix frontend
-   starting server/agent processes
-   configuring Zabbix frontend

If you want to run Zabbix agent as root, see [Running agent as
root](/manual/appendix/install/run_agent_as_root).

Zabbix web service process, which is used for [scheduled report
generation](/manual/web_interface/frontend_sections/reports/scheduled),
requires Google Chrome browser. The browser is not included into
packages and has to be installed manually.

[comment]: # ({/e938a4a8-0d2df460})

[comment]: # ({e53454de-99a402fa})
#### Importing data with Timescale DB

With TimescaleDB, in addition to the import command for PostgreSQL, also
run:

    cat /usr/share/zabbix-sql-scripts/postgresql/timescaledb.sql | sudo -u zabbix psql zabbix

::: notewarning
TimescaleDB is supported with Zabbix server
only.
:::

[comment]: # ({/e53454de-99a402fa})


[comment]: # ({b5f642c5-42b2b5fa})
#### SELinux configuration
Zabbix uses socket-based inter-process communication. On systems where SELinux is enabled, it may be required to add SELinux rules to allow Zabbix create/use UNIX domain sockets in the SocketDir directory. Currently socket files are used by server (alerter, preprocessing, IPMI) and proxy (IPMI). Socket files are persistent, meaning they are present while the process is running.

Having SELinux status enabled in enforcing mode, you need to execute the
following commands to enable communication between Zabbix frontend and
server:

RHEL 7 and later:

    # setsebool -P httpd_can_connect_zabbix on
    If the database is accessible over network (including 'localhost' in case of PostgreSQL), you need to allow Zabbix frontend to connect to the database too:
    # setsebool -P httpd_can_network_connect_db on

RHEL prior to 7:

    setsebool -P httpd_can_network_connect on
    setsebool -P zabbix_can_network on

After the frontend and SELinux configuration is done, restart the Apache
web server:

    service httpd restart

[comment]: # ({/b5f642c5-42b2b5fa})

[comment]: # ({68e957dd-3143c04c})
In addition, Zabbix provides the zabbix-selinux-policy package as part of source RPM packages for [RHEL 8](http://repo.zabbix.com/zabbix/6.5/rhel/8/SRPMS/) and [RHEL 7](http://repo.zabbix.com/zabbix/6.5/rhel/7/SRPMS/). This package provides a basic default policy for SELinux and makes zabbix components work out-of-the-box by allowing Zabbix to create and use sockets and enabling httpd connection to PostgreSQL (used by frontend). 

The source *zabbix_policy.te* file contains the following rules:  

    module zabbix_policy 1.2;

    require {
      type zabbix_t;
      type zabbix_port_t;
      type zabbix_var_run_t;
      type postgresql_port_t;
      type httpd_t;
      class tcp_socket name_connect;
      class sock_file { create unlink };
      class unix_stream_socket connectto;
    }
    
    #============= zabbix_t ==============
    allow zabbix_t self:unix_stream_socket connectto;
    allow zabbix_t zabbix_port_t:tcp_socket name_connect;
    allow zabbix_t zabbix_var_run_t:sock_file create;
    allow zabbix_t zabbix_var_run_t:sock_file unlink;
    allow httpd_t zabbix_port_t:tcp_socket name_connect;
    
    #============= httpd_t ==============
    allow httpd_t postgresql_port_t:tcp_socket name_connect;

This package has been created to prevent users from turning off SELinux because of the configuration complexity. It contains the default policy that is sufficient to speed up Zabbix deployment and configuration. For maximum security level, it is recommended to set custom SELinux settings. 

[comment]: # ({/68e957dd-3143c04c})

[comment]: # ({6e234c36-73fa82aa})
### Proxy installation

Once the required repository is added, you can install Zabbix proxy by
running:

    dnf install zabbix-proxy-mysql zabbix-sql-scripts

Substitute 'mysql' in the commands with 'pgsql' to use PostgreSQL, or
with 'sqlite3' to use SQLite3 (proxy only).

The package 'zabbix-sql-scripts' contains database schemas for all supported database management systems for both Zabbix server and Zabbix proxy and will be used for data import.

[comment]: # ({/6e234c36-73fa82aa})

[comment]: # ({fe6abb8e-fe6abb8e})
##### Creating database

[Create](/manual/appendix/install/db_scripts) a separate database for
Zabbix proxy.

Zabbix server and Zabbix proxy cannot use the same database. If they are
installed on the same host, the proxy database must have a different
name.

[comment]: # ({/fe6abb8e-fe6abb8e})

[comment]: # ({fa48c063-2ab835d7})
##### Importing data

Import initial schema:

    cat /usr/share/zabbix-sql-scripts/mysql/proxy.sql | mysql -uzabbix -p zabbix

For proxy with PostgreSQL (or SQLite):

    cat /usr/share/zabbix-sql-scripts/postgresql/proxy.sql | sudo -u zabbix psql zabbix
    cat /usr/share/zabbix-sql-scripts/sqlite3/proxy.sql | sqlite3 zabbix.db

[comment]: # ({/fa48c063-2ab835d7})

[comment]: # ({d14db67a-c3b44766})
##### Configure database for Zabbix proxy

Edit zabbix\_proxy.conf:

    vi /etc/zabbix/zabbix_proxy.conf
    DBHost=localhost
    DBName=zabbix
    DBUser=zabbix
    DBPassword=<password>

In DBName for Zabbix proxy use a separate database from Zabbix server.

In DBPassword use Zabbix database password for MySQL; PostgreSQL user
password for PostgreSQL.

Use `DBHost=` with PostgreSQL. You might want to keep the default
setting `DBHost=localhost` (or an IP address), but this would make
PostgreSQL use a network socket for connecting to Zabbix. See [SELinux
configuration](/manual/installation/install_from_packages/rhel#selinux_configuration)
for instructions.

[comment]: # ({/d14db67a-c3b44766})

[comment]: # ({5ee559b9-e4843785})
##### Starting Zabbix proxy process

To start a Zabbix proxy process and make it start at system boot:

    service zabbix-proxy start
    systemctl enable zabbix-proxy

[comment]: # ({/5ee559b9-e4843785})

[comment]: # ({871a973b-871a973b})
##### Frontend configuration

A Zabbix proxy does not have a frontend; it communicates with Zabbix
server only.

[comment]: # ({/871a973b-871a973b})

[comment]: # ({74368349-4875fc22})
### Java gateway installation

It is required to install [Java gateway](/manual/concepts/java) only if
you want to monitor JMX applications. Java gateway is lightweight and
does not require a database.

Once the required repository is added, you can install Zabbix Java
gateway by running:

    dnf install zabbix-java-gateway

Proceed to [setup](/manual/concepts/java/from_rhel) for more
details on configuring and running Java gateway.

[comment]: # ({/74368349-4875fc22})

[comment]: # ({6268c78e-f64e7f46})
### Installing debuginfo packages

:::noteclassic
 Debuginfo packages are currently available for RHEL
versions 9, 7, 6 and 5. 
::: 

To enable debuginfo repository, edit
*/etc/yum.repos.d/zabbix.repo* file. Change `enabled=0` to `enabled=1`
for zabbix-debuginfo repository.

    [zabbix-debuginfo]
    name=Zabbix Official Repository debuginfo - $basearch
    baseurl=http://repo.zabbix.com/zabbix/6.5/rhel/7/$basearch/debuginfo/
    enabled=0
    gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-ZABBIX-A14FE591
    gpgcheck=1

This will allow you to install the zabbix-debuginfo package.

    dnf install zabbix-debuginfo

This single package contains debug information for all binary Zabbix
components.

[comment]: # ({/6268c78e-f64e7f46})
