[comment]: # ({7900383c-7900383c})
# Upgrade from sources

[comment]: # ({/7900383c-7900383c})

[comment]: # ({05df148e-92c7a8e5})
#### Overview

This section provides the steps required for a successful
[upgrade](/manual/installation/upgrade) from Zabbix **6.4**.x to Zabbix
**7.0**.x using official Zabbix sources.

::: notewarning
Before the upgrade make sure to read the relevant
[**upgrade notes**](/manual/installation/upgrade)!
:::

You may also want to check the
[requirements](/manual/installation/requirements) for 7.0.

::: notetip
It may be handy to run two parallel SSH sessions during
the upgrade, executing the upgrade steps in one and monitoring the
server/proxy logs in another. For example, run
`tail -f zabbix_server.log` or `tail -f zabbix_proxy.log` in the second
SSH session showing you the latest log file entries and possible errors
in real time. This can be critical for production
instances.
:::

[comment]: # ({/05df148e-92c7a8e5})

[comment]: # ({101d6faf-101d6faf})
#### Server upgrade process

[comment]: # ({/101d6faf-101d6faf})

[comment]: # ({b8308740-b8308740})
##### 1 Stop server

Stop Zabbix server to make sure that no new data is inserted into
database.

[comment]: # ({/b8308740-b8308740})

[comment]: # ({ab13a6a4-ab13a6a4})
##### 2 Back up the existing Zabbix database

This is a very important step. Make sure that you have a backup of your
database. It will help if the upgrade procedure fails (lack of disk
space, power off, any unexpected problem).

[comment]: # ({/ab13a6a4-ab13a6a4})

[comment]: # ({d2778675-d2778675})
##### 3 Back up configuration files, PHP files and Zabbix binaries

Make a backup copy of Zabbix binaries, configuration files and the PHP
file directory.

[comment]: # ({/d2778675-d2778675})

[comment]: # ({2bb75ddd-2bb75ddd})
##### 4 Install new server binaries

Use these
[instructions](/manual/installation/install#installing_zabbix_daemons)
to compile Zabbix server from sources.

[comment]: # ({/2bb75ddd-2bb75ddd})

[comment]: # ({0af0db83-ec6edae7})
##### 5 Review server configuration parameters

Make sure to review [Upgrade notes](/manual/installation/upgrade_notes_700) to check if any changes in the configuration parameters are required.

For new optional parameters, see the [What's
new](/manual/introduction/whatsnew700) page.

[comment]: # ({/0af0db83-ec6edae7})

[comment]: # ({a01e967f-a01e967f})
##### 6 Start new Zabbix binaries

Start new binaries. Check log files to see if the binaries have started
successfully.

Zabbix server will automatically upgrade the database. When starting up,
Zabbix server reports the current (mandatory and optional) and required
database versions. If the current mandatory version is older than the
required version, Zabbix server automatically executes the required
database upgrade patches. The start and progress level (percentage) of
the database upgrade is written to the Zabbix server log file. When the
upgrade is completed, a "database upgrade fully completed" message is
written to the log file. If any of the upgrade patches fail, Zabbix
server will not start. Zabbix server will also not start if the current
mandatory database version is newer than the required one. Zabbix server
will only start if the current mandatory database version corresponds to
the required mandatory version.

    8673:20161117:104750.259 current database version (mandatory/optional): 03040000/03040000
    8673:20161117:104750.259 required mandatory version: 03040000

Before you start the server:

-   Make sure the database user has enough permissions (create table,
    drop table, create index, drop index)
-   Make sure you have enough free disk space.

[comment]: # ({/a01e967f-a01e967f})

[comment]: # ({c1ead36e-22e0e4e9})
##### 7 Install new Zabbix web interface

The minimum required PHP version is 7.4. Update if needed and follow
[installation instructions](/manual/installation/frontend).


[comment]: # ({/c1ead36e-22e0e4e9})

[comment]: # ({49e4f43e-49e4f43e})
##### 8 Clear web browser cookies and cache

After the upgrade you may need to clear web browser cookies and web
browser cache for the Zabbix web interface to work properly.

[comment]: # ({/49e4f43e-49e4f43e})

[comment]: # ({75f5b3ed-75f5b3ed})
#### Proxy upgrade process

[comment]: # ({/75f5b3ed-75f5b3ed})

[comment]: # ({f33e6ddb-f33e6ddb})
##### 1 Stop proxy

Stop Zabbix proxy.

[comment]: # ({/f33e6ddb-f33e6ddb})

[comment]: # ({2e03c550-2e03c550})
##### 2 Back up configuration files and Zabbix proxy binaries

Make a backup copy of the Zabbix proxy binary and configuration file.

[comment]: # ({/2e03c550-2e03c550})

[comment]: # ({f9bb92b8-024201dc})
##### 3 Install new proxy binaries

Use these
[instructions](/manual/installation/install#installing-zabbix-daemons)
to compile Zabbix proxy from sources.

[comment]: # ({/f9bb92b8-024201dc})

[comment]: # ({2fbda571-2fbda571})
##### 4 Review proxy configuration parameters

There are no mandatory changes in this version to proxy
[parameters](/manual/appendix/config/zabbix_proxy).

[comment]: # ({/2fbda571-2fbda571})

[comment]: # ({9177a792-520f43c6})
##### 5 Start new Zabbix proxy

Start the new Zabbix proxy. Check log files to see if the proxy has
started successfully.

Zabbix proxy will automatically upgrade the database. Database upgrade
takes place similarly as when starting [Zabbix
server](/manual/installation/upgrade/sources#start-new-zabbix-binaries).

[comment]: # ({/9177a792-520f43c6})

[comment]: # ({59736bba-59736bba})
#### Agent upgrade process

::: noteimportant
Upgrading agents is not mandatory. You only need
to upgrade agents if it is required to access the new
functionality.
:::

The upgrade procedure described in this section may be used for
upgrading both the Zabbix agent and the Zabbix agent 2.

[comment]: # ({/59736bba-59736bba})

[comment]: # ({02698e69-02698e69})
##### 1 Stop agent

Stop Zabbix agent.

[comment]: # ({/02698e69-02698e69})

[comment]: # ({50263823-50263823})
##### 2 Back up configuration files and Zabbix agent binaries

Make a backup copy of the Zabbix agent binary and configuration file.

[comment]: # ({/50263823-50263823})

[comment]: # ({467f56b3-467f56b3})
##### 3 Install new agent binaries

Use these
[instructions](/manual/installation/install#installing_zabbix_daemons)
to compile Zabbix agent from sources.

Alternatively, you may download pre-compiled Zabbix agents from the
[Zabbix download page](http://www.zabbix.com/download.php).

[comment]: # ({/467f56b3-467f56b3})

[comment]: # ({34721aad-34721aad})
##### 4 Review agent configuration parameters

There are no mandatory changes in this version neither to
[agent](/manual/appendix/config/zabbix_agentd) nor to [agent
2](/manual/appendix/config/zabbix_agent2) parameters.

[comment]: # ({/34721aad-34721aad})

[comment]: # ({04c253a7-04c253a7})
##### 5 Start new Zabbix agent

Start the new Zabbix agent. Check log files to see if the agent has
started successfully.

[comment]: # ({/04c253a7-04c253a7})

[comment]: # ({8b73a4dd-a06019d2})
#### Upgrade between minor versions

When upgrading between minor versions of 7.0.x (for example from 7.0.1
to 7.0.3) it is required to execute the same actions for
server/proxy/agent as during the upgrade between major versions. The
only difference is that when upgrading between minor versions no changes
to the database are made.

[comment]: # ({/8b73a4dd-a06019d2})
