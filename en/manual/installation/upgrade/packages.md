[comment]: # ({490a37c2-490a37c2})
# Upgrade from packages

[comment]: # ({/490a37c2-490a37c2})

[comment]: # ({d142a0e9-0fa07e9f})
#### Overview

This section provides the steps required for a successful
[upgrade](/manual/installation/upgrade) using official RPM and DEB
packages provided by Zabbix for:

-   [Red Hat Enterprise
    Linux](/manual/installation/upgrade/packages/rhel)
-   [Debian/Ubuntu](/manual/installation/upgrade/packages/debian_ubuntu)

[comment]: # ({/d142a0e9-0fa07e9f})

[comment]: # ({69a2013d-69a2013d})
##### Zabbix packages from OS repositories

Often, OS distributions (in particular, Debian-based distributions)
provide their own Zabbix packages.\
Note, that these packages are not supported by Zabbix, they are
typically out of date and lack the latest features and bug fixes. Only
the packages from [repo.zabbix.com](https://repo.zabbix.com/) are
officially supported.

If you are upgrading from packages provided by OS distributions (or had
them installed at some point), follow this procedure to switch to
official Zabbix packages:

1.  Always uninstall the old packages first.
2.  Check for residual files that may have been left after
    deinstallation.
3.  Install official packages following [installation
    instructions](https://www.zabbix.com/download) provided by Zabbix.

Never do a direct update, as this may result in a broken installation.

[comment]: # ({/69a2013d-69a2013d})
