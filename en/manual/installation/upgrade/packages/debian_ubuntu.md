[comment]: # ({91aab3e5-91aab3e5})
# 2 Debian/Ubuntu

[comment]: # ({/91aab3e5-91aab3e5})

[comment]: # ({8b6296d5-d02acea9})
#### Overview

This section provides the steps required for a successful
[upgrade](/manual/installation/upgrade) from Zabbix **6.4**.x to Zabbix
**7.0**.x using official Zabbix packages for Debian/Ubuntu.

::: notewarning
Before the upgrade make sure to read the relevant
[**upgrade notes**](/manual/installation/upgrade)!
:::

You may also want to check the
[requirements](/manual/installation/requirements) for 7.0.

::: notetip
It may be handy to run two parallel SSH sessions during
the upgrade, executing the upgrade steps in one and monitoring the
server/proxy logs in another. For example, run
`tail -f zabbix_server.log` or `tail -f zabbix_proxy.log` in the second
SSH session showing you the latest log file entries and possible errors
in real time. This can be critical for production
instances.
:::

[comment]: # ({/8b6296d5-d02acea9})

[comment]: # ({93ff8b03-93ff8b03})
#### Upgrade procedure

[comment]: # ({/93ff8b03-93ff8b03})

[comment]: # ({cc3d8cb3-f8102233})
##### 1 Stop Zabbix processes

Stop Zabbix server to make sure that no new data is inserted into
database.

    service zabbix-server stop

If upgrading Zabbix proxy, stop proxy too.

    service zabbix-proxy stop

[comment]: # ({/cc3d8cb3-f8102233})

[comment]: # ({ab13a6a4-ab13a6a4})
##### 2 Back up the existing Zabbix database

This is a very important step. Make sure that you have a backup of your
database. It will help if the upgrade procedure fails (lack of disk
space, power off, any unexpected problem).

[comment]: # ({/ab13a6a4-ab13a6a4})

[comment]: # ({59495358-6c141723})
##### 3 Back up configuration files, PHP files and Zabbix binaries

Make a backup copy of Zabbix binaries, configuration files and the PHP
file directory.

Configuration files:

    mkdir /opt/zabbix-backup/
    cp /etc/zabbix/zabbix_server.conf /opt/zabbix-backup/
    cp /etc/apache2/conf-enabled/zabbix.conf /opt/zabbix-backup/

PHP files and Zabbix binaries:

    cp -R /usr/share/zabbix/ /opt/zabbix-backup/
    cp -R /usr/share/zabbix-* /opt/zabbix-backup/

[comment]: # ({/59495358-6c141723})

[comment]: # ({4edc6fb7-31c75b55})
##### 4 Update repository configuration package

To proceed with the update your current repository package has to be
uninstalled.

    rm -Rf /etc/apt/sources.list.d/zabbix.list

Then install the new repository configuration package.

On **Debian 12** run:

    wget https://repo.zabbix.com/zabbix/6.5/debian/pool/main/z/zabbix-release/zabbix-release_6.5-1+debian12_all.deb
    dpkg -i zabbix-release_6.5-1+debian12_all.deb

On **Debian 11** run:

    wget https://repo.zabbix.com/zabbix/6.5/debian/pool/main/z/zabbix-release/zabbix-release_6.5-1+debian11_all.deb
    dpkg -i zabbix-release_6.5-1+debian11_all.deb

On **Debian 10** run:

    wget https://repo.zabbix.com/zabbix/6.5/debian/pool/main/z/zabbix-release/zabbix-release_6.5-1+debian10_all.deb
    dpkg -i zabbix-release_6.5-1+debian10_all.deb

On **Debian 9** run:

    wget https://repo.zabbix.com/zabbix/6.5/debian/pool/main/z/zabbix-release/zabbix-release_6.5-1+debian9_all.deb
    dpkg -i zabbix-release_6.5-1+debian9_all.deb

On **Ubuntu 22.04** run:

    wget https://repo.zabbix.com/zabbix/6.5/ubuntu/pool/main/z/zabbix-release/zabbix-release_6.5-1+ubuntu22.04_all.deb
    dpkg -i zabbix-release_6.5-1+ubuntu22.04_all.deb

On **Ubuntu 20.04** run:

    wget https://repo.zabbix.com/zabbix/6.5/ubuntu/pool/main/z/zabbix-release/zabbix-release_6.5-1+ubuntu20.04_all.deb
    dpkg -i zabbix-release_6.5-1+ubuntu20.04_all.deb

On **Ubuntu 18.04** run:

    wget https://repo.zabbix.com/zabbix/6.5/ubuntu/pool/main/z/zabbix-release/zabbix-release_6.5-1+ubuntu18.04_all.deb
    dpkg -i zabbix-release_6.5-1+ubuntu18.04_all.deb

Update the repository information.

    apt-get update

[comment]: # ({/4edc6fb7-31c75b55})

[comment]: # ({b6b189d0-08c7383c})
##### 5 Upgrade Zabbix components

To upgrade Zabbix components you may run something like:

    apt-get install --only-upgrade zabbix-server-mysql zabbix-frontend-php zabbix-agent

If using PostgreSQL, substitute `mysql` with `pgsql` in the command. If
upgrading the proxy, substitute `server` with `proxy` in the command. If
upgrading the Zabbix agent 2, substitute `zabbix-agent` with
`zabbix-agent2` in the command.

Then, to upgrade the web frontend with Apache correctly, also run:

    apt-get install zabbix-apache-conf

[comment]: # ({/b6b189d0-08c7383c})

[comment]: # ({f1d3f770-455f4e97})
##### 6 Review component configuration parameters

Make sure to review [Upgrade notes](/manual/installation/upgrade_notes_700) to check if any changes in the configuration parameters are required.

For new optional parameters, see the [What's
new](/manual/introduction/whatsnew700) page.

[comment]: # ({/f1d3f770-455f4e97})

[comment]: # ({44e5c261-624a8fc7})
##### 7 Start Zabbix processes

Start the updated Zabbix components.

    service zabbix-server start
    service zabbix-proxy start
    service zabbix-agent start
    service zabbix-agent2 start

[comment]: # ({/44e5c261-624a8fc7})

[comment]: # ({49e4f43e-49e4f43e})
##### 8 Clear web browser cookies and cache

After the upgrade you may need to clear web browser cookies and web
browser cache for the Zabbix web interface to work properly.

[comment]: # ({/49e4f43e-49e4f43e})

[comment]: # ({93148a26-9bab02f2})
#### Upgrade between minor versions

It is possible to upgrade minor versions of 7.0.x (for example, from
7.0.1 to 7.0.3). It is easy.

To upgrade Zabbix minor version please run:

    sudo apt install --only-upgrade 'zabbix.*'

To upgrade Zabbix server minor version please run:

    sudo apt install --only-upgrade 'zabbix-server.*'

To upgrade Zabbix agent minor version please run:

    sudo apt install --only-upgrade 'zabbix-agent.*'

or, for Zabbix agent 2:

    sudo apt install --only-upgrade 'zabbix-agent2.*'

[comment]: # ({/93148a26-9bab02f2})
