[comment]: # ({e2c1904c-e2c1904c})
# 4 Installation from packages

[comment]: # ({/e2c1904c-e2c1904c})

[comment]: # ({e0c0ed0f-8d855b7c})
#### From Zabbix official repository

Zabbix SIA provides official RPM and DEB packages for:

-   [Red Hat Enterprise
    Linux](/manual/installation/install_from_packages/rhel)
-   [Debian/Ubuntu/Raspbian](/manual/installation/install_from_packages/debian_ubuntu)
-   [SUSE Linux Enterprise
    Server](/manual/installation/install_from_packages/suse)

Package files for yum/dnf, apt and zypper repositories for various OS
distributions are available at [repo.zabbix.com](https://repo.zabbix.com/).

Some OS distributions (in particular, Debian-based distributions) provide their own Zabbix packages. Note that these packages are **not**
supported by Zabbix. Third-party Zabbix packages can be out of date and may lack the latest features and bug fixes. It is
recommended to use only the official packages from [repo.zabbix.com](https://repo.zabbix.com/). If you have previously used
unofficial Zabbix packages, see notes about [upgrading the Zabbix packages from OS repositories](/manual/installation/upgrade/packages#zabbix_packages_from_os_repositories).

[comment]: # ({/e0c0ed0f-8d855b7c})
