<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="en" datatype="plaintext" original="manual/installation/requirements/best_practices.md">
    <body>
      <trans-unit id="7ef4b2b9" xml:space="preserve">
        <source># Best practices for secure Zabbix setup</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/installation/requirements/best_practices.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="b9719f19" xml:space="preserve">
        <source>### Overview

This section contains best practices that should be observed in order to
set up Zabbix in a secure way.

The practices contained here are not required for the functioning of
Zabbix. They are recommended for better security of the system.</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/installation/requirements/best_practices.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="cebf8306" xml:space="preserve">
        <source>### Access control</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/installation/requirements/best_practices.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="dc572a02" xml:space="preserve">
        <source>#### Principle of least privilege

The principle of least privilege should be used at all times for Zabbix.
This principle means that user accounts (in Zabbix frontend) or process
user (for Zabbix server/proxy or agent) have only those privileges that
are essential to perform intended functions. In other words, user
accounts at all times should run with as few privileges as possible.

::: noteimportant
Giving extra permissions to 'zabbix' user will
allow it to access configuration files and execute operations that can
compromise the overall security of the infrastructure.
:::

When implementing the least privilege principle for user accounts,
Zabbix [frontend user
types](/manual/config/users_and_usergroups/permissions) should be taken
into account. It is important to understand that while a "Admin" user
type has less privileges than "Super Admin" user type, it has
administrative permissions that allow managing configuration and execute
custom scripts.

::: noteclassic
Some information is available even for non-privileged users.
For example, while *Alerts* → *Scripts* is not available for
non-Super Admins, scripts themselves are available for retrieval by
using Zabbix API. Limiting script permissions and not adding sensitive
information (like access credentials, etc) should be used to avoid
exposure of sensitive information available in global
scripts.
:::</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/installation/requirements/best_practices.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="1631be73" xml:space="preserve">
        <source>#### Secure user for Zabbix agent

In the default configuration, Zabbix server and Zabbix agent processes
share one 'zabbix' user. If you wish to make sure that the agent cannot
access sensitive details in server configuration (e.g. database login
information), the agent should be run as a different user:

1.  Create a secure user
2.  Specify this user in the agent [configuration
    file](/manual/appendix/config/zabbix_agentd) ('User' parameter)
3.  Restart the agent with administrator privileges. Privileges will be
    dropped to the specified user.</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/installation/requirements/best_practices.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="c02c850f" xml:space="preserve">
        <source>#### Revoke write access to SSL configuration file in Windows

Zabbix Windows agent compiled with OpenSSL will try to reach the SSL
configuration file in c:\\openssl-64bit. The "openssl-64bit" directory
on disk C: can be created by non-privileged users.

So for security hardening, it is required to create this directory
manually and revoke write access from non-admin users.

Please note that the directory names will be different on 32-bit and
64-bit versions of Windows.</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/installation/requirements/best_practices.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="6cfe5a3b" xml:space="preserve">
        <source>### Cryptography</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/installation/requirements/best_practices.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="f8f5054e" xml:space="preserve">
        <source>#### Setting up SSL for Zabbix frontend

On RHEL, install mod\_ssl package:

    dnf install mod_ssl

Create directory for SSL keys:

    mkdir -p /etc/httpd/ssl/private
    chmod 700 /etc/httpd/ssl/private

Create SSL certificate:

    openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/httpd/ssl/private/apache-selfsigned.key -out /etc/httpd/ssl/apache-selfsigned.crt

Fill out the prompts appropriately. The most important line is the one
that requests the Common Name. You need to enter the domain name that
you want to be associated with your server. You can enter the public IP
address instead if you do not have a domain name. We will use
*example.com* in this article.

    Country Name (2 letter code) [XX]:
    State or Province Name (full name) []:
    Locality Name (eg, city) [Default City]:
    Organization Name (eg, company) [Default Company Ltd]:
    Organizational Unit Name (eg, section) []:
    Common Name (eg, your name or your server's hostname) []:example.com
    Email Address []:

Edit Apache SSL configuration:

    /etc/httpd/conf.d/ssl.conf

    DocumentRoot "/usr/share/zabbix"
    ServerName example.com:443
    SSLCertificateFile /etc/httpd/ssl/apache-selfsigned.crt
    SSLCertificateKeyFile /etc/httpd/ssl/private/apache-selfsigned.key

Restart the Apache service to apply the changes:

    systemctl restart httpd.service</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/installation/requirements/best_practices.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="40cfdec2" xml:space="preserve">
        <source>### Web server hardening</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/installation/requirements/best_practices.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="bb3706f4" xml:space="preserve">
        <source>#### Enabling Zabbix on root directory of URL

Add a virtual host to Apache configuration and set permanent redirect
for document root to Zabbix SSL URL. Do not forget to replace
*example.com* with the actual name of the server.

    /etc/httpd/conf/httpd.conf

    #Add lines

    &lt;VirtualHost *:*&gt;
        ServerName example.com
        Redirect permanent / https://example.com
    &lt;/VirtualHost&gt;

Restart the Apache service to apply the changes:

    systemctl restart httpd.service</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/installation/requirements/best_practices.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="c7ee0bb2" xml:space="preserve">
        <source>#### Enabling HTTP Strict Transport Security (HSTS) on the web server

To protect Zabbix frontend against protocol downgrade attacks, we
recommend to enable
[HSTS](https://en.wikipedia.org/wiki/HTTP_Strict_Transport_Security)
policy on the web server.

For example, to enable HSTS policy for your Zabbix frontend in Apache
configuration:

    /etc/httpd/conf/httpd.conf

add the following directive to your virtual host's configuration:

    &lt;VirtualHost *:443&gt;
       Header set Strict-Transport-Security "max-age=31536000"
    &lt;/VirtualHost&gt;

Restart the Apache service to apply the changes:

    systemctl restart httpd.service</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/installation/requirements/best_practices.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="cd09dcd1" xml:space="preserve">
        <source>#### Disabling web server information exposure

It is recommended to disable all web server signatures as part of the
web server hardening process. The web server is exposing software
signature by default:

![](../../../../assets/en/manual/installation/requirements/software_signature.png)

The signature can be disabled by adding two lines to the Apache (used as
an example) configuration file:

    ServerSignature Off
    ServerTokens Prod

PHP signature (X-Powered-By HTTP header) can be disabled by changing the
php.ini configuration file (signature is disabled by default):

    expose_php = Off

Web server restart is required for configuration file changes to be
applied.

Additional security level can be achieved by using the mod\_security
(package libapache2-mod-security2) with Apache. mod\_security allows to
remove server signature instead of only removing version from server
signature. Signature can be altered to any value by changing
"SecServerSignature" to any desired value after installing
mod\_security.

Please refer to documentation of your web server to find help on how to
remove/change software signatures.</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/installation/requirements/best_practices.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="720052da" xml:space="preserve">
        <source>#### Disabling default web server error pages

It is recommended to disable default error pages to avoid information
exposure. Web server is using built-in error pages by default:

![](../../../../assets/en/manual/installation/requirements/error_page_text.png)

Default error pages should be replaced/removed as part of the web server
hardening process. The "ErrorDocument" directive can be used to define a
custom error page/text for Apache web server (used as an example).

Please refer to documentation of your web server to find help on how to
replace/remove default error pages.</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/installation/requirements/best_practices.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="ba1547c0" xml:space="preserve">
        <source>#### Removing web server test page

It is recommended to remove the web server test page to avoid
information exposure. By default, web server webroot contains a test
page called index.html (Apache2 on Ubuntu is used as an example):

![](../../../../assets/en/manual/installation/requirements/test_page.png)

The test page should be removed or should be made unavailable as part of
the web server hardening process.</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/installation/requirements/best_practices.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="7e4e4a45" xml:space="preserve">
        <source>#### Set X-Frame-Options HTTP response header

By default, Zabbix is configured with *X-Frame-Options HTTP response
header* set to `SAMEORIGIN`, meaning that content can only be loaded in
a frame that has the same origin as the page itself.

Zabbix frontend elements that pull content from external URLs (namely,
the URL [dashboard
widget](/manual/web_interface/frontend_sections/dashboards/widgets/url))
display retrieved content in a sandbox with all sandboxing restrictions
enabled.

These settings enhance the security of the Zabbix frontend and provide
protection against XSS and clickjacking attacks. Super Admins can
[modify](/manual/web_interface/frontend_sections/administration/general#security)
*iframe sandboxing* and *X-Frame-Options HTTP response header*
parameters as needed. Please carefully weigh the risks and benefits
before changing default settings. Turning sandboxing or X-Frame-Options
off completely is not recommended.</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/installation/requirements/best_practices.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="3f76ae70" xml:space="preserve">
        <source>#### Hiding the file with list of common passwords

To increase the complexity of password brute force attacks, it is
suggested to limit access to the file `ui/data/top_passwords.txt` by
modifying web server configuration. This file contains a list of the
most common and context-specific passwords, and is used to prevent users
from setting such passwords if *Avoid easy-to-guess passwords* parameter
is enabled in the [password
policy](/manual/web_interface/frontend_sections/users/authentication#internal_authentication).

For example, on NGINX file access can be limited by using the `location`
directive:

    location = /data/top_passwords.txt {​​​​​​​
        deny all;
        return 404;
    }​​​​​​​

On Apache - by using `.htaccess` file:

    &lt;Files "top_passwords.txt"&gt;  
      Order Allow,Deny
      Deny from all
    &lt;/Files&gt;</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/installation/requirements/best_practices.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="40e70f9e" xml:space="preserve">
        <source>### UTF-8 encoding

UTF-8 is the only encoding supported by Zabbix. It is known to work
without any security flaws. Users should be aware that there are known
security issues if using some of the other encodings.</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/installation/requirements/best_practices.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="62eeb2ec" xml:space="preserve">
        <source>### Windows installer paths
When using Windows installers, it is recommended to use default paths provided by the installer as using custom paths without proper permissions could compromise the security of the installation.</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/installation/requirements/best_practices.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="ba0d4036" xml:space="preserve">
        <source>
### Zabbix Security Advisories and CVE database

See [Zabbix Security Advisories and CVE database](https://www.zabbix.com/security_advisories).</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/installation/requirements/best_practices.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
    </body>
  </file>
</xliff>
