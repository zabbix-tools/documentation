[comment]: # ({02b10f8d-02b10f8d})
# 2 Requirements

[comment]: # ({/02b10f8d-02b10f8d})

[comment]: # ({80330f65-80330f65})
#### Hardware

[comment]: # ({/80330f65-80330f65})

[comment]: # ({6a2790aa-4ae00568})
**Memory**

Zabbix requires both physical and disk memory. The amount of required disk 
memory obviously depends on the number of hosts and parameters that are 
being monitored. If you're planning to keep a long history of monitored 
parameters, you should be thinking of at least a couple of gigabytes to 
have enough space to store the history in the database. Each Zabbix daemon 
process requires several connections to a database server. The amount of 
memory allocated for the connection depends on the configuration of the 
database engine. 

::: noteclassic
The more physical memory you have, the faster the database (and therefore Zabbix) works. 
:::

[comment]: # ({/6a2790aa-4ae00568})

[comment]: # ({f102e6b1-7967309a})
**CPU**

Zabbix and especially Zabbix database may require significant CPU
resources depending on number of monitored parameters and chosen
database engine.

[comment]: # ({/f102e6b1-7967309a})

[comment]: # ({ff3b8ce1-1cbee7bf})
**Other hardware**

A serial communication port and a serial GSM modem are required for
using SMS notification support in Zabbix. USB-to-serial converter will
also work.

[comment]: # ({/ff3b8ce1-1cbee7bf})

[comment]: # ({430300b9-42d1d93e})
#### Examples of hardware configuration

The table provides examples of hardware configuration, assuming a **Linux/BSD/Unix** platform.

These are size and hardware configuration examples to start with. Each Zabbix installation is unique. 
Make sure to benchmark the performance of your Zabbix system in a staging or development environment, 
so that you can fully understand your requirements before deploying the Zabbix installation to its 
production environment.

|Installation size|Monitored metrics^**1**^|CPU/vCPU cores|Memory<br>(GiB)|Database|Amazon EC2^**2**^|
|-|-|-|-|--|--|
|Small|1 000|2|8|MySQL Server,<br>Percona Server,<br>MariaDB Server,<br>PostgreSQL|m6i.large/m6g.large|
|Medium|10 000|4|16|MySQL Server,<br>Percona Server,<br>MariaDB Server,<br>PostgreSQL|m6i.xlarge/m6g.xlarge|
|Large|100 000|16|64|MySQL Server,<br>Percona Server,<br>MariaDB Server,<br>PostgreSQL,<br>Oracle|m6i.4xlarge/m6g.4xlarge|
|Very large|1 000 000|32|96|MySQL Server,<br>Percona Server,<br>MariaDB Server,<br>PostgreSQL,<br>Oracle|m6i.8xlarge/m6g.8xlarge|

^**1**^ 1 metric = 1 item + 1 trigger + 1 graph<br>
^**2**^ Example with Amazon general purpose EC2 instances, using ARM64 or x86_64 architecture, a 
proper instance type like Compute/Memory/Storage optimised should be selected during Zabbix 
installation evaluation and testing before installing in its production environment.

::: noteclassic
Actual configuration depends on the number of active items
and refresh rates very much (see [database size](#database-size) section of this
page for details). It is highly recommended to run the database on a
separate box for large installations.
:::

[comment]: # ({/430300b9-42d1d93e})

[comment]: # ({ad0c32cf-ad0c32cf})
#### Supported platforms

Due to security requirements and the mission-critical nature of the
monitoring server, UNIX is the only operating system that can
consistently deliver the necessary performance, fault tolerance, and
resilience. Zabbix operates on market-leading versions.

Zabbix components are available and tested for the following platforms:

|Platform|Server|Agent|Agent2|
|--------|------|-----|------|
|Linux|x|x|x|
|IBM AIX|x|x|\-|
|FreeBSD|x|x|\-|
|NetBSD|x|x|\-|
|OpenBSD|x|x|\-|
|HP-UX|x|x|\-|
|Mac OS X|x|x|\-|
|Solaris|x|x|\-|
|Windows|\-|x|x|

::: noteclassic
Zabbix server/agent may work on other Unix-like operating
systems as well. Zabbix agent is supported on all Windows desktop and
server versions since XP.
:::

::: noteimportant
Zabbix disables core dumps if compiled with
encryption and does not start if the system does not allow disabling of
core dumps.
:::

[comment]: # ({/ad0c32cf-ad0c32cf})

[comment]: # ({323b6241-323b6241})
#### Required software

Zabbix is built around modern web servers, leading database engines, and
PHP scripting language.

[comment]: # ({/323b6241-323b6241})

[comment]: # ({40572773-fd454df8})
##### Third-party external surrounding software

Mandatory requirements are needed always. Optional requirements are
needed for the support of the specific function.

|Software|Mandatory status|Supported versions|Comments|
|--|-|-|------|
|*MySQL/Percona*|One of|8.0.30 and newer|Required if MySQL (or Percona) is used as Zabbix backend database. InnoDB engine is required.<br><br>We recommend using the [C API (libmysqlclient)](https://dev.mysql.com/downloads/c-api/) library for building server/proxy.|
|*MariaDB*|^|10.5.00-10.11.X|InnoDB engine is required.<br><br>We recommend using the [MariaDB Connector/C](https://downloads.mariadb.org/connector-c/) library for building server/proxy.<br><br>See also: [Possible deadlocks with MariaDB](/manual/installation/known_issues#possible-deadlocks-with-mysqlmariadb).|
|*Oracle*|^|19c - 21c|Required if Oracle is used as Zabbix backend database.|
|*PostgreSQL*|^|13.0-15.X|Required if PostgreSQL is used as Zabbix backend database.<br>Depending on the installation size, it might be required to increase PostgreSQL *work_mem* configuration property (4MB being the default value), so that the amount of memory used by the database for particular operation is sufficient and query execution does not take too much time.|
|*TimescaleDB* for PostgreSQL|^|2.0.1-2.11|Required if TimescaleDB is used as a PostgreSQL database extension. Make sure to install TimescaleDB Community Edition, which supports compression.<br><br>Note that PostgreSQL 15 is supported since TimescaleDB 2.10.<br>You may also refer to the [official documentation](https://docs.timescale.com/self-hosted/latest/upgrades/upgrade-pg/) for details regarding PostgreSQL and TimescaleDB version compatibility.|
|*SQLite*|Optional|3.3.5-3.34.X|SQLite is only supported with Zabbix proxies. Required if SQLite is used as Zabbix proxy database.|
|*smartmontools*|^|7.1 or later|Required for Zabbix agent 2.|
|*who*|^| |Required for the user count plugin.|
|*dpkg*|^| |Required for the system.sw.packages plugin.|
|*pkgtool*|^| |Required for the system.sw.packages plugin.|
|*rpm*|^| |Required for the system.sw.packages plugin.|
|*pacman*|^| |Required for the system.sw.packages plugin.|
|*q applets*|^| |`qlist` and `qsize`, as part of [q applets](https://wiki.gentoo.org/wiki/Q_applets), are required for the system.sw.packages plugin on Gentoo Linux.|

::: noteclassic
 Although Zabbix can work with databases available in the
operating systems, for the best experience, we recommend using databases
installed from the official database developer repositories.
:::

[comment]: # ({/40572773-fd454df8})

[comment]: # ({58fd4f73-75f0586e})
##### Frontend

The minimum supported screen width for Zabbix frontend is 1200px.

Mandatory requirements are needed always. Optional requirements are
needed for the support of the specific function.

|Software|Mandatory status|Version|Comments|
|--|-|-|------|
|*Apache*|yes|1.3.12 or later| |
|*PHP*|^|7.4.0 - 8.2.X|It is recommended to use PHP 8.0 or newer, because PHP 7.4 is no longer supported by the vendor.|
|PHP extensions:|<|<|<|
|*gd*|yes|2.0.28 or later|PHP GD extension must support PNG images (*--with-png-dir*), JPEG (*--with-jpeg-dir*) images and FreeType 2 (*--with-freetype-dir*).|
|*bcmath*|^| |php-bcmath (*--enable-bcmath*)|
|*ctype*|^| |php-ctype (*--enable-ctype*)|
|*libXML*|^|2.6.15 or later|php-xml, if provided as a separate package by the distributor.|
|*xmlreader*|^| |php-xmlreader, if provided as a separate package by the distributor.|
|*xmlwriter*|^| |php-xmlwriter, if provided as a separate package by the distributor.|
|*session*|^| |php-session, if provided as a separate package by the distributor.|
|*sockets*|^| |php-net-socket (*--enable-sockets*). Required for user script support.|
|*mbstring*|^| |php-mbstring (*--enable-mbstring*)|
|*gettext*|^| |php-gettext (*--with-gettext*). Required for translations to work.|
|*ldap*|No| |php-ldap. Required only if LDAP authentication is used in the frontend.|
|*openssl*|^| |php-openssl. Required only if SAML authentication is used in the frontend.|
|*mysqli*|^| |Required if MySQL is used as Zabbix backend database.|
|*oci8*|^| |Required if Oracle is used as Zabbix backend database.|
|*pgsql*|^| |Required if PostgreSQL is used as Zabbix backend database.|

Third-party frontend libraries that are supplied with Zabbix:

|Library|Mandatory status|Minimum version|Comments|
|--|-|-|------|
|[jQuery JavaScript Library](https://jquery.com/)|Yes|3.6.0|JavaScript library that simplifies the process of cross-browser development.|
|[jQuery UI](https://jqueryui.com/)|^|1.12.1|A set of user interface interactions, effects, widgets, and themes built on top of jQuery.|
|[OneLogin's SAML PHP Toolkit](https://github.com/onelogin/php-saml)|^|4.0.0|A PHP toolkit that adds SAML 2.0 authentication support to be able to sign in to Zabbix.|
|[Symfony Yaml Component](https://symfony.com/doc/current/components/yaml.html)|^|5.1.0|Adds support to export and import Zabbix configuration elements in the YAML format.|

::: noteclassic
Zabbix may work on previous versions of Apache, MySQL,
Oracle, and PostgreSQL as well.
:::

::: noteimportant
For other fonts than the default DejaVu, PHP
function
[imagerotate](http://php.net/manual/en/function.imagerotate.php) might
be required. If it is missing, these fonts might be rendered incorrectly
when a graph is displayed. This function is only available if PHP is
compiled with bundled GD, which is not the case in Debian and other
distributions.
:::

Third-party libraries used for writing and debugging Zabbix frontend code:

|Library|Mandatory status|Minimum version|Description|
|--|-|-|------|
|[Composer](https://getcomposer.org/)|No|2.4.1|An application-level package manager for PHP that provides a standard format for managing dependencies of PHP software and required libraries.|
|[PHPUnit](https://phpunit.de/)|^|8.5.29|A PHP unit testing framework for testing Zabbix frontend.|
|[SASS](https://sass-lang.com/)|^|3.4.22|A preprocessor scripting language that is interpreted and compiled into Cascading Style Sheets (CSS).|


[comment]: # ({/58fd4f73-75f0586e})

[comment]: # ({dda86afe-dda86afe})
##### Web browser on client side

Cookies and JavaScript must be enabled.

The latest stable versions of Google Chrome, Mozilla Firefox, Microsoft
Edge, Apple Safari, and Opera are supported.

::: notewarning
The same-origin policy for IFrames is implemented,
which means that Zabbix cannot be placed in frames on a different
domain.\
\
Still, pages placed into a Zabbix frame will have access to Zabbix
frontend (through JavaScript) if the page that is placed in the frame
and Zabbix frontend are on the same domain. A page like
`http://secure-zabbix.com/cms/page.html`, if placed into dashboards on
`http://secure-zabbix.com/zabbix/`, will have full JS access to
Zabbix.
:::

[comment]: # ({/dda86afe-dda86afe})

[comment]: # ({746e123a-096d332c})
##### Server/proxy

Mandatory requirements are needed always. Optional requirements are
needed for the support of the specific function.

|Requirement|Mandatory status|Description|
|--|-|-------|
|*libpcre/libpcre2*|One of|PCRE/PCRE2 library is required for [Perl Compatible Regular Expression](https://en.wikipedia.org/wiki/Perl_Compatible_Regular_Expressions) (PCRE) support.<br>The naming may differ depending on the GNU/Linux distribution, for example 'libpcre3' or 'libpcre1'. PCRE v8.x and PCRE2 v10.x (from Zabbix 6.0.0) are supported.|
|*libevent*|Yes|Required for inter-process communication. Version 1.4 or higher.|
|*libevent-pthreads*|^|Required for inter-process communication.|
|*libpthread*|^|Required for mutex and read-write lock support (could be part of libc).|
|*libresolv*|^|Required for DNS resolution (could be part of libc).|
|*libiconv*|^|Required for text encoding/format conversion (could be part of libc). Mandatory for Zabbix server on Linux.|
|*libz*|^|Required for compression support.|
|*libm*|^|Math library. Required by Zabbix server only.|
|*libmysqlclient*|One of|Required if MySQL is used.|
|*libmariadb*|^|Required if MariaDB is used.|
|*libclntsh*|^|Required if Oracle is used; *libclntsh* version must match or be higher than the version of the Oracle database used.|
|*libpq5*|^|Required if PostgreSQL is used; *libpq5* version must match or be higher than the version of the PostgreSQL database used.|
|*libsqlite3*|^|Required if Sqlite is used. Required for Zabbix proxy only.|
|*libOpenIPMI*|No|Required for IPMI support. Required for Zabbix server only.|
|*libssh2* or *libssh*|^|Required for [SSH checks](/manual/config/items/itemtypes/ssh_checks#overview). Version 1.0 or higher (libssh2); 0.9.0 or higher (libssh).<br>libssh is supported since Zabbix 4.4.6.|
|*libcurl*|^|Required for web monitoring, VMware monitoring, SMTP authentication, `web.page.*` Zabbix agent [items](/manual/config/items/itemtypes/zabbix_agent), HTTP agent items and Elasticsearch (if used). Version 7.28.0 or higher is recommended.<br>Libcurl version requirements:<br>- SMTP authentication: version 7.20.0 or higher<br>- Elasticsearch: version 7.28.0 or higher|
|*libxml2*|^|Required for VMware monitoring and XML XPath preprocessing.|
|*net-snmp*|^|Required for SNMP support. Version 5.3.0 or higher.<br> Support of strong encryption protocols (AES192/AES192C, AES256/AES256C) is available starting with net-snmp library 5.8; on RHEL 8+ based systems it is recommended to use net-snmp 5.8.15 or later.|
|*libunixodbc*|^|Required for database monitoring.|
|*libgnutls* or *libopenssl*|^|Required when using [encryption](/manual/encryption#compiling_zabbix_with_encryption_support).<br>Minimum versions: *libgnutls* - 3.1.18, *libopenssl* - 1.0.1|
|*libldap*|^|Required for LDAP support.|
|*fping*|^|Required for [ICMP ping items](/manual/config/items/itemtypes/simple_checks#icmp_pings).|

[comment]: # ({/746e123a-096d332c})

[comment]: # ({428d9730-800c8308})
##### Agent

|Requirement|Mandatory status|Description|
|--|-|-------|
|*libpcre/libpcre2*|One of|PCRE/PCRE2 library is required for [Perl Compatible Regular Expression](https://en.wikipedia.org/wiki/Perl_Compatible_Regular_Expressions) (PCRE) support.<br>The naming may differ depending on the GNU/Linux distribution, for example 'libpcre3' or 'libpcre1'. PCRE v8.x and PCRE2 v10.x (from Zabbix 6.0.0) are supported.<br>Required for log monitoring. Also required on Windows.|
|*libpthread*|Yes|Required for mutex and read-write lock support (could be part of libc). Not required on Windows.|
|*libresolv*|^|Required for DNS resolution (could be part of libc). Not required on Windows.|
|*libiconv*|^|Required for text encoding/format conversion to UTF-8 in log items, file content, file regex and regmatch items (could be part of libc). Not required on Windows.|
|*libgnutls* or *libopenssl*|No|Required if using [encryption](/manual/encryption#compiling_zabbix_with_encryption_support).<br>Minimum versions: *libgnutls* - 3.1.18, *libopenssl* - 1.0.1<br>On Microsoft Windows OpenSSL 1.1.1 or later is required.|
|*libldap*|^|Required if LDAP is used. Not supported on Windows.|
|*libcurl*|^|Required for `web.page.*` Zabbix agent [items](/manual/config/items/itemtypes/zabbix_agent). Not supported on Windows.<br>Version 7.28.0 or higher is recommended.|
|*libmodbus*|^|Only required if Modbus monitoring is used.<br>Version 3.0 or higher.|

::: noteclassic
 Starting from version 5.0.3, Zabbix agent will not work on
AIX platforms below versions 6.1 TL07 / AIX 7.1 TL01. 
:::

[comment]: # ({/428d9730-800c8308})

[comment]: # ({bba2b99e-4aa86212})
##### Agent 2

|Requirement|Mandatory status|Description|
|--|-|-------|
|*libpcre/libpcre2*|One of|PCRE/PCRE2 library is required for [Perl Compatible Regular Expression](https://en.wikipedia.org/wiki/Perl_Compatible_Regular_Expressions) (PCRE) support.<br>The naming may differ depending on the GNU/Linux distribution, for example 'libpcre3' or 'libpcre1'. PCRE v8.x and PCRE2 v10.x (from Zabbix 6.0.0) are supported.<br>Required for log monitoring. Also required on Windows.|
|*libopenssl*|No|Required when using encryption.<br>OpenSSL 1.0.1 or later is required on UNIX platforms.<br>The OpenSSL library must have PSK support enabled. LibreSSL is not supported.<br>On Microsoft Windows systems OpenSSL 1.1.1 or later is required.|

**Golang libraries**

|Requirement|Mandatory status|Minimum version|Description|
|--|-|-|-------|
|[git.zabbix.com/ap/plugin-support](https://git.zabbix.com/projects/AP/repos/plugin-support)|Yes|1.X.X|Zabbix own support library. Mostly for plugins. |
|[github.com/BurntSushi/locker](https://github.com/BurntSushi/locker)|^|0.0.0|Named read/write locks, access sync. |
|[github.com/chromedp/cdproto](https://github.com/chromedp/cdproto)|^|0.0.0|Generated commands, types, and events for the Chrome DevTools Protocol domains. |
|[github.com/chromedp/chromedp](https://github.com/chromedp/chromedp)|^|0.6.0|Chrome DevTools Protocol support (report generation). |
|[github.com/dustin/gomemcached](https://github.com/dustin/gomemcached)|^|0.0.0|A memcached binary protocol toolkit for go. |
|[github.com/eclipse/paho.mqtt.golang](https://github.com/eclipse/paho.mqtt.golang)|^|1.2.0|A library to handle MQTT connections. |
|[github.com/fsnotify/fsnotify](https://github.com/fsnotify/fsnotify)|^|1.4.9|Cross-platform file system notifications for Go. |
|[github.com/go-ldap/ldap](https://github.com/go-ldap/ldap)|^|3.0.3|Basic LDAP v3 functionality for the GO programming language. |
|[github.com/go-ole/go-ole](https://github.com/go-ole/go-ole)|^|1.2.4|Win32 ole implementation for golang. |
|[github.com/godbus/dbus](https://github.com/godbus/dbus)|^|4.1.0|Native Go bindings for D-Bus. |
|[github.com/go-sql-driver/mysql](https://github.com/go-sql-driver/mysql)|^|1.5.0|MySQL driver. |
|[github.com/godror/godror](https://github.com/godror/godror)|^|0.20.1|Oracle DB driver. |
|[github.com/mattn/go-sqlite3](https://github.com/mattn/go-sqlite3)|^|2.0.3|Sqlite3 driver. |
|[github.com/mediocregopher/radix/v3](https://github.com/mediocregopher/radix)|^|3.5.0|Redis client. |
|[github.com/memcachier/mc/v3](https://github.com/memcachier/mc)|^|3.0.1|Binary Memcached client. |
|[github.com/miekg/dns](https://github.com/miekg/dns)|^|1.1.43|DNS library. |
|[github.com/omeid/go-yarn](https://github.com/omeid/go-yarn)|^|0.0.1|Embeddable filesystem mapped key-string store. |
|[github.com/goburrow/modbus](https://github.com/goburrow/modbus)|^|0.1.0|Fault-tolerant implementation of Modbus. |
|[golang.org/x/sys](https://golang.org/x/sys)|^|0.0.0|Go packages for low-level interactions with the operating system.<br>Also used in plugin support lib. Used in MongoDB and PostgreSQL plugins. |
|[github.com/Microsoft/go-winio](https://github.com/microsoft/go-winio)|On Windows.<br>Yes, indirect**1**|0.6.0|Windows named pipe implementation.<br>Also used in plugin support lib. Used in MongoDB and PostgreSQL plugins. |
|[github.com/goburrow/serial](https://github.com/goburrow/serial)|Yes, indirect^**1**^|0.1.0|Serial library for Modbus. |
|[golang.org/x/xerrors](https://golang.org/x/xerrors)|^|0.0.0|Functions to manipulate errors. |
|[gopkg.in/asn1-ber.v1](https://gopkg.in/asn1-ber.v1)|^|1.0.0|Encoding/decoding library for ASN1 BER. |
|[github.com/go-stack/stack](https://github.com/go-stack/stack)|No, indirect^**1**^|1.8.0| |
|[github.com/golang/snappy](https://github.com/golang/snappy)|^|0.0.1| |
|[github.com/klauspost/compress](https://github.com/klauspost/compress)|^|1.13.6| |
|[github.com/xdg-go/pbkdf2](https://github.com/xdg-go/pbkdf2)|^|1.0.0| |
|[github.com/xdg-go/scram](https://github.com/xdg-go/scram)|^|1.0.2| |
|[github.com/xdg-go/stringprep](https://github.com/xdg-go/stringprep)|^|1.0.2| |
|[github.com/youmark/pkcs8](https://github.com/youmark/pkcs8)|^|0.0.0| |

^**1**^ "Indirect" means that it is used in one of the libraries that the agent uses. It's required since Zabbix uses the library that uses the package.

See also dependencies for loadable plugins:

-   [PostgreSQL](/manual/installation/requirements/plugins/postgresql)
-   [MongoDB](/manual/installation/requirements/plugins/mongodb)

[comment]: # ({/bba2b99e-4aa86212})

[comment]: # ({da65a6cf-149075a3})
##### Java gateway

If you obtained Zabbix from the source repository or an archive, then
the necessary dependencies are already included in the source tree.

If you obtained Zabbix from your distribution's package, then the
necessary dependencies are already provided by the packaging system.

In both cases above, the software is ready to be used and no additional
downloads are necessary.

If, however, you wish to provide your versions of these dependencies
(for instance, if you are preparing a package for some Linux
distribution), below is the list of library versions that Java gateway
is known to work with. Zabbix may work with other versions of these
libraries, too.

The following table lists JAR files that are currently bundled with Java
gateway in the original code:

|Library|Mandatory status|Minimum version|Comments|
|--|-|-|------|
|[android-json](https://mvnrepository.com/artifact/com.vaadin.external.google/android-json)|Yes|4.3r1|JSON (JavaScript Object Notation) is a lightweight data-interchange format. This is the org.json compatible Android implementation extracted from the Android SDK.|
|[logback-classic](https://mvnrepository.com/artifact/ch.qos.logback/logback-classic)|^|1.2.9| |
|[logback-core](https://mvnrepository.com/artifact/ch.qos.logback/logback-core)|^|1.2.9| |
|[slf4j-api](https://mvnrepository.com/artifact/org.slf4j/slf4j-api)|^|1.7.32| |

Java gateway can be built using either Oracle Java or open-source
OpenJDK (version 1.6 or newer). Packages provided by Zabbix are compiled
using OpenJDK. The table below provides information about OpenJDK
versions used for building Zabbix packages by distribution:

|Distribution|OpenJDK version|
|------------|---------------|
|RHEL 8|1.8.0|
|RHEL 7|1.8.0|
|SLES 15|11.0.4|
|SLES 12|1.8.0|
|Debian 10|11.0.8|
|Ubuntu 20.04|11.0.8|
|Ubuntu 18.04|11.0.8|

[comment]: # ({/da65a6cf-149075a3})

[comment]: # ({cb45070b-c8024c35})

#### Default port numbers

The following list of open ports per component is applicable for default configuration:

|Zabbix component|Port number|Protocol|Type of connection|
|-------|-------|-------|-------|
|Zabbix agent|10050|TCP|on demand|
|Zabbix agent 2|10050|TCP|on demand|
|Zabbix server|10051|TCP|on demand|
|Zabbix proxy|10051|TCP|on demand|
|Zabbix Java gateway|10052|TCP|on demand|
|Zabbix web service|10053|TCP|on demand|
|Zabbix frontend|80|HTTP|on demand|
|^|443|HTTPS|on demand|
|Zabbix trapper|10051 |TCP| on demand|

::: noteclassic
The port numbers should be open in firewall to enable Zabbix communications. Outgoing TCP connections usually do not require explicit firewall settings.
::: 

[comment]: # ({/cb45070b-c8024c35})

[comment]: # ({76e2aef2-1d73b238})
#### Database size

Zabbix configuration data require a fixed amount of disk space and do
not grow much.

Zabbix database size mainly depends on these variables, which define the
amount of stored historical data:

-   Number of processed values per second

This is the average number of new values Zabbix server receives every
second. For example, if we have 3000 items for monitoring with a refresh
rate of 60 seconds, the number of values per second is calculated as
3000/60 = **50**.

It means that 50 new values are added to Zabbix database every second.

-   Housekeeper settings for history

Zabbix keeps values for a fixed period of time, normally several weeks
or months. Each new value requires a certain amount of disk space for
data and index.

So, if we would like to keep 30 days of history and we receive 50 values
per second, the total number of values will be around
(**30**\*24\*3600)\* **50** = 129.600.000, or about 130M of values.

Depending on the database engine used, type of received values (floats,
integers, strings, log files, etc), the disk space for keeping a single
value may vary from 40 bytes to hundreds of bytes. Normally it is around
90 bytes per value for numeric items^**2**^. In our case, it means that
130M of values will require 130M \* 90 bytes = **10.9GB** of disk space.

::: noteclassic
The size of text/log item values is impossible to predict
exactly, but you may expect around 500 bytes per value.
:::

-   Housekeeper setting for trends

Zabbix keeps a 1-hour max/min/avg/count set of values for each item in
the table **trends**. The data is used for trending and long period
graphs. The one hour period can not be customized.

Zabbix database, depending on the database type, requires about 90 bytes
per each total. Suppose we would like to keep trend data for 5 years.
Values for 3000 items will require 3000\*24\*365\* **90** = **2.2GB**
per year, or **11GB** for 5 years.

-   Housekeeper settings for events

Each Zabbix event requires approximately 250 bytes of disk space^**1**^.
It is hard to estimate the number of events generated by Zabbix daily.
In the worst-case scenario, we may assume that Zabbix generates one
event per second.

For each recovered event, an event\_recovery record is created. Normally
most of the events will be recovered so we can assume one
event\_recovery record per event. That means additional 80 bytes per
event.

Optionally events can have tags, each tag record requiring approximately
100 bytes of disk space^**1**^. The number of tags per event (\#tags)
depends on configuration. So each will need an additional \#tags \* 100
bytes of disk space.

It means that if we want to keep 3 years of events, this would require
3\*365\*24\*3600\* (250+80+\#tags\*100) = **\~30GB**+\#tags\*100B disk
space^**2**^.

::: noteclassic
 ^**1**^ More when having non-ASCII event names, tags and values.\
^**2**^ The size approximations are based on MySQL and might be different for other databases. 
:::

The table contains formulas that can be used to calculate the disk space
required for Zabbix system:

|Parameter|Formula for required disk space (in bytes)|
|---------|------------------------------------------|
|*Zabbix configuration*|Fixed size. Normally 10MB or less.|
|*History*|days\*(items/refresh rate)\*24\*3600\*bytes<br>items : number of items<br>days : number of days to keep history<br>refresh rate : average refresh rate of items<br>bytes : number of bytes required to keep single value, depends on database engine, normally \~90 bytes.|
|*Trends*|days\*(items/3600)\*24\*3600\*bytes<br>items : number of items<br>days : number of days to keep history<br>bytes : number of bytes required to keep single trend, depends on the database engine, normally \~90 bytes.|
|*Events*|days\*events\*24\*3600\*bytes<br>events : number of event per second. One (1) event per second in worst-case scenario.<br>days : number of days to keep history<br>bytes : number of bytes required to keep single trend, depends on the database engine, normally \~330 + average number of tags per event \* 100 bytes.|

So, the total required disk space can be calculated as:\
**Configuration + History + Trends + Events**\
The disk space will NOT be used immediately after Zabbix installation.
Database size will grow then it will stop growing at some point, which
depends on housekeeper settings.

[comment]: # ({/76e2aef2-1d73b238})

[comment]: # ({520ea0fa-520ea0fa})
#### Time synchronization

It is very important to have precise system time on the server with
Zabbix running. [ntpd](http://www.ntp.org/) is the most popular daemon
that synchronizes the host's time with the time of other machines. It's
strongly recommended to maintain synchronized system time on all systems
Zabbix components are running on.

[comment]: # ({/520ea0fa-520ea0fa})

[comment]: # ({5c2693a2-9e23ba76})

#### Network requirements

A following list of open ports per component is applicable for default configuration.

|Port|Components |
|------------|---------------|
|Frontend|http on 80, https on 443|
|Server|10051 (for use with active proxy/agents)|
|Active Proxy |10051|
|Passive Proxy|10051|
|Agent2|10050|
|Trapper| |
|JavaGateway|10053|
|WebService|10053|

::: noteclassic
The port numbers should be opened in the firewall to enable external communications with Zabbix. Outgoing TCP connections usually do not require explicit firewall settings.
::: 

[comment]: # ({/5c2693a2-9e23ba76})
