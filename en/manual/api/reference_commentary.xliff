<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="en" datatype="plaintext" original="manual/api/reference_commentary.md">
    <body>
      <trans-unit id="6d056871" xml:space="preserve">
        <source># Appendix 1. Reference commentary</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/api/reference_commentary.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="6594bdc4" xml:space="preserve">
        <source>### Notation</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/api/reference_commentary.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="2df08058" xml:space="preserve">
        <source>#### Data types

The Zabbix API supports the following data types as input:

|Type|Description|
|----|-----------|
|boolean|A boolean value, accepts either `true` or `false`.|
|flag|The value is considered to be `true` if it is passed and not equal to `null` and `false` otherwise.|
|integer|A whole number.|
|float|A floating point number.|
|string|A text string.|
|text|A longer text string.|
|timestamp|A Unix timestamp.|
|array|An ordered sequence of values, that is, a plain array.|
|object|An associative array.|
|query|A value which defines, what data should be returned.&lt;br&gt;&lt;br&gt;Can be defined as an array of property names to return only specific properties, or as one of the predefined values:&lt;br&gt;`extend` - returns all object properties;&lt;br&gt;`count` - returns the number of retrieved records, supported only by certain subselects.|

::: noteimportant
Zabbix API always returns values as strings or
arrays only.
:::</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/api/reference_commentary.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="a2c16a18" xml:space="preserve">
        <source>#### Property behavior

Some of the object properties are marked with short labels to describe their behavior. The following labels are used:

-   *read-only* - the value of the property is set automatically and cannot be defined or changed by the user, even in some specific conditions
    (e.g., *read-only* for inherited objects or discovered objects);
-   *write-only* - the value of the property can be set, but cannot be accessed after;
-   *constant* - the value of the property can be set when creating an object, but cannot be changed after;
-   *supported* - the value of the property is not required to be set, but is allowed to be set in some specific conditions
    (e.g., *supported* if `type` is set to "Simple check", "External check", "SSH agent", "TELNET agent", or "HTTP agent");
-   *required* - the value of the property is required to be set for all operations (except get operations) or in some specific conditions
    (e.g., *required* for create operations; *required* if `operationtype` is set to "global script" and `opcommand_hst` is not set).

::: noteclassic
For update operations a property is considered as "set" when setting it during the update operation.
:::

Properties that are not marked with labels are optional.</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/api/reference_commentary.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="f255da9d" xml:space="preserve">
        <source>#### Parameter behavior

Some of the operation parameters are marked with short labels to describe their behavior for the operation.
The following labels are used:

-   *read-only* - the value of the parameter is set automatically and cannot be defined or changed by the user, even in some specific conditions
    (e.g., *read-only* for inherited objects or discovered objects);
-   *write-only* - the value of the parameter can be set, but cannot be accessed after;
-   *supported* - the value of the parameter is not required to be set, but is allowed to be set in some specific conditions
    (e.g., *supported* if `status` of Proxy object is set to "passive proxy");
-   *required* - the value of the parameter is required to be set.

Parameters that are not marked with labels are optional.</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/api/reference_commentary.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="5e99748d" xml:space="preserve">
        <source>### Reserved ID value "0"

Reserved ID value "0" can be used to filter elements and to remove
referenced objects. For example, to remove a referenced proxy from a
host, proxy\_hostid should be set to 0 ("proxy\_hostid": "0") or to
filter hosts monitored by server option proxyids should be set to 0
("proxyids": "0").</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/api/reference_commentary.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="b4e0f5ab" xml:space="preserve">
        <source>### Common "get" method parameters

The following parameters are supported by all `get` methods:

|Parameter|[Type](#data_types)|Description|
|---------|-------------------|-----------|
|countOutput|boolean|Return the number of records in the result instead of the actual data.|
|editable|boolean|If set to `true` return only objects that the user has write permissions to.&lt;br&gt;&lt;br&gt;Default: `false`.|
|excludeSearch|boolean|Return results that do not match the criteria given in the `search` parameter.|
|filter|object|Return only those results that exactly match the given filter.&lt;br&gt;&lt;br&gt;Accepts an array, where the keys are property names, and the values are either a single value or an array of values to match against.&lt;br&gt;&lt;br&gt;Doesn't work for `text` fields.|
|limit|integer|Limit the number of records returned.|
|output|query|Object properties to be returned.&lt;br&gt;&lt;br&gt;Default: `extend`.|
|preservekeys|boolean|Use IDs as keys in the resulting array.|
|search|object|Return results that match the given pattern (case-insensitive).&lt;br&gt;&lt;br&gt;Accepts an array, where the keys are property names, and the values are strings to search for. If no additional options are given, this will perform a `LIKE "%…%"` search.&lt;br&gt;&lt;br&gt;Works only for `string` and `text` fields.|
|searchByAny|boolean|If set to `true` return results that match any of the criteria given in the `filter` or `search` parameter instead of all of them.&lt;br&gt;&lt;br&gt;Default: `false`.|
|searchWildcardsEnabled|boolean|If set to `true` enables the use of "\*" as a wildcard character in the `search` parameter.&lt;br&gt;&lt;br&gt;Default: `false`.|
|sortfield|string/array|Sort the result by the given properties. Refer to a specific API get method description for a list of properties that can be used for sorting. Macros are not expanded before sorting.&lt;br&gt;&lt;br&gt;If no value is specified, data will be returned unsorted.|
|sortorder|string/array|Order of sorting. If an array is passed, each value will be matched to the corresponding property given in the `sortfield` parameter.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;`ASC` - *(default)* ascending;&lt;br&gt;`DESC` - descending.|
|startSearch|boolean|The `search` parameter will compare the beginning of fields, that is, perform a `LIKE "…%"` search instead.&lt;br&gt;&lt;br&gt;Ignored if `searchWildcardsEnabled` is set to `true`.|</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/api/reference_commentary.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="b41637d2" xml:space="preserve">
        <source>### Examples</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/api/reference_commentary.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="7a121fac" xml:space="preserve">
        <source>#### User permission check

Does the user have permission to write to hosts whose names begin with
"MySQL" or "Linux" ?

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "host.get",
    "params": {
        "countOutput": true,
        "search": {
            "host": ["MySQL", "Linux"]
        },
        "editable": true,
        "startSearch": true,
        "searchByAny": true
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": "0",
    "id": 1
}
```

::: noteclassic
Zero result means no hosts with read/write
permissions.
:::</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/api/reference_commentary.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="ea3cccd8" xml:space="preserve">
        <source>#### Mismatch counting

Count the number of hosts whose names do not contain the substring
"ubuntu"

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "host.get",
    "params": {
        "countOutput": true,
        "search": {
            "host": "ubuntu"
        },
        "excludeSearch": true
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": "44",
    "id": 1
}
```</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/api/reference_commentary.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="e911c08b" xml:space="preserve">
        <source>#### Searching for hosts using wildcards

Find hosts whose name contains word "server" and have interface ports
"10050" or "10071". Sort the result by host name in descending order and
limit it to 5 hosts.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "host.get",
    "params": {
        "output": ["hostid", "host"],
        "selectInterfaces": ["port"],
        "filter": {
            "port": ["10050", "10071"]
        },
        "search": {
            "host": "*server*"
        },
        "searchWildcardsEnabled": true,
        "searchByAny": true,
        "sortfield": "host",
        "sortorder": "DESC",
        "limit": 5
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": [
        {
            "hostid": "50003",
            "host": "WebServer-Tomcat02",
            "interfaces": [
                {
                    "port": "10071"
                }
            ]
        },
        {
            "hostid": "50005",
            "host": "WebServer-Tomcat01",
            "interfaces": [
                {
                    "port": "10071"
                }
            ]
        },
        {
            "hostid": "50004",
            "host": "WebServer-Nginx",
            "interfaces": [
                {
                    "port": "10071"
                }
            ]
        },
        {
            "hostid": "99032",
            "host": "MySQL server 01",
            "interfaces": [
                {
                    "port": "10050"
                }
            ]
        },
        {
            "hostid": "99061",
            "host": "Linux server 01",
            "interfaces": [
                {
                    "port": "10050"
                }
            ]
        }
    ],
    "id": 1
}
```</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/api/reference_commentary.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="8ce6f554" xml:space="preserve">
        <source>#### Searching for hosts using wildcards with "preservekeys"

If you add the parameter "preservekeys" to the previous request, the
result is returned as an associative array, where the keys are the id of
the objects.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "host.get",
    "params": {
        "output": ["hostid", "host"],
        "selectInterfaces": ["port"],
        "filter": {
            "port": ["10050", "10071"]
        },
        "search": {
            "host": "*server*"
        },
        "searchWildcardsEnabled": true,
        "searchByAny": true,
        "sortfield": "host",
        "sortorder": "DESC",
        "limit": 5,
        "preservekeys": true
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "50003": {
            "hostid": "50003",
            "host": "WebServer-Tomcat02",
            "interfaces": [
                {
                    "port": "10071"
                }
            ]
        },
        "50005": {
            "hostid": "50005",
            "host": "WebServer-Tomcat01",
            "interfaces": [
                {
                    "port": "10071"
                }
            ]
        },
        "50004": {
            "hostid": "50004",
            "host": "WebServer-Nginx",
            "interfaces": [
                {
                    "port": "10071"
                }
            ]
        },
        "99032": {
            "hostid": "99032",
            "host": "MySQL server 01",
            "interfaces": [
                {
                    "port": "10050"
                }
            ]
        },
        "99061": {
            "hostid": "99061",
            "host": "Linux server 01",
            "interfaces": [
                {
                    "port": "10050"
                }
            ]
        }
    },
    "id": 1
}
```</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/api/reference_commentary.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
    </body>
  </file>
</xliff>
