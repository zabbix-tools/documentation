[comment]: # ({8444baa3-52ad42a2})
# Configuration

This class is designed to export and import Zabbix configuration data.

Available methods:

-   [configuration.export](/manual/api/reference/configuration/export) - export configuration data
-   [configuration.import](/manual/api/reference/configuration/import) - import configuration data
-   [configuration.importcompare](/manual/api/reference/configuration/importcompare) - compare import file with current system elements

[comment]: # ({/8444baa3-52ad42a2})
