[comment]: # ({6a96957a-6a96957a})
# hostgroup.delete

[comment]: # ({/6a96957a-6a96957a})

[comment]: # ({849053ec-3c7f9c00})
### Description

`object hostgroup.delete(array hostGroupIds)`

This method allows to delete host groups.

A host group cannot be deleted if:

-   it contains hosts that belong to this group only;
-   it is marked as internal;
-   it is used by a host prototype;
-   it is used in a global script;
-   it is used in a correlation condition.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/849053ec-3c7f9c00})

[comment]: # ({af1f0187-af1f0187})
### Parameters

`(array)` IDs of the host groups to delete.

[comment]: # ({/af1f0187-af1f0187})

[comment]: # ({288daefc-288daefc})
### Return values

`(object)` Returns an object containing the IDs of the deleted host
groups under the `groupids` property.

[comment]: # ({/288daefc-288daefc})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({4122f4d9-a6d8d11e})
#### Deleting multiple host groups

Delete two host groups.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "hostgroup.delete",
    "params": [
        "107824",
        "107825"
    ],
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "groupids": [
            "107824",
            "107825"
        ]
    },
    "id": 1
}
```

[comment]: # ({/4122f4d9-a6d8d11e})

[comment]: # ({f496d50e-f496d50e})
### Source

CHostGroup::delete() in
*ui/include/classes/api/services/CHostGroup.php*.

[comment]: # ({/f496d50e-f496d50e})
