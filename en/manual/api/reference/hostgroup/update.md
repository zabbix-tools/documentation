[comment]: # ({259f747f-259f747f})
# hostgroup.update

[comment]: # ({/259f747f-259f747f})

[comment]: # ({44386689-32bf01b5})
### Description

`object hostgroup.update(object/array hostGroups)`

This method allows to update existing hosts groups.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/44386689-32bf01b5})

[comment]: # ({9c20e34d-9c20e34d})
### Parameters

`(object/array)` [Host group properties](object#host_group) to be
updated.

The `groupid` property must be defined for each host group, all other
properties are optional. Only the given properties will be updated, all
others will remain unchanged.

[comment]: # ({/9c20e34d-9c20e34d})

[comment]: # ({736f3b05-736f3b05})
### Return values

`(object)` Returns an object containing the IDs of the updated host
groups under the `groupids` property.

[comment]: # ({/736f3b05-736f3b05})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({f2ceab2e-a296ad88})
#### Renaming a host group

Rename a host group to "Linux hosts."

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "hostgroup.update",
    "params": {
        "groupid": "7",
        "name": "Linux hosts"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "groupids": [
            "7"
        ]
    },
    "id": 1
}
```

[comment]: # ({/f2ceab2e-a296ad88})

[comment]: # ({b75d89b4-b75d89b4})
### Source

CHostGroup::update() in
*ui/include/classes/api/services/CHostGroup.php*.

[comment]: # ({/b75d89b4-b75d89b4})
