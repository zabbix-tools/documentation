[comment]: # ({524e7e9d-524e7e9d})
# > Host group object

The following objects are directly related to the `hostgroup` API.

[comment]: # ({/524e7e9d-524e7e9d})

[comment]: # ({78626252-52ddc167})
### Host group

The host group object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|groupid|string|ID of the host group.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*<br>- *required* for update operations|
|name|string|Name of the host group.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* for create operations|
|flags|integer|Origin of the host group.<br><br>Possible values:<br>0 - a plain host group;<br>4 - a discovered host group.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*|
|uuid|string|Universal unique identifier, used for linking imported host groups to already existing ones. Auto-generated, if not given.|

[comment]: # ({/78626252-52ddc167})
