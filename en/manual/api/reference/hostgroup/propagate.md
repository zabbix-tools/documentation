[comment]: # ({de01ad95-f9e8f670})
# hostgroup.propagate


[comment]: # ({/de01ad95-f9e8f670})



[comment]: # ({84212e5d-b5ff62ed})
### Description

`object hostgroup.propagate(object parameters)`

This method allows to apply permissions and tag filters to all subgroups of a host group.

::: noteclassic
This method is only available to *Super admin* user types.
Permissions to call the method can be revoked in user role settings.
See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::


[comment]: # ({/84212e5d-b5ff62ed})



[comment]: # ({b150eede-6102edd8})
### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|groups|object/array|Host groups to propagate.<br><br>The host groups must have the `groupid` property defined.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required*|
|permissions|boolean|Set to "true" to propagate permissions.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required* if `tag_filters` is not set|
|tag_filters|boolean|Set to "true" to propagate tag filters.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required* if `permissions` is not set|


[comment]: # ({/b150eede-6102edd8})



[comment]: # ({f0b30152-42ead635})
### Return values

`(object)` Returns an object containing the IDs of the propagated host
groups under the `groupids` property.


[comment]: # ({/f0b30152-42ead635})



[comment]: # ({b41637d2-018502e3})
### Examples


[comment]: # ({/b41637d2-018502e3})



[comment]: # ({bd95271f-8ec85132})
#### Propagating host group permissions and tag filters to its subgroups.

Propagate host group permissions and tag filters to its subgroups.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "hostgroup.propagate",
    "params": {
        "groups": [
            {
                "groupid": "6"
            }
        ],
        "permissions": true,
        "tag_filters": true
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "groupids": [
            "6",
        ]
    },
    "id": 1
}
```


[comment]: # ({/bd95271f-8ec85132})



[comment]: # ({5e0375b3-10bafee5})
### See also

-   [hostgroup.update](update)
-   [hostgroup.massadd](massadd)
-   [Host](/manual/api/reference/host/object#host)


[comment]: # ({/5e0375b3-10bafee5})



[comment]: # ({fb50ec6c-a745b72a})
### Source

CHostGroup::propagate() in
*ui/include/classes/api/services/CHostGroup.php*.


[comment]: # ({/fb50ec6c-a745b72a})
