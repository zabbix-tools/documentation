[comment]: # ({5ebe3ff8-5ebe3ff8})
# hostgroup.create

[comment]: # ({/5ebe3ff8-5ebe3ff8})

[comment]: # ({0a562b4e-06c5b0b6})
### Description

`object hostgroup.create(object/array hostGroups)`

This method allows to create new host groups.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/0a562b4e-06c5b0b6})

[comment]: # ({9743504d-7c6d41ff})
### Parameters

`(object/array)` Host groups to create.

The method accepts host groups
with the [standard host group properties](object#host_group).

[comment]: # ({/9743504d-7c6d41ff})

[comment]: # ({e3c164f1-e3c164f1})
### Return values

`(object)` Returns an object containing the IDs of the created host
groups under the `groupids` property. The order of the returned IDs
matches the order of the passed host groups.

[comment]: # ({/e3c164f1-e3c164f1})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({f82f8eea-37cc26c2})
#### Creating a host group

Create a host group called "Linux servers".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "hostgroup.create",
    "params": {
        "name": "Linux servers"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "groupids": [
            "107819"
        ]
    },
    "id": 1
}
```

[comment]: # ({/f82f8eea-37cc26c2})

[comment]: # ({df4dc6d7-df4dc6d7})
### Source

CHostGroup::create() in
*ui/include/classes/api/services/CHostGroup.php*.

[comment]: # ({/df4dc6d7-df4dc6d7})
