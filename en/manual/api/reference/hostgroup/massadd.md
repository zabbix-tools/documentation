[comment]: # ({3f9b711d-3f9b711d})
# hostgroup.massadd

[comment]: # ({/3f9b711d-3f9b711d})

[comment]: # ({8cedc1b8-30b8c9f4})
### Description

`object hostgroup.massadd(object parameters)`

This method allows to simultaneously add multiple related objects to all
the given host groups.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/8cedc1b8-30b8c9f4})

[comment]: # ({0805f908-b8ff6430})
### Parameters

`(object)` Parameters containing the IDs of the host groups to update
and the objects to add to all the host groups.

The method accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|groups|object/array|Host groups to be updated.<br><br>The host groups must have the `groupid` property defined.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required*|
|hosts|object/array|Hosts to add to all host groups.<br><br>The hosts must have the `hostid` property defined.|

[comment]: # ({/0805f908-b8ff6430})

[comment]: # ({736f3b05-736f3b05})
### Return values

`(object)` Returns an object containing the IDs of the updated host
groups under the `groupids` property.

[comment]: # ({/736f3b05-736f3b05})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({893ecf7b-7698103b})
#### Adding hosts to host groups

Add two hosts to host groups with IDs 5 and 6.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "hostgroup.massadd",
    "params": {
        "groups": [
            {
                "groupid": "5"
            },
            {
                "groupid": "6"
            }
        ],
        "hosts": [
            {
                "hostid": "30050"
            },
            {
                "hostid": "30001"
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "groupids": [
            "5",
            "6"
        ]
    },
    "id": 1
}
```

[comment]: # ({/893ecf7b-7698103b})

[comment]: # ({6a994b89-e5f0abe1})
### See also

-   [Host](/manual/api/reference/host/object#host)

[comment]: # ({/6a994b89-e5f0abe1})

[comment]: # ({19273fb6-19273fb6})
### Source

CHostGroup::massAdd() in
*ui/include/classes/api/services/CHostGroup.php*.

[comment]: # ({/19273fb6-19273fb6})
