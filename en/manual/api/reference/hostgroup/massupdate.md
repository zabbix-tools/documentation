[comment]: # ({c07c9092-c07c9092})
# hostgroup.massupdate

[comment]: # ({/c07c9092-c07c9092})

[comment]: # ({fb4e6f8d-0ec0a392})
### Description

`object hostgroup.massupdate(object parameters)`

This method allows to replace hosts and templates with the specified
ones in multiple host groups.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/fb4e6f8d-0ec0a392})

[comment]: # ({93e5e51c-d4028a54})
### Parameters

`(object)` Parameters containing the IDs of the host groups to update
and the objects that should be updated.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|groups|object/array|Host groups to be updated.<br><br>The host groups must have the `groupid` property defined.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required*|
|hosts|object/array|Hosts to replace the current hosts on the given host groups.<br>All other hosts, except the ones mentioned, will be excluded from host groups.<br>Discovered hosts will not be affected.<br><br>The hosts must have the `hostid` property defined.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required*|

[comment]: # ({/93e5e51c-d4028a54})

[comment]: # ({736f3b05-736f3b05})
### Return values

`(object)` Returns an object containing the IDs of the updated host
groups under the `groupids` property.

[comment]: # ({/736f3b05-736f3b05})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({aff1f6e2-b4adf299})
#### Replacing hosts in a host group

Replace all hosts in a host group to ones mentioned host.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "hostgroup.massupdate",
    "params": {
        "groups": [
            {
                "groupid": "6"
            }
        ],
        "hosts": [
            {
                "hostid": "30050"
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "groupids": [
            "6",
        ]
    },
    "id": 1
}
```

[comment]: # ({/aff1f6e2-b4adf299})

[comment]: # ({5e0375b3-a1410648})
### See also

-   [hostgroup.update](update)
-   [hostgroup.massadd](massadd)
-   [Host](/manual/api/reference/host/object#host)

[comment]: # ({/5e0375b3-a1410648})

[comment]: # ({243072aa-243072aa})
### Source

CHostGroup::massUpdate() in
*ui/include/classes/api/services/CHostGroup.php*.

[comment]: # ({/243072aa-243072aa})
