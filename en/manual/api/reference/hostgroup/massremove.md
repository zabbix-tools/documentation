[comment]: # ({8703e8df-8703e8df})
# hostgroup.massremove

[comment]: # ({/8703e8df-8703e8df})

[comment]: # ({626e115a-bcf1ded4})
### Description

`object hostgroup.massremove(object parameters)`

This method allows to remove related objects from multiple host groups.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/626e115a-bcf1ded4})

[comment]: # ({3232187a-6b7b7475})
### Parameters

`(object)` Parameters containing the IDs of the host groups to update
and the objects that should be removed.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|groupids|string/array|IDs of the host groups to be updated.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required*|
|hostids|string/array|Hosts to remove from all host groups.|

[comment]: # ({/3232187a-6b7b7475})

[comment]: # ({736f3b05-736f3b05})
### Return values

`(object)` Returns an object containing the IDs of the updated host
groups under the `groupids` property.

[comment]: # ({/736f3b05-736f3b05})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({31b87672-cf36d4e8})
#### Removing hosts from host groups

Remove two hosts from the given host groups.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "hostgroup.massremove",
    "params": {
        "groupids": [
            "5",
            "6"
        ],
        "hostids": [
            "30050",
            "30001"
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "groupids": [
            "5",
            "6"
        ]
    },
    "id": 1
}
```

[comment]: # ({/31b87672-cf36d4e8})

[comment]: # ({3700b99d-3700b99d})
### Source

CHostGroup::massRemove() in
*ui/include/classes/api/services/CHostGroup.php*.

[comment]: # ({/3700b99d-3700b99d})
