[comment]: # ({27368a1a-830462e8})
# Service

This class is designed to work with IT infrastructure/business services.

Object references:

-   [Service](/manual/api/reference/service/object#service)
-   [Status rule](/manual/api/reference/service/object#status_rule)
-   [Service tag](/manual/api/reference/service/object#service_tag)
-   [Service alarm](/manual/api/reference/service/object#service_alarm)
-   [Problem tag](/manual/api/reference/service/object#problem_tag)

Available methods:

-   [service.create](/manual/api/reference/service/create) - create new services
-   [service.delete](/manual/api/reference/service/delete) - delete services
-   [service.get](/manual/api/reference/service/get) - retrieve services
-   [service.update](/manual/api/reference/service/update) - update services

[comment]: # ({/27368a1a-830462e8})
