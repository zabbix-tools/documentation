[comment]: # ({95bb70b6-4fff4611})
# Problem

This class is designed to work with problems.

Object references:

-   [Problem](/manual/api/reference/problem/object#problem)
-   [Problem tag](/manual/api/reference/problem/object#problem_tag)
-   [Media type URL](/manual/api/reference/problem/object#media_type_url)

Available methods:

-   [problem.get](/manual/api/reference/problem/get) - retrieve problems

[comment]: # ({/95bb70b6-4fff4611})
