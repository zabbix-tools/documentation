[comment]: # ({669b84c5-669b84c5})
# trend.get

[comment]: # ({/669b84c5-669b84c5})

[comment]: # ({8296b8fc-b49a69bd})
### Description

`integer/array trend.get(object parameters)`

The method allows to retrieve trend data according to the given
parameters.

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/8296b8fc-b49a69bd})

[comment]: # ({2ce9660c-e33350ec})
### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|itemids|string/array|Return only trends with the given item IDs.|
|time\_from|timestamp|Return only values that have been collected after or at the given time.|
|time\_till|timestamp|Return only values that have been collected before or at the given time.|
|countOutput|boolean|Count the number of retrieved objects.|
|limit|integer|Limit the amount of retrieved objects.|
|output|query|Set fields to output.|

[comment]: # ({/2ce9660c-e33350ec})

[comment]: # ({7223bab1-7223bab1})
### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.

[comment]: # ({/7223bab1-7223bab1})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({27c6d8d0-98fd9c3f})
#### Retrieving item trend data

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "trend.get",
    "params": {
        "output": [
            "itemid",
            "clock",
            "num",
            "value_min",
            "value_avg",
            "value_max",
        ],
        "itemids": [
            "23715"
        ],
        "limit": "1"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": [
        {
            "itemid": "23715",
            "clock": "1446199200",
            "num": "60",
            "value_min": "0.165",
            "value_avg": "0.2168",
            "value_max": "0.35",
        }
    ],
    "id": 1
}
```

[comment]: # ({/27c6d8d0-98fd9c3f})

[comment]: # ({852a9753-852a9753})
### Source

CTrend::get() in *ui/include/classes/api/services/CTrend.php*.

[comment]: # ({/852a9753-852a9753})
