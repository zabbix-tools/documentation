[comment]: # ({faff6037-faff6037})
# dashboard.delete

[comment]: # ({/faff6037-faff6037})

[comment]: # ({4d3377fa-6f29484b})
### Description

`object dashboard.delete(array dashboardids)`

This method allows to delete dashboards.

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/4d3377fa-6f29484b})

[comment]: # ({4fd471a4-4fd471a4})
### Parameters

`(array)` IDs of the dashboards to delete.

[comment]: # ({/4fd471a4-4fd471a4})

[comment]: # ({c04394d8-c04394d8})
### Return values

`(object)` Returns an object containing the IDs of the deleted
dashboards under the `dashboardids` property.

[comment]: # ({/c04394d8-c04394d8})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({bca9b517-4d5d4a5d})
#### Deleting multiple dashboards

Delete two dashboards.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "dashboard.delete",
    "params": [
        "2",
        "3"
    ],
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "dashboardids": [
            "2",
            "3"
        ]
    },
    "id": 1
}
```

[comment]: # ({/bca9b517-4d5d4a5d})

[comment]: # ({2bc6b04b-2bc6b04b})
### Source

CDashboard::delete() in
*ui/include/classes/api/services/CDashboard.php*.

[comment]: # ({/2bc6b04b-2bc6b04b})
