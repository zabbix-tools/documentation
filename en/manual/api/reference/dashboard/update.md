[comment]: # ({66fb9690-66fb9690})
# dashboard.update

[comment]: # ({/66fb9690-66fb9690})

[comment]: # ({becba6de-f235159f})
### Description

`object dashboard.update(object/array dashboards)`

This method allows to update existing dashboards.

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/becba6de-f235159f})

[comment]: # ({38323bbf-62947aa0})
### Parameters

`(object/array)` Dashboard properties to be updated.

The `dashboardid` property must be defined for each dashboard, all
other properties are optional. Only the passed properties will be
updated, all others will remain unchanged.

Additionally to the [standard dashboard properties](object#dashboard),
the method accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|pages|array|Dashboard [pages](object#dashboard_page) to replace the existing dashboard pages.<br><br>Dashboard pages are updated by the `dashboard_pageid` property. New dashboard pages will be created for objects without `dashboard_pageid` property and the existing dashboard pages will be deleted if not reused. Dashboard pages will be ordered in the same order as specified. Only the specified properties of the dashboard pages will be updated.|
|users|array|Dashboard [user](object#dashboard_user) shares to replace the existing elements.|
|userGroups|array|Dashboard [user group](object#dashboard_user_group) shares to replace the existing elements.|

[comment]: # ({/38323bbf-62947aa0})

[comment]: # ({28e89b38-28e89b38})
### Return values

`(object)` Returns an object containing the IDs of the updated
dashboards under the `dashboardids` property.

[comment]: # ({/28e89b38-28e89b38})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({d0eb0276-7fcf7de4})
#### Renaming a dashboard

Rename a dashboard to "SQL server status".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "dashboard.update",
    "params": {
        "dashboardid": "2",
        "name": "SQL server status"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "dashboardids": [
            "2"
        ]
    },
    "id": 1
}
```

[comment]: # ({/d0eb0276-7fcf7de4})

[comment]: # ({da48a540-b8348b8a})
#### Updating dashboard pages

Rename the first dashboard page, replace widgets on the second dashboard
page and add a new page as the third one. Delete all other dashboard
pages.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "dashboard.update",
    "params": {
        "dashboardid": "2",
        "pages": [
            {
                "dashboard_pageid": 1,
                "name": "Renamed Page"
            },
            {
                "dashboard_pageid": 2,
                "widgets": [
                    {
                        "type": "clock",
                        "x": 0,
                        "y": 0,
                        "width": 4,
                        "height": 3
                    }
                ]
            },
            {
                "display_period": 60
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "dashboardids": [
            "2"
        ]
    },
    "id": 1
}
```

[comment]: # ({/da48a540-b8348b8a})

[comment]: # ({6990c258-e4a57dd3})
#### Change dashboard owner

Available only for admins and super admins.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "dashboard.update",
    "params": {
        "dashboardid": "2",
        "userid": "1"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "dashboardids": [
            "2"
        ]
    },
    "id": 1
}
```

[comment]: # ({/6990c258-e4a57dd3})

[comment]: # ({299ae089-299ae089})
### See also

-   [Dashboard page](object#dashboard_page)
-   [Dashboard widget](object#dashboard_widget)
-   [Dashboard widget field](object#dashboard_widget_field)
-   [Dashboard user](object#dashboard_user)
-   [Dashboard user group](object#dashboard_user_group)

[comment]: # ({/299ae089-299ae089})

[comment]: # ({456ac32b-456ac32b})
### Source

CDashboard::update() in
*ui/include/classes/api/services/CDashboard.php*.

[comment]: # ({/456ac32b-456ac32b})
