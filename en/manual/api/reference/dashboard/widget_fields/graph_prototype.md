[comment]: # ({9ee1626a-2be71f02})
# 10 Graph prototype

[comment]: # ({/9ee1626a-2be71f02})

[comment]: # ({7666229a-4ce2099a})
### Description

These parameters and the possible property values for the respective dashboard widget field objects allow to configure
the [*Graph prototype*](/manual/web_interface/frontend_sections/dashboards/widgets/graph_prototype) widget in `dashboard.create` and `dashboard.update` methods.

[comment]: # ({/7666229a-4ce2099a})

[comment]: # ({4fdd3469-83184d1c})
### Parameters

The following parameters are supported for the *Graph prototype* widget.

|Parameter|[type](/manual/api/reference/dashboard/object#dashboard-widget-field)|name|value|
|-----|-|-----|-------------------|
|*Refresh interval*|0|rf_rate|0 - No refresh;<br>10 - 10 seconds;<br>30 - 30 seconds;<br>60 - *(default)* 1 minute;<br>120 - 2 minutes;<br>600 - 10 minutes;<br>900 - 15 minutes.|
|*Source*|0|source_type|2 - *(default)* Graph prototype;<br>3 - Simple graph prototype.|
|*Graph prototype*|7|graphid|[Graph prototype](/manual/api/reference/graphprototype/get) ID.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required* if *Source* is set to "Graph prototype"|
|*Item prototype*|5|itemid|[Item prototype](/manual/api/reference/itemprototype/get) ID.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required* if *Source* is set to "Simple graph prototype"|
|*Show legend*|0|show_legend|0 - Disabled;<br>1 - *(default)* Enabled.|
|*Enable host selection*|0|dynamic|0 - *(default)* Disabled;<br>1 - Enabled.|
|*Columns*|0|columns|Valid values range from 1-24.<br><br> Default: 2.|
|*Rows*|0|rows|Valid values range from 1-16.<br><br> Default: 1.|

[comment]: # ({/4fdd3469-83184d1c})

[comment]: # ({bc39c20a-df884afa})
### Examples

The following examples aim to only describe the configuration of the dashboard widget field objects for the *Graph prototype* widget.
For more information on configuring a dashboard, see [`dashboard.create`](/manual/api/reference/dashboard/create).

[comment]: # ({/bc39c20a-df884afa})

[comment]: # ({e65738b0-826a8d29})
#### Configuring a *Graph prototype* widget

Configure a *Graph prototype* widget that displays a grid of 3 graphs (3 columns, 1 row) created from an item prototype (ID: "42316") by low-level discovery.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "dashboard.create",
    "params": {
        "name": "My dashboard",
        "display_period": 30,
        "auto_start": 1,
        "pages": [
            {
                "widgets": [
                    {
                        "type": "graphprototype",
                        "name": "Graph prototype",
                        "x": 0,
                        "y": 0,
                        "width": 16,
                        "height": 5,
                        "view_mode": 0,
                        "fields": [
                            {
                                "type": 0,
                                "name": "source_type",
                                "value": 3
                            },
                            {
                                "type": 5,
                                "name": "itemid",
                                "value": 42316
                            },
                            {
                                "type": 0,
                                "name": "columns",
                                "value": 3
                            }
                        ]
                    }
                ]
            }
        ],
        "userGroups": [
            {
                "usrgrpid": 7,
                "permission": 2
            }
        ],
        "users": [
            {
                "userid": 1,
                "permission": 3
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "dashboardids": [
            "3"
        ]
    },
    "id": 1
}
```

[comment]: # ({/e65738b0-826a8d29})

[comment]: # ({e9d30322-46917953})
### See also

-   [Dashboard widget field](/manual/api/reference/dashboard/object#dashboard-widget-field)
-   [`dashboard.create`](/manual/api/reference/dashboard/create)
-   [`dashboard.update`](/manual/api/reference/dashboard/update)

[comment]: # ({/e9d30322-46917953})
