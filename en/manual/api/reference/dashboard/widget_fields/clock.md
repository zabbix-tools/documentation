[comment]: # ({7074b2d8-be6356d8})
# 2 Clock

[comment]: # ({/7074b2d8-be6356d8})

[comment]: # ({05fc2da4-9d7a8838})
### Description

These parameters and the possible property values for the respective dashboard widget field objects allow to configure
the [*Clock*](/manual/web_interface/frontend_sections/dashboards/widgets/clock) widget in `dashboard.create` and `dashboard.update` methods.

[comment]: # ({/05fc2da4-9d7a8838})

[comment]: # ({0a8fcc04-28857267})
### Parameters

The following parameters are supported for the *Clock* widget.

|Parameter|[type](/manual/api/reference/dashboard/object#dashboard-widget-field)|name|value|
|-----|-|-----|-------------------|
|*Refresh interval*|0|rf_rate|0 - No refresh;<br>10 - 10 seconds;<br>30 - 30 seconds;<br>60 - 1 minute;<br>120 - 2 minutes;<br>600 - 10 minutes;<br>900 - *(default)* 15 minutes.|
|*Time type*|0|time_type|0 - *(default)* Local time;<br>1 - Server time;<br>2 - Host time.|
|*Clock type*|0|clock_type|0 - *(default)* Analog;<br>1 - Digital.|

The following parameters are supported if *Time type* is set to "Host time".

|Parameter|[type](/manual/api/reference/dashboard/object#dashboard-widget-field)|name|value|
|-----|-|-----|-------------------|
|*Item*|4|itemid|[Item](/manual/api/reference/item/get) ID.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required*|

The following parameters are supported if *Clock type* is set to "Digital".

|Parameter|[type](/manual/api/reference/dashboard/object#dashboard-widget-field)|name|value|
|-----|-|-----|-------------------|
|*Show*|0|show|1 - Date;<br>2 - *(default)* Time;<br>3 - Time zone.<br><br>Note: To configure multiple values, create a dashboard widget field object for each value.|

[comment]: # ({/0a8fcc04-28857267})

[comment]: # ({7cfdce97-ab3ddf88})
#### Advanced configuration

The following advanced configuration parameters are supported if *Clock type* is set to "Digital".

|Parameter|[type](/manual/api/reference/dashboard/object#dashboard-widget-field)|name|value|
|-----|-|-----|-------------------|
|*Background color*|1|bg_color|Hexadecimal color code (e.g. `FF0000`).<br><br>Default: `""` (empty).|

[comment]: # ({/7cfdce97-ab3ddf88})

[comment]: # ({15a8a9fc-9f05ef41})
##### Date

The following advanced configuration parameters are supported if *Clock type* is set to "Digital", and *Show* is set to "Date".

|Parameter|[type](/manual/api/reference/dashboard/object#dashboard-widget-field)|name|value|
|-----|-|-----|-------------------|
|*Size*|0|date_size|Valid values range from 1-100.<br><br>Default: 20.|
|*Bold*|0|date_bold|0 - *(default)* Disabled;<br>1 - Enabled.|
|*Color*|1|date_color|Hexadecimal color code (e.g. `FF0000`).<br><br>Default: `""` (empty).|

[comment]: # ({/15a8a9fc-9f05ef41})

[comment]: # ({db81bbb5-76db53db})
##### Time

The following advanced configuration parameters are supported if *Clock type* is set to "Digital", and *Show* is set to "Time".

|Parameter|[type](/manual/api/reference/dashboard/object#dashboard-widget-field)|name|value|
|-----|-|-----|-------------------|
|*Size*|0|time_size|Valid values range from 1-100.<br><br>Default: 30.|
|*Bold*|0|time_bold|0 - *(default)* Disabled;<br>1 - Enabled.|
|*Color*|1|time_color|Hexadecimal color code (e.g. `FF0000`).<br><br>Default: `""` (empty).|
|*Seconds*|0|time_sec|0 - Disabled;<br>1 - *(default)* Enabled.|
|*Format*|0|time_format|0 - *(default)* 24-hour;<br>1 - 12-hour.|

[comment]: # ({/db81bbb5-76db53db})

[comment]: # ({f513a5b6-b6649dee})
##### Time zone

The following advanced configuration parameters are supported if *Clock type* is set to "Digital", and *Show* is set to "Time zone".

|Parameter|[type](/manual/api/reference/dashboard/object#dashboard-widget-field)|name|value|
|-----|-|-----|-------------------|
|*Size*|0|tzone_size|Valid values range from 1-100.<br><br>Default: 20.|
|*Bold*|0|tzone_bold|0 - *(default)* Disabled;<br>1 - Enabled.|
|*Color*|1|tzone_color|Hexadecimal color code (e.g. `FF0000`).<br><br>Default: `""` (empty).|
|*Time zone*|1|tzone_timezone|Valid timezone string (e.g. `Europe/Riga`, `system`, `UTC`, etc.). For the full list of supported time zones please refer to [PHP documentation](https://www.php.net/manual/en/timezones.php).<br><br>Default: `local`.<br><br>Parameter *Time zone* not available if *Time type* is set to "Host time".|
|*Format*|0|tzone_format|0 - *(default)* Short;<br>1 - Full.<br><br>Parameter *Format* not available if *Time type* is set to "Host time".|

[comment]: # ({/f513a5b6-b6649dee})

[comment]: # ({7b4615c3-3b5604a0})
### Examples

The following examples aim to only describe the configuration of the dashboard widget field objects for the *Clock* widget.
For more information on configuring a dashboard, see [`dashboard.create`](/manual/api/reference/dashboard/create).

[comment]: # ({/7b4615c3-3b5604a0})

[comment]: # ({a92cc31d-4be326f4})
#### Configuring a *Clock* widget

Configure a *Clock* widget that displays local date, time and time zone in a customized digital clock. 

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "dashboard.create",
    "params": {
        "name": "My dashboard",
        "display_period": 30,
        "auto_start": 1,
        "pages": [
            {
                "widgets": [
                    {
                        "type": "clock",
                        "name": "Clock",
                        "x": 0,
                        "y": 0,
                        "width": 4,
                        "height": 3,
                        "view_mode": 0,
                        "fields": [
                            {
                                "type": 0,
                                "name": "clock_type",
                                "value": 1
                            },
                            {
                                "type": 0,
                                "name": "show",
                                "value": 1
                            },
                            {
                                "type": 0,
                                "name": "show",
                                "value": 2
                            },
                            {
                                "type": 0,
                                "name": "show",
                                "value": 3
                            },
                            {
                                "type": 0,
                                "name": "date_size",
                                "value": 20
                            },
                            {
                                "type": 1,
                                "name": "date_color",
                                "value": "E1E1E1"
                            },
                            {
                                "type": 0,
                                "name": "time_bold",
                                "value": 1
                            },
                            {
                                "type": 0,
                                "name": "tzone_size",
                                "value": 10
                            },
                            {
                                "type": 1,
                                "name": "tzone_color",
                                "value": "E1E1E1"
                            },
                            {
                                "type": 1,
                                "name": "tzone_timezone",
                                "value": "Europe/Riga"
                            },
                            {
                                "type": 0,
                                "name": "tzone_format",
                                "value": 1
                            }
                        ]
                    }
                ]
            }
        ],
        "userGroups": [
            {
                "usrgrpid": 7,
                "permission": 2
            }
        ],
        "users": [
            {
                "userid": 1,
                "permission": 3
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "dashboardids": [
            "3"
        ]
    },
    "id": 1
}
```

[comment]: # ({/a92cc31d-4be326f4})

[comment]: # ({e9d30322-c9d720b7})
### See also

-   [Dashboard widget field](/manual/api/reference/dashboard/object#dashboard-widget-field)
-   [`dashboard.create`](/manual/api/reference/dashboard/create)
-   [`dashboard.update`](/manual/api/reference/dashboard/update)

[comment]: # ({/e9d30322-c9d720b7})
