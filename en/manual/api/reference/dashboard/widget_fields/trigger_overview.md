[comment]: # ({da50605d-de7c2ddb})
# 22 Trigger overview

[comment]: # ({/da50605d-de7c2ddb})

[comment]: # ({6eef4121-53633302})
### Description

These parameters and the possible property values for the respective dashboard widget field objects allow to configure
the [*Trigger Overview*](/manual/web_interface/frontend_sections/dashboards/widgets/trigger_overview) widget in `dashboard.create` and `dashboard.update` methods.

[comment]: # ({/6eef4121-53633302})

[comment]: # ({3c48e2c2-6a6f9912})
### Parameters

The following parameters are supported for the *Trigger Overview* widget.

|Parameter|<|[type](/manual/api/reference/dashboard/object#dashboard-widget-field)|name|value|
|-|--------|--|--------|-------------------------------|
|*Refresh interval*|<|0|rf_rate|0 - No refresh;<br>10 - 10 seconds;<br>30 - 30 seconds;<br>60 - *(default)* 1 minute;<br>120 - 2 minutes;<br>600 - 10 minutes;<br>900 - 15 minutes.|
|*Show*|<|0|show|1 - *(default)* Recent problems;<br>2 - Any;<br>3 - Problems.|
|*Host groups*|<|2|groupids|[Host group](/manual/api/reference/hostgroup/get) ID.<br><br>Note: To configure multiple host groups, create a dashboard widget field object for each host group.<br><br>Parameter *Host groups* not available when configuring the widget on a [template dashboard](/manual/api/reference/templatedashboard/object).|
|*Hosts*|<|3|hostids|[Host](/manual/api/reference/host/get) ID.<br><br>Note: To configure multiple hosts, create a dashboard widget field object for each host. For multiple hosts, the parameter *Host groups* must either be not configured at all or configured with at least one host group that the configured hosts belong to.<br><br>Parameter *Hosts* not available when configuring the widget on a [template dashboard](/manual/api/reference/templatedashboard/object).|
|*Problem tags* (the number in the property name (e.g. tags.tag.0) references tag order in the tag evaluation list)|<|<|<|<|
|<|*Evaluation type*|0|evaltype|0 - *(default)* And/Or;<br>2 - Or.|
|^|*Tag name*|1|tags.tag.0|Any string value.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required* if configuring *Problem tags*|
|^|*Operator*|0|tags.operator.0|0 - Contains;<br>1 - Equals;<br>2 - Does not contain;<br>3 - Does not equal;<br>4 - Exists;<br>5 - Does not exist.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required* if configuring *Problem tags*|
|^|*Tag value*|1|tags.value.0|Any string value.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required* if configuring *Problem tags*|
|*Show suppressed problems*|<|0|show_suppressed|0 - *(default)* Disabled;<br>1 - Enabled.|
|*Hosts location*|<|0|style|0 - *(default)* Left;<br>1 - Top.|

[comment]: # ({/3c48e2c2-6a6f9912})

[comment]: # ({003e5965-ac37d8fc})
### Examples

The following examples aim to only describe the configuration of the dashboard widget field objects for the *Trigger overview* widget.
For more information on configuring a dashboard, see [`dashboard.create`](/manual/api/reference/dashboard/create).

[comment]: # ({/003e5965-ac37d8fc})

[comment]: # ({165325ac-f965093e})
#### Configuring a *Trigger overview* widget

Configure a *Trigger overview* widget that displays trigger states for all host groups that have triggers with a tag that has the name "scope" and contains value "availability".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "dashboard.create",
    "params": {
        "name": "My dashboard",
        "display_period": 30,
        "auto_start": 1,
        "pages": [
            {
                "widgets": [
                    {
                        "type": "trigover",
                        "name": "Trigger overview",
                        "x": 0,
                        "y": 0,
                        "width": 12,
                        "height": 5,
                        "view_mode": 0,
                        "fields": [
                            {
                                "type": 1,
                                "name": "tags.tag.0",
                                "value": "scope"
                            },
                            {
                                "type": 0,
                                "name": "tags.operator.0",
                                "value": 0
                            },
                            {
                                "type": 1,
                                "name": "tags.value.0",
                                "value": "availability"
                            }
                        ]
                    }
                ]
            }
        ],
        "userGroups": [
            {
                "usrgrpid": 7,
                "permission": 2
            }
        ],
        "users": [
            {
                "userid": 1,
                "permission": 3
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "dashboardids": [
            "3"
        ]
    },
    "id": 1
}
```

[comment]: # ({/165325ac-f965093e})

[comment]: # ({e9d30322-43c002cb})
### See also

-   [Dashboard widget field](/manual/api/reference/dashboard/object#dashboard-widget-field)
-   [`dashboard.create`](/manual/api/reference/dashboard/create)
-   [`dashboard.update`](/manual/api/reference/dashboard/update)

[comment]: # ({/e9d30322-43c002cb})
