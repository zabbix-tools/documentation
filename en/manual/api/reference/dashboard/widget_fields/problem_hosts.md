[comment]: # ({ebb3a9a4-516a6885})
# 16 Problem hosts

[comment]: # ({/ebb3a9a4-516a6885})

[comment]: # ({4477adb5-1ee1c5af})
### Description

These parameters and the possible property values for the respective dashboard widget field objects allow to configure
the [*Problem hosts*](/manual/web_interface/frontend_sections/dashboards/widgets/problem_hosts) widget in `dashboard.create` and `dashboard.update` methods.

[comment]: # ({/4477adb5-1ee1c5af})

[comment]: # ({6437cb07-e1337d2e})
### Parameters

The following parameters are supported for the *Problem hosts* widget.

|Parameter|<|[type](/manual/api/reference/dashboard/object#dashboard-widget-field)|name|value|
|-|--------|--|--------|-------------------------------|
|*Refresh interval*|<|0|rf_rate|0 - No refresh;<br>10 - 10 seconds;<br>30 - 30 seconds;<br>60 - *(default)* 1 minute;<br>120 - 2 minutes;<br>600 - 10 minutes;<br>900 - 15 minutes.|
|*Host groups*|<|2|groupids|[Host group](/manual/api/reference/hostgroup/get) ID.<br><br>Note: To configure multiple host groups, create a dashboard widget field object for each host group.<br><br>Parameter *Host groups* not available when configuring the widget on a [template dashboard](/manual/api/reference/templatedashboard/object).|
|*Exclude host groups*|<|2|exclude_groupids|[Host group](/manual/api/reference/hostgroup/get) ID.<br><br>Note: To exclude multiple host groups, create a dashboard widget field object for each host group.<br><br>Parameter *Exclude host groups* not available when configuring the widget on a [template dashboard](/manual/api/reference/templatedashboard/object).|
|*Hosts*|<|3|hostids|[Host](/manual/api/reference/host/get) ID.<br><br>Note: To configure multiple hosts, create a dashboard widget field object for each host. For multiple hosts, the parameter *Host groups* must either be not configured at all or configured with at least one host group that the configured hosts belong to.<br><br>Parameter *Hosts* not available when configuring the widget on a [template dashboard](/manual/api/reference/templatedashboard/object).|
|*Problem*|<|1|problem|Problem [event name](/manual/config/triggers/trigger#configuration) (case insensitive, full name or part of it).|
|*Severity*|<|0|severities|0 - Not classified;<br>1 - Information;<br>2 - Warning;<br>3 - Average;<br>4 - High;<br>5 - Disaster.<br><br>Default: 1, 2, 3, 4, 5 (all enabled).<br><br>Note: To configure multiple values, create a dashboard widget field object for each value.|
|*Problem tags* (the number in the property name (e.g. tags.tag.0) references tag order in the tag evaluation list)|<|<|<|<|
|<|*Evaluation type*|0|evaltype|0 - *(default)* And/Or;<br>2 - Or.|
|^|*Tag name*|1|tags.tag.0|Any string value.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required* if configuring *Problem tags*|
|^|*Operator*|0|tags.operator.0|0 - Contains;<br>1 - Equals;<br>2 - Does not contain;<br>3 - Does not equal;<br>4 - Exists;<br>5 - Does not exist.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required* if configuring *Problem tags*|
|^|*Tag value*|1|tags.value.0|Any string value.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required* if configuring *Problem tags*|
|*Show suppressed problems*|<|0|show_suppressed|0 - *(default)* Disabled;<br>1 - Enabled.|
|*Hide groups without problems*|<|0|hide_empty_groups|0 - *(default)* Disabled;<br>1 - Enabled.<br><br>Parameter *Hide groups without problems* not available when configuring the widget on a [template dashboard](/manual/api/reference/templatedashboard/object).|
|*Problem display*|<|0|ext_ack|0 - *(default)* All;<br>1 - Unacknowledged only;<br>2 - Separated.|

[comment]: # ({/6437cb07-e1337d2e})

[comment]: # ({bcd0ecfc-67b6ab01})
### Examples

The following examples aim to only describe the configuration of the dashboard widget field objects for the *Problem hosts* widget.
For more information on configuring a dashboard, see [`dashboard.create`](/manual/api/reference/dashboard/create).

[comment]: # ({/bcd0ecfc-67b6ab01})

[comment]: # ({e16995d5-581163e0})
#### Configuring a *Problem hosts* widget

Configure a *Problem hosts* widget that displays hosts from host groups "2" and "4"
that have problems with a name that includes the string "CPU" and that have the following severities: "Warning", "Average", "High", "Disaster".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "dashboard.create",
    "params": {
        "name": "My dashboard",
        "display_period": 30,
        "auto_start": 1,
        "pages": [
            {
                "widgets": [
                    {
                        "type": "problemhosts",
                        "name": "Problem hosts",
                        "x": 0,
                        "y": 0,
                        "width": 12,
                        "height": 5,
                        "view_mode": 0,
                        "fields": [
                            {
                                "type": 2,
                                "name": "groupids",
                                "value": 2
                            },
                            {
                                "type": 2,
                                "name": "groupids",
                                "value": 4
                            },
                            {
                                "type": 1,
                                "name": "problem",
                                "value": "cpu"
                            },
                            {
                                "type": 0,
                                "name": "severities",
                                "value": 2
                            },
                            {
                                "type": 0,
                                "name": "severities",
                                "value": 3
                            },
                            {
                                "type": 0,
                                "name": "severities",
                                "value": 4
                            },
                            {
                                "type": 0,
                                "name": "severities",
                                "value": 5
                            }
                        ]
                    }
                ]
            }
        ],
        "userGroups": [
            {
                "usrgrpid": 7,
                "permission": 2
            }
        ],
        "users": [
            {
                "userid": 1,
                "permission": 3
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "dashboardids": [
            "3"
        ]
    },
    "id": 1
}
```

[comment]: # ({/e16995d5-581163e0})

[comment]: # ({e9d30322-2c4b70f3})
### See also

-   [Dashboard widget field](/manual/api/reference/dashboard/object#dashboard-widget-field)
-   [`dashboard.create`](/manual/api/reference/dashboard/create)
-   [`dashboard.update`](/manual/api/reference/dashboard/update)

[comment]: # ({/e9d30322-2c4b70f3})
