<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="en" datatype="plaintext" original="manual/api/reference/dashboard/widget_fields/system.md">
    <body>
      <trans-unit id="d8b4b063" xml:space="preserve">
        <source># 20 System information</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/api/reference/dashboard/widget_fields/system.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="acdd6f8c" xml:space="preserve">
        <source>### Description

These parameters and the possible property values for the respective dashboard widget field objects allow to configure
the [*System Information*](/manual/web_interface/frontend_sections/dashboards/widgets/system) widget in `dashboard.create` and `dashboard.update` methods.</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/api/reference/dashboard/widget_fields/system.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="59ca3d9b" xml:space="preserve">
        <source>### Parameters

The following parameters are supported for the *System Information* widget.

|Parameter|[type](/manual/api/reference/dashboard/object#dashboard-widget-field)|name|value|
|-----|-|-----|-------------------|
|*Refresh interval*|0|rf_rate|0 - No refresh;&lt;br&gt;10 - 10 seconds;&lt;br&gt;30 - 30 seconds;&lt;br&gt;60 - 1 minute;&lt;br&gt;120 - 2 minutes;&lt;br&gt;600 - 10 minutes;&lt;br&gt;900 - *(default)* 15 minutes.|
|*Show*|0|info_type|0 - *(default)* System stats;&lt;br&gt;1 - High availability nodes.|</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/api/reference/dashboard/widget_fields/system.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="c23d8683" xml:space="preserve">
        <source>### Examples

The following examples aim to only describe the configuration of the dashboard widget field objects for the *System information* widget.
For more information on configuring a dashboard, see [`dashboard.create`](/manual/api/reference/dashboard/create).</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/api/reference/dashboard/widget_fields/system.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="91f3e344" xml:space="preserve">
        <source>#### Configuring a *System information* widget

Configure a *System information* widget that displays system stats with a refresh interval of 10 minutes.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "dashboard.create",
    "params": {
        "name": "My dashboard",
        "display_period": 30,
        "auto_start": 1,
        "pages": [
            {
                "widgets": [
                    {
                        "type": "systeminfo",
                        "name": "System information",
                        "x": 0,
                        "y": 0,
                        "width": 12,
                        "height": 5,
                        "view_mode": 0,
                        "fields": [
                            {
                                "type": 0,
                                "name": "rf_rate",
                                "value": 600
                            }
                        ]
                    }
                ]
            }
        ],
        "userGroups": [
            {
                "usrgrpid": 7,
                "permission": 2
            }
        ],
        "users": [
            {
                "userid": 1,
                "permission": 3
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "dashboardids": [
            "3"
        ]
    },
    "id": 1
}
```</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/api/reference/dashboard/widget_fields/system.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="ee77e7cb" xml:space="preserve">
        <source>### See also

-   [Dashboard widget field](/manual/api/reference/dashboard/object#dashboard-widget-field)
-   [`dashboard.create`](/manual/api/reference/dashboard/create)
-   [`dashboard.update`](/manual/api/reference/dashboard/update)</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/api/reference/dashboard/widget_fields/system.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
    </body>
  </file>
</xliff>
