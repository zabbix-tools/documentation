[comment]: # ({563db206-534d39c6})
# 6 Favorite maps

[comment]: # ({/563db206-534d39c6})

[comment]: # ({59be3abe-84098726})
### Description

These parameters and the possible property values for the respective dashboard widget field objects allow to configure
the [*Favorite maps*](/manual/web_interface/frontend_sections/dashboards/widgets/favorite_maps) widget in `dashboard.create` and `dashboard.update` methods.

[comment]: # ({/59be3abe-84098726})

[comment]: # ({49829f3f-7c9dc890})
### Parameters

The following parameters are supported for the *Favorite maps* widget.

|Parameter|[type](/manual/api/reference/dashboard/object#dashboard-widget-field)|name|value|
|-----|-|-----|-------------------|
|*Refresh interval*|0|rf_rate|0 - No refresh;<br>10 - 10 seconds;<br>30 - 30 seconds;<br>60 - 1 minute;<br>120 - 2 minutes;<br>600 - 10 minutes;<br>900 - *(default)* 15 minutes.|

[comment]: # ({/49829f3f-7c9dc890})

[comment]: # ({09fc685e-8d90edd1})
### Examples

The following examples aim to only describe the configuration of the dashboard widget field objects for the *Favorite maps* widget.
For more information on configuring a dashboard, see [`dashboard.create`](/manual/api/reference/dashboard/create).

[comment]: # ({/09fc685e-8d90edd1})

[comment]: # ({3974c418-cca60ae7})
#### Configuring a *Favorite maps* widget

Configure a *Favorite maps* widget with the refresh interval set to 10 minutes.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "dashboard.create",
    "params": {
        "name": "My dashboard",
        "display_period": 30,
        "auto_start": 1,
        "pages": [
            {
                "widgets": [
                    {
                        "type": "favmaps",
                        "name": "Favorite maps",
                        "x": 0,
                        "y": 0,
                        "width": 4,
                        "height": 3,
                        "view_mode": 0,
                        "fields": [
                            {
                                "type": 0,
                                "name": "rf_rate",
                                "value": 600
                            }
                        ]
                    }
                ]
            }
        ],
        "userGroups": [
            {
                "usrgrpid": 7,
                "permission": 2
            }
        ],
        "users": [
            {
                "userid": 1,
                "permission": 3
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "dashboardids": [
            "3"
        ]
    },
    "id": 1
}
```

[comment]: # ({/3974c418-cca60ae7})

[comment]: # ({e9d30322-3b9216d7})
### See also

-   [Dashboard widget field](/manual/api/reference/dashboard/object#dashboard-widget-field)
-   [`dashboard.create`](/manual/api/reference/dashboard/create)
-   [`dashboard.update`](/manual/api/reference/dashboard/update)

[comment]: # ({/e9d30322-3b9216d7})
