<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="en" datatype="plaintext" original="manual/api/reference/dashboard/widget_fields/web_monitoring.md">
    <body>
      <trans-unit id="7df23091" xml:space="preserve">
        <source># 24 Web monitoring</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/api/reference/dashboard/widget_fields/web_monitoring.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="b0a71456" xml:space="preserve">
        <source>### Description

These parameters and the possible property values for the respective dashboard widget field objects allow to configure
the [*Web monitoring*](/manual/web_interface/frontend_sections/dashboards/widgets/web_monitoring) widget in `dashboard.create` and `dashboard.update` methods.</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/api/reference/dashboard/widget_fields/web_monitoring.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="e5dda4bf" xml:space="preserve">
        <source>### Parameters

The following parameters are supported for the *Web monitoring* widget.

|Parameter|&lt;|[type](/manual/api/reference/dashboard/object#dashboard-widget-field)|name|value|
|-|--------|--|--------|-------------------------------|
|*Refresh interval*|&lt;|0|rf_rate|0 - No refresh;&lt;br&gt;10 - 10 seconds;&lt;br&gt;30 - 30 seconds;&lt;br&gt;60 - *(default)* 1 minute;&lt;br&gt;120 - 2 minutes;&lt;br&gt;600 - 10 minutes;&lt;br&gt;900 - 15 minutes.|
|*Host groups*|&lt;|2|groupids|[Host group](/manual/api/reference/hostgroup/get) ID.&lt;br&gt;&lt;br&gt;Note: To configure multiple host groups, create a dashboard widget field object for each host group.&lt;br&gt;&lt;br&gt;Parameter *Host groups* not available when configuring the widget on a [template dashboard](/manual/api/reference/templatedashboard/object).|
|*Exclude host groups*|&lt;|2|exclude_groupids|[Host group](/manual/api/reference/hostgroup/get) ID.&lt;br&gt;&lt;br&gt;Note: To exclude multiple host groups, create a dashboard widget field object for each host group.&lt;br&gt;&lt;br&gt;Parameter *Exclude host groups* not available when configuring the widget on a [template dashboard](/manual/api/reference/templatedashboard/object).|
|*Hosts*|&lt;|3|hostids|[Host](/manual/api/reference/host/get) ID.&lt;br&gt;&lt;br&gt;Note: To configure multiple hosts, create a dashboard widget field object for each host. For multiple hosts, the parameter *Host groups* must either be not configured at all or configured with at least one host group that the configured hosts belong to.&lt;br&gt;&lt;br&gt;Parameter *Hosts* not available when configuring the widget on a [template dashboard](/manual/api/reference/templatedashboard/object).|
|*Scenario tags* (the number in the property name (e.g. tags.tag.0) references tag order in the tag evaluation list)|&lt;|&lt;|&lt;|&lt;|
|&lt;|*Evaluation type*|0|evaltype|0 - *(default)* And/Or;&lt;br&gt;2 - Or.|
|^|*Tag name*|1|tags.tag.0|Any string value.&lt;br&gt;&lt;br&gt;[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):&lt;br&gt;- *required* if configuring *Scenario tags*|
|^|*Operator*|0|tags.operator.0|0 - Contains;&lt;br&gt;1 - Equals;&lt;br&gt;2 - Does not contain;&lt;br&gt;3 - Does not equal;&lt;br&gt;4 - Exists;&lt;br&gt;5 - Does not exist.&lt;br&gt;&lt;br&gt;[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):&lt;br&gt;- *required* if configuring *Scenario tags*|
|^|*Tag value*|1|tags.value.0|Any string value.&lt;br&gt;&lt;br&gt;[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):&lt;br&gt;- *required* if configuring *Scenario tags*|
|*Show hosts in maintenance*|&lt;|0|maintenance|0 - Disabled;&lt;br&gt;1 - *(default)* Enabled.|</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/api/reference/dashboard/widget_fields/web_monitoring.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="e86e46ba" xml:space="preserve">
        <source>### Examples

The following examples aim to only describe the configuration of the dashboard widget field objects for the *Web monitoring* widget.
For more information on configuring a dashboard, see [`dashboard.create`](/manual/api/reference/dashboard/create).</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/api/reference/dashboard/widget_fields/web_monitoring.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="01a76e6e" xml:space="preserve">
        <source>#### Configuring a *Web monitoring* widget

Configure a *Web monitoring* widget that displays a status summary of the active web monitoring scenarios for host group "4".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "dashboard.create",
    "params": {
        "name": "My dashboard",
        "display_period": 30,
        "auto_start": 1,
        "pages": [
            {
                "widgets": [
                    {
                        "type": "web",
                        "name": "Web monitoring",
                        "x": 0,
                        "y": 0,
                        "width": 6,
                        "height": 3,
                        "view_mode": 0,
                        "fields": [
                            {
                                "type": 2,
                                "name": "groupids",
                                "value": 4
                            }
                        ]
                    }
                ]
            }
        ],
        "userGroups": [
            {
                "usrgrpid": 7,
                "permission": 2
            }
        ],
        "users": [
            {
                "userid": 1,
                "permission": 3
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "dashboardids": [
            "3"
        ]
    },
    "id": 1
}
```</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/api/reference/dashboard/widget_fields/web_monitoring.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="79381d10" xml:space="preserve">
        <source>### See also

-   [Dashboard widget field](/manual/api/reference/dashboard/object#dashboard-widget-field)
-   [`dashboard.create`](/manual/api/reference/dashboard/create)
-   [`dashboard.update`](/manual/api/reference/dashboard/update)</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/api/reference/dashboard/widget_fields/web_monitoring.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
    </body>
  </file>
</xliff>
