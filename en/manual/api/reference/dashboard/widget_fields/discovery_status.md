[comment]: # ({d91968a5-115541ab})
# 4 Discovery status

[comment]: # ({/d91968a5-115541ab})

[comment]: # ({d659fea9-b151e8cb})
### Description

These parameters and the possible property values for the respective dashboard widget field objects allow to configure
the [*Discovery status*](/manual/web_interface/frontend_sections/dashboards/widgets/discovery_status) widget in `dashboard.create` and `dashboard.update` methods.

[comment]: # ({/d659fea9-b151e8cb})

[comment]: # ({74dbf415-6b53c99e})
### Parameters

The following parameters are supported for the *Discovery status* widget.

|Parameter|[type](/manual/api/reference/dashboard/object#dashboard-widget-field)|name|value|
|-----|-|-----|-------------------|
|*Refresh interval*|0|rf_rate|0 - No refresh;<br>10 - 10 seconds;<br>30 - 30 seconds;<br>60 - *(default)* 1 minute;<br>120 - 2 minutes;<br>600 - 10 minutes;<br>900 - 15 minutes.|

[comment]: # ({/74dbf415-6b53c99e})

[comment]: # ({7d790087-de81ca88})
### Examples

The following examples aim to only describe the configuration of the dashboard widget field objects for the *Discovery status* widget.
For more information on configuring a dashboard, see [`dashboard.create`](/manual/api/reference/dashboard/create).

[comment]: # ({/7d790087-de81ca88})

[comment]: # ({a3ac88b3-a81c2c31})
#### Configuring *Discovery status* widget

Configure a *Discovery status* widget with the refresh interval set to 15 minutes.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "dashboard.create",
    "params": {
        "name": "My dashboard",
        "display_period": 30,
        "auto_start": 1,
        "pages": [
            {
                "widgets": [
                    {
                        "type": "discovery",
                        "name": "Discovery status",
                        "x": 0,
                        "y": 0,
                        "width": 6,
                        "height": 3,
                        "view_mode": 0,
                        "fields": [
                            {
                                "type": 0,
                                "name": "rf_rate",
                                "value": 900
                            }
                        ]
                    }
                ]
            }
        ],
        "userGroups": [
            {
                "usrgrpid": 7,
                "permission": 2
            }
        ],
        "users": [
            {
                "userid": 1,
                "permission": 3
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "dashboardids": [
            "3"
        ]
    },
    "id": 1
}
```

[comment]: # ({/a3ac88b3-a81c2c31})

[comment]: # ({e9d30322-85ed1e05})
### See also

-   [Dashboard widget field](/manual/api/reference/dashboard/object#dashboard-widget-field)
-   [`dashboard.create`](/manual/api/reference/dashboard/create)
-   [`dashboard.update`](/manual/api/reference/dashboard/update)

[comment]: # ({/e9d30322-85ed1e05})
