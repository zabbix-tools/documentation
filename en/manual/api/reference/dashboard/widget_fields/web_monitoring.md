[comment]: # ({54046c3a-7df23091})
# 24 Web monitoring

[comment]: # ({/54046c3a-7df23091})

[comment]: # ({25d2347d-b0a71456})
### Description

These parameters and the possible property values for the respective dashboard widget field objects allow to configure
the [*Web monitoring*](/manual/web_interface/frontend_sections/dashboards/widgets/web_monitoring) widget in `dashboard.create` and `dashboard.update` methods.

[comment]: # ({/25d2347d-b0a71456})

[comment]: # ({203f5f53-e5dda4bf})
### Parameters

The following parameters are supported for the *Web monitoring* widget.

|Parameter|<|[type](/manual/api/reference/dashboard/object#dashboard-widget-field)|name|value|
|-|--------|--|--------|-------------------------------|
|*Refresh interval*|<|0|rf_rate|0 - No refresh;<br>10 - 10 seconds;<br>30 - 30 seconds;<br>60 - *(default)* 1 minute;<br>120 - 2 minutes;<br>600 - 10 minutes;<br>900 - 15 minutes.|
|*Host groups*|<|2|groupids|[Host group](/manual/api/reference/hostgroup/get) ID.<br><br>Note: To configure multiple host groups, create a dashboard widget field object for each host group.<br><br>Parameter *Host groups* not available when configuring the widget on a [template dashboard](/manual/api/reference/templatedashboard/object).|
|*Exclude host groups*|<|2|exclude_groupids|[Host group](/manual/api/reference/hostgroup/get) ID.<br><br>Note: To exclude multiple host groups, create a dashboard widget field object for each host group.<br><br>Parameter *Exclude host groups* not available when configuring the widget on a [template dashboard](/manual/api/reference/templatedashboard/object).|
|*Hosts*|<|3|hostids|[Host](/manual/api/reference/host/get) ID.<br><br>Note: To configure multiple hosts, create a dashboard widget field object for each host. For multiple hosts, the parameter *Host groups* must either be not configured at all or configured with at least one host group that the configured hosts belong to.<br><br>Parameter *Hosts* not available when configuring the widget on a [template dashboard](/manual/api/reference/templatedashboard/object).|
|*Scenario tags* (the number in the property name (e.g. tags.tag.0) references tag order in the tag evaluation list)|<|<|<|<|
|<|*Evaluation type*|0|evaltype|0 - *(default)* And/Or;<br>2 - Or.|
|^|*Tag name*|1|tags.tag.0|Any string value.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required* if configuring *Scenario tags*|
|^|*Operator*|0|tags.operator.0|0 - Contains;<br>1 - Equals;<br>2 - Does not contain;<br>3 - Does not equal;<br>4 - Exists;<br>5 - Does not exist.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required* if configuring *Scenario tags*|
|^|*Tag value*|1|tags.value.0|Any string value.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required* if configuring *Scenario tags*|
|*Show hosts in maintenance*|<|0|maintenance|0 - Disabled;<br>1 - *(default)* Enabled.|

[comment]: # ({/203f5f53-e5dda4bf})

[comment]: # ({d3cc817a-e86e46ba})
### Examples

The following examples aim to only describe the configuration of the dashboard widget field objects for the *Web monitoring* widget.
For more information on configuring a dashboard, see [`dashboard.create`](/manual/api/reference/dashboard/create).

[comment]: # ({/d3cc817a-e86e46ba})

[comment]: # ({603f7b3b-01a76e6e})
#### Configuring a *Web monitoring* widget

Configure a *Web monitoring* widget that displays a status summary of the active web monitoring scenarios for host group "4".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "dashboard.create",
    "params": {
        "name": "My dashboard",
        "display_period": 30,
        "auto_start": 1,
        "pages": [
            {
                "widgets": [
                    {
                        "type": "web",
                        "name": "Web monitoring",
                        "x": 0,
                        "y": 0,
                        "width": 6,
                        "height": 3,
                        "view_mode": 0,
                        "fields": [
                            {
                                "type": 2,
                                "name": "groupids",
                                "value": 4
                            }
                        ]
                    }
                ]
            }
        ],
        "userGroups": [
            {
                "usrgrpid": 7,
                "permission": 2
            }
        ],
        "users": [
            {
                "userid": 1,
                "permission": 3
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "dashboardids": [
            "3"
        ]
    },
    "id": 1
}
```

[comment]: # ({/603f7b3b-01a76e6e})

[comment]: # ({e9d30322-79381d10})
### See also

-   [Dashboard widget field](/manual/api/reference/dashboard/object#dashboard-widget-field)
-   [`dashboard.create`](/manual/api/reference/dashboard/create)
-   [`dashboard.update`](/manual/api/reference/dashboard/update)

[comment]: # ({/e9d30322-79381d10})
