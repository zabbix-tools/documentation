[comment]: # ({8f310831-0cec15ee})
# 23 URL

[comment]: # ({/8f310831-0cec15ee})

[comment]: # ({e539bd89-103f7d14})
### Description

These parameters and the possible property values for the respective dashboard widget field objects allow to configure
the [*URL*](/manual/web_interface/frontend_sections/dashboards/widgets/url) widget in `dashboard.create` and `dashboard.update` methods.

[comment]: # ({/e539bd89-103f7d14})

[comment]: # ({db330979-df854d95})
### Parameters

The following parameters are supported for the *URL* widget.

|Parameter|[type](/manual/api/reference/dashboard/object#dashboard-widget-field)|name|value|
|-----|-|-----|-------------------|
|*Refresh interval*|0|rf_rate|0 - *(default)* No refresh;<br>10 - 10 seconds;<br>30 - 30 seconds;<br>60 - 1 minute;<br>120 - 2 minutes;<br>600 - 10 minutes;<br>900 - 15 minutes.|
|*URL*|1|url|Valid URL string.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required*|
|*Enable host selection*|0|dynamic|0 - *(default)* Disabled;<br>1 - Enabled.|

[comment]: # ({/db330979-df854d95})

[comment]: # ({b34af70f-44ecbf19})
### Examples

The following examples aim to only describe the configuration of the dashboard widget field objects for the *URL* widget.
For more information on configuring a dashboard, see [`dashboard.create`](/manual/api/reference/dashboard/create).

[comment]: # ({/b34af70f-44ecbf19})

[comment]: # ({54e0aa60-34af51c0})
#### Configuring a *URL* widget

Configure a *URL* widget that displays the home page of Zabbix manual.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "dashboard.create",
    "params": {
        "name": "My dashboard",
        "display_period": 30,
        "auto_start": 1,
        "pages": [
            {
                "widgets": [
                    {
                        "type": "url",
                        "name": "URL",
                        "x": 0,
                        "y": 0,
                        "width": 12,
                        "height": 5,
                        "view_mode": 0,
                        "fields": [
                            {
                                "type": 1,
                                "name": "url",
                                "value": "https://www.zabbix.com/documentation/6.4/en"
                            }
                        ]
                    }
                ]
            }
        ],
        "userGroups": [
            {
                "usrgrpid": 7,
                "permission": 2
            }
        ],
        "users": [
            {
                "userid": 1,
                "permission": 3
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "dashboardids": [
            "3"
        ]
    },
    "id": 1
}
```

[comment]: # ({/54e0aa60-34af51c0})

[comment]: # ({e9d30322-c2753f05})
### See also

-   [Dashboard widget field](/manual/api/reference/dashboard/object#dashboard-widget-field)
-   [`dashboard.create`](/manual/api/reference/dashboard/create)
-   [`dashboard.update`](/manual/api/reference/dashboard/update)

[comment]: # ({/e9d30322-c2753f05})
