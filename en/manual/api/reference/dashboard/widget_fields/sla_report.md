[comment]: # ({485672e6-6acd087d})
# 19 SLA report

[comment]: # ({/485672e6-6acd087d})

[comment]: # ({61052479-b112d173})
### Description

These parameters and the possible property values for the respective dashboard widget field objects allow to configure
the [*SLA report*](/manual/web_interface/frontend_sections/dashboards/widgets/sla_report) widget in `dashboard.create` and `dashboard.update` methods.

[comment]: # ({/61052479-b112d173})

[comment]: # ({6235dc14-b48fed5a})
### Parameters

The following parameters are supported for the *SLA report* widget.

|Parameter|[type](/manual/api/reference/dashboard/object#dashboard-widget-field)|name|value|
|-----|-|-----|-------------------|
|*Refresh interval*|0|rf_rate|0 - *(default)* No refresh;<br>10 - 10 seconds;<br>30 - 30 seconds;<br>60 - 1 minute;<br>120 - 2 minutes;<br>600 - 10 minutes;<br>900 - 15 minutes.|
|*SLA*|10|slaid|[SLA](/manual/api/reference/sla/get) ID.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required*|
|*Service*|9|serviceid|[Service](/manual/api/reference/service/get) ID.|
|*Show periods*|0|show_periods|Valid values range from 1-100.<br><br>Default: 20.|
|*From*|1|date_from|Valid date string in format `YYYY-MM-DD`.<br>[Relative dates](/manual/config/visualization/graphs/simple#time-period-selector) with modifiers `d`, `w`, `M`, `y` (e.g. `now`, `now/d`, `now/w-1w`, etc.) are supported.|
|*To*|1|date_to|Valid date string in format `YYYY-MM-DD`.<br>[Relative dates](/manual/config/visualization/graphs/simple#time-period-selector) with modifiers `d`, `w`, `M`, `y` (e.g. `now`, `now/d`, `now/w-1w`, etc.) are supported.|

[comment]: # ({/6235dc14-b48fed5a})

[comment]: # ({a1887e00-50899945})
### Examples

The following examples aim to only describe the configuration of the dashboard widget field objects for the *SLA report* widget.
For more information on configuring a dashboard, see [`dashboard.create`](/manual/api/reference/dashboard/create).

[comment]: # ({/a1887e00-50899945})

[comment]: # ({5b24ba8b-d501029a})
#### Configuring an *SLA report* widget

Configure an *SLA report* widget that displays the SLA report for SLA "4" service "2" for the period of last 30 days. 

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "dashboard.create",
    "params": {
        "name": "My dashboard",
        "display_period": 30,
        "auto_start": 1,
        "pages": [
            {
                "widgets": [
                    {
                        "type": "slareport",
                        "name": "SLA report",
                        "x": 0,
                        "y": 0,
                        "width": 12,
                        "height": 5,
                        "view_mode": 0,
                        "fields": [
                            {
                                "type": 10,
                                "name": "slaid",
                                "value": 4
                            },
                            {
                                "type": 9,
                                "name": "serviceid",
                                "value": 2
                            },
                            {
                                "type": 1,
                                "name": "date_from",
                                "value": "now-30d"
                            },
                            {
                                "type": 1,
                                "name": "date_to",
                                "value": "now"
                            }
                        ]
                    }
                ]
            }
        ],
        "userGroups": [
            {
                "usrgrpid": 7,
                "permission": 2
            }
        ],
        "users": [
            {
                "userid": 1,
                "permission": 3
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "dashboardids": [
            "3"
        ]
    },
    "id": 1
}
```

[comment]: # ({/5b24ba8b-d501029a})

[comment]: # ({e9d30322-ccbde9ad})
### See also

-   [Dashboard widget field](/manual/api/reference/dashboard/object#dashboard-widget-field)
-   [`dashboard.create`](/manual/api/reference/dashboard/create)
-   [`dashboard.update`](/manual/api/reference/dashboard/update)

[comment]: # ({/e9d30322-ccbde9ad})
