[comment]: # ({38b63d3a-e5b69436})
# 1 Action log

[comment]: # ({/38b63d3a-e5b69436})

[comment]: # ({7c9398f5-7950f8a2})
### Description

These parameters and the possible property values for the respective dashboard widget field objects allow to configure
the [*Action log*](/manual/web_interface/frontend_sections/dashboards/widgets/action_log) widget in `dashboard.create` and `dashboard.update` methods.

[comment]: # ({/7c9398f5-7950f8a2})

[comment]: # ({7444bc9c-74bdffdc})
### Parameters

The following parameters are supported for the *Action log* widget.

|Parameter|[type](/manual/api/reference/dashboard/object#dashboard-widget-field)|name|value|
|-----|-|-----|-------------------|
|*Refresh interval*|0|rf_rate|0 - No refresh;<br>10 - 10 seconds;<br>30 - 30 seconds;<br>60 - *(default)* 1 minute;<br>120 - 2 minutes;<br>600 - 10 minutes;<br>900 - 15 minutes.|
|*Recipients*|11|userids|[User](/manual/api/reference/user/get) ID.<br><br>Note: To configure multiple users, create a dashboard widget field object for each user.|
|*Actions*|12|actionids|[Action](/manual/api/reference/action/get) ID.<br><br>Note: To configure multiple actions, create a dashboard widget field object for each action.|
|*Media types*|13|mediatypeids|[Media type](/manual/api/reference/mediatype/get) ID.<br><br>Note: To configure multiple media types, create a dashboard widget field object for each media type.|
|*Status*|0|statuses|0 - In progress;<br>1 - Sent/Executed;<br>2 - Failed.<br><br>Note: To configure multiple values, create a dashboard widget field object for each value.|
|*Search string*|1|message|Any string value.|
|*Sort entries by*|0|sort_triggers|3 - Time (ascending);<br>4 - *(default)* Time (descending);<br>5 - Type (ascending);<br>6 - Type (descending);<br>7 - Status (ascending);<br>8 - Status (descending);<br>11 - Recipient (ascending);<br>12 - Recipient (descending).|
|*Show lines*|0|show_lines|Valid values range from 1-100.<br><br>Default: 25.|

[comment]: # ({/7444bc9c-74bdffdc})

[comment]: # ({211d52e5-2dd9270a})
### Examples

The following examples aim to only describe the configuration of the dashboard widget field objects for the *Action log* widget.
For more information on configuring a dashboard, see [`dashboard.create`](/manual/api/reference/dashboard/create).

[comment]: # ({/211d52e5-2dd9270a})

[comment]: # ({fa228700-bf9fdcfc})
#### Configuring an *Action log* widget

Configure an *Action log* widget that displays 10 entries of action operation details, sorted by time (in ascending order).
In addition, display details only for those action operations that attempted to send an email to user "1", but were unsuccessful.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "dashboard.create",
    "params": {
        "name": "My dashboard",
        "display_period": 30,
        "auto_start": 1,
        "pages": [
            {
                "widgets": [
                    {
                        "type": "actionlog",
                        "name": "Action log",
                        "x": 0,
                        "y": 0,
                        "width": 12,
                        "height": 5,
                        "view_mode": 0,
                        "fields": [
                            {
                                "type": 0,
                                "name": "show_lines",
                                "value": 10
                            },
                            {
                                "type": 0,
                                "name": "sort_triggers",
                                "value": 3
                            },
                            {
                                "type": 11,
                                "name": "userids",
                                "value": 1
                            },
                            {
                                "type": 13,
                                "name": "mediatypeids",
                                "value": 1
                            },
                            {
                                "type": 0,
                                "name": "statuses",
                                "value": 2
                            }
                        ]
                    }
                ]
            }
        ],
        "userGroups": [
            {
                "usrgrpid": 7,
                "permission": 2
            }
        ],
        "users": [
            {
                "userid": 1,
                "permission": 3
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "dashboardids": [
            "3"
        ]
    },
    "id": 1
}
```

[comment]: # ({/fa228700-bf9fdcfc})

[comment]: # ({e9d30322-5780d59d})
### See also

-   [Dashboard widget field](/manual/api/reference/dashboard/object#dashboard-widget-field)
-   [`dashboard.create`](/manual/api/reference/dashboard/create)
-   [`dashboard.update`](/manual/api/reference/dashboard/update)

[comment]: # ({/e9d30322-5780d59d})
