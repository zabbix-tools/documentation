[comment]: # ({093ad1f3-522e6f46})
# 3 Data overview

[comment]: # ({/093ad1f3-522e6f46})

[comment]: # ({e5c6a78c-ae03fe60})

::: noteimportant
This widget is deprecated and will be removed in the upcoming major release.
:::

[comment]: # ({/e5c6a78c-ae03fe60})

[comment]: # ({23158015-10fd6f8e})
### Description

These parameters and the possible property values for the respective dashboard widget field objects allow to configure
the [*Data overview*](/manual/web_interface/frontend_sections/dashboards/widgets/data_overview) widget in `dashboard.create` and `dashboard.update` methods.

[comment]: # ({/23158015-10fd6f8e})

[comment]: # ({280917e7-d1871136})
### Parameters

The following parameters are supported for the *Data overview* widget.

|Parameter|<|[type](/manual/api/reference/dashboard/object#dashboard-widget-field)|name|value|
|-|--------|--|--------|-------------------------------|
|*Refresh interval*|<|0|rf_rate|0 - No refresh;<br>10 - 10 seconds;<br>30 - 30 seconds;<br>60 - *(default)* 1 minute;<br>120 - 2 minutes;<br>600 - 10 minutes;<br>900 - 15 minutes.|
|*Host groups*|<|2|groupids|[Host group](/manual/api/reference/hostgroup/get) ID.<br><br>Note: To configure multiple host groups, create a dashboard widget field object for each host group.<br><br>Parameter *Host groups* not available when configuring the widget on a [template dashboard](/manual/api/reference/templatedashboard/object).|
|*Hosts*|<|3|hostids|[Host](/manual/api/reference/host/get) ID.<br><br>Note: To configure multiple hosts, create a dashboard widget field object for each host. For multiple hosts, the parameter *Host groups* must either be not configured at all or configured with at least one host group that the configured hosts belong to.<br><br>Parameter *Hosts* not available when configuring the widget on a [template dashboard](/manual/api/reference/templatedashboard/object).|
|*Item tags* (the number in the property name (e.g. tags.tag.0) references tag order in the tag evaluation list)|<|<|<|<|
|<|*Evaluation type*|0|evaltype|0 - *(default)* And/Or;<br>2 - Or.|
|^|*Tag name*|1|tags.tag.0|Any string value.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required* if configuring *Item tags*|
|^|*Operator*|0|tags.operator.0|0 - Contains;<br>1 - Equals;<br>2 - Does not contain;<br>3 - Does not equal;<br>4 - Exists;<br>5 - Does not exist.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required* if configuring *Item tags*|
|^|*Tag value*|1|tags.value.0|Any string value.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required* if configuring *Item tags*|
|*Show suppressed problems*|<|0|show_suppressed|0 - *(default)* Disabled;<br>1 - Enabled.|
|*Hosts location*|<|0|style|0 - *(default)* Left;<br>1 - Top.|

[comment]: # ({/280917e7-d1871136})

[comment]: # ({7663ef93-9f13be38})
### Examples

The following examples aim to only describe the configuration of the dashboard widget field objects for the *Data overview* widget.
For more information on configuring a dashboard, see [`dashboard.create`](/manual/api/reference/dashboard/create).

[comment]: # ({/7663ef93-9f13be38})

[comment]: # ({c4879d12-afab3ffb})
#### Configuring a *Data overview* widget

Configure a *Data overview* widget that displays data for host "10084" and only for items for which the tag with the name "component" contains value "cpu".
In addition, display the data with hosts located on top.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "dashboard.create",
    "params": {
        "name": "My dashboard",
        "display_period": 30,
        "auto_start": 1,
        "pages": [
            {
                "widgets": [
                    {
                        "type": "dataover",
                        "name": "Data overview",
                        "x": 0,
                        "y": 0,
                        "width": 12,
                        "height": 5,
                        "view_mode": 0,
                        "fields": [
                            {
                                "type": 3,
                                "name": "hostids",
                                "value": 10084
                            },
                            {
                                "type": 1,
                                "name": "tags.tag.0",
                                "value": "component"
                            },
                            {
                                "type": 0,
                                "name": "tags.operator.0",
                                "value": 0
                            },
                            {
                                "type": 1,
                                "name": "tags.value.0",
                                "value": "cpu"
                            },
                            {
                                "type": 0,
                                "name": "style",
                                "value": 1
                            }
                        ]
                    }
                ]
            }
        ],
        "userGroups": [
            {
                "usrgrpid": 7,
                "permission": 2
            }
        ],
        "users": [
            {
                "userid": 1,
                "permission": 3
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "dashboardids": [
            "3"
        ]
    },
    "id": 1
}
```

[comment]: # ({/c4879d12-afab3ffb})

[comment]: # ({e9d30322-a871b266})
### See also

-   [Dashboard widget field](/manual/api/reference/dashboard/object#dashboard-widget-field)
-   [`dashboard.create`](/manual/api/reference/dashboard/create)
-   [`dashboard.update`](/manual/api/reference/dashboard/update)

[comment]: # ({/e9d30322-a871b266})
