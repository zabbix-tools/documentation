<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="en" datatype="plaintext" original="manual/api/reference/dashboard/widget_fields/problems.md">
    <body>
      <trans-unit id="dfa1aefb" xml:space="preserve">
        <source># 17 Problems</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/api/reference/dashboard/widget_fields/problems.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="c733131f" xml:space="preserve">
        <source>### Description

These parameters and the possible property values for the respective dashboard widget field objects allow to configure
the [*Problems*](/manual/web_interface/frontend_sections/dashboards/widgets/problems) widget in `dashboard.create` and `dashboard.update` methods.</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/api/reference/dashboard/widget_fields/problems.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="07abd3fb" xml:space="preserve">
        <source>### Parameters

The following parameters are supported for the *Problems* widget.

|Parameter|&lt;|[type](/manual/api/reference/dashboard/object#dashboard-widget-field)|name|value|
|-|--------|--|--------|-------------------------------|
|*Refresh interval*|&lt;|0|rf_rate|0 - No refresh;&lt;br&gt;10 - 10 seconds;&lt;br&gt;30 - 30 seconds;&lt;br&gt;60 - *(default)* 1 minute;&lt;br&gt;120 - 2 minutes;&lt;br&gt;600 - 10 minutes;&lt;br&gt;900 - 15 minutes.|
|*Show*|&lt;|0|show|1 - *(default)* Recent problems;&lt;br&gt;2 - History;&lt;br&gt;3 - Problems.|
|*Host groups*|&lt;|2|groupids|[Host group](/manual/api/reference/hostgroup/get) ID.&lt;br&gt;&lt;br&gt;Note: To configure multiple host groups, create a dashboard widget field object for each host group.&lt;br&gt;&lt;br&gt;Parameter *Host groups* not available when configuring the widget on a [template dashboard](/manual/api/reference/templatedashboard/object).|
|*Exclude host groups*|&lt;|2|exclude_groupids|[Host group](/manual/api/reference/hostgroup/get) ID.&lt;br&gt;&lt;br&gt;Note: To exclude multiple host groups, create a dashboard widget field object for each host group.&lt;br&gt;&lt;br&gt;Parameter *Exclude host groups* not available when configuring the widget on a [template dashboard](/manual/api/reference/templatedashboard/object).|
|*Hosts*|&lt;|3|hostids|[Host](/manual/api/reference/host/get) ID.&lt;br&gt;&lt;br&gt;Note: To configure multiple hosts, create a dashboard widget field object for each host. For multiple hosts, the parameter *Host groups* must either be not configured at all or configured with at least one host group that the configured hosts belong to.&lt;br&gt;&lt;br&gt;Parameter *Hosts* not available when configuring the widget on a [template dashboard](/manual/api/reference/templatedashboard/object).|
|*Problem*|&lt;|1|problem|Problem [event name](/manual/config/triggers/trigger#configuration) (case insensitive, full name or part of it).|
|*Severity*|&lt;|0|severities|0 - Not classified;&lt;br&gt;1 - Information;&lt;br&gt;2 - Warning;&lt;br&gt;3 - Average;&lt;br&gt;4 - High;&lt;br&gt;5 - Disaster.&lt;br&gt;&lt;br&gt;Default: 1, 2, 3, 4, 5 (all enabled).&lt;br&gt;&lt;br&gt;Note: To configure multiple values, create a dashboard widget field object for each value.|
|*Problem tags* (the number in the property name (e.g. tags.tag.0) references tag order in the tag evaluation list)|&lt;|&lt;|&lt;|&lt;|
|&lt;|*Evaluation type*|0|evaltype|0 - *(default)* And/Or;&lt;br&gt;2 - Or.|
|^|*Tag name*|1|tags.tag.0|Any string value.&lt;br&gt;&lt;br&gt;[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):&lt;br&gt;- *required* if configuring *Problem tags*|
|^|*Operator*|0|tags.operator.0|0 - Contains;&lt;br&gt;1 - Equals;&lt;br&gt;2 - Does not contain;&lt;br&gt;3 - Does not equal;&lt;br&gt;4 - Exists;&lt;br&gt;5 - Does not exist.&lt;br&gt;&lt;br&gt;[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):&lt;br&gt;- *required* if configuring *Problem tags*|
|^|*Tag value*|1|tags.value.0|Any string value.&lt;br&gt;&lt;br&gt;[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):&lt;br&gt;- *required* if configuring *Problem tags*|
|*Show tags*|&lt;|0|show_tags|0 - *(default)* None;&lt;br&gt;1 - 1;&lt;br&gt;2 - 2;&lt;br&gt;3 - 3.|
|*Tag name* (format)|&lt;|0|tag_name_format|0 - *(default)* Full;&lt;br&gt;1 - Shortened;&lt;br&gt;2 - None.&lt;br&gt;&lt;br&gt;Parameter *Tag name* (format) not available if *Show tags* is set to "None".|
|*Tag display priority*|&lt;|1|tag_priority|Comma-separated list of tags.&lt;br&gt;&lt;br&gt;Parameter *Tag display priority* not available if *Show tags* is set to "None".|
|*Show operational data*|&lt;|0|show_opdata|0 - *(default)* None;&lt;br&gt;1 - Separately;&lt;br&gt;2 - With problem name.|
|*Show suppressed problems*|&lt;|0|show_suppressed|0 - *(default)* Disabled;&lt;br&gt;1 - Enabled.|
|*Acknowledgement status*|&lt;|0|acknowledgement_status|0 - *(default)* all;&lt;br&gt;1 - Unacknowledged; &lt;br&gt;2 - Acknowledged.|
|*By me*|&lt;|0|acknowledged_by_me|0 - *(default)* Disabled;&lt;br&gt;1 - Enabled.|
|*Sort entries by*|&lt;|0|sort_triggers|1 - Severity (descending);&lt;br&gt;2 - Host (ascending);&lt;br&gt;3 - Time (ascending);&lt;br&gt;4 - *(default)* Time (descending);&lt;br&gt;13 - Severity (ascending);&lt;br&gt;14 - Host (descending);&lt;br&gt;15 - Problem (ascending);&lt;br&gt;16 - Problem (descending).&lt;br&gt;&lt;br&gt;For all values, except "Time (descending)" and "Time (ascending)", the parameter *Show timeline* must be set to "Disabled".&lt;br&gt;&lt;br&gt;Values "Host (ascending)" and "Host (descending)" not available when configuring the widget on a [template dashboard](/manual/api/reference/templatedashboard/object).|
|*Show timeline*|&lt;|0|show_timeline|0 - Disabled;&lt;br&gt;1 - *(default)* Enabled.&lt;br&gt;&lt;br&gt;Parameter *Show timeline* available if *Sort entries by* is set to "Time (descending)" or "Time (ascending)".|
|*Show lines*|&lt;|0|show_lines|Valid values range from 1-100.&lt;br&gt;&lt;br&gt;Default: 25.|</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/api/reference/dashboard/widget_fields/problems.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="c49bef65" xml:space="preserve">
        <source>### Examples

The following examples aim to only describe the configuration of the dashboard widget field objects for the *Problems* widget.
For more information on configuring a dashboard, see [`dashboard.create`](/manual/api/reference/dashboard/create).</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/api/reference/dashboard/widget_fields/problems.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="1317b036" xml:space="preserve">
        <source>#### Configuring a *Problems* widget

Configure a *Problems* widget that displays problems for host group "4" that satisfy the following conditions:

-   Problems that have a tag with the name "scope" that contains values "performance" or "availability", or "capacity".
-   Problems that have the following severities: "Warning", "Average", "High", "Disaster".

In addition, configure the widget to show tags and operational data.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "dashboard.create",
    "params": {
        "name": "My dashboard",
        "display_period": 30,
        "auto_start": 1,
        "pages": [
            {
                "widgets": [
                    {
                        "type": "problems",
                        "name": "Problems",
                        "x": 0,
                        "y": 0,
                        "width": 12,
                        "height": 5,
                        "view_mode": 0,
                        "fields": [
                            {
                                "type": 2,
                                "name": "groupids",
                                "value": 4
                            },
                            {
                                "type": 1,
                                "name": "tags.tag.0",
                                "value": "scope"
                            },
                            {
                                "type": 0,
                                "name": "tags.operator.0",
                                "value": 0
                            },
                            {
                                "type": 1,
                                "name": "tags.value.0",
                                "value": "performance"
                            },
                            {
                                "type": 1,
                                "name": "tags.tag.1",
                                "value": "scope"
                            },
                            {
                                "type": 0,
                                "name": "tags.operator.1",
                                "value": 0
                            },
                            {
                                "type": 1,
                                "name": "tags.value.1",
                                "value": "availability"
                            },
                            {
                                "type": 1,
                                "name": "tags.tag.2",
                                "value": "scope"
                            },
                            {
                                "type": 0,
                                "name": "tags.operator.2",
                                "value": 0
                            },
                            {
                                "type": 1,
                                "name": "tags.value.2",
                                "value": "capacity"
                            },
                            {
                                "type": 0,
                                "name": "severities",
                                "value": 2
                            },
                            {
                                "type": 0,
                                "name": "severities",
                                "value": 3
                            },
                            {
                                "type": 0,
                                "name": "severities",
                                "value": 4
                            },
                            {
                                "type": 0,
                                "name": "severities",
                                "value": 5
                            },
                            {
                                "type": 0,
                                "name": "show_tags",
                                "value": 1
                            },
                            {
                                "type": 0,
                                "name": "show_opdata",
                                "value": 1
                            }
                        ]
                    }
                ]
            }
        ],
        "userGroups": [
            {
                "usrgrpid": 7,
                "permission": 2
            }
        ],
        "users": [
            {
                "userid": 1,
                "permission": 3
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "dashboardids": [
            "3"
        ]
    },
    "id": 1
}
```</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/api/reference/dashboard/widget_fields/problems.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="a778f187" xml:space="preserve">
        <source>### See also

-   [Dashboard widget field](/manual/api/reference/dashboard/object#dashboard-widget-field)
-   [`dashboard.create`](/manual/api/reference/dashboard/create)
-   [`dashboard.update`](/manual/api/reference/dashboard/update)</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/api/reference/dashboard/widget_fields/problems.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
    </body>
  </file>
</xliff>
