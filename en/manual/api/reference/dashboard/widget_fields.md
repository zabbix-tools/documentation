[comment]: # ({df2241c9-def34ea6})
# Dashboard widget fields

[comment]: # ({/df2241c9-def34ea6})

[comment]: # ({0459afda-42341e64})

This page contains navigation links for dashboard widget parameters
and possible property values for the respective [dashboard widget field](/manual/api/reference/dashboard/object#dashboard-widget-field) objects.

To see the parameters and property values for each widget, go to individual widget pages for:

-   [Action log](/manual/api/reference/dashboard/widget_fields/action_log)
-   [Clock](/manual/api/reference/dashboard/widget_fields/clock)
-   [Discovery status](/manual/api/reference/dashboard/widget_fields/discovery_status)
-   [Favorite graphs](/manual/api/reference/dashboard/widget_fields/favorite_graphs)
-   [Favorite maps](/manual/api/reference/dashboard/widget_fields/favorite_maps)
-   [Geomap](/manual/api/reference/dashboard/widget_fields/geomap)
-   [Graph](/manual/api/reference/dashboard/widget_fields/graph)
-   [Graph (classic)](/manual/api/reference/dashboard/widget_fields/graph_classic)
-   [Graph prototype](/manual/api/reference/dashboard/widget_fields/graph_prototype)
-   [Host availability](/manual/api/reference/dashboard/widget_fields/host_availability)
-   [Item value](/manual/api/reference/dashboard/widget_fields/item_value)
-   [Map](/manual/api/reference/dashboard/widget_fields/map)
-   [Map navigation tree](/manual/api/reference/dashboard/widget_fields/map_tree)
-   [Plain text](/manual/api/reference/dashboard/widget_fields/plain_text)
-   [Problem hosts](/manual/api/reference/dashboard/widget_fields/problem_hosts)
-   [Problems](/manual/api/reference/dashboard/widget_fields/problems)
-   [SLA report](/manual/api/reference/dashboard/widget_fields/sla_report)
-   [System information](/manual/api/reference/dashboard/widget_fields/system)
-   [Problems by severity](/manual/api/reference/dashboard/widget_fields/problems_severity)
-   [Top hosts](/manual/api/reference/dashboard/widget_fields/top_hosts)
-   [Trigger overview](/manual/api/reference/dashboard/widget_fields/trigger_overview)
-   [URL](/manual/api/reference/dashboard/widget_fields/url)
-   [Web monitoring](/manual/api/reference/dashboard/widget_fields/web_monitoring)

Deprecated widgets:

-   [Data overview](/manual/api/reference/dashboard/widget_fields/data_overview)

[comment]: # ({/0459afda-42341e64})
