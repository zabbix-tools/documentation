[comment]: # ({c6507e4b-79c270c0})
# Map

This class is designed to work with maps.

Object references:

-   [Map](/manual/api/reference/map/object#map)
-   [Map element](/manual/api/reference/map/object#map_element)
-   [Map element Host](/manual/api/reference/map/object#map_element_host)
-   [Map element Host group](/manual/api/reference/map/object#map_element_host_group)
-   [Map element Map](/manual/api/reference/map/object#map_element_map)
-   [Map element Trigger](/manual/api/reference/map/object#map_element_trigger)
-   [Map element tag](/manual/api/reference/map/object#map_element_tag)
-   [Map element URL](/manual/api/reference/map/object#map_element_url)
-   [Map link](/manual/api/reference/map/object#map_link)
-   [Map link trigger](/manual/api/reference/map/object#map_link_trigger)
-   [Map URL](/manual/api/reference/map/object#map_url)
-   [Map user](/manual/api/reference/map/object#map_user)
-   [Map user group](/manual/api/reference/map/object#map_user_group)
-   [Map shapes](/manual/api/reference/map/object#map_shapes)
-   [Map lines](/manual/api/reference/map/object#map_lines)

Available methods:

-   [map.create](/manual/api/reference/map/create) - create new maps
-   [map.delete](/manual/api/reference/map/delete) - delete maps
-   [map.get](/manual/api/reference/map/get) - retrieve maps
-   [map.update](/manual/api/reference/map/update) - update maps

[comment]: # ({/c6507e4b-79c270c0})
