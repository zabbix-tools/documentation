[comment]: # ({12a28e04-8eb68faf})
# Host group

This class is designed to work with host groups.

Object references:

-   [Host group](/manual/api/reference/hostgroup/object#host_group)

Available methods:

-   [hostgroup.create](/manual/api/reference/hostgroup/create) - create new host groups
-   [hostgroup.delete](/manual/api/reference/hostgroup/delete) - delete host groups
-   [hostgroup.get](/manual/api/reference/hostgroup/get) - retrieve host groups
-   [hostgroup.massadd](/manual/api/reference/hostgroup/massadd) - add related objects to host groups
-   [hostgroup.massremove](/manual/api/reference/hostgroup/massremove) - remove related objects from host groups
-   [hostgroup.massupdate](/manual/api/reference/hostgroup/massupdate) - replace or remove related objects from host groups
-   [hostgroup.propagate](/manual/api/reference/hostgroup/propagate) - propagate permissions and tag filters to host groups' subgroups
-   [hostgroup.update](/manual/api/reference/hostgroup/update) - update host groups

[comment]: # ({/12a28e04-8eb68faf})
