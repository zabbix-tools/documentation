[comment]: # ({f5d7e62f-f5d7e62f})
# service.create

[comment]: # ({/f5d7e62f-f5d7e62f})

[comment]: # ({b56f2d44-c44d5a50})
### Description

`object service.create(object/array services)`

This method allows to create new services.

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/b56f2d44-c44d5a50})

[comment]: # ({ff608509-69be5e01})
### Parameters

`(object/array)` services to create.

Additionally to the [standard service properties](object#service), the
method accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|children|array|Child services to be linked to the service.<br><br>The children must have the `serviceid` property defined.|
|parents|array|Parent services to be linked to the service.<br><br>The parents must have the `serviceid` property defined.|
|tags|array|Service [tags](/manual/api/reference/service/object#service_tag) to be created for the service.|
|problem\_tags|array|[Problem tags](/manual/api/reference/service/object#problem_tag) to be created for the service.|
|status\_rules|array|[Status rules](/manual/api/reference/service/object#status_rule) to be created for the service.|

[comment]: # ({/ff608509-69be5e01})

[comment]: # ({f5eb6cfd-f5eb6cfd})
### Return values

`(object)` Returns an object containing the IDs of the created services
under the `serviceids` property. The order of the returned IDs matches
the order of the passed services.

[comment]: # ({/f5eb6cfd-f5eb6cfd})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({42d64619-fc6bed3c})
#### Creating a service

Create a service that will be switched to problem state, if at least one
child has a problem.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "service.create",
    "params": {
        "name": "Server 1",
        "algorithm": 1,
        "sortorder": 1
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "serviceids": [
            "5"
        ]
    },
    "id": 1
}
```

[comment]: # ({/42d64619-fc6bed3c})

[comment]: # ({fda6729e-fda6729e})
### Source

CService::create() in *ui/include/classes/api/services/CService.php*.

[comment]: # ({/fda6729e-fda6729e})
