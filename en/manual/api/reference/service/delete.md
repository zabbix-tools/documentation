[comment]: # ({bf12fc32-bf12fc32})
# service.delete

[comment]: # ({/bf12fc32-bf12fc32})

[comment]: # ({78088b4b-8ea5261f})
### Description

`object service.delete(array serviceIds)`

This method allows to delete services.

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/78088b4b-8ea5261f})

[comment]: # ({7af05f92-7af05f92})
### Parameters

`(array)` IDs of the services to delete.

[comment]: # ({/7af05f92-7af05f92})

[comment]: # ({23960c29-23960c29})
### Return values

`(object)` Returns an object containing the IDs of the deleted services
under the `serviceids` property.

[comment]: # ({/23960c29-23960c29})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({16e82f40-5036ec70})
#### Deleting multiple services

Delete two services.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "service.delete",
    "params": [
        "4",
        "5"
    ],
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "serviceids": [
            "4",
            "5"
        ]
    },
    "id": 1
}
```

[comment]: # ({/16e82f40-5036ec70})

[comment]: # ({c99e5046-c99e5046})
### Source

CService::delete() in *ui/include/classes/api/services/CService.php*.

[comment]: # ({/c99e5046-c99e5046})
