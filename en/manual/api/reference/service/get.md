[comment]: # ({2587902b-2587902b})
# service.get

[comment]: # ({/2587902b-2587902b})

[comment]: # ({8223dd96-bfd5de7f})
### Description

`integer/array service.get(object parameters)`

The method allows to retrieve services according to the given
parameters.

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/8223dd96-bfd5de7f})

[comment]: # ({8cb6e004-e7637a1d})
### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|serviceids|string/array|Return only services with the given IDs.|
|parentids|string/array|Return only services that are linked to the given parent services.|
|deep\_parentids|flag|Return all direct and indirect child services. Used together with `parentids`.|
|childids|string/array|Return only services that are linked to the given child services.|
|evaltype|integer|Rules for tag searching.<br><br>Possible values:<br>0 - *(default)* And/Or;<br>2 - Or.|
|tags|object/array of objects|Return only services with given tags. Exact match by tag and case-sensitive or case-insensitive search by tag value depending on operator value.<br>Format: `[{"tag": "<tag>", "value": "<value>", "operator": "<operator>"}, ...]`.<br>An empty array returns all services.<br><br>Possible operator values:<br>0 - *(default)* Contains;<br>1 - Equals;<br>2 - Does not contain;<br>3 - Does not equal;<br>4 - Exists;<br>5 - Does not exist.|
|problem\_tags|object/array of objects|Return only services with given problem tags. Exact match by tag and case-sensitive or case-insensitive search by tag value depending on operator value.<br>Format: `[{"tag": "<tag>", "value": "<value>", "operator": "<operator>"}, ...]`.<br>An empty array returns all services.<br><br>Possible operator values:<br>0 - *(default)* Contains;<br>1 - Equals;<br>2 - Does not contain;<br>3 - Does not equal;<br>4 - Exists;<br>5 - Does not exist.|
|without\_problem\_tags|flag|Return only services without problem tags.|
|slaids|string/array|Return only services that are linked to the specific SLA(s).|
|selectChildren|query|Return a `children` property with the child services.<br><br>Supports `count`.|
|selectParents|query|Return a `parents` property with the parent services.<br><br>Supports `count`.|
|selectTags|query|Return a [`tags`](/manual/api/reference/service/object#service_tag) property with service tags.<br><br>Supports `count`.|
|selectProblemEvents|query|Return a `problem_events` property with an array of problem event objects.<br><br>The problem event object has the following properties:<br>`eventid` - `(string)` Event ID;<br>`severity` - `(string)` Current event severity;<br>`name` - `(string)` Resolved event name.<br><br>Supports `count`.|
|selectProblemTags|query|Return a [`problem_tags`](/manual/api/reference/service/object#problem_tag) property with problem tags.<br><br>Supports `count`.|
|selectStatusRules|query|Return a [`status_rules`](/manual/api/reference/service/object#status_rule) property with status rules.<br><br>Supports `count`.|
|selectStatusTimeline|object/array of objects|Return a `status_timeline` property containing service state changes for given periods.<br><br>Format `[{"period_from": "<period_from>", "period_to": "<period_to>"}, ...]` - `period_from` being a starting date (inclusive; integer timestamp) and `period_to` being an ending date (exclusive; integer timestamp) for the period you're interested in.<br><br>Returns an array of entries containing a `start_value` property and an [`alarms`](/manual/api/reference/service/object#service-alarm) array for the state changes within specified periods.|
|sortfield|string/array|Sort the result by the given properties.<br><br>Possible values: `serviceid`, `name`, `status`, `sortorder`, `created_at`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters).|
|editable|boolean|^|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

[comment]: # ({/8cb6e004-e7637a1d})

[comment]: # ({7223bab1-7223bab1})
### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.

[comment]: # ({/7223bab1-7223bab1})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({70117b93-cd4e4011})
#### Retrieving all services

Retrieve all data about all services and their dependencies.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "service.get",
    "params": {
        "output": "extend",
        "selectChildren": "extend",
        "selectParents": "extend"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": [
        {
            "serviceid": "1",
            "name": "My Service - 0001",
            "status": "-1",
            "algorithm": "2",
            "sortorder": "0",
            "weight": "0",
            "propagation_rule": "0",
            "propagation_value": "0",
            "description": "My Service Description 0001.",
            "uuid": "dfa4daeaea754e3a95c04d6029182681",
            "created_at": "946684800",
            "readonly": false,
            "parents": [],
            "children": []
        },
        {
            "serviceid": "2",
            "name": "My Service - 0002",
            "status": "-1",
            "algorithm": "2",
            "sortorder": "0",
            "weight": "0",
            "propagation_rule": "0",
            "propagation_value": "0",
            "description": "My Service Description 0002.",
            "uuid": "20ea0d85212841219130abeaca28c065",
            "created_at": "946684800",
            "readonly": false,
            "parents": [],
            "children": []
        }
    ],
    "id": 1
}
```

[comment]: # ({/70117b93-cd4e4011})

[comment]: # ({05e3cf18-05e3cf18})
### Source

CService::get() in *ui/include/classes/api/services/CService.php*.

[comment]: # ({/05e3cf18-05e3cf18})
