[comment]: # ({b1a89f1b-b1a89f1b})
# > Service object

The following objects are directly related to the `service` API.

[comment]: # ({/b1a89f1b-b1a89f1b})

[comment]: # ({5abf08c5-5d7a3d70})
### Service

The service object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|serviceid|string|ID of the service.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*<br>- *required* for update operations|
|algorithm|integer|Status calculation rule. Only applicable if child services exist.<br><br>Possible values:<br>0 - set status to OK;<br>1 - most critical if all children have problems;<br>2 - most critical of child services.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* for create operations|
|name|string|Name of the service.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* for create operations|
|sortorder|integer|Position of the service used for sorting.<br><br>Possible values: 0-999.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* for create operations|
|weight|integer|Service weight.<br><br>Possible values: 0-1000000.<br><br>Default: 0.|
|propagation\_rule|integer|Status propagation rule.<br><br>Possible values:<br>0 - *(default)* propagate service status as is - without any changes;<br>1 - increase the propagated status by a given `propagation_value` (by 1 to 5 severities);<br>2 - decrease the propagated status by a given `propagation_value` (by 1 to 5 severities);<br>3 - ignore this service - the status is not propagated to the parent service at all;<br>4 - set fixed service status using a given `propagation_value`.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `propagation_value` is set|
|propagation\_value|integer|Status propagation value.<br><br>Possible values if `propagation_rule` is set to "0" or "3":<br>0 - Not classified.<br><br>Possible values if `propagation_rule` is set to "1" or "2":<br>1 - Information;<br>2 - Warning;<br>3 - Average;<br>4 - High;<br>5 - Disaster.<br><br>Possible values if `propagation_rule` is set to "4":<br>-1 - OK;<br>0 - Not classified;<br>1 - Information;<br>2 - Warning;<br>3 - Average;<br>4 - High;<br>5 - Disaster.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `propagation_rule` is set|
|status|integer|Whether the service is in OK or problem state.<br><br>If the service is in problem state, `status` is equal either to:<br>- the severity of the most critical problem;<br>- the highest status of a child service in problem state.<br><br>If the service is in OK state, `status` is equal to: -1.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*|
|description|string|Description of the service.|
|uuid|string|Universal unique identifier, used for linking imported services to already existing ones. Auto-generated, if not given.|
|created_at|integer|Unix timestamp when service was created.|
|readonly|boolean|Access to the service.<br><br>Possible values:<br>0 - Read-write;<br>1 - Read-only.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*|

[comment]: # ({/5abf08c5-5d7a3d70})

[comment]: # ({a07ec2b6-c2041847})
### Status rule

The status rule object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|type|integer|Condition for setting (New status) status.<br><br>Possible values:<br>0 - if at least (N) child services have (Status) status or above;<br>1 - if at least (N%) of child services have (Status) status or above;<br>2 - if less than (N) child services have (Status) status or below;<br>3 - if less than (N%) of child services have (Status) status or below;<br>4 - if weight of child services with (Status) status or above is at least (W);<br>5 - if weight of child services with (Status) status or above is at least (N%);<br>6 - if weight of child services with (Status) status or below is less than (W);<br>7 - if weight of child services with (Status) status or below is less than (N%).<br><br>Where:<br>- N (W) is `limit_value`;<br>- (Status) is `limit_status`;<br>- (New status) is `new_status`.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|limit\_value|integer|Limit value.<br><br>Possible values:<br>- for N and W: 1-100000;<br>- for N%: 1-100.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|limit\_status|integer|Limit status.<br><br>Possible values:<br>-1 - OK;<br>0 - Not classified;<br>1 - Information;<br>2 - Warning;<br>3 - Average;<br>4 - High;<br>5 - Disaster.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|new\_status|integer|New status value.<br><br>Possible values:<br>0 - Not classified;<br>1 - Information;<br>2 - Warning;<br>3 - Average;<br>4 - High;<br>5 - Disaster.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|

[comment]: # ({/a07ec2b6-c2041847})

[comment]: # ({8b8faf20-c301cfb2})
### Service tag

The service tag object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|tag|string|Service tag name.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|value|string|Service tag value.|

[comment]: # ({/8b8faf20-c301cfb2})

[comment]: # ({da314eef-e6daa7b4})
### Service alarm

::: noteclassic
Service alarms cannot be directly created, updated or
deleted via the Zabbix API.
:::

The service alarm objects represent a service's state change. It has
the following properties.

|Property |[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|clock|timestamp|Time when the service state change has happened.|
|value|integer|Status of the service.<br><br>Refer to the [service status property](object#service) for a list of possible values.|

[comment]: # ({/da314eef-e6daa7b4})

[comment]: # ({1ccef800-6b88d3dc})
### Problem tag

Problem tags allow linking services with problem events. The problem tag
object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|tag|string|Problem tag name.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|operator|integer|Mapping condition operator.<br><br>Possible values:<br>0 - *(default)* equals;<br>2 - like.|
|value|string|Problem tag value.|

[comment]: # ({/1ccef800-6b88d3dc})
