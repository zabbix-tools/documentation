[comment]: # ({cd4a930c-cd4a930c})
# service.update

[comment]: # ({/cd4a930c-cd4a930c})

[comment]: # ({5044bc6a-79afe9b7})
### Description

`object service.update(object/array services)`

This method allows to update existing services.

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/5044bc6a-79afe9b7})

[comment]: # ({74e0e89f-5fb5ebb3})
### Parameters

`(object/array)` service properties to be updated.

The `serviceid` property must be defined for each service, all other
properties are optional. Only the passed properties will be updated, all
others will remain unchanged.

Additionally to the [standard service properties](object#service), the
method accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)| Description|
|--|--|------|
|children|array|Child services to replace the current service children.<br><br>The children must have the `serviceid` property defined.|
|parents|array|Parent services to replace the current service parents.<br><br>The parents must have the `serviceid` property defined.|
|tags|array|Service [tags](/manual/api/reference/service/object#service_tag) to replace the current service tags.|
|problem\_tags|array|[Problem tags](/manual/api/reference/service/object#problem_tag) to replace the current problem tags.|
|status\_rules|array|[Status rules](/manual/api/reference/service/object#status_rule) to replace the current status rules.|

[comment]: # ({/74e0e89f-5fb5ebb3})

[comment]: # ({9465ee4d-9465ee4d})
### Return values

`(object)` Returns an object containing the IDs of the updated services
under the `serviceids` property.

[comment]: # ({/9465ee4d-9465ee4d})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({3db3fe16-b9918036})
#### Setting the parent for a service

Make service with ID "3" to be the parent for service with ID "5".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "service.update",
    "params": {
        "serviceid": "5",
        "parents": [
            {
                "serviceid": "3"
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "serviceids": [
            "5"
        ]
    },
    "id": 1
}
```

[comment]: # ({/3db3fe16-b9918036})

[comment]: # ({96cfca7d-263b1baa})
#### Adding a scheduled downtime

Add a downtime for service with ID "4" scheduled weekly from Monday
22:00 till Tuesday 10:00.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "service.update",
    "params": {
        "serviceid": "4",
        "times": [
            {
                "type": "1",
                "ts_from": "165600",
                "ts_to": "201600"
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "serviceids": [
            "4"
        ]
    },
    "id": 1
}
```

[comment]: # ({/96cfca7d-263b1baa})

[comment]: # ({fd1b6894-fd1b6894})
### Source

CService::update() in *ui/include/classes/api/services/CService.php*.

[comment]: # ({/fd1b6894-fd1b6894})
