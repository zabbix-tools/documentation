[comment]: # ({2aef6e95-2aef6e95})
# templatedashboard.create

[comment]: # ({/2aef6e95-2aef6e95})

[comment]: # ({9231e0c6-8072f2f2})
### Description

`object templatedashboard.create(object/array templateDashboards)`

This method allows to create new template dashboards.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/9231e0c6-8072f2f2})

[comment]: # ({c81f9dd3-1caae149})
### Parameters

`(object/array)` Template dashboards to create.

Additionally to the [standard template dashboard
properties](object#template_dashboard), the method accepts the following
parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|pages|array|Template dashboard [pages](object#dashboard_page) to be created for the dashboard. Dashboard pages will be ordered in the same order as specified.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required*|

[comment]: # ({/c81f9dd3-1caae149})

[comment]: # ({4bfe9ed0-4bfe9ed0})
### Return values

`(object)` Returns an object containing the IDs of the created template
dashboards under the `dashboardids` property. The order of the returned
IDs matches the order of the passed template dashboards.

[comment]: # ({/4bfe9ed0-4bfe9ed0})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({41c665c3-49684d2f})
#### Creating a template dashboard

Create a template dashboard named “Graphs” with one Graph widget on a
single dashboard page.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "templatedashboard.create",
    "params": {
        "templateid": "10318",
        "name": "Graphs",
        "pages": [
            {
                "widgets": [
                    {
                        "type": "graph",
                        "x": 0,
                        "y": 0,
                        "width": 12,
                        "height": 5,
                        "view_mode": 0,
                        "fields": [
                            {
                                "type": 6,
                                "name": "graphid",
                                "value": "1123"
                            }
                        ]
                    }
                ]

            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "dashboardids": [
            "32"
        ]
    },
    "id": 1
}
```

[comment]: # ({/41c665c3-49684d2f})

[comment]: # ({1171cc63-1171cc63})
### See also

-   [Template dashboard page](object#template_dashboard_page)
-   [Template dashboard widget](object#template_dashboard_widget)
-   [Template dashboard widget
    field](object#template_dashboard_widget_field)

[comment]: # ({/1171cc63-1171cc63})

[comment]: # ({78c6e281-78c6e281})
### Source

CTemplateDashboard::create() in
*ui/include/classes/api/services/CTemplateDashboard.php*.

[comment]: # ({/78c6e281-78c6e281})
