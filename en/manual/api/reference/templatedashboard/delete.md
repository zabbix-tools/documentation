[comment]: # ({9dee678d-9dee678d})
# templatedashboard.delete

[comment]: # ({/9dee678d-9dee678d})

[comment]: # ({9a7f7f58-f76c3800})
### Description

`object templatedashboard.delete(array templateDashboardIds)`

This method allows to delete template dashboards.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/9a7f7f58-f76c3800})

[comment]: # ({276bf1c0-276bf1c0})
### Parameters

`(array)` IDs of the template dashboards to delete.

[comment]: # ({/276bf1c0-276bf1c0})

[comment]: # ({73b9628f-73b9628f})
### Return values

`(object)` Returns an object containing the IDs of the deleted template
dashboards under the `dashboardids` property.

[comment]: # ({/73b9628f-73b9628f})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({d5d697e7-5c967ef8})
#### Deleting multiple template dashboards

Delete two template dashboards.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "templatedashboard.delete",
    "params": [
        "45",
        "46"
    ],
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "dashboardids": [
            "45",
            "46"
        ]
    },
    "id": 1
}
```

[comment]: # ({/d5d697e7-5c967ef8})

[comment]: # ({d4de7a93-d4de7a93})
### Source

CTemplateDashboard::delete() in
*ui/include/classes/api/services/CTemplateDashboard.php*.

[comment]: # ({/d4de7a93-d4de7a93})
