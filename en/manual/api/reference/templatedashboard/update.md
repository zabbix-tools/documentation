[comment]: # ({6d5ea9cd-6d5ea9cd})
# templatedashboard.update

[comment]: # ({/6d5ea9cd-6d5ea9cd})

[comment]: # ({e82cca7f-3db870df})
### Description

`object templatedashboard.update(object/array templateDashboards)`

This method allows to update existing template dashboards.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/e82cca7f-3db870df})

[comment]: # ({44e74f32-051bc982})
### Parameters

`(object/array)` Template dashboard properties to be updated.

The `dashboardid` property must be specified for each dashboard, all
other properties are optional. Only the specified properties will be
updated.

Additionally to the [standard template dashboard
properties](object#template_dashboard), the method accepts the following
parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|pages|array|Template dashboard [pages](object#template_dashboard_page) to replace the existing dashboard pages.<br><br>Dashboard pages are updated by the `dashboard_pageid` property. New dashboard pages will be created for objects without `dashboard_pageid` property and the existing dashboard pages will be deleted if not reused. Dashboard pages will be ordered in the same order as specified. Only the specified properties of the dashboard pages will be updated. At least one dashboard page object is required for `pages` property.|

[comment]: # ({/44e74f32-051bc982})

[comment]: # ({d8b2cba8-d8b2cba8})
### Return values

`(object)` Returns an object containing the IDs of the updated template
dashboards under the `dashboardids` property.

[comment]: # ({/d8b2cba8-d8b2cba8})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({7e85dec3-7b6290ef})
#### Renaming a template dashboard

Rename a template dashboard to "Performance graphs".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "templatedashboard.update",
    "params": {
        "dashboardid": "23",
        "name": "Performance graphs"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "dashboardids": [
            "23"
        ]
    },
    "id": 1
}
```

[comment]: # ({/7e85dec3-7b6290ef})

[comment]: # ({deee7d3d-cf0dcd1e})
#### Updating template dashboard pages

Rename the first dashboard page, replace widgets on the second dashboard
page and add a new page as the third one. Delete all other dashboard
pages.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "templatedashboard.update",
    "params": {
        "dashboardid": "2",
        "pages": [
            {
                "dashboard_pageid": 1,
                "name": "Renamed Page"
            },
            {
                "dashboard_pageid": 2,
                "widgets": [
                    {
                        "type": "clock",
                        "x": 0,
                        "y": 0,
                        "width": 4,
                        "height": 3
                    }
                ]
            },
            {
                "display_period": 60
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "dashboardids": [
            "2"
        ]
    },
    "id": 1
}
```

[comment]: # ({/deee7d3d-cf0dcd1e})

[comment]: # ({24de180a-24de180a})
### See also

-   [Template dashboard widget](object#template_dashboard_widget)
-   [Template dashboard widget
    field](object#template_dashboard_widget_field)

[comment]: # ({/24de180a-24de180a})

[comment]: # ({fb185432-fb185432})
### Source

CTemplateDashboard::update() in
*ui/include/classes/api/services/CTemplateDashboard.php*.

[comment]: # ({/fb185432-fb185432})
