[comment]: # ({1d7fc2e5-1d7fc2e5})
# > Template dashboard object

The following objects are directly related to the `templatedashboard`
API.

[comment]: # ({/1d7fc2e5-1d7fc2e5})

[comment]: # ({18160d62-e0343baf})
### Template dashboard

The template dashboard object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|dashboardid|string|ID of the template dashboard.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*<br>- *required* for update operations|
|name|string|Name of the template dashboard.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* for create operations|
|templateid|string|ID of the template the dashboard belongs to.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *constant*<br>- *required* for create operations|
|display\_period|integer|Default page display period (in seconds).<br><br>Possible values: 10, 30, 60, 120, 600, 1800, 3600.<br><br>Default: 30.|
|auto\_start|integer|Auto start slideshow.<br><br>Possible values:<br>0 - do not auto start slideshow;<br>1 - *(default)* auto start slideshow.|
|uuid|string|Universal unique identifier, used for linking imported template dashboards to already existing ones. Auto-generated, if not given.|

[comment]: # ({/18160d62-e0343baf})

[comment]: # ({3448ff3f-69f1df50})
### Template dashboard page

The template dashboard page object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|dashboard\_pageid|string|ID of the dashboard page.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*|
|name|string|Dashboard page name.<br><br>Default: empty string.|
|display\_period|integer|Dashboard page display period (in seconds).<br><br>Possible values: 0, 10, 30, 60, 120, 600, 1800, 3600.<br><br>Default: 0 (will use the default page display period).|
|widgets|array|Array of the [template dashboard widget](object#template_dashboard_widget) objects.|

[comment]: # ({/3448ff3f-69f1df50})

[comment]: # ({75982a6f-9c7cc873})
### Template dashboard widget

The template dashboard widget object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|widgetid|string|ID of the dashboard widget.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*|
|type|string|Type of the dashboard widget.<br><br>Possible values:<br>actionlog - Action log;<br>clock - Clock;<br>*(deprecated)* dataover - Data overview;<br>discovery - Discovery status;<br>favgraphs - Favorite graphs;<br>favmaps - Favorite maps;<br>graph - Graph (classic);<br>graphprototype - Graph prototype;<br>hostavail - Host availability;<br>item - Item value;<br>map - Map;<br>navtree - Map Navigation Tree;<br>plaintext - Plain text;<br>problemhosts - Problem hosts;<br>problems - Problems;<br>problemsbysv - Problems by severity;<br>slareport - SLA report;<br>svggraph - Graph;<br>systeminfo - System information;<br>tophosts - Top hosts;<br>trigover - Trigger overview;<br>url - URL;<br>web - Web monitoring.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|name|string|Custom widget name.|
|x|integer|A horizontal position from the left side of the dashboard.<br><br>Valid values range from 0 to 23.|
|y|integer|A vertical position from the top of the dashboard.<br><br>Valid values range from 0 to 62.|
|width|integer|The widget width.<br><br>Valid values range from 1 to 24.|
|height|integer|The widget height.<br><br>Valid values range from 2 to 32.|
|view\_mode|integer|The widget view mode.<br><br>Possible values:<br>0 - *(default)* default widget view;<br>1 - with hidden header;|
|fields|array|Array of the [template dashboard widget field](object#template_dashboard_widget_field) objects.|

[comment]: # ({/75982a6f-9c7cc873})

[comment]: # ({95c3b01e-76d2eb3b})
### Template dashboard widget field

The template dashboard widget field object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|type|integer|Type of the widget field.<br><br>Possible values:<br>0 - Integer;<br>1 - String;<br>4 - Item;<br>5 - Item prototype;<br>6 - Graph;<br>7 - Graph prototype;<br>8 - Map;<br>9 - Service;<br>10 - SLA;<br>11 - User;<br>12 - Action;<br>13 - Media type.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|name|string|Widget field name.<br><br>Possible values: see [Dashboard widget fields](/manual/api/reference/dashboard/widget_fields). Note that some host-related parameters (e.g., *Host groups*, *Exclude host groups* and *Hosts* in the [*Problems*](/manual/api/reference/dashboard/widget_fields/problems#parameters) widget, *Host groups* in the [*Host availability*](/manual/api/reference/dashboard/widget_fields/host_availability#parameters) widget, etc.) are not available when configuring the widget on a template dashboard. This is because template dashboards display data only from the host that the template is linked to.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|value|mixed|Widget field value depending on the type.<br><br>Possible values: see [Dashboard widget fields](/manual/api/reference/dashboard/widget_fields). Note that some host-related parameters (e.g., *Host groups*, *Exclude host groups* and *Hosts* in the [*Problems*](/manual/api/reference/dashboard/widget_fields/problems#parameters) widget, *Host groups* in the [*Host availability*](/manual/api/reference/dashboard/widget_fields/host_availability#parameters) widget, etc.) are not available when configuring the widget on a template dashboard. This is because template dashboards display data only from the host that the template is linked to.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|

[comment]: # ({/95c3b01e-76d2eb3b})
