[comment]: # ({d14758f6-b1a4e412})
# Correlation

This class is designed to work with correlations.

Object references:\

-   [Correlation](/manual/api/reference/correlation/object#correlation)
-   [Correlation operation](/manual/api/reference/correlation/object#correlation_operation)
-   [Correlation filter](/manual/api/reference/correlation/object#correlation_filter)
-   [Correlation filter condition](/manual/api/reference/correlation/object#correlation_filter_condition)

Available methods:\

-   [correlation.create](/manual/api/reference/correlation/create) - create new correlations
-   [correlation.delete](/manual/api/reference/correlation/delete) - delete correlations
-   [correlation.get](/manual/api/reference/correlation/get) - retrieve correlations
-   [correlation.update](/manual/api/reference/correlation/update) - update correlations

[comment]: # ({/d14758f6-b1a4e412})
