[comment]: # ({576421be-576421be})
# graphprototype.create

[comment]: # ({/576421be-576421be})

[comment]: # ({e23e6a64-fc8d3493})
### Description

`object graphprototype.create(object/array graphPrototypes)`

This method allows to create new graph prototypes.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/e23e6a64-fc8d3493})

[comment]: # ({69be1443-9beca929})
### Parameters

`(object/array)` Graph prototypes to create.

Additionally to the [standard graph prototype
properties](object#graph_prototype), the method accepts the following
parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|gitems|array|Graph [items](/manual/api/reference/graphitem/object) to be created for the graph prototypes. Graph items can reference both items and item prototypes, but at least one item prototype must be present.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required*|

[comment]: # ({/69be1443-9beca929})

[comment]: # ({f1317691-f1317691})
### Return values

`(object)` Returns an object containing the IDs of the created graph
prototypes under the `graphids` property. The order of the returned IDs
matches the order of the passed graph prototypes.

[comment]: # ({/f1317691-f1317691})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({89fcb1e2-24ec4c22})
#### Creating a graph prototype

Create a graph prototype with two items.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "graphprototype.create",
    "params": {
        "name": "Disk space usage {#FSNAME}",
        "width": 900,
        "height": 200,
        "gitems": [
            {
                "itemid": "22828",
                "color": "00AA00"
            },
            {
                "itemid": "22829",
                "color": "3333FF"
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "graphids": [
            "652"
        ]
    },
    "id": 1
}
```

[comment]: # ({/89fcb1e2-24ec4c22})

[comment]: # ({88266563-88266563})
### See also

-   [Graph item](/manual/api/reference/graphitem/object#graph_item)

[comment]: # ({/88266563-88266563})

[comment]: # ({c4b4834a-c4b4834a})
### Source

CGraphPrototype::create() in
*ui/include/classes/api/services/CGraphPrototype.php*.

[comment]: # ({/c4b4834a-c4b4834a})
