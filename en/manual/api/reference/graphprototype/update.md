[comment]: # ({a23a9b82-a23a9b82})
# graphprototype.update

[comment]: # ({/a23a9b82-a23a9b82})

[comment]: # ({02aaf7f4-5ac42d98})
### Description

`object graphprototype.update(object/array graphPrototypes)`

This method allows to update existing graph prototypes.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/02aaf7f4-5ac42d98})

[comment]: # ({e9448d67-c4171dc6})
### Parameters

`(object/array)` Graph prototype properties to be updated.

The `graphid` property must be defined for each graph prototype, all
other properties are optional. Only the passed properties will be
updated, all others will remain unchanged.

Additionally to the [standard graph prototype
properties](object#graph_prototype), the method accepts the following
parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|gitems|array|Graph [items](/manual/api/reference/graphitem/object) to replace existing graph items. If a graph item has the `gitemid` property defined it will be updated, otherwise a new graph item will be created.|

[comment]: # ({/e9448d67-c4171dc6})

[comment]: # ({8d626778-8d626778})
### Return values

`(object)` Returns an object containing the IDs of the updated graph
prototypes under the `graphids` property.

[comment]: # ({/8d626778-8d626778})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({5121447c-c044eff0})
#### Changing the size of a graph prototype

Change the size of a graph prototype to 1100 to 400 pixels.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "graphprototype.update",
    "params": {
        "graphid": "439",
        "width": 1100,
        "height": 400
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "graphids": [
            "439"
        ]
    },
    "id": 1
}
```

[comment]: # ({/5121447c-c044eff0})

[comment]: # ({743007eb-743007eb})
### Source

CGraphPrototype::update() in
*ui/include/classes/api/services/CGraphPrototype.php*.

[comment]: # ({/743007eb-743007eb})
