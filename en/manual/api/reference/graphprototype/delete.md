[comment]: # ({0a247a7d-0a247a7d})
# graphprototype.delete

[comment]: # ({/0a247a7d-0a247a7d})

[comment]: # ({6433bb0c-8256b270})
### Description

`object graphprototype.delete(array graphPrototypeIds)`

This method allows to delete graph prototypes.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/6433bb0c-8256b270})

[comment]: # ({3e4a53cc-3e4a53cc})
### Parameters

`(array)` IDs of the graph prototypes to delete.

[comment]: # ({/3e4a53cc-3e4a53cc})

[comment]: # ({82123f70-82123f70})
### Return values

`(object)` Returns an object containing the IDs of the deleted graph
prototypes under the `graphids` property.

[comment]: # ({/82123f70-82123f70})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({ce5f8001-d24b4f06})
#### Deleting multiple graph prototypes

Delete two graph prototypes.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "graphprototype.delete",
    "params": [
        "652",
        "653"
    ],
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "graphids": [
            "652",
            "653"
        ]
    },
    "id": 1
}
```

[comment]: # ({/ce5f8001-d24b4f06})

[comment]: # ({21f88b2f-21f88b2f})
### Source

CGraphPrototype::delete() in
*ui/include/classes/api/services/CGraphPrototype.php*.

[comment]: # ({/21f88b2f-21f88b2f})
