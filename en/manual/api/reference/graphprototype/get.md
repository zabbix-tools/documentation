[comment]: # ({6a93039d-6a93039d})
# graphprototype.get

[comment]: # ({/6a93039d-6a93039d})

[comment]: # ({aeecab61-27b69e53})
### Description

`integer/array graphprototype.get(object parameters)`

The method allows to retrieve graph prototypes according to the given
parameters.

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/aeecab61-27b69e53})

[comment]: # ({80cb99da-de0f74af})
### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|discoveryids|string/array|Return only graph prototypes that belong to the given discovery rules.|
|graphids|string/array|Return only graph prototypes with the given IDs.|
|groupids|string/array|Return only graph prototypes that belong to hosts or templates in the given host groups or template groups.|
|hostids|string/array|Return only graph prototypes that belong to the given hosts.|
|inherited|boolean|If set to `true` return only graph prototypes inherited from a template.|
|itemids|string/array|Return only graph prototypes that contain the given item prototypes.|
|templated|boolean|If set to `true` return only graph prototypes that belong to templates.|
|templateids|string/array|Return only graph prototypes that belong to the given templates.|
|selectDiscoveryRule|query|Return a [`discoveryRule`](/manual/api/reference/discoveryrule/object#lld_rule) property with the LLD rule that the graph prototype belongs to.|
|selectGraphItems|query|Return a [`gitems`](/manual/api/reference/graphitem/object) property with the graph items used in the graph prototype.|
|selectHostGroups|query|Return a [`hostgroups`](/manual/api/reference/hostgroup/object) property with the host groups that the graph prototype belongs to.|
|selectHosts|query|Return a [`hosts`](/manual/api/reference/host/object) property with the hosts that the graph prototype belongs to.|
|selectItems|query|Return an `items` property with the [items](/manual/api/reference/item/object) and [item prototypes](/manual/api/reference/itemprototype/object) used in the graph prototype.|
|selectTemplateGroups|query|Return a [`templategroups`](/manual/api/reference/templategroup/object) property with the template groups that the graph prototype belongs to.|
|selectTemplates|query|Return a [`templates`](/manual/api/reference/template/object) property with the templates that the graph prototype belongs to.|
|filter|object|Return only those results that exactly match the given filter.<br><br>Accepts an array, where the keys are property names, and the values are either a single value or an array of values to match against.<br><br>Supports additional filters:<br>`host` - technical name of the host that the graph prototype belongs to;<br>`hostid` - ID of the host that the graph prototype belongs to.|
|sortfield|string/array|Sort the result by the given properties.<br><br>Possible values: `graphid`, `name`, `graphtype`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters).|
|editable|boolean|^|
|excludeSearch|boolean|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|
|selectGroups<br>(deprecated)|query|This parameter is deprecated, please use `selectHostGroups` or `selectTemplateGroups` instead.<br>Return a `groups` property with the host groups and template groups that the graph prototype belongs to.|

[comment]: # ({/80cb99da-de0f74af})

[comment]: # ({7223bab1-7223bab1})
### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.

[comment]: # ({/7223bab1-7223bab1})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({fa3cd0a8-62edff03})
#### Retrieving graph prototypes from a LLD rule

Retrieve all graph prototypes from an LLD rule.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "graphprototype.get",
    "params": {
        "output": "extend",
        "discoveryids": "27426"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": [
        {
            "graphid": "1017",
            "parent_itemid": "27426",
            "name": "Disk space usage {#FSNAME}",
            "width": "600",
            "height": "340",
            "yaxismin": "0.0000",
            "yaxismax": "0.0000",
            "templateid": "442",
            "show_work_period": "0",
            "show_triggers": "0",
            "graphtype": "2",
            "show_legend": "1",
            "show_3d": "1",
            "percent_left": "0.0000",
            "percent_right": "0.0000",
            "ymin_type": "0",
            "ymax_type": "0",
            "ymin_itemid": "0",
            "ymax_itemid": "0",
            "discover": "0"
        }
    ],
    "id": 1
}
```

[comment]: # ({/fa3cd0a8-62edff03})

[comment]: # ({e4f07557-e4f96e80})
### See also

-   [Discovery
    rule](/manual/api/reference/discoveryrule/object#discovery_rule)
-   [Graph item](/manual/api/reference/graphitem/object#graph_item)
-   [Item](/manual/api/reference/item/object#item)
-   [Host](/manual/api/reference/host/object#host)
-   [Host group](/manual/api/reference/hostgroup/object#host_group)
-   [Template](/manual/api/reference/template/object#template)
-   [Template group](/manual/api/reference/templategroup/object#template_group)


[comment]: # ({/e4f07557-e4f96e80})

[comment]: # ({fc80fda8-fc80fda8})
### Source

CGraphPrototype::get() in
*ui/include/classes/api/services/CGraphPrototype.php*.

[comment]: # ({/fc80fda8-fc80fda8})
