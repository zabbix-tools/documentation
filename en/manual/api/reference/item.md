[comment]: # ({bae2348b-3816cd64})
# Item

This class is designed to work with items.

Object references:

-   [Item](/manual/api/reference/item/object#item)
-   [Item tag](/manual/api/reference/item/object#item_tag)
-   [Item preprocessing](/manual/api/reference/item/object#item_preprocessing)

Available methods:

-   [item.create](/manual/api/reference/item/create) - create new items
-   [item.delete](/manual/api/reference/item/delete) - delete items
-   [item.get](/manual/api/reference/item/get) - retrieve items
-   [item.update](/manual/api/reference/item/update) - update items

[comment]: # ({/bae2348b-3816cd64})
