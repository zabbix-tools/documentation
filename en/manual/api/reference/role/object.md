[comment]: # ({2135842b-2135842b})
# > Role object

The following objects are directly related to the `role` API.

[comment]: # ({/2135842b-2135842b})

[comment]: # ({84851cb3-1708e020})
### Role

The role object has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|roleid|string|ID of the role.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*<br>- *required* for update operations|
|name|string|Name of the role.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* for create operations|
|type|integer|User type.<br><br>Possible values:<br>1 - *(default)* User;<br>2 - Admin;<br>3 - Super admin.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* for create operations|
|readonly|integer|Whether the role is readonly.<br><br>Possible values:<br>0 - *(default)* No;<br>1 - Yes.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*|

[comment]: # ({/84851cb3-1708e020})

[comment]: # ({a9f09416-8a9b3008})
### Role rules

The role rules object has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|ui|array|Array of the [UI element](object#ui_element) objects.|
|ui.default\_access|integer|Whether access to new UI elements is enabled.<br><br>Possible values:<br>0 - Disabled;<br>1 - *(default)* Enabled.|
|services.read.mode|integer|Read-only access to services.<br><br>Possible values:<br>0 - Read-only access to the services, specified by the `services.read.list` or matched by the `services.read.tag` properties;<br>1 - *(default)* Read-only access to all services.|
|services.read.list|array|Array of [Service](object#service) objects.<br><br>The specified services, including child services, will be granted a read-only access to the user role. Read-only access will not override read-write access to the services.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `services.read.mode` is set to "0"|
|services.read.tag|object|Array of [Service tag](object#service_tag) object.<br><br>The tag matched services, including child services, will be granted a read-only access to the user role. Read-only access will not override read-write access to the services.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `services.read.mode` is set to "0"|
|services.write.mode|integer|Read-write access to services.<br><br>Possible values:<br>0 - *(default)* Read-write access to the services, specified by the `services.write.list` or matched by the `services.write.tag` properties;<br>1 - Read-write access to all services.|
|services.write.list|array|Array of [Service](object#service) objects.<br><br>The specified services, including child services, will be granted a read-write access to the user role. Read-write access will override read-only access to the services.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `services.write.mode` is set to "0"|
|services.write.tag|object|Array of [Service tag](object#service_tag) object.<br><br>The tag matched services, including child services, will be granted a read-write access to the user role. Read-write access will override read-only access to the services.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `services.write.mode` is set to "0"|
|modules|array|Array of the [module](object#module) objects.|
|modules.default\_access|integer|Whether access to new modules is enabled.<br><br>Possible values:<br>0 - Disabled;<br>1 - *(default)* Enabled.|
|api.access|integer|Whether access to API is enabled.<br><br>Possible values:<br>0 - Disabled;<br>1 - *(default)* Enabled.|
|api.mode|integer|Mode for treating API methods listed in the `api` property.<br><br>Possible values:<br>0 - *(default)* Deny list;<br>1 - Allow list.|
|api|array|Array of API methods.|
|actions|array|Array of the [action](object#action) objects.|
|actions.default\_access|integer|Whether access to new actions is enabled.<br><br>Possible values:<br>0 - Disabled;<br>1 - *(default)* Enabled.|

[comment]: # ({/a9f09416-8a9b3008})

[comment]: # ({4375b5d1-006cdfb5})
### UI element

The UI element object has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|name|string|Name of the UI element.<br><br>Possible values if `type` of [Role object](#role) is set to "User", "Admin", or "Super admin":<br>`monitoring.dashboard` - *Dashboards*;<br>`monitoring.problems` - *Monitoring → Problems*;<br>`monitoring.hosts` - *Monitoring → Hosts*;<br>`monitoring.latest_data` - *Monitoring → Latest data*;<br>`monitoring.maps` - *Monitoring → Maps*;<br>`services.services` - *Services → Services*;<br>`services.sla_report` - *Services → SLA report*;<br>`inventory.overview` - *Inventory → Overview*;<br>`inventory.hosts` - *Inventory → Hosts*;<br>`reports.availability_report` - *Reports → Availability report*;<br>`reports.top_triggers` - *Reports → Triggers top 100*.<br><br>Possible values if `type` of [Role object](#role) is set to "Admin" or "Super admin":<br>`monitoring.discovery` - *Monitoring → Discovery*;<br>`services.sla` - *Services → SLA*;<br>`reports.scheduled_reports` - *Reports → Scheduled reports*;<br>`reports.notifications` - *Reports → Notifications*;<br>`configuration.template_groups` - *Data collection → Template groups*;<br>`configuration.host_groups` - *Data collection → Host groups*;<br>`configuration.templates` - *Data collection → Templates*;<br>`configuration.hosts` - *Data collection → Hosts*;<br>`configuration.maintenance` - *Data collection → Maintenance*;<br>`configuration.discovery` - *Data collection → Discovery*;<br>`configuration.trigger_actions` - *Alerts → Actions → Trigger actions*;<br>`configuration.service_actions` - *Alerts → Actions → Service actions*;<br>`configuration.discovery_actions` - *Alerts → Actions → Discovery actions*;<br>`configuration.autoregistration_actions` - *Alerts → Actions → Autoregistration actions*;<br>`configuration.internal_actions` - *Alerts → Actions → Internal actions*.<br><br>Possible values if `type` of [Role object](#role) is set to "Super admin":<br>`reports.system_info` - *Reports → System information*;<br>`reports.audit` - *Reports → Audit log*;<br>`reports.action_log` - *Reports → Action log*;<br>`configuration.event_correlation` - *Data collection → Event correlation*;<br>`administration.media_types` - *Alerts → Media types*;<br>`administration.scripts` - *Alerts → Scripts*;<br>`administration.user_groups` - *Users → User groups*;<br>`administration.user_roles` - *Users → User roles*;<br>`administration.users` - *Users → Users*;<br>`administration.api_tokens` - *Users → API tokens*;<br>`administration.authentication` - *Users → Authentication*;<br>`administration.general` - *Administration → General*;<br>`administration.audit_log` - *Administration → Audit log*;<br>`administration.housekeeping` - *Administration → Housekeeping*;<br>`administration.proxies` - *Administration → Proxies*;<br>`administration.macros` - *Administration → Macros*;<br>`administration.queue` - *Administration → Queue*.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|status|integer|Whether access to the UI element is enabled.<br><br>Possible values:<br>0 - Disabled;<br>1 - *(default)* Enabled.|

[comment]: # ({/4375b5d1-006cdfb5})

[comment]: # ({b8603f6e-ebf4bb3a})
### Service

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|serviceid|string|ID of the Service.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|

[comment]: # ({/b8603f6e-ebf4bb3a})

[comment]: # ({1e4d9d21-300e6d5b})
### Service tag

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|tag|string|Tag name.<br><br>If empty string is specified, the service tag will not be used for service matching.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|value|string|Tag value.<br><br>If no value or empty string is specified, only the tag name will be used for service matching.|

[comment]: # ({/1e4d9d21-300e6d5b})

[comment]: # ({21d1ba7b-f70e75e9})
### Module

The module object has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|moduleid|string|ID of the module.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|status|integer|Whether access to the module is enabled.<br><br>Possible values:<br>0 - Disabled;<br>1 - *(default)* Enabled.|

[comment]: # ({/21d1ba7b-f70e75e9})

[comment]: # ({cd7f3b4c-957a00bb})
### Action

The action object has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|name|string|Name of the action.<br><br>Possible values if `type` of [Role object](#role) is set to "User", "Admin", or "Super admin:<br>`edit_dashboards` - Create and edit dashboards;<br>`edit_maps` - Create and edit maps;<br>`add_problem_comments` - Add problem comments;<br>`change_severity` - Change problem severity;<br>`acknowledge_problems` - Acknowledge problems;<br>`suppress_problems` - Suppress problems;<br>`close_problems` - Close problems;<br>`execute_scripts` - Execute scripts;<br>`manage_api_tokens` - Manage API tokens.<br><br>Possible values if `type` of [Role object](#role) is set to "Admin" or "Super admin":<br>`edit_maintenance` - Create and edit maintenances;<br>`manage_scheduled_reports` - Manage scheduled reports,<br>`manage_sla` - Manage SLA.<br><br>Possible values if `type` of [Role object](#role) is set to "User" or "Admin":<br>`invoke_execute_now` - allows to execute item checks for users that have only read permissions on host.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|status|integer|Whether access to perform the action is enabled.<br><br>Possible values:<br>0 - Disabled;<br>1 - *(default)* Enabled.|

[comment]: # ({/cd7f3b4c-957a00bb})
