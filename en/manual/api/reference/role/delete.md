[comment]: # ({cc40eabf-cc40eabf})
# role.delete

[comment]: # ({/cc40eabf-cc40eabf})

[comment]: # ({55e0b409-c4fe1cb7})
### Description

`object role.delete(array roleids)`

This method allows to delete roles.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/55e0b409-c4fe1cb7})

[comment]: # ({3eacfbc2-3eacfbc2})
### Parameters

`(array)` IDs of the roles to delete.

[comment]: # ({/3eacfbc2-3eacfbc2})

[comment]: # ({ba69c118-ba69c118})
### Return values

`(object)` Returns an object containing the IDs of the deleted roles
under the `roleids` property.

[comment]: # ({/ba69c118-ba69c118})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({ff0acc64-4fc52493})
#### Deleting multiple user roles

Delete two user roles.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "role.delete",
    "params": [
        "4",
        "5"
    ],
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "roleids": [
            "4",
            "5"
        ]
    },
    "id": 1
}
```

[comment]: # ({/ff0acc64-4fc52493})

[comment]: # ({32808752-32808752})
### Source

CRole::delete() in *ui/include/classes/api/services/CRole.php*.

[comment]: # ({/32808752-32808752})
