[comment]: # ({619023b5-619023b5})
# role.get

[comment]: # ({/619023b5-619023b5})

[comment]: # ({082b3945-ed6f4d70})
### Description

`integer/array role.get(object parameters)`

The method allows to retrieve roles according to the given parameters.

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/082b3945-ed6f4d70})

[comment]: # ({c82b9ef8-3b10ad42})
### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|roleids|string/array|Return only roles with the given IDs.|
|selectRules|query|Return a [`rules`](/manual/api/reference/role/object#role_rules) property with the role rules.|
|selectUsers|query|Return a [`users`](/manual/api/reference/user/object) property with the users that the role is assigned to.|
|sortfield|string/array|Sort the result by the given properties.<br><br>Possible values: `roleid`, `name`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters) page.|
|editable|boolean|^|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

[comment]: # ({/c82b9ef8-3b10ad42})

[comment]: # ({7223bab1-7223bab1})
### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.

[comment]: # ({/7223bab1-7223bab1})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({6eca0009-ac3322b8})
#### Retrieving role data

Retrieve "Super admin role" role data and its access rules.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "role.get",
    "params": {
        "output": "extend",
        "selectRules": "extend",
        "roleids": "3"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": [
        {
            "roleid": "3",
            "name": "Super admin role",
            "type": "3",
            "readonly": "1",
            "rules": {
                "ui": [
                    {
                        "name": "monitoring.dashboard",
                        "status": "1"
                    },
                    {
                        "name": "monitoring.problems",
                        "status": "1"
                    },
                    {
                        "name": "monitoring.hosts",
                        "status": "1"
                    },
                    {
                        "name": "monitoring.latest_data",
                        "status": "1"
                    },
                    {
                        "name": "monitoring.maps",
                        "status": "1"
                    },
                    {
                        "name": "services.services",
                        "status": "1"
                    },
                    {
                        "name": "services.sla_report",
                        "status": "1"
                    },
                    {
                        "name": "inventory.overview",
                        "status": "1"
                    },
                    {
                        "name": "inventory.hosts",
                        "status": "1"
                    },
                    {
                        "name": "reports.availability_report",
                        "status": "1"
                    },
                    {
                        "name": "reports.top_triggers",
                        "status": "1"
                    },
                    {
                        "name": "monitoring.discovery",
                        "status": "1"
                    },
                    {
                        "name": "services.sla",
                        "status": "1"
                    },
                    {
                        "name": "reports.scheduled_reports",
                        "status": "1"
                    },
                    {
                        "name": "reports.notifications",
                        "status": "1"
                    },
                    {
                        "name": "configuration.template_groups",
                        "status": "1"
                    },
                    {
                        "name": "configuration.host_groups",
                        "status": "1"
                    },
                    {
                        "name": "configuration.templates",
                        "status": "1"
                    },
                    {
                        "name": "configuration.hosts",
                        "status": "1"
                    },
                    {
                        "name": "configuration.maintenance",
                        "status": "1"
                    },
                    {
                        "name": "configuration.discovery",
                        "status": "1"
                    },
                    {
                        "name": "configuration.trigger_actions",
                        "status": "1"
                    },
                    {
                        "name": "configuration.service_actions",
                        "status": "1"
                    },
                    {
                        "name": "configuration.discovery_actions",
                        "status": "1"
                    },
                    {
                        "name": "configuration.autoregistration_actions",
                        "status": "1"
                    },
                    {
                        "name": "configuration.internal_actions",
                        "status": "1"
                    },
                    {
                        "name": "reports.system_info",
                        "status": "1"
                    },
                    {
                        "name": "reports.audit",
                        "status": "1"
                    },
                    {
                        "name": "reports.action_log",
                        "status": "1"
                    },
                    {
                        "name": "configuration.event_correlation",
                        "status": "1"
                    },
                    {
                        "name": "administration.media_types",
                        "status": "1"
                    },
                    {
                        "name": "administration.scripts",
                        "status": "1"
                    },
                    {
                        "name": "administration.user_groups",
                        "status": "1"
                    },
                    {
                        "name": "administration.user_roles",
                        "status": "1"
                    },
                    {
                        "name": "administration.users",
                        "status": "1"
                    },
                    {
                        "name": "administration.api_tokens",
                        "status": "1"
                    },
                    {
                        "name": "administration.authentication",
                        "status": "1"
                    },
                    {
                        "name": "administration.general",
                        "status": "1"
                    },
                    {
                        "name": "administration.audit_log",
                        "status": "1"
                    },
                    {
                        "name": "administration.housekeeping",
                        "status": "1"
                    },
                    {
                        "name": "administration.proxies",
                        "status": "1"
                    },
                    {
                        "name": "administration.macros",
                        "status": "1"
                    },
                    {
                        "name": "administration.queue",
                        "status": "1"
                    }
                ],
                "ui.default_access": "1",
                "services.read.mode": "1",
                "services.read.list": [],
                "services.read.tag": {
                    "tag": "",
                    "value": ""
                },
                "services.write.mode": "1",
                "services.write.list": [],
                "services.write.tag": {
                    "tag": "",
                    "value": ""
                },
                "modules": [],
                "modules.default_access": "1",
                "api.access": "1",
                "api.mode": "0",
                "api": [],
                "actions": [
                    {
                        "name": "edit_dashboards",
                        "status": "1"
                    },
                    {
                        "name": "edit_maps",
                        "status": "1"
                    },
                    {
                        "name": "acknowledge_problems",
                        "status": "1"
                    },
                    {
                        "name": "suppress_problems",
                        "status": "1"
                    },
                    {
                        "name": "close_problems",
                        "status": "1"
                    },
                    {
                        "name": "change_severity",
                        "status": "1"
                    },
                    {
                        "name": "add_problem_comments",
                        "status": "1"
                    },
                    {
                        "name": "execute_scripts",
                        "status": "1"
                    },
                    {
                        "name": "manage_api_tokens",
                        "status": "1"
                    },
                    {
                        "name": "edit_maintenance",
                        "status": "1"
                    },
                    {
                        "name": "manage_scheduled_reports",
                        "status": "1"
                    },
                    {
                        "name": "manage_sla",
                        "status": "1"
                    },
                    {
                        "name": "invoke_execute_now",
                        "status": "1"
                    }
                ],
                "actions.default_access": "1"
            }
        }
    ],
    "id": 1
}
```

[comment]: # ({/6eca0009-ac3322b8})

[comment]: # ({f3933c89-f3933c89})
### See also

-   [Role rules](/manual/api/reference/role/object#role_rules)
-   [User](/manual/api/reference/user/object#user)

[comment]: # ({/f3933c89-f3933c89})

[comment]: # ({780eeb68-780eeb68})
### Source

CRole::get() in *ui/include/classes/api/services/CRole.php*.

[comment]: # ({/780eeb68-780eeb68})
