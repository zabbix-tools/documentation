[comment]: # ({312e3a40-312e3a40})
# role.create

[comment]: # ({/312e3a40-312e3a40})

[comment]: # ({58ce01b8-17ca7358})
### Description

`object role.create(object/array roles)`

This method allows to create new roles.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/58ce01b8-17ca7358})

[comment]: # ({c2f5deb8-be3f23e2})
### Parameters

`(object/array)` Roles to create.

Additionally to the [standard role properties](object#role), the method
accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|rules|array|Role [rules](object#role_rules) to be created for the role.|

[comment]: # ({/c2f5deb8-be3f23e2})

[comment]: # ({f343e9f2-f343e9f2})
### Return values

`(object)` Returns an object containing the IDs of the created roles
under the `roleids` property. The order of the returned IDs matches the
order of the passed roles.

[comment]: # ({/f343e9f2-f343e9f2})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({060c032d-7e61457f})
#### Creating a role

Create a role with type "User" and denied access to two UI elements.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "role.create",
    "params": {
        "name": "Operator",
        "type": "1",
        "rules": {
            "ui": [
                {
                    "name": "monitoring.hosts",
                    "status": "0"
                },
                {
                    "name": "monitoring.maps",
                    "status": "0"
                }
            ]
        }
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "roleids": [
            "5"
        ]
    },
    "id": 1
}
```

[comment]: # ({/060c032d-7e61457f})

[comment]: # ({c3d5b904-c3d5b904})
### See also

-   [Role rules](object#role_rules)
-   [UI element](object#ui_element)
-   [Module](object#module)
-   [Action](object#action)

[comment]: # ({/c3d5b904-c3d5b904})

[comment]: # ({4b30d86c-4b30d86c})
### Source

CRole::create() in *ui/include/classes/api/services/CRole.php*.

[comment]: # ({/4b30d86c-4b30d86c})
