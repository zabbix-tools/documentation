[comment]: # ({8c618db1-8c618db1})
# template.massadd

[comment]: # ({/8c618db1-8c618db1})

[comment]: # ({644f677f-399b9834})
### Description

`object template.massadd(object parameters)`

This method allows to simultaneously add multiple related objects to the
given templates.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/644f677f-399b9834})

[comment]: # ({f9698554-4167b841})
### Parameters

`(object)` Parameters containing the IDs of the templates to update and
the objects to add to the templates.

The method accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|templates|object/array|Templates to be updated.<br><br>The templates must have the `templateid` property defined.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required*|
|groups|object/array|[Template groups](/manual/api/reference/templategroup/object) to add the given templates to.<br><br>The template groups must have the `groupid` property defined.|
|macros|object/array|[User macros](/manual/api/reference/usermacro/object) to be created for the given templates.|
|templates\_link|object/array|Templates to link to the given templates.<br><br>The templates must have the `templateid` property defined.|

[comment]: # ({/f9698554-4167b841})

[comment]: # ({dcba01c8-dcba01c8})
### Return values

`(object)` Returns an object containing the IDs of the updated templates
under the `templateids` property.

[comment]: # ({/dcba01c8-dcba01c8})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({9ee15989-f8fe44e4})
#### Link a group to templates

Add template group "2" to two templates.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "template.massadd",
    "params": {
        "templates": [
            {
                "templateid": "10085"
            },
            {
                "templateid": "10086"
            }
        ],
        "groups": [
            {
                "groupid": "2"
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "templateids": [
            "10085",
            "10086"
        ]
    },
    "id": 1
}
```

[comment]: # ({/9ee15989-f8fe44e4})

[comment]: # ({87e8784f-7dbed7e5})
#### Link two templates to a template

Link templates "10106" and "10104" to template "10073".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "template.massadd",
    "params": {
        "templates": [
            {
                "templateid": "10073"
            }
        ],
        "templates_link": [
            {
                "templateid": "10106"
            },
            {
                "templateid": "10104"
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "templateids": [
            "10073"
        ]
    },
    "id": 1
}
```

[comment]: # ({/87e8784f-7dbed7e5})

[comment]: # ({ddd17552-bd8dd370})
### See also

-   [template.update](update)
-   [Host](/manual/api/reference/host/object#host)
-   [Template group](/manual/api/reference/templategroup/object#template_group)
-   [User macro](/manual/api/reference/usermacro/object#hosttemplate_level_macro)

[comment]: # ({/ddd17552-bd8dd370})

[comment]: # ({2f7abcb1-2f7abcb1})
### Source

CTemplate::massAdd() in *ui/include/classes/api/services/CTemplate.php*.

[comment]: # ({/2f7abcb1-2f7abcb1})
