[comment]: # ({e42e8a0a-e42e8a0a})
# template.massupdate

[comment]: # ({/e42e8a0a-e42e8a0a})

[comment]: # ({07d18cd5-cf27d8f8})
### Description

`object template.massupdate(object parameters)`

This method allows to simultaneously replace or remove related objects
and update properties on multiple templates.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/07d18cd5-cf27d8f8})

[comment]: # ({7bdcae3e-1f7d5759})
### Parameters

`(object)` Parameters containing the IDs of the templates to update and the objects to replace for the templates.

The method accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|templates|object/array|[Templates](/manual/api/reference/template/object) to be updated.<br><br>The templates must have the `templateid` property defined.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required*|
|groups|object/array|[Template groups](/manual/api/reference/templategroup/object) to replace the current template groups the templates belong to.<br><br>The template groups must have the `groupid` property defined.|
|macros|object/array|[User macros](/manual/api/reference/usermacro/object) to replace all of the current user macros on the given templates.|
|templates\_clear|object/array|Templates to unlink and clear from the given templates.<br><br>The templates must have the `templateid` property defined.|
|templates\_link|object/array|Templates to replace the currently linked templates.<br><br>The templates must have the `templateid` property defined.|

[comment]: # ({/7bdcae3e-1f7d5759})

[comment]: # ({dcba01c8-dcba01c8})
### Return values

`(object)` Returns an object containing the IDs of the updated templates
under the `templateids` property.

[comment]: # ({/dcba01c8-dcba01c8})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({089f8542-d18b091c})
#### Unlinking a template

Unlink and clear template "10091" from the given templates.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "template.massupdate",
    "params": {
        "templates": [
            {
                "templateid": "10085"
            },
            {
                "templateid": "10086"
            }
        ],
        "templates_clear": [
            {
                "templateid": "10091"
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "templateids": [
            "10085",
            "10086"
        ]
    },
    "id": 1
}
```

[comment]: # ({/089f8542-d18b091c})

[comment]: # ({eec18141-0ecefb75})
#### Replacing user macros

Replace all user macros with the given user macro on multiple templates.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "template.massupdate",
    "params": {
        "templates": [
            {
                "templateid": "10074"
            },
            {
                "templateid": "10075"
            },
            {
                "templateid": "10076"
            },
            {
                "templateid": "10077"
            }
        ],
        "macros": [
            {
                "macro": "{$AGENT.TIMEOUT}",
                "value": "5m",
                "description": "Timeout after which agent is considered unavailable. Works only for agents reachable from Zabbix server/proxy (passive mode)."
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "templateids": [
            "10074",
            "10075",
            "10076",
            "10077"
        ]
    },
    "id": 1
}
```

[comment]: # ({/eec18141-0ecefb75})

[comment]: # ({865fb704-5d283e35})
### See also

-   [template.update](update)
-   [template.massadd](massadd)
-   [Template group](/manual/api/reference/templategroup/object#template_group)
-   [User macro](/manual/api/reference/usermacro/object#hosttemplate_level_macro)

[comment]: # ({/865fb704-5d283e35})

[comment]: # ({b5f8dcd9-b5f8dcd9})
### Source

CTemplate::massUpdate() in
*ui/include/classes/api/services/CTemplate.php*.

[comment]: # ({/b5f8dcd9-b5f8dcd9})
