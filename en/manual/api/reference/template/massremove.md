[comment]: # ({ae0db7b8-ae0db7b8})
# template.massremove

[comment]: # ({/ae0db7b8-ae0db7b8})

[comment]: # ({6ed93d40-ba911402})
### Description

`object template.massremove(object parameters)`

This method allows to remove related objects from multiple templates.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/6ed93d40-ba911402})

[comment]: # ({d6830d63-bd7f8b44})
### Parameters

`(object)` Parameters containing the IDs of the templates to update and
the objects that should be removed.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|templateids|string/array|IDs of the templates to be updated.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required*|
|groupids|string/array|[Template groups](/manual/api/reference/templategroup/object) from which to remove the given templates.|
|macros|string/array|[User macros](/manual/api/reference/usermacro/object) to delete from the given templates.|
|templateids\_clear|string/array|Templates to unlink and clear from the given templates (upstream).|
|templateids\_link|string/array|Templates to unlink from the given templates (upstream).|

[comment]: # ({/d6830d63-bd7f8b44})

[comment]: # ({dcba01c8-dcba01c8})
### Return values

`(object)` Returns an object containing the IDs of the updated templates
under the `templateids` property.

[comment]: # ({/dcba01c8-dcba01c8})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({eb3f1552-20c1ddc2})
#### Removing templates from a group

Remove two templates from group "2".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "template.massremove",
    "params": {
        "templateids": [
            "10085",
            "10086"
        ],
        "groupids": "2"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "templateids": [
            "10085",
            "10086"
        ]
    },
    "id": 1
}
```

[comment]: # ({/eb3f1552-20c1ddc2})

[comment]: # ({77ff616d-6459bc33})
#### Unlinking templates from a host

Unlink templates "10106" and "10104" from template "10085".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "template.massremove",
    "params": {
        "templateids": "10085",
        "templateids_link": [
            "10106",
            "10104"
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "templateids": [
            "10085"
        ]
    },
    "id": 1
}
```

[comment]: # ({/77ff616d-6459bc33})

[comment]: # ({186aec66-186aec66})
### See also

-   [template.update](update)
-   [User macro](/manual/api/reference/usermacro/object#hosttemplate_level_macro)

[comment]: # ({/186aec66-186aec66})

[comment]: # ({aba9c6f9-aba9c6f9})
### Source

CTemplate::massRemove() in
*ui/include/classes/api/services/CTemplate.php*.

[comment]: # ({/aba9c6f9-aba9c6f9})
