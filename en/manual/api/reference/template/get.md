[comment]: # ({896915aa-896915aa})
# template.get

[comment]: # ({/896915aa-896915aa})

[comment]: # ({808af69c-4e400711})
### Description

`integer/array template.get(object parameters)`

The method allows to retrieve templates according to the given
parameters.

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/808af69c-4e400711})

[comment]: # ({4b814db9-97f0e40b})
### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|templateids|string/array|Return only templates with the given template IDs.|
|groupids|string/array|Return only templates that belong to the given template groups.|
|parentTemplateids|string/array|Return only templates that the given template is linked to.|
|hostids|string/array|Return only templates that are linked to the given hosts/templates.|
|graphids|string/array|Return only templates that contain the given graphs.|
|itemids|string/array|Return only templates that contain the given items.|
|triggerids|string/array|Return only templates that contain the given triggers.|
|with\_items|flag|Return only templates that have items.|
|with\_triggers|flag|Return only templates that have triggers.|
|with\_graphs|flag|Return only templates that have graphs.|
|with\_httptests|flag|Return only templates that have web scenarios.|
|evaltype|integer|Rules for tag searching.<br><br>Possible values:<br>0 - (default) And/Or;<br>2 - Or.|
|tags|array/object|Return only templates with given tags. Exact match by tag and case-sensitive or case-insensitive search by tag value depending on operator value.<br>Format: `[{"tag": "<tag>", "value": "<value>", "operator": "<operator>"}, ...]`.<br>An empty array returns all templates.<br><br>Possible operator values:<br>0 - (default) Contains;<br>1 - Equals;<br>2 - Not like;<br>3 - Not equal;<br>4 - Exists;<br>5 - Not exists.|
|selectTags|query|Return template tags in the [`tags`](/manual/api/reference/template/object#template_tag) property.|
|selectHosts|query|Return the hosts that are linked to the template in the [`hosts`](/manual/api/reference/host/object) property.<br><br>Supports `count`.|
|selectTemplateGroups|query|Return the template groups that the template belongs to in the [`templategroups`](/manual/api/reference/templategroup/object) property.|
|selectTemplates|query|Return templates to which the given template is linked in the [`templates`](/manual/api/reference/template/object) property.<br><br>Supports `count`.|
|selectParentTemplates|query|Return templates that are linked to the given template in the `parentTemplates` property.<br><br>Supports `count`.|
|selectHttpTests|query|Return the web scenarios from the template in the [`httpTests`](/manual/api/reference/httptest/object) property.<br><br>Supports `count`.|
|selectItems|query|Return items from the template in the [`items`](/manual/api/reference/item/object) property.<br><br>Supports `count`.|
|selectDiscoveries|query|Return low-level discoveries from the template in the `discoveries` property.<br><br>Supports `count`.|
|selectTriggers|query|Return triggers from the template in the [`triggers`](/manual/api/reference/trigger/object) property.<br><br>Supports `count`.|
|selectGraphs|query|Return graphs from the template in the [`graphs`](/manual/api/reference/graph/object) property.<br><br>Supports `count`.|
|selectMacros|query|Return the macros from the template in the `macros` property.|
|selectDashboards|query|Return dashboards from the template in the [`dashboards`](/manual/api/reference/templatedashboard/object) property.<br><br>Supports `count`.|
|selectValueMaps|query|Return a [`valuemaps`](/manual/api/reference/valuemap/object) property with template value maps.|
|limitSelects|integer|Limits the number of records returned by subselects.<br><br>Applies to the following subselects:<br>`selectTemplates` - results will be sorted by `name`;<br>`selectHosts` - sorted by `host`;<br>`selectParentTemplates` - sorted by `host`;<br>`selectItems` - sorted by `name`;<br>`selectDiscoveries` - sorted by `name`;<br>`selectTriggers` - sorted by `description`;<br>`selectGraphs` - sorted by `name`;<br>`selectDashboards` - sorted by `name`.|
|sortfield|string/array|Sort the result by the given properties.<br><br>Possible values: `hostid`, `host`, `name`, `status`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters).|
|editable|boolean|^|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|
|selectGroups<br>(deprecated)|query|This parameter is deprecated, please use `selectTemplateGroups` instead.<br>Return the template groups that the template belongs to in the [`groups`](/manual/api/reference/templategroup/object) property.|

[comment]: # ({/4b814db9-97f0e40b})

[comment]: # ({7223bab1-7223bab1})
### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.

[comment]: # ({/7223bab1-7223bab1})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({2bd380bd-7e546e9a})
#### Retrieving templates by name

Retrieve all data about two templates named "Linux" and "Windows".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "template.get",
    "params": {
        "output": "extend",
        "filter": {
            "host": [
                "Linux",
                "Windows"
            ]
        }
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": [
        {
            "proxy_hostid": "0",
            "host": "Linux",
            "status": "3",
            "disable_until": "0",
            "error": "",
            "available": "0",
            "errors_from": "0",
            "lastaccess": "0",
            "ipmi_authtype": "0",
            "ipmi_privilege": "2",
            "ipmi_username": "",
            "ipmi_password": "",
            "ipmi_disable_until": "0",
            "ipmi_available": "0",
            "snmp_disable_until": "0",
            "snmp_available": "0",
            "maintenanceid": "0",
            "maintenance_status": "0",
            "maintenance_type": "0",
            "maintenance_from": "0",
            "ipmi_errors_from": "0",
            "snmp_errors_from": "0",
            "ipmi_error": "",
            "snmp_error": "",
            "jmx_disable_until": "0",
            "jmx_available": "0",
            "jmx_errors_from": "0",
            "jmx_error": "",
            "name": "Linux",
            "flags": "0",
            "templateid": "10001",
            "description": "",
            "tls_connect": "1",
            "tls_accept": "1",
            "tls_issuer": "",
            "tls_subject": "",
            "tls_psk_identity": "",
            "tls_psk": "",
            "uuid": "282ffe33afc74cccaf1524d9aa9dc502"
        },
        {
            "proxy_hostid": "0",
            "host": "Windows",
            "status": "3",
            "disable_until": "0",
            "error": "",
            "available": "0",
            "errors_from": "0",
            "lastaccess": "0",
            "ipmi_authtype": "0",
            "ipmi_privilege": "2",
            "ipmi_username": "",
            "ipmi_password": "",
            "ipmi_disable_until": "0",
            "ipmi_available": "0",
            "snmp_disable_until": "0",
            "snmp_available": "0",
            "maintenanceid": "0",
            "maintenance_status": "0",
            "maintenance_type": "0",
            "maintenance_from": "0",
            "ipmi_errors_from": "0",
            "snmp_errors_from": "0",
            "ipmi_error": "",
            "snmp_error": "",
            "jmx_disable_until": "0",
            "jmx_available": "0",
            "jmx_errors_from": "0",
            "jmx_error": "",
            "name": "Windows",
            "flags": "0",
            "templateid": "10081",
            "description": "",
            "tls_connect": "1",
            "tls_accept": "1",
            "tls_issuer": "",
            "tls_subject": "",
            "tls_psk_identity": "",
            "tls_psk": "",
            "uuid": "522d17e1834049be879287b7c0518e5d"
        }
    ],
    "id": 1
}
```

[comment]: # ({/2bd380bd-7e546e9a})

[comment]: # ({4e2a2df6-84d0cbf3})
#### Retrieving template groups

Retrieve template groups that the template "Linux by Zabbix agent" is a member of.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "template.get",
    "params": {
        "output": ["hostid"],
        "selectTemplateGroups": "extend",
        "filter": {
            "host": [
                "Linux by Zabbix agent"
            ]
        }
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": [
        {
            "templateid": "10001",
            "templategroups": [
                {
                    "groupid": "10",
                    "name": "Templates/Operating systems",
                    "uuid": "846977d1dfed4968bc5f8bdb363285bc"
                }
            ]
        }
    ],
    "id": 1
}
```

[comment]: # ({/4e2a2df6-84d0cbf3})

[comment]: # ({ef35791c-0fe2d603})
#### Searching by template tags

Retrieve templates that have tag "Host name" equal to "{HOST.NAME}".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "template.get",
    "params": {
        "output": ["hostid"],
        "selectTags": "extend",
        "evaltype": 0,
        "tags": [
            {
                "tag": "Host name",
                "value": "{HOST.NAME}",
                "operator": 1
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": [
        {
            "hostid": "10402",
            "tags": [
                {
                    "tag": "Host name",
                    "value": "{HOST.NAME}"
                }
            ]
        }
    ],
    "id": 1
}
```

[comment]: # ({/ef35791c-0fe2d603})

[comment]: # ({6e0a148e-ee3fc022})
### See also

-   [Template group](/manual/api/reference/templategroup/object#template_group)
-   [Template](object#template)
-   [User
    macro](/manual/api/reference/usermacro/object#hosttemplate_level_macro)
-   [Host
    interface](/manual/api/reference/hostinterface/object#host_interface)

[comment]: # ({/6e0a148e-ee3fc022})

[comment]: # ({4fdbde5d-4fdbde5d})
### Source

CTemplate::get() in *ui/include/classes/api/services/CTemplate.php*.

[comment]: # ({/4fdbde5d-4fdbde5d})
