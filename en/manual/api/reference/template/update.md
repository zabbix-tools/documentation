[comment]: # ({36f834f7-36f834f7})
# template.update

[comment]: # ({/36f834f7-36f834f7})

[comment]: # ({c65d8691-f8425904})
### Description

`object template.update(object/array templates)`

This method allows to update existing templates.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/c65d8691-f8425904})

[comment]: # ({14f97ddb-5d06cf34})
### Parameters

`(object/array)` Template properties to be updated.

The `templateid` property must be defined for each template, all other
properties are optional. Only the given properties will be updated, all
others will remain unchanged.

Additionally to the [standard template properties](object#template), the
method accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|groups|object/array|[Template groups](/manual/api/reference/templategroup/object) to replace the current template groups the templates belong to.<br><br>The template groups must have the `groupid` property defined.|
|tags|object/array|Template [tags](/manual/api/reference/template/object#template_tag) to replace the current template tags.|
|macros|object/array|[User macros](/manual/api/reference/usermacro/object) to replace the current user macros on the given templates.|
|templates|object/array|[Templates](/manual/api/reference/template/object) to replace the currently linked templates. Templates that are not passed are only unlinked.<br><br>The templates must have the `templateid` property defined.|
|templates\_clear|object/array|[Templates](/manual/api/reference/template/object) to unlink and clear from the given templates.<br><br>The templates must have the `templateid` property defined.|

[comment]: # ({/14f97ddb-5d06cf34})

[comment]: # ({dcba01c8-dcba01c8})
### Return values

`(object)` Returns an object containing the IDs of the updated templates
under the `templateids` property.

[comment]: # ({/dcba01c8-dcba01c8})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({973fc26f-0080b19f})
#### Renaming a template

Rename the template to "Template OS Linux".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "template.update",
    "params": {
        "templateid": "10086",
        "name": "Template OS Linux"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "templateids": [
            "10086"
        ]
    },
    "id": 1
}
```

[comment]: # ({/973fc26f-0080b19f})

[comment]: # ({6329c800-e7972b69})
#### Updating template tags

Replace all template tags with a new one.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "template.update",
    "params": {
        "templateid": "10086",
        "tags": [
            {
                "tag": "Host name",
                "value": "{HOST.NAME}"
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "templateids": [
            "10086"
        ]
    },
    "id": 1
}
```

[comment]: # ({/6329c800-e7972b69})

[comment]: # ({eb72cc1c-eb72cc1c})
### Source

CTemplate::update() in *ui/include/classes/api/services/CTemplate.php*.

[comment]: # ({/eb72cc1c-eb72cc1c})
