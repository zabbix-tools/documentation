[comment]: # ({ac6966b9-ac6966b9})
# > Template object

The following objects are directly related to the `template` API.

[comment]: # ({/ac6966b9-ac6966b9})

[comment]: # ({8be76a77-ccfaad06})
### Template

The template object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|templateid|string|ID of the template.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*<br>- *required* for update operations|
|host|string|Technical name of the template.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* for create operations|
|description|text|Description of the template.|
|name|string|Visible name of the template.<br><br>Default: `host` property value.|
|uuid|string|Universal unique identifier, used for linking imported templates to already existing ones. Auto-generated, if not given.|
|vendor_name|string|Template vendor name.<br><br>For create operations, both `vendor_name` and `vendor_version` should be either set or left empty. For update operations, `vendor_version` can be left empty if it has a value in the database.|
|vendor_version|string|Template vendor version.<br><br>For create operations, both `vendor_name` and `vendor_version` should be either set or left empty. For update operations, `vendor_name` can be left empty if it has a value in the database.|

[comment]: # ({/8be76a77-ccfaad06})

[comment]: # ({c2b706cc-2248aef2})
### Template tag

The template tag object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|tag|string|Template tag name.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|value|string|Template tag value.|

[comment]: # ({/c2b706cc-2248aef2})
