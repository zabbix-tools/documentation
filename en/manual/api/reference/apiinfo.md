[comment]: # ({1a5ac452-7fceb632})
# API info

This class is designed to retrieve meta information about the API.

Available methods:

-   [apiinfo.version](/manual/api/reference/apiinfo/version) - retrieve the version of Zabbix API

[comment]: # ({/1a5ac452-7fceb632})
