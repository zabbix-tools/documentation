[comment]: # ({cdc53a5f-cdc53a5f})
# graph.get

[comment]: # ({/cdc53a5f-cdc53a5f})

[comment]: # ({b80d99e9-d2f8406d})
### Description

`integer/array graph.get(object parameters)`

The method allows to retrieve graphs according to the given parameters.

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/b80d99e9-d2f8406d})

[comment]: # ({284ebbeb-5b1c5b22})
### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|graphids|string/array|Return only graphs with the given IDs.|
|groupids|string/array|Return only graphs that belong to hosts or templates in the given host groups or template groups.|
|templateids|string/array|Return only graph that belong to the given templates.|
|hostids|string/array|Return only graphs that belong to the given hosts.|
|itemids|string/array|Return only graphs that contain the given items.|
|templated|boolean|If set to `true` return only graphs that belong to templates.|
|inherited|boolean|If set to `true` return only graphs inherited from a template.|
|expandName|flag|Expand macros in the graph name.|
|selectHostGroups|query|Return a [`hostgroups`](/manual/api/reference/hostgroup/object) property with the host groups that the graph belongs to.|
|selectTemplateGroups|query|Return a [`templategroups`](/manual/api/reference/templategroup/object) property with the template groups that the graph belongs to.|
|selectTemplates|query|Return a [`templates`](/manual/api/reference/template/object) property with the templates that the graph belongs to.|
|selectHosts|query|Return a [`hosts`](/manual/api/reference/host/object) property with the hosts that the graph belongs to.|
|selectItems|query|Return an [`items`](/manual/api/reference/item/object) property with the items used in the graph.|
|selectGraphDiscovery|query|Return a `graphDiscovery` property with the graph discovery object. The graph discovery objects links the graph to a graph prototype from which it was created.<br><br>It has the following properties:<br>`graphid` - `(string)` ID of the graph;<br>`parent_graphid` - `(string)` ID of the graph prototype from which the graph has been created.|
|selectGraphItems|query|Return a [`gitems`](/manual/api/reference/graphitem/object) property with the items used in the graph.|
|selectDiscoveryRule|query|Return a [`discoveryRule`](/manual/api/reference/drule/object) property with the low-level discovery rule that created the graph.|
|filter|object|Return only those results that exactly match the given filter.<br><br>Accepts an array, where the keys are property names, and the values are either a single value or an array of values to match against.<br><br>Supports additional filters:<br>`host` - technical name of the host that the graph belongs to;<br>`hostid` - ID of the host that the graph belongs to.|
|sortfield|string/array|Sort the result by the given properties.<br><br>Possible values: `graphid`, `name`, `graphtype`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters) page.|
|editable|boolean|^|
|excludeSearch|boolean|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|
|selectGroups<br> (deprecated)|query|This parameter is deprecated, please use `selectHostGroups` or `selectTemplateGroups` instead.<br> Return a `groups` property with the host groups and template groups that the graph belongs to.|

[comment]: # ({/284ebbeb-5b1c5b22})

[comment]: # ({7223bab1-7223bab1})
### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.

[comment]: # ({/7223bab1-7223bab1})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({d11e19d3-e7fae381})
#### Retrieving graphs from hosts

Retrieve all graphs from host "10107" and sort them by name.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "graph.get",
    "params": {
        "output": "extend",
        "hostids": 10107,
        "sortfield": "name"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": [
        {
            "graphid": "612",
            "name": "CPU jumps",
            "width": "900",
            "height": "200",
            "yaxismin": "0",
            "yaxismax": "100",
            "templateid": "439",
            "show_work_period": "1",
            "show_triggers": "1",
            "graphtype": "0",
            "show_legend": "1",
            "show_3d": "0",
            "percent_left": "0",
            "percent_right": "0",
            "ymin_type": "0",
            "ymax_type": "0",
            "ymin_itemid": "0",
            "ymax_itemid": "0",
            "flags": "0"
        },
        {
            "graphid": "613",
            "name": "CPU load",
            "width": "900",
            "height": "200",
            "yaxismin": "0",
            "yaxismax": "100",
            "templateid": "433",
            "show_work_period": "1",
            "show_triggers": "1",
            "graphtype": "0",
            "show_legend": "1",
            "show_3d": "0",
            "percent_left": "0",
            "percent_right": "0",
            "ymin_type": "1",
            "ymax_type": "0",
            "ymin_itemid": "0",
            "ymax_itemid": "0",
            "flags": "0"
        },
        {
            "graphid": "614",
            "name": "CPU utilization",
            "width": "900",
            "height": "200",
            "yaxismin": "0",
            "yaxismax": "100",
            "templateid": "387",
            "show_work_period": "1",
            "show_triggers": "0",
            "graphtype": "1",
            "show_legend": "1",
            "show_3d": "0",
            "percent_left": "0",
            "percent_right": "0",
            "ymin_type": "1",
            "ymax_type": "1",
            "ymin_itemid": "0",
            "ymax_itemid": "0",
            "flags": "0"
        },
        {
            "graphid": "645",
            "name": "Disk space usage /",
            "width": "600",
            "height": "340",
            "yaxismin": "0",
            "yaxismax": "0",
            "templateid": "0",
            "show_work_period": "0",
            "show_triggers": "0",
            "graphtype": "2",
            "show_legend": "1",
            "show_3d": "1",
            "percent_left": "0",
            "percent_right": "0",
            "ymin_type": "0",
            "ymax_type": "0",
            "ymin_itemid": "0",
            "ymax_itemid": "0",
            "flags": "4"
        }
    ],
    "id": 1
}
```

[comment]: # ({/d11e19d3-e7fae381})

[comment]: # ({e4f07557-e4f96e80})
### See also

-   [Discovery
    rule](/manual/api/reference/discoveryrule/object#discovery_rule)
-   [Graph item](/manual/api/reference/graphitem/object#graph_item)
-   [Item](/manual/api/reference/item/object#item)
-   [Host](/manual/api/reference/host/object#host)
-   [Host group](/manual/api/reference/hostgroup/object#host_group)
-   [Template](/manual/api/reference/template/object#template)
-   [Template group](/manual/api/reference/templategroup/object#template_group)

[comment]: # ({/e4f07557-e4f96e80})

[comment]: # ({6f7b2d0a-6f7b2d0a})
### Source

CGraph::get() in *ui/include/classes/api/services/CGraph.php*.

[comment]: # ({/6f7b2d0a-6f7b2d0a})
