[comment]: # ({d2516189-d2516189})
# graph.update

[comment]: # ({/d2516189-d2516189})

[comment]: # ({5971d417-992bc0de})
### Description

`object graph.update(object/array graphs)`

This method allows to update existing graphs.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/5971d417-992bc0de})

[comment]: # ({c695fc44-72c99624})
### Parameters

`(object/array)` Graph properties to be updated.

The `graphid` property must be defined for each graph, all other
properties are optional. Only the passed properties will be updated, all
others will remain unchanged.

Additionally to the [standard graph properties](object#graph) the method
accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|gitems|array|Graph [items](/manual/api/reference/graphitem/object) to replace existing graph items. If a graph item has the `gitemid` property defined it will be updated, otherwise a new graph item will be created.|

[comment]: # ({/c695fc44-72c99624})

[comment]: # ({9a0b4500-9a0b4500})
### Return values

`(object)` Returns an object containing the IDs of the updated graphs
under the `graphids` property.

[comment]: # ({/9a0b4500-9a0b4500})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({fdd1e9a6-7b33f5ce})
#### Setting the maximum for the Y scale

Set the maximum of the Y scale to a fixed value of 100.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "graph.update",
    "params": {
        "graphid": "439",
        "ymax_type": 1,
        "yaxismax": 100
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "graphids": [
            "439"
        ]
    },
    "id": 1
}
```

[comment]: # ({/fdd1e9a6-7b33f5ce})

[comment]: # ({84b1446b-84b1446b})
### Source

CGraph::update() in *ui/include/classes/api/services/CGraph.php*.

[comment]: # ({/84b1446b-84b1446b})
