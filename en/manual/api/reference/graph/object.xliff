<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="en" datatype="plaintext" original="manual/api/reference/graph/object.md">
    <body>
      <trans-unit id="c0099aca" xml:space="preserve">
        <source># &gt; Graph object

The following objects are directly related to the `graph` API.</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/api/reference/graph/object.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="9849a58f" xml:space="preserve">
        <source>### Graph

The graph object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|graphid|string|ID of the graph.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *read-only*&lt;br&gt;- *required* for update operations|
|height|integer|Height of the graph in pixels.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* for create operations|
|name|string|Name of the graph.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* for create operations|
|width|integer|Width of the graph in pixels.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* for create operations|
|flags|integer|Origin of the graph.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* a plain graph;&lt;br&gt;4 - a discovered graph.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *read-only*|
|graphtype|integer|Graph's layout type.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* normal;&lt;br&gt;1 - stacked;&lt;br&gt;2 - pie;&lt;br&gt;3 - exploded.|
|percent\_left|float|Left percentile.&lt;br&gt;&lt;br&gt;Default: 0.|
|percent\_right|float|Right percentile.&lt;br&gt;&lt;br&gt;Default: 0.|
|show\_3d|integer|Whether to show pie and exploded graphs in 3D.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* show in 2D;&lt;br&gt;1 - show in 3D.|
|show\_legend|integer|Whether to show the legend on the graph.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - hide;&lt;br&gt;1 - *(default)* show.|
|show\_work\_period|integer|Whether to show the working time on the graph.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - hide;&lt;br&gt;1 - *(default)* show.|
|show\_triggers|integer|Whether to show the trigger line on the graph.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - hide;&lt;br&gt;1 - *(default)* show.|
|templateid|string|ID of the parent template graph.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *read-only*|
|yaxismax|float|The fixed maximum value for the Y axis.&lt;br&gt;&lt;br&gt;Default: 100.|
|yaxismin|float|The fixed minimum value for the Y axis.&lt;br&gt;&lt;br&gt;Default: 0.|
|ymax\_itemid|string|ID of the item that is used as the maximum value for the Y axis.&lt;br&gt;&lt;br&gt;If a user has no access to the specified item, the graph is rendered as if `ymax_type` is set to "calculated".|
|ymax\_type|integer|Maximum value calculation method for the Y axis.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* calculated;&lt;br&gt;1 - fixed;&lt;br&gt;2 - item.|
|ymin\_itemid|string|ID of the item that is used as the minimum value for the Y axis.&lt;br&gt;&lt;br&gt;If a user has no access to the specified item, the graph is rendered as if `ymin_type` is set to "calculated".|
|ymin\_type|integer|Minimum value calculation method for the Y axis.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* calculated;&lt;br&gt;1 - fixed;&lt;br&gt;2 - item.|
|uuid|string|Universal unique identifier, used for linking imported graphs to already existing ones. Auto-generated, if not given.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if the graph belongs to a template|</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/api/reference/graph/object.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
    </body>
  </file>
</xliff>
