[comment]: # ({cf547db2-cf547db2})
# graph.delete

[comment]: # ({/cf547db2-cf547db2})

[comment]: # ({f46de7e0-c740de99})
### Description

`object graph.delete(array graphIds)`

This method allows to delete graphs.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/f46de7e0-c740de99})

[comment]: # ({54514063-54514063})
### Parameters

`(array)` IDs of the graphs to delete.

[comment]: # ({/54514063-54514063})

[comment]: # ({eb5c8bd8-eb5c8bd8})
### Return values

`(object)` Returns an object containing the IDs of the deleted graphs
under the `graphids` property.

[comment]: # ({/eb5c8bd8-eb5c8bd8})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({44ebaf66-3a78f2ec})
#### Deleting multiple graphs

Delete two graphs.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "graph.delete",
    "params": [
        "652",
        "653"
    ],
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "graphids": [
            "652",
            "653"
        ]
    },
    "id": 1
}
```

[comment]: # ({/44ebaf66-3a78f2ec})

[comment]: # ({e06af123-e06af123})
### Source

CGraph::delete() in *ui/include/classes/api/services/CGraph.php*.

[comment]: # ({/e06af123-e06af123})
