[comment]: # ({000fdd04-000fdd04})
# graph.create

[comment]: # ({/000fdd04-000fdd04})

[comment]: # ({c0a9ce2e-e0ee1e5d})
### Description

`object graph.create(object/array graphs)`

This method allows to create new graphs.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/c0a9ce2e-e0ee1e5d})

[comment]: # ({6c10f6f3-fc8b02f4})
### Parameters

`(object/array)` Graphs to create.

Additionally to the [standard graph properties](object#graph), the
method accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|gitems|array|Graph [items](/manual/api/reference/graphitem/object) to be created for the graph.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required*|

[comment]: # ({/6c10f6f3-fc8b02f4})

[comment]: # ({d64b873d-d64b873d})
### Return values

`(object)` Returns an object containing the IDs of the created graphs
under the `graphids` property. The order of the returned IDs matches the
order of the passed graphs.

[comment]: # ({/d64b873d-d64b873d})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({a5d4a0e3-cd12048c})
#### Creating a graph

Create a graph with two items.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "graph.create",
    "params": {
        "name": "MySQL bandwidth",
        "width": 900,
        "height": 200,
        "gitems": [
            {
                "itemid": "22828",
                "color": "00AA00"
            },
            {
                "itemid": "22829",
                "color": "3333FF"
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "graphids": [
            "652"
        ]
    },
    "id": 1
}
```

[comment]: # ({/a5d4a0e3-cd12048c})

[comment]: # ({88266563-88266563})
### See also

-   [Graph item](/manual/api/reference/graphitem/object#graph_item)

[comment]: # ({/88266563-88266563})

[comment]: # ({4e535c8e-4e535c8e})
### Source

CGraph::create() in *ui/include/classes/api/services/CGraph.php*.

[comment]: # ({/4e535c8e-4e535c8e})
