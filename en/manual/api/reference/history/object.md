[comment]: # ({57ba3541-57ba3541})
# > History object

The following objects are directly related to the `history` API.

::: noteclassic
History objects differ depending on the item's type of
information. They are created by the Zabbix server and cannot be
modified via the API.
:::

[comment]: # ({/57ba3541-57ba3541})

[comment]: # ({525f12cf-399f1014})
### Float history

The float history object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|clock|timestamp|Time when that value was received.|
|itemid|string|ID of the related item.|
|ns|integer|Nanoseconds when the value was received.|
|value|float|Received value.|

[comment]: # ({/525f12cf-399f1014})

[comment]: # ({47696f1e-3e505499})
### Integer history

The integer history object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|clock|timestamp|Time when that value was received.|
|itemid|string|ID of the related item.|
|ns|integer|Nanoseconds when the value was received.|
|value|integer|Received value.|

[comment]: # ({/47696f1e-3e505499})

[comment]: # ({b5aa6e20-11561120})
### String history

The string history object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|clock|timestamp|Time when that value was received.|
|itemid|string|ID of the related item.|
|ns|integer|Nanoseconds when the value was received.|
|value|string|Received value.|

[comment]: # ({/b5aa6e20-11561120})

[comment]: # ({5c27d17e-8e4826ab})
### Text history

The text history object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|id|string|ID of the history entry.|
|clock|timestamp|Time when that value was received.|
|itemid|string|ID of the related item.|
|ns|integer|Nanoseconds when the value was received.|
|value|text|Received value.|

[comment]: # ({/5c27d17e-8e4826ab})

[comment]: # ({3369f4f1-0df964c4})
### Log history

The log history object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|id|string|ID of the history entry.|
|clock|timestamp|Time when that value was received.|
|itemid|string|ID of the related item.|
|logeventid|integer|Windows event log entry ID.|
|ns|integer|Nanoseconds when the value was received.|
|severity|integer|Windows event log entry level.|
|source|string|Windows event log entry source.|
|timestamp|timestamp|Windows event log entry time.|
|value|text|Received value.|

[comment]: # ({/3369f4f1-0df964c4})
