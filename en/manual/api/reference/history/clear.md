[comment]: # ({991d071b-991d071b})
# history.clear

[comment]: # ({/991d071b-991d071b})

[comment]: # ({7c7c67ac-3bf9d0da})
### Description

`object history.clear(array itemids)`

This method allows to clear item history.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/7c7c67ac-3bf9d0da})

[comment]: # ({f449cc26-f449cc26})
### Parameters

`(array)` IDs of items to clear.

[comment]: # ({/f449cc26-f449cc26})

[comment]: # ({a59d9134-a59d9134})
### Return values

`(object)` Returns an object containing the IDs of the cleared items
under the `itemids` property.

[comment]: # ({/a59d9134-a59d9134})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({c73cef81-e482fed3})
#### Clear history

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "history.clear",
    "params": [
        "10325",
        "13205"
    ],
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "10325",
            "13205"
        ]
    },
    "id": 1
}
```

[comment]: # ({/c73cef81-e482fed3})

[comment]: # ({e7aa34e8-e7aa34e8})
### Source

CHistory::clear() in *ui/include/classes/api/services/CHistory.php*.

[comment]: # ({/e7aa34e8-e7aa34e8})
