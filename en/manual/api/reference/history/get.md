[comment]: # ({ad84ea6b-ad84ea6b})
# history.get

[comment]: # ({/ad84ea6b-ad84ea6b})

[comment]: # ({e154f335-385acf3d})
### Description

`integer/array history.get(object parameters)`

The method allows to retrieve history data according to the given
parameters.

::: noteimportant
This method may return historical data of a
deleted entity if this data has not been removed by the housekeeper
yet.
:::

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/e154f335-385acf3d})

[comment]: # ({3654b907-ddac6e9c})
### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|history|integer|History object types to return.<br><br>Possible values:<br>0 - numeric float;<br>1 - character;<br>2 - log;<br>3 - *(default)* numeric unsigned;<br>4 - text.|
|hostids|string/array|Return only history from the given hosts.|
|itemids|string/array|Return only history from the given items.|
|time\_from|timestamp|Return only values that have been received after or at the given time.|
|time\_till|timestamp|Return only values that have been received before or at the given time.|
|sortfield|string/array|Sort the result by the given properties.<br><br>Possible values: `itemid`, `clock`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters) page.|
|editable|boolean|^|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|integer|^|
|output|query|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

[comment]: # ({/3654b907-ddac6e9c})

[comment]: # ({7223bab1-7223bab1})
### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.

[comment]: # ({/7223bab1-7223bab1})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({099d0f6d-cf7bb886})
#### Retrieving item history data

Return 10 latest values received from a numeric(float) item.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "history.get",
    "params": {
        "output": "extend",
        "history": 0,
        "itemids": "23296",
        "sortfield": "clock",
        "sortorder": "DESC",
        "limit": 10
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": [
        {
            "itemid": "23296",
            "clock": "1351090996",
            "value": "0.085",
            "ns": "563157632"
        },
        {
            "itemid": "23296",
            "clock": "1351090936",
            "value": "0.16",
            "ns": "549216402"
        },
        {
            "itemid": "23296",
            "clock": "1351090876",
            "value": "0.18",
            "ns": "537418114"
        },
        {
            "itemid": "23296",
            "clock": "1351090816",
            "value": "0.21",
            "ns": "522659528"
        },
        {
            "itemid": "23296",
            "clock": "1351090756",
            "value": "0.215",
            "ns": "507809457"
        },
        {
            "itemid": "23296",
            "clock": "1351090696",
            "value": "0.255",
            "ns": "495509699"
        },
        {
            "itemid": "23296",
            "clock": "1351090636",
            "value": "0.36",
            "ns": "477708209"
        },
        {
            "itemid": "23296",
            "clock": "1351090576",
            "value": "0.375",
            "ns": "463251343"
        },
        {
            "itemid": "23296",
            "clock": "1351090516",
            "value": "0.315",
            "ns": "447947017"
        },
        {
            "itemid": "23296",
            "clock": "1351090456",
            "value": "0.275",
            "ns": "435307141"
        }
    ],
    "id": 1
}
```

[comment]: # ({/099d0f6d-cf7bb886})

[comment]: # ({621f421c-621f421c})
### Source

CHistory::get() in *ui/include/classes/api/services/CHistory.php*.

[comment]: # ({/621f421c-621f421c})
