[comment]: # ({e73a60a4-e73a60a4})
# > Authentication object

The following objects are directly related to the `authentication` API.

[comment]: # ({/e73a60a4-e73a60a4})

[comment]: # ({195a79fc-2df9176c})
### Authentication

The authentication object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|authentication\_type|integer|Default authentication.<br><br>Possible values:<br>0 - *(default)* Internal;<br>1 - LDAP.|
|http\_auth\_enabled|integer|HTTP authentication.<br><br>Possible values:<br>0 - *(default)* Disabled;<br>1 - Enabled.|
|http\_login\_form|integer|Default login form.<br><br>Possible values:<br>0 - *(default)* Zabbix login form;<br>1 - HTTP login form.|
|http\_strip\_domains|string|Domain name to remove.|
|http\_case\_sensitive|integer|HTTP case sensitive login.<br><br>Possible values:<br>0 - Off;<br>1 - *(default)* On.|
|ldap\_auth\_enabled|integer|LDAP authentication.<br><br>Possible values:<br>0 - *(default)* Disabled;<br>1 - Enabled.|
|ldap\_case\_sensitive|integer|LDAP case sensitive login.<br><br>Possible values:<br>0 - Off;<br>1 - *(default)* On.|
|ldap\_userdirectoryid|string|Default user directory for LDAP authentication. Used for user groups with `gui_access` set to LDAP or System default.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `ldap_auth_enabled` is set to "Enabled"|
|saml\_auth\_enabled|integer|SAML authentication.<br><br>Possible values:<br>0 - *(default)* Disabled;<br>1 - Enabled.|
|saml\_case\_sensitive|integer|SAML case sensitive login.<br><br>Possible values:<br>0 - Off;<br>1 - *(default)* On.|
|passwd\_min\_length|integer|Password minimal length requirement.<br><br>Valid values range from 1 to 70.<br><br>Default: 8.|
|passwd\_check\_rules|integer|Password checking rules.<br>This is a bitmask field, any combination of possible bitmap values is acceptable.<br><br>Possible bitmap values:<br>0 - check password length;<br>1 - check if password uses uppercase and lowercase Latin letters;<br>2 - check if password uses digits;<br>4 - check if password uses special characters;<br>8 - *(default)* check if password is not in the list of commonly used passwords, does not contain derivations of word "Zabbix" or user's name, last name or username.|
|ldap\_jit\_status|integer|Status of LDAP provisioning.<br><br>Possible values:<br>0 - Disabled for configured LDAP IdPs;<br>1 - Enabled for configured LDAP IdPs.|
|saml\_jit\_status|integer|Status of SAML provisioning.<br><br>Possible values:<br>0 - Disabled for configured SAML IdPs;<br>1 - Enabled for configured SAML IdPs.|
|jit\_provision\_interval|string|Time interval between JIT provision requests for logged-in user.<br>Accepts seconds and time unit with suffix with month and year support (3600s,60m,1h,1d,1M,1y). Minimum value: 1h.<br><br>Default: 1h.<br><br>Available only for LDAP provisioning.|
|disabled\_usrgrpid|integer|User group ID to assign the deprovisioned user to.<br>The user group must be disabled and cannot be enabled or deleted when configured.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `ldap_jit_status` is set to "Enabled for configured LDAP IdPs", or `saml_jit_status` is set to "Enabled for configured SAML IdPs"|

[comment]: # ({/195a79fc-2df9176c})
