[comment]: # ({35521717-35521717})
# authentication.update

[comment]: # ({/35521717-35521717})

[comment]: # ({19c357e4-4244aae4})
### Description

`object authentication.update(object authentication)`

This method allows to update existing authentication settings.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/19c357e4-4244aae4})

[comment]: # ({105c772c-6894664c})
### Parameters

`(object)` [Authentication properties](object#authentication) to be updated.

[comment]: # ({/105c772c-6894664c})

[comment]: # ({f63e579c-440363c6})
### Return values

`(array)` Returns an array with the names of updated parameters.

[comment]: # ({/f63e579c-440363c6})

[comment]: # ({8072648f-f95d3166})
### Examples

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "authentication.update",
    "params": {
        "http_auth_enabled": 1,
        "http_case_sensitive": 0,
        "http_login_form": 1
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": [
        "http_auth_enabled",
        "http_case_sensitive",
        "http_login_form"
    ],
    "id": 1
}
```

[comment]: # ({/8072648f-f95d3166})

[comment]: # ({97fcecb8-8e7357d5})
### Source

CAuthentication::update() in *ui/include/classes/api/services/CAuthentication.php*.

[comment]: # ({/97fcecb8-8e7357d5})
