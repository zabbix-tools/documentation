[comment]: # ({558a988d-558a988d})
# authentication.get

[comment]: # ({/558a988d-558a988d})

[comment]: # ({eaed48ba-f493fd10})
### Description

`object authentication.get(object parameters)`

The method allows to retrieve authentication object according to the given parameters.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings.
See [User roles](/manual/web_interface/frontend_sections/users/user_roles) for more information.
:::

[comment]: # ({/eaed48ba-f493fd10})

[comment]: # ({c43c1377-24b20f8d})
### Parameters

`(object)` Parameters defining the desired output.

The method supports only one parameter.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|output|query|This parameter being common for all `get` methods described in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters).|

[comment]: # ({/c43c1377-24b20f8d})

[comment]: # ({6394c247-6394c247})
### Return values

`(object)` Returns authentication object.

[comment]: # ({/6394c247-6394c247})

[comment]: # ({ad1cb1f9-cad6afa3})
### Examples

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "authentication.get",
    "params": {
        "output": "extend"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "authentication_type": "0",
        "http_auth_enabled": "0",
        "http_login_form": "0",
        "http_strip_domains": "",
        "http_case_sensitive": "1",
        "ldap_auth_enabled": "0",
        "ldap_case_sensitive": "1",
        "ldap_userdirectoryid": "0",
        "saml_auth_enabled": "0",
        "saml_case_sensitive": "0",
        "passwd_min_length": "8",
        "passwd_check_rules": "15",
        "jit_provision_interval": "1h",
        "saml_jit_status": "0",
        "ldap_jit_status": "0",
        "disabled_usrgrpid": "9"
    },
    "id": 1
}
```

[comment]: # ({/ad1cb1f9-cad6afa3})

[comment]: # ({5f5520aa-b8be0c1f})
### Source

CAuthentication::get() in *ui/include/classes/api/services/CAuthentication.php*.

[comment]: # ({/5f5520aa-b8be0c1f})
