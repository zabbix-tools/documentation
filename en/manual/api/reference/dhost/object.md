[comment]: # ({854803de-854803de})
# > Discovered host object

The following objects are directly related to the `dhost` API.

[comment]: # ({/854803de-854803de})

[comment]: # ({b93e8735-236628fe})
### Discovered host

::: noteclassic
Discovered host are created by the Zabbix server and cannot
be modified via the API.
:::

The discovered host object contains information about a host discovered
by a network discovery rule. It has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|dhostid|string|ID of the discovered host.|
|druleid|string|ID of the discovery rule that detected the host.|
|lastdown|timestamp|Time when the discovered host last went down.|
|lastup|timestamp|Time when the discovered host last went up.|
|status|integer|Whether the discovered host is up or down. A host is up if it has at least one active discovered service.<br><br>Possible values:<br>0 - host up;<br>1 - host down.|

[comment]: # ({/b93e8735-236628fe})
