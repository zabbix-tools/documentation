[comment]: # ({38f2889f-38f2889f})
# autoregistration.update

[comment]: # ({/38f2889f-38f2889f})

[comment]: # ({011d2d29-13c79ed6})
### Description

`object autoregistration.update(object autoregistration)`

This method allows to update existing autoregistration.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/011d2d29-13c79ed6})

[comment]: # ({5d041d55-dc051772})
### Parameters

`(object)` [Autoregistration properties](object#autoregistration) to be updated.

[comment]: # ({/5d041d55-dc051772})

[comment]: # ({4bb135e8-4bb135e8})
### Return values

`(boolean )` Returns boolean true as result on successful update.

[comment]: # ({/4bb135e8-4bb135e8})

[comment]: # ({5b287322-d84ea91e})
### Examples

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "autoregistration.update",
    "params": {
        "tls_accept": "3",
        "tls_psk_identity": "PSK 001",
        "tls_psk": "11111595725ac58dd977beef14b97461a7c1045b9a1c923453302c5473193478"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": true,
    "id": 1
}
```

[comment]: # ({/5b287322-d84ea91e})

[comment]: # ({0e014313-0e014313})
### Source

CAutoregistration::update() in
*ui/include/classes/api/services/CAutoregistration.php*.

[comment]: # ({/0e014313-0e014313})
