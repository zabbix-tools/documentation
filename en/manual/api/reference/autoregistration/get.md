[comment]: # ({8b0c7f00-8b0c7f00})
# autoregistration.get

[comment]: # ({/8b0c7f00-8b0c7f00})

[comment]: # ({a0c78d0c-c6a45cf0})
### Description

`object autoregistration.get(object parameters)`

The method allows to retrieve autoregistration object according to the
given parameters.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/a0c78d0c-c6a45cf0})

[comment]: # ({c43c1377-24b20f8d})
### Parameters

`(object)` Parameters defining the desired output.

The method supports only one parameter.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|output|query|This parameter being common for all `get` methods described in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters).|

[comment]: # ({/c43c1377-24b20f8d})

[comment]: # ({805927e0-805927e0})
### Return values

`(object)` Returns autoregistration object.

[comment]: # ({/805927e0-805927e0})

[comment]: # ({caa89384-1f25ddf1})
### Examples

[Request](/manual/api#performing-requests):

```json
{
   "jsonrpc": "2.0",
    "method": "autoregistration.get",
    "params": {
        "output": "extend"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "tls_accept": "3"
    },
    "id": 1
}
```

[comment]: # ({/caa89384-1f25ddf1})

[comment]: # ({c74f8347-c74f8347})
### Source

CAutoregistration::get() in
*ui/include/classes/api/services/CAutoregistration.php*.

[comment]: # ({/c74f8347-c74f8347})
