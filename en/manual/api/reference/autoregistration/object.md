[comment]: # ({de6776f4-de6776f4})
# > Autoregistration object

The following objects are directly related to the `autoregistration`
API.

[comment]: # ({/de6776f4-de6776f4})

[comment]: # ({3bb2dbe2-1e4b66e4})
### Autoregistration

The autoregistration object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|tls\_accept|integer|Type of allowed incoming connections for autoregistration.<br><br>Possible values:<br>1 - allow insecure connections;<br>2 - allow TLS with PSK;<br>3 - allow both insecure and TLS with PSK connections.|
|tls\_psk\_identity|string|PSK identity string.<br><br>Do not put sensitive information in the PSK identity, it is transmitted unencrypted over the network to inform a receiver which PSK to use.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *write-only*|
|tls\_psk|string|PSK value string (an even number of hexadecimal characters).<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *write-only*|

[comment]: # ({/3bb2dbe2-1e4b66e4})
