[comment]: # ({cbb0ecad-068e317e})
# Regular expression

This class is designed to work with global regular expressions.

Object references:

-   [Regular expression](/manual/api/reference/regexp/object#regular_expression)
-   [Expressions](/manual/api/reference/regexp/object#expressions)

Available methods:

-   [regexp.create](/manual/api/reference/regexp/create) - create new regular expressions
-   [regexp.delete](/manual/api/reference/regexp/delete) - delete regular expressions
-   [regexp.get](/manual/api/reference/regexp/get) - retrieve regular expressions
-   [regexp.update](/manual/api/reference/regexp/update) - update regular expressions

[comment]: # ({/cbb0ecad-068e317e})
