[comment]: # ({1874bf4c-6525f8c0})
# History

This class is designed to work with history data.

Object references:

-   [Float history](/manual/api/reference/history/object#float_history)
-   [Integer history](/manual/api/reference/history/object#integer_history)
-   [String history](/manual/api/reference/history/object#string_history)
-   [Text history](/manual/api/reference/history/object#text_history)
-   [Log history](/manual/api/reference/history/object#log_history)

Available methods:

-   [history.clear](/manual/api/reference/history/clear) - clear history data
-   [history.get](/manual/api/reference/history/get) - retrieve history data

[comment]: # ({/1874bf4c-6525f8c0})
