[comment]: # ({a7f32a18-81ae720b})
# Maintenance

This class is designed to work with maintenances.

Object references:

-   [Maintenance](/manual/api/reference/maintenance/object#maintenance)
-   [Time period](/manual/api/reference/maintenance/object#time_period)
-   [Problem tag](/manual/api/reference/maintenance/object#problem_tag)

Available methods:

-   [maintenance.create](/manual/api/reference/maintenance/create) - create new maintenances
-   [maintenance.delete](/manual/api/reference/maintenance/delete) - delete maintenances
-   [maintenance.get](/manual/api/reference/maintenance/get) - retrieve maintenances
-   [maintenance.update](/manual/api/reference/maintenance/update) - update maintenances

[comment]: # ({/a7f32a18-81ae720b})
