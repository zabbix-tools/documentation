[comment]: # ({612dd80a-4a04064b})
# Audit log

This class is designed to work with audit log.

Object references:

-   [Audit log](/manual/api/reference/auditlog/object#audit_log)

Available methods:

-   [auditlog.get](/manual/api/reference/auditlog/get) - retrieve audit log records

[comment]: # ({/612dd80a-4a04064b})
