[comment]: # ({69bd0fdb-6bf804b5})
# Host interface

This class is designed to work with host interfaces.

Object references:

-   [Host interface](/manual/api/reference/hostinterface/object#host_interface)
-   [Details](/manual/api/reference/hostinterface/object#details)

Available methods:

-   [hostinterface.create](/manual/api/reference/hostinterface/create) - create new host interfaces
-   [hostinterface.delete](/manual/api/reference/hostinterface/delete) - delete host interfaces
-   [hostinterface.get](/manual/api/reference/hostinterface/get) - retrieve host interfaces
-   [hostinterface.massadd](/manual/api/reference/hostinterface/massadd) - add host interfaces to hosts
-   [hostinterface.massremove](/manual/api/reference/hostinterface/massremove) - remove host interfaces from hosts
-   [hostinterface.replacehostinterfaces](/manual/api/reference/hostinterface/replacehostinterfaces) - replace host interfaces on a host
-   [hostinterface.update](/manual/api/reference/hostinterface/update) - update host interfaces

[comment]: # ({/69bd0fdb-6bf804b5})
