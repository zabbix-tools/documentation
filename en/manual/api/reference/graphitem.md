[comment]: # ({4e1382b0-ba6b9d7f})
# Graph item

This class is designed to work with graph items.

Object references:

-   [Graph item](/manual/api/reference/graphitem/object#graph_item)

Available methods:

-   [graphitem.get](/manual/api/reference/graphitem/get) - retrieve graph items

[comment]: # ({/4e1382b0-ba6b9d7f})
