[comment]: # ({a6b91936-92e6c52c})
# > Task object

The following objects are directly related to the `task` API.

The task object has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|taskid|string|ID of the task.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*|
|type|integer|Type of the task.<br><br>Possible values:<br>1 - Diagnostic information;<br>2 - Refresh proxy configuration;<br>6 - Execute now.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|status|integer|Status of the task.<br><br>Possible values:<br>1 - new task;<br>2 - task in progress;<br>3 - task is completed;<br>4 - task is expired.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*|
|clock|timestamp|Time when the task was created.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*|
|ttl|integer|The time in seconds after which task expires.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*|
|proxy\_hostid|string|ID of the proxy about which diagnostic information statistic is collected.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `type` is set to "Diagnostic information" or "Refresh proxy configuration"|
|request|object|Task request object according to the task type:<br>Object of 'Execute now' task is [described in detail below](/manual/api/reference/task/object#check_now_request_object);<br>Object of 'Refresh proxy configuration' task is [described in detail below](/manual/api/reference/task/object#refresh_proxy_configuration);<br>Object of 'Diagnostic information' task is [described in detail below](/manual/api/reference/task/object#diagnostic_information_request_object).<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|result|object|Result object of the diagnostic information task.<br>May contain NULL if result is not yet ready.<br>Result object is [described in detail below](/manual/api/reference/task/object#statistic_result_object).<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*|

[comment]: # ({/a6b91936-92e6c52c})

[comment]: # ({25750c2c-cd63c2fd})
### 'Execute now' request object

The 'Execute now' task request object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|itemid|string|ID of item and low-level discovery rules.|

[comment]: # ({/25750c2c-cd63c2fd})

[comment]: # ({b41a0a3c-b5d3bd1c})
### 'Refresh proxy configuration' request object

The 'Refresh proxy configuration' task request object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|proxy_hostids|array|Proxy IDs.|

[comment]: # ({/b41a0a3c-b5d3bd1c})

[comment]: # ({e62935dc-52d75b77})
### 'Diagnostic information' request object

The diagnostic information task request object has the following
properties. Statistic request object for all types of properties is
[described in detail
below](/manual/api/reference/task/object#statistic_request_object).

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|historycache|object|History cache statistic request. Available on server and proxy.|
|valuecache|object|Items cache statistic request. Available on server.|
|preprocessing|object|Preprocessing manager statistic request. Available on server and proxy.|
|alerting|object|Alert manager statistic request. Available on server.|
|lld|object|LLD manager statistic request. Available on server.|

[comment]: # ({/e62935dc-52d75b77})

[comment]: # ({b8137482-5149852c})
#### Statistic request object

Statistic request object is used to define what type of information
should be collected about server/proxy internal processes. It has the
following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|stats|query|Statistic object properties to be returned.<br>The list of available fields for each type of diagnostic information statistic are [described in detail below](object#list_of_statistic_fields_available_for_each_type_of_diagnostic_information_request).<br><br>Default: `extend` will return all available statistic fields.|
|top|object|Object to sort and limit returned statistic values.<br>The list of available fields for each type of diagnostic information statistic are [described in detail below](object#list_of_sorting_fields_available_for_each_type_of_diagnostic_information_request).<br><br>Example: { “source.alerts”: 10 }|

[comment]: # ({/b8137482-5149852c})

[comment]: # ({39c2fb30-60e59ca2})
##### List of statistic fields available for each type of diagnostic information request

Following statistic fields can be requested for each type of diagnostic
information request property.

|Diagnostic type|Available fields|Description|
|--|--|------|
|historycache|items|Number of cached items.|
|^|values|Number of cached values.|
|^|memory|Shared memory statistics (free space, number of used chunks, number of free chunks, max size of free chunk).|
|^|memory.data|History data cache shared memory statistics.|
|^|memory.index|History index cache shared memory statistics.|
|valuecache|items|Number of cached items.|
|^|values|Number of cached values.|
|^|memory|Shared memory statistics (free space, number of used chunks, number of free chunks, max size of free chunk).|
|^|mode|Value cache mode.|
|preprocessing|values|Number of queued values.|
|^|preproc.values|Number of queued values with preprocessing steps.|
|alerting|alerts|Number of queued alerts.|
|lld|rules|Number of queued rules.|
|^|values|Number of queued values.|

[comment]: # ({/39c2fb30-60e59ca2})

[comment]: # ({816cd872-09dc3377})
##### List of sorting fields available for each type of diagnostic information request

Following statistic fields can be used to sort and limit requested
information.

|Diagnostic type|Available fields|[Type](/manual/api/reference_commentary#data_types)|
|--|--|------|
|historycache|values|integer|
|valuecache|values|integer|
|^|request.values|integer|
|preprocessing|values|integer|
|alerting|media.alerts|integer|
|^|source.alerts|integer|
|lld|values|integer|

[comment]: # ({/816cd872-09dc3377})

[comment]: # ({1a50c10a-e680be6c})
#### Statistic result object

Statistic result object is retrieved in `result` field of task object.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|status|integer|Status of the task result.<br><br>Possible values:<br>-1 - error occurred during performing task;<br>0 - task result is created.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*|
|data|string/object|Results according the statistic request object of particular diagnostic information task.<br>Contains error message string if error occurred during performing task.|

[comment]: # ({/1a50c10a-e680be6c})
