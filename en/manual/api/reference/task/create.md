[comment]: # ({c9bc126a-c9bc126a})
# task.create

[comment]: # ({/c9bc126a-c9bc126a})

[comment]: # ({df2d3c83-0e3179d3})
### Description

`object task.create(object/array tasks)`

This method allows to create a new task (such as collect diagnostic data
or check items or low-level discovery rules without config reload).

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/df2d3c83-0e3179d3})

[comment]: # ({ca412c18-2574c234})
### Parameters

`(object/array)` A task to create.

The method accepts tasks with the [standard task properties](object#task-object).

Note that 'Execute now' tasks can be created only for the following
types of items/discovery rules:

-   Zabbix agent
-   SNMPv1/v2/v3 agent
-   Simple check
-   Internal check
-   External check
-   Database monitor
-   HTTP agent
-   IPMI agent
-   SSH agent
-   TELNET agent
-   Calculated check
-   JMX agent
-   Dependent item

[comment]: # ({/ca412c18-2574c234})

[comment]: # ({639f303b-918de761})
If item or discovery rule is of type "Dependent item", then top level master item must be of type:

-   Zabbix agent
-   SNMPv1/v2/v3 agent
-   Simple check
-   Internal check
-   External check
-   Database monitor
-   HTTP agent
-   IPMI agent
-   SSH agent
-   TELNET agent
-   Calculated check
-   JMX agent

[comment]: # ({/639f303b-918de761})

[comment]: # ({ab87ce2a-ab87ce2a})
### Return values

`(object)` Returns an object containing the IDs of the created tasks
under the `taskids` property. One task is created for each item and
low-level discovery rule. The order of the returned IDs matches the
order of the passed `itemids`.

[comment]: # ({/ab87ce2a-ab87ce2a})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({efa8e2ed-5c5fa6f1})
#### Creating a task

Create a task `Execute now` for two items. One is an item, the other is a low-level discovery rule.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "task.create",
    "params": [
        {
            "type": 6,
            "request": {
                "itemid": "10092"
            }
        },
        {
            "type": 6,
            "request": {
                "itemid": "10093"
            }
        }
    ],
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "taskids": [
            "1",
            "2"
        ]
    },
    "id": 1
}
```

Create a task `Refresh proxy configuration` for two proxies.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "task.create",
    "params": [
        {
            "type": 2,
            "request": {
                "proxy_hostids": ["10459", "10460"]
            }
        }
    ],
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "taskids": [
            "1"
        ]
    },
    "id": 1
}
```

Create a task `diagnostic information`.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "task.create",
    "params": [
        {
            "type": 1,
            "request": {
                "alerting": {
                    "stats": [
                        "alerts"
                    ],
                    "top": {
                        "media.alerts": 10
                    }
                },
                "lld": {
                    "stats": "extend",
                    "top": {
                        "values": 5
                    }
                }
            },
            "proxy_hostid": 0
        }
    ],
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "taskids": [
            "3"
        ]
    },
    "id": 1
}
```

[comment]: # ({/efa8e2ed-5c5fa6f1})

[comment]: # ({b9d5db95-d87c906b})
### See also

-   [Task](/manual/api/reference/task/object)
-   ['Execute now' request object](/manual/api/reference/task/object)
-   ['Diagnostic information' request object](/manual/api/reference/task/object)
-   [Statistic request object](/manual/api/reference/task/object)

[comment]: # ({/b9d5db95-d87c906b})

[comment]: # ({be350bd3-be350bd3})
### Source

CTask::create() in *ui/include/classes/api/services/CTask.php*.

[comment]: # ({/be350bd3-be350bd3})
