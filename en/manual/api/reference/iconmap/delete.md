[comment]: # ({5c9f356a-5c9f356a})
# iconmap.delete

[comment]: # ({/5c9f356a-5c9f356a})

[comment]: # ({463105c4-5e250e88})
### Description

`object iconmap.delete(array iconMapIds)`

This method allows to delete icon maps.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/463105c4-5e250e88})

[comment]: # ({f911029a-f911029a})
### Parameters

`(array)` IDs of the icon maps to delete.

[comment]: # ({/f911029a-f911029a})

[comment]: # ({44afa603-44afa603})
### Return values

`(object)` Returns an object containing the IDs of the deleted icon maps
under the `iconmapids` property.

[comment]: # ({/44afa603-44afa603})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({fdda7695-c7690d07})
#### Delete multiple icon maps

Delete two icon maps.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "iconmap.delete",
    "params": [
        "2",
        "5"
    ],
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "iconmapids": [
            "2",
            "5"
        ]
    },
    "id": 1
}
```

[comment]: # ({/fdda7695-c7690d07})

[comment]: # ({6198efd4-6198efd4})
### Source

CIconMap::delete() in *ui/include/classes/api/services/CIconMap.php*.

[comment]: # ({/6198efd4-6198efd4})
