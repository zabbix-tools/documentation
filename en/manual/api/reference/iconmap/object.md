[comment]: # ({b72559a5-b72559a5})
# > Icon map object

The following objects are directly related to the `iconmap` API.

[comment]: # ({/b72559a5-b72559a5})

[comment]: # ({c72666aa-74882a27})
### Icon map

The icon map object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|iconmapid|string|ID of the icon map.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*<br>- *required* for update operations|
|default\_iconid|string|ID of the default icon.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* for create operations|
|name|string|Name of the icon map.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* for create operations|

[comment]: # ({/c72666aa-74882a27})

[comment]: # ({bdd746d7-e88b40af})
### Icon mapping

The icon mapping object defines a specific icon to be used for hosts
with a certain inventory field value. It has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|iconid|string|ID of the icon used by the icon mapping.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|expression|string|Expression to match the inventory field against.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|inventory\_link|integer|ID of the host inventory field.<br><br>Refer to the [host inventory object](/manual/api/reference/host/object#host_inventory) for a list of supported inventory fields.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|sortorder|integer|Position of the icon mapping in the icon map.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*|

[comment]: # ({/bdd746d7-e88b40af})
