[comment]: # ({b7611ecd-b7611ecd})
# iconmap.update

[comment]: # ({/b7611ecd-b7611ecd})

[comment]: # ({f9eb2b72-15ec7450})
### Description

`object iconmap.update(object/array iconMaps)`

This method allows to update existing icon maps.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/f9eb2b72-15ec7450})

[comment]: # ({51807852-6f92ef73})
### Parameters

`(object/array)` Icon map properties to be updated.

The `iconmapid` property must be defined for each icon map, all other
properties are optional. Only the passed properties will be updated, all
others will remain unchanged.

Additionally to the [standard icon map properties](object#icon_map), the
method accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|mappings|array|[Icon mappings](/manual/api/reference/iconmap/object#icon_mapping) to replace the existing icon mappings.|

[comment]: # ({/51807852-6f92ef73})

[comment]: # ({a1dbbc92-a1dbbc92})
### Return values

`(object)` Returns an object containing the IDs of the updated icon maps
under the `iconmapids` property.

[comment]: # ({/a1dbbc92-a1dbbc92})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({54ff8f7c-7013c996})
#### Rename icon map

Rename an icon map to "OS icons".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "iconmap.update",
    "params": {
        "iconmapid": "1",
        "name": "OS icons"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "iconmapids": [
            "1"
        ]
    },
    "id": 1
}
```

[comment]: # ({/54ff8f7c-7013c996})

[comment]: # ({8df05cf6-8df05cf6})
### See also

-   [Icon mapping](object#icon_mapping)

[comment]: # ({/8df05cf6-8df05cf6})

[comment]: # ({f35a7b0b-f35a7b0b})
### Source

CIconMap::update() in *ui/include/classes/api/services/CIconMap.php*.

[comment]: # ({/f35a7b0b-f35a7b0b})
