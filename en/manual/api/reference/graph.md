[comment]: # ({74440151-b8d971b2})
# Graph

This class is designed to work with graphs.

Object references:

-   [Graph](/manual/api/reference/graph/object#graph)

Available methods:

-   [graph.create](/manual/api/reference/graph/create) - create new graphs
-   [graph.delete](/manual/api/reference/graph/delete) - delete graphs
-   [graph.get](/manual/api/reference/graph/get) - retrieve graphs
-   [graph.update](/manual/api/reference/graph/update) - update graphs

[comment]: # ({/74440151-b8d971b2})
