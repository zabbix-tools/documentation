<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="en" datatype="plaintext" original="manual/api/reference/alert/object.md">
    <body>
      <trans-unit id="05585755" xml:space="preserve">
        <source># &gt; Alert object

The following objects are directly related to the `alert` API.</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/api/reference/alert/object.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="933fed24" xml:space="preserve">
        <source>### Alert

::: noteclassic
Alerts are created by Zabbix server and cannot be modified via the API.
:::

The alert object contains information about whether certain action
operations have been executed successfully. It has the following
properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|alertid|string|ID of the alert.|
|actionid|string|ID of the action that generated the alert.|
|alerttype|integer|Alert type.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - message;&lt;br&gt;1 - remote command.|
|clock|timestamp|Time when the alert was generated.|
|error|string|Error text if there are problems sending a message or running a command.|
|esc\_step|integer|Action escalation step during which the alert was generated.|
|eventid|string|ID of the event that triggered the action.|
|mediatypeid|string|ID of the media type that was used to send the message.|
|message|text|Message text.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `alerttype` is set to "message"|
|retries|integer|Number of times Zabbix tried to send the message.|
|sendto|string|Address, user name or other identifier of the recipient.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `alerttype` is set to "message"|
|status|integer|Status indicating whether the action operation has been executed successfully.&lt;br&gt;&lt;br&gt;Possible values if `alerttype` is set to "message":&lt;br&gt;0 - message not sent;&lt;br&gt;1 - message sent;&lt;br&gt;2 - failed after a number of retries;&lt;br&gt;3 - new alert is not yet processed by alert manager.&lt;br&gt;&lt;br&gt;Possible values if `alerttype` is set to "remote command":&lt;br&gt;0 - command not run;&lt;br&gt;1 - command run;&lt;br&gt;2 - tried to run the command on Zabbix agent, but it was unavailable.|
|subject|string|Message subject.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `alerttype` is set to "message"|
|userid|string|ID of the user that the message was sent to.|
|p\_eventid|string|ID of problem event, which generated the alert.|
|acknowledgeid|string|ID of acknowledgment, which generated the alert.|</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/api/reference/alert/object.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
    </body>
  </file>
</xliff>
