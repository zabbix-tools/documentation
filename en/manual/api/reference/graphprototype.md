[comment]: # ({3f31cfac-62384434})
# Graph prototype

This class is designed to work with graph prototypes.

Object references:

-   [Graph prototype](/manual/api/reference/graphprototype/object#graph_prototype)

Available methods:

-   [graphprototype.create](/manual/api/reference/graphprototype/create) - create new graph prototypes
-   [graphprototype.delete](/manual/api/reference/graphprototype/delete) - delete graph prototypes
-   [graphprototype.get](/manual/api/reference/graphprototype/get) - retrieve graph prototypes
-   [graphprototype.update](/manual/api/reference/graphprototype/update) - update graph prototypes

[comment]: # ({/3f31cfac-62384434})
