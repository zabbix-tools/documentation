[comment]: # ({48cd3524-48cd3524})
# problem.get

[comment]: # ({/48cd3524-48cd3524})

[comment]: # ({8a42147b-a18b2713})
### Description

`integer/array problem.get(object parameters)`

The method allows to retrieve problems according to the given
parameters.

This method is for retrieving unresolved problems. It is also possible,
if specified, to additionally retrieve recently resolved problems. The
period that determines how old is "recently" is defined in
*Administration* →
*[General](/manual/web_interface/frontend_sections/administration/general#trigger_displaying_options)*.
Problems that were resolved prior to that period are not kept in the
problem table. To retrieve problems that were resolved further back in
the past, use the [`event.get`](/manual/api/reference/event/get) method.

::: noteimportant
This method may return problems of a deleted
entity if these problems have not been removed by the housekeeper
yet.
:::

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/8a42147b-a18b2713})

[comment]: # ({bffa4f3a-98f39c6a})
### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|eventids|string/array|Return only problems with the given IDs.|
|groupids|string/array|Return only problems created by objects that belong to the given host groups.|
|hostids|string/array|Return only problems created by objects that belong to the given hosts.|
|objectids|string/array|Return only problems created by the given objects.|
|source|integer|Return only problems with the given type.<br><br>Refer to the [problem event object page](object#problem) for a list of supported event types.<br><br>Default: 0 - problem created by a trigger.|
|object|integer|Return only problems created by objects of the given type.<br><br>Refer to the [problem event object page](object#problem) for a list of supported object types.<br><br>Default: 0 - trigger.|
|acknowledged|boolean|`true` - return acknowledged problems only;<br>`false` - unacknowledged only.|
|suppressed|boolean|`true` - return only suppressed problems;<br>`false` - return problems in the normal state.|
|symptom|boolean|`true` - return only symptom problem events;<br>`false` - return only cause problem events.|
|severities|integer/array|Return only problems with given event severities. Applies only if object is trigger.|
|evaltype|integer|Rules for tag searching.<br><br>Possible values:<br>0 - (default) And/Or;<br>2 - Or.|
|tags|array of objects|Return only problems with given tags. Exact match by tag and case-insensitive search by value and operator.<br>Format: `[{"tag": "<tag>", "value": "<value>", "operator": "<operator>"}, ...]`.<br>An empty array returns all problems.<br><br>Possible operator types:<br>0 - (default) Like;<br>1 - Equal;<br>2 - Not like;<br>3 - Not equal<br>4 - Exists;<br>5 - Not exists.|
|recent|boolean|`true` - return PROBLEM and recently RESOLVED problems (depends on Display OK triggers for N seconds)<br>Default: `false` - UNRESOLVED problems only|
|eventid\_from|string|Return only problems with IDs greater or equal to the given ID.|
|eventid\_till|string|Return only problems with IDs less or equal to the given ID.|
|time\_from|timestamp|Return only problems that have been created after or at the given time.|
|time\_till|timestamp|Return only problems that have been created before or at the given time.|
|selectAcknowledges|query|Return an `acknowledges` property with the problem updates. Problem updates are sorted in reverse chronological order.<br><br>The problem update object has the following properties:<br>`acknowledgeid` - `(string)` update's ID;<br>`userid` - `(string)` ID of the user that updated the event;<br>`eventid` - `(string)` ID of the updated event;<br>`clock` - `(timestamp)` time when the event was updated;<br>`message` - `(string)` text of the message;<br>`action` - `(integer)`type of update action (see [`event.acknowledge`](/manual/api/reference/event/acknowledge));<br>`old_severity` - `(integer)` event severity before this update action;<br>`new_severity` - `(integer)` event severity after this update action;<br>`suppress_until` - `(timestamp)` time till event will be suppressed;<br>`taskid` - `(string)` ID of task if current event is undergoing a rank change;<br><br>Supports `count`.|
|selectTags|query|Return a [`tags`](/manual/api/reference/problem/object#problem_tag) property with the problem tags. Output format: `[{"tag": "<tag>", "value": "<value>"}, ...]`.|
|selectSuppressionData|query|Return a `suppression_data` property with the list of active maintenances and manual suppressions:<br>`maintenanceid` - `(string)` ID of the maintenance;<br>`userid` - `(string)` ID of user who suppressed the problem;<br>`suppress_until` - `(integer)` time until the problem is suppressed.|
|filter|object|Return only those results that exactly match the given filter.<br><br>Accepts an object, where the keys are property names, and the values are either a single value or an array of values to match against.<br><br>Supports additional filters:<br>`action` - the [event update actions](/manual/api/reference/event/acknowledge#parameters) that have been performed for particular problem event. For multiple actions use combination of any acceptable bitmap values as bitmask;<br>`action_userid` - ID of user who performed event update action.|
|sortfield|string/array|Sort the result by the given properties.<br><br>Possible values: `eventid`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters) page.|
|editable|boolean|^|
|excludeSearch|boolean|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

[comment]: # ({/bffa4f3a-98f39c6a})

[comment]: # ({7223bab1-7223bab1})
### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.

[comment]: # ({/7223bab1-7223bab1})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({dba417e0-4d83529d})
#### Retrieving trigger problem events

Retrieve recent events from trigger "15112."

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "problem.get",
    "params": {
        "output": "extend",
        "selectAcknowledges": "extend",
        "selectTags": "extend",
        "selectSuppressionData": "extend",
        "objectids": "15112",
        "recent": "true",
        "sortfield": ["eventid"],
        "sortorder": "DESC"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": [
        {
            "eventid": "1245463",
            "source": "0",
            "object": "0",
            "objectid": "15112",
            "clock": "1472457242",
            "ns": "209442442",
            "r_eventid": "1245468",
            "r_clock": "1472457285",
            "r_ns": "125644870",
            "correlationid": "0",
            "userid": "1",
            "name": "Zabbix agent on localhost is unreachable for 5 minutes",
            "acknowledged": "1",
            "severity": "3",
            "cause_eventid": "0",
            "opdata": "",
            "acknowledges": [
                {
                    "acknowledgeid": "14443",
                    "userid": "1",
                    "eventid": "1245463",
                    "clock": "1472457281",
                    "message": "problem solved",
                    "action": "6",
                    "old_severity": "0",
                    "new_severity": "0",
                    "suppress_until": "1472511600",
                    "taskid": "0"
                }
            ],
            "suppression_data": [
                {
                    "maintenanceid": "15",
                    "suppress_until": "1472511600",
                    "userid": "0"
                }
            ],
            "suppressed": "1",
            "tags": [
                {
                    "tag": "test tag",
                    "value": "test value"
                }
            ]
        }
    ],
    "id": 1
}
```

[comment]: # ({/dba417e0-4d83529d})

[comment]: # ({4df4ab9d-by})
#### Retrieving problems acknowledged by specified user

Retrieving problems acknowledged by user with ID=10

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "problem.get",
    "params": {
        "output": "extend",
        "selectAcknowledges": ["userid", "action"],
        "filter": {
            "action": 2,
            "action_userid": 10
        },
        "sortfield": ["eventid"],
        "sortorder": "DESC"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": [
        {
            "eventid": "1248566",
            "source": "0",
            "object": "0",
            "objectid": "15142",
            "clock": "1472457242",
            "ns": "209442442",
            "r_eventid": "1245468",
            "r_clock": "1472457285",
            "r_ns": "125644870",
            "correlationid": "0",
            "userid": "10",
            "name": "Zabbix agent on localhost is unreachable for 5 minutes",
            "acknowledged": "1",
            "severity": "3",
            "cause_eventid": "0",
            "opdata": "",
            "acknowledges": [
                {
                    "userid": "10",
                    "action": "2"
                }
            ],
            "suppressed": "0"
        }
    ],
    "id": 1
}
```

[comment]: # ({/4df4ab9d-by})

[comment]: # ({c6eb3ba0-c6eb3ba0})
### See also

-   [Alert](/manual/api/reference/alert/object)
-   [Item](/manual/api/reference/item/object)
-   [Host](/manual/api/reference/host/object)
-   [LLD rule](/manual/api/reference/discoveryrule/object#lld_rule)
-   [Trigger](/manual/api/reference/trigger/object)

[comment]: # ({/c6eb3ba0-c6eb3ba0})

[comment]: # ({15f9267d-15f9267d})
### Source

CEvent::get() in *ui/include/classes/api/services/CProblem.php*.

[comment]: # ({/15f9267d-15f9267d})
