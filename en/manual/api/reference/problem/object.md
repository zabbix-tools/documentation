[comment]: # ({a5270fd6-001bbea1})
# > Problem object

The following objects are directly related to the `problem` API.

[comment]: # ({/a5270fd6-001bbea1})

[comment]: # ({aada44fa-1ce0b7ed})
### Problem

::: noteclassic
Problems are created by the Zabbix server and cannot be
modified via the API.
:::

The problem object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|eventid|string|ID of the problem event.|
|source|integer|Type of the problem event.<br><br>Possible values:<br>0 - event created by a trigger;<br>3 - internal event;<br>4 - event created on service status update.|
|object|integer|Type of object that is related to the problem event.<br><br>Possible values if `source` is set to "event created by a trigger":<br>0 - trigger.<br><br>Possible values if `source` is set to "internal event":<br>0 - trigger;<br>4 - item;<br>5 - LLD rule.<br><br>Possible values if `source` is set to "event created on service status update":<br>6 - service.|
|objectid|string|ID of the related object.|
|clock|timestamp|Time when the problem event was created.|
|ns|integer|Nanoseconds when the problem event was created.|
|r\_eventid|string|Recovery event ID.|
|r\_clock|timestamp|Time when the recovery event was created.|
|r\_ns|integer|Nanoseconds when the recovery event was created.|
|cause_eventid|string|Cause event ID.|
|correlationid|string|Correlation rule ID if this event was recovered by global correlation rule.|
|userid|string|User ID if the problem was manually closed.|
|name|string|Resolved problem name.|
|acknowledged|integer|Acknowledge state for problem.<br><br>Possible values:<br>0 - not acknowledged;<br>1 - acknowledged.|
|severity|integer|Problem current severity.<br><br>Possible values:<br>0 - not classified;<br>1 - information;<br>2 - warning;<br>3 - average;<br>4 - high;<br>5 - disaster.|
|suppressed|integer|Whether the problem is suppressed.<br><br>Possible values:<br>0 - problem is in normal state;<br>1 - problem is suppressed.|
|opdata|string|Operational data with expanded macros.|
|urls|array|Active [media type URLs](/manual/api/reference/problem/object#media-type-url).|

[comment]: # ({/aada44fa-1ce0b7ed})

[comment]: # ({e22c1c36-acd9b507})
### Problem tag

The problem tag object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|tag|string|Problem tag name.|
|value|string|Problem tag value.|

[comment]: # ({/e22c1c36-acd9b507})

[comment]: # ({bd0579ef-52ff7df7})
### Media type URL

The media type URL object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|name|string|Media type defined URL name.|
|url|string|Media type defined URL value.|

Results will contain entries only for active media types with enabled event menu entry.
Macro used in properties will be expanded, but if one of the properties contains an unexpanded macro, both properties will be excluded from results.
For supported macros, see [*Supported macros*](/manual/appendix/macros/supported_by_location).

[comment]: # ({/bd0579ef-52ff7df7})
