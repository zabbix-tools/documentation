[comment]: # ({c9539601-c9539601})
# > Maintenance object

The following objects are directly related to the `maintenance` API.

[comment]: # ({/c9539601-c9539601})

[comment]: # ({83642866-6c1a70bc})
### Maintenance

The maintenance object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|maintenanceid|string|ID of the maintenance.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*<br>- *required* for update operations|
|name|string|Name of the maintenance.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* for create operations|
|active\_since|timestamp|Time when the maintenance becomes active.<br><br>The given value will be rounded down to minutes.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* for create operations|
|active\_till|timestamp|Time when the maintenance stops being active.<br><br>The given value will be rounded down to minutes.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* for create operations|
|description|string|Description of the maintenance.|
|maintenance\_type|integer|Type of maintenance.<br><br>Possible values:<br>0 - *(default)* with data collection;<br>1 - without data collection.|
|tags\_evaltype|integer|Problem tag evaluation method.<br><br>Possible values:<br>0 - *(default)* And/Or;<br>2 - Or.|

[comment]: # ({/83642866-6c1a70bc})

[comment]: # ({b6d4ec79-506f52aa})
### Time period

The time period object is used to define periods when the maintenance must come into effect.
It has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|period|integer|Duration of the maintenance period in seconds.<br><br>The given value will be rounded down to minutes.<br><br>Default: 3600.|
|timeperiod\_type|integer|Type of time period.<br><br>Possible values:<br>0 - *(default)* one time only;<br>2 - daily;<br>3 - weekly;<br>4 - monthly.|
|start\_date|timestamp|Date when the maintenance period must come into effect.<br>The given value will be rounded down to minutes.<br><br>Default: current date.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `timeperiod_type` is set to "one time only"|
|start\_time|integer|Time of day when the maintenance starts in seconds.<br>The given value will be rounded down to minutes.<br><br>Default: 0.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `timeperiod_type` is set to "daily", "weekly", or "monthly"|
|every|integer|For daily and weekly periods `every` defines the day or week intervals at which the maintenance must come into effect.<br>Default value if `timeperiod_type` is set to "daily" or "weekly": 1.<br><br>For monthly periods when `day` is set, the `every` property defines the day of the month when the maintenance must come into effect.<br>Default value if `timeperiod_type` is set to "monthly" and `day` is set: 1.<br><br>For monthly periods when `dayofweek` is set, the `every` property defines the week of the month when the maintenance must come into effect.<br>Possible values if `timeperiod_type` is set to "monthly" and `dayofweek` is set:<br>1 - *(default)* first week;<br>2 - second week;<br>3 - third week;<br>4 - fourth week;<br>5 - last week.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `timeperiod_type` is set to "daily", "weekly", or "monthly"|
|dayofweek|integer|Days of the week when the maintenance must come into effect.<br><br>Days are stored in binary form with each bit representing the corresponding day. For example, 4 equals 100 in binary and means, that maintenance will be enabled on Wednesday.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `timeperiod_type` is set to "weekly", or if `timeperiod_type` is set to "monthly" and `day` is not set<br>- *supported* if `timeperiod_type` is set to "monthly"|
|day|integer|Day of the month when the maintenance must come into effect.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `timeperiod_type` is set to "monthly" and `dayofweek` is not set<br>- *supported* if `timeperiod_type` is set to "monthly"|
|month|integer|Months when the maintenance must come into effect.<br><br>Months are stored in binary form with each bit representing the corresponding month. For example, 5 equals 101 in binary and means, that maintenance will be enabled in January and March.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `timeperiod_type` is set to "monthly"|

[comment]: # ({/b6d4ec79-506f52aa})

[comment]: # ({b7c44600-2913a3ae})
### Problem tag

The problem tag object is used to define which problems must be suppressed when the maintenance comes into effect.
Tags can only be specified if `maintenance_type` of [Maintenance object](#maintenance) is set to "with data collection".
It has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|tag|string|Problem tag name.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|operator|integer|Condition operator.<br><br>Possible values:<br>0 - Equals;<br>2 - *(default)* Contains.|
|value|string|Problem tag value.|

[comment]: # ({/b7c44600-2913a3ae})
