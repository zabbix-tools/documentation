[comment]: # ({8c711e69-8c711e69})
# maintenance.update

[comment]: # ({/8c711e69-8c711e69})

[comment]: # ({e3d076ea-292dcffa})
### Description

`object maintenance.update(object/array maintenances)`

This method allows to update existing maintenances.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/e3d076ea-292dcffa})

[comment]: # ({3d7c1c86-678c22b3})
### Parameters

`(object/array)` Maintenance properties to be updated.

The `maintenanceid` property must be defined for each maintenance, all
other properties are optional. Only the passed properties will be
updated, all others will remain unchanged.

Additionally to the [standard maintenance
properties](object#maintenance), the method accepts the following
parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|groups|object/array|Host [groups](/manual/api/reference/hostgroup/object) to replace the current groups.<br><br>The host groups must have the `groupid` property defined.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required* if `hosts` is not set|
|hosts|object/array|[Hosts](/manual/api/reference/host/object) to replace the current hosts.<br><br>The hosts must have only the `hostid` property defined.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required* if `groups` is not set|
|timeperiods|object/array|Maintenance [time periods](/manual/api/reference/maintenance/object#time_period) to replace the current periods.|
|tags|object/array|[Problem tags](/manual/api/reference/maintenance/object#problem_tag) to replace the current tags.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *supported* if `maintenance_type` of [Maintenance object](object#maintenance) is set to "with data collection"|
|groupids<br>(deprecated)|array|This parameter is deprecated, please use `groups` instead.<br>IDs of the host groups that will undergo maintenance.|
|hostids<br>(deprecated)|array|This parameter is deprecated, please use `hosts` instead.<br>IDs of the hosts that will undergo maintenance.|

[comment]: # ({/3d7c1c86-678c22b3})

[comment]: # ({b9ffa219-b9ffa219})
### Return values

`(object)` Returns an object containing the IDs of the updated
maintenances under the `maintenanceids` property.

[comment]: # ({/b9ffa219-b9ffa219})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({ffac6646-25215f8d})
#### Assigning different hosts

Replace the hosts currently assigned to maintenance with two different ones.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "maintenance.update",
    "params": {
        "maintenanceid": "3",
        "hosts": [
            {"hostid": "10085"},
            {"hostid": "10084"}
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "maintenanceids": [
            "3"
        ]
    },
    "id": 1
}
```

[comment]: # ({/ffac6646-25215f8d})

[comment]: # ({82b3ef0e-82b3ef0e})
### See also

-   [Time period](object#time_period)

[comment]: # ({/82b3ef0e-82b3ef0e})

[comment]: # ({63ad1661-63ad1661})
### Source

CMaintenance::update() in
*ui/include/classes/api/services/CMaintenance.php*.

[comment]: # ({/63ad1661-63ad1661})
