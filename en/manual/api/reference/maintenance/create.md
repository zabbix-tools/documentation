[comment]: # ({f47b150e-f47b150e})
# maintenance.create

[comment]: # ({/f47b150e-f47b150e})

[comment]: # ({a2eec896-e8498ebc})
### Description

`object maintenance.create(object/array maintenances)`

This method allows to create new maintenances.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/a2eec896-e8498ebc})

[comment]: # ({bb5d434e-6c14daac})
### Parameters

`(object/array)` Maintenances to create.

Additionally to the [standard maintenance
properties](object#maintenance), the method accepts the following
parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|groups|object/array|Host [groups](/manual/api/reference/hostgroup/object) that will undergo maintenance.<br><br>The host groups must have the `groupid` property defined.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required* if `hosts` is not set|
|hosts|object/array|[Hosts](/manual/api/reference/host/object) that will undergo maintenance.<br><br>The hosts must have only the `hostid` property defined.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required* if `groups` is not set|
|timeperiods|object/array|Maintenance [time periods](/manual/api/reference/maintenance/object#time_period).<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required*|
|tags|object/array|[Problem tags](/manual/api/reference/maintenance/object#problem_tag).<br><br>Define what problems must be suppressed.<br>If no tags are given, all active maintenance host problems will be suppressed.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *supported* if `maintenance_type` of [Maintenance object](object#maintenance) is set to "with data collection"|
|groupids<br>(deprecated)|array|This parameter is deprecated, please use `groups` instead.<br>IDs of the host groups that will undergo maintenance.|
|hostids<br>(deprecated)|array|This parameter is deprecated, please use `hosts` instead.<br>IDs of the hosts that will undergo maintenance.|

[comment]: # ({/bb5d434e-6c14daac})

[comment]: # ({48c4869c-48c4869c})
### Return values

`(object)` Returns an object containing the IDs of the created
maintenances under the `maintenanceids` property. The order of the
returned IDs matches the order of the passed maintenances.

[comment]: # ({/48c4869c-48c4869c})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({09eaf740-bb812c67})
#### Creating a maintenance

Create a maintenance with data collection for host group with ID "2" and with
problem tags **service:mysqld** and **error**. It must be active from
22.01.2013 till 22.01.2014, come in effect each Sunday at 18:00 and last
for one hour.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "maintenance.create",
    "params": {
        "name": "Sunday maintenance",
        "active_since": 1358844540,
        "active_till": 1390466940,
        "tags_evaltype": 0,
        "groups": [
            {"groupid": "2"}
        ],
        "timeperiods": [
            {
                "period": 3600,
                "timeperiod_type": 3,
                "start_time": 64800,
                "every": 1,
                "dayofweek": 64
            }
        ],
        "tags": [
            {
                "tag": "service",
                "operator": "0",
                "value": "mysqld"
            },
            {
                "tag": "error",
                "operator": "2",
                "value": ""
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "maintenanceids": [
            "3"
        ]
    },
    "id": 1
}
```

[comment]: # ({/09eaf740-bb812c67})

[comment]: # ({82b3ef0e-82b3ef0e})
### See also

-   [Time period](object#time_period)

[comment]: # ({/82b3ef0e-82b3ef0e})

[comment]: # ({3e3a556f-3e3a556f})
### Source

CMaintenance::create() in
*ui/include/classes/api/services/CMaintenance.php*.

[comment]: # ({/3e3a556f-3e3a556f})
