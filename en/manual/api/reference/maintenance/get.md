[comment]: # ({0d3af930-0d3af930})
# maintenance.get

[comment]: # ({/0d3af930-0d3af930})

[comment]: # ({6ac016c2-a04dd4c9})
### Description

`integer/array maintenance.get(object parameters)`

The method allows to retrieve maintenances according to the given
parameters.

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/6ac016c2-a04dd4c9})

[comment]: # ({1300c40b-9ca798d7})
### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|groupids|string/array|Return only maintenances that are assigned to the given host groups.|
|hostids|string/array|Return only maintenances that are assigned to the given hosts.|
|maintenanceids|string/array|Return only maintenances with the given IDs.|
|selectHostGroups|query|Return a [`hostgroups`](/manual/api/reference/hostgroup/object) property with host groups assigned to the maintenance.|
|selectHosts|query|Return a [`hosts`](/manual/api/reference/host/object) property with hosts assigned to the maintenance.|
|selectTags|query|Return a [`tags`](/manual/api/reference/maintenance/object#problem_tag) property with problem tags of the maintenance.|
|selectTimeperiods|query|Return a [`timeperiods`](/manual/api/reference/maintenance/object#Time_period) property with time periods of the maintenance.|
|sortfield|string/array|Sort the result by the given properties.<br><br>Possible values: `maintenanceid`, `name`, `maintenance_type`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters).|
|editable|boolean|^|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|
|selectGroups<br>(deprecated)|query|This parameter is deprecated, please use `selectHostGroups` instead.<br>Return a [`groups`](/manual/api/reference/hostgroup/object) property with host groups assigned to the maintenance.|

[comment]: # ({/1300c40b-9ca798d7})

[comment]: # ({7223bab1-7223bab1})
### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.

[comment]: # ({/7223bab1-7223bab1})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({f9db3d05-0b1cb387})
#### Retrieving maintenances

Retrieve all configured maintenances, and the data about the assigned
host groups, defined time periods and problem tags.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "maintenance.get",
    "params": {
        "output": "extend",
        "selectHostGroups": "extend",
        "selectTimeperiods": "extend",
        "selectTags": "extend"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": [
        {
            "maintenanceid": "3",
            "name": "Sunday maintenance",
            "maintenance_type": "0",
            "description": "",
            "active_since": "1358844540",
            "active_till": "1390466940",
            "tags_evaltype": "0",
            "hostgroups": [
                {
                    "groupid": "4",
                    "name": "Zabbix servers",
                    "flags": "0",
                    "uuid": "6f6799aa69e844b4b3918f779f2abf08"
                }
            ],
            "timeperiods": [
                {
                    "timeperiod_type": "3",
                    "every": "1",
                    "month": "0",
                    "dayofweek": "1",
                    "day": "0",
                    "start_time": "64800",
                    "period": "3600",
                    "start_date": "2147483647"
                }
            ],
            "tags": [
                {
                    "tag": "service",
                    "operator": "0",
                    "value": "mysqld",
                },
                {
                    "tag": "error",
                    "operator": "2",
                    "value": ""
                }
            ]
        }
    ],
    "id": 1
}
```

[comment]: # ({/f9db3d05-0b1cb387})

[comment]: # ({dbcf678a-dbcf678a})
### See also

-   [Host](/manual/api/reference/host/object#host)
-   [Host group](/manual/api/reference/hostgroup/object#host_group)
-   [Time period](object#time_period)

[comment]: # ({/dbcf678a-dbcf678a})

[comment]: # ({50ed5be3-50ed5be3})
### Source

CMaintenance::get() in
*ui/include/classes/api/services/CMaintenance.php*.

[comment]: # ({/50ed5be3-50ed5be3})
