[comment]: # ({0aafdb81-0aafdb81})
# maintenance.delete

[comment]: # ({/0aafdb81-0aafdb81})

[comment]: # ({e15fc88c-5afbd3a3})
### Description

`object maintenance.delete(array maintenanceIds)`

This method allows to delete maintenance periods.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/e15fc88c-5afbd3a3})

[comment]: # ({a218bada-9828d580})
### Parameters

`(array)` IDs of the maintenance periods to delete.

[comment]: # ({/a218bada-9828d580})

[comment]: # ({9a613669-1bb5e665})
### Return values

`(object)` Returns an object containing the IDs of the deleted
maintenance periods under the `maintenanceids` property.

[comment]: # ({/9a613669-1bb5e665})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({1045fb6f-9baa4c26})
#### Deleting multiple maintenance periods

Delete two maintenance periods.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "maintenance.delete",
    "params": [
        "3",
        "1"
    ],
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "maintenanceids": [
            "3",
            "1"
        ]
    },
    "id": 1
}
```

[comment]: # ({/1045fb6f-9baa4c26})

[comment]: # ({87f95129-87f95129})
### Source

CMaintenance::delete() in
*ui/include/classes/api/services/CMaintenance.php*.

[comment]: # ({/87f95129-87f95129})
