[comment]: # ({8f6818ce-b4f3eb78})
# Module

[comment]: # ({/8f6818ce-b4f3eb78})

[comment]: # ({c3c0ad47-8202f870})

This class is designed to work with frontend modules.

Object references:

-   [Module](/manual/api/reference/module/object#module)

Available methods:

-   [module.create](/manual/api/reference/module/create) - install new modules
-   [module.delete](/manual/api/reference/module/delete) - uninstall modules
-   [module.get](/manual/api/reference/module/get) - retrieve modules
-   [module.update](/manual/api/reference/module/update) - update modules

[comment]: # ({/c3c0ad47-8202f870})
