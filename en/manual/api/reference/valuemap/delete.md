[comment]: # ({a1e429c6-a1e429c6})
# valuemap.delete

[comment]: # ({/a1e429c6-a1e429c6})

[comment]: # ({a8cd22a2-b73af783})
### Description

`object valuemap.delete(array valuemapids)`

This method allows to delete value maps.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/a8cd22a2-b73af783})

[comment]: # ({db6b3809-db6b3809})
### Parameters

`(array)` IDs of the value maps to delete.

[comment]: # ({/db6b3809-db6b3809})

[comment]: # ({59feda24-59feda24})
### Return values

`(object)` Returns an object containing the IDs of the deleted value
maps under the `valuemapids` property.

[comment]: # ({/59feda24-59feda24})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({66cfb77c-bf1c93a7})
#### Deleting multiple value maps

Delete two value maps.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "valuemap.delete",
    "params": [
        "1",
        "2"
    ],
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "valuemapids": [
            "1",
            "2"
        ]
    },
    "id": 1
}
```

[comment]: # ({/66cfb77c-bf1c93a7})

[comment]: # ({908d048e-908d048e})
### Source

CValueMap::delete() in *ui/include/classes/api/services/CValueMap.php*.

[comment]: # ({/908d048e-908d048e})
