[comment]: # ({41154bde-41154bde})
# > Value map object

The following objects are directly related to the `valuemap` API.

[comment]: # ({/41154bde-41154bde})

[comment]: # ({151203af-65471f47})
### Value map

The value map object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|valuemapid|string|ID of the value map.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*<br>- *required* for update operations|
|hostid|id|ID of the host or template that the value map belongs to.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *constant*<br>- *required* for create operations|
|name|string|Name of the value map.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* for create operations|
|mappings|array|Value mappings for current value map. The mapping object is [described in detail below](object#value_mappings).<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* for create operations|
|uuid|string|Universal unique identifier, used for linking imported value maps to already existing ones. Auto-generated, if not given.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if the value map belongs to a template|

[comment]: # ({/151203af-65471f47})

[comment]: # ({46223286-145a9918})
### Value mappings

The value mappings object defines value mappings of the value map. It
has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|type|integer|Mapping match type.<br><br>Possible values:<br>0 - *(default)* mapping will be applied if value is equal;<br>1 - mapping will be applied if value is greater or equal^1^;<br>2 - mapping will be applied if value is less or equal^1^;<br>3 - mapping will be applied if value is in range (ranges are inclusive; multiple ranges, separated by comma character, can be defined)^1^;<br>4 - mapping will be applied if value matches a regular expression^2^;<br>5 - if no matches are found, mapping will not be applied, and the default value will be used.<br><br>If `type` is set to "0", "1", "2", "3", "4", then `value` cannot be empty.<br><br>If `type` is set to "5", then `value` must be empty.|
|value|string|Original value.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `type` is set to "1", "2", "3", "4"<br>- *supported* if `type` is set to "5"|
|newvalue|string|Value to which the original value is mapped to.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|

^1^ supported only for items having value type "numeric unsigned", "numeric float".\
^2^ supported only for items having value type "character".\

[comment]: # ({/46223286-145a9918})
