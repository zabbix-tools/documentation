[comment]: # ({6698fb90-6698fb90})
# valuemap.create

[comment]: # ({/6698fb90-6698fb90})

[comment]: # ({34b10afc-37547a16})
### Description

`object valuemap.create(object/array valuemaps)`

This method allows to create new value maps.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/34b10afc-37547a16})

[comment]: # ({efa9b6ec-efa9b6ec})
### Parameters

`(object/array)` Value maps to create.

The method accepts value maps with the [standard value map
properties](object#value_map).

[comment]: # ({/efa9b6ec-efa9b6ec})

[comment]: # ({6809040a-6809040a})
### Return values

`(object)` Returns an object containing the IDs of the created value
maps the `valuemapids` property. The order of the returned IDs matches
the order of the passed value maps.

[comment]: # ({/6809040a-6809040a})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({07a2014d-ace763eb})
#### Creating a value map

Create one value map with two mappings.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "valuemap.create",
    "params": {
        "hostid": "50009",
        "name": "Service state",
        "mappings": [
            {
                "type": "1",
                "value": "1",
                "newvalue": "Up"
            },
            {
                "type": "5",
                "newvalue": "Down"
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "valuemapids": [
            "1"
        ]
    },
    "id": 1
}
```

[comment]: # ({/07a2014d-ace763eb})

[comment]: # ({58f99a13-58f99a13})
### Source

CValueMap::create() in *ui/include/classes/api/services/CValueMap.php*.

[comment]: # ({/58f99a13-58f99a13})
