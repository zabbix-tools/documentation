[comment]: # ({6b085503-6b085503})
# valuemap.update

[comment]: # ({/6b085503-6b085503})

[comment]: # ({932b4e3f-ab463b0c})
### Description

`object valuemap.update(object/array valuemaps)`

This method allows to update existing value maps.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/932b4e3f-ab463b0c})

[comment]: # ({0094db2a-0094db2a})
### Parameters

`(object/array)` [Value map properties](object#value_map) to be updated.

The `valuemapid` property must be defined for each value map, all other
properties are optional. Only the passed properties will be updated, all
others will remain unchanged.

[comment]: # ({/0094db2a-0094db2a})

[comment]: # ({0dbe7e67-0dbe7e67})
### Return values

`(object)` Returns an object containing the IDs of the updated value
maps under the `valuemapids` property.

[comment]: # ({/0dbe7e67-0dbe7e67})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({ff5e06e9-571fcc62})
#### Changing value map name

Change value map name to "Device status".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "valuemap.update",
    "params": {
        "valuemapid": "2",
        "name": "Device status"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "valuemapids": [
            "2"
        ]
    },
    "id": 1
}
```

[comment]: # ({/ff5e06e9-571fcc62})

[comment]: # ({1b91e008-7b3b6bc4})
#### Changing mappings for one value map.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "valuemap.update",
    "params": {
        "valuemapid": "2",
        "mappings": [
            {
                "type": "0",
                "value": "0",
                "newvalue": "Online"
            },
            {
                "type": "0",
                "value": "1",
                "newvalue": "Offline"
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "valuemapids": [
            "2"
        ]
    },
    "id": 1
}
```

[comment]: # ({/1b91e008-7b3b6bc4})

[comment]: # ({fc02e5cf-fc02e5cf})
### Source

CValueMap::update() in *ui/include/classes/api/services/CValueMap.php*.

[comment]: # ({/fc02e5cf-fc02e5cf})
