[comment]: # ({ca1ffead-f9e8f670})
# userdirectory.update

[comment]: # ({/ca1ffead-f9e8f670})

[comment]: # ({e12dcb91-b5ff62ed})
### Description

`object userdirectory.update(object/array userDirectory)`

This method allows to update existing user directories.

::: noteclassic
This method is only available to *Super admin* user type.
:::

[comment]: # ({/e12dcb91-b5ff62ed})

[comment]: # ({2530a9ee-d99c84a0})
### Parameters

`(object/array)` [User directory properties](object#user-directory) to be updated.

The `userdirectoryid` property must be defined for each user directory, all other properties are optional.
Only the passed properties will be updated, all others will remain unchanged.

[comment]: # ({/2530a9ee-d99c84a0})

[comment]: # ({e5397fc6-e6134a80})
### Return values

`(object)` Returns an object containing the IDs of the updated user directories
under the `userdirectoryids` property.

[comment]: # ({/e5397fc6-e6134a80})

[comment]: # ({b41637d2-8e924efa})
### Examples

[comment]: # ({/b41637d2-8e924efa})

[comment]: # ({4e45c74e-27b63737})
##### Update bind password for user directory

Set new bind password for a user directory.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "userdirectory.update",
    "params": {
        "userdirectoryid": "3",
        "bind_password": "newldappassword"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "userdirectoryids": [
            "3"
        ]
    },
    "id": 1
}
```

[comment]: # ({/4e45c74e-27b63737})

[comment]: # ({076c9f2e-7e502f9e})
##### Update mappings for user directory

Update provisioning groups mappings and media type mappings for user directory "2".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "userdirectory.update",
    "params": {
        "userdirectoryid": "2",
        "provision_media": [
                {
                    "name": "example.com",
                    "mediatypeid": "1",
                    "attribute": "admin@example.com"
                }
            ],
            "provision_groups": [
                {
                    "name": "Zabbix administrators",
                    "roleid": "2",
                    "user_groups": [
                        {
                            "usrgrpid": "7"
                        },
                        {
                            "usrgrpid": "8"
                        },
                        {
                            "usrgrpid": "11"
                        }
                    ]
                }
            ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "userdirectoryids": [
            "2"
        ]
    },
    "id": 1
}
```

[comment]: # ({/076c9f2e-7e502f9e})

[comment]: # ({954e7426-33fc154f})
### Source

CUserDirectory::update() in *ui/include/classes/api/services/CUserDirectory.php*.

[comment]: # ({/954e7426-33fc154f})
