[comment]: # ({53d969c5-f9e8f670})
# userdirectory.get

[comment]: # ({/53d969c5-f9e8f670})

[comment]: # ({f09e8f55-b5ff62ed})
### Description

`integer/array userdirectory.get(object parameters)`

The method allows to retrieve user directories according to the given parameters.

::: noteclassic
This method is only available to *Super admin* user types.
:::

[comment]: # ({/f09e8f55-b5ff62ed})

[comment]: # ({e3214b35-d99c84a0})
### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|userdirectoryids|string/array|Return only user directories with the given IDs.|
|selectUsrgrps|query|Return a `usrgrps` property with [user groups](/manual/api/reference/usergroup/object) associated with a user directory.<br><br>Supports `count`.|
|selectProvisionMedia|query|Return a `provision_media` property with [media type mappings](/manual/api/reference/userdirectory/object#media-type-mappings) associated with a user directory.|
|selectProvisionGroups|query|Return a `provision_groups` property with [provisioning groups mappings](/manual/api/reference/userdirectory/object#provisioning-groups-mappings) associated with a user directory.|
|sortfield|string/array|Sort the result by the given properties.<br><br>Possible values: `name`, `host`.|
|filter|object|Return only those results that exactly match the given filter.<br><br>Accepts an object, where the keys are property names, and the values are either a single value or an array of values.<br><br>Supported keys: `userdirectoryid`, `idp_type`, `provision_status`.|
|search|object|Return results that match the given pattern (case-insensitive).<br><br>Possible values: `name`, `description`.<br><br>User directory of type SAML will have an empty value for both `name` and `description` fields. Both fields can be changed with `userdirectory.update` operation.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters).|
|excludeSearch|boolean|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

[comment]: # ({/e3214b35-d99c84a0})

[comment]: # ({7223bab1-a18ff82b})
### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.

[comment]: # ({/7223bab1-a18ff82b})

[comment]: # ({b41637d2-ba1c8d4c})
### Examples

[comment]: # ({/b41637d2-ba1c8d4c})

[comment]: # ({0410a52f-6acef378})
##### Retrieving user directories

Retrieve all user directories with additional properties that display media type mappings and provisioning groups mappings associated with each user directory.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "userdirectory.get",
    "params": {
        "output": "extend",
        "selectProvisionMedia": "extend",
        "selectProvisionGroups": "extend"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": [
       {
            "userdirectoryid": "1",
            "idp_type": "2",
            "name": "",
            "provision_status": "1",
            "description": "",
            "group_name": "groups",
            "user_username": "",
            "user_lastname": "",
            "idp_entityid": "http://example.com/simplesaml/saml2/idp/metadata.php",
            "sso_url": "http://example.com/simplesaml/saml2/idp/SSOService.php",
            "slo_url": "",
            "username_attribute": "uid",
            "sp_entityid": "zabbix",
            "nameid_format": "",
            "sign_messages": "0",
            "sign_assertions": "0",
            "sign_authn_requests": "0",
            "sign_logout_requests": "0",
            "sign_logout_responses": "0",
            "encrypt_nameid": "0",
            "encrypt_assertions": "0",
            "scim_status": "1",
            "provision_media": [
                {
                    "name": "example.com",
                    "mediatypeid": "1",
                    "attribute": "user@example.com"
                }
            ],
            "provision_groups": [
                {
                    "name": "*",
                    "roleid": "1",
                    "user_groups": [
                        {
                            "usrgrpid": "13"
                        }
                    ]
                }
            ]
        },
        {
            "userdirectoryid": "2",
            "idp_type": "1",
            "name": "AD server",
            "provision_status": "1",
            "description": "",
            "host": "host.example.com",
            "port": "389",
            "base_dn": "DC=zbx,DC=local",
            "search_attribute": "sAMAccountName",
            "bind_dn": "CN=Admin,OU=Users,OU=Zabbix,DC=zbx,DC=local",
            "start_tls": "0",
            "search_filter": "",
            "group_basedn": "OU=Zabbix,DC=zbx,DC=local",
            "group_name": "CN",
            "group_member": "member",
            "group_filter": "(%{groupattr}=CN=%{ref},OU=Users,OU=Zabbix,DC=zbx,DC=local)",
            "group_membership": "",
            "user_username": "givenName",
            "user_lastname": "sn",
            "user_ref_attr": "CN",
            "provision_media": [
                {
                    "name": "example.com",
                    "mediatypeid": "1",
                    "attribute": "user@example.com"
                }
            ],
            "provision_groups": [
                {
                    "name": "*",
                    "roleid": "4",
                    "user_groups": [
                        {
                            "usrgrpid": "8"
                        }
                    ]
                },
                {
                    "name": "Zabbix administrators",
                    "roleid": "2",
                    "user_groups": [
                        {
                            "usrgrpid": "7"
                        },
                        {
                            "usrgrpid": "8"
                        }
                    ]
                }
            ]
        },
        {
            "userdirectoryid": "3",
            "idp_type": "1",
            "name": "LDAP API server #1",
            "provision_status": "0",
            "description": "",
            "host": "ldap://local.ldap",
            "port": "389",
            "base_dn": "ou=Users,dc=example,dc=org",
            "search_attribute": "uid",
            "bind_dn": "cn=ldap_search,dc=example,dc=org",
            "start_tls": "1",
            "search_filter": "",
            "group_basedn": "",
            "group_name": "",
            "group_member": "",
            "group_filter": "",
            "group_membership": "",
            "user_username": "",
            "user_lastname": "",
            "user_ref_attr": "",
            "provision_media": [],
            "provision_groups": []
        }
    ],
    "id": 1
}
```

[comment]: # ({/0410a52f-6acef378})

[comment]: # ({860ec55f-5f3bc200})
### See also

-   [User group](/manual/api/reference/usergroup/object#user)

[comment]: # ({/860ec55f-5f3bc200})

[comment]: # ({901c82dd-392498dd})
### Source

CUserDirectory::get() in *ui/include/classes/api/services/CUserDirectory.php*.

[comment]: # ({/901c82dd-392498dd})
