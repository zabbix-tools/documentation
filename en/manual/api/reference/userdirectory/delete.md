[comment]: # ({f61ef399-f9e8f670})
# userdirectory.delete

[comment]: # ({/f61ef399-f9e8f670})

[comment]: # ({06f84879-b5ff62ed})
### Description

`object userdirectory.delete(array userDirectoryIds)`

This method allows to delete user directories. User directory cannot be deleted when it is directly used for at least one user group.\
Default LDAP user directory cannot be deleted when `authentication.ldap_configured` is set to 1 or when there are more user directories left.

::: noteclassic
This method is only available to *Super admin* user type.
:::

[comment]: # ({/06f84879-b5ff62ed})

[comment]: # ({1e1862ff-90e6a45f})
### Parameters

`(array)` IDs of the user directories to delete.

[comment]: # ({/1e1862ff-90e6a45f})

[comment]: # ({71d313b7-e48d17a6})
### Return values

`(object)` Returns an object containing the IDs of the deleted user directories under the `userdirectoryids` property.

[comment]: # ({/71d313b7-e48d17a6})

[comment]: # ({b41637d2-94dc6263})
### Examples

[comment]: # ({/b41637d2-94dc6263})

[comment]: # ({93ba09bc-ec4f71a7})
#### Deleting multiple user directories

Delete two user directories.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "userdirectory.delete",
    "params": [
        "2",
        "12"
    ],
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "userdirectoryids": [
            "2",
            "12"
        ]
    },
    "id": 1
}
```

[comment]: # ({/93ba09bc-ec4f71a7})

[comment]: # ({7d9028d5-1eee2f52})
### Source

CUserDirectory::delete() in *ui/include/classes/api/services/CUserDirectory.php*.

[comment]: # ({/7d9028d5-1eee2f52})
