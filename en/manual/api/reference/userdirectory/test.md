[comment]: # ({caf91c6b-f9e8f670})
# userdirectory.test

[comment]: # ({/caf91c6b-f9e8f670})

[comment]: # ({99f1236b-b5ff62ed})
### Description

`object userdirectory.test(array userDirectory)`

This method allows to test user directory connection settings.

::: noteclassic
This method also allows to test what configured data matches the user directory settings for user provisioning
(e.g., what user role, user groups, user medias will be assigned to the user).
For this type of test the API request should be made for a [user directory](/manual/api/reference/userdirectory/object#userdirectory) that has `provision_status` set to enabled.
:::

::: noteclassic
This method is only available to *Super admin* user type.
:::

[comment]: # ({/99f1236b-b5ff62ed})

[comment]: # ({01dc85f0-d99c84a0})
### Parameters

`(object)` User directory properties.

Since `userdirectory.get` API does not return `bind_password` field, `userdirectoryid` and/or `bind_password` should be supplied.\
Additionally to the [standard user directory properties](object#userdirectory), the method accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|test_username|string|Username to test in user directory.|
|test_password|string|Username associated password to test in user directory.|

[comment]: # ({/01dc85f0-d99c84a0})

[comment]: # ({0a19e6a5-99a75467})
### Return values

`(bool)` Returns true on success.

[comment]: # ({/0a19e6a5-99a75467})

[comment]: # ({b41637d2-47b9eb66})
### Examples

[comment]: # ({/b41637d2-47b9eb66})

[comment]: # ({0ec66727-9f73d089})
##### Test user directory for existing user

Test user directory "3" for "user1".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "userdirectory.test",
    "params": {
        "userdirectoryid": "3",
        "host": "127.0.0.1",
        "port": "389",
        "base_dn": "ou=Users,dc=example,dc=org",
        "search_attribute": "uid",
        "bind_dn": "cn=ldap_search,dc=example,dc=org",
        "bind_password": "password",
        "test_username": "user1",
        "test_password": "password"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": true,
    "id": 1
}
```

[comment]: # ({/0ec66727-9f73d089})

[comment]: # ({fa573532-a8a25347})
##### Test user directory for non-existing user

Test user directory "3" for non-existing "user2".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "userdirectory.test",
    "params": {
        "userdirectoryid": "3",
        "host": "127.0.0.1",
        "port": "389",
        "base_dn": "ou=Users,dc=example,dc=org",
        "search_attribute": "uid",
        "bind_dn": "cn=ldap_search,dc=example,dc=org",
        "test_username": "user2",
        "test_password": "password"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "error": {
        "code": -32500,
        "message": "Application error.",
        "data": "Incorrect user name or password or account is temporarily blocked."
    },
    "id": 1
}
```

[comment]: # ({/fa573532-a8a25347})

[comment]: # ({79a981ca-54c18b14})
##### Test user directory for user provisioning

Test userdirectory "3" for what configured data matches the user directory settings for "user3" provisioning
(e.g., what user role, user groups, user medias will be assigned to the user).

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "userdirectory.test",
    "params": {
        "userdirectoryid": "2",
        "host": "host.example.com",
        "port": "389",
        "base_dn": "DC=zbx,DC=local",
        "search_attribute": "sAMAccountName",
        "bind_dn": "CN=Admin,OU=Users,OU=Zabbix,DC=zbx,DC=local",
        "test_username": "user3",
        "test_password": "password"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "username": "user3",
        "name": "John",
        "surname": "Doe",
        "medias": [],
        "usrgrps": [
            {
                "usrgrpid": "8"
            },
            {
                "usrgrpid": "7"
            }
        ],
        "roleid": "2",
        "userdirectoryid": "2"
    },
    "id": 1
}
```

[comment]: # ({/79a981ca-54c18b14})

[comment]: # ({e28d4bf9-0402a0f4})
### Source

CUserDirectory::test() in *ui/include/classes/api/services/CUserDirectory.php*.

[comment]: # ({/e28d4bf9-0402a0f4})
