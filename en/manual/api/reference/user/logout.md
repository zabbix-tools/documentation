[comment]: # ({860084e0-860084e0})
# user.logout

[comment]: # ({/860084e0-860084e0})

[comment]: # ({6bb0ca9b-d6ff52eb})
### Description

`string/object user.logout(array)`

This method allows to log out of the API and invalidates the current
authentication token.

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/6bb0ca9b-d6ff52eb})

[comment]: # ({4fa8a419-4fa8a419})
### Parameters

`(array)` The method accepts an empty array.

[comment]: # ({/4fa8a419-4fa8a419})

[comment]: # ({66018290-66018290})
### Return values

`(boolean)` Returns `true` if the user has been logged out successfully.

[comment]: # ({/66018290-66018290})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({1d5720be-b5326b2b})
#### Logging out

Log out from the API.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "user.logout",
    "params": [],
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": true,
    "id": 1
}
```

[comment]: # ({/1d5720be-b5326b2b})

[comment]: # ({02f79c40-02f79c40})
### See also

-   [user.login](login)

[comment]: # ({/02f79c40-02f79c40})

[comment]: # ({7fd9f53c-7fd9f53c})
### Source

CUser::login() in *ui/include/classes/api/services/CUser.php*.

[comment]: # ({/7fd9f53c-7fd9f53c})
