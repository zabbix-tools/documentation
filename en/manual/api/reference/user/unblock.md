[comment]: # ({8790eaaa-8790eaaa})
# user.unblock

[comment]: # ({/8790eaaa-8790eaaa})

[comment]: # ({60286833-ae802b92})
### Description

`object user.unblock(array userids)`

This method allows to unblock users.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/60286833-ae802b92})

[comment]: # ({cdc313c2-cdc313c2})
### Parameters

`(array)` IDs of users to unblock.

[comment]: # ({/cdc313c2-cdc313c2})

[comment]: # ({751d77e0-751d77e0})
### Return values

`(object)` Returns an object containing the IDs of the unblocked users
under the `userids` property.

[comment]: # ({/751d77e0-751d77e0})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({40ffcaf8-2d447a22})
#### Unblocking multiple users

Unblock two users.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "user.unblock",
    "params": [
        "1",
        "5"
    ],
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "userids": [
            "1",
            "5"
        ]
    },
    "id": 1
}
```

[comment]: # ({/40ffcaf8-2d447a22})

[comment]: # ({e10b30be-e10b30be})
### Source

CUser::unblock() in *ui/include/classes/api/services/CUser.php*.

[comment]: # ({/e10b30be-e10b30be})
