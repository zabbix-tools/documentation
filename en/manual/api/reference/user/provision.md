[comment]: # ({733de228-82b6f6ff})
# user.provision

[comment]: # ({/733de228-82b6f6ff})

[comment]: # ({bec34a7c-a78ac238})
### Description

`object user.provision(object/array users)`

This method allows to provision LDAP users.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings.
See [User roles](/manual/web_interface/frontend_sections/users/user_roles) for more information.
:::

[comment]: # ({/bec34a7c-a78ac238})

[comment]: # ({c0aab0b4-d03ad838})
### Parameters

`(array)` IDs of users to provision.

[comment]: # ({/c0aab0b4-d03ad838})

[comment]: # ({bcb326f0-26affb31})
### Return values

`(object)` Returns an object containing the IDs of the provisioned users under the `userids` property.

[comment]: # ({/bcb326f0-26affb31})

[comment]: # ({b41637d2-b507f273})
### Examples

[comment]: # ({/b41637d2-b507f273})

[comment]: # ({5b6b0473-aef28656})
#### Provisioning multiple users

Provision two users.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "user.provision",
    "params": [
        "1",
        "5"
    ],
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "userids": [
            "1",
            "5"
        ]
    },
    "id": 1
}
```

[comment]: # ({/5b6b0473-aef28656})

[comment]: # ({287ccec1-ab3a6b91})
### Source

CUser::provision() in *ui/include/classes/api/services/CUser.php*.

[comment]: # ({/287ccec1-ab3a6b91})
