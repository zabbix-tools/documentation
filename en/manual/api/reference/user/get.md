[comment]: # ({2485e0f7-2485e0f7})
# user.get

[comment]: # ({/2485e0f7-2485e0f7})

[comment]: # ({5aeb1b12-36f89e39})
### Description

`integer/array user.get(object parameters)`

The method allows to retrieve users according to the given parameters.

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/5aeb1b12-36f89e39})

[comment]: # ({f5213696-888512e9})
### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|mediaids|string/array|Return only users that use the given media.|
|mediatypeids|string/array|Return only users that use the given media types.|
|userids|string/array|Return only users with the given IDs.|
|usrgrpids|string/array|Return only users that belong to the given user groups.|
|getAccess|flag|Adds additional information about user permissions.<br><br>Adds the following properties for each user:<br>`gui_access` - `(integer)` user's frontend authentication method. Refer to the `gui_access` property of the [user group object](/manual/api/reference/usergroup/object#user_group) for a list of possible values.<br>`debug_mode` - `(integer)` indicates whether debug is enabled for the user. Possible values: 0 - debug disabled, 1 - debug enabled.<br>`users_status` - `(integer)` indicates whether the user is disabled. Possible values: 0 - user enabled, 1 - user disabled.|
|selectMedias|query|Return media used by the user in the [`medias`](/manual/api/reference/user/object#media) property.|
|selectMediatypes|query|Return media types used by the user in the [`mediatypes`](/manual/api/reference/mediatype/object) property.|
|selectUsrgrps|query|Return user groups that the user belongs to in the [`usrgrps`](/manual/api/reference/usergroup/object) property.|
|selectRole|query|Return user role in the [`role`](/manual/api/reference/role/object#role) property.|
|sortfield|string/array|Sort the result by the given properties.<br><br>Possible values: `userid`, `username`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters).|
|editable|boolean|^|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

[comment]: # ({/f5213696-888512e9})

[comment]: # ({7223bab1-7223bab1})
### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.

[comment]: # ({/7223bab1-7223bab1})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({6778ddec-7b5c610b})
#### Retrieving users

Retrieve all of the configured users.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "user.get",
    "params": {
        "output": "extend"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": [
        {
            "userid": "1",
            "username": "Admin",
            "name": "Zabbix",
            "surname": "Administrator",
            "url": "",
            "autologin": "1",
            "autologout": "0",
            "lang": "en_GB",
            "refresh": "0s",
            "theme": "default",
            "attempt_failed": "0",
            "attempt_ip": "",
            "attempt_clock": "0",
            "rows_per_page": "50",
            "timezone": "default",
            "roleid": "3",
            "userdirectoryid": "0",
            "ts_provisioned": "0"
        },
        {
            "userid": "2",
            "username": "guest",
            "name": "",
            "surname": "",
            "url": "",
            "autologin": "0",
            "autologout": "15m",
            "lang": "default",
            "refresh": "30s",
            "theme": "default",
            "attempt_failed": "0",
            "attempt_ip": "",
            "attempt_clock": "0",
            "rows_per_page": "50",
            "timezone": "default",
            "roleid": "4",
            "userdirectoryid": "0",
            "ts_provisioned": "0"
        },
        {
            "userid": "3",
            "username": "user",
            "name": "Zabbix",
            "surname": "User",
            "url": "",
            "autologin": "0",
            "autologout": "0",
            "lang": "ru_RU",
            "refresh": "15s",
            "theme": "dark-theme",
            "attempt_failed": "0",
            "attempt_ip": "",
            "attempt_clock": "0",
            "rows_per_page": "100",
            "timezone": "default",
            "roleid": "1",
            "userdirectoryid": "0",
            "ts_provisioned": "0"
        }
    ],
    "id": 1
}
```

[comment]: # ({/6778ddec-7b5c610b})

[comment]: # ({2c8d809a-d85f9654})
#### Retrieving user data

Retrieve data of a user with ID "12".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "user.get",
    "params": {
        "output": ["userid", "username"],
        "selectRole": "extend",
        "userids": "12"
    },
    "id": 1
}
```

Response:

```json
{
  "jsonrpc": "2.0",
  "result": [
      {
          "userid": "12",
          "username": "John",
          "role": {
              "roleid": "5",
              "name": "Operator",
              "type": "1",
              "readonly": "0"
          }
      }
  ],
  "id": 1
}
```

[comment]: # ({/2c8d809a-d85f9654})

[comment]: # ({8d892781-8d892781})
### See also

-   [Media](/manual/api/reference/user/object#media)
-   [Media type](/manual/api/reference/mediatype/object#media_type)
-   [User group](/manual/api/reference/usergroup/object#user_group)
-   [Role](/manual/api/reference/role/object#role)

[comment]: # ({/8d892781-8d892781})

[comment]: # ({842f2a42-842f2a42})
### Source

CUser::get() in *ui/include/classes/api/services/CUser.php*.

[comment]: # ({/842f2a42-842f2a42})
