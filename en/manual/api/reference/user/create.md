[comment]: # ({a79da37c-a79da37c})
# user.create

[comment]: # ({/a79da37c-a79da37c})

[comment]: # ({6269f520-7dc5480b})
### Description

`object user.create(object/array users)`

This method allows to create new users.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

::: noteclassic
The strength of user password is validated according the
password policy rules defined by Authentication API. See [Authentication
API](/manual/api/reference/authentication) for more
information.
:::

[comment]: # ({/6269f520-7dc5480b})

[comment]: # ({9f19c27d-9e6b7fba})
### Parameters

`(object/array)` Users to create.

Additionally to the [standard user properties](object#user), the method
accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|usrgrps|array|User [groups](/manual/api/reference/user/create) to add the user to.<br><br>The user groups must have the `usrgrpid` property defined.|
|medias|array|User [media](/manual/api/reference/user/object#media) to be created.|

[comment]: # ({/9f19c27d-9e6b7fba})

[comment]: # ({7e32f5f9-7e32f5f9})
### Return values

`(object)` Returns an object containing the IDs of the created users
under the `userids` property. The order of the returned IDs matches the
order of the passed users.

[comment]: # ({/7e32f5f9-7e32f5f9})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({37809083-0df8c129})
#### Creating a user

Create a new user, add him to a user group and create a new media for
him.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "user.create",
    "params": {
        "username": "John",
        "passwd": "Doe123",
        "roleid": "5",
        "usrgrps": [
            {
                "usrgrpid": "7"
            }
        ],
        "medias": [
            {
                "mediatypeid": "1",
                "sendto": [
                    "support@company.com"
                ],
                "active": 0,
                "severity": 63,
                "period": "1-7,00:00-24:00"
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "userids": [
            "12"
        ]
    },
    "id": 1
}
```

[comment]: # ({/37809083-0df8c129})

[comment]: # ({130699eb-130699eb})
### See also

-   [Authentication](/manual/api/reference/authentication)
-   [Media](/manual/api/reference/user/object#media)
-   [User group](/manual/api/reference/usergroup/object#user_group)
-   [Role](/manual/api/reference/role/object#role)

[comment]: # ({/130699eb-130699eb})

[comment]: # ({61de33e2-61de33e2})
### Source

CUser::create() in *ui/include/classes/api/services/CUser.php*.

[comment]: # ({/61de33e2-61de33e2})
