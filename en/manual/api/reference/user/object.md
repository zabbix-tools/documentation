[comment]: # ({0834cf88-0834cf88})
# > User object

The following objects are directly related to the `user` API.

[comment]: # ({/0834cf88-0834cf88})

[comment]: # ({863785f2-0ecc45be})
### User

The user object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|userid|string|ID of the user.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*<br>- *required* for update operations|
|username|string|User's name.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* for create operations<br>- *read-only* for [provisioned users](/manual/web_interface/frontend_sections/users/authentication#default-authentication) if the user is linked to a [user directory](/manual/api/reference/userdirectory/object) (`userdirectoryid` is set to a valid value that is not "0"), and user directory provisioning status is enabled (`provision_status` of [User directory object](/manual/api/reference/userdirectory/object#userdirectory) is set to "1"), and authentication status of all LDAP or SAML provisioning is enabled (`ldap_jit_status` of [Authentication object](/manual/api/reference/authentication/object#authentication) is set to "Enabled for configured LDAP IdPs" or `saml_jit_status` of [Authentication object](/manual/api/reference/authentication/object#authentication) is set to "Enabled for configured SAML IdPs")|
|passwd|string|User's password.<br><br>The value of this parameter can be an empty string if the user is linked to a [user directory](/manual/api/reference/userdirectory/object).<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *write-only*|
|roleid|string|Role ID of the user.|
|attempt\_clock|timestamp|Time of the last unsuccessful login attempt.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*|
|attempt\_failed|integer|Recent failed login attempt count.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*|
|attempt\_ip|string|IP address from where the last unsuccessful login attempt came from.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*|
|autologin|integer|Whether to enable auto-login.<br><br>Possible values:<br>0 - *(default)* auto-login disabled;<br>1 - auto-login enabled.|
|autologout|string|User session life time. Accepts seconds and time unit with suffix. If set to 0s, the session will never expire.<br><br>Default: 15m.|
|lang|string|Language code of the user's language, for example, `en_GB`.<br><br>Default: `default` - system default.|
|name|string|Name of the user.|
|refresh|string|Automatic refresh period. Accepts seconds and time unit with suffix.<br><br>Default: 30s.|
|rows\_per\_page|integer|Amount of object rows to show per page.<br><br>Default: 50.|
|surname|string|Surname of the user.|
|theme|string|User's theme.<br><br>Possible values:<br>`default` - *(default)* system default;<br>`blue-theme` - Blue;<br>`dark-theme` - Dark.|
|ts_provisioned|timestamp|Time when the latest [provisioning](/manual/api/reference/user/provision) operation was made.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*|
|url|string|URL of the page to redirect the user to after logging in.|
|userdirectoryid|string|ID of the [user directory](/manual/api/reference/userdirectory/object) that the user in linked to.<br><br>Used for provisioning (creating or updating), as well as to login a user that is linked to a user directory.<br><br>For login operations the value of this property will have priority over the `userdirectoryid` property of [user groups](/manual/api/reference/usergroup/object#user-group) that the user belongs to.<br><br>Default: 0.|
|timezone|string|User's time zone, for example, `Europe/London`, `UTC`.<br><br>Default: `default` - system default.<br><br>For the full list of supported time zones please refer to [PHP documentation](https://www.php.net/manual/en/timezones.php).|

[comment]: # ({/863785f2-0ecc45be})

[comment]: # ({fbfe2203-1ad487be})
### Media

The media object has the following properties.

|Property|Type|Description|
|--|--|------|
|mediatypeid|string|ID of the media type used by the media.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|sendto|string/array|Address, user name or other identifier of the recipient.<br><br>If `type` of [Media type](/manual/api/reference/mediatype/object#mediatype) is set to "Email", values are represented as array. For other types of [Media types](/manual/api/reference/mediatype/object#mediatype), value is represented as a string.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|active|integer|Whether the media is enabled.<br><br>Possible values:<br>0 - *(default)* enabled;<br>1 - disabled.|
|severity|integer|Trigger severities to send notifications about.<br><br>Severities are stored in binary form with each bit representing the corresponding severity. For example, "12" equals "1100" in binary and means that notifications will be sent from triggers with severities "warning" and "average".<br><br>Refer to the [trigger object page](/manual/api/reference/trigger/object#trigger) for a list of supported trigger severities.<br><br>Default: 63.|
|period|string|Time when the notifications can be sent as a [time period](/manual/appendix/time_period) or user macros separated by a semicolon.<br><br>Default: 1-7,00:00-24:00.|

[comment]: # ({/fbfe2203-1ad487be})
