[comment]: # ({19701d17-19701d17})
# user.login

[comment]: # ({/19701d17-19701d17})

[comment]: # ({c647dda4-c647dda4})
### Description

`string/object user.login(object parameters)`

This method allows to log in to the API and generate an authentication
token.

::: notewarning
When using this method, you also need to do
[user.logout](/manual/api/reference/user/logout) to prevent the
generation of a large number of open session records.
:::

::: noteimportant
This method is only available to unauthenticated
users and must be called without the `auth` parameter in the JSON-RPC
request.
:::

[comment]: # ({/c647dda4-c647dda4})

[comment]: # ({40e0861c-68f0a066})
### Parameters

`(object)` Parameters containing the user name and password.

The method accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|password|string|User password.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required*|
|username|string|User name.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required*|
|userData|flag|Return information about the authenticated user.|

[comment]: # ({/40e0861c-68f0a066})

[comment]: # ({084c666b-bc924c62})
### Return values

`(string/object)` If the `userData` parameter is used, returns an object
containing information about the authenticated user.

Additionally to the [standard user properties](object#user), the
following information is returned:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|auth\_type|integer|Default authentication for the user.<br><br>Refer to the `authentication_type` property of the [Authentication object](/manual/api/reference/authentication/object#authentication-object) for a list of possible values.|
|debug\_mode|integer|Whether debug mode is enabled or disabled for the user.<br><br>Refer to the `debug_mode` property of the [User group object](/manual/api/reference/usergroup/object#user_group) for a list of possible values.|
|deprovisioned|boolean|Whether the user belongs to a [deprovisioned users group](/manual/web_interface/frontend_sections/users/authentication#configuration).|
|gui\_access|string|User's authentication method to the frontend.<br><br>Refer to the `gui_access` property of the [User group object](/manual/api/reference/usergroup/object#user_group) for a list of possible values.|
|secret|string|Random 32 characters string. Is generated on user login.|
|sessionid|string|Authentication token, which must be used in the following API requests.|
|type|integer|User type.<br><br>Refer to the `type` property of the [Role object](/manual/api/reference/role/object#role) for a list of possible values.|
|userip|string|IP address of the user.|

::: notetip
If a user has been successfully authenticated after one
or more failed attempts, the method will return the current values for
the `attempt_clock`, `attempt_failed` and `attempt_ip` properties and
then reset them.
:::

If the `userData` parameter is not used, the method returns an
authentication token.

::: notetip
The generated authentication token should be remembered
and used in the `auth` parameter of the following JSON-RPC requests. It
is also required when using HTTP authentication.
:::

[comment]: # ({/084c666b-bc924c62})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({ce05d68f-46abbe5a})
#### Authenticating a user

Authenticate a user.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "user.login",
    "params": {
        "username": "Admin",
        "password": "zabbix"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": "0424bd59b807674191e7d77572075f33",
    "id": 1
}
```

[comment]: # ({/ce05d68f-46abbe5a})

[comment]: # ({3bf01059-8448c8ab})
#### Requesting authenticated user's information

Authenticate and return additional information about the user.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "user.login",
    "params": {
        "username": "Admin",
        "password": "zabbix",
        "userData": true
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "userid": "1",
        "username": "Admin",
        "name": "Zabbix",
        "surname": "Administrator",
        "url": "",
        "autologin": "1",
        "autologout": "0",
        "lang": "ru_RU",
        "refresh": "0",
        "theme": "default",
        "attempt_failed": "0",
        "attempt_ip": "127.0.0.1",
        "attempt_clock": "1355919038",
        "rows_per_page": "50",
        "timezone": "Europe/Riga",
        "roleid": "3",
        "userdirectoryid": "0",
        "type": 3,
        "userip": "127.0.0.1",
        "debug_mode": 0,
        "gui_access": "0",
        "deprovisioned": false,
        "auth_type": 0,
        "sessionid": "5b56eee8be445e98f0bd42b435736e42",
        "secret": "cd0ba923319741c6586f3d866423a8f4"
    },
    "id": 1
}
```

[comment]: # ({/3bf01059-8448c8ab})

[comment]: # ({22a14fca-22a14fca})
### See also

-   [user.logout](logout)

[comment]: # ({/22a14fca-22a14fca})

[comment]: # ({7fd9f53c-7fd9f53c})
### Source

CUser::login() in *ui/include/classes/api/services/CUser.php*.

[comment]: # ({/7fd9f53c-7fd9f53c})
