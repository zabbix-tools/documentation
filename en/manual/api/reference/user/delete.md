[comment]: # ({bab9094f-bab9094f})
# user.delete

[comment]: # ({/bab9094f-bab9094f})

[comment]: # ({c62404c3-1f1b6511})
### Description

`object user.delete(array users)`

This method allows to delete users.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/c62404c3-1f1b6511})

[comment]: # ({213fafcd-213fafcd})
### Parameters

`(array)` IDs of users to delete.

[comment]: # ({/213fafcd-213fafcd})

[comment]: # ({17d395f4-17d395f4})
### Return values

`(object)` Returns an object containing the IDs of the deleted users
under the `userids` property.

[comment]: # ({/17d395f4-17d395f4})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({8da6abf8-783cb2af})
#### Deleting multiple users

Delete two users.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "user.delete",
    "params": [
        "1",
        "5"
    ],
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "userids": [
            "1",
            "5"
        ]
    },
    "id": 1
}
```

[comment]: # ({/8da6abf8-783cb2af})

[comment]: # ({8c5510d6-8c5510d6})
### Source

CUser::delete() in *ui/include/classes/api/services/CUser.php*.

[comment]: # ({/8c5510d6-8c5510d6})
