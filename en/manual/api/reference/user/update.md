[comment]: # ({084d9051-084d9051})
# user.update

[comment]: # ({/084d9051-084d9051})

[comment]: # ({131691fb-dbc24bad})
### Description

`object user.update(object/array users)`

This method allows to update existing users.

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

::: noteclassic
The strength of user password is validated according the
password policy rules defined by Authentication API. See [Authentication
API](/manual/api/reference/authentication) for more
information.
:::

[comment]: # ({/131691fb-dbc24bad})

[comment]: # ({7c1f1655-3b478f18})
### Parameters

`(object/array)` User properties to be updated.

The `userid` property must be defined for each user, all other
properties are optional. Only the passed properties will be updated, all
others will remain unchanged.

Additionally to the [standard user properties](object#user), the method
accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|current_passwd|string|User's current password.<br><br>The value of this parameter can be an empty string if:<br>- the user is linked to a [user directory](/manual/api/reference/userdirectory/object);<br>- a user that is of *Super admin* type is changing password for a different user.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *write-only*<br>- *required* if `passwd` of [User object](object#user) is set and user changes own user password|
|usrgrps|array|User [groups](/manual/api/reference/user/create) to replace existing user groups.<br><br>The user groups must have the `usrgrpid` property defined.|
|medias|array|[User media](/manual/api/reference/user/object#media) to replace existing media.|

[comment]: # ({/7c1f1655-3b478f18})

[comment]: # ({4c3fb877-4c3fb877})
### Return values

`(object)` Returns an object containing the IDs of the updated users
under the `userids` property.

[comment]: # ({/4c3fb877-4c3fb877})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({9e930c76-c0483e48})
#### Renaming a user

Rename a user to John Doe.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "user.update",
    "params": {
        "userid": "1",
        "name": "John",
        "surname": "Doe"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "userids": [
            "1"
        ]
    },
    "id": 1
}
```

[comment]: # ({/9e930c76-c0483e48})

[comment]: # ({d54d987f-7ae4c491})
#### Changing user role

Change a role of a user.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "user.update",
    "params": {
        "userid": "12",
        "roleid": "6"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "userids": [
            "12"
        ]
    },
    "id": 1
}
```

[comment]: # ({/d54d987f-7ae4c491})

[comment]: # ({1083dc26-1083dc26})
### See also

-   [Authentication](/manual/api/reference/authentication)

[comment]: # ({/1083dc26-1083dc26})

[comment]: # ({bbd5a07f-bbd5a07f})
### Source

CUser::update() in *ui/include/classes/api/services/CUser.php*.

[comment]: # ({/bbd5a07f-bbd5a07f})
