[comment]: # ({9d4a1084-9d4a1084})
# usergroup.update

[comment]: # ({/9d4a1084-9d4a1084})

[comment]: # ({e2d33943-9fdfde6c})
### Description

`object usergroup.update(object/array userGroups)`

This method allows to update existing user groups.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/e2d33943-9fdfde6c})

[comment]: # ({c52c1240-93a5d635})
### Parameters

`(object/array)` User group properties to be updated.

The `usrgrpid` property must be defined for each user group, all other
properties are optional. Only the passed properties will be updated, all
others will remain unchanged.

Additionally to the [standard user group properties](object#user_group),
the method accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|hostgroup_rights|object/array|Host group [permissions](/manual/api/reference/usergroup/object#permission) to replace the current permissions assigned to the user group.|
|templategroup_rights|object/array|Template group [permissions](/manual/api/reference/usergroup/object#permission) to replace the current permissions assigned to the user group.|
|tag\_filters|array|[Tag-based permissions](/manual/api/reference/usergroup/object#tag_based_permission) to assign to the user group.|
|users|object/array|[Users](/manual/api/reference/user/create) to add to the user group.<br><br>The user must have the `userid` property defined.|
|rights<br>(deprecated)|object/array|This parameter is deprecated, please use `hostgroup_rights` or `templategroup_rights` instead.<br>[Permissions](/manual/api/reference/usergroup/object#permission) to assign to the user group.|

[comment]: # ({/c52c1240-93a5d635})

[comment]: # ({c04afd7f-c04afd7f})
### Return values

`(object)` Returns an object containing the IDs of the updated user
groups under the `usrgrpids` property.

[comment]: # ({/c04afd7f-c04afd7f})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({4234cf5e-e1a75e7b})
#### Enabling a user group and updating permissions

Enable a user group and provide read-write access for it to host groups "2" and "4".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "usergroup.update",
    "params": {
        "usrgrpid": "17",
        "users_status": "0",
        "hostgroup_rights": [
            {
                "id": "2",
                "permission": 3
            },
            {
                "id": "4",
                "permission": 3
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "usrgrpids": [
            "17"
        ]
    },
    "id": 1
}
```

[comment]: # ({/4234cf5e-e1a75e7b})

[comment]: # ({d5c7ed4f-d5c7ed4f})
### See also

-   [Permission](object#permission)

[comment]: # ({/d5c7ed4f-d5c7ed4f})

[comment]: # ({09e213c5-09e213c5})
### Source

CUserGroup::update() in
*ui/include/classes/api/services/CUserGroup.php*.

[comment]: # ({/09e213c5-09e213c5})
