[comment]: # ({7e2bc778-7e2bc778})
# usergroup.delete

[comment]: # ({/7e2bc778-7e2bc778})

[comment]: # ({849042c4-c1ef47a5})
### Description

`object usergroup.delete(array userGroupIds)`

This method allows to delete user groups.

::: noteimportant
[Deprovisioned](/manual/web_interface/frontend_sections/users/authentication#configuration) users group
(the user group specified for `disabled_usrgrpid` in [Authentication](/manual/api/reference/authentication/object)) cannot be deleted.
:::

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings.
See [User roles](/manual/web_interface/frontend_sections/users/user_roles) for more information.
:::

[comment]: # ({/849042c4-c1ef47a5})

[comment]: # ({22b6416d-22b6416d})
### Parameters

`(array)` IDs of the user groups to delete.

[comment]: # ({/22b6416d-22b6416d})

[comment]: # ({dde57d64-dde57d64})
### Return values

`(object)` Returns an object containing the IDs of the deleted user
groups under the `usrgrpids` property.

[comment]: # ({/dde57d64-dde57d64})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({0572824b-156418d8})
#### Deleting multiple user groups

Delete two user groups.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "usergroup.delete",
    "params": [
        "20",
        "21"
    ],
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "usrgrpids": [
            "20",
            "21"
        ]
    },
    "id": 1
}
```

[comment]: # ({/0572824b-156418d8})

[comment]: # ({9a88f22f-9a88f22f})
### Source

CUserGroup::delete() in
*ui/include/classes/api/services/CUserGroup.php*.

[comment]: # ({/9a88f22f-9a88f22f})
