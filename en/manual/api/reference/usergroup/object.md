[comment]: # ({119c5103-119c5103})
# > User group object

The following objects are directly related to the `usergroup` API.

[comment]: # ({/119c5103-119c5103})

[comment]: # ({5e3a5a9d-3c2622e5})
### User group

The user group object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|usrgrpid|string|ID of the user group.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*<br>- *required* for update operations|
|name|string|Name of the user group.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* for create operations|
|debug\_mode|integer|Whether debug mode is enabled or disabled.<br><br>Possible values:<br>0 - *(default)* disabled;<br>1 - enabled.|
|gui\_access|integer|Frontend authentication method of the users in the group.<br><br>Possible values:<br>0 - *(default)* use the system default authentication method;<br>1 - use internal authentication;<br>2 - use LDAP authentication;<br>3 - disable access to the frontend.|
|users\_status|integer|Whether the user group is enabled or disabled.<br>For [deprovisioned](/manual/web_interface/frontend_sections/users/authentication#configuration) users, the user group cannot be enabled.<br><br>Possible values:<br>0 - *(default)* enabled;<br>1 - disabled.|
|userdirectoryid|string|User directory used for authentication.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `gui_access` is set to "use the system default authentication method" or "use LDAP authentication"|

[comment]: # ({/5e3a5a9d-3c2622e5})

[comment]: # ({fc5fdb98-ba46c802})
### Permission

The permission object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|id|string|ID of the host group or template group to add permission to.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|permission|integer|Access level to the host group or template group.<br><br>Possible values:<br>0 - access denied;<br>2 - read-only access;<br>3 - read-write access.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* for create operations|

[comment]: # ({/fc5fdb98-ba46c802})

[comment]: # ({7f94b53a-33ad932c})
### Tag-based permission

The tag-based permission object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|groupid|string|ID of the host group to add permission to.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|tag|string|Tag name.|
|value|string|Tag value.|

[comment]: # ({/7f94b53a-33ad932c})
