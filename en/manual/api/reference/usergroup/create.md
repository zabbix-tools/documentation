[comment]: # ({c40e8cd1-c40e8cd1})
# usergroup.create

[comment]: # ({/c40e8cd1-c40e8cd1})

[comment]: # ({1c2f9f54-c74b5711})
### Description

`object usergroup.create(object/array userGroups)`

This method allows to create new user groups.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/1c2f9f54-c74b5711})

[comment]: # ({b88e3dea-3b20a75b})
### Parameters

`(object/array)` User groups to create.

Additionally to the [standard user group properties](object#user_group),
the method accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|hostgroup_rights|object/array|Host group [permissions](/manual/api/reference/usergroup/object#permission) to assign to the user group.|
|templategroup_rights|object/array|Template group [permissions](/manual/api/reference/usergroup/object#permission) to assign to the user group.|
|tag\_filters|array|[Tag-based permissions](/manual/api/reference/usergroup/object#tag_based_permission) to assign to the user group.|
|users|object/array|[Users](/manual/api/reference/user/create) to add to the user group.<br><br>The user must have the `userid` property defined.|
|rights<br>(deprecated)|object/array|This parameter is deprecated, please use `hostgroup_rights` or `templategroup_rights` instead.<br>[Permissions](/manual/api/reference/usergroup/object#permission) to assign to the user group.|

[comment]: # ({/b88e3dea-3b20a75b})

[comment]: # ({19e4f0cc-19e4f0cc})
### Return values

`(object)` Returns an object containing the IDs of the created user
groups under the `usrgrpids` property. The order of the returned IDs
matches the order of the passed user groups.

[comment]: # ({/19e4f0cc-19e4f0cc})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({b48de358-61abd39a})
#### Creating a user group

Create a user group *Operation managers* with denied access to host group "2", and add a user to it.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "usergroup.create",
    "params": {
        "name": "Operation managers",
        "hostgroup_rights": {
            "id": "2",
            "permission": 0
        },
        "users": [
            {
                "userid": "12"
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "usrgrpids": [
            "20"
        ]
    },
    "id": 1
}
```

[comment]: # ({/b48de358-61abd39a})

[comment]: # ({d5c7ed4f-d5c7ed4f})
### See also

-   [Permission](object#permission)

[comment]: # ({/d5c7ed4f-d5c7ed4f})

[comment]: # ({2d7f8c71-2d7f8c71})
### Source

CUserGroup::create() in
*ui/include/classes/api/services/CUserGroup.php*.

[comment]: # ({/2d7f8c71-2d7f8c71})
