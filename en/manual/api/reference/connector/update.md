[comment]: # ({0ba948d5-0f96d4e3})
# connector.update

[comment]: # ({/0ba948d5-0f96d4e3})

[comment]: # ({d595df91-c5c262c6})
### Description

`object connector.update(object/array connectors)`

This method allows to update existing connectors.

::: noteclassic
This method is only available to *Super admin* user type. Permissions to call the method can be revoked in user role settings.
See [User roles](/manual/web_interface/frontend_sections/users/user_roles) for more information.
:::

[comment]: # ({/d595df91-c5c262c6})

[comment]: # ({82c39c7f-7604a8a7})
### Parameters

`(object/array)` Connector properties to be updated.

The `connectorid` property must be defined for each connector, all other properties are optional.
Only the passed properties will be updated, all others will remain unchanged.

Additionally to the [standard connector properties](object#connector), the method accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|tags|array|Connector [tag filter](/manual/api/reference/connector/object#tag-filter) to replace the current tag filter.|

[comment]: # ({/82c39c7f-7604a8a7})

[comment]: # ({ce3e1358-aed16236})
### Return values

`(object)` Returns an object containing the IDs of the updated connectors under the `connectorids` property.

[comment]: # ({/ce3e1358-aed16236})

[comment]: # ({b41637d2-20154cf3})
### Examples

[comment]: # ({/b41637d2-20154cf3})

[comment]: # ({e1aaffff-4bc523df})
#### Changing HTTP authentication type

Change HTTP authentication type to Bearer for connector with ID "3".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "connector.update",
    "params": {
        "connectorid": 3,
        "authtype": 5,
        "token": "{$DATA_EXPORT_BEARER_TOKEN}"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "connectorids": [
            "3"
        ]
    },
    "id": 1
}
```

[comment]: # ({/e1aaffff-4bc523df})

[comment]: # ({246e1630-c1adf037})
#### Updating tag filter

Change tag filter for connector with ID "5".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "connector.update",
    "params": [
        {
            "connectorid": 5,
            "tags_evaltype": 2,
            "tags": [
                {
                    "tag": "service",
                    "operator": 0,
                    "value": "mysqld"
                },
                {
                    "tag": "error",
                    "operator": 12,
                    "value": ""
                }
            ]
        }
    ],
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "connectorids": [
            "5"
        ]
    },
    "id": 1
}
```

[comment]: # ({/246e1630-c1adf037})

[comment]: # ({37d671ce-f93b099e})
### Source

CConnector::update() in *ui/include/classes/api/services/CConnector.php*.

[comment]: # ({/37d671ce-f93b099e})
