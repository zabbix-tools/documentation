[comment]: # ({9f875ada-d96a863c})
# > Connector object

The following objects are directly related to the `connector` API.

[comment]: # ({/9f875ada-d96a863c})

[comment]: # ({61ee088b-bb03100b})
### Connector

The connector object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|connectorid|string|ID of the connector.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*<br>- *required* for update operations|
|name|string|Name of the connector.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* for create operations|
|url|integer|Endpoint URL, that is, URL of the receiver.<br>User macros are supported.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* for create operations|
|protocol|integer|Communication protocol.<br><br>Possible values:<br>0 - *(default)* Zabbix Streaming Protocol v1.0.|
|data_type|integer|Data type.<br><br>Possible values:<br>0 - *(default)* Item values;<br>1 - Events.|
|max\_records|integer|Maximum number of events or items that can be sent within one message.<br><br>Possible values: 0-2147483647 (max value of 32-bit signed integer).<br><br>Default: 0 - Unlimited.|
|max\_senders|integer|Number of sender processes to run for this connector.<br><br>Possible values: 1-100.<br><br>Default: 1.|
|max\_attempts|integer|Number of attempts.<br><br>Possible values: 1-5.<br><br>Default: 1.|
|timeout|string|Timeout.<br>Time suffixes are supported (e.g., 30s, 1m).<br>User macros are supported.<br><br>Possible values: 1s-60s.<br><br>Default: 5s.|
|http\_proxy|string|HTTP(S) proxy connection string given as <br>*\[protocol\]://\[username\[:password\]@\]proxy.example.com\[:port\]*.<br><br>User macros are supported.|
|authtype|integer|HTTP authentication method.<br><br>Possible values:<br>0 - *(default)* None;<br>1 - Basic;<br>2 - NTLM;<br>3 - Kerberos;<br>4 - Digest;<br>5 - Bearer.|
|username|string|User name.<br>User macros are supported.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `authtype` is set to "Basic", "NTLM", "Kerberos", or "Digest"|
|password|string|Password.<br>User macros are supported.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `authtype` is set to "Basic", "NTLM", "Kerberos", or "Digest"|
|token|string|Bearer token.<br>User macros are supported.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `authtype` is set to "Bearer"|
|verify\_peer|integer|Whether to validate the host name matches with the URLs provided in the *Common Name* or *Subject Alternate Name* fields of the host certificate.<br><br>Possible values:<br>0 - Do not validate;<br>1 - *(default)* Validate.|
|verify\_host|integer|Whether to validate the authenticity of the host certificate.<br><br>Possible values:<br>0 - Do not validate;<br>1 - *(default)* Validate.|
|ssl\_cert\_file|integer|Public SSL Key file path.<br>User macros are supported.|
|ssl\_key\_file|integer|Private SSL Key file path.<br>User macros are supported.|
|ssl\_key\_password|integer|Password for SSL Key file.<br>User macros are supported.|
|description|string|Description of the connector.|
|status|integer|Whether the connector is enabled.<br><br>Possible values:<br>0 - Disabled;<br>1 - *(default)* Enabled.|
|tags\_evaltype|integer|Tag evaluation method.<br><br>Possible values:<br>0 - *(default)* And/Or;<br>2 - Or.|

[comment]: # ({/61ee088b-bb03100b})

[comment]: # ({2bce9536-228356ed})
### Tag filter

Tag filter allows to export only matching item values or events. If not set then everything will be exported.
The tag filter object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|tag|string|Tag name.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|operator|integer|Condition operator.<br><br>Possible values:<br>0 - *(default)* Equals;<br>1 - Does not equal;<br>2 - Contains;<br>3 - Does not contain;<br>12 - Exists;<br>1 - Does not exist.|
|value|string|Tag value.|

[comment]: # ({/2bce9536-228356ed})
