[comment]: # ({5c2c8c32-2a00571f})
# connector.create

[comment]: # ({/5c2c8c32-2a00571f})

[comment]: # ({abae348f-bad67477})
### Description

`object connector.create(object/array connectors)`

This method allows to create new connector objects.

::: noteclassic
This method is only available to *Super admin* user type. Permissions to call the method can be revoked in user role settings.
See [User roles](/manual/web_interface/frontend_sections/users/user_roles) for more information.
:::

[comment]: # ({/abae348f-bad67477})

[comment]: # ({01fae15a-98722c06})
### Parameters

`(object/array)` Connector objects to create.

Additionally to the [standard connector properties](object#connector), the method accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|tags|array|Connector [tag filter](/manual/api/reference/connector/object#tag-filter).|

[comment]: # ({/01fae15a-98722c06})

[comment]: # ({d4487654-a513e07e})
### Return values

`(object)` Returns an object containing the IDs of the created connectors under the `connectorids` property.
The order of the returned IDs matches the order of the passed connectors.

[comment]: # ({/d4487654-a513e07e})

[comment]: # ({b41637d2-983d5841})
### Examples

[comment]: # ({/b41637d2-983d5841})

[comment]: # ({88c51e60-5936d90d})
#### Creating a connector

Create a connector to export trigger events with a tag filter. HTTP authentication will be performed using Bearer token.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "connector.create",
    "params": [
        {
            "name": "Export of events",
            "data_type": 1,
            "url": "{$DATA_EXPORT_URL}",
            "authtype": 5,
            "token": "{$DATA_EXPORT_BEARER_TOKEN}",
            "tags": [
                {
                    "tag": "service",
                    "operator": 0,
                    "value": "mysqld"
                }
            ]
        }
    ],
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "connectorid": [
            "3"
        ]
    },
    "id": 1
}
```

[comment]: # ({/88c51e60-5936d90d})

[comment]: # ({af9876f9-6550862d})
### Source

CConnector::create() in *ui/include/classes/api/services/CConnector.php*.

[comment]: # ({/af9876f9-6550862d})
