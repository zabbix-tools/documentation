[comment]: # ({1cabb47e-0fc7ba20})
# connector.get

[comment]: # ({/1cabb47e-0fc7ba20})

[comment]: # ({efd10e48-c152f697})
### Description

`integer/array connector.get(object parameters)`

The method allows to retrieve connector objects according to the given parameters.

::: noteclassic
This method is only available to *Super admin* user type. Permissions to call the method can be revoked in user role settings.
See [User roles](/manual/web_interface/frontend_sections/users/user_roles) for more information.
:::

[comment]: # ({/efd10e48-c152f697})

[comment]: # ({793dc81b-b096237f})
### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|connectorids|string/array|Return only connectors with the given IDs.|
|selectTags|query|Return a `tags` property with connector [tag filter](/manual/api/reference/connector/object#tag-filter).<br><br>Supports `count`.|
|sortfield|string/array|Sort the result by the given properties.<br><br>Possible values: `connectorid`, `name`, `data_type`, `status`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters).|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

[comment]: # ({/793dc81b-b096237f})

[comment]: # ({fbd15acf-830d8762})
### Return values

`(integer/array)` Returns either:

- an array of objects;
- the count of retrieved objects, if the `countOutput` parameter has been used.

[comment]: # ({/fbd15acf-830d8762})

[comment]: # ({b41637d2-122d6c45})
### Examples

[comment]: # ({/b41637d2-122d6c45})

[comment]: # ({9235ecca-3a1b6d17})
#### Retrieving all connectors

Retrieve all data about all connectors and their properties.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "connector.get",
    "params": {
        "output": "extend",
        "selectTags": ["tag", "operator", "value"],
        "preservekeys": true
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": [
        {
            "connectorid": "1",
            "name": "Export of item values",
            "protocol": "0",
            "data_type": "0",
            "url": "{$DATA_EXPORT_VALUES_URL}",
            "max_records": "0",
            "max_senders": "4",
            "max_attempts": "2",
            "timeout": "10s",
            "http_proxy": "{$DATA_EXPORT_VALUES_PROXY}",
            "authtype": "4",
            "username": "{$DATA_EXPORT_VALUES_USERNAME}",
            "password": "{$DATA_EXPORT_VALUES_PASSWORD}",
            "token": "",
            "verify_peer": "1",
            "verify_host": "1",
            "ssl_cert_file": "{$DATA_EXPORT_VALUES_SSL_CERT_FILE}",
            "ssl_key_file": "{$DATA_EXPORT_VALUES_SSL_KEY_FILE}",
            "ssl_key_password": "",
            "description": "",
            "status": "1",
            "tags_evaltype": "0",
            "tags": [
                {
                    "tag": "component",
                    "operator": "0",
                    "value": "memory"
                }
            ]
        },
        {
            "connectorid": "2",
            "name": "Export of events",
            "protocol": "0",
            "data_type": "1",
            "url": "{$DATA_EXPORT_EVENTS_URL}",
            "max_records": "0",
            "max_senders": "2",
            "max_attempts": "2",
            "timeout": "5s",
            "http_proxy": "",
            "authtype": "5",
            "username": "",
            "password": "",
            "token": "{$DATA_EXPORT_EVENTS_BEARER_TOKEN}",
            "verify_peer": "1",
            "verify_host": "1",
            "ssl_cert_file": "",
            "ssl_key_file": "",
            "ssl_key_password": "",
            "description": "",
            "status": "1",
            "tags_evaltype": "0",
            "tags": [
                {
                    "tag": "scope",
                    "operator": "0",
                    "value": "performance"
                }
            ]
        }
    ],
    "id": 1
}
```

[comment]: # ({/9235ecca-3a1b6d17})

[comment]: # ({572555ee-4963c0b1})
### Source

CConnector:get() in *ui/include/classes/api/services/CConnector.php*.

[comment]: # ({/572555ee-4963c0b1})
