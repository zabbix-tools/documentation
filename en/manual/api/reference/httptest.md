[comment]: # ({682f2335-7aa664d0})
# Web scenario

This class is designed to work with web scenarios.

Object references:

-   [Web scenario](/manual/api/reference/httptest/object#web_scenario)
-   [Web scenario tag](/manual/api/reference/httptest/object#web_scenario_tag)
-   [Scenario step](/manual/api/reference/httptest/object#scenario_step)
-   [HTTP field](/manual/api/reference/httptest/object#http_field)

Available methods:

-   [httptest.create](/manual/api/reference/httptest/create) - create new web scenarios
-   [httptest.delete](/manual/api/reference/httptest/delete) - delete web scenarios
-   [httptest.get](/manual/api/reference/httptest/get) - retrieve web scenarios
-   [httptest.update](/manual/api/reference/httptest/update) - update web scenarios

[comment]: # ({/682f2335-7aa664d0})
