[comment]: # ({8b1700c1-8b1700c1})
# > Script object

The following objects are directly related to the `script` API.

[comment]: # ({/8b1700c1-8b1700c1})

[comment]: # ({6ee8e193-9c6db006})
### Script

The script object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|scriptid|string|ID of the script.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*<br>- *required* for update operations|
|name|string|Name of the script.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* for create operations|
|type|integer|Script type.<br><br>Possible values if `scope` is set to "action operation":<br>0 - Script;<br>1 - IPMI;<br>2 - SSH;<br>3 - TELNET;<br>5 - Webhook.<br><br>Possible values if `scope` is set to "manual host action" or "manual event action":<br>6 - URL.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* for create operations|
|command|string|Command to run.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `type` is set to "Script", "IPMI", "SSH", "TELNET", or "Webhook"|
|scope|integer|Script scope.<br><br>Possible values:<br>1 - action operation;<br>2 - manual host action;<br>4 - manual event action.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* for create operations|
|execute\_on|integer|Where to run the script.<br><br>Possible values:<br>0 - run on Zabbix agent;<br>1 - run on Zabbix server;<br>2 - *(default)* run on Zabbix server (proxy).<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `type` is set to "Script"|
|menu\_path|string|Folders separated by slash that form a menu like navigation in frontend when clicked on host or event.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `scope` is set to "manual host action" or "manual event action"|
|authtype|integer|Authentication method used for SSH script type.<br><br>Possible values:<br>0 - password;<br>1 - public key.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `type` is set to "SSH"|
|username|string|User name used for authentication.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `type` is set to "SSH" or "TELNET"|
|password|string|Password used for SSH scripts with password authentication and TELNET scripts.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `type` is set to "SSH" and `authtype` is set to "password", or `type` is set to "TELNET"|
|publickey|string|Name of the public key file used for SSH scripts with public key authentication.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `type` is set to "SSH" and `authtype` is set to "public key"|
|privatekey|string|Name of the private key file used for SSH scripts with public key authentication.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `type` is set to "SSH" and `authtype` is set to "public key"|
|port|string|Port number used for SSH and TELNET scripts.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `type` is set to "SSH" or "TELNET"|
|groupid|string|ID of the host group that the script can be run on.<br><br>If set to "0", the script will be available on all host groups.<br><br>Default: 0.|
|usrgrpid|string|ID of the user group that will be allowed to run the script.<br><br>If set to "0", the script will be available for all user groups.<br><br>Default: 0.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `scope` is set to "manual host action" or "manual event action"|
|host\_access|integer|Host permissions needed to run the script.<br><br>Possible values:<br>2 - *(default)* read;<br>3 - write.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `scope` is set to "manual host action" or "manual event action"|
|confirmation|string|Confirmation pop up text.<br>The pop up will appear when trying to run the script from the Zabbix frontend.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `scope` is set to "manual host action" or "manual event action"|
|timeout|string|Webhook script execution timeout in seconds. Time suffixes are supported (e.g., 30s, 1m).<br><br>Possible values: 1-60s.<br><br>Default: 30s.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `type` is set to "Webhook"|
|parameters|array|Array of [webhook input parameters](/manual/api/reference/script/object#webhook-parameters).<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `type` is set to "Webhook"|
|description|string|Description of the script.|
|url|string|User defined URL.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `type` is set to "URL"|
|new_window|integer|Open URL in a new window.<br><br>Possible values:<br>0 - No;<br>1 - *(default)* Yes.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `type` is set to "URL"|

[comment]: # ({/6ee8e193-9c6db006})

[comment]: # ({24e86350-05b0e37d})
### Webhook parameters

Parameters passed to webhook script when it is called have the following
properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|name|string|Parameter name.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|value|string|Parameter value. Supports [macros](/manual/appendix/macros/supported_by_location).|

[comment]: # ({/24e86350-05b0e37d})

[comment]: # ({5ab53a02-587d2fd0})
### Debug

Debug information of executed webhook script. The debug object has the
following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|logs|array|Array of [log entries](/manual/api/reference/script/object#log-entry).|
|ms|string|Script execution duration in milliseconds.|

[comment]: # ({/5ab53a02-587d2fd0})

[comment]: # ({63401e0b-8ea23127})
### Log entry

The log entry object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|level|integer|Log level.|
|ms|string|The time elapsed in milliseconds since the script was run before log entry was added.|
|message|string|Log message.|

[comment]: # ({/63401e0b-8ea23127})
