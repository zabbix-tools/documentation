[comment]: # ({8961fc33-8961fc33})
# script.create

[comment]: # ({/8961fc33-8961fc33})

[comment]: # ({a2df1a0f-d7a51ffa})
### Description

`object script.create(object/array scripts)`

This method allows to create new scripts.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/a2df1a0f-d7a51ffa})

[comment]: # ({bcb9a453-bcb9a453})
### Parameters

`(object/array)` Scripts to create.

The method accepts scripts with the [standard script
properties](/manual/api/reference/script/object#script).

[comment]: # ({/bcb9a453-bcb9a453})

[comment]: # ({15216b8d-15216b8d})
### Return values

`(object)` Returns an object containing the IDs of the created scripts
under the `scriptids` property. The order of the returned IDs matches
the order of the passed scripts.

[comment]: # ({/15216b8d-15216b8d})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({655bc2d0-55d911d7})
#### Create a webhook script

Create a webhook script that sends HTTP request to external service.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "script.create",
    "params": {
        "name": "Webhook script",
        "command": "try {\n var request = new HttpRequest(),\n response,\n data;\n\n request.addHeader('Content-Type: application/json');\n\n response = request.post('https://localhost/post', value);\n\n try {\n response = JSON.parse(response);\n }\n catch (error) {\n response = null;\n }\n\n if (request.getStatus() !== 200 || !('data' in response)) {\n throw 'Unexpected response.';\n }\n\n data = JSON.stringify(response.data);\n\n Zabbix.log(3, '[Webhook Script] response data: ' + data);\n\n return data;\n}\ncatch (error) {\n Zabbix.log(3, '[Webhook Script] script execution failed: ' + error);\n throw 'Execution failed: ' + error + '.';\n}",
        "type": 5,
        "timeout": "40s",
        "parameters": [
            {
                "name": "token",
                "value": "{$WEBHOOK.TOKEN}"
            },
            {
                "name": "host",
                "value": "{HOST.HOST}"
            },
            {
                "name": "v",
                "value": "2.2"
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "scriptids": [
            "3"
        ]
    },
    "id": 1
}
```

[comment]: # ({/655bc2d0-55d911d7})

[comment]: # ({2c37940e-16a09744})
#### Create an SSH script

Create an SSH script with public key authentication that can be executed
on a host and has a context menu.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "script.create",
    "params": {
        "name": "SSH script",
        "command": "my script command",
        "type": 2,
        "username": "John",
        "publickey": "pub.key",
        "privatekey": "priv.key",
        "password": "secret",
        "port": "12345",
        "scope": 2,
        "menu_path": "All scripts/SSH",
        "usrgrpid": "7",
        "groupid": "4"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "scriptids": [
            "5"
        ]
    },
    "id": 1
}
```

[comment]: # ({/2c37940e-16a09744})

[comment]: # ({826d8673-0e2eb663})
#### Create a custom script

Create a custom script that will reboot a server. The script will
require write access to the host and will display a configuration
message before running in the frontend.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "script.create",
    "params": {
        "name": "Reboot server",
        "command": "reboot server 1",
        "confirmation": "Are you sure you would like to reboot the server?",
        "scope": 2,
        "type": 0
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "scriptids": [
            "4"
        ]
    },
    "id": 1
}
```

[comment]: # ({/826d8673-0e2eb663})

[comment]: # ({5a978c77-c363dc9b})
#### Create a URL type script

Create a URL type script that for host scope and remains in same window and has confirmation text.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "script.create",
    "params": {
        "name": "URL script",
        "type": 6,
        "scope": 2,
        "url": "http://zabbix/ui/zabbix.php?action=host.edit&hostid={HOST.ID}",
        "confirmation": "Edit host {HOST.NAME}?",
        "new_window": 0
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "scriptids": [
            "56"
        ]
    },
    "id": 1
}
```

[comment]: # ({/5a978c77-c363dc9b})

[comment]: # ({8c09f129-8c09f129})
### Source

CScript::create() in *ui/include/classes/api/services/CScript.php*.

[comment]: # ({/8c09f129-8c09f129})
