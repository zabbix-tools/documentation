[comment]: # ({ceca660a-ceca660a})
# script.delete

[comment]: # ({/ceca660a-ceca660a})

[comment]: # ({ad3a6f0d-db451e99})
### Description

`object script.delete(array scriptIds)`

This method allows to delete scripts.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/ad3a6f0d-db451e99})

[comment]: # ({17e2e442-17e2e442})
### Parameters

`(array)` IDs of the scripts to delete.

[comment]: # ({/17e2e442-17e2e442})

[comment]: # ({64908342-64908342})
### Return values

`(object)` Returns an object containing the IDs of the deleted scripts
under the `scriptids` property.

[comment]: # ({/64908342-64908342})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({7b361f53-8184d373})
#### Delete multiple scripts

Delete two scripts.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "script.delete",
    "params": [
        "3",
        "4"
    ],
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "scriptids": [
            "3",
            "4"
        ]
    },
    "id": 1
}
```

[comment]: # ({/7b361f53-8184d373})

[comment]: # ({ccc98e5e-ccc98e5e})
### Source

CScript::delete() in *ui/include/classes/api/services/CScript.php*.

[comment]: # ({/ccc98e5e-ccc98e5e})
