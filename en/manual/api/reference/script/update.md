[comment]: # ({4f2ac53b-4f2ac53b})
# script.update

[comment]: # ({/4f2ac53b-4f2ac53b})

[comment]: # ({3c9f3e07-3bbc6786})
### Description

`object script.update(object/array scripts)`

This method allows to update existing scripts.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/3c9f3e07-3bbc6786})

[comment]: # ({8f315bab-8f315bab})
### Parameters

`(object/array)` [Script properties](object#script) to be updated.

The `scriptid` property must be defined for each script, all other
properties are optional. Only the passed properties will be updated, all
others will remain unchanged. An exception is `type` property change
from 5 (Webhook) to other: the `parameters` property will be cleaned.

[comment]: # ({/8f315bab-8f315bab})

[comment]: # ({5062432a-5062432a})
### Return values

`(object)` Returns an object containing the IDs of the updated scripts
under the `scriptids` property.

[comment]: # ({/5062432a-5062432a})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({96417b97-4f3506ff})
#### Change script command

Change the command of the script to "/bin/ping -c 10 {HOST.CONN}
2>&1".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "script.update",
    "params": {
        "scriptid": "1",
        "command": "/bin/ping -c 10 {HOST.CONN} 2>&1"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "scriptids": [
            "1"
        ]
    },
    "id": 1
}
```

[comment]: # ({/96417b97-4f3506ff})

[comment]: # ({cded8a11-cded8a11})
### Source

CScript::update() in *ui/include/classes/api/services/CScript.php*.

[comment]: # ({/cded8a11-cded8a11})
