[comment]: # ({acf00dc8-acf00dc8})
# regexp.create

[comment]: # ({/acf00dc8-acf00dc8})

[comment]: # ({7f99f0b4-3c60fab5})
### Description

`object regexp.create(object/array regularExpressions)`

This method allows to create new global regular expressions.

::: noteclassic
This method is only available to *Super admin* user types.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/7f99f0b4-3c60fab5})

[comment]: # ({dbf2f6f9-f518ef9f})
### Parameters

`(object/array)` Regular expressions to create.

Additionally to the [standard properties](object), the method accepts
the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|expressions|array|[Expressions](/manual/api/reference/regexp/object#expressions) options.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required*|

[comment]: # ({/dbf2f6f9-f518ef9f})

[comment]: # ({f0011f1b-f0011f1b})
### Return values

`(object)` Returns an object containing the IDs of the created regular
expressions under the `regexpids` property.

[comment]: # ({/f0011f1b-f0011f1b})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({87169e24-3d653b4d})
#### Creating a new global regular expression.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "regexp.create",
    "params": {
      "name": "Storage devices for SNMP discovery",
      "test_string": "/boot",
      "expressions": [
        {
          "expression": "^(Physical memory|Virtual memory|Memory buffers|Cached memory|Swap space)$",
          "expression_type": "4",
          "case_sensitive": "1"
        }
      ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "regexpids": [
            "16"
        ]
    },
    "id": 1
}
```

[comment]: # ({/87169e24-3d653b4d})

[comment]: # ({cf9f5f19-cf9f5f19})
### Source

CRegexp::create() in *ui/include/classes/api/services/CRegexp.php*.

[comment]: # ({/cf9f5f19-cf9f5f19})
