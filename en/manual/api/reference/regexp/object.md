[comment]: # ({da0b13a3-da0b13a3})
# > Regular expression object

The following objects are directly related to the `regexp` API.

[comment]: # ({/da0b13a3-da0b13a3})

[comment]: # ({eea6b7c3-37784778})
### Regular expression

The global regular expression object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|regexpid|string|ID of the regular expression.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*<br>- *required* for update operations|
|name|string|Name of the regular expression.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* for create operations|
|test\_string|string|Test string.|

[comment]: # ({/eea6b7c3-37784778})

[comment]: # ({9623208a-ecdb1dfd})
### Expressions

The expressions object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|expression|string|Regular expression.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|expression\_type|integer|Type of Regular expression.<br><br>Possible values:<br>0 - Character string included;<br>1 - Any character string included;<br>2 - Character string not included;<br>3 - Result is TRUE;<br>4 - Result is FALSE.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|exp\_delimiter|string|Expression delimiter.<br><br>Default value: `","`.<br><br>Possible values: `","` or `"."`, or `"/"`.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `expression_type` is set to "Any character string included"|
|case\_sensitive|integer|Case sensitivity.<br><br>Default value: `0`.<br><br>Possible values:<br>0 - Case insensitive;<br>1 - Case sensitive.|

[comment]: # ({/9623208a-ecdb1dfd})
