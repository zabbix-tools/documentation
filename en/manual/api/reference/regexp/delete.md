[comment]: # ({c073dc7d-c073dc7d})
# regexp.delete

[comment]: # ({/c073dc7d-c073dc7d})

[comment]: # ({1b074d0b-702ce926})
### Description

`object regexp.delete(array regexpids)`

This method allows to delete global regular expressions.

::: noteclassic
This method is only available to *Super admin* user types.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/1b074d0b-702ce926})

[comment]: # ({99c30c43-99c30c43})
### Parameters

`(array)` IDs of the regular expressions to delete.

[comment]: # ({/99c30c43-99c30c43})

[comment]: # ({b583665a-b583665a})
### Return values

`(object)` Returns an object containing the IDs of the deleted regular
expressions under the `regexpids` property.

[comment]: # ({/b583665a-b583665a})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({6f2cf24a-13da0716})
#### Deleting multiple global regular expressions.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "regexp.delete",
    "params": [
        "16",
        "17"
    ],
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "regexpids": [
            "16",
            "17"
        ]
    },
    "id": 1
}
```

[comment]: # ({/6f2cf24a-13da0716})

[comment]: # ({e0390136-e0390136})
### Source

CRegexp::delete() in *ui/include/classes/api/services/CRegexp.php*.

[comment]: # ({/e0390136-e0390136})
