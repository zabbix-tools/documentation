[comment]: # ({ffe55246-ffe55246})
# regexp.update

[comment]: # ({/ffe55246-ffe55246})

[comment]: # ({80dc8e27-596faacb})
### Description

`object regexp.update(object/array regularExpressions)`

This method allows to update existing global regular expressions.

::: noteclassic
This method is only available to *Super admin* user types.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/80dc8e27-596faacb})

[comment]: # ({cf45243e-f26d83f8})
### Parameters

`(object/array)` Regular expression properties to be updated.

The `regexpid` property must be defined for each object, all other
properties are optional. Only the passed properties will be updated, all
others will remain unchanged.

Additionally to the [standard properties](object#expressions),
the method accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|expressions|array|[Expressions](/manual/api/reference/regexp/object#expressions) options.|

[comment]: # ({/cf45243e-f26d83f8})

[comment]: # ({7a7252f7-7a7252f7})
### Return values

`(object)` Returns an object containing the IDs of the updated regular
expressions under the `regexpids` property.

[comment]: # ({/7a7252f7-7a7252f7})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({4dcfac64-aef504e5})
#### Updating global regular expression for file systems discovery.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "regexp.update",
    "params": {
      "regexpid": "1",
      "name": "File systems for discovery",
      "test_string": "",
      "expressions": [
        {
          "expression": "^(btrfs|ext2|ext3|ext4|reiser|xfs|ffs|ufs|jfs|jfs2|vxfs|hfs|apfs|refs|zfs)$",
          "expression_type": "3",
          "exp_delimiter": ",",
          "case_sensitive": "0"
        },
        {
          "expression": "^(ntfs|fat32|fat16)$",
          "expression_type": "3",
          "exp_delimiter": ",",
          "case_sensitive": "0"
        }
      ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "regexpids": [
            "1"
        ]
    },
    "id": 1
}
```

[comment]: # ({/4dcfac64-aef504e5})

[comment]: # ({90c4873e-90c4873e})
### Source

CRegexp::update() in *ui/include/classes/api/services/CRegexp.php*.

[comment]: # ({/90c4873e-90c4873e})
