[comment]: # ({8ae78469-6bf4ac44})
# Media type

This class is designed to work with media types.

Object references:

-   [Media type](/manual/api/reference/mediatype/object#media_type)
-   [Webhook parameters](/manual/api/reference/mediatype/object#webhook_parameters)
-   [Script parameters](/manual/api/reference/mediatype/object#script_parameters)
-   [Message template](/manual/api/reference/mediatype/object#message_template)

Available methods:

-   [mediatype.create](/manual/api/reference/mediatype/create) - create new media types
-   [mediatype.delete](/manual/api/reference/mediatype/delete) - delete media types
-   [mediatype.get](/manual/api/reference/mediatype/get) - retrieve media types
-   [mediatype.update](/manual/api/reference/mediatype/update) - update media types

[comment]: # ({/8ae78469-6bf4ac44})
