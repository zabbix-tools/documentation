[comment]: # ({25145295-25145295})
# dcheck.get

[comment]: # ({/25145295-25145295})

[comment]: # ({ed6dc14d-cf2cd01b})
### Description

`integer/array dcheck.get(object parameters)`

The method allows to retrieve discovery checks according to the given
parameters.

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/ed6dc14d-cf2cd01b})

[comment]: # ({9a7af23e-3db1bd90})
### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|dcheckids|string/array|Return only discovery checks with the given IDs.|
|druleids|string/array|Return only discovery checks that belong to the given discovery rules.|
|dserviceids|string/array|Return only discovery checks that have detected the given discovered services.|
|sortfield|string/array|Sort the result by the given properties.<br><br>Possible values: `dcheckid`, `druleid`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters).|
|editable|boolean|^|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

[comment]: # ({/9a7af23e-3db1bd90})

[comment]: # ({7223bab1-7223bab1})
### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.

[comment]: # ({/7223bab1-7223bab1})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({fdd89e62-90f6afd9})
#### Retrieve discovery checks for a discovery rule

Retrieve all discovery checks used by discovery rule "6".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "dcheck.get",
    "params": {
        "output": "extend",
        "dcheckids": "6"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": [
        {
            "dcheckid": "6",
            "druleid": "4",
            "type": "3",
            "key_": "",
            "snmp_community": "",
            "ports": "21",
            "snmpv3_securityname": "",
            "snmpv3_securitylevel": "0",
            "snmpv3_authpassphrase": "",
            "snmpv3_privpassphrase": "",
            "uniq": "0",
            "snmpv3_authprotocol": "0",
            "snmpv3_privprotocol": "0",
            "snmpv3_contextname": "",
            "host_source": "1",
            "name_source": "0",
            "allow_redirect": "0"
        }
    ],
    "id": 1
}
```

[comment]: # ({/fdd89e62-90f6afd9})

[comment]: # ({d1a6b685-d1a6b685})
### Source

CDCheck::get() in *ui/include/classes/api/services/CDCheck.php*.

[comment]: # ({/d1a6b685-d1a6b685})
