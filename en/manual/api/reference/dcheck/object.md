[comment]: # ({a9b6b9e5-a9b6b9e5})
# > Discovery check object

The following objects are directly related to the `dcheck` API.

[comment]: # ({/a9b6b9e5-a9b6b9e5})

[comment]: # ({0cee6953-662afbc9})
### Discovery check

The discovery check object defines a specific check performed by a
network discovery rule. It has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|dcheckid|string|ID of the discovery check.|
|druleid|string|ID of the discovery rule that the check belongs to.|
|key\_|string|Item key (if `type` is set to "Zabbix agent") or SNMP OID (if `type` is set to "SNMPv1 agent", "SNMPv2 agent", or "SNMPv3 agent").<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `type` is set to "Zabbix agent", "SNMPv1 agent", "SNMPv2 agent", or "SNMPv3 agent"|
|ports|string|One or several port ranges to check, separated by commas.<br><br>Default: 0.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `type` is set to "SSH" (0), "LDAP" (1), "SMTP" (2), "FTP" (3), "HTTP" (4), "POP" (5), "NNTP" (6), "IMAP" (7), "TCP" (8), "Zabbix agent" (9), "SNMPv1 agent" (10), "SNMPv2 agent" (11), "SNMPv3 agent" (13), "HTTPS" (14), or "Telnet" (15)|
|snmp\_community|string|SNMP community.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `type` is set to "SNMPv1 agent" or "SNMPv2 agent"|
|snmpv3\_authpassphrase|string|Authentication passphrase.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `type` is set to "SNMPv3 agent" and `snmpv3_securitylevel` is set to "authNoPriv" or "authPriv"|
|snmpv3\_authprotocol|integer|Authentication protocol.<br><br>Possible values:<br>0 - *(default)* MD5;<br>1 - SHA1;<br>2 - SHA224;<br>3 - SHA256;<br>4 - SHA384;<br>5 - SHA512.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `type` is set to "SNMPv3 agent" and `snmpv3_securitylevel` is set to "authNoPriv" or "authPriv"|
|snmpv3\_contextname|string|SNMPv3 context name.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `type` is set to "SNMPv3 agent"|
|snmpv3\_privpassphrase|string|Privacy passphrase.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `type` is set to "SNMPv3 agent" and `snmpv3_securitylevel` is set to "authPriv"|
|snmpv3\_privprotocol|integer|Privacy protocol.<br><br>Possible values:<br>0 - *(default)* DES;<br>1 - AES128;<br>2 - AES192;<br>3 - AES256;<br>4 - AES192C;<br>5 - AES256C.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `type` is set to "SNMPv3 agent" and `snmpv3_securitylevel` is set to "authPriv"|
|snmpv3\_securitylevel|string|Security level.<br><br>Possible values:<br>0 - noAuthNoPriv;<br>1 - authNoPriv;<br>2 - authPriv.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `type` is set to "SNMPv3 agent"|
|snmpv3\_securityname|string|Security name.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `type` is set to "SNMPv3 agent"|
|type|integer|Type of check.<br><br>Possible values:<br>0 - SSH;<br>1 - LDAP;<br>2 - SMTP;<br>3 - FTP;<br>4 - HTTP;<br>5 - POP;<br>6 - NNTP;<br>7 - IMAP;<br>8 - TCP;<br>9 - Zabbix agent;<br>10 - SNMPv1 agent;<br>11 - SNMPv2 agent;<br>12 - ICMP ping;<br>13 - SNMPv3 agent;<br>14 - HTTPS;<br>15 - Telnet.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|uniq|integer|Whether to use this check as a device uniqueness criteria. Only a single unique check can be configured for a discovery rule.<br><br>Possible values:<br>0 - *(default)* do not use this check as a uniqueness criteria;<br>1 - use this check as a uniqueness criteria.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `type` is set to "Zabbix agent", "SNMPv1 agent", "SNMPv2 agent", or "SNMPv3 agent"|
|host\_source|integer|Source for host name.<br><br>Possible values:<br>1 - *(default)* DNS;<br>2 - IP;<br>3 - discovery value of this check.|
|name\_source|integer|Source for visible name.<br><br>Possible values:<br>0 - *(default)* not specified;<br>1 - DNS;<br>2 - IP;<br>3 - discovery value of this check.|
|allow\_redirect|integer|Allow situation where the target being ICMP pinged responds from a different IP address.<br><br>Possible values:<br>0 - *(default)* treat redirected responses as if the target host is down (fail);<br>1 - treat redirected responses as if the target host is up (success).<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `type` is set to "ICMP ping"|

[comment]: # ({/0cee6953-662afbc9})
