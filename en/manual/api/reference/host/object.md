[comment]: # ({b7ea8d04-b7ea8d04})
# > Host object

The following objects are directly related to the host API.

[comment]: # ({/b7ea8d04-b7ea8d04})

[comment]: # ({f677a3dd-2f6c5c76})
### Host

The host object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|hostid|string|ID of the host.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*<br>- *required* for update operations|
|host|string|Technical name of the host.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* for create operations|
|description|text|Description of the host.|
|flags|integer|Origin of the host.<br><br>Possible values:<br>0 - a plain host;<br>4 - a discovered host.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*|
|inventory\_mode|integer|Host inventory population mode.<br><br>Possible values:<br>-1 - *(default)* disabled;<br>0 - manual;<br>1 - automatic.|
|ipmi\_authtype|integer|IPMI authentication algorithm.<br><br>Possible values:<br>-1 - *(default)* default;<br>0 - none;<br>1 - MD2;<br>2 - MD5<br>4 - straight;<br>5 - OEM;<br>6 - RMCP+.|
|ipmi\_password|string|IPMI password.|
|ipmi\_privilege|integer|IPMI privilege level.<br><br>Possible values:<br>1 - callback;<br>2 - *(default)* user;<br>3 - operator;<br>4 - admin;<br>5 - OEM.|
|ipmi\_username|string|IPMI username.|
|maintenance\_from|timestamp|Starting time of the effective maintenance.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*|
|maintenance\_status|integer|Effective maintenance status.<br><br>Possible values:<br>0 - *(default)* no maintenance;<br>1 - maintenance in effect.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*|
|maintenance\_type|integer|Effective maintenance type.<br><br>Possible values:<br>0 - *(default)* maintenance with data collection;<br>1 - maintenance without data collection.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*|
|maintenanceid|string|ID of the maintenance that is currently in effect on the host.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*|
|name|string|Visible name of the host.<br><br>Default: `host` property value.|
|proxy\_hostid|string|ID of the proxy that is used to monitor the host.|
|status|integer|Status and function of the host.<br><br>Possible values:<br>0 - *(default)* monitored host;<br>1 - unmonitored host.|
|tls\_connect|integer|Connections to host.<br><br>Possible values:<br>1 - *(default)* No encryption;<br>2 - PSK;<br>4 - certificate.|
|tls\_accept|integer|Connections from host.<br>This is a bitmask field, any combination of possible bitmap values is acceptable.<br><br>Possible bitmap values:<br>1 - *(default)* No encryption;<br>2 - PSK;<br>4 - certificate.|
|tls\_issuer|string|Certificate issuer.|
|tls\_subject|string|Certificate subject.|
|tls\_psk\_identity|string|PSK identity.<br>Do not put sensitive information in the PSK identity, it is transmitted unencrypted over the network to inform a receiver which PSK to use.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *write-only*<br>- *required* if `tls_connect` is set to "PSK", or `tls_accept` contains the "PSK" bit|
|tls\_psk|string|The preshared key, at least 32 hex digits.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *write-only*<br>- *required* if `tls_connect` is set to "PSK", or `tls_accept` contains the "PSK" bit|
|active_available|integer|Host active interface availability status.<br><br>Possible values:<br>0 - interface status is unknown;<br>1 - interface is available;<br>2 - interface is not available.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*|

[comment]: # ({/f677a3dd-2f6c5c76})

[comment]: # ({c5a8c385-859da78d})
### Host inventory

The host inventory object has the following properties.

::: notetip
Each property has it's own unique ID number, which is
used to associate host inventory fields with items.
:::

|ID|Property|[Type](/manual/api/reference_commentary#data_types)|Description|Maximum length|
|-|--|-|-----|-----|
|4|alias|string|Alias.|128 characters|
|11|asset\_tag|string|Asset tag.|64 characters|
|28|chassis|string|Chassis.|64 characters|
|23|contact|string|Contact person.|Depends on the database used:<br>- 65535 characters for SQL databases<br>- 2048 characters for Oracle databases|
|32|contract\_number|string|Contract number.|64 characters|
|47|date\_hw\_decomm|string|HW decommissioning date.|64 characters|
|46|date\_hw\_expiry|string|HW maintenance expiry date.|64 characters|
|45|date\_hw\_install|string|HW installation date.|64 characters|
|44|date\_hw\_purchase|string|HW purchase date.|64 characters|
|34|deployment\_status|string|Deployment status.|64 characters|
|14|hardware|string|Hardware.|255 characters|
|15|hardware\_full|string|Detailed hardware.|Depends on the database used:<br>- 65535 characters for SQL databases<br>- 2048 characters for Oracle databases|
|39|host\_netmask|string|Host subnet mask.|39 characters|
|38|host\_networks|string|Host networks.|Depends on the database used:<br>- 65535 characters for SQL databases<br>- 2048 characters for Oracle databases|
|40|host\_router|string|Host router.|39 characters|
|30|hw\_arch|string|HW architecture.|32 characters|
|33|installer\_name|string|Installer name.|64 characters|
|24|location|string|Location.|Depends on the database used:<br>- 65535 characters for SQL databases<br>- 2048 characters for Oracle databases|
|25|location\_lat|string|Location latitude.|16 characters|
|26|location\_lon|string|Location longitude.|16 characters|
|12|macaddress\_a|string|MAC address A.|64 characters|
|13|macaddress\_b|string|MAC address B.|64 characters|
|29|model|string|Model.|64 characters|
|3|name|string|Name.|128 characters|
|27|notes|string|Notes.|Depends on the database used:<br>- 65535 characters for SQL databases<br>- 2048 characters for Oracle databases|
|41|oob\_ip|string|OOB IP address.|39 characters|
|42|oob\_netmask|string|OOB host subnet mask.|39 characters|
|43|oob\_router|string|OOB router.|39 characters|
|5|os|string|OS name.|128 characters|
|6|os\_full|string|Detailed OS name.|255 characters|
|7|os\_short|string|Short OS name.|128 characters|
|61|poc\_1\_cell|string|Primary POC mobile number.|64 characters|
|58|poc\_1\_email|string|Primary email.|128 characters|
|57|poc\_1\_name|string|Primary POC name.|128 characters|
|63|poc\_1\_notes|string|Primary POC notes.|Depends on the database used:<br>- 65535 characters for SQL databases<br>- 2048 characters for Oracle databases|
|59|poc\_1\_phone\_a|string|Primary POC phone A.|64 characters|
|60|poc\_1\_phone\_b|string|Primary POC phone B.|64 characters|
|62|poc\_1\_screen|string|Primary POC screen name.|64 characters|
|68|poc\_2\_cell|string|Secondary POC mobile number.|64 characters|
|65|poc\_2\_email|string|Secondary POC email.|128 characters|
|64|poc\_2\_name|string|Secondary POC name.|128 characters|
|70|poc\_2\_notes|string|Secondary POC notes.|Depends on the database used:<br>- 65535 characters for SQL databases<br>- 2048 characters for Oracle databases|
|66|poc\_2\_phone\_a|string|Secondary POC phone A.|64 characters|
|67|poc\_2\_phone\_b|string|Secondary POC phone B.|64 characters|
|69|poc\_2\_screen|string|Secondary POC screen name.|64 characters|
|8|serialno\_a|string|Serial number A.|64 characters|
|9|serialno\_b|string|Serial number B.|64 characters|
|48|site\_address\_a|string|Site address A.|128 characters|
|49|site\_address\_b|string|Site address B.|128 characters|
|50|site\_address\_c|string|Site address C.|128 characters|
|51|site\_city|string|Site city.|128 characters|
|53|site\_country|string|Site country.|64 characters|
|56|site\_notes|string|Site notes.|Depends on the database used:<br>- 65535 characters for SQL databases<br>- 2048 characters for Oracle databases|
|55|site\_rack|string|Site rack location.|128 characters|
|52|site\_state|string|Site state.|64 characters|
|54|site\_zip|string|Site ZIP/postal code.|64 characters|
|16|software|string|Software.|255 characters|
|18|software\_app\_a|string|Software application A.|64 characters|
|19|software\_app\_b|string|Software application B.|64 characters|
|20|software\_app\_c|string|Software application C.|64 characters|
|21|software\_app\_d|string|Software application D.|64 characters|
|22|software\_app\_e|string|Software application E.|64 characters|
|17|software\_full|string|Software details.|Depends on the database used:<br>- 65535 characters for SQL databases<br>- 2048 characters for Oracle databases|
|10|tag|string|Tag.|64 characters|
|1|type|string|Type.|64 characters|
|2|type\_full|string|Type details.|64 characters|
|35|url\_a|string|URL A.|2048 characters|
|36|url\_b|string|URL B.|2048 characters|
|37|url\_c|string|URL C.|2048 characters|
|31|vendor|string|Vendor.|64 characters|

[comment]: # ({/c5a8c385-859da78d})

[comment]: # ({763fc913-0ae1807e})
### Host tag

The host tag object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|tag|string|Host tag name.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|value|string|Host tag value.|
|automatic|integer|Type of host tag.<br><br>Possible values:<br>0 - *(default)* manual (tag created by user);<br>1 - automatic (tag created by low-level discovery) |

[comment]: # ({/763fc913-0ae1807e})
