[comment]: # ({ce3b3e73-ce3b3e73})
# host.create

[comment]: # ({/ce3b3e73-ce3b3e73})

[comment]: # ({a2bcf5e3-2e20b613})
### Description

`object host.create(object/array hosts)`

This method allows to create new hosts.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/a2bcf5e3-2e20b613})

[comment]: # ({2e9a1fbe-48df9d2f})
### Parameters

`(object/array)` Hosts to create.

Additionally to the [standard host properties](object#host), the method
accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|groups|object/array|Host [groups](/manual/api/reference/hostgroup/object) to add the host to.<br><br>The host groups must have the `groupid` property defined.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required*|
|interfaces|object/array|[Interfaces](/manual/api/reference/hostinterface/object) to be created for the host.|
|tags|object/array|Host [tags](/manual/api/reference/host/object#host_tag).|
|templates|object/array|[Templates](/manual/api/reference/template/object) to be linked to the host.<br><br>The templates must have the `templateid` property defined.|
|macros|object/array|[User macros](/manual/api/reference/usermacro/object) to be created for the host.|
|inventory|object|Host [inventory](/manual/api/reference/host/object#host_inventory) properties.|

[comment]: # ({/2e9a1fbe-48df9d2f})

[comment]: # ({de799b9d-de799b9d})
### Return values

`(object)` Returns an object containing the IDs of the created hosts
under the `hostids` property. The order of the returned IDs matches the
order of the passed hosts.

[comment]: # ({/de799b9d-de799b9d})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({90729907-26320041})
#### Creating a host

Create a host called "Linux server" with an IP interface and tags, add
it to a group, link a template to it and set the MAC addresses in the
host inventory.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "host.create",
    "params": {
        "host": "Linux server",
        "interfaces": [
            {
                "type": 1,
                "main": 1,
                "useip": 1,
                "ip": "192.168.3.1",
                "dns": "",
                "port": "10050"
            }
        ],
        "groups": [
            {
                "groupid": "50"
            }
        ],
        "tags": [
            {
                "tag": "Host name",
                "value": "Linux server"
            }
        ],
        "templates": [
            {
                "templateid": "20045"
            }
        ],
        "macros": [
            {
                "macro": "{$USER_ID}",
                "value": "123321"
            },
            {
                "macro": "{$USER_LOCATION}",
                "value": "0:0:0",
                "description": "latitude, longitude and altitude coordinates"
            }
        ],
        "inventory_mode": 0,
        "inventory": {
            "macaddress_a": "01234",
            "macaddress_b": "56768"
        }
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "hostids": [
            "107819"
        ]
    },
    "id": 1
}
```

[comment]: # ({/90729907-26320041})

[comment]: # ({0ae07890-9572ca08})
#### Creating a host with SNMP interface

Create a host called "SNMP host" with an SNMPv3 interface with details.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "host.create",
    "params": {
        "host": "SNMP host",
        "interfaces": [
            {
                "type": 2,
                "main": 1,
                "useip": 1,
                "ip": "127.0.0.1",
                "dns": "",
                "port": "161",
                "details": {
                    "version": 3,
                    "bulk": 0,
                    "securityname": "mysecurityname",
                    "contextname": "",
                    "securitylevel": 1
                }
            }
        ],
        "groups": [
            {
                "groupid": "4"
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "hostids": [
            "10658"
        ]
    },
    "id": 1
}
```

[comment]: # ({/0ae07890-9572ca08})

[comment]: # ({44acfb2a-4c198338})
#### Creating a host with PSK encryption

Create a host called "PSK host" with PSK encryption configured.
Note that the host has to be [pre-configured to use PSK](/manual/encryption/using_pre_shared_keys#configuring-psk-for-server-agent-communication-example).

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "host.create",
    "params": {
        "host": "PSK host",
        "interfaces": [
            {
                "type": 1,
                "ip": "192.168.3.1",
                "dns": "",
                "port": "10050",
                "useip": 1,
                "main": 1
            }
        ],
        "groups": [
            {
                "groupid": "2"
            }
        ],
        "tls_accept": 2,
        "tls_connect": 2,
        "tls_psk_identity": "PSK 001",
        "tls_psk": "1f87b595725ac58dd977beef14b97461a7c1045b9a1c963065002c5473194952"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "hostids": [
            "10590"
        ]
    },
    "id": 1
}
```

[comment]: # ({/44acfb2a-4c198338})

[comment]: # ({24b8f767-203ca033})
### See also

-   [Host group](/manual/api/reference/hostgroup/object#host_group)
-   [Template](/manual/api/reference/template/object#template)
-   [User macro](/manual/api/reference/usermacro/object#hosttemplate_level_macro)
-   [Host interface](/manual/api/reference/hostinterface/object#host_interface)
-   [Host inventory](object#host_inventory)
-   [Host tag](object#host_tag)

[comment]: # ({/24b8f767-203ca033})

[comment]: # ({f7e47995-f7e47995})
### Source

CHost::create() in *ui/include/classes/api/services/CHost.php*.

[comment]: # ({/f7e47995-f7e47995})
