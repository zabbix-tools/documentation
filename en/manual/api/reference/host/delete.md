[comment]: # ({dd2b9519-dd2b9519})
# host.delete

[comment]: # ({/dd2b9519-dd2b9519})

[comment]: # ({4fc1750c-8f1cd3fa})
### Description

`object host.delete(array hosts)`

This method allows to delete hosts.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/4fc1750c-8f1cd3fa})

[comment]: # ({cede3d95-cede3d95})
### Parameters

`(array)` IDs of hosts to delete.

[comment]: # ({/cede3d95-cede3d95})

[comment]: # ({54b4a855-54b4a855})
### Return values

`(object)` Returns an object containing the IDs of the deleted hosts
under the `hostids` property.

[comment]: # ({/54b4a855-54b4a855})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({47a8da31-ef767d02})
#### Deleting multiple hosts

Delete two hosts.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "host.delete",
    "params": [
        "13",
        "32"
    ],
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "hostids": [
            "13",
            "32"
        ]
    },
    "id": 1
}
```

[comment]: # ({/47a8da31-ef767d02})

[comment]: # ({9d5f3989-9d5f3989})
### Source

CHost::delete() in *ui/include/classes/api/services/CHost.php*.

[comment]: # ({/9d5f3989-9d5f3989})
