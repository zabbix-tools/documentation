[comment]: # ({2a0a5084-2a0a5084})
# host.massremove

[comment]: # ({/2a0a5084-2a0a5084})

[comment]: # ({c46733ea-9c943195})
### Description

`object host.massremove(object parameters)`

This method allows to remove related objects from multiple hosts.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/c46733ea-9c943195})

[comment]: # ({08066fc5-689d39a3})
### Parameters

`(object)` Parameters containing the IDs of the hosts to update and the
objects that should be removed.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|hostids|string/array|IDs of the hosts to be updated.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required*|
|groupids|string/array|Host groups to remove the given hosts from.|
|interfaces|object/array|Host interfaces to remove from the given hosts.<br><br>The host interface object must have the `ip`, `dns` and `port` properties defined.|
|macros|string/array|User macros to delete from the given hosts.|
|templateids|string/array|Templates to unlink from the given hosts.|
|templateids\_clear|string/array|Templates to unlink and clear from the given hosts.|

[comment]: # ({/08066fc5-689d39a3})

[comment]: # ({d166b99b-d166b99b})
### Return values

`(object)` Returns an object containing the IDs of the updated hosts
under the `hostids` property.

[comment]: # ({/d166b99b-d166b99b})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({e1df8faa-8c635a7b})
#### Unlinking templates

Unlink a template from two hosts and delete all of the templated
entities.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "host.massremove",
    "params": {
        "hostids": ["69665", "69666"],
        "templateids_clear": "325"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "hostids": [
            "69665",
            "69666"
        ]
    },
    "id": 1
}
```

[comment]: # ({/e1df8faa-8c635a7b})

[comment]: # ({abd9a8d3-abd9a8d3})
### See also

-   [host.update](update)
-   [User
    macro](/manual/api/reference/usermacro/object#hosttemplate_level_macro)
-   [Host
    interface](/manual/api/reference/hostinterface/object#host_interface)

[comment]: # ({/abd9a8d3-abd9a8d3})

[comment]: # ({bda21fe6-bda21fe6})
### Source

CHost::massRemove() in *ui/include/classes/api/services/CHost.php*.

[comment]: # ({/bda21fe6-bda21fe6})
