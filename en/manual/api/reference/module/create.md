[comment]: # ({33a6b22e-2f868b63})
# module.create

[comment]: # ({/33a6b22e-2f868b63})

[comment]: # ({da5198e9-b100d64b})
### Description

`object module.create(object/array modules)`

This method allows to install new frontend modules.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings.
See [User roles](/manual/web_interface/frontend_sections/users/user_roles) for more information.
:::

::: noteimportant
Module files must be unpacked manually in the correct subdirectories, matching the `relative_path` property of the modules.
:::

[comment]: # ({/da5198e9-b100d64b})

[comment]: # ({b5fc0458-a3899c85})
### Parameters

`(object/array)` Modules to create.

The method accepts modules with the [standard module properties](object#module).

[comment]: # ({/b5fc0458-a3899c85})

[comment]: # ({ecf1e93b-b4af5197})
### Return values

`(object)` Returns an object containing the IDs of the installed modules under the `moduleids` property.
The order of the returned IDs matches the order of the passed modules.

[comment]: # ({/ecf1e93b-b4af5197})

[comment]: # ({b41637d2-d5892b27})
### Examples

[comment]: # ({/b41637d2-d5892b27})

[comment]: # ({78517ea1-f4bbc4e4})
#### Installing a module

Install a module with the status "Enabled".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "module.create",
    "params": {
        "id": "example_module",
        "relative_path": "modules/example_module",
        "status": 1
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "moduleids": [
            "25"
        ]
    },
    "id": 1
}
```

[comment]: # ({/78517ea1-f4bbc4e4})

[comment]: # ({30745fe9-97287e12})
### See also

-   [Module](object#module)
-   [Frontend modules](/manual/extensions/frontendmodules)

[comment]: # ({/30745fe9-97287e12})

[comment]: # ({c96370ba-174be480})
### Source

CModule::create() in *ui/include/classes/api/services/CModule.php*.

[comment]: # ({/c96370ba-174be480})
