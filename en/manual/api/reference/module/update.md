[comment]: # ({95e2f203-05e9a4a1})
# module.update

[comment]: # ({/95e2f203-05e9a4a1})

[comment]: # ({c3aa435b-779d6c90})
### Description

`object module.update(object/array modules)`

This method allows to update existing modules.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings.
See [User roles](/manual/web_interface/frontend_sections/users/user_roles) for more information.
:::

[comment]: # ({/c3aa435b-779d6c90})

[comment]: # ({e10bed16-ad356045})
### Parameters

`(object/array)` Module properties to be updated.

The `moduleid` property must be defined for each module, all other properties are optional.
Only the specified properties will be updated.

The method accepts modules with the [standard module properties](object#module).

[comment]: # ({/e10bed16-ad356045})

[comment]: # ({a32b2d36-cc12ced5})
### Return values

`(object)` Returns an object containing the IDs of the updated modules under the `moduleids` property.

[comment]: # ({/a32b2d36-cc12ced5})

[comment]: # ({b41637d2-161c7484})
### Examples

[comment]: # ({/b41637d2-161c7484})

[comment]: # ({4f50afb8-ee634823})
#### Disabling a module

Disable module "25".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "module.update",
    "params": {
        "moduleid": "25",
        "status": 0
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "moduleids": [
            "25"
        ]
    },
    "id": 1
}
```

[comment]: # ({/4f50afb8-ee634823})

[comment]: # ({30745fe9-28606e62})
### See also

-   [Module](object#module)
-   [Frontend modules](/manual/extensions/frontendmodules)

[comment]: # ({/30745fe9-28606e62})

[comment]: # ({b1f70e97-9dc6573c})
### Source

CModule::update() in *ui/include/classes/api/services/CModule.php*.

[comment]: # ({/b1f70e97-9dc6573c})
