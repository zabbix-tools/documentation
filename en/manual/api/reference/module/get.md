[comment]: # ({63f61bee-1c702cf9})
# module.get

[comment]: # ({/63f61bee-1c702cf9})

[comment]: # ({774f7691-55f8403c})
### Description

`integer/array module.get(object parameters)`

The method allows to retrieve modules according to the given parameters.

::: noteclassic
This method is available to users of any type. Permissions to call the method can be revoked in user role settings.
See [User roles](/manual/web_interface/frontend_sections/users/user_roles) for more information.
:::

[comment]: # ({/774f7691-55f8403c})

[comment]: # ({e4a581ad-c81b6507})
### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|moduleids|string/array|Return only modules with the given IDs.|
|sortfield|string/array|Sort the result by the given properties.<br><br>Possible values: `moduleid`, `relative_path`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [Reference commentary](/manual/api/reference_commentary#common_get_method_parameters) page.|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

[comment]: # ({/e4a581ad-c81b6507})

[comment]: # ({07ca11ca-46698515})
### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has been used.

[comment]: # ({/07ca11ca-46698515})

[comment]: # ({b41637d2-0d9fe9e7})
### Examples

[comment]: # ({/b41637d2-0d9fe9e7})

[comment]: # ({b4a4b966-8cbe68b4})
#### Retrieving a module by ID

Retrieve all data about modules "1", "2", and "25".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "module.get",
    "params": {
        "output": "extend",
        "moduleids": [
            "1",
            "2",
            "25"
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": [
        {
            "moduleid": "1",
            "id": "actionlog",
            "relative_path": "widgets/actionlog",
            "status": "1",
            "config": []
        },
        {
            "moduleid": "2",
            "id": "clock",
            "relative_path": "widgets/clock",
            "status": "1",
            "config": []
        },
        {
            "moduleid": "25",
            "id": "example",
            "relative_path": "modules/example_module",
            "status": "1",
            "config": []
        }
    ],
    "id": 1
}
```

[comment]: # ({/b4a4b966-8cbe68b4})

[comment]: # ({393f0c8a-77300f42})
### See also

-   [Module](object#module)
-   [Dashboard widget](/manual/api/reference/dashboard/object#dashboard-widget)
-   [Frontend modules](/manual/extensions/frontendmodules)

[comment]: # ({/393f0c8a-77300f42})

[comment]: # ({397c2807-c9c85790})
### Source

CModule::get() in *ui/include/classes/api/services/CModule.php*.

[comment]: # ({/397c2807-c9c85790})
