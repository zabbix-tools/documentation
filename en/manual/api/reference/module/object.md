[comment]: # ({1f977cf8-af3de79b})
# > Module object

The following objects are directly related to the `module` API.

[comment]: # ({/1f977cf8-af3de79b})

[comment]: # ({9d3087b1-4b252ad6})
### Module

The module object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|-|-------|
|moduleid|string|ID of the module as stored in the database.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*<br>- *required* for update operations|
|id|string|Unique module ID as defined by a developer in the [manifest.json](/manual/extensions/frontendmodules#reference) file of the module.<br><br>Possible values for built-in modules:<br>see property "type" description in [Dashboard widget](/manual/api/reference/dashboard/object#dashboard-widget).<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* for create operations|
|relative_path|string|Path to the directory of the module relative to the directory of the Zabbix frontend.<br><br>Possible values:<br>`widgets/*` - for built-in widget modules;<br>`modules/*` - for third-party modules.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* for create operations|
|status|integer|Whether the module is enabled or disabled.<br><br>Possible values:<br>0 - *(default)* Disabled;<br>1 - Enabled.|
|config|object|[Module configuration](/manual/extensions/frontendmodules#reference).|

[comment]: # ({/9d3087b1-4b252ad6})
