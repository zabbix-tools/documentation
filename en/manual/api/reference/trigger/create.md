[comment]: # ({56486046-56486046})
# trigger.create

[comment]: # ({/56486046-56486046})

[comment]: # ({26d870f8-8efa90c6})
### Description

`object trigger.create(object/array triggers)`

This method allows to create new triggers.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/26d870f8-8efa90c6})

[comment]: # ({cd513dbf-c2c3e90f})
### Parameters

`(object/array)` Triggers to create.

Additionally to the [standard trigger properties](object#trigger) the
method accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|dependencies|array|Triggers that the trigger is dependent on.<br><br>The triggers must have the `triggerid` property defined.|
|tags|array|Trigger [tags.](/manual/api/reference/trigger/object#trigger_tag)|

::: noteimportant
The trigger expression has to be given in its
expanded form.
:::

[comment]: # ({/cd513dbf-c2c3e90f})

[comment]: # ({4938d22d-4938d22d})
### Return values

`(object)` Returns an object containing the IDs of the created triggers
under the `triggerids` property. The order of the returned IDs matches
the order of the passed triggers.

[comment]: # ({/4938d22d-4938d22d})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({6e3f4d9f-f57689d3})
#### Creating a trigger

Create a trigger with a single trigger dependency.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "trigger.create",
    "params": [
        {
            "description": "Processor load is too high on {HOST.NAME}",
            "expression": "last(/Linux server/system.cpu.load[percpu,avg1])>5",
            "dependencies": [
                {
                    "triggerid": "17367"
                }
            ]
        },
        {
            "description": "Service status",
            "expression": "length(last(/Linux server/log[/var/log/system,Service .* has stopped]))<>0",
            "dependencies": [
                {
                    "triggerid": "17368"
                }
            ],
            "tags": [
                {
                    "tag": "service",
                    "value": "{{ITEM.VALUE}.regsub(\"Service (.*) has stopped\", \"\\1\")}"
                },
                {
                    "tag": "error",
                    "value": ""
                }
            ]
        }
    ],
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "triggerids": [
            "17369",
            "17370"
        ]
    },
    "id": 1
}
```

[comment]: # ({/6e3f4d9f-f57689d3})

[comment]: # ({7d49da78-7d49da78})
### Source

CTrigger::create() in *ui/include/classes/api/services/CTrigger.php*.

[comment]: # ({/7d49da78-7d49da78})
