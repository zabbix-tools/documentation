[comment]: # ({a7557797-a7557797})
# trigger.update

[comment]: # ({/a7557797-a7557797})

[comment]: # ({f363309c-476a7855})
### Description

`object trigger.update(object/array triggers)`

This method allows to update existing triggers.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/f363309c-476a7855})

[comment]: # ({bac918b1-b918ffdf})
### Parameters

`(object/array)` Trigger properties to be updated.

The `triggerid` property must be defined for each trigger, all other
properties are optional. Only the passed properties will be updated, all
others will remain unchanged.

Additionally to the [standard trigger properties](object#trigger) the
method accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|dependencies|array|Triggers that the trigger is dependent on.<br><br>The triggers must have the `triggerid` property defined.|
|tags|array|Trigger [tags.](/manual/api/reference/trigger/object#trigger_tag)|

::: noteimportant
The trigger expression has to be given in its
expanded form.
:::

[comment]: # ({/bac918b1-b918ffdf})

[comment]: # ({18d0cc04-18d0cc04})
### Return values

`(object)` Returns an object containing the IDs of the updated triggers
under the `triggerids` property.

[comment]: # ({/18d0cc04-18d0cc04})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({b04776c4-07c64481})
#### Enabling a trigger

Enable a trigger, that is, set its status to "0".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "trigger.update",
    "params": {
        "triggerid": "13938",
        "status": 0
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "triggerids": [
            "13938"
        ]
    },
    "id": 1
}
```

[comment]: # ({/b04776c4-07c64481})

[comment]: # ({ccacfb9d-b2bab7ce})
#### Replacing triggers tags

Replace tags for trigger.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "trigger.update",
    "params": {
        "triggerid": "13938",
        "tags": [
            {
                "tag": "service",
                "value": "{{ITEM.VALUE}.regsub(\"Service (.*) has stopped\", \"\\1\")}"
            },
            {
                "tag": "error",
                "value": ""
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "triggerids": [
            "13938"
        ]
    },
    "id": 1
}
```

[comment]: # ({/ccacfb9d-b2bab7ce})

[comment]: # ({38de55de-db463204})
#### Replacing dependencies

Replace dependencies for trigger.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "trigger.update",
    "params": {
        "triggerid": "22713",
        "dependencies": [
            {
                "triggerid": "22712"
            },
            {
                "triggerid": "22772"
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "triggerids": [
            "22713"
        ]
    },
    "id": 1
}
```

[comment]: # ({/38de55de-db463204})

[comment]: # ({180a8172-180a8172})
### Source

CTrigger::update() in *ui/include/classes/api/services/CTrigger.php*.

[comment]: # ({/180a8172-180a8172})
