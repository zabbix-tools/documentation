[comment]: # ({bce5854d-bce5854d})
# trigger.delete

[comment]: # ({/bce5854d-bce5854d})

[comment]: # ({2770f7e5-adb8b388})
### Description

`object trigger.delete(array triggerIds)`

This method allows to delete triggers.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/2770f7e5-adb8b388})

[comment]: # ({fc913e85-fc913e85})
### Parameters

`(array)` IDs of the triggers to delete.

[comment]: # ({/fc913e85-fc913e85})

[comment]: # ({a2fb984a-a2fb984a})
### Return values

`(object)` Returns an object containing the IDs of the deleted triggers
under the `triggerids` property.

[comment]: # ({/a2fb984a-a2fb984a})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({9186c860-752e2ebb})
#### Delete multiple triggers

Delete two triggers.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "trigger.delete",
    "params": [
        "12002",
        "12003"
    ],
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "triggerids": [
            "12002",
            "12003"
        ]
    },
    "id": 1
}
```

[comment]: # ({/9186c860-752e2ebb})

[comment]: # ({087c0548-087c0548})
### Source

CTrigger::delete() in *ui/include/classes/api/services/CTrigger.php*.

[comment]: # ({/087c0548-087c0548})
