[comment]: # ({fff19f5f-fff19f5f})
# hostinterface.massadd

[comment]: # ({/fff19f5f-fff19f5f})

[comment]: # ({5e6b661e-30999da7})
### Description

`object hostinterface.massadd(object parameters)`

This method allows to simultaneously add host interfaces to multiple
hosts.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/5e6b661e-30999da7})

[comment]: # ({b88f5a10-35c0864e})
### Parameters

`(object)` Parameters containing the host interfaces to be created on
the given hosts.

The method accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|interfaces|object/array|[Host interfaces](/manual/api/reference/hostinterface/object) to create on the given hosts.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required*|
|hosts|object/array|Hosts to be updated.<br><br>The hosts must have the `hostid` property defined.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required*|

[comment]: # ({/b88f5a10-35c0864e})

[comment]: # ({88283807-88283807})
### Return values

`(object)` Returns an object containing the IDs of the created host
interfaces under the `interfaceids` property.

[comment]: # ({/88283807-88283807})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({707ed219-ad87458a})
#### Creating interfaces

Create an interface on two hosts.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "hostinterface.massadd",
    "params": {
        "hosts": [
            {
                "hostid": "30050"
            },
            {
                "hostid": "30052"
            }
        ],
        "interfaces": {
            "dns": "",
            "ip": "127.0.0.1",
            "main": 0,
            "port": "10050",
            "type": 1,
            "useip": 1
        }
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "interfaceids": [
            "30069",
            "30070"
        ]
    },
    "id": 1
}
```

[comment]: # ({/707ed219-ad87458a})

[comment]: # ({a2a46209-a2a46209})
### See also

-   [hostinterface.create](create)
-   [host.massadd](/manual/api/reference/host/massadd)
-   [Host](/manual/api/reference/host/object#host)

[comment]: # ({/a2a46209-a2a46209})

[comment]: # ({9b65afcc-9b65afcc})
### Source

CHostInterface::massAdd() in
*ui/include/classes/api/services/CHostInterface.php*.

[comment]: # ({/9b65afcc-9b65afcc})
