[comment]: # ({2898fc4b-2898fc4b})
# hostinterface.replacehostinterfaces

[comment]: # ({/2898fc4b-2898fc4b})

[comment]: # ({e440e997-f14e7778})
### Description

`object hostinterface.replacehostinterfaces(object parameters)`

This method allows to replace all host interfaces on a given host.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/e440e997-f14e7778})

[comment]: # ({779a3d2c-f5cf0b40})
### Parameters

`(object)` Parameters containing the ID of the host to be updated and
the new host interfaces.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|interfaces|object/array|[Host interfaces](/manual/api/reference/hostinterface/object) to replace the current host interfaces with.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required*|
|hostid|string|ID of the host to be updated.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required*|

[comment]: # ({/779a3d2c-f5cf0b40})

[comment]: # ({88283807-88283807})
### Return values

`(object)` Returns an object containing the IDs of the created host
interfaces under the `interfaceids` property.

[comment]: # ({/88283807-88283807})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({01fd5e38-98e67db1})
#### Replacing host interfaces

Replace all host interfaces with a single agent interface.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "hostinterface.replacehostinterfaces",
    "params": {
        "hostid": "30052",
        "interfaces": {
            "dns": "",
            "ip": "127.0.0.1",
            "main": 1,
            "port": "10050",
            "type": 1,
            "useip": 1
        }
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "interfaceids": [
            "30081"
        ]
    },
    "id": 1
}
```

[comment]: # ({/01fd5e38-98e67db1})

[comment]: # ({272c42ef-272c42ef})
### See also

-   [host.update](/manual/api/reference/host/update)
-   [host.massupdate](/manual/api/reference/host/massupdate)

[comment]: # ({/272c42ef-272c42ef})

[comment]: # ({9d1e0b55-9d1e0b55})
### Source

CHostInterface::replaceHostInterfaces() in
*ui/include/classes/api/services/CHostInterface.php*.

[comment]: # ({/9d1e0b55-9d1e0b55})
