[comment]: # ({9230837a-9230837a})
# hostinterface.massremove

[comment]: # ({/9230837a-9230837a})

[comment]: # ({75214f21-4559aff6})
### Description

`object hostinterface.massremove(object parameters)`

This method allows to remove host interfaces from the given hosts.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/75214f21-4559aff6})

[comment]: # ({c81c7a33-8970fdbb})
### Parameters

`(object)` Parameters containing the IDs of the hosts to be updated and
the interfaces to be removed.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|interfaces|object/array|Host interfaces to remove from the given hosts.<br><br>The host interface object must have the `ip`, `dns` and `port` properties defined.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required*|
|hostids|string/array|IDs of the hosts to be updated.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required*|

[comment]: # ({/c81c7a33-8970fdbb})

[comment]: # ({61a8b976-61a8b976})
### Return values

`(object)` Returns an object containing the IDs of the deleted host
interfaces under the `interfaceids` property.

[comment]: # ({/61a8b976-61a8b976})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({c2d2ec7c-cceffaf2})
#### Removing interfaces

Remove the "127.0.0.1" SNMP interface from two hosts.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "hostinterface.massremove",
    "params": {
        "hostids": [
            "30050",
            "30052"
        ],
        "interfaces": {
            "dns": "",
            "ip": "127.0.0.1",
            "port": "161"
        }
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "interfaceids": [
            "30069",
            "30070"
        ]
    },
    "id": 1
}
```

[comment]: # ({/c2d2ec7c-cceffaf2})

[comment]: # ({a3245281-a3245281})
### See also

-   [hostinterface.delete](delete)
-   [host.massremove](/manual/api/reference/host/massremove)

[comment]: # ({/a3245281-a3245281})

[comment]: # ({6853cf7a-6853cf7a})
### Source

CHostInterface::massRemove() in
*ui/include/classes/api/services/CHostInterface.php*.

[comment]: # ({/6853cf7a-6853cf7a})
