[comment]: # ({0bfa3a60-0bfa3a60})
# hostinterface.create

[comment]: # ({/0bfa3a60-0bfa3a60})

[comment]: # ({c7c96547-e87d74af})
### Description

`object hostinterface.create(object/array hostInterfaces)`

This method allows to create new host interfaces.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/c7c96547-e87d74af})

[comment]: # ({4df7f085-e81b8e01})
### Parameters

`(object/array)` Host interfaces to create.

The method accepts host interfaces with the [standard host interface properties](object#host_interface).

[comment]: # ({/4df7f085-e81b8e01})

[comment]: # ({4b59ae6b-4b59ae6b})
### Return values

`(object)` Returns an object containing the IDs of the created host
interfaces under the `interfaceids` property. The order of the returned
IDs matches the order of the passed host interfaces.

[comment]: # ({/4b59ae6b-4b59ae6b})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({12d64d13-150295f4})
#### Create a new interface

Create a secondary IP agent interface on host "30052."

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "hostinterface.create",
    "params": {
        "hostid": "30052",
        "main": "0",
        "type": "1",
        "useip": "1",
        "ip": "127.0.0.1",
        "dns": "",
        "port": "10050",
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "interfaceids": [
            "30062"
        ]
    },
    "id": 1
}
```

[comment]: # ({/12d64d13-150295f4})

[comment]: # ({50caf3a1-c22176c4})
#### Create an interface with SNMP details

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "hostinterface.create",
    "params": {
        "hostid": "10456",
        "main": "0",
        "type": "2",
        "useip": "1",
        "ip": "127.0.0.1",
        "dns": "",
        "port": "1601",
        "details": {
            "version": "2",
            "bulk": "1",
            "community": "{$SNMP_COMMUNITY}"
        }
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "interfaceids": [
            "30063"
        ]
    },
    "id": 1
}
```

[comment]: # ({/50caf3a1-c22176c4})

[comment]: # ({9afed0b0-9afed0b0})
### See also

-   [hostinterface.massadd](massadd)
-   [host.massadd](/manual/api/reference/host/massadd)

[comment]: # ({/9afed0b0-9afed0b0})

[comment]: # ({5b41f219-5b41f219})
### Source

CHostInterface::create() in
*ui/include/classes/api/services/CHostInterface.php*.

[comment]: # ({/5b41f219-5b41f219})
