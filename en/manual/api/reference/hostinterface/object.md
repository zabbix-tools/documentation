[comment]: # ({7ed888d2-7ed888d2})
# > Host interface object

The following objects are directly related to the `hostinterface` API.

[comment]: # ({/7ed888d2-7ed888d2})

[comment]: # ({bc413b9e-ce77a3b0})
### Host interface

The host interface object has the following properties.

::: noteimportant
Note that both `ip` and `dns` properties are *required* for create operations.
If you do not want to use DNS, set it to an empty string.
:::

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|interfaceid|string|ID of the interface.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*<br>- *required* for update operations|
|available|integer|Availability of host interface.<br><br>Possible values:<br>0 - *(default)* unknown;<br>1 - available;<br>2 - unavailable.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*|
|hostid|string|ID of the host that the interface belongs to.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *constant*<br>- *required* for create operations|
|type|integer|Interface type.<br><br>Possible values:<br>1 - Agent;<br>2 - SNMP;<br>3 - IPMI;<br>4 - JMX.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* for create operations|
|ip|string|IP address used by the interface.<br><br>Can be empty if the connection is made via DNS.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* for create operations|
|dns|string|DNS name used by the interface.<br><br>Can be empty if the connection is made via IP.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* for create operations|
|port|string|Port number used by the interface.<br>Can contain user macros.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* for create operations|
|useip|integer|Whether the connection should be made via IP.<br><br>Possible values:<br>0 - connect using host DNS name;<br>1 - connect using host IP address.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* for create operations|
|main|integer|Whether the interface is used as default on the host. Only one interface of some type can be set as default on a host.<br><br>Possible values:<br>0 - not default;<br>1 - default.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* for create operations|
|details|array|Additional [details](#details) object for interface.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `type` is set to "SNMP"|
|disable\_until|timestamp|The next polling time of an unavailable host interface.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*|
|error|string|Error text if host interface is unavailable.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*|
|errors\_from|timestamp|Time when host interface became unavailable.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*|

[comment]: # ({/bc413b9e-ce77a3b0})

[comment]: # ({dbfc895b-3bb71fcc})
### Details

The details object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|version|integer|SNMP interface version.<br><br>Possible values:<br>1 - SNMPv1;<br>2 - SNMPv2c;<br>3 - SNMPv3.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|bulk|integer|Whether to use bulk SNMP requests.<br><br>Possible values:<br>0 - don't use bulk requests;<br>1 - (default) - use bulk requests.|
|community|string|SNMP community. Used only by SNMPv1 and SNMPv2 interfaces.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `version` is set to "SNMPv1" or "SNMPv2c"|
|securityname|string|SNMPv3 security name. Used only by SNMPv3 interfaces.|
|securitylevel|integer|SNMPv3 security level. Used only by SNMPv3 interfaces.<br><br>Possible values:<br>0 - (default) - noAuthNoPriv;<br>1 - authNoPriv;<br>2 - authPriv.|
|authpassphrase|string|SNMPv3 authentication passphrase. Used only by SNMPv3 interfaces.|
|privpassphrase|string|SNMPv3 privacy passphrase. Used only by SNMPv3 interfaces.|
|authprotocol|integer|SNMPv3 authentication protocol. Used only by SNMPv3 interfaces.<br><br>Possible values:<br>0 - (default) - MD5;<br>1 - SHA1;<br>2 - SHA224;<br>3 - SHA256;<br>4 - SHA384;<br>5 - SHA512.|
|privprotocol|integer|SNMPv3 privacy protocol. Used only by SNMPv3 interfaces.<br><br>Possible values:<br>0 - (default) - DES;<br>1 - AES128;<br>2 - AES192;<br>3 - AES256;<br>4 - AES192C;<br>5 - AES256C.|
|contextname|string|SNMPv3 context name. Used only by SNMPv3 interfaces.|

[comment]: # ({/dbfc895b-3bb71fcc})
