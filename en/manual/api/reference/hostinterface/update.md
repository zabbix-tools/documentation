[comment]: # ({59b410c0-59b410c0})
# hostinterface.update

[comment]: # ({/59b410c0-59b410c0})

[comment]: # ({ff3d5f77-253ed57f})
### Description

`object hostinterface.update(object/array hostInterfaces)`

This method allows to update existing host interfaces.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/ff3d5f77-253ed57f})

[comment]: # ({a99ac0ad-a99ac0ad})
### Parameters

`(object/array)` [Host interface properties](object#host_interface) to
be updated.

The `interfaceid` property must be defined for each host interface, all
other properties are optional. Only the given properties will be
updated, all others will remain unchanged.

[comment]: # ({/a99ac0ad-a99ac0ad})

[comment]: # ({d3be3202-d3be3202})
### Return values

`(object)` Returns an object containing the IDs of the updated host
interfaces under the `interfaceids` property.

[comment]: # ({/d3be3202-d3be3202})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({d6135bbd-eb8b35e8})
#### Changing a host interface port

Change the port of a host interface.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "hostinterface.update",
    "params": {
        "interfaceid": "30048",
        "port": "30050"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "interfaceids": [
            "30048"
        ]
    },
    "id": 1
}
```

[comment]: # ({/d6135bbd-eb8b35e8})

[comment]: # ({97e4c8d0-97e4c8d0})
### Source

CHostInterface::update() in
*ui/include/classes/api/services/CHostInterface.php*.

[comment]: # ({/97e4c8d0-97e4c8d0})
