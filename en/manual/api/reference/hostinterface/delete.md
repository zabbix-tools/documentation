[comment]: # ({fc20d2a9-fc20d2a9})
# hostinterface.delete

[comment]: # ({/fc20d2a9-fc20d2a9})

[comment]: # ({38b988e0-6a90d1d2})
### Description

`object hostinterface.delete(array hostInterfaceIds)`

This method allows to delete host interfaces.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/38b988e0-6a90d1d2})

[comment]: # ({617ba57c-617ba57c})
### Parameters

`(array)` IDs of the host interfaces to delete.

[comment]: # ({/617ba57c-617ba57c})

[comment]: # ({61a8b976-61a8b976})
### Return values

`(object)` Returns an object containing the IDs of the deleted host
interfaces under the `interfaceids` property.

[comment]: # ({/61a8b976-61a8b976})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({664e15f5-91934c15})
#### Delete a host interface

Delete the host interface with ID 30062.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "hostinterface.delete",
    "params": [
        "30062"
    ],
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "interfaceids": [
            "30062"
        ]
    },
    "id": 1
}
```

[comment]: # ({/664e15f5-91934c15})

[comment]: # ({a3d1e725-a3d1e725})
### See also

-   [hostinterface.massremove](massremove)
-   [host.massremove](/manual/api/reference/host/massremove)

[comment]: # ({/a3d1e725-a3d1e725})

[comment]: # ({cf54c99c-cf54c99c})
### Source

CHostInterface::delete() in
*ui/include/classes/api/services/CHostInterface.php*.

[comment]: # ({/cf54c99c-cf54c99c})
