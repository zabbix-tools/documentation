[comment]: # ({4d843d5e-6a1da71f})
# Host

This class is designed to work with hosts.

Object references:

-   [Host](/manual/api/reference/host/object#host)
-   [Host inventory](/manual/api/reference/host/object#host_inventory)
-   [Host tag](/manual/api/reference/host/object#host_tag)

Available methods:

-   [host.create](/manual/api/reference/host/create) - create new hosts
-   [host.delete](/manual/api/reference/host/delete) - delete hosts
-   [host.get](/manual/api/reference/host/get) - retrieve hosts
-   [host.massadd](/manual/api/reference/host/massadd) - add related objects to hosts
-   [host.massremove](/manual/api/reference/host/massremove) - remove related objects from hosts
-   [host.massupdate](/manual/api/reference/host/massupdate) - replace or remove related objects from hosts
-   [host.update](/manual/api/reference/host/update) - update hosts

[comment]: # ({/4d843d5e-6a1da71f})
