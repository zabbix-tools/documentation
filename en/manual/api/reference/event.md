[comment]: # ({a78185d1-4312b82f})
# Event

This class is designed to work with events.

Object references:

-   [Event](/manual/api/reference/event/object#event)
-   [Event tag](/manual/api/reference/event/object#event_tag)
-   [Media type URL](/manual/api/reference/event/object#media_type_url)

Available methods:

-   [event.get](/manual/api/reference/event/get) - retrieve events
-   [event.acknowledge](/manual/api/reference/event/acknowledge) - acknowledge events

[comment]: # ({/a78185d1-4312b82f})
