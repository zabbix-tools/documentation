[comment]: # ({0ec30c22-0ec30c22})
# item.update

[comment]: # ({/0ec30c22-0ec30c22})

[comment]: # ({53e9b148-38961105})
### Description

`object item.update(object/array items)`

This method allows to update existing items.

::: noteclassic
Web items cannot be updated via the Zabbix API.
:::

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/53e9b148-38961105})

[comment]: # ({a9b2dfd5-4e151fd6})
### Parameters

`(object/array)` Item properties to be updated.

The `itemid` property must be defined for each item, all other
properties are optional. Only the passed properties will be updated, all
others will remain unchanged.

Additionally to the [standard item properties](object#item), the method
accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|preprocessing|array|[Item preprocessing](/manual/api/reference/item/object#item_preprocessing) options to replace the current preprocessing options.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *read-only* for inherited objects or discovered objects|
|tags|array|Item [tags](/manual/api/reference/item/object#item_tag).<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *read-only* for discovered objects|

[comment]: # ({/a9b2dfd5-4e151fd6})

[comment]: # ({aefff750-aefff750})
### Return values

`(object)` Returns an object containing the IDs of the updated items
under the `itemids` property.

[comment]: # ({/aefff750-aefff750})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({c681d7b3-5388578b})
#### Enabling an item

Enable an item, that is, set its status to "0".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "item.update",
    "params": {
        "itemid": "10092",
        "status": 0
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "10092"
        ]
    },
    "id": 1
}
```

[comment]: # ({/c681d7b3-5388578b})

[comment]: # ({09eff812-0bf89382})
#### Update dependent item

Update Dependent item name and Master item ID. Only dependencies on same
host are allowed, therefore Master and Dependent item should have same
hostid.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "item.update",
    "params": {
        "name": "Dependent item updated name",
        "master_itemid": "25562",
        "itemid": "189019"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "189019"
        ]
    },
    "id": 1
}
```

[comment]: # ({/09eff812-0bf89382})

[comment]: # ({509fa40e-8f365363})
#### Update HTTP agent item

Enable item value trapping.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "item.update",
    "params": {
        "itemid": "23856",
        "allow_traps": 1
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "23856"
        ]
    },
    "id": 1
}
```

[comment]: # ({/509fa40e-8f365363})

[comment]: # ({c593b346-44f323b8})
#### Updating an item with preprocessing

Update an item with item preprocessing rule "In range".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "item.update",
    "params": {
        "itemid": "23856",
        "preprocessing": [
            {
                "type": 13,
                "params": "\n100",
                "error_handler": 1,
                "error_handler_params": ""
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "23856"
        ]
    },
    "id": 1
}
```

[comment]: # ({/c593b346-44f323b8})

[comment]: # ({02841321-dcfb98a0})
#### Updating a script item

Update a script item with a different script and remove unnecessary parameters that were used by previous script.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "item.update",
    "params": {
        "itemid": "23865",
        "parameters": [],
        "script": "Zabbix.log(3, 'Log test');\nreturn 1;"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "23865"
        ]
    },
    "id": 1
}
```

[comment]: # ({/02841321-dcfb98a0})

[comment]: # ({646fe32a-646fe32a})
### Source

CItem::update() in *ui/include/classes/api/services/CItem.php*.

[comment]: # ({/646fe32a-646fe32a})
