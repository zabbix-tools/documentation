[comment]: # ({959fe29c-959fe29c})
# item.create

[comment]: # ({/959fe29c-959fe29c})

[comment]: # ({a1aebf86-e3078f54})
### Description

`object item.create(object/array items)`

This method allows to create new items.

::: noteclassic
Web items cannot be created via the Zabbix API.
:::

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/a1aebf86-e3078f54})

[comment]: # ({f82ccd43-777e8bec})
### Parameters

`(object/array)` Items to create.

Additionally to the [standard item properties](object#host), the method
accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|preprocessing|array|Item [preprocessing](/manual/api/reference/item/object#item_preprocessing) options.|
|tags|array|Item [tags](/manual/api/reference/item/object#item_tag).|

[comment]: # ({/f82ccd43-777e8bec})

[comment]: # ({632959fe-632959fe})
### Return values

`(object)` Returns an object containing the IDs of the created items
under the `itemids` property. The order of the returned IDs matches the
order of the passed items.

[comment]: # ({/632959fe-632959fe})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({1485b7ac-6ffb1f45})
#### Creating an item

Create a numeric Zabbix agent item with 2 item tags to monitor free disk space on host with ID "30074".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "item.create",
    "params": {
        "name": "Free disk space on /home/joe/",
        "key_": "vfs.fs.size[/home/joe/,free]",
        "hostid": "30074",
        "type": 0,
        "value_type": 3,
        "interfaceid": "30084",
        "tags": [
            {
                "tag": "Disk usage"
            },
            {
                "tag": "Equipment",
                "value": "Workstation"
            }
        ],
        "delay": "30s"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "24758"
        ]
    },
    "id": 1
}
```

[comment]: # ({/1485b7ac-6ffb1f45})

[comment]: # ({91af8fa5-0f29886e})
#### Creating a host inventory item

Create a Zabbix agent item to populate the host's "OS" inventory field.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "item.create",
    "params": {
        "name": "uname",
        "key_": "system.uname",
        "hostid": "30021",
        "type": 0,
        "interfaceid": "30007",
        "value_type": 1,
        "delay": "10s",
        "inventory_link": 5
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "24759"
        ]
    },
    "id": 1
}
```

[comment]: # ({/91af8fa5-0f29886e})

[comment]: # ({aac399db-f45b1a9a})
#### Creating an item with preprocessing

Create an item using custom multiplier.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "item.create",
    "params": {
        "name": "Device uptime",
        "key_": "sysUpTime",
        "hostid": "11312",
        "type": 4,
        "snmp_oid": "SNMPv2-MIB::sysUpTime.0",
        "value_type": 1,
        "delay": "60s",
        "units": "uptime",
        "interfaceid": "1156",
        "preprocessing": [
            {
                "type": 1,
                "params": "0.01",
                "error_handler": 1,
                "error_handler_params": ""
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "44210"
        ]
    },
    "id": 1
}
```

[comment]: # ({/aac399db-f45b1a9a})

[comment]: # ({166357cc-6a32a64f})
#### Creating dependent item

Create a dependent item for the master item with ID 24759. Only
dependencies on the same host are allowed, therefore master and the
dependent item should have the same hostid.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "item.create",
    "params": {
      "hostid": "30074",
      "name": "Dependent test item",
      "key_": "dependent.item",
      "type": 18,
      "master_itemid": "24759",
      "value_type": 2
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "44211"
        ]
    },
    "id": 1
}
```

[comment]: # ({/166357cc-6a32a64f})

[comment]: # ({e8d3a1df-70eb9eae})
#### Create HTTP agent item

Create POST request method item with JSON response preprocessing.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "item.create",
    "params": {
        "url":"http://127.0.0.1/http.php",
        "query_fields": [
            {
                "mode":"json"
            },
            {
                "min": "10"
            },
            {
                "max": "100"
            }
        ],
        "interfaceid": "1",
        "type": 19,
        "hostid": "10254",
        "delay": "5s",
        "key_": "json",
        "name": "HTTP agent example JSON",
        "value_type": 0,
        "output_format": 1,
        "preprocessing": [
            {
                "type": 12,
                "params": "$.random",
                "error_handler": 0,
                "error_handler_params": ""
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "23865"
        ]
    },
    "id": 1
}
```

[comment]: # ({/e8d3a1df-70eb9eae})

[comment]: # ({568de3f0-313c9999})
#### Create script item

Create a simple data collection using a script item.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "item.create",
    "params": {
        "name": "Script example",
        "key_": "custom.script.item",
        "hostid": "12345",
        "type": 21,
        "value_type": 4,
        "params": "var request = new HttpRequest();\nreturn request.post(\"https://postman-echo.com/post\", JSON.parse(value));",
        "parameters": [
            {
                "name": "host",
                "value": "{HOST.CONN}"
            }
        ],
        "timeout": "6s",
        "delay": "30s"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "23865"
        ]
    },
    "id": 1
}
```

[comment]: # ({/568de3f0-313c9999})

[comment]: # ({ecb97b1d-ecb97b1d})
### Source

CItem::create() in *ui/include/classes/api/services/CItem.php*.

[comment]: # ({/ecb97b1d-ecb97b1d})
