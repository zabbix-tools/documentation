[comment]: # ({cd0e20e4-cd0e20e4})
# item.delete

[comment]: # ({/cd0e20e4-cd0e20e4})

[comment]: # ({f4397773-16951644})
### Description

`object item.delete(array itemIds)`

This method allows to delete items.

::: noteclassic
Web items cannot be deleted via the Zabbix API.
:::

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/f4397773-16951644})

[comment]: # ({3574199c-3574199c})
### Parameters

`(array)` IDs of the items to delete.

[comment]: # ({/3574199c-3574199c})

[comment]: # ({b676662e-b676662e})
### Return values

`(object)` Returns an object containing the IDs of the deleted items
under the `itemids` property.

[comment]: # ({/b676662e-b676662e})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({23911e9d-92fe5da6})
#### Deleting multiple items

Delete two items.\
Dependent items and item prototypes are removed automatically if master
item is deleted.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "item.delete",
    "params": [
        "22982",
        "22986"
    ],
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "22982",
            "22986"
        ]
    },
    "id": 1
}
```

[comment]: # ({/23911e9d-92fe5da6})

[comment]: # ({4c7d30be-4c7d30be})
### Source

CItem::delete() in *ui/include/classes/api/services/CItem.php*.

[comment]: # ({/4c7d30be-4c7d30be})
