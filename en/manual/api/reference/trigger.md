[comment]: # ({1a75a2f5-9dae9ddc})
# Trigger

This class is designed to work with triggers.

Object references:

-   [Trigger](/manual/api/reference/trigger/object#trigger)
-   [Trigger tag](/manual/api/reference/trigger/object#trigger_tag)

Available methods:

-   [trigger.create](/manual/api/reference/trigger/create) - create new triggers
-   [trigger.delete](/manual/api/reference/trigger/delete) - delete triggers
-   [trigger.get](/manual/api/reference/trigger/get) - retrieve triggers
-   [trigger.update](/manual/api/reference/trigger/update) - update triggers

[comment]: # ({/1a75a2f5-9dae9ddc})
