[comment]: # ({287fa0f2-03e77d6d})
# Dashboard

This class is designed to work with dashboards.

Object references:

-   [Dashboard](/manual/api/reference/dashboard/object#dashboard)
-   [Dashboard page](/manual/api/reference/dashboard/object#dashboard_page)
-   [Dashboard widget](/manual/api/reference/dashboard/object#dashboard_widget)
-   [Dashboard widget field](/manual/api/reference/dashboard/object#dashboard_widget_field)
-   [Dashboard user group](/manual/api/reference/dashboard/object#dashboard_user_group)
-   [Dashboard user](/manual/api/reference/dashboard/object#dashboard_user)

Available methods:

-   [dashboard.create](/manual/api/reference/dashboard/create) - create new dashboards
-   [dashboard.delete](/manual/api/reference/dashboard/delete) - delete dashboards
-   [dashboard.get](/manual/api/reference/dashboard/get) - retrieve dashboards
-   [dashboard.update](/manual/api/reference/dashboard/update) - update dashboards

[comment]: # ({/287fa0f2-03e77d6d})
