[comment]: # ({262a4555-06845577})
# Action

This class is designed to work with actions.

Object references:

-   [Action](/manual/api/reference/action/object#action)
-   [Action operation](/manual/api/reference/action/object#action_operation)
-   [Action operation message](/manual/api/reference/action/object#action_operation_message)
-   [Action operation condition](/manual/api/reference/action/object#action_operation_condition)
-   [Action recovery operation](/manual/api/reference/action/object#action_recovery_operation)
-   [Action update operation](/manual/api/reference/action/object#action_update_operation)
-   [Action filter](/manual/api/reference/action/object#action_filter)
-   [Action filter condition](/manual/api/reference/action/object#action_filter_condition)

Available methods:

-   [action.create](/manual/api/reference/action/create) - create new actions
-   [action.delete](/manual/api/reference/action/delete) - delete actions
-   [action.get](/manual/api/reference/action/get) - retrieve actions
-   [action.update](/manual/api/reference/action/update) - update actions

[comment]: # ({/262a4555-06845577})
