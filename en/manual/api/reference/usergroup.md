[comment]: # ({9b9c999a-f92dbd28})
# User group

This class is designed to work with user groups.

Object references:

-   [User group](/manual/api/reference/usergroup/object#user_group)
-   [Permission](/manual/api/reference/usergroup/object#permission)
-   [Tag-based permission](/manual/api/reference/usergroup/object#tag_based_permission)

Available methods:

-   [usergroup.create](/manual/api/reference/usergroup/create) - create new user groups
-   [usergroup.delete](/manual/api/reference/usergroup/delete) - delete user groups
-   [usergroup.get](/manual/api/reference/usergroup/get) - retrieve user groups
-   [usergroup.update](/manual/api/reference/usergroup/update) - update user groups

[comment]: # ({/9b9c999a-f92dbd28})
