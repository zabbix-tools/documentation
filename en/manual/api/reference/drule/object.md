[comment]: # ({141f308b-141f308b})
# > Discovery rule object

The following objects are directly related to the `drule` API.

[comment]: # ({/141f308b-141f308b})

[comment]: # ({02d4849c-8884a1d1})
### Discovery rule

The discovery rule object defines a network discovery rule. It has the
following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|druleid|string|ID of the discovery rule.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*<br>- *required* for update operations|
|iprange|string|One or several IP ranges to check, separated by commas.<br><br>Refer to the [network discovery configuration](/manual/discovery/network_discovery/rule) section for more information on supported formats of IP ranges.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* for create operations|
|name|string|Name of the discovery rule.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* for create operations|
|delay|string|Execution interval of the discovery rule.<br>Accepts seconds, time unit with suffix, or a user macro.<br><br>Default: 1h.|
|proxy\_hostid|string|ID of the proxy used for discovery.|
|status|integer|Whether the discovery rule is enabled.<br><br>Possible values:<br>0 - *(default)* enabled;<br>1 - disabled.|
|concurrency\_max|integer|Maximum number of concurrent checks per discovery rule.<br><br>Possible values:<br>0 - *(default)* unlimited number of checks;<br>1 - one check;<br>2-999 - custom number of checks.|

[comment]: # ({/02d4849c-8884a1d1})
