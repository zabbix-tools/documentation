[comment]: # ({dbbaf81a-dbbaf81a})
# drule.get

[comment]: # ({/dbbaf81a-dbbaf81a})

[comment]: # ({6a314bbc-bbd48f20})
### Description

`integer/array drule.get(object parameters)`

The method allows to retrieve discovery rules according to the given
parameters.

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/6a314bbc-bbd48f20})

[comment]: # ({fb6ee9fa-0b971cc5})
### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|dhostids|string/array|Return only discovery rules that created the given discovered hosts.|
|druleids|string/array|Return only discovery rules with the given IDs.|
|dserviceids|string/array|Return only discovery rules that created the given discovered services.|
|selectDChecks|query|Return a [`dchecks`](/manual/api/reference/dcheck/object) property with the discovery checks used by the discovery rule.<br><br>Supports `count`.|
|selectDHosts|query|Return a [`dhosts`](/manual/api/reference/dhost/object) property with the discovered hosts created by the discovery rule.<br><br>Supports `count`.|
|limitSelects|integer|Limits the number of records returned by subselects.<br><br>Applies to the following subselects:<br>`selectDChecks` - results will be sorted by `dcheckid`;<br>`selectDHosts` - results will be sorted by `dhostsid`.|
|sortfield|string/array|Sort the result by the given properties.<br><br>Possible values: `druleid`, `name`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters).|
|editable|boolean|^|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

[comment]: # ({/fb6ee9fa-0b971cc5})

[comment]: # ({7223bab1-7223bab1})
### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.

[comment]: # ({/7223bab1-7223bab1})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({6899ee92-153ca11b})
#### Retrieve all discovery rules

Retrieve all configured discovery rules and the discovery checks they
use.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "drule.get",
    "params": {
        "output": "extend",
        "selectDChecks": "extend"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": [
        {
            "druleid": "2",
            "proxy_hostid": "0",
            "name": "Local network",
            "iprange": "192.168.3.1-255",
            "delay": "5s",
            "status": "0",
            "concurrency_max": "0",
            "dchecks": [
                {
                    "dcheckid": "7",
                    "druleid": "2",
                    "type": "3",
                    "key_": "",
                    "snmp_community": "",
                    "ports": "21",
                    "snmpv3_securityname": "",
                    "snmpv3_securitylevel": "0",
                    "snmpv3_authpassphrase": "",
                    "snmpv3_privpassphrase": "",
                    "uniq": "0",
                    "snmpv3_authprotocol": "0",
                    "snmpv3_privprotocol": "0",
                    "snmpv3_contextname": "",
                    "host_source": "1",
                    "name_source": "0",
                    "allow_redirect": "0"
                },
                {
                    "dcheckid": "8",
                    "druleid": "2",
                    "type": "4",
                    "key_": "",
                    "snmp_community": "",
                    "ports": "80",
                    "snmpv3_securityname": "",
                    "snmpv3_securitylevel": "0",
                    "snmpv3_authpassphrase": "",
                    "snmpv3_privpassphrase": "",
                    "uniq": "0",
                    "snmpv3_authprotocol": "0",
                    "snmpv3_privprotocol": "0",
                    "snmpv3_contextname": "",
                    "host_source": "1",
                    "name_source": "0",
                    "allow_redirect": "0"
                }
            ]
        },
        {
            "druleid": "6",
            "proxy_hostid": "0",
            "name": "Zabbix agent discovery",
            "iprange": "192.168.1.1-255",
            "delay": "1h",
            "status": "0",
            "concurrency_max": "10",
            "dchecks": [
                {
                    "dcheckid": "10",
                    "druleid": "6",
                    "type": "9",
                    "key_": "system.uname",
                    "snmp_community": "",
                    "ports": "10050",
                    "snmpv3_securityname": "",
                    "snmpv3_securitylevel": "0",
                    "snmpv3_authpassphrase": "",
                    "snmpv3_privpassphrase": "",
                    "uniq": "0",
                    "snmpv3_authprotocol": "0",
                    "snmpv3_privprotocol": "0",
                    "snmpv3_contextname": "",
                    "host_source": "2",
                    "name_source": "3",
                    "allow_redirect": "0"
                }
            ]
        }
    ],
    "id": 1
}
```

[comment]: # ({/6899ee92-153ca11b})

[comment]: # ({f2f964c1-f2f964c1})
### See also

-   [Discovered
    host](/manual/api/reference/dhost/object#discovered_host)
-   [Discovery
    check](/manual/api/reference/dcheck/object#discovery_check)

[comment]: # ({/f2f964c1-f2f964c1})

[comment]: # ({365d15ae-365d15ae})
### Source

CDRule::get() in *ui/include/classes/api/services/CDRule.php*.

[comment]: # ({/365d15ae-365d15ae})
