[comment]: # ({0a1af107-0a1af107})
# drule.update

[comment]: # ({/0a1af107-0a1af107})

[comment]: # ({c28bd5aa-ac17a9e2})
### Description

`object drule.update(object/array discoveryRules)`

This method allows to update existing discovery rules.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/c28bd5aa-ac17a9e2})

[comment]: # ({cdfb53ac-0dd5b0c8})
### Parameters

`(object/array)` Discovery rule properties to be updated.

The `druleid` property must be defined for each discovery rule, all
other properties are optional. Only the passed properties will be
updated, all others will remain unchanged.

Additionally to the [standard discovery rule
properties](object#discovery_rule), the method accepts the following
parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|dchecks|array|Discovery [checks](/manual/api/reference/dcheck/object) to replace existing checks.|

[comment]: # ({/cdfb53ac-0dd5b0c8})

[comment]: # ({495f933e-495f933e})
### Return values

`(object)` Returns an object containing the IDs of the updated discovery
rules under the `druleids` property.

[comment]: # ({/495f933e-495f933e})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({c912da13-11119b8e})
#### Change the IP range of a discovery rule

Change the IP range of a discovery rule to "192.168.2.1-255".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "drule.update",
    "params": {
        "druleid": "6",
        "iprange": "192.168.2.1-255"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "druleids": [
            "6"
        ]
    },
    "id": 1
}
```

[comment]: # ({/c912da13-11119b8e})

[comment]: # ({54664cce-54664cce})
### See also

-   [Discovery
    check](/manual/api/reference/dcheck/object#discovery_check)

[comment]: # ({/54664cce-54664cce})

[comment]: # ({d9252108-d9252108})
### Source

CDRule::update() in *ui/include/classes/api/services/CDRule.php*.

[comment]: # ({/d9252108-d9252108})
