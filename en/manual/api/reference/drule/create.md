[comment]: # ({8102730f-8102730f})
# drule.create

[comment]: # ({/8102730f-8102730f})

[comment]: # ({e51fc566-9e7d03a2})
### Description

`object drule.create(object/array discoveryRules)`

This method allows to create new discovery rules.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/e51fc566-9e7d03a2})

[comment]: # ({e1ac6ddc-7afdb3b3})
### Parameters

`(object/array)` Discovery rules to create.

Additionally to the [standard discovery rule
properties](object#discovery_rule), the method accepts the following
parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|dchecks|array|Discovery [checks](/manual/api/reference/dcheck/object) to create for the discovery rule.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required*|

[comment]: # ({/e1ac6ddc-7afdb3b3})

[comment]: # ({8b5259e3-2eb6d0d5})
### Return values

`(object)` Returns an object containing the IDs of the created
discovery rules under the `druleids` property. The order of the
returned IDs matches the order of the passed discovery rules.

[comment]: # ({/8b5259e3-2eb6d0d5})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({fd18072b-b03b87c1})
#### Create a discovery rule

Create a discovery rule to find machines running the Zabbix agent in the
local network. The rule must use a single Zabbix agent check on port
10050.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "drule.create",
    "params": {
        "name": "Zabbix agent discovery",
        "iprange": "192.168.1.1-255",
        "concurrency_max": "10",
        "dchecks": [
            {
                "type": "9",
                "key_": "system.uname",
                "ports": "10050",
                "uniq": "0"
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "druleids": [
            "6"
        ]
    },
    "id": 1
}
```

[comment]: # ({/fd18072b-b03b87c1})

[comment]: # ({54664cce-54664cce})
### See also

-   [Discovery
    check](/manual/api/reference/dcheck/object#discovery_check)

[comment]: # ({/54664cce-54664cce})

[comment]: # ({1333854c-1333854c})
### Source

CDRule::create() in *ui/include/classes/api/services/CDRule.php*.

[comment]: # ({/1333854c-1333854c})
