[comment]: # ({f125871b-f125871b})
# drule.delete

[comment]: # ({/f125871b-f125871b})

[comment]: # ({88b5ebf1-2430e072})
### Description

`object drule.delete(array discoveryRuleIds)`

This method allows to delete discovery rules.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/88b5ebf1-2430e072})

[comment]: # ({5b654054-5b654054})
### Parameters

`(array)` IDs of the discovery rules to delete.

[comment]: # ({/5b654054-5b654054})

[comment]: # ({35b8a89c-35b8a89c})
### Return values

`(object)` Returns an object containing the IDs of the deleted discovery
rules under the `druleids` property.

[comment]: # ({/35b8a89c-35b8a89c})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({615d2e2f-4df0ae86})
#### Delete multiple discovery rules

Delete two discovery rules.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "drule.delete",
    "params": [
        "4",
        "6"
    ],
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "druleids": [
            "4",
            "6"
        ]
    },
    "id": 1
}
```

[comment]: # ({/615d2e2f-4df0ae86})

[comment]: # ({89517129-89517129})
### Source

CDRule::delete() in *ui/include/classes/api/services/CDRule.php*.

[comment]: # ({/89517129-89517129})
