[comment]: # ({7292ec25-68e053db})
# Report

This class is designed to work with scheduled reports.

Object references:

-   [Report](/manual/api/reference/report/object#report)
-   [Users](/manual/api/reference/report/object#users)
-   [User groups](/manual/api/reference/report/object#user_groups)

Available methods:

-   [report.create](/manual/api/reference/report/create) - create new scheduled reports
-   [report.delete](/manual/api/reference/report/delete) - delete scheduled reports
-   [report.get](/manual/api/reference/report/get) - retrieve scheduled reports
-   [report.update](/manual/api/reference/report/update) - update scheduled reports

[comment]: # ({/7292ec25-68e053db})
