[comment]: # ({7cbac97f-7cbac97f})
# report.delete

[comment]: # ({/7cbac97f-7cbac97f})

[comment]: # ({acb60529-c6ffcbad})
### Description

`object report.delete(array reportids)`

This method allows to delete scheduled reports.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user type. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/acb60529-c6ffcbad})

[comment]: # ({d8777019-d8777019})
### Parameters

`(array)` IDs of the scheduled reports to delete.

[comment]: # ({/d8777019-d8777019})

[comment]: # ({ee60543b-ee60543b})
### Return values

`(object)` Returns an object containing the IDs of the deleted scheduled
reports under the `reportids` property.

[comment]: # ({/ee60543b-ee60543b})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({1a799b75-71c35868})
#### Deleting multiple scheduled reports

Delete two scheduled reports.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "report.delete",
    "params": [
        "1",
        "2"
    ],
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "reportids": [
            "1",
            "2"
        ]
    },
    "id": 1
}
```

[comment]: # ({/1a799b75-71c35868})

[comment]: # ({3c3c511e-3c3c511e})
### Source

CReport::delete() in *ui/include/classes/api/services/CReport.php*.

[comment]: # ({/3c3c511e-3c3c511e})
