[comment]: # ({9793e988-9793e988})
# report.create

[comment]: # ({/9793e988-9793e988})

[comment]: # ({ea36381d-a8301e64})
### Description

`object report.create(object/array reports)`

This method allows to create new scheduled reports.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/ea36381d-a8301e64})

[comment]: # ({aee0f0ca-869414cf})
### Parameters

`(object/array)` Scheduled reports to create.

Additionally to the [standard scheduled report
properties](object#report), the method accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|users|object/array of objects|[Users](object#users) to send the report to.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required* if `user_groups` is not set|
|user\_groups|object/array of objects|[User groups](object#user_groups) to send the report to.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required* if `users` is not set|

[comment]: # ({/aee0f0ca-869414cf})

[comment]: # ({05ef7a50-05ef7a50})
### Return values

`(object)` Returns an object containing the IDs of the created scheduled
reports under the `reportids` property. The order of the returned IDs
matches the order of the passed scheduled reports.

[comment]: # ({/05ef7a50-05ef7a50})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({05ea3a37-1cfccf99})
#### Creating a scheduled report

Create a weekly report that will be prepared for the previous week every
Monday-Friday at 12:00 from 2021-04-01 to 2021-08-31.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "report.create",
    "params": {
        "userid": "1",
        "name": "Weekly report",
        "dashboardid": "1",
        "period": "1",
        "cycle": "1",
        "start_time": "43200",
        "weekdays": "31",
        "active_since": "2021-04-01",
        "active_till": "2021-08-31",
        "subject": "Weekly report",
        "message": "Report accompanying text",
        "status": "1",
        "description": "Report description",
        "users": [
            {
                "userid": "1",
                "access_userid": "1",
                "exclude": "0"
            },
            {
                "userid": "2",
                "access_userid": "0",
                "exclude": "1"
            }
        ],
        "user_groups": [
            {
                "usrgrpid": "7",
                "access_userid": "0"
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "reportids": [
            "1"
        ]
    },
    "id": 1
}
```

[comment]: # ({/05ea3a37-1cfccf99})

[comment]: # ({c52572c8-c52572c8})
### See also

-   [Users](object#users)
-   [User groups](object#user_groups)

[comment]: # ({/c52572c8-c52572c8})

[comment]: # ({f8046691-f8046691})
### Source

CReport::create() in *ui/include/classes/api/services/CReport.php*.

[comment]: # ({/f8046691-f8046691})
