[comment]: # ({fde2c9b2-fde2c9b2})
# report.update

[comment]: # ({/fde2c9b2-fde2c9b2})

[comment]: # ({4abe9d67-3aeb5e9e})
### Description

`object report.update(object/array reports)`

This method allows to update existing scheduled reports.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user type. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/4abe9d67-3aeb5e9e})

[comment]: # ({7fa609f5-e2e33f61})
### Parameters

`(object/array)` Scheduled report properties to be updated.

The `reportid` property must be defined for each scheduled report, all
other properties are optional. Only the passed properties will be
updated, all others will remain unchanged.

Additionally to the [standard scheduled report
properties](object#report) the method accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|users|object/array of objects|[Users](object#users) to replace the current users assigned to the scheduled report.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required* if `user_groups` is not set|
|user\_groups|object/array of objects|[User groups](object#user_groups) to replace the current user groups assigned to the scheduled report.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required* if `users` is not set|

[comment]: # ({/7fa609f5-e2e33f61})

[comment]: # ({de75bc91-de75bc91})
### Return values

`(object)` Returns an object containing the IDs of the updated scheduled
reports under the `reportids` property.

[comment]: # ({/de75bc91-de75bc91})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({b38759b3-3f9e0b32})
#### Disabling scheduled report

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "report.update",
    "params": {
        "reportid": "1",
        "status": "0"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "reportids": [
            "1"
        ]
    },
    "id": 1
}
```

[comment]: # ({/b38759b3-3f9e0b32})

[comment]: # ({c52572c8-c52572c8})
### See also

-   [Users](object#users)
-   [User groups](object#user_groups)

[comment]: # ({/c52572c8-c52572c8})

[comment]: # ({74d81ce5-74d81ce5})
### Source

CReport::update() in *ui/include/classes/api/services/CReport.php*.

[comment]: # ({/74d81ce5-74d81ce5})
