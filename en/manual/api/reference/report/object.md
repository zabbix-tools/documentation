[comment]: # ({6b675424-6b675424})
# > Report object

The following objects are directly related to the `report` API.

[comment]: # ({/6b675424-6b675424})

[comment]: # ({949f0163-94caf5ac})
### Report

The report object has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|reportid|string|ID of the report.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*<br>- *required* for update operations|
|userid|string|ID of the user who created the report.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* for create operations|
|name|string|Unique name of the report.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* for create operations|
|dashboardid|string|ID of the dashboard that the report is based on.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* for create operations|
|period|integer|Period for which the report will be prepared.<br><br>Possible values:<br>0 - *(default)* previous day;<br>1 - previous week;<br>2 - previous month;<br>3 - previous year.|
|cycle|integer|Period repeating schedule.<br><br>Possible values:<br>0 - *(default)* daily;<br>1 - weekly;<br>2 - monthly;<br>3 - yearly.|
|start\_time|integer|Time of the day, in seconds, when the report will be prepared for sending.<br><br>Default: 0.|
|weekdays|integer|Days of the week for sending the report.<br><br>Days of the week are stored in binary form with each bit representing the corresponding week day. For example, 12 equals 1100 in binary and means that reports will be sent every Wednesday and Thursday.<br><br>Default: 0.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `cycle` is set to "weekly".|
|active\_since|string|On which date to start.<br><br>Possible values:<br>empty string - *(default)* not specified (stored as 0);<br>specific date in YYYY-MM-DD format (stored as a timestamp of the beginning of a day (00:00:00)).|
|active\_till|string|On which date to end.<br><br>Possible values:<br>empty string - *(default)* not specified (stored as 0);<br>specific date in YYYY-MM-DD format (stored as a timestamp of the end of a day (23:59:59)).|
|subject|string|Report message subject.|
|message|string|Report message text.|
|status|integer|Whether the report is enabled or disabled.<br><br>Possible values:<br>0 - Disabled;<br>1 - *(default)* Enabled.|
|description|text|Description of the report.|
|state|integer|State of the report.<br><br>Possible values:<br>0 - *(default)* report was not yet processed;<br>1 - report was generated and successfully sent to all recipients;<br>2 - report generating failed; "info" contains error information;<br>3 - report was generated, but sending to some (or all) recipients failed; "info" contains error information.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*|
|lastsent|timestamp|Unix timestamp of the last successfully sent report.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*|
|info|string|Error description or additional information.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*|

[comment]: # ({/949f0163-94caf5ac})

[comment]: # ({38f01807-b2778637})
### Users

The users object has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|userid|string|ID of user to send the report to.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|access\_userid|string|ID of user on whose behalf the report will be generated.<br><br>0 - *(default)* Generate report by recipient.|
|exclude|integer|Whether to exclude the user from mailing list.<br><br>Possible values:<br>0 - *(default)* Include;<br>1 - Exclude.|

[comment]: # ({/38f01807-b2778637})

[comment]: # ({5498e274-483e81dc})
### User groups

The user groups object has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|usrgrpid|string|ID of user group to send the report to.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|access\_userid|string|ID of user on whose behalf the report will be generated.<br><br>0 - *(default)* Generate report by recipient.|

[comment]: # ({/5498e274-483e81dc})
