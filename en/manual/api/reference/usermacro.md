[comment]: # ({24808e49-bf4e6957})
# User macro

This class is designed to work with host-level and global user macros.

Object references:

-   [Global macro](/manual/api/reference/usermacro/object#global_macro)
-   [Host macro](/manual/api/reference/usermacro/object#host_macro)

Available methods:

-   [usermacro.create](/manual/api/reference/usermacro/create) - create new host macros
-   [usermacro.createglobal](/manual/api/reference/usermacro/createglobal) - create new global macros
-   [usermacro.delete](/manual/api/reference/usermacro/delete) - delete host macros
-   [usermacro.deleteglobal](/manual/api/reference/usermacro/deleteglobal) - delete global macros
-   [usermacro.get](/manual/api/reference/usermacro/get) - retrieve host and global macros
-   [usermacro.update](/manual/api/reference/usermacro/update) - update host macros
-   [usermacro.updateglobal](/manual/api/reference/usermacro/updateglobal) - update global macros

[comment]: # ({/24808e49-bf4e6957})
