[comment]: # ({19b577f7-14213bb4})
# Discovered service

This class is designed to work with discovered services.

Object references:

-   [Discovered service](/manual/api/reference/dservice/object#discovered_service)

Available methods:

-   [dservice.get](/manual/api/reference/dservice/get) - retrieve discovered services

[comment]: # ({/19b577f7-14213bb4})
