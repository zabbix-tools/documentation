[comment]: # ({ce0a697a-7e2bad53})
# Item prototype

This class is designed to work with item prototypes.

Object references:

-   [Item prototype](/manual/api/reference/itemprototype/object#item_prototype)
-   [Item prototype tag](/manual/api/reference/itemprototype/object#item_prototype_tag)
-   [Item prototype preprocessing](/manual/api/reference/itemprototype/object#item_prototype_preprocessing)

Available methods:

-   [itemprototype.create](/manual/api/reference/itemprototype/create) - create new item prototypes
-   [itemprototype.delete](/manual/api/reference/itemprototype/delete) - delete item prototypes
-   [itemprototype.get](/manual/api/reference/itemprototype/get) - retrieve item prototypes
-   [itemprototype.update](/manual/api/reference/itemprototype/update) - update item prototypes

[comment]: # ({/ce0a697a-7e2bad53})
