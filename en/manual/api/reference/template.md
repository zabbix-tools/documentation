[comment]: # ({854b7ad9-4f0fdd87})
# Template

This class is designed to work with templates.

Object references:

-   [Template](/manual/api/reference/template/object#template)
-   [Template tag](/manual/api/reference/template/object#template_tag)

Available methods:

-   [template.create](/manual/api/reference/template/create) - create new templates
-   [template.delete](/manual/api/reference/template/delete) - delete templates
-   [template.get](/manual/api/reference/template/get) - retrieve templates
-   [template.massadd](/manual/api/reference/template/massadd) - add related objects to templates
-   [template.massremove](/manual/api/reference/template/massremove) - remove related objects from templates
-   [template.massupdate](/manual/api/reference/template/massupdate) - replace or remove related objects from templates
-   [template.update](/manual/api/reference/template/update) - update templates

[comment]: # ({/854b7ad9-4f0fdd87})
