[comment]: # ({8b346cf3-f9e8f670})
# templategroup.massremove

[comment]: # ({/8b346cf3-f9e8f670})

[comment]: # ({d7f16b22-b5ff62ed})
### Description

`object templategroup.massremove(object parameters)`

This method allows to remove related objects from multiple template groups.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/d7f16b22-b5ff62ed})

[comment]: # ({27e84655-6102edd8})
### Parameters

`(object)` Parameters containing the IDs of the template groups to update
and the objects that should be removed.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|groupids|string/array|IDs of the template groups to be updated.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required*|
|templateids|string/array|Templates to remove from all template groups.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required*|

[comment]: # ({/27e84655-6102edd8})

[comment]: # ({9cf84bdf-2acda80b})
### Return values

`(object)` Returns an object containing the IDs of the updated template
groups under the `groupids` property.

[comment]: # ({/9cf84bdf-2acda80b})

[comment]: # ({b41637d2-6044f589})
### Examples

[comment]: # ({/b41637d2-6044f589})

[comment]: # ({22cd8076-2f48a174})
#### Removing templates from template groups

Remove two templates from the given template groups.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "hostgroup.massremove",
    "params": {
        "groupids": [
            "5",
            "6"
        ],
        "hostids": [
            "30050",
            "30001"
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "groupids": [
            "5",
            "6"
        ]
    },
    "id": 1
}
```

[comment]: # ({/22cd8076-2f48a174})

[comment]: # ({cb632db0-1d275edf})
### Source

CTemplateGroup::massRemove() in
*ui/include/classes/api/services/CTemplateGroup.php*.

[comment]: # ({/cb632db0-1d275edf})
