[comment]: # ({c5e721d8-f9e8f670})
# templategroup.create

[comment]: # ({/c5e721d8-f9e8f670})

[comment]: # ({e21431df-b5ff62ed})
### Description

`object templategroup.create(object/array templateGroups)`

This method allows to create new template groups.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/e21431df-b5ff62ed})

[comment]: # ({e4a03979-6102edd8})
### Parameters

`(object/array)` Template groups to create. The method accepts template groups
with the [standard template group properties](object#template_group).

[comment]: # ({/e4a03979-6102edd8})

[comment]: # ({cd6782ad-76d53254})
### Return values

`(object)` Returns an object containing the IDs of the created template
groups under the `groupids` property. The order of the returned IDs
matches the order of the passed template groups.

[comment]: # ({/cd6782ad-76d53254})

[comment]: # ({b41637d2-e8653e4d})
### Examples

[comment]: # ({/b41637d2-e8653e4d})

[comment]: # ({c2e0e8c7-1aca44e8})
#### Creating a template group

Create a template group called "Templates/Databases".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "templategroup.create",
    "params": {
        "name": "Templates/Databases"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "groupids": [
            "107820"
        ]
    },
    "id": 1
}
```

[comment]: # ({/c2e0e8c7-1aca44e8})

[comment]: # ({f3aa7eca-225e8cd3})
### Source

CTemplateGroup::create() in
*ui/include/classes/api/services/CTemplateGroup.php*.

[comment]: # ({/f3aa7eca-225e8cd3})
