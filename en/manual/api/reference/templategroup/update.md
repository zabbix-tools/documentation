[comment]: # ({08ca2ccd-f9e8f670})
# templategroup.update

[comment]: # ({/08ca2ccd-f9e8f670})

[comment]: # ({3ee92a94-b5ff62ed})
### Description

`object templategroup.update(object/array templateGroups)`

This method allows to update existing template groups.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/3ee92a94-b5ff62ed})

[comment]: # ({d3110348-6102edd8})
### Parameters

`(object/array)` [Template group properties](object#template_group) to be
updated.

The `groupid` property must be defined for each template group, all other
properties are optional. Only the given properties will be updated, all
others will remain unchanged.

[comment]: # ({/d3110348-6102edd8})

[comment]: # ({9cf84bdf-1275df2c})
### Return values

`(object)` Returns an object containing the IDs of the updated template
groups under the `groupids` property.

[comment]: # ({/9cf84bdf-1275df2c})

[comment]: # ({b41637d2-9341f38c})
### Examples

[comment]: # ({/b41637d2-9341f38c})

[comment]: # ({085ee377-8889f8e9})
#### Renaming a template group

Rename a template group to "Templates/Databases"

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "templategroup.update",
    "params": {
        "groupid": "7",
        "name": "Templates/Databases"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "groupids": [
            "7"
        ]
    },
    "id": 1
}
```

[comment]: # ({/085ee377-8889f8e9})

[comment]: # ({7ae098d5-931c9c76})
### Source

CTemplateGroup::update() in
*ui/include/classes/api/services/CTemplateGroup.php*.

[comment]: # ({/7ae098d5-931c9c76})
