[comment]: # ({011dac20-f9e8f670})
# templategroup.propagate


[comment]: # ({/011dac20-f9e8f670})



[comment]: # ({d92be2e3-b5ff62ed})
### Description

`object templategroup.propagate(object parameters)`

This method allows to apply permissions to all template groups' subgroups.

::: noteclassic
This method is only available to *Super admin* user types.
Permissions to call the method can be revoked in user role settings.
See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::


[comment]: # ({/d92be2e3-b5ff62ed})



[comment]: # ({cd55cdfd-6102edd8})
### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|groups|object/array|Template groups to propagate.<br><br>The template groups must have the `groupid` property defined.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required*|
|permissions|boolean|Set `true` if need to propagate permissions.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required*|


[comment]: # ({/cd55cdfd-6102edd8})



[comment]: # ({9ef1e089-fac1a1ce})
### Return values

`(object)` Returns an object containing the IDs of the propagated template
groups under the `groupids` property.


[comment]: # ({/9ef1e089-fac1a1ce})



[comment]: # ({b41637d2-0ed127f4})
### Examples


[comment]: # ({/b41637d2-0ed127f4})



[comment]: # ({66027f30-58284f05})
#### Propagating template group permissions to its subgroups.

Propagate template group permissions to its subgroups.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "templategroup.propagate",
    "params": {
        "groups": [
            {
                "groupid": "15"
            }
        ],
        "permissions": true
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "groupids": [
            "15",
        ]
    },
    "id": 1
}
```


[comment]: # ({/66027f30-58284f05})



[comment]: # ({00455354-de4769f6})
### See also

-   [templategroup.update](update)
-   [templategroup.massadd](massadd)
-   [Template](/manual/api/reference/template/object#template)


[comment]: # ({/00455354-de4769f6})



[comment]: # ({7cdbc801-870c4596})
### Source

CTemplateGroup::propagate() in
*ui/include/classes/api/services/CTemplateGroup.php*.


[comment]: # ({/7cdbc801-870c4596})
