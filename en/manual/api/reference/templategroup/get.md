[comment]: # ({c7f20528-f9e8f670})
# templategroup.get

[comment]: # ({/c7f20528-f9e8f670})

[comment]: # ({97635317-b5ff62ed})
### Description

`integer/array templategroup.get(object parameters)`

The method allows to retrieve template groups according to the given
parameters.

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/97635317-b5ff62ed})

[comment]: # ({4733bc39-6102edd8})
### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|graphids|string/array|Return only template groups that contain templates with the given graphs.|
|groupids|string/array|Return only template groups with the given template group IDs.|
|templateids|string/array|Return only template groups that contain the given templates.|
|triggerids|string/array|Return only template groups that contain templates with the given triggers.|
|with\_graphs|flag|Return only template groups that contain templates with graphs.|
|with\_graph\_prototypes|flag|Return only template groups that contain templates with graph prototypes.|
|with\_httptests|flag|Return only template groups that contain templates with web checks.|
|with\_items|flag|Return only template groups that contain templates with items.<br><br>Overrides the `with_simple_graph_items` parameters.|
|with\_item\_prototypes|flag|Return only template groups that contain templates with item prototypes.<br><br>Overrides the `with_simple_graph_item_prototypes` parameter.|
|with\_simple\_graph\_item\_prototypes|flag|Return only template groups that contain templates with item prototypes, which are enabled for creation and have numeric type of information.|
|with\_simple\_graph\_items|flag|Return only template groups that contain templates with numeric items.|
|with\_templates|flag|Return only template groups that contain templates.|
|with\_triggers|flag|Return only template groups that contain templates with triggers.|
|selectTemplates|query|Return a [`templates`](/manual/api/reference/template/object) property with the templates that belong to the template group.<br><br>Supports `count`.|
|limitSelects|integer|Limits the number of records returned by subselects.<br><br>Applies to the following subselects:<br>`selectTemplates` - results will be sorted by `template`.|
|sortfield|string/array|Sort the result by the given properties.<br><br>Possible values: `groupid`, `name`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters) page.|
|editable|boolean|^|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

[comment]: # ({/4733bc39-6102edd8})

[comment]: # ({7223bab1-2bc18eb2})
### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.

[comment]: # ({/7223bab1-2bc18eb2})

[comment]: # ({b41637d2-802b3dd1})
### Examples

[comment]: # ({/b41637d2-802b3dd1})

[comment]: # ({3ca44c43-c32b9462})
#### Retrieving data by name

Retrieve all data about two template groups named "Templates/Databases" and
"Templates/Modules".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "templategroup.get",
    "params": {
        "output": "extend",
        "filter": {
            "name": [
                "Templates/Databases",
                "Templates/Modules"
            ]
        }
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": [
        {
            "groupid": "13",
            "name": "Templates/Databases",
            "uuid": "748ad4d098d447d492bb935c907f652f"
        },
        {
            "groupid": "8",
            "name": "Templates/Modules",
            "uuid": "57b7ae836ca64446ba2c296389c009b7"
        }
    ],
    "id": 1
}
```

[comment]: # ({/3ca44c43-c32b9462})

[comment]: # ({f3cd9368-585b4960})
### See also

-   [Template](/manual/api/reference/template/object#template)

[comment]: # ({/f3cd9368-585b4960})

[comment]: # ({9f94aa1e-31239fd5})
### Source

CTemplateGroup::get() in *ui/include/classes/api/services/CTemplateGroup.php*.

[comment]: # ({/9f94aa1e-31239fd5})
