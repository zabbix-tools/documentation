[comment]: # ({1f6c1203-f9e8f670})
# > Template group object

The following objects are directly related to the `templategroup` API.

[comment]: # ({/1f6c1203-f9e8f670})

[comment]: # ({675e0a35-ad144502})
### Template group

The template group object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|groupid|string|ID of the template group.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*<br>- *required* for update operations|
|name|string|Name of the template group.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* for create operations|
|uuid|string|Universal unique identifier, used for linking imported template groups to already existing ones. Auto-generated, if not given.|

[comment]: # ({/675e0a35-ad144502})
