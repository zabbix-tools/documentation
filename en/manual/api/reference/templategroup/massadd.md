[comment]: # ({8e295f21-f9e8f670})
# templategroup.massadd

[comment]: # ({/8e295f21-f9e8f670})

[comment]: # ({4eebb3e3-b5ff62ed})
### Description

`object templategroup.massadd(object parameters)`

This method allows to simultaneously add multiple related objects to all
the given template groups.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/4eebb3e3-b5ff62ed})

[comment]: # ({6ab2cb33-3ff1a765})
### Parameters

`(object)` Parameters containing the IDs of the template groups to update
and the objects to add to all the template groups.

The method accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|groups|object/array|Template groups to be updated.<br><br>The template groups must have the `groupid` property defined.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required*|
|templates|object/array|Templates to add to all template groups.<br><br>The templates must have the `templateid` property defined.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required*|

[comment]: # ({/6ab2cb33-3ff1a765})

[comment]: # ({9cf84bdf-cc7938a3})
### Return values

`(object)` Returns an object containing the IDs of the updated template
groups under the `groupids` property.

[comment]: # ({/9cf84bdf-cc7938a3})

[comment]: # ({b41637d2-518dc5ee})
### Examples

[comment]: # ({/b41637d2-518dc5ee})

[comment]: # ({d6d75ca0-3954c00e})
#### Adding templates to template groups

Add two templates to template groups with IDs 12 and 13.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "templategroup.massadd",
    "params": {
        "groups": [
            {
                "groupid": "12"
            },
            {
                "groupid": "13"
            }
        ],
        "templates": [
            {
                "templateid": "10486"
            },
            {
                "templateid": "10487"
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "groupids": [
            "12",
            "13"
        ]
    },
    "id": 1
}
```

[comment]: # ({/d6d75ca0-3954c00e})

[comment]: # ({f3cd9368-d05fe284})
### See also

-   [Template](/manual/api/reference/template/object#template)

[comment]: # ({/f3cd9368-d05fe284})

[comment]: # ({afff73ad-47263542})
### Source

CTemplateGroup::massAdd() in
*ui/include/classes/api/services/CTemplateGroup.php*.

[comment]: # ({/afff73ad-47263542})
