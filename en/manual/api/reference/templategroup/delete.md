[comment]: # ({29d349c1-f9e8f670})
# templategroup.delete

[comment]: # ({/29d349c1-f9e8f670})

[comment]: # ({e39f857d-b5ff62ed})
### Description

`object templategroup.delete(array templateGroupIds)`

This method allows to delete template groups.

A template group can not be deleted if it contains templates that belong to this group only.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/e39f857d-b5ff62ed})

[comment]: # ({cc11ccac-55384a41})
### Parameters

`(array)` IDs of the template groups to delete.

[comment]: # ({/cc11ccac-55384a41})

[comment]: # ({783f539f-d49e0667})
### Return values

`(object)` Returns an object containing the IDs of the deleted template
groups under the `groupids` property.

[comment]: # ({/783f539f-d49e0667})

[comment]: # ({b41637d2-e8653e4d})
### Examples

[comment]: # ({/b41637d2-e8653e4d})

[comment]: # ({461526cf-1aca44e8})
#### Deleting multiple template groups

Delete two template groups.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "templategroup.delete",
    "params": [
        "107814",
        "107815"
    ],
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "groupids": [
            "107814",
            "107815"
        ]
    },
    "id": 1
}
```

[comment]: # ({/461526cf-1aca44e8})

[comment]: # ({48ae452a-918ba5f6})
### Source

CTemplateGroup::delete() in
*ui/include/classes/api/services/CTemplateGroup.php*.

[comment]: # ({/48ae452a-918ba5f6})
