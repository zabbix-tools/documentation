<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="en" datatype="plaintext" original="manual/api/reference/correlation/object.md">
    <body>
      <trans-unit id="e205e572" xml:space="preserve">
        <source># &gt; Correlation object

The following objects are directly related to the `correlation` API.</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/api/reference/correlation/object.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="935ba6ef" xml:space="preserve">
        <source>### Correlation

The correlation object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|correlationid|string|ID of the correlation.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *read-only*&lt;br&gt;- *required* for update operations|
|name|string|Name of the correlation.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* for create operations|
|description|string|Description of the correlation.|
|status|integer|Whether the correlation is enabled or disabled.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* enabled;&lt;br&gt;1 - disabled.|</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/api/reference/correlation/object.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="d12c2169" xml:space="preserve">
        <source>### Correlation operation

The correlation operation object defines an operation that will be
performed when a correlation is executed. It has the following
properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|type|integer|Type of operation.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - close old events;&lt;br&gt;1 - close new event.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/api/reference/correlation/object.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="d0662dba" xml:space="preserve">
        <source>### Correlation filter

The correlation filter object defines a set of conditions that must be met to perform the configured correlation operations.
It has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|evaltype|integer|Filter condition evaluation method.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - and/or;&lt;br&gt;1 - and;&lt;br&gt;2 - or;&lt;br&gt;3 - custom expression.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|
|conditions|array|Set of [filter conditions](#correlation-filter-condition) to use for filtering results.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|
|eval\_formula|string|Generated expression that will be used for evaluating filter conditions. The expression contains IDs that reference specific filter conditions by its `formulaid`. The value of `eval_formula` is equal to the value of `formula` for filters with a custom expression.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *read-only*|
|formula|string|User-defined expression to be used for evaluating conditions of filters with a custom expression. The expression must contain IDs that reference specific filter conditions by its `formulaid`. The IDs used in the expression must exactly match the ones defined in the filter conditions: no condition can remain unused or omitted.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* if `evaltype` is set to "custom expression"|</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/api/reference/correlation/object.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="72ee5420" xml:space="preserve">
        <source>#### Correlation filter condition

The correlation filter condition object defines a specific condition
that must be checked before running the correlation operations.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|type|integer|Type of condition.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - old event tag;&lt;br&gt;1 - new event tag;&lt;br&gt;2 - new event host group;&lt;br&gt;3 - event tag pair;&lt;br&gt;4 - old event tag value;&lt;br&gt;5 - new event tag value.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|
|tag|string|Event tag (old or new).&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* if `type` is set to "old event tag", "new event tag", "old event tag value", or "new event tag value"|
|groupid|string|Host group ID.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* if `type` is set to "new event host group"|
|oldtag|string|Old event tag.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* if `type` is set to "event tag pair"|
|newtag|string|Old event tag.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* if `type` is set to "event tag pair"|
|value|string|Event tag (old or new) value.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* if `type` is set to "old event tag value" or "new event tag value"|
|formulaid|string|Arbitrary unique ID that is used to reference the condition from a custom expression. Can only contain capital-case letters. The ID must be defined by the user when modifying filter conditions, but will be generated anew when requesting them afterward.|
|operator|integer|Condition operator.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* if `type` is set to "new event host group", "old event tag value", or "new event tag value"|

::: notetip
To better understand how to use filters with various
types of expressions, see examples on the
[correlation.get](get#retrieve_correlations) and
[correlation.create](create#using_a_custom_expression_filter) method
pages.
:::

The following operators and values are supported for each condition
type.

|Condition|Condition name|Supported operators|Expected value|
|---------|--------------|-------------------|--------------|
|2|Host group|=, &lt;&gt;|Host group ID.|
|4|Old event tag value|=, &lt;&gt;, like, not like|string|
|5|New event tag value|=, &lt;&gt;, like, not like|string|</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/api/reference/correlation/object.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
    </body>
  </file>
</xliff>
