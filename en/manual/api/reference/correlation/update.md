[comment]: # ({c71a2e54-c71a2e54})
# correlation.update

[comment]: # ({/c71a2e54-c71a2e54})

[comment]: # ({277763f5-b9562236})
### Description

`object correlation.update(object/array correlations)`

This method allows to update existing correlations.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/277763f5-b9562236})

[comment]: # ({ce355bf6-326bc80e})
### Parameters

`(object/array)` Correlation properties to be updated.

The `correlationid` property must be defined for each correlation, all
other properties are optional. Only the passed properties will be
updated, all others will remain unchanged.

Additionally to the [standard correlation
properties](object#correlation), the method accepts the following
parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|filter|object|Correlation [filter](/manual/api/reference/correlation/object#correlation_filter) object to replace the current filter.|
|operations|array|Correlation [operations](/manual/api/reference/correlation/object#correlation_operation) to replace existing operations.|

[comment]: # ({/ce355bf6-326bc80e})

[comment]: # ({13e1dc13-13e1dc13})
### Return values

`(object)` Returns an object containing the IDs of the updated
correlations under the `correlationids` property.

[comment]: # ({/13e1dc13-13e1dc13})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({801b5d8b-67bb2dfb})
#### Disable correlation

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "correlation.update",
    "params": {
        "correlationid": "1",
        "status": "1"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "correlationids": [
            "1"
        ]
    },
    "id": 1
}
```

[comment]: # ({/801b5d8b-67bb2dfb})

[comment]: # ({df1d1004-d6ef1778})
#### Replace conditions, but keep the evaluation method

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "correlation.update",
    "params": {
        "correlationid": "1",
        "filter": {
            "conditions": [
                {
                    "type": 3,
                    "oldtag": "error",
                    "newtag": "ok"
                }
            ]
        }
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "correlationids": [
            "1"
        ]
    },
    "id": 1
}
```

[comment]: # ({/df1d1004-d6ef1778})

[comment]: # ({e7d15410-e7d15410})
### See also

-   [Correlation filter](object#correlation_filter)
-   [Correlation operation](object#correlation_operation)

[comment]: # ({/e7d15410-e7d15410})

[comment]: # ({40fa665f-40fa665f})
### Source

CCorrelation::update() in
*ui/include/classes/api/services/CCorrelation.php*.

[comment]: # ({/40fa665f-40fa665f})
