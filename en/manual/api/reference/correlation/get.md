[comment]: # ({7416ad19-7416ad19})
# correlation.get

[comment]: # ({/7416ad19-7416ad19})

[comment]: # ({6e13bc51-858936aa})
### Description

`integer/array correlation.get(object parameters)`

The method allows to retrieve correlations according to the given
parameters.

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/6e13bc51-858936aa})

[comment]: # ({1b895b23-06a650a0})
### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|correlationids|string/array|Return only correlations with the given IDs.|
|selectFilter|query|Return a [`filter`](/manual/api/reference/correlation/object#correlation_filter) property with the correlation conditions.|
|selectOperations|query|Return an [`operations`](/manual/api/reference/correlation/object#correlation_operation) property with the correlation operations.|
|sortfield|string/array|Sort the result by the given properties.<br><br>Possible values: `correlationid`, `name`, `status`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters).|
|editable|boolean|^|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

[comment]: # ({/1b895b23-06a650a0})

[comment]: # ({7223bab1-7223bab1})
### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.

[comment]: # ({/7223bab1-7223bab1})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({ef8be6d3-d14c4f20})
#### Retrieve correlations

Retrieve all configured correlations together with correlation
conditions and operations. The filter uses the "and/or" evaluation type,
so the `formula` property is empty and `eval_formula` is generated
automatically.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "correlation.get",
    "params": {
        "output": "extend",
        "selectOperations": "extend",
        "selectFilter": "extend"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": [
        {
            "correlationid": "1",
            "name": "Correlation 1",
            "description": "",
            "status": "0",
            "filter": {
                "evaltype": "0",
                "formula": "",
                "conditions": [
                    {
                        "type": "3",
                        "oldtag": "error",
                        "newtag": "ok",
                        "formulaid": "A"
                    }
                ],
                "eval_formula": "A"
            },
            "operations": [
                {
                    "type": "0"
                }
            ]
        }
    ],
    "id": 1
}
```

[comment]: # ({/ef8be6d3-d14c4f20})

[comment]: # ({e7d15410-e7d15410})
### See also

-   [Correlation filter](object#correlation_filter)
-   [Correlation operation](object#correlation_operation)

[comment]: # ({/e7d15410-e7d15410})

[comment]: # ({bbab04bd-bbab04bd})
### Source

CCorrelation::get() in
*ui/include/classes/api/services/CCorrelation.php*.

[comment]: # ({/bbab04bd-bbab04bd})
