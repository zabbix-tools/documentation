[comment]: # ({e205e572-e205e572})
# > Correlation object

The following objects are directly related to the `correlation` API.

[comment]: # ({/e205e572-e205e572})

[comment]: # ({d4571dd8-935ba6ef})
### Correlation

The correlation object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|correlationid|string|ID of the correlation.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*<br>- *required* for update operations|
|name|string|Name of the correlation.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* for create operations|
|description|string|Description of the correlation.|
|status|integer|Whether the correlation is enabled or disabled.<br><br>Possible values:<br>0 - *(default)* enabled;<br>1 - disabled.|

[comment]: # ({/d4571dd8-935ba6ef})

[comment]: # ({e242e1d8-d12c2169})
### Correlation operation

The correlation operation object defines an operation that will be
performed when a correlation is executed. It has the following
properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|type|integer|Type of operation.<br><br>Possible values:<br>0 - close old events;<br>1 - close new event.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|

[comment]: # ({/e242e1d8-d12c2169})

[comment]: # ({59d7af11-d0662dba})
### Correlation filter

The correlation filter object defines a set of conditions that must be met to perform the configured correlation operations.
It has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|evaltype|integer|Filter condition evaluation method.<br><br>Possible values:<br>0 - and/or;<br>1 - and;<br>2 - or;<br>3 - custom expression.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|conditions|array|Set of [filter conditions](#correlation-filter-condition) to use for filtering results.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|eval\_formula|string|Generated expression that will be used for evaluating filter conditions. The expression contains IDs that reference specific filter conditions by its `formulaid`. The value of `eval_formula` is equal to the value of `formula` for filters with a custom expression.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*|
|formula|string|User-defined expression to be used for evaluating conditions of filters with a custom expression. The expression must contain IDs that reference specific filter conditions by its `formulaid`. The IDs used in the expression must exactly match the ones defined in the filter conditions: no condition can remain unused or omitted.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `evaltype` is set to "custom expression"|

[comment]: # ({/59d7af11-d0662dba})

[comment]: # ({806339c2-72ee5420})
#### Correlation filter condition

The correlation filter condition object defines a specific condition
that must be checked before running the correlation operations.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|type|integer|Type of condition.<br><br>Possible values:<br>0 - old event tag;<br>1 - new event tag;<br>2 - new event host group;<br>3 - event tag pair;<br>4 - old event tag value;<br>5 - new event tag value.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|tag|string|Event tag (old or new).<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `type` is set to "old event tag", "new event tag", "old event tag value", or "new event tag value"|
|groupid|string|Host group ID.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `type` is set to "new event host group"|
|oldtag|string|Old event tag.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `type` is set to "event tag pair"|
|newtag|string|Old event tag.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `type` is set to "event tag pair"|
|value|string|Event tag (old or new) value.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `type` is set to "old event tag value" or "new event tag value"|
|formulaid|string|Arbitrary unique ID that is used to reference the condition from a custom expression. Can only contain capital-case letters. The ID must be defined by the user when modifying filter conditions, but will be generated anew when requesting them afterward.|
|operator|integer|Condition operator.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `type` is set to "new event host group", "old event tag value", or "new event tag value"|

::: notetip
To better understand how to use filters with various
types of expressions, see examples on the
[correlation.get](get#retrieve_correlations) and
[correlation.create](create#using_a_custom_expression_filter) method
pages.
:::

The following operators and values are supported for each condition
type.

|Condition|Condition name|Supported operators|Expected value|
|---------|--------------|-------------------|--------------|
|2|Host group|=, <>|Host group ID.|
|4|Old event tag value|=, <>, like, not like|string|
|5|New event tag value|=, <>, like, not like|string|

[comment]: # ({/806339c2-72ee5420})
