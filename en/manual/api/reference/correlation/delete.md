[comment]: # ({89029829-89029829})
# correlation.delete

[comment]: # ({/89029829-89029829})

[comment]: # ({89774c86-4922e104})
### Description

`object correlation.delete(array correlationids)`

This method allows to delete correlations.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/89774c86-4922e104})

[comment]: # ({b445698c-b445698c})
### Parameters

`(array)` IDs of the correlations to delete.

[comment]: # ({/b445698c-b445698c})

[comment]: # ({855af217-855af217})
### Return values

`(object)` Returns an object containing the IDs of the deleted
correlations under the `correlationids` property.

[comment]: # ({/855af217-855af217})

[comment]: # ({c9f65268-c9f65268})
### Example

[comment]: # ({/c9f65268-c9f65268})

[comment]: # ({a7a7df8b-bfc49991})
#### Delete multiple correlations

Delete two correlations.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "correlation.delete",
    "params": [
        "1",
        "2"
    ],
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "correlationids": [
            "1",
            "2"
        ]
    },
    "id": 1
}
```

[comment]: # ({/a7a7df8b-bfc49991})

[comment]: # ({b0713c0f-b0713c0f})
### Source

CCorrelation::delete() in
*ui/include/classes/api/services/CCorrelation.php*.

[comment]: # ({/b0713c0f-b0713c0f})
