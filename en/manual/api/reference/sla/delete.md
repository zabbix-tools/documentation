[comment]: # ({bd64f320-bf12fc32})
# sla.delete

[comment]: # ({/bd64f320-bf12fc32})

[comment]: # ({a0011f22-8ea5261f})
### Description

`object sla.delete(array slaids)`

This method allows to delete SLA entries.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/a0011f22-8ea5261f})

[comment]: # ({a9a79673-7af05f92})
### Parameters

`(array)` IDs of the SLAs to delete.

[comment]: # ({/a9a79673-7af05f92})

[comment]: # ({9b9adfaa-23960c29})
### Return values

`(object)` Returns an object containing the IDs of the deleted SLAs
under the `slaids` property.

[comment]: # ({/9b9adfaa-23960c29})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({4eeafec1-5036ec70})
#### Deleting multiple SLAs

Delete two SLA entries.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "sla.delete",
    "params": [
        "4",
        "5"
    ],
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "slaids": [
            "4",
            "5"
        ]
    },
    "id": 1
}
```

[comment]: # ({/4eeafec1-5036ec70})

[comment]: # ({20f60157-c99e5046})
### Source

CSla::delete() in *ui/include/classes/api/services/CSla.php*.

[comment]: # ({/20f60157-c99e5046})
