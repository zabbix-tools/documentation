[comment]: # ({c7a5132c-2587902b})
# sla.get

[comment]: # ({/c7a5132c-2587902b})

[comment]: # ({19361ddd-bfd5de7f})
### Description

`integer/array sla.get(object parameters)`

The method allows to retrieve SLA objects according to the given parameters.

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/19361ddd-bfd5de7f})

[comment]: # ({8511f614-e7637a1d})
### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|slaids|string/array|Return only SLAs with the given IDs.|
|serviceids|string/array|Return only SLAs matching the specific services.|
|selectSchedule|query|Return a `schedule` property with SLA schedules.<br><br>Supports `count`.|
|selectExcludedDowntimes|query|Return an `excluded_downtimes` property with SLA excluded downtimes.<br><br>Supports `count`.|
|selectServiceTags|query|Return a `service_tags` property with SLA service tags.<br><br>Supports `count`.|
|sortfield|string/array|Sort the result by the given properties.<br><br>Possible values: `slaid`, `name`, `period`, `slo`, `effective_date`, `timezone`, `status`, `description`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters).|
|editable|boolean|^|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

[comment]: # ({/8511f614-e7637a1d})

[comment]: # ({07ca11ca-7223bab1})
### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has been used.

[comment]: # ({/07ca11ca-7223bab1})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({8b2fac50-cd4e4011})
#### Retrieving all SLAs

Retrieve all data about all SLAs and their properties.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "sla.get",
    "params": {
        "output": "extend",
        "selectSchedule": ["period_from", "period_to"],
        "selectExcludedDowntimes": ["name", "period_from", "period_to"],
        "selectServiceTags": ["tag", "operator", "value"],
        "preservekeys": true
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "1": {
            "slaid": "1",
            "name": "Database Uptime",
            "period": "1",
            "slo": "99.9995",
            "effective_date": "1672444800",
            "timezone": "America/Toronto",
            "status": "1",
            "description": "Provide excellent uptime for main SQL database engines.",
            "service_tags": [
                {
                    "tag": "Database",
                    "operator": "0",
                    "value": "MySQL"
                },
                {
                    "tag": "Database",
                    "operator": "0",
                    "value": "PostgreSQL"
                }
            ],
            "schedule": [
                {
                    "period_from": "0",
                    "period_to": "601200"
                }
            ],
            "excluded_downtimes": [
                {
                    "name": "Software version upgrade rollout",
                    "period_from": "1648760400",
                    "period_to": "1648764900"
                }
            ]
        }
    },
    "id": 1
}
```

[comment]: # ({/8b2fac50-cd4e4011})

[comment]: # ({1f2163e3-05e3cf18})
### Source

CSla:get() in *ui/include/classes/api/services/CSla.php*.

[comment]: # ({/1f2163e3-05e3cf18})
