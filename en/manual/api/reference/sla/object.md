[comment]: # ({bc490008-4e570b8c})
# > SLA object

The following objects are directly related to the `sla` (Service Level Agreement) API.

[comment]: # ({/bc490008-4e570b8c})

[comment]: # ({d71814e0-8451803c})
### SLA

The SLA object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|slaid|string|ID of the SLA.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*<br>- *required* for update operations|
|name|string|Name of the SLA.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* for create operations|
|period|integer|Reporting period of the SLA.<br><br>Possible values:<br>0 - daily;<br>1 - weekly;<br>2 - monthly;<br>3 - quarterly;<br>4 - annually.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* for create operations|
|slo|float|Minimum acceptable Service Level Objective expressed as a percent. If the Service Level Indicator (SLI) drops lower, the SLA is considered to be in problem/unfulfilled state.<br><br>Possible values: 0-100 (up to 4 fractional digits).<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* for create operations|
|effective_date|integer|Effective date of the SLA.<br><br>Possible values: date timestamp in UTC.|
|timezone|string|Reporting time zone, for example: `Europe/London`, `UTC`.<br><br>For the full list of supported time zones please refer to [PHP documentation](https://www.php.net/manual/en/timezones.php).<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* for create operations|
|status|integer|Status of the SLA.<br><br>Possible values:<br>0 - *(default)* disabled SLA;<br>1 - enabled SLA.|
|description|string|Description of the SLA.|

[comment]: # ({/d71814e0-8451803c})

[comment]: # ({e5b9974b-632f7fa7})
### SLA Schedule

The SLA schedule object defines periods where the connected service(s) are scheduled to be in working order.
It has the following properties.

[comment]: # ({/e5b9974b-632f7fa7})

[comment]: # ({5124412f-db954c0c})
|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|period\_from|integer|Starting time of the recurrent weekly period of time (inclusive).<br><br>Possible values: number of seconds (counting from Sunday).<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|period\_to|integer|Ending time of the recurrent weekly period of time (exclusive).<br><br>Possible values: number of seconds (counting from Sunday).<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|

[comment]: # ({/5124412f-db954c0c})

[comment]: # ({79292401-5bbe7eb6})
### SLA excluded downtime

The excluded downtime object defines periods where the connected service(s) are scheduled to be out of working order,
without affecting SLI, e.g., undergoing planned maintenance.
It has the following properties.

[comment]: # ({/79292401-5bbe7eb6})

[comment]: # ({c6429487-fa71231c})

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|name|string|Name of the excluded downtime.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|period\_from|integer|Starting time of the excluded downtime (inclusive).<br><br>Possible values: timestamp.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|period\_to|integer|Ending time of the excluded downtime (exclusive).<br><br>Possible values: timestamp.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|

[comment]: # ({/c6429487-fa71231c})

[comment]: # ({23d825be-9f9ccbfd})
### SLA service tag

The SLA service tag object links services to include in the calculations for the SLA. It has the following properties.

[comment]: # ({/23d825be-9f9ccbfd})

[comment]: # ({4a53a912-e4881380})
|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|tag|string|SLA service tag name.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|operator|integer|SLA service tag operator.<br><br>Possible values:<br>0 - _(default)_ equals;<br>2 - like|
|value|string|SLA service tag value.|

[comment]: # ({/4a53a912-e4881380})
