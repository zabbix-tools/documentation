[comment]: # ({d1387fc4-f9178426})
# sla.getsli

[comment]: # ({/d1387fc4-f9178426})

[comment]: # ({85d3848d-531f7cc6})
### Description

`object sla.getsli(object parameters)`

This method allows to calculate the Service Level Indicator (SLI) data.

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/85d3848d-531f7cc6})

[comment]: # ({0acd1682-bcbf1cf2})
### Parameters

`(object)` Parameters containing the SLA ID, reporting periods and, optionally,
the IDs of the services - to calculate the SLI for.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|slaid|string|IDs of services to return availability information for.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required*|
|period\_from|timestamp|Starting date (inclusive) to report the SLI for.<br><br>Possible values: timestamp.|
|period\_to|timestamp|Ending date (exclusive) to report the SLI for.<br><br>Possible values: timestamp.|
|periods|array|Preferred number of periods to report.<br><br>Possible values: 1-100|
|serviceids|string/array|IDs of services to return the SLI for.|

[comment]: # ({/0acd1682-bcbf1cf2})

[comment]: # ({662b31f2-31bc72e4})
#### Partitioning of periods

The following demonstrates the arrangement of returned _period slices_ based on combinations of parameters.

|Parameters|   |   |Description|
|--|--|--|----|
|**period\_from**|**period\_to**|**periods**|   |
|-|-|-|The last 20 periods (including the current one) but not past the first available period based on the effective date of the SLA.|
|-|-|specified|The last periods specified by the **periods** parameter.|
|-|specified|-|The last 20 periods before the **specified date**, but not past the first available period based on the effective date of the SLA.|
|-|specified|specified|The last periods specified by the **periods** parameter before the **specified date**.|
|specified|-|-|The first 20 periods (including the current one) but not past the current one.|
|specified|-|specified|The first periods specified by the **periods** parameter starting with the **specified date**.|
|specified|specified|-|Periods within the specified date range, but no more than 100 and not past the first available period based on the effective date of the SLA.|
|specified|specified|specified|Periods within the specified date range, but no more than the specified number of periods and not past the first available period based on the effective date of the SLA.|

[comment]: # ({/662b31f2-31bc72e4})

[comment]: # ({af9e88c4-b46f1257})
### Return values

`(object)` Returns the results of the calculation.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|periods|array|List of the reported periods.<br><br>Each reported period is represented as an object consisting of:<br>- `period_from` - Starting date of the reported period (timestamp).<br>- `period_to` - Ending date of the reported period (timestamp).<br><br>Periods are sorted by `period_from` field ascending.|
|serviceids|array|List of service IDs in the reported periods.<br><br>The sorting order of the list is not defined. Even if `serviceids` parameter was passed to the `sla.getsli` method.|
|sli|array|SLI data (as a **two-dimensional array**) for each reported period and service.<br><br>The index of the `periods` property is used as the **first** dimension of the `sli` property.<br><br>The index of the `serviceids` property is used as the **second** dimension of the `sli` property.|

[comment]: # ({/af9e88c4-b46f1257})

[comment]: # ({aa24c098-ba0b5441})
#### SLI data

The SLI data returned for each reported period and service consists of:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|uptime|integer|Amount of time service spent in an _OK_ state during scheduled uptime, less the excluded downtimes.|
|downtime|integer|Amount of time service spent in a _not OK_ state during scheduled uptime, less the excluded downtimes.|
|sli|float|SLI (per cent of total uptime), based on uptime and downtime.|
|error\_budget|integer|Error budget (in seconds), based on the SLI and the SLO.|
|excluded\_downtimes|array|Array of excluded downtimes in this reporting period.<br><br>Each object will contain the following parameters:<br>- `name` - Name of the excluded downtime.<br>- `period_from` - Starting date and time (inclusive) of the excluded downtime.<br>- `period_to` - Ending date and time (exclusive) of the excluded downtime.<br><br>Excluded downtimes are sorted by period_from field ascending.|

[comment]: # ({/aa24c098-ba0b5441})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({3a0c729c-c6c818e1})
#### Calculating SLI

Retrieve SLI on services with IDs "50, 60 and 70" linked to an SLA with ID of "5" for 3 periods starting from Nov 01, 2021.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "sla.getsli",
    "params": {
        "slaid": "5",
        "serviceids": [
            50,
            60,
            70
        ],
        "periods": 3,
        "period_from": "1635724800"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "periods": [
            {
                "period_from": 1635724800,
                "period_to": 1638316800
            },
            {
                "period_from": 1638316800,
                "period_to": 1640995200
            },
            {
                "period_from": 1640995200,
                "period_to": 1643673600
            }
        ],
        "serviceids": [
            50,
            60,
            70
        ],
        "sli": [
            [
                {
                    "uptime": 1186212,
                    "downtime": 0,
                    "sli": 100,
                    "error_budget": 0,
                    "excluded_downtimes": [
                        {
                            "name": "Excluded Downtime - 1",
                            "period_from": 1637836212,
                            "period_to": 1638316800
                        }
                    ]
                },
                {
                    "uptime": 1186212,
                    "downtime": 0,
                    "sli": 100,
                    "error_budget": 0,
                    "excluded_downtimes": [
                        {
                            "name": "Excluded Downtime - 1",
                            "period_from": 1637836212,
                            "period_to": 1638316800
                        }
                    ]
                },
                {
                    "uptime": 1186212,
                    "downtime": 0,
                    "sli": 100,
                    "error_budget": 0,
                    "excluded_downtimes": [
                        {
                            "name": "Excluded Downtime - 1",
                            "period_from": 1637836212,
                            "period_to": 1638316800
                        }
                    ]
                }
            ],
            [
                {
                    "uptime": 1147548,
                    "downtime": 0,
                    "sli": 100,
                    "error_budget": 0,
                    "excluded_downtimes": [
                        {
                            "name": "Excluded Downtime - 1",
                            "period_from": 1638439200,
                            "period_to": 1639109652
                        }
                    ]
                },
                {
                    "uptime": 1147548,
                    "downtime": 0,
                    "sli": 100,
                    "error_budget": 0,
                    "excluded_downtimes": [
                        {
                            "name": "Excluded Downtime - 1",
                            "period_from": 1638439200,
                            "period_to": 1639109652
                        }
                    ]
                },
                {
                    "uptime": 1147548,
                    "downtime": 0,
                    "sli": 100,
                    "error_budget": 0,
                    "excluded_downtimes": [
                        {
                            "name": "Excluded Downtime - 1",
                            "period_from": 1638439200,
                            "period_to": 1639109652
                        }
                    ]
                }
            ],
            [
                {
                    "uptime": 1674000,
                    "downtime": 0,
                    "sli": 100,
                    "error_budget": 0,
                    "excluded_downtimes": []
                },
                {
                    "uptime": 1674000,
                    "downtime": 0,
                    "sli": 100,
                    "error_budget": 0,
                    "excluded_downtimes": []
                },
                {
                    "uptime": 1674000,
                    "downtime": 0,
                    "sli": 100,
                    "error_budget": 0,
                    "excluded_downtimes": []
                }
            ]
        ]
    },
    "id": 1
}
```

[comment]: # ({/3a0c729c-c6c818e1})

[comment]: # ({13a91d62-a5414084})
### Source

CSla::getSli() in *ui/include/classes/api/services/CSla.php*

[comment]: # ({/13a91d62-a5414084})
