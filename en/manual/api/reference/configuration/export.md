[comment]: # ({690a015b-690a015b})
# configuration.export

[comment]: # ({/690a015b-690a015b})

[comment]: # ({104e574c-98d36809})
### Description

`string configuration.export(object parameters)`

This method allows to export configuration data as a serialized string.

::: noteclassic
This method is available to users of any type.
Permissions to call the method can be revoked in user role settings.
See [User roles](/manual/web_interface/frontend_sections/users/user_roles) for more information.
:::

[comment]: # ({/104e574c-98d36809})

[comment]: # ({43d0e9a4-ef6ad2c8})
### Parameters

`(object)` Parameters defining the objects to be exported and the format to use.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|format|string|Format in which the data must be exported.<br><br>Possible values:<br>`yaml` - YAML;<br>`xml` - XML;<br>`json` - JSON;<br>`raw` - unprocessed PHP array.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required*|
|prettyprint|boolean|Make the output more human readable by adding indentation.<br><br>Possible values:<br>`true` - add indentation;<br>`false` - *(default)* do not add indentation.|
|options|object|Objects to be exported.<br><br>The `options` object has the following parameters:<br>`host_groups` - `(array)` IDs of host groups to export;<br>`hosts` - `(array)` IDs of hosts to export;<br>`images` - `(array)` IDs of images to export;<br>`maps` - `(array)` IDs of maps to export;<br>`mediaTypes` - `(array)` IDs of media types to export;<br>`template_groups` - `(array)` IDs of template groups to export;<br>`templates` - `(array)` IDs of templates to export.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required*|

[comment]: # ({/43d0e9a4-ef6ad2c8})

[comment]: # ({985d3621-0bfd9762})
### Return values

`(string)` Returns a serialized string containing the requested configuration data.

[comment]: # ({/985d3621-0bfd9762})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({a72c4506-5c446795})
#### Exporting a template

Export the configuration of template "10571" as an XML string.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "configuration.export",
    "params": {
        "options": {
            "templates": [
                "10571"
            ]
        },
        "format": "xml"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<zabbix_export><version>6.4</version><template_groups><template_group><uuid>7df96b18c230490a9a0a9e2307226338</uuid><name>Templates</name></template_group></template_groups><templates><template><uuid>5aef0444a82a4d8cb7a95dc4c0c85330</uuid><template>New template</template><name>New template</name><groups><group><name>Templates</name></group></groups><items><item><uuid>7f1e6f1e48aa4a128e5b6a958a5d11c3</uuid><name>Zabbix agent ping</name><key>agent.ping</key></item><item><uuid>77ba228662be4570830aa3c503fcdc03</uuid><name>Apache server uptime</name><type>DEPENDENT</type><key>apache.server.uptime</key><delay>0</delay><trends>0</trends><value_type>TEXT</value_type><preprocessing><step><type>REGEX</type><parameters><parameter>&lt;dt&gt;Server uptime: (.*)&lt;\/dt&gt;</parameter><parameter>\\1</parameter></parameters></step></preprocessing><master_item><key>web.page.get[127.0.0.1/server-status]</key></master_item></item><item><uuid>6805d4c39a624a8bab2cc8ab63df1ab3</uuid><name>CPU load</name><key>system.cpu.load</key><value_type>FLOAT</value_type><triggers><trigger><uuid>ab4c2526c2bc42e48a633082255ebcb3</uuid><expression>avg(/New template/system.cpu.load,3m)&gt;2</expression><name>CPU load too high on 'New host' for 3 minutes</name><priority>WARNING</priority></trigger></triggers></item><item><uuid>590efe5731254f089265c76ff9320726</uuid><name>Apache server status</name><key>web.page.get[127.0.0.1/server-status]</key><trends>0</trends><value_type>TEXT</value_type></item></items><valuemaps><valuemap><uuid>8fd5814c45d44a00a15ac6eaae1f3946</uuid><name>Zabbix agent ping</name><mappings><mapping><value>1</value><newvalue>Available</newvalue></mapping><mapping><value>0</value><newvalue>Not available</newvalue></mapping></mappings></valuemap></valuemaps></template></templates></zabbix_export>\n",
    "id": 1
}
```

[comment]: # ({/a72c4506-5c446795})

[comment]: # ({77321abb-a3a5fdbf})
### Source

CConfiguration::export() in *ui/include/classes/api/services/CConfiguration.php*.

[comment]: # ({/77321abb-a3a5fdbf})
