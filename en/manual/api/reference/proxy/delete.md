[comment]: # ({226837df-226837df})
# proxy.delete

[comment]: # ({/226837df-226837df})

[comment]: # ({85f8cc0f-a16e4b7f})
### Description

`object proxy.delete(array proxies)`

This method allows to delete proxies.

::: noteclassic
This method is only available to *Super admin* user type. Permissions to call the method can be revoked in user role
settings. See [User roles](/manual/web_interface/frontend_sections/users/user_roles) for more information.
:::

[comment]: # ({/85f8cc0f-a16e4b7f})

[comment]: # ({0c678a75-0c678a75})
### Parameters

`(array)` IDs of proxies to delete.

[comment]: # ({/0c678a75-0c678a75})

[comment]: # ({6700a9c1-426b06a5})
### Return values

`(object)` Returns an object containing the IDs of the deleted proxies under the `proxyids` property.

[comment]: # ({/6700a9c1-426b06a5})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({904632f5-dcfc3683})
#### Delete multiple proxies

Delete two proxies.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "proxy.delete",
    "params": [
        "10286",
        "10285"
    ],
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "proxyids": [
            "10286",
            "10285"
        ]
    },
    "id": 1
}
```

[comment]: # ({/904632f5-dcfc3683})

[comment]: # ({4dd75afc-4dd75afc})
### Source

CProxy::delete() in *ui/include/classes/api/services/CProxy.php*.

[comment]: # ({/4dd75afc-4dd75afc})
