[comment]: # ({92a86fab-92a86fab})
# proxy.update

[comment]: # ({/92a86fab-92a86fab})

[comment]: # ({dc390ed1-381a99b9})
### Description

`object proxy.update(object/array proxies)`

This method allows to update existing proxies.

::: noteclassic
This method is only available to *Super admin* user type. Permissions to call the method can be revoked in user role
settings. See [User roles](/manual/web_interface/frontend_sections/users/user_roles) for more information.
:::

[comment]: # ({/dc390ed1-381a99b9})

[comment]: # ({7ff994f7-4d6a0721})
### Parameters

`(object/array)` Proxy properties to be updated.

The `proxyid` property must be defined for each proxy, all other properties are optional. Only the passed properties
will be updated, all others will remain unchanged.

Additionally to the [standard proxy properties](object#proxy), the method accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|hosts|array|[Hosts](/manual/api/reference/host/object) to be monitored by the proxy. If a host is already monitored by a different proxy, it will be reassigned to the current proxy.<br><br>The hosts must have the `hostid` property defined.|
|interface|object|Host [interface](/manual/api/reference/hostinterface/object) to replace the existing interface for the passive proxy.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *supported* if `status` of [Proxy object](object#proxy) is set to "passive proxy"|

[comment]: # ({/7ff994f7-4d6a0721})

[comment]: # ({3cd30381-bab02241})
### Return values

`(object)` Returns an object containing the IDs of the updated proxies under the `proxyids` property.

[comment]: # ({/3cd30381-bab02241})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({f3b77dc6-3613ce9a})
#### Change hosts monitored by a proxy

Update the proxy to monitor the two given hosts.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "proxy.update",
    "params": {
        "proxyid": "10293",
        "hosts": [
            {
                "hostid": "10294"
            },
            {
                "hostid": "10295"
            },
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "proxyids": [
            "10293"
        ]
    },
    "id": 1
}
```

[comment]: # ({/f3b77dc6-3613ce9a})

[comment]: # ({a2f2c130-f06b99dc})
#### Change proxy status

Change the proxy to an active proxy and rename it to "Active proxy".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "proxy.update",
    "params": {
        "proxyid": "10293",
        "host": "Active proxy",
        "status": "5"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "proxyids": [
            "10293"
        ]
    },
    "id": 1
}
```

[comment]: # ({/a2f2c130-f06b99dc})

[comment]: # ({273e0fd8-273e0fd8})
### See also

-   [Host](/manual/api/reference/host/object#host)
-   [Proxy interface](object#proxy_interface)

[comment]: # ({/273e0fd8-273e0fd8})

[comment]: # ({8237a78a-8237a78a})
### Source

CProxy::update() in *ui/include/classes/api/services/CProxy.php*.

[comment]: # ({/8237a78a-8237a78a})
