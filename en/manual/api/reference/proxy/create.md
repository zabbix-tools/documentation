[comment]: # ({07099686-07099686})
# proxy.create

[comment]: # ({/07099686-07099686})

[comment]: # ({31fbd044-e0abed38})
### Description

`object proxy.create(object/array proxies)`

This method allows to create new proxies.

::: noteclassic
This method is only available to *Super admin* user type. Permissions to call the method can be revoked in user role
settings. See [User roles](/manual/web_interface/frontend_sections/users/user_roles) for more information.
:::

[comment]: # ({/31fbd044-e0abed38})

[comment]: # ({4eda6358-fc26a415})
### Parameters

`(object/array)` Proxies to create.

Additionally to the [standard proxy properties](object#proxy), the method accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|hosts|array|[Hosts](/manual/api/reference/host/object) to be monitored by the proxy. If a host is already monitored by a different proxy, it will be reassigned to the current proxy.<br><br>The hosts must have the `hostid` property defined.|
|interface|object|Host [interface](/manual/api/reference/hostinterface/object) to be created for the passive proxy.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required* if `status` of [Proxy object](object#proxy) is set to "passive proxy"|

[comment]: # ({/4eda6358-fc26a415})

[comment]: # ({41544960-606a1465})
### Return values

`(object)` Returns an object containing the IDs of the created proxies under the `proxyids` property. The order of the
returned IDs matches the order of the passed proxies.

[comment]: # ({/41544960-606a1465})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({15b126b0-86fb9238})
#### Create an active proxy

Create an action proxy "Active proxy" and assign a host to be monitored by it.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "proxy.create",
    "params": {
        "host": "Active proxy",
        "status": "5",
        "hosts": [
            {
                "hostid": "10279"
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "proxyids": [
            "10280"
        ]
    },
    "id": 1
}
```

[comment]: # ({/15b126b0-86fb9238})

[comment]: # ({bce1dcd4-8e1098f9})
#### Create a passive proxy

Create a passive proxy "Passive proxy" and assign two hosts to be monitored by it.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "proxy.create",
    "params": {
        "host": "Passive proxy",
        "status": "6",
        "interface": {
            "ip": "127.0.0.1",
            "dns": "",
            "useip": "1",
            "port": "10051"
        },
        "hosts": [
            {
                "hostid": "10192"
            },
            {
                "hostid": "10139"
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "proxyids": [
            "10284"
        ]
    },
    "id": 1
}
```

[comment]: # ({/bce1dcd4-8e1098f9})

[comment]: # ({273e0fd8-273e0fd8})
### See also

-   [Host](/manual/api/reference/host/object#host)
-   [Proxy interface](object#proxy_interface)

[comment]: # ({/273e0fd8-273e0fd8})

[comment]: # ({453e4545-453e4545})
### Source

CProxy::create() in *ui/include/classes/api/services/CProxy.php*.

[comment]: # ({/453e4545-453e4545})
