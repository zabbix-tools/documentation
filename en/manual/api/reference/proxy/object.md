[comment]: # ({7b0425dd-7b0425dd})
# > Proxy object

The following objects are directly related to the `proxy` API.

[comment]: # ({/7b0425dd-7b0425dd})

[comment]: # ({4582765c-52ccb0be})
### Proxy

The proxy object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|proxyid|string|ID of the proxy.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*<br>- *required* for update operations|
|host|string|Name of the proxy.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* for create operations|
|status|integer|Type of proxy.<br><br>Possible values:<br>5 - active proxy;<br>6 - passive proxy.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* for create operations|
|description|text|Description of the proxy.|
|lastaccess|timestamp|Time when the proxy last connected to the server.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*|
|tls\_connect|integer|Connections to host.<br><br>Possible values:<br>1 - *(default)* No encryption;<br>2 - PSK;<br>4 - certificate.|
|tls\_accept|integer|Connections from host.<br>This is a bitmask field, any combination of possible bitmap values is acceptable.<br><br>Possible bitmap values:<br>1 - *(default)* No encryption;<br>2 - PSK;<br>4 - certificate.|
|tls\_issuer|string|Certificate issuer.|
|tls\_subject|string|Certificate subject.|
|tls\_psk\_identity|string|PSK identity.<br>Do not put sensitive information in the PSK identity, it is transmitted unencrypted over the network to inform a receiver which PSK to use.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *write-only*<br>- *required* if `tls_connect` is set to "PSK", or `tls_accept` contains the "PSK" bit|
|tls\_psk|string|The preshared key, at least 32 hex digits.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *write-only*<br>- *required* if `tls_connect` is set to "PSK", or `tls_accept` contains the "PSK" bit|
|proxy\_address|string|Comma-delimited IP addresses or DNS names of active Zabbix proxy.|
|auto\_compress|integer|Indicates if communication between Zabbix server and proxy is compressed.<br><br>Possible values:<br>0 - No compression;<br>1 - Compression enabled.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*|
|version|integer|Version of proxy.<br><br>Three-part Zabbix version number, where two decimal digits are used for each part, e.g., 50401 for version 5.4.1, 60200 for version 6.2.0, etc.<br>0 - Unknown proxy version.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*|
|compatibility|integer|Version of proxy compared to Zabbix server version.<br><br>Possible values:<br>0 - Undefined;<br>1 - Current version (proxy and server have the same major version);<br>2 - Outdated version (proxy version is older than server version, but is partially supported);<br>3 - Unsupported version (proxy version is older than server previous LTS release version or server major version is older than proxy major version).<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*|

[comment]: # ({/4582765c-52ccb0be})

[comment]: # ({7ae4bf12-4e21547f})
### Proxy interface

The proxy interface object defines the interface used to connect to a passive proxy. It has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|dns|string|DNS name to connect to.<br><br>Can be empty if connections are made via IP address.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `useip` is set to "connect using DNS name"|
|ip|string|IP address to connect to.<br><br>Can be empty if connections are made via DNS names.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `useip` is set to "connect using IP address"|
|port|string|Port number to connect to.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|useip|integer|Whether the connection should be made via IP address.<br><br>Possible values:<br>0 - connect using DNS name;<br>1 - connect using IP address.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|

[comment]: # ({/7ae4bf12-4e21547f})
