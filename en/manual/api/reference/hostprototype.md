[comment]: # ({d4709448-517ea62c})
# Host prototype

This class is designed to work with host prototypes.

Object references:

-   [Host prototype](/manual/api/reference/hostprototype/object#host_prototype)
-   [Group link](/manual/api/reference/hostprototype/object#group_link)
-   [Group prototype](/manual/api/reference/hostprototype/object#group_prototype)
-   [Host prototype tag](/manual/api/reference/hostprototype/object#host_prototype_tag)
-   [Custom interface](/manual/api/reference/hostprototype/object#custom_interface)
-   [Custom interface details](/manual/api/reference/hostprototype/object#custom_interface_details)

Available methods:

-   [hostprototype.create](/manual/api/reference/hostprototype/create) - create new host prototypes
-   [hostprototype.delete](/manual/api/reference/hostprototype/delete) - delete host prototypes
-   [hostprototype.get](/manual/api/reference/hostprototype/get) - retrieve host prototypes
-   [hostprototype.update](/manual/api/reference/hostprototype/update) - update host prototypes

[comment]: # ({/d4709448-517ea62c})
