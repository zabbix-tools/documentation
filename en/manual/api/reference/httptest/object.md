[comment]: # ({a17bb95b-a17bb95b})
# > Web scenario object

The following objects are directly related to the `webcheck` API.

[comment]: # ({/a17bb95b-a17bb95b})

[comment]: # ({daeaf60c-e60908bb})
### Web scenario

The web scenario object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|httptestid|string|ID of the web scenario.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*<br>- *required* for update operations|
|hostid|string|ID of the host that the web scenario belongs to.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *constant*<br>- *required* for create operations|
|name|string|Name of the web scenario.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* for create operations|
|agent|string|User agent string that will be used by the web scenario.<br><br>Default: Zabbix|
|authentication|integer|Authentication method that will be used by the web scenario.<br><br>Possible values:<br>0 - *(default)* none;<br>1 - basic HTTP authentication;<br>2 - NTLM authentication.|
|delay|string|Execution interval of the web scenario.<br>Accepts seconds, time unit with suffix, or a user macro.<br><br>Default: 1m.|
|headers|array|[HTTP headers](/manual/api/reference/httptest/object#http-field) that will be sent when performing a request.|
|http\_password|string|Password used for basic HTTP or NTLM authentication.|
|http\_proxy|string|Proxy that will be used by the web scenario given as *http://\[username\[:password\]@\]proxy.example.com\[:port\]*.|
|http\_user|string|User name used for basic HTTP or NTLM authentication.|
|retries|integer|Number of times a web scenario will try to execute each step before failing.<br><br>Default: 1.|
|ssl\_cert\_file|string|Name of the SSL certificate file used for client authentication (must be in PEM format).|
|ssl\_key\_file|string|Name of the SSL private key file used for client authentication (must be in PEM format).|
|ssl\_key\_password|string|SSL private key password.|
|status|integer|Whether the web scenario is enabled.<br><br>Possible values:<br>0 - *(default)* enabled;<br>1 - disabled.|
|templateid|string|ID of the parent template web scenario.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*|
|variables|array|Web scenario [variables](/manual/api/reference/httptest/object#http-field).|
|verify\_host|integer|Whether to verify that the host name specified in the SSL certificate matches the one used in the scenario.<br><br>Possible values:<br>0 - *(default)* skip host verification;<br>1 - verify host.|
|verify\_peer|integer|Whether to verify the SSL certificate of the web server.<br><br>Possible values:<br>0 - *(default)* skip peer verification;<br>1 - verify peer.|
|uuid|string|Global unique identifier, used for linking imported web scenarios to already existing ones. Auto-generated, if not given.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if the web scenario belongs to a template|

[comment]: # ({/daeaf60c-e60908bb})

[comment]: # ({0a47168c-c7ab7fb0})
### Web scenario tag

The web scenario tag object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|tag|string|Web scenario tag name.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|value|string|Web scenario tag value.|

[comment]: # ({/0a47168c-c7ab7fb0})

[comment]: # ({550e6dd8-eb20f47d})
### Scenario step

The scenario step object defines a specific web scenario check. It has
the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|name|string|Name of the scenario step.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|no|integer|Sequence number of the step in a web scenario.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|url|string|URL to be checked.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|follow\_redirects|integer|Whether to follow HTTP redirects.<br><br>Possible values:<br>0 - don't follow redirects;<br>1 - *(default)* follow redirects.|
|headers|array|[HTTP headers](/manual/api/reference/httptest/object#http-field) that will be sent when performing a request. Scenario step headers will overwrite headers specified for the web scenario.|
|posts|string/array|HTTP POST variables as a string (raw post data) or as an array of [HTTP fields](/manual/api/reference/httptest/object#http-field) (form field data).|
|required|string|Text that must be present in the response.|
|retrieve\_mode|integer|Part of the HTTP response that the scenario step must retrieve.<br><br>Possible values:<br>0 - *(default)* only body;<br>1 - only headers;<br>2 - headers and body.|
|status\_codes|string|Ranges of required HTTP status codes, separated by commas.|
|timeout|string|Request timeout in seconds. Accepts seconds, time unit with suffix, or a user macro.<br><br>Default: 15s. Maximum: 1h. Minimum: 1s.|
|variables|array|Scenario step [variables](/manual/api/reference/httptest/object#http-field).|
|query\_fields|array|Query fields - array of [HTTP fields](/manual/api/reference/httptest/object#http-field) that will be added to URL when performing a request.|

[comment]: # ({/550e6dd8-eb20f47d})

[comment]: # ({10a55bc3-d69e8b7b})
### HTTP field

The HTTP field object defines the name and value that is used to specify the web scenario variables, HTTP headers, and POST fields or query fields.
It has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|name|string|Name of header/variable/POST or GET field.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|value|string|Value of header/variable/POST or GET field.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|

[comment]: # ({/10a55bc3-d69e8b7b})
