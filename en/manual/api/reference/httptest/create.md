[comment]: # ({3c277f74-3c277f74})
# httptest.create

[comment]: # ({/3c277f74-3c277f74})

[comment]: # ({9408d3d8-c4bddb28})
### Description

`object httptest.create(object/array webScenarios)`

This method allows to create new web scenarios.

::: noteclassic
Creating a web scenario will automatically create a set of
[web monitoring items](/manual/web_monitoring/items).
:::

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/9408d3d8-c4bddb28})

[comment]: # ({e162d298-5bd5a38b})
### Parameters

`(object/array)` Web scenarios to create.

Additionally to the [standard web scenario
properties](object#web_scenario), the method accepts the following
parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|steps|array|Web scenario [steps](/manual/api/reference/httptest/object#scenario_step).<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required*|
|tags|array|Web scenario [tags](/manual/api/reference/httptest/object#web_scenario_tag).|

[comment]: # ({/e162d298-5bd5a38b})

[comment]: # ({0f4da548-0f4da548})
### Return values

`(object)` Returns an object containing the IDs of the created web
scenarios under the `httptestids` property. The order of the returned
IDs matches the order of the passed web scenarios.

[comment]: # ({/0f4da548-0f4da548})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({5125d2fe-0b10bb11})
#### Creating a web scenario

Create a web scenario to monitor the company home page. The scenario
will have two steps, to check the home page and the "About" page and
make sure they return the HTTP status code 200.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "httptest.create",
    "params": {
        "name": "Homepage check",
        "hostid": "10085",
        "steps": [
            {
                "name": "Homepage",
                "url": "http://example.com",
                "status_codes": "200",
                "no": 1
            },
            {
                "name": "Homepage / About",
                "url": "http://example.com/about",
                "status_codes": "200",
                "no": 2
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "httptestids": [
            "5"
        ]
    },
    "id": 1
}
```

[comment]: # ({/5125d2fe-0b10bb11})

[comment]: # ({e8ca015f-e8ca015f})
### See also

-   [Scenario step](object#scenario_step)

[comment]: # ({/e8ca015f-e8ca015f})

[comment]: # ({6b938292-6b938292})
### Source

CHttpTest::create() in *ui/include/classes/api/services/CHttpTest.php*.

[comment]: # ({/6b938292-6b938292})
