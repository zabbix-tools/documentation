[comment]: # ({629f1e7c-629f1e7c})
# httptest.delete

[comment]: # ({/629f1e7c-629f1e7c})

[comment]: # ({af6afc07-67a29a65})
### Description

`object httptest.delete(array webScenarioIds)`

This method allows to delete web scenarios.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/af6afc07-67a29a65})

[comment]: # ({16d66a94-16d66a94})
### Parameters

`(array)` IDs of the web scenarios to delete.

[comment]: # ({/16d66a94-16d66a94})

[comment]: # ({fd0832d6-fd0832d6})
### Return values

`(object)` Returns an object containing the IDs of the deleted web
scenarios under the `httptestids` property.

[comment]: # ({/fd0832d6-fd0832d6})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({1f0b85e9-4924488e})
#### Deleting multiple web scenarios

Delete two web scenarios.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "httptest.delete",
    "params": [
        "2",
        "3"
    ],
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "httptestids": [
            "2",
            "3"
        ]
    },
    "id": 1
}
```

[comment]: # ({/1f0b85e9-4924488e})

[comment]: # ({0ba69a51-0ba69a51})
### Source

CHttpTest::delete() in *ui/include/classes/api/services/CHttpTest.php*.

[comment]: # ({/0ba69a51-0ba69a51})
