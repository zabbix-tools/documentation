[comment]: # ({3d0c523c-3d0c523c})
# action.delete

[comment]: # ({/3d0c523c-3d0c523c})

[comment]: # ({fd1336f0-c056f978})
### Description

`object action.delete(array actionIds)`

This method allows to delete actions.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/fd1336f0-c056f978})

[comment]: # ({f66aff82-f66aff82})
### Parameters

`(array)` IDs of the actions to delete.

[comment]: # ({/f66aff82-f66aff82})

[comment]: # ({04c6deae-04c6deae})
### Return values

`(object)` Returns an object containing the IDs of the deleted actions
under the `actionids` property.

[comment]: # ({/04c6deae-04c6deae})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({b1f7d266-36abb590})
#### Delete multiple actions

Delete two actions.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "action.delete",
    "params": [
        "17",
        "18"
    ],
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "actionids": [
            "17",
            "18"
        ]
    },
    "id": 1
}
```

[comment]: # ({/b1f7d266-36abb590})

[comment]: # ({70602a31-70602a31})
### Source

CAction::delete() in *ui/include/classes/api/services/CAction.php*.

[comment]: # ({/70602a31-70602a31})
