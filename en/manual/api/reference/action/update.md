[comment]: # ({e4b5aac4-e4b5aac4})
# action.update

[comment]: # ({/e4b5aac4-e4b5aac4})

[comment]: # ({753e174e-b1673b49})
### Description

`object action.update(object/array actions)`

This method allows to update existing actions.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/753e174e-b1673b49})

[comment]: # ({685606f0-703e85ce})
### Parameters

`(object/array)` Action properties to be updated.

The `actionid` property must be defined for each action, all other
properties are optional. Only the passed properties will be updated, all
others will remain unchanged.

Additionally to the [standard action properties](object#action), the
method accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|filter|object|Action [filter](/manual/api/reference/action/object#action_filter) object to replace the current filter.|
|operations|array|Action [operations](/manual/api/reference/action/object#action_operation) to replace existing operations.|
|recovery\_operations|array|Action [recovery operations](/manual/api/reference/action/object#action_recovery_operation) to replace existing recovery operations.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *supported* if `eventsource` of [Action object](/manual/api/reference/action/object#action) is set to "event created by a trigger", "internal event", or "event created on service status update"|
|update\_operations|array|Action [update operations](/manual/api/reference/action/object#action_update_operation) to replace existing update operations.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *supported* if `eventsource` of [Action object](/manual/api/reference/action/object#action) is set to "event created by a trigger" or "event created on service status update"|

[comment]: # ({/685606f0-703e85ce})

[comment]: # ({8cbaa103-8cbaa103})
### Return values

`(object)` Returns an object containing the IDs of the updated actions
under the `actionids` property.

[comment]: # ({/8cbaa103-8cbaa103})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({6695ece3-77b78ec5})
#### Disable action

Disable an action, that is, set its status to "1".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "action.update",
    "params": {
        "actionid": "2",
        "status": "1"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "actionids": [
            "2"
        ]
    },
    "id": 1
}
```

[comment]: # ({/6695ece3-77b78ec5})

[comment]: # ({755496f7-755496f7})
### See also

-   [Action filter](object#action_filter)
-   [Action operation](object#action_operation)

[comment]: # ({/755496f7-755496f7})

[comment]: # ({df9f1d35-df9f1d35})
### Source

CAction::update() in *ui/include/classes/api/services/CAction.php*.

[comment]: # ({/df9f1d35-df9f1d35})
