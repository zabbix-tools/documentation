[comment]: # ({e91a170d-e91a170d})
# usermacro.deleteglobal

[comment]: # ({/e91a170d-e91a170d})

[comment]: # ({36f2370e-f33727ba})
### Description

`object usermacro.deleteglobal(array globalMacroIds)`

This method allows to delete global macros.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/36f2370e-f33727ba})

[comment]: # ({0a9d3dbc-0a9d3dbc})
### Parameters

`(array)` IDs of the global macros to delete.

[comment]: # ({/0a9d3dbc-0a9d3dbc})

[comment]: # ({30415b7d-30415b7d})
### Return values

`(object)` Returns an object containing the IDs of the deleted global
macros under the `globalmacroids` property.

[comment]: # ({/30415b7d-30415b7d})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({a07f69e4-b377e629})
#### Deleting multiple global macros

Delete two global macros.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "usermacro.deleteglobal",
    "params": [
        "32",
        "11"
    ],
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "globalmacroids": [
            "32",
            "11"
        ]
    },
    "id": 1
}
```

[comment]: # ({/a07f69e4-b377e629})

[comment]: # ({331322ff-331322ff})
### Source

CUserMacro::deleteGlobal() in
*ui/include/classes/api/services/CUserMacro.php*.

[comment]: # ({/331322ff-331322ff})
