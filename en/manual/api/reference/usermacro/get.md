[comment]: # ({02ea8022-02ea8022})
# usermacro.get

[comment]: # ({/02ea8022-02ea8022})

[comment]: # ({78b5ab06-28befc82})
### Description

`integer/array usermacro.get(object parameters)`

The method allows to retrieve host and global macros according to the
given parameters.

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/78b5ab06-28befc82})

[comment]: # ({3842e3fe-141835f9})
### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|globalmacro|flag|Return global macros instead of host macros.|
|globalmacroids|string/array|Return only global macros with the given IDs.|
|groupids|string/array|Return only host macros that belong to hosts or templates from the given host groups or template groups.|
|hostids|string/array|Return only macros that belong to the given hosts or templates.|
|hostmacroids|string/array|Return only host macros with the given IDs.|
|inherited|boolean|If set to `true` return only host prototype user macros inherited from a template.|
|selectHostGroups|query|Return host groups that the host macro belongs to in the [`hostgroups`](/manual/api/reference/hostgroup/object) property.<br><br>Used only when retrieving host macros.|
|selectHosts|query|Return hosts that the host macro belongs to in the [`hosts`](/manual/api/reference/host/object) property.<br><br>Used only when retrieving host macros.|
|selectTemplateGroups|query|Return template groups that the template macro belongs to in the [`templategroups`](/manual/api/reference/templategroup/object) property.<br><br>Used only when retrieving template macros.|
|selectTemplates|query|Return templates that the host macro belongs to in the [`templates`](/manual/api/reference/template/object) property.<br><br>Used only when retrieving host macros.|
|sortfield|string/array|Sort the result by the given properties.<br><br>Possible values: `macro`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters) page.|
|editable|boolean|^|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|
|selectGroups<br>(deprecated)|query|This parameter is deprecated, please use `selectHostGroups` or `selectTemplateGroups` instead.<br>Return host groups and template groups that the host macro belongs to in the `groups` property.<br><br>Used only when retrieving host macros.|

[comment]: # ({/3842e3fe-141835f9})

[comment]: # ({7223bab1-7223bab1})
### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.

[comment]: # ({/7223bab1-7223bab1})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({4540e2f0-7089e052})
#### Retrieving host macros for a host

Retrieve all host macros defined for host "10198".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "usermacro.get",
    "params": {
        "output": "extend",
        "hostids": "10198"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": [
        {
            "hostmacroid": "9",
            "hostid": "10198",
            "macro": "{$INTERFACE}",
            "value": "eth0",
            "description": "",
            "type": "0",
            "automatic": "0"
        },
        {
            "hostmacroid": "11",
            "hostid": "10198",
            "macro": "{$SNMP_COMMUNITY}",
            "value": "public",
            "description": "",
            "type": "0",
            "automatic": "0"
        }
    ],
    "id": 1
}
```

[comment]: # ({/4540e2f0-7089e052})

[comment]: # ({93c3e1f5-29b64a7a})
#### Retrieving global macros

Retrieve all global macros.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "usermacro.get",
    "params": {
        "output": "extend",
        "globalmacro": true
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": [
        {
            "globalmacroid": "6",
            "macro": "{$SNMP_COMMUNITY}",
            "value": "public",
            "description": "",
            "type": "0"
        }
    ],
    "id": 1
}
```

[comment]: # ({/93c3e1f5-29b64a7a})

[comment]: # ({26db0ec2-26db0ec2})
### Source

CUserMacro::get() in *ui/include/classes/api/services/CUserMacro.php*.

[comment]: # ({/26db0ec2-26db0ec2})
