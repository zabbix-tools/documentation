[comment]: # ({a16741b3-a16741b3})
# usermacro.updateglobal

[comment]: # ({/a16741b3-a16741b3})

[comment]: # ({9d774d8a-19b16b0b})
### Description

`object usermacro.updateglobal(object/array globalMacros)`

This method allows to update existing global macros.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/9d774d8a-19b16b0b})

[comment]: # ({be7c969e-be7c969e})
### Parameters

`(object/array)` [Global macro properties](object#global_macro) to be
updated.

The `globalmacroid` property must be defined for each global macro, all
other properties are optional. Only the passed properties will be
updated, all others will remain unchanged.

[comment]: # ({/be7c969e-be7c969e})

[comment]: # ({9c6d43ad-9c6d43ad})
### Return values

`(object)` Returns an object containing the IDs of the updated global
macros under the `globalmacroids` property.

[comment]: # ({/9c6d43ad-9c6d43ad})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({4df50124-bd208b05})
#### Changing the value of a global macro

Change the value of a global macro to "public".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "usermacro.updateglobal",
    "params": {
        "globalmacroid": "1",
        "value": "public"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "globalmacroids": [
            "1"
        ]
    },
    "id": 1
}
```

[comment]: # ({/4df50124-bd208b05})

[comment]: # ({85ca09bc-85ca09bc})
### Source

CUserMacro::updateGlobal() in
*ui/include/classes/api/services/CUserMacro.php*.

[comment]: # ({/85ca09bc-85ca09bc})
