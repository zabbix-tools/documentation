[comment]: # ({28092341-28092341})
# > User macro object

The following objects are directly related to the `usermacro` API.

[comment]: # ({/28092341-28092341})

[comment]: # ({a510d71d-e654cd12})
### Global macro

The global macro object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|globalmacroid|string|ID of the global macro.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*<br>- *required* for update operations|
|macro|string|Macro string.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* for create operations|
|value|string|Value of the macro.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *write-only* if `type` is set to "Secret macro"<br>- *required* for create operations|
|type|integer|Type of macro.<br><br>Possible values:<br>0 - *(default)* Text macro;<br>1 - Secret macro;<br>2 - Vault secret.|
|description|string|Description of the macro.|

[comment]: # ({/a510d71d-e654cd12})

[comment]: # ({7b361675-5a3a9f9f})
### Host macro

The host macro object defines a macro available on a host, host
prototype or template. It has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|hostmacroid|string|ID of the host macro.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*<br>- *required* for update operations|
|hostid|string|ID of the host that the macro belongs to.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *constant*<br>- *required* for create operations|
|macro|string|Macro string.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* for create operations|
|value|string|Value of the macro.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *write-only* if `type` is set to "Secret macro"<br>- *required* for create operations|
|type|integer|Type of macro.<br><br>Possible values:<br>0 - *(default)* Text macro;<br>1 - Secret macro;<br>2 - Vault secret.|
|description|string|Description of the macro.|
|automatic|integer|Defines whether the macro is controlled by discovery rule.<br><br>Possible values:<br>0 - *(default)* Macro is managed by user;<br>1 - Macro is managed by discovery rule.<br><br>User is not allowed to create automatic macro.<br>To update automatic macro, it must be [converted to manual](/manual/api/reference/usermacro/update#change-macro-value-that-was-created-by-discovery-rule).|

[comment]: # ({/7b361675-5a3a9f9f})
