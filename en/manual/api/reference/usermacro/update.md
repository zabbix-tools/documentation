[comment]: # ({2ed6e81f-2ed6e81f})
# usermacro.update

[comment]: # ({/2ed6e81f-2ed6e81f})

[comment]: # ({418ab7e0-23e3a148})
### Description

`object usermacro.update(object/array hostMacros)`

This method allows to update existing host macros.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/418ab7e0-23e3a148})

[comment]: # ({f8ce947c-f8ce947c})
### Parameters

`(object/array)` [Host macro properties](object#host_macro) to be
updated.

The `hostmacroid` property must be defined for each host macro, all
other properties are optional. Only the passed properties will be
updated, all others will remain unchanged.

[comment]: # ({/f8ce947c-f8ce947c})

[comment]: # ({5e19fc38-5e19fc38})
### Return values

`(object)` Returns an object containing the IDs of the updated host
macros under the `hostmacroids` property.

[comment]: # ({/5e19fc38-5e19fc38})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({985ee438-bc8ce678})
#### Changing the value of a host macro

Change the value of a host macro to "public".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "usermacro.update",
    "params": {
        "hostmacroid": "1",
        "value": "public"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "hostmacroids": [
            "1"
        ]
    },
    "id": 1
}
```

[comment]: # ({/985ee438-bc8ce678})

[comment]: # ({6948283e-129f453e})
#### Change macro value that was created by discovery rule

Convert discovery rule created "automatic" macro to "manual" and change its value to "new-value".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "usermacro.update",
    "params": {
        "hostmacroid": "1",
        "value": "new-value",
        "automatic": "0"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "hostmacroids": [
            "1"
        ]
    },
    "id": 1
}
```

[comment]: # ({/6948283e-129f453e})

[comment]: # ({2eddd7d2-2eddd7d2})
### Source

CUserMacro::update() in
*ui/include/classes/api/services/CUserMacro.php*.

[comment]: # ({/2eddd7d2-2eddd7d2})
