[comment]: # ({83b3ff24-83b3ff24})
# usermacro.create

[comment]: # ({/83b3ff24-83b3ff24})

[comment]: # ({51763cd1-db3d5c68})
### Description

`object usermacro.create(object/array hostMacros)`

This method allows to create new host macros.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/51763cd1-db3d5c68})

[comment]: # ({7c7df9b5-7c7df9b5})
### Parameters

`(object/array)` Host macros to create.

The method accepts host macros with the [standard host macro
properties](object#host_macro).

[comment]: # ({/7c7df9b5-7c7df9b5})

[comment]: # ({bd0c3c49-bd0c3c49})
### Return values

`(object)` Returns an object containing the IDs of the created host
macros under the `hostmacroids` property. The order of the returned IDs
matches the order of the passed host macros.

[comment]: # ({/bd0c3c49-bd0c3c49})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({b1d03639-7c0da88a})
#### Creating a host macro

Create a host macro "{$SNMP\_COMMUNITY}" with the value "public" on host
"10198".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "usermacro.create",
    "params": {
        "hostid": "10198",
        "macro": "{$SNMP_COMMUNITY}",
        "value": "public"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "hostmacroids": [
            "11"
        ]
    },
    "id": 1
}
```

[comment]: # ({/b1d03639-7c0da88a})

[comment]: # ({084b8f3c-084b8f3c})
### Source

CUserMacro::create() in
*ui/include/classes/api/services/CUserMacro.php*.

[comment]: # ({/084b8f3c-084b8f3c})
