[comment]: # ({f1e165e2-f1e165e2})
# usermacro.createglobal

[comment]: # ({/f1e165e2-f1e165e2})

[comment]: # ({c29b0af1-2655f9fe})
### Description

`object usermacro.createglobal(object/array globalMacros)`

This method allows to create new global macros.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/c29b0af1-2655f9fe})

[comment]: # ({3bf123cd-3bf123cd})
### Parameters

`(object/array)` Global macros to create.

The method accepts global macros with the [standard global macro
properties](object#global_macro).

[comment]: # ({/3bf123cd-3bf123cd})

[comment]: # ({0b8f323a-0b8f323a})
### Return values

`(object)` Returns an object containing the IDs of the created global
macros under the `globalmacroids` property. The order of the returned
IDs matches the order of the passed global macros.

[comment]: # ({/0b8f323a-0b8f323a})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({e95df171-752894f1})
#### Creating a global macro

Create a global macro "{$SNMP\_COMMUNITY}" with value "public".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "usermacro.createglobal",
    "params":  {
        "macro": "{$SNMP_COMMUNITY}",
        "value": "public"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "globalmacroids": [
            "6"
        ]
    },
    "id": 1
}
```

[comment]: # ({/e95df171-752894f1})

[comment]: # ({7b4f640f-7b4f640f})
### Source

CUserMacro::createGlobal() in
*ui/include/classes/api/services/CUserMacro.php*.

[comment]: # ({/7b4f640f-7b4f640f})
