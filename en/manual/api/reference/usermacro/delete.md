[comment]: # ({edb56cfa-edb56cfa})
# usermacro.delete

[comment]: # ({/edb56cfa-edb56cfa})

[comment]: # ({c54b3aeb-5eb2a780})
### Description

`object usermacro.delete(array hostMacroIds)`

This method allows to delete host macros.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/c54b3aeb-5eb2a780})

[comment]: # ({dd1b319f-dd1b319f})
### Parameters

`(array)` IDs of the host macros to delete.

[comment]: # ({/dd1b319f-dd1b319f})

[comment]: # ({cd694080-cd694080})
### Return values

`(object)` Returns an object containing the IDs of the deleted host
macros under the `hostmacroids` property.

[comment]: # ({/cd694080-cd694080})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({ca696482-b2f5bee1})
#### Deleting multiple host macros

Delete two host macros.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "usermacro.delete",
    "params": [
        "32",
        "11"
    ],
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "hostmacroids": [
            "32",
            "11"
        ]
    },
    "id": 1
}
```

[comment]: # ({/ca696482-b2f5bee1})

[comment]: # ({9d01c95a-9d01c95a})
### Source

CUserMacro::delete() in
*ui/include/classes/api/services/CUserMacro.php*.

[comment]: # ({/9d01c95a-9d01c95a})
