[comment]: # ({2fbbc9a6-2fbbc9a6})
# > Media type object

The following objects are directly related to the `mediatype` API.

[comment]: # ({/2fbbc9a6-2fbbc9a6})

[comment]: # ({4b53c1f9-4630c87a})
### Media type

The media type object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|mediatypeid|string|ID of the media type.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*<br>- *required* for update operations|
|name|string|Name of the media type.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* for create operations|
|type|integer|Transport used by the media type.<br><br>Possible values:<br>0 - Email;<br>1 - Script;<br>2 - SMS;<br>4 - Webhook.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* for create operations|
|exec\_path|string|For script media types `exec_path` contains the name of the executed script.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `type` is set to "Script"|
|gsm\_modem|string|Serial device name of the GSM modem.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `type` is set to "SMS"|
|passwd|string|Authentication password.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `type` is set to "Email"|
|smtp\_email|string|Email address from which notifications will be sent.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `type` is set to "Email"|
|smtp\_helo|string|SMTP HELO.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `type` is set to "Email"|
|smtp\_server|string|SMTP server.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `type` is set to "Email"|
|smtp\_port|integer|SMTP server port to connect to.|
|smtp\_security|integer|SMTP connection security level to use.<br><br>Possible values:<br>0 - None;<br>1 - STARTTLS;<br>2 - SSL/TLS.|
|smtp\_verify\_host|integer|SSL verify host for SMTP.<br><br>Possible values:<br>0 - No;<br>1 - Yes.|
|smtp\_verify\_peer|integer|SSL verify peer for SMTP.<br><br>Possible values:<br>0 - No;<br>1 - Yes.|
|smtp\_authentication|integer|SMTP authentication method to use.<br><br>Possible values:<br>0 - None;<br>1 - Normal password.|
|status|integer|Whether the media type is enabled.<br><br>Possible values:<br>0 - *(default)* Enabled;<br>1 - Disabled.|
|username|string|User name.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `type` is set to "Email"|
|maxsessions|integer|The maximum number of alerts that can be processed in parallel.<br><br>Possible values if `type` is set to "SMS": *(default)* 1.<br><br>Possible values if `type` is set to "Email", "Script", or "Webhook": 0-100.|
|maxattempts|integer|The maximum number of attempts to send an alert.<br><br>Possible values: 1-100.<br><br>Default value: 3.|
|attempt\_interval|string|The interval between retry attempts. Accepts seconds and time unit with suffix.<br><br>Possible values: 0-1h.<br><br>Default value: 10s.|
|content\_type|integer|Message format.<br><br>Possible values:<br>0 - plain text;<br>1 - *(default)* html.|
|script|string|Media type webhook script javascript body.|
|timeout|string|Media type webhook script timeout.<br>Accepts seconds and time unit with suffix.<br><br>Possible values: 1-60s.<br><br>Default: 30s.|
|process\_tags|integer|Defines should the webhook script response to be interpreted as tags and these tags should be added to associated event.<br><br>Possible values:<br>0 - *(default)* Ignore webhook script response;<br>1 - Process webhook script response as tags.|
|show\_event\_menu|integer|Show media type entry in `problem.get` and `event.get` property `urls`.<br><br>Possible values:<br>0 - *(default)* Do not add `urls` entry;<br>1 - Add media type to `urls` property.|
|event\_menu\_url|string|Define `url` property of media type entry in `urls` property of `problem.get` and `event.get`.|
|event\_menu\_name|string|Define `name` property of media type entry in `urls` property of `problem.get` and `event.get`.|
|parameters|array|Array of [webhook](#webhook-parameters) or [script](#script-parameters) input parameters.|
|description|string|Media type description.|

[comment]: # ({/4b53c1f9-4630c87a})

[comment]: # ({071b5d6c-46e1e59a})
### Webhook parameters

Parameters passed to a webhook script when it is being called have the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|name|string|Parameter name.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|value|string|Parameter value, supports macros.<br>Supported macros are described on the [Supported macros](/manual/appendix/macros/supported_by_location) page.|

[comment]: # ({/071b5d6c-46e1e59a})

[comment]: # ({cdfd3b21-parameters})
### Script parameters

Parameters passed to a script when it is being called have the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|**sortorder**<br>(required)|integer|The order in which the parameters will be passed to the script as command-line arguments. Starting with 0 as the first one.|
|value|string|Parameter value, supports macros.<br>Supported macros are described on the [Supported macros](/manual/appendix/macros/supported_by_location) page.|

[comment]: # ({/cdfd3b21-parameters})

[comment]: # ({ed4e5836-9c81491f})
### Message template

The message template object defines a template that will be used as a
default message for action operations to send a notification. It has the
following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|eventsource|integer|Event source.<br><br>Possible values:<br>0 - triggers;<br>1 - discovery;<br>2 - autoregistration;<br>3 - internal;<br>4 - services.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|recovery|integer|Operation mode.<br><br>Possible values:<br>0 - operations;<br>1 - recovery operations;<br>2 - update operations.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|subject|string|Message subject.|
|message|string|Message text.|

[comment]: # ({/ed4e5836-9c81491f})
