[comment]: # ({97b0e55a-97b0e55a})
# mediatype.update

[comment]: # ({/97b0e55a-97b0e55a})

[comment]: # ({6519686e-4cbfe789})
### Description

`object mediatype.update(object/array mediaTypes)`

This method allows to update existing media types.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/6519686e-4cbfe789})

[comment]: # ({32a8e1c7-7db8f4bd})
### Parameters

`(object/array)` Media type properties to be updated.

The `mediatypeid` property must be defined for each media type, all
other properties are optional. Only the passed properties will be
updated, all others will remain unchanged.

Additionally to the [standard media type properties](object#media_type),
the method accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|parameters|array|[Script](object#script_parameters) or [webhook](object#webhook_parameters) parameters to replace the current parameters.|
|message\_templates|array|[Message templates](object#message_template) to replace the current message templates.|

[comment]: # ({/32a8e1c7-7db8f4bd})

[comment]: # ({665d000d-665d000d})
### Return values

`(object)` Returns an object containing the IDs of the updated media
types under the `mediatypeids` property.

[comment]: # ({/665d000d-665d000d})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({740b73a6-8d3ba0cc})
#### Enabling a media type

Enable a media type, that is, set its status to "0".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "mediatype.update",
    "params": {
        "mediatypeid": "6",
        "status": "0"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "mediatypeids": [
            "6"
        ]
    },
    "id": 1
}
```

[comment]: # ({/740b73a6-8d3ba0cc})

[comment]: # ({b9cdc31e-b9cdc31e})
### Source

CMediaType::update() in
*ui/include/classes/api/services/CMediaType.php*.

[comment]: # ({/b9cdc31e-b9cdc31e})
