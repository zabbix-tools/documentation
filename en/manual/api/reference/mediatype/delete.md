[comment]: # ({03fad7e1-03fad7e1})
# mediatype.delete

[comment]: # ({/03fad7e1-03fad7e1})

[comment]: # ({a647c5bd-3ee75870})
### Description

`object mediatype.delete(array mediaTypeIds)`

This method allows to delete media types.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/a647c5bd-3ee75870})

[comment]: # ({854e3ea2-854e3ea2})
### Parameters

`(array)` IDs of the media types to delete.

[comment]: # ({/854e3ea2-854e3ea2})

[comment]: # ({ade2598d-ade2598d})
### Return values

`(object)` Returns an object containing the IDs of the deleted media
types under the `mediatypeids` property.

[comment]: # ({/ade2598d-ade2598d})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({abc79d72-09d9ff4a})
#### Deleting multiple media types

Delete two media types.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "mediatype.delete",
    "params": [
        "3",
        "5"
    ],
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "mediatypeids": [
            "3",
            "5"
        ]
    },
    "id": 1
}
```

[comment]: # ({/abc79d72-09d9ff4a})

[comment]: # ({68bd428e-68bd428e})
### Source

CMediaType::delete() in
*ui/include/classes/api/services/CMediaType.php*.

[comment]: # ({/68bd428e-68bd428e})
