[comment]: # ({bc1a928b-bc1a928b})
# map.delete

[comment]: # ({/bc1a928b-bc1a928b})

[comment]: # ({c7f8fca3-24398e69})
### Description

`object map.delete(array mapIds)`

This method allows to delete maps.

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/c7f8fca3-24398e69})

[comment]: # ({57a8b77a-57a8b77a})
### Parameters

`(array)` IDs of the maps to delete.

[comment]: # ({/57a8b77a-57a8b77a})

[comment]: # ({f04e039f-f04e039f})
### Return values

`(object)` Returns an object containing the IDs of the deleted maps
under the `sysmapids` property.

[comment]: # ({/f04e039f-f04e039f})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({4cd159b0-87539545})
#### Delete multiple maps

Delete two maps.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "map.delete",
    "params": [
        "12",
        "34"
    ],
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "sysmapids": [
            "12",
            "34"
        ]
    },
    "id": 1
}
```

[comment]: # ({/4cd159b0-87539545})

[comment]: # ({4880bc2b-4880bc2b})
### Source

CMap::delete() in *ui/include/classes/api/services/CMap.php*.

[comment]: # ({/4880bc2b-4880bc2b})
