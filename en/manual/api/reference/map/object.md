[comment]: # ({cdacabd4-cdacabd4})
# > Map object

The following objects are directly related to the `map` API.

[comment]: # ({/cdacabd4-cdacabd4})

[comment]: # ({79501e84-62f73225})
### Map

The map object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|sysmapid|string|ID of the map.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*<br>- *required* for update operations|
|height|integer|Height of the map in pixels.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* for create operations|
|name|string|Name of the map.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* for create operations|
|width|integer|Width of the map in pixels.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* for create operations|
|backgroundid|string|ID of the image used as the background for the map.|
|expand\_macros|integer|Whether to expand macros in labels when configuring the map.<br><br>Possible values:<br>0 - *(default)* do not expand macros;<br>1 - expand macros.|
|expandproblem|integer|Whether the problem trigger will be displayed for elements with a single problem.<br><br>Possible values:<br>0 - always display the number of problems;<br>1 - *(default)* display the problem trigger if there's only one problem.|
|grid\_align|integer|Whether to enable grid aligning.<br><br>Possible values:<br>0 - disable grid aligning;<br>1 - *(default)* enable grid aligning.|
|grid\_show|integer|Whether to show the grid on the map.<br><br>Possible values:<br>0 - do not show the grid;<br>1 - *(default)* show the grid.|
|grid\_size|integer|Size of the map grid in pixels.<br><br>Supported values: 20, 40, 50, 75 and 100.<br><br>Default: 50.|
|highlight|integer|Whether icon highlighting is enabled.<br><br>Possible values:<br>0 - highlighting disabled;<br>1 - *(default)* highlighting enabled.|
|iconmapid|string|ID of the icon map used on the map.|
|label\_format|integer|Whether to enable advanced labels.<br><br>Possible values:<br>0 - *(default)* disable advanced labels;<br>1 - enable advanced labels.|
|label\_location|integer|Location of the map element label.<br><br>Possible values:<br>0 - *(default)* bottom;<br>1 - left;<br>2 - right;<br>3 - top.|
|label\_string\_host|string|Custom label for host elements.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `label_type_host` is set to "custom"|
|label\_string\_hostgroup|string|Custom label for host group elements.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `label_type_hostgroup` is set to "custom"|
|label\_string\_image|string|Custom label for image elements.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `label_type_image` is set to "custom"|
|label\_string\_map|string|Custom label for map elements.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `label_type_map` is set to "custom"|
|label\_string\_trigger|string|Custom label for trigger elements.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `label_type_trigger` is set to "custom"|
|label\_type|integer|Map element label type.<br><br>Possible values:<br>0 - label;<br>1 - IP address;<br>2 - *(default)* element name;<br>3 - status only;<br>4 - nothing.|
|label\_type\_host|integer|Label type for host elements.<br><br>Possible values:<br>0 - label;<br>1 - IP address;<br>2 - *(default)* element name;<br>3 - status only;<br>4 - nothing;<br>5 - custom.|
|label\_type\_hostgroup|integer|Label type for host group elements.<br><br>Possible values:<br>0 - label;<br>2 - *(default)* element name;<br>3 - status only;<br>4 - nothing;<br>5 - custom.|
|label\_type\_image|integer|Label type for host group elements.<br><br>Possible values:<br>0 - label;<br>2 - *(default)* element name;<br>4 - nothing;<br>5 - custom.|
|label\_type\_map|integer|Label type for map elements.<br><br>Possible values:<br>0 - label;<br>2 - *(default)* element name;<br>3 - status only;<br>4 - nothing;<br>5 - custom.|
|label\_type\_trigger|integer|Label type for trigger elements.<br><br>Possible values:<br>0 - label;<br>2 - *(default)* element name;<br>3 - status only;<br>4 - nothing;<br>5 - custom.|
|markelements|integer|Whether to highlight map elements that have recently changed their status.<br><br>Possible values:<br>0 - *(default)* do not highlight elements;<br>1 - highlight elements.|
|severity\_min|integer|Minimum severity of the triggers that will be displayed on the map.<br><br>Refer to the [trigger `severity` property](/manual/api/reference/trigger/object#trigger) for a list of supported trigger severities.|
|show\_unack|integer|How problems should be displayed.<br><br>Possible values:<br>0 - *(default)* display the count of all problems;<br>1 - display only the count of unacknowledged problems;<br>2 - display the count of acknowledged and unacknowledged problems separately.|
|userid|string|Map owner user ID.|
|private|integer|Type of map sharing.<br><br>Possible values:<br>0 - public map;<br>1 - *(default)* private map.|
|show\_suppressed|integer|Whether suppressed problems are shown.<br><br>Possible values:<br>0 - *(default)* hide suppressed problems;<br>1 - show suppressed problems.|

[comment]: # ({/79501e84-62f73225})

[comment]: # ({8701172f-c7b92dfc})
### Map element

The map element object defines an object displayed on a map. It has the
following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|selementid|string|ID of the map element.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*|
|elements|array|Element data object.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `elementtype` is set to "host", "map", "trigger" or "host group"|
|elementtype|integer|Type of map element.<br><br>Possible values:<br>0 - host;<br>1 - map;<br>2 - trigger;<br>3 - host group;<br>4 - image.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|iconid\_off|string|ID of the image used to display the element in default state.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|areatype|integer|How separate host group hosts should be displayed.<br><br>Possible values:<br>0 - *(default)* the host group element will take up the whole map;<br>1 - the host group element will have a fixed size.|
|elementsubtype|integer|How a host group element should be displayed on a map.<br><br>Possible values:<br>0 - *(default)* display the host group as a single element;<br>1 - display each host in the group separately.|
|evaltype|integer|Map element tag filtering condition evaluation method.<br><br>Possible values:<br>0 - *(default)* AND / OR;<br>2 - OR.|
|height|integer|Height of the fixed size host group element in pixels.<br><br>Default: 200.|
|iconid\_disabled|string|ID of the image used to display disabled map elements.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `elementtype` is set to "host", "map", "trigger", or "host group"|
|iconid\_maintenance|string|ID of the image used to display map elements in maintenance.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `elementtype` is set to "host", "map", "trigger", or "host group"|
|iconid\_on|string|ID of the image used to display map elements with problems.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `elementtype` is set to "host", "map", "trigger", or "host group"|
|label|string|Label of the element.|
|label\_location|integer|Location of the map element label.<br><br>Possible values:<br>-1 - *(default)* default location;<br>0 - bottom;<br>1 - left;<br>2 - right;<br>3 - top.|
|permission|integer|Type of permission level.<br><br>Possible values:<br>-1 - none;<br>2 - read only;<br>3 - read-write.|
|sysmapid|string|ID of the map that the element belongs to.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*|
|urls|array|Map element URLs.<br><br>The map element URL object is [described in detail below](object#map_element_url).|
|use\_iconmap|integer|Whether icon mapping must be used for host elements.<br><br>Possible values:<br>0 - do not use icon mapping;<br>1 - *(default)* use icon mapping.|
|viewtype|integer|Host group element placing algorithm.<br><br>Possible values:<br>0 - *(default)* grid.|
|width|integer|Width of the fixed size host group element in pixels.<br><br>Default: 200.|
|x|integer|X-coordinates of the element in pixels.<br><br>Default: 0.|
|y|integer|Y-coordinates of the element in pixels.<br><br>Default: 0.|

[comment]: # ({/8701172f-c7b92dfc})

[comment]: # ({62e76926-305dd3e0})
#### Map element Host

The map element Host object defines one host element.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|hostid|string|Host ID|

[comment]: # ({/62e76926-305dd3e0})

[comment]: # ({95fc77e5-e1118d67})
#### Map element Host group

The map element Host group object defines one host group element.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|groupid|string|Host group ID|

[comment]: # ({/95fc77e5-e1118d67})

[comment]: # ({b9b86c4e-2d745dcc})
#### Map element Map

The map element Map object defines one map element.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|sysmapid|string|Map ID|

[comment]: # ({/b9b86c4e-2d745dcc})

[comment]: # ({5bd3a35a-0283fd46})
#### Map element Trigger

The map element Trigger object defines one or more trigger elements.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|triggerid|string|Trigger ID|

[comment]: # ({/5bd3a35a-0283fd46})

[comment]: # ({506c2f01-a7495119})
#### Map element tag

The map element tag object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|tag|string|Map element tag name.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|operator|string|Map element tag condition operator.<br><br>Possible values:<br>0 - *(default)* Contains;<br>1 - Equals;<br>2 - Does not contain;<br>3 - Does not equal;<br>4 - Exists;<br>5 - Does not exist.|
|value|string|Map element tag value.|

[comment]: # ({/506c2f01-a7495119})

[comment]: # ({b85044c9-5a112b10})
#### Map element URL

The map element URL object defines a clickable link that will be
available for a specific map element. It has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|sysmapelementurlid|string|ID of the map element URL.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*|
|name|string|Link caption.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|url|string|Link URL.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|selementid|string|ID of the map element that the URL belongs to.|

[comment]: # ({/b85044c9-5a112b10})

[comment]: # ({7b420354-1e44fd2a})
### Map link

The map link object defines a link between two map elements. It has the
following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|linkid|string|ID of the map link.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*|
|selementid1|string|ID of the first map element linked on one end.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|selementid2|string|ID of the first map element linked on the other end.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|color|string|Line color as a hexadecimal color code.<br><br>Default: `000000`.|
|drawtype|integer|Link line draw style.<br><br>Possible values:<br>0 - *(default)* line;<br>2 - bold line;<br>3 - dotted line;<br>4 - dashed line.|
|label|string|Link label.|
|linktriggers|array|Map link triggers to use as link status indicators.<br><br>The map link trigger object is [described in detail below](object#map_link_trigger).|
|permission|integer|Type of permission level.<br><br>Possible values:<br>-1 - none;<br>2 - read only;<br>3 - read-write.|
|sysmapid|string|ID of the map the link belongs to.|

[comment]: # ({/7b420354-1e44fd2a})

[comment]: # ({fc001cb5-219c2bce})
#### Map link trigger

The map link trigger object defines a map link status indicator based on
the state of a trigger. It has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|linktriggerid|string|ID of the map link trigger.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*|
|triggerid|string|ID of the trigger used as a link indicator.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|color|string|Indicator color as a hexadecimal color code.<br><br>Default: `DD0000`.|
|drawtype|integer|Indicator draw style.<br><br>Possible values:<br>0 - *(default)* line;<br>2 - bold line;<br>3 - dotted line;<br>4 - dashed line.|
|linkid|string|ID of the map link that the link trigger belongs to.|

[comment]: # ({/fc001cb5-219c2bce})

[comment]: # ({61b96c6d-96c2bee0})
### Map URL

The map URL object defines a clickable link that will be available for
all elements of a specific type on the map. It has the following
properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|sysmapurlid|string|ID of the map URL.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*|
|name|string|Link caption.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|url|string|Link URL.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|elementtype|integer|Type of map element for which the URL will be available.<br><br>Refer to the [map element `type` property](object#map_element) for a list of supported types.<br><br>Default: 0.|
|sysmapid|string|ID of the map that the URL belongs to.|

[comment]: # ({/61b96c6d-96c2bee0})

[comment]: # ({8cb7ba77-942f03ad})
### Map user

List of map permissions based on users. It has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|sysmapuserid|string|ID of the map user.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*|
|userid|string|User ID.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|permission|integer|Type of permission level.<br><br>Possible values:<br>2 - read only;<br>3 - read-write.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|

[comment]: # ({/8cb7ba77-942f03ad})

[comment]: # ({b7491ae6-f6e63a8f})
### Map user group

List of map permissions based on user groups. It has the following
properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|sysmapusrgrpid|string|ID of the map user group.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*|
|usrgrpid|string|User group ID.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|permission|integer|Type of permission level.<br><br>Possible values:<br>2 - read only;<br>3 - read-write.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|

[comment]: # ({/b7491ae6-f6e63a8f})

[comment]: # ({44fd42e0-9d0acde7})
### Map shapes

The map shape object defines a geometric shape (with or without text)
displayed on a map. It has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|sysmap\_shapeid|string|ID of the map shape element.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*|
|type|integer|Type of map shape element.<br><br>Possible values:<br>0 - rectangle;<br>1 - ellipse.<br><br>Property is required when new shapes are created.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|x|integer|X-coordinates of the shape in pixels.<br><br>Default: 0.|
|y|integer|Y-coordinates of the shape in pixels.<br><br>Default: 0.|
|width|integer|Width of the shape in pixels.<br><br>Default: 200.|
|height|integer|Height of the shape in pixels.<br><br>Default: 200.|
|text|string|Text of the shape.|
|font|integer|Font of the text within shape.<br><br>Possible values:<br>0 - Georgia, serif<br>1 - “Palatino Linotype”, “Book Antiqua”, Palatino, serif<br>2 - “Times New Roman”, Times, serif<br>3 - Arial, Helvetica, sans-serif<br>4 - “Arial Black”, Gadget, sans-serif<br>5 - “Comic Sans MS”, cursive, sans-serif<br>6 - Impact, Charcoal, sans-serif<br>7 - “Lucida Sans Unicode”, “Lucida Grande”, sans-serif<br>8 - Tahoma, Geneva, sans-serif<br>9 - “Trebuchet MS”, Helvetica, sans-serif<br>10 - Verdana, Geneva, sans-serif<br>11 - “Courier New”, Courier, monospace<br>12 - “Lucida Console”, Monaco, monospace<br><br>Default: 9.|
|font\_size|integer|Font size in pixels.<br><br>Default: 11.|
|font\_color|string|Font color.<br><br>Default: `000000`.|
|text\_halign|integer|Horizontal alignment of text.<br><br>Possible values:<br>0 - center;<br>1 - left;<br>2 - right.<br><br>Default: 0.|
|text\_valign|integer|Vertical alignment of text.<br><br>Possible values:<br>0 - middle;<br>1 - top;<br>2 - bottom.<br><br>Default: 0.|
|border\_type|integer|Type of the border.<br><br>Possible values:<br>0 - none;<br>1 - `—————`;<br>2 - `·····`;<br>3 - `- - -`.<br><br>Default: 0.|
|border\_width|integer|Width of the border in pixels.<br><br>Default: 0.|
|border\_color|string|Border color.<br><br>Default: `000000`.|
|background\_color|string|Background color (fill color).<br><br>Default: `(empty)`.|
|zindex|integer|Value used to order all shapes and lines (z-index).<br><br>Default: 0.|

[comment]: # ({/44fd42e0-9d0acde7})

[comment]: # ({f63599fe-b1536eee})
### Map lines

The map line object defines a line displayed on a map. It has the
following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|sysmap\_shapeid|string|ID of the map shape element.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*|
|x1|integer|X-coordinates of the line point 1 in pixels.<br><br>Default: 0.|
|y1|integer|Y-coordinates of the line point 1 in pixels.<br><br>Default: 0.|
|x2|integer|X-coordinates of the line point 2 in pixels.<br><br>Default: 200.|
|y2|integer|Y-coordinates of the line point 2 in pixels.<br><br>Default: 200.|
|line\_type|integer|Type of the lines.<br><br>Possible values:<br>0 - none;<br>1 - `—————`;<br>2 - `·····`;<br>3 - `- - -`.<br><br>Default: 0.|
|line\_width|integer|Width of the lines in pixels.<br><br>Default: 0.|
|line\_color|string|Line color.<br><br>Default: `000000`.|
|zindex|integer|Value used to order all shapes and lines (z-index).<br><br>Default: 0.|

[comment]: # ({/f63599fe-b1536eee})
