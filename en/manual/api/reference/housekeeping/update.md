[comment]: # ({17e99ab8-17e99ab8})
# housekeeping.update

[comment]: # ({/17e99ab8-17e99ab8})

[comment]: # ({9403302c-e666a98a})
### Description

`object housekeeping.update(object housekeeping)`

This method allows to update existing housekeeping settings.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/9403302c-e666a98a})

[comment]: # ({77d4245f-59628702})
### Parameters

`(object)` [Housekeeping properties](object#housekeeping) to be updated.

[comment]: # ({/77d4245f-59628702})

[comment]: # ({f63e579c-440363c6})
### Return values

`(array)` Returns an array with the names of updated parameters.

[comment]: # ({/f63e579c-440363c6})

[comment]: # ({e9fb8fb2-16b418fc})
### Examples

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "housekeeping.update",
    "params": {
        "hk_events_mode": "1",
        "hk_events_trigger": "200d",
        "hk_events_internal": "2d",
        "hk_events_discovery": "2d"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": [
        "hk_events_mode",
        "hk_events_trigger",
        "hk_events_internal",
        "hk_events_discovery"
    ],
    "id": 1
}
```

[comment]: # ({/e9fb8fb2-16b418fc})

[comment]: # ({d089883f-d089883f})
### Source

CHousekeeping::update() in
*ui/include/classes/api/services/CHousekeeping.php*.

[comment]: # ({/d089883f-d089883f})
