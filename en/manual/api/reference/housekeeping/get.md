[comment]: # ({8a5e44d6-8a5e44d6})
# housekeeping.get

[comment]: # ({/8a5e44d6-8a5e44d6})

[comment]: # ({20b555a9-695e57b4})
### Description

`object housekeeping.get(object parameters)`

The method allows to retrieve housekeeping object according to the given
parameters.

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/20b555a9-695e57b4})

[comment]: # ({c43c1377-24b20f8d})
### Parameters

`(object)` Parameters defining the desired output.

The method supports only one parameter.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|output|query|This parameter being common for all `get` methods described in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters).|

[comment]: # ({/c43c1377-24b20f8d})

[comment]: # ({490df090-490df090})
### Return values

`(object)` Returns housekeeping object.

[comment]: # ({/490df090-490df090})

[comment]: # ({98545d97-1d4e9444})
### Examples

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "housekeeping.get",
    "params": {
        "output": "extend"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "hk_events_mode": "1",
        "hk_events_trigger": "365d",
        "hk_events_service": "1d",
        "hk_events_internal": "1d",
        "hk_events_discovery": "1d",
        "hk_events_autoreg": "1d",
        "hk_services_mode": "1",
        "hk_services": "365d",
        "hk_audit_mode": "1",
        "hk_audit": "365d",
        "hk_sessions_mode": "1",
        "hk_sessions": "365d",
        "hk_history_mode": "1",
        "hk_history_global": "0",
        "hk_history": "90d",
        "hk_trends_mode": "1",
        "hk_trends_global": "0",
        "hk_trends": "365d",
        "db_extension": "",
        "compression_status": "0",
        "compress_older": "7d"
    },
    "id": 1
}
```

[comment]: # ({/98545d97-1d4e9444})

[comment]: # ({523212cf-523212cf})
### Source

CHousekeeping ::get() in
*ui/include/classes/api/services/CHousekeeping.php*.

[comment]: # ({/523212cf-523212cf})
