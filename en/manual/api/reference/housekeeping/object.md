[comment]: # ({1a412580-1a412580})
# > Housekeeping object

The following objects are directly related to the `housekeeping` API.

[comment]: # ({/1a412580-1a412580})

[comment]: # ({87b6fe8e-9d9d905d})
### Housekeeping

The settings object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|hk\_events\_mode|integer|Enable internal housekeeping for events and alerts.<br><br>Possible values:<br>0 - Disable;<br>1 - *(default)* Enable.|
|hk\_events\_trigger|string|Trigger data storage period. Accepts seconds and time unit with suffix.<br><br>Default: 365d.|
|hk\_events\_service|string|Service data storage period. Accepts seconds and time unit with suffix.<br><br>Default: 1d.|
|hk\_events\_internal|string|Internal data storage period. Accepts seconds and time unit with suffix.<br><br>Default: 1d.|
|hk\_events\_discovery|string|Network discovery data storage period. Accepts seconds and time unit with suffix.<br><br>Default: 1d.|
|hk\_events\_autoreg|string|Autoregistration data storage period. Accepts seconds and time unit with suffix.<br><br>Default: 1d.|
|hk\_services\_mode|integer|Enable internal housekeeping for services.<br><br>Possible values:<br>0 - Disable;<br>1 - *(default)* Enable.|
|hk\_services|string|Services data storage period. Accepts seconds and time unit with suffix.<br><br>Default: 365d.|
|hk\_audit\_mode|integer|Enable internal housekeeping for audit.<br><br>Possible values:<br>0 - Disable;<br>1 - *(default)* Enable.|
|hk\_audit|string|Audit data storage period. Accepts seconds and time unit with suffix.<br><br>Default: 365d.|
|hk\_sessions\_mode|integer|Enable internal housekeeping for sessions.<br><br>Possible values:<br>0 - Disable;<br>1 - *(default)* Enable.|
|hk\_sessions|string|Sessions data storage period. Accepts seconds and time unit with suffix.<br><br>Default: 365d.|
|hk\_history\_mode|integer|Enable internal housekeeping for history.<br><br>Possible values:<br>0 - Disable;<br>1 - *(default)* Enable.|
|hk\_history\_global|integer|Override item history period.<br><br>Possible values:<br>0 - Do not override;<br>1 - *(default)* Override.|
|hk\_history|string|History data storage period. Accepts seconds and time unit with suffix.<br><br>Default: 90d.|
|hk\_trends\_mode|integer|Enable internal housekeeping for trends.<br><br>Possible values:<br>0 - Disable;<br>1 - *(default)* Enable.|
|hk\_trends\_global|integer|Override item trend period.<br><br>Possible values:<br>0 - Do not override;<br>1 - *(default)* Override.|
|hk\_trends|string|Trends data storage period. Accepts seconds and time unit with suffix.<br><br>Default: 365d.|
|db\_extension|string|Configuration flag DB extension. If this flag is set to "timescaledb" then the server changes its behavior for housekeeping and item deletion.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*|
|compression\_availability|integer|Whether data compression is supported by the database (or its extension).<br><br>Possible values:<br>0 - Unavailable;<br>1 - Available.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*|
|compression\_status|integer|Enable TimescaleDB compression for history and trends.<br><br>Possible values:<br>0 - *(default)* Off;<br>1 - On.|
|compress\_older|string|Compress history and trends records older than specified period. Accepts seconds and time unit with suffix.<br><br>Default: 7d.|

[comment]: # ({/87b6fe8e-9d9d905d})
