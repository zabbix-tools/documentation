[comment]: # ({b3da5b48-11ad57db})
# Authentication

This class is designed to work with authentication settings.

Object references:

-   [Authentication](/manual/api/reference/authentication/object#authentication)

Available methods:

-   [authentication.get](/manual/api/reference/authentication/get) - retrieve authentication settings
-   [authentication.update](/manual/api/reference/authentication/update) - update authentication settings

[comment]: # ({/b3da5b48-11ad57db})
