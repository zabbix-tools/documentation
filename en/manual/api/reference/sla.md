[comment]: # ({0f4670a8-830462e8})
# SLA

This class is designed to work with SLA (Service Level Agreement) objects used to estimate the performance of IT infrastructure and business services.

Object references:

-   [SLA](/manual/api/reference/sla/object#sla)
-   [SLA schedule](/manual/api/reference/sla/object#sla_schedule)
-   [SLA excluded downtime](/manual/api/reference/sla/object#sla_excluded_downtime)
-   [SLA service tag](/manual/api/reference/sla/object#sla_service_tag)

Available methods:

-   [sla.create](/manual/api/reference/sla/create) - create new SLAs
-   [sla.delete](/manual/api/reference/sla/delete) - delete SLAs
-   [sla.get](/manual/api/reference/sla/get) - retrieve SLAs
-   [sla.getsli](/manual/api/reference/sla/getsli) - retrieve availability information as Service Level Indicator (SLI)
-   [sla.update](/manual/api/reference/sla/update) - update SLAs

[comment]: # ({/0f4670a8-830462e8})
