[comment]: # ({46429d7a-3b434db6})
# Housekeeping

This class is designed to work with housekeeping.

Object references:

-   [Housekeeping](/manual/api/reference/housekeeping/object#housekeeping)

Available methods:

-   [housekeeping.get](/manual/api/reference/housekeeping/get) - retrieve housekeeping
-   [housekeeping.update](/manual/api/reference/housekeeping/update) - update housekeeping

[comment]: # ({/46429d7a-3b434db6})
