[comment]: # ({4dda7c54-09ff1eef})
# Value map

This class is designed to work with value maps.

Object references:

-   [Value map](/manual/api/reference/valuemap/object#value_map)
-   [Value mappings](/manual/api/reference/valuemap/object#value_mappings)

Available methods:

-   [valuemap.create](/manual/api/reference/valuemap/create) - create new value maps
-   [valuemap.delete](/manual/api/reference/valuemap/delete) - delete value maps
-   [valuemap.get](/manual/api/reference/valuemap/get) - retrieve value maps
-   [valuemap.update](/manual/api/reference/valuemap/update) - update value maps

[comment]: # ({/4dda7c54-09ff1eef})
