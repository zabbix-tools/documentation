[comment]: # ({c0df6427-c0df6427})
# hostprototype.update

[comment]: # ({/c0df6427-c0df6427})

[comment]: # ({7da22d8d-796670ab})
### Description

`object hostprototype.update(object/array hostPrototypes)`

This method allows to update existing host prototypes.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/7da22d8d-796670ab})

[comment]: # ({4af04022-50727275})
### Parameters

`(object/array)` Host prototype properties to be updated.

The `hostid` property must be defined for each host prototype, all other
properties are optional. Only the passed properties will be updated, all
others will remain unchanged.

Additionally to the [standard host prototype
properties](object#host_prototype), the method accepts the following
parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|groupLinks|array|Group [links](/manual/api/reference/hostprototype/object#group_link) to replace the current group links on the host prototype.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *read-only* for inherited objects|
|groupPrototypes|array|Group [prototypes](/manual/api/reference/hostprototype/object#group_prototype) to replace the existing group prototypes on the host prototype.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *read-only* for inherited objects|
|macros|object/array|[User macros](/manual/api/reference/usermacro/object) to replace the current user macros.<br><br>All macros that are not listed in the request will be removed.|
|tags|object/array|Host prototype [tags](/manual/api/reference/hostprototype/object#host_prototype_tag) to replace the current tags.<br><br>All tags that are not listed in the request will be removed.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *read-only* for inherited objects|
|interfaces|object/array|Host prototype [custom interfaces](/manual/api/reference/hostprototype/object#custom_interface) to replace the current interfaces.<br><br>Custom interface object should contain all its parameters.<br>All interfaces that are not listed in the request will be removed.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *supported* if `custom_interfaces` of [Host prototype object](/manual/api/reference/usermacro/object#host-prototype) is set to "use host prototypes custom interfaces"<br>- *read-only* for inherited objects|
|templates|object/array|[Templates](/manual/api/reference/template/object) to replace the currently linked templates.<br><br>The templates must have the `templateid` property defined.|

[comment]: # ({/4af04022-50727275})

[comment]: # ({c97defa2-c97defa2})
### Return values

`(object)` Returns an object containing the IDs of the updated host
prototypes under the `hostids` property.

[comment]: # ({/c97defa2-c97defa2})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({b9437ec2-9e34a671})
#### Disabling a host prototype

Disable a host prototype, that is, set its status to "1".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "hostprototype.update",
    "params": {
        "hostid": "10092",
        "status": 1
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "hostids": [
            "10092"
        ]
    },
    "id": 1
}
```

[comment]: # ({/b9437ec2-9e34a671})

[comment]: # ({df5cfee0-440003e2})
#### Updating host prototype tags

Replace host prototype tags with new ones.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "hostprototype.update",
    "params": {
        "hostid": "10092",
        "tags": [
            {
                "tag": "Datacenter",
                "value": "{#DATACENTER.NAME}"
            },
            {
                "tag": "Instance type",
                "value": "{#INSTANCE_TYPE}"
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "hostids": [
            "10092"
        ]
    },
    "id": 1
}
```

[comment]: # ({/df5cfee0-440003e2})

[comment]: # ({5195d3ed-7a7b18a4})
#### Updating host prototype custom interfaces

Replace inherited interfaces with host prototype custom interfaces.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "hostprototype.update",
    "params": {
        "hostid": "10092",
        "custom_interfaces": "1",
        "interfaces": [
            {
                "main": "1",
                "type": "2",
                "useip": "1",
                "ip": "127.0.0.1",
                "dns": "",
                "port": "161",
                "details": {
                    "version": "2",
                    "bulk": "1",
                    "community": "{$SNMP_COMMUNITY}"
                }
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "hostids": [
            "10092"
        ]
    },
    "id": 1
}
```

[comment]: # ({/5195d3ed-7a7b18a4})

[comment]: # ({c4a38bf3-c4a38bf3})
### See also

-   [Group link](object#group_link)
-   [Group prototype](object#group_prototype)
-   [Host prototype tag](object#host_prototype_tag)
-   [Custom interface](object#custom_interface)
-   [User
    macro](/manual/api/reference/usermacro/object#hosttemplate_level_macro)

[comment]: # ({/c4a38bf3-c4a38bf3})

[comment]: # ({a380e3fb-a380e3fb})
### Source

CHostPrototype::update() in
*ui/include/classes/api/services/CHostPrototype.php*.

[comment]: # ({/a380e3fb-a380e3fb})
