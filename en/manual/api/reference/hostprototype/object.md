[comment]: # ({6e26e29b-6e26e29b})
# > Host prototype object

The following objects are directly related to the `hostprototype` API.

[comment]: # ({/6e26e29b-6e26e29b})

[comment]: # ({ca8ddca7-e69daa4d})
### Host prototype

The host prototype object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|hostid|string|ID of the host prototype.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*<br>- *required* for update operations|
|host|string|Technical name of the host prototype.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* for create operations<br>- *read-only* for inherited objects|
|name|string|Visible name of the host prototype.<br><br>Default: `host` property value.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only* for inherited objects|
|status|integer|Status of the host prototype.<br><br>Possible values:<br>0 - *(default)* monitored host;<br>1 - unmonitored host.|
|inventory\_mode|integer|Host inventory population mode.<br><br>Possible values:<br>-1 - *(default)* disabled;<br>0 - manual;<br>1 - automatic.|
|templateid|string|ID of the parent template host prototype.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*|
|discover|integer|Host prototype discovery status.<br><br>Possible values:<br>0 - *(default)* new hosts will be discovered;<br>1 - new hosts will not be discovered and existing hosts will be marked as lost.|
|custom\_interfaces|integer|Source of [custom interfaces](#custom-interface) for hosts created by the host prototype.<br><br>Possible values:<br>0 - *(default)* inherit interfaces from parent host;<br>1 - use host prototypes custom interfaces.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only* for inherited objects|
|uuid|string|Universal unique identifier, used for linking imported host prototypes to already existing ones. Auto-generated, if not given.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if the host prototype belongs to a template|

[comment]: # ({/ca8ddca7-e69daa4d})

[comment]: # ({e1d3a680-e3297546})
### Group link

The group link object links a host prototype with a host group.
It has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|groupid|string|ID of the host group.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|

[comment]: # ({/e1d3a680-e3297546})

[comment]: # ({1936e762-dabb4716})
### Group prototype

The group prototype object defines a group that will be created for a discovered host.
It has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|name|string|Name of the group prototype.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|

[comment]: # ({/1936e762-dabb4716})

[comment]: # ({98beb903-d42c6308})
### Host prototype tag

The host prototype tag object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|tag|string|Host prototype tag name.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|value|string|Host prototype tag value.|

[comment]: # ({/98beb903-d42c6308})

[comment]: # ({ddf4137f-ed8d8550})
### Custom interface

Custom interfaces are supported if `custom_interfaces` of [Host prototype object](#host-prototype) is set to "use host prototypes custom interfaces".
The custom interface object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|type|integer|Interface type.<br><br>Possible values:<br>1 - Agent;<br>2 - SNMP;<br>3 - IPMI;<br>4 - JMX.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|useip|integer|Whether the connection should be made via IP.<br><br>Possible values:<br>0 - connect using host DNS name;<br>1 - connect using host IP address.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|ip|string|IP address used by the interface.<br>Can contain macros.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `useip` is set to "connect using host IP address"|
|dns|string|DNS name used by the interface.<br>Can contain macros.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `useip` is set to "connect using host DNS name"|
|port|string|Port number used by the interface.<br>Can contain user and LLD macros.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|main|integer|Whether the interface is used as default on the host.<br>Only one interface of some type can be set as default on a host.<br><br>Possible values:<br>0 - not default;<br>1 - default.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|details|array|Additional object for interface.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `type` is set to "SNMP"|

[comment]: # ({/ddf4137f-ed8d8550})

[comment]: # ({9aa1cce1-6ea1b012})
### Custom interface details

The details object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|version|integer|SNMP interface version.<br><br>Possible values:<br>1 - SNMPv1;<br>2 - SNMPv2c;<br>3 - SNMPv3.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|bulk|integer|Whether to use bulk SNMP requests.<br><br>Possible values:<br>0 - don't use bulk requests;<br>1 - *(default)* - use bulk requests.|
|community|string|SNMP community.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `version` is set to "SNMPv1" or "SNMPv2c"|
|securityname|string|SNMPv3 security name.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `version` is set to "SNMPv3"|
|securitylevel|integer|SNMPv3 security level.<br><br>Possible values:<br>0 - *(default)* - noAuthNoPriv;<br>1 - authNoPriv;<br>2 - authPriv.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `version` is set to "SNMPv3"|
|authpassphrase|string|SNMPv3 authentication passphrase.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `version` is set to "SNMPv3" and `securitylevel` is set to "authNoPriv" or "authPriv"|
|privpassphrase|string|SNMPv3 privacy passphrase.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `version` is set to "SNMPv3" and `securitylevel` is set to "authPriv"|
|authprotocol|integer|SNMPv3 authentication protocol.<br><br>Possible values:<br>0 - *(default)* - MD5;<br>1 - SHA1;<br>2 - SHA224;<br>3 - SHA256;<br>4 - SHA384;<br>5 - SHA512.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `version` is set to "SNMPv3" and `securitylevel` is set to "authNoPriv" or "authPriv"|
|privprotocol|integer|SNMPv3 privacy protocol. Used only by SNMPv3 interfaces.<br><br>Possible values:<br>0 - *(default)* - DES;<br>1 - AES128;<br>2 - AES192;<br>3 - AES256;<br>4 - AES192C;<br>5 - AES256C.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `version` is set to "SNMPv3" and `securitylevel` is set to "authPriv"|
|contextname|string|SNMPv3 context name.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `version` is set to "SNMPv3"|

[comment]: # ({/9aa1cce1-6ea1b012})
