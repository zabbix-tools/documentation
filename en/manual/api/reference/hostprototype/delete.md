[comment]: # ({8f15992f-8f15992f})
# hostprototype.delete

[comment]: # ({/8f15992f-8f15992f})

[comment]: # ({7dc64059-f7c792a6})
### Description

`object hostprototype.delete(array hostPrototypeIds)`

This method allows to delete host prototypes.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/7dc64059-f7c792a6})

[comment]: # ({65284f99-65284f99})
### Parameters

`(array)` IDs of the host prototypes to delete.

[comment]: # ({/65284f99-65284f99})

[comment]: # ({78e6f07c-78e6f07c})
### Return values

`(object)` Returns an object containing the IDs of the deleted host
prototypes under the `hostids` property.

[comment]: # ({/78e6f07c-78e6f07c})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({01ce9708-48be53a2})
#### Deleting multiple host prototypes

Delete two host prototypes.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "hostprototype.delete",
    "params": [
        "10103",
        "10105"
    ],
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "hostids": [
            "10103",
            "10105"
        ]
    },
    "id": 1
}
```

[comment]: # ({/01ce9708-48be53a2})

[comment]: # ({dd993f84-dd993f84})
### Source

CHostPrototype::delete() in
*ui/include/classes/api/services/CHostPrototype.php*.

[comment]: # ({/dd993f84-dd993f84})
