[comment]: # ({984854ac-984854ac})
# hostprototype.create

[comment]: # ({/984854ac-984854ac})

[comment]: # ({3e4ca937-3ddb6b5d})
### Description

`object hostprototype.create(object/array hostPrototypes)`

This method allows to create new host prototypes.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/3e4ca937-3ddb6b5d})

[comment]: # ({d5692ee6-c4127cc1})
### Parameters

`(object/array)` Host prototypes to create.

Additionally to the [standard host prototype
properties](object#host_prototype), the method accepts the following
parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|groupLinks|array|Group [links](/manual/api/reference/hostprototype/object#group_link) to be created for the host prototype.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required*|
|ruleid|string|ID of the LLD rule that the host prototype belongs to.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required*|
|groupPrototypes|array|Group [prototypes](/manual/api/reference/hostprototype/object#group_prototype) to be created for the host prototype.|
|macros|object/array|[User macros](/manual/api/reference/usermacro/object) to be created for the host prototype.|
|tags|object/array|Host prototype [tags](/manual/api/reference/hostprototype/object#host_prototype_tag).|
|interfaces|object/array|Host prototype [custom interfaces](/manual/api/reference/hostprototype/object#custom_interface).|
|templates|object/array|[Templates](/manual/api/reference/template/object) to be linked to the host prototype.<br><br>The templates must have the `templateid` property defined.|

[comment]: # ({/d5692ee6-c4127cc1})

[comment]: # ({b9613119-b9613119})
### Return values

`(object)` Returns an object containing the IDs of the created host
prototypes under the `hostids` property. The order of the returned IDs
matches the order of the passed host prototypes.

[comment]: # ({/b9613119-b9613119})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({41e9cbc6-0ce023ac})
#### Creating a host prototype

Create a host prototype "{\#VM.NAME}" on LLD rule "23542" with a group
prototype "{\#HV.NAME}", tag pair "Datacenter": "{\#DATACENTER.NAME}"
and custom SNMPv2 interface 127.0.0.1:161 with community
{$SNMP\_COMMUNITY}. Link it to host group "2".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "hostprototype.create",
    "params": {
        "host": "{#VM.NAME}",
        "ruleid": "23542",
        "custom_interfaces": "1",
        "groupLinks": [
            {
                "groupid": "2"
            }
        ],
        "groupPrototypes": [
            {
                "name": "{#HV.NAME}"
            }
        ],
        "tags": [
            {
                "tag": "Datacenter",
                "value": "{#DATACENTER.NAME}"
            }
        ],
        "interfaces": [
            {
                "main": "1",
                "type": "2",
                "useip": "1",
                "ip": "127.0.0.1",
                "dns": "",
                "port": "161",
                "details": {
                    "version": "2",
                    "bulk": "1",
                    "community": "{$SNMP_COMMUNITY}"
                }
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "hostids": [
            "10103"
        ]
    },
    "id": 1
}
```

[comment]: # ({/41e9cbc6-0ce023ac})

[comment]: # ({c4a38bf3-c4a38bf3})
### See also

-   [Group link](object#group_link)
-   [Group prototype](object#group_prototype)
-   [Host prototype tag](object#host_prototype_tag)
-   [Custom interface](object#custom_interface)
-   [User
    macro](/manual/api/reference/usermacro/object#hosttemplate_level_macro)

[comment]: # ({/c4a38bf3-c4a38bf3})

[comment]: # ({4d10a354-4d10a354})
### Source

CHostPrototype::create() in
*ui/include/classes/api/services/CHostPrototype.php*.

[comment]: # ({/4d10a354-4d10a354})
