[comment]: # ({cc00d3db-cc00d3db})
# itemprototype.update

[comment]: # ({/cc00d3db-cc00d3db})

[comment]: # ({189466f4-63442cbb})
### Description

`object itemprototype.update(object/array itemPrototypes)`

This method allows to update existing item prototypes.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/189466f4-63442cbb})

[comment]: # ({607a518f-16fcb884})
### Parameters

`(object/array)` Item prototype properties to be updated.

The `itemid` property must be defined for each item prototype, all other
properties are optional. Only the passed properties will be updated, all
others will remain unchanged.

Additionally to the [standard item prototype
properties](object#item_prototype), the method accepts the following
parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|preprocessing|array|Item prototype [preprocessing](/manual/api/reference/itemprototype/object#item_prototype_preprocessing) options to replace the current preprocessing options.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *read-only* for inherited objects|
|tags|array|Item prototype [tags](/manual/api/reference/itemprototype/object#item_prototype_tag).|

[comment]: # ({/607a518f-16fcb884})

[comment]: # ({f7f1feb9-f7f1feb9})
### Return values

`(object)` Returns an object containing the IDs of the updated item
prototypes under the `itemids` property.

[comment]: # ({/f7f1feb9-f7f1feb9})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({817dd908-48cea947})
#### Changing the interface of an item prototype

Change the host interface that will be used by discovered items.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "itemprototype.update",
    "params": {
        "itemid": "27428",
        "interfaceid": "132"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "27428"
        ]
    },
    "id": 1
}
```

[comment]: # ({/817dd908-48cea947})

[comment]: # ({6430348b-e1d8fc73})
#### Update dependent item prototype

Update Dependent item prototype with new Master item prototype ID. Only
dependencies on same host (template/discovery rule) are allowed,
therefore Master and Dependent item should have same hostid and ruleid.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "itemprototype.update",
    "params": {
        "master_itemid": "25570",
        "itemid": "189030"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "189030"
        ]
    },
    "id": 1
}
```

[comment]: # ({/6430348b-e1d8fc73})

[comment]: # ({78304ef2-d7fe7206})
#### Update HTTP agent item prototype

Change query fields and remove all custom headers.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "itemprototype.update",
    "params": {
        "itemid":"28305",
        "query_fields": [
            {
                "random": "qwertyuiopasdfghjklzxcvbnm"
            }
        ],
        "headers": []
    }
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "28305"
        ]
    },
    "id": 1
}
```

[comment]: # ({/78304ef2-d7fe7206})

[comment]: # ({42088f69-e5fc44e0})
#### Updating item preprocessing options

Update an item prototype with item preprocessing rule “Custom
multiplier”.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "itemprototype.update",
    "params": {
        "itemid": "44211",
        "preprocessing": [
            {
                "type": 1,
                "params": "4",
                "error_handler": 2,
                "error_handler_params": "5"
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "44211"
        ]
    },
    "id": 1
}
```

[comment]: # ({/42088f69-e5fc44e0})

[comment]: # ({c56158ac-15ba4525})
#### Updating a script item prototype

Update a script item prototype with a different script and remove
unnecessary parameters that were used by previous script.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "itemprototype.update",
    "params": {
        "itemid": "23865",
        "parameters": [],
        "script": "Zabbix.log(3, 'Log test');\nreturn 1;"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "23865"
        ]
    },
    "id": 1
}
```

[comment]: # ({/c56158ac-15ba4525})

[comment]: # ({bc6956c2-bc6956c2})
### Source

CItemPrototype::update() in
*ui/include/classes/api/services/CItemPrototype.php*.

[comment]: # ({/bc6956c2-bc6956c2})
