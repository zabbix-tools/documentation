[comment]: # ({dbb7dd13-dbb7dd13})
# > Item prototype object

The following objects are directly related to the `itemprototype` API.

[comment]: # ({/dbb7dd13-dbb7dd13})

[comment]: # ({780d0cad-36a2631c})
### Item prototype

The item prototype object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|itemid|string|ID of the item prototype.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*<br>- *required* for update operations|
|delay|string|Update interval of the item prototype.<br>Accepts seconds or a time unit with suffix (30s,1m,2h,1d).<br>Optionally one or more [custom intervals](/manual/config/items/item/custom_intervals) can be specified either as flexible intervals or scheduling.<br>Multiple intervals are separated by a semicolon.<br>User macros and LLD macros may be used. A single macro has to fill the whole field. Multiple macros in a field or macros mixed with text are not supported.<br>Flexible intervals may be written as two macros separated by a forward slash (e.g. `{$FLEX_INTERVAL}/{$FLEX_PERIOD}`).<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `type` is set to "Zabbix agent" (0), "Simple check" (3), "Zabbix internal" (5), "External check" (10), "Database monitor" (11), "IPMI agent" (12), "SSH agent" (13), "TELNET agent" (14), "Calculated" (15), "JMX agent" (16), "HTTP agent" (19), "SNMP agent" (20), "Script" (21), or if `type` is set to "Zabbix agent (active)" (7) and `key_` does not contain "mqtt.get"|
|hostid|string|ID of the host that the item prototype belongs to.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *constant*<br>- *required* for create operations|
|interfaceid|string|ID of the item prototype's host interface.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if item prototype belongs to host and `type` is set to "Zabbix agent", "IPMI agent", "JMX agent", "SNMP trap", or "SNMP agent"<br>- *supported* if item prototype belongs to host and `type` is set to "Simple check", "External check", "SSH agent", "TELNET agent", or "HTTP agent"|
|key\_|string|Item prototype key.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* for create operations<br>- *read-only* for inherited objects|
|name|string|Name of the item prototype.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* for create operations<br>- *read-only* for inherited objects|
|type|integer|Type of the item prototype.<br><br>Possible values:<br>0 - Zabbix agent;<br>2 - Zabbix trapper;<br>3 - Simple check;<br>5 - Zabbix internal;<br>7 - Zabbix agent (active);<br>10 - External check;<br>11 - Database monitor;<br>12 - IPMI agent;<br>13 - SSH agent;<br>14 - TELNET agent;<br>15 - Calculated;<br>16 - JMX agent;<br>17 - SNMP trap;<br>18 - Dependent item;<br>19 - HTTP agent;<br>20 - SNMP agent;<br>21 - Script.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* for create operations<br>- *read-only* for inherited objects|
|url|string|URL string.<br>Supports LLD macros, user macros, {HOST.IP}, {HOST.CONN}, {HOST.DNS}, {HOST.HOST}, {HOST.NAME}, {ITEM.ID}, {ITEM.KEY}.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `type` is set to "HTTP agent"<br>- *read-only* for inherited objects|
|value\_type|integer|Type of information of the item prototype.<br><br>Possible values:<br>0 - numeric float;<br>1 - character;<br>2 - log;<br>3 - numeric unsigned;<br>4 - text.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* for create operations<br>- *read-only* for inherited objects|
|allow\_traps|integer|Allow to populate value similarly to the trapper item.<br><br>0 - *(default)* Do not allow to accept incoming data;<br>1 - Allow to accept incoming data.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `type` is set to "HTTP agent"|
|authtype|integer|Authentication method.<br><br>Possible values if `type` is set to "SSH agent":<br>0 - *(default)* password;<br>1 - public key.<br><br>Possible values if `type` is set to "HTTP agent":<br>0 - *(default)* none;<br>1 - basic;<br>2 - NTLM;<br>3 - Kerberos.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `type` is set to "SSH agent" or "HTTP agent"<br>- *read-only* for inherited objects (if `type` is set to "HTTP agent")|
|description|string|Description of the item prototype.|
|follow\_redirects|integer|Follow response redirects while pooling data.<br><br>Possible values:<br>0 - Do not follow redirects;<br>1 - *(default)* Follow redirects.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `type` is set to "HTTP agent"<br>- *read-only* for inherited objects|
|headers|object|Object with HTTP(S) request headers, where header name is used as key and header value as value.<br><br>Example: { "User-Agent": "Zabbix" }<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `type` is set to "HTTP agent"<br>- *read-only* for inherited objects|
|history|string|A time unit of how long the history data should be stored.<br>Also accepts user macro and LLD macro.<br><br>Default: 90d.|
|http\_proxy|string|HTTP(S) proxy connection string.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `type` is set to "HTTP agent"<br>- *read-only* for inherited objects|
|ipmi\_sensor|string|IPMI sensor.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `type` is set to "IPMI agent" and `key_` is not set to "ipmi.get"<br>- *supported* if `type` is set to "IPMI agent"<br>- *read-only* for inherited objects|
|jmx\_endpoint|string|JMX agent custom connection string.<br><br>Default: service:jmx:rmi:///jndi/rmi://{HOST.CONN}:{HOST.PORT}/jmxrmi<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `type` is set to "JMX agent"|
|logtimefmt|string|Format of the time in log entries.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `value_type` is set to "log"<br>- *read-only* for inherited objects|
|master\_itemid|integer|Master item ID.<br>Recursion up to 3 dependent items and item prototypes and maximum count of dependent items and item prototypes equal to 29999 are allowed.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `type` is set to "Dependent item"<br>- *read-only* for inherited objects|
|output\_format|integer|Should the response be converted to JSON.<br><br>Possible values:<br>0 - *(default)* Store raw;<br>1 - Convert to JSON.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `type` is set to "HTTP agent"<br>- *read-only* for inherited objects|
|params|string|Additional parameters depending on the type of the item prototype:<br>- executed script for SSH agent and TELNET agent item prototypes;<br>- SQL query for database monitor item prototypes;<br>- formula for calculated item prototypes.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `type` is set to "Database monitor", "SSH agent", "TELNET agent", "Calculated", or "Script"<br>- *read-only* for inherited objects (if `type` is set to "Script")|
|parameters|array|Additional parameters if `type` is set to "Script". Array of objects with `name` and `value` properties, where `name` must be unique.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `type` is set to "Script"<br>- *read-only* for inherited objects|
|password|string|Password for authentication.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `type` is set to "JMX agent" and `username` is set<br>- *supported* if `type` is set to "Simple check", "SSH agent", "TELNET agent", "Database monitor", or "HTTP agent"<br>- *read-only* for inherited objects (if `type` is set to "HTTP agent")|
|post\_type|integer|Type of post data body stored in `posts` property.<br><br>Possible values:<br>0 - *(default)* Raw data.<br>2 - JSON data.<br>3 - XML data.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `type` is set to "HTTP agent"<br>- *read-only* for inherited objects|
|posts|string|HTTP(S) request body data.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `type` is set to "HTTP agent" and `post_type` is set to "JSON data" or "XML data"<br>- *supported* if `type` is set to "HTTP agent" and `post_type` is set to "Raw data"<br>- *read-only* for inherited objects|
|privatekey|string|Name of the private key file.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `type` is set to "SSH agent" and `authtype` is set to "public key"|
|publickey|string|Name of the public key file.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `type` is set to "SSH agent" and `authtype` is set to "public key"|
|query\_fields|array|Query parameters. Array of objects with `key`:`value` pairs, where `value` can be empty string.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `type` is set to "HTTP agent"<br>- *read-only* for inherited objects|
|request\_method|integer|Type of request method.<br><br>Possible values:<br>0 - *(default)* GET;<br>1 - POST;<br>2 - PUT;<br>3 - HEAD.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `type` is set to "HTTP agent"<br>- *read-only* for inherited objects|
|retrieve\_mode|integer|What part of response should be stored.<br><br>Possible values if `request_method` is set to "GET", "POST", or "PUT":<br>0 - *(default)* Body;<br>1 - Headers;<br>2 - Both body and headers will be stored.<br><br>Possible values if `request_method` is set to "HEAD":<br>1 - Headers.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `type` is set to "HTTP agent"<br>- *read-only* for inherited objects|
|snmp\_oid|string|SNMP OID.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `type` is set to "SNMP agent"<br>- *read-only* for inherited objects|
|ssl\_cert\_file|string|Public SSL Key file path.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `type` is set to "HTTP agent"<br>- *read-only* for inherited objects|
|ssl\_key\_file|string|Private SSL Key file path.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `type` is set to "HTTP agent"<br>- *read-only* for inherited objects|
|ssl\_key\_password|string|Password for SSL Key file.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `type` is set to "HTTP agent"<br>- *read-only* for inherited objects|
|status|integer|Status of the item prototype.<br><br>Possible values:<br>0 - *(default)* enabled item prototype;<br>1 - disabled item prototype;<br>3 - unsupported item prototype.|
|status\_codes|string|Ranges of required HTTP status codes, separated by commas.<br>Also supports user macros or LLD macros as part of comma separated list.<br><br>Example: 200,200-{$M},{$M},200-400<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `type` is set to "HTTP agent"<br>- *read-only* for inherited objects|
|templateid|string|ID of the parent template item prototype.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*|
|timeout|string|Item data polling request timeout.<br>Supports user macros and LLD macros.<br><br>Default: `3s`.<br>Maximum value: `60s`.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `type` is set to "HTTP agent" or "Script"<br>- *read-only* for inherited objects|
|trapper\_hosts|string|Allowed hosts.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `type` is set to "Zabbix trapper", or if `type` is set to "HTTP agent" and `allow_traps` is set to "Allow to accept incoming data"|
|trends|string|A time unit of how long the trends data should be stored.<br>Also accepts user macro and LLD macro.<br><br>Default: 365d.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `value_type` is set to "numeric float" or "numeric unsigned"|
|units|string|Value units.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `value_type` is set to "numeric float" or "numeric unsigned"<br>- *read-only* for inherited objects|
|username|string|Username for authentication.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `type` is set to "SSH agent" or "TELNET agent", or if `type` is set to "JMX agent" and `password` is set<br>- *supported* if `type` is set to "Simple check", "Database monitor", or "HTTP agent"<br>- *read-only* for inherited objects (if `type` is set to "HTTP agent")|
|uuid|string|Universal unique identifier, used for linking imported item prototypes to already existing ones. Auto-generated, if not given.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if the item prototype belongs to a template|
|valuemapid|string|ID of the associated value map.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `value_type` is set to "numeric float", "character", or "numeric unsigned"<br>- *read-only* for inherited objects|
|verify\_host|integer|Validate host name in URL is in *Common Name* field or a *Subject Alternate Name* field of host certificate.<br><br>Possible values:<br>0 - *(default)* Do not validate;<br>1 - Validate.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `type` is set to "HTTP agent"<br>- *read-only* for inherited objects|
|verify\_peer|integer|Validate is host certificate authentic.<br><br>Possible values:<br>0 - *(default)* Do not validate;<br>1 - Validate.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `type` is set to "HTTP agent"<br>- *read-only* for inherited objects|
|discover|integer|Item prototype discovery status.<br><br>Possible values:<br>0 - *(default)* new items will be discovered;<br>1 - new items will not be discovered and existing items will be marked as lost.|

[comment]: # ({/780d0cad-36a2631c})

[comment]: # ({89948444-66f440ed})
### Item prototype tag

The item prototype tag object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|tag|string|Item prototype tag name.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|value|string|Item prototype tag value.|

[comment]: # ({/89948444-66f440ed})

[comment]: # ({ad00a112-ad34a8b2})
### Item prototype preprocessing

The item prototype preprocessing object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|type|integer|The preprocessing option type.<br><br>Possible values:<br>1 - Custom multiplier;<br>2 - Right trim;<br>3 - Left trim;<br>4 - Trim;<br>5 - Regular expression matching;<br>6 - Boolean to decimal;<br>7 - Octal to decimal;<br>8 - Hexadecimal to decimal;<br>9 - Simple change;<br>10 - Change per second;<br>11 - XML XPath;<br>12 - JSONPath;<br>13 - In range;<br>14 - Matches regular expression;<br>15 - Does not match regular expression;<br>16 - Check for error in JSON;<br>17 - Check for error in XML;<br>18 - Check for error using regular expression;<br>19 - Discard unchanged;<br>20 - Discard unchanged with heartbeat;<br>21 - JavaScript;<br>22 - Prometheus pattern;<br>23 - Prometheus to JSON;<br>24 - CSV to JSON;<br>25 - Replace;<br>26 - Check unsupported;<br>27 - XML to JSON;<br>28 - SNMP walk value;<br>29 - SNMP walk to JSON.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|params|string|Additional parameters used by preprocessing option.<br>Multiple parameters are separated by the newline (\\n) character.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `type` is set to "Custom multiplier" (1), "Right trim" (2), "Left trim" (3), "Trim" (4), "Regular expression matching" (5), "XML XPath" (11), "JSONPath" (12), "In range" (13), "Matches regular expression" (14), "Does not match regular expression" (15), "Check for error in JSON" (16), "Check for error in XML" (17), "Check for error using regular expression" (18), "Discard unchanged with heartbeat" (20), "JavaScript" (21), "Prometheus pattern" (22), "Prometheus to JSON" (23), "CSV to JSON" (24), "Replace" (25), "SNMP walk value" (28), or "SNMP walk to JSON" (29)|
|error\_handler|integer|Action type used in case of preprocessing step failure.<br><br>Possible values:<br>0 - Error message is set by Zabbix server;<br>1 - Discard value;<br>2 - Set custom value;<br>3 - Set custom error message.<br><br>Possible values if `type` is set to "Check unsupported":<br>1 - Discard value;<br>2 - Set custom value;<br>3 - Set custom error message.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `type` is set to "Custom multiplier" (1), "Regular expression matching" (5), "Boolean to decimal" (6), "Octal to decimal" (7), "Hexadecimal to decimal" (8), "Simple change" (9), "Change per second" (10), "XML XPath" (11), "JSONPath" (12), "In range" (13), "Matches regular expression" (14), "Does not match regular expression" (15), "Check for error in JSON" (16), "Check for error in XML" (17), "Check for error using regular expression" (18), "Prometheus pattern" (22), "Prometheus to JSON" (23), "CSV to JSON" (24), "Check unsupported" (26), "XML to JSON" (27), "SNMP walk value" (28), or "SNMP walk to JSON" (29)|
|error\_handler\_params|string|Error handler parameters.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `error_handler` is set to "Set custom value" or "Set custom error message"|

The following parameters and error handlers are supported for each preprocessing type.

|Preprocessing type|Name|Parameter 1|Parameter 2|Parameter 3|Supported error handlers|
|------------------|----|-----------|-----------|-----------|------------------------|
|1|Custom multiplier|number^1,\ 6^|<|<|0, 1, 2, 3|
|2|Right trim|list of characters^2^|<|<|<|
|3|Left trim|list of characters^2^|<|<|<|
|4|Trim|list of characters^2^|<|<|<|
|5|Regular expression|pattern^3^|output^2^|<|0, 1, 2, 3|
|6|Boolean to decimal|<|<|<|0, 1, 2, 3|
|7|Octal to decimal|<|<|<|0, 1, 2, 3|
|8|Hexadecimal to decimal|<|<|<|0, 1, 2, 3|
|9|Simple change|<|<|<|0, 1, 2, 3|
|10|Change per second|<|<|<|0, 1, 2, 3|
|11|XML XPath|path^4^|<|<|0, 1, 2, 3|
|12|JSONPath|path^4^|<|<|0, 1, 2, 3|
|13|In range|min^1,\ 6^|max^1,\ 6^|<|0, 1, 2, 3|
|14|Matches regular expression|pattern^3^|<|<|0, 1, 2, 3|
|15|Does not match regular expression|pattern^3^|<|<|0, 1, 2, 3|
|16|Check for error in JSON|path^4^|<|<|0, 1, 2, 3|
|17|Check for error in XML|path^4^|<|<|0, 1, 2, 3|
|18|Check for error using regular expression|pattern^3^|output^2^|<|0, 1, 2, 3|
|19|Discard unchanged|<|<|<|<|
|20|Discard unchanged with heartbeat|seconds^5,\ 6^|<|<|<|
|21|JavaScript|script^2^|<|<|<|
|22|Prometheus pattern|pattern^6,\ 7^|`value`, `label`, `function`|output^8,\ 9^|0, 1, 2, 3|
|23|Prometheus to JSON|pattern^6,\ 7^|<|<|0, 1, 2, 3|
|24|CSV to JSON|character^2^|character^2^|0,1|0, 1, 2, 3|
|25|Replace|search string^2^|replacement^2^|<|<|
|26|Check unsupported|<|<|<|1, 2, 3|
|27|XML to JSON|<|<|<|0, 1, 2, 3|
|28|SNMP walk value|OID^2^|Format:<br>0 - Unchanged<br>1 - UTF-8 from Hex-STRING<br>2 - MAC from Hex-STRING<br>3 - Integer from BITS|<|0, 1, 2, 3|
|29|SNMP walk to JSON^10^|Field name^2^|OID prefix^2^|Format:<br>0 - Unchanged<br>1 - UTF-8 from Hex-STRING<br>2 - MAC from Hex-STRING<br>3 - Integer from BITS|0, 1, 2, 3|

^1^ integer or floating-point number\
^2^ string\
^3^ regular expression\
^4^ JSONPath or XML XPath\
^5^ positive integer (with support of time suffixes, e.g. 30s, 1m, 2h,
1d)\
^6^ user macro, LLD macro\
^7^ Prometheus pattern following the syntax:
`<metric name>{<label name>="<label value>", ...} == <value>`. Each
Prometheus pattern component (metric, label name, label value and metric
value) can be user macro or LLD macro.\
^8^ Prometheus output following the syntax: `<label name>` (can be a user macro or an LLD macro) if `label` is selected as the
second parameter.\
^9^ One of the aggregation functions: `sum`, `min`, `max`, `avg`, `count` if `function` is selected as the second
parameter.\
^10^ Supports multiple "Field name,OID prefix,Format records" records delimited by a new line character.

[comment]: # ({/ad00a112-ad34a8b2})
