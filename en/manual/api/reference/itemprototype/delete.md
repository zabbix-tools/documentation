[comment]: # ({1b9515ab-1b9515ab})
# itemprototype.delete

[comment]: # ({/1b9515ab-1b9515ab})

[comment]: # ({88b4accc-ee2fdfa0})
### Description

`object itemprototype.delete(array itemPrototypeIds)`

This method allows to delete item prototypes.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/88b4accc-ee2fdfa0})

[comment]: # ({7755a7b3-7755a7b3})
### Parameters

`(array)` IDs of the item prototypes to delete.

[comment]: # ({/7755a7b3-7755a7b3})

[comment]: # ({db63ca62-db63ca62})
### Return values

`(object)` Returns an object containing the IDs of the deleted item
prototypes under the `prototypeids` property.

[comment]: # ({/db63ca62-db63ca62})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({f0b002c2-0c135019})
#### Deleting multiple item prototypes

Delete two item prototypes.\
Dependent item prototypes are removed automatically if master item or
item prototype is deleted.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "itemprototype.delete",
    "params": [
        "27352",
        "27356"
    ],
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "prototypeids": [
            "27352",
            "27356"
        ]
    },
    "id": 1
}
```

[comment]: # ({/f0b002c2-0c135019})

[comment]: # ({8cee4f1f-8cee4f1f})
### Source

CItemPrototype::delete() in
*ui/include/classes/api/services/CItemPrototype.php*.

[comment]: # ({/8cee4f1f-8cee4f1f})
