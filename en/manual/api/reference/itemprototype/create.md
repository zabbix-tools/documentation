[comment]: # ({450c25c2-450c25c2})
# itemprototype.create

[comment]: # ({/450c25c2-450c25c2})

[comment]: # ({95b293d4-0ad3a6f6})
### Description

`object itemprototype.create(object/array itemPrototypes)`

This method allows to create new item prototypes.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/95b293d4-0ad3a6f6})

[comment]: # ({c8087876-56c4ce2f})
### Parameters

`(object/array)` Item prototype to create.

Additionally to the [standard item prototype
properties](object#item_prototype), the method accepts the following
parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|ruleid|string|ID of the LLD rule that the item belongs to.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required*|
|preprocessing|array|Item prototype [preprocessing](/manual/api/reference/itemprototype/object#item_prototype_preprocessing) options.|
|tags|array|Item prototype [tags](/manual/api/reference/itemprototype/object#item_prototype_tag).|

[comment]: # ({/c8087876-56c4ce2f})

[comment]: # ({4682a6ee-4682a6ee})
### Return values

`(object)` Returns an object containing the IDs of the created item
prototypes under the `itemids` property. The order of the returned IDs
matches the order of the passed item prototypes.

[comment]: # ({/4682a6ee-4682a6ee})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({a96d5464-9956fee4})
#### Creating an item prototype

Create an item prototype to monitor free disk space on a discovered file
system. Discovered items should be numeric Zabbix agent items updated
every 30 seconds.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "itemprototype.create",
    "params": {
        "name": "Free disk space on {#FSNAME}",
        "key_": "vfs.fs.size[{#FSNAME},free]",
        "hostid": "10197",
        "ruleid": "27665",
        "type": 0,
        "value_type": 3,
        "interfaceid": "112",
        "delay": "30s"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "27666"
        ]
    },
    "id": 1
}
```

[comment]: # ({/a96d5464-9956fee4})

[comment]: # ({b8963302-d22f43ae})
#### Creating an item prototype with preprocessing

Create an item using change per second and a custom multiplier as a
second step.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "itemprototype.create",
    "params": {
        "name": "Incoming network traffic on {#IFNAME}",
        "key_": "net.if.in[{#IFNAME}]",
        "hostid": "10001",
        "ruleid": "27665",
        "type": 0,
        "value_type": 3,
        "delay": "60s",
        "units": "bps",
        "interfaceid": "1155",
        "preprocessing": [
            {
                "type": 10,
                "params": "",
                "error_handler": 0,
                "error_handler_params": ""
            },
            {
                "type": 1,
                "params": "8",
                "error_handler": 2,
                "error_handler_params": "10"
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "44211"
        ]
    },
    "id": 1
}
```

[comment]: # ({/b8963302-d22f43ae})

[comment]: # ({7962580a-76dad6d6})
#### Creating dependent item prototype

Create Dependent item prototype for Master item prototype with ID 44211.
Only dependencies on same host (template/discovery rule) are allowed,
therefore Master and Dependent item should have same hostid and ruleid.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "itemprototype.create",
    "params": {
      "hostid": "10001",
      "ruleid": "27665",
      "name": "Dependent test item prototype",
      "key_": "dependent.prototype",
      "type": 18,
      "master_itemid": "44211",
      "value_type": 3
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "44212"
        ]
    },
    "id": 1
}
```

[comment]: # ({/7962580a-76dad6d6})

[comment]: # ({2f78dc8b-94e7709f})
#### Create HTTP agent item prototype

Create item prototype with URL using user macro, query fields and custom
headers.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "itemprototype.create",
    "params": {
        "type": "19",
        "hostid": "10254",
        "ruleid": "28256",
        "interfaceid": "2",
        "name": "api item prototype example",
        "key_": "api_http_item",
        "value_type": 3,
        "url": "{$URL_PROTOTYPE}",
        "query_fields": [
            {
                "min": "10"
            },
            {
                "max": "100"
            }
        ],
        "headers": {
            "X-Source": "api"
        },
        "delay": "35"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "28305"
        ]
    },
    "id": 1
}
```

[comment]: # ({/2f78dc8b-94e7709f})

[comment]: # ({c3cae877-9e8477fc})
#### Create script item prototype

Create a simple data collection using a script item prototype.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "itemprototype.create",
    "params": {
        "name": "Script example",
        "key_": "custom.script.itemprototype",
        "hostid": "12345",
        "type": 21,
        "value_type": 4,
        "params": "var request = new HttpRequest();\nreturn request.post(\"https://postman-echo.com/post\", JSON.parse(value));",
        "parameters": [
            {
                "name": "host",
                "value": "{HOST.CONN}"
            }
        ],
        "timeout": "6s",
        "delay": "30s"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "23865"
        ]
    },
    "id": 1
}
```

[comment]: # ({/c3cae877-9e8477fc})

[comment]: # ({4f193e2e-4f193e2e})
### Source

CItemPrototype::create() in
*ui/include/classes/api/services/CItemPrototype.php*.

[comment]: # ({/4f193e2e-4f193e2e})
