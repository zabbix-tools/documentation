[comment]: # ({3a290d9c-bd841621})
# High availability node

This class is designed to work with server nodes that are part of a high availability cluster or a standalone server instance.

Object references:

-   [High availability node](/manual/api/reference/hanode/object#high_availability_node)

Available methods:

-   [hanode.get](/manual/api/reference/hanode/get) - retrieve nodes

[comment]: # ({/3a290d9c-bd841621})
