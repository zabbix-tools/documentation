[comment]: # ({24066526-8674a4c3})
# Discovery rule

This class is designed to work with network discovery rules.

::: notetip
This API is meant to work with network discovery rules.
For low-level discovery rules, see [LLD rule API](discoveryrule).
:::

Object references:

-   [Discovery rule](/manual/api/reference/drule/object#discovery_rule)

Available methods:

-   [drule.create](/manual/api/reference/drule/create) - create new network discovery rules
-   [drule.delete](/manual/api/reference/drule/delete) - delete network discovery rules
-   [drule.get](/manual/api/reference/drule/get) - retrieve network discovery rules
-   [drule.update](/manual/api/reference/drule/update) - update network discovery rules

[comment]: # ({/24066526-8674a4c3})
