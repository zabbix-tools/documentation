[comment]: # ({34f25b55-34f25b55})
# apiinfo.version

[comment]: # ({/34f25b55-34f25b55})

[comment]: # ({bc32199a-bc32199a})
### Description

`string apiinfo.version(array)`

This method allows to retrieve the version of the Zabbix API.

::: noteimportant
This method is only available to unauthenticated
users and must be called without the `auth` parameter in the JSON-RPC
request.
:::

[comment]: # ({/bc32199a-bc32199a})

[comment]: # ({4fa8a419-4fa8a419})
### Parameters

`(array)` The method accepts an empty array.

[comment]: # ({/4fa8a419-4fa8a419})

[comment]: # ({53521d11-53521d11})
### Return values

`(string)` Returns the version of the Zabbix API.

::: notetip
Starting from Zabbix 2.0.4 the version of the API
matches the version of Zabbix.
:::

[comment]: # ({/53521d11-53521d11})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({00505c85-6ba4bdd0})
#### Retrieving the version of the API

Retrieve the version of the Zabbix API.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "apiinfo.version",
    "params": [],
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": "6.4.0",
    "id": 1
}
```

[comment]: # ({/00505c85-6ba4bdd0})

[comment]: # ({25499b5e-25499b5e})
### Source

CAPIInfo::version() in *ui/include/classes/api/services/CAPIInfo.php*.

[comment]: # ({/25499b5e-25499b5e})
