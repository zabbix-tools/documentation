[comment]: # ({d216868d-2d53bfd4})
# Trigger prototype

This class is designed to work with trigger prototypes.

Object references:

-   [Trigger prototype](/manual/api/reference/triggerprototype/object#trigger_prototype)
-   [Trigger prototype tag](/manual/api/reference/triggerprototype/object#trigger_prototype_tag)

Available methods:

-   [triggerprototype.create](/manual/api/reference/triggerprototype/create) - create new trigger prototypes
-   [triggerprototype.delete](/manual/api/reference/triggerprototype/delete) - delete trigger prototypes
-   [triggerprototype.get](/manual/api/reference/triggerprototype/get) - retrieve trigger prototypes
-   [triggerprototype.update](/manual/api/reference/triggerprototype/update) - update trigger prototypes

[comment]: # ({/d216868d-2d53bfd4})
