[comment]: # ({be70f0cc-be70f0cc})
# triggerprototype.update

[comment]: # ({/be70f0cc-be70f0cc})

[comment]: # ({0af53e19-842b5955})
### Description

`object triggerprototype.update(object/array triggerPrototypes)`

This method allows to update existing trigger prototypes.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/0af53e19-842b5955})

[comment]: # ({66187b73-827e4920})
### Parameters

`(object/array)` Trigger prototype properties to be updated.

The `triggerid` property must be defined for each trigger prototype, all
other properties are optional. Only the passed properties will be
updated, all others will remain unchanged.

Additionally to the [standard trigger prototype
properties](object#trigger_prototype) the method accepts the following
parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|dependencies|array|Triggers and trigger prototypes that the trigger prototype is dependent on.<br><br>The triggers must have the `triggerid` property defined.|
|tags|array|Trigger prototype [tags](/manual/api/reference/triggerprototype/object#trigger_prototype_tag).|

::: noteimportant
The trigger expression has to be given in its
expanded form and must contain at least one item prototype.
:::

[comment]: # ({/66187b73-827e4920})

[comment]: # ({200d7b9d-200d7b9d})
### Return values

`(object)` Returns an object containing the IDs of the updated trigger
prototypes under the `triggerids` property.

[comment]: # ({/200d7b9d-200d7b9d})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({7b2a44a5-bcca41dc})
#### Enabling a trigger prototype

Enable a trigger prototype, that is, set its status to "0".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "triggerprototype.update",
    "params": {
        "triggerid": "13938",
        "status": 0
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "triggerids": [
            "13938"
        ]
    },
    "id": 1
}
```

[comment]: # ({/7b2a44a5-bcca41dc})

[comment]: # ({708ac736-724af3ce})
#### Replacing trigger prototype tags

Replace tags for one trigger prototype.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "triggerprototype.update",
    "params": {
        "triggerid": "17373",
        "tags": [
            {
                "tag": "volume",
                "value": "{#FSNAME}"
            },
            {
                "tag": "type",
                "value": "{#FSTYPE}"
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "triggerids": [
            "17373"
        ]
    },
    "id": 1
}
```

[comment]: # ({/708ac736-724af3ce})

[comment]: # ({585ae96a-585ae96a})
### Source

CTriggerPrototype::update() in
*ui/include/classes/api/services/CTriggerPrototype.php*.

[comment]: # ({/585ae96a-585ae96a})
