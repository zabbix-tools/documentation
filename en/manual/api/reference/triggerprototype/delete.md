[comment]: # ({c233abea-c233abea})
# triggerprototype.delete

[comment]: # ({/c233abea-c233abea})

[comment]: # ({41f71699-4ed080b7})
### Description

`object triggerprototype.delete(array triggerPrototypeIds)`

This method allows to delete trigger prototypes.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/41f71699-4ed080b7})

[comment]: # ({93be9503-93be9503})
### Parameters

`(array)` IDs of the trigger prototypes to delete.

[comment]: # ({/93be9503-93be9503})

[comment]: # ({91b09722-91b09722})
### Return values

`(object)` Returns an object containing the IDs of the deleted trigger
prototypes under the `triggerids` property.

[comment]: # ({/91b09722-91b09722})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({bb7f3152-ed4ab6a8})
#### Deleting multiple trigger prototypes

Delete two trigger prototypes.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "triggerprototype.delete",
    "params": [
        "12002",
        "12003"
    ],
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "triggerids": [
            "12002",
            "12003"
        ]
    },
    "id": 1
}
```

[comment]: # ({/bb7f3152-ed4ab6a8})

[comment]: # ({c4ac1758-c4ac1758})
### Source

CTriggerPrototype::delete() in
*ui/include/classes/api/services/CTriggerPrototype.php*.

[comment]: # ({/c4ac1758-c4ac1758})
