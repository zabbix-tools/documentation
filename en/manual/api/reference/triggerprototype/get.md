[comment]: # ({c7072ca6-c7072ca6})
# triggerprototype.get

[comment]: # ({/c7072ca6-c7072ca6})

[comment]: # ({1e3ce6d6-12701833})
### Description

`integer/array triggerprototype.get(object parameters)`

The method allows to retrieve trigger prototypes according to the given parameters.

::: noteclassic
This method is available to users of any type. Permissions to call the method can be revoked in user role settings.
See [User roles](/manual/web_interface/frontend_sections/users/user_roles) for more information.
:::

[comment]: # ({/1e3ce6d6-12701833})

[comment]: # ({2cb32ec5-14171778})
### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|active|flag|Return only enabled trigger prototypes that belong to monitored hosts.|
|discoveryids|string/array|Return only trigger prototypes that belong to the given LLD rules.|
|functions|string/array|Return only triggers that use the given functions.<br><br>Refer to the [Supported functions](/manual/appendix/functions) page for a list of supported functions.|
|group|string|Return only trigger prototypes that belong to hosts or templates from the host groups or template groups with the given name.|
|groupids|string/array|Return only trigger prototypes that belong to hosts or templates from the given host groups or template groups.|
|host|string|Return only trigger prototypes that belong to hosts with the given name.|
|hostids|string/array|Return only trigger prototypes that belong to the given hosts.|
|inherited|boolean|If set to `true` return only trigger prototypes inherited from a template.|
|maintenance|boolean|If set to `true` return only enabled trigger prototypes that belong to hosts in maintenance.|
|min\_severity|integer|Return only trigger prototypes with severity greater or equal than the given severity.|
|monitored|flag|Return only enabled trigger prototypes that belong to monitored hosts and contain only enabled items.|
|templated|boolean|If set to `true` return only trigger prototypes that belong to templates.|
|templateids|string/array|Return only trigger prototypes that belong to the given templates.|
|triggerids|string/array|Return only trigger prototypes with the given IDs.|
|expandExpression|flag|Expand functions and macros in the trigger expression.|
|selectDependencies|query|Return trigger prototypes and triggers that the trigger prototype depends on in the `dependencies` property.|
|selectDiscoveryRule|query|Return the [LLD rule](/manual/api/reference/discoveryrule/object) that the trigger prototype belongs to in the `discoveryRule` property.|
|selectFunctions|query|Return functions used in the trigger prototype in the `functions` property.<br><br>The function objects represent the functions used in the trigger expression and has the following properties:<br>`functionid` - `(string)` ID of the function;<br>`itemid` - `(string)` ID of the item used in the function;<br>`function` - `(string)` name of the function;<br>`parameter` - `(string)` parameter passed to the function. Query parameter is replaced by `$` symbol in returned string.|
|selectHostGroups|query|Return the host groups that the trigger prototype belongs to in the [`hostgroups`](/manual/api/reference/hostgroup/object) property.|
|selectHosts|query|Return the hosts that the trigger prototype belongs to in the [`hosts`](/manual/api/reference/host/object) property.|
|selectItems|query|Return items and item prototypes used the trigger prototype in the [`items`](/manual/api/reference/item/object) property.|
|selectTags|query|Return the trigger prototype tags in [`tags`](/manual/api/reference/triggerprototype/object#Trigger_prototype_tag) property.|
|selectTemplateGroups|query|Return the template groups that the trigger prototype belongs to in the [`templategroups`](/manual/api/reference/templategroup/object) property.|
|filter|object|Return only those results that exactly match the given filter.<br><br>Accepts an array, where the keys are property names, and the values are either a single value or an array of values to match against.<br><br>Supports additional filters:<br>`host` - technical name of the host that the trigger prototype belongs to;<br>`hostid` - ID of the host that the trigger prototype belongs to.|
|limitSelects|integer|Limits the number of records returned by subselects.<br><br>Applies to the following subselects:<br>`selectHosts` - results will be sorted by `host`.|
|sortfield|string/array|Sort the result by the given properties.<br><br>Possible values: `triggerid`, `description`, `status`, `priority`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters).|
|editable|boolean|^|
|excludeSearch|boolean|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|
|selectGroups<br>(deprecated)|query|This parameter is deprecated, please use `selectHostGroups` or `selectTemplateGroups` instead.<br>Return the host groups and template groups that the trigger prototype belongs to in the `groups` property.|

[comment]: # ({/2cb32ec5-14171778})

[comment]: # ({07ca11ca-7223bab1})
### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has been used.

[comment]: # ({/07ca11ca-7223bab1})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({582baf6f-81de28c1})
#### Retrieve trigger prototypes from an LLD rule

Retrieve all trigger prototypes and their functions from an LLD rule.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "triggerprototype.get",
    "params": {
        "output": "extend",
        "selectFunctions": "extend",
        "discoveryids": "22450"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": [
        {
            "triggerid": "13272",
            "expression": "{12598}<20",
            "description": "Free inodes is less than 20% on volume {#FSNAME}",
            "url": "",
            "status": "0",
            "value": "0",
            "priority": "2",
            "lastchange": "0",
            "comments": "",
            "error": "",
            "templateid": "0",
            "type": "0",
            "state": "0",
            "flags": "2",
            "recovery_mode": "0",
            "recovery_expression": "",
            "correlation_mode": "0",
            "correlation_tag": "",
            "manual_close": "0",
            "opdata": "",
            "discover": "0",
            "event_name": "",
            "uuid": "6ce467d05e8745409a177799bed34bb3",
            "url_name": "",
            "functions": [
                {
                    "functionid": "12598",
                    "itemid": "22454",
                    "triggerid": "13272",
                    "parameter": "$",
                    "function": "last"
                }
            ]
        },
        {
            "triggerid": "13266",
            "expression": "{13500}<20",
            "description": "Free disk space is less than 20% on volume {#FSNAME}",
            "url": "",
            "status": "0",
            "value": "0",
            "priority": "2",
            "lastchange": "0",
            "comments": "",
            "error": "",
            "templateid": "0",
            "type": "0",
            "state": "0",
            "flags": "2",
            "recovery_mode": "0",
            "recovery_expression": "",
            "correlation_mode": "0",
            "correlation_tag": "",
            "manual_close": "0",
            "opdata": "",
            "discover": "0",
            "event_name": "",
            "uuid": "74a1fc62bfe24b7eabe4e244c70dc384",
            "url_name": "",
            "functions": [
                {
                    "functionid": "13500",
                    "itemid": "22686",
                    "triggerid": "13266",
                    "parameter": "$",
                    "function": "last"
                }
            ]
        }
    ],
    "id": 1
}
```

[comment]: # ({/582baf6f-81de28c1})

[comment]: # ({af95a04d-2cb80f2d})
#### Retrieving a specific trigger prototype with tags

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "triggerprototype.get",
    "params": {
        "output": [
            "triggerid",
            "description"
        ]
        "selectTags": "extend",
        "triggerids": [
            "17373"
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": [
        {
            "triggerid": "17373",
            "description": "Free disk space is less than 20% on volume {#FSNAME}",
            "tags": [
                {
                    "tag": "volume",
                    "value": "{#FSNAME}"
                },
                {
                    "tag": "type",
                    "value": "{#FSTYPE}"
                }
            ]
        }
    ],
    "id": 1
}
```

[comment]: # ({/af95a04d-2cb80f2d})

[comment]: # ({11e314dd-222d46a0})
### See also

-   [Discovery rule](/manual/api/reference/discoveryrule/object#discovery_rule)
-   [Item](/manual/api/reference/item/object#item)
-   [Host](/manual/api/reference/host/object#host)
-   [Host group](/manual/api/reference/hostgroup/object#host_group)
-   [Template group](/manual/api/reference/templategroup/object#template_group)

[comment]: # ({/11e314dd-222d46a0})

[comment]: # ({526ca55d-1136c1c7})
### Source

CTriggerPrototype::get() in *ui/include/classes/api/services/CTriggerPrototype.php*.

[comment]: # ({/526ca55d-1136c1c7})
