[comment]: # ({49f9e765-49f9e765})
# triggerprototype.create

[comment]: # ({/49f9e765-49f9e765})

[comment]: # ({641d8b86-c66bc66b})
### Description

`object triggerprototype.create(object/array triggerPrototypes)`

This method allows to create new trigger prototypes.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/641d8b86-c66bc66b})

[comment]: # ({2a1e778b-e93145a6})
### Parameters

`(object/array)` Trigger prototypes to create.

Additionally to the [standard trigger prototype
properties](object#trigger_prototype) the method accepts the following
parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|dependencies|array|Triggers and trigger prototypes that the trigger prototype is dependent on.<br><br>The triggers must have the `triggerid` property defined.|
|tags|array|Trigger prototype [tags](/manual/api/reference/triggerprototype/object#trigger_prototype_tag).|

::: noteimportant
The trigger expression has to be given in its
expanded form and must contain at least one item prototype.
:::

[comment]: # ({/2a1e778b-e93145a6})

[comment]: # ({8d1f4c4d-8d1f4c4d})
### Return values

`(object)` Returns an object containing the IDs of the created trigger
prototypes under the `triggerids` property. The order of the returned
IDs matches the order of the passed trigger prototypes.

[comment]: # ({/8d1f4c4d-8d1f4c4d})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({b17d7ba1-87a61295})
#### Creating a trigger prototype

Create a trigger prototype to detect when a file system has less than
20% free disk space.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "triggerprototype.create",
    "params": {
        "description": "Free disk space is less than 20% on volume {#FSNAME}",
        "expression": "last(/Zabbix server/vfs.fs.size[{#FSNAME},pfree])<20",
        "tags": [
            {
                "tag": "volume",
                "value": "{#FSNAME}"
            },
            {
                "tag": "type",
                "value": "{#FSTYPE}"
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "triggerids": [
            "17372"
        ]
    },
    "id": 1
}
```

[comment]: # ({/b17d7ba1-87a61295})

[comment]: # ({43cf23db-43cf23db})
### Source

CTriggerPrototype::create() in
*ui/include/classes/api/services/CTriggerPrototype.php*.

[comment]: # ({/43cf23db-43cf23db})
