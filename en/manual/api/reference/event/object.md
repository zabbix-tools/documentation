[comment]: # ({bef38847-bef38847})
# > Event object

The following objects are directly related to the `event` API.

[comment]: # ({/bef38847-bef38847})

[comment]: # ({18fa2100-355941f0})
### Event

::: noteclassic
Events are created by the Zabbix server and cannot be
modified via the API.
:::

The event object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|eventid|string|ID of the event.|
|source|integer|Type of the event.<br><br>Possible values:<br>0 - event created by a trigger;<br>1 - event created by a discovery rule;<br>2 - event created by active agent autoregistration;<br>3 - internal event;<br>4 - event created on service status update.|
|object|integer|Type of object that is related to the event.<br><br>Possible values if `source` is set to "event created by a trigger":<br>0 - trigger.<br><br>Possible values if `source` is set to "event created by a discovery rule":<br>1 - discovered host;<br>2 - discovered service.<br><br>Possible values if `source` is set to "event created by active agent autoregistration":<br>3 - auto-registered host.<br><br>Possible values if `source` is set to "internal event":<br>0 - trigger;<br>4 - item;<br>5 - LLD rule.<br><br>Possible values if `source` is set to "event created on service status update":<br>6 - service.|
|objectid|string|ID of the related object.|
|acknowledged|integer|Whether the event has been acknowledged.|
|clock|timestamp|Time when the event was created.|
|ns|integer|Nanoseconds when the event was created.|
|name|string|Resolved event name.|
|value|integer|State of the related object.<br><br>Possible values if `source` is set to "event created by a trigger" or "event created on service status update":<br>0 - OK;<br>1 - problem.<br><br>Possible values if `source` is set to "event created by a discovery rule":<br>0 - host or service up;<br>1 - host or service down;<br>2 - host or service discovered;<br>3 - host or service lost.<br><br>Possible values if `source` is seto to "internal event":<br>0 - "normal" state;<br>1 - "unknown" or "not supported" state.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `source` is set to "event created by a trigger", "event created by a discovery rule", "internal event", or "event created on service status update"|
|severity|integer|Event current severity.<br><br>Possible values:<br>0 - not classified;<br>1 - information;<br>2 - warning;<br>3 - average;<br>4 - high;<br>5 - disaster.|
|r\_eventid|string|Recovery event ID.|
|c\_eventid|string|ID of the event that was used to override (close) current event under global correlation rule. See `correlationid` to identify exact correlation rule.<br>This parameter is only defined when the event is closed by global correlation rule.|
|cause_eventid|string|Cause event ID.|
|correlationid|string|ID of the correlation rule that generated closing of the problem.<br>This parameter is only defined when the event is closed by global correlation rule.|
|userid|string|User ID if the event was manually closed.|
|suppressed|integer|Whether the event is suppressed.<br><br>Possible values:<br>0 - event is in normal state;<br>1 - event is suppressed.|
|opdata|string|Operational data with expanded macros.|
|urls|array|Active [media type URLs](/manual/api/reference/problem/object#media-type-url).|

[comment]: # ({/18fa2100-355941f0})

[comment]: # ({35cb4fc7-b89991b4})
### Event tag

The event tag object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|tag|string|Event tag name.|
|value|string|Event tag value.|

[comment]: # ({/35cb4fc7-b89991b4})

[comment]: # ({bd0579ef-52ff7df7})
### Media type URL

The media type URL object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|name|string|Media type defined URL name.|
|url|string|Media type defined URL value.|

Results will contain entries only for active media types with enabled event menu entry.
Macro used in properties will be expanded, but if one of the properties contains an unexpanded macro, both properties will be excluded from results.
For supported macros, see [*Supported macros*](/manual/appendix/macros/supported_by_location).

[comment]: # ({/bd0579ef-52ff7df7})
