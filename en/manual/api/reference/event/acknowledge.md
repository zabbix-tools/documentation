[comment]: # ({2ccfd922-2ccfd922})
# event.acknowledge

[comment]: # ({/2ccfd922-2ccfd922})

[comment]: # ({96ece3aa-eed7fdda})
### Description

`object event.acknowledge(object/array parameters)`

This method allows to update events. The following update actions can be
performed:

-   Close event. If event is already resolved, this action will be
    skipped.
-   Acknowledge event. If event is already acknowledged, this action
    will be skipped.
-   Unacknowledge event. If event is not acknowledged, this action will be skipped.
-   Add message.
-   Change event severity. If event already has same severity, this
    action will be skipped.
-   Suppress event. If event is already suppressed, this action will be skipped.
-   Unsuppress event. If event is not suppressed, this action will be skipped.

::: noteimportant
Only trigger events can be updated.\
Only problem events can be updated.\
Read/Write rights for trigger are required to close the event or to
change event's severity.\
To close an event, manual close should be allowed in the
trigger.
:::

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/96ece3aa-eed7fdda})

[comment]: # ({db16a9fb-32560b88})
### Parameters

`(object/array)` Parameters containing the IDs of the events and update
operations that should be performed.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|eventids|string/object|IDs of the events to acknowledge.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required*|
|action|integer|Event update action(s).<br>This is a bitmask field, any combination of possible bitmap values is acceptable.<br><br>Possible bitmap values:<br>1 - close problem;<br>2 - acknowledge event;<br>4 - add message;<br>8 - change severity;<br>16 - unacknowledge event;<br>32 - suppress event;<br>64 - unsuppress event;<br>128 - change event rank to cause;<br>256 - change event rank to symptom.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required*|
|cause_eventid|string|Cause event ID.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required* if `action` contains the "change event rank to symptom" bit|
|message|string|Text of the message.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required* if `action` contains the "add message" bit|
|severity|integer|New severity for events.<br><br>Possible values:<br>0 - not classified;<br>1 - information;<br>2 - warning;<br>3 - average;<br>4 - high;<br>5 - disaster.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required* if `action` contains the "change severity" bit|
|suppress_until|integer|Unix timestamp until which event must be suppressed.<br><br>If set to "0", the suppression will be indefinite.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *required* if `action` contains the "suppress event" bit|

[comment]: # ({/db16a9fb-32560b88})

[comment]: # ({94f3ab84-94f3ab84})
### Return values

`(object)` Returns an object containing the IDs of the updated events
under the `eventids` property.

[comment]: # ({/94f3ab84-94f3ab84})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({2aa7a118-9e923e4c})
#### Acknowledging an event

Acknowledge a single event and leave a message.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "event.acknowledge",
    "params": {
        "eventids": "20427",
        "action": 6,
        "message": "Problem resolved."
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "eventids": [
            "20427"
        ]
    },
    "id": 1
}
```

[comment]: # ({/2aa7a118-9e923e4c})

[comment]: # ({63d8190d-94e25d7e})
#### Changing event's severity

Change severity for multiple events and leave a message.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "event.acknowledge",
    "params": {
        "eventids": ["20427", "20428"],
        "action": 12,
        "message": "Maintenance required to fix it.",
        "severity": 4
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "eventids": [
            "20427",
            "20428"
        ]
    },
    "id": 1
}
```

[comment]: # ({/63d8190d-94e25d7e})

[comment]: # ({aadd99f7-aadd99f7})
### Source

CEvent::acknowledge() in *ui/include/classes/api/services/CEvent.php*.

[comment]: # ({/aadd99f7-aadd99f7})
