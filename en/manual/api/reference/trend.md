[comment]: # ({78244d1c-be32ecd0})
# Trend

This class is designed to work with trend data.

Object references:

-   [Float trend](/manual/api/reference/trend/object#float_trend)
-   [Integer trend](/manual/api/reference/trend/object#integer_trend)

Available methods:

-   [trend.get](/manual/api/reference/trend/get) - retrieve trends

[comment]: # ({/78244d1c-be32ecd0})
