[comment]: # ({d229f871-d229f871})
# image.update

[comment]: # ({/d229f871-d229f871})

[comment]: # ({71734a46-71e9b7b3})
### Description

`object image.update(object/array images)`

This method allows to update existing images.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/71734a46-71e9b7b3})

[comment]: # ({137e0fc6-b1c7183d})
### Parameters

`(object/array)` Image properties to be updated.

The `imageid` property must be defined for each image, all other properties are optional.
Only the passed properties will be updated, all others will remain unchanged.

The method accepts images with the [standard image properties](object#image).

[comment]: # ({/137e0fc6-b1c7183d})

[comment]: # ({a4a768f8-a4a768f8})
### Return values

`(object)` Returns an object containing the IDs of the updated images
under the `imageids` property.

[comment]: # ({/a4a768f8-a4a768f8})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({3929ab3b-4aaa0fb1})
#### Rename image

Rename image to "Cloud icon".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "image.update",
    "params": {
        "imageid": "2",
        "name": "Cloud icon"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "imageids": [
            "2"
        ]
    },
    "id": 1
}
```

[comment]: # ({/3929ab3b-4aaa0fb1})

[comment]: # ({5590c086-5590c086})
### Source

CImage::update() in *ui/include/classes/api/services/CImage.php*.

[comment]: # ({/5590c086-5590c086})
