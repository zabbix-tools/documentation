[comment]: # ({e9ddc4ef-e9ddc4ef})
# > Image object

The following objects are directly related to the `image` API.

[comment]: # ({/e9ddc4ef-e9ddc4ef})

[comment]: # ({c4a62ce2-eee50efa})
### Image

The image object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|imageid|string|ID of the image.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*<br>- *required* for update operations|
|name|string|Name of the image.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* for create operations|
|imagetype|integer|Type of image.<br><br>Possible values:<br>1 - *(default)* icon;<br>2 - background image.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *constant*<br>- *required* for create operations|
|image|string|Base64 encoded image.<br>The maximum size of the encoded image is 1 MB. Maximum size can be adjusted by changing `ZBX_MAX_IMAGE_SIZE` constant value.<br>Supported image formats: PNG, JPEG, GIF.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* for create operations|

[comment]: # ({/c4a62ce2-eee50efa})
