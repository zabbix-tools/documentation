[comment]: # ({46b153da-46b153da})
# image.delete

[comment]: # ({/46b153da-46b153da})

[comment]: # ({0a94a9d5-60f587fa})
### Description

`object image.delete(array imageIds)`

This method allows to delete images.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/0a94a9d5-60f587fa})

[comment]: # ({c7ef61f7-c7ef61f7})
### Parameters

`(array)` IDs of the images to delete.

[comment]: # ({/c7ef61f7-c7ef61f7})

[comment]: # ({4b3e57f6-4b3e57f6})
### Return values

`(object)` Returns an object containing the IDs of the deleted images
under the `imageids` property.

[comment]: # ({/4b3e57f6-4b3e57f6})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({caf12312-925dee02})
#### Delete multiple images

Delete two images.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "image.delete",
    "params": [
        "188",
        "192"
    ],
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "imageids": [
            "188",
            "192"
        ]
    },
    "id": 1
}
```

[comment]: # ({/caf12312-925dee02})

[comment]: # ({dfdead8e-dfdead8e})
### Source

CImage::delete() in *ui/include/classes/api/services/CImage.php*.

[comment]: # ({/dfdead8e-dfdead8e})
