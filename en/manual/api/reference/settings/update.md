[comment]: # ({acca5f57-acca5f57})
# settings.update

[comment]: # ({/acca5f57-acca5f57})

[comment]: # ({6375071d-9252b567})
### Description

`object settings.update(object settings)`

This method allows to update existing common settings.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/6375071d-9252b567})

[comment]: # ({0782d4ac-7e621fea})
### Parameters

`(object)` [Settings properties](object#settings) to be updated.

[comment]: # ({/0782d4ac-7e621fea})

[comment]: # ({f63e579c-440363c6})
### Return values

`(array)` Returns an array with the names of updated parameters.

[comment]: # ({/f63e579c-440363c6})

[comment]: # ({475c98ad-c59f767c})
### Examples

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "settings.update",
    "params": {
        "login_attempts": "1",
        "login_block": "1m"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": [
        "login_attempts",
        "login_block"
    ],
    "id": 1
}
```

[comment]: # ({/475c98ad-c59f767c})

[comment]: # ({e23f9161-e23f9161})
### Source

CSettings::update() in *ui/include/classes/api/services/CSettings.php*.

[comment]: # ({/e23f9161-e23f9161})
