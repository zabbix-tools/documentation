[comment]: # ({e0668305-e0668305})
# settings.get

[comment]: # ({/e0668305-e0668305})

[comment]: # ({392c0fed-28fc5821})
### Description

`object settings.get(object parameters)`

The method allows to retrieve settings object according to the given
parameters.

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/392c0fed-28fc5821})

[comment]: # ({c43c1377-24b20f8d})
### Parameters

`(object)` Parameters defining the desired output.

The method supports only one parameter.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|output|query|This parameter being common for all `get` methods described in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters).|

[comment]: # ({/c43c1377-24b20f8d})

[comment]: # ({774b33bb-774b33bb})
### Return values

`(object)` Returns settings object.

[comment]: # ({/774b33bb-774b33bb})

[comment]: # ({a4071316-de19d71c})
### Examples

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "settings.get",
    "params": {
        "output": "extend"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "default_theme": "blue-theme",
        "search_limit": "1000",
        "max_in_table": "50",
        "server_check_interval": "10",
        "work_period": "1-5,09:00-18:00",
        "show_technical_errors": "0",
        "history_period": "24h",
        "period_default": "1h",
        "max_period": "2y",
        "severity_color_0": "97AAB3",
        "severity_color_1": "7499FF",
        "severity_color_2": "FFC859",
        "severity_color_3": "FFA059",
        "severity_color_4": "E97659",
        "severity_color_5": "E45959",
        "severity_name_0": "Not classified",
        "severity_name_1": "Information",
        "severity_name_2": "Warning",
        "severity_name_3": "Average",
        "severity_name_4": "High",
        "severity_name_5": "Disaster",
        "custom_color": "0",
        "ok_period": "5m",
        "blink_period": "2m",
        "problem_unack_color": "CC0000",
        "problem_ack_color": "CC0000",
        "ok_unack_color": "009900",
        "ok_ack_color": "009900",
        "problem_unack_style": "1",
        "problem_ack_style": "1",
        "ok_unack_style": "1",
        "ok_ack_style": "1",
        "discovery_groupid": "5",
        "default_inventory_mode": "-1",
        "alert_usrgrpid": "7",
        "snmptrap_logging": "1",
        "default_lang": "en_GB",
        "default_timezone": "system",
        "login_attempts": "5",
        "login_block": "30s",
        "validate_uri_schemes": "1",
        "uri_valid_schemes": "http,https,ftp,file,mailto,tel,ssh",
        "x_frame_options": "SAMEORIGIN",
        "iframe_sandboxing_enabled": "1",
        "iframe_sandboxing_exceptions": "",
        "max_overview_table_size": "50",
        "connect_timeout": "3s",
        "socket_timeout": "3s",
        "media_type_test_timeout": "65s",
        "script_timeout": "60s",
        "item_test_timeout": "60s",
        "url": "",
        "report_test_timeout": "60s",
        "auditlog_enabled": "1",
        "ha_failover_delay": "1m",
        "geomaps_tile_provider": "OpenStreetMap.Mapnik",
        "geomaps_tile_url": "",
        "geomaps_max_zoom": "0",
        "geomaps_attribution": "",
        "vault_provider": "0"
    },
    "id": 1
}
```

[comment]: # ({/a4071316-de19d71c})

[comment]: # ({7f5d54e5-7f5d54e5})
### Source

CSettings::get() in *ui/include/classes/api/services/CSettings.php*.

[comment]: # ({/7f5d54e5-7f5d54e5})
