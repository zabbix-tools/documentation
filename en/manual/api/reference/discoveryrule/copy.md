[comment]: # ({04928fa3-04928fa3})
# discoveryrule.copy

[comment]: # ({/04928fa3-04928fa3})

[comment]: # ({67be7f89-8ae93b5b})
### Description

`object discoveryrule.copy(object parameters)`

This method allows to copy LLD rules with all of the prototypes to the
given hosts.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/67be7f89-8ae93b5b})

[comment]: # ({14e9b8f3-47295f20})
### Parameters

`(object)` Parameters defining the LLD rules to copy and the target
hosts.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|discoveryids|array|IDs of the LLD rules to be copied.|
|hostids|array|IDs of the hosts to copy the LLD rules to.|

[comment]: # ({/14e9b8f3-47295f20})

[comment]: # ({b8760eee-b8760eee})
### Return values

`(boolean)` Returns `true` if the copying was successful.

[comment]: # ({/b8760eee-b8760eee})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({4bc5adc2-91be874e})
#### Copy an LLD rule to multiple hosts

Copy an LLD rule to two hosts.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "discoveryrule.copy",
    "params": {
        "discoveryids": [
            "27426"
        ],
        "hostids": [
            "10196",
            "10197"
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": true,
    "id": 1
}
```

[comment]: # ({/4bc5adc2-91be874e})

[comment]: # ({8870f994-8870f994})
### Source

CDiscoveryrule::copy() in
*ui/include/classes/api/services/CDiscoveryRule.php*.

[comment]: # ({/8870f994-8870f994})
