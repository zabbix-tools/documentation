[comment]: # ({d756b50a-d756b50a})
# discoveryrule.update

[comment]: # ({/d756b50a-d756b50a})

[comment]: # ({5ceb4e4b-0d23e0d8})
### Description

`object discoveryrule.update(object/array lldRules)`

This method allows to update existing LLD rules.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/5ceb4e4b-0d23e0d8})

[comment]: # ({2a9a216a-cf98658c})
### Parameters

`(object/array)` LLD rule properties to be updated.

The `itemid` property must be defined for each LLD rule, all other
properties are optional. Only the passed properties will be updated, all
others will remain unchanged.

Additionally to the [standard LLD rule properties](object#lld_rule), the
method accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|filter|object|LLD rule [filter](/manual/api/reference/discoveryrule/object#lld_rule_filter) object to replace the current filter.|
|preprocessing|array|LLD rule [preprocessing](/manual/api/reference/discoveryrule/object#lld_rule_preprocessing) options to replace the current preprocessing options.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *read-only* for inherited objects|
|lld\_macro\_paths|array|LLD rule [lld\_macro\_path](/manual/api/reference/discoveryrule/object#lld_macro_path) options.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *read-only* for inherited objects|
|overrides|array|LLD rule [overrides](/manual/api/reference/discoveryrule/object#lld_rule_overrides) options.<br><br>[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):<br>- *read-only* for inherited objects|

[comment]: # ({/2a9a216a-cf98658c})

[comment]: # ({6f962e26-6f962e26})
### Return values

`(object)` Returns an object containing the IDs of the updated LLD rules
under the `itemids` property.

[comment]: # ({/6f962e26-6f962e26})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({e13e1807-67c9278f})
#### Adding a filter to an LLD rule

Add a filter so that the contents of the *{\#FSTYPE}* macro would match
the *\@File systems for discovery* regexp.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "discoveryrule.update",
    "params": {
        "itemid": "22450",
        "filter": {
            "evaltype": 1,
            "conditions": [
                {
                    "macro": "{#FSTYPE}",
                    "value": "@File systems for discovery"
                }
            ]
        }
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "22450"
        ]
    },
    "id": 1
}
```

[comment]: # ({/e13e1807-67c9278f})

[comment]: # ({d95f4004-f3fd4d6c})
#### Adding LLD macro paths

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "discoveryrule.update",
    "params": {
        "itemid": "22450",
        "lld_macro_paths": [
            {
                "lld_macro": "{#MACRO1}",
                "path": "$.json.path"
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "22450"
        ]
    },
    "id": 1
}
```

[comment]: # ({/d95f4004-f3fd4d6c})

[comment]: # ({1e8f3244-8d8cc019})
#### Disable trapping

Disable LLD trapping for discovery rule.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "discoveryrule.update",
    "params": {
        "itemid": "28336",
        "allow_traps": 0
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "28336"
        ]
    },
    "id": 1
}
```

[comment]: # ({/1e8f3244-8d8cc019})

[comment]: # ({10cc7a6c-bef0a5f5})
#### Updating LLD rule preprocessing options

Update an LLD rule with preprocessing rule “JSONPath”.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "discoveryrule.update",
    "params": {
        "itemid": "44211",
        "preprocessing": [
            {
                "type": 12,
                "params": "$.path.to.json",
                "error_handler": 2,
                "error_handler_params": "5"
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "44211"
        ]
    },
    "id": 1
}
```

[comment]: # ({/10cc7a6c-bef0a5f5})

[comment]: # ({5e692d62-3f18a1c3})
#### Updating LLD rule script

Update an LLD rule script with a different script and remove unnecessary
parameters that were used by previous script.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "discoveryrule.update",
    "params": {
        "itemid": "23865",
        "parameters": [],
        "script": "Zabbix.log(3, 'Log test');\nreturn 1;"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "23865"
        ]
    },
    "id": 1
}
```

[comment]: # ({/5e692d62-3f18a1c3})

[comment]: # ({6a73b645-6a73b645})
### Source

CDiscoveryRule::update() in
*ui/include/classes/api/services/CDiscoveryRule.php*.

[comment]: # ({/6a73b645-6a73b645})
