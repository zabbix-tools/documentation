[comment]: # ({c5a9e121-c5a9e121})
# > LLD rule object

The following objects are directly related to the `discoveryrule` API.

[comment]: # ({/c5a9e121-c5a9e121})

[comment]: # ({5c4c0cd4-c17fa830})
### LLD rule

The low-level discovery rule object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|itemid|string|ID of the LLD rule.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*<br>- *required* for update operations|
|delay|string|Update interval of the LLD rule. Accepts seconds or time unit with suffix and with or without one or more [custom intervals](/manual/config/items/item/custom_intervals) that consist of either flexible intervals and scheduling intervals as serialized strings. Also accepts user macros. Flexible intervals could be written as two macros separated by a forward slash. Intervals are separated by a semicolon.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `type` is set to "Zabbix agent" (0), "Simple check" (3), "Zabbix internal" (5), "External check" (10), "Database monitor" (11), "IPMI agent" (12), "SSH agent" (13), "TELNET agent" (14), "JMX agent" (16), "HTTP agent" (19), "SNMP agent" (20), "Script" (21), or if `type` is set to "Zabbix agent (active)" (7) and `key_` does not contain "mqtt.get"|
|hostid|string|ID of the host that the LLD rule belongs to.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *constant*<br>- *required* for create operations|
|interfaceid|string|ID of the LLD rule's host interface.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if LLD rule belongs to host and `type` is set to "Zabbix agent", "IPMI agent", "JMX agent", or "SNMP agent"<br>- *supported* if LLD rule belongs to host and `type` is set to "Simple check", "External check", "SSH agent", "TELNET agent", or "HTTP agent"|
|key\_|string|LLD rule key.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* for create operations<br>- *read-only* for inherited objects|
|name|string|Name of the LLD rule.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* for create operations<br>- *read-only* for inherited objects|
|type|integer|Type of the LLD rule.<br><br>Possible values:<br>0 - Zabbix agent;<br>2 - Zabbix trapper;<br>3 - Simple check;<br>5 - Zabbix internal;<br>7 - Zabbix agent (active);<br>10 - External check;<br>11 - Database monitor;<br>12 - IPMI agent;<br>13 - SSH agent;<br>14 - TELNET agent;<br>16 - JMX agent;<br>18 - Dependent item;<br>19 - HTTP agent;<br>20 - SNMP agent;<br>21 - Script.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* for create operations<br>- *read-only* for inherited objects|
|url|string|URL string.<br>Supports user macros, {HOST.IP}, {HOST.CONN}, {HOST.DNS}, {HOST.HOST}, {HOST.NAME}, {ITEM.ID}, {ITEM.KEY}.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `type` is set to "HTTP agent"<br>- *read-only* for inherited objects|
|allow\_traps|integer|Allow to populate value similarly to the trapper item.<br><br>Possible values:<br>0 - *(default)* Do not allow to accept incoming data;<br>1 - Allow to accept incoming data.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `type` is set to "HTTP agent"|
|authtype|integer|Authentication method.<br><br>Possible values if `type` is set to "SSH agent":<br>0 - *(default)* password;<br>1 - public key.<br><br>Possible values if `type` is set to "HTTP agent":<br>0 - *(default)* none;<br>1 - basic;<br>2 - NTLM.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `type` is set to "SSH agent" or "HTTP agent"<br>- *read-only* for inherited objects (if `type` is set to "HTTP agent")|
|description|string|Description of the LLD rule.|
|error|string|Error text if there are problems updating the LLD rule.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*|
|follow\_redirects|integer|Follow response redirects while pooling data.<br><br>Possible values:<br>0 - Do not follow redirects;<br>1 - *(default)* Follow redirects.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `type` is set to "HTTP agent"<br>- *read-only* for inherited objects|
|headers|object|Object with HTTP(S) request headers, where header name is used as key and header value as value.<br><br>Example: { "User-Agent": "Zabbix" }<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `type` is set to "HTTP agent"<br>- *read-only* for inherited objects|
|http\_proxy|string|HTTP(S) proxy connection string.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `type` is set to "HTTP agent"<br>- *read-only* for inherited objects|
|ipmi\_sensor|string|IPMI sensor.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `type` is set to "IPMI agent" and `key_` is not set to "ipmi.get"<br>- *supported* if `type` is set to "IPMI agent"<br>- *read-only* for inherited objects|
|jmx\_endpoint|string|JMX agent custom connection string.<br><br>Default: service:jmx:rmi:///jndi/rmi://{HOST.CONN}:{HOST.PORT}/jmxrmi<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `type` is set to "JMX agent"|
|lifetime|string|Time period after which items that are no longer discovered will be deleted. Accepts seconds, time unit with suffix, or a user macro.<br><br>Default: `30d`.|
|master\_itemid|integer|Master item ID.<br>Recursion up to 3 dependent items and maximum count of dependent items equal to 999 are allowed.<br>Discovery rule cannot be master item for another discovery rule.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `type` is set to "Dependent item"<br>- *read-only* for inherited objects|
|output\_format|integer|Should the response be converted to JSON.<br><br>Possible values:<br>0 - *(default)* Store raw;<br>1 - Convert to JSON.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `type` is set to "HTTP agent"<br>- *read-only* for inherited objects|
|params|string|Additional parameters depending on the type of the LLD rule:<br>- executed script for SSH and Telnet LLD rules;<br>- SQL query for database monitor LLD rules;<br>- formula for calculated LLD rules.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `type` is set to "Database monitor", "SSH agent", "TELNET agent", or "Script"<br>- *read-only* for inherited objects (if `type` is set to "Script")|
|parameters|array|Additional parameters if `type` is set to "Script".<br>Array of objects with `name` and `value` properties, where `name` must be unique.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `type` is set to "Script"<br>- *read-only* for inherited objects|
|password|string|Password for authentication.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `type` is set to "JMX agent" and `username` is set<br>- *supported* if `type` is set to "Simple check", "Database monitor", "SSH agent", "TELNET agent", or "HTTP agent"<br>- *read-only* for inherited objects (if `type` is set to "HTTP agent")|
|post\_type|integer|Type of post data body stored in `posts` property.<br><br>Possible values:<br>0 - *(default)* Raw data;<br>2 - JSON data;<br>3 - XML data.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `type` is set to "HTTP agent"<br>- *read-only* for inherited objects|
|posts|string|HTTP(S) request body data.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `type` is set to "HTTP agent" and `post_type` is set to "JSON data" or "XML data"<br>- *supported* if `type` is set to "HTTP agent" and `post_type` is set to "Raw data"<br>- *read-only* for inherited objects|
|privatekey|string|Name of the private key file.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `type` is set to "SSH agent" and `authtype` is set to "public key"|
|publickey|string|Name of the public key file.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `type` is set to "SSH agent" and `authtype` is set to "public key"|
|query\_fields|array|Query parameters.<br>Array of objects with `key`:`value` pairs, where `value` can be an empty string.|
|request\_method|integer|Type of request method.<br><br>Possible values:<br>0 - *(default)* GET;<br>1 - POST;<br>2 - PUT;<br>3 - HEAD.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `type` is set to "HTTP agent"<br>- *read-only* for inherited objects|
|retrieve\_mode|integer|What part of response should be stored.<br><br>Possible values if `request_method` is set to "GET", "POST", or "PUT":<br>0 - *(default)* Body;<br>1 - Headers;<br>2 - Both body and headers will be stored.<br><br>Possible values if `request_method` is set to "HEAD":<br>1 - Headers.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `type` is set to "HTTP agent"<br>- *read-only* for inherited objects|
|snmp\_oid|string|SNMP OID.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `type` is set to "SNMP agent"<br>- *read-only* for inherited objects|
|ssl\_cert\_file|string|Public SSL Key file path.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `type` is set to "HTTP agent"<br>- *read-only* for inherited objects|
|ssl\_key\_file|string|Private SSL Key file path.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `type` is set to "HTTP agent"<br>- *read-only* for inherited objects|
|ssl\_key\_password|string|Password for SSL Key file.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `type` is set to "HTTP agent"<br>- *read-only* for inherited objects|
|state|integer|State of the LLD rule.<br><br>Possible values:<br>0 - *(default)* normal;<br>1 - not supported.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*|
|status|integer|Status of the LLD rule.<br><br>Possible values:<br>0 - *(default)* enabled LLD rule;<br>1 - disabled LLD rule.|
|status\_codes|string|Ranges of required HTTP status codes, separated by commas. Also supports user macros as part of comma separated list.<br><br>Example: 200,200-{$M},{$M},200-400<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `type` is set to "HTTP agent"<br>- *read-only* for inherited objects|
|templateid|string|ID of the parent template LLD rule.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*|
|timeout|string|Item data polling request timeout. Supports user macros.<br><br>Default: `3s`.<br>Maximum value: `60s`.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `type` is set to "HTTP agent" or "Script"<br>- *read-only* for inherited objects|
|trapper\_hosts|string|Allowed hosts.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `type` is set to "Zabbix trapper", or if `type` is set to "HTTP agent" and `allow_traps` is set to "Allow to accept incoming data"|
|username|string|Username for authentication.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `type` is set to "SSH agent", "TELNET agent", or if `type` is set to "JMX agent" and `password` is set<br>- *supported* if `type` is set to "Simple check", "Database monitor", or "HTTP agent"<br>- *read-only* for inherited objects (if `type` is set to "HTTP agent")|
|uuid|string|Universal unique identifier, used for linking imported LLD rules to already existing ones. Auto-generated, if not given.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if the LLD rule belongs to a template|
|verify\_host|integer|Validate host name in URL is in Common Name field or a Subject Alternate Name field of host certificate.<br><br>Possible values:<br>0 - *(default)* Do not validate;<br>1 - Validate.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `type` is set to "HTTP agent"<br>- *read-only* for inherited objects|
|verify\_peer|integer|Validate is host certificate authentic.<br><br>Possible values:<br>0 - *(default)* Do not validate;<br>1 - Validate.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *supported* if `type` is set to "HTTP agent"<br>- *read-only* for inherited objects|

[comment]: # ({/5c4c0cd4-c17fa830})

[comment]: # ({12f245fa-b0412737})
### LLD rule filter

The LLD rule filter object defines a set of conditions that can be used
to filter discovered objects. It has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|conditions|array|Set of filter conditions to use for filtering results.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|evaltype|integer|Filter condition evaluation method.<br><br>Possible values:<br>0 - and/or;<br>1 - and;<br>2 - or;<br>3 - custom expression.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|eval\_formula|string|Generated expression that will be used for evaluating filter conditions. The expression contains IDs that reference specific filter conditions by its `formulaid`. The value of `eval_formula` is equal to the value of `formula` for filters with a custom expression.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*|
|formula|string|User-defined expression to be used for evaluating conditions of filters with a custom expression. The expression must contain IDs that reference specific filter conditions by its `formulaid`. The IDs used in the expression must exactly match the ones defined in the filter conditions: no condition can remain unused or omitted.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `evaltype` is set to "custom expression"|

[comment]: # ({/12f245fa-b0412737})

[comment]: # ({e9e2f6b9-be83ef5c})
#### LLD rule filter condition

The LLD rule filter condition object defines a separate check to perform
on the value of an LLD macro. It has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|macro|string|LLD macro to perform the check on.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|value|string|Value to compare with.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|formulaid|string|Arbitrary unique ID that is used to reference the condition from a custom expression. Can only contain capital-case letters. The ID must be defined by the user when modifying filter conditions, but will be generated anew when requesting them afterward.|
|operator|integer|Condition operator.<br><br>Possible values:<br>8 - *(default)* matches regular expression;<br>9 - does not match regular expression;<br>12 - exists;<br>13 - does not exist.|

::: notetip
To better understand how to use filters with various
types of expressions, see examples on the
[discoveryrule.get](get#retrieving_filter_conditions) and
[discoveryrule.create](create#using_a_custom_expression_filter) method
pages.
:::

[comment]: # ({/e9e2f6b9-be83ef5c})

[comment]: # ({4694ea00-4a2b52b9})
### LLD macro path

The LLD macro path has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|lld\_macro|string|LLD macro.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|path|string|Selector for value which will be assigned to corresponding macro.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|

[comment]: # ({/4694ea00-4a2b52b9})

[comment]: # ({3e016a5f-14dc55c1})
### LLD rule preprocessing

The LLD rule preprocessing object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|type|integer|The preprocessing option type.<br><br>Possible values:<br>5 - Regular expression matching;<br>11 - XML XPath;<br>12 - JSONPath;<br>15 - Does not match regular expression;<br>16 - Check for error in JSON;<br>17 - Check for error in XML;<br>20 - Discard unchanged with heartbeat;<br>23 - Prometheus to JSON;<br>24 - CSV to JSON;<br>25 - Replace;<br>27 - XML to JSON;<br>28 - SNMP walk value;<br>29 - SNMP walk to JSON.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|params|string|Additional parameters used by preprocessing option. Multiple parameters are separated by the newline (\\n) character.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `type` is set to "Regular expression matching" (5), "XML XPath" (11), "JSONPath" (12), "Does not match regular expression" (15), "Check for error in JSON" (16), "Check for error in XML" (17), "Discard unchanged with heartbeat" (20), "Prometheus to JSON" (23), "CSV to JSON" (24), "Replace" (25), "SNMP walk value" (28), or "SNMP walk to JSON" (29)|
|error\_handler|integer|Action type used in case of preprocessing step failure.<br><br>Possible values:<br>0 - Error message is set by Zabbix server;<br>1 - Discard value;<br>2 - Set custom value;<br>3 - Set custom error message.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `type` is set to "Regular expression matching" (5), "XML XPath" (11), "JSONPath" (12), "Does not match regular expression" (15), "Check for error in JSON" (16), "Check for error in XML" (17), "Prometheus to JSON" (23), "CSV to JSON" (24), "XML to JSON" (27), "SNMP walk value" (28), or "SNMP walk to JSON" (29)|
|error\_handler\_params|string|Error handler parameters.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `error_handler` is set to "Set custom value" or "Set custom error message"|

The following parameters and error handlers are supported for each preprocessing type.

|Preprocessing type|Name|Parameter 1|Parameter 2|Parameter 3|Supported error handlers|
|------------------|----|-----------|-----------|-----------|------------------------|
|5|Regular expression|pattern^1^|output^2^|<|0, 1, 2, 3|
|11|XML XPath|path^3^|<|<|0, 1, 2, 3|
|12|JSONPath|path^3^|<|<|0, 1, 2, 3|
|15|Does not match regular expression|pattern^1^|<|<|0, 1, 2, 3|
|16|Check for error in JSON|path^3^|<|<|0, 1, 2, 3|
|17|Check for error in XML|path^3^|<|<|0, 1, 2, 3|
|20|Discard unchanged with heartbeat|seconds^4,\ 5,\ 6^|<|<|<|
|23|Prometheus to JSON|pattern^5,\ 7^|<|<|0, 1, 2, 3|
|24|CSV to JSON|character^2^|character^2^|0,1|0, 1, 2, 3|
|25|Replace|search string^2^|replacement^2^|<|<|
|27|XML to JSON|<|<|<|0, 1, 2, 3|
|28|SNMP walk value|OID^2^|Format:<br>0 - Unchanged<br>1 - UTF-8 from Hex-STRING<br>2 - MAC from Hex-STRING<br>3 - Integer from BITS|<|0, 1, 2, 3|
|29|SNMP walk to JSON^9^|Field name^2^|OID prefix^2^|Format:<br>0 - Unchanged<br>1 - UTF-8 from Hex-STRING<br>2 - MAC from Hex-STRING<br>3 - Integer from BITS|0, 1, 2, 3|

^1^ regular expression\
^2^ string\
^3^ JSONPath or XML XPath\
^4^ positive integer (with support of time suffixes, e.g. 30s, 1m, 2h,
1d)\
^5^ user macro\
^6^ LLD macro\
^7^ Prometheus pattern following the syntax:
`<metric name>{<label name>="<label value>", ...} == <value>`. Each
Prometheus pattern component (metric, label name, label value and metric
value) can be user macro.\
^8^ Prometheus output following the syntax: `<label name>`.
^9^ Supports multiple "Field name,OID prefix,Format records" records delimited by a new line character.

[comment]: # ({/3e016a5f-14dc55c1})

[comment]: # ({0d11ffcb-2f32e5f0})
### LLD rule overrides

The LLD rule overrides object defines a set of rules (filters,
conditions and operations) that are used to override properties of
different prototype objects. It has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|name|string|Unique override name.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|step|integer|Unique order number of the override.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|stop|integer|Stop processing next overrides if matches.<br><br>Possible values:<br>0 - *(default)* don't stop processing overrides;<br>1 - stop processing overrides if filter matches.|
|filter|object|Override filter.|
|operations|array|Override operations.|

[comment]: # ({/0d11ffcb-2f32e5f0})

[comment]: # ({67e417ce-573b52c4})
#### LLD rule override filter

The LLD rule override filter object defines a set of conditions that if
they match the discovered object the override is applied. It has the
following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|evaltype|integer|Override filter condition evaluation method.<br><br>Possible values:<br>0 - and/or;<br>1 - and;<br>2 - or;<br>3 - custom expression.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|conditions|array|Set of override filter conditions to use for matching the discovered objects.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|eval\_formula|string|Generated expression that will be used for evaluating override filter conditions. The expression contains IDs that reference specific override filter conditions by its `formulaid`. The value of `eval_formula` is equal to the value of `formula` for filters with a custom expression.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*|
|formula|string|User-defined expression to be used for evaluating conditions of override filters with a custom expression. The expression must contain IDs that reference specific override filter conditions by its `formulaid`. The IDs used in the expression must exactly match the ones defined in the override filter conditions: no condition can remain unused or omitted.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* if `evaltype` is set to "custom expression"|

[comment]: # ({/67e417ce-573b52c4})

[comment]: # ({faa12403-e47f6c6a})
#### LLD rule override filter condition

The LLD rule override filter condition object defines a separate check
to perform on the value of an LLD macro. It has the following
properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|macro|string|LLD macro to perform the check on.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|value|string|Value to compare with.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|formulaid|string|Arbitrary unique ID that is used to reference the condition from a custom expression. Can only contain capital-case letters. The ID must be defined by the user when modifying filter conditions, but will be generated anew when requesting them afterward.|
|operator|integer|Condition operator.<br><br>Possible values:<br>8 - *(default)* matches regular expression;<br>9 - does not match regular expression;<br>12 - exists;<br>13 - does not exist.|

[comment]: # ({/faa12403-e47f6c6a})

[comment]: # ({dd711737-e7b08f3c})
#### LLD rule override operation

The LLD rule override operation is combination of conditions and actions
to perform on the prototype object. It has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|operationobject|integer|Type of discovered object to perform the action.<br><br>Possible values:<br>0 - Item prototype;<br>1 - Trigger prototype;<br>2 - Graph prototype;<br>3 - Host prototype.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|operator|integer|Override condition operator.<br><br>Possible values:<br>0 - *(default)* equals;<br>1 - does not equal;<br>2 - contains;<br>3 - does not contain;<br>8 - matches;<br>9 - does not match.|
|value|string|Pattern to match item, trigger, graph or host prototype name depending on selected object.|
|opstatus|object|Override operation status object for item, trigger and host prototype objects.|
|opdiscover|object|Override operation discover status object (all object types).|
|opperiod|object|Override operation period (update interval) object for item prototype object.|
|ophistory|object|Override operation history object for item prototype object.|
|optrends|object|Override operation trends object for item prototype object.|
|opseverity|object|Override operation severity object for trigger prototype object.|
|optag|array|Override operation tag object for trigger and host prototype objects.|
|optemplate|array|Override operation template object for host prototype object.|
|opinventory|object|Override operation inventory object for host prototype object.|

[comment]: # ({/dd711737-e7b08f3c})

[comment]: # ({9eeccad9-52d3b579})
##### LLD rule override operation status

LLD rule override operation status that is set to discovered object. It
has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|status|integer|Override the status for selected object.<br><br>Possible values:<br>0 - Create enabled;<br>1 - Create disabled.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|

[comment]: # ({/9eeccad9-52d3b579})

[comment]: # ({0d82585d-ff620350})
##### LLD rule override operation discover

LLD rule override operation discover status that is set to discovered
object. It has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|discover|integer|Override the discover status for selected object.<br><br>Possible values:<br>0 - Yes, continue discovering the objects;<br>1 - No, new objects will not be discovered and existing ones will be marked as lost.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|

[comment]: # ({/0d82585d-ff620350})

[comment]: # ({595d53cf-6e469c1b})
##### LLD rule override operation period

LLD rule override operation period is an update interval value (supports
custom intervals) that is set to discovered item. It has the following
properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|delay|string|Override the update interval of the item prototype. Accepts seconds or a time unit with suffix (30s,1m,2h,1d) as well as flexible and scheduling intervals and user macros or LLD macros. Multiple intervals are separated by a semicolon.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|

[comment]: # ({/595d53cf-6e469c1b})

[comment]: # ({0542f612-ff97489f})
##### LLD rule override operation history

LLD rule override operation history value that is set to discovered
item. It has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|history|string|Override the history of item prototype which is a time unit of how long the history data should be stored. Also accepts user macro and LLD macro.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|

[comment]: # ({/0542f612-ff97489f})

[comment]: # ({88a906e8-10b52601})
##### LLD rule override operation trends

LLD rule override operation trends value that is set to discovered item.
It has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|trends|string|Override the trends of item prototype which is a time unit of how long the trends data should be stored. Also accepts user macro and LLD macro.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|

[comment]: # ({/88a906e8-10b52601})

[comment]: # ({e1702ec2-991f3bdc})
##### LLD rule override operation severity

LLD rule override operation severity value that is set to discovered
trigger. It has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|severity|integer|Override the severity of trigger prototype.<br><br>Possible values:<br>0 - *(default)* not classified;<br>1 - information;<br>2 - warning;<br>3 - average;<br>4 - high;<br>5 - disaster.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|

[comment]: # ({/e1702ec2-991f3bdc})

[comment]: # ({be6b5a26-0693b8f3})
##### LLD rule override operation tag

LLD rule override operation tag object contains tag name and value that
are set to discovered object. It has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|tag|string|New tag name.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|
|value|string|New tag value.|

[comment]: # ({/be6b5a26-0693b8f3})

[comment]: # ({43c84d93-5eebc05a})
##### LLD rule override operation template

LLD rule override operation template object that is linked to discovered
host. It has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|templateid|string|Override the template of host prototype linked templates.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|

[comment]: # ({/43c84d93-5eebc05a})

[comment]: # ({3fac4fcc-9c5cc3d1})
##### LLD rule override operation inventory

LLD rule override operation inventory mode value that is set to
discovered host. It has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|inventory\_mode|integer|Override the host prototype inventory mode.<br><br>Possible values:<br>-1 - disabled;<br>0 - *(default)* manual;<br>1 - automatic.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required*|

[comment]: # ({/3fac4fcc-9c5cc3d1})
