[comment]: # ({293a482d-293a482d})
# discoveryrule.delete

[comment]: # ({/293a482d-293a482d})

[comment]: # ({e3f7b3a0-1760a96c})
### Description

`object discoveryrule.delete(array lldRuleIds)`

This method allows to delete LLD rules.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/e3f7b3a0-1760a96c})

[comment]: # ({27340eaa-27340eaa})
### Parameters

`(array)` IDs of the LLD rules to delete.

[comment]: # ({/27340eaa-27340eaa})

[comment]: # ({36803485-ae1ac187})
### Return values

`(object)` Returns an object containing the IDs of the deleted LLD rules
under the `ruleids` property.

[comment]: # ({/36803485-ae1ac187})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({a2d19e6d-98660baf})
#### Deleting multiple LLD rules

Delete two LLD rules.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "discoveryrule.delete",
    "params": [
        "27665",
        "27668"
    ],
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "ruleids": [
            "27665",
            "27668"
        ]
    },
    "id": 1
}
```

[comment]: # ({/a2d19e6d-98660baf})

[comment]: # ({76a51bc0-76a51bc0})
### Source

CDiscoveryRule::delete() in
*ui/include/classes/api/services/CDiscoveryRule.php*.

[comment]: # ({/76a51bc0-76a51bc0})
