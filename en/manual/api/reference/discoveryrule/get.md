[comment]: # ({ddce888e-ddce888e})
# discoveryrule.get

[comment]: # ({/ddce888e-ddce888e})

[comment]: # ({e9f79f9b-b87cd1ee})
### Description

`integer/array discoveryrule.get(object parameters)`

The method allows to retrieve LLD rules according to the given
parameters.

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

[comment]: # ({/e9f79f9b-b87cd1ee})

[comment]: # ({37b8384e-57007d3d})
### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|itemids|string/array|Return only LLD rules with the given IDs.|
|groupids|string/array|Return only LLD rules that belong to the hosts from the given groups.|
|hostids|string/array|Return only LLD rules that belong to the given hosts.|
|inherited|boolean|If set to `true` return only LLD rules inherited from a template.|
|interfaceids|string/array|Return only LLD rules use the given host interfaces.|
|monitored|boolean|If set to `true` return only enabled LLD rules that belong to monitored hosts.|
|templated|boolean|If set to `true` return only LLD rules that belong to templates.|
|templateids|string/array|Return only LLD rules that belong to the given templates.|
|selectFilter|query|Return a [`filter`](/manual/api/reference/discoveryrule/object#LLD_rule_filter) property with data of the filter used by the LLD rule.|
|selectGraphs|query|Returns a [`graphs`](/manual/api/reference/graph/object) property with graph prototypes that belong to the LLD rule.<br><br>Supports `count`.|
|selectHostPrototypes|query|Return a [`hostPrototypes`](/manual/api/reference/hostprototype/object) property with host prototypes that belong to the LLD rule.<br><br>Supports `count`.|
|selectHosts|query|Return a [`hosts`](/manual/api/reference/host/object) property with an array of hosts that the LLD rule belongs to.|
|selectItems|query|Return an [`items`](/manual/api/reference/item/object) property with item prototypes that belong to the LLD rule.<br><br>Supports `count`.|
|selectTriggers|query|Return a [`triggers`](/manual/api/reference/trigger/object) property with trigger prototypes that belong to the LLD rule.<br><br>Supports `count`.|
|selectLLDMacroPaths|query|Return an [`lld_macro_paths`](/manual/api/reference/discoveryrule/object#lld_macro_path) property with a list of LLD macros and paths to values assigned to each corresponding macro.|
|selectPreprocessing|query|Return a [`preprocessing`](/manual/api/reference/discoveryrule/object#lld-rule-preprocessing) property with LLD rule preprocessing options.<br><br>It has the following properties:<br>`type` - `(string)` The preprocessing option type:<br>5 - Regular expression matching;<br>11 - XML XPath;<br>12 - JSONPath;<br>15 - Does not match regular expression;<br>16 - Check for error in JSON;<br>17 - Check for error in XML;<br>20 - Discard unchanged with heartbeat;<br>23 - Prometheus to JSON;<br>24 - CSV to JSON;<br>25 - Replace;<br>27 - XML to JSON.<br>28 - SNMP walk value<br>29 - SNMP walk to JSON<br><br>`params` - `(string)` Additional parameters used by preprocessing option. Multiple parameters are separated by the newline (\\n) character.<br>`error_handler` - `(string)` Action type used in case of preprocessing step failure:<br>0 - Error message is set by Zabbix server;<br>1 - Discard value;<br>2 - Set custom value;<br>3 - Set custom error message.<br><br>`error_handler_params` - `(string)` Error handler parameters.|
|selectOverrides|query|Return an [`lld_rule_overrides`](/manual/api/reference/discoveryrule/object#lld_rule_overrides) property with a list of override filters, conditions and operations that are performed on prototype objects.|
|filter|object|Return only those results that exactly match the given filter.<br><br>Accepts an array, where the keys are property names, and the values are either a single value or an array of values to match against.<br><br>Supports additional filters:<br>`host` - technical name of the host that the LLD rule belongs to.|
|limitSelects|integer|Limits the number of records returned by subselects.<br><br>Applies to the following subselects: `selectItems`, `selectGraphs`, `selectTriggers`.|
|sortfield|string/array|Sort the result by the given properties.<br><br>Possible values: `itemid`, `name`, `key_`, `delay`, `type`, `status`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters).|
|editable|boolean|^|
|excludeSearch|boolean|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

[comment]: # ({/37b8384e-57007d3d})

[comment]: # ({7223bab1-7223bab1})
### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.

[comment]: # ({/7223bab1-7223bab1})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({c7d26f2f-5f8b5429})
#### Retrieving discovery rules from a host

Retrieve all discovery rules for specific host ID.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "discoveryrule.get",
    "params": {
        "output": "extend",
        "hostids": "10202"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": [
        {
            "itemid": "27425",
            "type": "0",
            "snmp_oid": "",
            "hostid": "10202",
            "name": "Network interface discovery",
            "key_": "net.if.discovery",
            "delay": "1h",
            "status": "0",
            "trapper_hosts": "",
            "templateid": "22444",
            "valuemapid": "0",
            "params": "",
            "ipmi_sensor": "",
            "authtype": "0",
            "username": "",
            "password": "",
            "publickey": "",
            "privatekey": "",
            "interfaceid": "119",
            "description": "Discovery of network interfaces as defined in global regular expression \"Network interfaces for discovery\".",
            "lifetime": "30d",
            "jmx_endpoint": "",
            "master_itemid": "0",
            "timeout": "3s",
            "url": "",
            "query_fields": [],
            "posts": "",
            "status_codes": "200",
            "follow_redirects": "1",
            "post_type": "0",
            "http_proxy": "",
            "headers": [],
            "retrieve_mode": "0",
            "request_method": "0",
            "ssl_cert_file": "",
            "ssl_key_file": "",
            "ssl_key_password": "",
            "verify_peer": "0",
            "verify_host": "0",
            "allow_traps": "0",
            "uuid": "",
            "state": "0",
            "error": "",
            "parameters": []
        },
        {
            "itemid": "27426",
            "type": "0",
            "snmp_oid": "",
            "hostid": "10202",
            "name": "Mounted filesystem discovery",
            "key_": "vfs.fs.discovery",
            "delay": "1h",
            "status": "0",
            "trapper_hosts": "",
            "templateid": "22450",
            "valuemapid": "0",
            "params": "",
            "ipmi_sensor": "",
            "authtype": "0",
            "username": "",
            "password": "",
            "publickey": "",
            "privatekey": "",
            "interfaceid": "119",
            "description": "Discovery of file systems of different types as defined in global regular expression \"File systems for discovery\".",
            "lifetime": "30d",
            "jmx_endpoint": "",
            "master_itemid": "0",
            "timeout": "3s",
            "url": "",
            "query_fields": [],
            "posts": "",
            "status_codes": "200",
            "follow_redirects": "1",
            "post_type": "0",
            "http_proxy": "",
            "headers": [],
            "retrieve_mode": "0",
            "request_method": "0",
            "ssl_cert_file": "",
            "ssl_key_file": "",
            "ssl_key_password": "",
            "verify_peer": "0",
            "verify_host": "0",
            "allow_traps": "0",
            "uuid": "",
            "state": "0",
            "error": "",
            "parameters": []
        }
    ],
    "id": 1
}
```

[comment]: # ({/c7d26f2f-5f8b5429})

[comment]: # ({22ef0184-4d0af030})
#### Retrieving filter conditions

Retrieve the name of the LLD rule "24681" and its filter conditions. The
filter uses the "and" evaluation type, so the `formula` property is
empty and `eval_formula` is generated automatically.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "discoveryrule.get",
    "params": {
        "output": ["name"],
        "selectFilter": "extend",
        "itemids": ["24681"]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": [
        {
            "itemid": "24681",
            "name": "Filtered LLD rule",
            "filter": {
                "evaltype": "1",
                "formula": "",
                "conditions": [
                    {
                        "macro": "{#MACRO1}",
                        "value": "@regex1",
                        "operator": "8",
                        "formulaid": "A"
                    },
                    {
                        "macro": "{#MACRO2}",
                        "value": "@regex2",
                        "operator": "9",
                        "formulaid": "B"
                    },
                    {
                        "macro": "{#MACRO3}",
                        "value": "",
                        "operator": "12",
                        "formulaid": "C"
                    },
                    {
                        "macro": "{#MACRO4}",
                        "value": "",
                        "operator": "13",
                        "formulaid": "D"
                    }
                ],
                "eval_formula": "A and B and C and D"
            }
        }
    ],
    "id": 1
}
```

[comment]: # ({/22ef0184-4d0af030})

[comment]: # ({0bc15130-e3de6fe8})
#### Retrieve LLD rule by URL

Retrieve LLD rule for host by rule URL field value. Only exact match of URL string defined for LLD rule is supported.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "discoveryrule.get",
    "params": {
        "hostids": "10257",
        "filter": {
            "type": 19,
            "url": "http://127.0.0.1/discoverer.php"
        }
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": [
        {
            "itemid": "28336",
            "type": "19",
            "snmp_oid": "",
            "hostid": "10257",
            "name": "API HTTP agent",
            "key_": "api_discovery_rule",
            "delay": "5s",
            "status": "0",
            "trapper_hosts": "",
            "templateid": "0",
            "valuemapid": "0",
            "params": "",
            "ipmi_sensor": "",
            "authtype": "0",
            "username": "",
            "password": "",
            "publickey": "",
            "privatekey": "",
            "interfaceid": "5",
            "description": "",
            "lifetime": "30d",
            "jmx_endpoint": "",
            "master_itemid": "0",
            "timeout": "3s",
            "url": "http://127.0.0.1/discoverer.php",
            "query_fields": [
                {
                    "mode": "json"
                },
                {
                    "elements": "2"
                }
            ],
            "posts": "",
            "status_codes": "200",
            "follow_redirects": "1",
            "post_type": "0",
            "http_proxy": "",
            "headers": {
                "X-Type": "api",
                "Authorization": "Bearer mF_A.B5f-2.1JcM"
            },
            "retrieve_mode": "0",
            "request_method": "1",
            "ssl_cert_file": "",
            "ssl_key_file": "",
            "ssl_key_password": "",
            "verify_peer": "0",
            "verify_host": "0",
            "allow_traps": "0",
            "uuid": "",
            "state": "0",
            "error": "",
            "parameters": []
        }
    ],
    "id": 1
}
```

[comment]: # ({/0bc15130-e3de6fe8})

[comment]: # ({5c3541f3-c0ae2d21})
#### Retrieve LLD rule with overrides

Retrieve one LLD rule that has various override settings.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "discoveryrule.get",
    "params": {
        "output": ["name"],
        "itemids": "30980",
        "selectOverrides": ["name", "step", "stop", "filter", "operations"]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": [
        {
            "name": "Discover database host",
            "overrides": [
                {
                    "name": "Discover MySQL host",
                    "step": "1",
                    "stop": "1",
                    "filter": {
                        "evaltype": "2",
                        "formula": "",
                        "conditions": [
                            {
                                "macro": "{#UNIT.NAME}",
                                "operator": "8",
                                "value": "^mysqld\\.service$",
                                "formulaid": "A"
                            },
                            {
                                "macro": "{#UNIT.NAME}",
                                "operator": "8",
                                "value": "^mariadb\\.service$",
                                "formulaid": "B"
                            }
                        ],
                        "eval_formula": "A or B"
                    },
                    "operations": [
                        {
                            "operationobject": "3",
                            "operator": "2",
                            "value": "Database host",
                            "opstatus": {
                                "status": "0"
                            },
                            "optag": [
                                {
                                    "tag": "Database",
                                    "value": "MySQL"
                                }
                            ],
                            "optemplate": [
                                {
                                    "templateid": "10170"
                                }
                            ]
                        }
                    ]
                },
                {
                    "name": "Discover PostgreSQL host",
                    "step": "2",
                    "stop": "1",
                    "filter": {
                        "evaltype": "0",
                        "formula": "",
                        "conditions": [
                            {
                                "macro": "{#UNIT.NAME}",
                                "operator": "8",
                                "value": "^postgresql\\.service$",
                                "formulaid": "A"
                            }
                        ],
                        "eval_formula": "A"
                    },
                    "operations": [
                        {
                            "operationobject": "3",
                            "operator": "2",
                            "value": "Database host",
                            "opstatus": {
                                "status": "0"
                            },
                            "optag": [
                                {
                                    "tag": "Database",
                                    "value": "PostgreSQL"
                                }
                            ],
                            "optemplate": [
                                {
                                    "templateid": "10263"
                                }
                            ]
                        }
                    ]
                }
            ]
        }
    ],
    "id": 1
}
```

[comment]: # ({/5c3541f3-c0ae2d21})

[comment]: # ({1bec0758-f25d02bb})
### See also

-   [Graph prototype](/manual/api/reference/graphprototype/object#graph_prototype)
-   [Host](/manual/api/reference/host/object#host)
-   [Item prototype](/manual/api/reference/itemprototype/object#item_prototype)
-   [LLD rule filter](object#lld_rule_filter)
-   [Trigger prototype](/manual/api/reference/triggerprototype/object#trigger_prototype)

[comment]: # ({/1bec0758-f25d02bb})

[comment]: # ({e14efed8-e14efed8})
### Source

CDiscoveryRule::get() in
*ui/include/classes/api/services/CDiscoveryRule.php*.

[comment]: # ({/e14efed8-e14efed8})
