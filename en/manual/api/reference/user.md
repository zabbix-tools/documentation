[comment]: # ({2cc43904-392125b2})
# User

This class is designed to work with users.

Object references:

-   [User](/manual/api/reference/user/object#user)
-   [Media](/manual/api/reference/user/object#media)

Available methods:

-   [user.checkauthentication](/manual/api/reference/user/checkauthentication) - check and prolong user sessions
-   [user.create](/manual/api/reference/user/create) - create new users
-   [user.delete](/manual/api/reference/user/delete) - delete users
-   [user.get](/manual/api/reference/user/get) - retrieve users
-   [user.login](/manual/api/reference/user/login) - log in to the API
-   [user.logout](/manual/api/reference/user/logout) - log out of the API
-   [user.provision](/manual/api/reference/user/provision) - provision LDAP users
-   [user.unblock](/manual/api/reference/user/unblock) - unblock users
-   [user.update](/manual/api/reference/user/update) - update users

[comment]: # ({/2cc43904-392125b2})
