[comment]: # ({252eb841-4cd7085b})
# Connector

This class is designed to work with connectors.

Object references:

- [Connector](/manual/api/reference/connector/object#connector)
- [Tag filter](/manual/api/reference/connector/object#tag-filter)

Available methods:

- [connector.create](/manual/api/reference/connector/create) - create new connectors
- [connector.delete](/manual/api/reference/connector/delete) - delete connectors
- [connector.get](/manual/api/reference/connector/get) - retrieve connectors
- [connector.update](/manual/api/reference/connector/update) - update connectors

[comment]: # ({/252eb841-4cd7085b})
