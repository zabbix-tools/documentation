[comment]: # ({0090073b-5d6f5a54})
# Template dashboard

This class is designed to work with template dashboards.

Object references:

-   [Template dashboard](/manual/api/reference/templatedashboard/object#template_dashboard)
-   [Template dashboard page](/manual/api/reference/templatedashboard/object#template_dashboard_page)
-   [Template dashboard widget](/manual/api/reference/templatedashboard/object#template_dashboard_widget)
-   [Template dashboard widget field](/manual/api/reference/templatedashboard/object#template_dashboard_widget_field)

Available methods:

-   [templatedashboard.create](/manual/api/reference/templatedashboard/create) - create new template dashboards
-   [templatedashboard.delete](/manual/api/reference/templatedashboard/delete) - delete template dashboards
-   [templatedashboard.get](/manual/api/reference/templatedashboard/get) - retrieve template dashboards
-   [templatedashboard.update](/manual/api/reference/templatedashboard/update) - update template dashboards

[comment]: # ({/0090073b-5d6f5a54})
