[comment]: # ({7063b41c-7063b41c})
# token.update

[comment]: # ({/7063b41c-7063b41c})

[comment]: # ({f2679bab-f2679bab})
### Description

`object token.update(object/array tokens)`

This method allows to update existing tokens.

::: noteclassic
Only *Super admin* user type is allowed to manage tokens for
other users.
:::

[comment]: # ({/f2679bab-f2679bab})

[comment]: # ({416a8426-416a8426})
### Parameters

`(object/array)` Token properties to be updated.

The `tokenid` property must be defined for each token, all other
properties are optional. Only the passed properties will be updated, all
others will remain unchanged.

The method accepts tokens with the [standard token properties](object).

[comment]: # ({/416a8426-416a8426})

[comment]: # ({d0adc398-d0adc398})
### Return values

`(object)` Returns an object containing the IDs of the updated tokens
under the `tokenids` property.

[comment]: # ({/d0adc398-d0adc398})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({305dae6e-4f041664})
#### Remove token expiry

Remove expiry date from token.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "token.update",
    "params": {
        "tokenid": "2",
        "expires_at": "0"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "tokenids": [
            "2"
        ]
    },
    "id": 1
}
```

[comment]: # ({/305dae6e-4f041664})

[comment]: # ({63651b34-63651b34})
### Source

CToken::update() in *ui/include/classes/api/services/CToken.php*.

[comment]: # ({/63651b34-63651b34})
