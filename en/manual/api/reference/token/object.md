[comment]: # ({3683e83d-3683e83d})
# > Token object

The following objects are directly related to the `token` API.

[comment]: # ({/3683e83d-3683e83d})

[comment]: # ({e72b3f86-e5e50206})
### Token

The token object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|tokenid|string|ID of the token.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*<br>- *required* for update operations|
|name|string|Name of the token.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *required* for create operations|
|description|text|Description of the token.|
|userid|string|A user the token has been assigned to.<br><br>Default: *current user*.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *constant*|
|lastaccess|timestamp|Most recent date and time the token was authenticated.<br><br>"0" if the token has never been authenticated.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*|
|status|integer|Token status.<br><br>Possible values:<br>0 - *(default)* enabled token;<br>1 - disabled token.|
|expires\_at|timestamp|Token expiration date and time.<br><br>"0" for never-expiring tokens.|
|created\_at|timestamp|Token creation date and time.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*|
|creator\_userid|string|The creator user of the token.<br><br>[Property behavior](/manual/api/reference_commentary#property-behavior):<br>- *read-only*|

[comment]: # ({/e72b3f86-e5e50206})
