[comment]: # ({c28978f7-c28978f7})
# token.create

[comment]: # ({/c28978f7-c28978f7})

[comment]: # ({9af66986-3cdde1b6})
### Description

`object token.create(object/array tokens)`

This method allows to create new tokens.

::: noteclassic
Only *Super admin* user type is allowed to manage tokens for
other users.
:::

::: noteimportant
A token created by this method also has
to be [generated](generate) before it is usable.
:::

[comment]: # ({/9af66986-3cdde1b6})

[comment]: # ({b3691541-b3691541})
### Parameters

`(object/array)` Tokens to create.

The method accepts tokens with the [standard token properties](object).

[comment]: # ({/b3691541-b3691541})

[comment]: # ({da7805eb-da7805eb})
### Return values

`(object)` Returns an object containing the IDs of the created tokens
under the `tokenids` property. The order of the returned IDs matches the
order of the passed tokens.

[comment]: # ({/da7805eb-da7805eb})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({ac487158-119ef871})
#### Create a token

Create an enabled token that never expires and authenticates user of ID
2.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "token.create",
    "params": {
        "name": "Your token",
        "userid": "2"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "tokenids": [
            "188"
        ]
    },
    "id": 1
}
```

Create a disabled token that expires at January 21st, 2021. This token
will authenticate current user.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "token.create",
    "params": {
        "name": "Your token",
        "status": "1",
        "expires_at": "1611238072"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "tokenids": [
            "189"
        ]
    },
    "id": 1
}
```

[comment]: # ({/ac487158-119ef871})

[comment]: # ({5c90c156-5c90c156})
### Source

CToken::create() in *ui/include/classes/api/services/CToken.php*.

[comment]: # ({/5c90c156-5c90c156})
