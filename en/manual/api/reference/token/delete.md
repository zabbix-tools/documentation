[comment]: # ({c63958e0-c63958e0})
# token.delete

[comment]: # ({/c63958e0-c63958e0})

[comment]: # ({e5872416-e5872416})
### Description

`object token.delete(array tokenids)`

This method allows to delete tokens.

::: noteclassic
Only *Super admin* user type is allowed to manage tokens for
other users.
:::

[comment]: # ({/e5872416-e5872416})

[comment]: # ({92c0da91-92c0da91})
### Parameters

`(array)` IDs of the tokens to delete.

[comment]: # ({/92c0da91-92c0da91})

[comment]: # ({fd9b032e-fd9b032e})
### Return values

`(object)` Returns an object containing the IDs of the deleted tokens
under the `tokenids` property.

[comment]: # ({/fd9b032e-fd9b032e})

[comment]: # ({b41637d2-b41637d2})
### Examples

[comment]: # ({/b41637d2-b41637d2})

[comment]: # ({69adf3b7-7f001ef1})
#### Delete multiple tokens

Delete two tokens.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "token.delete",
    "params": [
        "188",
        "192"
    ],
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "tokenids": [
            "188",
            "192"
        ]
    },
    "id": 1
}
```

[comment]: # ({/69adf3b7-7f001ef1})

[comment]: # ({9ebff2e2-9ebff2e2})
### Source

CToken::delete() in *ui/include/classes/api/services/CToken.php*.

[comment]: # ({/9ebff2e2-9ebff2e2})
