[comment]: # ({80cac37b-f9e8f670})
# Template group

This class is designed to work with template groups.

Object references:

-   [Template group](/manual/api/reference/templategroup/object#template_group)

Available methods:

-   [templategroup.create](/manual/api/reference/templategroup/create) - create new template groups
-   [templategroup.delete](/manual/api/reference/templategroup/delete) - delete template groups
-   [templategroup.get](/manual/api/reference/templategroup/get) - retrieve template groups
-   [templategroup.massadd](/manual/api/reference/templategroup/massadd) - add related objects to template groups
-   [templategroup.massremove](/manual/api/reference/templategroup/massremove) - remove related objects from template groups
-   [templategroup.massupdate](/manual/api/reference/templategroup/massupdate) - replace or remove related objects from template groups
-   [templategroup.propagate](/manual/api/reference/templategroup/propagate) - propagate permissions to template groups' subgroups
-   [templategroup.update](/manual/api/reference/templategroup/update) - update template groups

[comment]: # ({/80cac37b-f9e8f670})
