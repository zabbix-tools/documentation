[comment]: # ({a6f34308-9b6159cd})
# Discovery check

This class is designed to work with discovery checks.

Object references:

-   [Discovery check](/manual/api/reference/dcheck/object#discovery_check)

Available methods:

-   [dcheck.get](/manual/api/reference/dcheck/get) - retrieve discovery checks

[comment]: # ({/a6f34308-9b6159cd})
