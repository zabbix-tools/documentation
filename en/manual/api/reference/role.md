[comment]: # ({5966b46c-a8999f2e})
# Role

This class is designed to work with user roles.

Object references:

-   [Role](/manual/api/reference/role/object#role)
-   [Role rules](/manual/api/reference/role/object#role_rules)
-   [UI element](/manual/api/reference/role/object#ui_element)
-   [Service](/manual/api/reference/role/object#service)
-   [Service tag](/manual/api/reference/role/object#service_tag)
-   [Module](/manual/api/reference/role/object#module)
-   [Action](/manual/api/reference/role/object#action)

Available methods:

-   [role.create](/manual/api/reference/role/create) - create new user roles
-   [role.delete](/manual/api/reference/role/delete) - delete user roles
-   [role.get](/manual/api/reference/role/get) - retrieve user roles
-   [role.update](/manual/api/reference/role/update) - update user roles

[comment]: # ({/5966b46c-a8999f2e})
