[comment]: # ({e87d2514-c7c352d1})
# 5 Network maps

[comment]: # ({/e87d2514-c7c352d1})

[comment]: # ({4a66853f-f69156a2})
#### Overview

Network map [export](/manual/xml_export_import) contains:

-   All related images
-   Map structure - all map settings, all contained elements with their
    settings, map links and map link status indicators

::: notewarning
Any host groups, hosts, triggers, other maps or
other elements that may be related to the exported map are not exported.
Thus, if at least one of the elements the map refers to is missing,
importing it will fail.
:::

Network map export/import is supported since Zabbix 1.8.2.

[comment]: # ({/4a66853f-f69156a2})

[comment]: # ({9f30d473-bc82aec8})
#### Exporting

To export network maps, do the following:

1. Go to *Monitoring* → *Maps*.
2. Mark the checkboxes of the network maps to export.
3. Click on *Export* below the list.

![](../../../assets/en/manual/xml_export_import/export_maps.png)

Depending on the selected format, maps are exported to a local file with
a default name:

-   *zabbix\_export\_maps.yaml* - in YAML export (default option for export);
-   *zabbix\_export\_maps.xml* - in XML export;
-   *zabbix\_export\_maps.json* - in JSON export.

[comment]: # ({/9f30d473-bc82aec8})

[comment]: # ({3df3b0e5-cc31311c})
#### Importing

To import network maps, do the following:

1. Go to *Monitoring* → *Maps*.
2. Click on *Import* to the right.
3. Select the import file.
4. Mark the required options in import rules.
5. Click on *Import*.

![](../../../assets/en/manual/xml_export_import/import_maps.png){width="600"}

Import rules:

|Rule|Description|
|----|-----------|
|*Update existing*|Existing maps will be updated with data taken from the import file. Otherwise they will not be updated.|
|*Create new*|The import will add new maps using data from the import file. Otherwise it will not add them.|

If you uncheck both map options and check the respective options for
images, images only will be imported. Image importing is only available
to Super Admin users.

A success or failure message of the import will be displayed in the
frontend.

::: notewarning
If replacing an existing image, it will affect all
maps that are using this image.
:::

[comment]: # ({/3df3b0e5-cc31311c})

[comment]: # ({cf46e23d-53418cf1})
#### Export format

Export to YAML:

```yaml
zabbix_export:
  version: '7.0'
  images:
    - name: Zabbix_server_3D_(128)
      imagetype: '1'
      encodedImage: iVBOR...5CYII=
  maps:
    - name: 'Local network'
      width: '680'
      height: '200'
      label_type: '0'
      label_location: '0'
      highlight: '1'
      expandproblem: '1'
      markelements: '1'
      show_unack: '0'
      severity_min: '0'
      show_suppressed: '0'
      grid_size: '50'
      grid_show: '1'
      grid_align: '1'
      label_format: '0'
      label_type_host: '2'
      label_type_hostgroup: '2'
      label_type_trigger: '2'
      label_type_map: '2'
      label_type_image: '2'
      label_string_host: ''
      label_string_hostgroup: ''
      label_string_trigger: ''
      label_string_map: ''
      label_string_image: ''
      expand_macros: '1'
      background: {  }
      iconmap: {  }
      urls: {  }
      selements:
        - elementtype: '0'
          elements:
            - host: 'Zabbix server'
          label: |
            {HOST.NAME}
            {HOST.CONN}
          label_location: '0'
          x: '111'
          'y': '61'
          elementsubtype: '0'
          areatype: '0'
          width: '200'
          height: '200'
          viewtype: '0'
          use_iconmap: '0'
          selementid: '1'
          icon_off:
            name: Zabbix_server_3D_(128)
          icon_on: {  }
          icon_disabled: {  }
          icon_maintenance: {  }
          urls: {  }
          evaltype: '0'
      shapes:
        - type: '0'
          x: '0'
          'y': '0'
          width: '680'
          height: '15'
          text: '{MAP.NAME}'
          font: '9'
          font_size: '11'
          font_color: '000000'
          text_halign: '0'
          text_valign: '0'
          border_type: '0'
          border_width: '0'
          border_color: '000000'
          background_color: ''
          zindex: '0'
      lines: {  }
      links: {  }
```

[comment]: # ({/cf46e23d-53418cf1})

[comment]: # ({7d1aa385-efe541b5})
#### Element tags

Element tag values are explained in the table below.

|Element|Element property|Type|Range|Description|
|--|--|--|----|--------|
|images| | | |Root element for images.|
| |name|`string`| |Unique image name.|
| |imagetype|`integer`|1 - image<br>2 - background|Image type.|
| |encodedImage| | |Base64 encoded image.|
|maps| | | |Root element for maps.|
| |name|`string`| |Unique map name.|
| |width|`integer`| |Map width, in pixels.|
| |height|`integer`| |Map height, in pixels.|
| |label\_type|`integer`|0 - label<br>1 - host IP address<br>2 - element name<br>3 - status only<br>4 - nothing|Map element label type.|
| |label\_location|`integer`|0 - bottom<br>1 - left<br>2 - right<br>3 - top|Map element label location by default.|
| |highlight|`integer`|0 - no<br>1 - yes|Enable icon highlighting for active triggers and host statuses.|
| |expandproblem|`integer`|0 - no<br>1 - yes|Display problem trigger for elements with a single problem.|
| |markelements|`integer`|0 - no<br>1 - yes|Highlight map elements that have recently changed their status.|
| |show\_unack|`integer`|0 - count of all problems<br>1 - count of unacknowledged problems<br>2 - count of acknowledged and unacknowledged problems separately|Problem display.|
| |severity\_min|`integer`|0 - not classified<br>1 - information<br>2 - warning<br>3 - average<br>4 - high<br>5 - disaster|Minimum trigger severity to show on the map by default.|
| |show\_suppressed|`integer`|0 - no<br>1 - yes|Display problems which would otherwise be suppressed (not shown) because of host maintenance.|
| |grid\_size|`integer`|20, 40, 50, 75 or 100|Cell size of a map grid in pixels, if "grid\_show=1"|
| |grid\_show|`integer`|0 - yes<br>1 - no|Display a grid in map configuration.|
| |grid\_align|`integer`|0 - yes<br>1 - no|Automatically align icons in map configuration.|
| |label\_format|`integer`|0 - no<br>1 - yes|Use advanced label configuration.|
| |label\_type\_host|`integer`|0 - label<br>1 - host IP address<br>2 - element name<br>3 - status only<br>4 - nothing<br>5 - custom label|Display as host label, if "label\_format=1"|
| |label\_type\_hostgroup|`integer`|0 - label<br>2 - element name<br>3 - status only<br>4 - nothing<br>5 - custom label|Display as host group label, if "label\_format=1"|
| |label\_type\_trigger|`integer`|0 - label<br>2 - element name<br>3 - status only<br>4 - nothing<br>5 - custom label|Display as trigger label, if "label\_format=1"|
| |label\_type\_map|`integer`|0 - label<br>2 - element name<br>3 - status only<br>4 - nothing<br>5 - custom label|Display as map label, if "label\_format=1"|
| |label\_type\_image|`integer`|0 - label<br>2 - element name<br>4 - nothing<br>5 - custom label|Display as image label, if "label\_format=1"|
| |label\_string\_host|`string`| |Custom label for host elements, if "label\_type\_host=5"|
| |label\_string\_hostgroup|`string`| |Custom label for host group elements, if "label\_type\_hostgroup=5"|
| |label\_string\_trigger|`string`| |Custom label for trigger elements, if "label\_type\_trigger=5"|
| |label\_string\_map|`string`| |Custom label for map elements, if "label\_type\_map=5"|
| |label\_string\_image|`string`| |Custom label for image elements, if "label\_type\_image=5"|
| |expand\_macros|`integer`|0 - no<br>1 - yes|Expand macros in labels in map configuration.|
| |background|`id`| |ID of the background image (if any), if "imagetype=2"|
| |iconmap|`id`| |ID of the icon mapping (if any).|
|urls| | | |Used by maps or each map element.|
| |name|`string`| |Link name.|
| |url|`string`| |Link URL.|
| |elementtype|`integer`|0 - host<br>1 - map<br>2 - trigger<br>3 - host group<br>4 - image|Map item type the link belongs to.|
|selements| | | | |
| |elementtype|`integer`|0 - host<br>1 - map<br>2 - trigger<br>3 - host group<br>4 - image|Map element type.|
| |label|`string`| |Icon label.|
| |label\_location|`integer`|-1 - use map default<br>0 - bottom<br>1 - left<br>2 - right<br>3 - top| |
| |x|`integer`| |Location on the X axis.|
| |y|`integer`| |Location on the Y axis.|
| |elementsubtype|`integer`|0 - single host group<br>1 - all host groups|Element subtype, if "elementtype=3"|
| |areatype|`integer`|0 - same as whole map<br>1 - custom size|Area size, if "elementsubtype=1"|
| |width|`integer`| |Width of area, if "areatype=1"|
| |height|`integer`| |Height of area, if "areatype=1"|
| |viewtype|`integer`|0 - place evenly in the area|Area placement algorithm, if "elementsubtype=1"|
| |use\_iconmap|`integer`|0 - no<br>1 - yes|Use icon mapping for this element. Relevant only if iconmapping is activated on map level.|
| |selementid|`id`| |Unique element record ID.|
| |evaltype|`integer`| |Evaluation type for tags.|
|tags| | | |Problem tags (for host and host group elements). If tags are given, only problems with these tags will be displayed on the map.|
| |tag| | |Tag name.|
| |value| | |Tag value.|
| |operator| | |Operator.|
|elements| | | |Zabbix entities that are represented on the map (host, host group, map etc).|
| |host| | | |
|icon\_off| | | |Image to use when element is in 'OK' status.|
|icon\_on| | | |Image to use when element is in 'Problem' status.|
|icon\_disabled| | | |Image to use when element is disabled.|
|icon\_maintenance| | | |Image to use when element is in maintenance.|
| |name|`string`| |Unique image name.|
|shapes| | | | |
| |type|`integer`|0 - rectangle<br>1 - ellipse|Shape type.|
| |x|`integer`| |X coordinates of the shape in pixels.|
| |y|`integer`| |Y coordinates of the shape in pixels.|
| |width|`integer`| |Shape width.|
| |height|`integer`| |Shape height.|
| |border\_type|`integer`|0 - none<br>1 - bold line<br>2 - dotted line<br>3 - dashed line|Type of the border for the shape.|
| |border\_width|`integer`| |Width of the border in pixels.|
| |border\_color|`string`| |Border color represented in hexadecimal code.|
| |text|`string`| |Text inside of shape.|
| |font|`integer`|0 - Georgia, serif<br>1 - "Palatino Linotype", "Book Antiqua", Palatino, serif<br>2 - "Times New Roman", Times, serif<br>3 - Arial, Helvetica, sans-serif<br>4 - "Arial Black", Gadget, sans-serif<br>5 - "Comic Sans MS", cursive, sans-serif<br>6 - Impact, Charcoal, sans-serif<br>7 - "Lucida Sans Unicode", "Lucida Grande", sans-serif<br>8 - Tahoma, Geneva, sans-serif<br>9 - "Trebuchet MS", Helvetica, sans-serif<br>10 - Verdana, Geneva, sans-serif<br>11 - "Courier New", Courier, monospace<br>12 - "Lucida Console", Monaco, monospace|Text font style.|
| |font\_size|`integer`| |Font size in pixels.|
| |font\_color|`string`| |Font color represented in hexadecimal code.|
| |text\_halign|`integer`|0 - center<br>1 - left<br>2 - right|Horizontal alignment of text.|
| |text\_valign|`integer`|0 - middle<br>1 - top<br>2 - bottom|Vertical alignment of text.|
| |background\_color|`string`| |Background (fill) color represented in hexadecimal code.|
| |zindex|`integer`| |Value used to order all shapes and lines (z-index).|
|lines| | | | |
| |x1|`integer`| |X coordinates of the line point 1 in pixels.|
| |y1|`integer`| |Y coordinates of the line point 1 in pixels.|
| |x2|`integer`| |X coordinates of the line point 2 in pixels.|
| |y2|`integer`| |Y coordinates of the line point 2 in pixels.|
| |line\_type|`integer`|0 - none<br>1 - bold line<br>2 - dotted line<br>3 - dashed line|Line type.|
| |line\_width|`integer`| |Line width in pixels.|
| |line\_color|`string`| |Line color represented in hexadecimal code.|
| |zindex|`integer`| |Value used to order all shapes and lines (z-index).|
|links| | | |Links between map elements.|
| |drawtype|`integer`|0 - line<br>2 - bold line<br>3 - dotted line<br>4 - dashed line|Link style.|
| |color|`string`| |Link color (6 symbols, hex).|
| |label|`string`| |Link label.|
| |selementid1|`id`| |ID of one element to connect.|
| |selementid2|`id`| |ID of the other element to connect.|
|linktriggers| | | |Link status indicators.|
| |drawtype|`integer`|0 - line<br>2 - bold line<br>3 - dotted line<br>4 - dashed line|Link style when trigger is in the 'Problem' state.|
| |color|`string`| |Link color (6 symbols, hex) when trigger is in the 'Problem' state.|
|trigger| | | |Trigger used for indicating link status.|
| |description|`string`| |Trigger name.|
| |expression|`string`| |Trigger expression.|
| |recovery\_expression|`string`| |Trigger recovery expression.|

[comment]: # ({/7d1aa385-efe541b5})
