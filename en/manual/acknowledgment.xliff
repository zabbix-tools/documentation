<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="en" datatype="plaintext" original="manual/acknowledgment.md">
    <body>
      <trans-unit id="61d493ca" xml:space="preserve">
        <source># 13. Problem acknowledgment</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/acknowledgment.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="79474bd8" xml:space="preserve">
        <source>#### Overview

Problem events in Zabbix can be acknowledged by users.

If a user gets notified about a problem event, they can go to Zabbix
frontend, open the problem update popup window of that problem using one
of the ways listed below and acknowledge the problem. When
acknowledging, they can enter their comment for it, saying that they are
working on it or whatever else they may feel like saying about it.

This way, if another system user spots the same problem, they
immediately see if it has been acknowledged and the comments so far.

This way the workflow of resolving problems with more than one system
user can take place in a coordinated way.

Acknowledgment status is also used when defining [action
operations](/manual/config/notifications/action/operation). You can
define, for example, that a notification is sent to a higher level
manager only if an event is not acknowledged for some time.

To acknowledge events and comment on them, a user must have at least
read permissions to the corresponding triggers. To change problem
severity or close problem, a user must have read-write permissions to
the corresponding triggers.

There are **several** ways to access the problem update popup window,
which allows acknowledging a problem.

-   You may select problems in *Monitoring* → *Problems* and then click
    on *Mass update* below the list
-   You can click on *Update* in the *Update* column of a problem in:
    -   *Dashboards* (*Problems* and *Problems by severity*
        widgets)
    -   *Monitoring → Problems*
    -   *Monitoring → Problems → Event details*
-   You can click on an unresolved problem cell in:
    -   *Dashboards* (*Trigger overview* widget)

The popup menu contains an *Update* option that will take you to
the problem update window.</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/acknowledgment.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="fa7fdaab" xml:space="preserve">
        <source>#### Updating problems

The problem update popup allows to:

-   comment on the problem
-   view comments and actions so far
-   change problem severity
-   suppress/unsuppress problem
-   acknowledge/unacknowledge problem
-   change symptom problem to cause problem
-   manually close problem

![](../../assets/en/manual/acknowledges/update_problem.png){width="600"}

All mandatory input fields are marked with a red asterisk.

|Parameter|Description|
|--|--------|
|*Problem*|If only one problem is selected, the problem name is displayed.&lt;br&gt;If several problems are selected, *N problems selected* is displayed.|
|*Message*|Enter text to comment on the problem (maximum 2048 characters).|
|*History*|Previous activities and comments on the problem are listed, along with the time and user details.&lt;br&gt;For the meaning of icons used to denote user actions see the [event detail](/manual/web_interface/frontend_sections/monitoring/problems#viewing_details) page.&lt;br&gt;Note that history is displayed if only one problem is selected for the update.|
|*Scope*|Define the scope of such actions as changing severity, acknowledging or manually closing problems:&lt;br&gt;**Only selected problem** - will affect this event only&lt;br&gt;**Selected and all other problems of related triggers** - in case of acknowledgment/closing problem, will affect this event and all other problems that are not acknowledged/closed so far. If the scope contains problems already acknowledged or closed, these problems will not be acknowledged/closed repeatedly. On the other hand, the number of message and severity change operations are not limited.|
|*Change severity*|Mark the checkbox and click on the severity button to update problem severity.&lt;br&gt;The checkbox for changing severity is available if read-write permissions exist for at least one of the selected problems. Only those problems that are read-writable will be updated when clicking on *Update*.&lt;br&gt;If read-write permissions exist for none of the selected triggers, the checkbox is disabled.|
|*Suppress*|Mark the checkbox to suppress the problem:&lt;br&gt;**Indefinitely** - suppress indefinitely&lt;br&gt;**Until** - suppress until a given time. Both [absolute and relative](/manual/config/visualization/graphs/simple#time-period-selector) time formats are supported, for example:&lt;br&gt;`now+1d` - for one day from now (default)&lt;br&gt;`now/w` - until the end of the current week&lt;br&gt;`2022-05-28 12:00:00` - until absolute date/time&lt;br&gt;Note that a simple period (e. g., `1d`, `1w`) is not supported.&lt;br&gt;Availability of this option depends on the "Suppress problems" user role settings.&lt;br&gt;See also: [Problem suppression](/manual/acknowledgment/suppression)|
|*Unsuppress*|Mark the checkbox to unsuppress the problem. This checkbox is active only if at least one of the selected problems is suppressed.&lt;br&gt;Availability of this option depends on the "Suppress problems" user role settings.|
|*Acknowledge*|Mark the checkbox to acknowledge the problem.&lt;br&gt;This checkbox is available if there is at least one unacknowledged problem among the selected.&lt;br&gt;It is not possible to add another acknowledgment for an already acknowledged problem (it is possible to add another comment though).|
|*Unacknowledge*|Mark the checkbox to unacknowledge the problem.&lt;br&gt;This checkbox is available if there is at least one acknowledged problem among the selected.|
|*Convert to cause*|Mark the checkbox to convert the symptom problem(s) to cause problem(s).|
|*Close problem*|Mark the checkbox to manually close the selected problem(s).&lt;br&gt;The checkbox for closing a problem is available if the *Allow manual close* option is checked in [trigger configuration](/manual/config/triggers/trigger) for at least one of the selected problems. Only those problems will be closed that are allowed to be closed when clicking on *Update*.&lt;br&gt;If no problem is manually closeable, the checkbox is disabled.&lt;br&gt;Already closed problems will not be closed repeatedly.|</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/acknowledgment.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="36ba3858" xml:space="preserve">
        <source>#### Display

Based on acknowledgment information it is possible to configure how the
problem count is displayed in the dashboard or maps. To do that, you
have to make selections in the *Problem display* option, available in
both [map
configuration](/manual/config/visualization/maps/map#creating_a_map) and
the *Problems by severity* [dashboard
widget](/manual/web_interface/frontend_sections/dashboards##adding_widgets).
It is possible to display all problem count, unacknowledged problem
count as separated from the total or unacknowledged problem count only.

Based on problem update information (acknowledgment, etc.), it is
possible to configure update operations - send a message or execute
remote commands.</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/acknowledgment.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
    </body>
  </file>
</xliff>
