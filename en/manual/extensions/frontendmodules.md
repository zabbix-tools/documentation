[comment]: # ({d2a688ac-ed2f6c13})
# 3 Frontend modules

[comment]: # ({/d2a688ac-ed2f6c13})

[comment]: # ({8c4f6bda-1060eddf})
#### Overview

It is possible to enhance Zabbix frontend functionality by adding third-party modules or by developing your own modules
without the need to change the source code of Zabbix.

Note that the module code will run with the same privileges as Zabbix
source code. This means:

-   third-party modules can be harmful. You must trust the modules you are
    installing;
-   Errors in a third-party module code may crash the frontend. If this
    happens, just remove the module code from the frontend. As soon as
    you reload Zabbix frontend, you'll see a note saying that some
    modules are absent. Go to [Module
    administration](/manual/web_interface/frontend_sections/administration/general#modules)
    (in *Administration* → *General* → *Modules*) and click *Scan
    directory* again to remove non-existent modules from the database.

[comment]: # ({/8c4f6bda-1060eddf})

[comment]: # ({cffdc09c-cffdc09c})
#### Installation

Please always read the installation manual for a particular module. It
is recommended to install new modules one by one to catch failures
easily.

Just before you install a module:

-   Make sure you have downloaded the module from a trusted source.
    Installation of harmful code may lead to consequences, such as data
    loss
-   Different versions of the same module (same ID) can be installed in
    parallel, but only a single version can be enabled at once

Steps to install a module:

-   Unpack your module within its own folder in the `modules` folder of
    the Zabbix frontend
-   Ensure that your module folder contains at least the manifest.json
    file
-   Navigate to [Module
    administration](/manual/web_interface/frontend_sections/administration/general#modules)
    and click the *Scan directory* button
-   New module will appear in the list along with its version, author,
    description and status
-   Enable module by clicking on its status

Troubleshooting:

|Problem|Solution|
|-------|--------|
|*Module did not appear in the list*|Make sure that the manifest.json file exists in `modules/your-module/` folder of the Zabbix frontend. If it does that means the module does not suit the current Zabbix version. If manifest.json file does not exist, you have probably unpacked in the wrong directory.|
|*Frontend crashed*|The module code is not compatible with the current Zabbix version or server configuration. Please delete module files and reload the frontend. You'll see a notice that some modules are absent. Go to [Module administration](/manual/web_interface/frontend_sections/administration/general#modules) and click *Scan directory* again to remove non-existent modules from the database.|
|*Error message about identical namespace, ID or actions appears*|New module tried to register a namespace, ID or actions which are already registered by other enabled modules. Disable the conflicting module (mentioned in error message) prior to enabling the new one.|
|*Technical error messages appear*|Report errors to the developer of the module.|

[comment]: # ({/cffdc09c-cffdc09c})

[comment]: # ({5ca56638-26c922c0})
#### Developing modules

For information about developing custom modules, see [Developer center](/devel/modules).

[comment]: # ({/5ca56638-26c922c0})
