[comment]: # ({61c28a0f-ea7b20e9})
# 2 Plugins

[comment]: # ({/61c28a0f-ea7b20e9})

[comment]: # ({b18abd4f-84502826})
#### Overview

Plugins provide an option to extend the monitoring capabilities of Zabbix. Plugins are written in Go programming
language and are supported by Zabbix agent 2 only.
Plugins provide an alternative to [loadable modules](/manual/extensions/loadablemodules) (written in C),
and [other methods](/manual/extensions) for extending Zabbix functionality.

The following features are specific to agent 2 and its plugins:

-   support of scheduled and flexible intervals for both passive and
    active checks;
-   task queue management with respect to schedule and task concurrency;
-   plugin-level timeouts;
-   compatibility check of Zabbix agent 2 and its plugins on start up.

Since Zabbix 6.0.0, plugins don't have to be integrated into the agent 2 directly and can be added as loadable plugins,
thus making the creation process of additional plugins for gathering new monitoring metrics easier.

This page lists Zabbix native and loadable plugins, and describes plugin configuration principles from the user perspective.

For instructions and tutorials on writing your own plugins, see [Developer center](/devel/plugins).

For more information on the communication process between Zabbix agent 2 and a loadable plugin, as well as the metrics collection process, see [Connection diagram](/devel/plugins#connection-diagram).

[comment]: # ({/b18abd4f-84502826})

[comment]: # ({66c56a02-ff9223df})
#### Configuring plugins

This section provides common plugin configuration principles and best practices.

All plugins are configured using *Plugins.\** parameter, which can either be part of the Zabbix agent 2 [configuration
file](/manual/appendix/config/zabbix_agent2) or a plugin's own
[configuration file](/manual/appendix/config/zabbix_agent2_plugins). If a plugin uses a separate configuration file,
path to this file should be specified in the Include parameter of Zabbix agent 2 configuration file.

A typical plugin parameter has the following structure:

*Plugins.<PluginName>.<Parameter>=<Value>*

Additionally, there are two specific groups of parameters: 

-   *Plugins.<PluginName>.Default.<Parameter>=<Value>* used for defining [default parameter values](#default-values).

-   *Plugins.<PluginName>.<SessionName>.<Parameter>=<Value>* used for defining separate sets of parameters for different monitoring targets via [named sessions](#named-sessions).

All parameter names should adhere to the following requirements:

-   it is recommended to capitalize the names of your plugins;
-   the parameter should be capitalized;
-   special characters are not allowed;
-   nesting isn’t limited by a maximum level;
-   the number of parameters is not limited.

For example, to perform [active checks](/manual/concepts/agent2#passive-and-active-checks) that do not have *Scheduling* [update interval](/manual/config/items/item/custom_intervals#scheduling-intervals) immediately after the agent restart only for the Uptime plugin,
set `Plugins.Uptime.System.ForceActiveChecksOnStart=1` in the [configuration file](/manual/appendix/config/zabbix_agent2).
Similarly, to set custom limit for [concurrent checks](/manual/concepts/agent2#check-concurrency) for the CPU plugin,
set the `Plugins.CPU.System.Capacity=N` in the [configuration file](/manual/appendix/config/zabbix_agent2).

[comment]: # ({/66c56a02-ff9223df})

[comment]: # ({21ee51ed-bebf9145})
##### Default values

You can set default values for the connection-related parameters (URI, username, password, etc.) in the configuration file
in the format: 

*Plugins.<PluginName>.Default.<Parameter>=<Value>*

For example, *Plugins.Mysql.Default.Username=zabbix*, *Plugins.MongoDB.Default.Uri=tcp://127.0.0.1:27017*, etc.

If a value for such parameter is not provided in an item key or in the [named session](#named-sessions) parameters,
the plugin will use the default value. If a default parameter is also undefined, hardcoded defaults will be used.

:::note-classic
If an item key does not have any parameters, Zabbix agent 2 will attempt to collect the metric using values defined in the default parameters section.
:::

[comment]: # ({/21ee51ed-bebf9145})

[comment]: # ({d3fd777d-36aaae86})
##### Named sessions

Named sessions represent an additional level of plugin parameters and can be used to specify separate sets of authentication parameters for each of the instances being monitored.
Each named session parameter should have the following structure:

*Plugins.<PluginName>.Sessions.<SessionName>.<Parameter>=<Value>*

A session name can be used as a connString item key parameter instead of specifying a URI, username, and/or password separately.

In item keys, the first parameter can be either a connString or a URI.
If the first key parameter doesn't match any session name, it will be treated as a URI.
Note that passing embedded URI credentials in the item key is not supported, use named session parameters instead.

The list of available [named session parameters](/manual/appendix/config/zabbix_agent2_plugins) depends on the plugin.

*Since Zabbix 6.4.2*, it is possible to override session parameters by specifying new values in the item key parameters (see [example](#example-2)).

*Since Zabbix 6.4.3*, if a parameter is not defined for the named session, Zabbix agent 2 will use the value defined in the [default plugin parameter](#default-values).

[comment]: # ({/d3fd777d-36aaae86})

[comment]: # ({897e119d-62cf279d})

##### Parameter priority

*Since version 6.4.3*, Zabbix agent 2 plugins search for connection-related parameter values in the following order: 

![](../../../assets/en/diagrams/agent2_parameters.png)

1. The first item key parameter is compared to session names. If no match is found it is treated as an actual value; in this case, step 3 will be skipped. If a match is found, the parameter value (usually, a URI) must be defined in the named session.
2. Other parameters will be taken from the item key if defined.
3. If an item key parameter (for example, password) is empty, plugin will look for the corresponding named session parameter.
4. If the session parameter is also not specified, the value defined in the corresponding [default parameter](#default-values) will be used.
5. If all else fails, the plugin will use the hardcoded default value.

[comment]: # ({/897e119d-62cf279d})

[comment]: # ({4d31915b-038eec44})

##### Example 1

Monitoring of two instances “MySQL1” and “MySQL2”.

Configuration parameters:

```bash
Plugins.Mysql.Sessions.MySQL1.Uri=tcp://127.0.0.1:3306
Plugins.Mysql.Sessions.MySQL1.User=mysql1_user
Plugins.Mysql.Sessions.MySQL1.Password=unique_password
Plugins.Mysql.Sessions.MySQL2.Uri=tcp://192.0.2.0:3306
Plugins.Mysql.Sessions.MySQL2.User=mysql2_user
Plugins.Mysql.Sessions.MySQL2.Password=different_password
```

[Item keys](/manual/config/items/itemtypes/zabbix_agent/zabbix_agent2): `mysql.ping[MySQL1]`, `mysql.ping[MySQL2]`

[comment]: # ({/4d31915b-038eec44})

[comment]: # ({21dc08ec-c0e9a7f9})

##### Example 2

Providing some of the parameters in the item key (supported since Zabbix 6.4.2).

Configuration parameters:

```bash
Plugins.PostgreSQL.Sessions.Session1.Uri=tcp://192.0.2.234:5432
Plugins.PostgreSQL.Sessions.Session1.User=old_username
Plugins.PostgreSQL.Sessions.Session1.Password=session_password
```

[Item key](/manual/config/items/itemtypes/zabbix_agent/zabbix_agent2): `pgsql.ping[session1,new_username,,postgres]`

As a result of this configuration, the agent will connect to PostgreSQL using the following parameters:

- URI from session parameter: *192.0.2.234:5432*
- Username from the item key: *new_username*
- Password from session parameter (since it is omitted in the item key): *session_password*
- Database name from the item key: *postgres*

[comment]: # ({/21dc08ec-c0e9a7f9})

[comment]: # ({55fd6560-414321c1})

##### Example 3

Collecting a metric using default configuration parameters.

Configuration parameters:

```bash
Plugins.PostgreSQL.Default.Uri=tcp://192.0.2.234:5432
Plugins.PostgreSQL.Default.User=zabbix
Plugins.PostgreSQL.Default.Password=password
```

[Item key](/manual/config/items/itemtypes/zabbix_agent/zabbix_agent2): `pgsql.ping[,,,postgres]`

As a result of this configuration, the agent will connect to PostgreSQL using the parameters:

- Default URI: *192.0.2.234:5432*
- Default username: *zabbix*
- Default password: *password*
- Database name from the item key: *postgres*

[comment]: # ({/55fd6560-414321c1})

[comment]: # ({a7f15700-8385f300})
##### Connections

Some plugins support gathering metrics from multiple instances simultaneously. Both local and remote instances can be
monitored. TCP and Unix-socket connections are supported.

It is recommended to configure plugins to keep connections to instances in an open state. The benefits are reduced
network congestion, latency, and CPU and memory usage due to the lower number of connections. The client library takes
care of this.

::: notetip
Time period for which unused connections should remain open can be determined by *Plugins.<PluginName>.KeepAlive*
parameter. Example: *Plugins.Memcached.KeepAlive*
:::

[comment]: # ({/a7f15700-8385f300})

[comment]: # ({395104ad-4c4f3ca2})

#### Plugins 

All metrics supported by Zabbix agent 2 are collected by plugins.

[comment]: # ({/395104ad-4c4f3ca2})

[comment]: # ({5e7b2f03-ebdf4ed5})

##### Built-in

The following plugins for Zabbix agent 2 are available out-of-the-box. Click on the plugin name to go to the plugin repository with additional information.

|Plugin name|Description|[Supported item keys](/manual/config/items/itemtypes/zabbix_agent/zabbix_agent2)|Comments|
|--|--|--|----|
|Agent|Metrics of the Zabbix agent being used.|agent.hostname, agent.ping, agent.version|Supported keys have the same parameters as Zabbix agent [keys](/manual/config/items/itemtypes/zabbix_agent).|
|[Ceph](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/ceph/README.md)|Ceph monitoring.|ceph.df.details, ceph.osd.stats, ceph.osd.discovery, ceph.osd.dump,<br>ceph.ping, ceph.pool.discovery, ceph.status| |
|[CPU](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/system/cpu)|System CPU monitoring (number of CPUs/CPU cores, discovered CPUs, utilization percentage).|system.cpu.discovery, system.cpu.num, system.cpu.util|Supported keys have the same parameters as Zabbix agent [keys](/manual/config/items/itemtypes/zabbix_agent).|
|[Docker](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/docker)|Monitoring of Docker containers.|docker.container\_info, docker.container\_stats, docker.containers, docker.containers.discovery,<br>docker.data\_usage, docker.images, docker.images.discovery, docker.info, docker.ping|See also:<br>[Configuration parameters](/manual/appendix/config/zabbix_agent2_plugins/d_plugin)|
|File|File metrics collection.|vfs.file.cksum, vfs.file.contents, vfs.file.exists, vfs.file.md5sum,<br>vfs.file.regexp, vfs.file.regmatch, vfs.file.size, vfs.file.time|Supported keys have the same parameters as Zabbix agent [keys](/manual/config/items/itemtypes/zabbix_agent).|
|[Kernel](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/kernel)|Kernel monitoring.|kernel.maxfiles, kernel.maxproc|Supported keys have the same parameters as Zabbix agent [keys](/manual/config/items/itemtypes/zabbix_agent).|
|[Log](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/log)|Log file monitoring.|log, log.count, logrt, logrt.count|Supported keys have the same parameters as Zabbix agent [keys](/manual/config/items/itemtypes/zabbix_agent).<br><br>See also:<br>Plugin configuration parameters ([Unix](/manual/appendix/config/zabbix_agent2)/[Windows](/manual/appendix/config/zabbix_agent2_win))|
|[Memcached](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/memcached/README.md)|Memcached server monitoring.|memcached.ping, memcached.stats| |
|[Modbus](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/modbus/README.md)|Reads Modbus data.|modbus.get|Supported keys have the same parameters as Zabbix agent [keys](/manual/config/items/itemtypes/zabbix_agent).|
|[MQTT](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/mqtt/README.md)|Receives published values of MQTT topics.|mqtt.get|To configure encrypted connection to the MQTT broker, specify the TLS parameters in the agent configuration file as [named session](#named_sessions) or [default](#default-values) parameters. Currently, TLS parameters cannot be passed as item key parameters. |
|[MySQL](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/mysql/README.md)|Monitoring of MySQL and its forks.|mysql.db.discovery, mysql.db.size, mysql.get\_status\_variables,<br>mysql.ping, mysql.replication.discovery, mysql.replication.get\_slave\_status, mysql.version|To configure encrypted connection to the database, specify the TLS parameters in the agent configuration file as [named session](#named_sessions) or [default](#default-values) parameters. Currently, TLS parameters cannot be passed as item key parameters.|
|[NetIf](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/net/netif)|Monitoring of network interfaces.|net.if.collisions, net.if.discovery, net.if.in, net.if.out, net.if.total|Supported keys have the same parameters as Zabbix agent [keys](/manual/config/items/itemtypes/zabbix_agent).|
|[Oracle](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/oracle/README.md)|Oracle Database monitoring.|oracle.diskgroups.stats, oracle.diskgroups.discovery, oracle.archive.info, oracle.archive.discovery,<br>oracle.cdb.info, oracle.custom.query, oracle.datafiles.stats, oracle.db.discovery,<br>oracle.fra.stats, oracle.instance.info, oracle.pdb.info, oracle.pdb.discovery,<br>oracle.pga.stats, oracle.ping, oracle.proc.stats, oracle.redolog.info,<br>oracle.sga.stats, oracle.sessions.stats, oracle.sys.metrics, oracle.sys.params,<br>oracle.ts.stats, oracle.ts.discovery, oracle.user.info|Install the [Oracle Instant Client](https://www.oracle.com/database/technologies/instant-client/downloads.html) before using the plugin.|
|[Proc](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/proc)|Process CPU utilization percentage.|proc.cpu.util|Supported key has the same parameters as Zabbix agent [key](/manual/config/items/itemtypes/zabbix_agent).|
|[Redis](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/redis/README.md)|Redis server monitoring.|redis.config, redis.info, redis.ping, redis.slowlog.count| |
|[Smart](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/smart)|S.M.A.R.T. monitoring.|smart.attribute.discovery, smart.disk.discovery, smart.disk.get|Sudo/root access rights to smartctl are required for the user executing Zabbix agent 2. The minimum required smartctl version is 7.1.<br><br>Supported [keys](/manual/config/items/itemtypes/zabbix_agent/zabbix_agent2) can be used with Zabbix agent 2 only on Linux/Windows, both as a passive and active check.<br>See also:<br>[Configuration parameters](/manual/appendix/config/zabbix_agent2_plugins/smart_plugin)|
|[SW](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/system/sw)|Listing of installed packages.|system.sw.packages, system.sw.packages.get|The supported keys have the same parameters as Zabbix agent [key](/manual/config/items/itemtypes/zabbix_agent).|
|[Swap](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/system/swap)|Swap space size in bytes/percentage.|system.swap.size|Supported key has the same parameters as Zabbix agent [key](/manual/config/items/itemtypes/zabbix_agent).|
|SystemRun|Runs specified command.|system.run|Supported key has the same parameters as Zabbix agent [key](/manual/config/items/itemtypes/zabbix_agent).<br><br>See also:<br>Plugin configuration parameters ([Unix](/manual/appendix/config/zabbix_agent2)/[Windows](/manual/appendix/config/zabbix_agent2_win))|
|Systemd|Monitoring of systemd services.|systemd.unit.discovery, systemd.unit.get, systemd.unit.info| |
|[TCP](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/net/tcp)|TCP connection availability check.|net.tcp.port|Supported key has the same parameters as Zabbix agent [key](/manual/config/items/itemtypes/zabbix_agent).|
|[UDP](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/net/udp)|Monitoring of the UDP services availability and performance.|net.udp.service, net.udp.service.perf|Supported keys have the same parameters as Zabbix agent [keys](/manual/config/items/itemtypes/zabbix_agent).|
|[Uname](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/system/uname)|Retrieval of information about the system.|system.hostname, system.sw.arch, system.uname|Supported keys have the same parameters as Zabbix agent [keys](/manual/config/items/itemtypes/zabbix_agent).|
|[Uptime](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/system/uptime)|System uptime metrics collection.|system.uptime|Supported key has the same parameters as Zabbix agent [key](/manual/config/items/itemtypes/zabbix_agent).|
|[VFSDev](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/vfs/dev)|VFS metrics collection.|vfs.dev.discovery, vfs.dev.read, vfs.dev.write|Supported keys have the same parameters as Zabbix agent [keys](/manual/config/items/itemtypes/zabbix_agent).|
|[WebCertificate](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/web/certificate)|Monitoring of TLS/SSL website certificates.|web.certificate.get| |
|[WebPage](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/web/page)|Web page monitoring.|web.page.get, web.page.perf, web.page.regexp|Supported keys have the same parameters as Zabbix agent [keys](/manual/config/items/itemtypes/zabbix_agent).|
|[ZabbixAsync](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/zabbix/async)|Asynchronous metrics collection.|net.tcp.listen, net.udp.listen, sensor, system.boottime, system.cpu.intr, system.cpu.load,<br>system.cpu.switches, system.hw.cpu, system.hw.macaddr, system.localtime, system.sw.os,<br>system.swap.in, system.swap.out, vfs.fs.discovery|Supported keys have the same parameters as Zabbix agent [keys](/manual/config/items/itemtypes/zabbix_agent).|
|[ZabbixStats](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/zabbix/stats)|Zabbix server/proxy internal metrics or number of delayed items in a queue.|zabbix.stats|Supported keys have the same parameters as Zabbix agent [keys](/manual/config/items/itemtypes/zabbix_agent).|
|[ZabbixSync](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/zabbix/sync)|Synchronous metrics collection.|net.dns, net.dns.record, net.tcp.service, net.tcp.service.perf, proc.mem,<br>proc.num, system.hw.chassis, system.hw.devices, system.sw.packages,<br>system.users.num, vfs.dir.count, vfs.dir.size, vfs.fs.get, vfs.fs.inode,<br>vfs.fs.size, vm.memory.size.|Supported keys have the same parameters as Zabbix agent [keys](/manual/config/items/itemtypes/zabbix_agent).|

[comment]: # ({/5e7b2f03-ebdf4ed5})

[comment]: # ({aa51ee2c-9ecbdda6})

##### Loadable

::: notetip
Loadable plugins, when launched with:<br>
-   *-V --version* - print plugin version and license information;<br>
-   *-h --help* - print help information.
:::

Click on the plugin name to go to the plugin repository with additional information.

|Plugin name|Description|Supported item keys|Comments|
|--|--|--|----|
|[MongoDB](https://git.zabbix.com/projects/AP/repos/mongodb/browse/README.md)|Monitoring of MongoDB servers and clusters (document-based, distributed database).|mongodb.collection.stats, mongodb.collections.discovery, mongodb.collections.usage, mongodb.connpool.stats,<br>mongodb.db.stats, mongodb.db.discovery, mongodb.jumbo\_chunks.count, mongodb.oplog.stats,<br>mongodb.ping, mongodb.rs.config, mongodb.rs.status, mongodb.server.status,<br>mongodb.sh.discovery|To configure encrypted connections to the database, specify the TLS parameters in the agent configuration file as [named session](#named_sessions) parameters.<br>Currently, TLS parameters cannot be passed as item key parameters.<br><br>See also [MongoDB plugin configuration parameters](/manual/appendix/config/zabbix_agent2_plugins/mongodb_plugin#parameters).|
|[PostgreSQL](https://git.zabbix.com/projects/AP/repos/postgresql/browse/README.md)|Monitoring of PostgreSQL and its forks.|pgsql.autovacuum.count, pgsql.archive, pgsql.bgwriter, pgsql.cache.hit, pgsql.connections,<br> pgsql.custom.query, pgsql.dbstat, pgsql.dbstat.sum, pgsql.db.age, pgsql.db.bloating\_tables, <br> pgsql.db.discovery, pgsql.db.size, pgsql.locks, pgsql.oldest.xid, pgsql.ping, pgsql.queries, <br> pgsql.replication.count, pgsql.replication.process, pgsql.replication.process.discovery, pgsql.replication.recovery\_role, pgsql.replication.status, <br> pgsql.replication\_lag.b, pgsql.replication\_lag.sec, pgsql.uptime, pgsql.wal.stat |To configure encrypted connections to the database, specify the TLS parameters in the agent configuration file as [named session](#named_sessions) or [default](#default-values) parameters.<br>Currently, TLS parameters cannot be passed as item key parameters.<br><br>See also [PostgreSQL plugin configuration parameters](/manual/appendix/config/zabbix_agent2_plugins/postgresql_plugin#parameters).|

See also: [Building loadable plugins](/manual/extensions/plugins/build).

[comment]: # ({/aa51ee2c-9ecbdda6})
