<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="en" datatype="plaintext" original="manual/it_services/service_tree.md">
    <body>
      <trans-unit id="f9e8f670" xml:space="preserve">
        <source># 1 Service tree

Service tree is configured in the *Services-&gt;Services* menu section.  In the upper right corner, switch from [View](/manual/web_interface/frontend_sections/services/service) to the Edit mode. 

![](../../../assets/en/manual/config/service_config.png){width=600}


To [configure](#service_configuration) a new service, click on the
*Create service* button in the top right-hand corner.

To quickly add a child service, you can alternatively press a plus icon next to the parent service. This will open the same service configuration form, but the Parent services parameter will be pre-filled. </source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/it_services/service_tree.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="6faf3b84" xml:space="preserve">
        <source>### Service configuration

In the **Service** tab, specify required service parameters:

![](../../../assets/en/manual/web_interface/service.png){width=600}

All mandatory input fields are marked with a red asterisk.

|Parameter|Description|
|--|--------|
|_**Name**_|Service name.|
|_**Parent services**_|Parent services the service belongs to.&lt;br&gt; Leave this field empty if you are adding the service of highest level.&lt;br&gt; One service may have multiple parent services. In this case, it will be displayed in the service tree under each of the parent services.  |
|_**Problem tags**_|Specify tags to map problem data to the service:&lt;br&gt;**Equals** - include the specified tag names and values (case-sensitive)&lt;br&gt;**Contains** - include the specified tag names where the tag values contain the entered string (substring match, case-insensitive)&lt;br&gt;Tag name matching is always case-sensitive.|
|_**Sort order**_|Sort order for display, lowest comes first.|
|_**Status calculation rule**_|Rule for calculating service status:&lt;br&gt;**Most critical if all children have problems** - the most critical problem in the child services is used to color the service status, if all children have problems&lt;br&gt;**Most critical of child services** - the most critical problem in the child services is used to color the service status&lt;br&gt;**Set status to OK** - do not calculate service status&lt;br&gt;Additional status calculation rules can be configured in the [advanced configuration](#advanced-configuration) options.|
|_**Description**_|Service description.|
|_**Created at**_|The time when the service was created; displayed when editing an existing service.|
|_**Advanced configuration**_|Click on the *Advanced configuration* label to display [advanced configuration](#advanced-configuration) options.|</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/it_services/service_tree.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="23bc31ba" xml:space="preserve">
        <source>
#### Advanced configuration

![](../../../assets/en/manual/web_interface/service_a.png){width=600}

|Parameter|Description|
|--|--------|
|_**Additional rules**_|Click on *Add* to configure additional status calculation rules.|
|*Set status to*|Set service status to either *OK* (default), *Not classified*, *Information*, *Warning*, *Average*, *High* or *Disaster* in case of a condition match.|
|*Condition*|Select the condition for direct child services:&lt;br&gt;**if at least (N) child services have (Status) status or above**&lt;br&gt;**if at least (N%) of child services have (Status) status or above**&lt;br&gt;**if less than (N) child services have (Status) status or below**&lt;br&gt;**if less than (N%) of child services have (Status) status or below**&lt;br&gt;**if weight of child services with (Status) status or above is at least (W)**&lt;br&gt;**if weight of child services with (Status) status or above is at least (N%)**&lt;br&gt;**if weight of child services with (Status) status or below is less than (W)**&lt;br&gt;**if weight of child services with (Status) status or below is less than (N%)** &lt;br&gt;&lt;br&gt; If several conditions are specified and the situation matches more than one condition, the highest severity will be set.|
|*N (W)*|Set the value of N or W (1-100000), or N% (1-100) in the condition.|
|*Status*|Select the value of *Status* in the condition: *OK* (default), *Not classified*, *Information*, *Warning*, *Average*, *High* or *Disaster*.|
|_**Status propagation rule**_|Rule for propagating the service status to the parent service:&lt;br&gt;**As is** - the status is propagated without change&lt;br&gt;**Increase by** - you may increase the propagated status by 1 to 5 severities&lt;br&gt;**Decrease by** - you may decrease the propagated status by 1 to 5 severities&lt;br&gt;**Ignore this service** - the status is not propagated to the parent service at all&lt;br&gt;**Fixed status** - the status is propagated statically, i.e. as always the same|
|_**Weight**_|Weight of the service (integer in the range from 0 (default) to 1000000).|

:::noteclassic
Additional status calculation rules can only be used to increase severity level over the level calculated according to the main *Status calculation rule* parameter. If according to additional rules the status should be Warning, but according to the *Status calculation rule* the status is Disaster - the service will have status Disaster. 
:::</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/it_services/service_tree.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="eb22558f" xml:space="preserve">
        <source>
The **Tags** tab contains [service-level tags](#service-tags). Service-level tags are 
used to identify a service. Tags of this type are not used to map 
problems to the service (for that, use *[Problem tags](#problem-tags)* from the first tab).

The **Child services** tab allows to specify dependant services.
Click on *Add* to add a service from the list of existing services. If you want to add a new child service, save this service first, then click on a plus icon next to the service that you have just created. </source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/it_services/service_tree.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="cbeb5931" xml:space="preserve">
        <source>
### Tags
There are two different types of tags in services:

 - Service tags
 - Problem tags

#### Service tags

Service tags are used to match services with [service actions](/manual/config/notifications/action) and [SLAs](/manual/it_services/sla). These tags are specified at the *Tabs* service configuration tab. 
For mapping SLAs *OR* logic is used: a service will be mapped to an SLA if it has at least one matching tag.
In service actions, mapping rules are configurable and can use either *AND*, *OR*, or *AND/OR* logic. 

![](../../../assets/en/manual/config/service_tags.png){width=600}</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/it_services/service_tree.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="b5d55da8" xml:space="preserve">
        <source>
#### Problem tags

Problem tags are used to match problems and services. These tags are specified at the primary service configuration tab. 

Only child services of the lowest hierarchy level may have problem tags defined and be directly correlated to problems. If problem tags match, 
the service status will change to the same status as the problem has. In case of several problems, a service will have the status of the most severe one. Status of a 
parent service is then calculated based on child services statuses according to Status calculation rules. 

If several tags are specified, *AND* logic is used: a problem must have all tags 
specified in the service configuration to be mapped to the service. 

![](../../../assets/en/manual/config/problem_tags.png){width=600}


:::noteclassic
A problem in Zabbix inherits tags from the whole chain of templates, hosts, items, web scenarios, and triggers. Any of these tags can be used for matching problems to services.
::: </source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/it_services/service_tree.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="637a1377" xml:space="preserve">
        <source>
*Example:*

Problem *Web camera 3 is down* has tags `type:video surveillance`, `floor:1st` and `name:webcam 3` and status *Warning*

The service **Web camera 3** has the only problem tag specified: `name:webcam 3`  

   ![](../../../assets/en/manual/config/services_example_tags.png){width=600}
   
Service status will change from *OK* to *Warning* when this problem is detected. 

If the service **Web camera 3** had problem tags `name:webcam 3` and `floor:2nd`, its status would not be changed, when the problem is detected, because the conditions are only partially met. </source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/it_services/service_tree.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
    </body>
  </file>
</xliff>
