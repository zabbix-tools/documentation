[comment]: # ({545b357e-f9e8f670})
# 1 Service tree

Service tree is configured in the *Services->Services* menu section.  In the upper right corner, switch from [View](/manual/web_interface/frontend_sections/services/service) to the Edit mode. 

![](../../../assets/en/manual/config/service_config.png){width=600}


To [configure](#service_configuration) a new service, click on the
*Create service* button in the top right-hand corner.

To quickly add a child service, you can alternatively press a plus icon next to the parent service. This will open the same service configuration form, but the Parent services parameter will be pre-filled. 

[comment]: # ({/545b357e-f9e8f670})

[comment]: # ({5cd624ac-6faf3b84})
### Service configuration

In the **Service** tab, specify required service parameters:

![](../../../assets/en/manual/web_interface/service.png){width=600}

All mandatory input fields are marked with a red asterisk.

|Parameter|Description|
|--|--------|
|_**Name**_|Service name.|
|_**Parent services**_|Parent services the service belongs to.<br> Leave this field empty if you are adding the service of highest level.<br> One service may have multiple parent services. In this case, it will be displayed in the service tree under each of the parent services.  |
|_**Problem tags**_|Specify tags to map problem data to the service:<br>**Equals** - include the specified tag names and values (case-sensitive)<br>**Contains** - include the specified tag names where the tag values contain the entered string (substring match, case-insensitive)<br>Tag name matching is always case-sensitive.|
|_**Sort order**_|Sort order for display, lowest comes first.|
|_**Status calculation rule**_|Rule for calculating service status:<br>**Most critical if all children have problems** - the most critical problem in the child services is used to color the service status, if all children have problems<br>**Most critical of child services** - the most critical problem in the child services is used to color the service status<br>**Set status to OK** - do not calculate service status<br>Additional status calculation rules can be configured in the [advanced configuration](#advanced-configuration) options.|
|_**Description**_|Service description.|
|_**Created at**_|The time when the service was created; displayed when editing an existing service.|
|_**Advanced configuration**_|Click on the *Advanced configuration* label to display [advanced configuration](#advanced-configuration) options.|

[comment]: # ({/5cd624ac-6faf3b84})

[comment]: # ({5c73872f-23bc31ba})

#### Advanced configuration

![](../../../assets/en/manual/web_interface/service_a.png){width=600}

|Parameter|Description|
|--|--------|
|_**Additional rules**_|Click on *Add* to configure additional status calculation rules.|
|*Set status to*|Set service status to either *OK* (default), *Not classified*, *Information*, *Warning*, *Average*, *High* or *Disaster* in case of a condition match.|
|*Condition*|Select the condition for direct child services:<br>**if at least (N) child services have (Status) status or above**<br>**if at least (N%) of child services have (Status) status or above**<br>**if less than (N) child services have (Status) status or below**<br>**if less than (N%) of child services have (Status) status or below**<br>**if weight of child services with (Status) status or above is at least (W)**<br>**if weight of child services with (Status) status or above is at least (N%)**<br>**if weight of child services with (Status) status or below is less than (W)**<br>**if weight of child services with (Status) status or below is less than (N%)** <br><br> If several conditions are specified and the situation matches more than one condition, the highest severity will be set.|
|*N (W)*|Set the value of N or W (1-100000), or N% (1-100) in the condition.|
|*Status*|Select the value of *Status* in the condition: *OK* (default), *Not classified*, *Information*, *Warning*, *Average*, *High* or *Disaster*.|
|_**Status propagation rule**_|Rule for propagating the service status to the parent service:<br>**As is** - the status is propagated without change<br>**Increase by** - you may increase the propagated status by 1 to 5 severities<br>**Decrease by** - you may decrease the propagated status by 1 to 5 severities<br>**Ignore this service** - the status is not propagated to the parent service at all<br>**Fixed status** - the status is propagated statically, i.e. as always the same|
|_**Weight**_|Weight of the service (integer in the range from 0 (default) to 1000000).|

:::noteclassic
Additional status calculation rules can only be used to increase severity level over the level calculated according to the main *Status calculation rule* parameter. If according to additional rules the status should be Warning, but according to the *Status calculation rule* the status is Disaster - the service will have status Disaster. 
:::

[comment]: # ({/5c73872f-23bc31ba})

[comment]: # ({c613d9ab-eb22558f})

The **Tags** tab contains [service-level tags](#service-tags). Service-level tags are 
used to identify a service. Tags of this type are not used to map 
problems to the service (for that, use *[Problem tags](#problem-tags)* from the first tab).

The **Child services** tab allows to specify dependant services.
Click on *Add* to add a service from the list of existing services. If you want to add a new child service, save this service first, then click on a plus icon next to the service that you have just created. 

[comment]: # ({/c613d9ab-eb22558f})

[comment]: # ({e137addb-cbeb5931})

### Tags
There are two different types of tags in services:

 - Service tags
 - Problem tags

#### Service tags

Service tags are used to match services with [service actions](/manual/config/notifications/action) and [SLAs](/manual/it_services/sla). These tags are specified at the *Tabs* service configuration tab. 
For mapping SLAs *OR* logic is used: a service will be mapped to an SLA if it has at least one matching tag.
In service actions, mapping rules are configurable and can use either *AND*, *OR*, or *AND/OR* logic. 

![](../../../assets/en/manual/config/service_tags.png){width=600}

[comment]: # ({/e137addb-cbeb5931})

[comment]: # ({4ec2c862-b5d55da8})

#### Problem tags

Problem tags are used to match problems and services. These tags are specified at the primary service configuration tab. 

Only child services of the lowest hierarchy level may have problem tags defined and be directly correlated to problems. If problem tags match, 
the service status will change to the same status as the problem has. In case of several problems, a service will have the status of the most severe one. Status of a 
parent service is then calculated based on child services statuses according to Status calculation rules. 

If several tags are specified, *AND* logic is used: a problem must have all tags 
specified in the service configuration to be mapped to the service. 

![](../../../assets/en/manual/config/problem_tags.png){width=600}


:::noteclassic
A problem in Zabbix inherits tags from the whole chain of templates, hosts, items, web scenarios, and triggers. Any of these tags can be used for matching problems to services.
::: 

[comment]: # ({/4ec2c862-b5d55da8})

[comment]: # ({2a4b7326-637a1377})

*Example:*

Problem *Web camera 3 is down* has tags `type:video surveillance`, `floor:1st` and `name:webcam 3` and status *Warning*

The service **Web camera 3** has the only problem tag specified: `name:webcam 3`  

   ![](../../../assets/en/manual/config/services_example_tags.png){width=600}
   
Service status will change from *OK* to *Warning* when this problem is detected. 

If the service **Web camera 3** had problem tags `name:webcam 3` and `floor:2nd`, its status would not be changed, when the problem is detected, because the conditions are only partially met. 

[comment]: # ({/2a4b7326-637a1377})

### Modifying existing services

:::noteclassic
The buttons described below are visible only when *Services* section is in the Edit mode.
:::

To edit an existing service, press the pencil icon next to the service. 

To clone an existing service, press the pencil icon to open its configuration and then press Clone button. When a service is cloned, its parent links are preserved, while the child links are not.

To delete a service, press on the `x` icon next to it. When you delete a parent service, its child services will not be deleted and will move one level higher in the service tree (1st level children will get the same level as the deleted parent service). 

Two buttons below the list of services offer some mass-editing options:

-   *Mass update* - mass update service properties
-   *Delete* - delete the services

To use these options, mark the checkboxes before the respective
services, then click on the required button.
