[comment]: # ({7e34cab5-b1cfef1b})
# 3 Graph prototypes

We can create graph prototypes, too:

![](../../../../assets/en/manual/discovery/low_level_discovery/graph_prototype_fs.png)

Attributes that are specific for graph prototypes:

|Parameter|Description|
|--|--------|
|*Discover*|If checked (default) the graph will be added to a discovered entity.<br>If unchecked, the graph will not be added to a discovered entity, unless this setting is [overridden](/manual/discovery/low_level_discovery#override) in the discovery rule.|

![](../../../../assets/en/manual/discovery/low_level_discovery/graph_prototypes_fs.png)

Finally, we have created a discovery rule that looks as shown below. It
has five item prototypes, two trigger prototypes, and one graph
prototype.

![](../../../../assets/en/manual/discovery/low_level_discovery/lld_rules_fs.png)


[comment]: # ({/7e34cab5-b1cfef1b})
