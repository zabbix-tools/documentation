[comment]: # ({f9c9612b-54ecc26e})
# 5 Notes on low-level discovery

[comment]: # ({/f9c9612b-54ecc26e})

[comment]: # ({40e2ce9f-40e2ce9f})
#### Using LLD macros in user macro contexts

LLD macros may be used inside user macro context, for example, [in
trigger
prototypes](/manual/config/macros/user_macros_context#use_cases).

[comment]: # ({/40e2ce9f-40e2ce9f})

[comment]: # ({071e9701-071e9701})
#### Multiple LLD rules for the same item

Since Zabbix agent version 3.2 it is possible to define several
low-level discovery rules with the same discovery item.

To do that you need to define the Alias agent
[parameter](/manual/appendix/config/zabbix_agentd), allowing to use
altered discovery item keys in different discovery rules, for example
`vfs.fs.discovery[foo]`, `vfs.fs.discovery[bar]`, etc.

[comment]: # ({/071e9701-071e9701})

[comment]: # ({a8d6d602-a8d6d602})
#### Data limits for return values

There is no limit for low-level discovery rule JSON data if it is
received directly by Zabbix server, because return values are processed
without being stored in a database. There's also no limit for custom
low-level discovery rules, however, if it is intended to acquire custom
LLD data using a user parameter, then the user parameter return value
limit applies (512 KB).

If data has to go through Zabbix proxy it has to store this data in
database so [database
limits](/manual/config/items/item#text_data_limits) apply.

[comment]: # ({/a8d6d602-a8d6d602})
