[comment]: # ({a4d380ae-b42a3fab})
# 9 Discovery of Windows services

[comment]: # ({/a4d380ae-b42a3fab})

[comment]: # ({b48a59b0-b48a59b0})
#### Overview

In a similar way as [file
systems](/manual/discovery/low_level_discovery#configuring_low-level_discovery)
are discovered, it is possible to also discover Windows services.

[comment]: # ({/b48a59b0-b48a59b0})

[comment]: # ({ba2093da-ba2093da})
#### Item key

The item to use in the [discovery
rule](/manual/discovery/low_level_discovery#discovery_rule) is

    service.discovery

This item is supported since Zabbix Windows agent 3.0.

[comment]: # ({/ba2093da-ba2093da})

[comment]: # ({058eb654-058eb654})
#### Supported macros

The following macros are supported for use in the discovery rule
[filter](/manual/discovery/low_level_discovery#discovery_rule_filter)
and prototypes of items, triggers and graphs:

|Macro|Description|
|-----|-----------|
|{\#SERVICE.NAME}|Service name.|
|{\#SERVICE.DISPLAYNAME}|Displayed service name.|
|{\#SERVICE.DESCRIPTION}|Service description.|
|{\#SERVICE.STATE}|Numerical value of the service state:<br>0 - Running<br>1 - Paused<br>2 - Start pending<br>3 - Pause pending<br>4 - Continue pending<br>5 - Stop pending<br>6 - Stopped<br>7 - Unknown|
|{\#SERVICE.STATENAME}|Name of the service state (*Running*, *Paused*, *Start pending*, *Pause pending*, *Continue pending*, *Stop pending*, *Stopped* or *Unknown*).|
|{\#SERVICE.PATH}|Service path.|
|{\#SERVICE.USER}|Service user.|
|{\#SERVICE.STARTUP}|Numerical value of the service startup type:<br>0 - Automatic<br>1 - Automatic delayed<br>2 - Manual<br>3 - Disabled<br>4 - Unknown|
|{\#SERVICE.STARTUPNAME}|Name of the service startup type (*Automatic*, *Automatic delayed*, *Manual*, *Disabled*, *Unknown*).|
|{\#SERVICE.STARTUPTRIGGER}|Numerical value to indicate if the service startup type has:<br>0 - no startup triggers<br>1 - has startup triggers<br>This macro is supported since Zabbix 3.4.4. It is useful to discover such service startup types as *Automatic (trigger start)*, *Automatic delayed (trigger start)* and *Manual (trigger start).*<br>|

Based on Windows service discovery you may create an
[item](/manual/config/items/itemtypes/zabbix_agent/win_keys) prototype
like

    service.info[{#SERVICE.NAME},<param>]

where `param` accepts the following values: *state*, *displayname*,
*path*, *user*, *startup* or *description*.

For example, to acquire the display name of a service you may use a
"service.info\[{\#SERVICE.NAME},displayname\]" item. If `param` value is
not specified ("service.info\[{\#SERVICE.NAME}\]"), the default *state*
parameter is used.

[comment]: # ({/058eb654-058eb654})
