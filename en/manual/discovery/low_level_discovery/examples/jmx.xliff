<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="en" datatype="plaintext" original="manual/discovery/low_level_discovery/examples/jmx.md">
    <body>
      <trans-unit id="386ff732" xml:space="preserve">
        <source># 6 Discovery of JMX objects</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/discovery/low_level_discovery/examples/jmx.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="c07df864" xml:space="preserve">
        <source>#### Overview

It is possible to [discover](/manual/discovery/low_level_discovery) all
JMX MBeans or MBean attributes or to specify a pattern for the discovery
of these objects.

It is mandatory to understand the difference between an MBean and MBean
attributes for discovery rule configuration. An MBean is an object which
can represent a device, an application, or any resource that needs to be
managed.

For example, there is an MBean which represents a web server. Its
attributes are connection count, thread count, request timeout, http
file cache, memory usage, etc. Expressing this thought in human
comprehensive language we can define a coffee machine as an MBean which
has the following attributes to be monitored: water amount per cup,
average consumption of water for a certain period of time, number of
coffee beans required per cup, coffee beans and water refill time, etc.</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/discovery/low_level_discovery/examples/jmx.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="ccae3f99" xml:space="preserve">
        <source>#### Item key

In [discovery
rule](/manual/discovery/low_level_discovery#discovery_rule)
configuration, select **JMX agent** in the *Type* field.

Two item keys are supported for JMX object discovery - jmx.discovery\[\]
and jmx.get\[\]:

|**Item key**|&lt;|&lt;|&lt;|
|------------|-|-|-|
|&lt;|Return value|Parameters|Comment|
|**jmx.discovery**\[&lt;discovery mode&gt;,&lt;object name&gt;,&lt;unique short description&gt;\]|&lt;|&lt;|&lt;|
|&lt;|This item returns a JSON array with LLD macros describing MBean objects or their attributes.|**discovery mode** - one of the following: *attributes* (retrieve JMX MBean attributes, default) or *beans* (retrieve JMX MBeans)&lt;br&gt;**object name** - object name pattern (see [documentation](https://docs.oracle.com/javase/7/docs/api/javax/management/ObjectName.html)) identifying the MBean names to be retrieved (empty by default, retrieving all registered beans)&lt;br&gt;**unique short description** - a unique description that allows multiple JMX items with the same discovery mode and object name on the host (optional)|Examples:&lt;br&gt;→ jmx.discovery - retrieve all JMX MBean attributes&lt;br&gt;→ jmx.discovery\[beans\] - retrieve all JMX MBeans&lt;br&gt;→ jmx.discovery\[attributes,"\*:type=GarbageCollector,name=\*"\] - retrieve all garbage collector attributes&lt;br&gt;→ jmx.discovery\[beans,"\*:type=GarbageCollector,name=\*"\] - retrieve all garbage collectors&lt;br&gt;&lt;br&gt;There are some [limitations](#limitations) to what MBean properties this item can return based on limited characters that are supported in macro name generation (supported characters can be described by the following regular expression: `A-Z0-9_\.`). So, for example, to discover MBean properties with a hyphenated word or non-ASCII characters, you need to use `jmx.get[]`.&lt;br&gt;&lt;br&gt;Supported since Zabbix Java gateway 3.4.|
|**jmx.get**\[&lt;discovery mode&gt;,&lt;object name&gt;,&lt;unique short description&gt;\]|&lt;|&lt;|&lt;|
|&lt;|This item returns a JSON array with MBean objects or their attributes.&lt;br&gt;&lt;br&gt;Compared to `jmx.discovery[]` it does not define LLD macros.|**discovery mode** - one of the following: *attributes* (retrieve JMX MBean attributes, default) or *beans* (retrieve JMX MBeans)&lt;br&gt;**object name** - object name pattern (see [documentation](https://docs.oracle.com/javase/7/docs/api/javax/management/ObjectName.html)) identifying the MBean names to be retrieved (empty by default, retrieving all registered beans)&lt;br&gt;**unique short description** - a unique description that allows multiple JMX items with the same discovery mode and object name on the host (optional)|When using this item, it is needed to define custom low-level discovery macros, pointing to values extracted from the returned JSON using JSONPath.&lt;br&gt;&lt;br&gt;Supported since Zabbix Java gateway 4.4.|

::: noteimportant
If no parameters are passed, all MBean attributes
from JMX are requested. Not specifying parameters for JMX discovery or
trying to receive all attributes for a wide range like `*:type=*,name=*`
may lead to potential performance problems.
:::</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/discovery/low_level_discovery/examples/jmx.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="dbdbb9bc" xml:space="preserve">
        <source>#### Using jmx.discovery

This item returns a JSON object with low-level discovery macros
describing MBean objects or attributes. For example, in the discovery of
MBean attributes (reformatted for clarity):

``` {.javascript}
[
    {
        "{#JMXVALUE}":"0",
        "{#JMXTYPE}":"java.lang.Long",
        "{#JMXOBJ}":"java.lang:type=GarbageCollector,name=PS Scavenge",
        "{#JMXDESC}":"java.lang:type=GarbageCollector,name=PS Scavenge,CollectionCount",
        "{#JMXATTR}":"CollectionCount"
    },
    {
        "{#JMXVALUE}":"0",
        "{#JMXTYPE}":"java.lang.Long",
        "{#JMXOBJ}":"java.lang:type=GarbageCollector,name=PS Scavenge",
        "{#JMXDESC}":"java.lang:type=GarbageCollector,name=PS Scavenge,CollectionTime",
        "{#JMXATTR}":"CollectionTime"
    },
    {
        "{#JMXVALUE}":"true",
        "{#JMXTYPE}":"java.lang.Boolean",
        "{#JMXOBJ}":"java.lang:type=GarbageCollector,name=PS Scavenge",
        "{#JMXDESC}":"java.lang:type=GarbageCollector,name=PS Scavenge,Valid",
        "{#JMXATTR}":"Valid"
    },
    {
        "{#JMXVALUE}":"PS Scavenge",
        "{#JMXTYPE}":"java.lang.String",
        "{#JMXOBJ}":"java.lang:type=GarbageCollector,name=PS Scavenge",
        "{#JMXDESC}":"java.lang:type=GarbageCollector,name=PS Scavenge,Name",
        "{#JMXATTR}":"Name"
    },
    {
        "{#JMXVALUE}":"java.lang:type=GarbageCollector,name=PS Scavenge",
        "{#JMXTYPE}":"javax.management.ObjectName",
        "{#JMXOBJ}":"java.lang:type=GarbageCollector,name=PS Scavenge",
        "{#JMXDESC}":"java.lang:type=GarbageCollector,name=PS Scavenge,ObjectName",
        "{#JMXATTR}":"ObjectName"
    }
]
```

In the discovery of MBeans (reformatted for clarity):

``` {.javascript}
[
    {
        "{#JMXDOMAIN}":"java.lang",
        "{#JMXTYPE}":"GarbageCollector",
        "{#JMXOBJ}":"java.lang:type=GarbageCollector,name=PS Scavenge",
        "{#JMXNAME}":"PS Scavenge"
    }
]

```</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/discovery/low_level_discovery/examples/jmx.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="41c7d097" xml:space="preserve">
        <source>##### Supported macros

The following macros are supported for use in the discovery rule
[filter](/manual/discovery/low_level_discovery#discovery_rule_filter)
and prototypes of items, triggers and graphs:

|Macro|Description|
|-----|-----------|
|Discovery of MBean attributes|&lt;|
|{\#JMXVALUE}|Attribute value.|
|{\#JMXTYPE}|Attribute type.|
|{\#JMXOBJ}|Object name.|
|{\#JMXDESC}|Object name including attribute name.|
|{\#JMXATTR}|Attribute name.|
|Discovery of MBeans|&lt;|
|{\#JMXDOMAIN}|MBean domain. (*Zabbix reserved name*)|
|{\#JMXOBJ}|Object name. (*Zabbix reserved name*)|
|{\#JMX&lt;key property&gt;}|MBean properties (like {\#JMXTYPE}, {\#JMXNAME}) (see [Limitations](#limitations) below).|</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/discovery/low_level_discovery/examples/jmx.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="8c9889ad" xml:space="preserve">
        <source>##### Limitations

There are some limitations associated with the algorithm of creating LLD
macro names from MBean property names:

-   attribute names are changed to uppercase
-   attribute names are ignored (no LLD macros are generated) if they
    consist of unsupported characters for LLD macro names. Supported
    characters can be described by the following regular expression:
    `A-Z0-9_\.`
-   if an attribute is called "obj" or "domain" they will be ignored
    because of the overlap with the values of the reserved Zabbix
    properties {\#JMXOBJ} and {\#JMXDOMAIN} (supported since Zabbix
    3.4.3.)

Please consider this jmx.discovery (with "beans" mode) example. MBean
has the following properties defined:

    name=test
    тип=Type
    attributes []=1,2,3
    Name=NameOfTheTest
    domAin=some

As a result of JMX discovery, the following LLD macros will be
generated:

-   {\#JMXDOMAIN} - Zabbix internal, describing the domain of MBean
-   {\#JMXOBJ} - Zabbix internal, describing MBean object
-   {\#JMXNAME} - created from "name" property

Ignored properties are:

-   тип : its name contains unsupported characters (non-ASCII)
-   attributes\[\] : its name contains unsupported characters (square
    brackets are not supported)
-   Name : it's already defined (name=test)
-   domAin : it's a Zabbix reserved name</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/discovery/low_level_discovery/examples/jmx.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="905ac1fe" xml:space="preserve">
        <source>##### Examples

Let’s review two more practical examples of a LLD rule creation with the
use of Mbean. To understand the difference between a LLD rule collecting
Mbeans and a LLD rule collecting Mbean attributes better please take a
look at following table:

|   |   |   |
|---|---|---|
|**MBean1**|**MBean2**|**MBean3**|
|MBean1Attribute1|MBean2Attribute1|MBean3Attribute1|
|MBean1Attribute2|MBean2Attribute2|MBean3Attribute2|
|MBean1Attribute3|MBean2Attribute3|MBean3Attribute3|</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/discovery/low_level_discovery/examples/jmx.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="f7377f35" xml:space="preserve">
        <source>##### Example 1: Discovering Mbeans

This rule will return 3 objects: the top row of the column: MBean1,
MBean2, MBean3.

For more information about objects please refer to [supported
macros](/manual/discovery/low_level_discovery/examples/jmx#supported_macros)
table, *Discovery of MBeans* section.

Discovery rule configuration collecting Mbeans (without the attributes)
looks like the following:

![lld\_rule\_mbean.png](../../../../../assets/en/manual/discovery/low_level_discovery/lld_rule_mbean.png)

The key used here:

    jmx.discovery[beans,"*:type=GarbageCollector,name=*"]

All the garbage collectors without attributes will be discovered. As
Garbage collectors have the same attribute set, we can use desired
attributes in item prototypes the following way:

![lld\_rule\_mbean\_prototypes.png](../../../../../assets/en/manual/discovery/low_level_discovery/lld_rule_mbean_prototypes.png)

The keys used here:

    jmx[{#JMXOBJ},CollectionCount] 
    jmx[{#JMXOBJ},CollectionTime] 
    jmx[{#JMXOBJ},Valid] 

LLD discovery rule will result in something close to this (items are
discovered for two Garbage collectors):

![discovery\_rule\_mbean\_3.png](../../../../../assets/en/manual/discovery/low_level_discovery/discovery_rule_mbean_3.png){width="600"}</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/discovery/low_level_discovery/examples/jmx.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="ded9e4d5" xml:space="preserve">
        <source>##### Example 2: Discovering Mbean attributes

This rule will return 9 objects with the following fields:
MBean1Attribute1, MBean2Attribute1,
Mbean3Attribute1,MBean1Attribute2,MBean2Attribute2, Mbean3Attribute2,
MBean1Attribute3, MBean2Attribute3, Mbean3Attribute3.

For more information about objects please refer to [supported
macros](/manual/discovery/low_level_discovery/examples/jmx#supported_macros)
table, *Discovery of MBean attributes* section.

Discovery rule configuration collecting Mbean attributes looks like the
following:

![lld\_rule\_mbean\_attr.png](../../../../../assets/en/manual/discovery/low_level_discovery/lld_rule_mbean_attr.png)

The key used here:

    jmx.discovery[attributes,"*:type=GarbageCollector,name=*"]

All the garbage collectors with a single item attribute will be
discovered.

![lld\_rule\_mbean\_attr\_prototypes.png](../../../../../assets/en/manual/discovery/low_level_discovery/lld_rule_mbean_attr_prototypes.png)

In this particular case an item will be created from prototype for every
MBean attribute. The main drawback of this configuration is that trigger
creation from trigger prototypes is impossible as there is only one item
prototype for all attributes. So this setup can be used for data
collection, but is not recommended for automatic monitoring.</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/discovery/low_level_discovery/examples/jmx.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="6d62dc78" xml:space="preserve">
        <source>#### Using jmx.get

`jmx.get[]` is similar to the `jmx.discovery[]` item, but it does not
turn Java object properties into low-level discovery macro names and
therefore can return values without [limitations](#limitations) that are
associated with LLD macro name generation such as hyphens or non-ASCII
characters.

When using `jmx.get[]` for discovery, low-level discovery macros can be
defined separately in the custom [LLD
macro](/manual/discovery/low_level_discovery#discovery_rule) tab of the
discovery rule configuration, using JSONPath to point to the required
values.</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/discovery/low_level_discovery/examples/jmx.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="b087c327" xml:space="preserve">
        <source>##### Discovering MBeans

Discovery item: `jmx.get[beans,"com.example:type=*,*"]`

Response:

``` {.json}
[
    {
        "object": "com.example:type=Hello,data-src=data-base,ключ=значение",
        "domain": "com.example",
        "properties": {
            "data-src": "data-base",
            "ключ": "значение",
            "type": "Hello"
        }
    },
    {
        "object": "com.example:type=Atomic",
        "domain": "com.example",
        "properties": {
            "type": "Atomic"
        }
    }
]
```</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/discovery/low_level_discovery/examples/jmx.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="cb2449af" xml:space="preserve">
        <source>##### Discovering MBean attributes

Discovery item: `jmx.get[attributes,"com.example:type=*,*"]`

Response:

``` {.json}
[
    {
        "object": "com.example:type=*",
        "domain": "com.example",
        "properties": {
            "type": "Simple"
        }
    },
    {
        "object": "com.zabbix:type=yes,domain=zabbix.com,data-source=/dev/rand,ключ=значение,obj=true",
        "domain": "com.zabbix",
        "properties": {
            "type": "Hello",
            "domain": "com.example",
            "data-source": "/dev/rand",
            "ключ": "значение",
            "obj": true
        }
    }
]
```</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/discovery/low_level_discovery/examples/jmx.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
    </body>
  </file>
</xliff>
