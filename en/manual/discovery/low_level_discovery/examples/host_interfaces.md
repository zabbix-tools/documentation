[comment]: # ({b31dccac-235db3de})
# 15 Discovery of host interfaces in Zabbix

[comment]: # ({/b31dccac-235db3de})

[comment]: # ({f39b74a6-f39b74a6})
#### Overview

It is possible to [discover](/manual/discovery/low_level_discovery) all
interfaces configured in Zabbix frontend for a host.

[comment]: # ({/f39b74a6-f39b74a6})

[comment]: # ({797dbf7c-797dbf7c})
#### Item key

The item to use in the [discovery
rule](/manual/discovery/low_level_discovery#discovery_rule) is the

    zabbix[host,discovery,interfaces]

internal item. This item is supported since Zabbix server 3.4.

This item returns a JSON with the description of interfaces, including:

-   IP address/DNS hostname (depending on the “Connect to” host setting)
-   Port number
-   Interface type (Zabbix agent, SNMP, JMX, IPMI)
-   If it is the default interface or not
-   If the bulk request feature is enabled - for SNMP interfaces only.

For example:

    [{"{#IF.CONN}":"192.168.3.1","{#IF.IP}":"192.168.3.1","{#IF.DNS}":"","{#IF.PORT}":"10050","{#IF.TYPE}":"AGENT","{#IF.DEFAULT}":1}]  

With multiple interfaces their records in JSON are ordered by:

-   Interface type,
-   Default - the default interface is put before non-default
    interfaces,
-   Interface ID (in ascending order).

[comment]: # ({/797dbf7c-797dbf7c})

[comment]: # ({b669a58b-b669a58b})
#### Supported macros

The following macros are supported for use in the discovery rule
[filter](/manual/discovery/low_level_discovery#discovery_rule_filter)
and prototypes of items, triggers and graphs:

|Macro|Description|
|-----|-----------|
|{\#IF.CONN}|Interface IP address or DNS host name.|
|{\#IF.IP}|Interface IP address.|
|{\#IF.DNS}|Interface DNS host name.|
|{\#IF.PORT}|Interface port number.|
|{\#IF.TYPE}|Interface type ("AGENT", "SNMP", "JMX", or "IPMI").|
|{\#IF.DEFAULT}|Default status for the interface:<br>0 - not default interface<br>1 - default interface|
|{\#IF.SNMP.BULK}|SNMP bulk processing status for the interface:<br>0 - disabled<br>1 - enabled<br>This macro is returned only if interface type is “SNMP”.|

[comment]: # ({/b669a58b-b669a58b})
