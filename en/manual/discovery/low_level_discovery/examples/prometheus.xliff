<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="en" datatype="plaintext" original="manual/discovery/low_level_discovery/examples/prometheus.md">
    <body>
      <trans-unit id="f06f3ae9" xml:space="preserve">
        <source># 13 Discovery using Prometheus data</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/discovery/low_level_discovery/examples/prometheus.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="4d2224d9" xml:space="preserve">
        <source>#### Overview

Data provided in Prometheus line format can be used for low-level
discovery.

See [Prometheus checks](/manual/config/items/itemtypes/prometheus) for
details how Prometheus data querying is implemented in Zabbix.</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/discovery/low_level_discovery/examples/prometheus.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="266cbb44" xml:space="preserve">
        <source>#### Configuration

The low-level discovery rule should be created as a [dependent
item](/manual/config/items/itemtypes/dependent_items) to the HTTP master
item that collects Prometheus data.</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/discovery/low_level_discovery/examples/prometheus.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="5a48de63" xml:space="preserve">
        <source>##### Prometheus to JSON

In the discovery rule, go to the Preprocessing tab and select the
*Prometheus to JSON* preprocessing option. Data in JSON format are
needed for discovery and the *Prometheus to JSON* preprocessing option
will return exactly that, with the following attributes:

-   metric name
-   metric value
-   help (if present)
-   type (if present)
-   labels (if present)
-   raw line

For example, querying `wmi_logical_disk_free_bytes`:

![](../../../../../assets/en/manual/discovery/low_level_discovery/lld_rule_prom_json.png)

from these Prometheus lines:

    # HELP wmi_logical_disk_free_bytes Free space in bytes (LogicalDisk.PercentFreeSpace)
    # TYPE wmi_logical_disk_free_bytes gauge
    wmi_logical_disk_free_bytes{volume="C:"} 3.5180249088e+11
    wmi_logical_disk_free_bytes{volume="D:"} 2.627731456e+09
    wmi_logical_disk_free_bytes{volume="HarddiskVolume4"} 4.59276288e+08

will return:

``` {.java}
[
    {
        "name": "wmi_logical_disk_free_bytes",
        "help": "Free space in bytes (LogicalDisk.PercentFreeSpace)",
        "type": "gauge",
        "labels": {
            "volume": "C:"
         },
        "value": "3.5180249088e+11",
        "line_raw": "wmi_logical_disk_free_bytes{volume=\"C:\"} 3.5180249088e+11"
    },
    {
        "name": "wmi_logical_disk_free_bytes",
        "help": "Free space in bytes (LogicalDisk.PercentFreeSpace)",
        "type": "gauge",
        "labels": {
            "volume": "D:"
         },
        "value": "2.627731456e+09",
        "line_raw": "wmi_logical_disk_free_bytes{volume=\"D:\"} 2.627731456e+09"
    },
    {
        "name": "wmi_logical_disk_free_bytes",
        "help": "Free space in bytes (LogicalDisk.PercentFreeSpace)",
        "type": "gauge",
        "labels": {
            "volume": "HarddiskVolume4"
         },
        "value": "4.59276288e+08",
        "line_raw": "wmi_logical_disk_free_bytes{volume=\"HarddiskVolume4\"} 4.59276288e+08"
    }
]
```</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/discovery/low_level_discovery/examples/prometheus.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="38a03356" xml:space="preserve">
        <source>##### Mapping LLD macros

Next you have to go to the LLD macros tab and make the following
mappings:

    {#VOLUME}=$.labels['volume']
    {#METRIC}=$['name']
    {#HELP}=$['help']</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/discovery/low_level_discovery/examples/prometheus.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="b367cc91" xml:space="preserve">
        <source>##### Item prototype

You may want to create an item prototype like this:

![](../../../../../assets/en/manual/discovery/low_level_discovery/lld_item_prototype_prom.png)

with preprocessing options:

![](../../../../../assets/en/manual/discovery/low_level_discovery/lld_item_prototype_prom_b.png)</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/discovery/low_level_discovery/examples/prometheus.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
    </body>
  </file>
</xliff>
