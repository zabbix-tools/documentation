[comment]: # ({2ebf7276-e49a61f3})
# 14 Discovery of block devices

In a similar way as [file
systems](/manual/discovery/low_level_discovery#configuring_low-level_discovery)
are discovered, it is possible to also discover block devices and their
type.

[comment]: # ({/2ebf7276-e49a61f3})

[comment]: # ({77e56d52-77e56d52})
#### Item key

The item key to use in the [discovery
rule](/manual/discovery/low_level_discovery#discovery_rule) is

    vfs.dev.discovery

This item is supported on Linux platforms only, since Zabbix agent 4.4.

You may create discovery rules using this discovery item and:

-   filter: **{\#DEVNAME} matches `sd[\D]$`** - to discover devices
    named "sd0", "sd1", "sd2", ...
-   filter: **{\#DEVTYPE} matches `disk` AND {\#DEVNAME} does not match
    `^loop.*`** - to discover disk type devices whose name does not
    start with "loop"

[comment]: # ({/77e56d52-77e56d52})

[comment]: # ({320f7862-320f7862})
#### Supported macros

This discovery key returns two macros - {\#DEVNAME} and {\#DEVTYPE}
identifying the block device name and type respectively, e.g.:

``` {.java}
[ 
   { 
      "{#DEVNAME}":"loop1",
      "{#DEVTYPE}":"disk"
   },
   { 
      "{#DEVNAME}":"dm-0",
      "{#DEVTYPE}":"disk"
   },
   { 
      "{#DEVNAME}":"sda",
      "{#DEVTYPE}":"disk"
   },
   { 
      "{#DEVNAME}":"sda1",
      "{#DEVTYPE}":"partition"
   }
]
```

Block device discovery allows to use `vfs.dev.read[]` and
`vfs.dev.write[]` items to create item prototypes using the {\#DEVNAME}
macro, for example:

-   "vfs.dev.read\[{\#DEVNAME},sps\]"
-   "vfs.dev.write\[{\#DEVNAME},sps\]"

{\#DEVTYPE} is intended for device filtering.

[comment]: # ({/320f7862-320f7862})
