[comment]: # ({a14b62f1-a14b62f1})
# 3 Low-level discovery

[comment]: # ({/a14b62f1-a14b62f1})

[comment]: # ({e09717b5-4fc0f22c})
### Overview

Low-level discovery provides a way to automatically create items,
triggers, and graphs for different entities on a computer. For instance,
Zabbix can automatically start monitoring file systems or network
interfaces on your machine, without the need to create items for each
file system or network interface manually. Additionally, it is possible
to configure Zabbix to remove unneeded entities automatically based on
the actual results of periodically performed discovery.

A user can define their own types of discovery, provided they follow a
particular JSON protocol.

The general architecture of the discovery process is as follows.

First, a user creates a discovery rule in *Data collection → Templates*, in the
*Discovery* column. A discovery rule consists of (1) an item that
discovers the necessary entities (for instance, file systems or network
interfaces) and (2) prototypes of items, triggers, and graphs that
should be created based on the value of that item.

An item that discovers the necessary entities is like a regular item
seen elsewhere: the server asks a Zabbix agent (or whatever the type of
the item is set to) for a value of that item, the agent responds with a
textual value. The difference is that the value the agent responds with
should contain a list of discovered entities in a JSON format. While the
details of this format are only important for implementers of custom
discovery checks, it is necessary to know that the returned value
contains a list of macro → value pairs. For instance, item
"net.if.discovery" might return two pairs: "{\#IFNAME}" → "lo" and
"{\#IFNAME}" → "eth0".

These macros are used in names, keys and other prototype fields where
they are then substituted with the received values for creating real
items, triggers, graphs or even hosts for each discovered entity. See
the full list of [options](/manual/config/macros/lld_macros) for using
LLD macros.

When the server receives a value for a discovery item, it looks at the
macro → value pairs and for each pair generates real items, triggers,
and graphs, based on their prototypes. In the example with
"net.if.discovery" above, the server would generate one set of items,
triggers, and graphs for the loopback interface "lo", and another set
for interface "eth0".

Note that since **Zabbix 4.2**, the format of the JSON returned by
low-level discovery rules has been changed. It is no longer expected
that the JSON will contain the "data" object. Low-level discovery will
now accept a normal JSON containing an array, in order to support new
features such as the item value preprocessing and custom paths to
low-level discovery macro values in a JSON document.

Built-in discovery keys have been updated to return an array of LLD rows
at the root of JSON document. Zabbix will automatically extract a macro
and value if an array field uses the {\#MACRO} syntax as a key. Any new
native discovery checks will use the new syntax without the "data"
elements. When processing a low-level discovery value first the root is
located (array at `$.` or `$.data`).

While the "data" element has been removed from all native items related
to discovery, for backward compatibility Zabbix will still accept the
JSON notation with a "data" element, though its use is discouraged. If
the JSON contains an object with only one "data" array element, then it
will automatically extract the content of the element using JSONPath
`$.data`. Low-level discovery now accepts optional user-defined LLD
macros with a custom path specified in JSONPath syntax.

::: notewarning
As a result of the changes above, newer agents no
longer will be able to work with an older Zabbix server.
:::

See also: [Discovered entities](#discovered_entities)

[comment]: # ({/e09717b5-4fc0f22c})

[comment]: # ({e452e81c-61b5f21c})
### Configuring low-level discovery

We will illustrate low-level discovery based on an example of file
system discovery.

To configure the discovery, do the following:

-   Go to: *Data collection* → *Templates* or *Hosts*
-   Click on *Discovery* in the row of an appropriate template/host

![](../../../assets/en/manual/discovery/low_level_discovery/fs_templates.png)

-   Click on *Create discovery rule* in the upper right corner of the
    screen
-   Fill in the discovery rule form with the required details

[comment]: # ({/e452e81c-61b5f21c})

[comment]: # ({7375b733-ae1c521e})
#### Discovery rule

The discovery rule form contains five tabs, representing, from left to
right, the data flow during discovery:

-   *Discovery rule* - specifies, most importantly, the built-in item or
    custom script to retrieve discovery data
-   *Preprocessing* - applies some preprocessing to the discovered data
-   *LLD macros* - allows to extract some macro values to use in
    discovered items, triggers, etc
-   *Filters* - allows to filter the discovered values
-   *Overrides* - allows to modify items, triggers, graphs or host
    prototypes when applying to specific discovered objects

The **Discovery rule** tab contains the item key to use for discovery
(as well as some general discovery rule attributes):

![](../../../assets/en/manual/discovery/low_level_discovery/lld_rule_fs.png)

All mandatory input fields are marked with a red asterisk.

|Parameter|Description|
|--|--------|
|*Name*|Name of discovery rule.|
|*Type*|The type of check to perform discovery.<br>In this example we are using a *Zabbix agent* item key.<br>The discovery rule can also be a [dependent item](/manual/config/items/itemtypes/dependent_items), depending on a regular item. It cannot depend on another discovery rule. For a dependent item, select the respective type (*Dependent item*) and specify the master item in the 'Master item' field. The master item must exist.|
|*Key*|Enter the discovery item key (up to 2048 characters).<br>For example, you may use the built-in "vfs.fs.discovery" item key to return a JSON with the list of file systems present on the computer, their types and mount options.<br>Note that another option for filesystem discovery is using discovery results by the "vfs.fs.get" agent key, supported since Zabbix 4.4.5 (see [example](/manual/discovery/low_level_discovery/examples/mounted_filesystems)).|
|*Update interval*|This field specifies how often Zabbix performs discovery. In the beginning, when you are just setting up file system discovery, you might wish to set it to a small interval, but once you know it works you can set it to 30 minutes or more, because file systems usually do not change very often.<br>[Time suffixes](/manual/appendix/suffixes) are supported, e.g. 30s, 1m, 2h, 1d, since Zabbix 3.4.0.<br>[User macros](/manual/config/macros/user_macros) are supported, since Zabbix 3.4.0.<br>*Note*: The update interval can only be set to '0' if custom intervals exist with a non-zero value. If set to '0', and a custom interval (flexible or scheduled) exists with a non-zero value, the item will be polled during the custom interval duration.<br>New discovery rules will be checked within 60 seconds of their creation, unless they have Scheduling or Flexible update interval and the *Update interval* is set to 0.<br>*Note* that for an existing discovery rule the discovery can be performed immediately by pushing the *Execute now* [button](#form_buttons).|
|*Custom intervals*|You can create custom rules for checking the item:<br>**Flexible** - create an exception to the *Update interval* (interval with different frequency)<br>**Scheduling** - create a custom polling schedule.<br>For detailed information see [Custom intervals](/manual/config/items/item/custom_intervals). Scheduling is supported since Zabbix 3.0.0.|
|*Keep lost resources period*|This field allows you to specify the duration for how long the discovered entity will be retained (won't be deleted) once its discovery status becomes "Not discovered anymore" (between 1 hour to 25 years; or "0").<br>[Time suffixes](/manual/appendix/suffixes) are supported, e.g. 2h, 1d, since Zabbix 3.4.0.<br>[User macros](/manual/config/macros/user_macros) are supported, since Zabbix 3.4.0.<br>*Note:* If set to "0", entities will be deleted immediately. Using "0" is not recommended, since just wrongly editing the filter may end up in the entity being deleted with all the historical data.|
|*Description*|Enter a description.|
|*Enabled*|If checked, the rule will be processed.|

::: noteclassic
Discovery rule history is not preserved.
:::

[comment]: # ({/7375b733-ae1c521e})

[comment]: # ({c7dfa178-0e438e85})
#### Preprocessing

The **Preprocessing** tab allows to define transformation rules to apply
to the result of discovery. One or several transformations are possible
in this step. Transformations are executed in the order in which they
are defined. All preprocessing is done by Zabbix server.

See also:

-   [Preprocessing
    details](/manual/config/items/preprocessing/preprocessing_details)
-   [Preprocessing testing](/manual/config/items/preprocessing#testing)

![](../../../assets/en/manual/discovery/low_level_discovery/lld_fs_b.png)


|Type|<|<|
|-|----------|----------------------------------------|
| |*Transformation*|Description|
|Text|<|<|
|<|*Regular expression*|Match the received value to the <pattern> regular expression and replace value with the extracted <output>. The regular expression supports extraction of maximum 10 captured groups with the \\N sequence.<br>Parameters:<br>**pattern** - regular expression<br>**output** - output formatting template. An \\N (where N=1…9) escape sequence is replaced with the Nth matched group. A \\0 escape sequence is replaced with the matched text.<br>If you mark the *Custom on fail* checkbox, it is possible to specify custom error handling options: either to discard the value, set a specified value or set a specified error message.|
|^|*Replace*|Find the search string and replace it with another (or nothing). All occurrences of the search string will be replaced.<br>Parameters:<br>**search string** - the string to find and replace, case-sensitive (required)<br>**replacement** - the string to replace the search string with. The replacement string may also be empty effectively allowing to delete the search string when found.<br>It is possible to use escape sequences to search for or replace line breaks, carriage return, tabs and spaces "\\n \\r \\t \\s"; backslash can be escaped as "\\\\" and escape sequences can be escaped as "\\\\n". Escaping of line breaks, carriage return, tabs is automatically done during low-level discovery.<br>Supported since 5.0.0.|
|Structured data|<|<|
|<|*JSONPath*|Extract value or fragment from JSON data using [JSONPath functionality](/manual/config/items/preprocessing/jsonpath_functionality).<br>If you mark the *Custom on fail* checkbox, the item will not become unsupported in case of failed preprocessing step and it is possible to specify custom error handling options: either to discard the value, set a specified value or set a specified error message.|
|<|*XML XPath*|Extract value or fragment from XML data using XPath functionality.<br>For this option to work, Zabbix server must be compiled with libxml support.<br>Examples:<br>`number(/document/item/value)` will extract `10` from `<document><item><value>10</value></item></document>`<br>`number(/document/item/@attribute)` will extract `10` from `<document><item attribute="10"></item></document>`<br>`/document/item` will extract `<item><value>10</value></item>` from `<document><item><value>10</value></item></document>`<br>Note that namespaces are not supported.<br>Supported since 4.4.0.<br>If you mark the *Custom on fail* checkbox, it is possible to specify custom error handling options: either to discard the value, set a specified value or set a specified error message.|
|<|*CSV to JSON*|Convert CSV file data into JSON format.<br>For more information, see: [CSV to JSON preprocessing](/manual/config/items/preprocessing/csv_to_json#csv_header_processing).<br>Supported since 4.4.0.|
|^|*XML to JSON*|Convert data in XML format to JSON.<br>For more information, see: [Serialization rules](/manual/config/items/preprocessing/javascript/javascript_objects#serialization_rules).<br>If you mark the *Custom on fail* checkbox, it is possible to specify custom error handling options: either to discard the value, set a specified value or set a specified error message.|
|SNMP|<|<|
| |*SNMP walk value*|Extract value by the specified OID/MIB name and apply formatting options:<br>**Unchanged** - return Hex-STRING as unescaped hex string;<br>**UTF-8 from Hex-STRING** - convert Hex-STRING to UTF-8 string;<br>**MAC from Hex-STRING** - convert Hex-STRING to MAC address string (which will have  `' '` replaced by `':'`);<br>**Integer from BITS** - convert the first 8 bytes of a bit string expressed as a sequence of hex characters (e.g. "1A 2B 3C 4D") into a 64-bit unsigned integer. In bit strings longer than 8 bytes, consequent bytes will be ignored.<br>If you mark the *Custom on fail* checkbox, the item will not become unsupported in case of failed preprocessing step and it is possible to specify custom error-handling options: either to discard the value, set a specified value or set a specified error message.|
| |*SNMP walk to JSON*|Convert SNMP values to JSON. Specify a field name in the JSON and the corresponding SNMP OID path. Field values will be populated by values in the specified SNMP OID path.<br>You may use this preprocessing step for [SNMP OID discovery](/manual/discovery/low_level_discovery/examples/snmp_oids_walk).<br>Similar value formatting options as in the *SNMP walk value* step are available.<br>If you mark the *Custom on fail* checkbox, the item will not become unsupported in case of failed preprocessing step and it is possible to specify custom error-handling options: either to discard the value, set a specified value or set a specified error message.|
|Custom scripts|<|<|
|<|*JavaScript*|Enter JavaScript code in the block that appears when clicking in the parameter field or on *Open*.<br>Note that available JavaScript length depends on the [database used](/manual/config/items/item#custom_script_limit).<br>For more information, see: [Javascript preprocessing](/manual/config/items/preprocessing/javascript)|
|Validation|<|<|
|<|*Does not match regular expression*|Specify a regular expression that a value must not match.<br>E.g. `Error:(.*?)\.`<br>If you mark the *Custom on fail* checkbox, it is possible to specify custom error handling options: either to discard the value, set a specified value or set a specified error message.|
|^|*Check for error in JSON*|Check for an application-level error message located at JSONPath. Stop processing if succeeded and message is not empty; otherwise continue processing with the value that was before this preprocessing step. Note that these external service errors are reported to user as is, without adding preprocessing step information.<br>E.g. `$.errors`. If a JSON like `{"errors":"e1"}` is received, the next preprocessing step will not be executed.<br>If you mark the *Custom on fail* checkbox, it is possible to specify custom error handling options: either to discard the value, set a specified value or set a specified error message.|
|^|*Check for error in XML*|Check for an application-level error message located at xpath. Stop processing if succeeded and message is not empty; otherwise continue processing with the value that was before this preprocessing step. Note that these external service errors are reported to user as is, without adding preprocessing step information.<br>No error will be reported in case of failing to parse invalid XML.<br>Supported since 4.4.0.<br>If you mark the *Custom on fail* checkbox, it is possible to specify custom error handling options: either to discard the value, set a specified value or set a specified error message.|
|Throttling|<|<|
|<|*Discard unchanged with heartbeat*|Discard a value if it has not changed within the defined time period (in seconds).<br>Positive integer values are supported to specify the seconds (minimum - 1 second). Time suffixes can be used in this field (e.g. 30s, 1m, 2h, 1d). User macros and low-level discovery macros can be used in this field.<br>Only one throttling option can be specified for a discovery item.<br>E.g. `1m`. If identical text is passed into this rule twice within 60 seconds, it will be discarded.<br>*Note*: Changing item prototypes does not reset throttling. Throttling is reset only when preprocessing steps are changed.|
|Prometheus|<|<|
|<|*Prometheus to JSON*|Convert required Prometheus metrics to JSON.<br>See [Prometheus checks](/manual/config/items/itemtypes/prometheus) for more details.|

Note that if the discovery rule has been applied to the host via
template then the content of this tab is read-only.

[comment]: # ({/c7dfa178-0e438e85})

[comment]: # ({2182dc95-c5db0ca2})
#### Custom macros

The **LLD macros** tab allows to specify custom low-level discovery
macros.

Custom macros are useful in cases when the returned JSON does not have
the required macros already defined. So, for example:

-   The native `vfs.fs.discovery` key for filesystem discovery returns a
    JSON with some pre-defined LLD macros such as {\#FSNAME},
    {\#FSTYPE}. These macros can be used in item, trigger prototypes
    (see subsequent sections of the page) directly; defining custom
    macros is not needed;
-   The `vfs.fs.get` agent item also returns a JSON with [filesystem
    data](/manual/discovery/low_level_discovery/examples/mounted_filesystems),
    but without any pre-defined LLD macros. In this case you may define
    the macros yourself, and map them to the values in the JSON using
    JSONPath:

![](../../../assets/en/manual/discovery/low_level_discovery/lld_fs_c.png)

The extracted values can be used in discovered items, triggers, etc.
Note that values will be extracted from the result of discovery and any
preprocessing steps so far.

|Parameter|Description|
|--|--------|
|*LLD macro*|Name of the low-level discovery macro, using the following syntax: {\#MACRO}.|
|*JSONPath*|Path that is used to extract LLD macro value from a LLD row, using JSONPath syntax.<br>For example, `$.foo` will extract "bar" and "baz" from this JSON: `[{"foo":"bar"}, {"foo":"baz"}]`<br>The values extracted from the returned JSON are used to replace the LLD macros in item, trigger, etc. prototype fields.<br>JSONPath can be specified using the dot notation or the bracket notation. Bracket notation should be used in case of any special characters and Unicode, like `$['unicode + special chars #1']['unicode + special chars #2']`.|

[comment]: # ({/2182dc95-c5db0ca2})

[comment]: # ({c4b2ee5f-db0f5889})
#### Filter

A filter can be used to generate real items, triggers, and graphs only
for entities that match the criteria. The **Filters** tab contains
discovery rule filter definitions allowing to filter discovery values:

![](../../../assets/en/manual/discovery/low_level_discovery/lld_fs_d.png)

|Parameter|Description|
|--|--------|
|*Type of calculation*|The following options for calculating filters are available:<br>**And** - all filters must be passed;<br>**Or** - enough if one filter is passed;<br>**And/Or** - uses *And* with different macro names and *Or* with the same macro name;<br>**Custom expression** - offers the possibility to define a custom calculation of filters. The formula must include all filters in the list. Limited to 255 symbols.|
|*Filters*|The following filter condition operators are available: *matches*, *does not match*, *exists*, *does not exist*.<br>*Matches* and *does not match* operators expect a [Perl Compatible Regular Expression](https://en.wikipedia.org/wiki/Perl_Compatible_Regular_Expressions) (PCRE). For instance, if you are only interested in C:, D:, and E: file systems, you could put {\#FSNAME} into "Macro" and "\^C\|\^D\|\^E" regular expression into "Regular expression" text fields. Filtering is also possible by file system types using {\#FSTYPE} macro (e.g. "\^ext\|\^reiserfs") and by drive types (supported only by Windows agent) using {\#FSDRIVETYPE} macro (e.g., "fixed").<br>You can enter a regular expression or reference a global [regular expression](/manual/regular_expressions) in "Regular expression" field.<br>In order to test a regular expression you can use "grep -E", for example: ````for f in ext2 nfs reiserfs smbfs; do echo $f | grep -E '^ext|^reiserfs' || echo "SKIP: $f"; done```` <br><br>*Exists* and *does not exist* operators allow to filter entities based on the presence or absence of the specified LLD macro in the response.<br>Note that if a macro from the filter is missing in the response, the found entity will be ignored, unless a "does not exist" condition is specified for this macro.<br><br>A warning will be displayed, if the absence of a macro affects the expression result. For example, if {#B} is missing in:<br>{#A} matches 1 and {#B} matches 2 - will give a warning<br>{#A} matches 1 or {#B} matches 2 - no warning|

::: notewarning
A mistake or a typo in the regular expression used
in the LLD rule (for example, an incorrect "File systems for discovery"
regular expression) may cause deletion of thousands of configuration
elements, historical values, and events for many hosts. 
:::

::: noteimportant
Zabbix database in MySQL must be created as
case-sensitive if file system names that differ only by case are to be
discovered correctly.
:::

[comment]: # ({/c4b2ee5f-db0f5889})

[comment]: # ({d8b3449a-8d616544})
#### Override

The **Override** tab allows setting rules to modify the list of item,
trigger, graph and host prototypes or their attributes for discovered
objects that meet given criteria.

![](../../../assets/en/manual/discovery/low_level_discovery/lld_fs_e.png)

Overrides (if any) are displayed in a reorderable drag-and-drop list and
executed in the order in which they are defined.  To configure details
of a new override, click on
![](../../../assets/en/manual/config/add_link.png) in the *Overrides*
block. To edit an existing override, click on the override name. A popup
window will open allowing to edit the override rule details.

![](../../../assets/en/manual/discovery/low_level_discovery/lld_override.png)

All mandatory parameters are marked with red asterisks.

|Parameter|Description|
|--|--------|
|*Name*|A unique (per LLD rule) override name.|
|*If filter matches*|Defines whether next overrides should be processed when filter conditions are met:<br>**Continue overrides** - subsequent overrides will be processed.<br>**Stop processing** - operations from preceding (if any) and this override will be executed, subsequent overrides will be ignored for matched LLD rows.|
|*Filters*|Determines to which discovered entities the override should be applied. Override filters are processed after discovery rule [filters](low_level_discovery#filter) and have the same functionality.|
|*Operations*|Override operations are displayed with these details:<br>**Condition** - an object type (item prototype/trigger prototype/graph prototype/host prototype) and a condition to be met (equals/does not equal/contains/does not contain/matches/does not match)<br>**Action** - links for editing and removing an operation are displayed.|

**Configuring an operation**

To configure details of a new operation, click on
![](../../../assets/en/manual/config/add_link.png) in the Operations
block. To edit an existing operation, click on
![](../../../assets/en/manual/config/edit_link.png) next to the
operation. A popup window where you can edit the operation details will
open.

![](../../../assets/en/manual/discovery/low_level_discovery/lld_override_op.png)

|Parameter|<|<|Description|
|-|-|----------|----------------------------------------|
|*Object*|<|<|Four types of objects are available:<br>Item prototype<br>Trigger prototype<br>Graph prototype<br>Host prototype|
|*Condition*|<|<|Allows filtering entities to which the operation should be applied.|
| |Operator|<|Supported operators:<br>**equals** - apply to this prototype<br>**does not equal** - apply to all prototypes, except this<br>**contains** - apply, if prototype name contains this string<br>**does not contain** - apply, if prototype name does not contain this string<br>**matches** - apply, if prototype name matches regular expression<br>**does not match** - apply, if prototype name does not match regular expression<br>|
|^|Pattern|<|A [regular expression](/manual/regular_expressions) or a string to search for.|
|^|Object: *Item prototype*|<|<|
|^| |*Create enabled*|When the checkbox is marked, the buttons will appear, allowing to override original item prototype settings:<br>*Yes* - the item will be added in an enabled state.<br>*No* - the item will be added to a discovered entity but in a disabled state.|
|^|^|*Discover*|When the checkbox is marked, the buttons will appear, allowing to override original item prototype settings:<br>*Yes* - the item will be added.<br>*No* - the item will not be added.|
|^|^|*Update interval*|When the checkbox is marked, two options will appear, allowing to set different interval for the item:<br>*Delay* - Item update interval. [User macros](/manual/config/macros/user_macros) and [time suffixes](/manual/appendix/suffixes) (e.g. 30s, 1m, 2h, 1d) are supported. Should be set to 0 if *Custom interval* is used.<br>*Custom interval* - click ![](../../../assets/en/manual/config/add_link.png) to specify flexible/scheduling intervals. For detailed information see [Custom intervals](/manual/config/items/item/custom_intervals).|
|^|^|*History storage period*|When the checkbox is marked, the buttons will appear, allowing to set different history storage period for the item:<br>*Do not keep history* - if selected, the history will not be stored.<br>*Storage period* - if selected, an input field for specifying storage period will appear to the right. [User macros](/manual/config/macros/user_macros) and [LLD macros](/manual/config/macros/lld_macros) are supported.|
|^|^|*Trend storage period*|When the checkbox is marked, the buttons will appear, allowing to set different trend storage period for the item:<br>*Do not keep trends* - if selected, the trends will not be stored.<br>*Storage period* - if selected, an input field for specifying storage period will appear to the right. [User macros](/manual/config/macros/user_macros) and [LLD macros](/manual/config/macros/lld_macros) are supported.|
|^|^|*Tags*|When the checkbox is marked, a new block will appear, allowing to specify tag-value pairs.<br>These tags will be appended to the tags specified in the item prototype, even if the tag names match.|
|^|Object: *Trigger prototype*|<|<|
|^| |*Create enabled*|When the checkbox is marked, the buttons will appear, allowing to override original trigger prototype settings:<br>*Yes* - the trigger will be added in an enabled state.<br>*No* - the trigger will be added to a discovered entity, but in a disabled state.|
|^|^|*Discover*|When the checkbox is marked, the buttons will appear, allowing to override original trigger prototype settings:<br>*Yes* - the trigger will be added.<br>*No* - the trigger will not be added.|
|^|^|*Severity*|When the checkbox is marked, trigger severity buttons will appear, allowing to modify trigger severity.|
|^|^|*Tags*|When the checkbox is marked, a new block will appear, allowing to specify tag-value pairs.<br>These tags will be appended to the tags specified in the trigger prototype, even if the tag names match.|
|^|Object: *Graph prototype*|<|<|
|^| |*Discover*|When the checkbox is marked, the buttons will appear, allowing to override original graph prototype settings:<br>*Yes* - the graph will be added.<br>*No* - the graph will not be added.|
|^|Object: *Host prototype*|<|<|
|^| |*Create enabled*|When the checkbox is marked, the buttons will appear, allowing to override original host prototype settings:<br>*Yes* - the host will be created in an enabled state.<br>*No* - the host will be created in a disabled state.|
|^|^|*Discover*|When the checkbox is marked, the buttons will appear, allowing to override original host prototype settings:<br>*Yes* - the host will be discovered.<br>*No* - the host will not be discovered.|
|^|^|*Link templates*|When the checkbox is marked, an input field for specifying templates will appear. Start typing the template name or click on *Select* next to the field and select templates from the list in a popup window.<br>All templates linked to a host prototype will be replaced by templates from this override.|
|^|^|*Tags*|When the checkbox is marked, a new block will appear, allowing to specify tag-value pairs.<br>These tags will be appended to the tags specified in the host prototype, even if the tag names match.|
|^|^|*Host inventory*|When the checkbox is marked, the buttons will appear, allowing to select different inventory [mode](/manual/config/hosts/inventory) for the host prototype:<br>*Disabled* - do not populate host inventory<br>*Manual* - provide details manually<br>*Automated* - auto-fill host inventory data based on collected metrics.|

[comment]: # ({/d8b3449a-8d616544})

[comment]: # ({6d61d4eb-50cefaf6})
#### Form buttons

Buttons at the bottom of the form allow to perform several operations.

|   |   |
|--|--------|
|![](../../../assets/en/manual/config/button_add.png)|Add a discovery rule. This button is only available for new discovery rules.|
|![](../../../assets/en/manual/config/button_update.png)|Update the properties of a discovery rule. This button is only available for existing discovery rules.|
|![](../../../assets/en/manual/config/button_clone.png)|Create another discovery rule based on the properties of the current discovery rule.|
|![](../../../assets/en/manual/config/button_check_now.png)|Perform discovery based on the discovery rule immediately. The discovery rule must already exist. See [more details](/manual/config/items/check_now).<br>*Note* that when performing discovery immediately, configuration cache is not updated, thus the result will not reflect very recent changes to discovery rule configuration.|
|![](../../../assets/en/manual/config/button_delete.png)|Delete the discovery rule.|
|![](../../../assets/en/manual/config/button_cancel.png)|Cancel the editing of discovery rule properties.|

[comment]: # ({/6d61d4eb-50cefaf6})

[comment]: # ({0dea10fa-0dea10fa})
### Discovered entities

The screenshots below illustrate how discovered items, triggers, and
graphs look like in the host's configuration. Discovered entities are
prefixed with an orange link to a discovery rule they come from.

![](../../../assets/en/manual/discovery/low_level_discovery/discovered_items1.png)

Note that discovered entities will not be created in case there are
already existing entities with the same uniqueness criteria, for
example, an item with the same key or graph with the same name. An error
message is displayed in this case in the frontend that the low-level
discovery rule could not create certain entities. The discovery rule
itself, however, will not turn unsupported because some entity could not
be created and had to be skipped. The discovery rule will go on
creating/updating other entities.

Items (similarly, triggers and graphs) created by a low-level discovery
rule will be deleted automatically if a discovered entity (file system,
interface, etc) stops being discovered (or does not pass the filter
anymore). In this case the items, triggers and graphs will be deleted
after the days defined in the *Keep lost resources period* field pass.

When discovered entities become 'Not discovered anymore', a lifetime
indicator is displayed in the item list. Move your mouse pointer over it
and a message will be displayed indicating how many days are left until
the item is deleted.

![](../../../assets/en/manual/discovery/low_level_discovery/not_discovered_message.png)

If entities were marked for deletion, but were not deleted at the
expected time (disabled discovery rule or item host), they will be
deleted the next time the discovery rule is processed.

Entities containing other entities, which are marked for deletion, will
not update if changed on the discovery rule level. For example,
LLD-based triggers will not update if they contain items that are marked
for deletion.

![](../../../assets/en/manual/discovery/low_level_discovery/discovered_triggers1.png)

![](../../../assets/en/manual/discovery/low_level_discovery/discovered_graphs1.png)

[comment]: # ({/0dea10fa-0dea10fa})

[comment]: # ({ab744aed-ab744aed})
### Other types of discovery

More detail and how-tos on other types of out-of-the-box discovery is
available in the following sections:

-   discovery of [network
    interfaces](/manual/discovery/low_level_discovery/examples/network_interfaces);
-   discovery of [CPUs and CPU
    cores](/manual/discovery/low_level_discovery/examples/cpu);
-   discovery of [SNMP
    OIDs](/manual/discovery/low_level_discovery/examples/snmp_oids);
-   discovery of [JMX
    objects](/manual/discovery/low_level_discovery/examples/jmx);
-   discovery using [ODBC SQL
    queries](/manual/discovery/low_level_discovery/examples/sql_queries);
-   discovery of [Windows
    services](/manual/discovery/low_level_discovery/examples/windows_services);
-   discovery of [host
    interfaces](/manual/discovery/low_level_discovery/examples/host_interfaces)
    in Zabbix.

For more detail on the JSON format for discovery items and an example of
how to implement your own file system discoverer as a Perl script, see
[creating custom LLD rules](#creating_custom_lld_rules).

[comment]: # ({/ab744aed-ab744aed})

[comment]: # ({0a7ce1b3-554f9404})
### Creating custom LLD rules

It is also possible to create a completely custom LLD rule, discovering
any type of entities - for example, databases on a database server.

To do so, a custom item should be created that returns JSON, specifying
found objects and optionally - some properties of them. The amount of
macros per entity is not limited - while the built-in discovery rules
return either one or two macros (for example, two for filesystem
discovery), it is possible to return more.

The required JSON format is best illustrated with an example. Suppose we
are running an old Zabbix 1.8 agent (one that does not support
"vfs.fs.discovery"), but we still need to discover file systems. Here is
a simple Perl script for Linux that discovers mounted file systems and
outputs JSON, which includes both file system name and type. One way to
use it would be as a UserParameter with key "vfs.fs.discovery\_perl":

``` {.perl}
#!/usr/bin/perl

$first = 1;

print "[\n";

for (`cat /proc/mounts`)
{
    ($fsname, $fstype) = m/\S+ (\S+) (\S+)/;

    print "\t,\n" if not $first;
    $first = 0;

    print "\t{\n";
    print "\t\t\"{#FSNAME}\":\"$fsname\",\n";
    print "\t\t\"{#FSTYPE}\":\"$fstype\"\n";
    print "\t}\n";
}

print "]\n";
```

::: noteimportant
Allowed symbols for LLD macro names are **0-9** ,
**A-Z** , **\_** , **.**\
\
Lowercase letters are not supported in the names.
:::

An example of its output (reformatted for clarity) is shown below. JSON
for custom discovery checks has to follow the same format.

``` {.javascript}
[
    { "{#FSNAME}":"/",                           "{#FSTYPE}":"rootfs"   },
    { "{#FSNAME}":"/sys",                        "{#FSTYPE}":"sysfs"    },
    { "{#FSNAME}":"/proc",                       "{#FSTYPE}":"proc"     },
    { "{#FSNAME}":"/dev",                        "{#FSTYPE}":"devtmpfs" },
    { "{#FSNAME}":"/dev/pts",                    "{#FSTYPE}":"devpts"   },
    { "{#FSNAME}":"/lib/init/rw",                "{#FSTYPE}":"tmpfs"    },
    { "{#FSNAME}":"/dev/shm",                    "{#FSTYPE}":"tmpfs"    },
    { "{#FSNAME}":"/home",                       "{#FSTYPE}":"ext3"     },
    { "{#FSNAME}":"/tmp",                        "{#FSTYPE}":"ext3"     },
    { "{#FSNAME}":"/usr",                        "{#FSTYPE}":"ext3"     },
    { "{#FSNAME}":"/var",                        "{#FSTYPE}":"ext3"     },
    { "{#FSNAME}":"/sys/fs/fuse/connections",    "{#FSTYPE}":"fusectl"  }
]
```

In previous example it is required that the keys match the LLD macro
names used in prototypes, the alternative is to extract LLD macro values
using JSONPath `{#FSNAME}` → `$.fsname` and `{#FSTYPE}` → `$.fstype`,
thus making such script possible:

``` {.perl}
#!/usr/bin/perl
 
$first = 1;
 
print "[\n";
 
for (`cat /proc/mounts`)
{
    ($fsname, $fstype) = m/\S+ (\S+) (\S+)/;
 
    print "\t,\n" if not $first;
    $first = 0;
 
    print "\t{\n";
    print "\t\t\"fsname\":\"$fsname\",\n";
    print "\t\t\"fstype\":\"$fstype\"\n";
    print "\t}\n";
}
 
print "]\n";
```

An example of its output (reformatted for clarity) is shown below. JSON
for custom discovery checks has to follow the same format.

``` {.javascript}
[
    { "fsname":"/",                           "fstype":"rootfs"   },
    { "fsname":"/sys",                        "fstype":"sysfs"    },
    { "fsname":"/proc",                       "fstype":"proc"     },
    { "fsname":"/dev",                        "fstype":"devtmpfs" },
    { "fsname":"/dev/pts",                    "fstype":"devpts"   },
    { "fsname":"/lib/init/rw",                "fstype":"tmpfs"    },
    { "fsname":"/dev/shm",                    "fstype":"tmpfs"    },
    { "fsname":"/home",                       "fstype":"ext3"     },
    { "fsname":"/tmp",                        "fstype":"ext3"     },
    { "fsname":"/usr",                        "fstype":"ext3"     },
    { "fsname":"/var",                        "fstype":"ext3"     },
    { "fsname":"/sys/fs/fuse/connections",    "fstype":"fusectl"  }
]
```

Then, in the discovery rule's "Filter" field, we could specify
"{\#FSTYPE}" as a macro and "rootfs|ext3" as a regular expression.

::: noteclassic
You don't have to use macro names FSNAME/FSTYPE with custom
LLD rules, you are free to use whatever names you like. In case JSONPath
is used then LLD row will be an array element that can be an object, but
it can be also another array or a value. 
:::

Note that, if using a user parameter, the return value is limited to 512
KB. For more details, see [data limits for LLD return
values](/manual/discovery/low_level_discovery/notes#data_limits_for_return_values).

[comment]: # ({/0a7ce1b3-554f9404})
