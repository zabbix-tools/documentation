[comment]: # ({00e36b2a-00e36b2a})
# 5 Java gateway

[comment]: # ({/00e36b2a-00e36b2a})

[comment]: # ({82908e23-02637e76})
#### Overview

Native support for monitoring JMX applications exists in the form of a
Zabbix daemon called "Zabbix Java gateway", available since Zabbix 2.0.
Zabbix Java gateway is a daemon written in Java. To find out the value
of a particular JMX counter on a host, Zabbix server queries Zabbix Java
gateway, which uses the [JMX management
API](http://java.sun.com/javase/technologies/core/mntr-mgmt/javamanagement/)
to query the application of interest remotely. The application does not
need any additional software installed, it just has to be started with
`-Dcom.sun.management.jmxremote` option on the command line.

Java gateway accepts incoming connection from Zabbix server or proxy and
can only be used as a "passive proxy". As opposed to Zabbix proxy, it
may also be used from Zabbix proxy (Zabbix proxies cannot be chained).
Access to each Java gateway is configured directly in Zabbix server or
proxy configuration file, thus only one Java gateway may be configured
per Zabbix server or Zabbix proxy. If a host will have items of type
**JMX agent** and items of other type, only the **JMX agent** items will
be passed to Java gateway for retrieval.

[comment]: # ({/82908e23-02637e76})

[comment]: # ({b6c87e02-05197937})
When an item has to be updated over Java gateway, Zabbix server or proxy
will connect to the Java gateway and request the value, which Java
gateway in turn retrieves and passes back to the server or proxy. As
such, Java gateway does not cache any values.

Zabbix server or proxy has a specific type of processes that connect to
Java gateway, controlled by the option **StartJavaPollers**. Internally,
Java gateway starts multiple threads, controlled by the
**START\_POLLERS** [option](/manual/appendix/config/zabbix_java). On the
server side, if a connection takes more than **Timeout** seconds, it
will be terminated, but Java gateway might still be busy retrieving
value from the JMX counter. To solve this, there is the **TIMEOUT**
option in Java gateway that allows to set timeout for JMX network
operations.

[comment]: # ({/b6c87e02-05197937})

[comment]: # ({3f08e5ee-475ef799})

Zabbix server or proxy will try to pool requests to a single JMX target
together as much as possible (affected by item intervals) and send them
to the Java gateway in a single connection for better performance.

It is suggested to have **StartJavaPollers** less than or equal to
**START\_POLLERS**, otherwise there might be situations when no threads
are available in the Java gateway to service incoming requests; in such
a case Java gateway uses ThreadPoolExecutor.CallerRunsPolicy, meaning
that the main thread will service the incoming request and will not accept 
any new requests temporarily.

If you are trying to monitor Wildfly-based Java applications with Zabbix Java gateway, please install the latest jboss-client.jar available on the [Wildfly download page](https://www.wildfly.org/downloads/).

[comment]: # ({/3f08e5ee-475ef799})

[comment]: # ({0905c34a-d8d54db7})
#### Getting Java gateway

You can install Java gateway either from the sources or packages
downloaded from [Zabbix website](http://www.zabbix.com/download.php).

Using the links below you can access information how to get and run
Zabbix Java gateway, how to configure Zabbix server (or Zabbix proxy) to
use Zabbix Java gateway for JMX monitoring, and how to configure Zabbix
items in Zabbix frontend that correspond to particular JMX counters.

|Installation from|Instructions|Instructions|
|-----------------|------------|------------|
|*Sources*|[Installation](/manual/installation/install#installing_java_gateway)|[Setup](/manual/concepts/java/from_sources)|
|*RHEL packages*|[Installation](/manual/installation/install_from_packages/rhel#java_gateway_installation)|[Setup](/manual/concepts/java/from_rhel)|
|*Debian/Ubuntu packages*|[Installation](/manual/installation/install_from_packages/debian_ubuntu#java_gateway_installation)|[Setup](/manual/concepts/java/from_debian_ubuntu)|

[comment]: # ({/0905c34a-d8d54db7})
