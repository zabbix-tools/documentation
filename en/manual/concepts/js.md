[comment]: # ({e8144915-e8144915})
# 8 JS

[comment]: # ({/e8144915-e8144915})

[comment]: # ({b0613168-b0613168})
#### Overview

zabbix\_js is a command line utility that can be used for embedded
script testing.

This utility will execute a user script with a string parameter and
print the result. Scripts are executed using the embedded Zabbix
scripting engine.

In case of compilation or execution errors zabbix\_js will print the
error in stderr and exit with code 1.

[comment]: # ({/b0613168-b0613168})

[comment]: # ({5fd793d3-de3fe561})
#### Usage

    zabbix_js -s script-file -p input-param [-l log-level] [-t timeout]
    zabbix_js -s script-file -i input-file [-l log-level] [-t timeout]
    zabbix_js -h
    zabbix_js -V

zabbix\_js accepts the following command line parameters:

      -s, --script script-file          Specify the file name of the script to execute. If '-' is specified as file name, the script will be read from stdin.
      -i, --input input-file            Specify the file name of the input parameter. If '-' is specified as file name, the input will be read from stdin.
      -p, --param input-param           Specify the input parameter.
      -l, --loglevel log-level          Specify the log level.
      -t, --timeout timeout             Specify the timeout in seconds. Valid range: 1-60 seconds (default: 10 seconds).
      -h, --help                        Display help information.
      -V, --version                     Display the version number.

Example:

    zabbix_js -s script-file.js -p example

[comment]: # ({/5fd793d3-de3fe561})
