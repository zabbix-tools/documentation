[comment]: # ({14379c17-14379c17})
# 7 Get

[comment]: # ({/14379c17-14379c17})

[comment]: # ({f50cbd4e-f50cbd4e})
#### Overview

Zabbix get is a command line utility which can be used to communicate
with Zabbix agent and retrieve required information from the agent.

The utility is usually used for the troubleshooting of Zabbix agents.

[comment]: # ({/f50cbd4e-f50cbd4e})

[comment]: # ({3df37123-c625ea25})
#### Running Zabbix get

An example of running Zabbix get under UNIX to get the processor load
value from the agent:

    cd bin
    ./zabbix_get -s 127.0.0.1 -p 10050 -k system.cpu.load[all,avg1]

Another example of running Zabbix get for capturing a string from a
website:

    cd bin
    ./zabbix_get -s 192.168.1.1 -p 10050 -k "web.page.regexp[www.example.com,,,\"USA: ([a-zA-Z0-9.-]+)\",,\1]"

Note that the item key here contains a space so quotes are used to mark
the item key to the shell. The quotes are not part of the item key; they
will be trimmed by the shell and will not be passed to Zabbix agent.

Zabbix get accepts the following command line parameters:

      -s --host <host name or IP>      Specify host name or IP address of a host.
      -p --port <port number>          Specify port number of agent running on the host. Default is 10050.
      -I --source-address <IP address> Specify source IP address.
      -t --timeout <seconds>           Specify timeout. Valid range: 1-30 seconds (default: 30 seconds).
      -k --key <item key>              Specify key of item to retrieve value of.
      -h --help                        Give this help.
      -V --version                     Display version number.

See also [Zabbix get manpage](/manpages/zabbix_get) for more
information.

Zabbix get on Windows can be run similarly:

    zabbix_get.exe [options]

[comment]: # ({/3df37123-c625ea25})
