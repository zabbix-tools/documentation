[comment]: # (tags: agent)

[comment]: # ({0f9ecee9-0f9ecee9})
# 2 Agent

[comment]: # ({/0f9ecee9-0f9ecee9})

[comment]: # ({f5143fcc-f5143fcc})
#### Overview

Zabbix agent is deployed on a monitoring target to actively monitor
local resources and applications (hard drives, memory, processor
statistics, etc.).

The agent gathers operational information locally and reports data to
Zabbix server for further processing. In case of failures (such as a
hard disk running full or a crashed service process), Zabbix server can
actively alert the administrators of the particular machine that
reported the failure.

Zabbix agents are extremely efficient because of use of native system
calls for gathering statistical information.

[comment]: # ({/f5143fcc-f5143fcc})

[comment]: # ({41e6af7c-41e6af7c})
##### Passive and active checks

Zabbix agents can perform passive and active checks.

In a [passive
check](/manual/appendix/items/activepassive#passive_checks) the agent
responds to a data request. Zabbix server (or proxy) asks for data, for
example, CPU load, and Zabbix agent sends back the result.

[Active checks](/manual/appendix/items/activepassive#active_checks)
require more complex processing. The agent must first retrieve a list of
items from Zabbix server for independent processing. Then it will
periodically send new values to the server.

Whether to perform passive or active checks is configured by selecting
the respective monitoring [item
type](/manual/config/items/itemtypes/zabbix_agent). Zabbix agent
processes items of type 'Zabbix agent' or 'Zabbix agent (active)'.

[comment]: # ({/41e6af7c-41e6af7c})

[comment]: # ({2148be27-b05a4949})
#### Supported platforms

Zabbix agent is [supported](https://www.zabbix.com/download_agents?version=6.4&release=6.4.0&os=Windows&os_version=Any&hardware=amd64&encryption=OpenSSL&packaging=MSI&show_legacy=0) on the following platforms:

-   Windows (all desktop and server versions since XP)
-   Linux (also available in [distribution packages](https://www.zabbix.com/download?zabbix=6.4&os_distribution=alma_linux&os_version=9&components=agent&db=&ws=))
-   macOS
-   IBM AIX
-   FreeBSD
-   OpenBSD
-   Solaris

It is also possible to download legacy Zabbix agent binaries for [NetBSD](https://www.zabbix.com/download_agents?version=3.2&release=3.2.0&os=NetBSD&os_version=5.0&hardware=i386&encryption=No+encryption&packaging=Archive&show_legacy=1) and [HP-UX](https://www.zabbix.com/download_agents?version=2.4&release=2.4.4&os=HPUX&os_version=11.31&hardware=amd64&encryption=No+encryption&packaging=Archive&show_legacy=1), and those are compatible with current Zabbix server/proxy version.

[comment]: # ({/2148be27-b05a4949})

[comment]: # ({5bbb67d9-5bbb67d9})
#### Agent on UNIX-like systems

Zabbix agent on UNIX-like systems is run on the host being monitored.

[comment]: # ({/5bbb67d9-5bbb67d9})

[comment]: # ({32937b35-32937b35})
##### Installation

See the [package
installation](/manual/installation/install_from_packages) section for
instructions on how to install Zabbix agent as package.

Alternatively see instructions for [manual
installation](/manual/installation/install#installing_zabbix_daemons) if
you do not want to use packages.

::: noteimportant
In general, 32bit Zabbix agents will work on 64bit
systems, but may fail in some cases.
:::

[comment]: # ({/32937b35-32937b35})

[comment]: # ({9a63a1bb-99f37c64})
##### If installed as package

Zabbix agent runs as a daemon process. The agent can be started by
executing:

    service zabbix-agent start

This will work on most of GNU/Linux systems. On other systems you may
need to run:

    /etc/init.d/zabbix-agent start

Similarly, for stopping/restarting/viewing status of Zabbix agent, use
the following commands:

    service zabbix-agent stop
    service zabbix-agent restart
    service zabbix-agent status

[comment]: # ({/9a63a1bb-99f37c64})

[comment]: # ({10786837-5e83177d})
##### Start up manually

If the above does not work you have to start it manually. Find the path
to the zabbix\_agentd binary and execute:

    zabbix_agentd

[comment]: # ({/10786837-5e83177d})

[comment]: # ({f276f35a-f276f35a})
#### Agent on Windows systems

Zabbix agent on Windows runs as a Windows service.

[comment]: # ({/f276f35a-f276f35a})

[comment]: # ({23b0be18-23b0be18})
##### Preparation

Zabbix agent is distributed as a zip archive. After you download the
archive you need to unpack it. Choose any folder to store Zabbix agent
and the configuration file, e. g.

    C:\zabbix

Copy bin\\zabbix\_agentd.exe and conf\\zabbix\_agentd.conf files to
c:\\zabbix.

Edit the c:\\zabbix\\zabbix\_agentd.conf file to your needs, making sure
to specify a correct "Hostname" parameter.

[comment]: # ({/23b0be18-23b0be18})

[comment]: # ({c7c6daac-c7c6daac})
##### Installation

After this is done use the following command to install Zabbix agent as
Windows service:

    C:\> c:\zabbix\zabbix_agentd.exe -c c:\zabbix\zabbix_agentd.conf -i

Now you should be able to configure "Zabbix agent" service normally as
any other Windows service.

See [more
details](/manual/appendix/install/windows_agent#installing_agent_as_windows_service)
on installing and running Zabbix agent on Windows.

[comment]: # ({/c7c6daac-c7c6daac})

[comment]: # ({8729e024-fa025a89})
#### Other agent options

It is possible to run multiple instances of the agent on a host. A
single instance can use the default configuration file or a
configuration file specified in the command line. In case of multiple
instances each agent instance must have its own configuration file (one
of the instances can use the default configuration file).

The following command line parameters can be used with Zabbix agent:

|Parameter|Description|
|--|--------|
|**UNIX and Windows agent**|<|
|-c --config <config-file>|Path to the configuration file.<br>You may use this option to specify a configuration file that is not the default one.<br>On UNIX, default is /usr/local/etc/zabbix\_agentd.conf or as set by [compile-time](/manual/installation/install#installing_zabbix_daemons) variables *--sysconfdir* or *--prefix*<br>On Windows, default is c:\\zabbix\_agentd.conf|
|-p --print|Print known items and exit.<br>*Note*: To return [user parameter](/manual/config/items/userparameters) results as well, you must specify the configuration file (if it is not in the default location).|
|-t --test <item key>|Test specified item and exit.<br>*Note*: To return [user parameter](/manual/config/items/userparameters) results as well, you must specify the configuration file (if it is not in the default location).|
|-h --help|Display help information|
|-V --version|Display version number|
|**UNIX agent only**|<|
|-R --runtime-control <option>|Perform administrative functions. See [runtime control](/manual/concepts/agent#runtime_control).|
|**Windows agent only**|<|
|-m --multiple-agents|Use multiple agent instances (with -i,-d,-s,-x functions).<br>To distinguish service names of instances, each service name will include the Hostname value from the specified configuration file.|
|**Windows agent only (functions)**|<|
|-i --install|Install Zabbix Windows agent as service|
|-d --uninstall|Uninstall Zabbix Windows agent service|
|-s --start|Start Zabbix Windows agent service|
|-x --stop|Stop Zabbix Windows agent service|

Specific **examples** of using command line parameters:

-   printing all built-in agent items with values
-   testing a user parameter with "mysql.ping" key defined in the
    specified configuration file
-   installing a "Zabbix Agent" service for Windows using the default
    path to configuration file c:\\zabbix\_agentd.conf
-   installing a "Zabbix Agent \[Hostname\]" service for Windows using
    the configuration file zabbix\_agentd.conf located in the same
    folder as agent executable and make the service name unique by
    extending it by Hostname value from the config file

```{=html}
<!-- -->
```
    zabbix_agentd --print
    zabbix_agentd -t "mysql.ping" -c /etc/zabbix/zabbix_agentd.conf
    zabbix_agentd.exe -i
    zabbix_agentd.exe -i -m -c zabbix_agentd.conf

[comment]: # ({/8729e024-fa025a89})

[comment]: # ({93600c51-6ad4cc3c})
##### Runtime control

With runtime control options you may change the log level of agent
processes.

|Option|Description|Target|
|--|------|------|
|log\_level\_increase\[=<target>\]|Increase log level.<br>If target is not specified, all processes are affected.|Target can be specified as:<br>**process type** - all processes of specified type (e.g., listener)<br>See all [agent process types](#agent_process_types).<br>**process type,N** - process type and number (e.g., listener,3)<br>**pid** - process identifier (1 to 65535). For larger values specify target as 'process-type,N'.|
|log\_level\_decrease\[=<target>\]|Decrease log level.<br>If target is not specified, all processes are affected.|^|
|userparameter\_reload|Reload values of the *UserParameter* and *Include* options from the current configuration file.| |

Examples:

-   increasing log level of all processes
-   increasing log level of the third listener process
-   increasing log level of process with PID 1234
-   decreasing log level of all active check processes

```{=html}
<!-- -->
```
    zabbix_agentd -R log_level_increase
    zabbix_agentd -R log_level_increase=listener,3
    zabbix_agentd -R log_level_increase=1234
    zabbix_agentd -R log_level_decrease="active checks"

::: noteclassic
Runtime control is not supported on OpenBSD, NetBSD and
Windows.
:::

[comment]: # ({/93600c51-6ad4cc3c})

[comment]: # ({929667fd-929667fd})
#### Agent process types

-   `active checks` - process for performing active checks
-   `collector` - process for data collection
-   `listener` - process for listening to passive checks

The agent log file can be used to observe these process types.

[comment]: # ({/929667fd-929667fd})

[comment]: # ({18ab16f1-18ab16f1})
#### Process user

Zabbix agent on UNIX is designed to run as a non-root user. It will run
as whatever non-root user it is started as. So you can run agent as any
non-root user without any issues.

If you will try to run it as 'root', it will switch to a hardcoded
'zabbix' user, which must be present on your system. You can only run
agent as 'root' if you modify the 'AllowRoot' parameter in the agent
configuration file accordingly.

[comment]: # ({/18ab16f1-18ab16f1})

[comment]: # ({fc6b2c61-fc6b2c61})
#### Configuration file

For details on configuring Zabbix agent see the configuration file
options for [zabbix\_agentd](/manual/appendix/config/zabbix_agentd) or
[Windows agent](/manual/appendix/config/zabbix_agentd_win).

[comment]: # ({/fc6b2c61-fc6b2c61})

[comment]: # ({7c3bd34c-7c3bd34c})
#### Locale

Note that the agent requires a UTF-8 locale so that some textual agent
items can return the expected content. Most modern Unix-like systems
have a UTF-8 locale as default, however, there are some systems where
that may need to be set specifically.

[comment]: # ({/7c3bd34c-7c3bd34c})

[comment]: # ({5581c546-5581c546})
#### Exit code

Before version 2.2 Zabbix agent returned 0 in case of successful exit
and 255 in case of failure. Starting from version 2.2 and higher Zabbix
agent returns 0 in case of successful exit and 1 in case of failure.

[comment]: # ({/5581c546-5581c546})
