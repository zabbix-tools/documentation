[comment]: # ({d60be363-d60be363})
# 3 Agent 2

[comment]: # ({/d60be363-d60be363})

[comment]: # ({ec8fe317-734dc3ed})
#### Overview

Zabbix agent 2 is a new generation of Zabbix agent and may be used in
place of Zabbix agent. Zabbix agent 2 has been developed to:

-   reduce the number of TCP connections
-   provide improved concurrency of checks
-   be easily extendible with plugins. A plugin should be able to:
    -   provide trivial checks consisting of only a few simple lines of
        code
    -   provide complex checks consisting of long-running scripts and
        standalone data gathering with periodic sending back of the data
-   be a drop-in replacement for Zabbix agent (in that it supports all
    the previous functionality)

Agent 2 is written in Go programming language (with some C code of Zabbix agent reused). A configured Go environment with a currently supported [Go
version](https://golang.org/doc/devel/release#policy) is required for
building Zabbix agent 2.

Agent 2 does not have built-in daemonization support on Linux; it can be
run as a [Windows service](/manual/appendix/install/windows_agent).

[comment]: # ({/ec8fe317-734dc3ed})

[comment]: # ({cfd11d67-eaa0f2bb})
##### Passive and active checks

Passive checks work similarly to Zabbix agent. Active checks support
scheduled/flexible intervals and check concurrency within one active
server. 

::: noteclassic
  By default, after a restart, Zabbix agent 2 will schedule the first data collection for active checks at a conditionally random time within the item's update interval to prevent spikes in resource usage. To perform active checks that do not have *Scheduling* [update interval](/manual/config/items/item/custom_intervals#scheduling-intervals) immediately after the agent restart, set `ForceActiveChecksOnStart` parameter (global-level) or `Plugins.<Plugin name>.System.ForceActiveChecksOnStart` (affects only specific plugin checks) in the [configuration file](/manual/appendix/config/zabbix_agent2). Plugin-level parameter, if set, will override the global parameter. Forcing active checks on start is supported since Zabbix 6.0.2. 
:::

[comment]: # ({/cfd11d67-eaa0f2bb})

[comment]: # ({eb103d1f-05ea1336})
##### Check concurrency

Checks from different plugins can be executed concurrently. The number
of concurrent checks within one plugin is limited by the plugin capacity
setting. Each plugin may have a hardcoded capacity setting (100 being
default) that can be lowered using the `Plugins.<PluginName>.System.Capacity=N` setting in the *Plugins*
configuration [parameter](#configuration_file). Former name of this parameter `Plugins.<PluginName>.Capacity` is still supported, but has been deprecated in Zabbix 6.0.

[comment]: # ({/eb103d1f-05ea1336})

[comment]: # ({09ccc819-a2064519})
#### Supported platforms

Zabbix agent 2 is supported on the following platforms:

-   Windows (all desktop and server versions since XP; also available as a [pre-compiled binary](https://www.zabbix.com/download_agents?version=6.4&release=6.4.0&os=Windows&os_version=Any&hardware=amd64&encryption=OpenSSL&packaging=MSI&show_legacy=0))
-   Linux (also available in [distribution packages](https://www.zabbix.com/download?zabbix=6.4&os_distribution=alma_linux&os_version=9&components=agent_2&db=&ws=))

[comment]: # ({/09ccc819-a2064519})

[comment]: # ({ecf4d908-cabc0f5f})
#### Installation

Zabbix agent 2 is available in pre-compiled Zabbix packages.
To compile Zabbix agent 2 from [sources](https://www.zabbix.com/download_sources) you have to specify the `--enable-agent2` configure option.

[comment]: # ({/ecf4d908-cabc0f5f})

[comment]: # ({be07d6b8-45c02f12})
#### Options

The following command line parameters can be used with Zabbix agent 2:

|Parameter|Description|
|--|--------|
|-c --config <config-file>|Path to the configuration file.<br>You may use this option to specify a configuration file that is not the default one.<br>On UNIX, default is /usr/local/etc/zabbix\_agent2.conf or as set by [compile-time](/manual/installation/install#installing_zabbix_daemons) variables *--sysconfdir* or *--prefix*|
|-f --foreground|Run Zabbix agent in foreground (default: true).|
|-p --print|Print known items and exit.<br>*Note*: To return [user parameter](/manual/config/items/userparameters) results as well, you must specify the configuration file (if it is not in the default location).|
|-t --test <item key>|Test specified item and exit.<br>*Note*: To return [user parameter](/manual/config/items/userparameters) results as well, you must specify the configuration file (if it is not in the default location).|
|-h --help|Print help information and exit.|
|-v --verbose|Print debugging information. Use this option with -p and -t options.|
|-V --version|Print agent version and license information.|
|-R --runtime-control <option>|Perform administrative functions. See [runtime control](/manual/concepts/agent2#runtime_control).|

Specific **examples** of using command line parameters:

-   print all built-in agent items with values
-   test a user parameter with "mysql.ping" key defined in the specified configuration file

    zabbix_agent2 --print
    zabbix_agent2 -t "mysql.ping" -c /etc/zabbix/zabbix_agentd.conf

[comment]: # ({/be07d6b8-45c02f12})

[comment]: # ({1a245e04-11aa0de0})
##### Runtime control

Runtime control provides some options for remote control.

|Option|Description|
|--|--------|
|log\_level\_increase|Increase log level.|
|log\_level\_decrease|Decrease log level.|
|metrics|List available metrics.|
|version|Display agent version.|
|userparameter\_reload|Reload values of the *UserParameter* and *Include* options from the current configuration file.|
|help|Display help information on runtime control.|

Examples:

-   increasing log level for agent 2
-   print runtime control options

```{=html}
<!-- -->
```
    zabbix_agent2 -R log_level_increase
    zabbix_agent2 -R help

[comment]: # ({/1a245e04-11aa0de0})

[comment]: # ({e2091b46-77e9b590})
#### Configuration file

The configuration parameters of agent 2 are mostly compatible with
Zabbix agent with some exceptions.

|New parameters|Description|
|--|--------|
|*ControlSocket*|The runtime control socket path. Agent 2 uses a control socket for [runtime commands](#runtime_control).|
|*EnablePersistentBuffer*,<br>*PersistentBufferFile*,<br>*PersistentBufferPeriod*|These parameters are used to configure persistent storage on agent 2 for active items.|
|*ForceActiveChecksOnStart*|Determines whether the agent should perform active checks immediately after restart or spread evenly over time.|
|*Plugins*|Plugins may have their own parameters, in the format `Plugins.<Plugin name>.<Parameter>=<value>`. A common plugin parameter is *System.Capacity*, setting the limit of checks that can be executed at the same time.|
|*StatusPort*|The port agent 2 will be listening on for HTTP status request and display of a list of configured plugins and some internal parameters|
|**Dropped parameters**|**Description**|
|*AllowRoot*, *User*|Not supported because daemonization is not supported.|
|*LoadModule*, *LoadModulePath*|Loadable modules are not supported.|
|*StartAgents*|This parameter was used in Zabbix agent to increase passive check concurrency or disable them. In Agent 2, the concurrency is configured at a plugin level and can be limited by a capacity setting. Whereas disabling passive checks is not currently supported.|
|*HostInterface*, *HostInterfaceItem*|Not yet supported.|

For more details see the configuration file options for
[zabbix\_agent2](/manual/appendix/config/zabbix_agent2).

[comment]: # ({/e2091b46-77e9b590})

[comment]: # ({472820ac-472820ac})
#### Exit codes

Starting from version 4.4.8 Zabbix agent 2 can also be compiled with
older OpenSSL versions (1.0.1, 1.0.2).

In this case Zabbix provides mutexes for locking in OpenSSL. If a mutex
lock or unlock fails then an error message is printed to the standard
error stream (STDERR) and Agent 2 exits with return code 2 or 3,
respectively.

[comment]: # ({/472820ac-472820ac})
