[comment]: # (tags: proxy)

[comment]: # ({f9458f4e-f9458f4e})
# 4 Proxy

[comment]: # ({/f9458f4e-f9458f4e})

[comment]: # ({05d8de8a-05d8de8a})
#### Overview

Zabbix proxy is a process that may collect monitoring data from one or
more monitored devices and send the information to the Zabbix server,
essentially working on behalf of the server. All collected data is
buffered locally and then transferred to the Zabbix server the proxy
belongs to.

Deploying a proxy is optional, but may be very beneficial to distribute
the load of a single Zabbix server. If only proxies collect data,
processing on the server becomes less CPU and disk I/O hungry.

A Zabbix proxy is the ideal solution for centralized monitoring of
remote locations, branches and networks with no local administrators.

Zabbix proxy requires a separate database.

::: noteimportant
Note that databases supported with Zabbix proxy
are SQLite, MySQL and PostgreSQL. Using Oracle is at your own risk and
may contain some limitations as, for example, in [return
values](/manual/discovery/low_level_discovery#overview) of low-level
discovery rules.
:::

See also: [Using proxies in a distributed
environment](/manual/distributed_monitoring/proxies)

[comment]: # ({/05d8de8a-05d8de8a})

[comment]: # ({709824a5-709824a5})
#### Running proxy

[comment]: # ({/709824a5-709824a5})

[comment]: # ({ee6e6435-a0a6c8d1})
##### If installed as package

Zabbix proxy runs as a daemon process. The proxy can be started by
executing:

    service zabbix-proxy start

This will work on most of GNU/Linux systems. On other systems you may
need to run:

    /etc/init.d/zabbix-proxy start

Similarly, for stopping/restarting/viewing status of Zabbix proxy, use
the following commands:

    service zabbix-proxy stop
    service zabbix-proxy restart
    service zabbix-proxy status

[comment]: # ({/ee6e6435-a0a6c8d1})

[comment]: # ({92fb7247-0b35181b})
##### Start up manually

If the above does not work you have to start it manually. Find the path
to the zabbix\_proxy binary and execute:

    zabbix_proxy

You can use the following command line parameters with Zabbix proxy:

    -c --config <file>              path to the configuration file
    -f --foreground                 run Zabbix proxy in foreground
    -R --runtime-control <option>   perform administrative functions
    -h --help                       give this help
    -V --version                    display version number

Examples of running Zabbix proxy with command line parameters:

    zabbix_proxy -c /usr/local/etc/zabbix_proxy.conf
    zabbix_proxy --help
    zabbix_proxy -V

[comment]: # ({/92fb7247-0b35181b})

[comment]: # ({aae3358c-ee4dd6f0})
##### Runtime control

Runtime control options:

|Option|Description|Target|
|--|------|------|
|config\_cache\_reload|Reload configuration cache. Ignored if cache is being currently loaded.<br>Active Zabbix proxy will connect to the Zabbix server and request configuration data.<br>Passive Zabbix proxy will request configuration data from Zabbix server the next time when the server connects to the proxy.| |
|diaginfo\[=<**section**>\]|Gather diagnostic information in the proxy log file.|**historycache** - history cache statistics<br>**preprocessing** - preprocessing manager statistics<br>**locks** - list of mutexes (is empty on **BSD* systems)|
|snmp\_cache\_reload|Reload SNMP cache, clear the SNMP properties (engine time, engine boots, engine id, credentials) for all hosts.| |
|housekeeper\_execute|Start the housekeeping procedure. Ignored if the housekeeping procedure is currently in progress.| |
|log\_level\_increase\[=<**target**>\]|Increase log level, affects all processes if target is not specified. <br>Not supported on **BSD* systems.|**process type** - All processes of specified type (e.g., poller)<br>See all [proxy process types](#proxy_process_types).<br>**process type,N** - Process type and number (e.g., poller,3)<br>**pid** - Process identifier (1 to 65535). For larger values specify target as 'process type,N'.|
|log\_level\_decrease\[=<**target**>\]|Decrease log level, affects all processes if target is not specified.<br>Not supported on **BSD* systems.|^|
|prof\_enable\[=<**target**>\]|Enable profiling.<br>Affects all processes if target is not specified.<br>Enabled profiling provides details of all rwlocks/mutexes by function name.|**process type** - All processes of specified type (e.g., history syncer)<br>See all [proxy process types](#proxy_process_types).<br>**process type,N** - Process type and number (e.g., history syncer,1)<br>**pid** - Process identifier (1 to 65535). For larger values specify target as 'process type,N'.<br>**scope** - `rwlock`, `mutex`, `processing` can be used with the process type and number (e.g., history syncer,1,processing) or all processes of type (e.g., history syncer,rwlock)|
|prof\_disable\[=<**target**>\]|Disable profiling.<br>Affects all processes if target is not specified.|**process type** - All processes of specified type (e.g., history syncer)<br>See all [proxy process types](#proxy_process_types).<br>**process type,N** - Process type and number (e.g., history syncer,1)<br>**pid** - Process identifier (1 to 65535). For larger values specify target as 'process type,N'.|

Example of using runtime control to reload the proxy configuration
cache:

    zabbix_proxy -c /usr/local/etc/zabbix_proxy.conf -R config_cache_reload

Examples of using runtime control to gather diagnostic information:

    # Gather all available diagnostic information in the proxy log file:
    zabbix_proxy -R diaginfo

    # Gather history cache statistics in the proxy log file:
    zabbix_proxy -R diaginfo=historycache

Example of using runtime control to reload the SNMP cache:

    zabbix_proxy -R snmp_cache_reload  

Example of using runtime control to trigger execution of housekeeper

    zabbix_proxy -c /usr/local/etc/zabbix_proxy.conf -R housekeeper_execute

Examples of using runtime control to change log level:

    # Increase log level of all processes:
    zabbix_proxy -c /usr/local/etc/zabbix_proxy.conf -R log_level_increase

    # Increase log level of second poller process:
    zabbix_proxy -c /usr/local/etc/zabbix_proxy.conf -R log_level_increase=poller,2

    # Increase log level of process with PID 1234:
    zabbix_proxy -c /usr/local/etc/zabbix_proxy.conf -R log_level_increase=1234

    # Decrease log level of all http poller processes:
    zabbix_proxy -c /usr/local/etc/zabbix_proxy.conf -R log_level_decrease="http poller"

[comment]: # ({/aae3358c-ee4dd6f0})

[comment]: # ({a9af55d3-a9af55d3})
##### Process user

Zabbix proxy is designed to run as a non-root user. It will run as
whatever non-root user it is started as. So you can run proxy as any
non-root user without any issues.

If you will try to run it as 'root', it will switch to a hardcoded
'zabbix' user, which must be present on your system. You can only run
proxy as 'root' if you modify the 'AllowRoot' parameter in the proxy
configuration file accordingly.

[comment]: # ({/a9af55d3-a9af55d3})

[comment]: # ({441f5ec7-441f5ec7})
##### Configuration file

See the [configuration file](/manual/appendix/config/zabbix_proxy)
options for details on configuring zabbix\_proxy.

[comment]: # ({/441f5ec7-441f5ec7})

[comment]: # ({cdcb59fa-2cd46511})
#### Proxy process types

-   `availability manager` - process for host availability updates
-   `configuration syncer` - process for managing in-memory cache of
    configuration data
-   `data sender` - proxy data sender
-   `discovery manager` - manager process for discovery of devices
-   `discovery worker` - process for handling discovery tasks from the discovery manager
-   `history syncer` - history DB writer
-   `housekeeper` - process for removal of old historical data
-   `http poller` - web monitoring poller
-   `icmp pinger` - poller for icmpping checks
-   `ipmi manager` - IPMI poller manager
-   `ipmi poller` - poller for IPMI checks
-   `java poller` - poller for Java checks
-   `odbc poller` - poller for ODBC checks
-   `poller` - normal poller for passive checks
-   `preprocessing manager` - manager of preprocessing tasks
-   `preprocessing worker` - process for data preprocessing
-   `self-monitoring` - process for collecting internal server
    statistics
-   `snmp trapper` - trapper for SNMP traps
-   `task manager` - process for remote execution of tasks requested by
    other components (e.g. close problem, acknowledge problem, check
    item value now, remote command functionality)
-   `trapper` - trapper for active checks, traps, proxy communication
-   `unreachable poller` - poller for unreachable devices
-   `vmware collector` - VMware data collector responsible for data
    gathering from VMware services

The proxy log file can be used to observe these process types.

Various types of Zabbix proxy processes can be monitored using the
**zabbix\[process,<type>,<mode>,<state>\]** internal
[item](/manual/config/items/itemtypes/internal).

[comment]: # ({/cdcb59fa-2cd46511})

[comment]: # ({2c7aa9fb-087c822f})
#### Supported platforms

Zabbix proxy runs on the same list of
[supported platforms](/manual/concepts/server#supported_platforms)
as Zabbix server.

[comment]: # ({/2c7aa9fb-087c822f})

[comment]: # ({1af7e555-c703b792})
#### Locale

Note that the proxy requires a UTF-8 locale so that some textual items
can be interpreted correctly. Most modern Unix-like systems have a UTF-8
locale as default, however, there are some systems where that may need to be set specifically.

[comment]: # ({/1af7e555-c703b792})
