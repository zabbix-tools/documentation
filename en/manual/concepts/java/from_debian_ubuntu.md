[comment]: # ({e983a9df-e983a9df})
# 3 Setup from Debian/Ubuntu packages

[comment]: # ({/e983a9df-e983a9df})

[comment]: # ({a044dd01-a044dd01})
#### Overview

If
[installed](/manual/installation/install_from_packages/debian_ubuntu#java_gateway_installation)
from Debian/Ubuntu packages, the following information will help you in
setting up Zabbix [Java gateway](/manual/concepts/java).

[comment]: # ({/a044dd01-a044dd01})

[comment]: # ({752626b4-01191552})
#### Configuring and running Java gateway

Java gateway configuration may be tuned in the file:

    /etc/zabbix/zabbix_java_gateway.conf

For more details, see Zabbix Java gateway configuration
[parameters](/manual/appendix/config/zabbix_java).

To start Zabbix Java gateway:

    service zabbix-java-gateway restart

To automatically start Zabbix Java gateway on boot:

    systemctl enable zabbix-java-gateway

[comment]: # ({/752626b4-01191552})

[comment]: # ({81ca4902-81ca4902})
#### Configuring server for use with Java gateway

With Java gateway up and running, you have to tell Zabbix server where
to find Zabbix Java gateway. This is done by specifying JavaGateway and
JavaGatewayPort parameters in the [server configuration
file](/manual/appendix/config/zabbix_server). If the host on which JMX
application is running is monitored by Zabbix proxy, then you specify
the connection parameters in the [proxy configuration
file](/manual/appendix/config/zabbix_proxy) instead.

    JavaGateway=192.168.3.14
    JavaGatewayPort=10052

By default, server does not start any processes related to JMX
monitoring. If you wish to use it, however, you have to specify the
number of pre-forked instances of Java pollers. You do this in the same
way you specify regular pollers and trappers.

    StartJavaPollers=5

Do not forget to restart server or proxy, once you are done with
configuring them.

[comment]: # ({/81ca4902-81ca4902})

[comment]: # ({05f991d0-05f991d0})
#### Debugging Java gateway

Zabbix Java gateway log file is:

    /var/log/zabbix/zabbix_java_gateway.log

If you like to increase the logging, edit the file:

    /etc/zabbix/zabbix_java_gateway_logback.xml

and change `level="info"` to "debug" or even "trace" (for deep
troubleshooting):

    <configuration scan="true" scanPeriod="15 seconds">
    [...]
          <root level="info">
                  <appender-ref ref="FILE" />
          </root>

    </configuration>

[comment]: # ({/05f991d0-05f991d0})

[comment]: # ({4332cfb8-4332cfb8})
#### JMX monitoring

See [JMX monitoring](/manual/config/items/itemtypes/jmx_monitoring) page
for more details.

[comment]: # ({/4332cfb8-4332cfb8})
