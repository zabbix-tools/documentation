[comment]: # ({8b95abf8-f9e8f670})

# 22. Quick reference guides

## Overview

This documentation section contains quick recipes for setting up Zabbix for some commonly required monitoring goals.

It is designed with the new Zabbix user in mind and can be used as a navigator through other documentation sections that 
contain information required for resolving the task.

[comment]: # ({/8b95abf8-f9e8f670})

[comment]: # ({cdc0e8e5-b2664a94})

The following quick reference guides are available:

- [Monitor Linux with Zabbix agent](/manual/guides/monitor_linux)
- [Monitor Windows with Zabbix agent](/manual/guides/monitor_windows)
- [Monitor Apache via HTTP](/manual/guides/monitor_apache)
- [Monitor MySQL with Zabbix agent 2](/manual/guides/monitor_mysql)

[comment]: # ({/cdc0e8e5-b2664a94})
