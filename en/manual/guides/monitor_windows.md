[comment]: # ({df8dbb26-3fc5b143})
# 2. Monitor Windows with Zabbix agent

[comment]: # ({/df8dbb26-3fc5b143})

[comment]: # ({5a0f005b-fd7b4328})
## Introduction

This page walks you through the steps required to start basic monitoring of Windows machines with Zabbix.

**Who this guide is for**

This guide is designed for new Zabbix users and contains the minimum set 
of steps required to enable basic monitoring of your Windows machine. 
If you are looking for deep customization options or require more advanced 
configuration, see  [Configuration](/manual/config) section of Zabbix 
manual.  

**Prerequisites**

Before proceeding with this installation guide, you must [download and install](https://www.zabbix.com/download) Zabbix 
server and Zabbix frontend according to instructions for your OS.

[comment]: # ({/5a0f005b-fd7b4328})

[comment]: # ({3e340b44-7cec4ce2})

## Install Zabbix agent

Zabbix agent is the process responsible for gathering data. You need to install it on the Windows machine that you want 
to monitor. Follow Zabbix agent installation instructions for [Windows](/installation/install_from_packages/win_msi).

![](../../../assets/en/manual/installation/install_from_packages/msi0_b.png)

[comment]: # ({/3e340b44-7cec4ce2})

[comment]: # ({541ea782-c5ab18d1})

## Configure Zabbix for monitoring

Zabbix agent can collect metrics in active or passive mode (simultaneously). 

::: noteclassic
A passive check is a simple data request. Zabbix server or proxy asks for some data (for example, CPU load) and Zabbix 
agent sends back the result to the server. Active checks require more complex processing. The agent must first retrieve 
from the server(s) a list of items for independent processing and then bulk send the data back. See 
[Passive and active agent checks](/manual/appendix/items/activepassive) for more info.
:::

Monitoring templates provided by Zabbix usually offer two alternatives - a template for Zabbix agent and a template for 
Zabbix agent (active). With the first option, the agent will collect metrics in passive mode. Such templates will deliver 
identical monitoring results, but using different communication protocols. 

Further Zabbix configuration depends on whether you select a template for [active](#active-checks) or 
[passive](#passive-checks) Zabbix agent checks.

[comment]: # ({/541ea782-c5ab18d1})

[comment]: # ({5410a86a-76c0a12c})
### Passive checks

[comment]: # ({/5410a86a-76c0a12c})

[comment]: # ({37dc55e0-854b1e0b})

#### Zabbix frontend

1\. Log in to Zabbix frontend.

2\. [Create a host](/manual/config/hosts/host) in Zabbix web interface. 

This host will represent your Windows machine. 

3\. In the *Interfaces* parameter, add *Agent* interface and specify the IP 
address or DNS name of the Windows machine where the agent is installed. 

4\. In the *Templates* parameter, type or select *Windows by Zabbix agent*. 

![](../../../assets/en/manual/guides/win_host_passive.png){width="600"}

[comment]: # ({/37dc55e0-854b1e0b})

[comment]: # ({615b4a19-17dcee1f})
#### Zabbix agent

For passive checks Zabbix agent needs to know the IP address or DNS name of Zabbix server. If you 
have provided correct information during the agent installation, the configuration file is already updated. Otherwise, you 
need to manually specify it. Go to the `C:\Program files\Zabbix Agent` folder, open the file 
*zabbix_agent.conf* and add the IP/DNS of your Zabbix server to the *Server* parameter.

Example:

    Server=192.0.2.22

[comment]: # ({/615b4a19-17dcee1f})

[comment]: # ({4353394a-287beca2})
### Active checks

[comment]: # ({/4353394a-287beca2})

[comment]: # ({5fa75beb-9d58b1d3})
#### Zabbix frontend

1\. Log in to Zabbix frontend.

2\. [Create a host](/manual/config/hosts/host) in Zabbix web interface. 

This host will represent your Windows machine. 

3\. In the *Templates* parameter, type or select  *Windows by Zabbix agent active*.

![](../../../assets/en/manual/guides/win_host_active.png){width="600"}

[comment]: # ({/5fa75beb-9d58b1d3})

[comment]: # ({65489b4f-ec395430})
#### Zabbix agent

In the `C:\Program files\Zabbix Agent` folder open the file *zabbix_agent.conf* and add:

- The name of the host you created in Zabbix web interface to the *Hostname* parameter.
- The IP address or DNS name of your Zabbix server machine to the *ServerActive* parameter (might be prefilled if you have provided it during Zabbix agent setup).

Example:
  
    ServerActive= 192.0.2.22
    Hostname=Windows workstation

[comment]: # ({/65489b4f-ec395430})

[comment]: # ({7ed12346-2bb9a9d4})
## View collected metrics

Congratulations! At this point, Zabbix is already monitoring your Windows machine. 

To view collected metrics, open the *Monitoring->Hosts* 
[menu section](/manual/web_interface/frontend_sections/monitoring/hosts) and click on 
the *Latest data* next to the host. 

![](../../../assets/en/manual/guides/win_latest_data.png){width="600"}

[comment]: # ({/7ed12346-2bb9a9d4})

[comment]: # ({1626a19b-eaf2b29b})
## Set up problem alerts

Zabbix can notify you about a problem with your infrastructure using a variety of methods. This guide provides 
configuration steps for sending email alerts. 

1\. Go to the *User settings -> Profile*, switch to the tab *Media* and [add your email](/manual/quickstart/login#adding-user).

![](../../../assets/en/manual/quickstart/new_media.png){width="600"}

2\. Follow the guide for [Receiving problem notification](/manual/quickstart/notification).

Next time, when Zabbix detects a problem you should receive an alert via email.

:::notetip
On Windows, you can use [CpuStres](https://docs.microsoft.com/en-us/sysinternals/downloads/cpustres) utility to simulate 
high CPU load and as a result receive a problem alert. 
:::

**See also:**

- [Creating an item](/manual/config/items/item) - how to start monitoring additional metrics (custom monitoring without templates).
- [Zabbix agent items](/manual/config/items/itemtypes/zabbix_agent), [Zabbix agent items for Windows](/manual/config/items/itemtypes/zabbix_agent/win_keys) - full list of metrics you can monitor using Zabbix agent on Windows.
- [Problem escalations](/manual/config/notifications/action/escalations) - how to create multi-step alert scenarios (e.g., first send message to the system administrator, then, if a problem is not resolved in 45 minutes, send message to the data center manager).

[comment]: # ({/1626a19b-eaf2b29b})
