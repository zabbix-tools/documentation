[comment]: # ({9a839c05-f9e8f670})
# 4 Secret user macros

[comment]: # ({/9a839c05-f9e8f670})

Zabbix provides two options for protecting sensitive information in user macro values:

- Secret text 
- Vault secret

*Note* that while the value of a secret macro is hidden, the value can be revealed through the use in items. For example, in an external script an 'echo' statement referencing a secret macro may be used to reveal the macro value to the frontend because Zabbix server has access to the real macro value.

Secret macros cannot be used in trigger expressions. 

[comment]: # ({d900ad44-740aad18})
## Secret text 

Values of secret text macros are masked by the asterisks.

To make macro value 'secret', click on the button at the end of the value  field and select the option *Secret text*.

![](../../../../assets/en/manual/config/macros/macro_value_type.png) 

Once the configuration is saved, it will no longer be possible to view the value. 

The macro value will be displayed as asterisks. 

To enter a new value, hover over the value field and press Set new value button (appears on hover).

![](../../../../assets/en/manual/config/macros/macro_type_secret2.png) 

If you change macro value type or press *Set new value*, current value will be erased. To revert the original value, use the backwards arrow at the right end of the *Value* field ![](../../../../assets/en/manual/config/macros/macro_type_secret3.png) (only available before saving new configuration). Reverting the value will not expose it. 

::: noteclassic
 URLs that contain a secret macro will not work as the macro
in them will be resolved as "\*\*\*\*\*\*". 
::: 

[comment]: # ({/d900ad44-740aad18})

[comment]: # ({b1e99bf8-480b89ed})
## Vault secret 

With Vault secret macros, the actual macro value is stored in an external secret management software (vault). 

To configure a Vault secret macro, click on the button at the end of the Value field and select the option *Vault secret*.

![](../../../../assets/en/manual/config/macros/macro_value_type1.png) 

The macro value should point to a vault secret. The input format depends on the vault provider. For provider-specific configuration examples, see:

- [HashiCorp](/manual/config/secrets/hashicorp#user_macro_values)
- [CyberArk](/manual/config/secrets/cyberark#user_macro_values)

[comment]: # ({/b1e99bf8-480b89ed})

[comment]: # ({8672e4e0-94cba921})
Vault secret values are retrieved by Zabbix server on every refresh of configuration data and then stored in the configuration cache. 

To manually trigger refresh of secret values from a vault, use the 'secrets_reload' command-line [option](/manual/concepts/server#runtime_control).

Zabbix proxy receives values of vault secret macros from Zabbix server on each configuration sync and stores them in its own configuration cache. The proxy never retrieves macro values from the vault directly. That means a Zabbix proxy cannot start data collection after a restart until it receives the configuration data update from Zabbix server for the first time. 

Encryption must be enabled between Zabbix server and proxy; otherwise a server warning message is logged.

::: notewarning
If a macro value cannot be retrieved successfully, the corresponding item using the value will turn unsupported.
::: 

[comment]: # ({/8672e4e0-94cba921})
