[comment]: # ({fea50b39-b9198e3b})
# 6 Expression macros

[comment]: # ({/fea50b39-b9198e3b})

[comment]: # ({507536ce-e27300f7})
#### Overview

Expression macros are useful for formula calculations. They are calculated 
by expanding all macros inside and evaluating the resulting expression.

Expression macros have a special syntax:

    {?EXPRESSION}

The syntax in EXPRESSION is the same as in [trigger expressions](/manual/config/triggers/expression) (see usage limitations below). 

{HOST.HOST<1-9>} and {ITEM.KEY<1-9>} macros are supported inside expression macros.

[comment]: # ({/507536ce-e27300f7})

[comment]: # ({572077d2-16162d7f})

#### Usage

In the following locations:

   - graph names
   - map element labels 
   - map shape labels
   - map link labels 

only a **single** function, from the following set: `avg`, `last`, `max`, `min`, 
is allowed as an expression macro, e.g.:

    {?avg(/{HOST.HOST}/{ITEM.KEY},1h)}
    
Expressions such as `{?last(/host/item1)/last(/host/item2)}`, `{?count(/host/item1,5m)}` 
and `{?last(/host/item1)*10}` are incorrect in these locations.

However, in:

   - trigger event names 
   - trigger-based notifications and commands
   - problem update notifications and commands 
   
**complex** expressions are allowed, e.g.: 

    {?trendavg(/host/item1,1M:now/M)/trendavg(/host/item1,1M:now/M-1y)*100}

See also: 

-   [Supported macros](/manual/appendix/macros/supported_by_location) for a list of supported locations of the expression macro
-   [Example](/manual/config/triggers/expression#example_18) of using an expression macro in the event name

[comment]: # ({/572077d2-16162d7f})
