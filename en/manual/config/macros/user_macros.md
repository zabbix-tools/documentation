[comment]: # ({d30dc182-d30dc182})
# 2 User macros

[comment]: # ({/d30dc182-d30dc182})

[comment]: # ({3e220640-3e220640})
#### Overview

User macros are supported in Zabbix for greater flexibility, in addition
to the macros [supported](/manual/appendix/macros/supported_by_location)
out-of-the-box.

User macros can be defined on global, template and host level. These
macros have a special syntax:

    {$MACRO}

Zabbix resolves macros according to the following precedence:

1.  host level macros (checked first)
2.  macros defined for first level templates of the host (i.e.,
    templates linked directly to the host), sorted by template ID
3.  macros defined for second level templates of the host, sorted by
    template ID
4.  macros defined for third level templates of the host, sorted by
    template ID, etc.
5.  global macros (checked last)

In other words, if a macro does not exist for a host, Zabbix will try to
find it in the host templates of increasing depth. If still not found, a
global macro will be used, if exists.

::: notewarning
If a macro with the **same name** exists on multiple
linked templates of the same level, the macro from the template with the
lowest ID will be used. Thus having macros with the same name in
multiple templates is a configuration risk.
:::

If Zabbix is unable to find a macro, the macro will not be resolved.

::: noteimportant
Macros (including user macros) are left unresolved
in the Configuration section (for example, in the trigger list) by
design to make complex configuration more transparent.
:::

User macros can be used in:

-   item key parameter
-   item update intervals and flexible intervals
-   trigger name and description
-   trigger expression parameters and constants (see
    [examples](#examples))
-   many other locations - see the [full
    list](/manual/appendix/macros/supported_by_location_user)

[comment]: # ({/3e220640-3e220640})

[comment]: # ({e3755145-e3755145})
##### Common use cases of global and host macros

-   use a global macro in several locations; then change the macro value
    and apply configuration changes to all locations with one click
-   take advantage of templates with host-specific attributes:
    passwords, port numbers, file names, regular expressions, etc.

[comment]: # ({/e3755145-e3755145})

[comment]: # ({be6c0958-60e46db2})
::: notetip
It is advisable to use host macros instead of global macros because adding, updating or deleting global macros forces incremental configuration update for all hosts. For more information, see [Passive and active agent checks](/manual/appendix/items/activepassive#active-checks).
:::

[comment]: # ({/be6c0958-60e46db2})

[comment]: # ({3c640a92-561314a3})
#### Configuration

To define user macros, go to the corresponding location in the frontend:

-   for global macros, visit *Administration → Macros*
-   for host and template level macros, open host or template properties
    and look for the *Macros* tab

::: notetip
If a user macro is used in items or triggers in a
template, it is suggested to add that macro to the template even if it
is defined on a global level. That way, if the macro type is *text*
exporting the template to XML and importing it in another system will
still allow it to work as expected. Values of secret macros are not
[exported](/manual/xml_export_import). 
:::

A user macro has the following attributes:

![](../../../../assets/en/manual/config/macros/user_macros.png){width="600"}

|Parameter|Description|
|--|--------|
|*Macro*|Macro name. The name must be wrapped in curly brackets and start with a dollar sign.<br>Example: {$FRONTEND\_URL}. The following characters are allowed in the macro names: **A-Z** (uppercase only) , **0-9** , **\_** , **.**|
|*Value*|Macro value. Three value types are supported:<br>**Text** (default) - plain-text value<br>**[Secret text](/manual/config/macros/secret_macros#secret_text)** - the value is masked with asterisks<br> **[Vault secret](/manual/config/macros/secret_macros#vault_secret)** - the value contains a path/query to a [vault secret](/manual/config/secrets). <br><br>To change the value type click on the button at the end of the value input field.<br><br>Maximum length of a user macro value is 2048 characters (255 characters in versions before 5.2.0).|
|*Description*|Text field used to provide more information about this macro.|

::: noteimportant
In trigger expressions user macros will resolve if
referencing a parameter or constant. They will NOT resolve if
referencing a host, item key, function, operator or another trigger
expression. Secret macros cannot be used in trigger
expressions.
:::

[comment]: # ({/3c640a92-561314a3})

[comment]: # ({4c860844-4c860844})
#### Examples

[comment]: # ({/4c860844-4c860844})

[comment]: # ({b9cf8f56-b9cf8f56})
##### Example 1

Use of host-level macro in the "Status of SSH daemon" item key:

`net.tcp.service[ssh,,{$SSH_PORT}]`

This item can be assigned to multiple hosts, providing that the value of
**{$SSH\_PORT}** is defined on those hosts.

[comment]: # ({/b9cf8f56-b9cf8f56})

[comment]: # ({0fa7cf4a-0fa7cf4a})
##### Example 2

Use of host-level macro in the "CPU load is too high" trigger:

`last(/ca_001/system.cpu.load[,avg1])>{$MAX_CPULOAD}`

Such a trigger would be created on the template, not edited in
individual hosts.

::: notetip
If you want to use the amount of values as the function
parameter (for example, **max(/host/key,\#3)**), include hash mark in
the macro definition like this: SOME\_PERIOD => \#3
:::

[comment]: # ({/0fa7cf4a-0fa7cf4a})

[comment]: # ({36dc94ca-36dc94ca})
##### Example 3

Use of two macros in the "CPU load is too high" trigger:

`min(/ca_001/system.cpu.load[,avg1],{$CPULOAD_PERIOD})>{$MAX_CPULOAD}`

Note that a macro can be used as a parameter of trigger function, in
this example function **min()**.

[comment]: # ({/36dc94ca-36dc94ca})

[comment]: # ({0dabfae5-0dabfae5})
##### Example 4

Synchronize the agent unavailability condition with the item update
interval:

-   define {$INTERVAL} macro and use it in the item update interval;
-   use {$INTERVAL} as parameter of the agent unavailability trigger:

`nodata(/ca_001/agent.ping,{$INTERVAL})=1`

[comment]: # ({/0dabfae5-0dabfae5})

[comment]: # ({2d346656-0f8052f1})
##### Example 5

Centralize configuration of working hours:

-   create a global {$WORKING\_HOURS} macro equal to `1-5,09:00-18:00`;
-   use it in the *Working time* field in *Administration* → *General* →
    *GUI*;
-   use it in the *When active* field in *Users* → *Users*, *Media* tab of a user;
-   use it to set up more frequent item polling during working hours:

![](../../../../assets/en/manual/config/macros/usermacro_example5.png)

-   use it in the *Time period* action condition;
-   adjust the working time in *Administration* → *Macros*,
    if needed.

[comment]: # ({/2d346656-0f8052f1})

[comment]: # ({3c2ac944-3c2ac944})
##### Example 6

Use host prototype macro to configure items for discovered hosts:

-   on a host prototype define user macro {$SNMPVALUE} with
    {\#SNMPVALUE} [low-level
    discovery](/manual/config/macros/lld_macros) macro as a value:

![](../../../../assets/en/manual/config/macros/usermacro_example6.png)

-   assign *Generic SNMPv2* template to the host prototype;
-   use {$SNMPVALUE} in the *SNMP OID* field of *Generic SNMPv2*
    template items.

[comment]: # ({/3c2ac944-3c2ac944})

[comment]: # ({44ca625a-44ca625a})
#### User macro context

See [user macros with
context](/manual/config/macros/user_macros_context).

[comment]: # ({/44ca625a-44ca625a})
