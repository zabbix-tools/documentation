[comment]: # ({c176fd71-2d9d6e33})
# 5 Low-level discovery macros

[comment]: # ({/c176fd71-2d9d6e33})

[comment]: # ({bb12a85a-bb12a85a})
#### Overview

There is a type of macro used within the [low-level
discovery](/manual/discovery/low_level_discovery) (LLD) function:

    {#MACRO} 

It is a macro that is used in an LLD rule and returns real values of the
file system name, network interface, SNMP OID, etc.

These macros can be used for creating item, trigger and graph
*prototypes*. Then, when discovering real file systems, network
interfaces etc., these macros are substituted with real values and are
the basis for creating real items, triggers and graphs.

These macros are also used in creating host and host group *prototypes*
in virtual machine [discovery](/manual/vm_monitoring#host_prototypes).

Some low-level discovery macros come "pre-packaged" with the LLD
function in Zabbix - {\#FSNAME}, {\#FSTYPE}, {\#IFNAME}, {\#SNMPINDEX},
{\#SNMPVALUE}. However, adhering to these names is not compulsory when
creating a
[custom](/manual/discovery/low_level_discovery#creating_custom_lld_rules)
low-level discovery rule. Then you may use any other LLD macro name and
refer to that name.

[comment]: # ({/bb12a85a-bb12a85a})

[comment]: # ({ec9d04e7-fc325ce1})
#### Supported locations

LLD macros can be used:

-   in the low-level discovery rule filter
-   for item prototypes in
    -   name
    -   key parameters
    -   unit
    -   update interval^[1](lld_macros#footnotes)^
    -   history storage period^[1](lld_macros#footnotes)^
    -   trend storage period^[1](lld_macros#footnotes)^
    -   item value preprocessing steps
    -   SNMP OID
    -   IPMI sensor field
    -   calculated item formula
    -   SSH script and Telnet script
    -   database monitoring SQL query
    -   JMX item endpoint field
    -   description
    -   HTTP agent URL field
    -   HTTP agent HTTP query fields field
    -   HTTP agent request body field
    -   HTTP agent required status codes field
    -   HTTP agent headers field key and value
    -   HTTP agent HTTP authentication username field
    -   HTTP agent HTTP authentication password field
    -   HTTP agent HTTP proxy field
    -   HTTP agent HTTP SSL certificate file field
    -   HTTP agent HTTP SSL key file field
    -   HTTP agent HTTP SSL key password field
    -   HTTP agent HTTP timeout^[1](lld_macros#footnotes)^ field
    -   tags
-   for trigger prototypes in
    -   name
    -   operational data
    -   expression (only in constants and function parameters)
    -   URL
    -   description
    -   tags
-   for graph prototypes in
    -   name
-   for host prototypes in
    -   name
    -   visible name
    -   custom interface fields: IP, DNS, port, SNMP v1/v2 community,
        SNMP v3 context name, SNMP v3 security name, SNMP v3
        authentication passphrase, SNMP v3 privacy passphrase
    -   host group prototype name
    -   host tag value
    -   host macro value
    -   (see the [full list](/manual/vm_monitoring/discovery_fields))

In all those places, except the low-level discovery rule filter, LLD macros can be
used inside static user [macro context](/manual/config/macros/user_macros_context).

[comment]: # ({/ec9d04e7-fc325ce1})

[comment]: # ({5b26ab61-5b26ab61})
#### Using macro functions

Macro functions are supported with low-level discovery macros (except in
low-level discovery rule filter), allowing to extract a certain part of
the macro value using a regular expression.

For example, you may want to extract the customer name and interface
number from the following LLD macro for the purposes of event tagging:

    {#IFALIAS}=customername_1

To do so, the `regsub` macro function can be used with the macro in the
event tag value field of a trigger prototype:

![](../../../../assets/en/manual/config/macros/lld_macro_function.png)

Note, that commas are not allowed in unquoted item [key
parameters](/manual/config/items/item/key#key_parameters), so the
parameter containing a macro function has to be quoted. The backslash
(`\`) character should be used to escape double quotes inside the
parameter. Example:

    net.if.in["{{#IFALIAS}.regsub(\"(.*)_([0-9]+)\", \1)}",bytes]

For more information on macro function syntax, see: [Macro
functions](/manual/config/macros/macro_functions)

Macro functions are supported in low-level discovery macros since Zabbix
4.0.

[comment]: # ({/5b26ab61-5b26ab61})

[comment]: # ({573cd0dc-573cd0dc})
##### Footnotes

^**1**^ In the fields marked with ^[1](lld_macros#footnotes)^ a single
macro has to fill the whole field. Multiple macros in a field or macros
mixed with text are not supported.

[comment]: # ({/573cd0dc-573cd0dc})
