[comment]: # ({e8edda3d-5ae18af3})
# JMX template operation

Steps to ensure correct operation of templates that collect metrics by
[JMX](/manual/config/items/itemtypes/jmx_monitoring):

1\. Make sure Zabbix [Java
gateway](/manual/concepts/java#getting_java_gateway) is installed and
set up properly.\
2. [Link](/manual/config/templates/linking#linking_a_template) the
template to the target host. The host should have JMX interface set up.\
If the template is not available in your Zabbix installation, you may
need to import the template's .xml file first - see [Templates
out-of-the-box](/manual/config/templates_out_of_the_box) section for
instructions.\
3. If necessary, adjust the values of template macros.\
4. Configure the instance being monitored to allow sharing data with
Zabbix.

A detailed description of a template, including the full list of macros, items and triggers is available in the template's Readme.md file (accessible by clicking on a template name). 

The following templates are available:

-   [Apache ActiveMQ by JMX](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/app/activemq_jmx/README.md) 
-   [Apache Cassandra by JMX](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/db/cassandra_jmx/README.md) 
-   [Apache Kafka by JMX](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/app/kafka_jmx/README.md) 
-   [Apache Tomcat by JMX](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/app/tomcat_jmx/README.md) 
-   [GridGain by JMX](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/db/gridgain_jmx/README.md) 
-   [Ignite by JMX](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/db/ignite_jmx/README.md) 
-   [WildFly Domain by JMX](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/app/wildfly_domain_jmx/README.md) 
-   [WildFly Server by JMX](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/app/wildfly_server_jmx/README.md) 

[comment]: # ({/e8edda3d-5ae18af3})
