[comment]: # (tags: kubernetes )

[comment]: # ({42ff580b-ebdbb780})
# HTTP template operation

Steps to ensure correct operation of templates that collect metrics with
[HTTP agent](/manual/config/items/itemtypes/http):

1\. Create a host in Zabbix and specify an IP address or DNS name of the
monitoring target as the main interface. This is needed for the
{HOST.CONN} macro to resolve properly in the template items.\
2. [Link](/manual/config/templates/linking#linking_a_template) the
template to the host created in step 1 (if the template is not available
in your Zabbix installation, you may need to import the template's .xml
file first - see [Templates
out-of-the-box](/manual/config/templates_out_of_the_box) section for
instructions).\
3. If necessary, adjust the values of template macros.\
4. Configure the instance being monitored to allow sharing data with
Zabbix. 

  A detailed description of a template, including the full list of macros, items and triggers is available in the template's Readme.md file (accessible by clicking on a template name). 

The following templates are available:

-   [Apache by HTTP](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/app/apache_http/README.md) 
-   [Asterisk by HTTP](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/tel/asterisk_http/README.md) 
-   [AWS by HTTP](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/cloud/AWS/aws_http/README.md)
-   [AWS EC2 by HTTP](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/cloud/AWS/aws_ec2_http/README.md) 
-   [AWS RDS instance by HTTP](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/cloud/AWS/aws_rds_http/README.md) 
-   [AWS S3 bucket by HTTP](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/cloud/AWS/aws_s3_http/README.md) 
-   [Azure by HTTP](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/cloud/azure_http/README.md) 
-   [ClickHouse by HTTP](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/db/clickhouse_http/README.md) 
-   [Cloudflare by HTTP](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/app/cloudflare_http/README.md) 
-   [CockroachDB by HTTP](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/db/cockroachdb_http/README.md) 
-   [Control-M enterprise manager by HTTP](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/app/controlm_http/README.md)
-   [Control-M server by HTTP](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/app/controlm_http/README.md)
-   [DELL PowerEdge R720 by HTTP](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/server/dell/dell_r720_http/README.md)
-   [DELL PowerEdge R740 by HTTP](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/server/dell/dell_r740_http/README.md)
-   [DELL PowerEdge R820 by HTTP](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/server/dell/dell_r820_http/README.md)
-   [DELL PowerEdge R840 by HTTP](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/server/dell/dell_r840_http/README.md) 
-   [Elasticsearch Cluster by HTTP](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/app/elasticsearch_http/README.md) 
-   [Envoy Proxy by HTTP](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/app/envoy_proxy_http/README.md) 
-   [Etcd by HTTP](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/app/etcd_http/README.md) 
-   [GitLab by HTTP](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/app/gitlab_http/README.md) 
-   [Google Cloud Platform (GCP) by HTTP](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/cloud/gcp/README.md)
-   [Hadoop by HTTP](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/app/hadoop_http/README.md) 
-   [HAProxy by HTTP](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/app/haproxy_http/README.md) 
-   [HashiCorp Consul Cluster by HTTP](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/app/consul_http/consul_cluster/README.md) 
-   [HashiCorp Vault by HTTP](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/app/vault_http/README.md) 
-   [Hikvision camera by HTTP](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/cctv/hikvision/README.md) 
-   [HPE MSA 2040 Storage by HTTP](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/san/hpe_msa2040_http/README.md)
-   [HPE MSA 2060 Storage by HTTP](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/san/hpe_msa2060_http/README.md) 
-   [HPE Primera by HTTP](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/san/hpe_primera_http/README.md) 
-   [HPE Synergy by HTTP](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/server/hpe_synergy_http/README.md) 
-   [InfluxDB by HTTP](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/db/influxdb_http/README.md) 
-   [Jenkins by HTTP](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/app/jenkins/README.md)
-   [Kubernetes API server by HTTP](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/app/kubernetes_http/kubernetes_api_server_http/README.md) 
-   [Kubernetes Controller manager by HTTP](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/app/kubernetes_http/kubernetes_controller_manager_http/README.md)  
-   [Kubernetes kubelet by HTTP](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/app/kubernetes_http/kubernetes_kubelet_http/README.md)  
-   [Kubernetes nodes by HTTP](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/app/kubernetes_http/kubernetes_nodes_http/README.md) 
-   [Kubernetes Scheduler by HTTP](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/app/kubernetes_http/kubernetes_scheduler_http/README.md) 
-   [Kubernetes cluster state by HTTP](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/app/kubernetes_http/kubernetes_state_http/README.md)  
-   [Microsoft SharePoint by HTTP](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/app/sharepoint_http/README.md) 
-   [NetApp AFF A700 by HTTP](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/san/netapp_aff_a700_http/README.md) 
-   [NGINX by HTTP](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/app/nginx_http/README.md) 
-   [NGINX Plus by HTTP](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/app/nginx_plus_http/README.md) 
-   [OpenWeatherMap by HTTP](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/app/openweathermap_http/README.md) 
-   [PHP-FPM by HTTP](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/app/php-fpm_agent/README.md) 
-   [Proxmox VE by HTTP](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/app/proxmox/README.md) 
-   [RabbitMQ cluster by HTTP](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/app/rabbitmq_http/README.md) 
-   [TiDB by HTTP](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/db/tidb_http/tidb_tidb_http/README.md) 
-   [TiDB PD by HTTP](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/db/tidb_http/tidb_pd_http/README.md) 
-   [TiDB TiKV by HTTP](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/db/tidb_http/tidb_tikv_http/README.md) 
-   [Travis CI by HTTP](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/app/travis_ci_http/README.md) 
-   [Veeam Backup Enterprise Manager by HTTP](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/app/veeam/enterprise_manager_http/README.md)
-   [Veeam Backup and Replication by HTTP](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/app/veeam/backup_replication_http/README.md)
-   [VMware SD-WAN VeloCloud by HTTP](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/net/velocloud_http/README.md) 
-   [ZooKeeper by HTTP](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/app/zookeeper_http/README.md) 

[comment]: # ({/42ff580b-ebdbb780})
