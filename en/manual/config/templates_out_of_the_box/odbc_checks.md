[comment]: # ({302fae5b-69d56449})
# ODBC template operation

Steps to ensure correct operation of templates that collect metrics via
[ODBC monitoring](/manual/config/items/itemtypes/odbc_checks):

1\. Make sure that required ODBC driver is installed on Zabbix server or
proxy.\
2. [Link](/manual/config/templates/linking#linking_a_template) the
template to a target host (if the template is not available in your
Zabbix installation, you may need to import the template's .xml file
first - see [Templates
out-of-the-box](/manual/config/templates_out_of_the_box) section for
instructions).\
3. If necessary, adjust the values of template macros.\
4. Configure the instance being monitored to allow sharing data with
Zabbix.

A detailed description of a template, including the full list of macros, items and triggers is available in the template's Readme.md file (accessible by clicking on a template name). 

The following templates are available:

-   [MSSQL by ODBC](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/db/mssql_odbc/README.md) 
-   [MySQL by ODBC](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/db/mysql_odbc/README.md) 
-   [Oracle by ODBC](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/db/oracle_odbc/README.md) 

[comment]: # ({/302fae5b-69d56449})
