[comment]: # ({7a75c626-7a75c626})
# Standardized templates for network devices

[comment]: # ({/7a75c626-7a75c626})

[comment]: # ({98ed32f5-1dc52a1f})
#### Overview

In order to provide monitoring for network devices such as switches and
routers, we have created two so-called models: for the network device
itself (its chassis basically) and for network interface.

Since Zabbix 3.4 templates for many families of network devices are
provided. All templates cover (where possible to get these items from
the device):

-   Chassis fault monitoring (power supplies, fans and temperature,
    overall status)
-   Chassis performance monitoring (CPU and memory items)
-   Chassis inventory collection (serial numbers, model name, firmware
    version)
-   Network interface monitoring with IF-MIB and EtherLike-MIB
    (interface status, interface traffic load, duplex status for
    Ethernet)

These templates are available:

-   In new installations - in *Data collection* → *Templates*;
-   If you are upgrading from previous versions, you can find these
    templates in the *zabbix/templates* directory of the downloaded latest
    Zabbix version. While in *Data collection* → *Templates* you can
    import them manually from this directory.

If you are importing the new out-of-the-box templates, you may want to
also update the `@Network interfaces for discovery` global regular
expression to:

    Result is FALSE: ^Software Loopback Interface
    Result is FALSE: ^(In)?[lL]oop[bB]ack[0-9._]*$
    Result is FALSE: ^NULL[0-9.]*$
    Result is FALSE: ^[lL]o[0-9.]*$
    Result is FALSE: ^[sS]ystem$
    Result is FALSE: ^Nu[0-9.]*$

to filter out loopbacks and null interfaces on most systems.

[comment]: # ({/98ed32f5-1dc52a1f})

[comment]: # ({6a6858c9-b5f67cef})
#### Devices

List of device families for which templates are available:

|Template name|Vendor|Device family|Known models|OS|MIBs used|**[Tags](/manual/config/templates_out_of_the_box/network_devices#tags)**|
|----|--|--|----|--|----|-----|
|*Alcatel Timetra TiMOS SNMP*|Alcatel|Alcatel Timetra|ALCATEL SR 7750|TiMOS|TIMETRA-SYSTEM-MIB,TIMETRA-CHASSIS-MIB|Certified|
|*Brocade FC SNMP*|Brocade|Brocade FC switches|Brocade 300 SAN Switch-|\-|SW-MIB,ENTITY-MIB|Performance, Fault|
|*Brocade\_Foundry Stackable SNMP*|Brocade|Brocade ICX|Brocade ICX6610, Brocade ICX7250-48, Brocade ICX7450-48F| |FOUNDRY-SN-AGENT-MIB, FOUNDRY-SN-STACKING-MIB|Certified|
|*Brocade\_Foundry Nonstackable SNMP*|Brocade, Foundry|Brocade MLX, Foundry|Brocade MLXe, Foundry FLS648, Foundry FWSX424| |FOUNDRY-SN-AGENT-MIB|Performance, Fault|
|*Cisco Catalyst 3750<device model> SNMP*|Cisco|Cisco Catalyst 3750|Cisco Catalyst 3750V2-24FS, Cisco Catalyst 3750V2-24PS, Cisco Catalyst 3750V2-24TS, Cisco Catalyst SNMP, Cisco Catalyst SNMP| |CISCO-MEMORY-POOL-MIB, IF-MIB, EtherLike-MIB, SNMPv2-MIB, CISCO-PROCESS-MIB, CISCO-ENVMON-MIB, ENTITY-MIB|Certified|
|*Cisco IOS SNMP*|Cisco|Cisco IOS ver > 12.2 3.5|Cisco C2950|IOS|CISCO-PROCESS-MIB,CISCO-MEMORY-POOL-MIB,CISCO-ENVMON-MIB|Certified|
|*Cisco IOS versions 12.0\_3\_T-12.2\_3.5 SNMP*|Cisco|Cisco IOS > 12.0 3 T and < 12.2 3.5|\-|IOS|CISCO-PROCESS-MIB,CISCO-MEMORY-POOL-MIB,CISCO-ENVMON-MIB|Certified|
|*Cisco IOS prior to 12.0\_3\_T SNMP*|Cisco|Cisco IOS < 12.0 3 T|\-|IOS|OLD-CISCO-CPU-MIB,CISCO-MEMORY-POOL-MIB|Certified|
|*D-Link DES\_DGS Switch SNMP*|D-Link|DES/DGX switches|D-Link DES-xxxx/DGS-xxxx,DLINK DGS-3420-26SC|\-|DLINK-AGENT-MIB,EQUIPMENT-MIB,ENTITY-MIB|Certified|
|*D-Link DES 7200 SNMP*|D-Link|DES-7xxx|D-Link DES 7206|\-|ENTITY-MIB,MY-SYSTEM-MIB,MY-PROCESS-MIB,MY-MEMORY-MIB|Performance Fault Interfaces|
|*Dell Force S-Series SNMP*|Dell|Dell Force S-Series|S4810| |F10-S-SERIES-CHASSIS-MIB|Certified|
|*Extreme Exos SNMP*|Extreme|Extreme EXOS|X670V-48x|EXOS|EXTREME-SYSTEM-MIB,EXTREME-SOFTWARE-MONITOR-MIB|Certified|
|*Huawei VRP SNMP*|Huawei|Huawei VRP|S2352P-EI|\-|ENTITY-MIB,HUAWEI-ENTITY-EXTENT-MIB|Certified|
|*Intel\_Qlogic Infiniband SNMP*|Intel/QLogic|Intel/QLogic Infiniband devices|Infiniband 12300| |ICS-CHASSIS-MIB|Fault Inventory|
|*Juniper SNMP*|Juniper|MX,SRX,EX models|Juniper MX240, Juniper EX4200-24F|JunOS|JUNIPER-MIB|Certified|
|*Mellanox SNMP*|Mellanox|Mellanox Infiniband devices|SX1036|MLNX-OS|HOST-RESOURCES-MIB,ENTITY-MIB,ENTITY-SENSOR-MIB,MELLANOX-MIB|Certified|
|*MikroTik CCR<device model> SNMP*|MikroTik|MikroTik Cloud Core Routers (CCR series)|Separate dedicated templates are available for MikroTik CCR1009-7G-1C-1S+, MikroTik CCR1009-7G-1C-1S+PC, MikroTik CCR1009-7G-1C-PC, MikroTik CCR1016-12G, MikroTik CCR1016-12S-1S+, MikroTik CCR1036-12G-4S-EM, MikroTik CCR1036-12G-4S, MikroTik CCR1036-8G-2S+, MikroTik CCR1036-8G-2S+EM, MikroTik CCR1072-1G-8S+, MikroTik CCR2004-16G-2S+, MikroTik CCR2004-1G-12S+2XS|RouterOS|MIKROTIK-MIB,HOST-RESOURCES-MIB|Certified|
|*MikroTik CRS<device model> SNMP*|MikroTik|MikroTik Cloud Router Switches (CRS series)|Separate dedicated templates are available for MikroTik CRS106-1C-5S, MikroTik CRS109-8G-1S-2HnD-IN, MikroTik CRS112-8G-4S-IN, MikroTik CRS112-8P-4S-IN, MikroTik CRS125-24G-1S-2HnD-IN, MikroTik CRS212-1G-10S-1S+IN, MikroTik CRS305-1G-4S+IN, MikroTik CRS309-1G-8S+IN, MikroTik CRS312-4C+8XG-RM, MikroTik CRS317-1G-16S+RM, MikroTik CRS326-24G-2S+IN, MikroTik CRS326-24G-2S+RM, MikroTik CRS326-24S+2Q+RM, MikroTik CRS328-24P-4S+RM, MikroTik CRS328-4C-20S-4S+RM, MikroTik CRS354-48G-4S+2Q+RM, MikroTik CRS354-48P-4S+2Q+RM|RouterOS/SwitchOS|MIKROTIK-MIB,HOST-RESOURCES-MIB|Certified|
|*MikroTik CSS<device model> SNMP*|MikroTik|MikroTik Cloud Smart Switches (CSS series)|Separate dedicated templates are available for MikroTik CSS326-24G-2S+RM, MikroTik CSS610-8G-2S+IN|RouterOS|MIKROTIK-MIB,HOST-RESOURCES-MIB|Certified|
|*MikroTik FiberBox SNMP*|MikroTik|MikroTik FiberBox|MikroTik FiberBox|RouterOS|MIKROTIK-MIB,HOST-RESOURCES-MIB|Certified|
|*MikroTik hEX <device model> SNMP*|MikroTik|MikroTik hEX|Separate dedicated templates are available for MikroTik hEX, MikroTik hEX lite, MikroTik hEX PoE, MikroTik hEX PoE lite, MikroTik hEX S|RouterOS|MIKROTIK-MIB,HOST-RESOURCES-MIB|Certified|
|*MikroTik netPower <device model> SNMP*|MikroTik|MikroTik netPower|Separate dedicated templates are available for MikroTik netPower 15FR, MikroTik netPower 16P SNMP, MikroTik netPower Lite 7R|RouterOS/SwitchOS, SwitchOS Lite|MIKROTIK-MIB,HOST-RESOURCES-MIB|Certified|
|*MikroTik PowerBox <device model> SNMP*|MikroTik|MikroTik PowerBox|Separate dedicated templates are available for MikroTik PowerBox, MikroTik PowerBox Pro|RouterOS|MIKROTIK-MIB,HOST-RESOURCES-MIB|Certified|
|*MikroTik RB<device model> SNMP*|MikroTik|MikroTik RB series routers|Separate dedicated templates are available for MikroTik RB1100AHx4, MikroTik RB1100AHx4 Dude Edition, MikroTik RB2011iL-IN, MikroTik RB2011iL-RM, MikroTik RB2011iLS-IN, MikroTik RB2011UiAS-IN, MikroTik RB2011UiAS-RM, MikroTik RB260GS, MikroTik RB3011UiAS-RM, MikroTik RB4011iGS+RM, MikroTik RB5009UG+S+IN|RouterOS|MIKROTIK-MIB,HOST-RESOURCES-MIB|Certified|
|*MikroTik SNMP*|MikroTik|MikroTik RouterOS devices|MikroTik CCR1016-12G, MikroTik RB2011UAS-2HnD, MikroTik 912UAG-5HPnD, MikroTik 941-2nD, MikroTik 951G-2HnD, MikroTik 1100AHx2|RouterOS|MIKROTIK-MIB,HOST-RESOURCES-MIB|Certified|
|*QTech QSW SNMP*|QTech|Qtech devices|Qtech QSW-2800-28T|\-|QTECH-MIB,ENTITY-MIB|Performance Inventory|
|*Ubiquiti AirOS SNMP*|Ubiquiti|Ubiquiti AirOS wireless devices|NanoBridge,NanoStation,Unifi|AirOS|FROGFOOT-RESOURCES-MIB,IEEE802dot11-MIB|Performance|
|*HP Comware HH3C SNMP*|HP|HP (H3C) Comware|HP A5500-24G-4SFP HI Switch| |HH3C-ENTITY-EXT-MIB,ENTITY-MIB|Certified|
|*HP Enterprise Switch SNMP*|HP|HP Enterprise Switch|HP ProCurve J4900B Switch 2626, HP J9728A 2920-48G Switch| |STATISTICS-MIB,NETSWITCH-MIB,HP-ICF-CHASSIS,ENTITY-MIB,SEMI-MIB|Certified|
|*TP-LINK SNMP*|TP-LINK|TP-LINK|T2600G-28TS v2.0| |TPLINK-SYSMONITOR-MIB,TPLINK-SYSINFO-MIB|Performance Inventory|
|*Netgear Fastpath SNMP*|Netgear|Netgear Fastpath|M5300-28G| |FASTPATH-SWITCHING-MIB,FASTPATH-BOXSERVICES-PRIVATE-MIB|Fault Inventory|

[comment]: # ({/6a6858c9-b5f67cef})

[comment]: # ({8c7ba32a-07c98c5a})
#### Template design

Templates were designed with the following in mind:

-   User macros are used as much as possible so triggers can be tuned by
    the user;
-   Low-level discovery is used as much as possible to minimize the
    number of unsupported items;
-   All templates depend on Template ICMP Ping so all devices are also
    checked by ICMP;
-   Items don't use any MIBs - SNMP OIDs are used in items and low-level
    discoveries. So it's not necessary to load any MIBs into Zabbix for
    templates to work;
-   Loopback network interfaces are filtered when discovering as well as
    interfaces with ifAdminStatus = down(2)
-   64bit counters are used from IF-MIB::ifXTable where possible. If it
    is not supported, default 32bit counters are used instead.
 
All discovered network interfaces have a trigger that monitors its operational status (link), for example:

   ```
    {$IFCONTROL:"{#IFNAME}"}=1 and last(/Alcatel Timetra TiMOS SNMP/net.if.status[ifOperStatus.{#SNMPINDEX}])=2 and (last(/Alcatel Timetra TiMOS SNMP/net.if.status[ifOperStatus.{#SNMPINDEX}],#1)<>last(/Alcatel Timetra TiMOS SNMP/net.if.status[ifOperStatus.{#SNMPINDEX}],#2))
   ```

-   If you do no want to monitor this condition for a specific
    interface create a user macro with context with the value 0. For example:

![](../../../../assets/en/manual/config/template_ifcontrol.png)

where Gi0/0 is {\#IFNAME}. That way the trigger is not used any more for
this specific interface.

-   You can also change the default behavior for all triggers not to fire and activate this trigger only to limited number of interfaces like uplinks:

![](../../../../assets/en/manual/config/template_ifcontrol2.png)

[comment]: # ({/8c7ba32a-07c98c5a})

[comment]: # ({6d84c190-6d84c190})
#### Tags

-   Performance – device family MIBs provide a way to monitor CPU and
    memory items;
-   Fault - device family MIBs provide a way to monitor at least one
    temperature sensor;
-   Inventory – device family MIBs provide a way to collect at least the
    device serial number and model name;
-   Certified – all three main categories above are covered.

[comment]: # ({/6d84c190-6d84c190})
