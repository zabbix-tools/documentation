[comment]: # ({47cd751e-7d2aa6f8})
# IPMI template operation

IPMI templates do not require any specific setup. To start monitoring,
[link](/manual/config/templates/linking#linking_a_template) the template
to a target host (if the template is not available in your Zabbix
installation, you may need to import the template's .xml file first -
see [Templates out-of-the-box](/manual/config/templates_out_of_the_box)
section for instructions).\

A detailed description of a template, including the full list of macros, items and triggers is available in the template's Readme.md file (accessible by clicking on a template name). 

Available template:

-  [Chassis by IPMI](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/server/chassis_ipmi/README.md) 

[comment]: # ({/47cd751e-7d2aa6f8})
