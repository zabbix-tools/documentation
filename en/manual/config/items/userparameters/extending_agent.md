[comment]: # ({ffe98fbd-ffe98fbd})
# 1 Extending Zabbix agents

This tutorial provides step-by-step instructions on how to extend the
functionality of Zabbix agent with the use of a [user
parameter](/manual/config/items/userparameters).

[comment]: # ({/ffe98fbd-ffe98fbd})

[comment]: # ({8f2323c0-8f2323c0})
##### Step 1

Write a script or command line to retrieve required parameter.

For example, we may write the following command in order to get total
number of queries executed by a MySQL server:

    mysqladmin -uroot status | cut -f4 -d":" | cut -f1 -d"S"

When executed, the command returns total number of SQL queries.

[comment]: # ({/8f2323c0-8f2323c0})

[comment]: # ({6117d271-6117d271})
##### Step 2

Add the command to zabbix\_agentd.conf:

    UserParameter=mysql.questions,mysqladmin -uroot status | cut -f4 -d":" | cut -f1 -d"S"

**mysql.questions** is a unique identifier. It can be any valid key
identifier, for example, *queries*.

Test this parameter by using Zabbix agent with "-t" flag (if running
under root, however, note that the agent may have different permissions
when launched as a daemon):

    zabbix_agentd -t mysql.questions

[comment]: # ({/6117d271-6117d271})

[comment]: # ({67b38df5-67b38df5})
##### Step 3

Reload user parameters from the configuration file by running:

    zabbix_agentd -R userparameter_reload

You may also restart the agent instead of the runtime control command.

Test the parameter by using [zabbix\_get](/manual/concepts/get) utility.

[comment]: # ({/67b38df5-67b38df5})

[comment]: # ({4a46fd71-4a46fd71})
##### Step 4

Add new item with Key=mysql.questions to the monitored host. Type of the
item must be either Zabbix Agent or Zabbix Agent (active).

Be aware that type of returned values must be set correctly on Zabbix
server. Otherwise Zabbix won't accept them.

[comment]: # ({/4a46fd71-4a46fd71})
