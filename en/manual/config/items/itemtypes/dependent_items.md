[comment]: # ({02a3a9ac-02a3a9ac})
# 15 Dependent items

[comment]: # ({/02a3a9ac-02a3a9ac})

[comment]: # ({4eb72974-4eb72974})
#### Overview

There are situations when one item gathers multiple metrics at a time or
it even makes more sense to collect related metrics simultaneously, for
example:

-   CPU utilization of individual cores
-   Incoming/outgoing/total network traffic

To allow for bulk metric collection and simultaneous use in several
related items, Zabbix supports dependent items. Dependent items depend
on the master item that collects their data simultaneously, in one
query. A new value for the master item automatically populates the
values of the dependent items. Dependent items cannot have a different
update interval than the master item.

Zabbix preprocessing options can be used to extract the part that is
needed for the dependent item from the master item data.

Preprocessing is managed by a `preprocessing manager` process, which has
been added in Zabbix 3.4, along with workers that perform the
preprocessing steps. All values (with or without preprocessing) from
different data gatherers pass through the preprocessing manager before
being added to the history cache. Socket-based IPC communication is used
between data gatherers (pollers, trappers, etc) and the preprocessing
process.

Zabbix server or Zabbix proxy (if host is monitored by proxy) are
performing preprocessing steps and processing dependent items.

Item of any type, even dependent item, can be set as master item.
Additional levels of dependent items can be used to extract smaller
parts from the value of an existing dependent item.

[comment]: # ({/4eb72974-4eb72974})

[comment]: # ({3e1d96f5-3e1d96f5})
##### Limitations

-   Only same host (template) dependencies are allowed
-   An item prototype can depend on another item prototype or regular
    item from the same host
-   Maximum count of dependent items for one master item is limited to
    29999 (regardless of the number of dependency levels)
-   Maximum 3 dependency levels allowed
-   Dependent item on a host with master item from template will not be
    exported to XML

[comment]: # ({/3e1d96f5-3e1d96f5})

[comment]: # ({57c4a4d3-3f705816})
#### Item configuration

A dependent item depends on its master item for data. That is why the
**master item** must be configured (or exist) first:

-   Go to: *Data collection* → *Hosts*
-   Click on *Items* in the row of the host
-   Click on *Create item*
-   Enter parameters of the item in the form

![](../../../../../assets/en/manual/config/items/itemtypes/master_item.png)

All mandatory input fields are marked with a red asterisk.

Click on *Add* to save the master item.

Then you can configure a **dependent item**.

![](../../../../../assets/en/manual/config/items/itemtypes/dependent_item.png)

All mandatory input fields are marked with a red asterisk.

The fields that require specific information for dependent items are:

|   |   |
|--|--------|
|*Type*|Select **Dependent item** here.|
|*Key*|Enter a key that will be used to recognize the item.|
|*Master item*|Select the master item. Master item value will be used to populate dependent item value.|
|*Type of information*|Select the type of information that will correspond the format of data that will be stored.|

You may use item value
[preprocessing](/manual/config/items/item#item_value_preprocessing) to
extract the required part of the master item value.

![](../../../../../assets/en/manual/config/items/itemtypes/dependent_item_preprocessing.png){width="600"}

Without preprocessing, the dependent item value will be exactly the same
as the master item value.

Click on *Add* to save the dependent item.

A shortcut to creating a dependent item quicker can be accessed by clicking on the ![](../../../../../assets/en/manual/config/items/itemtypes/dependent_item_button.png) button in the item list
and selecting *Create dependent item*.

![](../../../../../assets/en/manual/config/items/itemtypes/dependent_item_menu.png){width="350"}

[comment]: # ({/57c4a4d3-3f705816})

[comment]: # ({8323a97a-8323a97a})
##### Display

In the item list dependent items are displayed with their master item
name as prefix.

![](../../../../../assets/en/manual/config/items/itemtypes/dependent_items.png){width="600"}

If a master item is deleted, so are all its dependent items.

[comment]: # ({/8323a97a-8323a97a})
