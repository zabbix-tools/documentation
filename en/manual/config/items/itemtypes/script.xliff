<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="en" datatype="plaintext" original="manual/config/items/itemtypes/script.md">
    <body>
      <trans-unit id="1a1e2756" xml:space="preserve">
        <source># 18 Script items</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/config/items/itemtypes/script.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="cb89bc38" xml:space="preserve">
        <source>### Overview

Script items can be used to collect data by executing a user-defined
JavaScript code with the ability to retrieve data over HTTP/HTTPS. In
addition to the script, an optional list of parameters (pairs of name
and value) and timeout can be specified.

This item type may be useful in data collection scenarios that require
multiple steps or complex logic. As an example, a Script item can be
configured to make an HTTP call, then process the data received in the
first step in some way, and pass transformed value to the second HTTP
call.

Script items are processed by Zabbix server or proxy pollers.</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/config/items/itemtypes/script.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="7ed55e87" xml:space="preserve">
        <source>### Configuration

In the *Type* field of [item configuration
form](/manual/config/items/item) select Script then fill out required
fields.

![script\_item.png](../../../../../assets/en/manual/config/items/itemtypes/script_item.png)

All mandatory input fields are marked with a red asterisk.

The fields that require specific information for Script items are:

|Field|Description|
|--|--------|
|Key|Enter a unique key that will be used to identify the item.|
|Parameters|Specify the variables to be passed to the script as the attribute and value pairs.&lt;br&gt;[User macros](/manual/config/macros/user_macros) are supported. To see which built-in macros are supported, do a search for "Script-type item" in the [supported macro](/manual/appendix/macros/supported_by_location) table.|
|Script|Enter JavaScript code in the block that appears when clicking in the parameter field (or on the view/edit button next to it). This code must provide the logic for returning the metric value.&lt;br&gt;The code has access to all parameters, it may perform HTTP GET, POST, PUT and DELETE requests and has control over HTTP headers and request body.&lt;br&gt;See also: [Additional JavaScript objects](/manual/config/items/preprocessing/javascript/javascript_objects), [JavaScript Guide](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide).|
|Timeout|JavaScript execution timeout (1-60s, default 3s); exceeding it will return error.&lt;br&gt;Time suffixes are supported, e.g. 30s, 1m.&lt;br&gt;Depending on the script it might take longer for the timeout to trigger. |</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/config/items/itemtypes/script.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="4c860844" xml:space="preserve">
        <source>### Examples</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/config/items/itemtypes/script.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="9c6c78ee" xml:space="preserve">
        <source>##### Simple data collection

Collect the content of *https://www.example.com/release\_notes*:

-   Create an item with type "Script".
-   In the *Script* field, enter:

```javascript
var request = new HttpRequest();
return request.get("https://www.example.com/release_notes");
```</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/config/items/itemtypes/script.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="7115c5ef" xml:space="preserve">
        <source>##### Data collection with parameters

Collect the content of a specific page and make use of parameters: 

- Create an item with type "Script" and two parameters:
    - **url : {$DOMAIN}** (the user macro {$DOMAIN} should be defined, preferably on the host level)
    - **subpage : /release_notes**

![](../../../../../assets/en/manual/config/items/itemtypes/script_example1.png){width=600}

- In the *Script* field, enter: 

```javascript
var obj = JSON.parse(value);
var url = obj.url;
var subpage = obj.subpage;
var request = new HttpRequest();
return request.get(url + subpage);
```</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/config/items/itemtypes/script.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="18a089f4" xml:space="preserve">
        <source>##### Multiple HTTP requests

Collect the content of both *https://www.example.com* and
*https://www.example.com/release\_notes*:

-   Create an item with type "Script".
-   In the *Script* field, enter:

```javascript
var request = new HttpRequest();
return request.get("https://www.example.com") + request.get("https://www.example.com/release_notes");
```</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/config/items/itemtypes/script.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="d837bcdd" xml:space="preserve">
        <source>##### Logging

Add the "Log test" entry to the Zabbix server log and receive the item
value "1" in return:

-   Create an item with type "Script".
-   In the *Script* field, enter:

```javascript
Zabbix.log(3, 'Log test');
return 1;
```</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/config/items/itemtypes/script.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
    </body>
  </file>
</xliff>
