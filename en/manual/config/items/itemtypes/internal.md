[comment]: # attributes: notoc

[comment]: # (tags: internal items, internal item, items)

[comment]: # ({8981e17d-8981e17d})
# 8 Internal checks

[comment]: # ({/8981e17d-8981e17d})

[comment]: # ({ed000da1-34928067})
#### Overview

Internal checks allow to monitor the internal processes of Zabbix. In other words, 
you can monitor what goes on with Zabbix server or Zabbix proxy.

Internal checks are calculated:

-   on Zabbix server - if the host is monitored by server
-   on Zabbix proxy - if the host is monitored by proxy

Internal checks are processed by server or proxy regardless of the host maintenance status.

To use this item, choose the **Zabbix internal** item type.

::: notetip
Internal checks are processed by Zabbix pollers.
:::

[comment]: # ({/ed000da1-34928067})

[comment]: # ({a1f5f51e-5d8f3b63})
#### Performance

Using some internal items may negatively affect performance. These items are:

-   `zabbix[host,,items]`
-   `zabbix[host,,items_unsupported]`
-   `zabbix[hosts]`
-   `zabbix[items]`
-   `zabbix[items_unsupported]`
-   `zabbix[queue]`
-   `zabbix[requiredperformance]`
-   `zabbix[stats,,,queue]`
-   `zabbix[triggers]`

The [System information](/manual/web_interface/frontend_sections/reports/status_of_zabbix) and [Queue](/manual/web_interface/frontend_sections/administration/queue) 
frontend sections are also affected.

[comment]: # ({/a1f5f51e-5d8f3b63})

[comment]: # ({91ebcdc6-74b446f4})
#### Supported checks

The item keys are listed without optional parameters and additional information. Click on the item key to see the full details.

|Item key|Description|
|--|--------|
|[zabbix\[boottime\]](#boottime)|The startup time of Zabbix server or Zabbix proxy process in seconds.|
|[zabbix\[cluster,discovery,nodes\]](#cluster.discovery)|Discovers the [high availability cluster](/manual/concepts/server/ha) nodes.|
|[zabbix\[connector_queue\]](#connector.queue)|The count of values enqueued in the connector queue.|
|[zabbix\[discovery_queue\]](#discovery.queue)|The count of network checks enqueued in the discovery queue.|
|[zabbix\[host,,items\]](#host.items)|The number of enabled items (supported and not supported) on the host.|
|[zabbix\[host,,items_unsupported\]](#host.items.unsupported)|The number of enabled unsupported items on the host.|
|[zabbix\[host,,maintenance\]](#maintenance)|The current maintenance status of the host.|
|[zabbix\[host,active_agent,available\]](#active.available)|The availability of active agent checks on the host.|
|[zabbix\[host,discovery,interfaces\]](#discovery.interfaces)|The details of all configured interfaces of the host in Zabbix frontend.|
|[zabbix\[host,available\]](#host.available)|The availability of the main interface of a particular type of checks on the host.|
|[zabbix\[hosts\]](#hosts)|The number of monitored hosts.|
|[zabbix\[items\]](#items)|The number of enabled items (supported and not supported).|
|[zabbix\[items_unsupported\]](#items.unsupported)|The number of unsupported items.|
|[zabbix\[java\]](#java)|The information about Zabbix Java gateway.|
|[zabbix\[lld_queue\]](#lld.queue)|The count of values enqueued in the low-level discovery processing queue.|
|[zabbix\[preprocessing_queue\]](#preprocessing.queue)|The count of values enqueued in the preprocessing queue.|
|[zabbix\[process\]](#process)|The percentage of time a particular Zabbix process or a group of processes (identified by \<type\> and \<mode\>) spent in \<state\>.|
|[zabbix\[proxy\]](#proxy)|The information about Zabbix proxy.|
|[zabbix\[proxy,discovery\]](#proxy.discovery)|The list of Zabbix proxies.|
|[zabbix\[proxy_history\]](#proxy.history)|The number of values in the proxy history table waiting to be sent to the server.|
|[zabbix\[queue\]](#queue)|The number of monitored items in the queue which are delayed at least by \<from\> seconds, but less than \<to\> seconds.|
|[zabbix\[rcache\]](#rcache)|The availability statistics of the Zabbix configuration cache.|
|[zabbix\[requiredperformance\]](#required.performance)|The required performance of Zabbix server or Zabbix proxy, in new values per second expected.|
|[zabbix\[stats\]](#stats)|The internal metrics of a remote Zabbix server or proxy.|
|[zabbix\[stats,,,queue\]](#stats.queue)|The internal queue metrics of a remote Zabbix server or proxy.|
|[zabbix\[tcache\]](#tcache)|The effectiveness statistics of the Zabbix trend function cache.|
|[zabbix\[triggers\]](#triggers)|The number of enabled triggers in Zabbix database, with all items enabled on enabled hosts.|
|[zabbix\[uptime\]](#uptime)|The uptime of the Zabbix server or proxy process in seconds.|
|[zabbix\[vcache,buffer\]](#vcache)|The availability statistics of the Zabbix value cache.|
|[zabbix\[vcache,cache\]](#vcache.parameter)|The effectiveness statistics of the Zabbix value cache.|
|[zabbix\[version\]](#version)|The version of Zabbix server or proxy.|
|[zabbix\[vmware,buffer\]](#vmware)|The availability statistics of the Zabbix vmware cache.|
|[zabbix\[wcache\]](#wcache)|The statistics and availability of the Zabbix write cache.|

[comment]: # ({/91ebcdc6-74b446f4})

[comment]: # ({706c3c6b-0f6066ab})

### Item key details

-   Parameters without angle brackets are constants - for example,
    'host' and 'available' in `zabbix[host,<type>,available]`. Use them in the item key *as is*.
-   Values for items and item parameters that are "not supported on proxy" can only be retrieved if the host is monitored by server. And vice versa, values "not supported on server" can only be retrieved if the host is monitored by proxy.

[comment]: # ({/706c3c6b-0f6066ab})

[comment]: # ({148a8a10-e52bbd1a})

##### zabbix[boottime] {#boottime}

<br>
The startup time of Zabbix server or Zabbix proxy process in seconds.<br>
Return value: *Integer*.

[comment]: # ({/148a8a10-e52bbd1a})

[comment]: # ({327865ff-d1cde038})

##### zabbix[cluster,discovery,nodes] {#cluster.discovery}

<br>
Discovers the [high availability cluster](/manual/concepts/server/ha) nodes.<br>
Return value: *JSON object*.

This item can be used in low-level discovery.

[comment]: # ({/327865ff-d1cde038})

[comment]: # ({3d077ed4-09c4f0cf})

##### zabbix[connector_queue] {#connector.queue}

<br>
The count of values enqueued in the connector queue.<br>
Return value: *Integer*.

This item is supported since Zabbix 6.4.0.

[comment]: # ({/3d077ed4-09c4f0cf})

[comment]: # ({15dcffea-fcd989ee})

##### zabbix[discovery_queue] {#discovery.queue}

<br>
The count of network checks enqueued in the discovery queue.<br>
Return value: *Integer*.

[comment]: # ({/15dcffea-fcd989ee})

[comment]: # ({099bfd88-f065c85f})

##### zabbix[host,,items] {#host.items}

<br>
The number of enabled items (supported and not supported) on the host.<br>
Return value: *Integer*.

[comment]: # ({/099bfd88-f065c85f})

[comment]: # ({4b7ee3b8-31a0bdcc})

##### zabbix[host,,items_unsupported] {#host.items.unsupported}

<br>
The number of enabled unsupported items on the host.<br>
Return value: *Integer*.

[comment]: # ({/4b7ee3b8-31a0bdcc})

[comment]: # ({e4711a6f-6220db31})

##### zabbix[host,,maintenance] {#maintenance}

<br>
The current maintenance status of the host.<br>
Return values: *0* - normal state; *1* - maintenance with data collection; *2* - maintenance without data collection.

Comments:

-   This item is always processed by Zabbix server regardless of the host location (on server or proxy). The proxy will not receive this item with configuration data. 
-   The second parameter must be empty and is reserved for future use.

[comment]: # ({/e4711a6f-6220db31})

[comment]: # ({c0f07926-ec7187d4})

##### zabbix[host,active_agent,available] {#active.available}

<br>
The availability of active agent checks on the host.<br>
Return values: *0* - unknown; *1* - available; *2* - not available.

[comment]: # ({/c0f07926-ec7187d4})

[comment]: # ({e96b325e-48ad1e26})

##### zabbix[host,discovery,interfaces] {#discovery.interfaces}

<br>
The details of all configured interfaces of the host in Zabbix frontend.<br>
Return value: *JSON object*.

Comments:

-   This item can be used in [low-level discovery](/manual/discovery/low_level_discovery/examples/host_interfaces);
-   This item is not supported on Zabbix proxy.

[comment]: # ({/e96b325e-48ad1e26})

[comment]: # ({2b0474be-9752a684})

##### zabbix[host,<type>,available] {#host.available}

<br>
The availability of the main interface of a particular type of checks on the host.<br>
Return values: *0* - not available; *1* - available; *2* - unknown.

Comments:

-   Valid **types** are: *agent*, *snmp*, *ipmi*, *jmx*;
-   The item value is calculated according to the configuration parameters regarding host [unreachability/unavailability](/manual/appendix/items/unreachability).

[comment]: # ({/2b0474be-9752a684})

[comment]: # ({e4039f9c-a65ef5bf})

##### zabbix[hosts] {#hosts}

<br>
The number of monitored hosts.<br>
Return value: *Integer*.

[comment]: # ({/e4039f9c-a65ef5bf})

[comment]: # ({6cfb760d-f291ac50})

##### zabbix[items] {#items}

<br>
The number of enabled items (supported and not supported).<br>
Return value: *Integer*.

[comment]: # ({/6cfb760d-f291ac50})

[comment]: # ({241ba13b-53295b6d})

##### zabbix[items_unsupported] {#items.unsupported}

<br>
The number of unsupported items.<br>
Return value: *Integer*.

[comment]: # ({/241ba13b-53295b6d})

[comment]: # ({a49e77f8-ba37fff4})

##### zabbix[java,,<param>] {#java}

<br>
The information about Zabbix Java gateway.<br>
Return values: *1* - if <param> is *ping*; *Java gateway version* -  if <param> is *version* (for example: "2.0.0").

Comments:

-   Valid values for **param** are: *ping*, *version*;
-   This item can be used to check Java gateway availability using the `nodata()` trigger function;
-   The second parameter must be empty and is reserved for future use.

[comment]: # ({/a49e77f8-ba37fff4})

[comment]: # ({afd9b480-8f8235d6})

##### zabbix[lld_queue] {#lld.queue}

<br>
The count of values enqueued in the low-level discovery processing queue.<br>
Return value: *Integer*.

This item can be used to monitor the low-level discovery processing queue length.

[comment]: # ({/afd9b480-8f8235d6})

[comment]: # ({dfaef764-3e07a93d})

##### zabbix[preprocessing_queue] {#preprocessing.queue}

<br>
The count of values enqueued in the preprocessing queue.<br>
Return value: *Integer*.

This item can be used to monitor the preprocessing queue length.

[comment]: # ({/dfaef764-3e07a93d})

[comment]: # ({15146f7f-b74b9537})

##### zabbix[process,<type>,<mode>,<state>] {#process}

<br>
The percentage of time a particular Zabbix process or a group of processes (identified by \<type\> and \<mode\>) spent in \<state\>. It is calculated for the last minute only.<br>
Return value: *Float*.

Parameters:

-   **type** - for [server processes](/manual/concepts/server#server_process_types): *alert manager*, *alert syncer*, *alerter*, *availability manager*, *configuration syncer*, *connector manager*, *connector worker*, *discovery manager*, *discovery worker*, *escalator*, *ha manager*, *history poller*, *history syncer*, *housekeeper*, *http poller*, *icmp pinger*, *ipmi manager*, *ipmi poller*, *java poller*, *lld manager*, *lld worker*, *odbc poller*, *poller*, *preprocessing manager*, *preprocessing worker*, *proxy poller*, *self-monitoring*, *service manager*, *snmp trapper*, *task manager*, *timer*, *trapper*, *trigger housekeeper*, *unreachable poller*, *vmware collector*;<br>for [proxy processes](/manual/concepts/proxy#proxy_process_types): *availability manager*, *configuration syncer*, *data sender*, *discovery manager*, *discovery worker*, *history syncer*, *housekeeper*, *http poller*, *icmp pinger*, *ipmi manager*, *ipmi poller*, *java poller*, *odbc poller*, *poller*, *preprocessing manager*, *preprocessing worker*, *self-monitoring*, *snmp trapper*, *task manager*, *trapper*, *unreachable poller*, *vmware collector*
-   **mode** - *avg* - average value for all processes of a given type (default)<br>*count* - returns number of forks for a given process type, <state> should not be specified<br>*max* - maximum value<br>*min* - minimum value<br>*<process number>* - process number (between 1 and the number of pre-forked instances). For example, if 4 trappers are running, the value is between 1 and 4.
-   **state** - *busy* - process is in busy state, for example, the processing request (default); *idle* - process is in idle state doing nothing.

Comments:

-   If \<mode\> is a Zabbix process number that is not running (for example, with 5 pollers running the \<mode\> is specified to be 6), such an item will turn unsupported;
-   Minimum and maximum refers to the usage percentage for a single process. So if in a group of 3 pollers usage percentages per process were 2, 18 and 66, min would return 2 and max would return 66.
-   Processes report what they are doing in shared memory and the self-monitoring process summarizes that data each second. State changes (busy/idle) are registered upon change - thus a process that becomes busy registers as such and doesn't change or update the state until it becomes idle. This ensures that even fully hung processes will be correctly registered as 100% busy.
-   Currently, "busy" means "not sleeping", but in the future additional states might be introduced - waiting for locks, performing database queries, etc.
-   On Linux and most other systems, resolution is 1/100 of a second.

Examples:

    zabbix[process,poller,avg,busy] #the average time of poller processes spent doing something during the last minute
    zabbix[process,"icmp pinger",max,busy] #the maximum time spent doing something by any ICMP pinger process during the last minute
    zabbix[process,"history syncer",2,busy] #the time spent doing something by history syncer number 2 during the last minute
    zabbix[process,trapper,count] #the amount of currently running trapper processes

[comment]: # ({/15146f7f-b74b9537})

[comment]: # ({509862f2-d90bc125})

##### zabbix[proxy,<name>,<param>] {#proxy}

<br>
The information about Zabbix proxy.<br>
Return value: *Integer*.

Parameters:

-   **name** - the proxy name;
-   **param** - *delay* - how long the collected values are unsent, calculated as "proxy delay" (difference between the current proxy time and the timestamp of the oldest unsent value on proxy) + ("current server time" - "proxy lastaccess").

[comment]: # ({/509862f2-d90bc125})

[comment]: # ({9be15d1c-fd9ea3f4})

##### zabbix[proxy,discovery] {#proxy.discovery}

<br>
The list of Zabbix proxies with name, mode, encryption, compression, version, last seen, host count, item count, required values per second (vps) and version status (current/outdated/unsupported).<br>
Return value: *JSON object*.

[comment]: # ({/9be15d1c-fd9ea3f4})

[comment]: # ({adefa0e8-5b463b30})

##### zabbix[proxy_history] {#proxy.history}

<br>
The number of values in the proxy history table waiting to be sent to the server.<br>
Return values: *Integer*.

This item is not supported on Zabbix server.

[comment]: # ({/adefa0e8-5b463b30})

[comment]: # ({75358d3a-ebe20dec})

##### zabbix[queue,<from>,<to>] {#queue}

<br>
The number of monitored items in the queue which are delayed at least by \<from\> seconds, but less than \<to\> seconds.<br>
Return value: *Integer*.

Parameters:

-   **from** - default: 6 seconds;
-   **to** - default: infinity.

[Time-unit symbols](/manual/appendix/suffixes) (s,m,h,d,w) are supported in the parameters.

[comment]: # ({/75358d3a-ebe20dec})

[comment]: # ({f4422d28-04aafd0c})

##### zabbix[rcache,<cache>,<mode>] {#rcache}

<br>
The availability statistics of the Zabbix configuration cache.<br>
Return values: *Integer* (for size); *Float* (for percentage).

Parameters:

-   **cache** - *buffer*;
-   **mode** - *total* - the total size of buffer<br>*free* - the size of free buffer<br>*pfree* - the percentage of free buffer<br>*used* - the size of used buffer<br>*pused* - the percentage of used buffer

[comment]: # ({/f4422d28-04aafd0c})

[comment]: # ({2602301c-f2b37a90})

##### zabbix[requiredperformance] {#required.performance}

<br>
The required performance of Zabbix server or Zabbix proxy, in new values per second expected.<br>
Return value: *Float*.

Approximately correlates with "Required server performance, new values per second" in *Reports → [System information](/manual/web_interface/frontend_sections/reports/status_of_zabbix)*.

[comment]: # ({/2602301c-f2b37a90})

[comment]: # ({13bd1ca6-8054c8ab})

##### zabbix[stats,<ip>,<port>] {#stats}

<br>
The internal metrics of a remote Zabbix server or proxy.<br>
Return values: *JSON object*.

Parameters:

-   **ip** - the IP/DNS/network mask list of servers/proxies to be remotely queried (default is 127.0.0.1);
-   **port** - the port of server/proxy to be remotely queried (default is 10051).

Comments:

-   The stats request will only be accepted from the addresses listed in the 'StatsAllowedIP' [server](/manual/appendix/config/zabbix_server)/[proxy](/manual/appendix/config/zabbix_proxy) parameter on the target instance;
-   A selected set of internal metrics is returned by this item. For details, see [Remote monitoring of Zabbix stats](/manual/appendix/items/remote_stats#exposed_metrics).

[comment]: # ({/13bd1ca6-8054c8ab})

[comment]: # ({0b5874fe-d7c2c9be})

##### zabbix[stats,<ip>,<port>,queue,<from>,<to>] {#stats.queue}

<br>
The internal queue metrics (see `zabbix[queue,<from>,<to>]`) of a remote Zabbix server or proxy.<br>
Return values: *JSON object*.

Parameters:

-   **ip** - the IP/DNS/network mask list of servers/proxies to be remotely queried (default is 127.0.0.1);
-   **port** - the port of server/proxy to be remotely queried (default is 10051);
-   **from** - delayed by at least (default is 6 seconds);
-   **to** - delayed by at most (default is infinity).

Comments:

-   The stats request will only be accepted from the addresses listed in the 'StatsAllowedIP' [server](/manual/appendix/config/zabbix_server)/[proxy](/manual/appendix/config/zabbix_proxy) parameter on the target instance;
-   A selected set of internal metrics is returned by this item. For details, see [Remote monitoring of Zabbix stats](/manual/appendix/items/remote_stats#exposed_metrics).

[comment]: # ({/0b5874fe-d7c2c9be})

[comment]: # ({00bad982-c688d590})

##### zabbix[tcache,<cache>,<parameter>] {#tcache}

<br>
The effectiveness statistics of the Zabbix trend function cache.<br>
Return values: *Integer* (for size); *Float* (for percentage).

Parameters:

-   **cache** - *buffer*;
-   **mode** - *all* - total cache requests (default)<br>*hits* - cache hits<br>*phits* - percentage of cache hits<br>*misses* - cache misses<br>*pmisses* - percentage of cache misses<br>*items* - the number of cached items<br>*requests* - the number of cached requests<br>*pitems* - percentage of cached items from cached items + requests. Low percentage most likely means that the cache size can be reduced.

This item is not supported on Zabbix proxy.

[comment]: # ({/00bad982-c688d590})

[comment]: # ({c7c19511-9699e4ca})

##### zabbix[triggers] {#triggers}

<br>
The number of enabled triggers in Zabbix database, with all items enabled on enabled hosts.<br>
Return value: *Integer*.

This item is not supported on Zabbix proxy.

[comment]: # ({/c7c19511-9699e4ca})

[comment]: # ({3883a5b8-a5c8cdec})

##### zabbix[uptime] {#uptime}

<br>
The uptime of the Zabbix server or proxy process in seconds.<br>
Return value: *Integer*.

[comment]: # ({/3883a5b8-a5c8cdec})

[comment]: # ({fc836916-304cb09c})

##### zabbix[vcache,buffer,<mode>] {#vcache}

<br>
The availability statistics of the Zabbix value cache.<br>
Return values: *Integer* (for size); *Float* (for percentage).

Parameter:

-   **mode** - *total* - the total size of buffer<br>*free* - the size of free buffer<br>*pfree* - the percentage of free buffer<br>*used* - the size of used buffer<br>*pused* - the percentage of used buffer

This item is not supported on Zabbix proxy.

[comment]: # ({/fc836916-304cb09c})

[comment]: # ({60a388d9-1454b994})

##### zabbix[vcache,cache,<parameter>] {#vcache.parameter}

<br>
The effectiveness statistics of the Zabbix value cache.<br>
Return values: *Integer*. With the *mode* parameter returns: 0 - normal mode; 1 - low memory mode.

Parameters:

-   **parameter** - *requests* - the total number of requests<br>*hits* - the number of cache hits (history values taken from the cache)<br>*misses* - the number of cache misses (history values taken from the database)<br>*mode* - the value cache operating mode

Comments:

-   Once the low-memory mode has been switched on, the value cache will remain in this state for 24 hours, even if the problem that triggered this mode is resolved sooner;
-   You may use this key with the *Change per second* preprocessing step in order to get values-per-second statistics;
-   This item is not supported on Zabbix proxy.

[comment]: # ({/60a388d9-1454b994})

[comment]: # ({a737b7c3-ddde46a0})

##### zabbix[version] {#version}

<br>
The version of Zabbix server or proxy.<br>
Return value: *String*. For example: `6.0.0beta1`.

[comment]: # ({/a737b7c3-ddde46a0})

[comment]: # ({aae81fd0-dda0c008})

##### zabbix[vmware,buffer,<mode>] {#vmware}

<br>
The availability statistics of the Zabbix vmware cache.<br>
Return values: *Integer* (for size); *Float* (for percentage).

Parameters:

-   **mode** - *total* - the total size of buffer<br>*free* - the size of free buffer<br>*pfree* - the percentage of free buffer<br>*used* - the size of used buffer<br>*pused* - the percentage of used buffer

[comment]: # ({/aae81fd0-dda0c008})

[comment]: # ({d7ffea4b-9fa76fcd})

##### zabbix[wcache,<cache>,<mode>] {#wcache}

<br>
The statistics and availability of the Zabbix write cache.<br>
Return values: *Integer* (for number/size); *Float* (for percentage).

Parameters:

-   **cache** - *values*, *history*, *index*, or *trend*;
-   **mode** - (with *values*) *all* (default) - the total number of values processed by Zabbix server/proxy, except unsupported items (counter)<br>*float* - the number of processed float values (counter)<br>*uint* - the number of processed unsigned integer values (counter)<br>*str* - the number of processed character/string values (counter)<br>*log* - the number of processed log values (counter)<br>*text* - the number of processed text values (counter)<br>*not supported* - the number of times item processing resulted in item becoming unsupported or keeping that state (counter)<br>(with *history*, *index*, *trend* cache) *pfree* (default) - the percentage of free buffer<br>*total* - the total size of buffer<br>*free* - the size of free buffer<br>*used* - the size of used buffer<br>*pused* - the percentage of used buffer

Comments:

-   Specifying \<cache\> is mandatory. The `trend` cache parameter is not supported with Zabbix proxy.
-   The history cache is used to store item values. A low number indicates performance problems on the database side.
-   The history index cache is used to index the values stored in the history cache;
-   The trend cache stores the aggregate for the current hour for all items that receive data;
-   You may use the zabbix[wcache,values] key with the *Change per second* preprocessing step in order to get values-per-second statistics.

[comment]: # ({/d7ffea4b-9fa76fcd})
