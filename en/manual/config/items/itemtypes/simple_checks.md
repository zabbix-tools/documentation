[comment]: # attributes: notoc

[comment]: # (terms: net.tcp.service, net.tcp.service.perf, net.udp.service, net.udp.service.perf )

[comment]: # (tags: ping, icmpping, icmppingloss )

[comment]: # ({be4ab03d-be4ab03d})
# 5 Simple checks

[comment]: # ({/be4ab03d-be4ab03d})

[comment]: # ({05b34fc5-76023448})
### Overview

Simple checks are normally used for remote agent-less checks of services.

Note that Zabbix agent is not needed for simple checks. Zabbix
server/proxy is responsible for the processing of simple checks (making
external connections, etc).

Examples of using simple checks:

    net.tcp.service[ftp,,155]
    net.tcp.service[http]
    net.tcp.service.perf[http,,8080]
    net.udp.service.perf[ntp]

::: noteclassic
*User name* and *Password* fields (limited to 255 characters) in simple check item configuration are used for VMware monitoring items; ignored otherwise.
:::

[comment]: # ({/05b34fc5-76023448})

[comment]: # ({9aaf2d21-f59a5ccc})
### Supported checks

The item keys are listed without optional parameters and additional information. Click on the item key to see the full details.

See also [VMware monitoring item keys](/manual/vm_monitoring/vmware_keys).

|Item key|Description|
|--|--------|
|[icmpping](#icmpping)|The host accessibility by ICMP ping.|
|[icmppingloss](#icmppingloss)|The percentage of lost packets.|
|[icmppingsec](#icmppingsec)|The ICMP ping response time.|
|[net.tcp.service](#nettcpservice)|Checks if a service is running and accepting TCP connections.|
|[net.tcp.service.perf](#nettcpserviceperf)|Checks the performance of a TCP service.|
|[net.udp.service](#netudpservice)|Checks if a service is running and responding to UDP requests.|
|[net.udp.service.perf](#netudpserviceperf)|Checks the performance of a UDP service.|

[comment]: # ({/9aaf2d21-f59a5ccc})

[comment]: # ({b1bc108a-23d7246f})

### Item key details

Parameters without angle brackets are mandatory. Parameters marked with angle brackets **<** **>** are optional.

[comment]: # ({/b1bc108a-23d7246f})

[comment]: # ({52d234e4-7d8dff2e})

##### icmpping\[<target>,<packets>,<interval>,<size>,<timeout>,<options>\] {#icmpping}

<br>
The host accessibility by ICMP ping.<br>
Return value: *0* - ICMP ping fails; *1* - ICMP ping successful.

Parameters:

-   **target** - the host IP or DNS name;
-   **packets** - the number of packets;
-   **interval** - the time between successive packets in milliseconds;
-   **size** - the packet size in bytes;
-   **timeout** - the timeout in milliseconds;
-   **options** - used for allowing redirect: if empty (default value), redirected responses are treated as target host down; if set to *allow_redirect*, redirected responses are treated as target host up.

See also the table of [default values](#default-values).

Example:

    icmpping[,4] #If at least one packet of the four is returned, the item will return 1.

[comment]: # ({/52d234e4-7d8dff2e})

[comment]: # ({1976ee61-6cd0f1b1})

##### icmppingloss\[<target>,<packets>,<interval>,<size>,<timeout>,<options>\] {#icmppingloss}

<br>
The percentage of lost packets.<br>
Return value: *Float*.

Parameters:

-   **target** - the host IP or DNS name;
-   **packets** - the number of packets;
-   **interval** - the time between successive packets in milliseconds;
-   **size** - the packet size in bytes;
-   **timeout** - the timeout in milliseconds;
-   **options** - used for allowing redirect: if empty (default value), redirected responses are treated as target host down; if set to *allow_redirect*, redirected responses are treated as target host up.

See also the table of [default values](#default-values).

[comment]: # ({/1976ee61-6cd0f1b1})

[comment]: # ({5c6701a7-b5636838})

##### icmppingsec\[<target>,<packets>,<interval>,<size>,<timeout>,<mode>,<options>\] {#icmppingsec}

<br>
The ICMP ping response time (in seconds).<br>
Return value: *Float*.

Parameters:

-   **target** - the host IP or DNS name;
-   **packets** - the number of packets;
-   **interval** - the time between successive packets in milliseconds;
-   **size** - the packet size in bytes;
-   **timeout** - the timeout in milliseconds;
-   **mode** - possible values: *min*, *max*, or *avg* (default);
-   **options** - used for allowing redirect: if empty (default value), redirected responses are treated as target host down; if set to *allow_redirect*, redirected responses are treated as target host up.

Comments:

-   Packets which are lost or timed out are not used in the calculation;
-   If the host is not available (timeout reached), the item will return 0;
-   If the return value is less than 0.0001 seconds, the value will be set to 0.0001 seconds;
-   See also the table of [default values](#default-values).

[comment]: # ({/5c6701a7-b5636838})

[comment]: # ({3a1d49d8-b0a71170})

##### net.tcp.service[service,<ip>,<port>] {#nettcpservice}

<br>
Checks if a service is running and accepting TCP connections.<br>
Return value: *0* - the service is down; *1* - the service is running.

Parameters:

-   **service** - possible values: *ssh*, *ldap*, *smtp*, *ftp*, *http*, *pop*, *nntp*, *imap*, *tcp*, *https*, *telnet* (see [details](/manual/appendix/items/service_check_details));
-   **ip** - the IP address or DNS name (by default the host IP/DNS is used);
-   **port** - the port number (by default the standard service port number is used).

Comments:

-   Note that with *tcp* service indicating the port is mandatory;
-   These checks may result in additional messages in system daemon logfiles (SMTP and SSH sessions being logged usually);
-   Checking of encrypted protocols (like IMAP on port 993 or POP on port 995) is currently not supported. As a workaround, please use `net.tcp.service[tcp,<ip>,port]` for checks like these.

Example:

    net.tcp.service[ftp,,45] #This item can be used to test the availability of FTP server on TCP port 45.

[comment]: # ({/3a1d49d8-b0a71170})

[comment]: # ({39da67f1-946385b4})

##### net.tcp.service.perf[service,<ip>,<port>] {#nettcpserviceperf}

<br>
Checks the performance of a TCP service.<br>
Return value: *Float*: *0.000000* - the service is down; *seconds* - the number of seconds spent while connecting to the service.

Parameters:

-   **service** - possible values: *ssh*, *ldap*, *smtp*, *ftp*, *http*, *pop*, *nntp*, *imap*, *tcp*, *https*, *telnet* (see [details](/manual/appendix/items/service_check_details));
-   **ip** - the IP address or DNS name (by default the host IP/DNS is used);
-   **port** - the port number (by default the standard service port number is used).

Comments:

-   Note that with *tcp* service indicating the port is mandatory;
-   Checking of encrypted protocols (like IMAP on port 993 or POP on port 995) is currently not supported. As a workaround, please use `net.tcp.service[tcp,<ip>,port]` for checks like these.

Example:

    net.tcp.service.perf[ssh] #This item can be used to test the speed of initial response from SSH server.

[comment]: # ({/39da67f1-946385b4})

[comment]: # ({5cba2094-2859223a})

##### net.udp.service[service,<ip>,<port>] {#netudpservice}

<br>
Checks if a service is running and responding to UDP requests.<br>
Return value: *0* - the service is down; *1* - the service is running.

Parameters:

-   **service** - possible values: *ntp* (see [details](/manual/appendix/items/service_check_details));
-   **ip** - the IP address or DNS name (by default the host IP/DNS is used);
-   **port** - the port number (by default the standard service port number is used).

Example:

    net.udp.service[ntp,,45] #This item can be used to test the availability of NTP service on UDP port 45.

[comment]: # ({/5cba2094-2859223a})

[comment]: # ({bae0ccf0-9cf5922c})

##### net.udp.service.perf[service,<ip>,<port>] {#netudpserviceperf}

<br>
Checks the performance of a UDP service.<br>
Return value: *Float*: *0.000000* - the service is down; *seconds* - the number of seconds spent waiting for response from the service.

Parameters:

-   **service** - possible values: *ntp* (see [details](/manual/appendix/items/service_check_details));
-   **ip** - the IP address or DNS name (by default the host IP/DNS is used);
-   **port** - the port number (by default the standard service port number is used).

Example:

    net.udp.service.perf[ntp] #This item can be used to test the response time from NTP service.

[comment]: # ({/bae0ccf0-9cf5922c})

[comment]: # ({621c13d4-85612a2b})
  
::: noteimportant
For SourceIP support in LDAP simple checks (e.g. `net.tcp.service[ldap]`), OpenLDAP version 2.6.1 or above is required.
:::

[comment]: # ({/621c13d4-85612a2b})

[comment]: # ({92a6c4ae-92a6c4ae})
##### Timeout processing

Zabbix will not process a simple check longer than the Timeout seconds
defined in the Zabbix server/proxy configuration file.

[comment]: # ({/92a6c4ae-92a6c4ae})

[comment]: # ({30611a67-b08a4508})
### ICMP pings

Zabbix uses an external utility **[fping](https://fping.org/)** to process ICMP pings (**icmpping**, **icmppingloss**, **icmppingsec**).

[comment]: # ({/30611a67-b08a4508})

[comment]: # ({d81af73e-e3dd9826})

##### Installation

fping is not included with Zabbix and needs to be installed separately:

- Various Unix-based platforms have the fping package in their default repositories, but it is not pre-installed. In this case you can use the package manager to install fping.

- Zabbix provides [fping packages](http://repo.zabbix.com/non-supported/rhel/) for RHEL. Please note that these packages are provided without official support.

- fping can also be compiled [from source](https://github.com/schweikert/fping/blob/develop/README.md#installation).

[comment]: # ({/d81af73e-e3dd9826})

[comment]: # ({b2c27900-9ced6345})

##### Configuration

Specify fping location in the *[FpingLocation](/manual/appendix/config/zabbix_server#fpinglocation)* parameter of Zabbix server/proxy configuration file 
(or *[Fping6Location](/manual/appendix/config/zabbix_server#fping6location)* parameter for using IPv6 addresses).

fping should be executable by the user Zabbix server/proxy run as and this user should have sufficient rights.

See also: [Known issues](/manual/installation/known_issues#simple_checks) for processing simple checks with fping versions below 3.10.

[comment]: # ({/b2c27900-9ced6345})

[comment]: # ({4edda8c0-3c21487d})

##### Default values

Defaults, limits and description of values for ICMP check parameters:

|Parameter|Unit|Description|Fping's flag|Defaults set by|<|Allowed limits<br>by Zabbix|<|
|--|--|--------|-|--|--|--|--|
|||||**fping**|**Zabbix**|**min**|**max**|
|packets|number|number of request packets sent to a target|-C||3|1|10000|
|interval|milliseconds|time to wait between successive packets to an individual target|-p|1000||20|unlimited|
|size|bytes|packet size in bytes<br>56 bytes on x86, 68 bytes on x86_64|-b|56 or 68||24|65507|
|timeout|milliseconds|**fping v3.x** - timeout to wait after last packet sent, affected by *-C* flag<br> **fping v4.x** - individual timeout for each packet|-t|**fping v3.x** - 500<br>**fping v4.x** and newer - inherited from *-p* flag, but not more than 2000||50|unlimited|

The defaults may differ slightly depending on the platform and version.

In addition, Zabbix uses fping options *-i interval ms* (do not mix up with the item parameter *interval* mentioned in the table above,
which corresponds to fping option *-p*) and *-S source IP address* (or *-I* in older fping versions).
These options are auto-detected by running checks with different option combinations.
Zabbix tries to detect the minimal value in milliseconds that fping allows to use with *-i* by trying 3 values: 0, 1 and 10.
The value that first succeeds is then used for subsequent ICMP checks.
This process is done by each [ICMP pinger](/manual/concepts/server#server_process_types) process individually.

Auto-detected fping options are invalidated every hour and detected again on the next attempt to perform ICMP check.
Set [DebugLevel](/manual/appendix/config/zabbix_server#debuglevel)>=4 in order to view details of this process in the server or proxy log file.

Zabbix writes IP addresses to be checked by any of the three *icmpping\** keys to a temporary file, which is then passed to fping.
If items have different key parameters, only the ones with identical key parameters are written to a single file.
All IP addresses written to the single file will be checked by fping in parallel,
so Zabbix ICMP pinger process will spend fixed amount of time disregarding the number of IP addresses in the file.

[comment]: # ({/4edda8c0-3c21487d})
