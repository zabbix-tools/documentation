[comment]: # ({677840d9-677840d9})
# 2 Recommended UnixODBC settings for PostgreSQL

[comment]: # ({/677840d9-677840d9})

[comment]: # ({d7dc7017-041bb1bb})
#### Installation

-   **Red Hat Enterprise Linux**:

```{=html}
<!-- -->
```
    dnf install postgresql-odbc

-   **Debian/Ubuntu**:

Please refer to [PostgreSQL
documentation](https://www.postgresql.org/download/linux/ubuntu/) to
download necessary database driver for the corresponding platform.

For some additional information please refer to: [installing
unixODBC](/manual/config/items/itemtypes/odbc_checks/).

[comment]: # ({/d7dc7017-041bb1bb})

[comment]: # ({7233138a-d41ee6ca})
#### Configuration

ODBC configuration is done by editing the **odbcinst.ini** and
**odbc.ini** files. These configuration files can be found in */etc*
folder. The file **odbcinst.ini** may be missing and in this case it is
necessary to create it manually.

Please consider the following examples:

**odbcinst.ini**

    [postgresql]
    Description = General ODBC for PostgreSQL
    Driver      = /usr/lib64/libodbcpsql.so
    Setup       = /usr/lib64/libodbcpsqlS.so
    FileUsage   = 1
    # Since 1.6 if the driver manager was built with thread support you may add another entry to each driver entry.
    # This entry alters the default thread serialization level.
    Threading   = 2

**odbc.ini**

    [TEST_PSQL]
    Description = PostgreSQL database 1
    Driver  = postgresql
    #CommLog = /tmp/sql.log
    Username = zbx_test
    Password = zabbix
    # Name of Server. IP or DNS
    Servername = 127.0.0.1
    # Database name
    Database = zabbix
    # Postmaster listening port
    Port = 5432
    # Database is read only
    # Whether the datasource will allow updates.
    ReadOnly = No
    # PostgreSQL backend protocol
    # Note that when using SSL connections this setting is ignored.
    # 7.4+: Use the 7.4(V3) protocol. This is only compatible with 7.4 and higher backends.
    Protocol = 7.4+
    # Includes the OID in SQLColumns
    ShowOidColumn = No
    # Fakes a unique index on OID
    FakeOidIndex  = No
    # Row Versioning
    # Allows applications to detect whether data has been modified by other users
    # while you are attempting to update a row.
    # It also speeds the update process since every single column does not need to be specified in the where clause to update a row.
    RowVersioning = No
    # Show SystemTables
    # The driver will treat system tables as regular tables in SQLTables. This is good for Access so you can see system tables.
    ShowSystemTables = No
    # If true, the driver automatically uses declare cursor/fetch to handle SELECT statements and keeps 100 rows in a cache.
    Fetch = Yes
    # Bools as Char
    # Bools are mapped to SQL_CHAR, otherwise to SQL_BIT.
    BoolsAsChar = Yes
    # SSL mode
    SSLmode = Yes
    # Send to backend on connection
    ConnSettings =

[comment]: # ({/7233138a-d41ee6ca})
