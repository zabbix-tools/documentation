[comment]: # ({e966b3a3-e966b3a3})
# 1 Recommended UnixODBC settings for MySQL

[comment]: # ({/e966b3a3-e966b3a3})

[comment]: # ({6c7b998d-e5f46c02})
#### Installation

-    **Red Hat Enterprise Linux**:

    dnf install mysql-connector-odbc

-    **Debian/Ubuntu**:

Please refer to [MySQL
documentation](https://dev.mysql.com/downloads/connector/odbc/) to
download necessary database driver for the corresponding platform.

For some additional information please refer to: [installing
unixODBC](/manual/config/items/itemtypes/odbc_checks/).

[comment]: # ({/6c7b998d-e5f46c02})

[comment]: # ({dcea6601-dcea6601})
#### Configuration

ODBC configuration is done by editing **odbcinst.ini** and **odbc.ini**
files. These configuration files can be found in */etc* folder. The file
**odbcinst.ini** may be missing and in this case it is necessary to
create it manually.

**odbcinst.ini**

    [mysql]
    Description = General ODBC for MySQL
    Driver      = /usr/lib64/libmyodbc5.so
    Setup       = /usr/lib64/libodbcmyS.so 
    FileUsage   = 1

Please consider the following examples of **odbc.ini** configuration
parameters.

-   An example with a connection through an IP:

```{=html}
<!-- -->
```
    [TEST_MYSQL]                                                     
    Description = MySQL database 1                                   
    Driver  = mysql                                                  
    Port = 3306                                                      
    Server = 127.0.0.1

-   An example with a connection through an IP and with the use of
    credentials. A Zabbix database is used by default:

```{=html}
<!-- -->
```
    [TEST_MYSQL_FILLED_CRED]                       
    Description = MySQL database 2                 
    Driver  = mysql                                
    User = root                                    
    Port = 3306                                    
    Password = zabbix                           
    Database = zabbix                             
    Server = 127.0.0.1                             

-   An example with a connection through a socket and with the use of
    credentials. A Zabbix database is used by default:

```{=html}
<!-- -->
```
    [TEST_MYSQL_FILLED_CRED_SOCK]                  
    Description = MySQL database 3                 
    Driver  = mysql                                
    User = root                                    
    Password = zabbix                           
    Socket = /var/run/mysqld/mysqld.sock           
    Database = zabbix

All other possible configuration parameter options can be found in
[MySQL official
documentation](https://dev.mysql.com/doc/connector-odbc/en/connector-odbc-configuration-connection-parameters.html)
web page.

[comment]: # ({/dcea6601-dcea6601})
