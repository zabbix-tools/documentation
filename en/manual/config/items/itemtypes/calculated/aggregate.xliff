<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="en" datatype="plaintext" original="manual/config/items/itemtypes/calculated/aggregate.md">
    <body>
      <trans-unit id="bfe1a94d" xml:space="preserve">
        <source># Aggregate calculations</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/config/items/itemtypes/calculated/aggregate.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="f8e62abe" xml:space="preserve">
        <source>#### Overview

Aggregate calculations are a [calculated item](/manual/config/items/itemtypes/calculated) 
type allowing to collect information from several items by Zabbix server and 
then calculate an aggregate, depending on the aggregate function used.

Aggregate calculations do not require any agent running on the host
being monitored.</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/config/items/itemtypes/calculated/aggregate.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="63d83a0c" xml:space="preserve">
        <source>#### Syntax

To retrieve aggregates use one of the supported [aggregate functions](/manual/appendix/functions/aggregate#aggregate-functions-1): `avg`, `max`, `min`, `sum`, etc. 
Then add the **foreach** function as the only parameter and its item filter to select the required items:

    aggregate_function(function_foreach(/host/key?[group="host group"],timeperiod))

A **foreach** function (e.g. *avg_foreach*, *count_foreach*, etc.) returns one aggregate value for each selected item. 
Items are selected by using the item filter (`/host/key?[group="host group"]`), from item history. For more details, see 
[foreach functions](/manual/appendix/functions/aggregate/foreach).

If some of the items have no data for the requested period, they are ignored in the calculation. If no items have 
data, the function will return an error.

Alternatively you may list several items as parameters for aggregation:

    aggregate_function(function(/host/key,parameter),function(/host2/key2,parameter),...)

Note that `function` here must be a history/trend function.

::: noteclassic
If the aggregate results in a float value it will be trimmed
to an integer if the aggregated item type of information is *Numeric
(unsigned)*.
:::

An aggregate calculation may become unsupported if:

-   none of the referenced items is found (which may happen if the item
    key is incorrect, none of the items exists or all included groups
    are incorrect)
-   no data to calculate a function</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/config/items/itemtypes/calculated/aggregate.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="3bbe172c" xml:space="preserve">
        <source>#### Usage examples

Examples of keys for aggregate calculations.</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/config/items/itemtypes/calculated/aggregate.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="874bcf94" xml:space="preserve">
        <source>##### Example 1

Total disk space of host group 'MySQL Servers'.

    sum(last_foreach(/*/vfs.fs.size[/,total]?[group="MySQL Servers"]))</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/config/items/itemtypes/calculated/aggregate.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="3182672a" xml:space="preserve">
        <source>##### Example 2

Sum of latest values of all items matching net.if.in\[\*\] on the host.

    sum(last_foreach(/host/net.if.in[*]))</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/config/items/itemtypes/calculated/aggregate.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="cdf2da8b" xml:space="preserve">
        <source>##### Example 3

Average processor load of host group 'MySQL Servers'.

    avg(last_foreach(/*/system.cpu.load[,avg1]?[group="MySQL Servers"]))</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/config/items/itemtypes/calculated/aggregate.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="db9c8fce" xml:space="preserve">
        <source>##### Example 4

5-minute average of the number of queries per second for host group
'MySQL Servers'.

    avg(avg_foreach(/*/mysql.qps?[group="MySQL Servers"],5m))</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/config/items/itemtypes/calculated/aggregate.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="0233edd3" xml:space="preserve">
        <source>##### Example 5

Average CPU load on all hosts in multiple host groups that have the
specific tags.

    avg(last_foreach(/*/system.cpu.load?[(group="Servers A" or group="Servers B" or group="Servers C") and (tag="Service:" or tag="Importance:High")]))</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/config/items/itemtypes/calculated/aggregate.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="30448f5d" xml:space="preserve">
        <source>##### Example 6

Calculation used on the latest item value sums of a whole host group.

    sum(last_foreach(/*/net.if.out[eth0,bytes]?[group="video"])) / sum(last_foreach(/*/nginx_stat.sh[active]?[group="video"])) </source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/config/items/itemtypes/calculated/aggregate.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="3e9404a6" xml:space="preserve">
        <source>##### Example 7

The total number of unsupported items in host group 'Zabbix servers'.

    sum(last_foreach(/*/zabbix[host,,items_unsupported]?[group="Zabbix servers"]))</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/config/items/itemtypes/calculated/aggregate.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="f094da29" xml:space="preserve">
        <source>
##### Examples of correct/incorrect syntax

Expressions (including function calls) cannot be used as history, trend, or foreach [function](/manual/appendix/functions) parameters. However, those functions themselves can be used in other (non-historical) function parameters.

|Expression|Example|
|-|---------|
|Valid|`avg(last(/host/key1),last(/host/key2)*10,last(/host/key1)*100)`&lt;br&gt;`max(avg(avg_foreach(/*/system.cpu.load?[group="Servers A"],5m)),avg(avg_foreach(/*/system.cpu.load?[group="Servers B"],5m)),avg(avg_foreach(/*/system.cpu.load?[group="Servers C"],5m)))`|
|Invalid|`sum(/host/key,10+2)`&lt;br&gt;`sum(/host/key, avg(10,2))`&lt;br&gt;`sum(/host/key,last(/host/key2))`|

Note that in an expression like:

    sum(sum_foreach(//resptime[*],5m))/sum(count_foreach(//resptime[*],5m))

it cannot be guaranteed that both parts of the equation will always have the same set of values. 
While one part of the expression is evaluated, a new value for the requested period may arrive and 
then the other part of the expression will have a different set of values.</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/config/items/itemtypes/calculated/aggregate.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
    </body>
  </file>
</xliff>
