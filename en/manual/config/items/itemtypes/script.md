[comment]: # (tags: script )

[comment]: # ({1a1e2756-1a1e2756})
# 18 Script items

[comment]: # ({/1a1e2756-1a1e2756})

[comment]: # ({4a773286-cb89bc38})
### Overview

Script items can be used to collect data by executing a user-defined
JavaScript code with the ability to retrieve data over HTTP/HTTPS. In
addition to the script, an optional list of parameters (pairs of name
and value) and timeout can be specified.

This item type may be useful in data collection scenarios that require
multiple steps or complex logic. As an example, a Script item can be
configured to make an HTTP call, then process the data received in the
first step in some way, and pass transformed value to the second HTTP
call.

Script items are processed by Zabbix server or proxy pollers.

[comment]: # ({/4a773286-cb89bc38})

[comment]: # ({e0894ef0-7ed55e87})
### Configuration

In the *Type* field of [item configuration
form](/manual/config/items/item) select Script then fill out required
fields.

![script\_item.png](../../../../../assets/en/manual/config/items/itemtypes/script_item.png)

All mandatory input fields are marked with a red asterisk.

The fields that require specific information for Script items are:

|Field|Description|
|--|--------|
|Key|Enter a unique key that will be used to identify the item.|
|Parameters|Specify the variables to be passed to the script as the attribute and value pairs.<br>[User macros](/manual/config/macros/user_macros) are supported. To see which built-in macros are supported, do a search for "Script-type item" in the [supported macro](/manual/appendix/macros/supported_by_location) table.|
|Script|Enter JavaScript code in the block that appears when clicking in the parameter field (or on the view/edit button next to it). This code must provide the logic for returning the metric value.<br>The code has access to all parameters, it may perform HTTP GET, POST, PUT and DELETE requests and has control over HTTP headers and request body.<br>See also: [Additional JavaScript objects](/manual/config/items/preprocessing/javascript/javascript_objects), [JavaScript Guide](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide).|
|Timeout|JavaScript execution timeout (1-60s, default 3s); exceeding it will return error.<br>Time suffixes are supported, e.g. 30s, 1m.<br>Depending on the script it might take longer for the timeout to trigger. |

[comment]: # ({/e0894ef0-7ed55e87})

[comment]: # ({b41637d2-4c860844})
### Examples

[comment]: # ({/b41637d2-4c860844})

[comment]: # ({2f86a97a-9c6c78ee})
##### Simple data collection

Collect the content of *https://www.example.com/release\_notes*:

-   Create an item with type "Script".
-   In the *Script* field, enter:

```javascript
var request = new HttpRequest();
return request.get("https://www.example.com/release_notes");
```

[comment]: # ({/2f86a97a-9c6c78ee})

[comment]: # ({ccceecb6-7115c5ef})
##### Data collection with parameters

Collect the content of a specific page and make use of parameters: 

- Create an item with type "Script" and two parameters:
    - **url : {$DOMAIN}** (the user macro {$DOMAIN} should be defined, preferably on the host level)
    - **subpage : /release_notes**

![](../../../../../assets/en/manual/config/items/itemtypes/script_example1.png){width=600}

- In the *Script* field, enter: 

```javascript
var obj = JSON.parse(value);
var url = obj.url;
var subpage = obj.subpage;
var request = new HttpRequest();
return request.get(url + subpage);
```

[comment]: # ({/ccceecb6-7115c5ef})

[comment]: # ({d5553e63-18a089f4})
##### Multiple HTTP requests

Collect the content of both *https://www.example.com* and
*https://www.example.com/release\_notes*:

-   Create an item with type "Script".
-   In the *Script* field, enter:

```javascript
var request = new HttpRequest();
return request.get("https://www.example.com") + request.get("https://www.example.com/release_notes");
```

[comment]: # ({/d5553e63-18a089f4})

[comment]: # ({0a711007-d837bcdd})
##### Logging

Add the "Log test" entry to the Zabbix server log and receive the item
value "1" in return:

-   Create an item with type "Script".
-   In the *Script* field, enter:

```javascript
Zabbix.log(3, 'Log test');
return 1;
```

[comment]: # ({/0a711007-d837bcdd})
