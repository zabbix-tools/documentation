[comment]: # ({0ee06d3b-0ee06d3b})
# 11 External checks

[comment]: # ({/0ee06d3b-0ee06d3b})

[comment]: # ({cd36d798-cd36d798})
#### Overview

External check is a check executed by Zabbix server by [running a shell
script](/manual/appendix/command_execution) or a binary. However, when
hosts are monitored by a Zabbix proxy, the external checks are executed
by the proxy.

External checks do not require any agent running on a host being
monitored.

The syntax of the item key is:

    script[<parameter1>,<parameter2>,...]

Where:

|ARGUMENT|DEFINITION|
|--------|----------|
|**script**|Name of a shell script or a binary.|
|**parameter(s)**|Optional command line parameters.|

If you don't want to pass any parameters to the script you may use:

    script[] or
    script

Zabbix server will look in the directory defined as the location for
external scripts (parameter 'ExternalScripts' in [Zabbix server
configuration file](/manual/appendix/config/zabbix_server)) and execute
the command. The command will be executed as the user Zabbix server runs
as, so any access permissions or environment variables should be handled
in a wrapper script, if necessary, and permissions on the command should
allow that user to execute it. Only commands in the specified directory
are available for execution.

::: notewarning
Do not overuse external checks! As each script
requires starting a fork process by Zabbix server, running many scripts
can decrease Zabbix performance a lot.
:::

[comment]: # ({/cd36d798-cd36d798})

[comment]: # ({55e878ce-55e878ce})
#### Usage example

Executing the script **check\_oracle.sh** with the first parameters
'-h'. The second parameter will be replaced by IP address or DNS name,
depending on the selection in the host properties.

    check_oracle.sh["-h","{HOST.CONN}"]

Assuming host is configured to use IP address, Zabbix will execute:

    check_oracle.sh '-h' '192.168.1.4'

[comment]: # ({/55e878ce-55e878ce})

[comment]: # ({011b7534-011b7534})
#### External check result

The return value of the check is standard output together with standard
error (the full output with trimmed trailing whitespace is returned
since Zabbix 2.0).

::: noteimportant
A text (character, log or text type of
information) item will not become unsupported in case of standard error
output.
:::

In case the requested script is not found or Zabbix server has no
permissions to execute it, the item will become unsupported and
corresponding error message will be set. In case of a timeout, the item
will be marked as unsupported as well, an according error message will
be displayed and the forked process for the script will be killed.

[comment]: # ({/011b7534-011b7534})
