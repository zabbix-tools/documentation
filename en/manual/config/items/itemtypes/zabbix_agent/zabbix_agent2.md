[comment]: # attributes: notoc

[comment]: # (terms: ceph.df.details, ceph.osd.stats, ceph.osd.discovery, ceph.osd.dump, ceph.ping, ceph.pool.discovery, ceph.status, docker.container_info, docker.container_stats, docker.containers, docker.containers.discovery, docker.data_usage, docker.images, docker.images.discovery, docker.info, docker.ping, memcached.ping, memcached.stats, mongodb.collection.stats, mongodb.collections.discovery, mongodb.collections.usage, mongodb.connpool.stats, mongodb.db.stats, mongodb.db.discovery, mongodb.jumbo_chunks.count, mongodb.oplog.stats, mongodb.ping, mongodb.rs.config, mongodb.rs.status, mongodb.server.status, mongodb.sh.discovery, mqtt.get, mysql.db.discovery, mysql.db.size, mysql.get_status_variables, mysql.ping, mysql.replication.discovery, mysql.replication.get_slave_status, mysql.version, oracle.diskgroups.stats, oracle.diskgroups.discovery, oracle.archive.info, oracle.cdb.info, oracle.custom.query, oracle.datafiles.stats, oracle.db.discovery, oracle.fra.stats, oracle.instance.info, oracle.pdb.info, oracle.pdb.discovery, oracle.pga.stats, oracle.ping, oracle.proc.stats, oracle.redolog.info, oracle.sga.stats, oracle.sessions.stats, oracle.sys.metrics, oracle.sys.params, oracle.ts.stats, oracle.ts.discovery, oracle.user.info, pgsql.autovacuum.count, pgsql.archive, pgsql.bgwriter, pgsql.cache.hit, pgsql.connections, pgsql.dbstat, pgsql.dbstat.sum, pgsql.db.age, pgsql.db.bloating_tables, pgsql.db.discovery, pgsql.db.size, pgsql.locks, pgsql.oldest.xid, pgsql.ping, pgsql.replication.count, pgsql.replication.recovery_role, pgsql.replication.status, pgsql.replication_lag.b, pgsql.replication_lag.sec, pgsql.uptime, pgsql.wal.stat, redis.config, redis.info, redis.ping, redis.slowlog.count, smart.attribute.discovery, smart.disk.discovery, smart.disk.get, systemd.unit.get, systemd.unit.info, systemd.unit.discovery, web.certificate.get )

[comment]: # ({871f8735-871f8735})
# Zabbix agent 2

[comment]: # ({/871f8735-871f8735})

[comment]: # ({a27e9ce8-3728d5da})

Zabbix agent 2 supports all item keys supported for Zabbix agent on [Unix](/manual/config/items/itemtypes/zabbix_agent) and [Windows](/manual/config/items/itemtypes/zabbix_agent/win_keys). This page provides details on the additional item keys, which you can use with Zabbix agent 2 only, grouped by the plugin they belong to.

The item keys are listed without parameters and additional information. Click on the item key to see the full details.

|Item key|Description|Plugin|
|--|-------|-|
|[ceph.df.details](#ceph.df.details)|The cluster’s data usage and distribution among pools.|Ceph|
|[ceph.osd.stats](#ceph.osd.stats)|Aggregated and per OSD statistics.|^|
|[ceph.osd.discovery](#ceph.osd.discovery)|The list of discovered OSDs.|^|
|[ceph.osd.dump](#ceph.osd.dump)|The usage thresholds and statuses of OSDs.|^|
|[ceph.ping](#ceph.ping)|Tests whether a connection to Ceph can be established.|^|
|[ceph.pool.discovery](#ceph.pool.discovery)|The list of discovered pools.|^|
|[ceph.status](#ceph.status)|The overall cluster's status.|^|
|[docker.container_info](#docker.container.info)|Low-level information about a container.|Docker|
|[docker.container_stats](#docker.container.stats)|The container resource usage statistics.|^|
|[docker.containers](#docker.containers)|Returns the list of containers.|^|
|[docker.containers.discovery](#docker.containers.discovery)|Returns the list of containers. Used for low-level discovery.|^|
|[docker.data.usage](#docker.data.usage)|Information about the current data usage.|^|
|[docker.images](#docker.images)|Returns the list of images.|^|
|[docker.images.discovery](#docker.images.discovery)|Returns the list of images. Used for low-level discovery.|^|
|[docker.info](#docker.info)|The system information.|^|
|[docker.ping](#docker.ping)|Test if the Docker daemon is alive or not.|^|
|[memcached.ping](#memcached.ping)|Test if a connection is alive or not.|Memcached|
|[memcached.stats](#memcached.stats)|Gets the output of the STATS command.|^|
|[mongodb.collection.stats](#mongodb.collection.stats)|Returns a variety of storage statistics for a given collection.|MongoDB|
|[mongodb.collections.discovery](#mongodb.collections.discovery)|Returns a list of discovered collections.|^|
|[mongodb.collections.usage](#mongodb.collections.usage)|Returns the usage statistics for collections.|^|
|[mongodb.connpool.stats](#mongodb.connpool.stats)|Returns information regarding the open outgoing connections from the current database instance to other members of the sharded cluster or replica set.|^|
|[mongodb.db.stats](#mongodb.db.stats)|Returns the statistics reflecting a given database system state.|^|
|[mongodb.db.discovery](#mongodb.db.discovery)|Returns a list of discovered databases.|^|
|[mongodb.jumbo_chunks.count](#mongodb.jumbo.chunks.count)|Returns the count of jumbo chunks.|^|
|[mongodb.oplog.stats](#mongodb.oplog.stats)|Returns the status of the replica set, using data polled from the oplog.|^|
|[mongodb.ping](#mongodb.ping)|Test if a connection is alive or not.|^|
|[mongodb.rs.config](#mongodb.rs.config)|Returns the current configuration of the replica set.|^|
|[mongodb.rs.status](#mongodb.rs.status)|Returns the replica set status from the point of view of the member where the method is run.|^|
|[mongodb.server.status](#mongodb.server.status)|Returns the database state.|^|
|[mongodb.sh.discovery](#mongodb.sh.discovery)|Returns the list of discovered shards present in the cluster.|^|
|[mqtt.get](#mqtt.get)|Subscribes to a specific topic or topics (with wildcards) of the provided broker and waits for publications.|MQTT|
|[mysql.db.discovery](#mysql.db.discovery)|Returns the list of MySQL databases.|MySQL|
|[mysql.db.size](#mysql.db.size)|The database size in bytes.|^|
|[mysql.get_status_variables](#mysql.get.status.variables)|Values of the global status variables.|^|
|[mysql.ping](#mysql.ping)|Test if a connection is alive or not.|^|
|[mysql.replication.discovery](#mysql.replication.discovery)|Returns the list of MySQL replications.|^|
|[mysql.replication.get_slave_status](#mysql.replication.get.slave.status)|The replication status.|^|
|[mysql.version](#mysql.version)|The MySQL version.|^|
|[oracle.diskgroups.stats](#oracle.diskgroups.stats)|Returns the Automatic Storage Management (ASM) disk groups statistics.|Oracle|
|[oracle.diskgroups.discovery](#oracle.diskgroups.discovery)|Returns the list of ASM disk groups.|^|
|[oracle.archive.info](#oracle.archive.info)|The archive logs statistics.|^|
|[oracle.cdb.info](#oracle.cdb.info)|The Container Databases (CDBs) information.|^|
|[oracle.custom.query](#oracle.custom.query)|The result of a custom query.|^|
|[oracle.datafiles.stats](#oracle.datafiles.stats)|Returns the data files statistics.|^|
|[oracle.db.discovery](#oracle.db.discovery)|Returns the list of databases.|^|
|[oracle.fra.stats](#oracle.fra.stats)|Returns the Fast Recovery Area (FRA) statistics.|^|
|[oracle.instance.info](#oracle.instance.info)|The instance statistics.|^|
|[oracle.pdb.info](#oracle.pdb.info)|The Pluggable Databases (PDBs) information.|^|
|[oracle.pdb.discovery](#oracle.pdb.discovery)|Returns the list of PDBs.|^|
|[oracle.pga.stats](#oracle.pga.stats)|Returns the Program Global Area (PGA) statistics.|^|
|[oracle.ping](#oracle.ping)|Test whether a connection to Oracle can be established.|^|
|[oracle.proc.stats](#oracle.proc.stats)|Returns the processes statistics.|^|
|[oracle.redolog.info](#oracle.redolog.info)|The log file information from the control file.|^|
|[oracle.sga.stats](#oracle.sga.stats)|Returns the System Global Area (SGA) statistics.|^|
|[oracle.sessions.stats](#oracle.sessions.stats)|Returns the sessions statistics.|^|
|[oracle.sys.metrics](#oracle.sys.metrics)|Returns a set of system metric values.|^|
|[oracle.sys.params](#oracle.sys.params)|Returns a set of system parameter values.|^|
|[oracle.ts.stats](#oracle.ts.stats)|Returns the tablespaces statistics.|^|
|[oracle.ts.discovery](#oracle.ts.discovery)|Returns a list of tablespaces.|^|
|[oracle.user.info](#oracle.user.info)|Returns Oracle user information.|^|
|[pgsql.autovacuum.count](#pgsql.autovacuum.count)|The number of autovacuum workers.|PostgreSQL|
|[pgsql.archive](#pgsql.archive)|The information about archived files.|^|
|[pgsql.bgwriter](#pgsql.bgwriter)|The combined number of checkpoints for the database cluster, broken down by checkpoint type.|^|
|[pgsql.cache.hit](#pgsql.cache.hit)|The PostgreSQL buffer cache hit rate.|^|
|[pgsql.connections](#pgsql.connections)|Returns connections by type.|^|
|[pgsql.custom.query](#pgsql.custom.query)|Returns the result of a custom query.|^|
|[pgsql.db.age](#pgsql.db.age)|The age of the oldest FrozenXID of the database.|^|
|[pgsql.db.bloating_tables](#pgsql.db.bloating.tables)|The number of bloating tables per database.|^|
|[pgsql.db.discovery](#pgsql.db.discovery)|The list of PostgreSQL databases.|^|
|[pgsql.db.size](#pgsql.db.size)|The database size in bytes.|^|
|[pgsql.dbstat](#pgsql.dbstat)|Collects the statistics per database.|^|
|[pgsql.dbstat.sum](#pgsql.dbstat.sum)|The summarized data for all databases in a cluster.|^|
|[pgsql.locks](#pgsql.locks)|The information about granted locks per database.|^|
|[pgsql.oldest.xid](#pgsql.oldest.xid)|The age of the oldest XID.|^|
|[pgsql.ping](#pgsql.ping)|Test if a connection is alive or not.|^|
|[pgsql.queries](#pgsql.queries)|Query metrics by execution time.|^|
|[pgsql.replication.count](#pgsql.replication.count)|The number of standby servers.|^|
|[pgsql.replication.process](#pgsql.replication.process)|The flush lag, write lag and replay lag per each sender process.|^|
|[pgsql.replication.process.discovery](#pgsql.replication.process.discovery)|The replication process name discovery.|^|
|[pgsql.replication.recovery_role](#pgsql.replication.recovery.role)|The recovery status.|^|
|[pgsql.replication.status](#pgsql.replication.status)|The status of replication.|^|
|[pgsql.replication_lag.b](#pgsql.replication.lag.b)|The replication lag in bytes.|^|
|[pgsql.replication_lag.sec](#pgsql.replication.lag.sec)|The replication lag in seconds.|^|
|[pgsql.uptime](#pgsql.uptime)|The PostgreSQL uptime in milliseconds.|^|
|[pgsql.wal.stat](#pgsql.wal.stat)|The WAL statistics.|^|
|[redis.config](#redis.config)|Gets the configuration parameters of a Redis instance that match the pattern.|Redis|
|[redis.info](#redis.info)|Gets the output of the INFO command.|^|
|[redis.ping](#redis.ping)|Test if a connection is alive or not.|^|
|[redis.slowlog.count](#redis.slowlog.count)|The number of slow log entries since Redis was started.|^|
|[smart.attribute.discovery](#smart.attribute.discovery)|Returns a list of S.M.A.R.T. device attributes.|S.M.A.R.T.|
|[smart.disk.discovery](#smart.disk.discovery)|Returns a list of S.M.A.R.T. devices.|^|
|[smart.disk.get](#smart.disk.get)|Returns all available properties of S.M.A.R.T. devices.|^|
|[systemd.unit.get](#systemd.unit.get)|Returns all properties of a systemd unit.|Systemd|
|[systemd.unit.info](#systemd.unit.info)|Systemd unit information.|^|
|[systemd.unit.discovery](#systemd.unit.discovery)|The list of systemd units and their details.|^|
|[web.certificate.get](#web.certificate.get)|Validates the certificates and returns certificate details.|Web certificates|

See also: [Built-in plugins](/manual/extensions/plugins#built-in)

[comment]: # ({/a27e9ce8-3728d5da})

[comment]: # ({b1bc108a-3077d649})
### Item key details

Parameters without angle brackets are mandatory. Parameters marked with angle brackets **<** **>** are optional.

[comment]: # ({/b1bc108a-3077d649})

[comment]: # ({5db49140-e443ac84})

##### ceph.df.details[connString,<user>,<apikey>] {#ceph.df.details}

<br>
The cluster’s data usage and distribution among pools.<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the Ceph login credentials.<br>

[comment]: # ({/5db49140-e443ac84})

[comment]: # ({29a131ed-8b08b33c})

##### ceph.osd.stats[connString,<user>,<apikey>] {#ceph.osd.stats}

<br>
Aggregated and per OSD statistics.<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the Ceph login credentials.<br>

[comment]: # ({/29a131ed-8b08b33c})

[comment]: # ({a2075fd5-07bfdcb6})

##### ceph.osd.discovery[connString,<user>,<apikey>] {#ceph.osd.discovery}

<br>
The list of discovered OSDs. Used for [low-level discovery](/manual/discovery/low_level_discovery).<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the Ceph login credentials.<br>

[comment]: # ({/a2075fd5-07bfdcb6})

[comment]: # ({cf2f228b-236454b9})

##### ceph.osd.dump[connString,<user>,<apikey>] {#ceph.osd.dump}

<br>
The usage thresholds and statuses of OSDs.<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the Ceph login credentials.<br>

[comment]: # ({/cf2f228b-236454b9})

[comment]: # ({aabd73bc-04750a8f})

##### ceph.ping[connString,<user>,<apikey>] {#ceph.ping}

<br>
Tests whether a connection to Ceph can be established.<br>
Return value: *0* - connection is broken (if there is any error presented including AUTH and configuration issues); *1* - connection is successful.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the Ceph login credentials.<br>

[comment]: # ({/aabd73bc-04750a8f})

[comment]: # ({8d1e7aa7-5930838a})

##### ceph.pool.discovery[connString,<user>,<apikey>] {#ceph.pool.discovery}

<br>
The list of discovered pools. Used for [low-level discovery](/manual/discovery/low_level_discovery).<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the Ceph login credentials.<br>

[comment]: # ({/8d1e7aa7-5930838a})

[comment]: # ({4a92a695-d2e29bd4})

##### ceph.status[connString,<user>,<apikey>] {#ceph.status}

<br>
The overall cluster's status.<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the Ceph login credentials.<br>

[comment]: # ({/4a92a695-d2e29bd4})

[comment]: # ({4103f9f0-0f9ad2d9})

##### docker.container_info[<ID>,<info>] {#docker.container.info}

<br>
Low-level information about a container.<br>
Return value: The output of the [ContainerInspect](https://docs.docker.com/engine/api/v1.28/#operation/ContainerInspect) API call serialized as JSON.

Parameters:

-   **ID** - the ID or name of the container;<br>
-   **info** - the amount of information returned. Supported values: *short* (default) or *full*.

The Agent 2 user ('zabbix') must be added to the 'docker' [group](https://docs.docker.com/engine/install/linux-postinstall/#manage-docker-as-a-non-root-user) for sufficient privileges. Otherwise the check will fail.

[comment]: # ({/4103f9f0-0f9ad2d9})

[comment]: # ({26f35a6d-9e9006b3})

##### docker.container_stats[<ID>] {#docker.container.stats}

<br>
The container resource usage statistics.<br>
Return value: The output of the [ContainerStats](https://docs.docker.com/engine/api/v1.28/#operation/ContainerStats) API call and CPU usage percentage serialized as JSON.

Parameter:

-   **ID** - the ID or name of the container.

The Agent 2 user ('zabbix') must be added to the 'docker' [group](https://docs.docker.com/engine/install/linux-postinstall/#manage-docker-as-a-non-root-user) for sufficient privileges. Otherwise the check will fail.

[comment]: # ({/26f35a6d-9e9006b3})

[comment]: # ({313a1f91-30610c8f})

##### docker.containers {#docker.containers}

<br>
The list of containers.<br>
Return value: The output of the [ContainerList](https://docs.docker.com/engine/api/v1.28/#operation/ContainerList) API call serialized as JSON.

The Agent 2 user ('zabbix') must be added to the 'docker' [group](https://docs.docker.com/engine/install/linux-postinstall/#manage-docker-as-a-non-root-user) for sufficient privileges. Otherwise the check will fail.

[comment]: # ({/313a1f91-30610c8f})

[comment]: # ({72959fc2-39cceff0})

##### docker.containers.discovery[<options>] {#docker.containers.discovery}

<br>
Returns the list of containers. Used for [low-level discovery](/manual/discovery/low_level_discovery/).<br>
Return value: *JSON object*.

Parameter:

-   **options** - specify whether all or only running containers should be discovered. Supported values: *true* - return all containers; *false* - return only running containers (default).

The Agent 2 user ('zabbix') must be added to the 'docker' [group](https://docs.docker.com/engine/install/linux-postinstall/#manage-docker-as-a-non-root-user) for sufficient privileges. Otherwise the check will fail.

[comment]: # ({/72959fc2-39cceff0})

[comment]: # ({689589f9-7a99eb4b})

##### docker.data.usage {#docker.data.usage}

<br>
Information about the current data usage.<br>
Return value: The output of the [SystemDataUsage](https://docs.docker.com/engine/api/v1.28/#operation/SystemDataUsage) API call serialized as JSON.

The Agent 2 user ('zabbix') must be added to the 'docker' [group](https://docs.docker.com/engine/install/linux-postinstall/#manage-docker-as-a-non-root-user) for sufficient privileges. Otherwise the check will fail.

[comment]: # ({/689589f9-7a99eb4b})

[comment]: # ({23c201b2-6c71011c})

##### docker.images {#docker.images}

<br>
Returns the list of images.<br>
Return value: The output of the [ImageList](https://docs.docker.com/engine/api/v1.28/#operation/ImageList) API call serialized as JSON.

The Agent 2 user ('zabbix') must be added to the 'docker' [group](https://docs.docker.com/engine/install/linux-postinstall/#manage-docker-as-a-non-root-user) for sufficient privileges. Otherwise the check will fail.

[comment]: # ({/23c201b2-6c71011c})

[comment]: # ({ecfc491e-1c03f201})

##### docker.images.discovery {#docker.images.discovery}

<br>
Returns the list of images. Used for [low-level discovery](/manual/discovery/low_level_discovery/).<br>
Return value: *JSON object*.

The Agent 2 user ('zabbix') must be added to the 'docker' [group](https://docs.docker.com/engine/install/linux-postinstall/#manage-docker-as-a-non-root-user) for sufficient privileges. Otherwise the check will fail.

[comment]: # ({/ecfc491e-1c03f201})

[comment]: # ({2d5cdfb5-d549fed5})

##### docker.info {#docker.info}

<br>
The system information.<br>
Return value: The output of the [SystemInfo](https://docs.docker.com/engine/api/v1.28/#operation/SystemInfo) API call serialized as JSON.

The Agent 2 user ('zabbix') must be added to the 'docker' [group](https://docs.docker.com/engine/install/linux-postinstall/#manage-docker-as-a-non-root-user) for sufficient privileges. Otherwise the check will fail.

[comment]: # ({/2d5cdfb5-d549fed5})

[comment]: # ({9ee8f9b2-e8a64d82})

##### docker.ping {#docker.ping}

<br>
Test if the Docker daemon is alive or not.<br>
Return value: *1* - the connection is alive; *0* - the connection is broken.

The Agent 2 user ('zabbix') must be added to the 'docker' [group](https://docs.docker.com/engine/install/linux-postinstall/#manage-docker-as-a-non-root-user) for sufficient privileges. Otherwise the check will fail.

[comment]: # ({/9ee8f9b2-e8a64d82})

[comment]: # ({7849bde9-d4eb298e})

##### memcached.ping[connString,<user>,<password>] {#memcached.ping}

<br>
Test if a connection is alive or not.<br>
Return value: *1* - the connection is alive; *0* - the connection is broken (if there is any error presented including AUTH and configuration issues).

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the Memcached login credentials.<br>

[comment]: # ({/7849bde9-d4eb298e})

[comment]: # ({52d1a7a8-ceb374d4})

##### memcached.stats[connString,<user>,<password>,<type>] {#memcached.stats}

<br>
Gets the output of the STATS command.<br>
Return value: *JSON* - the output is serialized as JSON.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the Memcached login credentials;<br>
-   **type** - stat type to be returned: *items*, *sizes*, *slabs* or *settings* (empty by default, returns general statistics).

[comment]: # ({/52d1a7a8-ceb374d4})

[comment]: # ({96758cfd-ba10c6a3})

##### mongodb.collection.stats[connString,<user>,<password>,<database>,collection] {#mongodb.collection.stats}

<br>
Returns a variety of storage statistics for a given collection.<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the MongoDB login credentials;<br>
-   **database** - the database name (default: admin);<br>
-   **collection** - the collection name.

[comment]: # ({/96758cfd-ba10c6a3})

[comment]: # ({50c42aa5-d3f908ab})

##### mongodb.collections.discovery[connString,<user>,<password>] {#mongodb.collections.discovery}

<br>
Returns a list of discovered collections. Used for [low-level discovery](/manual/discovery/low_level_discovery).<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the MongoDB login credentials.<br>

[comment]: # ({/50c42aa5-d3f908ab})

[comment]: # ({f403e136-d9af9de6})

##### mongodb.collections.usage[connString,<user>,<password>] {#mongodb.collections.usage}

<br>
Returns the usage statistics for collections.<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the MongoDB login credentials.<br>

[comment]: # ({/f403e136-d9af9de6})

[comment]: # ({eb4c0b74-20da93d0})

##### mongodb.connpool.stats[connString,<user>,<password>] {#mongodb.connpool.stats}

<br>
Returns information regarding the open outgoing connections from the current database instance to other members of the sharded cluster or replica set.<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the MongoDB login credentials;<br>
-   **database** - the database name (default: admin);<br>
-   **collection** - the collection name.

[comment]: # ({/eb4c0b74-20da93d0})

[comment]: # ({15d8595f-52b800b7})

##### mongodb.db.stats[connString,<user>,<password>,<database>] {#mongodb.db.stats}

<br>
Returns the statistics reflecting a given database system state.<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the MongoDB login credentials;<br>
-   **database** - the database name (default: admin).<br>

[comment]: # ({/15d8595f-52b800b7})

[comment]: # ({d8916238-e12a4df1})

##### mongodb.db.discovery[connString,<user>,<password>] {#mongodb.db.discovery}

<br>
Returns a list of discovered databases. Used for [low-level discovery](/manual/discovery/low_level_discovery).<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the MongoDB login credentials.<br>

[comment]: # ({/d8916238-e12a4df1})

[comment]: # ({9d00bcc9-2d0db89c})

##### mongodb.jumbo_chunks.count[connString,<user>,<password>] {#mongodb.jumbo.chunks.count}

<br>
Returns the count of jumbo chunks.<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the MongoDB login credentials.<br>

[comment]: # ({/9d00bcc9-2d0db89c})

[comment]: # ({f8da8fd1-5bb28367})

##### mongodb.oplog.stats[connString,<user>,<password>] {#mongodb.oplog.stats}

<br>
Returns the status of the replica set, using data polled from the oplog.<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the MongoDB login credentials.<br>

[comment]: # ({/f8da8fd1-5bb28367})

[comment]: # ({f43ecfdd-57fd60cc})

##### mongodb.ping[connString,<user>,<password>] {#mongodb.ping}

<br>
Test if a connection is alive or not.<br>
Return value: *1* - the connection is alive; *0* - the connection is broken (if there is any error presented including AUTH and configuration issues).

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the MongoDB login credentials.<br>

[comment]: # ({/f43ecfdd-57fd60cc})

[comment]: # ({0f7e3c66-7bd73d75})

##### mongodb.rs.config[connString,<user>,<password>] {#mongodb.rs.config}

<br>
Returns the current configuration of the replica set.<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the MongoDB login credentials.<br>

[comment]: # ({/0f7e3c66-7bd73d75})

[comment]: # ({ab923404-fbfe0a2b})

##### mongodb.rs.status[connString,<user>,<password>] {#mongodb.rs.status}

<br>
Returns the replica set status from the point of view of the member where the method is run.<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the MongoDB login credentials.<br>

[comment]: # ({/ab923404-fbfe0a2b})

[comment]: # ({263d5f42-df1e29bd})

##### mongodb.server.status[connString,<user>,<password>] {#mongodb.server.status}

<br>
Returns the database state.<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the MongoDB login credentials.<br>

[comment]: # ({/263d5f42-df1e29bd})

[comment]: # ({1ffbe870-989959f8})

##### mongodb.sh.discovery[connString,<user>,<password>] {#mongodb.sh.discovery}

<br>
Returns the list of discovered shards present in the cluster.<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the MongoDB login credentials.<br>

[comment]: # ({/1ffbe870-989959f8})

[comment]: # ({902b7b85-d1bb976a})

##### mqtt.get[<broker url>,topic,<username>,<password>] {#mqtt.get}

<br>
Subscribes to a specific topic or topics (with wildcards) of the provided broker and waits for publications.<br>
Return value: Depending on topic content. If wildcards are used, returns topic content as JSON.

Parameters:

-   **broker url** - the MQTT broker URL in the format `protocol://host:port` without query parameters (supported protocols: `tcp`, `ssl`, `ws`). If no value is specified, the agent will use `tcp://localhost:1883`. If a protocol or port are omitted, default protocol (`tcp`) or port (`1883`) will be used; <br>
-   **topic** - the MQTT topic (mandatory). Wildcards (+,\#) are supported;<br>
-   **user, password** - the authentication credentials (if required).<br>

Comments:

-   The item must be configured as an [active check](/manual/appendix/items/activepassive#active_checks) ('Zabbix agent (active)' item type);
-   TLS encryption certificates can be used by saving them into a default location (e.g. `/etc/ssl/certs/` directory for Ubuntu). For TLS, use the `tls://` scheme.

[comment]: # ({/902b7b85-d1bb976a})

[comment]: # ({2ae031cb-87221485})

##### mysql.db.discovery[connString,<user>,<password>] {#mysql.db.discovery}

<br>
Returns the list of MySQL databases. Used for [low-level discovery](/manual/discovery/low_level_discovery).<br>
Return value: The result of the "show databases" SQL query in LLD JSON format.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the MySQL login credentials.<br>

[comment]: # ({/2ae031cb-87221485})

[comment]: # ({7e0d7805-661ad031})

##### mysql.db.size[connString,<user>,<password>,<database name>] {#mysql.db.size}

<br>
The database size in bytes.<br>
Return value: Result of the "select coalesce(sum(data_length + index_length),0) as size from information_schema.tables where table_schema=?" SQL query for specific database in bytes.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the MySQL login credentials;<br>
-   **database name** - the database name.

[comment]: # ({/7e0d7805-661ad031})

[comment]: # ({a3ee5128-c135cdf1})

##### mysql.get_status_variables[connString,<user>,<password>] {#mysql.get.status.variables}

<br>
Values of the global status variables.<br>
Return value: Result of the "show global status" SQL query in JSON format.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the MySQL login credentials.<br>

[comment]: # ({/a3ee5128-c135cdf1})

[comment]: # ({a67f6196-353c20ea})

##### mysql.ping[connString,<user>,<password>] {#mysql.ping}

<br>
Test if a connection is alive or not.<br>
Return value: *1* - the connection is alive; *0* - the connection is broken (if there is any error presented including AUTH and configuration issues).

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the MySQL login credentials.<br>

[comment]: # ({/a67f6196-353c20ea})

[comment]: # ({ae989d3d-baf67c1f})

##### mysql.replication.discovery[connString,<user>,<password>] {#mysql.replication.discovery}

<br>
Returns the list of MySQL replications. Used for [low-level discovery](/manual/discovery/low_level_discovery).<br>
Return value: The result of the "show slave status" SQL query in LLD JSON format.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the MySQL login credentials.<br>

[comment]: # ({/ae989d3d-baf67c1f})

[comment]: # ({a6d06bcf-90f6ce4d})

##### mysql.replication.get_slave_status[connString,<user>,<password>,<master host>] {#mysql.replication.get.slave.status}

<br>
The replication status.<br>
Return value: Result of the "show slave status" SQL query in JSON format.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the MySQL login credentials;<br>
-   **master host** - the replication master host name.<br>

[comment]: # ({/a6d06bcf-90f6ce4d})

[comment]: # ({9a93a3c2-f55a22aa})

##### mysql.version[connString,<user>,<password>] {#mysql.version}

<br>
The MySQL version.<br>
Return value: *String* (with the MySQL instance version).

Parameters:

-   **connString** - the URI or session name;<br>
-   **user, password** - the MySQL login credentials.<br>

[comment]: # ({/9a93a3c2-f55a22aa})

[comment]: # ({32898456-9773f5ca})

##### oracle.diskgroups.stats[connString,<user>,<password>,<service>,<diskgroup>] {#oracle.diskgroups.stats}

<br>
Returns the Automatic Storage Management (ASM) disk groups statistics.<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user** - the Oracle username, supports appending one of the login options `as sysdba`, `as sysoper`, or `as sysasm` in the format `user as sysdba` (a login option is case-insensitive, must not contain a trailing space);<br>
-   **password** - the Oracle password;<br>
-   **service** - the Oracle service name;<br>
-   **diskgroup** - the name of the ASM disk group to query.

[comment]: # ({/32898456-9773f5ca})

[comment]: # ({85603286-ecd5f683})

##### oracle.diskgroups.discovery[connString,<user>,<password>,<service>] {#oracle.diskgroups.discovery}

<br>
Returns the list of ASM disk groups. Used for [low-level discovery](/manual/discovery/low_level_discovery).<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user** - the Oracle username, supports appending one of the login options `as sysdba`, `as sysoper`, or `as sysasm` in the format `user as sysdba` (a login option is case-insensitive, must not contain a trailing space);<br>
-   **password** - the Oracle password;<br>
-   **service** - the Oracle service name.<br>

[comment]: # ({/85603286-ecd5f683})

[comment]: # ({6cfefc73-dcf5ffe2})

##### oracle.archive.info[connString,<user>,<password>,<service>,<destination>] {#oracle.archive.info}

<br>
The archive logs statistics.<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user** - the Oracle username, supports appending one of the login options `as sysdba`, `as sysoper`, or `as sysasm` in the format `user as sysdba` (a login option is case-insensitive, must not contain a trailing space);<br>
-   **password** - the Oracle password;<br>
-   **service** - the Oracle service name;<br>
-   **destination** - the name of the destination to query.

[comment]: # ({/6cfefc73-dcf5ffe2})

[comment]: # ({f7f9ff77-a1afc726})

##### oracle.cdb.info[connString,<user>,<password>,<service>,<database>] {#oracle.cdb.info}

<br>
The Container Databases (CDBs) information.<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user** - the Oracle username, supports appending one of the login options `as sysdba`, `as sysoper`, or `as sysasm` in the format `user as sysdba` (a login option is case-insensitive, must not contain a trailing space);<br>
-   **password** - the Oracle password;<br>
-   **service** - the Oracle service name;<br>
-   **destination** - the name of the database to query.

[comment]: # ({/f7f9ff77-a1afc726})

[comment]: # ({6112c920-83723c39})

##### oracle.custom.query[connString,<user>,<password>,<service>,queryName,<args...>] {#oracle.custom.query}

<br>
The result of a custom query.<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user** - the Oracle username, supports appending one of the login options `as sysdba`, `as sysoper`, or `as sysasm` in the format `user as sysdba` (a login option is case-insensitive, must not contain a trailing space);<br>
-   **password** - the Oracle password;<br>
-   **service** - the Oracle service name;<br>
-   **queryName** — the name of a custom query (must be equal to the name of an SQL file without an extension);
-   **args...** — one or several comma-separated arguments to pass to the query.

[comment]: # ({/6112c920-83723c39})

[comment]: # ({c26cec2b-ef413040})

##### oracle.datafiles.stats[connString,<user>,<password>,<service>] {#oracle.datafiles.stats}

<br>
Returns the data files statistics.<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user** - the Oracle username, supports appending one of the login options `as sysdba`, `as sysoper`, or `as sysasm` in the format `user as sysdba` (a login option is case-insensitive, must not contain a trailing space);<br>
-   **password** - the Oracle password;<br>
-   **service** - the Oracle service name;<br>
-   **diskgroup** - the name of the ASM disk group to query.

[comment]: # ({/c26cec2b-ef413040})

[comment]: # ({22c0704b-3aee96da})

##### oracle.db.discovery[connString,<user>,<password>,<service>] {#oracle.db.discovery}

<br>
Returns the list of databases. Used for [low-level discovery](/manual/discovery/low_level_discovery).<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user** - the Oracle username, supports appending one of the login options `as sysdba`, `as sysoper`, or `as sysasm` in the format `user as sysdba` (a login option is case-insensitive, must not contain a trailing space);<br>
-   **password** - the Oracle password;<br>
-   **service** - the Oracle service name.<br>

[comment]: # ({/22c0704b-3aee96da})

[comment]: # ({21a4553d-d1925e26})

##### oracle.fra.stats[connString,<user>,<password>,<service>] {#oracle.fra.stats}

<br>
Returns the Fast Recovery Area (FRA) statistics.<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user** - the Oracle username, supports appending one of the login options `as sysdba`, `as sysoper`, or `as sysasm` in the format `user as sysdba` (a login option is case-insensitive, must not contain a trailing space);<br>
-   **password** - the Oracle password;<br>
-   **service** - the Oracle service name.<br>

[comment]: # ({/21a4553d-d1925e26})

[comment]: # ({9434d6e7-d0b09033})

##### oracle.instance.info[connString,<user>,<password>,<service>] {#oracle.instance.info}

<br>
The instance statistics.<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user** - the Oracle username, supports appending one of the login options `as sysdba`, `as sysoper`, or `as sysasm` in the format `user as sysdba` (a login option is case-insensitive, must not contain a trailing space);<br>
-   **password** - the Oracle password;<br>
-   **service** - the Oracle service name.<br>

[comment]: # ({/9434d6e7-d0b09033})

[comment]: # ({2fb8fbb2-e6cb70e2})

##### oracle.pdb.info[connString,<user>,<password>,<service>,<database>] {#oracle.pdb.info}

<br>
The Pluggable Databases (PDBs) information.<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user** - the Oracle username, supports appending one of the login options `as sysdba`, `as sysoper`, or `as sysasm` in the format `user as sysdba` (a login option is case-insensitive, must not contain a trailing space);<br>
-   **password** - the Oracle password;<br>
-   **service** - the Oracle service name;<br>
-   **destination** - the name of the database to query.

[comment]: # ({/2fb8fbb2-e6cb70e2})

[comment]: # ({ed2b3db0-7cabfeb8})

##### oracle.pdb.discovery[connString,<user>,<password>,<service>] {#oracle.pdb.discovery}

<br>
Returns the list of PDBs. Used for [low-level discovery](/manual/discovery/low_level_discovery).<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user** - the Oracle username, supports appending one of the login options `as sysdba`, `as sysoper`, or `as sysasm` in the format `user as sysdba` (a login option is case-insensitive, must not contain a trailing space);<br>
-   **password** - the Oracle password;<br>
-   **service** - the Oracle service name.<br>

[comment]: # ({/ed2b3db0-7cabfeb8})

[comment]: # ({5320d47a-2ca3eeaf})

##### oracle.pga.stats[connString,<user>,<password>,<service>] {#oracle.pga.stats}

<br>
Returns the Program Global Area (PGA) statistics.<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user** - the Oracle username, supports appending one of the login options `as sysdba`, `as sysoper`, or `as sysasm` in the format `user as sysdba` (a login option is case-insensitive, must not contain a trailing space);<br>
-   **password** - the Oracle password;<br>
-   **service** - the Oracle service name.<br>

[comment]: # ({/5320d47a-2ca3eeaf})

[comment]: # ({6b6f2f0d-e13b9288})

##### oracle.ping[connString,<user>,<password>,<service>] {#oracle.ping}

<br>
Test whether a connection to Oracle can be established.<br>
Return value: *1* - the connection is successful; *0* - the connection is broken (if there is any error presented including AUTH and configuration issues).

Parameters:

-   **connString** - the URI or session name;<br>
-   **user** - the Oracle username, supports appending one of the login options `as sysdba`, `as sysoper`, or `as sysasm` in the format `user as sysdba` (a login option is case-insensitive, must not contain a trailing space);<br>
-   **password** - the Oracle password;<br>
-   **service** - the Oracle service name.<br>

[comment]: # ({/6b6f2f0d-e13b9288})

[comment]: # ({1ffdbba2-77cd7d3d})

##### oracle.proc.stats[connString,<user>,<password>,<service>] {#oracle.proc.stats}

<br>
Returns the processes statistics.<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user** - the Oracle username, supports appending one of the login options `as sysdba`, `as sysoper`, or `as sysasm` in the format `user as sysdba` (a login option is case-insensitive, must not contain a trailing space);<br>
-   **password** - the Oracle password;<br>
-   **service** - the Oracle service name.<br>

[comment]: # ({/1ffdbba2-77cd7d3d})

[comment]: # ({622bb10c-f219cf27})

##### oracle.redolog.info[connString,<user>,<password>,<service>] {#oracle.redolog.info}

<br>
The log file information from the control file.<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user** - the Oracle username, supports appending one of the login options `as sysdba`, `as sysoper`, or `as sysasm` in the format `user as sysdba` (a login option is case-insensitive, must not contain a trailing space);<br>
-   **password** - the Oracle password;<br>
-   **service** - the Oracle service name.<br>

[comment]: # ({/622bb10c-f219cf27})

[comment]: # ({393dd9eb-2008672b})

##### oracle.sga.stats[connString,<user>,<password>,<service>] {#oracle.sga.stats}

<br>
Returns the System Global Area (SGA) statistics.<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user** - the Oracle username, supports appending one of the login options `as sysdba`, `as sysoper`, or `as sysasm` in the format `user as sysdba` (a login option is case-insensitive, must not contain a trailing space);<br>
-   **password** - the Oracle password;<br>
-   **service** - the Oracle service name.<br>

[comment]: # ({/393dd9eb-2008672b})

[comment]: # ({5547c74d-2a343448})

##### oracle.sessions.stats[connString,<user>,<password>,<service>,<lockMaxTime>] {#oracle.sessions.stats}

<br>
Returns the sessions statistics.<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user** - the Oracle username, supports appending one of the login options `as sysdba`, `as sysoper`, or `as sysasm` in the format `user as sysdba` (a login option is case-insensitive, must not contain a trailing space);<br>
-   **password** - the Oracle password;<br>
-   **service** - the Oracle service name;<br>
-   **lockMaxTime** - the maximum session lock duration in seconds to count the session as a prolongedly locked. Default: 600 seconds.

[comment]: # ({/5547c74d-2a343448})

[comment]: # ({3dc57e1d-585ccbd4})

##### oracle.sys.metrics[connString,<user>,<password>,<service>,<duration>] {#oracle.sys.metrics}

<br>
Returns a set of system metric values.<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user** - the Oracle username, supports appending one of the login options `as sysdba`, `as sysoper`, or `as sysasm` in the format `user as sysdba` (a login option is case-insensitive, must not contain a trailing space);<br>
-   **password** - the Oracle password;<br>
-   **service** - the Oracle service name;<br>
-   **duration** - the capturing interval (in seconds) of system metric values. Possible values: *60* — long duration (default), *15* — short duration.

[comment]: # ({/3dc57e1d-585ccbd4})

[comment]: # ({42f98447-4ad20624})

##### oracle.sys.params[connString,<user>,<password>,<service>] {#oracle.sys.params}

<br>
Returns a set of system parameter values.<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user** - the Oracle username, supports appending one of the login options `as sysdba`, `as sysoper`, or `as sysasm` in the format `user as sysdba` (a login option is case-insensitive, must not contain a trailing space);<br>
-   **password** - the Oracle password;<br>
-   **service** - the Oracle service name.<br>

[comment]: # ({/42f98447-4ad20624})

[comment]: # ({94b8678b-fd9b5b8e})

##### oracle.ts.stats[connString,<user>,<password>,<service>,<tablespace>,<type>,<conname>] {#oracle.ts.stats}

<br>
Returns the tablespaces statistics.<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user** - the Oracle username, supports appending one of the login options `as sysdba`, `as sysoper`, or `as sysasm` in the format `user as sysdba` (a login option is case-insensitive, must not contain a trailing space);<br>
-   **password** - the Oracle password;<br>
-   **service** - the Oracle service name;<br>
-   **tablespace** - name of the tablespace to query. Default (if left empty and `type` is set):<br>- "TEMP" (if `type` is set to "TEMPORARY");<br>- "USERS" (if `type` is set to "PERMANENT").
-   **type** - the type of the tablespace to query. Default (if `tablespace` is set): "PERMANENT".
-   **conname** - name of the container for which the information is required.

If `tablespace`, `type`, or `conname` is omitted, the item will return tablespace statistics for all matching containers (including PDBs and CDB).

[comment]: # ({/94b8678b-fd9b5b8e})

[comment]: # ({0c4bc7f0-836285d4})

##### oracle.ts.discovery[connString,<user>,<password>,<service>] {#oracle.ts.discovery}

<br>
Returns a list of tablespaces. Used for [low-level discovery](/manual/discovery/low_level_discovery).<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user** - the Oracle username, supports appending one of the login options `as sysdba`, `as sysoper`, or `as sysasm` in the format `user as sysdba` (a login option is case-insensitive, must not contain a trailing space);<br>
-   **password** - the Oracle password;<br>
-   **service** - the Oracle service name.

[comment]: # ({/0c4bc7f0-836285d4})

[comment]: # ({e612d95b-213a30fa})

##### oracle.user.info[connString,<user>,<password>,<service>,<username>] {#oracle.user.info}

<br>
Returns Oracle user information.<br>
Return value: *JSON object*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **user** - the Oracle username, supports appending one of the login options `as sysdba`, `as sysoper`, or `as sysasm` in the format `user as sysdba` (a login option is case-insensitive, must not contain a trailing space);<br>
-   **password** - the Oracle password;<br>
-   **service** - the Oracle service name;<br>
-   **username** - the username for which the information is needed. Lowercase usernames are not supported. Default: current user.

[comment]: # ({/e612d95b-213a30fa})

[comment]: # ({3606c222-681f20c2})

##### pgsql.autovacuum.count[uri,<username>,<password>,<database name>] {#pgsql.autovacuum.count}

<br>
The number of autovacuum workers.<br>
Return value: *Integer*.

Parameters:

-   **uri** - the URI or session name;<br>
-   **username, password** - the PostgreSQL credentials;<br>
-   **database name** - the database name.<br>

[comment]: # ({/3606c222-681f20c2})

[comment]: # ({5221f538-d00cb61c})

##### pgsql.archive[uri,<username>,<password>,<database name>] {#pgsql.archive}

<br>
The information about archived files.<br>
Return value: *JSON object*.

Parameters:

-   **uri** - the URI or session name;<br>
-   **username, password** - the PostgreSQL credentials;<br>
-   **database name** - the database name.<br>

The returned data are processed by dependent items:

-   **pgsql.archive.count_archived_files** - the number of WAL files that have been successfully archived;<br>
-   **pgsql.archive.failed_trying_to_archive** - the number of failed attempts for archiving WAL files;<br>
-   **pgsql.archive.count_files_to_archive** - the number of files to archive;<br>
-   **pgsql.archive.size_files_to_archive** - the size of files to archive.

[comment]: # ({/5221f538-d00cb61c})

[comment]: # ({18791e61-00011d59})

##### pgsql.bgwriter[uri,<username>,<password>,<database name>] {#pgsql.bgwriter}

<br>
The combined number of checkpoints for the database cluster, broken down by checkpoint type.<br>
Return value: *JSON object*.

Parameters:

-   **uri** - the URI or session name;<br>
-   **username, password** - the PostgreSQL credentials;<br>
-   **database name** - the database name.<br>

The returned data are processed by dependent items:

-   **pgsql.bgwriter.buffers_alloc** - the number of buffers allocated;<br>
-   **pgsql.bgwriter.buffers_backend** - the number of buffers written directly by a backend;<br>
-   **pgsql.bgwriter.maxwritten_clean** - the number of times the background writer stopped a cleaning scan, because it had written too many buffers;<br>
-   **pgsql.bgwriter.buffers_backend_fsync** -the number of times a backend had to execute its own fsync call instead of the background writer;
-   **pgsql.bgwriter.buffers_clean** - the number of buffers written by the background writer;
-   **pgsql.bgwriter.buffers_checkpoint** - the number of buffers written during checkpoints;
-   **pgsql.bgwriter.checkpoints_timed** - the number of scheduled checkpoints that have been performed;
-   **pgsql.bgwriter.checkpoints_req** - the number of requested checkpoints that have been performed;
-   **pgsql.bgwriter.checkpoint_write_time** - the total amount of time spent in the portion of checkpoint processing where files are written to disk, in milliseconds;
-   **pgsql.bgwriter.sync_time** - the total amount of time spent in the portion of checkpoint processing where files are synchronized with disk.

[comment]: # ({/18791e61-00011d59})

[comment]: # ({2b9f1955-00ab276d})

##### pgsql.cache.hit[uri,<username>,<password>,<database name>] {#pgsql.cache.hit}

<br>
The PostgreSQL buffer cache hit rate.<br>
Return value: *Float*.

Parameters:

-   **uri** - the URI or session name;<br>
-   **username, password** - the PostgreSQL credentials;<br>
-   **database name** - the database name.<br>

[comment]: # ({/2b9f1955-00ab276d})

[comment]: # ({5408ea56-32909ece})

##### pgsql.connections[uri,<username>,<password>,<database name>] {#pgsql.connections}

<br>
Returns connections by type.<br>
Return value: *JSON object*.

Parameters:

-   **uri** - the URI or session name;<br>
-   **username, password** - the PostgreSQL credentials;<br>
-   **database name** - the database name.<br>

The returned data are processed by dependent items:

-   **pgsql.connections.active** - the backend is executing a query;<br>
-   **pgsql.connections.fastpath_function_call** - the backend is executing a fast-path function;<br>
-   **pgsql.connections.idle** - the backend is waiting for a new client command;<br>
-   **pgsql.connections.idle_in_transaction** - the backend is in a transaction, but is not currently executing a query;<br>
-   **pgsql.connections.prepared** - the number of prepared connections;<br>
-   **pgsql.connections.total** - the total number of connections;<br>
-   **pgsql.connections.total_pct** - percentage of total connections in respect to ‘max\_connections’ setting of the PostgreSQL server;<br>
-   **pgsql.connections.waiting** - number of connections in a query;<br>
-   **pgsql.connections.idle_in_transaction_aborted** - the backend is in a transaction, but is not currently executing a query and one of the statements in the transaction caused an error.

[comment]: # ({/5408ea56-32909ece})

[comment]: # ({af31ff7b-21431ffe})

##### pgsql.custom.query[uri,<username>,<password>,queryName[,args...]] {#pgsql.custom.query}

<br>
Returns the result of a custom query.<br>
Return value: *JSON object*.

Parameters:

-   **uri** - the URI or session name;<br>
-   **username, password** - the PostgreSQL credentials;<br>
-   **queryName** - the name of a custom query, must match the SQL file name without an extension;<br>
-   **args**(optional) - the arguments to pass to the query.

[comment]: # ({/af31ff7b-21431ffe})

[comment]: # ({55557d93-11f6be08})

##### pgsql.db.age[uri,<username>,<password>,<database name>] {#pgsql.db.age}

<br>
The age of the oldest FrozenXID of the database.<br>
Return value: *Integer*.

Parameters:

-   **uri** - the URI or session name;<br>
-   **username, password** - the PostgreSQL credentials;<br>
-   **database name** - the database name.<br>

[comment]: # ({/55557d93-11f6be08})

[comment]: # ({e2d41203-ae9c9740})

##### pgsql.db.bloating_tables[uri,<username>,<password>,<database name>] {#pgsql.db.bloating.tables}

<br>
The number of bloating tables per database.<br>
Return value: *Integer*.

Parameters:

-   **uri** - the URI or session name;<br>
-   **username, password** - the PostgreSQL credentials;<br>
-   **database name** - the database name.<br>

[comment]: # ({/e2d41203-ae9c9740})

[comment]: # ({906aea38-bead66f0})

##### pgsql.db.discovery[uri,<username>,<password>,<database name>] {#pgsql.db.discovery}

<br>
The list of PostgreSQL databases. Used for [low-level discovery](/manual/discovery/low_level_discovery).<br>
Return value: *JSON object*.

Parameters:

-   **uri** - the URI or session name;<br>
-   **username, password** - the PostgreSQL credentials;<br>
-   **database name** - the database name.<br>

[comment]: # ({/906aea38-bead66f0})

[comment]: # ({3325bbee-a209d73f})

##### pgsql.db.size[uri,<username>,<password>,<database name>] {#pgsql.db.size}

<br>
The database size in bytes.<br>
Return value: *Integer*.

Parameters:

-   **uri** - the URI or session name;<br>
-   **username, password** - the PostgreSQL credentials;<br>
-   **database name** - the database name.<br>

[comment]: # ({/3325bbee-a209d73f})

[comment]: # ({c6fb95ba-2a2e3c05})

##### pgsql.dbstat[uri,<username>,<password>,<database name>] {#pgsql.dbstat}

<br>
Collects the statistics per database. Used for [low-level discovery](/manual/discovery/low_level_discovery).<br>
Return value: *JSON object*.

Parameters:

-   **uri** - the URI or session name;<br>
-   **username, password** - the PostgreSQL credentials;<br>
-   **database name** - the database name.<br>

The returned data are processed by dependent items:

-   **pgsql.dbstat.numbackends["{#DBNAME}"]** - the number of backends currently connected to this database;<br>
-   **pgsql.dbstat.sum.blk_read_time["{#DBNAME}"]** - the time spent reading data file blocks by backends in this database, in milliseconds;<br>
-   **pgsql.dbstat.sum.blk_write_time["{#DBNAME}"]** - the time spent writing data file blocks by backends in this database, in milliseconds;<br>
-   **pgsql.dbstat.sum.checksum_failures["{#DBNAME}"]** - the number of data page checksum failures detected (or on a shared object), or NULL if data checksums are not enabled (PostgreSQL version 12 only);<br>
-   **pgsql.dbstat.blks_read.rate["{#DBNAME}"]** - the number of disk blocks read in this database;<br>
-   **pgsql.dbstat.deadlocks.rate["{#DBNAME}"]** - the number of deadlocks detected in this database;<br>
-   **pgsql.dbstat.blks_hit.rate["{#DBNAME}"]** - the number of times disk blocks were found already in the buffer cache, so that a read was not necessary (this only includes hits in the PostgreSQL Pro buffer cache, not the operating system's file system cache);<br>
-   **pgsql.dbstat.xact_rollback.rate["{#DBNAME}"]** - the number of transactions in this database that have been rolled back;<br>
-   **pgsql.dbstat.xact_commit.rate["{#DBNAME}"]** - the number of transactions in this database that have been committed;<br>
-   **pgsql.dbstat.tup_updated.rate["{#DBNAME}"]** - the number of rows updated by queries in this database;<br>
-   **pgsql.dbstat.tup_returned.rate["{#DBNAME}"]** - the number of rows returned by queries in this database;<br>
-   **pgsql.dbstat.tup_inserted.rate["{#DBNAME}"]** - the number of rows inserted by queries in this database;<br>
-   **pgsql.dbstat.tup_fetched.rate["{#DBNAME}"]** - the number of rows fetched by queries in this database;<br>
-   **pgsql.dbstat.tup_deleted.rate["{#DBNAME}"]** - the number of rows deleted by queries in this database;<br>
-   **pgsql.dbstat.conflicts.rate["{#DBNAME}"]** - the number of queries canceled due to conflicts with recovery in this database (the conflicts occur only on standby servers);<br>
-   **pgsql.dbstat.temp_files.rate["{#DBNAME}"]** - the number of temporary files created by queries in this database. All temporary files are counted, regardless of the log_temp_files settings and reasons for which the temporary file was created (e.g., sorting or hashing);<br>
-   **pgsql.dbstat.temp_bytes.rate["{#DBNAME}"]** - the total amount of data written to temporary files by queries in this database. Includes data from all temporary files, regardless of the log_temp_files settings and reasons for which the temporary file was created (e.g., sorting or hashing).

[comment]: # ({/c6fb95ba-2a2e3c05})

[comment]: # ({cc1b5a61-96f0fa06})

##### pgsql.dbstat.sum[uri,<username>,<password>,<database name>] {#pgsql.dbstat.sum}

<br>
The summarized data for all databases in a cluster.<br>
Return value: *JSON object*.

Parameters:

-   **uri** - the URI or session name;<br>
-   **username, password** - the PostgreSQL credentials;<br>
-   **database name** - the database name.<br>

The returned data are processed by dependent items:

-   **pgsql.dbstat.numbackends** - the number of backends currently connected to this database;<br>
-   **pgsql.dbstat.sum.blk\_read\_time** - time spent reading data file blocks by backends in this database, in milliseconds;<br>
-   **pgsql.dbstat.sum.blk\_write\_time** - time spent writing data file blocks by backends in this database, in milliseconds;<br>
-   **pgsql.dbstat.sum.checksum\_failures** - the number of data page checksum failures detected (or on a shared object), or NULL if data checksums are not enabled (PostgreSQL version 12 only);<br>
-   **pgsql.dbstat.sum.xact\_commit** - the number of transactions in this database that have been committed;<br>
-   **pgsql.dbstat.sum.conflicts** - database statistics about query cancels due to conflict with recovery on standby servers;<br>
-   **pgsql.dbstat.sum.deadlocks** - the number of deadlocks detected in this database;<br>
-   **pgsql.dbstat.sum.blks\_read** - the number of disk blocks read in this database;<br>
-   **pgsql.dbstat.sum.blks\_hit** - the number of times disk blocks were found already in the buffer cache, so a read was not necessary (only hits in the PostgreSQL Pro buffer cache are included);<br>
-   **pgsql.dbstat.sum.temp\_bytes** - the total amount of data written to temporary files by queries in this database. Includes data from all temporary files, regardless of the log\_temp\_files settings and reasons for which the temporary file was created (e.g., sorting or hashing);<br>
-   **pgsql.dbstat.sum.temp_files** - the number of temporary files created by queries in this database. All temporary files are counted, regardless of the log\_temp\_files settings and reasons for which the temporary file was created (e.g., sorting or hashing);<br>
-   **pgsql.dbstat.sum.xact_rollback** - the number of rolled-back transactions in this database;<br>
-   **pgsql.dbstat.sum.tup_deleted** - the number of rows deleted by queries in this database;<br>
-   **pgsql.dbstat.sum.tup_fetched** - the number of rows fetched by queries in this database;<br>
-   **pgsql.dbstat.sum.tup_inserted** - the number of rows inserted by queries in this database;<br>
-   **pgsql.dbstat.sum.tup_returned** - the number of rows returned by queries in this database;<br>
-   **pgsql.dbstat.sum.tup_updated** - the number of rows updated by queries in this database.

[comment]: # ({/cc1b5a61-96f0fa06})

[comment]: # ({fe18575a-14ca0aec})

##### pgsql.locks[uri,<username>,<password>,<database name>] {#pgsql.locks}

<br>
The information about granted locks per database. Used for [low-level discovery](/manual/discovery/low_level_discovery).<br>
Return value: *JSON object*.

Parameters:

-   **uri** - the URI or session name;<br>
-   **username, password** - the PostgreSQL credentials;<br>
-   **database name** - the database name.<br>

The returned data are processed by dependent items:

-   **pgsql.locks.shareupdateexclusive["{#DBNAME}"]** - the number of share update exclusive locks;<br>
-   **pgsql.locks.accessexclusive["{#DBNAME}"]** - the number of access exclusive locks;<br>
-   **pgsql.locks.accessshare["{#DBNAME}"]** - the number of access share locks;<br>
-   **pgsql.locks.exclusive["{#DBNAME}"]** - the number of exclusive locks;<br>
-   **pgsql.locks.rowexclusive["{#DBNAME}"]** - the number of row exclusive locks;<br>
-   **pgsql.locks.rowshare["{#DBNAME}"]** - the number of row share locks;<br>
-   **pgsql.locks.share["{#DBNAME}"]** - the number of shared locks;<br>
-   **pgsql.locks.sharerowexclusive["{#DBNAME}"]** - the number of share row exclusive locks.

[comment]: # ({/fe18575a-14ca0aec})

[comment]: # ({e29d5ddf-f37282e1})

##### pgsql.oldest.xid[uri,<username>,<password>,<database name>] {#pgsql.oldest.xid}

<br>
The age of the oldest XID.<br>
Return value: *Integer*.

Parameters:

-   **uri** - the URI or session name;<br>
-   **username, password** - the PostgreSQL credentials;<br>
-   **database name** - the database name.<br>

[comment]: # ({/e29d5ddf-f37282e1})

[comment]: # ({26d9b81d-a61feaf8})

##### pgsql.ping[uri,<username>,<password>,<database name>] {#pgsql.ping}

<br>
Tests whether a connection is alive or not.<br>
Return value: *1* - the connection is alive; *0* - the connection is broken (if there is any error presented including AUTH and configuration issues).

Parameters:

-   **uri** - the URI or session name;<br>
-   **username, password** - the PostgreSQL credentials;<br>
-   **database name** - the database name.<br>

[comment]: # ({/26d9b81d-a61feaf8})

[comment]: # ({017fc882-1240f49d})

##### pgsql.queries[uri,<username>,<password>,<database name>,<time period>] {#pgsql.queries}

<br>
Queries metrics by execution time.<br>
Return value: *JSON object*.

Parameters:

-   **uri** - the URI or session name;<br>
-   **username, password** - the PostgreSQL credentials;<br>
-   **database name** - the database name;<br>
-   **timePeriod** - the execution time limit for the count of slow queries (must be a positive integer).

The returned data are processed by dependent items:

-   **pgsql.queries.mro.time_max["{#DBNAME}"]** - the max maintenance query time;<br>
-   **pgsql.queries.query.time_max["{#DBNAME}"]** - the max query time;<br>
-   **pgsql.queries.tx.time_max["{#DBNAME}"]** - the max transaction query time;<br>
-   **pgsql.queries.mro.slow_count["{#DBNAME}"]** - the slow maintenance query count;<br>
-   **pgsql.queries.query.slow_count["{#DBNAME}"]** - the slow query count;<br>
-   **pgsql.queries.tx.slow_count["{#DBNAME}"]** - the slow transaction query count;<br>
-   **pgsql.queries.mro.time_sum["{#DBNAME}"]** - the sum maintenance query time;<br>
-   **pgsql.queries.query.time_sum["{#DBNAME}"]** - the sum query time;<br>
-   **pgsql.queries.tx.time_sum["{#DBNAME}"]** - the sum transaction query time.

[comment]: # ({/017fc882-1240f49d})

[comment]: # ({47358084-7da2d7a1})

##### pgsql.replication.count[uri,<username>,<password>] {#pgsql.replication.count}

<br>
The number of standby servers.<br>
Return value: *Integer*.

Parameters:

-   **uri** - the URI or session name;<br>
-   **username, password** - the PostgreSQL credentials.

[comment]: # ({/47358084-7da2d7a1})

[comment]: # ({10745ed9-46c214a7})

##### pgsql.replication.process[uri,<username>,<password>] {#pgsql.replication.process}

<br>
The flush lag, write lag and replay lag per each sender process.<br>
Return value: *JSON object*.

Parameters:

-   **uri** - the URI or session name;<br>
-   **username, password** - the PostgreSQL credentials.

[comment]: # ({/10745ed9-46c214a7})

[comment]: # ({0218eeb9-4e97798b})

##### pgsql.replication.process.discovery[uri,<username>,<password>] {#pgsql.replication.process.discovery}

<br>
The replication process name discovery.<br>
Return value: *JSON object*.

Parameters:

-   **uri** - the URI or session name;<br>
-   **username, password** - the PostgreSQL credentials.

[comment]: # ({/0218eeb9-4e97798b})

[comment]: # ({03869f1d-2db43ebf})

##### pgsql.replication.recovery_role[uri,<username>,<password>] {#pgsql.replication.recovery.role}

<br>
The recovery status.<br>
Return value: *0* - master mode; *1* - recovery is still in progress (standby mode).

Parameters:

-   **uri** - the URI or session name;<br>
-   **username, password** - the PostgreSQL credentials.

[comment]: # ({/03869f1d-2db43ebf})

[comment]: # ({fa4ca4ae-0b42de61})

##### pgsql.replication.status[uri,<username>,<password>] {#pgsql.replication.status}

<br>
The status of replication.<br>
Return value: *0* - streaming is down; *1* - streaming is up; *2* - master mode.

Parameters:

-   **uri** - the URI or session name;<br>
-   **username, password** - the PostgreSQL credentials.

[comment]: # ({/fa4ca4ae-0b42de61})

[comment]: # ({733430cc-dce31b13})

##### pgsql.replication_lag.b[uri,<username>,<password>] {#pgsql.replication.lag.b}

<br>
The replication lag in bytes.<br>
Return value: *Integer*.

Parameters:

-   **uri** - the URI or session name;<br>
-   **username, password** - the PostgreSQL credentials.

[comment]: # ({/733430cc-dce31b13})

[comment]: # ({39a7c467-911ce6f3})

##### pgsql.replication_lag.sec[uri,<username>,<password>] {#pgsql.replication.lag.sec}

<br>
The replication lag in seconds.<br>
Return value: *Integer*.

Parameters:

-   **uri** - the URI or session name;<br>
-   **username, password** - the PostgreSQL credentials.

[comment]: # ({/39a7c467-911ce6f3})

[comment]: # ({7d94cb35-a81f90e6})

##### pgsql.uptime[uri,<username>,<password>,<database name>] {#pgsql.uptime}

<br>
The PostgreSQL uptime in milliseconds.<br>
Return value: *Float*.

Parameters:

-   **uri** - the URI or session name;<br>
-   **username, password** - the PostgreSQL credentials;<br>
-   **database name** - the database name.<br>

[comment]: # ({/7d94cb35-a81f90e6})

[comment]: # ({5fd155b9-3a4ac3ad})

##### pgsql.wal.stat[uri,<username>,<password>,<database name>] {#pgsql.wal.stat}

<br>
The WAL statistics.<br>
Return value: *JSON object*.

Parameters:

-   **uri** - the URI or session name;<br>
-   **username, password** - the PostgreSQL credentials;<br>
-   **database name** - the database name.<br>

The returned data are processed by dependent items:

-   **pgsql.wal.count** — the number of WAL files;<br>
-   **pgsql.wal.write** - the WAL lsn used (in bytes).

[comment]: # ({/5fd155b9-3a4ac3ad})

[comment]: # ({dca8c3c6-fb03aca5})

##### redis.config[connString,<password>,<pattern>] {#redis.config}

<br>
Gets the configuration parameters of a Redis instance that match the pattern.<br>
Return value: *JSON* - if a glob-style pattern was used; single value - if a pattern did not contain any wildcard character.

Parameters:

-   **connString** - the URI or session name;<br>
-   **password** - the Redis password;<br>
-   **pattern** - a glob-style pattern (*\** by default).

[comment]: # ({/dca8c3c6-fb03aca5})

[comment]: # ({2332f252-8c5408f6})

##### redis.info[connString,<password>,<section>] {#redis.info}

<br>
Gets the output of the INFO command.<br>
Return value: *JSON* - the output is serialized as JSON.

Parameters:

-   **connString** - the URI or session name;<br>
-   **password** - the Redis password;<br>
-   **section** - the [section](https://redis.io/commands/info) of information (*default* by default).<br>

[comment]: # ({/2332f252-8c5408f6})

[comment]: # ({8a82f58e-7952fb43})

##### redis.ping[connString,<password>] {#redis.ping}

<br>
Test if a connection is alive or not.<br>
Return value: *1* - the connection is alive; *0* - the connection is broken (if there is any error presented including AUTH and configuration issues).

Parameters:

-   **connString** - the URI or session name;<br>
-   **password** - the Redis password.<br>

[comment]: # ({/8a82f58e-7952fb43})

[comment]: # ({68b1ad5c-e6d86f50})

##### redis.slowlog.count[connString,<password>] {#redis.slowlog.count}

<br>
The number of slow log entries since Redis was started.<br>
Return value: *Integer*.

Parameters:

-   **connString** - the URI or session name;<br>
-   **password** - the Redis password.<br>

[comment]: # ({/68b1ad5c-e6d86f50})

[comment]: # ({3b5f8468-74faa2be})

##### smart.attribute.discovery {#smart.attribute.discovery}

<br>
Returns a list of S.M.A.R.T. device attributes.<br>
Return value: *JSON object*.

Comments:

-   The following macros and their values are returned: {\#NAME}, {\#DISKTYPE}, {\#ID}, {\#ATTRNAME}, {\#THRESH};
-   HDD, SSD and NVME drive types are supported. Drives can be alone or combined in a RAID. {\#NAME} will have an add-on in case of RAID, e.g: {"{\#NAME}": "/dev/sda cciss,2"}.

[comment]: # ({/3b5f8468-74faa2be})

[comment]: # ({dfb10ece-c0ca5d9d})

##### smart.disk.discovery {#smart.disk.discovery}

<br>
Returns a list of S.M.A.R.T. devices.<br>
Return value: *JSON object*.

Comments:

-   The following macros and their values are returned: {\#NAME}, {\#DISKTYPE}, {\#MODEL}, {\#SN}, {\#PATH}, {\#ATTRIBUTES}, {\#RAIDTYPE};
-   HDD, SSD and NVME drive types are supported. If a drive does not belong to a RAID, the {\#RAIDTYPE} will be empty. {\#NAME} will have an add-on in case of RAID, e.g: {"{\#NAME}": "/dev/sda cciss,2"}.

[comment]: # ({/dfb10ece-c0ca5d9d})

[comment]: # ({2a7c8eea-85fda450})

##### smart.disk.get[<path>,<raid type>] {#smart.disk.get}

<br>
Returns all available properties of S.M.A.R.T. devices.<br>
Return value: *JSON object*.

Parameters:

-   **path** - the disk path, the {\#PATH} macro may be used as a value;<br>
-   **raid_type** - the RAID type, the {\#RAID} macro may be used as a value

Comments:

-   HDD, SSD and NVME drive types are supported. Drives can be alone or combined in a RAID;<br>
-   The data includes smartctl version and call arguments, and additional fields:<br>*disk\_name* - holds the name with the required add-ons for RAID discovery, e.g: {"disk\_name": "/dev/sda cciss,2"}<br>*disk\_type* - holds the disk type HDD, SSD, or NVME, e.g: {"disk\_type": "ssd"};<br>
-   If no parameters are specified, the item will return information about all disks.

[comment]: # ({/2a7c8eea-85fda450})

[comment]: # ({57eccce8-a2e1203a})

##### systemd.unit.get[unit name,<interface>] {#systemd.unit.get}

<br>
Returns all properties of a systemd unit.<br>
Return value: *JSON object*.

Parameters:

-   **unit name** - the unit name (you may want to use the {\#UNIT.NAME} macro in item prototype to discover the name);<br>
-   **interface** - the unit interface type, possible values: *Unit* (default), *Service*, *Socket*, *Device*, *Mount*, *Automount*, *Swap*, *Target*, *Path*.

Comments:

-   This item is supported on Linux platform only;
-   LoadState, ActiveState and UnitFileState for Unit interface are returned as text and integer: `"ActiveState":{"state":1,"text":"active"}`.

[comment]: # ({/57eccce8-a2e1203a})

[comment]: # ({c6880990-04d19e13})

##### systemd.unit.info[unit name,<property>,<interface>] {#systemd.unit.info}

<br>
A systemd unit information.<br>
Return value: *String*.

Parameters:

-   **unit name** - the unit name (you may want to use the {\#UNIT.NAME} macro in item prototype to discover the name);<br>
-   **property** - unit property (e.g. ActiveState (default), LoadState, Description);
-   **interface** - the unit interface type (e.g. Unit (default), Socket, Service).

Comments:

-   This item is supported on Linux platform only;
-   This item allows to retrieve a specific property from specific type of interface as described in [dbus API](https://www.freedesktop.org/wiki/Software/systemd/dbus/).

Examples:

    systemd.unit.info["{#UNIT.NAME}"] #collect active state (active, reloading, inactive, failed, activating, deactivating) info on discovered systemd units
    systemd.unit.info["{#UNIT.NAME}",LoadState] #collect load state info on discovered systemd units
    systemd.unit.info[mysqld.service,Id] #retrieve the service technical name (mysqld.service)
    systemd.unit.info[mysqld.service,Description] #retrieve the service description (MySQL Server)
    systemd.unit.info[mysqld.service,ActiveEnterTimestamp] #retrieve the last time the service entered the active state (1562565036283903)
    systemd.unit.info[dbus.socket,NConnections,Socket] #collect the number of connections from this socket unit

[comment]: # ({/c6880990-04d19e13})

[comment]: # ({6af49cf5-28908462})

##### systemd.unit.discovery[<type>] {#systemd.unit.discovery}

<br>
List of systemd units and their details. Used for [low-level discovery](/manual/discovery/low_level_discovery/examples/systemd).<br>
Return value: *JSON object*.

Parameter:

-   **type** - possible values: *all*, *automount*, *device*, *mount*, *path*, *service* (default), *socket*, *swap*, *target*.

This item is supported on Linux platform only.

[comment]: # ({/6af49cf5-28908462})

[comment]: # ({7e724a76-9a731445})

##### web.certificate.get[hostname,<port>,<address>] {#web.certificate.get}

<br>
Validates the certificates and returns certificate details.<br>
Return value: *JSON object*.

Parameter:

-   **hostname** - can be either IP or DNS.<br>May contain the URL scheme (*https* only), path (it will be ignored), and port.<br>If a port is provided in both the first and the second parameters, their values must match.<br>If address (the 3rd parameter) is specified, the hostname is only used for SNI and hostname verification;<br>
-   **port** - the port number (default is 443 for HTTPS);<br>
-   **address** - can be either IP or DNS. If specified, it will be used for connection, and hostname (the 1st parameter) will be used for SNI, and host verification. In case, the 1st parameter is an IP and the 3rd parameter is DNS, the 1st parameter will be used for connection, and the 3rd parameter will be used for SNI and host verification.

Comments:

-   This item turns unsupported if the resource specified in `host` does not exist or is unavailable or if TLS handshake fails with any error except an invalid certificate;<br>
-   Currently, AIA (Authority Information Access) X.509 extension, CRLs and OCSP (including OCSP stapling), Certificate Transparency, and custom CA trust store are not supported.

[comment]: # ({/7e724a76-9a731445})
