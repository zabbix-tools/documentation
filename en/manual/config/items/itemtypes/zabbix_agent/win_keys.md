[comment]: # attributes: notoc

[comment]: # ({cf79dc25-071dd107})
# Windows Zabbix agent

[comment]: # ({/cf79dc25-071dd107})

[comment]: # ({37a1a613-bfb617f2})

### Overview

The Windows Zabbix agent items are presented in two lists:

- [Shared items](#shared-items) - the item keys that are shared with the UNIX Zabbix agent;
- [Windows-specific items](#windows-specific-items) - the item keys that are supported **only** on Windows.

Note that all item keys supported by Zabbix agent on Windows are also supported by 
the new generation Zabbix agent 2. See the [additional item keys](/manual/config/items/itemtypes/zabbix_agent/zabbix_agent2) 
that you can use with the agent 2 only.

See also: [Minimum permissions for Windows items](/manual/appendix/items/win_permissions)

[comment]: # ({/37a1a613-bfb617f2})

[comment]: # ({9c68e4d4-52f3ae09})

### Shared items

The table below lists Zabbix agent items that are supported on Windows and 
are shared with the UNIX Zabbix agent:

-   The item key is a link to full details of the UNIX Zabbix agent item
-   Windows-relevant item comments are included

|Item key|Description|Item group|
|--|-------|-|
|[log](/manual/config/items/itemtypes/zabbix_agent#log)|The monitoring of a log file. This item is not supported for Windows Event Log.<br>The `persistent_dir` parameter is not supported on Windows.|[Log monitoring](/manual/config/items/itemtypes/log_items)|
|[log.count](/manual/config/items/itemtypes/zabbix_agent#log.count)|The count of matched lines in a monitored log file. This item is not supported for Windows Event Log.<br>The `persistent_dir` parameter is not supported on Windows.|^|
|[logrt](/manual/config/items/itemtypes/zabbix_agent#logrt)|The monitoring of a log file that is rotated. This item is not supported for Windows Event Log.<br>The `persistent_dir` parameter is not supported on Windows.|^|
|[logrt.count](/manual/config/items/itemtypes/zabbix_agent#logrt.count)|The count of matched lines in a monitored log file that is rotated. This item is not supported for Windows Event Log.<br>The `persistent_dir` parameter is not supported on Windows.|^|
|[modbus.get](/manual/config/items/itemtypes/zabbix_agent#modbus)|Reads Modbus data.|Modbus|
|[net.dns](/manual/config/items/itemtypes/zabbix_agent#net.dns)|Checks if the DNS service is up.<br>The `ip`, `timeout` and `count` parameters are ignored on Windows.|Network|
|[net.dns.record](/manual/config/items/itemtypes/zabbix_agent#net.dns.record)|Performs a DNS query.<br>The `ip`, `timeout` and `count` parameters are ignored on Windows.|^|
|[net.if.discovery](/manual/config/items/itemtypes/zabbix_agent#net.if.discovery)|The list of network interfaces.<br>Some Windows versions (for example, Server 2008) might require the latest updates installed to support non-ASCII characters in interface names.|^|
|[net.if.in](/manual/config/items/itemtypes/zabbix_agent#net.if.in)|The incoming traffic statistics on a network interface.<br>On Windows, the item gets values from 64-bit counters if available. 64-bit interface statistic counters were introduced in Windows Vista and Windows Server 2008. If 64-bit counters are not available, the agent uses 32-bit counters.<br>Multi-byte interface names on Windows are supported.<br>You may obtain network interface descriptions on Windows with net.if.discovery or net.if.list items.|^|
|[net.if.out](/manual/config/items/itemtypes/zabbix_agent#net.if.out)|The outgoing traffic statistics on a network interface.<br>On Windows, the item gets values from 64-bit counters if available. 64-bit interface statistic counters were introduced in Windows Vista and Windows Server 2008. If 64-bit counters are not available, the agent uses 32-bit counters.<br>Multi-byte interface names on Windows are supported.<br>You may obtain network interface descriptions on Windows with net.if.discovery or net.if.list items.|^|
|[net.if.total](/manual/config/items/itemtypes/zabbix_agent#net.if.total)|The sum of incoming and outgoing traffic statistics on a network interface.<br>On Windows, the item gets values from 64-bit counters if available. 64-bit interface statistic counters were introduced in Windows Vista and Windows Server 2008. If 64-bit counters are not available, the agent uses 32-bit counters.<br>You may obtain network interface descriptions on Windows with net.if.discovery or net.if.list items.|^|
|[net.tcp.listen](/manual/config/items/itemtypes/zabbix_agent#net.tcp.listen)|Checks if this TCP port is in LISTEN state.|^|
|[net.tcp.port](/manual/config/items/itemtypes/zabbix_agent#net.tcp.port)|Checks if it is possible to make a TCP connection to the specified port.|^|
|[net.tcp.service](/manual/config/items/itemtypes/zabbix_agent#net.tcp.service)|Checks if a service is running and accepting TCP connections.<br>Checking of LDAP and HTTPS on Windows is only supported by Zabbix agent 2.|^|
|[net.tcp.service.perf](/manual/config/items/itemtypes/zabbix_agent#net.tcp.service.perf)|Checks the performance of a TCP service.<br>Checking of LDAP and HTTPS on Windows is only supported by Zabbix agent 2.|^|
|[net.tcp.socket.count](/manual/config/items/itemtypes/zabbix_agent#net.tcp.socket.count)|Returns the number of TCP sockets that match parameters.<br>This item is supported on Linux by Zabbix agent, but on Windows it is supported only by [Zabbix agent 2](/manual/config/items/itemtypes/zabbix_agent/zabbix_agent2) on 64-bit Windows.|^|
|[net.udp.service](/manual/config/items/itemtypes/zabbix_agent#net.udp.service)|Checks if a service is running and responding to UDP requests.|^|
|[net.udp.service.perf](/manual/config/items/itemtypes/zabbix_agent#net.udp.service.perf)|Checks the performance of a UDP service.|^|
|[net.udp.socket.count](/manual/config/items/itemtypes/zabbix_agent#net.udp.socket.count)|Returns the number of UDP sockets that match parameters.<br>This item is supported on Linux by Zabbix agent, but on Windows it is supported only by [Zabbix agent 2](/manual/config/items/itemtypes/zabbix_agent/zabbix_agent2) on 64-bit Windows.|^|
|[proc.num](/manual/config/items/itemtypes/zabbix_agent#proc.num)|The number of processes.<br>On Windows, only the `name` and `user` parameters are supported.|Processes|
|[system.cpu.discovery](/manual/config/items/itemtypes/zabbix_agent#system.cpu.discovery)|The list of detected CPUs/CPU cores.|System|
|[system.cpu.load](/manual/config/items/itemtypes/zabbix_agent#system.cpu.load)|The CPU load.|^|
|[system.cpu.num](/manual/config/items/itemtypes/zabbix_agent#system.cpu.num)|The number of CPUs.|^|
|[system.cpu.util](/manual/config/items/itemtypes/zabbix_agent#system.cpu.util)|The CPU utilization percentage.<br>The value is acquired using the *Processor Time* performance counter. Note that since Windows 8 its Task Manager shows CPU utilization based on the *Processor Utility* performance counter, while in previous versions it was the *Processor Time* counter.<br>*system* is the only `type` parameter supported on Windows.|^|
|[system.hostname](/manual/config/items/itemtypes/zabbix_agent#system.hostname)|The system host name.<br>The value is acquired by either GetComputerName() (for **netbios**), GetComputerNameExA() (for **fqdn**), or gethostname() (for **host**) functions on Windows.<br>See also a [more detailed description](/manual/appendix/install/windows_agent#configuration).|^|
|[system.localtime](/manual/config/items/itemtypes/zabbix_agent#system.localtime)|The system time.|^|
|[system.run](/manual/config/items/itemtypes/zabbix_agent#system.run)|Run the specified command on the host.|^|
|[system.sw.arch](/manual/config/items/itemtypes/zabbix_agent#system.sw.arch)|The software architecture information.|^|
|[system.swap.size](/manual/config/items/itemtypes/zabbix_agent#system.swap.size)|The swap space size in bytes or in percentage from total.<br>The `pused` type parameter is supported on Linux by Zabbix agent, but on Windows it is supported only by [Zabbix agent 2](/manual/config/items/itemtypes/zabbix_agent/zabbix_agent2).<br>Note that this key might report incorrect swap space size/percentage on virtualized (VMware ESXi, VirtualBox) Windows platforms. In this case you may use the `perf_counter[\700(_Total)\702]` key to obtain correct swap space percentage.|^|
|[system.uname](/manual/config/items/itemtypes/zabbix_agent#system.uname)|Identification of the system.<br>On Windows the value for this item is obtained from Win32\_OperatingSystem and Win32\_Processor WMI classes. The OS name (including edition) might be translated to the user's display language. On some versions of Windows it contains trademark symbols and extra spaces.|^|
|[system.uptime](/manual/config/items/itemtypes/zabbix_agent#system.uptime)|The system uptime in seconds.|^|
|[vfs.dir.count](/manual/config/items/itemtypes/zabbix_agent#vfs.dir.count)|The directory entry count.<br>On Windows, directory symlinks are skipped and hard links are counted only once.|Virtual file systems|
|[vfs.dir.get](/manual/config/items/itemtypes/zabbix_agent#vfs.dir.get)|The directory entry list.<br>On Windows, directory symlinks are skipped and hard links are counted only once.|^|
|[vfs.dir.size](/manual/config/items/itemtypes/zabbix_agent#vfs.dir.size)|The directory size.<br>On Windows any symlink is skipped and hard links are taken into account only once.|^|
|[vfs.file.cksum](/manual/config/items/itemtypes/zabbix_agent#vfs.file.cksum)|The file checksum, calculated by the UNIX cksum algorithm.|^|
|[vfs.file.contents](/manual/config/items/itemtypes/zabbix_agent#vfs.file.contents)|Retrieving the contents of a file.|^|
|[vfs.file.exists](/manual/config/items/itemtypes/zabbix_agent#vfs.file.exists)|Checks if the file exists.<br>On Windows the double quotes have to be backslash '\\' escaped and the whole item key enclosed in double quotes when using the command line utility for calling zabbix\_get.exe or agent2.<br>Note that the item may turn unsupported on Windows if a directory is searched within a non-existing directory, e.g. `vfs.file.exists[C:\no\dir,dir]` (where 'no' does not exist).|^|
|[vfs.file.get](/manual/config/items/itemtypes/zabbix_agent#vfs.file.get)|Returns information about a file.<br>Supported file types on Windows: regular file, directory, symbolic link|^|
|[vfs.file.md5sum](/manual/config/items/itemtypes/zabbix_agent#vfs.file.md5sum)|The MD5 checksum of file.|^|
|[vfs.file.owner](/manual/config/items/itemtypes/zabbix_agent#vfs.file.owner)|Retrieves the owner of a file.|^|
|[vfs.file.regexp](/manual/config/items/itemtypes/zabbix_agent#vfs.file.regexp)|Retrieve a string in the file.|^|
|[vfs.file.regmatch](/manual/config/items/itemtypes/zabbix_agent#vfs.file.regmatch)|Find a string in the file.|^|
|[vfs.file.size](/manual/config/items/itemtypes/zabbix_agent#vfs.file.sizefilemode)|The file size.|^|
|[vfs.file.time](/manual/config/items/itemtypes/zabbix_agent#vfs.file.timefilemode)|The file time information.<br>On Windows XP `vfs.file.time[file,change]` may be equal to `vfs.file.time[file,access]`.|^|
|[vfs.fs.discovery](/manual/config/items/itemtypes/zabbix_agent#vfs.fs.discovery)|The list of mounted filesystems with their type and mount options.<br>The {#FSLABEL} macro is supported on Windows since Zabbix 6.0.|^|
|[vfs.fs.get](/manual/config/items/itemtypes/zabbix_agent#vfs.fs.get)|The list of mounted filesystems with their type, available disk space, inode statistics and mount options.<br>The {#FSLABEL} macro is supported on Windows since Zabbix 6.0.|^|
|[vfs.fs.size](/manual/config/items/itemtypes/zabbix_agent#vfs.fs.size)|The disk space in bytes or in percentage from total.|^|
|[vm.memory.size](/manual/config/items/itemtypes/zabbix_agent#vm.memory.size)|The memory size in bytes or in percentage from total.|Virtual memory|
|[web.page.get](/manual/config/items/itemtypes/zabbix_agent#web.page.get)|Get the content of a web page.|Web monitoring|
|[web.page.perf](/manual/config/items/itemtypes/zabbix_agent#web.page.perf)|The loading time of a full web page.|^|
|[web.page.regexp](/manual/config/items/itemtypes/zabbix_agent#web.page.regexp)|Find a string on the web page.|^|
|[agent.hostmetadata](/manual/config/items/itemtypes/zabbix_agent#agent.hostmetadata)|The agent host metadata.|Zabbix|
|[agent.hostname](/manual/config/items/itemtypes/zabbix_agent#agent.hostname)|The agent host name.|^|
|[agent.ping](/manual/config/items/itemtypes/zabbix_agent#agent.ping)|The agent availability check.|^|
|[agent.variant](/manual/config/items/itemtypes/zabbix_agent#agent.variant)|The variant of Zabbix agent (Zabbix agent or Zabbix agent 2).|^|
|[agent.version](/manual/config/items/itemtypes/zabbix_agent#agent.version)|The version of Zabbix agent.|^|
|[zabbix.stats](/manual/config/items/itemtypes/zabbix_agent#zabbix.stats)|Returns a set of Zabbix server or proxy internal metrics remotely.|^| 
|[zabbix.stats](/manual/config/items/itemtypes/zabbix_agent#zabbix.stats.two)|Returns the number of monitored items in the queue which are delayed on Zabbix server or proxy remotely.|^|

[comment]: # ({/9c68e4d4-52f3ae09})

[comment]: # ({72d570f9-2356bb2e})

### Windows-specific items

The table provides details on the item keys that are supported **only** by the Windows Zabbix agent.

Windows-specific items sometimes are an approximate counterpart of a similar 
agent item, for example `proc_info`, supported on Windows, roughly corresponds 
to the `proc.mem` item, not supported on Windows.

The item key is a link to full item key details.

|Item key|Description|Item group|
|--|-------|-|
|[eventlog](#eventlog)|The Windows event log monitoring.|Log monitoring|
|[net.if.list](#net.if.list)|The network interface list (includes interface type, status, IPv4 address, description).|Network|
|[perf_counter](#perf.counter)|The value of any Windows performance counter.|Performance counters|
|[perf_counter_en](#perf.counter.en)|The value of any Windows performance counter in English.|^|
|[perf_instance.discovery](#perf.instance.discovery)|The list of object instances of Windows performance counters.|^|
|[perf_instance_en.discovery](#perf.instance.en.discovery)|The list of object instances of Windows performance counters, discovered using the object names in English.|^|
|[proc_info](#proc.info)|Various information about specific process(es).|Processes|
|[registry.data](#registry.data)|Return data for the specified value name in the Windows Registry key.|Registry|
|[registry.get](#registry.get)|The list of Windows Registry values or keys located at given key.|^|
|[service.discovery](#service.discovery)|The list of Windows services.|Services|
|[service.info](#service.info)|Information about a service.|^|
|[services](#services)|The listing of services.|^|
|[vm.vmemory.size](#vm.vmemory.size)|The virtual memory size in bytes or in percentage from the total.|Virtual memory|
|[wmi.get](#wmi.get)|Execute a WMI query and return the first selected object.|WMI|
|[wmi.getall](#wmi.getall)|Execute a WMI query and return the whole response.|^|

[comment]: # ({/72d570f9-2356bb2e})

[comment]: # ({b1bc108a-3077d649})
### Item key details

Parameters without angle brackets are mandatory. Parameters marked with angle brackets **<** **>** are optional.

[comment]: # ({/b1bc108a-3077d649})

[comment]: # ({b78e9b7a-ea4d4b73})

##### eventlog[name,<regexp>,<severity>,<source>,<eventid>,<maxlines>,<mode>] {#eventlog}

<br>
The event log monitoring.<br>
Return value: *Log*.

Parameters:

-   **name** - the name of the event log;<br>
-   **regexp** - a regular [expression](/manual/regular_expressions#overview) describing the required pattern (case sensitive);<br>
-   **severity** - a regular expression describing severity (case insensitive). This parameter accepts a regular expression based on the following values: "Information", "Warning", "Error", "Critical", "Verbose" (running on Windows Vista or newer).<br>
-   **source** - a regular expression describing the source identifier (case insensitive);<br>
-   **eventid** - a regular expression describing the event identifier(s) (case sensitive);<br>
-   **maxlines** - the maximum number of new lines per second the agent will send to Zabbix server or proxy. This parameter overrides the value of 'MaxLinesPerSecond' in [zabbix\_agentd.win.conf](/manual/appendix/config/zabbix_agentd_win).<br>
-   **mode** - possible values: *all* (default) or *skip* - skip the processing of older data (affects only newly created items).

Comments:

-   The item must be configured as an [active check](/manual/appendix/items/activepassive#active_checks);
-   The agent is unable to send in events from the "Forwarded events" log;
-   Windows Eventing 6.0 is supported;
-   Selecting a non-Log [type of information](/manual/config/items/item#configuration) for this item will lead to the loss of local timestamp, as well as log severity and source information;
-   See also additional information on [log monitoring](/manual/config/items/itemtypes/log_items).

Examples:

    eventlog[Application]
    eventlog[Security,,"Failure Audit",,^(529|680)$]
    eventlog[System,,"Warning|Error"]
    eventlog[System,,,,^1$]
    eventlog[System,,,,@TWOSHORT] #here a custom regular expression named `TWOSHORT` is referenced (defined as a *Result is TRUE* type, the expression itself being `^1$|^70$`).
    

[comment]: # ({/b78e9b7a-ea4d4b73})

[comment]: # ({6ffc2a7f-2304e84f})

##### net.if.list {#net.if.list}

<br>
The network interface list (includes interface type, status, IPv4 address, description).<br>
Return value: *Text*.

Comments:

-   Multi-byte interface names supported;
-   Disabled interfaces are not listed;
-   Enabling/disabling some components may change their ordering in the Windows interface name;
-   Some Windows versions (for example, Server 2008) might require the latest updates installed to support non-ASCII characters in interface names.

[comment]: # ({/6ffc2a7f-2304e84f})

[comment]: # ({2bee28e0-9a990f49})

##### perf_counter[counter,<interval>] {#perf.counter}

<br>
The value of any Windows performance counter.<br>
Return value: *Integer*, *float*, *string* or *text* (depending on the request).

Parameters:

-   **counter** - the path to the counter;<br>
-   **interval** - the last N seconds for storing the average value. The `interval` must be between 1 and 900 seconds (included) and the default value is 1.

Comments:

-   `interval` is used for counters that require more than one sample (like CPU utilization), so the check returns an average value for last "interval" seconds every time;
-   Performance Monitor can be used to obtain the list of available counters. 
-   See also: [Windows performance counters](/manual/config/items/perfcounters).

[comment]: # ({/2bee28e0-9a990f49})

[comment]: # ({ac4ecc34-68dc3c35})

##### perf_counter_en[counter,<interval>] {#perf.counter.en}

<br>
The value of any Windows performance counter in English.<br>
Return value: *Integer*, *float*, *string* or *text* (depending on the request).

Parameters:

-   **counter** - the path to the counter in English;<br>
-   **interval** - the last N seconds for storing the average value. The `interval` must be between 1 and 900 seconds (included) and the default value is 1.

Comments:

-   `interval` is used for counters that require more than one sample (like CPU utilization), so the check returns an average value for last "interval" seconds every time;
-   This item is only supported on **Windows Server 2008/Vista** and above;
-   You can find the list of English strings by viewing the following registry key: `HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Perflib\009`.

[comment]: # ({/ac4ecc34-68dc3c35})

[comment]: # ({88b467ce-f4ce8a45})

##### perf_instance.discovery[object] {#perf.instance.discovery}

<br>
The list of object instances of Windows performance counters. Used for [low-level discovery](/manual/discovery/low_level_discovery/examples/windows_perf_instances).<br>
Return value: *JSON object*.

Parameter:

-   **object** - the object name (localized).

[comment]: # ({/88b467ce-f4ce8a45})

[comment]: # ({8ab292e7-817f7111})

##### perf_instance_en.discovery[object] {#perf.instance.en.discovery}

<br>
The list of object instances of Windows performance counters, discovered using the object names in English. Used for [low-level discovery](/manual/discovery/low_level_discovery/examples/windows_perf_instances).<br>
Return value: *JSON object*.

Parameter:

-   **object** - the object name (in English).

[comment]: # ({/8ab292e7-817f7111})

[comment]: # ({4982f581-36e50fdd})

##### proc_info[process,<attribute>,<type>] {#proc.info}

<br>
Various information about specific process(es).<br>
Return value: *Float*.

Parameters:

-   **process** - the process name;<br>
-   **attribute** - the requested process attribute;<br>
-   **type** - the representation type (meaningful when more than one process with the same name exists)

Comments:

-   The following `attributes` are supported:<br>*vmsize* (default) - size of process virtual memory in Kbytes<br>*wkset* - size of process working set (amount of physical memory used by process) in Kbytes<br>*pf* - number of page faults<br>*ktime* - process kernel time in milliseconds<br>*utime* - process user time in milliseconds<br>*io\_read\_b* - number of bytes read by process during I/O operations<br>*io\_read\_op* - number of read operation performed by process<br>*io\_write\_b* - number of bytes written by process during I/O operations<br>*io\_write\_op* - number of write operation performed by process<br>*io\_other\_b* - number of bytes transferred by process during operations other than read and write operations<br>*io\_other\_op* - number of I/O operations performed by process, other than read and write operations<br>*gdiobj* - number of GDI objects used by process<br>*userobj* - number of USER objects used by process;<br>
-   Valid `types` are:<br>*avg* (default) - average value for all processes named <process><br>*min* - minimum value among all processes named <process><br>*max* - maximum value among all processes named <process><br>*sum* - sum of values for all processes named <process>;
-   *io\_\**, *gdiobj* and *userobj* attributes are available only on Windows 2000 and later versions of Windows, not on Windows NT 4.0;
-   On a 64-bit system, a 64-bit Zabbix agent is required for this item to work correctly.<br>

Examples:

    proc_info[iexplore.exe,wkset,sum] #retrieve the amount of physical memory taken by all Internet Explorer processes
    proc_info[iexplore.exe,pf,avg] #retrieve the average number of page faults for Internet Explorer processes

[comment]: # ({/4982f581-36e50fdd})

[comment]: # ({b382dd98-ff3c15dc})

##### registry.data[key,<value name>] {#registry.data}

<br>
Return data for the specified value name in the Windows Registry key.<br>
Return value: *Integer*, *string* or *text* (depending on the value type)

Parameters:

-   **key** - the registry key including the root key; root abbreviations (e.g. HKLM) are allowed;
-   **value name** - the registry value name in the key (empty string "" by default). The default value is returned if the value name is not supplied.

Comments:

-   Supported root abbreviations:<br>HKCR - HKEY_CLASSES_ROOT<br>HKCC - HKEY_CURRENT_CONFIG<br>HKCU - HKEY_CURRENT_USER<br>HKCULS - HKEY_CURRENT_USER_LOCAL_SETTINGS<br>HKLM - HKEY_LOCAL_MACHINE<br>HKPD - HKEY_PERFORMANCE_DATA<br>HKPN - HKEY_PERFORMANCE_NLSTEXT<br>HKPT - HKEY_PERFORMANCE_TEXT<br>HKU - HKEY_USERS<br>
-   Keys with spaces must be double-quoted.

Examples:

    registry.data["HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\Windows Error Reporting"] #return the data of the default value of this key
    registry.data["HKLM\SOFTWARE\Microsoft\Windows\Windows Error Reporting","EnableZip"] #return the data of the value named "Enable Zip" in this key

[comment]: # ({/b382dd98-ff3c15dc})

[comment]: # ({dda8d63c-e1a87947})

##### registry.get[key,<mode>,<name regexp>] {#registry.get}

<br>
The list of Windows Registry values or keys located at given key.<br>
Return value: *JSON object*.

Parameters:

-   **key** - the registry key including the root key; root abbreviations (e.g. HKLM) are allowed (see comments for registry.data\[\] to see full list of abbreviations);<br>
-   **mode** - possible values:<br>*values* (default) or *keys*;<br>
-   **name regexp** - only discover values with names that match the regexp (default - discover all values). Allowed only with *values* as `mode`.

Keys with spaces must be double-quoted.

Examples:

    registry.get[HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall,values,"^DisplayName|DisplayVersion$"] #return the data of the values named "DisplayName" or "DisplayValue" in this key. The JSON will include details of the key, last subkey, value name, value type and value data.
    registry.get[HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall,values] #return the data of the all values in this key. The JSON will include details of the key, last subkey, value name, value type and value data.
    registry.get[HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall,keys] #return all subkeys of this key. The JSON will include details of the key and last subkey.

[comment]: # ({/dda8d63c-e1a87947})

[comment]: # ({11678b2b-e7b67f01})

##### service.discovery {#service.discovery}

<br>
The list of Windows services. Used for [low-level discovery](/manual/discovery/low_level_discovery/examples/windows_services).<br>
Return value: *JSON object*.

[comment]: # ({/11678b2b-e7b67f01})

[comment]: # ({8023471a-f01b9c46})

##### service.info[service,<param>] {#service.info}

<br>
Information about a service.<br>
Return value: *Integer* - with `param` as *state*, *startup*; *String* - with `param` as *displayname*, *path*, *user*; *Text* - with `param` as *description*<br>Specifically for *state*: 0 - running, 1 - paused, 2 - start pending, 3 - pause pending, 4 - continue pending, 5 - stop pending, 6 - stopped, 7 - unknown, 255 - no such service<br>Specifically for *startup*: 0 - automatic, 1 - automatic delayed, 2 - manual, 3 - disabled, 4 - unknown, 5 - automatic trigger start, 6 - automatic delayed trigger start, 7 - manual trigger start

Parameters:

-   **service** - a real service name or its display name as seen in the MMC Services snap-in;
-   **param** - *state* (default), *displayname*, *path*, *user*, *startup*, or *description*.

Comments:

-   Items like `service.info[service,state]` and `service.info[service]` will return the same information;
-   Only with `param` as *state* this item returns a value for non-existing services (255).

Examples:

    service.info[SNMPTRAP] - state of the SNMPTRAP service;
    service.info[SNMP Trap] - state of the same service, but with the display name specified;
    service.info[EventLog,startup] - the startup type of the EventLog service

[comment]: # ({/8023471a-f01b9c46})

[comment]: # ({7786edd8-2d4eec00})

##### services[<type>,<state>,<exclude>] {#services}

<br>
The listing of services.<br>
Return value: *0* - if empty; *Text* - the list of services separated by a newline.

Parameters:

-   **type** - *all* (default), *automatic*, *manual*, or *disabled*;
-   **state** - *all* (default), *stopped*, *started*, *start_pending*, *stop_pending*, *running*, *continue_pending*, *pause_pending*, or *paused*;
-   **exclude** - the services to exclude from the result. Excluded services should be listed in double quotes, separated by comma, without spaces.

Examples:

    services[,started] #returns the list of started services;
    services[automatic, stopped] #returns the list of stopped services that should be running;
    services[automatic, stopped, "service1,service2,service3"] #returns the list of stopped services that should be running, excluding services named "service1", "service2" and "service3"

[comment]: # ({/7786edd8-2d4eec00})

[comment]: # ({3087dd59-ef131dd0})

##### vm.vmemory.size[<type>] {#vm.vmemory.size}

<br>
The virtual memory size in bytes or in percentage from the total.<br>
Return value: *Integer* - for bytes; *float* - for percentage.

Parameter:

-   **type** - possible values: *available* (available virtual memory), *pavailable* (available virtual memory, in percent), *pused* (used virtual memory, in percent), *total* (total virtual memory, default), or *used* (used virtual memory)

Comments:

-   The monitoring of virtual memory statistics is based on:<br>
    -   Total virtual memory on Windows (total physical + page file size);<br>
    -   The maximum amount of memory Zabbix agent can commit;<br>
    -   The current committed memory limit for the system or Zabbix agent, whichever is smaller.

Example:

    vm.vmemory.size[pavailable] #return the available virtual memory, in percentage

[comment]: # ({/3087dd59-ef131dd0})

[comment]: # ({64ba6977-a80553ac})

##### wmi.get[<namespace>,<query>] {#wmi.get}

<br>
Execute a WMI query and return the first selected object.<br>
Return value: *Integer*, *float*, *string* or *text* (depending on the request).

Parameters:

-   **namespace** - the WMI namespace;<br>
-   **query** - the WMI query returning a single object.

WMI queries are performed with [WQL](https://en.wikipedia.org/wiki/WQL).

Example:

    wmi.get[root\cimv2,select status from Win32_DiskDrive where Name like '%PHYSICALDRIVE0%'] #returns the status of the first physical disk

[comment]: # ({/64ba6977-a80553ac})

[comment]: # ({18a1d63c-8c19f9d3})

##### wmi.getall[<namespace>,<query>] {#wmi.getall}

<br>
Execute a WMI query and return the whole response. Can be used for [low-level discovery](/manual/discovery/low_level_discovery/examples/wmi).<br>
Return value: *JSON object*

Parameters:

-   **namespace** - the WMI namespace;<br>
-   **query** - the WMI query.

Comments:

-   WMI queries are performed with [WQL](https://en.wikipedia.org/wiki/WQL).
-   JSONPath [preprocessing](/manual/config/items/item#item_value_preprocessing) can be used to point to more specific values in the returned JSON.

Example:

    wmi.getall[root\cimv2,select * from Win32_DiskDrive where Name like '%PHYSICALDRIVE%'] #returns status information of physical disks

[comment]: # ({/18a1d63c-8c19f9d3})

[comment]: # ({c2b4f623-c2b4f623})
### Monitoring Windows services

This tutorial provides step-by-step instructions for setting up the
monitoring of Windows services. It is assumed that Zabbix server and
agent are configured and operational.

[comment]: # ({/c2b4f623-c2b4f623})

[comment]: # ({9f748198-110cbb70})
##### Step 1

Get the service name.

You can get the service name by going to the MMC Services snap-in and bringing up
the properties of the service. In the *General* tab you should see a field
called "Service name". The value that follows is the name you will use
when setting up an item for monitoring.
For example, if you wanted to monitor the "workstation" service, then
your service might be: **lanmanworkstation**.

[comment]: # ({/9f748198-110cbb70})

[comment]: # ({48c8801c-d5dd9159})
##### Step 2

[Configure an item](/manual/config/items/item) for monitoring the
service.

The item `service.info[service,<param>]` retrieves information
about a particular service. Depending on the information you need,
specify the `param` option which accepts the following values:
*displayname*, *state*, *path*, *user*, *startup* or *description*. The
default value is *state* if `param` is not specified
(`service.info[service]`).

The type of return value depends on chosen `param`: integer for *state*
and *startup*; character string for *displayname*, *path* and *user*;
text for *description*.

Example:

-   *Key:* `service.info[lanmanworkstation]`
-   *Type of information:* Numeric (unsigned)

The item `service.info[lanmanworkstation]` will retrieve information about the state of the service as a numerical value.
To map a numerical value to a text representation in the frontend ("0" as "Running", "1" as "Paused", etc.), you can configure [value mapping](/manual/config/items/mapping#configuration) on the host on which the item is configured.
To do this, either [link the template](/manual/config/templates/linking#linking-a-template) *Windows services by Zabbix agent* or *Windows services by Zabbix agent active* to the host,
or configure on the host a new value map that is based on the *Windows service state* value map configured on the mentioned templates.

Note that both of the mentioned templates have a discovery rule configured that will discover services automatically.
If you do not want this, you can [disable the discovery rule](/manual/web_interface/frontend_sections/data_collection/hosts/discovery) on the host level once the template has been linked to the host.

[comment]: # ({/48c8801c-d5dd9159})

[comment]: # ({bee949c4-bee949c4})
### Discovery of Windows services

[Low-level discovery](/manual/discovery/low_level_discovery) provides a
way to automatically create items, triggers, and graphs for different
entities on a computer. Zabbix can automatically start monitoring
Windows services on your machine, without the need to know the exact
name of a service or create items for each service manually. A filter
can be used to generate real items, triggers, and graphs only for
services of interest.

[comment]: # ({/bee949c4-bee949c4})
