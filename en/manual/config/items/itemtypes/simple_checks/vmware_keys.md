[comment]: # ({5479dd79-5479dd79})
# VMware monitoring item keys

[comment]: # ({/5479dd79-5479dd79})

[comment]: # ({1fca54f3-98337d09})

List of VMware monitoring [item keys](/manual/vm_monitoring/vmware_keys) has been moved to [VMware monitoring](/manual/vm_monitoring) section. 

[comment]: # ({/1fca54f3-98337d09})
