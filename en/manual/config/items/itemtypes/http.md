[comment]: # ({eb22f00a-eb22f00a})
# 16 HTTP agent

[comment]: # ({/eb22f00a-eb22f00a})

[comment]: # ({6871ec1a-a236c83a})
#### Overview

This item type allows data polling using the HTTP/HTTPS protocol.
Trapping is also possible using Zabbix sender or Zabbix sender protocol.

HTTP item check is executed by Zabbix server. However, when hosts are
monitored by a Zabbix proxy, HTTP item checks are executed by the proxy.

HTTP item checks do not require any agent running on a host being
monitored.

HTTP agent supports both HTTP and HTTPS. Zabbix will optionally follow
redirects (see the *Follow redirects* option below). Maximum number of
redirects is hard-coded to 10 (using cURL option CURLOPT\_MAXREDIRS).

::: noteimportant
Zabbix server/proxy must be initially configured
with cURL (libcurl) support.
:::

[comment]: # ({/6871ec1a-a236c83a})

[comment]: # ({c382d2ac-11697b04})
#### Configuration

To configure an HTTP item:

-   Go to: *Data collection* → *Hosts*
-   Click on *Items* in the row of the host
-   Click on *Create item*
-   Enter parameters of the item in the form

![](../../../../../assets/en/manual/config/items/itemtypes/http_item.png)

All mandatory input fields are marked with a red asterisk.

The fields that require specific information for HTTP items are:

|Parameter|Description|
|--|--------|
|*Type*|Select **HTTP agent** here.|
|*Key*|Enter a unique item key.|
|*URL*|URL to connect to and retrieve data. For example:<br>https://www.example.com<br>http://www.example.com/download<br>Domain names can be specified in Unicode characters. They are automatically punycode-converted to ASCII when executing the HTTP check.<br>The *Parse* button can be used to separate optional query fields (like ?name=Admin&password=mypassword) from the URL, moving the attributes and values into *Query fields* for automatic URL-encoding.<br>Limited to 2048 characters.<br>Supported macros: {HOST.IP}, {HOST.CONN}, {HOST.DNS}, {HOST.HOST}, {HOST.NAME}, {ITEM.ID}, {ITEM.KEY}, {ITEM.KEY.ORIG}, user macros, low-level discovery macros.<br>This sets the [CURLOPT\_URL](https://curl.haxx.se/libcurl/c/CURLOPT_URL.html) cURL option.|
|*Query fields*|Variables for the URL (see above).<br>Specified as attribute and value pairs.<br>Values are URL-encoded automatically. Values from macros are resolved and then URL-encoded automatically.<br>Supported macros: {HOST.IP}, {HOST.CONN}, {HOST.DNS}, {HOST.HOST}, {HOST.NAME}, {ITEM.ID}, {ITEM.KEY}, {ITEM.KEY.ORIG}, user macros, low-level discovery macros.<br>This sets the [CURLOPT\_URL](https://curl.haxx.se/libcurl/c/CURLOPT_URL.html) cURL option.|
|*Request type*|Select request method type: *GET*, *POST*, *PUT* or *HEAD*|
|*Timeout*|Zabbix will not spend more than the set amount of time on processing the URL (1-60 seconds). Actually this parameter defines the maximum time for making a connection to the URL and maximum time for performing an HTTP request. Therefore, Zabbix will not spend more than 2 x Timeout seconds on one check.<br>Time suffixes are supported, e.g. 30s, 1m.<br>Supported macros: user macros, low-level discovery macros.<br>This sets the [CURLOPT\_TIMEOUT](https://curl.haxx.se/libcurl/c/CURLOPT_TIMEOUT.html) cURL option.|
|*Request body type*|Select the request body type:<br>**Raw data** - custom HTTP request body, macros are substituted but no encoding is performed<br>**JSON data** - HTTP request body in JSON format. Macros can be used as string, number, true and false; macros used as strings must be enclosed in double quotes. Values from macros are resolved and then escaped automatically. If "Content-Type" is not specified in headers then it will default to "Content-Type: application/json"<br>**XML data** - HTTP request body in XML format. Macros can be used as a text node, attribute or CDATA section. Values from macros are resolved and then escaped automatically in a text node and attribute. If "Content-Type" is not specified in headers then it will default to "Content-Type: application/xml"<br>*Note* that selecting *XML data* requires libxml2.|
|*Request body*|Enter the request body.<br>Supported macros: {HOST.IP}, {HOST.CONN}, {HOST.DNS}, {HOST.HOST}, {HOST.NAME}, {ITEM.ID}, {ITEM.KEY}, {ITEM.KEY.ORIG}, user macros, low-level discovery macros.|
|*Headers*|Custom HTTP headers that will be sent when performing a request.<br>Specified as attribute and value pairs.<br>Supported macros: {HOST.IP}, {HOST.CONN}, {HOST.DNS}, {HOST.HOST}, {HOST.NAME}, {ITEM.ID}, {ITEM.KEY}, {ITEM.KEY.ORIG}, user macros, low-level discovery macros.<br>This sets the [CURLOPT\_HTTPHEADER](https://curl.haxx.se/libcurl/c/CURLOPT_HTTPHEADER.html) cURL option.|
|*Required status codes*|List of expected HTTP status codes. If Zabbix gets a code which is not in the list, the item will become unsupported. If empty, no check is performed.<br>For example: 200,201,210-299<br>Supported macros in the list: user macros, low-level discovery macros.<br>This uses the [CURLINFO\_RESPONSE\_CODE](https://curl.haxx.se/libcurl/c/CURLINFO_RESPONSE_CODE.html) cURL option.|
|*Follow redirects*|Mark the checkbox to follow HTTP redirects.<br>This sets the [CURLOPT\_FOLLOWLOCATION](https://curl.haxx.se/libcurl/c/CURLOPT_FOLLOWLOCATION.html) cURL option.|
|*Retrieve mode*|Select the part of response that must be retrieved:<br>**Body** - body only<br>**Headers** - headers only<br>**Body and headers** - body and headers|
|*Convert to JSON*|Headers are saved as attribute and value pairs under the "header" key.<br>If 'Content-Type: application/json' is encountered then body is saved as an object, otherwise it is stored as string, for example:<br>![](../../../../../assets/en/manual/config/items/itemtypes/http_conv_json.png)|
|*HTTP proxy*|You can specify an HTTP proxy to use, using the format `[protocol://][username[:password]@]proxy.example.com[:port]`.<br>The optional `protocol://` prefix may be used to specify alternative proxy protocols (e.g. https, socks4, socks5; see [documentation](https://curl.haxx.se/libcurl/c/CURLOPT_PROXY.html); the protocol prefix support was added in cURL 7.21.7). With no protocol specified, the proxy will be treated as an HTTP proxy. If you specify the wrong protocol, the connection will fail and the item will become unsupported.<br>By default, 1080 port will be used.<br>If specified, the proxy will overwrite proxy related environment variables like http\_proxy, HTTPS\_PROXY. If not specified, the proxy will not overwrite proxy-related environment variables. The entered value is passed on "as is", no sanity checking takes place.<br>*Note* that only simple authentication is supported with HTTP proxy.<br>Supported macros: {HOST.IP}, {HOST.CONN}, {HOST.DNS}, {HOST.HOST}, {HOST.NAME}, {ITEM.ID}, {ITEM.KEY}, {ITEM.KEY.ORIG}, user macros, low-level discovery macros.<br>This sets the [CURLOPT\_PROXY](https://curl.haxx.se/libcurl/c/CURLOPT_PROXY.html) cURL option.|
|*HTTP authentication*|Select the authentication option:<br>**None** - no authentication used;<br>**Basic** - basic authentication is used;<br>**NTLM** - NTLM ([Windows NT LAN Manager](http://en.wikipedia.org/wiki/NTLM)) authentication is used;<br>**Kerberos** - Kerberos authentication is used (see also: [Configuring Kerberos with Zabbix](/manual/appendix/items/kerberos));<br>**Digest** - Digest authentication is used.<br>This sets the [CURLOPT\_HTTPAUTH](https://curl.haxx.se/libcurl/c/CURLOPT_HTTPAUTH.html) cURL option.|
|*Username*|Enter the user name (up to 255 characters).<br>This field is available if *HTTP authentication* is set to Basic, NTLM, Kerberos, or Digest. User macros and low-level discovery macros are supported. |
|*Password*|Enter the user password (up to 255 characters).<br>This field is available if *HTTP authentication* is set to Basic, NTLM, Kerberos, or Digest. User macros and low-level discovery macros are supported. |
|*SSL verify peer*|Mark the checkbox to verify the SSL certificate of the web server. The server certificate will be automatically taken from system-wide certificate authority (CA) location. You can override the location of CA files using Zabbix server or proxy configuration parameter SSLCALocation.<br>This sets the [CURLOPT\_SSL\_VERIFYPEER](http://curl.haxx.se/libcurl/c/CURLOPT_SSL_VERIFYPEER.html) cURL option.|
|*SSL verify host*|Mark the checkbox to verify that the Common Name field or the Subject Alternate Name field of the web server certificate matches.<br>This sets the [CURLOPT\_SSL\_VERIFYHOST](http://curl.haxx.se/libcurl/c/CURLOPT_SSL_VERIFYHOST.html) cURL option.|
|*SSL certificate file*|Name of the SSL certificate file used for client authentication. The certificate file must be in PEM^1^ format. If the certificate file contains also the private key, leave the SSL key file field empty. If the key is encrypted, specify the password in SSL key password field. The directory containing this file is specified by Zabbix server or proxy configuration parameter SSLCertLocation.<br>Supported macros: {HOST.IP}, {HOST.CONN}, {HOST.DNS}, {HOST.HOST}, {HOST.NAME}, {ITEM.ID}, {ITEM.KEY}, {ITEM.KEY.ORIG}, user macros, low-level discovery macros.<br>This sets the [CURLOPT\_SSLCERT](http://curl.haxx.se/libcurl/c/CURLOPT_SSLCERT.html) cURL option.|
|*SSL key file*|Name of the SSL private key file used for client authentication. The private key file must be in PEM^1^ format. The directory containing this file is specified by Zabbix server or proxy configuration parameter SSLKeyLocation.<br>Supported macros: {HOST.IP}, {HOST.CONN}, {HOST.DNS}, {HOST.HOST}, {HOST.NAME}, {ITEM.ID}, {ITEM.KEY}, {ITEM.KEY.ORIG}, user macros, low-level discovery macros.<br>This sets the [CURLOPT\_SSLKEY](http://curl.haxx.se/libcurl/c/CURLOPT_SSLKEY.html) cURL option.|
|*SSL key password*|SSL private key file password.<br>Supported macros: user macros, low-level discovery macros.<br>This sets the [CURLOPT\_KEYPASSWD](http://curl.haxx.se/libcurl/c/CURLOPT_KEYPASSWD.html) cURL option.|
|*Enable trapping*|With this checkbox marked, the item will also function as [trapper item](/manual/config/items/itemtypes/trapper) and will accept data sent to this item by Zabbix sender or using Zabbix sender protocol.|
|*Allowed hosts*|Visible only if *Enable trapping* checkbox is marked.<br>List of comma delimited IP addresses, optionally in CIDR notation, or DNS names.<br>If specified, incoming connections will be accepted only from the hosts listed here.<br>If IPv6 support is enabled then '127.0.0.1', '::127.0.0.1', '::ffff:127.0.0.1' are treated equally and '::/0' will allow any IPv4 or IPv6 address.<br>'0.0.0.0/0' can be used to allow any IPv4 address.<br>Note, that "IPv4-compatible IPv6 addresses" (0000::/96 prefix) are supported but deprecated by [RFC4291](https://tools.ietf.org/html/rfc4291#section-2.5.5).<br>Example: 127.0.0.1, 192.168.1.0/24, 192.168.3.1-255, 192.168.1-10.1-255, ::1,2001:db8::/32, mysqlserver1, zabbix.example.com, {HOST.HOST}<br>Spaces and [user macros](/manual/config/macros/user_macros) are allowed in this field.<br>Host macros: {HOST.HOST}, {HOST.NAME}, {HOST.IP}, {HOST.DNS}, {HOST.CONN} are allowed in this field.|

::: notetip
If the *HTTP proxy* field is left empty, another way for
using an HTTP proxy is to set proxy-related environment variables.

For HTTP - set the `http_proxy` environment variable for the Zabbix
server user. For example:\
`http_proxy=http://proxy_ip:proxy_port`.

For HTTPS - set the `HTTPS_PROXY` environment variable. For example:\
`HTTPS_PROXY=http://proxy_ip:proxy_port`. More details are available by
running a shell command: *\# man curl*.
:::

::: noteimportant
 \[1\] Zabbix supports certificate and private key
files in PEM format only. In case you have your certificate and private
key data in PKCS \#12 format file (usually with extension \*.p12 or
\*.pfx) you may generate the PEM file from it using the following
commands:

    openssl pkcs12 -in ssl-cert.p12 -clcerts -nokeys -out ssl-cert.pem
    openssl pkcs12 -in ssl-cert.p12 -nocerts -nodes  -out ssl-cert.key


:::

[comment]: # ({/c382d2ac-11697b04})

[comment]: # ({4c860844-4c860844})
#### Examples

[comment]: # ({/4c860844-4c860844})

[comment]: # ({67d717da-67d717da})
##### Example 1

Send simple GET requests to retrieve data from services such as
Elasticsearch:

-   Create a GET item with URL: `localhost:9200/?pretty`
-   Notice the response:

```{=html}
<!-- -->
```
    {
      "name" : "YQ2VAY-",
      "cluster_name" : "elasticsearch",
      "cluster_uuid" : "kH4CYqh5QfqgeTsjh2F9zg",
      "version" : {
        "number" : "6.1.3",
        "build_hash" : "af51318",
        "build_date" : "2018-01-26T18:22:55.523Z",
        "build_snapshot" : false,
        "lucene_version" : "7.1.0",
        "minimum_wire_compatibility_version" : "5.6.0",
        "minimum_index_compatibility_version" : "5.0.0"
      },
      "tagline" : "You know, for search"
    }

-   Now extract the version number using a JSONPath preprocessing step:
    `$.version.number`

[comment]: # ({/67d717da-67d717da})

[comment]: # ({712fb81c-712fb81c})
##### Example 2

Send simple POST requests to retrieve data from services such as
Elasticsearch:

-   Create a POST item with URL:
    `http://localhost:9200/str/values/_search?scroll=10s`
-   Configure the following POST body to obtain the processor load (1
    min average per core)

```{=html}
<!-- -->
```
    {
        "query": {
            "bool": {
                "must": [{
                    "match": {
                        "itemid": 28275
                    }
                }],
                "filter": [{
                    "range": {
                        "clock": {
                            "gt": 1517565836,
                            "lte": 1517566137
                        }
                    }
                }]
            }
        }
    }

-   Received:

```{=html}
<!-- -->
```
    {
        "_scroll_id": "DnF1ZXJ5VGhlbkZldGNoBQAAAAAAAAAkFllRMlZBWS1UU1pxTmdEeGVwQjRBTFEAAAAAAAAAJRZZUTJWQVktVFNacU5nRHhlcEI0QUxRAAAAAAAAACYWWVEyVkFZLVRTWnFOZ0R4ZXBCNEFMUQAAAAAAAAAnFllRMlZBWS1UU1pxTmdEeGVwQjRBTFEAAAAAAAAAKBZZUTJWQVktVFNacU5nRHhlcEI0QUxR",
        "took": 18,
        "timed_out": false,
        "_shards": {
            "total": 5,
            "successful": 5,
            "skipped": 0,
            "failed": 0
        },
        "hits": {
            "total": 1,
            "max_score": 1.0,
            "hits": [{
                "_index": "dbl",
                "_type": "values",
                "_id": "dqX9VWEBV6sEKSMyk6sw",
                "_score": 1.0,
                "_source": {
                    "itemid": 28275,
                    "value": "0.138750",
                    "clock": 1517566136,
                    "ns": 25388713,
                    "ttl": 604800
                }
            }]
        }
    }

-   Now use a JSONPath preprocessing step to get the item value:
    `$.hits.hits[0]._source.value`

[comment]: # ({/712fb81c-712fb81c})

[comment]: # ({f480040f-f480040f})
##### Example 3

Checking if Zabbix API is alive, using
[apiinfo.version](/manual/api/reference/apiinfo/version).

-   Item configuration:

![](../../../../../assets/en/manual/config/items/itemtypes/example3_a.png)

Note the use of the POST method with JSON data, setting request headers
and asking to return headers only:

-   Item value preprocessing with regular expression to get HTTP code:

![](../../../../../assets/en/manual/config/items/itemtypes/example3_b.png)

-   Checking the result in *Latest data*:

![](../../../../../assets/en/manual/config/items/itemtypes/example3_c.png){width="600"}

[comment]: # ({/f480040f-f480040f})

[comment]: # ({466eee2b-466eee2b})
##### Example 4

Retrieving weather information by connecting to the Openweathermap
public service.

-   Configure a master item for bulk data collection in a single JSON:

![](../../../../../assets/en/manual/config/items/itemtypes/example4_a.png)

Note the usage of macros in query fields. Refer to the [Openweathermap
API](https://openweathermap.org/current) for how to fill them.

Sample JSON returned in response to HTTP agent:

``` {.json}
{
    "body": {
        "coord": {
            "lon": 40.01,
            "lat": 56.11
        },
        "weather": [{
            "id": 801,
            "main": "Clouds",
            "description": "few clouds",
            "icon": "02n"
        }],
        "base": "stations",
        "main": {
            "temp": 15.14,
            "pressure": 1012.6,
            "humidity": 66,
            "temp_min": 15.14,
            "temp_max": 15.14,
            "sea_level": 1030.91,
            "grnd_level": 1012.6
        },
        "wind": {
            "speed": 1.86,
            "deg": 246.001
        },
        "clouds": {
            "all": 20
        },
        "dt": 1526509427,
        "sys": {
            "message": 0.0035,
            "country": "RU",
            "sunrise": 1526432608,
            "sunset": 1526491828
        },
        "id": 487837,
        "name": "Stavrovo",
        "cod": 200
    }
}
```

The next task is to configure dependent items that extract data from the
JSON.

-   Configure a sample dependent item for humidity:

![](../../../../../assets/en/manual/config/items/itemtypes/example4_b.png)

Other weather metrics such as 'Temperature' are added in the same
manner.

-   Sample dependent item value preprocessing with JSONPath:

![](../../../../../assets/en/manual/config/items/itemtypes/example4_c.png)

-   Check the result of weather data in *Latest data*:

![](../../../../../assets/en/manual/config/items/itemtypes/example4_d.png){width="600"}

[comment]: # ({/466eee2b-466eee2b})

[comment]: # ({44596b14-44596b14})
##### Example 5

Connecting to Nginx status page and getting its metrics in bulk.

-   Configure Nginx following the [official
    guide](https://nginx.ru/en/docs/http/ngx_http_stub_status_module.html).

```{=html}
<!-- -->
```
-   Configure a master item for bulk data collection:

![](../../../../../assets/en/manual/config/items/itemtypes/example5_a.png)

Sample Nginx stub status output:

    Active connections: 1 Active connections:
    server accepts handled requests
     52 52 52 
    Reading: 0 Writing: 1 Waiting: 0

The next task is to configure dependent items that extract data.

-   Configure a sample dependent item for requests per second:

![](../../../../../assets/en/manual/config/items/itemtypes/example5_b.png)

-   Sample dependent item value preprocessing with regular expression
    `server accepts handled requests\s+([0-9]+) ([0-9]+) ([0-9]+)`:

![](../../../../../assets/en/manual/config/items/itemtypes/example5_c.png){width="600"}

-   Check the complete result from stub module in *Latest data*:

![](../../../../../assets/en/manual/config/items/itemtypes/example5_d.png){width="600"}

[comment]: # ({/44596b14-44596b14})
