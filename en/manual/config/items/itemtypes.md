[comment]: # ({06812ee1-06812ee1})
# 3 Item types

[comment]: # ({/06812ee1-06812ee1})

[comment]: # ({e39a5d1e-f03e2e9a})
#### Overview

Item types cover various methods of acquiring data from your system.
Each item type comes with its own set of supported item keys and
required parameters.

The following items types are currently offered by Zabbix:

-   [Zabbix agent checks](/manual/config/items/itemtypes/zabbix_agent)
-   [SNMP agent checks](/manual/config/items/itemtypes/snmp)
-   [SNMP traps](/manual/config/items/itemtypes/snmptrap)
-   [IPMI checks](/manual/config/items/itemtypes/ipmi)
-   [Simple checks](/manual/config/items/itemtypes/simple_checks)
    -   [VMware
        monitoring](/manual/vm_monitoring/vmware_keys)
-   [Log file monitoring](/manual/config/items/itemtypes/log_items)
-   [Calculated items](/manual/config/items/itemtypes/calculated)
    -   [Aggregate
        calculations](/manual/config/items/itemtypes/calculated/aggregate)
-   [Zabbix internal checks](/manual/config/items/itemtypes/internal)
-   [SSH checks](/manual/config/items/itemtypes/ssh_checks)
-   [Telnet checks](/manual/config/items/itemtypes/telnet_checks)
-   [External checks](/manual/config/items/itemtypes/external)
-   [Trapper items](/manual/config/items/itemtypes/trapper)
-   [JMX monitoring](/manual/config/items/itemtypes/jmx_monitoring)
-   [ODBC checks](/manual/config/items/itemtypes/odbc_checks)
-   [Dependent items](/manual/config/items/itemtypes/dependent_items)
-   [HTTP checks](/manual/config/items/itemtypes/http)
-   [Prometheus checks](/manual/config/items/itemtypes/prometheus)
-   [Script items](/manual/config/items/itemtypes/script)

Details for all item types are included in the subpages of this section.
Even though item types offer a lot of options for data gathering,
there are further options through [user parameters](/manual/config/items/userparameters)
or [loadable modules](/manual/extensions/loadablemodules).

Some checks are performed by Zabbix server alone (as agent-less
monitoring) while others require Zabbix agent or even Zabbix Java
gateway (with JMX monitoring).

::: noteimportant
If a particular item type requires a particular
interface (like an IPMI check needs an IPMI interface on the host) that
interface must exist in the host definition.
:::

Multiple interfaces can be set in the host definition: Zabbix agent,
SNMP agent, JMX and IPMI. If an item can use more than one interface, it
will search the available host interfaces (in the order:
Agent→SNMP→JMX→IPMI) for the first appropriate one to be linked with.

All items that return text (character, log, text types of information)
can return whitespace only as well (where applicable) setting the return
value to an empty string (supported since 2.0).

[comment]: # ({/e39a5d1e-f03e2e9a})
