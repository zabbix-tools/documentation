[comment]: # ({50342b23-50342b23})
# 1 Creating an item

[comment]: # ({/50342b23-50342b23})

[comment]: # ({9fd6079b-d3a04061})
#### Overview

To create an item in Zabbix frontend, do the following:

-   Go to: *Data collection* → *Hosts*
-   Click on *Items* in the row of the host
-   Click on *Create item* in the upper right corner of the screen
-   Enter parameters of the item in the form

You can also create an item by opening an existing one, pressing the
*Clone* button and then saving under a different name.


[comment]: # ({/9fd6079b-d3a04061})

[comment]: # ({9d8785c0-d0a0a311})
#### Configuration

The **Item** tab contains general item attributes.

![](../../../../assets/en/manual/config/items/item.png)

All mandatory input fields are marked with a red asterisk.

|Parameter|Description|
|--|--------|
|*Name*|Item name.|
|*Type*|Item type. See individual [item type](/manual/config/items/itemtypes) sections.|
|*Key*|Item key (up to 2048 characters).<br>The supported [item keys](/manual/config/items/itemtypes) can be found in individual item type sections.<br>The key must be unique within a single host.<br>If key type is 'Zabbix agent', 'Zabbix agent (active)' or 'Simple check', the key value must be supported by Zabbix agent or Zabbix server.<br>See also: the correct [key format](/manual/config/items/item/key).|
|*Type of information*|Type of data as stored in the database after performing conversions, if any.<br>**Numeric (unsigned)** - 64-bit unsigned integer<br>**Numeric (float)** - 64-bit floating point number<br>This type will allow precision of approximately 15 digits and range from approximately -1.79E+308 to 1.79E+308 (with exception of [PostgreSQL 11 and earlier versions](/manual/installation/known_issues#numeric-float-data-type-range-with-postgresql-11-and-earlier)).<br>Receiving values in scientific notation is also supported. E.g. 1.23E+7, 1e308, 1.1E-4.<br>**Character** - short text data<br>**Log** - long text data with optional log related properties (timestamp, source, severity, logeventid)<br>**Text** - long text data. See also [text data limits](#text-data-limits).<br>**Binary** - binary number (supported for dependent items only). A binary number will be resolved to a static "binary value" string in *Latest data*; {ITEM.VALUE}, {ITEM.LASTVALUE} and expression macros will resolve to UNKNOWN.<br>For item keys that return data only in one specific format, matching type of information is selected automatically.|
|*Host interface*|Select the host interface. This field is available when editing an item on the host level.|
|*Units*|If a unit symbol is set, Zabbix will add postprocessing to the received value and display it with the set unit postfix.<br>By default, if the raw value exceeds 1000, it is divided by 1000 and displayed accordingly. For example, if you set *bps* and receive a value of 881764, it will be displayed as 881.76 Kbps.<br>The [JEDEC](https://en.wikipedia.org/wiki/JEDEC_memory_standards) memory standard is used for processing **B** (byte), **Bps** (bytes per second) units, which are divided by 1024. Thus, if units are set to **B** or **Bps** Zabbix will display:<br>1 as 1B/1Bps<br>1024 as 1KB/1KBps<br>1536 as 1.5KB/1.5KBps<br>Special processing is used if the following time-related units are used:<br>**unixtime** - translated to "yyyy.mm.dd hh:mm:ss". To translate correctly, the received value must be a *Numeric (unsigned)* type of information.<br>**uptime** - translated to "hh:mm:ss" or "N days, hh:mm:ss"<br>For example, if you receive the value as 881764 (seconds), it will be displayed as "10 days, 04:56:04"<br>**s** - translated to "yyy mmm ddd hhh mmm sss ms"; parameter is treated as number of seconds.<br>For example, if you receive the value as 881764 (seconds), it will be displayed as "10d 4h 56m"<br>Only 3 upper major units are shown, like "1m 15d 5h" or "2h 4m 46s". If there are no days to display, only two levels are displayed - "1m 5h" (no minutes, seconds or milliseconds are shown). Will be translated to "< 1 ms" if the value is less than 0.001.<br>*Note* that if a unit is prefixed with `!`, then no unit prefixes/processing is applied to item values. See [unit conversion](#unit_conversion).|
|*Update interval*|Retrieve a new value for this item every N seconds. Maximum allowed update interval is 86400 seconds (1 day).<br>[Time suffixes](/manual/appendix/suffixes) are supported, e.g., 30s, 1m, 2h, 1d.<br>[User macros](/manual/config/macros/user_macros) are supported.<br>A single macro has to fill the whole field. Multiple macros in a field or macros mixed with text are not supported.<br>*Note*: The update interval can only be set to '0' if custom intervals exist with a non-zero value. If set to '0', and a custom interval (flexible or scheduled) exists with a non-zero value, the item will be polled during the custom interval duration.<br>*Note* that the first item poll after the item became active or after update interval change might occur earlier than the configured value. <br> New items will be checked within 60 seconds of their creation, unless they have Scheduling or Flexible update interval and the *Update interval* is set to 0.  <br>An existing passive item can be polled for value immediately by pushing the *Execute now* [button](#form_buttons).|
|*Custom intervals*|You can create custom rules for checking the item:<br>**Flexible** - create an exception to the *Update interval* (interval with different frequency)<br>**Scheduling** - create a custom polling schedule.<br>For detailed information see [Custom intervals](/manual/config/items/item/custom_intervals).<br>[Time suffixes](/manual/appendix/suffixes) are supported in the *Interval* field, e.g., 30s, 1m, 2h, 1d.<br>[User macros](/manual/config/macros/user_macros) are supported.<br>A single macro has to fill the whole field. Multiple macros in a field or macros mixed with text are not supported.<br>Scheduling is supported since Zabbix 3.0.0.<br>*Note*: custom intervals for active checks are supported by Zabbix agent 2 only. |
|*History storage period*|Select either:<br>**Do not keep history** - item history is not stored. Useful for master items if only dependent items need to keep history.<br>This setting cannot be overridden by global housekeeper [settings](/manual/web_interface/frontend_sections/administration/housekeeping).<br>**Storage period** - specify the duration of keeping detailed history in the database (1 hour to 25 years). Older data will be removed by the housekeeper. Stored in seconds.<br>[Time suffixes](/manual/appendix/suffixes) are supported, e.g., 2h, 1d. [User macros](/manual/config/macros/user_macros) are supported.<br>The *Storage period* value can be overridden globally in *Administration → [Housekeeping](/manual/web_interface/frontend_sections/administration/housekeeping)*.<br>If a global overriding setting exists, a green ![](../../../../assets/en/manual/config/info.png) info icon is displayed. If you position your mouse on it, a warning message is displayed, e.g., *Overridden by global housekeeper settings (1d)*.<br>It is recommended to keep the recorded values for the smallest possible time to reduce the size of value history in the database. Instead of keeping a long history of values, you can keep longer data of trends.<br>See also [History and trends](/manual/config/items/history_and_trends).|
|*Trend storage period*|Select either:<br>**Do not keep trends** - trends are not stored.<br>This setting cannot be overridden by global housekeeper [settings](/manual/web_interface/frontend_sections/administration/housekeeping).<br>**Storage period** - specify the duration of keeping aggregated (hourly min, max, avg, count) history in the database (1 day to 25 years). Older data will be removed by the housekeeper. Stored in seconds.<br>[Time suffixes](/manual/appendix/suffixes) are supported, e.g., 24h, 1d. [User macros](/manual/config/macros/user_macros) are supported.<br>The *Storage period* value can be overridden globally in *Administration → [Housekeeping](/manual/web_interface/frontend_sections/administration/housekeeping)*.<br>If a global overriding setting exists, a green ![](../../../../assets/en/manual/config/info.png) info icon is displayed. If you position your mouse on it, a warning message is displayed, e.g., *Overridden by global housekeeper settings (7d)*.<br>*Note:* Keeping trends is not available for non-numeric data - character, log and text.<br>See also [History and trends](/manual/config/items/history_and_trends).|
|*Value mapping*|Apply value mapping to this item. [Value mapping](/manual/config/items/mapping) does not change received values, it is for displaying data only.<br>It works with *Numeric(unsigned)*, *Numeric(float)* and *Character* items.<br>For example, "Windows service states".|
|*Log time format*|Available for items of type **Log** only. Supported placeholders:<br>\* **y**: *Year (1970-2038)*<br>\* **M**: *Month (01-12)*<br>\* **d**: *Day (01-31)*<br>\* **h**: *Hour (00-23)*<br>\* **m**: *Minute (00-59)*<br>\* **s**: *Second (00-59)*<br>If left blank, the timestamp will not be parsed.<br>For example, consider the following line from the Zabbix agent log file:<br>" 23480:20100328:154718.045 Zabbix agent started. Zabbix 1.8.2 (revision 11211)."<br>It begins with six character positions for PID, followed by date, time, and the rest of the line.<br>Log time format for this line would be "pppppp:yyyyMMdd:hhmmss".<br>Note that "p" and ":" chars are just placeholders and can be anything but "yMdhms".|
|*Populates host inventory field*|You can select a host inventory field that the value of item will populate. This will work if automatic [inventory](/manual/config/hosts/inventory) population is enabled for the host.<br>This field is not available if *Type of information* is set to 'Log'.|
|*Description*|Enter an item description. [User macros](/manual/config/macros/user_macros) are supported.|
|*Enabled*|Mark the checkbox to enable the item so it will be processed.|
|*Latest data*|Click on the link to view the latest data for the item.<br>This link is only available when editing an already existing item.|

::: noteclassic
Item type specific fields are described on [corresponding
pages](/manual/config/items/itemtypes).
::: 

::: noteclassic
When editing an existing
[template](/manual/config/templates) level item on a host level, a
number of fields are read-only. You can use the link in the form header
and go to the template level and edit them there, keeping in mind that
the changes on a template level will change the item for all hosts that
the template is linked to.
:::

The **Tags** tab allows to define item-level
[tags](/manual/config/tagging).

![](../../../../assets/en/manual/config/items/item_b.png)

[comment]: # ({/9d8785c0-d0a0a311})

[comment]: # ({4f9077b8-4f9077b8})
##### Item value preprocessing

The **Preprocessing** tab allows to define [transformation
rules](/manual/config/items/preprocessing) for the received values.

[comment]: # ({/4f9077b8-4f9077b8})

[comment]: # ({ff2c0878-006c45ea})
#### Testing

::: noteimportant
To perform item testing, ensure that the system time on the server and the proxy is [synchronized](/manual/installation/requirements#time-synchronization).
In the case when the server time is behind, item testing may return an error message "The task has been expired."
Having set different time zones on the server and the proxy, however, won't affect the testing result.
:::

It is possible to test an item and, if configured correctly, get a real
value in return. Testing can occur even before an item is saved.

Testing is available for host and template items, item prototypes and
low-level discovery rules. Testing is not available for active items.

Item testing is available for the following passive item types:

-   Zabbix agent
-   SNMP agent (v1, v2, v3)
-   IPMI agent
-   SSH checks
-   Telnet checks
-   JMX agent
-   Simple checks (except `icmpping*`, `vmware.*` items)
-   Zabbix internal
-   Calculated items
-   External checks
-   Database monitor
-   HTTP agent
-   Script

To test an item, click on the *Test* button at the bottom of the item
configuration form. Note that the *Test* button will be disabled for
items that cannot be tested (like active checks, excluded simple
checks).

![](../../../../assets/en/manual/config/items/item_test_button.png)

The item testing form has fields for the required host parameters (host
address, port, proxy name/no proxy) and item-specific details (such as
SNMPv2 community or SNMPv3 security credentials). These fields are
context aware:

-   The values are pre-filled when possible, i.e., for items requiring an
    agent, by taking the information from the selected agent interface
    of the host
-   The values have to be filled manually for template items
-   Plain-text macro values are resolved
-   Fields where the value (or part of the value) is a secret or Vault
    macro are empty and have to be entered manually. If any item
    parameter contains a secret macro value, the following warning
    message is displayed: "Item contains user-defined macros with secret
    values. Values of these macros should be entered manually."
-   The fields are disabled when not needed in the context of the item
    type (e.g., the host address field and the proxy field are disabled
    for calculated items)

To test the item, click on *Get value*. If the value is retrieved
successfully, it will fill the *Value* field, moving the current value
(if any) to the *Previous value* field while also calculating the *Prev.
time* field, i.e., the time difference between the two values (clicks)
and trying to detect an EOL sequence and switch to CRLF if detecting
"\\n\\r" in retrieved value.

![](../../../../assets/en/manual/config/items/item_test.png){width="600"}

If the configuration is incorrect, an error message is displayed
describing the possible cause.

![](../../../../assets/en/manual/config/items/item_test_error.png)

A successfully retrieved value from host can also be used to test
[preprocessing steps](/manual/config/items/preprocessing#testing).

[comment]: # ({/ff2c0878-006c45ea})

[comment]: # ({ea5149c6-4b126a62})
#### Form buttons

Buttons at the bottom of the form allow to perform several operations.

|   |   |
|--|--------|
|![](../../../../assets/en/manual/config/button_add.png)|Add an item. This button is only available for new items.|
|![](../../../../assets/en/manual/config/button_update.png)|Update the properties of an item.|
|![](../../../../assets/en/manual/config/button_clone.png)|Create another item based on the properties of the current item.|
|![](../../../../assets/en/manual/config/button_execute.png)|Execute a check for a new item value immediately. Supported for **passive** checks only (see [more details](/manual/config/items/check_now)).<br>*Note* that when checking for a value immediately, configuration cache is not updated, thus the value will not reflect very recent changes to item configuration.|
|![](../../../../assets/en/manual/config/button_test.png)|Test if item configuration is correct by getting a value.|
|![](../../../../assets/en/manual/config/button_clear.png)|Delete the item history and trends.|
|![](../../../../assets/en/manual/config/button_delete.png)|Delete the item.|
|![](../../../../assets/en/manual/config/button_cancel.png)|Cancel the editing of item properties.|

[comment]: # ({/ea5149c6-4b126a62})

[comment]: # ({8cb32bf5-8cb32bf5})
#### Text data limits

Text data limits depend on the database backend. Before storing text
values in the database they get truncated to match the database value
type limit:

|Database|Type of information|<|<|
|--------|-------------------|-|-|
| |**Character**|**Log**|**Text**|
|MySQL|255 characters|65536 bytes|65536 bytes|
|PostgreSQL|255 characters|65536 characters|65536 characters|
|Oracle|255 characters|65536 characters|65536 characters|

[comment]: # ({/8cb32bf5-8cb32bf5})

[comment]: # ({aeb2b058-aeb2b058})
#### Unit conversion

By default, specifying a unit for an item results in a multiplier prefix
being added - for example, an incoming value '2048' with unit 'B' would
be displayed as '2KB'.

To prevent a unit from conversion, use the `!` prefix, for example,
`!B`. To better understand how the conversion works with and without the
exclamation mark, see the following examples of values and units:

    1024 !B → 1024 B
    1024 B → 1 KB
    61 !s → 61 s
    61 s → 1m 1s
    0 !uptime → 0 uptime
    0 uptime → 00:00:00
    0 !! → 0 !
    0 ! → 0

::: noteclassic
Before Zabbix 4.0, there was a hardcoded unit stoplist
consisting of `ms`, `rpm`, `RPM`, `%`. This stoplist has been
deprecated, thus the correct way to prevent converting such units is
`!ms`, `!rpm`, `!RPM`, `!%`.
:::

[comment]: # ({/aeb2b058-aeb2b058})

[comment]: # ({526f3e7c-d4406a73})
#### Custom script limit

Available custom script length depends on the database used:

|Database|Limit in characters|Limit in bytes|
|---|---|---|
|**MySQL**|65535|65535|
|**Oracle Database**|2048|4000|
|**PostgreSQL**|65535|not limited|
|**SQLite (only Zabbix proxy)**|65535|not limited|

[comment]: # ({/526f3e7c-d4406a73})

[comment]: # ({2cd40177-2cd40177})
#### Unsupported items

An item can become unsupported if its value cannot be retrieved for some
reason. Such items are still rechecked at their standard *[Update
interval](/manual/config/items/item?#configuration)*.

Unsupported items are reported as having a NOT SUPPORTED state.

[comment]: # ({/2cd40177-2cd40177})
