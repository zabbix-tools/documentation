[comment]: # ({856c911b-856c911b})
# 1 Item key format

Item key format, including key parameters, must follow syntax rules. The
following illustrations depict the supported syntax. Allowed elements
and characters at each point can be determined by following the arrows -
if some block can be reached through the line, it is allowed, if not -
it is not allowed.

![](../../../../../assets/en/manual/config/item_key_2.png){width="600"}

To construct a valid item key, one starts with specifying the key name,
then there's a choice to either have parameters or not - as depicted by
the two lines that could be followed.

[comment]: # ({/856c911b-856c911b})

[comment]: # ({63ccf8e4-63ccf8e4})
#### Key name

The key name itself has a limited range of allowed characters, which
just follow each other. Allowed characters are:

    0-9a-zA-Z_-.

Which means:

-   all numbers;
-   all lowercase letters;
-   all uppercase letters;
-   underscore;
-   dash;
-   dot.

![](../../../../../assets/en/manual/config/key_name.png)

[comment]: # ({/63ccf8e4-63ccf8e4})

[comment]: # ({7262f26f-af5ae7f1})
#### Key parameters

An item key can have multiple parameters that are comma separated.

![](../../../../../assets/en/manual/config/key_parameters.png)

Each key parameter can be either a quoted string, an unquoted string or
an array.

![](../../../../../assets/en/manual/config/item_parameter.png)

The parameter can also be left empty, thus using the default value. In
that case, the appropriate number of commas must be added if any further
parameters are specified. For example, item key
**icmpping\[,,200,,500\]** would specify that the interval between
individual pings is 200 milliseconds, timeout - 500 milliseconds, and
all other parameters are left at their defaults.

It is possible to include macros in the parameters. Those can be [user macros](/manual/appendix/macros/supported_by_location_user#items-item-prototypes) or some of the built-in macros.
To see what particular built-in macros are supported in item key parameters,
search the page [Supported macros](/manual/appendix/macros/supported_by_location) for "item key parameters".

[comment]: # ({/7262f26f-af5ae7f1})

[comment]: # ({aa62ac28-b4a70ec9})
#### Parameter - quoted string

If the key parameter is a quoted string, any Unicode character is
allowed. If the key parameter string contains a quotation mark, this parameter has
to be quoted, and each quotation mark which is a part of the parameter
string has to be escaped with a backslash (`\`) character. If the key parameter string contains comma, this parameter has to be quoted.

![](../../../../../assets/en/manual/config/key_param_quoted_string.png)

::: notewarning
To quote item key parameters, use double quotes
only. Single quotes are not supported.
:::

[comment]: # ({/aa62ac28-b4a70ec9})

[comment]: # ({3376836e-3376836e})
#### Parameter - unquoted string

If the key parameter is an unquoted string, any Unicode character is
allowed except comma and right square bracket (\]). Unquoted parameter
cannot start with left square bracket (\[).

![](../../../../../assets/en/manual/config/key_param_unquoted_string.png)

[comment]: # ({/3376836e-3376836e})

[comment]: # ({5289a973-5289a973})
#### Parameter - array

If the key parameter is an array, it is again enclosed in square
brackets, where individual parameters come in line with the rules and
syntax of specifying multiple parameters.

![](../../../../../assets/en/manual/config/key_param_array.png)

::: noteimportant
Multi-level parameter arrays, e.g.
`[a,[b,[c,d]],e]`, are not allowed.
:::

[comment]: # ({/5289a973-5289a973})
