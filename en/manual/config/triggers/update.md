[comment]: # ({3a1b4ec0-3a1b4ec0})
# 6 Mass update

[comment]: # ({/3a1b4ec0-3a1b4ec0})

[comment]: # ({66063302-66063302})
#### Overview

With mass update you may change some attribute for a number of triggers
at once, saving you the need to open each individual trigger for
editing.

[comment]: # ({/66063302-66063302})

[comment]: # ({00b96653-00b96653})
#### Using mass update

To mass-update some triggers, do the following:

-   Mark the checkboxes of the triggers you want to update in the list
-   Click on *Mass update* below the list
-   Navigate to the tab with required attributes (*Trigger*, *Tags* or
    *Dependencies*)
-   Mark the checkboxes of any attribute to update

![](../../../../assets/en/manual/config/triggers/trigger_mass.png)

![](../../../../assets/en/manual/config/triggers/trigger_mass_b.png)

The following options are available when selecting the respective button
for tag update:

-   *Add* - allows to add new tags for the triggers;
-   *Replace* - will remove any existing tags from the trigger and
    replace them with the one(s) specified below;
-   *Remove* - will remove specified tags from triggers.

Note, that tags with the same name, but different values are not
considered 'duplicates' and can be added to the same trigger.

![](../../../../assets/en/manual/config/triggers/trigger_mass_c.png)

*Replace dependencies* - will remove any existing dependencies from the
trigger and replace them with the one(s) specified.

Click on *Update* to apply the changes.

[comment]: # ({/00b96653-00b96653})
