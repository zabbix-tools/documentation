[comment]: # ({81dd14e7-81dd14e7})
# 5 Customizing trigger severities

Trigger severity names and colors for severity related GUI elements can
be configured in *Administration → General → Trigger displaying
options*. Colors are shared among all GUI themes.

[comment]: # ({/81dd14e7-81dd14e7})

[comment]: # ({78725316-78725316})
#### Translating customized severity names

::: noteimportant
If Zabbix frontend translations are used, custom
severity names will override translated names by default.
:::
Default trigger severity names are available for translation in all
locales. If a severity name is changed, a custom name is used in all
locales and additional manual translation is needed.

Custom severity name translation procedure:

-   set required custom severity name, for example, 'Important'
-   edit
    <frontend\_dir>/locale/<required\_locale>/LC\_MESSAGES/frontend.po
-   add 2 lines:

```{=html}
<!-- -->
```
    msgid "Important"
    msgstr "<translation string>"

and save file.

-   create .mo files as described in <frontend\_dir>/locale/README

Here **msgid** should match the new custom severity name and **msgstr**
should be the translation for it in the specific language.

This procedure should be performed after each severity name change.

[comment]: # ({/78725316-78725316})
