[comment]: # ({94205f99-94205f99})
# 2 Items

[comment]: # ({/94205f99-94205f99})

[comment]: # ({3f680eac-3f680eac})
#### Overview

Items are the ones that gather data from a host.

Once you have configured a host, you need to add some monitoring items
to start getting actual data.

An item is an individual metric. One way of quickly adding many items is
to attach one of the predefined templates to a host. For optimized
system performance though, you may need to fine-tune the templates to
have only as many items and as frequent monitoring as is really
necessary.

In an individual item you specify what sort of data will be gathered
from the host.

For that purpose you use the [item key](/manual/config/items/item/key).
Thus an item with the key name **system.cpu.load** will gather data of
the processor load, while an item with the key name **net.if.in** will
gather incoming traffic information.

To specify further parameters with the key, you include those in square
brackets after the key name. Thus, system.cpu.load**\[avg5\]** will
return processor load average for the last 5 minutes, while
net.if.in**\[eth0\]** will show incoming traffic in the interface eth0.

::: noteclassic
For all supported item types and item keys, see individual
sections of [item types.](/manual/config/items/itemtypes)
:::

Proceed to [creating and configuring an
item](/manual/config/items/item).

[comment]: # ({/3f680eac-3f680eac})
