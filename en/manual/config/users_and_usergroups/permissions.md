[comment]: # ({ba72dbf1-ba72dbf1})
# 2 Permissions

[comment]: # ({/ba72dbf1-ba72dbf1})

[comment]: # ({a6655102-a9157075})
#### Overview

Permissions in Zabbix depend on the user type, customized user roles and access to hosts, which is specified based on the user group.

[comment]: # ({/a6655102-a9157075})

[comment]: # ({cb195572-ac897fff})

#### User types

Permissions in Zabbix depend, primarily, on the user type:

   - *User* - has limited access rights to menu sections (see below) and no access to any resources by default. Any permissions to host or template groups must be explicitly assigned;
   - *Admin* - has incomplete access rights to menu sections (see below). The user has no access to any host groups by default. Any permissions to host or template groups must be explicitly given;
   - *Super admin* - has access to all menu sections. The user has a read-write access to all host and template groups. Permissions cannot be revoked by denying access to specific groups.

**Menu access**

The following table illustrates access to Zabbix menu sections per user type:

|Menu section|<|User|Admin|Super admin|
|-|-------------|------|------|------|
|**Dashboards**|<|+|+|+|
|**Monitoring**|<|+|+|+|
| |*Problems*|+|+|+|
|^|*Hosts*|+|+|+|
|^|*Latest data*|+|+|+|
|^|*Maps*|+|+|+|
|^|*Discovery*| |+|+|
|**Services**|<|+|+|+|
| |*Services*|+|+|+|
|^|*SLA*| |+|+|
|^|*SLA report*|+|+|+|
|**Inventory**|<|+|+|+|
| |*Overview*|+|+|+|
|^|*Hosts*|+|+|+|
|**Reports**|<|+|+|+|
| |*System information*| | |+|
|^|*Scheduled reports*| |+|+|
|^|*Availability report*|+|+|+|
|^|*Triggers top 100*|+|+|+|
|^|*Audit log*| | |+|
|^|*Action log*| | |+|
|^|*Notifications*| |+|+|
|**Data collection**|<| |+|+|
| |*Template groups*| |+|+|
|^|*Host groups*| |+|+|
|^|*Templates*| |+|+|
|^|*Hosts*| |+|+|
|^|*Maintenance*| |+|+|
|^|*Event correlation*| | |+|
|^|*Discovery*| |+|+|
|**Alerts**|<| |+|+|
| |*Trigger actions*| |+|+|
| |*Service actions*| |+|+|
| |*Discovery actions*| |+|+|
| |*Autoregistration actions*| |+|+|
| |*Internal actions*| |+|+|
|^|*Media types*| | |+|
|^|*Scripts*| | |+|
|**Users**|<| | |+|
| |*User groups*| | |+|
|^|*User roles*| | |+|
|^|*Users*| | |+|
|^|*API tokens*| | |+|
|^|*Authentication*| | |+|
|**Administration**|<| | |+|
| |*General*| | |+|
|^|*Audit log*| | |+|
|^|*Housekeeping*| | |+|
|^|*Proxies*| | |+|
|^|*Macros*| | |+|
|^|*Queue*| | |+|

[comment]: # ({/cb195572-ac897fff})

[comment]: # ({d4537d79-a852da1b})

#### User roles

User roles allow to make custom adjustments to the permissions defined by the user type. 
While no permissions can be added (that would exceed those of the user type), some permissions can be revoked.

Furthermore, a user role determines access not only to menu sections, but also to services, modules, API methods and 
various actions in the frontend.

[User roles](/manual/web_interface/frontend_sections/users/user_roles)
are configured in the *Users → User roles* section by Super admin 
users.

User roles are assigned to users in the user configuration form, *Permissions* tab, by Super admin users.

![user\_permissions.png](../../../../assets/en/manual/config/user_permissions.png)

[comment]: # ({/d4537d79-a852da1b})

[comment]: # ({978f00b7-f38cb50e})
#### Access to hosts

Access to any host and template data in Zabbix is granted to [user
groups](/manual/config/users_and_usergroups/usergroup) on the host/template group
level only.

That means that an individual user cannot be directly granted access to
a host (or host group). It can only be granted access to a host by being
part of a user group that is granted access to the host group that
contains the host. 

Similarly, a user can only be granted access to a template by being part of a user group that is granted access to the template group that
contains the template.

[comment]: # ({/978f00b7-f38cb50e})
