[comment]: # ({8235aadd-317d63e9})
# HashiCorp configuration  

This section explains how to configure Zabbix to retrieve secrets from HashiCorp Vault KV Secrets Engine - Version 2.

The vault should be deployed and configured as per the official HashiCorp [documentation](https://www.vaultproject.io/docs/secrets/kv/kv-v2). 

To learn about configuring TLS in Zabbix, see [Storage of secrets](/manual/config/secrets#configuring_tls) section.

[comment]: # ({/8235aadd-317d63e9})

[comment]: # ({15ef61a6-9c0ce526})
### Database credentials

Access to a secret with database credentials is configured for each Zabbix component separately. 

#### Server and proxies
To obtain database credentials for Zabbix [server](/manual/appendix/config/zabbix_server) or [proxy](/manual/appendix/config/zabbix_proxy) from the vault, specify the following configuration parameters in the configuration file:

- *Vault* - specifies which vault provider should be used.   
- *VaultToken* - vault authentication token (see Zabbix server/proxy configuration file for details).
- *VaultURL* - vault server HTTP[S] URL.
- *VaultDBPath* - path to the vault secret containing database credentials. Zabbix server or proxy will retrieve the credentials by keys 'password' and 'username'.

:::noteimportant
Zabbix server also uses these configuration parameters (except VaultDBPath) for vault authentication when processing vault secret macros. 
:::

Zabbix server and Zabbix proxy read the vault-related configuration parameters from zabbix_server.conf and zabbix_proxy.conf upon startup.

Zabbix server and Zabbix proxy will additionally read "VAULT_TOKEN" environment variable once during startup and unset it so that it would not be available through forked scripts; it is an error if both VaultToken and VAULT_TOKEN contain value.

[comment]: # ({/15ef61a6-9c0ce526})

[comment]: # ({8d049430-19842818})
**Example** 

In zabbix_server.conf, specify: 

    Vault=HashiCorp
    VaultToken=hvs.CAESIIG_PILmULFYOsEyWHxkZ2mF2a8VPKNLE8eHqd4autYGGh4KHGh2cy5aeTY0NFNSaUp3ZnpWbDF1RUNjUkNTZEg
    VaultURL=https://127.0.0.1:8200
    VaultDBPath=secret/zabbix/database

Run the following CLI commands to create required secret in the vault:

    # Enable "secret/" mount point if not already enabled, note that "kv-v2" must be used
    vault secrets enable -path=secret/ kv-v2

    # Put new secrets with keys username and password under mount point "secret/" and path "secret/zabbix/database"
    vault kv put secret/zabbix/database username=zabbix password=<password>

    # Test that secret is successfully added
    vault kv get secret/zabbix/database

    # Finally test with Curl, note that "data" need to be manually added after mount point and "/v1" before the mount point, also see --capath parameter
    curl --header "X-Vault-Token: <VaultToken>" https://127.0.0.1:8200/v1/secret/data/zabbix/database

As a result of this configuration, Zabbix server will retrieve the following credentials for database authentication:

- Username: zabbix 
- Password: <password>

[comment]: # ({/8d049430-19842818})

[comment]: # ({e64930f1-0cae9564})
#### Frontend

To obtain database credentials for Zabbix frontend from the vault, specify required settings during frontend [installation](/manual/installation/frontend). 

At the *Configure DB Connection* step, set *Store credentials in* parameter to HashiCorp Vault. 

![](../../../../assets/en/manual/config/hashicorp_setup.png)

Then, fill in additional parameters:

|Parameter|Mandatory|Default value| Description|
|--|-|--|--------|
|Vault API endpoint | yes | https://localhost:8200 |  Specify the URL for connecting to the vault in the format `scheme://host:port` |
|Vault secret path | no | | A path to the secret from where credentials for the database shall be retrieved by the keys 'password' and 'username' <br> **Example:** `secret/zabbix/database_frontend` |
|Vault authentication token | no |  |Provide an authentication token for read-only access to the secret path. <br><br> See [HashiCorp documentation](https://learn.hashicorp.com/tutorials/vault/tokens) for information about creating tokens and vault policies. | 

[comment]: # ({/e64930f1-0cae9564})

[comment]: # ({d1e15440-2e69e703})
### User macro values

To use HashiCorp Vault for storing *Vault secret* user macro values, make sure that:

- The *Vault provider* parameter in the *Administration -> General -> Other* web interface [section](/manual/web_interface/frontend_sections/administration/general#other-parameters) is set to HashiCorp Vault (default). 

![](../../../../assets/en/manual/config/provider_hashicorp.png)

- Zabbix server is [configured](/manual/config/secrets/hashicorp#server_and_proxies) to work with HashiCorp Vault.   

The macro value should contain a reference path (as `path:key`, for example, `secret/zabbix:password`). The authentication token specified during Zabbix server configuration (by 'VaultToken' parameter) must provide read-only access to this path.


See [Vault secret macros](/manual/config/macros/secret_macros#vault_secret) for detailed information about macro value processing by Zabbix.

[comment]: # ({/d1e15440-2e69e703})

[comment]: # ({9ebd51b5-b5cf890a})
#### Path syntax

The symbols forward slash and colon are reserved. A forward slash can only be used to separate a mount point from a path (e.g. *secret/zabbix* where the mount point is "secret" and "zabbix" is the path) and, in case of Vault macros, a colon can only be used to separate a path/query from a key. It is possible to URL-encode "/" and ":" if there is a need to create a mount point with the name that is separated by a forward slash (e.g. *foo/bar/zabbix*, where the mount point is "foo/bar" and the path is "zabbix", as "foo%2Fbar/zabbix") and if a mount point name or path need to contain a colon.

[comment]: # ({/9ebd51b5-b5cf890a})

[comment]: # ({aefa7eb9-0c64c650})
**Example**

In Zabbix: add user macro {$PASSWORD} with type *Vault secret* and value `secret/zabbix:password`

![](../../../../assets/en/manual/config/hashi_macro.png)

Run the following CLI commands to create required secret in the vault:

    # Enable "secret/" mount point if not already enabled, note that "kv-v2" must be used
    vault secrets enable -path=secret/ kv-v2

    # Put new secret with key password under mount point "secret/" and path "secret/zabbix"
    vault kv put secret/zabbix password=<password>

    # Test that secret is successfully added
    vault kv get secret/zabbix

    # Finally test with Curl, note that "data" need to be manually added after mount point and "/v1" before the mount point, also see --capath parameter
    curl --header "X-Vault-Token: <VaultToken>" https://127.0.0.1:8200/v1/secret/data/zabbix

Now the macro {$PASSWORD} will resolve to the value: <password>

[comment]: # ({/aefa7eb9-0c64c650})
