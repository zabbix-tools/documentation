[comment]: # ({dcbd72b9-4ce68864})
# CyberArk configuration  

This section explains how to configure Zabbix to retrieve secrets from CyberArk Vault CV12.

The vault should be installed and configured as per the official CyberArk [documentation](https://docs.cyberark.com/Product-Doc/OnlineHelp/PAS/12.1/en/Content/HomeTilesLPs/LP-Tile6.htm?tocpath=Installation%7C_____0).

To learn about configuring TLS in Zabbix, see [Storage of secrets](/manual/config/secrets) section.

[comment]: # ({/dcbd72b9-4ce68864})

[comment]: # ({37f7f107-bf4f1f87})
### Database credentials

Access to a secret with database credentials is configured for each Zabbix component separately. 

#### Server and proxies

To obtain database credentials for Zabbix [server](/manual/appendix/config/zabbix_server) or [proxy](/manual/appendix/config/zabbix_proxy) from the vault, specify the following configuration parameters in the configuration file:

- *Vault* - specifies which vault provider should be used.   
- *VaultURL* - vault server HTTP\[S\] URL.
- *VaultDBPath* - query to the vault secret containing database credentials. The credentials will be retrieved by keys 'Content' and 'UserName'.
- *VaultTLSCertFile*, *VaultTLSKeyFile* - SSL certificate and key file names. Setting up these options is not mandatory, but highly recommended.

:::noteimportant
Zabbix server also uses these configuration parameters (except VaultDBPath) for vault authentication when processing vault secret macros. 
:::

Zabbix server and Zabbix proxy read the vault-related configuration parameters from zabbix_server.conf and zabbix_proxy.conf upon startup.

[comment]: # ({/37f7f107-bf4f1f87})

[comment]: # ({49b19aab-c8ed5f40})
**Example** 

In zabbix_server.conf, specify: 

    Vault=CyberArk
    VaultURL=https://127.0.0.1:1858
    VaultDBPath=AppID=zabbix_server&Query=Safe=passwordSafe;Object=zabbix_server_database
    VaultTLSCertFile=cert.pem
    VaultTLSKeyFile=key.pem 

Zabbix will send the following API request to the vault:
    
    curl \
    --header "Content type: application/json" \
    --cert cert.pem \
    --key key.pem \
    https://127.0.0.1:1858/AIMWebService/api/Accounts?AppID=zabbix_server&Query=Safe=passwordSafe;Object=zabbix_server_database

Vault response, from which the keys "Content" and "UserName" should be retrieved:

    {
    "Content": <password>,
    "UserName": <username>,
    "Address": <address>,
    "Database" :<Database>,
    "PasswordChangeInProcess":<PasswordChangeInProcess>
    }
    
As a result, Zabbix will use the following credentials for database authentication: 

- Username: <username>
- Password: <password>

[comment]: # ({/49b19aab-c8ed5f40})

[comment]: # ({7183aebc-f5f1a375})
#### Frontend

To obtain database credentials for Zabbix frontend from the vault, specify required settings during frontend [installation](/manual/installation/frontend).  

At the *Configure DB Connection* step, set *Store credentials in* parameter to CyberArk Vault. 

![](../../../../assets/en/manual/config/cyberark_setup.png)

Then, fill in additional parameters:

|Parameter|Mandatory|Default value| Description|
|--|-|--|--------|
|Vault API endpoint | yes | https://localhost:1858 |  Specify the URL for connecting to the vault in the format `scheme://host:port` |
|Vault secret query string | yes | | A query, which specifies from where database credentials should be retrieved. <br> **Example:** `AppID=foo&Query=Safe=bar;Object=buzz:key` |
|Vault certificates | no | |After marking the checkbox, additional parameters will appear allowing to configure client authentication. <br> While this parameter is optional, it is highly recommended to enable it for communication with the CyberArk Vault. |
|SSL certificate file | no | conf/certs/cyberark-cert.pem | Path to SSL certificate file. The file must be in PEM format. <br> If the certificate file contains also the private key, leave the SSL key file parameter empty. |
|SSL key file |no | conf/certs/cyberark-key.pem |Name of the SSL private key file used for client authentication. The file must be in PEM format.|

[comment]: # ({/7183aebc-f5f1a375})

[comment]: # ({56682ebd-11bde60a})
### User macro values

To use CyberArk Vault for storing *Vault secret* user macro values:

- Set the *Vault provider* parameter in the *Administration -> General -> Other* web interface [section](/manual/web_interface/frontend_sections/administration/general#other-parameters) to CyberArk Vault. 

![](../../../../assets/en/manual/config/provider_cyberark.png)

- Make sure that Zabbix server is [configured](/manual/config/secrets/cyberark#server_and_proxies) to work with CyberArk Vault.   

The macro value should contain a query (as `query:key`). 

See [Vault secret macros](/manual/config/macros/secret_macros#vault_secret) for detailed information about macro value processing by Zabbix.

[comment]: # ({/56682ebd-11bde60a})

[comment]: # ({89507233-1b41be0a})
#### Query syntax

The colon symbol (`:`) is reserved for separating the query from the key. If a query itself contains a forward slash or a colon, these symbols should be URL-encoded (`/` is encoded as `%2F`, `:` is encoded as `%3A`).

[comment]: # ({/89507233-1b41be0a})

[comment]: # ({4694809a-423b1929})
**Example**

In Zabbix: add user macro {$PASSWORD} with type *Vault secret* and value: `AppID=zabbix_server&Query=Safe=passwordSafe;Object=zabbix:Content`

![](../../../../assets/en/manual/config/cyberark_macro.png)

Zabbix will send API request to the vault:
    
    curl \
    --header "Content type: application/json" \
    --cert cert.pem \
    --key key.pem \
    https://127.0.0.1:1858/AIMWebService/api/Accounts?AppID=zabbix_server&Query=Safe=passwordSafe;Object=zabbix_server_database

Vault response, from which the key "Content" should be retrieved:

    {
    "Content": <password>,
    "UserName": <username>,
    "Address": <address>,
    "Database" :<Database>,
    "PasswordChangeInProcess":<PasswordChangeInProcess>
    }
    
Macro resolves to the value: <password>

[comment]: # ({/4694809a-423b1929})




