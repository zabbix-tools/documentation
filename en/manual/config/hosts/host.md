[comment]: # ({1da35dcd-1da35dcd})
# 1 Configuring a host

[comment]: # ({/1da35dcd-1da35dcd})

[comment]: # ({776fcc5e-0ee38f7e})
#### Overview

To configure a host in Zabbix frontend, do the following:

-   Go to: *Data collection → Hosts* or *Monitoring → Hosts*
-   Click on *Create host* to the right (or on the host name to edit an
    existing host)
-   Enter parameters of the host in the form

You can also use the *Clone* button in the configuration form of an existing host to create a new host.
This host will have all of the properties of the existing host, including linked templates, entities (items, triggers, etc.) from those templates, as well as the entities directly attached to the existing host.

Note that when a host is cloned, it will retain all template entities as
they are originally on the template. Any changes to those entities made
on the existing host level (such as changed item interval, modified
regular expression or added prototypes to the low-level discovery rule)
will not be cloned to the new host; instead they will be as on the
template.

[comment]: # ({/776fcc5e-0ee38f7e})

[comment]: # ({3250bf59-e2f56cfc})
#### Configuration

The **Host** tab contains general host attributes:

![](../../../../assets/en/manual/config/host_a.png)

All mandatory input fields are marked with a red asterisk.

|Parameter|<|Description|
|-|----------|----------------------------------------|
|*Host name*|<|Enter a unique host name. Alphanumerics, spaces, dots, dashes and underscores are allowed. However, leading and trailing spaces are disallowed.<br>*Note:* With Zabbix agent running on the host you are configuring, the agent [configuration file](/manual/appendix/config/zabbix_agentd) parameter *Hostname* must have the same value as the host name entered here. The name in the parameter is needed in the processing of [active checks](/manual/appendix/items/activepassive).|
|*Visible name*|<|Enter a unique visible name for the host. If you set this name, it will be the one visible in lists, maps, etc instead of the technical host name. This attribute has UTF-8 support.|
|*Templates*|<|Link [templates](/manual/config/templates) to the host. All entities (items, triggers, [etc.](/manual/config/templates/linking)) will be inherited from the template.<br>To link a new template, start typing the template name in the text input field. A list of matching templates will appear; scroll down to select. Alternatively, you may click on *Select* next to the field and select templates from the list in a popup window. All selected templates will be linked to the host when the host configuration form is saved or updated.<br>To unlink a template, use one of the two options in the *Linked templates* block:<br>*Unlink* - unlink the template, but preserve its entities (items, triggers, [etc.](/manual/config/templates/linking));<br>*Unlink and clear* - unlink the template and remove all its entities (items, triggers, [etc.](/manual/config/templates/linking)).<br>Listed template names are clickable links leading to the template configuration form.|
|*Host groups*|<|Select host groups the host belongs to. A host must belong to at least one host group. A new group can be created and linked to the host by adding a non-existing group name.|
|*Interfaces*|<|Several host interface types are supported for a host: *Agent*, *SNMP*, *JMX* and *IPMI*.<br>No interfaces are defined by default. To add a new interface, click on *Add* in the *Interfaces* block, select the interface type and enter *IP/DNS*, *Connect to* and *Port* info.<br>*Note:* Interfaces that are used in any items cannot be removed and link *Remove* is grayed out for them.<br>See [Configuring SNMP monitoring](/manual/config/items/itemtypes/snmp#configuring_snmp_monitoring) for additional details on configuring an SNMP interface (v1, v2 and v3).|
| |*IP address*|Host IP address (optional).|
|^|*DNS name*|Host DNS name (optional).|
|^|*Connect to*|Clicking the respective button will tell Zabbix server what to use to retrieve data from agents:<br>**IP** - Connect to the host IP address (recommended)<br>**DNS** - Connect to the host DNS name|
|^|*Port*|TCP/UDP port number. Default values are: 10050 for Zabbix agent, 161 for SNMP agent, 12345 for JMX and 623 for IPMI.|
|^|*Default*|Check the radio button to set the default interface.|
|*Description*|<|Enter the host description.|
|*Monitored by proxy*|<|The host can be monitored either by Zabbix server or one of Zabbix proxies:<br>**(no proxy)** - host is monitored by Zabbix server<br>**Proxy name** - host is monitored by Zabbix proxy "Proxy name"|
|*Enabled*|<|Mark the checkbox to make the host active, ready to be monitored. If unchecked, the host is not active, thus not monitored.|

The **IPMI** tab contains IPMI management attributes.

|Parameter|Description|
|--|--------|
|*Authentication algorithm*|Select the authentication algorithm.|
|*Privilege level*|Select the privilege level.|
|*Username*|User name for authentication. User macros may be used.|
|*Password*|Password for authentication. User macros may be used.|

The **Tags** tab allows you to define host-level
[tags](/manual/config/tagging). All problems of this host will be tagged
with the values entered here.

![](../../../../assets/en/manual/config/host_d.png)

User macros, {INVENTORY.\*} macros, {HOST.HOST}, {HOST.NAME},
{HOST.CONN}, {HOST.DNS}, {HOST.IP}, {HOST.PORT} and {HOST.ID} macros are
supported in tags.

The **Macros** tab allows you to define host-level [user
macros](/manual/config/macros/user_macros) as a name-value pairs. Note
that macro values can be kept as plain text, secret text or Vault
secret. Adding a description is also supported.

![](../../../../assets/en/manual/config/host_e.png)

You may also view here template-level and global user macros if you
select the *Inherited and host macros* option. That is where all defined
user macros for the host are displayed with the value they resolve to as
well as their origin.

![](../../../../assets/en/manual/config/host_e2.png){width="600"}

For convenience, links to respective templates and global macro
configuration are provided. It is also possible to edit a
template/global macro on the host level, effectively creating a copy of
the macro on the host.

The **Inventory** tab allows you to manually enter
[inventory](inventory) information for the host. You can also select to
enable *Automatic* inventory population, or disable inventory population
for this host.

![](../../../../assets/en/manual/config/host_f.png)

If inventory is enabled (manual or automatic), a green dot is displayed
with the tab name.

[comment]: # ({/3250bf59-e2f56cfc})

[comment]: # ({d6974ece-6875b717})
##### Encryption

The **Encryption** tab allows you to require
[encrypted](/manual/encryption) connections with the host.

|Parameter|Description|
|--|--------|
|*Connections to host*|How Zabbix server or proxy connects to Zabbix agent on a host: no encryption (default), using PSK (pre-shared key) or certificate.|
|*Connections from host*|Select what type of connections are allowed from the host (i.e. from Zabbix agent and Zabbix sender). Several connection types can be selected at the same time (useful for testing and switching to other connection type). Default is "No encryption".|
|*Issuer*|Allowed issuer of certificate. Certificate is first validated with CA (certificate authority). If it is valid, signed by the CA, then the *Issuer* field can be used to further restrict allowed CA. This field is intended to be used if your Zabbix installation uses certificates from multiple CAs. If this field is empty then any CA is accepted.|
|*Subject*|Allowed subject of certificate. Certificate is first validated with CA. If it is valid, signed by the CA, then the *Subject* field can be used to allow only one value of *Subject* string. If this field is empty then any valid certificate signed by the configured CA is accepted.|
|*PSK identity*|Pre-shared key identity string.<br>Do not put sensitive information in the PSK identity, it is transmitted unencrypted over the network to inform a receiver which PSK to use.|
|*PSK*|Pre-shared key (hex-string). Maximum length: 512 hex-digits (256-byte PSK) if Zabbix uses GnuTLS or OpenSSL library, 64 hex-digits (32-byte PSK) if Zabbix uses mbed TLS (PolarSSL) library. Example: 1f87b595725ac58dd977beef14b97461a7c1045b9a1c963065002c5473194952|

[comment]: # ({/d6974ece-6875b717})

[comment]: # ({513311ca-513311ca})
##### Value mapping

The **Value mapping** tab allows to configure human-friendly
representation of item data in [value
mappings](/manual/config/items/mapping).

[comment]: # ({/513311ca-513311ca})

[comment]: # ({9094c44c-7cc0883e})
#### Creating a host group

::: noteimportant
Only Super Admin users can create host groups.
:::

To create a host group in Zabbix frontend, do the following:

-   Go to: *Data collection* → *Host groups*
-   Click on *Create host group* in the upper right corner of the screen
-   Enter the group name in the form

![](../../../../assets/en/manual/config/host_group.png)

To create a nested host group, use the '/' forward slash separator, for example `Europe/Latvia/Riga/Zabbix servers`. You can create this group even if none of the three parent host groups (`Europe/Latvia/Riga/`) exist. In this case creating these parent host groups is up to the user; they will not be created automatically.<br>Leading and trailing slashes, several slashes in a row are not allowed. Escaping of '/' is not supported.

[comment]: # ({/9094c44c-7cc0883e})

[comment]: # ({35265553-22cf222a})

Once the group is created, you can click on the group name in the list to edit group name, clone the group or set additional option:

![](../../../../assets/en/manual/config/host_group2.png)

*Apply permissions and tag filters to all subgroups* - mark this checkbox and click on *Update* to apply the same level of permissions/tag filters to all nested host groups. For user groups that may have had differing [permissions](/manual/config/users_and_usergroups/usergroup#configuration) assigned to nested host groups, the permission level of the parent host group will be enforced on the nested groups. This is a one-time option that is not saved in the database.

**Permissions to nested host groups**

-   When creating a child host group to an existing parent host group,
    [user group](/manual/config/users_and_usergroups/usergroup)
    permissions to the child are inherited from the parent (for example,
    when creating `Riga/Zabbix servers` if `Riga` already exists)
-   When creating a parent host group to an existing child host group,
    no permissions to the parent are set (for example, when creating
    `Riga` if `Riga/Zabbix servers` already exists)

[comment]: # ({/35265553-22cf222a})
