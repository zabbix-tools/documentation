[comment]: # (tags: action)

[comment]: # ({526f6fc0-526f6fc0})
# 2 Actions

[comment]: # ({/526f6fc0-526f6fc0})

[comment]: # ({a440870f-b19d9072})
#### Overview

If you want some operations taking place as a result of events (for
example, notifications sent), you need to configure actions.

Actions can be defined in response to events of all supported types:

-   Trigger actions - for events when trigger status changes from *OK*
    to *PROBLEM* and back
-   Service actions - for events when service status changes from *OK*
    to *PROBLEM* and back
-   Discovery actions - for events when network discovery takes place
-   Autoregistration actions - for events when new active agents
    auto-register (or host metadata changes for registered ones)
-   Internal actions - for events when items become unsupported or
    triggers go into an unknown state

The key differences of service actions are: 

-   User access to service actions depends on access rights to services granted by user's [role](/manual/web_interface/frontend_sections/users/user_roles)
-   Service actions support different set of [conditions](/manual/config/notification/action/conditions)

[comment]: # ({/a440870f-b19d9072})

[comment]: # ({d9a327e7-cf7fda79})
#### Configuring an action

To configure an action, do the following:

-   Go to *Alerts* → *Actions* and select
    the required action type from the submenu (you can
    switch to another type later, using the title dropdown)
-   Click on *Create action*
-   Name the action
-   Choose [conditions](/manual/config/notifications/action/conditions)
    upon which operations are carried out
-   Choose the
    [operations](/manual/config/notifications/action/operation) to carry
    out

General action attributes:

![](../../../../assets/en/manual/config/notifications/action.png)

All mandatory input fields are marked with a red asterisk.

|Parameter|Description|
|--|--------|
|*Name*|Unique action name.|
|*Type of calculation*|Select the evaluation [option](/manual/config/notifications/action/conditions#type_of_calculation) for action conditions (with more than one condition):<br>**And** - all conditions must be met.<br>**Or** - enough if one condition is met.<br>**And/Or** - combination of the two: AND with different condition types and OR with the same condition type.<br>**Custom expression** - a user-defined calculation formula for evaluating action conditions.|
|*Conditions*|List of action conditions.<br>Click on *Add* to add a new [condition](/manual/config/notifications/action/conditions).|
|*Enabled*|Mark the checkbox to enable the action. Otherwise, it will be disabled.|

[comment]: # ({/d9a327e7-cf7fda79})
