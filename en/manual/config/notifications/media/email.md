[comment]: # ({473c0010-d40475f4})
# 1 Email

[comment]: # ({/473c0010-d40475f4})

[comment]: # ({d47d3bca-9d59e93d})
#### Overview

To configure email as the delivery channel for messages, you need to
configure email as the media type and assign specific addresses to
users.

::: noteclassic
 Multiple notifications for single event will
be grouped together on the same email thread. 
:::

[comment]: # ({/d47d3bca-9d59e93d})

[comment]: # ({be6d6ca1-3711e99d})
#### Configuration

To configure email as the media type:

1.  Go to *Alerts → Media types*.
2.  Click on *Create media type* (or click on *Email* in the list of
    pre-defined media types).

The **Media type** tab contains general media type attributes:

![](../../../../../assets/en/manual/config/notifications/media/media_email.png){width="600"}

All mandatory input fields are marked with a red asterisk.

The following parameters are specific for the email media type:

|Parameter|Description|
|--|--------|
|*Email provider*|Select the email provider: *Generic SMTP*, *Gmail*, *Gmail relay*, *Office365*, or *Office365 relay*.<br>If you select the Gmail/Office365-related options, you will only need to supply the sender email address and password; such options as *SMTP server*, *SMTP server port*, *SMTP helo*, and *Connection security* will be automatically filled by Zabbix. See also: [Automated Gmail/Office365 media types](/manual/config/notifications/media#automated-gmailoffice365-media-types). |
|*SMTP server*|Set an SMTP server to handle outgoing messages.<br>This field is available if *Generic SMTP* is selected as the email provider.|
|*SMTP server port*|Set the SMTP server port to handle outgoing messages.<br>This field is available if *Generic SMTP* is selected as the email provider.|
|*Email*|The address entered here will be used as the **From** address for the messages sent.<br>Adding a sender display name (like "Zabbix\_info" in *Zabbix\_info <zabbix\@company.com>* in the screenshot above) with the actual email address is supported since Zabbix 2.2 version.<br>There are some restrictions on display names in Zabbix emails in comparison to what is allowed by RFC 5322, as illustrated by examples:<br>Valid examples:<br>*zabbix\@company.com* (only email address, no need to use angle brackets)<br>*Zabbix\_info <zabbix\@company.com>* (display name and email address in angle brackets)<br>*∑Ω-monitoring <zabbix\@company.com>* (UTF-8 characters in display name)<br>Invalid examples:<br>*Zabbix HQ zabbix\@company.com* (display name present but no angle brackets around email address)<br>*"Zabbix\\@\\<H(comment)Q\\>" <zabbix\@company.com>* (although valid by RFC 5322, quoted pairs and comments are not supported in Zabbix emails)|
|*SMTP helo*|Set a correct SMTP helo value, normally a domain name.<br>If empty, the domain name of the email will be sent (i.e., what comes after `@` in the *Email* field). If it is impossible to fetch the domain name, a debug-level warning will be logged and the server hostname will be sent as the domain for HELO command.<br>This field is available if *Generic SMTP* is selected as the email provider.|
|*Connection security*|Select the level of connection security:<br>**None** - do not use the [CURLOPT\_USE\_SSL](http://curl.haxx.se/libcurl/c/CURLOPT_USE_SSL.html) option<br>**STARTTLS** - use the CURLOPT\_USE\_SSL option with CURLUSESSL\_ALL value<br>**SSL/TLS** - use of CURLOPT\_USE\_SSL is optional|
|*SSL verify peer*|Mark the checkbox to verify the SSL certificate of the SMTP server.<br>The value of "SSLCALocation" server configuration directive should be put into [CURLOPT\_CAPATH](http://curl.haxx.se/libcurl/c/CURLOPT_CAPATH.html) for certificate validation.<br>This sets cURL option [CURLOPT\_SSL\_VERIFYPEER](http://curl.haxx.se/libcurl/c/CURLOPT_SSL_VERIFYPEER.html).|
|*SSL verify host*|Mark the checkbox to verify that the *Common Name* field or the *Subject Alternate Name* field of the SMTP server certificate matches.<br>This sets cURL option [CURLOPT\_SSL\_VERIFYHOST](http://curl.haxx.se/libcurl/c/CURLOPT_SSL_VERIFYHOST.html).|
|*Authentication*|Select the level of authentication:<br>**None** - no cURL options are set<br>(since 3.4.2) **Username and password** - implies "AUTH=\*" leaving the choice of authentication mechanism to cURL<br>(until 3.4.2) **Normal password** - [CURLOPT\_LOGIN\_OPTIONS](http://curl.haxx.se/libcurl/c/CURLOPT_LOGIN_OPTIONS.html) is set to "AUTH=PLAIN"|
|*Username*|User name to use in authentication.<br>This sets the value of [CURLOPT\_USERNAME](http://curl.haxx.se/libcurl/c/CURLOPT_USERNAME.html).|
|*Password*|Password to use in authentication.<br>This sets the value of [CURLOPT\_PASSWORD](http://curl.haxx.se/libcurl/c/CURLOPT_PASSWORD.html).|
|*Message format*|Select message format:<br>**HTML** - send as HTML<br>**Plain text** - send as plain text|

::: noteimportant
To make SMTP authentication options available,
Zabbix server should be compiled with the --with-libcurl
[compilation](/manual/installation/install#configure_the_sources) option
with cURL 7.20.0 or higher. 
:::

See also [common media type
parameters](/manual/config/notifications/media#common_parameters) for
details on how to configure default messages and alert processing
options.

[comment]: # ({/be6d6ca1-3711e99d})

[comment]: # ({3dc3705e-98f29ec0})

#### Media type testing

To test whether a configured email media type works correctly:

1.  Locate the relevant email in the [list](/manual/config/notifications/media#overview) of media types.
2.  Click on *Test* in the last column of the list (a testing window
    will open).
3.  Enter a *Send to* recipient address, message body and, optionally,
    subject.
4.  Click on *Test* to send a test message.

Test success or failure message will be displayed in the same window:

![](../../../../../assets/en/manual/config/notifications/media/test_email0.png){width="600"}

[comment]: # ({/3dc3705e-98f29ec0})

[comment]: # ({7c5d91f3-5e74f274})
#### User media

Once the email media type is configured, go to the *Users →
Users* section and edit user profile to assign email media to the user.
Steps for setting up user media, being common for all media types, are
described on the [Media
types](/manual/config/notifications/media#user_media) page.

[comment]: # ({/7c5d91f3-5e74f274})
