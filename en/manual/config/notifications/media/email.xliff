<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="en" datatype="plaintext" original="manual/config/notifications/media/email.md">
    <body>
      <trans-unit id="d40475f4" xml:space="preserve">
        <source># 1 Email</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/config/notifications/media/email.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="9d59e93d" xml:space="preserve">
        <source>#### Overview

To configure email as the delivery channel for messages, you need to
configure email as the media type and assign specific addresses to
users.

::: noteclassic
 Multiple notifications for single event will
be grouped together on the same email thread. 
:::</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/config/notifications/media/email.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="3711e99d" xml:space="preserve">
        <source>#### Configuration

To configure email as the media type:

1.  Go to *Alerts → Media types*.
2.  Click on *Create media type* (or click on *Email* in the list of
    pre-defined media types).

The **Media type** tab contains general media type attributes:

![](../../../../../assets/en/manual/config/notifications/media/media_email.png){width="600"}

All mandatory input fields are marked with a red asterisk.

The following parameters are specific for the email media type:

|Parameter|Description|
|--|--------|
|*Email provider*|Select the email provider: *Generic SMTP*, *Gmail*, *Gmail relay*, *Office365*, or *Office365 relay*.&lt;br&gt;If you select the Gmail/Office365-related options, you will only need to supply the sender email address and password; such options as *SMTP server*, *SMTP server port*, *SMTP helo*, and *Connection security* will be automatically filled by Zabbix. See also: [Automated Gmail/Office365 media types](/manual/config/notifications/media#automated-gmailoffice365-media-types). |
|*SMTP server*|Set an SMTP server to handle outgoing messages.&lt;br&gt;This field is available if *Generic SMTP* is selected as the email provider.|
|*SMTP server port*|Set the SMTP server port to handle outgoing messages.&lt;br&gt;This field is available if *Generic SMTP* is selected as the email provider.|
|*Email*|The address entered here will be used as the **From** address for the messages sent.&lt;br&gt;Adding a sender display name (like "Zabbix\_info" in *Zabbix\_info &lt;zabbix\@company.com&gt;* in the screenshot above) with the actual email address is supported since Zabbix 2.2 version.&lt;br&gt;There are some restrictions on display names in Zabbix emails in comparison to what is allowed by RFC 5322, as illustrated by examples:&lt;br&gt;Valid examples:&lt;br&gt;*zabbix\@company.com* (only email address, no need to use angle brackets)&lt;br&gt;*Zabbix\_info &lt;zabbix\@company.com&gt;* (display name and email address in angle brackets)&lt;br&gt;*∑Ω-monitoring &lt;zabbix\@company.com&gt;* (UTF-8 characters in display name)&lt;br&gt;Invalid examples:&lt;br&gt;*Zabbix HQ zabbix\@company.com* (display name present but no angle brackets around email address)&lt;br&gt;*"Zabbix\\@\\&lt;H(comment)Q\\&gt;" &lt;zabbix\@company.com&gt;* (although valid by RFC 5322, quoted pairs and comments are not supported in Zabbix emails)|
|*SMTP helo*|Set a correct SMTP helo value, normally a domain name.&lt;br&gt;If empty, the domain name of the email will be sent (i.e., what comes after `@` in the *Email* field). If it is impossible to fetch the domain name, a debug-level warning will be logged and the server hostname will be sent as the domain for HELO command.&lt;br&gt;This field is available if *Generic SMTP* is selected as the email provider.|
|*Connection security*|Select the level of connection security:&lt;br&gt;**None** - do not use the [CURLOPT\_USE\_SSL](http://curl.haxx.se/libcurl/c/CURLOPT_USE_SSL.html) option&lt;br&gt;**STARTTLS** - use the CURLOPT\_USE\_SSL option with CURLUSESSL\_ALL value&lt;br&gt;**SSL/TLS** - use of CURLOPT\_USE\_SSL is optional|
|*SSL verify peer*|Mark the checkbox to verify the SSL certificate of the SMTP server.&lt;br&gt;The value of "SSLCALocation" server configuration directive should be put into [CURLOPT\_CAPATH](http://curl.haxx.se/libcurl/c/CURLOPT_CAPATH.html) for certificate validation.&lt;br&gt;This sets cURL option [CURLOPT\_SSL\_VERIFYPEER](http://curl.haxx.se/libcurl/c/CURLOPT_SSL_VERIFYPEER.html).|
|*SSL verify host*|Mark the checkbox to verify that the *Common Name* field or the *Subject Alternate Name* field of the SMTP server certificate matches.&lt;br&gt;This sets cURL option [CURLOPT\_SSL\_VERIFYHOST](http://curl.haxx.se/libcurl/c/CURLOPT_SSL_VERIFYHOST.html).|
|*Authentication*|Select the level of authentication:&lt;br&gt;**None** - no cURL options are set&lt;br&gt;(since 3.4.2) **Username and password** - implies "AUTH=\*" leaving the choice of authentication mechanism to cURL&lt;br&gt;(until 3.4.2) **Normal password** - [CURLOPT\_LOGIN\_OPTIONS](http://curl.haxx.se/libcurl/c/CURLOPT_LOGIN_OPTIONS.html) is set to "AUTH=PLAIN"|
|*Username*|User name to use in authentication.&lt;br&gt;This sets the value of [CURLOPT\_USERNAME](http://curl.haxx.se/libcurl/c/CURLOPT_USERNAME.html).|
|*Password*|Password to use in authentication.&lt;br&gt;This sets the value of [CURLOPT\_PASSWORD](http://curl.haxx.se/libcurl/c/CURLOPT_PASSWORD.html).|
|*Message format*|Select message format:&lt;br&gt;**HTML** - send as HTML&lt;br&gt;**Plain text** - send as plain text|

::: noteimportant
To make SMTP authentication options available,
Zabbix server should be compiled with the --with-libcurl
[compilation](/manual/installation/install#configure_the_sources) option
with cURL 7.20.0 or higher. 
:::

See also [common media type
parameters](/manual/config/notifications/media#common_parameters) for
details on how to configure default messages and alert processing
options.</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/config/notifications/media/email.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="98f29ec0" xml:space="preserve">
        <source>
#### Media type testing

To test whether a configured email media type works correctly:

1.  Locate the relevant email in the [list](/manual/config/notifications/media#overview) of media types.
2.  Click on *Test* in the last column of the list (a testing window
    will open).
3.  Enter a *Send to* recipient address, message body and, optionally,
    subject.
4.  Click on *Test* to send a test message.

Test success or failure message will be displayed in the same window:

![](../../../../../assets/en/manual/config/notifications/media/test_email0.png){width="600"}</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/config/notifications/media/email.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="5e74f274" xml:space="preserve">
        <source>#### User media

Once the email media type is configured, go to the *Users →
Users* section and edit user profile to assign email media to the user.
Steps for setting up user media, being common for all media types, are
described on the [Media
types](/manual/config/notifications/media#user_media) page.</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/config/notifications/media/email.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
    </body>
  </file>
</xliff>
