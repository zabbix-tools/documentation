<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="en" datatype="plaintext" original="manual/config/notifications/media/webhook.md">
    <body>
      <trans-unit id="56bf7160" xml:space="preserve">
        <source># 4 Webhook</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/config/notifications/media/webhook.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="6584b266" xml:space="preserve">
        <source>#### Overview

The webhook media type is useful for making HTTP calls using custom
JavaScript code for straightforward integration with external software
such as helpdesk systems, chats, or messengers. You may choose to import
an integration provided by Zabbix or create a custom integration from
scratch.</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/config/notifications/media/webhook.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="27a06b1f" xml:space="preserve">
        <source>#### Integrations

The following integrations are available allowing to use predefined
webhook media types for pushing Zabbix notifications to:

-   [brevis.one](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/brevis.one/README.md)
-   [Discord](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/discord/README.md)
-   [Event-Driven Ansible](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/event_driven_ansible/README.md)
-   [Express.ms
    messenger](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/express.ms/README.md)
-   [Github
    issues](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/github/README.md)
-   [GLPi](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/glpi/README.md)
-   [iLert](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/ilert/README.md)
-   [iTop](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/itop/README.md)
-   [Jira](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/jira/README.md)
-   [Jira Service
    Desk](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/jira_servicedesk/README.md)
-   [ManageEngine
    ServiceDesk](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/manageengine_servicedesk/README.md)
-   [Mattermost](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/mattermost/README.md)
-   [Microsoft
    Teams](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/msteams/README.md)
-   [LINE](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/line/README.md)
-   [Opsgenie](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/opsgenie/README.md)
-   [OTRS](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/otrs/README.md)
-   [Pagerduty](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/pagerduty/README.md)
-   [Pushover](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/pushover/README.md)
-   [Redmine](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/redmine/README.md)
-   [Rocket.Chat](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/rocketchat/README.md)
-   [ServiceNow](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/servicenow/README.md)
-   [SIGNL4](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/signl4/README.md)
-   [Slack](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/slack/README.md)
-   [SolarWinds](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/solarwinds/README.md)
-   [SysAid](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/sysaid/README.md)
-   [Telegram](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/telegram/README.md)
-   [TOPdesk](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/topdesk/README.md)
-   [VictorOps](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/victorops/README.md)
-   [Zammad](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/zammad/README.md)
-   [Zendesk](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/zendesk/README.md)

::: notetip
 In addition to the services listed here, Zabbix can be
integrated with **Spiceworks** (no webhook is required). To convert
Zabbix notifications into Spiceworks tickets, create an [email media
type](/manual/config/notifications/media/email) and enter Spiceworks
helpdesk email address (e.g. help\@zabbix.on.spiceworks.com) in the
profile settings of a designated Zabbix user. 
:::</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/config/notifications/media/webhook.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="d84f1038" xml:space="preserve">
        <source>#### Configuration

To start using a webhook integration:

1.  Locate required .xml file in the `templates/media` directory of the
    downloaded Zabbix version or download it from Zabbix [git
    repository](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse).
2.  [Import](/manual/xml_export_import/media#importing) the file into
    your Zabbix installation. The webhook will appear in the list of
    media types.
3.  Configure the webhook according to instructions in the *Readme.md*
    file (you may click on a webhook's name above to quickly access
    *Readme.md*).

To create a custom webhook from scratch:

1.  Go to *Alerts → Media types*.
2.  Click on *Create media type*.

The **Media type** tab contains various attributes specific for this
media type:

![](../../../../../assets/en/manual/config/notifications/media/media_webhook_express.png){width="600"}

All mandatory input fields are marked with a red asterisk.

The following parameters are specific for the webhook media type:

|Parameter|Description|
|--|--------|
|*Parameters*|Specify the webhook variables as the attribute and value pairs.&lt;br&gt;For preconfigured webhooks, a list of parameters varies, depending on the service. Check the webhook's *Readme.md* file for parameter description.&lt;br&gt;For new webhooks, several common variables are included by default (URL:&lt;empty&gt;, HTTPProxy:&lt;empty&gt;, To:{ALERT.SENDTO}, Subject:{ALERT.SUBJECT}, Message:{ALERT.MESSAGE}), feel free to keep or remove them.&lt;br&gt;&lt;br&gt;Webhook parameters support [user macros](/manual/appendix/macros/supported_by_location_user), all [macros](/manual/appendix/macros/supported_by_location) that are supported in problem notifications and, additionally, {ALERT.SENDTO}, {ALERT.SUBJECT}, and {ALERT.MESSAGE} macros.&lt;br&gt;&lt;br&gt;If you specify an HTTP proxy, the field supports the same functionality as in the item configuration [HTTP proxy](/manual/config/items/itemtypes/http#configuration) field. The proxy string may be prefixed with `[scheme]://` to specify which kind of proxy is used (e.g., https, socks4, socks5; see [documentation](https://curl.haxx.se/libcurl/c/CURLOPT_PROXY.html)).|
|*Script*|Enter JavaScript code in the block that appears when clicking in the parameter field (or on the view/edit button next to it). This code will perform the webhook operation.&lt;br&gt;The script is a function code that accepts parameter - value pairs. The values should be converted into JSON objects using JSON.parse() method, for example: `var params = JSON.parse(value);`.&lt;br&gt;&lt;br&gt;The code has access to all parameters, it may perform HTTP GET, POST, PUT and DELETE requests and has control over HTTP headers and request body.&lt;br&gt;The script must contain a return operator, otherwise it will not be valid. It may return OK status along with an optional list of tags and tag values (see *Process tags* option) or an error string.&lt;br&gt;&lt;br&gt;Note that the script is executed only after an alert is created. If the script is configured to return and process tags, these tags will not get resolved in {EVENT.TAGS} and {EVENT.RECOVERY.TAGS} macros in the initial problem message and recovery messages because the script has not had the time to run yet.&lt;br&gt;&lt;br&gt;See also: [Webhook development guidelines](https://www.zabbix.com/documentation/guidelines/en/webhooks), [Webhook script examples](/manual/config/notifications/media/webhook/webhook_examples), [Additional JavaScript objects](/manual/config/items/preprocessing/javascript/javascript_objects).&lt;br&gt;|
|*Timeout*|JavaScript execution timeout (1-60s, default 30s).&lt;br&gt;Time suffixes are supported, e.g., 30s, 1m.|
|*Process tags*|Mark the checkbox to process returned JSON property values as tags. These tags are added to the already existing (if any) problem event tags in Zabbix.&lt;br&gt;If a webhook uses tags (the *Process tags* checkbox is marked), the webhook should always return a JSON object containing at least an empty object for tags:`var result = {tags: {}};`.&lt;br&gt;Examples of tags that can be returned: *Jira ID: PROD-1234*, *Responsible: John Smith*, *Processed:&lt;no value&gt;*, etc.|
|*Include event menu entry*|Mark the checkbox to include an entry in the [event menu](/manual/web_interface/menu/event_menu) linking to the created external ticket.&lt;br&gt;If marked, the webhook should not be used to send notifications to different users (consider creating a [dedicated user](/manual/config/notifications/media/webhook#user_media) instead) or in several alert actions [related to a single problem event](/manual/config/notifications/media/webhook#configuring_alert_actions).|
|*Menu entry name*|Specify the menu entry name.&lt;br&gt;{EVENT.TAGS.&lt;tag name&gt;} macro is supported.&lt;br&gt;This field is only mandatory if *Include event menu entry* is selected.|
|*Menu entry URL*|Specify the underlying URL of the menu entry.&lt;br&gt;{EVENT.TAGS.&lt;tag name&gt;} macro is supported.&lt;br&gt;This field is only mandatory if *Include event menu entry* is selected.|

See [common media type
parameters](/manual/config/notifications/media#common_parameters) for
details on how to configure default messages and alert processing
options.

::: notewarning
 Even if a webhook doesn't use default messages,
message templates for operation types used by this webhook must still be
defined.
:::</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/config/notifications/media/webhook.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="e5e73ea8" xml:space="preserve">
        <source>
#### Media type testing

To test a configured webhook media type:

1.  Locate the relevant webhook in the [list](/manual/config/notifications/media#overview) of media types.
2.  Click on *Test* in the last column of the list (a testing window
    will open).
3.  Edit the webhook parameter values, if needed.
4.  Click on *Test*.

By default, webhook tests are performed with parameters entered during
configuration. However, it is possible to change attribute values for
testing. Replacing or deleting values in the testing window affects the
test procedure only, the actual webhook attribute values will remain
unchanged.

![](../../../../../assets/en/manual/config/webhook_test1.png){width="600"}

To view media type test log entries without leaving the test window, click on *Open log* (a new pop-up window will open).

![](../../../../../assets/en/manual/config/mediatype_test2.png){width="600"}</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/config/notifications/media/webhook.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="b2974109" xml:space="preserve">
        <source>**If the webhook test is successful:**

-   *"Media type test successful."* message is displayed.
-   Server response appears in the gray *Response* field.
-   Response type (JSON or String) is specified below the *Response*
    field.</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/config/notifications/media/webhook.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="abfbc807" xml:space="preserve">
        <source>**If the webhook test fails:**

-   *"Media type test failed."* message is displayed, followed by
    additional failure details.</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/config/notifications/media/webhook.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="6eeec89a" xml:space="preserve">
        <source>
#### User media

Once the media type is configured, go to the *Users → Users*
section and assign the webhook media to an existing user or create a new
user to represent the webhook. Steps for setting up user media for an
existing user, being common for all media types, are described on the
[Media types](/manual/config/notifications/media#user_media) page.

If a webhook uses tags to store ticket\\message ID, avoid assigning the
same webhook as a media to different users as doing so may cause webhook
errors (applies to the majority of webhooks that utilize *Include event
menu entry* option). In this case, the best practice is to create a
dedicated user to represent the webhook:

1.  After configuring the webhook media type, go to the *Users →
    Users* section and create a dedicated Zabbix user to represent the
    webhook - for example, with a username *Slack* for the Slack
    webhook. All settings, except media, can be left at their defaults
    as this user will not be logging into Zabbix.
2.  In the user profile, go to a tab *Media* and [add a
    webhook](/manual/config/notifications/media#user_media) with the
    required contact information. If the webhook does not use a *Send
    to* field, enter any combination of supported characters to bypass
    validation requirements.
3.  Grant this user at least read
    [permissions](/manual/config/users_and_usergroups/permissions#permissions_to_host_groups)
    to all hosts for which it should send the alerts.

When configuring alert action, add this user in the *Send to users*
field in Operation details - this will tell Zabbix to use the webhook
for notifications from this action.</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/config/notifications/media/webhook.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="ac89791d" xml:space="preserve">
        <source>#### Configuring alert actions

Actions determine which notifications should be sent via the webhook.
Steps for [configuring actions](/manual/config/notifications/action)
involving webhooks are the same as for all other media types with these
exceptions:

-   If a webhook uses tags to store ticket\\message ID and to follow up
    with update\\resolve operations, this webhook should not be used in
    several alert actions for a single problem event. If
    {EVENT.TAGS.&lt;name&gt;} already exists, and is updated in the
    webhook, then its resulting value is not defined. For such a case, a
    new tag name should be used in the webhook to store updated values.
    This applies to Jira, Jira Service Desk, Mattermost, Opsgenie, OTRS,
    Redmine, ServiceNow, Slack, Zammad, and Zendesk webhooks provided by
    Zabbix and to the majority of webhooks that utilize *Include event
    menu entry* option. Using the webhook in several operations is
    allowed if those operations or escalation steps belong to the same
    action. It is also ok to use this webhook in different actions if
    the actions will not be applied to the same problem event due to
    different filter conditions.
-   When using a webhook in actions for [internal
    events](/manual/config/events/sources#internal_events): in the
    action operation configuration, check the *Custom message* checkbox
    and define the custom message, otherwise, a notification will not be
    sent.</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/config/notifications/media/webhook.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
    </body>
  </file>
</xliff>
