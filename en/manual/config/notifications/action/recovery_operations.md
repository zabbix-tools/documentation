[comment]: # ({10ba847f-10ba847f})
# 3 Recovery operations

[comment]: # ({/10ba847f-10ba847f})

[comment]: # ({5e9fcb63-e5432fa9})
### Overview

Recovery operations allow you to be notified when problems are resolved.

Both messages and remote commands are supported in recovery operations.
While several operations can be added, escalation is not supported - all
operations are assigned to a single step and therefore will be performed
simultaneously.

[comment]: # ({/5e9fcb63-e5432fa9})

[comment]: # ({1ba86115-960d3247})
### Use cases

Some use cases for recovery operations are as follows:

1.  Notify on a recovery all users that were notified on the problem:
    -  Select *Notify all involved* as operation type.

2.  Have multiple operations upon recovery: send a notification and execute a remote command:
    -  Add operation types for sending a message and executing a command.

3.  Open a ticket in external helpdesk/ticketing system and close it when the problem is resolved:
    -  Create an external script that communicates with the helpdesk system.
    -  Create an action having operation that executes this script and thus opens a ticket.
    -  Have a recovery operation that executes this script with other parameters and closes the ticket.
    -  Use the {EVENT.ID} macro to reference the original problem.

[comment]: # ({/1ba86115-960d3247})

[comment]: # ({1d47a414-82501aec})
### Configuring a recovery operation

To configure a recovery operation, go to the *Operations* tab in
[action](/manual/config/notifications/action) configuration.

![](../../../../../assets/en/manual/config/notifications/action_operation.png){width="600"}

To configure details of a new recovery operation,
click on ![](../../../../../assets/en/manual/config/add_link.png) in the *Recovery operations* block.
To edit an existing operation,
click on ![](../../../../../assets/en/manual/config/edit_link.png) next to the operation.
A pop-up window will open where you can edit the operation step details.

[comment]: # ({/1d47a414-82501aec})

[comment]: # ({73596c26-f2127d8b})
#### Recovery operation details

![](../../../../../assets/en/manual/config/recovery_operation_details.png){width="600"}

Three operation types are available for recovery events:

-   **Send message** - send recovery message to specified user.
-   **Notify all involved** - send recovery message to all users who were notified on the problem event.
-   **<remote command name>** - execute a remote command. Commands are available for execution if previously defined in [global scripts](/manual/web_interface/frontend_sections/alerts/scripts#configuring_a_global_script) with *Action operation* selected as its scope.

Parameters for each operation type are described below. All mandatory input fields are marked with a red asterisk.
When done, click on *Add* to add operation to the list of *Recovery operations*.

::: noteclassic
Note that if the same recipient is defined in several operation types without specified *Custom message*,
duplicate notifications are not sent.
:::

[comment]: # ({/73596c26-f2127d8b})

[comment]: # ({0d57f423-7dbd8d4d})

#### Operation type: [send message](/manual/config/notifications/action/operation/message)

|Parameter|<|Description|
|-|--|---------------------------|
|*Send to user groups*|<|Select user groups to send the recovery message to.<br>The user group must have at least "read" [permissions](/manual/config/users_and_usergroups/permissions) to the host in order to be notified.|
|*Send to users*|<|Select users to send the recovery message to.<br>The user must have at least "read" [permissions](/manual/config/users_and_usergroups/permissions) to the host in order to be notified.|
|*Send only to*|<|Send default recovery message to all defined media types or a selected one only.|
|*Custom message*|<|If selected, a custom message can be defined.|
| |*Subject*|Subject of the custom message. The subject may contain macros.|
|^|*Message*|The custom message. The message may contain macros.|

[comment]: # ({/0d57f423-7dbd8d4d})

[comment]: # ({df960477-37fc532b})

#### Operation type: [remote command](/manual/config/notifications/action/operation/remote_command)

|Parameter|Description|
|--|--------|
|*Target list*|Select targets to execute the command on:<br>**Current host** - command is executed on the host of the trigger that caused the problem event. This option will not work if there are multiple hosts in the trigger.<br>**Host** - select host(s) to execute the command on.<br>**Host group** - select host group(s) to execute the command on. Specifying a parent host group implicitly selects all nested host groups. Thus the remote command will also be executed on hosts from nested groups.<br>A command on a host is executed only once, even if the host matches more than once (e.g. from several host groups; individually and from a host group).<br>The target list is meaningless if the command is executed on Zabbix server. Selecting more targets in this case only results in the command being executed on the server more times.<br>Note that for global scripts, the target selection also depends on the *Host group* setting in global script [configuration](/manual/web_interface/frontend_sections/alerts/scripts#configuring_a_global_script).|

[comment]: # ({/df960477-37fc532b})

[comment]: # ({21d4c855-945f9a49})

#### Operation type: notify all involved

|Parameter|<|Description|
|-|--|-----------------|
|*Custom message*|<|If selected, a custom message can be defined.|
| |*Subject*|Subject of the custom message. The subject may contain macros.|
|^|*Message*|The custom message. The message may contain macros.|


[comment]: # ({/21d4c855-945f9a49})
