[comment]: # ({6eb0119d-6eb0119d})
# 4 Update operations

[comment]: # ({/6eb0119d-6eb0119d})

[comment]: # ({ab000832-68ae5c34})
#### Overview

Update operations are available in actions with the following event
sources:

-   *Triggers* - when problems are
    [updated](/manual/acknowledgment#updating_problems) by other users,
    i.e. commented upon, acknowledged, severity has been changed, closed
    (manually);
-   *Services* - when the severity of a service has changed but the
    service is still not recovered.

Both messages and remote commands are supported in update operations.
While several operations can be added, escalation is not supported - all
operations are assigned to a single step and therefore will be performed
simultaneously.

[comment]: # ({/ab000832-68ae5c34})

[comment]: # ({7d869f97-1379ee27})
#### Configuring an update operation

To configure an update operation go to the *Operations* tab in action
[configuration](/manual/config/notifications/action).

![](../../../../../assets/en/manual/config/notifications/action_operation.png){width="600"}

To configure details of a new update operation,
click on ![](../../../../../assets/en/manual/config/add_link.png) in the *Update operations* block.
To edit an existing operation,
click on ![](../../../../../assets/en/manual/config/edit_link.png) next to the operation.
A pop-up window will open where you can edit the operation step details.

[comment]: # ({/7d869f97-1379ee27})

[comment]: # ({bde6d30a-175ae116})
#### Update operation details

![](../../../../../assets/en/manual/config/update_operation_details.png){width="600"}

Update operations offer the same set of parameters as [Recovery operations](/manual/config/notifications/action/recovery_operations#Recovery_operation_details).

[comment]: # ({/bde6d30a-175ae116})
