[comment]: # ({0cdc5e2e-0cdc5e2e})
# 2 Remote commands

[comment]: # ({/0cdc5e2e-0cdc5e2e})

[comment]: # ({36945095-36945095})
#### Overview

With remote commands you can define that a certain pre-defined command
is automatically executed on the monitored host upon some condition.

Thus remote commands are a powerful mechanism for smart pro-active
monitoring.

In the most obvious uses of the feature you can try to:

-   Automatically restart some application (web server, middleware, CRM)
    if it does not respond
-   Use IPMI 'reboot' command to reboot some remote server if it does
    not answer requests
-   Automatically free disk space (removing older files, cleaning /tmp)
    if running out of disk space
-   Migrate a VM from one physical box to another depending on the CPU
    load
-   Add new nodes to a cloud environment upon insufficient CPU (disk,
    memory, whatever) resources

Configuring an action for remote commands is similar to that for sending
a message, the only difference being that Zabbix will execute a command
instead of sending a message.

Remote commands can be executed by Zabbix server, proxy or agent. Remote
commands on Zabbix agent can be executed directly by Zabbix server or
through Zabbix proxy. Both on Zabbix agent and Zabbix proxy remote
commands are disabled by default. They can be enabled by:

-   adding an `AllowKey=system.run[*]` parameter in agent configuration;
-   setting the EnableRemoteCommands parameter to '1' in proxy
    configuration.

Remote commands executed by Zabbix server are run as described in
[Command execution](/manual/appendix/command_execution) including exit
code checking.

Remote commands are executed even if the target host is in maintenance.

[comment]: # ({/36945095-36945095})

[comment]: # ({7ca4c9c3-7ca4c9c3})
##### Remote command limit

Remote command limit after resolving all macros depends on the type of
database and character set (non- ASCII characters require more than one
byte to be stored):

|   |   |   |
|---|---|---|
|*Database*|*Limit in characters*|*Limit in bytes*|
|**MySQL**|65535|65535|
|**Oracle Database**|2048|4000|
|**PostgreSQL**|65535|not limited|
|**SQLite (only Zabbix proxy)**|65535|not limited|

The following tutorial provides step-by-step instructions on how to set
up remote commands.

[comment]: # ({/7ca4c9c3-7ca4c9c3})

[comment]: # ({36bf70c4-349859a6})
#### Configuration

Those remote commands that are executed on Zabbix agent (custom scripts)
must be first enabled in the agent
[configuration](/manual/appendix/config/zabbix_agentd).

Make sure that the AllowKey=system.run[<command>,\*] parameter is added for each allowed command in agent configuration to allow specific command with nowait mode. Restart agent daemon if changing this parameter.

Then, when configuring a new action in *Alerts → Actions → Trigger actions*:

1.  Define the appropriate conditions,
    for example, set that the action is activated upon any disaster problems with one of Apache applications.

![](../../../../../../assets/en/manual/config/notifications/action/conditions_restart.png){width="600"}

2.  In the [*Operations*](/manual/config/notifications/action/operation#configuring_an_operation) tab,
    click on *Add* in the *Operations*, *Recovery operations*, or *Update operations* block.

![](../../../../../../assets/en/manual/config/notifications/action/operations.png){width="600"}

3.  Select one of the predefined scripts from the *Operation* dropdown list and set the *Target list* for the script.

![](../../../../../../assets/en/manual/config/notifications/action/operation_restart_webserver.png){width="600"}

[comment]: # ({/36bf70c4-349859a6})

[comment]: # ({382693c2-c5296379})
#### Predefined scripts

Scripts that are available for action operations (webhook, script, SSH, Telnet, IPMI) are defined in 
[global scripts](/manual/web_interface/frontend_sections/alerts/scripts).

For example:

    sudo /etc/init.d/apache restart 

In this case, Zabbix will try to restart an Apache process. With this
command, make sure that the command is executed on Zabbix agent (click
the *Zabbix agent* button against *Execute on*).

::: noteimportant
Note the use of **sudo** - Zabbix user does not
have permissions to restart system services by default. See below for
hints on how to configure **sudo**.
:::

::: noteclassic
Starting with Zabbix agent 7.0, remote commands can also be executed on an agent that is operating in active mode.
Zabbix agent - whether active or passive - should run on the remote host, and executes the commands in background.
:::

Remote commands on Zabbix agent are executed without timeout by the
system.run\[,nowait\] key and are not checked for execution results. On
Zabbix server and Zabbix proxy, remote commands are executed with
timeout as set in the TrapperTimeout parameter of zabbix\_server.conf or
zabbix\_proxy.conf file and are
[checked](/manual/appendix/command_execution#exit_code_checking) for
execution results. For additional information, see [*Script timeout*](/manual/web_interface/frontend_sections/alerts/scripts#script-timeout).

[comment]: # ({/382693c2-c5296379})

[comment]: # ({69bb3fd0-a1b1e7ea})
#### Access permissions

Make sure that the 'zabbix' user has execute permissions for configured
commands. One may be interested in using **sudo** to give access to
privileged commands. To configure access, execute as root:

    visudo

Example lines that could be used in *sudoers* file:

    # allows 'zabbix' user to run all commands without password.
    zabbix ALL=NOPASSWD: ALL

    # allows 'zabbix' user to restart apache without password.
    zabbix ALL=NOPASSWD: /etc/init.d/apache restart

::: notetip
On some systems *sudoers* file will prevent non-local
users from executing commands. To change this, comment out
**requiretty** option in */etc/sudoers*.
:::

[comment]: # ({/69bb3fd0-a1b1e7ea})

[comment]: # ({7726777a-1f3becef})
#### Remote commands with multiple interfaces

If the target system has multiple interfaces of the selected type
(Zabbix agent or IPMI), remote commands will be executed on the default
interface.

It is possible to execute remote commands via SSH and Telnet using
another interface than the Zabbix agent one. The available interface to
use is selected in the following order:

* Zabbix agent default interface
* SNMP default interface
* JMX default interface
* IPMI default interface

[comment]: # ({/7726777a-1f3becef})

[comment]: # ({26b199db-26b199db})
#### IPMI remote commands

For IPMI remote commands the following syntax should be used:

    <command> [<value>]

where

-   <command> - one of IPMI commands without spaces
-   <value> - 'on', 'off' or any unsigned integer. <value>
    is an optional parameter.

[comment]: # ({/26b199db-26b199db})

[comment]: # ({2fa683a2-c230845b})
#### Examples

Examples of [global
scripts](/manual/web_interface/frontend_sections/alerts/scripts#configuring_a_global_script)
that may be used as remote commands in action operations.

[comment]: # ({/2fa683a2-c230845b})

[comment]: # ({b762312c-1d68d18d})
##### Example 1

Restart of Windows on certain condition.

In order to automatically restart Windows upon a problem detected by
Zabbix, define the following script:

|Script parameter|Value|
|--|--------|
|*Scope*|'Action operation'|
|*Type*|'Script'|
|*Command*|c:\\windows\\system32\\shutdown.exe -r -f|

[comment]: # ({/b762312c-1d68d18d})

[comment]: # ({bdbb69b1-8e3d9a17})
##### Example 2

Restart the host by using IPMI control.

|Script parameter|Value|
|--|--------|
|*Scope*|'Action operation'|
|*Type*|'IPMI'|
|*Command*|reset|

[comment]: # ({/bdbb69b1-8e3d9a17})

[comment]: # ({474cb292-c9d4e7c6})
##### Example 3

Power off the host by using IPMI control.

|Script parameter|Value|
|--|--------|
|*Scope*|'Action operation'|
|*Type*|'IPMI'|
|*Command*|power off|

[comment]: # ({/474cb292-c9d4e7c6})
