[comment]: # ({66eca2c9-66eca2c9})
# 1 Sending message

[comment]: # ({/66eca2c9-66eca2c9})

[comment]: # ({a1d73332-a1d73332})
#### Overview

Sending a message is one of the best ways of notifying people about a
problem. That is why it is one of the primary actions offered by Zabbix.

[comment]: # ({/a1d73332-a1d73332})

[comment]: # ({76865189-2540ef35})
#### Configuration

To be able to send and receive notifications from Zabbix you have to:

-   [define the media](/manual/config/notifications/media) to send a
    message to


If the operation takes place outside of the [When active](/manual/config/notifications/media#user-media) time period 
defined for the selected media in the user configuration, the message will not be sent.

The default trigger severity ('Not classified')
**must be** checked in user media
[configuration](/manual/config/notifications/media/email#user_media) if
you want to receive notifications for non-trigger events such as
discovery, active agent autoregistration or internal evens.


-   [configure an action
    operation](/manual/config/notifications/action/operation) that sends
    a message to one of the defined media

::: noteimportant
Zabbix sends notifications only to those users
that have at least 'read' permissions to the host that generated the
event. At least one host of a trigger expression must be
accessible.
:::

You can configure custom scenarios for sending messages using
[escalations](/manual/config/notifications/action/escalations).

To successfully receive and read emails from Zabbix, email
servers/clients must support standard 'SMTP/MIME email' format since
Zabbix sends UTF-8 data (If the subject contains ASCII characters only,
it is not UTF-8 encoded.). The subject and the body of the message are
base64-encoded to follow 'SMTP/MIME email' format standard.

Message limit after all macros expansion is the same as message limit
for [Remote
commands](/manual/config/notifications/action/operation/remote_command).

[comment]: # ({/76865189-2540ef35})

[comment]: # ({c0442483-c0442483})
#### Tracking messages

You can view the status of messages sent in *Monitoring → Problems*.

In the *Actions* column you can see summarized information about actions
taken. In there green numbers represent messages sent, red ones - failed
messages. *In progress* indicates that an action is initiated. *Failed*
informs that no action has executed successfully.

If you click on the event time to view event details, you will also see
the *Message actions* block containing details of messages sent (or not
sent) due to the event.

In *Reports → Action log* you will see details of all actions taken for
those events that have an action configured.

[comment]: # ({/c0442483-c0442483})
