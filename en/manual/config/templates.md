[comment]: # (tags: template, templates)

[comment]: # ({1d542baa-bd7905d8})
# 8 Templates and template groups

[comment]: # ({/1d542baa-bd7905d8})

[comment]: # ({9acb176c-59f74d33})
#### Overview

The use of templates is an excellent way of reducing one's
workload and streamlining the Zabbix configuration. A template is a set of entities that can be conveniently applied to
multiple hosts. 

The entities may be:

-   items
-   triggers
-   graphs
-   dashboards
-   low-level discovery rules
-   web scenarios

As many hosts in real life are identical or fairly similar so it
naturally follows that the set of entities (items, triggers, graphs,...)
you have created for one host, may be useful for many. Of course, you
could copy them to each new host, but that would be a lot of manual
work. Instead, with templates you can copy them to one template and then
apply the template to as many hosts as needed.

When a template is linked to a host, all entities (items, triggers,
graphs,...) of the template are added to the host. Templates are
assigned to each individual host directly (and not to a host group).

Templates are often used to group entities for particular services or
applications (like Apache, MySQL, PostgreSQL, Postfix...) and then
applied to hosts running those services.

Another benefit of using templates is when something has to be changed
for all the hosts. Changing something on the template level once will
propagate the change to all the linked hosts.

Templates are organized in template [groups](/manual/config/templates/template#creating-a-template-group).


Proceed to [creating and configuring a
template](/manual/config/templates/template).

[comment]: # ({/9acb176c-59f74d33})
