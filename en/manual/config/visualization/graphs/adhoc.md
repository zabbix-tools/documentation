[comment]: # ({c530550a-c530550a})
# 3 Ad-hoc graphs

[comment]: # ({/c530550a-c530550a})

[comment]: # ({c4395023-c4395023})
#### Overview

While a [simple graph](simple) is great for accessing data of one item
and [custom graphs](custom) offer customization options, none of the two
allow to quickly create a comparison graph for multiple items with
little effort and no maintenance.

To address this issue, since Zabbix 2.4 it is possible to create ad-hoc
graphs for several items in a very quick way.

[comment]: # ({/c4395023-c4395023})

[comment]: # ({43b7cbdf-43b7cbdf})
#### Configuration

To create an ad-hoc graph, do the following:

-   Go to *Monitoring* → *Latest data*
-   Use filter to display items that you want
-   Mark checkboxes of the items you want to graph
-   Click on *Display stacked graph* or *Display graph* buttons

![](../../../../../assets/en/manual/config/visualization/ad_hoc_graphs.png){width="600"}

Your graph is created instantly:

![](../../../../../assets/en/manual/config/visualization/ad_hoc_graph.png){width="600"}

Note that to avoid displaying too many lines in the graph, only the
average value for each item is displayed (min/max value lines are not
displayed). Triggers and trigger information is not displayed in the
graph.

In the created graph window you have the [time period
selector](/manual/config/visualization/graphs/simple#time_period_selector)
available and the possibility to switch from the "normal" line graph to
a stacked one (and back).

![](../../../../../assets/en/manual/config/visualization/ad_hoc_stacked.png){width="600"}

[comment]: # ({/43b7cbdf-43b7cbdf})
