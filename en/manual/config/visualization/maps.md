[comment]: # (tags: map)

[comment]: # ({2d9add76-2d9add76})
# 2 Network maps

[comment]: # ({/2d9add76-2d9add76})

[comment]: # ({e50ec238-e50ec238})
#### Overview

If you have a network to look after, you may want to have an overview of
your infrastructure somewhere. For that purpose, you can create maps in
Zabbix - of networks and of anything you like.

All users can create network maps. The maps can be public (available to
all users) or private (available to selected users).

Proceed to [configuring a network
map](/manual/config/visualization/maps/map).

[comment]: # ({/e50ec238-e50ec238})
