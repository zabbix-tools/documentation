[comment]: # ({38173e9c-38173e9c})
# 3 Link indicators

[comment]: # ({/38173e9c-38173e9c})

[comment]: # ({6eabc005-6eabc005})
#### Overview

You can assign some triggers to a
[link](/manual/config/visualization/maps/map#linking_elements) between
elements in a network map. When these triggers go into a problem state,
the link can reflect that.

When you configure a link, you set the default link type and color. When
you assign triggers to a link, you can assign different link types and
colors with these triggers.

Should any of these triggers go into a problem state, their link style
and color will be displayed on the link. So maybe your default link was
a green line. Now, with the trigger in the problem state, your link may
become bold red (if you have defined it so).

[comment]: # ({/6eabc005-6eabc005})

[comment]: # ({bcebbfa2-bcebbfa2})
#### Configuration

To assign triggers as link indicators, do the following:

-   select a map element
-   click on *Edit* in the *Links* section for the appropriate link
-   click on *Add* in the *Link indicators* block and select one or more
    triggers

![](../../../../../assets/en/manual/config/visualization/map_triggers.png){width="600"}

All mandatory input fields are marked with a red asterisk.

Added triggers can be seen in the *Link indicators* list.

You can set the link type and color for each trigger directly from the
list. When done, click on *Apply*, close the form and click on *Update*
to save the map changes.

[comment]: # ({/bcebbfa2-bcebbfa2})

[comment]: # ({41bb80f3-41bb80f3})
#### Display

In *Monitoring → Maps* the respective color will be displayed on the
link if the trigger goes into a problem state.

![](../../../../../assets/en/manual/config/visualization/map_problem.png)

::: noteclassic
If multiple triggers go into a problem state, the problem
with the highest severity will determine the link style and color. If
multiple triggers with the same severity are assigned to the same map
link, the one with the lowest ID takes precedence. Note also that:\
\
1. *Minimum trigger severity* and *Show suppressed problem* settings
from map configuration affect which problems are taken into account.\
2. In the case of triggers with multiple problems (multiple problem
generation), each problem may have a severity that differs from trigger
severity (changed manually), may have different tags (due to macros),
and may be suppressed.
:::

[comment]: # ({/41bb80f3-41bb80f3})
