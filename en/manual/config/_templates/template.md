[comment]: # ({3222767b-3222767b})
# 1 Configuring a template

[comment]: # ({/3222767b-3222767b})

[comment]: # ({630aea00-630aea00})
#### Overview

Configuring a template requires that you first create a template by
defining its general parameters and then you add entities (items,
triggers, graphs, etc.) to it.

[comment]: # ({/630aea00-630aea00})

[comment]: # ({6c97952a-de7b2ff6})
#### Creating a template

To create a template, do the following:

1. Go to *Data collection* → *Templates*.
2. Click on *Create template*.
3. Edit template attributes.

The **Templates** tab contains general template attributes.

![](../../../../assets/en/manual/config/template_a.png){width=600}

All mandatory input fields are marked with a red asterisk.

Template attributes:

|Parameter|Description|
|--|--------|
|*Template name*|Unique template name.<br>Alphanumerics, spaces, dots, dashes, and underscores are allowed.<br>Leading and trailing spaces are not allowed.|
|*Visible name*|If you set this name, it will be the one visible in lists, maps, etc.|
|*Templates*|Link one or more templates to this template. All entities (items, triggers, etc.) will be inherited from the linked templates.<br>To link a new template, start typing the template name in the *Link new templates* field. A list of matching templates will appear; scroll down to select. Alternatively, you may click on *Select* next to the field and select templates from the list in a pop-up window. The templates that are selected in the *Link new templates* field will be linked to the template when the template configuration form is saved or updated.<br>To unlink a template, use one of the two options in the *Linked templates* block:<br>*Unlink* - unlink the template, but preserve its entities (items, triggers, etc.);<br>*Unlink and clear* - unlink the template and remove all of its entities (items, triggers, etc.).|
|*Template groups*|Groups the template belongs to. |
|*Description*|Template description.|
|*Vendor and version*|Template vendor and version; displayed only when updating existing templates ([out-of-the-box templates](/manual/config/templates_out_of_the_box) provided by Zabbix, [imported templates](/manual/xml_export_import/templates#importing), or templates modified through the [Template API](/manual/api/reference/template)) if the template configuration contains such information.<br>Cannot be modified in Zabbix frontend.<br>For out-of-the-box templates, version is displayed as follows: major version of Zabbix, delimiter ("-"), revision number (increased with each new version of the template, and reset with each major version of Zabbix). For example, 6.4-0, 6.4-5, 7.0-0, 7.0-3.|

The **Tags** tab allows you to define template-level
[tags](/manual/config/tagging). All problems of hosts linked to this
template will be tagged with the values entered here.

![](../../../../assets/en/manual/config/template_c.png)

User macros, {INVENTORY.\*} macros, {HOST.HOST}, {HOST.NAME},
{HOST.CONN}, {HOST.DNS}, {HOST.IP}, {HOST.PORT} and {HOST.ID} macros are
supported in tags.

The **Macros** tab allows you to define template-level [user
macros](/manual/config/macros/user_macros) as a name-value pairs. Note
that macro values can be kept as plain text, secret text, or Vault
secret. Adding a description is also supported.

![](../../../../assets/en/manual/config/template_d1.png)

If you select the *Inherited and template macros* option, you may also view macros from linked templates and global macros that the template will inherit, as well as the values that the macros will resolve to.

![](../../../../assets/en/manual/config/template_d2.png)

For convenience, links to the respective templates, as well as a link to global macro configuration is provided.
It is also possible to edit a linked template macro or global macro on the template level, effectively creating a copy of the macro on the template.

The **Value mapping** tab allows to configure human-friendly
representation of item data in [value
mappings](/manual/config/items/mapping).

Buttons:

|   |   |
|--|--------|
|![](../../../../assets/en/manual/config/button_add.png)|Add the template. The added template should appear in the list.|
|![](../../../../assets/en/manual/config/button_update.png)|Update the properties of an existing template.|
|![](../../../../assets/en/manual/config/button_clone.png)|Create another template based on the properties of the current template, including the entities (items, triggers, etc.) both inherited from linked templates and directly attached to the current template, but excluding template vendor and version.|
|![](../../../../assets/en/manual/config/button_delete.png)|Delete the template; entities of the template (items, triggers, etc.) remain with the linked hosts.|
|![](../../../../assets/en/manual/config/button_clear.png)|Delete the template and all its entities from linked hosts.|
|![](../../../../assets/en/manual/config/button_cancel.png)|Cancel the editing of template properties.|

[comment]: # ({/6c97952a-de7b2ff6})

[comment]: # ({518aed53-0d973911})
#### Adding items, triggers, graphs

::: noteimportant
Items have to be added to a template first.
Triggers and graphs cannot be added without the corresponding
item.
:::

There are two ways to add items to the template:\

1. To create new items, follow the guidelines for [Creating an item](/manual/config/items/item).\

2. To add existing items to the template:
- Go to *Data collection → Hosts* (or *Templates*).
- Click on *Items* in the row of the required host/template.
- Mark the checkboxes of items you want to add to the template.
- Click on *Copy* below the item list.
- Select the template (or group of templates) the items should be copied to and click on *Copy*.\
All the selected items should be copied to the template.

Adding triggers and graphs is done in a similar fashion (from the list
of triggers and graphs respectively), again, keeping in mind that they
can only be added if the required items are added first.

[comment]: # ({/518aed53-0d973911})

[comment]: # ({320136e2-b78a3626})
#### Adding dashboards

To add dashboards to a template in *Data collection* → *Templates*, do the
following:

1. Click on *Dashboards* in the row of the template.
2. Configure a dashboard following the guidelines of [configuring dashboards](/manual/web_interface/frontend_sections/dashboards).

::: noteimportant
When configuring widgets on a template dashboard (instead of a global dashboard), the host-related parameters are not available, and some parameters have a different label.
This is because template dashboards display data only from the host that the template is linked to.
For example, the parameters *Host groups*, *Exclude host groups* and *Hosts* in the [*Problems*](/manual/web_interface/frontend_sections/dashboards/widgets/problems) widget are not available,
the parameter *Host groups* in the [*Host availability*](/manual/web_interface/frontend_sections/dashboards/widgets/host_availability) widget is not available, and the parameter *Show hosts in maintenance* is renamed to *Show data in maintenance*, etc.
For more information on the availability of parameters in template dashboard widgets, see specific parameters for each [dashboard widget](/manual/web_interface/frontend_sections/dashboards/widgets).
:::

::: notetip
For details on accessing host dashboards that are
created from template dashboards, see the [host
dashboard](/manual/config/visualization/host_screens#accessing_host_dashboards)
section.
:::

[comment]: # ({/320136e2-b78a3626})

[comment]: # ({513e93e5-513e93e5})
#### Configuring low-level discovery rules

See the [low-level discovery](/manual/discovery/low_level_discovery)
section of the manual.

[comment]: # ({/513e93e5-513e93e5})

[comment]: # ({bc7c835f-e3935a9d})
#### Adding web scenarios

To add web scenarios to a template in *Data collection* → *Templates*, do
the following:

1. Click on *Web* in the row of the template.
2. Configure a web scenario following the usual method of [configuring web scenarios](/manual/web_monitoring#configuring_a_web_scenario).

[comment]: # ({/bc7c835f-e3935a9d})

[comment]: # ({cbd44921-d357b380})
#### Creating a template group

::: noteimportant
Only Super Admin users can create template groups.
:::

To create a template group in Zabbix frontend, do the following:

1. Go to: *Data collection → Template groups*.
2. Click on *Create template group* in the upper right corner of the screen.
3. Enter the group name in the form.

![](../../../../assets/en/manual/config/template_group.png)

To create a nested template group, use the '/' forward slash separator, for example `Linux servers/Databases/MySQL`. You can create this group even if none of the two parent template groups (`Linux servers/Databases/`) exist. In this case creating these parent template groups is up to the user; they will not be created automatically.<br>Leading and trailing slashes, several slashes in a row are not allowed. Escaping of '/' is not supported.

[comment]: # ({/cbd44921-d357b380})

[comment]: # ({f5312b20-ed4ea073})

Once the group is created, you can click on the group name in the list to edit group name, clone the group or set additional option:

![](../../../../assets/en/manual/config/template_group2.png)

*Apply permissions to all subgroups* - mark this checkbox and click on *Update* to apply the same level of permissions to all nested template groups. For user groups that may have had differing [permissions](/manual/config/users_and_usergroups/usergroup#configuration) assigned to nested template groups, the permission level of the parent template group will be enforced on the nested groups. This is a one-time option that is not saved in the database.

**Permissions to nested template groups:**

-   When creating a child template group to an existing parent template group,
    [user group](/manual/config/users_and_usergroups/usergroup)
    permissions to the child are inherited from the parent (for example,
    when creating `Databases/MySQL` if `Databases` already exists).
-   When creating a parent template group to an existing child template group,
    no permissions to the parent are set (for example, when creating
    `Databases` if `Databases/MySQL` already exists).

[comment]: # ({/f5312b20-ed4ea073})
