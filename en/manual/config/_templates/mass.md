[comment]: # ({bfcc3bc2-bfcc3bc2})
# 4 Mass update

[comment]: # ({/bfcc3bc2-bfcc3bc2})

[comment]: # ({cb0c0890-cb0c0890})
#### Overview

Sometimes you may want to change some attribute for a number of
templates at once. Instead of opening each individual template for
editing, you may use the mass update function for that.

[comment]: # ({/cb0c0890-cb0c0890})

[comment]: # ({b2510f6f-b2a07f91})
#### Using mass update

To mass-update some templates, do the following:

1. Mark the checkboxes before the templates you want to update in the [template list](/manual/web_interface/frontend_sections/data_collection/templates).
2. Click on *Mass update* below the list.
3. Navigate to the tab with required attributes (*Template*, *Tags*, *Macros* or *Value mapping*).
4. Mark the checkboxes of any attribute to update and enter a new value for them.

[comment]: # ({/b2510f6f-b2a07f91})

[comment]: # ({b3dbed52-be3b0440})

The **Template** tab contains general template mass update options.

![](../../../../assets/en/manual/config/templates/templ_mass.png){width=600}

The following options are available when selecting the respective button for the *Link templates* update:

-   *Link* - specify which additional templates to link;
-   *Replace* - specify which templates to link while at the same time unlinking any previously linked templates;
-   *Unlink* - specify which templates to unlink.

To specify the templates to link/unlink, start typing the template name in the auto-complete field until a dropdown appears offering the matching templates.
Just scroll down to select the required templates.

The *Clear when unlinking* option will allow to unlink any previously linked templates, as well as to remove all elements inherited from them (items, triggers, graphs, etc.).

The following options are available when selecting the respective button
for the *Template groups* update:

-   *Add* - allows to specify additional template groups from the existing ones or enter completely new template groups for the templates;
-   *Replace* - will remove the template from any existing template groups and replace them with the one(s) specified in this field (existing or new template groups);
-   *Remove* - will remove specific template groups from templates.

These fields are auto-complete - starting to type in them offers a
dropdown of matching template groups. If the template group is new, it also
appears in the dropdown and it is indicated by *(new)* after the string.
Just scroll down to select.

[comment]: # ({/b3dbed52-be3b0440})

[comment]: # ({db4e2e18-fd5d684f})

The **Tags** tab allows you to mass update template-level tags.

![](../../../../assets/en/manual/config/templates/templ_mass_c.png){width=600}

User macros, {INVENTORY.\*} macros, {HOST.HOST}, {HOST.NAME},
{HOST.CONN}, {HOST.DNS}, {HOST.IP}, {HOST.PORT} and {HOST.ID} macros are
supported in tags. Note that tags with the same name, but different
values are not considered 'duplicates' and can be added to the same
template.

[comment]: # ({/db4e2e18-fd5d684f})

[comment]: # ({f929dbf9-62e49505})

The **Macros** tab allows you to mass update template-level macros.

![](../../../../assets/en/manual/config/templates/templ_mass_d.png){width=600}

The following options are available when selecting the respective button
for macros update:

-   *Add* - allows to specify additional user macros for the templates.
    If *Update existing* checkbox is checked, value, type and
    description for the specified macro name will be updated. If
    unchecked, if a macro with that name already exist on the
    template(s), it will not be updated.
-   *Update* - will replace values, types and descriptions of macros
    specified in this list. If *Add missing* checkbox is checked, macro
    that didn't previously exist on a template will be added as new
    macro. If unchecked, only macros that already exist on a template
    will be updated.
-   *Remove* - will remove specified macros from templates. If *Except
    selected* box is checked, all macros except specified in the list
    will be removed. If unchecked, only macros specified in the list
    will be removed.
-   *Remove all* - will remove all user macros from templates. If *I
    confirm to remove all macros* checkbox is not checked, a new popup
    window will open asking to confirm removal of all macros.

[comment]: # ({/f929dbf9-62e49505})

[comment]: # ({ec6802ef-40ab4576})

The **Value mapping** tab allows you to mass update [value mappings](/manual/config/items/mapping).

![](../../../../assets/en/manual/config/templates/templ_mass_e.png){width=600}

Buttons with the following options are available for value map update:

-   *Add* - add value maps to the templates. If you mark *Update
    existing*, all properties of the value map with this name will be
    updated. Otherwise, if a value map with that name already exists, it
    will not be updated.
-   *Update* - update existing value maps. If you mark *Add missing*, a
    value map that didn't previously exist on a template will be added
    as a new value map. Otherwise only the value maps that already exist
    on a template will be updated.
-   *Rename* - give new name to an existing value map.
-   *Remove* - remove the specified value maps from the templates. If
    you mark *Except selected*, all value maps will be removed
    **except** the ones that are specified.
-   *Remove all* - remove all value maps from the templates. If the *I
    confirm to remove all value maps* checkbox is not marked, a new
    popup window will open asking to confirm the removal.

*Add from template* and *Add from host* options are available for value mapping add/update operations. 
They allow to select value mappings from a template or a host respectively.

[comment]: # ({/ec6802ef-40ab4576})

[comment]: # ({0c4810c5-84e57bb5})

When done with all required changes, click on *Update*. The attributes
will be updated accordingly for all the selected templates.

[comment]: # ({/0c4810c5-84e57bb5})
