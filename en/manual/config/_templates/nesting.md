[comment]: # ({5893b7b9-64738f4d})
# 3 Nesting

[comment]: # ({/5893b7b9-64738f4d})

[comment]: # ({d2187f57-186a70f3})
#### Overview

Nesting is a way of one template encompassing one or more other templates.

As it makes sense to separate out entities on individual templates for various services, applications, etc.,
you may end up with quite a few templates all of which may need to be linked to quite a few hosts.
To simplify the picture, it is possible to link some templates together in a single template.

The benefit of nesting is that you have to link only one template to the host,
and the host will automatically inherit all entities from the templates that are linked to the one template.
For example, if we link *T1* and *T2* to *T3*, we supplement *T3* with all entities from *T1* and *T2*, but not vice versa.
If we link *T1* to *T2* and *T3*, we supplement *T2* and *T3* with entities from *T1*.

[comment]: # ({/d2187f57-186a70f3})

[comment]: # ({e4830cd7-edef5103})
#### Configuring nested templates

To link templates, you need to take an existing template (or create a new one), and then:

1. Open the [template configuration form](/manual/config/templates/template#creating-a-template).
2. Find the *Templates* field.
3. Click on *Select* to open the *Templates* pop-up window.
4. In the pop-up window, choose the required templates, and then click on *Select* to add the templates to the list.
5. Click on *Add* or *Update* in the template configuration form.

Thus, all entities of the configured template, as well as all entities of linked templates will now appear in the template configuration.
This includes items, triggers, graphs, low-level discovery rules, and web scenarios, but excludes dashboards. 
However, linked template dashboards will, nevertheless, be inherited by hosts.

To unlink any of the linked templates, click on *Unlink* or *Unlink and clear* in the template configuration form, and then click on *Update*.

The *Unlink* option will simply remove the association with the linked template, while not removing any of its entities (items, triggers, etc.).

The *Unlink and clear* option will remove both the association with the linked template, as well as all its entities (items, triggers, etc.).

[comment]: # ({/e4830cd7-edef5103})
