[comment]: # ({5c86be0b-5c86be0b})
# 18. Web interface

[comment]: # ({/5c86be0b-5c86be0b})

[comment]: # ({3d6b6eb0-8e039e89})
### Overview

For an easy access to Zabbix from anywhere and from any platform, the
web-based interface is provided.

::: noteclassic
If using more than one frontend instance make sure that the
locales and libraries (LDAP, SAML etc.) are installed and configured
identically for all frontends.
:::

[comment]: # ({/3d6b6eb0-8e039e89})

[comment]: # ({6398acaf-278be5f6})
### Frontend help

A help link ![](../../assets/en/manual/web_interface/help_link.png) is provided in Zabbix frontend forms with direct links to the corresponding parts of the documentation.

[comment]: # ({/6398acaf-278be5f6})
