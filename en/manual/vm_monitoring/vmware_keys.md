[comment]: # attributes: notoc

[comment]: # ({962ef293-e03ef403})
# 1 VMware monitoring item keys

[comment]: # ({/962ef293-e03ef403})

[comment]: # ({c9e49cce-0c5b0b79})
### Overview

This page provides details on the simple checks that can be used to monitor [VMware environments](/manual/vm_monitoring). 
The metrics are grouped by the monitoring target.

[comment]: # ({/c9e49cce-0c5b0b79})

[comment]: # ({07445f60-33fb12b3})
### Supported item keys

The item keys are listed without parameters and additional information. Click on the item key to see the full details.

|Item key|Description|Item group|
|--|-------|-|
|[vmware.eventlog](#vmware.eventlog)|The VMware event log.|General service|
|[vmware.fullname](#vmware.fullname)|The VMware service full name.|^|
|[vmware.version](#vmware.version)|The VMware service version.|^|
|[vmware.cl.perfcounter](#vmware.cl.perfcounter)|The VMware cluster performance counter metrics.|Cluster|
|[vmware.cluster.alarms.get](#vmware.cluster.alarms)|The VMware cluster alarms data. |^|
|[vmware.cluster.discovery](#vmware.cluster.discovery)|The discovery of VMware clusters.|^|
|[vmware.cluster.property](#vmware.cluster.property)|The VMware cluster property.|^|
|[vmware.cluster.status](#vmware.cluster.status)|The VMware cluster status.|^|
|[vmware.cluster.tags.get](#vmware.cluster.tags)|The VMware cluster tags array.|^|
|[vmware.datastore.alarms.get](#vmware.datastore.alarms)|The VMware datastore alarms data.|Datastore|
|[vmware.datastore.discovery](#vmware.datastore.discovery)|The discovery of VMware datastores.|^|
|[vmware.datastore.hv.list](#vmware.datastore.hv.list)|The list of datastore hypervisors.|^|
|[vmware.datastore.perfcounter](#vmware.datastore.perfcounter)|The VMware datastore performance counter value.|^|
|[vmware.datastore.property](#vmware.datastore.property)|The VMware datastore property.|^|
|[vmware.datastore.read](#vmware.datastore.read)|The amount of time for a read operation from the datastore.|^|
|[vmware.datastore.size](#vmware.datastore.size)|The VMware datastore space in bytes or in percentage from total.|^|
|[vmware.datastore.tags.get](#vmware.datastore.tags)|The VMware datastore tags array.|^|
|[vmware.datastore.write](#vmware.datastore.write)|The amount of time for a write operation to the datastore.|^|
|[vmware.dc.alarms.get](#vmware.dc.alarms)|The VMware datacenter alarms data.|Datacenter|
|[vmware.dc.discovery](#vmware.dc.discovery)|The discovery of VMware datacenters.|^|
|[vmware.dc.tags.get](#vmware.dc.tags)|The VMware datacenter tags array.|^|
|[vmware.dvswitch.discovery](#vmware.dvswitch.discovery)|The discovery of VMware vSphere Distributed Switches.|vSphere Distributed Switch|
|[vmware.dvswitch.fetchports.get](#vmware.dvswitch.fetchports)|The VMware vSphere Distributed Switch ports data.|^|
|[vmware.hv.alarms.get](#vmware.hv.alarms)|The VMware hypervisor alarms data.|Hypervisor|
|[vmware.hv.cluster.name](#vmware.hv.cluster)|The VMware hypervisor cluster name.|^|
|[vmware.hv.connectionstate](#vmware.hv.connectionstate)|The VMware hypervisor connection state.|^|
|[vmware.hv.cpu.usage](#vmware.hv.cpu)|The VMware hypervisor processor usage (Hz).|^|
|[vmware.hv.cpu.usage.perf](#vmware.hv.cpu.perf)|The VMware hypervisor processor usage as a percentage during the interval.|^|
|[vmware.hv.cpu.utilization](#vmware.hv.cpu.utilization)|The VMware hypervisor processor usage as a percentage during the interval, depends on power management or HT.|^|
|[vmware.hv.datacenter.name](#vmware.hv.datacenter)|The VMware hypervisor datacenter name.|^|
|[vmware.hv.datastore.discovery](#vmware.hv.datastore.discovery)|The discovery of VMware hypervisor datastores.|^|
|[vmware.hv.datastore.list](#vmware.hv.datastore.list)|The list of VMware hypervisor datastores.|^|
|[vmware.hv.datastore.multipath](#vmware.hv.datastore.multipath)|The number of available datastore paths.|^|
|[vmware.hv.datastore.read](#vmware.hv.datastore.read)|The average amount of time for a read operation from the datastore.|^|
|[vmware.hv.datastore.size](#vmware.hv.datastore.size)|The VMware datastore space in bytes or in percentage from total.|^|
|[vmware.hv.datastore.write](#vmware.hv.datastore.write)|The average amount of time for a write operation to the datastore.|^|
|[vmware.hv.discovery](#vmware.hv.discovery)|The discovery of VMware hypervisors.|^|
|[vmware.hv.diskinfo.get](#vmware.hv.diskinfo)|The VMware hypervisor disk data.|^|
|[vmware.hv.fullname](#vmware.hv.fullname)|The VMware hypervisor name.|^|
|[vmware.hv.hw.cpu.freq](#vmware.hv.hw.cpu.freq)|The VMware hypervisor processor frequency.|^|
|[vmware.hv.hw.cpu.model](#vmware.hv.hw.cpu.model)|The VMware hypervisor processor model.|^|
|[vmware.hv.hw.cpu.num](#vmware.hv.hw.cpu.num)|The number of processor cores on VMware hypervisor.|^|
|[vmware.hv.hw.cpu.threads](#vmware.hv.hw.cpu.threads)|The number of processor threads on VMware hypervisor.|^|
|[vmware.hv.hw.memory](#vmware.hv.hw.memory)|The VMware hypervisor total memory size.|^|
|[vmware.hv.hw.model](#vmware.hv.hw.model)|The VMware hypervisor model.|^|
|[vmware.hv.hw.sensors.get](#vmware.hv.hw.sensors)|The VMware hypervisor hardware sensors value.|^|
|[vmware.hv.hw.serialnumber](#vmware.hv.hw.serialnumber)|The VMware hypervisor serial number.|^|
|[vmware.hv.hw.uuid](#vmware.hv.hw.uuid)|The VMware hypervisor BIOS UUID.|^|
|[vmware.hv.hw.vendor](#vmware.hv.hw.vendor)|The VMware hypervisor vendor name.|^|
|[vmware.hv.maintenance](#vmware.hv.maintenance)|The VMware hypervisor maintenance status.|^|
|[vmware.hv.memory.size.ballooned](#vmware.hv.memory.size.ballooned)|The VMware hypervisor ballooned memory size.|^|
|[vmware.hv.memory.used](#vmware.hv.memory.used)|The VMware hypervisor used memory size.|^|
|[vmware.hv.net.if.discovery](#vmware.hv.net.if.discovery)|The discovery of VMware hypervisor network interfaces.|^|
|[vmware.hv.network.in](#vmware.hv.network.in)|The VMware hypervisor network input statistics.|^|
|[vmware.hv.network.linkspeed](#vmware.hv.network.linkspeed)|The VMware hypervisor network interface speed.|^|
|[vmware.hv.network.out](#vmware.hv.network.out)|The VMware hypervisor network output statistics.|^|
|[vmware.hv.perfcounter](#vmware.hv.perfcounter)|The VMware hypervisor performance counter value.|^|
|[vmware.hv.property](#vmware.hv.property)|The VMware hypervisor property.|^|
|[vmware.hv.power](#vmware.hv.power)|The VMware hypervisor power usage.|^|
|[vmware.hv.sensor.health.state](#vmware.hv.sensor.health)|The VMware hypervisor health state rollup sensor.|^|
|[vmware.hv.sensors.get](#vmware.hv.sensors)|The VMware hypervisor HW vendor state sensors.|^|
|[vmware.hv.status](#vmware.hv.status)|The VMware hypervisor status.|^|
|[vmware.hv.tags.get](#vmware.hv.tags)|The VMware hypervisor tags array.|^|
|[vmware.hv.uptime](#vmware.hv.uptime)|The VMware hypervisor uptime.|^|
|[vmware.hv.version](#vmware.hv.version)|The VMware hypervisor version.|^|
|[vmware.hv.vm.num](#vmware.hv.vm.num)|The number of virtual machines on the VMware hypervisor.|^|
|[vmware.rp.cpu.usage](#vmware.rp.cpu)|The CPU usage in hertz during the interval on VMware Resource Pool.|Resource pool|
|[vmware.rp.memory](#vmware.rp.memory)|The memory metrics of VMware resource pool.|^|
|[vmware.alarms.get](#vmware.alarms)|The VMware virtual center alarms data.|Virtual center|
|[vmware.vm.alarms.get](#vmware.vm.alarms)|The VMware virtual machine alarms data.|Virtual machine|
|[vmware.vm.attribute](#vmware.vm.attribute)|The VMware virtual machine custom attribute value.|^|
|[vmware.vm.cluster.name](#vmware.vm.cluster.name)|The VMware virtual machine name.|^|
|[vmware.vm.consolidationneeded](#vmware.vm.consolidationneeded)|The VMware virtual machine disk requires consolidation.|^|
|[vmware.vm.cpu.latency](#vmware.vm.cpu.latency)|The percentage of time the virtual machine is unable to run because it is contending for access to the physical CPU(s).|^|
|[vmware.vm.cpu.num](#vmware.vm.cpu.num)|The number of processors on VMware virtual machine.|^|
|[vmware.vm.cpu.readiness](#vmware.vm.cpu.readiness)|The percentage of time that the virtual machine was ready, but could not get scheduled to run on the physical CPU.|^|
|[vmware.vm.cpu.ready](#vmware.vm.cpu.ready)|The time that the virtual machine was ready, but could not get scheduled to run on the physical CPU.|^|
|[vmware.vm.cpu.swapwait](#vmware.vm.cpu.swapwait)|The percentage of CPU time spent waiting for swap-in.|^|
|[vmware.vm.cpu.usage](#vmware.vm.cpu.usage)|The VMware virtual machine processor usage (Hz).|^|
|[vmware.vm.cpu.usage.perf](#vmware.vm.cpu.usage.perf)|The VMware virtual machine processor usage as a percentage during the interval.|^|
|[vmware.vm.datacenter.name](#vmware.vm.datacenter.name)|The VMware virtual machine datacenter name.|^|
|[vmware.vm.discovery](#vmware.vm.discovery)|The discovery of VMware virtual machines.|^|
|[vmware.vm.guest.memory.size.swapped](#vmware.vm.guest.memory.size.swapped)|The amount of guest physical memory that is swapped out to the swap space.|^|
|[vmware.vm.guest.osuptime](#vmware.vm.guest.osuptime)|The total time elapsed since the last operating system boot-up.|^|
|[vmware.vm.hv.name](#vmware.vm.hv.name)|The VMware virtual machine hypervisor name.|^|
|[vmware.vm.memory.size](#vmware.vm.memory.size)|The VMware virtual machine total memory size.|^|
|[vmware.vm.memory.size.ballooned](#vmware.vm.memory.size.ballooned)|The VMware virtual machine ballooned memory size.|^|
|[vmware.vm.memory.size.compressed](#vmware.vm.memory.size.compressed)|The VMware virtual machine compressed memory size.|^|
|[vmware.vm.memory.size.consumed](#vmware.vm.memory.size.consumed)|The amount of host physical memory consumed for backing up guest physical memory pages.|^|
|[vmware.vm.memory.size.private](#vmware.vm.memory.size.private)|The VMware virtual machine private memory size.|^|
|[vmware.vm.memory.size.shared](#vmware.vm.memory.size.shared)|The VMware virtual machine shared memory size.|^|
|[vmware.vm.memory.size.swapped](#vmware.vm.memory.size.swapped)|The VMware virtual machine swapped memory size.|^|
|[vmware.vm.memory.size.usage.guest](#vmware.vm.memory.size.usage.guest)|The VMware virtual machine guest memory usage.|^|
|[vmware.vm.memory.size.usage.host](#vmware.vm.memory.size.usage.host)|The VMware virtual machine host memory usage.|^|
|[vmware.vm.memory.usage](#vmware.vm.memory.usage)|The percentage of host physical memory that has been consumed.|^|
|[vmware.vm.net.if.discovery](#vmware.vm.net.if.discovery)|The discovery of VMware virtual machine network interfaces.|^|
|[vmware.vm.net.if.in](#vmware.vm.net.if.in)|The VMware virtual machine network interface input statistics.|^|
|[vmware.vm.net.if.out](#vmware.vm.net.if.out)|The VMware virtual machine network interface output statistics.|^|
|[vmware.vm.net.if.usage](#vmware.vm.net.if.usage)|The VMware virtual machine network utilization during the interval.|^|
|[vmware.vm.perfcounter](#vmware.vm.perfcounter)|The VMware virtual machine performance counter value.|^|
|[vmware.vm.powerstate](#vmware.vm.powerstate)|The VMware virtual machine power state.|^|
|[vmware.vm.property](#vmware.vm.property)|The VMware virtual machine property.|^|
|[vmware.vm.snapshot.get](#vmware.vm.snapshot)|The VMware virtual machine snapshot state.|^|
|[vmware.vm.state](#vmware.vm.state)|The VMware virtual machine state.|^|
|[vmware.vm.storage.committed](#vmware.vm.storage.committed)|The VMware virtual machine committed storage space.|^|
|[vmware.vm.storage.readoio](#vmware.vm.storage.readoio)|The average number of outstanding read requests to the virtual disk during the collection interval.|^|
|[vmware.vm.storage.totalreadlatency](#vmware.vm.storage.totalreadlatency)|The average time a read from the virtual disk takes.|^|
|[vmware.vm.storage.totalwritelatency](#vmware.vm.storage.totalwritelatency)|The average time a write to the virtual disk takes.|^|
|[vmware.vm.storage.uncommitted](#vmware.vm.storage.uncommitted)|The VMware virtual machine uncommitted storage space.|^|
|[vmware.vm.storage.unshared](#vmware.vm.storage.unshared)|The VMware virtual machine unshared storage space.|^|
|[vmware.vm.storage.writeoio](#vmware.vm.storage.writeoio)|The average number of outstanding write requests to the virtual disk during the collection interval.|^|
|[vmware.vm.tags.get](#vmware.vm.tags)|The VMware virtual machine tags array.|^|
|[vmware.vm.tools](#vmware.vm.tools)|The VMware virtual machine guest tools state.|^|
|[vmware.vm.uptime](#vmware.vm.uptime)|The VMware virtual machine uptime.|^|
|[vmware.vm.vfs.dev.discovery](#vmware.vm.vfs.dev.discovery)|The discovery of VMware virtual machine disk devices.|^|
|[vmware.vm.vfs.dev.read](#vmware.vm.vfs.dev.read)|The VMware virtual machine disk device read statistics.|^|
|[vmware.vm.vfs.dev.write](#vmware.vm.vfs.dev.write)|The VMware virtual machine disk device write statistics.|^|
|[vmware.vm.vfs.fs.discovery](#vmware.vm.vfs.fs.discovery)|The discovery of VMware virtual machine file systems.|^|
|[vmware.vm.vfs.fs.size](#vmware.vm.vfs.fs.size)|The VMware virtual machine file system statistics.|^|

[comment]: # ({/07445f60-33fb12b3})

[comment]: # ({b1bc108a-0781738e})

### Item key details

Parameters without angle brackets are mandatory. Parameters marked with angle brackets **<** **>** are optional.

[comment]: # ({/b1bc108a-0781738e})

[comment]: # ({44039dbf-eb1a8260})

##### vmware.eventlog[url,<mode>] {#vmware.eventlog}

<br>
The VMware event log.<br>
Return value: *Log*.

Parameters:

-   **url** - the VMware service URL;
-   **mode** - *all* (default) or *skip* - skip the processing of older data.

Comments:

-   There must be only one `vmware.eventlog` item key per URL;
-   See also [example of filtering](/manual/config/items/preprocessing/examples#filtering_vmware_event_log_records) VMware event log records.

[comment]: # ({/44039dbf-eb1a8260})

[comment]: # ({629bc687-47230c57})

##### vmware.fullname[url] {#vmware.fullname}

<br>
The VMware service full name.<br>
Return value: *String*.

Parameters:

-   **url** - the VMware service URL.

[comment]: # ({/629bc687-47230c57})

[comment]: # ({dc0ba19f-40d29880})

##### vmware.version[url] {#vmware.version}

<br>
The VMware service version.<br>
Return value: *String*.

Parameters:

-   **url** - the VMware service URL.

[comment]: # ({/dc0ba19f-40d29880})

[comment]: # ({3ffcc17d-4a552773})

##### vmware.cl.perfcounter[url,id,path,<instance>] {#vmware.cl.perfcounter}

<br>
The VMware cluster performance counter metrics.<br>
Return value: *Integer*.

Parameters:

-   **url** - the VMware service URL;
-   **id** - the VMware cluster ID. `id` can be received from `vmware.cluster.discovery[]` as {#CLUSTER.ID}.
-   **path** - the performance counter path^**[1](vmware_keys#footnotes)**^;
-   **instance** - the performance counter instance.

[comment]: # ({/3ffcc17d-4a552773})

[comment]: # ({130b0206-6cd1a24f})

##### vmware.cluster.alarms.get[url,id] {#vmware.cluster.alarms}

<br>
The VMware cluster alarms data.<br>
Return value: [JSON object](/manual/vm_monitoring/vmware_json#vmware..alarms.get).

Parameters:

-   **url** - the VMware service URL;
-   **id** - the VMware cluster ID.

[comment]: # ({/130b0206-6cd1a24f})

[comment]: # ({aea52990-4b9278e5})

##### vmware.cluster.discovery[url] {#vmware.cluster.discovery}

<br>
The discovery of VMware clusters.<br>
Return value: [JSON object](/manual/vm_monitoring/discovery_fields).

Parameters:

-   **url** - the VMware service URL.

[comment]: # ({/aea52990-4b9278e5})

[comment]: # ({ac941582-690fc990})

##### vmware.cluster.property[url,id,prop] {#vmware.cluster.property}

<br>
The VMware cluster property.<br>
Return value: *String*.

Parameters:

-   **url** - the VMware service URL;
-   **id** - the VMware cluster ID;
-   **prop** - the property path.

[comment]: # ({/ac941582-690fc990})

[comment]: # ({e1e8fb62-1c5e3b36})

##### vmware.cluster.status[url,name] {#vmware.cluster.status}

<br>
The VMware cluster status.<br>
Return value: *0* - gray; *1* - green; *2* - yellow; *3* - red.

Parameters:

-   **url** - the VMware service URL;
-   **name** - the VMware cluster name.

[comment]: # ({/e1e8fb62-1c5e3b36})

[comment]: # ({11fcf972-5ae94c76})

##### vmware.cluster.tags.get[url,id] {#vmware.cluster.tags}

<br>
The VMware cluster tags array.<br>
Return value: [JSON object](/manual/vm_monitoring/vmware_json#vmware..tags.get).

Parameters:

-   **url** - the VMware service URL;
-   **id** - the VMware cluster ID.

This item works with vSphere 6.5 and newer.

[comment]: # ({/11fcf972-5ae94c76})

[comment]: # ({321c0c62-01134fff})

##### vmware.datastore.alarms.get[url,uuid] {#vmware.datastore.alarms}

<br>
The VMware datastore alarms data.<br>
Return value: [JSON object](/manual/vm_monitoring/vmware_json#vmware..alarms.get).

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware datastore global unique identifier.

[comment]: # ({/321c0c62-01134fff})

[comment]: # ({55b56caf-aab32d77})

##### vmware.datastore.discovery[url] {#vmware.datastore.discovery}

<br>
The discovery of VMware datastores.<br>
Return value: [JSON object](/manual/vm_monitoring/discovery_fields).

Parameters:

-   **url** - the VMware service URL.

[comment]: # ({/55b56caf-aab32d77})

[comment]: # ({f3c34291-db3f867b})

##### vmware.datastore.hv.list[url,datastore] {#vmware.datastore.hv.list}

<br>
The list of datastore hypervisors.<br>
Return value: *String*.

Parameters:

-   **url** - the VMware service URL;
-   **datastore** - the datastore name.

Output example:

    esx7-01-host.zabbix.sandbox
    esx7-02-host.zabbix.sandbox

[comment]: # ({/f3c34291-db3f867b})

[comment]: # ({9af1af0c-c35f9d68})

##### vmware.datastore.perfcounter[url,uuid,path,<instance>] {#vmware.datastore.perfcounter}

<br>
The VMware datastore performance counter value.<br>
Return value: *Integer* ^**[2](vmware_keys#footnotes)**^.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware datastore global unique identifier;
-   **path** - the performance counter path^**[1](vmware_keys#footnotes)**^;
-   **instance** - the performance counter instance. Use empty instance for aggregate values (default).

[comment]: # ({/9af1af0c-c35f9d68})

[comment]: # ({f82a95b3-84faeba8})

##### vmware.datastore.property[url,uuid,prop] {#vmware.datastore.property}

<br>
The VMware datastore property.<br>
Return value: *String*.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware datastore global unique identifier;
-   **prop** - the property path.

[comment]: # ({/f82a95b3-84faeba8})

[comment]: # ({144e403e-57bff0f5})

##### vmware.datastore.read[url,datastore,<mode>] {#vmware.datastore.read}

<br>
The amount of time for a read operation from the datastore (milliseconds).<br>
Return value: *Integer* ^**[2](vmware_keys#footnotes)**^.

Parameters:

-   **url** - the VMware service URL;
-   **datastore** - the datastore name;
-   **mode** - *latency* (average value, default) or *maxlatency* (maximum value).

[comment]: # ({/144e403e-57bff0f5})

[comment]: # ({3f6f7cc1-10d128fa})

##### vmware.datastore.size[url,datastore,<mode>] {#vmware.datastore.size}

<br>
The VMware datastore space in bytes or in percentage from total.<br>
Return value: *Integer* - for bytes; *Float* - for percentage.

Parameters:

-   **url** - the VMware service URL;
-   **datastore** - the datastore name;
-   **mode** - possible values: *total* (default), *free*, *pfree* (free percentage), *uncommitted*.

[comment]: # ({/3f6f7cc1-10d128fa})

[comment]: # ({18c56cf7-55fcd167})

##### vmware.datastore.tags.get[url,uuid] {#vmware.datastore.tags}

<br>
The VMware datastore tags array.<br>
Return value: [JSON object](/manual/vm_monitoring/vmware_json#vmware..tags.get).

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware datastore global unique identifier.

This item works with vSphere 6.5 and newer.

[comment]: # ({/18c56cf7-55fcd167})

[comment]: # ({4999547e-2afa2261})

##### vmware.datastore.write[url,datastore,<mode>] {#vmware.datastore.write}

<br>
The amount of time for a write operation to the datastore (milliseconds).<br>
Return value: *Integer* ^**[2](vmware_keys#footnotes)**^.

Parameters:

-   **url** - the VMware service URL;
-   **datastore** - the datastore name;
-   **mode** - *latency* (average value, default) or *maxlatency* (maximum value).

[comment]: # ({/4999547e-2afa2261})

[comment]: # ({fb840f26-d2405f1e})

##### vmware.dc.alarms.get[url,id] {#vmware.dc.alarms}

<br>
The VMware datacenter alarms data.<br>
Return value: [JSON object](/manual/vm_monitoring/vmware_json#vmware..alarms.get).

Parameters:

-   **url** - the VMware service URL;
-   **id** - the VMware datacenter ID.

[comment]: # ({/fb840f26-d2405f1e})

[comment]: # ({cfe9ae44-eb9cd94d})

##### vmware.dc.discovery[url] {#vmware.dc.discovery}

<br>
The discovery of VMware datacenters.<br>
Return value: [JSON object](/manual/vm_monitoring/discovery_fields).

Parameters:

-   **url** - the VMware service URL.

[comment]: # ({/cfe9ae44-eb9cd94d})

[comment]: # ({f43c6569-b2b4cdd1})

##### vmware.dc.tags.get[url,id] {#vmware.dc.tags}

<br>
The VMware datacenter tags array.<br>
Return value: [JSON object](/manual/vm_monitoring/vmware_json#vmware..tags.get).

Parameters:

-   **url** - the VMware service URL;
-   **id** - the VMware datacenter ID.

This item works with vSphere 6.5 and newer.

[comment]: # ({/f43c6569-b2b4cdd1})

[comment]: # ({69c24d0d-e8cdca74})

##### vmware.dvswitch.discovery[url] {#vmware.dvswitch.discovery}

<br>
The discovery of VMware vSphere Distributed Switches.<br>
Return value: [JSON object](/manual/vm_monitoring/discovery_fields).

Parameters:

-   **url** - the VMware service URL.

[comment]: # ({/69c24d0d-e8cdca74})

[comment]: # ({57925e30-61815421})

##### vmware.dvswitch.fetchports.get[url,uuid,<filter>,<mode>] {#vmware.dvswitch.fetchports}

<br>
The VMware vSphere Distributed Switch ports data.<br>
Return value: [JSON object](/manual/vm_monitoring/vmware_json#vmware.dvswitch.fetchports.get).

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware DVSwitch global unique identifier.
-   **filter** - a single string with comma-separated criteria for selecting ports;
-   **mode** - *state* (all XML without "config" XML nodes, default) or *full*.

The **filter** parameter supports the [criteria](https://vdc-download.vmware.com/vmwb-repository/dcr-public/bf660c0a-f060-46e8-a94d-4b5e6ffc77ad/208bc706-e281-49b6-a0ce-b402ec19ef82/SDK/vsphere-ws/docs/ReferenceGuide/vim.dvs.PortCriteria.html) available in the VMware data object DistributedVirtualSwitchPortCriteria.

Example:

    vmware.dvswitch.fetchports.get[{$VMWARE.URL},{$VMWARE.DVS.UUID},"active:true,connected:false,host:host-18,inside:true,nsxPort:true,uplinkPort:false",state]

[comment]: # ({/57925e30-61815421})

[comment]: # ({5a34a67a-b1788443})

##### vmware.hv.alarms.get[url,uuid] {#vmware.hv.alarms}

<br>
The VMware hypervisor alarms data.<br>
Return value: [JSON object](/manual/vm_monitoring/vmware_json#vmware..alarms.get).

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware hypervisor global unique identifier.

[comment]: # ({/5a34a67a-b1788443})

[comment]: # ({155c0eae-44ab2a58})

##### vmware.hv.cluster.name[url,uuid] {#vmware.hv.cluster}

<br>
The VMware hypervisor cluster name.<br>
Return value: *String*.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware hypervisor global unique identifier.

[comment]: # ({/155c0eae-44ab2a58})

[comment]: # ({b947645d-fc3c8b05})

##### vmware.hv.connectionstate[url,uuid] {#vmware.hv.connectionstate}

<br>
The VMware hypervisor connection state.<br>
Return value: *String*: *connected*, *disconnected*, or *notResponding*.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware hypervisor global unique identifier.

[comment]: # ({/b947645d-fc3c8b05})

[comment]: # ({d59e5b76-79a9b9a9})

##### vmware.hv.cpu.usage[url,uuid] {#vmware.hv.cpu}

<br>
The VMware hypervisor processor usage (Hz).<br>
Return value: *Integer*.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware hypervisor global unique identifier.

[comment]: # ({/d59e5b76-79a9b9a9})

[comment]: # ({3a33766e-ed709c0b})

##### vmware.hv.cpu.usage.perf[url,uuid] {#vmware.hv.cpu.perf}

<br>
The VMware hypervisor processor usage as a percentage during the interval.<br>
Return value: *Float*.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware hypervisor global unique identifier.

[comment]: # ({/3a33766e-ed709c0b})

[comment]: # ({6475f154-09ef4007})

##### vmware.hv.cpu.utilization[url,uuid] {#vmware.hv.cpu.utilization}

<br>
The VMware hypervisor processor usage as a percentage during the interval, depends on power management or HT.<br>
Return value: *Float*.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware hypervisor global unique identifier.

[comment]: # ({/6475f154-09ef4007})

[comment]: # ({1562d6e1-c0cc4f7c})

##### vmware.hv.datacenter.name[url,uuid] {#vmware.hv.datacenter}

<br>
The VMware hypervisor datacenter name.<br>
Return value: *String*.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware hypervisor global unique identifier.

[comment]: # ({/1562d6e1-c0cc4f7c})

[comment]: # ({692c0714-41c29bb3})

##### vmware.hv.datastore.discovery[url,uuid] {#vmware.hv.datastore.discovery}

<br>
The discovery of VMware hypervisor datastores.<br>
Return value: [JSON object](/manual/vm_monitoring/discovery_fields).

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware hypervisor global unique identifier.

[comment]: # ({/692c0714-41c29bb3})

[comment]: # ({6817c996-021b27bb})

##### vmware.hv.datastore.list[url,uuid] {#vmware.hv.datastore.list}

<br>
The list of VMware hypervisor datastores.<br>
Return value: *String*.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware hypervisor global unique identifier.

Output example:

    SSD-RAID1-VAULT1
    SSD-RAID1-VAULT2
    SSD-RAID10

[comment]: # ({/6817c996-021b27bb})

[comment]: # ({a94edc98-f091b3ef})

##### vmware.hv.datastore.multipath[url,uuid,<datastore>,<partitionid>] {#vmware.hv.datastore.multipath}

<br>
The number of available datastore paths.<br>
Return value: *Integer*.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware hypervisor global unique identifier;
-   **datastore** - the datastore name;
-   **partitionid** - the internal ID of physical device from `vmware.hv.datastore.discovery`.

[comment]: # ({/a94edc98-f091b3ef})

[comment]: # ({2c7b766f-d04716c2})

##### vmware.hv.datastore.read[url,uuid,datastore,<mode>] {#vmware.hv.datastore.read}

<br>
The average amount of time for a read operation from the datastore (milliseconds).<br>
Return value: *Integer* ^**[2](vmware_keys#footnotes)**^.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware hypervisor global unique identifier;
-   **datastore** - the datastore name;
-   **mode** - *latency* (default).

[comment]: # ({/2c7b766f-d04716c2})

[comment]: # ({a2bcb2dd-df2f59cb})

##### vmware.hv.datastore.size[url,uuid,datastore,<mode>] {#vmware.hv.datastore.size}

<br>
The VMware datastore space in bytes or in percentage from total.<br>
Return value: *Integer* - for bytes; *Float* - for percentage.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware hypervisor global unique identifier;
-   **datastore** - the datastore name;
-   **mode** - possible values: *total* (default), *free*, *pfree* (free percentage), *uncommitted*.

[comment]: # ({/a2bcb2dd-df2f59cb})

[comment]: # ({e778149b-dc35b4be})

##### vmware.hv.datastore.write[url,uuid,datastore,<mode>] {#vmware.hv.datastore.write}

<br>
The average amount of time for a write operation to the datastore (milliseconds).<br>
Return value: *Integer* ^**[2](vmware_keys#footnotes)**^.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware hypervisor global unique identifier;
-   **datastore** - the datastore name;
-   **mode** - *latency* (default).

[comment]: # ({/e778149b-dc35b4be})

[comment]: # ({6412aa8f-4e89c2a4})

##### vmware.hv.discovery[url] {#vmware.hv.discovery}

<br>
The discovery of VMware hypervisors.<br>
Return value: [JSON object](/manual/vm_monitoring/discovery_fields).

Parameters:

-   **url** - the VMware service URL.

[comment]: # ({/6412aa8f-4e89c2a4})

[comment]: # ({3f14528a-3ac4b78f})

##### vmware.hv.diskinfo.get[url,uuid] {#vmware.hv.diskinfo}

<br>
The VMware hypervisor disk data.<br>
Return value: [JSON object](/manual/vm_monitoring/vmware_json#vmware.hv.diskinfo.get).

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware hypervisor global unique identifier.

[comment]: # ({/3f14528a-3ac4b78f})

[comment]: # ({8cb34942-28eaca68})

##### vmware.hv.fullname[url,uuid] {#vmware.hv.fullname}

<br>
The VMware hypervisor name.<br>
Return value: *String*.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware hypervisor global unique identifier.

[comment]: # ({/8cb34942-28eaca68})

[comment]: # ({4ef60a8d-feec5b12})

##### vmware.hv.hw.cpu.freq[url,uuid] {#vmware.hv.hw.cpu.freq}

<br>
The VMware hypervisor processor frequency (Hz).<br>
Return value: *Integer*.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware hypervisor global unique identifier.

[comment]: # ({/4ef60a8d-feec5b12})

[comment]: # ({f27d9eaa-32246e63})

##### vmware.hv.hw.cpu.model[url,uuid] {#vmware.hv.hw.cpu.model}

<br>
The VMware hypervisor processor model.<br>
Return value: *String*.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware hypervisor global unique identifier.

[comment]: # ({/f27d9eaa-32246e63})

[comment]: # ({2c5312fe-c7e2a1a4})

##### vmware.hv.hw.cpu.num[url,uuid] {#vmware.hv.hw.cpu.num}

<br>
The number of processor cores on VMware hypervisor.<br>
Return value: *Integer*.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware hypervisor global unique identifier.

[comment]: # ({/2c5312fe-c7e2a1a4})

[comment]: # ({7b6f9aa3-2be1b516})

##### vmware.hv.hw.cpu.threads[url,uuid] {#vmware.hv.hw.cpu.threads}

<br>
The number of processor threads on VMware hypervisor.<br>
Return value: *Integer*.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware hypervisor global unique identifier.

[comment]: # ({/7b6f9aa3-2be1b516})

[comment]: # ({483171f8-7ff303a5})

##### vmware.hv.hw.memory[url,uuid] {#vmware.hv.hw.memory}

<br>
The VMware hypervisor total memory size (bytes).<br>
Return value: *Integer*.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware hypervisor global unique identifier.

[comment]: # ({/483171f8-7ff303a5})

[comment]: # ({66752463-797d6787})

##### vmware.hv.hw.model[url,uuid] {#vmware.hv.hw.model}

<br>
The VMware hypervisor model.<br>
Return value: *String*.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware hypervisor global unique identifier.

[comment]: # ({/66752463-797d6787})

[comment]: # ({edb032bc-c5e37a6f})

##### vmware.hv.hw.sensors.get[url,uuid] {#vmware.hv.hw.sensors}

<br>
The VMware hypervisor hardware sensors value.<br>
Return value: [JSON object](/manual/vm_monitoring/vmware_json#vmware.hv.hw.sensors.get).

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware hypervisor global unique identifier.

[comment]: # ({/edb032bc-c5e37a6f})

[comment]: # ({ed4aff03-3f95ebc4})

##### vmware.hv.hw.serialnumber[url,uuid] {#vmware.hv.hw.serialnumber}

<br>
The VMware hypervisor serial number.<br>
Return value: *String*.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware hypervisor global unique identifier.

This item works with vSphere API 6.7 and newer.

[comment]: # ({/ed4aff03-3f95ebc4})

[comment]: # ({d23e30dd-0e52ef18})

##### vmware.hv.hw.uuid[url,uuid] {#vmware.hv.hw.uuid}

<br>
The VMware hypervisor BIOS UUID.<br>
Return value: *String*.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware hypervisor global unique identifier.

This item works with vSphere API 6.7 and newer.

[comment]: # ({/d23e30dd-0e52ef18})

[comment]: # ({70ec69ef-07339f93})

##### vmware.hv.hw.vendor[url,uuid] {#vmware.hv.hw.vendor}

<br>
The VMware hypervisor vendor name.<br>
Return value: *String*.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware hypervisor global unique identifier.

This item works with vSphere API 6.7 and newer.

[comment]: # ({/70ec69ef-07339f93})

[comment]: # ({6616720a-69b3cb3d})

##### vmware.hv.maintenance[url,uuid] {#vmware.hv.maintenance}

<br>
The VMware hypervisor maintenance status.<br>
Return value: *0* - not in maintenance; *1* - in maintenance.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware hypervisor global unique identifier.

[comment]: # ({/6616720a-69b3cb3d})

[comment]: # ({11d12b22-9da13c63})

##### vmware.hv.memory.size.ballooned[url,uuid] {#vmware.hv.memory.size.ballooned}

<br>
The VMware hypervisor ballooned memory size (bytes).<br>
Return value: *Integer*.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware hypervisor global unique identifier.

[comment]: # ({/11d12b22-9da13c63})

[comment]: # ({78c91188-3bc8aa58})

##### vmware.hv.memory.used[url,uuid] {#vmware.hv.memory.used}

<br>
The VMware hypervisor used memory size (bytes).<br>
Return value: *Integer*.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware hypervisor global unique identifier.

[comment]: # ({/78c91188-3bc8aa58})

[comment]: # ({4dd0cfc7-9976b898})

##### vmware.hv.net.if.discovery[url,uuid] {#vmware.hv.net.if.discovery}

<br>
The discovery of VMware hypervisor network interfaces.<br>
Return value: *JSON object*.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware hypervisor global unique identifier.

[comment]: # ({/4dd0cfc7-9976b898})

[comment]: # ({98f321c8-3059d853})

##### vmware.hv.network.in[url,uuid,<mode>] {#vmware.hv.network.in}

<br>
The VMware hypervisor network input statistics (bytes per second).<br>
Return value: *Integer* ^**[2](vmware_keys#footnotes)**^.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware hypervisor global unique identifier;
-   **mode** - *bps* (default), *packets*, *dropped*, *errors*, *broadcast*.

[comment]: # ({/98f321c8-3059d853})

[comment]: # ({b4d1d682-3f2fd410})

##### vmware.hv.network.linkspeed[url,uuid,ifname] {#vmware.hv.network.linkspeed}

<br>
The VMware hypervisor network interface speed.<br>
Return value: *Integer*. Returns *0*, if the network interface is down, otherwise the speed value of the interface.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware hypervisor global unique identifier;
-   **ifname** - the interface name.

[comment]: # ({/b4d1d682-3f2fd410})

[comment]: # ({5d540cfc-01422615})

##### vmware.hv.network.out[url,uuid,<mode>] {#vmware.hv.network.out}

<br>
The VMware hypervisor network output statistics (bytes per second).<br>
Return value: *Integer* ^**[2](vmware_keys#footnotes)**^.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware hypervisor global unique identifier;
-   **mode** - *bps* (default), *packets*, *dropped*, *errors*, *broadcast*.

[comment]: # ({/5d540cfc-01422615})

[comment]: # ({95bee9fa-11cc123c})

##### vmware.hv.perfcounter[url,uuid,path,<instance>] {#vmware.hv.perfcounter}

<br>
The VMware hypervisor performance counter value.<br>
Return value: *Integer* ^**[2](vmware_keys#footnotes)**^.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware hypervisor global unique identifier;
-   **path** - the performance counter path ^**[1](vmware_keys#footnotes)**^;
-   **instance** - the performance counter instance. Use empty instance for aggregate values (default).

[comment]: # ({/95bee9fa-11cc123c})

[comment]: # ({74aeef79-782fdde9})

##### vmware.hv.property[url,uuid,prop] {#vmware.hv.property}

<br>
The VMware hypervisor property.<br>
Return value: *String*.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware hypervisor global unique identifier;
-   **prop** - the property path.

[comment]: # ({/74aeef79-782fdde9})

[comment]: # ({b94eccea-2f71e268})

##### vmware.hv.power[url,uuid,<max>] {#vmware.hv.power}

<br>
The VMware hypervisor power usage (W).<br>
Return value: *Integer*.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware hypervisor global unique identifier;
-   **max** - the maximum allowed power usage.

[comment]: # ({/b94eccea-2f71e268})

[comment]: # ({c40fd845-d238babe})

##### vmware.hv.sensor.health.state[url,uuid] {#vmware.hv.sensor.health}

<br>
The VMware hypervisor health state rollup sensor.<br>
Return value: *Integer*: *0* - gray; *1* - green; *2* - yellow; *3* - red.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware hypervisor global unique identifier.

Note that the item might not work in VMware vSphere 6.5 and newer, because VMware has deprecated the *VMware Rollup Health State* sensor.

[comment]: # ({/c40fd845-d238babe})

[comment]: # ({dcdadbe4-ece550c4})

##### vmware.hv.sensors.get[url,uuid] {#vmware.hv.sensors}

<br>
The VMware hypervisor HW vendor state sensors.<br>
Return value: [JSON object](/manual/vm_monitoring/vmware_json#vmware.hv.sensors.get).

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware hypervisor global unique identifier.

[comment]: # ({/dcdadbe4-ece550c4})

[comment]: # ({88ae2029-2fc73e5d})

##### vmware.hv.status[url,uuid] {#vmware.hv.status}

<br>
The VMware hypervisor status.<br>
Return value: *Integer*: *0* - gray; *1* - green; *2* - yellow; *3* - red.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware hypervisor global unique identifier.

This item uses the host system overall status property.

[comment]: # ({/88ae2029-2fc73e5d})

[comment]: # ({1339cb51-435e43e6})

##### vmware.hv.tags.get[url,uuid] {#vmware.hv.tags}

<br>
The VMware hypervisor tags array.<br>
Return value: [JSON object](/manual/vm_monitoring/vmware_json#vmware..tags.get).

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware hypervisor global unique identifier.

This item works with vSphere 6.5 and newer.

[comment]: # ({/1339cb51-435e43e6})

[comment]: # ({5df73a16-c86fa750})

##### vmware.hv.uptime[url,uuid] {#vmware.hv.uptime}

<br>
The VMware hypervisor uptime (seconds).<br>
Return value: *Integer*.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware hypervisor global unique identifier.

This item uses the host system overall status property.

[comment]: # ({/5df73a16-c86fa750})

[comment]: # ({4bbec7a9-043965f6})

##### vmware.hv.version[url,uuid] {#vmware.hv.version}

<br>
The VMware hypervisor version.<br>
Return value: *String*.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware hypervisor global unique identifier.

[comment]: # ({/4bbec7a9-043965f6})

[comment]: # ({5a25c3fe-23a77394})

##### vmware.hv.vm.num[url,uuid] {#vmware.hv.vm.num}

<br>
The number of virtual machines on the VMware hypervisor.<br>
Return value: *Integer*.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware hypervisor global unique identifier.

[comment]: # ({/5a25c3fe-23a77394})

[comment]: # ({798fc386-30b903c3})

##### vmware.rp.cpu.usage[url,rpid] {#vmware.rp.cpu}

<br>
The CPU usage in hertz during the interval on VMware Resource Pool.<br>
Return value: *Integer*.

Parameters:

-   **url** - the VMware service URL;
-   **rpid** - the VMware resource pool ID.

[comment]: # ({/798fc386-30b903c3})

[comment]: # ({7173bd06-f519c2b7})

##### vmware.rp.memory[url,rpid,<mode>] {#vmware.rp.memory}

<br>
The memory metrics of VMware resource pool.<br>
Return value: *Integer*.

Parameters:

-   **url** - the VMware service URL;
-   **rpid** - the VMware resource pool ID;
-   **mode** - possible values:<br>*consumed* (default) - the amount of host physical memory consumed for backing up guest physical memory pages<br> *ballooned* - the amount of guest physical memory reclaimed from the virtual machine by the balloon driver in the guest<br>*overhead* - the host physical memory consumed by ESXi data structures for running the virtual machines

[comment]: # ({/7173bd06-f519c2b7})

[comment]: # ({201aeb52-5081f28a})

##### vmware.alarms.get[url] {#vmware.alarms}

<br>
The VMware virtual center alarms data.<br>
Return value: [JSON object](/manual/vm_monitoring/vmware_json#vmware..alarms.get).

Parameters:

-   **url** - the VMware service URL.

[comment]: # ({/201aeb52-5081f28a})

[comment]: # ({dc84579f-1e57aebd})

##### vmware.vm.alarms.get[url,uuid] {#vmware.vm.alarms}

<br>
The VMware virtual machine alarms data.<br>
Return value: [JSON object](/manual/vm_monitoring/vmware_json#vmware..alarms.get).

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware virtual machine global unique identifier.

[comment]: # ({/dc84579f-1e57aebd})

[comment]: # ({4c9b5314-72b6205f})

##### vmware.vm.attribute[url,uuid,name] {#vmware.vm.attribute}

<br>
The VMware virtual machine custom attribute value.<br>
Return value: *String*.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware virtual machine global unique identifier;
-   **name** - the custom attribute name.

[comment]: # ({/4c9b5314-72b6205f})

[comment]: # ({45085cee-a03e0f2b})

##### vmware.vm.cluster.name[url,uuid] {#vmware.vm.cluster.name}

<br>
The VMware virtual machine name.<br>
Return value: *String*.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware virtual machine global unique identifier;
-   **name** - the custom attribute name.

[comment]: # ({/45085cee-a03e0f2b})

[comment]: # ({a247f767-7ed0802f})

##### vmware.vm.consolidationneeded[url,uuid] {#vmware.vm.consolidationneeded}

<br>
The VMware virtual machine disk requires consolidation.<br>
Return value: *String*: *true* - consolidation is needed; *false* - consolidation is not needed.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware virtual machine global unique identifier.

[comment]: # ({/a247f767-7ed0802f})

[comment]: # ({f41c5db0-f2d2b4c1})

##### vmware.vm.cpu.latency[url,uuid] {#vmware.vm.cpu.latency}

<br>
The percentage of time the virtual machine is unable to run because it is contending for access to the physical CPU(s).<br>
Return value: *Float*.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware virtual machine global unique identifier.

[comment]: # ({/f41c5db0-f2d2b4c1})

[comment]: # ({13506340-be1cf7ea})

##### vmware.vm.cpu.num[url,uuid] {#vmware.vm.cpu.num}

<br>
The number of processors on VMware virtual machine.<br>
Return value: *Integer*.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware virtual machine global unique identifier.

[comment]: # ({/13506340-be1cf7ea})

[comment]: # ({e2990fd9-28479715})

##### vmware.vm.cpu.readiness[url,uuid,<instance>] {#vmware.vm.cpu.readiness}

<br>
The percentage of time that the virtual machine was ready, but could not get scheduled to run on the physical CPU.<br>
Return value: *Float*.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware virtual machine global unique identifier;
-   **instance** - the CPU instance.

[comment]: # ({/e2990fd9-28479715})

[comment]: # ({50dfc8a5-099c1d95})

##### vmware.vm.cpu.ready[url,uuid] {#vmware.vm.cpu.ready}

<br>
The time (in milliseconds) that the virtual machine was ready, but could not get scheduled to run on the physical CPU. CPU ready time is dependent on the number of virtual machines on the host and their CPU loads (%).<br>
Return value: *Integer* ^**[2](vmware_keys#footnotes)**^.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware virtual machine global unique identifier.

[comment]: # ({/50dfc8a5-099c1d95})

[comment]: # ({15b8b257-7ca65c9e})

##### vmware.vm.cpu.swapwait[url,uuid,<instance>] {#vmware.vm.cpu.swapwait}

<br>
The percentage of CPU time spent waiting for swap-in.<br>
Return value: *Float*.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware virtual machine global unique identifier;
-   **instance** - the CPU instance.

[comment]: # ({/15b8b257-7ca65c9e})

[comment]: # ({ceacf0d3-7b77e5b1})

##### vmware.vm.cpu.usage[url,uuid] {#vmware.vm.cpu.usage}

<br>
The VMware virtual machine processor usage (Hz).<br>
Return value: *Integer*.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware virtual machine global unique identifier.

[comment]: # ({/ceacf0d3-7b77e5b1})

[comment]: # ({680db888-918ae7ad})

##### vmware.vm.cpu.usage.perf[url,uuid] {#vmware.vm.cpu.usage.perf}

<br>
The VMware virtual machine processor usage as a percentage during the interval.<br>
Return value: *Float*.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware virtual machine global unique identifier.

[comment]: # ({/680db888-918ae7ad})

[comment]: # ({c44bbea8-17da5a98})

##### vmware.vm.datacenter.name[url,uuid] {#vmware.vm.datacenter.name}

<br>
The VMware virtual machine datacenter name.<br>
Return value: *String*.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware virtual machine global unique identifier.

[comment]: # ({/c44bbea8-17da5a98})

[comment]: # ({37432ffa-c7a48a1f})

##### vmware.vm.discovery[url] {#vmware.vm.discovery}

<br>
The discovery of VMware virtual machines.<br>
Return value: [JSON object](/manual/vm_monitoring/discovery_fields).

Parameters:

-   **url** - the VMware service URL.

[comment]: # ({/37432ffa-c7a48a1f})

[comment]: # ({90baaadb-396f4e2c})

##### vmware.vm.guest.memory.size.swapped[url,uuid] {#vmware.vm.guest.memory.size.swapped}

<br>
The amount of guest physical memory that is swapped out to the swap space (KB).<br>
Return value: *Integer*.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware virtual machine global unique identifier.

[comment]: # ({/90baaadb-396f4e2c})

[comment]: # ({f696c432-3979320e})

##### vmware.vm.guest.osuptime[url,uuid] {#vmware.vm.guest.osuptime}

<br>
The total time elapsed since the last operating system boot-up (in seconds).<br>
Return value: *Integer*.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware virtual machine global unique identifier.

[comment]: # ({/f696c432-3979320e})

[comment]: # ({3472eb66-09ed96fc})

##### vmware.vm.hv.name[url,uuid] {#vmware.vm.hv.name}

<br>
The VMware virtual machine hypervisor name.<br>
Return value: *String*.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware virtual machine global unique identifier.

[comment]: # ({/3472eb66-09ed96fc})

[comment]: # ({2c9e4439-a0b0280a})

##### vmware.vm.memory.size[url,uuid] {#vmware.vm.memory.size}

<br>
The VMware virtual machine total memory size (bytes).<br>
Return value: *Integer*.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware virtual machine global unique identifier.

[comment]: # ({/2c9e4439-a0b0280a})

[comment]: # ({3fc5cb28-2cb0d24e})

##### vmware.vm.memory.size.ballooned[url,uuid] {#vmware.vm.memory.size.ballooned}

<br>
The VMware virtual machine ballooned memory size (bytes).<br>
Return value: *Integer*.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware virtual machine global unique identifier.

[comment]: # ({/3fc5cb28-2cb0d24e})

[comment]: # ({db25a1ac-137bfeaf})

##### vmware.vm.memory.size.compressed[url,uuid] {#vmware.vm.memory.size.compressed}

<br>
The VMware virtual machine compressed memory size (bytes).<br>
Return value: *Integer*.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware virtual machine global unique identifier.

[comment]: # ({/db25a1ac-137bfeaf})

[comment]: # ({15985880-fa8e1197})

##### vmware.vm.memory.size.consumed[url,uuid] {#vmware.vm.memory.size.consumed}

<br>
The amount of host physical memory consumed for backing up guest physical memory pages (KB).<br>
Return value: *Integer*.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware virtual machine global unique identifier.

[comment]: # ({/15985880-fa8e1197})

[comment]: # ({698735c4-2b9baf4b})

##### vmware.vm.memory.size.private[url,uuid] {#vmware.vm.memory.size.private}

<br>
The VMware virtual machine private memory size (bytes).<br>
Return value: *Integer*.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware virtual machine global unique identifier.

[comment]: # ({/698735c4-2b9baf4b})

[comment]: # ({02664105-b68ac975})

##### vmware.vm.memory.size.shared[url,uuid] {#vmware.vm.memory.size.shared}

<br>
The VMware virtual machine shared memory size (bytes).<br>
Return value: *Integer*.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware virtual machine global unique identifier.

[comment]: # ({/02664105-b68ac975})

[comment]: # ({3e4ee124-7e959166})

##### vmware.vm.memory.size.swapped[url,uuid] {#vmware.vm.memory.size.swapped}

<br>
The VMware virtual machine swapped memory size (bytes).<br>
Return value: *Integer*.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware virtual machine global unique identifier.

[comment]: # ({/3e4ee124-7e959166})

[comment]: # ({f1d2f2e5-47cf74ab})

##### vmware.vm.memory.size.usage.guest[url,uuid] {#vmware.vm.memory.size.usage.guest}

<br>
The VMware virtual machine guest memory usage (bytes).<br>
Return value: *Integer*.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware virtual machine global unique identifier.

[comment]: # ({/f1d2f2e5-47cf74ab})

[comment]: # ({6d0d0723-65627368})

##### vmware.vm.memory.size.usage.host[url,uuid] {#vmware.vm.memory.size.usage.host}

<br>
The VMware virtual machine host memory usage (bytes).<br>
Return value: *Integer*.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware virtual machine global unique identifier.

[comment]: # ({/6d0d0723-65627368})

[comment]: # ({7cdb57e3-a612183b})

##### vmware.vm.memory.usage[url,uuid] {#vmware.vm.memory.usage}

<br>
The percentage of host physical memory that has been consumed.<br>
Return value: *Float*.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware virtual machine global unique identifier.

[comment]: # ({/7cdb57e3-a612183b})

[comment]: # ({9eed9a97-530a28ca})

##### vmware.vm.net.if.discovery[url,uuid] {#vmware.vm.net.if.discovery}

<br>
The discovery of VMware virtual machine network interfaces.<br>
Return value: [JSON object](/manual/vm_monitoring/discovery_fields).

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware virtual machine global unique identifier.

[comment]: # ({/9eed9a97-530a28ca})

[comment]: # ({90cac04a-8a5fbed5})

##### vmware.vm.net.if.in[url,uuid,instance,<mode>] {#vmware.vm.net.if.in}

<br>
The VMware virtual machine network interface input statistics (bytes/packets per second).<br>
Return value: *Integer* ^**[2](vmware_keys#footnotes)**^.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware virtual machine global unique identifier;
-   **instance** - the network interface instance;
-   **mode** - *bps* (default) or *pps* - bytes or packets per second.

[comment]: # ({/90cac04a-8a5fbed5})

[comment]: # ({34365e20-71f52f11})

##### vmware.vm.net.if.out[url,uuid,instance,<mode>] {#vmware.vm.net.if.out}

<br>
The VMware virtual machine network interface output statistics (bytes/packets per second).<br>
Return value: *Integer* ^**[2](vmware_keys#footnotes)**^.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware virtual machine global unique identifier;
-   **instance** - the network interface instance;
-   **mode** - *bps* (default) or *pps* - bytes or packets per second.

[comment]: # ({/34365e20-71f52f11})

[comment]: # ({042253e1-4f89074d})

##### vmware.vm.net.if.usage[url,uuid,<instance>] {#vmware.vm.net.if.usage}

<br>
The VMware virtual machine network utilization (combined transmit-rates and receive-rates) during the interval (KBps).<br>
Return value: *Integer*.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware virtual machine global unique identifier;
-   **instance** - the network interface instance.

[comment]: # ({/042253e1-4f89074d})

[comment]: # ({0c002175-398c5f14})

##### vmware.vm.perfcounter[url,uuid,path,<instance>] {#vmware.vm.perfcounter}

<br>
The VMware virtual machine performance counter value.<br>
Return value: *Integer* ^**[2](vmware_keys#footnotes)**^.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware virtual machine global unique identifier;
-   **path** - the performance counter path ^**[1](vmware_keys#footnotes)**^;
-   **instance** - the performance counter instance. Use empty instance for aggregate values (default).

[comment]: # ({/0c002175-398c5f14})

[comment]: # ({1d2ef5d0-f57fc56b})

##### vmware.vm.powerstate[url,uuid] {#vmware.vm.powerstate}

<br>
The VMware virtual machine power state.<br>
Return value: *0* - poweredOff; *1* - poweredOn; *2* - suspended.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware virtual machine global unique identifier.

[comment]: # ({/1d2ef5d0-f57fc56b})

[comment]: # ({7e826d69-787961d6})

##### vmware.vm.property[url,uuid,prop] {#vmware.vm.property}

<br>
The VMware virtual machine property.<br>
Return value: *String*.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware virtual machine global unique identifier;
-   **prop** - the property path.

[comment]: # ({/7e826d69-787961d6})

[comment]: # ({79066566-05b78c4d})

##### vmware.vm.snapshot.get[url,uuid] {#vmware.vm.snapshot}

<br>
The VMware virtual machine snapshot state.<br>
Return value: [JSON object](/manual/vm_monitoring/vmware_json#vmware.vm.snapshot.get).

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware virtual machine global unique identifier.

[comment]: # ({/79066566-05b78c4d})

[comment]: # ({97a8fd2c-ddd3559d})

##### vmware.vm.state[url,uuid] {#vmware.vm.state}

<br>
The VMware virtual machine state.<br>
Return value: *String*: *notRunning*, *resetting*, *running*, *shuttingDown*, *standby*, or *unknown*.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware virtual machine global unique identifier.

[comment]: # ({/97a8fd2c-ddd3559d})

[comment]: # ({40a1fdf2-f4e64b1e})

##### vmware.vm.storage.committed[url,uuid] {#vmware.vm.storage.committed}

<br>
The VMware virtual machine committed storage space (bytes).<br>
Return value: *Integer*.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware virtual machine global unique identifier.

[comment]: # ({/40a1fdf2-f4e64b1e})

[comment]: # ({503236c3-768832c2})

##### vmware.vm.storage.readoio[url,uuid,instance] {#vmware.vm.storage.readoio}

<br>
The average number of outstanding read requests to the virtual disk during the collection interval.<br>
Return value: *Integer*.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware virtual machine global unique identifier;
-   **instance** - the disk device instance.

[comment]: # ({/503236c3-768832c2})

[comment]: # ({c63e2893-ebfcaecb})

##### vmware.vm.storage.totalreadlatency[url,uuid,instance] {#vmware.vm.storage.totalreadlatency}

<br>
The average time a read from the virtual disk takes (milliseconds).<br>
Return value: *Integer*.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware virtual machine global unique identifier;
-   **instance** - the disk device instance.

[comment]: # ({/c63e2893-ebfcaecb})

[comment]: # ({45e36d0d-b336d96d})

##### vmware.vm.storage.totalwritelatency[url,uuid,instance] {#vmware.vm.storage.totalwritelatency}

<br>
The average time a write to the virtual disk takes (milliseconds).<br>
Return value: *Integer*.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware virtual machine global unique identifier;
-   **instance** - the disk device instance.

[comment]: # ({/45e36d0d-b336d96d})

[comment]: # ({16b5dfb4-3a56bfe0})

##### vmware.vm.storage.uncommitted[url,uuid] {#vmware.vm.storage.uncommitted}

<br>
The VMware virtual machine uncommitted storage space (bytes).<br>
Return value: *Integer*.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware virtual machine global unique identifier.

[comment]: # ({/16b5dfb4-3a56bfe0})

[comment]: # ({f8d470c9-f62e9476})

##### vmware.vm.storage.unshared[url,uuid] {#vmware.vm.storage.unshared}

<br>
The VMware virtual machine unshared storage space (bytes).<br>
Return value: *Integer*.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware virtual machine global unique identifier.

[comment]: # ({/f8d470c9-f62e9476})

[comment]: # ({c65c7344-de2cfd80})

##### vmware.vm.storage.writeoio[url,uuid,instance] {#vmware.vm.storage.writeoio}

<br>
The average number of outstanding write requests to the virtual disk during the collection interval.<br>
Return value: *Integer*.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware virtual machine global unique identifier;
-   **instance** - the disk device instance.

[comment]: # ({/c65c7344-de2cfd80})

[comment]: # ({565c1905-ce5cdae6})

##### vmware.vm.tags.get[url,uuid] {#vmware.vm.tags}

<br>
The VMware virtual machine tags array.<br>
Return value: [JSON object](/manual/vm_monitoring/vmware_json#vmware..tags.get).

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware virtual machine global unique identifier.

This item works with vSphere 6.5 and newer.

[comment]: # ({/565c1905-ce5cdae6})

[comment]: # ({275aa0c2-8b6eaeb3})

##### vmware.vm.tools[url,uuid,mode] {#vmware.vm.tools}

<br>
The VMware virtual machine guest tools state.<br>
Return value: *String*: *guestToolsExecutingScripts* - VMware Tools is starting; *guestToolsNotRunning* - VMware Tools is not running; *guestToolsRunning* - VMware Tools is running.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware virtual machine global unique identifier;
-   **mode** - *version*, *status*.

[comment]: # ({/275aa0c2-8b6eaeb3})

[comment]: # ({7b073018-4fa0c20c})

##### vmware.vm.uptime[url,uuid] {#vmware.vm.uptime}

<br>
The VMware virtual machine uptime (seconds).<br>
Return value: *Integer*.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware virtual machine global unique identifier.

[comment]: # ({/7b073018-4fa0c20c})

[comment]: # ({9e42d1a9-bbfa14b4})

##### vmware.vm.vfs.dev.discovery[url,uuid] {#vmware.vm.vfs.dev.discovery}

<br>
The discovery of VMware virtual machine disk devices.<br>
Return value: [JSON object](/manual/vm_monitoring/discovery_fields).

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware virtual machine global unique identifier.

[comment]: # ({/9e42d1a9-bbfa14b4})

[comment]: # ({bcd2ecc1-62251acd})

##### vmware.vm.vfs.dev.read[url,uuid,instance,<mode>] {#vmware.vm.vfs.dev.read}

<br>
The VMware virtual machine disk device read statistics (bytes/operations per second).<br>
Return value: *Integer* ^**[2](vmware_keys#footnotes)**^.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware virtual machine global unique identifier;
-   **instance** - the disk device instance;
-   **mode** - *bps* (default) or *ops* - bytes or operations per second.

[comment]: # ({/bcd2ecc1-62251acd})

[comment]: # ({f5a61834-c3dd3850})

##### vmware.vm.vfs.dev.write[url,uuid,instance,<mode>] {#vmware.vm.vfs.dev.write}

<br>
The VMware virtual machine disk device write statistics (bytes/operations per second).<br>
Return value: *Integer* ^**[2](vmware_keys#footnotes)**^.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware virtual machine global unique identifier;
-   **instance** - the disk device instance;
-   **mode** - *bps* (default) or *ops* - bytes or operations per second.

[comment]: # ({/f5a61834-c3dd3850})

[comment]: # ({fae7c662-4a444974})

##### vmware.vm.vfs.fs.discovery[url,uuid] {#vmware.vm.vfs.fs.discovery}

<br>
The discovery of VMware virtual machine file systems.<br>
Return value: [JSON object](/manual/vm_monitoring/discovery_fields).

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware virtual machine global unique identifier.

VMware Tools must be installed on the guest virtual machine for this item to work.

[comment]: # ({/fae7c662-4a444974})

[comment]: # ({7a304e26-e379bb53})

##### vmware.vm.vfs.fs.size[url,uuid,fsname,<mode>] {#vmware.vm.vfs.fs.size}

<br>
The VMware virtual machine file system statistics (bytes/percentages).<br>
Return value: *Integer*.

Parameters:

-   **url** - the VMware service URL;
-   **uuid** - the VMware virtual machine global unique identifier;
-   **fsname** - the file system name;
-   **mode** - *total*, *free*, *used*, *pfree*, or *pused*.

VMware Tools must be installed on the guest virtual machine for this item to work.

[comment]: # ({/7a304e26-e379bb53})

[comment]: # ({80f4d2d7-d3b26934})
##### Footnotes

^**1**^ The VMware performance counter path has the
`group/counter[rollup]` format where:

-   `group` - the performance counter group, for example *cpu*
-   `counter` - the performance counter name, for example *usagemhz*
-   `rollup` - the performance counter rollup type, for example
    *average*

So the above example would give the following counter path:
`cpu/usagemhz[average]`

See also: [Creating custom performance counter names for VMware](/manual/appendix/items/perf_counters).

The performance counter group descriptions, counter names and rollup
types can be found in [VMware
documentation](https://developer.vmware.com/apis/968). 

^**2**^ The value of these items is obtained from VMware performance
counters and the VMwarePerfFrequency
[parameter](/manual/appendix/config/zabbix_server) is used to refresh
their data in Zabbix VMware cache:

-   vmware.cl.perfcounter
-   vmware.hv.datastore.read
-   vmware.hv.datastore.write
-   vmware.hv.network.in
-   vmware.hv.network.out
-   vmware.hv.perfcounter
-   vmware.vm.cpu.ready
-   vmware.vm.net.if.in
-   vmware.vm.net.if.out
-   vmware.vm.perfcounter
-   vmware.vm.vfs.dev.read
-   vmware.vm.vfs.dev.write

[comment]: # ({/80f4d2d7-d3b26934})

[comment]: # ({0b282d59-0b282d59})
#### More info

See [Virtual machine monitoring](/manual/vm_monitoring) for detailed
information how to configure Zabbix to monitor VMware environments.

[comment]: # ({/0b282d59-0b282d59})

[comment]: # aside:1
