[comment]: # ({566c4be6-566c4be6})
# 14. Configuration export/import

[comment]: # ({/566c4be6-566c4be6})

[comment]: # ({c2262f8d-265e7dfe})
#### Overview

Zabbix export/import functionality makes it possible to exchange various
configuration entities between one Zabbix system and another.

Typical use cases for this functionality:

-   share templates or network maps - Zabbix users may share their
    configuration parameters
-   share web scenarios on *share.zabbix.com* - export a template with
    the web scenarios and upload to *share.zabbix.com*. Then others can
    download the template and import the file into Zabbix.
-   integrate with third-party tools - universal YAML, XML and JSON
    formats make integration and data import/export possible with third-party
    tools and applications

[comment]: # ({/c2262f8d-265e7dfe})

[comment]: # ({fa663f5b-19950447})
##### What can be exported/imported

Objects that can be exported/imported are:

-   [Host groups](/manual/xml_export_import/hostgroups) (*through Zabbix API only*)
-   [Template groups](/manual/xml_export_import/templategroups) (*through Zabbix API only*) 
-   [Templates](/manual/xml_export_import/templates)
-   [Hosts](/manual/xml_export_import/hosts)
-   [Network maps](/manual/xml_export_import/maps)
-   [Media types](/manual/xml_export_import/media)
-   Images

[comment]: # ({/fa663f5b-19950447})

[comment]: # ({44b0f5b3-44b0f5b3})
##### Export format

Data can be exported using the Zabbix web frontend or [Zabbix
API](/manual/api/reference/configuration). Supported export formats are
YAML, XML and JSON.

[comment]: # ({/44b0f5b3-44b0f5b3})

[comment]: # ({b87bd84d-b87bd84d})
#### Details about export

-   All supported elements are exported in one file.
-   Host and template entities (items, triggers, graphs, discovery
    rules) that are inherited from linked templates are not exported.
    Any changes made to those entities on a host level (such as changed
    item interval, modified regular expression or added prototypes to
    the low-level discovery rule) will be lost when exporting; when
    importing, all entities from linked templates are re-created as on
    the original linked template.
-   Entities created by low-level discovery and any entities depending
    on them are not exported. For example, a trigger created for an
    LLD-rule generated item will not be exported.

[comment]: # ({/b87bd84d-b87bd84d})

[comment]: # ({fcd8f798-6ea18fa7})
#### Details about import

-   Import stops at the first error.
-   When updating existing images during image import, "imagetype" field
    is ignored, i.e. it is impossible to change image type via import.
-   When importing hosts/templates using the "Delete missing" option,
    host/template macros not present in the import file will be deleted from  
    the host/template after the import.
-   Empty tags for items, triggers, graphs, host/template applications,
    discoveryRules, itemPrototypes, triggerPrototypes, graphPrototypes
    are meaningless i.e. it's the same as if it was missing. Other tags,
    for example, item applications, are meaningful i.e. empty tag means
    no applications for item, missing tag means don't update
    applications.
-   Import supports YAML, XML and JSON, the import file must have a
    correct file extension: .yaml and .yml for YAML, .xml for XML and
    .json for JSON. See [compatibility information](/manual/appendix/compatibility)
    about supported XML versions.
-   Import supports configuration files only in UTF-8 encoding (with or without [BOM](https://en.wikipedia.org/wiki/Byte_order_mark));
    other encodings (UTF16LE, UTF16BE, UTF32LE, UTF32BE, etc.) will result in an import conversion error.

[comment]: # ({/fcd8f798-6ea18fa7})

[comment]: # ({d571a030-790544d4})
#### YAML base format

```yaml
zabbix_export:
  version: '7.0'
```

    zabbix_export:

Root node for Zabbix YAML export.

    version: '7.0'

Export version.

Other nodes are dependent on exported objects.

[comment]: # ({/d571a030-790544d4})

[comment]: # ({3ceb18c9-8c839240})
#### XML format

```xml
<?xml version="1.0" encoding="UTF-8"?>
<zabbix_export>
    <version>7.0</version>
</zabbix_export>
```

    <?xml version="1.0" encoding="UTF-8"?>

Default header for XML documents.

    <zabbix_export>

Root element for Zabbix XML export.

    <version>7.0</version>

Export version.

Other tags are dependent on exported objects.

[comment]: # ({/3ceb18c9-8c839240})

[comment]: # ({8f308951-239421b8})
#### JSON format

```json
{
    "zabbix_export": {
        "version": "7.0"
    }
}
```

    "zabbix_export":

Root node for Zabbix JSON export.

    "version": "7.0"

Export version.

Other nodes are dependent on exported objects.

[comment]: # ({/8f308951-239421b8})
