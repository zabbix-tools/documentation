[comment]: # ({8fc97588-8fc97588})
# 5 Frontend maintenance mode

[comment]: # ({/8fc97588-8fc97588})

[comment]: # ({fca7c17f-fca7c17f})
#### Overview

Zabbix web frontend can be temporarily disabled in order to prohibit
access to it. This can be useful for protecting the Zabbix database from
any changes initiated by users, thus protecting the integrity of
database.

Zabbix database can be stopped and maintenance tasks can be performed
while Zabbix frontend is in maintenance mode.

Users from defined IP addresses will be able to work with the frontend
normally during maintenance mode.

[comment]: # ({/fca7c17f-fca7c17f})

[comment]: # ({1f4215ba-dcfd8b13})
#### Configuration

In order to enable maintenance mode, the `maintenance.inc.php` file
(located in /conf of Zabbix HTML document directory on the
web server) must be modified to uncomment the following lines:

    // Maintenance mode.
    define('ZBX_DENY_GUI_ACCESS', 1);

    // Array of IP addresses, which are allowed to connect to frontend (optional).
    $ZBX_GUI_ACCESS_IP_RANGE = array('127.0.0.1');

    // Message shown on warning screen (optional).
    $ZBX_GUI_ACCESS_MESSAGE = 'We are upgrading MySQL database till 15:00. Stay tuned...';

[comment]: # ({/1f4215ba-dcfd8b13})

[comment]: # ({b962271a-ae331777})

::: notetip
Mostly the `maintenance.inc.php` file is located in `/conf` of Zabbix HTML document directory on the
web server. However, the location of the directory may differ depending on the operating system and a web server it uses.

For example, the location for:

-  SUSE and RedHat is `/etc/zabbix/web/maintenance.inc.php`.
-  Debian-based systems is `/usr/share/zabbix/conf/`.

See also [Copying PHP files](/manual/installation/install#copying-php-files). 
:::

|Parameter|Details|
|--|--------|
|**ZBX\_DENY\_GUI\_ACCESS**|Enable maintenance mode:<br>1 – maintenance mode is enabled, disabled otherwise|
|**ZBX\_GUI\_ACCESS\_IP\_RANGE**|Array of IP addresses, which are allowed to connect to frontend (optional).<br>For example:<br>`array('192.168.1.1', '192.168.1.2')`|
|**ZBX\_GUI\_ACCESS\_MESSAGE**|A message you can enter to inform users about the maintenance (optional).|

Note that the [location](/manual/installation/install#copying-php-files) of the `/conf` directory will vary based on the operating system and web server. 

[comment]: # ({/b962271a-ae331777})

[comment]: # ({987fdc44-987fdc44})
#### Display

The following screen will be displayed when trying to access the Zabbix
frontend while in maintenance mode. The screen is refreshed every 30
seconds in order to return to a normal state without user intervention
when the maintenance is over.

![](../../../assets/en/manual/web_interface/frontend_maintenance.png)

IP addresses defined in ZBX\_GUI\_ACCESS\_IP\_RANGE will be able to
access the frontend as always.

[comment]: # ({/987fdc44-987fdc44})
