[comment]: # ({290553c7-290553c7})
# 1 System information

[comment]: # ({/290553c7-290553c7})

[comment]: # ({7d165d32-ede74e69})
#### Overview

In *Reports → System information* a summary of key Zabbix server and
system data is displayed.

Note that in a high availability setup it is possible to redirect the system information source (server instance).
To do this, edit the *zabbix.conf.php* file - uncomment and set `$ZBX_SERVER` or both `$ZBX_SERVER` and `$ZBX_SERVER_PORT` to a server other than the one shown active.
Note that when setting `$ZBX_SERVER` only, a default value (10051) for `$ZBX_SERVER_PORT` will be used.

With the high availability setup enabled, a separate block is displayed
below the system stats with details of high availability nodes. This
block is visible to Zabbix *Super Admin* users only.

*System information* is also available as a dashboard
[widget](/manual/web_interface/frontend_sections/dashboards/widgets).

[comment]: # ({/7d165d32-ede74e69})

[comment]: # ({295ca363-bfa24672})
#### System stats

![](../../../../../assets/en/manual/web_interface/system_information1.png){width="600"}

Displayed data:

|Parameter|Value|Details|
|--|------|------|
|*Zabbix server is running*|Status of Zabbix server:<br>**Yes** - server is running<br>**No** - server is not running<br>*Note:* To display the rest of the information the web frontend needs the server to be running and there must be at least one trapper process started on the server (StartTrappers parameter in [zabbix\_server.conf](/manual/appendix/config/zabbix_server) file > 0).|Location and port of Zabbix server.|
|*Zabbix server version*|Current server version number is displayed.<br>*Note:* It is only displayed when Zabbix server is running.| |
|*Zabbix frontend version*|Zabbix frontend version number is displayed.| |
|*Number of hosts*|Total number of hosts configured is displayed.|Number of monitored hosts/not monitored hosts.|
|*Number of templates*|Total number of templates is displayed.| |
|*Number of items*|Total number of items is displayed.|Number of monitored/disabled/unsupported items.<br>Items on disabled hosts are counted as disabled.|
|*Number of triggers*|Total number of triggers is displayed.|Number of enabled/disabled triggers. \[Triggers in problem/ok state.\]<br>Triggers assigned to disabled hosts or depending on disabled items are counted as disabled.|
|*Number of users*|Total number of users configured is displayed.|Number of users online.|
|*Required server performance, new values per second*|The expected number of new values processed by Zabbix server per second is displayed.|*Required server performance* is an estimate and can be useful as a guideline. For precise numbers of values processed, use the `zabbix[wcache,values,all]` [internal item](/manual/config/items/itemtypes/internal).<br><br>Enabled items from monitored hosts are included in the calculation. Log items are counted as one value per item update interval. Regular interval values are counted; flexible and scheduling interval values are not. The calculation is not adjusted during a "nodata" maintenance period. Trapper items are not counted.|
|*High availability cluster*|Status of [high availability cluster](/manual/concepts/server/ha) for Zabbix server:<br>**disabled** - standalone server<br>**enabled** - at least one high availability node exists|If enabled, the failover delay is displayed.|

*System information* will also display an error message in the following conditions:

-   The database used does not have the required character set or collation (UTF-8).
-   The version of the database is below or above
    the [supported range](/manual/installation/requirements#required_software)
    (available only to users with the *Super admin role* type).
-   [Housekeeping](/manual/web_interface/frontend_sections/administration/housekeeping)
    for [TimescaleDB](/manual/appendix/install/timescaledb) is incorrectly configured
    (history or trend tables contain compressed chunks,
    but *Override item history period* or *Override item trend period* options are disabled).

[comment]: # ({/295ca363-bfa24672})

[comment]: # ({0e9610c1-76697959})
#### High availability nodes

If [high availability cluster](/manual/concepts/server/ha) is enabled,
then another block of data is displayed with the status of each high
availability node.

![](../../../../../assets/en/manual/web_interface/ha_nodes.png){width="600"}

Displayed data:

|Column|Description|
|--|--------|
|*Name*|Node name, as defined in server configuration.|
|*Address*|Node IP address and port.|
|*Last access*|Time of node last access.<br>Hovering over the cell shows the timestamp of last access in long format.|
|*Status*|Node status:<br>**Active** - node is up and working<br>**Unavailable** - node hasn't been seen for more than failover delay (you may want to find out why)<br>**Stopped** - node has been stopped or couldn't start (you may want to start it or delete it)<br>**Standby** - node is up and waiting|

[comment]: # ({/0e9610c1-76697959})
