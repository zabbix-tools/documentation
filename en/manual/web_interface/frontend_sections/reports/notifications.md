[comment]: # ({58cc1743-58cc1743})
# 7 Notifications

[comment]: # ({/58cc1743-58cc1743})

[comment]: # ({c29272bd-c29272bd})
#### Overview

In the *Reports → Notifications* section a report on the number of
notifications sent to each user is displayed.

From the dropdowns in the top right-hand corner you can choose the media
type (or all), period (data for each day/week/month/year) and year for
the notifications sent.

![](../../../../../assets/en/manual/web_interface/frontend_sections/reports/notifications.png){width="600"}

Each column displays totals per one system user.

[comment]: # ({/c29272bd-c29272bd})
