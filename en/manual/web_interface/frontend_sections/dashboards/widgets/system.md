[comment]: # ({c8e59db6-c8e59db6})
# 20 System information

[comment]: # ({/c8e59db6-c8e59db6})

[comment]: # ({b25faeae-b25faeae})
#### Overview

This widget displays the same information as in *Reports* → *[System
information](/manual/web_interface/frontend_sections/reports/status_of_zabbix)*,
however, a single dashboard widget can only display either the system stats or
the high availability nodes at a time (not both).

[comment]: # ({/b25faeae-b25faeae})

[comment]: # ({bc40a663-27793c11})
#### Configuration

To configure, select *System information* as type:

![](../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/system_information.png)

All configuration parameters are [common](/manual/web_interface/frontend_sections/dashboards/widgets#common-parameters) 
for all widgets.

[comment]: # ({/bc40a663-27793c11})
