[comment]: # ({d91968a5-d91968a5})
# 4 Discovery status

[comment]: # ({/d91968a5-d91968a5})

[comment]: # ({700f6bc9-e3595eca})
#### Overview

This widget displays a status summary of the active network discovery
rules.

![](../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/discovery_status.png)

All configuration parameters are [common](/manual/web_interface/frontend_sections/dashboards/widgets#common-parameters) 
for all widgets.

[comment]: # ({/700f6bc9-e3595eca})
