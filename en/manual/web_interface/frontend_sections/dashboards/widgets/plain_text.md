[comment]: # ({c1a63b69-c1a63b69})
# 15 Plain text

[comment]: # ({/c1a63b69-c1a63b69})

[comment]: # ({569590b5-569590b5})
#### Overview

In the plain text widget, you can display the latest item data in plain
text.

[comment]: # ({/569590b5-569590b5})

[comment]: # ({d0d25a13-e25ab8a1})
#### Configuration

To configure, select *Plain text* as type:

![](../../../../../../assets/en/manual/web_interface/frontend_sections/dashboards/widgets/plain_text.png)

In addition to the parameters that are [common](/manual/web_interface/frontend_sections/dashboards/widgets#common-parameters) 
for all widgets, you may set the following specific options:

|   |   |
|--|--------|
|*Items*|Select the items.<br>Displaying items with binary values is not supported.|
|*Items location*|Choose the location of selected items to be displayed in the widget.|
|*Show lines*|Set how many latest data lines will be displayed in the widget.|
|*Show text as HTML*|Set to display text as HTML.|
|*[Enable host selection](/manual/web_interface/frontend_sections/dashboards#dynamic_widgets)*|Set to display different data depending on the selected host.|

[comment]: # ({/d0d25a13-e25ab8a1})
