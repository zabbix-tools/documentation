[comment]: # ({ebb3a9a4-ebb3a9a4})
# 16 Problem hosts

[comment]: # ({/ebb3a9a4-ebb3a9a4})

[comment]: # ({9cb7def0-3853d2c7})
#### Overview

In the problem host widget, you can display problem count by host group and the highest problem severity within a group.

The problem count is displayed only for cause problems.

[comment]: # ({/9cb7def0-3853d2c7})

[comment]: # ({37ccdee5-5709a5f0})
#### Configuration

To configure, select *Problem hosts* as type:

![](../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/problem_hosts.png){width="600"}

In addition to the parameters that are [common](/manual/web_interface/frontend_sections/dashboards/widgets#common-parameters) 
for all widgets, you may set the following specific options:

|Parameter|Description|
|--|--------|
|*Host groups*|Enter host groups to display in the widget.<br>This field is auto-complete so starting to type the name of a group will offer a dropdown of matching groups.<br>Specifying a parent host group implicitly selects all nested host groups.<br>Host data from these host groups will be displayed in the widget; if no host groups are entered, all host groups will be displayed.<br>This parameter is not available when configuring the widget on a [template dashboard](/manual/config/templates/template#adding-dashboards).|
|*Exclude host groups*|Enter host groups to hide from the widget.<br>This field is auto-complete so starting to type the name of a group will offer a dropdown of matching groups.<br>Specifying a parent host group implicitly selects all nested host groups.<br>Host data from these host groups will not be displayed in the widget. For example, hosts 001, 002, 003 may be in Group A and hosts 002, 003 in Group B as well. If we select to *show* Group A and *exclude* Group B at the same time, only data from host 001 will be displayed in the dashboard.<br>This parameter is not available when configuring the widget on a [template dashboard](/manual/config/templates/template#adding-dashboards).|
|*Hosts*|Enter hosts to display in the widget.<br>This field is auto-complete so starting to type the name of a host will offer a dropdown of matching hosts.<br>If no hosts are entered, all hosts will be displayed.<br>This parameter is not available when configuring the widget on a [template dashboard](/manual/config/templates/template#adding-dashboards).|
|*Problem*|You can limit the number of problem hosts displayed by the problem name.<br>If you enter a string here, only those hosts with problems whose name contains the entered string will be displayed.<br>Macros are not expanded.|
|*Severity*|Mark the problem severities to be displayed in the widget.|
|*Problem tags*|Specify problem tags to limit the number of problems displayed in the widget.<br>It is possible to include as well as exclude specific tags and tag values. Several conditions can be set. Tag name matching is always case-sensitive.<br><br>There are several operators available for each condition:<br>**Exists** - include the specified tag names;<br>**Equals** - include the specified tag names and values (case-sensitive);<br>**Contains** - include the specified tag names where the tag values contain the entered string (substring match, case-insensitive);<br>**Does not exist** - exclude the specified tag names;<br>**Does not equal** - exclude the specified tag names and values (case-sensitive);<br>**Does not contain** - exclude the specified tag names where the tag values contain the entered string (substring match, case-insensitive).<br><br>There are two calculation types for conditions:<br>**And/Or** - all conditions must be met, conditions having the same tag name will be grouped by the *Or* condition;<br>**Or** - enough if one condition is met.|
|*Show suppressed problems*|Mark the checkbox to display problems that would otherwise be suppressed (not shown) because of host maintenance.|
|*Hide groups without problems*|Mark the *Hide groups without problems* option to hide data from host groups without problems in the widget.<br>This parameter is not available when configuring the widget on a [template dashboard](/manual/config/templates/template#adding-dashboards).|
|*Problem display*|Display problem count as:<br>**All** - full problem count will be displayed;<br>**Separated** - unacknowledged problem count will be displayed separated as a number of the total problem count;<br>**Unacknowledged only** - only the unacknowledged problem count will be displayed.|

[comment]: # ({/37ccdee5-5709a5f0})
