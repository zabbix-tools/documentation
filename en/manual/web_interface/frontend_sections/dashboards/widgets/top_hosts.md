[comment]: # ({11ceac39-11ceac39})
# 21 Top hosts

[comment]: # ({/11ceac39-11ceac39})

[comment]: # ({5653f4fe-00820133})
#### Overview

This widget provides a way to create custom tables for displaying the data situation, 
allowing to display *Top N*-like reports and progress-bar reports useful for capacity planning. 

The maximum number of hosts that can be displayed is 100.

![](../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/top_hosts.png){width="600"}

![](../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/top_hosts2.png){width="600"}

[comment]: # ({/5653f4fe-00820133})

[comment]: # ({952bb455-1a44cc5c})
#### Configuration

To configure, select *Top hosts* as type:

![](../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/top_hosts.1.png){width="600"}

[comment]: # ({/952bb455-1a44cc5c})

[comment]: # ({bc5ebb84-de161855})
In addition to the parameters that are [common](/manual/web_interface/frontend_sections/dashboards/widgets#common-parameters) 
for all widgets, you may set the following specific options:

|   |   |
|--|--------|
|*Host groups*|Host groups to display data for.<br>This parameter is not available when configuring the widget on a [template dashboard](/manual/config/templates/template#adding-dashboards).|
|*Hosts*|Hosts to display data for.<br>This parameter is not available when configuring the widget on a [template dashboard](/manual/config/templates/template#adding-dashboards).|
|*Host tags*|Specify tags to limit the number of hosts displayed in the widget.<br>It is possible to include as well as exclude specific tags and tag values. Several conditions can be set. Tag name matching is always case-sensitive.<br><br>There are several operators available for each condition:<br>**Exists** - include the specified tag names;<br>**Equals** - include the specified tag names and values (case-sensitive);<br>**Contains** - include the specified tag names where the tag values contain the entered string (substring match, case-insensitive);<br>**Does not exist** - exclude the specified tag names;<br>**Does not equal** - exclude the specified tag names and values (case-sensitive);<br>**Does not contain** - exclude the specified tag names where the tag values contain the entered string (substring match, case-insensitive).<br><br>There are two calculation types for conditions:<br>**And/Or** - all conditions must be met, conditions having the same tag name will be grouped by the Or condition;<br>**Or** - enough if one condition is met.<br><br>This parameter is not available when configuring the widget on a [template dashboard](/manual/config/templates/template#adding-dashboards).|
|*Columns*|Add data [columns](#columns) to display.<br>The column order determines their display from left to right.<br>Columns can be reordered by dragging up and down by the handle before the column name.|
|*Order*|Specify the ordering of rows:<br>**Top N** - in descending order by the *Order column* aggregated value;<br>**Bottom N** - in ascending order by the *Order column* aggregated value.|
|*Order column*|Specify the column from the defined *Columns* list to use for *Top N* or *Bottom N* ordering.|
|*Host count*|Count of host rows to be shown (1-100).<br>This parameter is not available when configuring the widget on a [template dashboard](/manual/config/templates/template#adding-dashboards).|

[comment]: # ({/bc5ebb84-de161855})

[comment]: # ({5013aa3c-54c7e604})
##### Column configuration

![](../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/top_hosts.2.png){width="600"}

[comment]: # ({/5013aa3c-54c7e604})

[comment]: # ({9f59a45c-ded10671})
Common column parameters:

|   |   |
|--|--------|
|*Name*|Name of the column.|
|*Data*|Data type to display in the column:<br>**Item value** - value of the specified item;<br>**Host name** - host name of the item specified in the *Item value* column;<br>**Text** - static text string.|
|*Base color*|Background color of the column; fill color if *Item value* data is displayed as bar/indicators.<br>For *Item value* data the default color can be overridden by custom color, if the item value is over one of the specified thresholds.|

[comment]: # ({/9f59a45c-ded10671})

[comment]: # ({c3ecbef5-c994be1c})
Specific parameters for item value columns:

|   |   |
|--|--------|
|*Item*|Select the item.<br>When configuring the widget on a [template dashboard](/manual/config/templates/template#adding-dashboards), only [items configured on the template](/manual/config/templates/template#adding-items-triggers-graphs) can be selected.<br>Selecting items with binary values is not supported.|
|*Time shift*|Specify time shift if required.<br>You may use [time suffixes](/manual/appendix/suffixes#time_suffixes) in this field. Negative values are allowed.|
|*Aggregation function*|Specify which aggregation function to use:<br>**min** - display the smallest value;<br>**max** - display the largest value;<br>**avg** - display the average value;<br>**sum** - display the sum of values;<br>**count** - display the count of values;<br>**first** - display the first value;<br>**last** - display the last value;<br>**none** - display all values (no aggregation).<br><br>Aggregation allows to display an aggregated value for the chosen interval (5 minutes, an hour, a day), instead of all values.<br>Note that only numeric items can be displayed in this column if this setting is not "none".|
|*Aggregation interval*|Specify the interval for aggregating values.<br>You may use [time suffixes](/manual/appendix/suffixes#time_suffixes) in this field. A numeric value without a suffix will be regarded as seconds.<br>This parameter will not be displayed if *Aggregation function* is set to "none".|
|*Display*|Define how the value should be displayed:<br>**As is** - as regular text;<br>**Bar** - as solid, color-filled bar;<br>**Indicators** - as segmented, color-filled bar.<br><br>Note that only numeric items can be displayed in this column if this setting is not "as is".|
|*History*|Take data from history or trends:<br>**Auto** - automatic selection;<br>**History** - take history data;<br>**Trends** - take trend data.<br><br>This setting applies only to numeric data. Non-numeric data will always be taken from history.|
|*Min*|Minimum value for bar/indicators.|
|*Max*|Maximum value for bar/indicators.|
|*Decimal places*|Specify how many decimal places will be displayed with the value.<br>This setting applies only to numeric data.|
|*Thresholds*|Specify threshold values when the background/fill color should change.<br>The list will be sorted in ascending order when saved.<br>Note that only numeric items can be displayed in this column if thresholds are used.|

[comment]: # ({/c3ecbef5-c994be1c})

[comment]: # ({8451ec79-b7e22215})
Specific parameters for text columns:

|   |   |
|--|--------|
|*Text*|Enter the string to display.<br>May contain host and inventory [macros](/manual/appendix/macros/supported_by_location).|

[comment]: # ({/8451ec79-b7e22215})
