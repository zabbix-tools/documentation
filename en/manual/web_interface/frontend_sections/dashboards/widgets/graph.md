[comment]: # ({167707a1-167707a1})
# 8 Graph

[comment]: # ({/167707a1-167707a1})

[comment]: # ({bd8ad980-21bacb3e})
#### Overview

The graph widget provides a modern and versatile way of visualizing data
collected by Zabbix using a vector image drawing technique. This graph
widget is supported since Zabbix 4.0. Note that the graph widget
supported before Zabbix 4.0 can still be used as [Graph
(classic)](/manual/web_interface/frontend_sections/dashboards/widgets/graph_classic).
See also [*Adding widgets*](/manual/web_interface/frontend_sections/dashboards#adding-widgets)
section on *Dashboards* page for more details.

[comment]: # ({/bd8ad980-21bacb3e})

[comment]: # ({01e38353-b1257d27})
#### Configuration

To configure, select *Graph* as type:

![](../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/graph_dataset.png){width="600"}

[comment]: # ({/01e38353-b1257d27})

[comment]: # ({fc20d2c5-d1f705fb})
#### Data set

The **Data set** tab allows to add data sets and define their visual
representation:

|   |   |   |
|-|----------|----------------------------------------|
|*Data set*|<|Enter the host and item patterns; data of items that match the entered patterns is displayed on the graph; up to 50 items may be displayed. Host pattern and item pattern parameters are mandatory.<br><br>Wildcard patterns may be used for selection (for example, `*` will return results that match zero or more characters).<br>To specify a wildcard pattern, just enter the string manually and press *Enter*.<br>The wildcard symbol is always interpreted, therefore, it is not possible to add, for example, an item named *item\** individually, if there are other matching items (e.g., item2, item3). See also: [Data set configuration details](#data-set-configuration-details).<br><br>**Alternatively** to specifying item patterns, you may select a list of items if the data set has been added with the *Item list* option (see the description of the *Add new data set* button).<br><br>When configuring the widget on a [template dashboard](/manual/config/templates/template#adding-dashboards), the parameter for specifying host patterns is not available, and the parameter for specifying an item list allows to select only [items configured on the template](/manual/config/templates/template#adding-items-triggers-graphs).|
| |*Draw*|Choose the draw type of the metric.<br>Possible draw types: *Line* (set by default), *Points*, *Staircase*, and *Bar*.<br>Note that if there is only one data point in the line/staircase graph, it is drawn as a point regardless of the draw type. The point size is calculated from the line width, but it cannot be smaller than 3 pixels, even if the line width is less.|
|^|*Stacked*|Mark the checkbox to display data as stacked (filled areas displayed).<br>This option is disabled when *Points* draw type is selected.|
|^|*Width*|Set the line width.<br>This option is available when *Line* or *Staircase* draw type is selected.|
|^|*Point size*|Set the point size.<br>This option is available when *Points* draw type is selected.|
|^|*Transparency*|Set the transparency level.|
|^|*Fill*|Set the fill level.<br>This option is available when *Line* or *Staircase* draw type is selected.|
|^|*Missing data*|Select the option for displaying missing data:<br>**None** - the gap is left empty;<br>**Connected** - two border values are connected;<br>**Treat as 0** - the missing data is displayed as 0 values;<br>**Last known** - the missing data is displayed with the same value as the last known value; not applicable for the *Points* and *Bar* draw type.|
|^|*Y-axis*|Select the side of the graph where the Y-axis will be displayed.|
|^|*Time shift*|Specify time shift if required.<br>You may use [time suffixes](/manual/appendix/suffixes#time_suffixes) in this field. Negative values are allowed.|
|^|*Aggregation function*|Specify which aggregation function to use:<br>**min** - display the smallest value;<br>**max** - display the largest value;<br>**avg** - display the average value;<br>**sum** - display the sum of values;<br>**count** - display the count of values;<br>**first** - display the first value;<br>**last** - display the last value;<br>**none** - display all values (no aggregation).<br><br>Aggregation allows to display an aggregated value for the chosen interval (5 minutes, an hour, a day), instead of all values. See also: [Aggregation in graphs](/manual/config/visualization/graphs/aggregate).|
|^|*Aggregation interval*|Specify the interval for aggregating values.<br>You may use [time suffixes](/manual/appendix/suffixes#time_suffixes) in this field. A numeric value without a suffix will be regarded as seconds.|
|^|*Aggregate*|Specify whether to aggregate:<br>**Each item** - each item in the dataset will be aggregated and displayed separately;<br>**Data set** - all dataset items will be aggregated and displayed as one value.|
|^|*Approximation*|Specify what value to display when more than one value exists per vertical graph pixel:<br>**all** - display the smallest, the largest and the average values;<br>**min** - display the smallest value;<br>**max** - display the largest value;<br>**avg** - display the average value.<br><br>This setting is useful when displaying a graph for a large time period with frequent update interval (such as one year of values collected every 10 minutes).|
|^|*Data set label*|Specify the data set label that is displayed in graph *Data set* configuration and in graph *Legend* (for aggregated data sets).<br>All data sets are numbered including those with a specified *Data set label*. If no label is specified, the data set will be labeled automatically according to its number (e.g. "Data set #2", "Data set #3", etc.). Data set numbering ir recalculated after reordering/dragging data sets.<br>Data set labels that are too long will be shortened to fit where displayed (e.g. "Number of proc...").|

[comment]: # ({/fc20d2c5-d1f705fb})

[comment]: # ({b09054f1-63c8cd81})

Existing data sets are displayed in a list. You may:

-   ![](../../../../../../assets/en/manual/web_interface/frontend_sections/dashboards/move_icon.png) -
    click on the move icon and drag a data set to a new place in the
    list.
-   ![](../../../../../../assets/en/manual/web_interface/frontend_sections/dashboards/expand_icon.png) -
    click on the expand icon to expand data set details. When expanded, this icon turns into a collapse icon.
-   ![](../../../../../../assets/en/manual/web_interface/frontend_sections/dashboards/color_icon.png) -
    click on the color icon to change the base color, either from the color picker or manually. The base color is used to calculate different colors for each item of the data set.
-   ![](../../../../../../assets/en/manual/web_interface/frontend_sections/dashboards/add_data_set.png) -
    click on this button to add an empty data set allowing to select the host/item pattern.
    -   If you click on the downward pointing icon next to the *Add new data set* button, a drop-down menu appears allowing to add a new data set with item pattern/item list or by cloning the currently open data set. If all data sets are collapsed, the *Clone* option is not available.

![](../../../../../../assets/en/manual/web_interface/frontend_sections/dashboards/graph_dataset_add.png)

[comment]: # ({/b09054f1-63c8cd81})

[comment]: # ({4ed99a8a-4bf7d5e9})
##### Data set configuration details

The *host pattern* and *item pattern* fields in the *Data set* tab both recognize full names
or patterns containing a wildcard symbol (\*).
This functionality enables to select all the host names and item names containing the selected pattern.
Most importantly, while typing the item name or item pattern in the *item pattern* field,
only the items that belong to the selected host name(s) are displayed on a drop-down list.
For example, having typed a pattern **z\*** in the *host pattern* field,
the drop-down list displays all the host names containing this pattern: z\*, Zabbix server, and Zabbix proxy.
After pressing *Enter*, this pattern is accepted and is displayed as **z\***.
Similarly, the pattern can be created in the *item pattern* field.
For example, having typed the pattern **a\*** in the *item pattern* field,
the drop-down list displays all the item names containing this pattern:
a\*, Available memory, Available memory in %.

See the image of the *Data set* tab below.

![](../../../../../../assets/en/manual/web_interface/frontend_sections/dashboards/widgets/graph_widget_it_host_pattern_1.png){width="600"}

After pressing *Enter*, this pattern is accepted and is displayed as **a\***,
and all the selected items that belong to the selected host name(s) are displayed above the *Data set* tab.
See the image of the *Data set* tab below.

![](../../../../../../assets/en/manual/web_interface/frontend_sections/dashboards/widgets/graph_widget_it_host_pattern_2.png){width="600"}

[comment]: # ({/4ed99a8a-4bf7d5e9})

[comment]: # ({29174d95-efef79c8})
#### Displaying options

The **Displaying options** tab allows to define history data selection:

![](../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/graph_displaying_options.png)

|   |   |
|--|--------|
|*History data selection*|Set the source of graph data:<br>**Auto** - data are sourced according to the classic graph [algorithm](/manual/config/visualization/graphs/simple#generating_from_historytrends) (default);<br>**History** - data from history;<br>**Trends** - data from trends.|
|*Simple triggers*|Mark the checkbox to show simple triggers as lines with black dashes over the trigger severity color.|
|*Working time*|Mark the checkbox to show working time on the graph. Working time (working days) is displayed in graphs as a white background, while non-working time is displayed in gray (with the *Original blue* default frontend theme).|
|*Percentile line (left)*|Mark the checkbox and enter the percentile value to show the specified percentile as a line on the left Y-axis of the graph.<br>If, for example, a 95% percentile is set, then the percentile line will be at the level where 95 percent of the values fall under.|
|*Percentile line (right)*|Mark the checkbox and enter the percentile value to show the specified percentile as a line on the right Y-axis of the graph.<br>If, for example, a 95% percentile is set, then the percentile line will be at the level where 95 percent of the values fall under.|

[comment]: # ({/29174d95-efef79c8})

[comment]: # ({ef4fe9e7-a747b952})
#### Time period

The **Time period** tab allows to set a custom time period:

![](../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/graph_time_period.png)

|   |   |
|--|--------|
|*Set custom time period*|Mark this checkbox to set the custom time period for the graph (unmarked by default).|
|*From*|Set the start time of the custom time period for the graph.|
|*To*|Set the end time of the custom time period for the graph.|

[comment]: # ({/ef4fe9e7-a747b952})

[comment]: # ({c50c2a8e-ec3778fa})
#### Axes
The **Axes** tab allows to customize how axes are displayed:

![](../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/graph_axes.png)

|   |   |
|--|--------|
|*Left Y*|Mark this checkbox to make left Y-axis visible.<br>The checkbox may be disabled if unselected either in *Data set* or in *Overrides* tab.|
|*Right Y*|Mark this checkbox to make right Y-axis visible.<br>The checkbox may be disabled if unselected either in *Data set* or in *Overrides* tab.|
|*X-Axis*|Unmark this checkbox to hide X-axis (marked by default).|
|*Min*|Set the minimum value of the corresponding axis.<br>Visible range minimum value of Y-axis is specified.|
|*Max*|Set the maximum value of the corresponding axis.<br>Visible range maximum value of Y-axis is specified.|
|*Units*|Choose the unit for the graph axis values from the dropdown.<br>If the *Auto* option is chosen, axis values are displayed using units of the first item of the corresponding axis.<br>*Static* option allows you to assign the corresponding axis' custom name. If the *Static* option is chosen and the *value* input field left blank the corresponding axis' name will only consist of a numeric value.|

[comment]: # ({/c50c2a8e-ec3778fa})

[comment]: # ({cb45a767-b12a63bc})
#### Legend

The **Legend** tab allows to customize the graph legend:

![](../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/graph_legend.png)

|   |   |
|--|--------|
|*Show legend*|Unmark this checkbox to hide the legend on the graph (marked by default).|
|*Display min/max/avg*|Mark this checkbox to display the minimum, maximum and average values of the item in the legend.|
|*Number of rows*|Set the number of legend rows to be displayed.|
|*Number of columns*|Set the number of legend columns to be displayed.|

[comment]: # ({/cb45a767-b12a63bc})

[comment]: # ({e3ceab10-07ed8642})
#### Problems

The **Problems** tab allows to customize the problem display:

![](../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/graph_problems.png){width="600"}

|   |   |
|--|--------|
|*Show problems*|Mark this checkbox to enable problem displaying on the graph (unmarked, i.e., disabled by default).|
|*Selected items only*|Mark this checkbox to include problems for the selected items only to be displayed on the graph.|
|*Problem hosts*|Select the problem hosts to be displayed on the graph.<br><br>Wildcard patterns may be used (for example, `*` will return results that match zero or more characters).<br>To specify a wildcard pattern, just enter the string manually and press *Enter*.<br>While you are typing, note how all matching hosts are displayed in the dropdown.<br><br>This parameter is not available when configuring the widget on a [template dashboard](/manual/config/templates/template#adding-dashboards).|
|*Severity*|Mark the problem severities to be displayed on the graph.|
|*Problem*|Specify the problem's name to be displayed on the graph.|
|*Problem tags*|Specify problem tags to limit the number of problems displayed in the widget.<br>It is possible to include as well as exclude specific tags and tag values. Several conditions can be set. Tag name matching is always case-sensitive.<br><br>There are several operators available for each condition:<br>**Exists** - include the specified tag names;<br>**Equals** - include the specified tag names and values (case-sensitive);<br>**Contains** - include the specified tag names where the tag values contain the entered string (substring match, case-insensitive);<br>**Does not exist** - exclude the specified tag names;<br>**Does not equal** - exclude the specified tag names and values (case-sensitive);<br>**Does not contain** - exclude the specified tag names where the tag values contain the entered string (substring match, case-insensitive).<br><br>There are two calculation types for conditions:<br>**And/Or** - all conditions must be met, conditions having the same tag name will be grouped by the *Or* condition;<br>**Or** - enough if one condition is met.|

[comment]: # ({/e3ceab10-07ed8642})

[comment]: # ({b8d2e4c7-bb195e90})
#### Overrides

The **Overrides** tab allows to add custom overrides for data sets:

![](../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/graph_overrides.png){width="600"}

Overrides are useful when several items are selected for a data set
using the `*` wildcard and you want to change how the items are
displayed by default (e.g. default base color or any other property).

Existing overrides (if any) are displayed in a list. To add a new
override:

-   Click on the
    ![](../../../../../../assets/en/manual/web_interface/frontend_sections/dashboards/add_override.png)
    button
-   Select hosts and items for the override. Alternatively, you may
    enter host and item patterns. Wildcard patterns may be used (for
    example, `*` will return results that match zero or more
    characters). To specify a wildcard pattern, just enter the string
    manually and press *Enter*. While you are typing, note how all
    matching hosts are displayed in the dropdown. The wildcard symbol is
    always interpreted, therefore it is not possible to add, for
    example, an item named "item\*" individually if there are other
    matching items (e.g. item2, item3). Host pattern and item pattern
    parameters are mandatory.
    The parameter for specifying host patterns is not available when configuring the widget on a [template dashboard](/manual/config/templates/template#adding-dashboards).
    The parameter for specifying an item list allows to select only [items configured on the template](/manual/config/templates/template#adding-items-triggers-graphs) when configuring the widget on a [template dashboard](/manual/config/templates/template#adding-dashboards).
-   Click on
    ![](../../../../../../assets/en/manual/web_interface/frontend_sections/dashboards/add_override2.png),
    to select override parameters. At least one override parameter
    should be selected. For parameter descriptions, see the *Data set*
    tab above.

Information displayed by the graph widget can be downloaded as a .png
image using the [widget
menu](/manual/web_interface/frontend_sections/dashboards#widget_menu):

![](../../../../../../assets/en/manual/web_interface/frontend_sections/dashboards/graph_widget_as_png.png)

A screenshot of the widget will be saved to the Downloads folder.

[comment]: # ({/b8d2e4c7-bb195e90})
