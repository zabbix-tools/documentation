[comment]: # ({5d9c8f6f-5d9c8f6f})
# 13 Map

[comment]: # ({/5d9c8f6f-5d9c8f6f})

[comment]: # ({3ec267cf-4389647e})
#### Overview

In the map widget you can display either:

-   a single configured network map;
-   one of the configured network maps in the [map navigation tree](/manual/web_interface/frontend_sections/dashboards/widgets/map_tree)
    (when clicking on the map name in the tree).

[comment]: # ({/3ec267cf-4389647e})

[comment]: # ({3ae5fed8-574343be})
#### Configuration

To configure, select *Map* as type:

![](../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/map.png){width="600"}

In addition to the parameters that are [common](/manual/web_interface/frontend_sections/dashboards/widgets#common-parameters) 
for all widgets, you may set the following specific options:

|   |   |
|--|--------|
|*Source type*|Select to display:<br>**Map** - network map;<br>**Map navigation tree** - one of the maps in the selected map navigation tree.|
|*Map*|Select the map to display.<br>This field is auto-complete so starting to type the name of a map will offer a dropdown of matching maps.<br>This option is available if 'Map' is selected as *Source type*.|
|*Filter*|Select the map navigation tree to display the maps of.<br>This option is available if 'Map navigation tree' is selected as *Source type*.|

[comment]: # ({/3ae5fed8-574343be})
