[comment]: # ({4b675dc4-4b675dc4})
# 18 Problems by severity

[comment]: # ({/4b675dc4-4b675dc4})

[comment]: # ({fd83b094-5f30193d})
#### Overview

In this widget, you can display the problem count by severity. You can limit what
hosts and triggers are displayed in the widget and define how the
problem count is displayed.

The problem count is displayed only for cause problems.

[comment]: # ({/fd83b094-5f30193d})

[comment]: # ({941ac1ff-20ab3b4d})
#### Configuration

To configure, select *Problems by severity* as type:

![](../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/problems_by_severity.png){width="600"}

In addition to the parameters that are [common](/manual/web_interface/frontend_sections/dashboards/widgets#common-parameters) 
for all widgets, you may set the following specific options:

|Parameter|Description|
|--|--------|
|*Host groups*|Enter host groups to display in the widget.<br>This field is auto-complete so starting to type the name of a group will offer a dropdown of matching groups.<br>Specifying a parent host group implicitly selects all nested host groups.<br>Host data from these host groups will be displayed in the widget; if no host groups are entered, all host groups will be displayed.<br>This parameter is not available when configuring the widget on a [template dashboard](/manual/config/templates/template#adding-dashboards).|
|*Exclude host groups*|Enter host groups to hide from the widget.<br>This field is auto-complete so starting to type the name of a group will offer a dropdown of matching groups.<br>Specifying a parent host group implicitly selects all nested host groups.<br>Host data from these host groups will not be displayed in the widget. For example, hosts 001, 002, 003 may be in Group A and hosts 002, 003 in Group B as well. If we select to *show* Group A and *exclude* Group B at the same time, only data from host 001 will be displayed in the dashboard.<br>This parameter is not available when configuring the widget on a [template dashboard](/manual/config/templates/template#adding-dashboards).|
|*Hosts*|Enter hosts to display in the widget.<br>This field is auto-complete so starting to type the name of a host will offer a dropdown of matching hosts.<br>If no hosts are entered, all hosts will be displayed.<br>This parameter is not available when configuring the widget on a [template dashboard](/manual/config/templates/template#adding-dashboards).|
|*Problem*|You can limit the number of problem hosts displayed by the problem name.<br>If you enter a string here, only those hosts with problems whose name contains the entered string will be displayed.<br>Macros are not expanded.|
|*Severity*|Mark the problem severities to be displayed in the widget.|
|*Problem tags*|Specify problem tags to limit the number of problems displayed in the widget.<br>It is possible to include as well as exclude specific tags and tag values. Several conditions can be set. Tag name matching is always case-sensitive.<br><br>There are several operators available for each condition:<br>**Exists** - include the specified tag names;<br>**Equals** - include the specified tag names and values (case-sensitive);<br>**Contains** - include the specified tag names where the tag values contain the entered string (substring match, case-insensitive);<br>**Does not exist** - exclude the specified tag names;<br>**Does not equal** - exclude the specified tag names and values (case-sensitive);<br>**Does not contain** - exclude the specified tag names where the tag values contain the entered string (substring match, case-insensitive).<br><br>There are two calculation types for conditions:<br>**And/Or** - all conditions must be met, conditions having the same tag name will be grouped by the *Or* condition;<br>**Or** - enough if one condition is met.|
|*Show*|Select the show option:<br>**Host groups** - display problems per host group;<br>**Totals** - display a problem total for all selected host groups in colored blocks corresponding to the problem severity.<br>This parameter is not available when configuring the widget on a [template dashboard](/manual/config/templates/template#adding-dashboards), and only a problem total will be displayed.|
|*Layout*|Select the layout option:<br>**Horizontal** - colored blocks of totals will be displayed horizontally;<br>**Vertical** - colored blocks of totals will be displayed vertically.<br>This parameter is available if *Show* is set to "Totals".|
|*Show suppressed problems*|Mark the checkbox to display problems that would otherwise be suppressed (not shown) because of host maintenance.|
|*Hide groups without problems*|Mark the *Hide groups without problems* option to hide data from host groups without problems in the widget.<br>This parameter is not available when configuring the widget on a [template dashboard](/manual/config/templates/template#adding-dashboards).|
|*Show operational data*|Mark the checkbox to display operational data (see description of *[Operational data](/manual/web_interface/frontend_sections/monitoring/problems#overview)* in *Monitoring* → *Problems*).|
|*Problem display*|Display problem count as:<br>**All** - full problem count will be displayed;<br>**Separated** - unacknowledged problem count will be displayed separated as a number of the total problem count;<br>**Unacknowledged only** - only the unacknowledged problem count will be displayed.|
|*Show timeline*|Mark the checkbox to display a visual timeline.|

[comment]: # ({/941ac1ff-20ab3b4d})
