[comment]: # ({9ee1626a-9ee1626a})
# 10 Graph prototype

[comment]: # ({/9ee1626a-9ee1626a})

[comment]: # ({8c5a70cd-8c5a70cd})
#### Overview

In the graph prototype widget, you can display a grid of graphs created
from either a graph prototype or an item prototype by low-level
discovery.

[comment]: # ({/8c5a70cd-8c5a70cd})

[comment]: # ({1fb48451-59ea4161})
#### Configuration

To configure, select *Graph prototype* as widget type:

![](../../../../../../assets/en/manual/web_interface/frontend_sections/dashboards/widgets/graph_prototype.png)

In addition to the parameters that are [common](/manual/web_interface/frontend_sections/dashboards/widgets#common-parameters) 
for all widgets, you may set the following specific options:

|   |   |
|--|--------|
|*Source*|Select source: either a **Graph prototype** or a **Simple graph prototype**.|
|*Graph prototype*|Select a graph prototype to display discovered graphs of the graph prototype.<br>This option is available if 'Graph prototype' is selected as Source.|
|*Item prototype*|Select an item prototype to display simple graphs based on discovered items of an item prototype.<br>This option is available if 'Simple graph prototype' is selected as Source.|
|*Show legend*|Mark this checkbox to show the legend on the graphs (marked by default).|
|*[Enable host selection](/manual/web_interface/frontend_sections/dashboards#dynamic_widgets)*|Set graphs to display different data depending on the selected host.|
|*Columns*|Enter the number of columns of graphs to display within a graph prototype widget.|
|*Rows*|Enter the number of rows of graphs to display within a graph prototype widget.|

While the *Columns* and *Rows* settings allow fitting more than one
graph in the widget, there still may be more discovered graphs than
there are columns/rows in the widget. In this case paging becomes
available in the widget and a slide-up header allows to switch between
pages using the left and right arrows.

![](../../../../../../assets/en/manual/config/visualization/host_dashboards_discovered.png)

[comment]: # ({/1fb48451-59ea4161})
