[comment]: # ({d220a982-d220a982})
# 12 Item value

[comment]: # ({/d220a982-d220a982})

[comment]: # ({30c9ce4b-5115c78c})
#### Overview

This widget is useful for displaying the value of a single item 
prominently.

![](../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/item_value_single.png)

Besides the value itself, additional elements can be displayed, if desired:

- time of the metric
- item description
- change indicator for the value
- background color for the value 
- item unit 

The widget can display numeric and string values. Displaying binary values is not supported.

String values are displayed on a single line and truncated, if needed. "No data" is displayed, if there is no value for the item.

Clicking on the value leads to an ad-hoc graph for numeric items or latest data for string items.

The widget and all elements in it can be visually fine-tuned using [advanced configuration](#advanced-configuration) options, allowing to create a 
wide variety of visual styles:

![](../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/item_value_widgets.png)

[comment]: # ({/30c9ce4b-5115c78c})

[comment]: # ({adff5c4a-f59b75d6})
#### Configuration

To configure, select *Item value* as the widget type:

![](../../../../../../assets/en/manual/web_interface/frontend_sections/dashboards/widgets/item_value.png)

In addition to the parameters that are [common](/manual/web_interface/frontend_sections/dashboards/widgets#common-parameters) 
for all widgets, you may set the following specific options:

|   |   |
|--|--------|
|*Item*|Select the item. |
|*Show*|Mark the checkbox to display the respective element (description, value, time, change indicator). Unmark to hide.<br>At least one element must be selected. |
|*Advanced configuration*|Click on the *Advanced configuration* label to display [advanced configuration](#advanced-configuration) options. |
|*[Enable host selection](/manual/web_interface/frontend_sections/dashboards#dynamic-widgets)*|Mark the checkbox to display a different value depending on the selected host. |

[comment]: # ({/adff5c4a-f59b75d6})

[comment]: # ({ffbe4401-002f13c4})
#### Advanced configuration

Advanced configuration options are available in the collapsible *Advanced configuration* section,
and only for those elements that are selected in the *Show* field (see above).

Additionally, advanced configuration allows to change the background 
color - static or dynamic - for the whole widget.

![](../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/item_value_advanced.png)

|   |   |
|--|--------|
|*Description* |Enter the item description. This description may override the default item name. Multiline descriptions are supported. A combination of text and supported macros is possible.<br>{HOST.\*}, {ITEM.\*}, {INVENTORY.\*} and user macros are supported. |
|*Horizontal position* |Select horizontal position of the item description - left, right or center. |
|*Vertical position* |Select vertical position of the item description - top, bottom or middle. |
|*Size* |Enter font size height for the item description (in percent relative to total widget height). |
|*Bold* |Mark the checkbox to display item description in bold type. |
|*Color* |Select the item description color from the color picker.<br>`D` stands for default color (depends on the frontend theme). To return to the default value, click the *Use default* button in the color picker. |
|*Value*| |
|*Decimal places* |Select how many decimal places will be displayed with the value. This value will affect only float items. |
|*Size* |Enter font size height for the decimal places (in percent relative to total widget height). |
|*Horizontal position* |Select horizontal position of the item value - left, right or center. |
|*Vertical position* |Select vertical position of the item value - top, bottom or middle. |
|*Size* |Enter font size height for the item value (in percent relative to total widget height).<br>Note that the size of item value is prioritized; other elements have to concede space for the value. With the change indicator though, if the value is too large, it will be truncated to show the change indicator. |
|*Bold* |Mark the checkbox to display item value in bold type. |
|*Color* |Select the item value color from the color picker.<br>`D` stands for default color (depends on the frontend theme). To return to the default value, click the *Use default* button in the color picker. |
|*Units* |Mark the checkbox to display units with the item value. If you enter a unit name, it will override the unit from item configuration. |
|*Position* |Select the item unit position - above, below, before or after the value. |
|*Size* |Enter font size height for the item unit (in percent relative to total widget height). |
|*Bold* |Mark the checkbox to display item unit in bold type. |
|*Color* |Select the item unit color from the color picker.<br>`D` stands for default color (depends on the frontend theme). To return to the default value, click the *Use default* button in the color picker. |
|*Time*|Time is the clock value from item history. |
|*Horizontal position* |Select horizontal position of the time - left, right or center. |
|*Vertical position* |Select vertical position of the time - top, bottom or middle. |
|*Size* |Enter font size height for the time (in percent relative to total widget height). |
|*Bold* |Mark the checkbox to display time in bold type. |
|*Color* |Select the time color from the color picker.<br>`D` stands for default color (depends on the frontend theme). To return to the default value, click the *Use default* button in the color picker. |
|*Change indicator*|Select the color of change indicators from the color picker. The change indicators are as follows:<br>**↑** - item value is up (for numeric items)<br>**↓** - item value is down (for numeric items)<br>**↕** - item value has changed (for string items and items with value mapping)<br>`D` stands for default color (depends on the frontend theme). To return to the default value, click the *Use default* button in the color picker.<br>Vertical size of the change indicator is equal to the size of the value (integer part of the value for numeric items).<br>Note that up and down indicators are not shown with just one value. |
|*Background color*|Select the background color for the whole widget from the color picker.<br>`D` stands for default color (depends on the frontend theme). To return to the default value, click the *Use default* button in the color picker. |
|*Thresholds*|Configure the dynamic background color for the whole widget. Click *Add* to add a threshold, select the background color from the color picker, and specify a numeric value. Once the item value equals or is greater than the threshold value, the background color will change.<br>The list will be sorted in ascending order when saved. <br> Note that the dynamic background color will be displayed correctly only for numeric items.|

Note that multiple elements cannot occupy the same space; if they are placed in the same space, an error message will be displayed.

[comment]: # ({/ffbe4401-002f13c4})
