[comment]: # ({1b514b16-1b514b16})
# 5 Favorite graphs

[comment]: # ({/1b514b16-1b514b16})

[comment]: # ({5942683c-9ef1ebfc})
#### Overview

This widget contains shortcuts to the most needed graphs, sorted alphabetically.

The list of shortcuts is populated when you
view a graph in Monitoring -> Latest data -> Graphs, and then click on its
![](../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/button_add_fav.png)
*Add to favorites* button.

![](../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/favorite_graphs.png){width="600"}

All configuration parameters are [common](/manual/web_interface/frontend_sections/dashboards/widgets#common-parameters) 
for all widgets.

[comment]: # ({/5942683c-9ef1ebfc})
