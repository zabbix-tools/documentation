[comment]: # ({07287643-07287643})
# 11 Host availability

[comment]: # ({/07287643-07287643})

[comment]: # ({7240fd40-b6291bce})

#### Overview

In the host availability widget, high-level statistics about host
availability are displayed in four colored columns/lines.

|   |   |
|---|---|
|![](../../../../../../assets/en/manual/web_interface/frontend_sections/dashboards/availability_h.png)|Horizontal display (columns).|
|![](../../../../../../assets/en/manual/web_interface/frontend_sections/dashboards/availability_v.png)|Vertical display (lines).|

Host availability in each column/line is counted as follows:

-   *Available* - hosts with all interfaces available
-   *Not available* - hosts with at least one interface unavailable
-   *Unknown* - hosts with at least one interface unknown (none
    unavailable)
-   *Total* - total of all hosts

[comment]: # ({/7240fd40-b6291bce})

[comment]: # ({ccbba52d-521f8782})

#### Configuration

To configure, select *Host availability* as type:

![](../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/host_availability.png)

In addition to the parameters that are [common](/manual/web_interface/frontend_sections/dashboards/widgets#common-parameters) 
for all widgets, you may set the following specific options:

|   |   |
|--|--------|
|*Host groups*|Select host groups.<br>This field is auto-complete so starting to type the name of a group will offer a dropdown of matching groups. Scroll down to select. Click on 'x' to remove the selected.<br>This parameter is not available when configuring the widget on a [template dashboard](/manual/config/templates/template#adding-dashboards).|
|*Interface type*|Select which host interfaces you want to see availability data for.<br>Availability of all interfaces is displayed by default if nothing is selected.|
|*Layout*|Select horizontal display (columns) or vertical display (lines).|
|*Show hosts in maintenance*|Include hosts that are in maintenance in the statistics.<br>This parameter is labeled *Show data in maintenance* when configuring the widget on a [template dashboard](/manual/config/templates/template#adding-dashboards).|

[comment]: # ({/ccbba52d-521f8782})
