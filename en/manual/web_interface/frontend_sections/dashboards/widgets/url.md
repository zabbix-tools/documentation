[comment]: # ({8f310831-8f310831})
# 23 URL

[comment]: # ({/8f310831-8f310831})

[comment]: # ({de63a307-de63a307})
#### Overview

This widget displays the content retrieved from the specified URL.

[comment]: # ({/de63a307-de63a307})

[comment]: # ({dd73f3d8-b6837968})
#### Configuration

To configure, select *URL* as type:

![](../../../../../../assets/en/manual/web_interface/frontend_sections/dashboards/widgets/url.png)

In addition to the parameters that are [common](/manual/web_interface/frontend_sections/dashboards/widgets#common-parameters) 
for all widgets, you may set the following specific options:

|   |   |
|--|--------|
|*URL*|Enter the URL (up to 2048 characters) to display.<br>Relative paths are allowed since Zabbix 4.4.8.<br>{HOST.\*} macros are supported.|
|*[Enable host selection](/manual/web_interface/frontend_sections/dashboards#dynamic_widgets)*|Set to display different URL content depending on the selected host.<br>This can work if {HOST.\*} macros are used in the URL.|

::: noteimportant
Browsers might not load an HTTP page included in
the widget if Zabbix frontend is accessed over HTTPS.
:::

[comment]: # ({/dd73f3d8-b6837968})
