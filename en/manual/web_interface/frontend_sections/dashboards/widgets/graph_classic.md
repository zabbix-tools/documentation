[comment]: # ({84b81c15-84b81c15})
# 9 Graph (classic)

[comment]: # ({/84b81c15-84b81c15})

[comment]: # ({57f0025a-57f0025a})
#### Overview

In the classic graph widget, you can display a single custom graph or
simple graph.

[comment]: # ({/57f0025a-57f0025a})

[comment]: # ({9988b6c8-70ee38b8})
#### Configuration

To configure, select *Graph (classic)* as type:

![](../../../../../../assets/en/manual/web_interface/frontend_sections/dashboards/widgets/graph_classic.png)

In addition to the parameters that are [common](/manual/web_interface/frontend_sections/dashboards/widgets#common-parameters) 
for all widgets, you may set the following specific options:

|   |   |
|--|--------|
|*Source*|Select graph type:<br>**Graph** - custom graph<br>**Simple graph** - simple graph|
|*Graph*|Select the custom graph to display.<br>This option is available if 'Graph' is selected as *Source*.|
|*Item*|Select the item to display in a simple graph.<br>This option is available if 'Simple graph' is selected as *Source*.|
|*Show legend*|Unmark this checkbox to hide the legend on the graph (marked by default).|
|*[Enable host selection](/manual/web_interface/frontend_sections/dashboards#dynamic_widgets)*|Set graph to display different data depending on the selected host.|

Information displayed by the classic graph widget can be downloaded as
.png image using the [widget
menu](/manual/web_interface/frontend_sections/dashboards#widget_menu):

![](../../../../../../assets/en/manual/web_interface/frontend_sections/dashboards/graph_download.png)

A screenshot of the widget will be saved to the Downloads folder.

[comment]: # ({/9988b6c8-70ee38b8})
