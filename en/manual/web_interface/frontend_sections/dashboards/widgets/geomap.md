[comment]: # ({2091b9ba-2091b9ba})
# 7 Geomap

[comment]: # ({/2091b9ba-2091b9ba})

[comment]: # ({ea309297-ea309297})
#### Overview

Geomap widget displays hosts as markers on a geographical map using
open-source JavaScript interactive maps library Leaflet.

::: noteclassic
 Zabbix offers multiple predefined map tile service
providers and an option to add a custom tile service provider or even
host tiles themselves (configurable in the *Administration → General →
Geographical maps* [menu
section](/manual/web_interface/frontend_sections/administration/general#geographical_maps)).
:::

[comment]: # ({/ea309297-ea309297})

[comment]: # ({8e1da957-8e1da957})
By default, the widget displays all enabled hosts with valid geographical coordinates defined in the host configuration. It is
possible to configure host filtering in the widget parameters.

The valid host coordinates are: 

- Latitude: from -90 to 90 (can be integer or float number) 
- Longitude: from -180 to 180 (can be integer or float number) 

[comment]: # ({/8e1da957-8e1da957})

[comment]: # ({98ed7bd1-08db40dc})
#### Configuration

To add the widget, select *Geomap* as type.

![](../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/geomap.png)

[comment]: # ({/98ed7bd1-08db40dc})

[comment]: # ({11bc7c7b-e0aba304})
In addition to the parameters that are [common](/manual/web_interface/frontend_sections/dashboards/widgets#common-parameters) 
for all widgets, you may set the following specific options:

|   |   |
|--|--------|
|*Host groups*|Select host groups to be displayed on the map.<br>This field is auto-complete so starting to type the name of a group will offer a dropdown of matching groups.<br>Scroll down to select. Click on 'x' to remove selected groups.<br>If nothing is selected in both *Host groups* and *Hosts* fields, all hosts with valid coordinates will be displayed.<br>This parameter is not available when configuring the widget on a [template dashboard](/manual/config/templates/template#adding-dashboards).|
|*Hosts*|Select hosts to be displayed all the map.<br>This field is auto-complete so starting to type the name of a host will offer a dropdown of matching hosts.<br>Scroll down to select. Click on 'x' to remove selected hosts.<br>If nothing is selected in both *Host groups* and *Hosts* fields, all hosts with valid coordinates will be displayed.<br>This parameter is not available when configuring the widget on a [template dashboard](/manual/config/templates/template#adding-dashboards).|
|*Tags*|Specify tags to limit the number of hosts displayed in the widget.<br>It is possible to include as well as exclude specific tags and tag values. Several conditions can be set. Tag name matching is always case-sensitive.<br><br>There are several operators available for each condition:<br>**Exists** - include the specified tag names;<br>**Equals** - include the specified tag names and values (case-sensitive);<br>**Contains** - include the specified tag names where the tag values contain the entered string (substring match, case-insensitive);<br>**Does not exist** - exclude the specified tag names;<br>**Does not equal** - exclude the specified tag names and values (case-sensitive);<br>**Does not contain** - exclude the specified tag names where the tag values contain the entered string (substring match, case-insensitive).<br><br>There are two calculation types for conditions:<br>**And/Or** - all conditions must be met, conditions having the same tag name will be grouped by the *Or* condition;<br>**Or** - enough if one condition is met.<br><br>This parameter is not available when configuring the widget on a [template dashboard](/manual/config/templates/template#adding-dashboards).|
|*Initial view*|Comma-separated center coordinates and an optional zoom level to display when the widget is initially loaded in the format `<latitude>,<longitude>,<zoom>`<br>If initial zoom is specified, the Geomap widget is loaded at the given zoom level. Otherwise, initial zoom is calculated as half of the [max zoom](/manual/web_interface/frontend_sections/administration/general#geographical_maps) for the particular tile provider.<br>The initial view is ignored if the default view is set (see below).<br>Examples:<br>40.6892494,-74.0466891,14<br>40.6892494,-122.0466891|

[comment]: # ({/11bc7c7b-e0aba304})

[comment]: # ({d6bd2545-620ce415})
Host markers displayed on the map have the color of the host's most
serious problem and green color if a host has no problems. Clicking on
a host marker allows viewing the host's visible name and the number of
unresolved problems grouped by severity. Clicking on the visible name
will open [host
menu](/manual/web_interface/menu/host_menu).

Hosts displayed on the map can be filtered by problem severity. Press on
the filter icon in the widget's upper right corner and mark the required
severities.

![](../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/geomap_widget_filter.png)

[comment]: # ({/d6bd2545-620ce415})

[comment]: # ({496470de-e9bb04a4})
It is possible to zoom in and out the map by using the plus and minus
buttons in the widget's upper left corner or by using the mouse scroll
wheel or touchpad. To set the current view as default, right-click
anywhere on the map and select *Set this view as default*. This setting
will override *Initial view* widget parameter for the current user. To
undo this action, right-click anywhere on the map again and select
*Reset to initial view*.

When *Initial view* or *Default view* is set, you can return to this
view at any time by pressing on the home icon on the left.

![](../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/geomap_widget3.png)

[comment]: # ({/496470de-e9bb04a4})

