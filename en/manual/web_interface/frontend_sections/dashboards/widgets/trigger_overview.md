[comment]: # ({da50605d-da50605d})
# 22 Trigger overview

[comment]: # ({/da50605d-da50605d})

[comment]: # ({6450b9c4-8f45cbdc})
#### Overview

In the trigger overview widget, you can display the trigger states for a group of hosts.

-   The trigger states are displayed as colored blocks
    (the color of the blocks for PROBLEM triggers depends on the problem severity color, which can be adjusted in the [problem update](/manual/acknowledgment#updating_problems) screen).
    Note that recent trigger state changes (within the last 2 minutes) will be displayed as blinking blocks.
-   Gray up and down arrows indicate triggers that have dependencies. On mouseover, dependency details are revealed.
-   A checkbox icon indicates acknowledged problems.
    All problems or resolved problems of the trigger must be acknowledged for this icon to be displayed.

Clicking on a trigger block provides context-dependent links to problem
events of the trigger, the problem acknowledgment screen, trigger
configuration, trigger URL or a simple graph/latest values list.

Note that 50 records are displayed by default (configurable in
*Administration* → *General* →
*[GUI](/manual/web_interface/frontend_sections/administration/general#gui)*,
using the *Max number of columns and rows in overview tables* option).
If more records exist than are configured to display, a message is
displayed at the bottom of the table, asking to provide more specific
filtering criteria. There is no pagination. Note that this limit is applied 
first, before any further filtering of data, for example, by tags.

[comment]: # ({/6450b9c4-8f45cbdc})

[comment]: # ({e01ff8e0-d9e485e9})
#### Configuration

To configure, select *Trigger overview* as type:

![](../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/trigger_overview.png){width="600"}

In addition to the parameters that are [common](/manual/web_interface/frontend_sections/dashboards/widgets#common-parameters) 
for all widgets, you may set the following specific options:

|   |   |
|--|--------|
|*Show*|Filter triggers by trigger state:<br>**Recent problems** - *(default)* show triggers that recently have been or still are in a PROBLEM state (resolved and unresolved);<br>**Problems** - show triggers that are in a PROBLEM state (unresolved);<br>**Any** - show all triggers.|
|*Host groups*|Select the host group(s).<br>This field is auto-complete, so starting to type the name of a group will offer a dropdown of matching groups.<br>This parameter is not available when configuring the widget on a [template dashboard](/manual/config/templates/template#adding-dashboards).|
|*Hosts*|Select hosts.<br>This field is auto-complete, so starting to type the name of a host will offer a dropdown of matching hosts.<br>Scroll down to select. Click on 'x' to remove the selected.<br>This parameter is not available when configuring the widget on a [template dashboard](/manual/config/templates/template#adding-dashboards).|
|*Problem tags*|Specify tags to filter the triggers displayed in the widget.<br>It is possible to include as well as exclude specific tags and tag values. Several conditions can be set. Tag name matching is always case-sensitive.<br><br>**Note:** If the parameter *Show* is set to 'Any', all triggers will be displayed even if tags are specified. However, while recent trigger state changes (displayed as blinking blocks) will update for all triggers, the trigger state details (problem severity color and whether the problem is acknowledged) will only update for triggers that match the specified tags.<br><br>There are several operators available for each condition:<br>**Exists** - include the specified tag names;<br>**Equals** - include the specified tag names and values (case-sensitive);<br>**Contains** - include the specified tag names where the tag values contain the entered string (substring match, case-insensitive);<br>**Does not exist** - exclude the specified tag names;<br>**Does not equal** - exclude the specified tag names and values (case-sensitive);<br>**Does not contain** - exclude the specified tag names where the tag values contain the entered string (substring match, case-insensitive).<br><br>There are two calculation types for conditions:<br>**And/Or** - all conditions must be met, conditions having the same tag name will be grouped by the *Or* condition;<br>**Or** - enough if one condition is met.|
|*Show suppressed problems*|Mark the checkbox to display problems that would otherwise be suppressed (not shown) because of host maintenance.|
|*Hosts location*|Select host location - left or top.<br>This parameter is labeled *Host location* when configuring the widget on a [template dashboard](/manual/config/templates/template#adding-dashboards).|

[comment]: # ({/e01ff8e0-d9e485e9})
