[comment]: # ({38b63d3a-38b63d3a})
# 1 Action log

[comment]: # ({/38b63d3a-38b63d3a})

[comment]: # ({ab1339fd-b37edab5})
#### Overview

In the action log widget, you can display details of action operations (notifications, remote commands).
It replicates information from *Reports → [Action log](/manual/web_interface/frontend_sections/reports/action_log)*.

[comment]: # ({/ab1339fd-b37edab5})

[comment]: # ({f6794bf2-95422d28})
#### Configuration

To configure, select *Action log* as type:

![](../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/action_log.png){width="600"}

In addition to the parameters that are [common](/manual/web_interface/frontend_sections/dashboards/widgets#common-parameters) 
for all widgets, you may set the following specific options:

|   |   |
|--|--------|
|*Recipients*|Filter entries by recipients. This field is auto-complete, so starting to type the name of a recipient will offer a dropdown of matching recipients. If no recipients are selected, details of action operations for all recipients will be displayed.|
|*Actions*|Filter entries by actions. This field is auto-complete, so starting to type the name of an action will offer a dropdown of matching actions. If no actions are selected, details of action operations for all actions will be displayed.|
|*Media types*|Filter entries by media types. This field is auto-complete, so starting to type the name of a media type will offer a dropdown of matching media types. If no media types are selected, details of action operations for all media types will be displayed.|
|*Status*|Mark the checkbox to filter entries by the respective status:<br>**In progress** - action operations that are in progress are displayed<br>**Sent/Executed** - action operations that have sent a notification or have been executed are displayed<br>**Failed** - action operations that have failed are displayed|
|*Search string*|Filter entries by the content of the message/remote command. If you enter a string here, only those action operations whose message/remote command contains the entered string will be displayed. Macros are not resolved.|
|*Sort entries by*|Sort entries by:<br>**Time** (descending or ascending)<br>**Type** (descending or ascending)<br>**Status** (descending or ascending)<br>**Recipient** (descending or ascending)|
|*Show lines*|Set how many action log lines will be displayed in the widget.|

[comment]: # ({/f6794bf2-95422d28})
