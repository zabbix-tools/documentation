[comment]: # ({04251815-04251815})
# Dashboard widgets

[comment]: # ({/04251815-04251815})

[comment]: # ({0491865d-6b9b82d5})
#### Overview

This section provides the details of parameters that are 
common for [dashboard](/manual/web_interface/frontend_sections/dashboards)
widgets.

#### Common parameters

The following parameters are common for every single widget:

|   |   |
|--|--------|
|*Name*|Enter a widget name.|
|*Refresh interval*|Configure default refresh interval. Default refresh intervals for widgets range from *No refresh* to *15 minutes* depending on the type of widget. For example: *No refresh* for URL widget, *1 minute* for action log widget, *15 minutes* for clock widget.|
|*Show header*|Mark the checkbox to show the header permanently.<br>When unchecked the header is hidden to save space and only slides up and becomes visible again when the mouse is positioned over the widget, both in view and edit modes. It is also semi-visible when dragging a widget to a new place.|

Refresh intervals for a widget can be set to a default value for all the
corresponding users and also each user can set his own refresh interval
value:

-   To set a default value for all the corresponding users switch to
    editing mode (click the *Edit dashboard* button, find the right
    widget, click the *Edit* button opening the editing form of a
    widget), and choose the required refresh interval from the dropdown
    list.
-   Setting a unique refresh interval for each user separately is
    possible in view mode by clicking the
    ![](../../../../../../assets/en/manual/web_interface/frontend_sections/dashboards/edit_button_widget_view.png)
    button for a certain widget.

Unique refresh interval set by a user has priority over the widget
setting and once it's set it's always preserved when the widget's
setting is modified.

To see **specific parameters** for each widget, go to individual widget pages 
for:

-   [Action log](/manual/web_interface/frontend_sections/dashboards/widgets/action_log)
-   [Clock](/manual/web_interface/frontend_sections/dashboards/widgets/clock)
-   [Data overview](/manual/web_interface/frontend_sections/dashboards/widgets/data_overview)
-   [Discovery status](/manual/web_interface/frontend_sections/dashboards/widgets/discovery_status)
-   [Favorite graphs](/manual/web_interface/frontend_sections/dashboards/widgets/favorite_graphs)
-   [Favorite maps](/manual/web_interface/frontend_sections/dashboards/widgets/favorite_maps)
-   [Geomap](/manual/web_interface/frontend_sections/dashboards/widgets/geomap)
-   [Graph](/manual/web_interface/frontend_sections/dashboards/widgets/graph)
-   [Graph (classic)](/manual/web_interface/frontend_sections/dashboards/widgets/graph_classic)
-   [Graph prototype](/manual/web_interface/frontend_sections/dashboards/widgets/graph_prototype)
-   [Host availability](/manual/web_interface/frontend_sections/dashboards/widgets/host_availability)
-   [Item value](/manual/web_interface/frontend_sections/dashboards/widgets/item_value)
-   [Map](/manual/web_interface/frontend_sections/dashboards/widgets/map)
-   [Map navigation tree](/manual/web_interface/frontend_sections/dashboards/widgets/map_tree)
-   [Plain text](/manual/web_interface/frontend_sections/dashboards/widgets/plain_text)
-   [Problem hosts](/manual/web_interface/frontend_sections/dashboards/widgets/problem_hosts)
-   [Problems](/manual/web_interface/frontend_sections/dashboards/widgets/problems)
-   [Problems by severity](/manual/web_interface/frontend_sections/dashboards/widgets/problems_severity)
-   [SLA report](/manual/web_interface/frontend_sections/dashboards/widgets/sla_report)
-   [System information](/manual/web_interface/frontend_sections/dashboards/widgets/system)
-   [Top hosts](/manual/web_interface/frontend_sections/dashboards/widgets/top_hosts)
-   [Trigger overview](/manual/web_interface/frontend_sections/dashboards/widgets/trigger_overview)
-   [URL](/manual/web_interface/frontend_sections/dashboards/widgets/url)
-   [Web monitoring](/manual/web_interface/frontend_sections/dashboards/widgets/web_monitoring)

[comment]: # ({/0491865d-6b9b82d5})

[comment]: # ({49f06c21-b3c5b55d})
Deprecated widgets:

-   [Data overview](/manual/web_interface/frontend_sections/dashboards/widgets/data_overview)

[comment]: # ({/49f06c21-b3c5b55d})

[comment]: # ({7132e239-7132e239})
:::noteimportant
Deprecated widgets will be removed in upcoming major release.
:::

[comment]: # ({/7132e239-7132e239})
