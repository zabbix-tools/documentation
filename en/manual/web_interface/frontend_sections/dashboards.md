[comment]: # (tags: dashboard, screen)


[comment]: # ({c51ed5d4-e22ef2ab})
# 1 Dashboards

[comment]: # ({/c51ed5d4-e22ef2ab})

[comment]: # ({59d5bafb-6e26b249})
#### Overview

The *Dashboards* section is designed to display summaries of
all the important information in a **dashboard**.

While only one dashboard can displayed at one time, it is possible to
configure several dashboards. Each dashboard may contain one or several
pages that can be rotated in a slideshow.

A dashboard page consists of widgets and each widget is designed to
display information of a certain kind and source, which can be a
summary, a map, a graph, the clock, etc.

Access to hosts in the widgets depends on host [permissions](/manual/config/users_and_usergroups/permissions#access-to-hosts).

![](../../../../assets/en/manual/web_interface/frontend_sections/dashboards/dashboard.png){width="600"}

Pages and widgets are added to the dashboard and edited in the dashboard
editing mode. Pages can be viewed and rotated in the dashboard viewing
mode.

The time period that is displayed in graph widgets is controlled by the
[time period
selector](/manual/config/visualization/graphs/simple#time_period_selector)
located above the widgets. The time period selector label, located to
the right, displays the currently selected time period. Clicking the tab
label allows expanding and collapsing the time period selector.

Note that when the dashboard is displayed in kiosk mode and widgets only
are displayed, it is possible to zoom out the graph period by
double-clicking in the graph.

[comment]: # ({/59d5bafb-6e26b249})

[comment]: # ({0dd1dd24-33700391})

##### Dashboard size

The minimum width of a dashboard is 1200 pixels. The dashboard will not shrink below this width; instead a 
horizontal scrollbar is displayed if the browser window is smaller than that.

The maximum width of a dashboard is the browser window width. Dashboard widgets stretch horizontally to fit the window. 
At the same time, a dashboard widget cannot be stretched horizontally beyond the window limits.

Technically the dashboard consists of 12 horizontal columns of always equal width that stretch/shrink dynamically (but 
not to less than 1200 pixels total).

Vertically the dashboard may contain a maximum of 64 rows. Each row has a fixed height of 70 pixels. A widget may be 
up to 32 rows high.

[comment]: # ({/0dd1dd24-33700391})

[comment]: # ({ea1c782d-66f99095})
#### Viewing dashboards

To view all configured dashboards, click on *All dashboards* just below
the section title.

![](../../../../assets/en/manual/web_interface/frontend_sections/dashboards/dashboards.png)

Dashboards are displayed with a [sharing](#sharing) tag:

-   *My* - indicates a private dashboard
-   *Shared* - indicates a public dashboard or a private dashboard
    shared with any user or user group

The filter located to the right above the list allows to filter
dashboards by name and by those created by the current user.

To delete one or several dashboards, mark the checkboxes of the
respective dashboards and click on *Delete* below the list.

[comment]: # ({/ea1c782d-66f99095})

[comment]: # ({fe41bbb5-211bf3e2})
#### Viewing a dashboard

To view a single dashboard, click on its name in the list of dashboards.

When **viewing** a dashboard, the following options are available:

|   |   |   |
|-|----------|----------------------------------------|
|![](../../../../assets/en/manual/web_interface/dashboard_edit_button.png){width="107"}|<|Switch to the dashboard **editing** mode.<br>The editing mode is also opened when a new dashboard is being created and when you click on the ![](../../../../assets/en/manual/web_interface/widget_edit.png) edit button of a widget.|
|![](../../../../assets/en/manual/web_interface/dashboard_action_menu2.png)|<|Open the action menu (see action descriptions below).|
|![](../../../../assets/en/manual/web_interface/frontend_sections/dashboards/dashboard_view_actions.png){width="208"}|<|*Sharing* - edit [sharing preferences](#sharing) for the dashboard.<br>*Create new* - [create](#creating_a_dashboard) a new dashboard.<br>*Clone* - create a new dashboard by copying properties of the existing one. First you are prompted to enter dashboard parameters. Then, the new dashboard opens in editing mode with all the widgets of the original dashboard.<br>*Delete* - delete the dashboard.<br>*Create new report* - open a pop-up window with report [configuration form](/manual/config/reports). Disabled if the user does not have permission to manage scheduled reports.<br>*View related reports* - open a pop-up window with a list of existing reports based on the current dashboard. Disabled if there are no related reports or the user does not have permission to view scheduled reports.|
|![](../../../../assets/en/manual/web_interface/button_kiosk.png)|<|Display only page content ([kiosk mode](/manual/web_interface/frontend_sections/monitoring#view_mode_buttons)).<br>Kiosk mode can also be accessed with the following URL parameters: `/zabbix.php?action=dashboard.view&kiosk=1`.<br>To exit to normal mode: `/zabbix.php?action=dashboard.view&kiosk=0`|

[comment]: # ({/fe41bbb5-211bf3e2})

[comment]: # ({788087e7-da387b0f})
##### Sharing

Dashboards can be made public or private. 

Public dashboards are visible to all users. Private dashboards are visible only to their owner. 
Private dashboards can be shared by the owner with other users and user groups. 

The sharing status of a dashboard is displayed in the list of all dashboards. 
To edit the sharing status of a dashboard, click on the *Sharing* option in 
the action menu when viewing a single dashboard:

|Parameter|Description|
|---------|-----------|
|*Type*|Select dashboard type:<br>**Private** - dashboard is visible only to selected user groups and users<br>**Public** - dashboard is visible to all|
|*List of user group shares*|Select user groups that the dashboard is accessible to.<br>You may allow read-only or read-write access.|
|*List of user shares*|Select users that the dashboard is accessible to.<br>You may allow read-only or read-write access.|

[comment]: # ({/788087e7-da387b0f})

[comment]: # ({538b420a-226e4f93})
#### Editing a dashboard

When **editing** a dashboard, the following options are available:

|   |   |   |
|-|----------|----------------------------------------|
|![](../../../../assets/en/manual/web_interface/dashboard_properties.png)|<|Edit general dashboard [parameters](#creating_a_dashboard).|
|![](../../../../assets/en/manual/web_interface/frontend_sections/dashboards/dashboard_add_button.png)|<|Add a new widget.<br>Clicking on the arrow button will open the action menu (see action descriptions below).|
| |![](../../../../assets/en/manual/web_interface/frontend_sections/dashboards/dashboard_edit_actions.png)|*Add widget* - add a new widget<br>*Add page* - add a new page<br>*Paste widget* - paste a copied widget. This option is grayed out if no widget has been copied. Only one entity (widget or page) can be copied at one time.<br>*Paste page* - paste a copied page. This option is grayed out if no page has been copied.|
|![](../../../../assets/en/manual/web_interface/dashboard_save.png)|<|Save dashboard changes.|
|![](../../../../assets/en/manual/web_interface/dashboard_cancel.png)|<|Cancel dashboard changes.|

[comment]: # ({/538b420a-226e4f93})

[comment]: # ({539b1090-4750aaf1})
#### Creating a dashboard

It is possible to create a new dashboard in two ways:

-   Click on *Create dashboard*, when viewing all dashboards
-   Select *Create new* from the action menu, when viewing a single
    dashboard

You will be first asked to enter general dashboard parameters:

![dashboard\_properties.png](../../../../assets/en/manual/web_interface/frontend_sections/dashboards/dashboard_properties.png)

|Parameter|Description|
|--|--------|
|*Owner*|Select system user that will be the dashboard owner.|
|*Name*|Enter dashboard name.|
|*Default page display period*|Select period for how long a dashboard page is displayed before rotating to the next page in a [slideshow](#creating_a_slideshow).|
|*Start slideshow automatically*|Mark this checkbox to run a slideshow automatically one more than one dashboard page exists.|

When you click on *Apply*, an empty dashboard is opened:

![](../../../../assets/en/manual/web_interface/frontend_sections/dashboards/dashboard_add.png){width="600"}

To populate the dashboard, you can add widgets and pages.

Click on the *Save changes* button to save the dashboard. If you click
on *Cancel*, the dashboard will not be created.

[comment]: # ({/539b1090-4750aaf1})

[comment]: # ({40043ea3-0136f9fd})
#### Adding widgets

To add a widget to a dashboard:

-   Click on the
    ![](../../../../assets/en/manual/web_interface/frontend_sections/dashboards/dashboard_add_button.png)
    button or the *Add widget* option in the action menu that can be
    opened by clicking on the arrow. Fill the widget configuration form.
    The widget will be created in its default size and placed after the
    existing widgets (if any);

Or

-   Move your mouse to the desired empty spot for the new widget. Notice
    how a placeholder appears, on mouseover, on any empty slot on the
    dashboard. Then click to open the widget configuration form. After
    filling the form the widget will be created in its default size or,
    if its default size is bigger than is available, take up the
    available space. Alternatively, you may click and drag the
    placeholder to the desired widget size, then release, and then fill
    the widget configuration form. (Note that when there is a widget
    copied onto the clipboard, you will be first prompted to select
    between *Add widget* and *Paste widget* options to create a widget.)

![](../../../../assets/en/manual/web_interface/dashboard_click.png)

![](../../../../assets/en/manual/web_interface/dashboard_select.png)

In the widget configuration form:

-   Select the *Type* of widget
-   Enter widget parameters
-   Click on *Add*

![](../../../../assets/en/manual/web_interface/frontend_sections/dashboards/widget_add.png)

#### Widgets

A wide variety of [widgets](/manual/web_interface/frontend_sections/dashboards/widgets) (e.g. [Clock](/manual/web_interface/frontend_sections/dashboards/widgets/clock), [Host availability](/manual/web_interface/frontend_sections/dashboards/widgets/host_availability) or [Trigger overview](/manual/web_interface/frontend_sections/dashboards/widgets/trigger_overview)) can be added to a dashboard: these can be resized and moved around the dashboard in dashboard editing mode by clicking on the widget title bar and dragging it to a new location. Also, you can click on the following buttons in the top-right
corner of the widget to:

-   ![](../../../../assets/en/manual/web_interface/widget_edit.png) -
    edit a widget;
-   ![](../../../../assets/en/manual/web_interface/button_widget_menu.png) -
    access the [widget menu](#widget_menu)

Click on *Save changes* for the dashboard to make any changes to the
widgets permanent.

[comment]: # ({/40043ea3-0136f9fd})

[comment]: # ({c9679ae2-c9679ae2})
#### Copying/pasting widgets

Dashboard widgets can be copied and pasted, allowing to create a new
widget with the properties of an existing one. They can be copy-pasted
within the same dashboard, or between dashboards opened in different
tabs.

A widget can be copied using the [widget menu](#widget_menu). To paste
the widget:

-   click on the arrow next to the *Add* button and selecting the *Paste
    widget* option, when editing the dashboard
-   use the *Paste widget* option when adding a new widget by selecting
    some area in the dashboard (a widget must be copied first for the
    paste option to become available)

A copied widget can be used to paste over an existing widget using the
*Paste* option in the [widget menu](#widget_menu).

[comment]: # ({/c9679ae2-c9679ae2})

[comment]: # ({cc1ad546-70c1bf80})
#### Creating a slideshow

A slideshow will run automatically if the dashboard contains two or more
pages (see [Adding pages](#adding_pages)) and if one of the following is
true:

-   The *Start slideshow automatically* option is marked in dashboard
    properties
-   The dashboard URL contains a `slideshow=1` parameter

The pages rotate according to the intervals given in the properties of
the dashboard and individual pages. Click on:

-   *Stop slideshow* - to stop the slideshow
-   *Start slideshow* - to start the slideshow

![](../../../../assets/en/manual/web_interface/frontend_sections/dashboards/dashboard_page_tabs.png)

Slideshow-related controls are also available in [kiosk
mode](#viewing_and_editing_a_dashboard) (where only the page content is
shown):

-   ![](../../../../assets/en/manual/web_interface/frontend_sections/dashboards/slideshow_stop_kiosk.png) -
    stop slideshow
-   ![](../../../../assets/en/manual/web_interface/frontend_sections/dashboards/slideshow_start_kiosk.png) -
    start slideshow
-   ![](../../../../assets/en/manual/web_interface/frontend_sections/dashboards/slideshow_back_kiosk.png) -
    go back one page
-   ![](../../../../assets/en/manual/web_interface/frontend_sections/dashboards/slideshow_next_kiosk.png) -
    go to the next page

[comment]: # ({/cc1ad546-70c1bf80})

[comment]: # ({ca26f29e-53487df8})
#### Adding pages

To add a new page to a dashboard:

-   Make sure the dashboard is in the [editing
    mode](#viewing_and_editing_a_dashboard)
-   Click on the arrow next to the *Add* button and select the *Add
    page* option

![](../../../../assets/en/manual/web_interface/frontend_sections/dashboards/dashboard_page_add.png)

-   Fill the general page parameters and click on *Apply*. If you leave
    the name empty, the page will be added with a `Page N` name where
    'N' is the incremental number of the page. The page display period
    allows to customize how long a page is displayed in a slideshow.

![](../../../../assets/en/manual/web_interface/frontend_sections/dashboards/dashboard_page_properties.png)

A new page will be added, indicated by a new tab (*Page 2*).

![](../../../../assets/en/manual/web_interface/frontend_sections/dashboards/dashboard_page_new.png)

The pages can be reordered by dragging-and-dropping the page tabs.
Reordering maintains the original page naming. It is always possible to
go to each page by clicking on its tab.

When a new page is added, it is empty. You can add widgets to it as
described above.

[comment]: # ({/ca26f29e-53487df8})

[comment]: # ({c2af5c74-90a8c064})
#### Copying/pasting pages

Dashboard pages can be copied and pasted, allowing to create a new page
with the properties of an existing one. They can be pasted from the same
dashboard or a different dashboard.

To paste an existing page to the dashboard, first copy it, using the
[page menu](#page_menu):

![](../../../../assets/en/manual/web_interface/frontend_sections/dashboards/dashboard_page_copy.png)

To paste the copied page:

-   Make sure the dashboard is in the [editing
    mode](#viewing_and_editing_a_dashboard)
-   Click on the arrow next to the *Add* button and select the *Paste
    page* option

[comment]: # ({/c2af5c74-90a8c064})

[comment]: # ({00f487c9-7680d810})
#### Page menu

The page menu can be opened by clicking on the three dots
![](../../../../assets/en/manual/web_interface/frontend_sections/dashboards/dashboard_page_menu.png)
next to the page name:

![](../../../../assets/en/manual/web_interface/frontend_sections/dashboards/page_menu.png)

It contains the following options:

-   *Copy* - copy the page
-   *Delete* - delete the page (pages can only be deleted in the
    dashboard editing mode)
-   *Properties* - customize the page parameters (the name and the page
    display period in a slideshow)

[comment]: # ({/00f487c9-7680d810})

[comment]: # ({1f5816b7-0d0ccdf9})
#### Widget menu

The widget menu contains different options based on whether the
dashboard is in the edit or view mode:

|Widget menu|Options|
|----|------|
|In dashboard edit mode:<br>![widget\_menu\_edit.png](../../../../assets/en/manual/web_interface/widget_menu_edit.png)|*Copy* - copy the widget<br>*Paste* - paste a copied widget over this widget<br>This option is grayed out if no widget has been copied.<br>*Delete* - delete the widget|
|In dashboard view mode:<br>![widget\_menu\_view.png](../../../../assets/en/manual/web_interface/widget_menu_view.png)|*Copy* - copy the widget<br>*Download image* - download the widget as a PNG image<br>(only available for [graph](/manual/web_interface/frontend_sections/dashboards/widgets/graph)/[classic graph](/manual/web_interface/frontend_sections/dashboards/widgets/graph_classic) widgets)<br>*Refresh interval* - select the frequency of refreshing<br>the widget contents|

[comment]: # ({/1f5816b7-0d0ccdf9})

[comment]: # ({e4011f4e-cbf4c212})
#### Dynamic widgets {#dynamic-widgets}

When
[configuring](/manual/web_interface/frontend_sections/dashboards/widgets)
some of the widgets:

-   Classic graph
-   Graph prototype
-   Item value
-   Plain text
-   URL

there is an extra option called *Enable host selection*. You can check this box
to make the widget dynamic - i.e. capable of displaying different
content based on the selected host.

Now, when saving the dashboard, you will notice that a new host
selection field has appeared atop the dashboard for selecting the host
(while the *Select* button allows selecting the host group in a popup):

![](../../../../assets/en/manual/web_interface/frontend_sections/monitoring/dynamic_selection.png)

Thus you have a widget, which can display content that is based on the
data from the host that is selected. The benefit of this is that you do
not need to create extra widgets just because, for example, you want to
see the same graph containing data from various hosts.

[comment]: # ({/e4011f4e-cbf4c212})

[comment]: # ({3fd2543c-3fd2543c})
#### Permissions to dashboards

Permissions to dashboards for regular users and users of 'Admin' type
are limited in the following way:

-   They can see and clone a dashboard if they have at least READ rights
    to it;
-   They can edit and delete dashboard only if they have READ/WRITE
    rights to it;
-   They cannot change the dashboard owner.

[comment]: # ({/3fd2543c-3fd2543c})
