[comment]: # ({1762e554-4d2e7993})
# 3 Users

[comment]: # ({/1762e554-4d2e7993})

[comment]: # ({aa8925de-a4ab546e})
#### Overview

In the *Users → Users* section users of the system are
maintained.

[comment]: # ({/aa8925de-a4ab546e})

[comment]: # ({74f50825-78d54ef9})
#### Users

A listing of existing users with their details is displayed.

![](../../../../../assets/en/manual/web_interface/users.png){width="600"}

Displayed data:

|Column|Description|
|--|--------|
|*Username*|Username for logging into Zabbix. Clicking on the username opens the user [configuration form](/manual/config/users_and_usergroups/user).|
|*Name*|First name of the user.|
|*Last name*|Second name of the user.|
|*User role*|[User role](/manual/web_interface/frontend_sections/users/user_roles) is displayed.|
|*Groups*|Groups that the user is a member of are listed. Clicking on the user group name opens the user group configuration form. Disabled groups are displayed in red.|
|*Is online?*|The on-line status of the user is displayed - *Yes* or *No*. The time of last user activity is displayed in parentheses.|
|*Login*|The login status of the user is displayed - *Ok* or *Blocked*. A user can become temporarily blocked upon exceeding the number of unsuccessful login attempts set in the *[Administration → General → Other](/manual/web_interface/frontend_sections/administration/general#other_parameters)* section (five by default). By clicking on *Blocked* you can unblock the user.|
|*Frontend access*|Frontend access level is displayed - *System default*, *Internal* or *Disabled*, depending on the one set for the whole user group.|
|*API access*|API access status is displayed - *Enabled* or *Disabled*, depending on the one set for the user role.|
|*Debug mode*|Debug mode status is displayed - *Enabled* or *Disabled*, depending on the one set for the whole user group.|
|*Status*|User status is displayed - *Enabled* or *Disabled*, depending on the one set for the whole user group.|
|*Provisioned*|The date when the user was last provisioned is displayed.<br>Used for users created by JIT provisioning from LDAP/SAML.|
|*Info*|Information about errors is displayed.<br>A yellow warning is displayed for users without user groups.<br>A red warning is displayed for users without roles, and for users without roles and user groups.|

To configure a new user, click on the *Create user* button in the top
right-hand corner.

[comment]: # ({/74f50825-78d54ef9})

[comment]: # ({51a09ff6-4439c153})
##### Mass editing options

Buttons below the list offer some mass-editing options:

-   *Provision now* - update user information from LDAP (this option is only enabled if an LDAP user is selected)
-   *Unblock* - re-enable system access to blocked users
-   *Delete* - delete the users

To use these options, mark the check-boxes before the respective users,
then click on the required button.

[comment]: # ({/51a09ff6-4439c153})

[comment]: # ({e38d4c53-84060895})
##### Using filter

You can use the filter to display only the users you are interested in.
For better search performance, data is searched with macros unresolved.

The *Filter* link is available above the list of users. If you click on
it, a filter becomes available where you can filter users by username,
name, last name, user role and user group.

![](../../../../../assets/en/manual/web_interface/user_filter.png){width="600"}

[comment]: # ({/e38d4c53-84060895})
