[comment]: # ({36ca9900-0948f764})
# 1 User groups

[comment]: # ({/36ca9900-0948f764})

[comment]: # ({cedd4afb-670f12ed})
#### Overview

In the *Users → User groups* section user groups of the system
are maintained.

[comment]: # ({/cedd4afb-670f12ed})

[comment]: # ({f400cb93-563be9f1})
#### User groups

A listing of existing user groups with their details is displayed.

![](../../../../../assets/en/manual/web_interface/user_groups0.png){width="600"}

Displayed data:

|Column|Description|
|--|--------|
|*Name*|Name of the user group. Clicking on the user group name opens the user group [configuration form](/manual/config/users_and_usergroups/usergroup#configuration).|
|*\#*|The number of users in the group. Clicking on *Users* will display the respective users filtered out in the user list.|
|*Members*|Usernames of individual users in the user group (with name and surname in parentheses). Clicking on the username will open the user configuration form. Users from disabled groups are displayed in red.|
|*Frontend access*|Frontend access level is displayed:<br>**System default** - Zabbix, LDAP or HTTP authentication; depending on the chosen authentication [method](authentication)<br>**Internal** - the user is authenticated by Zabbix regardless of system settings<br>**Disabled** - frontend access for this user is disabled.<br>By clicking on the current level you can change it.|
|*Debug mode*|[Debug mode](/manual/web_interface/debug_mode) status is displayed - *Enabled* or *Disabled*. By clicking on the status you can change it.|
|*Status*|User group status is displayed - *Enabled* or *Disabled*. By clicking on the status you can change it.|

To configure a new user group, click on the *Create user group* button
in the top right-hand corner.

[comment]: # ({/f400cb93-563be9f1})

[comment]: # ({5cf9e877-5cf9e877})
##### Mass editing options

Buttons below the list offer some mass-editing options:

-   *Enable* - change the user group status to *Enabled*
-   *Disable* - change the user group status to *Disabled*
-   *Enable debug mode* - enable debug mode for the user groups
-   *Disable debug mode* - disable debug mode for the user groups
-   *Delete* - delete the user groups

To use these options, mark the checkboxes before the respective user
groups, then click on the required button.

[comment]: # ({/5cf9e877-5cf9e877})

[comment]: # ({17694202-17694202})
##### Using filter

You can use the filter to display only the user groups you are
interested in. For better search performance, data is searched with
macros unresolved.

The *Filter* link is available above the list of user groups. If you
click on it, a filter becomes available where you can filter user groups
by name and status.

![](../../../../../assets/en/manual/web_interface/user_groups_filter1.png){width="600"}

[comment]: # ({/17694202-17694202})
