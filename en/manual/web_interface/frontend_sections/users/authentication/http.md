[comment]: # ({a429c249-1d523b6c})
# 1 HTTP

[comment]: # ({/a429c249-1d523b6c})

[comment]: # ({17732322-e5bdba8c})
#### Overview

HTTP or web server-based [authentication](/manual/web_interface/frontend_sections/users/authentication) 
(for example: BasicAuthentication, NTLM/Kerberos) can be used to check user names and
passwords. Note that a user must exist in Zabbix as well, however its
Zabbix password will not be used.

::: noteimportant
Be careful! Make sure that web server
authentication is configured and works properly before switching it
on.
:::

[comment]: # ({/17732322-e5bdba8c})

[comment]: # ({8e819456-7677c7bc})

#### Configuration

![](../../../../../../assets/en/manual/web_interface/frontend_sections/administration/auth_http.png){width="600"}

Configuration parameters:

|Parameter|Description|
|--|--------|
|*Enable HTTP authentication*|Mark the checkbox to enable HTTP authentication. Hovering the mouse over ![](../../../../../../assets/en/manual/web_interface/frontend_sections/administration/auth_http_2.png) will bring up a hint box warning that in the case of web server authentication, all users (even with [frontend access](/manual/config/users_and_usergroups/usergroup#configuration) set to LDAP/Internal) will be authenticated by the web server, not by Zabbix.|
|*Default login form*|Specify whether to direct non-authenticated users to:<br>**Zabbix login form** - standard Zabbix login page.<br>**HTTP login form** - HTTP login page.<br>It is recommended to enable web-server based authentication for the `index_http.php` page only. If *Default login form* is set to 'HTTP login page' the user will be logged in automatically if web server authentication module will set valid user login in the `$_SERVER` variable.<br>Supported `$_SERVER` keys are `PHP_AUTH_USER`, `REMOTE_USER`, `AUTH_USER`.|
|*Remove domain name*|A comma-delimited list of domain names that should be removed from the username.<br>E.g. `comp,any` - if username is 'Admin\@any', 'comp\\Admin', user will be logged in as 'Admin'; if username is 'notacompany\\Admin', login will be denied.|
|*Case sensitive login*|Unmark the checkbox to disable case-sensitive login (enabled by default) for usernames.<br>E.g. disable case-sensitive login and log in with, for example, 'ADMIN' user even if the Zabbix user is 'Admin'.<br>*Note* that with case-sensitive login disabled the login will be denied if multiple users exist in Zabbix database with similar usernames (e.g. Admin, admin).|

::: notetip
For internal users who are unable to log in using HTTP
credentials (with HTTP login form set as default) leading to the 401
error, you may want to add a `ErrorDocument 401 /index.php?form=default`
line to basic authentication directives, which will redirect to the
regular Zabbix login form.
:::

[comment]: # ({/8e819456-7677c7bc})
