[comment]: # ({840f7138-70beca7a})
# 3 SAML

[comment]: # ({/840f7138-70beca7a})

[comment]: # ({e25245da-455714b6})
#### Overview

SAML 2.0 [authentication](/manual/web_interface/frontend_sections/users/authentication) 
can be used to sign in to Zabbix. 

If only SAML sign-in is configured, then the user must also exist in Zabbix, however, 
its Zabbix password will not be used. If authentication is successful, then Zabbix will match 
a local username with the username attribute returned by SAML.

[comment]: # ({/e25245da-455714b6})

[comment]: # ({da0941ed-85c848c9})

#### User provisioning

It is possible to configure JIT (just-in-time) **user provisioning** for SAML users. In this case, 
it is not required that a user already exists in Zabbix. The user account can be created when 
the user logs into Zabbix for the first time.

:::noteimportant
If JIT provisioning is enabled, a user group for deprovisioned users must be specified in the *Authentication* tab.
:::

On top of JIT provisioning it is also possible to enable and configure SCIM (System for Cross-domain Identity Management) provisioning - *continuous* user account management
for those users that have been created by user provisioning. SCIM provisioning requires a Zabbix [API token](/manual/web_interface/frontend_sections/users/api_tokens) (with Super admin permissions)
for authentication into Zabbix.

For example, if a user is moved from one SAML group to another, the user will also be moved from one group to another in Zabbix;
if a user is removed from a SAML group, the user will also be removed from the group in Zabbix and, if not belonging to any other group, added to the user group for deprovisioned users.

If SCIM is enabled and configured, a SAML user will be provisioned at the moment the user logs into Zabbix and continuously updated based on changes in SAML.
Already existing SAML users will not be provisioned, and only provisioned users will be updated.
Note that only valid media will be added to a user when the user is provisioned or updated.

If SCIM is not enabled, a SAML user will be provisioned (and later updated) at the moment the user logs into Zabbix.

::: noteclassic
If SAML authentication is enabled, users will be able to choose between logging in locally or via SAML single sign-on. 
If JIT provisioning is used, then only single sign-on is possible.
:::

[comment]: # ({/da0941ed-85c848c9})

[comment]: # ({7be842a1-4cca45cd})
#### Setting up identity provider

In order to work with Zabbix, a SAML identity provider ([onelogin.com](https://onelogin.com), [auth0.com](https://auth0.com),
[okta.com](https://okta.com), etc.) needs to be configured in the following way:

-   *Assertion Consumer URL* should be set to
    `<path_to_zabbix_ui>/index_sso.php?acs`
-   *Single Logout URL* should be set to
    `<path_to_zabbix_ui>/index_sso.php?sls`

`<path_to_zabbix_ui>` examples: `https://example.com/zabbix/ui`,
`http://another.example.com/zabbix`, `http://<any_public_ip_address>/zabbix`

[comment]: # ({/7be842a1-4cca45cd})

[comment]: # ({20acfae1-274513e2})
#### Setting up Zabbix

::: noteimportant
It is required to install php-openssl if you want
to use SAML authentication in the frontend.
:::

To use SAML authentication Zabbix should be configured in the following
way:

1\. Private key and certificate should be stored in the
*ui/conf/certs*/, unless custom paths are provided in
[zabbix.conf.php](authentication#advanced_settings).

By default, Zabbix will look in the following locations:

-   ui/conf/certs/sp.key - SP private key file
-   ui/conf/certs/sp.crt - SP cert file
-   ui/conf/certs/idp.crt - IDP cert file

2\. All of the most important settings can be configured in the Zabbix
frontend. However, it is possible to specify additional settings in the
[configuration file](authentication#advanced_settings).

![](../../../../../../assets/en/manual/web_interface/frontend_sections/users/auth_saml.png)

Configuration parameters, available in the Zabbix frontend:

|Parameter|Description|
|--|--------|
|*Enable SAML authentication*|Mark the checkbox to enable SAML authentication.|
|*Enable JIT provisioning*|Mark the checkbox to enable JIT user provisioning.|
|*IDP entity ID*|The unique entity identifier within the SAML identity provider.|
|*SSO service URL*|The URL users will be redirected to when logging in.|
|*SLO service URL*|The URL users will be redirected to when logging out. If left empty, the SLO service will not be used.|
|*Username attribute*|SAML attribute to be used as a username when logging into Zabbix.<br>The list of supported values is determined by the identity provider.<br><br>Examples:<br>uid<br>u,serprincipalname<br>samaccountname<br>username<br>userusername<br><urn:oid:0.9.2342.19200300.100.1.1><br><urn:oid:1.3.6.1.4.1.5923.1.1.1.13><br><urn:oid:0.9.2342.19200300.100.1.44>|
|*SP entity ID*|The unique service provider identifier (if not matching, the operation will be rejected).<br>It is possible to specify a URL or any string of data.|
|*SP name ID format*|Defines which name identifier format should be used.<br><br>Examples:<br><urn:oasis:names:tc:SAML:2.0:nameid-format:persistent><br><urn:oasis:names:tc:SAML:2.0:nameid-format:transient><br><urn:oasis:names:tc:SAML:2.0:nameid-format:kerberos><br><urn:oasis:names:tc:SAML:2.0:nameid-format:entity>|
|*Sign*|Mark the checkboxes to select entities for which SAML signature should be enabled:<br>*Messages*<br>*Assertions*<br>*AuthN requests*<br>*Logout requests*<br>*Logout responses*|
|*Encrypt*|Mark the checkboxes to select entities for which SAML encryption should be enabled:<br>*Name ID*<br>*Assertions*|
|*Case-sensitive login*|Mark the checkbox to enable case-sensitive login (disabled by default) for usernames.<br>E.g. disable case-sensitive login and log in with, for example, 'ADMIN' user even if the Zabbix user is 'Admin'.<br>*Note* that with case-sensitive login disabled the login will be denied if multiple users exist in Zabbix database with similar usernames (e.g. Admin, admin).|
|*Configure JIT provisioning*|Mark this checkbox to show options related to JIT user provisioning.|
|*Group name attribute*|Specify the group name attribute for JIT user provisioning.|
|*User name attribute*|Specify the user name attribute for JIT user provisioning.|
|*User last name attribute*|Specify the user last name attribute for JIT user provisioning.|
|*User group mapping*|Map a SAML user group pattern to Zabbix user group and user role.<br>This is required to determine what user group/role the provisioned user will get in Zabbix.<br>Click on *Add* to add a mapping.<br>The *SAML group pattern* field supports wildcards. The group name must match an existing group.<br>If a SAML user matches several Zabbix user groups, the user becomes a member of all of them.<br>If a user matches several Zabbix user roles, the user will get the highest permission level among them.|
|*Media type mapping*|Map the user's SAML media attributes (e.g. email) to Zabbix user media for sending notifications.|
|*Enable SCIM provisioning*|Mark this checkbox to enable SCIM 2.0 provisioning.|

See examples of configuring SAML identity providers for sign-in and user provisioning into Zabbix with:

-   [Microsoft Azure AD](/manual/appendix/install/azure_ad)
-   [Okta](/manual/appendix/install/okta)
-   [Onelogin](/manual/appendix/install/onelogin)

[comment]: # ({/20acfae1-274513e2})

[comment]: # ({e49fd5e5-d739be65})

#### Notes on SCIM provisioning

For SCIM provisioning specify the path to the Zabbix frontend and append api_scim.php to it, on the identity provider side, i.e.:

    https://<your-zabbix-url>/zabbix/api_scim.php

User attributes that are used in Zabbix (username, user name, user lastname and media attributes) need to be added as custom attributes and, if necessary, external namespace should be the same as user schema: `urn:ietf:params:scim:schemas:core:2.0:User`.

[comment]: # ({/e49fd5e5-d739be65})

[comment]: # ({aff062a8-2c636ca1})

##### Advanced settings

Additional SAML parameters can be configured in the Zabbix frontend
configuration file (*zabbix.conf.php*):

-   $SSO\['SP\_KEY'\] = '<path to the SP private key file>';
-   $SSO\['SP\_CERT'\] = '<path to the SP cert file>';
-   $SSO\['IDP\_CERT'\] = '<path to the IDP cert file>';
-   $SSO\['SETTINGS'\]

::: noteclassic
 Zabbix uses [OneLogin's SAML PHP
Toolkit](https://github.com/onelogin/php-saml/tree/3.4.1) library
(version 3.4.1). The structure of $SSO\['SETTINGS'\] section should be
similar to the structure used by the library. For the description of
configuration options, see official library
[documentation](https://github.com/onelogin/php-saml/tree/3.4.1/#user-content-settings).

:::

Only the following options can be set as part of $SSO\['SETTINGS'\]:

-   *strict*
-   *baseurl*
-   *compress*
-   *contactPerson*
-   *organization*
-   *sp* (only options specified in this list)
    -   *attributeConsumingService*
    -   *x509certNew*
-   *idp* (only options specified in this list)
    -   *singleLogoutService* (only one option)
        -   *responseUrl*
    -   *certFingerprint*
    -   *certFingerprintAlgorithm*
    -   *x509certMulti*
-   *security* (only options specified in this list)
    -   *signMetadata*
    -   *wantNameId*
    -   *requestedAuthnContext*
    -   *requestedAuthnContextComparison*
    -   *wantXMLValidation*
    -   *relaxDestinationValidation*
    -   *destinationStrictlyMatches*
    -   *rejectUnsolicitedResponsesWithInResponseTo*
    -   *signatureAlgorithm*
    -   *digestAlgorithm*
    -   *lowercaseUrlencoding*

All other options will be taken from the database and cannot be
overridden. The *debug* option will be ignored.

In addition, if Zabbix UI is behind a proxy or a load balancer, the
custom *use\_proxy\_headers* option can be used:

-   *false* (default) - ignore the option;
-   *true* - use X-Forwarded-\* HTTP headers for building the base URL.
  
If using a load balancer to connect to Zabbix instance, where the load balancer uses TLS/SSL and Zabbix does not, you must indicate 'baseurl', 'strict' and 'use_proxy_headers' parameters as follows:

    $SSO_SETTINGS=['strict' => false, 'baseurl' => "https://zabbix.example.com/zabbix/", 'use_proxy_headers' => true]


**Configuration example:**

    $SSO['SETTINGS'] = [
        'security' => [
            'signatureAlgorithm' => 'http://www.w3.org/2001/04/xmldsig-more#rsa-sha384'
            'digestAlgorithm' => 'http://www.w3.org/2001/04/xmldsig-more#sha384',
            // ...
        ],
        // ...
    ];

[comment]: # ({/aff062a8-2c636ca1})
