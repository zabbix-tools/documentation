[comment]: # ({c87233da-318de170})
# 8 Users

[comment]: # ({/c87233da-318de170})

[comment]: # ({c09b7115-7d66c187})
#### Overview

This menu features sections that are related to configuring 
users in Zabbix. This menu is available to [SuperAdmin](/manual/config/users_and_usergroups/permissions) user type users only.

[comment]: # ({/c09b7115-7d66c187})
