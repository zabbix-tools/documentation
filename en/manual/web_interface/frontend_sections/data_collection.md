[comment]: # ({d96f35f6-c6632d7f})
# 6 Data collection

[comment]: # ({/d96f35f6-c6632d7f})

[comment]: # ({36e86eda-917a67b4})
#### Overview

This menu features sections that are related to configuring 
data collection.

[comment]: # ({/36e86eda-917a67b4})
