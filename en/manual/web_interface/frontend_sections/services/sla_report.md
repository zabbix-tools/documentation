[comment]: # ({be8f3d19-315566e5})
# 3 SLA report

[comment]: # ({/be8f3d19-315566e5})

[comment]: # ({2a881d20-3b69a682})
### Overview

This section allows to view SLA reports, based on the criteria selected 
in the filter.

SLA reports can also be displayed as a [dashboard widget](/manual/web_interface/frontend_sections/dashboards/widgets/sla_report).

[comment]: # ({/2a881d20-3b69a682})

[comment]: # ({18f6e4c9-18f6e4c9})

### Report

The filter allows to select the report based on the SLA name as well as the 
service name. It is also possible to limit the displayed period.

![](../../../../../assets/en/manual/web_interface/sla_report.png){width="600"}

Each column (period) displays the SLI for that period. SLIs that are in breach of the set 
SLO are highlighted in red.

20 periods are displayed in the report. A maximum of 100 periods can be displayed, if 
both the *From* date and *To* date are specified. 

[comment]: # ({/18f6e4c9-18f6e4c9})

[comment]: # ({03eed45c-03eed45c})

### Report details

If you click on the service name in the report, you can access another report 
that displays a more detailed view.

![](../../../../../assets/en/manual/web_interface/sla_report_details.png){width="600"}

[comment]: # ({/03eed45c-03eed45c})
