[comment]: # (tags: sla, slo)

[comment]: # ({8cd9b935-fa269a08})
# 2 SLA

[comment]: # ({/8cd9b935-fa269a08})

[comment]: # ({dcb1c2a8-1d46f9b5})
### Overview

This section allows to view and [configure](/manual/it_services/sla) SLAs.

[comment]: # ({/dcb1c2a8-1d46f9b5})

[comment]: # ({55388fdb-424c7051})
### SLAs

![](../../../../../assets/en/manual/web_interface/sla_list.png){width="600"}

A list of the configured SLAs is displayed. *Note* that only the SLAs 
related to services accessible to the user will be displayed (as read-only, 
unless *Manage SLA* is enabled for the user role).


Displayed data:

|Parameter|Description|
|--|--------|
|*Name*|The SLA name is displayed.<br>The name is a link to [SLA configuration](/manual/it_services/sla#configuration). |
|*SLO*|The service level objective (SLO) is displayed. |
|*Effective date*|The date of starting SLA calculation is displayed. |
|*Reporting period*|The period used in the SLA report is displayed - *daily*, *weekly*, *monthly*, *quarterly*, or *annually*. |
|*Time zone*|The SLA time zone is displayed. |
|*Schedule*|The SLA schedule is displayed - 24x7 or custom. |
|*SLA report*|Click on the link to see the SLA report for this SLA. |
|*Status*|The SLA status is displayed - enabled or disabled. |

[comment]: # ({/55388fdb-424c7051})

