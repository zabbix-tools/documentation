[comment]: # ({fa1a4804-33bc9cef})
# 5 Macros

[comment]: # ({/fa1a4804-33bc9cef})

[comment]: # ({c19b9fef-b195d98e})
#### Overview

This section allows to define system-wide [user
macros](/manual/config/macros/user_macros) as name-value pairs. Note
that macro values can be kept as plain text, secret text or Vault
secret. Adding a description is also supported.

![](../../../../../assets/en/manual/web_interface/user_macros_global.png){width="600"}

[comment]: # ({/c19b9fef-b195d98e})
