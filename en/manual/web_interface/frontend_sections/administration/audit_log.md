[comment]: # ({e52bee27-66bd8c75})
# 2 Audit log

[comment]: # ({/e52bee27-66bd8c75})

[comment]: # ({10ff516d-b503e694})
#### Overview

This section allows configuring audit log settings.

![](../../../../../assets/en/manual/web_interface/frontend_sections/administration/audit_log.png)

The following parameters are available:

|Parameter|Description|
|--|--------|
|Enable audit logging|Enable/disable audit logging. Marked by default.|
|Enable internal housekeeping|Enable/disable internal housekeeping for audit. Marked by default.|
|Data storage period|Amount of days audit records should be kept for before being removed by the housekeeper. Mandatory if housekeeping is enabled. Default: 365 days.|

[comment]: # ({/10ff516d-b503e694})
