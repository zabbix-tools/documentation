[comment]: # ({f1a9b92b-b226bc20})
# 6 Queue

[comment]: # ({/f1a9b92b-b226bc20})

[comment]: # ({d8ba5686-f936bb89})
#### Overview

In the *Administration → Queue* section items that are waiting to be
updated are displayed.

Ideally, when you open this section it should all be "green" meaning no
items in the queue. If all items are updated without delay, there are
none waiting. However, due to lacking server performance, connection
problems or problems with agents, some items may get delayed and the
information is displayed in this section. For more details, see the
[Queue](/manual/config/items/queue) section.

::: noteclassic
Queue is available only if Zabbix server is
running.
:::

The *Administration → Queue* section contains the following pages:

-   Queue overview — displays queue by item type;
-   Queue overview by proxy — displays queue by proxy;
-   Queue details — displays a list of delayed items.

The list of available pages appears upon pressing on *Queue* in the
*Administration* menu section. It is also possible to switch between
pages by using a title dropdown in the top left corner.

|   |   |
|---|---|
|![](../../../../../assets/en/manual/web_interface/frontend_sections/administration/queue_menu.png){width="300"}|![](../../../../../assets/en/manual/web_interface/frontend_sections/administration/queue_selector.png)|
|Third-level menu.|Title dropdown.|

[comment]: # ({/d8ba5686-f936bb89})

[comment]: # ({ccd11c9f-ccd11c9f})
##### Overview by item type

In this screen it is easy to locate if the problem is related to one or
several item types.

![](../../../../../assets/en/manual/web_interface/queue.png){width="600"}

Each line contains an item type. Each column shows the number of waiting
items - waiting for 5-10 seconds/10-30 seconds/30-60 seconds/1-5
minutes/5-10 minutes or over 10 minutes respectively.

[comment]: # ({/ccd11c9f-ccd11c9f})

[comment]: # ({d13acc32-d13acc32})
##### Overview by proxy

In this screen it is easy to locate if the problem is related to one of
the proxies or the server.

![](../../../../../assets/en/manual/web_interface/queue_proxy.png){width="600"}

Each line contains a proxy, with the server last in the list. Each
column shows the number of waiting items - waiting for 5-10
seconds/10-30 seconds/30-60 seconds/1-5 minutes/5-10 minutes or over 10
minutes respectively.

[comment]: # ({/d13acc32-d13acc32})

[comment]: # ({eeb425e4-dcd90684})
##### List of waiting items

In this screen, each waiting item is listed.

![](../../../../../assets/en/manual/web_interface/queue_details.png){width="600"}

Displayed data:

|Column|Description|
|--|--------|
|*Scheduled check*|The time when the check was due is displayed.|
|*Delayed by*|The length of the delay is displayed.|
|*Host*|Host of the item is displayed.|
|*Name*|Name of the waiting item is displayed.|
|*Proxy*|The proxy name is displayed, if the host is monitored by proxy.|

[comment]: # ({/eeb425e4-dcd90684})

[comment]: # ({43937018-1eab4d89})
##### Possible error messages

You may encounter a situation when no data is displayed and the
following error message appears:

![](../../../../../assets/en/manual/web_interface/error_message_1.png){width="600"}

Error message in this case is the following:

    Cannot display item queue. Permission denied

This happens when the PHP configuration parameters in the *zabbix.conf.php* file - `$ZBX_SERVER` or both `$ZBX_SERVER` and `$ZBX_SERVER_PORT` - point to an existing Zabbix server that uses a different database.

[comment]: # ({/43937018-1eab4d89})
