[comment]: # ({ce25d2b5-ce25d2b5})
# 6 Event correlation

[comment]: # ({/ce25d2b5-ce25d2b5})

[comment]: # ({8d92e187-7fb3700b})
#### Overview

In the *Data collection → Event correlation* section users can configure
and maintain global correlation rules for Zabbix events.

![](../../../../../assets/en/manual/web_interface/correlation_rules.png){width="600"}

Displayed data:

|Column|Description|
|--|--------|
|*Name*|Name of the correlation rule. Clicking on the correlation rule name opens the rule [configuration form](/manual/config/event_correlation/global#configuration).|
|*Conditions*|Correlation rule conditions are displayed.|
|*Operations*|Correlation rule operations are displayed.|
|*Status*|Correlation rule status is displayed - *Enabled* or *Disabled*.<br>By clicking on the status you can change it.|

To configure a new correlation rule, click on the *Create event correlation*
button in the top right-hand corner.

[comment]: # ({/8d92e187-7fb3700b})

[comment]: # ({8983c64f-8983c64f})
##### Mass editing options

Buttons below the list offer some mass-editing options:

-   *Enable* - change the correlation rule status to *Enabled*
-   *Disable* - change the correlation rule status to *Disabled*
-   *Delete* - delete the correlation rules

To use these options, mark the checkboxes before the respective
correlation rules, then click on the required button.

[comment]: # ({/8983c64f-8983c64f})

[comment]: # ({52850c61-52850c61})
##### Using filter

You can use the filter to display only the correlation rules you are
interested in. For better search performance, data is searched with
macros unresolved.

The *Filter* link is available above the list of correlation rules. If
you click on it, a filter becomes available where you can filter
correlation rules by name and status.

![](../../../../../assets/en/manual/web_interface/correlation_rules_filter1.png){width="600"}

[comment]: # ({/52850c61-52850c61})
