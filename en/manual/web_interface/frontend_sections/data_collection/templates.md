[comment]: # ({adaceae4-e8fbaf7c})
# 3 Templates

[comment]: # ({/adaceae4-e8fbaf7c})

[comment]: # ({6a6803ed-2fff982e})
#### Overview

In the *Data collection* → *Templates* section users can configure and
maintain templates.

A listing of existing templates with their details is displayed.

![](../../../../../assets/en/manual/web_interface/templates.png){width="600"}

Displayed data:

|Column|Description|
|--|--------|
|*Name*|Name of the template.<br>Clicking on the template name opens the template [configuration form](/manual/config/templates/template#creating_a_template).|
|*Hosts*|Number of editable hosts to which the template is linked; read-only hosts are not included.<br>Clicking on *Hosts* will open the host list with only those hosts filtered that are linked to the template.|
|*Entities (Items, Triggers, Graphs, Dashboards, Discovery, Web)*|Number of the respective entities in the template (displayed in gray).<br>Clicking on the entity name will, in the whole listing of that entity, filter out those that belong to the template.|
|*Linked templates*|Templates that are [linked](/manual/config/templates/nesting) to the template.|
|*Linked to templates*|Templates that the template is [linked](/manual/config/templates/nesting) to.<br>Since Zabbix 5.0.3, this column no longer includes hosts.|
|*Vendor*, *Version*|Template vendor and version; displayed if the template configuration contains such information, and only for [out-of-the-box templates](/manual/config/templates_out_of_the_box), [imported templates](/manual/xml_export_import/templates#importing), or templates modified through the [Template API](/manual/api/reference/template).<br>For out-of-the-box templates, version is displayed as follows: major version of Zabbix, delimiter ("-"), revision number (increased with each new version of the template, and reset with each major version of Zabbix). For example, 6.4-0, 6.4-3, 7.0-0, 7.0-3.|
|*Tags*|[Tags](/manual/config/tagging) of the template, with macros unresolved.|

To [configure a new template](/manual/config/templates/template), click on the *Create template* button in the top right-hand corner.

To [import a template](/manual/xml_export_import/templates#importing) from a YAML, XML, or JSON file, click on the *Import* button in the top right-hand corner.

[comment]: # ({/6a6803ed-2fff982e})

[comment]: # ({b55c7499-f09409e4})
##### Using filter

You can use the filter to display only the templates you are interested
in. For better search performance, data is searched with macros
unresolved.

The *Filter* link is available below *Create template* and *Import*
buttons. If you click on it, a filter becomes available where you can
filter templates by template group, linked templates, name and tags.

![](../../../../../assets/en/manual/web_interface/template_filter.png){width="600"}

|Parameter|Description|
|--|--------|
|*Template groups*|Filter by one or more template groups.<br>Specifying a parent template group implicitly selects all nested groups.|
|*Linked templates*|Filter by directly linked templates.|
|*Name*|Filter by template name.|
|*Vendor*|Filter by template vendor.|
|*Version*|Filter by template version.|
|*Tags*|Filter by template tag name and value.<br>Filtering is possible only by template-level tags (not inherited ones). It is possible to include as well as exclude specific tags and tag values. Several conditions can be set. Tag name matching is always case-sensitive.<br><br>There are several operators available for each condition:<br>**Exists** - include the specified tag names;<br>**Equals** - include the specified tag names and values (case-sensitive);<br>**Contains** - include the specified tag names where the tag values contain the entered string (substring match, case-insensitive);<br>**Does not exist** - exclude the specified tag names;<br>**Does not equal** - exclude the specified tag names and values (case-sensitive);<br>**Does not contain** - exclude the specified tag names where the tag values contain the entered string (substring match, case-insensitive).<br><br>There are two calculation types for conditions:<br>**And/Or** - all conditions must be met, conditions having the same tag name will be grouped by the Or condition;<br>**Or** - enough if one condition is met.|

[comment]: # ({/b55c7499-f09409e4})

[comment]: # ({dbb94a8e-1ff75389})
##### Mass editing options

Buttons below the list offer some mass-editing options:

-   *Export* - export the template to a YAML, XML or JSON file;
-   *Mass update* - [update several properties](/manual/config/templates/mass) for a number of templates at once;
-   *Delete* - delete the template while leaving its linked entities (items, triggers etc.) with the hosts;
-   *Delete and clear* - delete the template and its linked entities from the hosts.

To use these options, mark the checkboxes before the respective
templates, then click on the required button.

[comment]: # ({/dbb94a8e-1ff75389})
