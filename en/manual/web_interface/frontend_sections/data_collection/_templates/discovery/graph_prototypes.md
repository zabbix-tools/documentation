[comment]: # ({e690b77d-e690b77d})
# 3 Graph prototypes

[comment]: # ({/e690b77d-e690b77d})

[comment]: # ({0254a602-a4f653e7})
#### Overview

In this section the configured graph prototypes of a low-level discovery rule on the template are 
displayed. 

If the template is linked to the host, graph prototypes will become the basis of 
creating real host [graphs](/manual/web_interface/frontend_sections/data_collection/hosts/graphs) 
during low-level discovery.

![](../../../../../../../assets/en/manual/web_interface/template_graph_prototypes.png){width="600"}

Displayed data:

|Column|Description|
|--|--------|
|*Name*|Name of the graph prototype, displayed as a blue link.<br>Clicking on the name opens the graph prototype [configuration form](/manual/discovery/low_level_discovery/graph_prototypes).<br>If the graph prototype belongs to a linked template, the template name is displayed before the graph name, as a gray link. Clicking on the template link will open the graph prototype list on the linked template level.|
|*Width*|Width of the graph prototype is displayed.|
|*Height*|Height of the graph prototype is displayed.|
|*Type*|Type of the graph prototype is displayed - *Normal*, *Stacked*, *Pie* or *Exploded*.|
|*Discover*|Discover the graph based on this prototype:<br>**Yes** - discover<br>**No** - do not discover. You can switch between 'Yes' and 'No' by clicking on them.|

To configure a new graph prototype, click on the *Create
graph prototype* button at the top right corner.

[comment]: # ({/0254a602-a4f653e7})

[comment]: # ({03646b6f-03646b6f})
##### Mass editing options

Buttons below the list offer some mass-editing options:

-   *Delete* - delete these graph prototypes

To use these options, mark the checkboxes before the respective
graph prototypes, then click on the required button.

[comment]: # ({/03646b6f-03646b6f})
