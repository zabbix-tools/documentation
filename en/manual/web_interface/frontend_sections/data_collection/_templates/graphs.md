[comment]: # ({df60ffd9-df60ffd9})
# 3 Graphs

[comment]: # ({/df60ffd9-df60ffd9})

[comment]: # ({dca7fdf9-751b721f})
#### Overview

The custom graph list for a template can be accessed from *Data collection
→ Templates* by clicking on *Graphs* for the respective template.

A list of existing graphs is displayed.

![](../../../../../../assets/en/manual/web_interface/template_graphs.png){width="600"}

Displayed data:

|Column|Description|
|--|--------|
|*Template*|Template the graph belongs to.<br>This column is displayed only if multiple templates are selected in the filter.|
|*Name*|Name of the custom graph, displayed as a blue link to graph details.<br>Clicking on the graph name link opens the graph [configuration form](/manual/config/visualization/graphs/custom#configuring_custom_graphs).<br>If the graph is inherited from another template, the template name is displayed before the graph name, as a gray link. Clicking on the template link will open the graph list on that template level.|
|*Width*|Graph width is displayed.|
|*Height*|Graph height is displayed.|
|*Graph type*|Graph type is displayed - *Normal*, *Stacked*, *Pie* or *Exploded*.|

To configure a new graph, click on the *Create graph* button at the top
right corner.

[comment]: # ({/dca7fdf9-751b721f})

[comment]: # ({5ee2ddb1-5ee2ddb1})
##### Mass editing options

Buttons below the list offer some mass-editing options:

-   *Copy* - copy the graphs to other hosts or templates
-   *Delete* - delete the graphs

To use these options, mark the checkboxes before the respective graphs,
then click on the required button.

[comment]: # ({/5ee2ddb1-5ee2ddb1})

[comment]: # ({d78ea058-7822c6df})
##### Using filter

You can filter graphs by template group and template. For better search
performance, data is searched with macros unresolved.

[comment]: # ({/d78ea058-7822c6df})
