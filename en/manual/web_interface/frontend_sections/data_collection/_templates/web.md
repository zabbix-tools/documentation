[comment]: # ({0decccc5-0decccc5})
# 5 Web scenarios

[comment]: # ({/0decccc5-0decccc5})

[comment]: # ({6b953bee-ebe4d9df})
#### Overview

The [web scenario](/manual/web_monitoring) list for a template can be accessed from *Data collection
→ Templates* by clicking on *Web* for the respective template.

A list of existing web scenarios is displayed.

![](../../../../../../assets/en/manual/web_interface/template_web_scenarios.png){width="600"}

Displayed data:

|Column|Description|
|--|--------|
|*Name*|Name of the web scenario. Clicking on the web scenario name opens the web scenario [configuration form](/manual/web_monitoring#configuring-a-web-scenario).<br>If the web scenario is inherited from another template, the template name is displayed before the web scenario name, as a gray link. Clicking on the template link will open the web scenarios list on that template level.|
|*Number of steps*|The number of steps the scenario contains.|
|*Update interval*|How often the scenario is performed.|
|*Attempts*|How many attempts for executing web scenario steps are performed.|
|*Authentication*|Authentication method is displayed - Basic, NTLM on None.|
|*HTTP proxy*|Displays HTTP proxy or 'No' if not used.|
|*Status*|Web scenario status is displayed - *Enabled* or *Disabled*.<br>By clicking on the status you can change it.|
|*Tags*|Web scenario tags are displayed.<br>Up to three tags (name:value pairs) can be displayed. If there are more tags, a "..." link is displayed that allows to see all tags on mouseover.|

To configure a new web scenario, click on the *Create web scenario*
button at the top right corner.

[comment]: # ({/6b953bee-ebe4d9df})

[comment]: # ({915ace12-915ace12})
##### Mass editing options

Buttons below the list offer some mass-editing options:

-   *Enable* - change the scenario status to *Enabled*
-   *Disable* - change the scenario status to *Disabled*
-   *Delete* - delete the web scenarios

To use these options, mark the checkboxes before the respective web
scenarios, then click on the required button.

[comment]: # ({/915ace12-915ace12})

[comment]: # ({b10ebbac-647d252a})
##### Using filter

You can use the filter to display only the scenarios you are interested
in. For better search performance, data is searched with macros
unresolved.

The *Filter* link is available above the list of web scenarios. If you
click on it, a filter becomes available where you can filter scenarios
by template group, template, status and tags.

![](../../../../../../assets/en/manual/web_interface/template_web_scenario_filter.png){width="600"}

[comment]: # ({/b10ebbac-647d252a})
