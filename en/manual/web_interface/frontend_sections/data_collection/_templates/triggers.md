[comment]: # ({41736fb0-41736fb0})
# 2 Triggers

[comment]: # ({/41736fb0-41736fb0})

[comment]: # ({f2be3859-6da73e9a})
#### Overview

The trigger list for a template can be accessed from *Data collection →
Templates* by clicking on *Triggers* for the respective template.

![](../../../../../../assets/en/manual/web_interface/template_triggers.png){width="600"}

Displayed data:

|Column|Description|
|--|--------|
|*Severity*|Severity of the trigger is displayed by both name and cell background color.|
|*Template*|Template the trigger belongs to.<br>This column is displayed only if multiple templates are selected in the filter.|
|*Name*|Name of the trigger displayed as a blue link to trigger details.<br>Clicking on the trigger name link opens the trigger [configuration form](/manual/config/triggers/trigger#configuration).<br>If the trigger is inherited from another template, the template name is displayed before the trigger name, as a gray link. Clicking on the template link will open the trigger list on that template level.|
|*Operational data*|Operational data definition of the trigger, containing arbitrary strings and macros that will resolve dynamically in *Monitoring* → *Problems*.|
|*Expression*|Trigger expression is displayed. The template-item part of the expression is displayed as a link, leading to the item configuration form.|
|*Status*|Trigger status is displayed - *Enabled* or *Disabled*. By clicking on the status you can change it - from Enabled to Disabled (and back).|
|*Tags*|If a trigger contains tags, tag name and value are displayed in this column.|

To configure a new trigger, click on the *Create trigger* button at the
top right corner.

[comment]: # ({/f2be3859-6da73e9a})

[comment]: # ({4829e943-4829e943})
##### Mass editing options

Buttons below the list offer some mass-editing options:

-   *Enable* - change trigger status to *Enabled*
-   *Disable* - change trigger status to *Disabled*
-   *Copy* - copy the triggers to other hosts or templates
-   *Mass update* - update several properties for a number of triggers
    at once
-   *Delete* - delete the triggers

To use these options, mark the checkboxes before the respective
triggers, then click on the required button.

[comment]: # ({/4829e943-4829e943})

[comment]: # ({9657af71-4b90c6e1})
##### Using filter

You can use the filter to display only the triggers you are interested
in. For better search performance, data is searched with macros
unresolved.

The *Filter* icon is available at the top right corner. Clicking on it
will open a filter where you can specify the desired filtering criteria.

![](../../../../../../assets/en/manual/web_interface/template_triggers_filter.png){width="600"}

|Parameter|Description|
|--|--------|
|*Template groups*|Filter by one or more template groups.<br>Specifying a parent template group implicitly selects all nested groups.|
|*Templates*|Filter by one or more templates.<br>If template groups are already selected above, template selection is limited to those groups.|
|*Name*|Filter by trigger name.|
|*Severity*|Select to filter by one or several trigger severities.|
|*Status*|Filter by trigger status.|
|*Tags*|Filter by trigger tag name and value. It is possible to include as well as exclude specific tags and tag values. Several conditions can be set. Tag name matching is always case-sensitive.<br>There are several operators available for each condition:<br>**Exists** - include the specified tag names<br>**Equals** - include the specified tag names and values (case-sensitive)<br>**Contains** - include the specified tag names where the tag values contain the entered string (substring match, case-insensitive)<br>**Does not exist** - exclude the specified tag names<br>**Does not equal** - exclude the specified tag names and values (case-sensitive)<br>**Does not contain** - exclude the specified tag names where the tag values contain the entered string (substring match, case-insensitive)<br>There are two calculation types for conditions:<br>**And/Or** - all conditions must be met, conditions having the same tag name will be grouped by the Or condition<br>**Or** - enough if one condition is met<br>Macros and [macro functions](/manual/config/macros/macro_functions) are supported in tag name and tag value fields.|
|*Inherited*|Filter triggers inherited (or not inherited) from linked templates.|
|*With dependencies*|Filter triggers with (or without) dependencies.|

[comment]: # ({/9657af71-4b90c6e1})
