[comment]: # ({e0d353ca-e0d353ca})
# 4 Host prototypes

[comment]: # ({/e0d353ca-e0d353ca})

[comment]: # ({447a9888-5a2e0083})
#### Overview

In this section the host prototypes of a low-level discovery rule on the host are 
displayed. Host prototypes are the basis of real [hosts](/manual/web_interface/frontend_sections/data_collection/hosts) 
that are created during low-level discovery.

![](../../../../../../../assets/en/manual/web_interface/host_host_prototypes.png){width="600"}

Displayed data:

|Column|Description|
|--|--------|
|*Name*|Name of the host prototype, displayed as a blue link.<br>Clicking on the name opens the host prototype configuration form.<br>If the host prototype belongs to a linked template, the template name is displayed before the host name, as a gray link. Clicking on the template link will open the host prototype list on the linked template level.|
|*Templates*|Templates of the host prototype are displayed.|
|*Create enabled*|Create the host based on this prototype as:<br>**Yes** - enabled<br>**No** - disabled. You can switch between 'Yes' and 'No' by clicking on them.|
|*Discover*|Discover the host based on this prototype:<br>**Yes** - discover<br>**No** - do not discover. You can switch between 'Yes' and 'No' by clicking on them.|
|*Tags*|Tags of the host prototype are displayed.|

To configure a new host prototype, click on the *Create
host prototype* button at the top right corner.

[comment]: # ({/447a9888-5a2e0083})

[comment]: # ({678c98ff-678c98ff})
##### Mass editing options

Buttons below the list offer some mass-editing options:

-   *Create enabled* - create these hosts as *Enabled*
-   *Create disabled* - create these hosts as *Disabled*
-   *Delete* - delete these host prototypes

To use these options, mark the checkboxes before the respective
host prototypes, then click on the required button.

[comment]: # ({/678c98ff-678c98ff})
