[comment]: # ({d2561446-d2561446})
# 1 Item prototypes

[comment]: # ({/d2561446-d2561446})

[comment]: # ({3e327bba-a6782b59})
#### Overview

In this section the item prototypes of a low-level discovery rule on the host are 
displayed. Item prototypes are the basis of real host [items](/manual/web_interface/frontend_sections/data_collection/hosts/items) 
that are created during low-level discovery.

![](../../../../../../../assets/en/manual/web_interface/host_item_prototypes.png){width="600"}

Displayed data:

|Column|Description|
|--|--------|
|*Name*|Name of the item prototype, displayed as a blue link.<br>Clicking on the name opens the item prototype [configuration form](/manual/discovery/low_level_discovery/item_prototypes).<br>If the item prototype belongs to a template, the template name is displayed before the rule name, as a gray link. Clicking on the template link will open the item prototype list on the template level.|
|*Key*|Key of the item prototype is displayed.|
|*Interval*|Frequency of the check is displayed.|
|*History*|How many days to keep item data history is displayed.|
|*Trends*|How many days to keep item trends history is displayed.|
|*Type*|Type of the item prototype is displayed (Zabbix agent, SNMP agent, simple check, etc).|
|*Create enabled*|Create the item based on this prototype as:<br>**Yes** - enabled<br>**No** - disabled. You can switch between 'Yes' and 'No' by clicking on them.|
|*Discover*|Discover the item based on this prototype:<br>**Yes** - discover<br>**No** - do not discover. You can switch between 'Yes' and 'No' by clicking on them.|
|*Tags*|Tags of the item prototype are displayed.|

To configure a new item prototype, click on the *Create
item prototype* button at the top right corner.

[comment]: # ({/3e327bba-a6782b59})

[comment]: # ({065ce16f-065ce16f})
##### Mass editing options

Buttons below the list offer some mass-editing options:

-   *Create enabled* - create these items as *Enabled*
-   *Create disabled* - create these items as *Disabled*
-   *Mass update* - mass update these item prototypes
-   *Delete* - delete these item prototypes

To use these options, mark the checkboxes before the respective
item prototypes, then click on the required button.

[comment]: # ({/065ce16f-065ce16f})
