[comment]: # ({41736fb0-41736fb0})
# 2 Triggers

[comment]: # ({/41736fb0-41736fb0})

[comment]: # ({cf3f7516-de145ee1})
#### Overview

The trigger list for a host can be accessed from *Data collection → Hosts*
by clicking on *Triggers* for the respective host.

![](../../../../../../assets/en/manual/web_interface/triggers.png){width="600"}

Displayed data:

|Column|Description|
|--|--------|
|*Severity*|Severity of the trigger is displayed by both name and cell background color.|
|*Value*|Trigger value is displayed:<br>**OK** - the trigger is in the OK state<br>**PROBLEM** - the trigger is in the Problem state|
|*Host*|Host of the trigger.<br>This column is displayed only if multiple hosts are selected in the filter.|
|*Name*|Name of the trigger, displayed as a blue link to trigger details.<br>Clicking on the trigger name link opens the trigger [configuration form](/manual/config/triggers/trigger#configuration).<br>If the host trigger belongs to a template, the template name is displayed before the trigger name, as a gray link. Clicking on the template link will open the trigger list on the template level.<br>If the trigger has been created from a trigger prototype, its name is preceded by the low-level discovery rule name, in orange. Clicking on the discovery rule name will open the trigger prototype list.|
|*Operational data*|Operational data definition of the trigger, containing arbitrary strings and macros that will resolve dynamically in *Monitoring* → *Problems*.|
|*Expression*|Trigger expression is displayed. The host-item part of the expression is displayed as a link, leading to the item configuration form.|
|*Status*|Trigger status is displayed - *Enabled*, *Disabled* or *Unknown*. By clicking on the status you can change it - from Enabled to Disabled (and back); from Unknown to Disabled (and back).<br>Problems of a disabled trigger are no longer displayed in the frontend, but are not deleted.|
|*Info*|If everything is working correctly, no icon is displayed in this column. In case of errors, a square icon with the letter "i" is displayed. Hover over the icon to see a tooltip with the error description.|
|*Tags*|If a trigger contains tags, tag name and value are displayed in this column.|

To configure a new trigger, click on the *Create trigger* button at the
top right corner.

[comment]: # ({/cf3f7516-de145ee1})

[comment]: # ({4829e943-4829e943})
##### Mass editing options

Buttons below the list offer some mass-editing options:

-   *Enable* - change trigger status to *Enabled*.
-   *Disable* - change trigger status to *Disabled*.
-   *Copy* - copy the triggers to other hosts or templates.
-   *Mass update* - update several properties for a number of triggers
    at once.
-   *Delete* - delete the triggers.

To use these options, mark the checkboxes before the respective
triggers, then click on the required button.

[comment]: # ({/4829e943-4829e943})

[comment]: # ({dc50fb69-8aa798f5})
#### Using filter

You can use the filter to display only the triggers you are interested
in. For better search performance, data is searched with macros
unresolved.

The *Filter* icon is available at the top right corner. Clicking on it
will open a filter where you can specify the desired filtering criteria.

![](../../../../../../assets/en/manual/web_interface/trigger_filter.png){width="600"}

|Parameter|Description|
|--|--------|
|*Host groups*|Filter by one or more host groups.<br>Specifying a parent host group implicitly selects all nested host groups.<br>Host groups containing templates only cannot be selected.|
|*Hosts*|Filter by one or more hosts.<br>If host groups are already selected above, host selection is limited to those groups.|
|*Name*|Filter by trigger name.|
|*Severity*|Select to filter by one or several trigger severities.|
|*State*|Filter by trigger state.|
|*Status*|Filter by trigger status.|
|*Value*|Filter by trigger value.|
|*Tags*|Filter by trigger tag name and value. It is possible to include as well as exclude specific tags and tag values. Several conditions can be set. Tag name matching is always case-sensitive.<br>There are several operators available for each condition:<br>**Exists** - include the specified tag names<br>**Equals** - include the specified tag names and values (case-sensitive)<br>**Contains** - include the specified tag names where the tag values contain the entered string (substring match, case-insensitive)<br>**Does not exist** - exclude the specified tag names<br>**Does not equal** - exclude the specified tag names and values (case-sensitive)<br>**Does not contain** - exclude the specified tag names where the tag values contain the entered string (substring match, case-insensitive)<br>There are two calculation types for conditions:<br>**And/Or** - all conditions must be met, conditions having the same tag name will be grouped by the Or condition<br>**Or** - enough if one condition is met<br>Macros and [macro functions](/manual/config/macros/macro_functions) are supported both in tag name and tag value fields.|
|*Inherited*|Filter triggers inherited (or not inherited) from a template.|
|*Discovered*|Filter triggers discovered (or not discovered) by low-level discovery.|
|*With dependencies*|Filter triggers with (or without) dependencies.|

[comment]: # ({/dc50fb69-8aa798f5})
