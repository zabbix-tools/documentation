[comment]: # ({f72762fa-f72762fa})
# 1 Items

[comment]: # ({/f72762fa-f72762fa})

[comment]: # ({7c4e9698-6feb614f})
#### Overview

The item list for a host can be accessed from *Data collection → Hosts* by
clicking on *Items* for the respective host.

A list of existing items is displayed.

![](../../../../../../assets/en/manual/web_interface/items.png){width="600"}

Displayed data:

|Column|Description|
|--|--------|
|*Item context menu*|Click on the three-dot icon to open the [item context menu](/manual/web_interface/menu/item_menu).|
|*Host*|Host of the item.<br>This column is displayed only if multiple hosts are selected in the filter.|
|*Name*|Name of the item displayed as a blue link to item details.<br>Clicking on the item name link opens the item [configuration form](/manual/config/items/item#configuration).<br>If the host item belongs to a template, the template name is displayed before the item name as a gray link. Clicking on the template link will open the item list on the template level.<br>If the item has been created from an item prototype, its name is preceded by the low-level discovery rule name, in orange. Clicking on the discovery rule name will open the item prototype list.|
|*Triggers*|Moving the mouse over Triggers will display an infobox displaying the triggers associated with the item.<br>The number of the triggers is displayed in gray.|
|*Key*|Item key is displayed.|
|*Interval*|Frequency of the check is displayed.<br>*Note* that passive items can also be checked immediately by pushing the *Execute now* [button](#mass_editing_options).|
|*History*|How many days item data history will be kept is displayed.|
|*Trends*|How many days item trends history will be kept is displayed.|
|*Type*|Item type is displayed (Zabbix agent, SNMP agent, simple check, etc).|
|*Status*|Item status is displayed - *Enabled*, *Disabled* or *Not supported*. You can change the status by clicking on it - from Enabled to Disabled (and back); from Not supported to Disabled (and back).|
|*Tags*|Item tags are displayed.<br>Up to three tags (name:value pairs) can be displayed. If there are more tags, a "..." link is displayed that allows to see all tags on mouseover.|
|*Info*|If the item is working correctly, no icon is displayed in this column. In case of errors, a square icon with the letter "i" is displayed. Hover over the icon to see a tooltip with the error description.|

To configure a new item, click on the *Create item* button at the top
right corner.

[comment]: # ({/7c4e9698-6feb614f})

[comment]: # ({5a2f88a0-c14b7517})
##### Mass editing options

Buttons below the list offer some mass-editing options:

-   *Enable* - change item status to *Enabled*
-   *Disable* - change item status to *Disabled*
-   *Execute now* - execute a check for new item values immediately.
    Supported for **passive** checks only (see [more
    details](/manual/config/items/check_now)). Note that when checking
    for values immediately, configuration cache is not updated, thus the
    values will not reflect very recent changes to item configuration.
-   *Clear history* - delete history and trend data for items.
-   *Copy* - copy the items to other hosts or templates.
-   *Mass update* - [update several
    properties](/manual/config/items/itemupdate) for a number of items
    at once.
-   *Delete* - delete the items.

To use these options, mark the checkboxes before the respective items,
then click on the required button.

[comment]: # ({/5a2f88a0-c14b7517})

[comment]: # ({9e72937d-d8727c20})
##### Using filter

You can use the filter to display only the items you are interested in.
For better search performance, data is searched with macros unresolved.

The *Filter* icon is available at the top right corner. Clicking on it
will open a filter where you can specify the desired filtering criteria.

![](../../../../../../assets/en/manual/web_interface/item_filter.png){width="600"}

|Parameter|Description|
|--|--------|
|*Host groups*|Filter by one or more host groups.<br>Specifying a parent host group implicitly selects all nested host groups.<br>Host groups containing templates only cannot be selected.|
|*Hosts*|Filter by one or more hosts.|
|*Name*|Filter by item name.|
|*Key*|Filter by item key.|
|*Value mapping*|Filter by the value map used.<br>This parameter is not displayed if the *Hosts* option is empty.|
|*Type*|Filter by item type (Zabbix agent, SNMP agent, etc.).|
|*Type of information*|Filter by type of information (Numeric unsigned, float, etc.).|
|*History*|Filter by how long item history is kept.|
|*Trends*|Filter by how long item trends are kept.|
|*Update interval*|Filter by item update interval.|
|*Tags*|Specify tags to limit the number of items displayed. It is possible to include as well as exclude specific tags and tag values. Several conditions can be set. Tag name matching is always case-sensitive.<br>There are several operators available for each condition:<br>**Exists** - include the specified tag names<br>**Equals** - include the specified tag names and values (case-sensitive)<br>**Contains** - include the specified tag names where the tag values contain the entered string (substring match, case-insensitive)<br>**Does not exist** - exclude the specified tag names<br>**Does not equal** - exclude the specified tag names and values (case-sensitive)<br>**Does not contain** - exclude the specified tag names where the tag values contain the entered string (substring match, case-insensitive)<br>There are two calculation types for conditions:<br>**And/Or** - all conditions must be met, conditions having the same tag name will be grouped by the Or condition<br>**Or** - enough if one condition is met|
|*State*|Filter by item state - *Normal* or *Not supported*.|
|*Status*|Filter by item status - *Enabled* or *Disabled*.|
|*Triggers*|Filter items with (or without) triggers.|
|*Inherited*|Filter items inherited (or not inherited) from a template.|
|*Discovery*|Filter items discovered (or not discovered) by low-level discovery.|

The **Subfilter** below the filter offers further filtering options (for
the data already filtered). You can select groups of items with a common
parameter value. Upon clicking on a group, it gets highlighted and only
the items with this parameter value remain in the list.

[comment]: # ({/9e72937d-d8727c20})
