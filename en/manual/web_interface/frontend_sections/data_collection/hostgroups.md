[comment]: # ({86b74647-d3c68ec7})
# 2 Host groups

[comment]: # ({/86b74647-d3c68ec7})

[comment]: # ({db417806-884d1193})
#### Overview

In the *Data collection* → *Host groups* section users can configure and
maintain host groups. 

A listing of existing host groups with their details is displayed. You
can search and filter host groups by name.

![](../../../../../assets/en/manual/web_interface/host_groups1.png){width="600"}

Displayed data:

|Column|Description|
|--|--------|
|*Name*|Name of the host group. Clicking on the group name opens the group [configuration form](/manual/config/hosts/host#configuring_a_host_group).|
|*Hosts*|Number of hosts in the group (displayed in gray) followed by the list of group members. <br> Clicking on a host name will open the host configuration form.<br> Clicking on the number will, in the whole listing of hosts, filter out those that belong to the group.|
|*Info*|Error information (if any) regarding the host group is displayed.|

[comment]: # ({/db417806-884d1193})

[comment]: # ({f7827105-f7827105})
##### Mass editing options

Buttons below the list offer some mass-editing options:

-   *Enable hosts* - change the status of all hosts in the group to
    "Monitored"
-   *Disable hosts* - change the status of all hosts in the group to
    "Not monitored"
-   *Delete* - delete the host groups

To use these options, mark the checkboxes before the respective host
groups, then click on the required button.

[comment]: # ({/f7827105-f7827105})

[comment]: # ({f0d59a13-f0d59a13})
##### Using filter

You can use the filter to display only the host groups you are
interested in. For better search performance, data is searched with
macros unresolved.

[comment]: # ({/f0d59a13-f0d59a13})
