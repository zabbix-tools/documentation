[comment]: # ({0f054c43-0f054c43})
# 7 Discovery

[comment]: # ({/0f054c43-0f054c43})

[comment]: # ({0a4d673a-bb0be016})
#### Overview

In the *Data collection → Discovery* section users can configure and
maintain discovery rules.

A listing of existing discovery rules with their details is displayed.

![](../../../../../assets/en/manual/web_interface/discovery_rules0.png){width="600"}

Displayed data:

|Column|Description|
|--|--------|
|*Name*|Name of the discovery rule. Clicking on the discovery rule name opens the discovery rule [configuration form](/manual/discovery/network_discovery/rule).|
|*IP range*|The range of IP addresses to use for network scanning is displayed.|
|*Proxy*|The proxy name is displayed, if discovery is performed by the proxy.|
|*Interval*|The frequency of performing discovery displayed.|
|*Checks*|The types of checks used for discovery are displayed.|
|*Status*|Action status is displayed - *Enabled* or *Disabled*.<br>By clicking on the status you can change it.|

To configure a new discovery rule, click on the *Create discovery rule*
button in the top right-hand corner.

[comment]: # ({/0a4d673a-bb0be016})

[comment]: # ({341916c8-341916c8})
##### Mass editing options

Buttons below the list offer some mass-editing options:

-   *Enable* - change the discovery rule status to *Enabled*
-   *Disable* - change the discovery rule status to *Disabled*
-   *Delete* - delete the discovery rules

To use these options, mark the checkboxes before the respective
discovery rules, then click on the required button.

[comment]: # ({/341916c8-341916c8})

[comment]: # ({bae19eae-bae19eae})
##### Using filter

You can use the filter to display only the discovery rules you are
interested in. For better search performance, data is searched with
macros unresolved.

The *Filter* link is available above the list of discovery rules. If you
click on it, a filter becomes available where you can filter discovery
rules by name and status.

![](../../../../../assets/en/manual/web_interface/discovery_rules_filter1.png){width="600"}

[comment]: # ({/bae19eae-bae19eae})
