[comment]: # ({e4e11bc3-542796d3})

# 3 Scripts

[comment]: # ({/e4e11bc3-542796d3})

[comment]: # ({0071f75c-58923c7d})
#### Overview

In the *Alerts → Scripts* section user-defined global scripts can be configured and maintained.

Global scripts, depending on the configured scope and also user permissions, are available for execution:

-   from the [host menu](/manual/web_interface/menu/host_menu) in various frontend locations 
    (*Dashboard*, *Problems*, *Latest data*, *Maps*, etc.)
-   from the [event menu](/manual/web_interface/menu/event_menu)
-   can be run as an action operation

The scripts are executed on Zabbix agent, Zabbix server (proxy) or Zabbix server only. 
See also [Command execution](/manual/appendix/command_execution).

Both on Zabbix agent and Zabbix proxy remote scripts are disabled by default. 
They can be enabled by:

-   For remote commands executed on Zabbix agent:
    - adding an AllowKey=system.run[<command>,\*] parameter for each allowed command in agent configuration,
     \* stands for wait and nowait mode;
-   For remote commands executed on Zabbix proxy:
    - **Warning: It is not required to enable remote commands on Zabbix proxy if remote commands are executed 
    on Zabbix agent that is monitored by Zabbix proxy.** If, however, it is required to execute remote commands 
    on Zabbix proxy, set *EnableRemoteCommands* parameter to '1' in the proxy configuration.

A listing of existing scripts with their details is displayed.

![](../../../../../assets/en/manual/web_interface/scripts.png){width="600"}

Displayed data:

|Column|Description|
|--|--------|
|*Name*|Name of the script. Clicking on the script name opens the script [configuration form](scripts#configuring_a_global_script).|
|*Scope*|Scope of the script - action operation, manual host action or manual event action. This setting determines where the script is available.|
|*Used in actions*|Actions where the script is used are displayed.|
|*Type*|Script type is displayed - *URL*, *Webhook*, *Script*, *SSH*, *Telnet* or *IPMI* command.|
|*Execute on*|It is displayed whether the script will be executed on Zabbix agent, Zabbix server (proxy) or Zabbix server only.|
|*Commands*|All commands to be executed within the script are displayed.|
|*User group*|The user group that the script is available to is displayed (or *All* for all user groups).|
|*Host group*|The host group that the script is available for is displayed (or *All* for all host groups).|
|*Host access*|The permission level for the host group is displayed - *Read* or *Write*. Only users with the required permission level will have access to executing the script.|

To configure a new script, click on the *Create script* button in the top right-hand corner.

[comment]: # ({/0071f75c-58923c7d})

[comment]: # ({c3aba318-44c45ea0})
##### Mass editing options

A button below the list offers one mass-editing option:

-   *Delete* - delete the scripts

To use this option, mark the checkboxes before the respective scripts and click on *Delete*.

[comment]: # ({/c3aba318-44c45ea0})

[comment]: # ({6c10930f-1c5dd978})
##### Using filter

You can use the filter to display only the scripts you are interested in. For better search performance, data is 
searched with macros unresolved.

The *Filter* link is available above the list of scripts. If you click on it, a filter becomes available where you can 
filter scripts by name and scope.

![](../../../../../assets/en/manual/web_interface/script_filter.png){width="600"}

[comment]: # ({/6c10930f-1c5dd978})

[comment]: # ({0c7f5882-201a0633})
#### Configuring a global script

![](../../../../../assets/en/manual/web_interface/script.png){width="600"}

Script attributes:

|Parameter|<|Description|
|-|----------|----------------------------------------|
|*Name*|<|Unique name of the script.<br>E.g. `Clear /tmp filesystem`|
|*Scope*|<|Scope of the script - action operation, manual host action or manual event action. This setting determines where the script can be used - in remote commands of action operations, from the [host menu](/manual/web_interface/menu/host_menu) or from the [event menu](/manual/web_interface/menu/event_menu) respectively.<br>Setting the scope to 'Action operation' makes the script available for all users with access to *Alerts* → *Actions*.<br>If a script is actually used in an action, its scope cannot be changed away from 'action operation'.<br>**Macro support**<br>The scope affects the range of available macros. For example, user-related macros ({USER.\*}) are supported in scripts to allow passing information about the user that launched the script. However, they are not supported if the script scope is action operation, as action operations are executed automatically.<br>To find out which macros are supported, do a search for 'Trigger-based notifications and commands/Trigger-based commands', 'Manual host action scripts' and 'Manual event action scripts' in the [supported macro](/manual/appendix/macros/supported_by_location) table. Note that if a macro may resolve to a value with spaces (for example, host name), don't forget to quote as needed.|
|*Menu path*|<|The desired menu path to the script. For example, `Default` or `Default/`, will display the script in the respective directory. Menus can be nested, e.g. `Main menu/Sub menu1/Sub menu2`. When accessing scripts through the host/event menu in monitoring sections, they will be organized according to the given directories.<br>This field is displayed only if 'Manual host action' or 'Manual event action' is selected as *Scope*.|
|*Type*|<|Click the respective button to select script type:<br>**URL**, **Webhook**, **Script**, **SSH**, **Telnet** or **[IPMI](/manual/config/notifications/action/operation/remote_command#ipmi_remote_commands)** command.<br> The type **URL** is available only when 'Manual host action' or 'Manual event action' is selected as *Scope*. |
| |Script type: URL|<|
|^|*URL*|Specify the URL for quick access from the [host menu](/manual/web_interface/menu/host_menu) or [event menu](/manual/web_interface/menu/event_menu).<br>[Macros](/manual/appendix/macros/supported_by_location) and custom [user macros](/manual/config/macros/user_macros) are supported. Macro support depends on the scope of the script (see *Scope* above). <br> Macro values must not be URL-encoded. |
|^|*Open in new window*|Determines whether the URL should be opened in a new or the same browser tab.|
| |Script type: Webhook|<|
|^|*Parameters*|Specify the webhook variables as attribute-value pairs.<br>See also: [Webhook](/manual/config/notifications/media/webhook) media configuration.<br>[Macros](/manual/appendix/macros/supported_by_location) and custom [user macros](/manual/config/macros/user_macros) are supported in parameter values. Macro support depends on the scope of the script (see *Scope* above).|
|^|*Script*|Enter the JavaScript code in the block that appears when clicking in the parameter field (or on the view/edit button next to it).<br>Macro support depends on the scope of the script (see *Scope* above).<br>See also: [Webhook](/manual/config/notifications/media/webhook) media configuration, [Additional Javascript objects](/manual/config/items/preprocessing/javascript/javascript_objects).|
|^|*Timeout*|JavaScript execution timeout (1-60s, default 30s).<br>Time suffixes are supported, e.g. 30s, 1m.|
| |Script type: Script|<|
|^|*Execute on*|Click the respective button to execute the shell script on:<br>**Zabbix agent** - the script will be executed by Zabbix agent (if the system.run item is [allowed](/manual/config/items/restrict_checks)) on the host<br>**Zabbix server (proxy)** - the script will be executed by Zabbix server or proxy (if enabled by [EnableRemoteCommands](/manual/appendix/config/zabbix_proxy)) - depending on whether the host is monitored by server or proxy<br>**Zabbix server** - the script will be executed by Zabbix server only|
|^|*Commands*|Enter full path to the commands to be executed within the script.<br>Macro support depends on the scope of the script (see *Scope* above). Custom [user macros](/manual/config/macros/user_macros) are supported.|
| |Script type: SSH|<|
|^|*Authentication method*|Select authentication method - password or public key.|
|^|*Username*|Enter the username.|
|^|*Password*|Enter the password.<br>This field is available if 'Password' is selected as the authentication method.|
|^|*Public key file*|Enter the path to the public key file.<br>This field is available if 'Public key' is selected as the authentication method.|
|^|*Private key file*|Enter the path to the private key file.<br>This field is available if 'Public key' is selected as the authentication method.|
|^|*Passphrase*|Enter the passphrase.<br>This field is available if 'Public key' is selected as the authentication method.|
|^|*Port*|Enter the port.|
|^|*Commands*|Enter the commands.<br>Macro support depends on the scope of the script (see *Scope* above). Custom [user macros](/manual/config/macros/user_macros) are supported.|
| |Script type: Telnet|<|
|^|*Username*|Enter the username.|
|^|*Password*|Enter the password.|
|^|*Port*|Enter the port.|
|^|*Commands*|Enter the commands.<br>Macro support depends on the scope of the script (see *Scope* above). Custom [user macros](/manual/config/macros/user_macros) are supported.|
| |Script type: IPMI|<|
|^|*Command*|Enter the IPMI command.<br>Macro support depends on the scope of the script (see *Scope* above). Custom [user macros](/manual/config/macros/user_macros) are supported.|
|*Description*|<|Enter a description for the script.|
|*Host group*|<|Select the host group that the script will be available for (or *All* for all host groups).|
|*User group*|<|Select the user group that the script will be available to (or *All* for all user groups).<br>This field is displayed only if 'Manual host action' or 'Manual event action' is selected as *Scope*.|
|*Required host permissions*|<|Select the permission level for the host group - *Read* or *Write*. Only users with the required permission level will have access to executing the script.<br>This field is displayed only if 'Manual host action' or 'Manual event action' is selected as *Scope*.|
|*Enable confirmation*|<|Mark the checkbox to display a confirmation message before executing the script. This feature might be especially useful with potentially dangerous operations (like a reboot script) or ones that might take a long time.<br>This option is displayed only if 'Manual host action' or 'Manual event action' is selected as *Scope*.|
|*Confirmation text*|<|Enter a custom confirmation text for the confirmation popup enabled with the checkbox above (for example, *Remote system will be rebooted. Are you sure?*). To see how the text will look like, click on *Test confirmation* next to the field.<br>[Macros](/manual/appendix/macros/supported_by_location) and custom [user macros](/manual/config/macros/user_macros) are supported.<br>*Note:* the macros will not be expanded when testing the confirmation message.<br>This field is displayed only if 'Manual host action' or 'Manual event action' is selected as *Scope*.|

[comment]: # ({/0c7f5882-201a0633})

[comment]: # ({28a692cd-ca68e16a})
#### Script execution and result

Scripts run by Zabbix server are executed by the order described in 
[Command execution](/manual/appendix/command_execution) section including exit code checking. The script result will 
be displayed in a pop-up window that will appear after the script is run.

*Note:* The return value of the script is standard output together with standard error.

See an example of a script and the result window below:

    uname -v
    /tmp/non_existing_script.sh
    echo "This script was started by {USER.USERNAME}"

![](../../../../../assets/en/manual/web_interface/frontend_sections/administration/script_result.png)

The script result does not display the script itself.

[comment]: # ({/28a692cd-ca68e16a})

[comment]: # ({96d6e598-96d6e598})
#### Script timeout

[comment]: # ({/96d6e598-96d6e598})

[comment]: # ({9e753bc9-5b600786})
##### Zabbix agent

You may encounter a situation when a timeout occurs while executing a script.

See an example of a script running on Zabbix agent and the result window below:

    sleep 5
    df -h

![](../../../../../assets/en/manual/web_interface/frontend_sections/administration/script_timeout_1.png)

The error message, in this case, is the following:

    Timeout while executing a shell script.

To avoid such situations, it is advised to optimize the script itself (in the example above, "5") instead of adjusting the `Timeout` parameter in [Zabbix agent configuration](/manual/appendix/config/zabbix_agentd#timeout) and [Zabbix server configuration](/manual/appendix/config/zabbix_server#timeout).
However, for Zabbix agent in active mode, the `Timeout` parameter in [Zabbix server configuration](/manual/appendix/config/zabbix_server#timeout) should be at least several seconds longer than the `RefreshActiveChecks` parameter in [Zabbix agent configuration](/manual/appendix/config/zabbix_agentd#refreshactivechecks).
This ensures that the server has enough time to receive the active check results from the agent. Note that script execution on an active agent is supported since Zabbix agent 7.0.

In case the `Timeout` parameter has been changed in [Zabbix agent configuration](/manual/appendix/config/zabbix_agentd), the following error message will appear:

    Get value from agent failed: ZBX_TCP_READ() timed out.

It means that the modification has been made in [Zabbix agent configuration](/manual/appendix/config/zabbix_agentd), but it is required to modify the `Timeout` parameter in [Zabbix server configuration](/manual/appendix/config/zabbix_server) as well.

[comment]: # ({/9e753bc9-5b600786})

[comment]: # ({576a2ecf-0b38e381})
##### Zabbix server/proxy

See an example of a script running on Zabbix server and the result window below:

    sleep 11
    df -h

![](../../../../../assets/en/manual/web_interface/frontend_sections/administration/script_timeout_3.png)

It is also advised to optimize the script itself (instead of adjusting `TrapperTimeout` parameter to a corresponding value 
(in our case, > ‘11’) by modifying the [Zabbix server configuration](/manual/appendix/config/zabbix_server)).

[comment]: # ({/576a2ecf-0b38e381})
