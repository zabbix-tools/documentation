[comment]: # ({eb414961-98ea19ec})
# 3 Services

[comment]: # ({/eb414961-98ea19ec})

[comment]: # ({55d55266-55d55266})
#### Overview

The Services menu is for the [service monitoring](/manual/it_services) functions of Zabbix.

[comment]: # ({/55d55266-55d55266})
