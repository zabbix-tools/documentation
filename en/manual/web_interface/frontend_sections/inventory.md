[comment]: # ({1a686ca2-7ed38ae8})
# 4 Inventory

[comment]: # ({/1a686ca2-7ed38ae8})

[comment]: # ({65644c90-65644c90})
#### Overview

The Inventory menu features sections providing an overview of host
inventory data by a chosen parameter as well as the ability to view host
inventory details.

[comment]: # ({/65644c90-65644c90})
