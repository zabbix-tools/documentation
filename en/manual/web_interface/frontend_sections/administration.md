[comment]: # ({c13d08e6-fa188d70})
# 9 Administration

[comment]: # ({/c13d08e6-fa188d70})

[comment]: # ({d36c3924-cded1b68})
#### Overview

The Administration menu is for administrative functions of Zabbix. This
menu is available to [SuperAdmin](/manual/config/users_and_usergroups/permissions) user type users only.

[comment]: # ({/d36c3924-cded1b68})
