[comment]: # ({49816b31-7574e81d})
# 7 Alerts

[comment]: # ({/49816b31-7574e81d})

[comment]: # ({ec90a991-b7053644})
#### Overview

This menu features sections that are related to configuring 
alerts in Zabbix.

[comment]: # ({/ec90a991-b7053644})
