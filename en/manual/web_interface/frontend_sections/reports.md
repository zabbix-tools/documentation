[comment]: # ({2370021b-9ada3186})
# 5 Reports

[comment]: # ({/2370021b-9ada3186})

[comment]: # ({8b50039d-8b50039d})
#### Overview

The Reports menu features several sections that contain a variety of
predefined and user-customizable reports focused on displaying an
overview of such parameters as system information, triggers and gathered
data.

[comment]: # ({/8b50039d-8b50039d})
