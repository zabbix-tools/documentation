[comment]: # ({968b5cfd-a637692c})
# 5 Discovery

[comment]: # ({/968b5cfd-a637692c})

[comment]: # ({6877d127-6877d127})
#### Overview

In the *Monitoring → Discovery* section results of [network
discovery](/manual/discovery/network_discovery) are shown. Discovered
devices are sorted by the discovery rule.

![](../../../../../assets/en/manual/web_interface/discovery_status0.png){width="600"}

If a device is already monitored, the host name will be listed in the
*Monitored host* column, and the duration of the device being discovered
or lost after previous discovery is shown in the *Uptime/Downtime*
column.

After that follow the columns showing the state of individual services
for each discovered device (red cells show services that are down).
Service uptime or downtime is included within the cell.

::: noteimportant
Only those services that have been found on at
least one device will have a column showing their state.
:::

[comment]: # ({/6877d127-6877d127})

[comment]: # ({84099469-84099469})
##### Buttons

View mode buttons being common for all sections are described on the
[Monitoring](/manual/web_interface/frontend_sections/monitoring#view_mode_buttons)
page.

[comment]: # ({/84099469-84099469})

[comment]: # ({8b46b0e3-8b46b0e3})
##### Using filter

You can use the filter to display only the discovery rules you are
interested in. For better search performance, data is searched with
macros unresolved.

With nothing selected in the filter, all enabled discovery rules are
displayed. To select a specific discovery rule for display, start typing
its name in the filter. All matching enabled discovery rules will be
listed for selection. More than one discovery rule can be selected.

[comment]: # ({/8b46b0e3-8b46b0e3})
