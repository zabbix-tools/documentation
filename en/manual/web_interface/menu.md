[comment]: # ({764560a7-764560a7})
# 1 Menu

[comment]: # ({/764560a7-764560a7})

[comment]: # ({5c43e04e-4edaa9a7})
### Overview

A vertical menu in a sidebar provides access to various Zabbix frontend
sections.

The menu is dark blue in the default theme.

![](../../../assets/en/manual/web_interface/vertical_menu.png){width="600"}

[comment]: # ({/5c43e04e-4edaa9a7})

[comment]: # ({89f5c060-6ddbf7a4})
### Working with the menu

A [global search](/manual/web_interface/global_search) box is located
below the Zabbix logo.

The menu can be collapsed or hidden completely:

-   To collapse, click on
    ![](../../../assets/en/manual/web_interface/button_collapse.png)
    next to Zabbix logo. In the collapsed menu only the icons are visible.

![](../../../assets/en/manual/web_interface/collapsed_menu.png)

-   To hide, click on
    ![](../../../assets/en/manual/web_interface/button_hide.png) next to
    Zabbix logo. In the hidden menu everything is hidden.

![](../../../assets/en/manual/web_interface/hidden_menu.png)

[comment]: # ({/89f5c060-6ddbf7a4})

[comment]: # ({aa77585c-3086e8b2})
#### Collapsed menu

When the menu is collapsed to icons only, a full menu reappears as soon
as the mouse cursor is placed upon it. Note that it reappears over page
content; to move page content to the right you have to click on the
expand button. If the mouse cursor again is placed outside the full
menu, the menu will collapse again after two seconds.

You can also make a collapsed menu reappear fully by hitting the Tab
key. Hitting the Tab key repeatedly will allow to focus on the next menu
element.

[comment]: # ({/aa77585c-3086e8b2})

[comment]: # ({4e1b1437-7dd192ee})
#### Hidden menu

Even when the menu is hidden completely, a full menu is just one mouse
click away, by clicking on the burger icon. Note that it reappears over
page content; to move page content to the right you have to unhide the
menu by clicking on the show sidebar button.

[comment]: # ({/4e1b1437-7dd192ee})

[comment]: # ({de86008e-0d46829f})

#### Menu levels

There are up to three levels in the menu.

![](../../../assets/en/manual/web_interface/menu_levels.png)

[comment]: # ({/de86008e-0d46829f})

[comment]: # ({fff9228f-5ec4ea88})
### Context menu

In addition to the main menu, Zabbix provides [host context menu](/manual/web_interface/menu/host_menu),
[item context menu](/manual/web_interface/menu/item_menu), and 
[event context menu](/manual/web_interface/menu/event_menu) for a quick access to and configuration of frequently used
entities, such as latest values, simple graphs, configuration form, related scripts or external links.
The context menus are accessible by clicking on the host, item or
problem/trigger name in supported locations (follow the links given above).

[comment]: # ({/fff9228f-5ec4ea88})
