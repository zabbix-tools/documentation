[comment]: # ({7eb2710c-7eb2710c})
# 11 Time zones

[comment]: # ({/7eb2710c-7eb2710c})

[comment]: # ({e102f05a-e102f05a})
#### Overview

The frontend time zone can be set globally in the frontend and adjusted
for individual users.

![](../../../assets/en/manual/web_interface/time_zone_global.png)

If *System* is selected, the web server time zone will be used for the
frontend (including the value of 'date.timezone' of php.ini, if set),
while Zabbix server will use the time zone of the machine it is running
on.

::: noteclassic
Zabbix server will only use the specified global/user
timezone when expanding macros in notifications (e.g. {EVENT.TIME} can
expand to a different time zone per user) and for the time limit when
notifications are sent (see "When active" setting in user [media
configuration](/manual/config/notifications/media#user_media)).
:::

[comment]: # ({/e102f05a-e102f05a})

[comment]: # ({297606c9-297606c9})
#### Configuration

The global timezone:

-   can be set manually when [installing](/manual/installation/frontend)
    the frontend
-   can be modified in *Administration* → *General* →
    *[GUI](/manual/web_interface/frontend_sections/administration/general#gui)*

User-level time zone:

-   can be set when
    [configuring/updating](/manual/config/users_and_usergroups/user#general_attributes)
    a user
-   can be set by each user in their [user
    profile](/manual/web_interface/user_profile#configuration)

[comment]: # ({/297606c9-297606c9})
