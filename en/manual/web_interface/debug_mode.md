[comment]: # ({18b1e10f-18b1e10f})
# 9 Debug mode

[comment]: # ({/18b1e10f-18b1e10f})

[comment]: # ({17174fc3-17174fc3})
#### Overview

Debug mode may be used to diagnose performance problems with frontend
pages.

[comment]: # ({/17174fc3-17174fc3})

[comment]: # ({5a1baa05-0cf216e8})
#### Configuration

Debug mode can be activated for individual users who belong to a user
group:

-   when configuring a [user
    group](/manual/config/users_and_usergroups/usergroup#configuration);
-   when viewing configured [user
    groups](//manual/web_interface/frontend_sections/users/user_groups).

When *Debug mode* is enabled for a user group, its users will see a
*Debug* button in the lower right corner of the browser window:

![](../../../assets/en/manual/web_interface/debug_button.png)

Clicking on the *Debug* button opens a new window below the page
contents which contains the SQL statistics of the page, along with a
list of API calls and individual SQL statements:

![](../../../assets/en/manual/web_interface/debug_mode.png){width="600"}

In case of performance problems with the page, this window may be used
to search for the root cause of the problem.

::: notewarning
Enabled *Debug mode* negatively affects frontend
performance.
:::

[comment]: # ({/5a1baa05-0cf216e8})
