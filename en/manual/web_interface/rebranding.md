[comment]: # ({5af64792-5af64792})
# 12 Rebranding

[comment]: # ({/5af64792-5af64792})

[comment]: # ({e32931f2-0e860221})
#### Overview

There are several ways in which you can customize and rebrand your Zabbix frontend installation:

-   replace the Zabbix logo with a desired one;
-   hide the links to the *Zabbix Support* and *Zabbix Integrations* pages;
-   set a custom link to the *Help* page;
-   change the copyright notice in the footer.

[comment]: # ({/e32931f2-0e860221})

[comment]: # ({672ab486-e820f754})
#### How to

First you need to create a PHP file and save it as *local/conf/brand.conf.php*.
The contents of the file should be as follows:

```php
<?php

return [];
```

This will hide the links to the *Zabbix Support* and *Zabbix Integrations* pages.

[comment]: # ({/672ab486-e820f754})

[comment]: # ({d8eb0f58-9245e2e7})
##### Custom logo

To use a custom logo, add the following line to the array from the previous listing:

```php
'BRAND_LOGO' => '{Path to an image on the disk or URL}',
```

Note that the path to the image on the disk should be relative to the Zabbix frontend root directory.
So, for example, if the image is located in the same directory as the *brand.conf.php* file, then the path to the image should be: `'./local/conf/custom_logo.png'`.

There are two additional images of the Zabbix logo that can be overridden:

-   `BRAND_LOGO_SIDEBAR` - displayed when the sidebar is expanded;
-   `BRAND_LOGO_SIDEBAR_COMPACT` - displayed when the sidebar is collapsed.

To override:

```php
'BRAND_LOGO_SIDEBAR' => '{Path to an image on the disk or URL}',
'BRAND_LOGO_SIDEBAR_COMPACT' => '{Path to an image on the disk or URL}',
```

Any image format supported by modern browsers can be used: JPG, PNG, SVG, BMP, WebP, and GIF.

::: noteclassic
Custom logos will not be scaled, resized or modified in any other way, and will be displayed in their original size and proportions,
except for the `BRAND_LOGO_SIDEBAR_COMPACT` that will be cropped (width: 24px; height: the same as `BRAND_LOGO_SIDEBAR`) to fit the collapsed sidebar.
For reference, the following dimensions are used for Zabbix logos:<br><br>

-   `BRAND_LOGO`: 114x30px
-   `BRAND_LOGO_SIDEBAR`: 91x24px
-   `BRAND_LOGO_SIDEBAR_COMPACT`: 24x24px
:::

[comment]: # ({/d8eb0f58-9245e2e7})

[comment]: # ({4ddb21dc-b118dd81})
##### Custom copyright notice

To set a custom copyright notice, add `BRAND_FOOTER` to the array from the first listing.
Please be aware that HTML is not supported here.
Setting `BRAND_FOOTER` to an empty string will hide the copyright notice completely (but the footer will stay in place).

```php
'BRAND_FOOTER' => '{text}',
```

[comment]: # ({/4ddb21dc-b118dd81})

[comment]: # ({4b4bf1a7-042915f3})
##### Custom help location

To replace the default *Help* link with a link of your choice, add `BRAND_HELP_URL` to the array from the first listing.

```php
'BRAND_HELP_URL' => '{URL}'
```

[comment]: # ({/4b4bf1a7-042915f3})

[comment]: # ({c66556d8-a1618b57})
##### File example

```php
<?php

return [
    'BRAND_LOGO' => './my_images/custom_logo.png',
    'BRAND_LOGO_SIDEBAR' => './my_images/custom_logo_sidebar.png',
    'BRAND_LOGO_SIDEBAR_COMPACT' => './my_images/custom_logo_sidebar_compact.png',
    'BRAND_FOOTER' => '© Zabbix',
    'BRAND_HELP_URL' => 'https://www.example.com/help/'
];
```

[comment]: # ({/c66556d8-a1618b57})
