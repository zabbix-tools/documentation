[comment]: # ({4c0e8118-5bf78e6f})

# 2 Host context menu

[comment]: # ({/4c0e8118-5bf78e6f})

[comment]: # ({f7892651-5b802363})

## Overview

The host context menu provides shortcuts for executing frequently required actions or navigating to UI sections related to a host.

![](../../../../assets/en/manual/web_interface/host_context_menu.png)

[comment]: # ({/f7892651-5b802363})

[comment]: # ({c5b40924-60febe87})

## Content

The host context menu has four sections: *View*, *Configuration*, *Links*, and *Scripts*.
For the entities that are not configured, links are disabled and displayed in grey color.
The sections *Scripts* and *Links* are displayed if their entities are configured.

*View* section contains links to:

-  **Dashboards** - opens widgets and graphs.
-  **Problems** - opens the *Problems* section with the list of unresolved problems of the underlying trigger.
-  **Latest data** - opens the *Latest data* section with the list of all the latest data of the current host.
-  **Graphs** - opens simple graphs of the current host.
-  **Web**- opens the link to the configured web scenarios.
-  **Inventory** - opens the link to the inventory of the current host.

*Configuration* section contains links to:

-  **Host** - configuration form of the current host.
-  **Items** - the list of the current host items.
-  **Triggers** - the list of the current host triggers.
-  **Graphs** - simple graphs of the current host.
-  **Discovery** - the list of low-level discovery rules of the current host.
-  **Web** - the list of web scenarios of the current host.

::: notetip
Note that configuration section is available only for Admin and Super admin users.
:::

*Links* section contains links to:

-  access a configured [trigger URL](/manual/config/triggers/trigger#configuration).
-  access custom links configured in [Global scripts](/manual/web_interface/frontend_sections/alerts/scripts)
  (with scope *Manual host action* and type 'URL').

*Scripts* section allows to execute [global scripts](/manual/web_interface/frontend_sections/alerts/scripts)
configured for the current host. These scripts need to have their scope defined as *Manual host action* to be available
in the host menu.

[comment]: # ({/c5b40924-60febe87})

[comment]: # ({59b15f74-3ad956d9})

## Supported locations

The host menu is accessible by clicking on a host in various frontend sections, for example:

- Dashboards [widgets](/manual/web_interface/frontend_sections/dashboards/widgets), such as Data overview, Trigger
  overview, Problems, etc.
- Monitoring → [Problems](/manual/web_interface/frontend_sections/monitoring/problems)
- Monitoring → [Problems](/manual/web_interface/frontend_sections/monitoring/problems) → Event details
- Monitoring → [Hosts](/manual/web_interface/frontend_sections/monitoring/hosts)
- Monitoring → Hosts → [Web Monitoring](/manual/web_interface/frontend_sections/monitoring/hosts/web)
- Monitoring → [Latest data](/manual/web_interface/frontend_sections/monitoring/latest_data)
- Monitoring → [Maps](/manual/web_interface/frontend_sections/monitoring/maps)
- Inventory → [Hosts](/manual/web_interface/frontend_sections/inventory/hosts)
- Reports → [Triggers top 100](/manual/web_interface/frontend_sections/reports/triggers_top)

[comment]: # ({/59b15f74-3ad956d9})
