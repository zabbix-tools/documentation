<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="en" datatype="plaintext" original="manual/web_interface/maintenance_mode.md">
    <body>
      <trans-unit id="8fc97588" xml:space="preserve">
        <source># 5 Frontend maintenance mode</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/web_interface/maintenance_mode.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="fca7c17f" xml:space="preserve">
        <source>#### Overview

Zabbix web frontend can be temporarily disabled in order to prohibit
access to it. This can be useful for protecting the Zabbix database from
any changes initiated by users, thus protecting the integrity of
database.

Zabbix database can be stopped and maintenance tasks can be performed
while Zabbix frontend is in maintenance mode.

Users from defined IP addresses will be able to work with the frontend
normally during maintenance mode.</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/web_interface/maintenance_mode.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="dcfd8b13" xml:space="preserve">
        <source>#### Configuration

In order to enable maintenance mode, the `maintenance.inc.php` file
(located in /conf of Zabbix HTML document directory on the
web server) must be modified to uncomment the following lines:

    // Maintenance mode.
    define('ZBX_DENY_GUI_ACCESS', 1);

    // Array of IP addresses, which are allowed to connect to frontend (optional).
    $ZBX_GUI_ACCESS_IP_RANGE = array('127.0.0.1');

    // Message shown on warning screen (optional).
    $ZBX_GUI_ACCESS_MESSAGE = 'We are upgrading MySQL database till 15:00. Stay tuned...';</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/web_interface/maintenance_mode.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="ae331777" xml:space="preserve">
        <source>
::: notetip
Mostly the `maintenance.inc.php` file is located in `/conf` of Zabbix HTML document directory on the
web server. However, the location of the directory may differ depending on the operating system and a web server it uses.

For example, the location for:

-  SUSE and RedHat is `/etc/zabbix/web/maintenance.inc.php`.
-  Debian-based systems is `/usr/share/zabbix/conf/`.

See also [Copying PHP files](/manual/installation/install#copying-php-files). 
:::

|Parameter|Details|
|--|--------|
|**ZBX\_DENY\_GUI\_ACCESS**|Enable maintenance mode:&lt;br&gt;1 – maintenance mode is enabled, disabled otherwise|
|**ZBX\_GUI\_ACCESS\_IP\_RANGE**|Array of IP addresses, which are allowed to connect to frontend (optional).&lt;br&gt;For example:&lt;br&gt;`array('192.168.1.1', '192.168.1.2')`|
|**ZBX\_GUI\_ACCESS\_MESSAGE**|A message you can enter to inform users about the maintenance (optional).|

Note that the [location](/manual/installation/install#copying-php-files) of the `/conf` directory will vary based on the operating system and web server. </source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/web_interface/maintenance_mode.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="987fdc44" xml:space="preserve">
        <source>#### Display

The following screen will be displayed when trying to access the Zabbix
frontend while in maintenance mode. The screen is refreshed every 30
seconds in order to return to a normal state without user intervention
when the maintenance is over.

![](../../../assets/en/manual/web_interface/frontend_maintenance.png)

IP addresses defined in ZBX\_GUI\_ACCESS\_IP\_RANGE will be able to
access the frontend as always.</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/web_interface/maintenance_mode.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
    </body>
  </file>
</xliff>
