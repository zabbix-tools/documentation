<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="en" datatype="plaintext" original="manual/introduction/overview.md">
    <body>
      <trans-unit id="bade5a47" xml:space="preserve">
        <source># 4 Zabbix overview</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/introduction/overview.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="b4d6d531" xml:space="preserve">
        <source>#### Architecture

Zabbix consists of several major software components. Their
responsibilities are outlined below.</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/introduction/overview.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="5957a1ba" xml:space="preserve">
        <source>##### Server

[Zabbix server](/manual/concepts/server) is the central component to
which agents report availability and integrity information and
statistics. The server is the central repository in which all
configuration, statistical and operational data are stored.</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/introduction/overview.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="494fb840" xml:space="preserve">
        <source>##### Database storage

All configuration information as well as the data gathered by Zabbix is
stored in a database.</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/introduction/overview.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="bd637ffa" xml:space="preserve">
        <source>##### Web interface

For an easy access to Zabbix from anywhere and from any platform, the
web-based interface is provided. The interface is part of Zabbix server,
and usually (but not necessarily) runs on the same physical machine as
the one running the server.</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/introduction/overview.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="bfd730af" xml:space="preserve">
        <source>##### Proxy

[Zabbix proxy](/manual/concepts/proxy) can collect performance and
availability data on behalf of Zabbix server. A proxy is an optional
part of Zabbix deployment; however, it may be very beneficial to
distribute the load of a single Zabbix server.</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/introduction/overview.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="b9991630" xml:space="preserve">
        <source>##### Agent

Zabbix agents are deployed on monitoring targets to actively monitor
local resources and applications and report the gathered data to Zabbix
server. Since Zabbix 4.4, there are two types of agents available: the
[Zabbix agent](/manual/concepts/agent) (lightweight, supported on many
platforms, written in C) and the [Zabbix agent 2](/manual/concepts/agent2)
(extra-flexible, easily extendable with plugins, written in Go).</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/introduction/overview.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="9ec23332" xml:space="preserve">
        <source>#### Data flow

In addition it is important to take a step back and have a look at the
overall data flow within Zabbix. In order to create an item that gathers
data you must first create a host. Moving to the other end of the Zabbix
spectrum you must first have an item to create a trigger. You must have
a trigger to create an action. Thus if you want to receive an alert that
your CPU load is too high on *Server X* you must first create a host
entry for *Server X* followed by an item for monitoring its CPU, then a
trigger which activates if the CPU is too high, followed by an action
which sends you an email. While that may seem like a lot of steps, with
the use of templating it really isn't. However, due to this design it is
possible to create a very flexible setup.</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/introduction/overview.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
    </body>
  </file>
</xliff>
