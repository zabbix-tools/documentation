[comment]: # ({cbbea730-cbbea730})
# 1 Manual structure

[comment]: # ({/cbbea730-cbbea730})

[comment]: # ({14bd850a-14bd850a})
#### Structure

The content of this manual is divided into sections and subsections to
provide easy access to particular subjects of interest.

When you navigate to respective sections, make sure that you expand
section folders to reveal full content of what is included in
subsections and individual pages.

Cross-linking between pages of related content is provided as much as
possible to make sure that relevant information is not missed by the
users.

[comment]: # ({/14bd850a-14bd850a})

[comment]: # ({b3fa8c65-4f79bd48})
#### Sections

[Introduction](about) provides general information about current Zabbix
software. Reading this section should equip you with some good reasons
to choose Zabbix.

[Zabbix concepts](/manual/definitions) explain the terminology used in
Zabbix and provides details on Zabbix components.

[Installation](/manual/installation/getting_zabbix) and
[Quickstart](/manual/quickstart/item) sections should help you to get
started with Zabbix. [Zabbix appliance](/manual/appliance) is an
alternative for getting a quick taster of what it is like to use Zabbix.

[Configuration](/manual/config) is one of the largest and more important
sections in this manual. It contains loads of essential advice about how
to set up Zabbix to monitor your environment, from setting up hosts to
getting essential data to viewing data to configuring notifications and
remote commands to be executed in case of problems.

[Service monitoring](/manual/it_services) section details how to use Zabbix for
a high-level overview of your monitoring environment.

[Web monitoring](/manual/web_monitoring) should help you learn how to
monitor the availability of web sites.

[Virtual machine monitoring](/manual/vm_monitoring) presents a how-to
for configuring VMware environment monitoring.

[Maintenance](/manual/maintenance), [Regular
expressions](/manual/regular_expressions), [Event
acknowledgment](/manual/acknowledges) and [XML
export/import](/manual/xml_export_import) are further sections that
reveal how to use these various aspects of Zabbix software.

[Discovery](/manual/discovery) contains instructions for setting up
automatic discovery of network devices, active agents, file systems,
network interfaces, etc.

[Distributed monitoring](/manual/distributed_monitoring) deals with the
possibilities of using Zabbix in larger and more complex environments.

[Encryption](/manual/encryption) helps explaining the possibilities of
encrypting communications between Zabbix components.

[Web interface](/manual/web_interface) contains information specific for
using the web interface of Zabbix.

[API](/manual/api) section presents details of working with Zabbix API.

Detailed lists of technical information are included in
[Appendixes](/manual/appendix). This is where you will also find a FAQ
section.

[comment]: # ({/b3fa8c65-4f79bd48})
