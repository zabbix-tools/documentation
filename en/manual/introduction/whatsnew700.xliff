<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="en" datatype="plaintext" original="manual/introduction/whatsnew700.md">
    <body>
      <trans-unit id="06931244" xml:space="preserve">
        <source># 5 What's new in Zabbix 7.0.0</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/introduction/whatsnew700.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="e926bfb2" xml:space="preserve">
        <source>### Breaking changes

Zabbix 7.0.0 does not have any breaking changes yet.</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/introduction/whatsnew700.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="dc0a44cf" xml:space="preserve">
        <source>
### Concurrency in network discovery

Previously each network discovery rule was processed by one discoverer process. Thus all service checks within the rule could only be performed 
sequentially. 

In the new version the network discovery process has been reworked to allow concurrency between service checks. 
A new discovery manager process has been added along with a configurable number of discovery workers (or threads). 

The discovery manager processes discovery rules and creates a discovery job per each rule with tasks (service checks). 
The service checks are picked up and performed by the discovery workers. Only those checks that have the same IP and port are 
scheduled sequentially because some devices may not allow concurrent connections on the same port. 

A new `zabbix[discovery_queue]` internal item allows to monitor the number of discovery checks in the queue.

The [StartDiscoverers](/manual/appendix/config/zabbix_server#startdiscoverers) parameter now determines the total number of available discovery workers for discovery. The default number of StartDiscoverers has been upped from 1 to 5, and the range from 0-250 to 0-1000. 
The `discoverer` processes from previous Zabbix versions have been dropped.

Additionally, the number of available workers per each rule is now configurable in the [frontend](/manual/discovery/network_discovery/rule). This parameter is optional.</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/introduction/whatsnew700.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="b6cdcf96" xml:space="preserve">
        <source>### Items

The [system.sw.packages](/manual/config/items/itemtypes/zabbix_agent#system.sw.packages) and [system.sw.packages.get](/manual/config/items/itemtypes/zabbix_agent#system.sw.packages.get) 
agent items are now supported on Gentoo Linux.

The [system.hostname](/manual/config/items/itemtypes/zabbix_agent#system.hostname) item now can return a Fully Qualified Domain Name,
if the new *fqdn* option is specified in the **type** parameter.

The **[wmi.get](/manual/config/items/itemtypes/zabbix_agent/win_keys#wmi-get)** and **[wmi.getall](/manual/config/items/itemtypes/zabbix_agent/win_keys#wmi-getall)** items used with Zabbix agent 2 now return JSON with boolean values represented as strings (for example, `"RealTimeProtectionEnabled": "True"` instead of `"RealTimeProtectionEnabled": true` returned previously) to match the output format of these items on Zabbix agent.

A new **options** parameter has been added to the [icmpping](/manual/config/items/itemtypes/simple_checks#icmpping), [icmppingloss](/manual/config/items/itemtypes/simple_checks#icmppingloss), 
and [icmppingsec](/manual/config/items/itemtypes/simple_checks#icmppingsec) items. This parameter can be used to specify whether redirected responses should be treated as 
target host up or target host down. See [simple checks](/manual/config/items/itemtypes/simple_checks#supported-simple-checks) for more details.

The [oracle.ts.discovery](/manual/config/items/itemtypes/zabbix_agent/zabbix_agent2?hl=oracle.ts.stats#oracle.ts.discovery) Zabbix agent 2 item now returns a new {#CON_NAME} LLD macro with container name.

The [oracle.ts.stats](/manual/config/items/itemtypes/zabbix_agent/zabbix_agent2#oracle.ts.stats) Zabbix agent 2 item has a new **conname** parameter to specify the target container name.
The JSON format of the returned data has been updated. When no **tablespace**, **type**, or **conname** is specified in the key parameters, the returned data will include an additional JSON level with the container name, allowing differentiation between containers.</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/introduction/whatsnew700.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="6b2dd9f4" xml:space="preserve">
        <source>### Functions</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/introduction/whatsnew700.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="45142bff" xml:space="preserve">
        <source>##### Updated functions

Several functions have been updated:

-   Aggregate functions now also support non-numeric types for calculation. This may be useful, for example, with the [count](/manual/appendix/functions/aggregate#count) and [count_foreach](/manual/appendix/functions/aggregate/foreach#additional-parameters) functions.
-   The [count](/manual/appendix/functions/aggregate#count) and [count_foreach](/manual/appendix/functions/aggregate/foreach#additional-parameters) aggregate functions support optional parameters *operator* and *pattern*, which can be used to fine-tune item filtering and only count values that match given criteria.
-   All [foreach functions](/appendix/functions/aggregate/foreach) no longer include unsupported items in the count.
-   The function **[last_foreach](/appendix/functions/aggregate/foreach#time-period)**, previously configured to ignore the time period argument, accepts it as an optional parameter.
-   Supported range for values returned by [prediction functions](/manual/appendix/functions/prediction) has been expanded to match the range of double data type.
    Now *timeleft()* function can accept values up to 1.7976931348623158E+308 and *forecast()* function can accept values ranging from -1.7976931348623158E+308 to 1.7976931348623158E+308.</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/introduction/whatsnew700.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="5f91b819" xml:space="preserve">
        <source>### Remote commands on active agents

Starting with Zabbix agent 7.0, it is now possible to execute [remote commands](/manual/config/notifications/action/operation/remote_command) on an agent that is operating in active mode.
Once the execution of a remote command is triggered by an action [operation](/manual/config/notifications/action/operation#operations) or manual [script](/manual/web_interface/frontend_sections/alerts/scripts) execution,
the command will be included in the active checks configuration and executed once the active agent receives it.
Note that older active agents will ignore any remote commands included in the active checks configuration.
For more information, see [*Passive and active agent checks*](/manual/appendix/items/activepassive#active-checks).</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/introduction/whatsnew700.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="1bc98669" xml:space="preserve">
        <source>### Increased character limit for configuration fields

##### URL fields

The character limit for all URL fields is now 2048 characters.
This now includes: *Tile URL* for settings related to [geographical maps](/manual/web_interface/frontend_sections/administration/general#geographical-maps),
*Frontend URL* for configuring miscellaneous [frontend parameters](/manual/web_interface/frontend_sections/administration/general#other-parameters),
*URLs* for [network maps](/manual/config/visualization/maps/map#creating-a-map) and [network map elements](/manual/config/visualization/maps/map#adding-elements),
*URL A-C* for [host inventory](/manual/api/reference/host/object#host-inventory) fields,
and *URL* for the [URL dashboard widget](/manual/web_interface/frontend_sections/dashboards/widgets/url#url).

##### Authentication fields

The character limit for authentication fields *User/User name* and *Password* is now 255 characters.
This applies to configuring HTTP authentication for [HTTP agent](/manual/config/items/itemtypes/http#configuration) items, [web scenarios](/manual/web_monitoring#configuring-authentication), and [connectors](/manual/config/export/streaming#configuration),
as well as configuring authentication for [simple checks](/manual/config/items/itemtypes/simple_checks/#overview),
[ODBC monitoring](/manual/config/items/itemtypes/odbc_checks#item-configuration-in-zabbix-frontend),
[SSH checks](/manual/config/items/itemtypes/ssh_checks#item-configuration),
[Telnet checks](/manual/config/items/itemtypes/telnet_checks),
and [JMX monitoring](/manual/config/items/itemtypes/jmx_monitoring#adding-jmx-agent-item).</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/introduction/whatsnew700.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="numeric" xml:space="preserve">
        <source>### Support for the old numeric type dropped

The old style of floating point values, previously deprecated, is no longer supported, as numeric values of extended range are used.</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/introduction/whatsnew700.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="7389886f" xml:space="preserve">
        <source>### Cloning simplified

Previously it was possible to *Clone* and *Full clone* [hosts](/manual/config/hosts/host#overview), [templates](/manual/config/templates/template#creating-a-template) and [maps](/manual/config/visualization/maps/map#creating-a-map).

Now the option *Clone* has been removed, and the option *Full clone* has been renamed to *Clone* while still preserving all of the previous functionality of *Full clone*.</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/introduction/whatsnew700.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="7d92d0f0" xml:space="preserve">
        <source>### Versions displayed

Zabbix frontend and Zabbix server version numbers are now viewable on the [System information page](/manual/web_interface/frontend_sections/reports/status_of_zabbix).</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/introduction/whatsnew700.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="4f802bf8" xml:space="preserve">
        <source>### Expanded widget availability on template dashboards

Previously, on a [template dashboard](/manual/config/templates/template#adding-dashboards), you could create only the following widgets: *Clock*, *Graph (classic)*, *Graph prototype*, *Item value*, *Plain text*, *URL*.

Now template dashboards support the creation of all widgets.</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/introduction/whatsnew700.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="8442e72a" xml:space="preserve">
        <source>### Collapsible advanced configuration

The *Advanced configuration* checkboxes, responsible for displaying advanced configuration options, have been replaced with collapsible blocks
(see, for example, [Connector configuration](/manual/config/export/streaming#configuration), [Service configuration](/manual/it_services/service_tree#service-configuration), [*Clock* widget configuration](/manual/web_interface/frontend_sections/dashboards/widgets/clock#configuration), etc.).
This improves user experience, as collapsing these blocks and saving the configuration will no longer reset the configured advanced options to their default values.</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/introduction/whatsnew700.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="35832a7c" xml:space="preserve">
        <source>### Frontend</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/introduction/whatsnew700.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="347ec551" xml:space="preserve">
        <source>##### Documentation link for each standard item

Each standard item now has a direct link from the frontend to its documentation page. 

![](../../../assets/en/manual/introduction/item_helper.png){width="600"}

The links are placed under the question mark icon, when opening the item helper window 
from the item configuration form (click on *Select* next to the item key field).</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/introduction/whatsnew700.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="e4dba1ee" xml:space="preserve">
        <source>##### Icons replaced by fonts

All icons in the frontend have been switched from icon image sheets to fonts.</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/introduction/whatsnew700.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="8a0c4fd0" xml:space="preserve">
        <source>##### Modal forms

Several frontend forms are now opened in modal (pop-up) windows:

- [Network discovery rule](/manual/discovery/network_discovery/rule#rule-attributes) configuration;
- [Global script](/manual/web_interface/frontend_sections/alerts/scripts#configuring-a-global-script) configuration;
- [Event correlation](/manual/config/event_correlation/global#configuration) configuration;
- [Module](/manual/web_interface/frontend_sections/administration/general#modules)
configuration;
- [Media type](/manual/config/notifications/media) configuration.</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/introduction/whatsnew700.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="f355ddd7" xml:space="preserve">
        <source>### Miscellaneous

Default value of the [BufferSize](/manual/appendix/config/zabbix_agent2) configuration parameter for Zabbix agent 2 has been increased from 100 to 1000.

Some [dashboard widget](/manual/web_interface/frontend_sections/dashboards/widgets) parameters with the label *Tags* have been renamed for more clarity:
*Item tags* (for *Data overview* widget), *Scenario tags* (for *Web monitoring* widget),
*Problem tags* (for *Graph*, *Problem hosts*, *Problems*, *Problems by severity*, and *Trigger overview* widget).</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/introduction/whatsnew700.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="325187ab" xml:space="preserve">
        <source>New filtering option has been added to [Latest data](manual/web_interface/frontend_sections/monitoring/latest_data) section: it now allows you to filter items by their state (supported/unsupported).

New *Acknowledgement status* filtering option has been added to [Problems](manual/web_interface/frontend_sections/monitoring/problems) section: it now allows you to filter problems by their state (unacknowledged/acknowledged/acknowledged by me).</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/introduction/whatsnew700.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
    </body>
  </file>
</xliff>
