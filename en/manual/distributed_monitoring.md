[comment]: # ({9f1711bf-9f1711bf})
# 16. Distributed monitoring

[comment]: # ({/9f1711bf-9f1711bf})

[comment]: # ({e207d054-e207d054})
#### Overview

Zabbix provides an effective and reliable way of monitoring a
distributed IT infrastructure using Zabbix
[proxies](/manual/distributed_monitoring/proxies).

Proxies can be used to collect data locally on behalf of a centralized
Zabbix server and then report the data to the server.

[comment]: # ({/e207d054-e207d054})

[comment]: # ({67181317-6744cab9})
##### Proxy features

When making a choice of using/not using a proxy, several considerations
must be taken into account.

| |Proxy|
|-|-----|
|*Lightweight*|**Yes**|
|*GUI*|No|
|*Works independently*|**Yes**|
|*Easy maintenance*|**Yes**|
|*Automatic DB creation*^1^|**Yes**|
|*Local administration*|No|
|*Ready for embedded hardware*|**Yes**|
|*One way TCP connections*|**Yes**|
|*Centralized configuration*|**Yes**|
|*Generates notifications*|No|

::: noteclassic
\[1\] Automatic DB creation feature only works with SQLite.
Other databases require a [manual
setup](/manual/installation/install#requirements).
:::

[comment]: # ({/67181317-6744cab9})
