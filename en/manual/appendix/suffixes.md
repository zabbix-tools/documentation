[comment]: # ({4d5c1b80-4d5c1b80})
# 8 Unit symbols

[comment]: # ({/4d5c1b80-4d5c1b80})

[comment]: # ({393f9290-393f9290})
#### Overview

Having to use some large numbers, for example '86400' to represent the
number of seconds in one day, is both difficult and error-prone. This is
why you can use some appropriate unit symbols (or suffixes) to simplify
Zabbix trigger expressions and item keys.

Instead of '86400' for the number of seconds you can simply enter '1d'.
Suffixes function as multipliers.

[comment]: # ({/393f9290-393f9290})

[comment]: # ({88d14140-163b4f50})
#### Time suffixes

For time you can use:

-   **s** - seconds (when used, works the same as the raw value)
-   **m** - minutes
-   **h** - hours
-   **d** - days
-   **w** - weeks
-   **M** - months ([trend functions](/manual/appendix/functions/trends) only)
-   **y** - years ([trend functions](/manual/appendix/functions/trends) only)

Time suffixes support only integer numbers (so '1h' is supported, '1,5h'
or '1.5h' are not; use '90m' instead).

Time suffixes are supported in:

-   trigger [expression](/manual/config/triggers/expression) constants
    and function parameters
-   constants of [calculated
    item](/manual/config/items/itemtypes/calculated) formulas
-   parameters of the **zabbix\[queue,<from>,<to>\]**
    [internal item](/manual/config/items/itemtypes/internal)
-   time period parameter of [aggregate
    calculations](/manual/config/items/itemtypes/calculated/aggregate)
-   item configuration ('Update interval', 'Custom intervals', 'History
    storage period' and 'Trend storage period' fields)
-   item prototype configuration ('Update interval', 'Custom intervals',
    'History storage period' and 'Trend storage period' fields)
-   low-level discovery rule configuration ('Update interval', 'Custom
    intervals', 'Keep lost resources' fields)
-   network discovery configuration ('Update interval' field)
-   web scenario configuration ('Update interval', 'Timeout' fields)
-   action operation configuration ('Default operation step duration',
    'Step duration' fields)
-   user profile settings ('Auto-logout', 'Refresh', 'Message timeout'
    fields)
-   graph
    [widget](/manual/web_interface/frontend_sections/dashboards/widgets/graph)
    of *Dashboards* ('Time shift' field)
-   *Administration* → *Housekeeping* (storage period
    fields)
-   *Administration* → *General* → *Trigger displaying options*
    ('Display OK triggers for', 'On status change triggers blink for'
    fields)
-   *Administration* → *General* → *Other* ('Login blocking interval'
    field and fields related to communication with Zabbix server)
-   Zabbix server `ha_set_failover_delay=delay` [runtime
    control](/manual/concepts/server#runtime_control) option

[comment]: # ({/88d14140-163b4f50})

[comment]: # ({11368220-11368220})
#### Memory suffixes

Memory size suffixes are supported in:

-   trigger [expression](/manual/config/triggers/expression) constants
    and function parameters
-   constants of [calculated
    item](/manual/config/items/itemtypes/calculated) formulas

For memory size you can use:

-   **K** - kilobyte
-   **M** - megabyte
-   **G** - gigabyte
-   **T** - terabyte

[comment]: # ({/11368220-11368220})

[comment]: # ({c9f73444-c9f73444})
#### Other uses

Unit symbols are also used for a human-readable representation of data
in the frontend.

In both Zabbix server and frontend these symbols are supported:

-   **K** - kilo
-   **M** - mega
-   **G** - giga
-   **T** - tera

When item values in B, Bps are displayed in the frontend, base 2 is
applied (1K = 1024). Otherwise a base of 10 is used (1K = 1000).

Additionally the frontend also supports the display of:

-   **P** - peta
-   **E** - exa
-   **Z** - zetta
-   **Y** - yotta

[comment]: # ({/c9f73444-c9f73444})

[comment]: # ({46f2ffdf-46f2ffdf})
#### Usage examples

By using some appropriate suffixes you can write trigger expressions
that are easier to understand and maintain, for example these
expressions:

    last(/host/system.uptime[])<86400s
    avg(/host/system.cpu.load,600s)<10
    last(/host/vm.memory.size[available])<20971520

could be changed to:

    last(/host/system.uptime[])<1d
    avg(/host/system.cpu.load,10m)<10
    last(/host/vm.memory.size[available])<20M

[comment]: # ({/46f2ffdf-46f2ffdf})
