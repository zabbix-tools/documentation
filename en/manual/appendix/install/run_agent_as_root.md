[comment]: # ({324a64cd-6f1ad3f2})
# 8 Running agent as root

Since Zabbix **5.0.0**, the systemd service file for Zabbix agent in [official
packages](https://www.zabbix.com/download) explicitly includes directives for `User` and `Group`.
Both are set to `zabbix`.

It is no longer possible to configure which user Zabbix agent runs as via `zabbix_agentd.conf` file,
because the agent will bypass this configuration and run as the user specified in the systemd service file.
To run Zabbix agent as root you need to make the modifications described below.

[comment]: # ({/324a64cd-6f1ad3f2})

[comment]: # ({86009682-2f6d2fe8})

### Zabbix agent

To override the default user and group for Zabbix agent, run:

    systemctl edit zabbix-agent

Then, add the following content:

    [Service]
    User=root
    Group=root

Reload daemons and restart the zabbix-agent service:

    systemctl daemon-reload
    systemctl restart zabbix-agent

For **Zabbix agent** this re-enables the functionality of configuring user in the `zabbix_agentd.conf` file.
Now you need to set `User=root` and `AllowRoot=1` configuration parameters in the agent [configuration file](/manual/appendix/config/zabbix_agentd).

[comment]: # ({/86009682-2f6d2fe8})

[comment]: # ({13ba87ac-f2ff86e2})

### Zabbix agent 2

To override the default user and group for Zabbix agent 2, run:

    systemctl edit zabbix-agent2

Then, add the following content:

    [Service]
    User=root
    Group=root

Reload daemons and restart the zabbix-agent service:

    systemctl daemon-reload
    systemctl restart zabbix-agent2

For **Zabbix agent2** this completely determines the user that it runs as.
No additional modifications are required.

[comment]: # ({/13ba87ac-f2ff86e2})
