[comment]: # ({ae007255-e09cf279})
# 11 SAML setup with Okta

This section provides guidelines for configuring [Okta](https://okta.com) to enable SAML 2.0
authentication and user provisioning for Zabbix.

[comment]: # ({/ae007255-e09cf279})

[comment]: # ({b543a071-c8df5f13})
#### Okta configuration

1\. Go to <https://developer.okta.com/signup/> and register/sign into your account.

2\. In the Okta web interface navigate to *Applications → Applications*.

3\. Click on *Create App Integration*.

![](../../../../assets/en/manual/appendix/install/okta_app_create.png){width="600"}

Select "SAML 2.0" as the sign-in method and click on *Next*.

4\. In general settings, fill in the app name and click on *Next*.

5\. In SAML configuration, enter the values provided below, then
click on *Next*.

![](../../../../assets/en/manual/appendix/install/okta_app_conf.png){width="600"}

-   In **General** add:
    -   *Single sign-on URL*:
        `http://<your-zabbix-url>/zabbix/index_sso.php?acs`\
        Note the use of "http", and not "https", so that the `acs` parameter is not cut out in the request. The *Use this for Recipient URL and Destination URL* checkbox should also be marked.
    -   *Audience URI (SP Entity ID)*: `zabbix`\
        Note that this value will be used within the SAML assertion as
        a unique service provider identifier (if not matching, the
        operation will be rejected). It is possible to specify a URL or
        any string of data in this field.
    -   *Default RelayState*:\
        Leave this field blank; if a custom redirect is required, it can
        be added in Zabbix in the *Users → Users* settings.
    -   Fill in other fields according to your preferences.

-   In **Attribute Statements/Group Attribute Statements** add:

![](../../../../assets/en/manual/appendix/install/okta_app_conf2.png){width="600"}

These attribute statements are inserted into the SAML assertions shared with Zabbix. 

The attribute names used here are arbitrary examples. You may use different attribute names, however, it is required that they match the respective field value in Zabbix SAML settings.

If you want to configure SAML sign-in into Zabbix *without* JIT user provisioning, then only the email attribute is required.

::: noteclassic
 If planning to use an encrypted connection, generate the private
and public encryption certificates, then upload the public certificate to
Okta. The certificate upload form appears when *Assertion Encryption* is 
set to "Encrypted" (click *Show Advanced Settings* to find this parameter).
:::

6\. In the next tab, select "I'm a software vendor. I'd like to
integrate my app with Okta" and press "Finish".

7\. Navigate to the "Assignments" tab of the newly-created application 
and click on the *Assign* button, then select "Assign to People" from the drop-down.

![](../../../../assets/en/manual/appendix/install/okta_app_assign.png)

8\. In a popup that appears, assign the app to people that will use
SAML 2.0 to authenticate with Zabbix, then click on *Save and go back*.

9\. Navigate to the "Sign On" tab and click on the *View Setup
Instructions* button. 

Setup **instructions** will be opened in a new tab; keep this tab open while configuring Zabbix.

[comment]: # ({/b543a071-c8df5f13})

[comment]: # ({f9846cb2-bdad4ea8})
#### Zabbix configuration

1\. In Zabbix, go to the [SAML settings](/manual/web_interface/frontend_sections/users/authentication/saml#setting-up-zabbix) 
and fill the configuration options based on setup instructions from Okta:

![](../../../../assets/en/manual/appendix/install/okta_zabbix_saml.png){width="600"}

|Zabbix field|Setup field in Okta|Sample value|
|----|----|--|
|*IdP entity ID*|Identity Provider Issuer||
|*SSO service URL*|Identity Provider Single Sign-On URL||
|*Username attribute*|Attribute name|`usrEmail`|
|*SP entity ID*|Audience URI|`zabbix`|
|*Group name attribute*|Attribute name|`groups`|
|*User name attribute*|Attribute name|`user_name`|
|*User last name attribute*|Attribute name|`user_lastname`|

It is also required to configure user group and media mapping.

2\. Download the certificate provided in the Okta SAML setup instructions
into *ui/conf/certs* folder as idp.crt. 

Set 644 permissions to it by running:

    chmod 644 idp.crt

3\. If *Assertion Encryption* has been set to "Encrypted" in Okta, the
"Assertions" checkbox of the *Encrypt* parameter should be marked in
Zabbix as well.

4\. Press the "Update" button to save these settings.

[comment]: # ({/f9846cb2-bdad4ea8})

[comment]: # ({f3dd407d-ff17ddca})

#### SCIM provisioning

1\. To turn on SCIM provisioning, go to "General" -> "App Settings" of the application in Okta. 

Mark the *Enable SCIM provisioning* checkbox. As a result, a new *Provisioning* tab appears.

2\. Go to the "Provisioning" tab to set up a SCIM connection:

-   In *SCIM connector base URL* specify the path to the Zabbix frontend and append `api_scim.php` to it, i.e.:\
    `https://<your-zabbix-url>/zabbix/api_scim.php`
-   *Unique identifier field for users*: `email`
-   *Authentication mode*: `HTTP header`
-   In *Authorization* enter a valid API token with Super admin rights

![](../../../../assets/en/manual/appendix/install/okta_scim_conn.png){width="600"}

::: noteimportant
If you are using Apache, you may need to change the default Apache configuration in `/etc/apache2/apache2.conf` by adding the following line:

    SetEnvIf Authorization "(.*)" HTTP_AUTHORIZATION=$1

Otherwise Apache does not send the Authorization header in request.
:::

3\. Click on *Test Connector Configuration* to test the connection. If all is correct a success message will be displayed.

4\. In "Provisioning" -> "To App", make sure to mark the following checkboxes:

-    Create Users
-    Update User Attributes
-    Deactivate Users

This will make sure that these request types will be sent to Zabbix.

5\. Make sure that all attributes defined in SAML are defined in SCIM. You can access the profile editor for your app in "Provisioning" -> "To App", by clicking on *Go to Profile Editor*.

Click on *Add Attribute*. Fill the values for *Display name*, *Variable name*, *External name* with the SAML attribute name, for example, `user_name`.

![](../../../../assets/en/manual/appendix/install/okta_add_attr.png)

*External namespace* should be the same as user schema: `urn:ietf:params:scim:schemas:core:2.0:User`

6\. Go to "Provisioning" -> "To App" -> "Attribute Mappings" of your application. Click on *Show Unmapped Attributes* at the bottom. Newly added attributes appear.

7\. Map each added attribute.

![](../../../../assets/en/manual/appendix/install/okta_map_attr.png)

8\. Add users in the "Assignments" tab. The users previously need to be added in *Directory* -> *People*. All these assignments will be sent as requests to Zabbix.

9\. Add groups in the "Push Groups" tab. The user group mapping pattern in Zabbix SAML settings must match a group specified here. If there is no match, the user cannot be created in Zabbix.

Information about group members is sent every time when some change is made.

[comment]: # ({/f3dd407d-ff17ddca})
