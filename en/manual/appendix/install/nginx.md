[comment]: # ({79670f64-840eea1a})
# 7 Distribution-specific notes on setting up Nginx for Zabbix

[comment]: # ({/79670f64-840eea1a})

[comment]: # ({0dd9de5d-4b966990})
#### RHEL

Nginx is available only in EPEL:

    dnf -y install epel-release

[comment]: # ({/0dd9de5d-4b966990})

[comment]: # ({b9b885ad-fb2c0b94})
#### SLES 12

In SUSE Linux Enterprise Server 12 you need to add the Nginx repository,
before installing Nginx:

    zypper addrepo -G -t yum -c 'http://nginx.org/packages/sles/12' nginx

You also need to configure `php-fpm` (the path to configuration file may vary slightly depending on the service pack):

    cp /etc/php7/fpm/php-fpm.conf{.default,}
    sed -i 's/user = nobody/user = wwwrun/; s/group = nobody/group = www/' /etc/php7/fpm/php-fpm.conf

[comment]: # ({/b9b885ad-fb2c0b94})

[comment]: # ({bd6448c8-31bd54eb})
#### SLES 15

In SUSE Linux Enterprise Server 15 you need to configure `php-fpm` (the path to configuration file may vary slightly depending on the service pack):

    cp /etc/php7/fpm/php-fpm.conf{.default,}
    cp /etc/php7/fpm/php-fpm.d/www.conf{.default,}
    sed -i 's/user = nobody/user = wwwrun/; s/group = nobody/group = www/' /etc/php7/fpm/php-fpm.d/www.conf

[comment]: # ({/bd6448c8-31bd54eb})
