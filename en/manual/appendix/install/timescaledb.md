[comment]: # ({98c082ce-98c082ce})
# 5 TimescaleDB setup

[comment]: # ({/98c082ce-98c082ce})

[comment]: # ({acb21280-acb21280})
#### Overview

Zabbix supports TimescaleDB, a PostgreSQL-based database solution of
automatically partitioning data into time-based chunks to support faster
performance at scale.

::: notewarning
Currently TimescaleDB is not supported by Zabbix
proxy.
:::

Instructions on this page can be used for creating TimescaleDB database
or migrating from existing PostgreSQL tables to TimescaleDB.

[comment]: # ({/acb21280-acb21280})

[comment]: # ({4c218e87-f731725c})
#### Configuration

We assume that TimescaleDB extension has been already installed on the
database server (see [installation
instructions](https://docs.timescale.com/latest/getting-started/installation)).

TimescaleDB extension must also be enabled for the specific DB by
executing:

    echo "CREATE EXTENSION IF NOT EXISTS timescaledb CASCADE;" | sudo -u postgres psql zabbix

Running this command requires database administrator privileges.

::: noteclassic
If you use a database schema other than 'public' you need to
add a SCHEMA clause to the command above. E.g.:\
`echo "CREATE EXTENSION IF NOT EXISTS timescaledb SCHEMA yourschema CASCADE;" | sudo -u postgres psql zabbix`
:::

Then run the `timescaledb.sql` script located in database/postgresql.
For new installations the script must be run after the regular
PostgreSQL database has been created with initial schema/data (see
[database creation](/manual/appendix/install/db_scripts)):

    cat /usr/share/zabbix-sql-scripts/postgresql/timescaledb.sql | sudo -u zabbix psql zabbix

::: noteimportant
Please ignore warning messages informing that the best practices are not followed
while running `timescaledb.sql` script on TimescaleDB version 2.9.0 and higher.
Regardless of this warning, the configuration will be completed successfully.
:::

The migration of existing history and trend data may take a lot of time.
Zabbix server and frontend must be down for the period of migration.

The `timescaledb.sql` script sets the following housekeeping parameters:

-   Override item history period
-   Override item trend period

In order to use partitioned housekeeping for history and trends, both
these options must be enabled. It is also possible to enable override individually
either for history only or trends only.

For PostgreSQL version 10.2 or higher and TimescaleDB version 1.5 or
higher, the `timescaledb.sql` script sets two additional parameters:

-   Enable compression
-   Compress records older than 7 days

[comment]: # ({/4c218e87-f731725c})

[comment]: # ({63d49c5c-60e46db2})

Compression can be used only if both *Override item history period*
and *Override item trend period* options are enabled.
If override is disabled and tables have compressed chunks,
the housekeeper will not remove data from these tables, and warnings about incorrect configuration will be displayed
in the [*Housekeeping*](/manual/web_interface/frontend_sections/administration/housekeeping)
and [*System information*](/manual/web_interface/frontend_sections/reports/status_of_zabbix) sections.

[comment]: # ({/63d49c5c-60e46db2})

[comment]: # ({ba3c7b1d-8c3a80a9})

All of these parameters can be changed in *Administration* →
[*Housekeeping*](/manual/web_interface/frontend_sections/administration/housekeeping) after the installation.

::: notetip
You may want to run the timescaledb-tune tool provided
by TimescaleDB to optimize PostgreSQL configuration parameters in your
`postgresql.conf`.
:::

[comment]: # ({/ba3c7b1d-8c3a80a9})

[comment]: # ({b889a598-2012f0a6})
#### TimescaleDB compression

Native TimescaleDB compression is supported starting from Zabbix 5.0 for
PostgreSQL version 10.2 or higher and TimescaleDB version 1.5 or higher
for all Zabbix tables that are managed by TimescaleDB. During the
upgrade or migration to TimescaleDB, initial compression of the large
tables may take a lot of time.

Note that compression is supported under the "timescale" Timescale Community license and 
it is not supported under "apache" Apache 2.0 license. If Zabbix detects that compression is 
not supported a warning message is written into the Zabbix server log and users cannot 
enable compression in the frontend.

::: notetip
Users are encouraged to get familiar with
[TimescaleDB](https://docs.timescale.com/latest/using-timescaledb/compression)
compression documentation before using compression.
:::

Note, that there are certain limitations imposed by compression,
specifically:

-   Compressed chunk modifications (inserts, deletes, updates) are not
    allowed
-   Schema changes for compressed tables are not allowed.

Compression settings can be changed in the *History and trends
compression* block in *Administration* → *Housekeeping*
section of Zabbix frontend.

|Parameter|Default|Comments|
|--|--|------|
|*Enable compression*|Enabled|Checking or unchecking the checkbox does not activate/deactivate compression immediately. Because compression is handled by the Housekeeper, the changes will take effect in up to 2 times `HousekeepingFrequency` hours (set in [zabbix\_server.conf](/manual/appendix/config/zabbix_server))<br><br>After disabling compression, new chunks that fall into the compression period will not be compressed. However, all previously compressed data will stay compressed. To uncompress previously compressed chunks, follow instructions in [TimescaleDB](https://docs.timescale.com/latest/using-timescaledb/compression) documentation.<br><br>When upgrading from older versions of Zabbix with TimescaleDB support, compression will not be enabled by default.|
|*Compress records older than*|7d|This parameter cannot be less than 7 days.<br><br>Due to immutability of compressed chunks all late data (e.g. data delayed by a proxy) that is older than this value will be discarded.|

[comment]: # ({/b889a598-2012f0a6})
