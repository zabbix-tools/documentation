[comment]: # ({e26480a0-e26480a0})
# 3 Database upgrade to primary keys

[comment]: # ({/e26480a0-e26480a0})

[comment]: # ({da817d61-53564572})
### Overview

Since Zabbix 6.0, primary keys are used for all tables in 
new installations.

This section provides instructions for manually upgrading the
history tables in existing installations to primary keys.

Instructions are available for:

* [MySQL](#mysql)
* [PostgreSQL](#postgresql)
* [TimescaleDB](#postgresql-timescaledb)
* [Oracle](#oracle)

[comment]: # ({/da817d61-53564572})

[comment]: # ({99edd6cb-3c15c985})

::: noteimportant
The instructions provided on this page are designed for advanced users.
Note that these instructions might need to be adjusted for your specific configuration.
:::

[comment]: # ({/99edd6cb-3c15c985})

[comment]: # ({62ef1e0d-980f7329})

### Important notes

- Make sure to back up the database before the upgrade.
- If the database uses partitions, contact the DB administrator or Zabbix support team for help.
- Stopping Zabbix server for the time of the upgrade is strongly recommended. However, if absolutely necessary, there is a way to perform an upgrade while the server is running (only for MySQL, MariaDB and PostgreSQL without TimescaleDB).
- CSV files can be removed after a successful upgrade to primary keys.
- Optionally, Zabbix frontend may be switched to [maintenance mode](/manual/web_interface/maintenance_mode).
- Upgrade to primary keys should be done after upgrading Zabbix server to 6.0.
- On proxy, history tables that are not used can be upgraded by executing history_pk_prepare.sql.

[comment]: # ({/62ef1e0d-980f7329})

[comment]: # ({4f99bcf1-1551eb55})
### MySQL

Export and import must be performed in tmux/screen to ensure that the session isn't dropped.

See also: [Important notes](#important-notes)

[comment]: # ({/4f99bcf1-1551eb55})

[comment]: # ({efb698b7-dc89d749})

#### MySQL 8.0+ with mysqlsh

This method can be used with a running Zabbix server, but it is recommended to stop the server for the time of the upgrade.
The MySQL Shell (*mysqlsh*) must be [installed](https://dev.mysql.com/doc/mysql-shell/8.0/en/mysql-shell-install-linux-quick.html) and able to connect to the DB. 

* Log in to MySQL console as root (recommended) or as any user with FILE privileges. 

* Start MySQL with [local_infile](https://dev.mysql.com/doc/refman/8.0/en/server-system-variables.html#sysvar_local_infile) variable enabled.

* Rename old tables and create new tables by running `history_pk_prepare.sql`.

```
mysql -uzabbix -p<password> zabbix < /usr/share/zabbix-sql-scripts/mysql/history_pk_prepare.sql
```

* Export and import data.

Connect via mysqlsh. If using a socket connection, specifying the path might be required.

    sudo mysqlsh -uroot -S /run/mysqld/mysqld.sock --no-password -Dzabbix

Run (CSVPATH can be changed as needed):

``` {.javascript}
CSVPATH="/var/lib/mysql-files";

util.exportTable("history_old", CSVPATH + "/history.csv", { dialect: "csv" });
util.importTable(CSVPATH + "/history.csv", {"dialect": "csv", "table": "history" });

util.exportTable("history_uint_old", CSVPATH + "/history_uint.csv", { dialect: "csv" });
util.importTable(CSVPATH + "/history_uint.csv", {"dialect": "csv", "table": "history_uint" });

util.exportTable("history_str_old", CSVPATH + "/history_str.csv", { dialect: "csv" });
util.importTable(CSVPATH + "/history_str.csv", {"dialect": "csv", "table": "history_str" });

util.exportTable("history_log_old", CSVPATH + "/history_log.csv", { dialect: "csv" });
util.importTable(CSVPATH + "/history_log.csv", {"dialect": "csv", "table": "history_log" });

util.exportTable("history_text_old", CSVPATH + "/history_text.csv", { dialect: "csv" });
util.importTable(CSVPATH + "/history_text.csv", {"dialect": "csv", "table": "history_text" });
```

* Follow [post-migration instructions](#post-migration) to drop the old tables.

[comment]: # ({/efb698b7-dc89d749})

[comment]: # ({3bb034ed-b132f69b})

#### MariaDB/MySQL 8.0+ without mysqlsh

This upgrade method takes more time and should be used only if an upgrade with *mysqlsh* is not possible.

##### Table upgrade

* Log in to MySQL console as root (recommended) or any user with FILE privileges. 

* Start MySQL with [local_infile](https://dev.mysql.com/doc/refman/8.0/en/server-system-variables.html#sysvar_local_infile) variable enabled.

* Rename old tables and create new tables by running `history_pk_prepare.sql`:

``` {.bash}
mysql -uzabbix -p<password> zabbix < /usr/share/zabbix-sql-scripts/mysql/history_pk_prepare.sql
```

##### Migration with stopped server

*max_execution_time* must be disabled before migrating data to avoid timeout during migration.

``` {.sql}
SET @@max_execution_time=0;

INSERT IGNORE INTO history SELECT * FROM history_old;
INSERT IGNORE INTO history_uint SELECT * FROM history_uint_old;
INSERT IGNORE INTO history_str SELECT * FROM history_str_old;
INSERT IGNORE INTO history_log SELECT * FROM history_log_old;
INSERT IGNORE INTO history_text SELECT * FROM history_text_old;
```

Follow [post-migration instructions](#post-migration) to drop the old tables.

##### Migration with running server

Check for which paths import/export is enabled:

``` {.sql}
mysql> SELECT @@secure_file_priv;
+-----------------------+
| @@secure_file_priv    |
+-----------------------+
| /var/lib/mysql-files/ |
+-----------------------+
```

If *secure_file_priv* value is a path to a directory, export/import will be performed for 
files in that directory. In this case, edit paths to files in queries accordingly or set the *secure_file_priv* value to an empty string for the upgrade time. 

If *secure_file_priv* value is empty, export/import 
can be performed from any location.  

If *secure_file_priv* value is NULL, set it to the path that contains exported table data ('/var/lib/mysql-files/' in the example above). 

For more information, see 
[MySQL documentation](https://dev.mysql.com/doc/refman/5.7/en/server-system-variables.html#sysvar_secure_file_priv). 

*max_execution_time* must be disabled before exporting data to avoid timeout during export.

``` {.sql}
SET @@max_execution_time=0;

SELECT * INTO OUTFILE '/var/lib/mysql-files/history.csv' FIELDS TERMINATED BY ',' ESCAPED BY '"' LINES TERMINATED BY '\n' FROM history_old;
LOAD DATA INFILE '/var/lib/mysql-files/history.csv' IGNORE INTO TABLE history FIELDS TERMINATED BY ',' ESCAPED BY '"' LINES TERMINATED BY '\n';

SELECT * INTO OUTFILE '/var/lib/mysql-files/history_uint.csv' FIELDS TERMINATED BY ',' ESCAPED BY '"' LINES TERMINATED BY '\n' FROM history_uint_old;
LOAD DATA INFILE '/var/lib/mysql-files/history_uint.csv' IGNORE INTO TABLE history_uint FIELDS TERMINATED BY ',' ESCAPED BY '"' LINES TERMINATED BY '\n';

SELECT * INTO OUTFILE '/var/lib/mysql-files/history_str.csv' FIELDS TERMINATED BY ',' ESCAPED BY '"' LINES TERMINATED BY '\n' FROM history_str_old;
LOAD DATA INFILE '/var/lib/mysql-files/history_str.csv' IGNORE INTO TABLE history_str FIELDS TERMINATED BY ',' ESCAPED BY '"' LINES TERMINATED BY '\n';

SELECT * INTO OUTFILE '/var/lib/mysql-files/history_log.csv' FIELDS TERMINATED BY ',' ESCAPED BY '"' LINES TERMINATED BY '\n' FROM history_log_old;
LOAD DATA INFILE '/var/lib/mysql-files/history_log.csv' IGNORE INTO TABLE history_log FIELDS TERMINATED BY ',' ESCAPED BY '"' LINES TERMINATED BY '\n';

SELECT * INTO OUTFILE '/var/lib/mysql-files/history_text.csv' FIELDS TERMINATED BY ',' ESCAPED BY '"' LINES TERMINATED BY '\n' FROM history_text_old;
LOAD DATA INFILE '/var/lib/mysql-files/history_text.csv' IGNORE INTO TABLE history_text FIELDS TERMINATED BY ',' ESCAPED BY '"' LINES TERMINATED BY '\n';
```

Follow [post-migration instructions](#post-migration) to drop the old tables.

[comment]: # ({/3bb034ed-b132f69b})

[comment]: # ({cf73a939-1139207b})
### PostgreSQL

Export and import must be performed in tmux/screen to ensure that the session isn't dropped.
For installations with TimescaleDB, skip this section and proceed to [PostgreSQL + TimescaleDB](#postgresql-timescaledb).

See also: [Important notes](#important-notes)

#### Table upgrade

* Rename tables using `history_pk_prepare.sql`:

``` {.bash}
sudo -u zabbix psql zabbix < /usr/share/zabbix-sql-scripts/postgresql/history_pk_prepare.sql
```

#### Migration with stopped server

* Export current history, import it to the temp table, 
then insert the data into new tables while ignoring duplicates:

``` {.sql}
INSERT INTO history SELECT * FROM history_old ON CONFLICT (itemid,clock,ns) DO NOTHING;

INSERT INTO history_uint SELECT * FROM history_uint_old ON CONFLICT (itemid,clock,ns) DO NOTHING;

INSERT INTO history_str SELECT * FROM history_str_old ON CONFLICT (itemid,clock,ns) DO NOTHING;

INSERT INTO history_log SELECT * FROM history_log_old ON CONFLICT (itemid,clock,ns) DO NOTHING;

INSERT INTO history_text SELECT * FROM history_text_old ON CONFLICT (itemid,clock,ns) DO NOTHING;
```

See tips for improving INSERT performance: [PostgreSQL: Bulk Loading Huge Amounts of Data](https://www.cybertec-postgresql.com/en/postgresql-bulk-loading-huge-amounts-of-data), [Checkpoint Distance and Amount of WAL](https://www.cybertec-postgresql.com/en/checkpoint-distance-and-amount-of-wal).

* Follow [post-migration instructions](#post-migration) to drop the old tables.

[comment]: # ({/cf73a939-1139207b})

[comment]: # ({0f156e4c-7e590ff3})

#### Migration with running server

* Export current history, import it to the temp table, 
then insert the data into new tables while ignoring duplicates:

``` {.sql}
\copy history_old TO '/tmp/history.csv' DELIMITER ',' CSV
CREATE TEMP TABLE temp_history (
    itemid                   bigint                                    NOT NULL,
    clock                    integer         DEFAULT '0'               NOT NULL,
    value                    DOUBLE PRECISION DEFAULT '0.0000'          NOT NULL,
    ns                       integer         DEFAULT '0'               NOT NULL
);
\copy temp_history FROM '/tmp/history.csv' DELIMITER ',' CSV
INSERT INTO history SELECT * FROM temp_history ON CONFLICT (itemid,clock,ns) DO NOTHING;

\copy history_uint_old TO '/tmp/history_uint.csv' DELIMITER ',' CSV
CREATE TEMP TABLE temp_history_uint (
    itemid                   bigint                                    NOT NULL,
    clock                    integer         DEFAULT '0'               NOT NULL,
    value                    numeric(20)     DEFAULT '0'               NOT NULL,
    ns                       integer         DEFAULT '0'               NOT NULL
);
\copy temp_history_uint FROM '/tmp/history_uint.csv' DELIMITER ',' CSV
INSERT INTO history_uint SELECT * FROM temp_history_uint ON CONFLICT (itemid,clock,ns) DO NOTHING;

\copy history_str_old TO '/tmp/history_str.csv' DELIMITER ',' CSV
CREATE TEMP TABLE temp_history_str (
    itemid                   bigint                                    NOT NULL,
    clock                    integer         DEFAULT '0'               NOT NULL,
    value                    varchar(255)    DEFAULT ''                NOT NULL,
    ns                       integer         DEFAULT '0'               NOT NULL
);
\copy temp_history_str FROM '/tmp/history_str.csv' DELIMITER ',' CSV
INSERT INTO history_str (itemid,clock,value,ns) SELECT * FROM temp_history_str ON CONFLICT (itemid,clock,ns) DO NOTHING;

\copy history_log_old TO '/tmp/history_log.csv' DELIMITER ',' CSV
CREATE TEMP TABLE temp_history_log (
    itemid                   bigint                                    NOT NULL,
    clock                    integer         DEFAULT '0'               NOT NULL,
    timestamp                integer         DEFAULT '0'               NOT NULL,
    source                   varchar(64)     DEFAULT ''                NOT NULL,
    severity                 integer         DEFAULT '0'               NOT NULL,
    value                    text            DEFAULT ''                NOT NULL,
    logeventid               integer         DEFAULT '0'               NOT NULL,
    ns                       integer         DEFAULT '0'               NOT NULL
);
\copy temp_history_log FROM '/tmp/history_log.csv' DELIMITER ',' CSV
INSERT INTO history_log SELECT * FROM temp_history_log ON CONFLICT (itemid,clock,ns) DO NOTHING;

\copy history_text_old TO '/tmp/history_text.csv' DELIMITER ',' CSV
CREATE TEMP TABLE temp_history_text (
    itemid                   bigint                                    NOT NULL,
    clock                    integer         DEFAULT '0'               NOT NULL,
    value                    text            DEFAULT ''                NOT NULL,
    ns                       integer         DEFAULT '0'               NOT NULL
);
\copy temp_history_text FROM '/tmp/history_text.csv' DELIMITER ',' CSV
INSERT INTO history_text SELECT * FROM temp_history_text ON CONFLICT (itemid,clock,ns) DO NOTHING;
```
* Follow [post-migration instructions](#post-migration) to drop the old tables.

[comment]: # ({/0f156e4c-7e590ff3})

[comment]: # ({8c7756f1-589d2f09})

### PostgreSQL + TimescaleDB

Export and import must be performed in tmux/screen to ensure that the session isn't dropped.
Zabbix server should be down during the upgrade.

See also: [Important notes](#important-notes)

* Rename tables using `history_pk_prepare.sql`.

``` {.bash}
sudo -u zabbix psql zabbix < /usr/share/zabbix-sql-scripts/postgresql/history_pk_prepare.sql
```
* Run TimescaleDB hypertable migration scripts (compatible with both TSDB v2.x and v1.x version) based on compression settings:
  * If compression is enabled (on default installation), run scripts from `database/postgresql/tsdb_history_pk_upgrade_with_compression`:
    ``` {.bash}
    cat /usr/share/zabbix-sql-scripts/postgresql/tsdb_history_pk_upgrade_with_compression/history_pk.sql | sudo -u zabbix psql zabbix
    cat /usr/share/zabbix-sql-scripts/postgresql/tsdb_history_pk_upgrade_with_compression/history_pk_uint.sql | sudo -u zabbix psql zabbix
    cat /usr/share/zabbix-sql-scripts/postgresql/tsdb_history_pk_upgrade_with_compression/history_pk_log.sql | sudo -u zabbix psql zabbix
    cat /usr/share/zabbix-sql-scripts/postgresql/tsdb_history_pk_upgrade_with_compression/history_pk_str.sql | sudo -u zabbix psql zabbix
    cat /usr/share/zabbix-sql-scripts/postgresql/tsdb_history_pk_upgrade_with_compression/history_pk_text.sql | sudo -u zabbix psql zabbix
    ```
  * If compression is disabled, run scripts from `database/postgresql/tsdb_history_pk_upgrade_no_compression`:
    ``` {.bash}
    cat /usr/share/zabbix-sql-scripts/postgresql/tsdb_history_pk_upgrade_no_compression/history_pk.sql | sudo -u zabbix psql zabbix
    cat /usr/share/zabbix-sql-scripts/postgresql/tsdb_history_pk_upgrade_no_compression/history_pk_uint.sql | sudo -u zabbix psql zabbix
    cat /usr/share/zabbix-sql-scripts/postgresql/tsdb_history_pk_upgrade_no_compression/history_pk_log.sql | sudo -u zabbix psql zabbix
    cat /usr/share/zabbix-sql-scripts/postgresql/tsdb_history_pk_upgrade_no_compression/history_pk_str.sql | sudo -u zabbix psql zabbix
    cat /usr/share/zabbix-sql-scripts/postgresql/tsdb_history_pk_upgrade_no_compression/history_pk_text.sql | sudo -u zabbix psql zabbix
    ```

See also: [Tips](https://blog.timescale.com/blog/13-tips-to-improve-postgresql-insert-performance/) for improving INSERT performance.

* Follow [post-migration instructions](#post-migration) to drop the old tables.

[comment]: # ({/8c7756f1-589d2f09})

[comment]: # ({07c27add-4b428c7c})

### Oracle

Export and import must be performed in tmux/screen to ensure that the session isn't dropped.
Zabbix server should be down during the upgrade.

See also: [Important notes](#important-notes)

#### Table upgrade

* Install Oracle Data Pump (available in the [Instant Client Tools package](https://www.oracle.com/database/technologies/instant-client/linux-x86-64-downloads.html)). 

See Oracle Data Pump [documentation](https://docs.oracle.com/en/database/oracle/oracle-database/19/sutil/oracle-data-pump-performance-tips.html) for performance tips.

* Rename tables using `history_pk_prepare.sql`.

``` {.bash}
cd /usr/share/zabbix/zabbix-sql-scripts/database/oracle
sqlplus zabbix/password@oracle_host/service
sqlplus> @history_pk_prepare.sql
```

[comment]: # ({/07c27add-4b428c7c})

[comment]: # ({59c3de85-0dd33561})

#### Batch migration of history tables

* Prepare directories for Data Pump.

Data Pump must have read and write permissions to these directories.

Example:

``` {.bash}
mkdir -pv /export/history
chown -R oracle:oracle /export
```

* Create a directory object and grant read and write permissions to this object to the user used for Zabbix authentication ('zabbix' in the example below). Under *sysdba* role, run:

``` {.sql}
create directory history as '/export/history';
grant read,write on directory history to zabbix;
```

* Export tables. Replace N with the desired thread count.

``` {.bash}
expdp zabbix/password@oracle_host/service \
    DIRECTORY=history \
    TABLES=history_old,history_uint_old,history_str_old,history_log_old,history_text_old \
    PARALLEL=N
```

* Import tables. Replace N with the desired thread count.

``` {.bash}
impdp zabbix/password@oracle_host/service \
    DIRECTORY=history \
    TABLES=history_uint_old \
 REMAP_TABLE=history_old:history,history_uint_old:history_uint,history_str_old:history_str,history_log_old:history_log,history_text_old:history_text \
    data_options=SKIP_CONSTRAINT_ERRORS table_exists_action=APPEND  PARALLEL=N CONTENT=data_only
```

* Follow [post-migration instructions](#post-migration) to drop the old tables.

[comment]: # ({/59c3de85-0dd33561})

[comment]: # ({91b4fc19-f0426f06})
#### Individual migration of history tables

* Prepare directories for Data Pump for each history table.
Data Pump must have read and write permissions to these directories.

Example:

``` {.bash}
mkdir -pv /export/history /export/history_uint /export/history_str /export/history_log /export/history_text
chown -R oracle:oracle /export
```

* Create a directory object and grant read and write permissions to this object to the user used for Zabbix authentication ('zabbix' in the example below). Under *sysdba* role, run:

``` {.sql}
create directory history as '/export/history';
grant read,write on directory history to zabbix;

create directory history_uint as '/export/history_uint';
grant read,write on directory history_uint to zabbix;

create directory history_str as '/export/history_str';
grant read,write on directory history_str to zabbix;

create directory history_log as '/export/history_log';
grant read,write on directory history_log to zabbix;

create directory history_text as '/export/history_text';
grant read,write on directory history_text to zabbix;
```

* Export and import each table. Replace N with the desired thread count.

``` {.bash}
expdp zabbix/password@oracle_host:1521/xe DIRECTORY=history TABLES=history_old PARALLEL=N

impdp zabbix/password@oracle_host:1521/xe DIRECTORY=history TABLES=history_old REMAP_TABLE=history_old:history data_options=SKIP_CONSTRAINT_ERRORS table_exists_action=APPEND PARALLEL=N CONTENT=data_only

expdp zabbix/password@oracle_host:1521/xe DIRECTORY=history_uint TABLES=history_uint_old PARALLEL=N

impdp zabbix/password@oracle_host:1521/xe DIRECTORY=history_uint TABLES=history_uint_old REMAP_TABLE=history_uint_old:history_uint data_options=SKIP_CONSTRAINT_ERRORS table_exists_action=APPEND PARALLEL=N CONTENT=data_only

expdp zabbix/password@oracle_host:1521/xe DIRECTORY=history_str TABLES=history_str_old PARALLEL=N

impdp zabbix/password@oracle_host:1521/xe DIRECTORY=history_str TABLES=history_str_old REMAP_TABLE=history_str_old:history_str data_options=SKIP_CONSTRAINT_ERRORS table_exists_action=APPEND PARALLEL=N CONTENT=data_only

expdp zabbix/password@oracle_host:1521/xe DIRECTORY=history_log TABLES=history_log_old PARALLEL=N

impdp zabbix/password@oracle_host:1521/xe DIRECTORY=history_log TABLES=history_log_old REMAP_TABLE=history_log_old:history_log data_options=SKIP_CONSTRAINT_ERRORS table_exists_action=APPEND PARALLEL=N CONTENT=data_only

expdp zabbix/password@oracle_host:1521/xe DIRECTORY=history_text TABLES=history_text_old PARALLEL=N

impdp zabbix/password@oracle_host:1521/xe DIRECTORY=history_text TABLES=history_text_old REMAP_TABLE=history_text_old:history_text data_options=SKIP_CONSTRAINT_ERRORS table_exists_action=APPEND PARALLEL=N CONTENT=data_only
```

* Follow [post-migration instructions](#post-migration) to drop the old tables.

[comment]: # ({/91b4fc19-f0426f06})

[comment]: # ({2c44ab4c-b041e427})

### Post-migration

For all databases, once the migration is completed, do the following:

* Verify that everything works as expected.

* Drop old tables:

``` {.sql}
DROP TABLE history_old;
DROP TABLE history_uint_old;
DROP TABLE history_str_old;
DROP TABLE history_log_old;
DROP TABLE history_text_old;
```

[comment]: # ({/2c44ab4c-b041e427})
