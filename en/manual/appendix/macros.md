[comment]: # attributes: notoc

[comment]: # ({835c1fd7-d28448e0})
# 7 Macros

It is possible to use out-of-the-box [Supported macros](/manual/appendix/macros/supported_by_location)
and [User macros supported by location](/manual/appendix/macros/supported_by_location_user).

[comment]: # ({/835c1fd7-d28448e0})
