[comment]: # attributes: notoc

[comment]: # (terms: ascii, bitlength, bytelength, char, concat, insert, left, length, ltrim, mid, repeat, replace, right, rtrim, trim )

[comment]: # ({35d3261a-b12f316b})

# 9 String functions

All functions listed here are supported in:

-   [Trigger expressions](/manual/config/triggers/expression)
-   [Calculated items](/manual/config/items/itemtypes/calculated)

The functions are listed without additional information. Click on the function to see the full details.

|Function|Description|
|--|--------|
|[ascii](#ascii)|The ASCII code of the leftmost character of the value.|
|[bitlength](#bitlength)|The length of value in bits.|
|[bytelength](#bytelength)|The length of value in bytes.|
|[char](#char)|Return the character by interpreting the value as ASCII code.|
|[concat](#concat)|The string resulting from concatenating the referenced item values or constant values.|
|[insert](#insert)|Insert specified characters or spaces into the character string beginning at the specified position in the string.|
|[left](#left)|Return the leftmost characters of the value.|
|[length](#length)|The length of value in characters.|
|[ltrim](#ltrim)|Remove specified characters from the beginning of string.|
|[mid](#mid)|Return a substring of N characters beginning at the character position specified by 'start'.|
|[repeat](#repeat)|Repeat a string.|
|[replace](#replace)|Find the pattern in the value and replace with replacement.|
|[right](#right)|Return the rightmost characters of the value.|
|[rtrim](#rtrim)|Remove specified characters from the end of string.|
|[trim](#trim)|Remove specified characters from the beginning and end of string.|

[comment]: # ({/35d3261a-b12f316b})

[comment]: # ({3df12990-e91f4bca})
### Function details

Some general notes on function parameters:

-   Function parameters are separated by a comma
-   Expressions are accepted as parameters
-   String parameters must be double-quoted; otherwise they might get
    misinterpreted
-   Optional function parameters (or parameter parts) are indicated by
    `<` `>`

[comment]: # ({/3df12990-e91f4bca})

[comment]: # ({6ce6045e-6bdf63a4})

##### ascii(value) {#ascii}

The ASCII code of the leftmost character of the value.<br>
Supported value types: *String*, *Text*, *Log*.

Parameter: 

-   **value** - the value to check

For example, a value like 'Abc' will return '65' (ASCII code for 'A').

Example:

    ascii(last(/host/key))

[comment]: # ({/6ce6045e-6bdf63a4})

[comment]: # ({1ab24b6c-30f96a52})

##### bitlength(value) {#bitlength}

The length of value in bits.<br>
Supported value types: *String*, *Text*, *Log*, *Integer*.

Parameter: 

-   **value** - the value to check

Example:

    bitlength(last(/host/key))

[comment]: # ({/1ab24b6c-30f96a52})

[comment]: # ({8c21016a-d58806ed})

##### bytelength(value) {#bytelength}

The length of value in bytes.<br>
Supported value types: *String*, *Text*, *Log*, *Integer*.

Parameter: 

-   **value** - the value to check

Example:

    bytelength(last(/host/key))

[comment]: # ({/8c21016a-d58806ed})

[comment]: # ({62456378-e35a326a})

##### char(value) {#char}

Return the character by interpreting the value as ASCII code.<br>
Supported value types: *Integer*.

Parameter: 

-   **value** - the value to check

The value must be in the 0-255 range. For example, a value like '65' (interpreted as ASCII code) will return 'A'.

Example:

    char(last(/host/key))

[comment]: # ({/62456378-e35a326a})

[comment]: # ({e3481e84-5e2f76e5})

##### concat(<value1>,<value2>,...) {#concat}

The string resulting from concatenating the referenced item values or constant values.<br>
Supported value types: *String*, *Text*, *Log*, *Float*, *Integer*.

Parameter: 

-   **valueX** - the value returned by one of the history functions or a constant value (string, integer, or float number). Must contain at least two parameters.

For example, a value like 'Zab' concatenated to 'bix' (the constant string) will return 'Zabbix'.

Examples:

    concat(last(/host/key),"bix")
    concat("1 min: ",last(/host/system.cpu.load[all,avg1]),", 15 min: ",last(/host/system.cpu.load[all,avg15]))

[comment]: # ({/e3481e84-5e2f76e5})

[comment]: # ({b4017055-06698de3})

##### insert(value,start,length,replacement) {#insert}

Insert specified characters or spaces into the character string beginning at the specified position in the string.<br>
Supported value types: *String*, *Text*, *Log*.

Parameter: 

-   **value** - the value to check;<br>
-   **start** - start position;<br>
-   **length** - positions to replace;<br>
-   **replacement** - replacement string.

For example, a value like 'Zabbbix' will be replaced by 'Zabbix' if 'bb' (starting position 3, positions to replace 2) is replaced by 'b'.

Example:

    insert(last(/host/key),3,2,"b")

[comment]: # ({/b4017055-06698de3})

[comment]: # ({7a2ce9be-16f928ed})

##### left(value,count) {#left}

Return the leftmost characters of the value.<br>
Supported value types: *String*, *Text*, *Log*.

Parameter: 

-   **value** - the value to check;<br>
-   **count** - the number of characters to return.

For example, you may return 'Zab' from 'Zabbix' by specifying 3 leftmost characters to return. See also [right()](#right).

Example:

    left(last(/host/key),3) #return three leftmost characters

[comment]: # ({/7a2ce9be-16f928ed})

[comment]: # ({b64efd51-b8f86065})

##### length(value) {#length}

The length of value in characters.<br>
Supported value types: *String*, *Text*, *Log*.

Parameter: 

-   **value** - the value to check.

Examples:

    length(last(/host/key)) #the length of the latest value
    length(last(/host/key,#3)) #the length of the third most recent value
    length(last(/host/key,#1:now-1d)) #the length of the most recent value one day ago

[comment]: # ({/b64efd51-b8f86065})

[comment]: # ({7ce6b018-01a5023c})

##### ltrim(value,<chars>) {#ltrim}

Remove specified characters from the beginning of string.<br>
Supported value types: *String*, *Text*, *Log*.

Parameter: 

-   **value** - the value to check;<br>
-   **chars** (optional) - specify the characters to remove.

Whitespace is left-trimmed by default (if no optional characters are specified). See also: [rtrim()](#rtrim), [trim()](#trim).

Examples:

    ltrim(last(/host/key)) #remove whitespace from the beginning of string
    ltrim(last(/host/key),"Z") #remove any 'Z' from the beginning of string
    ltrim(last(/host/key)," Z") #remove any space and 'Z' from the beginning of string

[comment]: # ({/7ce6b018-01a5023c})

[comment]: # ({a4997bcd-de3605ed})

##### mid(value,start,length) {#mid}

Return a substring of N characters beginning at the character position specified by 'start'.<br>
Supported value types: *String*, *Text*, *Log*.

Parameter: 

-   **value** - the value to check;<br>
-   **start** - start position of the substring;<br>
-   **length** - positions to return in substring.

For example, it is possible return 'abbi' from a value like 'Zabbix' if starting position is 2, and positions to return is 4.

Example:

    mid(last(/host/key),2,4)="abbi"

[comment]: # ({/a4997bcd-de3605ed})

[comment]: # ({3bc12e24-9e3e42f7})

##### repeat(value,count) {#repeat}

Repeat a string.<br>
Supported value types: *String*, *Text*, *Log*.

Parameter: 

-   **value** - the value to check;<br>
-   **count** - the number of times to repeat.

Example:

    repeat(last(/host/key),2) #repeat the value two times

[comment]: # ({/3bc12e24-9e3e42f7})

[comment]: # ({2acee182-6aa607aa})

##### replace(value,pattern,replacement) {#replace}

Find the pattern in the value and replace with replacement. All occurrences of the pattern will be replaced.<br>
Supported value types: *String*, *Text*, *Log*.

Parameter: 

-   **value** - the value to check;<br>
-   **pattern** - the pattern to find;<br>
-   **replacement** - the string to replace the pattern with.

Example:

    replace(last(/host/key),"ibb","abb") - replace all 'ibb' with 'abb'

[comment]: # ({/2acee182-6aa607aa})

[comment]: # ({c18d71fc-ccefcd9e})

##### right(value,count) {#right}

Return the rightmost characters of the value.<br>
Supported value types: *String*, *Text*, *Log*.

Parameter: 

-   **value** - the value to check;<br>
-   **count** - the number of characters to return.

For example, you may return 'bix' from 'Zabbix' by specifying 3 rightmost characters to return. See also [left()](#left).

Example:

    right(last(/host/key),3) #return three rightmost characters

[comment]: # ({/c18d71fc-ccefcd9e})

[comment]: # ({50453b68-6e9972ef})

##### rtrim(value,<chars>) {#rtrim}

Remove specified characters from the end of string.<br>
Supported value types: *String*, *Text*, *Log*.

Parameter: 

-   **value** - the value to check;<br>
-   **chars** (optional) - specify the characters to remove.

Whitespace is right-trimmed by default (if no optional characters are specified). See also: [ltrim()](#ltrim), [trim()](#trim).

Examples:

    rtrim(last(/host/key)) #remove whitespace from the end of string
    rtrim(last(/host/key),"x") #remove any 'x' from the end of string
    rtrim(last(/host/key),"x ") #remove any 'x' and space from the end of string

[comment]: # ({/50453b68-6e9972ef})

[comment]: # ({8be4a541-97fb4186})

##### trim(value,<chars>) {#trim}

Remove specified characters from the beginning and end of string.<br>
Supported value types: *String*, *Text*, *Log*.

Parameter: 

-   **value** - the value to check;<br>
-   **chars** (optional) - specify the characters to remove.

Whitespace is trimmed from both sides by default (if no optional characters are specified). See also: [ltrim()](#ltrim), [rtrim()](#rtrim).

Examples:

    trim(last(/host/key)) - remove whitespace from the beginning and end of string
    trim(last(/host/key),"_") - remove '_' from the beginning and end of string

[comment]: # ({/8be4a541-97fb4186})

[comment]: # ({ba59dd3a-b8eeecec})

See [all supported functions](/manual/appendix/functions).

[comment]: # ({/ba59dd3a-b8eeecec})
