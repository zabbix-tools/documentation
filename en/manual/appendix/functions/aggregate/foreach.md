[comment]: # ({6537e63f-6537e63f})
# 1 Foreach functions

[comment]: # ({/6537e63f-6537e63f})

[comment]: # ({b969f625-7d50e3a2})
### Overview

Foreach functions are used in [aggregate calculations](/manual/config/items/itemtypes/calculated/aggregate) to return one aggregate value for each item that is selected by the used **item filter**. An array of values is returned.

For example, the *avg_foreach* function will return an array of values, where each value is the *average* history value of the selected item, during the time interval that is specified.

The [item filter](#item-filter-syntax) is part of the syntax used by foreach functions.
The use of wildcards is supported in the item filter, thus the required items can be selected quite flexibly.

[comment]: # ({/b969f625-7d50e3a2})

[comment]: # ({be40d9c6-c9e1960d})

### Supported functions

|Function|Description|
|--|--------|
|*avg_foreach*|Returns the average value for each item.|
|*bucket_rate_foreach*|Returns pairs (bucket upper bound, rate value) suitable for use in the [histogram\_quantile()](/manual/appendix/functions/aggregate#aggregate_functions1) function, where "bucket upper bound" is the value of item key parameter defined by the <parameter number> [parameter](#additional_parameters).|
|*count_foreach*|Returns the number of values for each item.|
|*exists_foreach*|Returns '1' for each enabled item.|
|*last_foreach*|Returns the last value for each item.|
|*max_foreach*|Returns the maximum value for each item.|
|*min_foreach*|Returns the minimum value for each item.|
|*sum_foreach*|Returns the sum of values for each item.|

[comment]: # ({/be40d9c6-c9e1960d})

[comment]: # ({e6ad63b7-2cf639a9})
### Function syntax

Foreach functions support two common parameters: `item filter` (see details below) and `time period`:

    foreach_function(item filter,time period)

For example: 

`avg_foreach(/*/mysql.qps?[group="MySQL Servers"],5m)`

will return the five-minute average of each 'mysql.qps' item in the MySQL server group.

Note that some functions support additional [parameters](#additional_parameters).

[comment]: # ({/e6ad63b7-2cf639a9})

[comment]: # ({5cffbecf-737d2e38})
#### Item filter syntax

The item filter:

    /host/key[parameters]?[conditions]

consists of four parts, where:

*   host - host name
*   key - item key (without parameters)
*   parameters - item key parameters
*   conditions - host group and/or item tag based conditions (as expression)

Spaces are allowed only inside the conditions expression.

**Wildcard usage**

*   Wildcard can be used to replace the host name, item key or an individual item key parameter.
*   Either the host or item key must be specified without wildcard. So `/host/*` and `/*/key` are valid filters, but `/*/*` is invalid.
*   Wildcard cannot be used for a *part* of host name, item key, item key parameter.
*   Wildcard does not match more than a single item key parameter. So a wildcard must be specified for each parameter in separation (i.e. `key[abc,*,*]`).

**Conditions expression**

The conditions expression supports:

*   operands:
    *   group - host group
    *   tag - item tag
    *   `"<text>"` - string constant, with the `\` escape character to escape `"` and `\`
*   case-sensitive string comparison operators: `=`, `<>`
*   logical operators: `and`, `or`, `not`
*   grouping with parentheses: `(` `)`

Quotation of string constants is mandatory. Only case-sensitive full string comparison is supported.

**Examples**

A complex filter may be used, referencing the item key, host group and tags, as illustrated by the examples:

|Syntax example|Description|
|--|--|
|`/host/key[abc,*]`|Matches similar items on this host.|
|`/*/key`|Matches the same item of any host.|
|`/*/key?[group="ABC" and tag="tagname:value"]`|Matches the same item of any host from the ABC group having 'tagname:value' tags.|
|`/*/key[a,*,c]?[(group="ABC" and tag="Tag1") or (group="DEF" and (tag="Tag2" or tag="Tag3:value"))]`|Matches similar items of any host from the ABC or DEF group with the respective tags.|

All referenced items must exist and collect data.
Only enabled items on enabled hosts are included in the calculations.
Items in the unsupported state are not included.

::: noteimportant
If the item key of a referenced item is changed, the filter must be updated manually.
:::

Specifying a parent host group includes the parent group and all nested host groups with their items.

[comment]: # ({/5cffbecf-737d2e38})

[comment]: # ({a5305339-0a9b9b28})
#### Time period

The **second** parameter allows to specify the time period for aggregation.
The time period can only be expressed as time, the amount of values (prefixed with **\#**) is not supported.

[Supported unit symbols](/manual/appendix/suffixes) can be used in this parameter for convenience,
for example, '5m' (five minutes) instead of '300s' (300 seconds) or '1d' (one day) instead of '86400' (86400 seconds).

For the *last\_foreach* function time period is an optional parameter (supported since Zabbix 7.0), which can be omitted:

    last_foreach(/*/key?[group="host group"])

Time period is not supported with the *exists\_foreach* function.

[comment]: # ({/a5305339-0a9b9b28})

[comment]: # ({937c30de-bf880e22})
#### Additional parameters

**bucket\_rate\_foreach**

A third optional parameter is supported by the *bucket\_rate\_foreach* function:

    bucket_rate_foreach(item filter,time period,<parameter number>)

where <parameter number> is the position of the "bucket" value in the item key.
For example, if the "bucket" value in myItem\[aaa,0.2\] is '0.2', then its position is 2.

The default value of <parameter number> is '1'.

**count\_foreach**

Third and fourth optional parameters are supported by the *count\_foreach* function:

    count_foreach(item filter,time period,<operator>,<pattern>)

Where:

-   **operator** is the conditional operator for item values (must be double-quoted). Supported `operators`:<br>*eq* - equal<br>*ne* - not equal<br>*gt* - greater<br>*ge* - greater or equal<br>*lt* - less<br>*le* - less or equal<br>*like* - matches if contains pattern (case-sensitive)<br>*bitand* - bitwise AND<br>*regexp* - case-sensitive match of the regular expression given in `pattern`<br>*iregexp* - case-insensitive match of the regular expression given in `pattern`<br>
-   **pattern** is the required pattern (string arguments must be double-quoted); supported if *operator* is specified in the third parameter.

Comments:

-   Optional parameters *operator* or *pattern* can't be left empty after a comma, only fully omitted.
-   With *bitand* as the third parameter, the fourth `pattern` parameter can be specified as two numbers, separated by '/': **number_to_compare_with/mask**. count() calculates "bitwise AND" from the value and the *mask* and compares the result to *number_to_compare_with*. If the result of "bitwise AND" is equal to *number_to_compare_with*, the value is counted.<br>If *number_to_compare_with* and *mask* are equal, only the *mask* need be specified (without '/').
-   With *regexp* or *iregexp* as the third parameter, the fourth `pattern` parameter can be an ordinary or [global](/manual/regular_expressions#global_regular_expressions) (starting with '@') regular expression. In case of global regular expressions case sensitivity is inherited from global regular expression settings. For the purpose of regexp matching, float values will always be represented with 4 decimal digits after '.'. Also note that for large numbers difference in decimal (stored in database) and binary (used by Zabbix server) representation may affect the 4th decimal digit.

See [aggregate calculations](/manual/config/items/itemtypes/calculated/aggregate) for more details and examples on using foreach functions.

[comment]: # ({/937c30de-bf880e22})

[comment]: # ({fc23e115-328844f6})
#### Behavior depending on availability

The following table illustrates how each function behaves in cases of limited availability of host/item and history data.

|Function|Disabled host|Unavailable host with data|Unavailable host without data|Disabled item|Unsupported item|Data retrieval error (SQL)|
|--|--|--|--|--|--|--|
|*avg_foreach*|ignore|return avg|ignore|ignore|ignore|ignore|
|*bucket_rate_foreach*|ignore|return bucket rate|ignore|ignore|ignore|ignore|
|*count_foreach*|ignore|return count|0|ignore|ignore|ignore|
|*exists_foreach*|ignore|1|1|ignore|1|n/a|
|*last_foreach*|ignore|return last|ignore|ignore|ignore|ignore|
|*max_foreach*|ignore|return max|ignore|ignore|ignore|ignore|
|*min_foreach*|ignore|return min|ignore|ignore|ignore|ignore|
|*sum_foreach*|ignore|return sum|ignore|ignore|ignore|ignore|

If the item is *ignored*, nothing is added to the aggregation.

[comment]: # ({/fc23e115-328844f6})
