[comment]: # attributes: notoc

[comment]: # (terms: abs, acos, asin, atan, atan2, avg, cbrt, ceil, cos, cosh, cot, degrees, e, exp, expm1, floor, log, log10, max, min, mod, pi, power, radians, rand, round, signum, sin, sinh, sqrt, sum, tan, truncate )

[comment]: # ({56ac0bd9-ef4bef6c})
# 6 Mathematical functions

All functions listed here are supported in:

-   [Trigger expressions](/manual/config/triggers/expression)
-   [Calculated items](/manual/config/items/itemtypes/calculated)

Mathematical functions are supported with float and integer value types,
unless stated otherwise.

The functions are listed without additional information. Click on the function to see the full details.

|Function|Description|
|--|--------|
|[abs](#abs)|The absolute value of a value.|
|[acos](#acos)|The arccosine of a value as an angle, expressed in radians.|
|[asin](#asin)|The arcsine of a value as an angle, expressed in radians.|
|[atan](#atan)|The arctangent of a value as an angle, expressed in radians.|
|[atan2](#atan2)|The arctangent of the ordinate (value) and abscissa coordinates specified as an angle, expressed in radians.|
|[avg](#avg)|The average value of the referenced item values.|
|[cbrt](#cbrt)|The cube root of a value.|
|[ceil](#ceil)|Round the value up to the nearest greater or equal integer.|
|[cos](#cos)|The cosine of a value, where the value is an angle expressed in radians.|
|[cosh](#cosh)|The hyperbolic cosine of a value.|
|[cot](#cot)|The cotangent of a value, where the value is an angle expressed in radians.|
|[degrees](#degrees)|Converts a value from radians to degrees.|
|[e](#e)|The Euler's number (2.718281828459045).|
|[exp](#exp)|The Euler's number at a power of a value.|
|[expm1](#expm1)|The Euler's number at a power of a value minus 1.|
|[floor](#floor)|Round the value down to the nearest smaller or equal integer.|
|[log](#log)|The natural logarithm.|
|[log10](#log10)|The decimal logarithm.|
|[max](#max)|The highest value of the referenced item values.|
|[min](#min)|The lowest value of the referenced item values.|
|[mod](#mod)|The division remainder.|
|[pi](#pi)|The Pi constant (3.14159265358979).|
|[power](#power)|The power of a value.|
|[radians](#radians)|Converts a value from degrees to radians.|
|[rand](#rand)|Return a random integer value.|
|[round](#round)|Round the value to decimal places.|
|[signum](#signum)|Returns '-1' if a value is negative, '0' if a value is zero, '1' if a value is positive.|
|[sin](#sin)|The sine of a value, where the value is an angle expressed in radians.|
|[sinh](#sinh)|The hyperbolical sine of a value, where the value is an angle expressed in radians.|
|[sqrt](#sqrt)|The square root of a value.|
|[sum](#sum)|The sum of the referenced item values.| 
|[tan](#tan)|The tangent of a value.|
|[truncate](#truncate)|Truncate the value to decimal places.|

[comment]: # ({/56ac0bd9-ef4bef6c})

[comment]: # ({e635e2a6-f932d69d})
### Function details

Some general notes on function parameters:

-   Function parameters are separated by a comma
-   Expressions are accepted as parameters
-   Optional function parameters (or parameter parts) are indicated by
    `<` `>`

[comment]: # ({/e635e2a6-f932d69d})

[comment]: # ({3813a030-89cbc486})

##### abs(value) {#abs}

The absolute value of a value.<br>
Supported value types: *Float*, *Integer*, *String*, *Text*, *Log*.<br>
For strings returns: 0 - the values are equal; 1 - the values differ.

Parameter: 

-   **value** - the value to check

The absolute numeric difference will be calculated, as seen with these incoming example values ('previous' and 'latest' value = absolute difference): '1' and '5' = `4`; '3' and '1' = `2`; '0' and '-2.5' = `2.5`

Example:

    abs(last(/host/key))>10

[comment]: # ({/3813a030-89cbc486})

[comment]: # ({754d73c7-f40a2e61})

##### acos(value) {#acos}

The arccosine of a value as an angle, expressed in radians.

Parameter: 

-   **value** - the value to check

The value must be between -1 and 1. For example, the arccosine of a value '0.5' will be '2.0943951'.

Example:

    acos(last(/host/key))

[comment]: # ({/754d73c7-f40a2e61})

[comment]: # ({f32d79d9-b44dbca5})

##### asin(value) {#asin}

The arcsine of a value as an angle, expressed in radians.

Parameter: 

-   **value** - the value to check

The value must be between -1 and 1. For example, the arcsine of a value '0.5' will be '-0.523598776'.

Example:

    asin(last(/host/key)) 

[comment]: # ({/f32d79d9-b44dbca5})

[comment]: # ({8e7917ff-08e0843c})

##### atan(value) {#atan}

The arctangent of a value as an angle, expressed in radians.

Parameter: 

-   **value** - the value to check

The value must be between -1 and 1. For example, the arctangent of a value '1' will be '0.785398163'.

Example:

    atan(last(/host/key))

[comment]: # ({/8e7917ff-08e0843c})

[comment]: # ({d4066088-6df7b343})

##### atan2(value,abscissa) {#atan2}

The arctangent of the ordinate (value) and abscissa coordinates specified as an angle, expressed in radians.

Parameter: 

-   **value** - the value to check;
-   **abscissa** - the abscissa value.

For example, the arctangent of the ordinate and abscissa coordinates of a value '1' will be '2.21429744'.

Example:

    atan(last(/host/key),2)

[comment]: # ({/d4066088-6df7b343})

[comment]: # ({3e04693f-6d42ceda})

##### avg(<value1>,<value2>,...) {#avg}

The average value of the referenced item values.

Parameter: 

-   **valueX** - the value returned by one of the history functions.

Example:

    avg(avg(/host/key),avg(/host2/key2))

[comment]: # ({/3e04693f-6d42ceda})

[comment]: # ({ef0cb9ef-65742fe9})

##### cbrt(value) {#cbrt}

The cube root of a value.

Parameter: 

-   **value** - the value to check

For example, the cube root of '64' will be '4', of '63' will be '3.97905721'.

Example:

    cbrt(last(/host/key))

[comment]: # ({/ef0cb9ef-65742fe9})

[comment]: # ({696b523c-4290a457})

##### ceil(value) {#ceil}

Round the value up to the nearest greater or equal integer.

Parameter: 

-   **value** - the value to check

For example, '2.4' will be rounded up to '3'. See also [floor()](#floor).

Example:

    ceil(last(/host/key))

[comment]: # ({/696b523c-4290a457})

[comment]: # ({dee428c3-b07f5dfa})

##### cos(value) {#cos}

The cosine of a value, where the value is an angle expressed in radians.

Parameter: 

-   **value** - the value to check

For example, the cosine of a value '1' will be '0.54030230586'.

Example:

    cos(last(/host/key))

[comment]: # ({/dee428c3-b07f5dfa})

[comment]: # ({a9e4704e-4df693f6})

##### cosh(value) {#cosh}

The hyperbolic cosine of a value.
Returns the value as a real number, not as scientific notation.

Parameter: 

-   **value** - the value to check

For example, the hyperbolic cosine of a value '1' will be '1.54308063482'.

Example:

    cosh(last(/host/key))

[comment]: # ({/a9e4704e-4df693f6})

[comment]: # ({b169abdb-7b0480bb})

##### cot(value) {#cot}

The cotangent of a value, where the value is an angle expressed in radians.

Parameter: 

-   **value** - the value to check

For example, the cotangent of a value '1' will be '0.54030230586'.

Example:

    cot(last(/host/key))

[comment]: # ({/b169abdb-7b0480bb})

[comment]: # ({49d843a0-552d8ac9})

##### degrees(value) {#degrees}

Converts a value from radians to degrees.

Parameter: 

-   **value** - the value to check

For example, a value '1' converted to degrees will be '57.2957795'.

Example:

    degrees(last(/host/key))

[comment]: # ({/49d843a0-552d8ac9})

[comment]: # ({7078d3f3-790837bf})

##### e {#e}

The Euler's number (2.718281828459045).

Example:

    e()

[comment]: # ({/7078d3f3-790837bf})

[comment]: # ({329513b5-5f0104cf})

##### exp(value) {#exp}

The Euler's number at a power of a value.

Parameter: 

-   **value** - the value to check

For example, Euler's number at a power of a value '2' will be '7.38905609893065'.

Example:

    exp(last(/host/key))

[comment]: # ({/329513b5-5f0104cf})

[comment]: # ({ae4a884c-fa7bce72})

##### expm1(value) {#expm1}

The Euler's number at a power of a value minus 1.

Parameter: 

-   **value** - the value to check

For example, Euler's number at a power of a value '2' minus 1 will be '6.38905609893065'.

Example:

    expm1(last(/host/key))

[comment]: # ({/ae4a884c-fa7bce72})

[comment]: # ({d6651028-367cea41})

##### floor(value) {#floor}

Round the value down to the nearest smaller or equal integer.

Parameter: 

-   **value** - the value to check

For example, '2.6' will be rounded down to '2'. See also [ceil()](#ceil).

Example:

    floor(last(/host/key))

[comment]: # ({/d6651028-367cea41})

[comment]: # ({fb4ffc82-02960b00})

##### log(value) {#log}

The natural logarithm.

Parameter: 

-   **value** - the value to check

For example, the natural logarithm of a value '2' will be '0.69314718055994529'.

Example:

    log(last(/host/key))

[comment]: # ({/fb4ffc82-02960b00})

[comment]: # ({e44235f6-9eb02241})

##### log10(value) {#log10}

The decimal logarithm.

Parameter: 

-   **value** - the value to check

For example, the decimal logarithm of a value '5' will be '0.69897000433'.

Example:

    log10(last(/host/key))

[comment]: # ({/e44235f6-9eb02241})

[comment]: # ({19974b7c-9b8efc17})

##### max(<value1>,<value2>,...) {#max}

The highest value of the referenced item values.

Parameter: 

-   **valueX** - the value returned by one of the history functions.

Example:

    max(avg(/host/key),avg(/host2/key2))

[comment]: # ({/19974b7c-9b8efc17})

[comment]: # ({7a594dec-5e82e822})

##### min(<value1>,<value2>,...) {#min}

The lowest value of the referenced item values.

Parameter: 

-   **valueX** - the value returned by one of the history functions.

Example:

    min(avg(/host/key),avg(/host2/key2))

[comment]: # ({/7a594dec-5e82e822})

[comment]: # ({7e4ca5a9-120dd48b})

##### mod(value,denominator) {#mod}

The division remainder.

Parameter: 

-   **value** - the value to check;
-   **denominator** - the division denominator.

For example, division remainder of a value '5' with division denominator '2' will be '1'.

Example:

    mod(last(/host/key),2)

[comment]: # ({/7e4ca5a9-120dd48b})

[comment]: # ({5fae5f18-d8cd6290})

##### pi {#pi}

The Pi constant (3.14159265358979).

Example:

    pi()

[comment]: # ({/5fae5f18-d8cd6290})

[comment]: # ({0cb82367-617a0fc5})

##### power(value,power value) {#power}

The power of a value.

Parameter: 

-   **value** - the value to check;
-   **power value** - the Nth power to use.

For example, the 3rd power of a value '2' will be '8'.

Example:

    power(last(/host/key),3)

[comment]: # ({/0cb82367-617a0fc5})

[comment]: # ({2e3144c2-c94a9d57})

##### radians(value) {#radians}

Converts a value from degrees to radians.

Parameter: 

-   **value** - the value to check

For example, a value '1' converted to radians will be '0.0174532925'.

Example:

    radians(last(/host/key))

[comment]: # ({/2e3144c2-c94a9d57})

[comment]: # ({91941daf-16f1713f})

##### rand {#rand}

Return a random integer value. A pseudo-random generated number using time as seed (enough for mathematical purposes, but not cryptography).

Example:

    rand()

[comment]: # ({/91941daf-16f1713f})

[comment]: # ({7f0a355c-e19fd7cb})

##### round(value,decimal places) {#round}

Round the value to decimal places.

Parameter: 

-   **value** - the value to check;
-   **decimal places** - specify decimal places for rounding (0 is also possible).

For example, a value '2.5482' rounded to 2 decimal places will be '2.55'.

Example:

    round(last(/host/key),2)

[comment]: # ({/7f0a355c-e19fd7cb})

[comment]: # ({12a89282-9668b2dc})

##### signum(value) {#signum}

Returns '-1' if a value is negative, '0' if a value is zero, '1' if a value is positive.

Parameter: 

-   **value** - the value to check.

Example:

    signum(last(/host/key))

[comment]: # ({/12a89282-9668b2dc})

[comment]: # ({8b506ba0-1c54e7c1})

##### sin(value) {#sin}

The sine of a value, where the value is an angle expressed in radians.

Parameter: 

-   **value** - the value to check

For example, the sine of a value '1' will be '0.8414709848'.

Example:

    sin(last(/host/key))

[comment]: # ({/8b506ba0-1c54e7c1})

[comment]: # ({09da637d-f2223e46})

##### sinh(value) {#sinh}

The hyperbolical sine of a value, where the value is an angle expressed in radians.

Parameter: 

-   **value** - the value to check

For example, the hyperbolical sine of a value '1' will be '1.17520119364'.

Example:

    sinh(last(/host/key))

[comment]: # ({/09da637d-f2223e46})

[comment]: # ({b3339bb1-a732b60e})

##### sqrt(value) {#sqrt}

The square root of a value.<br>
This function will fail with a negative value.

Parameter: 

-   **value** - the value to check

For example, the square root of a value '3.5' will be '1.87082869339'.

Example:

    sqrt(last(/host/key))

[comment]: # ({/b3339bb1-a732b60e})

[comment]: # ({43353605-0bbc007d})

##### sum(<value1>,<value2>,...) {#sum}

The sum of the referenced item values.

Parameter: 

-   **valueX** - the value returned by one of the history functions.

Example:

    sum(avg(/host/key),avg(/host2/key2))

[comment]: # ({/43353605-0bbc007d})

[comment]: # ({f06f407d-49e10a41})

##### tan(value) {#tan}

The tangent of a value.

Parameter: 

-   **value** - the value to check

For example, the tangent of a value '1' will be '1.55740772465'.

Example:

    tan(last(/host/key))

[comment]: # ({/f06f407d-49e10a41})

[comment]: # ({a6a9c548-53de5a0f})

##### truncate(value,decimal places) {#truncate}

Truncate the value to decimal places.

Parameter: 

-   **value** - the value to check;
-   **decimal places** - specify decimal places for truncating (0 is also possible).

For example, a value '2.5482' truncated to 2 decimal places will be '2.54'.

Example:

    truncate(last(/host/key),2)

[comment]: # ({/a6a9c548-53de5a0f})

[comment]: # ({544a48ff-92620b8c})

See [all supported functions](/manual/appendix/functions).  

[comment]: # ({/544a48ff-92620b8c})
