[comment]: # attributes: notoc

[comment]: # ({e63d56cf-a5990009})
# 7 Operator functions

All functions listed here are supported in:

-   [Trigger expressions](/manual/config/triggers/expression)
-   [Calculated items](/manual/config/items/itemtypes/calculated)

The functions are listed without additional information. Click on the function to see the full details.

|Function|Description|
|--|--------|
|[between](#between)|Check if the value belongs to the given range.|
|[in](#in)|Check if the value is equal to at least one of the listed values.|

[comment]: # ({/e63d56cf-a5990009})

[comment]: # ({e284c360-3b2fb872})
### Function details

Some general notes on function parameters:

-   Function parameters are separated by a comma
-   Expressions are accepted as parameters

[comment]: # ({/e284c360-3b2fb872})

[comment]: # ({56290315-0d7a79bf})

##### between(value,min,max) {#between}

Check if the value belongs to the given range.<br>
Supported value types: *Integer*, *Float*.<br>
Returns: 1 - in range; 0 - otherwise.

Parameter: 

-   **value** - the value to check;<br>
-   **min** - the minimum value;<br>
-   **max** - the maximum value.

Example:

    between(last(/host/key),1,10)=1 #trigger if the value is between 1 and 10

[comment]: # ({/56290315-0d7a79bf})

[comment]: # ({48b101b6-2300beb9})

##### in(value,value1,value2,...valueN) {#in}

Check if the value is equal to at least one of the listed values.<br>
Supported value types: *Integer*, *Float*, *Character*, *Text*, *Log*.<br>
Returns: 1 - if equal; 0 - otherwise.

Parameter: 

-   **value** - the value to check;<br>
-   **valueX** - listed values (string values must be double-quoted).

The value is compared to the listed values as numbers, if all of these values can be converted to numeric; otherwise compared as strings.

Example:

    in(last(/host/key),5,10)=1 #trigger if the last value is equal to 5 or 10
    in("text",last(/host/key),last(/host/key,#2))=1 #trigger if "text" is equal to either of the last 2 values

[comment]: # ({/48b101b6-2300beb9})

[comment]: # ({ba59dd3a-690cfdc0})

See [all supported functions](/manual/appendix/functions).

[comment]: # ({/ba59dd3a-690cfdc0})
