[comment]: # attributes: notoc

[comment]: # (terms:  baselinedev, baselinewma, change, changecount, count, countunique, find, first, fuzzytime, last, logeventid, logseverity, logsource, monodec, monoinc, nodata, percentile, rate, trendavg, trendcount, trendmax, trendmin, trendstl, trendsum )

[comment]: # ({f0d0ecf4-7da5ea1c})
# 4 History functions

All functions listed here are supported in:

-   [Trigger expressions](/manual/config/triggers/expression)
-   [Calculated items](/manual/config/items/itemtypes/calculated)

The functions are listed without additional information. Click on the function to see the full details.

|Function|Description|
|--|--------|
|[change](#change)|The amount of difference between the previous and latest value.|
|[changecount](#changecount)|The number of changes between adjacent values within the defined evaluation period.|
|[count](#count)|The number of values within the defined evaluation period.|
|[countunique](#countunique)|The number of unique values within the defined evaluation period.|
|[find](#find)|Find a value match within the defined evaluation period.|
|[first](#first)|The first (the oldest) value within the defined evaluation period.|
|[fuzzytime](#fuzzytime)|Check how much the passive agent time differs from the Zabbix server/proxy time.|
|[last](#last)|The most recent value.|
|[logeventid](#logeventid)|Check if the event ID of the last log entry matches a regular expression.|
|[logseverity](#logseverity)|The log severity of the last log entry.|
|[logsource](#logsource)|Check if log source of the last log entry matches a regular expression.|
|[monodec](#monodec)|Check if there has been a monotonous decrease in values.|
|[monoinc](#monoinc)|Check if there has been a monotonous increase in values.|
|[nodata](#nodata)|Check for no data received.|
|[percentile](#percentile)|The P-th percentile of a period, where P (percentage) is specified by the third parameter.|
|[rate](#rate)|The per-second average rate of the increase in a monotonically increasing counter within the defined time period.|

[comment]: # ({/f0d0ecf4-7da5ea1c})

[comment]: # ({4d343eb8-4d343eb8})
##### Common parameters

-   `/host/key` is a common mandatory first parameter for the functions
    referencing the host item history
-   `(sec|#num)<:time shift>` is a common second parameter for the
    functions referencing the host item history, where:
    -   **sec** - maximum [evaluation
        period](/manual/config/triggers#evaluation_period) in seconds
        (time [suffixes](/manual/appendix/suffixes) can be used), or
    -   **\#num** - maximum [evaluation
        range](/manual/config/triggers#evaluation_period) in latest
        collected values (if preceded by a hash mark)
    -   **time shift** (optional) allows to move the evaluation point
        back in time. See [more
        details](/manual/config/triggers/expression#time_shift) on
        specifying time shift.

[comment]: # ({/4d343eb8-4d343eb8})

[comment]: # ({c645f0e4-8f3a0028})
### Function details

Some general notes on function parameters:

-   Function parameters are separated by a comma
-   Optional function parameters (or parameter parts) are indicated by
    `<` `>`
-   Function-specific parameters are described with each function
-   `/host/key` and `(sec|#num)<:time shift>` parameters must never be
    quoted

[comment]: # ({/c645f0e4-8f3a0028})

[comment]: # ({56243e71-4afc9a02})

##### change(/host/key) {#change}

The amount of difference between the previous and latest value.<br>
Supported value types: *Float*, *Integer*, *String*, *Text*, *Log*.<br>
For strings returns: 0 - values are equal; 1 - values differ.

Parameters: see [common parameters](#common-parameters).

Comments:

-   Numeric difference will be calculated, as seen with these incoming example values ('previous' and 'latest' value = difference):<br>'1' and '5' = `+4`<br>'3' and '1' = `-2`<br>'0' and '-2.5' = `-2.5`<br>
-   See also: [abs](/manual/appendix/functions/math) for comparison.

Examples:

    change(/host/key)>10

[comment]: # ({/56243e71-4afc9a02})

[comment]: # ({f49c21ef-57458a90})

##### changecount(/host/key,(sec|#num)<:time shift>,<mode>) {#changecount}

The number of changes between adjacent values within the defined evaluation period.<br>
Supported value types: *Float*, *Integer*, *String*, *Text*, *Log*.

Parameters: 

-   See [common parameters](#common-parameters);<br>
-   **mode** (must be double-quoted) - possible values: *all* - count all changes (default); *dec* - count decreases; *inc* - count increases

For non-numeric value types, the *mode* parameter is ignored.

Examples:

    changecount(/host/key,1w) #the number of value changes for the last week until now
    changecount(/host/key,#10,"inc") #the number of value increases (relative to the adjacent value) among the last 10 values
    changecount(/host/key,24h,"dec") #the number of value decreases (relative to the adjacent value) for the last 24 hours until now

[comment]: # ({/f49c21ef-57458a90})

[comment]: # ({6b02906d-f9ce5dd9})

##### count(/host/key,(sec|#num)<:time shift>,<operator>,<pattern>) {#count}

The number of values within the defined evaluation period.<br>
Supported value types: *Float*, *Integer*, *String*, *Text*, *Log*.

Parameters: 

-   See [common parameters](#common-parameters);<br>
-   **operator** (must be double-quoted). Supported `operators`:<br>*eq* - equal (default for integer, float)<br>*ne* - not equal<br>*gt* - greater<br>*ge* - greater or equal<br>*lt* - less<br>*le* - less or equal<br>*like* (default for string, text, log) - matches if contains pattern (case-sensitive)<br>*bitand* - bitwise AND<br>*regexp* - case-sensitive match of the regular expression given in `pattern`<br>*iregexp* - case-insensitive match of the regular expression given in `pattern`<br>
-   **pattern** - the required pattern (string arguments must be double-quoted).

Comments:

-   Float items match with the precision of 2.22e-16;
-   *like* is not supported as operator for integer values; 
-   *like* and *bitand* are not supported as operators for float values;
-   For string, text, and log values only *eq*, *ne*, *like*, *regexp* and *iregexp* operators are supported;
-   With *bitand* as operator, the fourth `pattern` parameter can be specified as two numbers, separated by '/': **number_to_compare_with/mask**. count() calculates "bitwise AND" from the value and the *mask* and compares the result to *number_to_compare_with*. If the result of "bitwise AND" is equal to *number_to_compare_with*, the value is counted.<br>If *number_to_compare_with* and *mask* are equal, only the *mask* need be specified (without '/').
-   With *regexp* or *iregexp* as operator, the fourth `pattern` parameter can be an ordinary or [global](/manual/regular_expressions#global_regular_expressions) (starting with '@') regular expression. In case of global regular expressions case sensitivity is inherited from global regular expression settings. For the purpose of regexp matching, float values will always be represented with 4 decimal digits after '.'. Also note that for large numbers difference in decimal (stored in database) and binary (used by Zabbix server) representation may affect the 4th decimal digit.

Examples:

    count(/host/key,10m) #the values for the last 10 minutes until now
    count(/host/key,10m,"like","error") #the number of values for the last 10 minutes until now that contain 'error'
    count(/host/key,10m,,12) #the number of values for the last 10 minutes until now that equal '12'
    count(/host/key,10m,"gt",12) #the number of values for the last 10 minutes until now that are over '12'
    count(/host/key,#10,"gt",12) #the number of values within the last 10 values until now that are over '12'
    count(/host/key,10m:now-1d,"gt",12) #the number of values between 24 hours and 10 minutes and 24 hours ago from now that were over '12'
    count(/host/key,10m,"bitand","6/7") #the number of values for the last 10 minutes until now having '110' (in binary) in the 3 least significant bits
    count(/host/key,10m:now-1d) #the number of values between 24 hours and 10 minutes and 24 hours ago from now

[comment]: # ({/6b02906d-f9ce5dd9})

[comment]: # ({bdf23dd3-bf8dc20c})

##### countunique(/host/key,(sec|#num)<:time shift>,<operator>,<pattern>) {#countunique}

The number of unique values within the defined evaluation period.<br>
Supported value types: *Float*, *Integer*, *String*, *Text*, *Log*.

Parameters: 

-   See [common parameters](#common-parameters);<br>
-   **operator** (must be double-quoted). Supported `operators`:<br>*eq* - equal (default for integer, float)<br>*ne* - not equal<br>*gt* - greater<br>*ge* - greater or equal<br>*lt* - less<br>*le* - less or equal<br>*like* (default for string, text, log) - matches if contains pattern (case-sensitive)<br>*bitand* - bitwise AND<br>*regexp* - case-sensitive match of the regular expression given in `pattern`<br>*iregexp* - case-insensitive match of the regular expression given in `pattern`<br>
-   **pattern** - the required pattern (string arguments must be double-quoted).

Comments:

-   Float items match with the precision of 2.22e-16;
-   *like* is not supported as operator for integer values; 
-   *like* and *bitand* are not supported as operators for float values;
-   For string, text, and log values only *eq*, *ne*, *like*, *regexp* and *iregexp* operators are supported;
-   With *bitand* as operator, the fourth `pattern` parameter can be specified as two numbers, separated by '/': **number_to_compare_with/mask**. countunique() calculates "bitwise AND" from the value and the *mask* and compares the result to *number_to_compare_with*. If the result of "bitwise AND" is equal to *number_to_compare_with*, the value is counted.<br>If *number_to_compare_with* and *mask* are equal, only the *mask* need be specified (without '/').
-   With *regexp* or *iregexp* as operator, the fourth `pattern` parameter can be an ordinary or [global](/manual/regular_expressions#global_regular_expressions) (starting with '@') regular expression. In case of global regular expressions case sensitivity is inherited from global regular expression settings. For the purpose of regexp matching, float values will always be represented with 4 decimal digits after '.'. Also note that for large numbers difference in decimal (stored in database) and binary (used by Zabbix server) representation may affect the 4th decimal digit.

Examples:

    countunique(/host/key,10m) #the number of unique values for the last 10 minutes until now
    countunique(/host/key,10m,"like","error") #the number of unique values for the last 10 minutes until now that contain 'error'
    countunique(/host/key,10m,,12) #the number of unique values for the last 10 minutes until now that equal '12'
    countunique(/host/key,10m,"gt",12) #the number of unique values for the last 10 minutes until now that are over '12'
    countunique(/host/key,#10,"gt",12) #the number of unique values within the last 10 values until now that are over '12'
    countunique(/host/key,10m:now-1d,"gt",12) #the number of unique values between 24 hours and 10 minutes and 24 hours ago from now that were over '12'
    countunique(/host/key,10m,"bitand","6/7") #the number of unique values for the last 10 minutes until now having '110' (in binary) in the 3 least significant bits
    countunique(/host/key,10m:now-1d) #the number of unique values between 24 hours and 10 minutes and 24 hours ago from now

[comment]: # ({/bdf23dd3-bf8dc20c})

[comment]: # ({184ffd8b-148d7d16})

##### find(/host/key,(sec|#num)<:time shift>,<operator>,<pattern>) {#find}

Find a value match within the defined evaluation period.<br>
Supported value types: *Float*, *Integer*, *String*, *Text*, *Log*.<br>
Returns: 1 - found; 0 - otherwise.

Parameters: 

-   See [common parameters](#common-parameters);<br>
-   **sec** or **#num** (optional) - defaults to the latest value if not specified
-   **operator** (must be double-quoted). Supported `operators`:<br>*eq* - equal (default for integer, float)<br>*ne* - not equal<br>*gt* - greater<br>*ge* - greater or equal<br>*lt* - less<br>*le* - less or equal<br>*like* (default for string, text, log) - matches if contains the string given in `pattern` (case-sensitive)<br>*bitand* - bitwise AND<br>*regexp* - case-sensitive match of the regular expression given in `pattern`<br>*iregexp* - case-insensitive match of the regular expression given in `pattern`<br>
-   **pattern** - the required pattern (string arguments must be double-quoted); [Perl Compatible Regular Expression](https://en.wikipedia.org/wiki/Perl_Compatible_Regular_Expressions) (PCRE) regular expression if `operator` is *regexp*, *iregexp*.

Comments:

-   If more than one value is processed, '1' is returned if there is at least one matching value;
-   *like* is not supported as operator for integer values; 
-   *like* and *bitand* are not supported as operators for float values;
-   For string, text, and log values only *eq*, *ne*, *like*, *regexp* and *iregexp* operators are supported;
-   With *regexp* or *iregexp* as operator, the fourth `pattern` parameter can be an ordinary or [global](/manual/regular_expressions#global_regular_expressions) (starting with '@') regular expression. In case of global regular expressions case sensitivity is inherited from the global regular expression settings.

Example:

    find(/host/key,10m,"like","error") #find a value that contains 'error' within the last 10 minutes until now

[comment]: # ({/184ffd8b-148d7d16})

[comment]: # ({c4d477a8-0ae5f587})

##### first(/host/key,sec<:time shift>) {#first}

The first (the oldest) value within the defined evaluation period.<br>
Supported value types: *Float*, *Integer*, *String*, *Text*, *Log*.

Parameters: 

-   See [common parameters](#common-parameters).

See also [last()](#last).

Example:

    first(/host/key,1h) #retrieve the oldest value within the last hour until now

[comment]: # ({/c4d477a8-0ae5f587})

[comment]: # ({82eea7f8-1c3609f8})

##### fuzzytime(/host/key,sec) {#fuzzytime}

Check how much the passive agent time differs from the Zabbix server/proxy time.<br>
Supported value types: *Float*, *Integer*.<br>
Returns: 1 - difference between the passive item value (as timestamp) and Zabbix server/proxy timestamp (the clock of value collection) is less than or equal to T seconds; 0 - otherwise.

Parameters: 

-   See [common parameters](#common-parameters).

Comments:

-   Usually used with the 'system.localtime' item to check that local time is in sync with the local time of Zabbix server. *Note* that 'system.localtime' must be configured as a [passive check](/manual/appendix/items/activepassive#passive_checks).
-   Can be used also with the `vfs.file.time[/path/file,modify]` key to check that the file did not get updates for long time;
-   This function is not recommended for use in complex trigger expressions (with multiple items involved), because it may cause unexpected results (time difference will be measured with the most recent metric), e.g. in `fuzzytime(/Host/system.localtime,60s)=0 or last(/Host/trap)<>0`.

Example:

    fuzzytime(/host/key,60s)=0 #detect a problem if the time difference is over 60 seconds<br><br>

[comment]: # ({/82eea7f8-1c3609f8})

[comment]: # ({7b88d2a6-8059d345})

##### last(/host/key,<#num<:time shift>>) {#last}

The most recent value.<br>
Supported value types: *Float*, *Integer*, *String*, *Text*, *Log*.

Parameters: 

-   See [common parameters](#common-parameters);<br>
-   **#num** (optional) - the Nth most recent value.

Comments:

-   Take note that a hash-tagged time period (#N) works differently here than with many other functions. For example: `last()` is always equal to `last(#1)`; `last(#3)` - the third most recent value (*not* three latest values);
-   Zabbix does not guarantee the exact order of values if more than two values exist within one second in history;
-   See also [first()](#first).

Example:

    last(/host/key) #retrieve the last value
    last(/host/key,#2) #retrieve the previous value
    last(/host/key,#1) <> last(/host/key,#2) #the last and previous values differ

[comment]: # ({/7b88d2a6-8059d345})

[comment]: # ({21933439-ff5dcd5e})

##### logeventid(/host/key,<#num<:time shift>>,<pattern>) {#logeventid}

Check if the event ID of the last log entry matches a regular expression.<br>
Supported value types: *Log*.<br>
Returns: 0 - does not match; 1 - matches.

Parameters: 

-   See [common parameters](#common-parameters);<br>
-   **#num** (optional) - the Nth most recent value;<br>
-   **pattern** (optional) - the regular expression describing the required pattern, [Perl Compatible Regular Expression](https://en.wikipedia.org/wiki/Perl_Compatible_Regular_Expressions) (PCRE) style (string arguments must be double-quoted).

[comment]: # ({/21933439-ff5dcd5e})

[comment]: # ({d0b60829-5053b990})

##### logseverity(/host/key,<#num<:time shift>>) {#logseverity}

Log severity of the last log entry.<br>
Supported value types: *Log*.<br>
Returns: 0 - default severity; N - severity (integer, useful for Windows event logs: 1 - Information, 2 - Warning, 4 - Error, 7 - Failure Audit, 8 - Success Audit, 9 - Critical, 10 - Verbose).

Parameters: 

-   See [common parameters](#common-parameters);<br>
-   **#num** (optional) - the Nth most recent value.

Zabbix takes log severity from the **Information** field of Windows event log.

[comment]: # ({/d0b60829-5053b990})

[comment]: # ({e2ac7631-a9148f5c})

##### logsource(/host/key,<#num<:time shift>>,<pattern>) {#logsource}

Check if log source of the last log entry matches a regular expression.<br>
Supported value types: *Log*.<br>
Returns: 0 - does not match; 1 - matches.

Parameters: 

-   See [common parameters](#common-parameters);<br>
-   **#num** (optional) - the Nth most recent value;<br>
-   **pattern** (optional) - the regular expression describing the required pattern, [Perl Compatible Regular Expression](https://en.wikipedia.org/wiki/Perl_Compatible_Regular_Expressions) (PCRE) style (string arguments must be double-quoted).

Normally used for Windows event logs.

Example:

    logsource(/host/key,,"VMware Server")

[comment]: # ({/e2ac7631-a9148f5c})

[comment]: # ({1d728ea7-6a63b5c8})

##### monodec(/host/key,(sec|#num)<:time shift>,<mode>) {#monodec}

Check if there has been a monotonous decrease in values.<br>
Supported value types: *Integer*.<br>
Returns: 1 - if all elements in the time period continuously decrease; 0 - otherwise.

Parameters: 

-   See [common parameters](#common-parameters);<br>
-   **mode** (must be double-quoted) - *weak* (every value is smaller or the same as the previous one; default) or *strict* (every value has decreased).

Example:

    monodec(/Host1/system.swap.size[all,free],60s) + monodec(/Host2/system.swap.size[all,free],60s) + monodec(/Host3/system.swap.size[all,free],60s) #calculate in how many hosts there has been a decrease in free swap size

[comment]: # ({/1d728ea7-6a63b5c8})

[comment]: # ({7e0d5aff-755c1821})

##### monoinc(/host/key,(sec|#num)<:time shift>,<mode>) {#monoinc}

Check if there has been a monotonous increase in values.<br>
Supported value types: *Integer*.<br>
Returns: 1 - if all elements in the time period continuously increase; 0 - otherwise.

Parameters: 

-   See [common parameters](#common-parameters);<br>
-   **mode** (must be double-quoted) - *weak* (every value is bigger or the same as the previous one; default) or *strict* (every value has increased).

Example:

    monoinc(/Host1/system.localtime,#3,"strict")=0 #check if the system local time has been increasing consistently

[comment]: # ({/7e0d5aff-755c1821})

[comment]: # ({bc97010a-178e3950})

##### nodata(/host/key,sec,<mode>) {#nodata}

Check for no data received.<br>
Supported value types: *Integer*, *Float*, *Character*, *Text*, *Log*.<br>
Returns: 1 - if no data received during the defined period of time; 0 - otherwise.

Parameters: 

-   See [common parameters](#common-parameters);<br>
-   **sec** - the period should not be less than 30 seconds because the history syncer process calculates this function only every 30 seconds; `nodata(/host/key,0)` is disallowed.
-   **mode** - if set to *strict* (double-quoted), this function will be insensitive to proxy availability (see comments for details).

Comments:

-   the 'nodata' triggers monitored by proxy are, by default, sensitive to proxy availability - if proxy becomes unavailable, the 'nodata' triggers will not fire immediately after a restored connection, but will skip the data for the delayed period. Note that for passive proxies suppression is activated if connection is restored more than 15 seconds and no less than 2 & ProxyUpdateFrequency seconds later. For active proxies suppression is activated if connection is restored more than 15 seconds later. To turn off sensitiveness to proxy availability, use the third parameter, e.g.: `nodata(/host/key,5m,"strict")`; in this case the function will fire as soon as the evaluation period (five minutes) without data has past.<br>
-   This function will display an error if, within the period of the 1st parameter:<br>- there's no data and Zabbix server was restarted<br>- there's no data and maintenance was completed<br>- there's no data and the item was added or re-enabled<br>
-   Errors are displayed in the *Info* column in trigger [configuration](/manual/web_interface/frontend_sections/data_collection/hosts/triggers);<br>
-   This function may not work properly if there are time differences between Zabbix server, proxy and agent. See also: [Time synchronization requirement](/manual/installation/requirements#time_synchronization).

[comment]: # ({/bc97010a-178e3950})

[comment]: # ({228359d9-e8041c00})

##### percentile(/host/key,(sec|#num)<:time shift>,percentage) {#percentile}

The P-th percentile of a period, where P (percentage) is specified by the third parameter.<br>
Supported value types: *Float*, *Integer*.

Parameters: 

-   See [common parameters](#common-parameters);<br>
-   **percentage** - a floating-point number between 0 and 100 (inclusive) with up to 4 digits after the decimal point.

[comment]: # ({/228359d9-e8041c00})

[comment]: # ({33909bcc-3b1de601})

##### rate(/host/key,sec<:time shift>) {#rate}

The per-second average rate of the increase in a monotonically increasing counter within the defined time period.<br>
Supported value types: *Float*, *Integer*.

Parameters: 

-   See [common parameters](#common-parameters).

Functionally corresponds to '[rate](https://prometheus.io/docs/prometheus/latest/querying/functions/#rate)' of PromQL.

Example:

    rate(/host/key,30s) #if the monotonic increase over 30 seconds is 20, this function will return 0.67.

[comment]: # ({/33909bcc-3b1de601})

[comment]: # ({ba59dd3a-ce01d749})

See [all supported functions](/manual/appendix/functions).

[comment]: # ({/ba59dd3a-ce01d749})
