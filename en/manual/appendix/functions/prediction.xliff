<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="en" datatype="plaintext" original="manual/appendix/functions/prediction.md">
    <body>
      <trans-unit id="2e2a1a06" xml:space="preserve">
        <source># 8 Prediction functions

All functions listed here are supported in:

-   [Trigger expressions](/manual/config/triggers/expression)
-   [Calculated items](/manual/config/items/itemtypes/calculated)

The functions are listed without additional information. Click on the function to see the full details.

|Function|Description|
|--|--------|
|[forecast](#forecast)|The future value, max, min, delta or avg of the item.|
|[timeleft](#timeleft)|The time in seconds needed for an item to reach the specified threshold.|</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/appendix/functions/prediction.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="4d343eb8" xml:space="preserve">
        <source>##### Common parameters

-   `/host/key` is a common mandatory first parameter for the functions
    referencing the host item history
-   `(sec|#num)&lt;:time shift&gt;` is a common second parameter for the
    functions referencing the host item history, where:
    -   **sec** - maximum [evaluation
        period](/manual/config/triggers#evaluation_period) in seconds
        (time [suffixes](/manual/appendix/suffixes) can be used), or
    -   **#num** - maximum [evaluation
        range](/manual/config/triggers#evaluation_period) in latest
        collected values (if preceded by a hash mark)
    -   **time shift** (optional) allows to move the evaluation point
        back in time. See [more
        details](/manual/config/triggers/expression#time_shift) on
        specifying time shift.</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/appendix/functions/prediction.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="f3b63bc1" xml:space="preserve">
        <source>### Function details

Some general notes on function parameters:

-   Function parameters are separated by a comma
-   Optional function parameters (or parameter parts) are indicated by
    `&lt;` `&gt;`
-   Function-specific parameters are described with each function
-   `/host/key` and `(sec|#num)&lt;:time shift&gt;` parameters must never be
    quoted</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/appendix/functions/prediction.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="e4452b37" xml:space="preserve">
        <source>
##### forecast(/host/key,(sec|#num)&lt;:time shift&gt;,time,&lt;fit&gt;,&lt;mode&gt;) {#forecast}

The future value, max, min, delta or avg of the item.&lt;br&gt;
Supported value types: *Float*, *Integer*.

Parameters: 

-   See [common parameters](#common-parameters);&lt;br&gt;
-   **time** - the forecasting horizon in seconds (time suffixes can be used); negative values are supported;&lt;br&gt;
-   **fit** (optional; must be double-quoted) - the function used to fit historical data. Supported fits:&lt;br&gt;*linear* - linear function (default)&lt;br&gt;*polynomialN* - polynomial of degree N (1 &lt;= N &lt;= 6)&lt;br&gt;*exponential* - exponential function&lt;br&gt;*logarithmic* - logarithmic function&lt;br&gt;*power* - power function&lt;br&gt;Note that *polynomial1* is equivalent to *linear*;
-   **mode** (optional; must be double-quoted) - the demanded output. Supported modes:&lt;br&gt;*value* - value (default)&lt;br&gt;*max* - maximum&lt;br&gt;*min* - minimum&lt;br&gt;*delta* - *max*-*min*&lt;br&gt;*avg* - average&lt;br&gt;Note that *value* estimates the item value at the moment `now` + `time`; *max*, *min*, *delta* and *avg* investigate the item value estimate on the interval between `now` and `now` + `time`.

Comments:

-   If the value to return is larger than 1.7976931348623158E+308 or less than -1.7976931348623158E+308, the return value is cropped to 1.7976931348623158E+308 or -1.7976931348623158E+308 correspondingly;
-   Becomes unsupported only if misused in the expression (wrong item type, invalid parameters), otherwise returns -1 in case of errors;
-   See also additional information on [predictive trigger functions](/manual/config/triggers/prediction).

Examples:

    forecast(/host/key,#10,1h) #forecast the item value in one hour based on the last 10 values
    forecast(/host/key,1h,30m) #forecast the item value in 30 minutes based on the last hour data
    forecast(/host/key,1h:now-1d,12h) #forecast the item value in 12 hours based on one hour one day ago
    forecast(/host/key,1h,10m,"exponential") #forecast the item value in 10 minutes based on the last hour data and exponential function
    forecast(/host/key,1h,2h,"polynomial3","max") #forecast the maximum value the item can reach in the next two hours based on last hour data and cubic (third degree) polynomial
    forecast(/host/key,#2,-20m) #estimate the item value 20 minutes ago based on the last two values (this can be more precise than using last(), especially if the item is updated rarely, say, once an hour)</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/appendix/functions/prediction.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="9b065620" xml:space="preserve">
        <source>
##### timeleft(/host/key,(sec|#num)&lt;:time shift&gt;,threshold,&lt;fit&gt;) {#timeleft}

The time in seconds needed for an item to reach the specified threshold.&lt;br&gt;
Supported value types: *Float*, *Integer*.

Parameters: 

-   See [common parameters](#common-parameters);&lt;br&gt;
-   **threshold** - the value to reach ([unit suffixes](/manual/appendix/suffixes) can be used);
-   **fit** (optional; must be double-quoted) - see [forecast()](#forecast).

Comments:

-   If the value to return is larger than 1.7976931348623158E+308, the return value is cropped to 1.7976931348623158E+308;
-   Returns 1.7976931348623158E+308 if the threshold cannot be reached;
-   Becomes unsupported only if misused in the expression (wrong item type, invalid parameters), otherwise returns -1 in case of errors;
-   See also additional information on [predictive trigger functions](/manual/config/triggers/prediction).

Examples:

    timeleft(/host/key,#10,0) #the time until the item value reaches zero based on the last 10 values
    timeleft(/host/key,1h,100) #the time until the item value reaches 100 based on the last hour data
    timeleft(/host/key,1h:now-1d,100) #the time until the item value reaches 100 based on one hour one day ago
    timeleft(/host/key,1h,200,"polynomial2") #the time until the item value reaches 200 based on the last hour data and assumption that the item behaves like a quadratic (second degree) polynomial</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/appendix/functions/prediction.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="9958c480" xml:space="preserve">
        <source>
See [all supported functions](/manual/appendix/functions).</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/appendix/functions/prediction.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
    </body>
  </file>
</xliff>
