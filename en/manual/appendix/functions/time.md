[comment]: # attributes: notoc

[comment]: # ({6fdc3892-493edd72})
# 3 Date and time functions

All functions listed here are supported in:

-   [Trigger expressions](/manual/config/triggers/expression)
-   [Calculated items](/manual/config/items/itemtypes/calculated)

::: noteimportant
Date and time functions cannot be used in the
expression alone; at least one non-time-based function referencing the
host item must be present in the expression.
:::

The functions are listed without additional information. Click on the function to see the full details.

|Function|Description|
|--|--------|
|[date](#date)|The current date in YYYYMMDD format.|
|[dayofmonth](#dayofmonth)|The day of month in range of 1 to 31.|
|[dayofweek](#dayofweek)|The day of week in range of 1 to 7.|
|[now](#now)|The number of seconds since the Epoch (00:00:00 UTC, January 1, 1970).|
|[time](#time)|The current time in HHMMSS format.|

[comment]: # ({/6fdc3892-493edd72})

[comment]: # ({cc36f73e-fbd1f745})
### Function details

[comment]: # ({/cc36f73e-fbd1f745})

[comment]: # ({5bd32636-5fe053df})

##### date

The current date in YYYYMMDD format.

Example:

    date()<20220101

[comment]: # ({/5bd32636-5fe053df})

[comment]: # ({b41d8b0d-9ba00e3f})

##### dayofmonth

The day of month in range of 1 to 31.

Example:

    dayofmonth()=1

[comment]: # ({/b41d8b0d-9ba00e3f})

[comment]: # ({6e794389-d60d267c})

##### dayofweek

The day of week in range of 1 to 7 (Mon - 1, Sun - 7).

Example (only weekdays):

    dayofweek()<6

Example (only weekend):

    dayofweek()>5

[comment]: # ({/6e794389-d60d267c})

[comment]: # ({d6c3ffb1-e049cb14})

##### now

The number of seconds since the Epoch (00:00:00 UTC, January 1, 1970).

Example:

    now()<1640998800

[comment]: # ({/d6c3ffb1-e049cb14})

[comment]: # ({bd95c60c-6f5f853e})

##### time

The current time in HHMMSS format.

Example (only nighttime, 00:00-06:00):

    time()<060000

[comment]: # ({/bd95c60c-6f5f853e})

[comment]: # ({ba59dd3a-30214ece})

See [all supported functions](/manual/appendix/functions).

[comment]: # ({/ba59dd3a-30214ece})
