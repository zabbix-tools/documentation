[comment]: # attributes: notoc

[comment]: # (terms:  avg, bucket_percentile, count, histogram_quantile, item_count, kurtosis, mad, max, min, skewness, stddevpop, stddevsamp, sum, sumofsquares, varpop, varsamp )

[comment]: # (tags:  count )

[comment]: # ({595b72b9-ee60e21e})
# 1 Aggregate functions

Except where stated otherwise, all functions listed here are supported
in:

-   [Trigger expressions](/manual/config/triggers/expression)
-   [Calculated items](/manual/config/items/itemtypes/calculated)

Aggregate functions can work with either:

-   history of items, for example, `min(/host/key,1h)`
-   [foreach functions](/manual/appendix/functions/aggregate/foreach) as
    the only parameter, for example, `min(last_foreach(/*/key))` (only in calculated items; cannot be used in triggers)

The functions are listed without additional information. Click on the function to see the full details.

|Function|Description|
|--|--------|
|[avg](#avg)|The average value of an item within the defined evaluation period.|
|[bucket_percentile](#bucket-percentile)|Calculates the percentile from the buckets of a histogram.|
|[count](#count)|The count of values in an array returned by a foreach function.|
|[histogram_quantile](#histogram-quantile)|Calculates the φ-quantile from the buckets of a histogram.|
|[item_count](#item-count)|The count of existing items in configuration that match the filter criteria.|
|[kurtosis](#kurtosis)|The "tailedness" of the probability distribution in collected values within the defined evaluation period.|
|[mad](#mad)|The median absolute deviation in collected values within the defined evaluation period.|
|[max](#max)|The highest value of an item within the defined evaluation period.|
|[min](#min)|The lowest value of an item within the defined evaluation period.|
|[skewness](#skewness)|The asymmetry of the probability distribution in collected values within the defined evaluation period.|
|[stddevpop](#stddevpop)|The population standard deviation in collected values within the defined evaluation period.|
|[stddevsamp](#stddevsamp)|The sample standard deviation in collected values within the defined evaluation period.|
|[sum](#sum)|The sum of collected values within the defined evaluation period.|
|[sumofsquares](#sumofsquares)|The sum of squares in collected values within the defined evaluation period.|
|[varpop](#varpop)|The population variance of collected values within the defined evaluation period.|
|[varsamp](#varsamp)|The sample variance of collected values within the defined evaluation period.|

[comment]: # ({/595b72b9-ee60e21e})

[comment]: # ({c2252736-4d343eb8})
#### Common parameters

-   `/host/key` is a common mandatory first parameter for the functions
    referencing the host item history
-   `(sec|#num)<:time shift>` is a common second parameter for the
    functions referencing the host item history, where:
    -   **sec** - maximum [evaluation
        period](/manual/config/triggers#evaluation_period) in seconds
        (time [suffixes](/manual/appendix/suffixes) can be used), or
    -   **\#num** - maximum [evaluation
        range](/manual/config/triggers#evaluation_period) in latest
        collected values (if preceded by a hash mark)
    -   **time shift** (optional) allows to move the evaluation point
        back in time. See [more
        details](/manual/config/triggers/expression#time_shift) on
        specifying time shift.

[comment]: # ({/c2252736-4d343eb8})

[comment]: # ({c645f0e4-fe53c04c})
### Function details

Some general notes on function parameters:

-   Function parameters are separated by a comma
-   Optional function parameters (or parameter parts) are indicated by
    `<` `>`
-   Function-specific parameters are described with each function
-   `/host/key` and `(sec|#num)<:time shift>` parameters must never be
    quoted

[comment]: # ({/c645f0e4-fe53c04c})

[comment]: # ({d35dec16-f5993b4c})

##### avg(/host/key,(sec|#num)<:time shift>) {#avg}

The average value of an item within the defined evaluation period.<br>
Supported value types: *Float*, *Integer*.

Parameters: see [common parameters](#common-parameters).

Time shift is useful when there is a need to compare the current average value with the average value some time ago.

Examples:

    avg(/host/key,1h) #the average value for the last hour until now
    avg(/host/key,1h:now-1d) #the average value for an hour from 25 hours ago to 24 hours ago from now
    avg(/host/key,#5) #the average value of the five latest values
    avg(/host/key,#5:now-1d) #the average value of the five latest values excluding the values received in the last 24 hours

[comment]: # ({/d35dec16-f5993b4c})

[comment]: # ({3d2c3b94-af24d13f})

##### bucket_percentile(item filter,time period,percentage) {#bucket-percentile}

Calculates the percentile from the buckets of a histogram.<br>

Parameters: 

-   **item filter** - see [item filter](/manual/appendix/functions/aggregate/foreach#item_filter);<br>
-   **time period** - see [time period](/manual/appendix/functions/aggregate/foreach#time_period);<br>
-   **percentage** - percentage (0-100).

Comments:

-   Supported only in calculated items;
-   This function is an alias for `histogram_quantile(percentage/100, bucket_rate_foreach(item filter, time period, 1))`.

[comment]: # ({/3d2c3b94-af24d13f})

[comment]: # ({ae07bcff-3c498650})

##### count(func_foreach(item filter,<time period>),<operator>,<pattern>) {#count}

The count of values in an array returned by a foreach function.

Parameters: 

-   **func_foreach** - foreach function for which the number of returned values should be counted (with supported arguments). See [foreach functions](/manual/appendix/functions/aggregate/foreach) for details.
-   **item filter** - see [item filter](/manual/appendix/functions/aggregate/foreach#item_filter);<br>
-   **time period** - see [time period](/manual/appendix/functions/aggregate/foreach#time_period);<br>
-   **operator** (must be double-quoted). Supported `operators`:<br>*eq* - equal<br>*ne* - not equal<br>*gt* - greater<br>*ge* - greater or equal<br>*lt* - less<br>*le* - less or equal<br>*like* - matches if contains pattern (case-sensitive)<br>*bitand* - bitwise AND<br>*regexp* - case-sensitive match of the regular expression given in `pattern`<br>*iregexp* - case-insensitive match of the regular expression given in `pattern`<br>
-   **pattern** - the required pattern (string arguments must be double-quoted); supported if *operator* is specified in the third parameter.

Comments: 

-   Using **count()** with a history-related foreach function (max_foreach, avg_foreach, etc.) may lead to performance implications, whereas using **exists_foreach()**, which works only with configuration data, will not have such effect.
-   Optional parameters *operator* or *pattern* can't be left empty after a comma, only fully omitted.
-   With *bitand* as the third parameter, the fourth `pattern` parameter can be specified as two numbers, separated by '/': **number_to_compare_with/mask**. count() calculates "bitwise AND" from the value and the *mask* and compares the result to *number_to_compare_with*. If the result of "bitwise AND" is equal to *number_to_compare_with*, the value is counted.<br>If *number_to_compare_with* and *mask* are equal, only the *mask* need be specified (without '/').
-   With *regexp* or *iregexp* as the third parameter, the fourth `pattern` parameter can be an ordinary or [global](/manual/regular_expressions#global_regular_expressions) (starting with '@') regular expression. In case of global regular expressions case sensitivity is inherited from global regular expression settings. For the purpose of regexp matching, float values will always be represented with 4 decimal digits after '.'. Also note that for large numbers difference in decimal (stored in database) and binary (used by Zabbix server) representation may affect the 4th decimal digit.

Examples:

    count(max_foreach(/*/net.if.in[*],1h)) #the number of net.if.in items that received data in the last hour until now
    count(last_foreach(/*/vfs.fs.dependent.size[*,pused]),"gt",95) #the number of file systems with over 95% of disk space used

[comment]: # ({/ae07bcff-3c498650})

[comment]: # ({9ee1b045-f3ad7158})

##### histogram_quantile(quantile,bucket1,value1,bucket2,value2,...) {#histogram-quantile}

Calculates the φ-quantile from the buckets of a histogram.<br>

Parameters: 

-   **quantile** - 0 ≤ φ ≤ 1;<br>
-   **bucketN, valueN** - manually entered pairs (>=2) of parameters or the response of [bucket_rate_foreach](/manual/appendix/functions/aggregate/foreach).

Comments:

-   Supported only in calculated items;
-   Functionally corresponds to '[histogram\_quantile](https://prometheus.io/docs/prometheus/latest/querying/functions/#histogram_quantile)' of PromQL;
-   Returns -1 if values of the last 'Infinity' bucket (*"+inf"*) are equal to 0.

Examples:

    histogram_quantile(0.75,1.0,last(/host/rate_bucket[1.0]),"+Inf",last(/host/rate_bucket[Inf]))
    histogram_quantile(0.5,bucket_rate_foreach(//item_key,30s))

[comment]: # ({/9ee1b045-f3ad7158})

[comment]: # ({46026cda-c8e3133a})

##### item_count(item filter) {#item-count}

The count of existing items in configuration that match the filter criteria.<br>
Supported value type: *Integer*.

Parameter: 

-   **item filter** - criteria for item selection, allows referencing by host group, host, item key, and tags. Wildcards are supported. See [item filter](/manual/appendix/functions/aggregate/foreach#item_filter) for more details.<br>

Comments:

-   Supported only in calculated items;
-   Works as an alias for the *count(exists_foreach(item_filter))* function.

Examples:

    item_count(/*/agent.ping?[group="Host group 1"]) #the number of hosts with the *agent.ping* item in the "Host group 1"

[comment]: # ({/46026cda-c8e3133a})

[comment]: # ({6c18272c-88a54099})

##### kurtosis(/host/key,(sec|#num)<:time shift>) {#kurtosis}

The "tailedness" of the probability distribution in collected values within the defined evaluation period. See also: [Kurtosis](https://en.wikipedia.org/wiki/Kurtosis).<br>
Supported value types: *Float*, *Integer*.

Parameters: see [common parameters](#common-parameters).

Example:

    kurtosis(/host/key,1h) #kurtosis for the last hour until now

[comment]: # ({/6c18272c-88a54099})

[comment]: # ({1c1be117-bfb43a27})

##### mad(/host/key,(sec|#num)<:time shift>) {#mad}

The median absolute deviation in collected values within the defined evaluation period. See also: [Median absolute deviation](https://en.wikipedia.org/wiki/Median_absolute_deviation).<br>
Supported value types: *Float*, *Integer*.

Parameters: see [common parameters](#common-parameters).

Example:

    mad(/host/key,1h) #median absolute deviation for the last hour until now

[comment]: # ({/1c1be117-bfb43a27})

[comment]: # ({704d19d6-5882010b})

##### max(/host/key,(sec|#num)<:time shift>) {#max}

The highest value of an item within the defined evaluation period.<br>
Supported value types: *Float*, *Integer*.

Parameters: see [common parameters](#common-parameters).

Example:

    max(/host/key,1h) - min(/host/key,1h) #calculate the difference between the maximum and minimum values within the last hour until now (the delta of values)

[comment]: # ({/704d19d6-5882010b})

[comment]: # ({4a7c4c36-ffd1e499})

##### min(/host/key,(sec|#num)<:time shift>) {#min}

The lowest value of an item within the defined evaluation period.<br>
Supported value types: *Float*, *Integer*.

Parameters: see [common parameters](#common-parameters).

Example:

    max(/host/key,1h) - min(/host/key,1h) #calculate the difference between the maximum and minimum values within the last hour until now (the delta of values)

[comment]: # ({/4a7c4c36-ffd1e499})

[comment]: # ({8aa5d9e9-6b6fb273})

##### skewness(/host/key,(sec|#num)<:time shift>) {#skewness}

The asymmetry of the probability distribution in collected values within the defined evaluation period. See also: [Skewness](https://en.wikipedia.org/wiki/Skewness).<br>
Supported value types: *Float*, *Integer*.

Parameters: see [common parameters](#common-parameters).

Example:

    skewness(/host/key,1h) #the skewness for the last hour until now

[comment]: # ({/8aa5d9e9-6b6fb273})

[comment]: # ({0ac1f90c-11c2ea25})

##### stddevpop(/host/key,(sec|#num)<:time shift>) {#stddevpop}

The population standard deviation in collected values within the defined evaluation period. See also: [Standard deviation](https://en.wikipedia.org/wiki/Standard_deviation).<br>
Supported value types: *Float*, *Integer*.

Parameters: see [common parameters](#common-parameters).

Example:

    stddevpop(/host/key,1h) #the population standard deviation for the last hour until now

[comment]: # ({/0ac1f90c-11c2ea25})

[comment]: # ({3189e9f1-1db77c44})

##### stddevsamp(/host/key,(sec|#num)<:time shift>) {#stddevsamp}

The sample standard deviation in collected values within the defined evaluation period. See also: [Standard deviation](https://en.wikipedia.org/wiki/Standard_deviation).<br>
Supported value types: *Float*, *Integer*.

Parameters: see [common parameters](#common-parameters).

At least two data values are required for this function to work.

Example:

    stddevsamp(/host/key,1h) #the sample standard deviation for the last hour until now

[comment]: # ({/3189e9f1-1db77c44})

[comment]: # ({9678fcd6-0ecf79eb})

##### sum(/host/key,(sec|#num)<:time shift>) {#sum}

The sum of collected values within the defined evaluation period.<br>
Supported value types: *Float*, *Integer*.

Parameters: see [common parameters](#common-parameters).

Example:

    sum(/host/key,1h) #the sum of values for the last hour until [now

[comment]: # ({/9678fcd6-0ecf79eb})

[comment]: # ({9e78c491-6f6556f2})

##### sumofsquares(/host/key,(sec|#num)<:time shift>) {#sumofsquares}

The sum of squares in collected values within the defined evaluation period.<br>
Supported value types: *Float*, *Integer*.

Parameters: see [common parameters](#common-parameters).

Example:

    sumofsquares(/host/key,1h) #the sum of squares for the last hour until [now

[comment]: # ({/9e78c491-6f6556f2})

[comment]: # ({4c190228-d299bd33})

##### varpop(/host/key,(sec|#num)<:time shift>) {#varpop}

The population variance of collected values within the defined evaluation period. See also: [Variance](https://en.wikipedia.org/wiki/Variance).<br>
Supported value types: *Float*, *Integer*.

Parameters: see [common parameters](#common-parameters).

Example:

    varpop(/host/key,1h) #the population variance for the last hour until now

[comment]: # ({/4c190228-d299bd33})

[comment]: # ({1d906325-b1bc041c})

##### varsamp(/host/key,(sec|#num)<:time shift>) {#varsamp}

The sample variance of collected values within the defined evaluation period. See also: [Variance](https://en.wikipedia.org/wiki/Variance).<br>
Supported value types: *Float*, *Integer*.

Parameters: see [common parameters](#common-parameters).

At least two data values are required for this function to work.

Example:

    varsamp(/host/key,1h) #the sample variance for the last hour until now

[comment]: # ({/1d906325-b1bc041c})

[comment]: # ({ba59dd3a-541616fc})

See [all supported functions](/manual/appendix/functions).

[comment]: # ({/ba59dd3a-541616fc})
