[comment]: # attributes: notoc

[comment]: # (terms:  bitand, bitlshift, bitnot, bitor, bitrshift, bitxor )

[comment]: # ({f37511ba-ca911365})
# 2 Bitwise functions

All functions listed here are supported in:

-   [Trigger expressions](/manual/config/triggers/expression)
-   [Calculated items](/manual/config/items/itemtypes/calculated)

The functions are listed without additional information. Click on the function to see the full details.

|Function|Description|
|--|--------|
|[bitand](#bitand)|The value of "bitwise AND" of an item value and mask.|
|[bitlshift](#bitlshift)|The bitwise shift left of an item value.|
|[bitnot](#bitnot)|The value of "bitwise NOT" of an item value.|
|[bitor](#bitor)|The value of "bitwise OR" of an item value and mask.|
|[bitrshift](#bitrshift)|The bitwise shift right of an item value.|
|[bitxor](#bitxor)|The value of "bitwise exclusive OR" of an item value and mask.|

[comment]: # ({/f37511ba-ca911365})

[comment]: # ({e635e2a6-6803853f})
### Function details

Some general notes on function parameters:

-   Function parameters are separated by a comma
-   Expressions are accepted as parameters
-   Optional function parameters (or parameter parts) are indicated by
    `<` `>`

[comment]: # ({/e635e2a6-6803853f})

[comment]: # ({77074d8b-7ffd1e30})

##### bitand(value,mask) {#bitand}

The value of "bitwise AND" of an item value and mask.<br>
Supported value types: *Integer*.

Parameter: 

-   **value** - the value to check;
-   **mask** (mandatory) - a 64-bit unsigned integer (0 - 18446744073709551615).

Although the comparison is done in a bitwise manner, all the values must be supplied and are returned in decimal. For example, checking for the 3rd bit is done by comparing to 4, not 100.

Examples:

    bitand(last(/host/key),12)=8 or bitand(last(/host/key),12)=4 #3rd or 4th bit set, but not both at the same time
    bitand(last(/host/key),20)=16 #3rd bit not set and 5th bit set

[comment]: # ({/77074d8b-7ffd1e30})

[comment]: # ({c2f5e331-31f09935})

##### bitlshift(value,bits to shift) {#bitlshift}

The bitwise shift left of an item value.<br>
Supported value types: *Integer*.

Parameter: 

-   **value** - the value to check;
-   **bits to shift** (mandatory) - the number of bits to shift.

Although the comparison is done in a bitwise manner, all the values must be supplied and are returned in decimal. For example, checking for the 3rd bit is done by comparing to 4, not 100.

[comment]: # ({/c2f5e331-31f09935})

[comment]: # ({6e54f229-d3633e68})

##### bitnot(value) {#bitnot}

The value of "bitwise NOT" of an item value.<br>
Supported value types: *Integer*.

Parameter: 

-   **value** - the value to check.

Although the comparison is done in a bitwise manner, all the values must be supplied and are returned in decimal. For example, checking for the 3rd bit is done by comparing to 4, not 100.

[comment]: # ({/6e54f229-d3633e68})

[comment]: # ({98fab79a-781b9d08})

##### bitor(value,mask) {#bitor}

The value of "bitwise OR" of an item value and mask.<br>
Supported value types: *Integer*.

Parameter: 

-   **value** - the value to check;
-   **mask** (mandatory) - a 64-bit unsigned integer (0 - 18446744073709551615).

Although the comparison is done in a bitwise manner, all the values must be supplied and are returned in decimal. For example, checking for the 3rd bit is done by comparing to 4, not 100.

[comment]: # ({/98fab79a-781b9d08})

[comment]: # ({6ccf7ed7-ade72509})

##### bitrshift(value,bits to shift) {#bitrshift}

The bitwise shift right of an item value.<br>
Supported value types: *Integer*.

Parameter: 

-   **value** - the value to check;
-   **bits to shift** (mandatory) - the number of bits to shift.

Although the comparison is done in a bitwise manner, all the values must be supplied and are returned in decimal. For example, checking for the 3rd bit is done by comparing to 4, not 100.

[comment]: # ({/6ccf7ed7-ade72509})

[comment]: # ({bc33e6c6-51a714f0})

##### bitxor(value,mask) {#bitxor}

The value of "bitwise exclusive OR" of an item value and mask.<br>
Supported value types: *Integer*.

Parameter: 

-   **value** - the value to check;
-   **mask** (mandatory) - a 64-bit unsigned integer (0 - 18446744073709551615).

Although the comparison is done in a bitwise manner, all the values must be supplied and are returned in decimal. For example, checking for the 3rd bit is done by comparing to 4, not 100.

[comment]: # ({/bc33e6c6-51a714f0})

[comment]: # ({ba59dd3a-e800d07b})

See [all supported functions](/manual/appendix/functions).

[comment]: # ({/ba59dd3a-e800d07b})
