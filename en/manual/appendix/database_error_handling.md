[comment]: # ({da6bab56-17a30eba})
# 12 Database error handling

If Zabbix detects that the backend database is not accessible, it will
send a notification message and continue the attempts to connect to the
database. For some database engines, specific error codes are
recognized.

[comment]: # ({/da6bab56-17a30eba})

[comment]: # ({537463f7-537463f7})
#### MySQL

-   CR\_CONN\_HOST\_ERROR
-   CR\_SERVER\_GONE\_ERROR
-   CR\_CONNECTION\_ERROR
-   CR\_SERVER\_LOST
-   CR\_UNKNOWN\_HOST
-   ER\_SERVER\_SHUTDOWN
-   ER\_ACCESS\_DENIED\_ERROR
-   ER\_ILLEGAL\_GRANT\_FOR\_TABLE
-   ER\_TABLEACCESS\_DENIED\_ERROR
-   ER\_UNKNOWN\_ERROR

[comment]: # ({/537463f7-537463f7})
