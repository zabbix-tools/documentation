[comment]: # (tags: macros macro)

[comment]: # ({d428738a-d428738a})
# 2 User macros supported by location

[comment]: # ({/d428738a-d428738a})

[comment]: # ({f0afb8e0-f0afb8e0})
#### Overview

This section contains a list of locations, where
[user-definable](/manual/config/macros/user_macros) macros are
supported.

::: noteclassic
 Only global-level user macros are supported for *Actions*,
*Network discovery*, *Proxies* and all locations listed under *Other
locations* section of this page. In the mentioned locations, host-level
and template-level macros will not be resolved. 
:::

[comment]: # ({/f0afb8e0-f0afb8e0})

[comment]: # ({bb611c5d-7cb4c86b})
#### Actions

In [actions](/manual/config/notifications/action), user macros can be
used in the following fields:

|Location|<|Multiple macros/mix with text^[1](supported_by_location_user#footnotes)^|
|-|------------------------------|----------|
|Trigger-based notifications and commands|<|yes|
|Trigger-based internal notifications|<|yes|
|Problem update notifications|<|yes|
|Service-based notifications and commands|<|yes|
|Service update notifications|<|yes|
|Time period condition|<|no|
|*Operations*|<|<|
| |Default operation step duration|no|
|^|Step duration|no|

[comment]: # ({/bb611c5d-7cb4c86b})

[comment]: # ({74a90176-f30645e0})
#### Hosts/host prototypes

In a [host](/manual/config/hosts/host) and [host
prototype](/manual/vm_monitoring#host_prototypes) configuration, user
macros can be used in the following fields:

|Location|<|Multiple macros/mix with text^[1](supported_by_location_user#footnotes)^|
|-|------------------------------|----------|
|Interface IP/DNS|<|DNS only|
|Interface port|<|no|
|*SNMP v1, v2*|<|<|
| |SNMP community|yes|
|*SNMP v3*|<|<|
| |Context name|yes|
|^|Security name|yes|
|^|Authentication passphrase|yes|
|^|Privacy passphrase|yes|
|*IPMI*|<|<|
| |Username|yes|
|^|Password|yes|
|*Tags*^[2](supported_by_location_user#footnotes)^|<|<|
| |Tag names|yes|
|^|Tag values|yes|

[comment]: # ({/74a90176-f30645e0})

[comment]: # ({76618636-d51563d6})
#### Items / item prototypes

In an [item](/manual/config/items/item) or an [item
prototype](/manual/discovery/low_level_discovery#item_prototypes)
configuration, user macros can be used in the following fields:

|Location|<|Multiple macros/mix with text^[1](supported_by_location_user#footnotes)^|
|-|------------------------------|----------|
|Item key parameters|<|yes|
|Update interval|<|no|
|Custom intervals|<|no|
|History storage period|<|no|
|Trend storage period|<|no|
|Description|<|yes|
|*Calculated item*|<|<|
| |Formula|yes|
|*Database monitor*|<|<|
| |Username|yes|
|^|Password|yes|
|^|SQL query|yes|
|*HTTP agent*|<|<|
| |URL^[3](supported_by_location_user#footnotes)^|yes|
|^|Query fields|yes|
|^|Timeout|no|
|^|Request body|yes|
|^|Headers (names and values)|yes|
|^|Required status codes|yes|
|^|HTTP proxy|yes|
|^|HTTP authentication username|yes|
|^|HTTP authentication password|yes|
|^|SSl certificate file|yes|
|^|SSl key file|yes|
|^|SSl key password|yes|
|^|Allowed hosts|yes|
|*JMX agent*|<|<|
| |JMX endpoint|yes|
|*Script item*|<|<|
| |Parameter names and values|yes|
|*SNMP agent*|<|<|
| |SNMP OID|yes|
|*SSH agent*|<|<|
| |Username|yes|
|^|Public key file|yes|
|^|Private key file|yes|
|^|Password|yes|
|^|Script|yes|
|*TELNET agent*|<|<|
| |Username|yes|
|^|Password|yes|
|^|Script|yes|
|*Zabbix trapper*|<|<|
| |Allowed hosts|yes|
|*Tags*^[2](supported_by_location_user#footnotes)^|<|<|
| |Tag names|yes|
|^|Tag values|yes|
|*Preprocessing*|<|<|
| |Step parameters (including custom scripts)|yes|

[comment]: # ({/76618636-d51563d6})

[comment]: # ({073f5671-747bd0ca})
#### Low-level discovery

In a [low-level discovery
rule](/manual/discovery/low_level_discovery#configuring_low-level_discovery),
user macros can be used in the following fields:

|Location|<|Multiple macros/mix with text^[1](supported_by_location_user#footnotes)^|
|-|------------------------------|----------|
|Key parameters|<|yes|
|Update interval|<|no|
|Custom interval|<|no|
|Keep lost resources period|<|no|
|Description|<|yes|
|*SNMP agent*|<|<|
| |SNMP OID|yes|
|*SSH agent*|<|<|
| |Username|yes|
|^|Public key file|yes|
|^|Private key file|yes|
|^|Password|yes|
|^|Script|yes|
|*TELNET agent*|<|<|
| |Username|yes|
|^|Password|yes|
|^|Script|yes|
|*Zabbix trapper*|<|<|
| |Allowed hosts|yes|
|*Database monitor*|<|<|
| |Username|yes|
|^|Password|yes|
|^|SQL query|yes|
|*JMX agent*|<|<|
| |JMX endpoint|yes|
|*HTTP agent*|<|<|
| |URL^[3](supported_by_location_user#footnotes)^|yes|
|^|Query fields|yes|
|^|Timeout|no|
|^|Request body|yes|
|^|Headers (names and values)|yes|
|^|Required status codes|yes|
|^|HTTP authentication username|yes|
|^|HTTP authentication password|yes|
|*Filters*|<|<|
| |Regular expression|yes|
|*Overrides*|<|<|
| |Filters: regular expression|yes|
|^|Operations: update interval (for item prototypes)|no|
|^|Operations: history storage period (for item prototypes)|no|
|^|Operations: trend storage period (for item prototypes)|no|

[comment]: # ({/073f5671-747bd0ca})

[comment]: # ({7c6ab06a-95f2f372})
#### Network discovery

In a [network discovery rule](/manual/discovery/network_discovery/rule),
user macros can be used in the following fields:

|Location|<|Multiple macros/mix with text^[1](supported_by_location_user#footnotes)^|
|-|------------------------------|----------|
|Update interval|<|no|
|*SNMP v1, v2*|<|<|
| |SNMP community|yes|
|^|SNMP OID|yes|
|*SNMP v3*|<|<|
| |Context name|yes|
|^|Security name|yes|
|^|Authentication passphrase|yes|
|^|Privacy passphrase|yes|
|^|SNMP OID|yes|

[comment]: # ({/7c6ab06a-95f2f372})

[comment]: # ({250b0f56-575d88fc})
#### Proxies

In a [proxy](/manual/distributed_monitoring/proxies#configuration)
configuration, user macros can be used in the following field:

|Location|<|Multiple macros/mix with text^[1](supported_by_location_user#footnotes)^|
|-|------------------------------|----------|
|Interface port (for passive proxy)|<|no|

[comment]: # ({/250b0f56-575d88fc})

[comment]: # ({d5117b03-32b6aef1})
#### Templates

In a [template](/manual/config/templates/template) configuration, user
macros can be used in the following fields:

|Location|<|Multiple macros/mix with text^[1](supported_by_location_user#footnotes)^|
|-|------------------------------|----------|
|*Tags*^[2](supported_by_location_user#footnotes)^|<|<|
| |Tag names|yes|
|^|Tag values|yes|

[comment]: # ({/d5117b03-32b6aef1})

[comment]: # ({0d27e496-c78b04ef})
#### Triggers

In a [trigger](/manual/config/triggers/trigger) configuration, user macros can be used in the following fields:

|Location|<|Multiple macros/mix with text^[1](supported_by_location_user#footnotes)^|
|-|------------------------------|----------|
|Name|<|yes|
|Operational data|<|yes|
|Expression (only in constants and function parameters; secret macros are not supported)|<|yes|
|Tag for matching|<|yes|
|Menu entry name|<|yes|
|Menu entry URL^[3](supported_by_location_user#footnotes)^|<|yes|
|Description|<|yes|
|*Tags*^[2](supported_by_location_user#footnotes)^|<|<|
| |Tag names|yes|
|^|Tag values|yes|

[comment]: # ({/0d27e496-c78b04ef})

[comment]: # ({423f6587-78f1f511})
#### Web scenario

In a [web scenario](/manual/web_monitoring) configuration, user macros
can be used in the following fields:

|Location|<|Multiple macros/mix with text^[1](supported_by_location_user#footnotes)^|
|-|------------------------------|----------|
|Name|<|yes|
|Update interval|<|no|
|Agent|<|yes|
|HTTP proxy|<|yes|
|Variables (values only)|<|yes|
|Headers (names and values)|<|yes|
|*Steps*|<|<|
| |Name|yes|
|^|URL^[3](supported_by_location_user#footnotes)^|yes|
|^|Variables (values only)|yes|
|^|Headers (names and values)|yes|
|^|Timeout|no|
|^|Required string|yes|
|^|Required status codes|no|
|*Authentication*|<|<|
| |User|yes|
|^|Password|yes|
|^|SSL certificate|yes|
|^|SSL key file|yes|
|^|SSL key password|yes|
|*Tags*^[2](supported_by_location_user#footnotes)^|<|<|
| |Tag names|yes|
|^|Tag values|yes|

[comment]: # ({/423f6587-78f1f511})

[comment]: # ({9a835c56-efe84e1d})
#### Other locations

In addition to the locations listed here, user macros can be used in the
following fields:

|Location|<|Multiple macros/mix with text^[1](supported_by_location_user#footnotes)^|
|-|------------------------------|----------|
|Global scripts (URL, script, SSH, Telnet, IPMI), including confirmation text|<|yes|
|Webhooks|<|<|
| |JavaScript script|no|
|^|JavaScript script parameter name|no|
|^|JavaScript script parameter value|yes|
|*Dashboards*|<|<|
| |Description field of *Item value* dashboard widget|yes|
|^|URL^[3](supported_by_location_user#footnotes)^ field of *dynamic URL* dashboard widget|yes|
|*Users → Users → Media*|<|<|
| |When active|no|
|*Administration → General → GUI*|<|<|
| |Working time|no|
|*Administration → General → Connectors*|<|<|
| |URL|yes|
|^|Username|yes|
|^|Password|yes|
|^|Bearer token|yes|
|^|Timeout|no|
|^|HTTP proxy|yes|
|^|SSL certificate file|yes|
|^|SSL key file|yes|
|^|SSL key password|yes|
|*Alerts → Media types → Message templates*|<|<|
| |Subject|yes|
|^|Message|yes|
|*Alerts → Media types → Script*|<|<|
| |Script parameters|yes|

For a complete list of all macros supported in Zabbix, see [supported
macros](/manual/appendix/macros/supported_by_location).

[comment]: # ({/9a835c56-efe84e1d})

[comment]: # ({3f947571-5a370e78})
##### Footnotes

^**1**^ If multiple macros in a field or macros mixed with text are not
supported for the location, a single macro has to fill the whole field.

^**2**^ Macros used in tag names and values are resolved only during event generation process.

^**3**^ URLs that contain a [secret
macro](/manual/config/macros/user_macros#configuration) will not work,
as the macro in them will be resolved as "\*\*\*\*\*\*".

[comment]: # ({/3f947571-5a370e78})
