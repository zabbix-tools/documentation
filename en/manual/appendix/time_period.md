[comment]: # ({e723903a-e723903a})
# 9 Time period syntax

[comment]: # ({/e723903a-e723903a})

[comment]: # ({c717cede-c717cede})
#### Overview

To set a time period, the following format has to be used:

    d-d,hh:mm-hh:mm

where the symbols stand for the following:

|Symbol|Description|
|------|-----------|
|*d*|Day of the week: 1 - Monday, 2 - Tuesday ,... , 7 - Sunday|
|*hh*|Hours: 00-24|
|*mm*|Minutes: 00-59|

You can specify more than one time period using a semicolon (;)
separator:

    d-d,hh:mm-hh:mm;d-d,hh:mm-hh:mm...

Leaving the time period empty equals 01-07,00:00-24:00, which is the
default value.

::: noteimportant
The upper limit of a time period is not included.
Thus, if you specify 09:00-18:00 the last second included in the time
period is 17:59:59.
:::

[comment]: # ({/c717cede-c717cede})

[comment]: # ({a352b97d-a352b97d})
#### Examples

Working hours. Monday - Friday from 9:00 till 18:00:

    1-5,09:00-18:00

Working hours plus weekend. Monday - Friday from 9:00 till 18:00 and
Saturday, Sunday from 10:00 till 16:00:

    1-5,09:00-18:00;6-7,10:00-16:00

[comment]: # ({/a352b97d-a352b97d})
