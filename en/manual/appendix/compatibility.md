[comment]: # ({de5a4695-d25f4c9e})
# 11 Version compatibility

[comment]: # ({/de5a4695-d25f4c9e})

[comment]: # ({80c7f124-eefa9a73})
#### Supported agents

To be compatible with Zabbix 7.0, Zabbix agent must not be older than
version 1.4 and must not be newer than 7.0.

You may need to review the configuration of older agents as some
parameters have changed, for example, parameters related to
[logging](https://www.zabbix.com/documentation/3.0/manual/installation/upgrade_notes_300#changes_in_configuration_parameters_related_to_logging)
for versions before 3.0.

To take full advantage of the latest functionality, metrics, improved performance and
reduced memory usage, use the latest supported agent.

[comment]: # ({/80c7f124-eefa9a73})

[comment]: # ({e140f238-1a5df00a})
#### Supported agents 2

Older Zabbix agents 2 from version 4.4 onwards are compatible with
Zabbix 7.0; Zabbix agent 2 must not be newer than 7.0.

Note that when using Zabbix agent 2 versions 4.4 and 5.0, the default
interval of 10 minutes is used for refreshing unsupported items.

To take full advantage of the latest functionality, metrics, improved performance and
reduced memory usage, use the latest supported agent 2.

[comment]: # ({/e140f238-1a5df00a})

[comment]: # ({a0ce63d9-5be689b0})
#### Supported Zabbix proxies

To be fully compatible with Zabbix 7.0, the proxies must be of the same major version; thus only Zabbix 7.0.x proxies
are fully compatible with Zabbix 7.0.x server. However, outdated proxies are also supported, although only partially.

[comment]: # ({/a0ce63d9-5be689b0})

[comment]: # ({602cba0f-78c6278a})

In relation to Zabbix server, proxies can be:

-   *Current* (proxy and server have the same major version);
-   *Outdated* (proxy version is older than server version, but is partially supported);
-   *Unsupported* (proxy version is older than server previous LTS release version *or* proxy version is newer than server major version).

Examples:

|Server version|*Current* proxy version|*Outdated* proxy version|*Unsupported* proxy version|
|--------------|-----------------------|------------------------|---------------------------|
|6.4|6.4|6.0, 6.2|Older than 6.0; newer than 6.4|
|7.0|7.0|6.0, 6.2, 6.4|Older than 6.0; newer than 7.0|
|7.2|7.2|7.0|Older than 7.0; newer than 7.2|

[comment]: # ({/602cba0f-78c6278a})

[comment]: # ({18792cf0-9f9fce98})

Functionality supported by proxies:

|Proxy version|Data update|Configuration update|Tasks|
|--|--|--|----|
|*Current*|Yes|Yes|Yes|
|*Outdated*|Yes|No|[Remote commands](/manual/config/notifications/action/operation/remote_command) (e.g., shell scripts);<br>Immediate item value checks (i.e., *[Execute now](/manual/config/items/check_now)*);<br>Note: Preprocessing [tests with a real value](/manual/config/items/preprocessing#testing-real-value) are not supported.|
|*Unsupported*|No|No|No|

[comment]: # ({/18792cf0-9f9fce98})

[comment]: # ({20b1eccd-f9a2762b})

Warnings about using incompatible Zabbix daemon versions are logged.

[comment]: # ({/20b1eccd-f9a2762b})

[comment]: # ({8a4c7d5a-5f9e54ca})
#### Supported XML files

XML files not older than version 1.8 are supported for import in Zabbix
7.0.

::: noteimportant
In the XML export format, trigger dependencies are
stored by name only. If there are several triggers with the same name
(for example, having different severities and expressions) that have a
dependency defined between them, it is not possible to import them. Such
dependencies must be manually removed from the XML file and re-added
after import.
:::

[comment]: # ({/8a4c7d5a-5f9e54ca})
