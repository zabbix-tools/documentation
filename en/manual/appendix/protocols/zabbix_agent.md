[comment]: # ({a175d7cd-a175d7cd})
# 2 Zabbix agent protocol

Please refer to [Passive and active agent
checks](/manual/appendix/items/activepassive) page for more information.

[comment]: # ({/a175d7cd-a175d7cd})
