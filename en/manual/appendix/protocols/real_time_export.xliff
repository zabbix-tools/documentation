<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="en" datatype="plaintext" original="manual/appendix/protocols/real_time_export.md">
    <body>
      <trans-unit id="cb9df6d6" xml:space="preserve">
        <source># 7 Newline-delimited JSON export protocol

This section presents details of the export protocol in a
newline-delimited JSON format, used in:

-   [data export to files](/manual/config/export/files)
-   [streaming to external systems](/manual/config/export/streaming)

The following can be exported:

-   [trigger events](#trigger_events)
-   [item values](#item_values)
-   [trends](#trends) (export to files only)

All files have a .ndjson extension. Each line of the export file is a
JSON object.</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/appendix/protocols/real_time_export.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="bc637674" xml:space="preserve">
        <source>#### Trigger events

The following information is exported for a problem event:

|Field|&lt;|&lt;|Type|Description|
|-|-|----------|----------|------------------------------|
|*clock*|&lt;|&lt;|number|Number of seconds since Epoch to the moment when problem was detected (integer part).|
|*ns*|&lt;|&lt;|number|Number of nanoseconds to be added to `clock` to get a precise problem detection time.|
|*value*|&lt;|&lt;|number|1 (always).|
|*eventid*|&lt;|&lt;|number|Problem event ID.|
|*name*|&lt;|&lt;|string|Problem event name.|
|*severity*|&lt;|&lt;|number|Problem event severity (0 - Not classified, 1 - Information, 2 - Warning, 3 - Average, 4 - High, 5 - Disaster).|
|*hosts*|&lt;|&lt;|array|List of hosts involved in the trigger expression; there should be at least one element in array.|
| |\-|&lt;|object|&lt;|
|^| |*host*|string|Host name.|
|^|^|*name*|string|Visible host name.|
|*groups*|&lt;|&lt;|array|List of host groups of all hosts involved in the trigger expression; there should be at least one element in array.|
| |\-|&lt;|string|Host group name.|
|*tags*|&lt;|&lt;|array|List of problem tags (can be empty).|
| |\-|&lt;|object|&lt;|
|^| |*tag*|string|Tag name.|
|^|^|*value*|string|Tag value (can be empty).|

The following information is exported for a recovery event:

|Field|Type|Description|
|--|--|------|
|*clock*|number|Number of seconds since Epoch to the moment when problem was resolved (integer part).|
|*ns*|number|Number of nanoseconds to be added to `clock` to get a precise problem resolution time.|
|*value*|number|0 (always).|
|*eventid*|number|Recovery event ID.|
|*p\_eventid*|number|Problem event ID.|</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/appendix/protocols/real_time_export.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="9eccc413" xml:space="preserve">
        <source>##### Examples

Problem:

    {"clock":1519304285,"ns":123456789,"value":1,"name":"Either Zabbix agent is unreachable on Host B or pollers are too busy on Zabbix Server","severity":3,"eventid":42, "hosts":[{"host":"Host B", "name":"Host B visible"},{"host":"Zabbix Server","name":"Zabbix Server visible"}],"groups":["Group X","Group Y","Group Z","Zabbix servers"],"tags":[{"tag":"availability","value":""},{"tag":"data center","value":"Riga"}]}

Recovery:

    {"clock":1519304345,"ns":987654321,"value":0,"eventid":43,"p_eventid":42}

Problem (multiple problem event generation):

    {"clock":1519304286,"ns":123456789,"value":1,"eventid":43,"name":"Either Zabbix agent is unreachable on Host B or pollers are too busy on Zabbix Server","severity":3,"hosts":[{"host":"Host B", "name":"Host B visible"},{"host":"Zabbix Server","name":"Zabbix Server visible"}],"groups":["Group X","Group Y","Group Z","Zabbix servers"],"tags":[{"tag":"availability","value":""},{"tag":"data center","value":"Riga"}]}

    {"clock":1519304286,"ns":123456789,"value":1,"eventid":43,"name":"Either Zabbix agent is unreachable on Host B or pollers are too busy on Zabbix Server","severity":3,"hosts":[{"host":"Host B", "name":"Host B visible"},{"host":"Zabbix Server","name":"Zabbix Server visible"}],"groups":["Group X","Group Y","Group Z","Zabbix servers"],"tags":[{"tag":"availability","value":""},{"tag":"data center","value":"Riga"}]}

Recovery:

    {"clock":1519304346,"ns":987654321,"value":0,"eventid":44,"p_eventid":43}

    {"clock":1519304346,"ns":987654321,"value":0,"eventid":44,"p_eventid":42}</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/appendix/protocols/real_time_export.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="9faee484" xml:space="preserve">
        <source>#### Item values

The following information is exported for a collected item value:

|Field|&lt;|&lt;|Type|Description|
|-|-|---------|---------|---------------------------|
|*host*|&lt;|&lt;|object|Host name of the item host.|
| |host|&lt;|string|Host name.|
|^|name|&lt;|string|Visible host name.|
|*groups*|&lt;|&lt;|array|List of host groups of the item host; there should be at least one element in array.|
| |\-|&lt;|string|Host group name.|
|*item_tags*|&lt;|&lt;|array|List of item tags (can be empty).|
| |\-|&lt;|object|&lt;|
|^| |*tag*|string|Tag name.|
|^|^|*value*|string|Tag value (can be empty).|
|*itemid*|&lt;|&lt;|number|Item ID.|
|*name*|&lt;|&lt;|string|Visible item name.|
|*clock*|&lt;|&lt;|number|Number of seconds since Epoch to the moment when value was collected (integer part).|
|*ns*|&lt;|&lt;|number|Number of nanoseconds to be added to `clock` to get a precise value collection time.|
|*timestamp*&lt;br&gt;(*Log* only)|&lt;|&lt;|number|0 if not available.|
|*source*&lt;br&gt;(*Log* only)|&lt;|&lt;|string|Empty string if not available.|
|*severity*&lt;br&gt;(*Log* only)|&lt;|&lt;|number|0 if not available.|
|*eventid*&lt;br&gt;(*Log* only)|&lt;|&lt;|number|0 if not available.|
|*value*|&lt;|&lt;|number (for numeric items) or&lt;br&gt;string (for text items)|Collected item value.|
|*type*|&lt;|&lt;|number|Collected value type:&lt;br&gt;0 - numeric float, 1 - character, 2 - log, 3 - numeric unsigned, 4 - text|</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/appendix/protocols/real_time_export.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="683a131b" xml:space="preserve">
        <source>##### Examples

Numeric (unsigned) value:

    {"host":{"host":"Host B","name":"Host B visible"},"groups":["Group X","Group Y","Group Z"],"item_tags":[{"tag":"foo","value":"test"}],"itemid":3,"name":"Agent availability","clock":1519304285,"ns":123456789,"value":1,"type":3}

Numeric (float) value:

    {"host":{"host":"Host B","name":"Host B visible"},"groups":["Group X","Group Y","Group Z"],"item_tags":[{"tag":"foo","value":"test"}],"itemid":4,"name":"CPU Load","clock":1519304285,"ns":123456789,"value":0.1,"type":0}

Character, text value:

    {"host":{"host":"Host B","name":"Host B visible"},"groups":["Group X","Group Y","Group Z"],"item_tags":[{"tag":"foo","value":"test"}],"itemid":2,"name":"Agent version","clock":1519304285,"ns":123456789,"value":"3.4.4","type":4}

Log value:

    {"host":{"host":"Host A","name":"Host A visible"},"groups":["Group X","Group Y","Group Z"],"item_tags":[{"tag":"foo","value":"test"}],"itemid":1,"name":"Messages in log file","clock":1519304285,"ns":123456789,"timestamp":1519304285,"source":"","severity":0,"eventid":0,"value":"log file message","type":2}</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/appendix/protocols/real_time_export.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="f534dda9" xml:space="preserve">
        <source>#### Trends

The following information is exported for a calculated trend value:

|Field|&lt;|&lt;|Type|Description|
|-|-|---------|---------|---------------------------|
|*host*|&lt;|&lt;|object|Host name of the item host.|
| |host|&lt;|string|Host name.|
|^|name|&lt;|string|Visible host name.|
|*groups*|&lt;|&lt;|array|List of host groups of the item host; there should be at least one element in array.|
| |\-|&lt;|string|Host group name.|
|*item_tags*|&lt;|&lt;|array|List of item tags (can be empty).|
| |\-|&lt;|object|&lt;|
|^| |*tag*|string|Tag name.|
|^|^|*value*|string|Tag value (can be empty).|
|*itemid*|&lt;|&lt;|number|Item ID.|
|*name*|&lt;|&lt;|string|Visible item name.|
|*clock*|&lt;|&lt;|number|Number of seconds since Epoch to the moment when value was collected (integer part).|
|*count*|&lt;|&lt;|number|Number of values collected for a given hour.|
|*min*|&lt;|&lt;|number|Minimum item value for a given hour.|
|*avg*|&lt;|&lt;|number|Average item value for a given hour.|
|*max*|&lt;|&lt;|number|Maximum item value for a given hour.|
|*type*|&lt;|&lt;|number|Value type:&lt;br&gt;0 - numeric float, 3 - numeric unsigned|</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/appendix/protocols/real_time_export.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
      <trans-unit id="8606813d" xml:space="preserve">
        <source>##### Examples

Numeric (unsigned) value:

    {"host":{"host":"Host B","name":"Host B visible"},"groups":["Group X","Group Y","Group Z"],"item_tags":[{"tag":"foo","value":"test"}],"itemid":3,"name":"Agent availability","clock":1519311600,"count":60,"min":1,"avg":1,"max":1,"type":3}

Numeric (float) value:

    {"host":{"host":"Host B","name":"Host B visible"},"groups":["Group X","Group Y","Group Z"],"item_tags":[{"tag":"foo","value":"test"}],"itemid":4,"name":"CPU Load","clock":1519311600,"count":60,"min":0.01,"avg":0.15,"max":1.5,"type":0}</source>
        <note>https://git.zabbix.com/projects/WEB/repos/documentation/browse/en/manual/appendix/protocols/real_time_export.md?at=refs%2Fheads%2Frelease%2F7.0</note>
      </trans-unit>
    </body>
  </file>
</xliff>
