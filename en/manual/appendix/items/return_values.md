[comment]: # ({bbf44493-4427b720})
# 17 Return values

[comment]: # ({/bbf44493-4427b720})

[comment]: # ({14f53205-68fd5d61})
### Overview

This section provides return value details for some [Zabbix agent](/manual/config/items/itemtypes/zabbix_agent) items.

[comment]: # ({/14f53205-68fd5d61})

[comment]: # ({4d8c4440-2fa9a225})
### system.sw.packages.get

The output of this item is an array of objects each containing the following keys:

-   **name** - package name
-   **manager** - package manager that reported this data (`rpm`, `dpkg`, `pacman`, `pkgtool`, or `portage`)
-   **version** - package version
-   **size** - uncompressed package size (in bytes)
-   **arch** - package architecture
-   **buildtime** - an object with 2 entries:
    -   **timestamp** - UNIX timestamp when the package was built (if not available, set to 0)
    -   **value** - human readable date and time when the package was built (if not available, set to empty string)
-   **installtime** - an object with 2 entries:
    -   **timestamp** - UNIX timestamp when the package was installed (if not available, set to 0)
    -   **value** - human readable date and time when the package was installed (if not available, set to empty string)

For example:

``` {.java}
[
    {
        "name": "util-linux-core",
        "manager": "rpm",
        "version": "2.37.4-3.el9",
        "size": 1296335,
        "arch": "x86_64",
        "buildtime": {
            "timestamp" : 1653552239,
            "value" : "Sep 20 01:39:40 2021 UTC"
        },
        "installtime": {
            "timestamp" : 1660780885,
            "value" : "Aug 18 00:01:25 2022 UTC"
        }
    },
    {
        "name": "xfonts-base",
        "manager": "dpkg",
        "version": "1:1.0.5",
        "size": 7337984,
        "arch": "all",
        "buildtime": {
            "timestamp": 0,
            "value": ""
        },
        "installtime": {
            "timestamp": 0,
            "value": ""
        }
    }
]
```

[comment]: # ({/4d8c4440-2fa9a225})
