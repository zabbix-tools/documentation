[comment]: # ({6f5dcc8c-6f5dcc8c})
# 6 Large file support

Large file support, often abbreviated to LFS, is the term applied to the
ability to work with files larger than 2 GB on 32-bit operating systems.
Since Zabbix 2.0 support for large files has been added. This change
affects at least [log file
monitoring](/manual/config/items/itemtypes/log_items) and all
[vfs.file.\*
items](/manual/config/items/itemtypes/zabbix_agent#supported_item_keys).
Large file support depends on the capabilities of a system at Zabbix
compilation time, but is completely disabled on a 32-bit Solaris due to
its incompatibility with procfs and swapctl.

[comment]: # ({/6f5dcc8c-6f5dcc8c})
