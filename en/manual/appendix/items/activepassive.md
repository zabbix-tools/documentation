[comment]: # ({a6d31bd3-a6d31bd3})
# 2 Passive and active agent checks

[comment]: # ({/a6d31bd3-a6d31bd3})

[comment]: # ({df72fd3f-7bd0dd61})
#### Overview

This section provides details on passive and active checks performed by
[Zabbix agent](/manual/config/items/itemtypes/zabbix_agent).

Zabbix uses a JSON based communication protocol for communicating with
Zabbix agent.

See also: [Zabbix agent 2](/manual/appendix/protocols/zabbix_agent2) 
protocol details.

[comment]: # ({/df72fd3f-7bd0dd61})

[comment]: # ({e6db8dd6-e6db8dd6})
#### Passive checks

A passive check is a simple data request. Zabbix server or proxy asks
for some data (for example, CPU load) and Zabbix agent sends back the
result to the server.

**Server request**

For definition of header and data length please refer to [protocol
details](/manual/appendix/protocols/header_datalen).

    <item key>

**Agent response**

    <DATA>[\0<ERROR>]

Above, the part in square brackets is optional and is only sent for not
supported items.

For example, for supported items:

1.  Server opens a TCP connection
2.  Server sends **<HEADER><DATALEN>agent.ping**
3.  Agent reads the request and responds with
    **<HEADER><DATALEN>1**
4.  Server processes data to get the value, '1' in our case
5.  TCP connection is closed

For not supported items:

1.  Server opens a TCP connection
2.  Server sends **<HEADER><DATALEN>vfs.fs.size\[/nono\]**
3.  Agent reads the request and responds with
    **<HEADER><DATALEN>ZBX\_NOTSUPPORTED\\0Cannot obtain
    filesystem information: \[2\] No such file or directory**
4.  Server processes data, changes item state to not supported with the
    specified error message
5.  TCP connection is closed

[comment]: # ({/e6db8dd6-e6db8dd6})

[comment]: # ({2e0ae23e-ecdd6e8e})
#### Active checks

Active checks require more complex processing.
The agent must first retrieve from the server/proxy a list of items and/or [remote commands](/manual/config/notifications/action/operation/remote_command) for independent processing.

The servers/proxies to get the active checks from are listed in the 'ServerActive' parameter of the agent [configuration file](/manual/appendix/config/zabbix_agentd).
The frequency of asking for these checks is set by the 'RefreshActiveChecks' parameter in the same configuration file.
However, if refreshing active checks fails, it is retried after hardcoded 60 seconds.

[comment]: # ({/2e0ae23e-ecdd6e8e})

[comment]: # ({e520c471-4c14cd28})
::: notetip
Since Zabbix 6.4 the agent (in active mode) no longer receives from the server/proxy a full copy of the configuration once every two minutes (default).
Instead, in order to decrease network traffic and resources usage, an incremental configuration sync is performed every 5 seconds (default) upon which the server/proxy provides a full copy of the configuration **only** if the agent has not yet received it, or something has changed in host configuration, global macros or global regular expressions.
:::

[comment]: # ({/e520c471-4c14cd28})

[comment]: # ({63166bda-2f80420a})
The agent then periodically sends the new values to the server(s).
If the agent received any [remote commands](/manual/config/notifications/action/operation/remote_command) to execute, the execution result will also be sent.
Note that remote command execution on an active agent is supported since Zabbix agent 7.0.

::: notetip
If an agent is behind the firewall you might consider
using only Active checks because in this case you wouldn't need to
modify the firewall to allow initial incoming connections.
:::

[comment]: # ({/63166bda-2f80420a})

[comment]: # ({e8da2e40-d898e135})
##### Getting the list of items

**Agent request**

The active checks request is used to obtain the active checks to be processed by agent. 
This request is sent by the agent upon start and then with [RefreshActiveChecks](/manual/appendix/config/zabbix_agentd) intervals.

```json
{
  "request": "active checks",
  "host": "Zabbix server",
  "host_metadata": "mysql,nginx",
  "hostinterface": "zabbix.server.lan",
  "ip": "159.168.1.1",
  "port": 12050,
  "config_revision": 1,
  "session": "e3dcbd9ace2c9694e1d7bbd030eeef6e"
}
```

| Field | Type | Mandatory | Value |
|-|-|-|--------|
| request | _string_ | yes | `active checks` |
| host | _string_ | yes | Host name. |
| host_metadata | _string_ | no | The configuration parameter HostMetadata or HostMetadataItem metric value. |
| hostinterface | _string_ | no | The configuration parameter HostInterface or HostInterfaceItem metric value. |
| ip | _string_ | no | The configuration parameter ListenIP first IP if set. |
| port | _number_ | no | The configuration parameter ListenPort value if set and not default agent listening port. |
| config_revision | _number_ | no | Configuration identifier for [incremental configuration sync](#active-checks). |
| session | _string_ | no | Session identifier for [incremental configuration sync](#active-checks). |
  
**Server response**

The active checks response is sent by the server back to agent after processing the active checks request.
  
```json
{
  "response": "success",
  "data": [
    {
      "key": "log[/home/zabbix/logs/zabbix_agentd.log]",
      "key_orig": "log[/home/zabbix/logs/zabbix_agentd.log]",
      "itemid": 1234,
      "delay": "30s",
      "lastlogsize": 0,
      "mtime": 0
    },
    {
      "key": "agent.version",
      "key_orig": "agent.version",
      "itemid": 5678,
      "delay": "10m",
      "lastlogsize": 0,
      "mtime": 0
    }
  ],
  "commands": [
    {
      "command": "df -h --output=source,size / | awk 'NR>1 {print $2}'",
      "id": 1324,
      "wait": 1
    }
  ],
  "config_revision": 2
}

```

| Field |<| Type | Mandatory | Value |
|-|------|--|-|-----------------------|
| response |<| _string_ | yes | `success` \| `failed` |
| info |<| _string_ | no | Error information in the case of failure. |
| data |<| _array of objects_ | no | Active check items. Omitted if host configuration is unchanged. |
| | key | _string_ | no | Item key with expanded macros. |
|^| key_orig | _string_ | no | Item key without expanded macros. |
|^| itemid | _number_ | no | Item identifier. |
|^| delay | _string_ | no | Item update interval. |
|^| lastlogsize | _number_ | no | Item lastlogsize. |
|^| mtime | _number_ | no | Item mtime. |
| refresh_unsupported |<| _number_ | no | Unsupported item refresh interval. |
| regexp |<| _array of objects_ | no | Global regular expressions. |
| | name | _string_ | no | Global regular expression name. |
|^| expression | _string_ | no | Global regular expression. |
|^| expression_type | _number_ | no | Global regular expression type. |
|^| exp_delimiter | _string_ | no | Global regular expression delimiter. |
|^| case_sensitive | _number_ | no | Global regular expression case sensitivity setting. |
| commands |<| _array of objects_ | no | Remote commands to execute. Included if remote command execution has been triggered by an action [operation](/manual/config/notifications/action/operation#operations) or manual [script](/manual/web_interface/frontend_sections/alerts/scripts) execution. Note that remote command execution on an active agent is supported since Zabbix agent 7.0. Older active agents will ignore any remote commands included in the active checks server response. |
| | command | _string_ | no | Remote command. |
|^| id | _number_ | no | Remote command identifier. |
|^| wait | _number_ | no | Remote command mode of execution ("0" (nowait) for commands from action [operations](/manual/config/notifications/action/operation#operations); "1" (wait) for commands from manual [script](/manual/web_interface/frontend_sections/alerts/scripts) execution). |
| config_revision | < | _number_ | no | Configuration identifier for [incremental configuration sync](#active-checks). Omitted if host configuration is unchanged. Incremented if host configuration is changed. |
  
The server must respond with success.

For example:

1.  Agent opens a TCP connection
2.  Agent asks for the list of checks
3.  Server responds with a list of items and remote commands to execute
4.  Agent parses the response
5.  TCP connection is closed
6.  Agent starts periodical collection of data and executes remote commands (supported since Zabbix agent 7.0)

::: noteimportant
Note that (sensitive) configuration data may
become available to parties having access to the Zabbix server trapper
port when using an active check. This is possible because anyone may
pretend to be an active agent and request item configuration data;
authentication does not take place unless you use
[encryption](/manual/encryption) options.
:::

[comment]: # ({/e8da2e40-d898e135})

[comment]: # ({1010f84a-8c5ecfe1})
##### Sending in collected data

**Agent sends**

The agent data request contains the gathered item values and the values for executed remote commands (if any).
  
```json
{
  "request": "agent data",
  "data": [
    {
      "host": "Zabbix server",
      "key": "agent.version",
      "value": "2.4.0",
      "clock": 1400675595,
      "ns": 76808644
    },
    {
      "host": "Zabbix server",
      "key": "log[/home/zabbix/logs/zabbix_agentd.log]",
      "lastlogsize": 112,
      "value": " 19845:20140621:141708.521 Starting Zabbix Agent [<hostname>]. Zabbix 2.4.0 (revision 50000).",
      "clock": 1400675595,
      "ns": 77053975
    }
  ],
  "commands": [
    {
      "id": 1324,
      "value": "16G"
    }
  ],
  "session": "1234456akdsjhfoui"
}
```

| Field |<| Type | Mandatory | Value |
|-|------|--|-|-----------------------|
| request |<| _string_ | yes | `agent data` |
| session |<| _string_ | yes | Unique session identifier generated each time when agent is started. |
| data |<| _array of objects_ | yes | Item values. |
| | id | _number_ | yes | The value identifier (incremental counter used for checking duplicated values in the case of network problems). |
|^| host | _string_ | yes | Host name. |
|^| key | _string_ | yes | The item key. |
|^| value | _string_ | no | The item value. |
|^| lastlogsize | _number_ | no | The item lastlogsize. |
|^| mtime | _number_ | no | The item mtime. |
|^| state | _number_ | no | The item state. |
|^| source | _string_ | no | The value event log source. |
|^| eventid | _number_ | no | The value event log eventid. |
|^| severity | _number_ | no | The value event log severity. |
|^| timestamp | _number_ | no | The value event log timestamp. |
|^| clock | _number_ | yes | The value timestamp (seconds since Epoch). |
|^| ns | _number_ | yes | The value timestamp nanoseconds. |
| commands |<| _array of objects_ | no | Remote commands execution result. Note that remote command execution on an active agent is supported since Zabbix agent 7.0. Older active agents will ignore any remote commands included in the active checks server response. |
| | id | _number_ | no | Remote command identifier. |
|^| value | _string_ | no | Remote command execution result if the execution was successful. |
|^| error | _string_ | no | Remote command execution error message if the execution failed. |

A virtual ID is assigned to each value. Value ID is a simple ascending
counter, unique within one data session (identified by the session
token). This ID is used to discard duplicate values that might be sent
in poor connectivity environments.

**Server response**

The agent data response is sent by the server back to agent after processing the agent data request.
  
```json
{
  "response": "success",
  "info": "processed: 2; failed: 0; total: 2; seconds spent: 0.003534"
}
```

| Field | Type | Mandatory | Value |
|-|-|-|--------|
| response | _string_ | yes | `success` \| `failed` |
| info | _string_ | yes | Item processing results. |
  
::: noteimportant
If sending of some values fails on the server (for
example, because host or item has been disabled or deleted), agent will
not retry sending of those values.
:::

For example:

1.  Agent opens a TCP connection
2.  Agent sends a list of values
3.  Server processes the data and sends the status back
4.  TCP connection is closed

Note how in the example above the not supported status for
vfs.fs.size\[/nono\] is indicated by the "state" value of 1 and the
error message in "value" property.

::: noteimportant
Error message will be trimmed to 2048 symbols on server side.
:::

[comment]: # ({/1010f84a-8c5ecfe1})

[comment]: # ({34a853e9-ee2b7a2a})

##### Heartbeat message

The heartbeat message is sent by an active agent to Zabbix server/proxy 
every HeartbeatFrequency seconds (configured in the Zabbix agent 
[configuration file](/manual/appendix/config/zabbix_agentd)). 

It is used to monitor the availability of active checks.

```json
{
  "request": "active check heartbeat",
  "host": "Zabbix server",
  "heartbeat_freq": 60
}
```

| Field | Type | Mandatory | Value |
|-|-|-|--------|
| request | _string_ | yes | `active check heartbeat` |
| host | _string_ | yes | The host name. |
| heartbeat_freq | _number_ | yes | The agent heartbeat frequency (HeartbeatFrequency configuration parameter). |

[comment]: # ({/34a853e9-ee2b7a2a})

[comment]: # ({e66043c4-e66043c4})
#### Older XML protocol

::: noteclassic
Zabbix will take up to 16 MB of XML Base64-encoded data, but
a single decoded value should be no longer than 64 KB otherwise it will
be truncated to 64 KB while decoding.
:::

[comment]: # ({/e66043c4-e66043c4})
