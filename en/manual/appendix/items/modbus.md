[comment]: # ({582125b9-7f7de2a1})
# 15 modbus.get parameters

[comment]: # ({/582125b9-7f7de2a1})

[comment]: # ({e6f6ba81-e6f6ba81})
#### Overview

The table below presents details of the modbus.get\[\]
[item](/manual/config/items/itemtypes/zabbix_agent) parameters.

[comment]: # ({/e6f6ba81-e6f6ba81})

[comment]: # ({5b05c74f-6dc501ad})
#### Parameters

|Parameter|Description|Defaults|Example|
|--|------|--|---|
|*endpoint*|Protocol and address of the endpoint, defined as `protocol://connection_string`<br><br>Possible protocol values: *rtu*, *ascii* (Agent 2 only), *tcp*<br><br>Connection string format:<br><br>with *tcp* - `address:port`<br>with serial line: *rtu*, *ascii* - `port_name:speed:params`<br>where<br>'speed' - 1200, 9600 etc<br>'params' - data bits (5,6,7 or 8), parity (n,e or o for none/even/odd), stop bits (1 or 2)|protocol: none<br><br>*rtu/ascii* protocol:<br>port\_name: none<br>speed: 115200<br>params: 8n1<br><br>*tcp* protocol:<br>address: none<br>port: 502|tcp://192.168.6.1:511<br>tcp://192.168.6.2<br>tcp://\[::1\]:511<br>tcp://::1<br>tcp://localhost:511<br>tcp://localhost<br>rtu://COM1:9600:8n<br>ascii://COM2:1200:7o2<br>rtu://ttyS0:9600<br>ascii://ttyS1|
|*slave id*|Modbus address of the device it is intended for (1 to 247), see [MODBUS Messaging Implementation Guide](https://modbus.org/docs/Modbus_Messaging_Implementation_Guide_V1_0b.pdf) (page 23)<br><br>tcp device (not GW) will ignore the field|serial: 1<br><br>tcp: 255 (0xFF)|2|
|*function*|Empty or value of a supported function:<br><br>1 - Read Coil,<br>2 - Read Discrete Input,<br>3 - Read Holding Registers,<br>4 - Read Input Registers|empty|3|
|*address*|Address of the first registry, coil or input.<br><br>If 'function' is empty, then 'address' should be in range for:<br>Coil - 00001 - 09999<br>Discrete input - 10001 - 19999<br>Input register - 30001 - 39999<br>Holding register - 40001 - 49999<br><br>If 'function' is not empty, the 'address' field will be from 0 till 65535 and used without modification (PDU)|empty function: 00001<br><br>non-empty function: 0|9999|
|*count*|Count of sequenced 'type' which will be read from device, where:<br><br>for Coil or Discrete input the 'type' = 1 bit<br>for other cases: (count\*type)/2 = real count of registers for reading<br>If 'offset' is not 0, the value will be added to 'real count'<br>Acceptable range for 'real count' is 1:65535|1|2|
|*type*|Data type:<br><br>for Read Coil and Read Discrete Input - *bit*<br><br>for Read Holding Registers and Read Input Registers:<br>*int8* - 8bit<br>*uint8* - 8bit (unsigned)<br>*int16* - 16bit<br>*uint16* - 16bit (unsigned)<br>*int32* - 32bit<br>*uint32* - 32bit (unsigned)<br>*float* - 32bit<br>*uint64* - 64bit (unsigned)<br>*double* - 64bit|bit<br>uint16|uint64|
|*endianness*|Endianness type:<br>*be* - Big Endian<br>*le* - Little Endian<br>*mbe* - Mid-Big Endian<br>*mle* - Mid-Little Endian<br><br>Limitations:<br>for 1 bit - be<br>for 8 bits - be,le<br>for 16 bits - be,le|be|le|
|*offset*|Number of registers, starting from 'address', the result of which will be discarded.<br><br>The size of each register is 16bit (needed to support equipment that does not support random read access).|0|4|

[comment]: # ({/5b05c74f-6dc501ad})
