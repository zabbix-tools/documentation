[comment]: # ({e8dfa374-e8dfa374})
# 1 vm.memory.size parameters

[comment]: # ({/e8dfa374-e8dfa374})

[comment]: # ({0863fe63-0863fe63})
#### Overview

This section provides some parameter details for the
[vm.memory.size\[<mode>](/manual/config/items/itemtypes/zabbix_agent)\]
agent item.

[comment]: # ({/0863fe63-0863fe63})

[comment]: # ({9a92e4df-4ea4e379})
#### Parameters

The following parameters are available for this item:

-   **active** - memory currently in use or very recently used, and so
    it is in RAM
-   **anon** - memory not associated with a file (cannot be re-read from
    it)
-   **available** - available memory, calculated differently depending
    on the platform (see the table below)
-   **buffers** - cache for things like file system metadata
-   **cached** - cache for various things
-   **exec** - executable code, typically from a (program) file
-   **file** - cache for contents of recently accessed files
-   **free** - memory that is readily available to any entity requesting
    memory
-   **inactive** - memory that is marked as not used
-   **pavailable** - 'available' memory as percentage of 'total'
    (calculated as `available`/`total`\*100)
-   **pinned** - same as 'wired'
-   **pused** - 'used' memory as percentage of 'total' (calculated as
    `used`/`total`\*100)
-   **shared** - memory that may be simultaneously accessed by multiple
    processes
-   **slab** - total amount of memory used by the kernel to cache data
    structures for its own use
-   **total** - total physical memory available
-   **used** - used memory, calculated differently depending on the
    platform (see the table below)
-   **wired** - memory that is marked to always stay in RAM. It is never
    moved to disk.

::: notewarning
Some of these parameters are platform-specific and
might not be available on your platform. See [Zabbix agent items](/manual/config/items/itemtypes/zabbix_agent#virtual_memory_data) for
details.
:::

Platform-specific calculation of **available** and **used**:

|Platform|**"available"**|**"used"**|
|--------|---------------|----------|
|*AIX*|free + cached|real memory in use|
|*FreeBSD*|inactive + cached + free|active + wired + cached|
|*HP UX*|free|total - free|
|*Linux<3.14*|free + buffers + cached|total - free|
|*Linux 3.14+*<br>(also backported to 3.10 on RHEL 7)|/proc/meminfo, see "MemAvailable" in Linux kernel [documentation](https://www.kernel.org/doc/Documentation/filesystems/proc.txt) for details.<br>Note that free + buffers + cached is no longer equal to 'available' due to not all the page cache can be freed and low watermark being used in calculation.|total - free|
|*NetBSD*|inactive + execpages + file + free|total - free|
|*OpenBSD*|inactive + free + cached|active + wired|
|*OSX*|inactive + free|active + wired|
|*Solaris*|free|total - free|
|*Win32*|free|total - free|

::: noteimportant
The sum of *vm.memory.size\[used\]* and
*vm.memory.size\[available\]* does not necessarily equal total. For
instance, on FreeBSD:\
\* Active, inactive, wired, cached memories are considered used, because
they store some useful information.\
\* At the same time inactive, cached, free memories are considered
available, because these kinds of memories can be given instantly to
processes that request more memory.\
\
So inactive memory is both used and available simultaneously. Because of
this, the *vm.memory.size\[used\]* item is designed for informational
purposes only, while *vm.memory.size\[available\]* is designed to be
used in triggers.
:::

[comment]: # ({/9a92e4df-4ea4e379})

[comment]: # ({f80ad839-f80ad839})
### See also

1.  [Additional details about memory calculation in different
    OS](http://blog.zabbix.com/when-alexei-isnt-looking#vm.memory.size)

[comment]: # ({/f80ad839-f80ad839})
