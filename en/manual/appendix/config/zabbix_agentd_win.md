[comment]: # attributes: notoc

[comment]: # ({a36cb226-a36cb226})
# 5 Zabbix agent (Windows)

[comment]: # ({/a36cb226-a36cb226})

[comment]: # ({c1c842aa-c0c75d19})
### Overview

The parameters supported by the Windows Zabbix agent configuration file (zabbix\_agent.conf) are listed in this section. 

The parameters are listed without additional information. Click on the parameter to see the full details.

|Parameter|Description|
|--|--------|
|[Alias](#alias)|Sets an alias for an item key.|
|[AllowKey](#allowkey)|Allow the execution of those item keys that match a pattern.|
|[BufferSend](#buffersend)|Do not keep data longer than N seconds in buffer.|
|[BufferSize](#buffersize)|The maximum number of values in the memory buffer.|
|[DebugLevel](#debuglevel)|The debug level.|
|[DenyKey](#denykey)|Deny the execution of those item keys that match a pattern.|
|[EnableRemoteCommands](#enableremotecommands)|Whether remote commands from Zabbix server are allowed.|
|[HeartbeatFrequency](#heartbeatfrequency)|The frequency of heartbeat messages in seconds.|
|[HostInterface](#hostinterface)|An optional parameter that defines the host interface.|
|[HostInterfaceItem](#hostinterfaceitem)|An optional parameter that defines an item used for getting the host interface.|
|[HostMetadata](#hostmetadata)|An optional parameter that defines the host metadata.|
|[HostMetadataItem](#hostmetadataitem)|An optional parameter that defines a Zabbix agent item used for getting the host metadata.|
|[Hostname](#hostname)|An optional parameter that defines the hostname.|
|[HostnameItem](#hostnameitem)|An optional parameter that defines a Zabbix agent item used for getting the hostname.|
|[Include](#include)|You may include individual files or all files in a directory in the configuration file.|
|[ListenBacklog](#listenbacklog)|The maximum number of pending connections in the TCP queue.|
|[ListenIP](#listenip)|A list of comma-delimited IP addresses that the agent should listen on.|
|[ListenPort](#listenport)|The agent will listen on this port for connections from the server.|
|[LogFile](#logfile)|The name of the log file.|
|[LogFileSize](#logfilesize)|The maximum size of the log file.|
|[LogRemoteCommands](#logremotecommands)|Enable logging of executed shell commands as warnings.|
|[LogType](#logtype)|The type of the log output.|
|[MaxLinesPerSecond](#maxlinespersecond)|The maximum number of new lines the agent will send per second to Zabbix server or proxy when processing 'log' and 'logrt' active checks.|
|[PerfCounter](#perfcounter)|Defines a new parameter <parameter\_name> which is the average value for system performance counter <perf\_counter\_path> for the specified time period <period> (in seconds).|
|[PerfCounterEn](#perfcounteren)|Defines a new parameter <parameter\_name> which is the average value for system performance counter <perf\_counter\_path> for the specified time period <period> (in seconds). Compared to PerfCounter, the perfcounter paths must be in English.|
|[RefreshActiveChecks](#refreshactivechecks)|How often the list of active checks is refreshed.|
|[Server](#server)|A list of comma-delimited IP addresses, optionally in CIDR notation, or DNS names of Zabbix servers and Zabbix proxies.|
|[ServerActive](#serveractive)|The Zabbix server/proxy address or cluster configuration to get active checks from.|
|[SourceIP](#sourceip)|The source IP address.|
|[StartAgents](#startagents)|The number of pre-forked instances of zabbix\_agentd that process passive checks.|
|[Timeout](#timeout)|Spend no more than Timeout seconds on processing.|
|[TLSAccept](#tlsaccept)|What incoming connections to accept.|
|[TLSCAFile](#tlscafile)|The full pathname of a file containing the top-level CA(s) certificates for peer certificate verification, used for encrypted communications between Zabbix components.|
|[TLSCertFile](#tlscertfile)|The full pathname of a file containing the agent certificate or certificate chain, used for encrypted communications between Zabbix components.|
|[TLSConnect](#tlsconnect)|How the agent should connect to Zabbix server or proxy.|
|[TLSCRLFile](#tlscrlfile)|The full pathname of a file containing revoked certificates. This parameter is used for encrypted communications between Zabbix components.|
|[TLSKeyFile](#tlskeyfile)|The full pathname of a file containing the agent private key, used for encrypted communications between Zabbix components.|
|[TLSPSKFile](#tlspskfile)|The full pathname of a file containing the agent pre-shared key, used for encrypted communications with Zabbix server.|
|[TLSPSKIdentity](#tlspskidentity)|The pre-shared key identity string, used for encrypted communications with Zabbix server.|
|[TLSServerCertIssuer](#tlsservercertissuer)|The allowed server (proxy) certificate issuer.|
|[TLSServerCertSubject](#tlsservercertsubject)|The allowed server (proxy) certificate subject.|
|[UnsafeUserParameters](#unsafeuserparameters)|Allow all characters to be passed in arguments to user-defined parameters.|
|[UserParameter](#userparameter)|A user-defined parameter to monitor.|
|[UserParameterDir](#userparameterdir)|The default search path for UserParameter commands.|

All parameters are non-mandatory unless explicitly stated that the parameter is mandatory. 

Note that:

-   The default values reflect daemon defaults, not the values in the
    shipped configuration files;
-   Zabbix supports configuration files only in UTF-8 encoding without
    [BOM](https://en.wikipedia.org/wiki/Byte_order_mark);
-   Comments starting with "\#" are only supported in the beginning of
    the line.

[comment]: # ({/c1c842aa-c0c75d19})

[comment]: # ({8085875f-d041e5fe})
### Parameter details

[comment]: # ({/8085875f-d041e5fe})

[comment]: # ({52c5f23f-dbac533e})
##### Alias

Sets an alias for an item key. It can be used to substitute a long and complex item key with a shorter and simpler one. Multiple *Alias* parameters with the same *Alias* key may be present.<br>Different *Alias* keys may reference the same item key.<br>Aliases can be used in *HostMetadataItem* but not in *HostnameItem* or *PerfCounter* parameters.

Example 1: Retrieving the paging file usage in percentage from the server.

    Alias=pg_usage:perf_counter[\Paging File(_Total)\% Usage]
    
Now the shorthand key **pg_usage** may be used to retrieve data.

Example 2: Getting the CPU load with default and custom parameters.

    Alias=cpu.load:system.cpu.load
    Alias=cpu.load[*]:system.cpu.load[*]

This allows use **cpu.load** key to get the CPU load with default parameters as well as use **cpu.load[percpu,avg15]** to get specific data about the CPU load.

Example 3: Running multiple [low-level discovery](/manual/discovery/low_level_discovery) rules processing the same discovery items.

    Alias=vfs.fs.discovery[*]:vfs.fs.discovery

Now it is possible to set up several discovery rules using **vfs.fs.discovery** with different parameters for each rule, e.g., **vfs.fs.discovery[foo]**, **vfs.fs.discovery[bar]**, etc.

[comment]: # ({/52c5f23f-dbac533e})

[comment]: # ({6c104b7b-049a5192})
##### AllowKey

Allow the execution of those item keys that match a pattern. The key pattern is a wildcard expression that supports the "\*" character to match any number of any characters.<br>Multiple key matching rules may be defined in combination with DenyKey. The parameters are processed one by one according to their appearance order. See also: [Restricting agent checks](/manual/config/items/restrict_checks).

[comment]: # ({/6c104b7b-049a5192})

[comment]: # ({fce8e2ee-9341dc8c})
##### BufferSend

Do not keep data longer than N seconds in buffer.

Default: `5`<br>
Range: 1-3600

[comment]: # ({/fce8e2ee-9341dc8c})

[comment]: # ({96409162-11405d19})
##### BufferSize

The maximum number of values in the memory buffer. The agent will send all collected data to the Zabbix server or proxy if the buffer is full.

Default: `100`<br>
Range: 2-65535

[comment]: # ({/96409162-11405d19})

[comment]: # ({9c071f70-b292af40})
##### DebugLevel

Specify the debug level:<br>*0* - basic information about starting and stopping of Zabbix processes<br>*1* - critical information;<br>*2* - error information;<br>*3* - warnings;<br>*4* - for debugging (produces lots of information);<br>*5* - extended debugging (produces even more information).

Default: `3`<br>
Range: 0-5

[comment]: # ({/9c071f70-b292af40})

[comment]: # ({13d6d3a4-9a278481})
##### DenyKey

Deny the execution of those item keys that match a pattern. The key pattern is a wildcard expression that supports the "\*" character to match any number of any characters.<br>Multiple key matching rules may be defined in combination with AllowKey. The parameters are processed one by one according to their appearance order. See also: [Restricting agent checks](/manual/config/items/restrict_checks).

[comment]: # ({/13d6d3a4-9a278481})

[comment]: # ({c4e8c8b1-acca1798})
##### EnableRemoteCommands

Whether remote commands from Zabbix server are allowed. This parameter is **deprecated**, use AllowKey=system.run\[\*\] or DenyKey=system.run\[\*\] instead.<br>It is an internal alias for AllowKey/DenyKey parameters depending on value:<br>0 - DenyKey=system.run\[\*\]<br>1 - AllowKey=system.run\[\*\]

Default: `0`<br>
Values: 0 - do not allow, 1 - allow

[comment]: # ({/c4e8c8b1-acca1798})

[comment]: # ({dbb05958-672f4f6e})
##### HeartbeatFrequency

The frequency of heartbeat messages in seconds. Used for monitoring the availability of active checks.<br>0 - heartbeat messages disabled.

Default: `60`<br>
Range: 0-3600

[comment]: # ({/dbb05958-672f4f6e})

[comment]: # ({48de1a7d-99752970})
##### HostInterface

An optional parameter that defines the host interface. The host interface is used at host [autoregistration](/manual/discovery/auto_registration#using_dns_as_default_interface) process. If not defined, the value will be acquired from HostInterfaceItem.<br>The agent will issue an error and not start if the value is over the limit of 255 characters.

Range: 0-255 characters

[comment]: # ({/48de1a7d-99752970})

[comment]: # ({249a7eff-8ac8b769})
##### HostInterfaceItem

An optional parameter that defines an item used for getting the host interface.<br>Host interface is used at host [autoregistration](/manual/discovery/auto_registration#using_dns_as_default_interface) process.<br>During an autoregistration request the agent will log a warning message if the value returned by the specified item is over the limit of 255 characters.<br>The system.run\[\] item is supported regardless of AllowKey/DenyKey values.<br>This option is only used when HostInterface is not defined.

[comment]: # ({/249a7eff-8ac8b769})

[comment]: # ({5bcd7757-098e9a8d})
##### HostMetadata

An optional parameter that defines host metadata. Host metadata is used only at host autoregistration process (active agent). If not defined, the value will be acquired from HostMetadataItem.<br>The agent will issue an error and not start if the specified value is over the limit of 2034 bytes or a non-UTF-8 string.

Range: 0-2034 bytes

[comment]: # ({/5bcd7757-098e9a8d})

[comment]: # ({45e06744-9a501923})
##### HostMetadataItem

An optional parameter that defines a Zabbix agent item used for getting host metadata. This option is only used when HostMetadata is not defined. User parameters, performance counters and aliases are supported. The system.run\[\] item is supported regardless of AllowKey/DenyKey values.<br>The HostMetadataItem value is retrieved on each autoregistration attempt and is used only at host autoregistration process (active agent).<br>During an autoregistration request the agent will log a warning message if the value returned by the specified item is over the limit of 65535 UTF-8 code points. The value returned by the item must be a UTF-8 string otherwise it will be ignored.

[comment]: # ({/45e06744-9a501923})

[comment]: # ({6e272ef0-7ba40433})
##### Hostname

A list of comma-delimited, unique, case-sensitive hostnames. Required for active checks and must match hostnames as configured on the server. The value is acquired from HostnameItem if undefined.<br>Allowed characters: alphanumeric, '.', ' ', '\_' and '-'. Maximum length: 128 characters per hostname, 2048 characters for the entire line.

Default: Set by HostnameItem

[comment]: # ({/6e272ef0-7ba40433})

[comment]: # ({f4c8388a-3d732bce})
##### HostnameItem

An optional parameter that defines a Zabbix agent item used for getting the host name. This option is only used when Hostname is not defined. User parameters, performance counters or aliases are not supported, but the system.run\[\] item is supported regardless of AllowKey/DenyKey values.<br>The output length is limited to 512KB.<br>See also a [more detailed description](/manual/appendix/install/windows_agent#configuration).

Default: `system.hostname`

[comment]: # ({/f4c8388a-3d732bce})

[comment]: # ({6ace084e-4f66fa26})
##### Include

You may include individual files or all files in a directory in the configuration file. To only include relevant files in the specified directory, the asterisk wildcard character is supported for pattern matching.<br>See [special notes](special_notes_include) about limitations.

Example:

    Include=C:\Program Files\Zabbix Agent\zabbix_agentd.d\*.conf

[comment]: # ({/6ace084e-4f66fa26})

[comment]: # ({fcd70e26-f18a2cd5})
##### ListenBacklog

The maximum number of pending connections in the TCP queue.<br>The default value is a hard-coded constant, which depends on the system.<br>The maximum supported value depends on the system, too high values may be silently truncated to the 'implementation-specified maximum'.

Default: `SOMAXCONN`<br>
Range: 0 - INT\_MAX

[comment]: # ({/fcd70e26-f18a2cd5})

[comment]: # ({35f278c7-fa6fe20d})
##### ListenIP

A list of comma-delimited IP addresses that the agent should listen on.

Default: `0.0.0.0`

[comment]: # ({/35f278c7-fa6fe20d})

[comment]: # ({5f95bc53-04498be8})
##### ListenPort

The agent will listen on this port for connections from the server.

Default: `10050`<br>
Range: 1024-32767

[comment]: # ({/5f95bc53-04498be8})

[comment]: # ({957b7e21-7775e414})
##### LogFile

The name of the agent log file.

Default: `C:\\zabbix_agentd.log`<br>
Mandatory: Yes, if LogType is set to *file*; otherwise no

[comment]: # ({/957b7e21-7775e414})

[comment]: # ({f7604f8f-9135a275})
##### LogFileSize

The maximum size of a log file in MB.<br>0 - disable automatic log rotation.<br>*Note*: If the log file size limit is reached and file rotation fails, for whatever reason, the existing log file is truncated and started anew.

Default: `1`<br>
Range: 0-1024

[comment]: # ({/f7604f8f-9135a275})

[comment]: # ({cf3a9c90-c654859c})
##### LogRemoteCommands

Enable the logging of the executed shell commands as warnings. Commands will be logged only if executed remotely. Log entries will not be created if system.run\[\] is launched locally by HostMetadataItem, HostInterfaceItem or HostnameItem parameters.

Default: `0`<br>
Values: 0 - disabled, 1 - enabled

[comment]: # ({/cf3a9c90-c654859c})

[comment]: # ({2b4ff06b-ed73f4f7})
##### LogType

The type of the log output:<br>*file* - write log to the file specified by LogFile parameter;<br>*system* - write log to Windows Event Log;<br>*console* - write log to standard output.

Default: `file`

[comment]: # ({/2b4ff06b-ed73f4f7})

[comment]: # ({3d31a7c7-6eaa719f})
##### MaxLinesPerSecond

The maximum number of new lines the agent will send per second to Zabbix server or proxy when processing 'log', 'logrt', and 'eventlog' active checks. The provided value will be overridden by the 'maxlines' parameter, provided in the 'log', 'logrt', or 'eventlog' item key.<br>*Note*: Zabbix will process 10 times more new lines than set in *MaxLinesPerSecond* to seek the required string in log items.

Default: `20`<br>
Range: 1-1000

[comment]: # ({/3d31a7c7-6eaa719f})

[comment]: # ({e560bffc-fb950f2f})

##### PerfCounter

Defines a new parameter <parameter\_name> which is the average value for system performance counter <perf\_counter\_path> for the specified time period <period> (in seconds).<br>Syntax: <parameter\_name>,"<perf\_counter\_path>",<period>

For example, if you wish to receive the average number of processor interrupts per second for the last minute, you can define a new parameter "interrupts" as the following:<br>

    PerfCounter = interrupts,"\Processor(0)\Interrupts/sec",60

Please note the double quotes around the performance counter path. The parameter name (interrupts) is to be used as the item key when creating an item. Samples for calculating the average value will be taken every second.<br>You may run "typeperf -qx" to get the list of all performance counters available in Windows.

[comment]: # ({/e560bffc-fb950f2f})

[comment]: # ({33e9f331-0f61ea9b})

##### PerfCounterEn

Defines a new parameter <parameter\_name> which is the average value for system performance counter <perf\_counter\_path> for the specified time period <period> (in seconds). Compared to PerfCounter, the perfcounter paths must be in English. Supported only on **Windows Server 2008/Vista** and later.<br>Syntax: <parameter\_name>,"<perf\_counter\_path>",<period>

For example, if you wish to receive the average number of processor interrupts per second for the last minute, you can define a new parameter "interrupts" as the following:<br>

    PerfCounterEn = interrupts,"\Processor(0)\Interrupts/sec",60

Please note the double quotes around the performance counter path. The parameter name (interrupts) is to be used as the item key when creating an item. Samples for calculating the average value will be taken every second.<br>You can find the list of English strings by viewing the following registry key: `HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Perflib\009`.

[comment]: # ({/33e9f331-0f61ea9b})

[comment]: # ({d3de4e11-eba563a4})
##### RefreshActiveChecks

How often the list of active checks is refreshed, in seconds. Note that after failing to refresh active checks the next refresh will be attempted in 60 seconds.

Default: `5`<br>
Range: 1-86400

[comment]: # ({/d3de4e11-eba563a4})

[comment]: # ({0b9d34eb-f2ed869a})
##### Server

A list of comma-delimited IP addresses, optionally in CIDR notation, or DNS names of Zabbix servers or Zabbix proxies. Incoming connections will be accepted only from the hosts listed here. If IPv6 support is enabled then '127.0.0.1', '::127.0.0.1', '::ffff:127.0.0.1' are treated equally and '::/0' will allow any IPv4 or IPv6 address. '0.0.0.0/0' can be used to allow any IPv4 address. Note, that "IPv4-compatible IPv6 addresses" (0000::/96 prefix) are supported but deprecated by [RFC4291](https://tools.ietf.org/html/rfc4291#section-2.5.5). Spaces are allowed.

Example: 

    Server=127.0.0.1,192.168.1.0/24,::1,2001:db8::/32,zabbix.example.com

Mandatory: yes, if StartAgents is not explicitly set to 0

[comment]: # ({/0b9d34eb-f2ed869a})

[comment]: # ({e60298c6-35ed1929})
##### ServerActive

The Zabbix server/proxy address or cluster configuration to get active checks from. The server/proxy address is an IP address or DNS name and optional port separated by colon.<br>The cluster configuration is one or more server addresses separated by semicolon. Multiple Zabbix servers/clusters and Zabbix proxies can be specified, separated by comma. More than one Zabbix proxy should not be specified from each Zabbix server/cluster. If Zabbix proxy is specified then Zabbix server/cluster for that proxy should not be specified.<br>Multiple comma-delimited addresses can be provided to use several independent Zabbix servers in parallel. Spaces are allowed.<br>If the port is not specified, default port is used.<br>IPv6 addresses must be enclosed in square brackets if port for that host is specified. If port is not specified, square brackets for IPv6 addresses are optional.<br>If this parameter is not specified, active checks are disabled.

Example for Zabbix proxy: 

    ServerActive=127.0.0.1:10051

Example for multiple servers: 

    ServerActive=127.0.0.1:20051,zabbix.domain,[::1]:30051,::1,[12fc::1]

Example for high availability:

    ServerActive=zabbix.cluster.node1;zabbix.cluster.node2:20051;zabbix.cluster.node3

Example for high availability with two clusters and one server:

    ServerActive=zabbix.cluster.node1;zabbix.cluster.node2:20051,zabbix.cluster2.node1;zabbix.cluster2.node2,zabbix.domain

Range: (\*)

[comment]: # ({/e60298c6-35ed1929})

[comment]: # ({3628af0a-10fc73c6})
##### SourceIP

The source IP address for:<br>- outgoing connections to Zabbix server or Zabbix proxy;<br>- making connections while executing some items (web.page.get, net.tcp.port, etc.).

[comment]: # ({/3628af0a-10fc73c6})

[comment]: # ({5f1dbbab-d8dd10c4})
##### StartAgents

The number of pre-forked instances of zabbix\_agentd that process passive checks. If set to 0, passive checks are disabled and the agent will not listen on any TCP port.

Default: `3`<br>
Range: 0-63 (\*)

[comment]: # ({/5f1dbbab-d8dd10c4})

[comment]: # ({1b1125a4-895d5e50})
##### Timeout

Spend no more than Timeout seconds on processing.

Default: `3`<br>
Range: 1-30

[comment]: # ({/1b1125a4-895d5e50})

[comment]: # ({412cbce9-fb75760f})
##### TLSAccept

The incoming connections to accept. Used for passive checks. Multiple values can be specified, separated by comma:<br>*unencrypted* - accept connections without encryption (default)<br>*psk* - accept connections with TLS and a pre-shared key (PSK)<br>*cert* - accept connections with TLS and a certificate

Mandatory: yes, if TLS certificate or PSK parameters are defined (even for *unencrypted* connection); otherwise no

[comment]: # ({/412cbce9-fb75760f})

[comment]: # ({65d07bda-8301867c})
##### TLSCAFile

The full pathname of the file containing the top-level CA(s) certificates for peer certificate verification, used for encrypted communications between Zabbix components.

[comment]: # ({/65d07bda-8301867c})

[comment]: # ({005f60f1-8da364a3})
##### TLSCertFile

The full pathname of the file containing the agent certificate or certificate chain, used for encrypted communications with Zabbix components.

[comment]: # ({/005f60f1-8da364a3})

[comment]: # ({be99c4c7-3f5548eb})
##### TLSConnect

How the agent should connect to Zabbix server or proxy. Used for active checks. Only one value can be specified:<br>*unencrypted* - connect without encryption (default)<br>*psk* - connect using TLS and a pre-shared key (PSK)<br>*cert* - connect using TLS and a certificate

Mandatory: yes, if TLS certificate or PSK parameters are defined (even for *unencrypted* connection); otherwise no

[comment]: # ({/be99c4c7-3f5548eb})

[comment]: # ({94a4b758-95b41620})
##### TLSCRLFile

The full pathname of the file containing revoked certificates. This parameter is used for encrypted communications between Zabbix components.

[comment]: # ({/94a4b758-95b41620})

[comment]: # ({c2aa4315-89503f46})
##### TLSKeyFile

The full pathname of the file containing the agent private key, used for encrypted communications between Zabbix components.

[comment]: # ({/c2aa4315-89503f46})

[comment]: # ({11defa44-2936868d})
##### TLSPSKFile

The full pathname of the file containing the agent pre-shared key, used for encrypted communications with Zabbix server.

[comment]: # ({/11defa44-2936868d})

[comment]: # ({92d3eff0-262fa198})
##### TLSPSKIdentity

The pre-shared key identity string, used for encrypted communications with Zabbix server.

[comment]: # ({/92d3eff0-262fa198})

[comment]: # ({ae5c568f-9eef27fc})
##### TLSServerCertIssuer

The allowed server (proxy) certificate issuer.

[comment]: # ({/ae5c568f-9eef27fc})

[comment]: # ({fb69617f-abaebd84})
##### TLSServerCertSubject

The allowed server (proxy) certificate subject.

[comment]: # ({/fb69617f-abaebd84})

[comment]: # ({e61ac186-a721fb91})
##### UnsafeUserParameters

Allow all characters to be passed in arguments to user-defined parameters. The following characters are not allowed: \\ ' " \` \* ? \[ \] { } \~ $ ! & ; ( ) < > \| \# @<br>Additionally, newline characters are not allowed.

Default: `0`<br>
Values: 0 - do not allow, 1 - allow

[comment]: # ({/e61ac186-a721fb91})

[comment]: # ({de3660b7-8f64ff97})
##### UserParameter

A user-defined parameter to monitor. There can be several user-defined parameters.<br>Format: UserParameter=<key>,<shell command><br>Note that the shell command must not return empty string or EOL only. Shell commands may have relative paths, if the UserParameterDir parameter is specified.

Example:

    UserParameter=system.test,who|wc -l
    UserParameter=check_cpu,./custom_script.sh

[comment]: # ({/de3660b7-8f64ff97})

[comment]: # ({1803cab5-9e2d0466})
##### UserParameterDir

The default search path for UserParameter commands. If used, the agent will change its working directory to the one specified here before executing a command. Thereby, UserParameter commands can have a relative `./` prefix instead of a full path. Only one entry is allowed.

Example:

    UserParameterDir=/opt/myscripts

[comment]: # ({/1803cab5-9e2d0466})

[comment]: # ({cbf4a08e-7142df61})

::: noteclassic
 (\*) The number of active servers listed in ServerActive
plus the number of pre-forked instances for passive checks specified in
StartAgents must be less than 64.
:::

[comment]: # ({/cbf4a08e-7142df61})

[comment]: # ({179d1dff-179d1dff})
### See also

1.  [Differences in the Zabbix agent configuration for active and
    passive checks starting from version
    2.0.0](http://blog.zabbix.com/multiple-servers-for-active-agent-sure/858).

[comment]: # ({/179d1dff-179d1dff})
