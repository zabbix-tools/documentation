[comment]: # attributes: notoc

[comment]: # ({452d5bc5-452d5bc5})
# 2 Zabbix proxy

[comment]: # ({/452d5bc5-452d5bc5})

[comment]: # ({38d33a26-221978cc})
### Overview

The parameters supported by the Zabbix proxy configuration file (zabbix\_proxy.conf) are listed in this section. 

The parameters are listed without additional information. Click on the parameter to see the full details.

|Parameter|Description|
|--|--------|
|[AllowRoot](#allowroot)|Allow the proxy to run as 'root'.|
|[AllowUnsupportedDBVersions](#allowunsupporteddbversions)|Allow the proxy to work with unsupported database versions.|
|[CacheSize](#cachesize)|The size of the configuration cache.|
|[ConfigFrequency](#configfrequency)|This parameter is **deprecated** (use ProxyConfigFrequency instead).<br>How often the proxy retrieves configuration data from Zabbix server in seconds.|
|[DataSenderFrequency](#datasenderfrequency)|The proxy will send collected data to the server every N seconds.|
|[DBHost](#dbhost)|The database host name.|
|[DBName](#dbname)|The database name or path to the database file for SQLite3.|
|[DBPassword](#dbpassword)|The database password.|
|[DBPort](#dbport)|The database port when not using local socket.|
|[DBSchema](#dbschema)|The database schema name. Used for PostgreSQL.|
|[DBSocket](#dbsocket)|The path to the MySQL socket file.|
|[DBUser](#dbuser)|The database user.|
|[DBTLSConnect](#dbtlsconnect)|Setting this option to the specified value enforces to use a TLS connection to the database.|
|[DBTLSCAFile](#dbtlscafile)|The full pathname of a file containing the top-level CA(s) certificates for database certificate verification.|
|[DBTLSCertFile](#dbtlscertfile)|The full pathname of a file containing the Zabbix proxy certificate for authenticating to database.|
|[DBTLSKeyFile](#dbtlskeyfile)|The full pathname of a file containing the private key for authenticating to database.|
|[DBTLSCipher](#dbtlscipher)|The list of encryption ciphers that Zabbix proxy permits for TLS protocols up through TLS v1.2. Supported only for MySQL.|
|[DBTLSCipher13](#dbtlscipher13)|The list of encryption ciphersuites that Zabbix proxy permits for the TLS v1.3 protocol. Supported only for MySQL, starting from version 8.0.16.|
|[DebugLevel](#debuglevel)|The debug level.|
|[EnableRemoteCommands](#enableremotecommands)|Whether remote commands from Zabbix server are allowed.|
|[ExternalScripts](#externalscripts)|The location of external scripts.|
|[Fping6Location](#fping6location)|The location of fping6.|
|[FpingLocation](#fpinglocation)|The location of fping.|
|[HistoryCacheSize](#historycachesize)|The size of the history cache.|
|[HistoryIndexCacheSize](#historyindexcachesize)|The size of the history index cache.|
|[Hostname](#hostname)|A unique, case sensitive proxy name.|
|[HostnameItem](#hostnameitem)|The item used for setting Hostname if it is undefined.|
|[HousekeepingFrequency](#housekeepingfrequency)|How often Zabbix will perform the housekeeping procedure in hours.|
|[Include](#include)|You may include individual files or all files in a directory in the configuration file.|
|[JavaGateway](#javagateway)|The IP address (or hostname) of Zabbix Java gateway.|
|[JavaGatewayPort](#javagatewayport)|The port that Zabbix Java gateway listens on.|
|[ListenBacklog](#listenbacklog)|The maximum number of pending connections in the TCP queue.|
|[ListenIP](#listenip)|A list of comma-delimited IP addresses that the trapper should listen on.|
|[ListenPort](#listenport)|The listen port for trapper.|
|[LoadModule](#loadmodule)|The module to load at proxy startup.|
|[LoadModulePath](#loadmodulepath)|The full path to the location of proxy modules.|
|[LogFile](#logfile)|The name of the log file.|
|[LogFileSize](#logfilesize)|The maximum size of the log file.|
|[LogRemoteCommands](#logremotecommands)|Enable logging of executed shell commands as warnings.|
|[LogSlowQueries](#logslowqueries)|How long a database query may take before being logged.|
|[LogType](#logtype)|The type of the log output.|
|[PidFile](#pidfile)|The name of the PID file.|
|[ProxyConfigFrequency](#proxyconfigfrequency)|How often the proxy retrieves configuration data from Zabbix server in seconds.|
|[ProxyLocalBuffer](#proxylocalbuffer)|The proxy will keep data locally for N hours, even if the data have already been synced with the server.|
|[ProxyMode](#proxymode)|The proxy operating mode (active/passive).|
|[ProxyOfflineBuffer](#proxyofflinebuffer)|The proxy will keep data for N hours in case of no connectivity with Zabbix server.|
|[Server](#server)|If ProxyMode is set to active mode: Zabbix server IP address or DNS name (address:port) or cluster (address:port;address2:port) to get configuration data from and send data to.<br>If ProxyMode is set to passive mode: List of comma delimited IP addresses, optionally in CIDR notation, or DNS names of Zabbix server.|
|[SNMPTrapperFile](#snmptrapperfile)|The temporary file used for passing data from the SNMP trap daemon to the proxy.|
|[SocketDir](#socketdir)|The directory to store the IPC sockets used by internal Zabbix services.|
|[SourceIP](#sourceip)|The source IP address.|
|[SSHKeyLocation](#sshkeylocation)|The location of public and private keys for SSH checks and actions.|
|[SSLCertLocation](#sslcertlocation)|The location of SSL client certificate files for client authentication.|
|[SSLKeyLocation](#sslkeylocation)|The location of SSL private key files for client authentication.|
|[SSLCALocation](#sslcalocation)|Override the location of certificate authority (CA) files for SSL server certificate verification.|
|[StartDBSyncers](#startdbsyncers)|The number of pre-forked instances of history syncers.|
|[StartDiscoverers](#startdiscoverers)|The number of pre-forked instances of discovery workers.|
|[StartHTTPPollers](#starthttppollers)|The number of pre-forked instances of HTTP pollers.|
|[StartIPMIPollers](#startipmipollers)|The number of pre-forked instances of IPMI pollers.|
|[StartJavaPollers](#startjavapollers)|The number of pre-forked instances of Java pollers.|
|[StartODBCPollers](#startodbcpollers)|The number of pre-forked instances of ODBC pollers.|
|[StartPingers](#startpingers)|The number of pre-forked instances of ICMP pingers.|
|[StartPollersUnreachable](#startpollersunreachable)|The number of pre-forked instances of pollers for unreachable hosts (including IPMI and Java).|
|[StartPollers](#startpollers)|The number of pre-forked instances of pollers.|
|[StartPreprocessors](#startpreprocessors)|The number of pre-started instances of preprocessing workers.|
|[StartSNMPTrapper](#startsnmptrapper)|If set to 1, an SNMP trapper process will be started.|
|[StartTrappers](#starttrappers)|The number of pre-forked instances of trappers.|
|[StartVMwareCollectors](#startvmwarecollectors)|The number of pre-forked VMware collector instances.|
|[StatsAllowedIP](#statsallowedip)|A list of comma-delimited IP addresses, optionally in CIDR notation, or DNS names of external Zabbix instances. The stats request will be accepted only from the addresses listed here.|
|[Timeout](#timeout)|How long we wait for agent, SNMP device or external check in seconds.|
|[TLSAccept](#tlsaccept)|What incoming connections to accept from Zabbix server.|
|[TLSCAFile](#tlscafile)|The full pathname of a file containing the top-level CA(s) certificates for peer certificate verification, used for encrypted communications between Zabbix components.|
|[TLSCertFile](#tlscertfile)|The full pathname of a file containing the server certificate or certificate chain, used for encrypted communications between Zabbix components.|
|[TLSCipherAll](#tlscipherall)|The GnuTLS priority string or OpenSSL (TLS 1.2) cipher string. Override the default ciphersuite selection criteria for certificate- and PSK-based encryption.|
|[TLSCipherAll13](#tlscipherall13)|The cipher string for OpenSSL 1.1.1 or newer in TLS 1.3. Override the default ciphersuite selection criteria for certificate- and PSK-based encryption.|
|[TLSCipherCert](#tlsciphercert)|The GnuTLS priority string or OpenSSL (TLS 1.2) cipher string. Override the default ciphersuite selection criteria for certificate-based encryption.|
|[TLSCipherCert13](#tlsciphercert13)|The cipher string for OpenSSL 1.1.1 or newer in TLS 1.3. Override the default ciphersuite selection criteria for certificate-based encryption.|
|[TLSCipherPSK](#tlscipherpsk)|The GnuTLS priority string or OpenSSL (TLS 1.2) cipher string. Override the default ciphersuite selection criteria for PSK-based encryption.|
|[TLSCipherPSK13](#tlscipherpsk13)|The cipher string for OpenSSL 1.1.1 or newer in TLS 1.3. Override the default ciphersuite selection criteria for PSK-based encryption.|
|[TLSConnect](#tlsconnect)|How the proxy should connect to Zabbix server.|
|[TLSCRLFile](#tlscrlfile)|The full pathname of a file containing revoked certificates. This parameter is used for encrypted communications between Zabbix components.|
|[TLSKeyFile](#tlskeyfile)|The full pathname of a file containing the proxy private key, used for encrypted communications between Zabbix components.|
|[TLSPSKFile](#tlspskfile)|The full pathname of a file containing the proxy pre-shared key, used for encrypted communications with Zabbix server.|
|[TLSPSKIdentity](#tlspskidentity)|The pre-shared key identity string, used for encrypted communications with Zabbix server.|
|[TLSServerCertIssuer](#tlsservercertissuer)|The allowed server certificate issuer.|
|[TLSServerCertSubject](#tlsservercertsubject)|The allowed server certificate subject.|
|[TmpDir](#tmpdir)|The temporary directory.|
|[TrapperTimeout](#trappertimeout)|How many seconds the trapper may spend processing new data.|
|[UnavailableDelay](#unavailabledelay)|How often a host is checked for availability during the unavailability period.|
|[UnreachableDelay](#unreachabledelay)|How often a host is checked for availability during the unreachability period.|
|[UnreachablePeriod](#unreachableperiod)|After how many seconds of unreachability treat the host as unavailable.|
|[User](#user)|Drop privileges to a specific, existing user on the system.|
|[Vault](#vault)|The vault provider.|
|[VaultDBPath](#vaultdbpath)|The location, from where database credentials should be retrieved by keys.|
|[VaultTLSCertFile](#vaulttlscertfile)|The name of the SSL certificate file used for client authentication.|
|[VaultTLSKeyFile](#vaulttlskeyfile)|The name of the SSL private key file used for client authentication.|
|[VaultToken](#vaulttoken)|The HashiCorp vault authentication token.|
|[VaultURL](#vaulturl)|The vault server HTTP\[S\] URL.|
|[VMwareCacheSize](#vmwarecachesize)|The shared memory size for storing VMware data.|
|[VMwareFrequency](#vmwarefrequency)|The delay in seconds between data gathering from a single VMware service.|
|[VMwarePerfFrequency](#vmwareperffrequency)|The delay in seconds between performance counter statistics retrieval from a single VMware service.|
|[VMwareTimeout](#vmwareperffrequency)|The maximum number of seconds a vmware collector will wait for a response from VMware service.|

All parameters are non-mandatory unless explicitly stated that the parameter is mandatory.

Note that:

-   The default values reflect daemon defaults, not the values in the
    shipped configuration files;
-   Zabbix supports configuration files only in UTF-8 encoding without
    [BOM](https://en.wikipedia.org/wiki/Byte_order_mark);
-   Comments starting with "\#" are only supported in the beginning of
    the line.

[comment]: # ({/38d33a26-221978cc})

[comment]: # ({8085875f-7bf44544})
### Parameter details

[comment]: # ({/8085875f-7bf44544})

[comment]: # ({9260fe04-fc4d81ba})
##### AllowRoot

Allow the proxy to run as 'root'. If disabled and the proxy is started by 'root', the proxy will try to switch to the 'zabbix' user instead. Has no effect if started under a regular user.

Default: `0`<br>
Values: 0 - do not allow; 1 - allow

[comment]: # ({/9260fe04-fc4d81ba})

[comment]: # ({3abe15c1-c26999ee})
##### AllowUnsupportedDBVersions

Allow the proxy to work with unsupported database versions.

Default: `0`<br>
Values: 0 - do not allow; 1 - allow

[comment]: # ({/3abe15c1-c26999ee})

[comment]: # ({2afe10f0-317e46d4})
##### CacheSize

The size of the configuration cache, in bytes. The shared memory size for storing host and item data.

Default: `32M`<br>
Range: 128K-64G

[comment]: # ({/2afe10f0-317e46d4})

[comment]: # ({e92a162e-a1148208})
##### ConfigFrequency

This parameter is **deprecated** (use ProxyConfigFrequency instead).<br>How often the proxy retrieves configuration data from Zabbix server in seconds.<br>Active proxy parameter. Ignored for passive proxies (see ProxyMode parameter).

Default: `3600`<br>
Range: 1-604800

[comment]: # ({/e92a162e-a1148208})

[comment]: # ({14d6f2a6-797b5d4b})
##### DataSenderFrequency

The proxy will send collected data to the server every N seconds. Note that an active proxy will still poll Zabbix server every second for remote command tasks.<br>Active proxy parameter. Ignored for passive proxies (see ProxyMode parameter).

Default: `1`<br>
Range: 1-3600

[comment]: # ({/14d6f2a6-797b5d4b})

[comment]: # ({71f5559f-68d91db6})
##### DBHost

The database host name.<br>With MySQL `localhost` or empty string results in using a socket. With PostgreSQL only empty string results in attempt to use socket. With [Oracle](/manual/appendix/install/oracle#connection_set_up) empty string results in using the Net Service Name connection method; in this case consider using the TNS\_ADMIN environment variable to specify the directory of the tnsnames.ora file.

Default: `localhost`

[comment]: # ({/71f5559f-68d91db6})

[comment]: # ({508901db-a63deb51})
##### DBName

The database name or path to the database file for SQLite3 (the multi-process architecture of Zabbix does not allow to use [in-memory database](https://www.sqlite.org/inmemorydb.html), e.g. `:memory:`, `file::memory:?cache=shared` or `file:memdb1?mode=memory&cache=shared`).<br>*Warning*: Do not attempt to use the same database the Zabbix server is using.<br>With [Oracle](/manual/appendix/install/oracle#connection_set_up), if the Net Service Name connection method is used, specify the service name from tnsnames.ora or set to empty string; set the TWO\_TASK environment variable if DBName is set to empty string.

Mandatory: Yes

[comment]: # ({/508901db-a63deb51})

[comment]: # ({a8a2d4d3-41ef1a35})
##### DBPassword

The database password. Comment this line if no password is used. Ignored for SQLite.

[comment]: # ({/a8a2d4d3-41ef1a35})

[comment]: # ({244eb815-4f65edaa})
##### DBPort

The database port when not using local socket. Ignored for SQLite.<br>With [Oracle](/manual/appendix/install/oracle#connection_set_up), if the Net Service Name connection method is used, this parameter will be ignored; the port number from the tnsnames.ora file will be used instead.

Range: 1024-65535

[comment]: # ({/244eb815-4f65edaa})

[comment]: # ({e46eeb51-432321c5})
##### DBSchema

The database schema name. Used for PostgreSQL.

[comment]: # ({/e46eeb51-432321c5})

[comment]: # ({35bbd595-c1dbb854})
##### DBSocket

The path to the MySQL socket file.<br>The database port when not using local socket. Ignored for SQLite.

Default: `3306`

[comment]: # ({/35bbd595-c1dbb854})

[comment]: # ({2914485e-c2cec654})
##### DBUser

The database user. Ignored for SQLite.

[comment]: # ({/2914485e-c2cec654})

[comment]: # ({b665dd5c-c6670742})
##### DBTLSConnect

Setting this option enforces to use TLS connection to the database:<br>*required* - connect using TLS<br>*verify\_ca* - connect using TLS and verify certificate<br>*verify\_full* - connect using TLS, verify certificate and verify that database identity specified by DBHost matches its certificate<br>On MySQL starting from 5.7.11 and PostgreSQL the following values are supported: "required", "verify", "verify\_full".<br>On MariaDB starting from version 10.2.6 "required" and "verify\_full" values are supported.<br>By default not set to any option and the behavior depends on database configuration.

[comment]: # ({/b665dd5c-c6670742})

[comment]: # ({cbc09c0c-fd8ea2b2})
##### DBTLSCAFile

The full pathname of a file containing the top-level CA(s) certificates for database certificate verification.

Mandatory: no (yes, if DBTLSConnect set to *verify\_ca* or *verify\_full*)

[comment]: # ({/cbc09c0c-fd8ea2b2})

[comment]: # ({66290cd3-6ec7937a})
##### DBTLSCertFile

The full pathname of a file containing the Zabbix proxy certificate for authenticating to database.

[comment]: # ({/66290cd3-6ec7937a})

[comment]: # ({843ecedf-eda6649f})
##### DBTLSKeyFile

The full pathname of a file containing the private key for authenticating to the database.

[comment]: # ({/843ecedf-eda6649f})

[comment]: # ({9dab2703-150712b8})
##### DBTLSCipher

The list of encryption ciphers that Zabbix proxy permits for TLS protocols up through TLS v1.2. Supported only for MySQL.

[comment]: # ({/9dab2703-150712b8})

[comment]: # ({b6028e9d-1594abd5})
##### DBTLSCipher13

The list of encryption ciphersuites that Zabbix proxy permits for the TLS v1.3 protocol. Supported only for MySQL, starting from version 8.0.16.

[comment]: # ({/b6028e9d-1594abd5})

[comment]: # ({9c071f70-c65cefa5})
##### DebugLevel

Specify the debug level:<br>*0* - basic information about starting and stopping of Zabbix processes<br>*1* - critical information;<br>*2* - error information;<br>*3* - warnings;<br>*4* - for debugging (produces lots of information);<br>*5* - extended debugging (produces even more information).

Default: `3`<br>
Range: 0-5

[comment]: # ({/9c071f70-c65cefa5})

[comment]: # ({5b4a4bfb-404189de})
##### EnableRemoteCommands

Whether remote commands from Zabbix server are allowed.

Default: `0`<br>
Values: 0 - not allowed; 1 - allowed

[comment]: # ({/5b4a4bfb-404189de})

[comment]: # ({79e29f6f-62d00bff})
##### ExternalScripts

The location of external scripts (depends on the `datadir` compile-time installation variable).

Default: `/usr/local/share/zabbix/externalscripts`

[comment]: # ({/79e29f6f-62d00bff})

[comment]: # ({f5450a5e-c6a5104d})
##### Fping6Location

The location of fping6. Make sure that the fping6 binary has root ownership and the SUID flag set. Make empty ("Fping6Location=") if your fping utility is capable to process IPv6 addresses.

Default: `/usr/sbin/fping6`

[comment]: # ({/f5450a5e-c6a5104d})

[comment]: # ({dc27c714-1be0920c})
##### FpingLocation

The location of fping. Make sure that the fping binary has root ownership and the SUID flag set.

Default: `/usr/sbin/fping`

[comment]: # ({/dc27c714-1be0920c})

[comment]: # ({c72e665f-b516dcd6})
##### HistoryCacheSize

The size of the history cache, in bytes. The shared memory size for storing history data.

Default: `16M`<br>
Range: 128K-2G

[comment]: # ({/c72e665f-b516dcd6})

[comment]: # ({18425602-026e7a12})
##### HistoryIndexCacheSize

The size of the history index cache, in bytes. The shared memory size for indexing the history data stored in history cache. The index cache size needs roughly 100 bytes to cache one item.

Default: `4M`<br>
Range: 128K-2G

[comment]: # ({/18425602-026e7a12})

[comment]: # ({8febf51b-6281710b})
##### Hostname

A unique, case sensitive proxy name. Make sure the proxy name is known to the server.<br>Allowed characters: alphanumeric, '.', ' ', '\_' and '-'. Maximum length: 128

Default: Set by HostnameItem

[comment]: # ({/8febf51b-6281710b})

[comment]: # ({88920265-76df478d})
##### HostnameItem

The item used for setting Hostname if it is undefined (this will be run on the proxy similarly as on an agent). Ignored if Hostname is set.<br>Does not support UserParameters, performance counters or aliases, but does support system.run\[\].

Default: system.hostname

[comment]: # ({/88920265-76df478d})

[comment]: # ({bf3e0c31-44c82860})
##### HousekeepingFrequency

How often Zabbix will perform housekeeping procedure (in hours). Housekeeping is removing outdated information from the database.<br>*Note*: To lower load on proxy startup housekeeping is postponed for 30 minutes after proxy start. Thus, if HousekeepingFrequency is 1, the very first housekeeping procedure after proxy start will run after 30 minutes, and will repeat every hour thereafter.<br>Since Zabbix 3.0.0 it is possible to disable automatic housekeeping by setting HousekeepingFrequency to 0. In this case the housekeeping procedure can only be started by *housekeeper\_execute* runtime control option.

Default: `1`<br>
Range: 0-24

[comment]: # ({/bf3e0c31-44c82860})

[comment]: # ({60e43663-b4d0a8d4})
##### Include

You may include individual files or all files in a directory in the configuration file.<br>To only include relevant files in the specified directory, the asterisk wildcard character is supported for pattern matching.<br>See [special notes](special_notes_include) about limitations.

Example:

    Include=/absolute/path/to/config/files/*.conf

[comment]: # ({/60e43663-b4d0a8d4})

[comment]: # ({764f58a4-39256ab5})
##### JavaGateway

The IP address (or hostname) of Zabbix Java gateway. Only required if Java pollers are started.

[comment]: # ({/764f58a4-39256ab5})

[comment]: # ({9e49dbe0-d68c6bf4})
##### JavaGatewayPort

The port that Zabbix Java gateway listens on.

Default: `10052`<br>
Range: 1024-32767

[comment]: # ({/9e49dbe0-d68c6bf4})

[comment]: # ({fcd70e26-8ac8d5c2})
##### ListenBacklog

The maximum number of pending connections in the TCP queue.<br>The default value is a hard-coded constant, which depends on the system.<br>The maximum supported value depends on the system, too high values may be silently truncated to the 'implementation-specified maximum'.

Default: `SOMAXCONN`<br>
Range: 0 - INT\_MAX

[comment]: # ({/fcd70e26-8ac8d5c2})

[comment]: # ({1baa7180-2f753245})
##### ListenIP

A list of comma-delimited IP addresses that the trapper should listen on.<br>Trapper will listen on all network interfaces if this parameter is missing.

Default: `0.0.0.0`

[comment]: # ({/1baa7180-2f753245})

[comment]: # ({e2d7152a-8d927671})
##### ListenPort

The listen port for trapper.

Default: `10051`<br>
Range: 1024-32767

[comment]: # ({/e2d7152a-8d927671})

[comment]: # ({870c08f9-0cc86b53})
##### LoadModule

The module to load at proxy startup. Modules are used to extend the functionality of the proxy. The module must be located in the directory specified by LoadModulePath or the path must precede the module name. If the preceding path is absolute (starts with '/') then LoadModulePath is ignored.<br>Formats:<br>LoadModule=<module.so><br>LoadModule=<path/module.so><br>LoadModule=</abs\_path/module.so><br>It is allowed to include multiple LoadModule parameters.

[comment]: # ({/870c08f9-0cc86b53})

[comment]: # ({e5785fc6-7cb0d475})
##### LoadModulePath

The full path to the location of proxy modules. The default depends on compilation options.

[comment]: # ({/e5785fc6-7cb0d475})

[comment]: # ({ce9b40ca-4c7ecf23})
##### LogFile

The name of the log file.

Mandatory: Yes, if LogType is set to *file*; otherwise no

[comment]: # ({/ce9b40ca-4c7ecf23})

[comment]: # ({f7604f8f-13e6856d})
##### LogFileSize

The maximum size of a log file in MB.<br>0 - disable automatic log rotation.<br>*Note*: If the log file size limit is reached and file rotation fails, for whatever reason, the existing log file is truncated and started anew.

Default: `1`<br>
Range: 0-1024

[comment]: # ({/f7604f8f-13e6856d})

[comment]: # ({12ab855e-333e6ac4})
##### LogRemoteCommands

Enable the logging of executed shell commands as warnings.

Default: `0`<br>
Values: 0 - disabled, 1 - enabled

[comment]: # ({/12ab855e-333e6ac4})

[comment]: # ({ae46be35-3d118128})
##### LogType

The type of the log output:<br>*file* - write log to the file specified by LogFile parameter;<br>*system* - write log to syslog;<br>*console* - write log to standard output.

Default: `file`

[comment]: # ({/ae46be35-3d118128})

[comment]: # ({f96fa106-c4c01b03})
##### LogSlowQueries

How long a database query may take before being logged (in milliseconds).<br>0 - don't log slow queries.<br>This option becomes enabled starting with DebugLevel=3.

Default: `0`<br>
Range: 0-3600000

[comment]: # ({/f96fa106-c4c01b03})

[comment]: # ({2305112e-3bbd77c8})
##### PidFile

The name of the PID file.

Default: `/tmp/zabbix_proxy.pid`

[comment]: # ({/2305112e-3bbd77c8})

[comment]: # ({c639deb9-1289de66})
##### ProxyConfigFrequency

How often the proxy retrieves configuration data from Zabbix server in seconds.<br>Active proxy parameter. Ignored for passive proxies (see ProxyMode parameter).

Default: `10`<br>
Range: 1-604800

[comment]: # ({/c639deb9-1289de66})

[comment]: # ({57b06e6c-a8e60c77})
##### ProxyLocalBuffer

The proxy will keep data locally for N hours, even if the data have already been synced with the server.<br>This parameter may be used if local data will be used by third-party applications.

Default: `0`<br>
Range: 0-720

[comment]: # ({/57b06e6c-a8e60c77})

[comment]: # ({35b7f590-9ff179d3})
##### ProxyMode

The proxy operating mode.<br>0 - proxy in the active mode<br>1 - proxy in the passive mode<br>*Note* that (sensitive) proxy configuration data may become available to parties having access to the Zabbix server trapper port when using an active proxy. This is possible because anyone may pretend to be an active proxy and request configuration data; authentication does not take place.

Default: `0`<br>
Range: 0-1

[comment]: # ({/35b7f590-9ff179d3})

[comment]: # ({1ec92a93-de891fc0})
##### ProxyOfflineBuffer

The proxy will keep data for N hours in case of no connectivity with Zabbix server.<br>Older data will be lost.

Default: `1`<br>
Range: 1-720

[comment]: # ({/1ec92a93-de891fc0})

[comment]: # ({7bd81f67-a5cda34e})
##### Server

If ProxyMode is set to *active mode*:<br>Zabbix server IP address or DNS name (address:port) or [cluster](/manual/concepts/server/ha) (address:port;address2:port) to get configuration data from and send data to.<br>If port is not specified, the default port is used.<br>Cluster nodes must be separated by a semicolon.<br><br>If ProxyMode is set to *passive mode*:<br>List of comma delimited IP addresses, optionally in CIDR notation, or DNS names of Zabbix server. Incoming connections will be accepted only from the addresses listed here. If IPv6 support is enabled then '127.0.0.1', '::127.0.0.1', '::ffff:127.0.0.1' are treated equally.<br>'::/0' will allow any IPv4 or IPv6 address. '0.0.0.0/0' can be used to allow any IPv4 address.

Example: 

    Server=127.0.0.1,192.168.1.0/24,::1,2001:db8::/32,zabbix.example.com

Mandatory: yes

[comment]: # ({/7bd81f67-a5cda34e})

[comment]: # ({e15a3bf9-6984d380})
##### SNMPTrapperFile

A temporary file used for passing data from the SNMP trap daemon to the proxy.<br>Must be the same as in zabbix\_trap\_receiver.pl or SNMPTT configuration file.

Default: `/tmp/zabbix_traps.tmp`

[comment]: # ({/e15a3bf9-6984d380})

[comment]: # ({a20391fc-8ab25b82})
##### SocketDir

The directory to store IPC sockets used by internal Zabbix services.

Default: `/tmp`

[comment]: # ({/a20391fc-8ab25b82})

[comment]: # ({973cc739-94ec4682})
##### SourceIP

The source IP address for:<br>- outgoing connections to Zabbix server;<br>- agentless connections (VMware, SSH, JMX, SNMP, Telnet and simple checks);<br>- HTTP agent connections;<br>- script item JavaScript HTTP requests;<br>- preprocessing JavaScript HTTP requests;<br>- connections to the Vault

[comment]: # ({/973cc739-94ec4682})

[comment]: # ({a052566c-d3fc6fd2})
##### SSHKeyLocation

The location of public and private keys for SSH checks and actions.

[comment]: # ({/a052566c-d3fc6fd2})

[comment]: # ({90f62c3d-752b95c3})
##### SSLCertLocation

The location of SSL client certificate files for client authentication.<br>This parameter is used in web monitoring only.

[comment]: # ({/90f62c3d-752b95c3})

[comment]: # ({da3d2925-005ddd3a})
##### SSLKeyLocation

The location of SSL private key files for client authentication.<br>This parameter is used in web monitoring only.

[comment]: # ({/da3d2925-005ddd3a})

[comment]: # ({1be5ff4f-9e6c8051})
##### SSLCALocation

The location of certificate authority (CA) files for SSL server certificate verification.<br>Note that the value of this parameter will be set as the CURLOPT\_CAPATH libcurl option. For libcurl versions before 7.42.0, this only has effect if libcurl was compiled to use OpenSSL. For more information see the [cURL web page](http://curl.haxx.se/libcurl/c/CURLOPT_CAPATH.html).<br>This parameter is used in web monitoring and in SMTP authentication.

[comment]: # ({/1be5ff4f-9e6c8051})

[comment]: # ({790c0b86-13933066})
##### StartDBSyncers

The number of pre-forked instances of [history syncers](/manual/concepts/proxy#proxy_process_types).<br>*Note*: Be careful when changing this value, increasing it may do more harm than good.

Default: `4`<br>
Range: 1-100

[comment]: # ({/790c0b86-13933066})

[comment]: # ({e4e80449-2d92ef95})
##### StartDiscoverers

The number of pre-forked instances of [discovery workers](/manual/concepts/proxy#proxy_process_types).

Default: `5`<br>
Range: 0-1000

[comment]: # ({/e4e80449-2d92ef95})

[comment]: # ({9558ca6a-4493a1db})
##### StartHTTPPollers

The number of pre-forked instances of [HTTP pollers](/manual/concepts/proxy#proxy_process_types).

Default: `1` | Range: 0-1000

[comment]: # ({/9558ca6a-4493a1db})

[comment]: # ({e11c5d73-bff9bcdc})
##### StartIPMIPollers

The number of pre-forked instances of [IPMI pollers](/manual/concepts/proxy#proxy_process_types).

Default: `0`<br>
Range: 0-1000

[comment]: # ({/e11c5d73-bff9bcdc})

[comment]: # ({98cc822e-9f5d0d89})
##### StartJavaPollers

The number of pre-forked instances of [Java pollers](/manual/concepts/proxy#proxy_process_types).

Default: `0`<br>
Range: 0-1000

[comment]: # ({/98cc822e-9f5d0d89})

[comment]: # ({97d3acc7-e8b76471})
##### StartODBCPollers

The number of pre-forked instances of [ODBC pollers](/manual/concepts/proxy#proxy_process_types).

Default: `1`<br>
Range: 0-1000

[comment]: # ({/97d3acc7-e8b76471})


[comment]: # ({c1d83c4b-fa161a46})
##### StartPingers

The number of pre-forked instances of [ICMP pingers](/manual/concepts/proxy#proxy_process_types).

Default: `1`<br>
Range: 0-1000

[comment]: # ({/c1d83c4b-fa161a46})

[comment]: # ({acaf45bf-eef37c0d})
##### StartPollersUnreachable

The number of pre-forked instances of [pollers for unreachable hosts](/manual/concepts/proxy#proxy_process_types) (including IPMI and Java). At least one poller for unreachable hosts must be running if regular, IPMI or Java pollers are started.

Default: `1`<br>
Range: 0-1000

[comment]: # ({/acaf45bf-eef37c0d})

[comment]: # ({9149d7db-9d7927c0})
##### StartPollers

The number of pre-forked instances of [pollers](/manual/concepts/proxy#proxy_process_types).

Default: `5`<br>
Range: 0-1000

[comment]: # ({/9149d7db-9d7927c0})

[comment]: # ({27a142e3-8e40eddf})
##### StartPreprocessors

The number of pre-started instances of preprocessing [workers](/manual/concepts/proxy#proxy_process_types).

Default: `3`<br>
Range: 1-1000

[comment]: # ({/27a142e3-8e40eddf})

[comment]: # ({4c6c9953-43ff6725})
##### StartSNMPTrapper

If set to 1, an [SNMP trapper](/manual/concepts/proxy#proxy_process_types) process will be started.

Default: `0`<br>
Range: 0-1

[comment]: # ({/4c6c9953-43ff6725})

[comment]: # ({0078a397-3943d442})
##### StartTrappers

The number of pre-forked instances of [trappers](/manual/concepts/proxy#proxy_process_types).<br>Trappers accept incoming connections from Zabbix sender and active agents.

Default: `5`<br>
Range: 0-1000

[comment]: # ({/0078a397-3943d442})

[comment]: # ({bed9e17f-3a4ddabb})
##### StartVMwareCollectors

The number of pre-forked [VMware collector](/manual/concepts/proxy#proxy_process_types) instances.

Default: `0`<br>
Range: 0-250

[comment]: # ({/bed9e17f-3a4ddabb})

[comment]: # ({6c27cee6-32b0fe66})
##### StatsAllowedIP

A list of comma-delimited IP addresses, optionally in CIDR notation, or DNS names of external Zabbix instances. The stats request will be accepted only from the addresses listed here. If this parameter is not set no stats requests will be accepted.<br>If IPv6 support is enabled then '127.0.0.1', '::127.0.0.1', '::ffff:127.0.0.1' are treated equally and '::/0' will allow any IPv4 or IPv6 address. '0.0.0.0/0' can be used to allow any IPv4 address.

Example:

    StatsAllowedIP=127.0.0.1,192.168.1.0/24,::1,2001:db8::/32,zabbix.example.com

[comment]: # ({/6c27cee6-32b0fe66})

[comment]: # ({28cc089d-136f6bfe})
##### Timeout

Specifies how long we wait for agent, SNMP device or external check in seconds.

Default: `3`<br>
Range: 1-30

[comment]: # ({/28cc089d-136f6bfe})

[comment]: # ({684523b1-b2420c82})
##### TLSAccept

What incoming connections to accept from Zabbix server. Used for a passive proxy, ignored on an active proxy. Multiple values can be specified, separated by comma:<br>*unencrypted* - accept connections without encryption (default)<br>*psk* - accept connections with TLS and a pre-shared key (PSK)<br>*cert* - accept connections with TLS and a certificate

Mandatory: yes for passive proxy, if TLS certificate or PSK parameters are defined (even for *unencrypted* connection); otherwise no

[comment]: # ({/684523b1-b2420c82})

[comment]: # ({ab7b9064-a1178240})
##### TLSCAFile

The full pathname of a file containing the top-level CA(s) certificates for peer certificate verification, used for encrypted communications between Zabbix components.

[comment]: # ({/ab7b9064-a1178240})

[comment]: # ({48468901-a8ec843d})
##### TLSCertFile
The full pathname of a file containing the proxy certificate or certificate chain, used for encrypted communications between Zabbix components.

[comment]: # ({/48468901-a8ec843d})

[comment]: # ({953e7fd5-c9ea574f})
##### TLSCipherAll

The GnuTLS priority string or OpenSSL (TLS 1.2) cipher string. Override the default ciphersuite selection criteria for certificate- and PSK-based encryption.

Example:

    TLS_AES_256_GCM_SHA384:TLS_CHACHA20_POLY1305_SHA256:TLS_AES_128_GCM_SHA256

[comment]: # ({/953e7fd5-c9ea574f})

[comment]: # ({832946e7-985d238a})
##### TLSCipherAll13

The cipher string for OpenSSL 1.1.1 or newer in TLS 1.3. Override the default ciphersuite selection criteria for certificate- and PSK-based encryption.

Example for GnuTLS: 

    NONE:+VERS-TLS1.2:+ECDHE-RSA:+RSA:+ECDHE-PSK:+PSK:+AES-128-GCM:+AES-128-CBC:+AEAD:+SHA256:+SHA1:+CURVE-ALL:+COMP-NULL::+SIGN-ALL:+CTYPE-X.509

Example for OpenSSL: 

    EECDH+aRSA+AES128:RSA+aRSA+AES128:kECDHEPSK+AES128:kPSK+AES128

[comment]: # ({/832946e7-985d238a})

[comment]: # ({c9cf41e3-ccd8c768})
##### TLSCipherCert

The GnuTLS priority string or OpenSSL (TLS 1.2) cipher string. Override the default ciphersuite selection criteria for certificate-based encryption.

Example for GnuTLS: 

    NONE:+VERS-TLS1.2:+ECDHE-RSA:+RSA:+AES-128-GCM:+AES-128-CBC:+AEAD:+SHA256:+SHA1:+CURVE-ALL:+COMP-NULL:+SIGN-ALL:+CTYPE-X.509

Example for OpenSSL: 

    EECDH+aRSA+AES128:RSA+aRSA+AES128

[comment]: # ({/c9cf41e3-ccd8c768})

[comment]: # ({5956ba54-d746a953})
##### TLSCipherCert13

The cipher string for OpenSSL 1.1.1 or newer in TLS 1.3. Override the default ciphersuite selection criteria for certificate-based encryption.

[comment]: # ({/5956ba54-d746a953})

[comment]: # ({038b665a-5d6b530a})
##### TLSCipherPSK

The GnuTLS priority string or OpenSSL (TLS 1.2) cipher string. Override the default ciphersuite selection criteria for PSK-based encryption.

Example for GnuTLS: 

    NONE:+VERS-TLS1.2:+ECDHE-PSK:+PSK:+AES-128-GCM:+AES-128-CBC:+AEAD:+SHA256:+SHA1:+CURVE-ALL:+COMP-NULL:+SIGN-ALL

Example for OpenSSL: 

    kECDHEPSK+AES128:kPSK+AES128

[comment]: # ({/038b665a-5d6b530a})

[comment]: # ({b96d7f16-afe5c125})
##### TLSCipherPSK13

The cipher string for OpenSSL 1.1.1 or newer in TLS 1.3. Override the default ciphersuite selection criteria for PSK-based encryption.

Example:

    TLS_CHACHA20_POLY1305_SHA256:TLS_AES_128_GCM_SHA256

[comment]: # ({/b96d7f16-afe5c125})

[comment]: # ({b7dc9557-b6a5938b})
##### TLSConnect

How the proxy should connect to Zabbix server. Used for an active proxy, ignored on a passive proxy. Only one value can be specified:<br>*unencrypted* - connect without encryption (default)<br>*psk* - connect using TLS and a pre-shared key (PSK)<br>*cert* - connect using TLS and a certificate

Mandatory: yes for active proxy, if TLS certificate or PSK parameters are defined (even for *unencrypted* connection); otherwise no

[comment]: # ({/b7dc9557-b6a5938b})

[comment]: # ({139c3a4e-4ff06d66})
##### TLSCRLFile

The full pathname of a file containing revoked certificates. This parameter is used for encrypted communications between Zabbix components.

[comment]: # ({/139c3a4e-4ff06d66})

[comment]: # ({4a3bd5a8-2cc39b48})
##### TLSKeyFile

The full pathname of a file containing the proxy private key, used for encrypted communications between Zabbix components.

[comment]: # ({/4a3bd5a8-2cc39b48})

[comment]: # ({0f141bf6-0f2bf96e})
##### TLSPSKFile

The full pathname of a file containing the proxy pre-shared key, used for encrypted communications with Zabbix server.

[comment]: # ({/0f141bf6-0f2bf96e})

[comment]: # ({92d3eff0-b57e989d})
##### TLSPSKIdentity

The pre-shared key identity string, used for encrypted communications with Zabbix server.

[comment]: # ({/92d3eff0-b57e989d})

[comment]: # ({dbc419d0-c8d8910c})
##### TLSServerCertIssuer

The allowed server certificate issuer.

[comment]: # ({/dbc419d0-c8d8910c})

[comment]: # ({6c82e1dd-89726b49})
##### TLSServerCertSubject

The allowed server certificate subject.

[comment]: # ({/6c82e1dd-89726b49})

[comment]: # ({ddb80460-18ebe014})
##### TmpDir

The temporary directory.

Default: `/tmp`

[comment]: # ({/ddb80460-18ebe014})

[comment]: # ({0749878e-4880108e})
##### TrapperTimeout

How many seconds the trapper may spend processing new data.

Default: `300`<br>
Range: 1-300

[comment]: # ({/0749878e-4880108e})

[comment]: # ({0c8881b6-4d4f80c6})
##### UnavailableDelay

How often a host is checked for availability during the [unavailability](/manual/appendix/items/unreachability#unavailable_host) period in seconds.

Default: `60`<br>
Range: 1-3600

[comment]: # ({/0c8881b6-4d4f80c6})

[comment]: # ({ee2d0004-fad94d05})
##### UnreachableDelay

How often a host is checked for availability during the [unreachability](/manual/appendix/items/unreachability#unreachable_host) period in seconds.

Default: `15`<br>
Range: 1-3600

[comment]: # ({/ee2d0004-fad94d05})

[comment]: # ({977cabfa-316715df})
##### UnreachablePeriod

After how many seconds of [unreachability](/manual/appendix/items/unreachability#unreachable_host) treat a host as unavailable.

Default: `45`<br>
Range: 1-3600

[comment]: # ({/977cabfa-316715df})

[comment]: # ({ee4ad0a4-12e5d15b})
##### User

Drop privileges to a specific, existing user on the system.<br>Only has effect if run as 'root' and AllowRoot is disabled.

Default: `zabbix`

[comment]: # ({/ee4ad0a4-12e5d15b})

[comment]: # ({8c34598c-df11844d})
##### Vault

The vault provider:<br>*HashiCorp* - HashiCorp KV Secrets Engine version 2<br>*CyberArk*  - CyberArk Central Credential Provider<br>Must match the vault provider set in the frontend.

Default: `HashiCorp`

[comment]: # ({/8c34598c-df11844d})

[comment]: # ({5f2268f4-b096e1d2})
##### VaultDBPath

The location from where database credentials should be retrieved by keys. Depending on the vault, can be vault path or query.<br> The keys used for HashiCorp are 'password' and 'username'. 

Example:

    secret/zabbix/database

The keys used for CyberArk are 'Content' and 'UserName'.

Example:

    AppID=zabbix_server&Query=Safe=passwordSafe;Object=zabbix_proxy_database

This option can only be used if DBUser and DBPassword are not specified.

[comment]: # ({/5f2268f4-b096e1d2})

[comment]: # ({a5fcdc1e-2a2c177c})
##### VaultTLSCertFile

The name of the SSL certificate file used for client authentication. The certificate file must be in PEM1 format.<br>If the certificate file contains also the private key, leave the SSL key file field empty.<br>The directory containing this file is specified by the SSLCertLocation configuration parameter.<br>This option can be omitted, but is recommended for CyberArkCCP vault.

[comment]: # ({/a5fcdc1e-2a2c177c})

[comment]: # ({c010ab45-7d66e718})
##### VaultTLSKeyFile

The name of the SSL private key file used for client authentication. The private key file must be in PEM1 format.<br>The directory containing this file is specified by the SSLKeyLocation configuration parameter.<br>This option can be omitted, but is recommended for CyberArkCCP vault.

[comment]: # ({/c010ab45-7d66e718})


[comment]: # ({1d9b0801-54de6e11})
##### VaultToken

The HashiCorp vault authentication token that should have been generated exclusively for Zabbix proxy with read-only permission to the path specified in the optional VaultDBPath configuration parameter.<br>It is an error if VaultToken and the VAULT\_TOKEN environment variable are defined at the same time.

Mandatory: Yes, if Vault is set to *HashiCorp*; otherwise no

[comment]: # ({/1d9b0801-54de6e11})

[comment]: # ({73818a04-0ad4c413})
##### VaultURL

The vault server HTTP\[S\] URL. The system-wide CA certificates directory will be used if SSLCALocation is not specified.

Default: `https://127.0.0.1:8200`

[comment]: # ({/73818a04-0ad4c413})

[comment]: # ({141b81e3-25ec9d34})
##### VMwareCacheSize

The shared memory size for storing VMware data.<br>A VMware internal check zabbix\[vmware,buffer,...\] can be used to monitor the VMware cache usage (see [Internal checks](/manual/config/items/itemtypes/internal)).<br>Note that shared memory is not allocated if there are no vmware collector instances configured to start.

Default: `8M`<br>
Range: 256K-2G

[comment]: # ({/141b81e3-25ec9d34})

[comment]: # ({fefd3f8c-cd0433e9})
##### VMwareFrequency

The delay in seconds between data gathering from a single VMware service.<br>This delay should be set to the least update interval of any VMware monitoring item.

Default: `60`<br>
Range: 10-86400

[comment]: # ({/fefd3f8c-cd0433e9})

[comment]: # ({9668820d-4eb71305})
##### VMwarePerfFrequency

The delay in seconds between performance counter statistics retrieval from a single VMware service.<br>This delay should be set to the least update interval of any VMware monitoring [item](/manual/config/items/itemtypes/simple_checks/vmware_keys#footnotes) that uses VMware performance counters.

Default: `60`<br>
Range: 10-86400

[comment]: # ({/9668820d-4eb71305})

[comment]: # ({80852117-c2a39b44})
##### VMwareTimeout

The maximum number of seconds a vmware collector will wait for a response from VMware service (vCenter or ESX hypervisor).

Default: `10`<br>
Range: 1-300

[comment]: # ({/80852117-c2a39b44})
