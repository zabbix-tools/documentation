[comment]: # attributes: notoc

[comment]: # ({4963afdf-4963afdf})
# 6 Zabbix agent 2 (Windows)

[comment]: # ({/4963afdf-4963afdf})

[comment]: # ({a848e9e3-9fe069f9})
### Overview

Zabbix agent 2 is a new generation of Zabbix agent and may be used in
place of Zabbix agent.

The parameters supported by the Windows Zabbix agent 2 configuration file (zabbix\_agent2.win.conf) are listed in this section. 

The parameters are listed without additional information. Click on the parameter to see the full details.

|Parameter|Description|
|--|--------|
|[Alias](#alias)|Sets an alias for an item key.|
|[AllowKey](#allowkey)|Allow the execution of those item keys that match a pattern.|
|[BufferSend](#buffersend)|Do not keep data longer than N seconds in buffer.|
|[BufferSize](#buffersize)|The maximum number of values in the memory buffer.|
|[ControlSocket](#controlsocket)|The control socket, used to send runtime commands with the '-R' option.|
|[DebugLevel](#debuglevel)|The debug level.|
|[DenyKey](#denykey)|Deny the execution of those item keys that match a pattern.|
|[EnablePersistentBuffer](#enablepersistentbuffer)|Enable the usage of local persistent storage for active items.|
|[ForceActiveChecksOnStart](#forceactivechecksonstart)|Perform active checks immediately after the restart for the first received configuration.|
|[HeartbeatFrequency](#heartbeatfrequency)|The frequency of heartbeat messages in seconds.|
|[HostInterface](#hostinterface)|An optional parameter that defines the host interface.|
|[HostInterfaceItem](#hostinterfaceitem)|An optional parameter that defines an item used for getting the host interface.|
|[HostMetadata](#hostmetadata)|An optional parameter that defines the host metadata.|
|[HostMetadataItem](#hostmetadataitem)|An optional parameter that defines a Zabbix agent item used for getting the host metadata.|
|[Hostname](#hostname)|An optional parameter that defines the hostname.|
|[HostnameItem](#hostnameitem)|An optional parameter that defines a Zabbix agent item used for getting the hostname.|
|[Include](#include)|You may include individual files or all files in a directory in the configuration file.|
|[ListenIP](#listenip)|A list of comma-delimited IP addresses that the agent should listen on.|
|[ListenPort](#listenport)|The agent will listen on this port for connections from the server.|
|[LogFile](#logfile)|The name of the log file.|
|[LogFileSize](#logfilesize)|The maximum size of the log file.|
|[LogType](#logtype)|The type of the log output.|
|[PersistentBufferFile](#persistentbufferfile)|The file where Zabbix agent 2 should keep the SQLite database.|
|[PersistentBufferPeriod](#persistentbufferperiod)|The time period for which data should be stored when there is no connection to the server or proxy.|
|[Plugins.Log.MaxLinesPerSecond](#plugins.log.maxlinespersecond)|The maximum number of new lines the agent will send per second to Zabbix server or proxy when processing 'log' and 'logrt' active checks.|
|[Plugins.SystemRun.LogRemoteCommands](#plugins.systemrun.logremotecommands)|Enable the logging of the executed shell commands as warnings.|
|[PluginSocket](#pluginsocket)|The path to the UNIX socket for loadable plugin communications.|
|[PluginTimeout](#plugintimeout)|The timeout for connections with loadable plugins, in seconds.|
|[RefreshActiveChecks](#refreshactivechecks)|How often the list of active checks is refreshed.|
|[Server](#server)|A list of comma-delimited IP addresses, optionally in CIDR notation, or DNS names of Zabbix servers and Zabbix proxies.|
|[ServerActive](#serveractive)|The Zabbix server/proxy address or cluster configuration to get active checks from.|
|[SourceIP](#sourceip)|The source IP address.|
|[StatusPort](#statusport)|If set, the agent will listen on this port for HTTP status requests (http://localhost:<port>/status).|
|[Timeout](#timeout)|Spend no more than Timeout seconds on processing.|
|[TLSAccept](#tlsaccept)|What incoming connections to accept.|
|[TLSCAFile](#tlscafile)|The full pathname of a file containing the top-level CA(s) certificates for peer certificate verification, used for encrypted communications between Zabbix components.|
|[TLSCertFile](#tlscertfile)|The full pathname of a file containing the agent certificate or certificate chain, used for encrypted communications between Zabbix components.|
|[TLSConnect](#tlsconnect)|How the agent should connect to Zabbix server or proxy.|
|[TLSCRLFile](#tlscrlfile)|The full pathname of a file containing revoked certificates. This parameter is used for encrypted communications between Zabbix components.|
|[TLSKeyFile](#tlskeyfile)|The full pathname of a file containing the agent private key, used for encrypted communications between Zabbix components.|
|[TLSPSKFile](#tlspskfile)|The full pathname of a file containing the agent pre-shared key, used for encrypted communications with Zabbix server.|
|[TLSPSKIdentity](#tlspskidentity)|The pre-shared key identity string, used for encrypted communications with Zabbix server.|
|[TLSServerCertIssuer](#tlsservercertissuer)|The allowed server (proxy) certificate issuer.|
|[TLSServerCertSubject](#tlsservercertsubject)|The allowed server (proxy) certificate subject.|
|[UnsafeUserParameters](#unsafeuserparameters)|Allow all characters to be passed in arguments to user-defined parameters.|
|[UserParameter](#userparameter)|A user-defined parameter to monitor.|
|[UserParameterDir](#userparameterdir)|The default search path for UserParameter commands.|

All parameters are non-mandatory unless explicitly stated that the parameter is mandatory. 

Note that:

-   The default values reflect process defaults, not the values in the
    shipped configuration files;
-   Zabbix supports configuration files only in UTF-8 encoding without
    [BOM](https://en.wikipedia.org/wiki/Byte_order_mark);
-   Comments starting with "\#" are only supported in the beginning of
    the line.
<br>

[comment]: # ({/a848e9e3-9fe069f9})

[comment]: # ({8085875f-324d4c30})
### Parameter details

[comment]: # ({/8085875f-324d4c30})

[comment]: # ({5b774046-fa6993fd})
##### Alias

Sets an alias for an item key. It can be used to substitute a long and complex item key with a shorter and simpler one. Multiple *Alias* parameters with the same *Alias* key may be present.<br>Different *Alias* keys may reference the same item key.<br>Aliases can be used in *HostMetadataItem* but not in the *HostnameItem* parameter.

Example 1: Retrieving the paging file usage in percentage from the server.

    Alias=pg_usage:perf_counter[\Paging File(_Total)\% Usage]
    
Now the shorthand key **pg_usage** may be used to retrieve data.

Example 2: Getting the CPU load with default and custom parameters.

    Alias=cpu.load:system.cpu.load
    Alias=cpu.load[*]:system.cpu.load[*]

This allows use **cpu.load** key to get the CPU load with default parameters as well as use **cpu.load[percpu,avg15]** to get specific data about the CPU load.

Example 3: Running multiple [low-level discovery](/manual/discovery/low_level_discovery) rules processing the same discovery items.

    Alias=vfs.fs.discovery[*]:vfs.fs.discovery

Now it is possible to set up several discovery rules using **vfs.fs.discovery** with different parameters for each rule, e.g., **vfs.fs.discovery[foo]**, **vfs.fs.discovery[bar]**, etc.

[comment]: # ({/5b774046-fa6993fd})

[comment]: # ({6c104b7b-e05b8a23})
##### AllowKey

Allow the execution of those item keys that match a pattern. The key pattern is a wildcard expression that supports the "\*" character to match any number of any characters.<br>Multiple key matching rules may be defined in combination with DenyKey. The parameters are processed one by one according to their appearance order. See also: [Restricting agent checks](/manual/config/items/restrict_checks).

[comment]: # ({/6c104b7b-e05b8a23})

[comment]: # ({fc56d912-5d573779})
##### BufferSend

The time interval in seconds which determines how often values are sent from the buffer to Zabbix server.<br>Note, that if the buffer is full, the data will be sent sooner.

Default: `5`<br>
Range: 1-3600

[comment]: # ({/fc56d912-5d573779})

[comment]: # ({3a100014-adbf2018})
##### BufferSize

The maximum number of values in the memory buffer. The agent will send all collected data to the Zabbix server or proxy if the buffer is full.<br>This parameter should only be used if persistent buffer is disabled (*EnablePersistentBuffer=0*).

Default: `1000`<br>
Range: 2-65535

[comment]: # ({/3a100014-adbf2018})

[comment]: # ({78069119-dee4d084})
##### ControlSocket

The control socket, used to send runtime commands with the '-R' option.

Default: `\\.\pipe\agent.sock`

[comment]: # ({/78069119-dee4d084})

[comment]: # ({9c071f70-74594585})
##### DebugLevel

Specify the debug level:<br>*0* - basic information about starting and stopping of Zabbix processes<br>*1* - critical information;<br>*2* - error information;<br>*3* - warnings;<br>*4* - for debugging (produces lots of information);<br>*5* - extended debugging (produces even more information).

Default: `3`<br>
Range: 0-5

[comment]: # ({/9c071f70-74594585})

[comment]: # ({13d6d3a4-ad027e43})
##### DenyKey

Deny the execution of those item keys that match a pattern. The key pattern is a wildcard expression that supports the "\*" character to match any number of any characters.<br>Multiple key matching rules may be defined in combination with AllowKey. The parameters are processed one by one according to their appearance order. See also: [Restricting agent checks](/manual/config/items/restrict_checks).

[comment]: # ({/13d6d3a4-ad027e43})

[comment]: # ({52ac8f96-d9d538db})
##### EnablePersistentBuffer

Enable the usage of local persistent storage for active items. If persistent storage is disabled, the memory buffer will be used.

Default: `0`<br>
Values: 0 - disabled, 1 - enabled

[comment]: # ({/52ac8f96-d9d538db})

[comment]: # ({5fe3c615-1970c2c5})
##### ForceActiveChecksOnStart

Perform active checks immediately after the restart for the first received configuration. Also available as a per-plugin configuration parameter, for example: `Plugins.Uptime.System.ForceActiveChecksOnStart=1`

Default: `0`<br>
Values: 0 - disabled, 1 - enabled

[comment]: # ({/5fe3c615-1970c2c5})

[comment]: # ({dbb05958-bce527af})
##### HeartbeatFrequency

The frequency of heartbeat messages in seconds. Used for monitoring the availability of active checks.<br>0 - heartbeat messages disabled.

Default: `60`<br>
Range: 0-3600

[comment]: # ({/dbb05958-bce527af})

[comment]: # ({48de1a7d-9d3624af})
##### HostInterface

An optional parameter that defines the host interface. The host interface is used at host [autoregistration](/manual/discovery/auto_registration#using_dns_as_default_interface) process. If not defined, the value will be acquired from HostInterfaceItem.<br>The agent will issue an error and not start if the value is over the limit of 255 characters.

Range: 0-255 characters

[comment]: # ({/48de1a7d-9d3624af})

[comment]: # ({c63e1550-7378e07f})
##### HostInterfaceItem

An optional parameter that defines an item used for getting the host interface.<br>Host interface is used at host [autoregistration](/manual/discovery/auto_registration#using_dns_as_default_interface) process. This option is only used when HostInterface is not defined.<br>The system.run\[\] item is supported regardless of AllowKey/DenyKey values.<br>During an autoregistration request the agent will log a warning message if the value returned by the specified item is over the limit of 255 characters.

[comment]: # ({/c63e1550-7378e07f})

[comment]: # ({5bcd7757-3b42353f})
##### HostMetadata

An optional parameter that defines host metadata. Host metadata is used only at host autoregistration process (active agent). If not defined, the value will be acquired from HostMetadataItem.<br>The agent will issue an error and not start if the specified value is over the limit of 2034 bytes or a non-UTF-8 string.

Range: 0-2034 bytes

[comment]: # ({/5bcd7757-3b42353f})

[comment]: # ({b6039f1d-eff97518})
##### HostMetadataItem

An optional parameter that defines an item used for getting host metadata. This option is only used when HostMetadata is not defined. User parameters and aliases are supported. The system.run\[\] item is supported regardless of AllowKey/DenyKey values.<br>The HostMetadataItem value is retrieved on each autoregistration attempt and is used only at host autoregistration process.<br>During an autoregistration request the agent will log a warning message if the value returned by the specified item is over the limit of 65535 UTF-8 code points. The value returned by the item must be a UTF-8 string otherwise it will be ignored.

[comment]: # ({/b6039f1d-eff97518})

[comment]: # ({6e272ef0-542b591a})
##### Hostname

A list of comma-delimited, unique, case-sensitive hostnames. Required for active checks and must match hostnames as configured on the server. The value is acquired from HostnameItem if undefined.<br>Allowed characters: alphanumeric, '.', ' ', '\_' and '-'. Maximum length: 128 characters per hostname, 2048 characters for the entire line.

Default: Set by HostnameItem

[comment]: # ({/6e272ef0-542b591a})

[comment]: # ({832d0a32-085b8ddc})
##### HostnameItem

An optional parameter that defines an item used for getting the host name. This option is only used when Hostname is not defined. User parameters or aliases are not supported, but the system.run\[\] item is supported regardless of AllowKey/DenyKey values.<br>The output length is limited to 512KB.

Default: `system.hostname`

[comment]: # ({/832d0a32-085b8ddc})

[comment]: # ({2660fe03-d06cc342})
##### Include

You may include individual files or all files in a directory in the configuration file. During the installation Zabbix will create the include directory in /usr/local/etc, unless modified during the compile time. The path can be relative to the *zabbix\_agent2.win.conf* file location.<br>To only include relevant files in the specified directory, the asterisk wildcard character is supported for pattern matching.<br>See [special notes](special_notes_include) about limitations.

Example:

    Include=C:\Program Files\Zabbix Agent\zabbix_agent2.d\*.conf

[comment]: # ({/2660fe03-d06cc342})

[comment]: # ({f710f108-21abf7b1})
##### ListenIP

A list of comma-delimited IP addresses that the agent should listen on. The first IP address is sent to the Zabbix server, if connecting to it, to retrieve the list of active checks.

Default: `0.0.0.0`

[comment]: # ({/f710f108-21abf7b1})

[comment]: # ({5f95bc53-2f2c8140})
##### ListenPort

The agent will listen on this port for connections from the server.

Default: `10050`<br>
Range: 1024-32767

[comment]: # ({/5f95bc53-2f2c8140})

[comment]: # ({1c88a5a6-988826b6})
##### LogFile

The name of the agent log file.

Default: `c:\\zabbix_agent2.log`<br>
Mandatory: Yes, if LogType is set to *file*; otherwise no

[comment]: # ({/1c88a5a6-988826b6})

[comment]: # ({f7604f8f-ae05fc8d})
##### LogFileSize

The maximum size of a log file in MB.<br>0 - disable automatic log rotation.<br>*Note*: If the log file size limit is reached and file rotation fails, for whatever reason, the existing log file is truncated and started anew.

Default: `1`<br>
Range: 0-1024

[comment]: # ({/f7604f8f-ae05fc8d})

[comment]: # ({4226072e-e8dc6df9})
##### LogType

The type of the log output:<br>*file* - write log to the file specified by LogFile parameter;<br>*console* - write log to standard output.

Default: `file`

[comment]: # ({/4226072e-e8dc6df9})

[comment]: # ({de1f0041-dde3e6b9})
##### PersistentBufferFile

The file where Zabbix agent 2 should keep the SQLite database. Must be a full filename. This parameter is only used if persistent buffer is enabled (*EnablePersistentBuffer=1*).

[comment]: # ({/de1f0041-dde3e6b9})

[comment]: # ({4c6b037f-379dc265})
##### PersistentBufferPeriod

The time period for which data should be stored when there is no connection to the server or proxy. Older data will be lost. Log data will be preserved. This parameter is only used if persistent buffer is enabled (*EnablePersistentBuffer=1*).

Default: `1h`<br>
Range: 1m-365d

[comment]: # ({/4c6b037f-379dc265})

[comment]: # ({d537c2b6-c6daef84})
##### Plugins.Log.MaxLinesPerSecond

The maximum number of new lines the agent will send per second to Zabbix server or proxy when processing 'log', 'logrt' and 'eventlog' active checks. The provided value will be overridden by the 'maxlines' parameter, provided in the 'log', 'logrt' or 'eventlog' item key.<br>*Note*: Zabbix will process 10 times more new lines than set in *MaxLinesPerSecond* to seek the required string in log items.

Default: `20`<br>
Range: 1-1000

[comment]: # ({/d537c2b6-c6daef84})

[comment]: # ({62fb8c26-d7133c6e})
##### Plugins.SystemRun.LogRemoteCommands

Enable the logging of the executed shell commands as warnings. The commands will be logged only if executed remotely. Log entries will not be created if system.run\[\] is launched locally by the HostMetadataItem, HostInterfaceItem or HostnameItem parameters.

Default: `0`<br>
Values: 0 - disabled, 1 - enabled

[comment]: # ({/62fb8c26-d7133c6e})

[comment]: # ({9450d7ef-c712bfb8})
##### PluginSocket

The path to the UNIX socket for loadable plugin communications.

Default: `\\.\pipe\agent.plugin.sock`

[comment]: # ({/9450d7ef-c712bfb8})

[comment]: # ({14afe8ce-1db534a6})
##### PluginTimeout

The timeout for connections with loadable plugins, in seconds.

Default: `Timeout`<br>
Range: 1-30

[comment]: # ({/14afe8ce-1db534a6})

[comment]: # ({d3de4e11-25e3871b})
##### RefreshActiveChecks

How often the list of active checks is refreshed, in seconds. Note that after failing to refresh active checks the next refresh will be attempted in 60 seconds.

Default: `5`<br>
Range: 1-86400

[comment]: # ({/d3de4e11-25e3871b})

[comment]: # ({22be08ff-0ad80cbc})
##### Server

A list of comma-delimited IP addresses, optionally in CIDR notation, or DNS names of Zabbix servers or Zabbix proxies. Incoming connections will be accepted only from the hosts listed here. If IPv6 support is enabled then '127.0.0.1', '::127.0.0.1', '::ffff:127.0.0.1' are treated equally and '::/0' will allow any IPv4 or IPv6 address. '0.0.0.0/0' can be used to allow any IPv4 address. Spaces are allowed.

Example: 

    Server=127.0.0.1,192.168.1.0/24,::1,2001:db8::/32,zabbix.example.com

Mandatory: yes

[comment]: # ({/22be08ff-0ad80cbc})

[comment]: # ({dc746b77-0d31e03a})
##### ServerActive

The Zabbix server/proxy address or cluster configuration to get active checks from. The server/proxy address is an IP address or DNS name and optional port separated by colon.<br>The cluster configuration is one or more server addresses separated by semicolon. Multiple Zabbix servers/clusters and Zabbix proxies can be specified, separated by comma. More than one Zabbix proxy should not be specified from each Zabbix server/cluster. If a Zabbix proxy is specified then Zabbix server/cluster for that proxy should not be specified.<br>Multiple comma-delimited addresses can be provided to use several independent Zabbix servers in parallel. Spaces are allowed.<br>If the port is not specified, default port is used.<br>IPv6 addresses must be enclosed in square brackets if port for that host is specified. If port is not specified, square brackets for IPv6 addresses are optional.<br>If this parameter is not specified, active checks are disabled.

Example for Zabbix proxy: 

    ServerActive=127.0.0.1:10051

Example for multiple servers: 

    ServerActive=127.0.0.1:20051,zabbix.domain,\[::1\]:30051,::1,\[12fc::1\]

Example for high availability:

    ServerActive=zabbix.cluster.node1;zabbix.cluster.node2:20051;zabbix.cluster.node3

Example for high availability with two clusters and one server:

    ServerActive=zabbix.cluster.node1;zabbix.cluster.node2:20051,zabbix.cluster2.node1;zabbix.cluster2.node2,zabbix.domain

[comment]: # ({/dc746b77-0d31e03a})

[comment]: # ({3628af0a-f96761ad})
##### SourceIP

The source IP address for:<br>- outgoing connections to Zabbix server or Zabbix proxy;<br>- making connections while executing some items (web.page.get, net.tcp.port, etc.).

[comment]: # ({/3628af0a-f96761ad})

[comment]: # ({7d3f343b-8b9a658a})
##### StatusPort

If set, the agent will listen on this port for HTTP status requests (http://localhost:<port>/status).

Range: 1024-32767

[comment]: # ({/7d3f343b-8b9a658a})

[comment]: # ({1b1125a4-baa42b03})
##### Timeout

Spend no more than Timeout seconds on processing.

Default: `3`<br>
Range: 1-30

[comment]: # ({/1b1125a4-baa42b03})

[comment]: # ({412cbce9-5fffe23e})
##### TLSAccept

The incoming connections to accept. Used for passive checks. Multiple values can be specified, separated by comma:<br>*unencrypted* - accept connections without encryption (default)<br>*psk* - accept connections with TLS and a pre-shared key (PSK)<br>*cert* - accept connections with TLS and a certificate

Mandatory: yes, if TLS certificate or PSK parameters are defined (even for *unencrypted* connection); otherwise no

[comment]: # ({/412cbce9-5fffe23e})

[comment]: # ({65d07bda-01a58fb9})
##### TLSCAFile

The full pathname of the file containing the top-level CA(s) certificates for peer certificate verification, used for encrypted communications between Zabbix components.

[comment]: # ({/65d07bda-01a58fb9})

[comment]: # ({005f60f1-64ff2b38})
##### TLSCertFile

The full pathname of the file containing the agent certificate or certificate chain, used for encrypted communications with Zabbix components.

[comment]: # ({/005f60f1-64ff2b38})

[comment]: # ({be99c4c7-0c14341a})
##### TLSConnect

How the agent should connect to Zabbix server or proxy. Used for active checks. Only one value can be specified:<br>*unencrypted* - connect without encryption (default)<br>*psk* - connect using TLS and a pre-shared key (PSK)<br>*cert* - connect using TLS and a certificate

Mandatory: yes, if TLS certificate or PSK parameters are defined (even for *unencrypted* connection); otherwise no

[comment]: # ({/be99c4c7-0c14341a})

[comment]: # ({94a4b758-353fa6b2})
##### TLSCRLFile

The full pathname of the file containing revoked certificates. This parameter is used for encrypted communications between Zabbix components.

[comment]: # ({/94a4b758-353fa6b2})

[comment]: # ({c2aa4315-40db7da5})
##### TLSKeyFile

The full pathname of the file containing the agent private key, used for encrypted communications between Zabbix components.

[comment]: # ({/c2aa4315-40db7da5})

[comment]: # ({11defa44-216346d5})
##### TLSPSKFile

The full pathname of the file containing the agent pre-shared key, used for encrypted communications with Zabbix server.

[comment]: # ({/11defa44-216346d5})

[comment]: # ({92d3eff0-d9d037af})
##### TLSPSKIdentity

The pre-shared key identity string, used for encrypted communications with Zabbix server.

[comment]: # ({/92d3eff0-d9d037af})

[comment]: # ({ae5c568f-4a08c667})
##### TLSServerCertIssuer

The allowed server (proxy) certificate issuer.

[comment]: # ({/ae5c568f-4a08c667})

[comment]: # ({fb69617f-6446ac76})
##### TLSServerCertSubject

The allowed server (proxy) certificate subject.

[comment]: # ({/fb69617f-6446ac76})

[comment]: # ({e61ac186-42a95de3})
##### UnsafeUserParameters

Allow all characters to be passed in arguments to user-defined parameters. The following characters are not allowed: \\ ' " \` \* ? \[ \] { } \~ $ ! & ; ( ) < > \| \# @<br>Additionally, newline characters are not allowed.

Default: `0`<br>
Values: 0 - do not allow, 1 - allow

[comment]: # ({/e61ac186-42a95de3})

[comment]: # ({de3660b7-cba7618d})
##### UserParameter

A user-defined parameter to monitor. There can be several user-defined parameters.<br>Format: UserParameter=<key>,<shell command><br>Note that the shell command must not return empty string or EOL only. Shell commands may have relative paths, if the UserParameterDir parameter is specified.

Example:

    UserParameter=system.test,who|wc -l
    UserParameter=check_cpu,./custom_script.sh

[comment]: # ({/de3660b7-cba7618d})

[comment]: # ({fefd119e-f6be7620})
##### UserParameterDir

The default search path for UserParameter commands. If used, the agent will change its working directory to the one specified here before executing a command. Thereby, UserParameter commands can have a relative `./` prefix instead of a full path.<br>Only one entry is allowed.

Example:

    UserParameterDir=/opt/myscripts

[comment]: # ({/fefd119e-f6be7620})
