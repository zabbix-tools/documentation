[comment]: # ({66a8ced4-66a8ced4})
# 9 Zabbix web service

[comment]: # ({/66a8ced4-66a8ced4})

[comment]: # ({fcc8b472-132b4156})
### Overview

The Zabbix web service is a process that is used for communication with external web services.

The parameters supported by the Zabbix web service configuration file (zabbix\_web\_service.conf) are listed in this section. 

The parameters are listed without additional information. Click on the parameter to see the full details.

|Parameter|Description|
|--|--------|
|[AllowedIP](#allowedip)|A list of comma delimited IP addresses, optionally in CIDR notation, or DNS names of Zabbix servers and Zabbix proxies.|
|[DebugLevel](#debuglevel)|The debug level.|
|[ListenPort](#listenport)|The agent will listen on this port for connections from the server.|
|[LogFile](#logfile)|The name of the log file.|
|[LogFileSize](#logfilesize)|The maximum size of the log file.|
|[LogType](#logtype)|The type of the log output.|
|[Timeout](#timeout)|Spend no more than Timeout seconds on processing.|
|[TLSAccept](#tlsaccept)|What incoming connections to accept.|
|[TLSCAFile](#tlscafile)|The full pathname of a file containing the top-level CA(s) certificates for peer certificate verification, used for encrypted communications between Zabbix components.|
|[TLSCertFile](#tlscertfile)|The full pathname of a file containing the service certificate or certificate chain, used for encrypted communications between Zabbix components.|
|[TLSKeyFile](#tlskeyfile)|The full pathname of a file containing the service private key, used for encrypted communications between Zabbix components.|

All parameters are non-mandatory unless explicitly stated that the parameter is mandatory.

Note that:

-   The default values reflect process defaults, not the values in the
    shipped configuration files;
-   Zabbix supports configuration files only in UTF-8 encoding without
    [BOM](https://en.wikipedia.org/wiki/Byte_order_mark);
-   Comments starting with "\#" are only supported at the beginning of
    the line.

[comment]: # ({/fcc8b472-132b4156})

[comment]: # ({8085875f-69d7c913})
### Parameter details

[comment]: # ({/8085875f-69d7c913})

[comment]: # ({3ffd0b98-0f829aa7})
##### AllowedIP
A list of comma delimited IP addresses, optionally in CIDR notation, or DNS names of Zabbix servers and Zabbix proxies. Incoming connections will be accepted only from the hosts listed here.<br>If IPv6 support is enabled then `127.0.0.1`, `::127.0.0.1`, `::ffff:127.0.0.1` are treated equally and `::/0` will allow any IPv4 or IPv6 address. `0.0.0.0/0` can be used to allow any IPv4 address.

Example: 

    127.0.0.1,192.168.1.0/24,::1,2001:db8::/32,zabbix.example.com

Mandatory: yes

[comment]: # ({/3ffd0b98-0f829aa7})

[comment]: # ({9c071f70-e5288ea7})
##### DebugLevel

Specify the debug level:<br>*0* - basic information about starting and stopping of Zabbix processes<br>*1* - critical information;<br>*2* - error information;<br>*3* - warnings;<br>*4* - for debugging (produces lots of information);<br>*5* - extended debugging (produces even more information).

Default: `3`<br>
Range: 0-5

[comment]: # ({/9c071f70-e5288ea7})

[comment]: # ({ca521675-e99b72c0})
##### ListenPort

The service will listen on this port for connections from the server.

Default: `10053`<br>
Range: 1024-32767

[comment]: # ({/ca521675-e99b72c0})

[comment]: # ({ee32cb60-bb5252d8})
##### LogFile

The name of the log file.

Example:

    /tmp/zabbix_web_service.log

Mandatory: Yes, if LogType is set to *file*; otherwise no

[comment]: # ({/ee32cb60-bb5252d8})

[comment]: # ({f7604f8f-778f1edc})
##### LogFileSize

The maximum size of a log file in MB.<br>0 - disable automatic log rotation.<br>*Note*: If the log file size limit is reached and file rotation fails, for whatever reason, the existing log file is truncated and started anew.

Default: `1`<br>
Range: 0-1024

[comment]: # ({/f7604f8f-778f1edc})

[comment]: # ({ae46be35-9d26f327})
##### LogType

The type of the log output:<br>*file* - write log to the file specified by LogFile parameter;<br>*system* - write log to syslog;<br>*console* - write log to standard output.

Default: `file`

[comment]: # ({/ae46be35-9d26f327})

[comment]: # ({1b1125a4-ee64bcdf})
##### Timeout

Spend no more than Timeout seconds on processing.

Default: `3`<br>
Range: 1-30

[comment]: # ({/1b1125a4-ee64bcdf})

[comment]: # ({a6dff55c-849f8496})
##### TLSAccept

What incoming connections to accept:<br>*unencrypted* - accept connections without encryption (default)<br>*cert* - accept connections with TLS and a certificate

Default: `unencrypted`

[comment]: # ({/a6dff55c-849f8496})

[comment]: # ({65d07bda-336a620d})
##### TLSCAFile

The full pathname of the file containing the top-level CA(s) certificates for peer certificate verification, used for encrypted communications between Zabbix components.

[comment]: # ({/65d07bda-336a620d})

[comment]: # ({a608b1f1-185ad3c6})
##### TLSCertFile

The full pathname of the file containing the service certificate or certificate chain, used for encrypted communications with Zabbix components.

[comment]: # ({/a608b1f1-185ad3c6})

[comment]: # ({ae0ead19-cbcbba0a})
##### TLSKeyFile

The full pathname of the file containing the service private key, used for encrypted communications between Zabbix components.

[comment]: # ({/ae0ead19-cbcbba0a})
