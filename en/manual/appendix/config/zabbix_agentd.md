[comment]: # attributes: notoc

[comment]: # ({c52b2df3-c52b2df3})
# 3 Zabbix agent (UNIX)

[comment]: # ({/c52b2df3-c52b2df3})

[comment]: # ({2c3847bd-2773fc24})
### Overview

The parameters supported by the Zabbix agent configuration file (zabbix\_agentd.conf) are listed in this section. 

The parameters are listed without additional information. Click on the parameter to see the full details.

|Parameter|Description|
|--|--------|
|[Alias](#alias)|Sets an alias for an item key.|
|[AllowKey](#allowkey)|Allow the execution of those item keys that match a pattern.|
|[AllowRoot](#allowroot)|Allow the agent to run as 'root'.|
|[BufferSend](#buffersend)|Do not keep data longer than N seconds in buffer.|
|[BufferSize](#buffersize)|The maximum number of values in the memory buffer.|
|[DebugLevel](#debuglevel)|The debug level.|
|[DenyKey](#denykey)|Deny the execution of those item keys that match a pattern.|
|[EnableRemoteCommands](#enableremotecommands)|Whether remote commands from Zabbix server are allowed.|
|[HeartbeatFrequency](#heartbeatfrequency)|The frequency of heartbeat messages in seconds.|
|[HostInterface](#hostinterface)|An optional parameter that defines the host interface.|
|[HostInterfaceItem](#hostinterfaceitem)|An optional parameter that defines an item used for getting the host interface.|
|[HostMetadata](#hostmetadata)|An optional parameter that defines the host metadata.|
|[HostMetadataItem](#hostmetadataitem)|An optional parameter that defines a Zabbix agent item used for getting the host metadata.|
|[Hostname](#hostname)|An optional parameter that defines the hostname.|
|[HostnameItem](#hostnameitem)|An optional parameter that defines a Zabbix agent item used for getting the hostname.|
|[Include](#include)|You may include individual files or all files in a directory in the configuration file.|
|[ListenBacklog](#listenbacklog)|The maximum number of pending connections in the TCP queue.|
|[ListenIP](#listenip)|A list of comma-delimited IP addresses that the agent should listen on.|
|[ListenPort](#listenport)|The agent will listen on this port for connections from the server.|
|[LoadModule](#loadmodule)|The module to load at agent startup.|
|[LoadModulePath](#loadmodulepath)|The full path to the location of agent modules.|
|[LogFile](#logfile)|The name of the log file.|
|[LogFileSize](#logfilesize)|The maximum size of the log file.|
|[LogRemoteCommands](#logremotecommands)|Enable logging of executed shell commands as warnings.|
|[LogType](#logtype)|The type of the log output.|
|[MaxLinesPerSecond](#maxlinespersecond)|The maximum number of new lines the agent will send per second to Zabbix server or proxy when processing 'log' and 'logrt' active checks.|
|[PidFile](#pidfile)|The name of the PID file.|
|[RefreshActiveChecks](#refreshactivechecks)|How often the list of active checks is refreshed.|
|[Server](#server)|A list of comma-delimited IP addresses, optionally in CIDR notation, or DNS names of Zabbix servers and Zabbix proxies.|
|[ServerActive](#serveractive)|The Zabbix server/proxy address or cluster configuration to get active checks from.|
|[SourceIP](#sourceip)|The source IP address.|
|[StartAgents](#startagents)|The number of pre-forked instances of zabbix\_agentd that process passive checks.|
|[Timeout](#timeout)|Spend no more than Timeout seconds on processing.|
|[TLSAccept](#tlsaccept)|What incoming connections to accept.|
|[TLSCAFile](#tlscafile)|The full pathname of a file containing the top-level CA(s) certificates for peer certificate verification, used for encrypted communications between Zabbix components.|
|[TLSCertFile](#tlscertfile)|The full pathname of a file containing the agent certificate or certificate chain, used for encrypted communications between Zabbix components.|
|[TLSCipherAll](#tlscipherall)|The GnuTLS priority string or OpenSSL (TLS 1.2) cipher string. Override the default ciphersuite selection criteria for certificate- and PSK-based encryption.|
|[TLSCipherAll13](#tlscipherall13)|The cipher string for OpenSSL 1.1.1 or newer in TLS 1.3. Override the default ciphersuite selection criteria for certificate- and PSK-based encryption.|
|[TLSCipherCert](#tlsciphercert)|The GnuTLS priority string or OpenSSL (TLS 1.2) cipher string. Override the default ciphersuite selection criteria for certificate-based encryption.|
|[TLSCipherCert13](#tlsciphercert13)|The cipher string for OpenSSL 1.1.1 or newer in TLS 1.3. Override the default ciphersuite selection criteria for certificate-based encryption.|
|[TLSCipherPSK](#tlscipherpsk)|The GnuTLS priority string or OpenSSL (TLS 1.2) cipher string. Override the default ciphersuite selection criteria for PSK-based encryption.|
|[TLSCipherPSK13](#tlscipherpsk13)|The cipher string for OpenSSL 1.1.1 or newer in TLS 1.3. Override the default ciphersuite selection criteria for PSK-based encryption.|
|[TLSConnect](#tlsconnect)|How the agent should connect to Zabbix server or proxy.|
|[TLSCRLFile](#tlscrlfile)|The full pathname of a file containing revoked certificates. This parameter is used for encrypted communications between Zabbix components.|
|[TLSKeyFile](#tlskeyfile)|The full pathname of a file containing the agent private key, used for encrypted communications between Zabbix components.|
|[TLSPSKFile](#tlspskfile)|The full pathname of a file containing the agent pre-shared key, used for encrypted communications with Zabbix server.|
|[TLSPSKIdentity](#tlspskidentity)|The pre-shared key identity string, used for encrypted communications with Zabbix server.|
|[TLSServerCertIssuer](#tlsservercertissuer)|The allowed server (proxy) certificate issuer.|
|[TLSServerCertSubject](#tlsservercertsubject)|The allowed server (proxy) certificate subject.|
|[UnsafeUserParameters](#unsafeuserparameters)|Allow all characters to be passed in arguments to user-defined parameters.|
|[User](#user)|Drop privileges to a specific, existing user on the system.|
|[UserParameter](#userparameter)|A user-defined parameter to monitor.|
|[UserParameterDir](#userparameterdir)|The default search path for UserParameter commands.|

All parameters are non-mandatory unless explicitly stated that the parameter is mandatory.

Note that:

-   The default values reflect daemon defaults, not the values in the
    shipped configuration files;
-   Zabbix supports configuration files only in UTF-8 encoding without
    [BOM](https://en.wikipedia.org/wiki/Byte_order_mark);
-   Comments starting with "\#" are only supported in the beginning of
    the line.

[comment]: # ({/2c3847bd-2773fc24})

[comment]: # ({8085875f-bb271b3c})
### Parameter details

[comment]: # ({/8085875f-bb271b3c})

[comment]: # ({588053a3-20d32ed1})
##### Alias

Sets an alias for an item key. It can be used to substitute a long and complex item key with a shorter and simpler one. Multiple *Alias* parameters with the same *Alias* key may be present.<br>Different *Alias* keys may reference the same item key.<br>Aliases can be used in *HostMetadataItem* but not in *HostnameItem* parameters.

Example 1: Retrieving the ID of user 'zabbix'.

    Alias=zabbix.userid:vfs.file.regexp[/etc/passwd,"^zabbix:.:([0-9]+)",,,,\1]
    
Now the **zabbix.userid** shorthand key may be used to retrieve data.

Example 2: Getting CPU utilization with default and custom parameters.

    Alias=cpu.util:system.cpu.util
    Alias=cpu.util[*]:system.cpu.util[*]

This allows use the **cpu.util** key to get CPU utilization percentage with default parameters as well as use **cpu.util[all, idle, avg15]** to get specific data about CPU utilization.

Example 3: Running multiple [low-level discovery](/manual/discovery/low_level_discovery) rules processing the same discovery items.

    Alias=vfs.fs.discovery[*]:vfs.fs.discovery

Now it is possible to set up several discovery rules using **vfs.fs.discovery** with different parameters for each rule, e.g., **vfs.fs.discovery[foo]**, **vfs.fs.discovery[bar]**, etc.

[comment]: # ({/588053a3-20d32ed1})

[comment]: # ({6c104b7b-f52682cc})
##### AllowKey

Allow the execution of those item keys that match a pattern. The key pattern is a wildcard expression that supports the "\*" character to match any number of any characters.<br>Multiple key matching rules may be defined in combination with DenyKey. The parameters are processed one by one according to their appearance order. See also: [Restricting agent checks](/manual/config/items/restrict_checks).

[comment]: # ({/6c104b7b-f52682cc})

[comment]: # ({bba716af-a02d0dc0})
##### AllowRoot

Allow the agent to run as 'root'. If disabled and the agent is started by 'root', the agent will try to switch to user 'zabbix' instead. Has no effect if started under a regular user.

Default: `0`<br>
Values: 0 - do not allow; 1 - allow

[comment]: # ({/bba716af-a02d0dc0})

[comment]: # ({fce8e2ee-12f60552})
##### BufferSend

Do not keep data longer than N seconds in buffer.

Default: `5`<br>
Range: 1-3600

[comment]: # ({/fce8e2ee-12f60552})

[comment]: # ({96409162-397065ee})
##### BufferSize

The maximum number of values in the memory buffer. The agent will send all collected data to the Zabbix server or proxy if the buffer is full.

Default: `100`<br>
Range: 2-65535

[comment]: # ({/96409162-397065ee})

[comment]: # ({9c071f70-e5288ea7})
##### DebugLevel

Specify the debug level:<br>*0* - basic information about starting and stopping of Zabbix processes<br>*1* - critical information;<br>*2* - error information;<br>*3* - warnings;<br>*4* - for debugging (produces lots of information);<br>*5* - extended debugging (produces even more information).

Default: `3`<br>
Range: 0-5

[comment]: # ({/9c071f70-e5288ea7})

[comment]: # ({13d6d3a4-ef1d8b77})
##### DenyKey

Deny the execution of those item keys that match a pattern. The key pattern is a wildcard expression that supports the "\*" character to match any number of any characters.<br>Multiple key matching rules may be defined in combination with AllowKey. The parameters are processed one by one according to their appearance order. See also: [Restricting agent checks](/manual/config/items/restrict_checks).

[comment]: # ({/13d6d3a4-ef1d8b77})

[comment]: # ({c4e8c8b1-50dcbfc8})
##### EnableRemoteCommands

Whether remote commands from Zabbix server are allowed. This parameter is **deprecated**, use AllowKey=system.run\[\*\] or DenyKey=system.run\[\*\] instead.<br>It is an internal alias for AllowKey/DenyKey parameters depending on value:<br>0 - DenyKey=system.run\[\*\]<br>1 - AllowKey=system.run\[\*\]

Default: `0`<br>
Values: 0 - do not allow, 1 - allow

[comment]: # ({/c4e8c8b1-50dcbfc8})

[comment]: # ({dbb05958-764c3d3c})
##### HeartbeatFrequency

The frequency of heartbeat messages in seconds. Used for monitoring the availability of active checks.<br>0 - heartbeat messages disabled.

Default: `60`<br>
Range: 0-3600

[comment]: # ({/dbb05958-764c3d3c})

[comment]: # ({48de1a7d-e514f4a5})
##### HostInterface

An optional parameter that defines the host interface. The host interface is used at host [autoregistration](/manual/discovery/auto_registration#using_dns_as_default_interface) process. If not defined, the value will be acquired from HostInterfaceItem.<br>The agent will issue an error and not start if the value is over the limit of 255 characters.

Range: 0-255 characters

[comment]: # ({/48de1a7d-e514f4a5})

[comment]: # ({249a7eff-4965ac2b})
##### HostInterfaceItem

An optional parameter that defines an item used for getting the host interface.<br>Host interface is used at host [autoregistration](/manual/discovery/auto_registration#using_dns_as_default_interface) process.<br>During an autoregistration request the agent will log a warning message if the value returned by the specified item is over the limit of 255 characters.<br>The system.run\[\] item is supported regardless of AllowKey/DenyKey values.<br>This option is only used when HostInterface is not defined.

[comment]: # ({/249a7eff-4965ac2b})

[comment]: # ({5bcd7757-42990e42})
##### HostMetadata

An optional parameter that defines host metadata. Host metadata is used only at host autoregistration process (active agent). If not defined, the value will be acquired from HostMetadataItem.<br>The agent will issue an error and not start if the specified value is over the limit of 2034 bytes or a non-UTF-8 string.

Range: 0-2034 bytes

[comment]: # ({/5bcd7757-42990e42})

[comment]: # ({bb287246-953ee255})
##### HostMetadataItem

An optional parameter that defines a Zabbix agent item used for getting host metadata. This option is only used when HostMetadata is not defined. User parameters and aliases are supported. The system.run\[\] item is supported regardless of AllowKey/DenyKey values.<br>The HostMetadataItem value is retrieved on each autoregistration attempt and is used only at host autoregistration process (active agent).<br>During an autoregistration request the agent will log a warning message if the value returned by the specified item is over the limit of 65535 UTF-8 code points. The value returned by the item must be a UTF-8 string otherwise it will be ignored.

[comment]: # ({/bb287246-953ee255})

[comment]: # ({6e272ef0-79d09548})
##### Hostname

A list of comma-delimited, unique, case-sensitive hostnames. Required for active checks and must match hostnames as configured on the server. The value is acquired from HostnameItem if undefined.<br>Allowed characters: alphanumeric, '.', ' ', '\_' and '-'. Maximum length: 128 characters per hostname, 2048 characters for the entire line.

Default: Set by HostnameItem

[comment]: # ({/6e272ef0-79d09548})

[comment]: # ({93b5994f-14aaef49})
##### HostnameItem

An optional parameter that defines a Zabbix agent item used for getting the host name. This option is only used when Hostname is not defined. User parameters or aliases are not supported, but the system.run\[\] item is supported regardless of AllowKey/DenyKey values.<br>The output length is limited to 512KB.

Default: `system.hostname`

[comment]: # ({/93b5994f-14aaef49})

[comment]: # ({33e54751-1e31f1b3})
##### Include

You may include individual files or all files in a directory in the configuration file. To only include relevant files in the specified directory, the asterisk wildcard character is supported for pattern matching.<br>See [special notes](special_notes_include) about limitations.

Example:

    Include=/absolute/path/to/config/files/*.conf

[comment]: # ({/33e54751-1e31f1b3})

[comment]: # ({fcd70e26-d49e31a2})
##### ListenBacklog

The maximum number of pending connections in the TCP queue.<br>The default value is a hard-coded constant, which depends on the system.<br>The maximum supported value depends on the system, too high values may be silently truncated to the 'implementation-specified maximum'.

Default: `SOMAXCONN`<br>
Range: 0 - INT\_MAX

[comment]: # ({/fcd70e26-d49e31a2})

[comment]: # ({35f278c7-62349c68})
##### ListenIP

A list of comma-delimited IP addresses that the agent should listen on.

Default: `0.0.0.0`

[comment]: # ({/35f278c7-62349c68})

[comment]: # ({5f95bc53-e99b72c0})
##### ListenPort

The agent will listen on this port for connections from the server.

Default: `10050`<br>
Range: 1024-32767

[comment]: # ({/5f95bc53-e99b72c0})

[comment]: # ({4395a864-fb75d239})
##### LoadModule

The module to load at agent startup. Modules are used to extend the functionality of the agent. The module must be located in the directory specified by LoadModulePath or the path must precede the module name. If the preceding path is absolute (starts with '/') then LoadModulePath is ignored.<br>Formats:<br>LoadModule=<module.so><br>LoadModule=<path/module.so><br>LoadModule=</abs\_path/module.so><br>It is allowed to include multiple LoadModule parameters.

[comment]: # ({/4395a864-fb75d239})

[comment]: # ({e48fbe48-bd604c99})
##### LoadModulePath

The full path to the location of agent modules. The default depends on compilation options.

[comment]: # ({/e48fbe48-bd604c99})

[comment]: # ({ce9b40ca-bb5252d8})
##### LogFile

The name of the log file.

Mandatory: Yes, if LogType is set to *file*; otherwise no

[comment]: # ({/ce9b40ca-bb5252d8})

[comment]: # ({f7604f8f-778f1edc})
##### LogFileSize

The maximum size of a log file in MB.<br>0 - disable automatic log rotation.<br>*Note*: If the log file size limit is reached and file rotation fails, for whatever reason, the existing log file is truncated and started anew.

Default: `1`<br>
Range: 0-1024

[comment]: # ({/f7604f8f-778f1edc})

[comment]: # ({736b056e-3d97cc10})
##### LogRemoteCommands

Enable logging of the executed shell commands as warnings. Commands will be logged only if executed remotely. Log entries will not be created if system.run\[\] is launched locally by HostMetadataItem, HostInterfaceItem or HostnameItem parameters.

Default: `0`<br>
Values: 0 - disabled, 1 - enabled

[comment]: # ({/736b056e-3d97cc10})

[comment]: # ({ae46be35-9d26f327})
##### LogType

The type of the log output:<br>*file* - write log to the file specified by LogFile parameter;<br>*system* - write log to syslog;<br>*console* - write log to standard output.

Default: `file`

[comment]: # ({/ae46be35-9d26f327})

[comment]: # ({0d598af8-7d39adb8})
##### MaxLinesPerSecond

The maximum number of new lines the agent will send per second to Zabbix server or proxy when processing 'log' and 'logrt' active checks. The provided value will be overridden by the 'maxlines' parameter, provided in the 'log' or 'logrt' item key.<br>*Note*: Zabbix will process 10 times more new lines than set in *MaxLinesPerSecond* to seek the required string in log items.

Default: `20`<br>
Range: 1-1000

[comment]: # ({/0d598af8-7d39adb8})

[comment]: # ({3d558d6c-8a5c9259})
##### PidFile

The name of the PID file.

Default: `/tmp/zabbix_agentd.pid`

[comment]: # ({/3d558d6c-8a5c9259})

[comment]: # ({d3de4e11-b62afa60})
##### RefreshActiveChecks

How often the list of active checks is refreshed, in seconds. Note that after failing to refresh active checks the next refresh will be attempted in 60 seconds.

Default: `5`<br>
Range: 1-86400

[comment]: # ({/d3de4e11-b62afa60})

[comment]: # ({5fb05a68-46329efe})
##### Server

A list of comma-delimited IP addresses, optionally in CIDR notation, or DNS names of Zabbix servers and Zabbix proxies. Incoming connections will be accepted only from the hosts listed here. If IPv6 support is enabled then '127.0.0.1', '::127.0.0.1', '::ffff:127.0.0.1' are treated equally and '::/0' will allow any IPv4 or IPv6 address. '0.0.0.0/0' can be used to allow any IPv4 address. Note, that "IPv4-compatible IPv6 addresses" (0000::/96 prefix) are supported but deprecated by [RFC4291](https://tools.ietf.org/html/rfc4291#section-2.5.5). Spaces are allowed.

Example: 

    Server=127.0.0.1,192.168.1.0/24,::1,2001:db8::/32,zabbix.example.com

Mandatory: yes, if StartAgents is not explicitly set to 0

[comment]: # ({/5fb05a68-46329efe})

[comment]: # ({b4c0a838-d6a09b20})
##### ServerActive

The Zabbix server/proxy address or cluster configuration to get active checks from. The server/proxy address is an IP address or DNS name and optional port separated by colon.<br>Cluster configuration is one or more server addresses separated by semicolon. Multiple Zabbix servers/clusters and Zabbix proxies can be specified, separated by comma. More than one Zabbix proxy should not be specified from each Zabbix server/cluster. If Zabbix proxy is specified then Zabbix server/cluster for that proxy should not be specified.<br>Multiple comma-delimited addresses can be provided to use several independent Zabbix servers in parallel. Spaces are allowed.<br>If the port is not specified, default port is used.<br>IPv6 addresses must be enclosed in square brackets if port for that host is specified. If port is not specified, square brackets for IPv6 addresses are optional.<br>If this parameter is not specified, active checks are disabled.

Example for Zabbix proxy: 

    ServerActive=127.0.0.1:10051

Example for multiple servers: 

    ServerActive=127.0.0.1:20051,zabbix.domain,[::1]:30051,::1,[12fc::1]

Example for high availability:

    ServerActive=zabbix.cluster.node1;zabbix.cluster.node2:20051;zabbix.cluster.node3

Example for high availability with two clusters and one server:

    ServerActive=zabbix.cluster.node1;zabbix.cluster.node2:20051,zabbix.cluster2.node1;zabbix.cluster2.node2,zabbix.domain

[comment]: # ({/b4c0a838-d6a09b20})

[comment]: # ({3628af0a-020cc066})
##### SourceIP

The source IP address for:<br>- outgoing connections to Zabbix server or Zabbix proxy;<br>- making connections while executing some items (web.page.get, net.tcp.port, etc.).

[comment]: # ({/3628af0a-020cc066})

[comment]: # ({252719a4-6e2d3567})
##### StartAgents

The number of pre-forked instances of zabbix\_agentd that process passive checks. If set to 0, passive checks are disabled and the agent will not listen on any TCP port.

Default: `3`<br>
Range: 0-100

[comment]: # ({/252719a4-6e2d3567})

[comment]: # ({1b1125a4-ee64bcdf})
##### Timeout

Spend no more than Timeout seconds on processing.

Default: `3`<br>
Range: 1-30

[comment]: # ({/1b1125a4-ee64bcdf})

[comment]: # ({ee15aa7e-849f8496})
##### TLSAccept

What incoming connections to accept. Used for a passive checks. Multiple values can be specified, separated by comma:<br>*unencrypted* - accept connections without encryption (default)<br>*psk* - accept connections with TLS and a pre-shared key (PSK)<br>*cert* - accept connections with TLS and a certificate

Mandatory: yes, if TLS certificate or PSK parameters are defined (even for *unencrypted* connection); otherwise no

[comment]: # ({/ee15aa7e-849f8496})

[comment]: # ({65d07bda-336a620d})
##### TLSCAFile

The full pathname of the file containing the top-level CA(s) certificates for peer certificate verification, used for encrypted communications between Zabbix components.

[comment]: # ({/65d07bda-336a620d})

[comment]: # ({005f60f1-185ad3c6})
##### TLSCertFile

The full pathname of the file containing the agent certificate or certificate chain, used for encrypted communications with Zabbix components.

[comment]: # ({/005f60f1-185ad3c6})

[comment]: # ({953e7fd5-3e9bf2b8})
##### TLSCipherAll

The GnuTLS priority string or OpenSSL (TLS 1.2) cipher string. Override the default ciphersuite selection criteria for certificate- and PSK-based encryption.

Example:

    TLS_AES_256_GCM_SHA384:TLS_CHACHA20_POLY1305_SHA256:TLS_AES_128_GCM_SHA256

[comment]: # ({/953e7fd5-3e9bf2b8})

[comment]: # ({832946e7-03e7f520})
##### TLSCipherAll13

The cipher string for OpenSSL 1.1.1 or newer in TLS 1.3. Override the default ciphersuite selection criteria for certificate- and PSK-based encryption.

Example for GnuTLS: 

    NONE:+VERS-TLS1.2:+ECDHE-RSA:+RSA:+ECDHE-PSK:+PSK:+AES-128-GCM:+AES-128-CBC:+AEAD:+SHA256:+SHA1:+CURVE-ALL:+COMP-NULL::+SIGN-ALL:+CTYPE-X.509

Example for OpenSSL: 

    EECDH+aRSA+AES128:RSA+aRSA+AES128:kECDHEPSK+AES128:kPSK+AES128

[comment]: # ({/832946e7-03e7f520})

[comment]: # ({c9cf41e3-f7468437})
##### TLSCipherCert

The GnuTLS priority string or OpenSSL (TLS 1.2) cipher string. Override the default ciphersuite selection criteria for certificate-based encryption.

Example for GnuTLS: 

    NONE:+VERS-TLS1.2:+ECDHE-RSA:+RSA:+AES-128-GCM:+AES-128-CBC:+AEAD:+SHA256:+SHA1:+CURVE-ALL:+COMP-NULL:+SIGN-ALL:+CTYPE-X.509

Example for OpenSSL: 

    EECDH+aRSA+AES128:RSA+aRSA+AES128

[comment]: # ({/c9cf41e3-f7468437})

[comment]: # ({5956ba54-717d9f51})
##### TLSCipherCert13

The cipher string for OpenSSL 1.1.1 or newer in TLS 1.3. Override the default ciphersuite selection criteria for certificate-based encryption.

[comment]: # ({/5956ba54-717d9f51})

[comment]: # ({038b665a-3a633a0a})
##### TLSCipherPSK

The GnuTLS priority string or OpenSSL (TLS 1.2) cipher string. Override the default ciphersuite selection criteria for PSK-based encryption.

Example for GnuTLS: 

    NONE:+VERS-TLS1.2:+ECDHE-PSK:+PSK:+AES-128-GCM:+AES-128-CBC:+AEAD:+SHA256:+SHA1:+CURVE-ALL:+COMP-NULL:+SIGN-ALL

Example for OpenSSL: 

    kECDHEPSK+AES128:kPSK+AES128

[comment]: # ({/038b665a-3a633a0a})

[comment]: # ({b96d7f16-8208c770})
##### TLSCipherPSK13

The cipher string for OpenSSL 1.1.1 or newer in TLS 1.3. Override the default ciphersuite selection criteria for PSK-based encryption.

Example:

    TLS_CHACHA20_POLY1305_SHA256:TLS_AES_128_GCM_SHA256

[comment]: # ({/b96d7f16-8208c770})

[comment]: # ({be99c4c7-b1151069})
##### TLSConnect

How the agent should connect to Zabbix server or proxy. Used for active checks. Only one value can be specified:<br>*unencrypted* - connect without encryption (default)<br>*psk* - connect using TLS and a pre-shared key (PSK)<br>*cert* - connect using TLS and a certificate

Mandatory: yes, if TLS certificate or PSK parameters are defined (even for *unencrypted* connection); otherwise no

[comment]: # ({/be99c4c7-b1151069})

[comment]: # ({94a4b758-28aef301})
##### TLSCRLFile

The full pathname of the file containing revoked certificates. This parameter is used for encrypted communications between Zabbix components.

[comment]: # ({/94a4b758-28aef301})

[comment]: # ({c2aa4315-cbcbba0a})
##### TLSKeyFile

The full pathname of the file containing the agent private key, used for encrypted communications between Zabbix components.

[comment]: # ({/c2aa4315-cbcbba0a})

[comment]: # ({11defa44-845f5b70})
##### TLSPSKFile

The full pathname of the file containing the agent pre-shared key, used for encrypted communications with Zabbix server.

[comment]: # ({/11defa44-845f5b70})

[comment]: # ({92d3eff0-b02e4942})
##### TLSPSKIdentity

The pre-shared key identity string, used for encrypted communications with Zabbix server.

[comment]: # ({/92d3eff0-b02e4942})

[comment]: # ({ae5c568f-7a78525c})
##### TLSServerCertIssuer

The allowed server (proxy) certificate issuer.

[comment]: # ({/ae5c568f-7a78525c})

[comment]: # ({fb69617f-c8d1ac8b})
##### TLSServerCertSubject

The allowed server (proxy) certificate subject.

[comment]: # ({/fb69617f-c8d1ac8b})

[comment]: # ({e61ac186-676eb24d})
##### UnsafeUserParameters

Allow all characters to be passed in arguments to user-defined parameters. The following characters are not allowed: \\ ' " \` \* ? \[ \] { } \~ $ ! & ; ( ) < > \| \# @<br>Additionally, newline characters are not allowed.

Default: `0`<br>
Values: 0 - do not allow, 1 - allow

[comment]: # ({/e61ac186-676eb24d})

[comment]: # ({ee4ad0a4-e9779b31})
##### User

Drop privileges to a specific, existing user on the system.<br>Only has effect if run as 'root' and AllowRoot is disabled.

Default: `zabbix`

[comment]: # ({/ee4ad0a4-e9779b31})

[comment]: # ({de3660b7-1591e129})
##### UserParameter

A user-defined parameter to monitor. There can be several user-defined parameters.<br>Format: UserParameter=<key>,<shell command><br>Note that the shell command must not return empty string or EOL only. Shell commands may have relative paths, if the UserParameterDir parameter is specified.

Example:

    UserParameter=system.test,who|wc -l
    UserParameter=check_cpu,./custom_script.sh

[comment]: # ({/de3660b7-1591e129})

[comment]: # ({fefd119e-493da8fd})
##### UserParameterDir

The default search path for UserParameter commands. If used, the agent will change its working directory to the one specified here before executing a command. Thereby, UserParameter commands can have a relative `./` prefix instead of a full path.<br>Only one entry is allowed.

Example:

    UserParameterDir=/opt/myscripts

[comment]: # ({/fefd119e-493da8fd})

[comment]: # ({9170c364-9170c364})
#### See also

1.  [Differences in the Zabbix agent configuration for active and
    passive checks starting from version
    2.0.0](http://blog.zabbix.com/multiple-servers-for-active-agent-sure/858)

[comment]: # ({/9170c364-9170c364})
