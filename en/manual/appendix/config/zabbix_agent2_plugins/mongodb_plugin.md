[comment]: # ({f68118e5-f68118e5})
# 5 MongoDB plugin

[comment]: # ({/f68118e5-f68118e5})

[comment]: # ({41b68e8b-a831e9b6})
#### Overview

This section lists parameters supported in the MongoDB Zabbix agent 2 plugin configuration file (mongo.conf).

This is a loadable plugin, which is available and fully described in the [MongoDB plugin repository](https://git.zabbix.com/projects/AP/repos/mongodb/browse).

Note that:

-   The default values reflect process defaults, not the values in the
    shipped configuration files;
-   Zabbix supports configuration files only in UTF-8 encoding without
    [BOM](https://en.wikipedia.org/wiki/Byte_order_mark);
-   Comments starting with "\#" are only supported at the beginning of
    the line.

[comment]: # ({/41b68e8b-a831e9b6})

[comment]: # ({500a1356-eaba6cbf})

#### Options

|Parameter|Description|
|---------|-----------|
|-V --version|Print the plugin version and license information.|
|-h --help|Print help information (shorthand).|

[comment]: # ({/500a1356-eaba6cbf})

[comment]: # ({f6ea63df-83498ede})

#### Parameters

|Parameter|Mandatory|Range|Default|Description|
|--|--|--|--|----------|
|Plugins.MongoDB.Default.Password|no| | |Default password for connecting to MongoDB; used if no value is specified in an item key or named session.|
|Plugins.MongoDB.Default.Uri|no| | |Default URI for connecting to MongoDB; used if no value is specified in an item key or named session. <br><br>Should not include embedded credentials (they will be ignored).<br>Must match the URI format.<br>Only `tcp` scheme is supported; a scheme can be omitted.<br>A port can be omitted (default=27017).<br>Examples: `tcp://127.0.0.1:27017`, `tcp:localhost`, `localhost`|
|Plugins.MongoDB.Default.User|no| | |Default username for connecting to MongoDB; used if no value is specified in an item key or named session.|
|Plugins.MongoDB.KeepAlive|no|60-900|300|The maximum time of waiting (in seconds) before unused plugin connections are closed.|
|Plugins.MongoDB.Sessions.<SessionName>.Password|no| | |Named session password.<br>**<SessionName>** - define name of a session for using in item keys.|
|Plugins.MongoDB.Sessions.<SessionName>.TLSCAFile|no <br>(yes, if Plugins.MongoDB.Sessions.<SessionName>.TLSConnect is set to one of: verify_ca, verify_full)| | |Full pathname of a file containing the top-level CA(s) certificates for peer certificate verification, used for encrypted communications between Zabbix agent 2 and monitored databases.<br>**<SessionName>** - define name of a session for using in item keys. |
|Plugins.MongoDB.Sessions.<SessionName>.TLSCertFile|no <br>(yes, if Plugins.MongoDB.Sessions.<SessionName>.TLSConnect is set to one of: verify_ca, verify_full)| | |Full pathname of a file containing the agent certificate or certificate chain, used for encrypted communications between Zabbix agent 2 and monitored databases.<br>**<SessionName>** - define name of a session for using in item keys. |
|Plugins.MongoDB.Sessions.<SessionName>.TLSConnect|no | | |Encryption type for communications between Zabbix agent 2 and monitored databases.<br>**<SessionName>** - define name of a session for using in item keys.<br><br>Supported values:<br>*required* - require TLS connection;<br>*verify\_ca* - verify certificates;<br>*verify\_full* - verify certificates and IP address. <br><br> Supported since plugin version 1.2.1|
|Plugins.MongoDB.Sessions.<SessionName>.TLSKeyFile|no <br>(yes, if Plugins.MongoDB.Sessions.<SessionName>.TLSConnect is set to one of: verify_ca, verify_full)| | |Full pathname of a file containing the database private key used for encrypted communications between Zabbix agent 2 and monitored databases.<br>**<SessionName>** - define name of a session for using in item keys. |
|Plugins.MongoDB.Sessions.<SessionName>.Uri|no| | |Connection string of a named session.<br>**<SessionName>** - define name of a session for using in item keys.<br><br>Should not include embedded credentials (they will be ignored).<br>Must match the URI format.<br>Only `tcp` scheme is supported; a scheme can be omitted.<br>A port can be omitted (default=27017).<br>Examples: `tcp://127.0.0.1:27017`, `tcp:localhost`, `localhost`|
|Plugins.MongoDB.Sessions.<SessionName>.User|no| | |Named session username.<br>**<SessionName>** - define name of a session for using in item keys.|
|Plugins.MongoDB.System.Path|no| | |Path to plugin executable.|
|Plugins.MongoDB.Timeout|no|1-30|global timeout|Request execution timeout (how long to wait for a request to complete before shutting it down).|

See also:

-   Description of general Zabbix agent 2 configuration parameters:
    [Zabbix agent 2 (UNIX)](/manual/appendix/config/zabbix_agent2) /
    [Zabbix agent 2
    (Windows)](/manual/appendix/config/zabbix_agent2_win)
-   Instructions for configuring [plugins](/manual/extensions/plugins)

[comment]: # ({/f6ea63df-83498ede})
