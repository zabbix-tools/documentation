[comment]: # ({e0afbd96-e0afbd96})
# 6 MQTT plugin

[comment]: # ({/e0afbd96-e0afbd96})

[comment]: # ({88abc321-88abc321})
#### Overview

This section lists parameters supported in the MQTT Zabbix agent 2
plugin configuration file (mqtt.conf). 

Note that:

-   The default values reflect process defaults, not the values in the
    shipped configuration files;
-   Zabbix supports configuration files only in UTF-8 encoding without
    [BOM](https://en.wikipedia.org/wiki/Byte_order_mark);
-   Comments starting with "\#" are only supported at the beginning of
    the line.

[comment]: # ({/88abc321-88abc321})

[comment]: # ({ee6f6681-97d87248})
#### Parameters

|Parameter|Mandatory|Range|Default|Description|
|--|--|--|--|----------|
|Plugins.MQTT.Default.Password|no| | |Default password for connecting to MQTT; used if no value is specified in an item key or named session.<br> Supported since version 6.4.4|
|Plugins.MQTT.Default.TLSCAFile|no| | |Full pathname of a file containing the top-level CA(s) certificates for peer certificate verification for encrypted communications between Zabbix agent 2 and MQTT broker; used if no value is specified in a named session.<br> Supported since version 6.4.4|
|Plugins.MQTT.Default.TLSCertFile|no| | |Full pathname of a file containing the agent certificate or certificate chain for encrypted communications between Zabbix agent 2 and MQTT broker; used if no value is specified in a named session.<br> Supported since version 6.4.4|
|Plugins.MQTT.Default.TLSKeyFile|no| | |Full pathname of a file containing the MQTT private key for encrypted communications between Zabbix agent 2 and MQTT broker; used if no value is specified in a named session.<br> Supported since version 6.4.4|
|Plugins.MQTT.Default.Topic|no| | |Default topic for MQTT subscription; used if no value is specified in an item key or named session.<br><br>The topic may contain wildcards ("+","#")<br>Examples: `path/to/file`<br>`path/to/#`<br>`path/+/topic`<br>Supported since version 6.4.4|
|Plugins.MQTT.Default.Url|no| |tcp://localhost:1883|Default MQTT broker connection string; used if no value is specified in an item key or named session.<br><br>Should not include query parameters.<br>Must match the URL format.<br>Supported schemes: `tcp` (default), `ws`, `tls`; a scheme can be omitted.<br>A port can be omitted (default=1883).<br>Examples: `tcp://host:1883`<br>`localhost`<br>`ws://host:8080`<br>Supported since version 6.4.4|
|Plugins.MQTT.Default.User|no| | |Default username for connecting to MQTT; used if no value is specified in an item key or named session.<br>Supported since version 6.4.4|
|Plugins.MQTT.Sessions.<SessionName>.Password|no| | |Named session password.<br>**<SessionName>** - define name of a session for using in item keys.<br>Supported since version 6.4.4|
|Plugins.MQTT.Sessions.<SessionName>.TLSCAFile|no| | |Full pathname of a file containing the top-level CA(s) certificates for peer certificate verification, used for encrypted communications between Zabbix agent 2 and MQTT broker.<br>**<SessionName>** - define name of a session for using in item keys.<br>Supported since version 6.4.4|
|Plugins.MQTT.Sessions.<SessionName>.TLSCertFile|no| | |Full pathname of a file containing the agent certificate or certificate chain, used for encrypted communications between Zabbix agent 2 and MQTT broker.<br>**<SessionName>** - define name of a session for using in item keys.<br>Supported since version 6.4.4|
|Plugins.MQTT.Sessions.<SessionName>.TLSKeyFile|no| | |Full pathname of a file containing the MQTT private key used for encrypted communications between Zabbix agent 2 and MQTT broker.<br>**<SessionName>** - define name of a session for using in item keys.<br>Supported since version 6.4.4|
|Plugins.MQTT.Sessions.<SessionName>.Topic|no| | |Named session topic for MQTT subscription.<br>**<SessionName>** - define name of a session for using in item keys.<br><br>The topic may contain wildcards ("+","#")<br>Examples: `path/to/file`<br>`path/to/#`<br>`path/+/topic`<br>Supported since version 6.4.4|
|Plugins.MQTT.Sessions.<SessionName>.Url|no| | |Connection string of a named session.<br>**<SessionName>** - define name of a session for using in item keys.<br><br>Should not include query parameters.<br>Must match the URL format.<br>Supported schemes: `tcp` (default), `ws`, `tls`; a scheme can be omitted.<br>A port can be omitted (default=1883).<br>Examples: `tcp://host:1883`<br>`localhost`<br>`ws://host:8080`<br>Supported since version 6.4.4|
|Plugins.MQTT.Sessions.<SessionName>.User|no| | |Named session username.<br>**<SessionName>** - define name of a session for using in item keys.<br>Supported since version 6.4.4|
|Plugins.MQTT.Timeout|no|1-30|global timeout|Request execution timeout (how long to wait for a request to complete before shutting it down).|

See also:

-   Description of general Zabbix agent 2 configuration parameters:
    [Zabbix agent 2 (UNIX)](/manual/appendix/config/zabbix_agent2) /
    [Zabbix agent 2
    (Windows)](/manual/appendix/config/zabbix_agent2_win)
-   Instructions for configuring [plugins](/manual/extensions/plugins)

[comment]: # ({/ee6f6681-97d87248})
