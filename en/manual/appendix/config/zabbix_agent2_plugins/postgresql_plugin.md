[comment]: # ({726ee638-heading})
# 9 PostgreSQL plugin

[comment]: # ({/726ee638-heading})

[comment]: # ({33ad9e7e-overview})
### Overview

This section lists parameters supported in the PostgreSQL Zabbix agent 2 plugin configuration file (postgresql.conf).
 
This is a loadable plugin, which is available and fully described in the
[PostgreSQL plugin repository](https://git.zabbix.com/projects/AP/repos/postgresql/browse)

Note that:

-   The default values reflect process defaults, not the values in the
    shipped configuration files;
-   Zabbix supports configuration files only in UTF-8 encoding without
    [BOM](https://en.wikipedia.org/wiki/Byte_order_mark);
-   Comments starting with "\#" are only supported at the beginning of
    the line.

[comment]: # ({/33ad9e7e-overview})

[comment]: # ({1a59fae9-options})
### Options

|Parameter|Description|
|---------|-----------|
|-V --version|Print the plugin version and license information.|
|-h --help|Print help information (shorthand).|

[comment]: # ({/1a59fae9-options})

[comment]: # ({21d85663-parameters})
### Parameters

|Parameter|Mandatory|Range|Default|Description|
|--|--|--|--|----------|
|Plugins.PostgreSQL.CallTimeout|no|1-30|global timeout|Maximum wait time (in seconds) for a request to be completed.|
|Plugins.PostgreSQL.CustomQueriesPath|no| |disabled|Full pathname of the directory containing *.sql* files with custom queries.|
|Plugins.PostgreSQL.Default.Database|no| | |Default database for connecting to PostgreSQL; used if no value is specified in an item key or named session.|
|Plugins.PostgreSQL.Default.Password|no| | |Default password for connecting to PostgreSQL; used if no value is specified in an item key or named session.|
|Plugins.PostgreSQL.Default.TLSCAFile|no<br>(yes, if Plugins.PostgreSQL.Default.TLSConnect is set to one of: verify_ca, verify_full)| | |Full pathname of a file containing the top-level CA(s) certificate for peer certificate verification for encrypted communications between Zabbix agent 2 and monitored databases; used if no value is specified in a named session.|
|Plugins.PostgreSQL.Default.TLSCertFile|no<br>(yes, if Plugins.PostgreSQL.Default.TLSConnect is set to one of: verify_ca, verify_full)| | |Full pathname of a file containing the PostgreSQL certificate or certificate chain for encrypted communications between Zabbix agent 2 and monitored databases; used if no value is specified in a named session.|
|Plugins.PostgreSQL.Default.TLSConnect|no| | |Encryption type for communications between Zabbix agent 2 and monitored databases; used if no value is specified in a named session.<br>Supported values:<br>*required* - connect using TLS as transport mode without identity checks;<br>*verify\_ca* - connect using TLS and verify certificate;<br>*verify\_full* - connect using TLS, verify certificate and verify that database identity (CN) specified by DBHost matches its certificate.<br>Undefined encryption type means unencrypted connection.|
|Plugins.PostgreSQL.Default.TLSKeyFile|no<br>(yes, if Plugins.PostgreSQL.Default.TLSConnect is set to one of: verify_ca, verify_full)| | |Full pathname of a file containing the PostgreSQL private key for encrypted communications between Zabbix agent 2 and monitored databases; used if no value is specified in a named session.|
|Plugins.PostgreSQL.Default.Uri|no| | |Default URI for connecting to PostgreSQL; used if no value is specified in an item key or named session.<br><br>Should not include embedded credentials (they will be ignored).<br>Must match the URI format.<br>Supported schemes: `tcp`, `unix`.<br>Examples: `tcp://127.0.0.1:5432`<br>`tcp://localhost`<br>`unix:/var/run/postgresql/.s.PGSQL.5432`|
|Plugins.PostgreSQL.Default.User|no| | |Default username for connecting to PostgreSQL; used if no value is specified in an item key or named session.|
|Plugins.PostgreSQL.KeepAlive|no|60-900|300|Maximum time of waiting (in seconds) before unused plugin connections are closed.|
|Plugins.PostgreSQL.Sessions.<SessionName>.Database|no| | |Database for session connection.<br>**<SessionName>** - define name of a session for using in item keys.|
|Plugins.PostgreSQL.Sessions.<SessionName>.Password|no|Must match the password format.| |Password for session connection.<br>**<SessionName>** - define name of a session for using in item keys.|
|Plugins.PostgreSQL.Sessions.<SessionName>.TLSCAFile|no<br>(yes, if Plugins.PostgreSQL.Sessions.<SessionName>.TLSConnect is set to one of: verify_ca, verify_full)| | |Full pathname of a file containing the top-level CA(s) certificate peer certificate verification.<br>**<SessionName>** - define name of a session for using in item keys.|
|Plugins.PostgreSQL.Sessions.<SessionName>.TLSCertFile|no<br>(yes, if Plugins.PostgreSQL.Sessions.<SessionName>.TLSConnect is set to one of: verify_ca, verify_full)| | |Full pathname of a file containing the PostgreSQL certificate or certificate chain.<br>**<SessionName>** - define name of a session for using in item keys.|
|Plugins.PostgreSQL.Sessions.<SessionName>.TLSConnect|no| | |Encryption type for PostgreSQL connection.<br>**<SessionName>** - define name of a session for using in item keys.<br><br>Supported values:<br>*required* - connect using TLS as transport mode without identity checks;<br>*verify\_ca* - connect using TLS and verify certificate;<br>*verify\_full* - connect using TLS, verify certificate and verify that database identity (CN) specified by DBHost matches its certificate.<br>Undefined encryption type means unencrypted connection.|
|Plugins.PostgreSQL.Sessions.<SessionName>.TLSKeyFile|no<br>(yes, if Plugins.PostgreSQL.Sessions.<SessionName>.TLSConnect is set to one of: verify_ca, verify_full)| | |Full pathname of a file containing the PostgreSQL private key.<br>**<SessionName>** - define name of a session for using in item keys.|
|Plugins.PostgreSQL.Sessions.<SessionName>.Uri|no| | |Connection string of a named session.<br>**<SessionName>** - define name of a session for using in item keys.<br><br>Should not include embedded credentials (they will be ignored).<br>Must match the URI format.<br>Supported schemes: `tcp`, `unix`.<br>Examples: `tcp://127.0.0.1:5432`<br>`tcp://localhost`<br>`unix:/var/run/postgresql/.s.PGSQL.5432`|
|Plugins.PostgreSQL.Sessions.<SessionName>.User|no| | |Named session username.<br>**<SessionName>** - define name of a session for using in item keys.|
|Plugins.PostgreSQL.System.Path|yes| | |Path to external plugin executable.|
|Plugins.PostgreSQL.Timeout|no|1-30|global timeout|Request execution timeout (how long to wait for a request to complete before shutting it down).|

See also:

-   Description of general Zabbix agent 2 configuration parameters:
    [Zabbix agent 2 (UNIX)](/manual/appendix/config/zabbix_agent2) /
    [Zabbix agent 2
    (Windows)](/manual/appendix/config/zabbix_agent2_win)
-   Instructions for configuring [plugins](/manual/extensions/plugins)

[comment]: # ({/21d85663-parameters})
