[comment]: # ({fd969ff7-fd969ff7})
# 10 Redis plugin

[comment]: # ({/fd969ff7-fd969ff7})

[comment]: # ({13a11ede-2519af2c})
#### Overview

This section lists parameters supported in the Redis Zabbix agent 2
plugin configuration file (redis.conf).

Note that:

-   The default values reflect process defaults, not the values in the
    shipped configuration files;
-   Zabbix supports configuration files only in UTF-8 encoding without
    [BOM](https://en.wikipedia.org/wiki/Byte_order_mark);
-   Comments starting with "\#" are only supported at the beginning of
    the line.

[comment]: # ({/13a11ede-2519af2c})

[comment]: # ({5220eda6-2a4e55b0})
#### Parameters

|Parameter|Mandatory|Range|Default|Description|
|--|--|--|--|----------|
|Plugins.Redis.Default.Password|no| | |Default password for connecting to Redis; used if no value is specified in an item key or named session.|
|Plugins.Redis.Default.Uri|no| |tcp://localhost:6379|Default URI for connecting to Redis; used if no value is specified in an item key or named session. <br><br>Should not include embedded credentials (they will be ignored).<br>Must match the URI format.<br>Supported schemes: `tcp`, `unix`; a scheme can be omitted (since version 5.2.3).<br>A port can be omitted (default=6379).<br>Examples: `tcp://localhost:6379`<br>`localhost`<br>`unix:/var/run/redis.sock`|
|Plugins.Redis.KeepAlive|no|60-900|300|The maximum time of waiting (in seconds) before unused plugin connections are closed.|
|Plugins.Redis.Sessions.<SessionName>.Password|no| | |Named session password.<br>**<SessionName>** - define name of a session for using in item keys.|
|Plugins.Redis.Sessions.<SessionName>.Uri|no| | |Connection string of a named session.<br>**<SessionName>** - define name of a session for using in item keys.<br><br>Should not include embedded credentials (they will be ignored).<br>Must match the URI format.<br>Supported schemes: `tcp`, `unix`; a scheme can be omitted.<br>A port can be omitted (default=6379).<br>Examples: `tcp://localhost:6379`<br>`localhost`<br>`unix:/var/run/redis.sock`|
|Plugins.Redis.Timeout|no|1-30|global timeout|Request execution timeout (how long to wait for a request to complete before shutting it down).|

See also:

-   Description of general Zabbix agent 2 configuration parameters:
    [Zabbix agent 2 (UNIX)](/manual/appendix/config/zabbix_agent2) /
    [Zabbix agent 2
    (Windows)](/manual/appendix/config/zabbix_agent2_win)
-   Instructions for configuring [plugins](/manual/extensions/plugins)

[comment]: # ({/5220eda6-2a4e55b0})
