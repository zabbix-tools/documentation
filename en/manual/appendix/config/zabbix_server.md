[comment]: # attributes: notoc

[comment]: # ({dbb2a1f5-dbb2a1f5})
# 1 Zabbix server

[comment]: # ({/dbb2a1f5-dbb2a1f5})

[comment]: # ({24ddb79e-16f1be5f})
### Overview

The parameters supported by the Zabbix server configuration file (zabbix\_server.conf) are listed in this section. 

The parameters are listed without additional information. Click on the parameter to see the full details.

|Parameter|Description|
|--|--------|
|[AlertScriptsPath](#alertscriptspath)|The location of custom alert scripts.|
|[AllowRoot](#allowroot)|Allow the server to run as 'root'.|
|[AllowUnsupportedDBVersions](#allowunsupporteddbversions)|Allow the server to work with unsupported database versions.|
|[CacheSize](#cachesize)|The size of the configuration cache.|
|[CacheUpdateFrequency](#cacheupdatefrequency)|This parameter determines how often Zabbix will perform the configuration cache update in seconds.|
|[DBHost](#dbhost)|The database host name.|
|[DBName](#dbname)|The database name.|
|[DBPassword](#dbpassword)|The database password.|
|[DBPort](#dbport)|The database port when not using local socket.|
|[DBSchema](#dbschema)|The database schema name. Used for PostgreSQL.|
|[DBSocket](#dbsocket)|The path to the MySQL socket file.|
|[DBUser](#dbuser)|The database user.|
|[DBTLSConnect](#dbtlsconnect)|Setting this option to the specified value enforces to use a TLS connection to the database.|
|[DBTLSCAFile](#dbtlscafile)|The full pathname of a file containing the top-level CA(s) certificates for database certificate verification.|
|[DBTLSCertFile](#dbtlscertfile)|The full pathname of a file containing the Zabbix server certificate for authenticating to database.|
|[DBTLSKeyFile](#dbtlskeyfile)|The full pathname of a file containing the private key for authenticating to database.|
|[DBTLSCipher](#dbtlscipher)|The list of encryption ciphers that Zabbix server permits for TLS protocols up through TLS v1.2. Supported only for MySQL.|
|[DBTLSCipher13](#dbtlscipher13)|The list of encryption ciphersuites that Zabbix server permits for the TLS v1.3 protocol. Supported only for MySQL, starting from version 8.0.16.|
|[DebugLevel](#debuglevel)|Specify the debug level.|
|[ExportDir](#exportdir)|The directory for real-time export of events, history and trends in newline-delimited JSON format. If set, enables the real-time export.|
|[ExportFileSize](#exportfilesize)|The maximum size per export file in bytes.|
|[ExportType](#exporttype)|The list of comma-delimited entity types (events, history, trends) for real-time export (all types by default).|
|[ExternalScripts](#externalscripts)|The location of external scripts.|
|[Fping6Location](#fping6location)|The location of fping6.|
|[FpingLocation](#fpinglocation)|The location of fping.|
|[HANodeName](#hanodename)|The high availability cluster node name.|
|[HistoryCacheSize](#historycachesize)|The size of the history cache.|
|[HistoryIndexCacheSize](#historyindexcachesize)|The size of the history index cache.|
|[HistoryStorageDateIndex](#historystoragedateindex)|Enable preprocessing of history values in history storage to store values in different indices based on date.|
|[HistoryStorageURL](#historystorageurl)|The history storage HTTP\[S\] URL.|
|[HistoryStorageTypes](#historystoragetypes)|A comma-separated list of value types to be sent to the history storage.|
|[HousekeepingFrequency](#housekeepingfrequency)|This parameter determines how often Zabbix will perform the housekeeping procedure in hours.|
|[Include](#include)|You may include individual files or all files in a directory in the configuration file.|
|[JavaGateway](#javagateway)|The IP address (or hostname) of Zabbix Java gateway.|
|[JavaGatewayPort](#javagatewayport)|The port that Zabbix Java gateway listens on.|
|[ListenBacklog](#listenbacklog)|The maximum number of pending connections in the TCP queue.|
|[ListenIP](#listenip)|A list of comma-delimited IP addresses that the trapper should listen on.|
|[ListenPort](#listenport)|The listen port for trapper.|
|[LoadModule](#loadmodule)|The module to load at server startup.|
|[LoadModulePath](#loadmodulepath)|The full path to the location of server modules.|
|[LogFile](#logfile)|The name of the log file.|
|[LogFileSize](#logfilesize)|The maximum size of the log file.|
|[LogSlowQueries](#logslowqueries)|Determines how long a database query may take before being logged in milliseconds.|
|[LogType](#logtype)|The type of the log output.|
|[MaxHousekeeperDelete](#maxhousekeeperdelete)|No more than 'MaxHousekeeperDelete' rows (corresponding to [tablename], [field], [value]) will be deleted per one task in one housekeeping cycle.|
|[NodeAddress](#nodeaddress)|The IP or hostname with optional port to override how the frontend should connect to the server.|
|[PidFile](#pidfile)|The name of the PID file.|
|[ProblemHousekeepingFrequency](#problemhousekeepingfrequency)|Determines how often Zabbix will delete problems for deleted triggers.|
|[ProxyConfigFrequency](#proxyconfigfrequency)|Determines how often Zabbix server sends configuration data to a Zabbix proxy.|
|[ProxyDataFrequency](#proxydatafrequency)|Determines how often Zabbix server requests history data from a Zabbix proxy.|
|[ServiceManagerSyncFrequency](#servicemanagersyncfrequency)|Determines how often Zabbix will synchronize the configuration of a service manager.|
|[SNMPTrapperFile](#snmptrapperfile)|The temporary file used for passing data from the SNMP trap daemon to the server.|
|[SocketDir](#socketdir)|The directory to store the IPC sockets used by internal Zabbix services.|
|[SourceIP](#sourceip)|The source IP address.|
|[SSHKeyLocation](#sshkeylocation)|The location of public and private keys for SSH checks and actions.|
|[SSLCertLocation](#sslcertlocation)|The location of SSL client certificate files for client authentication.|
|[SSLKeyLocation](#sslkeylocation)|The location of SSL private key files for client authentication.|
|[SSLCALocation](#sslcalocation)|Override the location of certificate authority (CA) files for SSL server certificate verification.|
|[StartAlerters](#startalerters)|The number of pre-forked instances of alerters.|
|[StartConnectors](#startconnectors)|The number of pre-forked instances of connector workers.|
|[StartDBSyncers](#startdbsyncers)|The number of pre-forked instances of history syncers.|
|[StartDiscoverers](#startdiscoverers)|The number of pre-forked instances of discovery workers.|
|[StartEscalators](#startescalators)|The number of pre-forked instances of escalators.|
|[StartHistoryPollers](#starthistorypollers)|The number of pre-forked instances of history pollers.|
|[StartHTTPPollers](#starthttppollers)|The number of pre-forked instances of HTTP pollers.|
|[StartIPMIPollers](#startipmipollers)|The number of pre-forked instances of IPMI pollers.|
|[StartJavaPollers](#startjavapollers)|The number of pre-forked instances of Java pollers.|
|[StartLLDProcessors](#startlldprocessors)|The number of pre-forked instances of low-level discovery (LLD) workers.|
|[StartODBCPollers](#startodbcpollers)|The number of pre-forked instances of ODBC pollers.|
|[StartPingers](#startpingers)|The number of pre-forked instances of ICMP pingers.|
|[StartPollersUnreachable](#startpollersunreachable)|The number of pre-forked instances of pollers for unreachable hosts (including IPMI and Java).|
|[StartPollers](#startpollers)|The number of pre-forked instances of pollers.|
|[StartPreprocessors](#startpreprocessors)|The number of pre-started instances of preprocessing workers.|
|[StartProxyPollers](#startproxypollers)|The number of pre-forked instances of pollers for passive proxies.|
|[StartReportWriters](#startreportwriters)|The number of pre-forked instances of report writers.|
|[StartSNMPTrapper](#startsnmptrapper)|If set to 1, an SNMP trapper process will be started.|
|[StartTimers](#starttimers)|The number of pre-forked instances of timers.|
|[StartTrappers](#starttrappers)|The number of pre-forked instances of trappers.|
|[StartVMwareCollectors](#startvmwarecollectors)|The number of pre-forked VMware collector instances.|
|[StatsAllowedIP](#statsallowedip)|A list of comma-delimited IP addresses, optionally in CIDR notation, or DNS names of external Zabbix instances. The stats request will be accepted only from the addresses listed here.|
|[Timeout](#timeout)|Specifies how long we wait for agent, SNMP device or external check in seconds.|
|[TLSCAFile](#tlscafile)|The full pathname of a file containing the top-level CA(s) certificates for peer certificate verification, used for encrypted communications between Zabbix components.|
|[TLSCertFile](#tlscertfile)|The full pathname of a file containing the server certificate or certificate chain, used for encrypted communications between Zabbix components.|
|[TLSCipherAll](#tlscipherall)|The GnuTLS priority string or OpenSSL (TLS 1.2) cipher string. Override the default ciphersuite selection criteria for certificate- and PSK-based encryption.|
|[TLSCipherAll13](#tlscipherall13)|The cipher string for OpenSSL 1.1.1 or newer in TLS 1.3. Override the default ciphersuite selection criteria for certificate- and PSK-based encryption.|
|[TLSCipherCert](#tlsciphercert)|The GnuTLS priority string or OpenSSL (TLS 1.2) cipher string. Override the default ciphersuite selection criteria for certificate-based encryption.|
|[TLSCipherCert13](#tlsciphercert13)|The cipher string for OpenSSL 1.1.1 or newer in TLS 1.3. Override the default ciphersuite selection criteria for certificate-based encryption.|
|[TLSCipherPSK](#tlscipherpsk)|The GnuTLS priority string or OpenSSL (TLS 1.2) cipher string. Override the default ciphersuite selection criteria for PSK-based encryption.|
|[TLSCipherPSK13](#tlscipherpsk13)|The cipher string for OpenSSL 1.1.1 or newer in TLS 1.3. Override the default ciphersuite selection criteria for PSK-based encryption.|
|[TLSCRLFile](#tlscrlfile)|The full pathname of a file containing revoked certificates. This parameter is used for encrypted communications between Zabbix components.|
|[TLSKeyFile](#tlskeyfile)|The full pathname of a file containing the server private key, used for encrypted communications between Zabbix components.|
|[TmpDir](#tmpdir)|The temporary directory.|
|[TrapperTimeout](#trappertimeout)|Specifies how many seconds the trapper may spend processing new data.|
|[TrendCacheSize](#trendcachesize)|The size of the trend cache.|
|[TrendFunctionCacheSize](#trendfunctioncachesize)|The size of the trend function cache.|
|[UnavailableDelay](#unavailabledelay)|Determines how often host is checked for availability during the unavailability period.|
|[UnreachableDelay](#unreachabledelay)|Determines how often host is checked for availability during the unreachability period.|
|[UnreachablePeriod](#unreachableperiod)|Determines after how many seconds of unreachability treats a host as unavailable.|
|[User](#user)|Drop privileges to a specific, existing user on the system.|
|[ValueCacheSize](#valuecachesize)|The size of the history value cache.|
|[Vault](#vault)|Specifies the vault provider.|
|[VaultDBPath](#vaultdbpath)|Specifies a location, from where database credentials should be retrieved by keys.|
|[VaultTLSCertFile](#vaulttlscertfile)|The name of the SSL certificate file used for client authentication.|
|[VaultTLSKeyFile](#vaulttlskeyfile)|The name of the SSL private key file used for client authentication.|
|[VaultToken](#vaulttoken)|The HashiCorp vault authentication token.|
|[VaultURL](#vaulturl)|The vault server HTTP\[S\] URL.|
|[VMwareCacheSize](#vmwarecachesize)|The shared memory size for storing VMware data.|
|[VMwareFrequency](#vmwarefrequency)|The delay in seconds between data gathering from a single VMware service.|
|[VMwareTimeout](#vmwareperffrequency)|The maximum number of seconds a vmware collector will wait for a response from VMware service.|
|[WebServiceURL](#webserviceurl)|HTTP\[S\] URL to Zabbix web service in the format \<host:port\>/report.|

All parameters are non-mandatory unless explicitly stated that the parameter is mandatory.

Note that:

-   The default values reflect daemon defaults, not the values in the
    shipped configuration files;
-   Zabbix supports configuration files only in UTF-8 encoding without
    [BOM](https://en.wikipedia.org/wiki/Byte_order_mark);
-   Comments starting with "\#" are only supported in the beginning of
    the line.
  

[comment]: # ({/24ddb79e-16f1be5f})

[comment]: # ({8085875f-6428a44e})
### Parameter details

[comment]: # ({/8085875f-6428a44e})

[comment]: # ({f797abf5-e273f2ed})
##### AlertScriptsPath
The location of custom alert scripts (depends on the *datadir* compile-time installation variable).

Default: `/usr/local/share/zabbix/alertscripts`

[comment]: # ({/f797abf5-e273f2ed})

[comment]: # ({bd677866-1edb700a})
##### AllowRoot
Allow the server to run as 'root'. If disabled and the server is started by 'root', the server will try to switch to the 'zabbix' user instead. Has no effect if started under a regular user.

Default: `0`<br>
Values: 0 - do not allow; 1 - allow

[comment]: # ({/bd677866-1edb700a})

[comment]: # ({8d9264f2-37f8828a})
##### AllowUnsupportedDBVersions
Allow the server to work with unsupported database versions.

Default: `0`<br>
Values: 0 - do not allow; 1 - allow

[comment]: # ({/8d9264f2-37f8828a})

[comment]: # ({d42a1d5c-6c704994})
##### CacheSize
The size of the configuration cache, in bytes. The shared memory size for storing host, item and trigger data.

Default: `32M`<br>
Range: 128K-64G

[comment]: # ({/d42a1d5c-6c704994})

[comment]: # ({474ff68b-2c956137})
##### CacheUpdateFrequency
This parameter determines how often Zabbix will perform the configuration cache update in seconds. See also [runtime control](/manual/concepts/server#runtime-control) options.

Default: `10`<br>
Range: 1-3600

[comment]: # ({/474ff68b-2c956137})

[comment]: # ({71f5559f-d92a7b2b})
##### DBHost
The database host name.<br>With MySQL `localhost` or empty string results in using a socket. With PostgreSQL only empty string results in attempt to use socket. With [Oracle](/manual/appendix/install/oracle#connection_set_up) empty string results in using the Net Service Name connection method; in this case consider using the TNS\_ADMIN environment variable to specify the directory of the tnsnames.ora file.

Default: `localhost`

[comment]: # ({/71f5559f-d92a7b2b})

[comment]: # ({26659e19-1058c650})
##### DBName
The database name.<br>With [Oracle](/manual/appendix/install/oracle#connection_set_up), if the Net Service Name connection method is used, specify the service name from tnsnames.ora or set to empty string; set the TWO\_TASK environment variable if DBName is set to empty string.

Mandatory: Yes

[comment]: # ({/26659e19-1058c650})

[comment]: # ({3eae1d31-479ee610})
##### DBPassword
The database password. Comment this line if no password is used.

[comment]: # ({/3eae1d31-479ee610})

[comment]: # ({22cf271a-1b941880})
##### DBPort
The database port when not using local socket.<br>With [Oracle](/manual/appendix/install/oracle#connection_set_up), if the Net Service Name connection method is used, this parameter will be ignored; the port number from the tnsnames.ora file will be used instead.

Range: 1024-65535

[comment]: # ({/22cf271a-1b941880})

[comment]: # ({e46eeb51-abcff68d})
##### DBSchema
The database schema name. Used for PostgreSQL.

[comment]: # ({/e46eeb51-abcff68d})

[comment]: # ({68b4077a-3a14461c})
##### DBSocket
The path to the MySQL socket file.

[comment]: # ({/68b4077a-3a14461c})

[comment]: # ({d143a7f3-94782f63})
##### DBUser
The database user.

[comment]: # ({/d143a7f3-94782f63})

[comment]: # ({a08d4bce-500737d5})
##### DBTLSConnect
Setting this option to the following values enforces to use a TLS connection to the database:<br>*required* - connect using TLS<br>*verify\_ca* - connect using TLS and verify certificate<br>*verify\_full* - connect using TLS, verify certificate and verify that database identity specified by DBHost matches its certificate<br><br>With MySQL, starting from 5.7.11, and PostgreSQL the following values are supported: `required`, `verify_ca`, `verify_full`.<br>With MariaDB, starting from version 10.2.6, the `required` and `verify_full` values are supported.<br>By default not set to any option and the behavior depends on database configuration.

[comment]: # ({/a08d4bce-500737d5})

[comment]: # ({cbc09c0c-fc356178})
##### DBTLSCAFile
The full pathname of a file containing the top-level CA(s) certificates for database certificate verification.

Mandatory: no (yes, if DBTLSConnect set to *verify\_ca* or *verify\_full*)

[comment]: # ({/cbc09c0c-fc356178})

[comment]: # ({888a6d90-5f286da4})
##### DBTLSCertFile
The full pathname of a file containing the Zabbix server certificate for authenticating to database.

[comment]: # ({/888a6d90-5f286da4})

[comment]: # ({7e76481a-dd8b56f5})
##### DBTLSKeyFile
The full pathname of a file containing the private key for authenticating to database.

[comment]: # ({/7e76481a-dd8b56f5})

[comment]: # ({452df107-d8056d7a})
##### DBTLSCipher
The list of encryption ciphers that Zabbix server permits for TLS protocols up through TLS v1.2. Supported only for MySQL.

[comment]: # ({/452df107-d8056d7a})

[comment]: # ({b3ffe8b2-a327c2f2})
##### DBTLSCipher13
The list of encryption ciphersuites that Zabbix server permits for the TLS v1.3 protocol. Supported only for MySQL, starting from version 8.0.16.

[comment]: # ({/b3ffe8b2-a327c2f2})

[comment]: # ({b87015d1-d82f69fb})
##### DebugLevel
Specify the debug level:<br>*0* - basic information about starting and stopping of Zabbix processes<br>*1* - critical information;<br>*2* - error information;<br>*3* - warnings;<br>*4* - for debugging (produces lots of information);<br>*5* - extended debugging (produces even more information).<br>See also [runtime control](/manual/concepts/server#server_process) options.

Default: `3`<br>
Range: 0-5

[comment]: # ({/b87015d1-d82f69fb})

[comment]: # ({687798b3-99815de4})
##### ExportDir
The directory for [real-time export](/manual/appendix/install/real_time_export) of events, history and trends in newline-delimited JSON format. If set, enables the real-time export.

[comment]: # ({/687798b3-99815de4})

[comment]: # ({41b0eece-965e757e})
##### ExportFileSize
The maximum size per export file in bytes. Used for rotation if `ExportDir` is set.

Default: `1G`<br>
Range: 1M-1G

[comment]: # ({/41b0eece-965e757e})

[comment]: # ({25db198c-5447304d})
##### ExportType
The list of comma-delimited entity types (events, history, trends) for [real-time export](/manual/appendix/install/real_time_export) (all types by default). Valid only if ExportDir is set.<br>*Note* that if ExportType is specified, but ExportDir is not, then this is a configuration error and the server will not start.

Example for history and trends export:

    ExportType=history,trends

Example for event export only:

    ExportType=events

[comment]: # ({/25db198c-5447304d})

[comment]: # ({79e29f6f-d0a46edf})
##### ExternalScripts
The location of external scripts (depends on the `datadir` compile-time installation variable).

Default: `/usr/local/share/zabbix/externalscripts`

[comment]: # ({/79e29f6f-d0a46edf})

[comment]: # ({f5450a5e-bdd6796f})
##### Fping6Location
The location of fping6. Make sure that the fping6 binary has root ownership and the SUID flag set. Make empty ("Fping6Location=") if your fping utility is capable to process IPv6 addresses.

Default: `/usr/sbin/fping6`

[comment]: # ({/f5450a5e-bdd6796f})

[comment]: # ({dc27c714-025648b5})
##### FpingLocation
The location of fping. Make sure that the fping binary has root ownership and the SUID flag set.

Default: `/usr/sbin/fping`

[comment]: # ({/dc27c714-025648b5})

[comment]: # ({94661b6f-8bb05e99})
##### HANodeName
The high availability cluster node name. When empty the server is working in standalone mode and a node with empty name is created.

[comment]: # ({/94661b6f-8bb05e99})

[comment]: # ({c72e665f-c802a26c})
##### HistoryCacheSize
The size of the history cache, in bytes. The shared memory size for storing history data.

Default: `16M`<br>
Range: 128K-2G

[comment]: # ({/c72e665f-c802a26c})

[comment]: # ({18425602-2753befa})
##### HistoryIndexCacheSize
The size of the history index cache, in bytes. The shared memory size for indexing the history data stored in history cache. The index cache size needs roughly 100 bytes to cache one item.

Default: `4M`<br>
Range: 128K-2G

[comment]: # ({/18425602-2753befa})

[comment]: # ({9ba97300-b5dc4ecc})
##### HistoryStorageDateIndex
Enable preprocessing of history values in history storage to store values in different indices based on date.

Default: `0`<br>
Values: 0 - disable; 1 - enable

[comment]: # ({/9ba97300-b5dc4ecc})

[comment]: # ({db98938d-08949761})
##### HistoryStorageURL
The history storage HTTP\[S\] URL. This parameter is used for [Elasticsearch](/manual/appendix/install/elastic_search_setup) setup.

[comment]: # ({/db98938d-08949761})

[comment]: # ({ad448d41-131c3d46})
##### HistoryStorageTypes
A comma-separated list of value types to be sent to the history storage. This parameter is used for [Elasticsearch](/manual/appendix/install/elastic_search_setup) setup.

Default: `uint,dbl,str,log,text`

[comment]: # ({/ad448d41-131c3d46})

[comment]: # ({e40022ae-42928b03})
##### HousekeepingFrequency
This parameter determines how often Zabbix will perform the housekeeping procedure in hours. Housekeeping is removing outdated information from the database.<br>*Note*: To prevent housekeeper from being overloaded (for example, when history and trend periods are greatly reduced), no more than 4 times HousekeepingFrequency hours of outdated information are deleted in one housekeeping cyc,le, for each item. Thus, if HousekeepingFrequency is 1, no more than 4 hours of outdated information (starting from the oldest entry) will be deleted per cycle.<br>*Note*: To lower load on server startup housekeeping is postponed for 30 minutes after server start. Thus, if HousekeepingFrequency is 1, the very first housekeeping procedure after server start will run after 30 minutes, and will repeat with one hour delay thereafter.<br>It is possible to disable automatic housekeeping by setting HousekeepingFrequency to 0. In this case the housekeeping procedure can only be started by *housekeeper\_execute* runtime control option and the period of outdated information deleted in one housekeeping cycle is 4 times the period since the last housekeeping cycle, but not less than 4 hours and not greater than 4 days.<br>See also [runtime control](/manual/concepts/server#server_process) options.

Default: `1`<br>
Range: 0-24

[comment]: # ({/e40022ae-42928b03})

[comment]: # ({7bfe9fd5-5836e8ba})
##### Include
You may include individual files or all files in a directory in the configuration file. To only include relevant files in the specified directory, the asterisk wildcard character is supported for pattern matching. See [special notes](special_notes_include) about limitations.

Example:

    Include=/absolute/path/to/config/files/*.conf

[comment]: # ({/7bfe9fd5-5836e8ba})

[comment]: # ({764f58a4-97c00865})
##### JavaGateway
The IP address (or hostname) of Zabbix Java gateway. Only required if Java pollers are started.

[comment]: # ({/764f58a4-97c00865})

[comment]: # ({9e49dbe0-fc4d246a})
##### JavaGatewayPort
The port that Zabbix Java gateway listens on.

Default: `10052`<br>
Range: 1024-32767

[comment]: # ({/9e49dbe0-fc4d246a})

[comment]: # ({fcd70e26-22241946})
##### ListenBacklog
The maximum number of pending connections in the TCP queue.<br>The default value is a hard-coded constant, which depends on the system.<br>The maximum supported value depends on the system, too high values may be silently truncated to the 'implementation-specified maximum'.

Default: `SOMAXCONN`<br>
Range: 0 - INT\_MAX

[comment]: # ({/fcd70e26-22241946})

[comment]: # ({1baa7180-33f1aea9})
##### ListenIP
A list of comma-delimited IP addresses that the trapper should listen on.<br>Trapper will listen on all network interfaces if this parameter is missing.

Default: `0.0.0.0`

[comment]: # ({/1baa7180-33f1aea9})

[comment]: # ({e2d7152a-98e0be1b})
##### ListenPort
The listen port for trapper.

Default: `10051`<br>
Range: 1024-32767

[comment]: # ({/e2d7152a-98e0be1b})

[comment]: # ({da969410-55309e9c})
##### LoadModule
The module to load at server startup. Modules are used to extend the functionality of the server.  The module must be located in the directory specified by LoadModulePath or the path must precede the module name. If the preceding path is absolute (starts with '/') then LoadModulePath is ignored.<br>Formats:<br>LoadModule=<module.so><br>LoadModule=<path/module.so><br>LoadModule=</abs\_path/module.so><br>It is allowed to include multiple LoadModule parameters.

[comment]: # ({/da969410-55309e9c})

[comment]: # ({34b0b90c-0c0c0723})
##### LoadModulePath
The full path to the location of server modules. The default depends on compilation options.

[comment]: # ({/34b0b90c-0c0c0723})

[comment]: # ({ce9b40ca-c650f3c9})
##### LogFile
The name of the log file.

Mandatory: Yes, if LogType is set to *file*; otherwise no

[comment]: # ({/ce9b40ca-c650f3c9})

[comment]: # ({21f561eb-9c05648a})
##### LogFileSize
The maximum size of the log file in MB.<br>0 - disable automatic log rotation.<br>*Note*: If the log file size limit is reached and file rotation fails, for whatever reason, the existing log file is truncated and started anew.

Default: `1`<br>
Range: 0-1024<br>
Mandatory: Yes, if LogType is set to *file*; otherwise no

[comment]: # ({/21f561eb-9c05648a})

[comment]: # ({a42094ee-2f20d22f})
##### LogSlowQueries
Determines how long a database query may take before being logged in milliseconds.<br>0 - don't log slow queries.<br>This option becomes enabled starting with DebugLevel=3.

Default: `0`<br>
Range: 0-3600000

[comment]: # ({/a42094ee-2f20d22f})

[comment]: # ({5d1bf62c-926c45e2})
##### LogType
The type of the log output:<br>*file* - write log to file specified by LogFile parameter;<br>*system* - write log to syslog;<br>*console* - write log to standard output.

Default: `file`

[comment]: # ({/5d1bf62c-926c45e2})

[comment]: # ({fa2c8121-124ae689})
##### MaxHousekeeperDelete
No more than 'MaxHousekeeperDelete' rows (corresponding to \[tablename\], \[field\], \[value\]) will be deleted per one task in one housekeeping cycle.<br>If set to 0 then no limit is used at all. In this case you must know what you are doing, so as not to [overload the database!](zabbix_server#footnotes) **^[2](zabbix_server#footnotes)^**<br>This parameter applies only to deleting history and trends of already deleted items.

Default: `5000`<br>
Range: 0-1000000

[comment]: # ({/fa2c8121-124ae689})

[comment]: # ({fa7bc5a4-9cfb79d4})
##### NodeAddress
IP or hostname with optional port to override how the frontend should connect to the server.<br>Format: \<address>\[:\<port>\]<br><br>If IP or hostname is not set, the value of ListenIP will be used. If ListenIP is not set, the value `localhost` will be used.<br>If port is not set, the value of ListenPort will be used. If ListenPort is not set, the value `10051` will be used.<br><br>This option can be overridden by the address specified in the frontend configuration.<br><br>See also: [HANodeName](#hanodename) parameter; [Enabling high availability](/manual/concepts/server/ha#enabling-high-availability).

Default: 'localhost:10051'

[comment]: # ({/fa7bc5a4-9cfb79d4})

[comment]: # ({70607c45-8153735c})
##### PidFile
Name of the PID file.

Default: `/tmp/zabbix_server.pid`

[comment]: # ({/70607c45-8153735c})

[comment]: # ({c64dc750-b0d337ac})
##### ProblemHousekeepingFrequency
Determines how often Zabbix will delete problems for deleted triggers in seconds.

Default: `60`<br>
Range: 1-3600

[comment]: # ({/c64dc750-b0d337ac})

[comment]: # ({d51cde90-b3642a43})
##### ProxyConfigFrequency
Determines how often Zabbix server sends configuration data to a Zabbix proxy in seconds. Used only for proxies in a passive mode.

Default: `10`<br>
Range: 1-604800

[comment]: # ({/d51cde90-b3642a43})

[comment]: # ({73d90779-3bb5827f})
##### ProxyDataFrequency
Determines how often Zabbix server requests history data from a Zabbix proxy in seconds. Used only for proxies in the passive mode.

Default: `1`<br>
Range: 1-3600

[comment]: # ({/73d90779-3bb5827f})

[comment]: # ({dc7c58ce-7df5a0e9})
##### ServiceManagerSyncFrequency
Determines how often Zabbix will synchronize the configuration of a service manager in seconds.

Default: `60`<br>
Range: 1-3600

[comment]: # ({/dc7c58ce-7df5a0e9})

[comment]: # ({45fb51c5-9c385986})
##### SNMPTrapperFile
Temporary file used for passing data from the SNMP trap daemon to the server.<br>Must be the same as in zabbix\_trap\_receiver.pl or SNMPTT configuration file.

Default: `/tmp/zabbix_traps.tmp`

[comment]: # ({/45fb51c5-9c385986})

[comment]: # ({d3999f18-cd750f09})
##### SocketDir
Directory to store IPC sockets used by internal Zabbix services.

Default: `/tmp`

[comment]: # ({/d3999f18-cd750f09})

[comment]: # ({41b12cc3-bf3120f2})
##### SourceIP
Source IP address for:<br>- outgoing connections to Zabbix proxy and Zabbix agent;<br>- agentless connections (VMware, SSH, JMX, SNMP, Telnet and simple checks);<br>- HTTP agent connections;<br>- script item JavaScript HTTP requests;<br>- preprocessing JavaScript HTTP requests;<br>- sending notification emails (connections to SMTP server);<br>- webhook notifications (JavaScript HTTP connections);<br>- connections to the Vault

[comment]: # ({/41b12cc3-bf3120f2})

[comment]: # ({4beb1f20-8edbdae2})
##### SSHKeyLocation
Location of public and private keys for SSH checks and actions.

[comment]: # ({/4beb1f20-8edbdae2})

[comment]: # ({b465011c-f14a0fc6})
##### SSLCertLocation
Location of SSL client certificate files for client authentication.<br>This parameter is used in web monitoring only.

[comment]: # ({/b465011c-f14a0fc6})

[comment]: # ({156e51d2-d38d629d})
##### SSLKeyLocation
Location of SSL private key files for client authentication.<br>This parameter is used in web monitoring only.

[comment]: # ({/156e51d2-d38d629d})

[comment]: # ({28525b99-d4ecaa9a})
##### SSLCALocation
Override the location of certificate authority (CA) files for SSL server certificate verification. If not set, system-wide directory will be used.<br>Note that the value of this parameter will be set as libcurl option CURLOPT\_CAPATH. For libcurl versions before 7.42.0, this only has effect if libcurl was compiled to use OpenSSL. For more information see [cURL web page](http://curl.haxx.se/libcurl/c/CURLOPT_CAPATH.html).<br>This parameter is used in web monitoring and in SMTP authentication.

[comment]: # ({/28525b99-d4ecaa9a})

[comment]: # ({ec808c04-49587d38})
##### StartAlerters
The number of pre-forked instances of [alerters](/manual/concepts/server#server_process_types).

Default: `3`<br>
Range: 1-100

[comment]: # ({/ec808c04-49587d38})

[comment]: # ({3b9faf57-4404a76d})
##### StartConnectors
The number of pre-forked instances of [connector workers](/manual/concepts/server#server_process_types). The connector manager process is started automatically when a connector worker is started.

Range: 0-1000

[comment]: # ({/3b9faf57-4404a76d})

[comment]: # ({d0238e10-25c85aff})
##### StartDBSyncers
The number of pre-forked instances of [history syncers](/manual/concepts/server#server_process_types).<br>*Note*: Be careful when changing this value, increasing it may do more harm than good. Roughly, the default value should be enough to handle up to 4000 NVPS.

Default: `4`<br>
Range: 1-100

[comment]: # ({/d0238e10-25c85aff})

[comment]: # ({9f766cfd-40a86c5a})
##### StartDiscoverers
The number of pre-forked instances of [discovery workers](/manual/concepts/server#server_process_types).

Default: `5`<br>
Range: 0-1000

[comment]: # ({/9f766cfd-40a86c5a})

[comment]: # ({a2d0b609-5a8be3fd})
##### StartEscalators
The number of pre-forked instances of [escalators](/manual/concepts/server#server_process_types).

Default: `1`<br>
Range: 1-100

[comment]: # ({/a2d0b609-5a8be3fd})

[comment]: # ({899c458d-97ad4fb5})
##### StartHistoryPollers
The number of pre-forked instances of [history pollers](/manual/concepts/server#server_process_types).<br>Only required for calculated checks.

Default: `5`<br>
Range: 0-1000

[comment]: # ({/899c458d-97ad4fb5})

[comment]: # ({0b85d605-565030c6})
##### StartHTTPPollers
The number of pre-forked instances of [HTTP pollers](/manual/concepts/server#server_process_types)**^[1](zabbix_server#footnotes)^**.

Default: `1`<br>
Range: 0-1000

[comment]: # ({/0b85d605-565030c6})

[comment]: # ({616d3f0a-70082588})
##### StartIPMIPollers
The number of pre-forked instances of [IPMI pollers](/manual/concepts/server#server_process_types).

Default: `0`<br>
Range: 0-1000

[comment]: # ({/616d3f0a-70082588})

[comment]: # ({f1112e4f-5dbd1f62})
##### StartJavaPollers
The number of pre-forked instances of [Java pollers](/manual/concepts/server#server_process_types)**^[1](zabbix_server#footnotes)^**.

Default: `0`<br>
Range: 0-1000

[comment]: # ({/f1112e4f-5dbd1f62})

[comment]: # ({e3e5ccd1-d9d80e3e})
##### StartLLDProcessors
The number of pre-forked instances of low-level discovery (LLD) [workers](/manual/concepts/server#server_process_types)**^[1](zabbix_server#footnotes)^**.<br>The LLD manager process is automatically started when an LLD worker is started.

Default: `2`<br>
Range: 0-100

[comment]: # ({/e3e5ccd1-d9d80e3e})

[comment]: # ({1acacf45-79d5cf2e})
##### StartODBCPollers
The number of pre-forked instances of [ODBC pollers](/manual/concepts/server#server_process_types)**^[1](zabbix_server#footnotes)^**.

Default: `1`<br>
Range: 0-1000

[comment]: # ({/1acacf45-79d5cf2e})

[comment]: # ({a461f1a2-e788444b})
##### StartPingers
The number of pre-forked instances of [ICMP pingers](/manual/concepts/server#server_process_types)**^[1](zabbix_server#footnotes)^**.

Default: `1`<br>
Range: 0-1000

[comment]: # ({/a461f1a2-e788444b})

[comment]: # ({0f5e5022-a12742d5})
##### StartPollersUnreachable
The number of pre-forked instances of [pollers for unreachable hosts](/manual/concepts/server#server_process_types) (including IPMI and Java)**^[1](zabbix_server#footnotes)^**.<br>At least one poller for unreachable hosts must be running if regular, IPMI or Java pollers are started.

Default: `1`<br>
Range: 0-1000

[comment]: # ({/0f5e5022-a12742d5})

[comment]: # ({0d9b24b3-5a731224})
##### StartPollers
The number of pre-forked instances of [pollers](/manual/concepts/server#server_process_types)**^[1](zabbix_server#footnotes)^**.

Default: `5`<br>
Range: 0-1000

[comment]: # ({/0d9b24b3-5a731224})

[comment]: # ({20d34975-bdcb0d21})
##### StartPreprocessors
The number of pre-started instances of preprocessing [workers](/manual/concepts/server#server_process_types)**^[1](zabbix_server#footnotes)^**.

Default: `3`<br>
Range: 1-1000

[comment]: # ({/20d34975-bdcb0d21})

[comment]: # ({d51e4e85-d6d62537})
##### StartProxyPollers
The number of pre-forked instances of [pollers for passive proxies](/manual/concepts/server#server_process_types)**^[1](zabbix_server#footnotes)^**.

Default: `1`<br>
Range: 0-250

[comment]: # ({/d51e4e85-d6d62537})

[comment]: # ({647b405a-469b0927})
##### StartReportWriters
The number of pre-forked instances of [report writers](/manual/concepts/server#server_process_types).<br>If set to 0, scheduled report generation is disabled.<br>The report manager process is automatically started when a report writer is started.

Default: `0`<br>
Range: 0-100

[comment]: # ({/647b405a-469b0927})

[comment]: # ({eee4be4c-9cccc249})
##### StartSNMPTrapper
If set to 1, an [SNMP trapper](/manual/concepts/server#server_process_types) process will be started.

Default: `0`<br>
Range: 0-1

[comment]: # ({/eee4be4c-9cccc249})

[comment]: # ({797ae5ff-f1f30b44})
##### StartTimers
The number of pre-forked instances of [timers](/manual/concepts/server#server_process_types).<br>Timers process maintenance periods.

Default: `1`<br>
Range: 1-1000

[comment]: # ({/797ae5ff-f1f30b44})

[comment]: # ({e2d3c79b-368a443a})
##### StartTrappers
The number of pre-forked instances of [trappers](/manual/concepts/server#server_process_types)**^[1](zabbix_server#footnotes)^**.<br>Trappers accept incoming connections from Zabbix sender, active agents and active proxies.

Default: `5`<br>
Range: 1-1000

[comment]: # ({/e2d3c79b-368a443a})

[comment]: # ({da3d0c65-16e17139})
##### StartVMwareCollectors
The number of pre-forked [VMware collector](/manual/concepts/server#server_process_types) instances.

Default: `0`<br>
Range: 0-250

[comment]: # ({/da3d0c65-16e17139})

[comment]: # ({91fbe848-0da7c397})
##### StatsAllowedIP
A list of comma delimited IP addresses, optionally in CIDR notation, or DNS names of external Zabbix instances. Stats request will be accepted only from the addresses listed here. If this parameter is not set no stats requests will be accepted.<br>If IPv6 support is enabled then '127.0.0.1', '::127.0.0.1', '::ffff:127.0.0.1' are treated equally and '::/0' will allow any IPv4 or IPv6 address. '0.0.0.0/0' can be used to allow any IPv4 address.

Example:

    StatsAllowedIP=127.0.0.1,192.168.1.0/24,::1,2001:db8::/32,zabbix.example.com

[comment]: # ({/91fbe848-0da7c397})

[comment]: # ({28cc089d-db780aaa})
##### Timeout
Specifies how long we wait for agent, SNMP device or external check in seconds.

Default: `3`<br>
Range: 1-30

[comment]: # ({/28cc089d-db780aaa})

[comment]: # ({ab7b9064-68df390b})
##### TLSCAFile
The full pathname of a file containing the top-level CA(s) certificates for peer certificate verification, used for encrypted communications between Zabbix components.

[comment]: # ({/ab7b9064-68df390b})

[comment]: # ({17bd8bac-24534d6d})
##### TLSCertFile
The full pathname of a file containing the server certificate or certificate chain, used for encrypted communications between Zabbix components.

[comment]: # ({/17bd8bac-24534d6d})

[comment]: # ({953e7fd5-0c9251b8})
##### TLSCipherAll
The GnuTLS priority string or OpenSSL (TLS 1.2) cipher string. Override the default ciphersuite selection criteria for certificate- and PSK-based encryption.

Example:

    TLS_AES_256_GCM_SHA384:TLS_CHACHA20_POLY1305_SHA256:TLS_AES_128_GCM_SHA256

[comment]: # ({/953e7fd5-0c9251b8})

[comment]: # ({832946e7-74bef172})
##### TLSCipherAll13
The cipher string for OpenSSL 1.1.1 or newer in TLS 1.3. Override the default ciphersuite selection criteria for certificate- and PSK-based encryption.

Example for GnuTLS: 

    NONE:+VERS-TLS1.2:+ECDHE-RSA:+RSA:+ECDHE-PSK:+PSK:+AES-128-GCM:+AES-128-CBC:+AEAD:+SHA256:+SHA1:+CURVE-ALL:+COMP-NULL::+SIGN-ALL:+CTYPE-X.509

Example for OpenSSL: 

    EECDH+aRSA+AES128:RSA+aRSA+AES128:kECDHEPSK+AES128:kPSK+AES128

[comment]: # ({/832946e7-74bef172})

[comment]: # ({c9cf41e3-a0eeb337})
##### TLSCipherCert
The GnuTLS priority string or OpenSSL (TLS 1.2) cipher string. Override the default ciphersuite selection criteria for certificate-based encryption.

Example for GnuTLS: 

    NONE:+VERS-TLS1.2:+ECDHE-RSA:+RSA:+AES-128-GCM:+AES-128-CBC:+AEAD:+SHA256:+SHA1:+CURVE-ALL:+COMP-NULL:+SIGN-ALL:+CTYPE-X.509

Example for OpenSSL: 

    EECDH+aRSA+AES128:RSA+aRSA+AES128

[comment]: # ({/c9cf41e3-a0eeb337})

[comment]: # ({5956ba54-07722feb})
##### TLSCipherCert13
The cipher string for OpenSSL 1.1.1 or newer in TLS 1.3. Override the default ciphersuite selection criteria for certificate-based encryption.

[comment]: # ({/5956ba54-07722feb})

[comment]: # ({038b665a-b3e6744e})
##### TLSCipherPSK
The GnuTLS priority string or OpenSSL (TLS 1.2) cipher string. Override the default ciphersuite selection criteria for PSK-based encryption.

Example for GnuTLS: 

    NONE:+VERS-TLS1.2:+ECDHE-PSK:+PSK:+AES-128-GCM:+AES-128-CBC:+AEAD:+SHA256:+SHA1:+CURVE-ALL:+COMP-NULL:+SIGN-ALL

Example for OpenSSL: 

    kECDHEPSK+AES128:kPSK+AES128

[comment]: # ({/038b665a-b3e6744e})

[comment]: # ({b96d7f16-a67b06d2})
##### TLSCipherPSK13
The cipher string for OpenSSL 1.1.1 or newer in TLS 1.3. Override the default ciphersuite selection criteria for PSK-based encryption.

Example:

    TLS_CHACHA20_POLY1305_SHA256:TLS_AES_128_GCM_SHA256

[comment]: # ({/b96d7f16-a67b06d2})

[comment]: # ({139c3a4e-7b53bee4})
##### TLSCRLFile
The full pathname of a file containing revoked certificates. This parameter is used for encrypted communications between Zabbix components.

[comment]: # ({/139c3a4e-7b53bee4})

[comment]: # ({8267f910-e236df69})
##### TLSKeyFile
The full pathname of a file containing the server private key, used for encrypted communications between Zabbix components.

[comment]: # ({/8267f910-e236df69})

[comment]: # ({ddb80460-0b008346})
##### TmpDir
The temporary directory.

Default: `/tmp`

[comment]: # ({/ddb80460-0b008346})

[comment]: # ({b0f11a74-621f6e9f})
##### TrapperTimeout
Specifies how many seconds the trapper may spend processing new data.

Default: `300`<br>
Range: 1-300

[comment]: # ({/b0f11a74-621f6e9f})

[comment]: # ({d4dbd0ad-02c73c1a})
##### TrendCacheSize
The size of the trend cache, in bytes.<br>The shared memory size for storing trends data.

Default: `4M`<br>
Range: 128K-2G

[comment]: # ({/d4dbd0ad-02c73c1a})

[comment]: # ({9ce42d11-5eeafa8d})
##### TrendFunctionCacheSize
The size of the trend function cache, in bytes.<br>The shared memory size for caching calculated trend function data.

Default: `4M`<br>
Range: 128K-2G

[comment]: # ({/9ce42d11-5eeafa8d})

[comment]: # ({05459db6-f61cf9e1})
##### UnavailableDelay
Determines how often host is checked for availability during the [unavailability](/manual/appendix/items/unreachability#unavailable_host) period in seconds.

Default: `60`<br>
Range: 1-3600

[comment]: # ({/05459db6-f61cf9e1})

[comment]: # ({c068bc9d-98b994ea})
##### UnreachableDelay
Determines how often host is checked for availability during the [unreachability](/manual/appendix/items/unreachability#unreachable_host) period in seconds.

Default: `15`<br>
Range: 1-3600

[comment]: # ({/c068bc9d-98b994ea})

[comment]: # ({c6798a21-df350752})
##### UnreachablePeriod
Determines after how many seconds of [unreachability](/manual/appendix/items/unreachability#unreachable_host) treats a host as unavailable.

Default: `45`<br>
Range: 1-3600

[comment]: # ({/c6798a21-df350752})

[comment]: # ({ee4ad0a4-a9a2ec9d})
##### User
Drop privileges to a specific, existing user on the system.<br>Only has effect if run as 'root' and AllowRoot is disabled.

Default: `zabbix`

[comment]: # ({/ee4ad0a4-a9a2ec9d})

[comment]: # ({e75cbec4-20ddb562})
##### ValueCacheSize
The size of the history value cache, in bytes.<br>The shared memory size for caching item history data requests.<br>Setting to 0 disables the value cache (not recommended).<br>When the value cache runs out of the shared memory a warning message is written to the server log every 5 minutes.

Default: `8M`<br>
Range: 0,128K-64G

[comment]: # ({/e75cbec4-20ddb562})

[comment]: # ({7c856e0c-35521bcc})
##### Vault
Specifies the vault provider:<br>*HashiCorp* - HashiCorp KV Secrets Engine version 2<br>*CyberArk*  - CyberArk Central Credential Provider<br>Must match the vault provider set in the frontend.

Default: `HashiCorp`

[comment]: # ({/7c856e0c-35521bcc})

[comment]: # ({f8367d93-664924ee})
##### VaultDBPath
Specifies a location, from where database credentials should be retrieved by keys. Depending on the Vault, can be vault path or query.<br> The keys used for HashiCorp are 'password' and 'username'. 

Example: 

    secret/zabbix/database

The keys used for CyberArk are 'Content' and 'UserName'.

Example: 

    AppID=zabbix_server&Query=Safe=passwordSafe;Object=zabbix_proxy_database

This option can only be used if DBUser and DBPassword are not specified.

[comment]: # ({/f8367d93-664924ee})

[comment]: # ({5e002807-778803e5})
##### VaultTLSCertFile
The name of the SSL certificate file used for client authentication<br> The certificate file must be in PEM1 format. <br> If the certificate file contains also the private key, leave the SSL key file field empty. <br> The directory containing this file is specified by the configuration parameter SSLCertLocation.<br>This option can be omitted but is recommended for CyberArkCCP vault.

[comment]: # ({/5e002807-778803e5})

[comment]: # ({e8e87e65-755d874e})
##### VaultTLSKeyFile
The name of the SSL private key file used for client authentication. <br> The private key file must be in PEM1 format. <br> The directory containing this file is specified by the configuration parameter SSLKeyLocation. <br>This option can be omitted but is recommended for CyberArkCCP vault.

[comment]: # ({/e8e87e65-755d874e})

[comment]: # ({30558bb7-cc25a6c8})
##### VaultToken
The HashiCorp Vault authentication token that should have been generated exclusively for Zabbix server with read-only permission to the paths specified in [Vault macros](/manual/config/macros/user_macros#configuration) and read-only permission to the path specified in the optional VaultDBPath configuration parameter.<br>It is an error if VaultToken and VAULT\_TOKEN environment variable are defined at the same time.

Mandatory: Yes, if Vault is set to *HashiCorp*; otherwise no

[comment]: # ({/30558bb7-cc25a6c8})

[comment]: # ({73818a04-f46a0763})
##### VaultURL
The vault server HTTP\[S\] URL. The system-wide CA certificates directory will be used if SSLCALocation is not specified.

Default: `https://127.0.0.1:8200`

[comment]: # ({/73818a04-f46a0763})

[comment]: # ({141b81e3-854dfe73})
##### VMwareCacheSize
The shared memory size for storing VMware data.<br>A VMware internal check zabbix\[vmware,buffer,...\] can be used to monitor the VMware cache usage (see [Internal checks](/manual/config/items/itemtypes/internal)).<br>Note that shared memory is not allocated if there are no vmware collector instances configured to start.

Default: `8M`<br>
Range: 256K-2G

[comment]: # ({/141b81e3-854dfe73})

[comment]: # ({fefd3f8c-2a646d43})
##### VMwareFrequency
The delay in seconds between data gathering from a single VMware service.<br>This delay should be set to the least update interval of any VMware monitoring item.

Default: `60`<br>
Range: 10-86400

[comment]: # ({/fefd3f8c-2a646d43})

[comment]: # ({a7e9d0bb-cf5957ec})
##### VMwarePerfFrequency
The delay in seconds between performance counter statistics retrieval from a single VMware service. This delay should be set to the least update interval of any VMware monitoring [item](/manual/config/items/itemtypes/simple_checks/vmware_keys#footnotes) that uses VMware performance counters.

Default: `60`<br>
Range: 10-86400

[comment]: # ({/a7e9d0bb-cf5957ec})

[comment]: # ({80852117-eefd2428})
##### VMwareTimeout
The maximum number of seconds a vmware collector will wait for a response from VMware service (vCenter or ESX hypervisor).

Default: `10`<br>
Range: 1-300

[comment]: # ({/80852117-eefd2428})

[comment]: # ({87328cb2-738954e5})
##### WebServiceURL
The HTTP\[S\] URL to Zabbix web service in the format `<host:port>/report`. 

Example: 

    WebServiceURL=http://localhost:10053/report

[comment]: # ({/87328cb2-738954e5})

[comment]: # ({19557f80-2ab44494})
#### Footnotes

^**1**^ Note that too many data gathering processes (pollers,
unreachable pollers, ODBC pollers, HTTP pollers, Java pollers, pingers, trappers,
proxypollers) together with IPMI manager, SNMP trapper and preprocessing
workers can **exhaust** the per-process file descriptor limit for the
preprocessing manager.

::: notewarning
This will cause Zabbix server to stop (usually
shortly after the start, but sometimes it can take more time). The
configuration file should be revised or the limit should be raised to
avoid this situation.
:::

^**2**^ When a lot of items are deleted it increases the load to the
database, because the housekeeper will need to remove all the history
data that these items had. For example, if we only have to remove 1 item
prototype, but this prototype is linked to 50 hosts and for every host
the prototype is expanded to 100 real items, 5000 items in total have to
be removed (1\*50\*100). If 500 is set for MaxHousekeeperDelete
(MaxHousekeeperDelete=500), the housekeeper process will have to remove
up to 2500000 values (5000\*500) for the deleted items from history and
trends tables in one cycle.

[comment]: # ({/19557f80-2ab44494})
