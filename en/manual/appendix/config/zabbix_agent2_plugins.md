[comment]: # attributes: notoc

[comment]: # ({b4814cf1-b4814cf1})
# 7 Zabbix agent 2 plugins

[comment]: # ({/b4814cf1-b4814cf1})

[comment]: # ({32e5ff52-32e5ff52})
#### Overview

This section contains descriptions of configuration file parameters for
Zabbix agent 2 plugins. Please use the sidebar to access information
about the specific plugin.

[comment]: # ({/32e5ff52-32e5ff52})
