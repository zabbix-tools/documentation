[comment]: # aside:3

[comment]: # ({417cb4cd-df3774f3})

# Plugins

[comment]: # ({/417cb4cd-df3774f3})

[comment]: # ({243c3081-d4691cc1})

### Overview

Custom loadable plugins extend Zabbix agent 2 functionality. They are compiled separately, but use a package shared with Zabbix agent 2. 

Each plugin is a *Go* package that defines the structure and implements one or several plugin interfaces
(*Exporter*, *Configurator*, *Runner*).

Jump to:

- [Write your first plugin](/devel/plugins/how_to)
- [Plugin interfaces](/devel/plugins/interfaces) 

### Connection diagram

Zabbix agent 2 connects bidirectionally to the plugins using UNIX sockets on Linux and Named Pipes on Windows. 

The connection diagram below illustrates the communication process between Zabbix agent 2 and a loadable plugin and the metrics collection process.

![](../../assets/en/devel/plugin/connection_diagram.png)

[comment]: # ({/243c3081-d4691cc1})

