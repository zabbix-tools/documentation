[comment]: # ({579805a5-3e219327})
# Plugin interfaces

This section describes available plugin interfaces.

[comment]: # ({/579805a5-3e219327})

[comment]: # ({e3b2749e-bd74c36c})

### plugin.Exporter

*Exporter* is the simplest interface that performs a poll and returns a value (values), nothing, or error.
It accepts a preparsed item key, parameters, and context.
Access to all other plugin interfaces is exclusive and no method can be called if a plugin is already performing a task.
Also, there is a limit of 100 maximum concurrent *Export()* calls per plugin, which can be reduced according to the requirements for each plugin.

[comment]: # ({/e3b2749e-bd74c36c})

[comment]: # ({68794123-a9e7836a})

### plugin.Configurator

*Configurator* interface provides plugin configuration parameters from Zabbix agent 2 configuration files.

[comment]: # ({/68794123-a9e7836a})

[comment]: # ({6c39575d-d6cfe5ec})

### plugin.Runner

*Runner* interface provides the means for performing initialization when a plugin is started (activated) and deinitialization when a plugin is stopped (deactivated).
For example, a plugin can start/stop some background *goroutine* by implementing the Runner interface.

[comment]: # ({/6c39575d-d6cfe5ec})
