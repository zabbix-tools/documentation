[comment]: # aside:3

[comment]: # ({25f6ba1d-9761799b})

# Examples

You can use several empty examples as well as existing loadable plugins supplied by Zabbix as a reference: 

- [Examples](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/src/go/plugins/debug/external) 
- [MongoDB plugin](https://git.zabbix.com/projects/AP/repos/mongodb/browse)
- [PostgreSQL plugin](https://git.zabbix.com/projects/AP/repos/postgresql/browse)

[comment]: # ({/25f6ba1d-9761799b})
