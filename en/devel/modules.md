[comment]: # aside:2

[comment]: # ({f9b7e108-12a4ad08})

# Modules

[comment]: # ({/f9b7e108-12a4ad08})

[comment]: # ({1a0f6619-f1b884d6})
### What is a PHP frontend module?

-   A module is an entity with a unique ID, name, description, author, and other fields defined in it's manifest file,
    along with PHP, Javascript and other files located under a single subdirectory of */ui/modules*.
-   A module should conform to simple rules to guarantee correct operation.
-   A module must be installed (unpacked) and enabled in the frontend by the administrator.

[comment]: # ({/1a0f6619-f1b884d6})

[comment]: # ({05b693b4-4e4bad2b})

### What a module can be used for

-   Adding new functionality via custom frontend sections;
-   Creating custom dashboard widget types (see [widget modules](/devel/modules/widgets));
-   Overriding or extending the existing functionality.

[comment]: # ({/05b693b4-4e4bad2b})

[comment]: # ({ec03fb13-064ddd80})

### What a module cannot be used for

-   Registering a new API method or modifying an existing one.

[comment]: # ({/ec03fb13-064ddd80})

[comment]: # ({bacf43f1-bfe0360a})

### How modules work

-   An enabled module is launched on each HTTP request, before executing the action code.
-   The module will register new actions or redefine the existing ones.
-   The module will add new frontend sections and remove or redefine the existing ones.
-   The module will hook to frontend events like onBeforeAction and onTerminate, if needed.
-   The requested action is finally executed by running the action code - either the default one, or module-defined.

[comment]: # ({/bacf43f1-bfe0360a})

[comment]: # ({501a7539-7aea12f6})
### Where to go next

Whether you prefer to learn by doing or read the guidelines first, these pages contain the information and steps required to build your own modules:

- [Step-by-step tutorials for writing your first module](/devel/modules/tutorials)
- [Module file structure](/devel/modules/file_structure)
- [Widget module specifics](/devel/modules/widgets)
- [Module examples to reuse](/devel/modules/examples)

[comment]: # ({/501a7539-7aea12f6})
