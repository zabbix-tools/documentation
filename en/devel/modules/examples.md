[comment]: # aside:5

[comment]: # ({25e78166-cb56af0f})
# Examples

This section provides files of sample modules and widgets, which you can use as a base for your custom modules.

To use a module:

1. Download the ZIP archive.
2. Unpack the content into a separate directory inside */zabbix/ui/modules* folder.
3. Register the module in Zabbix frontend.

[comment]: # ({/25e78166-cb56af0f})

[comment]: # ({441c3aee-0914dca2})
### Module example

- When creating a host group, grant read permissions to configured user groups - [hg_auto_perm.zip](/../assets/en/devel/modules/examples/hg_auto_perm.zip)

[comment]: # ({/441c3aee-0914dca2})

[comment]: # ({a62af023-c8a013f0})
### Widget examples

- Minimal widget - [widget_min.zip](/../assets/en/devel/modules/examples/widget_min.zip)
- "Hello, world" widget using CSS only - [hello_world_css.zip](/../assets/en/devel/modules/examples/hello_world_css.zip)
- "Hello, world" widget using JavaScript only - [hello_world_js.zip](/../assets/en/devel/modules/examples/hello_world_js.zip)
- "Hello, world" widget using PHP - [hello_world_php.zip](/../assets/en/devel/modules/examples/hello_world_php.zip)

::: notetip
You can also use [Zabbix native widgets](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/ui/widgets) as examples.
:::

[comment]: # ({/a62af023-c8a013f0})
