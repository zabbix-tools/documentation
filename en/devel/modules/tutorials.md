[comment]: # aside:4

[comment]: # ({14c4fa65-c02f72d3})
# Tutorials

This section contains practical step-by-step tutorials to illustrate how to build a custom [module](module) and a [widget](widget) in Zabbix.

[comment]: # ({/14c4fa65-c02f72d3})
