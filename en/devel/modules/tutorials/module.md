[comment]: # aside:1

[comment]: # ({b311df1e-8fe7205c})

# Create a module (tutorial)

[comment]: # ({/b311df1e-8fe7205c})

[comment]: # ({6320d7bd-5fb5b83a})
This is a step-by-step tutorial to create a basic Zabbix frontend module. You can download all files of this module as a ZIP archive: [MyAddress.zip](../../../../assets/en/devel/modules/examples/MyAddress.zip).

[comment]: # ({/6320d7bd-5fb5b83a})

[comment]: # ({12901d3d-5ce71b9d})

## What you'll build

During this tutorial, you will create a frontend module that adds a new menu shortcut to open an existing Zabbix section
and then convert it into a more advanced frontend module that makes an HTTP request to *https://api.ipify.org*
and displays on the page the response.

![](../../../../assets/en/devel/modules/tutorials/module/module_view_finished.png){width=600}

[comment]: # ({/12901d3d-5ce71b9d})

[comment]: # ({22720451-8c2bcd9e})

## Part I - Basic module

[comment]: # ({/22720451-8c2bcd9e})

[comment]: # ({89493bfd-1d4de634})

### Register the module

1. Create a new directory *MyAddress* in *zabbix/ui/modules*.

2. Add *manifest.json* file with basic module metadata (see the description of [supported parameters](../file_structure/manifest)).

**ui/modules/MyAddress/manifest.json**

```json
{
    "manifest_version": 2.0,
    "id": "my-address",
    "name": "My IP Address",
    "version": "1.0",
    "namespace": "MyAddress",
    "description": "My External IP Address."
}
```

3. In Zabbix frontend, go to *Administration→General→Modules* section and press *Scan directory* button.

![](../../../../assets/en/devel/modules/tutorials/widget/scan_dir.png)

4. Locate a new module *My IP Address* in the list and change the module's status from Disabled to Enabled (click on the *Disabled* hyperlink in the *Status* column).

![](../../../../assets/en/devel/modules/tutorials/module/module_register.png){width=600}

The module is now registered in the frontend, but it is not visible anywhere, because you haven't defined any module functionality yet.
Once you add content to the module directory, you will see the changes in Zabbix frontend immediately upon refreshing the page.

[comment]: # ({/89493bfd-1d4de634})

[comment]: # ({959f9471-d88b8114})

### Create a menu entry

1. To add a new menu entry to the frontend, create *Module.php* file inside the directory *MyAddress*.

This file implements a new class *Module* that extends the default *CModule* class.
Class Module takes the pointer to the main menu and inserts a new menu entry called *My Address*.

*setAction()* method specifies an action to be executed upon clicking on the menu entry.
To start with, use the predefined action *userprofile.edit*, which will open the *User profile* page.
In part III of this tutorial you will learn how to create a custom action.

**ui/modules/MyAddress/Module.php**

```php
<?php

namespace Modules\MyAddress;

use Zabbix\Core\CModule,
    APP,
    CMenuItem;

class Module extends CModule {

    public function init(): void {
        APP::Component()->get('menu.main')
            ->add((new CMenuItem(_('My Address')))
            ->setAction('userprofile.edit'));
    }
}
```

:::noteclassic
You can replace 'userprofile.edit' with other actions, for example, 'charts.view' (opens Custom graphs), problems.view (opens Monitoring -> Problems), report.status (opens System information report).
:::

3. Refresh Zabbix frontend. There is now a new entry My Address at the bottom of the main Zabbix menu.
Click on *My Address* to open the User profile page.

![](../../../../assets/en/devel/modules/tutorials/module/my_address1.png){width=600}  

[comment]: # ({/959f9471-d88b8114})

[comment]: # ({fb4e785c-5c3fc480})

## Part II - Menu entry location change

In this section, you will move the menu entry My Address to the Monitoring section.
Now users will be able to access their user profile information from the Monitoring submenu.

1. Open and edit *Module.php* file.

**ui/modules/MyAddress/Module.php**

```php
<?php

namespace Modules\MyAddress;

use Zabbix\Core\CModule,
    APP,
    CMenuItem;

class Module extends CModule {

    public function init(): void {
        APP::Component()->get('menu.main')
            ->findOrAdd(_('Monitoring'))
            ->getSubmenu()
            ->insertAfter(_('Discovery'),
                (new CMenuItem(_('My Address')))->setAction('my.address')
            );
    }
}
```

2. Refresh Zabbix frontend. Expand Monitoring menu section and observe that My address is now located below Discovery section.

![](../../../../assets/en/devel/modules/tutorials/module/my_address2.png){width=600}

[comment]: # ({/fb4e785c-5c3fc480})

[comment]: # ({433ab77d-8798eff6})

## Part III - Module action

An action is implemented in 2 files: one takes care of the business logic implementation and another one is responsible for the view.
The action logic will be defined in the **MyAddress class** in the file **actions/MyAddress.php** and the view will be defined in the file **views/my.address.php**.

1. Add a directory *actions* to *MyAddress*.

2. Create *MyAddress.php* file inside the *actions* directory and define an action class *MyAddress*.

This action class will implement four functions: **init()**, **checkInput()**, **checkPermissions()**, **doAction()**.  Zabbix frontend calls the **doAction()** function when the action is requested. This function is responsible for the business logic of the module.

::: noteimportant
The data must be organized as an associated array. The array can be multidimensional and may contain any data expected by the view. 
:::

**ui/modules/MyAddress/actions/MyAddress.php**

```php
<?php

namespace Modules\MyAddress\Actions;

use CControllerResponseData,
    CController;

class MyAddress extends CController {

    public function init(): void {
        $this->disableCsrfValidation();
    }

    protected function checkInput(): bool {
        return true;
    }

    protected function checkPermissions(): bool {
        return true;
    }

    protected function doAction(): void {
        $data = ['my-ip' => file_get_contents("https://api.ipify.org")];
        $response = new CControllerResponseData($data);
        $this->setResponse($response);
    }
}
```

3. Add a directory *views* to the *MyAddress*.

4. Create *my.address.php* file inside the *views* directory and define the module view.

Note that the variable `$data` is available in the view without specifically defining it.
The framework automatically passes associated array to the view.

**ui/modules/MyAddress/views/my.address.php**

```php
<?php

(new CHtmlPage())
    ->setTitle(_('The HTML Title of My Address Page'))
    ->addItem(new CDiv($data['my-ip']))
    ->show();
```

5. The action has to be registered in the *manifest.json*. Open *manifest.json* and add a new object *actions* containing:

- action key - action name written in lowercase [a-z] with words separated by dots (in this tutorial, *my.address*).
- action class name (*MyAddress*) under the *class* key of the my.address object.
- action view name (*my.address*) under the *view* key of the my.address object.

**ui/modules/MyAddress/manifest.json**

```json
{
    "manifest_version": 2.0,
    "id": "my-address",
    "name": "My IP Address",
    "version": "1.0",
    "namespace": "MyAddress",
    "description": "My External IP Address.",
    "actions": {
        "my.address": {
            "class": "MyAddress",
            "view": "my.address"
        }
    }
}
```

6. Open *Module.php* and change the action name in the **setAction()** method to *my.address*.

**ui/modules/MyAddress/Module.php**

```php
<?php

namespace Modules\MyAddress;

use Zabbix\Core\CModule,
    APP,
    CMenuItem;

class Module extends CModule {

    public function init(): void {
        APP::Component()->get('menu.main')
            ->findOrAdd(_('Monitoring'))
            ->getSubmenu()
            ->insertAfter(_('Discovery'),
                (new CMenuItem(_('My Address')))->setAction('my.address')
            );
    }
}
```

7. Refresh Zabbix frontend. Click on the "My address" menu entry to see the IP address of your computer.

![](../../../../assets/en/devel/modules/tutorials/module/module_view_finished.png){width=600}


[comment]: # ({/433ab77d-8798eff6})
