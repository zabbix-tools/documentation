[comment]: # aside:3

[comment]: # ({e0e7fbad-7d26eea5})
# Widgets

Widgets are Zabbix frontend modules used for the dashboards.
Unless otherwise noted, all module guidelines are also applicable to widgets.

However, a widget is notably different from a module. To build a widget:

- specify the type "widget" in the [manifest.json file](/devel/modules/file_structure/manifest) ("type": "widget");
- include at least two views: one for the [widget presentation mode](/devel/modules/widgets/presentation#widget-view) and one for the [widget configuration mode](/devel/modules/widgets/configuration#widget-configuration-view) (example.widget.view.php and example.widget.edit.php);
- and a [controller](/devel/modules/widgets/presentation#widget-actions) for widget presentation (WidgetView.php);
- use and extend default [widget classes](/devel/modules/widgets/configuration).

[comment]: # ({/e0e7fbad-7d26eea5})
