[comment]: # aside:5

[comment]: # ({1f370d7c-8ac5649a})
# Register a new module

This section explains how to add a new module to Zabbix frontend.

[comment]: # ({/1f370d7c-8ac5649a})

[comment]: # ({77adee8c-cfc878a8})
## Pre-requisites

Before proceeding make sure, that:

- A module is located inside the *ui/modules* directory.
- The module has at least a basic version of [manifest.json](/devel/modules/file_structure/manifest) file.
- You have access to the Administration menu section in Zabbix (requires Super admin user role type).

:::notetip
The frontend will not install or even recognize incompatible modules.
:::

[comment]: # ({/77adee8c-cfc878a8})

[comment]: # ({5769cecf-65ec7200})
## Adding a module

Open *Administration→General→Modules* page and press *Scan directory*.

![](../../../../assets/en/devel/modules/tutorials/widget/scan_dir.png)

Locate your module in the list and activate it.

To activate a module, press on the *Disabled* hyperlink - the module's state will change to *Enabled*.

Press on the module name to view additional information about the module, such as author, version, or short description (if defined in the manifest).

[comment]: # ({/5769cecf-65ec7200})

[comment]: # ({0eb7d043-b30bf9b8})
## Widget preview

Widget modules, once added, become immediately visible in the dashboard widget list.

You can open a dashboard, switch to the edit mode and add the widget to the dashboard as usual.

When you make some changes to the widget, refresh the dashboard to view how the widget looks with the most recent updates.

[comment]: # ({/0eb7d043-b30bf9b8})
