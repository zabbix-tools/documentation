[comment]: # aside:4

[comment]: # ({3c100212-cd954d56})

# Assets

The folder *assets* may contain any files and subfolders that do not belong to other directories. You can use it for:

- JavaScript styles (must be inside *[assets/js](#assetsjs)*);
- CSS styles (must be inside *[assets/css](#assetscss)*);
- Images;
- Fonts;
- Anything else you need to include.

[comment]: # ({/3c100212-cd954d56})

[comment]: # ({2b4831f8-08585fb9})

### assets/js

*assets/js* directory is reserved and should only contain JavaScript files.
To be used by the widget, specify these files in the *[manifest.json](/devel/modules/file_structure/manifest)*.

For example:

```json
"assets": {
    "js": ["class.widget.js"]
}
```

[comment]: # ({/2b4831f8-08585fb9})

[comment]: # ({1da3de82-18641225})

### assets/css

*assets/css* is reserved and should only contain CSS style files.
To be used by the widget, specify these files in the *[manifest.json](/devel/modules/file_structure/manifest)*.

For example:

```json
"assets": {
    "css": ["mywidget.css"]
}
```

[comment]: # ({/1da3de82-18641225})

[comment]: # ({fcb10dd9-aedbe0c5})

##### CSS styles

CSS files may contain a custom attribute `theme` to define different style for a specific frontend themes.

Available themes and their attribute values:

-   **Blue** - [theme='blue-theme']
-   **Dark** - [theme='dark-theme']
-   **High-contrast light** - [theme='hc-light']
-   **High-contrast dark** - [theme='hc-dark']

Example:

```css
.widget {
    background-color: red;
}
 
[theme='dark-theme'] .widget {
    background-color: green;
}
```

[comment]: # ({/fcb10dd9-aedbe0c5})
