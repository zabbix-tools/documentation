[comment]: # aside:1

[comment]: # attributes: notoc

[comment]: # ({5af207c1-0ea8ed60})

# manifest.json

Any module needs the manifest.json file.
The file should be located in the module's primary directory (`ui/modules/module_name/manifest.json`).

As a bare minimum, manifest.json should specify these fields:

```json
{
    "manifest_version": 2.0,
    "id": "my_ip_address",
    "name": "My IP Address",
    "namespace": "MyIPAddress",
    "version": "1.0"
}
```

Parameters supported in **manifest.json** (press on the parameter name for a detailed description):

|Parameter |Description |Required |
|--|--------|--|
|[manifest_version](#manifest-version)|Manifest version of the module.| Yes |
|[id](#id) |Unique module ID. |^|
|[name](#name) |Module name that will be displayed in the Administration section. |^|
|[namespace](#namespace) |PHP namespace for module classes.|^|
|[version](#version) |Module version. |^|
|[type](#type) |Type of the module. For widget must be set to *widget* | Yes for widgets, otherwise no |
|[widget](#widget) |Widget configuration. Used for widgets only. |^|
|[actions](#actions) | Actions to register with the module. |^|
|[assets](#assets) | CSS styles and JavaScript files to include. | No |
|[author](#author) |Module author. |^|
|[config](#config) |Default values for custom module options. |^|
|[description](#description) |Module description.|^|
|[url](#url) |A link to the module description. |^|


[comment]: # ({/5af207c1-0ea8ed60})


[comment]: # ({e6ddbfde-7863337e})

### manifest_version {#manifest-version}

Manifest version of the module. Currently, supported version is **2.0**.

*Type*: Double

*Example*: 

```json
"manifest_version": 2.0
```

[comment]: # ({/e6ddbfde-7863337e})

[comment]: # ({4a354b32-40db7411})

### id

Module ID. Must be unique. To avoid future naming conflicts, it is recommended to use prefix for modules (author or company name, or any other).
For example, if a module is an example for lessons and the module name is "My module", then the ID will be "example_my_module".

*Type*: String

*Example*:

```json
"id": "example_my_module"
```

[comment]: # ({/4a354b32-40db7411})

[comment]: # ({6b60a366-1d94ce43})

### name

Module name that will be displayed in the Administration section.

*Type*: String

*Example*: 

```json
"name": "My module"
```

[comment]: # ({/6b60a366-1d94ce43})

[comment]: # ({346a79b5-b1e94eb0})

### namespace

PHP namespace for module classes.

*Type*: String

*Example*: 

```json
"namespace": "ClockWidget"
```

[comment]: # ({/346a79b5-b1e94eb0})

[comment]: # ({2801cc8a-6669130f})

### version

Module version. The version will be displayed in the Administration section.

*Type*: String

*Example*: 

```json
"version": "1.0"
```

[comment]: # ({/2801cc8a-6669130f})

[comment]: # ({3d827d1e-2f2ecfb4})

### type

Type of the module. Required for widgets and must equal "widget".

*Type*: String

*Default*: "module"

*Example*:

```json
"type": "widget"
```

[comment]: # ({/3d827d1e-2f2ecfb4})

[comment]: # ({9f849f5d-85cbb8b0})

### actions

Actions to register with the module.
Defining *class* object key for each action is required, other action keys are optional.

*Type*: Object

Supported object keys if *[type](#type)* is *module*:

-   **write.your.action.name** (object) - action name, should be written in lowercase [a-z], separating words with dots.
    Supports the keys:
    - **class** (string; required) - action class name.
    - **layout** (string) - action layout. Supported values: *layout.json*, *layout.htmlpage* (default), *null*.
    - **view** (string) - action view.

*Example*:

```json
"actions": {
    "module.example.list": {
        "class": "ExampleList",
        "view": "example.list",
        "layout": "layout.htmlpage"
        }
    }
```

Supported object keys if *[type](#type)* is *widget*:

-   **widget.{id}.view** (object) - file and class name for widget view. Replace **{id}** with the widget's [id](#id) value (for example, *widget.example_clock.view*).
    Supports the keys:
    - **class** (string; required) - action class name for widget view mode to extend the default CControllerDashboardWidgetView class.
      The class source file must be located in the *actions* directory.
    - **view** (string) - widget view. Must be located in the *views* directory.
      If the view file is *widget.view.php*, which is expected by default, this parameter maybe omitted.
      If using a different name, specify it here.
-   **widget.{id}.edit** (object) - file name for widget configuration view. Replace **{id}** with the widget's [id](#id) value (for example, *widget.example_clock.edit*).
    Supports the keys:
    - **class** (string; required) - action class name for widget configuration view mode. The class source file must be located in the *actions* directory.
    - **view** (string) - widget configuration view. Must be located in the *views* directory.
      If the view file is *widget.edit.php*, which is expected by default, this parameter maybe omitted.
      If using a different name, specify it here.

*Example*: 

```json
"actions": {
    "widget.tophosts.view": {
        "class": "WidgetView"
    },
    "widget.tophosts.column.edit": {
        "class": "ColumnEdit",
        "view": "column.edit",
        "layout": "layout.json"
    }
}
```

[comment]: # ({/9f849f5d-85cbb8b0})

[comment]: # ({29793a59-a0998fce})

### assets

CSS styles and JavaScript files to include.

*Type*: Object

*Supported object keys:*

-   **css** (array) - CSS files to include. The files must be located in the *assets/css*.
-   **js** (array) - JavaScript files to include. The files must be located in the *assets/js*.

*Example*: 

```json
"assets": {
    "css": ["widget.css"],
    "js": ["class.widget.js"]
}
```

[comment]: # ({/29793a59-a0998fce})

[comment]: # ({199df252-0a7c568c})

### author

Module author. The author will be displayed in the Administration section.

*Type*: String

*Example*: 

```json
"author": "John Smith"
```

[comment]: # ({/199df252-0a7c568c})

[comment]: # ({e21b146a-1})

### config

Default values for the module options. The object may contain any custom keys.
If specified, these values will be written into the database during module registration.
New variables added later will be written upon the first call.
Afterwards, the variable values can only be changed directly in the database.

*Type*: Object

*Example*:

```json
"config": {
    "username": "Admin",
    "password": "",
    "auth_url": "https://example.com/auth"
}
```

[comment]: # ({/e21b146a-1})

[comment]: # ({7be8b4c3-6a167d9e})

### description

Module description.

*Type*: String

*Example*: 

```json
"description": "This is a clock widget."
```

[comment]: # ({/7be8b4c3-6a167d9e})

[comment]: # ({23438afa-cb9c7cd8})

### widget

Widget configuration. Used, if *[type](#type)* is set to *widget*.

*Type*: Object

*Supported object keys:*

-   **name** (string) - used in the widget list and as default header. If empty, "name" parameter from the module will be used.

-   **size** (object) - default widget dimensions. Supports keys:
    - *width* (integer) - default widget width.
    - *height* (integer) - default widget height.

-   **form_class** (string) - class with widget fields form. Must be located in the *includes* directory.
    If the class is *WidgetForm.php*, which is expected by default, this parameter maybe omitted.
    If using a different name, specify it here.

-   **js_class** (string) - name of a JavaScript class for widget view mode to extend the default CWidget class.
    The class will be loaded with the dashboard.
    The class source file must be located in the assets/js directory. See also: [assets](#assets).

-   **use_time_selector** (boolean) - determines whether the widget requires dashboard time selector.
    Supported values: *true*, *false* (default).

-   **refresh_rate** (integer) - widget refresh rate in seconds (default: 60).

*Example*: 

```json
"widget": {
    "name": "",
    "size": {
        "width": 12,
        "height": 5
    },
    "form_class": "WidgetForm",
    "js_class": "CWidget",
    "use_time_selector": false,
    "refresh_rate": 60
}
```

[comment]: # ({/23438afa-cb9c7cd8})

[comment]: # ({f7946b9e-ee0f6140})

### url

A link to the module description. For widgets, this link will be opened when clicking on the help icon ![](/assets/en/manual/web_interface/help_link.png){class="nozoom"}
in the [*Add widget*](/manual/web_interface/frontend_sections/dashboards#adding-widgets) or
[*Edit widget*](/manual/web_interface/frontend_sections/dashboards#widgets) window.
If **url** is not specified, clicking on the help icon will open the general
[Dashboard widgets](/manual/web_interface/frontend_sections/dashboards/widgets) page.

*Type*: String

*Example*: 

```json
"url": "http://example.com"
```

[comment]: # ({/f7946b9e-ee0f6140})
