[comment]: # translation:outdated

[comment]: # ({new-3ca85094})
# 10. 虚拟机监控

[comment]: # ({/new-3ca85094})

[comment]: # ({new-621e23e6})
#### 概述

从 Zabbix 2.2.0版本开始支持对 VMware 的监控。

Zabbix 可以使用低级别自动发现（low-level discovery ） VMware
hypervisors（宿主机） 和
虚拟机，并根据事先定义的主机原型，为这些虚拟机创建Host并添加监控。

Zabbix 中默认提供了几个模板，可以直接用来监控 VMware vCenter 或 ESX
hypervisor。

支持 VMware vCenter 或 vSphere 版本最低为 4.1。

[comment]: # ({/new-621e23e6})

[comment]: # ({new-4c2e8939})
#### 明细

监控虚拟机分两个步骤完成。首先，Zabbix 是通过 *vmware collector*
进程来获取虚拟机数据。这些进程通过 SOAP 协议从 VMware Web
SDK服务获取必要的信息，对其进行预处理并存储到 Zabbix server
共享内存中。然后，zabbix pollers 通过 zabbix 简单检查 [VMware
keys](/manual/config/items/itemtypes/simple_checks/vmware_keys)
来检索这些数据。

从 Zabbix 2.4.4 开始，收集的数据分为两种类型：VMware配置数据和
VMware性能数据。这两种类型都由 *vmware collectors*
进程独立收集。因此，建议启用比受监控的VMware服务更多的收集器。否则，VMware性能统计信息的检索可能会由于检索VMware配置数据而延迟（对于较大型的环境，会需要一段时间）。

目前基于VMware性能统计信息只有数据存储，网络接口和磁盘设备统计信息和自定义性能计数器项。

[comment]: # ({/new-4c2e8939})

[comment]: # ({new-68c3f7d5})
#### 配置

要使虚拟机监控正常工作，[编译](/manual/installation/install#configure_the_sources)安装Zabbix时应加上
--with-libxml2 和 --with-libcurl 编译类库选项。

以下配置文件参数可用于调整虚拟机监控：

-   **StartVMwareCollectors** - 预先启动Vmware
    collector收集器实例的数量。\
    此值取决于要监控的 VMware 服务的数量。在大多数情况下，这应该是：\
    *servicenum < StartVMwareCollectors < (servicenum \* 2)*\
    其中 *servicenum* 是 VMware 服务的数量。例如：如果您有 1 个 VMware
    服务要将 StartVMwareCollectors 设置为 2，那么如果您有 3 个 VMware
    服务，请将其设置为 5。请注意，在大多数情况下，此值不应小于
    2，不应大于 VMware 数量的 2 倍服务。还要记住，此值还取决于 VMware
    环境大小和 *VMwareFrequency* 和 *VMwarePerfFrequency*
    配置参数（请参阅下文）。
-   **VMwareCacheSize** - 用于存储VMware
    数据的缓存容量，默认为8M，取值范围：256K-2G。
-   **VMwareFrequency** -
    连接到VMware服务收集一个新数据的频率，默认为60秒，取值范围：10-86400。
-   **VMwarePerfFrequency** -
    连接到VMware服务收集性能数据的频率，默认为60秒，取值范围10-86400。
-   **VMwareTimeout** - VMware collector 等待VMware
    服务响应的时间，默认为10秒，取值范围：1-300。

有关更多详细信息，请参阅 [server](/manual/appendix/config/zabbix_server)
和 [proxy](/manual/appendix/config/zabbix_proxy) 的配置文件页面。

[comment]: # ({/new-68c3f7d5})

[comment]: # ({new-e5966479})
#### 自动发现

Zabbix 可以使用 低级别发现(low-level discovery) 规则自动发现 VMware
hypervisors(宿主机)和虚拟机。

![](../../assets/en/manual/vm_monitoring/vm_hypervisor_lld.png)

所有强制输入字段都用红色的星号标记。

以上截图中的发现规则key值是 *vmware.hv.discovery\[{$URL}\]*。

[comment]: # ({/new-e5966479})


[comment]: # ({new-2f026fa1})
#### 可以使用的模板

Zabbix 中默认提供了几个现成的模板，用于监控 VMware vCenter 或 ESX
hypervisor。

这些模板包含事先定义的 LLD 规则以及用于监视虚拟安装的内置检查。

请注意，"*Template Virt VMware*"监控模板应用于 VMware vCenter 和 ESX
hypervisor（宿主机）监控。 "*Template Virt VMware
Hypervisor*"和"*Template Virt VMware
Guest*"模板由前者自动发现关联宿主机和虚拟机，通常不应该手动链接到单个主机。

![](../../assets/en/manual/vm_monitoring/vm_templates.png)

::: noteclassic
如果您的Zabbix从 2.2
之前的版本升级并且没有此类模板，您可以手动导入，从社区页面下载
[官方模板](http://www.zabbix.org/wiki/Zabbix_Templates/Official_Templates)。默认这些模板依赖于
*VMware VirtualMachinePowerState* 和 //VMware 状态值 //
映射，因此有必要首先创建这些值映射（使用 [SQL
脚本](https://www.zabbix.org/wiki/Zabbix_Templates/SQLs_for_Official_Templates)，手动或从
XML 导入）
:::

[comment]: # ({/new-2f026fa1})

[comment]: # ({new-317ec2ff})
#### 主机配置

要使用 VMware 简单检查，主机必须定义以下用户宏：

-   **{$URL}** - VMware 服务 (vCenter or ESX hypervisor) SDK URL
    (<https://servername/sdk>).
-   **{$USERNAME}** - VMware 服务用户名
-   **{$PASSWORD}** - VMware 服务{$ USERNAME}用户密码

[comment]: # ({/new-317ec2ff})

[comment]: # ({new-96cbfb15})
#### 例子

以下示例演示如何在 Zabbix 上快速配置 VMware 监控：

-   编译安装 zabbix server 时添加依赖项（--with-libxml2 和
    --with-libcurl）
-   将 Zabbix server 配置文件中的 StartVMwareCollectors 选项设置为 1
    或更多
-   创建新主机
-   设置 监控VMware 服务所需的身份验证相关的主机宏：

```{=html}
<!-- -->
```
        {{..:..:assets:en:manual:vm_monitoring:vm_host_macros.png|}}
    * 将 VMware 服务模板链接到主机：
        {{..:..:assets:en:manual:vm_monitoring:vm_host_templates.png|}}
    * 单击 //Add// 按钮保存主机

[comment]: # ({/new-96cbfb15})

[comment]: # ({new-44948bb1})
#### 扩展日志

使用调试级别 5 进行详细调试时，VMware
收集器收集的数据会记录到日志中。此级别可以在
[server](/manual/appendix/config/zabbix_server) 和
[proxy](/manual/appendix/config/zabbix_proxy)
配置文件中设置，或使用运行时控制选项（`-R log_level_increase="vmware collector,N"`，其中
N 是过程编号）。以下示例说明如果配置将调试级别设置为 4 ，扩展日志启动：

提高所有 vmware 收集器的日志级别：

    shell> zabbix_server -R log_level_increase="vmware collector"

    提高第二个 vmware 收集器的日志级别：
    shell> zabbix_server -R log_level_increase="vmware collector,2"

如果不需要对 VMware
收集器数据进行扩展日志，可以使用`-R log_level_decrease`选项进行停止。

[comment]: # ({/new-44948bb1})

[comment]: # ({new-f6f9d3ef})
#### 故障排查

-   如果Vmware监控失败，监控项不可用，请确认使用VMware
    vSphere较新版本，它们监控项是否不可用或默认被关闭，是否限制Zabbix访问VMware性能计数器数据库等配置。详情请参阅
    [ZBX-12094](https://support.zabbix.com/browse/ZBX-12094)

[comment]: # ({/new-f6f9d3ef})
