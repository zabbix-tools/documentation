[comment]: # translation:outdated

[comment]: # ({new-cbb57ea2})
# 1 Server

[comment]: # ({/new-cbb57ea2})

[comment]: # ({000782d3-0e549571})
#### 概述

Zabbix server 是整个 Zabbix 软件的核心程序。

Zabbix Server负责执行数据的主动轮询和被动获取，计算触发器条件，向用户发送通知。它是Zabbix Agent 和 Proxy 报告系统可用性和完整性数据的核心组件。Server自身可以通过简单服务远程检查网络服务（如Web服务器和邮件服务器）。

Zabbix Server是所有配置、统计和操作数据的中央存储中心，也是Zabbix监控系统的告警中心。在监控的系统中出现任何异常，将被发出通知给管理员。

基本的 Zabbix Server 的功能分解成为三个不同的组件。他们是：Zabbixserver、Web前端和数据库。

Zabbix 的所有配置信息都存储在 Server和Web前端进行交互的数据库中。例如，当你通过Web前端（或者API）新增一个监控项时，它会被添加到数据库的监控项表里。然后，Zabbixserver 以每分钟一次的频率查询监控项表中的有效项，接着将它存储在 Zabbix server 中的缓存里。这就是为什么 Zabbix前端所做的任何更改需要花费两分钟左右才能显示在最新的数据段的原因。

[comment]: # ({/000782d3-0e549571})

[comment]: # ({new-c20247df})
#### 服务进程

[comment]: # ({/new-c20247df})

[comment]: # ({d620bfa3-1314fd6f})
#####  通过二进制包安装的组件

Zabbix server 进程以守护进程（Deamon）运行。Zabbix server 的启动可以通过执行以下命令来完成：

    shell> service zabbix-server start

上述命令在大多数的 GNU/Linux 系统下都可以正常完成。如果是其他系统，你可能要尝试以下命令来运行：

    shell> /etc/init.d/zabbix-server start

类似的，停止、重启、查看状态，则需要执行以下命令：

    shell> service zabbix-server stop
    shell> service zabbix-server restart
    shell> service zabbix-server status

[comment]: # ({/d620bfa3-1314fd6f})

[comment]: # ({23278f53-bbb4c5d8})
##### 手动启动

如果以上操作均无效，您可能需要手动启动，找到 Zabbix Server
二进制文件的路径并且执行：

    shell> zabbix_server

您可以将以下命令行参数用于 Zabbix server：

    -c --config <file>              配置文件路径（默认/usr/local/etc/zabbix_server.conf）
    -R --runtime-control <option>   执行管理功能
    -h --help                       帮助
    -V --version                    显示版本号


使用命令行参数运行 Zabbix server 的示例:：

    shell> zabbix_server -c /usr/local/etc/zabbix_server.conf
    shell> zabbix_server --help
    shell> zabbix_server -V

[comment]: # ({/23278f53-bbb4c5d8})

[comment]: # ({new-a339702b})
##### 运行时控制

运行时控制选项：

|选项|描述|目标|
|--------|------------|------|
|config\_cache\_reload|重新加载配置缓存。如果当前正在加载缓存，则忽略。| |
|diaginfo\[=<**target**>\]|在服务器日志文件中收集诊断信息。|**historycache** - 历史缓存统计<br>**valuecache** - 值缓存统计<br>* *preprocessing** - 预处理管理器统计信息<br>**alerting** - 警报管理器统计信息<br>**lld** - LLD 管理器统计信息<br>**locks** - 互斥锁列表（在 ** 上为空BSD* 系统）|
|ha\_status|记录高可用性 (HA) 集群状态。| |
|ha\_remove\_node=target|删除由其列出的编号指定的高可用性 (HA) 节点。<br>请注意，无法删除活动/备用节点。|**target** - 列表中的节点编号（可以通过运行 ha\_status 获得）|
|ha\_set\_failover\_delay=delay|设置高可用性 (HA) 故障转移延迟。<br>支持时间后缀，例如10 秒，1 分钟。| |
|secrets\_reload|从 Vault 重新加载秘钥。| |
|service\_cache\_reload|重新加载服务管理器缓存。| |
|snmp\_cache\_reload|重新加载 SNMP 缓存，清除所有主机的 SNMP 属性（引擎时间、引擎启动、引擎 ID、凭据）。| |
|housekeeper\_execute|启动管家程序。如果当前正在进行管家处理程序，则忽略。| |
|log\_level\_increase\[=<**target**>\]|增加日志级别，如果未指定 target，则影响所有进程。<br>在 **BSD* 系统上不受支持。|**进程类型* * - 指定类型的所有进程（例如，轮询器）<br>查看所有 [服务器进程类型](#server_process_types)。<br>**进程类型，N** - 进程类型和编号（例如，轮询器，3） <br>**pid** - 进程标识符（1 到 65535）。对于较大的值，请将目标指定为“进程类型，N”。|
|log\_level\_decrease\[=<**target**>\]|降低日志级别，如果未指定 target，则影响所有进程。<br>在 **BSD* 系统上不支持。|^|

[comment]: # ({/new-a339702b})

[comment]: # ({6e72a8b1-1cb7f51c})
使用运行时控制重新加载服务器配置的示例
缓存：

    shell> zabbix_server -c /usr/local/etc/zabbix_server.conf -R config_cache_reload

使用运行时控制收集诊断信息的示例：

    在服务器日志文件中收集所有可用的诊断信息：
    shell> zabbix_server -R diaginfo

    在服务器日志文件中收集历史缓存统计信息：
    shell> zabbix_server -R diaginfo=historycache

使用运行时控制重新加载 SNMP 缓存的示例：

    shell> zabbix_server -R snmp_cache_reload

使用运行时控制触发管家执行的示例：

    shell> zabbix_server -c /usr/local/etc/zabbix_server.conf -R housekeeper_execute

使用运行时控制更改日志级别的示例：

    增加所有进程的日志级别：
    shell> zabbix_server -c /usr/local/etc/zabbix_server.conf -R log_level_increase

    增加第二个轮询进程的日志级别：
    shell> zabbix_server -c /usr/local/etc/zabbix_server.conf -R log_level_increase=poller,2

    使用 PID 1234 增加进程的日志级别：
    shell> zabbix_server -c /usr/local/etc/zabbix_server.conf -R log_level_increase=1234

    降低所有 http poller 进程的日志级别：
    shell> zabbix_server -c /usr/local/etc/zabbix_server.conf -R log_level_decrease="http poller"

将 HA 故障转移延迟设置为最短 10 秒的示例：

    shell> zabbix_server -R ha_set_failover_delay=10s

[comment]: # ({/6e72a8b1-1cb7f51c})

[comment]: # ({new-b4f10179})
##### 进程用户

Zabbix server 允许使用非 root 用户运行。它将以任何非 root用户的身份运行。因此，使用非 root 用户运行 server 是没有任何问题的.

如果你试图以“root”身份运行它，它将会切换到一个已经“写死”的“zabbix”用户，您可以参考[安装](/manual/installation/install) 章节。按此相应地修改 Zabbix server配置文件中的“AllowRoot”参数，则可以只以“root”身份运行 Zabbix server。

如果 Zabbix server 和 [agent](agent)均运行在同一台服务器上，建议您使用不同的用户运行 server 和 agent。否则,，如果两者都以相同的用户运行，Agent 可以访问 Server 的配置文件,任何 Zabbix 管理员级别的用户都可以很容易地检索到 Server的信息。例如，数据库密码。

[comment]: # ({/new-b4f10179})

[comment]: # ({new-0a81f475})
##### 配置文件

有关配置 Zabbix server 的详细信息，请查阅[配置文件](/manual/appendix/config/zabbix_server) 章节。


[comment]: # ({/new-0a81f475})

[comment]: # ({new-49247ffc})
##### 启动脚本

这些脚本用于在系统启动和关闭期间自动启动和停止 Zabbix 进程。 此脚本位于misc/init.d 目录下。

[comment]: # ({/new-49247ffc})

[comment]: # ({892ef95d-9f05badb})
#### 服务器进程类型

-   `alert manager` - 警报队列管理器
-   `alert syncer` - 警报数据库编写器
-   `alerter` - -   发送通知的进程
-   `availability manager` - 主机可用性更新进程
-   `configuration syncer` -管理配置数据的内存缓存进程
-   `discoverer` - 发现设备进程
-   `escalator` - 升级操作进程
-   `history poller` - 处理需要数据库连接的计算和内部检查进程
-   `history syncer` - 历史数据库编写器
-   `housekeeper` - 删除旧历史数据进程
-   `http poller` - web监控轮询器
-   `icmp pinger` - 用于 icmp ping 检查的轮询器
-   `ipmi manager` - IPMI 轮询管理器
-   `ipmi poller` - IPMI 检查的轮询器
-   `java poller` - 用于Java检查的轮询器
-   `lld manager` - 低级发现任务的管理进程
-   `lld worker` - 低级发现任务的工作进程
-   `odbc poller` - 用于 ODBC 检查的轮询器
-   `poller` - 用于被动检查的普通轮询器
-   `preprocessing manager` - 预处理任务管理器
-   `preprocessing worker` - 数据预处理进程
-   `problem housekeeper` - 消除已删除触发器问题进程
-   `proxy poller` - 被动代理轮询器
-   `report manager`- 计划报告生成任务管理器
-   `report writer` - 生成预定报告编写器
-   `self-monitoring` - 收集内部服务器统计数据的进程
-   `snmp trapper` -  SNMP 陷阱trapper 
-   `task manager` - 远程执行其他组件请求的任务进程（例如关闭问题、确认问题、立即检查项目值、远程命令功能）
-   `timer` - 处理维护的计时器
-   `trapper` - 用于主动检查、陷阱、代理通信的trapper 
-   `unreachable poller` - 不可达设备的轮询器
-   `vmware collector` - VMware 数据收集器，负责从 VMware 服务收集数据

服务器日志文件可用于观察这些进程类型。

可以使用**zabbix\[process,<type>,<mode>,<state>\]** 内部[监控项](/manual/config/items/itemtypes/internal)来监控各种类型的 Zabbix 服务器进程。

[comment]: # ({/892ef95d-9f05badb})

[comment]: # ({new-cdd68340})
#### 支持的平台

由于服务器操作的安全性要求和任务关键性，UNIX是唯一能够始终如一地提供必要性能、容错和弹性的操作系统。Zabbix以市场主流的操作系统版本运行。

经测试，Zabbix 可以运行在下列平台：

-   Linux
-   Solaris
-   AIX
-   HP-UX
-   Mac OS X
-   FreeBSD
-   OpenBSD
-   NetBSD
-   SCO Open Server
-   Tru64/OSF1

::: noteclassic
Zabbix 可以运行在其他类 Unix 操作系统上。
:::


[comment]: # ({/new-cdd68340})


[comment]: # ({new-982e2546})
#### 语言环境

值得注意的是，Zabbix server 需要 UTF-8语言环境，以便可以正确解释某些文本项。 大多数现代类 Unix 系统都默认使用UTF-8 语言环境，但是，有些系统可能需要做特定的设置。

[comment]: # ({/new-982e2546})
