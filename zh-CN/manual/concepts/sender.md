[comment]: # translation:outdated

[comment]: # ({new-43ef458c})
# 6 Sender

[comment]: # ({/new-43ef458c})

[comment]: # ({6903a91d-836aa47c})
#### 概述

Zabbix sender 是一个用来推送性能数据给 Zabbix Server 处理的命令行应用程序。

该应用程序通常用在定期推送可用性和性能数据等在长耗时的用户脚本上。

要将监控数据直接保存在Zabbix Server 或 Zabbix Proxy上，必须将监控配置为[trapper](/manual/config/items/itemtypes/trapper) 。

[comment]: # ({/6903a91d-836aa47c})

[comment]: # ({44457ad4-fce06a66})
#### 运行 Zabbix sender

一个运行 Zabbix UNIX sender 的例子：

    shell> cd bin
    shell> ./zabbix_sender -z zabbix -s "Linux DB3" -k db.connections -o 43

其中：

-   z - Zabbix server host (IP address can be used as well)
-   s - technical name of monitored host (as registered in Zabbix
    frontend)
-   k - item key
-   o - value to send

::: noteimportant
包含空格的选项必须使用双引号引用。
:::

Zabbix sender 可通过从输入文件发送多个值。 详见 [Zabbix sender manpage](/manpages/zabbix_sender) 

如果指定了配置文件，Zabbix sender将使用agent ServerActive配置参数中定义的所有地址发送数据。如果发送到一个地址失败，发件人将尝试发送到其他地址。如果将批数据发送到一个地址失败，则以下批不会发送到此地址。

Zabbix sender 接受 UTF-8 编码的字符串（对于类 UNIX 系统和 Windows ），且在文件中没有字节顺序标记（BOM）。

Zabbix sender 同样可以在 Windows 上运行：

    zabbix_sender.exe [options]

从 Zabbix 1.8.4 开始，zabbix_sender 实时发送方案已得到改进，可以连续接收多个传递给它的值，并通过单个连接将它们发送到 Zabbix Server。 两个不超过0.2秒的值可以放在同一堆栈中，但最大 pooling 时间仍然是1秒。

::: noteclassic
Zabbix sender 如果指定的配置文件中存在无效（不遵循 _parameter=value_ 注释）的参数条目，则 Zabbix sender 将终止。
:::

[comment]: # ({/44457ad4-fce06a66})
