[comment]: # translation:outdated

[comment]: # ({new-8e572e45})
# 9 Web service

[comment]: # ({/new-8e572e45})

[comment]: # ({c810b18f-cac50ff6})
#### 概述

Zabbix web service 是一个用来连接外部网站服务的进程。现在，Zabbix web service 用来收集和发送定时报告，并且计划未来添加更多功能。

Zabbix server通过HTTP(s)连接到web服务。Zabbix web service要求谷歌浏览器安装在同一台主机上；在某些发行版上，该服务也可以与Chromium一起使用（可查看 [known issues](/manual/installation/known_issues#chromium_for_zabbix_web_service_on_ubuntu_20)）。

[comment]: # ({/c810b18f-cac50ff6})

[comment]: # ({53a248b9-704910f2})
#### 安装

Zabbix web service 模块在预编译的Zabbix安装包中提供，可从[Zabbix 网站](http://www.zabbix.com/download.php)下载。通过源码包中编译[Zabbix web service](/manual/installation/install#installing_zabbix_web_service)时，需要指定`--enable-webservice`配置参数。

**另见：**

-   配置文件选项 [zabbix\_web\_service](/manual/appendix/config/zabbix_web_service);
-   [配置定时报表](/manual/appendix/install/web_service)

[comment]: # ({/53a248b9-704910f2})
