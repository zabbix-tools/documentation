[comment]: # translation:outdated

[comment]: # ({new-0f9ecee9})
# 2 Agent

[comment]: # ({/new-0f9ecee9})

[comment]: # ({new-f5143fcc})
#### 概述

Zabbix agent
部署在被监控目标上，以主动监控本地资源和应用程序（硬盘、内存、处理器统计信息等）。

Zabbix agent 收集本地的操作信息并将数据报告给 Zabbix server
用于进一步处理。一旦出现异常
(例如硬盘空间已满或者有崩溃的服务进程)，Zabbix server
会主动警告管理员指定机器上的异常。

Zabbix agents 的极高效率缘于它可以利用本地系统调用来完成统计数据的采集。

[comment]: # ({/new-f5143fcc})

[comment]: # ({new-41e6af7c})
##### 被动和主动检查

Zabbix agent 可以运行被动检查和主动检查。

在[被动检查](/manual/appendix/items/activepassive#passive_checks) 模式中
agent 应答数据请求。Zabbix server（或 proxy）询求数据，例如 CPU
load，然后 Zabbix agent 返还结果。

[主动检查](/manual/appendix/items/activepassive#active_checks)
处理过程将相对复杂。Agent 必须首先从 Zabbix sever
索取监控项列表以进行独立处理，然后会定期发送采集到的新值给 Zabbix
server。

是否执行被动或主动检查是通过选择相应的[监控项类型](/manual/config/items/itemtypes/zabbix_agent)来配置的。
Zabbix agent 处理“Zabbix agent”或“Zabbix agent（active）”类型的监控项。

[comment]: # ({/new-41e6af7c})

[comment]: # ({new-b05a4949})
#### 支持的平台

Zabbix agent 支持以下平台：

-   Linux
-   IBM AIX
-   FreeBSD
-   NetBSD
-   OpenBSD
-   HP-UX
-   Mac OS X
-   Solaris: 9, 10, 11
-   Windows：支持从 Windows XP 之后的桌面版和服务器版。

[comment]: # ({/new-b05a4949})

[comment]: # ({new-5bbb67d9})
#### 类 UNIX 系统上的 Agent

类 UNIX 系统上的 Zabbix agent 运行在被监控的主机上。

[comment]: # ({/new-5bbb67d9})

[comment]: # ({new-32937b35})
##### 安装

有关通过二进制包安装 Zabbix agent 的详细信息，请查阅
[以二进制包安装](/manual/installation/install_from_packages) 章节。

此外，如果您不想使用二进制包，请查阅[以源码包安装](/manual/installation/install#installing_zabbix_daemons)的说明。

::: noteimportant
通常，32位 Zabbix agent 可以在 64
位系统上运行，但在某些情况下可能会失败。
:::

[comment]: # ({/new-32937b35})

[comment]: # ({new-99f37c64})
##### 通过二进制包安装的组件

Zabbix agent 进程以守护进程（Deamon）运行。Zabbix agent
的启动可以通过执行以下命令来完成：

    shell> service zabbix-agent start

上述命令在大多数的 GNU/Linux
系统下都可以正常完成。如果是其他系统，你可能要尝试以下命令来运行：

    shell> /etc/init.d/zabbix-agent start

类似的，停止、重启、查看状态，则需要执行以下命令：

    shell> service zabbix-agent stop
    shell> service zabbix-agent restart
    shell> service zabbix-agent status

[comment]: # ({/new-99f37c64})

[comment]: # ({new-5e83177d})
##### 手动启动

如果以上操作均无效，您可能需要手动启动，找到 Zabbix agent
二进制文件的路径并且执行：

    shell> zabbix_agentd

[comment]: # ({/new-5e83177d})

[comment]: # ({new-f276f35a})
#### Windows 系统上的 Agent

Windows 系统上的 Zabbix agent 作为一个Windows服务运行。

[comment]: # ({/new-f276f35a})

[comment]: # ({new-23b0be18})
##### 准备

Zabbix agent 作为 zip 压缩文件分发。下载该文件后，您需要将其解压缩。
选择任何文件夹来存储Zabbix代理和配置文件，例如：

    C:\zabbix

复制二进制文件 \\bin\\zabbix\_agentd.exe 和配置文件
\\conf\\zabbix\_agentd.conf 到 c:\\zabbix 下。

按需编辑 c:\\zabbix\\zabbix\_agentd.conf 配置文件，确保指定了正确的
"Hostname" 参数。

[comment]: # ({/new-23b0be18})

[comment]: # ({new-c7c6daac})
##### 安装

完成此操作后，使用以下命令将 Zabbix agent 安装为 Windows 服务：

    C:\> c:\zabbix\zabbix_agentd.exe -c c:\zabbix\zabbix_agentd.conf -i

现在您可以像任何其他 Windows 服务一样配置“Zabbix agent”服务。

有关在 Windows 上安装和运行 Zabbix agent
的详细信息，请查阅[于此](/manual/appendix/install/windows_agent#installing_agent_as_windows_service)。

[comment]: # ({/new-c7c6daac})

[comment]: # ({new-fa025a89})
#### 其他 Agent 选项

您可以在主机上运行单个或多个 Agent 实例。
单个实例可以使用默认配置文件或命令行中指定的配置文件。
如果是多个实例，则每个 Agent
程序实例必须具有自己的配置文件（其中一个实例可以使用默认配置文件）。

以下命令参数可以在 Zabbix agent 中使用：

|**参数**                              *|描述**|
|-----------------------------------------|--------|
|**UNIX 和 Windows agent**|<|
|-c --config <config-file>|配置文件的绝对路径。<br>您可以使用此选项来制定配置文件，而不是使用默认文件。<br>在 UNIX 上，默认的配置文件是 /usr/local/etc/zabbix\_agentd.conf 或 由 [compile-time](/manual/installation/install#installing_zabbix_daemons) 中的 *--sysconfdir* 或 *--prefix* 变量来确定。<br>在 Windows上, 默认的配置文件是 c:\\zabbix\_agentd.conf|
|-p --print|输出已知的监控项并退出。<br>*注意*： 要返回 [用户自定义参数](/manual/config/items/userparameters) 的结果，您必须指定配置文件（如果它不在默认路径下）。|
|-t --test <item key>|测试指定的监控项并退出。<br>*注意*：要返回 [用户自定义参数](/manual/config/items/userparameters) 的结果，您必须指定配置文件（如果它不在默认路径下）。|
|-h --help|显示帮助信息|
|-V --version|显示版本号|
|**仅 UNIX agent**|<|
|-R --runtime-control <option>|执行管理功能。请参阅 [运行时机制的控制](/manual/concepts/agent#runtime_control)。|
|\*\*仅 Windows agent \*\*|<|
|-m --multiple-agents|使用多 Agent 实例（使用 -i、-d、-s、-x）。<br>为了区分实例的服务名称，每项服务名都会包涵来自配置文件里的 Hostname 值。|
|**仅 Windows agent （功能）**|<|
|-i --install|以服务的形式安装 Zabbix Windows agent。|
|-d --uninstall|卸载 Zabbix indows agent 服务。|
|-s --start|启动 Zabbix Windows agent 服务。|
|-x --stop|停止 Zabbix Windows agent 服务。|

使用命令行参数的具体**示例**：

-   打印输出所有内置监控项和它们的值。
-   使用指定的配置文件中的“mysql.ping”键值来测试用户自定义参数。
-   在 Windows 下使用默认路径下的配置文件 c:\\zabbix\_agentd.conf 安装
    Zabbix agent 服务。
-   使用位于与 agent 可执行文件同一文件夹中的配置文件
    zabbix\_agentd.conf 为 Windows 安装“Zabbix Agent
    \[Hostname\]”服务，并通过从配置文件中的唯一 Hostname 值扩来为命名。

```{=html}
<!-- -->
```
    shell> zabbix_agentd --print
    shell> zabbix_agentd -t "mysql.ping" -c /etc/zabbix/zabbix_agentd.conf
    shell> zabbix_agentd.exe -i
    shell> zabbix_agentd.exe -i -m -c zabbix_agentd.conf

[comment]: # ({/new-fa025a89})

[comment]: # ({new-6ad4cc3c})
##### 运行时控制

使用运行时控制选项，您可以更改代理进程的日志级别。

|选项                                      描|目标|<|
|-----------------------------------------------|------|-|
|log\_level\_increase\[=<target>\]|增加日志级别。\                    目标可以被指如果未指定目标，将影响所有进程。   `pid` - 进程标识符 (|为：<br>to 65535)<br>`process type` - 指定进程的所有类型 (例如，poller)<br>`process type,N` - 进程类型和编号 (例如，poller,3)|
|log\_level\_decrease\[=<target>\]|降低日志级别。\                    :::如果未指定目标，将影响所有进程。|<|

值得注意的是，用于更改单个 Agent 进程的日志级别的 PIDs
的可用范围是1到65535。在具有大 PIDs 的系统上，<process type,N>
目标可用于更改单个进程的日志级别。

例子：

-   给所有进程增加日志级别。
-   给第二个监听进程增加日志级别。
-   给 PID 号为 1234 的进程增加日志级别。
-   给所有主动检查进程降低日志级别。

```{=html}
<!-- -->
```
    shell> zabbix_agentd -R log_level_increase
    shell> zabbix_agentd -R log_level_increase=listener,2
    shell> zabbix_agentd -R log_level_increase=1234
    shell> zabbix_agentd -R log_level_decrease="active checks"

::: noteclassic
运行时控制不支持 OpenBSD 和 NetBSD 和 Windows
系统。
:::

[comment]: # ({/new-6ad4cc3c})

[comment]: # ({new-929667fd})
#### 进程用户

Zabbix agent 在 UNIX 上允许使用非 root 用户运行。它将以任何非 root
用户的身份运行。因此，使用非 root 用户运行 agent 是没有任何问题的.

如果你试图以“root”身份运行它，它将会切换到一个已经“写死”的“zabbix”用户，该用户必须存在于您的系统上。如果您只想以“root”用户运行
agent，您必须在 agent 配置文件里修改‘AllowRoot‘参数。

[comment]: # ({/new-929667fd})

[comment]: # ({new-18ab16f1})
#### 配置文件

有关配置 Zabbix agent 的详细信息，请查阅
[zabbix\_agentd](/manual/appendix/config/zabbix_agentd) 或 [Windows
agent](/manual/appendix/config/zabbix_agentd_win) 章节。

[comment]: # ({/new-18ab16f1})

[comment]: # ({new-fc6b2c61})
#### 语言环境

值得注意的是，Zabbix agent 需要 UTF-8 语言环境，以便某些文本 Zabbix
agent 监控项可以返回预期的内容。 大多数现代类 Unix 系统都默认使用 UTF-8
语言环境，但是，有些系统可能需要特定的设置。

[comment]: # ({/new-fc6b2c61})

[comment]: # ({new-7c3bd34c})
#### 退出码

在 2.2 版之前，Zabbix agent 在成功退出时返回0，在异常时返回255。 从版本
2.2 及更高版本开始，Zabbix agent 在成功退出时返回0，在异常时返回1。

[comment]: # ({/new-7c3bd34c})


[comment]: # ({6a401555-5581c546})

#### 退出码

在2.2版本之前，Zabbix代理在成功退出时返回0，在失败时返回255。从版本2.2和更高版本开始，Zabbix代理在成功退出时返回0，在失败时返回1。

[comment]: # ({/6a401555-5581c546})
