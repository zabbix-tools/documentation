[comment]: # translation:outdated

[comment]: # ({new-d60be363})
# 3 Agent 2

[comment]: # ({/new-d60be363})

[comment]: # ({9cf65518-734dc3ed})

#### 概述

Zabbix agent 2是新一代的Zabbix agent 甚至可以替代Zabbix agent. Zabbix agent 2 已经进步到:

-   减少TCP连接数量
-   提供改进的检查并发性
-   使用插件很容易扩展。一个插件应该能够:
    -   提供由几行简单代码组成的简单检查
    -   提供复杂的检查，包括长时间运行的脚本和独立的数据收集，并定期发回数据
-   做一个临时的替代品 Zabbix agent (因为它支持之前的所有功能)

Agent 2 是用Go 语言编写(复用了一些  Zabbix agent 中的C 语言代码). 需要配置当前支持的Go环境  [Go version](https://golang.org/doc/devel/release#policy)  在构建Zabbix agent 2 时。

Agent 2 Linux上没有内置的守护进程支持; 它可以作为  [Windows service](https://www.zabbix.com/documentation/devel/en/manual/appendix/install/windows_agent).

被动检查的工作类似于Zabbix agent。主动检查支持scheduled/flexible intervals 同时并发检查仅使用一个active server.

**检查并发**

来自不同插件的检查可以同时执行。一个插件的并发检查次数受插件容量设置的限制。每个插件都可以有一个硬编码的容量设置(默认为100)，可以使用 `Plugins.<Plugin name>.Capacity=N`  设置  _Plugins_  配置  [parameter](https://www.zabbix.com/documentation/devel/en/manual/concepts/agent2#configuration-file).

参考:  [Plugin development guidelines](https://www.zabbix.com/documentation/guidelines/plugins).

[comment]: # ({/9cf65518-734dc3ed})

[comment]: # ({new-eaa0f2bb})
##### Passive and active checks

Passive checks work similarly to Zabbix agent. Active checks support
scheduled/flexible intervals and check concurrency within one active
server. 

::: noteclassic
  By default, after a restart, Zabbix agent 2 will schedule the first data collection for active checks at a conditionally random time within the item's update interval to prevent spikes in resource usage. To perform active checks that do not have *Scheduling* [update interval](/manual/config/items/item/custom_intervals#scheduling-intervals) immediately after the agent restart, set `ForceActiveChecksOnStart` parameter (global-level) or `Plugins.<Plugin name>.System.ForceActiveChecksOnStart` (affects only specific plugin checks) in the [configuration file](/manual/appendix/config/zabbix_agent2). Plugin-level parameter, if set, will override the global parameter. Forcing active checks on start is supported since Zabbix 6.0.2. 
:::

[comment]: # ({/new-eaa0f2bb})

[comment]: # ({new-05ea1336})
##### Check concurrency

Checks from different plugins can be executed concurrently. The number
of concurrent checks within one plugin is limited by the plugin capacity
setting. Each plugin may have a hardcoded capacity setting (100 being
default) that can be lowered using the `Plugins.<PluginName>.System.Capacity=N` setting in the *Plugins*
configuration [parameter](#configuration_file). Former name of this parameter `Plugins.<PluginName>.Capacity` is still supported, but has been deprecated in Zabbix 6.0.

[comment]: # ({/new-05ea1336})

[comment]: # ({0e8e0939-a2064519})

####支持的平台

Agent 2 支持运行平台 Linux 和 Windows。

如果源码包安装， Agent 2 支持平台有:

-   RHEL/CentOS 6, 7, 8
-   SLES 15 SP1+
-   Debian 9, 10
-   Ubuntu 18.04, 20.04

在Windows上 Agent 2 支持的有:

-   Windows Server 2008 R2 and later
-   Windows 7 and later

[comment]: # ({/0e8e0939-a2064519})

[comment]: # ({e9eb9957-cabc0f5f})

#### 安装

Zabbix agent 2 可以在预编译的Zabbix包中获得。
要从源代码编译Zabbix agent 2，你必须指定 `--enable-agent2`配置选项。

[comment]: # ({/e9eb9957-cabc0f5f})

[comment]: # ({eaea6cc0-45c02f12})

#### 操作

可以使用以下命令行参数 Zabbix agent 2:

|**参数**|**参数描述**|
|-------------|---------------|
|-c --config <config-file>|配置文件的路径。<br>您可以使用此选项指定非默认配置文件的配置文件。<br>在 UNIX, 默认是 /usr/local/etc/zabbix\_agent2.conf 或者编译是设置 [compile-time](/manual/installation/install#installing_zabbix_daemons) 变量*--sysconfdir* or *--prefix*|
|-f --foreground|在前台运行Zabbix代理 (默认: true).|
|-p --print|打印已知项并退出。<br>*注意*: 也返回 [user parameter](/manual/config/items/userparameters)结果 , 必须指定配置文件(如果它不在默认位置)。|
|-t --test <item key>|测试指定项目并退出。<br>*注意*: 也返回 [user parameter](/manual/config/items/userparameters) 结果, 必须指定配置文件 (如果它不在默认位置)。|
|-h --help| 打印帮助信息并退出。|
|-v --verbose|打印调试信息。将此选项与-p和-t选项一起使用。|
|-V --version|打印代理版本号并退出。|
|-R --runtime-control <option>|执行管理功能。 查看 [runtime control](/manual/concepts/agent2#runtime_control).|

特殊 **例子** 使用命令行参数:

-  打印所有带有值的内置代理项
-  使用指定配置文件中定义的“mysql.ping”键测试一个用户参数 

```{=html}
<!-- -->
```
    shell> zabbix_agent2 --print
    shell> zabbix_agent2 -t "mysql.ping" -c /etc/zabbix/zabbix_agentd.conf

[comment]: # ({/eaea6cc0-45c02f12})

[comment]: # ({fb667c2d-11aa0de0})

##### 实时控制

实时控制提供了一些远程控制选项。

|操作|描述|
|------|-----------|
|log\_level\_increase|增加日志等级。|
|log\_level\_decrease|降低日志等级。|
|metrics|可用的指标列表。|
|version|显示agent版本。|
|userparameter\_reload|从当前配置文件重新加载用户参数。<br>请注意UserParameter是将被重新加载的唯一代理配置选项。|
|help|显示实时控制的帮助信息|

例子:

-   为agent 2增加日志等级
-   打印实时控制选项

```{=html}
<!-- -->
```
    shell> zabbix_agent2 -R log_level_increase
    shell> zabbix_agent2 -R help

[comment]: # ({/fb667c2d-11aa0de0})

[comment]: # ({64102bdb-77e9b590})

#### 配置文件

agent 2的配置参数大多与Zabbix agent 兼容，但也有一些例外。

|新的参数|描述|
|--------------|-----------|
|*ControlSocket*|实时控制套接字路径。 Agent 2使用控制套接字 [runtime commands](#runtime_control).|
|*EnablePersistentBuffer*,<br>*PersistentBufferFile*,<br>*PersistentBufferPeriod*|这些参数用于在agent 2上为活动项配置持久存储.|
|*Plugins*|插件可能有自己的参数, 以 `Plugins.<Plugin name>.<Parameter>=<value>`格式。 常见的插件参数是 *Capacity*, 设置可以同时执行的检查的限制。|
|*StatusPort*|端口agent 2将侦听HTTP状态请求，并显示已配置的插件列表和一些内部参数|
|丢弃参数|描述|
|*AllowRoot*, *User*|不支持，因为守护进程不支持。|
|*LoadModule*, *LoadModulePath*|不支持可加载模块。|
|*StartAgents*|此参数在Zabbix代理中用于增加被动检查并发性或禁用它们。 在Agent 2,并发是在插件级别配置的，可以通过容量设置来限制。 然而，当前不支持禁用被动检查。|
|*HostInterface*, *HostInterfaceItem*|不支持。|

有关详细信息，请参阅配置文件选项
[zabbix\_agent2](/manual/appendix/config/zabbix_agent2).

[comment]: # ({/64102bdb-77e9b590})

[comment]: # ({1850570d-472820ac})
#### 退出码

从版本4.4.8开始，Zabbix agent 2也可以用
旧的OpenSSL版本(1.0.1,1.0.2)。

在这种情况下，Zabbix提供了在OpenSSL中锁定的互斥体。如果互斥锁锁定或解锁失败，则错误消息被打印到标准错误流(STDERR)，Agent2退出，返回代码分别为2或3。

[comment]: # ({/1850570d-472820ac})
