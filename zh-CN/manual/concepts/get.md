[comment]: # translation:outdated

[comment]: # ({new-14379c17})
# 6 Get

[comment]: # ({/new-14379c17})

[comment]: # ({new-f50cbd4e})
#### 概述

Zabbix get 是一个命令行应用，它可以用于与 Zabbix agent 进行通信，并从
Zabbix agent 那里获取所需的信息。

该应用通常被用于 Zabbix agent 故障排错。

[comment]: # ({/new-f50cbd4e})

[comment]: # ({new-c625ea25})
#### 运行 Zabbix get

一个在 UNIX 下运行 Zabbix get 以从 Zabbix agent 获取 processor load
的值的例子。

    shell> cd bin
    shell> ./zabbix_get -s 127.0.0.1 -p 10050 -k system.cpu.load[all,avg1]

另一个运行 Zabbix get 以从网站捕获一个字符串的例子：

    shell> cd bin
    shell> ./zabbix_get -s 192.168.1.1 -p 10050 -k "web.page.regexp[www.zabbix.com,,,\"USA: ([a-zA-Z0-9.-]+)\",,\1]"

请注意，此处的监控项键值包含空格，因此引号用于将监控项键值标记为 shell。
引号不是监控项键值的一部分；它们将被 shell 修剪，不会被传递给Zabbix
agent。

Zabbix get 接受以下命令行参数：

      -s --host <host name or IP>      指定目标主机名或IP地址
      -p --port <port number>          指定主机上运行 Zabbix agent 的端口号。默认端口10050
      -I --source-address <IP address> 指定源 IP 地址
      -k --key <item key>              指定要从监控项键值检索的值
      -h --help                        获得帮助
      -V --version                     显示版本号

详见 [Zabbix get 手册](/manpages/zabbix_get)。

Zabbix get 同样可以在 Windows 上运行：

    zabbix_get.exe [options]

[comment]: # ({/new-c625ea25})
