[comment]: # translation:outdated

[comment]: # ({2afb009a-fb8f0d5a})
# 1 高可用集群

[comment]: # ({/2afb009a-fb8f0d5a})

[comment]: # ({909eb03f-ea8bc3db})
#### 概述

高可用性模式为Zabbix server提供了防止软件/硬件故障的保护，并允许最小化软件/硬件维护期间的停机时间。

Zabbix server 支持高可用性(HA)集群是一种可选择的解决方案。 本机HA解决方案被设计成使用简单，它可以跨站点工作，并且对Zabbix识别的数据库没有特定的要求。 用户可以自由使用本地Zabbix HA解决方案或第三方HA解决方案，这取决于他们的环境中哪种方案最适合高可用性需求。

解决方案由多个zabbix\_server实例或节点组成。
每一个节点:

-   单独配置(配置文件、脚本、加密、数据导出)
-   使用相同的数据库
-   有几种模式: active, standby, unavailable, stopped

同一个时间只能有一个节点处于活动状态(工作状态)。 备用节点不进行数据收集、处理或其他常规的服务器活动; 他们不监听港口; 它们具有最小的数据库连接。

主备节点每5秒更新一次访问时间。 备节点监控主节点的最后一次访问时间。 如果主节点的最后一次访问时间超过“故障切换延迟”秒，则备节点将自己切换为主节点，将“unavailable”状态分配给先前的活动节点。。

活动节点监视自己的数据库连接——如果丢失的时间超过`failover delay-5`秒，它必须停止所有处理并切换到standby模式。主节点同时监控备节点的状态 - 如果备节点最后一次访问时间超过“failover delay”秒, 备节点被分配为“unavailable”状态。

故障转移延迟可配置，最小为10秒。

这些节点被设计成在较小的Zabbix版本之间兼容。

[comment]: # ({/909eb03f-ea8bc3db})

[comment]: # ({772fb398-567d3671})
#### 启用高可用集群

[comment]: # ({/772fb398-567d3671})

[comment]: # ({3b12e384-23e81771})

##### 服务配置

要将任何Zabbix server从独立服务器转换为HA集群节点，Zabbix server 中配置指定HANodeName参数
[configuration](/manual/appendix/config/zabbix_server).

节点地址参数 (地址:端口), 一旦从Zabbix server前端设置active模式,  zabbix.conf.php里面配置自动被写入。

[comment]: # ({/3b12e384-23e81771})

[comment]: # ({new-3e9035e5})
##### Preparing frontend

Make sure that Zabbix server address:port is **not defined** in the 
frontend configuration.

Zabbix frontend will autodetect the active node by reading settings 
from the nodes table in Zabbix database. Node address of the active node 
will be used as the Zabbix server address.

[comment]: # ({/new-3e9035e5})

[comment]: # ({a7345b94-d47b6ef3})

##### Proxy 配置

要在高可用性设置中允许连接多个服务器，请在服务器中列出HA节点的地址
[parameter](/manual/appendix/config/zabbix_proxy) proxy 参数,
用分号分隔。

[comment]: # ({/a7345b94-d47b6ef3})

[comment]: # ({85a6a782-f2fc0e77})
##### Agent 配置

要在高可用性设置中启用多个服务器的连接，请列出中HA节点ServerActive的地址 
[parameter](/manual/appendix/config/zabbix_agentd) agent参数配置,
用逗号分隔.

[comment]: # ({/85a6a782-f2fc0e77})

[comment]: # ({new-311341fc})
### Failover to standby node

Zabbix will fail over to another node automatically if the active node stops. There 
must be at least one node in standby status for the failover to happen.

How fast will the failover be? All nodes update their last access time (and status, if 
it is changed) every 5 seconds. So: 

-   If the active node shuts down and manages to report its status 
as "shut down", another node will take over within **5 seconds**.

-   If the active node shuts down/becomes unavailable without being able to update 
its status, standby nodes will wait for the **failover delay** + 5 seconds to take over

The failover delay is configurable, with the supported range between 10 seconds and 15 
minutes (one minute by default). To change the failover delay, you may run:

```
zabbix_server -R ha_set_failover_delay=5m
```

[comment]: # ({/new-311341fc})

[comment]: # ({49127d42-593144b8})

#### HA集群管理

HA集群的当前状态可以通过专用的
[runtime control](/manual/concepts/server#runtime_control) 选项进行管理:

-   ha\_status - HA 集群状态在Zabbix server日志中;
-   ha\_remove\_node=target - -   移除HA节点 
    <target> - -   列表中的节点编号(该数字可从运行输出中获取 ha\_status). 注意
    active/standby -  节点不能被移除.
-   ha\_set\_failover\_delay=delay - 设置 HA 失败延迟时间 （灵活的时间支持, e.g. 10s, 1m)

节点状态可以被监控:

-   in *Reports* → *[System
    information](/manual/web_interface/frontend_sections/reports/status_of_zabbix#status_of_ha_cluster_nodes)*
-   在*System information* 面板组件
-   使用 `ha_status` -   服务的运行时控制选项 (见上文叙述).

此 `zabbix[cluster,discovery,nodes]` internal item可以用于节点发现，因为它返回一个JSON，包含高可用性节点信息。

[comment]: # ({/49127d42-593144b8})

[comment]: # ({d945471f-82cd7e56})

#### 禁用HA集群

禁用HA高可用集群:

-   备份配置文件
-   停止standby节点
-   移除HANodeName 参数从活跃的主服务上
-   重启主服务 (它将以独立模式启动)

[comment]: # ({/d945471f-82cd7e56})

[comment]: # ({new-7d944a36})
### Upgrading HA cluster

To perform a major version upgrade for the HA nodes:

-   stop all nodes;
-   create a full database backup;
-   if the database uses replication make sure that all nodes are in sync and have no issues. Do not upgrade if replication is broken.
-   select a single node that will perform database upgrade, change its configuration to standalone mode by commenting out HANodeName and [upgrade](/manual/installation/upgrade) it;
-   make sure that database upgrade is fully completed (*System information* should display that Zabbix server is running);
-   restart the node in HA mode;
-   upgrade and start the rest of nodes (it is not required to change them to standalone mode as the database is already upgraded at this point).

In a minor version upgrade it is sufficient to upgrade the first node, make sure it has upgraded and running, and then start upgrade on the next node.

[comment]: # ({/new-7d944a36})

[comment]: # ({new-f4d3143a})

### Implementation details

The high availability (HA) cluster is an opt-in solution and it is
supported for Zabbix server. The native HA solution is designed to be
simple in use, it will work across sites and does not have specific
requirements for the databases that Zabbix recognizes. Users are free to
use the native Zabbix HA solution, or a third party HA solution,
depending on what best suits the high availability requirements in their
environment.

The solution consists of multiple zabbix\_server instances or nodes.
Every node:

-   is configured separately
-   uses the same database
-   may have several modes: active, standby, unavailable, stopped

Only one node can be active (working) at a time. A standby node runs only one 
process - the HA manager. A standby node does no data collection, 
processing or other regular server activities; they do not listen 
on ports; they have minimum database connections.

Both active and standby nodes update their last access time every 5
seconds. Each standby node monitors the last access time of the active
node. If the last access time of the active node is over 'failover
delay' seconds, the standby node switches itself to be the active node
and assigns 'unavailable' status to the previously active node.

The active node monitors its own database connectivity - if it is lost
for more than `failover delay-5` seconds, it must stop all processing
and switch to standby mode. The active node also monitors the status of
the standby nodes - if the last access time of a standby node is over
'failover delay' seconds, the standby node is assigned the 'unavailable'
status.

The failover delay is configurable, with the minimum being 10 seconds.

The nodes are designed to be compatible across minor Zabbix versions.

[comment]: # ({/new-f4d3143a})
