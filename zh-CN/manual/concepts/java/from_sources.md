[comment]: # translation:outdated

[comment]: # ({548d996b-5380e993})
# 1 使用源码安装

[comment]: # ({/548d996b-5380e993})

[comment]: # ({180aad79-8d0f220e})
#### 概述

如果你是使用源码[安装]((/manual/installation/install#installing_java_gateway))，那么下列信息会帮助你配置Zabbix [Java网关](/manual/concepts/java)。

[comment]: # ({/180aad79-8d0f220e})

[comment]: # ({fe0de672-305c7876})
#### 文件概述

如果你从源码中获得Java网关，那么您最终会得到 $PREFIX/sbin/zabbix_java 下的Shell脚本、JAR和配置文件的集合。这些文件的作用总结如下。

    bin/zabbix-java-gateway-$VERSION.jar

Java 网关自身JAR 文件。

    lib/logback-core-0.9.27.jar
    lib/logback-classic-0.9.27.jar
    lib/slf4j-api-1.6.1.jar
    lib/android-json-4.3_r3.1.jar

Java 网关依赖于: [Logback](http://logback.qos.ch/),
[SLF4J](http://www.slf4j.org/), 和[Android
JSON](https://android.googlesource.com/platform/libcore/+/master/json)
库。

    lib/logback.xml  
    lib/logback-console.xml

Logback的配置文件。

    shutdown.sh  
    startup.sh

用于启动和停止Java网关的便捷脚本。

    settings.sh

用于控制上述启动和停止脚本的配置文件。

[comment]: # ({/fe0de672-305c7876})

[comment]: # ({8766a70a-571dbbe2})
#### 配置和运行Java网关

Java网关默认监听10052端口。如果你想运行Java网关在其他端口，你可以在settings.sh的脚本中指定其他端口号。详情可见[Java网关配置文件](/manual/appendix/config/zabbix_java)中描述的如何更改端口等其他信息。

::: notewarning
10050端口不是由 [IANA 注册的](http://www.iana.org/assignments/service-names-port-numbers/service-names-port-numbers.txt).
:::

当配置好，你可以使用启动脚本来运行Java网关:

    $ ./startup.sh

同样，如果你不再使用Java网关了，可以运行关闭脚本将其停止:

    $ ./shutdown.sh

请注意，Java网关是轻量应用，不需要数据库，这一点与Server和Proxy不同。

[comment]: # ({/8766a70a-571dbbe2})

[comment]: # ({082dfb54-81ca4902})
#### 配置Zabbix Server 关联Java网关

当Java网关启动并运行后，你需要告诉Zabbix server去哪里找Zabbix Java网关。通过在[server 配置文件](/manual/appendix/config/zabbix_server)中指定JavaGateway和JavaGatewayPort来完成这个操作。如果运行JMX应用程序的主机是由Zabbix代理监控的，则可以在[proxy 配置文件](/manual/appendix/config/zabbix_server)中指定连接参数。

    JavaGateway=192.168.3.14
    JavaGatewayPort=10052

默认情况下，server不会启动任何与JMX监控相关的进程。如果你希望用到它，则必须指定Java pollers的数量。此操作与配置常规 pollers 和 trappers数量一样。

    StartJavaPollers=5

配置完server或proxy后，一定不要忘记重启server或proxy。

[comment]: # ({/082dfb54-81ca4902})

[comment]: # ({6b4aedca-d86274ab})
#### 调试Java网关

为了防止在 Java gateway 出现任何问题或在 Zabbix 前端看不到详细的报错信息的情况下，你可以通过 Java gateway 日志文件来查看。

默认情况下，Java gateway 将其活动日志记录到日志级别为 "info" 的 /tmp/zabbix_java.log 文件中。有时候，该日志信息可能不够详细，需要在日志级别为 "debug" 中获取。为了提升日志级别，需要修改 lib/logback.xml 文件，并将 <root> 标记的日志等级属性更改为 "debug"：

    <root level="debug">
      <appender-ref ref="FILE" />
    </root>

值得注意的是，与 Zabbix server 或 Zabbix proxy 不同，更改 logback.xml 文件并不需要重启 Zabbix Java gateway，它会自动提交。当完成调试后，可以将日志级别还原成“info”。

如果希望将日志记录到其他文件或完全不同的介质，如数据库，那么只需要调整 logback.xml 文件。详见[Logback 手册](http://logback.qos.ch/manual/) 获取更多信息。

有时为了调试，将 Java 网关用作控制台应用比以守护进程来启动更方便。因此可以在 settings.sh 中注释掉 PID_FILE 变量，这时运行startup.sh 脚本启动 Java gateway 就会作为控制台应用来启动，并将 Logback 使用 lib/logback-console.xml 文件，这不仅会记录到控制台，还会启用日志级别 “debug”。

最后，请注意，由于 Java gateway 使用 SLF4J 来记录，您可以通过在 lib 目录放置合适的 JAR 文件来将 Logback 替换为您选中的框架。详见  [SLF4J 手册](http://www.slf4j.org/manual.html)  以获取更多信息。

[comment]: # ({/6b4aedca-d86274ab})

[comment]: # ({5fe324cc-4332cfb8})
#### JMX 监控

详见  [JMX 监控](/manual/config/items/itemtypes/jmx_monitoring)  页面以获取更多信息。

[comment]: # ({/5fe324cc-4332cfb8})
