[comment]: # translation:outdated

[comment]: # ({c353fd29-e983a9df})
# 3 从 Debian/Ubuntu 包安装

[comment]: # ({/c353fd29-e983a9df})

[comment]: # ({3f711cb8-a044dd01})
#### 概述

如果你是通过 Debian/Ubuntu 的包[安装](/manual/installation/install_from_packages/debian_ubuntu#java_gateway_installation)，那么下列信息会帮助你配置Zabbix [Java网关](/manual/concepts/java)。

[comment]: # ({/3f711cb8-a044dd01})

[comment]: # ({8630ce17-01191552})
#### 配置和运行 Java 网关

Java 网关的配置参数可以通过如下文件进行调整：

    /etc/zabbix/zabbix_java_gateway.conf

关于更多信息，详见 Zabbix Java geteway 配置
[参数](/manual/appendix/config/zabbix_java).

通过以下命令启动 Zabbix Java 网关：

    # service zabbix-java-gateway restart

通过以下命令配置 Zabbix Java 网关开机自启动：

    # systemctl enable zabbix-java-gateway

[comment]: # ({/8630ce17-01191552})

[comment]: # ({9295353e-81ca4902})
#### 配置 Zabbix Server 关联 Java 网关

当Java网关启动并运行后，你需要告诉Zabbix server去哪里找Zabbix Java网关。通过在[server 配置文件](/manual/appendix/config/zabbix_server)中指定JavaGateway和JavaGatewayPort来完成这个操作。如果运行JMX应用程序的主机是由Zabbix代理监控的，则可以在[proxy 配置文件](/manual/appendix/config/zabbix_server)中指定连接参数。

    JavaGateway=192.168.3.14
    JavaGatewayPort=10052

默认情况下，server不会启动任何与JMX监控相关的进程。如果你希望用到它，则必须指定Java pollers的数量。此操作与配置常规 pollers 和 trappers数量一样。

    StartJavaPollers=5

配置完server或proxy后，一定不要忘记重启server或proxy。

[comment]: # ({/9295353e-81ca4902})

[comment]: # ({218e371b-05f991d0})
#### 调试 Java 网关

Zabbix Java 网关的日志路径：

    /var/log/zabbix/zabbix_java_gateway.log

如果要增加日志记录，编辑以下文件：

    /etc/zabbix/zabbix_java_gateway_logback.xml

并将`level="info"`更改为"debug"或"trace"（深度排错模式）

    <configuration scan="true" scanPeriod="15 seconds">
    [...]
          <root level="info">
                  <appender-ref ref="FILE" />
          </root>

    </configuration>

[comment]: # ({/218e371b-05f991d0})

[comment]: # ({5fe324cc-4332cfb8})
#### JMX 监控

详见  [JMX 监控](/manual/config/items/itemtypes/jmx_monitoring)  页面以获取更多信息。

[comment]: # ({/5fe324cc-4332cfb8})
