[comment]: # translation:outdated

[comment]: # ({new-e8144915})
# 8 JS

[comment]: # ({/new-e8144915})

[comment]: # ({8e0a28ea-b0613168})
#### 概述

zabbix\_js 是一个命令行实用程序，可用于嵌入脚本测试。

该程序可执行带有字符串参数的用户自定义脚本并打印结果。脚本的执行是由内嵌的Zabbix脚本引擎来完成的。

在编译或执行错误的情况下，zabbix\_js将在stderr中打印错误并以代码1退出。

[comment]: # ({/8e0a28ea-b0613168})

[comment]: # ({4476bdcb-de3fe561})
#### 用法

    zabbix_js -s script-file -p input-param [-l log-level] [-t timeout]
    zabbix_js -s script-file -i input-file [-l log-level] [-t timeout]
    zabbix_js -h
    zabbix_js -V

zabbix\_js 可接收如下命令行参数：

      -s, --script script-file          指定待执行脚本的文件名。若 '-' 作为文件名时，脚本名由stdin输入。
      -i, --input input-file            指定输入参数的文件名。若 '-' 作为文件名时，脚本名由stdin输入。
      -p, --param input-param           指定输入参数。
      -l, --loglevel log-level          指定日志级别。
      -t, --timeout timeout             指定超时时间（单位：秒）。
      -h, --help                        显示帮助信息。
      -V, --version                     显示版本号。

例如：

    zabbix_js -s script-file.js -p example

[comment]: # ({/4476bdcb-de3fe561})
