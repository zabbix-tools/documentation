[comment]: # translation:outdated

[comment]: # ({new-00e36b2a})
# 4 Java gateway

[comment]: # ({/new-00e36b2a})

[comment]: # ({new-02637e76})
#### 概述

从 Zabbix 2.0 开始，以 Zabbix 守护进程方式原生支持监控 JMX
应用程序就存在了，称之为“Zabbix Java gateway”。Zabbix Java gateway
的守护进程是用 Java 编写。为了在特定主机上找到 JMX 计数器的值，Zabbix
server 向 Zabbix Java gateway 发送请求，后者使用 [JMX 管理
API](http://java.sun.com/javase/technologies/core/mntr-mgmt/javamanagement/)
来远程查询相关的应用。该应用不需要安装额外的软件。只需要在启动时，命令行添加`-Dcom.sun.management.jmxremote`选项即可。

Java gateway 接受来自 Zabbix server 或 Zabbix proxy
的传入连接，并且只能用作“被动 proxy”。与 Zabbix proxy 相反，它也可以从
Zabbix proxy （Zabbix proxy 不能被链接）调用。在 Zabbix server 或 Zabbix
proxy 配置文件中，可以直接配置每个 Java gateway 的访问，因此每个 Zabbix
pserver 或 Zabbix proxy 只能配置一个 Java gateway。如果主机将有 **JMX
agent** 或其他类型的监控项，则只将 **JMX agent** 监控项传递给 Java
gateway 进行检索。

当必须通过 Java gateway 更新监控项时，Zabbix server 或 proxy 将连接到
Java gateway 并请求该值，Java gateway 将检索该值并将其传递回 Zabbix
server 或 Zabbix proxy。 因此，Java gateway 不会缓存任何值。

Zabbix server 或 Zabbix proxy 具有连接到 Java gateway
的特定类型的进程，由 **StartJavaPollers** 选项控制。在内部，Java gateway
启动多个线程，由 **START\_POLLERS** 选项控制。 在服务器端，如果连接超过
**Timeout** 选项配置的秒数，它将被终止，但 Java gateway 可能仍在忙于从
JMX 计数器检索值。 为了解决这个问题，从 Zabbix 2.0.15、Zabbix 2.2.10 和
Zabbix 2.4.5 开始，Java gateway 中有 **TIMEOUT** 选项，允许为 JMX
网络操作设置超时。

Zabbix server 或 proxy 尝试尽可能地将请求汇集到单个 JMX
目标（受监控项取值间隔影响），并在单个连接中将它们发送到 Java Gateway
以获得更好的性能。

此外，建议让 **StartJavaPollers** 选项的值小于或等于
**START\_POLLERS**，否则可能会出现 Java gateway
中没有可用线程来为传入请求提供服务的情况。

以下部分描述了如何获取和运行Zabbix Java gateway，如何配置 Zabbix
server（或 Zabbix proxy）来使用 Zabbix Java gateway 进行 JMX
监控，以及如何在 Zabbix GUI 中配置与特定 JMX 计数器对应的 Zabbix
监控项。

[comment]: # ({/new-02637e76})

[comment]: # ({new-05197937})
When an item has to be updated over Java gateway, Zabbix server or proxy
will connect to the Java gateway and request the value, which Java
gateway in turn retrieves and passes back to the server or proxy. As
such, Java gateway does not cache any values.

Zabbix server or proxy has a specific type of processes that connect to
Java gateway, controlled by the option **StartJavaPollers**. Internally,
Java gateway starts multiple threads, controlled by the
**START\_POLLERS** [option](/manual/appendix/config/zabbix_java). On the
server side, if a connection takes more than **Timeout** seconds, it
will be terminated, but Java gateway might still be busy retrieving
value from the JMX counter. To solve this, there is the **TIMEOUT**
option in Java gateway that allows to set timeout for JMX network
operations.

[comment]: # ({/new-05197937})

[comment]: # ({new-475ef799})

Zabbix server or proxy will try to pool requests to a single JMX target
together as much as possible (affected by item intervals) and send them
to the Java gateway in a single connection for better performance.

It is suggested to have **StartJavaPollers** less than or equal to
**START\_POLLERS**, otherwise there might be situations when no threads
are available in the Java gateway to service incoming requests; in such
a case Java gateway uses ThreadPoolExecutor.CallerRunsPolicy, meaning
that the main thread will service the incoming request and temporarily[label](https://git.zabbix.com/projects/WEB/repos/documentation/compare)
will not accept any new requests.

If you are trying to monitor Wildfly-based Java applications with Zabbix Java gateway, please install the latest jboss-client.jar available on the [Wildfly download page](https://www.wildfly.org/downloads/).

[comment]: # ({/new-475ef799})

[comment]: # ({new-d8d54db7})
#### - 获取 Java gateway

获取 Java gateway 有两种方法。 一种是从 Zabbix 网站下载 Java gateway
二进制包，另一种是从源代码编译 Java gateway。

##### - 从 Zabbix 网站下载

Zabbix Java gateway 二进制包 （RHEL, Debian, Ubuntu）可以从
<http://www.zabbix.com/download.php> 下载。

##### - 从源码包中编译

为了编译 Java gateway，首先使用 `--enable-java` 选项运行 `./configure`
脚本。建议您使用 `--prefix` 选项来指定其他路径，而非默认的 /usr/local
路径，因为安装 Java gateway 将创建整个目录树，并非单个可执行文件。

    $ ./configure --enable-java --prefix=$PREFIX

要将 Java gateway 编译并打包到 JAR 文件中，请运行 `make`。
值得注意的是，对于此步骤，会使用 `javac` 和 `jar`
可执行文件，因此需要确保它们处于正确的路径下。

    $ make

现在您在 src/zabbix\_java/bin 路径下 有 zabbix-java-gateway-$VERSION.jar
文件。如果您熟悉在 src/zabbix\_java 分发目录下运行 Java
gateway，那么您可以继续执行配置和运行 Java gateway
的指令。否则，请确保您有拥有足够的权限来运行 `make install`。

    $ make install

#### - Java gateway 分发中的文件概述

无论您如何获得的 Java gateway，在 $PREFIX/sbin/zabbix\_java
路径下您都会获得一系列的 shell 脚本、JAR
和配置文件。这些文件的作用的概述如下。

    bin/zabbix-java-gateway-$VERSION.jar

Java gateway JAR 文件。

    lib/logback-core-0.9.27.jar
    lib/logback-classic-0.9.27.jar
    lib/slf4j-api-1.6.1.jar
    lib/android-json-4.3_r3.1.jar

Java gateway
依赖于：[Logback](http://logback.qos.ch/)、[SLF4J](http://www.slf4j.org/)
和 [Android
JSON](https://android.googlesource.com/platform/libcore/+/master/json)
库。

    lib/logback.xml  
    lib/logback-console.xml

用于 Logback 的配置文件：

    shutdown.sh  
    startup.sh

启动和停止 Java gateway 的便捷脚本：

    settings.sh

由上面启动和停止脚本提供的配置文件。

#### - 配置和运行 Java gateway

默认情况下，Java gateway 监听 10052 端口。如果您计划使用不同的端口来运行
Java gateway，则可以通过 setting.sh
脚本中指定端口。有关如何指定此选项和其他选项，详见 [Java gateway
配置文件](/manual/appendix/config/zabbix_java)。

::: notewarning
值得注意的是，端口 10052 并没有在 [IANA
注册](http://www.iana.org/assignments/service-names-port-numbers/service-names-port-numbers.txt)。
:::

待熟悉设置后，您可以通过运行 startup 脚本来启动 Java gateway：

    $ ./startup.sh

同样的，一旦您不需要 Java gateway，运行 shutdown 脚本即可关闭它。

    $ ./shutdown.sh

请注意，与 Zabbix server 或 Zabbix proxy 不同，Java gateway
是轻量级的，并不需要数据库。

#### - 配置 server 以使用 Java gateway

现在 Java gateway 正在运行，您必须告诉 Zabbix server 从哪里找到 Zabbix
Java gateway。因此需要在 [Zabbix server
配置文件](/manual/appendix/config/zabbix_server) 中指定 JavaGateway 和
JavaGatewayPort 参数。如果 Zabbix proxy 监控运行着 JMX
应用程序的主机，则在 [Zabbix proxy
配置文件](/manual/appendix/config/zabbix_proxy) 中指定连接参数。

    JavaGateway=192.168.3.14
    JavaGatewayPort=10052

默认情况下，Zabibx server 不会启动与 JMX
监控相关的任何进程。但是，如果要使用它，则必须指定 Java pollers 的
pre-forked 实例数。 同样的，您也可以指定常规的 pollers 和 trappers。

    StartJavaPollers=5

值得注意的是，在完成配置后，请不要忘记重新启动 Zabbix server 或 Zabbix
proxy。

#### - Java gateway 的调试

如果 Java gateway 出现任何问题或者您看到 Zabbix
前端中的监控项错误消息不充分时，您可以查看 Java gateway 的日志文件。

默认情况下，Java gateway 的日志会记录到 /tmp/zabbix\_java.log
文件中，日志级别为“info”。有时候这些信息是不够的，需要将日志级别修改为“dubug”。为了提高日志记录级别，需要修改lib/logback.xml
文件并将 <root> 标记的 level 属性更改为“debug”：

    <root level="debug">
      <appender-ref ref="FILE" />
    </root>

值得注意的是，与 Zabbix server 或 Zabbix proxy 不同，更改 logback.xml
文件后无需重新启动 Zabbix Java gateway，将自动完成 logback.xml
中的更改。 完成调试后，可以将日志记录级别修改回“info”。

如果您希望记录到其他文件或完全不同的介质（如数据库），请调整 logback.xml
文件以满足您的需要。详见 [Logback 手册](http://logback.qos.ch/manual/)。

有时为了方便调试，将 Java gateway
作为控制台应用程序而不是守护程序启动是更有用的。 为此，请在 settings.sh
中注释掉 PID\_FILE 变量。 如果省略 PID\_FILE ，则 startup.sh 脚本将 Java
gateway 作为控制台应用程序启动，并让 Logback 使用
lib/logback-console.xml
文件，这不仅会记录到控制台，还会启用日志记录级别“debug”。

最后，值得注意的，由于 Java gateway 使用 SLF4J
进行日志记录，因此可以适当地将 JAR 包放置在 lib 目录中， 来将 Logback
替换为您所选的框架。详见 [SLF4J
手册](http://www.slf4j.org/manual.html)。

[comment]: # ({/new-d8d54db7})
