[comment]: # translation:outdated

[comment]: # ({new-840eea1a})
# 7 Distribution-specific notes on setting up Nginx for Zabbix

[comment]: # ({/new-840eea1a})

[comment]: # ({new-4b966990})
#### RHEL

Nginx is available only in EPEL:

    # yum -y install epel-release

[comment]: # ({/new-4b966990})

[comment]: # ({new-fb2c0b94})
#### SLES 12

In SUSE Linux Enterprise Server 12 you need to add the Nginx repository,
before installing Nginx:

    zypper addrepo -G -t yum -c 'http://nginx.org/packages/sles/12' nginx

You also need to configure `php-fpm`:

    cp /etc/php5/fpm/php-fpm.conf{.default,}
    sed -i 's/user = nobody/user = wwwrun/; s/group = nobody/group = www/' /etc/php5/fpm/php-fpm.conf

[comment]: # ({/new-fb2c0b94})

[comment]: # ({new-31bd54eb})
#### SLES 15

In SUSE Linux Enterprise Server 15 you need to configure `php-fpm`:

    cp /etc/php7/fpm/php-fpm.conf{.default,}
    cp /etc/php7/fpm/php-fpm.d/www.conf{.default,}
    sed -i 's/user = nobody/user = wwwrun/; s/group = nobody/group = www/' /etc/php7/fpm/php-fpm.d/www.conf

[comment]: # ({/new-31bd54eb})
