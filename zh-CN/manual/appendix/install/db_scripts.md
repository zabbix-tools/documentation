[comment]: # translation:outdated

[comment]: # ({32ea9f34-7af1bbaa})
# 1 数据库的创建

[comment]: # ({/32ea9f34-7af1bbaa})

[comment]: # ({28dd6a87-fab90562})
#### 概述

在部署 Zabbix server 或 proxy 时必须要创建数据库。

本节提供创建 Zabbix 数据库的说明。 每个受支持的数据库都有对应的创建说明。

Zabbix 唯一支持的编码是 UTF-8 。它的运行没有任何安全漏洞。应注意如果使用其他的编码，则存在已知的安全问题。

[comment]: # ({/28dd6a87-fab90562})

[comment]: # ({717dcd08-fad527fc})
::: noteclassic
如果从 [Zabbix Git 存储库](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse) 安装 Zabbix，在进行下一步操作之前需要执行以下命令：
`$ make dbschema` 
:::

[comment]: # ({/717dcd08-fad527fc})

[comment]: # ({3554afdd-1e36e539})
#### MySQL

Zabbix Server/Proxy 完美适配 MySQL 数据库的重要前提是 MySQL 数据库的字符集必须使用 utf8（又名 utf8mb3）或 utf8mb4（分别使用 utf8_bin 和 utf8mb4_bin 排序规则）。建议使用 utf8mb4 进行新版本的安装。

 · shell> mysql -uroot -p<password>
 · mysql> create database zabbix character set utf8mb4 collate utf8mb4_bin;
 · mysql> create user 'zabbix'@'localhost' identified by '<password>';
 · mysql> grant all privileges on zabbix.* to 'zabbix'@'localhost';
 · mysql> quit;

::: notewarning
如果你是从 Zabbix **packages** 进行安装， 在这里暂停，并继续按照 [RHEL/CentOS](https://www.zabbix.com/download?zabbix=5.0&os_distribution=red_hat_enterprise_linux&os_version=8&db=mysql) 或 [Debian/Ubuntu](https://www.zabbix.com/download?zabbix=5.0&os_distribution=debian&os_version=10_buster&db=mysql) 的说明将数据导入数据库。
:::

如果你是从源码包安装 Zabbix，请继续按照以下步骤将数据导入数据库。对于 Zabbix Proxy 数据库，只需要导入 `schema.sql` 即可 （不需要 images.sql 和 data.sql）：

 · shell> cd database/mysql
 · shell> mysql -uzabbix -p<password> zabbix < schema.sql
 · # 如果是创建 Zabbix proxy 的数据库，以下两条命令便不需要再执行。
 · shell> mysql -uzabbix -p<password> zabbix < images.sql
 · shell> mysql -uzabbix -p<password> zabbix < data.sql

[comment]: # ({/3554afdd-1e36e539})

[comment]: # ({a6ba851e-61d6043c})
#### PostgreSQL

需要使用有创建数据库权限的用户进行操作。以下 shell 命令将创建 `zabbix` 用户。
按照提示输入密码并再次确认（注意，你可能首先被要求输入 `sudo` 密码）：

 · shell> sudo -u postgres createuser --pwprompt zabbix

接下来我们将使用先前创建的用户作为数据库所有者（`-O zabbix`）来设置 `zabbix` 数据库（最后一个参数）。

 · shell> sudo -u postgres createdb -O zabbix -E Unicode -T template0 zabbix

::: notewarning
如果您是从 Zabbix **packages** 进行安装，在这里暂停并继续按照 [RHEL/CentOS](https://www.zabbix.com/download?zabbix=5.0&os_distribution=red_hat_enterprise_linux&os_version=8&db=postgresql) 或 [Debian/Ubuntu](https://www.zabbix.com/download?zabbix=5.0&os_distribution=debian&os_version=10_buster&db=postgresql) 的说明将初始的架构和数据导入数据库。
:::

如果你是从 Zabbix 源码包进行安装，请继续按照以下步骤导入初始模式和数据（假设您位于 Zabbix 源码包的根目录中）。对于 Zabbix Proxy 的数据库，只需要导入 `schema.sql` 即可（不需要 images.sql 和 data.sql）。

 · shell> cd database/postgresql
 · shell> cat schema.sql | sudo -u zabbix psql zabbix
 · # 如果是创建 Zabbix proxy 的数据库，以下两条命令便不需要再执行。
 · shell> cat images.sql | sudo -u zabbix psql zabbix
 · shell> cat data.sql | sudo -u zabbix psql zabbix

::: noteimportant
上述命令仅作为参考示例提供，适用于大多数 GNU/Linux 的安装流程。你也可以使用不同的命令，例如 "psql -U <username>" ，具体取决于你的系统/数据库的配置方式。 如果你在设置数据库时遇到问题，请咨询数据库管理员。
:::

[comment]: # ({/a6ba851e-61d6043c})

[comment]: # ({e483d9c2-cc68ca58})
#### TimescaleDB

创建和配置 TimescaleDB 的操作在单独的  [章节](/manual/appendix/install/timescaledb) 中说明。

[comment]: # ({/e483d9c2-cc68ca58})

[comment]: # ({136c7e03-7b4d56a7})
#### Oracle

创建和配置 Oracle 数据库的操作在单独的 [章节](/manual/appendix/install/oracle) 中说明。

[comment]: # ({/136c7e03-7b4d56a7})

[comment]: # ({248a7245-02d49e4f})
#### SQLite

SQLite 数据库只能在  **Zabbix proxy** 中使用！

::: noteclassic
如果使用 SQLite 作为 Zabbix proxy 的数据库，创建时如果数据库不存在，将自动创建。
:::

 · shell> cd database/sqlite3
 · shell> sqlite3 /var/lib/sqlite/zabbix.db < schema.sql

返回 [安装部分](/manual/installation/install)。

[comment]: # ({/248a7245-02d49e4f})
