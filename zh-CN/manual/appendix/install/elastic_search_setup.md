[comment]: # translation:outdated

[comment]: # ({new-4ff8e217})
# 3 Elasticsearch 配置

[comment]: # ({/new-4ff8e217})

[comment]: # ({new-0b6587bd})
### 3 Elasticsearch setup

::: noteimportant

Elasticsearch的支持是实验性的！本节中考虑的配置步骤适用于以下Elasticsearch版本：\*\*
5.0.x - > 6.1.x \*\*。
如果使用早期或更高版本的Elasticsearch，某些功能可能无法按预期工作。
:::

::: noteimportant
Elasticsearch support is experimental!\
Setup procedure considered in this section is applicable to the
following Elasticsearch versions: **5.0.x -→ 6.1.x**. In case an earlier
or later version of Elasticsearch is used, some functionality may not
work as intended.
:::

Zabbix最近开始支持通过使用Elasticsearch而不是数据库来存储历史数据。
现在，用户可以在兼容数据库和Elasticsearch之间选择历史数据的存储位置。

Zabbix has recently started to support storage of historical data by
means of Elasticsearch instead of a database. Users are now given the
possibility to choose the storage place for historical data between a
compatible database and Elasticsearch.

[comment]: # ({/new-0b6587bd})

[comment]: # ({new-293ef64c})
#### 配置

[comment]: # ({/new-293ef64c})

[comment]: # ({new-e9a9c5bd})
#### Configuration

正确的设置Zabbix
server配置文件和前端配置文件中的参数，以保证所有元素之间的正确通信。 To
ensure proper communication between all elements involved make sure
server configuration file and frontend configuration file parameters are
properly configured.

[comment]: # ({/new-e9a9c5bd})

[comment]: # ({new-8451a0a2})
##### Zabbix server 和前端

[comment]: # ({/new-8451a0a2})

[comment]: # ({new-5c4a5e3c})
##### Zabbix server and frontend

已经更新参数的Zabbix server配置文件示例如下： Zabbix server
configuration file draft with parameters to be updated:

    ### Option: HistoryStorageURL
    # History storage HTTP[S] URL.
    #
    # Mandatory: no
    # Default:
    # HistoryStorageURL= 
    ### Option: HistoryStorageTypes
    # Comma separated list of value types to be sent to the history storage.
    #
    # Mandatory: no
    # Default:
    # HistoryStorageTypes=uint,dbl,str,log,text

用于设置Zabbix server配置文件的示例参数值如下： Example parameter values
to fill the Zabbix server configuration file with:

    HistoryStorageURL=http://test.elasticsearch.lan:9200
    HistoryStorageTypes=str,log,text

此配置文件使Zabbix
Server将相应数据库中的数值类型和文本类型的历史数据存储到Elasticsearch中。
This configuration forces Zabbix Server to store history values of
numeric types in the corresponding database and textual history data in
Elasticsearch.

Elasticsearch支持以下几种监控项类型： Elasticsearch supports the
following item types:

    uint,dbl,str,log,text

支持的监控项类型说明如下： Supported item type explanation:

|   |   |   |
|---|---|---|
|**Item value type**|**Database table**|**Elasticsearch type**|
|Numeric (unsigned)|history\_uint|uint|
|Numeric (float)|history|dbl|
|Character|history\_str|str|
|Log|history\_log|log|
|Text|history\_text|text|

已经更新参数的Zabbix前端配置文件(`conf/zabbix.conf.php`) 示例如下：
Zabbix frontend configuration file (`conf/zabbix.conf.php`) draft with
parameters to be updated:

    // Elasticsearch url (can be string if same url is used for all types).
    $HISTORY['url']   = [
          'uint' => 'http://localhost:9200',
          'text' => 'http://localhost:9200'
    ];
    // Value types stored in Elasticsearch.
    $HISTORY['types'] = ['uint', 'text'];

用于设置Zabbix前端配置文件的示例参数值如下： Example parameter values to
fill the Zabbix frontend configuration file with:

    $HISTORY['url']   = 'http://test.elasticsearch.lan:9200';
    $HISTORY['types'] = ['str', 'text', 'log'];

此配置文件将文本、字符、日志类型的历史数据存储到Elasticsearch中。 This
configuration forces to store `Text`, `Character` and `Log` history
values in Elasticsearch.

还需要在`conf / zabbix.conf.php`文件中配置$HISTORY为全局参数，以确保一切正常工作（请参阅`conf / zabbix.conf.php.example`以了解如何配置）：

It is also required to make $HISTORY global in `conf/zabbix.conf.php` to
ensure everything is working properly (see
`conf/zabbix.conf.php.example` for how to do it):

    // Zabbix GUI configuration file.
    global $DB, $HISTORY;

##### Elasticsearch配置和创建映射

##### Installing Elasticsearch and creating mapping

正常配置包括安装Elasticsearch和创建映射两个步骤。 Final two steps of
making things work are installing Elasticsearch itself and creating
mapping process.

安装Elasticsearch请参考[Elasticsearch安装指南](https://www.elastic.co/guide/en/elasticsearch/reference/current/setup.html)

To install Elasticsearch please refer to [Elasticsearch installation
guide](https://www.elastic.co/guide/en/elasticsearch/reference/current/setup.html).

::: noteclassic
映射是Elasticsearch中的数据结构（类似于数据库中的表）。
此处提供了所有历史数据类型的映射：`database / elasticsearch / elasticsearch.map`。
:::

::: noteclassic
Mapping is a data structure in Elasticsearch (similar to a
table in a database). Mapping for all history data types is available
here: `database/elasticsearch/elasticsearch.map`.
:::

::: notewarning
必须创建映射。
如果未按照要求创建映射，则某些功能将无法正常使用。
:::

::: notewarning
Creating mapping is mandatory. Some functionality
will be broken if mapping is not created according to the
instruction.
:::

创建`text`类型的映射可以发送如下请求到Elasticsearch： To create mapping
for `text` type send the following request to Elasticsearch:

``` {.java}
curl -X PUT \
 http://your-elasticsearch.here:9200/text \
 -H 'content-type:application/json' \
 -d '{
   "settings" : {
      "index" : {
         "number_of_replicas" : 1,
         "number_of_shards" : 5
      }
   },
   "mappings" : {
      "values" : {
         "properties" : {
            "itemid" : {
               "type" : "long"
            },
            "clock" : {
               "format" : "epoch_second",
               "type" : "date"
            },
            "value" : {
               "fields" : {
                  "analyzed" : {
                     "index" : true,
                     "type" : "text",
                     "analyzer" : "standard"
                  }
               },
               "index" : false,
               "type" : "text"
            }
         }
      }
   }
}'
```

对于创建“字符”和“日志”类型的历史数据映射，需要执行类似的请求，请求内容需要进行相应的修改。
Similar request is required to be executed for `Character` and `Log`
history values mapping creation with corresponding type correction.

::: noteclassic
要使用Elasticsearch，请参阅[Requirement
page](manual：installation：requirements＃server)以获取更多信息。
:::

::: noteclassic
To work with Elasticsearch please refer to [Requirement
page](/manual/installation/requirements#server) for additional
information.
:::

::: noteclassic
[Housekeeper](/manual/installation/requirements?s[]=housekeeper)
不会删除任何Elasticsearch中的数据
:::

::: noteclassic
[Housekeeper](/manual/installation/requirements?s[]=housekeeper)
is not deleting any data from Elasticsearch.
:::

#### 历史数据存储于多个基于时间的索引

#### Storing history data in multiple date-based indices

本节介绍使用pipeline和ingest节点所需的其他配置步骤。 This section
describes additional steps required to work with pipelines and ingest
nodes.

首先必须为索引创建一个模板。创建uint模板的请求示例如下： To begin with,
you must create templates for indices. The following example shows a
request for creating uint template:

``` {.java}
curl -X PUT \
 http://your-elasticsearch.here:9200/_template/uint_template \
 -H 'content-type:application/json' \
 -d '{
   "template": "uint*",
   "index_patterns": ["uint*"],
   "settings" : {
      "index" : {
         "number_of_replicas" : 1,
         "number_of_shards" : 5
      }
   },
   "mappings" : {
      "values" : {
         "properties" : {
            "itemid" : {
               "type" : "long"
            },
            "clock" : {
               "format" : "epoch_second",
               "type" : "date"
            },
            "value" : {
               "type" : "long"
            }
         }
      }
   }
}'
```

要创建其他模板，用户应更改请求URL（最后一部分是模板名称），更改“template”和“index\_patterns”字段以匹配索引名称并设置可从“database
/ elasticsearch / elasticsearch”中获取的有效映射
。例如，下面的命令能为一个文本索引创建一个模板：

To create other templates, user should change the URL (last part is the
name of template), change "template" and "index\_patterns" fields to
match index name and to set valid mapping that can be taken from
`database/elasticsearch/elasticsearch.map`. For example, the following
command can be used to create a template for text index:

``` {.java}
curl -X PUT \
 http://your-elasticsearch.here:9200/_template/text_template \
 -H 'content-type:application/json' \
 -d '{
   "template": "text*",
   "index_patterns": ["text*"],
   "settings" : {
      "index" : {
         "number_of_replicas" : 1,
         "number_of_shards" : 5
      }
   },
   "mappings" : {
      "values" : {
         "properties" : {
            "itemid" : {
               "type" : "long"
            },
            "clock" : {
               "format" : "epoch_second",
               "type" : "date"
            },
            "value" : {
               "fields" : {
                  "analyzed" : {
                     "index" : true,
                     "type" : "text",
                     "analyzer" : "standard"
                  }
               },
               "index" : false,
               "type" : "text"
            }
         }
      }
   }
}'
```

这是允许Elasticsearch为自动创建的索引设置有效的映射所必需做的。
然后需要创建pipeline定义。
在将数据放入索引之前，pipeline能对数据进行多种预处理操作。
以下命令可用于为uint索引创建pipeline：

This is required to allow Elasticsearch to set valid mapping for indices
created automatically. Then it is required to create the pipeline
definition. Pipeline is some sort of preprocessing of data before
putting data in indices. The following command can be used to create
pipeline for uint index:

``` {.java}
curl -X PUT \
 http://your-elasticsearch.here:9200/_ingest/pipeline/uint-pipeline \
 -H 'content-type:application/json' \
 -d '{
  "description": "daily uint index naming",
  "processors": [
    {
      "date_index_name": {
        "field": "clock",
        "date_formats": ["UNIX"],
        "index_name_prefix": "uint-",
        "date_rounding": "d"
      }
    }
  ]
}'
```

用户可以修改参数（“date\_rounding”）来设置特定的索引循环周期。
要创建其他pipeline，用户应更改请求URL（最后一部分是pipeline名称）并更改“index\_name\_prefix”字段以匹配索引名称。

User can change the rounding parameter ("date\_rounding") to set a
specific index rotation period. To create other pipelines, user should
change the URL (last part is the name of pipeline) and change
"index\_name\_prefix" field to match index name.

可以参考
[Elasticsearch文档](https://www.elastic.co/guide/en/elasticsearch/reference/master/date-index-name-processor.html).

See also [Elasticsearch
documentation](https://www.elastic.co/guide/en/elasticsearch/reference/master/date-index-name-processor.html).

另外，可以通过Zabbix
server配置文件中新加的参数来配置将历史数据存储于基于时间的多个索引。
Additionally, storing history data in multiple date-based indices should
also be enabled in the new parameter in Zabbix server configuration:

    ### Option: HistoryStorageDateIndex
    # Enable preprocessing of history values in history storage to store values in different indices based on date.
    # 0 - disable
    # 1 - enable
    #
    # Mandatory: no
    # Default:
    # HistoryStorageDateIndex=0

#### 故障诊断

#### Troubleshooting

以下步骤可帮助您解决Elasticsearch的配置问题： The following steps may
help you troubleshoot problems with Elasticsearch setup:

1.  检查映射是否正确(通过URL的发送GET请求获取索引信息，例如：`http://localhost:9200/uint`)。
2.  检查shards状态是否正常 (不正常时重启Elasticsearch可能解决问题)。
3.  检查Elasticsearch配置文件，配置文件应允许从Zabbix前端主机和Zabbix
    server主机进行访问。
4.  检查Elasticsearch日志。

```{=html}
<!-- -->
```
1.  Check if the mapping is correct (GET request to required index URL
    like `http://localhost:9200/uint`).
2.  Check if shards are not in failed state (restart of Elasticsearch
    should help).
3.  Check the configuration of Elasticsearch. Configuration should allow
    access from the Zabbix frontend host and the Zabbix server host.
4.  Check Elasticsearch logs.

如果您仍然遇到配置问题，请创建一个错误报告，其中包含映射，错误日志，配置，版本等信息。
If you are still experiencing problems with your installation then
please create a bug report with all the information from this list
(mapping, error logs, configuration, version, etc.)

[comment]: # ({/new-5c4a5e3c})
