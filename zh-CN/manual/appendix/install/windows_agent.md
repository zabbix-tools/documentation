[comment]: # translation:outdated

[comment]: # ({new-9884b1d2})
# 2 Windows 下的Zabbix agent

[comment]: # ({/new-9884b1d2})

[comment]: # ({new-bc82b50c})
### 2 Zabbix agent on Microsoft Windows

[comment]: # ({/new-bc82b50c})

[comment]: # ({new-011f29f0})
#### agent配置

[comment]: # ({/new-011f29f0})

[comment]: # ({new-1c178c2a})
#### Configuring agent

Zabbix agent 作为Windows服务运行。 Zabbix agent runs as a Windows
service.

在一台Windows主机上可以运行一个或多个Zabbix agent实例。
如果安装一个实例可以使用默认的配置文件`C:\zabbix_agentd.conf`
或者在命令中指定配置文件路径。
如果安装多个实例，每一个agent必须有自己的配置文件
(其中一个实例可以使用默认的配置文件)。

You can run a single instance of Zabbix agent or multiple instances of
the agent on a Microsoft Windows host. A single instance can use the
default configuration file `C:\zabbix_agentd.conf` or a configuration
file specified in the command line. In case of multiple instances each
agent instance must have its own configuration file (one of the
instances can use the default configuration file).

在Zabbix源文件目录有一个配置文件样例`conf/zabbix_agentd.win.conf`。

An example configuration file is available in Zabbix source archive as
`conf/zabbix_agentd.win.conf`.

关于Zabbix Windows agent 更多详细信息，参考
[配置文件](/manual/appendix/config/zabbix_agentd_win) 。

See the [configuration file](/manual/appendix/config/zabbix_agentd_win)
options for details on configuring Zabbix Windows agent.

[comment]: # ({/new-1c178c2a})

[comment]: # ({new-635edc59})
##### 主机名参数

[comment]: # ({/new-635edc59})

[comment]: # ({new-f3f0d3db})
##### Hostname parameter

主机执行 [active
checks](/manual/appendix/items/activepassive#active_checks) 时，Zabbix
agent 需要定义主机名。而且,agent端的主机名必须和前端配置的主机名 "[Host
name](/manual/config/hosts/host)"完全一致 。

To perform [active
checks](/manual/appendix/items/activepassive#active_checks) on a host
Zabbix agent needs to have the hostname defined. Moreover, the hostname
value set on the agent side should exactly match the "[Host
name](/manual/config/hosts/host)" configured for the host in the
frontend.

agent端的主机名可以通过配置文件[configuration
file](/manual/appendix/config/zabbix_agentd_win)中的**Hostname** 或
**HostnameItem**参数定义 - 如果不指定参数值将使用默认的主机名字。

The hostname value on the agent side can be defined by either the
**Hostname** or **HostnameItem** parameter in the agent [configuration
file](/manual/appendix/config/zabbix_agentd_win) - or the default values
are used if any of these parameters are not specified.

参数**HostnameItem** 的默认值即agent端key值为
"system.hostname"的监控项返回值，对于Windows平台返回的是NetBIOS的主机名。

The default value for **HostnameItem** parameter is the value returned
by the "system.hostname" agent key and for Windows platform it returns
the NetBIOS host name.

参数**Hostname**默认值为**HostnameItem**
参数的返回值。所以，实际上，如果这两个参数都是未指定的，实际的主机名将是主机NetBIOS名称;
Zabbix agent将使用NetBIOS主机名从Zabbix server获取active
checks列表，并将检查结果发送给它。

The default value for **Hostname** is the value returned by the
**HostnameItem** parameter. So, in effect, if both these parameters are
unspecified the actual hostname will be the host NetBIOS name; Zabbix
agent will use NetBIOS host name to retrieve the list of active checks
from Zabbix server and send results to it.

<note
important>**system.hostname**key始终返回限制为15个字符的NetBIOS主机名，并且全为大写字符
- 而不管实际主机名中的长度和字符大小写。
:::

::: noteimportant
The **system.hostname** key always returns the
NetBIOS host name which is limited to 15 symbols and in UPPERCASE only -
regardless of the length and lowercase/uppercase characters in the real
host name.
:::

从Windows Zabbix agent 1.8.6版本开始， "system.hostname" key支持可选参数
-名称的*类型*。此参数的默认值为"netbios" (用于向后兼容) 另一个可能的值是
"host".

Starting from Zabbix agent 1.8.6 version for Windows the
"system.hostname" key supports an optional parameter - *type* of the
name. The default value of this parameter is "netbios" (for backward
compatibility) and the other possible value is "host".

<note
important>**system.hostname\[host\]**键总是返回完整的，实际的（区分大小写的）Windows主机名。
:::

::: noteimportant
The **system.hostname\[host\]** key always returns
the full, real (case sensitive) Windows host name.
:::

因此，为了简化zabbix\_agentd.conf文件的配置并使其统一起来，可以使用两种不同的方法。

1.  不定义 **Hostname**或者**HostnameItem** 参数，Zabbix
    agent将使用NetBIOS主机名作为主机名;
2.  不定义 **Hostname** 参数，定义**HostnameItem** 如:\
    **HostnameItem=system.hostname\[host\]**\
    Zabbix
    agent将使用完整的，实际的（区分大小写的）Windows主机名作为主机名。

So, to simplify the configuration of zabbix\_agentd.conf file and make
it unified, two different approaches could be used.

1.  leave **Hostname** or **HostnameItem** parameters undefined and
    Zabbix agent will use NetBIOS host name as the hostname;
2.  leave **Hostname** parameter undefined and define **HostnameItem**
    like this:\
    **HostnameItem=system.hostname\[host\]**\
    and Zabbix agent will use the full, real (case sensitive) Windows
    host name as the hostname.

主机名也用作Windows服务名称的一部分，用于安装，启动，停止和卸载Windows服务。
例如，如果Zabbix agent配置文件指定`Hostname=Windows_db_server`,
那么agent将作为Windows服务"`Zabbix Agent [Windows_db_server]`"安装。因此，如果要每个Zabbix
agent实例拥有不同的Windows服务名称，则每个实例都必须使用不同的主机名。

Host name is also used as part of Windows service name which is used for
installing, starting, stopping and uninstalling the Windows service. For
example, if Zabbix agent configuration file specifies
`Hostname=Windows_db_server`, then the agent will be installed as a
Windows service "`Zabbix Agent [Windows_db_server]`". Therefore, to have
a different Windows service name for each Zabbix agent instance, each
instance must use a different host name.

[comment]: # ({/new-f3f0d3db})

[comment]: # ({new-db2e6708})
#### 将代理安装为Windows服务

#### Installing agent as Windows service

使用默认配置文件`c:\zabbix_agentd.conf`安装Zabbix agent的单个实例:

To install a single instance of Zabbix agent with the default
configuration file `c:\zabbix_agentd.conf`:

    zabbix_agentd.exe --install

<note
important>在64位系统上，运行64位进程相关的所有检查都正常工作需要64位的Zabbix
agent版本。
:::

::: noteimportant
On a 64-bit system, a 64-bit Zabbix agent version
is required for all checks related to running 64-bit processes to work
correctly.
:::

如果希望使用除`c:\zabbix_agentd.conf`以外的配置文件,
应该使用以下命令进行服务安装：

If you wish to use a configuration file other than
`c:\zabbix_agentd.conf`, you should use the following command for
service installation:

    zabbix_agentd.exe --config <your_configuration_file> --install

应指定配置文件的全路径。 A full path to the configuration file should be
specified.

Zabbix agent多实例作为服务安装的命令如下: Multiple instances of Zabbix
agent can be installed as services like this:

      zabbix_agentd.exe --config <configuration_file_for_instance_1> --install --multiple-agents
      zabbix_agentd.exe --config <configuration_file_for_instance_2> --install --multiple-agents
      ...
      zabbix_agentd.exe --config <configuration_file_for_instance_N> --install --multiple-agents

现在在控制面板中可以看到安装的服务。 The installed service should now be
visible in Control Panel.

#### 启动 agent

#### Starting agent

启动agent服务，可以使用控制面板或通过命令行方式。 To start the agent
service, you can use Control Panel or do it from command line.

启动使用默认配置文件的单实例Zabbix agent命令如下： To start a single
instance of Zabbix agent with the default configuration file:

     zabbix_agentd.exe --start

启动使用自定义配置文件的单实例Zabbix agent命令如下： To start a single
instance of Zabbix agent with another configuration file:

     zabbix_agentd.exe --config <your_configuration_file> --start

启动多实例Zabbix agent中的一个实例命令如下: To start one of multiple
instances of Zabbix agent:

     zabbix_agentd.exe --config <configuration_file_for_this_instance> --start --multiple-agents

#### 停止 agent

#### Stopping agent

停止agent服务，可以使用控制面板或通过命令行方式。 To stop the agent
service, you can use Control Panel or do it from command line.

停止使用默认配置文件的单实例Zabbix agent命令如下： To stop a single
instance of Zabbix agent started with the default configuration file:

     zabbix_agentd.exe --stop

停止使用自定义配置文件的单实例Zabbix agent命令如下： To stop a single
instance of Zabbix agent started with another configuration file:

     zabbix_agentd.exe --config <your_configuration_file> --stop

停止多实例Zabbix agent中的一个实例命令如下: To stop one of multiple
instances of Zabbix agent:

     zabbix_agentd.exe --config <configuration_file_for_this_instance> --stop --multiple-agents

#### 卸载 agent Windows 服务

#### Uninstalling agent Windows service

卸载使用默认配置文件的单实例Zabbix agent服务命令如下： To uninstall a
single instance of Zabbix agent using the default configuration file:

       zabbix_agentd.exe --uninstall

卸载使用自定义配置文件的单实例Zabbix agent服务命令如下： To uninstall a
single instance of Zabbix agent using a non-default configuration file:

       zabbix_agentd.exe --config <your_configuration_file> --uninstall

卸载多实例Zabbix agent服务命令如下: To uninstall multiple instances of
Zabbix agent from Windows services:

      zabbix_agentd.exe --config <configuration_file_for_instance_1> --uninstall --multiple-agents
      zabbix_agentd.exe --config <configuration_file_for_instance_2> --uninstall --multiple-agents
      ...
      zabbix_agentd.exe --config <configuration_file_for_instance_N> --uninstall --multiple-agents

[comment]: # ({/new-db2e6708})

[comment]: # ({new-b7b226d1})

#### Limitations

Zabbix agent for Windows does not support
non-standard Windows configurations where CPUs are distributed
non-uniformly across NUMA nodes. If logical CPUs are distributed
non-uniformly, then CPU performance metrics may not be available for
some CPUs. For example, if there are 72 logical CPUs with 2 NUMA nodes,
both nodes must have 36 CPUs each.

[comment]: # ({/new-b7b226d1})
