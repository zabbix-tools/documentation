[comment]: # translation:outdated

[comment]: # ({new-6f5dcc8c})
# 6 大文件支持

大型文件支持，通常缩写为LFS, 这个术语适用于在32位操作系统上处理大于2
GB的文件的能力。 自从Zabbix 2.0对大文件的支持已经被添加。该变动会影响
[log file monitoring](/manual/config/items/itemtypes/log_items)
和所有[vfs.file.\*
items](/manual/config/items/itemtypes/zabbix_agent#supported_item_keys).
大文件支持依赖于Zabbix编译时系统的性能，但是在32位Solaris上完全禁用，因为它与procfs和swapctl不兼容。

### 6 Large file support

Large file support, often abbreviated to LFS, is the term applied to the
ability to work with files larger than 2 GB on 32-bit operating systems.
Since Zabbix 2.0 support for large files has been added. This change
affects at least [log file
monitoring](/manual/config/items/itemtypes/log_items) and all
[vfs.file.\*
items](/manual/config/items/itemtypes/zabbix_agent#supported_item_keys).
Large file support depends on the capabilities of a system at Zabbix
compilation time, but is completely disabled on a 32-bit Solaris due to
its incompatibility with procfs and swapctl.

[comment]: # ({/new-6f5dcc8c})
