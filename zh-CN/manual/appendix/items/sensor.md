[comment]: # translation:outdated

[comment]: # ({new-b33b8329})
# 7 传感器

每个传感器芯片在sysfs /sys/devices 都有自己的目录。
要找到所有的传感器芯片，从/sys/class/hwmon/hwmon\*跟踪设备的符号链接更容易
, 这里 \* is 是个数字 (0,1,2,...)。

对于虚拟设备，传感器读数在 /sys/class/hwmon/hwmon\*/ 目录,
对于非虚拟设备，传感器读数在 /sys/class/hwmon/hwmon\*/device 目录。
hwmon\* 或 hwmon\*/device 目录中一个叫name的文件 包含该芯片的名称,
它对应于传感器芯片所使用的内核驱动程序的名称。

每个文件只有一个传感器读取值。
在上面提到的目录中包含传感器读数的文件的命令常用方案是:
<type><number>\_<item>, 这里

-   **type** - 对于传感器芯片： "in" (电压), "temp" (温度), "fan"
    (风扇), 等，
-   **item** - "input" (测量值), "max" (高阈值), "min" (低阈值), 等,
-   **number** - 总是用于可以不止一次出现的元素 (经常从 1开始,
    除了电压从 0开始)，
    如果文件不引用特定的元素，则它们的名称简单，没有数字。

可以通过 **sensor-detect** 和**sensors**
工具获取主机上可用的传感器信息(lm-sensors package:
<http://lm-sensors.org/>)。 **Sensors-detect**
帮助确定哪些模块对于可用的传感器是必需的。 当模块加载 **sensors**
程序时可以用来显示所有传感器芯片的读数。
该程序使用的传感器读数的标记可以和常规的命名方案不同
(<type><number>\_<item> ):

-   如果有一个名为 <type><number>\_label的文件,
    那么该文件中的标签会代替 <type><number><item>
    名字;
-   如果没有名为 <type><number>\_label 的文件, 那么程序会在
    /etc/sensors.conf (也许会为/etc/sensors3.conf, 或其他的)
    文件中找name的替代标签。

这个标签允许用户决定使用什么样的硬件。 如果既没有
<type><number>\_label 文件，配置文件中也没有label
，那么硬件的类型可以由分配的名字 (hwmon\*/device/name)决定。
zabbix\_agent接受的传感器的实际名称可以通过运行 **sensors** 程序带着 -u
参数 (**sensors -u**).

在**sensor** 程序中，可用的传感器被总线类型
(ISA适配器，PCI适配器，SPI适配器，虚拟设备，ACPI接口，HID适配器)分开。

[comment]: # ({/new-b33b8329})

[comment]: # ({new-61bf6279})
##### Linux 2.4:

(传感器读数从/ proc/sys/dev/sensor目录获得)

-   **device** - 设备名字 (如果使用了<mode>， 则是正则表达式);
-   **sensor** - 传感器名字 (如果使用了<mode>， 则是正则表达式);
-   **mode** - 可能的值: avg, max, min
    (如果忽略了这个参数，设备和传感器将逐字处理)。

例子: sensor\[w83781d-i2c-0-2d,temp1\]

在Zabbix 1.8.4之前，使用了sensor\[temp1\]格式。

[comment]: # ({/new-61bf6279})

[comment]: # ({new-2e1edda6})
##### Linux 2.6+:

(传感器读数从/ sys / class / hwmon目录获得)

-   **device** - 设备名称(非正则表达式)。
    设备名称可以是设备的实际名称(e.g
    0000:00:18.3)或使用传感器程序获取得的名称(例如:k8temp-pci-00c3)，这由用户决定使用哪个名称;
-   **sensor** - 传感器名称(非正则表达式);
-   **mode** - 可能的值: avg, max, min
    (如果忽略了这个参数，设备和传感器将逐字处理)。

例如:

sensor\[k8temp-pci-00c3,temp, max\] 或 sensor\[0000:00:18.3,temp1\]

sensor\[smsc47b397-isa-0880,in, avg\] 或 sensor\[smsc47b397.2176,in1\]

[comment]: # ({/new-2e1edda6})

[comment]: # ({new-0cb28aa9})
#### 获取传感器的名字

传感器标签，由*sensors*命令打印，不能总是被直接使用，因为标签的命名对于每个传感器芯片供应商来说可能是不同的。
例如, *sensors* 输出可能包含以下几行:

    $ sensors
    in0:         +2.24 V  (min =  +0.00 V, max =  +3.32 V)   
    Vcore:       +1.15 V  (min =  +0.00 V, max =  +2.99 V)   
    +3.3V:       +3.30 V  (min =  +2.97 V, max =  +3.63 V)   
    +12V:       +13.00 V  (min =  +0.00 V, max = +15.94 V)
    M/B Temp:    +30.0°C  (low  = -127.0°C, high = +127.0°C)

在这些情况下，只有一个标签可以直接使用:

    $ zabbix_get -s 127.0.0.1 -k sensor[lm85-i2c-0-2e,in0]
    2.240000

尝试使用其他标签 (像 *Vcore* 或 *+12V*) 是不会起作用的。

    $ zabbix_get -s 127.0.0.1 -k sensor[lm85-i2c-0-2e,Vcore]
    ZBX_NOTSUPPORTED

为了找到实际的Zabbix可以使用它来检索读数的传感器名称，运行 *sensors
-u*命令。 在输出中，可以看到到以下内容:

    $ sensors -u
    ...
    Vcore:
      in1_input: 1.15
      in1_min: 0.00
      in1_max: 2.99
      in1_alarm: 0.00
    ...    
    +12V:
      in4_input: 13.00
      in4_min: 0.00
      in4_max: 15.94
      in4_alarm: 0.00
    ...

所有*Vcore* 应该检索 *in1*,*+12V* 应该检索 *in4*.[^1]

    $ zabbix_get -s 127.0.0.1 -k sensor[lm85-i2c-0-2e,in1]
    1.301000

不止 电压 (in), 还有 电流 (curr), 温度 (temp) 和 风扇转速 (fan)
的读数都可以被Zabbix 检索到。

### 7 Sensor

Each sensor chip gets its own directory in the sysfs /sys/devices tree.
To find all sensor chips, it is easier to follow the device symlinks
from /sys/class/hwmon/hwmon\*, where \* is a real number (0,1,2,...).

The sensor readings are located either in /sys/class/hwmon/hwmon\*/
directory for virtual devices, or in /sys/class/hwmon/hwmon\*/device
directory for non-virtual devices. A file, called name, located inside
hwmon\* or hwmon\*/device directories contains the name of the chip,
which corresponds to the name of the kernel driver used by the sensor
chip.

There is only one sensor reading value per file. The common scheme for
naming the files that contain sensor readings inside any of the
directories mentioned above is:
<type><number>\_<item>, where

-   **type** - for sensor chips is "in" (voltage), "temp" (temperature),
    "fan" (fan), etc.,
-   **item** - "input" (measured value), "max" (high threshold), "min"
    (low threshold), etc.,
-   **number** - always used for elements that can be present more than
    once (usually starts from 1, except for voltages which start from
    0). If files do not refer to a specific element they have a simple
    name with no number.

The information regarding sensors available on the host can be acquired
using **sensor-detect** and **sensors** tools (lm-sensors package:
<http://lm-sensors.org/>). **Sensors-detect** helps to determine which
modules are necessary for available sensors. When modules are loaded the
**sensors** program can be used to show the readings of all sensor
chips. The labeling of sensor readings, used by this program, can be
different from the common naming scheme
(<type><number>\_<item> ):

-   if there is a file called <type><number>\_label, then
    the label inside this file will be used instead of
    <type><number><item> name;
-   if there is no <type><number>\_label file, then the
    program searches inside the /etc/sensors.conf (could be also
    /etc/sensors3.conf, or different) for the name substitution.

This labeling allows user to determine what kind of hardware is used. If
there is neither <type><number>\_label file nor label inside
the configuration file the type of hardware can be determined by the
name attribute (hwmon\*/device/name). The actual names of sensors, which
zabbix\_agent accepts, can be obtained by running **sensors** program
with -u parameter (**sensors -u**).

In **sensor** program the available sensors are separated by the bus
type (ISA adapter, PCI adapter, SPI adapter, Virtual device, ACPI
interface, HID adapter).

##### On Linux 2.4:

(Sensor readings are obtained from /proc/sys/dev/sensors directory)

-   **device** - device name (if <mode> is used, it is a regular
    expression);
-   **sensor** - sensor name (if <mode> is used, it is a regular
    expression);
-   **mode** - possible values: avg, max, min (if this parameter is
    omitted, device and sensor are treated verbatim).

Example key: sensor\[w83781d-i2c-0-2d,temp1\]

Prior to Zabbix 1.8.4, the sensor\[temp1\] format was used.

##### On Linux 2.6+:

(Sensor readings are obtained from /sys/class/hwmon directory)

-   **device** - device name (non regular expression). The device name
    could be the actual name of the device (e.g 0000:00:18.3) or the
    name acquired using sensors program (e.g. k8temp-pci-00c3). It is up
    to the user to choose which name to use;
-   **sensor** - sensor name (non regular expression);
-   **mode** - possible values: avg, max, min (if this parameter is
    omitted, device and sensor are treated verbatim).

Example key:

sensor\[k8temp-pci-00c3,temp, max\] or sensor\[0000:00:18.3,temp1\]

sensor\[smsc47b397-isa-0880,in, avg\] or sensor\[smsc47b397.2176,in1\]

#### Obtaining sensor names

Sensor labels, as printed by the *sensors* command, cannot always be
used directly because the naming of labels may be different for each
sensor chip vendor. For example, *sensors* output might contain the
following lines:

    $ sensors
    in0:         +2.24 V  (min =  +0.00 V, max =  +3.32 V)   
    Vcore:       +1.15 V  (min =  +0.00 V, max =  +2.99 V)   
    +3.3V:       +3.30 V  (min =  +2.97 V, max =  +3.63 V)   
    +12V:       +13.00 V  (min =  +0.00 V, max = +15.94 V)
    M/B Temp:    +30.0°C  (low  = -127.0°C, high = +127.0°C)

Out of these, only one label may be used directly:

    $ zabbix_get -s 127.0.0.1 -k sensor[lm85-i2c-0-2e,in0]
    2.240000

Attempting to use other labels (like *Vcore* or *+12V*) will not work.

    $ zabbix_get -s 127.0.0.1 -k sensor[lm85-i2c-0-2e,Vcore]
    ZBX_NOTSUPPORTED

To find out the actual sensor name, which can be used by Zabbix to
retrieve the sensor readings, run *sensors -u*. In the output, the
following may be observed:

    $ sensors -u
    ...
    Vcore:
      in1_input: 1.15
      in1_min: 0.00
      in1_max: 2.99
      in1_alarm: 0.00
    ...    
    +12V:
      in4_input: 13.00
      in4_min: 0.00
      in4_max: 15.94
      in4_alarm: 0.00
    ...

So *Vcore* should be queried as *in1*, and *+12V* should be queried as
*in4*.[^2]

    $ zabbix_get -s 127.0.0.1 -k sensor[lm85-i2c-0-2e,in1]
    1.301000

Not only voltage (in), but also current (curr), temperature (temp) and
fan speed (fan) readings can be retrieved by Zabbix.

[^1]: 根据
    [声明](https://www.kernel.org/doc/Documentation/hwmon/sysfs-interface)
    这些是芯片引脚上的电压，一般来说可能需要缩放。

[^2]: According to
    [specification](https://www.kernel.org/doc/Documentation/hwmon/sysfs-interface)
    these are voltages on chip pins and generally speaking may need
    scaling.

[comment]: # ({/new-0cb28aa9})
