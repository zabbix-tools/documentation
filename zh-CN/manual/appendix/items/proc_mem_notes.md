[comment]: # translation:outdated

[comment]: # ({new-098d0431})
# 8 proc.mem 监控项中memtype参数类型的注意事项

[comment]: # ({/new-098d0431})

[comment]: # ({new-2840c113})
#### 概述

Linux, AIX, FreeBSD 和 Solaris 都支持**memtype**参数。

'memtype' 参数的三个常用值 `pmem`, `rss` 和
`vsize`在所有系统中都适用。另外, 在一些系统中只支持该系统下的 'memtype'
值。

[comment]: # ({/new-2840c113})

[comment]: # ({new-04a03696})
#### AIX

请参见表中AIX上的“memtype”参数所支持的值.

|支持的参数值                 描述|Source|n procentry64 structure   Tries t|be compatible with|
|-----------------------------------------|------|---------------------------------|------------------|
|vsize (( - default value))|虚拟内存大小           pi\_s|ze|<|
|pmem|实际内存的百分比       pi\_prm|ps -o p|em|
|rss|驻留集大小             pi\_|rss + pi\_drss               ps -|rssize|
|size|进程大小(代码+数据)    pi\_dvm|"ps gvw|SIZE column|
|dsize|数据大小               pi<br>|dsize|<|
|tsize|文本(代码)的大小       pi\_ts|ze                         "ps gv|" TSIZ column|
|sdsize|来自共享库的数据大小   pi\_sdsiz|<|<|
|drss|数据驻留集大小         pi\_dr|s|<|
|trss|文本驻留集大小         pi\_tr|s|<|

[comment]: # ({/new-04a03696})

[comment]: # ({new-cc0df248})

Notes for AIX:

1. When choosing parameters for proc.mem[] item key on AIX, try to specify narrow process selection criteria. Otherwise there is a risk of getting unwanted processes counted into proc.mem[] result.

Example:
```
\$ zabbix_agentd -t proc.mem[,,,NonExistingProcess,rss]
proc.mem[,,,NonExistingProcess,rss]           [u|2879488]
```

This example shows how specifying only command line (regular expression to match) parameter results in Zabbix agent self-accounting - probably not what you want.

[comment]: # ({/new-cc0df248})

[comment]: # ({new-14c4cc2b})
2. Do not use "ps -ef" to browse processes - it shows only non-kernel processes. Use "ps -Af" to see all processes which will be seen by Zabbix agent.

3. Let's go through example of 'topasrec' how Zabbix agent proc.mem[] selects processes.

```
\$ ps -Af | grep topasrec
root 10747984        1   0   Mar 16      -  0:00 /usr/bin/topasrec  -L -s 300 -R 1 -r 6 -o /var/perf daily/ -ypersistent=1 -O type=bin -ystart_time=04:08:54,Mar16,2023
```

proc.mem[] has arguments:

```
proc.mem[<name>,<user>,<mode>,<cmdline>,<memtype>]
```

[comment]: # ({/new-14c4cc2b})

[comment]: # ({new-0be32659})

The 1st criterion is a process name (argument <name>). In our example Zabbix agent will see it as 'topasrec'. In order to match, you need to either specify 'topasrec' or to leave it empty.
The 2nd criterion is a user name (argument <user>). To match, you need to either specify 'root' or to leave it empty.
The 3rd criterion used in process selection is an argument <cmdline>. Zabbix agent will see its value as '/usr/bin/topasrec -L -s 300 -R 1 -r 6 -o /var/perf/daily/ -ypersistent=1 -O type=bin -ystart_time=04:08:54,Mar16,2023'. To match, you need to either specify a regular expression which matches this string or to leave it empty.

Arguments <mode> and <memtype> are applied after using the three criteria mentioned above. 

[comment]: # ({/new-0be32659})

[comment]: # ({new-8a700330})
#### FreeBSD

请参见表中FreeBSD上的“memtype”参数支持的值。

|Supported value|Description|Source in kinfo\_proc structure|Tries to be compatible with|
|---------------|-----------|-------------------------------|---------------------------|
|vsize|虚拟内存大小               kp\_e|roc.e\_vm.vm\_map.size or ki\_size   ps -o|vsz|
|pmem|实际内存的百分比           calcula|ed from rss                        ps -o p|em|
|rss|驻留集大小                 kp\_|proc.e\_vm.vm\_rssize or ki\_rssize   ps -|rss|
|size (( - default value))|进程(代码+数据+堆栈)大小   tsize + d|ize + ssize|<|
|tsize|文本(代码)的大小           kp\_ep|oc.e\_vm.vm\_tsize or ki\_tsize     ps -o|siz|
|dsize|数据大小                   kp<br>|eproc.e\_vm.vm\_dsize or ki\_dsize     ps|o dsiz|
|ssize|堆栈大小                   kp<br>|eproc.e\_vm.vm\_ssize or ki\_ssize     ps|o ssiz|

[comment]: # ({/new-8a700330})

[comment]: # ({new-f43d1dfd})
#### Linux

请参见表中Linux上的“memtype”参数支持的值。

|Supported value|Description|Source in /proc/<pid>/status file|
|---------------|-----------|---------------------------------------|
|vsize (( - default value))|虚拟内存大小           VmSiz|<|
|pmem|实际内存的百分比       (VmRSS/|otal\_memory) \* 100|
|rss|驻留集大小             VmRS|<|
|data|数据段的大小           VmDat|<|
|exe|代码段的大小           VmExe|<|
|hwm|驻留集峰值大小         VmHWM|<|
|lck|锁定内存大小           VmLck|<|
|lib|共享库的大小           VmLib|<|
|peak|虚拟内存峰值大小       VmPeak|<|
|pin|固定的页面大小         VmPin|<|
|pte|页表条目的大小         VmPTE|<|
|size|进程码+数据+栈段大小   VmExe +|mData + VmStk|
|stk|堆栈段大小             VmSt|<|
|swap|使用的交换空间大小     VmSwap|<|

Linux上注意事项:

1.  一些旧版本Linux 内核并不是支持所有'memtype' 值的。例如, Linux
    内核版本2.4就不支持 `hwm`, `pin`, `peak`, `pte` 和 `swap` 等值。
2.  我们发现 Zabbix agent
    主动检查进程参数`proc.mem[...,...,...,...,data]` 显示的值比agent 的
    /proc/<pid>/status 文件中 `VmData`行的值大大 4
    kB。在agent自我监控管理时，agent的数据碎片增长率4 kB
    ，然后又返回到先前的值。

[comment]: # ({/new-f43d1dfd})

[comment]: # ({new-96fd9f28})
#### Solaris

请参见表中的Solaris上的“memtype”参数所支持的值。

|支持的参数值                 描述|Source|n psinfo structure   兼容|<|
|-----------------------------------------|------|---------------------------|-|
|vsize (( - default value))|Size of process image|pr\_size|ps -o vsz|
|pmem|实际内存的百分比                         pr\_pct|em                   ps -o p|em|
|rss|驻留集大小\                              pr\_可能会被低估 - 参看 "man ps"中rss描述.|ssize                   ps -|rss|

[comment]: # ({/new-96fd9f28})

[comment]: # ({new-e0ce8129})
### 8 Notes on memtype parameter in proc.mem items

#### Overview

The **memtype** parameter is supported on Linux, AIX, FreeBSD, and
Solaris platforms.

Three common values of 'memtype' are supported on all of these
platforms: `pmem`, `rss` and `vsize`. Additionally, platform-specific
'memtype' values are supported on some platforms.

#### AIX

See values supported for 'memtype' parameter on AIX in the table.

|Supported value|Description|Source in procentry64 structure|Tries to be compatible with|
|---------------|-----------|-------------------------------|---------------------------|
|vsize (( - default value))|Virtual memory size|pi\_size|<|
|pmem|Percentage of real memory|pi\_prm|ps -o pmem|
|rss|Resident set size|pi\_trss + pi\_drss|ps -o rssize|
|size|Size of process (code + data)|pi\_dvm|"ps gvw" SIZE column|
|dsize|Data size|pi\_dsize|<|
|tsize|Text (code) size|pi\_tsize|"ps gvw" TSIZ column|
|sdsize|Data size from shared library|pi\_sdsize|<|
|drss|Data resident set size|pi\_drss|<|
|trss|Text resident set size|pi\_trss|<|

#### FreeBSD

See values supported for 'memtype' parameter on FreeBSD in the table.

|Supported value|Description|Source in kinfo\_proc structure|Tries to be compatible with|
|---------------|-----------|-------------------------------|---------------------------|
|vsize|Virtual memory size|kp\_eproc.e\_vm.vm\_map.size or ki\_size|ps -o vsz|
|pmem|Percentage of real memory|calculated from rss|ps -o pmem|
|rss|Resident set size|kp\_eproc.e\_vm.vm\_rssize or ki\_rssize|ps -o rss|
|size (( - default value))|Size of process (code + data + stack)|tsize + dsize + ssize|<|
|tsize|Text (code) size|kp\_eproc.e\_vm.vm\_tsize or ki\_tsize|ps -o tsiz|
|dsize|Data size|kp\_eproc.e\_vm.vm\_dsize or ki\_dsize|ps -o dsiz|
|ssize|Stack size|kp\_eproc.e\_vm.vm\_ssize or ki\_ssize|ps -o ssiz|

#### Linux

See values supported for 'memtype' parameter on Linux in the table.

|Supported value|Description|Source in /proc/<pid>/status file|
|---------------|-----------|---------------------------------------|
|vsize (( - default value))|Virtual memory size|VmSize|
|pmem|Percentage of real memory|(VmRSS/total\_memory) \* 100|
|rss|Resident set size|VmRSS|
|data|Size of data segment|VmData|
|exe|Size of code segment|VmExe|
|hwm|Peak resident set size|VmHWM|
|lck|Size of locked memory|VmLck|
|lib|Size of shared libraries|VmLib|
|peak|Peak virtual memory size|VmPeak|
|pin|Size of pinned pages|VmPin|
|pte|Size of page table entries|VmPTE|
|size|Size of process code + data + stack segments|VmExe + VmData + VmStk|
|stk|Size of stack segment|VmStk|
|swap|Size of swap space used|VmSwap|

Notes for Linux:

1.  Not all 'memtype' values are supported by older Linux kernels. For
    example, Linux 2.4 kernels do not support `hwm`, `pin`, `peak`,
    `pte` and `swap` values.
2.  We have noticed that self-monitoring of the Zabbix agent active
    check process with `proc.mem[...,...,...,...,data]` shows a value
    that is 4 kB larger than reported by `VmData` line in the agent's
    /proc/<pid>/status file. At the time of self-measurement the
    agent's data segment increases by 4 kB and then returns to the
    previous size.

#### Solaris

See values supported for 'memtype' parameter on Solaris in the table.

|Supported value|Description|Source in psinfo structure|Tries to be compatible with|
|---------------|-----------|--------------------------|---------------------------|
|vsize (( - default value))|Size of process image|pr\_size|ps -o vsz|
|pmem|Percentage of real memory|pr\_pctmem|ps -o pmem|
|rss|Resident set size<br>It may be underestimated - see rss description in "man ps".|pr\_rssize|ps -o rss|

[comment]: # ({/new-e0ce8129})
