[comment]: # translation:outdated

[comment]: # ({new-e8dfa374})
# 2 参数vm.memory.size

-   **total** - 总物理内存.
-   **free** - 可用内存.
-   **active** - 内存当前使用或最近使用，所以它在RAM中。
-   **inactive** - 未使用内存.
-   **wired** - 被标记为始终驻留在RAM中的内存，不会移动到磁盘。
-   **pinned** - 和'wired'一样。
-   **anon** - 与文件无关的内存(不能重新读取)。
-   **exec** - 可执行代码，通常来自于一个(程序)文件。
-   **file** - 缓存最近访问文件的目录。
-   **buffers** - 缓存文件系统元数据。
-   **cached** - 缓存为不同事情。
-   **shared** - 可以同时被多个进程访问的内存。
-   **used** - active + wired 内存。
-   **pused** - active + wired 总内存的百分比。
-   **available** - inactive + cached + free 内存。
-   **pavailable** - inactive + cached + free memory 占'total'的百分比。

::: noteimportant
 *vm.memory.size\[used\]* 和
*vm.memory.size\[available\]* 的和不是必需等于总内存。 例如, 在FreeBSD中
active, inactive, wired, cached被认为是使用的内存，
因为他们存储一些有用的信息。\
同样，inactive, cached, free 也被认为是可用内存，
因为这些内存可以立即被分配给需要更多内存的线程。\
所以不活动的内存是同时可以是使用和可用的。 正因为如此, item
*vm.memory.size\[used\]* 只用来获得信息, 监控项
*vm.memory.size\[available\]* 在触发器中使用。\

:::

::: noteclassic
 参看本页底部 ["另外见"](#see_also)
部分关于在不同的操作系统中内存计算的更多详细信息。
:::

[comment]: # ({/new-e8dfa374})

[comment]: # ({new-0863fe63})
##### 特定系统的注意事项

-   在Solaris中 **available** and **free** 是一样的。
-   在Linux中 **shared** 只在 kernel 2.4中起作用。

[comment]: # ({/new-0863fe63})

[comment]: # ({new-4ea4e379})
### 另见

1.  [关于不同操作系统内存计算的详细信息](http://blog.zabbix.com/when-alexei-isnt-looking#vm.memory.size)

[comment]: # ({/new-4ea4e379})

[comment]: # ({new-f80ad839})
### 2 vm.memory.size parameters

#### Overview

This section provides more details and platform-specific information on
the parameters of the `vm.memory.size[<mode>]` [agent
item](/manual/config/items/itemtypes/zabbix_agent).

#### Parameters

The following parameters are possible for this item:

-   **active** - memory currently in use or very recently used, and so
    it is in RAM
-   **anon** - memory not associated with a file (cannot be re-read from
    it)
-   **available** - available memory, calculated differently depending
    on the platform (see the table below)
-   **buffers** - cache for things like file system metadata
-   **cached** - cache for various things
-   **exec** - executable code, typically from a (program) file
-   **file** - cache for contents of recently accessed files
-   **free** - memory that is readily available to any entity requesting
    memory
-   **inactive** - memory that is marked as not used
-   **pavailable** - inactive + cached + free memory as percentage of
    'total'
-   **pinned** - same as 'wired'
-   **pused** - active + wired memory as percentage of 'total'
-   **shared** - memory that may be simultaneously accessed by multiple
    processes
-   **slab** - total amount of memory used by the kernel to cache data
    structures for its own use
-   **total** - total physical memory available
-   **used** - used memory, calculated differently depending on the
    platform (see the table below)
-   **wired** - memory that is marked to always stay in RAM. It is never
    moved to disk.

Platform-specific calculation of **available** and **used**:

|Platform|**"available"**|**"used"**|
|--------|---------------|----------|
|*AIX*|free + cached|real memory in use|
|*FreeBSD*|inactive + cached + free|active + wired + cached|
|*HP UX*|free|total - free|
|*Linux<3.14*|free + buffers|total - free|
|*Linux 3.14+*|/proc/meminfo, "Cached":+"MemAvailable:"|total - free|
|*NetBSD*|inactive + execpages + file + free|total - free|
|*OpenBSD*|inactive + free + cached|active + wired|
|*OSX*|inactive + free|active + wired|
|*Solaris*|free|total - free|
|*Win32*|free|total - free|

::: noteimportant
The sum of *vm.memory.size\[used\]* and
*vm.memory.size\[available\]* does not necessarily equal total. For
instance, on FreeBSD:\
\* Active, inactive, wired, cached memories are considered used, because
they store some useful information.\
\* At the same time inactive, cached, free memories are considered
available, because these kinds of memories can be given instantly to
processes that request more memory.\
\
So inactive memory is both used and available simultaneously. Because of
this, the *vm.memory.size\[used\]* item is designed for informational
purposes only, while *vm.memory.size\[available\]* is designed to be
used in triggers.
:::

See the ["See also"](#see_also) section at the bottom of this page to
find more detailed information about memory calculation in different OS.

##### Platform-specific notes

-   on Linux **shared** works only on kernel 2.4

### See also

1.  [Detailed information about memory calculation in different
    OS](http://blog.zabbix.com/when-alexei-isnt-looking#vm.memory.size)

[comment]: # ({/new-f80ad839})
