[comment]: # translation:outdated

[comment]: # ({new-1602be12})
# 4 捕捉器监控项

[comment]: # ({/new-1602be12})

[comment]: # ({new-ea58f0b6})
#### 概述

Zabbix服务器使用基于JSON的通信协议，在[trapper
item](manual：config：items：itemtypes：trapper)的帮助下从Zabbix发送器接收数据。

请求和响应消息必须以[header and data
length](/manual/appendix/protocols/header_datalen)开头。

[comment]: # ({/new-ea58f0b6})

[comment]: # ({new-51cd3761})
#### Zabbix发送请求

``` {.javascript}
{
    "request":"sender data",
    "data":[
        {
            "host":"<hostname>",
            "key":"trap",
            "value":"test value"
        }
    ]
}
```

[comment]: # ({/new-51cd3761})

[comment]: # ({new-c4e3e287})
#### Zabbix服务器响应

``` {.javascript}
{
    "response":"success",
    "info":"processed: 1; failed: 0; total: 1; seconds spent: 0.060753"
}
```

[comment]: # ({/new-c4e3e287})

[comment]: # ({new-64bfec01})
#### 或者，Zabbix发送者可以发送带有时间戳的请求

``` {.javascript}
{
    "request":"sender data",
    "data":[
        {
            "host":"<hostname>",
            "key":"trap",
            "value":"test value",
            "clock":1516710794
        },
        {
            "host":"<hostname>",
            "key":"trap",
            "value":"test value",
            "clock":1516710795
        }
    ],
    "clock":1516712029,
    "ns":873386094
}
```

[comment]: # ({/new-64bfec01})

[comment]: # ({new-c71e91fd})
#### Zabbix服务器响应

``` {.javascript}
{
    "response":"success",
    "info":"processed: 2; failed: 0; total: 2; seconds spent: 0.060904"
}
```

## 4 Trapper items

#### Overview

Zabbix server uses a JSON- based communication protocol for receiving
data from Zabbix sender with the help of [trapper
item](/manual/config/items/itemtypes/trapper).

Request and response messages must begin with [header and data
length](/manual/appendix/protocols/header_datalen).

#### Zabbix sender request

``` {.javascript}
{
    "request":"sender data",
    "data":[
        {
            "host":"<hostname>",
            "key":"trap",
            "value":"test value"
        }
    ]
}
```

#### Zabbix server response

``` {.javascript}
{
    "response":"success",
    "info":"processed: 1; failed: 0; total: 1; seconds spent: 0.060753"
}
```

#### Alternatively Zabbix sender can send request with a timestamp

``` {.javascript}
{
    "request":"sender data",
    "data":[
        {
            "host":"<hostname>",
            "key":"trap",
            "value":"test value",
            "clock":1516710794
        },
        {
            "host":"<hostname>",
            "key":"trap",
            "value":"test value",
            "clock":1516710795
        }
    ],
    "clock":1516712029,
    "ns":873386094
}
```

#### Zabbix server response

``` {.javascript}
{
    "response":"success",
    "info":"processed: 2; failed: 0; total: 2; seconds spent: 0.060904"
}
```

[comment]: # ({/new-c71e91fd})
