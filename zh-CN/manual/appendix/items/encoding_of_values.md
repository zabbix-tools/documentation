[comment]: # translation:outdated

[comment]: # ({new-df5bbb96})
# 5 返回值的编码

Zabbix server 期望每个返回的文本值都是UTF8编码的，
这涉及每一种类型的检查: zabbix agent, ssh, telnet等等。

不同的监视系统/设备和检查的返回值中可能有非ascii字符。对于这种情况，几乎所有的zabbix
keys都包含一个额外的item key参数\* \* <encoding> \* \*。
这个关键参数是可选的，但是如果返回的值不是UTF8编码，并且它包含非ascii字符，则应该指定它。否则，结果可能是出乎意料的和不可预测的。

在这种情况下，对不同数据库后台的行为描述如下。

[comment]: # ({/new-df5bbb96})

[comment]: # ({new-b4119079})
#### MySQL

如果一个值在非UTF8编码中包含非ascii字符，那么当数据库存储此值时，该字符及该字符后的值将被丢弃。没有警告信息写入
*zabbix\_server.log*.\
Relevant for at least MySQL version 5.1.61

[comment]: # ({/new-b4119079})

[comment]: # ({new-c305c3e1})
#### PostgreSQL

如果一个值在非UTF8编码中包含非ascii字符—这将导致一个失败的SQL查询(PGRES\_FATAL\_ERROR:编码的无效字节序列)和数据将不会被存储。会向*zabbix\_server.log*中写入一个适当的警告消息.\
Relevant for at least PostgreSQL version 9.1.3

### 5 Encoding of returned values

Zabbix server expects every returned text value in the UTF8 encoding.
This is related to any type of checks: zabbix agent, ssh, telnet, etc.

Different monitored systems/devices and checks can return non-ASCII
characters in the value. For such cases, almost all possible zabbix keys
contain an additional item key parameter - **<encoding>**. This
key parameter is optional but it should be specified if the returned
value is not in the UTF8 encoding and it contains non-ASCII characters.
Otherwise the result can be unexpected and unpredictable.

A description of behavior with different database back-ends in such
cases follows.

#### MySQL

If a value contains a non-ASCII character in non UTF8 encoding - this
character and the following will be discarded when the database stores
this value. No warning messages will be written to the
*zabbix\_server.log*.\
Relevant for at least MySQL version 5.1.61

#### PostgreSQL

If a value contains a non-ASCII character in non UTF8 encoding - this
will lead to a failed SQL query (PGRES\_FATAL\_ERROR:ERROR invalid byte
sequence for encoding) and data will not be stored. An appropriate
warning message will be written to the *zabbix\_server.log*.\
Relevant for at least PostgreSQL version 9.1.3

[comment]: # ({/new-c305c3e1})
