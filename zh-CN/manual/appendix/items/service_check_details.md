[comment]: # translation:outdated

[comment]: # ({new-0c4b8ed3})
# 10 net.tcp.service 和 net.udp.service 检查的实现细节

net.tcp.service 和net.udp.service 检查实现的细节在该页详细介绍，
不同的服务指定不同的服务参数。

[comment]: # ({/new-0c4b8ed3})

[comment]: # ({new-b8e37f71})
#### 监控项 net.tcp.service 参数

**ftp**

创建一个TCP连接，并期望响应的前4个字符是“220”，然后发送“QUIT\\r\\n”。
如果未指定，则使用缺省端口21。

**http**

创建一个TCP连接，而不需要等待和发送任何东西。如果未指定，则使用缺省端口80。

**https**

使用(并且只使用)libcurl，不验证证书的真实性，不验证SSL证书中的主机名，只获取响应头(HEAD请求)。
如果未指定端口，则使用默认端口443。

**imap**

创建一个TCP连接，并期望响应的前4个字符是“\* OK”，然后发送"a1
LOGOUT\\r\\n”。如果未指定，则使用缺省端口143。

**ldap**

打开到LDAP服务器的连接，并使用过滤器集执行LDAP搜索操作(objectClass=\*)。期望成功地检索第一个条目的第一个属性。如果未指定，则使用缺省端口389。

**nntp**

创建一个TCP连接，并期望响应的前3个字符是“200”或“201”，然后发送“QUIT\\r\\n”。如果未指定，则使用缺省端口119。

**pop**

创建一个TCP连接，并期望响应的前3个字符是“+OK”，然后发送“QUIT\\r\\n”。如果未指定，则使用缺省端口110。

**smtp**

创建一个TCP连接，并期望响应的前3个字符是“220”，然后是空格、行的结束或虚线。包含一个虚线的行属于多行响应，响应将被重新读取，直到收到一条没有虚线的行。然后发送“QUIT\\r\\n”。如果未指定，则使用缺省端口25。

**ssh**

创建一个TCP连接，
如果建立了连接，双方交换一个标识字符串(SSH-major.minor-XXXX)，其中major
和minor是协议版本，XXXX是一个字符串。
Zabbix检查是否找到了匹配该指定的字符串，不匹配则返回返回字符串“SSH-major.minor-zabbix\_agent\\r\\n”或者“0\\n"。
如果未指定，则使用缺省端口22。

**tcp**

创建一个TCP连接，而不需要等待和发送任何东西。与其他检查需要指定端口参数不同。

**telnet**

创建一个TCP连接，并期望一个登录提示(':'在最后)。如果未指定，则使用缺省端口23。

[comment]: # ({/new-b8e37f71})

[comment]: # ({new-fec6c203})
#### Item net.udp.service parameters

**ntp**

在UDP上发送一个SNTP包，并根据 [RFC 4330, section
5](http://tools.ietf.org/html/rfc4330#section-5)需要验证响应。
如果未指定，则使用默认端口123。

### 10 Implementation details of net.tcp.service and net.udp.service checks

Implementation of net.tcp.service and net.udp.service checks is detailed
on this page for various services specified in the service parameter.

#### Item net.tcp.service parameters

**ftp**

Creates a TCP connection and expects the first 4 characters of the
response to be "220 ", then sends "QUIT\\r\\n". Default port 21 is used
if not specified.

**http**

Creates a TCP connection without expecting and sending anything. Default
port 80 is used if not specified.

**https**

Uses (and only works with) libcurl, does not verify the authenticity of
the certificate, does not verify the host name in the SSL certificate,
only fetches the response header (HEAD request). Default port 443 is
used if not specified.

**imap**

Creates a TCP connection and expects the first 4 characters of the
response to be "\* OK", then sends "a1 LOGOUT\\r\\n". Default port 143
is used if not specified.

**ldap**

Opens a connection to an LDAP server and performs an LDAP search
operation with filter set to (objectClass=\*). Expects successful
retrieval of the first attribute of the first entry. Default port 389 is
used if not specified.

**nntp**

Creates a TCP connection and expects the first 3 characters of the
response to be "200" or "201", then sends "QUIT\\r\\n". Default port 119
is used if not specified.

**pop**

Creates a TCP connection and expects the first 3 characters of the
response to be "+OK", then sends "QUIT\\r\\n". Default port 110 is used
if not specified.

**smtp**

Creates a TCP connection and expects the first 3 characters of the
response to be "220", followed by a space, the line ending or a dash.
The lines containing a dash belong to a multi-line response and the
response will be re-read until a line without the dash is received. Then
sends "QUIT\\r\\n". Default port 25 is used if not specified.

**ssh**

Creates a TCP connection. If the connection has been established, both
sides exchange an identification string (SSH-major.minor-XXXX), where
major and minor are protocol versions and XXXX is a string. Zabbix
checks if the string matching the specification is found and then sends
back the string "SSH-major.minor-zabbix\_agent\\r\\n" or "0\\n" on
mismatch. Default port 22 is used if not specified.

**tcp**

Creates a TCP connection without expecting and sending anything. Unlike
the other checks requires the port parameter to be specified.

**telnet**

Creates a TCP connection and expects a login prompt (':' at the end).
Default port 23 is used if not specified.

#### Item net.udp.service parameters

**ntp**

Sends an SNTP packet over UDP and validates the response according to
[RFC 4330, section 5](http://tools.ietf.org/html/rfc4330#section-5).
Default port 123 is used if not specified.

[comment]: # ({/new-fec6c203})
