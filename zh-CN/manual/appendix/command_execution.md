[comment]: # translation:outdated

[comment]: # ({new-d53fbea3})
# 10 命令执行

[comment]: # ({/new-d53fbea3})

[comment]: # ({new-a11705f0})
### 10 Command execution

Zabbix uses common functionality for external checks, user parameters,
system.run items, custom alert scripts, remote commands and user
scripts.
Zabbix用常规功外部检查、用户参数、system.run监控项、自定义告警脚本、远程指令和用户指令。

[comment]: # ({/new-a11705f0})

[comment]: # ({new-ddd381e9})
#### Execution steps执行步骤

The command/script is executed similarly on both Unix and Windows
platforms:在Unix和Windows系统平台上，指令/脚本的执行方式相近

1.  Zabbix (the parent process) creates a pipe for communication Zabbix
    (父进程)创建了一个交流通道。
2.  Zabbix sets the pipe as the output for the to-be-created child
    process Zabbix将通道设置为将要被创建的子进程的输出接口
3.  Zabbix creates the child process (runs the command/script)
    Zabbix创建子进程（运行指令/脚本）
4.  A new process group (in Unix) or a job (in Windows) is created for
    the child process
    为子进程创建一个新的进程组(Unix平台)或一个作业(Windows平台)
5.  Zabbix reads from the pipe until timeout occurs or no one is writing
    to the other end (ALL handles/file descriptors have been closed).
    Note that the child process can create more processes and exit
    before they exit or close the handle/file descriptor.
    Zabbix从管道读取，直到发生超时或没有其他写入另一端（所有处理/文件描述符都已关闭）。请注意，子进程可以在退出或关闭处理/文件描述符之前创建更多进程并退出。
6.  If the timeout has not been reached, Zabbix waits until the initial
    child process exits or timeout occurs
    如果尚未达到超时，Zabbix将等待，直到初始子进程退出或发生超时
7.  If the initial child process exited and the timeout has not been
    reached, Zabbix checks exit code of the initial child process and
    compares it to 0 (non-zero value is considered as execution failure,
    only for custom alert scripts, remote commands and user scripts
    executed on Zabbix server and Zabbix proxy)
    如果退出初始子进程并且尚未达到超时，Zabbix将检查初始子进程的退出代码并将其与0进行比较（非零值被视为执行失败，仅适用于在Zabbix
    server 和Zabbix proxy上执行自定义告警脚本，远程命令和用户脚本 ）
8.  At this point it is assumed that everything is done and the whole
    process tree (i.e. the process group or the job) is terminated
    此时，假设一切都已完成，整个过程tree（即过程组或作业）终止

::: noteimportant
Zabbix assumes that a command/script has done
processing when the initial child process has exited AND no other
process is still keeping the output handle/file descriptor open. When
processing is done, ALL created processes are terminated.
Zabbix假定命令/脚本在初始子进程退出时已完成处理，并且没有其他进程仍保持输出处理/文件描述符处于打开状态。
处理完成后，将终止所有创建的进程
:::

All double quotes and backslashes in the command are escaped with
backslashes and the command is enclosed in double
quotes.命令中的所有双引号和反斜杠都使用反斜杠进行转义，命令用双引号括起来。

#### Exit code checking退出代码的检查

Exit code are checked with the following
conditions:使用以下条件检查退出代码：

    *Only for custom alert scripts, remote commands and user scripts executed on Zabbix server and Zabbix proxy.仅适用于在Zabbix server and Zabbix proxy上执行的自定义告警脚本，远程命令和用户脚本。
    *Any exit code that is different from 0 is considered as execution failure.任何不同于0的退出代码都被视为执行失败。
    *Contents of standard error and standard output for failed executions are collected and available in frontend (where execution result is displayed).收集失败执行的标准错误和标准输出的内容，并在前端（显示执行结果）中提供。
    *Additional log entry is created for remote commands on Zabbix server to save script execution output and can be enabled using LogRemoteCommands agent [[:manual/appendix/config/zabbix_agentd|parameter]]. 为Zabbix server上的远程命令创建附加日志条目以保存脚本执行输出，并可使用LogRemoteCommands agent [[:manual/appendix/config/zabbix_agentd|parameter]] 启用。

Possible frontend messages and log entries for failed commands/scripts:
可能出现的失败指令/脚本的前端信息和日志条目：

-   Contents of standard error and standard output for failed executions
    (if any). 执行失败的标准错误和标准输出的内容（如果有的话）
-   "Process exited with code: N." (for empty output, and exit code not
    equal to 0). "进程退出代码：N."（对于空输出，退出代码不等于0）。
-   "Process killed by signal: N." (for process terminated by a signal,
    on Linux only).
    "进程被信号杀死：N."（对于由信号终止的进程，仅在Linux上）。
-   "Process terminated unexpectedly." (for process terminated for
    unknown reasons). "进程意外终止。"（由于未知原因终止进程）。

------------------------------------------------------------------------

Read more about了解更多:

-   [External
    checks](/manual/config/items/itemtypes/external#external_check_result)
-   [User parameters](/manual/config/items/userparameters)
-   [system.run](/manual/config/items/itemtypes/zabbix_agent) items
-   [Custom alert scripts](/manual/config/notifications/media/script)
-   [Remote
    commands](/manual/config/notifications/action/operation/remote_command)
-   [Global
    scripts](/manual/web_interface/frontend_sections/administration/scripts)

[comment]: # ({/new-ddd381e9})
