[comment]: # translation:outdated

[comment]: # ({new-e723903a})
# 9 时间段配置

[comment]: # ({/new-e723903a})

[comment]: # ({new-c717cede})
### 9 Setting time periods

[comment]: # ({/new-c717cede})

[comment]: # ({new-a352b97d})
#### Overview概述

To set a time period, the following format has to be
used:若要设定一个时间段，就要运用下面的格式：

    d-d,hh:mm-hh:mm

where the symbols stand for the following:符号用法：

|Symbol符号   D|scription描述|
|----------------|---------------|
|*d*|Day of the week: 1 - Monday, 2 - Tuesday ,... , 7 - Sunday 星期: 1 - Monday, 2 - Tuesday ,… , 7 - Sunday|
|*hh*|Hours: 00-24 小时: 00-24|
|*mm*|Minutes: 00-59 分钟: 00-59|

You can specify more than one time period using a semicolon (;)
separator:您可以使用分号（;）分隔符指定多个时间段：

    d-d,hh:mm-hh:mm;d-d,hh:mm-hh:mm...

Leaving the time period empty equals 01-07,00:00-24:00, which is the
default value.如果时间段的参数为空，系统将默认为01-07,00:00-24:00

::: noteimportant
The upper limit of a time period is not included.
Thus, if you specify 09:00-18:00 the last second included in the time
period is 17:59:59. This is true starting from version 1.8.7, for
everything, while [Working
time](/manual/web_interface/frontend_sections/administration/general#working_time)
has always worked this
way.时间段的上限是开区间。因此，当您指定时间段为09:00-18:00时，该时间段包含的最后一秒钟将是17:59:59。该规则从1.8.7版本之后适用于各类设定，而[Working
time](/manual/web_interface/frontend_sections/administration/general#working_time)
一直沿用该规则。
:::

#### Examples举例

Working hours. Monday - Friday from 9:00 till
18:00:工作日。星期一到星期五的9:00到18:00:

    1-5,09:00-18:00

Working hours plus weekend. Monday - Friday from 9:00 till 18:00 and
Saturday, Sunday from 10:00 till
16:00:工作日加上周末。星期一到星期五的9:00到18:00，以及周六周日的10:00到16:00。

    1-5,09:00-18:00;6-7,10:00-16:00

[comment]: # ({/new-a352b97d})
