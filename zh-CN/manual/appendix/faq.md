[comment]: # translation:outdated

[comment]: # ({new-e032d93c})
# 1 常见问题/疑难解答 赵

常见问题

1.  Q：可以更新或清空队列（如菜单“管理”→“队列”中所展示的队列）？\
    A:不可以。
2.  Q: 如何从一个数据库迁移到另一个数据库？\
    A: 只需要转存数据 (对于MySQL, 使用参数 -t 或 --no-create-info),
    用Zabbix的schema文件创建新的数据库， 并导入数据。
3.  Q:
    想用下划线替换监控项key中的所有空格，因为空格只在老版本中起作用，而在3.0版本的监控项key中，空格不是一个有效的标示符（或者因为其它需要大量修改监控项key的场景），应该如何做以及有哪些注意事项？\
    A: 可以使用数据库更新语句用下划线替换所有出现的空格:\
    update items set key\_=replace(key\_,' ','\_');\
    触发器可以使用这些监控项而不需要额外的改动，但是需要修改以下位置的监控项引用:\
    \* Notifications (actions)\
    \* Map element and link labels\
    \* Calculated item formulas
4.  Q: 我的图形中有一些点而不是线或者有一些空白区域，为什么会这样?\
    A:
    数据丢失，这种情况的发生有多种原因——Zabbix数据库的性能问题、Zabbix服务器问题、网络问题、监控设备问题...
5.  Q: Zabbix守护进程无法启动消息*监听器， 错误信息为: socket() for
    \[\[-\]:10050\] failed with error 22: Invalid argument.*\
    A:
    当在一个内核2.6.26或更低内核版本的操作系统上，试图运行编译的版本为2.6.27或更高版本的Zabbix
    agent时会产生该错误。注意，在这种情况下，静态链接不会起作用，因为早期操作系统内核版本中不支持带SOCK\_CLOEXEC标志的socket()系统调用。
    [ZBX-3395](https://support.zabbix.com/browse/ZBX-3395)
6.  Q:
    尝试使用一个位置参数（如$1）去设置一个命令中灵活的用户参数，但它不起作用。
    怎么解决这个问题？\
    A: 使用两个$$符合，像这样 **$$1**
7.  Q:
    在Opera11中，所有的下拉菜单都有一个滚动条，看起来不太美观，为什么会这样呢?\
    A: 对于Opera11.00和11.01操作系统来说，这是一个bug; 更多信息请访问
    [Zabbix 问题跟踪](https://support.zabbix.com/browse/ZBX-3594)。
8.  Q: 如何更改自定义主题中的图形背景颜色?\
    A:
    参照数据库中的graph\_theme表和[主题帮助](/manual/web_interface/theming)。
9.  Q: 调试等级为4时，在zabbix server/proxy日志中出现 "Trapper got \[\]
    len 0" 信息，这是什么原因?\
    A: 很有可能是前端有问题，连接并检查服务是否仍在运行。
10. Q: 系统时间设置为将来的某一时间，导致没有数据出现。
    这个问题怎么解决?\
    A: 清除数据库中的字段 hosts.disable\_until\*, drules.nextcheck,
    httptest.nextcheck 的值，并重启zabbix server/proxy。
11. Q: 在前端使用 *{ITEM.VALUE}* 宏
    或者在其他情况下，item的文本类型值无论多大都会被修剪为20个字符，这种情况正常吗?\
    A: 是正常的,在include/items.inc.php
    中有一个硬编码限制，长度最大仅为20个字符。

#### 另见

\* [zabbix官方问题解决版块](https://www.zabbix.org/wiki/Troubleshooting)

## 1 Frequently asked questions / Troubleshooting

Frequently asked questions or FAQ.

1.  Q: Can I flush/clear the queue (as depicted in *Administration →
    Queue*)?\
    A: No.
2.  Q: How do I migrate from one database to another?\
    A: Dump data only (for MySQL, use flag -t or --no-create-info),
    create the new database using schema files from Zabbix and import
    the data.
3.  Q: I would like to replace all spaces with underscores in my item
    keys because they worked in older versions but space is not a valid
    symbol for an item key in 3.0 (or any other reason to mass-modify
    item keys). How should I do it and what should i beware of?\
    A: You may use a database query to replace all occurrences of spaces
    in item keys with underscores:\
    update items set key\_=replace(key\_,' ','\_');\
    Triggers will be able to use these items without any additional
    modifications, but you might have to change any item references in
    these locations:\
    \* Notifications (actions)\
    \* Map element and link labels\
    \* Calculated item formulas
4.  Q: My graphs have dots instead of lines or empty areas. Why so?\
    A: Data is missing. This can happen for a variety of reasons -
    performance problems on Zabbix database, Zabbix server, network,
    monitored devices...
5.  Q: Zabbix daemons fail to start up with a message *Listener failed
    with error: socket() for \[\[-\]:10050\] failed with error 22:
    Invalid argument.*\
    A: This error arises at attempt to run Zabbix agent compiled on
    version 2.6.27 or above on a platform with a kernel 2.6.26 and
    lower. Note that static linking will not help in this case because
    it is the socket() system call that does not support SOCK\_CLOEXEC
    flag on earlier kernels.
    [ZBX-3395](https://support.zabbix.com/browse/ZBX-3395)
6.  Q: I try to set up a flexible user parameter (one that accepts
    parameters) with a command that uses a positional parameter like $1,
    but it doesn't work (uses item parameter instead). How to solve
    this?\
    A: Use a double dollar sign like **$$1**
7.  Q: All dropdowns have a scrollbar and look ugly in Opera 11. Why
    so?\
    A: It's a known bug in Opera 11.00 and 11.01; see [Zabbix issue
    tracker](https://support.zabbix.com/browse/ZBX-3594) for more
    information.
8.  Q: How can I change graph background colour in a custom theme?\
    A: See graph\_theme table in the database and [theming
    guide](/manual/web_interface/theming).
9.  Q: With DebugLevel 4 I'm seeing messages "Trapper got \[\] len 0" in
    server/proxy log - what's that?\
    A: Most likely that is frontend, connecting and checking whether
    server is still running.
10. Q: My system had the time set in the future and now no data is
    coming in. How could this be solved?\
    A: Clear values of database fields hosts.disable\_until\*,
    drules.nextcheck, httptest.nextcheck and restart the server/proxy.
11. Q: Text item values in frontend (when using *{ITEM.VALUE}* macro and
    in other cases) are cut/trimmed to 20 symbols. Is that normal?\
    A: Yes, there is a hardcoded limit in include/items.inc.php
    currently.

#### See also

\* [Troubleshooting page on
zabbix.org](https://www.zabbix.org/wiki/Troubleshooting)

[comment]: # ({/new-e032d93c})
