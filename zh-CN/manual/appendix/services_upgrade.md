[comment]: # translation:outdated

[comment]: # ({new-f9e8f670})

# 17 Service monitoring upgrade

[comment]: # ({/new-f9e8f670})

[comment]: # ({new-f21be034})

### Overview 

In Zabbix 6.0, [service monitoring](/manual/it_services) functionality has been reworked significantly (see [What's new in Zabbix 6.0.0](https://www.zabbix.com/documentation/6.0/en/manual/introduction/whatsnew600#services) for the list of changes). 

This page describes how services and SLAs defined in earlier Zabbix versions are changed during an upgrade to Zabbix 6.0 or newer.

[comment]: # ({/new-f21be034})

[comment]: # ({new-58b995d0})

### Services

In older Zabbix versions, services had two types of dependencies: soft and hard. After an upgrade, all dependencies will become equal. 

If a service "Child service" was previously linked to "Parent service 1" via hard dependency and additionally "Parent service 2" via soft dependency, after an upgrade the "Child service" will simply be linked to two parent services ("Parent service 1" and "Parent service 2").

[comment]: # ({/new-58b995d0})

[comment]: # ({new-e48d17a6})

#### Status calculation rules

The 'Status calculation algorithm' will be upgraded using the following rules:

- Do not calculate → Set status to OK
- Problem, if at least one child has a problem → Most critical of child nodes
- Problem, if all children have problems → Most critical if all children have problems

:::noteclassic
If you have upgraded from Zabbix pre-6.0 to Zabbix 6.0.0, 6.0.1 or 6.0.2, see [Known issues](https://www.zabbix.com/documentation/current/en/manual/installation/known_issues#wrong-conversion-of-services-in-zabbix-6.0.0-6.0.2) for Zabbix 6.0 documentation. 
:::

[comment]: # ({/new-e48d17a6})

[comment]: # ({new-9341f38c})

### SLAs

Previously, SLA targets had to be defined for each service separately. Since Zabbix 6.0, SLA has become a separate entity which contains information about service schedule, expected service level objective (SLO) and downtime periods to exclude from the calculation. Once configured, an SLA can be assigned to multiple services through [service tags](/manual/it_services/services_tree#tags). 

During an upgrade: 

- Identical SLAs defined for each service will be grouped and one SLA per each group will be created. 
- Each affected service will get a special tag `SLA:<ID>` and the same tag will be specified in the *Service tags* parameter of the corresponding SLA. 
- Service creation time, which is a new metric used in SLA reports, will be set to 01/01/2000 00:00 for existing services. 

[comment]: # ({/new-9341f38c})

