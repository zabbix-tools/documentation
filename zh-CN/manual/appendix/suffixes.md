[comment]: # translation:outdated

[comment]: # ({new-4d5c1b80})
# 8 单位符号说明

[comment]: # ({/new-4d5c1b80})

[comment]: # ({new-393f9290})
### 8 Unit symbols

[comment]: # ({/new-393f9290})

[comment]: # ({new-163b4f50})
#### Overview概述

Having to use some large numbers, for example '86400' to represent the
number of seconds in one day, is both difficult and error-prone. This is
why you can use some appropriate unit symbols (or suffixes) to simplify
Zabbix trigger expressions and item
keys.若使用一些大数字时，例如'86400'来表示一天中的秒数，既困难又容易出错。
这就是您可以使用一些适当的单位符号（或后缀）来简化Zabbix trigger表达式和
item key的原因。

Instead of '86400' for the number of seconds you can simply enter '1d'.
Suffixes function as
multipliers.您可以直接输入'1d'，而不是'86400'的秒数。后缀d用作乘数。

[comment]: # ({/new-163b4f50})

[comment]: # ({new-11368220})
#### Time suffixes 时间后缀

For time you can use您可使用如下:

-   **s** - seconds (when used, works the same as the raw value)
    秒（使用时，与原始值相同）
-   **m** - minutes 分
-   **h** - hours 时
-   **d** - days 天
-   **w** - weeks 周

Time suffixes are supported in:以下支持时间后缀：

-   trigger [expression](/manual/config/triggers/expression) constants
    and function parameters 触发器
    [expression](/manual/config/triggers/expression) 常量和函数参数
-   item configuration ('Update interval', 'Custom intervals', 'History
    storage period' and 'Trend storage period' fields)
    监控项配置（'更新间隔'，'自定义时间间隔'，'历史数据保留时长'和'趋势存储时间'字段）
-   item prototype configuration ('Update interval', 'Custom intervals',
    'History storage period' and 'Trend storage period' fields)
    监控项原型配置（'更新间隔'，'自定义时间间隔'，'历史数据保留时长'和'趋势存储时间'字段）
-   low-level discovery rule configuration ('Update interval', 'Custom
    intervals', 'Keep lost resources' fields)
    低级别发现规则配置（'更新间隔'，'自定义时间间隔'，'资源周期不足'字段）
-   network discovery configuration ('Update interval' field)
    网络发现规则配置（'更新间隔'字段）
-   web scenario configuration ('Update interval', 'Timeout' fields) web
    scenario配置（'更新间隔'，'超时'字段）
-   action operation configuration ('Default operation step duration',
    'Step duration' fields)
    动作操作配置（'默认操作步骤持续时间'，'步骤持续时间'字段）
-   slide show configuration ('Default delay' field)
    幻灯片展示配置（'默认延迟'字段）
-   user profile settings ('Auto-logout', 'Refresh', 'Message timeout'
    fields) 用户基本资料配置（'自动登录'，'刷新'，'消息超时'字段）
-   *Administration* → *General* → *Housekeeping* (storage period
    fields) *管理* → *一般* → *管家* ('存储期'字段)
-   *Administration* → *General* → *Trigger displaying options*
    ('Display OK triggers for', 'On status change triggers blink for'
    fields) *管理* → *一般* → *触发器显示选项*
    ('显示OK触发器于'，'于状态改变时, 触发器因此闪烁于'字段)
-   *Administration* → *General* → *Other* ('Refresh unsupported items'
    field) *管理* → *一般* → *其他* ('刷新不支持的项目'字段)
-   parameters of the **zabbix\[queue,<from>,<to>\]**
    [internal item](/manual/config/items/itemtypes/internal) 参数
    **zabbix\[queue,<from>,<to>\]** [internal
    item](/manual/config/items/itemtypes/internal)
-   last parameter of [aggregate
    checks](/manual/config/items/itemtypes/aggregate) [aggregate
    checks](/manual/config/items/itemtypes/aggregate)最后一个参数

[comment]: # ({/new-11368220})

[comment]: # ({new-c9f73444})
#### Memory suffixes内存后缀

Memory size suffixes are supported in trigger
[expression](/manual/config/triggers/expression) constants and function
parameters.触发器[expression](/manual/config/triggers/expression)
常量和函数参数支持内存大小后缀。

For memory size you can use对于内存大小，用法如下:

-   **K** - kilobyte千字节
-   **M** - megabyte兆字节
-   **G** - gigabyte十亿字节
-   **T** - terabyte兆兆字节

[comment]: # ({/new-c9f73444})

[comment]: # ({new-46f2ffdf})
#### Other uses其他用法

Unit symbols are also used for a human-readable representation of data
in the frontend.单位符号还用于前端数据，人们可读的表示法。

In both Zabbix server and frontend these symbols are
supported:在Zabbix服务器和前端都支持这些符号：

-   **K** - kilo
-   **M** - mega
-   **G** - giga
-   **T** - tera

When item values in B, Bps are displayed in the frontend, base 2 is
applied (1K = 1024). Otherwise a base of 10 is used (1K =
1000).当B，Bps中的监控项值显示在前端时，应用基数2（1K = 1024），
否则使用10的基数（1K = 1000）。

Additionally the frontend also supports the display
of:此外，前端还支持以下显示：

-   **P** - peta
-   **E** - exa
-   **Z** - zetta
-   **Y** - yotta

#### Usage examples用法示例

By using some appropriate suffixes you can write trigger expressions
that are easier to understand and maintain, for example these
expressions:
通过使用一些适当的后缀，您可以编写更易于理解和维护的触发器表达式，例如以下表达式：

    {host:zabbix[proxy,zabbix_proxy,lastaccess]}>120
    {host:system.uptime[].last()}<86400
    {host:system.cpu.load.avg(600)}<10
    {host:vm.memory.size[available].last()}<20971520

could be changed to: 可以改为：

    {host:zabbix[proxy,zabbix_proxy,lastaccess]}>2m
    {host:system.uptime.last()}<1d
    {host:system.cpu.load.avg(10m)}<10
    {host:vm.memory.size[available].last()}<20M

[comment]: # ({/new-46f2ffdf})
