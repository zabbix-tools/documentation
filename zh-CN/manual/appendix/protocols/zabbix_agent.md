[comment]: # translation:outdated

[comment]: # ({new-a175d7cd})
# 2 Zabbix代理协议

有关详细信息，请参阅[被动和主动代理检查](手册：附录：项目：activepassive)页面。

#### 2 Zabbix agent protocol

Please refer to [Passive and active agent
checks](/manual/appendix/items/activepassive) page for more information.

[comment]: # ({/new-a175d7cd})
