[comment]: # translation:outdated

[comment]: # ({new-df0deee4})
# 3 Zabbix sender 协议

有关详细信息，请参阅[Trapper
items](/manual/appendix/items/trapper)页面。

#### 3 Zabbix sender protocol

Please refer to [Trapper items](/manual/appendix/items/trapper) page for
more information.

[comment]: # ({/new-df0deee4})
