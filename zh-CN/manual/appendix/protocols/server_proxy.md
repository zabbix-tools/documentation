[comment]: # translation:outdated

[comment]: # ({new-7669b1c2})
# 1 Server-proxy 数据交换协议

[comment]: # ({/new-7669b1c2})

[comment]: # ({new-7382efcd})
#### 概述

Server-proxy 数据交换基于JSON格式。

请求和响应消息必须以[header and data
length](/manual/appendix/protocols/header_datalen)开头

[comment]: # ({/new-7382efcd})

[comment]: # ({new-14661e11})
#### 被动代理

[comment]: # ({/new-14661e11})

[comment]: # ({new-fac082ad})
##### 代理配置请求

`proxy config` 请求由服务器发送以提供代理配置数据。 每次发送此请求
`ProxyConfigFrequency` （服务器配置参数）秒。

|name|<|<|<|value type|description|
|----|-|-|-|----------|-----------|
|server→proxy:|<|<|<|<|<|
|**request**|<|<|<|*string*|'proxy config'|
|**<table>**|<|<|<|*object*|one or more objects with <table> data|
|<|**fields**|<|<|*array*|array of field names|
|<|<|\-|<|*string*|field name|
|<|**data**|<|<|*array*|array of rows|
|<|<|\-|<|*array*|array of columns|
|<|<|<|\-|*string*,*number*|column value with type depending on column type in database schema|
|proxy→server:|<|<|<|<|<|
|**response**|<|<|<|*string*|the request success information ('success' or 'failed')|
|**version**|<|<|<|*string*|the proxy version (<major>.<minor>.<build>)|

例：

server→proxy:

``` {.javascript}
{
    "request": "proxy config",
    "globalmacro":{
        "fields":[
            "globalmacroid",
            "macro",
            "value"
        ],
        "data":[
            [
                2,
                "{$SNMP_COMMUNITY}",
                "public"
            ]
        ]
    },
    "hosts":{
        "fields":[
            "hostid",
            "host",
            "status",
            "ipmi_authtype",
            "ipmi_privilege",
            "ipmi_username",
            "ipmi_password",
            "name",
            "tls_connect",
            "tls_accept",
            "tls_issuer",
            "tls_subject",
            "tls_psk_identity",
            "tls_psk"
        ],
        "data":[
            [
                10001,
                "Template OS Linux",
                3,
                -1,
                2,
                "",
                "",
                "Template OS Linux",
                1,
                1,
                "",
                "",
                "",
                ""
            ],
            [
                10050,
                "Template App Zabbix Agent",
                3,
                -1,
                2,
                "",
                "",
                "Template App Zabbix Agent",
                1,
                1,
                "",
                "",
                "",
                ""
            ],
            [
                10105,
                "Logger",
                0,
                -1,
                2,
                "",
                "",
                "Logger",
                1,
                1,
                "",
                "",
                "",
                ""
            ]
        ]
    },
    "interface":{
        "fields":[
            "interfaceid",
            "hostid",
            "main",
            "type",
            "useip",
            "ip",
            "dns",
            "port",
            "bulk"
        ],
        "data":[
            [
                2,
                10105,
                1,
                1,
                1,
                "127.0.0.1",
                "",
                "10050",
                1
            ]
        ]
    },
    ...
}
```

proxy→server:

``` {.javascript}
{
  "response": "success",
  "version": "3.4.0"
}
```

[comment]: # ({/new-fac082ad})

[comment]: # ({new-c348e5d9})
##### 代理请求

`proxy data` request用于从代理获取主机可用性，历史，发现和自动注册数据。
每次发送此请求 `ProxyDataFrequency` （服务器配置参数）秒。

|name|<|value type|description|
|----|-|----------|-----------|
|server→proxy:|<|<|<|
|**request**|<|*string*|'proxy data'|
|proxy→server:|<|<|<|
|**host availability**|<|*array*|*(optional)* array of host availability data objects|
|<|**hostid**|*number*|host identifier|
|<|**available**|*number*|Zabbix agent availability<br><br>**0**, *HOST\_AVAILABLE\_UNKNOWN* - unknown<br>**1**, *HOST\_AVAILABLE\_TRUE* - available<br>**2**, *HOST\_AVAILABLE\_FALSE* - unavailable|
|<|**error**|*string*|Zabbix agent error message or empty string|
|<|**snmp\_available**|*number*|SNMP agent availability<br><br>**0**, *HOST\_AVAILABLE\_UNKNOWN* - unknown<br>**1**, *HOST\_AVAILABLE\_TRUE* - available<br>**2**, *HOST\_AVAILABLE\_FALSE* - unavailable|
|<|**snmp\_error**|*string*|SNMP agent error message or empty string|
|<|**ipmi\_available**|*number*|IPMI agent availability<br><br>**0**, *HOST\_AVAILABLE\_UNKNOWN* - unknown<br>**1**, *HOST\_AVAILABLE\_TRUE* - available<br>**2**, *HOST\_AVAILABLE\_FALSE* - unavailable|
|<|**ipmi\_error**|*string*|IPMI agent error message or empty string|
|<|**jmx\_available**|*number*|JMX agent availability<br><br>**0**, *HOST\_AVAILABLE\_UNKNOWN* - unknown<br>**1**, *HOST\_AVAILABLE\_TRUE* - available<br>**2**, *HOST\_AVAILABLE\_FALSE* - unavailable|
|<|**jmx\_error**|*string*|JMX agent error message or empty string|
|**history data**|<|*array*|*(optional)* array of history data objects|
|<|**itemid**|*number*|item identifier|
|<|**clock**|*number*|item value timestamp (seconds)|
|<|**ns**|*number*|item value timestamp (nanoseconds)|
|<|**value**|*string*|*(optional)* item value|
|<|**timestamp**|*number*|*(optional)* timestamp of log type items|
|<|**source**|*string*|*(optional)* eventlog item source value|
|<|**severity**|*number*|*(optional)* eventlog item severity value|
|<|**eventid**|*number*|*(optional)* eventlog item eventid value|
|<|**state**|*string*|*(optional)* item state<br>**0**, *ITEM\_STATE\_NORMAL*<br>**1**, *ITEM\_STATE\_NOTSUPPORTED*|
|<|**lastlogsize**|*number*|*(optional)* last logs ize of log type items|
|<|**mtime**|*number*|*(optional)* modify time of log type items|
|**discovery data**|<|*array*|*(optional)* array of discovery data objects|
|<|**clock**|*number*|the discovery data timestamp|
|<|**druleid**|*number*|the discovery rule identifier|
|<|**dcheckid**|*number*|the discovery check indentifier or null for discovery rule data|
|<|**type**|*number*|the discovery check type:<br><br>**-1** discovery rule data<br>**0**, *SVC\_SSH* - SSH service check<br>**1**, *SVC\_LDAP* - LDAP service check<br>**2**, *SVC\_SMTP* - SMTP service check<br>**3**, *SVC\_FTP* - FTP service check<br>**4**, *SVC\_HTTP* - HTTP service check<br>**5**, *SVC\_POP* - POP service check<br>**6**, *SVC\_NNTP* - NNTP service check<br>**7**, *SVC\_IMAP* - IMAP service check<br>**8**, *SVC\_TCP* - TCP port availability check<br>**9**, *SVC\_AGENT* - Zabbix agent<br>**10**, *SVC\_SNMPv1* - SNMPv1 agent<br>**11**, *SVC\_SNMPv2* - SNMPv2 agent<br>**12**, *SVC\_ICMPPING* - ICMP ping<br>**13**, *SVC\_SNMPv3* - SNMPv3 agent<br>**14**, *SVC\_HTTPS* - HTTPS service check<br>**15**, *SVC\_TELNET* - Telnet availability check|
|<|**ip**|*string*|the host IP address|
|<|**dns**|*string*|the host DNS name|
|<|**port**|*number*|*(optional)* service port number|
|<|**key\_**|*string*|*(optional)* the item key for discovery check of type **9** *SVC\_AGENT*|
|<|**value**|*string*|*(optional)* value received from the service, can be empty for most of services|
|<|**status**|*number*|*(optional)* service status:<br><br>**0**, *DOBJECT\_STATUS\_UP* - Service UP<br>**1**, *DOBJECT\_STATUS\_DOWN* - Service DOWN|
|**auto registration**|<|*array*|*(optional)* array of auto registration data objects|
|<|**clock**|*number*|the auto registration data timestamp|
|<|**host**|*string*|the host name|
|<|**ip**|*string*|*(optional)* the host IP address|
|<|**dns**|*string*|*(optional)* the resolved DNS name from IP address|
|<|**port**|*string*|*(optional)* the host port|
|<|**host\_metadata**|*string*|*(optional)* the host metadata sent by agent (based on HostMetadata or HostMetadataItem agent configuration parameter)|
|**tasks**|<|*array*|*(optional)* array of tasks|
|<|**type**|*number*|the task type:<br><br>**0**, *ZBX\_TM\_TASK\_PROCESS\_REMOTE\_COMMAND\_RESULT* - remote command result|
|<|**status**|*number*|the remote command execution status:<br><br>**0**, *ZBX\_TM\_REMOTE\_COMMAND\_COMPLETED* - the remote command completed successfully<br>**1**, *ZBX\_TM\_REMOTE\_COMMAND\_FAILED* - the remote command failed|
|<|**error**|*string*|*(optional)* the error message|
|<|**parent\_taskid**|*number*|the parent task id|
|**more**|<|*number*|*(optional)* 1 - there are more history data to send|
|**clock**|<|*number*|data transfer timestamp (seconds)|
|**ns**|<|*number*|data transfer timestamp (nanoseconds)|
|**version**|<|*string*|the proxy version (<major>.<minor>.<build>)|
|server→proxy:|<|<|<|
|**response**|<|*string*|the request success information ('success' or 'failed')|
|**tasks**|<|*array*|*(optional)* array of tasks|
|<|**type**|*number*|the task type:<br><br>**1**, *ZBX\_TM\_TASK\_PROCESS\_REMOTE\_COMMAND* - remote command|
|<|**clock**|*number*|the task creation time|
|<|**ttl**|*number*|the time in seconds after which task expires|
|<|**commandtype**|*number*|the remote command type:<br><br>**0**, *ZBX\_SCRIPT\_TYPE\_CUSTOM\_SCRIPT* - use custom script<br>**1**, *ZBX\_SCRIPT\_TYPE\_IPMI* - use IPMI<br>**2**, *ZBX\_SCRIPT\_TYPE\_SSH* - use SSH<br>**3**, *ZBX\_SCRIPT\_TYPE\_TELNET* - use Telnet<br>**4**, *ZBX\_SCRIPT\_TYPE\_GLOBAL\_SCRIPT* - use global script (currently functionally equivalent to custom script)|
|<|**command**|*string*|the remote command to execute|
|<|**execute\_on**|*number*|the execution target for custom scripts:<br><br>**0**, *ZBX\_SCRIPT\_EXECUTE\_ON\_AGENT* - execute script on agent<br>**1**, *ZBX\_SCRIPT\_EXECUTE\_ON\_SERVER* - execute script on server<br>**2**, *ZBX\_SCRIPT\_EXECUTE\_ON\_PROXY* - execute script on proxy|
|<|**port**|*number*|*(optional)* the port for telnet and ssh commands|
|<|**authtype**|*number*|*(optional)* the authentication type for ssh commands|
|<|**username**|*string*|*(optional)* the user name for telnet and ssh commands|
|<|**password**|*string*|*(optional)* the password for telnet and ssh commands|
|<|**publickey**|*string*|*(optional)* the public key for ssh commands|
|<|**privatekey**|*string*|*(optional)* the private key for ssh commands|
|<|**parent\_taskid**|*number*|the parent task id|
|<|**hostid**|*number*|target hostid|

例如:

server→proxy:

``` {.javascript}
{
  "request": "proxy data"
}
```

proxy→server:

``` {.javascript}
{
    "host availability":[
        {
            "hostid":10106,
            "available":1,
            "error":"",
            "snmp_available":0,
            "snmp_error":"",
            "ipmi_available":0,
            "ipmi_error":"",
            "jmx_available":0,
            "jmx_error":""
        },
        {
            "hostid":10107,
            "available":1,
            "error":"",
            "snmp_available":0,
            "snmp_error":"",
            "ipmi_available":0,
            "ipmi_error":"",
            "jmx_available":0,
            "jmx_error":""
        }
    ],
    "history data":[
        {
            "itemid":"12345",
            "clock":1478609647,
            "ns":332510044,
            "value":"52956612"
        },
        {
            "itemid":"12346",
            "clock":1478609647,
            "ns":330690279,
            "state":1,
            "value":"Cannot find information for this network interface in /proc/net/dev."
        }
    ],
    "discovery data":[
        {
            "clock":1478608764,
            "drule":2,
            "dcheck":3,
            "type":12,
            "ip":"10.3.0.10",
            "dns":"vdebian",
            "status":1
        },
        {
            "clock":1478608764,
            "drule":2,
            "dcheck":null,
            "type":-1,
            "ip":"10.3.0.10",
            "dns":"vdebian",
            "status":1
        }
    ],
    "auto registration":[
        {
            "clock":1478608371,
            "host":"Logger1",
            "ip":"10.3.0.1",
            "dns":"localhost",
            "port":"10050"
        },
        {
            "clock":1478608381,
            "host":"Logger2",
            "ip":"10.3.0.2",
            "dns":"localhost",
            "port":"10050"
        }
    ],
    "tasks":[
        {
            "type": 0,
            "status": 0,
            "parent_taskid": 10
        },
        {
            "type": 0,
            "status": 1,
            "error": "No permissions to execute task.",
            "parent_taskid": 20
        }
    ],    
    "clock":1478609648,
    "ns":157729208,
    "version":"3.4.0"
}
```

server→proxy:

``` {.javascript}
{
  "response": "success",
  "tasks":[
      {
         "type": 1,
         "clock": 1478608371,
         "ttl": 600,
         "commandtype": 2,
         "command": "restart_service1.sh",
         "execute_on": 2,
         "port": 80,
         "authtype": 0,
         "username": "userA",
         "password": "password1",
         "publickey": "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCqGKukO1De7zhZj6+H0qtjTkVxwTCpvKe",
         "privatekey": "lsuusFncCzWBQ7RKNUSesmQRMSGkVb1/3j+skZ6UtW+5u09lHNsj6tQ5QCqGKukO1De7zhd",
         "parent_taskid": 10,
         "hostid": 10070
      },
      {
         "type": 1,
         "clock": 1478608381,
         "ttl": 600,
         "commandtype": 1,
         "command": "restart_service2.sh",
         "execute_on": 0,
         "authtype": 0,
         "username": "",
         "password": "",
         "publickey": "",
         "privatekey": "",
         "parent_taskid": 20,
         "hostid": 10084
      }
  ]
}
```

[comment]: # ({/new-c348e5d9})

[comment]: # ({new-967ea060})
#### 主动代理

[comment]: # ({/new-967ea060})


[comment]: # ({new-bed30a3b})
##### 代理配置请求

`proxy config` 请求由代理发送以获取代理配置数据。 每次发送此请求
`ConfigFrequency` （代理配置参数）秒。

|name|<|<|<|value type|description|
|----|-|-|-|----------|-----------|
|proxy→server:|<|<|<|<|<|
|**request**|<|<|<|*string*|'proxy config'|
|**host**|<|<|<|*string*|proxy name|
|**version**|<|<|<|*string*|the proxy version (<major>.<minor>.<build>)|
|server→proxy:|<|<|<|<|<|
|**request**|<|<|<|*string*|'proxy config'|
|**<table>**|<|<|<|*object*|one or more objects with <table> data|
|<|**fields**|<|<|*array*|array of field names|
|<|<|\-|<|*string*|field name|
|<|**data**|<|<|*array*|array of rows|
|<|<|\-|<|*array*|array of columns|
|<|<|<|\-|*string*,*number*|column value with type depending on column type in database schema|
|proxy→server:|<|<|<|<|<|
|**response**|<|<|<|*string*|the request success information ('success' or 'failed')|

Example:

proxy→server:

``` {.javascript}
{
  "request": "proxy config",
  "host": "Proxy #12",
  "version":"3.4.0"
}
```

server→proxy:

``` {.javascript}
{
    "globalmacro":{
        "fields":[
            "globalmacroid",
            "macro",
            "value"
        ],
        "data":[
            [
                2,
                "{$SNMP_COMMUNITY}",
                "public"
            ]
        ]
    },
    "hosts":{
        "fields":[
            "hostid",
            "host",
            "status",
            "ipmi_authtype",
            "ipmi_privilege",
            "ipmi_username",
            "ipmi_password",
            "name",
            "tls_connect",
            "tls_accept",
            "tls_issuer",
            "tls_subject",
            "tls_psk_identity",
            "tls_psk"
        ],
        "data":[
            [
                10001,
                "Template OS Linux",
                3,
                -1,
                2,
                "",
                "",
                "Template OS Linux",
                1,
                1,
                "",
                "",
                "",
                ""
            ],
            [
                10050,
                "Template App Zabbix Agent",
                3,
                -1,
                2,
                "",
                "",
                "Template App Zabbix Agent",
                1,
                1,
                "",
                "",
                "",
                ""
            ],
            [
                10105,
                "Logger",
                0,
                -1,
                2,
                "",
                "",
                "Logger",
                1,
                1,
                "",
                "",
                "",
                ""
            ]
        ]
    },
    "interface":{
        "fields":[
            "interfaceid",
            "hostid",
            "main",
            "type",
            "useip",
            "ip",
            "dns",
            "port",
            "bulk"
        ],
        "data":[
            [
                2,
                10105,
                1,
                1,
                1,
                "127.0.0.1",
                "",
                "10050",
                1
            ]
        ]
    },
    ...
}
```

proxy→server:

``` {.javascript}
{
  "response": "success"
}
```

[comment]: # ({/new-bed30a3b})

[comment]: # ({new-ef0ba621})
##### 代理数据请求

`proxy data`
请求由代理发送，以提供主机可用性，历史记录，发现和自动注册数据。
每次发送此请求 `DataSenderFrequency` （代理配置参数）秒。

|name|<|value type|description|
|----|-|----------|-----------|
|proxy→server:|<|<|<|
|**request**|<|*string*|'proxy data'|
|**host**|<|*string*|the proxy name|
|**host availability**|<|*array*|*(optional)* array of host availability data objects|
|<|**hostid**|*number*|host identifier|
|<|**available**|*number*|Zabbix agent availability<br><br>**0**, *HOST\_AVAILABLE\_UNKNOWN* - unknown<br>**1**, *HOST\_AVAILABLE\_TRUE* - available<br>**2**, *HOST\_AVAILABLE\_FALSE* - unavailable|
|<|**error**|*string*|Zabbix agent error message or empty string|
|<|**snmp\_available**|*number*|SNMP agent availability<br><br>**0**, *HOST\_AVAILABLE\_UNKNOWN* - unknown<br>**1**, *HOST\_AVAILABLE\_TRUE* - available<br>**2**, *HOST\_AVAILABLE\_FALSE* - unavailable|
|<|**snmp\_error**|*string*|SNMP agent error message or empty string|
|<|**ipmi\_available**|*number*|IPMI agent availability<br><br>**0**, *HOST\_AVAILABLE\_UNKNOWN* - unknown<br>**1**, *HOST\_AVAILABLE\_TRUE* - available<br>**2**, *HOST\_AVAILABLE\_FALSE* - unavailable|
|<|**ipmi\_error**|*string*|IPMI agent error message or empty string|
|<|**jmx\_available**|*number*|JMX agent availability<br><br>**0**, *HOST\_AVAILABLE\_UNKNOWN* - unknown<br>**1**, *HOST\_AVAILABLE\_TRUE* - available<br>**2**, *HOST\_AVAILABLE\_FALSE* - unavailable|
|<|**jmx\_error**|*string*|JMX agent error message or empty string|
|**history data**|<|*array*|*(optional)* array of history data objects|
|<|**itemid**|*number*|item identifier|
|<|**clock**|*number*|item value timestamp (seconds)|
|<|**ns**|*number*|item value timestamp (nanoseconds)|
|<|**value**|*string*|*(optional)* item value|
|<|**timestamp**|*number*|*(optional)* timestamp of log type items|
|<|**source**|*string*|*(optional)* eventlog item source value|
|<|**severity**|*number*|*(optional)* eventlog item severity value|
|<|**eventid**|*number*|*(optional)* eventlog item eventid value|
|<|**state**|*string*|*(optional)* item state<br>**0**, *ITEM\_STATE\_NORMAL*<br>**1**, *ITEM\_STATE\_NOTSUPPORTED*|
|<|**lastlogsize**|*number*|*(optional)* last logs ize of log type items|
|<|**mtime**|*number*|*(optional)* modify time of log type items|
|**discovery data**|<|*array*|*(optional)* array of discovery data objects|
|<|**clock**|*number*|the discovery data timestamp|
|<|**druleid**|*number*|the discovery rule identifier|
|<|**dcheckid**|*number*|the discovery check indentifier or null for discovery rule data|
|<|**type**|*number*|the discovery check type:<br><br>**-1** discovery rule data<br>**0**, *SVC\_SSH* - SSH service check<br>**1**, *SVC\_LDAP* - LDAP service check<br>**2**, *SVC\_SMTP* - SMTP service check<br>**3**, *SVC\_FTP* - FTP service check<br>**4**, *SVC\_HTTP* - HTTP service check<br>**5**, *SVC\_POP* - POP service check<br>**6**, *SVC\_NNTP* - NNTP service check<br>**7**, *SVC\_IMAP* - IMAP service check<br>**8**, *SVC\_TCP* - TCP port availability check<br>**9**, *SVC\_AGENT* - Zabbix agent<br>**10**, *SVC\_SNMPv1* - SNMPv1 agent<br>**11**, *SVC\_SNMPv2* - SNMPv2 agent<br>**12**, *SVC\_ICMPPING* - ICMP ping<br>**13**, *SVC\_SNMPv3* - SNMPv3 agent<br>**14**, *SVC\_HTTPS* - HTTPS service check<br>**15**, *SVC\_TELNET* - Telnet availability check|
|<|**ip**|*string*|the host IP address|
|<|**dns**|*string*|the host DNS name|
|<|**port**|*number*|*(optional)* service port number|
|<|**key\_**|*string*|*(optional)* the item key for discovery check of type **9** *SVC\_AGENT*|
|<|**value**|*string*|*(optional)* value received from the service, can be empty for most of services|
|<|**status**|*number*|*(optional)* service status:<br><br>**0**, *DOBJECT\_STATUS\_UP* - Service UP<br>**1**, *DOBJECT\_STATUS\_DOWN* - Service DOWN|
|**auto registration**|<|*array*|*(optional)* array of auto registration data objects|
|<|**clock**|*number*|the auto registration data timestamp|
|<|**host**|*string*|the host name|
|<|**ip**|*string*|*(optional)* the host IP address|
|<|**dns**|*string*|*(optional)* the resolved DNS name from IP address|
|<|**port**|*string*|*(optional)* the host port|
|<|**host\_metadata**|*string*|*(optional)* the host metadata sent by agent (based on HostMetadata or HostMetadataItem agent configuration parameter)|
|**tasks**|<|*array*|*(optional)* array of tasks|
|<|**type**|*number*|the task type:<br><br>**0**, *ZBX\_TM\_TASK\_PROCESS\_REMOTE\_COMMAND\_RESULT* - remote command result|
|<|**status**|*number*|the remote command execution status:<br><br>**0**, *ZBX\_TM\_REMOTE\_COMMAND\_COMPLETED* - the remote command completed successfully<br>**1**, *ZBX\_TM\_REMOTE\_COMMAND\_FAILED* - the remote command failed|
|<|**error**|*string*|*(optional)* the error message|
|<|**parent\_taskid**|*number*|the parent task id|
|**more**|<|*number*|*(optional)* 1 - there are more history data to send|
|**clock**|<|*number*|data transfer timestamp (seconds)|
|**ns**|<|*number*|data transfer timestamp (nanoseconds)|
|**version**|<|*string*|the proxy version (<major>.<minor>.<build>)|
|server→proxy:|<|<|<|
|**response**|<|*string*|the request success information ('success' or 'failed')|
|**tasks**|<|*array*|*(optional)* array of tasks|
|<|**type**|*number*|the task type:<br><br>**1**, *ZBX\_TM\_TASK\_PROCESS\_REMOTE\_COMMAND* - remote command|
|<|**clock**|*number*|the task creation time|
|<|**ttl**|*number*|the time in seconds after which task expires|
|<|**commandtype**|*number*|the remote command type:<br><br>**0**, *ZBX\_SCRIPT\_TYPE\_CUSTOM\_SCRIPT* - use custom script<br>**1**, *ZBX\_SCRIPT\_TYPE\_IPMI* - use IPMI<br>**2**, *ZBX\_SCRIPT\_TYPE\_SSH* - use SSH<br>**3**, *ZBX\_SCRIPT\_TYPE\_TELNET* - use Telnet<br>**4**, *ZBX\_SCRIPT\_TYPE\_GLOBAL\_SCRIPT* - use global script (currently functionally equivalent to custom script)|
|<|**command**|*string*|the remote command to execute|
|<|**execute\_on**|*number*|the execution target for custom scripts:<br><br>**0**, *ZBX\_SCRIPT\_EXECUTE\_ON\_AGENT* - execute script on agent<br>**1**, *ZBX\_SCRIPT\_EXECUTE\_ON\_SERVER* - execute script on server<br>**2**, *ZBX\_SCRIPT\_EXECUTE\_ON\_PROXY* - execute script on proxy|
|<|**port**|*number*|*(optional)* the port for telnet and ssh commands|
|<|**authtype**|*number*|*(optional)* the authentication type for ssh commands|
|<|**username**|*string*|*(optional)* the user name for telnet and ssh commands|
|<|**password**|*string*|*(optional)* the password for telnet and ssh commands|
|<|**publickey**|*string*|*(optional)* the public key for ssh commands|
|<|**privatekey**|*string*|*(optional)* the private key for ssh commands|
|<|**parent\_taskid**|*number*|the parent task id|
|<|**hostid**|*number*|target hostid|

例如:

proxy→server:

``` {.javascript}
{
  "request": "proxy data",
  "host": "Proxy #12", 
    "host availability":[
        {
            "hostid":10106,
            "available":1,
            "error":"",
            "snmp_available":0,
            "snmp_error":"",
            "ipmi_available":0,
            "ipmi_error":"",
            "jmx_available":0,
            "jmx_error":""
        },
        {
            "hostid":10107,
            "available":1,
            "error":"",
            "snmp_available":0,
            "snmp_error":"",
            "ipmi_available":0,
            "ipmi_error":"",
            "jmx_available":0,
            "jmx_error":""
        }
    ],
    "history data":[
        {
            "itemid":"12345",
            "clock":1478609647,
            "ns":332510044,
            "value":"52956612"
        },
        {
            "itemid":"12346",
            "clock":1478609647,
            "ns":330690279,
            "state":1,
            "value":"Cannot find information for this network interface in /proc/net/dev."
        }
    ],
    "discovery data":[
        {
            "clock":1478608764,
            "drule":2,
            "dcheck":3,
            "type":12,
            "ip":"10.3.0.10",
            "dns":"vdebian",
            "status":1
        },
        {
            "clock":1478608764,
            "drule":2,
            "dcheck":null,
            "type":-1,
            "ip":"10.3.0.10",
            "dns":"vdebian",
            "status":1
        }
    ],
    "auto registration":[
        {
            "clock":1478608371,
            "host":"Logger1",
            "ip":"10.3.0.1",
            "dns":"localhost",
            "port":"10050"
        },
        {
            "clock":1478608381,
            "host":"Logger2",
            "ip":"10.3.0.2",
            "dns":"localhost",
            "port":"10050"
        }
    ],
    "tasks":[
        {
            "type": 2,
            "clock":1478608371,
            "ttl": 600,
            "commandtype": 2,
            "command": "restart_service1.sh",
            "execute_on": 2,
            "port": 80,
            "authtype": 0,
            "username": "userA",
            "password": "password1",
            "publickey": "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCqGKukO1De7zhZj6+H0qtjTkVxwTCpvKe",
            "privatekey": "lsuusFncCzWBQ7RKNUSesmQRMSGkVb1/3j+skZ6UtW+5u09lHNsj6tQ5QCqGKukO1De7zhd",
            "parent_taskid": 10,
            "hostid": 10070
        },
        {
            "type": 2,
            "clock":1478608381,
            "ttl": 600,
            "commandtype": 1,
            "command": "restart_service2.sh",
            "execute_on": 0,
            "authtype": 0,
            "username": "",
            "password": "",
            "publickey": "",
            "privatekey": "",
            "parent_taskid": 20,
            "hostid": 10084
        }
    ],
    "tasks":[
        {
            "type": 0,
            "status": 0,
            "parent_taskid": 10
        },
        {
            "type": 0,
            "status": 1,
            "error": "No permissions to execute task.",
            "parent_taskid": 20
        }
    ], 
    "clock":1478609648,
    "ns":157729208,
    "version":"3.4.0"
}
```

server→proxy:

``` {.javascript}
{
  "response": "success",
  "tasks":[
      {
         "type": 1,
         "clock": 1478608371,
         "ttl": 600,
         "commandtype": 2,
         "command": "restart_service1.sh",
         "execute_on": 2,
         "port": 80,
         "authtype": 0,
         "username": "userA",
         "password": "password1",
         "publickey": "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCqGKukO1De7zhZj6+H0qtjTkVxwTCpvKe",
         "privatekey": "lsuusFncCzWBQ7RKNUSesmQRMSGkVb1/3j+skZ6UtW+5u09lHNsj6tQ5QCqGKukO1De7zhd",
         "parent_taskid": 10,
         "hostid": 10070
      },
      {
         "type": 1,
         "clock": 1478608381,
         "ttl": 600,
         "commandtype": 1,
         "command": "restart_service2.sh",
         "execute_on": 0,
         "authtype": 0,
         "username": "",
         "password": "",
         "publickey": "",
         "privatekey": "",
         "parent_taskid": 20,
         "hostid": 10084
      }
  ]
}
```

#### 向后兼容性

服务器通过接受旧的支持部分向后兼容 `host availability`, `history data`,
`discovery data` and `auto registration` 请求.

### 1 Server-proxy data exchange protocol

#### Overview

Server - proxy data exchange is based on JSON format.

Request and response messages must begin with [header and data
length](/manual/appendix/protocols/header_datalen).

#### Passive proxy

##### Proxy config request

The `proxy config` request is sent by server to provide proxy
configuration data. This request is sent every `ProxyConfigFrequency`
(server configuration parameter) seconds.

|name|<|<|<|value type|description|
|----|-|-|-|----------|-----------|
|server→proxy:|<|<|<|<|<|
|**request**|<|<|<|*string*|'proxy config'|
|**<table>**|<|<|<|*object*|one or more objects with <table> data|
|<|**fields**|<|<|*array*|array of field names|
|<|<|\-|<|*string*|field name|
|<|**data**|<|<|*array*|array of rows|
|<|<|\-|<|*array*|array of columns|
|<|<|<|\-|*string*,*number*|column value with type depending on column type in database schema|
|proxy→server:|<|<|<|<|<|
|**response**|<|<|<|*string*|the request success information ('success' or 'failed')|
|**version**|<|<|<|*string*|the proxy version (<major>.<minor>.<build>)|

Example:

server→proxy:

``` {.javascript}
{
    "request": "proxy config",
    "globalmacro":{
        "fields":[
            "globalmacroid",
            "macro",
            "value"
        ],
        "data":[
            [
                2,
                "{$SNMP_COMMUNITY}",
                "public"
            ]
        ]
    },
    "hosts":{
        "fields":[
            "hostid",
            "host",
            "status",
            "ipmi_authtype",
            "ipmi_privilege",
            "ipmi_username",
            "ipmi_password",
            "name",
            "tls_connect",
            "tls_accept",
            "tls_issuer",
            "tls_subject",
            "tls_psk_identity",
            "tls_psk"
        ],
        "data":[
            [
                10001,
                "Template OS Linux",
                3,
                -1,
                2,
                "",
                "",
                "Template OS Linux",
                1,
                1,
                "",
                "",
                "",
                ""
            ],
            [
                10050,
                "Template App Zabbix Agent",
                3,
                -1,
                2,
                "",
                "",
                "Template App Zabbix Agent",
                1,
                1,
                "",
                "",
                "",
                ""
            ],
            [
                10105,
                "Logger",
                0,
                -1,
                2,
                "",
                "",
                "Logger",
                1,
                1,
                "",
                "",
                "",
                ""
            ]
        ]
    },
    "interface":{
        "fields":[
            "interfaceid",
            "hostid",
            "main",
            "type",
            "useip",
            "ip",
            "dns",
            "port",
            "bulk"
        ],
        "data":[
            [
                2,
                10105,
                1,
                1,
                1,
                "127.0.0.1",
                "",
                "10050",
                1
            ]
        ]
    },
    ...
}
```

proxy→server:

``` {.javascript}
{
  "response": "success",
  "version": "3.4.0"
}
```

##### Proxy request

The `proxy data` request is used to obtain host availability,
historical, discovery and autoregistration data from proxy. This request
is sent every `ProxyDataFrequency` (server configuration parameter)
seconds.

|name|<|value type|description|
|----|-|----------|-----------|
|server→proxy:|<|<|<|
|**request**|<|*string*|'proxy data'|
|proxy→server:|<|<|<|
|**host availability**|<|*array*|*(optional)* array of host availability data objects|
|<|**hostid**|*number*|host identifier|
|<|**available**|*number*|Zabbix agent availability<br><br>**0**, *HOST\_AVAILABLE\_UNKNOWN* - unknown<br>**1**, *HOST\_AVAILABLE\_TRUE* - available<br>**2**, *HOST\_AVAILABLE\_FALSE* - unavailable|
|<|**error**|*string*|Zabbix agent error message or empty string|
|<|**snmp\_available**|*number*|SNMP agent availability<br><br>**0**, *HOST\_AVAILABLE\_UNKNOWN* - unknown<br>**1**, *HOST\_AVAILABLE\_TRUE* - available<br>**2**, *HOST\_AVAILABLE\_FALSE* - unavailable|
|<|**snmp\_error**|*string*|SNMP agent error message or empty string|
|<|**ipmi\_available**|*number*|IPMI agent availability<br><br>**0**, *HOST\_AVAILABLE\_UNKNOWN* - unknown<br>**1**, *HOST\_AVAILABLE\_TRUE* - available<br>**2**, *HOST\_AVAILABLE\_FALSE* - unavailable|
|<|**ipmi\_error**|*string*|IPMI agent error message or empty string|
|<|**jmx\_available**|*number*|JMX agent availability<br><br>**0**, *HOST\_AVAILABLE\_UNKNOWN* - unknown<br>**1**, *HOST\_AVAILABLE\_TRUE* - available<br>**2**, *HOST\_AVAILABLE\_FALSE* - unavailable|
|<|**jmx\_error**|*string*|JMX agent error message or empty string|
|**history data**|<|*array*|*(optional)* array of history data objects|
|<|**itemid**|*number*|item identifier|
|<|**clock**|*number*|item value timestamp (seconds)|
|<|**ns**|*number*|item value timestamp (nanoseconds)|
|<|**value**|*string*|*(optional)* item value|
|<|**timestamp**|*number*|*(optional)* timestamp of log type items|
|<|**source**|*string*|*(optional)* eventlog item source value|
|<|**severity**|*number*|*(optional)* eventlog item severity value|
|<|**eventid**|*number*|*(optional)* eventlog item eventid value|
|<|**state**|*string*|*(optional)* item state<br>**0**, *ITEM\_STATE\_NORMAL*<br>**1**, *ITEM\_STATE\_NOTSUPPORTED*|
|<|**lastlogsize**|*number*|*(optional)* last logs ize of log type items|
|<|**mtime**|*number*|*(optional)* modify time of log type items|
|**discovery data**|<|*array*|*(optional)* array of discovery data objects|
|<|**clock**|*number*|the discovery data timestamp|
|<|**druleid**|*number*|the discovery rule identifier|
|<|**dcheckid**|*number*|the discovery check indentifier or null for discovery rule data|
|<|**type**|*number*|the discovery check type:<br><br>**-1** discovery rule data<br>**0**, *SVC\_SSH* - SSH service check<br>**1**, *SVC\_LDAP* - LDAP service check<br>**2**, *SVC\_SMTP* - SMTP service check<br>**3**, *SVC\_FTP* - FTP service check<br>**4**, *SVC\_HTTP* - HTTP service check<br>**5**, *SVC\_POP* - POP service check<br>**6**, *SVC\_NNTP* - NNTP service check<br>**7**, *SVC\_IMAP* - IMAP service check<br>**8**, *SVC\_TCP* - TCP port availability check<br>**9**, *SVC\_AGENT* - Zabbix agent<br>**10**, *SVC\_SNMPv1* - SNMPv1 agent<br>**11**, *SVC\_SNMPv2* - SNMPv2 agent<br>**12**, *SVC\_ICMPPING* - ICMP ping<br>**13**, *SVC\_SNMPv3* - SNMPv3 agent<br>**14**, *SVC\_HTTPS* - HTTPS service check<br>**15**, *SVC\_TELNET* - Telnet availability check|
|<|**ip**|*string*|the host IP address|
|<|**dns**|*string*|the host DNS name|
|<|**port**|*number*|*(optional)* service port number|
|<|**key\_**|*string*|*(optional)* the item key for discovery check of type **9** *SVC\_AGENT*|
|<|**value**|*string*|*(optional)* value received from the service, can be empty for most of services|
|<|**status**|*number*|*(optional)* service status:<br><br>**0**, *DOBJECT\_STATUS\_UP* - Service UP<br>**1**, *DOBJECT\_STATUS\_DOWN* - Service DOWN|
|**auto registration**|<|*array*|*(optional)* array of auto registration data objects|
|<|**clock**|*number*|the auto registration data timestamp|
|<|**host**|*string*|the host name|
|<|**ip**|*string*|*(optional)* the host IP address|
|<|**dns**|*string*|*(optional)* the resolved DNS name from IP address|
|<|**port**|*string*|*(optional)* the host port|
|<|**host\_metadata**|*string*|*(optional)* the host metadata sent by agent (based on HostMetadata or HostMetadataItem agent configuration parameter)|
|**tasks**|<|*array*|*(optional)* array of tasks|
|<|**type**|*number*|the task type:<br><br>**0**, *ZBX\_TM\_TASK\_PROCESS\_REMOTE\_COMMAND\_RESULT* - remote command result|
|<|**status**|*number*|the remote command execution status:<br><br>**0**, *ZBX\_TM\_REMOTE\_COMMAND\_COMPLETED* - the remote command completed successfully<br>**1**, *ZBX\_TM\_REMOTE\_COMMAND\_FAILED* - the remote command failed|
|<|**error**|*string*|*(optional)* the error message|
|<|**parent\_taskid**|*number*|the parent task id|
|**more**|<|*number*|*(optional)* 1 - there are more history data to send|
|**clock**|<|*number*|data transfer timestamp (seconds)|
|**ns**|<|*number*|data transfer timestamp (nanoseconds)|
|**version**|<|*string*|the proxy version (<major>.<minor>.<build>)|
|server→proxy:|<|<|<|
|**response**|<|*string*|the request success information ('success' or 'failed')|
|**tasks**|<|*array*|*(optional)* array of tasks|
|<|**type**|*number*|the task type:<br><br>**1**, *ZBX\_TM\_TASK\_PROCESS\_REMOTE\_COMMAND* - remote command|
|<|**clock**|*number*|the task creation time|
|<|**ttl**|*number*|the time in seconds after which task expires|
|<|**commandtype**|*number*|the remote command type:<br><br>**0**, *ZBX\_SCRIPT\_TYPE\_CUSTOM\_SCRIPT* - use custom script<br>**1**, *ZBX\_SCRIPT\_TYPE\_IPMI* - use IPMI<br>**2**, *ZBX\_SCRIPT\_TYPE\_SSH* - use SSH<br>**3**, *ZBX\_SCRIPT\_TYPE\_TELNET* - use Telnet<br>**4**, *ZBX\_SCRIPT\_TYPE\_GLOBAL\_SCRIPT* - use global script (currently functionally equivalent to custom script)|
|<|**command**|*string*|the remote command to execute|
|<|**execute\_on**|*number*|the execution target for custom scripts:<br><br>**0**, *ZBX\_SCRIPT\_EXECUTE\_ON\_AGENT* - execute script on agent<br>**1**, *ZBX\_SCRIPT\_EXECUTE\_ON\_SERVER* - execute script on server<br>**2**, *ZBX\_SCRIPT\_EXECUTE\_ON\_PROXY* - execute script on proxy|
|<|**port**|*number*|*(optional)* the port for telnet and ssh commands|
|<|**authtype**|*number*|*(optional)* the authentication type for ssh commands|
|<|**username**|*string*|*(optional)* the user name for telnet and ssh commands|
|<|**password**|*string*|*(optional)* the password for telnet and ssh commands|
|<|**publickey**|*string*|*(optional)* the public key for ssh commands|
|<|**privatekey**|*string*|*(optional)* the private key for ssh commands|
|<|**parent\_taskid**|*number*|the parent task id|
|<|**hostid**|*number*|target hostid|

Example:

server→proxy:

``` {.javascript}
{
  "request": "proxy data"
}
```

proxy→server:

``` {.javascript}
{
    "host availability":[
        {
            "hostid":10106,
            "available":1,
            "error":"",
            "snmp_available":0,
            "snmp_error":"",
            "ipmi_available":0,
            "ipmi_error":"",
            "jmx_available":0,
            "jmx_error":""
        },
        {
            "hostid":10107,
            "available":1,
            "error":"",
            "snmp_available":0,
            "snmp_error":"",
            "ipmi_available":0,
            "ipmi_error":"",
            "jmx_available":0,
            "jmx_error":""
        }
    ],
    "history data":[
        {
            "itemid":"12345",
            "clock":1478609647,
            "ns":332510044,
            "value":"52956612"
        },
        {
            "itemid":"12346",
            "clock":1478609647,
            "ns":330690279,
            "state":1,
            "value":"Cannot find information for this network interface in /proc/net/dev."
        }
    ],
    "discovery data":[
        {
            "clock":1478608764,
            "drule":2,
            "dcheck":3,
            "type":12,
            "ip":"10.3.0.10",
            "dns":"vdebian",
            "status":1
        },
        {
            "clock":1478608764,
            "drule":2,
            "dcheck":null,
            "type":-1,
            "ip":"10.3.0.10",
            "dns":"vdebian",
            "status":1
        }
    ],
    "auto registration":[
        {
            "clock":1478608371,
            "host":"Logger1",
            "ip":"10.3.0.1",
            "dns":"localhost",
            "port":"10050"
        },
        {
            "clock":1478608381,
            "host":"Logger2",
            "ip":"10.3.0.2",
            "dns":"localhost",
            "port":"10050"
        }
    ],
    "tasks":[
        {
            "type": 0,
            "status": 0,
            "parent_taskid": 10
        },
        {
            "type": 0,
            "status": 1,
            "error": "No permissions to execute task.",
            "parent_taskid": 20
        }
    ],    
    "clock":1478609648,
    "ns":157729208,
    "version":"3.4.0"
}
```

server→proxy:

``` {.javascript}
{
  "response": "success",
  "tasks":[
      {
         "type": 1,
         "clock": 1478608371,
         "ttl": 600,
         "commandtype": 2,
         "command": "restart_service1.sh",
         "execute_on": 2,
         "port": 80,
         "authtype": 0,
         "username": "userA",
         "password": "password1",
         "publickey": "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCqGKukO1De7zhZj6+H0qtjTkVxwTCpvKe",
         "privatekey": "lsuusFncCzWBQ7RKNUSesmQRMSGkVb1/3j+skZ6UtW+5u09lHNsj6tQ5QCqGKukO1De7zhd",
         "parent_taskid": 10,
         "hostid": 10070
      },
      {
         "type": 1,
         "clock": 1478608381,
         "ttl": 600,
         "commandtype": 1,
         "command": "restart_service2.sh",
         "execute_on": 0,
         "authtype": 0,
         "username": "",
         "password": "",
         "publickey": "",
         "privatekey": "",
         "parent_taskid": 20,
         "hostid": 10084
      }
  ]
}
```

#### Active proxy

##### Proxy heartbeat request

The `proxy heartbeat` request is sent by proxy to report that proxy is
running. This request is sent every `HeartbeatFrequency` (proxy
configuration parameter) seconds.

|name|<|value type|description|
|----|-|----------|-----------|
|proxy→server:|<|<|<|
|**request**|<|*string*|'proxy heartbeat'|
|**host**|<|*string*|the proxy name|
|**version**|<|*string*|the proxy version (<major>.<minor>.<build>)|
|server→proxy:|<|<|<|
|**response**|<|*string*|the request success information ('success' or 'failed')|

proxy→server:

``` {.javascript}
{
   "request": "proxy heartbeat",
   "host": "Proxy #12",
   "version": "3.4.0"
}
```

server→proxy:

``` {.javascript}
{
  "response": "success"
}
```

##### Proxy config request

The `proxy config` request is sent by proxy to obtain proxy
configuration data. This request is sent every `ConfigFrequency` (proxy
configuration parameter) seconds.

|name|<|<|<|value type|description|
|----|-|-|-|----------|-----------|
|proxy→server:|<|<|<|<|<|
|**request**|<|<|<|*string*|'proxy config'|
|**host**|<|<|<|*string*|proxy name|
|**version**|<|<|<|*string*|the proxy version (<major>.<minor>.<build>)|
|server→proxy:|<|<|<|<|<|
|**request**|<|<|<|*string*|'proxy config'|
|**<table>**|<|<|<|*object*|one or more objects with <table> data|
|<|**fields**|<|<|*array*|array of field names|
|<|<|\-|<|*string*|field name|
|<|**data**|<|<|*array*|array of rows|
|<|<|\-|<|*array*|array of columns|
|<|<|<|\-|*string*,*number*|column value with type depending on column type in database schema|
|proxy→server:|<|<|<|<|<|
|**response**|<|<|<|*string*|the request success information ('success' or 'failed')|

Example:

proxy→server:

``` {.javascript}
{
  "request": "proxy config",
  "host": "Proxy #12",
  "version":"3.4.0"
}
```

server→proxy:

``` {.javascript}
{
    "globalmacro":{
        "fields":[
            "globalmacroid",
            "macro",
            "value"
        ],
        "data":[
            [
                2,
                "{$SNMP_COMMUNITY}",
                "public"
            ]
        ]
    },
    "hosts":{
        "fields":[
            "hostid",
            "host",
            "status",
            "ipmi_authtype",
            "ipmi_privilege",
            "ipmi_username",
            "ipmi_password",
            "name",
            "tls_connect",
            "tls_accept",
            "tls_issuer",
            "tls_subject",
            "tls_psk_identity",
            "tls_psk"
        ],
        "data":[
            [
                10001,
                "Template OS Linux",
                3,
                -1,
                2,
                "",
                "",
                "Template OS Linux",
                1,
                1,
                "",
                "",
                "",
                ""
            ],
            [
                10050,
                "Template App Zabbix Agent",
                3,
                -1,
                2,
                "",
                "",
                "Template App Zabbix Agent",
                1,
                1,
                "",
                "",
                "",
                ""
            ],
            [
                10105,
                "Logger",
                0,
                -1,
                2,
                "",
                "",
                "Logger",
                1,
                1,
                "",
                "",
                "",
                ""
            ]
        ]
    },
    "interface":{
        "fields":[
            "interfaceid",
            "hostid",
            "main",
            "type",
            "useip",
            "ip",
            "dns",
            "port",
            "bulk"
        ],
        "data":[
            [
                2,
                10105,
                1,
                1,
                1,
                "127.0.0.1",
                "",
                "10050",
                1
            ]
        ]
    },
    ...
}
```

proxy→server:

``` {.javascript}
{
  "response": "success"
}
```

##### Proxy data request

The `proxy data` request is sent by proxy to provide host availability,
history, discovery and auto registation data. This request is sent every
`DataSenderFrequency` (proxy configuration parameter) seconds.

|name|<|value type|description|
|----|-|----------|-----------|
|proxy→server:|<|<|<|
|**request**|<|*string*|'proxy data'|
|**host**|<|*string*|the proxy name|
|**host availability**|<|*array*|*(optional)* array of host availability data objects|
|<|**hostid**|*number*|host identifier|
|<|**available**|*number*|Zabbix agent availability<br><br>**0**, *HOST\_AVAILABLE\_UNKNOWN* - unknown<br>**1**, *HOST\_AVAILABLE\_TRUE* - available<br>**2**, *HOST\_AVAILABLE\_FALSE* - unavailable|
|<|**error**|*string*|Zabbix agent error message or empty string|
|<|**snmp\_available**|*number*|SNMP agent availability<br><br>**0**, *HOST\_AVAILABLE\_UNKNOWN* - unknown<br>**1**, *HOST\_AVAILABLE\_TRUE* - available<br>**2**, *HOST\_AVAILABLE\_FALSE* - unavailable|
|<|**snmp\_error**|*string*|SNMP agent error message or empty string|
|<|**ipmi\_available**|*number*|IPMI agent availability<br><br>**0**, *HOST\_AVAILABLE\_UNKNOWN* - unknown<br>**1**, *HOST\_AVAILABLE\_TRUE* - available<br>**2**, *HOST\_AVAILABLE\_FALSE* - unavailable|
|<|**ipmi\_error**|*string*|IPMI agent error message or empty string|
|<|**jmx\_available**|*number*|JMX agent availability<br><br>**0**, *HOST\_AVAILABLE\_UNKNOWN* - unknown<br>**1**, *HOST\_AVAILABLE\_TRUE* - available<br>**2**, *HOST\_AVAILABLE\_FALSE* - unavailable|
|<|**jmx\_error**|*string*|JMX agent error message or empty string|
|**history data**|<|*array*|*(optional)* array of history data objects|
|<|**itemid**|*number*|item identifier|
|<|**clock**|*number*|item value timestamp (seconds)|
|<|**ns**|*number*|item value timestamp (nanoseconds)|
|<|**value**|*string*|*(optional)* item value|
|<|**timestamp**|*number*|*(optional)* timestamp of log type items|
|<|**source**|*string*|*(optional)* eventlog item source value|
|<|**severity**|*number*|*(optional)* eventlog item severity value|
|<|**eventid**|*number*|*(optional)* eventlog item eventid value|
|<|**state**|*string*|*(optional)* item state<br>**0**, *ITEM\_STATE\_NORMAL*<br>**1**, *ITEM\_STATE\_NOTSUPPORTED*|
|<|**lastlogsize**|*number*|*(optional)* last logs ize of log type items|
|<|**mtime**|*number*|*(optional)* modify time of log type items|
|**discovery data**|<|*array*|*(optional)* array of discovery data objects|
|<|**clock**|*number*|the discovery data timestamp|
|<|**druleid**|*number*|the discovery rule identifier|
|<|**dcheckid**|*number*|the discovery check indentifier or null for discovery rule data|
|<|**type**|*number*|the discovery check type:<br><br>**-1** discovery rule data<br>**0**, *SVC\_SSH* - SSH service check<br>**1**, *SVC\_LDAP* - LDAP service check<br>**2**, *SVC\_SMTP* - SMTP service check<br>**3**, *SVC\_FTP* - FTP service check<br>**4**, *SVC\_HTTP* - HTTP service check<br>**5**, *SVC\_POP* - POP service check<br>**6**, *SVC\_NNTP* - NNTP service check<br>**7**, *SVC\_IMAP* - IMAP service check<br>**8**, *SVC\_TCP* - TCP port availability check<br>**9**, *SVC\_AGENT* - Zabbix agent<br>**10**, *SVC\_SNMPv1* - SNMPv1 agent<br>**11**, *SVC\_SNMPv2* - SNMPv2 agent<br>**12**, *SVC\_ICMPPING* - ICMP ping<br>**13**, *SVC\_SNMPv3* - SNMPv3 agent<br>**14**, *SVC\_HTTPS* - HTTPS service check<br>**15**, *SVC\_TELNET* - Telnet availability check|
|<|**ip**|*string*|the host IP address|
|<|**dns**|*string*|the host DNS name|
|<|**port**|*number*|*(optional)* service port number|
|<|**key\_**|*string*|*(optional)* the item key for discovery check of type **9** *SVC\_AGENT*|
|<|**value**|*string*|*(optional)* value received from the service, can be empty for most of services|
|<|**status**|*number*|*(optional)* service status:<br><br>**0**, *DOBJECT\_STATUS\_UP* - Service UP<br>**1**, *DOBJECT\_STATUS\_DOWN* - Service DOWN|
|**auto registration**|<|*array*|*(optional)* array of auto registration data objects|
|<|**clock**|*number*|the auto registration data timestamp|
|<|**host**|*string*|the host name|
|<|**ip**|*string*|*(optional)* the host IP address|
|<|**dns**|*string*|*(optional)* the resolved DNS name from IP address|
|<|**port**|*string*|*(optional)* the host port|
|<|**host\_metadata**|*string*|*(optional)* the host metadata sent by agent (based on HostMetadata or HostMetadataItem agent configuration parameter)|
|**tasks**|<|*array*|*(optional)* array of tasks|
|<|**type**|*number*|the task type:<br><br>**0**, *ZBX\_TM\_TASK\_PROCESS\_REMOTE\_COMMAND\_RESULT* - remote command result|
|<|**status**|*number*|the remote command execution status:<br><br>**0**, *ZBX\_TM\_REMOTE\_COMMAND\_COMPLETED* - the remote command completed successfully<br>**1**, *ZBX\_TM\_REMOTE\_COMMAND\_FAILED* - the remote command failed|
|<|**error**|*string*|*(optional)* the error message|
|<|**parent\_taskid**|*number*|the parent task id|
|**more**|<|*number*|*(optional)* 1 - there are more history data to send|
|**clock**|<|*number*|data transfer timestamp (seconds)|
|**ns**|<|*number*|data transfer timestamp (nanoseconds)|
|**version**|<|*string*|the proxy version (<major>.<minor>.<build>)|
|server→proxy:|<|<|<|
|**response**|<|*string*|the request success information ('success' or 'failed')|
|**tasks**|<|*array*|*(optional)* array of tasks|
|<|**type**|*number*|the task type:<br><br>**1**, *ZBX\_TM\_TASK\_PROCESS\_REMOTE\_COMMAND* - remote command|
|<|**clock**|*number*|the task creation time|
|<|**ttl**|*number*|the time in seconds after which task expires|
|<|**commandtype**|*number*|the remote command type:<br><br>**0**, *ZBX\_SCRIPT\_TYPE\_CUSTOM\_SCRIPT* - use custom script<br>**1**, *ZBX\_SCRIPT\_TYPE\_IPMI* - use IPMI<br>**2**, *ZBX\_SCRIPT\_TYPE\_SSH* - use SSH<br>**3**, *ZBX\_SCRIPT\_TYPE\_TELNET* - use Telnet<br>**4**, *ZBX\_SCRIPT\_TYPE\_GLOBAL\_SCRIPT* - use global script (currently functionally equivalent to custom script)|
|<|**command**|*string*|the remote command to execute|
|<|**execute\_on**|*number*|the execution target for custom scripts:<br><br>**0**, *ZBX\_SCRIPT\_EXECUTE\_ON\_AGENT* - execute script on agent<br>**1**, *ZBX\_SCRIPT\_EXECUTE\_ON\_SERVER* - execute script on server<br>**2**, *ZBX\_SCRIPT\_EXECUTE\_ON\_PROXY* - execute script on proxy|
|<|**port**|*number*|*(optional)* the port for telnet and ssh commands|
|<|**authtype**|*number*|*(optional)* the authentication type for ssh commands|
|<|**username**|*string*|*(optional)* the user name for telnet and ssh commands|
|<|**password**|*string*|*(optional)* the password for telnet and ssh commands|
|<|**publickey**|*string*|*(optional)* the public key for ssh commands|
|<|**privatekey**|*string*|*(optional)* the private key for ssh commands|
|<|**parent\_taskid**|*number*|the parent task id|
|<|**hostid**|*number*|target hostid|

Example:

proxy→server:

``` {.javascript}
{
  "request": "proxy data",
  "host": "Proxy #12", 
    "host availability":[
        {
            "hostid":10106,
            "available":1,
            "error":"",
            "snmp_available":0,
            "snmp_error":"",
            "ipmi_available":0,
            "ipmi_error":"",
            "jmx_available":0,
            "jmx_error":""
        },
        {
            "hostid":10107,
            "available":1,
            "error":"",
            "snmp_available":0,
            "snmp_error":"",
            "ipmi_available":0,
            "ipmi_error":"",
            "jmx_available":0,
            "jmx_error":""
        }
    ],
    "history data":[
        {
            "itemid":"12345",
            "clock":1478609647,
            "ns":332510044,
            "value":"52956612"
        },
        {
            "itemid":"12346",
            "clock":1478609647,
            "ns":330690279,
            "state":1,
            "value":"Cannot find information for this network interface in /proc/net/dev."
        }
    ],
    "discovery data":[
        {
            "clock":1478608764,
            "drule":2,
            "dcheck":3,
            "type":12,
            "ip":"10.3.0.10",
            "dns":"vdebian",
            "status":1
        },
        {
            "clock":1478608764,
            "drule":2,
            "dcheck":null,
            "type":-1,
            "ip":"10.3.0.10",
            "dns":"vdebian",
            "status":1
        }
    ],
    "auto registration":[
        {
            "clock":1478608371,
            "host":"Logger1",
            "ip":"10.3.0.1",
            "dns":"localhost",
            "port":"10050"
        },
        {
            "clock":1478608381,
            "host":"Logger2",
            "ip":"10.3.0.2",
            "dns":"localhost",
            "port":"10050"
        }
    ],
    "tasks":[
        {
            "type": 2,
            "clock":1478608371,
            "ttl": 600,
            "commandtype": 2,
            "command": "restart_service1.sh",
            "execute_on": 2,
            "port": 80,
            "authtype": 0,
            "username": "userA",
            "password": "password1",
            "publickey": "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCqGKukO1De7zhZj6+H0qtjTkVxwTCpvKe",
            "privatekey": "lsuusFncCzWBQ7RKNUSesmQRMSGkVb1/3j+skZ6UtW+5u09lHNsj6tQ5QCqGKukO1De7zhd",
            "parent_taskid": 10,
            "hostid": 10070
        },
        {
            "type": 2,
            "clock":1478608381,
            "ttl": 600,
            "commandtype": 1,
            "command": "restart_service2.sh",
            "execute_on": 0,
            "authtype": 0,
            "username": "",
            "password": "",
            "publickey": "",
            "privatekey": "",
            "parent_taskid": 20,
            "hostid": 10084
        }
    ],
    "tasks":[
        {
            "type": 0,
            "status": 0,
            "parent_taskid": 10
        },
        {
            "type": 0,
            "status": 1,
            "error": "No permissions to execute task.",
            "parent_taskid": 20
        }
    ], 
    "clock":1478609648,
    "ns":157729208,
    "version":"3.4.0"
}
```

server→proxy:

``` {.javascript}
{
  "response": "success",
  "tasks":[
      {
         "type": 1,
         "clock": 1478608371,
         "ttl": 600,
         "commandtype": 2,
         "command": "restart_service1.sh",
         "execute_on": 2,
         "port": 80,
         "authtype": 0,
         "username": "userA",
         "password": "password1",
         "publickey": "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCqGKukO1De7zhZj6+H0qtjTkVxwTCpvKe",
         "privatekey": "lsuusFncCzWBQ7RKNUSesmQRMSGkVb1/3j+skZ6UtW+5u09lHNsj6tQ5QCqGKukO1De7zhd",
         "parent_taskid": 10,
         "hostid": 10070
      },
      {
         "type": 1,
         "clock": 1478608381,
         "ttl": 600,
         "commandtype": 1,
         "command": "restart_service2.sh",
         "execute_on": 0,
         "authtype": 0,
         "username": "",
         "password": "",
         "publickey": "",
         "privatekey": "",
         "parent_taskid": 20,
         "hostid": 10084
      }
  ]
}
```

#### Backwards compatibility

Server supports partial backwards compatibility by accepting old
`host availability`, `history data`, `discovery data` and
`auto registration` requests.

[comment]: # ({/new-ef0ba621})
