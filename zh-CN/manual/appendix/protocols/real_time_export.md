[comment]: # translation:outdated

[comment]: # ({new-cb9df6d6})
# 5 实时导出协议

此部分描述 [real-time export](/manual/appendix/install/real_time_export)
协议以换行符分隔的JSON格式：

-   [trigger events](#trigger_events)
-   [item values](#item_values)
-   [trends](#trends)

所有文件都有.ndjson扩展名。 导出文件的每一行都是JSON对象。

[comment]: # ({/new-cb9df6d6})

[comment]: # ({new-bc637674})
#### 触发事件

导出问题事件的以下信息：

|Field|<|<|Type|Description|
|-----|-|-|----|-----------|
|*hosts*|<|<|array|List of hosts involved in the trigger expression; there should be at least one element in array.|
|<|\-|<|string|Visible host name.|
|*groups*|<|<|array|list of host groups of all hosts involved in the trigger expression; there should be at least one element in array.|
|<|\-|<|string|Host group name.|
|*tags*|<|<|array|List of problem tags (can be empty).|
|<|\-|<|object|<|
|<|<|*tag*|string|Tag name.|
|<|<|*value*|string|Tag value (can be empty).|
|*name*|<|<|string|Problem event name.|
|*clock*|<|<|number|Number of seconds since Epoch to the moment when problem was detected (integer part).|
|*ns*|<|<|number|Number of nanoseconds to be added to `clock` to get a precise problem detection time.|
|*eventid*|<|<|number|Problem event ID.|
|*value*|<|<|number|1 (always).|

将为恢复事件导出以下信息：

|Field|Type|Description|
|-----|----|-----------|
|*clock*|number|Number of seconds since Epoch to the moment when problem was resolved (integer part).|
|*ns*|number|Number of nanoseconds to be added to `clock` to get a precise problem resolution time.|
|*eventid*|number|Recovery event ID.|
|*p\_eventid*|number|Problem event ID.|
|*value*|number|0 (always).|

[comment]: # ({/new-bc637674})

[comment]: # ({new-9eccc413})
##### 例子

Problem:

    {"hosts":["Host B","Zabbix Server"],"groups":["Group X","Group Y","Group Z","Zabbix servers"],"tags":[{"tag":"availability","value":""},{"tag":"data center","value":"Riga"}],"name":"Either Zabbix agent is unreachable on Host B or pollers are too busy on Zabbix Server","clock":1519304285,"ns":123456789,"eventid":42, "value":1}

Recovery:

    {"clock":1519304345,"ns":987654321,"eventid":43,"p_eventid":42,"value":0}

Problem (multiple problem event generation):

    {"hosts":["Host B","Zabbix Server"],"groups":["Group X","Group Y","Group Z","Zabbix servers"],"tags":[{"tag":"availability","value":""},{"tag":"data center","value":"Riga"}],"name":"Either Zabbix agent is unreachable on Host B or pollers are too busy on Zabbix Server","clock":1519304286,"ns":123456789,"eventid":43, "value":1}

    {"hosts":["Host B","Zabbix Server"],"groups":["Group X","Group Y","Group Z","Zabbix servers"],"tags":[{"tag":"availability","value":""},{"tag":"data center","value":"Riga"}],"name":"Either Zabbix agent is unreachable on Host B or pollers are too busy on Zabbix Server","clock":1519304286,"ns":123456789,"eventid":43, "value":1}

Recovery:

    {"clock":1519304346,"ns":987654321,"eventid":44,"p_eventid":43,"value":0}

    {"clock":1519304346,"ns":987654321,"eventid":44,"p_eventid":42,"value":0}

[comment]: # ({/new-9eccc413})

[comment]: # ({new-9faee484})
#### 监控项值

将为收集的项目值导出以下信息：

|Field|<|Type|Description|
|-----|-|----|-----------|
|*host*|<|string|Visible host name of the item host.|
|*groups*|<|array|List of host groups of the item host; there should be at least one element in array.|
|<|\-|string|Host group name.|
|*applications*|<|array|List of the item applications; empty if there are none.|
|<|\-|string|Application name.|
|*itemid*|<|number|Item ID.|
|*name*|<|string|Visible item name.|
|*clock*|<|number|Number of seconds since Epoch to the moment when value was collected (integer part).|
|*ns*|<|number|Number of nanoseconds to be added to `clock` to get a precise value collection time.|
|*timestamp*<br>(*Log* only)|<|number|0 if not available.|
|*source*<br>(*Log* only)|<|string|Empty string if not available.|
|*severity*<br>(*Log* only)|<|number|0 if not available.|
|*logeventid*<br>(*Log* only)|<|number|0 if not available.|
|*value*|<|number (for numeric items) or<br>string (for text items)|Collected item value.|

[comment]: # ({/new-9faee484})

[comment]: # ({new-683a131b})
##### 例子

Numeric (unsigned) value:

    {"host":"Host B","groups":["Group X","Group Y","Group Z"],"applications":["Zabbix Agent","Availability"],"itemid":3,"name":"Agent availability","clock":1519304285,"ns":123456789,"value":1}

Numeric (float) value:

    {"host":"Host B","groups":["Group X","Group Y","Group Z"],"applications":["CPU","Performance"],"itemid":4,"name":"CPU Load","clock":1519304285,"ns":123456789,"value":"0.1"}

Character, text value:

    {"host":"Host B","groups":["Group X","Group Y","Group Z"],"applications":["Zabbix Agent","Installed software versions"],"itemid":2,"name":"Agent version","clock":1519304285,"ns":123456789,"value":"3.4.4"}

Log value:

    {"host":"Host A","groups":["Group X","Group Y","Group Z"],"applications":["Log files","Critical"],"itemid":1,"name":"Messages in log file","clock":1519304285,"ns":123456789,"timestamp":1519304285,"source":"","severity":0,"logeventid":0,"value":"log file message"}

[comment]: # ({/new-683a131b})

[comment]: # ({new-f534dda9})
#### 趋势

将为计算的趋势值导出以下信息：

|Field|<|Type|Description|
|-----|-|----|-----------|
|*host*|<|string|Visible host name of the item host.|
|*groups*|<|array|List of host groups of the item host; there should be at least one element in array.|
|<|\-|string|Host group name.|
|*applications*|<|array|List of the item applications; empty if there are none.|
|<|\-|string|Application name.|
|*itemid*|<|number|Item ID.|
|*name*|<|string|Visible item name.|
|*clock*|<|number|Number of seconds since Epoch to the moment when value was collected (integer part).|
|*count*|<|number|Number of values collected for a given hour.|
|*min*|<|number|Minimum item value for a given hour.|
|*avg*|<|number|Average item value for a given hour.|
|*max*|<|number|Maximum item value for a given hour.|

[comment]: # ({/new-f534dda9})

[comment]: # ({new-8606813d})
##### 例子

Numeric (unsigned) value:

    {"host":"Host B","groups":["Group X","Group Y","Group Z"],"applications":["Zabbix Agent","Availability"],"itemid":3,"name":"Agent availability","clock":1519311600,"count":60,"min":1,"avg":1,"max":1}

Numeric (float) value:

    {"host":"Host B","groups":["Group X","Group Y","Group Z"],"applications":["CPU","Performance"],"itemid":4,"name":"CPU Load","clock":1519311600,"count":60,"min":0.01,"avg":0.15,"max":1.5}

### 5 Real-time export protocol

This section presents details of the [real-time
export](/manual/appendix/install/real_time_export) protocol in a
newline-delimited JSON format for:

-   [trigger events](#trigger_events)
-   [item values](#item_values)
-   [trends](#trends)

All files have a .ndjson extension. Each line of the export file is a
JSON object.

#### Trigger events

The following information is exported for a problem event:

|Field|<|<|Type|Description|
|-----|-|-|----|-----------|
|*hosts*|<|<|array|List of hosts involved in the trigger expression; there should be at least one element in array.|
|<|\-|<|string|Visible host name.|
|*groups*|<|<|array|list of host groups of all hosts involved in the trigger expression; there should be at least one element in array.|
|<|\-|<|string|Host group name.|
|*tags*|<|<|array|List of problem tags (can be empty).|
|<|\-|<|object|<|
|<|<|*tag*|string|Tag name.|
|<|<|*value*|string|Tag value (can be empty).|
|*name*|<|<|string|Problem event name.|
|*clock*|<|<|number|Number of seconds since Epoch to the moment when problem was detected (integer part).|
|*ns*|<|<|number|Number of nanoseconds to be added to `clock` to get a precise problem detection time.|
|*eventid*|<|<|number|Problem event ID.|
|*value*|<|<|number|1 (always).|

The following information is exported for a recovery event:

|Field|Type|Description|
|-----|----|-----------|
|*clock*|number|Number of seconds since Epoch to the moment when problem was resolved (integer part).|
|*ns*|number|Number of nanoseconds to be added to `clock` to get a precise problem resolution time.|
|*eventid*|number|Recovery event ID.|
|*p\_eventid*|number|Problem event ID.|
|*value*|number|0 (always).|

##### Examples

Problem:

    {"hosts":["Host B","Zabbix Server"],"groups":["Group X","Group Y","Group Z","Zabbix servers"],"tags":[{"tag":"availability","value":""},{"tag":"data center","value":"Riga"}],"name":"Either Zabbix agent is unreachable on Host B or pollers are too busy on Zabbix Server","clock":1519304285,"ns":123456789,"eventid":42, "value":1}

Recovery:

    {"clock":1519304345,"ns":987654321,"eventid":43,"p_eventid":42,"value":0}

Problem (multiple problem event generation):

    {"hosts":["Host B","Zabbix Server"],"groups":["Group X","Group Y","Group Z","Zabbix servers"],"tags":[{"tag":"availability","value":""},{"tag":"data center","value":"Riga"}],"name":"Either Zabbix agent is unreachable on Host B or pollers are too busy on Zabbix Server","clock":1519304286,"ns":123456789,"eventid":43, "value":1}

    {"hosts":["Host B","Zabbix Server"],"groups":["Group X","Group Y","Group Z","Zabbix servers"],"tags":[{"tag":"availability","value":""},{"tag":"data center","value":"Riga"}],"name":"Either Zabbix agent is unreachable on Host B or pollers are too busy on Zabbix Server","clock":1519304286,"ns":123456789,"eventid":43, "value":1}

Recovery:

    {"clock":1519304346,"ns":987654321,"eventid":44,"p_eventid":43,"value":0}

    {"clock":1519304346,"ns":987654321,"eventid":44,"p_eventid":42,"value":0}

#### Item values

The following information is exported for a collected item value:

|Field|<|Type|Description|
|-----|-|----|-----------|
|*host*|<|string|Visible host name of the item host.|
|*groups*|<|array|List of host groups of the item host; there should be at least one element in array.|
|<|\-|string|Host group name.|
|*applications*|<|array|List of the item applications; empty if there are none.|
|<|\-|string|Application name.|
|*itemid*|<|number|Item ID.|
|*name*|<|string|Visible item name.|
|*clock*|<|number|Number of seconds since Epoch to the moment when value was collected (integer part).|
|*ns*|<|number|Number of nanoseconds to be added to `clock` to get a precise value collection time.|
|*timestamp*<br>(*Log* only)|<|number|0 if not available.|
|*source*<br>(*Log* only)|<|string|Empty string if not available.|
|*severity*<br>(*Log* only)|<|number|0 if not available.|
|*logeventid*<br>(*Log* only)|<|number|0 if not available.|
|*value*|<|number (for numeric items) or<br>string (for text items)|Collected item value.|

##### Examples

Numeric (unsigned) value:

    {"host":"Host B","groups":["Group X","Group Y","Group Z"],"applications":["Zabbix Agent","Availability"],"itemid":3,"name":"Agent availability","clock":1519304285,"ns":123456789,"value":1}

Numeric (float) value:

    {"host":"Host B","groups":["Group X","Group Y","Group Z"],"applications":["CPU","Performance"],"itemid":4,"name":"CPU Load","clock":1519304285,"ns":123456789,"value":"0.1"}

Character, text value:

    {"host":"Host B","groups":["Group X","Group Y","Group Z"],"applications":["Zabbix Agent","Installed software versions"],"itemid":2,"name":"Agent version","clock":1519304285,"ns":123456789,"value":"3.4.4"}

Log value:

    {"host":"Host A","groups":["Group X","Group Y","Group Z"],"applications":["Log files","Critical"],"itemid":1,"name":"Messages in log file","clock":1519304285,"ns":123456789,"timestamp":1519304285,"source":"","severity":0,"logeventid":0,"value":"log file message"}

#### Trends

The following information is exported for a calculated trend value:

|Field|<|Type|Description|
|-----|-|----|-----------|
|*host*|<|string|Visible host name of the item host.|
|*groups*|<|array|List of host groups of the item host; there should be at least one element in array.|
|<|\-|string|Host group name.|
|*applications*|<|array|List of the item applications; empty if there are none.|
|<|\-|string|Application name.|
|*itemid*|<|number|Item ID.|
|*name*|<|string|Visible item name.|
|*clock*|<|number|Number of seconds since Epoch to the moment when value was collected (integer part).|
|*count*|<|number|Number of values collected for a given hour.|
|*min*|<|number|Minimum item value for a given hour.|
|*avg*|<|number|Average item value for a given hour.|
|*max*|<|number|Maximum item value for a given hour.|

##### Examples

Numeric (unsigned) value:

    {"host":"Host B","groups":["Group X","Group Y","Group Z"],"applications":["Zabbix Agent","Availability"],"itemid":3,"name":"Agent availability","clock":1519311600,"count":60,"min":1,"avg":1,"max":1}

Numeric (float) value:

    {"host":"Host B","groups":["Group X","Group Y","Group Z"],"applications":["CPU","Performance"],"itemid":4,"name":"CPU Load","clock":1519311600,"count":60,"min":0.01,"avg":0.15,"max":1.5}

[comment]: # ({/new-8606813d})
