<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="zh-CN" datatype="plaintext" original="manual/appendix/protocols/zabbix_agent2.md">
    <body>
      <trans-unit id="0ea9fa0f" xml:space="preserve">
        <source># 3 Zabbix agent 2 protocol</source>
      </trans-unit>
      <trans-unit id="e87a981b" xml:space="preserve">
        <source>
### Overview

This section provides information on:

*   Agent2 -&gt; Server : active checks request
*   Server -&gt; Agent2 : active checks response
*   Agent2 -&gt; Server : agent data request
*   Server -&gt; Agent2 : agent data response
*   Agent2 -&gt; Server : heartbeat message</source>
      </trans-unit>
      <trans-unit id="1c996373" xml:space="preserve">
        <source>
### Active checks request

The active checks request is used to obtain the active checks to be processed by agent. 
This request is sent by the agent upon start and then with [RefreshActiveChecks](/manual/appendix/config/zabbix_agent2) intervals.

| Field | Type | Mandatory | Value |
|-|-|-|--------|
| request | _string_ | yes | `active checks` |
| host | _string_ | yes | Host name. |
| version | _string_ | yes | The agent version: \&lt;major&gt;.\&lt;minor&gt;. |
| host_metadata | _string_ | no | The configuration parameter HostMetadata or HostMetadataItem metric value. |
| interface | _string_ | no | The configuration parameter HostInterface or HostInterfaceItem metric value. |
| ip | _string_ | no | The configuration parameter ListenIP first IP if set. |
| port | _number_ | no | The configuration parameter ListenPort value if set and not default agent listening port. |
| config_revision | _number_ | no | Configuration identifier for [incremental configuration sync](/manual/appendix/items/activepassive#active-checks). |
| session | _string_ | no | Session identifier for [incremental configuration sync](/manual/appendix/items/activepassive#active-checks). |

Example:
```json
{
  "request": "active checks",
  "host": "Zabbix server",
  "version": "6.0",
  "host_metadata": "mysql,nginx",
  "hostinterface": "zabbix.server.lan",
  "ip": "159.168.1.1",
  "port": 12050,
  "config_revision": 1,
  "session": "e3dcbd9ace2c9694e1d7bbd030eeef6e"
}
```</source>
      </trans-unit>
      <trans-unit id="44da8f0a" xml:space="preserve">
        <source>
### Active checks response

The active checks response is sent by the server back to agent after processing active checks request.

| Field |&lt;| Type | Mandatory | Value |
|-|------|--|-|-----------------------|
| response |&lt;| _string_ | yes | `success` \| `failed` |
| info |&lt;| _string_ | no | Error information in the case of failure. |
| data |&lt;| _array of objects_ | no | Active check items. Omitted if host configuration is unchanged. |
| | key | _string_ | no | Item key with expanded macros. |
|^| itemid | _number_ | no | Item identifier. |
|^| delay | _string_ | no | Item update interval. |
|^| lastlogsize | _number_ | no | Item lastlogsize. |
|^| mtime | _number_ | no | Item mtime. |
| regexp |&lt;| _array of objects_ | no | Global regular expressions. |
| | name | _string_ | no | Global regular expression name. |
|^| expression | _string_ | no | Global regular expression. |
|^| expression_type | _number_ | no | Global regular expression type. |
|^| exp_delimiter | _string_ | no | Global regular expression delimiter. |
|^| case_sensitive | _number_ | no | Global regular expression case sensitivity setting. |
| commands |&lt;| _array of objects_ | no | Remote commands to execute. Included if remote command execution has been triggered by an action [operation](/manual/config/notifications/action/operation#operations) or manual [script](/manual/web_interface/frontend_sections/alerts/scripts) execution. Note that remote command execution on an active agent is supported since Zabbix agent 7.0. Older active agents will ignore any remote commands included in the active checks server response. |
| | command | _string_ | no | Remote command. |
|^| id | _number_ | no | Remote command identifier. |
|^| wait | _number_ | no | Remote command mode of execution ("0" (nowait) for commands from action [operations](/manual/config/notifications/action/operation#operations); "1" (wait) for commands from manual [script](/manual/web_interface/frontend_sections/alerts/scripts) execution). |
| config_revision | &lt; | _number_ | no | Configuration identifier for [incremental configuration sync](/manual/appendix/items/activepassive#active-checks). Omitted if host configuration is unchanged. Incremented if host configuration is changed. |

Example:
```json
{
  "response": "success",
  "data": [
    {
      "key": "log[/home/zabbix/logs/zabbix_agentd.log]",
      "itemid": 1234,
      "delay": "30s",
      "lastlogsize": 0,
      "mtime": 0
    },
    {
      "key": "agent.version",
      "itemid": 5678,
      "delay": "10m",
      "lastlogsize": 0,
      "mtime": 0
    }
  ],
  "commands": [
    {
      "command": "df -h --output=source,size / | awk 'NR&gt;1 {print $2}'",
      "id": 1324,
      "wait": 1
    }
  ],
  "config_revision": 2
}
```</source>
      </trans-unit>
      <trans-unit id="8a564dbc" xml:space="preserve">
        <source>
### Agent data request

The agent data request contains the gathered item values and the values for executed remote commands (if any).

| Field |&lt;| Type | Mandatory | Value |
|-|------|--|-|-----------------------|
| request |&lt;| _string_ | yes | `agent data` |
| host |&lt;| _string_ | yes | Host name. |
| version |&lt;| _string_ | yes | The agent version: \&lt;major&gt;.\&lt;minor&gt;. |
| session |&lt;| _string_ | yes | Unique session identifier generated each time when agent is started. |
| data |&lt;| _array of objects_ | yes | Item values. |
| | id | _number_ | yes | The value identifier (incremental counter used for checking duplicated values in the case of network problems). |
|^| itemid | _number_ | yes | Item identifier. |
|^| value | _string_ | no | The item value. |
|^| lastlogsize | _number_ | no | The item lastlogsize. |
|^| mtime | _number_ | no | The item mtime. |
|^| state | _number_ | no | The item state. |
|^| source | _string_ | no | The value event log source. |
|^| eventid | _number_ | no | The value event log eventid. |
|^| severity | _number_ | no | The value event log severity. |
|^| timestamp | _number_ | no | The value event log timestamp. |
|^| clock | _number_ | yes | The value timestamp (seconds since Epoch). |
|^| ns | _number_ | yes | The value timestamp nanoseconds. |
| commands |&lt;| _array of objects_ | no | Remote commands execution result. Note that remote command execution on an active agent is supported since Zabbix agent 7.0. Older active agents will ignore any remote commands included in the active checks server response. |
| | id | _number_ | no | Remote command identifier. |
|^| value | _string_ | no | Remote command execution result if the execution was successful. |
|^| error | _string_ | no | Remote command execution error message if the execution failed. |

Example:
```json
{
  "request": "agent data",
  "data": [
    {
      "id": 1,
      "itemid": 5678,
      "value": "2.4.0",
      "clock": 1400675595,
      "ns": 76808644
    },
    {
      "id": 2,
      "itemid": 1234,
      "lastlogsize": 112,
      "value": " 19845:20140621:141708.521 Starting Zabbix Agent [&lt;hostname&gt;]. Zabbix 2.4.0 (revision 50000).",
      "clock": 1400675595,
      "ns": 77053975
    }
  ],
  "commands": [
    {
      "id": 1324,
      "value": "16G"
    }
  ],
  "host": "Zabbix server",
  "version": "6.0",
  "session": "1234456akdsjhfoui"
}
```</source>
      </trans-unit>
      <trans-unit id="b302d014" xml:space="preserve">
        <source>
### Agent data response

The agent data response is sent by the server back to agent after processing the agent data request.

| Field | Type | Mandatory | Value |
|-|-|-|--------|
| response | _string_ | yes | `success` \| `failed` |
| info | _string_ | yes | Item processing results. |

Example:
```json
{
  "response": "success",
  "info": "processed: 2; failed: 0; total: 2; seconds spent: 0.003534"
}
```</source>
      </trans-unit>
      <trans-unit id="70d7fe1b" xml:space="preserve">
        <source>
### Heartbeat message

The heartbeat message is sent by an active agent to Zabbix server/proxy 
every HeartbeatFrequency seconds (configured in the Zabbix agent 2 
[configuration file](/manual/appendix/config/zabbix_agent2)). 

It is used to monitor the availability of active checks.

```json
{
  "request": "active check heartbeat",
  "host": "Zabbix server",
  "heartbeat_freq": 60
}
```

| Field | Type | Mandatory | Value |
|-|-|-|--------|
| request | _string_ | yes | `active check heartbeat` |
| host | _string_ | yes | The host name. |
| heartbeat_freq | _number_ | yes | The agent heartbeat frequency (HeartbeatFrequency configuration parameter). |</source>
      </trans-unit>
    </body>
  </file>
</xliff>
