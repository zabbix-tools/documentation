[comment]: # translation:outdated

[comment]: # ({new-b03b9977})
# 6 关于“Include”参数的特别说明

[comment]: # ({/new-b03b9977})

[comment]: # ({new-15833d02})
### 6 Special notes on "Include" parameter

如果`Include` 参数用来包含一个文件， 该文件必须可读。 If an `Include`
parameter is used for including a file, the file must be readable.

如果`Include` 参数用来包含一个目录: If an `Include` parameter is used
for including a directory:

      - 该目录下所有文件必须可读。
      - 不考虑包含的特定顺序 (例如：文件不按字母顺序包含)。因此，不要在几个''Include''文件中定义一个相同参数(例如：以特定参数覆盖通用设置)。
      - 该目录下的所有文件都包含在配置文件中。
      - 注意一些文本编辑器会自动创建文件备份。 如, 如果编辑 ''include/my_specific.conf'' 会产生一个副本 ''include/my_specific_conf.BAK'' ，两个文件都会包含在内。请将''include/my_specific.conf.BAK'' 文件移除 。 在Linux系统中, ''Include'' 目录的内容可以通过''ls -al''命令来检查是否有不必要的文件。
      - All files in the directory must be readable.
      - No particular order of inclusion should be assumed (e.g. files are not included in alphabetical order). Therefore do not define one parameter in several ''Include'' files (e.g. to override a general setting with a specific one).
      - All files in the directory are included into configuration.
      - Beware of file backup copies automatically created by some text editors. For example, if editing the ''include/my_specific.conf'' file produces a backup copy ''include/my_specific_conf.BAK'' then both files will be included. Move ''include/my_specific.conf.BAK'' out of the "Include" directory. On Linux, contents of the ''Include'' directory can be checked with a ''ls -al'' command for unnecessary files.

如果`Include`参数使用模式来匹配包含的文件： If an `Include` parameter is
used for including files using a pattern:

      - 与模式匹配的所有文件都必须是可读的。
      - 不考虑包含的特定顺序 (例如：文件不按字母顺序包含)。因此，不要在几个''Include''文件中定义一个相同参数(例如：以特定参数覆盖通用设置)。
      - All files matching the pattern must be readable.
      - No particular order of inclusion should be assumed (e.g. files are not included in alphabetical order). Therefore do not define one parameter in several ''Include'' files (e.g. to override a general setting with a specific one).

[comment]: # ({/new-15833d02})


[comment]: # ({new-c13f85cf})
#### Notes on inclusion

If the `Include` parameter is used for including a file, the file must
be readable.

If the `Include` parameter is used for including a directory:

      - All files in the directory must be readable.
      - No particular order of inclusion should be assumed (e.g. files are not included in alphabetical order). Therefore do not define one parameter in several ''Include'' files (e.g. to override a general setting with a specific one).
      - All files in the directory are included into configuration.
      - Beware of file backup copies automatically created by some text editors. For example, if editing the ''include/my_specific.conf'' file produces a backup copy ''include/my_specific_conf.BAK'' then both files will be included. Move ''include/my_specific.conf.BAK'' out of the "Include" directory. On Linux, contents of the ''Include'' directory can be checked with a ''ls -al'' command for unnecessary files.

If the `Include` parameter is used for including files using a pattern:

      - All files matching the pattern must be readable.
      - No particular order of inclusion should be assumed (e.g. files are not included in alphabetical order). Therefore do not define one parameter in several ''Include'' files (e.g. to override a general setting with a specific one).

[comment]: # ({/new-c13f85cf})
