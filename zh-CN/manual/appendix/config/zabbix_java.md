[comment]: # attributes: notoc

[comment]: # translation:outdated

[comment]: # ({new-8bf9c5fc})
# 5 Zabbix Java 网关

### 5 Zabbix Java gateway

如果使用 `startup.sh` 和 `shutdown.sh` 脚本启动和停止 [Zabbix Java
网关](/manual/concepts/java),
那么就可以在`settings.sh`文件中指定必要的配置参数。startup 和shutdown
脚本以配置文件为输入源 ，并且将shell 变量 (第一列) 转换为相应的Java 属性
(第二列)。 If you use `startup.sh` and `shutdown.sh` scripts for
starting [Zabbix Java gateway](/manual/concepts/java), then you can
specify the necessary configuration parameters in file `settings.sh`.
The startup and shutdown scripts source the settings file and take care
of converting shell variables (listed in the first column) to Java
properties (listed in the second column).

如果通过手动运行`java`命令来起动Zabbix Java 网关,
可以通过命令行方式来指定Java属性. If you start Zabbix Java gateway
manually by running `java` directly, then you specify the corresponding
Java properties on the command line.

|变量       参|必须配|范围    默|值    描述|息|<|
|----------------|---------|-------------|-------------|---|-|
|Variable|Property|Mandatory|Range|Default|Description|

|   |   |   |   |   |   |
|---|---|---|---|---|---|
|LISTEN\_IP|zabbix.listenIP|否|<|.0.0.0|听IP。|
|LISTEN\_IP|zabbix.listenIP|no|<|0.0.0.0|IP address to listen on.|

|   |   |   |   |   |   |
|---|---|---|---|---|---|
|LISTEN\_PORT|zabbix.listenPort|否|024-32767|0052|听端口。|
|LISTEN\_PORT|zabbix.listenPort|no|1024-32767|10052|Port to listen on.|

|   |   |   |   |   |   |
|---|---|---|---|---|---|
|PID\_FILE|zabbix.pidFile|否|<|tmp/zabbix\_java.pid|ID文件的名称。 如果省略，Zabbix Java 网关将作为控制台应用程序启动。|
|PID\_FILE|zabbix.pidFile|no|<|/tmp/zabbix\_java.pid|Name of PID file. If omitted, Zabbix Java Gateway is started as a console application.|

|   |   |   |   |   |   |
|---|---|---|---|---|---|
|START\_POLLERS|zabbix.startPollers|否|-1000|<|动多少个轮询线程。|
|START\_POLLERS|zabbix.startPollers|no|1-1000|5|Number of worker threads to start.|

|   |   |   |   |   |   |
|---|---|---|---|---|---|
|TIMEOUT|zabbix.timeout|否|-30|<|络超时时间。 从Zabbix 2.0.15, 2.2.10 和 2.4.5开始支持该参数。|
|TIMEOUT|zabbix.timeout|no|1-30|3|How long to wait for network operations. This parameter is supported since Zabbix 2.0.15, 2.2.10 and 2.4.5.|

::: notewarning
端口10052没有[IANA
注册](http://www.iana.org/assignments/service-names-port-numbers/service-names-port-numbers.txt).
:::

::: notewarning
Port 10052 is not [IANA
registered](http://www.iana.org/assignments/service-names-port-numbers/service-names-port-numbers.txt).
:::

[comment]: # ({/new-8bf9c5fc})
