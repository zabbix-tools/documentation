[comment]: # translation:outdated

[comment]: # ({2ee52a19-6dcfe607})
#### Overview

下表包含Zabbix原生支持宏的完整列表。


::: notetip
要查看某个功能所支持的所有宏(例如，想要知道"map URL"功能中支持的所有宏)，你可以在浏览器的搜索框中输入这个功能的名称（用快捷键CTRL+F调出搜索框），然后点击“下一个”
:::

|宏名称|支持该宏的功能|具体描述|
|-----|------------|-----------|
|{ACTION.ID}|→ 基于触发器的通知和命令<br>→ 问题更新的通知和命令<br>→ 基于服务的通知和命令<br>→ 服务更新的通知和命令<br>→ 自动发现的通知和命令<br>→ 自动注册的通知和命令<br>→ 内部通知|*触发动作的数字ID*|
|{ACTION.NAME}|→ 基于触发器的通知和命令<br>→ 问题更新的通知和命令<br>→ 基于服务的通知和命令<br>→ 服务更新的通知和命令<br>→ 自动发现的通知和命令<br>→ 自动注册的通知和命令<br>→ 内部通知|*触发动作的名字*|
|{ALERT.MESSAGE}|→ 告警脚本参数|*配置“动作”时用到的“默认消息”*<br>从3.0.0版本开始支持|
|{ALERT.SENDTO}|→ 告警脚本参数|*'配置“媒体”时用到的“发送至”*<br>从3.0.0版本开始支持|
|{ALERT.SUBJECT}|→ 告警脚本参数|*配置“动作”时用到的“默认主题”*<br>从3.0.0版本开始支持|
|{DATE}|→ 基于触发器的通知和命令<br>→ 问题更新的通知和命令<br>→ 基于服务的通知和命令<br>→ 服务更新的通知和命令<br>→ 自动发现的通知和命令<br>→ 自动注册的通知和命令<br>→ 内部通知<br>→ 手动动作[脚本](/manual/web_interface/frontend_sections/administration/scripts)|*日期格式为yyyy.mm.dd*|
|{DISCOVERY.DEVICE.IPADDRESS}|→ 自动发现的通知和命令|*被发现的设备的IP地址*<br>永久有效，不依赖于是否添加了设备|
|{DISCOVERY.DEVICE.DNS}|→ 自动发现的通知和命令|*被发现的设备的DNS名称*<br>永久有效，不依赖于是否添加了设备|
|{DISCOVERY.DEVICE.STATUS}|→ 自动发现的通知和命令|*被发现设备的状态*: 可能是 UP 或者 DOWN.|
|{DISCOVERY.DEVICE.UPTIME}|→ 自动发现的通知和命令|*某设备最近一次被监控到状态改变的时间。*, 精确到秒<br>例如： 1h 29m 01s.<br>对于状态是DOWN的设备来说，这就是他们变成关机状态的时间长度|
|{DISCOVERY.RULE.NAME}|→ 自动发现的通知和命令|*用于发现设备或服务是否在线的自动发现规则的名称*|
|{DISCOVERY.SERVICE.NAME}|→ 自动发现的通知和命令|*自动发现的服务的名称*<br>例如： HTTP.|
|{DISCOVERY.SERVICE.PORT}|→ 自动发现的通知和命令|*自动发现的服务的端口*<br>例如： 80.|
|{DISCOVERY.SERVICE.STATUS}|→ 自动发现的通知和命令|*自动发现的 <服务://> 状态，可能是UP 或者 DOWN \| \|{DISCOVERY.SERVICE.UPTIME} \|→ 自动发现的通知和命令 \|*该服务自动发现状态最近改变的时间*, 精确到秒<br>例如： 1h 29m 01s.<br>对于状态是DOWN的服务，是该服务不可用的时间\| \|{ESC.HISTORY} \|→ 基于触发器的通知和命令<br>→ 问题更新的通知和命令<br>→ 基于服务的通知和命令<br>→ 服务更新的通知和命令<br>→ 内部通知 \|*升级history.Log 过去发送的消息。*<br>显示之前发送过的通知，通知是基于什么升级步骤发送的，以及他们的状态 (*已发送, *进行中* or *失败*).|
|{EVENT.ACK.STATUS}|→ 基于触发器的通知和命令<br>→ 问题更新的通知和命令<br>→ 手动动作[脚本](/manual/web_interface/frontend_sections/administration/scripts)|*事件确认发状态(是/否)*.|
|{EVENT.AGE}|→ 基于触发器的通知和命令<br>→ 问题更新的通知和命令<br>→ 基于服务的通知和命令<br>→ 服务更新的通知和命令<br>→ 服务恢复通知和命令<br>→ 自动发现的通知和命令<br>→ 自动注册的通知和命令<br>→ 内部通知<br>→ 手动动作[脚本](/manual/web_interface/frontend_sections/administration/scripts)|*触发动作的事件的时长*, 精确到秒<br>在升级消息时有用|
|{EVENT.DATE}|→ 基于触发器的通知和命令<br>→ 问题更新的通知和命令<br>→ 基于服务的通知和命令<br>→ 服务更新的通知和命令<br>→ 服务恢复通知和命令<br>→ 自动发现的通知和命令<br>→ 自动注册的通知和命令<br>→ 内部通知<br>→ 手动动作[脚本](/manual/web_interface/frontend_sections/administration/scripts)|*触发动作的事件的日期*|
|{EVENT.DURATION}|→ 基于触发器的通知和命令<br>→ 问题更新的通知和命令<br>→ 基于服务的通知和命令<br>→ 服务更新的通知和命令<br>→ 服务恢复通知和命令<br>→ 内部通知<br>→ 手动动作[脚本](/manual/web_interface/frontend_sections/administration/scripts)|*事件持续时间（发生问题和恢复事件之间的时差）*, 精确到秒<br>在问题恢复信息中有用<br><br>从 5.0.0版本开始支持|
|{EVENT.ID}|→ 基于触发器的通知和命令<br>→ 问题更新的通知和命令<br>→ 基于服务的通知和命令<br>→ 服务更新的通知和命令<br>→ 服务恢复通知和命令<br>→ 自动发现的通知和命令<br>→ 自动注册的通知和命令<br>→ 内部通知<br>→ 触发器 URLs<br>→ 手动动作[脚本](/manual/web_interface/frontend_sections/administration/scripts)|*触发动作的事件的数字ID*|
|{EVENT.NAME}|→ 基于触发器的通知和命令<br>→ 问题更新的通知和命令<br>→ 基于服务的通知和命令<br>→ 服务更新的通知和命令<br>→ 服务恢复通知和命令<br>→ 内部通知<br>→ 手动动作[脚本](/manual/web_interface/frontend_sections/administration/scripts)|*触发动作的问题事件名称*<br>从4.0.0版本开始支持|
|{EVENT.NSEVERITY}|→ 基于触发器的通知和命令<br>→ 问题更新的通知和命令<br>→ 基于服务的通知和命令<br>→ 服务更新的通知和命令<br>→ 服务恢复通知和命令<br>→ 手动动作[脚本](/manual/web_interface/frontend_sections/administration/scripts)|*描述事件严重性的数值* 包括: 0 - 未分级, 1 - 信息, 2 - 警告, 3 - 普通, 4 - 高, 5 - 灾难.<br>从4.0.0版本开始支持|
|{EVENT.OBJECT}|→ 基于触发器的通知和命令<br>→ 问题更新的通知和命令<br>→ 基于服务的通知和命令<br>→ 服务更新的通知和命令<br>→ 服务恢复通知和命令<br>→ 自动发现的通知和命令<br>→ 自动注册的通知和命令<br>→ 内部通知<br>→ 手动动作[脚本](/manual/web_interface/frontend_sections/administration/scripts)|*描述事件对象的数值* 包括：0 - 触发器, 1 - 自动发现的主机, 2 - 自动发现的服务, 3 - 低级自动发现规则, 4 - Item, 5 - 低级自动发现规则.<br>从4.4.0版本开始支持|
|{EVENT.OPDATA}|→ 基于触发器的通知和命令<br>→ 问题更新的通知和命令<br>→ 手动动作[脚本](/manual/web_interface/frontend_sections/administration/scripts)|*问题对应触发器的当前值*<br>从4.4.0版本开始支持 |
|{EVENT.RECOVERY.DATE}|→ 问题[恢复通知](/manual/config/notifications/action/recovery_operations) 和命令<br>→ 问题更新的通知和命令 (如果恢复了)<br>→ 服务恢复通知和命令<br>→ 手动动作[脚本](/manual/web_interface/frontend_sections/administration/scripts) (如果恢复了)|*事件恢复日期*|
|{EVENT.RECOVERY.ID}|→ 问题[恢复通知](/manual/config/notifications/action/recovery_operations) 和命令<br>→ 问题更新的通知和命令 (如果恢复了)<br>→ 服务恢复通知和命令<br>→ 手动动作[脚本](/manual/web_interface/frontend_sections/administration/scripts) (如果恢复了)|*恢复事件的数字ID*|
|{EVENT.RECOVERY.NAME}|→ 问题[恢复通知](/manual/config/notifications/action/recovery_operations) 和命令<br>→ 问题更新的通知和命令 (如果恢复了)<br>→ 服务恢复通知和命令<br>→ 手动动作[脚本](/manual/web_interface/frontend_sections/administration/scripts) (如果恢复了)|*恢复时间名称*<br>从4.4.1版本开始支持|
|{EVENT.RECOVERY.STATUS}|→ 问题[恢复通知](/manual/config/notifications/action/recovery_operations) 和命令<br>→ 问题更新的通知和命令 (如果恢复了)<br>→ 服务恢复通知和命令<br>→ 手动动作[脚本](/manual/web_interface/frontend_sections/administration/scripts) (如果恢复了)|*恢复事件的文字描述*|
|{EVENT.RECOVERY.TAGS}|→ 问题[恢复通知](/manual/config/notifications/action/recovery_operations) 和命令<br>→ 问题更新的通知和命令 (如果恢复了)<br>→ 服务恢复通知和命令<br>→ 手动动作[脚本](/manual/web_interface/frontend_sections/administration/scripts) (如果恢复了)|*用逗号分隔的恢复事件标签* 如果没有标签的，就是一个空字符<br>从3.2.0版本开始支持|
|{EVENT.RECOVERY.TAGSJSON}|→ 问题[恢复通知](/manual/config/notifications/action/recovery_operations) 和命令<br>→ 问题更新的通知和命令 (如果恢复了)<br>→ 服务恢复通知和命令<br>→ 手动动作[脚本](/manual/web_interface/frontend_sections/administration/scripts) (如果恢复了)|*一个包括事件标签[对象](/manual/api/reference/event/object#event_tag)的JSON数组.* 如果没有标签的，就是一个空数组<br>从5.0.0版本开始支持|
|{EVENT.RECOVERY.TIME}|→ 问题[恢复通知](/manual/config/notifications/action/recovery_operations) 和命令<br>→ 问题更新的通知和命令 (如果恢复了)<br>→ 服务恢复通知和命令<br>→ 手动动作[脚本](/manual/web_interface/frontend_sections/administration/scripts) (如果恢复了)|*恢复时间的时间.*|
|{EVENT.RECOVERY.VALUE}|→ 问题[恢复通知](/manual/config/notifications/action/recovery_operations) 和命令<br>→ 问题更新的通知和命令 (如果恢复了)<br>→ 服务恢复通知和命令<br>→ 手动动作[脚本](/manual/web_interface/frontend_sections/administration/scripts) (如果恢复了)|*恢复时间的数值*|
|{EVENT.SEVERITY}|→ 基于触发器的通知和命令<br>→ 问题更新的通知和命令<br>→ 基于服务的通知和命令<br>→ 服务更新的通知和命令<br>→ 服务恢复通知和命令<br>→ 手动动作[脚本](/manual/web_interface/frontend_sections/administration/scripts)|*时间严重性等级的名称*<br>从4.0.0版本开始支持|
|{EVENT.SOURCE}|→ 基于触发器的通知和命令<br>→ 问题更新的通知和命令<br>→ 基于服务的通知和命令<br>→ 服务更新的通知和命令<br>→ 服务恢复通知和命令<br>→ 自动发现的通知和命令<br>→ 自动注册的通知和命令<br>→ 内部通知<br>→ 手动动作[脚本](/manual/web_interface/frontend_sections/administration/scripts)|*事件来源的数字描述* 包括：0 - 触发器, 1 - 自动发现, 2 - 自动注册, 3 - 内部.<br>从4.0.0版本开始支持|
|{EVENT.STATUS}|→ 基于触发器的通知和命令<br>→ 问题更新的通知和命令<br>→ 基于服务的通知和命令<br>→ 服务更新的通知和命令<br>→ 服务恢复通知和命令<br>→ 内部通知<br>→ 手动动作[脚本](/manual/web_interface/frontend_sections/administration/scripts)|*出发动作的事件的文字描述*|
|{EVENT.TAGS}|→ 基于触发器的通知和命令<br>→ 问题更新的通知和命令<br>→ 基于服务的通知和命令<br>→ 服务更新的通知和命令<br>→ 服务恢复通知和命令<br>→ 手动动作[脚本](/manual/web_interface/frontend_sections/administration/scripts)|*一组逗号分割单时间标签.* 如果没有标签的，就是一个空字符<br>从3.2.0版本开始支持|
|{EVENT.TAGSJSON}|→ 基于触发器的通知和命令<br>→ 问题更新的通知和命令<br>→ 基于服务的通知和命令<br>→ 服务更新的通知和命令<br>→ 服务恢复通知和命令<br>→ 手动动作[脚本](/manual/web_interface/frontend_sections/administration/scripts)|*一个包括事件标签[对象](/manual/api/reference/event/object#event_tag)的JSON数组.* 如果没有标签的，就是一个空数组<br>从5.0.0版本开始支持|
|{EVENT.TAGS.<标签名称>}|→ 基于触发器的通知和命令<br>→ 问题更新的通知和命令<br>→ 基于服务的通知和命令<br>→ 服务更新的通知和命令<br>→ 服务恢复通知和命令<br>→ Wehook媒介URL名称和URL<br>→ 手动动作[脚本](/manual/web_interface/frontend_sections/administration/scripts)|*用标签名称来引用的事件标签值.*<br>如果标签名称包含非字母数字的字符（包括非英语多字节UTF字符）需用双引号括起来。标签名称中引号括起来的引号和反斜杠必须用反斜杠转义。<br>从4.4.2版本开始支持|
|{EVENT.TIME}|→ 基于触发器的通知和命令<br>→ 问题更新的通知和命令<br>→ 基于服务的通知和命令<br>→ 服务更新的通知和命令<br>→ 服务恢复通知和命令<br>→ 自动发现的通知和命令<br>→ 自动注册的通知和命令<br>→ 内部通知<br>→ 手动动作[脚本](/manual/web_interface/frontend_sections/administration/scripts)|* 触发动作的事件时间*|
|{EVENT.UPDATE.ACTION}|→ 问题更新的通知和命令|[问题更新](/manual/acknowledges#updating_problems)期间执行的*可读的动作名称* .<br>包括这些值: *已确认*, *已注释*, *严重级别从 (原始严重级别) 变化为 (更新严重级别)* 和 *已关闭* (取决于一次更新了多少个动作).<br>从4.0.0版本开始支持|
|{EVENT.UPDATE.DATE}|→ 问题更新的通知和命令<br>→ 服务更新的通知和命令|*事件[更新](/manual/config/notifications/action/update_operations) 的日期(例如：确认，等等).*<br>过去使用 {ACK.DATE}，当前版本已弃用|
|{EVENT.UPDATE.HISTORY}|→ 基于触发器的通知和命令<br>→ 问题更新的通知和命令<br>→ 手动动作[脚本](/manual/web_interface/frontend_sections/administration/scripts)|*问题更新(例如：确认，等等)的日志*<br>过去使用 {EVENT.ACK.HISTORY}，当前版本已弃用|
|{EVENT.UPDATE.MESSAGE}|→ 问题更新的通知和命令|*问题更新的消息。*<br>过去使用 {ACK.MESSAGE}，当前版本已弃用|
|{EVENT.UPDATE.STATUS}|→ 基于触发器的通知和命令<br>→ 问题更新的通知和命令<br>→ 手动动作[脚本](/manual/web_interface/frontend_sections/administration/scripts)|*问题更新状态的数值。* 包括： 0 - 因为问题/恢复事件触发了Webhook, 1 - 更新操作<br>从4.4.0版本开始支持|
|{EVENT.UPDATE.TIME}|→ 问题更新的通知和命令<br>→ 服务更新的通知和命令|*事件[更新](/manual/config/notifications/action/update_operations) (例如：确认，等等)的时间*<br>过去使用 {ACK.TIME}，当前版本已弃用|
|{EVENT.VALUE}|→ 基于触发器的通知和命令<br>→ 问题更新的通知和命令<br>→ 基于服务的通知和命令<br>→ 服务更新的通知和命令<br>→ 服务恢复通知和命令<br>→ 内部通知<br>→ 手动动作[脚本](/manual/web_interface/frontend_sections/administration/scripts)|*触发动作的事件的数值 (1 表示问题, 0 表示恢复)。*|
|{FUNCTION.VALUE<1-9>}|→ 基于触发器的通知和命令<br>→ 问题更新的通知和命令<br>→ 手动动作[脚本](/manual/web_interface/frontend_sections/administration/scripts)<br>→ 时间名称|*在发生事件的时刻，触发器表达式中，基于监控项的第N个函数的值*<br>只包括以*/host/key* 开始的函数. 具体参见 [宏索引](supported_by_location#indexed_macros)|
|{FUNCTION.RECOVERY.VALUE<1-9>}|→ 问题[恢复通知](/manual/config/notifications/action/recovery_operations) 和命令<br>→ 问题更新的通知和命令<br>→ 手动动作[脚本](/manual/web_interface/frontend_sections/administration/scripts)|*在发生事件的时刻，恢复表达式中，基于监控项的第N个函数的值*<br>只包括以*/host/key* 开始的函数. 具体参见 [宏索引](supported_by_location#indexed_macros).|
|{HOST.CONN}|→ 基于触发器的通知和命令<br>→ 问题更新的通知和命令<br>→ 内部通知<br>→ 地图元素标签，地图URL名称和值<br>→ 监控项键值参数^[1](supported_by_location#footnotes)^<br>→ 主机界面IP/DNS<br>→ Trapper 监控项 "主机范围" 字段<br>→ 数据库监控附加参数<br>→ SSH和Telnet脚本<br>→ JMX监控项端点字段<br>→ Web监控^[4](supported_by_location#footnotes)^<br>→ 低级自动发现规则过滤正则表达式<br>→ 仪表盘小部件动态URL的URL字段<br>→ 触发器名称, 时间名称, 当前操作数据和描述<br>→ 触发器URL<br>→ [标签名称和赋值](/manual/config/tagging#macro_support)<br>→ 脚本类型监控项，监控项原型和自动发现规则参数名称和赋值<br>→ HTTP agent类型监控项，监控项原型和自动发现规则字段:<br>URL, 查询字段, 请求体, 请求头, 代理, SSL证书文件, SSL key文件, 主机白名单.<br>→ 手动主机动作 [脚本](/manual/web_interface/frontend_sections/administration/scripts) （包括确认文本）<br>→ 手动动作[脚本](/manual/web_interface/frontend_sections/administration/scripts) （包括确认文本）|*依赖于主机设置的主机IP地址或DNS名称*^[2](supported_by_location#footnotes)^.<br><br>在触发器表达式中，可以用数字索引，来指向第一个，第二个，第三个主机，等等。例如：{HOST.CONN[<1-9>](supported_by_location#indexed_macros)}  详见 [索引宏](supported_by_location#indexed_macros).|
|{HOST.DESCRIPTION}|→ 基于触发器的通知和命令<br>→ 问题更新的通知和命令<br>→ 内部通知<br>→ 地图元素标签<br>→ 手动动作[脚本](/manual/web_interface/frontend_sections/administration/scripts)|*主机描述.*<br><br>在触发器表达式中，该宏可用配合数字索引使用，来指向第一个，第二个，第三个主机，等等。例如： {HOST.DESCRIPTION[<1-9>](supported_by_location#indexed_macros)} 详见[索引宏](supported_by_location#indexed_macros).|
|{HOST.DNS}|→ 基于触发器的通知和命令<br>→ 问题更新的通知和命令<br>→ 内部通知<br>→ 地图元素标签，地图URL名称和值<br>→ 监控项键值参数^[1](supported_by_location#footnotes)^<br>→ 主机界面IP/DNS<br>→ Trapper 监控项 "主机范围" 字段<br>→ 数据库监控附加参数<br>→ SSH和Telnet脚本<br>→ JMX监控项端点字段<br>→ Web监控^[4](supported_by_location#footnotes)^<br>→ 低级自动发现规则过滤正则表达式<br>→ 仪表盘小部件中，动态 URL的URL字段 <br>→ 触发器名称, 时间名称, 当前操作数据和描述<br>→ 触发器 URLs<br>→ [标签名称和赋值](/manual/config/tagging#macro_support)<br>→ 脚本类型监控项，监控项原型和自动发现规则参数名称和赋值<br>→ HTTP agent类型监控项，监控项原型和自动发现规则字段:<br>URL, 查询字段, 请求体, 请求头, 代理, SSL证书文件, SSL key文件, 主机白名单.<br>→ 手动主机动作 [脚本](/manual/web_interface/frontend_sections/administration/scripts) （包括确认文本）<br>→ 手动动作[脚本](/manual/web_interface/frontend_sections/administration/scripts) （包括确认文本）|*主机DNS名称*^[2](supported_by_location#footnotes)^.<br><br>T在触发器表达式中，该宏可用配合数字索引使用，来指向第一个，第二个，第三个主机，等等。例如：{HOST.DNS[<1-9>](supported_by_location#indexed_macros)} 详见 [索引宏](supported_by_location#indexed_macros).|
|{HOST.HOST}|→ 基于触发器的通知和命令<br>→ 问题更新的通知和命令<br>→ 自动注册的通知和命令<br>→ 内部通知<br>→ 监控项键值参数<br>→ 地图元素标签，地图URL名称和值<br>→ 主机界面IP/DNS<br>→ Trapper 监控项 "主机范围" 字段<br>→ 数据库监控附加参数<br>→ SSH和Telnet脚本<br>→ JMX监控项端点字段<br>→ Web监控^[4](supported_by_location#footnotes)^<br>→ 低级自动发现规则过滤正则表达式<br>→ 仪表盘小部件动态URL的URL字段<br>→ 触发器名称, 时间名称, 当前操作数据和描述<br>→ 触发器 URLs<br>→ [标签名称和赋值](/manual/config/tagging#macro_support)<br>→ 脚本类型监控项，监控项原型和自动发现规则参数名称和赋值<br>→ HTTP agent类型监控项，监控项原型和自动发现规则字段:<br>URL, 查询字段, 请求体, 请求头, 代理, SSL证书文件, SSL key文件, 主机白名单.<br>→ 手动主机动作 [脚本](/manual/web_interface/frontend_sections/administration/scripts) （包括确认文本）<br>→ 手动动作[脚本](/manual/web_interface/frontend_sections/administration/scripts) （包括确认文本）|*主机名称*<br>在触发器表达式中，可以用数字索引，来指向第一个，第二个，第三个主机，等等。例如： {HOST.HOST[<1-9>](supported_by_location#indexed_macros)} 详见 [索引宏](supported_by_location#indexed_macros).<br><br> 过去使用 `{HOSTNAME<1-9>}`，当前版本已弃用|
|{HOST.ID}|→ 基于触发器的通知和命令<br>→ 问题更新的通知和命令<br>→ 内部通知<br>→ 地图元素标签，地图URL名称和值<br>→ 仪表盘小部件动态URL的URL字段<br>→ 触发器URL<br>→ [标签名称和赋值](/manual/config/tagging#macro_support)<br>→ 手动动作[脚本](/manual/web_interface/frontend_sections/administration/scripts)|*主机 ID.*<br><br>在触发器表达式中，可以用数字索引，来指向第一个，第二个，第三个主机，等等。例如：{HOST.ID[<1-9>](supported_by_location#indexed_macros)} 详见[索引宏](supported_by_location#indexed_macros).|
|{HOST.IP}|→ 基于触发器的通知和命令<br>→ 问题更新通知和命令<br>→ 自动注册通知和命令<br>→ 内部通知<br>→ 映射元素标签，映射URL名称和值<br>→ 监控项关键参数^[1](supported_by_location#脚注)^<br>→ 主机接口IP/DNS<br>→ 触发器监控项 "允许的主机" 字段<br>→ 数据库监控附加字段<br>→ SSH和Telnet脚本<br>→ JMX项目终结点字段<br>→ Web监控^[4](supported_by_location#脚注)^<br>→ 低级别自动发现过滤正则表达式<br>→ 动态URL仪表盘组件的URL字段<br>→ 触发器名称，事件名称，操作型数据和描述<br>→ 触发器URL<br>→ [标签名称和值](/manual/config/tagging#macro_support)<br>→ 脚本类型监控项，监控项原型和发现规则参数名称和值<br>→ HTTP代理类型监控项，监控项原型和发现规则字段：<br>URL，查询字段，请求正文，报头，代理，SSL证书文件，SSL密钥文件，允许的主机。.<br>→ 自定义主机操作 [scripts](/manual/web_interface/frontend_sections/administration/scripts) (including confirmation text)<br>→ 自定义事件操作 [scripts](/manual/web_interface/frontend_sections/administration/scripts) (including confirmation text)|*主机IP地址*^[2](supported_by_location#脚注)^.<br><br>这个宏可以和一个数字索引一起使用，例如： {HOST.IP[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中的第1个、第2个、第3个等主机。参阅 [indexed macros](supported_by_location#indexed_macros).<br><br>`{IPADDRESS<1-9>}` 已弃用。|
|{HOST.METADATA}|→ 自动注册通知和命令|*主机元数据。*<br>仅适用活动代理自动注册。|
|{HOST.NAME}|→ 基于触发器的通知和命令<br>→ 问题更新通知和命令<br>→ 内部通知<br>→ 映射元素标签，映射URL名称和值<br>→ 监控项关键参数<br>→ 主机接口IP/DNS<br>→ 触发器监控项 "允许的主机" 字段<br>→ 数据库监控附加字段<br>→ SSH和Telnet脚本<br>→ Web监控^[4](supported_by_location#脚注)^<br>→ 低级别自动发现过滤正则表达式<br>→ 动态URL仪表盘组件的URL字段<br>→ 触发器名称，事件名称，操作型数据和描述<br>→ 触发器URL<br>→ [标签名称和值](/manual/config/tagging#macro_support)<br>→ 脚本类型监控项，监控项原型和发现规则参数名称和值<br>→ HTTP代理类型监控项，监控项原型和发现规则字段：<br>URL，查询字段，请求正文，报头，代理，SSL证书文件，SSL密钥文件，允许的主机。.<br>→ 自定义主机操作 [scripts](/manual/web_interface/frontend_sections/administration/scripts) (including confirmation text)<br>→ 自定义事件操作 [scripts](/manual/web_interface/frontend_sections/administration/scripts) (including confirmation text)|*可见的主机名。*<br><br>这个宏可以和一个数字索引一起使用，例如： {HOST.NAME[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中的第1个、第2个、第3个等主机。参阅 [indexed macros](supported_by_location#indexed_macros).|
|{HOST.PORT}|→ 基于触发器的通知和命令<br>→ 问题更新通知和命令<br>→ 自动注册通知和命令<br>→ 内部通知<br>→ 触发器名称，事件名称，操作型数据和描述<br>→ 触发器URL<br>→ JMX项目终结点字段<br>→ [标签名称和值](/manual/config/tagging#macro_support)<br>→ 自定义事件操作 [scripts](/manual/web_interface/frontend_sections/administration/scripts)|*主机（代理）端口 *^[2](supported_by_location#脚注)^.<br><br>这个宏可以和一个数字索引一起使用，例如： {HOST.PORT[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中的第1个、第2个、第3个等主机。参阅 [indexed macros](supported_by_location#indexed_macros).|
|{HOST.TARGET.CONN}|→ 基于触发器的命令<br>→ 问题更新命令<br>→ 发现命令<br>→ 自动注册命令|目标主机IP地址或DNS名称，取决于主机设置。<br>从5.4.0开始支持。|
|{HOST.TARGET.DNS}|→ 基于触发器的命令<br>→ 问题更新命令<br>→ 发现命令<br>→ 自动注册命令|目标主机的DNS名称。<br>从5.4.0开始支持。|
|{HOST.TARGET.HOST}|→ 基于触发器的命令<br>→ 问题更新命令<br>→ 发现命令<br>→ 自动注册命令|目标主机的技术名称。<br>从5.4.0开始支持。|
|{HOST.TARGET.IP}|→ 基于触发器的命令<br>→ 问题更新命令<br>→ 发现命令<br>→ 自动注册命令|目标主机的IP地址。<br>从5.4.0开始支持。|
|{HOST.TARGET.NAME}|→ 基于触发器的命令<br>→ 问题更新命令<br>→ 发现命令<br>→ 自动注册命令|目标主机的技术名称。<br>从5.4.0开始支持。|
|{HOSTGROUP.ID}|→ 映射元素标签，映射URL名称和值|*主机组ID。*|
|{INVENTORY.ALIAS}|→ 基于触发器的通知和命令<br>→ 问题更新通知和命令<br>→ 内部通知<br>→ [标签名称和值](/manual/config/tagging#macro_support)<br>→ 映射元素标签，映射URL名称和值<br>→ 自定义事件操作 [scripts](/manual/web_interface/frontend_sections/administration/scripts)|*主机资产记录中的别名字段。*<br><br>这个宏可以和一个数字索引一起使用，例如： {INVENTORY.ALIAS[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中的第1个、第2个、第3个等主机。参阅 [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.ASSET.TAG}|→ 基于触发器的通知和命令<br>→ 问题更新通知和命令<br>→ 内部通知<br>→ [标签名称和值](/manual/config/tagging#macro_support)<br>→ 映射元素标签，映射URL名称和值<br>→ 自定义事件操作 [scripts](/manual/web_interface/frontend_sections/administration/scripts)|*主机资产记录中的资产标签字段。*<br><br>这个宏可以和一个数字索引一起使用，例如： {INVENTORY.ASSET.TAG[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中的第1个、第2个、第3个等主机。参阅 [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.CHASSIS}|→ 基于触发器的通知和命令<br>→ 问题更新通知和命令<br>→ 内部通知<br>→ [标签名称和值](/manual/config/tagging#macro_support)<br>→ 映射元素标签，映射URL名称和值<br>→ 自定义事件操作 [scripts](/manual/web_interface/frontend_sections/administration/scripts)|*主机资产记录中的机壳字段。*<br><br>这个宏可以和一个数字索引一起使用，例如： {INVENTORY.CHASSIS[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中的第1个、第2个、第3个等主机。参阅 [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.CONTACT}|→ 基于触发器的通知和命令<br>→ 问题更新通知和命令<br>→ 内部通知<br>→ [标签名称和值](/manual/config/tagging#macro_support)<br>→ 映射元素标签，映射URL名称和值<br>→ 自定义事件操作 [scripts](/manual/web_interface/frontend_sections/administration/scripts)|*主机资产记录中的联系字段。*<br><br>这个宏可以和一个数字索引一起使用，例如： {INVENTORY.CONTACT[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中的第1个、第2个、第3个等主机。参阅 [indexed macros](supported_by_location#indexed_macros).<br><br>`{PROFILE.CONTACT<1-9>}` 已弃用。|
|{INVENTORY.CONTRACT.NUMBER}|→ 基于触发器的通知和命令<br>→ 问题更新通知和命令<br>→ 内部通知<br>→ [标签名称和值](/manual/config/tagging#macro_support)<br>→ 映射元素标签，映射URL名称和值<br>→ 自定义事件操作 [scripts](/manual/web_interface/frontend_sections/administration/scripts)|*主机资产记录中的合同号码字段。*<br><br>这个宏可以和一个数字索引一起使用，例如： {INVENTORY.CONTRACT.NUMBER[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中的第1个、第2个、第3个等主机。参阅 [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.DEPLOYMENT.STATUS}|→ 基于触发器的通知和命令<br>→ 问题更新通知和命令<br>→ 内部通知<br>→ [标签名称和值](/manual/config/tagging#macro_support)<br>→ 映射元素标签，映射URL名称和值<br>→ 自定义事件操作 [scripts](/manual/web_interface/frontend_sections/administration/scripts)|*主机资产记录中的部署状态字段。*<br><br>这个宏可以和一个数字索引一起使用，例如： {INVENTORY.DEPLOYMENT.STATUS[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中的第1个、第2个、第3个等主机。参阅 [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.HARDWARE}|→ 基于触发器的通知和命令<br>→ 问题更新通知和命令<br>→ 内部通知<br>→ [标签名称和值](/manual/config/tagging#macro_support)<br>→ 映射元素标签，映射URL名称和值<br>→ 自定义事件操作 [scripts](/manual/web_interface/frontend_sections/administration/scripts)|*主机资产记录中的硬件字段。*<br><br>这个宏可以和一个数字索引一起使用，例如： {INVENTORY.HARDWARE[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中的第1个、第2个、第3个等主机。参阅 [indexed macros](supported_by_location#indexed_macros).<br><br>`{PROFILE.HARDWARE<1-9>}` 已弃用。|
|{INVENTORY.HARDWARE.FULL}|→ 基于触发器的通知和命令<br>→ 问题更新通知和命令<br>→ 内部通知<br>→ [标签名称和值](/manual/config/tagging#macro_support)<br>→ 映射元素标签，映射URL名称和值<br>→ 自定义事件操作 [scripts](/manual/web_interface/frontend_sections/administration/scripts)|*主机资产记录中的硬件(全细节)字段。*<br><br>这个宏可以和一个数字索引一起使用，例如： {INVENTORY.HARDWARE.FULL[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中的第1个、第2个、第3个等主机。参阅 [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.HOST.NETMASK}|→ 基于触发器的通知和命令<br>→ 问题更新通知和命令<br>→ 内部通知<br>→ [标签名称和值](/manual/config/tagging#macro_support)<br>→ 映射元素标签，映射URL名称和值<br>→ 自定义事件操作 [scripts](/manual/web_interface/frontend_sections/administration/scripts)|*主机资产记录中的主机子网掩码字段。*<br><br>这个宏可以和一个数字索引一起使用，例如： {INVENTORY.HOST.NETMASK[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中的第1个、第2个、第3个等主机。参阅 [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.HOST.NETWORKS}|→ 基于触发器的通知和命令<br>→ 问题更新通知和命令<br>→ 内部通知<br>→ [标签名称和值](/manual/config/tagging#macro_support)<br>→ 映射元素标签，映射URL名称和值<br>→ 自定义事件操作 [scripts](/manual/web_interface/frontend_sections/administration/scripts)|*主机资产记录中的主机网络字段。*<br><br>这个宏可以和一个数字索引一起使用，例如： {INVENTORY.HOST.NETWORKS[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中的第1个、第2个、第3个等主机。参阅 [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.HOST.ROUTER}|→ 基于触发器的通知和命令<br>→ 问题更新通知和命令<br>→ 内部通知<br>→ [标签名称和值](/manual/config/tagging#macro_support)<br>→ 映射元素标签，映射URL名称和值<br>→ 自定义事件操作 [scripts](/manual/web_interface/frontend_sections/administration/scripts)|*主机资产记录中的主机路由字段。*<br><br>这个宏可以和一个数字索引一起使用，例如： {INVENTORY.HOST.ROUTER[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中的第1个、第2个、第3个等主机。参阅 [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.HW.ARCH}|→ 基于触发器的通知和命令<br>→ 问题更新通知和命令<br>→ 内部通知<br>→ [标签名称和值](/manual/config/tagging#macro_support)<br>→ 映射元素标签，映射URL名称和值<br>→ 自定义事件操作 [scripts](/manual/web_interface/frontend_sections/administration/scripts)|*主机资产记录中的硬件架构字段。*<br><br>这个宏可以和一个数字索引一起使用，例如： {INVENTORY.HW.ARCH[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中的第1个、第2个、第3个等主机。参阅 [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.HW.DATE.DECOMM}|→ 基于触发器的通知和命令<br>→ 问题更新通知和命令<br>→ 内部通知<br>→ [标签名称和值](/manual/config/tagging#macro_support)<br>→ 映射元素标签，映射URL名称和值<br>→ 自定义事件操作 [scripts](/manual/web_interface/frontend_sections/administration/scripts)|*主机资产记录中的硬件退役日期字段。*<br><br>这个宏可以和一个数字索引一起使用，例如： {INVENTORY.HW.DATE.DECOMM[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中的第1个、第2个、第3个等主机。参阅 [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.HW.DATE.EXPIRY}|→ 基于触发器的通知和命令<br>→ 问题更新通知和命令<br>→ 内部通知<br>→ [标签名称和值](/manual/config/tagging#macro_support)<br>→ 映射元素标签，映射URL名称和值<br>→ 自定义事件操作 [scripts](/manual/web_interface/frontend_sections/administration/scripts)|*主机资产记录中的硬件维修过期日期字段。*<br><br>这个宏可以和一个数字索引一起使用，例如： {INVENTORY.HW.DATE.EXPIRY[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中的第1个、第2个、第3个等主机。参阅 [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.HW.DATE.INSTALL}|→ 基于触发器的通知和命令<br>→ 问题更新通知和命令<br>→ 内部通知<br>→ [标签名称和值](/manual/config/tagging#macro_support)<br>→ 映射元素标签，映射URL名称和值<br>→ 自定义事件操作 [scripts](/manual/web_interface/frontend_sections/administration/scripts)|*主机资产记录中的硬件安装日期字段。*<br><br>这个宏可以和一个数字索引一起使用，例如： {INVENTORY.HW.DATE.INSTALL[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中的第1个、第2个、第3个等主机。参阅 [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.HW.DATE.PURCHASE}|→ 基于触发器的通知和命令<br>→ 问题更新通知和命令<br>→ 内部通知<br>→ [标签名称和值](/manual/config/tagging#macro_support)<br>→ 映射元素标签，映射URL名称和值<br>→ 自定义事件操作 [scripts](/manual/web_interface/frontend_sections/administration/scripts)|*主机资产记录中的硬件购买日期字段。*<br><br>这个宏可以和一个数字索引一起使用，例如： {INVENTORY.HW.DATE.PURCHASE[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中的第1个、第2个、第3个等主机。参阅 [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.INSTALLER.NAME}|→ 基于触发器的通知和命令<br>→ 问题更新通知和命令<br>→ 内部通知<br>→ [标签名称和值](/manual/config/tagging#macro_support)<br>→ 映射元素标签，映射URL名称和值<br>→ 自定义事件操作 [scripts](/manual/web_interface/frontend_sections/administration/scripts)|*主机资产记录中的安装名称字段。*<br><br>这个宏可以和一个数字索引一起使用，例如： {INVENTORY.INSTALLER.NAME[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中的第1个、第2个、第3个等主机。参阅 [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.LOCATION}|→ 基于触发器的通知和命令<br>→ 问题更新通知和命令<br>→ 内部通知<br>→ [标签名称和值](/manual/config/tagging#macro_support)<br>→ 映射元素标签，映射URL名称和值<br>→ 自定义事件操作 [scripts](/manual/web_interface/frontend_sections/administration/scripts)|*主机资产记录中的位置字段。*<br><br>这个宏可以和一个数字索引一起使用，例如： {INVENTORY.LOCATION[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中的第1个、第2个、第3个等主机。参阅 [indexed macros](supported_by_location#indexed_macros).<br><br>`{PROFILE.LOCATION<1-9>}` 已弃用。|
|{INVENTORY.LOCATION.LAT}|→ 基于触发器的通知和命令<br>→ 问题更新通知和命令<br>→ 内部通知<br>→ [标签名称和值](/manual/config/tagging#macro_support)<br>→ 映射元素标签，映射URL名称和值<br>→ 自定义事件操作 [scripts](/manual/web_interface/frontend_sections/administration/scripts)|*主机资产记录中的位置纬度字段。*<br><br>这个宏可以和一个数字索引一起使用，例如： {INVENTORY.LOCATION.LAT[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中的第1个、第2个、第3个等主机。参阅 [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.LOCATION.LON}|→ 基于触发器的通知和命令<br>→ 问题更新通知和命令<br>→ 内部通知<br>→ [标签名称和值](/manual/config/tagging#macro_support)<br>→ 映射元素标签，映射URL名称和值<br>→ 自定义事件操作 [scripts](/manual/web_interface/frontend_sections/administration/scripts)|*主机资产记录中的位置经度字段。*<br><br>这个宏可以和一个数字索引一起使用，例如： {INVENTORY.LOCATION.LON[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中的第1个、第2个、第3个等主机。参阅 [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.MACADDRESS.A}|→ 基于触发器的通知和命令<br>→ 问题更新通知和命令<br>→ 内部通知<br>→ [标签名称和值](/manual/config/tagging#macro_support)<br>→ 映射元素标签，映射URL名称和值<br>→ 自定义事件操作 [scripts](/manual/web_interface/frontend_sections/administration/scripts)|*主机资产记录中的Mac 地址 A字段。*<br><br>这个宏可以和一个数字索引一起使用，例如： {INVENTORY.MACADDRESS.A[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中的第1个、第2个、第3个等主机。参阅 [indexed macros](supported_by_location#indexed_macros).<br><br>`{PROFILE.MACADDRESS<1-9>}` 已弃用。|
|{INVENTORY.MACADDRESS.B}|→ 基于触发器的通知和命令<br>→ 问题更新通知和命令<br>→ 内部通知<br>→ [标签名称和值](/manual/config/tagging#macro_support)<br>→ 映射元素标签，映射URL名称和值<br>→ 自定义事件操作 [scripts](/manual/web_interface/frontend_sections/administration/scripts)|*主机资产记录中的Mac 地址 B字段。*<br><br>这个宏可以和一个数字索引一起使用，例如： {INVENTORY.MACADDRESS.B[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中的第1个、第2个、第3个等主机。参阅 [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.MODEL}|→ 基于触发器的通知和命令<br>→ 问题更新通知和命令<br>→ 内部通知<br>→ [标签名称和值](/manual/config/tagging#macro_support)<br>→ 映射元素标签，映射URL名称和值<br>→ 自定义事件操作 [scripts](/manual/web_interface/frontend_sections/administration/scripts)|*主机资产记录中的模型字段。*<br><br>这个宏可以和一个数字索引一起使用，例如： {INVENTORY.MODEL[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中的第1个、第2个、第3个等主机。参阅 [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.NAME}|→ 基于触发器的通知和命令<br>→ 问题更新通知和命令<br>→ 内部通知<br>→ [标签名称和值](/manual/config/tagging#macro_support)<br>→ 映射元素标签，映射URL名称和值<br>→ 自定义事件操作 [scripts](/manual/web_interface/frontend_sections/administration/scripts)|*主机资产记录中的名称字段。*<br><br>这个宏可以和一个数字索引一起使用，例如： {INVENTORY.NAME[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中的第1个、第2个、第3个等主机。参阅 [indexed macros](supported_by_location#indexed_macros).<br><br>`{PROFILE.NAME<1-9>}` 已弃用。|
|{INVENTORY.NOTES}|→ 基于触发器的通知和命令<br>→ 问题更新通知和命令<br>→ 内部通知<br>→ [标签名称和值](/manual/config/tagging#macro_support)<br>→ 映射元素标签，映射URL名称和值<br>→ 自定义事件操作 [scripts](/manual/web_interface/frontend_sections/administration/scripts)|*主机资产记录中的备注字段。*<br><br>这个宏可以和一个数字索引一起使用，例如： {INVENTORY.NOTES[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中的第1个、第2个、第3个等主机。参阅 [indexed macros](supported_by_location#indexed_macros).<br><br>`{PROFILE.NOTES<1-9>}` 已弃用。|
|{INVENTORY.OOB.IP}|→ 基于触发器的通知和命令<br>→ 问题更新通知和命令<br>→ 内部通知<br>→ [标签名称和值](/manual/config/tagging#macro_support)<br>→ 映射元素标签，映射URL名称和值<br>→ 自定义事件操作 [scripts](/manual/web_interface/frontend_sections/administration/scripts)|*主机资产记录中的带外 IP 地址字段。 field in host inventory.*<br><br>这个宏可以和一个数字索引一起使用，例如： {INVENTORY.OOB.IP[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中的第1个、第2个、第3个等主机。参阅 [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.OOB.NETMASK}|→ 基于触发器的通知和命令<br>→ 问题更新通知和命令<br>→ 内部通知<br>→ [标签名称和值](/manual/config/tagging#macro_support)<br>→ 映射元素标签，映射URL名称和值<br>→ 自定义事件操作 [scripts](/manual/web_interface/frontend_sections/administration/scripts)|*主机资产记录中的带外子网掩码字段。*<br><br>这个宏可以和一个数字索引一起使用，例如： {INVENTORY.OOB.NETMASK[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中的第1个、第2个、第3个等主机。参阅 [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.OOB.ROUTER}|→ 基于触发器的通知和命令<br>→ 问题更新通知和命令<br>→ 内部通知<br>→ [标签名称和值](/manual/config/tagging#macro_support)<br>→ 映射元素标签，映射URL名称和值<br>→ 自定义事件操作 [scripts](/manual/web_interface/frontend_sections/administration/scripts)|*主机资产记录中的带外路由器字段。*<br><br>这个宏可以和一个数字索引一起使用，例如： {INVENTORY.OOB.ROUTER[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中的第1个、第2个、第3个等主机。参阅 [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.OS}|→ 基于触发器的通知和命令<br>→ 问题更新通知和命令<br>→ 内部通知<br>→ [标签名称和值](/manual/config/tagging#macro_support)<br>→ 映射元素标签，映射URL名称和值<br>→ 自定义事件操作 [scripts](/manual/web_interface/frontend_sections/administration/scripts)|*主机资产记录中的操作系统字段。*<br><br>这个宏可以和一个数字索引一起使用，例如： {INVENTORY.OS[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中的第1个、第2个、第3个等主机。参阅 [indexed macros](supported_by_location#indexed_macros).<br><br>`{PROFILE.OS<1-9>}` 已弃用。|
|{INVENTORY.OS.FULL}|→ 基于触发器的通知和命令<br>→ 问题更新通知和命令<br>→ 内部通知<br>→ [标签名称和值](/manual/config/tagging#macro_support)<br>→ 映射元素标签，映射URL名称和值<br>→ 自定义事件操作 [scripts](/manual/web_interface/frontend_sections/administration/scripts)|*主机资产记录中的操作系统(所有细节)字段。*<br><br>这个宏可以和一个数字索引一起使用，例如： {INVENTORY.OS.FULL[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中的第1个、第2个、第3个等主机。参阅 [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.OS.SHORT}|→ 基于触发器的通知和命令<br>→ 问题更新通知和命令<br>→ 内部通知<br>→ [标签名称和值](/manual/config/tagging#macro_support)<br>→ 映射元素标签，映射URL名称和值<br>→ 自定义事件操作 [scripts](/manual/web_interface/frontend_sections/administration/scripts)|*主机资产记录中的操作系统( 简短)字段。*<br><br>这个宏可以和一个数字索引一起使用，例如： {INVENTORY.OS.SHORT[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中的第1个、第2个、第3个等主机。参阅 [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.POC.PRIMARY.CELL}|→ 基于触发器的通知和命令<br>→ 问题更新通知和命令<br>→ 内部通知<br>→ [标签名称和值](/manual/config/tagging#macro_support)<br>→ 映射元素标签，映射URL名称和值<br>→ 自定义事件操作 [scripts](/manual/web_interface/frontend_sections/administration/scripts)|*主机资产记录中的主要的POC手机字段。*<br><br>这个宏可以和一个数字索引一起使用，例如： {INVENTORY.POC.PRIMARY.CELL[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中的第1个、第2个、第3个等主机。参阅 [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.POC.PRIMARY.EMAIL}|→ 基于触发器的通知和命令<br>→ 问题更新通知和命令<br>→ 内部通知<br>→ [标签名称和值](/manual/config/tagging#macro_support)<br>→ 映射元素标签，映射URL名称和值<br>→ 自定义事件操作 [scripts](/manual/web_interface/frontend_sections/administration/scripts)|*主机资产记录中的主要的POC email字段。*<br><br>这个宏可以和一个数字索引一起使用，例如： {INVENTORY.POC.PRIMARY.EMAIL[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中的第1个、第2个、第3个等主机。参阅 [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.POC.PRIMARY.NAME}|→ 基于触发器的通知和命令<br>→ 问题更新通知和命令<br>→ 内部通知<br>→ [标签名称和值](/manual/config/tagging#macro_support)<br>→ 映射元素标签，映射URL名称和值<br>→ 自定义事件操作 [scripts](/manual/web_interface/frontend_sections/administration/scripts)|*主机资产记录中的主要的POC名称字段。*<br><br>这个宏可以和一个数字索引一起使用，例如： {INVENTORY.POC.PRIMARY.NAME[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中的第1个、第2个、第3个等主机。参阅 [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.POC.PRIMARY.NOTES}|→ 基于触发器的通知和命令<br>→ 问题更新通知和命令<br>→ 内部通知<br>→ [标签名称和值](/manual/config/tagging#macro_support)<br>→ 映射元素标签，映射URL名称和值<br>→ 自定义事件操作 [scripts](/manual/web_interface/frontend_sections/administration/scripts)|*主机资产记录中的主要的POC注记字段。*<br><br>这个宏可以和一个数字索引一起使用，例如： {INVENTORY.POC.PRIMARY.NOTES[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中的第1个、第2个、第3个等主机。参阅 [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.POC.PRIMARY.PHONE.A}|→ 基于触发器的通知和命令<br>→ 问题更新通知和命令<br>→ 内部通知<br>→ [标签名称和值](/manual/config/tagging#macro_support)<br>→ 映射元素标签，映射URL名称和值<br>→ 自定义事件操作 [scripts](/manual/web_interface/frontend_sections/administration/scripts)|*主机资产记录中的主要的POC电话A字段。*<br><br>这个宏可以和一个数字索引一起使用，例如： {INVENTORY.POC.PRIMARY.PHONE.A[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中的第1个、第2个、第3个等主机。参阅 [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.POC.PRIMARY.PHONE.B}|→ 基于触发器的通知和命令<br>→ 问题更新通知和命令<br>→ 内部通知<br>→ [标签名称和值](/manual/config/tagging#macro_support)<br>→ 映射元素标签，映射URL名称和值<br>→ 自定义事件操作 [scripts](/manual/web_interface/frontend_sections/administration/scripts)|*主机资产记录中的主要的POC电话B字段。*<br><br>这个宏可以和一个数字索引一起使用，例如： {INVENTORY.POC.PRIMARY.PHONE.B[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中的第1个、第2个、第3个等主机。参阅 [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.POC.PRIMARY.SCREEN}|→ 基于触发器的通知和命令<br>→ 问题更新通知和命令<br>→ 内部通知<br>→ [标签名称和值](/manual/config/tagging#macro_support)<br>→ 映射元素标签，映射URL名称和值<br>→ 自定义事件操作 [scripts](/manual/web_interface/frontend_sections/administration/scripts)|*主机资产记录中的主要的POC屏幕名称字段。。*<br><br>这个宏可以和一个数字索引一起使用，例如： {INVENTORY.POC.PRIMARY.SCREEN[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中的第1个、第2个、第3个等主机。参阅 [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.POC.SECONDARY.CELL}|→ 基于触发器的通知和命令<br>→ 问题更新通知和命令<br>→ 内部通知<br>→ [标签名称和值](/manual/config/tagging#macro_support)<br>→ 映射元素标签，映射URL名称和值<br>→ 自定义事件操作 [scripts](/manual/web_interface/frontend_sections/administration/scripts)|*主机资产记录中的第二个POC手机号码字段。*<br><br>这个宏可以和一个数字索引一起使用，例如： {INVENTORY.POC.SECONDARY.CELL[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中的第1个、第2个、第3个等主机。参阅 [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.POC.SECONDARY.EMAIL}|→ 基于触发器的通知和命令<br>→ 问题更新通知和命令<br>→ 内部通知<br>→ [标签名称和值](/manual/config/tagging#macro_support)<br>→ 映射元素标签，映射URL名称和值<br>→ 自定义事件操作 [scripts](/manual/web_interface/frontend_sections/administration/scripts)|*主机资产记录中的第二个POC email字段。*<br><br>这个宏可以和一个数字索引一起使用，例如： {INVENTORY.POC.SECONDARY.EMAIL[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中的第1个、第2个、第3个等主机。参阅 [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.POC.SECONDARY.NAME}|→ 基于触发器的通知和命令<br>→ 问题更新通知和命令<br>→ 内部通知<br>→ [标签名称和值](/manual/config/tagging#macro_support)<br>→ 映射元素标签，映射URL名称和值<br>→ 自定义事件操作 [scripts](/manual/web_interface/frontend_sections/administration/scripts)|*主机资产记录中的第二个POC名称字段。*<br><br>这个宏可以和一个数字索引一起使用，例如： {INVENTORY.POC.SECONDARY.NAME[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中的第1个、第2个、第3个等主机。参阅 [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.POC.SECONDARY.NOTES}|→ 基于触发器的通知和命令<br>→ 问题更新通知和命令<br>→ 内部通知<br>→ [标签名称和值](/manual/config/tagging#macro_support)<br>→ 映射元素标签，映射URL名称和值<br>→ 自定义事件操作 [scripts](/manual/web_interface/frontend_sections/administration/scripts)|*主机资产记录中的第二个POC注记字段。*<br><br>这个宏可以和一个数字索引一起使用，例如： {INVENTORY.POC.SECONDARY.NOTES[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中的第1个、第2个、第3个等主机。参阅 [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.POC.SECONDARY.PHONE.A}|→ 基于触发器的通知和命令<br>→ 问题更新通知和命令<br>→ 内部通知<br>→ [标签名称和值](/manual/config/tagging#macro_support)<br>→ 映射元素标签，映射URL名称和值<br>→ 自定义事件操作 [scripts](/manual/web_interface/frontend_sections/administration/scripts)|*主机资产记录中的第二个POC电话A字段。*<br><br>这个宏可以和一个数字索引一起使用，例如： {INVENTORY.POC.SECONDARY.PHONE.A[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中的第1个、第2个、第3个等主机。参阅 [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.POC.SECONDARY.PHONE.B}|→ 基于触发器的通知和命令<br>→ 问题更新通知和命令<br>→ 内部通知<br>→ [标签名称和值](/manual/config/tagging#macro_support)<br>→ 映射元素标签，映射URL名称和值<br>→ 自定义事件操作 [scripts](/manual/web_interface/frontend_sections/administration/scripts)|*主机资产记录中的第二个POC电话B字段。*<br><br>这个宏可以和一个数字索引一起使用，例如： {INVENTORY.POC.SECONDARY.PHONE.B[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中的第1个、第2个、第3个等主机。参阅 [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.POC.SECONDARY.SCREEN}|→ 基于触发器的通知和命令<br>→ 问题更新通知和命令<br>→ 内部通知<br>→ [标签名称和值](/manual/config/tagging#macro_support)<br>→ 映射元素标签，映射URL名称和值<br>→ 自定义事件操作 [scripts](/manual/web_interface/frontend_sections/administration/scripts)|*主机资产记录中的第二个POC聚合图形名称字段。*<br><br>这个宏可以和一个数字索引一起使用，例如： {INVENTORY.POC.SECONDARY.SCREEN[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中的第1个、第2个、第3个等主机。参阅 [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.SERIALNO.A}|→ 基于触发器的通知和命令<br>→ 问题更新通知和命令<br>→ 内部通知<br>→ [标签名称和值](/manual/config/tagging#macro_support)<br>→ 映射元素标签，映射URL名称和值<br>→ 自定义事件操作 [scripts](/manual/web_interface/frontend_sections/administration/scripts)|*主机资产记录中的序列号A字段。*<br><br>这个宏可以和一个数字索引一起使用，例如： {INVENTORY.SERIALNO.A[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中的第1个、第2个、第3个等主机。参阅 [indexed macros](supported_by_location#indexed_macros).<br><br>`{PROFILE.SERIALNO<1-9>}` 已弃用。|
|{INVENTORY.SERIALNO.B}|→ 基于触发器的通知和命令<br>→ 问题更新通知和命令<br>→ 内部通知<br>→ [标签名称和值](/manual/config/tagging#macro_support)<br>→ 映射元素标签，映射URL名称和值<br>→ 自定义事件操作 [scripts](/manual/web_interface/frontend_sections/administration/scripts)|*主机资产记录中的序列号B字段。*<br><br>这个宏可以和一个数字索引一起使用，例如： {INVENTORY.SERIALNO.B[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中的第1个、第2个、第3个等主机。参阅 [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.SITE.ADDRESS.A}|→ 基于触发器的通知和命令<br>→ 问题更新通知和命令<br>→ 内部通知<br>→ [标签名称和值](/manual/config/tagging#macro_support)<br>→ 映射元素标签，映射URL名称和值<br>→ 自定义事件操作 [scripts](/manual/web_interface/frontend_sections/administration/scripts)|*主机资产记录中的场所地址A字段。*<br><br>这个宏可以和一个数字索引一起使用，例如： {INVENTORY.SITE.ADDRESS.A[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中的第1个、第2个、第3个等主机。参阅 [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.SITE.ADDRESS.B}|→ 基于触发器的通知和命令<br>→ 问题更新通知和命令<br>→ 内部通知<br>→ [标签名称和值](/manual/config/tagging#macro_support)<br>→ 映射元素标签，映射URL名称和值<br>→ 自定义事件操作 [scripts](/manual/web_interface/frontend_sections/administration/scripts)|*主机资产记录中的场所地址B字段。*<br><br>这个宏可以和一个数字索引一起使用，例如： {INVENTORY.SITE.ADDRESS.B[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中的第1个、第2个、第3个等主机。参阅 [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.SITE.ADDRESS.C}|→ 基于触发器的通知和命令<br>→ 问题更新通知和命令<br>→ 内部通知<br>→ [标签名称和值](/manual/config/tagging#macro_support)<br>→ 映射元素标签，映射URL名称和值<br>→ 自定义事件操作 [scripts](/manual/web_interface/frontend_sections/administration/scripts)|*主机资产记录中的场所地址C字段。*<br><br>这个宏可以和一个数字索引一起使用，例如： {INVENTORY.SITE.ADDRESS.C[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中的第1个、第2个、第3个等主机。参阅 [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.SITE.CITY}|→ 基于触发器的通知和命令<br>→ 问题更新通知和命令<br>→ 内部通知<br>→ [标签名称和值](/manual/config/tagging#macro_support)<br>→ 映射元素标签，映射URL名称和值<br>→ 自定义事件操作 [scripts](/manual/web_interface/frontend_sections/administration/scripts)|*主机资产记录中的场所城市字段。*<br><br>这个宏可以和一个数字索引一起使用，例如： {INVENTORY.SITE.CITY[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中的第1个、第2个、第3个等主机。参阅 [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.SITE.COUNTRY}|→ 基于触发器的通知和命令<br>→ 问题更新通知和命令<br>→ 内部通知<br>→ [标签名称和值](/manual/config/tagging#macro_support)<br>→ 映射元素标签，映射URL名称和值<br>→ 自定义事件操作 [scripts](/manual/web_interface/frontend_sections/administration/scripts)|*主机资产记录中的场所国家字段。*<br><br>这个宏可以和一个数字索引一起使用，例如： {INVENTORY.SITE.COUNTRY[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中的第1个、第2个、第3个等主机。参阅 [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.SITE.NOTES}|→ 基于触发器的通知和命令<br>→ 问题更新通知和命令<br>→ 内部通知<br>→ [标签名称和值](/manual/config/tagging#macro_support)<br>→ 映射元素标签，映射URL名称和值<br>→ 自定义事件操作 [scripts](/manual/web_interface/frontend_sections/administration/scripts)|*主机资产记录中的场所备注字段。*<br><br>这个宏可以和一个数字索引一起使用，例如： {INVENTORY.SITE.NOTES[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中的第1个、第2个、第3个等主机。参阅 [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.SITE.RACK}|→ 基于触发器的通知和命令<br>→ 故障更新通知和命令<br>→ 内部通知<br>→ [标签的名称和值](/manual/config/tagging#macro_support)<br>→ 地图元素标签,地图URL的名称和值<br>→ 手动操作[脚本](/manual/web_interface/frontend_sections/administration/scripts)事件|*主机清单中站点的机架位置字段。*<br><br>该宏可以与数字索引一起使用，例如 {INVENTORY.SITE.RACK[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中匹配的第1个，第2个，第3个等主机。 请参考[宏索引](supported_by_location#indexed_macros)。|
|{INVENTORY.SITE.STATE}|→ 基于触发器的通知和命令<br>→ 故障更新通知和命令<br>→ 内部通知<br>→ [标签的名称和值](/manual/config/tagging#macro_support)<br>→ 地图元素标签,地图URL的名称和值<br>→ 手动操作[脚本](/manual/web_interface/frontend_sections/administration/scripts)事件|*主机清单中站点所属的省/市。*<br><br>该宏可以与数字索引一起使用，例如 {INVENTORY.SITE.STATE[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中匹配的第1个，第2个，第3个等主机。 请参考[宏索引](supported_by_location#indexed_macros)。|
|{INVENTORY.SITE.ZIP}|→ 基于触发器的通知和命令<br>→ 故障更新通知和命令<br>→ 内部通知<br>→ [标签的名称和值](/manual/config/tagging#macro_support)<br>→ 地图元素标签,地图URL的名称和值<br>→ 手动操作[脚本](/manual/web_interface/frontend_sections/administration/scripts)事件|*主机清单中站点的邮编字段*<br><br>该宏可以与数字索引一起使用，例如 {INVENTORY.SITE.ZIP[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中匹配的第1个，第2个，第3个等主机。 请参考[宏索引](supported_by_location#indexed_macros)。|
|{INVENTORY.SOFTWARE}|→ 基于触发器的通知和命令<br>→ 故障更新通知和命令<br>→ 内部通知<br>→ [标签的名称和值](/manual/config/tagging#macro_support)<br>→ 地图元素标签,地图URL的名称和值<br>→ 手动操作[脚本](/manual/web_interface/frontend_sections/administration/scripts)事件|*主机清单中的软件字段。*<br><br>该宏可以与数字索引一起使用，例如 {INVENTORY.SOFTWARE[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中匹配的第1个，第2个，第3个等主机。 请参考[宏索引](supported_by_location#indexed_macros)。<br><br>`{PROFILE.SOFTWARE<1-9>}` 已弃用。|
|{INVENTORY.SOFTWARE.APP.A}|→ 基于触发器的通知和命令<br>→ 故障更新通知和命令<br>→ 内部通知<br>→ [标签的名称和值](/manual/config/tagging#macro_support)<br>→ 地图元素标签,地图URL的名称和值<br>→ 手动操作[脚本](/manual/web_interface/frontend_sections/administration/scripts)事件|*主机清单中的软件应用程序A的字段。*<br><br>该宏可以与数字索引一起使用，例如 {INVENTORY.SOFTWARE.APP.A[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中匹配的第1个，第2个，第3个等主机。 请参考[宏索引](supported_by_location#indexed_macros)。|
|{INVENTORY.SOFTWARE.APP.B}|→ 基于触发器的通知和命令<br>→ 故障更新通知和命令<br>→ 内部通知<br>→ [标签的名称和值](/manual/config/tagging#macro_support)<br>→ 地图元素标签,地图URL的名称和值<br>→ 手动操作[脚本](/manual/web_interface/frontend_sections/administration/scripts)事件|*主机清单中的软件应用程序B的字段。*<br><br>该宏可以与数字索引一起使用，例如 {INVENTORY.SOFTWARE.APP.B[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中匹配的第1个，第2个，第3个等主机。 请参考[宏索引](supported_by_location#indexed_macros)。|
|{INVENTORY.SOFTWARE.APP.C}|→ 基于触发器的通知和命令<br>→ 故障更新通知和命令<br>→ 内部通知<br>→ [标签的名称和值](/manual/config/tagging#macro_support)<br>→ 地图元素标签,地图URL的名称和值<br>→ 手动操作[脚本](/manual/web_interface/frontend_sections/administration/scripts)事件|*主机清单中的软件应用程序C的字段。*<br><br>该宏可以与数字索引一起使用，例如 {INVENTORY.SOFTWARE.APP.C[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中匹配的第1个，第2个，第3个等主机。 请参考[宏索引](supported_by_location#indexed_macros)。|
|{INVENTORY.SOFTWARE.APP.D}|→ 基于触发器的通知和命令<br>→ 故障更新通知和命令<br>→ 内部通知<br>→ [标签的名称和值](/manual/config/tagging#macro_support)<br>→ 地图元素标签,地图URL的名称和值<br>→ 手动操作[脚本](/manual/web_interface/frontend_sections/administration/scripts)事件|*主机清单中的软件应用程序D的字段。*<br><br>该宏可以与数字索引一起使用，例如 {INVENTORY.SOFTWARE.APP.D[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中匹配的第1个，第2个，第3个等主机。 请参考[宏索引](supported_by_location#indexed_macros)。|
|{INVENTORY.SOFTWARE.APP.E}|→ 基于触发器的通知和命令<br>→ 故障更新通知和命令<br>→ 内部通知<br>→ [标签的名称和值](/manual/config/tagging#macro_support)<br>→ 地图元素标签,地图URL的名称和值<br>→ 手动操作[脚本](/manual/web_interface/frontend_sections/administration/scripts)事件|*主机清单中的软件应用程序E的字段。*<br><br>该宏可以与数字索引一起使用，例如 {INVENTORY.SOFTWARE.APP.E[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中匹配的第1个，第2个，第3个等主机。 请参考[宏索引](supported_by_location#indexed_macros)。|
|{INVENTORY.SOFTWARE.FULL}|→ 基于触发器的通知和命令<br>→ 故障更新通知和命令<br>→ 内部通知<br>→ [标签的名称和值](/manual/config/tagging#macro_support)<br>→ 地图元素标签,地图URL的名称和值<br>→ 手动操作[脚本](/manual/web_interface/frontend_sections/administration/scripts)事件|*主机清单中的软件（完整细节描述）字段。*<br><br>该宏可以与数字索引一起使用，例如 {INVENTORY.SOFTWARE.FULL[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中匹配的第1个，第2个，第3个等主机。 请参考[宏索引](supported_by_location#indexed_macros)。|
|{INVENTORY.TAG}|→ 基于触发器的通知和命令<br>→ 故障更新通知和命令<br>→ 内部通知<br>→ [标签的名称和值](/manual/config/tagging#macro_support)<br>→ 地图元素标签,地图URL的名称和值<br>→ 手动操作[脚本](/manual/web_interface/frontend_sections/administration/scripts)事件|*主机清单中的标签字段。*<br><br>该宏可以与数字索引一起使用，例如 {INVENTORY.TAG[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中匹配的第1个，第2个，第3个等主机。 请参考[宏索引](supported_by_location#indexed_macros)。<br><br>`{PROFILE.TAG<1-9>}` 已弃用。|
|{INVENTORY.TYPE}|→ 基于触发器的通知和命令<br>→ 故障更新通知和命令<br>→ 内部通知<br>→ [标签的名称和值](/manual/config/tagging#macro_support)<br>→ 地图元素标签,地图URL的名称和值<br>→ 手动操作[脚本](/manual/web_interface/frontend_sections/administration/scripts)事件|*主机清单中的类型字段。*<br><br>该宏可以与数字索引一起使用，例如 {INVENTORY.TYPE[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中匹配的第1个，第2个，第3个等主机。 请参考[宏索引](supported_by_location#indexed_macros)。<br><br>`{PROFILE.DEVICETYPE<1-9>}` 已弃用。|
|{INVENTORY.TYPE.FULL}|→ 基于触发器的通知和命令<br>→ 故障更新通知和命令<br>→ 内部通知<br>→ [标签的名称和值](/manual/config/tagging#macro_support)<br>→ 地图元素标签,地图URL的名称和值<br>→ 手动操作[脚本](/manual/web_interface/frontend_sections/administration/scripts)事件|*主机清单中类型（完整详细信息）字段的内容。*<br><br>该宏可以与数字索引一起使用，例如 {INVENTORY.TYPE.FULL[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中匹配的第1个，第2个，第3个等主机。 请参考[宏索引](supported_by_location#indexed_macros)。|
|{INVENTORY.URL.A}|→ 基于触发器的通知和命令<br>→ 故障更新通知和命令<br>→ 内部通知<br>→ [标签的名称和值](/manual/config/tagging#macro_support)<br>→ 地图元素标签,地图URL的名称和值<br>→ 手动操作[脚本](/manual/web_interface/frontend_sections/administration/scripts)事件|*主机清单中URL A的字段内容。*<br><br>该宏可以与数字索引一起使用，例如 {INVENTORY.URL.A[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中匹配的第1个，第2个，第3个等主机。 请参考[宏索引](supported_by_location#indexed_macros)。|
|{INVENTORY.URL.B}|→ 基于触发器的通知和命令<br>→ 故障更新通知和命令<br>→ 内部通知<br>→ [标签的名称和值](/manual/config/tagging#macro_support)<br>→ 地图元素标签,地图URL的名称和值<br>→ 手动操作[脚本](/manual/web_interface/frontend_sections/administration/scripts)事件|*主机清单中URL B的字段内容。*<br><br>该宏可以与数字索引一起使用，例如 {INVENTORY.URL.B[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中匹配的第1个，第2个，第3个等主机。 请参考[宏索引](supported_by_location#indexed_macros)。|
|{INVENTORY.URL.C}|→ 基于触发器的通知和命令<br>→ 故障更新通知和命令<br>→ 内部通知<br>→ [标签的名称和值](/manual/config/tagging#macro_support)<br>→ 地图元素标签,地图URL的名称和值<br>→ 手动操作[脚本](/manual/web_interface/frontend_sections/administration/scripts)事件|*主机清单中URL C的字段内容。*<br><br>该宏可以与数字索引一起使用，例如 {INVENTORY.URL.C[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中匹配的第1个，第2个，第3个等主机。 请参考[宏索引](supported_by_location#indexed_macros)。|
|{INVENTORY.VENDOR}|→ 基于触发器的通知和命令<br>→ 故障更新通知和命令<br>→ 内部通知<br>→ [标签的名称和值](/manual/config/tagging#macro_support)<br>→ 地图元素标签,地图URL的名称和值<br>→ 手动操作[脚本](/manual/web_interface/frontend_sections/administration/scripts)事件|*主机清单中的供应商字段的内容*<br><br>该宏可以与数字索引一起使用，例如 {INVENTORY.VENDOR[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中匹配的第1个，第2个，第3个等主机。 请参考[宏索引](supported_by_location#indexed_macros)。|
|{ITEM.DESCRIPTION}|→ 基于触发器的通知和命令<br>→ 故障更新通知和命令<br>→ 内部通知<br>→ 手动操作[脚本](/manual/web_interface/frontend_sections/administration/scripts)事件|*引发通知的触发器表达式中的第N个监控项的描述。*<br><br>该宏可以与数字索引一起使用，例如 {ITEM.DESCRIPTION[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中匹配的第1个，第2个，第3个等主机。 请参考[宏索引](supported_by_location#indexed_macros)。|
|{ITEM.DESCRIPTION.ORIG}|→ 基于触发器的通知和命令<br>→ 故障更新通知和命令<br>→ 内部通知<br>→ 手动操作[脚本](/manual/web_interface/frontend_sections/administration/scripts)事件|*引发通知的触发器表达式中的第N个监控项的描述（带有未解析的宏）。*<br><br>该宏可以与数字索引一起使用，例如 {ITEM.DESCRIPTION.ORIG[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中匹配的第1个，第2个，第3个等主机。 请参考[宏索引](supported_by_location#indexed_macros)。<br><br>支持的最低版本是 5.2.0.|
|{ITEM.ID}|→ 基于触发器的通知和命令<br>→ 故障更新通知和命令<br>→ 内部通知<br>→ 脚本类型item、item原型和发现规则参数的名称和值^[6](supported_by_location#footnotes)^<br>→ HTTP agent 类型监控项、监控项原型和发现规则字段:<br>URL,查询字段，请求正文,headers,代理proxy,SSL 证书文件,SSL密钥文件<br>→ 手动操作[脚本](/manual/web_interface/frontend_sections/administration/scripts)事件|*引发通知的触发器表达式中的第 N 个监控项的序列ID。*<br><br>该宏可以与数字索引一起使用，例如 {ITEM.ID[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中匹配的第1个，第2个，第3个等主机。 请参考[宏索引](supported_by_location#indexed_macros)。|
|{ITEM.KEY}|→ 基于触发器的通知和命令<br>→ 故障更新通知和命令<br>→ 内部通知<br>→ 脚本类型item、item原型和发现规则参数的名称和值^[6](supported_by_location#footnotes)^<br>→ HTTP agent 类型监控项、监控项原型和发现规则字段:<br>URL,查询字段，请求正文,headers,代理proxy,SSL 证书文件,SSL密钥文件<br>→ 手动操作[脚本](/manual/web_interface/frontend_sections/administration/scripts)事件|*触发器表达式中导致通知的第 N 监控项的键。*<br><br>该宏可以与数字索引一起使用，例如 {ITEM.KEY[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中匹配的第1个，第2个，第3个等主机。 请参考[宏索引](supported_by_location#indexed_macros)。<br><br>`{TRIGGER.KEY}` 已弃用。|
|{ITEM.KEY.ORIG}|→ 基于触发器的通知和命令<br>→ 故障更新通知和命令<br>→ 内部通知<br>→ 脚本类型item、item原型和发现规则参数的名称和值^[6](supported_by_location#footnotes)^<br>→ HTTP agent 类型监控项、监控项原型和发现规则字段:<br>URL,查询字段，请求正文,headers,代理proxy,SSL 证书文件,SSL密钥文件, 允许的主机。^[6](supported_by_location#footnotes)^<br>→ 手动操作[脚本](/manual/web_interface/frontend_sections/administration/scripts)事件|*触发器表达式中导致通知的第N监控项的原始键（宏未展开）。^[4](supported_by_location#footnotes)^.*<br><br>该宏可以与数字索引一起使用，例如 {ITEM.KEY.ORIG[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中匹配的第1个，第2个，第3个等主机。 请参考[宏索引](supported_by_location#indexed_macros)。|
|{ITEM.LASTVALUE}|→ 基于触发器的通知和命令<br>→ 故障更新通知和命令<br>→ 触发器名字，事件名字，操作数据和描述。<br>→ [标签的名称和值](/manual/config/tagging#macro_support)<br>→ 触发器的URL地址<br>→ 手动操作[脚本](/manual/web_interface/frontend_sections/administration/scripts)事件|*触发器表达式中导致通知的第N项的最新值。*<br>如果收集到的最新历史值超过了 *最大历史显示周期*，在前端就会解析为\*UNKNOWN\* 。 (请查看 [管理→通用](/manual/web_interface/frontend_sections/administration/general#gui) 菜单选项)。<br>请注意，从 4.0 开始，在问题名称中使用时，在查看问题事件时不会解析为最新的监控项值，而是保留问题发生时的监控项值。 <br>它是此 `last(/{HOST.HOST}/{ITEM.KEY})`的别名。<br>从Zabbix的版本3.2.0开始，该宏支持[自定义](/manual/config/macros/macro_functions) 宏的值； <br><br>该宏可以与数字索引一起使用，例如 {ITEM.LASTVALUE[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中的第一个、第二个、第三个等监控项。  请参考[宏索引](supported_by_location#indexed_macros)。|
|{ITEM.LOG.AGE}|→ 基于触发器的通知和命令<br>→ 故障更新通知和命令<br>→ 触发器名称、操作数据和描述<br>→ 触发器的URL地址<br>→ 事件的tags和值<br>→ 手动操作[脚本](/manual/web_interface/frontend_sections/administration/scripts)事件|*监控项事件日志的持续时间*，可以精确到秒。<br><br>该宏可以与数字索引一起使用，例如 {ITEM.LOG.AGE[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中匹配的第1个，第2个，第3个等主机。 请参考[宏索引](supported_by_location#indexed_macros)。|
|{ITEM.LOG.DATE}|→ 基于触发器的通知和命令<br>→ 故障更新通知和命令<br>→ 触发器名称、操作数据和描述<br>→ 触发器的URL地址<br>→ 事件的tags和值<br>→ 手动操作[脚本](/manual/web_interface/frontend_sections/administration/scripts)事件|*监控项事件日志的日期*。时间格式类似于 yyyy:mm:dd 。<br><br>该宏可以与数字索引一起使用，例如 {ITEM.LOG.DATE[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中匹配的第1个，第2个，第3个等主机。 请参考[宏索引](supported_by_location#indexed_macros)。|
|{ITEM.LOG.EVENTID}|→ 基于触发器的通知和命令<br>→ 故障更新通知和命令<br>→ 触发器名称、操作数据和描述<br>→ 触发器的URL地址<br>→ 事件的tags和值<br>→ 手动操作[脚本](/manual/web_interface/frontend_sections/administration/scripts)事件|*事件日志中的事件ID。*<br>仅适用于windows事件日志监控。<br><br>该宏可以与数字索引一起使用，例如 {ITEM.LOG.EVENTID[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中匹配的第1个，第2个，第3个等主机。 请参考[宏索引](supported_by_location#indexed_macros)。|
|{ITEM.LOG.NSEVERITY}|→ 基于触发器的通知和命令<br>→ 故障更新通知和命令<br>→ 触发器名称、操作数据和描述<br>→ 触发器的URL地址<br>→ 事件的tags和值<br>→ 手动操作[脚本](/manual/web_interface/frontend_sections/administration/scripts)事件|*事件日志中日志的严重级别，用数字级别描述（1--7）。*<br>仅适用于windows事件日志监控。<br><br>该宏可以与数字索引一起使用，例如 {ITEM.LOG.NSEVERITY[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中匹配的第1个，第2个，第3个等主机。 请参考[宏索引](supported_by_location#indexed_macros)。|
|{ITEM.LOG.SEVERITY}|→ 基于触发器的通知和命令<br>→ 故障更新通知和命令<br>→ 触发器名称、操作数据和描述<br>→ 触发器的URL地址<br>→ 事件的tags和值<br>→ 手动操作[脚本](/manual/web_interface/frontend_sections/administration/scripts)事件|*事件日志中日志的严重级别，使用文字描述。*<br>仅适用于windows事件日志监控。<br><br>该宏可以与数字索引一起使用，例如 {ITEM.LOG.SEVERITY[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中匹配的第1个，第2个，第3个等主机。 请参考[宏索引](supported_by_location#indexed_macros)。|
|{ITEM.LOG.SOURCE}|→ 基于触发器的通知和命令<br>→ 故障更新通知和命令<br>→ 触发器名称、操作数据和描述<br>→ 触发器的URL地址<br>→ 事件的tags和值<br>→ 手动操作[脚本](/manual/web_interface/frontend_sections/administration/scripts)事件|*事件日志中事件的源*<br>仅适用于windows事件日志监控。<br><br>该宏可以与数字索引一起使用，例如 {ITEM.LOG.SOURCE[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中匹配的第1个，第2个，第3个等主机。 请参考[宏索引](supported_by_location#indexed_macros)。|
|{ITEM.LOG.TIME}|→ 基于触发器的通知和命令<br>→ 故障更新通知和命令<br>→ 触发器名称、操作数据和描述<br>→ 触发器的URL地址<br>→ 事件的tags和值<br>→ 手动操作[脚本](/manual/web_interface/frontend_sections/administration/scripts)事件|*item事件日志的发生时间。*<br><br>该宏可以与数字索引一起使用，例如 {ITEM.LOG.TIME[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中匹配的第1个，第2个，第3个等主机。 请参考[宏索引](supported_by_location#indexed_macros)。|
|{ITEM.NAME}|→ 基于触发器的通知和命令<br>→ 故障更新通知和命令<br>→ 内部通知<br>→ 手动操作[脚本](/manual/web_interface/frontend_sections/administration/scripts)事件|*触发器表达式中导致通知的第N个监控项的名称。*<br><br>该宏可以与数字索引一起使用，例如 {ITEM.NAME[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中匹配的第1个，第2个，第3个等主机。 请参考[宏索引](supported_by_location#indexed_macros)。|
|{ITEM.NAME.ORIG}|→ 基于触发器的通知和命令<br>→ 故障更新通知和命令<br>→ 内部通知<br>→ 手动操作[脚本](/manual/web_interface/frontend_sections/administration/scripts)事件|从Zabbix 6.0之后此宏已弃用。 当监控项名称中支持用户宏和位置宏时，它用于在 Zabbix 6.0 之前的版本中解析为*监控项的原始名称（即没有解析宏）*。 <br><br>该宏可以与数字索引一起使用，例如 {ITEM.NAME.ORIG[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中匹配的第1个，第2个，第3个等主机。 请参考[宏索引](supported_by_location#indexed_macros)。|
|{ITEM.STATE}|→ 基于监控项的内部通知|*触发器表达式中导致通知的第 N 监控项的最新状态。* 可能的值: **不支持** 和 **正常**.<br><br>该宏可以与数字索引一起使用，例如 {ITEM.STATE[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中匹配的第1个，第2个，第3个等主机。 请参考[宏索引](supported_by_location#indexed_macros)。|
|{ITEM.VALUE}|→ 基于触发器的通知和命令<br>→ 故障更新通知和命令<br>→ 触发器名字，事件名字，操作数据和描述。<br>→ [标签的名称和值](/manual/config/tagging#macro_support)<br>→ 触发器的URL地址<br>→ 手动操作[脚本](/manual/web_interface/frontend_sections/administration/scripts)事件|可被解释为如下的任意一项:<br>1) 如果在触发器状态更改的上下文中使用，则解释为触发器表达式中第N个监控项的历史值（事件发生时），例如在展示事件或发送通知时。<br>2) 如果在没有触发器状态更改的上下文的情况下使用，触发器表达式中第N个监控项的最新值。例如，在弹出选择窗口中显示触发器列表时。在本例中工作方式与 {ITEM.LASTVALUE}相同。<br>在第一种情况下，如果历史记录值已被删除或从未存储过，它将解释为\*未知\*。<br>在第二种情况下并且仅在前端中，如果收集的最新历史值超过*最大历史显示周期*时间，它将解析为\*未知\*. (查看 [管理→通用](/manual/web_interface/frontend_sections/administration/general#gui) 菜单选项).<br><br>[自定义](/manual/config/macros/macro_functions) 此宏支持宏值，从 Zabbix 3.2.0 开始。<br><br>该宏可以与数字索引一起使用，例如 {ITEM.VALUE[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中的第一个、第二个、第三个等监控项。请参考[宏索引](supported_by_location#indexed_macros)。|
|{ITEM.VALUETYPE}|→ 基于触发器的通知和命令<br>→ 故障更新通知和命令<br>→ 内部通知<br>→ 手动操作[脚本](/manual/web_interface/frontend_sections/administration/scripts)事件|*触发器表达式中导致通知的第N个监控项的值类型。* 可能的值: 0 - numeric float, 1 - character, 2 - log, 3 - numeric unsigned, 4 - text。<br><br>该宏可以与数字索引一起使用，例如 {ITEM.VALUETYPE[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中匹配的第1个，第2个，第3个等主机。 请参考[宏索引](supported_by_location#indexed_macros)。<br><br>支持的最低版本是 5.4.0.|
|{LLDRULE.DESCRIPTION}|→ 基于 LLD 规则的内部通知|*引发通知的低级别自动发现规则的描述。*|
|{LLDRULE.DESCRIPTION.ORIG}|→ 基于 LLD 规则的内部通知|*引发通知的低级别自动发现规则的描述（描述中带有未解析的宏）*<br>支持的最低版本是 5.2.0.|
|{LLDRULE.ID}|→ 基于 LLD 规则的内部通知|*引发通知的低级别自动发现规则的规则序列ID*|
|{LLDRULE.KEY}|→ 基于 LLD 规则的内部通知|*引发通知的低级别自动发现规则的键值*|
|{LLDRULE.KEY.ORIG}|→ 基于 LLD 规则的内部通知|*引发通知的低级别自动发现规则的原始密钥（未扩展的宏）。*|
|{LLDRULE.NAME}|→ 基于 LLD 规则的内部通知|*导致通知的低级发现规则（已解析宏）的名称。*|
|{LLDRULE.NAME.ORIG}|→ 基于 LLD 规则的内部通知|*导致通知的低级发现规则的原始名称（即未解析宏）。*|
|{LLDRULE.STATE}|→ 基于 LLD 规则的内部通知|*低级发现规则的最新状态。* 可能的值: **不支持** 和 **正常**.|
|{MAP.ID}|→ 地图元素标签,地图URL的名称和值|*网络拓扑图ID*|
|{MAP.NAME}|→ 地图元素标签,地图URL的名称和值<br>→ Text field in map shapes|*网络拓扑图名称*<br>支持的最低版本是 3.4.0.|
|{PROXY.DESCRIPTION}|→ 基于触发器的通知和命令<br>→ 故障更新通知和命令<br>→ 发现的通知和命令<br>→ 自动注册的通知和命令<br>→ 内部通知<br>→ 手动操作[脚本](/manual/web_interface/frontend_sections/administration/scripts)事件|*代理的描述*。 可以被解释为如下的可能:<br>1) 触发器表达式中第 N 个监控项的代理（在基于触发器的通知中）。 您可以在此处使用[索引](/manual/appendix/macros/supported_by_location#indexed_macros) 宏。<br>2) 代理，它执行发现(在发现通知中)，在此处使用{PROXY.DESCRIPTION} ，无需索引。<br>3) 代理指向一个已经注册的agent (在自动注册通知中)。在此处使用{PROXY.DESCRIPTION}，无需索引。<br><br>该宏可以与数字索引一起使用，例如 {PROXY.DESCRIPTION[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中匹配的第1个，第2个，第3个等主机。 请参考[宏索引](supported_by_location#indexed_macros)。|
|{PROXY.NAME}|→ 基于触发器的通知和命令<br>→ 故障更新通知和命令<br>→ 发现的通知和命令<br>→ 自动注册通知和命令 <br>→ 内部通知<br>→ 手动操作[脚本](/manual/web_interface/frontend_sections/administration/scripts)事件|*代理的名称*. 可以被解释为如下的可能::<br>1)触发器表达式中第 N 个监控项的代理名称（在基于触发器的通知中）。 您可以在此处使用[索引](/manual/appendix/macros/supported_by_location#indexed_macros) 宏。<br>2)代理，它执行发现（在发现通知中）。在此处使用 {PROXY.NAME}，无需索引。<br>3) 代理指向一个已经注册的agent (在自动注册通知中)。 在此处使用{PROXY.NAME} ，无需索引。<br><br>该宏可以与数字索引一起使用，例如 {PROXY.NAME[<1-9>](supported_by_location#indexed_macros)} 指向触发器表达式中匹配的第1个，第2个，第3个等主机。 请参考[宏索引](supported_by_location#indexed_macros)。|
|{SERVICE.NAME}|→ 基于服务的通知和命令<br>→ 服务的通知和命令|*服务名称* (已解析宏)。|
|{SERVICE.ROOTCAUSE}|→ 基于服务的通知和命令<br>→ 服务的通知和命令|*导致服务失败的触发问题事件列表*, 使用安全性主机排序。 包括以下详细信息：主机名、事件名称、严重性、持续时间、服务标签和值。 |
|{SERVICE.TAGS}|→ 基于服务的通知和命令<br>→ 服务的通知和命令|*服务事件标签的逗号分隔列表。* 服务事件标签可以在服务配置选项标签中定义。如果不存在标签，则扩展为空字符串。|
|{SERVICE.TAGSJSON}|→ 基于服务的通知和命令<br>→ 服务的通知和命令|*包含服务事件标签对象的 JSON 数组。* 服务事件标签可以在服务配置选项标签中定义。 如果不存在标签，则扩展为空数组。|
|{SERVICE.TAGS.<tag name>}|→ 基于服务的通知和命令<br>→ 服务的通知和命令|*标签名称引用的服务事件标签值。* 服务事件标签可以在服务配置选项标签中定义。<br>包含非字母数字字符（包括非英语多字节 UTF 字符）的标签名称应该用双引号引起来。 带引号的标记名称中的引号和反斜杠必须使用反斜杠进行转义。|
|{TIME}|→ 基于触发器的通知和命令<br>→ 故障更新通知和命令<br>→ 基于服务的通知和命令<br>→ 服务的通知和命令<br>→ 发现的通知和命令<br>→ 自动注册通知和命令 <br>→ 内部通知<br>→ 触发器事件名称<br>→ 手动操作[脚本](/manual/web_interface/frontend_sections/administration/scripts)事件|*现在的时间，格式为 hh:mm:ss*|
|{TRIGGER.DESCRIPTION}|→ 基于触发器的通知和命令<br>→ 故障更新通知和命令<br>→ 基于触发器的内部通知<br>→ 手动操作[脚本](/manual/web_interface/frontend_sections/administration/scripts)事件|*触发器描述。*<br>如果在通知中使用了`{TRIGGER.DESCRIPTION}`，则触发器描述中支持的所有宏都将被扩展。<br>`{TRIGGER.COMMENT}` 已弃用。|
|{TRIGGER.EXPRESSION.EXPLAIN}|→ 基于触发器的通知和命令<br>→ 故障更新通知和命令<br>→ 手动操作[脚本](/manual/web_interface/frontend_sections/administration/scripts)事件<br>→ 事件名称|*部分评估的触发器表达式。*<br>基于监控项的函数在事件生成时被评估并替换为结果，而所有其他函数都显示为表达式中所写的。可用于调试触发器表达式。|
|{TRIGGER.EXPRESSION.RECOVERY.EXPLAIN}|→ Problem [recovery notifications](/manual/config/notifications/action/recovery_operations) and commands<br>→ 故障更新通知和命令<br>→ 手动操作[脚本](/manual/web_interface/frontend_sections/administration/scripts)事件|*部分评估触发恢复表达式.*<br>基于触发器的函数在事件生成时被评估并替换为结果，而所有其他函数都显示为表达式中所写的。可用于调试触发恢复表达式。|
|{TRIGGER.EVENTS.ACK}|→ 基于触发器的通知和命令<br>→ 故障更新通知和命令<br>→ 地图元素标签<br>→ 手动操作[脚本](/manual/web_interface/frontend_sections/administration/scripts)事件|*地图中地图元素或在通知中生成当前事件的触发器的已确认事件数。*|
|{TRIGGER.EVENTS.PROBLEM.ACK}|→ 基于触发器的通知和命令<br>→ 故障更新通知和命令<br>→ 地图元素标签<br>→ 手动操作[脚本](/manual/web_interface/frontend_sections/administration/scripts)事件|*所有触发器的已确认 PROBLEM 事件的数量，无论其状态。*|
|{TRIGGER.EVENTS.PROBLEM.UNACK}|→ 基于触发器的通知和命令<br>→ 故障更新通知和命令<br>→ 地图元素标签<br>→ 手动操作[脚本](/manual/web_interface/frontend_sections/administration/scripts)事件|*所有触发器的未确认问题事件数，无论其状态。*|
|{TRIGGER.EVENTS.UNACK}|→ 基于触发器的通知和命令<br>→ 故障更新通知和命令<br>→ 地图元素标签<br>→ 手动操作[脚本](/manual/web_interface/frontend_sections/administration/scripts)事件|*地图中地图元素或在通知中生成当前事件的触发器的未确认事件数。*|
|{TRIGGER.HOSTGROUP.NAME}|→ 基于触发器的通知和命令<br>→ 故障更新通知和命令<br>→ 基于触发器的内部通知<br>→ 手动操作[脚本](/manual/web_interface/frontend_sections/administration/scripts)事件|*定义了触发器的已排序（按 SQL 查询）、以逗号分隔的主机组列表名称。*|
|{TRIGGER.PROBLEM.EVENTS.PROBLEM.ACK}|→ 地图元素标签|*处于PROBLEM状态的触发器已确认的PROBLEM事件数。*|
|{TRIGGER.PROBLEM.EVENTS.PROBLEM.UNACK}|→ 地图元素标签|*处于PROBLEM状态的触发器的未确认PROBLEM事件数。*|
|{TRIGGER.EXPRESSION}|→ 基于触发器的通知和命令<br>→ 故障更新通知和命令<br>→ 基于触发器的内部通知<br>→ 手动操作[脚本](/manual/web_interface/frontend_sections/administration/scripts)事件|*触发器表达式。*|
|{TRIGGER.EXPRESSION.RECOVERY}|→ 基于触发器的通知和命令<br>→ 故障更新通知和命令<br>→ 基于触发器的内部通知<br>→ 手动操作[脚本](/manual/web_interface/frontend_sections/administration/scripts)事件|*触发恢复表达式* 如果*事件生成OK*在[触发器配置](/manual/config/triggers/trigger)被用于'恢复表达'; 否者返回空的string字段。<br>支持的最低版本是 3.2.0.|
|{TRIGGER.ID}|→ 基于触发器的通知和命令<br>→ 故障更新通知和命令<br>→ 基于触发器的内部通知<br>→ 地图元素标签,地图URL的名称和值<br>→ 触发器的URL地址<br>→ Trigger tag value<br>→ 手动操作[脚本](/manual/web_interface/frontend_sections/administration/scripts)事件|*触发此操作的触发器ID。*<br>从Zabbix的4.4.1版本开始在触发标记值中受支持。|
|{TRIGGER.NAME}|→ 基于触发器的通知和命令<br>→ 故障更新通知和命令<br>→ 基于触发器的内部通知<br>→ 手动操作[脚本](/manual/web_interface/frontend_sections/administration/scripts)事件|*触发器的名称* (已解析宏).<br>请注意自4.0.0开始，{EVENT.NAME} 可用于操作以显示已解析的宏触发的事件/问题名称。 |
|{TRIGGER.NAME.ORIG}|→ 基于触发器的通知和命令<br>→ 故障更新通知和命令<br>→ 基于触发器的内部通知<br>→ 手动操作[脚本](/manual/web_interface/frontend_sections/administration/scripts)事件|*触发器的原始名称* (即未解析宏).|
|{TRIGGER.NSEVERITY}|→ 基于触发器的通知和命令<br>→ 故障更新通知和命令<br>→ 基于触发器的内部通知<br>→ 手动操作[脚本](/manual/web_interface/frontend_sections/administration/scripts)事件|*使用数字表达的触发严重性* 可能的值: |0 - 未分类，1 - 信息，2 - 警告，3 - 平均，4 - 高，5 - 灾难。 
|{TRIGGER.SEVERITY}|→ 基于触发器的通知和命令<br>→ 故障更新通知和命令<br>→ 基于触发器的内部通知<br>→ 手动操作[脚本](/manual/web_interface/frontend_sections/administration/scripts)事件|*触发器严重性名称* 可以在这里定义 *管理 → 通用 → 触发器展示选项*.|
|{TRIGGER.STATE}|→ 基于触发器的内部通知|*触发器的最新状态* 可能的值: **未知** 和 **正常**.|
|{TRIGGER.STATUS}|→ 基于触发器的通知和命令<br>→ 故障更新通知和命令<br>→ 手动操作[脚本](/manual/web_interface/frontend_sections/administration/scripts)事件|*当前触发器的值* 可以是 PROBLEM 或者 OK.<br>`{STATUS}` 已弃用。|
|{TRIGGER.TEMPLATE.NAME}|→ 基于触发器的通知和命令<br>→ 故障更新通知和命令<br>→ 基于触发器的内部通知<br>→ 手动操作[脚本](/manual/web_interface/frontend_sections/administration/scripts)事件|*值为已排序（按 SQL 查询）、逗号分隔的模板列表中定义了触发器，如果触发器是在主机中定义的则为 \*未知\*。*|
|{TRIGGER.URL}|→ 基于触发器的通知和命令<br>→ 故障更新通知和命令<br>→ 基于触发器的内部通知<br>→ 手动操作[脚本](/manual/web_interface/frontend_sections/administration/scripts)事件|*触发器的URL地址。*|
|{TRIGGER.VALUE}|→ 基于触发器的通知和命令<br>→ 故障更新通知和命令<br>→ 触发器表达式<br>→ 手动操作[脚本](/manual/web_interface/frontend_sections/administration/scripts)事件|*当前触发数值*: 0 - 触发器处于正常状态, 1 - 触发器处于问题状态。|
|{TRIGGERS.UNACK}|→ 地图元素标签|*地图元素中已取消确认问题触发器的数量，忽略触发器状态。*<br>如果至少有一个问题事件已取消确认，则触发器被视为已取消确认。 |
|{TRIGGERS.PROBLEM.UNACK}|→ 地图元素标签|*在地图标签中未确认的触发器（状态为PROBLEM）总数。*<br>如果至少有一个问题事件已取消确认，则触发器被视为已取消确认。 |
|{TRIGGERS.ACK}|→ 地图元素标签|*地图元素中已确认触发器的数量, 忽略触发器状态。*<br>如果触发器的所有问题事件都被确认，则认为触发器已被确认。 |
|{TRIGGERS.PROBLEM.ACK}|→ 地图元素标签|*地图元素中已确认触发器的数量。*<br>如果触发器的所有问题事件都被确认，则认为触发器已被确认。 |
|{USER.FULLNAME}|→ 故障更新通知和命令<br>→ 对主机手动操作[脚本](/manual/web_interface/frontend_sections/administration/scripts) (including confirmation text)<br>→ 手动操作[脚本](/manual/web_interface/frontend_sections/administration/scripts)事件 (including confirmation text)|在事件确认和脚本操作中添加*用户的姓氏、名字和完整姓名*的信息。<br>从Zabbix的3.4.0版本开始在问题更新功能中支持此宏，从5.0.2开始在全局功能中支持。|
|{USER.NAME}|→ 对主机手动操作[脚本](/manual/web_interface/frontend_sections/administration/scripts) (including confirmation text)<br>→ 手动操作[脚本](/manual/web_interface/frontend_sections/administration/scripts)事件 (including confirmation text)|启动脚本的*用户的名字*。<br>支持的最低版本是 5.0.2.|
|{USER.SURNAME}|→ 对主机手动操作[脚本](/manual/web_interface/frontend_sections/administration/scripts) (including confirmation text)<br>→ 手动操作[脚本](/manual/web_interface/frontend_sections/administration/scripts)事件 (including confirmation text)|启动脚本的*用户的姓氏*。<br>支持的最低版本是 5.0.2.|
|{USER.USERNAME}|→ 对主机手动操作[脚本](/manual/web_interface/frontend_sections/administration/scripts) (including confirmation text)<br>→ 手动操作[脚本](/manual/web_interface/frontend_sections/administration/scripts)事件 (including confirmation text)|启动脚本的*完整用户名称*（包括姓氏和名字）。<br>支持的最低版本是 5.0.2.<br>{USER.ALIAS}仅支持Zabbix 5.4.0以前的版本,此版本已弃用。|
|{$MACRO}|→ 参阅: [User macros supported by location](/manual/appendix/macros/supported_by_location_user)|*[用户自定义](/manual/config/macros/user_macros) 宏。*|
|{\#MACRO}|→ 参阅: [低级别自动发现宏](/manual/config/macros/lld_macros)|*低级别自动发现宏。*<br><br>[定制](/manual/config/macros/macro_functions) 宏的值, 从Zabbix的4.0.0版本开始。|
|{?EXPRESSION}|→ 触发器事件名称<br>→ 基于触发器的通知和命令<br>→ 故障更新通知和命令<br>→ 地图元素标签^[3](supported_by_location#footnotes)^<br>→ 地图分享标签^[3](supported_by_location#footnotes)^<br>→ 地图中的链接标签^[3](supported_by_location#footnotes)^<br>→ 图表名称^[5](supported_by_location#footnotes)^|*表达式宏*用于公式计算。通过扩展内部的所有宏并评估结果表达式来计算。 <br>参阅 [案例](/[[/manual/config/triggers/expression#example_17) 在事件名称中使用表达式宏。<br>支持的最低版本是 5.2.0.|

[comment]: # ({/2ee52a19-6dcfe607})

[comment]: # ({new-d36c0744})
### 1 Macros supported by location

[comment]: # ({/new-d36c0744})

[comment]: # ({new-c2e14e96})

### Actions

|Macro|Supported in|Description|
|--|--------|--------|
|{ACTION.ID}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Service-based notifications and commands<br>→ Service update notifications and commands<br>→ Discovery notifications and commands<br>→ Autoregistration notifications and commands<br>→ Internal notifications|*Numeric ID of the triggered action.*|
|{ACTION.NAME}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Service-based notifications and commands<br>→ Service update notifications and commands<br>→ Discovery notifications and commands<br>→ Autoregistration notifications and commands<br>→ Internal notifications|*Name of the triggered action.*|
|{ALERT.MESSAGE}|→ Alert script parameters|*'Default message' value from action configuration.*<br>Supported since 3.0.0.|
|{ALERT.SENDTO}|→ Alert script parameters|*'Send to' value from user media configuration.*<br>Supported since 3.0.0.|
|{ALERT.SUBJECT}|→ Alert script parameters|*'Default subject' value from action configuration.*<br>Supported since 3.0.0.|
|{ESC.HISTORY} |→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Service-based notifications and commands<br>→ Service update notifications and commands<br>→ Internal notifications |*Escalation history. Log of previously sent messages.*<br>Shows previously sent notifications, on which escalation step they were sent and their status (*sent*, *in progress* or *failed*).|

[comment]: # ({/new-c2e14e96})

[comment]: # ({new-e2629abe})

### Date and time

|Macro|Supported in|Description|
|--|--------|--------|
|{DATE}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Service-based notifications and commands<br>→ Service update notifications and commands<br>→ Discovery notifications and commands<br>→ Autoregistration notifications and commands<br>→ Internal notifications<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)|*Current date in yyyy.mm.dd. format.*|
|{TIME}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Service-based notifications and commands<br>→ Service update notifications and commands<br>→ Discovery notifications and commands<br>→ Autoregistration notifications and commands<br>→ Internal notifications<br>→ Trigger event names<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)|*Current time in hh:mm:ss.*|

[comment]: # ({/new-e2629abe})

[comment]: # ({new-0862ff4d})

### Discovery

|Macro|Supported in|Description|
|--|--------|--------|
|{DISCOVERY.DEVICE.IPADDRESS}|→ Discovery notifications and commands|*IP address of the discovered device.*<br>Available always, does not depend on host being added.|
|{DISCOVERY.DEVICE.DNS}|→ Discovery notifications and commands|*DNS name of the discovered device.*<br>Available always, does not depend on host being added.|
|{DISCOVERY.DEVICE.STATUS}|→ Discovery notifications and commands|*Status of the discovered device*: can be either UP or DOWN.|
|{DISCOVERY.DEVICE.UPTIME}|→ Discovery notifications and commands|*Time since the last change of discovery status for a particular device*, with precision down to a second.<br>For example: 1h 29m 01s.<br>For devices with status DOWN, this is the period of their downtime.|
|{DISCOVERY.RULE.NAME}|→ Discovery notifications and commands|*Name of the discovery rule that discovered the presence or absence of the device or service.*|
|{DISCOVERY.SERVICE.NAME}|→ Discovery notifications and commands|*Name of the service that was discovered.*<br>For example: HTTP.|
|{DISCOVERY.SERVICE.PORT}|→ Discovery notifications and commands|*Port of the service that was discovered.*<br>For example: 80.|
|{DISCOVERY.SERVICE.STATUS}|→ Discovery notifications and commands|*Status of the discovered <service://> can be either UP or DOWN.* | 
|{DISCOVERY.SERVICE.UPTIME} |→ Discovery notifications and commands |*Time since the last change of discovery status for a particular service*, with precision down to a second.<br>For example: 1h 29m 01s.<br>For services with status DOWN, this is the period of their downtime. | 

[comment]: # ({/new-0862ff4d})

[comment]: # ({new-a970bbfc})

### Events

|Macro|Supported in|Description|
|--|--------|--------|
|{EVENT.ACK.STATUS}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)|*Acknowledgment status of the event (Yes/No)*.|
|{EVENT.AGE}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Service-based notifications and commands<br>→ Service update notifications and commands<br>→ Service recovery notifications and commands<br>→ Discovery notifications and commands<br>→ Autoregistration notifications and commands<br>→ Internal notifications<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)|*Age of the event that triggered an action*, with precision down to a second.<br>Useful in escalated messages.|
|{EVENT.DATE}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Service-based notifications and commands<br>→ Service update notifications and commands<br>→ Service recovery notifications and commands<br>→ Discovery notifications and commands<br>→ Autoregistration notifications and commands<br>→ Internal notifications<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)|*Date of the event that triggered an action.*|
|{EVENT.DURATION}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Service-based notifications and commands<br>→ Service update notifications and commands<br>→ Service recovery notifications and commands<br>→ Internal notifications<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)|*Duration of the event (time difference between problem and recovery events)*, with precision down to a second.<br>Useful in problem recovery messages.<br><br>Supported since 5.0.0.|
|{EVENT.ID}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Service-based notifications and commands<br>→ Service update notifications and commands<br>→ Service recovery notifications and commands<br>→ Discovery notifications and commands<br>→ Autoregistration notifications and commands<br>→ Internal notifications<br>→ Trigger URLs<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)|*Numeric ID of the event that triggered an action.*|
|{EVENT.NAME}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Service-based notifications and commands<br>→ Service update notifications and commands<br>→ Service recovery notifications and commands<br>→ Internal notifications<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)|*Name of the problem event that triggered an action.*<br>Supported since 4.0.0.|
|{EVENT.NSEVERITY}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Service-based notifications and commands<br>→ Service update notifications and commands<br>→ Service recovery notifications and commands<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)|*Numeric value of the event severity.* Possible values: 0 - Not classified, 1 - Information, 2 - Warning, 3 - Average, 4 - High, 5 - Disaster.<br>Supported since 4.0.0.|
|{EVENT.OBJECT}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Service-based notifications and commands<br>→ Service update notifications and commands<br>→ Service recovery notifications and commands<br>→ Discovery notifications and commands<br>→ Autoregistration notifications and commands<br>→ Internal notifications<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)|*Numeric value of the event object.* Possible values: 0 - Trigger, 1 - Discovered host, 2 - Discovered service, 3 - Autoregistration, 4 - Item, 5 - Low-level discovery rule.<br>Supported since 4.4.0.|
|{EVENT.OPDATA}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)|*Operational data of the underlying trigger of a problem.*<br>Supported since 4.4.0.|
|{EVENT.RECOVERY.DATE}|→ Problem [recovery notifications](/manual/config/notifications/action/recovery_operations) and commands<br>→ Problem update notifications and commands (if recovery took place)<br>→ Service recovery notifications and commands<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts) (if recovery took place)|*Date of the recovery event.*|
|{EVENT.RECOVERY.ID}|→ Problem [recovery notifications](/manual/config/notifications/action/recovery_operations) and commands<br>→ Problem update notifications and commands (if recovery took place)<br>→ Service recovery notifications and commands<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts) (if recovery took place)|*Numeric ID of the recovery event.*|
|{EVENT.RECOVERY.NAME}|→ Problem [recovery notifications](/manual/config/notifications/action/recovery_operations) and commands<br>→ Problem update notifications and commands (if recovery took place)<br>→ Service recovery notifications and commands<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts) (if recovery took place)|*Name of the recovery event.*<br>Supported since 4.4.1.|
|{EVENT.RECOVERY.STATUS}|→ Problem [recovery notifications](/manual/config/notifications/action/recovery_operations) and commands<br>→ Problem update notifications and commands (if recovery took place)<br>→ Service recovery notifications and commands<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts) (if recovery took place)|*Verbal value of the recovery event.*|
|{EVENT.RECOVERY.TAGS}|→ Problem [recovery notifications](/manual/config/notifications/action/recovery_operations) and commands<br>→ Problem update notifications and commands (if recovery took place)<br>→ Service recovery notifications and commands<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts) (if recovery took place)|*A comma separated list of recovery event tags.* Expanded to an empty string if no tags exist.<br>Supported since 3.2.0.|
|{EVENT.RECOVERY.TAGSJSON}|→ Problem [recovery notifications](/manual/config/notifications/action/recovery_operations) and commands<br>→ Problem update notifications and commands (if recovery took place)<br>→ Service recovery notifications and commands<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts) (if recovery took place)|*A JSON array containing event tag [objects](/manual/api/reference/event/object#event_tag).* Expanded to an empty array if no tags exist.<br>Supported since 5.0.0.|
|{EVENT.RECOVERY.TIME}|→ Problem [recovery notifications](/manual/config/notifications/action/recovery_operations) and commands<br>→ Problem update notifications and commands (if recovery took place)<br>→ Service recovery notifications and commands<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts) (if recovery took place)|*Time of the recovery event.*|
|{EVENT.RECOVERY.VALUE}|→ Problem [recovery notifications](/manual/config/notifications/action/recovery_operations) and commands<br>→ Problem update notifications and commands (if recovery took place)<br>→ Service recovery notifications and commands<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts) (if recovery took place)|*Numeric value of the recovery event.*|
|{EVENT.SEVERITY}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Service-based notifications and commands<br>→ Service update notifications and commands<br>→ Service recovery notifications and commands<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)|*Name of the event severity.*<br>Supported since 4.0.0.|
|{EVENT.SOURCE}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Service-based notifications and commands<br>→ Service update notifications and commands<br>→ Service recovery notifications and commands<br>→ Discovery notifications and commands<br>→ Autoregistration notifications and commands<br>→ Internal notifications<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)|*Numeric value of the event source.* Possible values: 0 - Trigger, 1 - Discovery, 2 - Autoregistration, 3 - Internal.<br>Supported since 4.4.0.|
|{EVENT.STATUS}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Service-based notifications and commands<br>→ Service update notifications and commands<br>→ Service recovery notifications and commands<br>→ Internal notifications<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)|*Verbal value of the event that triggered an action.*|
|{EVENT.TAGS}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Service-based notifications and commands<br>→ Service update notifications and commands<br>→ Service recovery notifications and commands<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)|*A comma separated list of event tags.* Expanded to an empty string if no tags exist.<br>Supported since 3.2.0.|
|{EVENT.TAGSJSON}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Service-based notifications and commands<br>→ Service update notifications and commands<br>→ Service recovery notifications and commands<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)|*A JSON array containing event tag [objects](/manual/api/reference/event/object#event_tag).* Expanded to an empty array if no tags exist.<br>Supported since 5.0.0.|
|{EVENT.TAGS.<tag name>}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Service-based notifications and commands<br>→ Service update notifications and commands<br>→ Service recovery notifications and commands<br>→ Webhook media type URL names and URLs<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)|*Event tag value referenced by the tag name.*<br>A tag name containing non-alphanumeric characters (including non-English multibyte-UTF characters) should be double quoted. Quotes and backslashes inside a quoted tag name must be escaped with a backslash.<br>Supported since 4.4.2.|
|{EVENT.TIME}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Service-based notifications and commands<br>→ Service update notifications and commands<br>→ Service recovery notifications and commands<br>→ Discovery notifications and commands<br>→ Autoregistration notifications and commands<br>→ Internal notifications<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)|*Time of the event that triggered an action.*|
|{EVENT.UPDATE.ACTION}|→ Problem update notifications and commands|*Human-readable name of the action(s)* performed during [problem update](/manual/acknowledgment#updating_problems).<br>Resolves to the following values: *acknowledged*, *commented*, *changed severity from (original severity) to (updated severity)* and *closed* (depending on how many actions are performed in one update).<br>Supported since 4.0.0.|
|{EVENT.UPDATE.DATE}|→ Problem update notifications and commands<br>→ Service update notifications and commands|*Date of event [update](/manual/config/notifications/action/update_operations) (acknowledgment, etc).*<br>Deprecated name: {ACK.DATE}|
|{EVENT.UPDATE.HISTORY}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)|*Log of problem updates (acknowledgments, etc).*<br>Deprecated name: {EVENT.ACK.HISTORY}|
|{EVENT.UPDATE.MESSAGE}|→ Problem update notifications and commands|*Problem update message.*<br>Deprecated name: {ACK.MESSAGE}|
|{EVENT.UPDATE.STATUS}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)|*Numeric value of the problem update status.* Possible values: 0 - Webhook was called because of problem/recovery event, 1 - Update operation.<br>Supported since 4.4.0.|
|{EVENT.UPDATE.TIME}|→ Problem update notifications and commands<br>→ Service update notifications and commands|*Time of event [update](/manual/config/notifications/action/update_operations) (acknowledgment, etc).*<br>Deprecated name: {ACK.TIME}|
|{EVENT.VALUE}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Service-based notifications and commands<br>→ Service update notifications and commands<br>→ Service recovery notifications and commands<br>→ Internal notifications<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)|*Numeric value of the event that triggered an action (1 for problem, 0 for recovering).*|

[comment]: # ({/new-a970bbfc})

[comment]: # ({new-c2d6fe18})

### Cause and symptom events

{EVENT.CAUSE.*} macros are used in the context of a symptom event, for example, in notifications; they return information about the cause event. 

The {EVENT.SYMPTOMS} macro is used in the context of the cause event and returns information about symptom events.

|Macro|Supported in|Description|
|--|--------|--------|
|{EVENT.CAUSE.ACK.STATUS}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)|*Acknowledgment status of the cause event (Yes/No)*.|
|{EVENT.CAUSE.AGE}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)|*Age of the cause event*, with precision down to a second.<br>Useful in escalated messages.|
|{EVENT.CAUSE.DATE}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)|*Date of the cause event.*|
|{EVENT.CAUSE.DURATION}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)|*Duration of the cause event (time difference between problem and recovery events)*, with precision down to a second.<br>Useful in problem recovery messages.|
|{EVENT.CAUSE.ID}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)|*Numeric ID of the cause event .*|
|{EVENT.CAUSE.NAME}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)|*Name of the cause problem event.*|
|{EVENT.CAUSE.NSEVERITY}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)|*Numeric value of the cause event severity.*<br>Possible values: 0 - Not classified, 1 - Information, 2 - Warning, 3 - Average, 4 - High, 5 - Disaster.|
|{EVENT.CAUSE.OBJECT}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)|*Numeric value of the cause event object.*<br>Possible values: 0 - Trigger, 1 - Discovered host, 2 - Discovered service, 3 - Autoregistration, 4 - Item, 5 - Low-level discovery rule.|
|{EVENT.CAUSE.OPDATA}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)|*Operational data of the underlying trigger of the cause problem.*|
|{EVENT.CAUSE.SEVERITY}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)|*Name of the cause event severity.*|
|{EVENT.CAUSE.SOURCE}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)|*Numeric value of the cause event source.*<br>Possible values: 0 - Trigger, 1 - Discovery, 2 - Autoregistration, 3 - Internal.|
|{EVENT.CAUSE.STATUS}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)|*Verbal value of the cause event.*|
|{EVENT.CAUSE.TAGS}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)|*A comma separated list of cause event tags.*<br>Expanded to an empty string if no tags exist.|
|{EVENT.CAUSE.TAGSJSON}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)|*A JSON array containing cause event tag [objects](/manual/api/reference/event/object#event_tag).*<br>Expanded to an empty array if no tags exist.|
|{EVENT.CAUSE.TAGS.<tag name>}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)|*Cause event tag value referenced by the tag name.*<br>A tag name containing non-alphanumeric characters (including non-English multibyte-UTF characters) should be double quoted. Quotes and backslashes inside a quoted tag name must be escaped with a backslash.|
|{EVENT.CAUSE.TIME}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)|*Time of the cause event.*|
|{EVENT.CAUSE.UPDATE.HISTORY}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)|*Log of cause problem updates (acknowledgments, etc).*|
|{EVENT.CAUSE.VALUE}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)|*Numeric value of the cause event (1 for problem, 0 for recovering).*|
|{EVENT.SYMPTOMS}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)|*The list of symptom events.*<br>Includes the following details: host name, event name, severity, age, service tags and values.|

[comment]: # ({/new-c2d6fe18})

[comment]: # ({new-a4586852})

### Functions

|Macro|Supported in|Description|
|--|--------|--------|
|{FUNCTION.VALUE<1-9>}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Event names|*Results of the Nth item-based function in the trigger expression at the time of the event.*<br>Only functions with */host/key* as the first parameter are counted. See [indexed macros](supported_by_location#indexed_macros).|
|{FUNCTION.RECOVERY.VALUE<1-9>}|→ Problem [recovery notifications](/manual/config/notifications/action/recovery_operations) and commands<br>→ Problem update notifications and commands<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)|*Results of the Nth item-based function in the recovery expression at the time of the event.*<br>Only functions with */host/key* as the first parameter are counted. See [indexed macros](supported_by_location#indexed_macros).|

[comment]: # ({/new-a4586852})

[comment]: # ({new-0559d116})

### Hosts

|Macro|Supported in|Description|
|--|--------|--------|
|{HOST.CONN}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ Map element labels, map URL names and values<br>→ Item key parameters^[1](supported_by_location#footnotes)^<br>→ Host interface IP/DNS<br>→ Trapper item "Allowed hosts" field<br>→ Database monitoring additional parameters<br>→ SSH and Telnet scripts<br>→ JMX item endpoint field<br>→ Web monitoring^[4](supported_by_location#footnotes)^<br>→ Low-level discovery rule filter regular expressions<br>→ URL field of dynamic URL dashboard widget<br>→ Trigger names, event names, operational data and descriptions<br>→ Trigger URLs<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Script-type item, item prototype and discovery rule parameter names and values<br>→ HTTP agent type item, item prototype and discovery rule fields:<br>URL, Query fields, Request body, Headers, Proxy, SSL certificate file, SSL key file, Allowed hosts.<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts) (including confirmation text)<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts) (including confirmation text)<br>→ Description of item value widget|*Host IP address or DNS name, depending on host settings*^[2](supported_by_location#footnotes)^.<br><br>May be used with a numeric index as {HOST.CONN[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{HOST.DESCRIPTION}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ Map element labels<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Host description.*<br><br>This macro may be used with a numeric index e.g. {HOST.DESCRIPTION[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{HOST.DNS}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ Map element labels, map URL names and values<br>→ Item key parameters^[1](supported_by_location#footnotes)^<br>→ Host interface IP/DNS<br>→ Trapper item "Allowed hosts" field<br>→ Database monitoring additional parameters<br>→ SSH and Telnet scripts<br>→ JMX item endpoint field<br>→ Web monitoring^[4](supported_by_location#footnotes)^<br>→ Low-level discovery rule filter regular expressions<br>→ URL field of dynamic URL dashboard widget<br>→ Trigger names, event names, operational data and descriptions<br>→ Trigger URLs<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Script-type item, item prototype and discovery rule parameter names and values<br>→ HTTP agent type item, item prototype and discovery rule fields:<br>URL, Query fields, Request body, Headers, Proxy, SSL certificate file, SSL key file, Allowed hosts.<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts) (including confirmation text)<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts) (including confirmation text)<br>→ Description of item value widget|*Host DNS name*^[2](supported_by_location#footnotes)^.<br><br>This macro may be used with a numeric index e.g. {HOST.DNS[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{HOST.HOST}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Autoregistration notifications and commands<br>→ Internal notifications<br>→ Item key parameters<br>→ Map element labels, map URL names and values<br>→ Host interface IP/DNS<br>→ Trapper item "Allowed hosts" field<br>→ Database monitoring additional parameters<br>→ SSH and Telnet scripts<br>→ JMX item endpoint field<br>→ Web monitoring^[4](supported_by_location#footnotes)^<br>→ Low-level discovery rule filter regular expressions<br>→ URL field of dynamic URL dashboard widget<br>→ Trigger names, event names, operational data and descriptions<br>→ Trigger URLs<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Script-type item, item prototype and discovery rule parameter names and values<br>→ HTTP agent type item, item prototype and discovery rule fields:<br>URL, Query fields, Request body, Headers, Proxy, SSL certificate file, SSL key file, Allowed hosts.<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts) (including confirmation text)<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts) (including confirmation text)<br>→ Description of item value widget|*Host name.*<br><br>This macro may be used with a numeric index e.g. {HOST.HOST[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).<br><br>`{HOSTNAME<1-9>}` is deprecated.|
|{HOST.ID}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ Map element labels, map URL names and values<br>→ URL field of dynamic URL dashboard widget<br>→ Trigger URLs<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts) (only for type URL, including confirmation text)<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts) (only for type URL, including confirmation text)<br>→ Description of item value widget|*Host ID.*<br><br>May be used with a numeric index as {HOST.ID[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{HOST.IP}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Autoregistration notifications and commands<br>→ Internal notifications<br>→ Map element labels, map URL names and values<br>→ Item key parameters^[1](supported_by_location#footnotes)^<br>→ Host interface IP/DNS<br>→ Trapper item "Allowed hosts" field<br>→ Database monitoring additional parameters<br>→ SSH and Telnet scripts<br>→ JMX item endpoint field<br>→ Web monitoring^[4](supported_by_location#footnotes)^<br>→ Low-level discovery rule filter regular expressions<br>→ URL field of dynamic URL dashboard widget<br>→ Trigger names, event names, operational data and descriptions<br>→ Trigger URLs<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Script-type item, item prototype and discovery rule parameter names and values<br>→ HTTP agent type item, item prototype and discovery rule fields:<br>URL, Query fields, Request body, Headers, Proxy, SSL certificate file, SSL key file, Allowed hosts.<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts) (including confirmation text)<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts) (including confirmation text)<br>→ Description of item value widget|*Host IP address*^[2](supported_by_location#footnotes)^.<br><br>This macro may be used with a numeric index e.g. {HOST.IP[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).<br><br>`{IPADDRESS<1-9>}` is deprecated.|
|{HOST.METADATA}|→ Autoregistration notifications and commands|*Host metadata.*<br>Used only for active agent autoregistration.|
|{HOST.NAME}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ Map element labels, map URL names and values<br>→ Item key parameters<br>→ Host interface IP/DNS<br>→ Trapper item "Allowed hosts" field<br>→ Database monitoring additional parameters<br>→ SSH and Telnet scripts<br>→ Web monitoring^[4](supported_by_location#footnotes)^<br>→ Low-level discovery rule filter regular expressions<br>→ URL field of dynamic URL dashboard widget<br>→ Trigger names, event names, operational data and descriptions<br>→ Trigger URLs<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Script-type item, item prototype and discovery rule parameter names and values<br>→ HTTP agent type item, item prototype and discovery rule fields:<br>URL, Query fields, Request body, Headers, Proxy, SSL certificate file, SSL key file, Allowed hosts.<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts) (including confirmation text)<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts) (including confirmation text)<br>→ Description of item value widget|*Visible host name.*<br><br>This macro may be used with a numeric index e.g. {HOST.NAME[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{HOST.PORT}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Autoregistration notifications and commands<br>→ Internal notifications<br>→ Trigger names, event names, operational data and descriptions<br>→ Trigger URLs<br>→ JMX item endpoint field<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Host (agent) port*^[2](supported_by_location#footnotes)^.<br><br>This macro may be used with a numeric index e.g. {HOST.PORT[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{HOST.TARGET.CONN}|→ Trigger-based commands<br>→ Problem update commands<br>→ Discovery commands<br>→ Autoregistration commands|IP address or DNS name of the target host, depending on host settings.<br>Supported since 5.4.0.|
|{HOST.TARGET.DNS}|→ Trigger-based commands<br>→ Problem update commands<br>→ Discovery commands<br>→ Autoregistration commands|DNS name of the target host.<br>Supported since 5.4.0.|
|{HOST.TARGET.HOST}|→ Trigger-based commands<br>→ Problem update commands<br>→ Discovery commands<br>→ Autoregistration commands|Technical name of the target host.<br>Supported since 5.4.0.|
|{HOST.TARGET.IP}|→ Trigger-based commands<br>→ Problem update commands<br>→ Discovery commands<br>→ Autoregistration commands|IP address of the target host.<br>Supported since 5.4.0.|
|{HOST.TARGET.NAME}|→ Trigger-based commands<br>→ Problem update commands<br>→ Discovery commands<br>→ Autoregistration commands|Visible name of the target host.<br>Supported since 5.4.0.|

See also: [Host inventory](#host-inventory)

[comment]: # ({/new-0559d116})

[comment]: # ({new-5140de49})

### Host groups

|Macro|Supported in|Description|
|--|--------|--------|
|{HOSTGROUP.ID}|→ Map element labels, map URL names and values|*Host group ID.*|

[comment]: # ({/new-5140de49})

[comment]: # ({new-15530041})

### Host inventory

|Macro|Supported in|Description|
|--|--------|--------|
|{INVENTORY.ALIAS}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Alias field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.ALIAS[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.ASSET.TAG}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Asset tag field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.ASSET.TAG[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.CHASSIS}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Chassis field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.CHASSIS[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.CONTACT}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Contact field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.CONTACT[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).<br><br>`{PROFILE.CONTACT<1-9>}` is deprecated.|
|{INVENTORY.CONTRACT.NUMBER}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Contract number field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.CONTRACT.NUMBER[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.DEPLOYMENT.STATUS}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Deployment status field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.DEPLOYMENT.STATUS[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.HARDWARE}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Hardware field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.HARDWARE[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).<br><br>`{PROFILE.HARDWARE<1-9>}` is deprecated.|
|{INVENTORY.HARDWARE.FULL}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Hardware (Full details) field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.HARDWARE.FULL[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.HOST.NETMASK}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Host subnet mask field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.HOST.NETMASK[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.HOST.NETWORKS}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Host networks field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.HOST.NETWORKS[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.HOST.ROUTER}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Host router field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.HOST.ROUTER[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.HW.ARCH}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Hardware architecture field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.HW.ARCH[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.HW.DATE.DECOMM}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Date hardware decommissioned field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.HW.DATE.DECOMM[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.HW.DATE.EXPIRY}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Date hardware maintenance expires field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.HW.DATE.EXPIRY[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.HW.DATE.INSTALL}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Date hardware installed field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.HW.DATE.INSTALL[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.HW.DATE.PURCHASE}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Date hardware purchased field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.HW.DATE.PURCHASE[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.INSTALLER.NAME}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Installer name field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.INSTALLER.NAME[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.LOCATION}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Sc→ ript-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Location field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.LOCATION[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).<br><br>`{PROFILE.LOCATION<1-9>}` is deprecated.|
|{INVENTORY.LOCATION.LAT}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Location latitude field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.LOCATION.LAT[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.LOCATION.LON}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Location longitude field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.LOCATION.LON[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.MACADDRESS.A}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*MAC address A field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.MACADDRESS.A[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).<br><br>`{PROFILE.MACADDRESS<1-9>}` is deprecated.|
|{INVENTORY.MACADDRESS.B}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*MAC address B field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.MACADDRESS.B[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.MODEL}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Model field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.MODEL[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.NAME}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Name field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.NAME[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).<br><br>`{PROFILE.NAME<1-9>}` is deprecated.|
|{INVENTORY.NOTES}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Notes field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.NOTES[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).<br><br>`{PROFILE.NOTES<1-9>}` is deprecated.|
|{INVENTORY.OOB.IP}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*OOB IP address field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.OOB.IP[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.OOB.NETMASK}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*OOB subnet mask field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.OOB.NETMASK[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.OOB.ROUTER}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*OOB router field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.OOB.ROUTER[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.OS}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*OS field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.OS[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).<br><br>`{PROFILE.OS<1-9>}` is deprecated.|
|{INVENTORY.OS.FULL}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*OS (Full details) field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.OS.FULL[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.OS.SHORT}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*OS (Short) field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.OS.SHORT[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.POC.PRIMARY.CELL}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Primary POC cell field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.POC.PRIMARY.CELL[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.POC.PRIMARY.EMAIL}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Primary POC email field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.POC.PRIMARY.EMAIL[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.POC.PRIMARY.NAME}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Primary POC name field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.POC.PRIMARY.NAME[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.POC.PRIMARY.NOTES}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Primary POC notes field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.POC.PRIMARY.NOTES[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.POC.PRIMARY.PHONE.A}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Primary POC phone A field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.POC.PRIMARY.PHONE.A[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.POC.PRIMARY.PHONE.B}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Primary POC phone B field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.POC.PRIMARY.PHONE.B[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.POC.PRIMARY.SCREEN}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Primary POC screen name field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.POC.PRIMARY.SCREEN[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.POC.SECONDARY.CELL}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Secondary POC cell field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.POC.SECONDARY.CELL[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.POC.SECONDARY.EMAIL}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Secondary POC email field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.POC.SECONDARY.EMAIL[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.POC.SECONDARY.NAME}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Secondary POC name field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.POC.SECONDARY.NAME[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.POC.SECONDARY.NOTES}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Secondary POC notes field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.POC.SECONDARY.NOTES[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.POC.SECONDARY.PHONE.A}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Secondary POC phone A field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.POC.SECONDARY.PHONE.A[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.POC.SECONDARY.PHONE.B}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Secondary POC phone B field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.POC.SECONDARY.PHONE.B[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.POC.SECONDARY.SCREEN}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Secondary POC screen name field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.POC.SECONDARY.SCREEN[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.SERIALNO.A}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Serial number A field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.SERIALNO.A[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).<br><br>`{PROFILE.SERIALNO<1-9>}` is deprecated.|
|{INVENTORY.SERIALNO.B}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Serial number B field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.SERIALNO.B[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.SITE.ADDRESS.A}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Site address A field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.SITE.ADDRESS.A[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.SITE.ADDRESS.B}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Site address B field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.SITE.ADDRESS.B[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.SITE.ADDRESS.C}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Site address C field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.SITE.ADDRESS.C[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.SITE.CITY}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Site city field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.SITE.CITY[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.SITE.COUNTRY}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Site country field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.SITE.COUNTRY[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.SITE.NOTES}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Site notes field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.SITE.NOTES[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.SITE.RACK}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Site rack location field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.SITE.RACK[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.SITE.STATE}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Site state/province field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.SITE.STATE[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.SITE.ZIP}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Site ZIP/postal field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.SITE.ZIP[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.SOFTWARE}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Software field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.SOFTWARE[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).<br><br>`{PROFILE.SOFTWARE<1-9>}` is deprecated.|
|{INVENTORY.SOFTWARE.APP.A}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Software application A field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.SOFTWARE.APP.A[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.SOFTWARE.APP.B}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Software application B field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.SOFTWARE.APP.B[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.SOFTWARE.APP.C}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Software application C field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.SOFTWARE.APP.C[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.SOFTWARE.APP.D}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Software application D field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.SOFTWARE.APP.D[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.SOFTWARE.APP.E}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Software application E field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.SOFTWARE.APP.E[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.SOFTWARE.FULL}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Software (Full details) field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.SOFTWARE.FULL[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.TAG}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Tag field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.TAG[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).<br><br>`{PROFILE.TAG<1-9>}` is deprecated.|
|{INVENTORY.TYPE}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Type field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.TYPE[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).<br><br>`{PROFILE.DEVICETYPE<1-9>}` is deprecated.|
|{INVENTORY.TYPE.FULL}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Type (Full details) field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.TYPE.FULL[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.URL.A}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*URL A field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.URL.A[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.URL.B}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*URL B field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.URL.B[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.URL.C}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*URL C field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.URL.C[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{INVENTORY.VENDOR}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Map element labels, map URL names and values<br>→ Script-type items^[6](supported_by_location#footnotes)^<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)^[6](supported_by_location#footnotes)^<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Vendor field in host inventory.*<br><br>This macro may be used with a numeric index e.g. {INVENTORY.VENDOR[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|

[comment]: # ({/new-15530041})

[comment]: # ({new-d0b03c62})

### Items

|Macro|Supported in|Description|
|--|--------|--------|
|{ITEM.DESCRIPTION}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Description of the Nth item in the trigger expression that caused a notification.*<br><br>This macro may be used with a numeric index e.g. {ITEM.DESCRIPTION[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{ITEM.DESCRIPTION.ORIG}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Description (with macros unresolved) of the Nth item in the trigger expression that caused a notification.*<br><br>This macro may be used with a numeric index e.g. {ITEM.DESCRIPTION.ORIG[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).<br><br>Supported since 5.2.0.|
|{ITEM.ID}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ Script-type item, item prototype and discovery rule parameter names and values<br>→ HTTP agent type item, item prototype and discovery rule fields:<br>URL, query fields, request body, headers, proxy, SSL certificate file, SSL key file<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Numeric ID of the Nth item in the trigger expression that caused a notification.*<br><br>This macro may be used with a numeric index e.g. {ITEM.ID[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{ITEM.KEY}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ Script-type item, item prototype and discovery rule parameter names and values<br>→ HTTP agent type item, item prototype and discovery rule fields:<br>URL, query fields, request body, headers, proxy, SSL certificate file, SSL key file<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Key of the Nth item in the trigger expression that caused a notification.*<br><br>This macro may be used with a numeric index e.g. {ITEM.KEY[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).<br><br>`{TRIGGER.KEY}` is deprecated.|
|{ITEM.KEY.ORIG}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ Script-type item, item prototype and discovery rule parameter names and values<br>→ HTTP agent type item, item prototype and discovery rule fields:<br>URL, Query fields, Request body, Headers, Proxy, SSL certificate file, SSL key file, Allowed hosts.<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Original key (with macros not expanded) of the Nth item in the trigger expression that caused a notification ^[4](supported_by_location#footnotes)^.*<br><br>This macro may be used with a numeric index e.g. {ITEM.KEY.ORIG[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{ITEM.LASTVALUE}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Trigger names, event names, operational data and descriptions<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Trigger URLs<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*The latest value of the Nth item in the trigger expression that caused a notification.*<br>It will resolve to \*UNKNOWN\* in the frontend if the latest history value has been collected more than the *Max history display period* time ago (set in the [Administration→General](/manual/web_interface/frontend_sections/administration/general#gui) menu section).<br>Note that since 4.0, when used in the problem name, it will not resolve to the latest item value when viewing problem events, instead it will keep the item value from the time of problem happening.<br>It is alias to `last(/{HOST.HOST}/{ITEM.KEY})`.<br><br>The resolved value is truncated to 20 characters to be usable, for example, in trigger URLs. To resolve to a full value, you may use [macro functions](/manual/config/macros/macro_functions#seeing-full-item-values).<br><br>[Customizing](/manual/config/macros/macro_functions) the macro value is supported for this macro; starting with Zabbix 3.2.0.<br><br>This macro may be used with a numeric index e.g. {ITEM.LASTVALUE[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. item in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{ITEM.LOG.AGE}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Trigger names, operational data and descriptions<br>→ Trigger URLs<br>→ Event tags and values<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Age of the log item event*, with precision down to a second.<br><br>This macro may be used with a numeric index e.g. {ITEM.LOG.AGE[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{ITEM.LOG.DATE}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Trigger names, operational data and descriptions<br>→ Trigger URLs<br>→ Event tags and values<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Date of the log item event.*<br><br>This macro may be used with a numeric index e.g. {ITEM.LOG.DATE[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{ITEM.LOG.EVENTID}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Trigger names, operational data and descriptions<br>→ Trigger URLs<br>→ Event tags and values<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*ID of the event in the event log.*<br>For Windows event log monitoring only.<br><br>This macro may be used with a numeric index e.g. {ITEM.LOG.EVENTID[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{ITEM.LOG.NSEVERITY}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Trigger names, operational data and descriptions<br>→ Trigger URLs<br>→ Event tags and values<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Numeric severity of the event in the event log.*<br>For Windows event log monitoring only.<br><br>This macro may be used with a numeric index e.g. {ITEM.LOG.NSEVERITY[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{ITEM.LOG.SEVERITY}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Trigger names, operational data and descriptions<br>→ Trigger URLs<br>→ Event tags and values<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Verbal severity of the event in the event log.*<br>For Windows event log monitoring only.<br><br>This macro may be used with a numeric index e.g. {ITEM.LOG.SEVERITY[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{ITEM.LOG.SOURCE}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Trigger names, operational data and descriptions<br>→ Trigger URLs<br>→ Event tags and values<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Source of the event in the event log.*<br>For Windows event log monitoring only.<br><br>This macro may be used with a numeric index e.g. {ITEM.LOG.SOURCE[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{ITEM.LOG.TIME}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Trigger names, operational data and descriptions<br>→ Trigger URLs<br>→ Event tags and values<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Time of the log item event.*<br><br>This macro may be used with a numeric index e.g. {ITEM.LOG.TIME[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{ITEM.NAME}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Name of the Nth item in the trigger expression that caused a notification.*<br><br>This macro may be used with a numeric index e.g. {ITEM.NAME[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{ITEM.NAME.ORIG}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|This macros is deprecated since Zabbix 6.0. It used to resolve to the *original name (i.e. without macros resolved) of the item* in pre-6.0 Zabbix versions when user macros and positional macros were supported in the item name.<br><br>This macro may be used with a numeric index e.g. {ITEM.NAME.ORIG[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{ITEM.STATE}|→ Item-based internal notifications<br>→ Description of item value widget|*The latest state of the Nth item in the trigger expression that caused a notification.* Possible values: **Not supported** and **Normal**.<br><br>This macro may be used with a numeric index e.g. {ITEM.STATE[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{ITEM.STATE.ERROR}|→ Item-based internal notifications|*Error message with details why an item became unsupported.* <br> <br> If an item goes into the unsupported state and then immediately gets supported again the error field can be empty.|
|{ITEM.VALUE}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Trigger names, event names, operational data and descriptions<br>→ [Tag names and values](/manual/config/tagging#macro_support)<br>→ Trigger URLs<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|Resolved to either:<br>1) the historical (at-the-time-of-event) value of the Nth item in the trigger expression, if used in the context of trigger status change, for example, when displaying events or sending notifications.<br>2) the latest value of the Nth item in the trigger expression, if used without the context of trigger status change, for example, when displaying a list of triggers in a pop-up selection window. In this case works the same as {ITEM.LASTVALUE}<br>In the first case it will resolve to \*UNKNOWN\* if the history value has already been deleted or has never been stored.<br>In the second case, and in the frontend only, it will resolve to \*UNKNOWN\* if the latest history value has been collected more than the *Max history display period* time ago (set in the [Administration→General](/manual/web_interface/frontend_sections/administration/general#gui) menu section).<br><br>The resolved value is truncated to 20 characters to be usable, for example, in trigger URLs. To resolve to a full value, you may use [macro functions](/manual/config/macros/macro_functions#seeing-full-item-values).<br><br>[Customizing](/manual/config/macros/macro_functions) the macro value is supported for this macro, starting with Zabbix 3.2.0.<br><br>This macro may be used with a numeric index e.g. {ITEM.VALUE[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. item in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{ITEM.VALUETYPE}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Internal notifications<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Description of item value widget|*Value type of the Nth item in the trigger expression that caused a notification.* Possible values: 0 - numeric float, 1 - character, 2 - log, 3 - numeric unsigned, 4 - text.<br><br>This macro may be used with a numeric index e.g. {ITEM.VALUETYPE[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).<br><br>Supported since 5.4.0.|

[comment]: # ({/new-d0b03c62})

[comment]: # ({new-d60e314e})

### Low-level discovery rules

|Macro|Supported in|Description|
|--|--------|--------|
|{LLDRULE.DESCRIPTION}|→ LLD-rule based internal notifications|*Description of the low-level discovery rule which caused a notification.*|
|{LLDRULE.DESCRIPTION.ORIG}|→ LLD-rule based internal notifications|*Description (with macros unresolved) of the low-level discovery rule which caused a notification.*<br>Supported since 5.2.0.|
|{LLDRULE.ID}|→ LLD-rule based internal notifications|*Numeric ID of the low-level discovery rule which caused a notification.*|
|{LLDRULE.KEY}|→ LLD-rule based internal notifications|*Key of the low-level discovery rule which caused a notification.*|
|{LLDRULE.KEY.ORIG}|→ LLD-rule based internal notifications|*Original key (with macros not expanded) of the low-level discovery rule which caused a notification.*|
|{LLDRULE.NAME}|→ LLD-rule based internal notifications|*Name of the low-level discovery rule (with macros resolved) that caused a notification.*|
|{LLDRULE.NAME.ORIG}|→ LLD-rule based internal notifications|*Original name (i.e. without macros resolved) of the low-level discovery rule that caused a notification.*|
|{LLDRULE.STATE}|→ LLD-rule based internal notifications|*The latest state of the low-level discovery rule.* Possible values: **Not supported** and **Normal**.|
|{LLDRULE.STATE.ERROR}|→ LLD-rule based internal notifications|*Error message with details why an LLD rule became unsupported.* <br> <br> If an LLD rule goes into the unsupported state and then immediately gets supported again the error field can be empty.|

[comment]: # ({/new-d60e314e})

[comment]: # ({new-fff4e71a})

### Maps

|Macro|Supported in|Description|
|--|--------|--------|
|{MAP.ID}|→ Map element labels, map URL names and values|*Network map ID.*|
|{MAP.NAME}|→ Map element labels, map URL names and values<br>→ Text field in map shapes|*Network map name.*<br>Supported since 3.4.0.|

[comment]: # ({/new-fff4e71a})

[comment]: # ({new-c0547f53})

### Proxies

|Macro|Supported in|Description|
|--|--------|--------|
|{PROXY.DESCRIPTION}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Discovery notifications and commands<br>→ Autoregistration notifications and commands<br>→ Internal notifications<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)|*Description of the proxy*. Resolves to either:<br>1) proxy of the Nth item in the trigger expression (in trigger-based notifications). You may use [indexed](/manual/appendix/macros/supported_by_location#indexed_macros) macros here.<br>2) proxy, which executed discovery (in discovery notifications). Use {PROXY.DESCRIPTION} here, without indexing.<br>3) proxy to which an active agent registered (in autoregistration notifications). Use {PROXY.DESCRIPTION} here, without indexing.<br><br>This macro may be used with a numeric index e.g. {PROXY.DESCRIPTION[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|
|{PROXY.NAME}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Discovery notifications and commands<br>→ Autoregistration notifications and commands<br>→ Internal notifications<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)|*Name of the proxy*. Resolves to either:<br>1) proxy of the Nth item in the trigger expression (in trigger-based notifications). You may use [indexed](/manual/appendix/macros/supported_by_location#indexed_macros) macros here.<br>2) proxy, which executed discovery (in discovery notifications). Use {PROXY.NAME} here, without indexing.<br>3) proxy to which an active agent registered (in autoregistration notifications). Use {PROXY.NAME} here, without indexing.<br><br>This macro may be used with a numeric index e.g. {PROXY.NAME[<1-9>](supported_by_location#indexed_macros)} to point to the first, second, third, etc. host in a trigger expression. See [indexed macros](supported_by_location#indexed_macros).|

[comment]: # ({/new-c0547f53})

[comment]: # ({new-35c7fe9d})

### Services

|Macro|Supported in|Description|
|--|--------|--------|
|{SERVICE.DESCRIPTION}|→ Service-based notifications and commands<br>→ Service update notifications and commands|*Description of the service* (with macros resolved).|
|{SERVICE.NAME}|→ Service-based notifications and commands<br>→ Service update notifications and commands|*Name of the service* (with macros resolved).|
|{SERVICE.ROOTCAUSE}|→ Service-based notifications and commands<br>→ Service update notifications and commands|*List of trigger problem events that caused a service to fail*, sorted by severity and host name. Includes the following details: host name, event name, severity, age, service tags and values.|
|{SERVICE.TAGS}|→ Service-based notifications and commands<br>→ Service update notifications and commands|*A comma separated list of service event tags.* Service event tags can be defined in the service configuration section Tags. Expanded to an empty string if no tags exist.|
|{SERVICE.TAGSJSON}|→ Service-based notifications and commands<br>→ Service update notifications and commands|*A JSON array containing service event tag objects.* Service event tags can be defined in the service configuration section Tags. Expanded to an empty array if no tags exist.|
|{SERVICE.TAGS.<tag name>}|→ Service-based notifications and commands<br>→ Service update notifications and commands|*Service event tag value referenced by the tag name.* Service event tags can be defined in the service configuration section Tags.<br>A tag name containing non-alphanumeric characters (including non-English multibyte-UTF characters) should be double quoted. Quotes and backslashes inside a quoted tag name must be escaped with a backslash.|

[comment]: # ({/new-35c7fe9d})

[comment]: # ({new-f378e3e7})

### Triggers

|Macro|Supported in|Description|
|--|--------|--------|
|{TRIGGER.DESCRIPTION}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Trigger-based internal notifications<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)|*Trigger description.*<br>All macros supported in a trigger description will be expanded if `{TRIGGER.DESCRIPTION}` is used in notification text.<br>`{TRIGGER.COMMENT}` is deprecated.|
|{TRIGGER.EXPRESSION.EXPLAIN}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)<br>→ Event names|*Partially evaluated trigger expression.*<br>Item-based functions are evaluated and replaced by the results at the time of event generation whereas all other functions are displayed as written in the expression. Can be used for debugging trigger expressions.|
|{TRIGGER.EXPRESSION.RECOVERY.EXPLAIN}|→ Problem [recovery notifications](/manual/config/notifications/action/recovery_operations) and commands<br>→ Problem update notifications and commands<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)|*Partially evaluated trigger recovery expression.*<br>Item-based functions are evaluated and replaced by the results at the time of event generation whereas all other functions are displayed as written in the expression. Can be used for debugging trigger recovery expressions.|
|{TRIGGER.EVENTS.ACK}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Map element labels<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)|*Number of acknowledged events for a map element in maps, or for the trigger which generated current event in notifications.*|
|{TRIGGER.EVENTS.PROBLEM.ACK}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Map element labels<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)|*Number of acknowledged PROBLEM events for all triggers disregarding their state.*|
|{TRIGGER.EVENTS.PROBLEM.UNACK}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Map element labels<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)|*Number of unacknowledged PROBLEM events for all triggers disregarding their state.*|
|{TRIGGER.EVENTS.UNACK}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Map element labels<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)|*Number of unacknowledged events for a map element in maps, or for the trigger which generated current event in notifications.*|
|{TRIGGER.HOSTGROUP.NAME}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Trigger-based internal notifications<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)|*A sorted (by SQL query), comma-space separated list of host groups in which the trigger is defined.*|
|{TRIGGER.PROBLEM.EVENTS.PROBLEM.ACK}|→ Map element labels|*Number of acknowledged PROBLEM events for triggers in PROBLEM state.*|
|{TRIGGER.PROBLEM.EVENTS.PROBLEM.UNACK}|→ Map element labels|*Number of unacknowledged PROBLEM events for triggers in PROBLEM state.*|
|{TRIGGER.EXPRESSION}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Trigger-based internal notifications<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)|*Trigger expression.*|
|{TRIGGER.EXPRESSION.RECOVERY}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Trigger-based internal notifications<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)|*Trigger recovery expression* if *OK event generation* in [trigger configuration](/manual/config/triggers/trigger) is set to 'Recovery expression'; otherwise an empty string is returned.<br>Supported since 3.2.0.|
|{TRIGGER.ID}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Trigger-based internal notifications<br>→ Map element labels, map URL names and values<br>→ Trigger URLs<br>→ Trigger tag value<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)|*Numeric trigger ID which triggered this action.*<br>Supported in trigger tag values since 4.4.1.|
|{TRIGGER.NAME}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Trigger-based internal notifications<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)|*Name of the trigger* (with macros resolved).<br>Note that since 4.0.0 {EVENT.NAME} can be used in actions to display the triggered event/problem name with macros resolved.|
|{TRIGGER.NAME.ORIG}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Trigger-based internal notifications<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)|*Original name of the trigger* (i.e. without macros resolved).|
|{TRIGGER.NSEVERITY}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Trigger-based internal notifications<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)|*Numerical trigger severity.* Possible values: 0 - Not classified, 1 - Information, 2 - Warning, 3 - Average, 4 - High, 5 - Disaster.|
|{TRIGGER.SEVERITY}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Trigger-based internal notifications<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)|*Trigger severity name.* Can be defined in *Administration → General → Trigger displaying options*.|
|{TRIGGER.STATE}|→ Trigger-based internal notifications|*The latest state of the trigger.* Possible values: **Unknown** and **Normal**.|
|{TRIGGER.STATE.ERROR}|→ Trigger-based internal notifications|*Error message with details why a trigger became unsupported.* <br> <br> If a trigger goes into the unsupported state and then immediately gets supported again the error field can be empty.|
|{TRIGGER.STATUS}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)|*Trigger value at the time of operation step execution.* Can be either PROBLEM or OK.<br>`{STATUS}` is deprecated.|
|{TRIGGER.TEMPLATE.NAME}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Trigger-based internal notifications<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)|*A sorted (by SQL query), comma-space separated list of templates in which the trigger is defined, or \*UNKNOWN\* if the trigger is defined in a host.*|
|{TRIGGER.URL}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Trigger-based internal notifications<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)|*Trigger URL.*|
|{TRIGGER.VALUE}|→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Trigger expressions<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts)|*Current trigger numeric value*: 0 - trigger is in OK state, 1 - trigger is in PROBLEM state.|
|{TRIGGERS.UNACK}|→ Map element labels|*Number of unacknowledged triggers for a map element, disregarding trigger state.*<br>A trigger is considered to be unacknowledged if at least one of its PROBLEM events is unacknowledged.|
|{TRIGGERS.PROBLEM.UNACK}|→ Map element labels|*Number of unacknowledged PROBLEM triggers for a map element.*<br>A trigger is considered to be unacknowledged if at least one of its PROBLEM events is unacknowledged.|
|{TRIGGERS.ACK}|→ Map element labels|*Number of acknowledged triggers for a map element, disregarding trigger state.*<br>A trigger is considered to be acknowledged if all of it's PROBLEM events are acknowledged.|
|{TRIGGERS.PROBLEM.ACK}|→ Map element labels|*Number of acknowledged PROBLEM triggers for a map element.*<br>A trigger is considered to be acknowledged if all of it's PROBLEM events are acknowledged.|

[comment]: # ({/new-f378e3e7})

[comment]: # ({new-3b1a947a})

### Users

|Macro|Supported in|Description|
|--|--------|--------|
|{USER.FULLNAME}|→ Problem update notifications and commands<br>→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts) (including confirmation text)<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts) (including confirmation text)|*Name, surname and username of the user* who added event acknowledgment or started the script.<br>Supported for problem updates since 3.4.0, for global scripts since 5.0.2|
|{USER.NAME}|→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts) (including confirmation text)<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts) (including confirmation text)|*Name of the user* who started the script.<br>Supported since 5.0.2.|
|{USER.SURNAME}|→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts) (including confirmation text)<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts) (including confirmation text)|*Surname of the user* who started the script.<br>Supported since 5.0.2.|
|{USER.USERNAME}|→ Manual host action [scripts](/manual/web_interface/frontend_sections/alerts/scripts) (including confirmation text)<br>→ Manual event action [scripts](/manual/web_interface/frontend_sections/alerts/scripts) (including confirmation text)|*Username of the user* who started the script.<br>Supported since 5.0.2.<br>{USER.ALIAS}, supported before Zabbix 5.4.0, is now deprecated.|

[comment]: # ({/new-3b1a947a})

[comment]: # ({new-08004baa})

### Other macro types

|Macro|Supported in|Description|
|--|--------|--------|
|{$MACRO}|→ See: [User macros supported by location](/manual/appendix/macros/supported_by_location_user)|*[User-definable](/manual/config/macros/user_macros) macros.*|
|{\#MACRO}|→ See: [Low-level discovery macros](/manual/config/macros/lld_macros)|*Low-level discovery macros.*<br><br>[Customizing](/manual/config/macros/macro_functions) the macro value is supported for this macro, starting with Zabbix 4.0.0.|
|{?EXPRESSION}|→ Trigger event names<br>→ Trigger-based notifications and commands<br>→ Problem update notifications and commands<br>→ Map element labels^[3](supported_by_location#footnotes)^<br>→ Map shape labels^[3](supported_by_location#footnotes)^<br>→ Link labels in maps^[3](supported_by_location#footnotes)^<br>→ Graph names^[5](supported_by_location#footnotes)^|See [expression macros](/manual/config/macros/expression_macros).<br>Supported since 5.2.0.|

[comment]: # ({/new-08004baa})

[comment]: # ({new-f4bb15e3})
#### 概述

[comment]: # ({/new-f4bb15e3})

[comment]: # ({new-3a90dc8f})
#### Overview

下表包含Zabbix支持宏的完整列表。 The table contains a complete list of
macros supported by Zabbix.

|宏|持场景       描述信息|<|
|---|----------------------------|-|
|Macro|Supported in|Description|

|   |   |   |
|---|---|---|
|{ACTION.ID}|→ 基于Trigger的通知和命令\                    *action→ 发现通知\                                   从2.→ 自动注册通知<br>→ 内部通知<br>→ 故障更新通知|数字标识。*<br>.0开始支持。|
|{ACTION.ID}|→ Trigger-based notifications and commands<br>→ Discovery notifications<br>→ Auto-registration notifications<br>→ Internal notifications<br>→ Problem update notifications|*Numeric ID of the triggered action.*<br>Supported since 2.2.0.|

|   |   |   |
|---|---|---|
|{ACTION.NAME}|→ 基于Trigger的通知和命令\                    *action→ 发现通知\                                   从2.→ 自动注册通知<br>→ 内部通知<br>→ 故障更新通知|名称。*<br>.0开始支持。|
|{ACTION.NAME}|→ Trigger-based notifications and commands<br>→ Discovery notifications<br>→ Auto-registration notifications<br>→ Internal notifications<br>→ Problem update notifications|*Name of the triggered action.*<br>Supported since 2.2.0.|

|   |   |   |
|---|---|---|
|{ALERT.MESSAGE}|→ 报警脚本参数              *默认值由|ction配置。*<br>从3.0.0开始支持。|
|{ALERT.MESSAGE}|→ Alert script parameters|*'Default message' value from action configuration.*<br>Supported since 3.0.0.|

|   |   |   |
|---|---|---|
|{ALERT.SENDTO}|→ 报警脚本参数              *值来自于|户报警媒介配置。*<br>从3.0.0开始支持。|
|{ALERT.SENDTO}|→ Alert script parameters|*'Send to' value from user media configuration.*<br>Supported since 3.0.0.|

|   |   |   |
|---|---|---|
|{ALERT.SUBJECT}|→ 报警脚本参数              *默认值由|ction配置。*<br>从3.0.0开始支持。|
|{ALERT.SUBJECT}|→ Alert script parameters|*'Default subject' value from action configuration.*<br>Supported since 3.0.0.|

|   |   |   |
|---|---|---|
|{DATE}|→ 基于Trigger的通知和命令\                    *当前时间使用→ 发现通知<br>→ 自动注册通知<br>→ 内部通知<br>→ 故障更新通知|yyyy.mm.dd格式。*|
|{DATE}|→ Trigger-based notifications and commands<br>→ Discovery notifications<br>→ Auto-registration notifications<br>→ Internal notifications<br>→ Problem update notifications|*Current date in yyyy.mm.dd. format.*|

|   |   |   |
|---|---|---|
|{DISCOVERY.DEVICE.IPADDRESS}|→ 发现通知                  *被发|设备的IP地址。*<br>不依赖于是否添加设备。|
|{DISCOVERY.DEVICE.IPADDRESS}|→ Discovery notifications|*IP address of the discovered device.*<br>Available always, does not depend on host being added.|

|   |   |   |
|---|---|---|
|{DISCOVERY.DEVICE.DNS}|→ 发现通知                  *被发|设备的DNS名称。*<br>不依赖于是否添加设备。|
|{DISCOVERY.DEVICE.DNS}|→ Discovery notifications|*DNS name of the discovered device.*<br>Available always, does not depend on host being added.|

|   |   |   |
|---|---|---|
|{DISCOVERY.DEVICE.STATUS}|→ 发现通知                  *被发|设备的状态。*: 可能是UP 或 DOWN.|
|{DISCOVERY.DEVICE.STATUS}|→ Discovery notifications|*Status of the discovered device*: can be either UP or DOWN.|

|   |   |   |
|---|---|---|
|{DISCOVERY.DEVICE.UPTIME}|→ 发现通知   //距|定设备最近一次发现状态改变的时间。 *<br>例如: 1h 29m.\\\\对于状态为DOWN的设备，这是其停机时间。 \| \|{DISCOVERY.DEVICE.UPTIME} \|→ Discovery notifications \|*Time since the last change of discovery status for a particular device.//<br>For example: 1h 29m.<br>For devices with status DOWN, this is the period of their downtime.|

|   |   |   |
|---|---|---|
|{DISCOVERY.RULE.NAME}|→ 发现通知                  *发现|备或服务是否存在的发现规则名称。*|
|{DISCOVERY.RULE.NAME}|→ Discovery notifications|*Name of the discovery rule that discovered the presence or absence of the device or service.*|

|   |   |   |
|---|---|---|
|{DISCOVERY.SERVICE.NAME}|→ 发现通知                  *被发|服务的名称。*\\\\例如: HTTP。|
|{DISCOVERY.SERVICE.NAME}|→ Discovery notifications|*Name of the service that was discovered.*<br>For example: HTTP.|

|   |   |   |
|---|---|---|
|{DISCOVERY.SERVICE.PORT}|→ 发现通知                  *被发|服务的端口。*<br>例如: 80。|
|{DISCOVERY.SERVICE.PORT}|→ Discovery notifications|*Port of the service that was discovered.*<br>For example: 80.|

|   |   |   |
|---|---|---|
|{DISCOVERY.SERVICE.STATUS}|→ 发现通知                  *被发|服务的状态。* 可能是UP 或 DOWN。|
|{DISCOVERY.SERVICE.STATUS}|→ Discovery notifications|//Status of the discovered <service://> can be either UP or DOWN.|

|   |   |   |
|---|---|---|
|{DISCOVERY.SERVICE.UPTIME}|→ 发现通知   //距|定服务最近一次发现状态改变的时间。 *<br>例如: 1h 29m.\\\\对于状态为DOWN的服务，这是其停服时间。\| \|{DISCOVERY.SERVICE.UPTIME} \|→ Discovery notifications \|*Time since the last change of discovery status for a particular service.//<br>For example: 1h 29m.<br>For services with status DOWN, this is the period of their downtime.|

|   |   |   |
|---|---|---|
|{ESC.HISTORY}|→ 基于Trigger的通知和命令\                    *记录以前发送→ 内部通知\                                   显示先→ 故障更新通知|息的日志。*<br>在升级步骤中发送的通知信息，且发送通知状态为： (*已发送*, *正在发送* 或 *发送失败*).|
|{ESC.HISTORY}|→ Trigger-based notifications and commands<br>→ Internal notifications<br>→ Problem update notifications|*Escalation history. Log of previously sent messages.*<br>Shows previously sent notifications, on which escalation step they were sent and their status (*sent*, *in progress* or *failed*).|

|   |   |   |
|---|---|---|
|{EVENT.ACK.STATUS}|→ 基于Trigger的通知和命令\                    *事件的确认状→ 故障更新通知|。(Yes/No)*.|
|{EVENT.ACK.STATUS}|→ Trigger-based notifications and commands<br>→ Problem update notifications|*Acknowledgement status of the event (Yes/No)*.|

|   |   |   |
|---|---|---|
|{EVENT.AGE}|→ 基于Trigger的通知和命令\                    *触发动作的事→ 发现通知\                                   对逐步→ 自动注册通知<br>→ 内部通知<br>→ 故障更新通知|持续时间。*<br>级的消息非常有用。|
|{EVENT.AGE}|→ Trigger-based notifications and commands<br>→ Discovery notifications<br>→ Auto-registration notifications<br>→ Internal notifications<br>→ Problem update notifications|*Age of the event that triggered an action.*<br>Useful in escalated messages.|

|   |   |   |
|---|---|---|
|{EVENT.DATE}|→ 基于Trigger的通知和命令\                    *触发动作的事→ 发现通知<br>→ 自动注册通知<br>→ 内部通知<br>→ 故障更新通知|发生日期。*|
|{EVENT.DATE}|→ Trigger-based notifications and commands<br>→ Discovery notifications<br>→ Auto-registration notifications<br>→ Internal notifications<br>→ Problem update notifications|*Date of the event that triggered an action.*|

|   |   |   |
|---|---|---|
|{EVENT.ID}|→ 基于Trigger的通知和命令\                    *触发动作的事→ 发现通知<br>→ 自动注册通知<br>→ 内部通知<br>→ 故障更新通知|数字标识。*|
|{EVENT.ID}|→ Trigger-based notifications and commands<br>→ Discovery notifications<br>→ Auto-registration notifications<br>→ Internal notifications<br>→ Problem update notifications|*Numeric ID of the event that triggered an action.*|

|   |   |   |
|---|---|---|
|{EVENT.NAME}|→ 基于Trigger的通知和命令\        *触发动作的故→ 故障更新通知                    从4.0.|或恢复事件的名字。*<br>开始支持。|
|{EVENT.NAME}|→ Trigger-based notifications<br>→ Problem update notifications|*Name of the problem/recovery event that triggered an action.*<br>Supported since 4.0.0.|

|   |   |   |
|---|---|---|
|{EVENT.NSEVERITY}|→ 基于Trigger的通知和命令\        *事件的级别。→ 故障更新通知                    从4.0.|可能的值: 0 - 未知, 1 - 信息, 2 - 警告, 3 - 普通, 4 - 高, 5 - 灾难。<br>开始支持。|
|{EVENT.NSEVERITY}|→ Trigger-based notifications<br>→ Problem update notifications|*Numeric value of the event severity.* Possible values: 0 - Not classified, 1 - Information, 2 - Warning, 3 - Average, 4 - High, 5 - Disaster.<br>Supported since 4.0.0.|

|   |   |   |
|---|---|---|
|{EVENT.RECOVERY.DATE}|→ 基于Trigger的通知和命令\       *恢复事件的发→ 内部通知\                      只能用→ 故障更新通知                   从2.2.|时间。*<br>[恢复](/manual/config/notifications/action#configuring_an_action)消息。<br>开始支持。|
|{EVENT.RECOVERY.DATE}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications|*Date of the recovery event.*<br>Can be used in [recovery](/manual/config/notifications/action#configuring_an_action) messages only.<br>Supported since 2.2.0.|

|   |   |   |
|---|---|---|
|{EVENT.RECOVERY.ID}|→ 基于Trigger的通知和命令\       *恢复事件的数→ 内部通知\                      只能用→ 故障更新通知|标识。*<br>[恢复](/manual/config/notifications/action#configuring_an_action)消息。 从2.2.0开始支持。|
|{EVENT.RECOVERY.ID}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications|*Numeric ID of the recovery event.*<br>Can be used in [recovery](/manual/config/notifications/action#configuring_an_action) messages only. Supported since 2.2.0.|

|   |   |   |
|---|---|---|
|{EVENT.RECOVERY.STATUS}|→ 基于Trigger的通知和命令\       *恢复事件的状→ 内部通知\                      只能用→ 故障更新通知|。*<br>[恢复](/manual/config/notifications/action#configuring_an_action)消息。 从2.2.0开始支持。|
|{EVENT.RECOVERY.STATUS}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications|*Verbal value of the recovery event.*<br>Can be used in [recovery](/manual/config/notifications/action#configuring_an_action) messages only. Supported since 2.2.0.|

|   |   |   |
|---|---|---|
|{EVENT.RECOVERY.TAGS}|→ 基于Trigger的通知和命令\                    *逗号分隔的恢→ 故障更新通知                                从3.2.|事件tag列表。*如果不存在tag，则为空字符串。<br>开始支持。|
|{EVENT.RECOVERY.TAGS}|→ Trigger-based notifications and commands<br>→ Problem update notifications|*A comma separated list of recovery event tags.* Expanded to an empty string if no tags exist.<br>Supported since 3.2.0.|

|   |   |   |
|---|---|---|
|{EVENT.RECOVERY.TIME}|→ 基于Trigger的通知和命令\       *恢复事件的时→ 内部通知\                      只能用→ 故障更新通知|。*<br>[恢复](/manual/config/notifications/action#configuring_an_action)消息。 从2.2.0开始支持。|
|{EVENT.RECOVERY.TIME}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications|*Time of the recovery event.*<br>Can be used in [recovery](/manual/config/notifications/action#configuring_an_action) messages only.<br>Supported since 2.2.0.|

|   |   |   |
|---|---|---|
|{EVENT.RECOVERY.VALUE}|→ 基于Trigger的通知和命令\       *恢复事件的数→ 内部通知\                      只能用→ 故障更新通知|值。*<br>[恢复](/manual/config/notifications/action#configuring_an_action)消息。 从2.2.0开始支持。|
|{EVENT.RECOVERY.VALUE}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications|*Numeric value of the recovery event.*<br>Can be used in [recovery](/manual/config/notifications/action#configuring_an_action) messages only.<br>Supported since 2.2.0.|

|   |   |   |
|---|---|---|
|{EVENT.SEVERITY}|→ 基于Trigger的通知和命令\        *事件的级别。→ 故障更新通知                    从4.0.|<br>开始支持。|
|{EVENT.SEVERITY}|→ Trigger-based notifications<br>→ Problem update notifications|*Name of the event severity.*<br>Supported since 4.0.0.|

|   |   |   |
|---|---|---|
|{EVENT.STATUS}|→ 基于Trigger的通知和命令\                    *触发动作的事→ 发现通知\                                   从2.→ 自动注册通知<br>→ 内部通知<br>→ 故障更新通知|状态。*<br>.0开始支持。|
|{EVENT.STATUS}|→ Trigger-based notifications and commands<br>→ Discovery notifications<br>→ Auto-registration notifications<br>→ Internal notifications<br>→ Problem update notifications|*Verbal value of the event that triggered an action.*<br>Supported since 2.2.0.|

|   |   |   |
|---|---|---|
|{EVENT.TAGS}|→ 基于Trigger的通知和命令\                    *用逗号分隔的→ 故障更新通知                                从3.2.|件tag列表。* 如果不存在tag，则为空字符串。<br>开始支持。|
|{EVENT.TAGS}|→ Trigger-based notifications and commands<br>→ Problem update notifications|*A comma separated list of event tags.* Expanded to an empty string if no tags exist.<br>Supported since 3.2.0.|

|   |   |   |
|---|---|---|
|{EVENT.TIME}|→ 基于Trigger的通知和命令\                    *触发动作的事→ 发现通知<br>→ 自动注册通知<br>→ 内部通知<br>→ 故障更新通知notifications|时间。*|
|{EVENT.TIME}|→ Trigger-based notifications and commands<br>→ Discovery notifications<br>→ Auto-registration notifications<br>→ Internal notifications<br>→ Problem update notifications|*Time of the event that triggered an action.*|

|   |   |   |
|---|---|---|
|{EVENT.UPDATE.ACTION}|→ 故障更新通知                   *可读的操|名称。* [故障更新](/manual/acknowledges#updating_problems)时执行。<br>解析为以下值: *acknowledged*, *commented*, *changed severity from (original severity) to (updated severity)* and *closed* (依赖于一次更新操作执行多少个动作).<br>从4.0.0开始支持。|
|{EVENT.UPDATE.ACTION}|→ Problem update notifications|*Human-readable name of the action(s)* performed during [problem update](/manual/acknowledges#updating_problems).<br>Resolves to the following values: *acknowledged*, *commented*, *changed severity from (original severity) to (updated severity)* and *closed* (depending on how many actions are performed in one update).<br>Supported since 4.0.0.|

|   |   |   |
|---|---|---|
|{EVENT.UPDATE.DATE}|→ 故障更新通知                   *故障更新|间。 (确认, 等)。*<br>取代以前的宏: {ACK.DATE}|
|{EVENT.UPDATE.DATE}|→ Problem update notifications|*Date of problem update (acknowledgement, etc).*<br>Deprecated name: {ACK.DATE}|

|   |   |   |
|---|---|---|
|{EVENT.UPDATE.HISTORY}|→ 基于Trigger的通知和命令\                    *记录故障更新→故障更新通知                                 取代以前的|志。(确认, 等).*<br>: {EVENT.ACK.HISTORY}|
|{EVENT.UPDATE.HISTORY}|→ Trigger-based notifications and commands<br>→ Problem update notifications|*Log of problem updates (acknowledgements, etc).*<br>Deprecated name: {EVENT.ACK.HISTORY}|

|   |   |   |
|---|---|---|
|{EVENT.UPDATE.MESSAGE}|→ 故障更新通知                   *故障更新|息。*<br>取代以前的宏: {ACK.MESSAGE}|
|{EVENT.UPDATE.MESSAGE}|→ Problem update notifications|*Problem update message.*<br>Deprecated name: {ACK.MESSAGE}|

|   |   |   |
|---|---|---|
|{EVENT.UPDATE.TIME}|→ 故障更新通知                   *故障更新|间。 (确认, 等).*<br>取代以前的宏: {ACK.TIME}|
|{EVENT.UPDATE.TIME}|→ Problem update notifications|*Time of problem update (acknowledgement, etc).*<br>Deprecated name: {ACK.TIME}|

|   |   |   |
|---|---|---|
|{EVENT.VALUE}|→ 基于Trigger的通知和命令\                    *触发动作的事→ 发现通知\                                   从2.→ 自动注册通知<br>→ 内部通知<br>→ 故障更新通知|类型 (1 为故障, 0 为恢复)。*<br>.0开始支持。|
|{EVENT.VALUE}|→ Trigger-based notifications and commands<br>→ Discovery notifications<br>→ Auto-registration notifications<br>→ Internal notifications<br>→ Problem update notifications|*Numeric value of the event that triggered an action (1 for problem, 0 for recovering).*<br>Supported since 2.2.0.|

|   |   |   |
|---|---|---|
|{HOST.CONN[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知和命令\                                                                          *设备IP或D→ 内部通知\                                                                                         从2.→ 故障更新通知<br>→ 全局脚本 (包括确认文本)<br>→ 地图中的Icon标签^[1](supported_by_location#footnotes)^<br>→ Item key 值^[2](supported_by_location#footnotes)^<br>→ 设备接口IP/DNS<br>→ 数据库监控附加字段^[5](supported_by_location#footnotes)^<br>→ SSH和Telnet脚本^[5](supported_by_location#footnotes)^<br>→ JMX item endpoint 字段<br>→ Web监控^[6](supported_by_location#footnotes)^<br>→ Low-level发现规则过滤正则表达式^[8](supported_by_location#footnotes)^<br>→ 动态URL仪表板小部件/屏幕元素的URL字段^[8](supported_by_location#footnotes)^<br>→ Trigger名字和描述<br>→ Trigger URLs^[10](supported_by_location#footnotes)^<br>→ 事件tag的名称和值<br>→ HTTP agent的item类型, item原型和发现规则字段:<br>URL, query fields, request body, headers, proxy, SSL certificate file, SSL key file.|S名称, 依赖于设备配置。*^[3](supported_by_location#footnotes)^.<br>.0开始支持。|
|{HOST.CONN[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications and commands<br>→ Internal notifications<br>→ Problem update notifications<br>→ Global scripts (including confirmation text)<br>→ Icon labels in maps^[1](supported_by_location#footnotes)^<br>→ Item key parameters^[2](supported_by_location#footnotes)^<br>→ Host interface IP/DNS<br>→ Database monitoring additional parameters^[5](supported_by_location#footnotes)^<br>→ SSH and Telnet scripts^[5](supported_by_location#footnotes)^<br>→ JMX item endpoint field<br>→ Web monitoring^[6](supported_by_location#footnotes)^<br>→ Low-level discovery rule filter regular expressions^[8](supported_by_location#footnotes)^<br>→ URL field of dynamic URL dashboard widget/screen element^[8](supported_by_location#footnotes)^<br>→ Trigger names and descriptions<br>→ Trigger URLs^[10](supported_by_location#footnotes)^<br>→ Event tags and values<br>→ HTTP agent type item, item prototype and discovery rule fields:<br>URL, query fields, request body, headers, proxy, SSL certificate file, SSL key file.|*Host IP address or DNS name, depending on host settings*^[3](supported_by_location#footnotes)^.<br>Supported in trigger names since 2.0.0.|

|   |   |   |
|---|---|---|
|{HOST.DESCRIPTION[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知和命令\                                    *设备描述。*→ 内部通知\                                                   从2.→ 故障更新通知<br>→ 地图中的Icon标签^[1](supported_by_location#footnotes)^|<.0开始支持。|
|{HOST.DESCRIPTION[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications and commands<br>→ Internal notifications<br>→ Problem update notifications<br>→ Icon labels in maps^[1](supported_by_location#footnotes)^|*Host description.*<br>Supported since 2.4.0.|

|   |   |   |
|---|---|---|
|{HOST.DNS[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知和命令\                                                                          *设备DNS名→ 内部通知\                                                                                         tri→ 故障更新通知<br>→ 全局脚本 (包括确认文本)<br>→ 地图中的Icon标签^[1](supported_by_location#footnotes)^<br>→ Item key 值^[2](supported_by_location#footnotes)^<br>→ 设备接口IP/DNS<br>→ 数据库监控附加字段^[5](supported_by_location#footnotes)^<br>→ SSH和Telnet脚本^[5](supported_by_location#footnotes)^<br>→ JMX item endpoint 字段<br>→ Web监控^[6](supported_by_location#footnotes)^<br>→ Low-level发现规则过滤正则表达式^[8](supported_by_location#footnotes)^<br>→ 动态URL仪表板小部件/屏幕元素的URL字段^[8](supported_by_location#footnotes)^<br>→ Trigger名字和描述<br>→ Trigger URLs^[10](supported_by_location#footnotes)^<br>→ 事件tag的名称和值<br>→ HTTP agent的item类型, item原型和发现规则字段:<br>URL, query fields, request body, headers, proxy, SSL certificate file, SSL key file.|*^[3](supported_by_location#footnotes)^.<br>ger名字从2.0.0开始支持。|
|{HOST.DNS[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications and commands<br>→ Internal notifications<br>→ Problem update notifications<br>→ Global scripts (including confirmation text)<br>→ Icon labels in maps^[1](supported_by_location#footnotes)^<br>→ Item key parameters^[2](supported_by_location#footnotes)^<br>→ Host interface IP/DNS<br>→ Database monitoring additional parameters^[5](supported_by_location#footnotes)^<br>→ SSH and Telnet scripts^[5](supported_by_location#footnotes)^<br>→ JMX item endpoint field<br>→ Web monitoring^[6](supported_by_location#footnotes)^<br>→ Low-level discovery rule filter regular expressions^[8](supported_by_location#footnotes)^<br>→ URL field of dynamic URL dashboard widget/screen element^[8](supported_by_location#footnotes)^<br>→ Trigger names and descriptions<br>→ Trigger URLs^[10](supported_by_location#footnotes)^<br>→ Event tags and values<br>→ HTTP agent type item, item prototype and discovery rule fields:<br>URL, query fields, request body, headers, proxy, SSL certificate file, SSL key file.|*Host DNS name*^[3](supported_by_location#footnotes)^.<br>Supported in trigger names since 2.0.0.|

|   |   |   |
|---|---|---|
|{HOST.HOST[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知和命令\                                                                          *设备名称。*→ 自动注册通知\                                                                                     `{HOS→ 内部通知<br>→ 故障更新通知<br>→ 全局脚本 (包括确认文本)<br>→ Item key 值<br>→ 地图中的Icon标签^[1](supported_by_location#footnotes)^<br>→ 设备接口IP/DNS<br>→ 数据库监控附加字段^[5](supported_by_location#footnotes)^<br>→ SSH和Telnet脚本 ^[5](supported_by_location#footnotes)^<br>→ JMX item endpoint 字段<br>→ Web监控^[6](supported_by_location#footnotes)^<br>→ Low-level发现规则过滤正则表达式^[8](supported_by_location#footnotes)^<br>→ 动态URL仪表板小部件/屏幕元素的URL字段^[8](supported_by_location#footnotes)^<br>→ Trigger名字和描述<br>→ Trigger URLs ^[10](supported_by_location#footnotes)^<br>→ 事件tag的名称和值<br>→ HTTP agent的item类型, item原型和发现规则字段:<br>URL, query fields, request body, headers, proxy, SSL certificate file, SSL key file。|<NAME<1-9>}`已经不被支持。|
|{HOST.HOST[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications and commands<br>→ Auto registration notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Global scripts (including confirmation text)<br>→ Item key parameters<br>→ Icon labels in maps^[1](supported_by_location#footnotes)^<br>→ Host interface IP/DNS<br>→ Database monitoring additional parameters^[5](supported_by_location#footnotes)^<br>→ SSH and Telnet scripts^[5](supported_by_location#footnotes)^<br>→ JMX item endpoint field<br>→ Web monitoring^[6](supported_by_location#footnotes)^<br>→ Low-level discovery rule filter regular expressions^[8](supported_by_location#footnotes)^<br>→ URL field of dynamic URL dashboard widget/screen element^[8](supported_by_location#footnotes)^<br>→ Trigger names and descriptions<br>→ Trigger URLs ^[10](supported_by_location#footnotes)^<br>→ Event tags and values<br>→ HTTP agent type item, item prototype and discovery rule fields:<br>URL, query fields, request body, headers, proxy, SSL certificate file, SSL key file.|*Host name.*<br>`{HOSTNAME<1-9>}` is deprecated.|

|   |   |   |
|---|---|---|
|{HOST.ID[<1-9>](supported_by_location#indexed_macros)}|→ 地图中URLs\                                                                                       *设→ 动态URL仪表板小部件/屏幕元素的URL字段^[8](supported_by_location#footnotes)^<br>→ Trigger URLs^[10](supported_by_location#footnotes)^<br>→ 事件tag的名称和值|ID。*|
|{HOST.ID[<1-9>](supported_by_location#indexed_macros)}|→ Map URLs<br>→ URL field of dynamic URL dashboard widget/screen element^[8](supported_by_location#footnotes)^<br>→ Trigger URLs^[10](supported_by_location#footnotes)^<br>→ Event tags and values|*Host ID.*|

|   |   |   |
|---|---|---|
|{HOST.IP[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知和命令\                                                                          *设备IP地址→ 自动注册通知\                                                                                     从2.0.→ 内部通知<br>→ 故障更新通知<br>→ 全局脚本 (包括确认文本)<br>→ 地图中的Icon标签^[1](supported_by_location#footnotes)^<br>→ Item key值^[2](supported_by_location#footnotes)^<br>→ 设备接口IP/DNS<br>→ Database monitoring additional parameters^[5](supported_by_location#footnotes)^<br>→ SSH和Telnet脚本^[5](supported_by_location#footnotes)^<br>→ JMX item endpoint 字段<br>→ Web监控^[6](supported_by_location#footnotes)^<br>→ Low-level发现规则过滤正则表达式^[8](supported_by_location#footnotes)^<br>→ 动态URL仪表板小部件/屏幕元素的URL字段^[8](supported_by_location#footnotes)^<br>→ Trigger名字和描述<br>→ Trigger URLs^[10](supported_by_location#footnotes)^<br>→ 事件tag的名称和值<br>→ HTTP agent的item类型, item原型和发现规则字段:<br>URL, query fields, request body, headers, proxy, SSL certificate file, SSL key file.|*^[3](supported_by_location#footnotes)^.<br>开始支持。宏`{IPADDRESS<1-9>}`已经不被支持。|
|{HOST.IP[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications and commands<br>→ Auto registration notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Global scripts (including confirmation text)<br>→ Icon labels in maps^[1](supported_by_location#footnotes)^<br>→ Item key parameters^[2](supported_by_location#footnotes)^<br>→ Host interface IP/DNS<br>→ Database monitoring additional parameters^[5](supported_by_location#footnotes)^<br>→ SSH and Telnet scripts^[5](supported_by_location#footnotes)^<br>→ JMX item endpoint field<br>→ Web monitoring^[6](supported_by_location#footnotes)^<br>→ Low-level discovery rule filter regular expressions^[8](supported_by_location#footnotes)^<br>→ URL field of dynamic URL dashboard widget/screen element^[8](supported_by_location#footnotes)^<br>→ Trigger names and descriptions<br>→ Trigger URLs^[10](supported_by_location#footnotes)^<br>→ Event tags and values<br>→ HTTP agent type item, item prototype and discovery rule fields:<br>URL, query fields, request body, headers, proxy, SSL certificate file, SSL key file.|*Host IP address*^[3](supported_by_location#footnotes)^.<br>Supported since 2.0.0. `{IPADDRESS<1-9>}` is deprecated.|

|   |   |   |
|---|---|---|
|{HOST.METADATA}|→ 自动注册通知                      *设备元数|。*<br>仅仅用于主动agent的自动注册。从2.2.0开始支持。|
|{HOST.METADATA}|→ Auto registration notifications|*Host metadata.*<br>Used only for active agent auto-registration. Supported since 2.2.0.|

|   |   |   |
|---|---|---|
|{HOST.NAME[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知和命令\                                                                          *用于显示的设→ 自动注册通知\                                                                                     从2.0.→ 故障更新通知<br>→ 内部通知<br>→ 全局脚本 (包括确认文本)<br>→ 地图中的Icon标签^[1](supported_by_location#footnotes)^<br>→ Item key值<br>→ 设备接口IP/DNS<br>→ 数据库监控附加字段^[5](supported_by_location#footnotes)^<br>→ SSH和Telnet脚本^[5](supported_by_location#footnotes)^<br>→ Web监控^[6](supported_by_location#footnotes)^<br>→ Low-level 发现规则过滤正则表达式^[8](supported_by_location#footnotes)^<br>→ 动态URL仪表板小部件/屏幕元素的URL字段^[8](supported_by_location#footnotes)^<br>→ Trigger名字和描述<br>→ Trigger URLs^[10](supported_by_location#footnotes)^<br>→ 事件tag的名称和值<br>→ HTTP agent的item类型, item原型和发现规则字段:<br>URL, query fields, request body, headers, proxy, SSL certificate file, SSL key file.|名称*<br>开始支持。|
|{HOST.NAME[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications and commands<br>→ Auto registration notifications<br>→ Problem update notifications<br>→ Internal notifications<br>→ Global scripts (including confirmation text)<br>→ Icon labels in maps^[1](supported_by_location#footnotes)^<br>→ Item key parameters<br>→ Host interface IP/DNS<br>→ Database monitoring additional parameters^[5](supported_by_location#footnotes)^<br>→ SSH and Telnet scripts^[5](supported_by_location#footnotes)^<br>→ Web monitoring^[6](supported_by_location#footnotes)^<br>→ Low-level discovery rule filter regular expressions^[8](supported_by_location#footnotes)^<br>→ URL field of dynamic URL dashboard widget/screen element^[8](supported_by_location#footnotes)^<br>→ Trigger names and descriptions<br>→ Trigger URLs^[10](supported_by_location#footnotes)^<br>→ Event tags and values<br>→ HTTP agent type item, item prototype and discovery rule fields:<br>URL, query fields, request body, headers, proxy, SSL certificate file, SSL key file.|*Visible host name.*<br>Supported since 2.0.0.|

|   |   |   |
|---|---|---|
|{HOST.PORT[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知和命令\                               *设备(age→ 自动注册通知\                                          从2.0.→ 内部通知\                                              从2.→ 故障更新通知<br>→ Trigger名字和描述<br>→ Trigger URLs^[10](supported_by_location#footnotes)^<br>→ JMX item endpoint字段<br>→ 事件tag的名称和值|t)端口*^[3](supported_by_location#footnotes)^.<br>开始支持自动注册通知。<br>.2开始支持用于trigger名称和描述，内部通知，事件tag的名称和值。|
|{HOST.PORT[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications and commands<br>→ Auto registration notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Trigger names and descriptions<br>→ Trigger URLs^[10](supported_by_location#footnotes)^<br>→ JMX item endpoint field<br>→ Event tags and values|*Host (agent) port*^[3](supported_by_location#footnotes)^.<br>Supported in auto-registration since 2.0.0.<br>Supported in trigger names, trigger descriptions, internal and trigger-based notifications since 2.2.2.|

|   |   |   |
|---|---|---|
|{HOSTGROUP.ID}|→ 地图URLs   *|备组标识。*|
|{HOSTGROUP.ID}|→ Map URLs|*Host group ID.*|

|   |   |   |
|---|---|---|
|{INVENTORY.ALIAS[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知<br>→ 故障更新通知<br>→ 事件tag的名称和值|中的别名字段。*|
|{INVENTORY.ALIAS[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*Alias field in host inventory.*|

|   |   |   |
|---|---|---|
|{INVENTORY.ASSET.TAG[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知<br>→ 故障更新通知<br>→ 事件tag的名称和值|中的资产标签字段。*|
|{INVENTORY.ASSET.TAG[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*Asset tag field in host inventory.*|

|   |   |   |
|---|---|---|
|{INVENTORY.CHASSIS[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知<br>→ 故障更新通知<br>→ 事件tag的名称和值|中的机箱字段。*|
|{INVENTORY.CHASSIS[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*Chassis field in host inventory.*|

|   |   |   |
|---|---|---|
|{INVENTORY.CONTACT[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知\                       宏`{→ 故障更新通知<br>→ 事件tag的名称和值|中的联系人字段。*<br>ROFILE.CONTACT<1-9>}`已经不被支持。|
|{INVENTORY.CONTACT[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*Contact field in host inventory.*<br>`{PROFILE.CONTACT<1-9>}` is deprecated.|

|   |   |   |
|---|---|---|
|{INVENTORY.CONTRACT.NUMBER[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知<br>→ 故障更新通知<br>→ 事件tag的名称和值|中的联系号码字段。*|
|{INVENTORY.CONTRACT.NUMBER[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*Contract number field in host inventory.*|

|   |   |   |
|---|---|---|
|{INVENTORY.DEPLOYMENT.STATUS[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知<br>→ 故障更新通知<br>→ 事件tag的名称和值|中的部署状态字段。*|
|{INVENTORY.DEPLOYMENT.STATUS[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*Deployment status field in host inventory.*|

|   |   |   |
|---|---|---|
|{INVENTORY.HARDWARE[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知\                       宏`{→ 故障更新通知<br>→ 事件tag的名称和值|中的硬件信息字段。*<br>ROFILE.HARDWARE<1-9>}`已经不被支持。|
|{INVENTORY.HARDWARE[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*Hardware field in host inventory.*<br>`{PROFILE.HARDWARE<1-9>}` is deprecated.|

|   |   |   |
|---|---|---|
|{INVENTORY.HARDWARE.FULL[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知<br>→ 故障更新通知<br>→ 事件tag的名称和值|中的硬件详细描述字段。*|
|{INVENTORY.HARDWARE.FULL[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*Hardware (Full details) field in host inventory.*|

|   |   |   |
|---|---|---|
|{INVENTORY.HOST.NETMASK[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知<br>→ 故障更新通知<br>→ 事件tag的名称和值|中的子网掩码字段。*|
|{INVENTORY.HOST.NETMASK[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*Host subnet mask field in host inventory.*|

|   |   |   |
|---|---|---|
|{INVENTORY.HOST.NETWORKS[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知<br>→ 故障更新通知<br>→ 事件tag的名称和值|中的网络字段。*|
|{INVENTORY.HOST.NETWORKS[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*Host networks field in host inventory.*|

|   |   |   |
|---|---|---|
|{INVENTORY.HOST.ROUTER[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知<br>→ 故障更新通知<br>→ 事件tag的名称和值|中的路由字段。*|
|{INVENTORY.HOST.ROUTER[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*Host router field in host inventory.*|

|   |   |   |
|---|---|---|
|{INVENTORY.HW.ARCH[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知<br>→ 故障更新通知<br>→ 事件tag的名称和值|中的硬件架构字段。*|
|{INVENTORY.HW.ARCH[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*Hardware architecture field in host inventory.*|

|   |   |   |
|---|---|---|
|{INVENTORY.HW.DATE.DECOMM[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *主机清→ 内部通知<br>→ 故障更新通知<br>→ 事件tag的名称和值|中的硬件下线日期字段。*|
|{INVENTORY.HW.DATE.DECOMM[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*Date hardware decommissioned field in host inventory.*|

|   |   |   |
|---|---|---|
|{INVENTORY.HW.DATE.EXPIRY[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知<br>→ 故障更新通知<br>→ 事件tag的名称和值|中的保修期字段。*|
|{INVENTORY.HW.DATE.EXPIRY[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*Date hardware maintenance expires field in host inventory.*|

|   |   |   |
|---|---|---|
|{INVENTORY.HW.DATE.INSTALL[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知<br>→ 故障更新通知<br>→ 事件tag的名称和值|中的硬件上线日期字段。*|
|{INVENTORY.HW.DATE.INSTALL[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*Date hardware installed field in host inventory.*|

|   |   |   |
|---|---|---|
|{INVENTORY.HW.DATE.PURCHASE[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知<br>→ 故障更新通知<br>→ 事件tag的名称和值|中硬件购买时间字段。*|
|{INVENTORY.HW.DATE.PURCHASE[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*Date hardware purchased field in host inventory.*|

|   |   |   |
|---|---|---|
|{INVENTORY.INSTALLER.NAME[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知<br>→ 故障更新通知<br>→ 事件tag的名称和值|中的安装名称字段。*|
|{INVENTORY.INSTALLER.NAME[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*Installer name field in host inventory.*|

|   |   |   |
|---|---|---|
|{INVENTORY.LOCATION[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知\                       宏`{→ 故障更新通知<br>→ 事件tag的名称和值|中的位置字段。*<br>ROFILE.LOCATION<1-9>}`已经不被支持。|
|{INVENTORY.LOCATION[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*Location field in host inventory.*<br>`{PROFILE.LOCATION<1-9>}` is deprecated.|

|   |   |   |
|---|---|---|
|{INVENTORY.LOCATION.LAT[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知<br>→ 故障更新通知<br>→ 事件tag的名称和值|中的位置纬度字段。*|
|{INVENTORY.LOCATION.LAT[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*Location latitude field in host inventory.*|

|   |   |   |
|---|---|---|
|{INVENTORY.LOCATION.LON[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知<br>→ 故障更新通知<br>→ 事件tag的名称和值|中的位置经度字段。*|
|{INVENTORY.LOCATION.LON[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*Location longitude field in host inventory.*|

|   |   |   |
|---|---|---|
|{INVENTORY.MACADDRESS.A[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知\                       宏`{→ 故障更新通知<br>→ 事件tag的名称和值|中的MAC地址字段。*<br>ROFILE.MACADDRESS<1-9>}`已经不被支持。|
|{INVENTORY.MACADDRESS.A[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*MAC address A field in host inventory.*<br>`{PROFILE.MACADDRESS<1-9>}` is deprecated.|

|   |   |   |
|---|---|---|
|{INVENTORY.MACADDRESS.B[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知<br>→ 故障更新通知<br>→ 事件tag的名称和值|中的MAC地址字段。*|
|{INVENTORY.MACADDRESS.B[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*MAC address B field in host inventory.*|

|   |   |   |
|---|---|---|
|{INVENTORY.MODEL[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知<br>→ 故障更新通知<br>→ 事件tag的名称和值|中的模型字段。*|
|{INVENTORY.MODEL[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*Model field in host inventory.*|

|   |   |   |
|---|---|---|
|{INVENTORY.NAME[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知<br>→ 故障更新通知<br>→ 事件tag的名称和值|中的名称字段。* 宏`{PROFILE.NAME<1-9>}`已经不被支持。|
|{INVENTORY.NAME[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*Name field in host inventory.* `{PROFILE.NAME<1-9>}` is deprecated.|

|   |   |   |
|---|---|---|
|{INVENTORY.NOTES[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知<br>→ 故障更新通知<br>→ 事件tag的名称和值|中的备注字段。* 宏`{PROFILE.NOTES<1-9>}`已经不被支持。|
|{INVENTORY.NOTES[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*Notes field in host inventory.* `{PROFILE.NOTES<1-9>}` is deprecated.|

|   |   |   |
|---|---|---|
|{INVENTORY.OOB.IP[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知<br>→ 故障更新通知<br>→ 事件tag的名称和值|中的OOB IP地址字段。*|
|{INVENTORY.OOB.IP[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*OOB IP address field in host inventory.*|

|   |   |   |
|---|---|---|
|{INVENTORY.OOB.NETMASK[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知<br>→ 故障更新通知<br>→ 事件tag的名称和值|中的OOB子网掩码字段。*|
|{INVENTORY.OOB.NETMASK[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*OOB subnet mask field in host inventory.*|

|   |   |   |
|---|---|---|
|{INVENTORY.OOB.ROUTER[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知<br>→ 故障更新通知<br>→ 事件tag的名称和值|中的OOB路由字段。*|
|{INVENTORY.OOB.ROUTER[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*OOB router field in host inventory.*|

|   |   |   |
|---|---|---|
|{INVENTORY.OS[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知\                       宏`{→ 故障更新通知<br>→ 事件tag的名称和值|中的操作系统字段。*<br>ROFILE.OS<1-9>}`已经不被支持。|
|{INVENTORY.OS[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*OS field in host inventory.*<br>`{PROFILE.OS<1-9>}` is deprecated.|

|   |   |   |
|---|---|---|
|{INVENTORY.OS.FULL[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知<br>→ 故障更新通知<br>→ 事件tag的名称和值|中的操作系统详细描述字段。*|
|{INVENTORY.OS.FULL[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*OS (Full details) field in host inventory.*|

|   |   |   |
|---|---|---|
|{INVENTORY.OS.SHORT[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知<br>→ 故障更新通知<br>→ 事件tag的名称和值|中的操作系统缩写字段。*|
|{INVENTORY.OS.SHORT[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*OS (Short) field in host inventory.*|

|   |   |   |
|---|---|---|
|{INVENTORY.POC.PRIMARY.CELL[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知<br>→ 故障更新通知<br>→ 事件tag的名称和值|中的主要POC cell字段。*|
|{INVENTORY.POC.PRIMARY.CELL[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*Primary POC cell field in host inventory.*|

|   |   |   |
|---|---|---|
|{INVENTORY.POC.PRIMARY.EMAIL[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知<br>→ 故障更新通知<br>→ 事件tag的名称和值|中的主要POC邮件字段。*|
|{INVENTORY.POC.PRIMARY.EMAIL[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*Primary POC email field in host inventory.*|

|   |   |   |
|---|---|---|
|{INVENTORY.POC.PRIMARY.NAME[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知<br>→ 故障更新通知<br>→ 事件tag的名称和值|中的POC名称字段。*|
|{INVENTORY.POC.PRIMARY.NAME[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*Primary POC name field in host inventory.*|

|   |   |   |
|---|---|---|
|{INVENTORY.POC.PRIMARY.NOTES[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知<br>→ 故障更新通知<br>→ 事件tag的名称和值|中POC备注字段。*|
|{INVENTORY.POC.PRIMARY.NOTES[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*Primary POC notes field in host inventory.*|

|   |   |   |
|---|---|---|
|{INVENTORY.POC.PRIMARY.PHONE.A[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知<br>→ 故障更新通知<br>→ 事件tag的名称和值|中的主要POC联系电话字段。*|
|{INVENTORY.POC.PRIMARY.PHONE.A[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*Primary POC phone A field in host inventory.*|

|   |   |   |
|---|---|---|
|{INVENTORY.POC.PRIMARY.PHONE.B[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知<br>→ 故障更新通知<br>→ 事件tag的名称和值|中的主要POC联系电话字段。*|
|{INVENTORY.POC.PRIMARY.PHONE.B[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*Primary POC phone B field in host inventory.*|

|   |   |   |
|---|---|---|
|{INVENTORY.POC.PRIMARY.SCREEN[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知<br>→ 故障更新通知<br>→ 事件tag的名称和值|中的主要POC screen名称字段。*|
|{INVENTORY.POC.PRIMARY.SCREEN[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*Primary POC screen name field in host inventory.*|

|   |   |   |
|---|---|---|
|{INVENTORY.POC.SECONDARY.CELL[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知<br>→ 故障更新通知<br>→ 事件tag的名称和值|中的辅助POC cell字段。*|
|{INVENTORY.POC.SECONDARY.CELL[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*Secondary POC cell field in host inventory.*|

|   |   |   |
|---|---|---|
|{INVENTORY.POC.SECONDARY.EMAIL[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *主机清→ 内部通知<br>→ 故障更新通知<br>→ 事件tag的名称和值|中的辅助POC电子邮件字段。*|
|{INVENTORY.POC.SECONDARY.EMAIL[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*Secondary POC email field in host inventory.*|

|   |   |   |
|---|---|---|
|{INVENTORY.POC.SECONDARY.NAME[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知<br>→ 故障更新通知<br>→ 事件tag的名称和值|中的辅助POC名称字段。*|
|{INVENTORY.POC.SECONDARY.NAME[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*Secondary POC name field in host inventory.*|

|   |   |   |
|---|---|---|
|{INVENTORY.POC.SECONDARY.NOTES[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知<br>→ 故障更新通知<br>→ 事件tag的名称和值|中的辅助POC备注字段。*|
|{INVENTORY.POC.SECONDARY.NOTES[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*Secondary POC notes field in host inventory.*|

|   |   |   |
|---|---|---|
|{INVENTORY.POC.SECONDARY.PHONE.A[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知<br>→ 故障更新通知<br>→ 事件tag的名称和值|中辅助POC电话号码字段。*|
|{INVENTORY.POC.SECONDARY.PHONE.A[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*Secondary POC phone A field in host inventory.*|

|   |   |   |
|---|---|---|
|{INVENTORY.POC.SECONDARY.PHONE.B[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知<br>→ 故障更新通知<br>→ 事件tag的名称和值|中辅助POC电话号码字段。*|
|{INVENTORY.POC.SECONDARY.PHONE.B[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*Secondary POC phone B field in host inventory.*|

|   |   |   |
|---|---|---|
|{INVENTORY.POC.SECONDARY.SCREEN[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知<br>→ 故障更新通知<br>→ 事件tag的名称和值|中的辅助POC screen名称字段。*|
|{INVENTORY.POC.SECONDARY.SCREEN[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*Secondary POC screen name field in host inventory.*|

|   |   |   |
|---|---|---|
|{INVENTORY.SERIALNO.A[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知\                       宏`{→ 故障更新通知<br>→ 事件tag的名称和值|中的序列号字段。*<br>ROFILE.SERIALNO<1-9>}`已经不被支持。|
|{INVENTORY.SERIALNO.A[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*Serial number A field in host inventory.*<br>`{PROFILE.SERIALNO<1-9>}` is deprecated.|

|   |   |   |
|---|---|---|
|{INVENTORY.SERIALNO.B[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知<br>→ 故障更新通知<br>→ 事件tag的名称和值|中的序列号字段。*|
|{INVENTORY.SERIALNO.B[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*Serial number B field in host inventory.*|

|   |   |   |
|---|---|---|
|{INVENTORY.SITE.ADDRESS.A[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知<br>→ 故障更新通知<br>→ 事件tag的名称和值|中的站点地址字段。*|
|{INVENTORY.SITE.ADDRESS.A[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*Site address A field in host inventory.*|

|   |   |   |
|---|---|---|
|{INVENTORY.SITE.ADDRESS.B[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知<br>→ 故障更新通知<br>→ 事件tag的名称和值|中的站点地址字段。*|
|{INVENTORY.SITE.ADDRESS.B[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*Site address B field in host inventory.*|

|   |   |   |
|---|---|---|
|{INVENTORY.SITE.ADDRESS.C[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知<br>→ 故障更新通知<br>→ 事件tag的名称和值|中的站点地址字段。*|
|{INVENTORY.SITE.ADDRESS.C[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*Site address C field in host inventory.*|

|   |   |   |
|---|---|---|
|{INVENTORY.SITE.CITY[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知<br>→ 故障更新通知<br>→ 事件tag的名称和值|中的站点城市字段。*|
|{INVENTORY.SITE.CITY[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*Site city field in host inventory.*|

|   |   |   |
|---|---|---|
|{INVENTORY.SITE.COUNTRY[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知<br>→ 故障更新通知<br>→ 事件tag的名称和值|中站点所属国家字段。*|
|{INVENTORY.SITE.COUNTRY[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*Site country field in host inventory.*|

|   |   |   |
|---|---|---|
|{INVENTORY.SITE.NOTES[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知<br>→ 故障更新通知<br>→ 事件tag的名称和值|中站点备注字段。*|
|{INVENTORY.SITE.NOTES[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*Site notes field in host inventory.*|

|   |   |   |
|---|---|---|
|{INVENTORY.SITE.RACK[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知<br>→ 故障更新通知<br>→ 事件tag的名称和值|中的站点机架位置字段。*|
|{INVENTORY.SITE.RACK[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*Site rack location field in host inventory.*|

|   |   |   |
|---|---|---|
|{INVENTORY.SITE.STATE[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知<br>→ 故障更新通知<br>→ 事件tag的名称和值|中站点所属州/省字段。*|
|{INVENTORY.SITE.STATE[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*Site state/province field in host inventory.*|

|   |   |   |
|---|---|---|
|{INVENTORY.SITE.ZIP[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知<br>→ 故障更新通知<br>→ 事件tag的名称和值|中的站点邮编字段。*|
|{INVENTORY.SITE.ZIP[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*Site ZIP/postal field in host inventory.*|

|   |   |   |
|---|---|---|
|{INVENTORY.SOFTWARE[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知<br>→ 故障更新通知<br>→ 事件tag的名称和值|中的软件描述字段。* 宏`{PROFILE.SOFTWARE<1-9>}` 已经不被支持。|
|{INVENTORY.SOFTWARE[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*Software field in host inventory.* `{PROFILE.SOFTWARE<1-9>}` is deprecated.|

|   |   |   |
|---|---|---|
|{INVENTORY.SOFTWARE.APP.A[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知<br>→ 故障更新通知<br>→ 事件tag的名称和值|中的应用软件字段。*|
|{INVENTORY.SOFTWARE.APP.A[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*Software application A field in host inventory.*|

|   |   |   |
|---|---|---|
|{INVENTORY.SOFTWARE.APP.B[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知<br>→ 故障更新通知<br>→ 事件tag的名称和值|中的应用软件字段。*|
|{INVENTORY.SOFTWARE.APP.B[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*Software application B field in host inventory.*|

|   |   |   |
|---|---|---|
|{INVENTORY.SOFTWARE.APP.C[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知<br>→ 故障更新通知<br>→ 事件tag的名称和值|中的应用软件字段。*|
|{INVENTORY.SOFTWARE.APP.C[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*Software application C field in host inventory.*|

|   |   |   |
|---|---|---|
|{INVENTORY.SOFTWARE.APP.D[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知<br>→ 故障更新通知<br>→ 事件tag的名称和值|中的应用软件字段。*|
|{INVENTORY.SOFTWARE.APP.D[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*Software application D field in host inventory.*|

|   |   |   |
|---|---|---|
|{INVENTORY.SOFTWARE.APP.E[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知<br>→ 故障更新通知<br>→ 事件tag的名称和值|中的应用软件字段。*|
|{INVENTORY.SOFTWARE.APP.E[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*Software application E field in host inventory.*|

|   |   |   |
|---|---|---|
|{INVENTORY.SOFTWARE.FULL[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知<br>→ 故障更新通知<br>→ 事件tag的名称和值|中的软件详细描述字段。*|
|{INVENTORY.SOFTWARE.FULL[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*Software (Full details) field in host inventory.*|

|   |   |   |
|---|---|---|
|{INVENTORY.TAG[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知<br>→ 故障更新通知<br>→ 事件tag的名称和值|中的Tag字段。* 宏`{PROFILE.TAG<1-9>}`已经不被支持。|
|{INVENTORY.TAG[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*Tag field in host inventory.* `{PROFILE.TAG<1-9>}` is deprecated.|

|   |   |   |
|---|---|---|
|{INVENTORY.TYPE[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知<br>→ 故障更新通知<br>→ 事件tag的名称和值|中的类型字段。* 宏`{PROFILE.DEVICETYPE<1-9>}`已经不被支持。|
|{INVENTORY.TYPE[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*Type field in host inventory.* `{PROFILE.DEVICETYPE<1-9>}` is deprecated.|

|   |   |   |
|---|---|---|
|{INVENTORY.TYPE.FULL[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知<br>→ 故障更新通知<br>→ 事件tag的名称和值|中的详细类型描述字段。*|
|{INVENTORY.TYPE.FULL[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*Type (Full details) field in host inventory.*|

|   |   |   |
|---|---|---|
|{INVENTORY.URL.A[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知<br>→ 故障更新通知<br>→ 事件tag的名称和值|中的URL字段。*|
|{INVENTORY.URL.A[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*URL A field in host inventory.*|

|   |   |   |
|---|---|---|
|{INVENTORY.URL.B[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知<br>→ 故障更新通知<br>→ 事件tag的名称和值|中的URL字段。*|
|{INVENTORY.URL.B[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*URL B field in host inventory.*|

|   |   |   |
|---|---|---|
|{INVENTORY.URL.C[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知<br>→ 故障更新通知<br>→ 事件tag的名称和值|中的URL字段。*|
|{INVENTORY.URL.C[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*URL C field in host inventory.*|

|   |   |   |
|---|---|---|
|{INVENTORY.VENDOR[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\              *设备清→ 内部通知<br>→ 故障更新通知<br>→ 事件tag的名称和值|中的供应商字段。*|
|{INVENTORY.VENDOR[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ Event tags and values|*Vendor field in host inventory.*|

|   |   |   |
|---|---|---|
|{ITEM.DESCRIPTION[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\             *触发器→ 内部通知<br>→ 故障更新通知|达式中导致发送通知的第N个item的描述信息。*从2.0.0开始支持。|
|{ITEM.DESCRIPTION[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications|*Description of the Nth item in the trigger expression that caused a notification.* Supported since 2.0.0.|

|   |   |   |
|---|---|---|
|{ITEM.ID[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\                                                                   *触发器→ 内部通知<br>→ 故障更新通知<br>→ HTTP agent的item类型, item原型和发现规则字段:<br>URL, query fields, request body, headers, proxy, SSL certificate file, SSL key file.|达式中导致发送通知的第N个item的数字标识。*从1.8.12开始支持。|
|{ITEM.ID[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ HTTP agent type item, item prototype and discovery rule fields:<br>URL, query fields, request body, headers, proxy, SSL certificate file, SSL key file.|*Numeric ID of the Nth item in the trigger expression that caused a notification.* Supported since 1.8.12.|

|   |   |   |
|---|---|---|
|{ITEM.KEY[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\                                                                   *触发器→ 内部通知\                                                                            宏`{→ 故障更新通知<br>→ HTTP agent的item类型, item原型和发现规则字段:<br>URL, query fields, request body, headers, proxy, SSL certificate file, SSL key file.|达式中导致发送通知的第N个item的key。* 从2.0.0开始支持。<br>RIGGER.KEY}`已经不被支持。|
|{ITEM.KEY[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications<br>→ HTTP agent type item, item prototype and discovery rule fields:<br>URL, query fields, request body, headers, proxy, SSL certificate file, SSL key file.|*Key of the Nth item in the trigger expression that caused a notification.* Supported since 2.0.0.<br>`{TRIGGER.KEY}` is deprecated.|

|   |   |   |
|---|---|---|
|{ITEM.KEY.ORIG[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\             *触发器→ 内部通知<br>→ 故障更新通知|达式中导致发送通知的第N个item的原始key。* 从2.0.6开始支持。|
|{ITEM.KEY.ORIG[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications|*Original key (with macros not expanded) of the Nth item in the trigger expression that caused a notification.* Supported since 2.0.6.|

|   |   |   |
|---|---|---|
|{ITEM.LASTVALUE[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\                *触发器→ 故障更新通知\                     如果最近一→ Trigger名称和描述\                从1.4→ 事件tag的名称和值                 <br>|达式中导致发送通知的第N个item的最近一个值。*<br>历史值采集时间已经超过参数*ZBX\_HISTORY\_PERIOD*定义的历史数据保存时间，那么在前端会显示值为\*UNKNOWN\*。 (参数*ZBX\_HISTORY\_PERIOD*定义于[defines.inc.php](/manual/web_interface/definitions))。<br>3开始支持。 该宏等同于宏`{{HOST.HOST}:{ITEM.KEY}.last()}`<br>从Zabbix 3.2.0开始支持 [自定义](/manual/config/macros/macro_functions) 宏值。|
|{ITEM.LASTVALUE[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Problem update notifications<br>→ Trigger names and descriptions<br>→ Event tags and values|*The latest value of the Nth item in the trigger expression that caused a notification.*<br>It will resolve to \*UNKNOWN\* in the frontend if the latest history value has been collected more than the *ZBX\_HISTORY\_PERIOD* time ago (defined in [defines.inc.php](/manual/web_interface/definitions)).<br>Supported since 1.4.3. It is alias to `{{HOST.HOST}:{ITEM.KEY}.last()}`<br><br>[Customizing](/manual/config/macros/macro_functions) the macro value is supported for this macro; starting with Zabbix 3.2.0.|

|   |   |   |
|---|---|---|
|{ITEM.LOG.AGE[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\             *日志i→ 故障更新通知|em事件的持续时间。*|
|{ITEM.LOG.AGE[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Problem update notifications|*Age of the log item event.*|

|   |   |   |
|---|---|---|
|{ITEM.LOG.DATE[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\             *日志i→ 故障更新通知|em事件的发生时间。*|
|{ITEM.LOG.DATE[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Problem update notifications|*Date of the log item event.*|

|   |   |   |
|---|---|---|
|{ITEM.LOG.EVENTID[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\             *日志事→ 故障更新通知                   仅用于Wi|的标识。*<br>dows事件日志监控 。|
|{ITEM.LOG.EVENTID[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Problem update notifications|*ID of the event in the event log.*<br>For Windows event log monitoring only.|

|   |   |   |
|---|---|---|
|{ITEM.LOG.NSEVERITY[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\             *日志事→ 故障更新通知                   仅用于Wi|的级别。*<br>dows事件日志监控 。|
|{ITEM.LOG.NSEVERITY[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Problem update notifications|*Numeric severity of the event in the event log.*<br>For Windows event log monitoring only.|

|   |   |   |
|---|---|---|
|{ITEM.LOG.SEVERITY[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\             *日志事→ 故障更新通知                   仅用于Wi|的级别。*<br>dows事件日志监控 。|
|{ITEM.LOG.SEVERITY[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Problem update notifications|*Verbal severity of the event in the event log.*<br>For Windows event log monitoring only.|

|   |   |   |
|---|---|---|
|{ITEM.LOG.SOURCE[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\             *日志事→ 故障更新通知                   仅用于Wi|的来源。*<br>dows事件日志监控 。|
|{ITEM.LOG.SOURCE[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Problem update notifications|*Source of the event in the event log.*<br>For Windows event log monitoring only.|

|   |   |   |
|---|---|---|
|{ITEM.LOG.TIME[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\             *日志i→ 故障更新通知|em事件的发生时间。*|
|{ITEM.LOG.TIME[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Problem update notifications|*Time of the log item event.*|

|   |   |   |
|---|---|---|
|{ITEM.NAME[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\             *触发器→ 内部通知<br>→ 故障更新通知|达式中导致发送通知的第N个item的名称。*|
|{ITEM.NAME[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications|*Name of the Nth item (with macros resolved) in the trigger expression that caused a notification.*|

|   |   |   |
|---|---|---|
|{ITEM.NAME.ORIG[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\             *触发器→ 内部通知\                      从2.→ 故障更新通知|达式中导致发送通知的第N个item的原始名称。*<br>.6开始支持。|
|{ITEM.NAME.ORIG[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Internal notifications<br>→ Problem update notifications|*Original name (i.e. without macros resolved) of the Nth item in the trigger expression that caused a notification.*<br>Supported since 2.0.6.|

|   |   |   |
|---|---|---|
|{ITEM.STATE[<1-9>](supported_by_location#indexed_macros)}|→ 基于Item的内部通知                  *触发器表达|中导致发送通知的第N个item的状态。* 可能的值: **Not supported** 和 **Normal**.<br>从2.2.0开始支持。|
|{ITEM.STATE[<1-9>](supported_by_location#indexed_macros)}|→ Item-based internal notifications|*The latest state of the Nth item in the trigger expression that caused a notification.* Possible values: **Not supported** and **Normal**.<br>Supported since 2.2.0.|

|   |   |   |
|---|---|---|
|{ITEM.VALUE[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知\                可能的值→ 故障更新通知\                     1) 如果→ Trigger名称和描述\                2) 如→ 事件tag的名称和值                 在第一种情况|<br>触发器状态更改的上下文中使用,例如,显示事件或发送通知。该值为触发器表达式中的第N个item的历史（at-the-time-of-event）值。<br>不在触发器状态更改的上下文中使用,例如,在弹出窗口中显示触发器列表时，该值为触发器表达式中的第N个item的最近一个值，类似于{ITEM.LASTVALUE}。<br>历史数据被删除或未入库时候值解析为\*UNKNOWN\*。<br>在第二种情况如果最近一个历史值采集时间已经超过参数*ZBX\_HISTORY\_PERIOD*定义的历史数据保存时间，那么在前端会显示值为\*UNKNOWN\*。(参数*ZBX\_HISTORY\_PERIOD*定义于[defines.inc.php](/manual/web_interface/definitions)).<br>从1.4.3开始支持。<br><br>从Zabbix 3.2.0开始支持[自定义](/manual/config/macros/macro_functions)宏值。|
|{ITEM.VALUE[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications<br>→ Problem update notifications<br>→ Trigger names and descriptions<br>→ Event tags and values|Resolved to either:<br>1) the historical (at-the-time-of-event) value of the Nth item in the trigger expression, if used in the context of trigger status change, for example, when displaying events or sending notifications.<br>2) the latest value of the Nth item in the trigger expression, if used without the context of trigger status change, for example, when displaying a list of triggers in a pop-up selection window. In this case works the same as {ITEM.LASTVALUE}<br>In the first case it will resolve to \*UNKNOWN\* if the history value has already been deleted or has never been stored.<br>In the second case, and in the frontend only, it will resolve to \*UNKNOWN\* if the latest history value has been collected more than the *ZBX\_HISTORY\_PERIOD* time ago (defined in [defines.inc.php](/manual/web_interface/definitions)).<br>Supported since 1.4.3.<br><br>[Customizing](/manual/config/macros/macro_functions) the macro value is supported for this macro, starting with Zabbix 3.2.0.|

|   |   |   |
|---|---|---|
|{LLDRULE.DESCRIPTION}|→ LLD-rule based 内部通知                 *触发|知的low-level发现规则描述。*<br>从2.2.0开始支持。|
|{LLDRULE.DESCRIPTION}|→ LLD-rule based internal notifications|*Description of the low-level discovery rule 、which caused a notification.*<br>Supported since 2.2.0.|

|   |   |   |
|---|---|---|
|{LLDRULE.ID}|→ LLD-rule based 内部通知                 *触发|知的low-level发现规则的数字标识。*<br>从2.2.0开始支持。|
|{LLDRULE.ID}|→ LLD-rule based internal notifications|*Numeric ID of the low-level discovery rule which caused a notification.*<br>Supported since 2.2.0.|

|   |   |   |
|---|---|---|
|{LLDRULE.KEY}|→ LLD-rule based 内部通知                 *触发|知的low-level发现规则的key。*<br>从2.2.0开始支持。|
|{LLDRULE.KEY}|→ LLD-rule based internal notifications|*Key of the low-level discovery rule which caused a notification.*<br>Supported since 2.2.0.|

|   |   |   |
|---|---|---|
|{LLDRULE.KEY.ORIG}|→ LLD-rule based 内部通知                 *触发|知的low-level发现规则的原始key（未扩展宏）。*<br>从2.2.0开始支持。|
|{LLDRULE.KEY.ORIG}|→ LLD-rule based internal notifications|*Original key (with macros not expanded) of the low-level discovery rule which caused a notification.*<br>Supported since 2.2.0.|

|   |   |   |
|---|---|---|
|{LLDRULE.NAME}|→ LLD-rule based 内部通知                 *触发|知的low-level发现规则的名称（未扩展宏）。*<br>从2.2.0开始支持。|
|{LLDRULE.NAME}|→ LLD-rule based internal notifications|*Name of the low-level discovery rule (with macros resolved) that caused a notification.*<br>Supported since 2.2.0.|

|   |   |   |
|---|---|---|
|{LLDRULE.NAME.ORIG}|→ LLD-rule based 内部通知                 *触发|知的low-level发现规则的原始名称（未扩展宏）。*<br>从2.2.0开始支持。|
|{LLDRULE.NAME.ORIG}|→ LLD-rule based internal notifications|*Original name (i.e. without macros resolved) of the low-level discovery rule that caused a notification.*<br>Supported since 2.2.0.|

|   |   |   |
|---|---|---|
|{LLDRULE.STATE}|→ LLD-rule based 内部通知                 *lo|-level发现规则的最新状态。* 可能的值: **Not supported** 和 **Normal**.<br>从2.2.0开始支持。|
|{LLDRULE.STATE}|→ LLD-rule based internal notifications|*The latest state of the low-level discovery rule.* Possible values: **Not supported** and **Normal**.<br>Supported since 2.2.0.|

|   |   |   |
|---|---|---|
|{MAP.ID}|→ 地图URLs   *|络地图标识。*|
|{MAP.ID}|→ Map URLs|*Network map ID.*|

|   |   |   |
|---|---|---|
|{MAP.NAME}|→ 地图形状中的文字描述字段   *网络地图名称。*<br>|<从3.4.0开始支持。|
|{MAP.NAME}|→ Text field in map shapes|*Network map name.*<br>Supported since 3.4.0.|

|   |   |   |
|---|---|---|
|{PROXY.DESCRIPTION[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知和命令<br>→ 发现通知<br>→ 自动注册通知<br>→ 内部通知<br>→ 故障更新通知|<|
|*proxy描述信息。*可能的值:<br>1)触发器表达式中第N个项的proxy的信息（基于Trigger的通知）。可以使用 [宏索引](/manual/appendix/macros/supported_by_location#indexed_macros)。<br>2) 执行发现的proxy信息(发现通知)。可以使用宏{PROXY.DESCRIPTION}，而不带宏索引。<br>3) 主动agent注册的proxy信息。(自动注册通知)。可以使用宏{PROXY.DESCRIPTION}，而不带宏索引。<br>从2.4.0开始支持。|<|<|
|{PROXY.DESCRIPTION[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications and commands<br>→ Discovery notifications<br>→ Auto-registration notifications<br>→ Internal notifications<br>→ Problem update notifications|*Description of the proxy*. Resolves to either:<br>1) proxy of the Nth item in the trigger expression (in trigger-based notifications). You may use [indexed](/manual/appendix/macros/supported_by_location#indexed_macros) macros here.<br>2) proxy, which executed discovery (in discovery notifications). Use {PROXY.DESCRIPTION} here, without indexing.<br>3) proxy to which an active agent registered (in auto-registration notifications). Use {PROXY.DESCRIPTION} here, without indexing.<br>Supported since 2.4.0.|

|   |   |   |
|---|---|---|
|{PROXY.NAME[<1-9>](supported_by_location#indexed_macros)}|→ 基于Trigger的通知和命令\                    *proxy的→ 发现通知\                                   1)→ 自动注册通知\                               2) 执行→ 内部通知\                                   3)→ 故障更新通知                                从1.8.|称。*。可能的值:<br>发器表达式中第N个项的proxy的名称（基于Trigger的通知）。可以使用 [宏索引](/manual/appendix/macros/supported_by_location#indexed_macros)。<br>现的proxy名称(发现通知)。可以使用宏 {PROXY.NAME} ，而不带宏索引。<br>动agent注册的proxy名称。(自动注册通知)。可以使用宏 {PROXY.NAME}，而不带宏索引。<br>开始支持。|
|{PROXY.NAME[<1-9>](supported_by_location#indexed_macros)}|→ Trigger-based notifications and commands<br>→ Discovery notifications<br>→ Auto-registration notifications<br>→ Internal notifications<br>→ Problem update notifications|*Name of the proxy*. Resolves to either:<br>1) proxy of the Nth item in the trigger expression (in trigger-based notifications). You may use [indexed](/manual/appendix/macros/supported_by_location#indexed_macros) macros here.<br>2) proxy, which executed discovery (in discovery notifications). Use {PROXY.NAME} here, without indexing.<br>3) proxy to which an active agent registered (in auto-registration notifications). Use {PROXY.NAME} here, without indexing.<br>Supported since 1.8.4.|

|   |   |   |
|---|---|---|
|{TIME}|→ 基于Trigger的通知和命令\                    *时间格式为：→ 发现通知<br>→ 自动注册通知<br>→ 内部通知<br>→ 故障更新通知|h:mm:ss.*|
|{TIME}|→ Trigger-based notifications and commands<br>→ Discovery notifications<br>→ Auto-registration notifications<br>→ Internal notifications<br>→ Problem update notifications|*Current time in hh:mm:ss.*|

|   |   |   |
|---|---|---|
|{TRIGGER.DESCRIPTION}|→ 基于Trigger的通知\                      *Tri→ Trigger-based 内部通知\                 从2.→ 故障更新通知                            宏`{TR|ger描述信息。* 从2.0.4开始支持。<br>.0开始如果在通知文本中使用“{TRIGGER.DESCRIPTION}”，trigger描述将支持所有宏。<br>GGER.COMMENT}`已经不被支持。|
|{TRIGGER.DESCRIPTION}|→ Trigger-based notifications<br>→ Trigger-based internal notifications<br>→ Problem update notifications|*Trigger description.* Supported since 2.0.4.<br>Starting with 2.2.0, all macros supported in a trigger description will be expanded if `{TRIGGER.DESCRIPTION}` is used in notification text.<br>`{TRIGGER.COMMENT}` is deprecated.|

|   |   |   |
|---|---|---|
|{TRIGGER.EVENTS.ACK}|→ 基于Trigger的通知\                                          *地图中→ 故障更新通知<br>→ 地图中的Icon标签^[1](supported_by_location#footnotes)^|素的已确认事件数，或者在通知中生成当前事件的触发器的已确认事件数。* 从1.8.3开始支持。|
|{TRIGGER.EVENTS.ACK}|→ Trigger-based notifications<br>→ Problem update notifications<br>→ Icon labels in maps^[1](supported_by_location#footnotes)^|*Number of acknowledged events for a map element in maps, or for the trigger which generated current event in notifications.* Supported since 1.8.3.|

|   |   |   |
|---|---|---|
|{TRIGGER.EVENTS.PROBLEM.ACK}|→ 基于Trigger的通知\                                          *忽略状→ 故障更新通知<br>→ 地图中的Icon标签^[1](supported_by_location#footnotes)^|的所有触发器的已确认故障事件数。* 从1.8.3开始支持。|
|{TRIGGER.EVENTS.PROBLEM.ACK}|→ Trigger-based notifications<br>→ Problem update notifications<br>→ Icon labels in maps^[1](supported_by_location#footnotes)^|*Number of acknowledged PROBLEM events for all triggers disregarding their state.* Supported since 1.8.3.|

|   |   |   |
|---|---|---|
|{TRIGGER.EVENTS.PROBLEM.UNACK}|→ 基于Trigger的通知\                                          *忽略状→ 故障更新通知<br>→ 地图中的Icon标签^[1](supported_by_location#footnotes)^|的所有触发器的未确认故障事件数。* 从1.8.3开始使用。|
|{TRIGGER.EVENTS.PROBLEM.UNACK}|→ Trigger-based notifications<br>→ Problem update notifications<br>→ Icon labels in maps^[1](supported_by_location#footnotes)^|*Number of unacknowledged PROBLEM events for all triggers disregarding their state.* Supported since 1.8.3.|

|   |   |   |
|---|---|---|
|{TRIGGER.EVENTS.UNACK}|→ 基于Trigger的通知\                                          *地图中→ 故障更新通知<br>→ 地图中的Icon标签^[1](supported_by_location#footnotes)^|素的未确认事件数，或者通知中生成当前事件的触发器的未确认事件数。* 从1.8.3开始支持地图元素标签。|
|{TRIGGER.EVENTS.UNACK}|→ Trigger-based notifications<br>→ Problem update notifications<br>→ Icon labels in maps^[1](supported_by_location#footnotes)^|*Number of unacknowledged events for a map element in maps, or for the trigger which generated current event in notifications.* Supported in map element labels since 1.8.3.|

|   |   |   |
|---|---|---|
|{TRIGGER.HOSTGROUP.NAME}|→ 基于Trigger的通知\                     *基于S→ 故障更新通知<br>→ 基于Trigger内部通知|L查询排序，逗号-空格分隔的trigger所属的设备组列表。* 从2.0.6开始支持。|
|{TRIGGER.HOSTGROUP.NAME}|→ Trigger-based notifications<br>→ Problem update notifications<br>→ Trigger-based internal notifications|*A sorted (by SQL query), comma-space separated list of host groups in which the trigger is defined.* Supported since 2.0.6.|

|   |   |   |
|---|---|---|
|{TRIGGER.PROBLEM.EVENTS.PROBLEM.ACK}|→ 地图Icon标签^[1](supported_by_location#footnotes)^          *触发|状态为问题的已确认问题事件数。* 从1.8.3开始支持。|
|{TRIGGER.PROBLEM.EVENTS.PROBLEM.ACK}|→ Icon labels in maps^[1](supported_by_location#footnotes)^|*Number of acknowledged PROBLEM events for triggers in PROBLEM state.* Supported since 1.8.3.|

|   |   |   |
|---|---|---|
|{TRIGGER.PROBLEM.EVENTS.PROBLEM.UNACK}|→ 地图Icon标签^[1](supported_by_location#footnotes)^          *触发|状态为问题的未确认问题事件数。* 从1.8.3开始支持。|
|{TRIGGER.PROBLEM.EVENTS.PROBLEM.UNACK}|→ Icon labels in maps^[1](supported_by_location#footnotes)^|*Number of unacknowledged PROBLEM events for triggers in PROBLEM state.* Supported since 1.8.3.|

|   |   |   |
|---|---|---|
|{TRIGGER.EXPRESSION}|→ 基于Trigger的通知\                      *Tri→ 基于Trigger内部通知<br>→ 故障更新通知|ger表达式。* 从1.8.12开始支持。|
|{TRIGGER.EXPRESSION}|→ Trigger-based notifications<br>→ Trigger-based internal notifications<br>→ Problem update notifications|*Trigger expression.* Supported since 1.8.12.|

|   |   |   |
|---|---|---|
|{TRIGGER.EXPRESSION.RECOVERY}|→ 基于Trigger的通知\                      *Tri→ 基于Trigger内部通知\                    从3.2.→ 故障更新通知|ger恢复表达式。*如果 *恢复事件*在[trigger配置](/manual/config/triggers/trigger)中设置为'Recovery expression'则返回表达式，否则返回空字符串。<br>开始支持。|
|{TRIGGER.EXPRESSION.RECOVERY}|→ Trigger-based notifications<br>→ Trigger-based internal notifications<br>→ Problem update notifications|*Trigger recovery expression* if *OK event generation* in [trigger configuration](/manual/config/triggers/trigger) is set to 'Recovery expression'; otherwise an empty string is returned.<br>Supported since 3.2.0.|

|   |   |   |
|---|---|---|
|{TRIGGER.ID}|→ 基于Trigger的通知\                      *触发动→ Trigger-based 内部通知\                 从1.→ 故障更新通知<br>→ 图形URLs<br>→ Trigger URLs|的Trigger数字标识。*<br>.8开始支持trigger URLs。|
|{TRIGGER.ID}|→ Trigger-based notifications<br>→ Trigger-based internal notifications<br>→ Problem update notifications<br>→ Map URLs<br>→ Trigger URLs|*Numeric trigger ID which triggered this action.*<br>Supported in trigger URLs since Zabbix 1.8.8.|

|   |   |   |
|---|---|---|
|{TRIGGER.NAME}|→ 基于Trigger的通知\                      *tri→ 基于Trigger内部通知\                    从 4.0→ 故障更新通知|ger名称。* (支持宏解析)。<br>0 开始宏{EVENT.NAME}不能用于动作中去显示触发事件名称（支持宏解析）。|
|{TRIGGER.NAME}|→ Trigger-based notifications<br>→ Trigger-based internal notifications<br>→ Problem update notifications|*Name of the trigger* (with macros resolved).<br>Note that since 4.0.0 {EVENT.NAME} can be used in actions to display the triggered event/problem name with macros resolved.|

|   |   |   |
|---|---|---|
|{TRIGGER.NAME.ORIG}|→ 基于Trigger的通知\                      *tri→ 基于Trigger内部通知\                    从2.0.→ 故障更新通知|ger的原始名称* (即没有宏解析).<br>开始支持。|
|{TRIGGER.NAME.ORIG}|→ Trigger-based notifications<br>→ Trigger-based internal notifications<br>→ Problem update notifications|*Original name of the trigger* (i.e. without macros resolved).<br>Supported since 2.0.6.|

|   |   |   |
|---|---|---|
|{TRIGGER.NSEVERITY}|→ 基于Trigger的通知\                      *tri→ 基于Trigger内部通知\                    从Zabb→ 故障更新通知|ger数字级别。* 可能的值: 0 - 未定义, 1 - 信息, 2 - 警告, 3 - 普通, 4 - 严重, 5 - 灾难.<br>x 1.6.2开始支持。|
|{TRIGGER.NSEVERITY}|→ Trigger-based notifications<br>→ Trigger-based internal notifications<br>→ Problem update notifications|*Numerical trigger severity.* Possible values: 0 - Not classified, 1 - Information, 2 - Warning, 3 - Average, 4 - High, 5 - Disaster.<br>Supported starting from Zabbix 1.6.2.|

|   |   |   |
|---|---|---|
|{TRIGGER.SEVERITY}|→ 基于Trigger的通知\                      *Tri→ 基于Trigger内部通知<br>→ 故障更新通知|ger级别名称.* 可在*管理 → 通用 → Trigger 级别*功能中定义。|
|{TRIGGER.SEVERITY}|→ Trigger-based notifications<br>→ Trigger-based internal notifications<br>→ Problem update notifications|*Trigger severity name.* Can be defined in *Administration → General → Trigger severities*.|

|   |   |   |
|---|---|---|
|{TRIGGER.STATE}|→ 基于Trigger内部通知                    *trig|er的最新状态。* 可能的值: **Unknown** and **Normal**.<br>从2.2.0开始支持。|
|{TRIGGER.STATE}|→ Trigger-based internal notifications|*The latest state of the trigger.* Possible values: **Unknown** and **Normal**.<br>Supported since 2.2.0.|

|   |   |   |
|---|---|---|
|{TRIGGER.STATUS}|→ 基于Trigger的通知\             *当前t→ 故障更新通知                   宏`{ST|igger的值。* 可能是PROBLEM或OK.<br>TUS}`已经不被支持。|
|{TRIGGER.STATUS}|→ Trigger-based notifications<br>→ Problem update notifications|*Current trigger value.* Can be either PROBLEM or OK.<br>`{STATUS}` is deprecated.|

|   |   |   |
|---|---|---|
|{TRIGGER.TEMPLATE.NAME}|→ 基于Trigger的通知\                      *排序（→ 基于Trigger内部通知<br>→ 故障更新通知|过SQL查询），逗号-空格分隔的触发器所属模板列表，如果触发器应用于具体设备，则为\* UNKNOWN \** 从2.0.6开始支持。|
|{TRIGGER.TEMPLATE.NAME}|→ Trigger-based notifications<br>→ Trigger-based internal notifications<br>→ Problem update notifications|*A sorted (by SQL query), comma-space separated list of templates in which the trigger is defined, or \*UNKNOWN\* if the trigger is defined in a host.* Supported since 2.0.6.|

|   |   |   |
|---|---|---|
|{TRIGGER.URL}|→ 基于Trigger的通知\                      *Tri→ 基于Trigger内部通知<br>→ 故障更新通知|ger URL.*|
|{TRIGGER.URL}|→ Trigger-based notifications<br>→ Trigger-based internal notifications<br>→ Problem update notifications|*Trigger URL.*|

|   |   |   |
|---|---|---|
|{TRIGGER.VALUE}|→ 基于Trigger的通知\             *触发器→ Trigger 表达式<br>→ 故障更新通知|当前值。*: 0 - trigger状态为OK, 1 - trigger状态为PROBLEM。|
|{TRIGGER.VALUE}|→ Trigger-based notifications<br>→ Trigger expressions<br>→ Problem update notifications|*Current trigger numeric value*: 0 - trigger is in OK state, 1 - trigger is in PROBLEM state.|

|   |   |   |
|---|---|---|
|{TRIGGERS.UNACK}|→ 地图Icon标签^[1](supported_by_location#footnotes)^          *忽略|发器状态，地图元素的未确认触发器数。*\\\\如果至少有一个PROBLEM事件未被确认，则认为触发器未被确认。|
|{TRIGGERS.UNACK}|→ Icon labels in maps^[1](supported_by_location#footnotes)^|*Number of unacknowledged triggers for a map element, disregarding trigger state.*<br>A trigger is considered to be unacknowledged if at least one of its PROBLEM events is unacknowledged.|

|   |   |   |
|---|---|---|
|{TRIGGERS.PROBLEM.UNACK}|→ 地图Icon标签^[1](supported_by_location#footnotes)^          *地图|素的未确认触发器（状态为PROBLEM）数。*<br>如果至少有一个PROBLEM事件未被确认，则认为触发器未被确认。<br>从1.8.3开始支持。|
|{TRIGGERS.PROBLEM.UNACK}|→ Icon labels in maps^[1](supported_by_location#footnotes)^|*Number of unacknowledged PROBLEM triggers for a map element.*<br>A trigger is considered to be unacknowledged if at least one of its PROBLEM events is unacknowledged.<br>Supported since 1.8.3.|

|   |   |   |
|---|---|---|
|{TRIGGERS.ACK}|→ 地图Icon标签^[1](supported_by_location#footnotes)^          *忽略|发器状态，地图元素的确认触发器数，*<br>当所有PROBLEM事件都被确认后，trigger才被认为已经确认。<br>从1.8.3开始支持。|
|{TRIGGERS.ACK}|→ Icon labels in maps^[1](supported_by_location#footnotes)^|*Number of acknowledged triggers for a map element, disregarding trigger state.*<br>A trigger is considered to be acknowledged if all of it's PROBLEM events are acknowledged.<br>Supported since 1.8.3.|

|   |   |   |
|---|---|---|
|{TRIGGERS.PROBLEM.ACK}|→ 地图Icon标签^[1](supported_by_location#footnotes)^          *地图|素的确认触发器（状态为PROBLEM）数。*<br>当所有PROBLEM事件都被确认后，trigger才被认为已经确认。<br>从1.8.3开始支持。|
|{TRIGGERS.PROBLEM.ACK}|→ Icon labels in maps^[1](supported_by_location#footnotes)^|*Number of acknowledged PROBLEM triggers for a map element.*<br>A trigger is considered to be acknowledged if all of it's PROBLEM events are acknowledged.<br>Supported since 1.8.3.|

|   |   |   |
|---|---|---|
|{USER.FULLNAME}|→ 故障更新通知                   *事件确认|作的用户全名。*<br>从3.4.0开始支持。|
|{USER.FULLNAME}|→ Problem update notifications|*Name and surname of the user* who added event acknowledgement.<br>Supported since 3.4.0.|

|   |   |   |
|---|---|---|
|{host:key.func(param)}|→ 基于Trigger的通知\                                                                                        *简单的→ 故障更新通知\                                                                                             <br>→ 地图Icon/shape标签^[1](supported_by_location#footnotes)^ ^[4](supported_by_location#footnotes)^\          从3.→ 地图Link标签^[4](supported_by_location#footnotes)^<br>→ 图形名称^[7](supported_by_location#footnotes)^<br>→ Trigger表达式^[9](supported_by_location#footnotes)^|，用于构建触发器[表达式](/manual/config/triggers/expression)*。<br>.2开始支持shape标签。|
|{$MACRO}|→ 参考: [用户自定义宏使用场景](/manual/appendix/macros/supported_by_location_user)                          *[用户自定义](/m|nual/config/macros/user_macros)宏。*|
|{\#MACRO}|→ 参考: [Low-level发现宏](/manual/config/macros/lld_macros)                                                 *Low|level发现宏*<br>从2.0.0开始支持。|
|{host:key.func(param)}|→ Trigger-based notifications<br>→ Problem update notifications<br>→ Icon/shape labels in maps^[1](supported_by_location#footnotes)^ ^[4](supported_by_location#footnotes)^<br>→ Link labels in maps^[4](supported_by_location#footnotes)^<br>→ Graph names^[7](supported_by_location#footnotes)^<br>→ Trigger expressions^[9](supported_by_location#footnotes)^|*Simple macros, as used in building trigger [expressions](/manual/config/triggers/expression)*.<br><br>Supported for shape labels in maps since 3.4.2.|
|{$MACRO}|→ See: [User macros supported by location](/manual/appendix/macros/supported_by_location_user)|*[User-definable](/manual/config/macros/user_macros) macros.*|
|{\#MACRO}|→ See: [Low-level discovery macros](/manual/config/macros/lld_macros)|*Low-level discovery macros.*<br>Supported since 2.0.0.|

##### 脚注

##### Footnotes

^**1**^ 从1.8开始地图标签支持宏。 ^**1**^ Macros for map labels are
supported since 1.8.

^**2**^ 宏`{HOST.*}`用于item
key参数将解析为所选item的接口。如果item无接口，将按优先顺序解析为设备的Zabbix
agent,SNMP,JMX，IPMI接口。 ^**2**^ The `{HOST.*}` macros supported in
item key parameters will resolve to the interface that is selected for
the item. When used in items without interfaces they will resolve to
either the Zabbix agent, SNMP, JMX or IPMI interface of the host in this
order of priority.

^**3**^ 在remote commands, global scripts, interface IP/DNS字段和web
scenarios宏将解析为主代理接口。如果不存在，则使用SNMP接口。如果SNMP接口也不存在，则使用JMX接口。如果JMX接口不存在则使用IPMI接口。
^**3**^ In remote commands, global scripts, interface IP/DNS fields and
web scenarios the macro will resolve to the main agent interface,
however, if it is not present, the main SNMP interface will be used. If
SNMP is also not present, the main JMX interface will be used. If JMX is
not present either, the main IPMI interface will be used.

^**4**^ 地图标签中的宏仅仅支持**avg**, **last**, **max** and **min**
函数, 以秒为单位。 ^**4**^ Only the **avg**, **last**, **max** and
**min** functions, with seconds as parameter are supported in this macro
in map labels.

^**5**^ 从2.0.3开始支持。

^**6**^ 从Zabbix 2.2.0开始, 宏`{HOST.*}`可以用于web scenario中的*Name*,
*Variables*, *Headers*, *SSL certificate file* and *SSL key file* fields
and in scenario step *Name*, *URL*, *Post*, *Headers* and *Required
string* 字段。 ^**6**^ Supported since Zabbix 2.2.0, `{HOST.*}` macros
are supported in web scenario *Name*, *Variables*, *Headers*, *SSL
certificate file* and *SSL key file* fields and in scenario step *Name*,
*URL*, *Post*, *Headers* and *Required string* fields.

^**7**^ 从Zabbix
2.2.0开始，地图标签中的宏仅支持avg，last，max和min函数，以秒为参数。
宏{HOST.HOST<1-9>} 可以用于引用某个设备。例如:

     * {Cisco switch:ifAlias[{#SNMPINDEX}].last()}
     * %%{{%%HOST.HOST}:ifAlias[{#SNMPINDEX}].last()}

^**8**^ 从2.4.0开始支持。

^**9**^ 虽然支持构建触发器表达式，但是不能在彼此内部使用简单的宏。
^**9**^ While supported to build trigger expressions, simple macros may
not be used inside each other.

^**10**^ 从3.0.0开始支持。 ^**10**^ Supported since 3.0.0.

#### 宏索引

#### Indexed macros

宏索引{MACRO**<1-9>**}语法仅限于**触发器表达式的上下文**。它能用于按顺序引用表达式中包含的设备。例如：在表达式中包含了设备1，设备2，设备3，那么宏{HOST.IP1},
{HOST.IP2}, {HOST.IP3}将分别引用设备1，设备2，设备3的IP地址信息。 The
indexed macro syntax of {MACRO**<1-9>**} is limited to the context
of **trigger expressions**. It can be used to reference hosts in the
order in which they appear in the expression. Macros like {HOST.IP1},
{HOST.IP2}, {HOST.IP3} will resolve to the IP of the first, second and
third host in the trigger expression (providing the trigger expression
contains those hosts).

另外，可以在图形名称中使用宏`{host:key.func(param)}`，同时再叠加使用宏{HOST.HOST<1-9>}。
示例,
图形名称中的宏`{{HOST.HOST2}:key.func()}`代表引用图形中的第二个设备。
Additionally the {HOST.HOST<1-9>} macro is also supported within
the `{host:key.func(param)}` macro in **graph names**. For example,
`{{HOST.HOST2}:key.func()}` in the graph name will refer to the host of
the second item in the graph.

::: notewarning
有些场景可以使用不带索引的宏。(例如：{HOST.HOST},
{HOST.IP}, 等)
::: 

::: notewarning
Use macros **without**
index (i. e.{HOST.HOST}, {HOST.IP}, etc) in all other
contexts.
:::

[comment]: # ({/new-3a90dc8f})
