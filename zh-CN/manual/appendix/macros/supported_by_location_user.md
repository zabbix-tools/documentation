[comment]: # translation:outdated

[comment]: # ({2d83699f-d428738a})
# 2 支持用户自定义宏的位置

[comment]: # ({/2d83699f-d428738a})

[comment]: # ({5eb64b93-f0afb8e0})
#### 概述

[用户自定义](/manual/config/macros/user_macros) 宏可以用于以下场景。

::: noteclassic
 *动作*,*网络发现*, *代理*以及本页 *其他位置*下列出的内容仅支持全局级用户宏。在这些提到的位置，主机级和模板级宏将不会被解析。
:::

[comment]: # ({/5eb64b93-f0afb8e0})

[comment]: # ({12c0698b-7cb4c86b})
#### 动作

在 [动作](/manual/config/notifications/action),中用户宏可用于以下位置：

|位置|<|多个宏/混合文本^[1](supported_by_location_user#footnotes)^|
|--------|-|------------------------------------------------------------------------|
|基于触发器的通知和命令|<|支持|
|基于触发器的内部通知|<|支持|
|问题更新通知|<|支持|
|基于服务的通知和命令|<|支持|
|服务更新通知|<|支持|
|时间段条件|<|不支持|
|*操作*|<|<|
|<|默认操作步骤持续时间|不支持|
|^|步骤持续时间|不支持|

[comment]: # ({/12c0698b-7cb4c86b})

[comment]: # ({2a98547e-f30645e0})
#### 主机/主机原型

在 [主机](/manual/config/hosts/host) 和 [主机原型](/manual/vm_monitoring#host_prototypes) 配置中,用户宏可用于以下字段:

|位置|<|多个宏/混合文本^[1](supported_by_location_user#footnotes)^|
|--------|-|------------------------------------------------------------------------|
|接口IP/DNS|<|仅支持DNS|
|接口端口|<|不支持|
|*SNMP v1, v2*|<|<|
|<|SNMP团体名|支持|
|*SNMP v3*|<|<|
|<|Context name|支持|
|^|Security name|支持|
|^|Authentication passphrase|支持|
|^|Privacy passphrase|支持|
|*IPMI*|<|<|
|<|用户名|支持|
|^|密码|支持|
|*标签*|<|<|
|<|标签名称|支持|
|^|标签值|支持|

[comment]: # ({/2a98547e-f30645e0})










[comment]: # ({76a77086-d51563d6})
#### 监控项 / 监控项原型

在 [监控项](/manual/config/items/item) 或 [监控项原型](/manual/discovery/low_level_discovery#item_prototypes)配置中,用户宏可用于以下字段：

|位置|<|多个宏/混合文本^[1](supported_by_location_user#footnotes)^|
|--------|-|------------------------------------------------------------------------|
|监控项键值参数|<|支持|
|更新间隔|<|不支持|
|自定义间隔|<|不支持|
|历史存储周期|<|不支持|
|趋势存储周期|<|不支持|
|描述|<|支持|
|*计算监控项*|<|<|
|<|公式|支持|
|*数据库监控*|<|<|
|<|用户名|支持|
|^|密码|支持|
|^|SQL查询|支持|
|*HTTP代理*|<|<|
|<|URL^[2](supported_by_location_user#footnotes)^|支持|
|^|查询字段|支持|
|^|超时时间|不支持|
|^|请求正文|支持|
|^|请求头部 (名字和值)|支持|
|^|返回状态码|支持|
|^|HTTP代理|支持|
|^|HTTP认证用户名|支持|
|^|HTTP认证密码|支持|
|^|SSl证书文件|支持|
|^|SSl密钥文件|
|^|SSl密钥密码|支持|
|^|允许的主机|支持|
|*JMX代理*|<|<|
|<|JMX端点|支持|
|*Script监控项*|<|<|
|<|参数名称和值|支持|
|*SNMP 代理*|<|<|
|<|SNMP OID|支持|
|*SSH 代理*|<|<|
|<|用户名|支持|
|^|公钥文件|支持|
|^|私钥文件|支持|
|^|密码|支持|
|^|脚本|支持|
|*TELNET 代理*|<|<|
|<|用户名|支持|
|^|密码|支持|
|^|脚本|支持|
|*Zabbix trapper*|<|<|
|<|允许的主机|支持|
|*标签*|<|<|
|<|标记名称|支持|
|^|标记值|支持|
|*预处理*|<|<|
|<|步骤参数（包括自定义脚本）|支持|

[comment]: # ({/76a77086-d51563d6})

[comment]: # ({a59da16e-747bd0ca})
#### 低级别自动发现

在[低级别自动发现规则](/manual/discovery/low_level_discovery#configuring_low-level_discovery)中用户宏可用于以下字段：

|位置|<|多个宏/混合文本^[1](supported_by_location_user#footnotes)^|
|--------|-|------------------------------------------------------------------------|
|键值参数|<|支持|
|更新间隔|<|不支持|
|自定义间隔|<|不支持|
|保持资源流失期|<|不支持|
|描述|<|支持|
|*SNMP 代理*|<|<|
|<|SNMP OID|支持|
|*SSH 代理*|<|<|
|<|用户名|支持|
|^|公钥文件|支持|
|^|私钥文件|支持|
|^|密码|支持|
|^|脚本|支持|
|*TELNET 代理*|<|<|
|<|用户名|支持|
|^|密码|支持|
|^|脚本|支持|
|*Zabbix trapper*|<|<|
|<|允许的主机|支持|
|*数据库监控*|<|<|
|<|附加参数|支持|
|*JMX 代理*|<|<|
|<|JMX 端点|支持|
|*HTTP 代理*|<|<|
|<|URL^[2](supported_by_location_user#footnotes)^|支持|
|^|查询字段|支持|
|^|超时时间|不支持|
|^|请求正文|支持|
|^|请求头部 (名字和值)|支持|
|^|返回状态码|支持|
|^|HTTP认证用户名|支持|
|^|HTTP认证密码|支持|
|*过滤器*|<|<|
|<|正则表达式|支持|
|*覆盖*|<|<|
|<|过滤器：正则表达式|支持|
|^|操作: 更新间隔 (对于监控项原型)|不支持|
|^|操作: 历史存储周期 (对于监控项原型)|不支持|
|^|操作: 趋势存储周期 (对于监控项原型)|不支持|

[comment]: # ({/a59da16e-747bd0ca})

[comment]: # ({c517cf76-95f2f372})
#### 网络发现

在 [网络发现规则](/manual/discovery/network_discovery/rule)中用户宏可用于以下字段：

|位置|<|多个宏/混合文本^[1](supported_by_location_user#footnotes)^|
|--------|-|------------------------------------------------------------------------|
|更新间隔|<|不支持|
|*SNMP v1, v2*|<|<|
|<|SNMP 团体名|支持|
|^|SNMP OID|支持|
|*SNMP v3*|<|<|
|<|Context name|支持|
|^|Security name|支持|
|^|Authentication passphrase|支持|
|^|Privacy passphrase|支持|
|^|SNMP OID|支持|

[comment]: # ({/c517cf76-95f2f372})

[comment]: # ({eeed8782-575d88fc})
####代理

在 [代理](/manual/distributed_monitoring/proxies#configuration)配置中用户宏可用于以下字段：

|位置|<|多个宏/混合文本^[1](supported_by_location_user#footnotes)^|
|--------|-|------------------------------------------------------------------------|
|接口端口（用于被动代理）|<|不支持|

[comment]: # ({/eeed8782-575d88fc})

[comment]: # ({5b382abb-32b6aef1})
#### 模板

在 [模板](/manual/config/templates/template) 配置中用户宏可用于以下字段：

|位置|<|多个宏/混合文本^[1](supported_by_location_user#footnotes)^|
|--------|-|------------------------------------------------------------------------|
|*标签*|<|<|
|<|标签名称|支持|
|^|标签值 |支持|

[comment]: # ({/5b382abb-32b6aef1})

[comment]: # ({64719378-c78b04ef})
#### 触发器

在 [触发器](/manual/config/triggers/trigger) 配置中用户宏可用于以下字段:

|位置|<|多个宏/混合文本^[1](supported_by_location_user#footnotes)^|
|--------|-|------------------------------------------------------------------------|
|名称|<|支持|
|操作数据|<|支持|
|表达式 (仅在常量和函数参数中; 不支持加密的宏).|<|支持|
|描述|<|支持|
|URL^[2](supported_by_location_user#footnotes)^|<|支持|
|匹配标签|<|支持|
|*标签*|<|<|
|<|标签名字|支持|
|^|标签值|支持|


[comment]: # ({/64719378-c78b04ef})

[comment]: # ({3c0049cf-78f1f511})
#### Web 场景

在 [web 场景](/manual/web_monitoring)配置中用户宏可用于以下字段:

|位置|<|多个宏/混合文本^[1](supported_by_location_user#footnotes)^|
|--------|-|------------------------------------------------------------------------|
|名称|<|支持|
|更新间隔|<|不支持|
|代理|<|支持|
|HTTP 代理|<|支持|
|变量 (只允许值)|<|支持|
|请求头部 (名称和值)|<|支持|
|*步骤*|<|<|
|<|名称|支持|
|^|URL^[2](supported_by_location_user#footnotes)^|支持|
|^|变量 (只允许值)|支持|
|^|请求头部 (名称和值)|支持|
|^|超时时间|不支持|
|^|返回字符串|支持|
|^|返回状态码|不支持|
|*认证*|<|<|
|<|用户|支持|
|^|密码|支持|
|^|SSL 证书|支持|
|^|SSL 密钥文件|支持|
|^|SSL 密钥密码|支持|
|*标签*|<|<|
|<|标签名称|支持|
|^|标签值|支持|

[comment]: # ({/3c0049cf-78f1f511})

[comment]: # ({8db0f379-efe84e1d})
#### 其他位置

除了以上列出的位置外，用户宏还可用于以下字段：

|位置|<|多个宏/混合文件^[1](supported_by_location_user#footnotes)^|
|--------|-|------------------------------------------------------------------------|
|全局脚本(脚本, SSH, Telnet, IPMI),包括确认文本|<|支持|
|Webhooks|<|<|
|<|JavaScript 脚本|不支持|
|<|JavaScript 脚本参数名称|不支持|
|<|JavaScript 脚本参数值|支持|
|*监控 → 仪表盘*|<|<|
|<|*监控项键值* 仪表板组件的描述字段|支持|
|<|*动态 URL* 仪表板组件的URL^[2](supported_by_location_user#footnotes)^字段|支持|
|*管理 → 用户 → 媒介*|<|<|
|<|接收邮件时间|不支持|
|*管理 → 常规 → GUI*|<|<|
|<|运行时间|不支持|
|*管理 → 媒介类型 → 信息模板*|<|<|
|<|主题|支持|
|^|信息|支持|

有关 Zabbix 支持的所有宏的完整列表，请参阅 [支持的宏](/manual/appendix/macros/supported_by_location)。

[comment]: # ({/8db0f379-efe84e1d})

[comment]: # ({40d47165-5a370e78})
##### 附注

^**1**^ 如果该位置不支持字段中的多个宏或与混合文本的宏，则必须使用单个宏填充整个字段。

^**2**^ URLs 包含[秘密宏](/manual/config/macros/user_macros#configuration) 将不起作用，因为其中的宏将被解析为 "\*\*\*\*\*\*"。

[comment]: # ({/40d47165-5a370e78})
