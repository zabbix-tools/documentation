[comment]: # translation:outdated

[comment]: # ({new-5c86be0b})
# 18. Web 界面

[comment]: # ({/new-5c86be0b})

[comment]: # ({new-8e039e89})
#### Overview

For an easy access to Zabbix from anywhere and from any platform, the
web-based interface is provided.

::: noteclassic
Trying to access two Zabbix frontend installations on the
same host, on different ports, simultaneously will fail. Logging into
the second one will terminate the session on the first one - unless the
default frontend session name is adjusted for the second frontend in
frontend [definitions](/manual/web_interface/definitions) (see
ZBX\_SESSION\_NAME).
:::

[comment]: # ({/new-8e039e89})

[comment]: # ({new-278be5f6})
### Frontend help

A help link ![](../../assets/en/manual/web_interface/help_link.png) is provided in Zabbix frontend forms with direct links to the corresponding parts of the documentation.

[comment]: # ({/new-278be5f6})
