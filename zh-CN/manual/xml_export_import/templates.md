[comment]: # translation:outdated

[comment]: # ({new-e8fbaf7c})
# 2 模板

[comment]: # ({/new-e8fbaf7c})

[comment]: # ({new-02fec82c})
#### 概述

模板就是 [导出](/manual/xml_export_import)
的许多相关联的对象和对象关系。

模板导出包含的内容：

-   链接的主机组
-   模板数据
-   到其他模板的链接
-   到主机组的链接
-   直接链接的应用集
-   直接链接的监控项
-   直接链接的触发器
-   直接链接的图形
-   直接链接的聚合图形
-   直接链接的带有所有原型的发现规则
-   直接链接的web场景
-   值映射

[comment]: # ({/new-02fec82c})

[comment]: # ({new-ca3e35f8})
#### 导出

要导出模板，按照如下的操作：

-   切换到：*配置（Configuration）* → *模板（Templates）*
-   选中要导出模板的复选框
-   单击列表下面的 *导出（Export）* 按钮

![](../../../assets/en/manual/xml_export_import/export_templates.png)

选中的模板被导出到本地的XML文件里，默认的名称是
*zabbix\_export\_templates.xml*。

[comment]: # ({/new-ca3e35f8})

[comment]: # ({new-78f6f939})
#### 导入

要导入模板，按照如下的操作：

-   切换到：*配置（Configuration）* → *模板（Templates）*
-   单击右上角的 *导入（Import）* 按钮
-   选择要导入的文件
-   标记导入规则里要求的选项
-   单击 *导入（Import）* 按钮

![](../../../assets/en/manual/xml_export_import/import_templates.png)

所有必填输入字段都标有红色星号。

导入成功或者失败的消息都会在前端页面显示。

导入规则：

|规则                              说|<|
|---------------------------------------|-|
|*更新现有的（Update existing）*   已有的元素会|导入文件里的数据更新，否则不会更新。|
|*创建新的（Create new）*          导入会使用|入文件的里数据增加新的元素，否则不会增加。|
|*删除不存在（Delete missing）*    导入会删除已|的但是在导入文件里没有的元素，否则不会删除。|

[comment]: # ({/new-78f6f939})

[comment]: # ({new-c04b1514})
#### 导出格式

``` {.xml}
<?xml version="1.0" encoding="UTF-8"?>
<zabbix_export>
    <version>4.0</version>
    <date>2018-04-03T06:25:22Z</date>
    <groups>
        <group>
            <name>Templates/Databases</name>
        </group>
    </groups>
    <templates>
        <template>
            <template>Template DB MySQL</template>
            <name>Template DB MySQL</name>
            <description/>
            <groups>
                <group>
                    <name>Templates/Databases</name>
                </group>
            </groups>
            <applications>
                <application>
                    <name>MySQL</name>
                </application>
            </applications>
            <items>
                <item>
                    <name>MySQL status</name>
                    <type>0</type>
                    <snmp_community/>
                    <snmp_oid/>
                    <key>mysql.ping</key>
                    <delay>1m</delay>
                    <history>1w</history>
                    <trends>365d</trends>
                    <status>0</status>
                    <value_type>3</value_type>
                    <allowed_hosts/>
                    <units/>
                    <snmpv3_contextname/>
                    <snmpv3_securityname/>
                    <snmpv3_securitylevel>0</snmpv3_securitylevel>
                    <snmpv3_authprotocol>0</snmpv3_authprotocol>
                    <snmpv3_authpassphrase/>
                    <snmpv3_privprotocol>0</snmpv3_privprotocol>
                    <snmpv3_privpassphrase/>
                    <params/>
                    <ipmi_sensor/>
                    <authtype>0</authtype>
                    <username/>
                    <password/>
                    <publickey/>
                    <privatekey/>
                    <port/>
                    <description>It requires user parameter mysql.ping, which is defined in userparameter_mysql.conf.

0 - MySQL server is down
1 - MySQL server is up</description>
                    <inventory_link>0</inventory_link>
                    <applications>
                        <application>
                            <name>MySQL</name>
                        </application>
                    </applications>
                    <valuemap>
                        <name>Service state</name>
                    </valuemap>
                    <logtimefmt/>
                    <preprocessing/>
                    <jmx_endpoint/>
                    <timeout>3s</timeout>
                    <url/>
                    <query_fields/>
                    <posts/>
                    <status_codes>200</status_codes>
                    <follow_redirects>1</follow_redirects>
                    <post_type>0</post_type>
                    <http_proxy/>
                    <headers/>
                    <retrieve_mode>0</retrieve_mode>
                    <request_method>1</request_method>
                    <output_format>0</output_format>
                    <allow_traps>0</allow_traps>
                    <ssl_cert_file/>
                    <ssl_key_file/>
                    <ssl_key_password/>
                    <verify_peer>0</verify_peer>
                    <verify_host>0</verify_host>
                    <master_item/>
                </item>
                <item>
                    <name>MySQL begin operations per second</name>
                    <type>0</type>
                    <snmp_community/>
                    <snmp_oid/>
                    <key>mysql.status[Com_begin]</key>
                    <delay>1m</delay>
                    <history>1w</history>
                    <trends>365d</trends>
                    <status>0</status>
                    <value_type>0</value_type>
                    <allowed_hosts/>
                    <units>qps</units>
                    <snmpv3_contextname/>
                    <snmpv3_securityname/>
                    <snmpv3_securitylevel>0</snmpv3_securitylevel>
                    <snmpv3_authprotocol>0</snmpv3_authprotocol>
                    <snmpv3_authpassphrase/>
                    <snmpv3_privprotocol>0</snmpv3_privprotocol>
                    <snmpv3_privpassphrase/>
                    <params/>
                    <ipmi_sensor/>
                    <authtype>0</authtype>
                    <username/>
                    <password/>
                    <publickey/>
                    <privatekey/>
                    <port/>
                    <description>It requires user parameter mysql.status[*], which is defined in userparameter_mysql.conf.</description>
                    <inventory_link>0</inventory_link>
                    <applications>
                        <application>
                            <name>MySQL</name>
                        </application>
                    </applications>
                    <valuemap/>
                    <logtimefmt/>
                    <preprocessing>
                        <step>
                            <type>10</type>
                            <params/>
                        </step>
                    </preprocessing>
                    <jmx_endpoint/>
                    <timeout>3s</timeout>
                    <url/>
                    <query_fields/>
                    <posts/>
                    <status_codes>200</status_codes>
                    <follow_redirects>1</follow_redirects>
                    <post_type>0</post_type>
                    <http_proxy/>
                    <headers/>
                    <retrieve_mode>0</retrieve_mode>
                    <request_method>1</request_method>
                    <output_format>0</output_format>
                    <allow_traps>0</allow_traps>
                    <ssl_cert_file/>
                    <ssl_key_file/>
                    <ssl_key_password/>
                    <verify_peer>0</verify_peer>
                    <verify_host>0</verify_host>
                    <master_item/>
                </item>
                <item>
                    <name>MySQL queries per second</name>
                    <type>0</type>
                    <snmp_community/>
                    <snmp_oid/>
                    <key>mysql.status[Questions]</key>
                    <delay>1m</delay>
                    <history>1w</history>
                    <trends>365d</trends>
                    <status>0</status>
                    <value_type>0</value_type>
                    <allowed_hosts/>
                    <units>qps</units>
                    <snmpv3_contextname/>
                    <snmpv3_securityname/>
                    <snmpv3_securitylevel>0</snmpv3_securitylevel>
                    <snmpv3_authprotocol>0</snmpv3_authprotocol>
                    <snmpv3_authpassphrase/>
                    <snmpv3_privprotocol>0</snmpv3_privprotocol>
                    <snmpv3_privpassphrase/>
                    <params/>
                    <ipmi_sensor/>
                    <authtype>0</authtype>
                    <username/>
                    <password/>
                    <publickey/>
                    <privatekey/>
                    <port/>
                    <description>It requires user parameter mysql.status[*], which is defined in userparameter_mysql.conf.</description>
                    <inventory_link>0</inventory_link>
                    <applications>
                        <application>
                            <name>MySQL</name>
                        </application>
                    </applications>
                    <valuemap/>
                    <logtimefmt/>
                    <preprocessing>
                        <step>
                            <type>10</type>
                            <params/>
                        </step>
                    </preprocessing>
                    <jmx_endpoint/>
                    <timeout>3s</timeout>
                    <url/>
                    <query_fields/>
                    <posts/>
                    <status_codes>200</status_codes>
                    <follow_redirects>1</follow_redirects>
                    <post_type>0</post_type>
                    <http_proxy/>
                    <headers/>
                    <retrieve_mode>0</retrieve_mode>
                    <request_method>1</request_method>
                    <output_format>0</output_format>
                    <allow_traps>0</allow_traps>
                    <ssl_cert_file/>
                    <ssl_key_file/>
                    <ssl_key_password/>
                    <verify_peer>0</verify_peer>
                    <verify_host>0</verify_host>
                    <master_item/>
                </item>
            </items>
            <discovery_rules/>
            <httptests/>
            <macros/>
            <templates/>
            <screens>
                <screen>
                    <name>MySQL performance</name>
                    <hsize>2</hsize>
                    <vsize>1</vsize>
                    <screen_items>
                        <screen_item>
                            <resourcetype>0</resourcetype>
                            <width>500</width>
                            <height>200</height>
                            <x>0</x>
                            <y>0</y>
                            <colspan>1</colspan>
                            <rowspan>1</rowspan>
                            <elements>0</elements>
                            <valign>1</valign>
                            <halign>0</halign>
                            <style>0</style>
                            <url/>
                            <dynamic>0</dynamic>
                            <sort_triggers>0</sort_triggers>
                            <resource>
                                <name>MySQL operations</name>
                                <host>Template DB MySQL</host>
                            </resource>
                            <max_columns>3</max_columns>
                            <application/>
                        </screen_item>
                    </screen_items>
                </screen>
            </screens>
        </template>
    </templates>
    <triggers>
        <trigger>
            <expression>{Template DB MySQL:mysql.ping.last(0)}=0</expression>
            <recovery_mode>0</recovery_mode>
            <recovery_expression/>
            <name>MySQL is down</name>
            <correlation_mode>0</correlation_mode>
            <correlation_tag/>
            <url/>
            <status>0</status>
            <priority>2</priority>
            <description/>
            <type>0</type>
            <manual_close>0</manual_close>
            <dependencies/>
            <tags/>
        </trigger>
    </triggers>
    <graphs>
        <graph>
            <name>MySQL operations</name>
            <width>900</width>
            <height>200</height>
            <yaxismin>0.0000</yaxismin>
            <yaxismax>100.0000</yaxismax>
            <show_work_period>1</show_work_period>
            <show_triggers>1</show_triggers>
            <type>0</type>
            <show_legend>1</show_legend>
            <show_3d>0</show_3d>
            <percent_left>0.0000</percent_left>
            <percent_right>0.0000</percent_right>
            <ymin_type_1>0</ymin_type_1>
            <ymax_type_1>0</ymax_type_1>
            <ymin_item_1>0</ymin_item_1>
            <ymax_item_1>0</ymax_item_1>
            <graph_items>
                <graph_item>
                    <sortorder>0</sortorder>
                    <drawtype>0</drawtype>
                    <color>C8C800</color>
                    <yaxisside>0</yaxisside>
                    <calc_fnc>2</calc_fnc>
                    <type>0</type>
                    <item>
                        <host>Template DB MySQL</host>
                        <key>mysql.status[Com_begin]</key>
                    </item>
                </graph_item>
            </graph_items>
        </graph>
    </graphs>
    <value_maps>
        <value_map>
            <name>Service state</name>
            <mappings>
                <mapping>
                    <value>0</value>
                    <newvalue>Down</newvalue>
                </mapping>
                <mapping>
                    <value>1</value>
                    <newvalue>Up</newvalue>
                </mapping>
            </mappings>
        </value_map>
    </value_maps>
</zabbix_export>
```

[comment]: # ({/new-c04b1514})

[comment]: # ({new-2c61d3f8})
#### 元素标签

元素标签值的释义在下面的表格中。

[comment]: # ({/new-2c61d3f8})

[comment]: # ({new-bdfd38fc})
##### 模板标签

|元素           元|属性      类型|范围   说明|<|<|
|--------------------|------------------|---------------|-|-|
|templates|<|<|<|模板的根元素。|
|template|<|<|<|单独的模板。|
|<|template|`字符`|唯|模板名称。|
|<|name|`字符`|显|模板名称。|
|<|description|`文本`|模|描述。|
|groups|<|<|<|主机组根元素。|
|group|<|<|<|单独的主机组。|
|<|name|`字符`|唯|主机组名称。|
|applications|<|<|<|模板应用集的根元素。|
|application|<|<|<|单独的模板应用集。|
|<|name|<|<|应用集名称。|
|macros|<|<|<|模板用户宏的根元素。|
|macro|<|<|<|单独的模板用户宏。|
|<|name|<|<|用户宏名称。|
|<|value|<|<|用户宏的值。|
|templates|<|<|<|链接模板的根元素。|
|template|<|<|<|单独的模板。|
|<|name|`字符`|模|名称。|

[comment]: # ({/new-bdfd38fc})

[comment]: # ({new-18631c1b})
##### 模板监控项标签

|元素            元|属性                 类型|范围|说明|<|
|---------------------|-----------------------------|------|------|-|
|items|<|<|<|监控项的根元素。|
|item|<|<|<|单独的监控项。|
|<|name|`字符`|监|项名称。|
|<|type|`整型`   0|- Zabbix agent\                                                          监1 - SNMPv1 agent<br>2 - Zabbix trapper<br>3 - simple check<br>4 - SNMPv2 agent<br>5 - internal<br>6 - SNMPv3 agent<br>7 - Zabbix agent (active)<br>8 - aggregate<br>9 - HTTP test (web monitoring scenario step)<br>10 - external<br>11 - database monitor<br>12 - IPMI agent<br>13 - SSH agent<br>14 - Telnet agent<br>15 - calculated<br>16 - JMX agent<br>17 - SNMP trap<br>18 - Dependent item<br>19 - HTTP agent item|项类型。|
|<|snmp\_community|`字符`|如|'type'的值是1或4，那这就是SNMP的团体名称。|
|<|snmp\_oid|`字符`|S|MP对象ID。|
|<|key|`字符`|监|项的key。|
|<|delay|`字符`|监|项的更新间隔。 秒，带后缀的时间单位，自定义间隔，用户宏或者低级别发现宏。|
|<|history|`字符`|决|历史数据存储时长的时间单位。带后缀的时间单位，用户宏或者低级别发现宏。|
|<|trends|`字符`|决|趋势数据存储时长的时间单位。带后缀的时间单位，用户宏或者低级别发现宏。|
|<|status|`整型`   0|- enabled\                                                               监1 - disabled|项状态。|
|<|value\_type|`整型`   0|- float\                                                                 收1 - character<br>2 - log<br>3 - unsigned integer<br>4 - text|值的类型。|
|<|allowed\_hosts|`字符`|如|'type'是2或者19，那这就是允许发送该监控项对应值的主机IP地址（逗号分隔）列表。|
|<|units|`字符`|返|值的单位（bps, B）。|
|<|snmpv3\_contextname|`字符`|S|MPv3上下文名称。|
|<|snmpv3\_securityname|`字符`|S|MPv3安全名称。|
|<|snmpv3\_securitylevel|`整型`   0|- noAuthNoPriv\                                                          S1 - authNoPriv<br>2 - authPriv|MPv3安全级别。|
|<|snmpv3\_authprotocol|`整型`   0|- MD5\                                                                   S1 - SHA|MPv3认证协议。|
|<|snmpv3\_authpassphrase|`字符`|S|MPv3认证密码。|
|<|snmpv3\_privprotocol|`整型`   0|- DES\                                                                   S1 - AES|MPv3私有协议。|
|<|snmpv3\_privpassphrase|`字符`|S|MPv3私有密码。|
|<|params|`文本`|如|'type'是13、14，这就是"执行脚本（Executed script）"的名称。<br>如果'type'是11，这就是"SQL查询（SQL query）"字段。<br>如果'type'是15，这是"公式（Formula）"字段。|
|<|ipmi\_sensor|`字符`|如|'type'是12，这是IPMI传感器ID。|
|<|authtype|`整型`   S|H客户端监控项的认证类型：\                                               如果'type'是13或者0 - password<br>1 - key<br><br>HTTP监控项认证类型：<br>0 - none<br>1 - basic<br>2 - NTLM|9，这是认证类型。|
|<|username|`字符`|如|'type'是11、13、14、19，这是用户名。|
|<|password|`字符`|如|'type'是11、13、14、19，这是密码。|
|<|publickey|`字符`|如|'type'是13，这是公共秘钥文件的名称。|
|<|privatekey|`字符`|如|'type'是13，这是私有密钥文件的名称。|
|<|port|`字符`|监|项的自定义端口。|
|<|description|`文本`|监|项描述。|
|<|inventory\_link|`整型`   0|- 无链接\                                                                使用监控*数字（number）* - 'host\_inventory'表里的字段数。|值来填充资产记录字段。|
|<|logtimefmt|`字符`|日|条目的时间格式。只有日志监控项使用。|
|<|jmx\_endpoint|`字符`|如|'type'是16，这是JMX端点。|
|<|url|`字符`|如|'type'是19，这是URL字符。|
|<|allow\_traps|`整型`   0|- 不允许trapping。\                                                      如果'ty1 - 允许trapping。|e'是19，属性允许发送数据给监控项。|
|<|follow\_redirects|`整型`   0|- 不跟随重定向。\                                                        如果'type'1 - 跟随重定向。|19，跟随HTTP重定向。|
|<|headers|`对象`|如|'type'是19，这是带有HTTP(S)请求头的对象。|
|<|http\_proxy|`字符`|如|'type'是19，这是HTTP(S)代理连接字符。|
|<|output\_format|`整型`   0|- 保持原样存储。\                                                        如果'type'1 - 转换为JSON。|19，怎样处理响应。|
|<|post\_type|`整形`   0|- 原始数据。\                                                            如果'typ2 - JSON数据。<br>3 - XML数据。|'是19，这是请求体的类型。|
|<|posts|`文本`|如|'type'是19，这是请求体。|
|<|query\_fields|`数组`|如|'type'是19，请求查询字段的对象数组。|
|<|request\_method|`整型`   0|- GET\                                                                   如1 - POST<br>2 - PUT<br>3 - HEAD|'type'是19，这是请求方法。|
|<|retrieve\_mode|`整型`   0|- 请求体\                                                                如果't1 - 请求头。<br>2 - 请求体和请求头都被存储。|pe'是19，响应的什么部分将被存储。|
|<|ssl\_cert\_file|`字符`|如|'type'是19，这是公共SSL密钥文件的路径。|
|<|ssl\_key\_file|`字符`|如|'type'是19，这是私有SSLK密钥文件的路径。|
|<|ssl\_key\_password|`字符`|如|'type'是19，这是SSL密钥文件的密码。|
|<|status\_codes|`字符`|如|'type'是19，这是逗号分隔的HTTP请求的状态码范围。|
|<|timeout|`字符`|如|'type'是19，监控项数据拉取请求的超时时间。|
|<|verify\_host|`整型`   0|- 不校验。\                                                              如果'ty1 - 校验。|e'是19，校验URL里的主机名是否在常见名称字段里，或者是否在主机证书的主题备用名称里。|
|<|verify\_peer|`整型`   0|- 不校验。\                                                              如果'ty1 - 校验。|e'是19，校验是否是主机证书验证。|
|value map|<|<|<|值映射。|
|<|name|`字符`|监|项使用的值映射名称。|
|applications|<|<|<|应用集的根元素。|
|application|<|<|<|单独的应用集。|
|<|name|<|<|应用集名称。|
|preprocessing|<|<|<|监控项值预处理。|
|step|<|<|<|单独的监控项值预处理步骤。|
|<|type|`整型`   1|- 自定义放大倍数\                                                        监控项值预处理步2 - 右截断<br>3 - 左截断<br>4 - 两端截断<br>5 - 正则表达式匹配<br>6 - 二进制到十进制<br>7 - 八进制到十进制<br>8 - 十六进制到十进制<br>9 - 简单改变； 计算为 (收到的值-之前的值)<br>10 - 每秒改变； 计算为 (当前收到的值-上一个值)/(当前时间-上一次检查时间)|的类型|
|<|params|`字符`|监|项值预处理步骤的参数。|
|master\_item|<|<|<|Individual item master item data.|
|<|key|`字符`|从|监控项的主监控项值。|

[comment]: # ({/new-18631c1b})

[comment]: # ({new-2fdd304e})
##### 模板低级别发现规则标签

|元素                      元|属性                                                                                           类型|范围|说明|<|
|-------------------------------|-------------------------------------------------------------------------------------------------------|------|------|-|
|discovery\_rules|<|<|<|低级别发现规则的根元素。|
|discovery\_rule|<|<|<|单独的低级别发现规则。|
|<|*对于大部分的元素标签值来说，请查阅常规监控项的元素标签值。下面仅描述低级别发现规则特有的标签。*|<|<|<|
|<|lifetime|`字符`|时|周期，监控项超过此时间不再被发现的话将被删除。秒，带有后缀的时间单位或者用户宏。|
|filter|<|<|<|单独的过滤条件。|
|<|evaltype|`整型`   0|- 与/或 逻辑\   检查低级别1 - 与 逻辑<br>2 - 或 逻辑<br>3 - 自定义公式|现规则过滤条件的逻辑。|
|<|formula|`字符`|过|条件的自定义计算公式。|
|<|conditions|<|<|过滤条件的根元素。|
|condition|<|<|<|单独的过滤器条件。|
|<|macro|`字符`|低|别发现宏变量名称。|
|<|value|`字符`|过|器值：正则表达式或者全局正则表达式。|
|<|operator|`整型`|<|<|
|<|formulaid|`字符`|过|条件ID。 在自定义计算公式里用到。|
|item\_prototypes|<|<|<|监控项原型的根元素。|
|item\_prototype|<|<|<|单独的监控项原型。|
|<|*对于大部分元素标签值来说，请查阅常规监控项的元素标签值。下面仅描述监控项原型特有的标签。*|<|<|<|
|application\_prototypes|<|<|<|应用集原型的根元素。|
|application\_prototype|<|<|<|单独的应用集原型。|
|<|name|<|<|应用集原型名称。|
|master\_item\_prototype|<|<|<|单独的监控项原型主监控项原型数据。|
|<|key|`字符`|单|的监控项原型主监控项原型键值。|

[comment]: # ({/new-2fdd304e})

[comment]: # ({new-8aada697})
##### 模板触发器标签

|元素           元|属性               类型|范围|说明|<|
|--------------------|---------------------------|------|------|-|
|triggers|<|<|<|触发器的根元素。|
|trigger|<|<|<|单独的触发器。|
|<|expression|`字符`|触|器表达式。|
|<|recovery\_mode|`整型`   0|- 表达式\            生成OK1 - 恢复表达式<br>2 - none|件的基础。|
|<|recovery\_expression|`字符`|触|器恢复表达式。|
|<|name|`字符`|触|器名称。|
|<|correlation\_mode|`整型`   0|- 没有事件关联\      关联模式。1 - 按标签的事件关联|<|
|<|correlation\_tag|`字符`|事|关联使用的标签名称.|
|<|url|`字符`|触|器 URL。|
|<|status|`整型`   0|- enabled\           触1 - disabled|器状态。|
|<|priority|`整型`   0|- 未分类\            触发器严1 - 信息<br>2 - 警告<br>3 - 一般严重<br>4 - 严重<br>5 - 灾难|性。|
|<|description|`文本`|触|器描述。|
|<|type|`整型`   0|- 单个问题事件\      事件生成类型。1 - 多个问题事件|<|
|<|manual\_close|`整型`   0|- 不允许\            手工关闭1 - 允许|题事件。|
|dependencies|<|<|<|依赖性的根元素|
|dependency|<|<|<|单独的依赖性。|
|<|name|`字符`|依|触发的名称。|
|<|expression|`字符`|依|触发器的表达式。|
|<|recovery\_expression|`字符`|依|触发器的恢复表达式。|
|tags|<|<|<|事件标签的根元素。|
|tag|<|<|<|单独的事件标签。|
|<|tag|`字符`|标|名称。|
|<|value|`字符`|标|值。|

[comment]: # ({/new-8aada697})

[comment]: # ({new-eec993d5})
##### 模板图形标签

|元素           元|属性             类型|范围|说明|<|
|--------------------|-------------------------|------|------|-|
|graphs|<|<|<|图形的根元素。|
|graph|<|<|<|单独的图形。|
|<|name|`字符`|图|名称。|
|<|width|`整型`|用|素表示的图形宽度。饼图/爆炸图和预览使用。|
|<|height|`整型`|用|素表示的图形高度。饼图/爆炸图和预览使用。|
|<|yaxismin|`双精度`|如果|ymin\_type\_1'是1，那么这是Y轴的最小值。|
|<|yaxismax|`双精度`|如果|ymax\_type\_1'是1，那么这是Y轴的最大值。|
|<|show\_work\_period|`整型`     0|- no\                                                        如1 - yes|'type'是0、1，突显非工作日。|
|<|show\_triggers|`整型`     0|- no\                                                        如1 - yes|'type'是0、1，以线条方式显示简单的触发器值。|
|<|type|`整型`     0|- 正常\                                                      图形类1 - 层积的<br>2 - 饼图<br>3 - 爆炸图<br>4 - 3D饼图<br>5 - 3D爆炸图|。|
|<|show\_legend|`整型`     0|- no\                                                        显1 - yes|图形图例。|
|<|show\_3d|`整型`     0|- 2D\                                                        如1 - 3D|'type'是2、3，启用3D风格。|
|<|percent\_left|`双精度`|如果|type'是0，显示左轴的百分位线。|
|<|percent\_right|`双精度`|如果|type'是0，显示右轴的百分位线。|
|<|ymin\_type\_1|`整型`     0|- 计算值\                                                    如果't1 - 固定值<br>2 - 所选监控项的最新值|pe'是0、1，这是Y轴的最小值。|
|<|ymax\_type\_1|`整型`     0|- 计算值\                                                    如果't1 - 固定值<br>2 - 所选监控项的最新值|pe'是0、1，这是Y轴的最大值。|
|<|ymin\_item\_1|`字符`     n|ll或者监控项明细                                             如果'ymin<br>|type\_1'是2，这是监控项明细。|
|<|ymax\_item\_1|`字符`     n|ll或者监控项明细                                             如果'ymax<br>|type\_1'是2，这是监控项明细。|
|graph\_items|<|<|<|图形监控项的根元素。|
|graph\_item|<|<|<|单独的图形监控项。|
|<|sortorder|`整型`|绘|顺序。先画较小的值。可以用它来画线条，或者另一个图形监控项的后面（或者前面）。|
|<|drawtype|`整型`     0|- 单行\                                                      如果图1 - 填充区域<br>2 - 粗线<br>3 - 虚线<br>4 - 短划线|'type'是0，这是绘制风格。|
|<|color|`字符`|元|颜色 (6个符号，十六进制的)。|
|<|yaxisside|`整型`     0|- 左轴\                                                      如果图1 - 右轴|'type'是0、1，这是元素所属的Y轴位置(左或者右)。|
|<|calc\_fnc|`整型`     1|- 最小值\                                                    如果监控2 - 平均值<br>4 - 最大值<br>7 - 所有值(如果图形'type'是0，这是最小值，平均值和最大值。)<br>9 - 最新值 (如果图形'type'不是0和1)|有多个值存在，将要绘制的数据。|
|<|type|`整型`     1|- 监控项的值按照比例绘制在饼图里。\                          饼图/爆炸图的绘制类型。2 - 监控项的值代表整个饼图(图形求和)|<|
|item|<|<|<|单独的监控项。|
|<|host|`字符`|监|项的主机。|
|<|key|`字符`|监|项的键。|

[comment]: # ({/new-eec993d5})

[comment]: # ({new-fd8f09c1})
##### 模板web场景标签

|元素        元|属性             类型|范围|说明|<|
|-----------------|-------------------------|------|------|-|
|httptests|<|<|<|web场景的根元素。|
|httptest|<|<|<|单独的web场景。|
|<|name|`字符`|w|b场景名称。|
|<|delay|`字符`|执|web场景的频率。秒，带有后缀的时间单位或者用户宏。|
|<|attempts|`整型`   1|10             执|Web场景步骤的尝试次数。|
|<|agent|`字符`|客|端agent。Zabbix假装是所选的浏览器。当网站为不同的浏览器返回不同的内容的时候，这很有用处。|
|<|http\_proxy|`字符`|指|要使用的HTTP代理，使用这个格式： `http://[username[:password]@]proxy.mycompany.com[:port]`|
|<|variables|`文本`|场|列表-可以在场景步骤中使用的级别变量（宏）。|
|<|headers|`文本`|当|行请求的时候，要发送的HTTP头部。|
|<|status|`整型`   0|- enabled\     w1 - disabled|b场景状态。|
|<|authentication|`整型`   0|- none\        认1 - basic<br>2 - NTLM|方法。|
|<|http\_user|`字符`|认|用户名。|
|<|http\_password|`字符`|指|用户名的认证密码。|
|<|verify\_peer|`整型`   0|- no\          校1 - yes|web服务器的SSL证书。|
|<|verify\_host|`整型`   0|- no\          校1 - yes|Web服务器证书的Common Name字段或Subject Alternate Name字段是否匹配。|
|<|ssl\_cert\_file|`字符`|客|端认证用到的SSL证书文件的名称。|
|<|ssl\_key\_file|`字符`|客|端认证用到的SSL私钥文件的名称。|
|<|ssl\_key\_password|`字符`|S|L私钥文件密码。|
|steps|<|<|<|web场景步骤的根元素。|
|step|<|<|<|单独的web场景步骤。|
|<|name|`字符`|w|b场景步骤名称。|
|<|url|`字符`|要|控的URL。|
|<|posts|`文本`|'|ost'变量的列表。|
|<|variables|`文本`|步|列表-这个步骤后面要应用到的级别变量（宏）。<br><br>如果变量值有'regex:'前缀，那么它的值将从按照'regex:'前缀后面的正则表达式模式而返回的数据里提取。|
|<|headers|`文本`|当|行请求的时候，发送的HTTP头部。|
|<|follow\_redirects|`整型`   0|- no\          跟1 - yes|HTTP跳转。|
|<|retrieve\_mode|`整型`   0|- 内容\        HTT1 - 仅HTTP头部|响应检索模式。|
|<|timeout|`字符`|执|步骤的超时时间。秒，带后缀的时间单位或者用户宏。|
|<|required|`字符`|必|字符。如果为空则忽略。|
|<|status\_codes|`字符`|逗|分隔的可接受的状态码列表。如果为空则忽略。例如：200-201,210-299。|

[comment]: # ({/new-fd8f09c1})



[comment]: # ({new-51ede6fc})
##### Template dashboard tags

|Element|Element property|Required|Type|Range^**[1](#footnotes)**^|Description|
|-------|----------------|--------|----|--------------------------|-----------|
|dashboards|<|\-|<|<|Root element for template dashboards.|
|<|uuid|x|`string`|<|Unique identifier for this dashboard.|
|<|name|x|`string`|<|Template dashboard name.|
|<|display period|\-|`integer`|<|Display period of dashboard pages.|
|<|auto\_start|\-|`string`|0 - no<br>1 - yes|Slideshow auto start.|
|pages|<|\-|<|<|Root element for template dashboard pages.|
|<|name|\-|`string`|<|Page name.|
|<|display period|\-|`integer`|<|Page display period.|
|<|sortorder|\-|`integer`|<|Page sorting order.|
|widgets|<|\-|<|<|Root element for template dashboard widgets.|
|<|type|x|`string`|<|Widget type.|
|<|name|\-|`string`|<|Widget name.|
|<|x|\-|`integer`|0-23|Horizontal position from the left side of the template dashboard.|
|<|y|\-|`integer`|0-62|Vertical position from the top of the template dashboard.|
|<|width|\-|`integer`|1-24|Widget width.|
|<|height|\-|`integer`|2-32|Widget height.|
|<|hide\_header|\-|`string`|0 - no<br>1 - yes|Hide widget header.|
|fields|<|\-|<|<|Root element for the template dashboard widget fields.|
|<|type|x|`string`|0 - INTEGER<br>1 - STRING<br>3 - HOST<br>4 - ITEM<br>5 - ITEM\_PROTOTYPE<br>6 - GRAPH<br>7 - GRAPH\_PROTOTYPE|Widget field type.|
|<|name|x|`string`|<|Widget field name.|
|<|value|x|mixed|<|Widget field value, depending on the field type.|

[comment]: # ({/new-51ede6fc})

[comment]: # ({new-869bd76e})
##### Footnotes

^**1**^ For string values, only the string will be exported (e.g.
"ZABBIX\_ACTIVE") without the numbering used in this table. The numbers
for range values (corresponding to the API values) in this table is used
for ordering only.

[comment]: # ({/new-869bd76e})
