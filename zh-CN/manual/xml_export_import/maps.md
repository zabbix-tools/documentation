[comment]: # translation:outdated

[comment]: # ({new-c7c352d1})
# 4 网络拓扑图

[comment]: # ({/new-c7c352d1})

[comment]: # ({new-f69156a2})
#### 概述

网络拓扑图 [导出（export）](/manual/xml_export_import) 包含：

-   所有相关的图片
-   拓扑图结构 -
    所有拓扑图设置，所有包含元素及其设置，拓扑图链接和拓扑图链接状态指示器

未导出的是主机组，主机，触发器，其他拓扑图或可能与导出的拓扑图相关的任何其他元素。
因此，如果缺少拓扑图所引用的元素中的任何一个，导入将失败。

自Zabbix 1.8.2起支持网络拓扑图导出/导入。

[comment]: # ({/new-f69156a2})

[comment]: # ({new-bc82aec8})
#### 导出

要导出网络拓扑图，请执行以下操作：

-   切换到： *检测中（Monitoring）* → *拓扑图（Maps）*
-   标记要导出的网络拓扑图的复选框
-   单击列表下方的*导出（Export）*按钮

![](../../../assets/en/manual/xml_export_import/export_maps.png)

选中的拓扑图以默认名称 *zabbix\_export\_maps.xml*
导出到本地的XML文件里。

[comment]: # ({/new-bc82aec8})

[comment]: # ({new-cc31311c})
#### 导入

要导入网络拓扑图，请执行以下操作：

-   切换到： *监测中（Monitoring）* → *拓扑图（Maps）*
-   点击右侧的*导入（Import）*按钮
-   选择导入文件
-   在导入规则中标记所需选项
-   单击*导入（Import）*按钮

![](../../../assets/en/manual/xml_export_import/import_maps.png)

所有必填输入字段都标有红色星号。

导入成功或失败的消息将显示在前端。

导入规则：

|规则                              说|<|
|---------------------------------------|-|
|*更新现有的（Update existing）*   将使用从导入|件中获取的数据更新现有拓扑图。 否则他们将不会更新。|
|*创建新的（Create new）*          导入将使用|入文件中的数据添加新拓扑图。 否则它不会添加它们。|

如果取消选中所有拓扑图选项并检查图像的相应选项，则仅导入图像。
图像导入仅适用于Zabbix Super Admin用户。

<note
warning>如果替换现有图像，则会影响使用此图像的所有拓扑图。
:::

[comment]: # ({/new-cc31311c})

[comment]: # ({new-53418cf1})
#### 导出格式

导出一个包含三个元素的小型网络图，它们的图像和它们之间的一些链接。
请注意，图像被清空以节省空间。

``` {.xml}
<?xml version="1.0" encoding="UTF-8"?>
<zabbix_export>
    <version>4.0</version>
    <date>2016-10-05T08:16:20Z</date>
    <images>
        <image>
            <name>Server_(64)</name>
            <imagetype>1</imagetype>
            <encodedImage>iVBOR...SuQmCC</encodedImage>
        </image>
        <image>
            <name>Workstation_(64)</name>
            <imagetype>1</imagetype>
            <encodedImage>iVBOR...SuQmCC</encodedImage>
        </image>
        <image>
            <name>Zabbix_server_3D_(96)</name>
            <imagetype>1</imagetype>
            <encodedImage>iVBOR...ggg==</encodedImage>
        </image>
    </images>
    <maps>
        <map>
            <name>Network</name>
            <width>590</width>
            <height>400</height>
            <label_type>0</label_type>
            <label_location>0</label_location>
            <highlight>1</highlight>
            <expandproblem>0</expandproblem>
            <markelements>1</markelements>
            <show_unack>0</show_unack>
            <severity_min>2</severity_min>
            <grid_size>40</grid_size>
            <grid_show>1</grid_show>
            <grid_align>1</grid_align>
            <label_format>0</label_format>
            <label_type_host>2</label_type_host>
            <label_type_hostgroup>2</label_type_hostgroup>
            <label_type_trigger>2</label_type_trigger>
            <label_type_map>2</label_type_map>
            <label_type_image>2</label_type_image>
            <label_string_host/>
            <label_string_hostgroup/>
            <label_string_trigger/>
            <label_string_map/>
            <label_string_image/>
            <expand_macros>0</expand_macros>
            <background/>
            <iconmap/>
            <urls/>
            <selements>
                <selement>
                    <elementtype>0</elementtype>
                    <label>Host 1</label>
                    <label_location>-1</label_location>
                    <x>476</x>
                    <y>28</y>
                    <elementsubtype>0</elementsubtype>
                    <areatype>0</areatype>
                    <width>200</width>
                    <height>200</height>
                    <viewtype>0</viewtype>
                    <use_iconmap>0</use_iconmap>
                    <selementid>8</selementid>
                    <elements>
                        <element>
                            <host>Discovered host</host>
                        </element>
                    </elements>
                    <icon_off>
                        <name>Server_(64)</name>
                    </icon_off>
                    <icon_on/>
                    <icon_disabled/>
                    <icon_maintenance/>
                    <application/>
                    <urls/>
                </selement>
                <selement>
                    <elementtype>0</elementtype>
                    <label>Zabbix server</label>
                    <label_location>-1</label_location>
                    <x>252</x>
                    <y>50</y>
                    <elementsubtype>0</elementsubtype>
                    <areatype>0</areatype>
                    <width>200</width>
                    <height>200</height>
                    <viewtype>0</viewtype>
                    <use_iconmap>0</use_iconmap>
                    <selementid>6</selementid>
                    <elements>
                        <element>
                            <host>Zabbix server</host>
                        </element>
                    </elements>
                    <icon_off>
                        <name>Zabbix_server_3D_(96)</name>
                    </icon_off>
                    <icon_on/>
                    <icon_disabled/>
                    <icon_maintenance/>
                    <application/>
                    <urls/>
                </selement>
                <selement>
                    <elementtype>0</elementtype>
                    <label>New host</label>
                    <label_location>-1</label_location>
                    <x>308</x>
                    <y>230</y>
                    <elementsubtype>0</elementsubtype>
                    <areatype>0</areatype>
                    <width>200</width>
                    <height>200</height>
                    <viewtype>0</viewtype>
                    <use_iconmap>0</use_iconmap>
                    <selementid>7</selementid>
                    <elements>
                        <element>
                            <host>Zabbix host</host>
                        </element>
                    </elements>
                    <icon_off>
                        <name>Workstation_(64)</name>
                    </icon_off>
                    <icon_on/>
                    <icon_disabled/>
                    <icon_maintenance/>
                    <application/>
                    <urls/>
                </selement>
            </selements>
            <links>
                <link>
                    <drawtype>0</drawtype>
                    <color>008800</color>
                    <label/>
                    <selementid1>6</selementid1>
                    <selementid2>8</selementid2>
                    <linktriggers/>
                </link>
                <link>
                    <drawtype>2</drawtype>
                    <color>00CC00</color>
                    <label>100MBps</label>
                    <selementid1>7</selementid1>
                    <selementid2>6</selementid2>
                    <linktriggers>
                        <linktrigger>
                            <drawtype>0</drawtype>
                            <color>DD0000</color>
                            <trigger>
                                <description>Zabbix agent on {HOST.NAME} is unreachable for 5 minutes</description>
                                <expression>{Zabbix host:agent.ping.nodata(5m)}=1</expression>
                                <recovery_expression/>
                            </trigger>
                        </linktrigger>
                    </linktriggers>
                </link>
            </links>
        </map>
    </maps>
</zabbix_export>
```

[comment]: # ({/new-53418cf1})

[comment]: # ({new-efe541b5})
#### 元素标签

元素标签值在下表中说明。

|元素                元|属性                   类型|范围|说明|<|
|-------------------------|-------------------------------|------|------|-|
|images|<|<|<|图像的根元素。|
|image|<|<|<|单独的图像。|
|<|name|`字符`|唯|图像名称。|
|<|imagetype|`整型`   1|- 图像\                          图像类2 - 背景|。|
|<|encodedImage|<|<|Base64编码图像。|
|maps|<|<|<|拓扑图的根元素。|
|map|<|<|<|单独的拓扑图。|
|<|name|`字符`|唯|拓扑图名称。|
|<|width|`整型`|拓|图宽度，以像素为单位。|
|<|height|`整型`|拓|图高度，以像素为单位。|
|<|label\_type|`整型`   0|- 标签\                          拓扑图1 - 主机IP地址<br>2 - 元素名称<br>3 - 仅状态<br>4 - 无|素标签类型。|
|<|label\_location|`整型`   0|- 底部\                          默认情1 - 左<br>2 - 右<br>3 - 顶部|下拓扑图元素标签位置。|
|<|highlight|`整型`   0|- no\                            为1 - yes|动触发器和主机状态启用图标突出显示。|
|<|expandproblem|`整型`   0|- no\                            显1 - yes|具有单个问题的元素的问题触发器。|
|<|markelements|`整型`   0|- no\                            突1 - yes|显示最近更改其状态的拓扑图元素。|
|<|show\_unack|`整型`   0|- 所有问题的数量\                问题显示。1 - 未确认问题的数量<br>2 - 分别统计已确认和未确认的问题|<|
|<|severity\_min|`整型`   0|- 未分类\                        默认情况1 - 信息<br>2 - 警告<br>3 - 一般严重<br>4 - 严重<br>5 - 灾难|显示在拓扑图上的最小触发严重性。|
|<|grid\_size|`整型`   2|, 40, 50, 75 或者 100            如果“|rid\_show = 1”，这是拓扑图网格的单元格大小（以像素为单位）。|
|<|grid\_show|`整型`   0|- yes\                           在1 - no|扑图配置中显示网格。|
|<|grid\_align|`整型`   0|- yes\                           在1 - no|扑图配置中自动对齐图标。|
|<|label\_format|`整型`   0|- no\                            使1 - yes|高级标签配置。|
|<|label\_type\_host|`整型`   0|- 标签\                          如果“1 - 主机IP地址<br>2 - 元素名称<br>3 - 仅状态<br>4 - 无<br>5 - 自定义标签|abel\_format = 1”，则显示为主机标签。|
|<|label\_type\_hostgroup|`整型`   0|- 标签\                          如果“2 - 元素名称<br>3 - 仅状态<br>4 - 无<br>5 - 自定义标签|abel\_format = 1”，则显示为主机组标签|
|<|label\_type\_trigger|`整型`   0|- 标签\                          如果“2 - 元素名称<br>3 - 仅状态<br>4 - 无<br>5 - 自定义标签|abel\_format = 1”，则显示为触发器标签|
|<|label\_type\_map|`整型`   0|- 标签\                          如果“2 - 元素名称<br>3 - 仅状态<br>4 - 无<br>5 - 自定义标签|abel\_format = 1”，则显示为拓扑图标签|
|<|label\_type\_image|`整型`   0|- 标签\                          显示为2 - 元素名称<br>4 - 无<br>5 - 自定义标签|像标签，如果“label\_format = 1”|
|<|label\_string\_host|`字符`|如|“label\_type\_host = 5”，这是主机元素的自定义标签。|
|<|label\_string\_hostgroup|`字符`|如|“label\_type\_hostgroup = 5”，这是主机组元素的自定义标签。|
|<|label\_string\_trigger|`字符`|如|“label\_type\_trigger = 5”，这是触发元素的自定义标签。|
|<|label\_string\_map|`字符`|如|“label\_type\_map = 5”，则是拓扑图元素的自定义标签|
|<|label\_string\_image|`字符`|如|“label\_type\_image = 5”，则是图像元素的自定义标签|
|<|expand\_macros|`字符`   0|- no\                            在1 - yes|扑图配置中展开标签中的宏。|
|<|background|`id`|<|如果“imagetype = 2”，则是背景图像的ID（如果有）|
|<|iconmap|`id`|<|图标映射的ID（如果有）。|
|urls|<|<|<|<|
|url|<|<|<|单独的URL。|
|<|name|`字符`|链|名称。|
|<|url|`字符`|链|URL。|
|<|elementtype|`整型`   0|- 主机\                          链接所1 - 拓扑图<br>2 - 触发器<br>3 - 主机组<br>4 - 图像|的拓扑图监控项类型。|
|selements|<|<|<|<|
|selement|<|<|<|单独的拓扑图元素|
|<|elementtype|`整型`   0|- 主机\                          拓扑图1 - 拓扑图<br>2 - 触发器<br>3 - 主机组<br>4 - 图像|素类型。|
|<|label|`字符`|图|标签。|
|<|label\_location|`整型`   -|- 使用拓扑图默认<br>0 - 底部<br>1 - 左<br>2 - 右<br>3 - 顶部|<|
|<|x|`整型`|X|上的位置。|
|<|y|`整型`|Y|上的位置。|
|<|elementsubtype|`整型`   0|- 单个主机组\                    如果“Ele1 - 所有主机组|entType=3”，则是元素子类型|
|<|areatype|`整型`   0|- 与整个拓扑图相同\              如果“elemen1 - 自定义大小|subtype = 1”，则是区域大小|
|<|width|`整型`|如|“areatype = 1”，则是面积宽度|
|<|height|`整型`|如|“areatype = 1”，则是面积高度|
|<|viewtype|`整型`   0|- 均匀地放在该区域               如果“elemen|subtype = 1”，则是区域放置算法|
|<|use\_iconmap|`整型`   0|- no\                            使1 - yes|此元素的图标映射。 仅在拓扑图级别激活图标映射时才相关。|
|<|selementid|`id`|<|唯一元素记录ID。|
|<|application|`字符`|应|集名称过滤器。 如果给出了应用集程序名称，则只会在拓扑图上显示属于给定应用集程序的触发器问题。|
|elements|<|<|<|<|
|element|<|<|<|在拓扑图上表示的单个Zabbix实体（拓扑图，主机组，主机等）。|
|<|host|<|<|<|
|icon\_off|<|<|<|元素处于“正常”状态时使用的图像。|
|icon\_on|<|<|<|元素处于“问题”状态时使用的图像。|
|icon\_disabled|<|<|<|禁用元素时要使用的图像。|
|icon\_maintenance|<|<|<|元素处于维护状态时使用的图像。|
|<|name|`字符`|唯|的图像名称。|
|links|<|<|<|<|
|link|<|<|<|拓扑图元素之间的个别链接。|
|<|drawtype|`整型`   0|- 线条\                          线条类2 - 粗线条<br>3 - 虚线<br>4 - 中划线|。|
|<|color|`字符`|链|颜色（6个符号，十六进制）。|
|<|label|`字符`|链|标签。|
|<|selementid1|`id`|<|要连接的一个元素的ID。|
|<|selementid2|`id`|<|要连接的其他元素的ID。|
|linktriggers|<|<|<|<|
|linktrigger|<|<|<|单独的链接状态指示灯。|
|<|drawtype|`整型`   0|- 线条\                          触发器2 - 粗线条<br>3 - 虚线<br>4 - 中划线|于“问题”状态时的链接样式。|
|<|color|`字符`|当|发器处于“问题”状态时，链接颜色（6个符号，十六进制）。|
|trigger|<|<|<|触发器用于指示链路状态。|
|<|description|`字符`|触|器名称。|
|<|expression|`字符`|触|器表达式。|
|<|recovery\_expression|`字符`|触|器恢复表达式。|

[comment]: # ({/new-efe541b5})
