[comment]: # translation:outdated

[comment]: # ({new-4151d28b})
# 12. 正则表达式\[杨青\]

[comment]: # ({/new-4151d28b})

[comment]: # ({new-17ed5ab5})
#### 概述

[Perl Compatible Regular
Expressions](https://en.wikipedia.org/wiki/Perl_Compatible_Regular_Expressions)
(PCRE) are supported in Zabbix.

Zabbix支持 [Perl Compatible Regular
Expressions](https://en.wikipedia.org/wiki/Perl_Compatible_Regular_Expressions)
(PCRE)。

There are two ways of using regular expressions in Zabbix:

在Zabbix中有两种使用正则表达式的方法：

-   manually entering a regular expression
-   using a global regular expression created in Zabbix
-   手动输入正则表达式
-   使用在Zabbix中创建的全局正则表达式

[comment]: # ({/new-17ed5ab5})

[comment]: # ({new-439895e1})
#### Regular expressions

[comment]: # ({/new-439895e1})

[comment]: # ({new-e24754dc})
#### 正则表达式

You may manually enter a regular expression in supported places. Note
that the expression may not start with @ because that symbol is used in
Zabbix for referencing global regular expressions.

可以在支持的位置手动输入正则表达式。请注意，表达式可能不以\@开头，因为该符号在Zabbix中用于引用全局正则表达式。

[comment]: # ({/new-e24754dc})

[comment]: # ({new-2ea2d961})
#### Global regular expressions

[comment]: # ({/new-2ea2d961})

[comment]: # ({new-4c860844})
#### 全局正则表达式

There is an advanced editor for creating and testing complex regular
expressions in Zabbix frontend.

在Zabbix前端，有一个高级的编辑器用于创建和测试复杂的正则表达式。

Once a regular expression has been created this way, it can be used in
several places in the frontend by referring to its name, prefixed with
@, for example, *\@mycustomregexp*.

一旦以这种方式创建了正则表达式，它就可以在前端的几个地方使用，方法是加个\@前缀来引用它的名称，例如，*\@mycustomregexp*。

To create a global regular expression:

要创建全局正则表达式：

-   Go to: *Administration → General*
-   Select *Regular expressions* from the dropdown
-   Click on *New regular expression*
-   切换到： *管理（Administration） → 一般（General）*
-   从右上角的下拉列表中选择 *正则表达式（Regular expressions）*
-   单击 *新的正则表达式（New regular expression）*

The **Regular expressions** tab allows to set the regular expression
name and add subexpressions.

**正则表达式（Regular
expressions）**选项卡允许设置正则表达式名称并添加子表达式。

![](../../assets/en/manual/regular_expressions/regexp_expr.png){width="600"}

All mandatory input fields are marked with a red asterisk.

所有必填输入字段都标有红色星号。

|Parameter|<|Description|
|---------|-|-----------|
|*Name*|<|Set the regular expression name. Any Unicode characters are allowed.|
|*Expressions*|<|Click on *Add* in the Expressions block to add a new subexpression.|
|<|*Expression type*|Select expression type:<br>**Character string included** - match the substring<br>**Any character string included** - match any substring from a delimited list. The delimited list includes a comma (,), a dot (.) or a forward slash (/).<br>**Character string not included** - match any string except the substring<br>**Result is TRUE** - match the regular expression<br>**Result is FALSE** - do not match the regular expression|
|<|*Expression*|Enter substring/regular expression.|
|*Delimiter*|<|A comma (,), a dot (.) or a forward slash (/) to separate text strings in a regular expression. This parameter is active only when "*Any character string included*" expression type is selected.|
|*Case sensitive*|<|A checkbox to specify whether a regular expression is sensitive to capitalization of letters.|

|参数|说|<|
|------|---|-|
|*名称（Name）*|设置正|表达式名称。 允许使用任何Unicode字符。|
|*表达式（Expressions）*|单击表达|块中的 *添加（Add）* 以添加新的子表达式。|
|<|*表达式类型（Expression type）*   选择表达式类|：<br>**字符串已包含（Character string included）** - 匹配子符子串<br>**包括任何字符串（Any character string included）** - 匹配列表里包含的字符子串。匹配分隔列表中的任何子字符串。分隔列表包括逗号（,），点号（.）或正斜杠（/）。<br>**字符串未包含（Character string not included）** - 匹配除此以外的任何字符串<br>**结果为真（Result is TRUE）** - 匹配正则表达式<br>**结果为假（Result is FALSE）** - 不匹配正则表达式|
|<|*表达式（Expression）*            输入子字|串/正则表达式。|
|*分隔符（Delimiter）*|用逗号（|），点号（.）或正斜杠（/）分隔正则表达式中的文本字符串。仅当选择”*包括任何字符串（Any character string included）*“表达式类型时，此参数才有效。|
|*区分大小写（Case sensitive）*|此复选框用于|定正则表达式是否对字母大小写敏感。|

Since Zabbix 2.4.0, a forward slash (/) in the expression is treated
literally, rather than a delimiter. This way it is possible to save
expressions containing a slash, whereas previously it would produce an
error.

从Zabbix
2.4.0开始，表达式中的正斜杠（/）按字面意思处理，而不是分隔符。这样就可以保存包含斜杠的表达式，而以前会产生错误。

::: noteimportant
A custom regular expression name in Zabbix may
contain commas, spaces, etc. In those cases where that may lead to
misinterpretation when referencing (for example, a comma in the
parameter of an item key) the whole reference may be put in quotes like
this: *"\@My custom regexp for purpose1, purpose2"*.\
Regular expression names must not be quoted in other locations (for
example, in LLD rule properties).
:::

<note
important>Zabbix里自定义的表达式名称可以包含逗号，空格等。在引用时可能导致误解的情况下（例如，监控项键的参数中的逗号），整个引用可以放在引号中，如下所示：//
" \@My custom regexp for purpose1, purpose2"//。\
不能在其他位置引用正则表达式名称（例如，在LLD规则属性中）。
:::

[comment]: # ({/new-4c860844})

[comment]: # ({new-eb858ac0})
#### Example

[comment]: # ({/new-eb858ac0})

[comment]: # ({new-be96a07d})
#### 举例

Use of the following regular expression in LLD to discover databases not
taking into consideration a database with a specific name:

在LLD中使用以下正则表达式来发现不考虑具有特定名称的数据库的数据库：

    ^TESTDATABASE$

![regexp\_expr\_2.png](../../assets/en/manual/regular_expressions/regexp_expr_2.png)

Chosen *Expression type*: "Result is FALSE". Doesn't match name,
containing string "*TESTDATABASE*".

选择 *表达式类型（Expression type）*： "结果为假（Result is
FALSE）"。不匹配名称，包含字符串“*TESTDATABASE*”。

[comment]: # ({/new-be96a07d})

[comment]: # ({new-489130e5})
#### Example with an inline regex modifier

[comment]: # ({/new-489130e5})

[comment]: # ({new-56f9b2c2})
#### 内联正则表达式修饰符的示例

Use of the following regular expression including an inline modifier
(?i) to match the characters "error":

使用如下带有内联修饰符（?i）的正则表达式匹配“error”字符：

    (?i)error

![regexp\_expr\_3a.png](../../assets/en/manual/regular_expressions/regexp_expr_3a.png)

Chosen *Expression type*: "Result is TRUE". Characters "error" are
matched.

选择 *表达式类型（Expression type）*： "结果为真（Result is
TRUE）"。“error”字符被匹配到。

#### Another example with an inline regex modifier

#### 内联正则表达式修饰符的另一个示例

Use of the following regular expression including multiple inline
modifiers to match the characters after a specific line:

使用以下正则表达式（包括多个内联修饰符）来匹配特定行之后的字符：

    (?<=match (?i)everything(?-i) after this line\n)(?sx).*    #我们增加了一个修饰符(?s)来使点号（.）具备匹配换行符的能力。

::: notetip
(?x) 打开自由间隔模式。
::: <note
tip>(?s)
对于“单行模式”，使点号匹配所有字符，包括换行符。Ruby或JavaScript不支持。在Tcl中，(?s)使\^匹配字符串的开头，$匹配字符串的结尾。
:::

::: notetip
(?i) 使正则表达式不区分大小写。
::: <note
tip>(?-i)
减号后的所有模式修饰符都将被关闭。也就是说，只有everything是不区分大小写的。
:::

::: notetip
(?<= 在正则表达式里，我们称之为Positive
Lookbehind。它告诉正则表达式引擎在字符串中暂时向后退一步，以检查look
behind内的文本是否可以在那里匹配。
:::

::: noteclassic
所以，上面这个例子告诉我们，匹配match everything after this
line\\n后面的字符串，且只有everything不区分大小写，而且开启了(?sx)模式。
:::

![regexp\_expr\_4\_new.png](../../assets/en/manual/regular_expressions/regexp_expr_4_new.png)

Chosen Expression type: "Result is TRUE". Characters after a specific
line are matched.

选择表达式类型： "结果为真（Result is TRUE）"。匹配特定行后的字符。

::: noteimportant
 **g** modifier can't be specified in line. The
list of available modifiers can be found in [pcresyntax man
page](https://www.pcre.org/original/doc/html/pcresyntax.html#SEC16). For
more information about PCRE syntax please refer to [PCRE HTML
documentation](https://www.pcre.org/original/doc/html/pcrepattern.html).

:::

::: noteimportant
 **g**修饰符不能在行中指定。可用修饰符列表可以在
[pcresyntax man
page](https://www.pcre.org/original/doc/html/pcresyntax.html#SEC16)
里找到。如果想了解更多的PCRE正则表达式语法，请参考 [PCRE HTML
documentation](https://www.pcre.org/original/doc/html/pcrepattern.html)。

:::

#### More complex example

#### 更复杂的例子

A custom regular expression may consist of multiple subexpressions, and
it can be tested in the **Test** tab by providing a test string.

自定义正则表达式可能包含多个子表达式，可以通过提供测试字符串在**Test**选项卡中进行测试。

![](../../assets/en/manual/regular_expressions/regexp_test.png){width="600"}

Results show the status of each subexpression and total custom
expression status.

结果显示每个子表达式的状态和整个自定义表达式的状态。

Total custom expression status is defined as *Combined result*. If
several sub expressions are defined Zabbix uses AND logical operator to
calculate *Combined result*. It means that if at least one Result is
False *Combined result* has also False status.

总自定义表达式状态定义为 *合并的结果（Combined
result）*。如果定义了几个子表达式，Zabbix使用AND逻辑运算符来计算
*合并的结果（Combined
result）*。这意味着如果只要有一个结果为False，*合并的结果（Combined
result）* 也为False状态。

#### Explanation of global regular expressions

#### 全局正则表达式的说明

|Global regexp|Expression|Description|
|-------------|----------|-----------|
|*File systems for discovery*|`^(btrfs\|ext2\|ext3\|ext4\|jfs\|reiser\|xfs\|ffs\|ufs\|jfs\|jfs2\|vxfs\|hfs\|refs\|ntfs\|fat32\|zfs)$`|Matches "btrfs" or "ext2" or "ext3" or "ext4" or "jfs" or "reiser" or " xfs" or "ffs" or "ufs" or "jfs" or "jfs2" or "vxfs" or "hfs" or "refs" or "ntfs" or "fat32" or "zfs"|
|*Network interfaces for discovery*|`^Software Loopback Interface`|Matches strings starting with "Software Loopback Interface"|
|<|`^lo$`|Matches "lo"|
|<|`^(In)?[Ll]oop[Bb]ack[0-9._]*$`|Matches strings that optionally start with "In", then have "L" or "l", then "oop", then "B" or "b", then "ack", which can be optionally followed by any number of digits, dots or underscores|
|<|`^NULL[0-9.]*$`|Matches strings staring with "NULL" optionally followed by any number of digits or dots|
|<|`^[Ll]o[0-9.]*$`|Matches strings starting with "Lo" or "lo" and optionally followed by any number of digits or dots|
|<|`^[Ss]ystem$`|Matches "System" or "system"|
|<|`^Nu[0-9.]*$`|Matches strings staring with "Nu" optionally followed by any number of digits or dots|
|*Storage devices for SNMP discovery*|`^(Physical memory\|Virtual memory\|Memory buffers\|Cached memory\|Swap space)$`|Matches "Physical memory" or "Virtual memory" or "Memory buffers" or "Cached memory" or "Swap space"|
|*Windows service names for discovery*|`^(MMCSS\|gupdate\|SysmonLog\|clr_optimization_v2.0.50727_32\|clr_optimization_v4.0.30319_32)$`|Matches "MMCSS" or "gupdate" or "SysmonLog" or strings like "clr\_optimization\_v2.0.50727\_32" and "clr\_optimization\_v4.0.30319\_32" where instead of dots you can put any character except newline.|
|*Windows service startup states for discovery*|`^(automatic\|automatic delayed)$`|Matches "automatic" or "automatic delayed".|

|全局正则表达式                                                              表达式|说明|<|
|--------------------------------------------------------------------------------------------|------|-|
|*发现文件系统（File systems for discovery）*                                `^(btrf|\|ext2\|ext3\|ext4\|jfs\|reiser\|xfs\|ffs\|ufs\|jfs\|jfs2\|vxfs\|hfs\|refs\|ntfs\|fat32\|zfs)$`       匹配“btrf|”或“ext2”或“ext3”或“ext4”或“jfs”或“reiser”或“xfs”或“ffs”或“ufs”或“jfs”或“jfs2”或“vxfs”或“hfs“或”refs“或”ntfs“或”fat32“或”zfs“|
|*发现网络接口（Network interfaces for discovery）*                          `^Softw|re Loopback Interface`                                                                匹配以"Sof|ware Loopback Interface"开头的字符串|
|<|`^lo$`|匹配"lo"|
|<|`^(In)?[Ll]oop[Bb]ack[0-9._]*$`|匹配以 "In" 开头（该项可选），然后是"L"或者"l"字符，然后是"oop"，然后是"B"或者"b"，然后是"ack"，最后以任意长度（长度可能为0）的数字（0-9），点号（.）或者下划线（\_）结尾的字符串|
|<|`^NULL[0-9.]*$`|匹配以“NULL”开头的字符串，后面是任意长度（长度可能为0）的数字（0-9）或者点号（.）|
|<|`^[Ll]o[0-9.]*$`|匹配以"Lo"或者"lo"开头的字符串，后面是任意长度（长度可能为0）的数字（0-9）或者点号（.）|
|<|`^[Ss]ystem$`|匹配"System"或者"system"|
|<|`^Nu[0-9.]*$`|匹配以 "Nu" 开头的字符串，后面是任意长度（长度可能为0）的数字（0-9）或者点号（.）|
|*使用SNMP发现存储设备（Storage devices for SNMP discovery）*                `^(Physic|l memory\|Virtual memory\|Memory buffers\|Cached memory\|Swap space)$`                  匹配"Physic|l memory"或"Virtual memory"或"Memory buffers"或"Cached memory"或"Swap space"|
|*发现Windows服务名（Windows service names for discovery）*                  `^(MMC|S\|gupdate\|SysmonLog\|clr_optimization_v2.0.50727_32\|clr_optimization_v4.0.30319_32)$`   匹配“MMC|S”或“gupdate”或“SysmonLog”或类似“clr\_optimization\_v2.0.50727\_32”和“clr\_optimization\_v4.0.30319\_32”的字符串，而不是点号，可以放置除换行符之外的任何字符。|
|*发现Windows服务启动状态（Windows service startup states for discovery）*   `^(automa|ic\|automatic delayed)$`                                                             匹配"automa|ic"或"automatic delayed"。|

#### Regular expression support by location

#### 支持正则表达式的位置

|Location|<|Regular expression|Global regular expression|Comments|
|--------|-|------------------|-------------------------|--------|
|**[Agent items](/manual/config/items/itemtypes/zabbix_agent)**|<|<|<|<|
|<|eventlog\[\]|Yes|Yes|`regexp`, `severity`, `source`, `eventid` parameters|
|^|log\[\]|^|^|`regexp` parameter|
|^|log.count\[\]|^|^|^|
|^|logrt\[\]|^|Yes/No|`regexp` parameter supports both, `file_regexp` parameter supports non-global expressions only|
|^|logrt.count\[\]|^|^|^|
|^|proc.cpu.util\[\]|^|No|`cmdline` parameter|
|^|proc.mem\[\]|^|^|^|
|^|proc.num\[\]|^|^|^|
|^|sensor\[\]|^|^|`device` and `sensor` parameters on Linux 2.4|
|^|system.hw.macaddr\[\]|^|^|`interface` parameter|
|^|system.sw.packages\[\]|^|^|`package` parameter|
|^|vfs.dir.count\[\]|^|^|`regex_incl` and `regex_excl` parameters|
|^|vfs.dir.size\[\]|^|^|`regex_incl` and `regex_excl` parameters|
|^|vfs.file.regexp\[\]|^|^|`regexp` parameter|
|^|vfs.file.regmatch\[\]|^|^|^|
|^|web.page.regexp\[\]|^|^|^|
|**[SNMP traps](/manual/config/items/itemtypes/snmptrap)**|<|<|<|<|
|<|snmptrap\[\]|Yes|Yes|`regexp` parameter|
|**[Item value preprocessing](/manual/config/items/item#item_value_preprocessing)**|<|Yes|No|`pattern` parameter|
|**[Trigger functions](/manual/appendix/triggers/functions)**|<|<|<|<|
|<|count()|Yes|Yes|`pattern` parameter if `operator` parameter is *regexp* or *iregexp*|
|^|logeventid()|^|^|`pattern` parameter|
|^|iregexp()|^|^|^|
|^|regexp()|^|^|^|
|**[Low-level discovery](/manual/discovery/low_level_discovery#discovery_rule_filter)**|<|Yes|Yes|*Filter* field|
|**[Web monitoring](/manual/web_monitoring#configuring_a_web_scenario)**|<|Yes|No|*Variables* with a **regex:** prefix<br>*Required string* field|
|**[Macro functions](/manual/config/macros/macro_functions)**|<|<|<|<|
|<|regsub()|Yes|No|`pattern` parameter|
|^|iregsub()|^|^|^|
|**[Icon mapping](/manual/web_interface/frontend_sections/administration/general#icon_mapping)**|<|Yes|Yes|*Expression* field|

|位置|正|表达式   全局正则表达|注释|<|
|------|---|------------------------------|------|-|
|**[Agent监控项（Agent items）](/manual/config/items/itemtypes/zabbix_agent)**|<|<|<|<|
|<|eventlog\[\]|Yes|Yes|`regexp`, `severity`, `source`, `eventid` 参数|
|^|log\[\]|^|^|`regexp` 参数|
|^|log.count\[\]|^|^|^|
|^|logrt\[\]|^|Yes/No|`regexp` 参数两者都支持, `file_regexp` 参数仅支持非全局表达式|
|^|logrt.count\[\]|^|^|^|
|^|proc.cpu.util\[\]|^|No|`cmdline` 参数|
|^|proc.mem\[\]|^|^|^|
|^|proc.num\[\]|^|^|^|
|^|sensor\[\]|^|^|`device` 和 `sensor` 参数在Linux 2.4中|
|^|system.hw.macaddr\[\]|^|^|`interface` 参数|
|^|system.sw.packages\[\]|^|^|`package` 参数|
|^|vfs.dir.count\[\]|^|^|`regex_incl` 和 `regex_excl` 参数|
|^|vfs.dir.size\[\]|^|^|`regex_incl` 和 `regex_excl` 参数|
|^|vfs.file.regexp\[\]|^|^|`regexp` 参数|
|^|vfs.file.regmatch\[\]|^|^|^|
|^|web.page.regexp\[\]|^|^|^|
|**[SNMP traps](/manual/config/items/itemtypes/snmptrap)**|<|<|<|<|
|<|snmptrap\[\]|Yes|Yes|`regexp` 参数|
|**[监控项值预处理（Item value preprocessing）](/manual/config/items/item#item_value_preprocessing)**|Yes|No|`pattern|参数|
|**[触发器函数（Trigger functions）](/manual/appendix/triggers/functions)**|<|<|<|<|
|<|count()|Yes|Yes|`pattern` 参数，如果 `operator` 参数是 *regexp* 或者 *iregexp*|
|^|logeventid()|^|^|`pattern` 参数|
|^|iregexp()|^|^|^|
|^|regexp()|^|^|^|
|**[低级别发现（Low-level discovery）](/manual/discovery/low_level_discovery#discovery_rule_filter)**|Yes|Yes|*Filte|* 字段|
|**[Web监测（Web monitoring）](/manual/web_monitoring#configuring_a_web_scenario)**|Yes|No|*Va|iables* 带有 **regex:** 前缀<br>*Required string* 字段|
|**[宏函数（Macro functions）](/manual/config/macros/macro_functions)**|<|<|<|<|
|<|regsub()|Yes|No|`pattern` 参数|
|^|iregsub()|^|^|^|
|**[图标映射（Icon mapping）](/manual/web_interface/frontend_sections/administration/general#icon_mapping)**|Yes|Yes|*Expr|ssion* 字段|

[comment]: # ({/new-56f9b2c2})
