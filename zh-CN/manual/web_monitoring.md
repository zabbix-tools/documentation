[comment]: # translation:outdated

[comment]: # ({new-734ebac5})
# 9. Web 监控\[ZeMing\]

[comment]: # ({/new-734ebac5})

[comment]: # ({new-f37bc18c})
# 9. Web monitoring

[comment]: # ({/new-f37bc18c})

[comment]: # ({new-f007debe})
#### Overview

[comment]: # ({/new-f007debe})

[comment]: # ({new-74c8baab})
#### 概述

您可以使用 Zabbix 对多个网站进行可用性方面监控：\
With Zabbix you can check several availability aspects of web sites.\

::: noteimportant
若要使用Web监控，必须
编译[编译（configured）](/zh/manual/installation/install#from_the_sources)安装是
加入cURL (libcurl) 库支持
:::

::: noteimportant
To perform web monitoring Zabbix server must be
initially [configured](/manual/installation/install#from_the_sources)
with cURL (libcurl) support.
:::

要使用 Web 监控，您需要定义 web 场景。Web 场景包括一个或多个 HTTP
请求或“步骤”。Zabbix 服务器根据预定义的命令周期性的执行这些步骤。

To activate web monitoring you need to define web scenarios. A web
scenario consists of one or several HTTP requests or "steps". The steps
are periodically executed by Zabbix server in a pre-defined order. If a
host is monitored by proxy, the steps are executed by the proxy.

从 Zabbix2.2 开始，Web 场景和 Items，Triggers 等一样，是依附在
Hosts/Templates 的。这意味着 web
场景也可以创建一个模板，然后应用于多个主机。 Since Zabbix 2.2 web
scenarios are attached to hosts/templates in the same way as items,
triggers, etc. That means that web scenarios can also be created on a
template level and then applied to multiple hosts in one move.

所有的 web 场景会收集下列数据： 整个场景中所有步骤的平均下载速度
失败的步骤数量 最后一次错误信息 对于 web
场景的所有步骤，都会收集下列数据： 平均下载速度 响应时间 HTTP 状态码
更多详情，请参见 [web monitoring
items](/zh/manual/web_monitoring/items).

The following information is collected in any web scenario:

-   average download speed per second for all steps of whole scenario
-   number of the step that failed
-   last error message

The following information is collected in any web scenario step:

-   download speed per second
-   response time
-   response code

For more details, see [web monitoring
items](/manual/web_monitoring/items).

执行 web 场景收集的数据保存在数据库中。数据自动用于图形、触发器和通知。

Data collected from executing web scenarios is kept in the database. The
data is automatically used for graphs, triggers and notifications.

Zabbix 还支持获取 HTML
内容中是否存在设置的字符串。还可以模拟登陆动作和模拟鼠标单击。 Zabbix
can also check if a retrieved HTML page contains a pre-defined string.
It can execute a simulated login and follow a path of simulated mouse
clicks on the page.

Zabbix web 监控同时支持 HTTP 和 HTTPS。当运行 web 场景时，Zabbix
将选择跟踪重定向（请参见下面的选择跟踪重定向）。重定向硬编码的最大数量为
10 （使用 cURL 选项
[CURLOPT\_MAXREDIRS](http://curl.haxx.se/libcurl/c/CURLOPT_MAXREDIRS.html))。在执行
web 场景时，所有 Cookie 都会保存。

Zabbix web monitoring supports both HTTP and HTTPS. When running a web
scenario, Zabbix will optionally follow redirects (see option *Follow
redirects* below). Maximum number of redirects is hard-coded to 10
(using cURL option
[CURLOPT\_MAXREDIRS](http://curl.haxx.se/libcurl/c/CURLOPT_MAXREDIRS.html)).
All cookies are preserved during the execution of a single scenario.

web 监控使用 HTTPS 协议请参阅
[已知问题](/manual/installation/known_issues#https_checks) See also
[known issues](/manual/installation/known_issues#https_checks) for web
monitoring using HTTPS protocol.

[comment]: # ({/new-74c8baab})

[comment]: # ({new-6f40ea68})
#### 配置 Web 场景

[comment]: # ({/new-6f40ea68})

[comment]: # ({new-4cce2d90})
#### Configuring a web scenario

配置 web 场景： To configure a web scenario:

\* 转到：*配置 (Configuration)-→主机*（或者 *模板*)

-   Go to: *Configuration → Hosts* (or *Templates*)
-   点击主机 (host)/ 模板 (template) 行中的 *Web*
-   Click on *Web* in the row of the host/template
-   点击右上角 *创建 web 场景*（或点击场景名字进行编辑现有的场景）
-   Click on *Create scenario* to the right (or on the scenario name to
    edit an existing scenario)
-   在场景的表单中输入参数
-   Enter parameters of the scenario in the form

**场景**选项卡允许您配置此 Web 场景的通用参数。 The **Scenario** tab
allows you to configure the general parameters of a web scenario.

![?600](../../assets/en/manual/config/scenario.png)
![](../../assets/en/manual/config/scenario.png){width="600"}

All mandatory input fields are marked with a red asterisk.

场景参数： Scenario parameters:

场景参数：

|参数                                 说|<|
|------------------------------------------|-|
|*主机 (Host)*                        场|所属的主机名或模板的名字。|
|*名称 (Name)*                        唯|的场景名称。<br>Zabbix 2.2 开始，这个名字支持用户宏和 {HOST.\*} [宏](/manual/appendix/macros/supported_by_location) 。|
|*应用 (Application)*                 选|一个场景属于的应用 。<br>Web 场景监控项在 // 监测中 (Monitoring)→最新数据 (Latest data)// 栏中将会分组在选择的应用中。|
|*新的应用 (New application)*         对场景|建个新的应用。|
|*更新间隔 (Update interval) （秒)*   执行场景时|间隔，以秒为单位。|
|*重试次数 (Attempts)*                尝试执|web 场景中步骤的次数。对于网络问题（超时，没有连接，等等） Zabbix 可以多次重复执行步骤。这个数字对场景的中的所有步骤都会生效。尝试次数最大可以设置为 10，默认值为 1。<br>*注意*: Zabbix 不会因为一个错误的响应代码或者期望的字符串没有出现就会触发这个重试。<br>*Zabbix 2.2* 开始支持此参数。|
|*代理 (Agent)*                       选|一个客户端。<br>zabbix 会模拟选择的浏览器，当一个网站对不同的浏览器返回不同的内容的时候是非常有用的。<br>*zabbix 2.2* 开始 ，这块可以使用用户自定义宏。|
|*HTTP 代理 (HTTP proxy)*             您|以指定要使用一个 HTTP 代理，使用格式 *http://\[username\[:password\]@\]proxy.mycompany.com\[:port\]*<br>默认使用 1080 端口。<br>如果指定，代理将覆盖代理相关联的环境变量，比如 http\_proxy HTTPS\_PROXY。如果没有指定，那么代理将不会覆盖代理相关的环境变量。<br>输入的值是通过“是 (as is)”, 不需要进行完整性检查。你也可以输入 SOCKS 代理地址。如果您指定了错误的协议，连接会失败，项目将成为不受支持的。没有指定的协议，代理将被视为一个 HTTP 代理、\\ // 注意 *: HTTP 代理仅支持简单身份验证。<br>此字段中可以使用用户宏。<br>*Zabbix 2.2// 开始支持此参数。|
|// 变量 (Variables)//                可|在场景中的步骤（URL，POST 变量）中使用变量。<br>它们具有以下格式：<br>**{macro1}**=value1<br>**{macro2}**=value2<br>**{macro3}**=regex:<regular expression><br>例如：<br>{username}=Alexei<br>{password}=kj3h5kJ34bd<br>{hostid}=regex:hostid is (\[0-9\]+)<br>然后可以在{username}，{password}和{hostid}的步骤中引用宏。 Zabbix 将自动将其替换为实际值。请注意，使用`regex:`的变量：需要一个步骤来获取正则表达式的值，因此提取的值只能应用于后续步骤。<br>如果值部分以 *regex* 开头，那么它之后的部分将被视为正则表达式，将搜索网页，如果找到，则将匹配存储在变量中。 注意，必须存在至少一个子组，以便可以提取匹配的值。<br>*Zabbix 2.2* 开始支持变量中的正则表达式匹配。<br>*Zabbix 2.2* 开始，{HOST.\*} [宏](/manual/appendix/macros/supported_by_location) 和用户宏可以在此字段中使用。<br>在查询字段或提交表单数据时，变量会自动进行 URL 编码，但使用 raw 方式提交数据或者直接在 URL 中使用时，必须手动进行 URL 编码|
|*HTTP 头 (Headers)*|行请求时将发送的自定义的 HTTP headers。<br>应使用与在 HTTP 协议中出现的语法相同的语法列出标题，可选地使用 [CURLOPT\_HTTPHEADER](http://curl.haxx.se/libcurl/c/CURLOPT_HTTPHEADER.html) cURL 选项支持的一些其他功能。<br>例如：<br>Accept-Charset=utf-8<br>Accept-Language=en-US<br>Content-Type=application/xml; charset=utf-8<br>用户宏和 {HOST.\*} [宏](/manual/appendix/macros/supported_by_location) 和可以在此字段中使用。<br>从 *Zabbix 2.4* 开始支持指定自定义头。|
|// 启用 (Enabled)//                  如|选中此复选框，则此场景处于启用状态，否则禁用。|

|Parameter|Description|
|---------|-----------|
|*Host*|Name of the host/template that the scenario belongs to.|
|*Name*|Unique scenario name.<br>User macros and {HOST.\*} [macros](/manual/appendix/macros/supported_by_location) are supported, since Zabbix 2.2.|
|*Application*|Select an application the scenario will belong to.<br>Web scenario items will be grouped under the selected application in *Monitoring → Latest data*.|
|*New application*|Enter the name of a new application for the scenario.|
|*Update interval*|How often the scenario will be executed.<br>[Time suffixes](/manual/appendix/suffixes) are supported, e.g. 30s, 1m, 2h, 1d, since Zabbix 3.4.0.<br>[User macros](/manual/config/macros/user_macros) are supported, since Zabbix 3.4.0.<br>*Note* that if a user macro is used and its value is changed (e.g. 5m → 30s), the next check will be executed according to the previous value (farther in the future with the example values).|
|*Attempts*|The number of attempts for executing web scenario steps. In case of network problems (timeout, no connectivity, etc) Zabbix can repeat executing a step several times. The figure set will equally affect each step of the scenario. Up to 10 attempts can be specified, default value is 1.<br>*Note*: Zabbix will not repeat a step because of a wrong response code or the mismatch of a required string.<br>This parameter is supported starting with *Zabbix 2.2*.|
|*Agent*|Select a client agent.<br>Zabbix will pretend to be the selected browser. This is useful when a website returns different content for different browsers.<br>User macros can be used in this field, *starting with Zabbix 2.2*.|
|*HTTP proxy*|You can specify an HTTP proxy to use, using the format: *http://\[username\[:password\]@\]proxy.mycompany.com\[:port\]*<br>By default, 1080 port will be used.<br>If specified, the proxy will overwrite proxy related environment variables like http\_proxy, HTTPS\_PROXY. If not specified, the proxy will not overwrite proxy related environment variables.<br>The entered value is passed on "as is", no sanity checking takes place. You may also enter a SOCKS proxy address. If you specify the wrong protocol, the connection will fail and the item will become unsupported. With no protocol specified, the proxy will be treated as an HTTP proxy.<br>*Note*: Only simple authentication is supported with HTTP proxy.<br>User macros can be used in this field.<br>This parameter is supported starting with *Zabbix 2.2*.|
|*Variables*|Variables that may be used in scenario steps (URL, post variables).<br>They have the following format:<br>**{macro1}**=value1<br>**{macro2}**=value2<br>**{macro3}**=regex:<regular expression><br>For example:<br>{username}=Alexei<br>{password}=kj3h5kJ34bd<br>{hostid}=regex:hostid is (\[0-9\]+)<br>The macros can then be referenced in the steps as {username}, {password} and {hostid}. Zabbix will automatically replace them with actual values. Note that variables with `regex:` need one step to get the value of the regular expression so the extracted value can only be applied to the step after.<br>If the value part starts with `regex:` then the part after it is treated as a regular expression that searches the web page and, if found, stores the match in the variable. At least one subgroup must be present so that the matched value can be extracted.<br>Regular expression match in variables is supported *since Zabbix 2.2*.<br>User macros and {HOST.\*} [macros](/manual/appendix/macros/supported_by_location) are supported, since Zabbix 2.2.<br>Variables are automatically URL-encoded when used in query fields or form data for post variables, but must be URL-encoded manually when used in raw post or directly in URL.|
|*Headers*|Custom HTTP headers that will be sent when performing a request.<br>Headers should be listed using the same syntax as they would appear in the HTTP protocol, optionally using some additional features supported by the [CURLOPT\_HTTPHEADER](http://curl.haxx.se/libcurl/c/CURLOPT_HTTPHEADER.html) cURL option.<br>For example:<br>Accept-Charset=utf-8<br>Accept-Language=en-US<br>Content-Type=application/xml; charset=utf-8<br>User macros and {HOST.\*} [macros](/manual/appendix/macros/supported_by_location) are supported.<br>Specifying custom headers is supported *starting with Zabbix 2.4*.|
|*Enabled*|The scenario is active if this box is checked, otherwise - disabled.|

注意，当编辑一个现有的场景时，会出现两个额外的按钮：

Note that when editing an existing scenario, two extra buttons are
available in the form:

|   |   |
|---|---|
|![](../../assets/en/manual/web_monitoring/scenario_edit_clone.png)|基于现有的场景的属性创建另一个场景。|
|![](../../assets/en/manual/web_monitoring/scenario_edit_clear.png)|删除场景的历史记录和趋势数据。 这将使服务器在删除数据后立即执行场景。|

|   |   |
|---|---|
|![](../../assets/en/manual/web_monitoring/scenario_edit_clone.png)|Create another scenario based on the properties of the existing one.|
|![](../../assets/en/manual/web_monitoring/scenario_edit_clear.png)|Delete history and trend data for the scenario. This will make the server perform the scenario immediately after deleting the data.|

::: notetip
如果 *HTTP proxy* 字段留空，使用 HTTP
代理的另一种方法是设置代理相关的环境变量。

对于 HTTP 检查 - 为 Zabbix 服务器用户设置 **http\_proxy** 环境变量。
例如，
//http\_proxy=[http:%%//%%proxy\_ip:proxy\_port//](http:%%//%%proxy_ip:proxy_port//).

对于 HTTPS 检查 - 设置 **HTTPS\_PROXY** 环境变量。 例如，
*HTTPS\_PROXY=[http:%%//%%proxy\_ip:proxy\_port//](http:%%//%%proxy_ip:proxy_port//).
通过运行 shell 命令可以获得更多详细信息：*\# man curl//.
:::

::: notetip
If *HTTP proxy* field is left empty, another way for
using an HTTP proxy is to set proxy related environment variables.

For HTTP checks - set the **http\_proxy** environment variable for the
Zabbix server user. For example,
//http\_proxy=[http:%%//%%proxy\_ip:proxy\_port//](http:%%//%%proxy_ip:proxy_port//).

For HTTPS checks - set the **HTTPS\_PROXY** environment variable. For
example,
//HTTPS\_PROXY=[http:%%//%%proxy\_ip:proxy\_port//](http:%%//%%proxy_ip:proxy_port//).
More details are available by running a shell command: *\# man
curl*.
:::

“步骤”选项卡允许您配置 Web 场景步骤。 要添加 Web 场景步骤，请在 // 步骤
(Steps)// 单击 // 添加 (Add)//。

The **Steps** tab allows you to configure the web scenario steps. To add
a web scenario step, click on *Add* in the *Steps* block.

![](../../assets/en/manual/config/scenario2.png){width="600"}

[comment]: # ({/new-4cce2d90})

[comment]: # ({new-ae869848})
#### 配置步骤

[comment]: # ({/new-ae869848})

[comment]: # ({new-42ab5787})
#### Configuring steps

![](../../assets/en/manual/config/scenario_step.png){width="600"}

Step parameters:

步骤参数：

|参数                                      说|<|
|-----------------------------------------------|-|
|// 名称 (Name)//                          唯|步骤名称。<br>*Zabbix 2.2* 开始，该名称可以支持用户宏和 {HOST.\*} [宏](/manual/appendix/macros/supported_by_location)。|
|// 网址 (URL)//                           用|连接和检索数据的网址。 例如：<br>https://www.google.com<br>http://www.zabbix.com/download<br>Zabbix 3.4 以后，可以以 Unicode 编码指定域名。 执行 Web 场景步骤时，它们将自动被禁止转换为 ASCII。<br>// 解析 // 按钮可用于从 URL 中分离可选的查询字段（例如？name = Admin&password = mypassword），将属性和值放到查询字段以进行自动 URL 编码。<br>变量可以在 URL 中使用，使用 {macro} 语法。 变量可以使用 {{macro}.urlencode()} 语法手动进行 URL 编码。<br>*Zabbix 2.2* 开始，{HOST.\*} [宏](/manual/appendix/macros/supported_by_location) 和用户宏可以在此字段中使用。<br>*Zabbix 2.4* 开始，最多字符为 2048 个。|
|// 查询字段 (Query fields)// ?            URL|的 HTTP GET 变量。<br>指定属性和值对。<br>值将自动进行 URL 编码。来自场景变量，用户宏或{HOST。\*}宏的值将被解析，然后自动进行 URL 编码。 使用{{macro}.urlencode()}语法将对其进行双重 URL 编码。<br>从 *Zabbix 2.2* 开始开始支持用户宏和 {HOST.\*} [宏](/manual/appendix/macros/supported_by_location) 。|
|*Post*|HTTP POST 变量。<br>在 **Form data** 模式下，指定属性和值。<br>值被自动进行 URL 编码。 来自场景变量、用户宏或 {HOST.\*} 宏的值将被解析，然后自动进行 URL 编码。<br>在 **Raw data** 模式中，属性 / 值显示在一条线上，并与 & 符号连接。<br>Raw 方式的值可以使用 {{macro}.urlencode()} 或 {{macro}.urldecode()} 手动进行 URL 编码 / 解码。<br>例如：id=2345&userid={user}<br>如果 {user} 被定义为 web 场景的变量，则当执行步骤时，它的值会被替换。<br>如果你想对变量进行 URL 编码，用 {{user}.urlencode()} 替换 {user} 。<br>// Zabbix 2.2// 开始支持用户宏和 {HOST.\*} [宏](/manual/appendix/macros/supported_by_location) 。|
|// 变量 (Variables)//                     可|于 GET 和 POST 方法的步级变量。<br>指定属性和值。<br>步骤变量覆盖之前的场景变量或步骤变量。 然而，一个步骤变量的值仅影响之后的步骤（而不是当前步骤）。<br>它们具有以下格式：<br>**{macro}**=value<br>**{macro}**=regex:<regular expression><br>有关更多信息，请参阅 [场景 (scenario)](web_monitoring#configuring_a_web_scenario) 级别上的变量描述。<br>*Zabbix 2.2* 开始支持步骤变量。<br>*Zabbix 2.2* 开始，{HOST.\*} [宏](/manual/appendix/macros/supported_by_location) 和用户宏可以在此字段中使用。<br>在查询字段或提交表单数据时，变量会自动进行 URL 编码，但使用 raw 方式提交数据或者直接在 URL 中使用时，必须手动进行 URL 编码|
|*HTTP 头 (Headers)*|行请求时将发送的自定义 HTTP headers。<br>指定属性和值<br>步骤级别上的 headers 将覆盖为该场景指定的 headers。<br>例如，设置“User-Agent：”为空时，将覆盖在场景上设置的 User-Agent 名称。<br>支持用户宏和 {HOST.\*} 宏、\\ 这将设置 [CURLOPT\_HTTPHEADER](http://curl.haxx.se/libcurl/c/CURLOPT_HTTPHEADER.html) cURL 选项。<br>*Zabbix 2.4* 开始，支持指定自定义 HTTP 头|
|// 跟踪重定向 (Follow redirects)//        选中该复|框以跟踪 HTTP 重定向。<br>将会设置 [CURLOPT\_FOLLOWLOCATION](http://curl.haxx.se/libcurl/c/CURLOPT_FOLLOWLOCATION.html) cURL 选项。<br>*Zabbix 2.4* 开始支持此选项。|
|// 仅检索标头 (Retrieve only headers)//   选中复选|，仅从 HTTP 响应中检索标题。<br>这将设置 [CURLOPT\_NOBODY](http://curl.haxx.se/libcurl/c/CURLOPT_NOBODY.html) cURL 选项。<br>*Zabbix 2.4* 开始支持此选项。|
|// 超时时间 (Timeout)//                   Zab|ix 根据设置的秒数以内来处理 URL。 实际上，此参数定义为连接到 URL 的最大时间和执行 HTTP 请求的最长时间。 因此，Zabbix 不会在步骤上花费超过 **2x 超时时间**。<br>例如：15|
|// 必需的字符串 (Required string)//       必需的正则|达式。<br>除非检索到的内容（HTML）匹配所需的模式，否则步骤将失败。 如果为空，则不执行检查。<br>例如：<br>Zabbix 的主页、\\ Welcome.\*admin<br>// 注意 //: 在此字段中不支持引用在 Zabbix 前端中创建的 [正则表达式 (regular expressions)](regular_expressions)。<br>*Zabbix 2.2* 开始，支持用户宏和 {HOST.\*} [宏](/manual/appendix/macros/supported_by_location)|
|// 状态码 (Required status codes)//       可以|置预期的 HTTP 状态代码列表。 如果 Zabbix 获取的 HTTP 状态码不在列表中，该步骤将认为为失败。<br>如果为空，则不执行检查。<br>例如：200,201,210-299<br>*Zabbix 2.2* 开始，支持用户宏。|

|Parameter|Description|
|---------|-----------|
|*Name*|Unique step name.<br>User macros and {HOST.\*} [macros](/manual/appendix/macros/supported_by_location) are supported, since Zabbix 2.2.|
|*URL*|URL to connect to and retrieve data. For example:<br>https://www.google.com<br>http://www.zabbix.com/download<br>Domain names can be specified in Unicode characters since Zabbix 3.4. They are automatically punycode-converted to ASCII when executing the web scenario step.<br>The *Parse* button can be used to separate optional query fields (like ?name=Admin&password=mypassword) from the URL, moving the attributes and values into *Query fields* for automatic URL-encoding.<br>Variables can be used in the URL, using the {macro} syntax. Variables can be URL-encoded manually using a {{macro}.urlencode()} syntax.<br>User macros and {HOST.\*} [macros](/manual/appendix/macros/supported_by_location) are supported, since Zabbix 2.2.<br>Limited to 2048 characters *starting with Zabbix 2.4*.|
|*Query fields*|HTTP GET variables for the URL.<br>Specified as attribute and value pairs.<br>Values are URL-encoded automatically. Values from scenario variables, user macros or {HOST.\*} macros are resolved and then URL-encoded automatically. Using a {{macro}.urlencode()} syntax will double URL-encode them.<br>User macros and {HOST.\*} [macros](/manual/appendix/macros/supported_by_location) are supported since Zabbix 2.2.|
|*Post*|HTTP POST variables.<br>In **Form data** mode, specified as attribute and value pairs.<br>Values are URL-encoded automatically. Values from scenario variables, user macros or {HOST.\*} macros are resolved and then URL-encoded automatically.<br>In **Raw data** mode, attributes/values are displayed on a single line and concatenated with a **&** symbol.<br>Raw values can be URL-encoded/decoded manually using a {{macro}.urlencode()} or {{macro}.urldecode()} syntax.<br>For example: id=2345&userid={user}<br>If {user} is defined as a variable of the web scenario, it will be replaced by its value when the step is executed. If you wish to URL-encode the variable, substitute {user} with {{user}.urlencode()}.<br>User macros and {HOST.\*} [macros](/manual/appendix/macros/supported_by_location) are supported, since Zabbix 2.2.|
|*Variables*|Step-level variables that may be used for GET and POST functions.<br>Specified as attribute and value pairs.<br>Step-level variables override scenario-level variables or variables from the previous step. However, the value of a step-level variable only affects the step after (and not the current step).<br>They have the following format:<br>**{macro}**=value<br>**{macro}**=regex:<regular expression><br>For more information see variable description on the [scenario](web_monitoring#configuring_a_web_scenario) level.<br>Having step-level variables is supported since Zabbix 2.2.<br>Variables are automatically URL-encoded when used in query fields or form data for post variables, but must be URL-encoded manually when used in raw post or directly in URL.|
|*Headers*|Custom HTTP headers that will be sent when performing a request.<br>Specified as attribute and value pairs.<br>Headers on the step level will overwrite the headers specified for the scenario.<br>For example, setting a 'User-Agent' attribute with no value will remove the User-Agent value set on scenario level.<br>User macros and {HOST.\*} macros are supported.<br>This sets the [CURLOPT\_HTTPHEADER](http://curl.haxx.se/libcurl/c/CURLOPT_HTTPHEADER.html) cURL option.<br>Specifying custom headers is supported *starting with Zabbix 2.4*.|
|*Follow redirects*|Mark the checkbox to follow HTTP redirects.<br>This sets the [CURLOPT\_FOLLOWLOCATION](http://curl.haxx.se/libcurl/c/CURLOPT_FOLLOWLOCATION.html) cURL option.<br>This option is supported *starting with Zabbix 2.4*.|
|*Retrieve only headers*|Mark the checkbox to retrieve only headers from the HTTP response.<br>This sets the [CURLOPT\_NOBODY](http://curl.haxx.se/libcurl/c/CURLOPT_NOBODY.html) cURL option.<br>This option is supported *starting with Zabbix 2.4*.|
|*Timeout*|Zabbix will not spend more than the set amount of time on processing the URL (maximum is 1 hour). Actually this parameter defines the maximum time for making connection to the URL and maximum time for performing an HTTP request. Therefore, Zabbix will not spend more than **2 x Timeout** seconds on the step.<br>[Time suffixes](/manual/appendix/suffixes) are supported, e.g. 30s, 1m, 1h. [User macros](/manual/config/macros/usermacros) are supported.|
|*Required string*|Required regular expressions pattern.<br>Unless retrieved content (HTML) matches required pattern the step will fail. If empty, no check is performed.<br>For example:<br>Homepage of Zabbix<br>Welcome.\*admin<br>*Note*: Referencing [regular expressions](regular_expressions) created in the Zabbix frontend is not supported in this field.<br>User macros and {HOST.\*} [macros](/manual/appendix/macros/supported_by_location) are supported, since Zabbix 2.2.|
|*Required status codes*|List of expected HTTP status codes. If Zabbix gets a code which is not in the list, the step will fail.<br>If empty, no check is performed.<br>For example: 200,201,210-299<br>User macros are supported since Zabbix 2.2.|

::: noteclassic
Web
场景步骤中的任何更改只有在保存整个场景时才会保存。
:::

::: noteclassic
Any changes in web scenario steps will only be saved when
the whole scenario is saved.
:::

另请参见如何配置 Web 监控步骤的 [示例](/manual/web_monitoring/example)。

See also a [real-life example](/manual/web_monitoring/example) of how
web monitoring steps can be configured.

#### 配置身份验证

#### Configuring authentication

**身份验证**选项卡允许您配置场景身份验证选项。

The **Authentication** tab allows you to configure scenario
authentication options.

![](../../assets/en/manual/config/scenario3.png)

Authentication parameters:

认证参数：

|参数                                    说|<|<|
|---------------------------------------------|-|-|
|// 验证 (Authentication)//              验|参数。\                                                                                                                                                                                                                                                                                                                     选中复选框以**None** - 未使用身份验证。\                                                                                                                                                                                                                                                                                                    服务器证书将自**基本认证** - 使用基本认证。\                                                                                                                                                                                                                                                                                                  这将设置 [CURL**NTLM authentication** - 使用 NTLM ([Windows NT LAN Manager)](http://en.wikipedia.org/wiki/NTLM) 身份验证。\                                                                                                                                                                                                                   *Zabbi选择身份验证方法将提供两个附加字段，用于输入用户名和密码。<br>// 从 Zabbix 2.2 开始 *，用户宏可以在用户和密码字段中使用 。 \| \|* 对等 SSL 验证 (SSL verify peer)//|证 Web 服务器的 SSL 证书。<br>从系统的证书颁发机构（CA）位置获取。 您可以使用 Zabbix 服务器或代理配置参数 [SSLCALocation](/manual/appendix/config/zabbix_server) 覆盖 CA 文件的位置。<br>PT\_SSL\_VERIFYPEER](http://curl.haxx.se/libcurl/c/CURLOPT_SSL_VERIFYPEER.html) cURL 参数.<br>2.4* 开始支持此选项。|
|*SSL 验证主机 (SSL verify host)*        选中复|框以验证 Web 服务器证书的 // 公用名称 (Common Name)// 字段或 // 主题备用名称 (Subject Alternate Name)// 字段是否匹配、\\ 这将会设置 [CURLOPT\_SSL\_VERIFYHOST](http://curl.haxx.se/libcurl/c/CURLOPT_SSL_VERIFYHOST.html) cURL 参数。<br>*Zabbix 2.4* 开始支持此选项。|<|
|*SSL 证书文件 (SSL certificate file)*   用于客|端认证的 SSL 证书文件的名称。 证书文件必须为 PEM^1^格式。 如果证书文件还包含私钥，请将 *SSL 密钥文件 (SSL key file)* 字段留空。 如果密钥加密，请在 *SSL 密钥密码 (SSL key password)* 字段中指定密码。 包含此文件的目录由 Zabbix 服务器或代理配置参数 [SSLCertLocation](/manual/appendix/config/zabbix_server) 指定。<br>`HOST.*`宏和用户宏可以在此字段中使用。<br>这将会设置 [CURLOPT\_SSLCERT](http://curl.haxx.se/libcurl/c/CURLOPT_SSLCERT.html) cURL 参数、\\ *Zabbix 2.4* 开始支持此选项。|<|
|*SSL 密钥文件 (SSL key file)*           用于客|端认证的 SSL 私钥文件的名称。 私钥文件必须为 PEM^1^格式。 包含此文件的目录由 Zabbix 服务器或代理配置参数 [SSLKeyLocation](/manual/appendix/config/zabbix_server) 指定。<br>`HOST.*` 宏和用户宏可以在此字段中使用。<br>这将设置 [CURLOPT\_SSLKEY](http://curl.haxx.se/libcurl/c/CURLOPT_SSLKEY.html) cURL 参数。<br>*Zabbix 2.4* 开始支持此选项。|<|
|*SSL 密钥密码 (SSL key password)*       SSL|私钥文件密码。<br>用户宏可以在此字段中使用。<br>这将设置 [CURLOPT\_KEYPASSWD](http://curl.haxx.se/libcurl/c/CURLOPT_KEYPASSWD.html) cURL 参数.<br>*Zabbix 2.4* 开始支持此选项。|<|

|Parameter|Description|
|---------|-----------|
|*Authentication*|Authentication options.<br>**None** - no authentication used.<br>**Basic authentication** - basic authentication is used.<br>**NTLM authentication** - NTLM ([Windows NT LAN Manager)](http://en.wikipedia.org/wiki/NTLM) authentication is used.<br>Selecting an authentication method will provide two additional fields for entering a user name and password.<br>User macros can be used in user and password fields, *starting with Zabbix 2.2*.|
|*SSL verify peer*|Mark the checkbox to verify the SSL certificate of the web server.<br>The server certificate will be automatically taken from system-wide certificate authority (CA) location. You can override the location of CA files using Zabbix server or proxy configuration parameter [SSLCALocation](/manual/appendix/config/zabbix_server).<br>This sets the [CURLOPT\_SSL\_VERIFYPEER](http://curl.haxx.se/libcurl/c/CURLOPT_SSL_VERIFYPEER.html) cURL option.<br>This option is supported *starting with Zabbix 2.4*.|
|*SSL verify host*|Mark the checkbox to verify that the *Common Name* field or the *Subject Alternate Name* field of the web server certificate matches.<br>This sets the [CURLOPT\_SSL\_VERIFYHOST](http://curl.haxx.se/libcurl/c/CURLOPT_SSL_VERIFYHOST.html) cURL option.<br>This option is supported *starting with Zabbix 2.4*.|
|*SSL certificate file*|Name of the SSL certificate file used for client authentication. The certificate file must be in PEM^1^ format. If the certificate file contains also the private key, leave the *SSL key file* field empty. If the key is encrypted, specify the password in *SSL key password* field. The directory containing this file is specified by Zabbix server or proxy configuration parameter [SSLCertLocation](/manual/appendix/config/zabbix_server).<br>`HOST.*` macros and user macros can be used in this field.<br>This sets the [CURLOPT\_SSLCERT](http://curl.haxx.se/libcurl/c/CURLOPT_SSLCERT.html) cURL option.<br>This option is supported *starting with Zabbix 2.4*.|
|*SSL key file*|Name of the SSL private key file used for client authentication. The private key file must be in PEM^1^ format. The directory containing this file is specified by Zabbix server or proxy configuration parameter [SSLKeyLocation](/manual/appendix/config/zabbix_server).<br>`HOST.*` macros and user macros can be used in this field.<br>This sets the [CURLOPT\_SSLKEY](http://curl.haxx.se/libcurl/c/CURLOPT_SSLKEY.html) cURL option.<br>This option is supported *starting with Zabbix 2.4*.|
|*SSL key password*|SSL private key file password.<br>User macros can be used in this field.<br>This sets the [CURLOPT\_KEYPASSWD](http://curl.haxx.se/libcurl/c/CURLOPT_KEYPASSWD.html) cURL option.<br>This option is supported *starting with Zabbix 2.4*.|

::: noteimportant
 \[1\] Zabbix 仅支持 PEM 格式的证书和私钥文件。
如果您在 PKCS＃12 格式文件（通常具有扩展名\* .p12 或\*
.pfx）中具有您的证书和私钥数据，您可以使用以下命令从中生成 PEM 文件：

    openssl pkcs12 -in ssl-cert.p12 -clcerts -nokeys -out ssl-cert.pem
    openssl pkcs12 -in ssl-cert.p12 -nocerts -nodes  -out ssl-cert.key


:::

::: noteimportant
 \[1\] Zabbix supports certificate and private key
files in PEM format only. In case you have your certificate and private
key data in PKCS \#12 format file (usually with extention \*.p12 or
\*.pfx) you may generate the PEM file from it using the following
commands:

    openssl pkcs12 -in ssl-cert.p12 -clcerts -nokeys -out ssl-cert.pem
    openssl pkcs12 -in ssl-cert.p12 -nocerts -nodes  -out ssl-cert.key


:::

::: noteclassic
 Zabbix 服务器对证书的更改无需重启。 
:::

::: noteclassic
 Zabbix server picks up changes in certificates without a
restart. 
:::

::: noteclassic
 如果在单个文件中有客户端证书和私钥，只需在“SSL
证书文件”字段中指定它，并将“SSL 密钥文件”字段留空即可。
证书和密钥必须仍为 PEM 格式。 组合证书和密钥很容易：

    cat client.crt client.key > client.pem


:::

::: noteclassic
 If you have client certificate and private key in a single
file just specify it in a "SSL certificate file" field and leave "SSL
key file" field empty. The certificate and key must still be in PEM
format. Combining certificate and key is easy:

    cat client.crt client.key > client.pem


:::

#### 界面

#### Display

要查看定义的 Web 场景的详细数据，请转到 // 监控中 (Monitoring)→Web// 或
// 最新数据 (Latest data)//。 单击方案名称以查看更详细的统计信息。

To view detailed data of defined web scenarios, go to *Monitoring → Web*
or *Latest data*. Click on the scenario name to see more detailed
statistics.

![](../../assets/en/manual/web_monitoring/scenario_details1.png){width="600"}

可以在 // 监控中 (Monitoring)→仪表板 (Dashboard)// 中查看 Web
监控场景的概述。

An overview of web monitoring scenarios can be viewed in *Monitoring →
Dashboard*.

#### 监控扩展

#### Extended monitoring

有时需要记录接收的 HTML 页面内容。 如果某些 Web
方案步骤失败时是非常有用的。 调试级别 5（跟踪）用于此目的。 此级别可以在
[服务端 (server)](/manual/appendix/config/zabbix_server) 和 [代理
(proxy)](/manual/appendix/config/zabbix_proxy)
代理配置文件中设置或使用运行时控制选项
(`-R log_level_increase="http poller,N"`，其中 N 是进程号）。
以下示例说明如果调试级别 4 已设置，监控扩展如何启动：

    Increase log level of all http pollers:
    shell> zabbix_server -R log_level_increase="http poller"

    Increase log level of second http poller:
    shell> zabbix_server -R log_level_increase="http poller,2"

Sometimes it is necessary to log received HTML page content. This is
especially useful if some web scenario step fails. Debug level 5 (trace)
serves that purpose. This level can be set in
[server](/manual/appendix/config/zabbix_server) and
[proxy](/manual/appendix/config/zabbix_proxy) configuration files or
using a runtime control option (`-R log_level_increase="http poller,N"`,
where N is the process number). The following examples demonstrate how
extended monitoring can be started provided debug level 4 is already
set:

    Increase log level of all http pollers:
    shell> zabbix_server -R log_level_increase="http poller"

    Increase log level of second http poller:
    shell> zabbix_server -R log_level_increase="http poller,2"

如果不需要扩展 Web 监控，可以使用`-R log_level_decrease`选项停止。

If extended web monitoring is not required it can be stopped using the
`-R log_level_decrease` option.

[comment]: # ({/new-42ab5787})
