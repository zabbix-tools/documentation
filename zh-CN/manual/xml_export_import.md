[comment]: # translation:outdated

[comment]: # ({new-566c4be6})
# 14. 配置导出/导入

[comment]: # ({/new-566c4be6})

[comment]: # ({new-265e7dfe})
#### 概述

通过Zabbix的导出/导入功能，你可以在不同的Zabbix系统之间交换配置实体。

该功能的典型使用场景如下：

-   分享模板或者网络maps - Zabbix用户可以分享他们的配置参数
-   在 *share.zabbix.com* 网站上分享web场景 -
    导出带有web场景的模板，上传到 *share.zabbix.com*
    即可。其他的用户就可以下载模板，然后往Zabbix导入XML模板文件
-   集成第三方平台 -
    通用的XML格式，让Zabbix与第三方平台或者应用集成及数据导入/导出成为可能

[comment]: # ({/new-265e7dfe})

[comment]: # ({new-19950447})
##### 哪些对象可以被导出/导入

可以被导出/导入的对象有：

-   [主机组](/manual/xml_export_import/groups) (*仅通过Zabbix API*)
-   [模板](/manual/xml_export_import/templates)
-   [主机](/manual/xml_export_import/hosts)
-   [网络拓扑图](/manual/xml_export_import/maps)
-   图像
-   [聚合图形](/manual/xml_export_import/screens)
-   值映射

[comment]: # ({/new-19950447})

[comment]: # ({new-44b0f5b3})
##### 导出格式

可以通过Zabbix前端或者 [Zabbix API](/manual/api/reference/configuration)
来导出数据。支持的导出格式如下：

-   XML - 在前端页面导出
-   XML or JSON - 通过Zabbix API导出

[comment]: # ({/new-44b0f5b3})

[comment]: # ({new-b87bd84d})
#### 关于导出功能的明细

-   所有支持导出的元素都在一个文件里。
-   从链接模板里继承的主机和模板实体（监控项，触发器，图表，发现规则）不会被导出。在主机层面对这些实体所做的任何更改（比如更改监控项间隔，修改正则表达式或者给低级别发现增加原型），在导出的时候都会丢失；在导入的时候，所有来自于链接模板的实体，就像在原始链接模板上一样会被重新创建。
-   由低级别发现创建的实体以及任何依赖于它们的实体都不会被导出。例如，为某个LLD规则生成的监控项而创建的触发器不会被导出。

[comment]: # ({/new-b87bd84d})

[comment]: # ({new-6ea18fa7})
#### 关于导入功能的明细

-   一旦遇到错误导出功能就会停止
-   如果刚好在图像导入过程中更新已有的图像，“图像类型（imagetype）”字段会被忽略。也就是说，不能通过导入来更改图像类型。
-   当导入主机/模板的时候使用“删除不存在（Delete
    missing）”选项，那么不在XML导入文件里的主机/模板宏（macros）也将会被删除。
-   监控项，触发器，图表，主机/模板应用，发现规则，监控项原型，触发器原型，图表原型的空标签是没有意义的，就好像不存在一样。其他的标签，比如，监控项应用是有意义的。也就是说，空标签代表监控项没有应用，丢失标签代表不需要更新应用。
-   导入支持XML和JSON两种格式，导入文件必须有正确的文件扩展名：XML的是.xml，JSON的是.json。
-   关于支持的XML版本，请查看
    [兼容性信息](/manual/appendix/compatibility)

[comment]: # ({/new-6ea18fa7})

[comment]: # ({new-790544d4})
#### XML基本格式

``` {.xml}
<?xml version="1.0" encoding="UTF-8"?>
<zabbix_export>
    <version>4.0</version>
    <date>2016-10-04T06:20:11Z</date>
</zabbix_export>
```

    <?xml version="1.0" encoding="UTF-8"?>

默认XML文件头格式。

    <zabbix_export>

Zabbix XML导出的格式标签。

    <version>4.0</version>

导出的版本。

    <date>2016-10-04T06:20:11Z</date>

导出的时候，日期以ISO 8601长格式创建，其他的标签取决于导出的对象。

[comment]: # ({/new-790544d4})



[comment]: # ({new-8c839240})
#### XML format

``` {.xml}
<?xml version="1.0" encoding="UTF-8"?>
<zabbix_export>
    <version>6.0</version>
    <date>2020-04-22T06:20:11Z</date>
</zabbix_export>
```

    <?xml version="1.0" encoding="UTF-8"?>

Default header for XML documents.

    <zabbix_export>

Root element for Zabbix XML export.

    <version>6.0</version>

Export version.

    <date>2020-04-22T06:20:11Z</date>

Date when export was created in ISO 8601 long format.

Other tags are dependent on exported objects.

[comment]: # ({/new-8c839240})

[comment]: # ({new-239421b8})
#### JSON format

``` {.json}
{
    "zabbix_export": {
        "version": "6.0",
        "date": "2020-04-22T06:20:11Z"
    }
}
```

      "zabbix_export":

Root node for Zabbix JSON export.

          "version": "6.0"

Export version.

          "date": "2020-04-22T06:20:11Z"

Date when export was created in ISO 8601 long format.

Other nodes are dependent on exported objects.

[comment]: # ({/new-239421b8})
