[comment]: # translation:outdated

[comment]: # ({new-58c98254})
# 3 PSK problems

[comment]: # ({/new-58c98254})

[comment]: # ({new-aebe0aa7})
#### PSK contains an odd number of hex-digits

Proxy or agent does not start, message in the proxy or agent log:

    invalid PSK in file "/home/zabbix/zabbix_proxy.psk"

[comment]: # ({/new-aebe0aa7})

[comment]: # ({new-887ee505})
#### PSK identity string longer than 128 bytes is passed to GnuTLS

In TLS client side log:

    gnutls_handshake() failed: -110 The TLS connection was non-properly terminated.

In TLS server side log.

    gnutls_handshake() failed: -90 The SRP username supplied is illegal.

[comment]: # ({/new-887ee505})

[comment]: # ({new-a2e71eaa})
#### PSK longer than 32 bytes is passed to mbed TLS (PolarSSL)

In any Zabbix log:

    ssl_set_psk(): SSL - Bad input parameters to function

[comment]: # ({/new-a2e71eaa})
