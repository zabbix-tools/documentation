[comment]: # translation:outdated

[comment]: # ({new-64bb0393})
# 2 Certificate problems

[comment]: # ({/new-64bb0393})

[comment]: # ({new-123d3f4b})
#### OpenSSL used with CRLs and for some CA in the certificate chain its CRL is not included in `TLSCRLFile`

In TLS server log in case of *mbed TLS (PolarSSL)* and *OpenSSL* peers:

    failed to accept an incoming connection: from 127.0.0.1: TLS handshake with 127.0.0.1 returned error code 1: \
        file s3_srvr.c line 3251: error:14089086: SSL routines:ssl3_get_client_certificate:certificate verify failed: \
        TLS write fatal alert "unknown CA"

In TLS server log in case of *GnuTLS* peer:

    failed to accept an incoming connection: from 127.0.0.1: TLS handshake with 127.0.0.1 returned error code 1: \
        file rsa_pk1.c line 103: error:0407006A: rsa routines:RSA_padding_check_PKCS1_type_1:\
        block type is not 01 file rsa_eay.c line 705: error:04067072: rsa routines:RSA_EAY_PUBLIC_DECRYPT:paddin

[comment]: # ({/new-123d3f4b})

[comment]: # ({new-7be8a029})
#### CRL expired or expires during server operation

[*OpenSSL*]{.underline}, in server log:

-   before expiration:

```{=html}
<!-- -->
```
    cannot connect to proxy "proxy-openssl-1.0.1e": TCP successful, cannot establish TLS to [[127.0.0.1]:20004]:\
        SSL_connect() returned SSL_ERROR_SSL: file s3_clnt.c line 1253: error:14090086:\
        SSL routines:ssl3_get_server_certificate:certificate verify failed:\
        TLS write fatal alert "certificate revoked"

-   after expiration:

```{=html}
<!-- -->
```
    cannot connect to proxy "proxy-openssl-1.0.1e": TCP successful, cannot establish TLS to [[127.0.0.1]:20004]:\
        SSL_connect() returned SSL_ERROR_SSL: file s3_clnt.c line 1253: error:14090086:\
        SSL routines:ssl3_get_server_certificate:certificate verify failed:\
        TLS write fatal alert "certificate expired"

The point here is that with valid CRL a revoked certificate is reported
as "certificate revoked". When CRL expires the error message changes to
"certificate expired" which is quite misleading.

[*GnuTLS*]{.underline}, in server log:

-   before and after expiration the same:

```{=html}
<!-- -->
```
    cannot connect to proxy "proxy-openssl-1.0.1e": TCP successful, cannot establish TLS to [[127.0.0.1]:20004]:\
          invalid peer certificate: The certificate is NOT trusted. The certificate chain is revoked.

[*mbed TLS*]{.underline} ([*PolarSSL*]{.underline}), in server log:

-   before expiration:

```{=html}
<!-- -->
```
    cannot connect to proxy "proxy-openssl-1.0.1e": TCP successful, cannot establish TLS to [[127.0.0.1]:20004]:\
        invalid peer certificate: revoked

-   after expiration:

```{=html}
<!-- -->
```
    cannot connect to proxy "proxy-openssl-1.0.1e": TCP successful, cannot establish TLS to [[127.0.0.1]:20004]:\
          invalid peer certificate: revoked, CRL expired

[comment]: # ({/new-7be8a029})


[comment]: # ({new-e3030ed0})
#### Self-signed certificate, unknown CA

[*OpenSSL*]{.underline}, in log:

    error:'self signed certificate: SSL_connect() set result code to SSL_ERROR_SSL: file ../ssl/statem/statem_clnt.c\
          line 1924: error:1416F086:SSL routines:tls_process_server_certificate:certificate verify failed:\
          TLS write fatal alert "unknown CA"'

This was observed when server certificate by mistake had the same Issuer
and Subject string, although it was signed by CA. Issuer and Subject are
equal in top-level CA certificate, but they cannot be equal in server
certificate. (The same applies to proxy and agent certificates.)

[comment]: # ({/new-e3030ed0})
