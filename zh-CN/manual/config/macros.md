[comment]: # translation:outdated

[comment]: # ({new-03ce9d2a})
# 10 宏

[comment]: # ({/new-03ce9d2a})

[comment]: # ({new-4c81df7a})
## 10 Macros

#### 概述

#### Overview

Zabbix支持许多在多种情况下使用的宏。宏是一个变量，由如下特殊语法标识：

Zabbix supports a number of macros which may be used in various
situations. Macros are variables, identified by a specific syntax:

    {MACRO} 

    {MACRO} 

根据在上下文中， 宏解析为一个特殊的值。

Macros resolve to a specific value depending on the context.

有效地使用宏可以节省时间，并使Zabbix变地更加高效。

Effective use of macros allows to save time and make Zabbix
configuration more transparent.

在一个的典型用途中，宏可以用于模板中。因此，模板的触发器可能命名为“Processor
load is too high on {HOST.NAME}”。当这个模板应用与主机（如 Zabbix Server
）时，并且当触发器展示在监控页面上时，触发器的名称讲解析为“Processor
load is too high on Zabbix server”。

In one of typical uses, a macro may be used in a template. Thus a
trigger on a template may be named "Processor load is too high on
{HOST.NAME}". When the template is applied to the host, such as Zabbix
server, the name will resolve to "Processor load is too high on Zabbix
server" when the trigger is displayed in the Monitoring section.

宏可以在监控项键值参数中使用。宏只能用在监控项键值参数的一部分中，例如
`item.key[server_{HOST.HOST}_local]`
。双引号参数不是必须的，因为Zabbix将处理任何模糊不清的特殊参数（如果这些参数存在于已解析的宏中）。

Macros may be used in item key parameters. A macro may be used for only
a part of the parameter, for example
`item.key[server_{HOST.HOST}_local]`. Double-quoting the parameter is
not necessary as Zabbix will take care of any ambiguous special symbols,
if present in the resolved macro.

详细请查阅：

See also:

-   [受支持的宏](/manual/appendix/macros/supported_by_location)的完整列表；
-   宏 [函数](/manual/config/macros/macro_functions)；
-   如何配置 [用户宏](/manual/config/macros/user_macros)。

```{=html}
<!-- -->
```
-   full list [of supported
    macros](/manual/appendix/macros/supported_by_location)
-   macro [functions](/manual/config/macros/macro_functions)
-   how to configure [user macros](/manual/config/macros/user_macros)

[comment]: # ({/new-4c81df7a})
