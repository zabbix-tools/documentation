[comment]: # translation:outdated

[comment]: # ({new-6f10822d})
# 1 配置用户

[comment]: # ({/new-6f10822d})

[comment]: # ({new-5f8fb5e5})
### 1 Configuring a user

[comment]: # ({/new-5f8fb5e5})

[comment]: # ({new-2b69e181})
#### 概述

[comment]: # ({/new-2b69e181})

[comment]: # ({new-f35e11e2})
#### Overview

根据以下步骤来配置一个用户：

To configure a user:

-   在 Zabbix 前端页面跳转到 *管理 → 用户*；
-   在当前页面点击*创建用户* （或在用户名中编辑现有的用户）；
-   在窗口中编辑用户属性。

```{=html}
<!-- -->
```
-   Go to *Administration → Users*
-   Click on *Create user* (or on the user name to edit an existing
    user)
-   Edit user attributes in the form

[comment]: # ({/new-f35e11e2})

[comment]: # ({new-bc3b9c76})
#### 常规属性

#### General attributes

在 *用户* 标签页包含常规用户属性：

The *User* tab contains general user attributes:

![](../../../../assets/en/manual/config/user.png){width="600"}

All mandatory input fields are marked with a red asterisk.

|参数                      描|<|
|-------------------------------|-|
|*别名*                    唯|的用户名，用作登陆名.|
|*名字*                    用|的名字 （可选的）.<br>如果此项不为空的话，则在确认信息和通知收件人信息中可见.|
|*姓氏*                    用|的姓氏 (可选的).<br>如果此项不为空的话，则在确认信息和通知收件人信息中可见.|
|*密码*                    输|用户密码的两个字段.<br>With an existing password, contains a *Password* button, clicking on which opens the password fields.|
|*用户组*                  用户|属 [用户组](usergroup) 的列表。 所属的用户组决定用户了用户可以[访问](permissions)的主机组和主机。点击*添加*进行添加用户组.|
|*语言*                    Z|bbix 前端的预言.PHP扩展插件 gettext 是翻译所必须的.|
|*主机*                    定|了前端的样式：<br>**系统默认** - 使用默认的系统设置<br>**蓝** - 标准的蓝色主题<br>**深色** - 另一种深色主题|
|*自动登录*                如果你|望Zabbix记住登录的信息并自动登录30天，请启用此选项.此选项需要用到浏览器的 cookies.|
|*自动登出 （最少90秒）*   勾选此选项以设置|户在不活跃时间（最少90秒）后自动退出登录.|
|*刷新 (秒)*               设置|形、聚合图形、文本数据等的刷新速率。可以设置为0即禁止刷新.|
|*每页行数*                设置每|页面显示的行数.|
|*URL (登录后)*            通过|置一个 URL,当你登录Zabbix后,可以跳转到此 URL.例如,设置为触发器的状态页面.|

|Parameter|Description|
|---------|-----------|
|*Alias*|Unique username, used as the login name.|
|*Name*|User first name (optional).<br>If not empty, visible in acknowledgement information and notification recipient information.|
|*Surname*|User second name (optional).<br>If not empty, visible in acknowledgement information and notification recipient information.|
|*Groups*|Select [user groups](usergroup) the user belongs to. Starting with Zabbix 3.4.3 this field is auto-complete so starting to type the name of a user group will offer a dropdown of matching groups. Scroll down to select. Alternatively, click on *Select* to add groups. Click on 'x' to remove the selected.<br>Adherence to user groups determines what host groups and hosts the user will have [access to](permissions).|
|*Password*|Two fields for entering the user password.<br>With an existing password, contains a *Password* button, clicking on which opens the password fields.|
|*Language*|Language of the Zabbix frontend.<br>The php gettext extension is required for the translations to work.|
|*Theme*|Defines how the frontend looks like:<br>**System default** - use default system settings<br>**Blue** - standard blue theme<br>**Dark** - alternative dark theme<br>**High-contrast light** - light theme with high contrast<br>**High-contrast dark** - dark theme with high contrast|
|*Auto-login*|Mark this checkbox to make Zabbix remember the user and log the user in automatically for 30 days. Browser cookies are used for this.|
|*Auto-logout*|With this checkbox marked the user will be logged out automatically, after the set amount of seconds (minimum 90 seconds, maximum 1 day).<br>[Time suffixes](/manual/appendix/suffixes) are supported, e.g. 90s, 5m, 2h, 1d.<br>Note that this option will not work:<br>\* If the "Show warning if Zabbix server is down" global configuration option is enabled and Zabbix frontend is kept open;<br>\* When Monitoring menu pages perform background information refreshes;<br>\* If logging in with the *Remember me for 30 days* option checked.|
|*Refresh*|Set the refresh rate used for graphs, screens, plain text data, etc. Can be set to 0 to disable.|
|*Rows per page*|You can determine how many rows per page will be displayed in lists.|
|*URL (after login)*|You can make Zabbix transfer the user to a specific URL after successful login, for example, to Problems page.|

#### 告警媒介

#### User media

*告警媒介*标签页包含用户定义的所有告警媒介。告警媒介用于发送通知。点击*添加*将告警媒介分配给用户。

The *Media* tab contains a listing of all media defined for the user.
Media are used for sending notifications. Click on *Add* to assign media
to the user.

关于配置告警媒介类型详细的信息，请参阅[告警媒介类型](/manual/config/notifications/media)。

See the [Media types](/manual/config/notifications/media) section for
details on configuring media types.

#### 权限

#### Permissions

*权限*标签页包含以下信息：

The *Permissions* tab contains information on:

-   用户类型 （Zabbix User, Zabbix Admin, Zabbix Super Admin）。
    用户不能改变自己的用户类型。
-   用户可以访问的主机组。默认情况下,“Zabbix User”和“Zabbix
    Admin”用户无权访问任何的主机组和主机。若要获得访问权限，需要将他们定义到访问相应主机组和主机的用户组中。

```{=html}
<!-- -->
```
-   the user type (Zabbix User, Zabbix Admin, Zabbix Super Admin). Users
    cannot change their own type.
-   host groups the user has access to. 'Zabbix User' and 'Zabbix Admin'
    users do not have access to any host groups and hosts by default. To
    get access they need to be included in user groups that have access
    to respective host groups and hosts.

关于详细信息，请参阅[用户权限](permissions)页面。

See the [User permissions](permissions) page for details.

[comment]: # ({/new-bc3b9c76})
