[comment]: # translation:outdated

[comment]: # ({new-46adf407})
# 3 用户组

[comment]: # ({/new-46adf407})

[comment]: # ({new-f58db4c9})
### 3 User groups

[comment]: # ({/new-f58db4c9})

[comment]: # ({new-fe6bb4a1})
#### 概述

[comment]: # ({/new-fe6bb4a1})

[comment]: # ({new-e74e2150})
#### Overview

用户组可以为组用户组织目的和对数据分配权限。对于主机组的监控数据权限只能分配给用户组，而不是个人用户。

User groups allow to group users both for organizational purposes and
for assigning permissions to data. Permissions to monitoring data of
host groups are assigned to user groups, not individual users.

将一组用户和另一组用户的可用信息单独分离开，这样做通常会更有意义。因为这样可以通过用户进行分组，然后将不同的权限分配给主机组来实现。

It may often make sense to separate what information is available for
one group of users and what - for another. This can be accomplished by
grouping users and then assigning varied permissions to host groups.

一个用户可以属于多个用户组。

A user can belong to any amount of groups.

[comment]: # ({/new-e74e2150})

[comment]: # ({new-931529bc})
#### 配置

#### Configuration

通过以下步骤配置用户组：

To configure a user group:

-   在 Zabbix 前端跳转到//管理 → 用户组 * \* 点击*创建用户组//
    （或者在用户组名上编辑现有的用户组）
-   在表单中编辑用户组属性。

```{=html}
<!-- -->
```
-   Go to *Administration → User groups*
-   Click on *Create user group* (or on the group name to edit an
    existing group)
-   Edit group attributes in the form

“用户组”标签页包含以下常规的用户组属性：

The **User group** tab contains general group attributes:

![](../../../../assets/en/manual/config/user_group_new.png){width="600"}

All mandatory input fields are marked with a red asterisk.

|参数         描|<|
|------------------|-|
|*组名*       唯|的组名.|
|*用户*       *|在组中的...**这个方框内包含当前组内用户的列表.<br>要将其他用户添加到此组中，请在*其他组*这个方框下选择相应的用户，并点击**<<**按钮进行添加.|
|*前端访问*   如何对|内用户进行身份验证.<br>**系统默认** - 使用默认的验证方式<br>**Internal** - 使用 Zabbix 验证.如果设置了[HTTP 验证](/manual/web_interface/frontend_sections/administration/authentication#http)，则忽略此项.<br>**停用的** - 被禁止访问 Zabbix GUI.|
|*已启用*     用户|和组成员的状态.<br>*已选中* - 用户组和用户被启用.<br>*未选中* - 用户组和用户被禁用.|
|*调试模式*   选中此|将会激活用户的调试模式.|

|Parameter|Description|
|---------|-----------|
|*Group name*|Unique group name.|
|*Users*|To add users to the group click *Select* button.|
|*Frontend access*|How the users of the group are authenticated.<br>**System default** - use default authentication<br>**Internal** - use Zabbix authentication. Ignored if [HTTP authentication](/manual/web_interface/frontend_sections/administration/authentication#http) is set<br>**Disabled** - access to Zabbix GUI is forbidden|
|*Enabled*|Status of user group and group members.<br>*Checked* - user group and users are enabled<br>*Unchecked* - user group and users are disabled|
|*Debug mode*|Mark this checkbox to activate [debug mode](/manual/web_interface/debug_mode) for the users.|

**权限**标签页允许你指定用户组访问主机组（和主机组内主机）数据：

The **Permissions** tab allows you to specify user group access to host
group (and thereby host) data:

![](../../../../assets/en/manual/config/user_group_permissions_new.png){width="600"}

主机组的当前权限显示在*权限*方框内。

Current permissions to host groups are displayed in the *Permissions*
block.

如果主机组的当前权限由所有嵌套主机组继承，则由主机组名称后面的括号中的*包含的子组*文本指示。

If current permissions of the host group are inherited by all nested
host groups, that is indicated by the *including subgroups* text in the
parenthesis after the host group name.

您可以更改对主机组的访问级别：

You may change the level of access to a host group:

-   **读写** - 对主机组具有读写权限；
-   **只读** - 对主机组具有只读权限；
-   **拒绝** - 拒绝对主机组的访问；
-   **无** - 不设置任何权限。

```{=html}
<!-- -->
```
-   **Read-write** - read-write access to a host group;
-   **Read** - read-only access to a host group;
-   **Deny** - access to a host group denied;
-   **None** - no permissions are set.

使用下面的选择字段选择主机组和对它们的访问级别（请注意，如果组已经在列表中，则选择*无*将从列表中删除主机组）。
如果要包括嵌套主机组，请选中“*包含子组*”复选框。
该字段是自动完成的，因此开始键入主机组的名称将提供匹配组的下拉列表。
如果你希望查看所有主机组，请单击*选择*按钮。

Use the selection field below to select host groups and the level of
access to them (note that selecting *None* will remove host group from
the list if the group is already in the list). If you wish to include
nested host groups, mark the *Include subgroups* checkbox. This field is
auto-complete so starting to type the name of a host group will offer a
dropdown of matching groups. If you wish to see all host groups, click
on *Select*.

请注意在主机组[configuration](/manual/config/hosts/host#configuring_a_host_group)
Zabbix超级管理员拥有内置主机组同等级别的权限。

Note that it is possible for Zabbix Super Admin users in host group
[configuration](/manual/config/hosts/host#configuring_a_host_group) to
enforce the same level of permissions to the nested host groups as the
parent host group.

**Tag
filter**标签页允许您通过过滤标签名和标签值，来设置用户组查看问题基于标签维度的权限。

The **Tag filter** tab allows you to set tag based permissions for user
groups to see problems filtered by tag name and its value:

![](../../../../assets/en/manual/config/user_group_tag_filter_new.png){width="600"}

选择一个标签过滤某个主机组，点击*Select*查看完整的已有的主机组列表或输入一个主机组名来获取匹配的主机组的下拉列表。如果您想使用内置的主机组标签，标记*Include
subgroups*复选框。

To select a host group to apply a tag filter for, click *Select* to get
the complete list of existing host groups or start to type the name of a
host group to get a dropdown of matching groups. If you want to apply
tag filters to nested host groups, mark the *Include subgroups*
checkbox.

标签过滤允许分离主机组的访问可能性。

Tag filter allows to separate the access to host group from the
possibility to see problems.

例如，如果一个数据库管理员需要只查看"MySQL"数据库的问题，则需要先创建一个数据管理员用户组，然后配置"Service"标签名的值为"MySQL"。

For example, if a database administrator needs to see only "MySQL"
database problems, it is required to create a user group for database
administrators first, than specify "Service" tag name and "MySQL" value.

![user\_group\_tag\_filter\_2.png](../../../../assets/en/manual/config/user_group_tag_filter_2.png)

如果在左侧空白处指定标签名和值，对应的用户组将可以看到该标签下所选主机的所有问题。

If "Service" tag name is specified and value field is left blank,
corresponding user group will see all problems for selected host group
with tag name "Service".

如果左侧空白处标签名和值都未指定，但是选择了主机组，对应的用户组将可以看到所选主机的所有问题。请确保准确地配置了标签名和标签值，否则对应的用户组将看不到任何问题。

If both tag name and value fields are left blank but host group
selected, corresponding user group will see all problems for selected
host group. Make sure a tag name and tag value are correctly specified
otherwise a corresponding user group will not see any problems.

如下是一个用户归属于多个用户组的例子。在本例中涉及到标签过滤的说明。

Let's review an example when a user is a member of several user groups
selected. Filtering in this case will use OR condition for tags.

|   |   |   |   |   |   |   |
|---|---|---|---|---|---|---|
|**用户组 A**|<|**|户组 B**|<|**两组中|户(组)的可见结果**|
|*标签过滤*|<|<|<|<|<|<|
|*主机组*              *标|名*   *标签值*|*主机组*|*标签名*   *标签|*|<|<|
|Templates/Databases|Service|MySQL|Templates/Databases|Service|Oracle|Service: MySQL or Oracle problems visible|
|Templates/Databases|*blank*|*blank*|Templates/Databases|Service|Oracle|All problems visible|
|*not selected*|*blank*|*blank*|Templates/Databases|Service|Oracle|<Service:Oracle> problems visible|

|   |   |   |   |   |   |   |
|---|---|---|---|---|---|---|
|**User group A**|<|<|**User group B**|<|<|**Visible result for a user (member) of both groups**|
|*Tag filter*|<|<|<|<|<|<|
|*Host group*|*Tag name*|*Tag value*|*Host group*|*Tag name*|*Tag value*|<|
|Templates/Databases|Service|MySQL|Templates/Databases|Service|Oracle|Service: MySQL or Oracle problems visible|
|Templates/Databases|*blank*|*blank*|Templates/Databases|Service|Oracle|All problems visible|
|*not selected*|*blank*|*blank*|Templates/Databases|Service|Oracle|<Service:Oracle> problems visible|

::: noteimportant

添加过滤(例如,在主机组名"Templates/Databases"中添加标签)将导致其它的主机组的问题不能够被发现。
:::

::: noteimportant
 Adding a filter (for example, all tags in a
certain host group "Templates/Databases") results in not being able to
see the problems of other host groups.
:::

#### 来自多个用户组的主机访问

#### Host access from several user groups

用户可以属于任意数量的用户组。这些组对主机可能具有不同的访问权限。

A user may belong to any number of user groups. These groups may have
different access permissions to hosts.

因此，重要的是要知道非特权用户将能够访问哪些主机。例如，让我们考虑如何在用户组A和B中的用户的各种情况下对
“主机\*\* X \*\*”（在主机组1中）的访问将受到影响。

Therefore, it is important to know what hosts an unprivileged user will
be able to access as a result. For example, let us consider how access
to host **X** (in Hostgroup 1) will be affected in various situations
for a user who is in user groups A and B.

-   如果“用户组 A ”没有定义权限，同时“用户组 B ”具有对“主机组 1
    ”的*读写*权限，那么用户将获得对“主机 X ”的**读写**访问。

```{=html}
<!-- -->
```
-   If Group A has only *Read* access to Hostgroup 1, but Group B
    *Read-write* access to Hostgroup 1, the user will get **Read-write**
    access to 'X'.

::: noteimportant
从 Zabbix 2.2 开始，”读写“
权限要优先于“只读”权限。 
:::

::: noteimportant
“Read-write” permissions have precedence over
“Read” permissions starting with Zabbix 2.2.
:::

-   在与上述相同的情况下，如果“主机组2”中的“主机 X ”同时拒绝“用户组 A
    ”或“用户组 B ”，那么“主机 X ”的访问将**不可用**，尽管“主机组 1
    ”有*读写*权限。
-   如果“用户组 A ”没有定义权限，同时“用户组 B ”具有对“主机组 1
    ”的*读写*权限，那么用户将获得对“主机 X ”的**读写**访问。
-   如果“用户组 A ”具有对“主机组 1 ”的*拒绝*权限，同时“用户组
    B”具有对“主机组 1 ”的*读写*权限，则用户访问“主机 X ”将被**拒绝**。

```{=html}
<!-- -->
```
-   In the same scenario as above, if 'X' is simultaneously also in
    Hostgroup 2 that is **denied** to Group A or B, access to 'X' will
    be **unavailable**, despite a *Read-write* access to Hostgroup 1.
-   If Group A has no permissions defined and Group B has a *Read-write*
    access to Hostgroup 1, the user will get **Read-write** access to
    'X'.
-   If Group A has *Deny* access to Hostgroup 1 and Group B has a
    *Read-write* access to Hostgroup 1, the user will get access to 'X'
    **denied**.

#### 其他细节

#### Other details

-   当拓扑图为空或者只有图片时，任何非Zabbix超级管理员(包含'guest')都可以看到网络图。当主机，主机组或者触发器添加到拓扑图中，就要考虑权限问题。同样，屏幕(screens)和幻灯片(slideshows)也如此。不考虑权限的情况下，用户可以看见任何非直接或者间接链接到主机的项。
-   如果一个具有对主机具有*读写*权限的管理级别用户无法访问*Templates*主机组，则具有*读写*访问主机的管理级用户将无法链接或取消链接模板。使用*只读*访问*Templates*主机组，他将能够链接或取消链接到主机的模板，但是，模板列表中不会看到任何模板，也不能在其他地方使用模板。
-   具有*只读*访问主机的管理级用户将不会在配置页面的主机列表中看到主机;
    但是，在IT服务配置中可以访问主机触发器。
-   只要地图为空或只有图像，任何非Zabbix超级管理员用户（包括“guest”）都可以看到网络地图。
    当主机、主机组或触发器被添加到地图时，权限被遵守。
    这同样适用于屏幕和幻灯片。
    无论权限如何，用户将看到任何没有直接或间接链接到主机的对象。

```{=html}
<!-- -->
```
-   Any non-Zabbix Super Admin user (including 'guest') can see network
    maps as long as the map is empty or has only images. When hosts,
    host groups or triggers are added to the map, permissions are
    respected. The same applies to screens and slideshows as well. The
    users, regardless of permissions, will see any objects that are not
    directly or indirectly linked to hosts.
-   An Admin level user with *Read-write* access to a host will not be
    able to link/unlink templates, if he has no access to the
    *Templates* group. With *Read* access to *Templates* group he will
    be able to link/unlink templates to the host, however, will not see
    any templates in the template list and will not be able to operate
    with templates in other places.
-   An Admin level user with *Read* access to a host will not see the
    host in the configuration section host list; however, the host
    triggers will be accessible in IT service configuration.
-   Any non-Zabbix Super Admin user (including 'guest') can see network
    maps as long as the map is empty or has only images. When hosts,
    host groups or triggers are added to the map, permissions are
    respected. The same applies to screens and slideshows as well. The
    users, regardless of permissions, will see any objects that are not
    directly or indirectly linked to hosts.
-   Zabbix server will not send notifications to users defined as action
    operation recipients if access to the concerned host is explicitly
    "denied".

[comment]: # ({/new-931529bc})
