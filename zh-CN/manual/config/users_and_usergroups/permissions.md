[comment]: # translation:outdated

[comment]: # ({new-ba72dbf1})
# 2 权限

[comment]: # ({/new-ba72dbf1})

[comment]: # ({new-a9157075})
### 2 Permissions

[comment]: # ({/new-a9157075})

[comment]: # ({new-ac897fff})

#### User types

Permissions in Zabbix depend, primarily, on the user type:

   - *User* - has limited access rights to menu sections (see below) and no access to any resources by default. Any permissions to host or template groups must be explicitly assigned;
   - *Admin* - has incomplete access rights to menu sections (see below). The user has no access to any host groups by default. Any permissions to host or template groups must be explicitly given;
   - *Super admin* - has access to all menu sections. The user has a read-write access to all host and template groups. Permissions cannot be revoked by denying access to specific groups.

**Menu access**

The following table illustrates access to Zabbix menu sections per user type:

|Menu section|<|User|Admin|Super admin|
|-|-------------|------|------|------|
|**Dashboards**|<|+|+|+|
|**Monitoring**|<|+|+|+|
| |*Problems*|+|+|+|
|^|*Hosts*|+|+|+|
|^|*Latest data*|+|+|+|
|^|*Maps*|+|+|+|
|^|*Discovery*| |+|+|
|**Services**|<|+|+|+|
| |*Services*|+|+|+|
|^|*SLA*| |+|+|
|^|*SLA report*|+|+|+|
|**Inventory**|<|+|+|+|
| |*Overview*|+|+|+|
|^|*Hosts*|+|+|+|
|**Reports**|<|+|+|+|
| |*System information*| | |+|
|^|*Scheduled reports*| |+|+|
|^|*Availability report*|+|+|+|
|^|*Triggers top 100*|+|+|+|
|^|*Audit log*| | |+|
|^|*Action log*| | |+|
|^|*Notifications*| |+|+|
|**Data collection**|<| |+|+|
| |*Template groups*| |+|+|
|^|*Host groups*| |+|+|
|^|*Templates*| |+|+|
|^|*Hosts*| |+|+|
|^|*Maintenance*| |+|+|
|^|*Event correlation*| | |+|
|^|*Discovery*| |+|+|
|**Alerts**|<| |+|+|
| |*Trigger actions*| |+|+|
| |*Service actions*| |+|+|
| |*Discovery actions*| |+|+|
| |*Autoregistration actions*| |+|+|
| |*Internal actions*| |+|+|
|^|*Media types*| | |+|
|^|*Scripts*| | |+|
|**Users**|<| | |+|
| |*User groups*| | |+|
|^|*User roles*| | |+|
|^|*Users*| | |+|
|^|*API tokens*| | |+|
|^|*Authentication*| | |+|
|**Administration**|<| | |+|
| |*General*| | |+|
|^|*Audit log*| | |+|
|^|*Housekeeping*| | |+|
|^|*Proxies*| | |+|
|^|*Macros*| | |+|
|^|*Queue*| | |+|

[comment]: # ({/new-ac897fff})

[comment]: # ({new-a852da1b})
#### 概述

[comment]: # ({/new-a852da1b})

[comment]: # ({new-f38cb50e})
#### Overview

您可以定义相应的用户类型，然后通过将无特权用户包含在具有访问主机组数据权限的用户组中来区分
Zabbix 中的用户权限。

You can differentiate user permissions in Zabbix by defining the
respective user type and then by including the unprivileged users in
user groups that have access to host group data.

#### 用户类型

#### User type

用户类型定义了对前端管理菜单的访问级别以及对主机组数据的默认访问权限。

The user type defines the level of access to administrative menus and
the default access to host group data.

|用户类型              描述|<|
|--------------------------------|-|
|*Zabbix 用户*         用|可以访问“监测中”菜单页面。 默认情况下，用户无权访问任何资源。 必须明确分配对主机组的任何权限。|
|*Zabbix 管理员*       用户|以访问“监测中和配置”菜单页面。 默认情况下，用户无权访问任何主机组。 必须明确给出对主机组的任何权限。|
|*Zabbix 超级管理员*   用户可以|问所有内容：监测中、配置和管理菜单页面。 用户对所有主机组具有读写访问权限。 权限不能通过拒绝对特定主机组的访问来撤销。|

|User type|Description|
|---------|-----------|
|*Zabbix User*|The user has access to the Monitoring menu. The user has no access to any resources by default. Any permissions to host groups must be explicitly assigned.|
|*Zabbix Admin*|The user has access to the Monitoring and Configuration menus. The user has no access to any host groups by default. Any permissions to host groups must be explicitly given.|
|*Zabbix Super Admin*|The user has access to everything: Monitoring, Configuration and Administration menus. The user has a read-write access to all host groups. Permissions cannot be revoked by denying access to specific host groups.|

#### 主机组权限

#### Permissions to host groups

只准许主机组级别的[用户组](/manual/config/users_and_usergroups/usergroup)访问
Zabbix 中的任何主机数据。

Access to any host data in Zabbix are granted to [user
groups](/manual/config/users_and_usergroups/usergroup) on host group
level only.

这意味着个人用户不能被直接授予对主机（或主机组）的访问权限。
个人用户只能通过其归属的用户组被授予主机组访问权限，进而访问该主机组下的主机
(即个人用户-----→用户组-----→主机组----→主机)。

That means that an individual user cannot be directly granted access to
a host (or host group). It can only be granted access to a host by being
part of a user group that is granted access to the host group that
contains the host.

[comment]: # ({/new-f38cb50e})
