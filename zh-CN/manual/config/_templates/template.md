[comment]: # translation:outdated

[comment]: # ({new-3222767b})
# 1 配置模板

[comment]: # ({/new-3222767b})

[comment]: # ({new-630aea00})
#### 概述

配置模板需要首先通过定义一些参数来创建模板，然后添加实体（项目，触发器，图形等）。

[comment]: # ({/new-630aea00})

[comment]: # ({new-de7b2ff6})
#### 创建模板

要创建模板，请执行以下操作：

-   转到*配置→模板*
-   点击*创建模板*
-   编辑模板属性

**模板**选项卡包含常规模板属性。

![](../../../../assets/en/manual/config/template.png){width="600"}

模板属性：

|参数          描|<|
|-------------------|-|
|*模板名称*    唯一的|板名称。|
|*可见名称*    如果你|置了这个名字，那么它将是列表，地图等中可见的。|
|*群组*        模|所属的主机/模板组。|
|*新的群组*    可以创|一个新组来保存模板。\\\\如果为空忽略。|
|*主机/模板*   应用模|的主机/模板列表。|
|*描述*        输|模板说明。|

**链接的模板**选项卡允许您将一个或多个“嵌套”模板链接到此模板。所有实体（项目，触发器，图表等）将从链接的模板继承。

要链接新的模板，请开始输入*链接指示器*字段，直到出现与输入的字母对应的模板列表。向下滚动选择。当选择要链接的所有模板时，单击*添加*。

要取消链接模板，请使用*链接的模板*模块中的两个选项之一：

-   *取消链接* - 取消链接模板，但保留其项目，触发器和图形
-   *取消链接并清理* - 取消链接模板并删除其所有项目，触发器和图形

**宏**选项卡允许您定义模板级[用户宏](/manual/config/macros/user_macros)。如果选择了*继承模板的宏*选项，则还可以从链接的模板和全局宏中查看宏。在这里，模板的所有定义的用户宏都显示了它们所决定的值以及它们的起源。

![](../../../../assets/en/manual/config/template_c.png){width="600"}

为方便起见，提供了相应模板和全局宏配置的链接。也可以在模板级别上编辑嵌套模板/全局宏，有效地创建模板上宏的副本。

按钮:

|   |   |
|---|---|
|![](../../../../assets/en/manual/config/button_add.png)|添加模板。添加的模板应该出现在列表中。|
|![](../../../../assets/en/manual/config/button_update.png)|更新现有模板的属性。|
|![](../../../../assets/en/manual/config/button_clone.png)|根据当前模板的属性创建另一个模板，包括从链接模板继承的实体（项目，触发器等）|
|![](../../../../assets/en/manual/config/button_full.png)|基于当前模板的属性创建另一个模板，包括从链接的模板继承并直接附加到当前模板的实体（项目，触发器等）。|
|![](../../../../assets/en/manual/config/button_delete.png)|删除模板; 模板（项目，触发器等）的实体与链接的主机保留。|
|![](../../../../assets/en/manual/config/button_clear.png)|从链接的主机中删除模板及其所有实体。|
|![](../../../../assets/en/manual/config/button_cancel.png)|取消编辑模板属性。|

创建一个模板，开始添加一些实体。

<note
important>项目必须首先添加到模板中。如果没有相应的项目，则无法添加触发器和图形。
:::

[comment]: # ({/new-de7b2ff6})

[comment]: # ({new-0d973911})
#### 添加监控项，触发器，图形

要向模板添监控项，请执行以下操作：

-   转到*配置→主机*（或*模板*）
-   单击所需*主机/模板*行中的监控项
-   标记要添加到模板的项目的复选框
-   点击项目列表下面的*复制*
-   选择要复制的项目的模板（或模板组），然后单击*复制*

所有选定的监控项都应该被复制到模板中。

添加触发器和图形以类似的方式完成（分别从触发器和图形列表），请记住，只有在首先添加所需项目时，才能添加它们。

[comment]: # ({/new-0d973911})

[comment]: # ({new-b78a3626})
#### 添加聚合图形

要在*配置→模板*中向屏幕添加聚合图形，请执行以下操作：

-   点击模板行中的*聚合图形*
-   按照通常的配置聚合图形的方法[配置聚合图形](/manual/config/visualisation/screens)

<note
important>可以包含在模板聚合图形中的元素有：简单图形，自定义图形，时钟，纯文本，URL。
:::

<note
tip>有关访问从模板局和图形创建的主机聚合图形的详细信息，请参阅[主机聚合图形](：manual / config / visualization / host_screens＃access_host_screens)部分。</
note>

[comment]: # ({/new-b78a3626})

[comment]: # ({new-513e93e5})
#### 配置自动发现规则

请参阅手册的[自动发现](/manual/discovery/low_level_discovery)部分。

[comment]: # ({/new-513e93e5})

[comment]: # ({new-e3935a9d})
#### 添加Web场景

要将*配置→模板*中的 Web场景添加到模板，请执行以下操作：

-   点击模板行中的*Web*
-   按照通常的Web方案配置方式[配置Web场景](/manual/web_monitoring#configuring_a_web_scenario)

[comment]: # ({/new-e3935a9d})

[comment]: # ({new-d357b380})
#### Creating a template group

::: noteimportant
Only Super Admin users can create template groups.
:::

To create a template group in Zabbix frontend, do the following:

-   Go to: *Configuration → Template groups*
-   Click on *Create template group* in the upper right corner of the screen
-   Enter the group name in the form

![](../../../../assets/en/manual/config/template_group.png)

To create a nested template group, use the '/' forward slash separator, for example `Linux servers/Databases/MySQL`. You can create this group even if none of the two parent template groups (`Linux servers/Databases/`) exist. In this case creating these parent template groups is up to the user; they will not be created automatically.<br>Leading and trailing slashes, several slashes in a row are not allowed. Escaping of '/' is not supported.

[comment]: # ({/new-d357b380})

[comment]: # ({new-ed4ea073})

Once the group is created, you can click on the group name in the list to edit group name, clone the group or set additional option:

![](../../../../assets/en/manual/config/template_group2.png)

*Apply permissions to all subgroups* - mark this checkbox and click on *Update* to apply the same level of permissions to all nested template groups. For user groups that may have had differing [permissions](/manual/config/users_and_usergroups/usergroup#configuration) assigned to nested template groups, the permission level of the parent template group will be enforced on the nested groups. This is a one-time option that is not saved in the database.

**Permissions to nested template groups**

-   When creating a child template group to an existing parent template group,
    [user group](/manual/config/users_and_usergroups/usergroup)
    permissions to the child are inherited from the parent (for example,
    when creating `Databases/MySQL` if `Databases` already exists)
-   When creating a parent template group to an existing child template group,
    no permissions to the parent are set (for example, when creating
    `Databases` if `Databases/MySQL` already exists)

[comment]: # ({/new-ed4ea073})
