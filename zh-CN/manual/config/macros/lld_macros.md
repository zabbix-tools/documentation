[comment]: # translation:outdated

[comment]: # ({new-2d9d6e33})
# 3 自动发现（LLD）宏

[comment]: # ({/new-2d9d6e33})

[comment]: # ({new-bb12a85a})
### 3 Low-level discovery macros

[comment]: # ({/new-bb12a85a})

[comment]: # ({new-fc325ce1})
#### 概述

[comment]: # ({/new-fc325ce1})

[comment]: # ({new-5b26ab61})
#### Overview

有一种[自动发现（LLD）](/manual/discovery/low_level_discovery)函数中使用的宏类型为:

There is a type of macro used within the [low-level
discovery](/manual/discovery/low_level_discovery) function:

    {#MACRO} 

    {#MACRO} 

它是一个在LLD规则中使用的宏，并返回文件系统名称、网络接口和 SNMP OIDs。

It is a macro that is used in an LLD rule and returns real values of
file system names, network interfaces and SNMP OIDs.

这些宏可以用于创建监控项、触发器和图形*原型*。然后，当发现真实的文件系统、网络接口等，这些宏将被替换为真实的值，并且以这些值来创建真实的监控项、触发器和图形。

These macros can be used for creating item, trigger and graph
*prototypes*. Then, when discovering real file systems, network
interfaces etc., these macros are substituted with real values and are
the basis for creating real items, triggers and graphs.

这些宏还用于在虚拟机[自动发现](/manual/vm_monitoring#host_prototypes)中创建主机和主机组*原型*。

These macros are also used in creating host and host group *prototypes*
in virtual machine [discovery](/manual/vm_monitoring#host_prototypes).

[comment]: # ({/new-5b26ab61})

[comment]: # ({new-573cd0dc})
#### 可支持的位置

#### Supported locations

LLD 宏可以用在：

LLD macros can be used:

-   用于监控项原型中：
    -   names
    -   key parameters
    -   units
    -   SNMP OIDs
    -   IPMI sensor fields
    -   calculated item formulas
    -   SSH and Telnet scripts
    -   database monitoring SQL queries
    -   descriptions (从 2.2.0 开始支持)
-   用于触发器原型中：
    -   names
    -   expressions
    -   URLs (从 3.0.0 开始支持)
    -   descriptions (从 2.2.0 开始支持)
    -   event tag names and values (从 3.2.0 开始支持)
-   用于图形原型中：
    -   names
-   用于主机原型中 (从 2.2.0 开始支持)：
    -   names
    -   visible names
    -   host group prototype names
    -   (详细查阅 [全部列表](/manual/vm_monitoring/discovery_fields))

```{=html}
<!-- -->
```
-   for item prototypes in
    -   names
    -   key parameters
    -   units
    -   update intervals
    -   history storage periods
    -   trend storage periods
    -   SNMP OIDs
    -   IPMI sensor fields
    -   calculated item formulas
    -   SSH and Telnet scripts
    -   database monitoring SQL queries
    -   JMX item endpoint fields
    -   descriptions
    -   since Zabbix 4.0 also in:
        -   item value preprocessing steps
        -   HTTP agent URL field
        -   HTTP agent HTTP query fields field
        -   HTTP agent request body field
        -   HTTP agent required status codes field
        -   HTTP agent headers field key and value
        -   HTTP agent HTTP authentication username field
        -   HTTP agent HTTP authentication password field
        -   HTTP agent HTTP proxy field
        -   HTTP agent HTTP SSL certificate file field
        -   HTTP agent HTTP SSL key file field
        -   HTTP agent HTTP SSL key password field
        -   HTTP agent HTTP timeout field
-   for trigger prototypes in
    -   names
    -   expressions
    -   URLs
    -   descriptions
    -   event tag names and values
-   for graph prototypes in
    -   names
-   for host prototypes in
    -   names
    -   visible names
    -   host group prototype names
    -   (see the [full list](/manual/vm_monitoring/discovery_fields))

在上述所有位置，LLD
宏都可以在用户[宏上下文](/manual/config/macros/user_macros#user_macro_context)中使用。

In all those places LLD macros can be used inside user [macro
context](/manual/config/macros/user_macros#user_macro_context).

一些自动发现（LLD）宏在 Zabbix 中是已经预先内置的，例如 {\#FSNAME}、
{\#FSTYPE}、{\#IFNAME}、 {\#SNMPINDEX}、 {\#SNMPVALUE}
这些宏。然而，当你在创建[自定义](/manual/discovery/low_level_discovery#creating_custom_lld_rules)自动发现规则的时候，遵守这些宏名称不是强制性的。所以，你可以使用任何其他的
LLD 宏名称并引用该名称。

Some low-level discovery macros come "pre-packaged" with the LLD
function in Zabbix - {\#FSNAME}, {\#FSTYPE}, {\#IFNAME}, {\#SNMPINDEX},
{\#SNMPVALUE}. However, adhering to these names is not compulsory when
creating a
[custom](/manual/discovery/low_level_discovery#creating_custom_lld_rules)
low-level discovery rule. Then you may use any other LLD macro name and
refer to that name.

[comment]: # ({/new-573cd0dc})
