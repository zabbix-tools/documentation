[comment]: # translation:outdated

[comment]: # ({new-0458cee0})
# 1 宏函数

[comment]: # ({/new-0458cee0})

[comment]: # ({new-c1632a9e})
### 1 Macro functions

[comment]: # ({/new-c1632a9e})

[comment]: # ({new-a0ad7754})
#### 概述

[comment]: # ({/new-a0ad7754})

[comment]: # ({new-9423d893})
#### Overview

宏函数能提供自定义[宏](/manual/config/macros)值的功能。

Macro functions offer the ability to customize
[macro](/manual/config/macros) values.

有时宏可能会解析为一个不一定易于使用的值。它可能很长，或包含你想提取的一个特殊感兴趣的子字符串。这在宏函数中是可以使用的。

Sometimes a macro may resolve to a value that is not necessarily easy to
work with. It may be long or contain a specific substring of interest
that you would like to extract. This is where macro functions can be
useful.

宏函数的语法为：

The syntax of a macro function is:

    {<macro>.<func>(<params>)}

    {<macro>.<func>(<params>)}

其中：

where:

-   <macro> - 这个参数为要定义的宏 （例如 {ITEM.VALUE}）；
-   <func> - 要应用的函数；
-   <params> - 以逗号分隔的函数参数列表。如果他们以'' '' (空格),
    `"` 或者包含 `)`, `,`这些符号开始，则参数必须要引用。

```{=html}
<!-- -->
```
-   <macro> - the macro to customize (for example {ITEM.VALUE})
-   <func> - the function to apply
-   <params> - a comma-delimited list of function parameters.
    Parameters must be quoted if they start with '' '' (space), `"` or
    contain `)`, `,`.

例如：

For example:

    {{ITEM.VALUE}.regsub(pattern, output)}

    {{ITEM.VALUE}.regsub(pattern, output)}

#### 受支持的宏函数

#### Supported macro functions

|函数|<|<|<|<|
|------|-|-|-|-|
|<|**描述**                                           *|参数**                                                                                                                 **受|持于**|<|
|**regsub** (<pattern>,<output>)|<|<|<|<|
|<|通过正则表达式匹配提取的子字符串（区分大小写）。   **pattern** - 匹配的正则表达式<br>|{ITEM.VALUE}<br>**output** - 输出的选项。 **\\1 - \\9** 占位符支持被正则表达式匹配的组placeholders are supported for captured groups.\   {ITEM.LASTVALUE}<br>如果参数 `pattern` 是一个不正确的正则表达式，那么将返回 “UNKNOWN” 。|<|<|
|**iregsub** (<pattern>,<output>)|<|<|<|<|
|<|通过正则表达式匹配提取的子字符串（区分大小写）。   **pattern** - 匹配的正则表达式<br>|{ITEM.VALUE}<br>**output** - 输出得选项 **\\1 - \\9** placeholders are supported for captured groups\                                    {ITE<br>如果参数 `pattern` 是一个不正确的正则表达式，那么将返回 “UNKNOWN” 。|<.LASTVALUE}|<|

|FUNCTION|<|<|<|<|
|--------|-|-|-|-|
|<|**Description**|**Parameters**|**Supported for**|<|
|**regsub** (<pattern>,<output>)|<|<|<|<|
|<|Substring extraction by a regular expression match (case sensitive).|**pattern** - the regular expression to match<br>**output** - the output options. **\\1 - \\9** placeholders are supported to capture groups. **\\0** returns the matched text.<br><br>If `pattern` is not a correct regular expression 'UNKNOWN' is returned.|{ITEM.VALUE}<br>{ITEM.LASTVALUE}|<|
|**iregsub** (<pattern>,<output>)|<|<|<|<|
|<|Substring extraction by a regular expression match (case insensitive).|**pattern** - the regular expression to match<br>**output** - the output options. **\\1 - \\9** placeholders are supported to capture groups. **\\0** returns the matched text.<br><br>If `pattern` is not a correct regular expression 'UNKNOWN' is returned.|{ITEM.VALUE}<br>{ITEM.LASTVALUE}|<|

如果在[受支持的位置](/manual/appendix/macros/supported_by_location)使用函数，但是应用于不支持宏函数得宏，
那么宏的计算结果为 “UNKNOWN”。

If a function is used in a [supported
location](/manual/appendix/macros/supported_by_location), but applied to
a macro not supporting macro functions, then the macro evaluates to
'UNKNOWN'.

如果在不支持宏函数的位置将宏函数应用于宏， 则忽略该函数。

If a macro function is applied to the macro in locations not supporting
macro functions then the function is ignored.

#### 示例

#### Examples

关于宏函数可用于自定义宏值的方法，在下面的示例中说明，其中包含的 “log
line” 作为接收值：

The ways in which macro functions can be used to customize macro values
is illustrated in the following examples containing log lines as
received value:

|接收值                      宏|输出|<|
|----------------------------------|------|-|
|`123Log line`|`{{ITEM.VALUE}.regsub(^[0-9]+, Problem)}`|`Problem`|
|`123 Log line`|`{{ITEM.VALUE}.regsub("^([0-9]+)", "Problem")}`|`Problem`|
|`123 Log line`|`{{ITEM.VALUE}.regsub("^([0-9]+)", Problem ID: \1)}`|`Problem ID: 123`|
|`Log line`|`{{ITEM.VALUE}.regsub(".*", "Problem ID: \1")}`|''Problem ID: ''|
|`MySQL crashed errno 123`|`{{ITEM.VALUE}.regsub("^([A-Z]+).*([0-9]+)", " Problem ID: \1_\2 ")}`|'' Problem ID: MySQL\_123''|
|`123 Log line`|`{{ITEM.VALUE}.regsub("([1-9]+", "Problem ID: \1")}`|`*UNKNOWN*` (invalid regular expression)|

|Received value|Macro|Output|
|--------------|-----|------|
|`123Log line`|`{{ITEM.VALUE}.regsub(^[0-9]+, Problem)}`|`Problem`|
|`123 Log line`|`{{ITEM.VALUE}.regsub("^([0-9]+)", "Problem")}`|`Problem`|
|`123 Log line`|`{{ITEM.VALUE}.regsub("^([0-9]+)", Problem ID: \1)}`|`Problem ID: 123`|
|`Log line`|`{{ITEM.VALUE}.regsub(".*", "Problem ID: \1")}`|''Problem ID: ''|
|`MySQL crashed errno 123`|`{{ITEM.VALUE}.regsub("^([A-Z]+).*([0-9]+)", " Problem ID: \1_\2 ")}`|'' Problem ID: MySQL\_123 ''|
|`123 Log line`|`{{ITEM.VALUE}.regsub("([1-9]+", "Problem ID: \1")}`|`*UNKNOWN*` (invalid regular expression)|

[comment]: # ({/new-9423d893})
