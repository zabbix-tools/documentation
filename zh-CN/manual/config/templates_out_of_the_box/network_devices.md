[comment]: # translation:outdated

[comment]: # ({new-7a75c626})
# 1 网络设备的标准化模板

[comment]: # ({/new-7a75c626})

[comment]: # ({new-1dc52a1f})
#### 概述

为了对交换机和路由器等网络设备进行监控，我们创建了两个所谓的模型：网络设备本身（基本上是机箱）和网络接口。

由于Zabbix 3.4提供了许多网络设备系列模板。
所有模板都覆盖（尽可能从设备中获取这些项目）：

-   机箱故障监控（电源，风扇和温度，总体状态）
-   机箱性能监控（CPU和内存项）
-   机箱资产收集（序列号，型号名称，固件版本）
-   使用IF-MIB和EtherLike-MIB进行网络接口监控（接口状态，接口流量负载，以太网的双工状态）

这些模板可用：

-   在*配置* - > *模板*的新安装中;
-   在[official
    Zabbix模板库](https://www.zabbix.org/wiki/Zabbix_Templates/Official_Templates)中。
    如果您已从3.4之前的Zabbix版本升级，则可以从XML导入这些模板。

如果要导入新的开箱即用模板，您可能还需要将“\@Network自动发现接口”全局正则表达式更新为：

    Result is FALSE: ^Software Loopback Interface
    Result is FALSE: ^(In)?[lL]oop[bB]ack[0-9._]*$
    Result is FALSE: ^NULL[0-9.]*$
    Result is FALSE: ^[lL]o[0-9.]*$
    Result is FALSE: ^[sS]ystem$
    Result is FALSE: ^Nu[0-9.]*$

在大多数系统上过滤掉环回和空接口。

[comment]: # ({/new-1dc52a1f})

[comment]: # ({new-b5f67cef})
#### 设备

可用模板的设备系列列表：

|模板名称                                                                              提供商|设备系列|已知模型|操作系统   已用MIB库|<|**[标签](/manual/config|templates_out_of_the_box/network_devices#tags)**|
|---------------------------------------------------------------------------------------------------|------------|------------|---------------------------|-|-------------------------|------------------------------------------------|
|*Template Net Alcatel Timetra TiMOS SNMPv2*|Alcatel|Alcatel Timetra|ALCATEL SR 7750|TiMOS|TIMETRA-SYSTEM-MIB,TIMETRA-CHASSIS-MIB|Certified|
|*Template Net Brocade FC SNMPv2*|Brocade|Brocade FC switches|Brocade 300 SAN Switch-|\-|SW-MIB,ENTITY-MIB|Performance, Fault|
|*Template Net Brocade\_Foundry Stackable SNMPv2*|Brocade|Brocade ICX|Brocade ICX6610, Brocade ICX7250-48, Brocade ICX7450-48F|<|FOUNDRY-SN-AGENT-MIB, FOUNDRY-SN-STACKING-MIB|Certified|
|*Template Net Brocade\_Foundry Nonstackable SNMPv2*|Brocade, Foundry|Brocade MLX, Foundry|Brocade MLXe, Foundry FLS648, Foundry FWSX424|<|FOUNDRY-SN-AGENT-MIB|Performance, Fault|
|*Template Net Cisco IOS SNMPv2*|Cisco|Cisco IOS ver > 12.2 3.5|Cisco C2950|IOS|CISCO-PROCESS-MIB,CISCO-MEMORY-POOL-MIB,CISCO-ENVMON-MIB|Certified|
|*Template Net Cisco releases later than 12.0\_3\_T and prior to 12.2\_3.5\_ SNMPv2*|Cisco|Cisco IOS > 12.0 3 T and < 12.2 3.5|\-|IOS|CISCO-PROCESS-MIB,CISCO-MEMORY-POOL-MIB,CISCO-ENVMON-MIB|Certified|
|*Template Net Cisco releases prior to 12.0\_3\_T SNMPv2*|Cisco|Cisco IOS < 12.0 3 T|\-|IOS|OLD-CISCO-CPU-MIB,CISCO-MEMORY-POOL-MIB|Certified|
|*Template Net D-Link DES\_DGS Switch SNMPv2*|D-Link|DES/DGX switches|D-Link DES-xxxx/DGS-xxxx,DLINK DGS-3420-26SC|\-|DLINK-AGENT-MIB,EQUIPMENT-MIB,ENTITY-MIB|Certified|
|*Template Net D-Link DES 7200 SNMPv2*|D-Link|DES-7xxx|D-Link DES 7206|\-|ENTITY-MIB,MY-SYSTEM-MIB,MY-PROCESS-MIB,MY-MEMORY-MIB|Performance Fault Interfaces|
|*Template Net Dell Force S-Series SNMPv2*|Dell|Dell Force S-Series|S4810|<|F10-S-SERIES-CHASSIS-MIB|Certified|
|*Template Net Extreme Exos SNMPv2*|Extreme|Extreme EXOS|X670V-48x|EXOS|EXTREME-SYSTEM-MIB,EXTREME-SOFTWARE-MONITOR-MIB|Certified|
|*Template Net Huawei VRP SNMPv2*|Huawei|Huawei VRP|S2352P-EI|\-|ENTITY-MIB,HUAWEI-ENTITY-EXTENT-MIB|Certified|
|*Template Net Intel\_Qlogic Infiniband SNMPv2*|Intel/QLogic|Intel/QLogic Infiniband devices|Infiniband 12300|<|ICS-CHASSIS-MIB|Fault Inventory|
|*Template Net Juniper SNMPv2*|Juniper|MX,SRX,EX models|Juniper MX240, Juniper EX4200-24F|JunOS|JUNIPER-MIB|Certified|
|*Template Net Mellanox SNMPv2*|Mellanox|Mellanox Infiniband devices|SX1036|MLNX-OS|HOST-RESOURCES-MIB,ENTITY-MIB,ENTITY-SENSOR-MIB,MELLANOX-MIB|Certified|
|*Template Net Mikrotik SNMPv2*|Mikrotik|Mikrotik RouterOS devices|Mikrotik CCR1016-12G, Mikrotik RB2011UAS-2HnD, Mikrotik 912UAG-5HPnD, Mikrotik 941-2nD, Mikrotik 951G-2HnD, Mikrotik 1100AHx2|RouterOS|MIKROTIK-MIB,HOST-RESOURCES-MIB|Certified|
|*Template Net QTech QSW SNMPv2*|QTech|Qtech devices|Qtech QSW-2800-28T|\-|QTECH-MIB,ENTITY-MIB|Performance Inventory|
|*Template Net Ubiquiti AirOS SNMPv1*|Ubiquiti|Ubiquiti AirOS wireless devices|NanoBridge,NanoStation,Unifi|AirOS|FROGFOOT-RESOURCES-MIB,IEEE802dot11-MIB|Performance|
|*Template Net HP Comware HH3C SNMPv2*|HP|HP (H3C) Comware|HP A5500-24G-4SFP HI Switch|<|HH3C-ENTITY-EXT-MIB,ENTITY-MIB|Certified|
|*Template Net HP Enterprise Switch SNMPv2*|HP|HP Enterprise Switch|HP ProCurve J4900B Switch 2626, HP J9728A 2920-48G Switch|<|STATISTICS-MIB,NETSWITCH-MIB,HP-ICF-CHASSIS,ENTITY-MIB,SEMI-MIB|Certified|
|*Template Net TP-LINK SNMPv2*|TP-LINK|TP-LINK|T2600G-28TS v2.0|<|TPLINK-SYSMONITOR-MIB,TPLINK-SYSINFO-MIB|Performance Inventory|
|*Template Net Netgear Fastpath SNMPv2*|Netgear|Netgear Fastpath|M5300-28G|<|FASTPATH-SWITCHING-MIB,FASTPATH-BOXSERVICES-PRIVATE-MIB|Fault Inventory|

[comment]: # ({/new-b5f67cef})

[comment]: # ({new-07c98c5a})
#### 模板设计

模板的设计考虑到以下因素：

-   尽可能使用用户宏，因此用户可以调整触发器
-   尽可能使用低级别发现来最小化不支持的项目数
-   为SNMPv2提供了模板。 如果已知大多数设备不支持SNMPv2，则使用SNMPv1。
-   所有模板都依赖于Template ICMP Ping，因此ICMP也会检查所有设备
-   项目不使用任何MIB - SNMP OID用于项目和低级别发现。
    因此，没有必要将任何MIB加载到Zabbix中以使模板工作。
-   发现接口以及ifAdminStatus = down（2）的接口时，环回网络接口被过滤
-   尽可能使用IF-MIB :: ifXTable中的64位计数器。
    如果不支持，则使用默认的32位计数器。
-   所有发现的网络接口都有一个控制其运行状态（链接）的触发器。
-   如果您不想监视特定接口的此条件，请创建具有值为0的上下文的用户宏。例如：

![](../../../../assets/en/manual/config/template_ifcontrol.png)

其中Gi0 / 0是{\#IFNAME}。 这样，触发器不再用于此特定接口。

    
\*您还可以更改所有触发器不会触发的默认行为，并仅将此触发器激活到有限数量的接口（如上行链路）

![](../../../../assets/en/manual/config/template_ifcontrol2.png)

[comment]: # ({/new-07c98c5a})

[comment]: # ({new-6d84c190})
#### 标签

-   性能 - 设备系列MIB提供了一种监控CPU和内存项的方法;
-   故障 - 设备系列MIB提供监控至少一个温度传感器的方法;
-   资产 - 设备系列MIB提供了至少收集设备序列号和型号名称的方法;
-   认证 - 涵盖上述所有三个主要类别。

[comment]: # ({/new-6d84c190})
