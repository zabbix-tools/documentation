[comment]: # translation:outdated

[comment]: # ({new-e02835c7})
# 3 触发器

[comment]: # ({/new-e02835c7})

[comment]: # ({91d7852a-d4a1b8b6})
#### 概述

触发器是“评估”监控项采集的数据和表示当前系统状况的逻辑表达式。

当我们使用监控项用于采集系统的数据时，一种不切合实际的情况是，我们无法始终等待一个警告或值得关注的条件。综上“评估”监控项数据的工作，我们可以留给触发器表达式。

触发器表达式允许定义一个什么状况的数据条件是“可接受”的阈值。因此，如果接收的数据超过了可接受的状态条件，则触发器会被触发
- 或将状态更改为异常。

一个触发器可以拥有下面几种状态：

|值|述|
|---|---|
|OK|这是一个正常的触发器状态。在旧版本的Zabbix中称为FALSE。|
|PROBLEM|通常意味着发生了某些事情。例如，CPU负载较高。在旧版本的Zabbix中称为TRUE。|

在基本触发器配置中，我们可能希望为某些监控数据的五分钟平均值设置告警阈值，例如 CPU 负载。这是通过定义一个触发器表达式来完成的，其中：

- 我们将“avg”函数应用于监控项键中收到的值
- 我们使用五分钟的时间进行评估
- 我们将阈值设置为“2”

```{=html}
avg(host/key,5m)>2
```
如果五分钟平均值超过 2，此触发器将“触发”（成为问题PROBLEM）。

在更复杂的触发器中，表达式可能包含**组合** 具有多种功能和多种阈值。也可以看看：[Trigger expression](/manual/config/triggers/expression).
  
大多数触发函数的评估基于[history](/manual/config/items/history_and_trends)数据，而一些 用于长期分析的触发函数，例如**趋势平均值（）**， **trendcount()** 等，使用趋势数据。

[comment]: # ({/91d7852a-d4a1b8b6})

[comment]: # ({new-7c263ca6})

::: notetip
After changing a trigger's status from *disabled* to *enabled*, the trigger expression will be evaluated as soon as an item associated with it receives a value or the time to handle a time-based function comes.
:::

[comment]: # ({/new-7c263ca6})

[comment]: # ({new-a45a4e79})

In a more complex trigger, the expression may include a **combination**
of multiple functions and multiple thresholds. See also: [Trigger
expression](/manual/config/triggers/expression).

Most trigger functions are evaluated based on
[history](/manual/config/items/history_and_trends) data, while some
trigger functions for long-term analytics, e.g. **trendavg()**,
**trendcount()**, etc, use trend data.

[comment]: # ({/new-a45a4e79})



[comment]: # ({new-6781ee38})
#### 计算时间

每次 Zabbix server接收到新值时，都会重新计算一次触发器 这是表达式的一部分。当接收到一个新值时，每个表达式中的函数都会重新计算（不仅仅是收到新值的那个）。

此外，每次出现新值时都会重新计算触发器 如果使用基于时间的函数，则每 30 秒接收一次**and** 表达方式。

如果在表达式中使用基于时间的函数(**nodata()**, **date()**, **dayofmonth()**,
**dayofweek()**, **time()**, **now()**)，触发器就会由Zabbix history syncer进程每30秒重新计算一次。如果在表达式中同时使用基于时间和非基于时间的函数，当接收到一个新值和每隔30秒都会重新计算触发器的状态。


[comment]: # ({/new-6781ee38})

[comment]: # ({6534dd6b-1fa7644e})
#### 评估周期

在引用监控项历史的函数中使用评估周期。 它允许指定我们感兴趣的间隔。它可以是 指定为时间段 (30s, 10m, 1h) 或值范围 (#5 - for 五个最近值）。

评估周期测量到“now(现在)” - 其中“now(现在)”是 触发器的最新重新计算时间（参见[Calculation time](#calculation_time) 上面）； “now(现在)”不是现在的“now(现在)”服务器时间。

评估周期指定：

- 考虑“now-time period（现在时间周期）”和“now(现在)”之间的所有值（或者，与 时间偏移量，在“now-time shift-time period(现在时间偏移点时间区间)”和 “now-time\_shift(现在\偏移量)”）
- 考虑不超过过去的值的数量，向上 到“now(现在)”
- 如果时间周期或计数有 0 个可用值 指定 - 然后是使用它的触发器或计算项 功能变得不受支持.

注意：

- 如果在触发器中只使用一个函数(引用监控数据历史)，那么“now（现在）”总是最新收到的值。例如，如果最后一个值是在一小时前收到的，那么评估期将被视为一小时前的最新值。
- 当第一个监控值被接收时，一个新的触发器被计算出来(history functions 历史函数);基于时间的函数将在30秒内计算。因此，即使设置的评估周期(例如 一个小时)自触发器创建以来还没有过去，也将计算触发器。触发器也将在第一个值之后计算，即使计算范围被设置为10个最新的值。

[comment]: # ({/6534dd6b-1fa7644e})

[comment]: # ({new-cad1df72})

#### Unknown state

It is possible that an unknown operand appears in a trigger expression if:

-   an unsupported item is used
-   the function evaluation for a supported item results in an error

In this case a trigger generally evaluates to "unknown" (although there are some exceptions). For more details, see [Expressions with unknown operands](/manual/config/triggers/expression#expressions-with-unknown-operands).

It is possible to [get notified](/manual/config/events/sources#internal-events) on unknown triggers.

[comment]: # ({/new-cad1df72})

