[comment]: # translation:outdated

[comment]: # ({new-6d7c832e})
# 5 事件关联

[comment]: # ({/new-6d7c832e})

[comment]: # ({ae95048b-fff764b2})
#### 概述

事件关联允许以非常精确灵活地方式关联问题事件和它们的解决方法。

事件关联可以被定义为：

-   [触发器级别](/manual/config/event_correlation/trigger)——不同的问题和解决方法可以关联同一个触发器
-   [全局](/manual/config/event_correlation/global)——使用全局关联规则可以通过不同的触发器轮询训方法将问题和它们的解决方法关联起来

[comment]: # ({/ae95048b-fff764b2})
