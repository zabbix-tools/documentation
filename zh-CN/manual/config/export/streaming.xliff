<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="zh-CN" datatype="plaintext" original="manual/config/export/streaming.md">
    <body>
      <trans-unit id="b45624e8" xml:space="preserve">
        <source># 2 Streaming to external systems</source>
      </trans-unit>
      <trans-unit id="c479d2fd" xml:space="preserve">
        <source>#### Overview

It is possible to stream item values and events from Zabbix to external systems over HTTP (see [protocol details](#protocol)).

:::notewarning
This feature currently has experimental status.
:::

The tag filter can be used to stream subsets of item values or events.

Two Zabbix server processes are responsible for data streaming: `connector manager` and `connector worker`. A Zabbix internal item `zabbix[connector_queue]` allows to monitor the count of values enqueued in the connector queue.</source>
      </trans-unit>
      <trans-unit id="cfad3c2f" xml:space="preserve">
        <source>#### Configuration

The following steps are required to configure data streaming to an external system:

**1.** Have a remote system set up for receiving data from Zabbix.

See the documentation of a simple [receiver](https://git.zabbix.com/projects/ZT/repos/receiver/browse). The receiver currently logs the received information in events.ndjson and history.ndjson files.

**2.** Set the required number of connector workers in Zabbix (see [StartConnectors](/manual/appendix/config/zabbix_server#startconnectors) in zabbix_server.conf). Restart the Zabbix server.

**3.** Configure a new connector in Zabbix (*Administration* -&gt; *General* -&gt; *Connectors*) and reload the server cache with "zabbix_server -R config_cache_reload".

![](../../../../assets/en/manual/config/connector.png)

Mandatory fields are marked by an asterisk.

|Parameter|Description|
|--|--------|
|*Name*|Enter the connector name. |
|*Data type*|Select the data type: *Item values* or *Events*. |
|*URL*|Enter the receiver URL. User macros are supported. |
|*Tag filter*|Export only values or events matching the tag filter. If not set, then export everything.&lt;br&gt;It is possible to include as well as exclude specific tags and tag values. Several conditions can be set. Tag name matching is always case-sensitive.&lt;br&gt;There are several operators available for each condition:&lt;br&gt;**Exists** - include the specified tag names&lt;br&gt;**Equals** - include the specified tag names and values (case-sensitive)&lt;br&gt;**Contains** - include the specified tag names where the tag values contain the entered string (substring match, case-insensitive)&lt;br&gt;**Does not exist** - exclude the specified tag names&lt;br&gt;**Does not equal** - exclude the specified tag names and values (case-sensitive)&lt;br&gt;**Does not contain** - exclude the specified tag names where the tag values contain the entered string (substring match, case-insensitive)&lt;br&gt;There are two calculation types for conditions:&lt;br&gt;**And/Or** - all conditions must be met, conditions having the same tag name will be grouped by the Or condition&lt;br&gt;**Or** - enough if one condition is met |
|*HTTP authentication*|Select the authentication option:&lt;br&gt;**None** - no authentication used;&lt;br&gt;**Basic** - basic authentication is used;&lt;br&gt;**NTLM** - NTLM ([Windows NT LAN Manager](http://en.wikipedia.org/wiki/NTLM)) authentication is used;&lt;br&gt;**Kerberos** - Kerberos authentication is used (see also: [Configuring Kerberos with Zabbix](/manual/appendix/items/kerberos));&lt;br&gt;**Digest** - Digest authentication is used;&lt;br&gt;**Bearer** - Bearer authentication is used.|
|*Username*|Enter the user name (up to 255 characters).&lt;br&gt;This field is available if *HTTP authentication* is set to Basic, NTLM, Kerberos, or Digest. User macros are supported. |
|*Password*|Enter the user password (up to 255 characters).&lt;br&gt;This field is available if *HTTP authentication* is set to Basic, NTLM, Kerberos, or Digest. User macros are supported. |
|*Bearer token*|Enter the Bearer token.&lt;br&gt;This field is available and required if *HTTP authentication* is set to Bearer. User macros are supported. |
|*Advanced configuration*|Click on the *Advanced configuration* label to display advanced configuration options (see below). |
|*Max records per message*|Specify the maximum number of values or events that can be sent within one message. |
|*Concurrent sessions*|Select the number of sender processes to run for this connector. Up to 100 sessions can be specified; the default value is '1'.|
|*Attempts*|Number of attempts for trying to send the data. Up to 5 attempts can be specified; the default value is '1'.|
|*Timeout*|Specify the message timeout (1-60 seconds, default 5 seconds).&lt;br&gt;Time suffixes are supported, e.g. 30s, 1m. User macros are supported. |
|*HTTP proxy*|You can specify an HTTP proxy to use, using the format `[protocol://][username[:password]@]proxy.example.com[:port]`.&lt;br&gt;The optional `protocol://` prefix may be used to specify alternative proxy protocols (the protocol prefix support was added in cURL 7.21.7). With no protocol specified, the proxy will be treated as an HTTP proxy.&lt;br&gt;By default, 1080 port will be used.&lt;br&gt;If specified, the proxy will overwrite proxy related environment variables like http\_proxy, HTTPS\_PROXY. If not specified, the proxy will not overwrite proxy-related environment variables. The entered value is passed on "as is", no sanity checking takes place.&lt;br&gt;You may also enter a SOCKS proxy address. If you specify the wrong protocol, the connection will fail and the item will become unsupported.&lt;br&gt;*Note* that only simple authentication is supported with HTTP proxy.&lt;br&gt;User macros are supported.|
|*SSL verify peer*|Mark the checkbox to verify the SSL certificate of the web server.&lt;br&gt;The server certificate will be automatically taken from system-wide certificate authority (CA) location. You can override the location of CA files using Zabbix server or proxy configuration parameter [SSLCALocation](/manual/appendix/config/zabbix_server).|
|*SSL verify host*|Mark the checkbox to verify that the *Common Name* field or the *Subject Alternate Name* field of the web server certificate matches.&lt;br&gt;This sets the [CURLOPT\_SSL\_VERIFYHOST](http://curl.haxx.se/libcurl/c/CURLOPT_SSL_VERIFYHOST.html) cURL option.|
|*SSL certificate file*|Name of the SSL certificate file used for client authentication. The certificate file must be in PEM^1^ format. If the certificate file contains also the private key, leave the *SSL key file* field empty. If the key is encrypted, specify the password in *SSL key password* field. The directory containing this file is specified by Zabbix server or proxy configuration parameter [SSLCertLocation](/manual/appendix/config/zabbix_server). User macros are supported. |
|*SSL key file*|Name of the SSL private key file used for client authentication. The private key file must be in PEM^1^ format. The directory containing this file is specified by Zabbix server or proxy configuration parameter [SSLKeyLocation](/manual/appendix/config/zabbix_server). User macros are supported. |
|*SSL key password*|SSL private key file password. User macros are supported. |
|*Description*|Enter the connector description. |
|*Enabled*|Mark the checkbox to enable the connector. |</source>
      </trans-unit>
      <trans-unit id="b67df609" xml:space="preserve">
        <source>#### Protocol

Communication between the server and the receiver is done over HTTP using REST API, NDJSON, "Content-Type: application/x-ndjson".

For more details, see [newline-delimited JSON export protocol](/manual/appendix/protocols/real_time_export).</source>
      </trans-unit>
      <trans-unit id="ae5d9204" xml:space="preserve">
        <source>##### Server request

Example of sending item values:

```json
POST /v1/history HTTP/1.1
Host: localhost:8080
Accept: */*
Accept-Encoding: deflate, gzip, br, zstd
Content-Length: 628
Content-Type: application/x-ndjson
 
{"host":{"host":"Zabbix server","name":"Zabbix server"},"groups":["Zabbix servers"],"item_tags":[{"tag":"foo","value":"test"}],"itemid":44457,"name":"foo","clock":1673454303,"ns":800155804,"value":0,"type":3}
{"host":{"host":"Zabbix server","name":"Zabbix server"},"groups":["Zabbix servers"],"item_tags":[{"tag":"foo","value":"test"}],"itemid":44457,"name":"foo","clock":1673454303,"ns":832290669,"value":1,"type":3}
{"host":{"host":"Zabbix server","name":"Zabbix server"},"groups":["Zabbix servers"],"item_tags":[{"tag":"bar","value":"test"}],"itemid":44458,"name":"bar","clock":1673454303,"ns":867770366,"value":123,"type":3}
```

Example of sending events:

```json
POST /v1/events HTTP/1.1
Host: localhost:8080
Accept: */*
Accept-Encoding: deflate, gzip, br, zstd
Content-Length: 333
Content-Type: application/x-ndjson
 
{"clock":1673454303,"ns":800155804,"value":1,"eventid":5,"name":"trigger for foo being 0","severity":0,"hosts":[{"host":"Zabbix server","name":"Zabbix server"}],"groups":["Zabbix servers"],"tags":[{"tag":"foo_trig","value":"test"},{"tag":"foo","value":"test"}]}
{"clock":1673454303,"ns":832290669,"value":0,"eventid":6,"p_eventid":5}
```</source>
      </trans-unit>
      <trans-unit id="89101d44" xml:space="preserve">
        <source>##### Receiver response

The response consists of the HTTP response status code and the JSON payload. The HTTP response status code must be "200" for requests that were handled successfully, other for failed requests.

Example of success:

```json
localhost:8080/v1/history": HTTP/1.1 200 OK
Date: Wed, 11 Jan 2023 16:40:30 GMT
Content-Length: 0
```

Example with errors:

```json
localhost:8080/v1/history": HTTP/1.1 422 Unprocessable Entity
Content-Type: application/json
Date: Wed, 11 Jan 2023 17:07:36 GMT
Content-Length: 55
 
{"error":"invalid character '{' after top-level value"}
```</source>
      </trans-unit>
    </body>
  </file>
</xliff>
