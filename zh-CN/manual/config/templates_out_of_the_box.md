[comment]: # translation:outdated

[comment]: # ({new-f733158d})
# 8 模板开箱即用

[comment]: # ({/new-f733158d})

[comment]: # ({new-0cc95643})
#### 概述

Zabbix致力于提供越来越多有用的开箱即用[templates](：manual / config / templates)列表。
开箱即用的模板已预先配置，因此是加速监视作业部署的有用方法。

[comment]: # ({/new-0cc95643})
