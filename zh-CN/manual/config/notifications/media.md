[comment]: # translation:outdated

[comment]: # ({new-e88c898d})
# 1 媒介类型

[comment]: # ({/new-e88c898d})

[comment]: # ({d6eae488-3c7a0fa2})


#### 概述

媒介的定义确定了 Zabbix 发送通知和告警的渠道。

您可以配置多种媒介类型：

-   [电子邮箱](/manual/config/notifications/media/email)
-   [短信](/manual/config/notifications/media/sms)
-   [自定义报警脚本](/manual/config/notifications/media/script)
-   [Webhook](/manual/config/notifications/media/webhook)

媒介类型的配置位于 *管理* → *媒介类型*。

![](../../../../assets/en/manual/config/notifications/media_type_list.png){width="600"}

某些媒介类型是在默认数据中完成了自定义的。您只需对其配置参数进行一些微调操作，即可正常运行。

媒介类型配置的正确与否，是可以通过点击对应媒介类型中最后一行的 *测试* 按钮进行测试的 (请参考 [媒介类型测试](#media_type_testing)）。

创建一个新的媒介类型，可以通过点击 *创建媒介类型* 按钮来完成。点击该按钮后，即可打开媒介类型的配置表单。



[comment]: # ({/d6eae488-3c7a0fa2})

[comment]: # ({0868d8f6-ec50bee8})

#### 通用参数

某些媒介类型的参数是通用参数。

![](../../../../assets/en/manual/config/notifications/media_types_common.png)

位于 **媒介类型** 选项卡中的常见一般属性包括：

|参数|说明|
|---------|-----------|
|*名称*|媒介类型的名称。|
|*类型*|选择媒介的类型。|
|*描述*|对该媒介类型的描述。|
|*启用*|通过勾选复选框来启用该媒介类型。|

有关媒介的具体参数，请参考各个媒体类型的说明页面。

用户可以在**消息模板** 选项卡中，对下列所有或者某些事件类型进行相关的默认通知消息配置：

-   问题
-   问题恢复
-   问题更新
-   业务
-   业务恢复
-   业务更新
-   自动发现
-   自动注册
-   内部问题
-   内部问题恢复

![](../../../../assets/en/manual/config/notifications/media_type_messages.png)

自定义消息模板需要：

-   在 *消息模板* 选项卡中，点击
    ![](../../../../assets/en/manual/config/add_link.png)：将打开 *消息模板* 弹出窗口。
-   选择需要的 *消息类型* ，编辑 *主题* 和添加 *消息* 文本内容。
-   点击 *添加* 按钮来保存该消息模板。

![](../../../../assets/en/manual/config/notifications/media/media_types2.png)

消息模板的配置参数：

|参数|<|<|<|<|说明|
|---------|-|-|-|-|-----------|
|*消息类型*|<|<|<|<|选择事件类型，该参数定义默认消息类型。<br>每种事件类型只支持定义一个默认消息。<br>|
|*主题*|<|<|<|<|默认的消息主题。该主题可以包含宏参数。主题的长度限定在255个字符。<br>SMS类型的媒介并不支持配置主题参数。|
|*消息*|<|<|<|<|默认的消息内容。根据数据库类型，消息内容被限制在一定的字符数量以内(更多信息，请参考 [消息发送](/manual/config/notifications/action/operation/message/) 中的内容)。<br>消息内可以包含支持的[宏](/manual/appendix/macros/supported_by_location)。<br>在问题和问题更新消息类型中，均支持宏表达式的应用 (举例来说， `{?avg(/host/key,1h)}`)。|

对现有的消息模板进行修改，需要：在 *动作* 一行中点击 ![](../../../../assets/en/manual/config/edit_link.png) 来编辑模板或者点击 ![](../../../../assets/en/manual/config/remove_link.png) 来删除消息模板。

用户也可以根据需求对一个自定义的消息模板定义一个特定的动作（相关具体内容，请参考 [动作实施](/manual/config/notifications/action/operation#operation_details)）。动作配置中的用户自定义消息会覆盖默认媒介类型的消息模板。

::: 注意警告
 消息模板的定义是必要且强制性的，其配置内容包括配置不使用默认消息通知的 webhooks 或者应用自定义报警脚本。举例来说，若未定义 Pushover webhook 的具体问题消息，那么针对动作“对Pushover webhook发送消息”将不会有任何通知发送。
:::

 **选择项** 选项卡包含了报警处理设置。每种媒介类型都可以配置相同的选项集。

所有媒介类型都是并行进行处理的。尽管，对每种媒介类型的最大并存会话数是可以进行配置的，但是在服务器上的报警进程总数是由参数  StartAlerters [参数](/manual/appendix/config/zabbix_server)进行限制的。由同一个触发器生成的报警信息会按照次序依次进行处理。从这点来看，只有配置多个触发器才可能同时生成多个报警。

![](../../../../assets/en/manual/config/notifications/media/media_type_options.png)

|参数|说明|
|---------|-----------|
|*并发会话*|选择媒介类型的并行报警会话数量：<br>**壹** - 单一对话<br>**无限制** - 无限制的对话数量<br>**自定义** - 对对话数量进行自定义设置<br> 无限制/自定义下的大数值设置，意味着发送通知会同时拥有多个会话并且会话数量也会增长。<br>无限制/自定义下的大数值设置，应在有大量通知需要同时发送的情境下使用。<br>如果需要发送的通知数量多于并发会话所支持的数量，那么剩余的通知则会排队等待；余下的通知并不会被系统丢弃。|
|*尝试次数*|尝试发送通知的次数。该参数最大可配置数值为100；该参数默认数值为‘3’。如果该参数被设定为‘1’，那么 Zabbix 只会发送一次通知，即便发送失败也不会尝试再次发送。|
|*尝试间隔*|当发送通知失败时，重新尝试发送通知的频率，单位为秒（0-3600）。若该参数设定为‘0’，那么在发送通知失败后，Zabbix 会立即重发。<br>支持定义时间后缀，例如：5s，3m，1h。|



[comment]: # ({/0868d8f6-ec50bee8})







[comment]: # ({55841c02-cb94600e})
#### 用户媒介

仅仅配置媒介类型并不能使用户收到提示信息，完成通知功能还需要在用户属性中，对媒介类型定义传送消息的媒介（例如，电子邮箱/手机号码/webhook 用户账号，等）。 <br>举例来说，用户 “Admin” 未在用户属性中定义 webhook “X” 的媒介，那么该用户就无法收到以 webhook “X” 为媒介类型的消息通知。

定义用户媒介，应：

-   在您的用户属性，或通过 *管理 → 用户* 来打开用户属性表单进行配置
-   在报警媒介中，点击
    ![](../../../../assets/en/manual/config/add_link.png)

![](../../../../assets/en/manual/config/notifications/media/user_media2.png)

用户媒介的标志性参数包括：

|参数|说明|
|---------|-----------|
|*类型*|所有已配置的媒介类型都会包含在下拉列表中。|
|*发送至*|提供需要发送消息的目标联系信息。<br><br>对电子邮件媒介类型来讲，可以通过点击电子邮箱栏下方![](../../../../assets/en/manual/config/add_link.png) 来添加多个地址。通过这种添加方式，系统会发送通知给所有已添加的电子邮箱。当然也可以在 *发送至* 信息栏中明确收件人信息，填写格式为 'Recipient name <address1\@company.com>'。 请注意，在收件人名称已经填写的情况下，用符号(<>)囊括目标电子邮箱。收件人名称支持使用 UTF-8 字符，不支持引号和注释服务。例如：*John Abercroft <manager\@nycdatacenter.com>* 和 *manager\@nycdatacenter.com* 都是有效的格式。<br>错误示范：*John Doe zabbix\@company.com*, *%%"Zabbix\\@\\<H(comment)Q\\>" <zabbix@company.com> %%*.|
|*何时发送*|您可以对消息的发送时间进行限制，比如，设定消息的发送时间仅限在工作日（1-5,09:00-18:00）。 请注意您所处在的时区[时区](/manual/web_interface/time_zone)。 为了避免错失重要消息，请您根据您所处在的时区对系统时区进行设置。<br>有关时间格式的具体描述，请参考[时间周期格式](/manual/appendix/time_period)。|
|*告警级别设置*|您可以通过勾选触发器严重程度来筛选希望收到何种程度的消息通知。<br>*请注意* 如果您想接收由非触发器引起的 [事件](/manual/config/events)通知，请 **一定** 要勾选默认程度（‘未分级’）。<br>保存该配置后，选中的触发级别将以对应的级别颜色显示，未分级类别将以灰色显示。|
|*状态*|用户媒介的状态。<br>**开启** - 使用中。<br>**禁用** - 禁止使用。|



[comment]: # ({/55841c02-cb94600e})
