[comment]: # translation:outdated

[comment]: # ({new-931a392a})
# 3 接收不受支持的项目的通知

[comment]: # ({/new-931a392a})

[comment]: # ({new-436ff6e8})
## 3 Receiving notification on unsupported items

[comment]: # ({/new-436ff6e8})

[comment]: # ({new-29f60fbd})
#### 概述

[comment]: # ({/new-29f60fbd})

[comment]: # ({new-6b217825})
#### Overview

Zabbix 2.2之后支持接收不支持的项目的通知。

Receiving notifications on unsupported items is supported since Zabbix
2.2.

它是Zabbix内部事件概念的一部分，允许用户在这些场合获得通知。内部事件反映了状态的变化：

It is part of the concept of internal events in Zabbix, allowing users
to be notified on these occasions. Internal events reflect a change of
state:

-   当监控项从“正常”变成“不支持”（或反之，即从“不支持”变成“正常”）
-   当触发器从“正常”改为“未知”（或反之，即从“未知”改为“正常”）
-   当低级发现规则从“正常”到“不支持”（或反之，即从“不支持”到“正常”）

```{=html}
<!-- -->
```
-   when items go from 'normal' to 'unsupported' (and back)
-   when triggers go from 'normal' to 'unknown' (and back)
-   when low-level discovery rules go from 'normal' to 'unsupported'
    (and back)

本节介绍了当项目不受支持时**接收通知**的操作方法。

This section presents a how-to for **receiving notification** when an
item turns unsupported.

[comment]: # ({/new-6b217825})

[comment]: # ({new-88b88d25})
#### 配置

[comment]: # ({/new-88b88d25})

[comment]: # ({new-9e5f70bb})
#### Configuration

配置 [一些媒介](media), 例如电子邮件，短信或Jabber，用于通知。
请参阅手册的相应章节执行此任务。

Overall, the process of setting up the notification should feel familiar
to those who have set up alerts in Zabbix before.

[comment]: # ({/new-9e5f70bb})

[comment]: # ({new-395fb8cf})
##### 步骤 1

[comment]: # ({/new-395fb8cf})

[comment]: # ({new-921fcf26})
##### Step 1

配置 [一些媒介](media), 例如电子邮件，短信或Jabber，用于通知。
请参阅手册的相应章节执行此任务。

Configure [some media](media), such as e-mail, SMS or Jabber, to use for
the notifications. Refer to the corresponding sections of the manual to
perform this task.

<note
important>为了通知内部事件，使用默认严重性（'未分类'），因此如果要接收内部事件的通知，请在配置[用户媒介](/manual/config/notifications/media/email#user_media)时选中。

:::

::: noteimportant
For notifying on internal events the default
severity ('Not classified') is used, so leave it checked when
configuring [user
media](/manual/config/notifications/media/email#user_media) if you want
to receive notifications for internal events.
:::

[comment]: # ({/new-921fcf26})

[comment]: # ({new-9035a0ae})
##### 步骤 2

##### Step 2

进入*配置 - >操作* 选择 *内部* 作为事件来源.点击右上角*创建action*
开一个动作配置表单。

Go to *Configuration→Actions* and select *Internal* as the event source.
Click on *Create action* on the upper right to open an action
configuration form.

![](../../../../assets/en/manual/config/notif_unsupp.png){width="600"}

##### 步骤 3

##### Step 3

在**动作**选项卡中输入操作的名称。然后在新条件块中选择
*事件类型*，选择*项目处于“不支持”状态* 作为值.

In the **Action** tab enter a name for the action. Then select *Event
type* in the New condition block and select *Item in "not supported"
state* as the value.

![](../../../../assets/en/manual/config/notif_unsupp_action.png)

不要忘记点击*Add*来实际列出*条件*块中的条件。

Don't forget to click on *Add* to actually list the condition in the
*Conditions* block.

##### 步骤 4

##### Step 4

在**操作**选项卡中，输入问题消息的主题/内容。

In the **Operations** tab, enter the subject/content of the problem
message.

点击 *操作* 模块中的
*New*，并且选择消息的一些接收者（用户组/用户）和用于传送的媒体类型（或“全部”）。

Click on *New* in the *Operations* block and select some recipients of
the message (user groups/users) and the media types (or 'All') to use
for delivery.

![](../../../../assets/en/manual/config/notif_unsupp_oper.png)

点击*操作细节*块中的*Add*，添加 *操作* 模块中实际所包含的操作。

Click on *Add* in the *Operation details* block to actually list the
operation in the *Operations* block.

如果您希望收到多个通知，请设置操作步骤持续时间（发送消息之间的间隔）并添加其他操作。

If you wish to receive more than one notification, set the operation
step duration (interval between messages sent) and add another
operation.

##### 步骤 5

##### Step 5

**恢复操作**选项卡允许在项目恢复到正常状态时配置恢复通知。

The **Recovery operations** tab allows to configure a recovery
notification when an item goes back to the normal state.

输入恢复信息的主题/内容。

Enter the subject/content of the recovery message.

点击 *操作* 模块中的
*New*，并且选择消息的一些接收者（用户组/用户）和用于传送的媒体类型（或“全部”）。

Click on *New* in the *Operations* block and select some recipients of
the message (user groups/users) and the media types (or 'All') to use
for delivery.

![](../../../../assets/en/manual/config/notif_unsupp_recovery.png)

点击*操作细节*块中的*Add*，添加 *操作* 模块中实际所包含的操作。

Click on *Add* in the *Operation details* block to actually list the
operation in the *Operations* block.

##### 步骤 6

##### Step 6

完成后，单击表单下方的“添加\*\*”按钮。

When finished, click on the **Add** button underneath the form.

就这样，你完成了！
现在，如果某些项目不受支持，您可以期待收到Zabbix的第一个通知。

And that's it, you're done! Now you can look forward to receiving your
first notification from Zabbix if some item turns unsupported.

[comment]: # ({/new-9035a0ae})
