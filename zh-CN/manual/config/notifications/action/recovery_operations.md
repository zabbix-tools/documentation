[comment]: # translation:outdated

[comment]: # ({new-10ba847f})
# -\#3 恢复操作

[comment]: # ({/new-10ba847f})

[comment]: # ({new-e5432fa9})
### 3 Recovery operations

[comment]: # ({/new-e5432fa9})

[comment]: # ({new-960d3247})
#### 概述

[comment]: # ({/new-960d3247})

[comment]: # ({new-82501aec})
#### Overview

恢复操作允许在问题解决时通知您。

Recovery operations allow you to be notified when problems are resolved.

恢复操作支持消息和远程命令。恢复操作不支持通知升级 -
因为所有操作都分配到一个单独的步骤。

Both messages and remote commands are supported in recovery operations.
Recovery operations do not support escalating - all operations are
assigned to a single step.

[comment]: # ({/new-82501aec})

[comment]: # ({new-f2127d8b})
#### 使用场景

#### Use cases

恢复操作的一些用例如下：

Some use cases for recovery operations are as follows:

1.  通知所有通知有关问题的用户

```{=html}
<!-- -->
```
       * 选择“发送恢复消息”作为操作类型
    - 恢复时有多个操作：发送通知并执行远程命令
       * 添加发送消息和执行命令的操作类型
    - 在外部帮助台/票务系统中打开机票，并在问题解决时将其关闭
       * 创建一个与帮助台系统通信的外部脚本
       * 创建一个操作，该操作具有执行此脚本的操作，从而打开一张票据
       * 恢复操作，使用其他参数执行此脚本并关闭故障单
       * 使用{EVENT.ID}宏来引用原始问题

1.  Notify all users that were notified on the problem

```{=html}
<!-- -->
```
       * Select 'Send recovery message' as operation type
    - Have multiple operations upon recovery: send a notification and execute a remote command
       * Add operation types for sending a message and executing a command
    - Open a ticket in external helpdesk/ticketing system and close it when the problem is resolved
       * Create an external script that communicates with the helpdesk system
       * Create an action having operation that executes this script and thus opens a ticket
       * Have a recovery operation that executes this script with other parameters and closes the ticket
       * Use the {EVENT.ID} macro to reference the original problem

#### 配置恢复操作

#### Configuring a recovery operation

配置恢复操作：

To configure a recovery operation:

-   进入action[配置](/manual/config/notifications/action)中的*恢复操作*标签
-   点击操作块中的*New*
-   编辑操作详情并且点击 *Add*

```{=html}
<!-- -->
```
-   Go to the *Recovery operations* tab in action
    [configuration](/manual/config/notifications/action)
-   Click on *New* in the Operations block
-   Edit the operation details and click on *Add*

以添加几个操作。

Several operations can be added.

恢复操作属性：

Recovery operation attributes:

![](../../../../../assets/en/manual/config/recovery_operation1.png){width="600"}

All mandatory input fields are marked with a red asterisk.

|参数|<|<|<|说|<|
|------|-|-|-|---|-|
|*Default subject*|<|<|<|<|恢复通知的默认消息主题. 主题可能包含 [宏](/manual/config/notifications/action/operation/macros).|
|*Default message*|<|<|<|<|恢复通知的默认消息. 消息可能包含 [宏](/manual/config/notifications/action/operation/macros).|
|*Operations*|<|<|<|<|恢复操作详细信息显示.<br>要配置新的恢复操作，请单击 *New*.<br>|
|*Operation details*|<|<|<|<|此块用于配置恢复操作的详细信息.|
|<|*Operation type*|<|<|<|有三种操作类型可用于恢复事件：<br>**Send recovery message** - 所有在问题事件通知的用户发送恢复消息<br>**Send message** - 发送恢复信息给指定的用户<br>**Remote command** - 执行远程命令|
|^|<|操作类型：发送恢复信息|<|<|<|
|^|^|*Default message*|<|<|如果选择，将使用默认消息（见上文）.|
|^|^|*Subject*|<|<|自定义消息的主题. 主题可能包含宏.|
|^|^|*Message*|<|<|自定义消息. 消息可能包含宏.|
|^|<|操作类型：[发信息](/manual/config/notifications/action/operation/message)|<|<|<|
|^|^|*发送到用户组*|点击|*A|d* 选择要发送恢复消息的用户组.<br>用户组必须至少具有“读取”[权限](/manual/config/users_and_usergroups/permissions)向主机通知.|
|^|^|*Send to users*|<|<|点击 *Add* 选择要发送恢复消息的用户组.<br>用户组必须至少具有“读取”" [权限](/manual/config/users_and_usergroups/permissions) 向主机通知.|
|^|^|*Send only to*|<|<|将恢复消息发送到所有定义的媒体类型或仅选定的媒体类型.|
|^|^|*Default message*|<|<|如果选择, 将使用默认消息（见上文）.|
|^|^|*Subject*|<|<|自定义消息的主题. 主题可能包含宏.|
|^|^|*Message*|<|<|自定义消息. 消息可能包含宏.|
|^|^|Operation type: [远程命令](/manual/config/notifications/action/operation/remote_command)|<|<|<|
|^|^|*Target list*|<|<|选择当前主机，其他主机或主机组作为目标执行命令.|
|^|^|*Type*|<|<|选择命令类型：<br>**IPMI** - 执行IPMI命令<br>**Custom script** - 执行一组自定义的命令. 您可以选择在Zabbix代理或Zabbix服务器上执行该命令。<br>**SSH** - 执行SSH命令<br>**Telnet** - 执行Telnet命令<br>**Global script** -执行*管理 - >脚本*其中定义的全局脚本之一.|
|^|^|*Execute on*|<|<|在Zabbix代理或Zabbix服务器上执行命令.|
|^|^|*Commands*|<|<|输入命令.|

|Parameter|<|<|<|<|Description|
|---------|-|-|-|-|-----------|
|*Default subject*|<|<|<|<|Default message subject for recovery notifications. The subject may contain [macros](/manual/config/notifications/action/operation/macros).|
|*Default message*|<|<|<|<|Default message for recovery notifications. The message may contain [macros](/manual/config/notifications/action/operation/macros).|
|*Operations*|<|<|<|<|Recovery operation details are displayed.<br>To configure a new recovery operation, click on *New*.<br>|
|*Operation details*|<|<|<|<|This block is used to configure the details of a recovery operation.|
|<|*Operation type*|<|<|<|Three operation types are available for recovery events:<br>**Send message** - send recovery message to specified user<br>**Remote command** - execute a remote command<br>**Notify all involved** - send recovery message to all users who were notified on the problem event<br>Note that if the same recipient with unchanged default subject/message is defined in several operation types, duplicate notifications are not sent.|
|^|<|Operation type: [send message](/manual/config/notifications/action/operation/message)|<|<|<|
|^|^|*Send to user groups*|<|<|Click on *Add* to select user groups to send the recovery message to.<br>The user group must have at least "read" [permissions](/manual/config/users_and_usergroups/permissions) to the host in order to be notified.|
|^|^|*Send to users*|<|<|Click on *Add* to select users to send the recovery message to.<br>The user must have at least "read" [permissions](/manual/config/users_and_usergroups/permissions) to the host in order to be notified.|
|^|^|*Send only to*|<|<|Send recovery message to all defined media types or a selected one only.|
|^|^|*Default message*|<|<|If selected, the default message will be used (see above).|
|^|^|*Subject*|<|<|Subject of the custom message. The subject may contain macros.|
|^|^|*Message*|<|<|The custom message. The message may contain macros.|
|^|^|Operation type: [remote command](/manual/config/notifications/action/operation/remote_command)|<|<|<|
|^|^|*Target list*|<|<|Select targets to execute the command on:<br>**Current host** - command is executed on the host of the trigger that caused the problem event. This option will not work if there are multiple hosts in the trigger.<br>**Host** - select host(s) to execute the command on.<br>**Host group** - select host group(s) to execute the command on. Specifying a parent host group implicitly selects all nested host groups. Thus the remote command will also be executed on hosts from nested groups.<br>A command on a host is executed only once, even if the host matches more than once (e.g. from several host groups; individually and from a host group).<br>The target list is meaningless if the command is executed on Zabbix server. Selecting more targets in this case only results in the command being executed on the server more times.<br>Note that for global scripts, the target selection also depends on the *Host group* setting in global script [configuration](/manual/web_interface/frontend_sections/administration/scripts#configuring_a_global_script).|
|^|^|*Type*|<|<|Select the command type:<br>**IPMI** - execute an [IPMI command](/manual/config/notifications/action/operation/remote_command#ipmi_remote_commands)<br>**Custom script** - execute a custom set of commands<br>**SSH** - execute an SSH command<br>**Telnet** - execute a Telnet command<br>**Global script** - execute one of the global scripts defined in *Administration→Scripts*.|
|^|^|*Execute on*|<|<|Execute a custom script on:<br>**Zabbix agent** - the script will be executed by Zabbix agent on the host<br>**Zabbix server (proxy)** - the script will be executed by Zabbix server or proxy - depending on whether the host is monitored by server or proxy<br>**Zabbix server** - the script will be executed by Zabbix server only<br>To execute scripts on the agent, it must be [configured](/manual/appendix/config/zabbix_agentd) to allow remote commands from the server.<br>This field is available if 'Custom script' is selected as *Type*.|
|^|^|*Commands*|<|<|Enter the command(s).<br>Supported macros will be resolved based on the trigger expression that caused the event. For example, host macros will resolve to the hosts of the trigger expression (and not of the target list).|
|^|<|Operation type: notify all involved|<|<|<|
|^|^|*Default message*|<|<|If selected, the default message will be used (see above).|
|^|^|*Subject*|<|<|Subject of the custom message. The subject may contain macros.|
|^|^|*Message*|<|<|The custom message. The message may contain macros.|

[comment]: # ({/new-f2127d8b})

[comment]: # ({new-7dbd8d4d})

#### Operation type: [send message](/manual/config/notifications/action/operation/message)

|Parameter|Description|
|--|--------|
|*Send to user groups*|Click on *Add* to select user groups to send the recovery message to.<br>The user group must have at least "read" [permissions](/manual/config/users_and_usergroups/permissions) to the host in order to be notified.|
|*Send to users*|Click on *Add* to select users to send the recovery message to.<br>The user must have at least "read" [permissions](/manual/config/users_and_usergroups/permissions) to the host in order to be notified.|
|*Send only to*|Send default recovery message to all defined media types or a selected one only.|
|*Custom message*|If selected, a custom message can be defined.|
|*Subject*|Subject of the custom message. The subject may contain macros.|
|*Message*|The custom message. The message may contain macros.|

[comment]: # ({/new-7dbd8d4d})

[comment]: # ({new-37fc532b})

#### Operation type: [remote command](/manual/config/notifications/action/operation/remote_command)

|Parameter|Description|
|--|--------|
|*Target list*|Select targets to execute the command on:<br>**Current host** - command is executed on the host of the trigger that caused the problem event. This option will not work if there are multiple hosts in the trigger.<br>**Host** - select host(s) to execute the command on.<br>**Host group** - select host group(s) to execute the command on. Specifying a parent host group implicitly selects all nested host groups. Thus the remote command will also be executed on hosts from nested groups.<br>A command on a host is executed only once, even if the host matches more than once (e.g. from several host groups; individually and from a host group).<br>The target list is meaningless if the command is executed on Zabbix server. Selecting more targets in this case only results in the command being executed on the server more times.<br>Note that for global scripts, the target selection also depends on the *Host group* setting in global script [configuration](/manual/web_interface/frontend_sections/administration/scripts#configuring_a_global_script).|

[comment]: # ({/new-37fc532b})

[comment]: # ({new-945f9a49})

#### Operation type: notify all involved

|Parameter|Description|
|--|--------|
|*Custom message*|If selected, a custom message can be defined.|
|*Subject*|Subject of the custom message. The subject may contain macros.|
|*Message*|The custom message. The message may contain macros.|


[comment]: # ({/new-945f9a49})
