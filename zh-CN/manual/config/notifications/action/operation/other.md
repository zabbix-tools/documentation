[comment]: # translation:outdated

[comment]: # ({new-ec342157})
# 3 附加操作

[comment]: # ({/new-ec342157})

[comment]: # ({a6e53c19-748f7486})
#### 概述

在本节你将看到对发现或自动注册事件的[附加操作](/manual/config/notifications/action/operation)。

[comment]: # ({/a6e53c19-748f7486})

[comment]: # ({320bf9d1-903c037b})
##### 添加主机

主机在发现过程中中添加，一旦发现立即添加，而非在发现过程结束后添加。

::: 提示
网络发现会因为太多不可达的主机或服务而用时很长，请耐心等待或使用合理IP地址范围。
:::

添加主机时，其名称由标准的** gethostbyname
**函数决定。如果可以解析主机，则使用解析名称。如果没有，则使用IP地址。此外，如果必须使用IPv6地址作为主机名，则所有“：”（冒号）将被替换为“\_”（下划线），因为主机名中不允许冒号。

::: 重要提示
如果通过代理执行发现，当前主机名查找仍然发生在Zabbix服务器上。
:::

::: 重要提示
如果一个新发现的主机与Zabbix的配置中已有的主机同名，1.8之前的版本将添加一个同名的主机。Zabbix
1.8.1及更高版本将** \_ N **添加到主机名中，** N**是从2开始递增的正整数。
:::

[comment]: # ({/320bf9d1-903c037b})
