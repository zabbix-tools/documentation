[comment]: # translation:outdated

[comment]: # ({new-0cdc5e2e})
# 2 远程命令

[comment]: # ({/new-0cdc5e2e})

[comment]: # ({e541a619-36945095})
#### 概述

使用远程命令，您可以预定义在某些情况下会在受监控主机上自动执行的命令。

因此远程命令是智能主动监控的强大机制。

在该功能最显而易见的用途中，你可以尝试：

-   自动重启某些没有响应的应用（web服务器、中间件、CRM）

-   使用IPMI 'reboot'命令重启某些确实无法响应的服务器。

-   磁盘空间不足时自动释放（删除旧文件、清理）
    
-   根据CPU负载将VM从一台物理机迁移到另一台
    
-   CPU（磁盘、内存或其他）资源不足时向云环境添加新节点

为远程命令配置操作与发送消息类似，唯一的区别是 Zabbix 将执行一个命令而不是发送消息。

远程命令可以由Zabbix服务器、代理或agent执行。Zabbix agent伤的远程命令可以由Zabbix服务器或通过Zabbix代理执行。Zabbix agent和Zabbix代理上的远程命令默认都是禁用的。 它们可以通过以下方式启用：

-   在agent的配置中添加一个`AllowKey=system.run[*]` 参数；

-   在代理的配置中将`EnableRemoteCommands`参数设为‘1’。 

由 Zabbix服务器执行的远程命令按照[命令执行](/manual/appendix/command_execution)所述运行，包括退出和代码检查。

即使目标主机在维护状态，远程命令也会执行。

[comment]: # ({/e541a619-36945095})

[comment]: # ({ecd79e3e-7ca4c9c3})
##### 远程命令限制

解析所有宏之后远程命令的限制取决于数据库和字符集（存储非 ASCII 字符需要一字节以上）：

|   |   |   |
|---|---|---|
|*数据库*|*字符限制*|*字节限制*|
|**MySQL**|65535|65535|
|**Oracle Database**|2048|4000|
|**PostgreSQL**|65535|not limited|
|**SQLite (only Zabbix proxy)**|65535|not limited|

以下教程提供了设置远程命令的分步指导。

[comment]: # ({/ecd79e3e-7ca4c9c3})

[comment]: # ({139f6575-349859a6})
#### 配置

在 Zabbix agent上执行远程命令（自定义脚本）必须先在agent[配置](/manual/appendix/config/zabbix_agentd)中启用。

必须确保添加了`AllowKey=system.run[*]`参数，并重启 agent后台驻留程序。

::: 重要提示
Zabbix主动agent上远程命令不不生效。
:::

然后，在*Configuration → Actions*配置新动作时：

-   定义适当的条件。在本例中，设置动作由Apache的任何灾难问题激活：

![](../../../../../../assets/en/manual/config/notifications/action/conditions_restart.png)

-   在 *[Operations](/manual/config/notifications/action/operation#configuring_an_operation)*选项卡，在Operations/Recovery/Update的操作块里点击
-   从Operation下拉菜单选择一个预定义的脚本 

![](../../../../../../assets/en/manual/config/notifications/action/operation_restart_webserver.png)

-   为脚本选择目标列表

[comment]: # ({/139f6575-349859a6})

[comment]: # ({8cf4b6a9-c5296379})
#### 预定义脚本

动作可用的所有脚本（webhook,、脚本、SSH、Telnet、IPMI） 定义在 [全局脚本](/manual/web_interface/frontend_sections/administration/scripts)。

例如：

    sudo /etc/init.d/apache restart 

此例中，Zabbix会尝试重启一个Apache进程。确保这个命令在Zabbix agent上执行（点击*Zabbix agent*按钮为*Execute on*）。

::: 重要提示
注意**sudo**的用法——Zabbix用户默认没有权限重启系统服务 。请查看以下关于配置**sudo**的提示。
:::

::: 经典提示
Zabbix agent应该运行在远程主机上并接受入方向的连接。Zabbix agent在后台执行命令。
:::

Zabbix agent上的远程命令由system.run\[,nowait\]键执行时没有timeout，并且不检查执行的结果。在Zabbix服务器和Zabbix代理上，远程命令执行时有timeout，在zabbix\_server.conf或zabbix\_proxy.conf文件里配置TrapperTimeout参数，并且[检查](/manual/appendix/command_execution#exit_code_checking) 执行的结果。

[comment]: # ({/8cf4b6a9-c5296379})

[comment]: # ({227a52a6-a1b1e7ea})
#### 访问权限

确保‘zabbix’用户拥有执行配置的命令的权限。用户可能有兴趣使用**sudo**给特权命令访问权限。要配置访问权限，用root执行以下命令：

    # visudo

可以在*sudoers*文件使用示例的几行：

    # allows 'zabbix' user to run all commands without password.
    zabbix ALL=NOPASSWD: ALL

    # allows 'zabbix' user to restart apache without password.
    zabbix ALL=NOPASSWD: /etc/init.d/apache restart

::: 提示
在有些操作系统中，*sudoers*文件禁止非本地用户执行命令，可以在*/etc/sudoers*文件中取消注释**requiretty**选项。
:::

[comment]: # ({/227a52a6-a1b1e7ea})

[comment]: # ({b28a0a43-1f3becef})
#### 有多个接口的远程命令

如果目标系统有多个所选类型（Zabbix agent或IPMI）的接口，远程命令会在默认的接口执行。

Zabbix agent接口以外，可以在其它接口通过SSH或Telnet执行远程命令。可用的接口按如下顺序选择：

      * Zabbix agent default interface
      * SNMP default interface
      * JMX default interface
      * IPMI default interface

[comment]: # ({/b28a0a43-1f3becef})

[comment]: # ({bf4a33b9-26b199db})
#### IPMI远程命令

对于IPMI远程命令应使用如下语法：

    <command> [<value>]

其中

-   \<command\>——表示一个没有空格的IPMI命令。
-   \<value\>——其值为‘on’、‘off’或任何无符号整数，\<value\>是可选参数。

[comment]: # ({/bf4a33b9-26b199db})

[comment]: # ({6d229a98-c230845b})
#### 示例

可能在动作的操作中使用的[全局脚本](/manual/web_interface/frontend_sections/administration/scripts#configuring_a_global_script)的示例。

[comment]: # ({/6d229a98-c230845b})

[comment]: # ({9acdbe11-1d68d18d})
##### 示例 1

在特定条件下重启Windows。

为了根据Zabbix检测到的问题自动重启Windows，定义如下脚本：

|Script parameter|Value|
|----------------|-----|
|*Scope*|'Action operation'|
|*Type*|'Script'|
|*Command*|c:\\windows\\system32\\shutdown.exe -r -f|

[comment]: # ({/9acdbe11-1d68d18d})

[comment]: # ({cfb505ce-8e3d9a17})
##### 示例 2

用 IPMI控制重启主机。

|Script parameter|Value|
|----------------|-----|
|*Scope*|'Action operation'|
|*Type*|'IPMI'|
|*Command*|reset|

[comment]: # ({/cfb505ce-8e3d9a17})

[comment]: # ({5fc58e05-c9d4e7c6})
##### 示例 3

使用 IPMI控制关闭主机电源。

|Script parameter|Value|
|----------------|-----|
|*Scope*|'Action operation'|
|*Type*|'IPMI'|
|*Command*|power off|

[comment]: # ({/5fc58e05-c9d4e7c6})
