[comment]: # translation:outdated

[comment]: # ({163256cc-b1624dc4})
# 4 在消息中使用宏

[comment]: # ({/163256cc-b1624dc4})

[comment]: # ({bf6d74a0-72998395})
#### 概述
在消息主题和消息文本中，可以使用宏获取更高效的问题报告。
Zabbix支持的可用的 [完整宏列表](/manual/appendix/macros/supported_by_location)。

[comment]: # ({/bf6d74a0-72998395})

[comment]: # ({0863609a-e5ad63df})
#### 示例
此处的示例说明了如何在消息中使用宏。

[comment]: # ({/0863609a-e5ad63df})

[comment]: # ({1571ac70-102863de})
##### 示例 1

消息主题：

    Problem: {TRIGGER.NAME}

当收到消息时，消息主题将被替换为类似如下内容：

    Problem: Processor load is too high on Zabbix server

[comment]: # ({/1571ac70-102863de})

[comment]: # ({23ef9f1c-1344257c})
##### 示例 2

消息：

    Processor load is: last(/zabbix.zabbix.com/system.cpu.load[,avg1])

当收到消息时，将被替换为类似如下内容：

    Processor load is: 1.45

[comment]: # ({/23ef9f1c-1344257c})

[comment]: # ({064f024a-3713b8a5})
##### 示例 3

消息：

    Latest value: last(/{HOST.HOST}/{ITEM.KEY})
    MAX for 15 minutes: max(/{HOST.HOST}/{ITEM.KEY},15m)
    MIN for 15 minutes: min(/{HOST.HOST}/{ITEM.KEY},15m)

当收到消息时，将被替换为类似如下内容：

    Latest value: 1.45
    MAX for 15 minutes: 2.33
    MIN for 15 minutes: 1.01

[comment]: # ({/064f024a-3713b8a5})

[comment]: # ({27cba7ac-88afe91c})
##### 示例 4

消息：

    http://<server_ip_or_name>/zabbix/tr_events.php?triggerid={TRIGGER.ID}&eventid={EVENT.ID}

当收到消息时，将会包含 *Event
details*页面的链接，该页面提供该事件、它的触发器和近期由相同触发器产生的事件列表。 

[comment]: # ({/27cba7ac-88afe91c})

[comment]: # ({b311bb70-d41797c6})
##### 示例 5

在触发器表达式中通知来自多个主机的值。

消息：

    Problem name: {TRIGGER.NAME}
    Trigger expression: {TRIGGER.EXPRESSION}
     
    1. Item value on {HOST.NAME1}: {ITEM.VALUE1} ({ITEM.NAME1})
    2. Item value on {HOST.NAME2}: {ITEM.VALUE2} ({ITEM.NAME2})

当收到消息时，将被替换为类似如下内容：

    Problem name: Processor load is too high on a local host
    Trigger expression: last(/Myhost/system.cpu.load[percpu,avg1])>5 or last(/Myotherhost/system.cpu.load[percpu,avg1])>5

    1. Item value on Myhost: 0.83 (Processor load (1 min average per core))
    2. Item value on Myotherhost: 5.125 (Processor load (1 min average per core))

[comment]: # ({/b311bb70-d41797c6})

[comment]: # ({c1ff6e06-0ee2664c})
##### 示例 6

在同一个[恢复](/manual/config/notifications/action/recovery_operations)消息中接收故障事件和恢复事件的详情：

消息：

    Problem:

    Event ID: {EVENT.ID}
    Event value: {EVENT.VALUE} 
    Event status: {EVENT.STATUS} 
    Event time: {EVENT.TIME}
    Event date: {EVENT.DATE}
    Event age: {EVENT.AGE}
    Event acknowledgment: {EVENT.ACK.STATUS} 
    Event update history: {EVENT.UPDATE.HISTORY}

    Recovery: 

    Event ID: {EVENT.RECOVERY.ID}
    Event value: {EVENT.RECOVERY.VALUE} 
    Event status: {EVENT.RECOVERY.STATUS} 
    Event time: {EVENT.RECOVERY.TIME}
    Event date: {EVENT.RECOVERY.DATE}
    Operational data: {EVENT.OPDATA}

当收到消息时，宏将被替换为类似如下内容： 

    Problem:

    Event ID: 21874
    Event value: 1 
    Event status: PROBLEM 
    Event time: 13:04:30
    Event date: 2018.01.02
    Event age: 5m
    Event acknowledgment: Yes 
    Event update history: 2018.01.02 13:05:51 "John Smith (Admin)"
    Actions: acknowledged.

    Recovery: 

    Event ID: 21896
    Event value: 0 
    Event status: OK 
    Event time: 13:10:07
    Event date: 2018.01.02
    Operational data: Current value is 0.83

::: 重要提示
自 Zabbix 2.2.0开始支持原始故障事件和恢复事件单独的通知宏。
:::

[comment]: # ({/c1ff6e06-0ee2664c})
