[comment]: # translation:outdated

[comment]: # ({f15550d6-66eca2c9})
# 1 发送消息

[comment]: # ({/f15550d6-66eca2c9})

[comment]: # ({83ac7442-a1d73332})
#### 概述

 Zabbix 提供发送消息作为主要操作之一的原因是，发送消息是通知人们有关某问题的最佳方式。

[comment]: # ({/83ac7442-a1d73332})

[comment]: # ({ddf661f5-2540ef35})
#### 配置

为了能够从Zabbix发送和接收消息你必须：

-   定义发送消息的[媒体](/manual/config/notifications/media) 

::: 警告
如果要接收非触发器事件如发现、主动代理自动注册或内部事件，**必须** 在用户媒体[配置](/manual/config/notifications/media/email#user_media) 中勾选默认触发器严重性（‘Not classified’）。
:::

-   配置发送消息到已经定义的媒体的[动作](/manual/config/notifications/action/operation) 

::: 重要
Zabbix只向对产生事件的主机上至少一个触发器表达式有‘read’权限的用户发送消息。 
:::

你可以配置自定义方案使用[升级](/manual/config/notifications/action/escalations)发送消息。

Zabbix发送的数据是UTF-8格式的（主题只包含ASCII字符不支持UTF-8编码），消息的主题和内容是遵循‘SMTP/MIME e-mail’格式采用base64编码的。为了成功从Zabbix接收和读取邮件，邮件服务器和客户端必须支持标准‘SMTP/MIME e-mail’格式。

宏扩展后的消息限制与[远程命令](/manual/config/notifications/action/operation/remote_command)消息限制相同。

[comment]: # ({/ddf661f5-2540ef35})

[comment]: # ({38747fbf-c0442483})
#### 跟踪消息

可以在*Monitoring → Problems*查看已发送消息的状态。

在*Actions*列可以查看已采取动作的汇总信息。绿色数字代表已发送消息，红色代表失败，*In progress*表示动作已发起，*Failed*表示动作没有执行成功。

如果点击时间时间查看事件详情，根据事件，*Message actions*包含（或不包含）已发送消息的详细信息。

在*Reports → Action log*可以看到所有配置了动作的事件的已发生动作的详细信息。

[comment]: # ({/38747fbf-c0442483})
