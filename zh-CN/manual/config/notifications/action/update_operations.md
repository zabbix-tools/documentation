[comment]: # translation:outdated

[comment]: # ({new-6eb0119d})
# 4 Update operations

[comment]: # ({/new-6eb0119d})

[comment]: # ({new-68ae5c34})
#### Overview

Update operations are available in actions with the following event
sources:

-   *Triggers* - when problems are
    [updated](/manual/acknowledges#updating_problems) by other users,
    i.e. commented upon, acknowledged, severity has been changed, closed
    (manually);
-   *Services* - when the severity of a service has changed but the
    service is still not recovered.

Both messages and remote commands are supported in update operations.
While several operations can be added, escalation is not supported - all
operations are assigned to a single step and therefore will be performed
simultaneously.

[comment]: # ({/new-68ae5c34})

[comment]: # ({new-1379ee27})
#### Configuring an update operation

To configure an update operation go to the *Operations* tab in action
[configuration](/manual/config/notifications/action).

![](../../../../../assets/en/manual/config/update_operation.png)

To configure details of a new update operation, click on
![](../../../../../assets/en/manual/config/add_link.png) in the Update
operations block. To edit an existing operation, click on
![](../../../../../assets/en/manual/config/edit_link.png) next to the
operation. A popup window will open where you can edit the operation
step details.

[comment]: # ({/new-1379ee27})

[comment]: # ({new-175ae116})

Update operations offer the same set of parameters as [Recovery operations](/manual/config/notifications/action/recovery_operations#Recovery_operation_details).

![](../../../../../assets/en/manual/config/update_operation_details.png)

[comment]: # ({/new-175ae116})

