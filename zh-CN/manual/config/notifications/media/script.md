[comment]: # translation:outdated

[comment]: # ({7cf0314f-e25ccc09})
# 3 自定义告警脚本

[comment]: # ({/7cf0314f-e25ccc09})

[comment]: # ({ba53f823-bc410af8})
#### 概述

如果当前的告警媒介类型无法满足您的要求，您可以创建一个自定义的脚本对告警通知进行处理。

告警脚本在Zabbix服务器上执行。这些脚本放置于服务器配置文件 [configuration
file](/manual/appendix/config/zabbix_server)中定义的 **AlertScriptsPath**目录下。



以下是一个自定义告警脚本的示例:

``` {.bash}
#!/bin/bash

to=$1
subject=$2
body=$3

cat <<EOF | mail -s "$subject" "$to"
$body
EOF
```

::: noteimportant
从3.4版本开始，Zabbix会检查执行的命令和脚本的退出代码。任何不为**0** 的退出代码都将被视为[command
execution](/manual/appendix/command_execution) 错误. 在这种情况下，Zabbix会尝试重复执行。
:::
环境变量不会为脚本保留或创建，因此它们应该被明确处理。

[comment]: # ({/ba53f823-bc410af8})

[comment]: # ({a83175db-47055351})
#### 配置

配置自定义告警脚本为媒介类型：

-   进入 *管理 → 媒介类型*
-   点击 *创建媒介类型*

**媒介类型** 页包含一些通用的媒介类型属性如下：

![](../../../../../assets/en/manual/config/notifications/media/media_script.png)

标红星的为必填字段。

如下参数只适用于脚本媒介类型

|参数|说明|
|---------|-----------|
|*Script name*|输入脚本的名称|
|*Script parameters*|添加脚本的命令行参数<br>{ALERT.SENDTO}, {ALERT.SUBJECT}和 {ALERT.MESSAGE} [宏](/manual/appendix/macros/supported_by_location) 在脚本参数中是支持的<br>从3.0版本开始，Zabbix开始支持自定义脚本参数.|

关于如何配置默认消息及告警处理的详细内容，请参见 [通用媒介类型参数](/manual/config/notifications/media#common_parameters) 

::: notewarning
 Even if an alertscript doesn't use default
messages, message templates for operation types used by this media type
must still be defined, otherwise a notification will not be sent.
即使告警脚本没有使用默认消息，此媒体类型使用的操作类型的消息模板仍必须定义，否则通知将无法发送。
:::

::: noteimportant
从Zabbix 3.4.0版本开始实现了多个告警媒介并行处理, 所以需要注意的是，当配置了多个告警脚本时，这些脚本是可以被告警进程并行处理的. 告警进程的进程数可以通过Zabbix Server的配置项StartAlerters[参数](/manual/appendix/config/zabbix_server)进行限制。
:::

[comment]: # ({/a83175db-47055351})

[comment]: # ({new-6a270eac})

#### Media type testing

To test a configured script media type:

-   Locate the relevant script in the [list](/manual/config/notifications/media#overview) of media types.
-   Click on *Test* in the last column of the list (a testing window
    will open).
-   Edit the script parameter values, if needed (editing affects the test procedure only, the actual values will not be changed).
-   Click on *Test*.

![](../../../../../assets/en/manual/config/notifications/media/script_test.png){width="600"}


[comment]: # ({/new-6a270eac})

[comment]: # ({72d35122-525cb972})
#### 用户媒介

配置完媒介类型后，需进入*管理 → 用户*为用户配置相应的媒介。配置用户媒介的步骤和配置其它媒介类型的方式类似，可以参见 [媒介类型](/manual/config/notifications/media#user_media) 页.

注意，在定义用户媒介的时候，*Send to* 字段不能为空。如果该字段在告警脚本中不会被使用，可以输入任意支持的字段以跳过该校验。

[comment]: # ({/72d35122-525cb972})
