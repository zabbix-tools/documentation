[comment]: # translation:outdated

[comment]: # ({new-56bf7160})
# 4 Webhook

[comment]: # ({/new-56bf7160})

[comment]: # ({10e3c5dd-6584b266})
#### 概述

webhook 媒体类型对于使用自定义的JavaScript代码进行HTTP调用非常有用，它可以直接与外部软件（如 Helpdesk 系统、聊天工具或信使）进行集成。您可以选择使用 Zabbix 提供的集成方式或创建一个自定义集成方式。

[comment]: # ({/10e3c5dd-6584b266})

[comment]: # ({eaf18bcf-27a06b1f})
#### 集成

以下集成方式允许使用预定义的webhook媒体类型来推送Zabbix通知:

-   [brevis.one](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/brevis.one/README.md)
-   [Discord](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/discord/README.md)
-   [Express.ms
    messenger](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/express.ms/README.md)
-   [Github
    issues](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/github/README.md)
-   [iLert](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/ilert/README.md)
-   [iTop](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/itop/README.md)
-   [Jira](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/jira/README.md)
-   [Jira Service
    Desk](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/jira_servicedesk/README.md)
-   [ManageEngine
    ServiceDesk](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/manageengine_servicedesk/README.md)
-   [Mattermost](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/mattermost/README.md)
-   [Microsoft
    Teams](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/msteams/README.md)
-   [Opsgenie](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/opsgenie/README.md)
-   [OTRS](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/otrs/README.md)
-   [Pagerduty](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/pagerduty/README.md)
-   [Pushover](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/pushover/README.md)
-   [Redmine](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/redmine/README.md)
-   [Rocket.Chat](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/rocketchat/README.md)
-   [ServiceNow](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/servicenow/README.md)
-   [SIGNL4](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/signl4/README.md)
-   [Slack](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/slack/README.md)
-   [SolarWinds](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/solarwinds/README.md)
-   [SysAid](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/sysaid/README.md)
-   [Telegram](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/telegram/README.md)
-   [TOPdesk](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/topdesk/README.md)
-   [VictorOps](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/victorops/README.md)
-   [Zammad](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/zammad/README.md)
-   [Zendesk](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/zendesk/README.md)

::: notetip
除了这里列出的服务外，Zabbix还可以集成 **Spiceworks** (无需webhook)。要把Zabbix通知转换成Spiceworks单据，需先创建一个[电子邮件媒介类型](/manual/config/notifications/media/email) ，并在指定的Zabbix用户配置设置中输入Spiceworks helpdesk的邮件地址(例如 help\@zabbix.on.spiceworks.com)。
:::

[comment]: # ({/eaf18bcf-27a06b1f})

[comment]: # ({18a5b1e9-d84f1038})
#### 配置

开始使用webhook集成:

1.  在已下载的 Zabbix 的 `templates/media` 录中，找到所需的.xml文件；或者从Zabbix的 [git
    仓库](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse)中下载
2.  将文件[导入](/manual/xml_export_import/media#importing) 到 Zabbix 安装中，Webhook 将出现在媒体类型列表中。
3.  根据 *Readme.md* 文件中的说明来配置 webhook （也可以点击 webhook's 名称来快速访问 *Readme.md* ） 。

从零开始创建一个自定义的webhook:

-   进入*管理 → 媒介类型*
-   点击*创建媒介类型*

**媒介类型**选项卡包含了针对这种媒体类型的各种属性:

![](../../../../../assets/en/manual/config/notifications/media/media_webhook_pushover.png)

红色星号标记的为必填字段。

webhook媒体类型的具体参数如下:

|参数|说明|
|---------|-----------|
|*Parameters*|webhook变量作为属性和值对。<br>对于预先配置的webhook，参数列表会随着服务的不同而变化。检查webhook的*Readme.md* 参数说明文件。<br>对于新的webhooks，默认情况下包含了几个常见的变量 (URL:<empty>, HTTPProxy:<empty>, To:{ALERT.SENDTO}, Subject:{ALERT.SUBJECT}, <Message:%7BALERT.MESSAGE>}), 你可以保留或删除它们。<br>参数中支持问题通知中支持的所有[宏](/manual/appendix/macros/supported_by_location) <br>如果指定了HTTP Proxy，该字段支持与监控项配置[HTTP proxy](/manual/config/items/itemtypes/http#configuration)字段相同的功能。 Proxy字符串可以加上前缀 `[scheme]://` 来指定使用哪种代理例如https, socks4, socks5; 请参见[documentation](https://curl.haxx.se/libcurl/c/CURLOPT_PROXY.html)).|
|*Script*|在点击参数字段(或旁边的查看/编辑按钮)时出现的块中输入JavaScript代码。这段代码将执行webhook操作。<br>脚本是接受参数-值对的一段功能代码，它的值应该使用JSON.parse() 方法转换为JSON对象，例如：`var params = JSON.parse(value);`.<br><br>代码可以访问所有参数，它可以执行HTTP GET、POST、PUT和DELETE请求，并可控制HTTP头和请求正文。<br>脚本必须包含一个返回值，否则它将无效。它可以返回OK状态以及一个可选的标签列表，标签值(参见*Process tags*选项)，或一个错误的字符串。<br><br>注意 脚本只有在创建警报之后才会执行。如果脚本被配置为返回和处理标签 (参见*Process tags* 选项),这些标签将不会在初始问题消息和恢复消息中的 {EVENT.TAGS} 和 {EVENT.RECOVERY.TAGS} 宏中解析，因为脚本还没有时间运行。<br><br>另请参阅： [Webhook development guidelines](https://www.zabbix.com/documentation/guidelines/webhooks), [Webhook script examples](/manual/config/notifications/media/webhook/webhook_examples), [Additional JavaScript objects](/manual/config/items/preprocessing/javascript/javascript_objects).<br>|
|*Timeout*|vaScript执行超时 (1-60s, 默认为30s)。<br>支持时间后缀, 例如 30s, 1m。|
|*Process tags*|选中复选框标以将返回的JSON属性值作为标签处理。这些标签将被添加到Zabbix中已经存在的(如果有的话)问题事件标记中。<br>如果webhook使用了标签(Process tags复选框被选中),webhook应该返回一个至少带有一个空标签的JSON对象`var result = {tags: {}};`.<br>返回的标签示例: *Jira ID: PROD-1234*, *Responsible: John Smith*, *Processed:<no value>*, 等.|
|*Include event menu entry*|选中复选框以在[event menu](/manual/web_interface/frontend_sections/monitoring/problems#event_menu) 中包含一个链接到创建的外部目标的条目。<br>若选中此项，webhook就不应该被用来向不同的用户发送通知 (考虑创建一个[专有用户](/manual/config/notifications/media/webhook#user_media)) 或者在几个告警动作中[关联单个问题事件](/manual/config/notifications/media/webhook#configuring_alert_actions).|
|*Menu entry name*|指定菜单入口名称。<br>支持{EVENT.TAGS.<tag name>}宏<br>只有当*Include event menu entry* 被选中时，该字段才为必填项。
|*Menu entry URL*|指定菜单入口的URL。<br>支持{EVENT.TAGS.<tag name>} 宏<br>只有当*Include event menu entry* 被选中时，该字段才为必填项。

关于如何配置默认消息和警报处理选项的详细信息，请参见 [通用媒介类型参数](/manual/config/notifications/media#common_parameters)

::: notewarning
 即使webhook不使用默认消息，webhook使用的操作类型的消息模板也必须定义。
:::

[comment]: # ({/18a5b1e9-d84f1038})

[comment]: # ({new-e5e73ea8})

#### Media type testing

To test a configured webhook media type:

-   Locate the relevant webhook in the [list](/manual/config/notifications/media#overview) of media types.
-   Click on *Test* in the last column of the list (a testing window
    will open).
-   Edit the webhook parameter values, if needed.
-   Click on *Test*.

By default, webhook tests are performed with parameters entered during
configuration. However, it is possible to change attribute values for
testing. Replacing or deleting values in the testing window affects the
test procedure only, the actual webhook attribute values will remain
unchanged.

![](../../../../../assets/en/manual/config/webhook_test1.png){width="600"}

To view media type test log entries without leaving the test window, click on *Open log* (a new popup window will open).

![](../../../../../assets/en/manual/config/mediatype_test2.png){width="600"}

[comment]: # ({/new-e5e73ea8})

[comment]: # ({new-b2974109})
**If the webhook test is successful:**

-   *"Media type test successful."* message is displayed
-   Server response appears in the gray *Response* field
-   Response type (JSON or String) is specified below the *Response*
    field

[comment]: # ({/new-b2974109})

[comment]: # ({new-abfbc807})
**If the webhook test fails:**

-   *"Media type test failed."* message is displayed, followed by
    additional failure details.

[comment]: # ({/new-abfbc807})

[comment]: # ({4db5c371-6eeec89a})
#### 用户媒介

媒介类型配置完成后， 前往 *管理 → 用户* 部分，为一个现有用户或创建一个新用户并指定其webhook媒介。关于如何为用户指定用户媒介的方式和指定其它媒介类型类似，具体请参见 [媒介类型](/manual/config/notifications/media#user_media)。

如果webhook使用标签来存储 ticket\\message ID, 避免将同一个 webhook 作为媒体分配给不同的用户，因为这样做可能会导致webhook错误 (适用于大多数使用了 _Include event menu entry_ 选项)。 在这种情况下，最好的做法是创建一个专用的用户来表示webhook:

1.  配置好webhook媒体类型后， 前往 *管理 → 用户* 部分，并创建一个专用的Zabbix用户来表示webhook - 例如, 为 Slack webhook添加一个别名  _Slack_。 所有的设置可以保持默认值（除了媒体），因为这个用户不会登录到Zabbix。
2.  在用户配置中， 进入*Media* 选项卡并且填写  [add a webhook](/manual/config/notifications/media#user_media)  所需的联系信息。 如果webhook没有使用*Send to*  字段, 可以输入任何支持的字符组合来绕过验证要求。
3.  至少授予该用户对要向其发送警报的所有主机有可读的 [权限](/manual/config/users_and_usergroups/permissions#permissions_to_host_groups)。

配置告警动作时，请在*Send to users*字段中添加该用户 - 这将告诉Zabbix使用webhook来获取来自这个动作的通知。

[comment]: # ({/4db5c371-6eeec89a})

[comment]: # ({7a4bb88d-ac89791d})
#### 配置告警动作

告警动作决定了哪些通知应该通过webhook发送。webhook的[configuring actions](/manual/config/notifications/action) 步骤与所有其他媒体类型相同，但有以下例外:

-   如果webhook使用标签来存储 ticket\\message ID 以及后续的 update\\resolve operations 操作, 这个webhook不应该用于单个问题事件的多个告警动作中。这适用于Zabbix提供的Jira, Jira Service Desk, Mattermost, Opsgenie, OTRS, Redmine, ServiceNow, Slack, Zammad和Zendesk webhooks以及大多数使用 *Include event menu entry* 选项的webhook。 如果操作或升级步骤属于同一个动作，允许在多个操作中使用webhook。在不同的动作中使用webhook也是可以的，由于不同的筛选器条件，动作不会应用到相同的问题事件中。
-   当在动作中使用webhook用于  [internal events](/manual/config/events/sources#internal_events): 在动作操作配置中，选中 *Custom message*复选框，输入自定义消息内容 ，否则通知不会被发送出去。

[comment]: # ({/7a4bb88d-ac89791d})
