[comment]: # translation:outdated

[comment]: # ({a90cdef7-82d32628})
# Webhook 脚本范例

[comment]: # ({/a90cdef7-82d32628})

[comment]: # ({6b15aaec-ba5f1fb7})
#### 概述

尽管Zabbix提供了大量现成的webhook集成，但您可能想要创建自己的webhook。本节提供了自定义webhook脚本的示例. (在 *Script* 参数中使用). 有关其他webhook参数的说明，请参见[webhook](/manual/config/notifications/media/webhook)

[comment]: # ({/6b15aaec-ba5f1fb7})

[comment]: # ({c1a73746-383d6d88})

#### Jira webhook (自定义)

![](../../../../../../assets/en/manual/config/notifications/media/media_webhook_jira.png)

此脚本将创建一个JIRA问题，并返回关于所创建问题的一些信息。

``` {.java}
try {
    Zabbix.log(4, '[ Jira webhook ] Started with params: ' + value);

    var result = {
            'tags': {
                'endpoint': 'jira'
            }
        },
        params = JSON.parse(value),
        req = new HttpRequest(),
        fields = {},
        resp;

    if (params.HTTPProxy) {
        req.setProxy(params.HTTPProxy);
    }

    req.addHeader('Content-Type: application/json');
    req.addHeader('Authorization: Basic ' + params.authentication);

    fields.summary = params.summary;
    fields.description = params.description;
    fields.project = {key: params.project_key};
    fields.issuetype = {id: params.issue_id};

    resp = req.post('https://tsupport.zabbix.lan/rest/api/2/issue/',
        JSON.stringify({"fields": fields})
    );

    if (req.getStatus() != 201) {
        throw 'Response code: ' + req.getStatus();
    }

    resp = JSON.parse(resp);
    result.tags.issue_id = resp.id;
    result.tags.issue_key = resp.key;

    return JSON.stringify(result);
}
catch (error) {
    Zabbix.log(4, '[ Jira webhook ] Issue creation failed json : ' + JSON.stringify({"fields": fields}));
    Zabbix.log(3, '[ Jira webhook ] issue creation failed : ' + error);

    throw 'Failed with error: ' + error;
}
```

[comment]: # ({/c1a73746-383d6d88})

[comment]: # ({38275ca5-5d1f46da})

#### Slack webhook (自定义)

此webhook将把Zabbix的通知转发到Slack channel中。

![](/manual/config/webhook_slack1.png)

``` {.java}
try {
    var params = JSON.parse(value),
        req = new HttpRequest(),
        response;

    if (params.HTTPProxy) {
        req.setProxy(params.HTTPProxy);
    }

    req.addHeader('Content-Type: application/x-www-form-urlencoded');

    Zabbix.log(4, '[ Slack webhook ] Webhook request with value=' + value);

    response = req.post(params.hook_url, 'payload=' + encodeURIComponent(value));
    Zabbix.log(4, '[ Slack webhook ] Responded with code: ' + req.Status() + '. Response: ' + response);

    try {
        response = JSON.parse(response);
    }
    catch (error) {
        if (req.getStatus() < 200 || req.getStatus() >= 300) {
            throw 'Request failed with status code ' + req.getStatus();
        }
        else {
            throw 'Request success, but response parsing failed.';
        }
    }

    if (req.getStatus() !== 200 || !response.ok || response.ok === 'false') {
        throw response.error;
    }

    return 'OK';
}
catch (error) {
    Zabbix.log(3, '[ Jira webhook ] Sending failed. Error: ' + error);

    throw 'Failed with error: ' + error;
}
```

[comment]: # ({/38275ca5-5d1f46da})
