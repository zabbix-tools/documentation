[comment]: # translation:outdated

[comment]: # ({15e4b796-d40475f4})
# 1 电子邮箱

[comment]: # ({/15e4b796-d40475f4})

[comment]: # ({6c0764a7-9d59e93d})
#### 概述

若要使用电子邮件作为消息发送的通道，那么您需要选择电子邮件作为媒介类型，同时为接收消息的用户指定具体的邮箱。

::: 请注意
 同一事件的不同通知，会总结在同一封邮件内统一发送出去。
:::



[comment]: # ({/6c0764a7-9d59e93d})

[comment]: # ({18f94e1d-3711e99d})
#### 配置

选择电子邮件作为媒介类型：

-   通过导航栏 *管理 → 媒介类型*
-   点击 *创建新的媒介类型* 按钮（或者点击预先定义的媒介类型列表中的 *电子邮件* ）。

在 **媒介类型** 选项卡中，包含了媒介类型的常规属性：

![](../../../../../assets/en/manual/config/notifications/media/media_email.png)

所有标记有红色星号的均为必填配置。

以下为设定电子邮箱作为媒介类型的具体参数：

| 参数              | 说明                                                         |
| ----------------- | ------------------------------------------------------------ |
| *SMTP 服务器*     | 设置一台SMTP服务器信息的发送。                               |
| *SMTP 服务器端口* | 设置SMTP服务器发送信息的端口。<br>*自 Zabbix 3.0 版本起* 支持该参数配置。 |
| *SMTP helo*       | 对SMTP设置正确的helo数值，通常设定为域名。                   |
| *SMTP 电子邮箱*   | 该邮箱地址作为邮件的发送邮箱，在邮件中作为 **发送自** 的信息诠释。<br>自 Zabbix 2.2 版本起，用户可以为实际的邮箱地址添加一个显示名称（比如，在上述截图中 "Zabbix\_info" 就是作为发送邮箱的显示名称，其设置格式为 *Zabbix\_info <zabbix\@company.com>*）。<br>同RFC5322中的内容相比较而言，在 Zabbix 的邮件名称显示上存在着一些限制，举例来说：<br>有效名称范例：<br>*zabbix\@company.com* （只包含邮箱地址，无需使用尖括号）<br>*Zabbix\_info <zabbix\@company.com>* （邮箱具有显示名称并且使用尖括号包含邮箱地址）<br>*∑Ω-monitoring <zabbix\@company.com>* （邮箱名称中有使用 UTF-8 字符）<br>无效名称范例：<br>*Zabbix HQ zabbix\@company.com* （有配置邮箱名称，但是没有使用尖括号囊括邮箱地址）<br>*"Zabbix\\@\\<H(comment)Q\\>" <zabbix\@company.com>* （从使用规则上来说，该格式的描述适用于RFC5322，但是对于 Zabbix 的邮件配置来说，并不支持括号与注释） |
| *连接安全等级*    | 选择连接的安全级别：<br>**None** - 不使用 [CURLOPT\_USE\_SSL](http://curl.haxx.se/libcurl/c/CURLOPT_USE_SSL.html) 选项<br>**STARTTLS** - 使用带有 CURLUSESSL\_ALL 数值的 CURLOPT\_USE\_SSL 选项<br>**SSL/TLS** - 设定 CURLOPT\_USE\_SSL 为选配<br>*自 Zabbix 3.0 版本起* 支持该参数的配置。 |
| *SSL 验证匹配*    | 勾选该复选框以开启对 SMTP 服务器的 SSL 证书验证。<br>完成证书验证功能，需要在 [CURLOPT\_CAPATH](http://curl.haxx.se/libcurl/c/CURLOPT_CAPATH.html) 输入有关 "SSLCALocation" 服务器配置指令的参数。<br>设置 cURL选项，请参考 [CURLOPT\_SSL\_VERIFYPEER](http://curl.haxx.se/libcurl/c/CURLOPT_SSL_VERIFYPEER.html)。<br>*自 Zabbix 3.0 版本起* 支持该参数的配置。 |
| *SSL 主机验证*    | 勾选该复选框以开启对 *通用名称* 信息栏或者field or the *目标备用名称* 信息栏的验证功能，以确保与 SMTP 服务器验证的一致性。<br>设置 cURL选项，请参考 [CURLOPT\_SSL\_VERIFYHOST](http://curl.haxx.se/libcurl/c/CURLOPT_SSL_VERIFYHOST.html)。<br>*自 Zabbix 3.0 版本起* 支持该参数的配置。 |
| *身份验证*        | 选择验证级别：<br>**None** - 不设置 cURL 选项<br>(自 3.4.2 版本起) **Username and password** - 设置该参数意味着，验证机制由 cURL完成而非 "AUTH=\*"<br>(自 3.4.2 版本起) **Normal password** - [CURLOPT\_LOGIN\_OPTIONS](http://curl.haxx.se/libcurl/c/CURLOPT_LOGIN_OPTIONS.html) 参数设定为 "AUTH=PLAIN"<br>*自 Zabbix 3.0 版本起* 支持该参数的配置。 |
| *用户名*          | 配置用于验证的用户名称。<br>该项参数的配置，是设定了 [CURLOPT\_USERNAME](http://curl.haxx.se/libcurl/c/CURLOPT_USERNAME.html) 参数的数值。<br>*自 Zabbix 3.0 版本起* 支持该参数的配置。 |
| *密码*            | 配置验证密码。<br>对 [CURLOPT\_PASSWORD](http://curl.haxx.se/libcurl/c/CURLOPT_PASSWORD.html)的数值进行配置。<br>*自 Zabbix 3.0 版本起* 支持该参数的配置。 |
| *消息格式*        | 对消息格式进行选择：<br>**HTML** - 以 HTML 的格式对消息进行发送<br>**Plain text** - 以纯文本的格式进行发送 |

::: 重要提示
为了确保 SMTP 验证功能的有效性，Zabbix server 应由 cURL 7.20.0 或者更新版本的 --with-libcurl [汇编语言](/manual/installation/install#configure_the_sources) 编译完成。
:::

有关如何配置默认消息以及告警处理的选项，详细信息请参考 [通用媒介类型参数](/manual/config/notifications/media#common_parameters) 。



[comment]: # ({/18f94e1d-3711e99d})

[comment]: # ({new-98f29ec0})

#### Media type testing

To test whether a configured e-mail media type works correctly:

-   Locate the relevant e-mail in the [list](/manual/config/notifications/media#overview) of media types.
-   Click on *Test* in the last column of the list (a testing window
    will open).
-   Enter a *Send to* recipient address, message body and, optionally,
    subject.
-   Click on *Test* to send a test message.

Test success or failure message will be displayed in the same window:

![](../../../../../assets/en/manual/config/notifications/media/test_email0.png){width="600"}

[comment]: # ({/new-98f29ec0})

[comment]: # ({8b3a996a-5e74f274})
#### 用户媒介

当完成电子邮件媒介类型的配置，请前往 *管理 → 用户* 配置栏，对用户属性中的电子邮件媒介进行布置。
用户媒介的设定步骤， 该设定适用于所有媒介类型， 请参考 [媒介类型](/manual/config/notifications/media#user_media) 页面。

[comment]: # ({/8b3a996a-5e74f274})
