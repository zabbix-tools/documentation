[comment]: # translation:outdated

[comment]: # ({new-adbf1c90})
# 2 SMS

[comment]: # ({/new-adbf1c90})

[comment]: # ({1d4d3e98-657f31be})
#### 概述

Zabbix 支持使用连接到 Zabbix server 串行口的串行 GSM 调制解调器发送短信。

请确保满足以下条件：

-   串行设备的速率（Linux下通常为 /dev/ttyS0 ）与GSM调制解调器的速度一致。Zabbix没有设置串行链路的速度。它使用默认设置。
-   'zabbix'用户对串行设备具有读/写访问权限。执行ls -l /dev/ttyS0命令来查看当前串口设备的权限。
-   GSM调制解调器已经输入了PIN码，并且在电源复位后会将其保留。或者，您可以禁用SIM卡上的PIN码。可以通过在终端软件（如Unix minicom或Windows HyperTerminal）中发出命令AT + CPIN =“NNNN” 来输入PIN码（NNNN是您的PIN码，且必须放在引号中）。

Zabbix已通过以下GSM调制解调器的测试：

-   Siemens MC35
-   Teltonika ModemCOM/G10

配置短信作为消息的传送通道时，需要将短信配置为媒介类型，并输入相应用户的电话号码。

[comment]: # ({/1d4d3e98-657f31be})

[comment]: # ({e2e4be53-e7ed9491})

#### 配置

要将 SMS 配置为媒介类型：

-   进入 *管理 → 媒介类型*
-   点击 *创建媒介类型* (或者点击预定义的媒介类型列表中的 SMS)。

如下参数仅适用于SMS媒介类型:

|参数|说明|
|---------|-----------|
|*GSM modem*|设置GSM调制解调器的串行设备名称.|

有关如何配置默认消息和告警处理的详细内容，请参见[通用媒体媒介类型参数](/manual/config/notifications/media#common_parameters) 。请注意，SMS 通知无法并行发送。

[comment]: # ({/e2e4be53-e7ed9491})

[comment]: # ({2f7938d5-84a44424})
#### 用户媒介
配置完SMS媒体类型后，需进入 *管理 →用户* 为用户配置SMS媒介。配置用户媒介的步骤和配置其它媒介类型的方式类似，可以参见[媒介类型](/manual/config/notifications/media#user_media) 

[comment]: # ({/2f7938d5-84a44424})
