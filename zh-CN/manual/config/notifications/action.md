[comment]: # translation:outdated

[comment]: # ({7ba67424-526f6fc0})
# 2 动作

[comment]: # ({/7ba67424-526f6fc0})

[comment]: # ({804e5e81-b19d9072})
#### 概述

如果您希望对产生的事件进行一些操作（例如发送通知），则需要配置动作。

可以根据所有支持类型的事件来定义动作：

-   触发器动作 - 当trigger的状态从 *OK*变为*PROBLEM*或者从  *PROBLEM*  恢复到  *OK*时
-   服务动作 - 当服务的状态从 *OK*变为*PROBLEM*或者从  *PROBLEM*  恢复到  *OK*时
-   自动发现动作 - 针对网络自动发现事件发生时
-   自动注册动作 - 当新的agents自动注册（或已注册主机元数据发生改变）时
-   内部动作 - 当监控项变成不支持状态或触发器进入未知状态时

[comment]: # ({/804e5e81-b19d9072})

[comment]: # ({new-cf7fda79})

#### 配置动作

配置动作，步骤如下：

-   在 *Configuration* 菜单中点击*Actions*并且选择对应的动作类型。(也可以在标题下拉框中进行动作类型的切换)
-   点击*Create action*
-   给动作命名
-   选择执行动作的条件[conditions](/manual/config/notifications/action/conditions)
-   选择执行动作的操作[operations](/manual/config/notifications/action/operation)

常见动作属性：

![](../../../../assets/en/manual/config/notifications/action.png)

带有红色星号的为必填字段。

|参数|说明|
|---------|-----------|
|*Name*|动作名称(需唯一)|
|*Type of calculation*|选择评估 [option](/manual/config/notifications/action/conditions#type_of_calculation) 作为行动条件（有多个条件）：<br>**And** - 必须满足所有条件<br>**Or** - 只需满足其中一个条件<br>**And/Or** - 两者的组合：AND有不同的条件类型，OR有相同的条件类型<br>**自定义的表达式** - 用户自定义计算公式来进行条件的计算.|
|*Conditions*|条件的清单。<br>点击*Add*添加一个新的[condition](/manual/config/notifications/action/conditions).|
|*Enabled*|选中复选框以启用该操作。否则将被禁用。|

[comment]: # ({/new-cf7fda79})
