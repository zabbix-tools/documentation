[comment]: # translation:outdated

[comment]: # ({4b924e39-81dd14e7})
# 5 自定义触发器严重性

可以在 *管理→常规 → 触发器严重性* 中配置触发器严重性名称和严重性颜色相关的GUI主题。颜色在所有GUI主题之间共享。


[comment]: # ({/4b924e39-81dd14e7})

[comment]: # ({2e1b7c7d-78725316})
#### 翻译自定义严重性的名称

::: noteimportant
如果使用Zabbix前端翻译, 自定义严重性名称将会覆盖默认翻译名称。
:::
默认触发器严重性名称可用于在所有语言环境中进行翻译。如果更改严重性名称，则会在所有区域设置中使用自定义名称，并且需要额外的手动翻译。

自定义严重性名称的翻译步骤:

-   设置自定义严重性名称，例如 '重要'
-   编辑<frontend\_dir>/locale/<required\_locale>/LC\_MESSAGES/frontend.po
-   添加如下2行:

```{=html}
<!-- -->
```
    msgid "重要"
    msgstr "<翻译字符串>"

保存文件。

-   在<frontend\_dir>/locale/README创建.mo文件作为描述

这里 **msgid** 应该匹配新的自定义严重性名称， **msgstr**
应该是用特定语言翻译的。

此过程应在每个严重性名称更改之后执行。

[comment]: # ({/2e1b7c7d-78725316})
