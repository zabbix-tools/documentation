[comment]: # translation:outdated

[comment]: # ({new-29480506})
# 1 配置一个触发器

[comment]: # ({/new-29480506})

[comment]: # ({new-ea19cfe1})
#### 概述

配置一个触发器，进行下面步骤：

-   进入： *配置* → *主机*
-   点击主机一行的 *触发器*
-   点击右上角的 *创建触发器*
    （或者点击触发器名称去修改一个已存在的触发器）
-   在窗口中输入触发器的参数

[comment]: # ({/new-ea19cfe1})

[comment]: # ({81365808-f29d794b})

#### 配置

**Trigger(触发器)**标签页包含了所有必要的触发器属性。
![](../../../../assets/en/manual/config/triggers/trigger.png)

所有强制输入字段都用红色星号标记。

|参数                 |描述|
|--------------------------|-|
|*名称*               |触发器名称.<br>支持的 [macros（宏）](/manual/appendix/macros/supported_by_location): `{HOST.HOST}, {HOST.NAME}, {HOST.CONN}, {HOST.DNS}, {HOST.IP}, {ITEM.VALUE}, {ITEM.LASTVALUE}` 和`{$MACRO}`。<br>**$1, $2...$9** 宏可以用来指第一,第二...第九表达式的常量。<br>*备注*: 如果引用了相对简单的常量或明确的表达式，$1-$9宏将会正确解析。例如, 如果表达式是{New host:system.cpu.load\[percpu,avg1\].last()}>5，则"Processor load above $1 on {HOST.NAME}"将会自动更改成 "Processor load above 5 on New host"|
|*事件名称*|如果定义了这个名称，将使用这个名称来创建问题事件名称，而不是触发器名称。<br>事件名称可用于构建包含问题数据的有意义的警报 (查看[示例(/manual/config/triggers/expression#example_17))。<br>支持与触发器名称相同的一组宏，加上{TIME}和{?EXPRESSION}表达式宏。<br>从Zabbix 5.2.0开始支持。|
|*操作数据*|操作数据允许定义任意字符串和宏。这些宏会在*Monitoring（监控）* → *[Problems（问题）](/manual/web_interface/frontend_sections/monitoring/problems)*.中动态地解析实时数据。触发器名称中的宏将在问题发生时解析为它们的值，并成为静态问题名称的基础，而操作数据中的宏则保持动态显示最新信息的能力。<br>支持与触发器名称相同的一组宏。|
|*严重性*|通过点击对应的按钮来设置所需的触发器[severity](严重性)。|
|*表达式*         |定义问题条件的逻辑表达式。当表达式中包含的所有条件都满足时，就会产生问题，即表达式的计算结果为TRUE。除非在恢复表达式中指定了额外的恢复条件，否则该问题将在表达式计算结果为FALSE时得到解决。|
|*生成OK事件*      | OK 事件生成选项：:<br>**Expression（表达式）** - OK事件基于与问题事件相同的表达式生成;<br>**Recovery expression（恢复表达式）** - 如果问题表达式计算为false，恢复表达式计算为true，则生成OK事件;<br>**None** - 在这种情况下，触发器将永远不会返回到OK状态。<br>从Zabbix 3.2.0开始支持|
|*恢复表达式*         |逻辑[expression(表达式)]用于定义问题解决的条件。<br>只有在表达式表达式计算为FALSE之后才对恢复表达式进行评估。如果问题条件仍然存在，则不可能通过恢复表达式来解决问题。<br>此字段是可选的，仅在*OK 事件生成*选择恢复表达式。<br>从Zabbix 3.2.0开始支持|
|*异常事件生成模式*   |生成问题事件的模式：：<br>**Single（单个）** - 当触发器第一次进入‘异常‘状态时，生成一条单个事件。;<br>**Multiple（多重）** - 每一个触发器“异常”评估都将生产一条事件。|
|*事件成功关闭*       |选择 OK 事件是否关闭：:<br>**All problems（所有问题）** - 此触发器的所有问题<br>**All problems if tag values match（所有问题如果标签值匹配）** - 只有那些匹配事件标签值引发的问题。<br>从Zabbix 3.2.0开始支持。|
|*匹配标记*           |输入事件标签名称以用于事件关联。<br>如果在*事件成功关闭*中选择了‘所有问题如果标签值匹配’，在这种情况下是强制性的。<br>从Zabbix 3.2.0开始支持。|
|*允许手动关闭*      | 检查是否允许[手动关闭](/manual/config/events/manual_close)由该触发器生成的问题事件。在确认问题事件时，手动关闭是可能的。<br>从Zabbix 3.2.0开始支持。|
|*URL*|如果不为空, 在*监测 → 问题*和*问题*仪表板中点击触发器名称，这里输入的URL可以作为链接。(*URL*选项在*触发器*上下文菜单)<br>可以在触发器URL字段中使用宏 - {TRIGGER.ID}, 多个[{HOST.\*}](/manual/appendix/macros/supported_by_location)宏(Zabbix3.0.0以后的版本)和用户宏(Zabbix3.0.0以后的版本).|
|*描述*               文|字段用于提供有关此触发器的更多信息。可能包含解决具体问题的指令、负责人员的联系细节等。<br>*从Zabbix 2.2开始*, 描述可以包含与触发器名称相同的宏集。|
|*已启用*             如果|要，不选中该框将禁用触发器。|


**Tags（标签）** 选项卡允许您定义触发级 [tags](/manual/config/tagging)。此触发器的所有问题都将使用此处输入的值进行标记。
![](../../../../assets/en/manual/config/triggers/trigger_b.png)

此外，*Inherited and trigger tags（继承和触发器标记）* 选项允许查看在模板级定义的标记，如果触发器来自该模板。如果有多个模板使用相同的标记，这些标记只显示一次，并且用逗号分隔模板名。触发器不“继承”和显示主机级标记。

|Parameter|Description|
|---------|-----------|
|*Name（名称）/Value（值）*|设置自定义标记来标记触发事件。<br>标签是一对标签名和值。只能使用名称或将其与值配对。一个触发器可能有几个名称相同但值不同的标记。<br>用户宏，用户宏上下文，低级别发现宏和宏[functions(函数)](/manual/config/宏/macro_functions)带有' {{ITEM.VALUE}},{{ITEM.LASTVALUE}}和低级发现宏。低级发现宏可以在宏上下文中使用。<br>{TRIGGER.ID}宏。它可能有助于识别从触发器原型创建的触发器，例如，在维护期间抑制这些触发器产生的问题。当扩展值的总长度超过255时，将被截断为255个字符。<br>查看所有支持事件标签的[macros（宏）](/manual/config/tagging#macro_support)。<br>[Event tags（事件标签）](/manual/config/tagging)可以用于事件关联，在动作条件中，也可以在*Monitoring*→*Problems*或*Problems* widget中看到|


**Dependencies（依赖项）** 选项卡包含触发器的所有 [dependencies](dependencies)。

单击 *Add* 添加新的依赖项。

::: 提醒
 您还可以通过打开现有触发器来配置触发器，
 按下 *Clone* 按钮，然后保存在不同的位置名称。
:::

[comment]: # ({/81365808-f29d794b})

[comment]: # ({1e02daf9-dbfe2e70})

#### 测试表达式

可以根据接收的值测试配置的触发器表达式，以确定表达式结果。

以官方模板的表达为例：

    {Template Net Cisco IOS SNMPv2:sensor.temp.value[ciscoEnvMonTemperatureValue.{#SNMPINDEX}].avg(5m)}>{$TEMP_WARN}
    or
    {Template Net Cisco IOS SNMPv2:sensor.temp.status[ciscoEnvMonTemperatureState.{#SNMPINDEX}].last(0)}={$TEMP_WARN_STATUS}

若要测试表达式，请点击表达式字段下的 *Expression constructor（表达式构造器）*。

![](../../../../assets/zh/manual/config/triggers/zh_expr_constructor_link_new.png)

在表达式构造器列出了所有单个表达式。打开测试窗口，点击在表达式列表下方*Test(测试)*。

![](../../../../assets/zh/manual/config/triggers/zh_triggersexpr_constructor_test_new.png.png)

在测试窗口中，您可以输入示例值（在这个示例中为“80, 70, 0，1”），然后点击
*测试* 按钮查看表达式结果。

![](../../../../assets/zh/manual/config/triggers/zh_expression_test_new.png.png){width="600"}

可以看到每个表达的结果以及整个表达的结果。

“TRUE”结果意味着指定的表达式是正确的。在这个特定的情况A下，“80”大于{$TEMP\_WARN}指定值“70”，出现“TRUE”结果。

“FALSE”结果表示指定的表达式不正确。在这个特定的情况B下，在这个例子中{$TEMP\_WARN\_STATUS}是“1”，需要与指定的“0”值相等，这是错误的。出现“FALSE”结果。

选择的表达式类型是“OR”/“true”。如果指定条件中的至少一个（在这种情况下为A或B）是真的，那么最终结果也是TRUE。意味着，当前值超过了警告值，出现了异常。

[comment]: # ({/1e02daf9-dbfe2e70})
