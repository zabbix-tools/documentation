[comment]: # translation:outdated

[comment]: # ({af3647d3-31b6b7d8})
# 1 主机和主机群组

[comment]: # ({/af3647d3-31b6b7d8})

[comment]: # ({b6e809fa-61c2f2bf})
#### 什么是“主机”？

通常来讲， Zabbix 主机是指你所要监控的设备（例如服务器、工作站、交换机等）。

创建主机是使用 Zabbix 监控的首要任务之一。例如，如果你想监控一台服务器 “X” 上的某些指标，你必须首先创建这台名为 “服务器X” 的主机，然后才能添加监视项。

主机群组是由主机组成的。

前往 [创建和配置主机](/manual/config/hosts/host)。

[comment]: # ({/b6e809fa-61c2f2bf})
