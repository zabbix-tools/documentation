[comment]: # translation:outdated

[comment]: # ({new-94205f99})
# 2 监控项



[comment]: # ({/new-94205f99})

[comment]: # ({01d6472b-3f680eac})
#### 概述

监控项用来从主机收集数据。

配置主机后，你需要添加一些监控项以开始获取实际数据。

一个监控项是一个独立的指标。快速添加多个监控项的一种方法是将一个预定义的模板附加到主机。然而，为了优化系统性能，您可能需要对模板进行微调，使其只有必要的监控项被以必要的频率监控。

在单个监控项中，你可以指定从主机收集哪些数据。

为此，你需要使用[监控项键值（key）](/zh/manual/config/items/item/key)。
从而，具有键值名称为 **system.cpu.load** 的监控项将收集处理器负载的数据，而键值名为 **net.if.in** 的监控项将收集传入流量信息。

要在监控项键值中指定更多的参数，请在键值名称后的方括号中添加这些参数。
例如，system.cpu.load**\[avg5\]** 将返回最近5分钟的处理器负载平均值，而 net.if.in**\[eth0\]** 将显示接口eth0的传入流量。

::: noteclassic
所有支持的监控项类型和监控项键值的信息，请参阅[监控项类型](/zh/manual/config/items/itemtypes)的各个部分。
:::

继续[创建和配置监控项](/zh/manual/config/items/item)。

[comment]: # ({/01d6472b-3f680eac})
