[comment]: # translation:outdated

[comment]: # ({new-23de852a})
# 11 值缓存

[comment]: # ({/new-23de852a})

[comment]: # ({new-f73e0537})
#### Overview 概览

为了更快地计算触发器表达式、计算或聚合类型监控项和一些宏。自ZABBIX
2.2起，ZABBIX Server支持值缓存选项。\

这个存放在内存中的缓存，可以用于访问历史数据，而不需要对数据库直接执行SQL调用。如果缓存中不存在请求得历史值，则会从数据库请求缺失的数据，并相应地更新缓存。\

要启用值缓存功能，Zabbix服务器[配置文件](/zh/manual/appendix/config/zabbix_server)支持可选的**ValueCacheSize**参数。\

有两个内部的监控项来监控值缓存：**zabbix
\[vcache，buffer，<mode>\]** 和 **zabbix
\[vcache，cache，<parameter>\]**。查看更多细节，请参阅\[\[zh:manual:config:items:itemtypes:internal|内部监控项\]。\

[comment]: # ({/new-f73e0537})

[comment]: # ({new-3add2a5c})
Item values remain in value cache either until:

-   the item is deleted (cached values are deleted after the next configuration sync);
-   the item value is outside the time or count range specified in the trigger/calculated item expression
    (cached value is removed when a new value is received);
-   the time or count range specified in the trigger/calculated item expression is changed
    so that less data is required for calculation (unnecessary cached values are removed after 24 hours).

::: notetip
Value cache status can be observed by using the server [runtime control](/manual/concepts/server#runtime-control) option
`diaginfo` (or `diaginfo=valuecache`) and inspecting the section for value cache diagnostic information.
This can be useful for determining misconfigured triggers or calculated items.
:::

[comment]: # ({/new-3add2a5c})

[comment]: # ({new-1fb59f91})
To enable the value cache functionality, an optional **ValueCacheSize**
parameter is supported by the Zabbix server
[configuration](/manual/appendix/config/zabbix_server) file.

Two internal items are supported for monitoring the value cache:
**zabbix\[vcache,buffer,<mode>\]** and
**zabbix\[vcache,cache,<parameter>\]**. See more details with
[internal items](/manual/config/items/itemtypes/internal).

[comment]: # ({/new-1fb59f91})
