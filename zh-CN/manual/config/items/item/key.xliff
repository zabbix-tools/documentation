<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="zh-CN" datatype="plaintext" original="manual/config/items/item/key.md">
    <body>
      <trans-unit id="856c911b" xml:space="preserve">
        <source># 1 Item key format

Item key format, including key parameters, must follow syntax rules. The
following illustrations depict the supported syntax. Allowed elements
and characters at each point can be determined by following the arrows -
if some block can be reached through the line, it is allowed, if not -
it is not allowed.

![](../../../../../assets/en/manual/config/item_key_2.png){width="600"}

To construct a valid item key, one starts with specifying the key name,
then there's a choice to either have parameters or not - as depicted by
the two lines that could be followed.</source>
        <target state="needs-translation"># 1 监控项键值的格式

监控项键值的格式（包括键值的参数）必须遵循语法规则。以下插图描述了支持的语法。可以通过跟随箭头来确定每个点上允许的元素和字符 - 如果可以通过线到达某个块，则允许，否则 - 不允许。

![](../../../../../assets/en/manual/config/item_key_2.png){width="600"}

要构建有效的监控项键值，首先要指定键值的名称，然后选择是否具有参数，后面的两行描述了这一点。</target>
      </trans-unit>
      <trans-unit id="63ccf8e4" xml:space="preserve">
        <source>#### Key name

The key name itself has a limited range of allowed characters, which
just follow each other. Allowed characters are:

    0-9a-zA-Z_-.

Which means:

-   all numbers;
-   all lowercase letters;
-   all uppercase letters;
-   underscore;
-   dash;
-   dot.

![](../../../../../assets/en/manual/config/key_name.png)</source>
        <target state="needs-translation">#### 键值 名称

Key名本身具有有限的允许字符范围，允许的字符是：

    0-9a-zA-Z_-.

即:

-   所有的数字;
-   所有的小写字母;
-   所有大写字母;
-   下划线;
-   减号;
-   点。

![](../../../../../assets/en/manual/config/key_name.png)</target>
      </trans-unit>
      <trans-unit id="af5ae7f1" xml:space="preserve">
        <source>#### Key parameters

An item key can have multiple parameters that are comma separated.

![](../../../../../assets/en/manual/config/key_parameters.png)

Each key parameter can be either a quoted string, an unquoted string or
an array.

![](../../../../../assets/en/manual/config/item_parameter.png)

The parameter can also be left empty, thus using the default value. In
that case, the appropriate number of commas must be added if any further
parameters are specified. For example, item key
**icmpping\[,,200,,500\]** would specify that the interval between
individual pings is 200 milliseconds, timeout - 500 milliseconds, and
all other parameters are left at their defaults.

It is possible to include macros in the parameters. Those can be [user macros](/manual/appendix/macros/supported_by_location_user#items-item-prototypes) or some of the built-in macros.
To see what particular built-in macros are supported in item key parameters,
search the page [Supported macros](/manual/appendix/macros/supported_by_location) for "item key parameters".</source>
        <target state="needs-translation">#### 键值的参数

监控项的键值可以有多个逗号分隔的参数。

![](../../../../../assets/en/manual/config/key_parameters.png)

每个key参数可以是带引号、无引号的字符串或数组。

![](../../../../../assets/en/manual/config/item_parameter.png)

参数也可以为空，此时使用默认值。在这种情况下，如果指定了其它参数，则必须添加对应数量的逗号。例如，键值
**icmpping\[,,200,,500\]**
将指定每ping一次的时间间隔为200毫秒，超时时间为500毫秒，其它所有参数为默认值。</target>
      </trans-unit>
      <trans-unit id="b4a70ec9" xml:space="preserve">
        <source>#### Parameter - quoted string

If the key parameter is a quoted string, any Unicode character is
allowed. If the key parameter string contains a quotation mark, this parameter has
to be quoted, and each quotation mark which is a part of the parameter
string has to be escaped with a backslash (`\`) character. If the key parameter string contains comma, this parameter has to be quoted.

![](../../../../../assets/en/manual/config/key_param_quoted_string.png)

::: notewarning
To quote item key parameters, use double quotes
only. Single quotes are not supported.
:::</source>
        <target state="needs-translation">#### 参数 - 带引号的字符串

如果键值参数为带引号的字符串，则允许任何Unicode字符。

如果键值参数的字符串中包含逗号，则该参数必须用引号引起来。

如果键值参数的字符串包含引号，则该参数必须用引号括起来，并且作为参数字符串一部分的每个引号都必须用反斜杠(\)进行转义。

![](../../../../../assets/en/manual/config/key_param_quoted_string.png)

::: notewarning
要引用监控项键值参数，只能使用双引号，不支持单引号。
:::</target>
      </trans-unit>
      <trans-unit id="3376836e" xml:space="preserve">
        <source>#### Parameter - unquoted string

If the key parameter is an unquoted string, any Unicode character is
allowed except comma and right square bracket (\]). Unquoted parameter
cannot start with left square bracket (\[).

![](../../../../../assets/en/manual/config/key_param_unquoted_string.png)</source>
      </trans-unit>
      <trans-unit id="5289a973" xml:space="preserve">
        <source>#### Parameter - array

If the key parameter is an array, it is again enclosed in square
brackets, where individual parameters come in line with the rules and
syntax of specifying multiple parameters.

![](../../../../../assets/en/manual/config/key_param_array.png)

::: noteimportant
Multi-level parameter arrays, e.g.
`[a,[b,[c,d]],e]`, are not allowed.
:::</source>
      </trans-unit>
    </body>
  </file>
</xliff>
