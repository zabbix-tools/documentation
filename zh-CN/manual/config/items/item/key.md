[comment]: # translation:outdated

[comment]: # ({3aa5f258-856c911b})
# 1 监控项键值的格式

监控项键值的格式（包括键值的参数）必须遵循语法规则。以下插图描述了支持的语法。可以通过跟随箭头来确定每个点上允许的元素和字符 - 如果可以通过线到达某个块，则允许，否则 - 不允许。

![](../../../../../assets/en/manual/config/item_key_2.png){width="600"}

要构建有效的监控项键值，首先要指定键值的名称，然后选择是否具有参数，后面的两行描述了这一点。

[comment]: # ({/3aa5f258-856c911b})

[comment]: # ({a77cdb1b-63ccf8e4})
#### 键值 名称

Key名本身具有有限的允许字符范围，允许的字符是：

    0-9a-zA-Z_-.

即:

-   所有的数字;
-   所有的小写字母;
-   所有大写字母;
-   下划线;
-   减号;
-   点。

![](../../../../../assets/en/manual/config/key_name.png)

[comment]: # ({/a77cdb1b-63ccf8e4})

[comment]: # ({72e928e2-af5ae7f1})
#### 键值的参数

监控项的键值可以有多个逗号分隔的参数。

![](../../../../../assets/en/manual/config/key_parameters.png)

每个key参数可以是带引号、无引号的字符串或数组。

![](../../../../../assets/en/manual/config/item_parameter.png)

参数也可以为空，此时使用默认值。在这种情况下，如果指定了其它参数，则必须添加对应数量的逗号。例如，键值
**icmpping\[,,200,,500\]**
将指定每ping一次的时间间隔为200毫秒，超时时间为500毫秒，其它所有参数为默认值。

[comment]: # ({/72e928e2-af5ae7f1})

[comment]: # ({a9b92998-b4a70ec9})
#### 参数 - 带引号的字符串

如果键值参数为带引号的字符串，则允许任何Unicode字符。

如果键值参数的字符串中包含逗号，则该参数必须用引号引起来。

如果键值参数的字符串包含引号，则该参数必须用引号括起来，并且作为参数字符串一部分的每个引号都必须用反斜杠(\)进行转义。

![](../../../../../assets/en/manual/config/key_param_quoted_string.png)

::: notewarning
要引用监控项键值参数，只能使用双引号，不支持单引号。
:::

[comment]: # ({/a9b92998-b4a70ec9})

[comment]: # ({new-3376836e})
#### 参数 - 不带引号的字符串

如果键值的参数是一个不带引号的字符串，除逗号和右方括号（\]）之外，不带引号的参数不能以左方括号（\[）开头。

![](../../../../../assets/en/manual/config/key_param_unquoted_string.png)

[comment]: # ({/new-3376836e})

[comment]: # ({new-5289a973})
#### 参数 - 数组

如果key参数是一个数组，它需要包含在方括号中，其中各个参数需要符合多个参数的规则和语法。

![](../../../../../assets/en/manual/config/key_param_array.png)

::: noteimportant
多级参数数组, 例如 `[a,[b,[c,d]],e]`,
是不支持的。
:::

[comment]: # ({/new-5289a973})
