[comment]: # translation:outdated

[comment]: # ({87f1dd6e-c19f94bc})

# 从JSONPath 中的 LLD 宏值中转义特殊字符

当在 JSONPath 预处理中使用低级发现宏并解析其值时，将应用以下特殊字符转义规则：

-  只考虑转义反斜杠(\\)和双引号(")字符；
-   如果解析的宏值包含这些字符，每个字符都用反斜杠转义；
-   如果解析的宏值包含这些字符，则每个字符都用反斜杠进行转义；

例子:

|JSONPath|LLD 宏值|替换后|
|--------|---------------|------------------|
|$.\[?(@.value == "{\#MACRO}")\]|special "value"|$.\[?(@.value == "special \\"value\\"")\]|
|^|c:\\temp|$.\[?(@.value == "c:\\\\temp")\]|
|^|a\\\\b|$.\[?(@.value == "a\\\\\\\\b")\]|

在表达式中使用时，可能有特殊字符的宏应该用双引号括起来:

|JSONPath|LLD 宏值|替换后|结果|
|--------|---------------|------------------|------|
|$.\[?(@.value == "{\#MACRO}")\]|special "value"|$.\[?(@.value == "special \\"value\\"")\]|OK|
|$.\[?(@.value == {\#MACRO})\]|^|$.\[?(@.value == special \\"value\\")\]|**Bad JSONPath expression**|

在路径中使用时，可能包含特殊字符的宏应括在方括号 **和** 双引号中：
|JSONPath|LLD 宏值|替换后|结果|
|--------|---------------|------------------|------|
|$.\["{\#MACRO}"\].value|c:\\temp|$.\["c:\\\\temp"\].value|OK|
|$.{\#MACRO}.value|^|$.c:\\\\temp.value|**Bad JSONPath expression**|

[comment]: # ({/87f1dd6e-c19f94bc})
