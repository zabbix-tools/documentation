[comment]: # translation:outdated

[comment]: # ({aa670026-d3accdff})
# 其他 JavaScript 对象

[comment]: # ({/aa670026-d3accdff})

[comment]: # ({f7725e3b-ab8bd378})
### 概述

本节介绍 Zabbix 添加到使用 Duktape 实现的 JavaScript 语言和支持的全局 JavaScript 函数。

[comment]: # ({/f7725e3b-ab8bd378})

[comment]: # ({0a354bd3-c73f2449})
### 内置对象

[comment]: # ({/0a354bd3-c73f2449})

[comment]: # ({96445352-e5dc8dd0})
#### Zabbix

Zabbix 对象提供与内部 Zabbix 功能的交互。

|方法|描述|
|------|-----------|
|`log(loglevel, message)`|使用 <loglevel> 日志级别将 <message> 写入 Zabbix 日志（参见配置文件 DebugLevel 参数）。|

示例:

    Zabbix.log(3, "this is a log entry written with 'Warning' log level")

您可以使用以下别名：

|别名|原义|
|-----|--------|
|console.log(object)|Zabbix.log(4, JSON.stringify(object))|
|console.warn(object)|Zabbix.log(3, JSON.stringify(object))|
|console.error(object)|Zabbix.log(2, JSON.stringify(object))|

|方法|描述|
|------|-----------|
|`sleep(delay)`|将 JavaScript 执行延迟 `delay` 毫秒。|

示例（延迟执行 15 秒）：

    Zabbix.sleep(15000)
  

[comment]: # ({/96445352-e5dc8dd0})

[comment]: # ({new-413974f8})

#### Http请求

该对象封装了 cURL 句柄，允许制作简单的 HTTP
要求。 错误作为异常抛出。

::: noteimportant
HttpRequest 是自 Zabbix 5.4 以来此对象的新名称。 以前它曾经被称为 CurlHttpRequest。 Zabbix 5.4 中的方法名称也已更改。 旧的对象/方法名称现在已弃用，并且在 Zabbix 6.0 之后将停止对它们的支持。
:::

|方法|描述|
|------|-----------|
|`addHeader(name, value)`|添加 HTTP 标头字段。此字段用于所有后续请求，直到使用 clearHeader() 方法清除。|
|`clearHeader()`|清除 HTTP 标头。如果没有设置头字段，HttpRequest 会将 Content-Type 设置为 application/json 如果发布的数据是 JSON 格式的；否则文本/纯文本。|
|`connect(url)`|向 URL 发送 HTTP CONNECT 请求并返回响应。|
|`customRequest(method, url, data)`|允许在第一个参数中指定任何 HTTP 方法。 | 将方法请求发送到带有可选 *data* 负载的 URL 并返回响应。|
|`delete(url, data)`|使用可选的 *data* 负载向 URL 发送 HTTP DELETE 请求并返回响应。|
|`getHeaders()`|返回接收到的 HTTP 标头字段的对象。|
|`get(url, data)`|发送 HTTP GET 请求到带有可选 *data* 负载的 URL 并返回响应。|
|`head(url)`|向 URL 发送 HTTP HEAD 请求并返回响应。|
|`options(url)`|向 URL 发送 HTTP OPTIONS 请求并返回响应。|
|`patch(url, data)`|将 HTTP PATCH 请求发送到带有可选 *data* 负载的 URL 并返回响应。|
|`put(url, data)`|将 HTTP PUT 请求发送到带有可选 *data* 负载的 URL 并返回响应。|
|`post(url, data)`|发送 HTTP POST 请求到带有可选 *data* 负载的 URL 并返回响应。|
|`getStatus()`|返回最后一个 HTTP 请求的状态码。|
|`setProxy(proxy)`|将 HTTP 代理设置为“代理”值。如果此参数为空，则不使用代理。|
|`setHttpAuth(bitmask, username, password)`|在“位掩码”参数中设置启用的 HTTP 身份验证方法（HTTPAUTH\_BASIC、HTTPAUTH\_DIGEST、HTTPAUTH\_NEGOTIATE、HTTPAUTH\_NTLM、HTTPAUTH\_NONE）。<br>HTTPAUTH \_NONE 标志允许禁用 HTTP 身份验证。<br>示例：<br>`request.setHttpAuth(HTTPAUTH_NTLM \| HTTPAUTH_BASIC, username, password)`<br>`request.setHttpAuth(HTTPAUTH_NONE)`|
|`trace(url, data)`|发送 HTTP TRACE 请求到带有可选 *data* 负载的 URL 并返回响应。|


示例:

``` {.java}
try {
    Zabbix.log(4, 'jira webhook script value='+value);
  
    var result = {
        'tags': {
            'endpoint': 'jira'
        }
    },
    params = JSON.parse(value),
    req = new HttpRequest(),
    fields = {},
    resp;
  
    req.addHeader('Content-Type: application/json');
    req.addHeader('Authorization: Basic '+params.authentication);
  
    fields.summary = params.summary;
    fields.description = params.description;
    fields.project = {"key": params.project_key};
    fields.issuetype = {"id": params.issue_id};
    resp = req.post('https://tsupport.zabbix.lan/rest/api/2/issue/',
        JSON.stringify({"fields": fields})
    );
  
    if (req.getStatus() != 201) {
        throw 'Response code: '+req.getStatus();
    }
  
    resp = JSON.parse(resp);
    result.tags.issue_id = resp.id;
    result.tags.issue_key = resp.key;
} catch (error) {
    Zabbix.log(4, 'jira issue creation failed json : '+JSON.stringify({"fields": fields}));
    Zabbix.log(4, 'jira issue creation failed : '+error);
  
    result = {};
}
  
return JSON.stringify(result);
```

[comment]: # ({/new-413974f8})

[comment]: # ({1891e5ae-06cac1ca})

#### XML

XML 对象允许处理项目中的 XML 数据以及低级发现预处理和 webhook。

::: noteimportant
为了使用 XML 对象，必须使用 libxml2 支持编译server/proxy。
:::

|Method|Description|
|------|-----------|
|`XML.query(expression, data)`|使用 XPath 检索节点内容。 如果未找到节点，则返回 null。<br>**expression** - XPath 表达式；<br>**data** - XML 数据作为字符串。|
|`XML.toJson(data)`|将 XML 格式的数据转换为 JSON。|
|`XML.fromJson(object)`|将 JSON 格式的数据转换为 XML。|

示例:

*输入:*

    <menu>
        <food type = "breakfast">
            <name>Chocolate</name>
            <price>$5.95</price>
            <description></description>
            <calories>650</calories>
        </food>
    </menu>

*输出:*

``` {.java}
{
    "menu": {
        "food": {
            "@type": "breakfast",
            "name": "Chocolate",
            "price": "$5.95",
            "description": null,
            "calories": "650"
        }
    }
}
```

[comment]: # ({/1891e5ae-06cac1ca})

[comment]: # ({5c0ac335-a12fbdc2})

##### 序列化规则

XML 到 JSON 的转换将根据以下规则进行处理（对于 JSON 到 XML 的转换，应用相反的规则）：

1\. XML 属性将被转换为名称前面带有“@”的键。

示例:

*输入:*

     <xml foo="FOO">
       <bar>
         <baz>BAZ</baz>
       </bar>
     </xml>

*输出:*

``` {.java}
 {
   "xml": {
     "@foo": "FOO",
     "bar": {
       "baz": "BAZ"
     }
   }
 }
```

2\. 自闭合元素 (<foo/>) 将被转换为具有 'null' 值。

示例:

*输入:*

    <xml>
      <foo/>
    </xml>

*输出:*

``` {.java}
{
  "xml": {
    "foo": null
  }
}
```

3\. 空属性（具有 "" 值）将被转换为具有空字符串 ('') 值。

示例:

*输入:*

    <xml>
      <foo bar="" />
    </xml>

*输出:*

``` {.java}
{
  "xml": {
    "foo": {
      "@bar": ""
    }
  }
}
```

4\. 具有相同元素名称的多个子节点将被转换为具有值数组作为其值的单个键。

示例:

*输入:*

    <xml>
      <foo>BAR</foo>
      <foo>BAZ</foo>
      <foo>QUX</foo>
    </xml>

*输出:*

``` {.java}
{
  "xml": {
    "foo": ["BAR", "BAZ", "QUX"]
  }
}
```

5\. 如果文本元素没有属性也没有子元素，它将被转换为字符串。

示例:

*输入:*

    <xml>
        <foo>BAZ</foo>
    </xml>

*输出:*

``` {.java}
{
  "xml": {
    "foo": "BAZ"
   }
}
```

6\.如果一个文本元素没有子元素，但有属性：文本内容将被转换为一个元素，键为'\#text'，内容为值； 属性将按照序列化规则 1 中的说明进行转换。

示例:

*输入:*

    <xml>
      <foo bar="BAR">
        BAZ
      </foo>
    </xml>

*输出:*

``` {.java}
{
  "xml": {
    "foo": {
      "@bar": "BAR",
      "#text": "BAZ"
    }
  }
}
```

[comment]: # ({/5c0ac335-a12fbdc2})

[comment]: # ({a4a92b03-1368a2b0})

### 全局 JavaScript 函数

使用 Duktape 实现了额外的全局 JavaScript 函数：

-   btoa(string) - 将字符串编码为 base64 字符串
-   atob(base64\_string) - 对 base64 字符串进行解码

``` {.java}
try {
    b64 = btoa("utf8 string");
    utf8 = atob(b64);
} 
catch (error) {
    return {'error.name' : error.name, 'error.message' : error.message}
}
```

-   md5(string) - 计算字符串的 MD5 哈希
-   sha256(string) - 计算字符串的 SHA256 哈希

[comment]: # ({/a4a92b03-1368a2b0})
