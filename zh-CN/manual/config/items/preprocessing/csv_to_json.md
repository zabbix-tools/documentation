[comment]: # translation:outdated

[comment]: # ({9357c1e8-c9f98b7f})
# 5 CSV 转换成 JSON 预处理

[comment]: # ({/9357c1e8-c9f98b7f})

[comment]: # ({37c59135-6cb1ec36})
#### 概述

在预处理的步骤中，可以将CSV文件数据转换为JSON格式，支持项目如下：

-   监控项 (监控项原型)
-   低级别自动发现

[comment]: # ({/37c59135-6cb1ec36})

[comment]: # ({d05077e9-77b863ab})
#### 配置

配置CSV到JSON的预处理步骤:

-   转到预处理选项卡
    [监控项](/manual/config/items/preprocessing)/[自动发现规则](/manual/discovery/low_level_discovery#preprocessing)
    配置
-   点击 *添加*
-   选择 *CSV to JSON* 选项

![](../../../../../assets/en/manual/appendix/csv_to_json_params.png){width="600"}

第一个参数允许设置自定义分隔符。注意，如果CSV输入的第一行开始"Sep="，紧随其后的是一个utf-8字符，字符将被用作分隔符的第一个参数没有设置，如果第一个参数没有设置分隔符并不是从"Sep="检索，然后一个逗号作为分隔符。

第二个可选参数允许设置引号符号。

如果 *With header row* 复选框被标记，标题行值将被解释为列名 (详见[Header
processing](#csv_header_processing) 了解更多信息)。

如果 *Custom on fail* 复选框被标记，那么在预处理步骤失败的情况下，该项将不会不受支持。另外，可以设置自定义错误处理选项:丢弃该值，设置指定值或设置指定的错误消息。

[comment]: # ({/d05077e9-77b863ab})

[comment]: # ({793cf7c5-b8624807})

#### 标题预处理

CSV文件标题行可以用两种不同的方式处理:

-   如果 *With header row* 复选框被标记 - 标题行值被解释为列名。在这种情况下，列名必须是唯一的，数据行不应该包含比标题行更多的列；
-   如果 *With header row* 复选框被标记 - 标题行解释为数据。自动生成列名(1,2,3,4…)

CSV 文件示例:

    Nr,Item name,Key,Qty
    1,active agent item,agent.hostname,33
    "2","passive agent item","agent.version","44"
    3,"active,passive agent items",agent.ping,55

::: noteclassic
输入中的引号字段中的引号字符必须在其前面加上另一个引号字符进行转义。
:::

**有标题行预处理**

有标题行预处理时期望JSON的输出:

``` {.json}
[
   {
      "Nr":"1",
      "Item name":"active agent item",
      "Key":"agent.hostname",
      "Qty":"33"
   },
   {
      "Nr":"2",
      "Item name":"passive agent item",
      "Key":"agent.version",
      "Qty":"44"
   },
   {
      "Nr":"3",
      "Item name":"active,passive agent items",
      "Key":"agent.ping",
      "Qty":"55"
   }
]
```

**无标题行预处理**

无标题行预处理时期望JSON的输出:

``` {.json}
[
   {
      "1":"Nr",
      "2":"Item name",
      "3":"Key"
      "4":"Qty"
   },
   {
      "1":"1",
      "2":"active agent item",
      "3":"agent.hostname"
      "4":"33"
   },
   {
      "1":"2",
      "2":"passive agent item",
      "3":"agent.version"
      "4":"44"
   },
   {
      "1":"3",
      "2":"active,passive agent items",
      "3":"agent.ping"
      "4":"55"
   }
]
```

[comment]: # ({/793cf7c5-b8624807})
