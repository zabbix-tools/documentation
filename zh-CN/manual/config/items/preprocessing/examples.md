[comment]: # translation:outdated

[comment]: # ({77c2b21f-1a3ec895})
# 1 使用示例

[comment]: # ({/77c2b21f-1a3ec895})

[comment]: # ({3c2ef906-f4923ce3})
#### 概述

本节介绍使用预处理步骤完成一些实际任务的示例。

[comment]: # ({/3c2ef906-f4923ce3})

[comment]: # ({2c0a8ed7-6c9dfb09})

#### 过滤VMware事件日志记录

使用正则表达式预处理过滤 VMWare 事件日志的不必要事件。

1\. 在工作的 VMWare Hypervisor 主机上，检查事件日志项 `vmware.eventlog[<url>,<mode>]` 是否存在并且工作正常。 请注意，如果在主机创建期间已链接 *Template VMVMWare* 模板，则事件日志项可能已经存在于管理程序上。

2\. 在 VMWare Hypervisor 主机上创建一个“日志”类型的 [从属项](/manual/config/items/itemtypes/dependent_items) 并将事件日志项设置为其主项。

在依赖项的“预处理”选项卡中，选择“匹配正则表达式”验证选项和填充模式，例如：

    ".* logged in .*" - 过滤事件日志中的所有日志事件
    "\bUser\s+\K\S+" -  只筛选事件日志中带有用户名的行

::: noteimportant
如果正则表达式不匹配，则依赖项变得不受支持，并显示相应的错误消息。例如，为避免这种情况，请标记“Custom on fai”复选框并选择丢弃不匹配的值。
:::

另一种允许使用匹配组和输出控制的方法是在“预处理”选项卡中选择“Regular expression”选项并设置参数，例如:

    pattern: ".*logged in.*", output: "\0" - 过滤事件日志中的每行日志事件
    pattern "User (.*?)(?=\ )", output: "\1" - 仅截取事件日志中筛选用户名信息

[comment]: # ({/2c0a8ed7-6c9dfb09})
