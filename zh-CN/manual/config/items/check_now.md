[comment]: # translation:outdated

[comment]: # ({new-17ac0201})
# 12 立刻检查

[comment]: # ({/new-17ac0201})

[comment]: # ({new-e417ef3e})
#### 概览

在ZABBIX中检查一个新监控项的值会基于已配置的更行间隔循环过程。虽然对于大多数监控项来说间隔非常短，但是还有些其它监控项（包括低级自动发现规则），更新间隔会很长。因此在显示情况下，可能需要更快的检查新的值。-例如，立刻获取可发现资源的变化。为了满足这种必要性，可以重新安排被动检查并立刻检索新的值。\

这个功能仅支持**被动** 检查。支持以下监控项的类型：

-   Zabbix agent (被动)
-   SNMPv1/v2/v3 agent
-   IPMI agent
-   简单检查
-   Zabbix 内部
-   Zabbix 聚合
-   外部检查
-   数据库监控
-   JMX 代理
-   SSH 代理
-   Telnet
-   计算
-   HTTP 代理

<note
important>检查必须存在于配置缓存中才能执行。有关详细信息请参阅[缓存更新频率](/zh/manual/appendix/config/zabbix_server)。\
在执行检查前，若配置缓存**没有**更新，那么将不会检索最近更改配置的监控项/自动发现规则。同样也无法检查刚刚创建的监控项/规则的最新值。

:::

[comment]: # ({/new-e417ef3e})

[comment]: # ({new-d51c596d})
#### 配置

要立刻执行被动检查：

-   在已存在的监控项（或自动发现规则）配置表单中点击*Check now*:

![check\_now.png](../../../../assets/en/manual/config/items/check_now.png)

-   在监控项（或发现规则列表）中，选定监控项/规则后，单击*Check now*:

![](../../../../assets/en/manual/config/items/check_now_list.png)

在第二种情况下，可以选择几个项目/规则，一次性对它们进行“立刻检查”。

[comment]: # ({/new-d51c596d})
