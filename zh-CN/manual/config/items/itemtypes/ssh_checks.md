[comment]: # translation:outdated

[comment]: # ({e3b6b1e8-26d9bc44})
# 9 SSH检查

[comment]: # ({/e3b6b1e8-26d9bc44})

[comment]: # ({37b7d483-4cdff2c1})
#### 概述

SSH检查作为一种agent-less 监控，不依赖于Zabbix agent执行。

要执行SSH检查，Zabbix服务器必须首先[配置](/manual/installation/install#from_the_sources) SSH2支持(libssh2或libssh)。参考:[要求](/manual/installation/requirements#server)。

<note important>从RHEL8/CentOS8开始只支持libssh.</note>

[comment]: # ({/37b7d483-4cdff2c1})

[comment]: # ({41d27374-5d32b87c})
#### 配置

[comment]: # ({/41d27374-5d32b87c})

[comment]: # ({991423ca-753f5c30})
##### 密码验证

SSH 检查提供两种身份验证方法，用户/密码对和基于密钥文件。

如果您正在构建从源码Zabbix，且不打算使用密钥文件，除了将 libssh2/libssh 链接到 Zabbix，无需额外配置。

[comment]: # ({/991423ca-753f5c30})

[comment]: # ({ab613f8b-44fd07da})
##### 密钥文件认证

SSH监控项基于密钥的身份验证，需要对服务器配置进行某些更改。

使用`root`身份打开 Zabbix 服务器([zabbix\_server.conf配置文件](/manual/appendix/config/zabbix_server)) 并查找以下行：

    # SSHKeyLocation=

取消注释，配置公钥和私钥所在文件夹的完整路径，样例如下：

    SSHKeyLocation=/home/zabbix/.ssh

保存文件并重启 zabbix_server 服务。

这里的 */home/zabbix* 是 *zabbix* 用户的家目录，*.ssh*是其中的一个目录。默认情况下，[ssh-keygen](http://en.wikipedia.org/wiki/Ssh-keygen)命令生成的公钥和私钥将保存在这个目录中。

不同发行版操作系统的 zabbix-server 安装程序，会创建带用户家目录的 *zabbix* 用户账户。其家目录的位置，通常会在不太明显的位置（像系统用户一样）例如：*/var/lib/zabbix*。

在生成秘钥的前可以考虑指定密钥保存的路径到更常见的位置，该路径需要跟 zabbix server 配置中的 *SSHKeyLocation* 保持一致.

如果已经根据[安装部分](/manual/installation/install#create_user_account)手动添加了*zabbix*账号，这些步骤可以跳过。因为在这种情况很可能主目录已经位于*/home/zabbix*。

更改 *zabbix* 用户的设置,必须先停止zabbix所有的进程：

 # service zabbix-agent start
 # service zabbix-server start

更改和移动一个已存在的目录应该执行以下命令:

    # usermod -m -d /home/zabbix zabbix

在旧版本中不存在zabbix根目录,需要手动创建,可以执行以下命令:

    # test -d /home/zabbix || mkdir /home/zabbix

为了保证安全，可以执行以下命令以设置跟目录的权限：

    # chown zabbix:zabbix /home/zabbix
    # chmod 700 /home/zabbix

当前进程如果是停止的,可以立即启动：

    # service zabbix-agent start
    # service zabbix-server start

生成公钥和私钥的命令和步骤如下：

    # sudo -u zabbix ssh-keygen -t rsa
    Generating public/private rsa key pair.
    Enter file in which to save the key (/home/zabbix/.ssh/id_rsa): 
    Created directory '/home/zabbix/.ssh'.
    Enter passphrase (empty for no passphrase): 
    Enter same passphrase again: 
    Your identification has been saved in /home/zabbix/.ssh/id_rsa.
    Your public key has been saved in /home/zabbix/.ssh/id_rsa.pub.
    The key fingerprint is:
    90:af:e4:c7:e3:f0:2e:5a:8d:ab:48:a2:0c:92:30:b9 zabbix@it0
    The key's randomart image is:
    +--[ RSA 2048]----+
    |                 |
    |       .         |
    |      o          |
    | .     o         |
    |+     . S        |
    |.+   o =         |
    |E .   * =        |
    |=o . ..* .       |
    |... oo.o+        |
    +-----------------+

注意: 公钥(*id\_rsa.pub*)和私钥(*id\_rsa*)默认生成在*/home/zabbix/.ssh* 路径下,该路对应对应Zabbix server的*SSHKeyLocation*配置参数

::: noteimportant
ssh-keygen命令和SSH服务可能支持“rsa”以外的密钥类型,但Zabbix使用的libssh2可能不支持。
:::

[comment]: # ({/ab613f8b-44fd07da})

[comment]: # ({0a7b1eb5-7e6b0273})
##### Shell 配置表单

对于每一个将被SSH监控的主机.这个步骤应该只执行一次.

通过下面的命令，**公钥**文件可以安装在远程主机*10.10.10.10*上，这样就可以使用*root*帐户执行SSH检查:

    # sudo -u zabbix ssh-copy-id root@10.10.10.10
 The authenticity of host '10.10.10.10 (10.10.10.10)' can't be established.
RSA key fingerprint is 38:ba:f2:a4:b5:d9:8f:52:00:09:f7:1f:75:cc:0b:46.
Are you sure you want to continue connecting (yes/no)? yes
Warning: Permanently added '10.10.10.10' (RSA) to the list of known hosts.
root@10.10.10.10's password: 
Now try logging into the machine, with "ssh 'root@10.10.10.10'", and check in:
  .ssh/authorized_keys
to make sure we haven't added extra keys that you weren't expecting.

现在*zabbix* 用户可以使用默认私钥(*/home/zabbix/.ssh/id\_rsa*) ,对目标主机进行SSH免密登录检查

    # sudo -u zabbix ssh root@10.10.10.10

如果登录成功,表示shell中的配置部分已经完成,可以关闭远程SSH会话.

[comment]: # ({/0a7b1eb5-7e6b0273})

[comment]: # ({5e2d1d6a-fe23daa8})
#####监控项配置
要执行的命令必须放在项目配置中的“Executed script”字段中.

通过将多个命令放在一行上,可以遍历执行多个命令.在这种情况下,返回的值也将被格式化为多行.

![](../../../../../assets/en/manual/config/items/itemtypes/ssh_item.png)

所有必填字段都标有红色星号。

SSH监控项需要特定信息的字段：

|参数|说明|备注|
|---------|-----------|--------|
|*类型*|在此处选择**SSH代理**。|<|
|*Key*|唯一（每个主机）项目密钥，格式为 **ssh.run\[<unique short description>,<ip>,<port>,<encoding>\]**|<unique short description> 是必需的并且对于每个主机的所有 SSH 项目应该是唯一的<br>默认端口是 22，而不是在分配此项目的接口中指定的端口|
|*认证方式*|“密码”或“公钥”之一|<|
|*用户名*|在远程主机上进行身份验证的用户名。<br>必填|<|
|*公钥文件*|如果 *Authentication method* 是“Public key”，则公钥的文件名。必需|示例：*id\_rsa.pub* - 命令生成的默认公钥文件名 [ssh-keygen](http://en.wikipedia.org/wiki/Ssh-keygen)|
|*私钥文件*|如果 *Authentication method* 是“Public key”，则私钥的文件名。必需|示例：*id\_rsa* - 默认私钥文件名|
|*Password* or<br>*Key passphrase*|验证密码或<br>Passphrase **如果**它用于私钥|如果没有使用密码，则将 *Key passphrase* 字段留空<br>另请参阅 [已知问题](/manual/installation/known_issues#ssh_checks) 关于密码短语的使用|
|*执行的脚本*|使用 SSH 远程会话执行的 shell 命令|示例：<br>*date +%s*<br>*service mysql-server status*<br>*ps auxww \| grep httpd \| wc-l*|

::: 注意重要
libssh2 库可能会将可执行脚本截断为
\~32kB。
:::

[comment]: # ({/5e2d1d6a-fe23daa8})
