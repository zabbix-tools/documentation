[comment]: # translation:outdated

[comment]: # ({d1f334e1-eb22f00a})
# HTTP 代理

[comment]: # ({/d1f334e1-eb22f00a})

[comment]: # ({b7cfc81f-a236c83a})
#### 概述

此监控项类型允许使用HTTP/HTTPS协议进行数据轮询。使用Zabbix sender或Zabbix sender协议也可以进行捕获。

HTTP监控项检查由Zabbix服务器执行。但是，当主机由Zabbix proxy监控时，HTTP项检查由proxy执行。

HTTP 监控项检查不需要任何 agent 运行在被监控的主机上。

HTTP agent同时支持HTTP和HTTPS。Zabbix可以选择跟随重定向（参考下文 *Follow redirects* 的选项）。最大重定向数硬编码为10（用cURL的参数 CURLOPT_MAXREDIRS）

了解何时使用HTTPS协议，另请参阅[已知问题](/manual/installation/known_issues#https_checks)

::: noteimportant
Zabbix server/proxy必须首先配置cURL(libcurl)支持。
:::

[comment]: # ({/b7cfc81f-a236c83a})

[comment]: # ({2641ef25-11697b04})

#### 配置

配置HTTP监控项：

-   进入:  *Configuration*  →  *Hosts*
-   在主机的那行点击 *Items*
-   点击  *Create item*
-   在表格中输入监控项的参数
- 
![](../../../../../assets/en/manual/config/items/itemtypes/http_item.png)

所有标有红色星号的为必填字段。

需要的HTTP监控项特定信息的字段是：

|   |   |
|---|---|
|*Type*|此处选择 **HTTP代理**。|
|*Key*|输入用于识别监控项唯一性的键.|
|*URL*| 连接和检索数据的URL. 例如: <br>https://www.example.com<br>http://www.example.com/download<br>可以用Unicode字符指定域名。 在执行web场景步骤时，它们将自动转换为ASCII。*Parse* 可以使用Parse按钮将可选查询字段(比如?name=Admin&password=mypassword)与URL分离，将属性和值移动到查询字段中，以便自动URL编码. 限制在2048个字符。<br>支持的宏: {HOST.IP}, {HOST.CONN}, {HOST.DNS}, {HOST.HOST}, {HOST.NAME}, {ITEM.ID}, {ITEM.KEY}, {ITEM.KEY.ORIG}, 用户宏, 低级发现宏<br>  这是设置[CURLOPT_URL](https://curl.haxx.se/libcurl/c/CURLOPT_URL.html) cURL选项|
|*Query fields*|URL的变量 (参见上文).<br>指定为属性和值对。<br>值是自动的URL编码。 <br>从宏中解析值，然后自动编码url 支持的宏: {HOST.IP}, {HOST.CONN}, {HOST.DNS}, {HOST.HOST}, {HOST.NAME}, {ITEM.ID}, {ITEM.KEY},                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             {ITEM.KEY.ORIG}, 用户宏, 低级自动发现宏. <br>设置cURL选项 [CURLOPT_URL](https://curl.haxx.se/libcurl/c/CURLOPT_URL.html)。|
|*Request type*|选择请求的类型：*GET*, *POST*, *PUT* 或 *HEAD* |
|*Timeout*| Zabbix不会花超过设定的时间来处理URL (1~60秒)。实际上，这个参数定义了连接URL的最大时间和执行HTTP请求的最大时间。 因此，Zabbix不会在一次检查中花费超过2倍的超时时间。  <br> 支持时间后缀, 例如 30s, 1m <br>支持的宏: 用户宏, 低级发现宏。  <br> 设置cURL选项 [CURLOPT_TIMEOUT](https://curl.haxx.se/libcurl/c/CURLOPT_TIMEOUT.html)|
|*Request body type*|设定请求体的类型<br>**Raw data** - 自定义HTTP请求体，替换宏，但不执行编码。<br>**JSON data** - HTTP请求体是JSON格式的， 宏可以用作字符串、数字、真和假;用作字符串的宏必须包含在双引号中。从宏中解析值，然后自动转义。 如果没有指定header，那么服务器将把默认的header值设置为"Content-Type: application/json"<br>**XML data** - HTTP请求体的XML格式。 宏可以用作文本节点、属性或CDATA部分。 从宏中解析值，然后在文本节点和属性中自动转义。 如果没有指定header  "Content-Type" 的值，那么服务器将把默认的header值设置为 "Content-Type: application/xml"  <br> *注意* 选择 *XML data* ， 需要libxml2的支持。|
|*Request body*|输入请求体<br>支持的宏: {HOST.IP}, {HOST.CONN}, {HOST.DNS}, {HOST.HOST}, {HOST.NAME}, {ITEM.ID}, {ITEM.KEY}, {ITEM.KEY.ORIG}, 用户宏, 低级自动发现宏。|
|*Headers*|执行请求时将发送的自定义HTTP头。 <br> 指定为属性和值对。<br> 支持的宏: {HOST.IP}, {HOST.CONN}, {HOST.DNS}, {HOST.HOST}, {HOST.NAME}, {ITEM.ID}, {ITEM.KEY}, {ITEM.KEY.ORIG}, 用户宏, 低级自动发现宏。<br>设置 [CURLOPT_HTTPHEADER](https://curl.haxx.se/libcurl/c/CURLOPT_HTTPHEADER.html) cURL 选项.|
|*Required status codes*|期望的HTTP状态码的列表。 如果Zabbix得到不在列表中的代码，那么这个项目简称为不受支持状态。如果为空，则不执行检查。<br> 例如: 200,201,210-299  <br> 列表里支持的宏: 用户宏, 低级自动发现宏。<br> 这使用了 [CURLINFO_RESPONSE_CODE](https://curl.haxx.se/libcurl/c/CURLINFO_RESPONSE_CODE.html) cURL选项。|
|*Follow redirects*|标记单选框使监控项跟随HTTP重定向。<br> 这使用了 [CURLOPT\_FOLLOWLOCATION](https://curl.haxx.se/libcurl/c/CURLOPT_FOLLOWLOCATION.html) cURL选项。|
|*Retrieve mode*|选择响应中必须检索的部分: <br> **Body** - 仅主体<br> **Headers** - 仅头部  <br> **Body and headers** - 主体和头部|
|*Convert to JSON*|头文件作为属性和值对保存在"header" 键下。<br> 如果遇到 'Content-Type: application/json' 主体被保存为对象，否则它被存储为string, 例如:  <br>![](../../../../../assets/en/manual/config/items/itemtypes/http_conv_json.png)|
|*HTTP proxy*|可以使用格式`[protocol://][username[:password]@]proxy.mycompany.com[:port]`指定要使用的HTTP代理。  <br> 其中 `protocol://` 部分作为协议前缀可以用于指定代理协议 (例如 https, socks4, socks5; 查看 [文档](https://curl.haxx.se/libcurl/c/CURLOPT_PROXY.html); 协议前缀的支持被添加于cURL 7.21.7)。<br> 如果不指定代理协议，将会视为使用HTTP代理。 如果您指定了错误的协议，那么连接将失败且监控项将不受支持。 由于没有指定协议，代理将被视为HTTP代理。<br>默认将使用1080端口。  <br> 如果指定本参数，代理信息将覆盖与代理相关的环境变量，如http_proxy\HTTPS_proxy等。<br> 如果本参数未指定，代理信息将不会覆盖与代理相关的环境变量。相关的值将会以“原样”传入，不会做健全性检查。<br> *注意* HTTP代理只支持简单的身份验证。  <br> 支持的宏: {HOST.IP}, {HOST.CONN}, {HOST.DNS}, {HOST.HOST}, {HOST.NAME}, {ITEM.ID}, {ITEM.KEY}, {ITEM.KEY.ORIG}, 用户宏, 低级自动发现宏。<br> 设置 [CURLOPT_PROXY](https://curl.haxx.se/libcurl/c/CURLOPT_PROXY.html) cURL 选项。|
|*HTTP authentication*|验证类型:  <br> **None** - 不使用身份验证。<br> **Basic authentication** - 使用基本身份验证。<br> **NTLM authentication** - 使用NTLM ([Windows NT LAN Manager)](http://en.wikipedia.org/wiki/NTLM) 验证。 <br> **Kerberos** - 使用Kerberos验证。详见 [为Zabbix配置Kerberos](/manual/appendix/items/kerberos)<br>  **Digest** - 使用Digest验证。<br> 选择身份验证方法后，展示输入用户名和密码提供两个额外字段的输入框，这里支持使用用户宏和低级发现宏。 <br> 设置 [CURLOPT_HTTPAUTH](https://curl.haxx.se/libcurl/c/CURLOPT_HTTPAUTH.html) cURL 选项。|
|*SSL verify peer*|勾选复选框以开启配置需要验证web服务器的ssl证书。 <br>服务器证书将自动从系统范围的证书颁发机构(CA)位置获取。 可以使用Zabbix服务器或代理配置参数SSLCALocation重写CA文件的位置。<br> 这设置的是cURL 选项 [CURLOPT\_SSL\_VERIFYPEER](http://curl.haxx.se/libcurl/c/CURLOPT_SSL_VERIFYPEER.html) 。|
|*SSL verify host*|勾选复选框以开启验证web服务器证书的通用名称字段或主题备用名称字段是否匹配。<br> 这设置的是cURL 选项[CURLOPT\_SSL\_VERIFYHOST](http://curl.haxx.se/libcurl/c/CURLOPT_SSL_VERIFYHOST.html) 。|
|*SSL certificate file** | 用于客户端认证的SSL证书文件名称。这个证书文件必须是PEM^1^格式的。<br> 如果证书文件内包含私钥，则SSL key file 字段请留空。如果密钥是加密的，需要设置SSL key password 字段配置加密密钥的密码。检索文件的目录，需要在Zabbix Server或Zabbix Proxy的配置文件中指定SSLCertLocation参数。<br> 支持的宏: {HOST.IP}, {HOST.CONN}, {HOST.DNS}, {HOST.HOST}, {HOST.NAME}, {ITEM.ID}, {ITEM.KEY}, {ITEM.KEY.ORIG}, 用户宏，低级自动发现宏。<br>这配置的是cURL 选项[CURLOPT\_SSLCERT](http://curl.haxx.se/libcurl/c/CURLOPT_SSLCERT.html) 。|
|*SSL key file*| 用于客户端认证的SSL私钥文件名称。这个证书文件必须是PEM^1^格式的。检索文件的目录，需要在Zabbix Server或Zabbix Proxy的配置文件中指定SSLCertLocation参数。<br> 支持的宏: {HOST.IP}, {HOST.CONN}, {HOST.DNS}, {HOST.HOST}, {HOST.NAME}, {ITEM.ID}, {ITEM.KEY}, {ITEM.KEY.ORIG}, 用户宏，低级自动发现宏。<br>这配置的是cURL 选项 [CURLOPT\_SSLKEY](http://curl.haxx.se/libcurl/c/CURLOPT_SSLKEY.html) 。|
|*SSL key password*|SSL私钥文件密码。<br>支持的宏: 用户宏，低级自动发现宏。<br>这配置的是cURL 选项 [CURLOPT\_KEYPASSWD](http://curl.haxx.se/libcurl/c/CURLOPT_KEYPASSWD.html) 。|
|*Enable trapping*|勾选此复选框，此监控项也可以被作为[trapper监控项](/manual/config/items/itemtypes/trapper) ，也就是可以接受数据从Zabbix sender工具发送或使用Zabbix sender协议上报的数据。|
|*Allowed hosts*|仅在勾选*Enable trapping* 复选框后可见。<br> 以逗号分隔的IP地址列表，也可使用CIDR表示法或主机名<br>如果本字段被指定，只有来自被列出的主机的入站连接才会被接受。<br> 如果IPv6支持被打开 '127.0.0.1', '::127.0.0.1', '::ffff:127.0.0.1' 会被等同处理，同时 '::/0' 允许任何IPv4或IPv6地址。<br> '0.0.0.0/0' 可以被用来允许任意IPv4地址<br> 注意，IPv4兼容的IPv6地址(0000::/96 prefix) 根据[RFC4291](https://tools.ietf.org/html/rfc4291#section-2.5.5)已经被废弃。<br> 示例：<br> Server=127.0.0.1, 192.168.1.0/24, 192.168.3.1-255, 192.168.1-10.1-255, ::1,2001:db8::/32, zabbix.domain<br> 空格和[用户宏](/manual/config/macros/user_macros)在此字段可以使用。<br> 下列主机宏可以用于此字段: {HOST.HOST}, {HOST.NAME}, {HOST.IP}, {HOST.DNS}, {HOST.CONN} 。|

::: notetip
如果 *HTTP proxy* 字段留空，环境变量中设定的其他HTTP代理相关设置将被应用

对于HTTP访问 - 为Zabbix server 进程用户设定 `http_proxy` 环境变量。如：\
`http_proxy=http://proxy_ip:proxy_port`。

对与 HTTPS访问 - 设定环境变量 `HTTPS_PROXY` 。如：\
`HTTPS_PROXY=http://proxy_ip:proxy_port`。 更多参考信息请运行shell 命令：  *\# man curl* 。
:::

::: noteimportant
 \[1\] Zabbix 仅支持PEM格式的证书与私钥文件。如果你拥有的是PKCS\#12格式的密钥数据文件（通常使用扩展名\*.p12 或 \*.pfx）你需要使用下列命令生成PEM格式文件：

    openssl pkcs12 -in ssl-cert.p12 -clcerts -nokeys -out ssl-cert.pem
    openssl pkcs12 -in ssl-cert.p12 -nocerts -nodes  -out ssl-cert.key

:::

[comment]: # ({/2641ef25-11697b04})

[comment]: # ({6863fa4f-4c860844})
#### 示例

[comment]: # ({/6863fa4f-4c860844})

[comment]: # ({69fc2595-67d717da})

##### 示例 1

发送一个简单GET请求，以从其他服务获取数据。案例为Elasticsearch：

-   创建一个从URL `localhost:9200/?pretty` GET 的监控项
-   注意获得的相应内容：

```{=html}
<!-- -->
```
    {
      "name" : "YQ2VAY-",
      "cluster_name" : "elasticsearch",
      "cluster_uuid" : "kH4CYqh5QfqgeTsjh2F9zg",
      "version" : {
        "number" : "6.1.3",
        "build_hash" : "af51318",
        "build_date" : "2018-01-26T18:22:55.523Z",
        "build_snapshot" : false,
        "lucene_version" : "7.1.0",
        "minimum_wire_compatibility_version" : "5.6.0",
        "minimum_index_compatibility_version" : "5.0.0"
      },
      "tagline" : "You know, for search"
    }

-  现在可以使用JSONPath 预处理步骤获取版本号信息： `$.version.number` 


[comment]: # ({/69fc2595-67d717da})

[comment]: # ({a1988df5-712fb81c})

##### 示例 2

发送一个简单POST请求，从其他服务抓取数据。以Elasticsearch为例：

-   创建一个POST监控项使用此URL:    `http://localhost:9200/str/values/_search?scroll=10s`
-   配置以下的POST请求体获取处理器负载（1分钟平均值/核心）

```{=html}
<!-- -->
```
    {
        "query": {
            "bool": {
                "must": [{
                    "match": {
                        "itemid": 28275
                    }
                }],
                "filter": [{
                    "range": {
                        "clock": {
                            "gt": 1517565836,
                            "lte": 1517566137
                        }
                    }
                }]
            }
        }
    }

-   得到响应:

```{=html}
<!-- -->
```
    {
        "_scroll_id": "DnF1ZXJ5VGhlbkZldGNoBQAAAAAAAAAkFllRMlZBWS1UU1pxTmdEeGVwQjRBTFEAAAAAAAAAJRZZUTJWQVktVFNacU5nRHhlcEI0QUxRAAAAAAAAACYWWVEyVkFZLVRTWnFOZ0R4ZXBCNEFMUQAAAAAAAAAnFllRMlZBWS1UU1pxTmdEeGVwQjRBTFEAAAAAAAAAKBZZUTJWQVktVFNacU5nRHhlcEI0QUxR",
        "took": 18,
        "timed_out": false,
        "_shards": {
            "total": 5,
            "successful": 5,
            "skipped": 0,
            "failed": 0
        },
        "hits": {
            "total": 1,
            "max_score": 1.0,
            "hits": [{
                "_index": "dbl",
                "_type": "values",
                "_id": "dqX9VWEBV6sEKSMyk6sw",
                "_score": 1.0,
                "_source": {
                    "itemid": 28275,
                    "value": "0.138750",
                    "clock": 1517566136,
                    "ns": 25388713,
                    "ttl": 604800
                }
            }]
        }
    }

-   现在可以使用JSONPath 预处理步骤获取监控项的值： `$.hits.hits[0]._source.value`  


[comment]: # ({/a1988df5-712fb81c})

[comment]: # ({4df23b9e-f480040f})
##### 示例 3

检查Zabbix API是否可用，可以用[apiinfo.version](/manual/api/reference/apiinfo/version) 这个接口

-   监控项配置:

![](../../../../../assets/en/manual/config/items/itemtypes/example3_a.png)

注意使用POST方法传输JSON数据，设定请求头并仅要求检索头信息：

-   配置监控项值的预处理功能，使用正则表达式方法获取HTTP响应代码：

![](../../../../../assets/en/manual/config/items/itemtypes/example3_b.png)

-   在 *最新数据* 中检查最新获取的数据：

![](../../../../../assets/en/manual/config/items/itemtypes/example3_c.png){width="600"}

[comment]: # ({/4df23b9e-f480040f})



[comment]: # ({4b39e73e-466eee2b})

##### 示例 4

通过连接到 Openweathermap 检索天气信息公共服务。

-   配置主要监控项将大量数据收集到一个简单JSON对象：

![](../../../../../assets/en/manual/config/items/itemtypes/example4_a.png)

注意在query的字段中使用宏。参考 [Openweathermap API文档](https://openweathermap.org/current) 来了解如何填写它们。

一个简单JSON对象会被返回给HTTPS agent：

``` {.json}
{
    "body": {
        "coord": {
            "lon": 40.01,
            "lat": 56.11
        },
        "weather": [{
            "id": 801,
            "main": "Clouds",
            "description": "few clouds",
            "icon": "02n"
        }],
        "base": "stations",
        "main": {
            "temp": 15.14,
            "pressure": 1012.6,
            "humidity": 66,
            "temp_min": 15.14,
            "temp_max": 15.14,
            "sea_level": 1030.91,
            "grnd_level": 1012.6
        },
        "wind": {
            "speed": 1.86,
            "deg": 246.001
        },
        "clouds": {
            "all": 20
        },
        "dt": 1526509427,
        "sys": {
            "message": 0.0035,
            "country": "RU",
            "sunrise": 1526432608,
            "sunset": 1526491828
        },
        "id": 487837,
        "name": "Stavrovo",
        "cod": 200
    }
}
```

下一个任务是配置从属监控项从这个JSON对象中导出必要的数据。

-   配置一个简单的从属监控项获取湿度数据：

![](../../../../../assets/en/manual/config/items/itemtypes/example4_b.png)

其他天气指标，例如 “温度” 可以以相同方式添加。

-  使用JSONPath来配置简单的从属监控项的值预处理步骤：

![](../../../../../assets/en/manual/config/items/itemtypes/example4_c.png)

-   在 *最新数据* 中检查天气数据的结果： 

![](../../../../../assets/en/manual/config/items/itemtypes/example4_d.png){width="600"}

[comment]: # ({/4b39e73e-466eee2b})

[comment]: # ({21958607-44596b14})

##### 示例 5

链接Nginx状态也，批量获取它的指标。

-   根据 [官方指南](https://nginx.ru/en/docs/http/ngx_http_stub_status_module.html)配置NGINX。

```{=html}
<!-- -->
```
-  配置一个主要监控项批量收集数据: 

![](../../../../../assets/en/manual/config/items/itemtypes/example5_a.png)

简单的Nginx状态也输出如下：

    Active connections: 1 Active connections:
    server accepts handled requests
     52 52 52 
    Reading: 0 Writing: 1 Waiting: 0

下一个任务是任务是配置从属监控项提取数据。

-   配置简单从属监控项名以采集每秒请求数数据：

![](../../../../../assets/en/manual/config/items/itemtypes/example5_b.png)

-   这个简单从属监控项，需要配置正则表达式预处理步骤
    `server accepts handled requests\s+([0-9]+) ([0-9]+) ([0-9]+)`:

![](../../../../../assets/en/manual/config/items/itemtypes/example5_c.png){width="600"}

-   对所有数据都配置好了从属监控项后，在 *最新数据* 中，可以查看到来自stub模块的完整结果

![](../../../../../assets/en/manual/config/items/itemtypes/example5_d.png){width="600"}

[comment]: # ({/21958607-44596b14})
