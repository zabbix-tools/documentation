[comment]: # translation:outdated

[comment]: # ({3a4cbf2a-68c88a1c})
#14 ODBC监控

[comment]: # ({/3a4cbf2a-68c88a1c})

[comment]: # ({73ab8fa3-b5170fd3})
#### 概述

ODBC监控对应于Zabbix前端中的 *数据库监控* 监控项类型.

ODBC是C语言编写的中间件API,用于访问数据库管理系统(DBMS).
ODBC是由Microsoft开发的,后来被移植到了其它平台.

Zabbix可以查询任何支持ODBC的数据库.为此,Zabbix不直接连接数据库,而是使用ODBC接口和在ODBC中设置的驱动程序.该功能允许出于多种目的,更加有效地监视不同的数据库.例如,检测特定的数据库队列、使用统计信息等.Zabbix支持unixODBC,是最常用的开源ODBCAPI实现之一.

[comment]: # ({/73ab8fa3-b5170fd3})

[comment]: # ({f3a44835-bf6fd8ba})
#### 安装 unixODBC

安装unixODBC的建议方法是使用Linux操作系统默认包库,在主流的Linux发行版中，默认情况下unixODBC包含在镜像库中
如果找不到,可以在 unixODBC 主页上获取：
<http://www.unixodbc.org/download.html>.

在RedHat/Fedora系统上使用*yum*包管理器安装unixODBC:

 · shell> yum -y install unixODBC unixODBC-devel

使用*zypper*包管理器在基于SUSE的系统上安装unixODBC:

 · # zypper in unixODBC-devel

::: 
noteclassic 需要编译unixODBC-devel包来支持unixODBC的Zabbix.
:::

[comment]: # ({/f3a44835-bf6fd8ba})

[comment]: # ({3d546a4c-eea09ed7})
#### 安装 unixODBC 驱动程序

应该为数据库安装一个unixODBC数据库驱动程序,它将被监控.
unixODBC有一个支持的数据库和驱动程序列表:<http://www.unixodbc.org/drivers.html>.
在一些Linux发行版中,数据库驱动程序包含在镜像库中.
在RedHat/Fedora系统上使用*yum*包管理器安装MySQL数据库驱动程序:

    shell> yum install mysql-connector-odbc

使用*zypper*包管理器在SUSE系统上安装MySQL数据库驱动程序:

    zypper in MyODBC-unixODBC

[comment]: # ({/3d546a4c-eea09ed7})

[comment]: # ({b76ebb2a-b15e771b})
#### 配置 unixODBC

ODBC配置是通过编辑** odbcinstn.ini**和** odbcst.ini**文件完成的,要验证配置文件的位置,输入:

    shell> odbcinst 

**odbcinstst .ini**用于列出已安装的ODBC数据库驱动程序:

[mysql]
Description = ODBC for MySQL
Driver      = /usr/lib/libmyodbc5.so


参数详情：

|属性|描述|
|---------|------------|
|*mysql*|数据库驱动名|
|*描述*|数据库驱动描述|
|*驱动程序*|数据库驱动程序库位置|

**odbc.ini** 用于自定义数据源：

[test]
Description = MySQL test database
Driver      = mysql
Server      = 127.0.0.1
User        = root
Password    =
Port        = 3306
Database    = zabbix

参数详情：

|属性|描述|
|---------|------------|
|*test*|数据源名称 (DSN)|
|*描述*|数据源描述|
|*Driver*|数据库驱动程序名称 - 在 odbcinst.ini 中指定|
|*服务器*|数据库服务器 IP/DNS|
|*用户*|用于连接的数据库用户|
|*密码*|数据库用户密码|
|*端口*|数据库连接端口|
|*数据库*|数据库名称|

要验证ODBC连接是否正常工作,应该测试到数据库的连接,这可以通过isql实用程序(包含在unixODBC包中)完成:

shell> isql test
+---------------------------------------+
| Connected!                            |
|                                       |
| sql-statement                         |
| help [tablename]                      |
| quit                                  |
|                                       |
+---------------------------------------+
SQL>

[comment]: # ({/b76ebb2a-b15e771b})

[comment]: # ({20d3cea2-9f87c9b6})
#### 编译支持ODBC的Zabbix

要启用ODBC支持，Zabbix应该使用以下标志进行编译：

      --with-unixodbc[=ARG]   use odbc driver against unixODBC package

::: 
noteclassic
从[源代码](/manual/installation/install#from_the_sources)中了解有关Zabbix安装的更多信息.
:::

[comment]: # ({/20d3cea2-9f87c9b6})

[comment]: # ({77ca88db-ca784f43})
#### Zabbix 前端的配置监控项

配置数据库[监控项]（/manual/config/items/item#overview）.

![](../../../../../assets/en/manual/config/items/itemtypes/db_monitor.png)

所有必填字段都标有红色星号

特别是对于数据库监控项,您必须输入：

| | |
|---|---|
|*类型*|在此处选择*数据库监视器*|
|*Key*|输入两个受支持的项目键之一：<br>**db.odbc.select**\[<unique short description>,<dsn>,<connection string>\] - 此项目旨在返回一个值,即SQL查询结果第一行的第一列如果查询返回多列,则仅读取第一列如果查询返回多行,则仅读取第一行<br>**db.odbc.get**\[<unique short description>,<dsn>,<connection string>\] - 此项为能够以 JSON 格式返回多行/列因此,它可以用作在一个系统调用中收集所有数据的主项,而 JSONPath 预处理可以用于从属项中以提取单个值有关详细信息,请参阅低级发现中使用的返回格式的 [示例](/manual/discovery/low_level_discovery/examples/sql_queries#using_dbodbcget)从 Zabbix 4.4 开始支持此项<br>唯一的描述将用于在触发器等中标识该项<br>虽然 `dsn` 和 `connection string` 是可选参数,但至少应该存在其中一个如果同时定义了数据源名称 (DSN) 和连接字符串,则 DSN 将被忽略<br>如果使用数据源名称,则必须按照 odbc.ini 中的指定设置<br>连接字符串可能包含驱动程序- 特定参数<br><br>示例（MySQL ODBC 驱动程序 5 的连接）：<br>=> db.odbc.get\[MySQL 示例,,"Driver=/usr/local/lib/libmyodbc5a.so;数据库=主服务器；服务器=127.0.0.1；端口=3306"\]|
|*用户名*|输入数据库用户名<br>如果在odbc.ini中指定了用户,则此参数是可选的<br>如果使用连接字符串,并且*用户名*字段不为空,则附加到连接字符串为 `UID=<user>`|
|*Password*|输入数据库用户密码<br>如果在odbc.ini中指定了密码,则此参数是可选的<br>如果使用连接字符串,并且*Password*字段不为空,则附加到连接中字符串为`PWD=<密码>`|
|*SQL 查询*|输入 SQL 查询<br>请注意,对于 `db.odbc.select[]` 项,查询必须只返回一个值|
|*信息类型*|了解查询将返回什么类型的信息很重要,这样才能在此处正确选择*信息类型*,如果信息类型不正确,项目将不受支持|

[comment]: # ({/77ca88db-ca784f43})

[comment]: # ({new-0188aaa2})
#### Item key details

Parameters without angle brackets are mandatory. Parameters marked with angle brackets **<** **>** are optional.

[comment]: # ({/new-0188aaa2})

[comment]: # ({new-13edfcd4})

##### db.odbc.select[<unique short description>,<dsn>,<connection string>] {#db.odbc.select}

<br>
Returns one value, i.e. the first column of the first row of the SQL query result.
Return value: depending on the SQL query.

Parameters:

-   **unique short description** - a unique short description to identify the item (for use in triggers, etc);
-   **dsn** - the data source name (as specified in odbc.ini);
-   **connection string** - the connection string (may contain driver-specific arguments).

Comments:

-   Although `dsn` and `connection string` are optional parameters, at least one of them must be present. If both data source name (DSN) and connection string are defined, the DSN will be ignored.
-   If a query returns more than one column, only the first column is read. If a query returns more than one line, only the first line is read.

[comment]: # ({/new-13edfcd4})

[comment]: # ({new-ebf5d449})

##### db.odbc.get[<unique short description>,<dsn>,<connection string>] {#db.odbc.get}

<br>
Transforms the SQL query result into a JSON array.<br>
Return value: *JSON object*.

Parameters:

-   **unique short description** - a unique short description to identify the item (for use in triggers, etc);
-   **dsn** - the data source name (as specified in odbc.ini);
-   **connection string** - the connection string (may contain driver-specific arguments).

Comments:

-   Although `dsn` and `connection string` are optional parameters, at least one of them must be present. If both data source name (DSN) and connection string are defined, the DSN will be ignored.
-   Multiple rows/columns in JSON format may be returned. This item may be used as a master item that collects all data in one system call, while JSONPath preprocessing may be used in dependent items to extract individual values. For more information, see an [example](/manual/discovery/low_level_discovery/examples/sql_queries#using-db.odbc.get) of the returned format, used in low-level discovery.

Example:

    db.odbc.get[MySQL example,,"Driver=/usr/local/lib/libmyodbc5a.so;Database=master;Server=127.0.0.1;Port=3306"] #connection for MySQL ODBC driver 5

[comment]: # ({/new-ebf5d449})

[comment]: # ({new-c8148c95})

##### db.odbc.discovery[<unique short description>,<dsn>,<connection string>] {#db.odbc.discovery}

<br>
Transforms the SQL query result into a JSON array, used for [low-level disovery](/manual/discovery/low_level_discovery/examples/sql_queries). 
The column names from the query result are turned into low-level discovery macro 
names paired with the discovered field values. These macros can be used in creating 
item, trigger, etc prototypes.<br>
Return value: *JSON object*.

Parameters:

-   **unique short description** - a unique short description to identify the item (for use in triggers, etc);
-   **dsn** - the data source name (as specified in odbc.ini);
-   **connection string** - the connection string (may contain driver-specific arguments).

Comments:

-   Although `dsn` and `connection string` are optional parameters, at least one of them must be present. If both data source name (DSN) and connection string are defined, the DSN will be ignored.

[comment]: # ({/new-c8148c95})

[comment]: # ({7eb764b1-06f9d2eb})
#### 注意事项

-   Zabbix不限制查询执行时间. 用户可以选择在合理时间内执行的查询。
-   Zabbix server的 [Timeout](/manual/appendix/config/zabbix_server)
    参数值也用作于ODBC登陆超时时间
    (请注意，根据ODBC驱动，登录超时设置可能会被忽略)。
-   查询只能返回一个值。
-   如果查询返回多个列，则只读取第一列。
-   如果查询返回多行，则只读取第一行。
-   SQL命令必须以`select`开头。
-   SQL命令不能包含任何换行符。
-   另请参阅ODBC检查的
    [已知问题](/manual/installation/known_issues#odbc_checks)

[comment]: # ({/7eb764b1-06f9d2eb})

[comment]: # ({fa377ba9-718edfdc})
#### 错误信息

ODBC错误消息被构造成字段,以提供详细信息.例如：

    Cannot execute ODBC query: [SQL_ERROR]:[42601][7][ERROR: syntax error at or near ";"; Error while executing the query]
    └───────────┬───────────┘  └────┬────┘ └──┬──┘└┬┘└─────────────────────────────┬─────────────────────────────────────┘
                │                   │         │    └─ Native error code            └─ Native error message
                │                   │         └─ SQLState
                └─ Zabbix message   └─ ODBC return code

请注意，错误消息长度限制为2048字节,因此信息可以被截断.如果有多个ODBC诊断记录,只要长度限制允许,Zabbix将尝试把它们连接起来（用“|”分隔）。

[comment]: # ({/fa377ba9-718edfdc})
