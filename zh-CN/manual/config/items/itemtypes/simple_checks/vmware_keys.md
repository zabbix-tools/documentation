[comment]: # translation:outdated

[comment]: # ({new-5479dd79})
# 1 VMware监控项

[comment]: # ({/new-5479dd79})

[comment]: # ({new-98337d09})
#### 监控项键值

该表提供了用于监控[VMware环境](/manual/vm_monitoring)的简单检查的详细说明。

|键值|<|<|<|<|
|------|-|-|-|-|
|<|描述                                                                                                             返|值                                  参数|注解|<|
|vmware.cluster.discovery\[<url>\]|<|<|<|<|
|<|发现VMware集群.                                                                                                  JSO|对象                                **url|* - VMware服务的URL地址|<|
|vmware.cluster.status\[<url>, <name>\]|<|<|<|<|
|<|VMware集群状态.                                                                                                  整型:|**url0 - 灰色;\                              *1 - 绿色;<br>2 - 黄色;<br>3 - 红色|* - VMware服务的URL地址<br>name** - VMware集群名称|<|
|vmware.eventlog\[<url>\]|<|<|<|<|
|<|VMware 事件日志.                                                                                                 Log|**u|l** - VMware服务的URL地址|<|
|vmware.fullname\[<url>\]|<|<|<|<|
|<|VMware服务全名.                                                                                                  字符串|**url*|- VMware服务的URL地址|<|
|vmware.hv.cluster.name\[<url>,<uuid>\]|<|<|<|<|
|<|VMware管理层集群名.                                                                                              字符串|**url**|VMware服务的URL地址<br>**uuid** - VMware虚拟机管理程序主机名|<|
|vmware.hv.cpu.usage\[<url>,<uuid>\]|<|<|<|<|
|<|VMware虚拟机管理程序处理器使用情况 (Hz).                                                                         整型|**url** - VMwar|服务的URL地址<br>**uuid** - VMware虚拟机管理程序主机名|<|
|vmware.hv.datacenter.name\[<url>,<uuid>\]|<|<|<|<|
|<|VMware虚拟管理器数据中心名称.                                                                                    字符串|**url** - VMw|re服务的URL地址<br>**uuid** - VMware虚拟机管理程序主机名|<|
|vmware.hv.datastore.discovery\[<url>,<uuid>\]|<|<|<|<|
|<|发现VMware虚拟管理程序数据存储.                                                                                  JSON对象|**url** - VMw|re服务的URL地址<br>**uuid** - VMware虚拟机管理程序主机名|<|
|vmware.hv.datastore.read\[<url>,<uuid>,<datastore>,<mode>\]|<|<|<|<|
|<|从数据存储读取操作的平均时间 (毫秒).                                                                             整型 ^**[2](vmwar|_keys#footnotes)**^   **url** - VMware服|的URL地址<br>**uuid** - VMware虚拟机管理程序主机名<br>**datastore** - 数据存储名称<br>**mode** - 延迟 (默认)|<|
|vmware.hv.datastore.size\[<url>,<uuid>,<datastore>,<mode>\]|<|<|<|<|
|<|VMware数据存储空间（字节为单位）或占总数的百分比.                                                                整型 - 字节数<br>|**url** - VMware服务的URL地址<br>浮点数 - 百分比                         **uui|从Zabbix 3.0.6, 3.2.2以后可用** - VMware虚拟机管理程序主机名<br>**datastore** - 数据存储名称<br>**mode** - 可能的值:<br>total (默认), free, pfree (剩余百分比), uncommitted|<|
|vmware.hv.datastore.write\[<url>,<uuid>,<datastore>,<mode>\]|<|<|<|<|
|<|对数据存储区进行写操作的平均时间 (毫秒).                                                                         整型 ^**[2](vmware_|eys#footnotes)**^   **url** - VMware服务的|RL地址<br>**uuid** - VMware虚拟机管理程序主机名<br>**datastore** - 数据存储名称<br>**mode** - 延迟 (默认)|<|
|vmware.hv.discovery\[<url>\]|<|<|<|<|
|<|发现VMware虚拟机管理程序.                                                                                        JSON对象|**url** -|Mware服务的URL地址|<|
|vmware.hv.fullname\[<url>,<uuid>\]|<|<|<|<|
|<|VMware虚拟机管理程序名称.                                                                                        字符串|**url** - V|ware服务的URL地址<br>**uuid** - VMware虚拟机管理程序主机名|<|
|vmware.hv.hw.cpu.freq\[<url>,<uuid>\]|<|<|<|<|
|<|VMware虚拟机管理程序处理器频率 (Hz).                                                                             整型|**url** - VMw|re服务的URL地址<br>**uuid** - VMware虚拟机管理程序主机名|<|
|vmware.hv.hw.cpu.model\[<url>,<uuid>\]|<|<|<|<|
|<|VMware虚拟机管理程序处理器模式.                                                                                  字符串|**url** - VMwa|e服务的URL地址<br>**uuid** - VMware虚拟机管理程序主机名|<|
|vmware.hv.hw.cpu.num\[<url>,<uuid>\]|<|<|<|<|
|<|VMware虚拟机管理程序上处理器的内核数.                                                                            整型|**url** - VMware|务的URL地址<br>**uuid** - VMware虚拟机管理程序主机名|<|
|vmware.hv.hw.cpu.threads\[<url>,<uuid>\]|<|<|<|<|
|<|VMware虚拟机管理程序上处理器的线程数.                                                                            整型|**url** - VMware|务的URL地址<br>**uuid** - VMware虚拟机管理程序主机名|<|
|vmware.hv.hw.memory\[<url>,<uuid>\]|<|<|<|<|
|<|VMware虚拟机管理程序总内存 (字节).                                                                               整型|**url** - VMw|re服务的URL地址<br>**uuid** - VMware虚拟机管理程序主机名|<|
|vmware.hv.hw.model\[<url>,<uuid>\]|<|<|<|<|
|<|VMware虚拟机管理程序模式.                                                                                        字符串|**url** - V|ware服务的URL地址<br>**uuid** - VMware虚拟机管理程序主机名|<|
|vmware.hv.hw.uuid\[<url>,<uuid>\]|<|<|<|<|
|<|VMware虚拟机管理程序BIOS UUID.                                                                                   字符串|**url** -|VMware服务的URL地址<br>**uuid** - VMware虚拟机管理程序主机名|<|
|vmware.hv.hw.vendor\[<url>,<uuid>\]|<|<|<|<|
|<|VMware虚拟机管理程序供应商名称.                                                                                  字符串|**url** - VMwa|e服务的URL地址<br>**uuid** - VMware虚拟机管理程序主机名|<|
|vmware.hv.memory.size.ballooned\[<url>,<uuid>\]|<|<|<|<|
|<|VMware虚拟机管理程序膨胀内存大小 (字节).                                                                         整型|**url** - VMware|务的URL地址<br>**uuid** - VMware虚拟机管理程序主机名|<|
|vmware.hv.memory.used\[<url>,<uuid>\]|<|<|<|<|
|<|VMware虚拟机管理程序内存使用大小 (字节).                                                                         整型|**url** - VMware|务的URL地址<br>**uuid** - VMware虚拟机管理程序主机名|<|
|vmware.hv.network.in\[<url>,<uuid>,<mode>\]|<|<|<|<|
|<|VMware虚拟机管理程序网络输入数据统计 (每秒字节数).                                                               整型 ^**[2](vmware_ke|s#footnotes)**^   **url** - VMware服务的UR|地址<br>**uuid** - VMware虚拟机管理程序主机名<br>**mode** - bps (默认)|<|
|vmware.hv.network.out\[<url>,<uuid>,<mode>\]|<|<|<|<|
|<|VMware虚拟机管理程序网络输出数据统计 (每秒字节数).                                                               整型 ^**[2](vmware_ke|s#footnotes)**^   **url** - VMware服务的UR|地址<br>**uuid** - VMware虚拟机管理程序主机名<br>**mode** - bps (默认)|<|
|vmware.hv.perfcounter\[<url>,<uuid>,<path>,<instance>\]|<|<|<|<|
|<|VMware虚拟机管理程序性能计数器值.                                                                                整型 ^**[2](vm|are_keys#footnotes)**^   **url** - VMwa|e服务的URL地址\                                从Zabbix 2.2.9, 2.4.**uuid** - VMware虚拟机管理程序主机名<br>**path** - 性能计数器路径 ^**[1](vmware_keys#footnotes)**^<br>**instance** - 性能计数器实例. 对聚合值使用空实例 (默认)|以后开始支持|
|vmware.hv.sensor.health.state\[<url>,<uuid>\]|<|<|<|<|
|<|VMware虚拟机管理程序健康状态汇总传感器.                                                                          整型:<br>|**url** - VMware服0 - 灰色;\                              *1 - 绿色;<br>2 - 黄色;<br>3 - 红色|的URL地址\                                从Zabbix 2.2.16, 3.0.6,uuid** - VMware虚拟机管理程序主机名|3.2.2以后开始支持|
|vmware.hv.status\[<url>,<uuid>\]|<|<|<|<|
|<|VMware虚拟机管理程序状态.                                                                                        整型:<br>|**url** -0 - 灰色;\                              *1 - 绿色;<br>2 - 黄色;<br>3 - 红色|Mware服务的URL地址\                                从Zabbix 2.2.16,uuid** - VMware虚拟机管理程序主机名|3.0.6, 3.2.2开始支持使用主机系统整体状态属性|
|vmware.hv.uptime\[<url>,<uuid>\]|<|<|<|<|
|<|VMware虚拟机管理程序运行时间 (秒).                                                                               整型|**url** - VMw|re服务的URL地址<br>**uuid** - VMware虚拟机管理程序主机名|<|
|vmware.hv.version\[<url>,<uuid>\]|<|<|<|<|
|<|VMware虚拟机管理程序版本.                                                                                        字符串|**url** - V|ware服务的URL地址<br>**uuid** - VMware虚拟机管理程序主机名|<|
|vmware.hv.vm.num\[<url>,<uuid>\]|<|<|<|<|
|<|VMware虚拟机管理程序上的虚拟主机数量.                                                                            整型|**url** - VMware|务的URL地址<br>**uuid** - VMware虚拟机管理程序主机名|<|
|vmware.version\[<url>\]|<|<|<|<|
|<|VMware服务版本.                                                                                                  字符串|**url*|- VMware服务的URL地址|<|
|vmware.vm.cluster.name\[<url>,<uuid>\]|<|<|<|<|
|<|VMware虚拟机名称.                                                                                                字符串|**url**|- VMware服务的URL地址<br>**uuid** - VMware虚拟机主机名|<|
|vmware.vm.cpu.num\[<url>,<uuid>\]|<|<|<|<|
|<|虚拟机上处理器的数量.                                                                                            整型|**url** - V|ware服务的URL地址<br>**uuid** - VMware虚拟机主机名|<|
|vmware.vm.cpu.ready\[<url>,<uuid>\]|<|<|<|<|
|<|虚拟机准备就绪，但不能在物理CPU上运行的时间(以毫秒为单位). CPU准备时间取决于主机上的虚拟机数量及其CPU负载 (%).   整型 ^**[2](vmware_keys#footnotes)**^   **url**|-VMware服务的URL地址<br>|从Zabbix version 3.0.0开始支持**uuid** - VMware虚拟机主机名|<|
|vmware.vm.cpu.usage\[<url>,<uuid>\]|<|<|<|<|
|<|VMware虚拟机cpu的使用率 (Hz).                                                                                    整型|**url**|VMware服务的URL地址<br>**uuid** -VMware虚拟机主机名|<|
|vmware.vm.datacenter.name\[<url>,<uuid>\]|<|<|<|<|
|<|VMware虚拟机数据中心名称.                                                                                        字符串|**url** - V|ware服务的URL地址<br>**uuid** - VMware虚拟机主机名|<|
|vmware.vm.discovery\[<url>\]|<|<|<|<|
|<|自动发现VMware虚拟机.                                                                                            JSON对象|**url**|VMware服务的URL地址|<|
|vmware.vm.hv.name\[<url>,<uuid>\]|<|<|<|<|
|<|VMware虚拟机管理程序名称.                                                                                        字符串|**url** - V|ware服务的URL地址<br>**uuid** - VMware虚拟机主机名|<|
|vmware.vm.memory.size\[<url>,<uuid>\]|<|<|<|<|
|<|VMware虚拟机总内存大小 (字节).                                                                                   整型|**url** - V|ware服务的URL地址<br>**uuid** - VMware虚拟机主机名|<|
|vmware.vm.memory.size.ballooned\[<url>,<uuid>\]|<|<|<|<|
|<|VMware虚拟机膨胀内存大小 (字节).                                                                                 整型|**url** - VM|are服务的URL地址<br>**uuid** - VMware虚拟机主机名|<|
|vmware.vm.memory.size.compressed\[<url>,<uuid>\]|<|<|<|<|
|<|VMware虚拟机压缩内存大小 (字节).                                                                                 整型|**url** - VM|are服务的URL地址<br>**uuid** - VMware虚拟机主机名|<|
|vmware.vm.memory.size.private\[<url>,<uuid>\]|<|<|<|<|
|<|VMware虚拟主机专用内存大小 (字节).                                                                               整型|**url** - VMw|re服务的URL地址<br>**uuid** - VMware虚拟机主机名|<|
|vmware.vm.memory.size.shared\[<url>,<uuid>\]|<|<|<|<|
|<|VMware虚拟机共享内存大小 (字节).                                                                                 整型|**url** - VM|are服务的URL地址<br>**uuid** - VMware虚拟机主机名|<|
|vmware.vm.memory.size.swapped\[<url>,<uuid>\]|<|<|<|<|
|<|VMware虚拟机交换内存大小 (字节).                                                                                 整型|**url** - VM|are服务的URL地址<br>**uuid** - VMware虚拟机主机名|<|
|vmware.vm.memory.size.usage.guest\[<url>,<uuid>\]|<|<|<|<|
|<|VMware虚拟机客户机内存使用量 (字节).                                                                             整型|**url** - VMwa|e服务的URL地址<br>**uuid** - VMware虚拟机主机名|<|
|vmware.vm.memory.size.usage.host\[<url>,<uuid>\]|<|<|<|<|
|<|VMware虚拟机主机内存使用量 (字节).                                                                               整型|**url** - VMw|re服务的URL地址<br>**uuid** - VMware虚拟机主机名|<|
|vmware.vm.net.if.discovery\[<url>,<uuid>\]|<|<|<|<|
|<|自动发现VMware虚拟机网络接口.                                                                                    JSON对象|**url** - VM|are服务的URL地址<br>**uuid** -VMware虚拟机主机名|<|
|vmware.vm.net.if.in\[<url>,<uuid>,<instance>,<mode>\]|<|<|<|<|
|<|VMware虚拟机网卡输入数据统计 (每秒字节/数据包).                                                                  整型 ^**[2](vmware_|eys#footnotes)**^   **url** - VMware服务的|RL地址<br>**uuid** - VMware虚拟机主机名<br>**instance** - 网卡实例<br>**mode** - bps (默认)/pps - 每秒字节/数据包|<|
|vmware.vm.net.if.out\[<url>,<uuid>,<instance>,<mode>\]|<|<|<|<|
|<|VMware虚拟机网卡输出数据统计 (每秒字节/数据包).                                                                  整型 ^**[2](vmware_|eys#footnotes)**^   **url** - VMware服务的|RL地址<br>**uuid** - VMware虚拟机主机名<br>**instance** - 网卡实例<br>**mode** - bps (默认)/pps - 每秒字节/数据包|<|
|vmware.vm.perfcounter\[<url>,<uuid>,<path>,<instance>\]|<|<|<|<|
|<|VMware虚拟机性能计数器值.                                                                                        整型 ^**[2|(vmware_keys#footnotes)**^   **url** -|Mware服务的URL地址\                                Available since**uuid** -VMware虚拟机主机名<br>**path** - 性能计数器路径 ^**[1](vmware_keys#footnotes)**^<br>**instance** - 性能计数器实例. 对聚合值使用空实例 (默认)|从Zabbix 2.2.9, 2.4.4开始支持|
|vmware.vm.powerstate\[<url>,<uuid>\]|<|<|<|<|
|<|VMware虚拟机电源状态.                                                                                            整型:<br>|**url**0 - 关闭;\                              *1 - 开机;<br>2 - 暂停|VMware服务的URL地址<br>uuid** - VMware虚拟机主机名|<|
|vmware.vm.storage.committed\[<url>,<uuid>\]|<|<|<|<|
|<|VMware虚拟机已提交存储空间 (字节).                                                                               整型|**url** - VMw|re服务的URL地址<br>**uuid** -VMware虚拟机主机名|<|
|vmware.vm.storage.uncommitted\[<url>,<uuid>\]|<|<|<|<|
|<|VMware虚拟机未提交存储空间 (字节).                                                                               整型|**url** - VMw|re服务的URL地址<br>**uuid** - VMware虚拟机主机名|<|
|vmware.vm.storage.unshared\[<url>,<uuid>\]|<|<|<|<|
|<|VMware虚拟机非共享存储空间 (字节).                                                                               整型|**url** - VMw|re服务的URL地址<br>**uuid** - VMware虚拟机主机名|<|
|vmware.vm.uptime\[<url>,<uuid>\]|<|<|<|<|
|<|VMware虚拟机运行时间 (秒).                                                                                       整数|**url** -|VMware服务的URL地址<br>**uuid** - VMware虚拟机主机名|<|
|vmware.vm.vfs.dev.discovery\[<url>,<uuid>\]|<|<|<|<|
|<|自动发现VMware虚拟机磁盘设备.                                                                                    JSON对象|**url** - VM|are服务的URL地址<br>**uuid** - VMware虚拟机主机名|<|
|vmware.vm.vfs.dev.read\[<url>,<uuid>,<instance>,<mode>\]|<|<|<|<|
|<|VMware虚拟机磁盘设备读取统计数据 (每秒字节/操作数).                                                              整型 ^**[2](vmware_ke|s#footnotes)**^   **url** - VMware服务的UR|地址<br>**uuid** - VMware虚拟机主机名<br>**instance** - 磁盘设备实例<br>**mode** - bps (默认)/ops - 每秒字节/操作数|<|
|vmware.vm.vfs.dev.write\[<url>,<uuid>,<instance>,<mode>\]|<|<|<|<|
|<|VMware虚拟机磁盘设备写入统计数据 (每秒字节/操作数).                                                              整型 ^**[2](vmware_ke|s#footnotes)**^   **url** - VMware服务的UR|地址<br>**uuid** - VMware虚拟机主机名<br>**instance** - 磁盘设备实例<br>**mode** - bps (默认)/ops - 每秒字节/操作数|<|
|vmware.vm.vfs.fs.discovery\[<url>,<uuid>\]|<|<|<|<|
|<|自动发现VMware虚拟机文件系统.                                                                                    JSON对象|**url** - VM|are服务的URL地址\                                VMware Tools必须安装在**uuid** - VMware虚拟机主机名|户虚拟机上.|
|vmware.vm.vfs.fs.size\[<url>,<uuid>,<fsname>,<mode>\]|<|<|<|<|
|<|VMware虚拟机系统文件统计信息 (字节/百分比).                                                                      整型|**url** - VMware服|的URL地址\                                VMware Tools必须安装在客户虚拟机**uuid** - VMware虚拟机主机名<br>**fsname** - 文件系统名<br>**mode** - total/free/used/pfree/pused|.|

[comment]: # ({/new-98337d09})


