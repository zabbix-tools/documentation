[comment]: # translation:outdated

[comment]: # ({ef5a738f-6fa4a853})
# 4 MSSQL数据库设推荐的UnixODBC设置

[comment]: # ({/ef5a738f-6fa4a853})

[comment]: # ({23f4b016-29c905c8})
#### 安装

-   **Red Hat Enterprise Linux/CentOS**:

```{=bash}
 # yum install freetds
```

-   **Debian/Ubuntu**:

请参阅[FreeTDS用户向导](http://www.freetds.org/userguide/) 来下载相应平台必要的数据库驱动。

如需其他相关信息，请参阅 [安装unixODBC](/manual/config/items/itemtypes/odbc_checks#installing-unixodbc)。

[comment]: # ({/23f4b016-29c905c8})

[comment]: # ({ece7b19c-aa4d8324})
#### Configuration

通过编辑 **odbcinst.ini** 和 **odbc.ini** 文件来完成ODBC的配置。这些配置文件可以在 */etc* 文件夹中找到。**odbcinst.ini** 文件可能不存在，这时我们需要手动来创建它。

请考虑以下  **odbc.ini**  配置参数的示例：

**odbcinst.ini**

    $ vi /etc/odbcinst.ini
    [FreeTDS]
    Driver = /usr/lib64/libtdsodbc.so.0

**odbc.ini**

    $ vi /etc/odbc.ini
    [sql1]
    Driver = FreeTDS
    Server = <SQL server 1 IP>
    PORT = 1433
    TDS_Version = 8.0

[comment]: # ({/ece7b19c-aa4d8324})
