[comment]: # translation:outdated

[comment]: # ({new-596937b2})
# 3 Oracle数据库推荐的UnixODBC设置

[comment]: # ({/new-596937b2})

[comment]: # ({83871470-4bfd360f})
#### 安装

请访问 [Oracle 文档](https://docs.oracle.com/database/121/ADFNS/adfns_odbc.htm)来获取详细说明。

如需其他相关信息，请参阅: [安装unixODBC](/manual/config/items/itemtypes/odbc_checks/).

[comment]: # ({/83871470-4bfd360f})
