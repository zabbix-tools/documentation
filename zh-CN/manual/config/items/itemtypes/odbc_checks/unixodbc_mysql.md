[comment]: # translation:outdated

[comment]: # ({d7db6371-e966b3a3})
# 1 MySQL推荐的UnixODBC设置

[comment]: # ({/d7db6371-e966b3a3})

[comment]: # ({f6728972-e5f46c02})
#### 安装

    *** 红帽企业 Linux/CentOS**：

    # yum 安装 mysql-connector-odbc

    ***Debian/Ubuntu**：

请参考[MySQL文档](https://dev.mysql.com/downloads/connector/odbc/)下载相应平台所需的数据库驱动程序.

有关其他一些信息,请参阅:[安装 unixODBC](/manual/config/items/itemtypes/odbc_checks/).

[comment]: # ({/f6728972-e5f46c02})

[comment]: # ({95073b6c-dcea6601})
### 配置

通过编辑**odbcinstn.ini**和**odbcst.ini**文件来完成ODBC配置.这些配置文件可以在*/etc*文件夹下找到.可能缺少**odbcinstn .ini**文件,在这种情况下,需要手动创建它.


**odbcinst.ini**

 · [mysql]
 · Description = General ODBC for MySQL
 · Driver · = /usr/lib64/libmyodbc5.so
 · Setup · = /usr/lib64/libodbcmyS.so 
 · FileUsage · = 1

请考虑以下 **odbc.ini** 配置示例参数。

- 通过 IP 连接的示例：

```{=html}
<!-- -->
```
 · [TEST_MYSQL] · 
 · Description = MySQL database 1 · 
 · Driver · = mysql · 
 · Port = 3306 · 
 · Server = 127.0.0.1

- 通过 IP 连接并使用证书.默认使用 Zabbix 数据库:

```{=html}
<!-- -->
```
 · [TEST_MYSQL_FILLED_CRED] · 
 · Description = MySQL database 2 · 
 · Driver · = mysql · 
 · User = root · 
 · Port = 3306 · 
 · Password = zabbix · 
 · Database = zabbix · 
 · Server = 127.0.0.1 · 

- 通过socket连接并使用证书.默认使用 Zabbix 数据库：

```{=html}
<!-- -->
```
 · [TEST_MYSQL_FILLED_CRED_SOCK] · 
 · Description = MySQL database 3 · 
 · Driver · = mysql · 
 · User = root · 
 · Password = zabbix · 
 · Socket = /var/run/mysqld/mysqld.sock · 
 · Database = zabbix

其他配置和参数都可以参考[MySQL官方文档](https://dev.mysql.com/doc/connector-odbc/en/connector-odbc-configuration-connection-parameters.html)

[comment]: # ({/95073b6c-dcea6601})
