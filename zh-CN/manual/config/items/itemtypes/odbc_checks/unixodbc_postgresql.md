[comment]: # translation:outdated

[comment]: # ({77fbd72c-677840d9})
# 2 PostgreSQL数据库推荐的UnixODBC设置

[comment]: # ({/77fbd72c-677840d9})

[comment]: # ({f0f709ff-041bb1bb})
#### 安装

-   **Red Hat Enterprise Linux/CentOS**:

```{=bash}
 # yum install postgresql-odbc
```

-   **Debian/Ubuntu**:

请参阅[PostgreSQL文档](https://www.postgresql.org/download/linux/ubuntu/) 为相应的平台下载必要的数据库驱动程序。

如需其他相关信息，请参考 [安装unixODBC](/manual/config/items/itemtypes/odbc_checks#installing-unixodbc)。

[comment]: # ({/f0f709ff-041bb1bb})

[comment]: # ({9f4e4b51-d41ee6ca})

#### Configuration

通过编辑 **odbcinst.ini** 和 **odbc.ini** 文件来完成ODBC的配置。这些配置文件可以在 */etc* 文件夹中找到。**odbcinst.ini** 文件可能不存在，这时我们需要手动来创建它。

请考虑以下  **odbc.ini**  配置参数的示例：

**odbcinst.ini**

    [postgresql]
    Description = General ODBC for PostgreSQL
    Driver      = /usr/lib64/libodbcpsql.so
    Setup       = /usr/lib64/libodbcpsqlS.so
    FileUsage   = 1
    # Since 1.6 if the driver manager was built with thread support you may add another entry to each driver entry.
    # This entry alters the default thread serialization level.
    Threading   = 2

**odbc.ini**

    [TEST_PSQL]
    Description = PostgreSQL database 1
    Driver  = postgresql
    #CommLog = /tmp/sql.log
    Username = zbx_test
    Password = zabbix
    # Name of Server. IP or DNS
    Servername = 127.0.0.1
    # Database name
    Database = zabbix
    # Postmaster listening port
    Port = 5432
    # Database is read only
    # Whether the datasource will allow updates.
    ReadOnly = No
    # PostgreSQL backend protocol
    # Note that when using SSL connections this setting is ignored.
    # 7.4+: Use the 7.4(V3) protocol. This is only compatible with 7.4 and higher backends.
    Protocol = 7.4+
    # Includes the OID in SQLColumns
    ShowOidColumn = No
    # Fakes a unique index on OID
    FakeOidIndex  = No
    # Row Versioning
    # Allows applications to detect whether data has been modified by other users
    # while you are attempting to update a row.
    # It also speeds the update process since every single column does not need to be specified in the where clause to update a row.
    RowVersioning = No
    # Show SystemTables
    # The driver will treat system tables as regular tables in SQLTables. This is good for Access so you can see system tables.
    ShowSystemTables = No
    # If true, the driver automatically uses declare cursor/fetch to handle SELECT statements and keeps 100 rows in a cache.
    Fetch = Yes
    # Bools as Char
    # Bools are mapped to SQL_CHAR, otherwise to SQL_BIT.
    BoolsAsChar = Yes
    # SSL mode
    SSLmode = Yes
    # Send tobackend on connection
    ConnSettings =

[comment]: # ({/9f4e4b51-d41ee6ca})
