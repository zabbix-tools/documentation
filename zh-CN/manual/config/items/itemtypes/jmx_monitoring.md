[comment]: # translation:outdated

[comment]: # ({b27acf8f-627e4087})
# 14 JMX监控

[comment]: # ({/b27acf8f-627e4087})

[comment]: # ({ff66fbd2-e3fddc4c})
#### 概述

JMX监控端可用于监控Java应用程序的JMX计数器.

从zabbix 2.0开始,JMX监控端以Zabbix守护进程的形式运行,称为“Zabbix Java gateway”.

为了检索主机上特定JMX计数器的值,Zabbix服务器查询Zabbix **Java gateway**,网关使用[JMX管理API](http://java.sun.com/javase/technologies/core/mntr-mgmt/javamanagement/)远程查询指定的应用程序,将结果返回给zabbix server.

有关更多细节和设置.请参考 [Zabbix Java gateway](/manual/concepts/java).

<note warning>Java gateway和JMX应用程序之间的通信应该在防火墙上放行.
:::

[comment]: # ({/ff66fbd2-e3fddc4c})

[comment]: # ({e2fdddb2-7feef73e})
#### 启用远程JMX监控java应用
Java应用程序不需要安装任何额外的软件,但是需要使用下面指定的命令行选项启动它,以支持远程JMX监控.

如果你只是希望开始在本地主机上监控一个简单的Java应用程序,没有安全性选项,请使用以下选项启动它:

     java \
    -Dcom.sun.management.jmxremote \
    -Dcom.sun.management.jmxremote.port=12345\
    -Dcom.sun.management.jmxremote.authenticate=false\
    -Dcom.sun.management.jmxremote.ssl=false\
    -Dcom.sun.management.jmxremote.registry.ssl=false\
    -jar /usr/share/doc/openjdk-6-jre-headless/demo/jfc/Notepad/Notepad.jar

这个 Java程序监听来自本地端口 12345 上的接入 JMX 连接（来自仅限本地主机）,并不需要身份验证或 SSL。

如果要允许其他主机的连接,请设置 -Djava.rmi.server.hostname 参数为该接口的 IP。

如果您希望在安全性方面更加严格,还有许多其他Java选项可供选择. 例如,下一个示例使用一组更通用的选项启动应用程序,并允许更多的网段连接,而不仅仅是本地主机。

     java \
    -Djava.rmi.server.hostname=192.168.3.14\
    -Dcom.sun.management.jmxremote \
    -Dcom.sun.management.jmxremote.port=12345\
    -Dcom.sun.management.jmxremote.authenticate=true \
    -Dcom.sun.management.jmxremote.password.file=/etc/java-6-openjdk/management/jmxremote.password \
    -Dcom.sun.management.jmxremote.access.file=/etc/java-6-openjdk/management/jmxremote.access \
    -Dcom.sun.management.jmxremote.ssl=true\
    -Dcom.sun.management.jmxremote.registry.ssl=true\
    -Djavax.net.ssl.keyStore=$YOUR_KEY_STORE \
    -Djavax.net.ssl.keyStorePassword=$YOUR_KEY_STORE_PASSWORD \
    -Djavax.net.ssl.trustStore=$YOUR_TRUST_STORE \
    -Djavax.net.ssl.trustStorePassword=$YOUR_TRUST_STORE_PASSWORD \
    -Dcom.sun.management.jmxremote.ssl.need.client.auth=true \
    -jar /usr/share/doc/openjdk-6-jre-headless/demo/jfc/Notepad/Notepad.jar

这些设置中的大部分可以在/etc/java-6-openjdk/management/management中指定（或者读取你系统上的配置文件）

请注意,如果您希望使用SSL,您必须修改startup.sh脚本,为Java网关添加“-Djavax.net.ssl.*”选项,以便它知道在哪里找到密钥和信任存储库

参见[使用JMX监控和管理](http://download.oracle.com/javase/1.5.0/docs/guide/management/agent.html)获得详细的描述。

[comment]: # ({/e2fdddb2-7feef73e})

[comment]: # ({372f18f6-4b8fd32c})
#### 在Zabbix web管理页面上配置JMX接口和监控项

Java网关在运行时,服务器会主动连接它,Java应用程序启用了远程JMX监视,现在可以在Zabbix GUI中配置接口和监控项了。

[comment]: # ({/372f18f6-4b8fd32c})

[comment]: # ({7f26d120-01c1becc})
##### 配置JMX接口

首先在相关主机上创建一个JMX类型的接口。\
![](../../../../../assets/en/manual/config/items/itemtypes/jmx_interface.png){width="600"}

标有红色星号的为必填项。

[comment]: # ({/7f26d120-01c1becc})

[comment]: # ({5036debe-f55573f5})
##### 添加 JMX 代理项

对于需要监控的每个 JMX 计数器,请添加 **JMX 代理** 项 附加到该接口.\

参考下面的截图中键的配置
`jmx["java.lang:type=Memory","HeapMemoryUsage.used"]`。

![](../../../../../assets/en/manual/config/items/itemtypes/jmx_item.png)

所有必填字段都标有红色星号。

需要 JMX 项的特定信息的字段是：

| | |
|---|---|
|*类型*|在此处设置**JMX代理**。|
|*Key*|`jmx[]` 项键包含三个参数：<br>**object name** - MBean 的对象名称<br>**attribute name** - 带有可选组合的 MBean 属性名称由点分隔的数据字段名称<br>**唯一的简短描述** - 允许多个 JMX 项目在主机上具有相同的对象名称和属性名称的唯一描述（可选）<br>有关 JMX 项目的更多详细信息，请参见下文<br>从 Zabbix 3.4 开始，您可以使用 `jmx.discovery[]` [low-level discovery](/manual/discovery/low_level_discovery/examples/jmx) 项来发现 MBean 和 MBean 属性。|
|*JMX 端点*|您可以指定自定义 JMX 端点.确保 JMX 端点连接参数与 JMX 接口匹配。这可以通过使用默认 JMX 端点中的 {HOST.\*} 宏来实现.<br>自 3.4.0 起支持此字段。支持 {HOST.\*} [宏](/manual/appendix/macros/supported_by_location) 和用户宏。|
|*用户名*|如果您在 Java 应用程序上配置了身份验证,请指定用户名.<br>支持用户宏.|
|*Password*|如果您在 Java 应用程序上配置了身份验证,请指定密码.<br>支持用户宏.|

如果您希望监视一个布尔计数器,它要么是"True",要么是 "false",然后您将信息类型指定为“数字（无符号）"并在 Preprocessing 中选择"Boolean to decimal" (预处理步骤标签).服务器将布尔值分别存储为 1 或 0.

[comment]: # ({/5036debe-f55573f5})

[comment]: # ({0e8eba78-9430cb1d})
#### JMX监控项详细信息

[comment]: # ({/0e8eba78-9430cb1d})

[comment]: # ({f9b7b25f-105cc785})
##### 简单属性

MBean对象名只不过是您在Java应用程序中定义的字符串.另一方面,属性名可能更复杂.如果一个属性返回原始数据类型(整数，字符串等)<请不要担心,键参考以下例子:

    jmx[com.example:Type=Hello,weight]

在这个例子中,一个对象的名称是"com".<example:Type=Hello>",属性名是"weight",可能返回值类型应该是"Numeric (float)".

[comment]: # ({/f9b7b25f-105cc785})

[comment]: # ({f78621c4-00e3fc9b})
##### 属性返回复合数据

当您的属性返回复合数据时,它变得更加复杂.
例如：您的属性名称是"apple"，它返回一个哈希表示其参数，如"重量"、"颜色"等.您的密钥可能看起来像这样:

    jmx[com.example:Type=Hello,apple.weight]

这就是属性名称和哈希键是如何分开的，通过使用"."符号。同样，如果一个属性返回嵌套的复合数据,那部分由"."分隔:

    jmx[com.example:Type=Hello,fruits.apple.weight]

[comment]: # ({/f78621c4-00e3fc9b})

[comment]: # ({8083ea23-ae61b0e7})
##### 返回表格数据的属性

表格数据属性由一个或多个复合属性组成.
如果在属性名参数中指定了这样一个属性,那么这个监控项值将以JSON格式返回该属性的完整结构.
表格数据属性中的单个元素值可以使用预处理来检索.

表格数据属性示例:

 · jmx[com.example:type=Hello,foodinfo]

监控项值示例:

``` {.javascript}
[
 · {
 · "a": "apple",
 · "b": "banana",
 · "c": "cherry"
 · },
 · {
 · "a": "potato",
 · "b": "lettuce",
 · "c": "onion"
 · }
]
```

[comment]: # ({/8083ea23-ae61b0e7})

[comment]: # ({9a43970e-3a5802d2})

##### 关于点的问题

当属性名或哈希键包含点,下面就是个例子:

    jmx[com.example:Type=Hello,all.fruits.apple.weight]

如何告诉Zabbix属性名是"all.fruits",而不只是“all”呢？如何区分作为属性名称一部分的点与分隔属性名和哈希键的点呢？这是一个问题.

在 **2.0.4** 版本之前，Zabbix
Java网关是无法处理此类情况的,在监控项里,用户只能留下UNSUPPORTED项了.
从2.0.4开始解决了此问题,你所需要做的就是用反斜杠来转义名字的一部分点：

    jmx[com.example:Type=Hello,all\.fruits.apple.weight]

同样，如果哈希键包含一个点，你也可以转义它：

    jmx[com.example:Type=Hello,all\.fruits.apple.total\.weight]

[comment]: # ({/9a43970e-3a5802d2})

[comment]: # ({0165a656-80aedcd9})
##### 其他问题

属性名中的反斜杠字符特应该被转义：

    jmx[com.example:type=Hello,c:\\documents]

有关处理JMX监控项键值中的其他特殊字符，请参见 [this section](/manual/config/items/item/key##parameter_-_quoted_string).
这就是全部了,希望可以快乐的使用jMX监控!

[comment]: # ({/0165a656-80aedcd9})



[comment]: # ({c5fe8cda-1813b2fc})
##### 非基本数据类型

从 Zabbix 4.0.0 开始,可以使用返回的自定义 MBean 覆盖 **toString()** 方法的非基本数据类型.

[comment]: # ({/c5fe8cda-1813b2fc})

[comment]: # ({bf1ffe06-ffdec0f9})
#### 在 JBoss EAP 6.4 中使用自定义锚点

自定义锚点允许使用默认RMI以外的不同传输协议.

为了说明这种可能性,让我们以配置JBoss EAP 6.4监控为例.首先,让我们做一些假设:

- 您已经安装了 Zabbix Java 网关。如果没有，那么你可以
    按照[文档](/manual/concepts/java) 执行。
- Zabbix server 和 Java gateway 都安装了前缀
    /usr/本地/
- JBoss 已经安装在 /opt/jboss-eap-6.4/ 并且正在运行
    独立模式
- 我们假设所有这些组件都在同一个主机上工作
- 防火墙和 SELinux 被禁用（或相应配置）

让我们在 zabbix\_server.conf 中做一些简单的设置：

    JavaGateway=127.0.0.1
    StartJavaPollers=5

并在 zabbix\_java/settings.sh 配置文件中（或
zabbix\_java\_gateway.conf）：

    START_POLLES=5

检查 JBoss 是否监听其标准管理端口：

    $ netstat -natp | grep 9999
    tcp 0 0 127.0.0.1:9999 0.0.0.0:* 听 10148/java

现在让我们在 Zabbix 中创建一个 JMX 接口为 127.0.0.1:9999 的主机。

![](../../../../../assets/en/manual/config/items/itemtypes/jmx_jboss_example.png){width="600"}


我们知道这个版本的 JBoss 使用了 JBoss Remoting
协议而不是 RMI，我们可以为JMX模板中的项目批量更新 JMX 锚点参数：

   service:jmx:remoting-jmx://{HOST.CONN}:{HOST.PORT}

![](../../../../../assets/en/manual/config/items/itemtypes/jmx_jboss_example_b.png)

让我们更新配置缓存：

    $ /usr/local/sbin/zabbix_server -R config_cache_reload

请注意,您可能首先会遇到错误.

![](../../../../../assets/en/manual/config/items/itemtypes/jmx_jboss_example4.png){width="600"}

"不支持的协议: remoting-jmx"表示 Java 网关不支持使用指定的协议.这可以通过
创建具有以下内容的 \~/needed\_modules.txt 文件：

 · jboss-as-remoting
 · jboss-logging
 · jboss-logmanager
 · jboss-marshalling
 · jboss-remoting
 · jboss-sasl
 · jcl-over-slf4j
 · jul-to-slf4j-stub
 · log4j-jboss-logmanager
 · remoting-jmx
 · slf4j-api
 · xnio-api
 · xnio-nio

然后执行命令：

· $ for i in $(cat ~/needed_modules.txt); do find /opt/jboss-eap-6.4 -iname ${i}*.jar -exec cp {} /usr/local/sbin/zabbix_java/lib/ \; ; done

因此，Java 网关将拥有所有必要的模块来使用jmx 远程处理.剩下的就是重启Java网关,稍等一下,如果你做的一切都正确,将看到 JMX 监控数据开始出现在 Zabbix（另见：[最新数据](/[[/manual/web_interface/frontend_sections/monitoring/latest_data)).

[comment]: # ({/bf1ffe06-ffdec0f9})
