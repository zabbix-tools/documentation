[comment]: # translation:outdated

[comment]: # ({ba868d51-0ee06d3b})
# 11 外部检查

[comment]: # ({/ba868d51-0ee06d3b})

[comment]: # ({bdbe5c2c-cd36d798})

#### 概述

外部检查是"Zabbix server"通过[运行shell脚本](/manual/appendix/command_execution)或二进制执行的检查.但是当主机被"Zabbix proxy"接管时,外部检查则由"Zabbix proxy"执行.

外部检查不需要在被监控的主机上运行任何客户端

监控键的语法是：

    脚本[<parameter1>,<parameter2>,...]

参数：

|字段|描述|
|--------|----------|
|**script**|shell 脚本或二进制文件的名称.|
|**parameter(s)**|可选的命令行参数.|

如果您不想将任何参数传递给脚本,您可以使用：

    脚本[] 或
    脚本

Zabbix服务器将查找定义为外部脚本位置的目录(Zabbix服务器配置文件中的“ExternalScripts”参数)并执行该命令.该命令将作为Zabbix服务器运行的用户执行,任何访问权限或环境变量都应该在封装在脚本中处理,该命令的权限应该只允许该用户在指定的目录下执行它.

::: 注意警告
不要过度使用外部检查! 因为每个脚本都需要Zabbix服务器启动一个fork进程,所以运行很多脚本会大幅度的下降Zabbix的性能.
:::

[comment]: # ({/bdbe5c2c-cd36d798})

[comment]: # ({6a807b6a-55e878ce})
#### 用法示例

使用第一个参数执行脚本 **check\_oracle.sh**
'-H'。第二个参数将替换为 IP 地址或 DNS 名称，
取决于主机属性中的选择。

    check_oracle.sh["-h","{HOST.CONN}"]

假设主机配置为使用 IP 地址，Zabbix 将执行：

    check_oracle.sh '-h' '192.168.1.4'

[comment]: # ({/6a807b6a-55e878ce})

[comment]: # ({1f248d52-011b7534})
####外部检查结果

检查的返回值是标准输出和标准错误(自Zabbix 2.0以来,返回带有截断空格后的完整输出).

::: 注意重要
在标准错误输出的情况下,文本(字符、日志或文本类型的信息)项将不再支持.
:::

如果没有找到请求的脚本,或者Zabbix服务器没有执行该脚本的权限,则该项目将标记不支持,并将提示相应的错误消息.在超时的情况下,该项目也将被标记为不支持,将显示相应的错误消息,脚本的forked进程将被结束.

[comment]: # ({/1f248d52-011b7534})
