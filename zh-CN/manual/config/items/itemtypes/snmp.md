[comment]: # translation:outdated

[comment]: # ({23b2ffed-43646dd0})
# 2 SNMP代理

[comment]: # ({/23b2ffed-43646dd0})

[comment]: # ({54b39e72-f312fc02})
＃＃＃＃ 概述

您可能希望在打印机、网络交换机、路由器或UPS等设备上使用SNMP监控，这些设备通常启用SNMP，在这些设备上尝试设置完整的操作系统和Zabbix代理是不切实际的。

为了能够监控SNMP代理在这些设备上提供的数据，Zabbix服务器初始化配置时必须支持(/manual/installation/install#configure_the_sources)SNMP数据源。
SNMP 检查只支持 UDP 协议。

Zabbix 服务器和代理守护进程向 SNMP 设备一个请求中查询多个值时，这会影响所有SNMP监控项（常规 SNMP监控项、具有动态索引的 SNMP 监控项和 SNMP 自动发现）同时可以使 SNMP 处理更加高效。见[批量处理](#internal_workings_of_bulk_processing) 技术部分内部如何运作的详细信息。对于无法使用“批量请求”的接口可以通过设置可以关闭。

如果Zabbix服务器和代理守护程序收到错误的SNMP响应，则它们的日志行与以下类似:
    SNMP response from host "gateway" does not contain all of the requested variable bindings

虽然它们并没有涵盖所有有问题的情况，但它们对于识别应禁用批量请求的单个SNMP设备非常有用。

Zabbix服务器/代理在查询尝试失败后将始终或至少重试一次：通过SNMP库的重试机制或通过内部[批量处理]机制。

::: 注意警告
如果监控SNMPv3设备，请确保msgAuthoritativeEngineID（也称为snmpEngineID或“引擎ID”）从不被两台设备共享。根据RFC 2571 （3.1.1.1节），每个设备必须是唯一的
:::snmpEngineID

::: 注意警告
 RFC3414 要求 SNMPv3 设备保留其engineBoots。某些设备不这么做，这会导致有些设备在运行时的不会丢弃过期的SNMP 消息。在这种情况下，需要在服务器/代理上手动清除 SNMP 缓存（通过使用 [-R snmp\_cache\_reload](/manual/concepts/server#runtime_control))或者需要重新启动服务器/代理。
:::

[comment]: # ({/54b39e72-f312fc02})

[comment]: # ({a22892dd-cac544a8})
#### 配置SNMP监控

要通过SNMP开始监控设备，必须执行以下步骤：

[comment]: # ({/a22892dd-cac544a8})

[comment]: # ({e80af954-501ff4eb})
＃＃＃＃＃ 步骤1

找出您要监控的项目的 SNMP 字符串（或 OID）。

要获取SNMP字符串列表，请使用 snmpwalk 命令（net-snmp的部分软件应该在Zabbix安装时同时安装）或等效工具：

    shell> snmpwalk -v 2c -c public <主机 IP> 。

此处的“2c”代表 SNMP 版本，您也可以将其替换为“1”，表示设备上的 SNMP 版本v1。

它会返回给你一个SNMP字符串及其最后一个值的列表。如果不是，那么SNMP 'community' 可能与标准的'public'不同，在这种情况下，请找出它是什么。

然后，您可以遍历列表，直到找到要监控的字符串，例如：如果要监视通过端口3进入交换机的字节，你将使用此行中的IF-MIB :: ifInOctets.3字符串：

    IF-MIB::ifInOctets.3 = Counter32: 3409739121

你现在可以使用 snmpget 命令找出'IF-MIB :: ifInOctets.3'的数字OID：

    shell> snmpget -v 2c -c 公共 -On 10.62.1.22 IF-MIB::ifInOctets.3

请注意，字符串中的最后一个数字是您需要监控的端口号。另请参阅：[动态索引]（/manual/config/items/itemtypes/snmp/dynamicindex）。
    .1.3.6.1.2.1.2.2.1.10.3 = Counter32：3472126941

同样，OID 中的最后一个数字是端口号。

::: Note
3COM似乎使用端口号在100以上，例如端口1=端口101，端口3=端口103，但Cisco使用常规数字，例如端口3=3。
:::

::: Note
一些最常用的SNMP OID，Zabbix将自动转换为数字表示。（/manual/config/items/itemtypes/SNMP/special_mibs）
:::

在上面的例子中，值类型是“Counter32”它在内部对应于ASN_COUNTER类型。完整的支持类型包括 ASN_COUNTER, ASN_COUNTER64, ASN_UINTEGER, ASN_UNSIGNED64, ASN_INTEGER, ASN_INTEGER64, ASN_FLOAT, ASN_DOUBLE, ASN_TIMETICKS, ASN_GAUGE, ASN_IPADDRESS, ASN_OCTET_STR 和 ASN_OBJECT_ID (从2.2.8, 2.4.3之后). 这些类型大致对应于snmpget 输出的 "Counter32", "Counter64", "UInteger32", "INTEGER", "Float", "Double", "Timeticks", "Gauge32", "IpAddress", "OCTET STRING", "OBJECT IDENTIFIER", 但也有可能显示为 "STRING", "Hex-STRING", "OID" 或者其它, 这取这取决于显示提示的表达方式。

[comment]: # ({/e80af954-501ff4eb})

[comment]: # ({62fddab0-929235a8})
＃＃＃＃＃ 第2步

为设备创建一个(/manual/config/hosts/host) 主机。

![](../../../../../assets/en/manual/config/items/itemtypes/snmp/snmp_host.png)

为主机添加 SNMP 接口：

- 输入 IP 地址/DNS 名称和端口号
- 从下拉列表中选择 *SNMP 版本*
- 根据所选 SNMP 版本添加接口凭据：
    - SNMPv1、v2 只需要community凭据（默认值是"'public'"）
    - SNMPv3 需要更具体的选项（见下文）
- 将*使用批量请求*复选框标记为允许批量
    处理 SNMP 请求

|SNMPv3参数|说明|
|----------------|-----------|
|*上下文名称*|输入上下文名称以识别 SNMP 子网上的项目。<br>自 Zabbix 2.2 起，SNMPv3 项目支持*上下文名称*。<br>在此字段中解析用户宏。|
|*安全名称*|输入安全名称。<br>用户宏在此字段中解析。|
|*安全级别*|选择安全级别：<br>**noAuthNoPriv** - 不使用身份验证或隐私协议<br>**AuthNoPriv** - 使用身份验证协议，不使用隐私协议<br>**AuthPriv ** - 使用身份验证和隐私协议|
|*身份验证协议*|选择身份验证协议 - *MD5*、*SHA1*、*SHA224*、*SHA256*、*SHA384* 或 *SHA512*。|
|*身份验证密码*|输入身份验证密码。<br>用户宏在此字段中解析。|
|*隐私协议*|选择隐私协议 - *DES*、*AES128*、*AES192*、*AES256*、*AES192C* (Cisco) 或 *AES256C* (Cisco)。|
|*隐私密码*|输入隐私密码。<br>用户宏在此字段中解析。|

如果SNMPv3凭据（安全名称，验证协议/口令，隐私协议）错误，Zabbix会从net-snmp收到错误，如果 私钥 错误，在这种情况下，Zabbix会从net-snmp收到TIMEOUT错误。

::: 警告
在不更改安全名称的情况下，对验证协议、验证口令、隐私协议或私钥的更改只有在手动清除服务器/代理上的缓存（通过使用-R snmp_cache_reload）或重新启动服务器/代理后才会生效。如果安全名称也已更改，则所有参数将立即更新。
:::

您可以使用zabbix提供的任意 SNMP 模板（ SNMP 设备模板和其他模板），该模板将自动添加监控项。但是，那模板可能与主机不兼容。点击 Add 保存主机。

[comment]: # ({/62fddab0-929235a8})

[comment]: # ({b300c734-290ebad1})
##### 步骤 3

创建一个监控项。

所以现在回到Zabbix并点击前面创建的SNMP主机的 监控项 。 如果你在创建主机时选择使用模板，你将拥有与主机相关联的SNMP监控项列表。我们假设你要使用snmpwalk和snmpget采集的信息创建监控项，单击 创建监控项。在新的监控项表单中：
- 输入监控项"名称"。
- 将"类型"字段更改为"SNMP客户端"
- 输入“键值”为有意义的内容，例如，SNMP-InOctets-Bps。
- 确保“主机接口”字段中有你的交换机/路由器
- 将你之前检索到的文本或数字OID输入到'SNMP OID'字段中，例如：.1.3.6.1.2.1.2.2.1.10.3
- 将“信息类型”设置为 浮点数
- 如果你希望“更新间隔”和“历史数据保留时长”与默认值不同，请选择一个自定义乘数（如果需要），并输入数值
- 在进程预处理选项卡中，添加 *Change per second* 步骤（重要！，否则您将从 SNMP 设备获取累积值，而不是差异值）。

![](../../../../../assets/en/manual/config/items/itemtypes/snmp_item.png)

所有必填字段都标有红色星号。

现在保存监控项，进入 监测中 → 最新数据 来获取你的SNMP数据!

[comment]: # ({/b300c734-290ebad1})

[comment]: # ({14e6790b-6021e1bd})
##### 示例 

通用示例：

|参数 |描述 |
|---------------------|-|
|**OID**|1.2.3.45.6.7.8.0 (or .1.2.3.45.6.7.8.0)|
|**键值**|用作触发器引用的唯一字符串><br>例如, "my\_param".|

请注意，OID 可以以数字或字符串形式给出。但是，在某些情况下，必须将字符串 OID 转换为数字表示。实用程序 snmpget 可用于此目的：
     shell> snmpget -On localhost public enterprises.ucdavis.memory.memTotalSwap.0

在配置Zabbix源时指定了 --with-net-snmp 标志，可以监视SNMP参数

[comment]: # ({/14e6790b-6021e1bd})

[comment]: # ({27aab31d-45af5cf0})
##### 示例 2

监控正常运行时间：

|参数|说明|
|---------|------------|
|**OID**|MIB::sysUpTime.0对应获取监控项值|
|**Key**|路由器正常运行时间|
|**Value type**|浮点数|
|**Units**|正常运行时间|
|**Multiplier**|倍数|

[comment]: # ({/27aab31d-45af5cf0})

[comment]: # ({new-ea70430a})

#### Native SNMP bulk requests

The **walk[OID1,OID2,...]** item allows to use native SNMP functionality for bulk requests (GetBulkRequest-PDUs), available in SNMP versions 2/3. 

A GetBulk request in SNMP executes multiple GetNext requests and returns the result in a single response. This may be used for regular SNMP items as well as for SNMP discovery to minimize network roundtrips.

The SNMP **walk[OID1,OID2,...]** item may be used as the master item that collects data in one request with dependent ityems that parse the response as needed using preprocessing. 

Note that using native SNMP bulk requests is not related to the option of combining SNMP requests, which is Zabbix own way of combining multiple SNMP requests (see next section).

[comment]: # ({/new-ea70430a})

[comment]: # ({406df320-c57b8645})
#### 批量处理的内部工作原理

从 Zabbix server和proxy 2.2.3 版本开始查询 SNMP 设备开始再一个请求获取多个值。这会影响几种类型的 SNMP监控项：

- 常规 SNMP 监控项
- SNMP监控项[带动态索引](/manual/config/items/itemtypes/snmp/dynamicindex)
- SNMP [低级别自动发现规则](/manual/discovery/low_level_discovery/examples/snmp_oids)

具有相同参数的单个接口上的所有SNMP监控项都将同时进行查询。前两种类型的监控项由轮询器分批采集，最多128个监控项，而低级发现规则如前所述单独处理。

在较低级别上，执行查询值的操作有两种：获取多个指定对象和游历OID树。

对于"getting"，GetRequest-PDU最多使用128个变量绑定。 对于"walking"，GetNextRequest-PDU用于SNMPv1和GetBulkRequest，"max-repetitions"字段最多128个用于SNMPv2和SNMPv3。

因此，每个SNMP监控项类型的批量处理的优势如下：

- 常规SNMP项目受益于“getting” 的改进;
- 具有动态索引的SNMP监控项受益于“getting”和“walking”改进：“getting”用于索引验证，“walking”用于构建缓存;
- SNMP低级发现规则受益于“walking”的改进。
然而，有一个技术问题，并非所有设备都能够根据请求返回128个值。有些总是给出正确的回应，其它情况则会以“tooBig（1）”错误做出回应，或者一旦潜在的回应超过了一定的限度，则一律不回应。

为了找到最佳数量的对象来查询给定的设备，Zabbix使用以下策略。它在请求中查询“值1”时谨慎开始。 如果成功，它会在请求中查询“值2”。 如果再次成功，则查询请求中的“值3”，并通过将查询对象的数量乘以1.5来继续，导致以下请求大小的顺序：1,2,3,4,6,9,13,19，28，42，63，94，128。

然而，一旦设备拒绝给出适当的响应（例如，对于42个变量），Zabbix会做两件事情。

首先，对于当前批量监控项，它将单个请求中的对象数减半，并查询21个变量。 如果设备处于活动状态，那么查询应该在绝大多数情况下都有效，因为已知28个变量可以工作，21个变量明显少于此。 但是，如果仍然失败，那么Zabbix会逐渐回到查询值。 如果此时仍然失败，那么设备肯定没有响应，请求大小也不是问题。

Zabbix为后续批量监控项做的第二件事是它从最后成功的变量数量开始（在我们的示例中为28），并继续将请求大小递增1，直到达到限制。 例如，假设最大响应大小为32个变量，后续请求的大小为29,30,31,32和33.最后一个请求将失败，Zabbix将永远不再发出大小为33的请求。 从那时起，Zabbix将为该设备查询最多32个变量。

如果大型查询因此数量的变量而失败，则可能意味着两件事之一。 设备用于限制响应大小的确切标准无法知晓，但我们尝试使用变量数来近似。 因此，第一种可能性是，在一般情况下，此数量的变量大约是设备的实际响应大小限制：有时响应小于限制，有时它大于限制。 第二种可能性是任何方向的UDP数据包都丢失了。 由于这些原因，如果Zabbix查询失败，它会减少最大数量的变量以尝试深入到设备的舒适范围，但（从2.2.8开始）最多只能达到两次。

在上面的示例中，如果包含32个变量的查询失败，Zabbix会将计数减少到31.如果发生这种情况也会失败，Zabbix也会将计数减少到30.但是，Zabbix不会将计数减少到30以下， 因为它会假设进一步的失败是由于UDP数据包丢失，而不是设备的限制。

但是，如果设备由于其他原因无法正确处理批量请求，并且上述启发式方法不起作用，Zabbix 2.4版本之后每个接口都有“使用批量请求”设置，允许禁用该设备的批量请求。

[comment]: # ({/406df320-c57b8645})
