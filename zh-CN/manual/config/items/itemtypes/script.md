[comment]: # translation:outdated

[comment]: # ({616cbaae-1a1e2756})
# 18 脚本监控项

[comment]: # ({/616cbaae-1a1e2756})

[comment]: # ({a7c78d4e-cb89bc38})
#### 概览

脚本监控项可以通过执行用户自定义的JavaScript代码，检索HTTP/HTTPS的方式来收集数据。除了脚本外，可以指定一个可选的参数列表（一些键值对）并配置超时限制。

此监控项类型在有收集数据过程中需要多个步骤或复杂逻辑的场景非常有用。举个例子，一个脚本监控项可以被配置为执行一个HTTP调用，然后经过某些方式处理从第一步调用得到的数据，并将转换后的数值传递给第二个HTTP调用。

脚本监控项有Zabbix server 或 Zabbix proxy的轮询器

[comment]: # ({/a7c78d4e-cb89bc38})

[comment]: # ({new-7ed55e87})
#### 配置

在[监控项配置表单](/manual/config/items/item)的*类型*字段选择脚本，然后填写必要字段。

![script\_item.png](../../../../../assets/en/manual/config/items/itemtypes/script_item.png)

所有标有红色星号的为必填字段。

需要的脚本监控项特定信息的字段是：

|字段|描述|
|-----|-----------|
|Key|输入用于识别监控项唯一性的键。|
|Parameters|指定传输给脚本的属性与值的配对。<br>支持的宏: [内建宏](/manual/config/macros) {HOST.CONN}, {HOST.DNS}, {HOST.HOST}, {HOST.IP}, {HOST.NAME}, {ITEM.ID}, {ITEM.KEY}, {ITEM.KEY.ORIG} 与[用户自定义宏](/manual/config/macros/user_macros)。|
|Script|在点击参数字段的空白所弹出的输入框中输入JavaScript代码（或在查看/编辑按钮后进入该输入框。代码必须提供返回指标数值的逻辑。<br>代码可以访问所有的参数，可以执行HTTP GET、POST、PUT、DELETE请求，并且可以控制HTTP标头和请求体<br>查看更多: [更多JavaScript对象](/manual/config/items/preprocessing/javascript/javascript_objects), [JavaScript Guide](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide)。|
|Timeout|JavaScript执行超时时间(1-60s, 默认3s)。<br>支持时间后缀，如：30s, 1m。|

[comment]: # ({/new-7ed55e87})

[comment]: # ({6863fa4f-4c860844})
#### 示例

[comment]: # ({/6863fa4f-4c860844})

[comment]: # ({c8e039e6-9c6c78ee})
##### 简单的数据采集

从 *https://www.example.com/release\_notes* 页面收集内容:

- 创建一个监控项，类型选择”脚本“。 \
- 在*脚本*字段填写下面的代码：

``` {.java}
var request = new HttpRequest();
return request.get("https://www.example.com/release_notes");
```

[comment]: # ({/c8e039e6-9c6c78ee})

[comment]: # ({new-7115c5ef})
##### Dat collection with parameters

Get the content of a specific page and make use of parameters: 

- Create an item with type Script and two parameters:
  - **url : {$DOMAIN}** (the user macro {$DOMAIN} should be defined, preferably on a host level)
  - **subpage : /release_notes**

![](../../../../../assets/en/manual/config/items/itemtypes/script_example1.png)

- In the script field, enter: 

   var obj = JSON.parse(value);
   var url = obj.url;
   var subpage = obj.subpage;
   var request = new HttpRequest();
   return request.get(url + subpage);

[comment]: # ({/new-7115c5ef})


[comment]: # ({0f3d6cf5-18a089f4})
##### 多个HTTP请求

同时从 *https://www.example.com* 和 *https://www.example.com/release\_notes*两个站点收集数据:

- 创建一个监控项，类型选择”脚本“。 
- 在*脚本*字段填写下面的代码：

``` {.java}
var request = new HttpRequest();
return request.get("https://www.example.com") + request.get("https://www.example.com/release_notes");
```

[comment]: # ({/0f3d6cf5-18a089f4})

[comment]: # ({42c79cec-d837bcdd})
##### 记录日志

添加"Log test"到Zabbix server 日志并返回数值 "1"给监控项：

- 创建一个监控项，类型选择”脚本“。 
- 在*脚本*字段填写下面的代码：

``` {.java}
Zabbix.log(3, 'Log test');
return 1;
```

[comment]: # ({/42c79cec-d837bcdd})
