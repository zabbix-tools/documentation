[comment]: # attributes: notoc

[comment]: # translation:outdated

[comment]: # ({new-8981e17d})
# 8 内部检查

[comment]: # ({/new-8981e17d})

[comment]: # ({new-34928067})
#### 概述

内部检查可以监控Zabbix的内部进程。换句话说，你可以监控Zabbix
server或Zabbix proxy的运行情况。

内部检查是：

-   在Zabbix server上 - 主机是否被服务器监控
-   在Zabbix proxy上 - 主机是否被代理服务器监控

内部检查由服务器或代理服务器执行，无论主机维护状态如何（从Zabbix
2.4.0起）

要使用此监控项，请选择 **Zabbix internal** 监控项类型。

::: notetip
内部检查由Zabbix轮询器处理。
:::

[comment]: # ({/new-34928067})

[comment]: # ({new-5d8f3b63})
#### Performance

Using some internal items may negatively affect performance. These items are:

-   `zabbix[host,,items]`
-   `zabbix[host,,items_unsupported]`
-   `zabbix[hosts]`
-   `zabbix[items]`
-   `zabbix[items_unsupported]`
-   `zabbix[queue]`
-   `zabbix[required_performance]`
-   `zabbix[stats,,,queue]`
-   `zabbix[triggers]`

The [System information](/manual/web_interface/frontend_sections/reports/status_of_zabbix) and [Queue](/manual/web_interface/frontend_sections/administration/queue) 
frontend sections are also affected.

[comment]: # ({/new-5d8f3b63})

[comment]: # ({new-74b446f4})
#### 支持的检查

-   没有尖括号的参数是常量 - 例如,
    `zabbix[host,<type>,available]`中的'host' and 'available'.
    在监控项键值中使用它们。
-   仅当主机被服务器监控时，才能收集“代理服务器不支持”的监控项和监控项参数的值。反之亦然，“服务器不支持”的值只能在代理监视主机时收集。

|键值|<|<|<|<|<|
|------|-|-|-|-|-|
|▲|描述|<|返|值                                                                                         注释|<|
|zabbix\[boottime\]|<|<|<|<|<|
|<|Zabbix server 或 Zabbix proxy进程启动时间（秒）|<|整数|<|<|
|zabbix\[history\]|<|<|<|<|<|
|<|存储在HISTORY表中的数量值.|<|整数.|如果使用MySQL|nnoDB, Oracle or PostgreSQL，请勿使用!<br>*(代理服务器不支持)*|
|zabbix\[history\_log\]|<|<|<|<|<|
|<|存储在HISTORY\_LOG表中的数量值|<|整数.|如果使用MySQL|nnoDB, Oracle or PostgreSQL，请勿使用!<br>从Zabbix **1.8.3** 开始支持此监控项<br>*(代理服务器不支持)*|
|zabbix\[history\_str\]|<|<|<|<|<|
|<|存储在HISTORY\_STR表中的数量值|<|整数.|如果使用MySQL|nnoDB, Oracle or PostgreSQL，请勿使用!<br>*(代理服务器不支持)*|
|zabbix\[history\_text\]|<|<|<|<|<|
|<|存储在HISTORY\_TEXT表中的数量值|<|整数|如果使用MySQL|nnoDB, Oracle or PostgreSQL，请勿使用!<br>从Zabbix **1.8.3** 开始支持此监控项<br>*(代理服务器不支持)*|
|zabbix\[history\_uint\]|<|<|<|<|<|
|<|存储在HISTORY\_UINT表中的值数|<|整数|如果使用MySQL|InnoDB, Oracle or PostgreSQL，请勿使用!<br>从Zabbix **1.8.3** 开始支持此监控项<br>*(代理服务器不支持)*|
|zabbix\[host,,items\]|<|<|<|<|<|
|<|主机上启用的监控项的数量（受支持和不受支持）.|<|整数|从Zabbix **3.0.0.** 开始支持|监控项|
|zabbix\[host,,items\_unsupported\]|<|<|<|<|<|
|<|主机上启用的不受支持的监控项数量|<|整数|从Zabbix **3.0.0.*|开始支持此监控项|
|zabbix\[host,,maintenance\]|<|<|<|<|<|
|<|当前主机的维护状态|<|0 - 主机处于|常状态,\                                                                         此监控项始终由Zabbix服务器1 - 主机处于维护状态但采集数据,\                                                               第二个参数必须为空，并保2 - 主机处于维护状态不采集数据.                                                                此监控项从Zabbix*|理，无论主机位置如何（在服务器或代理服务器上）。 代理将不会使用配置数据接收该监控项。<br>供将来使用。<br>2.4.0.**开始支持<br>|
|zabbix\[host,discovery,interfaces\]|<|<|<|<|<|
|<|Zabbix frontend中主机所有配置接口的详细信息|<|JSON对象|此监控项可以在 [低级发现](|manual/discovery/low_level_discovery/host_interfaces) 中使用<br>此监控项从Zabbix**3.4.0.**开始支持<br>*(代理服务器不支持)*|
|zabbix\[host,<type>,available\]|<|<|<|<|<|
|<|主机上特殊类型的检查。该监控项的值对应于主机列表中的可用性图标|<|0 - 不可用, 1 - 可用, 2 - 未知.|有效的类型是: **agent**, **snmp**, **ipmi**|**jmx**.<br><br>监控项的值根据有关主机 [不可达/不可用](/manual/appendix/items/unreachability) 的配置参数计算.<br><br>此监控项从Zabbix**2.0.0.**开始支持<br>|
|zabbix\[hosts\]|<|<|<|<|<|
|<|已监控主机数量.|<|整数|此监控项从Zab|ix**2.2.0**开始支持.|
|zabbix\[items\]|<|<|<|<|<|
|<|已启用监控项的数量（受支持和不受支持的）|<|整数|<|<|
|zabbix\[items\_unsupported\]|<|<|<|<|<|
|<|不支持的监控项数量|<|整数|<|<|
|zabbix\[java,,<param>\]|<|<|<|<|<|
|<|有关Zabbix Java网关的信息|<|如果<|aram>为 **ping**, 则返回“1”. 可以使用nodata（）触发功能来检查Java网关的可用性。\   <param>的有效值是: *ping*, *ver<br>如果<param>是 **version**, 则返回Java网关的版本。 例如: "2.0.0".                         第二个参数必须为空，并保留|ion*<br><br>将来使用。<br><br>此监控项从Zabbix**2.0.0.**开始支持|
|zabbix\[preprocessing\_queue\]|<|<|<|<|<|
|<|预处理队列中队列数量|<|整数|此监控项可用于监控预处|队列长度<br>\\\\此监控项从Zabbix**3.4.0.**开始支持|
|zabbix\[process,<type>,<mode>,<state>\]|<|<|<|<|<|
|<|时间是一个特定的Zabbix进程或一组进程（由<type>和<mode>标识）,以百分比形式在<state>中使用。 仅在最后一分钟计算。<br>\\\\如果<mode>是没有运行的Zabbix进程号 (例如，运行<mode>的5个轮询器被指定为6), 则此监控项将变为不受支持的状态。<br>最小和最大值是指单个进程的使用百分比。因此，如果在一组3个轮询器中，每个进程的使用百分比为2,18和66，则min将返回2，max将返回66。\\\\进程报告它们在共享内存中所做的事情，而自我监视进程每秒都会对这些数据进行汇总。 状态改变（忙/空闲）在更改时被注册 - 因此一个进程变得繁忙，并且直到状态变为空闲时才更改或更新状态。这确保即使完全挂起的进程也被正确地注册为100％繁忙。\\\\目前，"busy"表示"not sleeping"，但在将来可能会引入额外的状态 - 等待锁、执行数据库查询等。\                                                                                                                                                                                      **alert manager**在Linux和大多数其它系统上，解析度是1/100秒。|<报警任务管理器<br>|时间百分比<br>浮点数|目前支持以下进程类型:<br>**alerter** - 发送通知的进程 *(代理服务器不支持)*<br>**configuration|<yncer** - 用于管理配置数据的内存中缓存的进程<br>**data sender** - 代理服务器数据发送者 *(不支持Zabbix server)*<br>**discoverer** - 设备发现进程<br>**escalator** - action升级进程 *(代理服务器不支持)*<br>**heartbeat sender** - 代理服务器心跳发送方 *(不支持Zabbix server)*<br>**history syncer** - 历史数据库写入者<br>**housekeeper** - 删除旧历史数据的进程<br>**http poller** - web轮询检查器<br>**icmp pinger** - icmpping轮询检查器<br>**ipmi manager** - IPMI轮询管理<br>**ipmi poller** - IPMI轮询检查器<br>**java poller** - Java检查轮询器<br>**poller** - 被动检查的通用轮询器<br>**preprocessing manager** - 预处理任务管理<br>**preprocessing worker** - 数据预处理进程<br>**proxy poller** - 被动代理服务器的轮询器 *(代理服务器不支持)*<br>**self-monitoring** - 收集内部服务器统计信息的进程<br>**snmp trapper** - SNMP陷阱捕获器<br>**task manager** - 用于远程执行其他组件请求的任务的进程(例如手动关闭, 应答, 强制检查, 远程命令功能)<br>**timer** - 处理维护的计时器 *(代理服务器不支持)*<br>**trapper** - 进行主动检查、代理通信的trapper<br>**unreachable poller** - 无法访问的设备轮询器<br>**vmware collector** - 负责VMware服务数据采集的VMware数据收集器<br><br>注意：你还可以在服务器日志文件中查看这些进程类型。<br><br>有效模式是:<br>**avg** - 给定类型的所有进程的平均值（默认）<br>**count** - 返回给定进程类型的forks数，不应指定**<state>**<br>**max** - 最大值<br>**min** - 最小值<br>**<process number>** - 进程号 (在1和预分叉实例数之间)。 例如, 如果4个trappers正在运行，则该值在1到4之间。<br><br>有效状态是:<br>**busy** - 进程处于忙状态，例如处理请求（默认）<br>**idle** - 进程处于空闲状态，什么都不做。<br><br>示例:<br>=> zabbix\[process,poller,avg,busy\] → 在最后一分钟内，轮询进程的平均花费时间<br>=> zabbix\[process,"icmp pinger",max,busy\] → 在最后一分钟内，通过ICMP pinger进程花费最多时间<br>=> zabbix\[process,"history syncer",2,busy\] → 在最后一分钟内，第2号同步器执行某些操作花费的时间<br>=> zabbix\[process,trapper,count\] → 当前运行的trapper进程的数量<br><br>此监控项从Zabbix **1.8.5.** 开始支持|
|zabbix\[proxy,<name>,<param>\]|<|<|<|<|<|
|<|有关Zabbix proxy的信息.|<|整数|<na|e> - 代理服务器名<br>支持的参数列表 (<param>):<br>lastaccess - 从代理服务器上收到的最后心跳消息的时间戳<br><br>示例:<br>=> zabbix\[proxy,"Germany",lastaccess\]<br><br>**fuzzytime()** [触发器函数](/manual/appendix/triggers/functions) 可用于检查代理的可用性。<br>此监控项从Zabbix 2.4.0开始支持，该监控项始终由Zabbix服务器处理，无论主机位置如何（在服务器或代理服务器上）。|
|zabbix\[proxy\_history\]|<|<|<|<|<|
|<|代理服务器历史表中等待发送到服务器的值的数量。|<|整数|此监控项从Zabbix **2.2.0**开始支|。<br>*(不支持Zabbix server)*|
|zabbix\[queue,<from>,<to>\]|<|<|<|<|<|
|<|队列中被监视的监控项数量至少延迟了从<from>秒，但小于<to>秒.|<|整数|<from> - 默认: 6秒<br>|<<to> - 默认: 无限<br>[Time-unit symbols](/manual/appendix/suffixes) (s,m,h,d,w) 被这些参数支持<br>参数 `from` 和 `to` 从Zabbix **1.8.3.** 开始支持|
|zabbix\[rcache,<cache>,<mode>\]|<|<|<|<|<|
|<|Zabbix配置缓存的可用性统计信息|<|整数（大小）;浮点数（|分比）                                                                  缓存: **buffer**<br>|<Mode:<br>**total** - 缓冲区的总大小<br>**free** - 可用缓冲区大小<br>**pfree** - 可用缓存区百分比<br>**used** - 已用的缓存区大小|
|zabbix\[requiredperformance\]|<|<|<|<|<|
|<|Zabbix server或Zabbix proxy所需的性能，以每秒新增的值计算.|<|浮点数|与*Reports → [系统信息]|/manual/web_interface/frontend_sections/reports/status_of_zabbix)* 中的“所需服务器性能，每秒新值”大致相关。<br>此监控项从Zabbix **1.6.2.** 开始支持|
|zabbix\[trends\]|<|<|<|<|<|
|<|存储在TRENDS表中的数量值|<|整数|如果使用MySQL|nnoDB、Oracle或PostgreSQL，请勿使用！<br>*(代理服务器不支持)*|
|zabbix\[trends\_uint\]|<|<|<|<|<|
|<|存储在TRENDS\_UINT表中的数量值|<|整数|如果使用MySQL|nnoDB、Oracle或PostgreSQL，请勿使用！<br>此监控项从Zabbix **1.8.3.** 开始支持<br>*(代理服务器不支持)*|
|zabbix\[triggers\]|<|<|<|<|<|
|<|Zabbix数据库中启用的触发器数量，在启用的主机上启用所有的监控项|<|整数|*(代理服务器不支持)*|<|
|zabbix\[uptime\]|<|<|<|<|<|
|<|Zabbix server或zabbix proxy正常运行时间（秒）.|<|整数|<|<|
|zabbix\[vcache,buffer,<mode>\]|<|<|<|<|<|
|<|Zabbix值缓存的可用性统计信息|<|整数（大小）;浮点数|百分比）                                                                  模式:<br>|<**total** - 缓冲区的总大小<br>**free** - 可用缓冲区大小<br>**pfree** - 可用缓冲区百分比<br>**used** - 已用的缓冲区大小<br>**pused** - 已用的缓冲区百分比<br><br>此监控项从Zabbix **2.2.0**开始支持<br>*(代理服务器不支持)*|
|zabbix\[vcache,cache,<parameter>\]|<|<|<|<|<|
|<|Zabbix值缓存的有效性统计|<|整数<br>|参数:<br><br>使用**模式**参数:\                                                                             **hit0 - 正常模式,\                                                                                 **m1 - 低内存模式                                                                                 **mo|<**requests** - 总请求数量<br>** - 缓存命中数（从缓存中取出的历史值）<br>sses** - 高速缓存未命中数（从数据库获取的历史值）<br>e** - 值缓存操作模式<br>此监控项从Zabbix **2.2.0**开始支持， **模式** 参数从Zabbix **3.0.0**开始支持<br>*(代理服务器不支持)*<br><br>您可以使用这个键来进行 *每秒更改* 预处理步骤，以便获得每秒统计值。|
|zabbix\[vmware,buffer,<mode>\]|<|<|<|<|<|
|<|Zabbix vmware缓存的可用性统计信息|<|整数（大小）;浮点|（百分比）                                                                  模式:<br>|<**total** - 缓冲区的总大小<br>**free** - 可用缓冲区大小<br>**pfree** - 可用缓冲区百分比<br>**used** - 已用的缓冲区大小<br>**pused** - 已用的缓冲区百分比<br><br>此监控项从Zabbix **2.2.0**开始支持<br>|
|zabbix\[wcache,<cache>,<mode>\]|<|<|<|<|<|
|<|Zabbix写缓存的统计和可用性|<|<|必须指定<c|che>|
|<|**缓存**                                                                                                                                                                                                                                                                                                                                                                                                                                                               *|模式**|<|<|<|
|^|values|all<br>*(默认)*|由Zabbix server或Zabbix proxy处理的值的总数（不支持的监控项除外）   整数|计数器<br>您|<以使用这个键来进行 *每秒更改* 预处理步骤，以便获得每秒统计值。|
|^|^|float|处理的浮点值的数量.                                                 整数|计数器|<|
|^|^|uint|处理的无符号整数值的数量.                                           整数|计数器|<|
|^|^|str|处理的字符/字符串值的数量                                           整数|计数器|<|
|^|^|log|处理日志值的数量                                                    整数|计数器|<|
|^|^|text|已处理文本值的数量                                                  整数|计数器|<|
|^|^|not supported|项目处理导致监控项不受支持或保持该状态的次数.                       整数|计数器<br>|<*Not supported* 模式从Zabbix **1.8.6.**开始支持|
|^|history|pfree<br>*(默认)*|可用历史缓冲区的百分比.                                             浮点数|历史缓存用于存储监控项值。|比较低表示数据库端会有性能问题。|
|^|^|free|可用历史缓冲区大小                                                  整数|<|<|
|^|^|total|历史缓冲区总大小                                                    整数|<|<|
|^|^|used|已用的历史缓冲区大小                                                整数|<|<|
|^|index|pfree<br>*(默认)*|可用的历史索引缓冲区的百分比                                        浮点数|历史索引缓存用于索引存储在历史缓*|中的值。<br>引* 缓存从Zabbix **3.0.0**开始支持|
|^|^|free|可用历史索引缓冲区的大小                                            整数|<|<|
|^|^|total|历史记录索引缓冲区的总大小                                          整数|<|<|
|^|^|used|已用的历史索引缓冲区的大小                                          整数|<|<|
|^|trend|pfree<br>*(默认)*|可用趋势缓存的百分比                                                浮点数|趋势缓存存储接收数据的所*|监控项的当前小时的聚合。<br>代理服务器不支持)*|
|^|^|free|可用趋势缓存大小                                                    整数|*(代理服务器不支|)*|
|^|^|total|趋势缓存总大小                                                      整数|*(代理服务器不|持)*|
|^|^|used|已用的趋势缓存大小                                                  整数|*(代理服务器不支持|*|

[comment]: # ({/new-74b446f4})

[comment]: # ({new-0f6066ab})

### Item key details

-   Parameters without angle brackets are constants - for example,
    'host' and 'available' in `zabbix[host,<type>,available]`. Use them in the item key *as is*.
-   Values for items and item parameters that are "not supported on proxy" can only be retrieved if the host is monitored by server. And vice versa, values "not supported on server" can only be retrieved if the host is monitored by proxy.

[comment]: # ({/new-0f6066ab})

[comment]: # ({new-e52bbd1a})

##### zabbix[boottime] {#boottime}

<br>
The startup time of Zabbix server or Zabbix proxy process in seconds.<br>
Return value: *Integer*.

[comment]: # ({/new-e52bbd1a})

[comment]: # ({new-d1cde038})

##### zabbix[cluster,discovery,nodes] {#cluster.discovery}

<br>
Discovers the [high availability cluster](/manual/concepts/server/ha) nodes.<br>
Return value: *JSON object*.

This item can be used in low-level discovery.

[comment]: # ({/new-d1cde038})

[comment]: # ({new-09c4f0cf})

##### zabbix[connector_queue] {#connector.queue}

<br>
The count of values enqueued in the connector queue.<br>
Return value: *Integer*.

This item is supported since Zabbix 6.4.0.

[comment]: # ({/new-09c4f0cf})

[comment]: # ({new-fcd989ee})

##### zabbix[discovery_queue] {#discovery.queue}

<br>
The count of network checks enqueued in the discovery queue.<br>
Return value: *Integer*.

[comment]: # ({/new-fcd989ee})

[comment]: # ({new-f065c85f})

##### zabbix[host,,items] {#host.items}

<br>
The number of enabled items (supported and not supported) on the host.<br>
Return value: *Integer*.

[comment]: # ({/new-f065c85f})

[comment]: # ({new-31a0bdcc})

##### zabbix[host,,items_unsupported] {#host.items.unsupported}

<br>
The number of enabled unsupported items on the host.<br>
Return value: *Integer*.

[comment]: # ({/new-31a0bdcc})

[comment]: # ({new-6220db31})

##### zabbix[host,,maintenance] {#maintenance}

<br>
The current maintenance status of the host.<br>
Return values: *0* - normal state; *1* - maintenance with data collection; *2* - maintenance without data collection.

Comments:

-   This item is always processed by Zabbix server regardless of the host location (on server or proxy). The proxy will not receive this item with configuration data. 
-   The second parameter must be empty and is reserved for future use.

[comment]: # ({/new-6220db31})

[comment]: # ({new-ec7187d4})

##### zabbix[host,active_agent,available] {#active.available}

<br>
The availability of active agent checks on the host.<br>
Return values: *0* - unknown; *1* - available; *2* - not available.

[comment]: # ({/new-ec7187d4})

[comment]: # ({new-48ad1e26})

##### zabbix[host,discovery,interfaces] {#discovery.interfaces}

<br>
The details of all configured interfaces of the host in Zabbix frontend.<br>
Return value: *JSON object*.

Comments:

-   This item can be used in [low-level discovery](/manual/discovery/low_level_discovery/examples/host_interfaces);
-   This item is not supported on Zabbix proxy.

[comment]: # ({/new-48ad1e26})

[comment]: # ({new-9752a684})

##### zabbix[host,<type>,available] {#host.available}

<br>
The availability of the main interface of a particular type of checks on the host.<br>
Return values: *0* - not available; *1* - available; *2* - unknown.

Comments:

-   Valid **types** are: *agent*, *snmp*, *ipmi*, *jmx*;
-   The item value is calculated according to the configuration parameters regarding host [unreachability/unavailability](/manual/appendix/items/unreachability).

[comment]: # ({/new-9752a684})

[comment]: # ({new-a65ef5bf})

##### zabbix[hosts] {#hosts}

<br>
The number of monitored hosts.<br>
Return value: *Integer*.

[comment]: # ({/new-a65ef5bf})

[comment]: # ({new-f291ac50})

##### zabbix[items] {#items}

<br>
The number of enabled items (supported and not supported).<br>
Return value: *Integer*.

[comment]: # ({/new-f291ac50})

[comment]: # ({new-53295b6d})

##### zabbix[items_unsupported] {#items.unsupported}

<br>
The number of unsupported items.<br>
Return value: *Integer*.

[comment]: # ({/new-53295b6d})

[comment]: # ({new-ba37fff4})

##### zabbix[java,,<param>] {#java}

<br>
The information about Zabbix Java gateway.<br>
Return values: *1* - if <param> is *ping*; *Java gateway version* -  if <param> is *version* (for example: "2.0.0").

Comments:

-   Valid values for **param** are: *ping*, *version*;
-   This item can be used to check Java gateway availability using the `nodata()` trigger function;
-   The second parameter must be empty and is reserved for future use.

[comment]: # ({/new-ba37fff4})

[comment]: # ({new-8f8235d6})

##### zabbix[lld_queue] {#lld.queue}

<br>
The count of values enqueued in the low-level discovery processing queue.<br>
Return value: *Integer*.

This item can be used to monitor the low-level discovery processing queue length.

[comment]: # ({/new-8f8235d6})

[comment]: # ({new-3e07a93d})

##### zabbix[preprocessing_queue] {#preprocessing.queue}

<br>
The count of values enqueued in the preprocessing queue.<br>
Return value: *Integer*.

This item can be used to monitor the preprocessing queue length.

[comment]: # ({/new-3e07a93d})

[comment]: # ({new-b74b9537})

##### zabbix[process,<type>,<mode>,<state>] {#process}

<br>
The percentage of time a particular Zabbix process or a group of processes (identified by \<type\> and \<mode\>) spent in \<state\>. It is calculated for the last minute only.<br>
Return value: *Float*.

Parameters:

-   **type** - for [server processes](/manual/concepts/server#server_process_types): *alert manager*, *alert syncer*, *alerter*, *availability manager*, *configuration syncer*, *connector manager*, *connector worker*, *discoverer*, *escalator*, *history poller*, *history syncer*, *housekeeper*, *http poller*, *icmp pinger*, *ipmi manager*, *ipmi poller*, *java poller*, *lld manager*, *lld worker*, *odbc poller*, *poller*, *preprocessing manager*, *preprocessing worker*, *proxy poller*, *self-monitoring*, *snmp trapper*, *task manager*, *timer*, *trapper*, *unreachable poller*, *vmware collector*;<br>for [proxy processes](/manual/concepts/proxy#proxy_process_types): *availability manager*, *configuration syncer*, *data sender*, *discoverer*, *history syncer*, *housekeeper*, *http poller*, *icmp pinger*, *ipmi manager*, *ipmi poller*, *java poller*, *odbc poller*, *poller*, *preprocessing manager*, *preprocessing worker*, *self-monitoring*, *snmp trapper*, *task manager*, *trapper*, *unreachable poller*, *vmware collector*
-   **mode** - *avg* - average value for all processes of a given type (default)<br>*count* - returns number of forks for a given process type, <state> should not be specified<br>*max* - maximum value<br>*min* - minimum value<br>*<process number>* - process number (between 1 and the number of pre-forked instances). For example, if 4 trappers are running, the value is between 1 and 4.
-   **state** - *busy* - process is in busy state, for example, the processing request (default); *idle* - process is in idle state doing nothing.

Comments:

-   If \<mode\> is a Zabbix process number that is not running (for example, with 5 pollers running the \<mode\> is specified to be 6), such an item will turn unsupported;
-   Minimum and maximum refers to the usage percentage for a single process. So if in a group of 3 pollers usage percentages per process were 2, 18 and 66, min would return 2 and max would return 66.
-   Processes report what they are doing in shared memory and the self-monitoring process summarizes that data each second. State changes (busy/idle) are registered upon change - thus a process that becomes busy registers as such and doesn't change or update the state until it becomes idle. This ensures that even fully hung processes will be correctly registered as 100% busy.
-   Currently, "busy" means "not sleeping", but in the future additional states might be introduced - waiting for locks, performing database queries, etc.
-   On Linux and most other systems, resolution is 1/100 of a second.

Examples:

    zabbix[process,poller,avg,busy] #the average time of poller processes spent doing something during the last minute
    zabbix[process,"icmp pinger",max,busy] #the maximum time spent doing something by any ICMP pinger process during the last minute
    zabbix[process,"history syncer",2,busy] #the time spent doing something by history syncer number 2 during the last minute
    zabbix[process,trapper,count] #the amount of currently running trapper processes

[comment]: # ({/new-b74b9537})

[comment]: # ({new-d90bc125})

##### zabbix[proxy,<name>,<param>] {#proxy}

<br>
The information about Zabbix proxy.<br>
Return value: *Integer*.

Parameters:

-   **name** - the proxy name;
-   **param** - *delay* - how long the collected values are unsent, calculated as "proxy delay" (difference between the current proxy time and the timestamp of the oldest unsent value on proxy) + ("current server time" - "proxy lastaccess").

[comment]: # ({/new-d90bc125})

[comment]: # ({new-fd9ea3f4})

##### zabbix[proxy,discovery] {#proxy.discovery}

<br>
The list of Zabbix proxies with name, mode, encryption, compression, version, last seen, host count, item count, required values per second (vps) and version status (current/outdated/unsupported).<br>
Return value: *JSON object*.

[comment]: # ({/new-fd9ea3f4})

[comment]: # ({new-5b463b30})

##### zabbix[proxy_history] {#proxy.history}

<br>
The number of values in the proxy history table waiting to be sent to the server.<br>
Return values: *Integer*.

This item is not supported on Zabbix server.

[comment]: # ({/new-5b463b30})

[comment]: # ({new-ebe20dec})

##### zabbix[queue,<from>,<to>] {#queue}

<br>
The number of monitored items in the queue which are delayed at least by \<from\> seconds, but less than \<to\> seconds.<br>
Return value: *Integer*.

Parameters:

-   **from** - default: 6 seconds;
-   **to** - default: infinity.

[Time-unit symbols](/manual/appendix/suffixes) (s,m,h,d,w) are supported in the parameters.

[comment]: # ({/new-ebe20dec})

[comment]: # ({new-04aafd0c})

##### zabbix[rcache,<cache>,<mode>] {#rcache}

<br>
The availability statistics of the Zabbix configuration cache.<br>
Return values: *Integer* (for size); *Float* (for percentage).

Parameters:

-   **cache** - *buffer*;
-   **mode** - *total* - the total size of buffer<br>*free* - the size of free buffer<br>*pfree* - the percentage of free buffer<br>*used* - the size of used buffer<br>*pused* - the percentage of used buffer

[comment]: # ({/new-04aafd0c})

[comment]: # ({new-f2b37a90})

##### zabbix[requiredperformance] {#required.performance}

<br>
The required performance of Zabbix server or Zabbix proxy, in new values per second expected.<br>
Return value: *Float*.

Approximately correlates with "Required server performance, new values per second" in *Reports → [System information](/manual/web_interface/frontend_sections/reports/status_of_zabbix)*.

[comment]: # ({/new-f2b37a90})

[comment]: # ({new-8054c8ab})

##### zabbix[stats,<ip>,<port>] {#stats}

<br>
The internal metrics of a remote Zabbix server or proxy.<br>
Return values: *JSON object*.

Parameters:

-   **ip** - the IP/DNS/network mask list of servers/proxies to be remotely queried (default is 127.0.0.1);
-   **port** - the port of server/proxy to be remotely queried (default is 10051).

Comments:

-   The stats request will only be accepted from the addresses listed in the 'StatsAllowedIP' [server](/manual/appendix/config/zabbix_server)/[proxy](/manual/appendix/config/zabbix_proxy) parameter on the target instance;
-   A selected set of internal metrics is returned by this item. For details, see [Remote monitoring of Zabbix stats](/manual/appendix/items/remote_stats#exposed_metrics).

[comment]: # ({/new-8054c8ab})

[comment]: # ({new-d7c2c9be})

##### zabbix[stats,<ip>,<port>,queue,<from>,<to>] {#stats.queue}

<br>
The internal queue metrics (see `zabbix[queue,<from>,<to>]`) of a remote Zabbix server or proxy.<br>
Return values: *JSON object*.

Parameters:

-   **ip** - the IP/DNS/network mask list of servers/proxies to be remotely queried (default is 127.0.0.1);
-   **port** - the port of server/proxy to be remotely queried (default is 10051);
-   **from** - delayed by at least (default is 6 seconds);
-   **to** - delayed by at most (default is infinity).

Comments:

-   The stats request will only be accepted from the addresses listed in the 'StatsAllowedIP' [server](/manual/appendix/config/zabbix_server)/[proxy](/manual/appendix/config/zabbix_proxy) parameter on the target instance;
-   A selected set of internal metrics is returned by this item. For details, see [Remote monitoring of Zabbix stats](/manual/appendix/items/remote_stats#exposed_metrics).

[comment]: # ({/new-d7c2c9be})

[comment]: # ({new-c688d590})

##### zabbix[tcache,<cache>,<parameter>] {#tcache}

<br>
The effectiveness statistics of the Zabbix trend function cache.<br>
Return values: *Integer* (for size); *Float* (for percentage).

Parameters:

-   **cache** - *buffer*;
-   **mode** - **all* - total cache requests (default)<br>*hits* - cache hits<br>*phits* - percentage of cache hits<br>*misses* - cache misses<br>*pmisses* - percentage of cache misses<br>*items* - the number of cached items<br>*requests* - the number of cached requests<br>*pitems* - percentage of cached items from cached items + requests. Low percentage most likely means that the cache size can be reduced.

This item is not supported on Zabbix proxy.

[comment]: # ({/new-c688d590})

[comment]: # ({new-9699e4ca})

##### zabbix[triggers] {#triggers}

<br>
The number of enabled triggers in Zabbix database, with all items enabled on enabled hosts.<br>
Return value: *Integer*.

This item is not supported on Zabbix proxy.

[comment]: # ({/new-9699e4ca})

[comment]: # ({new-a5c8cdec})

##### zabbix[uptime] {#uptime}

<br>
The uptime of the Zabbix server or proxy process in seconds.<br>
Return value: *Integer*.

[comment]: # ({/new-a5c8cdec})

[comment]: # ({new-304cb09c})

##### zabbix[vcache,buffer,<mode>] {#vcache}

<br>
The availability statistics of the Zabbix value cache.<br>
Return values: *Integer* (for size); *Float* (for percentage).

Parameter:

-   **mode** - *total* - the total size of buffer<br>*free* - the size of free buffer<br>*pfree* - the percentage of free buffer<br>*used* - the size of used buffer<br>*pused* - the percentage of used buffer

This item is not supported on Zabbix proxy.

[comment]: # ({/new-304cb09c})

[comment]: # ({new-1454b994})

##### zabbix[vcache,cache,<parameter>] {#vcache.parameter}

<br>
The effectiveness statistics of the Zabbix value cache.<br>
Return values: *Integer*. With the *mode* parameter returns: 0 - normal mode; 1 - low memory mode.

Parameters:

-   **parameter** - *requests* - the total number of requests<br>*hits* - the number of cache hits (history values taken from the cache)<br>*misses* - the number of cache misses (history values taken from the database)<br>*mode* - the value cache operating mode

Comments:

-   Once the low-memory mode has been switched on, the value cache will remain in this state for 24 hours, even if the problem that triggered this mode is resolved sooner;
-   You may use this key with the *Change per second* preprocessing step in order to get values-per-second statistics;
-   This item is not supported on Zabbix proxy.

[comment]: # ({/new-1454b994})

[comment]: # ({new-ddde46a0})

##### zabbix[version] {#version}

<br>
The version of Zabbix server or proxy.<br>
Return value: *String*. For example: `6.0.0beta1`.

[comment]: # ({/new-ddde46a0})

[comment]: # ({new-dda0c008})

##### zabbix[vmware,buffer,<mode>] {#vmware}

<br>
The availability statistics of the Zabbix vmware cache.<br>
Return values: *Integer* (for size); *Float* (for percentage).

Parameters:

-   **mode** - *total* - the total size of buffer<br>*free* - the size of free buffer<br>*pfree* - the percentage of free buffer<br>*used* - the size of used buffer<br>*pused* - the percentage of used buffer

[comment]: # ({/new-dda0c008})

[comment]: # ({new-9fa76fcd})

##### zabbix[wcache,<cache>,<mode>] {#wcache}

<br>
The statistics and availability of the Zabbix write cache.<br>
Return values: *Integer* (for number/size); *Float* (for percentage).

Parameters:

-   **cache** - *values*, *history*, *index*, or *trend*;
-   **mode** - (with *values*) *all* (default) - the total number of values processed by Zabbix server/proxy, except unsupported items (counter)<br>*float* - the number of processed float values (counter)<br>*uint* - the number of processed unsigned integer values (counter)<br>*str* - the number of processed character/string values (counter)<br>*log* - the number of processed log values (counter)<br>*text* - the number of processed text values (counter)<br>*not supported* - the number of times item processing resulted in item becoming unsupported or keeping that state (counter)<br>(with *history*, *index*, *trend* cache) *pfree* (default) - the percentage of free buffer<br>*total* - the total size of buffer<br>*free* - the size of free buffer<br>*used* - the size of used buffer<br>*pused* - the percentage of used buffer

Comments:

-   Specifying \<cache\> is mandatory. The `trend` cache parameter is not supported with Zabbix proxy.
-   The history cache is used to store item values. A low number indicates performance problems on the database side.
-   The history index cache is used to index the values stored in the history cache;
-   The trend cache stores the aggregate for the current hour for all items that receive data;
-   You may use the zabbix[wcache,values] key with the *Change per second* preprocessing step in order to get values-per-second statistics.

[comment]: # ({/new-9fa76fcd})
