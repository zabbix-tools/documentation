[comment]: # translation:outdated

[comment]: # ({9d4ebdf2-c5a81a82})
# 1 动态索引

[comment]: # ({/9d4ebdf2-c5a81a82})

[comment]: # ({fd11159b-e95b21bb})
#### 概述

虽然你可能会在SNMP OID中找到所需的索引号（例如网络接口），但有时你不能完全依赖不变的索引号。

索引号可能是动态的 -它们可能会随时间而改变，因此你的监控项可能会停止工作。

为了避免这种情况，可以定义一个考虑到索引号改变的可能性的OID。

例如，如果需要检索索引值以匹配Cisco设备上的 **GigabitEthernet0/1**
接口的 **ifInOctets** ，请使用以下OID：

    ifInOctets["index","ifDescr","GigabitEthernet0/1"]

[comment]: # ({/fd11159b-e95b21bb})

[comment]: # ({b278afd3-6a6d7709})
##### 语法

使用OID的特殊语法：

**<OID of data>\["index","<base OID of index>","<string to search for>"\]**

|参数|描述 |
|----------------------------|-|
|OID of data|主OID用于监控项上的数据检索。|
|index |处理方法。目前支持一种方法：<br>**index** – 搜索索引，并将其附加到数据OID|
|base OID of index|该OID将被搜索以获取与该字符串对应的索引值。|
|string to search for|用于在进行查找时与值精确匹配的字符串。区分大小写。|

[comment]: # ({/b278afd3-6a6d7709})

[comment]: # ({659a87e4-3da26466})
#### 示例

获取 *apache* 进程的内存使用率。

如果使用这种OID语法:

    HOST-RESOURCES-MIB::hrSWRunPerfMem["index","HOST-RESOURCES-MIB::hrSWRunPath", "/usr/sbin/apache2"]

索引号将在这里查找:

    ...
    HOST-RESOURCES-MIB::hrSWRunPath.5376 = STRING: "/sbin/getty"
    HOST-RESOURCES-MIB::hrSWRunPath.5377 = STRING: "/sbin/getty"
    HOST-RESOURCES-MIB::hrSWRunPath.5388 = STRING: "/usr/sbin/apache2"
    HOST-RESOURCES-MIB::hrSWRunPath.5389 = STRING: "/sbin/sshd"
    ...

现在我们有索引，5388.索引将附加到此数据OID，以便接收我们需要的值：

    HOST-RESOURCES-MIB::hrSWRunPerfMem.5388 = INTEGER: 31468 KBytes

[comment]: # ({/659a87e4-3da26466})

[comment]: # ({48f52698-beab1c6d})
#### 索引查找缓存

当请求动态索引项时，Zabbix检索并缓存基础OID下的整个SNMP表用于索引（即使早发现了匹配）。这是为了在另一个监控项稍后引用相同的基础OID -Zabbix将在缓存中查找索引，而不是再次查询被监视的主机。请注意，每个轮询器进程使用单独的缓存。

在所有随后的值检索操作中，仅验证找到的索引。如果没有改变将，则请求结果值；如果已更改，则会重建高速缓存- 遇到已更改索引的每个轮询器再次建立SNMP索引表。

[comment]: # ({/48f52698-beab1c6d})
