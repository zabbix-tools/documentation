[comment]: # translation:outdated

[comment]: # ({da9af4a4-8afcd82f})
# 3 MIB文件

[comment]: # ({/da9af4a4-8afcd82f})

[comment]: # ({9a9531d4-f224bb08})
＃＃＃＃ 介绍

MIB 代表管理信息库。 MIB 文件允许您使用OID（对象标识符）的文本表示。

例如，

    ifHCOutOctets

是 OID 的文本表示

    1.3.6.1.2.1.31.1.1.1.10

在使用 Zabbix 监控 SNMP 设备时，您可以使用其中任何一种，但如果您使用文本表示时感觉更舒服，你必须安装 MIB 文件。

[comment]: # ({/9a9531d4-f224bb08})

[comment]: # ({4d049318-5aed9e1b})
#### 安装 MIB 文件

在基于 Debian 的系统上：

    # apt install snmp-mibs-downloader
    # 下载-mibs

在基于 RedHat 的系统上：

    # yum install net-snmp-libs

[comment]: # ({/4d049318-5aed9e1b})

[comment]: # ({3bca4271-5346f669})
#### 启用 MIB 文件

在基于 RedHat 的系统上，默认情况下应该启用 mib 文件。在基于 Debian 的系统，您必须编辑文件 `/etc/snmp/snmp.conf` 和注释掉 `mibs :` 的行

    # 由于许可原因，snmp 软件包没有 MIB 文件，默认情况下禁用 MIB 加载。如果您添加了 MIB，您可以重新启用
    # 通过注释掉以下行来加载它们。
    #mibs：

[comment]: # ({/3bca4271-5346f669})

[comment]: # ({d8162e2e-bb4c3c1f})
#### 测试 MIB 文件

可以使用 `snmpwalk` 实用程序来测试 snmp MIB。如果你没有安装，请使用以下说明。

在基于 Debian 的系统上：

    # api install snmp

在基于 RedHat 的系统上：

    # yum install net-snmp-utils

之后，查询网络设备时，以下命不会给出错误:

    $ snmpwalk -v 2c -c public <网络设备 IP> ifInOctets
    IF-MIB::ifInOctets.1 = Counter32: 176137634
    IF-MIB::ifInOctets.2 = Counter32: 0
    IF-MIB::ifInOctets.3 = Counter32: 240375057
    IF-MIB::ifInOctets.4 = Counter32: 220893420
    [...]

[comment]: # ({/d8162e2e-bb4c3c1f})

[comment]: # ({b31916c6-87dbd7d4})
#### 在 Zabbix 中使用 MIB

最重要的是要记住 Zabbix 进程不会得到通知对 MIB 文件所做的更改。所以每次改变后你必须重新启动 Zabbix server 或Zabbix proxy，例如：

    # service zabbix-server restart

重启zabbix服务后，对 MIB 文件所做的更改生效。

[comment]: # ({/b31916c6-87dbd7d4})

[comment]: # ({684dd53c-efd836b8})
#### 使用自定义 MIB 文件

每个 GNU/Linux 发行版都有标准的 MIB 文件。
但是一些设备供应商提供他们自己的。

假设您想使用[CISCO-SMI](ftp://ftp.cisco.com/pub/mibs/v2/CISCO-SMI.my) MIB 文件。这
以下说明将下载并安装它：

    # wget ftp://ftp.cisco.com/pub/mibs/v2/CISCO-SMI.my -P /tmp
    # mkdir -p /usr/local/share/snmp/mibs
    # grep -q '^mibdirs +/usr/local/share/snmp/mibs' /etc/snmp/snmp.conf 2>/dev/null || echo "mibdirs +/usr/local/share/snmp/mibs" >> /etc/snmp/snmp.conf
    # cp /tmp/CISCO-SMI.my /usr/local/share/snmp/mibs

现在你应该可以使用它了。试着翻译一下名字
对象 *ciscoProducts* 从 MIB 文件到 OID：

    # snmptranslate -IR -On CISCO-SMI::ciscoProducts
    .1.3.6.1.4.1.9.1

如果您收到错误而不是 OID，请确保之前的所有命令没有返回任何错误。

对象名称翻译成功，您可以使用自定义 MIB文件。请注意查询中使用的 MIB 名称前缀 (*CISCO-SMI::*)。你在使用命令行工具以及 Zabbix 时将需要这个。

Zabbix使用这个 MIB 文件之前不要忘记重启 Zabbix server/proxy服务。

::: 重要提示
请记住，MIB 文件可以具有依赖关系。
也就是说，一个 MIB 可能需要另一个 MIB。为了满足这些您必须安装所有受影响的 MIB 的依赖项文件。
:::

[comment]: # ({/684dd53c-efd836b8})
