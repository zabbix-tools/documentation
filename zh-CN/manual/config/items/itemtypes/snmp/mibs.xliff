<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="zh-CN" datatype="plaintext" original="manual/config/items/itemtypes/snmp/mibs.md">
    <body>
      <trans-unit id="8afcd82f" xml:space="preserve">
        <source># 3 MIB files</source>
        <target state="needs-translation"># 3 MIB文件</target>
      </trans-unit>
      <trans-unit id="f224bb08" xml:space="preserve">
        <source>#### Introduction

MIB stands for the Management Information Base. MIB files allow to use textual representation of an OID (Object Identifier).
It is possible to use raw OIDs when monitoring SNMP devices with Zabbix,
but if you feel more comfortable using textual representation, you need to install MIB files.

For example,

    ifHCOutOctets

is textual representation of the OID

    1.3.6.1.2.1.31.1.1.1.10</source>
        <target state="needs-translation">＃＃＃＃ 介绍

MIB 代表管理信息库。 MIB 文件允许您使用OID（对象标识符）的文本表示。

例如，

    ifHCOutOctets

是 OID 的文本表示

    1.3.6.1.2.1.31.1.1.1.10

在使用 Zabbix 监控 SNMP 设备时，您可以使用其中任何一种，但如果您使用文本表示时感觉更舒服，你必须安装 MIB 文件。</target>
      </trans-unit>
      <trans-unit id="5aed9e1b" xml:space="preserve">
        <source>#### Installing MIB files

On Debian-based systems:

    apt install snmp-mibs-downloader
    download-mibs

On RedHat-based systems:

    dnf install net-snmp-libs</source>
        <target state="needs-translation">#### 安装 MIB 文件

在基于 Debian 的系统上：

    # apt install snmp-mibs-downloader
    # 下载-mibs

在基于 RedHat 的系统上：

    # yum install net-snmp-libs</target>
      </trans-unit>
      <trans-unit id="5346f669" xml:space="preserve">
        <source>#### Enabling MIB files

On RedHat-based systems, MIB files should be enabled by default.
On Debian-based systems, you have to edit the file `/etc/snmp/snmp.conf` and comment out the line that says `mibs :`

    # As the snmp packages come without MIB files due to license reasons, loading
    # of MIBs is disabled by default. If you added the MIBs you can re-enable
    # loading them by commenting out the following line.
    mibs :</source>
        <target state="needs-translation">#### 启用 MIB 文件

在基于 RedHat 的系统上，默认情况下应该启用 mib 文件。在基于 Debian 的系统，您必须编辑文件 `/etc/snmp/snmp.conf` 和注释掉 `mibs :` 的行

    # 由于许可原因，snmp 软件包没有 MIB 文件，默认情况下禁用 MIB 加载。如果您添加了 MIB，您可以重新启用
    # 通过注释掉以下行来加载它们。
    #mibs：</target>
      </trans-unit>
      <trans-unit id="bb4c3c1f" xml:space="preserve">
        <source>#### Testing MIB files

Testing SNMP MIBs can be done using `snmpwalk` utility. If you don't have it installed, use the following instructions.

On Debian-based systems:

    apt install snmp

On RedHat-based systems:

    dnf install net-snmp-utils

After that, the following command must not give error when you query a
network device:

    $ snmpwalk -v 2c -c public &lt;NETWORK DEVICE IP&gt; ifInOctets
    IF-MIB::ifInOctets.1 = Counter32: 176137634
    IF-MIB::ifInOctets.2 = Counter32: 0
    IF-MIB::ifInOctets.3 = Counter32: 240375057
    IF-MIB::ifInOctets.4 = Counter32: 220893420
    [...]</source>
        <target state="needs-translation">#### 测试 MIB 文件

可以使用 `snmpwalk` 实用程序来测试 snmp MIB。如果你没有安装，请使用以下说明。

在基于 Debian 的系统上：

    # api install snmp

在基于 RedHat 的系统上：

    # yum install net-snmp-utils

之后，查询网络设备时，以下命不会给出错误:

    $ snmpwalk -v 2c -c public &lt;网络设备 IP&gt; ifInOctets
    IF-MIB::ifInOctets.1 = Counter32: 176137634
    IF-MIB::ifInOctets.2 = Counter32: 0
    IF-MIB::ifInOctets.3 = Counter32: 240375057
    IF-MIB::ifInOctets.4 = Counter32: 220893420
    [...]</target>
      </trans-unit>
      <trans-unit id="87dbd7d4" xml:space="preserve">
        <source>#### Using MIBs in Zabbix

The most important to keep in mind is that Zabbix processes do not get
informed of the changes made to MIB files. So after every change you
must restart Zabbix server or proxy, e. g.:

    service zabbix-server restart

After that, the changes made to MIB files are in effect.</source>
        <target state="needs-translation">#### 在 Zabbix 中使用 MIB

最重要的是要记住 Zabbix 进程不会得到通知对 MIB 文件所做的更改。所以每次改变后你必须重新启动 Zabbix server 或Zabbix proxy，例如：

    # service zabbix-server restart

重启zabbix服务后，对 MIB 文件所做的更改生效。</target>
      </trans-unit>
      <trans-unit id="efd836b8" xml:space="preserve">
        <source>#### Using custom MIB files

There are standard MIB files coming with every GNU/Linux distribution.
But some device vendors provide their own.

Let's say, you would like to use
[CISCO-SMI](ftp://ftp.cisco.com/pub/mibs/v2/CISCO-SMI.my) MIB file. The
following instructions will download and install it:

    wget ftp://ftp.cisco.com/pub/mibs/v2/CISCO-SMI.my -P /tmp
    mkdir -p /usr/local/share/snmp/mibs
    grep -q '^mibdirs +/usr/local/share/snmp/mibs' /etc/snmp/snmp.conf 2&gt;/dev/null || echo "mibdirs +/usr/local/share/snmp/mibs" &gt;&gt; /etc/snmp/snmp.conf
    cp /tmp/CISCO-SMI.my /usr/local/share/snmp/mibs

Now you should be able to use it. Try to translate the name of the
object *ciscoProducts* from the MIB file to OID:

    snmptranslate -IR -On CISCO-SMI::ciscoProducts
    .1.3.6.1.4.1.9.1

If you receive errors instead of the OID, ensure all the previous
commands did not return any errors.

The object name translation worked, you are ready to use custom MIB
file. Note the MIB name prefix (*CISCO-SMI::*) used in the query. You
will need this when using command-line tools as well as Zabbix.

Don't forget to restart Zabbix server/proxy before using this MIB file
in Zabbix.

::: noteimportant
Keep in mind that MIB files can have dependencies.
That is, one MIB may require another. In order to satisfy these
dependencies you have to install all the affected MIB
files.
:::</source>
        <target state="needs-translation">#### 使用自定义 MIB 文件

每个 GNU/Linux 发行版都有标准的 MIB 文件。
但是一些设备供应商提供他们自己的。

假设您想使用[CISCO-SMI](ftp://ftp.cisco.com/pub/mibs/v2/CISCO-SMI.my) MIB 文件。这
以下说明将下载并安装它：

    # wget ftp://ftp.cisco.com/pub/mibs/v2/CISCO-SMI.my -P /tmp
    # mkdir -p /usr/local/share/snmp/mibs
    # grep -q '^mibdirs +/usr/local/share/snmp/mibs' /etc/snmp/snmp.conf 2&gt;/dev/null || echo "mibdirs +/usr/local/share/snmp/mibs" &gt;&gt; /etc/snmp/snmp.conf
    # cp /tmp/CISCO-SMI.my /usr/local/share/snmp/mibs

现在你应该可以使用它了。试着翻译一下名字
对象 *ciscoProducts* 从 MIB 文件到 OID：

    # snmptranslate -IR -On CISCO-SMI::ciscoProducts
    .1.3.6.1.4.1.9.1

如果您收到错误而不是 OID，请确保之前的所有命令没有返回任何错误。

对象名称翻译成功，您可以使用自定义 MIB文件。请注意查询中使用的 MIB 名称前缀 (*CISCO-SMI::*)。你在使用命令行工具以及 Zabbix 时将需要这个。

Zabbix使用这个 MIB 文件之前不要忘记重启 Zabbix server/proxy服务。

::: 重要提示
请记住，MIB 文件可以具有依赖关系。
也就是说，一个 MIB 可能需要另一个 MIB。为了满足这些您必须安装所有受影响的 MIB 的依赖项文件。
:::</target>
      </trans-unit>
    </body>
  </file>
</xliff>
