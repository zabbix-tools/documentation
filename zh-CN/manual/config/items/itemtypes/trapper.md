[comment]: # translation:outdated

[comment]: # ({7d4ff779-5e482050})
# 13 采集器监控项

[comment]: # ({/7d4ff779-5e482050})

[comment]: # ({613a2864-03e7aedb})
#### 概述

采集器监控项接收传入的数据,它不会去主动采集数据.

它接受任何形式的推送到zabbix server的数据.

要使用采集器监控项，你需要：

-   在Zabbix中建立一个采集器监控项
-   将数据发送到Zabbix server

[comment]: # ({/613a2864-03e7aedb})

[comment]: # ({41d27374-5d32b87c})
#### 配置

[comment]: # ({/41d27374-5d32b87c})

[comment]: # ({8f03922a-62c22fbb})
#####项目配置

要配置采集器监控项：

- 转到：*配置* → *主机*
- 点击主机所在行的*Items*
- 点击*创建监控项*
- 在表单中输入监控项的参数

![](../../../../../assets/en/manual/config/items/itemtypes/trapper_item.png)

所有必填字段都标有红色星号.

需要填写采集器特定信息的字段是：

| | |
|---|---|
|*Type*|在此处选择**采集器**.|
|*Key*|输入发送数据时用于识别项目的监控项键.|
|*Type of information*|选择与将要发送的数据格式相对应的信息类型.|
|*Allowed hosts*|以逗号分隔的 IP 地址列表，可选择以 CIDR 表示法或主机名。<br>如果指定，则仅接受来自此处列出的主机的传入连接.<br>如果启用 IPv6 支持，则为 '127.0 .0.1'、'::127.0.0.1'、'::ffff:127.0.0.1' 被平等对待，'::/0' 将允许任何 IPv4 或 IPv6 地址。<br>'0.0.0.0/0' 可以用于允许任何 IPv4 地址.<br>请注意，"与 IPv4 兼容的 IPv6 地址"（0000::/96 前缀）受支持，但已被 [RFC4291](https://tools.ietf.org/html/ rfc4291#section-2.5.5).<br>示例：Server=127.0.0.1, 192.168.1.0/24, 192.168.3.1-255, 192.168.1-10.1-255, ::1,2001:db8::/ 32、zabbix.domain<br>从Zabbix 2.2.0开始,该字段允许使用空格和[用户宏](/manual/config/macros/user_macros).<br>主机宏：{HOST.HOST}, {HOST.自 Zabbix 4.0.2 起，NAME}、{HOST.IP}、{HOST.DNS}、{HOST.CONN} 允许在此字段中.|

::: 注意事项
在保存该项后,您可能需要等待60秒,直到服务器的缓存配置更新到最新的内容后,才能发送监控值
:::

[comment]: # ({/8f03922a-62c22fbb})

[comment]: # ({97467512-5f246209})
##### 数据发送

在最简单的情况下，我们可以使用 [zabbix\_sender](/manual/concepts/sender)
程序来发送一些“测试值”：

    zabbix_sender -z <server IP address> -p 10051 -s "New host" -k trap -o "test value"

我们使用下列这些键来发送值

*-z* - 指定Zabbix server的IP地址

*-p* - 指定Zabbix server的端口（默认为10051）

*-s* -指定主机（请确保在此使用“主机名称”的
[主机名](/manual/config/hosts/host#configuration) ，而不是“可见的名称”名称）

*-k* - 指定我们之前定义的监控项的键值

*-o* - 指定要发送的实际值

::: noteimportant Zabbix 采集器进程不会扩展监控项键值中使用的宏，以检查目标主机对应的监控项键值是否存在。
:::

[comment]: # ({/97467512-5f246209})

[comment]: # ({b2d80ac7-1e9d760a})
##### 展示

这是 *监控 → 最新数据 * 中的结果：

![](../../../../../assets/en/manual/config/items/itemtypes/trapped_data.png){width="600"}

请注意,如果传入一个数值,数据图将在该值的时间点的左边和右边显示一条水平线.

[comment]: # ({/b2d80ac7-1e9d760a})
