[comment]: # translation:outdated

[comment]: # ({49f474a1-622befb9})
# 3 SNMP 陷阱

[comment]: # ({/49f474a1-622befb9})

[comment]: # ({1c8b81ad-ff07904b})
＃＃＃＃ 概述

接收 SNMP 陷阱与查询启用 SNMP 的设备相反。

在这种情况下，信息是从支持 SNMP 的设备发送的，并且是被 Zabbix 收集或“捕获”。

通常，在某些条件更改时会发送陷阱，并且客户端的 162端口 连接到服务器（与用于查询客户端的端口 161 不同）。使用陷阱可能会检测到在查询间隔中发生的一些短期问题，并且可能会遗漏查询数据。

在 Zabbix 中接收 SNMP 陷阱要与 **snmptrapd** 一起使用，以及将snmp陷阱传递给 Zabbix 的内置机制Perl 脚本或 SNMPTT之一 -。

接收陷阱的工作流程：

1. **snmptrapd** 收到陷阱
2. snmptrapd 将陷阱传递给 SNMPTT 或调用 Perl 陷阱接收器
3. SNMPTT 或 Perl 陷阱接收器解析、格式化并将陷阱写入到一份文件
4、Zabbix SNMP trapper读取并解析trap文件
5. 对于每个陷阱，Zabbix 找到所有带有主机的“SNMP 陷阱”项与接收到的陷阱地址匹配的接口。请注意，只有在主机接口中选择的“IP”或“DNS”在使用期间匹配。
6. 对于每个找到的项目，将陷阱与 regexp 进行比较 “snmptrap\[正则表达式\]”。陷阱设置为 **all** 的值 匹配的监控项。如果没有找到匹配的项目并且有“snmptrap.fallback”项，陷阱设置为那个值。
7.如果trap没有设置为任何item的值，Zabbix默认 记录不匹配的陷阱。 （这是由“记录不匹配的 SNMP陷阱”在管理 → 常规 → 其他中。）

[comment]: # ({/1c8b81ad-ff07904b})

[comment]: # ({6e69edf1-a8df124c})
#### - 配置 SNMP 陷阱

在前端配置以下字段是特定于该监控项类型：

- 您的主机必须有 SNMP 接口

在 *Configuration → Hosts* 中，在 **Host interface** 字段中填入正确的 IP 或 DNS 地址设置 SNMP 接口。将每个接收到的陷阱的地址与所有 SNMP 接口的 IP 和 DNS 地址进行比较，以找到相应的主机。

- 配置监控项

在 **Key** 字段中，使用 SNMP 陷阱key之一：

|key|<|<|
|---|-|-|
|描述|返回值|评论|
|snmptrap\[正则表达式\]|<|<|
|捕获与 **regexp** 中指定的 [正则表达式](/manual/regular_expressions) 匹配的所有 SNMP 陷阱。如果未指定 regexp，则捕获任何陷阱。|SNMP 陷阱|此项只能为 SNMP 接口设置。<br>自 Zabbix **2.0.0 起支持此项。**<br>*注意*：从 Zabbix 开始2.0.5，此项key的参数中支持用户宏和全局正则表达式。
|snmptrap.fallback|<|<|
|捕获该接口的任何 snmptrap\[\] 项未捕获的所有 SNMP 陷阱。|SNMP 陷阱|只能为 SNMP 接口设置此项目。<br>自 Zabbix **2.0 起支持此项目。 0.**|

::: 经典笔记
此处不支持多行正则表达式匹配。
:::

将 **Type of information** 设置为 'Log' 以使时间戳为解析。请注意，也可以接受其他格式，例如“数字”但可能需要自定义陷阱处理程序。

::: 提示
要使 SNMP 陷阱监控工作，它必须首先正确设置。
:::

[comment]: # ({/6e69edf1-a8df124c})

[comment]: # ({a872b89e-a08bce2a})
#### - 设置SNMP trap监控

[comment]: # ({/a872b89e-a08bce2a})

[comment]: # ({cb38685e-581aa946})
##### 配置 Zabbix Server/Proxy

要读取trap，必须将ZabbixZabbix Server/Proxy配置为启动SNMPtrap进程，并指向由SNMPTT或perl trap接收器写入的trap文件。为此，请编辑配置文件
([zabbix\_server.conf](/zh/manual/appendix/config/zabbix_server)或者
 [zabbix\_proxy.conf](/zh/manual/appendix/config/zabbix_proxy))：

1.  StartSNMPTrapper=1
2.  SNMPTrapperFile=\[TRAP FILE\]

告警提示
如果使用系统参数
**[PrivateTmp](http://www.freedesktop.org/software/systemd/man/systemd.exec.html#PrivateTmp=)**，则该文件不能在*/tmp*下使用。
:::

[comment]: # ({/cb38685e-581aa946})

[comment]: # ({f9caed7e-af1e38f9})
##### 配置 SNMPTT

首先，snmptrapd 应该配置为使用 SNMPTT。

::: 提示
为获得最佳性能，SNMPTT 应配置使用 **snmptthandler-embedded** 将陷阱传递给它的守护进程。在其主页中有配置 SNMPTT 的说明：
<http://snmptt.sourceforge.net/docs/snmptt.shtml>
:::

当 SNMPTT 配置为接收陷阱时，配置 `snmptt.ini`：

1. 启用 NET-SNMP 包中的 Perl 模块：
    nei_snmp_perl_enable = 1
2. 将陷阱记录到 Zabbix 将读取的陷阱文件中：
    log_enable = = 1
    log_file = [陷阱文件]
3.设置日期时间格式：
    date_time_format = %H:%M:%S %Y/%m/%d = [DATE TIME FORMAT]

::: 注意警告
“net-snmp-perl”包已在RHEL/CentOS 8.0-8.2；在 RHEL 8.3 中重新添加。有关详细信息，请参阅
[已知问题]（/manual/installation/known_issues#snmp_traps）。
:::

现在格式化陷阱以便 Zabbix 识别它们（编辑 snmptt.conf）：

1.每个 格式化语句应以“ZBXTRAP [address]”开头，其中[address] 将与 Zabbix 上 SNMP 接口的 IP 和 DNS 地址进行比较。例如： 
 EVENT coldStart .1.3.6.1.6.3.1.1.5.1 "Status Events" Normal FORMAT ZBXTRAP $aA Device reinitialized (coldStart)
2. 在下面查看更多关于 SNMP 陷阱格式的信息。

::: 注意重要
不要使用未知陷阱 - Zabbix 将无法认出他们。未知的陷阱可以通过定义一个通用的来处理
snmptt.conf 中的事件：
EVENT general .* "General event" Normal
:::

[comment]: # ({/f9caed7e-af1e38f9})

[comment]: # ({5058b06e-0aa82990})
##### 配置 Perl trap 接收器

要求：Perl，Net-SNMP使用--enable-embedded-perl编译（默认情况下从Net-SNMP
5.4支持）
Perl trap接收器（查找misc/snmptrap/zabbix\_trap\_receiver.pl）可以直接从snmptrapd将trap传递给Zabbix服务器。配置过程:

-   将perl脚本添加到snmptrapd配置文件（snmptrapd.conf）中，例如：
    perl do "[FULL PATH TO PERL RECEIVER SCRIPT]";
-   配置接收器, 例如:
    $SNMPTrapperFile = '[TRAP FILE\]';
    $DateTimeFormat = '[DATE TIME FORMAT]';

::: 提示
如果没有引用脚本名称，snmptrapd将拒绝启动消息，类似:
SNMPv3/USM.

[comment]: # ({/5058b06e-0aa82990})

[comment]: # ({74a6a793-cd001892})
##### SNMP trap 格式

所有定制的perl trap接收器和SNMPTT trap配置必须按以下方式格式化trap：
**[timestamp] \[the trap, part 1] ZBXTRAP [address] [the trap,
part 2]**, 说明

-   [timestamp] - 用于日志监控项的时间戳
-   ZBXTRAP - 头表示新的trap从此行开始
-   [address] - 用于查找此trap的主机的IP地址

注意，“ZBXTRAP”和“[address]”将在处理过程中从消息中删除。如果trap格式化为其它方式，Zabbix也许能意外的解析trap。

trap示例:
11:30:15 2011/07/27 .1.3.6.1.6.3.1.1.5.3 Normal "Status Events"
localhost - ZBXTRAP 192.168.1.1 Link down on interface 2. Admin state:
1. Operational state: 2
This will result in the following trap for SNMP interface with IP=192.168.1.1:
11:30:15 2011/07/27 .1.3.6.1.6.3.1.1.5.3 Normal "Status Events" localhost - Link down on interface 2. Admin state: 1.

[comment]: # ({/74a6a793-cd001892})

[comment]: # ({dabab73e-357f1824})
#### - 系统要求

[comment]: # ({/dabab73e-357f1824})

[comment]: # ({2d79f0c9-6245c6ae})
##### 大文件支持

Zabbix为SNMP trap文件提供了“大文件支持”。Zabbix可以读取的最大文件大小为2^63（8
EiB）。请注意，文件系统可能会对文件大小添加下限。

[comment]: # ({/2d79f0c9-6245c6ae})

[comment]: # ({90703e5b-0f27a9bb})
##### 日志轮换

Zabbix不提供任何日志轮换系统（它应由用户处理）。
日志轮换应该首先重命名旧文件，然后才能将其删除，以免丢失trap：
  - Zabbix在最后一个已知位置打开trap文件，并转到步骤3
  - Zabbix通过比较inode号和定义trap文件的inode号，检查当前打开的文件是否已经轮换。如果没有打开的文件，Zabbix将重置最后一个位置并转到步骤1。
  - Zabbix从当前打开的文件中读取数据并设置新的位置。
  - 新数据被解析。如果这是轮换的文件，文件将关闭并返回到步骤2。
  - 如果没有新的数据，Zabbix 等待1秒钟后，然后回到步骤2。

[comment]: # ({/90703e5b-0f27a9bb})

[comment]: # ({5a17c0e4-608a14bd})
##### 文件系统

由于Trap文件的执行，Zabbix需要文件系统支持inode来区分文件（该信息由stat()调用获取）。

[comment]: # ({/5a17c0e4-608a14bd})

[comment]: # ({602f5c18-ee69482d})
#### - 设置示例

此示例使用 snmptrapd + SNMPTT 将陷阱传递给 Zabbix 服务器。
设置：

1. **zabbix\_server.conf** - 配置 Zabbix 启动 SNMP Trapper 和
    设置陷阱文件：
    启动SNMPTrapper=1
    SNMPTrapperFile=/tmp/my_zabbix_traps.tmp
2. **snmptrapd.conf** - 添加 SNMPTT 作为陷阱处理程序：
    陷阱句柄默认 snmptt
3. **snmptt.ini** -启用 NET-SNMP 包中的 Perl 模块：
    net_snmp_perl_enable = 1
    配置输出文件和时间格式：
    log_file = /tmp/my_zabbix_traps.tmp
    date_time_format = %H:%M:%S %Y/%m/%d
4. **snmptt.conf** - 定义默认陷阱格式：
    EVENT general .* "General event" Normal
    FORMAT ZBXTRAP $aA $ar
5. 创建一个 SNMP 项 TEST:
    主机的SNMP接口IP：127.0.0.1
    键：snmptrap[“General”]
    日志时间格式：hh:mm:ss yyyy/MM/dd

返回结果：

1. 用于发送陷阱的命令：
    snmptrap -v 1 -c 公共 127.0.0.1 '.1.3.6.1.6.3.1.1.5.3' '0.0.0.0' 6
    33 '55' .1.3.6.1.6.3.1.1.5.3 s "teststring000"
2.收到的trap：
    15:48:18 2011/07/26 .1.3.6.1.6.3.1.1.5.3.0.33 正常“一般事件”
    本地主机 - ZBXTRAP 127.0.0.1 127.0.0.1
3. 项目 TEST 的值：
    15:48:18 2011/07/26 .1.3.6.1.6.3.1.1.5.3.0.33 正常“一般事件”
    本地主机 - 127.0.0.1

::: 提示
这个简单的示例使用 SNMPTT 作为 **traphandle**。为了在生产系统上性能更好，使用嵌入式 Perl 来通过从 snmptrapd 到 SNMPTT 或直接到 Zabbix 的陷阱。
:::

[comment]: # ({/602f5c18-ee69482d})

[comment]: # ({e4b40ca1-75026475})
#### - 请参阅

-   [来自zabbix.org的基于CentOS的SNMP trap教程](https://www.zabbix.org/wiki/Start_with_SNMP_traps_in_Zabbix)

[comment]: # ({/e4b40ca1-75026475})
