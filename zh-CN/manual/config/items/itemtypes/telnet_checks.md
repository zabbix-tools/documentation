[comment]: # translation:outdated

[comment]: # ({fd6ca41e-9e60926e})
# 10 Telnet检查

[comment]: # ({/fd6ca41e-9e60926e})

[comment]: # ({770cf123-2cd46777})
#### 概述

执行Telnet检查无需客户端.Telnet检查不需要Zabbix客户端.

[comment]: # ({/770cf123-2cd46777})

[comment]: # ({38d31118-3db6c676})
#### 可配置字段

要执行的实际命令必须放在项目配置中的 **Executed script**  字段中。
通过将多个命令放在一行上，可以遍历执行多个命令,在这种情况下,返回值也将被格式化为多行.


支持的 shell 提示符结尾的字符：

- $
- \#
- >
- %

::: 注意
以上结束符的telnet提示行将从返回值中删除,但仅针对命令列表中的第一个命令,即仅在telnet会话开始时.
:::

|关键字|描述|评论|
|---|-----------|--------|
|**telnet.run\[<unique short description>,<ip>,<port>,<encoding>\]**|使用 telnet 连接在远程设备上运行命令|<|

::: 注意重要
如果telnet检查返回一个非ascii字符和非utf8编码的值,那应该正确的指定<编码>参数.有关详细信息.请参阅[编码返回值](/manual/appendix/items/encoding_of_values) 页面.
:::

[comment]: # ({/38d31118-3db6c676})
