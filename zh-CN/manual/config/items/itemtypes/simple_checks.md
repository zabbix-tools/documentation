[comment]: # attributes: notoc

[comment]: # translation:outdated

[comment]: # ({new-be4ab03d})
# 5 简单检查

[comment]: # ({/new-be4ab03d})

[comment]: # ({new-76023448})
#### Overview

[comment]: # ({/new-76023448})

[comment]: # ({new-f59a5ccc})
#### 概览

简单检查通常用于检查远程未安装Zabbix agent的服务。

请注意，简单检查不需要Zabbix agent，由Zabbix server和Zabbix
proxy来负责处理（例如创建外部连接等）。

简单检查使用示例：

    net.tcp.service[ftp,,155]
    net.tcp.service[http]
    net.tcp.service.perf[http,,8080]
    net.udp.service.perf[ntp]

::: noteclassic
在简单检查项的配置中，*用户名* 和 *密码*
字段用于Vmware的监控项；非VMware监控项则可忽略。
:::

[comment]: # ({/new-f59a5ccc})

[comment]: # ({new-23d7246f})

### Item key details

[comment]: # ({/new-23d7246f})

[comment]: # ({new-7d8dff2e})

##### icmpping\[<target>,<packets>,<interval>,<size>,<timeout>,<options>\] {#icmpping}

<br>
The host accessibility by ICMP ping.<br>
Return value: *0* - ICMP ping fails; *1* - ICMP ping successful.

Parameters:

-   **target** - the host IP or DNS name;
-   **packets** - the number of packets;
-   **interval** - the time between successive packets in milliseconds;
-   **size** - the packet size in bytes;
-   **timeout** - the timeout in milliseconds;
-   **options** - used for allowing redirect: if empty (default value), redirected responses are treated as target host down; if set to *allow_redirect*, redirected responses are treated as target host up.

See also the table of [default values](#default-values).

Example:

    icmpping[,4] #If at least one packet of the four is returned, the item will return 1.

[comment]: # ({/new-7d8dff2e})

[comment]: # ({new-6cd0f1b1})

##### icmppingloss\[<target>,<packets>,<interval>,<size>,<timeout>,<options>\] {#icmppingloss}

<br>
The percentage of lost packets.<br>
Return value: *Float*.

Parameters:

-   **target** - the host IP or DNS name;
-   **packets** - the number of packets;
-   **interval** - the time between successive packets in milliseconds;
-   **size** - the packet size in bytes;
-   **timeout** - the timeout in milliseconds;
-   **options** - used for allowing redirect: if empty (default value), redirected responses are treated as target host down; if set to *allow_redirect*, redirected responses are treated as target host up.

See also the table of [default values](#default-values).

[comment]: # ({/new-6cd0f1b1})

[comment]: # ({new-b5636838})

##### icmppingsec\[<target>,<packets>,<interval>,<size>,<timeout>,<mode>,<options>\] {#icmppingsec}

<br>
The ICMP ping response time (in seconds).<br>
Return value: *Float*.

Parameters:

-   **target** - the host IP or DNS name;
-   **packets** - the number of packets;
-   **interval** - the time between successive packets in milliseconds;
-   **size** - the packet size in bytes;
-   **timeout** - the timeout in milliseconds;
-   **mode** - possible values: *min*, *max*, or *avg* (default);
-   **options** - used for allowing redirect: if empty (default value), redirected responses are treated as target host down; if set to *allow_redirect*, redirected responses are treated as target host up.

Comments:

-   Packets which are lost or timed out are not used in the calculation;
-   If the host is not available (timeout reached), the item will return 0;
-   If the return value is less than 0.0001 seconds, the value will be set to 0.0001 seconds;
-   See also the table of [default values](#default-values).

[comment]: # ({/new-b5636838})

[comment]: # ({new-b0a71170})

##### net.tcp.service[service,<ip>,<port>] {#nettcpservice}

<br>
Checks if a service is running and accepting TCP connections.<br>
Return value: *0* - the service is down; *1* - the service is running.

Parameters:

-   **service** - possible values: *ssh*, *ldap*, *smtp*, *ftp*, *http*, *pop*, *nntp*, *imap*, *tcp*, *https*, *telnet* (see [details](/manual/appendix/items/service_check_details));
-   **ip** - the IP address or DNS name (by default the host IP/DNS is used);
-   **port** - the port number (by default the standard service port number is used).

Comments:

-   Note that with *tcp* service indicating the port is mandatory;
-   These checks may result in additional messages in system daemon logfiles (SMTP and SSH sessions being logged usually);
-   Checking of encrypted protocols (like IMAP on port 993 or POP on port 995) is currently not supported. As a workaround, please use `net.tcp.service[tcp,<ip>,port]` for checks like these.

Example:

    net.tcp.service[ftp,,45] #This item can be used to test the availability of FTP server on TCP port 45.

[comment]: # ({/new-b0a71170})

[comment]: # ({new-946385b4})

##### net.tcp.service.perf[service,<ip>,<port>] {#nettcpserviceperf}

<br>
Checks the performance of a TCP service.<br>
Return value: *Float*: *0.000000* - the service is down; *seconds* - the number of seconds spent while connecting to the service.

Parameters:

-   **service** - possible values: *ssh*, *ldap*, *smtp*, *ftp*, *http*, *pop*, *nntp*, *imap*, *tcp*, *https*, *telnet* (see [details](/manual/appendix/items/service_check_details));
-   **ip** - the IP address or DNS name (by default the host IP/DNS is used);
-   **port** - the port number (by default the standard service port number is used).

Comments:

-   Note that with *tcp* service indicating the port is mandatory;
-   Checking of encrypted protocols (like IMAP on port 993 or POP on port 995) is currently not supported. As a workaround, please use `net.tcp.service[tcp,<ip>,port]` for checks like these.

Example:

    net.tcp.service.perf[ssh] #This item can be used to test the speed of initial response from SSH server.

[comment]: # ({/new-946385b4})

[comment]: # ({new-2859223a})

##### net.udp.service[service,<ip>,<port>] {#netudpservice}

<br>
Checks if a service is running and responding to UDP requests.<br>
Return value: *0* - the service is down; *1* - the service is running.

Parameters:

-   **service** - possible values: *ntp* (see [details](/manual/appendix/items/service_check_details));
-   **ip** - the IP address or DNS name (by default the host IP/DNS is used);
-   **port** - the port number (by default the standard service port number is used).

Example:

    net.udp.service[ntp,,45] #This item can be used to test the availability of NTP service on UDP port 45.

[comment]: # ({/new-2859223a})

[comment]: # ({new-9cf5922c})

##### net.udp.service.perf[service,<ip>,<port>] {#netudpserviceperf}

<br>
Checks the performance of a UDP service.<br>
Return value: *Float*: *0.000000* - the service is down; *seconds* - the number of seconds spent waiting for response from the service.

Parameters:

-   **service** - possible values: *ntp* (see [details](/manual/appendix/items/service_check_details));
-   **ip** - the IP address or DNS name (by default the host IP/DNS is used);
-   **port** - the port number (by default the standard service port number is used).

Example:

    net.udp.service.perf[ntp] #This item can be used to test the response time from NTP service.

[comment]: # ({/new-9cf5922c})

[comment]: # ({new-85612a2b})
  
*Note* that for SourceIP support in LDAP simple checks, OpenLDAP version 2.6.1 or above is required.

[comment]: # ({/new-85612a2b})

[comment]: # ({new-92a6c4ae})
#### 支持的简单检查

Zabbix支持的简单检查列表：

另请参考：

-   [VMware monitoring item
    keys](/manual/config/items/itemtypes/simple_checks/vmware_keys)
-   [VMware监控项键值](/manual/config/items/itemtypes/simple_checks/vmware_keys)

|键值|<|<|<|<|
|------|-|-|-|-|
|<|**描述**                               *|返回值**                             **参数|*                                                                                                                                                                         **注解**|<|
|icmpping\[<target>,<packets>,<interval>,<size>,<timeout>\]|<|<|<|<|
|<|通过ICMP ping，检测主机的可访问性      0 - ICMP pi|g 失败\                    **target** -<br>1 - ICMP ping 成功                     *|机IP或者域名\                                                                                                                                                     示例:<br>**packets** - 数据包数量\                                                                                                                                                        =&gtinterval** - 连续数据包之间的时间间隔（以毫秒为单位）\                                                                                                                         <br>**size** - 数据包大小（以字节为单位）\                                                                                                                                           另请参考: [默认值](**timeout** - 超时时间（以毫秒为单位）|<icmpping\[,4\] → 4个包中只要一个有返回，那么该项则返回1.<br>imple_checks#icmp_pings)表|
|icmppingloss\[<target>,<packets>,<interval>,<size>,<timeout>\]|<|<|<|<|
|<|丢失数据包的百分比                     数值（浮点数）|**target** - 主机|P或者域名\                                                                                                                                                     另请参考: [默认值](simple_ch**packets** - 数据包数量<br>**interval** - 连续数据包之间的时间间隔（以毫秒为单位）<br>**size** - 数据包大小（以字节为单位）<br>**timeout** - 超时时间（以毫秒为单位）|cks#icmp_pings)表.|
|icmppingsec\[<target>,<packets>,<interval>,<size>,<timeout>,<mode>\]|<|<|<|<|
|<|ICMP ping响应时间（以秒为单位）        数值（浮点数）|**target** - 主机IP|者域名\                                                                                                                                                     如果主机不可用(达到超时), 则该监控项返回0**packets** - 数据包数量\                                                                                                                                                        如果返回**interval** - 连续数据包之间的时间间隔（以毫秒为单位）\                                                                                                                         <br>**size** - 数据包大小（以字节为单位）\                                                                                                                                           另请参考: [默认值](**timeout** - 超时时间（以毫秒为单位）<br>**mode** - 可能的值: *min*, *max*, *avg* (默认)|<br>小于0.0001秒, 该值将被设置为0.0001秒.<br>imple_checks#icmp_pings)表.|
|net.tcp.service\[service,<ip>,<port>\]|<|<|<|<|
|<|检测服务是否正在运行并且接受TCP连接.   0 - 服务停止<br>|**service** - 可能的值:<br>1 - 服务正在运行                       **por|*ssh*, *ldap*, *smtp*, *ftp*, *http*, *pop*, *nntp*, *imap*, *tcp*, *https*, *telnet* (另见 [详细说明](/manual/appendix/items/service_check_details))\   示例:<br>**ip** - IP地址或者域名 (默认使用主机IP/DNS)\                                                                                                                                    => net.t** - 端口号 (默认使用标准服务端口)                                                                                                                                         <br>|<p.service\[ftp,,45\] → 可用于测试运行在TCP 45端口上FTP服务器的可用性.<br>请注意，使用 *tcp* 服务必须指定端口.<br>这些检查可能会在系统守护进程日志文件中产生额外的信息 (通常会记录SMTP和SSH会话).<br>目前不支持检测加密协议(如端口993上的IMAP或端口995上的POP). 作为一种解决方法, 请使用 net.tcp.service\[tcp,<ip>,port\]进行检测.<br>从Zabbix 2.0以后开始支持*https* 和 *telnet* 服务。|
|net.tcp.service.perf\[service,<ip>,<port>\]|<|<|<|<|
|<|检测TCP服务性能.                       浮点数.<br>|**servic<br>0.000000 - 服务停止\                   **p<br>seconds - 连接到服务花费的时间（秒）|** - 可能的值: *ssh*, *ldap*, *smtp*, *ftp*, *http*, *pop*, *nntp*, *imap*, *tcp*, *https*, *telnet* (另见 [详细说明](/manual/appendix/items/service_check_details))\   示例:<br>**ip** - IP地址或者域名 (默认使用主机ip/DNS)\                                                                                                                                    => net.trt** - 端口号 (默认使用标准服务端口)                                                                                                                                         <br>目前不支持检测加密协议(|<p.service.perf\[ssh\] → 可以用来测试SSH服务器的初始响应速度.<br>请注意，使用 *tcp* 服务必须指定端口。<br>端口993上的IMAP或端口995上的POP)。 作为一种解决方法, 请使用 net.tcp.service.perf\[tcp,<ip>,port\] 进行检测.<br>从Zabbix 2.0以后开始支持*https* and *telnet* 服务。<br>在Zabbix 2.0之前，调用的是tcp\_perf。|
|net.udp.service\[service,<ip>,<port>\]|<|<|<|<|
|<|检测服务是否正在运行并响应UDP请求      0 - 服务停止<br>|**service** - 可能的值<br>1 - 服务正在运行                       **por|*ntp* (另见 [详细说明](/manual/appendix/items/service_check_details))\                                                                                   示例:<br>**ip** - IP地址或者域名 (默认使用主机ip/DNS)\                                                                                                                                    => net.u** - 端口号 (默认使用标准服务端口).                                                                                                                                        <br>|<p.service\[ntp,,45\] → 可用于测试UDP端口45上NTP服务的可用性.<br>从Zabbix 3.0以后开始支持此监控项, 但在之前的版本中 *ntp* 服务可用于net.tcp.service\[\]监控项。|
|net.udp.service.perf\[service,<ip>,<port>\]|<|<|<|<|
|<|检测UDP服务的性能                      浮点数.<br>|**service<br>0.000000 - 服务停止\                   **p<br>seconds - 等待服务响应的时间（秒）|* - 可能的值: *ntp* (另见 [详细说明](/manual/appendix/items/service_check_details))\                                                                                   示例:<br>**ip** - IP地址或者域名 (默认使用主机IP/DNS)\                                                                                                                                    => net.urt** - 端口号 (默认使用标准服务端口).                                                                                                                                        <br>|<p.service.perf\[ntp\] → 可用于测试NTP服务的响应时间.<br>从Zabbix 3.0以后开始支持此监控项, 但在之前的版本中 *ntp* 服务可用于net.tcp.service\[\]监控项。|

[comment]: # ({/new-92a6c4ae})

[comment]: # ({new-b08a4508})
##### 超时处理

如果简单检查时间超过了zabbix
server或是proxy配置文件中设置的超时时间,zabbix 将不会做处理。

#### ICMP pings

Zabbix使用外部程序 **fping** 来处理ICMP ping

fping不包含在Zabbix的发行版中，您需要另外安装。如果程序未安装、程序权限错误或者程序路径与配置文件中定义的不匹配
('FpingLocation' 参数)，则不会处理ICMP ping (**icmpping**,
**icmppingloss**, **icmppingsec**)

另请参考： [已知问题](/manual/installation/known_issues#simple_checks)

**fping**
fping必须可被Zabbix守护进程以root身份执行,需要设置setuid权限。为设置正确的权限，请以root身份执行这些命令：

    shell> chown root:zabbix /usr/sbin/fping
    shell> chmod 4710 /usr/sbin/fping

执行上述两条命令之后，检查fping可执行文件的所有权。在某些情况下，可以通过执行chmod命令来重置所有权。

还要检查一下，如果用户zabbix属于zabbix组，则运行：

    shell> groups zabbix

如果没有添加上，通过如下命令解决：

    shell> usermod -a -G zabbix zabbix

ICMP检测参数的默认值、限制和以及数值的描述：

|参数       单|描述|Fping|标志   fping默认设|Zab|ix允许的<br>|<限制|<|
|----------------|------|-----|-----------------------|---|---------------|-------|-|

::: notewarning
警告:
根据平台和版本的不同，fping的默认值也会有所不同 - 如有疑问,
请参考fping文档。
:::

Zabbix将三个 *icmpping\**
键值中任何一个IP地址写入一个临时文件中，然后传递给
**fping**。如果监控项有不同的键值参数，则只有具有相同键值参数的项才被写入单个文件。\
所有写入到单个文件的IP地址将通过fping并行检查，因此Zabbix icmp
pinger进程将花费固定的时间来忽略文件中的IP地址数量

[comment]: # ({/new-b08a4508})

[comment]: # ({new-e3dd9826})

##### Installation

fping is not included with Zabbix and needs to be installed separately:

- Various Unix-based platforms have the fping package in their default repositories, but it is not pre-installed. In this case you can use the package manager to install fping.

- Zabbix provides [fping packages](http://repo.zabbix.com/non-supported/rhel/) for RHEL. Please note that these packages are provided without official support.

- fping can also be compiled [from source](https://github.com/schweikert/fping/blob/develop/README.md#installation).

[comment]: # ({/new-e3dd9826})

[comment]: # ({new-9ced6345})

##### Configuration

Specify fping location in the *[FpingLocation](/manual/appendix/config/zabbix_server#fpinglocation)* parameter of Zabbix server/proxy configuration file 
(or *[Fping6Location](/manual/appendix/config/zabbix_server#fping6location)* parameter for using IPv6 addresses).

fping should be executable by the user Zabbix server/proxy run as and this user should have sufficient rights.

See also: [Known issues](/manual/installation/known_issues#simple_checks) for processing simple checks with fping versions below 3.10.

[comment]: # ({/new-9ced6345})

[comment]: # ({new-3c21487d})

##### Default values

Defaults, limits and description of values for ICMP check parameters:

|Parameter|Unit|Description|Fping's flag|Defaults set by|<|Allowed limits<br>by Zabbix|<|
|--|--|--------|-|--|--|--|--|
|||||**fping**|**Zabbix**|**min**|**max**|
|packets|number|number of request packets sent to a target|-C||3|1|10000|
|interval|milliseconds|time to wait between successive packets to an individual target|-p|1000||20|unlimited|
|size|bytes|packet size in bytes<br>56 bytes on x86, 68 bytes on x86_64|-b|56 or 68||24|65507|
|timeout|milliseconds|**fping v3.x** - timeout to wait after last packet sent, affected by *-C* flag<br> **fping v4.x** - individual timeout for each packet|-t|**fping v3.x** - 500<br>**fping v4.x** and newer - inherited from *-p* flag, but not more than 2000||50|unlimited|

The defaults may differ slightly depending on the platform and version.

In addition, Zabbix uses fping options *-i interval ms* (do not mix up with the item parameter *interval* mentioned in the table above,
which corresponds to fping option *-p*) and *-S source IP address* (or *-I* in older fping versions).
These options are auto-detected by running checks with different option combinations.
Zabbix tries to detect the minimal value in milliseconds that fping allows to use with *-i* by trying 3 values: 0, 1 and 10.
The value that first succeeds is then used for subsequent ICMP checks.
This process is done by each [ICMP pinger](/manual/concepts/server#server_process_types) process individually.

Auto-detected fping options are invalidated every hour and detected again on the next attempt to perform ICMP check.
Set [DebugLevel](/manual/appendix/config/zabbix_server#debuglevel)>=4 in order to view details of this process in the server or proxy log file.

Zabbix writes IP addresses to be checked by any of the three *icmpping\** keys to a temporary file, which is then passed to fping.
If items have different key parameters, only the ones with identical key parameters are written to a single file.
All IP addresses written to the single file will be checked by fping in parallel,
so Zabbix ICMP pinger process will spend fixed amount of time disregarding the number of IP addresses in the file.

[comment]: # ({/new-3c21487d})
