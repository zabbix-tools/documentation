<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="zh-CN" datatype="plaintext" original="manual/config/items/itemtypes/snmp.md">
    <body>
      <trans-unit id="43646dd0" xml:space="preserve">
        <source># 2 SNMP agent</source>
        <target state="needs-translation"># 2 SNMP代理</target>
      </trans-unit>
      <trans-unit id="f312fc02" xml:space="preserve">
        <source>#### Overview

You may want to use SNMP monitoring on devices such as printers, network
switches, routers or UPS that usually are SNMP-enabled and on which it
would be impractical to attempt setting up complete operating systems
and Zabbix agents.

To be able to retrieve data provided by SNMP agents on these devices,
Zabbix server must be [initially
configured](/manual/installation/install#configure-the-sources) with
SNMP support by specifying the `--with-net-snmp` flag.

SNMP checks are performed over the UDP protocol only.

Zabbix server and proxy daemons log lines similar to the following if
they receive an incorrect SNMP response:

    SNMP response from host "gateway" does not contain all of the requested variable bindings

While they do not cover all the problematic cases, they are useful for
identifying individual SNMP devices for which combined requests should be
disabled.

Zabbix server/proxy will always retry at least one time after an
unsuccessful query attempt: either through the SNMP library's retrying
mechanism or through the internal [combined
processing](#internal_workings_of_combined_processing) mechanism.

::: notewarning
If monitoring SNMPv3 devices, make sure that
msgAuthoritativeEngineID (also known as snmpEngineID or "Engine ID") is
never shared by two devices. According to [RFC
2571](http://www.ietf.org/rfc/rfc2571.txt) (section 3.1.1.1) it must be
unique for each device.
:::

::: notewarning
 RFC3414 requires the SNMPv3 devices to persist
their engineBoots. Some devices do not do that, which results in their
SNMP messages being discarded as outdated after being restarted. In such
situation, SNMP cache needs to be manually cleared on a server/proxy (by
using [-R snmp\_cache\_reload](/manual/concepts/server#runtime_control))
or the server/proxy needs to be restarted. 
:::</source>
        <target state="needs-translation">＃＃＃＃ 概述

您可能希望在打印机、网络交换机、路由器或UPS等设备上使用SNMP监控，这些设备通常启用SNMP，在这些设备上尝试设置完整的操作系统和Zabbix代理是不切实际的。

为了能够监控SNMP代理在这些设备上提供的数据，Zabbix服务器初始化配置时必须支持(/manual/installation/install#configure_the_sources)SNMP数据源。
SNMP 检查只支持 UDP 协议。

Zabbix 服务器和代理守护进程向 SNMP 设备一个请求中查询多个值时，这会影响所有SNMP监控项（常规 SNMP监控项、具有动态索引的 SNMP 监控项和 SNMP 自动发现）同时可以使 SNMP 处理更加高效。见[批量处理](#internal_workings_of_bulk_processing) 技术部分内部如何运作的详细信息。对于无法使用“批量请求”的接口可以通过设置可以关闭。

如果Zabbix服务器和代理守护程序收到错误的SNMP响应，则它们的日志行与以下类似:
    SNMP response from host "gateway" does not contain all of the requested variable bindings

虽然它们并没有涵盖所有有问题的情况，但它们对于识别应禁用批量请求的单个SNMP设备非常有用。

Zabbix服务器/代理在查询尝试失败后将始终或至少重试一次：通过SNMP库的重试机制或通过内部[批量处理]机制。

::: 注意警告
如果监控SNMPv3设备，请确保msgAuthoritativeEngineID（也称为snmpEngineID或“引擎ID”）从不被两台设备共享。根据RFC 2571 （3.1.1.1节），每个设备必须是唯一的
:::snmpEngineID

::: 注意警告
 RFC3414 要求 SNMPv3 设备保留其engineBoots。某些设备不这么做，这会导致有些设备在运行时的不会丢弃过期的SNMP 消息。在这种情况下，需要在服务器/代理上手动清除 SNMP 缓存（通过使用 [-R snmp\_cache\_reload](/manual/concepts/server#runtime_control))或者需要重新启动服务器/代理。
:::</target>
      </trans-unit>
      <trans-unit id="cac544a8" xml:space="preserve">
        <source>#### Configuring SNMP monitoring

To start monitoring a device through SNMP, the following steps have to
be performed:</source>
        <target state="needs-translation">#### 配置SNMP监控

要通过SNMP开始监控设备，必须执行以下步骤：</target>
      </trans-unit>
      <trans-unit id="501ff4eb" xml:space="preserve">
        <source>##### Step 1

Find out the SNMP string (or OID) of the item you want to monitor.

To get a list of SNMP strings, use the **snmpwalk** command (part of
[net-snmp](http://www.net-snmp.org/) software which you should have
installed as part of the Zabbix installation) or equivalent tool:

    snmpwalk -v 2c -c public &lt;host IP&gt; .

As '2c' here stands for SNMP version, you may also substitute it with
'1', to indicate SNMP Version 1 on the device.

This should give you a list of SNMP strings and their last value. If it
doesn't then it is possible that the SNMP 'community' is different from
the standard 'public' in which case you will need to find out what it
is.

You can then go through the list until you find the string you want to
monitor, e.g. if you wanted to monitor the bytes coming in to your
switch on port 3 you would use the `IF-MIB::ifHCInOctets.3` string from
this line:

    IF-MIB::ifHCInOctets.3 = Counter64: 3409739121

You may now use the **snmpget** command to find out the numeric OID for
'IF-MIB::ifHCInOctets.3':

    snmpget -v 2c -c public -On &lt;host IP&gt; IF-MIB::ifHCInOctets.3

Note that the last number in the string is the port number you are
looking to monitor. See also: [Dynamic
indexes](/manual/config/items/itemtypes/snmp/dynamicindex).

This should give you something like the following:

    .1.3.6.1.2.1.31.1.1.1.6.3 = Counter64: 3472126941

Again, the last number in the OID is the port number.

::: notetip
Some of the most used SNMP OIDs are [translated
automatically to a numeric
representation](/manual/config/items/itemtypes/snmp/special_mibs) by
Zabbix.
:::

In the last example above value type is "Counter64", which internally
corresponds to ASN\_COUNTER64 type. The full list of supported types is
ASN\_COUNTER, ASN\_COUNTER64, ASN\_UINTEGER, ASN\_UNSIGNED64,
ASN\_INTEGER, ASN\_INTEGER64, ASN\_FLOAT, ASN\_DOUBLE, ASN\_TIMETICKS,
ASN\_GAUGE, ASN\_IPADDRESS, ASN\_OCTET\_STR and ASN\_OBJECT\_ID. 
These types roughly correspond to "Counter32",
"Counter64", "UInteger32", "INTEGER", "Float", "Double", "Timeticks",
"Gauge32", "IpAddress", "OCTET STRING", "OBJECT IDENTIFIER" in
**snmpget** output, but might also be shown as "STRING", "Hex-STRING",
"OID" and other, depending on the presence of a display hint.</source>
        <target state="needs-translation">＃＃＃＃＃ 步骤1

找出您要监控的项目的 SNMP 字符串（或 OID）。

要获取SNMP字符串列表，请使用 snmpwalk 命令（net-snmp的部分软件应该在Zabbix安装时同时安装）或等效工具：

    shell&gt; snmpwalk -v 2c -c public &lt;主机 IP&gt; 。

此处的“2c”代表 SNMP 版本，您也可以将其替换为“1”，表示设备上的 SNMP 版本v1。

它会返回给你一个SNMP字符串及其最后一个值的列表。如果不是，那么SNMP 'community' 可能与标准的'public'不同，在这种情况下，请找出它是什么。

然后，您可以遍历列表，直到找到要监控的字符串，例如：如果要监视通过端口3进入交换机的字节，你将使用此行中的IF-MIB :: ifInOctets.3字符串：

    IF-MIB::ifInOctets.3 = Counter32: 3409739121

你现在可以使用 snmpget 命令找出'IF-MIB :: ifInOctets.3'的数字OID：

    shell&gt; snmpget -v 2c -c 公共 -On 10.62.1.22 IF-MIB::ifInOctets.3

请注意，字符串中的最后一个数字是您需要监控的端口号。另请参阅：[动态索引]（/manual/config/items/itemtypes/snmp/dynamicindex）。
    .1.3.6.1.2.1.2.2.1.10.3 = Counter32：3472126941

同样，OID 中的最后一个数字是端口号。

::: Note
3COM似乎使用端口号在100以上，例如端口1=端口101，端口3=端口103，但Cisco使用常规数字，例如端口3=3。
:::

::: Note
一些最常用的SNMP OID，Zabbix将自动转换为数字表示。（/manual/config/items/itemtypes/SNMP/special_mibs）
:::

在上面的例子中，值类型是“Counter32”它在内部对应于ASN_COUNTER类型。完整的支持类型包括 ASN_COUNTER, ASN_COUNTER64, ASN_UINTEGER, ASN_UNSIGNED64, ASN_INTEGER, ASN_INTEGER64, ASN_FLOAT, ASN_DOUBLE, ASN_TIMETICKS, ASN_GAUGE, ASN_IPADDRESS, ASN_OCTET_STR 和 ASN_OBJECT_ID (从2.2.8, 2.4.3之后). 这些类型大致对应于snmpget 输出的 "Counter32", "Counter64", "UInteger32", "INTEGER", "Float", "Double", "Timeticks", "Gauge32", "IpAddress", "OCTET STRING", "OBJECT IDENTIFIER", 但也有可能显示为 "STRING", "Hex-STRING", "OID" 或者其它, 这取这取决于显示提示的表达方式。</target>
      </trans-unit>
      <trans-unit id="929235a8" xml:space="preserve">
        <source>##### Step 2

[Create a host](/manual/config/hosts/host) corresponding to a device.

![](../../../../../assets/en/manual/config/items/itemtypes/snmp/snmp_host.png)

Add an SNMP interface for the host:

-   Enter the IP address/DNS name and port number
-   Select the *SNMP version* from the dropdown
-   Add interface credentials depending on the selected SNMP version:
    -   SNMPv1, v2 require only the community (usually 'public')
    -   SNMPv3 requires more specific options (see below)
-   Specify the max repetition value (default: 10) for [native SNMP bulk requests](#native-snmp-bulk-requests) (GetBulkRequest-PDUs); only for `discovery[]` and `walk[]` items in SNMPv2 and v3
-   Mark the *Use combined requests* checkbox to allow [combined processing](#internal_workings_of_combined_processing) of SNMP requests (not related to native SNMP bulk requests)

|SNMPv3 parameter|Description|
|--|--------|
|*Context name*|Enter context name to identify item on SNMP subnet.&lt;br&gt;*Context name* is supported for SNMPv3 items since Zabbix 2.2.&lt;br&gt;User macros are resolved in this field.|
|*Security name*|Enter security name.&lt;br&gt;User macros are resolved in this field.|
|*Security level*|Select security level:&lt;br&gt;**noAuthNoPriv** - no authentication nor privacy protocols are used&lt;br&gt;**AuthNoPriv** - authentication protocol is used, privacy protocol is not&lt;br&gt;**AuthPriv** - both authentication and privacy protocols are used|
|*Authentication protocol*|Select authentication protocol - *MD5*, *SHA1*, *SHA224*, *SHA256*, *SHA384* or *SHA512*.|
|*Authentication passphrase*|Enter authentication passphrase.&lt;br&gt;User macros are resolved in this field.|
|*Privacy protocol*|Select privacy protocol - *DES*, *AES128*, *AES192*, *AES256*, *AES192C* (Cisco) or *AES256C* (Cisco).&lt;br&gt;Note that: &lt;br&gt; - on some older systems net-snmp may not support AES256.
&lt;br&gt; - on some newer systems (for example, RHEL9) support of DES may be dropped for the net-snmp package.|
|*Privacy passphrase*|Enter privacy passphrase.&lt;br&gt;User macros are resolved in this field.|

In case of wrong SNMPv3 credentials (security name, authentication
protocol/passphrase, privacy protocol):

-   Zabbix receives an ERROR from net-snmp, except for wrong *Privacy passphrase* in which case Zabbix
receives a TIMEOUT error from net-snmp;
-   SNMP interface availability will switch to red (unavailable).

::: notewarning
 Changes in *Authentication protocol*,
*Authentication passphrase*, *Privacy protocol* or *Privacy passphrase*,
made without changing the *Security name*, will take effect only after
the cache on a server/proxy is manually cleared (by using [-R
snmp\_cache\_reload](/manual/concepts/server#running_server)) or the
server/proxy is restarted. In cases, where *Security name* is also
changed, all parameters will be updated immediately. 
:::

You can use one of the provided SNMP templates that will automatically add a set of items. Before using a template,
verify that it is compatible with the host. 

Click on *Add* to save the host.</source>
        <target state="needs-translation">＃＃＃＃＃ 第2步

为设备创建一个(/manual/config/hosts/host) 主机。

![](../../../../../assets/en/manual/config/items/itemtypes/snmp/snmp_host.png)

为主机添加 SNMP 接口：

- 输入 IP 地址/DNS 名称和端口号
- 从下拉列表中选择 *SNMP 版本*
- 根据所选 SNMP 版本添加接口凭据：
    - SNMPv1、v2 只需要community凭据（默认值是"'public'"）
    - SNMPv3 需要更具体的选项（见下文）
- 将*使用批量请求*复选框标记为允许批量
    处理 SNMP 请求

|SNMPv3参数|说明|
|----------------|-----------|
|*上下文名称*|输入上下文名称以识别 SNMP 子网上的项目。&lt;br&gt;自 Zabbix 2.2 起，SNMPv3 项目支持*上下文名称*。&lt;br&gt;在此字段中解析用户宏。|
|*安全名称*|输入安全名称。&lt;br&gt;用户宏在此字段中解析。|
|*安全级别*|选择安全级别：&lt;br&gt;**noAuthNoPriv** - 不使用身份验证或隐私协议&lt;br&gt;**AuthNoPriv** - 使用身份验证协议，不使用隐私协议&lt;br&gt;**AuthPriv ** - 使用身份验证和隐私协议|
|*身份验证协议*|选择身份验证协议 - *MD5*、*SHA1*、*SHA224*、*SHA256*、*SHA384* 或 *SHA512*。|
|*身份验证密码*|输入身份验证密码。&lt;br&gt;用户宏在此字段中解析。|
|*隐私协议*|选择隐私协议 - *DES*、*AES128*、*AES192*、*AES256*、*AES192C* (Cisco) 或 *AES256C* (Cisco)。|
|*隐私密码*|输入隐私密码。&lt;br&gt;用户宏在此字段中解析。|

如果SNMPv3凭据（安全名称，验证协议/口令，隐私协议）错误，Zabbix会从net-snmp收到错误，如果 私钥 错误，在这种情况下，Zabbix会从net-snmp收到TIMEOUT错误。

::: 警告
在不更改安全名称的情况下，对验证协议、验证口令、隐私协议或私钥的更改只有在手动清除服务器/代理上的缓存（通过使用-R snmp_cache_reload）或重新启动服务器/代理后才会生效。如果安全名称也已更改，则所有参数将立即更新。
:::

您可以使用zabbix提供的任意 SNMP 模板（ SNMP 设备模板和其他模板），该模板将自动添加监控项。但是，那模板可能与主机不兼容。点击 Add 保存主机。</target>
      </trans-unit>
      <trans-unit id="290ebad1" xml:space="preserve">
        <source>##### Step 3

Create an item for monitoring.

So, now go back to Zabbix and click on *Items* for the SNMP host you
created earlier. Depending on whether you used a template or not when
creating your host, you will have either a list of SNMP items associated
with your host or just an empty list. We will work on the assumption
that you are going to create the item yourself using the information you
have just gathered using snmpwalk and snmpget, so click on *Create
item*. 

Fill in the required parameters in the new item form:

![](../../../../../assets/en/manual/config/items/itemtypes/snmp_item.png)

|Parameter|Description|
|--|--------|
|*Name*|Enter the item name.|
|*Type*|Select **SNMP agent** here.|
|*Key*|Enter the key as something meaningful.|
|*Host interface*|Make sure to select the SNMP interface, e.g. of your switch/router.|
|*SNMP OID*|This field supports two options:&lt;br&gt;**1)** Enter a single textual or numeric OID, for example: 1.3.6.1.2.1.31.1.1.1.6.3 (in this case, make sure to add a *Change per second* step in the *Preprocessing* tab; otherwise you will get cumulative values from the SNMP device instead of the latest change).&lt;br&gt;**2)** Use the **walk[OID1,OID2,...]** item, which makes use of [native SNMP bulk requests](#native-snmp-bulk-requests) (GetBulkRequest-PDUs). You may use this as the master item, with dependent items that extract data from the master using preprocessing.&lt;br&gt;For example, `walk[1.3.6.1.2.1.2.2.1.2,1.3.6.1.2.1.2.2.1.3]`.&lt;br&gt;This item returns the output of the snmpwalk utility with -Oe -Ot -On parameters.&lt;br&gt;MIB names are supported as parameters; thus `walk[1.3.6.1.2.1.2.2.1.2]` and `walk[ifDescr]` will return the same output.&lt;br&gt;If several OIDs/MIBs are specified, i.e. `walk[ifDescr,ifType,ifPhysAddress]`, then the output is a concatenated list.&lt;br&gt;This item uses GetBulk requests with SNMPv2 and v3 interfaces and GetNext for SNMPv1 interfaces; max repetitions for bulk requests are configured on the interface level.&lt;br&gt;You may use this item as a master item in [SNMP discovery](/manual/discovery/low_level_discovery/examples/snmp_oids_walk).|

All mandatory input fields are marked with a red asterisk.

Now save the item and go to *Monitoring* → *Latest data* for your SNMP
data.</source>
        <target state="needs-translation">##### 步骤 3

创建一个监控项。

所以现在回到Zabbix并点击前面创建的SNMP主机的 监控项 。 如果你在创建主机时选择使用模板，你将拥有与主机相关联的SNMP监控项列表。我们假设你要使用snmpwalk和snmpget采集的信息创建监控项，单击 创建监控项。在新的监控项表单中：
- 输入监控项"名称"。
- 将"类型"字段更改为"SNMP客户端"
- 输入“键值”为有意义的内容，例如，SNMP-InOctets-Bps。
- 确保“主机接口”字段中有你的交换机/路由器
- 将你之前检索到的文本或数字OID输入到'SNMP OID'字段中，例如：.1.3.6.1.2.1.2.2.1.10.3
- 将“信息类型”设置为 浮点数
- 如果你希望“更新间隔”和“历史数据保留时长”与默认值不同，请选择一个自定义乘数（如果需要），并输入数值
- 在进程预处理选项卡中，添加 *Change per second* 步骤（重要！，否则您将从 SNMP 设备获取累积值，而不是差异值）。

![](../../../../../assets/en/manual/config/items/itemtypes/snmp_item.png)

所有必填字段都标有红色星号。

现在保存监控项，进入 监测中 → 最新数据 来获取你的SNMP数据!</target>
      </trans-unit>
      <trans-unit id="6021e1bd" xml:space="preserve">
        <source>##### Example 1

General example:

|Parameter|Description|
|--|--------|
|**OID**|1.2.3.45.6.7.8.0 (or .1.2.3.45.6.7.8.0)|
|**Key**|&lt;Unique string to be used as reference to triggers&gt;&lt;br&gt;For example, "my\_param".|

Note that OID can be given in either numeric or string form. However, in
some cases, string OID must be converted to numeric representation.
Utility snmpget may be used for this purpose:

    snmpget -On localhost public enterprises.ucdavis.memory.memTotalSwap.0</source>
        <target state="needs-translation">##### 示例 

通用示例：

|参数 |描述 |
|---------------------|-|
|**OID**|1.2.3.45.6.7.8.0 (or .1.2.3.45.6.7.8.0)|
|**键值**|用作触发器引用的唯一字符串&gt;&lt;br&gt;例如, "my\_param".|

请注意，OID 可以以数字或字符串形式给出。但是，在某些情况下，必须将字符串 OID 转换为数字表示。实用程序 snmpget 可用于此目的：
     shell&gt; snmpget -On localhost public enterprises.ucdavis.memory.memTotalSwap.0

在配置Zabbix源时指定了 --with-net-snmp 标志，可以监视SNMP参数</target>
      </trans-unit>
      <trans-unit id="45af5cf0" xml:space="preserve">
        <source>##### Example 2

Monitoring of uptime:

|Parameter|Description|
|--|--------|
|**OID**|MIB::sysUpTime.0|
|**Key**|router.uptime|
|**Value type**|Float|
|**Units**|uptime|
|**Preprocessing step: Custom multiplier**|0.01|</source>
        <target state="needs-translation">##### 示例 2

监控正常运行时间：

|参数|说明|
|---------|------------|
|**OID**|MIB::sysUpTime.0对应获取监控项值|
|**Key**|路由器正常运行时间|
|**Value type**|浮点数|
|**Units**|正常运行时间|
|**Multiplier**|倍数|</target>
      </trans-unit>
      <trans-unit id="ea70430a" xml:space="preserve">
        <source>
#### Native SNMP bulk requests

The **walk[OID1,OID2,...]** item allows to use native SNMP functionality for bulk requests (GetBulkRequest-PDUs), available in SNMP versions 2/3. 

A GetBulk request in SNMP executes multiple GetNext requests and returns the result in a single response. This may be used for regular SNMP items as well as for SNMP discovery to minimize network roundtrips.

The SNMP **walk[OID1,OID2,...]** item may be used as the master item that collects data in one request with dependent items that parse the response as needed using preprocessing. 

Note that using native SNMP bulk requests is not related to the option of combining SNMP requests, which is Zabbix own way of combining multiple SNMP requests (see next section).</source>
      </trans-unit>
      <trans-unit id="c57b8645" xml:space="preserve">
        <source>#### Internal workings of combined processing

Zabbix server and proxy may query SNMP devices for
multiple values in a single request. This affects several types of SNMP
items:

-   regular SNMP items
-   SNMP items [with dynamic
    indexes](/manual/config/items/itemtypes/snmp/dynamicindex)
-   SNMP [low-level discovery
    rules](/manual/discovery/low_level_discovery/examples/snmp_oids_walk)

All SNMP items on a single interface with identical parameters are
scheduled to be queried at the same time. The first two types of items
are taken by pollers in batches of at most 128 items, whereas low-level
discovery rules are processed individually, as before.

On the lower level, there are two kinds of operations performed for
querying values: getting multiple specified objects and walking an OID
tree.

For "getting", a GetRequest-PDU is used with at most 128 variable
bindings. For "walking", a GetNextRequest-PDU is used for SNMPv1 and
GetBulkRequest with "max-repetitions" field of at most 128 is used for
SNMPv2 and SNMPv3.

Thus, the benefits of combined processing for each SNMP item type are
outlined below:

-   regular SNMP items benefit from "getting" improvements;
-   SNMP items with dynamic indexes benefit from both "getting" and
    "walking" improvements: "getting" is used for index verification and
    "walking" for building the cache;
-   SNMP low-level discovery rules benefit from "walking" improvements.

However, there is a technical issue that not all devices are capable of
returning 128 values per request. Some always return a proper response,
but others either respond with a "tooBig(1)" error or do not respond at
all once the potential response is over a certain limit.

In order to find an optimal number of objects to query for a given
device, Zabbix uses the following strategy. It starts cautiously with
querying 1 value in a request. If that is successful, it queries 2
values in a request. If that is successful again, it queries 3 values in
a request and continues similarly by multiplying the number of queried
objects by 1.5, resulting in the following sequence of request sizes: 1,
2, 3, 4, 6, 9, 13, 19, 28, 42, 63, 94, 128.

However, once a device refuses to give a proper response (for example,
for 42 variables), Zabbix does two things.

First, for the current item batch it halves the number of objects in a
single request and queries 21 variables. If the device is alive, then
the query should work in the vast majority of cases, because 28
variables were known to work and 21 is significantly less than that.
However, if that still fails, then Zabbix falls back to querying values
one by one. If it still fails at this point, then the device is
definitely not responding and request size is not an issue.

The second thing Zabbix does for subsequent item batches is it starts
with the last successful number of variables (28 in our example) and
continues incrementing request sizes by 1 until the limit is hit. For
example, assuming the largest response size is 32 variables, the
subsequent requests will be of sizes 29, 30, 31, 32, and 33. The last
request will fail and Zabbix will never issue a request of size 33
again. From that point on, Zabbix will query at most 32 variables for
this device.

If large queries fail with this number of variables, it can mean one of
two things. The exact criteria that a device uses for limiting response
size cannot be known, but we try to approximate that using the number of
variables. So the first possibility is that this number of variables is
around the device's actual response size limit in the general case:
sometimes response is less than the limit, sometimes it is greater than
that. The second possibility is that a UDP packet in either direction
simply got lost. For these reasons, if Zabbix gets a failed query, it
reduces the maximum number of variables to try to get deeper into the
device's comfortable range, but (starting from 2.2.8) only up to two
times.

In the example above, if a query with 32 variables happens to fail,
Zabbix will reduce the count to 31. If that happens to fail, too, Zabbix
will reduce the count to 30. However, Zabbix will not reduce the count
below 30, because it will assume that further failures are due to UDP
packets getting lost, rather than the device's limit.

If, however, a device cannot handle combined requests properly for other
reasons and the heuristic described above does not work, since Zabbix
2.4 there is a "Use combined requests" setting for each interface that
allows to disable combined requests for that device.</source>
        <target state="needs-translation">#### 批量处理的内部工作原理

从 Zabbix server和proxy 2.2.3 版本开始查询 SNMP 设备开始再一个请求获取多个值。这会影响几种类型的 SNMP监控项：

- 常规 SNMP 监控项
- SNMP监控项[带动态索引](/manual/config/items/itemtypes/snmp/dynamicindex)
- SNMP [低级别自动发现规则](/manual/discovery/low_level_discovery/examples/snmp_oids)

具有相同参数的单个接口上的所有SNMP监控项都将同时进行查询。前两种类型的监控项由轮询器分批采集，最多128个监控项，而低级发现规则如前所述单独处理。

在较低级别上，执行查询值的操作有两种：获取多个指定对象和游历OID树。

对于"getting"，GetRequest-PDU最多使用128个变量绑定。 对于"walking"，GetNextRequest-PDU用于SNMPv1和GetBulkRequest，"max-repetitions"字段最多128个用于SNMPv2和SNMPv3。

因此，每个SNMP监控项类型的批量处理的优势如下：

- 常规SNMP项目受益于“getting” 的改进;
- 具有动态索引的SNMP监控项受益于“getting”和“walking”改进：“getting”用于索引验证，“walking”用于构建缓存;
- SNMP低级发现规则受益于“walking”的改进。
然而，有一个技术问题，并非所有设备都能够根据请求返回128个值。有些总是给出正确的回应，其它情况则会以“tooBig（1）”错误做出回应，或者一旦潜在的回应超过了一定的限度，则一律不回应。

为了找到最佳数量的对象来查询给定的设备，Zabbix使用以下策略。它在请求中查询“值1”时谨慎开始。 如果成功，它会在请求中查询“值2”。 如果再次成功，则查询请求中的“值3”，并通过将查询对象的数量乘以1.5来继续，导致以下请求大小的顺序：1,2,3,4,6,9,13,19，28，42，63，94，128。

然而，一旦设备拒绝给出适当的响应（例如，对于42个变量），Zabbix会做两件事情。

首先，对于当前批量监控项，它将单个请求中的对象数减半，并查询21个变量。 如果设备处于活动状态，那么查询应该在绝大多数情况下都有效，因为已知28个变量可以工作，21个变量明显少于此。 但是，如果仍然失败，那么Zabbix会逐渐回到查询值。 如果此时仍然失败，那么设备肯定没有响应，请求大小也不是问题。

Zabbix为后续批量监控项做的第二件事是它从最后成功的变量数量开始（在我们的示例中为28），并继续将请求大小递增1，直到达到限制。 例如，假设最大响应大小为32个变量，后续请求的大小为29,30,31,32和33.最后一个请求将失败，Zabbix将永远不再发出大小为33的请求。 从那时起，Zabbix将为该设备查询最多32个变量。

如果大型查询因此数量的变量而失败，则可能意味着两件事之一。 设备用于限制响应大小的确切标准无法知晓，但我们尝试使用变量数来近似。 因此，第一种可能性是，在一般情况下，此数量的变量大约是设备的实际响应大小限制：有时响应小于限制，有时它大于限制。 第二种可能性是任何方向的UDP数据包都丢失了。 由于这些原因，如果Zabbix查询失败，它会减少最大数量的变量以尝试深入到设备的舒适范围，但（从2.2.8开始）最多只能达到两次。

在上面的示例中，如果包含32个变量的查询失败，Zabbix会将计数减少到31.如果发生这种情况也会失败，Zabbix也会将计数减少到30.但是，Zabbix不会将计数减少到30以下， 因为它会假设进一步的失败是由于UDP数据包丢失，而不是设备的限制。

但是，如果设备由于其他原因无法正确处理批量请求，并且上述启发式方法不起作用，Zabbix 2.4版本之后每个接口都有“使用批量请求”设置，允许禁用该设备的批量请求。</target>
      </trans-unit>
    </body>
  </file>
</xliff>
