[comment]: # translation:outdated

[comment]: # ({c25d95bb-d1352224})
# 17 Prometheus 数据处理


[comment]: # ({/c25d95bb-d1352224})

[comment]: # ({b6763997-06c90457})
#### 概述

Zabbix 可以采集符合 Prometheus行协议格式的监控数据。

采集数据需要配置以下两步：

- 创建一个作为主要监控项的[HTTP监控项](/manual/config/items/itemtypes/http) 指向合适的站点批量采集数据。例如：`https://<prometheus host>/metrics`
- 配置使用Prometheus 预处理选项的从属监控项，来从主要监控项中获取需要的数据

Zabbix 有两个Prometheus 预处理选项：

- *Prometheus正则匹配* - 可以应用于一般监控项中，用来从查询Prometheus行协议中提取数据
- *Prometheus转JSON* - 可以应用于一般监控项或低级发现监控项。使用这个预处理步骤，将会将采集到的Prometheus 数据转化为JSON格式返回给监控项

[comment]: # ({/b6763997-06c90457})

[comment]: # ({a121f070-4473dc29})
##### 批量处理

从属监控项支持批量处理。为了启用缓存和索引，是 *Prometheus 正则匹配* 步骤必须是预处理配置的**第一个**步骤。当 *Prometheus 正则匹配*是预处理的第一步时，经过这个预处理步骤配置的第一组 `<label>==<value>` 条件解析后的Prometheus数据会被缓存并建立索引。这个缓存可以在其他从属监控项的其他后续预处理过程中得到重用。为了更好的优化性能，第一个作为条件的label，应当匹配尽可能多的不同值。

如果有额外的预处理步骤需要在第一步之前执行，正确做法是将该步骤放在主要监控项或者新建一个从属监控项，并将这个监控项作为处理Prometheus数据处理监控项的主监控项。

[comment]: # ({/a121f070-4473dc29})

[comment]: # ({b68b2d52-73ac7ad6})
#### 配置

已经事先正确配置了 HTTP代理类型的主要监控项，你需要创建一个 [从属监控项](/manual/config/items/itemtypes/dependent_items) 来应用Prometheus 预处理步骤：

- 在配置表单中填入一般从属监控项的参数
- 前往预处理选项卡
- 选择一个 **Prometheus** 预处理选项（*Prometheus正则匹配* or *Prometheus转JSON*）

![](../../../../../assets/en/manual/config/items/itemtypes/prometheus_item.png)

|参数|描述|示例|
|---------|-----------|--------|
|*Pattern*|要定义所需数据的正则匹配规则，可以使用与PromQL（Prometheus 查询语言）十分类似的检索语言 (详见 [对比表]](#query_language_comparison)), 举个例子：<br><metric name> - 指定指标名称<br>{\_\_name\_\_=\~"<regex>"} - 根据正则表达式，匹配指标名称<br>{<label name>="<label value>",...} - 指定标签名称<br>{<label name>=\~"<regex>",...} - 根据正则表达式，匹配标签名称<br>{\_\_name\_\_=\~".\*"}==<value> - 指定指标值<br>或者将以上的条件任意组合：<br><metric name>{<label1 name>="<label1 value>",<label2 name>=\~"<regex>",...}==<value><br><br>标签名称可以是任何UTF-8字符的序列，但对于反斜杠、双引号和换行符必须使用转义，就像 `\\`，`\"`， `\n`；其他自负不应被转义。|*wmi\_os\_physical\_memory\_free\_bytes*<br>*cpu\_usage\_system{cpu="cpu-total"}*<br>*cpu\_usage\_system{cpu=\~".\*"}*<br>*cpu\_usage\_system{cpu="cpu-total",host=\~".\*"}*<br>*wmi\_service\_state{name="dhcp"}==1*<br>*wmi\_os\_timezone{timezone=\~".\*"}==1*|
|*Result processing*| 定义如何返回处理结果，提供了**值**，**标签** 或应用适当的函数（如正则匹配的到了多行结果，需要对这些结果进行简单聚合）:<br>**值** - 返回指标的值（当匹配到多行记录时，会引发错误）<br>**标签** - 在*Label*字段中指定制定的标签返回值（当匹配到多个指标时，会引发错误）<br>**总和** - 返回值的总和<br>**最小值** - 返回最小值<br>**max** - 返回最大值<br>**平均值** - 返回平均值<br>**计数** - 返回值的计数<br>此参数字段仅适用于**Prometheus正则匹配**预处理步骤。 | 实际用例见表后。 |
|*Output*|定义标签名称（可选）。如果这里填了什么，那这个预处理步骤将返回与格子里内容匹配的标签名称<br>此参数字段仅适用于**Prometheus正则匹配**预处理步骤。|<|

[comment]: # ({/b68b2d52-73ac7ad6})

[comment]: # ({93c7a8a8-8902eb84})
#### Prometheus转JSON

来自Prometheus 的数据可以被低级自动发现使用。要这样使用数据，必须要转化为JSON格式。Zabbix提供了*Prometheus转JSON*作为预处理步骤的选项，这可以直接按照格式要求返回数据。

详情参见[在低级自动发现中，使用Prometheus数据](/manual/discovery/low_level_discovery/examples/prometheus)。

[comment]: # ({/93c7a8a8-8902eb84})

[comment]: # ({1baf5a36-2fbe720b})
#### 查询语言对照表

下表列出了PromQL与Zabbix Prometheus 预处理查询语言之间的异同。

|<|[PromQL瞬时向量选择器](https://prometheus.io/docs/prometheus/latest/querying/basics/#instant-vector-selectors)|Zabbix Prometheus预处理|
|-|------------------------------------------------------------------------------------------------------------------------|-------------------------------|
|**不同点**|<|<|
|查询目标|Prometheus服务器|Prometheus行协议中的纯文本|
|返回值|瞬时向量|指标或标签的值（Prometheus正则匹配）<br>JSON格式的包含单个值的指标构成的数组（Prometheus转JSON）|
|标签匹配操作符|**=**, **!=**, **=\~**, **!\~**|**=**, **!=**, **=\~**, **!\~**|
|在匹配标签或指标名称的正则表达式|RE2|PCRE|
|比较操作符|详见[清单](https://prometheus.io/docs/prometheus/latest/querying/operators/#comparison-binary-operators)|只有 **==** (相等)用于支持值过滤|
|**相似点**|<|<|
|依据指标名相同的字符串筛选|<metric name> or {\_\_name\_\_="<metric name>"}|<metric name> or {\_\_name\_\_="<metric name>"}|
|依据匹配正则表达式的指标名筛选|{\_\_name\_\_=\~"<regex>"}|{\_\_name\_\_=\~"<regex>"}|
|依据相同字符串的标签名称 值筛选|{<label name>="<label value>",...}|{<label name>="<label value>",...}|
|依据匹配正则表达式的标签名称 值筛选|{<label name>=\~"<regex>",...}|{<label name>=\~"<regex>",...}|
|根据相同的字符串的值筛选|{\_\_name\_\_=\~".\*"} == <value>|{\_\_name\_\_=\~".\*"} == <value>|

[comment]: # ({/1baf5a36-2fbe720b})
