[comment]: # translation:outdated

[comment]: # ({521b2835-02a3a9ac})
# 从属监控项

[comment]: # ({/521b2835-02a3a9ac})

[comment]: # ({8c2ba8a5-4eb72974})
#### 概述

有时一个监控项一次会收集多个度量，或者同时收集相关的度量会更有意义，例如：

-   单个内核的CPU利用率
-   输入/输出的总网络流量

为了允许进行批量指标收集和同时使用对几个相关监控项，Zabbix支持从属监控项。从属监控项依赖于主要监控项包含从属监控项的一次查询，主要监控项的新值会用于自动填充从属监控项的值。从属监控项的更新间隔与主要监控项相同且不可变。

Zabbix预处理选项可用于从主要监控项的数据中，提取从属监控项所需部分的值。

预处理是由一个“预处理管理器”进程管理的，它被添加于Zabbix 3.4版本中，与worker进程一起执行预处理步骤。所有来自不同数据收集器的值（不管是否有预处理步骤），在添加到历史缓存之前，都要经过预处理管理器。基于套接字的IPC连接用于数据收集器(pollers, trappers等)和预处理进程之间的通讯。

只有Zabbix server 或 Zabbix proxy ( 如果主机被Zabbix proxy 监控) 执行预处理步骤，并处理从属监控项。

任何类型的监控项，甚至是从属监控项，都可以设置为主要监控项。额外级别的从属监控项可以被用来从现有的从属监控项中提取出更小的值。（译者注：以从属监控项作为主要监控项的次一级从属监控项，可以用于提取上级从属监控项所得到值，进行进一步处理。）

[comment]: # ({/8c2ba8a5-4eb72974})

[comment]: # ({2e95523b-3e1d96f5})
##### 局限性

-   只允许相同的主机（模板）从属项
-   监控项原型可以依赖于来自同一主机的另一个监控项原型或常规监控项
-   主要监控项的从属项最大计数被限制为29999（不考虑从属级别的数量）
-   最大允许3个从属级别
-   当从属监控项使用主机上来自模板的监控项作为主要监控项时，不允许到处为XML

[comment]: # ({/2e95523b-3e1d96f5})

[comment]: # ({c5aee667-3f705816})

#### 监控项配置

从属监控项依赖于它主要监控项的数据，这就是为什么必须首先配置 **主要监控项** (或主要监控项已存在)。

-   进入:  *Configuration*  →  *Hosts*
-   在主机那一行点击  *Items*
-   点击  *Create item*
-   下表中输入监控项的参数

![](../../../../../assets/en/manual/config/items/itemtypes/master_item.png)

所有标有红色星号的为必填字段。

点击 *Add* 保存主监控项。

接着，你可以配置 **从属监控项**。

![](../../../../../assets/en/manual/config/items/itemtypes/dependent_item.png)

所有标有红色星号的为必填字段。

必要的从属监控项的特定信息的字段是：

|   |   |
|---|---|
|*Type*|这里选择 **Dependent item** 。|
|*Key*|输入一个用于识别监控项的键。|
|*Master item*|选择主要监控项。 主要监控项的值将用于填充从属监控项的值。|
|*Type of information*|选择与将要存储的数据格式相对应的信息类型 |

你可以使用监控项值的 [预处理](/manual/config/items/item#item_value_preprocessing) 来提取主要监控项的监控值中所需部分。

![](../../../../../assets/en/manual/config/items/itemtypes/dependent_item_preprocessing.png){width="600"}

如果不进行预处理，从属监控项的值将与主要监控项的值完全相同。

点击  *ADD*  保存从属监控项。

使用在监控项列表中的向导功能，是快速创建从属监控项的方法。

![](../../../../../assets/en/manual/config/items/itemtypes/dependent_item_wizard.png)

[comment]: # ({/c5aee667-3f705816})

[comment]: # ({766486f1-8323a97a})
##### 展示

在监控项列表中，从属监控项以其主要监控项的名称作为前缀显示。

![](https://www.zabbix.com/documentation/5.0/assets/zh/manual/config/items/itemtypes/dependent_items.png)

如果主监控项被删除，那么它的所有从属监控项也将会被删除。

[comment]: # ({/766486f1-8323a97a})
