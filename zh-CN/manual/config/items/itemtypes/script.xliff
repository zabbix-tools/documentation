<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="zh-CN" datatype="plaintext" original="manual/config/items/itemtypes/script.md">
    <body>
      <trans-unit id="1a1e2756" xml:space="preserve">
        <source># 18 Script items</source>
        <target state="needs-translation"># 18 脚本监控项</target>
      </trans-unit>
      <trans-unit id="cb89bc38" xml:space="preserve">
        <source>### Overview

Script items can be used to collect data by executing a user-defined
JavaScript code with the ability to retrieve data over HTTP/HTTPS. In
addition to the script, an optional list of parameters (pairs of name
and value) and timeout can be specified.

This item type may be useful in data collection scenarios that require
multiple steps or complex logic. As an example, a Script item can be
configured to make an HTTP call, then process the data received in the
first step in some way, and pass transformed value to the second HTTP
call.

Script items are processed by Zabbix server or proxy pollers.</source>
        <target state="needs-translation">#### 概览

脚本监控项可以通过执行用户自定义的JavaScript代码，检索HTTP/HTTPS的方式来收集数据。除了脚本外，可以指定一个可选的参数列表（一些键值对）并配置超时限制。

此监控项类型在有收集数据过程中需要多个步骤或复杂逻辑的场景非常有用。举个例子，一个脚本监控项可以被配置为执行一个HTTP调用，然后经过某些方式处理从第一步调用得到的数据，并将转换后的数值传递给第二个HTTP调用。

脚本监控项有Zabbix server 或 Zabbix proxy的轮询器</target>
      </trans-unit>
      <trans-unit id="7ed55e87" xml:space="preserve">
        <source>### Configuration

In the *Type* field of [item configuration
form](/manual/config/items/item) select Script then fill out required
fields.

![script\_item.png](../../../../../assets/en/manual/config/items/itemtypes/script_item.png)

All mandatory input fields are marked with a red asterisk.

The fields that require specific information for Script items are:

|Field|Description|
|--|--------|
|Key|Enter a unique key that will be used to identify the item.|
|Parameters|Specify the variables to be passed to the script as the attribute and value pairs.&lt;br&gt;[User macros](/manual/config/macros/user_macros) are supported. To see which built-in macros are supported, do a search for "Script-type item" in the [supported macro](/manual/appendix/macros/supported_by_location) table.|
|Script|Enter JavaScript code in the block that appears when clicking in the parameter field (or on the view/edit button next to it). This code must provide the logic for returning the metric value.&lt;br&gt;The code has access to all parameters, it may perform HTTP GET, POST, PUT and DELETE requests and has control over HTTP headers and request body.&lt;br&gt;See also: [Additional JavaScript objects](/manual/config/items/preprocessing/javascript/javascript_objects), [JavaScript Guide](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide).|
|Timeout|JavaScript execution timeout (1-60s, default 3s); exceeding it will return error.&lt;br&gt;Time suffixes are supported, e.g. 30s, 1m.&lt;br&gt;Depending on the script it might take longer for the timeout to trigger. |</source>
      </trans-unit>
      <trans-unit id="4c860844" xml:space="preserve">
        <source>### Examples</source>
        <target state="needs-translation">#### 示例</target>
      </trans-unit>
      <trans-unit id="9c6c78ee" xml:space="preserve">
        <source>##### Simple data collection

Collect the content of *https://www.example.com/release\_notes*:

-   Create an item with type "Script".
-   In the *Script* field, enter:

```javascript
var request = new HttpRequest();
return request.get("https://www.example.com/release_notes");
```</source>
        <target state="needs-translation">##### 简单的数据采集

从 *https://www.example.com/release\_notes* 页面收集内容:

- 创建一个监控项，类型选择”脚本“。 \
- 在*脚本*字段填写下面的代码：

``` {.java}
var request = new HttpRequest();
return request.get("https://www.example.com/release_notes");
```</target>
      </trans-unit>
      <trans-unit id="7115c5ef" xml:space="preserve">
        <source>##### Data collection with parameters

Collect the content of a specific page and make use of parameters: 

- Create an item with type "Script" and two parameters:
    - **url : {$DOMAIN}** (the user macro {$DOMAIN} should be defined, preferably on the host level)
    - **subpage : /release_notes**

![](../../../../../assets/en/manual/config/items/itemtypes/script_example1.png){width=600}

- In the *Script* field, enter: 

```javascript
var obj = JSON.parse(value);
var url = obj.url;
var subpage = obj.subpage;
var request = new HttpRequest();
return request.get(url + subpage);
```</source>
      </trans-unit>
      <trans-unit id="18a089f4" xml:space="preserve">
        <source>##### Multiple HTTP requests

Collect the content of both *https://www.example.com* and
*https://www.example.com/release\_notes*:

-   Create an item with type "Script".
-   In the *Script* field, enter:

```javascript
var request = new HttpRequest();
return request.get("https://www.example.com") + request.get("https://www.example.com/release_notes");
```</source>
        <target state="needs-translation">##### 多个HTTP请求

同时从 *https://www.example.com* 和 *https://www.example.com/release\_notes*两个站点收集数据:

- 创建一个监控项，类型选择”脚本“。 
- 在*脚本*字段填写下面的代码：

``` {.java}
var request = new HttpRequest();
return request.get("https://www.example.com") + request.get("https://www.example.com/release_notes");
```</target>
      </trans-unit>
      <trans-unit id="d837bcdd" xml:space="preserve">
        <source>##### Logging

Add the "Log test" entry to the Zabbix server log and receive the item
value "1" in return:

-   Create an item with type "Script".
-   In the *Script* field, enter:

```javascript
Zabbix.log(3, 'Log test');
return 1;
```</source>
        <target state="needs-translation">##### 记录日志

添加"Log test"到Zabbix server 日志并返回数值 "1"给监控项：

- 创建一个监控项，类型选择”脚本“。 
- 在*脚本*字段填写下面的代码：

``` {.java}
Zabbix.log(3, 'Log test');
return 1;
```</target>
      </trans-unit>
    </body>
  </file>
</xliff>
