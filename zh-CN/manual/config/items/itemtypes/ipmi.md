[comment]: # translation:outdated

[comment]: # ({new-52403269})
# 4 IPMI检查

[comment]: # ({/new-52403269})

[comment]: # ({new-2f30e610})
#### 概述

你可以在Zabbix中监控智能平台管理接口（IPMI）设备的运行状况和可用性。要执行IPMI检查，Zabbix服务器必须首先[配置](/zh/manual/installation/install#from_the_sources)IPMI支持。

IPMI是计算机系统的远程"关闭"或“带外”管理的标准接口。它可以独立于操作系统直接从所谓的“带外”管理卡监视硬件状态。

Zabbix IPMI监控仅适用于支持IPMI的设备(HP iLO, DELL DRAC, IBM RSA, Sun
SSP, 等等).

从Zabbix
3.4开始，添加了一个新的IPMI管理器进程来安排IPMI轮询器进行IPMI检查。现在，主机始终只由一个IPMI轮询器轮询，从而减少了与BMC控制器的打开连接数。通过这些更改，可以安全地增加IPMI轮询器的数量，而无需担心BMC控制器过载。
启动至少一个IPMI轮询器时，将自动启动IPMI管理器进程。

也可以参考IPMI检查的[已知问题](/zh/manual/installation/known_issues#ipmi_checks)。

[comment]: # ({/new-2f30e610})

[comment]: # ({new-5d32b87c})
#### 配置

[comment]: # ({/new-5d32b87c})

[comment]: # ({new-4f35db5e})
##### 主机配置

主机必须配置为处理IPMI检查。必须添加IPMI接口，必须定义相应的IP和端口号，并且必须定义IPMI认证参数。

更多细节请查看[主机定义](/zh/manual/config/hosts/host)。

[comment]: # ({/new-4f35db5e})

[comment]: # ({new-f7bb9e76})
##### 服务器配置

默认情况下，Zabbix服务器未配置为启动任何IPMI轮询，因此任何添加的IPMI监控项将无法正常工作。要更改此选项，请以root身份打开Zabbix服务器配置文件([zabbix\_server.conf](/zh/manual/appendix/config/zabbix_server))，并查找以下行：

    # StartIPMIPollers=0

取消注释，并设置poller计数为3，如下：

    StartIPMIPollers=3

保存文件，然后重新启动zabbix\_server。

[comment]: # ({/new-f7bb9e76})

[comment]: # ({new-b0db4658})
##### 监控项配置

配置主机级别的[监控项](/zh/manual/config/items/item)时：

-   对于 *主机接口* ，选择IPMI IP和端口
-   选择'IPMI agent'作为 *类型*
-   指定 *IPMI传感器* （例如在Dell Poweredge上的 'FAN MOD 1A RPM'
    ）。默认情况下，应指定传感器ID。 也可以在值之前使用前缀：

```{=html}
<!-- -->
```
        * ''id:'' - 指定传感器ID；
        * ''name:'' - 指定传感器全名。 这在传感器只能通过指定全名来区分的情况下非常有用。
    * 在主机中输入唯一的监控项[[zh:manual:config:items:item:key|key]]（例如，ipmi.fan.rpm）
    * 在这种情况下，选择相应的信息类型 ('Numeric (float)'，对于离散传感器 - 'Numeric (unsigned)')，单位(最可能的'rpm')和任何其它必需监控项属性

[comment]: # ({/new-b0db4658})

[comment]: # ({new-af2b187b})
#### 超时和会话终止

IPMI消息超时和重试计数在OpenIPMI库中定义。由于目前OpenIPMI的设计，无论在接口还是监控项级别都不能在Zabbix中使这些值进行配置。

LAN的IPMI会话不活动超时时间为60 +/-
3秒。目前无法使用OpenIPMI定期发送激活会话命令。如果没有从Zabbix到特定BMC的IPMI项检查超过在BMC中配置的会话超时，则超时超时后的下一次IPMI检查将由于单个消息超时、重试或接收错误而超时。之后，打开一个新的会话，并启动BMC的完全重新扫描。如果要避免BMC的不必要的rescans，建议将IPMI监控项轮询间隔设置为低于BMC中配置的IPMI会话不活动超时。

[comment]: # ({/new-af2b187b})

[comment]: # ({new-1e03516a})
#### 关于IPMI离散传感器的注意事项

要在主机上找到传感器启动Zabbix服务器，启用**DebugLevel=4**。等待几分钟，并在Zabbix服务器日志文件中查找传感器发现记录：

    $ grep 'Added sensor' zabbix_server.log
    8358:20130318:111122.170 Added sensor: host:'192.168.1.12:623' id_type:0 id_sz:7 id:'CATERR' reading_type:0x3 ('discrete_state') type:0x7 ('processor') full_name:'(r0.32.3.0).CATERR'
    8358:20130318:111122.170 Added sensor: host:'192.168.1.12:623' id_type:0 id_sz:15 id:'CPU Therm Trip' reading_type:0x3 ('discrete_state') type:0x1 ('temperature') full_name:'(7.1).CPU Therm Trip'
    8358:20130318:111122.171 Added sensor: host:'192.168.1.12:623' id_type:0 id_sz:17 id:'System Event Log' reading_type:0x6f ('sensor specific') type:0x10 ('event_logging_disabled') full_name:'(7.1).System Event Log'
    8358:20130318:111122.171 Added sensor: host:'192.168.1.12:623' id_type:0 id_sz:17 id:'PhysicalSecurity' reading_type:0x6f ('sensor specific') type:0x5 ('physical_security') full_name:'(23.1).PhysicalSecurity'
    8358:20130318:111122.171 Added sensor: host:'192.168.1.12:623' id_type:0 id_sz:14 id:'IPMI Watchdog' reading_type:0x6f ('sensor specific') type:0x23 ('watchdog_2') full_name:'(7.7).IPMI Watchdog'
    8358:20130318:111122.171 Added sensor: host:'192.168.1.12:623' id_type:0 id_sz:16 id:'Power Unit Stat' reading_type:0x6f ('sensor specific') type:0x9 ('power_unit') full_name:'(21.1).Power Unit Stat'
    8358:20130318:111122.171 Added sensor: host:'192.168.1.12:623' id_type:0 id_sz:16 id:'P1 Therm Ctrl %' reading_type:0x1 ('threshold') type:0x1 ('temperature') full_name:'(3.1).P1 Therm Ctrl %'
    8358:20130318:111122.172 Added sensor: host:'192.168.1.12:623' id_type:0 id_sz:16 id:'P1 Therm Margin' reading_type:0x1 ('threshold') type:0x1 ('temperature') full_name:'(3.2).P1 Therm Margin'
    8358:20130318:111122.172 Added sensor: host:'192.168.1.12:623' id_type:0 id_sz:13 id:'System Fan 2' reading_type:0x1 ('threshold') type:0x4 ('fan') full_name:'(29.1).System Fan 2'
    8358:20130318:111122.172 Added sensor: host:'192.168.1.12:623' id_type:0 id_sz:13 id:'System Fan 3' reading_type:0x1 ('threshold') type:0x4 ('fan') full_name:'(29.1).System Fan 3'
    8358:20130318:111122.172 Added sensor: host:'192.168.1.12:623' id_type:0 id_sz:14 id:'P1 Mem Margin' reading_type:0x1 ('threshold') type:0x1 ('temperature') full_name:'(7.6).P1 Mem Margin'
    8358:20130318:111122.172 Added sensor: host:'192.168.1.12:623' id_type:0 id_sz:17 id:'Front Panel Temp' reading_type:0x1 ('threshold') type:0x1 ('temperature') full_name:'(7.6).Front Panel Temp'
    8358:20130318:111122.173 Added sensor: host:'192.168.1.12:623' id_type:0 id_sz:15 id:'Baseboard Temp' reading_type:0x1 ('threshold') type:0x1 ('temperature') full_name:'(7.6).Baseboard Temp'
    8358:20130318:111122.173 Added sensor: host:'192.168.1.12:623' id_type:0 id_sz:9 id:'BB +5.0V' reading_type:0x1 ('threshold') type:0x2 ('voltage') full_name:'(7.1).BB +5.0V'
    8358:20130318:111122.173 Added sensor: host:'192.168.1.12:623' id_type:0 id_sz:14 id:'BB +3.3V STBY' reading_type:0x1 ('threshold') type:0x2 ('voltage') full_name:'(7.1).BB +3.3V STBY'
    8358:20130318:111122.173 Added sensor: host:'192.168.1.12:623' id_type:0 id_sz:9 id:'BB +3.3V' reading_type:0x1 ('threshold') type:0x2 ('voltage') full_name:'(7.1).BB +3.3V'
    8358:20130318:111122.173 Added sensor: host:'192.168.1.12:623' id_type:0 id_sz:17 id:'BB +1.5V P1 DDR3' reading_type:0x1 ('threshold') type:0x2 ('voltage') full_name:'(7.1).BB +1.5V P1 DDR3'
    8358:20130318:111122.173 Added sensor: host:'192.168.1.12:623' id_type:0 id_sz:17 id:'BB +1.1V P1 Vccp' reading_type:0x1 ('threshold') type:0x2 ('voltage') full_name:'(7.1).BB +1.1V P1 Vccp'
    8358:20130318:111122.174 Added sensor: host:'192.168.1.12:623' id_type:0 id_sz:14 id:'BB +1.05V PCH' reading_type:0x1 ('threshold') type:0x2 ('voltage') full_name:'(7.1).BB +1.05V PCH'

要解码IPMI传感器类型和状态，请在http:*www.intel.com/content/www/us/en/servers/ipmi/ipmi-specifications.html
(在撰写本文时，最新的文件是http:*www.intel.com/content/dam/www/public/us/en/documents/product-briefs/second-gen-interface-spec-v2.pdf)获取IPMI
2.0规范的副本

开始的第一个参数是“reading\_type”。从规范中使用“表42-1，事件/读取类型代码范围”来解码“reading\_type”代码。我们示例中的大多数传感器都有“reading\_type：0x1”，这意味着是“threshold”传感器。
“表42-3，传感器类型代码”表示：“类型：0x1”表示温度传感器；“类型：0x2” -
电压传感器；“类型：0x4” -
风扇等阈值传感器有时称为“模拟”传感器，因为它们测量连续参数，如温度，电压，每分钟转数。

另一个例子 -
一个带有“read\_type：0x3”的传感器。“表42-1，事件/读取类型代码范围”表示读取类型代码02h-0Ch表示“通用离散”传感器。离散传感器具有多达15个可能的状态（换句话说-最多15个有意义的位）。例如，对于具有“type：0x7”的传感器“CATERR”，“表42-3，传感器类型代码”表示此类型“处理器”，各个位的含义是：00h（最低有效位）-
IERR ；01h - 散热等。

在我们的示例中有几个传感器具有“reading\_type：0x6f”。对于这些传感器，“表42-1，事件/读取类型代码范围”建议使用“表42-3，传感器类型代码”来解码位的含义。
例如，传感器“Power Unit Stat”的类型为“0x9”，表示“Power Unit”。 Offset
00h表示“PowerOff / Power Down”。
换句话说，如果最低有效位为1，则服务器断电。为了测试这个位，可以使用**band**与掩码1的功能。触发表达式可能就像

       {www.zabbix.com:Power Unit Stat.band(#1,1)}=1

警告服务器关机。

[comment]: # ({/new-1e03516a})

[comment]: # ({new-3ea36861})
##### 关于OpenIPMI-2.0.16,2.0.17,2.0.18和2.0.19中离散传感器名称的注释

OpenIPMI-2.0.16,2.0.17和2.0.18中的离散传感器的名称通常在附近附加一个额外的
"`0`" （或其它数字或字母）。例如，当 `ipmitool`
和OpenIPMI-2.0.19将传感器名称显示为 "`PhysicalSecurity`" 或
"`CATERR`"时，在OpenIPMI-2.0.16,2.0.17和2.0.18中，名称分别为
"`PhysicalSecurity0`" 或 "`CATERR0`"。

当使用OpenIPMI-2.0.16,2.0.17和2.0.18配置IPMI项目时，请在IPMI代理监控项的IPMI传感器字段中使用以“0”结尾的名称。当你的Zabbix服务器升级到使用OpenIPMI-2.0.19（或更高版本）的新Linux发行版时，具有这些IPMI离散传感器的监控项将变为“不支持”。
你必须更改其IPMI传感器名称（最后删除“0”），并等待一段时间才能再次转为"Enabled"。

[comment]: # ({/new-3ea36861})

[comment]: # ({new-761eb9da})
##### 关于阈值和离散传感器同时可用的注意事项

一些IPMI代理提供了相同名称的阈值传感器和离散传感器。在2.2.8和2.4.3之前的Zabbix版本中，选择了第一个提供的传感器。从2.2.8和2.4.3版本以后，偏向于阈值传感器。

[comment]: # ({/new-761eb9da})

[comment]: # ({new-2adee003})
##### 连接终止注意事项

如果不执行IPMI检查（由于任何原因：所有主机IPMI监控项禁用/不支持、主机已禁用/已删除、主机维护等），IPMI连接将从Zabbix服务器或代理服务器终止3到4小时，具体时间取决于Zabbix服务器/代理服务器何时启动。

[comment]: # ({/new-2adee003})


[comment]: # ({new-69164d8f})
##### Notes on connection termination

If IPMI checks are not performed (by any reason: all host IPMI items
disabled/notsupported, host disabled/deleted, host in maintenance etc.)
the IPMI connection will be terminated from Zabbix server or proxy in 3
to 4 hours depending on the time when Zabbix server/proxy was started.

[comment]: # ({/new-69164d8f})
