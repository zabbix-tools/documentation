[comment]: # translation:outdated

[comment]: # ({new-b8c65ad1})
# 7 可计算监控项

[comment]: # ({/new-b8c65ad1})

[comment]: # ({e1643671-43ebe58c})
#### 概述

使用计算项目，可以根据其他项目的值创建计算。

计算可以同时使用：

-   单个项目的单一值
-   用于选择多个项目进行聚合的复杂过滤器(有关详细信息，参阅[聚合计算](/manual/config/items/itemtypes/calculated/aggregate))

因此，计算项是创建虚拟数据源的一种方式。所有计算仅由 Zabbix server 完成。这些值是根据所使用的算术表达式定期计算的。

结果数据与任何其他项目一样存储在 Zabbix 数据库中；历史和趋势值都被存储并且可以生成图表。

::: 注意
如果计算结果是浮点值，如果计算的信息项类型为*Numeric (unsigned)*，则将其修剪为整数。
:::

计算项与触发器[表达式](/manual/config/triggers/expression)共享它们的语法。在计算项中允许与字符串进行比较。计算项可以由宏或与任何其他项类型相同的其他实体引用。

要使用计算项目，请选择项目类型 **Calculated**。

[comment]: # ({/e1643671-43ebe58c})

[comment]: # ({d3937e48-ddc35784})
#### 可配置字段

 **key** 是唯一的监控项标识符(每个主机)。您可以使用支持的符号创建任何键名。

应在 **Formula** 字段中输入计算定义。公式和密钥之间几乎没有任何联系。公式中不以任何方式使用关键参数。

一个简单公式的语法是：

    function(/host/key,<parameter1>,<parameter2>,...)

这里:

|   |   |
|---|---|
|*function*|[支持的函数](/manual/appendix/functions)之一: last, min, max, avg, count, 等|
|*host*|用于计算的主机监控项<br>当前主机可以省略 (如下所示 `function(//key,parameter,...)`).|
|*key*|用于计算的监控项的键|
|*parameter(s)*|函数的参数，如果需要。|

::: 重要提示
如果用于引用函数参数或常量，公式中的[用户宏](/manual/config/macros/user_macros)将被扩展，如果引用函数、主机名、项目键、项目键参数或运算符，则不会扩展用户宏。
:::

更复杂的公式可以使用函数、运算符和括号的组合。您可以使用触发器表达式中支持的所有函数和
[运算符](/manual/config/triggers/expression#operators)。逻辑和运算符优先级完全相同。

与触发器表达式不同，Zabbix 根据项目更新间隔处理计算项目，而不是在接收到新值时。

计算项目公式中历史函数引用的所有项目都必须存在并且正在收集数据。此外，如果您更改引用项目的项目键，则必须使用该键手动更新任何公式。

在以下几种情况下，计算项可能会变得不受支持：

-   参考监控项
    -   没有找到
    -   被禁用
    -   属于禁用的主机
    -   不支持 (除了 nodata() 函数和具有未知值的 [运算符](/manual/config/triggers/expression#operators))
-   没有数据来计算函数
-   被整除
-  使用了不正确的语法

[comment]: # ({/d3937e48-ddc35784})

[comment]: # ({new-82c22f4f})
#### 用法示例

[comment]: # ({/new-82c22f4f})

[comment]: # ({47b349b2-426ab71a})
##### 示例 1

计算 '/'上可用磁盘空间的百分比。

使用函数 **last**:

    100*last(//vfs.fs.size[/,free])/last(//vfs.fs.size[/,total])
Zabbix 将获取可用和总磁盘空间的最新值，并根据给定的公式计算百分比。

[comment]: # ({/47b349b2-426ab71a})

[comment]: # ({edee601e-77bcdfd9})
##### 示例 2

计算 Zabbix 处理的值的 10 分钟平均值。

使用函数 **avg**:

    avg(/Zabbix Server/zabbix[wcache,values],10m)

请注意，长时间使用计算项可能会影响 Zabbix 服务器的性能。

[comment]: # ({/edee601e-77bcdfd9})

[comment]: # ({af3854ab-5d068733})
##### 示例 3

计算eth0的总带宽

两个函数之和:

    last(//net.if.in[eth0,bytes])+last(//net.if.out[eth0,bytes])

[comment]: # ({/af3854ab-5d068733})

[comment]: # ({9d22eb27-f8b53af1})
##### 示例 4

计算入站流量的百分比

更为复杂的表达式：

    100*last(//net.if.in[eth0,bytes])/(last(//net.if.in[eth0,bytes])+last(//net.if.out[eth0,bytes]))

另请参阅: [聚合计算示例](/manual/config/items/itemtypes/calculated/aggregate#usage_examples)

[comment]: # ({/9d22eb27-f8b53af1})
