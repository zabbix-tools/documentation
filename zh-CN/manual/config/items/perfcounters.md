[comment]: # translation:outdated

[comment]: # ({new-f89c7380})
# 6 Windows性能计数器

[comment]: # ({/new-f89c7380})

[comment]: # ({new-47e1ae8b})
#### 概览

你可以使用 *perf\_counter\[\]* 这个key有效的监控Windows性能计数器。\

例如：

    perf_counter["\Processor(0)\Interrupts/sec"]

或

    perf_counter["\Processor(0)\Interrupts/sec", 10]

有关使用此key的更多信息请参阅[Windows专用监控项](/zh/manual/config/items/itemtypes/zabbix_agent/win_keys)。

为了获取可用于监控的新能计数器完整列表，你可以运行：

    typeperf -qx

[comment]: # ({/new-47e1ae8b})

[comment]: # ({new-38d80c32})
#### 数字表示

由于性能计数器的命名在不同的Windows服务器上可能不同，这取决于服务器的地区设置。因此，在创建用于监控具有不同地区设置的多台Windows设备的模板时，会引发一定的问题。\

同时，每个新能计数器也可以通过其数字形式来引用，无论如何，数字形式都是唯一的，因此你可以使用数字表示而不是字符串。\

为了找到同义的数字，需要运行 **regedit**
，然后找到*HKEY\_LOCAL\_MACHINE\\SOFTWARE\\Microsoft\\Windows
NT\\CurrentVersion\\Perflib\\009*这个注册表。\

注册表中包含形如下面所示的信息：

    1
    1847
    2
    System
    4
    Memory
    6
    % Processor Time
    10
    File Read Operations/sec
    12
    File Write Operations/sec
    14
    File Control Operations/sec
    16
    File Read Bytes/sec
    18
    File Write Bytes/sec
    ....

这样你就可以找到性能计数器每个字符串对应的数字，例如：

    System → 2
    % Processor Time → 6

然后你就可以使用这些数字来表示性能计数器路径：

    \2\6

[comment]: # ({/new-38d80c32})

[comment]: # ({new-96efda14})
#### 性能计数器参数

你可以部署一些PerfCounter参数，来完成通过Windows性能计数器监控。\

例如，你可以将下面的内容添加到ZABBIX代理配置文件中：

       PerfCounter=UserPerfCounter1,"\Memory\Page Reads/sec",30
       or
       PerfCounter=UserPerfCounter2,"\4\24",30

配置了这些参数后，你就可以简单的使用 *UserPerfCounter1* 或
*UserPerfCounter2* 作为key来创建相应的监控项。\

当然，别忘了在更改了配置文件后重新启动ZABBIX Agent。\

#### 故障处理

有时ZABBIX Agent 不能再基于Windows
2000的系统中检索性能计数器的值，因为pdh.dll文件已过时。这个错误会在ZABBIX
Agent和Server的日志文件中会有失败信息。在这种情况下，phd.dll应当被更新到更新的
5.0.2195.2668 版本。

[comment]: # ({/new-96efda14})
