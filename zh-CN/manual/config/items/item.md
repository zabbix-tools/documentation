[comment]: # translation:outdated

[comment]: # ({new-50342b23})
# 1 创建监控项

[comment]: # ({/new-50342b23})

[comment]: # ({2cfcc77a-d3a04061})
#### 概述

要在Zabbix管理页面创建一个监控项，请执行以下操作：

-   进入到: *配置* → *主机*
-   在主机所在的行单击 *监控项*
-   点击屏幕右上角的*创建监控项*
-   输入表单中监控项的参数

你也可以打开一个监控项，按*克隆* 按钮，然后以不同的名称保存。

[comment]: # ({/2cfcc77a-d3a04061})

[comment]: # ({new-d0a0a311})
#### 配置

**监控项** 选项卡包含了常规监控项属性：

![](../../../../assets/zh/manual/config/items/item_zh.png){width="600"}

所有必填输入字段都标有红色星号。

| 参数  | 描述  |
|---|---|
|*名称*                   |监控项名称。<br>可以使用以下宏：<br>**$1, $2...$9** - 指的是监控项的第1、2...9个参数<br>例如：$1上的可用磁盘空间<br>如果监控项的key是 "vfs.fs.size\[/,free\]", 说明将自动更改为 "Free disk space on /"|
|*类型*                   |监控项类型。参考单个 [监控项类型](itemtypes) 章节.|
|*键值*                   |监控项键值.<br>可支持的[监控项键值](itemtypes) 能够在各个监控项类型中找到。<br>这个键值在单个主机中必须是唯一的。<br>如果键值的类型是'Zabbix 客户端'、'Zabbix客户端(主动式)', '简单检查' 或者 'Zabbix整合', 则此key必须被 Zabbix 客户端 或者 Zabbix 服务端 支持。<br>也可以查看: 正确的 [键值的格式](/zh/manual/config/items/item/key).|
|*信息类型*               |转换后存储在数据库中的数据类型（如果有）。<br>**数字(无正负)** - 64位无符号整数<br>**数字(浮点)** - 64位浮点数<br>允许大约15位的精度，范围从-1.79E+308到1.79E+308(with exception of [PostgreSQL 11和更早版本](/manual/installation/known_issues#floating_point_values)除外)<br>也支持科学计数值。例如:1.23E+7, 1e308, 1.1E-4。<br>**字符** - 短文本数据<br>**日志** - 具有可选日志相关属性的长文本数据(timestamp（时间戳）, source（源）, severity（严重性）, logeventid（日志事件id）)<br>**文本** - 长文本数据。可参见 [文本数据限制](#Text_data_limits)。<br>针对返回的数据是指定格式的监控项键值，匹配的信息类型会自动选择。|
|*主机接口*               |主机接口。编辑主机级别的监控项时，此字段可用。|
|*单位*                   |如果设置了单位符号，Zabbix将在收到数据后再加工处理，并使用设置单位后缀进行显示。<br>默认情况下，如果原始值超过1000，则除以1000，并相应显示。 例如，如果设置 *bps* 并接收到值为881764，则将显示为881.76 Kbps。<br> [JEDEC](https://en.wikipedia.org/wiki/JEDEC_memory_standards) 存储器标准用于处理B（字节），Bps（每秒字节数）单位，除以1024。因此，如果单位设置为B或Bps，Zabbix将显示：<br>1 为 1B/1Bps<br>1024 为 1KB/1KBps<br>1536 为 1.5KB/1.5KBps<br>如果使用以下与时间相关的单位，则使用特殊处理：<br>**unixtime** - 转换成“yyyy.mm.dd hh：mm：ss”。 要正确转换，接收的值必须是数字（无符号）类型的信息。<br>**uptime** - 转换为 "hh:mm:ss" 或者 "N days, hh:mm:ss"<br>例如，如果你收到的值为881764（秒），则显示为“10天，04:56:04”<br>**s** - 转换成“yyy mmm ddd hhh mmm sss ms”; 参数被视为秒数。<br>例如，如果您收到的值为881764（秒），则显示为“10d 4h 56m”<br>只显示3个主要单位，如“1m 15d 5h”或“2h 4m 46s”。 如果没有显示天数，则仅显示两个级别 - “1m 5h”（不显示分钟，秒或毫秒）。 如果该值小于0.001，将被转换成“<1 ms”。<br>注意，如果单位带有前缀“!”，单元前缀/处理不会应用到监控项的值，请参阅 [单位黑名单](#unit_blacklist).|
|*更新间隔*               |每N秒检索一次这个项目的新值。允许的最大更新间隔为86400秒（1天）。<br>支持[时间后缀](/zh/manual/appendix/suffixes)，例如 30s，1m，2h，1d。<br>支持[用户宏](/zh/manual/config/macros/usermacros)。<br>单个宏必须填充整个字段。不支持字段中的多个宏或文本混合的宏。<br>*注意*：仅当自定义间隔存在非零值时，更新间隔才能设置为“ 0”。如果设置为“ 0”，并且存在自定义间隔（灵活的或计划的）且具有非零值，则将在自定义间隔持续时间内轮询该项目。<br>*注意* 项目成为活动后或更新间隔更改后的第一个项目轮询可能发生在配置值之前。<br>可以通过按*立即检查*[按钮](#form_buttons)立即轮询现有被动监控项的值。|
|*自定义时间间隔*         |你可以创建用于检查监控项的自定义规则：<br>**Flexible**（灵活） - 为 *更新间隔* (不同频率的间隔)创建一个特例。<br>**Scheduling**（调度）- 创建自定义轮询时间表。<br>详细信息请查看 [自定义间隔](/zh/manual/config/items/item/custom_intervals)。<br>*间隔* 字段支持[时间后缀](/zh/manual/appendix/suffixes) , 例如 30s, 1m, 2h, 1d.<br>支持[用户宏](/zh/manual/config/macros/usermacros)。<br>从Zabix 3.0.0开始支持时间表。<br>*注意*: 不适用于Zabbix Agent的活动监控项。|
|*历史数据保留时长*|以下可供选择：<br>**不存储历史数据** - 不存储监控项历史数据。如果只有依赖项需要保留历史记录，则对主监控项很有用。<br>此设置不能被全局管家 [设置](/zh/manual/web_interface/frontend_sections/administration/general#housekeeper)覆盖。<br>**存储时长** - 指定将详细历史数据保留在数据库中的时长(1小时至25年)。 过期数据将被housekeeper管家删除。按秒存储。<br>支持[时间后缀](/zh/manual/appendix/suffixes)，例如2h，1d。支持[用户宏](/zh/manual/config/macros/user_macros)<br> *存储时长* 可以被 *Administration（管理） → General（通用） → [管家](/zh/manual/web_interface/frontend_sections/administration/general#housekeeper)*中的值覆盖。<br>如果存在全局设置，则显示绿色告警信息，![](../../../../assets/en/manual/config/info.png)。当鼠标移到上面时会提示告警信息，例如*被全局管家设置(1d)覆盖*.<br>建议保留最小可能天数的历史数据，以减轻数据库存储压力。可以保留更长的趋势数据来替代历史数据。<br>更多内容请参考[历史与趋势](/zh/manual/config/items/history_and_trends)。|
|*趋势存储时间*|以下可供选择：<br>**不存储趋势数据** - 将不会存储趋势数据。<br>此设置不能被全局管家[设置](/manual/web_interface/frontend_sections/administration/general#housekeeper)覆盖。<br>**存储时长** - 指定在数据库中保存聚合（每小时，最大，平均，计数）历史数据的时长（1天至25年）。管家将删除较旧的数据。按秒存储。<br>支持[时间后缀](/zh/manual/appendix/suffixes)，例如2h，1d。支持[用户宏](/zh/manual/config/macros/user_macros)<br>*存储时长*可以在*Administration（管理） → General（通用） → [管家](/zh/manual/web_interface/frontend_sections/administration/general#housekeeper)*中的值覆盖。<br>如果存在全局设置，将显示一条绿色的警告消息![](../../../../assets/en/manual/config/info.png)。当鼠标移到上面时会提示告警信息，例如：*被全局管家设置(7d)覆盖*.<br>*注意:* 保持趋势不适用于非数字数据 - 字符、日志和文本。<br>更多信息请参考 [历史与趋势](/zh/manual/config/items/history_and_trends).|
|*值映射*|将值映射应用于此监控项。[值映射](/zh/manual/config/items/mapping) 不会改变收到的值，仅用于显示数据。<br>适用于 *数值型(无符号)*, *数值型(浮点型)* 和 *字符型* 监控项。<br>例如："Windows service states".|
|*日志时间格式*           仅适用于*|日志**类型的监控项。 支持的占位符:<br>\* **y**: *年 (1970-2038)*<br>\* **M**: *月 (01-12)*<br>\* **d**: *日 (01-31)*<br>\* **h**: *小时 (00-23)*<br>\* **m**: *分钟 (00-59)*<br>\* **s**: *秒 (00-59)*<br>如果留空，则不会解析时间戳。<br>例如，从Zabbix Agent日志文件中考虑以下几行：“23480：20100328：154718.045 Zabbix代理启动。 Zabbix 1.8.2（修订11211）。“<br>它以PID的六个字符位置开始，后跟日期，时间和行的其余部分。<br>该行的日志时间格式为“pppppp：yyyyMMdd：hhmmss”。<br>注意，“p”和“:”字符只是占位符，只能是“yMdhms”。|
|*主机资产纪录字段*   |你可以选择项目的值填充的主机资产字段，如果你为主机启用了自动发现模式 [资产管理](/zh/manual/config/hosts/inventory)。<br>该字段在*信息类型*字段设置为'日志'时不可用。|
|*描述*                   |输入监控项描述。|
|*已启用*                 |选中复选框以启用该监控项，以便对其进行处理。|

::: noteclassic
监控项类型的特定字段在[对应页面](监控项类型)会有描述。
::: 
:::
 noteclassic
当编辑主机级别上的现有[模板](/zh/manual/config/templates)级别的监控项时，多个字段是只读的。你可以使用表单标题中的链接并转到模板级别并在其中进行编辑，但请记住，模板级别上的更改将更改模板链接到的所有主机的项目。
:::

**标签** 允许定义监控项级别的[标签](/zh/manual/config/tagging).

![](../../../../assets/en/manual/config/items/item_b.png)

[comment]: # ({/new-d0a0a311})

[comment]: # ({b60b557f-4f9077b8})
##### 监控项值预处理

 **预处理** 选项卡允许为接收到的值定义 [转换规则](/zh/manual/config/items/preprocessing) 。

[comment]: # ({/b60b557f-4f9077b8})

[comment]: # ({c1a46a61-006c45ea})
#### 测试

可以对监控项进行测试，如果配置正确，则可以返回实际值。甚至可以在保存项目之前进行测试。

可以对主机和模板的监控项、监控项原型和自动发现规则进行测试。agent（主动式）类型的监控项不能测试。

监控项测试可用于以下被动式监控项:

-   Zabbix agent
-   SNMP agent (v1, v2, v3)
-   IPMI agent
-   SSH checks
-   Telnet checks
-   JMX agent
-   Simple checks (except `icmpping*`, `vmware.*` items)
-   Zabbix internal
-   Calculated items
-   External checks
-   Database monitor
-   HTTP agent
-   Script

要测试监控项，请单击监控项配置表单底部的 *测试* 按钮。请注意，对于无法测试的监控项（例如简单检查之外的其他主动检查）， *测试* 按钮将被禁用。

![](../../../../assets/en/manual/config/items/item_test_button.png)

监控项测试表单包含主机参数(主机IP地址、端口、agent代理程序/无agent代理程序)和指定监控项（如SNMPv2团体或SNMPv3安全凭据）所需的字段。这些字段是上下文相关的:

-   所需的参数值可能是已填充的，例如，这个监控项是主机的监控项，所需信息就会从agent主机的接口传递过来
-   模板的监控项的相关参数需要手动填充
-   纯文本宏值会被解析
-   值(或部分值)为加密或Vault宏的字段为空，必须手动输入。 如果item参数中存在加密宏值，则显示如下警告信息:" 监控项包含用户定义的带有加密值的宏。这些宏的值应该手动输入。”
-   当在特定的监控项类型中上下文不需要的字段会被禁用（例如，在可计算和zabbix整合类型的监控项中主机地址地段被禁用，在可计算类型的监控项中，proxy字段被禁用）

要测试监控项，点击 *获取值*。如果成功获取到值，它会自动填充进对应的 *Value* (值) 字段，通过计算*Prev. 
time* （前一次）字段，即两个值之间的时间差(点击)，将当前值(如果有的话)移动到 *Previous value*（前一次值） 字段，并尝试检测EOL序列，在接收到的值中检测到“\\n\\r”时切换到CRLF。

![](../../../../assets/en/manual/config/items/item_test.png){width="600"}

如果配置不正确，则返回错误提示，并描述可能的原因。

![](../../../../assets/en/manual/config/items/item_test_error.png)

从主机接收成功的值也可以用于测试[预处理](/zh/manual/config/items/preprocessing#testing)。

[comment]: # ({/c1a46a61-006c45ea})

[comment]: # ({8ba0a88b-4b126a62})
#### 表单按钮

表单底部的按钮允许执行多种操作。

|   |   |
|---|---|
|![](../../../../assets/en/manual/config/button_add.png)|添加监控项。 此按钮仅适用于新监控项。|
|![](../../../../assets/en/manual/config/button_update.png)|更新监控项的属性。|
|![](../../../../assets/en/manual/config/button_clone.png)|根据当前监控项的属性创建另一个监控项。|
|![](../../../../assets/en/manual/config/button_execute.png)|立即执行新监控项值的检查。 仅支持 **passive** 检查 (参见 [更多详细信息](/zh/manual/config/items/check_now))。<br>*注意* 当立即检查值时，配置缓存不会更新，因此 该值不会反映项目配置的最新更改。|
|![](../../../../assets/en/manual/config/button_test.png)|通过获取一个值来验证监控项配置是否正确。|
|![](../../../../assets/en/manual/config/button_clear.png)|删除监控项历史记录和趋势。|
|![](../../../../assets/en/manual/config/button_delete.png)|删除监控项。|
|![](../../../../assets/en/manual/config/button_cancel.png)|取消编辑监控项属性。|

[comment]: # ({/8ba0a88b-4b126a62})

[comment]: # ({4d4e401d-8cb32bf5})
#### Text data limits

文本数据限制取决于数据库服务端。 在将文本值存储到数据库之前，它们会被截断以匹配数据库值类型限制：

|数据库|信息类型|||
|--------|-------------------|-|-|
| |**Character(字符)**|**Log（日志）**|**Text（文本）**|
|MySQL|255 characters|65536 bytes|65536 bytes|
|PostgreSQL|255 characters|65536 characters|65536 characters|
|Oracle|255 characters|65536 characters|65536 characters|

[comment]: # ({/4d4e401d-8cb32bf5})

[comment]: # ({45cfc7cf-aeb2b058})
#### 单位转换

默认情况下，为监控项指定单位会自动添加该单位的乘数前缀 - 例如，单位为“B”的传入值“2048”将显示为“2KB”。

但是，可以通过使用 `!` 前缀来阻止任何单位转换，例如 `!B`。为了更好地说明在有单位转换和没有单位转换的情况下转换的方式，请参见以下值和单位示例：

    1024 !B → 1024 B
    1024 B → 1 KB
    61 !s → 61 s
    61 s → 1m 1s
    0 !uptime → 0 uptime
    0 uptime → 00:00:00
    0 !! → 0 !
    0 ! → 0

::: noteclassic
在Zabbix 4.0之前，有一个硬编码的单位黑名单包括 `ms`, `rpm`, `RPM`, `%`。 这个黑名单已被弃用，因此将这些单位列入黑名单的正确方法是 `!ms`, `!rpm`, `!RPM`, `!%`。
:::

[comment]: # ({/45cfc7cf-aeb2b058})

[comment]: # ({1593f182-d4406a73})
#### 自定义脚本限制

可用的自定义脚本长度取决于使用的数据库:

|   |   |   |
|---|---|---|
|*数据库*|*字符限制*|*字节限制*|
|**MySQL**|65535|65535|
|**Oracle Database**|2048|4000|
|**PostgreSQL**|65535|无限制|
|**SQLite (仅Zabbix代理)**|65535|无限制|

[comment]: # ({/1593f182-d4406a73})


[comment]: # ({687b180a-2cd40177})
#### 不支持的监控项

如果由于某种原因无法检索到值，则该监控项可能不受支持。但是该监控项仍会以设定的 *[更新间隔](/manual/config/items/item?#configuration)*重新检索。

不支持的项目被报告为“不支持”状态。

[comment]: # ({/687b180a-2cd40177})
