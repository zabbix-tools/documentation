[comment]: # translation:outdated

[comment]: # ({new-b5fd3df7})
# 3 历史数据与趋势数据

[comment]: # ({/new-b5fd3df7})

[comment]: # ({new-e9319443})
#### 概述

历史数据（history）和趋势数据（trends）是Zabbix中存储收集到的数据的两种方式。\

历史数据：每一个收集到的监控数据\
趋势数据：按小时统计计算的平均值数据\

[comment]: # ({/new-e9319443})

[comment]: # ({new-e5d848c2})
#### 历史数据的留存

通过设置历史数据保留时长，可以指定历史数据留存的时长。\
在以下位置，你可以找到相关的输入框：

-   [监控项配置页](/zh/manual/config/items/item)-历史数据保留时长
-   在[批量更新](/zh/manual/config/items/itemupdate)监控项配置页-历史数据保留时长
-   [管家配置页](/zh/manual/web_interface/frontend_sections/administration/general#housekeeper)-历史记录-数据存储期

任何过旧的历史数据会被管家从数据库中删除。\

一般来讲，强烈建议将历史数据保留时长设置得尽可能的小。这么做可以让数据库不会因存储了大量的历史数据，导致超负荷运行。\

可以选择长时间的保留趋势数据，来替代长期需要的历史数据。例如：设置成保留14天历史数据和5年的趋势数据。\

参考[数据库空间大小](/zh/manual/installation/requirements#database_size)页，来了解历史数据和趋势数据各自需要的数据库空间。\

当设置了较短的历史数据保留时间，图形会使用趋势数据值显示旧数据，因此依旧可以通过图形查看旧数据。\

<note
important>如果历史数据保留时长被设置为“0”，那么该监控项将仅可用于更新资产记录。
:::

<note
tip>作为保存历史数据的替代方法，考虑使用可加载模块“[导出历史数据](/zh/manual/config/items/loadablemodules#providing_history_export_callbacks)”功能

:::

[comment]: # ({/new-e5d848c2})

[comment]: # ({new-3d94ff60})
#### 趋势数据的留存

趋势数据是一种内建的历史数据压缩机制，可以用来存储数字类型监控项的每小时的最小值、最大值、平均值和记录数量。\

通过设置趋势存储时间，可以指定趋势数据留存的时长。\
在以下位置，你可以找到相关的输入框：

-   [监控项配置页](/zh/manual/config/items/item)-趋势存储时间
-   在[批量更新](/zh/manual/config/items/itemupdate)监控项配置页-趋势存储时间
-   [管家配置页](/zh/manual/web_interface/frontend_sections/administration/general#housekeeper)-趋势-数据存储期

通常趋势数据设置的的留存时间应当比历史数据留存时间设置的长。任何过旧的趋势数据会被管家从数据库删除。\

::: noteimportant
如果趋势存储时间被设置为“0”，Zabbix
server将不再计算或存储该监控项的趋势数据
:::

::: noteclassic
趋势数据的计算和存储将会使用与原值相同的数据类型。\
无符号数字（unsigned
Numeric）数据类型的值，平均值计算的结果小数点后会被舍去，所以记录值之间的间隔越小，计算结果结果将会精确度越低。举个例子：如果监控项的得到了得到了两个值，分别是“0”和“1”，那么平均值的计算结果将会是“0”，而不是“0.5”。\

此外，重启服务器可能会导致当前小时无符号数字类型的数据，平均值计算的精度损失。
:::

[comment]: # ({/new-3d94ff60})
