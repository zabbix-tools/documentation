[comment]: # translation:outdated

[comment]: # ({new-06812ee1})
# 2 监控项类型

[comment]: # ({/new-06812ee1})

[comment]: # ({new-f03e2e9a})
#### 概述

监控项类型包含从系统获取数据的多种方式。每个监控项类型都有一组自己支持的监控项key和所需的参数。

以下监控项类型由Zabbix提供：

-   [Zabbix代理检查](/zh/manual/config/items/itemtypes/zabbix_agent)
-   [SNMP代理检查](/zh/manual/config/items/itemtypes/snmp)
-   [SNMP traps](/zh/manual/config/items/itemtypes/snmptrap)
-   [IPMI检查](/zh/manual/config/items/itemtypes/ipmi)
-   [简单检查](/zh/manual/config/items/itemtypes/simple_checks)
-   [VMware监控](/zh/manual/config/items/itemtypes/simple_checks/vmware_keys)
-   [日志文件监控](/zh/manual/config/items/itemtypes/log_items)
-   [计算监控项](/zh/manual/config/items/itemtypes/calculated)
-   [Zabbix内部检查](/zh/manual/config/items/itemtypes/internal)
-   [SSH检查](/zh/manual/config/items/itemtypes/ssh_checks)
-   [Telnet检查](/zh/manual/config/items/itemtypes/telnet_checks)
-   [外部检查](/zh/manual/config/items/itemtypes/external)
-   [汇总检查](/zh/manual/config/items/itemtypes/aggregate)
-   [捕捉器监控项](/zh/manual/config/items/itemtypes/trapper)
-   [JMX监控](/zh/manual/config/items/itemtypes/jmx_monitoring)
-   [ODBC监控](/zh/manual/config/items/itemtypes/odbc_checks)
-   [相关项目](/manual/config/items/itemtypes/dependent_items)
-   [HTTP 检查](/manual/config/items/itemtypes/http)

所有监控项类型的详细描述都包含在本章的各个小节中。即使监控项类型提供了大量的数据收集的方式，你还可以通过
[用户参数](/zh/manual/config/items/userparameters) 或
[可加载模块](/zh/manual/config/items/loadablemodules)进一步扩展数据收集方式。

一些监控检查由Zabbix服务器执行（称作无代理监控），而其它监控检查则需要Zabbix
agent或者Zabbix Java网关（使用JMX监视）执行。

<note
important>如果特定的项目类型需要特定的接口（如IPMI检查需要主机上的IPMI接口），该接口必须存在于主机定义中。
:::

可以在主机定义中设置多个接口：Zabbix agent，SNMP
agent，JMX和IPMI。如果一个监控项使用多个接口，它将搜索可用的主机接口（按照以下顺序：Agent→SNMP→JMX→IPMI）直到找到连接的第一个匹配的接口。

返回文本的所有监控项（字符，日志，文本信息类型）都可以返回空格（如适用）和值设置为空的字符串。（2.0版本后支持）

[comment]: # ({/new-f03e2e9a})
