[comment]: # translation:outdated

[comment]: # ({new-ffe98fbd})
# 1 扩展Zabbix Agents

本教程提供了有关如何使用用户自定义参数扩展Zabbix代理功能的分步说明。\

[comment]: # ({/new-ffe98fbd})

[comment]: # ({new-8f2323c0})
##### 第一步

写一个脚本或命令行以检测所需的参数。\

举个例子，我们编辑了下面的命令以获取 MySQL Server 执行的查询总数：

    mysqladmin -uroot status | cut -f4 -d":" | cut -f1 -d"S"

当这个命令被执行，将恢复返回 SQL 查询的总数。

[comment]: # ({/new-8f2323c0})

[comment]: # ({new-6117d271})
##### 第二步

添加命令到 zabbix\_agentd.conf:

    UserParameter=mysql.questions,mysqladmin -uroot status | cut -f4 -d":" | cut -f1 -d"S"

**mysql.questions** 作为key需要是唯一标识符。可以是任何有效的字符，比如
*queries*。

通过使用带有 ‘-t’ 标识的
zabbix\_agentd命令测试此用户自定义参数的执行。（如果是以root用户运行，请注意agent守护进程的执行者的权限）：

    zabbix_agentd -t mysql.questions

[comment]: # ({/new-6117d271})

[comment]: # ({new-67b38df5})
##### 第三步

重启Zabbix Agent。

Agent会重载配置文件。

使用 [zabbix\_get](/zh/manual/concepts/get)
实用程序测试该用户自定义参数。

[comment]: # ({/new-67b38df5})

[comment]: # ({new-4a46fd71})
##### 第四步

在被监控主机中添加使用key值为 ‘mysql.questions’
的新监控项。监控项类型必须使用 Zabbix Agent 或 Zabbix Agent（Active）。\

注意在 Zabbix Server
上。必须设置正确的返回值类型，否则Zabbix将不会接受它们。\

[comment]: # ({/new-4a46fd71})
