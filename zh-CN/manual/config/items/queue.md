[comment]: # translation:outdated

[comment]: # ({new-c981b8ac})
# 10 队列

[comment]: # ({/new-c981b8ac})

[comment]: # ({new-bf7ec8ec})
#### 概览

对列显示正在等待刷新的监控项。队列只是数据的一种逻辑上表现。ZABBIX中并没偶IPC队列或者其它任何队列的机制。\

由Proxy们监控的监控项也会被包含在列中 -
这些监控项将按Proxy历史数据更新周期被计数为队列

只有具有刷新时间计划的监控项才会记录在队列中。这表示，队列中将不包含以下的监控项类型：

-   log, logrt and eventlog 相关的ZABBIX Agent（主动）监控项
-   SNMP trap类型监控项
-   trapper类型监控项
-   web场景监控的监控项

队列显示的统计信息是ZABBIX Server是否健康的指标。

使用JSON协议直接从ZABBIX Server 检索队列。这个页面的信息只在ZABBIX
Server运行时可用。

[comment]: # ({/new-bf7ec8ec})

[comment]: # ({new-f73c67a5})
#### 阅读队列

要查看队列，请跳转*管理 → 队列* 。在右侧的下达菜单中选择*概览*。

![](../../../../assets/en/manual/config/items/queue.png){width="600"}

如图所示，大片的绿色，这样我们可以假设服务器运行时正常的。\

队列里有一个监控项等待5秒，有5个等待30秒。知道这些项目具体是什么会更好。\

马上帮你实现，在右上角的下拉菜单中选择*细节*。现在既可以看到这些延迟监控项的列表。\

![](../../../../assets/en/manual/config/items/queue_details.png){width="600"}

通过这些细节信息，可以找出这些监控项发生延迟的原因。\

有一两个延迟监控项，不要慌张。它们有可能在一秒内被更新。但是如果你看到了一大堆延迟很久的监控项，这可能导致严重的问题。

![](../../../../assets/en/manual/config/items/queue2.png){width="600"}

是不是Agent进程下线了？

[comment]: # ({/new-f73c67a5})

[comment]: # ({new-1e93992f})
#### 队列项目

有一个特别的内部监控项**zabbix\[queue,<from>,<to>\]**
可以用于监控ZABBIX中队列的健康状态。他会返回指定时间区间的监控项数目。有关更多信息请参阅[内部监控项](/zh/manual/config/items/itemtypes/internal)。\

[comment]: # ({/new-1e93992f})
