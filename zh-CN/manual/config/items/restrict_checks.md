[comment]: # translation:outdated

[comment]: # ({new-780b1aa2})
# 13 Restricting agent checks

[comment]: # ({/new-780b1aa2})

[comment]: # ({new-9de656b2})
#### Overview

It is possible to restrict checks on the agent side by creating an item
blacklist, a whitelist, or a combination of whitelist/blacklist.

To do that use a combination of two agent
[configuration](/manual/appendix/config/zabbix_agentd) parameters:

-   `AllowKey=<pattern>` - which checks are allowed; <pattern> is
    specified using a wildcard (\*) expression
-   `DenyKey=<pattern>` - which checks are denied; <pattern> is
    specified using a wildcard (\*) expression

Note that:

-   All `system.run[*]` items (remote commands, scripts) are disabled by
    default, even when no deny keys are specified;
-   Since Zabbix 5.0.2 the EnableRemoteCommands agent parameter is:

```{=html}
<!-- -->
```
       * deprecated by Zabbix agent
       * unsupported by Zabbix agent2

Therefore, to allow all remote commands, specify an
AllowKey=system.run\[\*\] parameter. To allow only some remote commands,
create a whitelist of specific system.run\[\] commands. To disallow
specific remote commands, add DenyKey parameters with system.run\[\]
commands before the AllowKey=system.run\[\*\] parameter.

[comment]: # ({/new-9de656b2})

[comment]: # ({new-3145c750})
#### Important rules

-   A whitelist without a deny rule is only allowed for system.run\[\*\]
    items. For all other items, AllowKey parameters are not allowed
    without a DenyKey parameter; in this case Zabbix agent **will not
    start** with only AllowKey parameters.
-   The order matters. The specified parameters are checked one by one
    according to their appearance order in the configuration file:
    -   As soon as an item key matches an allow/deny rule, the item is
        either allowed or denied; and rule checking stops. So if an item
        matches both an allow rule and a deny rule, the result will
        depend on which rule comes first.
    -   The order affects also EnableRemoteCommands parameter (if used).
-   Unlimited numbers of AllowKey/DenyKey parameters is supported.
-   AllowKey, DenyKey rules do not affect HostnameItem,
    HostMetadataItem, HostInterfaceItem configuration parameters.
-   Key pattern is a wildcard expression where the wildcard (\*)
    character matches any number of any characters in certain position.
    It might be used in both the key name and parameters.
-   If a specific item key is disallowed in the agent configuration, the
    item will be reported as unsupported (no hint is given as to the
    reason);
-   Zabbix agent with --print (-p) command line option will not show
    keys that are not allowed by configuration;
-   Zabbix agent with --test (-t) command line option will return
    "Unsupported item key." status for keys that are not allowed by
    configuration;
-   Denied remote commands will not be logged in the agent log (if
    LogRemoteCommands=1).

[comment]: # ({/new-3145c750})

[comment]: # ({new-253ad144})
#### Use cases

[comment]: # ({/new-253ad144})

[comment]: # ({new-866af1fd})
##### Deny specific check

-   Blacklist a specific check with DenyKey parameter. Matching keys
    will be disallowed. All non-matching keys will be allowed, except
    system.run\[\] items.

For example:

    # Deny secure data access
    DenyKey=vfs.file.contents[/etc/passwd,*]

::: noteimportant
A blacklist may not be a good choice, because a
new Zabbix version may have new keys that are not explicitly restricted
by the existing configuration. This could cause a security
flaw.
:::

[comment]: # ({/new-866af1fd})

[comment]: # ({new-10d0a182})
##### Deny specific command, allow others

-   Blacklist a specific command with DenyKey parameter. Whitelist all
    other commands, with the AllowKey parameter.

```{=html}
<!-- -->
```
    # Disallow specific command
    DenyKey=system.run[ls -l /]
     
    # Allow other scripts
    AllowKey=system.run[*]

[comment]: # ({/new-10d0a182})

[comment]: # ({new-83edb1ef})
##### Allow specific check, deny others

-   Whitelist specific checks with AllowKey parameters, deny others with
    `DenyKey=*`

For example:

    # Allow reading logs:
    AllowKey=vfs.file.*[/var/log/*]

    # Allow localtime checks
    AllowKey=system.localtime[*]

    # Deny all other keys
    DenyKey=*

[comment]: # ({/new-83edb1ef})

[comment]: # ({new-16bccaff})
#### Pattern examples

|Pattern|Description|Matches|No match|
|-------|-----------|-------|--------|
|*\**|Matches all possible keys with or without parameters.|Any|None|
|*vfs.file.contents*|Matches `vfs.file.contents` without parameters.|vfs.file.contents|vfs.file.contents\[/etc/passwd\]|
|*vfs.file.contents\[\]*|Matches `vfs.file.contents` with empty parameters.|vfs.file.contents\[\]|vfs.file.contents|
|*vfs.file.contents\[\*\]*|Matches `vfs.file.contents` with any parameters; will not match `vfs.file.contents` without square brackets.|vfs.file.contents\[\]<br>vfs.file.contents\[/path/to/file\]|vfs.file.contents|
|*vfs.file.contents\[/etc/passwd,\*\]*|Matches `vfs.file.contents` with first parameters matching /etc/passwd and all other parameters having any value (also empty).|vfs.file.contents\[/etc/passwd,\]<br>vfs.file.contents\[/etc/passwd,utf8\]|vfs.file.contents\[/etc/passwd\]<br>vfs.file.contents\[/var/log/zabbix\_server.log\]<br>vfs.file.contents\[\]|
|*vfs.file.contents\[\*passwd\*\]*|Matches `vfs.file.contents` with first parameter matching \*passwd\* and no other parameters.|vfs.file.contents\[/etc/passwd\]|vfs.file.contents\[/etc/passwd,\]<br>vfs.file.contents\[/etc/passwd, utf8\]|
|*vfs.file.contents\[\*passwd\*,\*\]*|Matches `vfs.file.contents` with only first parameter matching \*passwd\* and all following parameters having any value (also empty).|vfs.file.contents\[/etc/passwd,\]<br>vfs.file.contents\[/etc/passwd, utf8\]|vfs.file.contents\[/etc/passwd\]<br>vfs.file.contents\[/tmp/test\]|
|*vfs.file.contents\[/var/log/zabbix\_server.log,\*,abc\]*|Matches `vfs.file.contents` with first parameter matching /var/log/zabbix\_server.log, third parameter matching 'abc' and any (also empty) second parameter.|vfs.file.contents\[/var/log/zabbix\_server.log,,abc\]<br>vfs.file.contents\[/var/log/zabbix\_server.log,utf8,abc\]|vfs.file.contents\[/var/log/zabbix\_server.log,,abc,def\]|
|*vfs.file.contents\[/etc/passwd,utf8\]*|Matches `vfs.file.contents` with first parameter matching /etc/passwd, second parameter matching 'utf8' and no other arguments.|vfs.file.contents\[/etc/passwd,utf8\]|vfs.file.contents\[/etc/passwd,\]<br>vfs.file.contents\[/etc/passwd,utf16\]|
|*vfs.file.\**|Matches any keys starting with `vfs.file.` without any parameters.|vfs.file.contents<br>vfs.file.size|vfs.file.contents\[\]<br>vfs.file.size\[/var/log/zabbix\_server.log\]|
|*vfs.file.\*\[\*\]*|Matches any keys starting with `vfs.file.` with any parameters.|vfs.file.size.bytes\[\]<br>vfs.file.size\[/var/log/zabbix\_server.log, utf8\]|vfs.file.size.bytes|
|*vfs.\*.contents*|Matches any key starting with `vfs.` and ending with `.contents` without any parameters.|vfs.mount.point.file.contents<br>vfs..contents|vfs.contents|

[comment]: # ({/new-16bccaff})

[comment]: # ({new-9df9c213})
#### system.run and AllowKey

A hypothetical script like 'myscript.sh' may be executed on a host via
Zabbix agent in several ways:

1\. As an item key in a passive or active check, for example:

-   system.run\[myscript.sh\]
-   system.run\[myscript.sh,wait\]
-   system.run\[myscript.sh.nowait\]

Here the user may add "wait", "nowait" or omit the 2nd argument to use
its default value in system.run\[\].

2\. As a global script (initiated by user in frontend or API).

A user configures this script in *Administration* → *Scripts*, sets
"Execute on: Zabbix agent" and puts "myscript.sh" into the script's
"Commands" input field. When invoked from frontend or API the Zabbix
server sends to agent:

-   system.run\[myscript.sh,wait\] - up to Zabbix 5.0.4
-   system.run\[myscript.sh\] - since 5.0.5

Here the user does not control the "wait"/"nowait" parameters.

3\. As a remote command from an action. The Zabbix server sends to
agent:

-   system.run\[myscript.sh,nowait\]

Here again the user does not control the "wait"/"nowait" parameters.

What that means is if we set AllowKey like:

    AllowKey=system.run[myscript.sh]

then

-   system.run\[myscript.sh\] - will be allowed
-   system.run\[myscript.sh,wait\], system.run\[myscript.sh,nowait\]
    will not be allowed - the script will not be run if invoked as a
    step of action

To allow all described variants you may add:

    AllowKey=system.run[myscript.sh,*] 
    DenyKey=system.run[*]

to the agent/agent2 parameters.

[comment]: # ({/new-9df9c213})
