[comment]: # translation:outdated

[comment]: # ({new-9685d873})
# 11 用户和用户组

[comment]: # ({/new-9685d873})

[comment]: # ({new-07f1c89b})
## 11 Users and user groups

#### 概述

#### Overview

Zabbix 中的所有用户都通过 Web 前端去访问 Zabbix
应用程序。并为每个用户分配唯一的登陆名和密码。

All users in Zabbix access the Zabbix application through the web-based
frontend. Each user is assigned a unique login name and a password.

所有用户的密码都被加密并储存于 Zabbix
数据库中。用户不能使用其用户名和密码直接登陆到 UNIX
服务器中，除非他们也被因此建立在 UNIX 中。可以使用 SSL 来保护 Web
服务器和用户浏览器之间的通讯。

All user passwords are encrypted and stored in the Zabbix database.
Users cannot use their user id and password to log directly into the
UNIX server unless they have also been set up accordingly to UNIX.
Communication between the web server and the user browser can be
protected using SSL.

使用一个灵活的
[用户权限架构](/manual/config/users_and_usergroups/permissions)
可以限制和区分对以下内容的访问权限：

With a flexible [user permission
schema](/manual/config/users_and_usergroups/permissions) you can
restrict and differentiate access to:

-   管理 Zabbix 前端的功能；
-   主机组中监视的主机。

```{=html}
<!-- -->
```
-   administrative Zabbix frontend functions
-   monitored hosts in hostgroups

最初 Zabbix
安装后有两个预先定义好的用户“Admin”和“guest”。其中，“guest”用户用户未经验证身份的用户。在你使用“Admin”登陆前，你是“guest”用户。继续在
Zabbix 中[配置用户](/manual/config/users_and_usergroups/user)。

The initial Zabbix installation has two predefined users - 'Admin' and
'guest'. The 'guest' user is used for unauthenticated users. Before you
log in as 'Admin', you are 'guest'. Proceed to [configuring a
user](/manual/config/users_and_usergroups/user) in Zabbix.

[comment]: # ({/new-07f1c89b})
