[comment]: # translation:outdated

[comment]: # ({new-061a30b2})
# 2 资产管理

[comment]: # ({/new-061a30b2})

[comment]: # ({67b92e9a-4b8ec855})
#### 概述

你可以将联网设备的资产信息保存在Zabbix里。

Zabbix管理页面有一个特殊的 *Inventory（资产记录）*菜单。 但你一开始不会看到任何数据，这里你也不能输入任何资产相关的信息。资产信息是在配置主机时人工录入建立的资产数据，或者通过使用某些自动填充选项完成的录入。

[comment]: # ({/67b92e9a-4b8ec855})

[comment]: # ({afb013b7-69c69326})
#### 构建资产库



[comment]: # ({/afb013b7-69c69326})

[comment]: # ({bddd6af5-9a68f9a7})
##### 手动模式

当[配置主机](host)时，在 *Host inventory（资产记录）*选项卡中，你可以输入设备类型、序列号、位置、负责人等详细信息 - 这些数据将填充资产信息。

如果主机资产信息中包含URL，并以'http'或'https'开头，则会在 *Inventory* 中呈现为可点击的链接。

[comment]: # ({/bddd6af5-9a68f9a7})

[comment]: # ({13ecd631-a9359c4e})
##### 自动模式

主机资产也可以自动填充。为了使自动填充功能生效，配置主机时 *Host inventory（资产记录）*选项卡中的清单模式必须设置为 *Automatic（自动）*。

然后，在监控项配置中你可以通过[配置主机监控项](/zh/manual/config/items/item)用任意指示目的字段相应属性的值（即 *填充主机资产字段的监控项*）来配置对应的主机资产字段。

以下是对资产信息自动采集非常有用的监控项:

-   system.hw.chassis\[full|type|vendor|model|serial\] - 默认是 \[full\]，需要root权限
-   system.hw.cpu\[all|cpunum,full|maxfreq|vendor|model|curfreq\] - 默认是 \[all,full\]
-   system.hw.devices\[pci|usb\] - 默认是 \[pci\]
-   system.hw.macaddr\[interface,short|full\] - 默认是 \[all,full\]，interface 支持正则表达式
-   system.sw.arch
-   system.sw.os\[name|short|full\] - 默认是 \[name\]
-   system.sw.packages\[package,manager,short|full\] - 默认是 \[all,all,full\]，package 支持正则表达式

[comment]: # ({/13ecd631-a9359c4e})

[comment]: # ({c772654c-84351dfb})
##### 资产模式选择

可以在主机配置表单中选择资产模式。

默认情况下，新主机的资产模式是根据 *Administration（管理）* → *General（一般）* →
*[Other（其他）](/zh/manual/web_interface/frontend_sections/administration/general#other_parameters)* 中的*Default host inventory mode（默认主机资产记录模式）*设置选择的。

对于通过网络发现或自动注册操作添加的主机，可以定义 *Set host inventory mode（设置主机资产记录模式）* 操作，选择手动或自动模式。 此操作将覆盖 *Default host inventory mode（默认主机资产记录模式）* 设置。

[comment]: # ({/c772654c-84351dfb})

[comment]: # ({cb4dc36f-8291099a})
#### 资产清单概述

*Inventory（资产记录）*菜单中提供了所有现有资产数据的详细信息。

在 *Inventory（资产记录） → Overview（概览）*你可以通过资产的各个字段获取主机数。

在 *Inventory（资产记录） → Hosts（主机）*你可以看到所有具有资产信息的主机。单击主机名将以表单显示资产明细。

![](../../../../assets/en/manual/web_interface/inventory_host.png){width="600"}

 **Overview（概览）**标签内容如下:

|参数|<|<|<|<|描述|
|---------|-|-|-|-|-----------|
|*Host name*|<|<|<|<|主机名称。<br>单击名称将打开一个菜单，其中包含了为主机定义的脚本。<br>主机名显示为橙色图标，表示主机正在维护中。|
|*Visible name*|<|<|<|<|主机的显示名称（如已定义）。|
|*Host (Agent, SNMP, JMX, IPMI)<br>interfaces*|<|<|<|<|此区域提供了为主机配置的接口的详细信息。|
|*OS*|<|<|<|<|主机的操作系统资产清单字段（如已定义）。|
|*Hardware*|<|<|<|<|主机硬件清单字段（如已定义）。|
|*Software*|<|<|<|<|主机软件清单字段（如已定义）。|
|*Description*|<|<|<|<|主机描述。|
|*Monitoring*|<|<|<|<|与监控部分的链接，其中包含该主机的这些数据：*Web*、*Latest data*、*Problems*、*Graphs*、*Dashboards*。|
|*Configuration*|<|<|<|<|链接到此主机的这些配置部分：*Host*、*Applications*、*Items*、*Triggers*、*Graphs*、*Discovery*、*Web*。<br>配置的实体的数量在每个链接之后的括号中列出。|

 **Details** 选项卡显示填充的所有资产清单字段（不为空的）。

[comment]: # ({/cb4dc36f-8291099a})

[comment]: # ({a92a017a-6a9ca6a1})
#### 资产清单宏

有可用于通知的主机资产清单宏 {INVENTORY.\*} ，例如：

”{INVENTORY.LOCATION1}的服务器出现问题，负责人是{INVENTORY.CONTACT1}，联系电话{INVENTORY.POC.PRIMARY.PHONE.A1}。“

更多详细信息，请参阅 [supported macro（支持的宏）](/zh/manual/appendix/macros/supported_by_location)页面。

[comment]: # ({/a92a017a-6a9ca6a1})
