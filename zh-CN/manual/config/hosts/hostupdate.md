[comment]: # translation:outdated

[comment]: # ({new-1c6301ba})
# 3 批量更新

[comment]: # ({/new-1c6301ba})

[comment]: # ({cba4e174-7205743f})
#### 概述

有时你可能想要一次更改多个主机的某些属性。那么你可以使用批量更新功能来实现，而不是打开每个主机页面进行编辑。

[comment]: # ({/cba4e174-7205743f})

[comment]: # ({27693f6a-929367ac})
#### 使用批量更新

要批量更新某些主机，请按以下操作执行:

-   [主机列表](/zh/manual/web_interface/frontend_sections/configuration/hosts)中，在要更新的主机之前选中复选框
-   点击下方的 *Mass update（批量更新）* 按钮
-   跳转到属性所属的选项卡（*Host（主机）*、*IPMI*、*Tags（标签）*、*Macros（宏）*、*Inventory（资产记录）*、*Encryption（加密）* 或 *Value mapping（值映射）*）
-   选中要更新的任何属性的复选框，并输入新值

![](../../../../assets/en/manual/config/hosts/host_mass.png)

在选择 **template（模板）** 链接更新按钮时，如下选项可供选择：

-   *Link（链接）* - 指定需要链接的模板
-   *Replace（替换）* - 指定需要链接的模板，并取消任意之前已经链接到该主机且不需要的模板链接
-   *Unlink（取消链接）* - 指定需要取消链接的模板

要指定链接/取消链接的模板，请在搜索框（在此输入搜索）字段中输入模板名，直到出现一个提供匹配模板的下拉菜单。只需向下滚动即可选择所需的模板。

在选中 *Clear when unlinking（取消链接时清除）* 选项时，在取消与任何以前链接的模板的关联的同时还会删除所有继承自该模板的元素（监控项、触发器等）。

在选择**主机组**更新对应的按钮时，如下选项可供选择：
-   *Add（添加）*- 从现有主机组指定要添加的主机组，或为所选主机输入全新的主机组
-   *Replace（替换）*- 从任何现有主机组中删除所选主机，并替换为此字段中指定的主机组（现有的或新的主机组）
-   *Remove（移除）*- 从所选主机中移除特定主机组

这些字段都是可以自动填充的 - 开始输入时会提供匹配的主机组的下拉列表。新的主机组也会出现在下拉列表中，并在字符串后用*(new)*表示。 只需向下滚动即可选择。

![](../../../../assets/en/manual/config/hosts/host_mass_c.png)

![](../../../../assets/en/manual/config/hosts/host_mass_d.png)

标签支持以下宏：用户宏、{INVENTORY.\*}宏、{HOST.HOST}、{HOST.NAME}、{HOST.CONN}、{HOST.DNS}、{HOST.IP}、{HOST.PORT}和{HOST.ID}宏。
注意：名称相同但值不同的标签是不被当成重复标签的，可以添加到同一个主机。

![](../../../../assets/en/manual/config/hosts/host_mass_e.png)

在选择宏更新按钮时，如下选项可供选择：

-   *Add（添加）*- 允许为主机指定额外的用户宏。如果选中了 *Update existing（更新现有的）* 复选框，则将更新指定宏名称的值、类型和描述。不选的话，如果主机上已存在具有该名称的宏，则不会对其进行更新。
-   *Update（更新）*- 将替换此列表中指定的宏的值、类型和描述。如果选中 *Add missing（添加所缺的）* 复选框，则主机上以前不存在的宏将被添加为新宏。如果不选，则仅更新主机上已存在的宏。
-   *Remove（移除）*- 从主机中删除指定的宏。如果选中了 *Except selected（除选定对象之外）* 复选框，则除列表中指定的宏之外的所有宏将被删除。如果不选，则仅删除列表中指定的宏。
-   *Remove all（全部删除）*- 从主机上删除所有用户宏。如果没选中 *I confirm to remove all macros（我确认删除所有的宏）* 复选框，将弹出一个要求确认删除所有宏的窗口。

![](../../../../assets/en/manual/config/hosts/host_mass_f.png)

为了能够批量更新资产记录，*Inventory mode（资产记录模式）* 应该设置为'手动'或'自动'。

![](../../../../assets/en/manual/config/hosts/host_mass_g.png)

![](../../../../assets/en/manual/config/hosts/host_mass_h.png)

在选择值映射按钮时，如下选项可供选择：

-   *Add（增加）*- 增加主机的值映射。如果选中了 *Update existing（更新已有的）*，该名称的值映射下的所有属性都会被更新。否则，若该名称的值映射已存在，则不会被更新。
-   *Update（更新）*- 更新已有的值映射。如果选中了 *Add missing（添加所缺的）*，则原来主机中不存在的值映射会被加入到主机中。否则仅已经存在的值映射会被更新。
-   *Rename（重命名）*- 重命名已存在的值映射。
-   *Remove（移除）*- 从主机中删除指定的值映射。如果选中了 *Except selected（除选定对象之外）*，则**除了**选定的值映射外都会被删除。
-   *Remove all（全部删除）*- 删除主机的全部值映射配置。如果没选中 *I confirm to remove all value maps（我确认删除所有的值映射）* 复选框，将弹出一个要求确认删除所有值映射的窗口。

完成所有必要的更改后，单击 *Update（更新）*。所有选定主机的属性将相应更新。

[comment]: # ({/27693f6a-929367ac})
