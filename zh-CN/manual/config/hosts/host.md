[comment]: # translation:outdated

[comment]: # ({55d4f327-1da35dcd})
# 1 配置主机

[comment]: # ({/55d4f327-1da35dcd})

[comment]: # ({87c1702b-0ee38f7e})
#### 概述

按照以下步骤可以在 Zabbix 前端页面创建一台主机：

- · 点击: *Configuration（配置） → Hosts（主机）* 或 *Monitoring（监测中） → Hosts（主机）*
- · 在页面右侧点击 *Create host（创建主机）*  (点击主机名编辑一台已有的主机)
- · 在表单中输入主机的相关参数

你也可以在已经存在的主机上使用 *Clone（克隆）* 和 *Full clone（全克隆）* 按钮创建一台新的主机。点击 *Clone（克隆）* 将保留所有的主机参数和模板链接（保留这些模版中的所有实体），*Full clone（全克隆）* 将额外保留直接附加的实体（应用集、监控项、触发器、视图、低级自动发现规则和 Web 定制场景）。

*注意*: 当主机被克隆时，它将保留最初在模板上的所有模板实体。在现有主机级别上对这些实体所做的任何更改（例如更改的监控间隔、修改正则表达式或添加原型到底层发现规则）都不会克隆到新主机；相反，它们将与最初模板上的保持一致。

[comment]: # ({/87c1702b-0ee38f7e})

[comment]: # ({367f4ae4-e2f56cfc})
#### 配置

 **Host（主机）** 选项卡包含常规主机属性：

![](../../../../assets/en/manual/config/host_a.png){width="600"}

所有标有红色星号的为必填字段。

|参数|<|描述|
|---------|-|-----------|
|*Host name（主机名）*|<|输入唯一的主机名。允许使用字母、数字、空格、点、破折号和下划线。开头和结尾不允许空格。<br>*注意：* 配置正在运行的 Zabbix agent 主机时，agent [配置文件](/manual/appendix/config/zabbix_agentd) 的 *Hostname* 参数必须与此处输入的主机名相同。 [主动检查](/manual/appendix/items/activepassive) 需要依据该参数的值。|
|*Visible name（主机可见名）*|<|输入主机的唯一可见名称。如果你设置了这个名称，主机在列表、拓扑图等场景中将显示为可见名称而不是主机名。此属性支持 UTF-8 。|
|*Templates（模板）*|<|将 [模板](/manual/config/templates) 链接到主机。所有实体（监控项、触发器、图表等）都将从模板继承。<br>要关联新模板，请开始在 *Link new templates（链接新模板）* 字段中输入模板名称进行模糊匹配。此时系统将会跳出匹配的模板列表；向下滚动以选择。或者，您可以单击字段旁边的 *Select（选择）* ，然后从弹出窗口的列表中选择模板。*Link new templates（链接新模板）* 字段中选择的模板将在保存或更新主机配置表单时链接到主机。<br>取消关联模板，请使用 *Linked templates（链接的模板）* 框中的两个选项之一：<br>*Unlink（取消链接）* - 取消链接模板，但保留其监控项、触发器和图表<br>*Unlink and clear（取消链接并清理）* - 取消链接模板并删除其所有监控项、触发器和图表<br>列出的模板名可以点击跳转到模板配置表单。|
|*Groups（主机群组）*|<|选择主机所属的主机组。一台主机必须至少属于一个主机组。通过添加一个不存在的组名，可以创建一个新组并将主机关联到主机组。|
|*Interfaces（主机接口）*|<|主机支持多种主机接口类型： *Agent*, *SNMP*, *JMX* 和 *IPMI*。<br>默认没有定义主机接口。要增加一个新接口，在 *Interfaces* 区域点击 *Add* 并输入 *IP/DNS*, *Connect to* 和 *Port* 信息。<br>*注意：* 用在任何监控项的接口都不能被移除，并且 *Remove* 链接是灰色的。<br>有关配置 SNMP 接口（v1、v2 和 v3）的更多详细信息，请参阅 [配置 SNMP 监控](/manual/config/items/itemtypes/snmp#configuring_snmp_monitoring) 。|
|<|*IP address*|主机的IP地址（可选）。|
|^|*DNS name*|主机的DNS名称（可选）。|
|^|*Connect to*|点击对应的按钮告诉 Zabbix server 采用哪种模式从 agent 端获取数据：<br>**IP** - 连接到主机的IP地址（推荐）<br>**DNS** - 连接到主机的DNS名称|
|^|*Port*|TCP/UDP 端口。默认端口： Zabbix agent 是 10050， SNMP agent 是 161,  JMX 是 12345，IPMI 是 623。|
|^|*Default*|选择单选按钮设置默认接口。|
|*Description（描述）*|<|填写主机描述信息。|
|*Monitored by proxy（由 agent 代理程序监测*|<|主机可以被 Zabbix server 或者Zabbix proxy 监控：<br>**(no proxy)** - 主机被 Zabbix  server 监控<br>**Proxy name** - 主机被 Zabbix proxy “Proxy 名称” 监控|
|*Enabled（已启用）*|<|选中此项激活主机，准备接受监控。如果没选中，表示主机未激活，不能被监控。|

**IPMI** 选项卡包含 IPMI 管理属性。

|参数|描述|
|---------|-----------|
|*Authentication algorithm（认证算法）*|选择认证算法。|
|*Privilege level（特权级别）*|选择权限级别。|
|*Username（用户名）*|认证用户名。支持使用宏|
|*Password（密码）*|认证用户密码。支持使用宏|

**Tags（标签）** 选项卡允许定义主机级别的 [标签](/manual/config/tagging)。该主机的所有问题都将使用此处输入的值进行标记。

![](../../../../assets/en/manual/config/host_d.png){width="600"}

标签中支持以下宏：用户宏、{INVENTORY.\*} 宏、{HOST.HOST}，{HOST.NAME}，{HOST.CONN}，{HOST.DNS}，{HOST.IP}，{HOST.PORT} 和 {HOST.ID} 。

**Macros** 选项卡允许您将主机级别的 [用户宏](/manual/config/macros/user_macros) 定义为键值对。请注意，宏值可以保存为纯文本、加密文本或 Vault 机密。也支持添加描述。

![](../../../../assets/en/manual/config/host_e.png){width="600"}

如果选择 *Inherited and host macros（继承以及主机宏）* 选项，还可以在此处查看模板和全局级别的用户宏。所有为该主机定义的用户宏都显示为他们的来源以及对应的值。

![](../../../../assets/en/manual/config/host_e2.png){width="600"}

为方便起见，页面提供了指向各个模板和全局宏配置的链接。还可以在主机级别编辑模板/全局宏，从而有效地在主机上创建宏的副本。

**Host inventory（主机资产记录）** 选项卡允许你手动为主机输入 [资产](inventory) 信息。你还可以选择启用 *Automatic（自动）* 资产信息填充，或者禁用此主机的资产信息填充。

![](../../../../assets/en/manual/config/host_f.png){width="600"}

如果启用了资产记录（手动或自动），则会显示一个带有选项卡名称的绿点。


[comment]: # ({/367f4ae4-e2f56cfc})

[comment]: # ({5a48ac6f-6875b717})
##### 加密

 **Encryption（加密）** 选项卡允许建立与主机的 [加密](/manual/encryption) 连接。

|选项|描述|
|---------|-----------|
|*Connections to host（连接到主机）*|Zabbix server 或 proxy 如何与 Zabbix agent 连接：不加密（默认），使用 PSK（pre-shared 共享密钥）或证书。|
|*Connections from host（从主机连接）*|选择允许来自主机的连接类型（即来自 Zabbix agent 和 Zabbix sender）。可以同时选择多种连接类型（用于测试和切换到其他连接类型）。默认为“不加密”。|
|*Issuer（发行者）*|许可的证书颁发者。证书首先通过 CA （证书颁发机构）进行验证。如果它是由 CA 签名的有效证书，则 *Issuer* 字段可用于进一步限制允许的 CA。如果 Zabbix 安装使用来自多个 CA 的证书，则使用此字段。如果此字段为空，则接受任何 CA。|
|*Subject（主体）*|允许的证书主体。证书首先通过 CA （证书颁发机构）进行验证。如果它是由 CA 签名的有效证书，则 *Subject（主体）* 字段只能用于允许 *Subject* 字符串的一个值。如果此字段为空，则接受配置的 CA 签名的任何有效证书。|
|*PSK identity（共享密钥一致性）*|预共享密钥标识字符串。<br>不要将敏感信息放在此处，它会在网络上以未加密方式传输，以通知接收者使用哪个 PSK。|
|*PSK（共享密钥）*|共享密钥（十六进制字符串）。最大长度：如果 Zabbix 使用 GnuTLS 或 OpenSSL 库，则为 512 个十六进制数字（256 字节 PSK），如果 Zabbix 使用 mbed TLS（PolarSSL）库，则为 64 个十六进制数字（32 字节 PSK）。示例：1f87b595725ac58dd977beef14b97461a7c1045b9a1c963065002c5473194952。|

[comment]: # ({/5a48ac6f-6875b717})

[comment]: # ({c9679c3b-513311ca})
##### 值映射

 **Value mapping（值映射）** 选项卡允许在 [value mappings（值映射）](/manual/config/items/mapping) 中配置监控项数据的人性化展示。

[comment]: # ({/c9679c3b-513311ca})

[comment]: # ({new-7cc0883e})
#### Creating a host group

::: noteimportant
Only Super Admin users can create host groups.
:::

To create a host group in Zabbix frontend, do the following:

-   Go to: *Configuration → Host groups*
-   Click on *Create host group* in the upper right corner of the screen
-   Enter the group name in the form

![](../../../../assets/en/manual/config/host_group.png)

To create a nested host group, use the '/' forward slash separator, for example `Europe/Latvia/Riga/Zabbix servers`. You can create this group even if none of the three parent host groups (`Europe/Latvia/Riga/`) exist. In this case creating these parent host groups is up to the user; they will not be created automatically.<br>Leading and trailing slashes, several slashes in a row are not allowed. Escaping of '/' is not supported.

[comment]: # ({/new-7cc0883e})

[comment]: # ({new-22cf222a})

Once the group is created, you can click on the group name in the list to edit group name, clone the group or set additional option:

![](../../../../assets/en/manual/config/host_group2.png)

*Apply permissions and tag filters to all subgroups* - mark this checkbox and click on *Update* to apply the same level of permissions/tag filters to all nested host groups. For user groups that may have had differing [permissions](/manual/config/users_and_usergroups/usergroup#configuration) assigned to nested host groups, the permission level of the parent host group will be enforced on the nested groups. This is a one-time option that is not saved in the database.

**Permissions to nested host groups**

-   When creating a child host group to an existing parent host group,
    [user group](/manual/config/users_and_usergroups/usergroup)
    permissions to the child are inherited from the parent (for example,
    when creating `Riga/Zabbix servers` if `Riga` already exists)
-   When creating a parent host group to an existing child host group,
    no permissions to the parent are set (for example, when creating
    `Riga` if `Riga/Zabbix servers` already exists)

[comment]: # ({/new-22cf222a})

