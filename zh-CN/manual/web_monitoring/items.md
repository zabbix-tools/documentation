[comment]: # translation:outdated

[comment]: # ({new-a3537eda})
# 1 Web 监控项（items）

[comment]: # ({/new-a3537eda})

[comment]: # ({new-7f39438f})
### 1 Web monitoring items

[comment]: # ({/new-7f39438f})

[comment]: # ({new-6bf9e7b2})
#### 概述

[comment]: # ({/new-6bf9e7b2})

[comment]: # ({new-38a780ad})
#### Overview

在创建 Web 场景时，会自动添加一些新监控项以进行监控。

Some new items are automatically added for monitoring when web scenarios
are created.

[comment]: # ({/new-38a780ad})

[comment]: # ({new-1f198e97})
#### 场景监控项

[comment]: # ({/new-1f198e97})

[comment]: # ({new-abd173df})
#### Scenario items

创建场景后，Zabbix
会自动添加以下监控项进行监控，将它们链接到所选应用程序。

As soon as a scenario is created, Zabbix automatically adds the
following items for monitoring, linking them to the selected
application.

|监控项                                           说明|<|
|----------------------------------------------------------|-|
|// 场景 <Scenario> 的下载速度 //           此监控项将收|有关整个场景的下载速度（每秒字节数）的信息，即所有步骤的平均值。<br>监控项 key: web.test.in\[Scenario,,bps\]<br>类型：*Numeric(float)*|
|// 场景 <Scenario> 的失败步骤 //           此监控项将显|场景上失败的步骤的编号。如果所有步骤成功执行，则返回 0。<br>监控项 key: web.test.fail\[Scenario\]<br>类型：*Numeric(unsigned)*|
|// 场景 <Scenario> 的最后一个错误消息 //   此监控项返回场景的最|一个错误消息文本。仅当场景具有失败步骤时，才会存储新值。如果所有步骤都正常，则不会收集新值。<br>监控项 key: web.test.error\[Scenario\]<br>类型：*Character*|

|Item|Description|
|----|-----------|
|*Download speed for scenario <Scenario>*|This item will collect information about the download speed (bytes per second) of the whole scenario, i.e. average for all steps.<br>Item key: web.test.in\[Scenario,,bps\]<br>Type: *Numeric(float)*|
|*Failed step of scenario <Scenario>*|This item will display the number of the step that failed on the scenario. If all steps are executed successfully, 0 is returned.<br>Item key: web.test.fail\[Scenario\]<br>Type: *Numeric(unsigned)*|
|*Last error message of scenario <Scenario>*|This item returns the last error message text of the scenario. A new value is stored only if the scenario has a failed step. If all steps are ok, no new value is collected.<br>Item key: web.test.error\[Scenario\]<br>Type: *Character*|

使用实际场景名称而不是“Scenario”。

The actual scenario name will be used instead of "Scenario".

::: noteclassic
添加的 Web 监控项将保留 30 天历史记录和 90
天趋势记录。
:::

::: noteclassic
Web monitoring items are added with a 30 day history and a
90 day trend retention period.
:::

::: noteclassic
如果场景名称以双引号开头或包含逗号或方括号，则它将在监控项键中正确引用。在其他情况下，不会执行额外的引用。
:::

::: noteclassic
If scenario name starts with a doublequote or contains comma
or square bracket, it will be properly quoted in item keys. In other
cases no additional quoting will be performed.
:::

这些监控项可用于创建触发器和定义通知条件。 These items can be used to
create triggers and define notification conditions.

[comment]: # ({/new-abd173df})

[comment]: # ({new-fc2e76a2})
##### 例子 1

要创建“Web 场景失败”触发器，可以定义触发器表达式：

    {host:web.test.fail[Scenario].last()}<>0

确保将“Scenario”替换为场景的真实名称。

##### 例子 2

要在触发器名称中创建具有有用问题描述的“Web
场景失败”触发器，可以使用名称定义触发器：

    Web scenario "Scenario" failed: {ITEM.VALUE}

和触发器表达式：

    {host:web.test.error[Scenario].strlen()}>0 and {host:web.test.fail[Scenario].min()}>0

确保将“Scenario”替换为场景的真实名称。

##### 例子 3

要创建“Web application is slow”触发器，可以定义一个触发器表达式：

    {host:web.test.in[Scenario,,bps].last()}<10000

确保将“Scenario”替换为场景的真实名称。

##### Example 1

To create a "Web scenario failed" trigger, you can define a trigger
expression:

    {host:web.test.fail[Scenario].last()}<>0

Make sure to replace 'Scenario' with the real name of your scenario.

##### Example 2

To create a "Web scenario failed" trigger with a useful problem
description in the trigger name, you can define a trigger with name:

    Web scenario "Scenario" failed: {ITEM.VALUE}

and trigger expression:

    {host:web.test.error[Scenario].strlen()}>0 and {host:web.test.fail[Scenario].min()}>0

Make sure to replace 'Scenario' with the real name of your scenario.

##### Example 3

To create a "Web application is slow" trigger, you can define a trigger
expression:

    {host:web.test.in[Scenario,,bps].last()}<10000

Make sure to replace 'Scenario' with the real name of your scenario.

#### 场景步骤项

#### Scenario step items

一旦创建步骤，Zabbix
会自动添加以下监控项进行监控，将它们链接到所选应用程序。

As soon as a step is created, Zabbix automatically adds the following
items for monitoring, linking them to the selected application.

|监控项                                                       说明|<|
|----------------------------------------------------------------------|-|
|// 场景 <Scenario> 中步骤 <Step> 的下载速度 //   此监控项将收集关于|骤的下载速度（字节每秒）的信息。<br>监控项 key: web.test.in\[Scenario,Step,bps\]<br>类型：*Numeric(float)*|
|// 场景<Scenario>中此步骤<Step>的响应时间 //     此监控项将收集有关步|的响应时间的信息（以秒为单位）。响应时间从请求开始计时，直到所有信息传输完毕。<br>监控项 key: web.test.time\[Scenario,Step,resp\]<br>类型：*Numeric(float)*|
|// 场景 <Scenario> 的步骤 <Step> 的响应代码 //   此监控项将收集步骤|响应代码。<br>监控项 key: web.test.rspcode\[Scenario,Step\]<br>类型：*Numeric(unsigned)*|

|Item|Description|
|----|-----------|
|*Download speed for step <Step> of scenario <Scenario>*|This item will collect information about the download speed (bytes per second) of the step.<br>Item key: web.test.in\[Scenario,Step,bps\]<br>Type: *Numeric(float)*|
|*Response time for step <Step> of scenario <Scenario>*|This item will collect information about the response time of the step in seconds. Response time is counted from the beginning of the request until all information has been transferred.<br>Item key: web.test.time\[Scenario,Step,resp\]<br>Type: *Numeric(float)*|
|*Response code for step <Step> of scenario <Scenario>*|This item will collect response codes of the step.<br>Item key: web.test.rspcode\[Scenario,Step\]<br>Type: *Numeric(unsigned)*|

将分别使用实际场景和步骤名称而不是“Scenario”和“Step”。

Actual scenario and step names will be used instead of "Scenario" and
"Step" respectively.

::: noteclassic
添加的 Web 监控项将保留 30 天历史记录和 90
天趋势记录。
:::

::: noteclassic
Web monitoring items are added with a 30 day history and a
90 day trend retention period.
:::

::: noteclassic
如果场景名称以双引号开头或包含逗号或方括号，则它将在监控项键中正确引用。在其他情况下，不会引用
:::

::: noteclassic
If scenario name starts with a doublequote or contains comma
or square bracket, it will be properly quoted in item keys. In other
cases no additional quoting will be performed.
:::

这些监控项可用于创建触发器和定义通知条件。例如，要创建一个“Zabbix GUI
登录太慢”触发器，可以定义一个

{zabbix:web.test.time\[ZABBIX GUI,Login,resp\].last()}>3

These items can be used to create triggers and define notification
conditions. For example, to create a "Zabbix GUI login is too slow"
trigger, you can define a trigger expression:

    {zabbix:web.test.time[ZABBIX GUI,Login,resp].last()}>3

[comment]: # ({/new-fc2e76a2})
