[comment]: # aside: 2

[comment]: # translation:outdated

[comment]: # ({new-e6df368c})
# 1 虚拟机发现key值字段

下表列出了虚拟机发现key值返回的内容。

|**项目键**|<|<|
|-------------|-|-|
|**描述**                                                                                              *|字段**              **检|内容**|
|vmware.cluster.discovery|<|<|
|执行 VMware 集群发现 discovery,用于发现Hypervisorv（宿主机） 。                                       {\#CLUSTER.ID}|集群 ID。|<|
|^|{\#CLUSTER.NAME}|集群名称。|
|vmware.hv.discovery|<|<|
|执行 hypervisor 的 discovery 。                                                                       {\#|V.UUID}           唯一的|hypervisor 的 ID|
|^|{\#HV.ID}|Hypervisorv（宿主机） 的 ID（由HostSystem管理）|
|^|{\#HV.NAME}|Hypervisor（宿主机） 的名字|
|^|{\#CLUSTER.NAME}|Vmware群集名称，可能为空。|
|^|{\#DATACENTER.NAME}|Vmware 数据中心名称。|
|vmware.hv.datastore.discovery|<|<|
|执行 hypervisor 数据存储库的 discovery 。请注意，多个hypervisor 可以使用相同的数据存储(datastore)。   {\#DATASTORE}         数据存储|称。|<|
|vmware.vm.discovery|<|<|
|执行虚拟机的 discovery 。                                                                             {\#VM.|UID}           唯一虚拟机|D。|
|^|{\#VM.ID}|虚拟机 ID（由 VirtualMachine 管理）。|
|^|{\#VM.NAME}|虚拟机名。|
|^|{\#HV.NAME}|Hypervisor （宿主机）名称。|
|^|{\#CLUSTER.NAME}|Vmware 集群名称，可能为空。|
|^|{\#DATACENTER.NAME}|Vmware 数据中心名称。|
|vmware.vm.net.if.discovery|<|<|
|执行虚拟机网络接口的 discovery 。                                                                     {\#IFNAME}|网络接口名称。|<|
|vmware.vm.vfs.dev.discovery|<|<|
|执行虚拟机磁盘设备的 discovery 。                                                                     {\#DISKNAM|}          磁盘设备名称。|<|
|vmware.vm.vfs.fs.discovery|<|<|
|执行虚拟机文件系统的 discovery 。                                                                     {\#FSNAME}|文件系统名称。|<|

[comment]: # ({/new-e6df368c})
