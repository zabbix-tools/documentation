[comment]: # translation:outdated

[comment]: # ({new-c86bb2b9})
# 11. 维护

[comment]: # ({/new-c86bb2b9})

[comment]: # ({new-e70ea305})
#### 概述

在Zabbix里，可以为主机和主机群组定义维护期。有两种维护类型可选：一种是有数据收集；另一种是无数据收集。

在“有数据收集”的维护期里，触发器像往常一样正常处理，事件也会在需要的时候被创建。然而，对于正处在维护期的主机来说，如果在动作配置里勾选了
*维护期暂停操作（Pause operations while in maintenance）*
选项，那么问题升级会被暂停。在这种情况下，只要维护期间持续，可能包含的发送通知或者远程命令的升级步骤会被忽略。

比如，有三个升级步骤原计划是在问题发生后的第0分钟、30分钟和60分钟分别执行。现在定义一个半小时的维护期，持续时间刚好是从问题发生后的第10分钟到第40分钟。那么受维护期的影响，原计划在第30分钟和60分钟执行的步骤会被推迟半个小时。也就是说，步骤二会在问题发生后的第60分钟执行，步骤三会在问题发生后的第90分钟执行（假设问题仍然存在）。类似的，如果在维护期发生问题，那么问题升级会在维护期结束后开始。

如果需要在维护期间正常接收问题通知（没有延迟），必须在动作配置里取消勾选
*维护期暂停操作（Pause operations while in maintenance）* 选项。

::: noteclassic
只要有一个主机（触发器表达式中使用到的主机）不在维护模式里，Zabbix就可能会发送问题通知。
:::

在维护期间，Zabbix
server必须处于运行状态。Timer进程负责在每分钟的第0秒进行主机是否处于维护状态的切换。Zabbix
proxy节点不论在什么维护类型（包含“无数据收集”维护）下都会收集数据。只不过如果是“无数据收集”类型，这些数据后来会被Zabbix
server节点忽略。

当“无数据收集”维护期间刚结束的时候，使用了nodata()函数的触发器不会被触发。这些触发器在下一次检查以后才可能会被触发。

如果在主机处于维护状态的时候添加了一个日志相关的监控项，那么当维护结束时，只会收集自维护结束以来的新日志文件内容。

如果主机处于“无数据收集”维护类型期间，此时给它发送一个带时间戳的值（例如，使用
[Zabbix
sender](/manpages/zabbix_sender)），那么这个值会被丢弃。然而，如果主机的维护期过期了，此时给它发送一个带有时间戳的值，是会被接收的。

<note
important>为确保重复性维护期（每天，每周，每月）的行为在的预期之中，Zabbix的所有部件都应该使用相同的时区。
:::

[comment]: # ({/new-e70ea305})


[comment]: # ({new-6ef20399})
#### 配置

配置维护期：

-   切换到： *配置（Configuration） → 维护（Maintenance）*
-   单击 *创建维护期间（Create maintenance period）*
    （或者单击已存在的维护期的名称）

**维护（Maintenance）**选项卡包含了常见的维护期属性：

![](../../assets/en/manual/maintenance/maintenance.png)

所有必填输入字段都标有红色星号。

|参数                             说|<|
|--------------------------------------|-|
|*名称（Name）*                   维护期|的名称。|
|*维护类型（Maintenance type）*   有两种维护|型可以设置：<br>**有数据收集（With data collection）** - 在维护期间数据会被server收集，触发器也会被处理<br>**无数据收集（No data collection）** - 在维护期间数据不会被收集|
|*启用自从（Active since）*       执行维护期|的日期和时间变为活动状态。<br>*注意*： 单独设置这个时间并不能激活维护期间；需要切换到 *期间（Periods）* 选项卡进行操作。|
|*启用直到（Active till）*        执行维护期|的日期和时间停止处于活动状态。|
|*描述（Description）*            维护期|的描述。|

The **Periods** tab allows you to define the exact days and hours when
the maintenance takes place. Clicking on *New* opens a flexible
*Maintenance period* form where you can define the times - for daily,
weekly, monthly or one-time maintenance.

**期间（Periods）**选项卡允许您定义维护发生的确切天数和小时数。单击
*新建（New）* 会打开一个 *维护期间（Maintenance period）*
表单，可灵活配置维护期间的时间段 - 每天、每周、每月或者仅一次。

![](../../assets/en/manual/maintenance/maintenance2.png)

Daily and weekly periods have an *Every day/Every week* parameter, which
defaults to 1. Setting it to 2 would make the maintenance take place
every two days or every two weeks and so on. The starting day or week is
the day or week that *Active since* time falls on.

每天和每周期间有一个 *每天（Every day）/每周（Every week）*
参数，默认值是1。如果设置为2，那么维护期间就是每两天或者每两周执行一次，以此类推。起始日期或星期是
*启用自从（Active since）* 时间起作用时的日期或星期。

For example, having *Active since* set to 2013-09-06 12:00 and an hour
long daily recurrent period every two days at 23:00 will result in the
first maintenance period starting on 2013-09-06 at 23:00, while the
second maintenance period will start on 2013-09-08 at 23:00. Or, with
the same *Active since* time and an hour long daily recurrent period
every two days at 01:00, the first maintenance period will start on
2013-09-08 at 01:00, and the second maintenance period on 2013-09-10 at
01:00.

比如，*启用自从（Active since）* 设置为2013-09-06
12:00，如果有一个在23:00开始的为期一个小时的维护期间，每两天执行一次，那么第一次维护期间将会开始于2013-09-06
23:00，第二次维护期间开始于2013-09-08
23:00。或者，再举个例子，如果还是那个相同的 *启用自从（Active
since）*，每两天执行一次，每次一小时，开始时间设定为01:00，那么，第一次维护期间将开始于2013-09-08
01:00，第二次开始于2013-09-10 01:00。

The **Hosts & Groups** tab allows you to select the hosts and host
groups for maintenance.

**主机（Hosts） &
主机组（Groups）**选项卡允许选择需要维护的主机和主机组。

![](../../assets/en/manual/maintenance/maintenance3.png)

Specifying a parent host group implicitly selects all nested host
groups. Thus the maintenance will also be executed on hosts from nested
groups.

如果选择了某个父主机组，那么会隐式的选中其所有内嵌的主机组。因此，维护也将在内嵌的主机组的主机上执行。

[comment]: # ({/new-6ef20399})

[comment]: # ({new-4dc2aa1c})
#### Display

[comment]: # ({/new-4dc2aa1c})

[comment]: # ({new-6aeb0a4c})

::: noteimportant
When creating a maintenance period, the [time zone](/manual/web_interface/time_zone) of the user who creates it is used.
However, when recurring maintenance periods (*Daily*, *Weekly*, *Monthly*) are scheduled, the time zone of the Zabbix server is used.
To ensure predictable behavior of recurring maintenance periods, it is required to use a common time zone for all parts of Zabbix.
:::

[comment]: # ({/new-6aeb0a4c})

[comment]: # ({new-3b17f2f3})

When done, press *Add* to add the maintenance period to the *Periods* block.

Note that Daylight Saving Time (DST) changes do not affect how long the maintenance will be.
For example, let's say that we have a two-hour maintenance configured that usually starts at 01:00 and finishes at 03:00:

-   if after one hour of maintenance (at 02:00) a DST change happens and current time changes from 02:00 to 03:00, the maintenance will continue for one more hour (till 04:00);
-   if after two hours of maintenance (at 03:00) a DST change happens and current time changes from 03:00 to 02:00, the maintenance will stop, because two hours have passed;
-   if a maintenance period starts during the hour that is skipped by a DST change, then the maintenance will not start.

If a maintenance period is set to "1 day" (the actual period of the maintenance is 24 hours, since Zabbix calculates days in hours), starts at 00:00 and finishes at 00:00 the next day:

-   the maintenance will stop at 01:00 the next day if current time changes forward one hour;
-   the maintenance will stop at 23:00 that day if current time changes back one hour.

[comment]: # ({/new-3b17f2f3})

[comment]: # ({new-924e8c68})
#### 显示

An orange wrench icon next to the host name indicates that this host is
in maintenance in the *Monitoring → Dashboard* and *Inventory → Hosts →
Host inventory* sections.

主机名称旁边的橙色扳手图标表示该主机正处于维护状态。在
*监测中（Monitoring） → 仪表板（Dashboard）* 以及 *资产记录（Inventory）
→ 主机（Hosts） → 主机资产记录（Host inventory）*
页面，都可能看到这个维护标志。

![](../../assets/en/manual/maintenance/maintenance_icon.png)

Maintenance details are displayed when the mouse pointer is positioned
over the icon.

当鼠标指针停留在扳手图标上面的时候会显示维护的详细信息。

::: noteclassic
The display of hosts in maintenance in the Dashboard can be
unset altogether with the dashboard filtering function.
:::

::: noteclassic
可以使用仪表板过滤功能完全取消显示仪表板中处于维护状态的主机。
:::

Additionally, hosts in maintenance get an orange background in
*Monitoring → Maps* and in *Configuration → Hosts* their status is
displayed as 'In maintenance'.

此外，维护中的主机在 *监测中（Monitoring） → 拓扑图（Maps）*
中获得橙色背景，在 *配置（Configuration） → 主机（Hosts）*
中其状态显示为“维护中（In maintenance）”。

[comment]: # ({/new-924e8c68})



[comment]: # ({new-0e33672d})
##### Displaying hosts in maintenance

An orange wrench icon
![](../../assets/en/manual/web_interface/frontend_sections/configuration/maintenance_wrench_icon.png)
next to the host name indicates that this host is in maintenance in:

-   *Monitoring → Dashboard*
-   *Monitoring → Problems*
-   *Inventory → Hosts → Host inventory details*
-   *Configuration → Hosts* (See 'Status' column)

![](../../assets/en/manual/maintenance/maintenance_icon.png)

Maintenance details are displayed when the mouse pointer is positioned
over the icon.

Additionally, hosts in maintenance get an orange background in
*Monitoring → Maps*.

[comment]: # ({/new-0e33672d})

[comment]: # ({new-a923614c})
##### Displaying suppressed problems

Normally problems for hosts in maintenance are suppressed, i.e. not
displayed in the frontend. However, it is also possible to configure
that suppressed problems are shown, by selecting the *Show suppressed
problems* option in these locations:

-   *Monitoring* → *Dashboard* (in *Problem hosts*, *Problems*,
    *Problems by severity*, *Trigger overview* widget configuration)
-   *Monitoring* → *Problems* (in the filter)
-   *Monitoring* → *Maps* (in map configuration)
-   Global
    [notifications](/manual/web_interface/user_profile/global_notifications)
    (in user profile configuration)

When suppressed problems are displayed, the following icon is displayed:
![](../../assets/en/manual/web_interface/icon_suppressed.png). Rolling a
mouse over the icon displays more details:

![](../../assets/en/manual/web_interface/info_suppressed2.png)

[comment]: # ({/new-a923614c})
