[comment]: # translation:outdated

[comment]: # ({058950f0-51abb0f6})
#   SUSE Linux Enterprise Server

[comment]: # ({/058950f0-51abb0f6})

[comment]: # ({new-ee73b6da})
### 概述

Zabbix官方包适用于：

|   |   |
|---|---|
|SUSE Linux Enterprise Server 15|[Download](https://www.zabbix.com/download?zabbix=6.0&os_distribution=suse_linux_enterprise_server&os_version=15&db=mysql)|
|SUSE Linux Enterprise Server 12|[Download](https://www.zabbix.com/download?zabbix=6.0&os_distribution=suse_linux_enterprise_server&os_version=12&db=mysql)|

::: 注意
Zabbix 6.0 还未发布。 下载链接目前链至6.0之前的版本。
:::

::: 注意
由于旧版本MySQL库不支持*Verify CA* [encryption
mode](/manual/appendix/install/db_encrypt/mysql) 在SLES 12
（所有次要操作系统版本）中不支持。

:::

[comment]: # ({/new-ee73b6da})

[comment]: # ({new-37fbf2db})
### 添加Zabbix存储库

安装存储库配置包。包中含yum（软件包管理器）配置文件。
SLES 15:

    # rpm -Uvh --nosignature https://repo.zabbix.com/zabbix/6.0/sles/15/x86_64/zabbix-release-6.0-1.sles15.noarch.rpm
    # zypper --gpg-auto-import-keys refresh 'Zabbix Official Repository' 

SLES 12:

    # rpm -Uvh --nosignature https://repo.zabbix.com/zabbix/6.0/sles/12/x86_64/zabbix-release-6.0-1.sles12.noarch.rpm
    # zypper --gpg-auto-import-keys refresh 'Zabbix Official Repository' 

请注意，用于[scheduled report generation](/manual/web_interface/frontend_sections/reports/scheduled)的Zabbix网络服务进程，需要安装Google Chrome浏览器。安装包内不含浏览器，需手动另安装。

[comment]: # ({/new-37fbf2db})

[comment]: # ({f3180288-ef4f3e50})
### 安装Server/frontend/agent

安装支持MySQL的Zabbix server/frontend/agent:

    # zypper install zabbix-server-mysql zabbix-web-mysql zabbix-apache-conf zabbix-agent

 如果将包用于 Nginx 网络服务器，请将命令中的 'apache' 替换为 'nginx' 。详见：[Nginx setup for Zabbix on SLES 12/15](/manual/appendix/install/nginx)。

若使用Zabbix agen 2（仅 SLES 15 SP1+)，需将命令中的 'zabbix-agent' 替换为 'zabbix-agent2' 。

安装支持MySQL的Zabbix proxy：

    # zypper install zabbix-proxy-mysql

将命令中的 'mysql'替换为 'pgsql' 以使用PostgreSQL。

[comment]: # ({/f3180288-ef4f3e50})

[comment]: # ({4b3a8211-c573e862})
#### 创建数据库

对Zabbix [server](/manual/concepts/server) 和[proxy](/manual/concepts/proxy) 来说，无需运行 Zabbix [agent](/manual/concepts/agent)来守护数据库进程是必要的。

::: 警示
Zabbix server和Zabbix proxy不能使用同一个数据库，必须单独创。因此，如果他们被安装在了同一个主机上，数据库应用不同的名称创建。
:::

使用说明安装数据库，[MySQL](/manual/appendix/install/db_scripts#mysql) 与[PostgreSQL](/manual/appendix/install/db_scripts#postgresql)。

[comment]: # ({/4b3a8211-c573e862})

[comment]: # ({308010a3-35bc057d})
#### 数据导入

使用MySQL导入**server** 初始模型和数据：

    # zcat /usr/share/doc/packages/zabbix-sql-scripts/mysql/create.sql.gz | mysql -uzabbix -p zabbix

系统将提示你输入新创建的数据库密码。

使用PostgreSQL:

    # zcat /usr/share/doc/packages/zabbix-sql-scripts/postgresql/create.sql.gz | sudo -u zabbix psql zabbix

使用TimescaleDB，除了前面的命令，还要运行：

    # zcat /usr/share/doc/packages/zabbix-sql-scripts/postgresql/timescaledb.sql.gz | sudo -u <username> psql zabbix

::: 警示
须是Zabbix server支持的TimescaleDB。
:::

对于proxy，导入初始模型：

    # zcat /usr/share/doc/packages/zabbix-sql-scripts/mysql/schema.sql.gz | mysql -uzabbix -p zabbix

对于带有PostgreSQL的proxy：

    # zcat /usr/share/doc/packages/zabbix-sql-scripts/postgresql/schema.sql.gz | sudo -u zabbix psql zabbix

[comment]: # ({/308010a3-35bc057d})

[comment]: # ({3473e5a5-0ea127cd})
#### 为 Zabbix server/proxy配置数据库

编辑 /etc/zabbix/zabbix\_server.conf (and zabbix\_proxy.conf) 来使用它们各自的数据库。例如：

    # vi /etc/zabbix/zabbix_server.conf
    DBHost=localhost
    DBName=zabbix
    DBUser=zabbix
    DBPassword=<password>

在DBPassword中对MySQL使用Zabbix数据库密码 ；在PosgreSQL中用PosgreSQL用户密码。

将 `DBHost=` 与PostgreSQL一起用。你可能希望保留默认值设置  `DBHost=localhost` (或一个IP地址)，但这会使PostgreSQL使用网络套接连接到Zabbix。

[comment]: # ({/3473e5a5-0ea127cd})

[comment]: # ({3f420d0b-d162545b})
#### Zabbix前端配置

根据使用的网络服务器 (Apache/Nginx) 为Zabbix前端编辑相应配置文件：

-   对于Apache，配置文件在 `/etc/apache2/conf.d/zabbix.conf`。一些PHP设置已经配置好了。但还是有必要取消设置的注释 "date.timezone"
    ， [设置正确的时区](http://php.net/manual/en/timezones.php) 。

```{=html}
<!-- -->
```
    php_value max_execution_time 300
    php_value memory_limit 128M
    php_value post_max_size 16M
    php_value upload_max_filesize 2M
    php_value max_input_time 300
    php_value max_input_vars 10000
    php_value always_populate_raw_post_data -1
    # php_value date.timezone Europe/Riga

-   zabbix-nginx-conf包为Zabbix前端安装单独的Nginx server。它的配置文件位于`/etc/nginx/conf.d/zabbix.conf`。为了运行Zabbix 前端，但还是有必要取消注释并设置 `listen` 和/或 `server_name`指令。

```{=html}
<!-- -->
```
    # listen 80;
    # server_name example.com;

-   Zabbix使用自己的专用php-fpm连接池链接Nginx：

它的配置文件位于`/etc/php7/fpm/php-fpm.d/zabbix.conf`。一些PHP设置已经设置好了。但你还是有必要正确设置[日期.时区](http://php.net/manual/en/timezones.php)。

    php_value[max_execution_time] = 300
    php_value[memory_limit] = 128M
    php_value[post_max_size] = 16M
    php_value[upload_max_filesize] = 2M
    php_value[max_input_time] = 300
    php_value[max_input_vars] = 10000
    ; php_value[date.timezone] = Europe/Riga

现在，你可以继续进行 [前端安装步骤](/manual/installation/install#installing_frontend) 以访问新安装的Zabbix。

请注意Zabbix proxy没有前端，只与Zabbix server相通。

[comment]: # ({/3f420d0b-d162545b})

[comment]: # ({1b4fec93-71fda19d})
#### 启动Zabbix server/agent程序

启动Zabbix server和agent进程，并让其随系统启动而启动。

使用Apache网络服务器：

    # systemctl restart zabbix-server zabbix-agent apache2 php-fpm
    # systemctl enable zabbix-server zabbix-agent apache2 php-fpm

在Nginx网络服务器中用'apache2'替换'nginx'。

[comment]: # ({/1b4fec93-71fda19d})

[comment]: # ({3dc129d3-8e553867})
### 安装debuginfo包

为了启用debuginfo存储库编辑 */etc/zypp/repos.d/zabbix.repo*
文件。请将 zabbix-debuginfo存储库的`enabled=0`改为`enabled=1`。

    [zabbix-debuginfo]
    name=Zabbix Official Repository debuginfo
    type=rpm-md
    baseurl=http://repo.zabbix.com/zabbix/4.5/sles/15/x86_64/debuginfo/
    gpgcheck=1
    gpgkey=http://repo.zabbix.com/zabbix/4.5/sles/15/x86_64/debuginfo/repodata/repomd.xml.key
    enabled=0
    update=1

然后就可安装 zabbix-***<component>***-debuginfo包了。

[comment]: # ({/3dc129d3-8e553867})
