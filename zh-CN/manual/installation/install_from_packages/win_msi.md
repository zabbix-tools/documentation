[comment]: # translation:outdated

[comment]: # ({90295778-7050624a})
# 4 从MSI安装Windows代理

[comment]: # ({/90295778-7050624a})

[comment]: # ({2bcef682-04777b60})
#### 概述

可以从Windows MSI安装包 (32位或64位) （Zabbix Windows agent can be installed from Windows MSI installer packages available for
[download](https://www.zabbix.com/download_agents#tab:44).

32位包不能安装在64位Windows中。

所有软件包都支持TLS，然而，配置TLS是可选的。

支持UI和命令行的安装。

[comment]: # ({/2bcef682-04777b60})

[comment]: # ({84bab88d-0fc3b21c})

#### 安装步骤

请双击下载MSI文件进行安装。

![](../../../../assets/en/manual/installation/install_from_packages/msi0_b.png)

![](../../../../assets/en/manual/installation/install_from_packages/msi0_c.png)

接受许可证已进入下一步。

![](../../../../assets/en/manual/installation/install_from_packages/msi0_d.png)

具体参数。

|参数|描述|
|---------|-----------|
|*主机名*|指定主机名。|
|*Zabbix server IP/DNS*|指定Zabbix server的IP/DNS。|
|*Agent监听端口*|指定代理监听端口 (默认为10050)。|
|*主动检查Server或Proxy*|为激活agent主动检查指定Zabbix server/proxy的IP/DNS。|
|*启用PSK*|选中校验框，通过预共享密钥激活TLS支持。|
|*将agent位置添加到PATH*|将agent位置添加至PATH变量。|

![](../../../../assets/en/manual/installation/install_from_packages/msi0_e.png)

输入预共享密钥的标识和对应值。 此步骤仅在上一步中选中 *Enable PSK* 之后才有用。

![](../../../../assets/en/manual/installation/install_from_packages/msi0_f.png)

选择要安装的Zabbix组件- [Zabbix agent daemon](/manual/concepts/agent), [Zabbix sender](/manual/concepts/sender), [Zabbix get](/manual/concepts/get).

![](../../../../assets/en/manual/installation/install_from_packages/msi0_g.png)

Zabbix组件和配置文件将安装在程序文件 *Zabbix Agent* 文件夹中。 zabbix\_agentd.exe 在Windows服务中将被设置为自动启动。 

![](../../../../assets/en/manual/installation/install_from_packages/msi0_h.png)

[comment]: # ({/84bab88d-0fc3b21c})

[comment]: # ({7307ed07-1ca3dd90})
#### 命令行安装 

[comment]: # ({/7307ed07-1ca3dd90})

[comment]: # ({new-89028c66})
##### 支持的参数

创建的MSI支持以下参数集：

|序号|参数|说明|
|------|---------|-----------|
|1|日志类型|<|
|2|日志文件|<|
|3|服务器|<|
|4|监听|<|
|5|服务器活动|<|
|6|主机名|<|
|7|超时|<|
|8|TLS连接|<|
|9|TLS接受|<|
|10|TLSPSK身份|<|
|11|TLSPSK文件|<|
|12|TLSPSK值|<|
|13|TLSCA文件|<|
|14|TLSCRL文件|<|
|15|TLS服务器证书颁发者|<|
|16|TLS服务器证书对象|<|
|17|TLS证书文件|<|
|18|TLSKEY文件|<|
|19|LISTENIP|<|
|20|主机界面|<|
|21|主机元数据|<|
|22|主机元数据项|<|
|23|端口状态|仅限Zabbix agent。|
|24|启用持久缓冲区|仅限Zabbix agent。|
|25|持续缓冲区|仅限Zabbix agent。|
|26|持续缓冲文件|仅限Zabbix agent。|
|27|安装文件夹|<|
|28|启用路径|<|
|29|跳过|`SKIP=fw` - 不安装防火墙规则|
|30|包含|由 `;` 分隔的序列|
|31|允许拒绝密钥|"AllowKey" 和 "DenyKey" [参数](/manual/config/items/restrict_checks) 的序列，用 `;`分隔，使用 `\\;` 转义分隔符。|

安装，可运行：

    SET INSTALLFOLDER=C:\Program Files\za

    msiexec /l*v log.txt /i zabbix_agent-6.0.0-x86.msi /qn^
     LOGTYPE=file^
     LOGFILE="%INSTALLFOLDER%\za.log"^
     SERVER=192.168.6.76^
     LISTENPORT=12345^
     SERVERACTIVE=::1^
     HOSTNAME=myHost^
     TLSCONNECT=psk^
     TLSACCEPT=psk^
     TLSPSKIDENTITY=MyPSKID^
     TLSPSKFILE="%INSTALLFOLDER%\mykey.psk"^
     TLSCAFILE="c:\temp\f.txt1"^
     TLSCRLFILE="c:\temp\f.txt2"^
     TLSSERVERCERTISSUER="My CA"^
     TLSSERVERCERTSUBJECT="My Cert"^
     TLSCERTFILE="c:\temp\f.txt5"^
     TLSKEYFILE="c:\temp\f.txt6"^
     ENABLEPATH=1^
     INSTALLFOLDER="%INSTALLFOLDER%"^
     SKIP=fw^
     ALLOWDENYKEY="DenyKey=vfs.file.contents[/etc/passwd]"

or

    msiexec /l*v log.txt /i zabbix_agent-6.0.0-x86.msi /qn^
     SERVER=192.168.6.76^
     TLSCONNECT=psk^
     TLSACCEPT=psk^
     TLSPSKIDENTITY=MyPSKID^
     TLSPSKVALUE=1f87b595725ac58dd977beef14b97461a7c1045b9a1c963065002c5473194952

[comment]: # ({/new-89028c66})

[comment]: # ({new-bfabef9c})
##### Examples

To install Zabbix Windows agent from the command-line, you may run, for example:

    SET INSTALLFOLDER=C:\Program Files\za

    msiexec /l*v log.txt /i zabbix_agent-7.0.0-x86.msi /qn^
     LOGTYPE=file^
     LOGFILE="%INSTALLFOLDER%\za.log"^
     SERVER=192.168.6.76^
     LISTENPORT=12345^
     SERVERACTIVE=::1^
     HOSTNAME=myHost^
     TLSCONNECT=psk^
     TLSACCEPT=psk^
     TLSPSKIDENTITY=MyPSKID^
     TLSPSKFILE="%INSTALLFOLDER%\mykey.psk"^
     TLSCAFILE="c:\temp\f.txt1"^
     TLSCRLFILE="c:\temp\f.txt2"^
     TLSSERVERCERTISSUER="My CA"^
     TLSSERVERCERTSUBJECT="My Cert"^
     TLSCERTFILE="c:\temp\f.txt5"^
     TLSKEYFILE="c:\temp\f.txt6"^
     ENABLEPATH=1^
     INSTALLFOLDER="%INSTALLFOLDER%"^
     SKIP=fw^
     ALLOWDENYKEY="DenyKey=vfs.file.contents[/etc/passwd]"

You may also run, for example:

    msiexec /l*v log.txt /i zabbix_agent-7.0.0-x86.msi /qn^
     SERVER=192.168.6.76^
     TLSCONNECT=psk^
     TLSACCEPT=psk^
     TLSPSKIDENTITY=MyPSKID^
     TLSPSKVALUE=1f87b595725ac58dd977beef14b97461a7c1045b9a1c963065002c5473194952

::: noteclassic
If both TLSPSKFILE and TLSPSKVALUE are passed, then TLSPSKVALUE will be written to TLSPSKFILE.
:::

[comment]: # ({/new-bfabef9c})
