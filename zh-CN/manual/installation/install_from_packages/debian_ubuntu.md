[comment]: # translation:outdated

[comment]: # ({31732a7f-d96d3275})
#   Debian/Ubuntu/Raspbian

[comment]: # ({/31732a7f-d96d3275})

[comment]: # ({new-9df568e9})
＃＃＃ 概述

官方 Zabbix 软件包可用于：

| | |
|---|---|
|Debian 11 (Bullseye)|[下载](https://www.zabbix.com/download?zabbix=6.0&os_distribution=debian&os_version=11_bullseye&db=mysql)|
|Debian 10 (Buster)|[下载](https://www.zabbix.com/download?zabbix=6.0&os_distribution=debian&os_version=10_buster&db=mysql)|
|Debian 9 (Stretch)|[下载](https://www.zabbix.com/download?zabbix=6.0&os_distribution=debian&os_version=9_stretch&db=mysql)|
|Ubuntu 20.04 (Focal Fossa) LTS|[下载](https://www.zabbix.com/download?zabbix=6.0&os_distribution=ubuntu&os_version=20.04_focal&db=mysql)|
|Ubuntu 18.04 (Bionic Beaver) LTS|[下载](https://www.zabbix.com/download?zabbix=6.0&os_distribution=ubuntu&os_version=18.04_bionic&db=mysql)|
|Ubuntu 16.04 (Xenial Xerus) LTS|[下载](https://www.zabbix.com/download?zabbix=6.0&os_distribution=ubuntu&os_version=16.04_xenial&db=mysql)|
|Ubuntu 14.04 (Trusty Tahr) LTS|[下载](https://www.zabbix.com/download?zabbix=6.0&os_distribution=ubuntu&os_version=14.04_trusty&db=mysql)|
|Raspbian 11 (Bullseye)|[下载](https://www.zabbix.com/download?zabbix=6.0&os_distribution=raspberry_pi_os&os_version=11_bullseye&db=mysql)|
|Raspbian 10 (Buster)|[下载](https://www.zabbix.com/download?zabbix=6.0&os_distribution=raspberry_pi_os&os_version=10_buster&db=mysql)|
|Raspbian 9 (Stretch)|[下载](https://www.zabbix.com/download?zabbix=6.0&os_distribution=raspberry_pi_os&os_version=9_stretch&db=mysql)|

软件包可用于 MySQL/PostgreSQL 数据库和Apache/Nginx 网络服务器支持。

::: 注意
Zabbix 6.0 尚未发布。下载链接链至6.0 之前的软件包。
:::

[comment]: # ({/new-9df568e9})

[comment]: # ({new-ae631a63})
### 安装注意事项

请查阅 [installation instructions](https://www.zabbix.com/download?zabbix=6.0&os_distribution=debian&os_version=10_buster&db=mysql) 下载页面中的介绍：

-   安装存储库
-   安装server/agent/前端
-   创建初始数据库，导入初始数据
-   为Zabbix server配置数据库
-   为Zabbix前端配置PHP
-   启动server/agent进程
-   配置 Zabbix前端

如你希望以根用户运行 Zabbix agent，请参考 [以根运行agent](/manual/appendix/install/run_agent_as_root)。

Zabbix网络服务进程，用于[scheduled report generation](/manual/web_interface/frontend_sections/reports/scheduled),
需要安装 Google Chrome 浏览器。浏览器的安装包不包含，需另行手动安装。

[comment]: # ({/new-ae631a63})

[comment]: # ({new-99a402fa})
#### 使用 Timescale DB导入数据

对于 TimescaleDB, 除了PostgreSQL的导入的命令外，还可运行:

    # zcat /usr/share/doc/zabbix-sql-scripts/postgresql/timescaledb.sql.gz | sudo -u zabbix psql zabbix

::: 警示
仅Zabbix server支持TimescaleDB。
:::

[comment]: # ({/new-99a402fa})


[comment]: # ({47daee41-fa73411a})
#### 配置 SELinux 

参阅适用于RHEL/CentOS 的 [SELinux configuration](/manual/installation/install_from_packages/rhel_centos#selinux_configuration)。

前端和SELinux配置好之后，重启Apache网络服务器：

    # service apache2 restart

[comment]: # ({/47daee41-fa73411a})

[comment]: # ({9885dd4e-a12da003})
### Proxy安装

添加好所需存储库后，可通过运行以下命令来安装Zabbix proxy：

    # apt install zabbix-proxy-mysql

将命令中的 'mysql' 替换为 'pgsql' 以使用 PostgreSQL，或者替换为 'sqlite3' 以使用 SQLite3 （仅proxy适用）。

[comment]: # ({/9885dd4e-a12da003})

[comment]: # ({4ac8ad7a-fe6abb8e})
##### 创建数据库

为Zabbix proxy[创建](/manual/appendix/install/db_scripts) 一个单独的数据库。

Zabbix server和Zabbix proxy 不能使用同一个数据库。如果他们是安装在同一个主机中的，则proxy数据库需要另命名。

[comment]: # ({/4ac8ad7a-fe6abb8e})

[comment]: # ({new-2ab835d7})
##### 数据导入

导入初始架构:

    # zcat /usr/share/doc/zabbix-sql-scripts/mysql/schema.sql.gz | mysql -uzabbix -p zabbix

对于带 PostgreSQL（或 SQLite）的proxy:

    # zcat /usr/share/doc/zabbix-sql-scripts/postgresql/schema.sql.gz | sudo -u zabbix psql zabbix
    # zcat /usr/share/doc/zabbix-sql-scripts/sqlite3/schema.sql.gz | sqlite3 zabbix.db

[comment]: # ({/new-2ab835d7})

[comment]: # ({8ceddaff-d0a225c7})
##### 为Zabbix proxy配置数据库

编辑 zabbix\_proxy.conf:

    # vi /etc/zabbix/zabbix_proxy.conf
    DBHost=localhost
    DBName=zabbix
    DBUser=zabbix
    DBPassword=<password>

为 Zabbix proxy创建单独的数据库或重命名数据库。

在DBPassword 中对MySQL使用Zabbix数据库密码; 
PosgreSQL 用户
PosgreSQL 密码

将 `DBHost=` 与PostgreSQL一起用。你可能需要保留默认设置 `DBHost=localhost` (或1个 IP 地址)，但这可能会使PostgreSQL 通过网络套连接至Zabbix。参见 [respective section](/manual/installation/install_from_packages/rhel_centos#selinux_configuration)
获取 RHEL/CentOS 的指示说明。

[comment]: # ({/8ceddaff-d0a225c7})




[comment]: # ({0329186a-27de2ced})
##### 启动 Zabbix proxy进程

要启动Zabbix代理进程并使其在系统启动时启动，请执行以下操作:

    # systemctl restart zabbix-proxy
    # systemctl enable zabbix-proxy

[comment]: # ({/0329186a-27de2ced})

[comment]: # ({7f3b3dcb-871a973b})
##### 前端配置

Zabbix proxy没有前端；它只与Zabbix server通信。

[comment]: # ({/7f3b3dcb-871a973b})

[comment]: # ({718e8cd0-cd9340bd})
### 安装Java gateway

仅当你想监控JMX应用程序时，才需要安装 [Java gateway](/manual/concepts/java) 。 Java gateway 是轻量级的不需要数据库。

一旦添加了所需存储库，就可通过运行如下命令安装Zabbix Java gateway：

    # apt install zabbix-java-gateway

了解更多关于配置和运行Java gateway的详细信息可跳转至 [setup](/manual/concepts/java/from_debian_ubuntu) 。

[comment]: # ({/718e8cd0-cd9340bd})
