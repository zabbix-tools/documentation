[comment]: # translation:outdated

[comment]: # ({ee6a3858-bfa3f768})
# 5     从 PKG 安装 Mac OS Agent

[comment]: # ({/ee6a3858-bfa3f768})

[comment]: # ({ae3409af-5eec6c30})
#### 概述

Zabbix Mac OS Agent 可以从 PKG 安装程序包安装。
点此 [下载](https://www.zabbix.com/download_agents#tab:44)。
加密版本和不加密版本均可以下载。

[comment]: # ({/ae3409af-5eec6c30})

[comment]: # ({9f525fc1-3688af62})
#### 安装 agent

可以使用图形用户界面安装代理，也可以从命令行。 例如:

    sudo installer -pkg zabbix_agent-5.4.0-macos-amd64-openssl.pkg -target /

确保在命令中使用正确的 Zabbix 包版本。它必须与下载的程序包的名称匹配。

[comment]: # ({/9f525fc1-3688af62})

[comment]: # ({b04564d8-052e551c})
#### 运行 Agent

Agent将在安装或重新启动后自动启动。

如有需要，你可以编辑位于`/usr/local/etc/zabbix/zabbix_agentd.conf` 的配置文件。

手动启动 agent，你可以执行:

    sudo launchctl start com.zabbix.zabbix_agentd

手动停止 agent，你可以执行:

    sudo launchctl stop com.zabbix.zabbix_agentd

在升级过程中，现有配置文件不会被覆盖。
如有需要，它将被替换为一个新的 `zabbix_agentd.conf.NEW` 文件，用于查看和更新现有配置文件。
请记得在手动更改配置文件后重新启动 agent 。

[comment]: # ({/b04564d8-052e551c})

[comment]: # ({e0139420-3150eaf4})
#### 故障排除和删除代理

本节列出了一些有用的命令，可用于故障排除和删除 Zabbix 代理安装。

查看 Zabbix 代理是否正在运行：

    ps aux| grep zabbix_agentd

查看是否已经从包中安装了 Zabbix 代理：

    $ pkgutil --pkgs | grep zabbix
    com.zabbix.pkg.ZabbixAgent

查看从安装程序包中安装的文件（请注意此视图中不显示初始的 `/` ）：

    $ pkgutil --only-files --files com.zabbix.pkg.ZabbixAgent
    库/LaunchDaemons/com.zabbix.zabbix_agentd.plist
    usr/local/bin/zabbix_get
    usr/local/bin/zabbix_sender
    usr/local/etc/zabbix/zabbix_agentd/userparameter_examples.conf.NEW
    usr/local/etc/zabbix/zabbix_agentd/userparameter_mysql.conf.NEW
    usr/local/etc/zabbix/zabbix_agentd.conf.NEW
    usr/local/sbin/zabbix_agentd

停止Zabbix代理，如果它是用 `launchctl` 启动的：

    sudo launchctl unload /Library/LaunchDaemons/com.zabbix.zabbix_agentd.plist

删除随安装程序包一起安装的文件（包括配置和日志）：

    sudo rm -f /Library/LaunchDaemons/com.zabbix.zabbix_agentd.plist
    sudo rm -f /usr/local/sbin/zabbix_agentd
    sudo rm -f /usr/local/bin/zabbix_get
    sudo rm -f /usr/local/bin/zabbix_sender
    sudo rm -rf /usr/local/etc/zabbix
    sudo rm -rf /var/log/zabbix

忘记已经安装了Zabbix agent：

    sudo pkgutil --forget com.zabbix.pkg.ZabbixAgent

[comment]: # ({/e0139420-3150eaf4})
