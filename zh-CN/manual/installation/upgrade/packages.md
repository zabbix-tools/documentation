[comment]: # translation:outdated

[comment]: # ({4483cb2b-490a37c2})
# 由二进制包升级

[comment]: # ({/4483cb2b-490a37c2})

[comment]: # ({8520fca9-0fa07e9f})
#### 概述
本章节提供使用 Zabbix 的 RPM 和 DEB 二进制包成功升级至 Zabbix 6.0
所需的步骤[升级](/manual/installation/upgrade) ：
- [红帽企业 Linux/CentOS](/manual/installation/upgrade/packages/rhel_centos)
- [Debian/Ubuntu](/manual/installation/upgrade/packages/debian_ubuntu)

[comment]: # ({/8520fca9-0fa07e9f})


[comment]: # ({17da76c3-69a2013d})
##### 由操作系统存储库中的Zabbix软件包进行升级

通常，发行版本的操作系统（特别是基于 Debian 的发行版）会提供基于自身系统的Zabbix软件包。\
请注意，Zabbix 不支持这些软件包，它们通常已经过时并且缺乏最新的功能和错误修复。 建议通过由官方支持的 [repo.zabbix.com](https://repo.zabbix.com/) 软件安装包来完成安装。

若需从 OS 发行版提供的软件包进行升级（或在某时已安装），请按照以下步骤换到官方 Zabbix 软件包：

1. 首先，卸载就版本的安装包。
2. 检查卸载后可能留下的残留文件。
3. 按照Zabbix提供的 [installation instructions](https://www.zabbix.com/download)安装官方包。

切勿进行直接更新，因为这可能会导致安装中断。

[comment]: # ({/17da76c3-69a2013d})
