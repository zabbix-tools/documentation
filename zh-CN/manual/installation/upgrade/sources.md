[comment]: # translation:outdated

[comment]: # ({313be834-7900383c})
# 从源码包升级

[comment]: # ({/313be834-7900383c})

[comment]: # ({new-92c7a8e5})
#### 概述
本节提供使用 Zabbix 官方源码包从 Zabbix **5.4**.x 成功[升级](/manual/installation/upgrade)到 Zabbix **6.0**.x 所需的步骤。

虽然升级 agents 不是强制性的（但推荐），但 Server 和 Proxy 必须具有相同的[主版本](/manual/appendix/compatibility)。因此，在 Server-Proxy 这样的结构中，Zabbix Server 和所有 Proxy 都必须停止服务并进行升级。在升级期间保持 Proxy 运行没有任何用处，旧数据将被丢弃，在 Proxy 的配置信息与 Server 同步之前不会收集新数据。

::: noteimportant
不可能再让未升级的低版本 Proxy 向高版本的 Server 端发送数据。Zabbix 从未推荐或支持这种方法，现在已正式禁用，因而 Server 将忽略来自未升级 Proxy 的数据。
:::

由于不支持 SQLite 数据库升级，SQLite 数据库文件必须手动删除，因此升级前 proxy 的历史数据将丢失。proxy 第一次启动时会缺少 SQLite 数据库文件，但 proxy 会自动创建它。

根据数据库大小，数据库升级到 6.0 版本可能需要很长时间。

::: notewarning
在升级前请务必阅读相关**升级说明!**
:::

官方提供以下升级说明：

|升级前版本|阅读完整的升级说明|版本之间最主要变更|
|------------|-----------------------|---------------------------------------|
|5.4.x|适用于：<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)| |
|5.2.x|适用于：<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|要求的最低数据库版本提高；<br>聚合监控项开始作为一个单独的监控项类型。|
|5.0.x|适用于：<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|要求的最低 PHP 版本从 7.2.0 提高到 7.2.5。|
|4.4.x|适用于：<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)|不再支持 IBM 的 DB2 数据库；<br>要求的最低 PHP 版本从 5.4.0 提高到 7.2.0；<br>要求的最低数据库版本提高；<br>更改了  Zabbix PHP 的文件目录。|
|4.2.x|适用于：<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.4/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|删除了 Jabber、Ez 短信介质类型。|
|4.0.x LTS|适用于：<br>Zabbix [4.2](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_420)<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.4/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|低版本 proxy 不再可以向升级后的高版本 server 发送数据；<br>高版本的 agent 将不再适配低版本的 server。|
|3.4.x|适用于：<br>Zabbix [4.0](https://www.zabbix.com/documentation/4.0/manual/installation/upgrade_notes_400)<br>Zabbix [4.2](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_420)<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.4/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|“libpthread”和“zlib”库现在是强制性的；<br>对纯文本协议的支持被丢弃，并且头部是强制性的；<br>不再支持 1.4 之前版本的 Zabbix agent；<br>被动 proxy 配置文件中的 Server 参数现在是必需的。|
|3.2.x|适用于：<br>Zabbix [3.4](https://www.zabbix.com/documentation/3.4/manual/installation/upgrade_notes_340)<br>Zabbix [4.0](https://www.zabbix.com/documentation/4.0/manual/installation/upgrade_notes_400)<br>Zabbix [4.2](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_420)<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.4/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)| Zabbix server/前端 开始支持 SQLite 数据库作为后端存储库；<br>Perl 兼容正则表达式 (PCRE) 代替 POSIX 扩展；<br>Zabbix Server 必须使用“libpcre”和“libevent”库；<br>为用户参数、远程命令、不带有 “nowait” 标志的 system.run\[\] 监控项和 Zabbix Server 执行脚本添加了退出代码检查功能；<br>Zabbix Java 网关必须升级以支持新功能。|
|3.0.x LTS|适用于：<br>Zabbix [3.2](https://www.zabbix.com/documentation/3.2/manual/installation/upgrade_notes_320)<br>Zabbix [3.4](https://www.zabbix.com/documentation/3.4/manual/installation/upgrade_notes_340)<br>Zabbix [4.0](https://www.zabbix.com/documentation/4.0/manual/installation/upgrade_notes_400)<br>Zabbix [4.2](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_420)<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.4/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|数据库升级可能会很慢，主要取决于历史表的大小。|
|2.4.x|F适用于：<br>Zabbix [3.0](https://www.zabbix.com/documentation/3.0/manual/installation/upgrade_notes_300)<br>Zabbix [3.2](https://www.zabbix.com/documentation/3.2/manual/installation/upgrade_notes_320)<br>Zabbix [3.4](https://www.zabbix.com/documentation/3.4/manual/installation/upgrade_notes_340)<br>Zabbix [4.0](https://www.zabbix.com/documentation/4.0/manual/installation/upgrade_notes_400)<br>Zabbix [4.2](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_420)<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|要求的最低 PHP 版本从 5.3.0 提高到 5.4.0；<br>必须指定agent配置文件中的 LogFile 参数。|
|2.2.x LTS|适用于：<br>Zabbix [2.4](https://www.zabbix.com/documentation/2.4/manual/installation/upgrade_notes_240)<br>Zabbix [3.0](https://www.zabbix.com/documentation/3.0/manual/installation/upgrade_notes_300)<br>Zabbix [3.2](https://www.zabbix.com/documentation/3.2/manual/installation/upgrade_notes_320)<br>Zabbix [3.4](https://www.zabbix.com/documentation/3.4/manual/installation/upgrade_notes_340)<br>Zabbix [4.0](https://www.zabbix.com/documentation/4.0/manual/installation/upgrade_notes_400)<br>Zabbix [4.2](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_420)<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|移除了基于节点的分布式监控。|
|2.0.x|适用于：<br>Zabbix [2.2](https://www.zabbix.com/documentation/2.2/manual/installation/upgrade_notes_220)<br>Zabbix [2.4](https://www.zabbix.com/documentation/2.4/manual/installation/upgrade_notes_240)<br>Zabbix [3.0](https://www.zabbix.com/documentation/3.0/manual/installation/upgrade_notes_300)<br>Zabbix [3.2](https://www.zabbix.com/documentation/3.2/manual/installation/upgrade_notes_320)<br>Zabbix [3.4](https://www.zabbix.com/documentation/3.4/manual/installation/upgrade_notes_340)<br>Zabbix [4.0](https://www.zabbix.com/documentation/4.0/manual/installation/upgrade_notes_400)<br>Zabbix [4.2](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_420)<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|要求的最低 PHP 版本从 5.1.6 提高到 5.3.0；<br>服务端正常运行需要区分大小写的 MySQL 数据库；Zabbix Server 端需要使用 utf8 和 utf8\_ 字符集才能与 MySQL 数据库适配。点击 [数据库创建脚本](/manual/appendix/install/db_scripts#mysql) 获取创建数据库和表结构的流程。<br>PHP 扩展需要'mysqli' 而不是以前的 'mysql'。|

查看关于 6.0 的其他 [配置要求](/manual/installation/requirements)。

::: notetip
在升级期间同时运行两个并行的 SSH 会话可能会比较方便，其中一个执行升级步骤，另一个查看实时的 Server/Proxy 的日志。例如，在第二个 SSH 会话中运行 `tail -f zabbix_server.log` 或 `tail -f zabbix_proxy.log` 实时显示最新返回的日志文件记录和可能的错误。这对于生产环境可能很关键。
:::

[comment]: # ({/new-92c7a8e5})

[comment]: # ({f14f8b32-101d6faf})
#### Server 升级步骤

[comment]: # ({/f14f8b32-101d6faf})

[comment]: # ({8dea56cb-b8308740})
##### 1 停止 Server 服务

停止 Zabbix server 服务并确认不再有新的数据写入数据库。

[comment]: # ({/8dea56cb-b8308740})

[comment]: # ({8091ec8a-ab13a6a4})
##### 2 备份当前的 Zabbix 数据库

这是非常重要的一步。确保您有数据库的备份，如果升级过程失败（如磁盘空间不足、断电等任何意外问题），它将有所帮助。

[comment]: # ({/8091ec8a-ab13a6a4})

[comment]: # ({7152413c-d2778675})
##### 3 备份配置文件、PHP 文件和 Zabbix 二进制文件

备份 Zabbix 二进制文件、配置文件和 PHP 文件目录。

[comment]: # ({/7152413c-d2778675})

[comment]: # ({72333c47-2bb75ddd})
##### 4 安装新的server二进制文件

查看从源代码编译 Zabbix Server 的 [说明](/manual/installation/install#installing_zabbix_daemons)  。

[comment]: # ({/72333c47-2bb75ddd})

[comment]: # ({new-ec6edae7})
##### 5 查看 server 配置参数

有关  [强制更改](/manual/installation/upgrade_notes_600#configuration_parameters) 的详细信息，请参阅升级说明。

有关新增的可选参数，请参阅  [6.0新变化](/manual/introduction/whatsnew600#configuration_parameters) 。

[comment]: # ({/new-ec6edae7})

[comment]: # ({dff71120-a01e967f})
##### 6 启动新的 Zabbix 二进制文件

启动新的二进制文件。检查日志以确认二进制文件是否成功启动。

Zabbix server 会自动升级数据库。服务启动时，Zabbix server 会报告当前的（包括强制和可选）和所需要的数据库版本。

 · 8673:20161117:104750.259 current database version (mandatory/optional): 03040000/03040000
 · 8673:20161117:104750.259 required mandatory version: 03040000

如果当前强制版本比要求的版本低，Zabbix Server 会自动执行所要求的数据库版本升级补丁。数据库升级进度（百分比）被写入 Zabbix server 的日志。当日志出现 “database upgrade fully completed” 表示数据库升级成功。如果有任何补丁升级失败，Zabbix server 将不会启动。如果当前强制数据库版本比要求的更新，Zabbix server 也不会启动。仅当当前强制数据库版本对应于所需数据库版本时，Zabbix server 才会启动。

在启动 server 服务之前：

- · 确保数据库用户有足够的权限（创建表、删除表、创建索引、删除索引）；
- · 确保您有足够的可用磁盘空间。

[comment]: # ({/dff71120-a01e967f})

[comment]: # ({e0d81eca-22e0e4e9})
##### 7 安装新的 Zabbix web 界面

要求的最低 PHP 版本是 7.2.5。可参阅 [安装说明](/manual/installation/frontend)。

[comment]: # ({/e0d81eca-22e0e4e9})

[comment]: # ({d03c52a3-49e4f43e})
##### 8 清除浏览器 cookie 和缓存

升级后，您可能需要清除 Web 浏览器的 cookie 和缓存信息，以使 Zabbix Web 界面正常工作。

[comment]: # ({/d03c52a3-49e4f43e})

[comment]: # ({4b513045-75f5b3ed})
#### Proxy 升级步骤

[comment]: # ({/4b513045-75f5b3ed})

[comment]: # ({12bb6098-f33e6ddb})
##### 1 停止 proxy 服务

停止 proxy 服务

[comment]: # ({/12bb6098-f33e6ddb})

[comment]: # ({47214aea-2e03c550})
##### 2 备份配置文件和 Zabbix Proxy 二进制文件

备份配置文件和 Zabbix Proxy 二进制文件。

[comment]: # ({/47214aea-2e03c550})

[comment]: # ({df8ccc10-024201dc})
##### 3 安装新的 proxy 二进制文件

参考源码编译安装 Zabbix proxy 的 [说明](/manual/installation/install#installing_zabbix_daemons) 。

[comment]: # ({/df8ccc10-024201dc})

[comment]: # ({fd490dff-2fbda571})
##### 4 查看 proxy 配置参数

此版本没有对 proxy 的 [参数](/manual/appendix/config/zabbix_proxy) 做硬性变更。

[comment]: # ({/fd490dff-2fbda571})

[comment]: # ({0954ad17-520f43c6})
##### 5 启动新的 Zabbix proxy

启动新的 Zabbix proxy 服务。检查日志以确认是否升级成功。

Zabbix proxy 会自动升级数据库。数据库升级与启动 [Zabbix server](/manual/installation/upgrade#start_new_zabbix_binaries) 服务类似。

[comment]: # ({/0954ad17-520f43c6})

[comment]: # ({fe773e82-59736bba})
#### Agent 升级步骤

::: noteimportant
升级 agent 不是强制性的。只有在需要使用新功能时才需要升级 agent。
:::

本节的升级过程适用于升级 Zabbix agent 和 Zabbix agent2。

[comment]: # ({/fe773e82-59736bba})

[comment]: # ({f41c11dd-02698e69})
##### 1 停止 agent 服务

停掉 Zabbix agent 服务。

[comment]: # ({/f41c11dd-02698e69})

[comment]: # ({e2e8da4e-50263823})
##### 2 备份配置文件和 Zabbix agent 二进制文件

备份配置文件和 Zabbix agent 二进制文件。

[comment]: # ({/e2e8da4e-50263823})

[comment]: # ({6b746f30-467f56b3})
##### 3 安装新的 agent 二进制文件

参考源码编译安装 Zabbix agent 的 [说明](/manual/installation/install#installing_zabbix_daemons)。

[comment]: # ({/6b746f30-467f56b3})

[comment]: # ({cf4855ad-34721aad})
##### 4 查看 agent 配置参数

在此版本中，[agent](/manual/appendix/config/zabbix_agentd)  和 [agent2](/manual/appendix/config/zabbix_agent2)  的参数都没有强制更改部分。

[comment]: # ({/cf4855ad-34721aad})

[comment]: # ({624e07be-04c253a7})
##### 5 启动新的 Zabbix agent

启动新的 Zabbix agent 服务。检查日志以确认启动成功。

[comment]: # ({/624e07be-04c253a7})

[comment]: # ({new-a06019d2})
#### 次版本之间的升级

升级 6.0.x 的次版本（例如从 6.0.1 到 6.0.3）时，需要对 server/proxy/agent 执行与主版本升级步骤相同的操作。唯一的区别是，在次版本之间进行升级时，不会对数据库进行任何更改。

[comment]: # ({/new-a06019d2})
