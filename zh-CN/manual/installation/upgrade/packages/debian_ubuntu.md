[comment]: # translation:outdated

[comment]: # ({new-91aab3e5})
# 2 Debian/Ubuntu

[comment]: # ({/new-91aab3e5})

[comment]: # ({new-d02acea9})
#### 概述

本章节提供了在 Debian/Ubuntu 系统上使用 Zabbix 二进制包，从 Zabbix **5.4**.x 成功 [upgrade](/manual/installation/upgrade) 到 Zabbix **6.0**.x 所需的步骤。

虽然升级 Zabbix 代理不是强制性的（但建议将其升级），但 但 Zabbix server和proxy必须具有[相同主版本]。因此，在Zabbix server 和 Zabbix proxy 的架构升级时，Zabbix server 和所有的 Zabbix proxy必须停止服务并进行升级。在Zabbix server 升级期间，保持Zabbix proxy 运行正常并不好，因为在Zabbix proxy升级期间，它们的旧数据将被丢弃，并且在Zabbix proxy配置与Zabbix server同步前并不会收集新数据。

请注意，若Proxy的数据库为SQLite类型，其保留的历史数据会在版本升级完成后全部被删除，因为不支持 SQLite 数据库升级并且必须手动删除 SQLite 数据库文件。当第一次启动Zabbix proxy并且缺少 SQLite 数据库文件时，Zabbix proxy会自动创建它。

在数据库升级到版本 6.0 时，根据数据库存储大小，可能需花很长时间升级完毕。

::: 注意事项
升级前请务必阅读相关 **upgrade notes!**
:::

如下提供升级说明：

|升级前版本|阅读完整的升级说明|版本之间最重要的变化|
|------------|-----------|------------ --------------------------|
|5.4.x|适用于：<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|<|
|5.2.x|适用于：<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600) |提高了最低要求的数据库版本；<br>将聚合项作为单独的类型删除。|
|5.0.x|适用于：<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|所需的最低 PHP 版本从 7.2.0 提高到 7.2.5。|
|4.4.x|适用于：<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)|放弃对 IBM DB2 的支持；<br >最低要求的 PHP 版本从 5.4.0 提高到 7.2.0；<br>最低要求的数据库版本提高；<br>更改了 Zabbix PHP 文件目录。|
|4.2.x|适用于：<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.4/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|已删除 Jabber、Ez 短信媒体类型。|
|4.0.x LTS|适用于：<br>Zabbix [4.2](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_420)<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.4/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500)<br>Zabbix [5.2]( https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br >Zabbix [6.0](/manual/installation/upgrade_notes_600)|老的Zabbix agent不再能够向升级后的Zabbix server 报告数据；<br>新的Zabbix agent不再能够与老的 Zabbix server 一起工作。|
|3.4.x|适用于：<br>Zabbix [4.0](https://www.zabbix.com/documentation/4.0/manual/installation/upgrade_notes_400)<br>Zabbix [4.2](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_420)<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.4/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br> Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|'libpthread' 和 'zlib' 库现在强制；<br>删除对纯文本协议的支持，并且强制标头；<br>不再支持 1.4 之前版本的 Zabbix agent；<br>被动模式Zabbix agent配置中的 Server 参数现在是强制的。|
|3.2.x|适用于：<br>Zabbix [3.4](https://www.zabbix.com/documentation/3.4/manual/installation/upgrade_notes_340)<br>Zabbix [4.0](https://www.zabbix.com/documentation/4.0/manual/installation/upgrade_notes_400)<br>Zabbix [4.2](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_420)<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.4/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500)<br> Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|为 Zabbix server和前端删除了作为后端数据库的 SQLite 支持；<br>支持 Perl 兼容正则表达式 (PCRE)，而不是扩展 POSIX；<br> Zabbix server强制使用“libpcre”和“libevent”库；<br>为用户参数添加了退出代码检查仪表、远程命令和不带 'nowait' 标志的 system.run\[\] 项目以及 Zabbix server执行的脚本；<br>必须升级 Zabbix Java 网关以支持新功能。|
|3.0.x LTS|适用于：<br>Zabbix [3.2](https://www.zabbix.com/documentation/3.2/manual/installation/upgrade_notes_320)<br>Zabbix [3.4](https://www.zabbix.com/documentation/3.4/manual/installation/upgrade_notes_340)<br>Zabbix [4.0](https://www.zabbix.com/documentation/4.0/manual/installation/upgrade_notes_400)<br>Zabbix [4.2]( https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_420)<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.4/manual/installation/upgrade_notes_440)<br >Zabbix [5.0](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/安装/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|数据库升级可能会很慢，具体取决于历史表的大小。|
|2.4.x|适用于：<br>Zabbix [3.0](https://www.zabbix.com/documentation/3.0/manual/installation/upgrade_notes_300)<br>Zabbix [3.2](https://www.zabbix.com/documentation/3.2/manual/installation/upgrade_notes_320)<br>Zabbix [3.4](https://www.zabbix.com/documentation/3.4/manual/installation/upgrade_notes_340)<br>Zabbix [4.0](https://www.zabbix.com/documentation/4.0/manual/installation/upgrade_notes_400)<br>Zabbix [4.2](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_420)<br> Zabbix [4.4](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|最低要求的 PHP 版本从 5.3.0 提高到 5.4.0<br>LogFile ag ent 参数必须指定|
|2.2.x LTS|适用于：<br>Zabbix [2.4](https://www.zabbix.com/documentation/2.4/manual/installation/upgrade_notes_240)<br>Zabbix [3.0](https://www.zabbix.com/documentation/3.0/manual/installation/upgrade_notes_300)<br>Zabbix [3.2](https://www.zabbix.com/documentation/3.2/manual/installation/upgrade_notes_320)<br>Zabbix [3.4]( https://www.zabbix.com/documentation/3.4/manual/installation/upgrade_notes_340)<br>Zabbix [4.0](https://www.zabbix.com/documentation/4.0/manual/installation/upgrade_notes_400)<br >Zabbix [4.2](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_420)<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.2/manual/安装/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/文档/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/安装/upgrade_notes_600)|移除了基于节点的分布式监控|
|2.0.x|适用于：<br>Zabbix [2.2](https://www.zabbix.com/documentation/2.2/manual/installation/upgrade_notes_220)<br>Zabbix [2.4](https://www.zabbix.com/documentation/2.4/manual/installation/upgrade_notes_240)<br>Zabbix [3.0](https://www.zabbix.com/documentation/3.0/manual/installation/upgrade_notes_300)<br>Zabbix [3.2](https://www.zabbix.com/documentation/3.2/manual/installation/upgrade_notes_320)<br>Zabbix [3.4](https://www.zabbix.com/documentation/3.4/manual/installation/upgrade_notes_340)<br> Zabbix [4.0](https://www.zabbix.com/documentation/4.0/manual/installation/upgrade_notes_400)<br>Zabbix [4.2](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_420)<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_440)<br>Zabbix [5.

[comment]: # ({/new-d02acea9})

[comment]: # ({new-93ff8b03})
#### 升级步骤

[comment]: # ({/new-93ff8b03})

[comment]: # ({6465ebb1-f8102233})
##### 1 停止 Zabbix 进程

停止 Zabbix server服务来确保没有新数据写入数据库。

    # service zabbix-server stop

若需升级 Zabbix proxy，同理，需先停止 Zabbix proxy 进程。

    # service zabbix-proxy stop

[comment]: # ({/6465ebb1-f8102233})

[comment]: # ({36a12c32-ab13a6a4})
##### 2 备份当前的数据库

升级前请确保备份了数据库，是非常关键的一步。如果升级失败（因磁盘空间不足、断电或其他意外导致的升级失败），备份的数据库将大有帮助。


[comment]: # ({/36a12c32-ab13a6a4})

[comment]: # ({bcff5bb2-6c141723})
##### 3 备份配置文件、PHP 文件和 Zabbix 二进制文件

在升级前请确保备份了Zabbix 二进制文件、配置文件和 PHP 文件。

配置文件：

     # mkdir /opt/zabbix-backup/
     # cp /etc/zabbix/zabbix_server.conf /opt/zabbix-backup/
     # cp /etc/apache2/conf-enabled/zabbix.conf /opt/zabbix-backup/

PHP 文件和 Zabbix 二进制文件：

     # cp -R /usr/share/zabbix/ /opt/zabbix-backup/
     # cp -R /usr/share/doc/zabbix-* /opt/zabbix-backup/

[comment]: # ({/bcff5bb2-6c141723})

[comment]: # ({new-31c75b55})
##### 4 升级 Zabbix 软件仓库配置包

在升级之前，必须卸载当前的软件仓库包：

    # rm -Rf /etc/apt/sources.list.d/zabbix.list

然后再安装新的软件仓库包：

在 **Debian 10** 上运行：

    # wget https://repo.zabbix.com/zabbix/6.0/debian/pool/main/z/zabbix-release/zabbix-release_6.0-1+debian10_all.deb
    # dpkg -i zabbix-release_5.4-1+debian10_all.deb

在 **Debian 9** 上运行：

    # wget https://repo.zabbix.com/zabbix/6.0/debian/pool/main/z/zabbix-release/zabbix-release_6.0-1+debian9_all.deb
    # dpkg -i zabbix-release_5.4-1+debian9_all.deb

在 **Debian 8** 上运行：

    # wget https://repo.zabbix.com/zabbix/6.0/debian/pool/main/z/zabbix-release/zabbix-release_6.0-1+debian8_all.deb
    # dpkg -i zabbix-release_5.4-1+debian8_all.deb

在 **Ubuntu 20.04** 上运行：

    # wget https://repo.zabbix.com/zabbix/5.4/ubuntu/pool/main/z/zabbix-release/zabbix-release_6.0-1+ubuntu20.04_all.deb
    # dpkg -i zabbix-release_5.4-1+ubuntu20.04_all.deb

在 **Ubuntu 18.04** 上运行：

    # wget https://repo.zabbix.com/zabbix/5.4/ubuntu/pool/main/z/zabbix-release/zabbix-release_6.0-1+ubuntu18.04_all.deb
    # dpkg -i zabbix-release_6.0-1+ubuntu18.04_all.deb

在 **Ubuntu 16.04** 上运行：

    # wget https://repo.zabbix.com/zabbix/5.4/ubuntu/pool/main/z/zabbix-release/zabbix-release_6.0-1+ubuntu16.04_all.deb
    # dpkg -i zabbix-release_5.4-1+ubuntu16.04_all.deb

在 **Ubuntu 14.04** 上运行：

    # wget https://repo.zabbix.com/zabbix/5.4/ubuntu/pool/main/z/zabbix-release/zabbix-release_6.0-1+ubuntu14.04_all.deb
    # dpkg -i zabbix-release_6.0-1+ubuntu14.04_all.deb

更新软件仓库信息。

    # apt-get update

[comment]: # ({/new-31c75b55})

[comment]: # ({5ca72b55-08c7383c})
##### 5 升级Zabbix组件

升级 Zabbix 组件，可以运行以下命令：

      # apt-get install --only-upgrade zabbix-server-mysql zabbix-frontend-php zabbix-agent

若使用 PostgreSQL数据库，请在命令中将 `mysql` 替换为 `pgsql`。 若升级proxy，请在命令中将 `server` 替换为 `proxy`。 若升级 Zabbix agent 2，在命令中将 `zabbix-agent` 替换为 `zabbix-agent2`。

与此同时，要使得Apache 能正常升级 Web 前端，还需运行如下命令：

        # apt-get install zabbix-apache-conf

发行版**prior to Debian 10 (buster) / Ubuntu 18.04 (bionic) /
Raspbian 10 (buster)** 不提供 PHP 7.2 或更高版本，而其对Zabbix 前端 5.0又是必要的。 有关安装 Zabbix 前端旧发行版的信息，请查阅[information](/manual/installation/frontend/frontend_on_rhel7)。

[comment]: # ({/5ca72b55-08c7383c})

[comment]: # ({new-455f4e97})
##### 6 检查 Zabbix 组件配置文件的参数

在新版本中，Zabbix组件的配置文件发生了一些变化，详见升级说明 [mandatory
changes](/manual/installation/upgrade_notes_600#configuration_parameters)。

关于新的选项参数，详见此章节 [What's new](/manual/introduction/whatsnew600#configuration_parameters) 。

[comment]: # ({/new-455f4e97})

[comment]: # ({a393f257-624a8fc7})
##### 7 启动 Zabbix 进程

启动升级后的 Zabbix 组件。

    # service zabbix-server start
    # service zabbix-proxy start
    # service zabbix-agent start
    # service zabbix-agent2 start

[comment]: # ({/a393f257-624a8fc7})

[comment]: # ({760b25a7-49e4f43e})
##### 8 清除浏览器的 Cookies 和缓存

待升级完毕后，可能需要清除浏览器的 Cookies 和缓存，以便 Zabbix 的 Web
界面能正常工作。

[comment]: # ({/760b25a7-49e4f43e})

[comment]: # ({new-9bab02f2})
 #### Zabbix 次要版本之间的升级
如果要升级 Zabbix 6.0.x 的次要版本（例如：从 6.0.1 升级到 6.0.3），是非常容易的。

在升级 Zabbix 所有组件的次要版本时，只需运行以下命令：

     $ sudo apt install --only-upgrade 'zabbix.*'

在升级 Zabbix server 的次要版本时，只需运行以下命令：

     $ sudo apt install --only-upgrade 'zabbix-server.*'

在升级 Zabbix agent 的次要版本时，只需运行以下命令：

     $ sudo apt install --only-upgrade 'zabbix-agent.*'

在升级 Zabbix agent 2的次要版本时，只需运行以下命令：

     $ sudo apt install --only-upgrade 'zabbix-agent2.*'

[comment]: # ({/new-9bab02f2})
