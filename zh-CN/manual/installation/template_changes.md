[comment]: # translation:outdated

[comment]: # ({new-0a5cc230})
# 8 模板变更

此页面列出了 Zabbix 内置模板的所有变更。
根据这些变更，建议对现有模板进行，可以通过导入最新版本或手动执行更改来完成。

This page lists all changes to the stock templates that are shipped with
Zabbix. It is suggested to modify these templates in existing
installations - depending on the changes, it can be done either by
importing the latest version or by performing the change manually.

[comment]: # ({/new-0a5cc230})

[comment]: # ({new-94950308})
### CHANGES IN 7.0.0

[comment]: # ({/new-94950308})

[comment]: # ({new-651539db})
#### Updated templates

**Zabbix server/proxy health**

Templates for Zabbix server/proxy health have been updated according to the [changes](/manual/introduction/whatsnew700#concurrency-in-network-discovery) 
in network discovery. The items for monitoring the discoverer process (now removed) has been replaced by items to measure the 
discovery manager and discovery worker process utilization. A new internal item has been added for monitoring the discovery queue. 

[comment]: # ({/new-651539db})







