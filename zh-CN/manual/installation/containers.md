[comment]: # translation:outdated

[comment]: # ({ff1b4e5c-e62e1497})
# 5 从容器中安装

[comment]: # ({/ff1b4e5c-e62e1497})

[comment]: # ({new-a3ac452e})

### Overview

This section describes how to deploy Zabbix with [Docker](#docker) or [Docker Compose](#docker-compose).

Zabbix officially provides:

- Separate Docker images for each Zabbix
component to run as portable and self-sufficient containers.
- Compose files for defining and running
multi-container Zabbix components in Docker.

[comment]: # ({/new-a3ac452e})

[comment]: # ({75dfdb7e-f9acea87})
### Docker

Zabbix 为每个 Zabbix 组件提供 [Docker](https://www.docker.com) image 作为可移植和自给自足的容器，以加快部署和更新过程。

Zabbix 组件支持 MySQL 和 PostgreSQL 数据库、Apache2 和 Nginx Web 服务器。这些 image 被分成多个不同的 image。

[comment]: # ({/75dfdb7e-f9acea87})

[comment]: # ({new-e498c2aa})

#### Tags

Official Zabbix component images may contain the following tags:

|Tag|Description|Example|
|--|----------|----|
|latest            | The latest stable version of a Zabbix component based on Alpine Linux image. |zabbix-agent:latest |
|<OS>-trunk | The latest nightly build of the Zabbix version that is currently being developed on a specific operating system. <br> <br> **<OS>** - the base operating system. Supported values: <br> *alpine* - Alpine Linux; <br> *ltsc2019* -  Windows 10 LTSC 2019 (agent only); <br> *ol* - Oracle Linux; <br> *ltsc2022* - Windows 11 LTSC 2022 (agent only); <br> *ubuntu* - Ubuntu  |zabbix agent:ubuntu-trunk |
|<OS>-latest     | The latest stable version of a Zabbix component on a specific operating system. <br> <br> **<OS>** - the base operating system. Supported values:  <br> *alpine* - Alpine Linux; <br> *ltsc2019* -  Windows 10 LTSC 2019 (agent only); <br> *ol* - Oracle Linux; <br> *ltsc2022* - Windows 11 LTSC 2022 (agent only); <br> *ubuntu* - Ubuntu|zabbix-agent:ol-latest |
|<OS>-X.X-latest |The latest minor version of a Zabbix component of a specific major version and operating system. <br> <br> **<OS>** - the base operating system. Supported values:  <br> *alpine* - Alpine Linux; <br> *centos* - CentOS (only for Zabbix 4.0) <br> *ltsc2019* -  Windows 10 LTSC 2019 (agent only); <br> *ol* - Oracle Linux; <br> *ltsc2022* - Windows 11 LTSC 2022 (agent only); <br> *ubuntu* - Ubuntu<br><br>**X.X** - the major Zabbix version (supported: *4.0*, *5.0*, *6.0*, *6.2*). |zabbix-agent:alpine-6.0-latest |
|<OS>-X.X.*      |The latest minor version of a Zabbix component of a specific major version and operating system. <br> <br> **<OS>** - the base operating system. Supported values:  <br> *alpine* - Alpine Linux; <br> *centos* - CentOS (only for Zabbix 4.0) <br> *ltsc2019* -  Windows 10 LTSC 2019 (agent only); <br> *ol* - Oracle Linux; <br> *ltsc2022* - Windows 11 LTSC 2022 (agent only); <br> *ubuntu* - Ubuntu<br><br>**X.X** - the major Zabbix version (supported: *4.0*, *5.0*, *6.0*, *6.2*). <br><br> **\*** - the Zabbix minor version  |zabbix-agent:alpine-6.4.1|

[comment]: # ({/new-e498c2aa})


[comment]: # ({4bd5c5b5-fba82ded})
#### Docker file 源

任何人都可以使用位于 [github.com](https://github.com/) 上的 [官方仓库](https://github.com/zabbix/zabbix-docker) 来追踪 Docker file 的变化。
您可以 fork 项目，或根据官方 Docker file 制作自己的 image。

[comment]: # ({/4bd5c5b5-fba82ded})



[comment]: # ({4798f110-0b4b68c7})
#### 使用方法


[comment]: # ({/4798f110-0b4b68c7})

[comment]: # ({dad63d80-de9f41d4})
##### 环境变量

所有 Zabbix 组件 image 都提供环境变量来控制配置。 这些环境变量在每个组件 image 仓库中列出。这些环境变量是 Zabbix 配置文件中的选项，但具有不同的命名方法。 例如，ZBX_LOGSLOWQUERIES 等于来自 Zabbix server 和 Zabbix proxy 配置文件的 LogSlowQueries。

::: noteimportant
一些配置选项是不允许更改的。例如，`PIDFile`和 `LogType`。
:::

其中，一些组件有特定的环境变量，而这些环境变量在官方 Zabbix 配置文件并不存在：

|   |   |   |
|---|---|---|
|**变量 **|**组件**|**描述**|
|`DB_SERVER_HOST`|Server<br>Proxy<br>Web 界面|这个变量指的是 MySQL 或 PostgreSQL 的 IP 或 DNS。<br>默认情况下，这个值根据 MySQL 和 PostgreSQL，分别为mysql-server 或 postgres-server|
|`DB_SERVER_PORT`|Server<br>Proxy<br>Web 界面|这个变量指的是 MySQL 或 PostgreSQL 的端口。<br>默认情况下，这个值根据 MySQL 和 PostgreSQL，分别为 '3306' 或 '5432' 。|
|`MYSQL_USER`|Server<br>Proxy<br>Web 界面|MySQL 数据库用户。<br>默认情况下，这个值为 'zabbix'。|
|`MYSQL_PASSWORD`|Server<br>Proxy<br>Web 界面|MySQL 数据库密码。<br>默认情况下，这个值为 'zabbix'。|
|`MYSQL_DATABASE`|Server<br>Proxy<br>Web 界面|Zabbix 数据库库名。<br>默认情况下，这个值根据 Zabbix server 和 Zabbix proxy，分别为 'zabbix' 和 'zabbix_proxy' 。|
|`POSTGRES_USER`|Server<br>Web 界面|PostgreSQL 数据库用户。<br>默认情况下，这个值为 'zabbix'。|
|`POSTGRES_PASSWORD`|Server<br>Web 界面|PostgreSQL 数据库密码。<br>默认情况下，这个值为 'zabbix'。|
|`POSTGRES_DB`|Server<br>Web 界面|Zabbix 数据库库名。<br>默认情况下，这个值根据 Zabbix server 和 Zabbix proxy，分别为 'zabbix' 和 'zabbix_proxy' 。|
|`PHP_TZ`|Web 界面|PHP 时区格式。支持时区的完整列表参照 [php.net](http://php.net/manual/en/timezones.php)。<br>默认情况下，这个值为 'Europe/Riga' 。|
|`ZBX_SERVER_NAME`|Web 界面|Web 界面右上角显示的安装名称。<br>默认情况下，这个值为 'Zabbix Docker' 。|
|`ZBX_JAVAGATEWAY_ENABLE`|Server<br>Proxy|是否启用 Zabbix Java 网关 以采集与 Java 相关的检查数据。<br>默认情况下，这个值为 "false" 。|
|`ZBX_ENABLE_SNMP_TRAPS`|Server<br>Proxy|是否启用 SNMP trap 功能。 这需要存在 zabbix-snmptraps 容器实例，并共享 /var/lib/zabbix/snmptraps  volume 到 Zabbix server 或 proxy。|

[comment]: # ({/dad63d80-de9f41d4})

[comment]: # ({d0c9c28c-7d10f7dc})
##### Volumes

Image 中允许使用一些挂载点。根据 Zabbix 组件类型，这些挂载点各不相同：

|   |   |
|---|---|
|**Volume**|**描述**|
|**Zabbix agent**|<|
|*/etc/zabbix/zabbix\_agentd.d*| 这个 volume 允许包含 *\*.conf* 文件并使用 `UserParameter` 扩展 Zabbix agent。|
|*/var/lib/zabbix/modules*| 这个 volume 允许加载其它 module 并使用 [LoadModule](/manual/config/items/loadablemodules) 功能扩展 Zabbix agent。|
|*/var/lib/zabbix/enc*|这个 volume 用于存放 TLS 相关的文件。这些文件名使用 `ZBX_TLSCAFILE`, `ZBX_TLSCRLFILE`, `ZBX_TLSKEY_FILE` ，`ZBX_TLSPSKFILE` 等环境变量指定。|
|**Zabbix server**|<|
|*/usr/lib/zabbix/alertscripts*|这个 volume 用于自定义告警脚本。即 [zabbix\_server.conf](/manual/appendix/config/zabbix_server) 中的 `AlertScriptsPath` 参数。 |
|*/usr/lib/zabbix/externalscripts*| 这个 volume 用于 [外部检查](/manual/config/items/itemtypes/external)。即 [zabbix\_server.conf](/manual/appendix/config/zabbix_server) 中的 `ExternalScripts` 参数。|
|*/var/lib/zabbix/modules*|这个 volume 允许通过 [LoadModule](/manual/config/items/loadablemodules) 功能加载额外的模块以扩展 Zabbix agent。|
|*/var/lib/zabbix/enc*|这个 volume 用于存放 TLS 相关的文件。这些文件名使用 `ZBX_TLSCAFILE`, `ZBX_TLSCRLFILE`, `ZBX_TLSKEY_FILE` ，`ZBX_TLSPSKFILE` 等环境变量指定。|
|*/var/lib/zabbix/ssl/certs*|这个 volume 用于存放客户端认证的 SSL 客户端认证文件。即 [zabbix\_server.conf] 中的 `SSLCertLocation` 参数。|
|*/var/lib/zabbix/ssl/keys*|这个 volume 用于存放客户端认证的 SSL 私钥文件。即 [zabbix\_server.conf](/manual/appendix/config/zabbix_server) 中的 `SSLKeyLocation` 参数。|
|*/var/lib/zabbix/ssl/ssl\_ca*|这个 volume 用于存放 SSL 服务器证书认证的证书颁发机构(CA)文件。即 [zabbix\_server.conf](/manual/appendix/config/zabbix_server) 中的 `SSLCALocation` 参数。|
|*/var/lib/zabbix/snmptraps*|这个 volume 用于存放 snmptraps.log 文件。它可由 zabbix-snmptraps 容器共享，并在创建 Zabbix server 新实例时使用 Docker 的 `--volumes-from` 选项继承。可以通过共享 volume ，并将 ZBX_ENABLE_SNMP_TRAPS 环境变量切换为 'true' 以启用 SNMP trap 处理功能。|
|*/var/lib/zabbix/mibs*| 这个 volume 允许添加新的 MIB 文件。它不支持子目录，所有的 MIB 文件必须位于 `/var/lib/zabbix/mibs` 下。|
|**Zabbix proxy**|<|
|*/usr/lib/zabbix/externalscripts*|这个 volume 用于 [外部检查](/manual/config/items/itemtypes/external)。即 [zabbix\_proxy.conf](/manual/appendix/config/zabbix_proxy) 中的 `ExternalScripts` 参数。|
|*/var/lib/zabbix/modules*|The volume allows to load additional modules and extend Zabbix server using the [LoadModule](/manual/config/items/loadablemodules) feature|
|*/var/lib/zabbix/enc*|这个 volume 用于存放 TLS 相关的文件。这些文件名使用 `ZBX_TLSCAFILE`, `ZBX_TLSCRLFILE`, `ZBX_TLSKEY_FILE` ，`ZBX_TLSPSKFILE` 等环境变量指定。|
|*/var/lib/zabbix/ssl/certs*|这个 volume 用于存放客户端认证的 SSL 客户端认证文件。即 [zabbix\_proxy.conf](/manual/appendix/config/zabbix_proxy) 中的  `SSLCertLocation` 参数。|
|*/var/lib/zabbix/ssl/keys*|这个 volume 用于存放客户端认证的 SSL 私钥文件。即 [zabbix\_proxy.conf](/manual/appendix/config/zabbix_proxy) 中的 `SSLKeyLocation` 参数。|
|*/var/lib/zabbix/ssl/ssl\_ca*|这个 volume 用于存放 SSL 服务器证书认证的证书颁发机构(CA)文件。即[zabbix\_proxy.conf](/manual/appendix/config/zabbix_proxy) 中的 `SSLCALocation` 参数。 |
|*/var/lib/zabbix/snmptraps*|The volume is used as location of snmptraps.log file. It could be shared by the zabbix-snmptraps container and inherited using the volumes\_from Docker option while creating a new instance of Zabbix server. SNMP trap processing feature could be enabled by using shared volume and switching the `ZBX_ENABLE_SNMP_TRAPS` environment variable to 'true'|
|*/var/lib/zabbix/mibs*|这个 volume 允许添加新的 MIB 文件。它不支持子目录，所有的 MIB 文件必须位于 `/var/lib/zabbix/mibs` 下。|
|**基于 Apache2 web 服务器的 Zabbix web 界面**|<|
|*/etc/ssl/apache2*| 这个 volume 允许为 Zabbix Web 界面启用 HTTPS。这个 volume 必须包含为 Apache2 SSL 连接准备的 `ssl.crt` 和 `ssl.key` 两个文件。|
|**基于 Nginx web 服务器的 Zabbix web 界面**|<|
|*/etc/ssl/nginx*| 这个 volume 允许为 Zabbix Web 接口启用 HTTPS。这个 volume 必须包含为 Nginx SSL 连接装备的 `ssl.crt` 和 `ssl.key` 两个文件。|
|**Zabbix snmptraps**|<|
|*/var/lib/zabbix/snmptraps*|这个 volume 包含了以接收到的 SNMP traps 命名的 `snmptraps.log`日志文件。|
|*/var/lib/zabbix/mibs*|这个 volume 允许添加新的 MIB 文件。它不支持子目录，所有的 MIB 文件必须位于 `/var/lib/zabbix/mibs` 下。|

关于更多的信息请在 Docker Hub 的 Zabbix 官方仓库查看。

[comment]: # ({/d0c9c28c-7d10f7dc})

[comment]: # ({457f5293-492bd3ba})
##### 使用示例

**示例 1**

该示例示范了如何运行 MySQL 数据库支持的 Zabbix Server 、基于 Nginx Web 服务器的 Zabbix Web 界面和 Zabbix Java 网关。

1\. 创建专用于 Zabbix 组件容器的网络：

    # docker network create --subnet 172.20.0.0/16 --ip-range 172.20.240.0/20 zabbix-net

2\. 启动空的 MySQL 服务器实例：

    # docker run --name mysql-server -t \
          -e MYSQL_DATABASE="zabbix" \
          -e MYSQL_USER="zabbix" \
          -e MYSQL_PASSWORD="zabbix_pwd" \
          -e MYSQL_ROOT_PASSWORD="root_pwd" \
          --network=zabbix-net \
          -d mysql:8.0 \
          --restart unless-stopped \
          --character-set-server=utf8 --collation-server=utf8_bin \
          --default-authentication-plugin=mysql_native_password

3\. 启动 Zabbix Java 网关实例：

    # docker run --name zabbix-java-gateway -t \
          --network=zabbix-net \
          --restart unless-stopped \
          -d zabbix/zabbix-java-gateway:alpine-5.4-latest

4\. 启动 Zabbix server 实例，并将其关联到已创建的 MySQL server 实例：

    # docker run --name zabbix-server-mysql -t \
          -e DB_SERVER_HOST="mysql-server" \
          -e MYSQL_DATABASE="zabbix" \
          -e MYSQL_USER="zabbix" \
          -e MYSQL_PASSWORD="zabbix_pwd" \
          -e MYSQL_ROOT_PASSWORD="root_pwd" \
          -e ZBX_JAVAGATEWAY="zabbix-java-gateway" \
          --network=zabbix-net \
          -p 10051:10051 \
          --restart unless-stopped \
          -d zabbix/zabbix-server-mysql:alpine-5.4-latest

::: noteclassic
Zabbix server 实例将 10051/TCP 端口（Zabbix trapper）暴露给主机。
:::

5\. 启动 Zabbix Web 界面，并将其关联到已创建的 MySQL server 和 Zabbix server 实例：

    # docker run --name zabbix-web-nginx-mysql -t \
          -e ZBX_SERVER_HOST="zabbix-server-mysql" \
          -e DB_SERVER_HOST="mysql-server" \
          -e MYSQL_DATABASE="zabbix" \
          -e MYSQL_USER="zabbix" \
          -e MYSQL_PASSWORD="zabbix_pwd" \
          -e MYSQL_ROOT_PASSWORD="root_pwd" \
          --network=zabbix-net \
          -p 80:8080 \
          --restart unless-stopped \
          -d zabbix/zabbix-web-nginx-mysql:alpine-5.4-latest

::: noteclassic
Zabbix web 界面实例将 80/TCP 端口（HTTP）暴露给主机。
:::

**示例 2**

该示例示范了如何运行 PostgreSQL 数据库支持的 Zabbix server、基于 Nginx Web 服务器的 Zabbix Web 界面和 SNMP trap功能。

1\. 创建专用于 Zabbix 组件容器的网络：

    # docker network create --subnet 172.20.0.0/16 --ip-range 172.20.240.0/20 zabbix-net

2\. 启动空的 PostgreSQL server 实例：

    # docker run --name postgres-server -t \
          -e POSTGRES_USER="zabbix" \
          -e POSTGRES_PASSWORD="zabbix_pwd" \
          -e POSTGRES_DB="zabbix" \
          --network=zabbix-net \
          --restart unless-stopped \
          -d postgres:latest

3\. 启动 Zabbix snmptraps 实例：

    # docker run --name zabbix-snmptraps -t \
          -v /zbx_instance/snmptraps:/var/lib/zabbix/snmptraps:rw \
          -v /var/lib/zabbix/mibs:/usr/share/snmp/mibs:ro \
          --network=zabbix-net \
          -p 162:1162/udp \
          --restart unless-stopped \
          -d zabbix/zabbix-snmptraps:alpine-5.4-latest

::: noteclassic
Zabbix snmptrap 实例将 162/UDP 端口（SNMP traps）暴露给主机。
:::

4\. 启动 Zabbix server 实例，并将其关联到已创建的 PostgreSQL server 实例：

    # docker run --name zabbix-server-pgsql -t \
          -e DB_SERVER_HOST="postgres-server" \
          -e POSTGRES_USER="zabbix" \
          -e POSTGRES_PASSWORD="zabbix_pwd" \
          -e POSTGRES_DB="zabbix" \
          -e ZBX_ENABLE_SNMP_TRAPS="true" \
          --network=zabbix-net \
          -p 10051:10051 \
          --volumes-from zabbix-snmptraps \
          --restart unless-stopped \
          -d zabbix/zabbix-server-pgsql:alpine-5.4-latest

::: noteclassic
Zabbix server 实例将 10051/TCP 端口（Zabbix trapper）暴露给主机。
:::

5\. 启动 Zabbix Web 界面，并将其关联到已创建的 PostgreSQL server 和 Zabbix server 实例：

    # docker run --name zabbix-web-nginx-pgsql -t \
          -e ZBX_SERVER_HOST="zabbix-server-pgsql" \
          -e DB_SERVER_HOST="postgres-server" \
          -e POSTGRES_USER="zabbix" \
          -e POSTGRES_PASSWORD="zabbix_pwd" \
          -e POSTGRES_DB="zabbix" \
          --network=zabbix-net \
          -p 443:8443 \
          -p 80:8080 \
          -v /etc/ssl/nginx:/etc/ssl/nginx:ro \
          --restart unless-stopped \
          -d zabbix/zabbix-web-nginx-pgsql:alpine-5.4-latest

::: noteclassic
Zabbix web 界面实例将 443/TCP 端口（HTTPS）暴露给主机。\
*/etc/ssl/nginx* 目录必须包含具有所需名称的证书。
:::

**示例 3**

该示例示范了如何在Red Hat 8上使用 `podman` 运行 MySQL 数据库支持的 Zabbix Server 、基于 Nginx Web 服务器的 Zabbix Web 界面和 Zabbix Java 网关。

1\. 创建一个名为 `zabbix` 的 pod 并暴露端口 (web 界面、Zabbix server trapper)：

    podman pod create --name zabbix -p 80:8080 -p 10051:10051

2\. （可选）在 `zabbix` pod 中启动 Zabbix agent 容器：

    podman run --name zabbix-agent \
        -eZBX_SERVER_HOST="127.0.0.1,localhost" \
        --restart=always \
        --pod=zabbix \
        -d registry.connect.redhat.com/zabbix/zabbix-agent-50:latest

3\. 在主机上创建`./mysql/` 目录并启动 Oracle MySQL server
8.0:

    podman run --name mysql-server -t \
          -e MYSQL_DATABASE="zabbix" \
          -e MYSQL_USER="zabbix" \
          -e MYSQL_PASSWORD="zabbix_pwd" \
          -e MYSQL_ROOT_PASSWORD="root_pwd" \
          -v ./mysql/:/var/lib/mysql/:Z \
          --restart=always \
          --pod=zabbix \
          -d mysql:8.0 \
          --character-set-server=utf8 --collation-server=utf8_bin \
          --default-authentication-plugin=mysql_native_password

3\. 启动 Zabbix server 容器：

    podman run --name zabbix-server-mysql -t \
                      -e DB_SERVER_HOST="127.0.0.1" \
                      -e MYSQL_DATABASE="zabbix" \
                      -e MYSQL_USER="zabbix" \
                      -e MYSQL_PASSWORD="zabbix_pwd" \
                      -e MYSQL_ROOT_PASSWORD="root_pwd" \
                      -e ZBX_JAVAGATEWAY="127.0.0.1" \
                      --restart=always \
                      --pod=zabbix \
                      -d registry.connect.redhat.com/zabbix/zabbix-server-mysql-50

4\. 启动 Zabbix Java 网关容器：

    podman run --name zabbix-java-gateway -t \
          --restart=always \
          --pod=zabbix \
          -d registry.connect.redhat.com/zabbix/zabbix-java-gateway-50

5\. 启动 Zabbix web 界面 容器：

    podman run --name zabbix-web-mysql -t \
                      -e ZBX_SERVER_HOST="127.0.0.1" \
                      -e DB_SERVER_HOST="127.0.0.1" \
                      -e MYSQL_DATABASE="zabbix" \
                      -e MYSQL_USER="zabbix" \
                      -e MYSQL_PASSWORD="zabbix_pwd" \
                      -e MYSQL_ROOT_PASSWORD="root_pwd" \
                      --restart=always \
                      --pod=zabbix \
                      -d registry.connect.redhat.com/zabbix/zabbix-web-mysql-50

::: noteclassic
`zabbix` pod 从 `zabbix-web-mysql` 容器的 8080/TCP 向 主机的 80/TCP port (HTTP) 暴露端口。
:::

[comment]: # ({/457f5293-492bd3ba})

[comment]: # ({4b56d036-c443c22e})
### Docker Compose

Zabbix 还提供了用于在 Docker 中定义和运行多容器 Zabbix 组件的 compose 文件。 这些 compose 文件可以在 github.com: <https://github.com/zabbix/zabbix-docker> 上的 Zabbix docker 官方仓库中找到。这些 compose 文件作为示例添加，并支持广泛。例如，Zabbix proxy 支持 MySQL和 SQLite3。

以下为几个不同版本的 compose 文件：

|   |   |
|---|---|
|**文件名**|**描述**|
|`docker-compose_v3_alpine_mysql_latest.yaml`|该 compose 文件运行基于 Alpine Linux 的 Zabbix 5.4 最新版本的组件，支持 MySQL 数据库。|
|`docker-compose_v3_alpine_mysql_local.yaml`|该 compose 文件本地构建和运行基于 Alpine Linux 的 Zabbix 5.4 最新版本的组件，支持 MySQL数据库。|
|`docker-compose_v3_alpine_pgsql_latest.yaml`|该 compose 文件运行基于 Alpine Linux 的 Zabbix 5.4 最新版本的组件，支持 PostgreSQL 数据库。|
|`docker-compose_v3_alpine_pgsql_local.yaml`|该 compose 文件本地构建和运行基于 Apline Linux 的 Zabbix 5.4 最新版本的组件，支持 PostgreSQL 数据库。|
|`docker-compose_v3_centos_mysql_latest.yaml`|该 compose 文件运行基于 CentOS 8 的 Zabbix 5.4 最新版本的组件，支持 MySQL 数据库。|
|`docker-compose_v3_centos_mysql_local.yaml`|该 compose 文件本地构建和运行基于 CentOS 8 的 Zabbix 5.4 最新版本的组件，支持 MySQL 数据库。|
|`docker-compose_v3_centos_pgsql_latest.yaml`|该 compose 文件运行基于 CentOS 8 的 Zabbix 5.4 最新版本的组件，支持 PostgreSQL 数据库。|
|`docker-compose_v3_centos_pgsql_local.yaml`|该 compose 文件本地构建和运行基于 CentOS 8 的 Zabbix 5.4 最新版本的组件，支持 PostgreSQL 数据库。|
|`docker-compose_v3_ubuntu_mysql_latest.yaml`|该 compose 文件运行基于 Ubuntu 20.04 的 Zabbix 5.4 最新版本的组件，支持 MySQL 数据库。|
|`docker-compose_v3_ubuntu_mysql_local.yaml`|该 compose 文件本地构建和运行基于 Ubuntu 20.04 的 Zabbix 5.4 最新版本的组件，支持 MySQL 数据库。|
|`docker-compose_v3_ubuntu_pgsql_latest.yaml`|该 compose 文件运行基于 Ubuntu 20.04 的 Zabbix 5.4 最新版本的组件，支持 PostgreSQL 数据库。|
|`docker-compose_v3_ubuntu_pgsql_local.yaml`|该 compose 文件本地构建和运行基于 Ubuntu 20.04 的 Zabbix 5.4 最新版本的组件，支持 PosegreSQL 数据库。|

::: noteimportant
Docker compose 文件支持  Docker Compose 3 版本。
:::

[comment]: # ({/4b56d036-c443c22e})

[comment]: # ({1b4cda14-52e39127})
#### 存储

Compose 文件已经配置为支持主机上的存储。当你使用 Compose 文件运行 Zabbix 组件时，Docker Compose 将在其所在文件夹中创建一个 `zbx_env` 目录，该目录将包含于 [Volumes](#Volumes) 章节所述相同的结构，以用于数据库存储。

此外，volume 下的文件 `/etc/localtime` 和 `/etc/timezone` 为只读模式。

[comment]: # ({/1b4cda14-52e39127})

[comment]: # ({8c383927-0be3c140})
#### 环境变量文件

在 github.com 上与存放 compose 文件的同一目录中，您可以在 compose 文件中找到每个组件的默认环境变量文件，这些环境变量文件的命令与 `.env_<type of component>`  类似。

[comment]: # ({/8c383927-0be3c140})

[comment]: # ({4dad7731-ad55959c})
#### 示例

**示例 1**

    # git checkout 5.4
    # docker-compose -f ./docker-compose_v3_alpine_mysql_latest.yaml up -d

这个命令将会为每个 Zabbix 组件下载最新的 Zabbix 5.4 image，并以 detach 模式运行。

::: noteimportant
不要忘记从 github.com 的 Zabbix 官方镜像仓库下载 `.env_<type of component>` 文件和 compose 文件。
:::

**示例 2**

    # git checkout 5.4
    # docker-compose -f ./docker-compose_v3_ubuntu_mysql_local.yaml up -d

这个命令将会下载基于 Ubuntu 20.04 的 image，并在本地构建 Zabbix 5.4 组件，以 detach 模式运行。

[comment]: # ({/4dad7731-ad55959c})
