[comment]: # translation:outdated

[comment]: # ({new-8d986b3b})
# 1 获取 Zabbix

[comment]: # ({/new-8d986b3b})

[comment]: # ({5cec5705-74ef7c01})
#### 概述

获取 Zabbix 安装介质有四种方法：

-   从 [发行包](install_from_packages#From_distribution_packages) 安装；
-   下载最新的归档源码包并 [编译它](install#Installation_from_sources)；
-   从 [容器](containers) 中安装；
-   下载 [Zabbix 应用](/manual/appliance)。

请转到 [Zabbix 下载页面](https://www.zabbix.com/download)
下载最新的源码包或应用，此页面提供最新版本的直接链接。
如果要下载旧版本，请参阅以下稳定版本下载链接。

[comment]: # ({/5cec5705-74ef7c01})


[comment]: # ({59e8df32-e88f42af})
####获取Zabbix源码
如何获取Zabbix源码有如下几种方式：


- 可从 Zabbix 官方网站获取发布的稳定版本 [下载](https://www.zabbix.com/download_sources)    
 - 可从 Zabbix官方开发网站页面获取源架构 [下载](https://www.zabbix.com/developers) 
 
   -   可以从 Git 源代码中获取最新的开发版本
     存储系统：
       -   完整存储Zabbix源码的主要位置是
        <https://git.zabbix.com/scm/zbx/zabbix.git>
       -   主版本和支持版本也镜像到 Github，网址为
        <https://github.com/zabbix/zabbix>

必须安装 Git 客户端才能克隆存储源。 官方命令行 Git 客户端包在发布版本中通常称为 **git**。 示例：在 Debian/Ubuntu 上安装，请运行如下命令：

    sudo apt-get update
    sudo apt-get install git

若需获取所有 Zabbix 源代码，请更改相应文件来替换源码并执行如下命令：

    git clone https://git.zabbix.com/scm/zbx/zabbix.git

[comment]: # ({/59e8df32-e88f42af})
