[comment]: # translation:outdated

[comment]: # ({new-02b10f8d})
# 2 安装要求

[comment]: # ({/new-02b10f8d})

[comment]: # ({new-80330f65})
#### 硬件

[comment]: # ({/new-80330f65})

[comment]: # ({495e22ac-4ae00568})
##### 内存和磁盘

Zabbix 运行需要物理内存和磁盘空间。如果刚接触 Zabbix，128 MB 的物理内存和 256 MB 的可用磁盘空间可能是一个很好的起点。 然而，所需的内存和磁盘空间显然取决于被监控的主机数量和配置参数。 如果您计划调整参数以保留较长的历史数据，那么您应该考虑至少有几 GB 磁盘空间，以便有足够的磁盘空间将历史数据存储在数据库中。

每个 Zabbix 守护程序进程都需要与数据库服务器建立多个连接。 为连接分配的内存量取决于数据库引擎的配置。

::: noteclassic
您拥有的物理内存越多，数据库（以及 Zabbix ）的工作速度就越快！
:::

[comment]: # ({/495e22ac-4ae00568})

[comment]: # ({d0fa8e6c-7967309a})
##### CPU

Zabbix，尤其是 Zabbix 数据库可能需要大量 CPU 资源，该具体取决于被监控参数的数量和所选的数据库引擎。

[comment]: # ({/d0fa8e6c-7967309a})

[comment]: # ({bf95e007-1cbee7bf})
##### 其它硬件

如果需要启用短信（SMS）通知功能，需要串行通讯口（serial communication port）和串行GSM调制解调器（serial GSM modem）。USB转串行转接器也同样可以工作。

[comment]: # ({/bf95e007-1cbee7bf})

[comment]: # ({new-42d1d93e})
#### Examples of hardware configuration

The table provides examples of hardware configuration, assuming a **Linux/BSD/Unix** platform.

These are size and hardware configuration examples to start with. Each Zabbix installation is unique. 
Make sure to benchmark the performance of your Zabbix system in a staging or development environment, 
so that you can fully understand your requirements before deploying the Zabbix installation to its 
production environment.

|Installation size|Monitored metrics^**1**^|CPU/vCPU cores|Memory<br>(GiB)|Database|Amazon EC2^**2**^|
|-|-|-|-|-|-|
|Small|1 000|2|8|MySQL Server,<br>Percona Server,<br>MariaDB Server,<br>PostgreSQL|m6i.large/m6g.large|
|Medium|10 000|4|16|MySQL Server,<br>Percona Server,<br>MariaDB Server,<br>PostgreSQL|m6i.xlarge/m6g.xlarge|
|Large|100 000|16|64|MySQL Server,<br>Percona Server,<br>MariaDB Server,<br>PostgreSQL,<br>Oracle|m6i.4xlarge/m6g.4xlarge|
|Very large|1 000 000|32|96|MySQL Server,<br>Percona Server,<br>MariaDB Server,<br>Oracle|m6i.8xlarge/m6g.8xlarge|

^**1**^ 1 metric = 1 item + 1 trigger + 1 graph<br>
^**2**^ Example with Amazon general purpose EC2 instances, using ARM64 or x86_64 architecture, a 
proper instance type like Compute/Memory/Storage optimised should be selected during Zabbix 
installation evaluation and testing before installing in its production environment.

::: noteclassic
Actual configuration depends on the number of active items
and refresh rates very much (see [database
size](/manual/installation/requirements#Database_size) section of this
page for details). It is highly recommended to run the database on a
separate box for large installations.
:::

[comment]: # ({/new-42d1d93e})



[comment]: # ({b9c9fd17-ad0c32cf})
#### 受支持的平台

由于服务器操作的安全性要求和任务关键性，UNIX 是唯一能够始终如一地提供必要性能、容错和弹性的操作系统。 Zabbix以市场主流的操作系统版本运行。

经测试，Zabbix 组件 可以运行在下列平台：

|平台|Server|Agent|Agent2|
|--------|------|-----|------|
|Linux|x|x|x|
|IBM AIX|x|x|\-|
|FreeBSD|x|x|\-|
|NetBSD|x|x|\-|
|OpenBSD|x|x|\-|
|HP-UX|x|x|\-|
|Mac OS X|x|x|\-|
|Solaris|x|x|\-|
|Windows|\-|x|x|

::: noteclassic
Zabbix server/agent 也可以在其他类Unix操作系统上运行。自XP以来，所有Windows desktop 和 server 版本都支持Zabbix agent。
:::

::: noteimportant
如果使用加密编译，Zabbix将禁用核心转储（Core dumps），如果系统不允许禁用核心转储，则 Zabbix 不会启动。
:::

[comment]: # ({/b9c9fd17-ad0c32cf})

[comment]: # ({da389a5d-323b6241})
#### 软件

Zabbix是围绕现代Web服务器，领先的数据库引擎和PHP脚本语言构建的。

[comment]: # ({/da389a5d-323b6241})

[comment]: # ({new-fd454df8})
##### 数据库管理系统

|数据库软件|支持的版本|备注|
|--------|------------------|--------|
|*MySQL/Percona*|8.0.X|如果需要使用 MySQL (or Percona) 作为 Zabbix 后端数据库。 需要 InnoDB engine 。 我们推荐使用 [MariaDB Connector/C](https://downloads.mariadb.org/connector-c/) 来构建 server/proxy。|
|*MariaDB*|10.5.00-10.6.X|需要 InnoDB engine 。 我们推荐使用 [MariaDB Connector/C](https://downloads.mariadb.org/connector-c/) 来构建 server/proxy。|
|*Oracle*|19c - 21c|如果需要使用 Oracle 作为 Zabbix 后端数据库。|
|*PostgreSQL*|13.X|如果需要使用 PostgreSQL 作为 Zabbix 后端数据库。|
|*TimescaleDB* for PostgreSQL|2.0.1-2.3|如果需要使用 TimescaleDB 作为 Zabbix 后端数据库。确保安装的 TimescaleDB 发行版支持压缩。|
|*SQLite*|3.3.5-3.34.X| 如果需要使用 SQLite 作为 Zabbix 后端数据库。 SQLite 只支持 Zabbix proxy。|

::: noteclassic
尽管 Zabbix 可以使用操作系统中可用的数据库，但为了获得最佳体验，我们建议使用从官方数据库开发者仓库安装的数据库。

:::

[comment]: # ({/new-fd454df8})

[comment]: # ({12bb656b-75f0586e})
##### 前端

Zabbix 前端支持的最小屏幕宽度为 1200px。

|软件|版本|备注|
|--------|--------|--------|
|*Apache*|1.3.12 或更高版本|<|
|*PHP*|7.2.5 或更高版本|不支持 PHP 8.0。|
|PHP 扩展：|<|<|
|*gd*|2.0.28 或更高版本|PHP GD 扩展必须支持 PNG (*--with-png-dir*)、JPEG (*--with-jpeg-dir*) 和 FreeType 2 (*--with-freetype-dir*)。|
|*bcmath*|<|php-bcmath (*--enable-bcmath*)|
|*ctype*|<|php-ctype (*--enable-ctype*)|
|*libXML*|2.6.15 或更高版本|php-xml，如果由分发者作为单独的包提供。|
|*xmlreader*|<|php-xmlreader，如果由分发者作为单独的包提供。|
|*xmlwriter*|<|php-xmlwriter，如果由分发者作为单独的包提供。|
|*session*|<|php-session，如果由分发者作为单独的包提供。|
|*sockets*|<|php-net-socket (*--enable-sockets*)。需要用户脚本支持。|
|*mbstring*|<|php-mbstring (*--enable-mbstring*)|
|*gettext*|<|php-gettext (*--with-gettext*)。Required for translations to work.|
|*ldap*|<|php-ldap.仅当在前端使用 LDAP 身份验证时才需要。|
|*openssl*|<|php-openssl.仅当在前端使用 SAML 身份验证时才需要。|
|*mysqli*|<|如果 MySQL 用作 Zabbix 后端数据库，则需要。|
|*oci8*|<|如果使用 Oracle 作为 Zabbix 后端数据库，则需要。|
|*pgsql*|<|如果使用 PostgreSQL 作为 Zabbix 后端数据库，则需要。|

::: noteclassic
Zabbix 也许可以在以前的Apache、MySQL、Oracle 和 PostgreSQL 版本上运行。
:::

::: noteimportant
如果需要使用默认 DejaVu 以外的字体, 可能会需要 PHP 的 [imagerotate](http://php.net/manual/en/function.imagerotate.php) 函数。如果缺少，在 Zabbix 前端查看图形时可能会显示异常。该函数只有在使用捆绑的 GD 库编译 PHP 时才可用。在 Debian 和某些发行版本中，这个问题不存在。
:::

[comment]: # ({/12bb656b-75f0586e})

[comment]: # ({ecf89e26-dda86afe})
##### 客户端浏览器

浏览器必须启用 Cookies 和 Java Script 。

支持Google Chrome，Mozilla Firefox，Microsoft Edge，Apple Safari和Opera的最新稳定版本。

::: notewarning
为了执行 IFrame 的“同源政策”，意味着
Zabbix 不能放在不同域的 frames 中。\
\
但是，如果放置在 frames 中的页面和 Zabbix 前端位于同一个域中，则置于
Zabbix frames 中的页面将可以访问 Zabbix 前端（通过JavaScript）。像
`http://secure-zabbix.com/cms/page.html` 这样的页面，如果置于
`http://secure-zabbix.com/zabbix/` 的聚合图形或仪表盘上，将拥有对 Zabbix
的完整 JS 访问权限。
:::

[comment]: # ({/ecf89e26-dda86afe})

[comment]: # ({209baae6-096d332c})
##### Server

强制性要求始终需要。可选要求在支持特定功能时需要。

|需求|状态|描述|
|-----------|------|-----------|
|*libpcre*|强制|PCRE 库 被 [Perl 兼容正则表达式](https://en.wikipedia.org/wiki/Perl_Compatible_Regular_Expressions) (PCRE) 支持 所需要。<br> 命名可能因 GNU/Linux 发行版而异，例如 'libpcre3' 或 'libpcre1'。(Zabbix 6.0.0)支持 PCRE v8.x 及 PCRE2 v10.x。|
|*libevent*|^|Required for bulk metric support and IPMI monitoring. Version 1.4 or higher.<br>Note that for Zabbix proxy this requirement is optional; it is needed for IPMI monitoring support.|
|*libpthread*|^|被 互斥锁（mutex）和 读写分离锁 （read-write lock）支持 所需要。|
|*zlib*|^|被 压缩支持 所需要。|
|*OpenIPMI*|可选|被 IPMI 支持 所需要。|
|*libssh2*|^|被 SSH 支持所需要。 版本 1.0 以上。|
|*fping*|^|被 [ICMP ping 监控项](/manual/config/items/itemtypes/simple_checks#icmp_pings) 所需要。|
|*libcurl*|^| 被 web 监控， VMware 监控 和 SMTP 认证 所需要。如果是为了 SMTP 认证，需要 7.20.0 以上的版本，同时需要 Elasticsearch。|
|*libiksemel*|^|被 Jabber 支持 所需要。|
|*libxml2*|^|被 VMware 监控 所需要。|
|*net-snmp*|^|被 SNMP 支持 所需要。|

[comment]: # ({/209baae6-096d332c})

[comment]: # ({734d0616-800c8308})
##### Agent

|需求|状态|描述|
|-----------|------|-----------|
|*libpcre*|强制|PCRE 库 被 [Perl 兼容正则表达式](https://en.wikipedia.org/wiki/Perl_Compatible_Regular_Expressions) (PCRE) 支持 所需要。<br> 命名可能因 GNU/Linux 发行版而异，例如 'libpcre3' 或 'libpcre1'。(Zabbix 6.0.0)支持 PCRE v8.x 及 PCRE2 v10.x。|
|*GnuTLS*, *OpenSSL* or *LibreSSL*|可选|当使用 [加密](/manual/encryption#compiling_zabbix_with_encryption_support) 时需要。<br>在 Microsoft Windows 系统上需要 OpenSSL 1.1.1 及以上版本。|

::: noteclassic
从 5.0.3 版本开始，Zabbix agent 将不再支持低于 6.1 TL07 / AIX 7.1 TL01 版本的 AIX 平台。
:::

[comment]: # ({/734d0616-800c8308})

[comment]: # ({8c4b8390-4aa86212})
##### Agent 2

|需求|状态|描述|
|-----------|------|-----------|
|*libpcre*|强制|PCRE 库 被 [Perl 兼容正则表达式](https://en.wikipedia.org/wiki/Perl_Compatible_Regular_Expressions) (PCRE) 支持 所需要。<br> 命名可能因 GNU/Linux 发行版而异，例如 'libpcre3' 或 'libpcre1'。(Zabbix 6.0.0)支持 PCRE v8.x 及 PCRE2 v10.x。|
|*OpenSSL*|可选|当使用加密时需要。<br>UNIX 平台上需要 OpenSSL 1.0.1 或更高版本。<br>OpenSSL 库必须启用 PSK 支持。不支持 LibreSSL。<br> 在 Microsoft Windows 系统上需要 OpenSSL 1.1.1 及以上版本。|

[comment]: # ({/8c4b8390-4aa86212})

[comment]: # ({c9c5c0fd-149075a3})
##### Java 网关

如果您从源代码仓库或存档中获得了 Zabbix，则必要的依赖项已包含在源代码树中。

如果您从发行版的软件包中获得了Zabbix，那么打包系统已经提供了必要的依赖项。

在上述两种情况下，即可准备部署软件了，而不需要下载额外的依赖包。

但是，如果您希望提供这些依赖关系的版本（例如，如果您正在为某些 Linux 发行版准备软件包），则下面是 Java 网关已知可以使用的库的版本列表。 Zabbix 也许可以与这些库的其他版本一起使用。

下表列出了原始代码中当前与 Java 网关捆绑在一起的 JAR 文件：

|库名|许可|网站|备注|
|-------|-------|-------|--------|
|*logback-core-1.2.3.jar*|EPL 1.0, LGPL 2.1|<http://logback.qos.ch/>|0.9.27, 1.0.13, 1.1.1 和 1.2.3 测试通过。|
|*logback-classic-1.2.3.jar*|EPL 1.0, LGPL 2.1|<http://logback.qos.ch/>|0.9.27, 1.0.13, 1.1.1 和 1.2.3 测试通过。|
|*slf4j-api-1.7.30.jar*|MIT License|<http://www.slf4j.org/>|1.6.1, 1.6.6, 1.7.6 和 1.7.30 测试通过。|
|*android-json-4.3\_r3.1.jar*|Apache License 2.0|<https://android.googlesource.com/platform/libcore/+/master/json>|2.3.3\_r1.1 和 4.3\_r3.1 测试通过。关于创建 JAR 文件，详见 src/zabbix\_java/lib/README 说明。|

Java网关可以使用Oracle Java或开源OpenJDK（版本1.6或更高版本）构建。Zabbix提供的软件包是使用OpenJDK编译的。下表提供了有关用于按发行版构建Zabbix软件包的OpenJDK版本的信息：

|发行版|OpenJDK 版本|
|------------|---------------|
|RHEL/CentOS 8|1.8.0|
|RHEL/CentOS 7|1.8.0|
|SLES 15|11.0.4|
|SLES 12|1.8.0|
|Debian 10|11.0.8|
|Ubuntu 20.04|11.0.8|
|Ubuntu 18.04|11.0.8|

[comment]: # ({/c9c5c0fd-149075a3})

[comment]: # ({new-c8024c35})

#### Default port numbers

The following list of open ports per component is applicable for default configuration:

|Zabbix component|Port number|Protocol|Type of connection|
|-------|-------|-------|-------|
|Zabbix agent|10050|TCP|on demand|
|Zabbix agent 2|10050|TCP|on demand|
|Zabbix server|10051|TCP|on demand|
|Zabbix proxy|10051|TCP|on demand|
|Zabbix Java gateway|10052|TCP|on demand|
|Zabbix web service|10053|TCP|on demand|
|Zabbix frontend|80|HTTP|on demand|
|^|443|HTTPS|on demand|
|Zabbix trapper|10051 |TCP| on demand|

::: noteclassic
The port numbers should be open in firewall to enable Zabbix communications. Outgoing TCP connections usually do not require explicit firewall settings.
::: 

[comment]: # ({/new-c8024c35})




[comment]: # ({49a47ae5-1d73b238})
#### 数据库大小

Zabbix 配置文件数据需要固定数量的磁盘空间，且增长不大。

Zabbix 数据库大小主要取决于这些变量，这些变量决定了存储的历史数据量：

-   每秒处理值的数量

这是 Zabbix server 每秒接收的新值的平均数。 例如，如果有3000个监控项用于监控，取值间隔为60秒，则这个值的数量计算为 3000/60 = ** 50 **。

这意味着每秒有 50 个新值被添加到 Zabbix 数据库中。

-   Housekeeper 的历史记录设置

Zabbix 将接收到的值保存一段固定的时间，通常为几周或几个月。 每个新值都需要一定量的磁盘空间用于数据和索引。

所以，如果我们每秒收到 50 个值，且希望保留 30 天的历史数据，值的总数将大约在 (**30**\*24\*3600)\* **50** = 129.600.000，即大约 130M 个值。

根据所使用的数据库引擎，接收值的类型（浮点数、整数、字符串、日志文件等），单个值的磁盘空间可能在 40 字节到数百字节之间变化。通常，数值类型的每个值大约为 90 个字节^**2**^。在上面的例子中，这意味着 130M 个值需要占用 130M \* 90 bytes = **10.9GB** 磁盘空间。

::: noteclassic
文本/日志类型的监控项值的大小是无法确定的，但可以以每个值大约 500 字节来计算。
:::

-   Housekeeper 的趋势记录设置

Zabbix 为表 **trends** 中的每个项目保留1小时的最大值 / 最小值 / 平均值 / 统计值。该数据用于趋势图形和历史数据图形。这一个小时的时间段是无法自定义。

Zabbix数据库，根据数据库类型，每个值总共需要大约90个字节。假设我们希望将趋势数据保持5年。3000 个监控项的值每年需要占用 3000\*24\*365\* **90** = **2.2GB**，或者5年需要占用 **11GB** 。

-   Housekeeper 的事件记录设置

每个 Zabbix 事件需要大约 250 个字节的磁盘空间^**1**^。很难估计 Zabbix 每天生成的事件数量。 在最坏的情况下，假设 Zabbix 每秒生成一个事件。

对于每个恢复的事件，将创建一个event_recovery记录。通常，大多数事件将被恢复，因此我们可以假设每个事件有一个 event_recovery 记录。这意味着每个事件额外 80 个字节。

（可选）事件可以具有标记，每个标记记录需要大约 100 字节的磁盘空间^**1**^。每个事件的标签数 (\#tags)取决于配置。因此，每个事件都需要额外的 标签数\#tags * 100 字节 的磁盘空间。

这意味着如果想要保留3年的事件，这将需要 3\*365\*24\*3600\* (250+80+ 标签数\#tags\*100) = **\~30GB**+ 标签数\#tags\*100B 的磁盘空间^**2**^。

::: noteclassic
 ^**1**^ 当具有非 ASCII 的事件名称、标记和值时，需要的空间会更多。

^**2**^ 大小近似值基于MySQL，对于其他数据库可能有所不同。
:::

下表包含可用于计算 Zabbix 系统所需磁盘空间的公式：

|类型|所需磁盘空间的公式 (字节)|
|---------|------------------------------------------|
|*Zabbix 配置*|固定大小。通常为 10MB 或更少。|
|*历史数据*|days\*(items/refresh rate)\*24\*3600\*bytes<br> items：监控项数量<br>days：保留历史记录的天数<br>refresh rate：监控项的平均刷新率<br>bytes：保留单个值所需要占用的字节数，依赖于数据库引擎，通常为 \~90 字节。|
|*趋势数据*|days\*(items/3600)\*24\*3600\*bytes<br>items：监控项数量<br>days：保留历史记录的天数<br>bytes：保留单个趋势数据所需要占用的字节数，依赖于数据库引擎，通常为 \~90 字节。|
|*事件数据*|days\*events\*24\*3600\*bytes<br>events：每秒产生的事件数量。假设最糟糕的情况下，每秒产生 1 个事件。<br>days：保留历史数据的天数。<br>bytes：保留单个趋势数据所需的字节数，取决于数据库引擎，通常为 \~330 + 每个事件的平均标签数 \* 100 字节。|

因此，所需要的磁盘总空间按下列方法计算：\
**Zabbix 配置 + 历史数据 + 趋势数据 + 事件数据**\
在安装 Zabbix 后**不会**立即使用磁盘空间。
数据库大小取决于 housekeeper 设置，在某些时间点增长或停止增长。

[comment]: # ({/49a47ae5-1d73b238})

[comment]: # ({179154ac-520ea0fa})
#### 时间同步

服务器上拥有精确的系统时间对 Zabbix 的运行非常重要。 [ntpd](http://www.ntp.org/) 是最流行的守护程序，它将主机的时间与其他计算机的时间同步。强烈建议在运行 Zabbix 组件的所有系统上保持系统时间同步。

[comment]: # ({/179154ac-520ea0fa})

[comment]: # ({new-9e23ba76})

#### Network requirements

A following list of open ports per component is applicable for default configuration.


|Port|Components |
|------------|---------------|
|Frontend|http on 80, https on 443|
|Server|10051 (for use with active proxy/agents)|
|Active Proxy |10051 ( not sure about fetching config)|
|Passive Proxy|10051|
|Agent2|10050|
|Trapper| |
|JavaGateway|10053|
|WebService|10053|

::: noteclassic
In case the configuration is completed, the port numbers should be edited accordingly to prevent firewall from blocking communications with these ports.Outgoing TCP connections usually do not require explicit firewall settings.
::: 

[comment]: # ({/new-9e23ba76})
