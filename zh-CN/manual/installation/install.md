[comment]: # translation:outdated

[comment]: # ({17958beb-af2eaf2f})
# 3 从源代码包安装

您可以通过从源代码编译来获取最新版本的 Zabbix。

这里提供了具体安装步骤。

[comment]: # ({/17958beb-af2eaf2f})

[comment]: # ({5018ad9e-0040d992})
#### - 安装Zabbix守护进程

[comment]: # ({/5018ad9e-0040d992})

[comment]: # ({new-8cc2c1b1})
##### 1 下载源代码存档

转到 [Zabbix 下载页面](http://www.zabbix.com/download_sources)
下载源代码存档。待下载完毕后，执行以下命令解压缩源代码存档：

    $ tar -zxvf zabbix-5.2.0.tar.gz

::: 注意
请在命令中输入正确的 Zabbix 版本。
它必须与下载的存档的名称匹配。
:::

[comment]: # ({/new-8cc2c1b1})

[comment]: # ({c9c92682-87e1101d})
##### 2 创建用户账户

对于所有 Zabbix 守护进程，需要一个非特权用户。 如果从非特权用户帐户启动
Zabbix 守护程序，它将以该用户身份运行。

然而，如果一个守护进程以“root”启动，它会切换到“zabbix”用户，且这个用户必须存在。在
Linux系统中，可以使用下面命令建立一个用户（该用户属于自己的用户组，“zabbix”）：

    groupadd zabbix
    useradd -g zabbix zabbix

而对于 Zabbix 前端安装，并不需要单独的用户帐户。

如果 Zabbix [server](/manual/concepts/server) 和[agent](/manual/concepts/agent)
运行在相同的机器上，建议使用不同的用户运行来 Zabbix server 和 agent。
否则，如果两者都作为同一用户运行，则 Zabbix agent 可以访问 Zabbix server配置文件，并且可以轻松检索到 Zabbix中的任何管理员级别的用户，例如，数据库密码。

::: 注意
以 `root` 、`bin` 或其他具有特殊权限的账户运行Zabbix 是非常危险的。
:::

[comment]: # ({/c9c92682-87e1101d})

[comment]: # ({d6401620-06256db0})
##### 3 创建 Zabbix 数据库

对于 Zabbix [server](/manual/concepts/server) 和[proxy](/manual/concepts/proxy) 守护进程以及 Zabbix前端，必须需要一个数据库。但是 运行Zabbix [agent](/manual/concepts/agent)并不需要。

SQL [脚本](/manual/appendix/install/db_scripts) 用于创建数据库模型和插入数据集。Zabbix proxy 数据库只需要数据库模型，而 Zabbix server数据库在建立数据库模型后，还需要数据集。

创建好数据库后，继续执行编译 Zabbix 的步骤。

[comment]: # ({/d6401620-06256db0})

[comment]: # ({18217fe9-8ff1b41c})
##### 4 配置源代码

当为Zabbix server或proxy配置源码时，必须指定要用的数据库类型。编译server或proxy的同一时间只能用一种数据库类型。 

要查看所有支持的配置选项，请在提取的Zabbix源代码目录运行：

    ./configure --help

要为Zabbix server和agent配置源，可运行：

    ./configure --enable-server --enable-agent --with-mysql --enable-ipv6 --with-net-snmp --with-libcurl --with-libxml2 --with-openipmi

为Zabbix server(用PostgreSQL等)配置源 ，可运行：

    ./configure --enable-server --with-postgresql --with-net-snmp

为Zabbix proxy (用SQLite等)配置源，可运行：

    ./configure --prefix=/usr --enable-proxy --with-net-snmp --with-sqlite3 --with-ssh2

为Zabbix agent配置源，可运行：

    ./configure --enable-agent

或Zabbix agent 2:

    ./configure --enable-agent2

::: noteclassic
 构建Zabbix agent 2 需要配置Go环境 [Go 版本](https://golang.org/doc/devel/release#policy) 。 详见 [golang.org](https://golang.org/doc/install) 安装指示。
:::

编译选项注意事项:

-   编译命令行实用程序 zabbix\_get 和 zabbix\_sender  
     如果使用了 --enable-agent 选项
-   虚拟机监控需要配置 --with-libcurl 和 --with-libxml2 选项；
    SMTP认证和`web.page.*` Zabbix agent
    [items](/manual/config/items/itemtypes/zabbix_agent) 也需配置 --with-libcurl 。在配置 --with-libcurl 选项时，[需要](/manual/installation/requirements) cURL 7.20.0 或以上版本。
-   Zabbix 总是与PCRE库 (3.4.0以上版本)一起编译;
    这是不可选择的。 --with-libpcre=\[DIR\] 只允许指向特定的基本安装目录，而不是在libpcre文件中搜索一些常见位置。
-  你可能要用 --enable-static 标识来静态链接库。如果你计划在不用的server间分发编译的二进制文件，则需要使用这个标识使这些二进制文件无需所需的库也可工作。请注意，--enable-static 不适用于 [Solaris](https://docs.oracle.com/cd/E18659_01/html/821-1383/bkajp.html).
-   构建server时，不建议使用 --enable-static 选项。为了静态的构建server，每个外部库都需要一个静态版本。配置脚本中对此没有严格的检查。
-   添加可选路径到MySQL配置文件
    当需要使用不在默认位置的MySQL客户端时，用--with-mysql=/<path\_to\_the\_file>/mysql\_config 来选择所需的MySQL客户端库。这对同一个系统上装了几个版本的MySQL或者MySQL与MariaDB同时安装的情况很有用。
-   使用 --with-oracle 标识来指定OCI API的位置。

::: 注意
如果 ./configure 由于缺少库或其他条件而失败，请查看`config.log` 文件来了解更多错误信息。例如，如果缺少 `libssl` ，则及时错误信息可能具有误导性：

    checking for main in -lmysqlclient... no
    configure: error: Not found mysqlclient library

而 `config.log` 则有更详细的描述：

    /usr/bin/ld: cannot find -lssl
    /usr/bin/ld: cannot find -lcrypto


:::

另有:

-   [用加密支持编译Zabbix](/manual/encryption#compiling_zabbix_with_encryption_support)
    加密支持
-   [已知问题](/manual/installation/known_issues#compiling_zabbix_agent_on_hp-ux)
    在HP-UX上编译Zabbix agent

[comment]: # ({/18217fe9-8ff1b41c})

[comment]: # ({ebb69fca-08edd3dc})
##### 5 安装

::: 注意
如果从 [Zabbix Git repository](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse) 安装，需要先运行如下命令：

`$ make dbschema` 
:::

    make install

这一步需要使用一个拥有足够权限的用户来运行 (如 'root', 或 `sudo`).

默认情况下，运行 `make install` 将在 /usr/local/bin 中安装守护程序二进制文件 (zabbix\_server, zabbix\_agentd, zabbix\_proxy) 并在 /usr/local/sbin 中安装客户端二进制文件(zabbix\_get, zabbix\_sender)。

::: 注意
要制定不同于 /usr/local的位置, 请在配置源的上一步中使用 --prefix 键，例如 --prefix=/home/zabbix 。在此情况下，守护程序二进制文件将安装在 <prefix>/sbin 中，而实用程序则安装在<prefix>/bin中。手册将安装在  <prefix>/share 中。
:::

[comment]: # ({/ebb69fca-08edd3dc})

[comment]: # ({1a30cbca-112b74f7})
##### 6 查看和编辑配置文件

-   编辑 Zabbix agent 配置文件
    **/usr/local/etc/zabbix\_agentd.conf**

你需要为每台安装了zabbix\_agentd的主机配置此文件。

你必须在文件中指定Zabbix server的 **IP address** 。若从其他主机发起的请求会被拒绝。

-   编辑 Zabbix server 配置文件
    **/usr/local/etc/zabbix\_server.conf**

在使用时，必须指定数据库名、用户和密码。 

如果进行小环境部署（最多10个受监控主机），其余参数的默认值要适配环境。如果要最大化Zabbix server（或proxy）性能，则应该更改默认参数。详见 [p性能调整](/manual/appendix/performance_tuning)。

-   如果你安装好了Zabbix proxy, 请在此编辑proxy 配置文件 **/usr/local/etc/zabbix\_proxy.conf**

在使用时，你必须指定server 的 IP地址和 proxy 主机名（必须能被Zabbix server识别），同时要指定数据库的名称，用户和密码。

::: 注意
对于SQLite，必须指定数据库文件的完整路径；数据库用户名和密码则不是必须的。
:::

[comment]: # ({/1a30cbca-112b74f7})

[comment]: # ({944c843f-bbdd82ac})
##### 7 启动守护进程

在server端运行zabbix\_server。

    shell> zabbix_server

::: 注意
确保你的系统允许分配36MB（或更多）共享内存，否则server可能不启动，你可能会在server日志文件中看到"Cannot allocate shared memory for <type of cache>." 。 这可能会发生在 FreeBSD，Solaris 8 上。\ 详见文末 ["另请参阅"](#see_also) ，了解如何配置共享内存。
:::

在所有受监控的机器上运行 zabbix\_agentd 。

    shell> zabbix_agentd

::: 注意
确保你的系统允许分配2MB共享内存，否则server可能不启动，你可能会在server日志文件中看到 "Cannot allocate shared memory for collector." 。这可能会发生在Solaris 8上。
:::

如果你已经安装了 Zabbix proxy，请运行 zabbix\_proxy。

    shell> zabbix_proxy

[comment]: # ({/944c843f-bbdd82ac})

[comment]: # ({new-c9f154ca})
#### - 安装 Zabbix web 界面

[comment]: # ({/new-c9f154ca})

[comment]: # ({f60fac99-ed4e56ce})
##### 复制PHP文件

Zabbix前端是PHP编写的，所以运行它需要PHP支持的网络服务器。安装只需简单的从 UI 目录复制PHP文件到网络服务器 HTML文档目录。

Apache网络服务器的HTML文档目录的常见位置包括：

-   /usr/local/apache2/htdocs (从源代码安装Apache的默认目录)
-   /srv/www/htdocs (OpenSUSE, SLES)
-   /var/www/html (Debian, Ubuntu, Fedora, RHEL, CentOS)

建议使用子目录而非HTML根目录。要创建子目录并将Zabbix前端文件复制过去，请执行如下命令，以替换实际目录：

    mkdir <htdocs>/zabbix
    cd ui
    cp -a . <htdocs>/zabbix

如果计划用英语之外的语言，请参考[其他语言安装](/manual/appendix/install/locales) 。

[comment]: # ({/f60fac99-ed4e56ce})

[comment]: # ({new-63583cbb})
##### 安装前端

[comment]: # ({/new-63583cbb})

[comment]: # ({97358736-1a519c06})
#### 3 安装 Java gateway

仅在你想监控JMX应用程序时，才需要安装Java gateway。Java gateway是轻量级的，不需要数据库。

从源安装，先 [下载](/manual/installation/install#download_the_source_archive) 再提取原归档数据。

要编译Java gateway，请运行 `./configure` 脚本和
`--enable-java` 选项。建议制定 `--prefix` 选项来请求默认/usr/local 以外的安装路径，因为安装Java gateway将创建一个完整的目录树，而不仅仅是一个可执行文件。

    $ ./configure --enable-java --prefix=$PREFIX

要将java gateway编译并打包到JAR文件中，请运行 `make`。 请注意，这一步你可能需要将路径中的`javac` 和 `jar` 列为可执行。

    $ make

现在在 src/zabbix\_java/bin 中有一个 zabbix-java-gateway-$VERSION.jar 文件。如果你不介意在分发目录中从src/zabbix\_java运行Java gateway ，那么你可以继续按说明执行配置和运行 [Java gateway](/manual/concepts/java#overview_of_files_in_java_gateway_distribution)。
否则，请确保有足够的权限运行 `make install`。

    $ make install

更多配置和运行 Java gateway的细节，请点击 [安装](/manual/concepts/java/from_sources) .

[comment]: # ({/97358736-1a519c06})

[comment]: # ({0ef733e9-76c01064})
#### 4 安装Zabbix web服务

仅当你想使用[scheduled reports](/manual/web_interface/frontend_sections/reports/scheduled)时，才需要安装Zabbix web服务。

为了安装，请先[下载](/manual/installation/install#download_the_source_archive) 和提取源存档。

要编译Zabbix web服务， 请运行 `./configure` 脚本和
`--enable-webservice` 选项。

::: 注意
 已配置的 [Go](https://golang.org/doc/install) 版本，构建Zabbix web服务需要
1.13+ 环境。

:::

在安装了web服务的机器上运行 zabbix\_web\_service：

    shell> zabbix_web_service

运行 [setup](/manual/appendix/install/web_service) 了解更多关于配置计划报告生成的信息。

[comment]: # ({/0ef733e9-76c01064})

