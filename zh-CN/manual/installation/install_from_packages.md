[comment]: # translation:outdated

[comment]: # ({88624c5f-e2c1904c})
#  从二进制包安装

[comment]: # ({/88624c5f-e2c1904c})

[comment]: # ({71b7dd21-8d855b7c})

#### 使用ZABBIX官方存储库 

Zabbix SIA 提供如下官方RPM和DEB包:

-   [Red Hat Enterprise
    Linux/CentOS](/manual/installation/install_from_packages/rhel_centos)
-   [Debian/Ubuntu/Raspbian](/manual/installation/install_from_packages/debian_ubuntu)
-   [SUSE Linux Enterprise
    Server](/manual/installation/install_from_packages/suse)

yum/dnf, apt和zypper的各种操作系统发行版的软件包文件可以在[repo.zabbix.com](https://repo.zabbix.com/)上找到。

请注意，尽管某些操作系统发行版（特别是基于Debian的发行版）提供了它们自己的Zabbix包，但Zabbix不支持这些包。第三方提供的Zabbix包可能已过时，缺乏最新的特性和bug修复。推荐只使用[repo.zabbix.com](https://repo.zabbix.com/)上的官方软件包。如果之前用过非官方的Zabbix包，请参阅 [upgrading Zabbix packages from OS repositories](/manual/installation/upgrade/packages#zabbix_packages_from_os_repositories)的说明操作。

[comment]: # ({/71b7dd21-8d855b7c})
