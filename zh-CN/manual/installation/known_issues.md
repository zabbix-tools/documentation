[comment]: # translation:outdated

[comment]: # ({f40aa105-97796c37})
# 8 已知问题

[comment]: # ({/f40aa105-97796c37})

[comment]: # ({19db3a3d-e338613a})
#### 使用 MySQL 8.0.0-8.0.17 启动 Proxy 

zabbix\_proxy 在 8.0.0-8.0.17 版本的 MySQL 上启动失败并报错 "access denied" ：

 · [Z3001] connection to database 'zabbix' failed: [1227] Access denied; you need (at least one of) the SUPER, SYSTEM_VARIABLES_ADMIN or SESSION_VARIABLES_ADMIN privilege(s) for this operation

这是由于 MySQL 8.0.0 版本开始强制设置会话变量的特殊权限。然而，在 8.0.18 中删除了此行为：
[从 MySQL 8.0.18 开始，设置此系统变量的会话值不再是受限制的操作。](https://dev.mysql.com/doc/refman/8.0/en/server-system-variables.html)

解决方法是向`zabbix` 用户授予额外权限：

对于 MySQL 8.0.14 - 8.0.17:

 · grant SESSION_VARIABLES_ADMIN on *.* to 'zabbix'@'localhost';

对于 MySQL 8.0.0 - 8.0.13:

 · grant SYSTEM_VARIABLES_ADMIN on *.* to 'zabbix'@'localhost';

[comment]: # ({/19db3a3d-e338613a})


[comment]: # ({77f41f3c-68c1ee03})
#### Timescale DB

PostgreSQL 9.6-12 在更新具有大量分区的数据表时会使用过多的内存（[查看问题说明](https://www.postgresql-archive.org/memory-problems-and-crash-of-db-when-deleting-data-from-table-with-thousands-of-partitions-td6108612.html)）。这个问题会在 Zabbix + TimescaleDB 结构下更新系统上的趋势表（trends）且趋势表被分成相对较小（例如 1 天）的块的情况下体现出来。这导致趋势表中存在数百个具有默认管理设置的块 —— 因此 PostgreSQL 可能会耗尽内存。

如果使用 TimescaleDB 新装 Zabbix，则这个问题从 Zabbix 5.0.1 开始已得到解决。但如果在此之前就使用 Zabbix + TimescaleDB 结构，请参阅 [ZBX-16347](https://support.zabbix.com/browse/ZBX-16347?focusedCommentId=430816&page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel#comment-430816) 以获取迁移说明。

[comment]: # ({/77f41f3c-68c1ee03})

[comment]: # ({dfe9a773-5c2a7ea7})
#### 从 MariaDB 10.2.1 以及之前的版本升级

如果数据库表是使用 MariaDB 10.2.1 及之前的版本创建的，升级 Zabbix 可能会失败，因为在这些版本中，默认的 ROW_FORMAT（即行格式，是指数据的记录即数据行在磁盘中的物理存储方式） 是 compact。这可以通过将 ROW_FORMAT 更改为 dynamic 来解决 （另请参见 [ZBX-17690](https://support.zabbix.com/browse/ZBX-17690)）。

[comment]: # ({/dfe9a773-5c2a7ea7})

[comment]: # ({940058c7-40e33d04})
#### 与 MariaDB 的数据库 TLS 连接

如果使用 MariaDB 数据库，则 DBTLSConnect [参数](/manual/appendix/config/zabbix_server) 的 “verify_ca” 选项不支持数据库 TLS 连接。

[comment]: # ({/940058c7-40e33d04})

[comment]: # ({new-40ea4656})

#### Possible deadlocks with MySQL/MariaDB

When running under high load, and with more than one LLD worker involved, it is possible to run into a deadlock caused by an InnoDB error related to the row-locking strategy (see [upstream bug](https://github.com/mysql/mysql-server/commit/7037a0bdc83196755a3bf3e935cfb3c0127715d5)). The error has been fixed in MySQL since 8.0.29, but not in MariaDB. For more details, see [ZBX-21506](https://support.zabbix.com/browse/ZBX-21506).

[comment]: # ({/new-40ea4656})

[comment]: # ({a4844dc0-70c19e71})
#### 全局事件关联

如果第一个事件和第二个事件之间的时间间隔非常小，即半秒或更短，事件可能无法正确关联。

[comment]: # ({/a4844dc0-70c19e71})

[comment]: # ({8832de1b-215c95a7})
#### PostgreSQL 11 及更早版本的数据库支持的浮点数类型范围

PostgreSQL 11 及更早版本仅支持大约 -1.34E-154 到 1.34E+154 的浮点数范围。

[comment]: # ({/8832de1b-215c95a7})

[comment]: # ({6bcc4b75-dfc40df7})
#### NetBSD 8.0 及更新版本

在 NetBSD 8.X 和 9.X 上， Zabbix 各种进程可能会在启动时随机崩溃。这是由于默认堆栈大小（4MB）太小，需要执行以下命令来增加：

 · ulimit -s 10240

点击 [ZBX-18275](https://support.zabbix.com/browse/ZBX-18275) 查看相关问题报告。

[comment]: # ({/6bcc4b75-dfc40df7})

[comment]: # ({2f72fc70-3cf04fe3})
#### IPMI 检查

用 Debian 9 (stretch) 和 Ubuntu 16.04 (xenial) 之前版本的标准 OpenIPMI 库包运行 IPMI 检查会无法正常运行。要解决此问题，请重新编译 OpenIPMI 库并启用 OpenSSL，参考 [ZBX-6139](https://support.zabbix.com/browse/ZBX-6139)。

[comment]: # ({/2f72fc70-3cf04fe3})

[comment]: # ({854e33a1-8c5cdd23})
#### SSH 检查

如果从软件包（即 yum ）安装 libssh2 库，则某些 Linux 发行版（如 Debian、Ubuntu）不支持加密的私钥（带密码）。详情参考 [ZBX-4850](https://support.zabbix.com/browse/ZBX-4850) 。

在 CentOS 8 上使用带有 OpenSSH 8 的 libssh 0.9.x 时，SSH 检查可能偶尔会报错 “无法从 SSH 服务器读取数据”。这是由 libssh [问题](https://gitlab.com/libssh/libssh-mirror/-/merge_requests/101)  引起的 ([参考更详细的说明](https://bugs.libssh.org/T231))。预计该错误已在稳定的 libssh 0.9.5 版本中被修复。有关详细说明，另请参阅 [ZBX-17756](https://support.zabbix.com/browse/ZBX-17756) 。

[comment]: # ({/854e33a1-8c5cdd23})

[comment]: # ({a037b006-0c2fc2b9})
#### ODBC 检查

- ·  不要在针对 MariaDB connector library 编译的 Zabbix server 或 Zabbix proxy 上使用 MySQL unixODBC 驱动程序，反之亦然。如果可以，由于 [上游 bug](https://bugs.mysql.com/bug.php?id=73709)，最好避免使用与驱动程序相同的连接器。建议设置：

```{=html}
<!-- -->
```
 · PostgreSQL, SQLite or Oracle connector → MariaDB or MySQL unixODBC driver
 · MariaDB connector → MariaDB unixODBC driver
 · MySQL connector → MySQL unixODBC driver

请参阅 [ZBX-7665](https://support.zabbix.com/browse/ZBX-7665) 了解更多信息和可用的解决方案。

- · 从 Microsoft SQL Server 查询的 XML 数据在 Linux 和 UNIX 系统上可能会以各种方式被截断。

```{=html}
<!-- -->
```

- · 在 CentOS 8 上用  Oracle Instant Client for Linux 11.2.0.4.0 通过 ODBC 检查监控 Oracle 数据库会导致 Zabbix server 崩溃。该问题可以通过将 Oracle Instant Client 升级到 12.1.0.2.0、12.2.0.1.0、18.5.0.0.0 或 19 来解决。另请参见 [ZBX-18402](https://support.zabbix.com/browse/ZBX-18402)。

[comment]: # ({/a037b006-0c2fc2b9})

[comment]: # ({e6f23d51-1db730d3})
#### 监控项中的请求方法参数不正确

在 HTTP 检查中使用的 request_method 参数可能被错误地设置为 “1”，监控项的非默认值是由于从 Zabbix 4.0 之前的版本升级造成的。点击 [ZBX-19308](https://support.zabbix.com/browse/ZBX-19308) 查看解决方法。

[comment]: # ({/e6f23d51-1db730d3})


[comment]: # ({05a15e86-4713dff4})
#### Web 监控和 HTTP agent

由于 [上游 bug](https://bugzilla.redhat.com/show_bug.cgi?id=1057388)，在 Web 场景或 HTTP agent 中启用 “SSL  verify peer” 时，Zabbix server 在 CentOS 6、CentOS 7 或其他 Linux 发行版上可能存在内存泄漏（leaks memory）的问题。参考 [ZBX-10486](https://support.zabbix.com/browse/ZBX-10486)  获取更多信息和解决方案 。

[comment]: # ({/05a15e86-4713dff4})

[comment]: # ({51c34381-9cd7efe1})
#### 简单检查

在 v3.10 之前的  **fping**  版本中存在错误处理重复的回显重放数据包的 bug。可能会导致 `icmpping`, `icmppingloss`, `icmppingsec` 监控项故障。建议使用最新版本的 **fping**。参考 [ZBX-11726](https://support.zabbix.com/browse/ZBX-11726) 。

[comment]: # ({/51c34381-9cd7efe1})



















[comment]: # ({a043b298-a4574c73})
#### SNMP 检查

对于 OpenBSD 操作系统，如果在 Zabbix server 配置文件中设置了 SourceIP 参数，则 5.7.3（及之前）版本中 Net-SNMP 库的 use-after-free bug 会导致 Zabbix server 崩溃。其中一种解决办法是不设置 SourceIP 参数。同样的问题也存在于 Linux，但它不会导致 Zabbix server 停止工作。OpenBSD 上的 net-snmp 软件包的本地补丁已启用，并将与 OpenBSD 6.3 一起发布。

[comment]: # ({/a043b298-a4574c73})

[comment]: # ({5c590fce-d699f9d6})
#### SNMP 数据峰值

SNMP 监控数据中的峰值可能与某些物理因素有关，如电源中的电压峰值。详情点击 [ZBX-14318](https://support.zabbix.com/browse/ZBX-14318)。

[comment]: # ({/5c590fce-d699f9d6})

[comment]: # ({d3a7c1bb-7aeb682d})
#### SNMP trap

SNMP trap 所需的 “net-snmp-perl” 软件包已在 RHEL/CentOS 8.0-8.2 中删除，在 RHEL 8.3 中重新添加。

所以如果你使用的是 RHEL 8.0-8.2，最好的解决方案是升级到 RHEL 8.3；如果你使用的是 CentOS 8.0-8.2，您可以等待 CentOS 8.3 或使用 EPEL 源提供的软件包。

详情参阅 [ZBX-17192](https://support.zabbix.com/browse/ZBX-17192) 。

[comment]: # ({/d3a7c1bb-7aeb682d})

[comment]: # ({60953583-f46cb486})
#### Alerter 进程在 Centos/RHEL 7 中崩溃

在 Centos/RHEL 7 中遇到了 Zabbix server 的 alerter 进程崩溃的情况。有关详细信息，请参阅 [ZBX-10461](https://support.zabbix.com/browse/ZBX-10461) 。

[comment]: # ({/60953583-f46cb486})

[comment]: # ({ddb95a57-8ffad918})
#### 在 HP-UX 上编译 Zabbix agent

如果从常规的 HP-UX 软件包站点 http://hpux.connect.org.uk 安装 PCRE 库（例如 `pcre-8.42-ia64_64-11.31.depot` 文件），你将仅获得安装在 /usr/local/lib/hpux64 路径下的 64 位版本的库。

在这种情况下，为了成功编译 agent，需要为“配置”脚本使用自定义选项，例如：

 · CFLAGS="+DD64" ./configure --enable-agent --with-libpcre-include=/usr/local/include --with-libpcre-lib=/usr/local/lib/hpux64

[comment]: # ({/ddb95a57-8ffad918})

[comment]: # ({new-6e1fb8fe})
#### Flipping frontend locales

It has been observed that frontend locales may flip without apparent
logic, i. e. some pages (or parts of pages) are displayed in one
language while other pages (or parts of pages) in a different language.
Typically the problem may appear when there are several users, some of
whom use one locale, while others use another.

A known workaround to this is to disable multithreading in PHP and
Apache.

The problem is related to how setting the locale works [in
PHP](https://www.php.net/manual/en/function.setlocale): locale
information is maintained per process, not per thread. So in a
multi-thread environment, when there are several projects run by same
Apache process, it is possible that the locale gets changed in another
thread and that changes how data can be processed in the Zabbix thread.

For more information, please see related problem reports:

-   [ZBX-10911](https://support.zabbix.com/browse/ZBX-10911) (Problem
    with flipping frontend locales)
-   [ZBX-16297](https://support.zabbix.com/browse/ZBX-16297) (Problem
    with number processing in graphs using the `bcdiv` function of BC
    Math functions)

[comment]: # ({/new-6e1fb8fe})

[comment]: # ({17089747-81fe18ae})
#### PHP 7.3 opcache 配置

如果在 PHP 7.3 配置中启用了 “opcache”，第一次加载时 Zabbix 前端可能会显示一个空白屏幕。这是一个已注册的  [PHP bug](https://bugs.php.net/bug.php?id=78015)。要解决此问题，请在 PHP 配置（php.ini 文件）中将 "opcache.optimization\_level" 参数设置为 `0x7FFFBFDF` 。

[comment]: # ({/17089747-81fe18ae})

[comment]: # ({b4cf2813-4f3b73ce})
#### Graphs 图形

更改为夏令时 (DST) 会导致显示 X 轴标签时出现异常（例如日期重复、日期缺失等）。

[comment]: # ({/b4cf2813-4f3b73ce})

[comment]: # ({6a07a4f4-357fdb5b})
#### 日志文件监控

如果文件系统达到 100% 并且日志正在追加，则 `log[]` 和 `logrt[]` 监控项会从头重读日志文件，（参阅 [ZBX-10884](https://support.zabbix.com/browse/ZBX-10884) ）获取更多。

[comment]: # ({/6a07a4f4-357fdb5b})

[comment]: # ({new-82ff58c2})
#### Slow MySQL queries

Zabbix server generates slow select queries in case of non-existing
values for items. This is caused by a known
[issue](https://bugs.mysql.com/bug.php?id=74602) in MySQL 5.6/5.7
versions. A workaround to this is disabling the
index\_condition\_pushdown optimizer in MySQL. For an extended
discussion, see
[ZBX-10652](https://support.zabbix.com/browse/ZBX-10652).

[comment]: # ({/new-82ff58c2})


[comment]: # ({new-7ed39efb})
#### Slow configuration sync with Oracle

Configuration sync might be slow in Zabbix 6.0 installations with Oracle DB that have high number of items and item preprocessing steps.
This is caused by the Oracle database engine speed processing *nclob* type fields.

To improve performance, you can convert the field types from *nclob* to *nvarchar2* by manually applying the database patch [items_nvarchar_prepare.sql](/../assets/en/manual/installation/items_nvarchar_prepare.sql).
Note that this conversion will reduce the maximum field size limit from 65535 bytes to 4000 bytes
for item preprocessing parameters and item parameters such as *Description*, Script item's field *Script*,
HTTP agent item's fields *Request body* and *Headers*, Database monitor item's field *SQL query*.
Queries to determine template names that need to be deleted before applying the patch are provided in the patch as a comment. Alternatively, if MAX_STRING_SIZE is set you can change *nvarchar2(4000)* to *nvarchar2(32767)* in the patch queries to set the 32767 bytes field size limit.

For an extended discussion, see [ZBX-22363](https://support.zabbix.com/browse/ZBX-22363).

[comment]: # ({/new-7ed39efb})

[comment]: # ({0bd95e0a-b393528e})
#### API 登录

使用自定义脚本[方式](/manual/api/reference/user/login) 进行 `user.login` 登录而不执行  `user.logout`，会创建大量打开的用户会话。

[comment]: # ({/0bd95e0a-b393528e})

[comment]: # ({1419cf9a-17c4463f})
#### SNMPv3 trap 中的 IPv6 地址问题

由于 net-snmp bug，在 SNMP trap 中使用 SNMPv3 时可能无法正确显示 IPv6 地址。有关更多详细信息和可能的解决方法，请参阅 [ZBX-14541](https://support.zabbix.com/browse/ZBX-14541)。

[comment]: # ({/1419cf9a-17c4463f})

[comment]: # ({2e010717-d77627ce})
#### 裁剪了登录失败信息中的长 IPv6 IP 地址

登录失败的消息将仅显示存储的 IP 地址的前 39 个字符，这是由于数据库字段中的字符限制。这意味着超过 39 个字符的 IPv6 IP 地址将不能完整显示。

[comment]: # ({/2e010717-d77627ce})

[comment]: # ({1d36c16b-57420738})
#### Windows 的 Zabbix agent 

Zabbix agent 配置文件  (zabbix\_agentd.conf) 中设置非 DNS 性质的 `Server` 参数可能会增加 Windows 上 Zabbix agent 的响应时间。发生这种情况是因为 Windows DNS 缓存守护程序不会缓存 IPv4 地址的否定响应。但是会缓存  IPv6 地址的否定响应，因此可能的解决方法是在主机上禁用 IPv4。

[comment]: # ({/1d36c16b-57420738})

[comment]: # ({a062d4a5-40001075})
#### YAML 导出/导入

YAML [导出/导入](/manual/xml_export_import) 存在一些已知问题：

- ·  错误消息不会被翻译；
- ·  有时会无法导入带有 .yaml 文件扩展名的有效 JSON 文件；
- ·  日期如果不加引号会自动转换为 Unix 时间戳。

[comment]: # ({/a062d4a5-40001075})

[comment]: # ({c0114391-fcbf4bce})
#### 使用 NGINX 和 php-fpm 在 SUSE 上设置向导

在 SUSE 上使用 NGINX + php-fpm，前端设置向导将无法保存配置文件。这是由 /usr/lib/systemd/system/php-fpm.service 单元中的设置引起的，该设置阻止 Zabbix 写入 /etc。 (在 [PHP 7.4 中引入](https://bugs.php.net/bug.php?id=72510)).。

有两种解决方法可供选择：

- · 在 php-fpm systemd 单元中将 [ProtectSystem](https://www.freedesktop.org/software/systemd/man/systemd.exec.html#ProtectSystem=) 选项设置为 'true' 而不是 'full'。
- · 手动保存 /etc/zabbix/web/zabbix.conf.php 文件。

[comment]: # ({/c0114391-fcbf4bce})

[comment]: # ({c07636ea-8009b04b})
#### 在 Ubuntu 20 上使用 Chromium 运行 Zabbix web

尽管在大多数情况下，Zabbix Web 服务可以使用 Chromium 运行，但在 Ubuntu 20.04 上使用 Chromium 会导致以下错误：

 · Cannot fetch data: chrome failed to start:cmd_run.go:994:
 · WARNING: cannot create user data directory: cannot create "/var/lib/zabbix/snap/chromium/1564": mkdir /var/lib/zabbix: permission denied
 · Sorry, home directories outside of /home are not currently supported. See https://forum.snapcraft.io/t/11209 for details.

发生此错误是因为 `/var/lib/zabbix` 已经被用作用户 'zabbix' 的主目录。

[comment]: # ({/c07636ea-8009b04b})

[comment]: # ({0c298d6c-1f99c5d8})
#### MySQL 自定义错误代码

如果在 Azure 上安装 Zabbix 和  MySQL，Zabbix 日志中可能会出现不明确的报错信息 *\[9002\] Some errors occurred* 。这种通用错误文本由数据库发送到 Zabbix server 或 proxy。若要获取有关错误原因的详细信息，请查看 Azure 日志。

[comment]: # ({/0c298d6c-1f99c5d8})

[comment]: # ({59756107-eb422070})
#### 切换到 PCRE2 后正则表达式无效

在 Zabbix 6.0 中添加了对 PCRE2 的支持。尽管 PCRE 仍受支持 ，但 RHEL/CentOS 7 及更高版本、SLES（所有版本）、Debian 9 及更高版本、Ubuntu 16.04 及更高版本的 Zabbix 安装包已更新为使用 PCRE2。尽管有很多好处，但是切换到 PCRE2 可能导致某些现有的 PCRE 正则表达式无效或无法正确解析。特别是 *\^[\\w-\\.]* 表达式会受影响。为了使该正则表达式再次生效且不影响语义，请将表达式更改为 *\^[-\\w\\.]* 。 这是因为 PCRE2 将破折号视为分隔符，在字符类中创建了一个范围。
以下 Zabbix 安装包已更新为使用 PCRE2：RHEL/CentOS 7 及更新版本、SLES（所有版本）、Debian 9 及更新版本、Ubuntu 16.04 及更新版本。

[comment]: # ({/59756107-eb422070})

[comment]: # ({new-093b78e2})
#### Geomap widget error 

The maps in the Geomap widget may not load correctly, if you have upgraded from an older Zabbix version with NGINX and didn't switch to the new NGINX configuration file during the upgrade. 

To fix the issue, you can  discard the old configuration file, use the configuration file from the current version package and reconfigure it as described in the [download instructions](https://www.zabbix.com/download?zabbix=6.0&os_distribution=red_hat_enterprise_linux&os_version=8&db=mysql&ws=nginx) in section *e. Configure PHP for Zabbix frontend*.

Alternatively, you can manually edit an existing NGINX configuration file (typically, */etc/zabbix/nginx.conf*). To do so, open the file and locate the following block: 

    location ~ /(api\/|conf[^\.]|include|locale|vendor) {
            deny            all;
            return          404;
    }

Then, replace this block with: 

    location ~ /(api\/|conf[^\.]|include|locale) {
            deny            all;
            return          404;
    }

    location /vendor {
            deny            all;
            return          404;
    }


[comment]: # ({/new-093b78e2})
