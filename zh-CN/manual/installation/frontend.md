[comment]: # translation:outdated

[comment]: # ({dcafacfe-9cceae9f})
# 6 Web界面安装

本章节提供有关Zabbx Web界面的部署步骤说明。Zabbix 前端是由PHP语言编写，所以其网页服务的运行需要支持PHP语言的网站服务器。

[comment]: # ({/dcafacfe-9cceae9f})

[comment]: # ({new-0c649c0e})
:::notetip
You can find out more about setting up SSL for Zabbix frontend by referring to these [best practices](/manual/installation/requirements/best_practices).
:::

[comment]: # ({/new-0c649c0e})

[comment]: # ({93e37c87-6b173526})
#### 欢迎主界面

在浏览器中输入Zabbix 前端的URL来进入主界面。通过依赖包的方式对Zabbix进行安装，其URL的输入格式会略有不同，相关格式如下所示：

-   对于 Apache: *http://<server\_ip\_or\_name>/zabbix*
-   对于 Nginx: *http://<server\_ip\_or\_name>*

根据安装方式输入正确的URL后，您将会进入到前端安装的向导程序。

使用*系统默认语言*下拉菜单，更改系统默认语言，并以所选语言继续安装过程（非必选）。详细信息，请参考 [Installation of additional frontend languages](/manual/appendix/install/locales).

![](../../../assets/en/manual/installation/install_1.png){width="550"}

[comment]: # ({/93e37c87-6b173526})

[comment]: # ({a79720e5-2458c643})
#### 先决条件检查

确保满足所有软件先决条件。

![](../../../assets/en/manual/installation/install_2.png){width="550"}

|先决条件|最小值|描述|
|-------------|-------------|------------|
|*PHP 版本*|7.2.5|<|
|*PHP 内存\_limit 选项*|128MB|在 php.ini 中：<br>内存\_limit = 128M|
|*PHP post\_max\_size 选项*|16MB|在 php.ini 中：<br>post\_max\_size = 16M|
|*PHP 上传\_max\_filesize 选项*|2MB|在 php.ini 中：<br>上传\_max\_filesize = 2M|
|*PHP max\_execution\_time 选项*|300 秒（允许值 0 和 -1）|在 php.ini 中：<br>max\_execution\_time = 300|
|*PHP max\_input\_time 选项*|300 秒（允许值 0 和 -1）|在 php.ini 中：<br>max\_input\_time = 300|
|*PHP session.auto\_start 选项*|必须禁用|在 php.ini:<br>session.auto\_start = 0|
|*数据库支持*|其中之一：MySQL、Oracle、PostgreSQL。|必须安装以下模块之一：<br>mysql、oci8、pgsql|
|*bcmath*|<|php-bcmath|
|*mbstring*|<|php-mbstring|
|*PHP mbstring.func\_overload 选项*|必须禁用|在 php.ini:<br>mbstring.func\_overload = 0|
|*sockets*|<|php-net-socket.需要用户脚本支持。|
|*gd*|2.0.28|php-gd. PHP GD 扩展必须支持 PNG 图像 (*--with-png-dir*)、JPEG (*--with-jpeg-dir*) 图像和 FreeType 2 (*--with-freetype-dir*)。|
|*libxml*|2.6.15|php-xml|
|*xmlwriter*|<|php-xmlwriter|
|*xmlreader*|<|php-xmlreader|
|*ctype*|<|php-ctype|
|*会话*|<|php-会话|
|*gettext*|<|php-gettext<br>自 Zabbix 2.2.1 起，PHP gettext 扩展不是安装 Zabbix 的强制要求。如果未安装 gettext，前端将照常工作，但是翻译将不可用。|

可选的先决条件也会罗列在列表中。一个失败的可选先决条件会显示为橙色，并具有*Warning*的状态。如果可选先决条件不满足，安装程序也可以继续进行。

::: 重要注意事项
若需要更改 Apache 的用户或用户组，则必须验证会话文件夹的权限。否则 Zabbix 的安装将无法继续。
:::

[comment]: # ({/a79720e5-2458c643})

[comment]: # ({c5900b6f-879dc06f})
#### 配置数据库连通性

请在该页面输入连接到数据库所需的详细信息。在创建与数据库的连接前，Zabbix数据库必须先被创建。

![](../../../assets/en/manual/installation/install_3a.png){width="550"}

 若选择 *Database TLS encryption* 选项，则需要在出现的信息栏中填写有关 [configuring the TLS connection](/manual/appendix/install/db_encrypt)的配置信息（该功能仅限数据库类型为 MySQL 或 PostgreSQL）。
若选择 HashiCorp Vault 选项来进行凭据存储，请在附加的信息栏中输入相关信息，用以说明 Vault API 端点、隐藏路径以及身份验证令牌：

![](../../../assets/en/manual/installation/install_3b.png){width="550"}

[comment]: # ({/c5900b6f-879dc06f})

[comment]: # ({f559d1f1-98277238})
#### 配置

对Zabbix服务器进行命名的配置为可选配置。该配置一旦提交，设定的服务器名称就会显示在网页的菜单栏和页面标题中。

配置默认[time zone](/manual/web_interface/time_zone#overview)和前端的主题。

![](../../../assets/en/manual/installation/install_4.png){width="550"}

[comment]: # ({/f559d1f1-98277238})

[comment]: # ({19497ee6-42398398})
#### 预安装总概

查看配置概要。

![](../../../assets/en/manual/installation/install_5.png){width="550"}

[comment]: # ({/19497ee6-42398398})

[comment]: # ({663bd709-1124dd9e})
####安装

若采用从源代码安装 Zabbix，请下载配置文件并将其 Zabbix PHP 文件复制到所在网站服务器 HTML 文件子目录中的 conf/ 下。

![](../../../assets/en/manual/installation/install_6.png){width="550"}

![](../../../assets/en/manual/installation/saving_zabbix_conf.png){width="350"}

::: 提示
若网站服务器用户对 conf/ 目录具有写入权限，则配置文件将自动保存，并且可以立即执行下一步。
:::

完成安装。

![](../../../assets/en/manual/installation/install_7.png){width="550"}

[comment]: # ({/663bd709-1124dd9e})

[comment]: # ({86c2472e-d59dc4b9})
#### 登录

Zabbix 前端已准备就绪！ 默认用户名是**Admin**，密码**zabbix**。

![](../../../assets/en/manual/quickstart/login.png){width="350"}

继续 [getting started with Zabbix](/manual/quickstart/login).

[comment]: # ({/86c2472e-d59dc4b9})
