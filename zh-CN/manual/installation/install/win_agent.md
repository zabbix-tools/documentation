[comment]: # translation:outdated

[comment]: # ({c1c0a7f4-1f267b18})
# 在Windows上构建Zabbix agent

[comment]: # ({/c1c0a7f4-1f267b18})

[comment]: # ({1de8bcec-2cee80d5})
#### 概述

本节将演示如何从有或没有TLS的源构建 Zabbix Windows agent二进制文件。

[comment]: # ({/1de8bcec-2cee80d5})

[comment]: # ({a8a76d1c-8b346a36})

#### 编译OpenSSL

接下来的步骤将助你在MS Windows 10（64位）的源中编译OpenSSL。

1.  编译OpenSSL你需要在Windows机器上安装：
    1.  C compiler (e.g. VS 2017 RC),
    2.  NASM (<https://www.nasm.us/>),
    3.  Perl (e.g. Strawberry Perl from <http://strawberryperl.com/>),
    4.  Perl module Text::Template (cpan Text::Template).
2.  获取 OpenSSL 源代码 <https://www.openssl.org/>。这里用OpenSSL 1.1.1 版本。
3.  解压 OpenSSL 源，例如，解压在 E:\\openssl-1.1.1。
4.  打开命令行窗口。例如，VS 2017 RC的 x64 原生工具命令提示符。
5.  至 OpenSSL 源目录, 例如，E:\\openssl-1.1.1。
    1.  验证 NASM 能被找到：`e:\openssl-1.1.1> nasm --version
        NASM version 2.13.01 compiled on May  1 2017
        `
6.  配置OpenSSL，例如：`e:\openssl-1.1.1> perl E:\openssl-1.1.1\Configure VC-WIN64A no-shared no-capieng no-srp no-gost no-dgram no-dtls1-method no-dtls1_2-method  --api=1.1.0 --prefix=C:\OpenSSL-Win64-111-static --openssldir=C:\OpenSSL-Win64-111-static
    `
    -   请注意选项 'no-shared'：如果使用 'no-shared' 那么OpenSSL静态库 libcrypto.lib和libssl.lib 将
        '自给自足'，生成的Zabbix二进制文件本身将包含OpenSSL ，不需要外部的OpenSSL DLLs。
        优点： Zabbix二进制文件可以复制到其他没有OpenSSSL库的Windows机器上。 
        缺点：当新的OpenSSL bugfix 版本发布时，需要重新编译并安装Zabbix agent。
    -   如果不使用'no-shared'，那么静态库 libcrypto.lib和 libssl.lib 会在运行时使用OpenSSL DLLs 。
    - 优点：当新的OpenSSL bugfix版本发布时，你可能只需要升级OpenSSL DLLs不用重新编译Zabbix agent。
    - 缺点：复制Zabbix agent到另一个机器时，需要同时复制OpenSSL DLLs。
7.  编译OpenSSL，运行测试，安装：`e:\openssl-1.1.1> nmake
    e:\openssl-1.1.1> nmake test
    ...
    All tests successful.
    Files=152, Tests=1152, 501 wallclock secs ( 0.67 usr +  0.61 sys =  1.28 CPU)
    Result: PASS
    e:\openssl-1.1.1> nmake install_sw
    `'install\_sw' 仅安装软件组件（例如 库，头文件，但不安装文档）。如果你希望安装所有的文件，请用 "nmake install"。

[comment]: # ({/a8a76d1c-8b346a36})

[comment]: # ({6629df43-22b944ad})
#### 编译PCRE

1.  从 pcre.org (<https://github.com/PhilipHazel/pcre2/releases/download/pcre2-10.39/pcre2-10.39.zip>) 存储库中下载PCRE 或 PCRE2 (Zabbix 6.0以上支持) 库： 
2.  提取到目录*E:\\pcre2-10.39*
3.  从<https://cmake.org/download/> 安装CMake，安装过程中选择： 确保 cmake\\bin 在你的路径的 (测试版本3.9.4).
4.  创建一个新的空的构建目录，最好是源目录的子目录。例如*E:\\pcre2-10.39\\build*.
5.  打开命令行窗口，例如 VS 2017上的x64原生工具命令提示符 ，并且从该外部环境运行cmake-gui。不要试图从窗口开始菜单启动Cmake，因为这可能会导致错误。 
6.  分别为源目录和构建目录输入 *E:\\pcre2-10.39* 和*E:\\pcre2-10.39\\build* 。
7.  点击 "Configure" 按钮。
8.  为此项目指定生成器时，请选择 "NMake Makefiles"。
9.  创建一个新的空的安装目录。例如，*E:\\pcre2-10.39-install*。
10. GUI 将列出几个配置选项。确保选择了以下几个选项：
    -   **PCRE\_SUPPORT\_UNICODE\_PROPERTIES** ON
    -   **PCRE\_SUPPORT\_UTF** ON
    -   **CMAKE\_INSTALL\_PREFIX** *E:\\pcre2-10.39-install*
11. 再次点击 "Configure" 。The adjacent "Generate" button should now be active.
12. 点击 "Generate".
13. 如果出现错误，建议在尝试重复构建CMake的过程中删除CMake缓存。
    在CMake GUI中，缓存可以通过选择 "File > Delete Cache"来删除。
14. 构建目录现在应该包含了一个可用的构建系统 - *Makefile*.
15. 打开命令行窗口，例如 VS 2017上的x64原生工具命令提示符 ，导航到上面提到的 *Makefile* 。
16. 运行 NMake 命令： `E:\pcre2-10.39\build> nmake install
    `

[comment]: # ({/6629df43-22b944ad})

[comment]: # ({763db9cd-30b6b8b5})
#### 编译 Zabbix

以下步骤将助你从MS Windows 10 (64位)上的源码中编译Zabbix。在编译有或没有TLS支持的Zabbix时，唯一显著的区别在步骤4。

1.  在Linux机器上，检查git源代码：`` $ git clone https://git.zabbix.com/scm/zbx/zabbix.git
    $ cd zabbix
    $ ./bootstrap.sh
    $ ./configure --enable-agent --enable-ipv6 --prefix=`pwd`
    $ make dbschema
    $ make dist
     ``
2.  在Windows机器上复制和解压归档文件，例如 zabbix-4.4.0.tar.gz。
3.  我们假设源代码在 e:\\zabbix-4.4.0中。打开命令行窗口，例如 VS 2017 RC中的 x64原生工具命令提示符。转至 E:\\zabbix-4.4.0\\build\\win32\\project。
4.  编译 zabbix\_get， zabbix\_sender 和 zabbix\_agent。
    -   无TLS：
        `E:\zabbix-4.4.0\build\win32\project> nmake /K PCREINCDIR=E:\pcre2-10.39-install\include PCRELIBDIR=E:\pcre2-10.39-install\lib
        `
    -   有TLS：
        `E:\zabbix-4.4.0\build\win32\project> nmake /K -f Makefile_get TLS=openssl TLSINCDIR=C:\OpenSSL-Win64-111-static\include TLSLIBDIR=C:\OpenSSL-Win64-111-static\lib PCREINCDIR=E:\pcre2-10.39-install\include PCRELIBDIR=E:\pcre2-10.39-install\lib
        E:\zabbix-4.4.0\build\win32\project> nmake /K -f Makefile_sender TLS=openssl TLSINCDIR="C:\OpenSSL-Win64-111-static\include TLSLIBDIR="C:\OpenSSL-Win64-111-static\lib" PCREINCDIR=E:\pcre2-10.39-install\include PCRELIBDIR=E:\pcre2-10.39-install\lib
        E:\zabbix-4.4.0\build\win32\project> nmake /K -f Makefile_agent TLS=openssl TLSINCDIR=C:\OpenSSL-Win64-111-static\include TLSLIBDIR=C:\OpenSSL-Win64-111-static\lib PCREINCDIR=E:\pcre2-10.39-install\include PCRELIBDIR=E:\pcre2-10.39-install\lib
        `
5.  新的二进制文件位于 e:\\zabbix-4.4.0\\bin\\win64。由于 OpenSSL 是用 'no-shared' 选项编译的，Zabbix二进制文件本身包含OpenSSL，可以复制到其他没有OpenSSL的机器中。

[comment]: # ({/763db9cd-30b6b8b5})

[comment]: # ({6bfb6d24-c8fe4039})
#### 用LibreSSL编译Zabbix 

该过程类似于使用OpenSSL编译，但是你需要对位于`build\win32\project` 目录中的文件进行一些小的改变：

      * In ''Makefile_tls'' delete ''/DHAVE_OPENSSL_WITH_PSK''. i.e. find <code>

CFLAGS = $(CFLAGS) /DHAVE\_OPENSSL
/DHAVE\_OPENSSL\_WITH\_PSK</code>然后用`CFLAGS =    $(CFLAGS) /DHAVE_OPENSSL`进行替换

      * In ''Makefile_common.inc'' add ''/NODEFAULTLIB:LIBCMT'' i.e. find <code>

/MANIFESTUAC:"level='asInvoker' uiAccess='false'" /DYNAMICBASE:NO
/PDB:$(TARGETDIR)\\$(TARGETNAME).pdb</code>然后用
`/MANIFESTUAC:"level='asInvoker' uiAccess='false'" /DYNAMICBASE:NO /PDB:$(TARGETDIR)\$(TARGETNAME).pdb /NODEFAULTLIB:LIBCMT`进行替换

[comment]: # ({/6bfb6d24-c8fe4039})
