[comment]: # translation:outdated

[comment]: # ({d5a30f21-22c598e9})
# 在Windows中构建Zabbix agent 2 

[comment]: # ({/d5a30f21-22c598e9})

[comment]: # ({042c565e-84651997})
#### 概述

本节将演示如何从源代码构建Zabbix agent 2 (Windows)。

[comment]: # ({/042c565e-84651997})

[comment]: # ({0d2d458c-2a4da91c})
#### 安装MinGW编译器

1\. 下载带有SJLJ (设置跳转/长跳转) 异常处理和窗口线程的MinGW-w64 (例如 *x86\_64-8.1.0-release-win32-sjlj-rt\_v6-rev0.7z*)\
2. 提取并移动到 *c:\\mingw*\
3. 设置环境变量

    @echo off
    set PATH=%PATH%;c:\mingw\bin
    cmd

编译时使用Windows提示符代替MinGW提供的MSYS终端。

[comment]: # ({/0d2d458c-2a4da91c})

[comment]: # ({4c233338-dec81522})
#### 编译 PCRE 开发库
以下说明将编译并安装*c:\\dev\\pcre*中的64位PCRE
库和*c:\\dev\\pcre32* 的32位库:

1\. 从 pcre.org(<http://ftp.pcre.org/pub/pcre/>) 下载PCRE 8.XX 版本库，然后提取\
2. 打开 *cmd* 并导航到提取的源

[comment]: # ({/4c233338-dec81522})

[comment]: # ({a071d73b-5def516d})
##### 构建64位 PCRE

1\. 删除就配置/缓存（如果有）：

    del CMakeCache.txt
    rmdir /q /s CMakeFiles

2\. 运行 cmake (CMake 可从这里安装<https://cmake.org/download/>):

    cmake -G "MinGW Makefiles" -DCMAKE_C_COMPILER=gcc -DCMAKE_C_FLAGS="-O2 -g" -DCMAKE_CXX_FLAGS="-O2 -g" -DCMAKE_INSTALL_PREFIX=c:\dev\pcre

3\. 接下来，运行：

    mingw32-make clean
    mingw32-make install

[comment]: # ({/a071d73b-5def516d})

[comment]: # ({3beca240-04b79926})
##### 构建32位PCRE

1\. 运行：

    mingw32-make clean

2\. 删除 *CMakeCache.txt*：

    del CMakeCache.txt
    rmdir /q /s CMakeFiles

3\. 运行cmake：

    cmake -G "MinGW Makefiles" -DCMAKE_C_COMPILER=gcc -DCMAKE_C_FLAGS="-m32 -O2 -g" -DCMAKE_CXX_FLAGS="-m32 -O2 -g" -DCMAKE_EXE_LINKER_FLAGS="-Wl,-mi386pe" -DCMAKE_INSTALL_PREFIX=c:\dev\pcre32

4\. 接下来，运行：

    mingw32-make install

[comment]: # ({/3beca240-04b79926})

[comment]: # ({a4e29abf-80e7f73a})
#### 安装OpenSSL开发库

1\. 从 <https://curl.se/windows/>\ 下载32和64位版本
2. 相应地将文件提取到*c:\\dev\\openssl32* 和*c:\\dev\\openssl*。\
3. 然后删除提取的*\*.dll.a* (dll call wrapper libraries)
，因为MinGW在静态库前会优先考虑它们。

[comment]: # ({/a4e29abf-80e7f73a})

[comment]: # ({6509a8b6-f4af7b47})
#### 编译Zabbix agent 2

[comment]: # ({/6509a8b6-f4af7b47})

[comment]: # ({a0ec125f-5f096f18})
##### 32位

打开 MinGW 环境（Windows命令提示符）并导航至Zabbix源树中的*build/mingw* 目录。

运行：

    mingw32-make clean
    mingw32-make ARCH=x86 PCRE=c:\dev\pcre32 OPENSSL=c:\dev\openssl32

[comment]: # ({/a0ec125f-5f096f18})

[comment]: # ({a57bfe7d-f7876f1d})
##### 64位

打开MinGW环境（Windows命令提示符）并导航至Zabbix源树目录中的*build/mingw*。

运行：

    mingw32-make clean
    mingw32-make PCRE=c:\dev\pcre OPENSSL=c:\dev\openssl

::: 注意
32和64位 版本都可以构建在64位的平台上，但是32位平台只能构建32位版本。
在32位平台上运行时，请遵循64位版本在64位平台上运行的步骤。
:::

[comment]: # ({/a57bfe7d-f7876f1d})
