[comment]: # translation:outdated

[comment]: # ({6cd5e3d4-0535e4cb})
# 在 macOS 上构建 Zabbix agent

[comment]: # ({/6cd5e3d4-0535e4cb})

[comment]: # ({d3e03a28-161fafaa})
#### 概述

本节演示如何从包含或不包含 TLS 的源代码构建 Zabbix macOS agent 二进制文件。

[comment]: # ({/d3e03a28-161fafaa})

[comment]: # ({8feb35b1-7e7224fc})
#### 必要条件

您将需要命令行开发人员工具（不需要Xcode），Automake，pkg-config和PCRE（v8.x）或PCRE2（v10.x）。如果要使用TLS构建 agent 二进制文件，则还需要 OpenSSL 或 GnuTLS。

要安装 Automake 和 pkg-config ，您将需要来自 <https://brew.sh/> 的软件包管理器。要安装它，请打开终端并运行以下命令：

    $ /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

然后安装 Automake 和 pkg-config：

    $ brew install automake
    $ brew install pkg-config

如何准备 PCRE、OpenSSL 和 GnuTLS 库取决于它们如何链接到 agent。
如果您打算在已具有这些库的 macOS 计算机上运行代理二进制文件，则可以使用 Homebrew 提供的预编译库。这些通常是 macOS 机器，它们使用 Homebrew 来构建 Zabbix agent 二进制文件或用于其他目的。

如果 agent 的二进制文件将在没有共享版本的库的 macOS 计算机上使用，则应从源代码编译静态库，并将 Zabbix agent与它们链接。

[comment]: # ({/8feb35b1-7e7224fc})

[comment]: # ({f5b1e753-5e97a918})
#### 使用共享库构建 agent 二进制文件

安装 PCRE2 (如果需要的话，在下面的命令中将 *pcre2* 替换为 *pcre* )：

    $ brew install pcre2

使用TLS构建时，请安装OpenSSL和/或GnuTLS：

    $ brew install openssl
    $ brew install gnutls

下载 Zabbix 源代码：

    $ git clone https://git.zabbix.com/scm/zbx/zabbix.git

不使用 TLS 构建 agent：

    $ cd zabbix
    $ ./bootstrap.sh
    $ ./configure --sysconfdir=/usr/local/etc/zabbix --enable-agent --enable-ipv6
    $ make
    $ make install

使用 OpenSSL 构建 agent：

    $ cd zabbix
    $ ./bootstrap.sh
    $ ./configure --sysconfdir=/usr/local/etc/zabbix --enable-agent --enable-ipv6 --with-openssl=/usr/local/opt/openssl
    $ make
    $ make install

使用 GnuTLS 构建 agent：

    $ cd zabbix-source/
    $ ./bootstrap.sh
    $ ./configure --sysconfdir=/usr/local/etc/zabbix --enable-agent --enable-ipv6 --with-gnutls=/usr/local/opt/gnutls
    $ make
    $ make install

[comment]: # ({/f5b1e753-5e97a918})

[comment]: # ({57d3c5cb-90fe7eb9})
#### 使用不带 TLS 的静态库构建 agent 二进制文件

让我们假设PCRE静态库将安装在 `$HOME/static-libs` 中，我们将使用 PCRE2 10.39。

    $ PCRE_PREFIX="$HOME/static-libs/pcre2-10.39"

下载并构建具有 Unicode 支持的PCRE：

    $ mkdir static-libs-source
    $ cd static-libs-source
    $ curl --remote-name https://github.com/PhilipHazel/pcre2/releases/download/pcre2-10.39/pcre2-10.39.tar.gz
    $ tar xf pcre2-10.39.tar.gz
    $ cd pcre2-10.39
    $ ./configure --prefix="$PCRE_PREFIX" --disable-shared --enable-static --enable-unicode-properties
    $ make
    $ make check
    $ make install

下载 Zabbix 源代码并构建 agent：

    $ git clone https://git.zabbix.com/scm/zbx/zabbix.git
    $ cd zabbix
    $ ./bootstrap.sh
    $ ./configure --sysconfdir=/usr/local/etc/zabbix --enable-agent --enable-ipv6 --with-libpcre2="$PCRE_PREFIX"
    $ make
    $ make install

[comment]: # ({/57d3c5cb-90fe7eb9})

[comment]: # ({c6807ef0-b985c38b})
#### 使用 OpenSSL 构建带有静态库的 agent 二进制文件

构建OpenSSL时，建议在成功构建后运行`make test`。 即使构建成功，测试有时也会失败。 在这种情况下，应进行研究并解决问题，然后再继续。

让我们假设PCRE和OpenSSL静态库将安装在“$HOME/static-libs”中。我们将使用 PCRE2 10.39 和 OpenSSL 1.1.1a。

    $ PCRE_PREFIX="$HOME/static-libs/pcre2-10.39"
    $ OPENSSL_PREFIX="$HOME/static-libs/openssl-1.1.1a"

让我们在 `static-libs-source` 中构建静态库：

    $ mkdir static-libs-source
    $ cd static-libs-source

下载并构建具有 Unicode 支持的PCRE：

    $ curl --remote-name https://github.com/PhilipHazel/pcre2/releases/download/pcre2-10.39/pcre2-10.39.tar.gz
    $ tar xf pcre2-10.39.tar.gz
    $ cd pcre2-10.39
    $ ./configure --prefix="$PCRE_PREFIX" --disable-shared --enable-static --enable-unicode-properties
    $ make
    $ make check
    $ make install
    $ cd ..

下载并构建 OpenSSL：

    $ curl --remote-name https://www.openssl.org/source/openssl-1.1.1a.tar.gz
    $ tar xf openssl-1.1.1a.tar.gz
    $ cd openssl-1.1.1a
    $ ./Configure --prefix="$OPENSSL_PREFIX" --openssldir="$OPENSSL_PREFIX" --api=1.1.0 no-shared no-capieng no-srp no-gost no-dgram no-dtls1-method no-dtls1_2-method darwin64-x86_64-cc
    $ make
    $ make test
    $ make install_sw
    $ cd ..

下载 Zabbix 源代码并构建 agent：

    $ git clone https://git.zabbix.com/scm/zbx/zabbix.git
    $ cd zabbix
    $ ./bootstrap.sh
    $ ./configure --sysconfdir=/usr/local/etc/zabbix --enable-agent --enable-ipv6 --with-libpcre2="$PCRE_PREFIX" --with-openssl="$OPENSSL_PREFIX"
    $ make
    $ make install

[comment]: # ({/c6807ef0-b985c38b})

[comment]: # ({7c7677ea-8165604b})
#### 使用带有 GnuTLS 的静态库构建 agent 二进制文件

GnuTLS 依赖于 Nettle 加密后端 和 GMP 算法库。
本文将使用 Nettle 中包含的 mini-gmp，而不是使用完整的GMP库。

构建 GnuTLS 和 Nettle 时，建议在成功构建后运行`make check`。 即使构建成功，测试有时也会失败。 在这种情况下，应进行研究并解决问题，然后再继续。


让我们假设 PCRE、Nettle 和 GnuTLS 静态库 将被安装在 `$HOME/static-libs`。 我们将使用 PCRE2 10.39、Nettle 3.4.1 和 GnuTLS 3.6.5。

    $ PCRE_PREFIX="$HOME/static-libs/pcre2-10.39"
    $ NETTLE_PREFIX="$HOME/static-libs/nettle-3.4.1"
    $ GNUTLS_PREFIX="$HOME/static-libs/gnutls-3.6.5"

让我们在 `static-libs-source` 中构建静态库：

    $ mkdir static-libs-source
    $ cd static-libs-source

下载并构建 Nettle:

    $ curl --remote-name https://ftp.gnu.org/gnu/nettle/nettle-3.4.1.tar.gz
    $ tar xf nettle-3.4.1.tar.gz
    $ cd nettle-3.4.1
    $ ./configure --prefix="$NETTLE_PREFIX" --enable-static --disable-shared --disable-documentation --disable-assembler --enable-x86-aesni --enable-mini-gmp
    $ make
    $ make check
    $ make install
    $ cd ..

下载并构建 GnuTLS:

    $ curl --remote-name https://www.gnupg.org/ftp/gcrypt/gnutls/v3.6/gnutls-3.6.5.tar.xz
    $ tar xf gnutls-3.6.5.tar.xz
    $ cd gnutls-3.6.5
    $ PKG_CONFIG_PATH="$NETTLE_PREFIX/lib/pkgconfig" ./configure --prefix="$GNUTLS_PREFIX" --enable-static --disable-shared --disable-guile --disable-doc --disable-tools --disable-libdane --without-idn --without-p11-kit --without-tpm --with-included-libtasn1 --with-included-unistring --with-nettle-mini
    $ make
    $ make check
    $ make install
    $ cd ..

下载 Zabbix 源代码并构建 agent：

    $ git clone https://git.zabbix.com/scm/zbx/zabbix.git
    $ cd zabbix
    $ ./bootstrap.sh
    $ CFLAGS="-Wno-unused-command-line-argument -framework Foundation -framework Security" \
    > LIBS="-lgnutls -lhogweed -lnettle" \
    > LDFLAGS="-L$GNUTLS_PREFIX/lib -L$NETTLE_PREFIX/lib" \
    > ./configure --sysconfdir=/usr/local/etc/zabbix --enable-agent --enable-ipv6 --with-libpcre2="$PCRE_PREFIX" --with-gnutls="$GNUTLS_PREFIX"
    $ make
    $ make install

[comment]: # ({/7c7677ea-8165604b})
