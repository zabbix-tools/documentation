[comment]: # translation:outdated

[comment]: # ({27557011-7ef4b2b9})
# 安全设置 Zabbix 的最佳方式

[comment]: # ({/27557011-7ef4b2b9})

[comment]: # ({06862bec-b9719f19})
#### 概述

本章节内容提供了对于Zabbix来说，可采用更加安全的安装方式。

实现Zabbix的功能并不局限于该范例中所涉及的内容。但建议用户可通过参考该配置方式为Zabbix提供更好的系统安全性。

[comment]: # ({/06862bec-b9719f19})

[comment]: # ({new-cebf8306})
### Access control

[comment]: # ({/new-cebf8306})

[comment]: # ({2b8f70af-dc572a02})
#### 最小权限原则

ZZabbix应该始终遵守最小权限原则。 该原则意味着用户的账号（在Zabbix前端）或流程用户（对于Zabbix server/proxy或agent）仅具有执行预期功能所必需的权限。 换句话说，用户账号应始终以尽可能小的权限运行。
::: 重要事项
赋予“zabbix”用户提供额外权限将允许其访问配置文件，并执行可能损害基础架构整体安全性的操作。
:::

最小权限原则，应考虑 Zabbix [前端用户类型](/manual/config/users_and_usergroups/permissions)。 重要的是要理解，虽然“管理员”用户类型的权限低于“超级管理员”用户类型，但它具有允许管理配置和执行自定义脚本的管理权限。


::: 注意事项
某些信息甚至适用于非特权用户。例如，虽然 *Administration* → *Scripts* 不适用于非超级管理员，但脚本本身可通过使用 Zabbix API 来进行检索。 应使用限制脚本权限和不添加敏感信息（如访问凭据等）来避免暴露全局脚本中可用的敏感信息。
:::

[comment]: # ({/2b8f70af-dc572a02})

[comment]: # ({9ef87153-1631be73})
#### Zabbix agent 的安全用户

在默认配置中，Zabbix server 和 Zabbix agent 进程共享一个“zabbix”用户。若希望确保 Zabbix agent 无法访问 Zabbix server配置中的敏感信息（例：数据库登录信息），则应以不同的用户身份运行Zabbix agent：

1.  创建一个安全用户；``
2.  在 Zabbix agent 的  [configuration file](/manual/appendix/config/zabbix_agentd) 中指定此用户（修改 'User' 参数）；
3.  以管理员权限的用户重启 Zabbix agent。此后权限将赋予给先前指定的用户。

[comment]: # ({/9ef87153-1631be73})

[comment]: # ({7cdaf899-40e70f9e})
#### UTF-8 编码

UTF-8 是 Zabbix 唯一支持的编码类型。众所周知，应用该类型编码可使Zabbix正常运转且没有任何安全漏洞。请用户注意，如果使用其它一些编码类型，我们发现会出现许多安全问题。

[comment]: # ({/7cdaf899-40e70f9e})

[comment]: # ({new-62eeb2ec})
### Windows installer paths
When using Windows installers, it is recommended to use default paths provided by the installer as using custom paths without proper permissions could compromise the security of the installation.

[comment]: # ({/new-62eeb2ec})

[comment]: # ({7514dd22-f8f5054e})
#### 为 Zabbix 前端设置 SSL

在 RHEL/Centos 操作系统上，安装 mod\_ssl 包：

    yum install mod_ssl

为 SSL keys 创建目录：

    mkdir -p /etc/httpd/ssl/private
    chmod 700 /etc/httpd/ssl/private

创建 SSL 证书：

    openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/httpd/ssl/private/apache-selfsigned.key -out /etc/httpd/ssl/apache-selfsigned.crt

请用户根据需求适当填写下列提示内容。最重要的参数是请求 Common Name 参数。
您需要输入要与服务器关联的域名。 如果您没有域名，则可以输入公共IP地址。
下面将使用 *example.com*。

    Country Name (两个字母) [XX]:
    State or Province Name (全名) []:
    Locality Name (eg, city) [默认的城市]:
    Organization Name (eg, company) [默认的公司名]:
    Organizational Unit Name (eg, section) []:
    Common Name (eg, your name or your server's hostname) []:example.com
    Email Address []:

编辑 Apache SSL 配置：

    /etc/httpd/conf.d/ssl.conf

    DocumentRoot "/usr/share/zabbix"
    ServerName example.com:443
    SSLCertificateFile /etc/httpd/ssl/apache-selfsigned.crt
    SSLCertificateKeyFile /etc/httpd/ssl/private/apache-selfsigned.key

重启 Apache 服务使以上修改的配置生效：

    systemctl restart httpd.service

[comment]: # ({/7514dd22-f8f5054e})

[comment]: # ({new-40cfdec2})
### Web server hardening

[comment]: # ({/new-40cfdec2})

[comment]: # ({5f85103e-bb3706f4})
#### 在 URL 的根目录上启用 Zabbix

将虚拟主机添加到 Apache 配置，并将文档根目录的永久重定向设置为 Zabbix
SSL URL。 注意将 *example.com* 替换为服务器的实际名称。

    /etc/httpd/conf/httpd.conf

    #Add lines

    <VirtualHost *:*>
        ServerName example.com
        Redirect permanent / http://example.com
    </VirtualHost>

重启 Apache 服务使以上修改的配置生效：

    systemctl restart httpd.service


[comment]: # ({/5f85103e-bb3706f4})

[comment]: # ({14bb7570-c7ee0bb2})
#### 在Web服务器上启用强制安全传输技术

为了保护Zabbix前端不受协议降级攻击，我们建议在Web服务器上启用[HSTS](https://en.wikipedia.org/wiki/HTTP_Strict_Transport_Security)策略

例如，要在Apache配置中为您的Zabbix前端启用HSTS策略：

	/etc/httpd/conf/httpd.conf

将以下指令添加到虚拟主机的配置中：

    <VirtualHost *:443>
       Header set Strict-Transport-Security "max-age=31536000"
    </VirtualHost>
重新启动Apache服务以应用更改：

    systemctl restart httpd.service

[comment]: # ({/14bb7570-c7ee0bb2})






[comment]: # ({2744c768-cd09dcd1})
#### 禁用曝光的 Web 服务器信息

建议在 Web 服务器强化过程中禁用所有 Web 服务器签名。 默认情况下，Web
服务器正在公开软件签名：

![](../../../../assets/en/manual/installation/requirements/software_signature.png)

可以通过向 Apache（用作示例）配置文件添加两行来禁用签名：

    ServerSignature Off
    ServerTokens Prod


可以通过更改 php.ini 配置文件来禁用 PHP 签名（X-Powered-By HTTP header）（默认情况下禁用签名）：

    expose_php = Off

若使应用配置文件更改生效，需重新启动 Web 服务器。


通过在 Apache中 使用 mod\_security（libapache2-mod-security2）可以实现额外的安全级别。 mod\_security允许删除服务器签名，而不是仅仅从服务器签名中删除版本。 通过在安装mod\_security之后将“SecServerSignature”更改为任何所需的值，可以将签名更改为任何值。

请参阅 Web 服务器的文档以获取有关如何删除/更改软件签名的帮助。

[comment]: # ({/2744c768-cd09dcd1})

[comment]: # ({87f465c9-720052da})
#### 禁用默认 Web 服务器错误页面

建议禁用默认错误页面以避免信息泄露。 Web 服务器默认使用内置错误页面：

![](../../../../assets/en/manual/installation/requirements/error_page_text.png)

作为 Web 服务器强化过程的一部分，应替换/删除默认错误页面。 “ErrorDocument”指令可用于为 Apache Web 服务器定义自定义错误页面/文本（用作示例）。

请参阅您的 Web 服务器的文档以查找有关如何替换/删除默认错误页面的帮助。

[comment]: # ({/87f465c9-720052da})

[comment]: # ({new-ba1547c0})
#### 删除 Web 服务器的测试页面

建议删除 Web 服务器测试页以避免信息泄露。 默认情况下，Web 服务器的
webroot 包含一个名为 index.html 的测试页（以Ubuntu上的 Apache2 为例）：

![](../../../../assets/en/manual/installation/requirements/test_page.png)

应删除测试页面，或者应将其作为Web服务器强化过程的一部分使用。

[comment]: # ({/new-ba1547c0})

[comment]: # ({8b058dc5-7e4e4a45})
#### Zabbix 设置

在默认情况下，Zabbix 配置了 *X-Frame-Options HTTP 响应头 * 设置为 `SAMEORIGIN`，这意味着内容只能加载到与页面本身具有相同来源的框架中。

从外部 URL 中提取内容的 Zabbix 前端元素（即 URL [dashboard widget](/manual/web_interface/frontend_sections/monitoring/dashboard/widgets#url)）在启用了所有沙盒限制的沙盒中显示检索到的内容。

这些设置增强了 Zabbix 前端的安全性，并提供针对 XSS 和点击劫持攻击的保护。 超级管理员可以根据需要[修改](/manual/web_interface/frontend_sections/administration/general#security) *iframe sandboxing* 和 *X-Frame-Options HTTP response header* 参数。 在更改默认设置之前，请仔细权衡风险和效果。 不建议完全关闭沙盒及 X-Frame-Options。

[comment]: # ({/8b058dc5-7e4e4a45})

[comment]: # ({04a1213d-c02c850f})
#### 带有 OpenSSL 的在Windows上的zabbix agent

使用 OpenSSL 编译的 Zabbix Windows agent将尝试访问 c:\\openssl-64bit 中访问SSL配置文件。磁盘C上的“ openssl-64bit”目录可以由非特权用户创建。

因此，为了加强安全性，需手动创建此目录并撤销非管理员用户的写访问权限。

请注意，在Windows的32位和64位版本上，目录名称将有所不同。

[comment]: # ({/04a1213d-c02c850f})

[comment]: # ({new-6cfe5a3b})
### Cryptography

[comment]: # ({/new-6cfe5a3b})

[comment]: # ({new-3f76ae70})
#### Hiding the file with list of common passwords

To increase the complexity of password brute force attacks, it is
suggested to limit access to the file `ui/data/top_passwords.txt` by
modifying web server configuration. This file contains a list of the
most common and context-specific passwords, and is used to prevent users
from setting such passwords if *Avoid easy-to-guess passwords* parameter
is enabled in the [password
policy](/manual/web_interface/frontend_sections/administration/authentication#internal_authentication).

For example, on NGINX file access can be limited by using the `location`
directive:

    location = /data/top_passwords.txt {​​​​​​​
        deny all;
        return 404;
    }​​​​​​​

On Apache - by using `.htacess` file:

    <Files "top_passwords.txt">  
      Order Allow,Deny
      Deny from all
    </Files>

​

[comment]: # ({/new-3f76ae70})

[comment]: # ({new-ba0d4036})

#### Security vulnerabilities

##### CVE-2021-42550

In Zabbix Java gateway with logback version 1.2.7 and prior versions, an attacker with the required 
privileges to edit configuration files could craft a malicious configuration allowing to execute 
arbitrary code loaded from LDAP servers.

Vulnerability to [CVE-2021-42550](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-42550) 
has been fixed since Zabbix 5.4.9. However, as an additional security measure it is recommended 
to check permissions to the `/etc/zabbix/zabbix_java_gateway_logback.xml` file and set it read-only, 
if write permissions are available for the "zabbix" user. 

[comment]: # ({/new-ba0d4036})
