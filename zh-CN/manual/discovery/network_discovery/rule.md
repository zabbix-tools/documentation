[comment]: # translation:outdated

[comment]: # ({new-bd6224b0})
# 1 配置网络发现规则

[comment]: # ({/new-bd6224b0})

[comment]: # ({new-466f5982})
#### 概述

配置Zabbix的网络发现规则来发现主机和服务：

-   首先进入 *配置 → 自动发现*
-   单击 *创建发现规则（Create rule）*
    （或在自动发现规则名称上编辑现有规则）
-   编辑自动发现规则属性

[comment]: # ({/new-466f5982})

[comment]: # ({new-f020a6f3})
#### 规则属性

![](../../../../assets/zh/manual/discovery/network_discovery/d_rule.png)

|参数                                          描|<|
|---------------------------------------------------|-|
|*名称（Name）*                                规则名|，唯一。 例如： “Local network”。|
|*通过代理发现 (Discovery by proxy)*           谁执行当前|现规则:What performs discovery:<br>**no proxy** - Zabbix server 执行发现<br>**<proxy name>** - 这个proxy执行|
|*IP范围（IP range)*                           发现|则中的IP地址范围. 可能的格式如下:<br>单个IP: 192.168.1.33<br>IP段: 192.168.1-10.1-255. 范围受限于覆盖地址的总数（小于64K）。<br>子网掩码: : 192.168.4.0/24<br>支持的子网掩码:<br>/16 - /30 for IPv4 addresses<br>/112 - /128 for IPv6 addresses\\\\IP列表: 192.168.1.1-255, 192.168.2.1-100, 192.168.2.200, 192.168.4.0/24<br>Zabbix 3.0.0起，此字段支持空格，表格和多行。|
|*Update interval*|This parameter defines how often Zabbix will execute the rule.<br>The interval is measured after the execution of previous discovery instance ends so there is no overlap.<br>[Time suffixes](/manual/appendix/suffixes) are supported, e.g. 30s, 1m, 2h, 1d, since Zabbix 3.4.0.<br>[User macros](/manual/config/macros/user_macros) are supported, since Zabbix 3.4.0.<br>*Note* that if a user macro is used and its value is changed (e.g. 1w → 1h), the next check will be executed according to the previous value (far in the future with the example values).|
|*检查(Checks)*                                Z|bbix将使用这个检查列表进行发现。<br>支持的checks: SSH, LDAP, SMTP, FTP, HTTP, HTTPS, POP, NNTP, IMAP, TCP, Telnet, Zabbix agent, SNMPv1 agent, SNMPv2 agent, SNMPv3 agent, ICMP ping.<br>基于协议的发现使用 **net.tcp.service\[\]** f功能测试每个主机, 但不包括查询SNMP OID的SNMP协议。通过在未加密模式下查询监控项(item)来测试Zabbix agent 。有关更多详情，请参阅[agent items](/manual/config/items/itemtypes/zabbix_agent)<br>“端口”参数可以是以下之一：<br>单端口: 22<br>端口段: 22-45<br>端口列表: 22-45,55,60-70|
|*设备唯一标识 (Device uniqueness criteria)*   唯一标准如|:<br>\*\*IP地址 \*\* - 使用 IP 地址作为设备唯一性标识，不处理多IP设备。如果具有相同IP的设备已经存在，则将认为已经发现，并且不会添加新的主机。<br>**发现检查类型** - 使用 SNMP 或者 Zabbix agent 的 check 作为唯一标识。|
|*启用（Enabled）*                             Wit|the check-box marked the rule is active and will be executed by Zabbix server.<br>If unmarked, the rule is not active. It won't be executed.|

[comment]: # ({/new-f020a6f3})

[comment]: # ({new-cd84714a})
#### 修改代理(proxy)设置

从Zabbix
2.2.0起，不同的代理发现的主机被认为是不同的主机。虽然这允许在不同子网使用相同的IP段执行发现，但是对已监测子网改变代理非常复杂，是因为代理的变化也必须应用于所有发现的主机。例如，在发现规则中替换代理的步骤如下：

1.  禁用发现规则
2.  同步代理配置
3.  替换发现规则中的代理
4.  替换由此规则发现的所有主机的代理
5.  启用发现规则

[comment]: # ({/new-cd84714a})

[comment]: # ({new-78f4f491})
#### 真实使用场景

例如我们设置IP段为192.168.1.1-192.168.1.254的网络发现规则。

在我们的例子中，我们需要：

-   发现有Zabbix agent运行的主机
-   每10分钟执行一次
-   如果主机正常运行时间超过1小时，添加主机
-   如果主机停机时间超过24小时，删除主机
-   将Linux主机添加到“Linux servers”组
-   将Windows主机添加到“Windows servers”组
-   链接模板*Template OS Linux* 到Linux主机
-   链接模板*Template OS Windows*到Windows主机

[comment]: # ({/new-78f4f491})

[comment]: # ({new-b8e28285})
##### 步骤1

首先给我们的IP段定义网络发现规则。

![](../../../../assets/en/manual/discovery/network_discovery/discovery.png)

Zabbix试图通过连接Zabbix
agents并获取**system.uname**键值来发现IP段为192.168.1.1-192.168.1.254中的主机。根据不同键值来对应不同的操作系统的不同操作。根据不同键值来对应不同的操作系统的不同操作。例如将Windows服务器链接到Template
OS Windows，将Linux服务器链接到Template OS Linux。

规则将每10分钟（600秒）执行一次。

当规则添加后，Zabbix将自动执行发现规则并生成基于发现的事件做后续处理。

[comment]: # ({/new-b8e28285})

[comment]: # ({new-b1d8800e})
##### 步骤2

定义[动作（action）](/manual/config/notifications/action)
将所发现的Linux服务器添加到相应的组/模板

![](../../../../assets/zh/manual/discovery/network_discovery/disc_action_1.png)

如果发生以下情况，动作(action)将被激活:

-   “Zabbix agent”服务是“up”
-   system.uname(规则中定义的Zabbix agent键值)包含“Linux”
-   正常运行时间为1小时（3600秒）或更长

![](../../../../assets/zh/manual/discovery/network_discovery/disc_action_1b.png)

该动作(action)将执行以下操作：

-   将发现的主机添加到“Linux
    servers”组（如果以前未添加主机，也添加主机）
-   链接主机到“Template OS Linux”模板。Zabbix将自动开始使用“Template OS
    Linux”模板中的项目和触发器来监控主机。

[comment]: # ({/new-b1d8800e})

[comment]: # ({new-79eefb1b})
##### 步骤3

定义动作(action) 将所发现的Windows服务器添加到相应的组/模板

![](../../../../assets/zh/manual/discovery/network_discovery/disc_action_2.png)

![](../../../../assets/zh/manual/discovery/network_discovery/disc_action_2b.png)

##### 步骤4

定义动作删除失联主机

![](../../../../assets/zh/manual/discovery/network_discovery/disc_action_3.png)

![](../../../../assets/zh/manual/discovery/network_discovery/disc_action_3b.png)

如果“Zabbix agent”服务'down'超过24小时（86400秒），服务器将被删除。

[comment]: # ({/new-79eefb1b})
