[comment]: # translation:outdated

[comment]: # ({new-57ae64ac})
# 1 网络发现

[comment]: # ({/new-57ae64ac})

[comment]: # ({new-2330e6f5})
#### 概述

Zabbix为用户提供了高效灵活的网络自动发现功能。

适当的网络发现配置可以：

-   加快Zabbix部署
-   简化管理
-   无需过多管理，也能在快速变化的环境中使用Zabbix

Zabbix网络发现基于以下信息：

-   IP范围
-   可用的外部服务（FTP，SSH，WEB，POP3，IMAP，TCP等）
-   来自 zabbix agent 的信息（仅支持未加密模式）
-   来自 snmp agent 的信息

不支持：

-   发现网络拓扑

网络发现由两个阶段组成:发现（discovery）和动作（actions）。

[comment]: # ({/new-2330e6f5})

[comment]: # ({new-d3509504})
#### 发现

Zabbix定期检测[网络发现规则](/manual/discovery/network_discovery/rule)中定义的IP范围，并为每个规则单独配置检查的频次。

请注意，一个发现规则始终由单一发现进程处理，IP范围主机不会被分拆到多个发现进程处理。

每个规则中都定义了一组需要检测的服务。

::: noteclassic
发现检查与其他检查独立处理。如果任何检查未找到服务（或失败），则仍会处理其他检查。
:::

网络发现模块每次检测到 service 和 host(IP)都会生成一个 discovery 事件

|事件名称               对应的|查结果|
|------------------------------------|---------|
|*Service Discovered*|服务首次被发现或者由'down'变'up'|
|*Service Up*|服务持续 'up'|
|*Service Lost*|服务由 'up' 变 'down'|
|*Service Down*|服务持续 'down'|
|*Host Discovered*|在主机的所有服务都 'down' 之后，至少一个服务是'up'。|
|*Host Up*|主机至少有一个服务是 'up' 状态|
|*Host Lost*|主机的所有服务在至少一个是 'up' 之后全部是 'down'。|
|*Host Down*|所有服务都持续 'down'|

[comment]: # ({/new-d3509504})

[comment]: # ({new-4490d521})
#### 动作

Zabbix
所有[动作](/manual/config/notifications/action)都是基于发现事件,例如:

-   发送通知
-   添加/删除主机
-   启用/禁用主机
-   添加主机到组
-   从组中删除主机
-   将主机链接到/取消链接模板
-   执行远程脚本命令

基于事件的网络发现动作,
可以根据设备类型、IP地址、状态、运行时间/停机时间等进行配置，查看[操作](/manual/config/notifications/action/operation)
and [条件](/manual/config/notifications/action/conditions)页面。

[comment]: # ({/new-4490d521})

[comment]: # ({new-7c3012be})
##### 创建主机

如果在动作→操作选择添加主机操作，那么主机会被添加，
即使添加主机操作未被执行，通过下列的操作仍然可以添加主机，这样的操作是：

-   启用主机
-   禁用主机
-   添加主机到主机组
-   将主机链接到模板

当添加主机时，
如果反向查找失败，那么主机名就是DNS反向查找的结果或者是IP地址。查找是从Zabbix服务器或Zabbix代理执行的，具体取决于自动发现的执行。如果在Zabbix
proxy上查找失败，则不会在Zabbix
server上重试。如果具有相同名称的主机已经存在，那么下一个主机将会把\_2附加在主机名后，依次附加\_3等。

创建的主机会被添加到主机群组中的Discovered hosts下（默认情况下，在*管理*
→ *一般*
→*[其他](/manual/web_interface/frontend_sections/administration/general#other_parameters)*
可以进行配置），如果希望将主机添加到另一个主机群组中，
可以从动作→操作选择添加一个 *从主机群组中删除*
的操作类型（需要指定“Discovered
hosts”），当然也可以选择*添加到主机群组*的操作类型（需要指定其他的主机群组），因为主机必须属于主机群组。

如果主机已经存在，
且自动发现中同时存在已发现的IP地址，那么将不会创建新的主机，但是，如果自动发现的操作包含（链接模板，添加到主机群组等），则会在已经存在的主机上执行相应的操作。

[comment]: # ({/new-7c3012be})

[comment]: # ({new-cdaa96cf})
##### 移除主机

从Zabbix
2.4.0开始，如果已发现的实体不在自动发现规则的IP范围内，则由网络发现规则创建的主机将会被自动删除。主机将立即删除

[comment]: # ({/new-cdaa96cf})

[comment]: # ({new-cc30860b})
##### 添加主机时的创建接口

当网络自动发现,添加主机时，它们的接口根据以下规律来创建的:

-   检测到服务 - 例如，如果SNMP检查成功，那么将会创建一个SNMP接口；
-   如果主机响应Zabbix
    agent和SNMP的请求，那么这两种类型的接口都会被创建；
-   如果唯一性准则是是Zabbix agent键值或是SNMP OID返回的数据，
    这第一个接口发现的主机将会被创建，而这个接口将会被作为默认接口，其他IP地址将会作为附加接口被添加。
-   如果主机只响应agent检查，则只能创建agent接口。如果稍后开始响应SNMP的检查，那么将添加SNMP接口为附加接口。
-   如果最初创建了3个独立的主机，他们都被自动发现的唯一性准则“IP”发现，然后修改自动发现规则，为了使A、B和C自动发现的唯一性准则结果是相同的，那么接口B和C作为接口A的附加接口来创建第一个主机。主机B和C作为个体主机仍然存在。在*监控中
    →
    自动发现*中，添加的接口将以黑色字体和缩进形式显示在“已发现的设备”这一列中，但在“已监控的主机”这一列将只显示第一个创建的主机A。由于被认为附加接口的IP，所以不测量主机B和C的“在线时间/断线时间”。

[comment]: # ({/new-cc30860b})



[comment]: # ({new-9b18af68})
##### Interface creation when adding hosts

When hosts are added as a result of network discovery, they get
interfaces created according to these rules:

-   the services detected - for example, if an SNMP check succeeded, an
    SNMP interface will be created
-   if a host responded both to Zabbix agent and SNMP requests, both
    types of interfaces will be created
-   if uniqueness criteria are Zabbix agent or SNMP-returned data, the
    first interface found for a host will be created as the default one.
    Other IP addresses will be added as additional interfaces.
-   if a host responded to agent checks only, it will be created with an
    agent interface only. If it would start responding to SNMP later,
    additional SNMP interfaces would be added.
-   if 3 separate hosts were initially created, having been discovered
    by the "IP" uniqueness criteria, and then the discovery rule is
    modified so that hosts A, B and C have identical uniqueness criteria
    result, B and C are created as additional interfaces for A, the
    first host. The individual hosts B and C remain. In *Monitoring →
    Discovery* the added interfaces will be displayed in the "Discovered
    device" column, in black font and indented, but the "Monitored host"
    column will only display A, the first created host.
    "Uptime/Downtime" is not measured for IPs that are considered to be
    additional interfaces.

[comment]: # ({/new-9b18af68})

[comment]: # ({new-883e6b92})
#### Changing proxy setting

The hosts discovered by different proxies are always treated as
different hosts. While this allows to perform discovery on matching IP
ranges used by different subnets, changing proxy for an already
monitored subnet is complicated because the proxy changes must be also
applied to all discovered hosts.

For example the steps to replace proxy in a discovery rule:

1.  disable discovery rule
2.  sync proxy configuration
3.  replace the proxy in the discovery rule
4.  replace the proxy for all hosts discovered by this rule
5.  enable discovery rule

[comment]: # ({/new-883e6b92})
