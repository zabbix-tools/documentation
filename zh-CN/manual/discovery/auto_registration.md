[comment]: # translation:outdated

[comment]: # ({new-1c2455b7})
# 2 Active agent自动注册

[comment]: # ({/new-1c2455b7})

[comment]: # ({new-81edbbe2})
#### 概述

Zabbix Active
agent可以实现自动注册，进而服务器对其进行监控。通过这种方式，无需在服务器上进行手动配置便可直接启动对新host的监控。

当以前未知的active agent要求检查时，会发生自动注册。

这样功能可以非常方便的自动监控新的Cloud节点。一旦在Cloud中有一个新节点，Zabbix将自动启动host的性能和可用性数据的收集。

Active agent自动注册还支持对被添加的主机进行被动检查的监控。当active
agent要求检查时，前提是在配置文件中已定义好了“ListenIP”或“ListenPort”配置参数，这些参数将发送到服务器。（如果指定了多个IP地址，则第一个将被发送到服务器。）

服务器在添加新的自动注册主机时，使用接收到的IP地址和端口配置agent。如果没有接收到IP地址值，则使用传入连接的IP地址。如果没有接收到端口值，则使用10050。

以下情况下，自动注册会自动运行：

-   主机元数据信息发生变化
-   手动添加主机，元数据信息有缺失
-   手动切换主机，由另一台新的proxy监控
-   同一台host的自动注册由新的proxy发出

[comment]: # ({/new-81edbbe2})

[comment]: # ({new-5d32b87c})
#### 配置

[comment]: # ({/new-5d32b87c})

[comment]: # ({new-bf3762f0})
##### 指定服务器

请确保在 [配置文件中](/manual/appendix/config/zabbix_agentd)
指定了Zabbix server- zabbix\_agentd.conf

    ServerActive=10.0.0.1

如果你没有在zabbix\_agentd.conf中特别定义*Hostname*,
则服务器将使用agent的系统主机名命名主机。Linux中的系统主机名可以通过运行'hostname'命令获取。

修改配置文件后需要重启agent

[comment]: # ({/new-bf3762f0})

[comment]: # ({new-7b103292})
##### Aactive agent自动注册动作

当服务器从agent收到自动注册请求时，它会调用一个
[动作n](/manual/config/notifications/action).
必须要为agent自动注册配置一个事件源为“自动注册”的动作。

::: noteclassic
设置Setting up [网络发现](network_discovery) 不需要使active
agents自动注册。
:::

在Zabbix前端页面，点击*配置 → 动作*,选择*自动注册*
为事件源，然后单击*创建动作*:

-   在动作选项卡，定义动作名称
-   可选指定条件。如果要使用“主机元数据”条件，请参阅下一节。
-   在“操作”选项卡中，需要添加关联操作，如“添加主机”，“添加到主机组”（例如，*发现的主机*），“链接到模板”等。

<note
tip>如果自动注册主机只能支持主动监视（例如由于防火墙的原因，Zabbix服务器不允许访问的主机），则可能需要创建一个特定的模板，如*Template\_Linux-active*来做关联。
:::

[comment]: # ({/new-7b103292})

[comment]: # ({new-66cac4ef})
#### 使用主机元数据

当agent程序向服务器发送自动注册请求时，会发送其主机名。在某些情况下（例如，Amazon云端节点），Zabbix
Server单单通过主机名区分主机。这时可以选择主机元数据将其他信息从agent发送到服务器。

主机元数据在agent[配置文件](/manual/appendix/config/zabbix_agentd) -
zabbix\_agentd.conf中配置。在配置文件中指定主机元数据有两种方式：

    HostMetadata
    HostMetadataItem

请参阅上面链接中的选项描述。

<note:important>每当active
agent发送刷新主动检查请求到服务器时，都会进行自动注册尝试。请求的延迟在agent的[RefreshActiveChecks](/manual/appendix/config/zabbix_agentd)参数中指定。第一个请求在agent重新启动后立即发送。
:::

[comment]: # ({/new-66cac4ef})

[comment]: # ({new-9c275675})
##### 案例1

使用主机元数据来区分Linux和Windows主机。

假设你希望主机由Zabbix server自动注册，你的网络上有active Zabbix
agents（请参阅上面的“配置”部分），你的网络上有Windows主机和Linux主机，你有“Template
OS Linux”和“Template OS Windows” 模板，Zabbix页面可以使用。
在主机注册时，你希望将Linux /
Windows模板正确的应用在正在注册的主机。默认情况下，只有主机名在自动注册时会发送到服务器，但这还不够。为了确保将正确的模板应用于主机，你应该使用主机元数据。

[comment]: # ({/new-9c275675})

[comment]: # ({new-af247ed6})
##### 前段配置

第一步是配置前端，创建2个动作，第一个动作：

-   名称：Linux主机自动注册
-   条件：主机元数据,如*Linux*
-   动作：链接到模板：Template OS Linux

::: noteclassic
在这种情况下，您可以跳过“添加主机”的操作。链接到模板需要首先添加主机，服务器会自动执行“添加主机”的操作。
:::

第二个动作：

-   名称：Windows主机自动注册
-   条件：主机元数据，如*Windows*
-   操作：链接到模板：Template OS Windows

[comment]: # ({/new-af247ed6})

[comment]: # ({new-070aaeb4})
##### Agent配置

第二部进行Agent配置，添加下行至agent配置文件中：

    HostMetadataItem=system.uname

This way you make sure host metadata will contain "Linux" or "Windows"
depending on the host an agent is running on. An example of host
metadata in this case:

    Linux: Linux server3 3.2.0-4-686-pae #1 SMP Debian 3.2.41-2 i686 GNU/Linux
    Windows: Windows WIN-0PXGGSTYNHO 6.0.6001 Windows Server 2008 Service Pack 1 Intel IA-32

Do not forget to restart the agent after making any changes to the
configuration file.

[comment]: # ({/new-070aaeb4})

[comment]: # ({new-9543f562})
##### Example 2

*Step 1*

Using host metadata to allow some basic protection against unwanted
hosts registering.

[comment]: # ({/new-9543f562})

[comment]: # ({new-110dbc2b})
##### Frontend configuration

Create an action in the frontend, using some hard-to-guess secret code
to disallow unwanted hosts:

-   Name: Auto registration action Linux
-   Conditions:

```{=html}
<!-- -->
```
        * Type of calculation: AND
        * Condition (A): Host metadata like //Linux//
        * Condition (B): Host metadata like //21df83bf21bf0be663090bb8d4128558ab9b95fba66a6dbf834f8b91ae5e08ae//
    * Operations: 
        * Send message to users: Admin via all media
        * Add to host groups: Linux servers
        * Link to templates: Template OS Linux

Please note that this method alone does not provide strong protection
because data is transmitted in plain text. Configuration cache reload is
required for changes to have an immediate effect.

[comment]: # ({/new-110dbc2b})

[comment]: # ({new-85f695d6})
##### Agent configuration

Add the next line to the agent configuration file:

    HostMetadata=Linux    21df83bf21bf0be663090bb8d4128558ab9b95fba66a6dbf834f8b91ae5e08ae

where "Linux" is a platform, and the rest of the string is the
hard-to-guess secret text.

Do not forget to restart the agent after making any changes to the
configuration file.

*Step 2*

It is possible to add additional monitoring for an already registered
host.

[comment]: # ({/new-85f695d6})

[comment]: # ({new-c5ffa817})
##### Frontend configuration

Update the action in the frontend:

-   Name: Auto registration action Linux
-   Conditions:

```{=html}
<!-- -->
```
        * Type of calculation: AND
        * Condition (A): Host metadata like Linux
        * Condition (B): Host metadata like 21df83bf21bf0be663090bb8d4128558ab9b95fba66a6dbf834f8b91ae5e08ae
    * Operations:
        * Send message to users: Admin via all media
        * Add to host groups: Linux servers
        * Link to templates: Template OS Linux
        * Link to templates: Template DB MySQL

[comment]: # ({/new-c5ffa817})

[comment]: # ({new-9fca761d})
##### Agent configuration

Update the next line in the agent configuration file:

    HostMetadata=MySQL on Linux 21df83bf21bf0be663090bb8d4128558ab9b95fba66a6dbf834f8b91ae5e08ae

Do not forget to restart the agent after making any changes to the
configuration file.

[comment]: # ({/new-9fca761d})

[comment]: # ({new-6eb51339})
### 2 Active agent auto-registration

[comment]: # ({/new-6eb51339})

[comment]: # ({new-15c86816})
#### Overview

It is possible to allow active Zabbix agent auto-registration, after
which the server can start monitoring them. This way new hosts can be
added for monitoring without configuring them manually on the server.

Auto registration can happen when a previously unknown active agent asks
for checks.

The feature might be very handy for automatic monitoring of new Cloud
nodes. As soon as you have a new node in the Cloud Zabbix will
automatically start the collection of performance and availability data
of the host.

Active agent auto-registration also supports the monitoring of added
hosts with passive checks. When the active agent asks for checks,
providing it has the 'ListenIP' or 'ListenPort' configuration parameters
defined in the configuration file, these are sent along to the server.
(If multiple IP addresses are specified, the first one is sent to the
server.)

Server, when adding the new auto-registered host, uses the received IP
address and port to configure the agent. If no IP address value is
received, the one used for the incoming connection is used. If no port
value is received, 10050 is used.

Auto-registration is rerun:

-   if host [metadata](#using_host_metadata) information changes
-   for manually created hosts with metadata missing
-   if a host is manually changed to be monitored by another Zabbix
    proxy
-   if auto-registration for the same host comes from a new Zabbix proxy

#### Configuration

##### Specify server

Make sure you have the Zabbix server identified in the agent
[configuration file](/manual/appendix/config/zabbix_agentd) -
zabbix\_agentd.conf

    ServerActive=10.0.0.1

Unless you specifically define a *Hostname* in zabbix\_agentd.conf, the
system hostname of agent location will be used by server for naming the
host. The system hostname in Linux can be obtained by running the
'hostname' command.

Restart the agent after making any changes to the configuration file.

##### Action for active agent auto-registration

When server receives an auto-registration request from an agent it calls
an [action](/manual/config/notifications/action). An action of event
source "Auto registration" must be configured for agent
auto-registration.

::: noteclassic
Setting up [network discovery](network_discovery) is not
required to have active agents auto-register.
:::

In the Zabbix frontend, go to *Configuration → Actions*, select *Auto
registration* as the event source and click on *Create action*:

-   In the Action tab, give your action a name
-   Optionally specify conditions. If you are going to use the "Host
    metadata" condition, see the next section.
-   In the Operations tab, add relevant operations, such as - 'Add
    host', 'Add to host groups' (for example, *Discovered hosts*), 'Link
    to templates', etc.

::: notetip
If the hosts that will be auto-registering are likely to
be supported for active monitoring only (such as hosts that are
firewalled from your Zabbix server) then you might want to create a
specific template like *Template\_Linux-active* to link to.
:::

#### Using host metadata

When agent is sending an auto-registration request to the server it
sends its hostname. In some cases (for example, Amazon cloud nodes) a
hostname is not enough for Zabbix server to differentiate discovered
hosts. Host metadata can be optionally used to send other information
from an agent to the server.

Host metadata is configured in the agent [configuration
file](/manual/appendix/config/zabbix_agentd) - zabbix\_agentd.conf.
There are 2 ways of specifying host metadata in the configuration file:

    HostMetadata
    HostMetadataItem

See the description of the options in the link above.

<note:important>An auto-registration attempt happens every time an
active agent sends a request to refresh active checks to the server. The
delay between requests is specified in the
[RefreshActiveChecks](/manual/appendix/config/zabbix_agentd) parameter
of the agent. The first request is sent immediately after the agent is
restarted.
:::

##### Example 1

Using host metadata to distinguish between Linux and Windows hosts.

Say you would like the hosts to be auto-registered by the Zabbix server.
You have active Zabbix agents (see "Configuration" section above) on
your network. There are Windows hosts and Linux hosts on your network
and you have "Template OS Linux" and "Template OS Windows" templates
available in your Zabbix frontend. So at host registration you would
like the appropriate Linux/Windows template to be applied to the host
being registered. By default only the hostname is sent to the server at
auto-registration, which might not be enough. In order to make sure the
proper template is applied to the host you should use host metadata.

##### Frontend configuration

The first thing to do is to configure the frontend. Create 2 actions.
The first action:

-   Name: Linux host autoregistration
-   Conditions: Host metadata like *Linux*
-   Operations: Link to templates: Template OS Linux

::: noteclassic
You can skip an "Add host" operation in this case. Linking
to a template requires adding a host first so the server will do that
automatically.
:::

The second action:

-   Name: Windows host autoregistration
-   Conditions: Host metadata like *Windows*
-   Operations: Link to templates: Template OS Windows

##### Agent configuration

Now you need to configure the agents. Add the next line to the agent
configuration files:

    HostMetadataItem=system.uname

This way you make sure host metadata will contain "Linux" or "Windows"
depending on the host an agent is running on. An example of host
metadata in this case:

    Linux: Linux server3 3.2.0-4-686-pae #1 SMP Debian 3.2.41-2 i686 GNU/Linux
    Windows: Windows WIN-0PXGGSTYNHO 6.0.6001 Windows Server 2008 Service Pack 1 Intel IA-32

Do not forget to restart the agent after making any changes to the
configuration file.

[comment]: # ({/new-15c86816})
