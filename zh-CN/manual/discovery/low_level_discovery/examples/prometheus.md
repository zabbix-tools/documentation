[comment]: # translation:outdated

[comment]: # ({new-f06f3ae9})
# 12 Discovery using Prometheus data

[comment]: # ({/new-f06f3ae9})

[comment]: # ({new-4d2224d9})
#### Overview

Data provided in Prometheus line format can be used for low-level
discovery.

See [Prometheus checks](/manual/config/items/itemtypes/prometheus) for
details how Prometheus data querying is implemented in Zabbix.

[comment]: # ({/new-4d2224d9})

[comment]: # ({new-266cbb44})
#### Configuration

The low-level discovery rule should be created as a [dependent
item](/manual/config/items/itemtypes/dependent_items) to the HTTP master
item that collects Prometheus data.

[comment]: # ({/new-266cbb44})

[comment]: # ({new-5a48de63})
##### Prometheus to JSON

In the discovery rule, go to the Preprocessing tab and select the
*Prometheus to JSON* preprocessing option. Data in JSON format are
needed for discovery and the *Prometheus to JSON* preprocessing option
will return exactly that, with the following attributes:

-   metric name
-   metric value
-   help (if present)
-   type (if present)
-   labels (if present)
-   raw line

For example, querying `wmi_logical_disk_free_bytes`:

![](../../../../../assets/en/manual/discovery/low_level_discovery/lld_rule_prom_json.png)

from these Prometheus lines:

    # HELP wmi_logical_disk_free_bytes Free space in bytes (LogicalDisk.PercentFreeSpace)
    # TYPE wmi_logical_disk_free_bytes gauge
    wmi_logical_disk_free_bytes{volume="C:"} 3.5180249088e+11
    wmi_logical_disk_free_bytes{volume="D:"} 2.627731456e+09
    wmi_logical_disk_free_bytes{volume="HarddiskVolume4"} 4.59276288e+08

will return:

``` {.java}
[
    {
        "name": "wmi_logical_disk_free_bytes",
        "help": "Free space in bytes (LogicalDisk.PercentFreeSpace)",
        "type": "gauge",
        "labels": {
            "volume": "C:"
         },
        "value": "3.5180249088e+11",
        "line_raw": "wmi_logical_disk_free_bytes{volume=\"C:\"} 3.5180249088e+11"
    },
    {
        "name": "wmi_logical_disk_free_bytes",
        "help": "Free space in bytes (LogicalDisk.PercentFreeSpace)",
        "type": "gauge",
        "labels": {
            "volume": "D:"
         },
        "value": "2.627731456e+09",
        "line_raw": "wmi_logical_disk_free_bytes{volume=\"D:\"} 2.627731456e+09"
    },
    {
        "name": "wmi_logical_disk_free_bytes",
        "help": "Free space in bytes (LogicalDisk.PercentFreeSpace)",
        "type": "gauge",
        "labels": {
            "volume": "HarddiskVolume4"
         },
        "value": "4.59276288e+08",
        "line_raw": "wmi_logical_disk_free_bytes{volume=\"HarddiskVolume4\"} 4.59276288e+08"
    }
]
```

[comment]: # ({/new-5a48de63})

[comment]: # ({new-38a03356})
##### Mapping LLD macros

Next you have to go to the LLD macros tab and make the following
mappings:

    {#VOLUME}=$.labels['volume']
    {#METRIC}=$['name']
    {#HELP}=$['help']

[comment]: # ({/new-38a03356})

[comment]: # ({new-b367cc91})
##### Item prototype

You may want to create an item prototype like this:

![](../../../../../assets/en/manual/discovery/low_level_discovery/lld_item_prototype_prom.png)

with preprocessing options:

![](../../../../../assets/en/manual/discovery/low_level_discovery/lld_item_prototype_prom_b.png)

[comment]: # ({/new-b367cc91})
