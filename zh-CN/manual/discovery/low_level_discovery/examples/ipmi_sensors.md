[comment]: # translation:outdated

[comment]: # ({new-d10ce153})
# 6 Discovery of IPMI sensors

[comment]: # ({/new-d10ce153})

[comment]: # ({new-de00bb83})
#### Overview

It is possible to automatically discover IPMI sensors.

To do that, you may use a combination of:

-   the `ipmi.get` IPMI item (supported since Zabbix **5.0.0**) as the
    master item
-   dependent low-level discovery rule and item prototypes

[comment]: # ({/new-de00bb83})

[comment]: # ({new-5d32b87c})
#### Configuration

[comment]: # ({/new-5d32b87c})

[comment]: # ({new-d4ca9138})
##### Master item

Create an IPMI item using the following key:

    ipmi.get

![](../../../../../assets/en/manual/discovery/low_level_discovery/ipmi_get_item.png)

Set the type of information to "Text" for possibly big JSON data.

[comment]: # ({/new-d4ca9138})

[comment]: # ({new-eacff693})
##### Dependent LLD rule

Create a low-level discovery rule as "Dependent item" type:

![](../../../../../assets/en/manual/discovery/low_level_discovery/ipmi_get_lld.png)

As master item select the `ipmi.get` item we created.

In the "LLD macros" tab define a custom macro with the corresponding
JSONPath:

![](../../../../../assets/en/manual/discovery/low_level_discovery/ipmi_get_lld_b.png)

[comment]: # ({/new-eacff693})

[comment]: # ({new-11e01b0e})
##### Dependent item prototype

Create an item prototype with "Dependent item" type in this LLD rule. As
master item for this prototype select the `ipmi.get` item we created.

![](../../../../../assets/en/manual/discovery/low_level_discovery/ipmi_get_prototype.png)

Note the use of the {\#SENSOR\_ID} macro in the item prototype name and
key:

-   *Name*: IPMI value for sensor {\#SENSOR\_ID}
-   *Key*: ipmi\_sensor\[{\#SENSOR\_ID}\]

As type of information, *Numeric (unsigned)*.

In the item prototype "Preprocessing" tab select JSONPath and use the
following JSONPath expression as parameter:

    $.[?(@.id=='{#SENSOR_ID}')].value.first()

![](../../../../../assets/en/manual/discovery/low_level_discovery/ipmi_get_prototype_b.png)

When discovery starts, one item per each IPMI sensor will be created.
This item will return the integer value of the given sensor.

[comment]: # ({/new-11e01b0e})
