[comment]: # translation:outdated

[comment]: # ({new-54ecc26e})
# Notes on low-level discovery

[comment]: # ({/new-54ecc26e})

[comment]: # ({new-40e2ce9f})
#### Application discovery

Application prototypes support LLD macros.

One application prototype can be used by several item prototypes of the
same discovery rule.

If created application prototype is not used by any item prototype it
gets removed from 'Application prototypes' list automatically.

Like other discovered entities applications follow the lifetime defined
in discovery rule ('keep lost resources period' setting) - they are
removed after not being discovered for the specified number of days.

If an application is not discovered anymore all discovered items are
automatically removed from it, even if the application itself is not yet
removed because of the 'lost resources period' setting.

Application prototypes defined by one discovery rule can't discover the
same application. In this situation only the first prototype discovery
will succeed, the rest will report appropriate LLD error. Only
application prototypes defined in different discovery rules can result
in discovering the same application.

[comment]: # ({/new-40e2ce9f})



[comment]: # ({new-071e9701})
#### Multiple LLD rules for the same item

Since Zabbix agent version 3.2 it is possible to define several
low-level discovery rules with the same discovery item.

To do that you need to define the Alias agent
[parameter](/manual/appendix/config/zabbix_agentd), allowing to use
altered discovery item keys in different discovery rules, for example
`vfs.fs.discovery[foo]`, `vfs.fs.discovery[bar]`, etc.

[comment]: # ({/new-071e9701})

[comment]: # ({new-a8d6d602})
#### Data limits for return values

There is no limit for low-level discovery rule JSON data if it is
received directly by Zabbix server, because return values are processed
without being stored in a database. There's also no limit for custom
low-level discovery rules, however, if it is intended to acquire custom
LLD data using a user parameter, then the user parameter return value
limit applies (512 KB).

If data has to go through Zabbix proxy it has to store this data in
database so [database
limits](/manual/config/items/item#text_data_limits) apply.

[comment]: # ({/new-a8d6d602})
