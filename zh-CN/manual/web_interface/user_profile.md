[comment]: # translation:outdated

[comment]: # ({new-bba949fc})
# 2 User profile用户资料

[comment]: # ({/new-bba949fc})

[comment]: # ({new-2f38d1c3})
#### Overview概述

In the user profile you can customize some Zabbix frontend features,
such as the interface language, color theme, number of rows displayed in
the lists etc. The changes made here will apply for the user only.
在用户资料中，你可以自定义一些Zabbix的前端特性，比如：界面语言，主题颜色，列表中显示的行数等等。此改变只针对当前用户。

To access the user profile configuration form, click on the
![](../../../assets/en/manual/web_interface/user_profile.png) user
profile link in the upper right corner of Zabbix window.
点击zabbix窗口右上角的
![](../../../assets/en/manual/web_interface/user_profile.png)
来访问用户信息.

[comment]: # ({/new-2f38d1c3})

[comment]: # ({new-8fcf9738})
#### Configuration配置

The **User** tab allows you to set various user preferences. User
选项卡允许您设置关于用户相关配置.

![profile1.png](../../../assets/en/manual/web_interface/profile1.png)

|Parameter参数         D|scription描述|
|-------------------------|---------------|
|*Password*|Click on the link to display two fields for entering a new password.点击链接显示两个字段，来输入新的密码.|
|*Language*|Select the interface language of your choice.选择您想要的界面语言。<br>The php gettext extension is required for the translations to work.PHP的gettext扩展是翻译正常运作所必需的。|
|*Theme*|Select a color theme specifically for your profile为您的资料选择一种特殊的颜色主题:<br>**System default** - use default system settings系统默认<br>**Blue** - standard blue theme标准蓝色主题<br>**Dark** - alternative dark theme暗黑主题<br>**High-contrast light** - light theme with high contrast高对比的浅色主题<br>**High-contrast dark** - dark theme with high contrast高对比的暗黑主题|
|*Auto-login*|Mark this checkbox to make Zabbix remember you and log you in automatically for 30 days. Browser cookies are used for this. 选择复选框来标记自动登录，无需再次输入用户名和密码。|
|*Auto-logout*|With this checkbox marked you will be logged out automatically, after the set amount of seconds (minimum 90 seconds, maximum 1 day).勾选了这个复选框后，您将在设定的秒数后自动注销(最少90秒).<br>[Time suffixes](/manual/appendix/suffixes) are supported, e.g. 90s, 5m, 2h, 1d.支持的。如 90s, 5m, 2h, 1d.<br>Note that this option will not work:请注意，以下选项不起作用：<br>\* If the "Show warning if Zabbix server is down" global configuration option is enabled and Zabbix frontend is kept open;当Zabbix服务器宕机时显示告警，全局配置选项启用且Zabbix前端持续打开时；<br>\* When Monitoring menu pages perform background information refreshes;当监控菜单页面一直在后台进行信息刷新时，会无法正常工作；<br>\* If logging in with the *Remember me for 30 days* option checked.当选中"30天记住我"选项，请登录.|
|*Refresh*|You can set how often the information in the pages will be refreshed on the Monitoring menu, except for Dashboard, which uses its own refresh parameters for every widget.您可以设置监控目录下信息刷新的频率。 只有Dashboard例外,它使用为自己的每个部件使用自有的刷新参数。<br>[Time suffixes](/manual/appendix/suffixes) are supported, e.g. 30s, 5m, 2h, 1d. 支持的。如 30s, 5m, 2h, 1d.|
|*Rows per page*|You can set how many rows will be displayed per page in the lists. Fewer rows (and fewer records to display) mean faster loading times. 可设置每页显示的行数。行数越少（显示的记录越少）加载速度越快。|
|*URL (after login)*|You can set a specific URL to be displayed after the login. Instead of the default *Monitoring* → *Dashboard* it can be, for example, the URL of *Monitoring* → *Triggers*. 您可设置在登录后显示的自定义 URL 不同于默认的 Monitoring → Dashboard 例如，它甚至可以成 Monitoring 的 URL → Triggers.|

::: noteclassic
If some language is not available for selection in the user
profile it means that a locale for it is not installed on the web
server. See the [link](#see_also) at the bottom of this page to find out
how to install
them.如果某些语言在用户资料中无法选择，则意味着它的区域设置未安装在Web服务器上。
请参阅链接 [link](#see_also) ，了解如何安装。
:::

The **Media** tab allows you to specify the
[media](/manual/config/notifications/media) details for the user, such
as the types, the addresses to use and when to use them to deliver
notifications.媒介Media选项卡允许您指定给用户以
[media](/manual/config/notifications/media)
细节，例如类型、地址的使用以及何时使用它们来发送通知。

![profile2.png](../../../assets/en/manual/web_interface/profile2.png){width="600"}

::: noteclassic
Only [admin
level](/manual/config/users_and_usergroups/permissions) users (Admin and
Super Admin) can change their own media details.只有 管理员级别 [admin
level](/manual/config/users_and_usergroups/permissions)
用户（管理员和超级管理员）可以更改他们自己的 media 细节。
:::

The **Messaging** tab allows you to set [global
notifications](/manual/web_interface/user_profile/global_notifications).
可通过**Messaging**选项卡,设置全局通知[global
notifications](/manual/web_interface/user_profile/global_notifications).

[comment]: # ({/new-8fcf9738})

[comment]: # ({new-960ba61e})
### See also参考

1.  [How to install additional locales to be able to select unavailable
    languages in the user
    profile](http://www.zabbix.org/wiki/How_to/install_locale)

[comment]: # ({/new-960ba61e})
