[comment]: # translation:outdated

[comment]: # ({new-6192d6e3})
# 1 Services

[comment]: # ({/new-6192d6e3})

[comment]: # ({new-dfc7b2ba})
### Overview

In this section you can see a high-level status of whole services that have been 
configured in Zabbix, based on your infrastructure. 

A service may be a hierarchy consisting if several levels of other services, 
called "child" services, which are attributes to the overall status of the 
service (see also an overview of the [service monitoring](/manual/it_services) functionality.)

The main categories of service status are *OK* or *Problem*, where the *Problem* status 
is expressed by the corresponding problem severity name and color.

While the view mode allows to monitor services with their status and other details, you can 
also configure the service hierarchy in this section (add/edit services, child services) 
by switching to the edit mode. 

To switch from the view to the edit mode (and back) click on the respective button 
in the upper right corner:

-   ![](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/services_view_mode.png) -
    view services
-   ![](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/services_edit_mode.png) -
    add/edit services, and child services

Note that access to editing depends on [user role](/manual/web_interface/frontend_sections/administration/user_roles)
settings.

[comment]: # ({/new-dfc7b2ba})

[comment]: # ({new-9c989a32})
### Viewing services

![](../../../../../assets/en/manual/web_interface/services_view.png){width="600"}

A list of the existing services is displayed.

Displayed data:

|Parameter|Description|
|---------|-----------|
|*Name*|Service name.<br>The service name is a link to [service details](#service-details).<br>The number after the name indicates how many [child services](#service-details) the service has.|
|*Status*|Service status:<br>**OK** - no problems<br>**(trigger color and severity)** - indicates a problem and its severity. If there are multiple problems, the color and severity of the problem with highest severity is displayed.|
|*Root cause*|Underlying problems that directly or indirectly affect the service status are listed.<br>The same problems are listed as returned by the {SERVICE.ROOTCAUSE} [macro](/manual/appendix/macros/supported_by_location).<br>Click on the problem name to see more details about it in *Monitoring* → *Problems*.<br>Problems that do not affect the service status are not in the list.|
|*Created at*|The time when the service was created is displayed.|
|*Tags*|Tags of the service are displayed. Tags are used to identify a service.<br>Tags can be used in configuring service [actions](/manual/config/notifications/action).|

[comment]: # ({/new-9c989a32})

[comment]: # ({new-84099469})
##### Buttons

View mode buttons being common for all sections are described on the
[Monitoring](/manual/web_interface/frontend_sections/monitoring#view_mode_buttons)
page.

[comment]: # ({/new-84099469})

[comment]: # ({new-a1995254})
##### Using filter

You can use the filter to display only the services you are interested
in.

[comment]: # ({/new-a1995254})

[comment]: # ({new-512ba2ca})
### Editing services

Click on the *Edit* button to access the edit mode. When in edit mode, the listing 
is complemented with checkboxes before the entries and also these additional options:

-   ![](../../../../../assets/en/manual/web_interface/add_service.png) -
    add a child service to this service
-   ![](../../../../../assets/en/manual/web_interface/edit_service.png) -
    edit this service
-   ![](../../../../../assets/en/manual/web_interface/delete_service.png) -
    delete this service

![](../../../../../assets/en/manual/web_interface/services_edit.png){width="600"}

To [configure](#service_configuration) a new service, click on the
*Create service* button in the top right-hand corner.

Two buttons below the list offer some mass-editing options:

-   *Mass update* - mass update service properties
-   *Delete* - delete the services

To use these options, mark the checkboxes before the respective
services, then click on the required button.

[comment]: # ({/new-512ba2ca})

[comment]: # ({new-10028f4d})
### Service details

To access service details, click on the service name. To return to the 
list of all services, click on *All services*.

Service details include the info box and the list of child services.

![](../../../../../assets/en/manual/web_interface/service_details.png){width="600"}

To access the info box, click on the *Info* tab. The info box contains the 
following entries:

- Names of parent services (if any)
- Current status of this service
- Current SLA(s) of this service, in the format `SLA name:service level indicator`. 'SLA name' is also a link to the SLA report for this service. If you position the mouse on the info box next to the service-level indicator (SLI), a pop-up info list is displayed with SLI details. The service-level indicator displays the current service level, in percentage.
- Service tags

The info box also contains a link to the [service configuration](#service-configuration).

To use the filter for child services, click on the *Filter* tab. When in 
edit mode, the child service listing is complemented with additional 
editing options:

-   ![](../../../../../assets/en/manual/web_interface/add_service.png) -
    add a child service to this service
-   ![](../../../../../assets/en/manual/web_interface/edit_service.png) -
    edit this service
-   ![](../../../../../assets/en/manual/web_interface/delete_service.png) -
    delete this service

[comment]: # ({/new-10028f4d})


