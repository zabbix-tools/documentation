[comment]: # translation:outdated

[comment]: # ({new-315566e5})
# 4 SLA report

[comment]: # ({/new-315566e5})

[comment]: # ({new-3b69a682})
### Overview

This section allows to view SLA reports, based on the criteria selected 
in the filter.

SLA reports can also be displayed as a [dashboard widget](/manual/web_interface/frontend_sections/monitoring/dashboard/widgets/sla_report).

[comment]: # ({/new-3b69a682})

[comment]: # ({new-18f6e4c9})

### Report

The filter allows to select the report based on the SLA name as well as the 
service name. It is also possible to limit the displayed period.

![](../../../../../assets/en/manual/web_interface/sla_report.png){width="600"}

Each column (period) displays the SLI for that period. SLIs that are in breach of the set 
SLO are highlighted in red.

20 periods are displayed in the report. A maximum of 100 periods can be displayed, if 
both the *From* date and *To* date are specified. 

[comment]: # ({/new-18f6e4c9})

[comment]: # ({new-03eed45c})

### Report details

If you click on the service name in the report, you can access another report 
that displays a more detailed view.

![](../../../../../assets/en/manual/web_interface/sla_report_details.png){width="600"}

[comment]: # ({/new-03eed45c})
