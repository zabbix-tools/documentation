[comment]: # translation:outdated

[comment]: # ({new-d91968a5})
# 4 Discovery status

[comment]: # ({/new-d91968a5})

[comment]: # ({new-e3595eca})
#### Overview

This widget displays a status summary of the active network discovery
rules.

![](../../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/discovery_status.png)

All configuration parameters are [common](/manual/web_interface/frontend_sections/dashboards/widgets#common-parameters) 
for all widgets.

[comment]: # ({/new-e3595eca})
