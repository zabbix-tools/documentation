<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="zh-CN" datatype="plaintext" original="manual/web_interface/frontend_sections/dashboards/widgets/geomap.md">
    <body>
      <trans-unit id="2091b9ba" xml:space="preserve">
        <source># 7 Geomap</source>
      </trans-unit>
      <trans-unit id="ea309297" xml:space="preserve">
        <source>#### Overview

Geomap widget displays hosts as markers on a geographical map using
open-source JavaScript interactive maps library Leaflet.

::: noteclassic
 Zabbix offers multiple predefined map tile service
providers and an option to add a custom tile service provider or even
host tiles themselves (configurable in the *Administration → General →
Geographical maps* [menu
section](/manual/web_interface/frontend_sections/administration/general#geographical_maps)).
:::</source>
      </trans-unit>
      <trans-unit id="8e1da957" xml:space="preserve">
        <source>By default, the widget displays all enabled hosts with valid geographical coordinates defined in the host configuration. It is
possible to configure host filtering in the widget parameters.

The valid host coordinates are: 

- Latitude: from -90 to 90 (can be integer or float number) 
- Longitude: from -180 to 180 (can be integer or float number) </source>
      </trans-unit>
      <trans-unit id="08db40dc" xml:space="preserve">
        <source>#### Configuration

To add the widget, select *Geomap* as type.

![](../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/geomap.png)</source>
      </trans-unit>
      <trans-unit id="e0aba304" xml:space="preserve">
        <source>In addition to the parameters that are [common](/manual/web_interface/frontend_sections/dashboards/widgets#common-parameters) 
for all widgets, you may set the following specific options:

|   |   |
|--|--------|
|*Host groups*|Select host groups to be displayed on the map.&lt;br&gt;This field is auto-complete so starting to type the name of a group will offer a dropdown of matching groups.&lt;br&gt;Scroll down to select. Click on 'x' to remove selected groups.&lt;br&gt;If nothing is selected in both *Host groups* and *Hosts* fields, all hosts with valid coordinates will be displayed.&lt;br&gt;This parameter is not available when configuring the widget on a [template dashboard](/manual/config/templates/template#adding-dashboards).|
|*Hosts*|Select hosts to be displayed all the map.&lt;br&gt;This field is auto-complete so starting to type the name of a host will offer a dropdown of matching hosts.&lt;br&gt;Scroll down to select. Click on 'x' to remove selected hosts.&lt;br&gt;If nothing is selected in both *Host groups* and *Hosts* fields, all hosts with valid coordinates will be displayed.&lt;br&gt;This parameter is not available when configuring the widget on a [template dashboard](/manual/config/templates/template#adding-dashboards).|
|*Tags*|Specify tags to limit the number of hosts displayed in the widget.&lt;br&gt;It is possible to include as well as exclude specific tags and tag values. Several conditions can be set. Tag name matching is always case-sensitive.&lt;br&gt;&lt;br&gt;There are several operators available for each condition:&lt;br&gt;**Exists** - include the specified tag names;&lt;br&gt;**Equals** - include the specified tag names and values (case-sensitive);&lt;br&gt;**Contains** - include the specified tag names where the tag values contain the entered string (substring match, case-insensitive);&lt;br&gt;**Does not exist** - exclude the specified tag names;&lt;br&gt;**Does not equal** - exclude the specified tag names and values (case-sensitive);&lt;br&gt;**Does not contain** - exclude the specified tag names where the tag values contain the entered string (substring match, case-insensitive).&lt;br&gt;&lt;br&gt;There are two calculation types for conditions:&lt;br&gt;**And/Or** - all conditions must be met, conditions having the same tag name will be grouped by the *Or* condition;&lt;br&gt;**Or** - enough if one condition is met.&lt;br&gt;&lt;br&gt;This parameter is not available when configuring the widget on a [template dashboard](/manual/config/templates/template#adding-dashboards).|
|*Initial view*|Comma-separated center coordinates and an optional zoom level to display when the widget is initially loaded in the format `&lt;latitude&gt;,&lt;longitude&gt;,&lt;zoom&gt;`&lt;br&gt;If initial zoom is specified, the Geomap widget is loaded at the given zoom level. Otherwise, initial zoom is calculated as half of the [max zoom](/manual/web_interface/frontend_sections/administration/general#geographical_maps) for the particular tile provider.&lt;br&gt;The initial view is ignored if the default view is set (see below).&lt;br&gt;Examples:&lt;br&gt;40.6892494,-74.0466891,14&lt;br&gt;40.6892494,-122.0466891|</source>
      </trans-unit>
      <trans-unit id="620ce415" xml:space="preserve">
        <source>Host markers displayed on the map have the color of the host's most
serious problem and green color if a host has no problems. Clicking on
a host marker allows viewing the host's visible name and the number of
unresolved problems grouped by severity. Clicking on the visible name
will open [host
menu](/manual/web_interface/menu/host_menu).

Hosts displayed on the map can be filtered by problem severity. Press on
the filter icon in the widget's upper right corner and mark the required
severities.

![](../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/geomap_widget_filter.png)</source>
      </trans-unit>
      <trans-unit id="e9bb04a4" xml:space="preserve">
        <source>It is possible to zoom in and out the map by using the plus and minus
buttons in the widget's upper left corner or by using the mouse scroll
wheel or touchpad. To set the current view as default, right-click
anywhere on the map and select *Set this view as default*. This setting
will override *Initial view* widget parameter for the current user. To
undo this action, right-click anywhere on the map again and select
*Reset to initial view*.

When *Initial view* or *Default view* is set, you can return to this
view at any time by pressing on the home icon on the left.

![](../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/geomap_widget3.png)</source>
      </trans-unit>
    </body>
  </file>
</xliff>
