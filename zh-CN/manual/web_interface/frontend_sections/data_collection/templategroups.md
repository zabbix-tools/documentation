[comment]: # translation:outdated

[comment]: # ({new-52e806d3})
# 1 Template groups

[comment]: # ({/new-52e806d3})

[comment]: # ({new-eaab320d})
#### Overview

In the *Data collection → Templates groups* section users can configure and
maintain template groups. 

A listing of existing template groups with their details is displayed. You
can search and filter template groups by name.

![](../../../../../assets/en/manual/web_interface/template_groups.png){width="600"}

Displayed data:

|Column|Description|
|--|--------|
|*Name*|Name of the template group. Clicking on the group name opens the group [configuration form](/manual/config/templates/template#configuring_a_template_group).|
|*Templates*|Number of templates in the group (displayed in gray) followed by the list of group members. <br> Clicking on a template name will open the template configuration form.<br> Clicking on the number opens the list of templates in this group.|

[comment]: # ({/new-eaab320d})

[comment]: # ({new-bd18f145})
##### Mass editing options

To delete several template groups at once, mark the checkboxes before the respective groups, then click on the Delete button below the list.

[comment]: # ({/new-bd18f145})

[comment]: # ({new-e7ca25ce})
##### Using filter

You can use the filter to display only the template groups you are
interested in. For better search performance, data is searched with
macros unresolved.

[comment]: # ({/new-e7ca25ce})
