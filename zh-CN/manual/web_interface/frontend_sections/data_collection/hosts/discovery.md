[comment]: # translation:outdated

[comment]: # ({new-4158fb2b})
# 4 Discovery rules

[comment]: # ({/new-4158fb2b})

[comment]: # ({new-6f1f6e75})
#### Overview

The list of low-level discovery rules for a host can be accessed from
*Data collection → Hosts* by clicking on *Discovery* for the respective
host.

A list of existing low-level discovery rules is displayed. It is also
possible to see all discovery rules independently of the host, or all
discovery rules of a specific host group by changing the filter
settings.

![](../../../../../../assets/en/manual/web_interface/lld_rules.png){width="600"}

Displayed data:

|Column|Description|
|--|--------|
|*Host*|The visible host name is displayed.<br>In the absence of a visible host name, the technical host name is displayed.|
|*Name*|Name of the rule, displayed as a blue link.<br>Clicking on the rule name opens the low-level discovery rule [configuration form](/manual/discovery/low_level_discovery#discovery_rule).<br>If the discovery rule belongs to a template, the template name is displayed before the rule name, as a gray link. Clicking on the template link will open the rule list on the template level.|
|*Items*|A link to the list of item prototypes is displayed.<br>The number of existing item prototypes is displayed in gray.|
|*Triggers*|A link to the list of trigger prototypes is displayed.<br>The number of existing trigger prototypes is displayed in gray.|
|*Graphs*|A link to the list of graph prototypes is displayed.<br>The number of existing graph prototypes is displayed in gray.|
|*Hosts*|A link to the list of host prototypes is displayed.<br>The number of existing host prototypes is displayed in gray.|
|*Key*|The item key used for discovery is displayed.|
|*Interval*|The frequency of performing discovery is displayed.<br>*Note* that discovery can also be performed immediately by pushing the *Execute now* button below the list.|
|*Type*|The item type used for discovery is displayed (Zabbix agent, SNMP agent, etc).|
|*Status*|Discovery rule status is displayed - *Enabled*, *Disabled* or *Not supported*. By clicking on the status you can change it - from Enabled to Disabled (and back); from Not supported to Disabled (and back).|
|*Info*|If everything is fine, no icon is displayed in this column. In case of errors, a square icon with the letter "i" is displayed. Hover over the icon to see a tooltip with the error description.|

To configure a new low-level discovery rule, click on the *Create
discovery rule* button at the top right corner.

[comment]: # ({/new-6f1f6e75})

[comment]: # ({new-6ef754df})
##### Mass editing options

Buttons below the list offer some mass-editing options:

-   *Enable* - change the low-level discovery rule status to *Enabled*.
-   *Disable* - change the low-level discovery rule status to
    *Disabled*.
-   *Execute now* - perform discovery based on the discovery rules
    immediately. See [more details](/manual/config/items/check_now).
    Note that when performing discovery immediately, the configuration
    cache is not updated, thus the result will not reflect very recent
    changes to discovery rule configuration.
-   *Delete* - delete the low-level discovery rules.

To use these options, mark the checkboxes before the respective
discovery rules, then click on the required button.

[comment]: # ({/new-6ef754df})

[comment]: # ({new-7b657ae4})
##### Using filter

You can use the filter to display only the discovery rules you are
interested in. For better search performance, data is searched with
macros unresolved.

The *Filter* link is available above the list of discovery rules. If you
click on it, a filter becomes available where you can filter discovery
rules by host group, host, name, item key, item type, and other
parameters.

![](../../../../../../assets/en/manual/web_interface/lld_rule_filter.png){width="600"}

|Parameter|Description|
|--|--------|
|*Host groups*|Filter by one or more host groups.<br>Specifying a parent host group implicitly selects all nested host groups.|
|*Hosts*|Filter by one or more hosts.|
|*Name*|Filter by discovery rule name.|
|*Key*|Filter by discovery item key.|
|*Type*|Filter by discovery item type.|
|*Update interval*|Filter by update interval.<br>Not available for Zabbix trapper and dependent items.|
|*Keep lost resources period*|Filter by Keep lost resources period.|
|*SNMP OID*|Filter by SNMP OID.<br>Only available if *SNMP agent* is selected as type.|
|*State*|Filter by discovery rule state (All/Normal/Not supported).|
|*Status*|Filter by discovery rule status (All/Enabled/Disabled).|

[comment]: # ({/new-7b657ae4})
