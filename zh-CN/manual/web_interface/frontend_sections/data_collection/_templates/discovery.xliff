<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="zh-CN" datatype="plaintext" original="manual/web_interface/frontend_sections/data_collection/_templates/discovery.md">
    <body>
      <trans-unit id="4158fb2b" xml:space="preserve">
        <source># 4 Discovery rules</source>
      </trans-unit>
      <trans-unit id="5ec32232" xml:space="preserve">
        <source>#### Overview

The list of low-level discovery rules for a template can be accessed
from *Data collection* → *Templates* by clicking on *Discovery* for the
respective template.

A list of existing low-level discovery rules is displayed. It is also
possible to see all discovery rules independently of the template, or
all discovery rules of a specific template group by changing the filter
settings.

![](../../../../../../assets/en/manual/web_interface/template_lld_rules.png){width="600"}

Displayed data:

|Column|Description|
|--|--------|
|*Template*|The template discovery rule belongs to.|
|*Name*|Name of the rule, displayed as a blue link.&lt;br&gt;Clicking on the rule name opens the low-level discovery rule [configuration form](/manual/discovery/low_level_discovery#discovery_rule).&lt;br&gt;If the discovery rule is inherited from another template, the template name is displayed before the rule name, as a gray link. Clicking on the template link will open the discovery rule list on that template level.|
|*Items*|A link to the list of item prototypes is displayed.&lt;br&gt;The number of existing item prototypes is displayed in gray.|
|*Triggers*|A link to the list of trigger prototypes is displayed.&lt;br&gt;The number of existing trigger prototypes is displayed in gray.|
|*Graphs*|A link to the list of graph prototypes displayed.&lt;br&gt;The number of existing graph prototypes is displayed in gray.|
|*Hosts*|A link to the list of host prototypes displayed.&lt;br&gt;The number of existing host prototypes is displayed in gray.|
|*Key*|The item key used for discovery is displayed.|
|*Interval*|The frequency of performing discovery is displayed.|
|*Type*|The item type used for discovery is displayed (Zabbix agent, SNMP agent, etc).|
|*Status*|Discovery rule status is displayed - *Enabled* or *Disabled*. By clicking on the status you can change it - from Enabled to Disabled (and back).|

To configure a new low-level discovery rule, click on the *Create
discovery rule* button at the top right corner.</source>
      </trans-unit>
      <trans-unit id="a1b3e602" xml:space="preserve">
        <source>##### Mass editing options

Buttons below the list offer some mass-editing options:

-   *Enable* - change the low-level discovery rule status to *Enabled*
-   *Disable* - change the low-level discovery rule status to *Disabled*
-   *Delete* - delete the low-level discovery rules

To use these options, mark the checkboxes before the respective
discovery rules, then click on the required button.</source>
      </trans-unit>
      <trans-unit id="de8697ee" xml:space="preserve">
        <source>##### Using filter

You can use the filter to display only the discovery rules you are
interested in. For better search performance, data is searched with
macros unresolved.

The *Filter* icon is available at the top right corner. Clicking on it
will open a filter where you can specify the desired filtering criteria
such as template, discovery rule name, item key, item type, etc.

![](../../../../../../assets/en/manual/web_interface/template_drule_filter.png){width="600"}

|Parameter|Description|
|--|--------|
|*Template groups*|Filter by one or more template groups.&lt;br&gt;Specifying a parent template group implicitly selects all nested groups.|
|*Templates*|Filter by one or more templates.|
|*Name*|Filter by discovery rule name.|
|*Key*|Filter by discovery item key.|
|*Type*|Filter by discovery item type.|
|*Update interval*|Filter by update interval.&lt;br&gt;Not available for Zabbix trapper and dependent items.|
|*Keep lost resources period*|Filter by Keep lost resources period.|
|*Status*|Filter by discovery rule status (All/Enabled/Disabled).|</source>
      </trans-unit>
    </body>
  </file>
</xliff>
