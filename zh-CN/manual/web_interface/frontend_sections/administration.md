[comment]: # translation:outdated

[comment]: # ({new-fa188d70})
# 5 管理

[comment]: # ({/new-fa188d70})

[comment]: # ({new-cded1b68})
## 5 Administration

#### 概述

“管理”菜单用于Zabbix的管理功能。 此菜单仅 [Super
Administrators](/manual/config/users_and_usergroups/permissions) 适用。

#### Overview

The Administration menu is for administrative functions of Zabbix. This
menu is available to users of [Super
Administrators](/manual/config/users_and_usergroups/permissions) type
only.

[comment]: # ({/new-cded1b68})
