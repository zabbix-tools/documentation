[comment]: # translation:outdated

[comment]: # ({new-9ada3186})
# 3 报告

[comment]: # ({/new-9ada3186})

[comment]: # ({new-8b50039d})
#### 简介

“报告”菜单包含多个部分，其中包含各种预定义和用户可自定义的报告，这些报告侧重于显示系统信息，触发器和收集数据等参数的概括。

[comment]: # ({/new-8b50039d})
