[comment]: # translation:outdated

[comment]: # ({new-cad9ee1a})
# 2 主机

[comment]: # ({/new-cad9ee1a})

[comment]: # ({new-50a329ce})
#### 简介

进入方法 *资产记录 → 主机* ，
[资产记录信息](/manual/config/hosts/inventory) 。

通过右上角的下拉菜单选择要查看的主机组信息，当然你也可以通过页面上方中部的关键词过滤来细化操作。

![](../../../../../assets/en/manual/web_interface/inventory_filtered1.png){width="600"}

要显示所有主机清单，在组下拉列表中选择*全部*，清除过滤器中的比较字段，然后按*过滤器*。

虽然表中只显示了一些关键的库存字段，但您也可以查看该主机的所有可用库存信息。如果想这么查看请单击列表中第一个主机的名字。

[comment]: # ({/new-50a329ce})

[comment]: # ({new-1226a553})
#### 资产详情

在 **概览**
选项卡包含有关主机的一些一般信息以及预定义脚本的链接，最新的监视数据和主机配置选项：

![](../../../../../assets/en/manual/web_interface/inventory_host.png){width="600"}

在 **明细** 选项卡包含主机的所有可用库存明细：

![](../../../../../assets/en/manual/web_interface/inventory_host2.png){width="600"}

资产数据的完整性取决于与主机保持多少库存信息。
如果没有维护信息，则*详细信息*禁用。

[comment]: # ({/new-1226a553})
