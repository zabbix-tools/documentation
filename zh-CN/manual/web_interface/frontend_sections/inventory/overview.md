[comment]: # translation:outdated

[comment]: # ({new-86576837})
# 1 概述

[comment]: # ({/new-86576837})

[comment]: # ({new-8655b192})
#### 概述

*库存 -
>概述*部分提供了有关[主机盘点](/manual/config/hosts/inventory)数据概述的方法。

要显示的概述，请选择一个主机组（或所有组）和显示数据的库存字段。将显示与所选字段的每个条目相对应的主机数量。

![](../../../../../assets/en/manual/web_interface/inventory_overview.png){width="600"}

概述的完整性取决于主机维护多少库存信息。

*主机数*列中的数字是链接; 它们导致这些主机在*主机库存*表中被过滤掉。

![](../../../../../assets/en/manual/web_interface/inventory_filtered.png){width="600"}

[comment]: # ({/new-8655b192})
