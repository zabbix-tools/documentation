[comment]: # translation:outdated

[comment]: # ({new-06b863a3})
# 1 Cause and symptom problems

[comment]: # ({/new-06b863a3})

[comment]: # ({new-9c4b3959})
#### Overview

By default all new problems are classified as cause problems. It is possible to manually reclassify certain problems as symptom problems of the cause problem.

For example, power outage may be the actual root cause why some host is unreachable or some service is down. In this case, "host is unreachable" and "service is down" problems must be classified as symptom problems of "power outage" - the cause problem.

The cause-symptom hierarchy supports only two levels. A problem that is already a symptom cannot be assigned "subordinate" symptom problems; any problems assigned as symptoms to a symptom problem will become symptoms of the same cause problem.

Only cause problems are counted in problem totals in maps, dashboard widgets such as *Problems by severity* or *Problem hosts*, etc. However, problem ranking does not affect services.

A symptom problem can be linked to only one cause problem. Symptom problems are not automatically resolved, if the cause problem is resolved or closed.

[comment]: # ({/new-9c4b3959})

[comment]: # ({new-28035598})
#### Configuration

To reclassify a problem as symptom problem, first select it in the list of [problems](/manual/web_interface/frontend_sections/monitoring/problems). One or several problems can be selected.

Then go to the cause problem, and in its context menu click on the *Mark selected as symptoms* option.

![cause\_symptom\_conf.png](../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/cause_symptom_conf.png)

After that, the selected problems will be updated by the server to symptom problems of the cause problem. While that happens, their status column will show UPDATING.

[comment]: # ({/new-28035598})

[comment]: # ({new-13ae56a5})
#### Display

Symptom problems are displayed below the cause problem and marked accordingly in *Monotoring* -> *Problems* (and the *Problems* dashboard widget) - with an icon, smaller font and different background.

![cause\_symptom\_display2.png](../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/cause_symptom_display2.png)

In collapsed view, only the cause problem is seen; the existence of symptom problems is indicated by the number in the beginning of the line and the icon for expanding the view.

![cause\_symptom\_display.png](../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/cause_symptom_display.png)

It is also possible to additionally display symptom problems in normal font and in their own line. For that select *Show symptoms* in the filter settings or the widget configuration.

[comment]: # ({/new-13ae56a5})

[comment]: # ({new-fad06f5a})
#### Reverting to cause problem

A symptom problem can be reverted back to a cause problem. To do that, either:

* click on the *Mark as cause* option in the context menu of the symptom problem;
* mark the *Convert to cause* option in to the [problem update](/manual/acknowledgment#updating-problems) screen and click on *Update* (this option will also work if several problems are selected).

[comment]: # ({/new-fad06f5a})
