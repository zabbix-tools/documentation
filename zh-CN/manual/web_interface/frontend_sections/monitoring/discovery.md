[comment]: # translation:outdated

[comment]: # ({new-a637692c})
# 9 自动发现

[comment]: # ({/new-a637692c})

[comment]: # ({new-6877d127})
#### 简介

进入方法 *检测 → 自动发现* 显示了
[网络发现](/manual/discovery/network_discovery)的部分结果。发现的设备按发现规则排序。

![](../../../../../assets/en/manual/web_interface/discovery_status.png){width="600"}

如果设备已被监控，主机名将列在*监控的主机*列中，并且在上次发现后发现或丢失的设备的持续时间显示在正常运行/停机时间列中。

随后，显示每个发现的设备的各个服务状态的列。
每个单元格的工具提示将显示单独的服务正常运行时间或停机时间。

<note
important>只有在至少一台设备上找到的那些服务才会有一列显示其状态的列。

:::

[comment]: # ({/new-6877d127})



[comment]: # ({new-84099469})
##### Buttons

View mode buttons being common for all sections are described on the
[Monitoring](/manual/web_interface/frontend_sections/monitoring#view_mode_buttons)
page.

[comment]: # ({/new-84099469})

[comment]: # ({new-8b46b0e3})
##### Using filter

You can use the filter to display only the discovery rules you are
interested in. For better search performance, data is searched with
macros unresolved.

With nothing selected in the filter, all enabled discovery rules are
displayed. To select a specific discovery rule for display, start typing
its name in the filter. All matching enabled discovery rules will be
listed for selection. More than one discovery rule can be selected.

[comment]: # ({/new-8b46b0e3})
