[comment]: # translation:outdated

[comment]: # ({new-1c52788a})
# 5 最新数据

[comment]: # ({/new-1c52788a})

[comment]: # ({new-a86c547b})
#### 简介

*监测 → 最新数据* 可以用来查看监控项收集的最新值，以及访问各种项目图表。

第一次打开此页面时，不显示任何内容。

![](../../../../../assets/en/manual/web_interface/latest_data_default1.png){width="600"}

要访问数据，您需要在过滤器中进行选择，例如主机组，主机，应用程序或项目名称。

![](../../../../../assets/en/manual/web_interface/latest_data1.png){width="600"}

在显示的列表中，单击
![](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/expand.png)
在主机和相关应用程序之前显示该主机和应用程序的最新值。
您可以展开所有主机和所有应用程序，从而通过单击显示所有项目
![](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/expand.png)
在标题行中。

*注意*：被禁用主机的名称显示为红色。从Zabbix2.2.0起，被禁用主机数据（包括图表和项目值列表）在*最新数据*页可以被访问。

本页面的列表针对监控项展示以下列：监控项名称、最近检查记录、最后一个值、更改量(Change)以及一个跳转到项目值的简单图表/历史记录的链接。

默认情况下仅显示最近24小时内的数据。这样设置是为了优化页面加载数据的时间。
如果你想查看更多数据也可以到前端文件*include/defines.inc.php* 中更改
ZBX\_HISTORY\_PERIOD的[常量值](/manual/web_interface/definitions) 。

**使用筛选器**

您可以使用筛选器只显示您感兴趣的监控项。*筛选器*
链接位于表格上方中部。您可以使用它来过滤主机群组、主机、应用集、取自监控项名称中的字符串；还可以选择是否显示没有收集到数据的项目（查看无资料项目）。

指定一个父主机组，隐式选择所有嵌套的主机组。

*显示详细信息*
会增加显示监控项相关的以下项目：该监控项的键值、间隔设置、历史记录及趋势的保存时间设置、监控项的类型和监控项的错误（良好/不支持）等详细信息，同时键值是一个链接到监控项配置的超链接。

![](../../../../../assets/en/manual/web_interface/latest_data21.png){width="600"}

默认情况下，会显示没有数据的项目，但不显示详细内容。

**比较项目图表**

可以在第二列的复选框选择几个监控，然后用
[简单图形或堆叠图](/manual/config/visualization/graphs/adhoc)比较它们的数据。选择感兴趣的监控项，然后单击表下所需图形的按钮，即可查看图形。

**链接到值的历史/简单图形**

提供最新值列表中的最后一列：

-   一个 \*\* 历史链接 \*\* （用于所有文本项）-链接到的列表（values/
    500个最新values）显示前一个项目值的历史记录。

```{=html}
<!-- -->
```
-   一个 **图表连接** （用于所有数字项）-链接到一个
    [简单图形](/manual/config/visualization/graphs/simple)。
    图形被调用出来后，从右上角的下拉框也可以切换为显示值(values)或最新*500个值*(500
    latest values)。

![](../../../../../assets/en/manual/web_interface/values1.png){width="600"}

此列表中显示的是“原始的”值，即指未经处理的指。

::: noteclassic
显示的总值由“搜索限制”和“过滤结果”参数值定义，在[管理→
一般](/manual/web_interface/frontend_sections/administration/general)中设置。
:::

[comment]: # ({/new-a86c547b})




[comment]: # ({new-84099469})
##### Buttons

View mode buttons being common for all sections are described on the
[Monitoring](/manual/web_interface/frontend_sections/monitoring#view_mode_buttons)
page.

[comment]: # ({/new-84099469})

[comment]: # ({new-dd13c026})
##### Mass actions

Buttons below the list offer mass actions with one or several selected items:

-   *Display stacked graph* - display a stacked [ad-hoc graph](/manual/config/visualization/graphs/adhoc)
-   *Display graph* - display a simple [ad-hoc graph](/manual/config/visualization/graphs/adhoc)
-   *Execute now* - execute a check for new item values immediately.
    Supported for **passive** checks only (see [more
    details](/manual/config/items/check_now)). This option is available only for hosts with read-write 
    access. Accessing this option for hosts with read-only permissions depends on the 
    [user role](/manual/web_interface/frontend_sections/administration/user_roles) option called 
    *Invoke "Execute now" on read-only hosts*.

To use these options, mark the checkboxes before the respective items,
then click on the required button.

[comment]: # ({/new-dd13c026})

[comment]: # ({new-b6e60197})
##### Using filter

You can use the filter to display only the items you are interested in.
For better search performance, data is searched with macros unresolved.

The *Filter* link is located above the table to the right. You can use
it to filter items by host group, host, a string in the item name and
tags; you can also select to display items that have no data gathered.

Specifying a parent host group implicitly selects all nested host
groups.

*Show details* allows extending displayable information on the items.
Such details as refresh interval, history and trends settings, item
type, and item errors (fine/unsupported) are displayed. A link to item
configuration is also available.

![](../../../../../assets/en/manual/web_interface/latest_data2.png){width="600"}

By default, items without data are shown but details are not displayed.
For better page performance, the *Show items without data* option is
checked and disabled if no host is selected in the filter.

**Ad-hoc graphs for comparing items**

You may use the checkbox in the first column to select several items and
then compare their data in a simple or stacked [ad-hoc
graph](/manual/config/visualization/graphs/adhoc). To do that, select
items of interest, then click on the required graph button below the
table.

**Links to value history/simple graph**

The last column in the latest value list offers:

-   a **History** link (for all textual items) - leading to listings
    (*Values/500 latest values*) displaying the history of previous item
    values.

```{=html}
<!-- -->
```
-   a **Graph** link (for all numeric items) - leading to a [simple
    graph](/manual/config/visualization/graphs/simple). However, once
    the graph is displayed, a dropdown on the upper right offers a
    possibility to switch to *Values/500 latest values* as well.

![](../../../../../assets/en/manual/web_interface/latest_values.png){width="600"}

The values displayed in this list are "raw", that is, no postprocessing
is applied.

::: noteclassic
The total amount of values displayed is defined by the value
of *Limit for search and filter results* parameter, set in
[Administration →
General](/manual/web_interface/frontend_sections/administration/general).
:::

[comment]: # ({/new-b6e60197})
