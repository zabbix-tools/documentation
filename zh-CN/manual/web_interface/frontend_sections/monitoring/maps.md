[comment]: # translation:outdated

[comment]: # ({new-7b37d2bb})
# 8 拓扑图

[comment]: # ({/new-7b37d2bb})

[comment]: # ({new-20a11f83})
#### 简介

进入方法 *监测→ 拓扑图* 您可以配置，管理和查看
[拓扑图](/manual/config/visualization/maps)。

当您打开此部分时，您将看到您访问的最后一张拓扑图或您可以访问的所有拓扑图的列表。
拓扑图列表可以按名称过滤。

自Zabbix 3.0以来，所有拓扑图都可以是公共的或私有的。
所有用户都可以使用公共拓扑图，而私人拓扑图只能由其所有者和拓扑图共享的用户访问。

[comment]: # ({/new-20a11f83})

[comment]: # ({new-c1cb6946})
#### 拓扑图列表

![](../../../../../assets/en/manual/web_interface/map_list1.png){width="600"}

配置信息：

|参数                功|说明|
|-------------------------|------|
|*名称（Name）*      拓扑图|名称. 点击名称 [查看](/manual/web_interface/frontend_sections/monitoring/maps#viewing_maps) 对应的拓扑图.|
|*宽度（Width）*     显示拓|图宽度|
|*高度（Height）*    显示拓|图的高度|
|*操作（Actions）*   两项操|可做:<br>**属性** - 编辑拓扑图整体 [属性](/manual/config/visualization/maps/map#creating_a_map)<br>**结构** - 访问网格化的 [拓扑图元素](/manual/config/visualization/maps/map#adding_elements)来编辑|

[配置创建](/manual/config/visualization/maps/map#creating_a_map)新的拓扑图,点击右上角的//
创建拓扑图*按钮。要从XML文件导入拓扑图，请单击右上角的*导入//按钮。
导入拓扑图的用户将被设置为其所有者。

列表下方的两个按钮提供了一些批量编辑选项：

-   *导出（Export）* - 将拓扑图导出为XML文件
-   *删除（Delete）* - 删除拓扑图

要使用这些选项，请在各个拓扑图之前标记复选框，然后单击所需的按钮。

[comment]: # ({/new-c1cb6946})

[comment]: # ({new-1d312fad})
#### 查看拓扑图

要查看拓扑图，请在所有拓扑图列表中单击其名称。

![](../../../../../assets/en/manual/web_interface/maps.png){width="600"}

您可以使用拓扑图标题栏中的下拉列表来选择要显示的问题触发器的最低严重性级别。标记为
default
的严重性是映射配置中设置的级别。如果拓扑图包含子拓扑图，则导航到子拓扑图将保留较高级别的拓扑图严重性。

[comment]: # ({/new-1d312fad})

[comment]: # ({new-a9b8dc1b})
##### 图标突出显示

如果一个拓扑图元素处于问题状态，则以圆圈突出显示。圆的填充颜色对应于问题触发器的严重性颜色。所选严重性级别以上的问题只会与元素一起显示。如果所有问题都得到承认，则会显示圆圈周围的粗绿色边框。
另外，如果一个主机在 [维护](/manual/maintenance)状态，
则突出显示橙色的填充方块，禁用（未监视）主机以灰色突出显示，填充方块和着重显示只有在图标突出显示复选框被标记在拓扑图的[配置](/manual/config/visualization/maps/map#creating_a_map)中时，才会显示。

[comment]: # ({/new-a9b8dc1b})

[comment]: # ({new-f6436c14})
##### 最近更改的标记

向内指向元素周围的红色三角形表示最近的触发状态变化
-最近30分钟内发生的最近的触发状态更改。
如果触发器状态上的标记元素更改复选框在拓扑图[配置](/manual/config/visualization/maps/map#creating_a_map)中标记，则会显示这些三角形。

[comment]: # ({/new-f6436c14})

[comment]: # ({new-89ad8a4d})
##### 链接

点击拓扑图元素会打开一些包含一些可用链接的菜单。

[comment]: # ({/new-89ad8a4d})

[comment]: # ({new-0e0ddb36})
##### 控件

标题栏中有三个控制按钮：

-   ![](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/edit_map.png) -
    转到拓扑图构造器来编辑地图内容
-   ![](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/add_to_fav.png) -
    添加拓扑图到 [仪表板](dashboard)的小构件功能中
-   ![](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/full_window.png) -
    全屏展示拓扑图

[comment]: # ({/new-0e0ddb36})

[comment]: # ({new-8bceddc5})
##### Readable summary in maps

A hidden "aria-label" property is available allowing map information to
be read with a screen reader. Both general map description and
individual element description is available, in the following format:

-   for map description:
    `<Map name>, <* of * items in problem state>, <* problems in total>.`
-   for describing one element with one problem:
    `<Element type>, Status <Element status>, <Element name>, <Problem description>.`
-   for describing one element with multiple problems:
    `<Element type>, Status <Element status>, <Element name>, <* problems>.`
-   for describing one element without problems:
    `<Element type>, Status <Element status>, <Element name>.`

For example, this description is available:

    'Local network, 1 of 6 elements in problem state, 1 problem in total. Host, Status problem, My host, Free disk space is less than 20% on volume \/. Host group, Status ok, Virtual servers. Host, Status ok, Server 1. Host, Status ok, Server 2. Host, Status ok, Server 3. Host, Status ok, Server 4. '

for the following map:

![](../../../../../assets/en/manual/web_interface/map_aria_label.png){width="600"}

[comment]: # ({/new-8bceddc5})

[comment]: # ({new-15da840f})
##### Referencing a network map

Network maps can be referenced by both `sysmapid` and `mapname` GET
parameters. For example,

    http://zabbix/zabbix/maps.php?mapname=Local%20network

will open the map with that name (Local network).

If both `sysmapid` (map ID) and `mapname` (map name) are specified,
`mapname` has higher priority.

[comment]: # ({/new-15da840f})


[comment]: # ({new-c4642e16})
##### Referencing a network map

Network maps can be referenced by both `sysmapid` and `mapname` GET
parameters. For example,

    http://zabbix/zabbix/zabbix.php?action=map.view&mapname=Local%20network

will open the map with that name (Local network).

If both `sysmapid` (map ID) and `mapname` (map name) are specified,
`mapname` has higher priority.

[comment]: # ({/new-c4642e16})
