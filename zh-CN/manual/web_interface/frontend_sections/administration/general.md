[comment]: # translation:outdated

[comment]: # ({new-fa0262ff})
# 1 常规设置

[comment]: # ({/new-fa0262ff})

[comment]: # ({new-f36c2788})
#### 概述

*管理 - >常规*
部分包含多个用于设置前端相关默认值和自定义Zabbix的屏幕。

右侧的下拉菜单允许您在不同的配置屏幕之间切换。

![](../../../../../assets/en/manual/web_interface/frontend_sections/administration/general_dropdown.png){width="600"}

[comment]: # ({/new-f36c2788})

[comment]: # ({new-4513c0d7})
##### - GUI

此屏幕提供了与前端相关的默认值的定制。

![](../../../../../assets/en/manual/web_interface/frontend_sections/administration/general_gui.png)

配置参数：

|参数                                 描|<|
|------------------------------------------|-|
|*默认主题*                           没有在|个人资料中设置特定的用户的默认主题.|
|*下拉第一个入口*                     不管元素选择|拉列表中的第一个条目是 *全部* 或是 *无*.<br>并且勾选了*记住所选*项,当导航到另一个页面时，下拉列表中的最后一个选定的元素将被记住（而不是默认值）。|
|*限制搜索和过滤结果*                 将在Web界面列|中显示的元素（行）的最大数量，例如，在*监控 - >触发器*或者 *配置 - >主机*.<br>*注意*: 如果设置为例如“50”，则前50个元素将仅显示在所有受影响的前端列表中。如果一些列表包含五十多个元素，那么它的指示将是“+” *"显示\*\* 50 + \*\*发现中的1到50"*. 另外，如果使用过滤，并且仍然有超过50个匹配，则只显示前50个。|
|*最大元素数\                         对于单个在表格内显示*|格单元格中显示的条目，将不再显示此处配置的条目。|
|*启用事件确认*                       此参数定义|Zabbix界面中是否激活了事件确认|
|*显示不久于..的时间\                 此参数定义在“于(天数)*|发器状态”屏幕中显示多少天事件。 默认为7天。|
|*每个触发器显示的最大事件计数*       触发状态屏幕中每个触发器的|大事件数。 默认值为100。|
|*如果Zabbix服务器关闭，则显示警告*   如果无法访问Zabbix|务器（可能会关闭），此参数将使浏览器窗口中显示警告消息。 即使用户向下滚动页面，邮件仍然可见。 如果鼠标移过它，该信息将被暂时隐藏以显示下面的内容。<br>Zabbix 2.0.1之后的版本支持此参数。|

[comment]: # ({/new-4513c0d7})

[comment]: # ({new-2cf4d492})
##### - Housekeeper

家是由Zabbix服务器执行的定期流程。 该过程消除用户删除的过时信息和信息。

![](../../../../../assets/en/manual/web_interface/frontend_sections/administration/general_housekeeper.png)

在本节中，内容任务可以单独启用或禁用每个任务: 事件和警报/
IT服务/审核/用户会话/历史/趋势。如果启用了家政管理，可以设置管理员被删除之前数据记录将保留多少天。

对于历史和趋势，还有其他选择： *覆盖项目历史记录周期* 以及
*覆盖项目趋势期*. 此选项允许全局设置项目历史/趋势将保留多少天，
在这种情况下，覆盖[项目配置](/manual/config/items/item)中*保留历史记录/保留趋势*字段中各个项目的值集。

即使禁用内部管家，也可以覆盖历史/趋势存储期。因此，当使用外部管家时，可以使用历史记录*数据存储期间*字段设置历史存储期。

*恢复默认值* 按钮可以恢复所做的任何更改。

[comment]: # ({/new-2cf4d492})



[comment]: # ({new-6905e224})
##### - 正则表达式

此部分允许创建可在前端的多个位置使用的自定义正则表达式。参见[正则表达式](/manual/regular_expressions)
细节。

[comment]: # ({/new-6905e224})

[comment]: # ({new-f8d0c75f})
##### - 宏

本节允许定义系统范围的宏。

![](../../../../../assets/en/manual/web_interface/frontend_sections/administration/general_macros.png)

更多细节，参见[用户宏](/manual/config/macros/user_macros)。

[comment]: # ({/new-f8d0c75f})

[comment]: # ({new-b9fd47e9})
##### - 值映射

本部分允许管理对于Zabbix前端中输入数据的可读表示有用的值映射。

![](../../../../../assets/en/manual/web_interface/value_maps.png){width="600"}

更多细节，参见[值映射](/manual/config/items/mapping)。

[comment]: # ({/new-b9fd47e9})


[comment]: # ({new-cca854b5})
##### -触发严重级

此部分允许自定义[触发严重级](/manual/config/triggers/severity)名称和颜色

![](../../../../../assets/en/manual/web_interface/frontend_sections/administration/general_severities.png){width="600"}

此部分允许自定义触发严重性您可以输入新的名称和颜色代码，或单击颜色以从提供的调色板中选择另一个。

更多信息，请参见
[自定义触发严重级](/manual/config/triggers/customseverities)页面。

[comment]: # ({/new-cca854b5})

[comment]: # ({new-09d9d657})
##### - 触发显示选项

此部分允许自定义触发状态在前端中的显示方式。

![](../../../../../assets/en/manual/web_interface/frontend_sections/administration/general_trigger_display.png)

确认/未确认事件的颜色可以自定义并启用或禁用闪烁。

此外，可以定制显示OK触发的时间段和触发状态更改时的闪烁时间。
最大值为86400秒（24小时）。

[comment]: # ({/new-09d9d657})

[comment]: # ({new-ef76d5b4})
##### - 其他参数

此部分允许配置其他前端参数。

![](../../../../../assets/en/manual/web_interface/frontend_sections/administration/general_other.png)

|参数                           描|<|<|
|------------------------------------|-|-|
|*刷新不支持项（以秒为单位）*   由于用户参数错误或代理商|支持某些项目，某些项目可能会不受支持。Zabbix可以配置为定期使不受支持的项目处于活动状态。<br>Zabbix将在此处设置N秒钟激活不受支持的项目。 如果设置为0，则自动激活将被禁用。<br>代理检查每10分钟不支持的项目。 这是不可配置的代理。|<|
|*发现主机组*                   被[网络|现](/manual/discovery/network_discovery) 和[agent自动注册](/manual/discovery/auto_registration)发现的主机 将自动放置在主机组中，此处选择。|<|
|*默认主机库存模式*             主机库存默认|模式](/manual/config/hosts/inventory) 。 每当由服务器或前端创建新的主机或主机原型时都是可循的，除非在主机发现/自动注册中被//设置主机库存模式 *选项覆盖。 \| \|*数据库关闭消息的用户组//   用户组发送报警信息或“无”。<br>|<Zabbix服务器的可用性取决于后端数据库的可用性。如果没有数据库，它不能工作。 **Database watchdog**, 一个特殊的Zabbix服务器进程，会在遇到灾难时对选定的用户进行报警。如果数据库关闭，watchdog将使用所有配置的用户媒体条目向此处设置的用户组发送通知。Zabbix服务器不会停止; 它将等到数据库再次重新继续处理。<br>*Note*: 直到Zabbix版本1.8.2数据库watchdog才支持MySQL。 自1.8.2以来，它支持所有数据库。|
|*Log unmatched SNMP traps*|如果没有找到相应的SNMP接口。查看日志 [SNMP trap](/manual/config/items/itemtypes/snmptrap) 。|<|

[comment]: # ({/new-ef76d5b4})

[comment]: # ({new-72ef5238})
### 1 General

[comment]: # ({/new-72ef5238})

[comment]: # ({new-0e5d17fd})
#### - Connectors

This section allows to configure connectors for Zabbix data [streaming to external systems](/manual/config/export/streaming) over HTTP.

![](../../../../../assets/en/manual/web_interface/frontend_sections/administration/general_connectors.png){width="600"}

Click on *Create connector* to configure a new [connector](/manual/config/export/streaming#configuration).

You may filter connectors by name or status (enabled/disabled). Click on the connector status in the list to enable/disable a connector. 
You may also mass enable/disable connectors by selecting them in the list and then clicking on the *Enable/Disable* buttons below the list.

[comment]: # ({/new-0e5d17fd})


[comment]: # ({new-10e8eeaa})
##### - GUI

This screen provides customization of several frontend-related defaults.

![](../../../../../assets/en/manual/web_interface/frontend_sections/administration/general_gui.png)

Configuration parameters:

|Parameter|Description|
|---------|-----------|
|*Default theme*|Default theme for users who have not set a specific one in their profiles.|
|*Dropdown first entry*|Whether first entry in element selection dropdowns should be *All* or *None*.<br>With *remember selected* checked, the last selected element in the dropdown will be remembered (instead of the default) when navigating to another page.|
|*Limit for search and filter results*|Maximum amount of elements (rows) that will be displayed in a web-interface list, like, for example, in *Configuration → Hosts*.<br>*Note*: If set to, for example, '50', only the first 50 elements will be displayed in all affected frontend lists. If some list contains more than fifty elements, the indication of that will be the '+' sign in *"Displaying 1 to 50 of **50+** found"*. Also, if filtering is used and still there are more than 50 matches, only the first 50 will be displayed.|
|*Max count of elements<br>to show inside table cell*|For entries that are displayed in a single table cell, no more than configured here will be shown.|
|*Show warning if Zabbix server is down*|This parameter enables a warning message to be displayed in the browser window if Zabbix server cannot be reached (may be down). The message remains visible even if the user scrolls down the page. If the mouse is moved over it, the message is temporarily hidden to reveal the contents below.<br>This parameter is supported since Zabbix 2.0.1.|

[comment]: # ({/new-10e8eeaa})

[comment]: # ({new-1ff15253})
##### - Housekeeper

The housekeeper is a periodical process, executed by Zabbix server. The
process removes outdated information and information deleted by user.

![](../../../../../assets/en/manual/web_interface/frontend_sections/administration/general_housekeeper1.png)

In this section housekeeping tasks can be enabled or disabled on a
per-task basis separately for: events and alerts/IT services/audit/user
sessions/history/trends. If housekeeping is enabled, it is possible to
set for how many days data records will be kept before being removed by
the housekeeper.

Deleting an item/trigger will also delete problems generated by that
item/trigger.

Also, an event will only be deleted by the housekeeper if it is not
associated with a problem in any way. This means that if an event is
either a problem or recovery event, it will not be deleted until the
related problem record is removed. The housekeeper will delete problems
first and events after, to avoid potential problems with stale events or
problem records.

For history and trends an additional option is available: *Override item
history period* and *Override item trends period*. This option allows to
globally set for how many days item history/trends will be kept, in this
case overriding the values set for individual items in *Keep
history/Keep trends* fields in [item
configuration](/manual/config/items/item).

It is possible to override the history/trend storage period even if
internal housekeeping is disabled. Thus, when using an external
housekeeper, the history storage period could be set using the history
*Data storage period* field.

[Time suffixes](/manual/appendix/suffixes) are supported in the period
fields, e.g. 1d (one day), 1w (one week). Minimum is 1 day (1 hour for
history), maximum 25 years.

*Reset defaults* button allows to revert any changes made.

[comment]: # ({/new-1ff15253})

[comment]: # ({new-99496c72})
##### Storage of secrets

*Vault provider* parameter allows selecting secret management software for storing [user macro](/manual/config/macros/user_macros#configuration) values. Supported options: 
- *HashiCorp Vault* (default)
- *CyberArk Vault*

See also: [Storage of secrets](/manual/config/secrets).

[comment]: # ({/new-99496c72})

[comment]: # ({new-2b1937dd})
##### - Images

The Images section displays all the images available in Zabbix. Images
are stored in the database.

![](../../../../../assets/en/manual/web_interface/frontend_sections/administration/general_images.png){width="600"}

The *Type* dropdown allows you to switch between icon and background
images:

-   Icons are used to display [network
    map](/manual/config/visualization/maps/map) elements
-   Backgrounds are used as background images of network maps

**Adding image**

You can add your own image by clicking on the *Create icon* or *Create
background* button in the top right corner.

![](../../../../../assets/en/manual/web_interface/frontend_sections/administration/general_img_upload.png)

Image attributes:

|Parameter|Description|
|---------|-----------|
|*Name*|Unique name of an image.|
|*Upload*|Select the file (PNG, JPEG) from a local system to be uploaded to Zabbix.|

::: noteclassic
Maximum size of the upload file is limited by value of
ZBX\_MAX\_IMAGE\_SIZE that is 1024x1024 bytes or 1 MB.\
\
The upload of an image may fail if the image size is close to 1 MB and
the `max_allowed_packet` MySQL configuration parameter is at a default
of 1MB. In this case, increase the
[max\_allowed\_packet](http://dev.mysql.com/doc/refman/5.5/en/server-system-variables.html#sysvar_max_allowed_packet)
parameter.
:::

[comment]: # ({/new-2b1937dd})

[comment]: # ({new-c8d6c320})
##### - Icon mapping

This section allows to create the mapping of certain hosts with certain
icons. Host inventory field information is used to create the mapping.

The mappings can then be used in [network map
configuration](/manual/config/visualization/maps/map) to assign
appropriate icons to matching hosts automatically.

To create a new icon map, click on *Create icon map* in the top right
corner.

![](../../../../../assets/en/manual/web_interface/frontend_sections/administration/general_iconmap.png){width="600"}

Configuration parameters:

|Parameter|Description|
|---------|-----------|
|*Name*|Unique name of icon map.|
|*Mappings*|A list of mappings. The order of mappings determines which one will have priority. You can move mappings up and down the list with drag-and-drop.|
|*Inventory field*|Host inventory field that will be looked into to seek a match.|
|*Expression*|Regular expression describing the match.|
|*Icon*|Icon to use if a match for the expression is found.|
|*Default*|Default icon to use.|

##### - Regular expressions

This section allows to create custom regular expressions that can be
used in several places in the frontend. See [Regular
expressions](/manual/regular_expressions) section for details.

##### - Macros

This section allows to define system-wide macros.

![](../../../../../assets/en/manual/web_interface/frontend_sections/administration/general_macros.png)

See [User macros](/manual/config/macros/user_macros) section for more
details.

##### - Value mapping

This section allows to manage value maps that are useful for
human-readable representation of incoming data in Zabbix frontend.

![](../../../../../assets/en/manual/web_interface/value_maps.png){width="600"}

See [Value mapping](/manual/config/items/mapping) section for more
details.

##### - Working time

Working time is system-wide parameter, which defines working time.
Working time is displayed as a white background in graphs, while
non-working time is displayed in grey.

![](../../../../../assets/en/manual/web_interface/frontend_sections/administration/general_worktime.png)

See [Time period specification](/manual/appendix/time_period) page for
description of the time format. [User
macros](/manual/config/macros/user_macros) are supported (since Zabbix
3.4.0).

##### - Trigger severities

This section allows to customize [trigger
severity](/manual/config/triggers/severity) names and colors.

![](../../../../../assets/en/manual/web_interface/frontend_sections/administration/general_severities.png){width="600"}

You can enter new names and color codes or click on the color to select
another from the provided palette.

See [Customising trigger
severities](/manual/config/triggers/customseverities) page for more
information.

##### - Trigger displaying options

This section allows to customize how trigger status is displayed in the
frontend.

![](../../../../../assets/en/manual/web_interface/frontend_sections/administration/general_trigger_display1.png)

Checking "*Use custom event status colors*" checkbox enables
customization of the colors for acknowledged/unacknowledged events.
Unchecking this checkbox disables this customization, respectively.
Blinking isn't affected by this checkbox.

Also the time period for displaying OK triggers and for blinking upon
trigger status change can be customized. The maximum value is 86400
seconds (24 hours). [Time suffixes](/manual/appendix/suffixes) are
supported in the period fields, e.g. 5m, 2h, 1d.

##### - Other parameters

This section allows to configure several other frontend parameters.

![](../../../../../assets/en/manual/web_interface/frontend_sections/administration/general_other.png)

|Parameter|Description|<|
|---------|-----------|-|
|*Refresh unsupported items*|Some items may become unsupported due to errors in user parameters or because of an item not being supported by agent. Zabbix can be configured to periodically make unsupported items active.<br>Zabbix server will activate unsupported item every N period set here (1 day maximum). If set to 0, the automatic activation will be disabled.<br>[Time suffixes](/manual/appendix/suffixes) are supported, e.g. 60s, 5m, 2h, 1d.<br>The configured value also applies to how often Zabbix proxies reactivate unsupported items.|<|
|*Group for discovered hosts*|Hosts discovered by [network discovery](/manual/discovery/network_discovery) and [agent auto-registration](/manual/discovery/auto_registration) will be automatically placed in the host group, selected here.|<|
|*Default host inventory mode*|Default [mode](/manual/config/hosts/inventory) for host inventory. It will be followed whenever a new host or host prototype is created by server or frontend, unless overriden during host discovery/auto registration by the //Set host inventory mode *operation. \| \|*User group for database down message//|User group for sending alarm message or 'None'.<br>Zabbix server depends on the availability of backend database. It cannot work without a database. If the database is down, selected users can be notified by Zabbix. Notifications will be sent to the user group set here using all configured user media entries. Zabbix server will not stop; it will wait until the database is back again to continue processing.<br>Notification consists of the following content:<br>`[MySQL\|PostgreSQL\|Oracle\|IBM DB2] database <DB Name> [on <DB Host>:<DB Port>] is not available: <error message depending on the type of DBMS (database)>`<br><DB Host> is not added to the message if it is defined as an empty value and <DB Port> is not added if it is the default value ("0"). The alert manager (a special Zabbix server process) tries to establish a new connection to the database every 10 seconds. If the database is still down the alert manager repeats sending alerts, but not more often than every 15 minutes.|
|*Log unmatched SNMP traps*|Log [SNMP trap](/manual/config/items/itemtypes/snmptrap) if no corresponding SNMP interfaces have been found.|<|

[comment]: # ({/new-c8d6c320})
