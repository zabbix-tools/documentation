[comment]: # translation:outdated

[comment]: # ({new-66bd8c75})
# 5 Audit log

[comment]: # ({/new-66bd8c75})

[comment]: # ({new-b503e694})

#### Overview

In the *Reports → Audit log* section users can view records of changes made
in the frontend.

::: noteclassic
 Audit logging should be enabled in the Administration
[settings](/manual/web_interface/frontend_sections/administration/audit_log)
to display audit records. If logging is disabled, history of frontend
changes does not get recorded to the database and audit records cannot
be viewed. 
:::

![](../../../../../assets/en/manual/web_interface/audit_log.png){width="600"}

Audit log displays the following data:

|Column|Description|
|--|--------|
|*Time*|Timestamp of the audit record.|
|*User*|User who performed the activity.|
|*IP*|IP from which the activity was initiated.|
|*Resource*|Type of the affected resource (host, host group, etc.).|
|*Action*|Activity type: *Login, Logout, Added, Updated, Deleted, Enabled*, or *Disabled*.|
|*ID*|ID of the affected resource. Clicking on the hyperlink will result in filtering audit log records by this resource ID.|
|*Recordset ID*|Shared ID for all audit log records created as a result of the same frontend operation. For example, when linking a template to a host, a separate audit log record is created for each inherited template entity (item, trigger, etc.) - all these records will have the same Recordset ID.<br>Clicking on the hyperlink will result in filtering audit log records by this Recordset ID.|
|*Details*|Description of the resource and detailed information about the performed activity. If a record contains more than two rows, an additional link Details will be displayed. Click on this link to view the full list of changes.|

[comment]: # ({/new-b503e694})

[comment]: # ({new-d73129d8})

#### Using filter

The filter is located below the *Audit log* bar. It can be opened and
collapsed by clicking on the *Filter* tab in the upper right corner.

![](../../../../../assets/en/manual/web_interface/audit_log_filter.png){width="600"}

You may use the filter to narrow down the records by user, affected resource, resource ID and 
frontend operation (Recordset ID). One or more actions (e. g., add, update, delete, etc) for 
the resource can be selected in the filter.

For better search performance, all data is searched with macros unresolved.

[comment]: # ({/new-d73129d8})

[comment]: # ({new-23b43559})

#### Time period selector

The [time period
selector](/manual/config/visualization/graphs/simple#time_period_selector)
allows to select often required periods with one mouse click. The time
period selector can be opened by clicking on the time period tab next to
the filter.

[comment]: # ({/new-23b43559})
