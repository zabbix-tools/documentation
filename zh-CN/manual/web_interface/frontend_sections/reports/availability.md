[comment]: # translation:outdated

[comment]: # ({new-d12677af})
# 2 可用性报告

[comment]: # ({/new-d12677af})

[comment]: # ({new-75fed46f})
#### 简介

在*报表 -
>可用性报表*中，您可以看到每个触发器在问题/状态中的时间比例。
显示每个状态的时间百分比。

因此，很容易确定系统上各种元素的可用性情况。

![](../../../../../assets/en/manual/web_interface/availability_host.png){width="600"}

从右上角的下拉菜单中，您可以选择选择模式 -
是否显示主机或属于模板的触发器。然后在过滤器中，您可以将选择范围缩小到所需的选项和时间段。

![](../../../../../assets/en/manual/web_interface/availability_trigger.png){width="600"}

触发器的名称是指向该触发器的最新事件的链接。

**过滤器**

您可以使用过滤器来缩小选择范围。
指定父主机组会连同选择所有嵌套的主机组。

过滤器位于*可用性报表*栏下方。 可以通过单击左侧的// 过滤
//选项卡打开和折叠它。

**时间选择器**

[时间选择器](/manual/config/visualisation/graphs/simple#time_period_selector)
允许通过单击鼠标选择经常需要的时间段。
单击过滤器旁边的时间段选项卡可以打开时间段选择器。

点击“图形”列中的*显示*显示一个条形图，其中可用性信息以条形格式显示，表示当前年份过去一周的每个条。

![](../../../../../assets/en/manual/web_interface/availability_bar.png){width="600"}

绿色部分代表OK时间，红色表示异常时间。

[comment]: # ({/new-75fed46f})





[comment]: # ({new-f7f02710})
#### Using filter

The filter can help narrow down the number of hosts and/or triggers
displayed. For better search performance, data is searched with macros
unresolved.

The filter is located below the *Availability report* bar. It can be
opened and collapsed by clicking on the *Filter* tab on the left.

[comment]: # ({/new-f7f02710})

[comment]: # ({new-51ddfece})
##### Filtering by trigger template

In the *by trigger template* mode results can be filtered by one or
several parameters listed below.

|Parameter|Description|
|---------|-----------|
|*Template group*|Select all hosts with triggers from templates belonging to that group. Any host group that includes at least one template can be selected.|
|*Template*|Select hosts with triggers from the chosen template and all nested templates. Only triggers inherited from the selected template will be displayed. If a nested template has additional own triggers, those triggers will not be displayed.|
|//Template trigger //|Select hosts with chosen trigger. Other triggers of the selected hosts will not be displayed.|
|*Host group*|Select hosts belonging to the group.|

[comment]: # ({/new-51ddfece})

[comment]: # ({new-22214967})
##### Filtering by host

In the *by host* mode results can be filtered by a host or by the host
group. Specifying a parent host group implicitly selects all nested host
groups.

[comment]: # ({/new-22214967})

[comment]: # ({new-0533be8a})
#### Time period selector

The [time period
selector](/manual/config/visualization/graphs/simple#time_period_selector)
allows to select often required periods with one mouse click. The time
period selector can be opened by clicking on the time period tab next to
the filter.

Clicking on *Show* in the Graph column displays a bar graph where
availability information is displayed in bar format each bar
representing a past week of the current year.

![](../../../../../assets/en/manual/web_interface/availability_bar.png){width="600"}

The green part of a bar stands for OK time and red for problem time.

[comment]: # ({/new-0533be8a})
