[comment]: # translation:outdated

[comment]: # ({new-58cc1743})
# 6 通知

[comment]: # ({/new-58cc1743})

[comment]: # ({new-c29272bd})
#### 概述

在 *报告 - >通知* 栏中， 将显示发送给每个用户的通知数量的报告。

从右上角的下拉菜单中，您可以选择媒体类型（或全部），周期（每天/周/月/年的数据）和发送通知的年份。

![](../../../../../assets/en/manual/web_interface/notifications.png){width="600"}

每列显示每个系统用户的总计数。

[comment]: # ({/new-c29272bd})
