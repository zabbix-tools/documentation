[comment]: # translation:outdated

[comment]: # ({new-6e32ec39})
# 2 Scheduled reports

[comment]: # ({/new-6e32ec39})

[comment]: # ({new-94e249fa})
#### Overview

In the *Reports → Scheduled reports* users with sufficient can configure
scheduled generation of PDF versions of the dashboards, which will be
sent by email to specified recipients.

![](../../../../../assets/en/manual/web_interface/scheduled_overview.png){width="600"}

The opening screen displays information about scheduled reports, which
can be filtered out for easy navigation - see [Using
filter](#using_filter) section below.

Displayed data:

|Column|Description|
|------|-----------|
|Name|Name of the report|
|Owner|User that created the report|
|Repeats|Report generation frequency (daily/weekly/monthly/yearly)|
|Period|Period for which the report is prepared|
|Last sent|The date and time when the latest report has been sent|
|Status|Current status of the report (enabled/disabled/expired). Users with sufficient permissions can change the status by clicking on it - from Enabled to Disabled (and back); from Expired to Disabled (and back). Displayed as a text for users with insufficient rights.|
|Info|Displays informative icons:<br>A red icon indicates that report generation has failed; hovering over it will display a tooltip with the error information.<br>A yellow icon indicates that a report was generated, but sending to some (or all) recipients has failed or that a report is expired; hovering over it will display a tooltip with additional information|

[comment]: # ({/new-94e249fa})

[comment]: # ({new-39dd2c80})
##### Using filter

You may use the filter to narrow down the list of reports. For better
search performance, data is searched with macros unresolved.

The following filtering options are available:

-   Name - partial name match is allowed;
-   Report owner - created by current user or all reports;
-   Status - select between any (show all reports), enabled, disabled,
    or expired.

The filter is located above the Scheduled reports bar. It can be opened
and collapsed by clicking on the Filter tab in the upper right corner.

[comment]: # ({/new-39dd2c80})

[comment]: # ({new-814fdf02})
##### Mass update

Sometimes you may want to change status or delete a number of reports at
once. Instead of opening each individual report for editing, you may use
the mass update function for that.

To mass-update some reports, do the following:

-   Mark the checkboxes of the reports to update in the list
-   Click on the required button below the list to make changes (Enable,
    Disable or Delete).

[comment]: # ({/new-814fdf02})
