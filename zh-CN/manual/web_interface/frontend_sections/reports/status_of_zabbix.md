[comment]: # translation:outdated

[comment]: # ({new-290553c7})
# 1 系统信息

[comment]: # ({/new-290553c7})

[comment]: # ({new-ede74e69})
#### 概述

在*报表 - > Zabbix的状态*显示关键系统数据的摘要。

![](../../../../../assets/en/manual/web_interface/report_status.png){width="600"}

此报告也显示为[仪表板](/manual/web_interface/frontend_sections/monitoring/dashboard)中的小构件

[comment]: # ({/new-ede74e69})

[comment]: # ({new-bfa24672})
##### 显示数据

|参数                                                                                   值|功能|绍|
|--------------------------------------------------------------------------------------------|------|---|
|*Zabbix服务器正在运行（Zabbix server is running）*                                     Zabbix服务|的状态：\                                                                                                                                                    Zabbix服务器的位置和端口**Yes** - 服务器正在运行<br>**No** - 服务器没有运行<br>*Note:* 为了确保Web前端知道服务器正在运行，服务器上必须至少有一个Trapper进程（ [zabbix\_server.conf](/manual/appendix/config/zabbix_server)文件中的StartTrappers参数）|<|
|*主机数量（Number of hosts）*                                                          显示配置的|机总数。\\\\模板也算作主机类型。                                                                                                                             受监控主机数量/未监控主机/模板数。|<|
|*监控项的个数（Number of items）*                                                      显示监控项总数|仅指的是分配来启用主机的项目。                                                                                                                           受监控/禁用/不受支持的项目数。|<|
|*触发器数量（Number of triggers）*                                                     显示触发总数|只有分配给启用的主机和根据启用的项目的触发计数。                                                                                                          启用/禁用触发器。 \[触发问题/ ok状态.\]|<|
|*用户数量（Number of users）*                                                          显示配置的|户总数。                                                                                                                                                     N在线人数。|<|
|*所需的服务器性能，每秒新的值（Required server performance, new values per second）*   显示Zabbix服务器每秒处理|新值的预期数量。                                                                                                                               *所需服务器性能* 一个估计值，可以作为指导。要精确处理的数值，请|用'zabbix \[wcache，values，all\]''<br>\\\\计算中包含受监控主机的启用项目。日志项目被计为每个项目更新间隔的一个值. 定期间隔值被计数; 灵活和调度间隔值不是。“节点”维护期间的计算未进行调整。 trapper项目不计算在内。|

[comment]: # ({/new-bfa24672})


[comment]: # ({new-76697959})
#### High availability nodes

If [high availability cluster](/manual/concepts/server/ha) is enabled,
then another block of data is displayed with the status of each high
availability node.

![](../../../../../assets/en/manual/web_interface/ha_nodes.png){width="600"}

Displayed data:

|Column|Description|
|------|-----------|
|*Name*|Node name, as defined in server configuration.|
|*Address*|Node IP address and port.|
|*Last access*|Time of node last access.<br>Hovering over the cell shows the timestamp of last access in long format.|
|*Status*|Node status:<br>**Active** - node is up and working<br>**Unavailable** - node hasn't been seen for more than failover delay (you may want to find out why)<br>**Stopped** - node has been stopped or couldn't start (you may want to start it or delete it)<br>**Standby** - node is up and waiting|

[comment]: # ({/new-76697959})
