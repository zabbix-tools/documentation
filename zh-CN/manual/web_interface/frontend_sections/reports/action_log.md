[comment]: # translation:outdated

[comment]: # ({new-7d551978})
# 5 动作日志

[comment]: # ({/new-7d551978})

[comment]: # ({new-8be899f9})
#### 简介

在此页面中，将显示在操作中执行的操作（通知，远程命令）的详细信息。

![](../../../../../assets/en/manual/web_interface/action_log.png){width="600"}

设置信息:

|参数                     功|说明|
|------------------------------|------|
|*时间戳（Time）*         操作时间|<|
|*操作（Action）*         显示导|operation的action名称。<br>Zabbix 2.4.0之后的版本可以显示动作名称。|
|*类型（Type）*           显示操|类型 - *邮件* 或者 *命令*.|
|*别名（Recipient(s)）*   显示用|别名，姓名（括号中）和通知收件人的电子邮件地址。<br>Zabbix 2.4.0之后的版本可以显示显示用户别名，姓名|
|*消息（Message）*        将显示|息/远程命令的内容。|
|*状态（Status）*         显示操|状态：<br>*进行中* - actions正在进行中\\\\对于正在进行的actions，显示剩余的重试次数 - 服务器将尝试发送通知的剩余次数。<br>*已发送* - 通知已发送<br>*已执行* - 命令已执行<br>*未发送* - action并未结束|
|*信息（Info）*           显示有|操作执行的错误信息（如果有）。|

**使用过滤器**

你可以通过过滤收件人、时间等信息来缩小查看范围。

过滤器位于*动作日志*导航栏下方。 可以通过单击左侧的// 过滤
//选项卡打开和折叠它。

**时间选择器**

[时间选择器](/manual/config/visualisation/graphs/simple#time_period_selector)
允许通过单击鼠标选择经常需要的时间段。
单击过滤器旁边的时间段选项卡可以打开时间段选择器。

[comment]: # ({/new-8be899f9})

[comment]: # ({new-d84e7bb0})
#### Buttons

The button at the top right corner of the page offers the following option:

|   |   |
|--|--------|
|![](../../../../../assets/en/manual/web_interface/button_csv.png)|Export action log records from all pages to a CSV file. If a filter is applied, only the filtered records will be exported.<br>In the exported CSV file the columns "Recipient" and "Message" are divided into several columns - "Recipient's Zabbix username", "Recipient's name", "Recipient's surname", "Recipient", and "Subject", "Message", "Command".|

[comment]: # ({/new-d84e7bb0})

[comment]: # ({new-accea306})
#### Using filter

The filter is located below the *Action log* bar.
It can be opened and collapsed by clicking on the *Filter* tab at the top right corner of the page.

![](../../../../../assets/en/manual/web_interface/action_log_filter.png){width="600"}

You may use the filter to narrow down the records by notification recipients, actions, media types, status,
or by the message/remote command content (*Search string*).
For better search performance, data is searched with macros unresolved.

[comment]: # ({/new-accea306})

[comment]: # ({new-3eac5e48})
#### Time period selector

The [time period selector](/manual/config/visualization/graphs/simple#time_period_selector) allows to select often required periods with one mouse click.
The time period selector can be opened by clicking on the time period tab next to the filter.

[comment]: # ({/new-3eac5e48})
