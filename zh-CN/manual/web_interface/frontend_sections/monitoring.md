[comment]: # translation:outdated

[comment]: # ({new-7b41e2d5})
# 1 监测

[comment]: # ({/new-7b41e2d5})

[comment]: # ({new-c99a8958})
#### 简介

所有的监控数据都会在此模块中展示。
你可以通过进行简单的配置把你需要展现的拓扑图、告警、聚合图形在此模块进行展示。

[comment]: # ({/new-c99a8958})

[comment]: # ({new-e7927a8f})
#### View mode buttons

The following buttons located in the top right corner are common for
every section:

|   |   |
|---|---|
|![](../../../../assets/en/manual/web_interface/button_kiosk.png)|Display page in kiosk mode. In this mode only page content is displayed.<br>To exit kiosk mode, move the mouse cursor until the ![](../../../../assets/en/manual/web_interface/button_kiosk_leave.png) exit button appears and click on it. You will be taken back to normal mode.|

[comment]: # ({/new-e7927a8f})

