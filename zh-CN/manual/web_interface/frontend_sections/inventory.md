[comment]: # translation:outdated

[comment]: # ({new-7ed38ae8})
# 2 库存

[comment]: # ({/new-7ed38ae8})

[comment]: # ({new-65644c90})
#### 概述

“库存”菜单具有部分，根据所选参数概述主机库存数据，以及查看主机库存明细的功能。

[comment]: # ({/new-65644c90})
