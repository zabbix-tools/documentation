[comment]: # translation:outdated

[comment]: # ({new-0429da2a})
# 2 Sound in browsers浏览器中的声音

[comment]: # ({/new-0429da2a})

[comment]: # ({new-ca9dfcae})
#### Overview概述

For the sounds to be played in Zabbix frontend, *Frontend messaging*
must be enabled in the user profile *Messaging* tab, with all trigger
severities checked, and sounds should also be enabled in the global
notification pop-up window. 为了在Zabbix前端播放声音，Frontend messaging
必须在用户档案里的 Messaging
选项卡里被启用，并检查所有触发器的严重程度，同时声音也必须在全局通知的弹窗里被启用。

The sounds of Zabbix frontend have been successfully tested in the
following web browser versions and no additional configuration was
required:
Zabbix前端的声音已经在以下Web浏览器版本中成功测试，且不需要其的配置:

-   Firefox 3.5.16 on Linux Linux上的Firefox 3.5.16
-   Opera 11.01 on Linux Linux上的Opera 11.01
-   Google Chrome 9.0 on Windows Windows上的Google Chrome 9.0
-   Firefox 3.5.16 on Windows Windows上的Firefox 3.5.16
-   IE7 browser on Windows Windows上的IE7
-   Opera v11.01 on Windows Windows上的Opera v11.01
-   Chrome v9.0 on Windows Windows山的Chrome v9.0
-   Safari v5.0 on Windows, but this browser requires *Quick Time
    Player* to be installed Windows上的Safari v5.0, 但该浏览器需要安装
    Quick Time Player

#### Additional requirements附加要求

##### Firefox v 3.5.16

For playing `wav` files in the Firefox browser you can use one of the
following applications:
要在Firefox浏览器中播放wav文件，您可以使用以下应用：

-   Windows Media Player
-   Quick Time plug-in. Quick Time 插件

Then, in *Tools → Options → Applications*, in "Wave sound (audio/wav)"
set Windows Media Player to play these files. 然后，在 Tools（工具） →
Options（选项） → Applications（应用程序）, 中，“Wave sound（audio /
wav）”中设置Windows Media Player播放这些文件

##### Safari 5.0

*Quick Time Player* is required. 需要Quick Time Player.

##### Microsoft Internet Explorer

To play sounds in MSIE7 and MSIE8: 在 IE7和IE8中播放声音:

-   In *Tools → Internet Options → Advanced* enable *Play sounds in
    webpages*
-   In *Tools → Manage Add-ons...* enable **Windows Media Player**
-   In the Windows Media Player, in *Tools→Options→File Types* enable
    *Windows audio file (wav)*

In the Windows Media Player, in Tools→Options tab, "File Types" is only
available if the user is a member of "Power Users" or "Administrators"
group, i.e. a regular user does not have access to this tab and does not
see it.在Windows Media Player的“Tools”→“Options”选项卡中，“File
Types”仅在用户是“高级用户”或“管理员”组的成员时可用，即普通用户无权访问此选项卡而不是"看见"。

An additional thing - if IE does not have some \*.wav file in the local
cache directory (%userprofile%\\Local Settings\\Temporary Internet
Files) the sound will not play the first time. 另外 -
如果IE在本地缓存目录（％userprofile％\\ Local Settings \\ Temporary
Internet Files）中没有一些\* .wav文件，则声音将不会在第一次播放。

##### Known not to work已知无法正常工作的

Browsers where the sound did not work声音不起作用的浏览器:

-   Opera 10.11 on Linux. 在Linux下的Opera 10.11.

[comment]: # ({/new-ca9dfcae})
