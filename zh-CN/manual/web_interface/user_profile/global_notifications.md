[comment]: # translation:outdated

[comment]: # ({new-4cdbbfea})
# 1 Global notifications全局通知

[comment]: # ({/new-4cdbbfea})

[comment]: # ({new-5c281f24})
#### Overview概述

Global notifications are a way of displaying issues that are currently
happening right on the screen you're at in Zabbix
frontend.全局通知是一种在Zabbix前端屏幕上显示当前正在发生的问题的方法。

Without global notifications, working in some other location than *Host
groups* or *Dashboard* pages would not show any information regarding
issues that are currently happening. Global notifications will display
this information regardless of where you are.没有全局通知，触发器状态 or
Dashboard
页面，将不会显示任何有关当前正在发生的问题的信息。不论您在哪里，全局通知会显示这些信息。

Global notifications involve both showing a message and [playing a
sound](sound). 全局通知涉及到信息的显示和[playing a sound](sound).

[comment]: # ({/new-5c281f24})

[comment]: # ({new-35c1ef59})
#### Configuration配置

Global notifications can be enabled per user in the *Messaging* tab of
[profile
configuration](/manual/web_interface/user_profile).可以在[profile
configuration](manual：web_interface：user_profile)的// Messaging
//选项卡中为每个用户启用全局通知。

![profile3.png](../../../../assets/en/manual/web_interface/profile3.png)

|Parameter参数          D|scription描述|
|--------------------------|---------------|
|*Frontend messaging*|Mark the checkbox to enable global notifications. 选中该复选框以启用全局通知。|
|*Message timeout*|You can set for how long the message will be displayed. By default, messages will stay on screen for 60 seconds.您可以设置消息显示的时间。 默认情况下，消息将在屏幕上显示60秒。<br>[Time suffixes](/manual/appendix/suffixes) are supported, e.g. 30s, 5m, 2h, 1d. 支持的，如 30s, 5m, 2h, 1d.|
|*Play sound*|You can set how long the sound will be played.您可以设置声音的播放长度。<br>**Once** - sound is played once and fully.声音完整播放一次。<br>**10 seconds** - sound is repeated for 10 seconds.声音重复播放10秒。<br>**Message timeout** - sound is repeated while the message is visible. 当消息显示时，声音一直播放。|
|*Trigger severity*|You can set the trigger severities that global notifications and sounds will be activated for. You can also select the sounds appropriate for various severities.您可以设置全局通知和声音的触发器的严苛度，同时，您还可以针对不同的严苛度选择合适的声音。<br>If no severity is marked then no messages will be displayed at all.<br>Also, recovery messages will only be displayed for those severities that are marked. So if you mark *Recovery* and *Disaster*, global notifications will be displayed for the problems and the recoveries of disaster severity triggers. 如果没有标记严苛度，那么就不会显示任何消息。而且，只有标记的严苛性才会显示恢复信息。因此，如果标记Recovery和 Disaster, 全局通知将会显示问题，以及灾难严苛度触发器的恢复。|

[comment]: # ({/new-35c1ef59})

[comment]: # ({new-933467f7})
##### Global messages displayed全局信息显示

As the messages arrive, they are displayed in a floating section on the
right hand side. This section can be repositioned freely by dragging the
section header.
当消息到达时，它们显示在右侧的浮动部分中。通过拖动节标题可以自由地重新定位此部分。

![global\_messages.png](../../../../assets/en/manual/web_interface/global_messages.png)

For this section, several controls are
available在这个区域内，一些控件是可用的：:

-   ![](../../../../assets/en/manual/about/message_button_snooze.png)
    **Snooze** button silences currently active alarm
    sound键将会静音当前的警报音;
-   ![](../../../../assets/en/manual/about/message_button_mute.png)
    **Mute/Unmute** button switches between playing and not playing the
    alarm sounds键在播放与不播放警报音之间切换.

[comment]: # ({/new-933467f7})
