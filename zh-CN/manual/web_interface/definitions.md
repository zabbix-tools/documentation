[comment]: # translation:outdated

[comment]: # ({new-39f5a4d3})
# 6 Definitions定义

[comment]: # ({/new-39f5a4d3})

[comment]: # ({new-a9697948})
#### Overview概述

While many things in the frontend can be configured using the frontend
itself, some customisations are currently only possible by editing a
definitions file.
虽然可以使用前端本身配置前端中的许多内容，但目前只能通过编辑定义文件来进行某些自定义。

This file is `defines.inc.php` located in /include of the Zabbix HTML
document directory.该文件是位于/包含Zabbix
HTML文档目录的'define.inc.php'。

[comment]: # ({/new-a9697948})

[comment]: # ({new-3260c875})
#### Parameters参数

Parameters in this file that could be of interest to
users用户可能感兴趣的此文件中的参数:

-   ZBX\_LOGIN\_ATTEMPTS

Number of unsuccessful login attempts that is allowed to an existing
system user before a login block in applied (see ZBX\_LOGIN\_BLOCK). By
default 5 attempts. Once the set number of login attempts is tried
unsuccessfully, each additional unsuccessful attempt results in a login
block. Used with
[internal](/manual/web_interface/frontend_sections/administration/authentication)
authentication only.
应用登录块之前允许现有系统用户的不成功登录尝试次数（请参阅ZBX\_LOGIN\_BLOCK）。
默认为5次尝试。
一旦尝试了设置的登录尝试次数失败，则每次额外的不成功尝试都会导致登录阻止。
仅与[internal](：：manual / web_interface / frontend_sections / administration / authentication)身份验证一起使用。

-   ZBX\_LOGIN\_BLOCK

Number of seconds for blocking a user from accessing Zabbix frontend
after a number of unsuccessful login attempts (see
ZBX\_LOGIN\_ATTEMPTS). By default 30 seconds. Used with
[internal](/manual/web_interface/frontend_sections/administration/authentication)
authentication only.
在多次登录尝试失败后阻止用户访问Zabbix前端的秒数（请参阅ZBX\_LOGIN\_ATTEMPTS）。
默认为30秒。
仅与[internal](：：manual / web_interface / frontend_sections / administration / authentication)身份验证一起使用。

-   ZBX\_PERIOD\_DEFAULT

Default graph period, in seconds. One hour by
default.默认图表周期，以秒为单位。 默认为一小时。

-   ZBX\_MIN\_PERIOD

Minimum graph period, in seconds. One hour by
default.最短图表周期，以秒为单位。 默认为一小时。

-   ZBX\_MAX\_PERIOD

Maximum graph period, in seconds. Two years by default since 1.6.7, one
year before that.最大图形周期，以秒为单位。 一年前，两年后默认为1.6.7。

-   ZBX\_HISTORY\_PERIOD

The maximum period to display history data in *Latest data*, *Web*,
*Overview* pages and *Data overview* screen element in seconds. By
default set to 86400 seconds (24 hours). Unlimited period, if set to 0
seconds. 在*Latest data*, *Web*, *Overview*页面和*Data
overview*屏幕元素中以秒显示历史数据的最长期限。
默认设置为86400秒（24小时）。 无限期，如果设置为0秒。

-   GRAPH\_YAXIS\_SIDE\_DEFAULT

Default location of Y axis in simple graphs and default value for drop
down box when adding items to custom graphs. Possible values: 0 - left,
1 - right.
在将监控项添加到自定义图形时，简单图形中的Y轴的默认位置和下拉框的默认值。
可能的值：0 - 左，1 - 右。

Default默认值: 0

-   SCREEN\_REFRESH\_TIMEOUT (available since 2.0.4)

Used in screens and defines the timeout seconds for a screen element
update. When the defined number of seconds after launching an update
pass and the screen element has still not been updated, the screen
element will be darkened.用于聚合图形并定义聚合图形元素更新的超时秒数。
当启动更新过程后定义的秒数且聚合图形元素仍未更新时，聚合图形元素将变暗。

Default默认值: 30

-   SCREEN\_REFRESH\_RESPONSIVENESS (available since 2.0.4)

Used in screens and defines the number of seconds after which query
skipping will be switched off. Otherwise, if a screen element is in
update status all queries on update are skipped until a response is
received. With this parameter in use, another update query might be sent
after N seconds without having to wait for the response to the first
one. 在聚合图形中使用，并定义将关闭查询跳过的秒数。
否则，如果聚合图形元素处于更新状态，则会跳过所有更新查询，直到收到响应。
使用此参数后，可能会在N秒后发送另一个更新查询，而不必等待对第一个的响应。

Default默认值: 10

-   QUEUE\_DETAIL\_ITEM\_COUNT

Defines retrieval limit of the total items queued. Since Zabbix 3.2.4
may be set higher than default value.定义排队的总监控项的检索限制。
由于Zabbix 3.2.4可能设置为高于默认值。

Default默认值: 500

-   ZBX\_SHOW\_SQL\_ERRORS (available since 3.4.0)

Show SQL errors in the frontend, if 'true'. If changed to 'false' then
SQL errors will still be displayed to all users with *Debug mode*
[enabled](/manual/config/users_and_usergroups/usergroup#configuration).
With *Debug mode* disabled, only Zabbix Super Admin users will see SQL
errors. Others will see a generic message: "SQL error. Please contact
Zabbix administrator." 如果为'true'，则在前端显示SQL错误。
如果更改为“false”，则仍会以*调试模式*
[enabled](：：manual / config / users_and_usergroups / usergroup #configuration)向所有用户显示SQL错误。
在*调试模式*禁用的情况下，只有Zabbix Super Admin用户会看到SQL错误。
其他人会看到一条通用消息：“SQL错误。请联系Zabbix管理员。”

Default默认值: true

-   VALIDATE\_URI\_SCHEMES (available since 3.4.5)

Validate a URI against the scheme whitelist defined in
ZBX\_URI\_VALID\_SCHEMES.根据ZBX\_URI\_VALID\_SCHEMES中定义的方案白名单验证URI。

Default默认值: true

-   ZBX\_URI\_VALID\_SCHEMES (available since 3.4.2)

A comma-separated list of allowed URI schemes. Affects all places in the
frontend where URIs are used, for example, in map element
URLs.逗号分隔的允许URI方案列表。
影响使用URI的前端中的所有位置，例如，在地图元素URL中。

Default默认值: http,https,ftp,file,mailto,tel,ssh

-   ZBX\_SHOW\_TECHNICAL\_ERRORS (available since 3.4.4)

Show technical errors (PHP/SQL) to non-Zabbix Super admin users and to
users that are not part of user groups with [debug
mode](/manual/web_interface/debug_mode)
enabled.向非Zabbix超级管理员用户以及不启用[debug
mode](：manual / web_interface / debug_mode)的用户组的用户显示技术错误（PHP
/ SQL）。

Default默认值: false

-   ZBX\_SESSION\_NAME (available since 4.0.0)

String used as the name of the Zabbix frontend session
cookie.用作Zabbix前端会话cookie名称的字符串。

Default默认值: zbx\_sessionid

[comment]: # ({/new-3260c875})
