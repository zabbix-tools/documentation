[comment]: # translation:outdated

[comment]: # ({new-d1036ff8})
# 7 Creating your own theme制定自己的主题

[comment]: # ({/new-d1036ff8})

[comment]: # ({new-cb54c172})
#### Overview概述

By default, Zabbix provides a number of predefined themes. You may
follow the step-by-step procedure provided here in order to create your
own. Feel free to share the result of your work with Zabbix community if
you created something
nice.默认情况下，Zabbix预置了许多主题。您还可以按照以下提供的步骤，制作自定义主题。如果您创作了一些很好的主题，欢迎随时与Zabbix社区分享您的工作成果。

[comment]: # ({/new-cb54c172})

[comment]: # ({new-be6afd90})
##### Step 1 步骤 1

To define your own theme you'll need to create a CSS file and save it in
the *styles/* folder (for example, *custom-theme.css*). You can either
copy the files from a different theme and create your theme based on it
or start from scratch.为了制作属于您自己的主题，您需要在 *styles/*
文件夹下创建一个 CSS 文件(例如：
*custom-theme.css*)。您可以从不同的主题复制文件，并据此创建主题，或从头开始创作。

[comment]: # ({/new-be6afd90})

[comment]: # ({new-34bae249})
##### Step 2 步骤 2

Add your theme to the list of themes returned by the Z::getThemes()
method. You can do this by overriding the ZBase::getThemes() method in
the Z class. This can be done by adding the following code before the
closing brace in *include/classes/core/Z.php*:您可以通过 Z::getThemes()
方法将您的主题添加到主题列表中。您可以通过覆盖Z类中的 ZBase::getThemes()
方法来执行此操作。这可以通过在 *include/classes/core/Z.php*:
中的关闭括号之前添加以下代码：

      public static function getThemes() {
          return array_merge(parent::getThemes(), array(
              'custom-theme' => _('Custom theme')
          ));
      }

::: noteimportant
Note that the name you specify within the first
pair of quotes must match the name of the theme file without
extension.需要注意的是：您在第一对引号内指定的名称必须与没有扩展名的主题文件的名称相匹配。
:::

To add multiple themes, just list them under the first theme, for
[example:添加多个主题，只需要将它们罗列在第一个主题下面即可，例如](example:添加多个主题，只需要将它们罗列在第一个主题下面即可，例如)：

      public static function getThemes() {
          return array_merge(parent::getThemes(), array(
              'custom-theme' => _('Custom theme'),
              'anothertheme' => _('Another theme'),
              'onemoretheme' => _('One more theme')
          ));
      }

Note that every theme except the last one must have a trailing
comma.注意：除了最后的一个主题外，其他主题必须用逗号分隔。

::: notetip
To change graph colours, the entry must be added in the
*graph\_theme* database table.为了改变图形颜色，必须在 *graph\_theme*
数据库表格中添加该条目。
:::

[comment]: # ({/new-34bae249})

[comment]: # ({new-b67ccba3})
##### Step 3 步骤 3

Activate the new theme.激活新主题

In Zabbix frontend, you may either set this theme to be the default one
or change your theme in the user profile.
在Zabbix前端，您可以将此主题设置为默认主题或在用户资料改主题。

Enjoy the new look and feel! 享受新的外观吧！

[comment]: # ({/new-b67ccba3})
