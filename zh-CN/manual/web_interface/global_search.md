[comment]: # translation:outdated

[comment]: # ({new-13e47fde})
# 3 Global search全局搜索

It is possible to search Zabbix frontend for hosts, host groups and
templates.

The search input box is located in the upper right corner. The search
can be started by pressing *Enter* or clicking on the
![](../../../assets/en/manual/web_interface/search_icon.png) search
icon.
在Zabbix前端，可以搜索多种实体。实体搜索输入框在右上角。搜索可以通过摁
回车
键或者点击![](../../../assets/en/manual/web_interface/search_icon.png)搜索图标来开始搜索。

![](../../../assets/en/manual/web_interface/global_search_dropdown.png)

If there is a host that starts with the entered string, a dropdown will
appear, listing all such
hosts.如果有一个以输入的字符串开头的主机，将显示一个下拉列表，列出所有这样的主机:

[comment]: # ({/new-13e47fde})

[comment]: # ({new-5ca91d97})
#### Properties searched实体搜索

Hosts can be searched by the following
properties:可以搜索这些实体及其属性：

-   Host name主机名
-   Visible name可见名
-   IP address IP地址
-   DNS name DNS名

Host groups can be searched by name. Specifying a parent host group
implicitly selects all nested host
groups.指定父主机组间接地选择所有嵌套的主机组

Templates can be searched by name or visible name. If you search by a
name that is different from the visible name (of a template/host), in
the search results it is displayed below the visible name in
parentheses.可以按名称或可见名搜索模板。
如果使用与（模板/主机的）可见名不同的名称进行搜索，则在搜索结果中，它将显示在括号中的可见名称下方。

[comment]: # ({/new-5ca91d97})

[comment]: # ({new-c51d2c6b})
#### Search results搜索结果

Search results consist of three separate blocks for hosts, host groups
and templates. 搜索结果包含三个单独的块，用于主机，主机组和模板。

![](../../../assets/en/manual/web_interface/global_search_results.png){width="600"}

It is possible to collapse/expand each individual block. The entry count
is displayed at the bottom of each block, for example, *Displaying 13 of
13 found*. Total entries displayed within one block are limited to
100.可以折叠/展开每个单独的块。
条目计数显示在每个块的底部，例如，*显示13中的13个找到*。
一个块内显示的条目总数限制为100。

Each entry provides links to monitoring and configuration data. See
[links available](/#links_available).
每个实体都提供指向监视和配置数据的链接。 参见[links
available](：#links_available)。

For all configuration data (such as items, triggers, graphs) the amount
of entities found is displayed by a number next to the entity name, in
grey. **Note** that if there are zero entities, no number is
displayed.对于所有配置数据（例如项目，触发器，图形），找到的实体数量由实体名称旁边的数字显示，灰色。
**注意**如果实体为零，则不显示任何数字。

Enabled hosts are displayed in blue, disabled hosts in
red.已启用的主机以蓝色显示，已禁用的主机以红色显示。

[comment]: # ({/new-c51d2c6b})

[comment]: # ({new-1a40034e})
#### Links available可用链接

For each entry the following links are
available:对于找到的实体，下列链接均可用：

-   Hosts主机
    -   Monitoring监控
        -   Latest data最新数据
        -   Triggers触发器
        -   Problems异常
        -   Graphs 图
        -   Host screens主机聚合图形
        -   Web scenarios Web场景
    -   Configuration 配置
        -   Host properties 主机属性
        -   Applications 应用
        -   Items 监控项
        -   Triggers 触发器
        -   Graphs 图
        -   Discovery rules 发现规则
        -   Web scenarios Web场景

```{=html}
<!-- -->
```
-   Host groups 主机组
    -   Monitoring 监控
        -   Latest data 最新数据
        -   Triggers 监控项
        -   Problems 异常
        -   Graphs 图
        -   Web scenarios Web场景
    -   Configuration 配置
        -   Host group properties 主机组属性
        -   Host group members (hosts and templates)
            主机组成员（主机和模板）

```{=html}
<!-- -->
```
-   Templates 模板
    -   Configuration 配置
        -   Template properties 模板属性
        -   Applications 应用
        -   Items 监控项
        -   Triggers 触发器
        -   Graphs 图
        -   Template screens 模板聚合图形
        -   Discovery rules 发现规则
        -   Web scenarios Web场景

[comment]: # ({/new-1a40034e})
