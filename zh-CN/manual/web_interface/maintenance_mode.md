[comment]: # translation:outdated

[comment]: # ({new-8fc97588})
# 4 Frontend maintenance mode前端维护模式

[comment]: # ({/new-8fc97588})

[comment]: # ({new-fca7c17f})
#### Overview概述

Zabbix web frontend can be temporarily disabled in order to prohibit
access to it. This can be useful for protecting the Zabbix database from
any changes initiated by users, thus protecting the integrity of
database. Zabbix web前端可以暂时禁用，以禁止访问它。
这对于保护Zabbix数据库免受用户发起的任何更改非常有用，从而保护了数据库的完整性。

Zabbix database can be stopped and maintenance tasks can be performed
while Zabbix frontend is in maintenance mode.
Zabbix数据库可以被停止，并且维护任务可以在Zabbix 前端在维护模式中进行。

Users from defined IP addresses will be able to work with the frontend
normally during maintenance mode.
来自指定IP地址的用户将能够在维护模式期间正常工作。

[comment]: # ({/new-fca7c17f})

[comment]: # ({new-dcfd8b13})
#### Configuration配置

In order to enable maintenance mode, the `maintenance.inc.php` file
(located in /conf of the Zabbix HTML document directory on the
webserver) must be modified to uncomment the following lines:
为了启用维护模式，必须以取消注释的方法修改
`maintenance.inc.php`文件（位于web服务器上的Zabbix HTML 文档目录的/
conf中）：

    // Maintenance mode. 维护模式
    define('ZBX_DENY_GUI_ACCESS', 1);

    // Array of IP addresses, which are allowed to connect to frontend (optional). 一列包含IP地址的数组，它们可以连接到前端（可选）
    $ZBX_GUI_ACCESS_IP_RANGE = array('127.0.0.1');

    // Message shown on warning screen (optional). 警告屏幕上所显示的信息（可选）
    $ZBX_GUI_ACCESS_MESSAGE = 'We are upgrading MySQL database till 15:00. Stay tuned...';

|Parameter参数                     D|tails详情|
|-------------------------------------|-----------|
|**ZBX\_DENY\_GUI\_ACCESS**|Enable maintenance mode:打开维护模式:<br>1 – maintenance mode is enabled, disabled otherwise 维护模式已打开，其他的数字表示未打开|
|**ZBX\_GUI\_ACCESS\_IP\_RANGE**|Array of IP addresses, which are allowed to connect to frontend (optional).允许连接到前端的IP地址数组（可选）<br>For example例:<br>`array('192.168.1.1', '192.168.1.2')`|
|**ZBX\_GUI\_ACCESS\_MESSAGE**|A message you can enter to inform users about the maintenance (optional).您可以输入的消息以通知用户维护（可选）|

[comment]: # ({/new-dcfd8b13})

[comment]: # ({new-ae331777})

::: notetip
Mostly the `maintenance.inc.php` file is located in `/conf` of Zabbix HTML document directory on the
web server. However, the location of the directory may differ depending on the operating system and a web server it uses.

For example, the location for:

-  SUSE and RedHat is `/etc/zabbix/web/maintenance.inc.php`.
-  Debian-based systems is `/usr/share/zabbix/conf/`.

See also [Copying PHP files](/manual/installation/install#copying-php-files). 
:::

|Parameter|Details|
|--|--------|
|**ZBX\_DENY\_GUI\_ACCESS**|Enable maintenance mode:<br>1 – maintenance mode is enabled, disabled otherwise|
|**ZBX\_GUI\_ACCESS\_IP\_RANGE**|Array of IP addresses, which are allowed to connect to frontend (optional).<br>For example:<br>`array('192.168.1.1', '192.168.1.2')`|
|**ZBX\_GUI\_ACCESS\_MESSAGE**|A message you can enter to inform users about the maintenance (optional).|

Note that the [location](/manual/installation/install#copying-php-files) of the `/conf` directory will vary based on the operating system and web server. 

[comment]: # ({/new-ae331777})

[comment]: # ({new-987fdc44})
#### Display显示

The following screen will be displayed when trying to access the Zabbix
frontend while in maintenance mode. The screen is refreshed every 30
seconds in order to return to a normal state without user intervention
when the maintenance is over.
下图显示了在维护模式下访问Zabbix前端的情况。屏幕每30秒刷新一次，以便在维护结束后，无需用户干预即可恢复正常状态。

![](../../../assets/en/manual/web_interface/frontend_maintenance.png)

IP addresses defined in ZBX\_GUI\_ACCESS\_IP\_RANGE will be able to
access the frontend as
always.在ZBX\_GUI\_ACCESS\_IP\_RANGE中定义的IP地址也可以一直访问前端。

[comment]: # ({/new-987fdc44})
