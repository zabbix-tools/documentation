[comment]: # translation:outdated

[comment]: # ({new-761c4a6f})
# 2 Zabbix 介绍

[comment]: # ({/new-761c4a6f})

[comment]: # ({new-319fca18})
#### 概述

Zabbix 是由 Alexei Vladishev 创建，目前是由 Zabbix SIA
在持续开发和提供支持。

Zabbix 是一种企业级的分布式开源监控解决方案。

Zabbix 是一款能够监控众多网络参数和服务器的健康度和完整性的软件。Zabbix
使用灵活的通知机制，允许用户为几乎任何事件配置基于邮件的警报。这样可以快速相应服务器问题。Zabbix
基于存储的数据提供出色的报告和数据可视化。这些功能使得 Zabbix
成为容量规划的理想选择。

Zabbix 支持轮询和被动捕获。所有的 Zabbix
报告、统计信息和配置参数都可以通过基于 Web 的前端页面进行访问。基于 Web
的前端页面确保您的网络状态和服务器健康状况可以从任何地方进行评估。在经过适当的配置后，Zabbix
可以在监控 IT
基础设施方面发挥重要作用。无论是对于拥有少量服务器的小型组织，还是拥有大量服务器的大型公司而言，同样适用。

Zabbix 是免费的。Zabbix 是根据 GPL
通用公共许可证的第二版编写和分发的。这意味着它的源代码是免费分发的，并且可供公共使用。

[商业支持](http://www.zabbix.com/support.php) 由 Zabbix 公司提供。

了解更多 [Zabbix 功能](features)。

[comment]: # ({/new-319fca18})

[comment]: # ({new-46ee774b})
#### Zabbix 的用户

世界上许多不同规模的组织都依赖 Zabbix 作为主要的监控平台。

[comment]: # ({/new-46ee774b})
