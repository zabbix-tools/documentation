[comment]: # translation:outdated

[comment]: # ({new-bade5a47})
# 4 Zabbix 概述

[comment]: # ({/new-bade5a47})

[comment]: # ({new-b4d6d531})
#### 架构

Zabbix 由几个主要的功能组件组成，其职责如下所示。

[comment]: # ({/new-b4d6d531})

[comment]: # ({new-5957a1ba})
##### Server

[Zabbix server](/manual/concepts/server) 是 Zabbix agent
向其报告可用性、系统完整性信息和统计信息的核心组件。是存储所有配置信息、统计信息和操作信息的核心存储库。

[comment]: # ({/new-5957a1ba})

[comment]: # ({new-494fb840})
##### 数据库

所有配置信息以及 Zabbix 收集到的数据都被存储在数据库中。

[comment]: # ({/new-494fb840})

[comment]: # ({new-bd637ffa})
##### Web 界面

为了从任何地方和任何平台轻松访问 Zabbix ，我们提供了基于 web
的界面。该界面是 Zabbix server 的一部分，通常（但不一定）和 Zabbix
server 运行在同一台物理机器上。

[comment]: # ({/new-bd637ffa})

[comment]: # ({new-bfd730af})
##### Proxy

[Zabbix proxy](/manual/concepts/proxy) 可以替 Zabbix server
收集性能和可用性数据。Zabbix proxy 是 Zabbix
环境部署的可选部分；然而，它对于单个 Zabbix server
负载的分担是非常有益的。

[comment]: # ({/new-bfd730af})

[comment]: # ({new-b9991630})
##### Agent

[Zabbix agents](/manual/concepts/agent)
部署在被监控目标上，用于主动监控本地资源和应用程序，并将收集的数据发送给
Zabbix server。

[comment]: # ({/new-b9991630})

[comment]: # ({new-9ec23332})
#### 数据流

此外，重要的是，需要回过头来了解下 Zabbix
内部的整体数据流。首先，为了创建一个采集数据的监控项，您就必须先创建主机。其次，必须有一个监控项来创建触发器。最后，您必须有一个触发器来创建一个动作，这几个点构成了一个完整的数据流。因此，如果您想要收到
CPU load it too high on *Server X* 的告警，您必须首先为 //Server X //
创建一个主机条目，其次创建一个用于监视其 CPU
的监控项，最后创建一个触发器，用来触发 CPU is too high
这个动作，并将其发送到您的邮箱里。虽然这些步骤看起来很繁琐，但是使用模板的话，其实并不复杂。也正是由于这种设计，使得
Zabbix 的配置变得更加灵活易用。

[comment]: # ({/new-9ec23332})
