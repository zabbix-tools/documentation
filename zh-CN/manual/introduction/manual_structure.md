[comment]: # translation:outdated

[comment]: # ({new-cbbea730})
# 1 手册结构

[comment]: # ({/new-cbbea730})

[comment]: # ({new-14bd850a})
#### Structure

Zabbix 4.0
的手册内容分为几个章节和子章节，以便于您来访问感兴趣的特定主题。

当您导航到相应的章节时，请确保您展开该章节的被折叠页面，从而完整获取各个子章节和页面中的内容。

手册会将尽可能提供相关内容页面之间的交叉链接，以确保用户不会错过相关信息。

[comment]: # ({/new-14bd850a})

[comment]: # ({new-4f79bd48})
#### 章节

[简介](about) 提供了关于 Zabbix 的常规信息。阅读本章节应该会为您选择
Zabbix 提供一些好的理由。

[术语](/manual/concepts) 解释了在 Zabbix 中使用到的术语，并提供了有关
Zabbix 组件的详细信息。

[安装](/manual/installation/getting_zabbix) 和
[快速入门](/manual/quickstart/item) 章节可以帮助您开始使用 Zabbix。

[Zabbix 应用](/manual/appliance)
是一种可供选择的方案，通过此章节可以了解快速使用 Zabbix 的方法。

[配置](/manual/config)
是本手册中篇幅最多并且最为重要的章节之一。它包含了大量关于如何设置
Zabbix
去监控您的环境的基本建议，从设置主机到获取基本数据，再到查看数据，再到配置通知，以及问题拍错的相关命令。

[IT services](/manual/it_services) 章节详细说明了如何使用 Zabbix
，从更高层次的视角关注您的监控系统。

[Web 监控](/manual/web_monitoring) 可以帮助您学会如何监控 Web
网站的可用性。

[虚拟机监控](/manual/vm_monitoring) 介绍了如何配置 VMware 环境的监控。

[维护](/manual/maintenance), [正则表达式](/manual/regular_expressions),
[事件确认](/manual/acknowledges) 和
[配置的导入与导出](/manual/xml_export_import)
几个章节进一步展示了如何使用 Zabbix 的这些方面的功能。

[自动发现](/manual/discovery)
包含有关配置网络设备、Zabbix客户端(主动式)、文件系统、网络接口等的自动发现的说明。

[分布式监控](/manual/distributed_monitoring) 使用 Zabbix
支撑更庞大和更复杂环境的相关内容。

[加密](/manual/encryption) 解释了如何对 Zabbix 组件之间的通讯进行加密。

[Web 界面](/manual/web_interface) 包含了如何使用 Zabbix 的 Web
界面的内容。

[API](/manual/api) 介绍了使用 Zabbix API 的详细信息。

更为详细的技术细节，包含在 [附录](/manual/appendix)
中。附录也包含常见问题的详细解答。

[comment]: # ({/new-4f79bd48})
