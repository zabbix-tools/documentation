[comment]: # translation:outdated

[comment]: # ({new-09ce3b2e})
# 3 Zabbix 功能

[comment]: # ({/new-09ce3b2e})

[comment]: # ({new-a70701e6})
#### 概述

Zabbix
是一种高度集成的网络监控解决方案，在单一的软件包中提供了多种功能。

**[数据采集](/manual/config/items)**

-   可用性和性能采集；
-   支持 SNMP（包括主动轮询和被动捕获）、IPMI、JMX、VMware 监控；
-   自定义检查；
-   按照自定义的时间间隔采集需要的数据；
-   通过 Server/Proxy 和 Agents 来执行数据采集。

**[灵活的阈值定义](/manual/config/triggers)**

-   您可以定义非常灵活的告警阈值，称之为触发器，触发器从后端数据库获得参考值。

**[高度可配置化的告警](/manual/config/notifications)**

-   可以根据递增计划、接收者、媒介类型自定义发送告警通知；
-   使用宏变量可以使告警通知变得更加高效有益；
-   自动动作包含远程命令。

**[实时图形](/manual/config/visualization/graphs/simple)**

-   使用内置图形功能可实以将监控项绘制成图形。

**[Web 监控功能](/manual/web_monitoring)**

-   Zabbix 可以追踪模拟鼠标在 Web 网站上的点击操作，来检查 Web
    网站的功能和响应时间。

**[丰富的可视化选项](/manual/config/visualization)**

-   能够创建可以将多个监控项组合到单个视图中的自定义图形；
-   网络拓扑图；
-   以仪表盘样式展示自定义聚合图形和幻灯片演示；
-   报表；
-   监控资源的高层次（业务）视图。

**[历史数据存储](/manual/installation/requirements#database_size)**

-   存储在数据库中的数据；
-   可配置的历史数据；
-   内置数据管理机制（housekeeping）。

**[配置简单](/manual/config/hosts)**

-   将被监控设备添加为主机；
-   主机一旦添加到数据库中，就会采集主机数据用于监控；
-   将模板用于监控设备。

**[套用模板](/manual/config/templates)**

-   在模板中分组检查；
-   模板可以关联其他模板，获得继承。

**[网络发现](/manual/discovery)**

-   自动发现网络设备；
-   Zabbix Agent 发现设备后自动注册；
-   自动发现文件系统、网络接口和 SNMP OIDs 值。

**[快捷的 Web 界面](/manual/web_interface)**

-   基于 PHP 的 Web 前端；
-   可以从任何地方访问；
-   您可以定制自己的操作方式；
-   审计日志。

**[Zabbix API](/manual/api)**

-   Zabbix API 为 Zabbix
    提供可编程接口，用于批量操作、第三方软件集成和其他用途。

**[权限管理系统](/manual/config/users_and_usergroups)**

-   安全的用户身份验证；
-   将特定用户限制于访问特定的视图。

**[功能强大且易于扩展的 Zabbix Agent](/manual/concepts/agent)**

-   部署于被监控对象上；
-   完美支持 Linux 和 Windows ；

**[二进制守护进程](/manual/concepts/server)**

-   为了更好的性能和更少的内存占用，采用 C 语言编写；
-   便于移植。

**[适应更复杂的环境](/manual/distributed_monitoring)**

-   使用 Zabbix Proxy 代理，可以轻松实现分布式远程监控。

[comment]: # ({/new-a70701e6})
