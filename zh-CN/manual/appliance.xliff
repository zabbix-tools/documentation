<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="zh-CN" datatype="plaintext" original="manual/appliance.md">
    <body>
      <trans-unit id="8c911baa" xml:space="preserve">
        <source># 6. Zabbix appliance</source>
        <target state="needs-translation"># 6. Zabbix Appliance</target>
      </trans-unit>
      <trans-unit id="cfa1a361" xml:space="preserve">
        <source>#### Overview

As an alternative to setting up manually or reusing an existing server
for Zabbix, users may
[download](http://www.zabbix.com/download_appliance) a Zabbix appliance
or a Zabbix appliance installation CD image.

Zabbix appliance and installation CD versions are based on AlmaLinux 8
(x86\_64).

Zabbix appliance installation CD can be used for instant deployment of
Zabbix server (MySQL).

::: noteimportant
 You can use this Appliance to evaluate Zabbix.
The Appliance is not intended for serious production use. 
:::</source>
        <target state="needs-translation">#### 概述

作为手动设置或重用现有  Zabbix  服务器的替代方法，用户可以[下载](http://www.zabbix.com/download_appliance) Zabbix appliance 或 Zabbix appliance 安装 CD image。

Zabbix appliance 及 安装 CD 基于 CentOS 8 (x86\_64).

Zabbix appliance 安装 CD 可用于 Zabbix server (MySQL) 的即时部署。

::: noteimportant
您可以使用 Appliance 来评估Zabbix。
这个 Appliance 不为重要的生产用途设计。
:::</target>
      </trans-unit>
      <trans-unit id="cffa82a3" xml:space="preserve">
        <source>##### System requirements:

-   *RAM*: 1.5 GB
-   *Disk space*: at least 8 GB should be allocated for the virtual
    machine
-   *CPU*: 2 cores minimum 

Zabbix installation CD/DVD boot menu:

![](../../assets/en/manual/installation_cd_boot_menu1.png){width="600"}

Zabbix appliance contains a Zabbix server (configured and running on
MySQL) and a frontend.

Zabbix virtual appliance is available in the following formats:

-   VMware (.vmx)
-   Open virtualization format (.ovf)
-   Microsoft Hyper-V 2012 (.vhdx)
-   Microsoft Hyper-V 2008 (.vhd)
-   KVM, Parallels, QEMU, USB stick, VirtualBox, Xen (.raw)
-   KVM, QEMU (.qcow2)

To get started, boot the appliance and point a browser at the IP the
appliance has received over DHCP.

::: noteimportant
 DHCP must be enabled on the host. 
:::

To get the IP address from inside the virtual machine run:

    ip addr show

To access Zabbix frontend, go to **http://&lt;host\_ip&gt;** (for access
from the host's browser bridged mode should be enabled in the VM network
settings).

::: notetip
If the appliance fails to start up in Hyper-V, you may
want to press `Ctrl+Alt+F2` to switch tty sessions.
:::</source>
        <target state="needs-translation">##### 系统需求：

-   *RAM*: 1.5 GB
-   *磁盘空间*: 应至少为虚拟机分配 8 GB。

Zabbix 安装 CD/DVD 的 boot 菜单：

![](../../assets/en/manual/installation_cd_boot_menu1.png){width="600"}

Zabbix Appliance 包含 一个Zabbix server (已配置并在 MySQL 上运行) 及一个前端。

Zabbix Appliance 提供如下格式的虚拟机 image：

-   VMWare (.vmx)
-   Open virtualization format (.ovf)
-   Microsoft Hyper-V 2012 (.vhdx)
-   Microsoft Hyper-V 2008 (.vhd)
-   KVM, Parallels, QEMU, USB stick, VirtualBox, Xen (.raw)
-   KVM, QEMU (.qcow2)

要开始使用，请 boot Appliance 并通过浏览器访问 Appliance 通过 DHCP 接收的 IP。

::: noteimportant
必须在主机上启用 DHCP。
:::

在虚拟机内部查看 IP 地址，可以执行：

    ip addr show

要访问 Zabbix 前端，可以访问 **http://&lt;host\_ip&gt;** （应在虚机网络设置中启用桥接模式以便从主机的浏览器访问）。

::: notetip
如果 Appliance 在 Hyper-V 上启动失败，你可以按下 `Ctrl+Alt+F2` 以切换 tty session.
:::</target>
      </trans-unit>
      <trans-unit id="589fd5e2" xml:space="preserve">
        <source>#### - Changes to AlmaLinux 8 configuration

The appliance is based on AlmaLinux 8. There are some changes applied to
the base AlmaLinux configuration.</source>
        <target state="needs-translation">#### - 对 CentOS 8 配置的更改

Appliance 基于 CentOS 8。有一些配置与基本的 CentOS 设置有一定区别。</target>
      </trans-unit>
      <trans-unit id="193d3b23" xml:space="preserve">
        <source>##### - Repositories

Official Zabbix
[repository](/manual/installation/install_from_packages/rhel) has
been added to */etc/yum.repos.d*:

    [zabbix]
    name=Zabbix Official Repository - $basearch
    baseurl=http://repo.zabbix.com/zabbix/5.2/rhel/8/$basearch/
    enabled=1
    gpgcheck=1
    gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-ZABBIX-A14FE591</source>
        <target state="needs-translation">##### - 仓库

官方的 Zabbix [仓库](/manual/installation/install_from_packages/rhel_centos) 已经被添加到 */etc/yum.repos.d*：

    [zabbix]
    name=Zabbix Official Repository - $basearch
    baseurl=http://repo.zabbix.com/zabbix/5.2/rhel/8/$basearch/
    enabled=1
    gpgcheck=1
    gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-ZABBIX-A14FE591</target>
      </trans-unit>
      <trans-unit id="589dc798" xml:space="preserve">
        <source>##### - Firewall configuration

The appliance uses iptables firewall with predefined rules:

-   Opened SSH port (22 TCP);
-   Opened Zabbix agent (10050 TCP) and Zabbix trapper (10051 TCP)
    ports;
-   Opened HTTP (80 TCP) and HTTPS (443 TCP) ports;
-   Opened SNMP trap port (162 UDP);
-   Opened outgoing connections to NTP port (53 UDP);
-   ICMP packets limited to 5 packets per second;
-   All other incoming connections are dropped.</source>
        <target state="needs-translation">##### - 防火墙配置

Appliance 使用 iptables 防火墙预定义了一些规则：

-   开启了 SSH 端口 (22 TCP)；
-   开启了 Zabbix agent (10050 TCP) 及 Zabbix trapper (10051 TCP)
    端口；
-   开启了 HTTP (80 TCP) 及 HTTPS (443 TCP) 端口；
-   开启了 SNMP trap 端口 (162 UDP)；
-   开启了 连接到 NTP 的端口 (53 UDP)；
-   ICMP 数据包限制为每秒 5 个；
-   所有其他传入连接都将断开。</target>
      </trans-unit>
      <trans-unit id="b2283a9a" xml:space="preserve">
        <source>##### - Using a static IP address

By default the appliance uses DHCP to obtain the IP address. To specify
a static IP address:

-   Log in as root user;
-   Open */etc/sysconfig/network-scripts/ifcfg-eth0* file;
-   Replace *BOOTPROTO=dhcp* with *BOOTPROTO=none*
-   Add the following lines:
    -   *IPADDR=&lt;IP address of the appliance&gt;*
    -   *PREFIX=&lt;CIDR prefix&gt;*
    -   *GATEWAY=&lt;gateway IP address&gt;*
    -   *DNS1=&lt;DNS server IP address&gt;*
-   Run **systemctl restart network** command.

Consult the official Red Hat
[documentation](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/deployment_guide/s1-networkscripts-interfaces)
if needed.</source>
        <target state="needs-translation">##### - 使用静态 IP 地址

默认情况下，Appliance 使用 DHCP 来获取 IP 地址。如果要指定一个静态 IP 地址：

-   使用 root 用户登录；
-   打开 */etc/sysconfig/network-scripts/ifcfg-eth0* 文件；
-   将 *BOOTPROTO=dhcp* 替换为 *BOOTPROTO=none* ；
-   添加如下行：
    -   *IPADDR=&lt;Appliance 的 IP 地址&gt;*
    -   *PREFIX=&lt;CIDR 前缀&gt;*
    -   *GATEWAY=&lt;网关 IP 地址&gt;*
    -   *DNS1=&lt;DNS 服务器 IP 地址&gt;*
-   执行 **systemctl restart network** 命令。

如果需要的话，查询 Red Hat 官方[文档](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/deployment_guide/s1-networkscripts-interfaces)</target>
      </trans-unit>
      <trans-unit id="acf06935" xml:space="preserve">
        <source>##### - Changing time zone

By default the appliance uses UTC for the system clock. To change the
time zone, copy the appropriate file from */usr/share/zoneinfo* to
*/etc/localtime*, for example:

    cp /usr/share/zoneinfo/Europe/Riga /etc/localtime</source>
        <target state="needs-translation">##### - 更改时区

应用默认使用 UTC 作为系统时钟。如需更改时区，那么从 */usr/share/zoneinfo* 中复制合适的文件到
 */etc/localtime* 中，例如：

    cp /usr/share/zoneinfo/Asia/Shanghai /etc/localtime</target>
      </trans-unit>
      <trans-unit id="2c378c8d" xml:space="preserve">
        <source>#### - Zabbix configuration

Zabbix appliance setup has the following passwords and configuration
changes:</source>
        <target state="needs-translation">#### - Zabbix 配置

Zabbix Appliance 安装过程中使用了下列密码和配置：</target>
      </trans-unit>
      <trans-unit id="d39b5151" xml:space="preserve">
        <source>##### - Credentials (login:password)

System:

-   root:zabbix

Zabbix frontend:

-   Admin:zabbix

Database:

-   root:&lt;random&gt;
-   zabbix:&lt;random&gt;

::: noteclassic
Database passwords are randomly generated during the
installation process.\
Root password is stored inside the /root/.my.cnf file. It is not
required to input a password under the "root" account.
:::

To change the database user password, changes have to be made in the
following locations:

-   MySQL;
-   /etc/zabbix/zabbix\_server.conf;
-   /etc/zabbix/web/zabbix.conf.php.

::: noteclassic
 Separate users `zabbix_srv` and `zabbix_web` are defined
for the server and the frontend respectively. 
:::</source>
        <target state="needs-translation">##### - 凭证信息 (用户名:密码)

系统：

-   root:zabbix

Zabbix 前端：

-   Admin:zabbix

数据库：

-   root:&lt;random&gt;
-   zabbix:&lt;random&gt;

::: noteclassic
数据库密码是在安装过程中随机生成的。\
Root 密码存储在 /root/.my.cnf 文件中。不需要在 "root" 用户下输入密码。
:::

要更改数据库用户密码，必须在以下位置同时改变配置：

-   MySQL;
-   /etc/zabbix/zabbix\_server.conf;
-   /etc/zabbix/web/zabbix.conf.php.

::: noteclassic
分别为 Zabbix Server 和 Zabbix 前端 定义了单独的用户 `zabbix_srv` 及 `zabbix_web`。
:::</target>
      </trans-unit>
      <trans-unit id="f11ee379" xml:space="preserve">
        <source>##### - File locations

-   Configuration files are located in **/etc/zabbix**.
-   Zabbix server, proxy and agent logfiles are located in
    **/var/log/zabbix**.
-   Zabbix frontend is located in **/usr/share/zabbix**.
-   Home directory for the user **zabbix** is **/var/lib/zabbix**.</source>
        <target state="needs-translation">##### - 文件路径

-   配置文件位于 **/etc/zabbix**.
-   Zabbix server， proxy 及 agent 的日志文件位于   **/var/log/zabbix**。
-   Zabbix 前端位于 **/usr/share/zabbix**。
-    **zabbix** 用户的 home 目录位于 **/var/lib/zabbix**。</target>
      </trans-unit>
      <trans-unit id="e00773c5" xml:space="preserve">
        <source>##### - Changes to Zabbix configuration

-   Frontend timezone is set to Europe/Riga (this can be modified in
    **/etc/php-fpm.d/zabbix.conf**);</source>
        <target state="needs-translation">##### - 对 Zabbix 配置的更改

-   前端时区已被设置为 Europe/Riga （可以在 **/etc/php-fpm.d/zabbix.conf** 中修改）；</target>
      </trans-unit>
      <trans-unit id="2c4e4239" xml:space="preserve">
        <source>#### - Frontend access

By default, access to the frontend is allowed from anywhere.

The frontend can be accessed at *http://&lt;host&gt;*.

This can be customized in **/etc/nginx/conf.d/zabbix.conf**. Nginx has
to be restarted after modifying this file. To do so, log in using SSH as
**root** user and execute:

    systemctl restart nginx</source>
        <target state="needs-translation">#### - 访问前端

默认情况下，允许从任何位置访问前端。

前端可以从 *http://&lt;host&gt;* 进行访问。

可在 **/etc/nginx/conf.d/zabbix.conf** 中修改此设置。在修改此文件后必须重启 Nginx。为此，请使用 SSH 以 **root** 用户身份登录并执行：

    systemctl restart nginx</target>
      </trans-unit>
      <trans-unit id="4fbc391b" xml:space="preserve">
        <source>#### - Firewall

By default, only the ports listed in the [configuration
changes](#firewall_configuration) above are open. To open additional
ports, modify "*/etc/sysconfig/iptables*" file and reload firewall
rules:

    systemctl reload iptables</source>
        <target state="needs-translation">#### - 防火墙

默认情况下，只有[配置更改](#firewall_configuration) 上列举的端口是开放的。要添加额外的端口，可编辑 "*/etc/sysconfig/iptables*" 文件并重新加载防火墙规则。

    systemctl reload iptables</target>
      </trans-unit>
      <trans-unit id="bf60554b" xml:space="preserve">
        <source>#### - Upgrading

The Zabbix appliance packages may be upgraded. To do so, run:

    dnf update zabbix*</source>
        <target state="needs-translation">#### - 升级

Zabbix Appliance 的包可以升级。为此，可以执行：

    dnf update zabbix*</target>
      </trans-unit>
      <trans-unit id="d167c766" xml:space="preserve">
        <source>#### - System Services

Systemd services are available:

    systemctl list-units zabbix*</source>
        <target state="needs-translation">#### - 系统服务

在 Systemd 中列举 Zabbix 的 service：

    systemctl list-units zabbix*</target>
      </trans-unit>
      <trans-unit id="a582d1bf" xml:space="preserve">
        <source>#### - Format-specific notes</source>
        <target state="needs-translation">#### - 特定 image 的说明</target>
      </trans-unit>
      <trans-unit id="a45df55b" xml:space="preserve">
        <source>##### - VMware

The images in *vmdk* format are usable directly in VMware Player, Server
and Workstation products. For use in ESX, ESXi and vSphere they must be
converted using [VMware
converter](http://www.vmware.com/products/converter/).
If you use VMWare Converter, you may encounter issues with the hybrid network adapter. In that case, you can try
specifying the E1000 adapter during the conversion process. Alternatively, after the conversion is complete,
you can delete the existing adapter and add an E1000 adapter.</source>
        <target state="needs-translation">##### - VMware

*vmdk* 格式的 image 可直接在 VMware Player、 Server 和 Workstation 产品中使用。要在 ESX,、ESXi 和 vSphere 中使用，它们必须使用 [VMware
converter](http://www.vmware.com/products/converter/) 进行转换。</target>
      </trans-unit>
      <trans-unit id="7d1c1440" xml:space="preserve">
        <source>##### - HDD/flash image (raw)

    dd if=./zabbix_appliance_5.2.0.raw of=/dev/sdc bs=4k conv=fdatasync

Replace */dev/sdc* with your Flash/HDD disk device.</source>
        <target state="needs-translation">##### - HDD/flash image (raw)

    dd if=./zabbix_appliance_5.2.0.raw of=/dev/sdc bs=4k conv=fdatasync

将 */dev/sdc* 替换为你的 Flash/HDD 磁盘设备。</target>
      </trans-unit>
    </body>
  </file>
</xliff>
