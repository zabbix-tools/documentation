[comment]: # translation:outdated

[comment]: # ({new-8cf8bd5f})
# 3 新建监控项

[comment]: # ({/new-8cf8bd5f})

[comment]: # ({new-73502ab6})
#### 简介

本节你会学习如何新建一个监控项（Item）。

监控项是Zabbix中获得数据的基础。没有监控项，就没有数据——因为一个主机中只有监控项定义了单一的指标或者需要获得的数据。

[comment]: # ({/new-73502ab6})

[comment]: # ({new-8cca3d66})
#### 添加监控项

所有的监控项都是依赖于主机的。这就是当我们要配置一个监控项时，先要进入
*配置 → 主机* 页面查找到新建的主机。

在'新主机（New
host）'行中，*监控项（Items）*的链接旁的数量会显示为'0'。点击这个链接，然后点击*创建监控项（Create
item）*，将会显示一个监控项定义表格。\
![](../../../assets/en/manual/quickstart/new_item.png){width="600"}

所有必填项均以红色星标标示。

对于监控项的示例，需要输入以下必要的信息：

***名称（Name）***

-   输入 *CPU Load*
    作为值。在列表中和其他地方，都会显示这个值作为监控项名称。

***值（Key）***

-   手动输入 *system.cpu.load*
    作为值。这是监控项的一个技术上的名称，用于识别获取信息的类型。这个特定值需要是Zabbix
    Agent[预定义值](/manual/config/items/itemtypes/zabbix_agent)中的一种。

***信息类型（Type of information）***

-   在此处选择 *Numeric (float)*。这个属性定义了想获得数据的格式。

::: noteclassic
你也需要减少[监控项历史](/manual/config/items/history_and_trends)保留的天数，7或者14天。对于数据库而言，最佳实践是避免数据库保留过多的历史数据。
:::

我们暂时保持[其他选项](/manual/config/items/item#configuration)的默认值。

当完成后，点击*添加（Add）*。新的监控项将出现在监控项列表中。点击列表中的*详细（Details）*以查看具体细节。

![](../../../assets/en/manual/quickstart/item_created.png)

[comment]: # ({/new-8cca3d66})

[comment]: # ({new-c33e07f1})
#### 查看数据

当一个监控项定义完成后，你可能好奇它具体获得了什么值。前往*监控（Monitoring）
→ 最新数据（Latest data）*,
在过滤器中选择刚才新建的主机，然后点击*应用（Apply)*。

然后点击**- other -**前面的 **+**
，然后查看你之前定义的监控项和获得的值。

![](../../../assets/en/manual/quickstart/latest_data.png){width="600"}

同时，第一次获得的监控项值最多需要60秒才能到达。默认情况下，这是服务器读取变化后的配置文件，获取并执行新的监控项的频率。

如果你在‘变化（Change）’列中没有看到值，可能到目前为止只获得了一次值。等待30秒以获得新的监控项值。

如果你在没有看到类似截图中的监控项信息，请确认：

-   你输入的监控项'值（Key）' 和 '信息类型（Type of information）'
    同截图中的一致
-   agent和server都在运行状态
-   主机状态为'监控（Monitored）'并且它的可用性图标是绿色的
-   在主机的下拉菜单中已经选择了对应主机，且监控项处于启用状态

[comment]: # ({/new-c33e07f1})

[comment]: # ({new-7e0cbbcd})
##### 图表

当监控项运行了一段时间后，可以查看可视化图表。
[简单图表](/manual/config/visualisation/graphs/simple)
适用于任何被监控的数值型（numeric）监控项，且不需要额外的配置。这些图表会在运行时生成。

前往*监控（Monitoring） → 最新数据（Latest
data）*，然后点击监控项后的'图表（Graph）'链接以查看图表。

![](../../../assets/en/manual/quickstart/simple_graph1.png){width="600"}

[comment]: # ({/new-7e0cbbcd})
