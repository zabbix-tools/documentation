[comment]: # translation:outdated

[comment]: # ({new-848667e6})
# 4 新建触发器

[comment]: # ({/new-848667e6})

[comment]: # ({new-97bd6e32})
#### 概述

本节你会学习如何配置一个触发器（trigger）。

监控项只是用于收集数据。如果需要自动评估收到的数据，我们则需要定义触发器。触发器包含了一个表达式，这个表达式定义了数据的可接受的阈值级别。

如果收到的数据超过了这个定义好的级别，触发器将被“触发”，或者进入“异常（Problem）”状态——从而引起我们的注意，让我们知道有问题发生。如果数据再次恢复到合理的范围，触发器将会到“正常（Ok）”状态。

[comment]: # ({/new-97bd6e32})

[comment]: # ({new-e69902f4})
#### 添加触发器

为监控项配置触发器，前往*配置（Configuration） →
主机（Hosts）*，找到'新增主机（New
host）'，点击旁边的*触发器（Triggers）* ，然后点击*创建触发器（Create
trigger）*。这将会向我们展现一个触发器定义表单。\
![](../../../assets/en/manual/quickstart/new_trigger_a.png)

所有必填字端均以红色星标标示。

对于触发器，有下列必填项：

*名称（Name）*

-   输入 *CPU load too high on 'New host' for 3 minutes*
    作为值。这个值会作为触发器的名称被现实在列表和其他地方。

*表达式（Expression）*

-   输入：{New host:system.cpu.load.avg(3m)}>2

值时触发器的表达式。确认这个表达式输入正确，直到最后一个符号。此处，监控项值(system.cpu.load)用于指出具体的监控项。这个特定的表达式大致是说如果3分钟内，CPU负载的平均值超过2，那么就触发了问题的阈值。你可以查看更多的[触发器表达式语法](/manual/config/triggers/expression)信息。

完成后，点击*添加（Add）*。新的触发器将会显示在触发器列表中。

[comment]: # ({/new-e69902f4})

[comment]: # ({new-7a7f221f})
#### 显示触发器状态

当一个触发器定义完毕后，你可能想查看它的状态。

如果CPU负载超过了你在触发器中定义的阈值，这个问题将显示在*监控（Monitoring）
→ 问题（Problems）*中。

![](../../../assets/en/manual/quickstart/trigger_status1.png){width="600"}

闪烁意味着这个触发器状态最近30分钟内发生过变化。

[comment]: # ({/new-7a7f221f})
