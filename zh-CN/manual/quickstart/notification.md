[comment]: # translation:outdated

[comment]: # ({new-4c8ef099})
# 5 获取问题通知

[comment]: # ({/new-4c8ef099})

[comment]: # ({new-055c3bc8})
#### 简介

在本节中，你会学习如何在Zabbix中以通知（notifications）的方式配置报警（alerting）。

当监控项收集了数据后，触发器会根据异常状态触发报警。根据一些报警机制，它也会通知我们一些重要的事件，而不需要我们直接在Zabbix前端进行查看。

这就是通知（Notifications）的功能。E-mail是最常用的异常通知发送方式。我们将会学习如何配置e-mail通知。

[comment]: # ({/new-055c3bc8})

[comment]: # ({new-237d8e2e})
#### E-mail设置

Zabbix中最初内置了一些预定义的通知[发送方式](/manual/config/notifications/media)。[E-mail](/manual/config/notifications/media/email)
通知是其中的一种。

前往*管理（Administration） → 媒体类型（Media
types）*，点击预定义媒体类型列表中的*Email*，以配置E-mail。

![](../../../assets/en/manual/quickstart/media_types.png){width="600"}

这将向我们展现e-mail设置定义表单。\
![](../../../assets/en/manual/quickstart/media_type_email.png)

所有必填字段均以红色星标标示。

根据你的环境，设置SMTP服务器，SMTP helo， SMTP e-mail的值。

::: noteclassic
'SMTP
email'将作为Zabbix通知的'发件人（From）'地址。
:::

一切就绪后，点击 *更新（Update）*。

现在你已经配置了'Email'作为一种可用的媒体类型。一个媒体类型必须通过发送地址来关联用户(如同我们在[配置一个新用户](login#adding_user)中做的)，否则它将无法生效。

[comment]: # ({/new-237d8e2e})

[comment]: # ({new-c1762601})
#### 新建动作

发送通知是Zabbix中[动作（actions）](/manual/config/notifications/action)执行的操作之一。因此，为了建立一个通知，前往*配置（Configuration）
→ 动作（Actions）*，然后点击*创建动作（Create action）*。\
![](../../../assets/en/manual/quickstart/new_action1.png)

所有必填字段均以红色星标标示。

在这个表单中，输入这个动作的名称。

在大多数简单的例子中，如果我们不添加更多的指定[条件](/manual/config/notifications/action/conditions)，这个动作会在触发器从
'Ok' 变为 'Problem'时发生。

我们还需要定义这个动作具体做了什么 —— 即在 *操作（Operations）*
标签页中执行的操作。点击*新建（New）*，将会打开一个操作表单。\
![](../../../assets/en/manual/quickstart/new_operation1.png){width="600"}

所有必填字段均以红色星标标示。

这里，在*发送给用户（Send to
Users）*块中点击*添加（Add）*，然后选择我们之前定义的用户('user')。选择'Email'作为*Send
only to*的值。完成后，在操作明细区域中，点击*添加（Add）*。

在*默认主题（Default subject）* 和 *默认消息（Default
message）*字段可看到{TRIGGER.STATUS} 和 {TRIGGER.NAME}
宏（或者变量），它们会被具体的触发器状态和触发器名称替换。

这是一个简单的动作配置步骤，即点击动作表单中的*添加（Add）*。

[comment]: # ({/new-c1762601})

[comment]: # ({new-61999602})
#### 获得通知

现在，发送通知配置完成，我们看看它如何将通知发送给实际接收人。为了实现这个目的，我们需要你主机的负载，这样我们的[触发器](trigger#adding_trigger)才会被触发，我们会收到问题通知。

打开主机的控制台，并运行：

    cat /dev/urandom | md5sum

你需要运行一个或者多个[这样的进程](http://en.wikipedia.org/wiki/Md5sum)。

现在，前往*监控（Monitoring） → 最新数据（Latest data）*，查看'CPU
Load'的值是否已经增长。记住，为了使我们的触发器*触发（fire）*，'CPU
Load'的值需要在在3分钟运行的过程中超过2。一旦满足这个条件：

-   在*监控（Monitoring） →
    问题（Problems）*中，你可以看到闪烁‘Problem’状态的触发器。
-   你的e-mail中，会收到一个问题通知

::: noteimportant
如果通知功能没有正常工作：

-   再次验证e-mail设置和动作设置已经被正确配置
-   确认你创建的用户对生成事件的主机至少拥有读（read）权限。正如*[添加用户](login#adding_user)*步骤中提到的，'Zabbix
    administrators'用户组中的用户必须对'Linux
    servers'主机组（该主机所属组）至少拥有读（read）权限。
-   另外，你可以在*报告（Reports） → 动作日志（Action
    log）*中检查动作日志。


:::

[comment]: # ({/new-61999602})
