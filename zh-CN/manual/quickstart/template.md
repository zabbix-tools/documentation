[comment]: # translation:outdated

[comment]: # ({new-ffc9e78e})
# 6 新建模版

[comment]: # ({/new-ffc9e78e})

[comment]: # ({new-3bb3ba5d})
#### 概述

在本节中，你将会学习如何配置一个模版。

我们在之前的章节中学会了如何配置监控项、触发器，以及如果从主机上获得问题的通知。

虽然这些步骤提供了很大的灵活性，但仍然需要很多步骤才能完成。如果我们需要配置上千台主机，一些自动化操作会带来更多便利性。

模版（templates）功能可以实现这一点。模版允许对有用的监控项、触发器和其他对象进行分组，只需要一步就可以对监控主机应用模版，以达到反复重用的目的。

当一个模版链接到一个主机后，主机会继承这个模版中的所有对象。简单而言，一组预先定义好的检查会被快速应用到主机上。

[comment]: # ({/new-3bb3ba5d})

[comment]: # ({new-f182767e})
#### 添加模版

开始使用模版，你必须先创建一个。在*配置（Configuration） →
模版（Templates）*中，点击*创建模版（Create
template）*。这将会像我们展现一个模版配置表格。

![](../../../assets/en/manual/quickstart/new_template.png){width="550"}

所有必填字段以红色星标标示。

需要输入以下必填字段：

***模版名称（Template name）***

-   输入一个模版名称。可以使用数字、字母、空格及下划线。

***组（Groups）***

-   使用*选择（Select）*按钮选择一个或者多个组。模版必须属于一个组。

完成后，点击*添加（Add）*。你新建的模版可以在模版列表中查看。

![](../../../assets/en/manual/quickstart/template_list.png){width="600"}

你可以在这看到模版信息。但这个模版中没有任何信息——没有监控项、触发器或者其他对象。

[comment]: # ({/new-f182767e})

[comment]: # ({new-ad341c27})
#### 在模版中添加监控项

为了在模版中添加监控项，前往'New
host'的监控项列表。在*配置（Configuration） → 主机（Hosts）*，点击‘New
host’旁边的*监控项（Items）*。

然后：

-   选中列表中'CPU Load'监控项的选择框
-   点击列表下方的*复制（Copy）*
-   选择想要复制这个监控项的目标模版\

![](../../../assets/en/manual/quickstart/copy_to_template.png)

所有必填字段以红色星标标示。

-   点击*复制（Copy）*

你现在可以前往*配置（Configuration） → 模版（Templates）*，'新模版（New
template）'中会有一个新的监控项。

我们目前至创建了一个监控项，但你可以用同样的方法在模版中添加其他的监控项，触发器以及其他对象，直到完成满足特定需求（如监控OS，监控单个应用）的完整的对象组合。

[comment]: # ({/new-ad341c27})

[comment]: # ({new-230cf552})
#### 链接模版到主机

准备一个模版后，将它链接到一个主机。前往*配置（Configuration） →
主机（Hosts）*，点击'新主机（New
host）'打开表单，前往**模版（Templates）**标签页。

点击*链接新模版（Link new
templates）*旁边的*选择（Select）*，在弹出的窗口中，点击我们创建模版的名称('New
template')，它会出现在*链接新模版（Link new
templates）*区域，点击*添加（Add）*。这个模版会出现在*已链接模版（Linked
templates）*列表中。

![](../../../assets/en/manual/quickstart/link_template.png)

点击*更新（Update）*保存配置。现在，新模版及其所有的对象被添加到了主机。

你可能会想到，我们可以使用同样的方法将模版应用到其他主机。任何在模版级别的监控项、触发器及其他对象的变更，也会传递给所有链接该模版的主机。

[comment]: # ({/new-230cf552})

[comment]: # ({new-c7f1a5c4})
##### 链接预定义模版到主机

你可能注意到，Zabbix为各种操作系统、设备以及应用准备一些预定义的模版。为了快速部署监控，你可能会将它们中的一些与主机关联。但请注意，一些模版需要根据你的实际环境进行合适的调整。比如：一些检查项是不需要的，一些轮询周期过于频繁。

可参考该链接，查看更多关于[模版](/manual/config/templates)的信息。

[comment]: # ({/new-c7f1a5c4})
