[comment]: # translation:outdated

[comment]: # ({new-3f54a0e9})
# 1 登陆和配置用户

[comment]: # ({/new-3f54a0e9})

[comment]: # ({new-f6cb6160})
#### 简介

本章你会学习如何登陆Zabbix，以及在Zabbix内建立一个系统用户。

[comment]: # ({/new-f6cb6160})

[comment]: # ({new-6cb1478f})
#### 登陆

![](../../../assets/en/manual/quickstart/login.png){width="350"}

这是Zabbix的“欢迎”界面。输入用户名 **Admin** 以及密码 **zabbix** 以作为
[Zabbix超级用户](/manual/config/users_and_usergroups/permissions)登陆。

登陆后，你将会在页面右下角看到“以管理员连接（Connected as
Admin）”。同时会获得访问 *配置（Configuration）* and
*管理（Administration）* 菜单的权限。

[comment]: # ({/new-6cb1478f})

[comment]: # ({new-ecf369b8})
##### 暴力破解攻击的保护机制

为了防止暴力破解和词典攻击，如果发生连续五次尝试登陆失败，Zabbix界面将暂停30秒。

在下次成功登陆后，将会在界面上尝试登录失败的IP地址

[comment]: # ({/new-ecf369b8})

[comment]: # ({new-6be209b3})
#### 增加用户

可以在*管理（Administration） → 用户（Users）*下查看用户信息。

![](../../../assets/en/manual/quickstart/userlist.png){width="600"}

Zabbix在安装后只定义了两个用户。

-   'Admin' 用户是Zabbix的一个超级管理员，拥有所有权限。
-   'Guest'
    用户是一个特殊的默认用户。如果你没有登陆，你访问Zabbix的时候使用的其实是“guest”权限。默认情况下，"guest"用户对Zabbix中的对象没有任何权限。

点击 *创建用户（Create user）* 以增加用户。

在添加用户的表单中，确认将新增的用户添加到了一个已有的[用户组](/manual/config/users_and_usergroups/usergroup)，比如'Zabbix
administrators'。

![](../../../assets/en/manual/quickstart/new_user.png)

所有必填字端都以红色星标标记。

默认情况下，没有为新增的用户定义媒介（media，即通知发送方式)
。如需要创建，可以到'媒介（Media）'标签下，然后点击*增加（Add）*。

![](../../../assets/en/manual/quickstart/new_media.png)

在这个对话框中，为用户输入一个Email地址。

你可以为媒介指定一个时间活动周期，(访问[时间周期说明](/manual/appendix/time_period)页面，查看该字段格式的描述）。默认情况下，媒介一直是活动的。你也可以通过自定义[触发器严重等级](/manual/config/triggers/severity)来激活媒介，默认所有的等级都保持开启。

点击*新增（Add）*，然后在用户属性表单中点击*新增（Add）*。新的用户将出现在用户清单中。

![](../../../assets/en/manual/quickstart/userlist2.png){width="600"}

[comment]: # ({/new-6be209b3})

[comment]: # ({new-6e8f46b1})
##### Adding permissions

By default, a new user has no permissions to access hosts. To grant the
user rights, click on the group of the user in the *Groups* column (in
this case - 'Zabbix administrators'). In the group properties form, go
to the *Permissions* tab.

![](../../../assets/en/manual/quickstart/group_permissions.png){width="600"}

This user is to have read-only access to *Linux servers* group, so click
on *Select* next to the user group selection field.

![](../../../assets/en/manual/quickstart/add_permissions.png)

In this pop-up, mark the checkbox next to 'Linux servers', then click
*Select*. *Linux servers* should be displayed in the selection field.
Click the 'Read' button to set permission level and then *Add* to add
the group to the list of permissions. In the user group properties form,
click *Update*.

::: noteimportant
In Zabbix, access rights to hosts are assigned to
[user groups](/manual/config/users_and_usergroups/usergroup), not
individual users.
:::

Done! You may try to log in using the credentials of the new user.

[comment]: # ({/new-6e8f46b1})
