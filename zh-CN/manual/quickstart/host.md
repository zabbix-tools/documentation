[comment]: # translation:outdated

[comment]: # ({new-7905d51a})
# 2 新建主机

[comment]: # ({/new-7905d51a})

[comment]: # ({new-b1060785})
#### 简介

通过本节，你将会学习到如何建立一个新的主机。

Zabbix中的主机（Host）是一个你想要监控的网络实体（物理的，或者虚拟的）。Zabbix中，对于主机的定义非常灵活。它可以时一台物理服务器，一个网络交换机，一个虚拟机或者一些应用。

[comment]: # ({/new-b1060785})

[comment]: # ({new-9c1b7280})
#### 添加主机

Zabbix中，可以通过*配置（Configuration） →
主机（Hosts）*菜单，查看已配置的主机信息。默认已有一个名为'Zabbix
server'的预先定义好的主机。但我们需要学习如何添加另一个。

点击*创建主机（Create
host）*以添加新的主机，这将向我们显示一张主机配置表格。\
![](../../../assets/en/manual/quickstart/new_host.png){width="600"}

所有必填字端均以红色星标标示。

至少需要填写下列字段：

***主机名称（Host name）***

-   输入一个主机名称，可以使用字母数字、空格、点”."、中划线"-"、下划线"\_"。

***组***

-   从右边的选择框中，选择一个或者多个组，然后点击 **<<**
    移动它们到'所在组（In groups）'选择框。

::: noteclassic
所有访问权限都分配到主机组，而不是单独的主机。这也是主机需要属于至少一个组的原因。
:::

***IP地址***

-   输入主机的IP地址。注意如果这是Zabbix server的IP地址，它必须是Zabbix
    agent配置文件中‘Server’参数的值。

暂时保持[其他选项](/manual/config/hosts/host#configuration)的默认值。

当完成后，点击*添加（Add）*。你可以在主机列表中看到你新添加的主机。

<note
tip>如果*可用性（Availability）*列中的*ZBX*图标是红色的，通信可能存在一些问题。将你的鼠标移动到上面查看错误信息。如果这个图标是灰色的，说明目前状态还没更新。确认Zabbix
server正在运行，同时尝试过会儿刷新这个页面。
:::

[comment]: # ({/new-9c1b7280})
