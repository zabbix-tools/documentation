<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="zh-CN" datatype="plaintext" original="manual/quickstart/item.md">
    <body>
      <trans-unit id="8cf8bd5f" xml:space="preserve">
        <source># 3 New item</source>
      </trans-unit>
      <trans-unit id="73502ab6" xml:space="preserve">
        <source>#### Overview

In this section, you will learn how to set up an item.

Items are the basis of gathering data in Zabbix. Without items, there is
no data - because only an item defines a single metric or what kind of
data to collect from a host.</source>
      </trans-unit>
      <trans-unit id="8cca3d66" xml:space="preserve">
        <source>#### Adding item

All items are grouped around hosts. That is why to configure a sample
item we go to *Data collection → Hosts* and find the "New host" we have
created.

Click on the *Items* link in the row of "New host", and then click on
*Create item*. This will present us with an item definition form.

![](../../../assets/en/manual/quickstart/new_item.png)

All mandatory input fields are marked with a red asterisk.

For our sample item, the essential information to enter is:

***Name***

-   Enter *CPU load* as the value. This will be the item name displayed
    in lists and elsewhere.

***Key***

-   Manually enter *system.cpu.load* as the value. This is the technical
    name of an item that identifies the type of information that will be
    gathered. The particular key is just one of [pre-defined
    keys](/manual/config/items/itemtypes/zabbix_agent) that come with
    Zabbix agent.

***Type of information***

-   This attribute defines the format of the expected data. For the
    *system.cpu.load* key, this field will be automatically set to
    *Numeric (float)*.

::: noteclassic
You may also want to reduce the number of days [item
history](/manual/config/items/history_and_trends) will be kept, to 7 or
14. This is good practice to relieve the database from keeping lots of
historical values.
:::

[Other options](/manual/config/items/item#configuration) will suit us
with their defaults for now.

When done, click *Add*. The new item should appear in the item list.
Click on *Details* above the list to view what exactly was done.

![](../../../assets/en/manual/quickstart/item_created.png)</source>
      </trans-unit>
      <trans-unit id="c33e07f1" xml:space="preserve">
        <source>#### Viewing data

With an item defined, you might be curious if it is actually gathering
data. For that, go to *Monitoring → Latest data*, select 'New host' in
the filter and click on *Apply*.

![](../../../assets/en/manual/quickstart/latest_data.png){width="600"}

With that said, it may take up to 60 seconds for the first data to
arrive. That, by default, is how often the server reads configuration
changes and picks up new items to execute.

If you see no value in the 'Change' column, maybe only one value has
been received so far. Wait 30 seconds for another value to arrive.

If you do not see information about the item as in the screenshot, make
sure that:

-   you have filled out the item 'Key' and 'Type of information' fields;
    exactly as in the screenshot;
-   both the agent and the server are running;
-   host status is 'Monitored' and its availability icon is green;
-   the host selected in the host filter os correct; 
-   the item is enabled.</source>
      </trans-unit>
      <trans-unit id="7e0cbbcd" xml:space="preserve">
        <source>##### Graphs

With the item working for a while, it might be time to see something
visual. [Simple graphs](/manual/config/visualization/graphs/simple) are
available for any monitored numeric item without any additional
configuration. These graphs are generated on runtime.

To view the graph, go to *Monitoring → Latest data* and click on the
'Graph' link next to the item.

![](../../../assets/en/manual/quickstart/simple_graph.png){width="600"}</source>
      </trans-unit>
    </body>
  </file>
</xliff>
