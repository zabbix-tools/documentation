[comment]: # translation:outdated

[comment]: # ({new-d756b50a})
# discoveryrule.update

[comment]: # ({/new-d756b50a})

[comment]: # ({new-0d23e0d8})
### Description 说明

`object discoveryrule.update(object/array lldRules)`

This method allows to update existing LLD rules.
此方法允许更新已存在的LLD规则。

[comment]: # ({/new-0d23e0d8})

[comment]: # ({new-cf98658c})
### Parameters 参数

`(object/array)` LLD rule properties to be updated. `(object/array)`
要更新的''LLD规则属性。

The `itemid` property must be defined for each LLD rule, all other
properties are optional. Only the passed properties will be updated, all
others will remain unchanged.
每个LLD规则的`itemid`属性必须被定义，其他属性为可选。值传递要被更新的属性，其他属性保持不变。

Additionally to the [standard LLD rule properties](object#lld_rule), the
method accepts the following parameters. 另外见[standard LLD rule
properties](object#lld_rule)，此方法接受如下参数。

|Parameter|Type|Description|
|---------|----|-----------|
|filter|object|LLD rule filter object to replace the current filter. LLD规则要替换当前的筛选对象。|

[comment]: # ({/new-cf98658c})

[comment]: # ({new-6f962e26})
### Return values 返回值

`(object)` Returns an object containing the IDs of the updated LLD rules
under the `itemids` property.
`(object)`在`itemids`属性下返回一个包含被更新的LLD规则的IDs。

[comment]: # ({/new-6f962e26})

[comment]: # ({new-b41637d2})
### Examples 示例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-67c9278f})
#### Adding a filter to an LLD rule 为LLD规则添加一个筛选器

Add a filter so that the contents of the *{\#FSTYPE}* macro would match
the *\@File systems for discovery* regexp.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "discoveryrule.update",
    "params": {
        "itemid": "22450",
        "filter": {
            "evaltype": 1,
            "conditions": [
                {
                    "macro": "{#FSTYPE}",
                    "value": "@File systems for discovery"
                }
            ]
        }
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "22450"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-67c9278f})

[comment]: # ({new-f3fd4d6c})
#### Disable trapping 禁用trapping

Disable LLD trapping for discovery rule. 禁用LLD trapping 发现规则。

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "discoveryrule.update",
    "params": {
        "itemid": "28336",
        "allow_traps": "0"
    },
    "id": 36,
    "auth": "d678e0b85688ce578ff061bd29a20d3b"
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "28336"
        ]
    },
    "id": 36
}
```

[comment]: # ({/new-f3fd4d6c})

[comment]: # ({new-8d8cc019})
### Source

CDiscoveryRule::update() in
*frontends/php/include/classes/api/services/CDiscoveryRule.php*.

[comment]: # ({/new-8d8cc019})




[comment]: # ({new-bef0a5f5})
#### Updating LLD rule preprocessing options

Update an LLD rule with preprocessing rule “JSONPath”.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "discoveryrule.update",
    "params": {
        "itemid": "44211",
        "preprocessing": [
            {
                "type": "12",
                "params": "$.path.to.json",
                "error_handler": "2",
                "error_handler_params": "5"
            }
        ]
    },
    "auth": "700ca65537074ec963db7efabda78259",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "44211"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-bef0a5f5})

[comment]: # ({new-3f18a1c3})
#### Updating LLD rule script

Update an LLD rule script with a different script and remove unnecessary
parameters that were used by previous script.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "discoveryrule.update",
    "params": {
        "itemid": "23865",
        "parameters": [],
        "script": "Zabbix.Log(3, 'Log test');\nreturn 1;"
    },
    "auth": "700ca65537074ec963db7efabda78259",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "23865"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-3f18a1c3})

[comment]: # ({new-6a73b645})
### Source

CDiscoveryRule::update() in
*ui/include/classes/api/services/CDiscoveryRule.php*.

[comment]: # ({/new-6a73b645})
