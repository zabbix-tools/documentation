[comment]: # translation:outdated

[comment]: # ({new-04928fa3})
# discoveryrule.copy

[comment]: # ({/new-04928fa3})

[comment]: # ({new-8ae93b5b})
### Description

[comment]: # ({/new-8ae93b5b})

[comment]: # ({new-47295f20})
### 说明

`object discoveryrule.copy(object parameters)`

This method allows to copy LLD rules with all of the prototypes to the
given hosts. 此方法允许复制包含所有属性的LLD规则到给定的主机。

[comment]: # ({/new-47295f20})

[comment]: # ({new-b8760eee})
### Parameters 参数

`(object)` Parameters defining the LLD rules to copy and the target
hosts.

|Parameter|Type|Description|
|---------|----|-----------|
|discoveryids|array|IDs of the LLD rules to be copied.|
|hostids|array|IDs of the hosts to copy the LLD rules to.|

[comment]: # ({/new-b8760eee})

[comment]: # ({new-b41637d2})
### Return values

`(boolean)` Returns `true` if the copying was successful.

[comment]: # ({/new-b41637d2})

[comment]: # ({new-91be874e})
### Examples

[comment]: # ({/new-91be874e})

[comment]: # ({new-8870f994})
#### Copy an LLD rule to multiple hosts

Copy an LLD rule to two hosts.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "discoveryrule.copy",
    "params": {
        "discoveryids": [
            "27426"
        ],
        "hostids": [
            "10196",
            "10197"
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": true,
    "id": 1
}
```

### Source

CDiscoveryrule::copy() in
*frontends/php/include/classes/api/services/CDiscoveryRule.php*.

[comment]: # ({/new-8870f994})
