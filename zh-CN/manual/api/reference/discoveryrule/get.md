[comment]: # translation:outdated

[comment]: # ({new-ddce888e})
# discoveryrule.get

[comment]: # ({/new-ddce888e})

[comment]: # ({new-b87cd1ee})
### Description 说明

`integer/array discoveryrule.get(object parameters)`

The method allows to retrieve LLD rules according to the given
parameters. 此方法允许根据给定的参数获取LLD规则。

[comment]: # ({/new-b87cd1ee})

[comment]: # ({new-57007d3d})
### Parameters 参数

`(object)` Parameters defining the desired output. `(object)`
参数定义期望输出。

The method supports the following parameters. 此方法支持如下参数。

|Parameter|Type|Description|
|---------|----|-----------|
|itemids|string/array|Return only LLD rules with the given IDs. 返回给定IDs的LLD规则。|
|hostids|string/array|Return only LLD rules that belong to the given hosts. 返回属于给定主机的LLD规则。|
|inherited|boolean|If set to `true` return only LLD rules inherited from a template. 如果设为`true`，返回自称自某模板的LLD规则。|
|interfaceids|string/array|Return only LLD rules use the given host interfaces. 返回使用给定主机接口的LLD规则。|
|monitored|boolean|If set to `true` return only enabled LLD rules that belong to monitored hosts. 如果设为`true`，返回已经启用的属于已监控主机的LLD规则。|
|templated|boolean|If set to `true` return only LLD rules that belong to templates. 如果设为`true`，返回属于（多个）模板的LLD规则。|
|templateids|string/array|Return only LLD rules that belong to the given templates. 返回属于给定给定模板的LLD规则。|
|selectFilter|query|Returns the filter used by the LLD rule in the `filter` property. 在`filter`中返回LLD使用的筛选器。|
|selectGraphs|query|Returns graph prototypes that belong to the LLD rule in the `graphs` property. 在`graphs`属性中返回属于LLD规则的图表原型。<br><br>Supports `count`.|
|selectHostPrototypes|query|Returns host prototypes that belong to the LLD rule in the `hostPrototypes` property. 在`hostPrototypes`属性中返回属于该LLD规则的主机原型。<br><br>Supports `count`.|
|selectHosts|query|Returns the host that the LLD rule belongs to as an array in the `hosts` property. 在`hosts`属性下以数组形式返回属于该LLD规则的主机。|
|selectItems|query|Returns item prototypes that belong to the LLD rule in the `items` property. 在`items`下返回属于该LLD规则的item。<br><br>Supports `count`.|
|selectTriggers|query|Returns trigger prototypes that belong to the LLD rule in the `triggers` property. 在`triggers`属性下返回属于该触发器原型。<br><br>Supports `count`.|
|filter|object|Return only those results that exactly match the given filter. 仅返回紧缺匹配给定筛选条件的结果。<br><br>Accepts an array, where the keys are property names, and the values are either a single value or an array of values to match against. 接受一个数组，这些数组的键为属性名称，值是一个或数组中的值的要匹配的值。<br><br>Supports additional filters:<br>`host` - technical name of the host that the LLD rule belongs to.|
|limitSelects|integer|Limits the number of records returned by subselects. 限制子选择返回的结果的数量。<br><br>Applies to the following subselects:<br>`selctItems`;<br>`selectGraphs`;<br>`selectTriggers`.|
|sortfield|string/array|Sort the result by the given properties. 根据给定的属性把结果进行排序。<br><br>Possible values are: `itemid`, `name`, `key_`, `delay`, `type` and `status`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters).|
|editable|boolean|^|
|excludeSearch|boolean|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

[comment]: # ({/new-57007d3d})

[comment]: # ({new-7223bab1})
### Return values 返回值

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Examples 示例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-5f8b5429})
#### Retrieving discovery rules from a host 从一个主机获取多有的发现规则

Retrieve all discovery rules from host "10202".
获取主机“10202”所有的发现规则。

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "discoveryrule.get",
    "params": {
        "output": "extend",
        "hostids": "10202"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "itemid": "27425",
            "type": "0",
            "snmp_community": "",
            "snmp_oid": "",
            "hostid": "10202",
            "name": "Network interface discovery",
            "key_": "net.if.discovery",
            "delay": "1h",
            "state": "0",
            "status": "0",
            "trapper_hosts": "",
            "snmpv3_securityname": "",
            "snmpv3_securitylevel": "0",
            "snmpv3_authpassphrase": "",
            "snmpv3_privpassphrase": "",
            "error": "",
            "templateid": "22444",
            "params": "",
            "ipmi_sensor": "",
            "authtype": "0",
            "username": "",
            "password": "",
            "publickey": "",
            "privatekey": "",
            "interfaceid": "119",
            "port": "",
            "description": "Discovery of network interfaces as defined in global regular expression \"Network interfaces for discovery\".",
            "lifetime": "30d",
            "snmpv3_authprotocol": "0",
            "snmpv3_privprotocol": "0",
            "snmpv3_contextname": "",
            "jmx_endpoint": "",
            "master_itemid": "0",
            "timeout": "3s",
            "url": "",
            "query_fields": [],
            "posts": "",
            "status_codes": "200",
            "follow_redirects": "1",
            "post_type": "0",
            "http_proxy": "",
            "headers": [],
            "retrieve_mode": "0",
            "request_method": "1",
            "ssl_cert_file": "",
            "ssl_key_file": "",
            "ssl_key_password": "",
            "verify_peer": "0",
            "verify_host": "0",
            "allow_traps": "0"
        },
        {
            "itemid": "27426",
            "type": "0",
            "snmp_community": "",
            "snmp_oid": "",
            "hostid": "10202",
            "name": "Mounted filesystem discovery",
            "key_": "vfs.fs.discovery",
            "delay": "1h",
            "state": "0",
            "status": "0",
            "trapper_hosts": "",
            "snmpv3_securityname": "",
            "snmpv3_securitylevel": "0",
            "snmpv3_authpassphrase": "",
            "snmpv3_privpassphrase": "",
            "error": "",
            "templateid": "22450",
            "params": "",
            "ipmi_sensor": "",
            "authtype": "0",
            "username": "",
            "password": "",
            "publickey": "",
            "privatekey": "",
            "interfaceid": "119",
            "port": "",
            "description": "Discovery of file systems of different types as defined in global regular expression \"File systems for discovery\".",
            "lifetime": "30d",
            "snmpv3_authprotocol": "0",
            "snmpv3_privprotocol": "0",
            "snmpv3_contextname": "",
            "jmx_endpoint": "",
            "master_itemid": "0",
            "timeout": "3s",
            "url": "",
            "query_fields": [],
            "posts": "",
            "status_codes": "200",
            "follow_redirects": "1",
            "post_type": "0",
            "http_proxy": "",
            "headers": [],
            "retrieve_mode": "0",
            "request_method": "1",
            "ssl_cert_file": "",
            "ssl_key_file": "",
            "ssl_key_password": "",
            "verify_peer": "0",
            "verify_host": "0",
            "allow_traps": "0"
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-5f8b5429})

[comment]: # ({new-4d0af030})
#### Retrieving filter conditions

Retrieve the name of the LLD rule "24681" and its filter conditions. The
filter uses the "and" evaluation type, so the `formula` property is
empty and `eval_formula` is generated automatically.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "discoveryrule.get",
    "params": {
        "output": [
            "name"
        ],
        "selectFilter": "extend",
        "itemids": ["24681"]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "itemid": "24681",
            "name": "Filtered LLD rule",
            "filter": {
                "evaltype": "1",
                "formula": "",
                "conditions": [
                    {
                        "macro": "{#MACRO1}",
                        "value": "@regex1",
                        "operator": "8",
                        "formulaid": "A"
                    },
                    {
                        "macro": "{#MACRO2}",
                        "value": "@regex2",
                        "operator": "8",
                        "formulaid": "B"
                    },
                    {
                        "macro": "{#MACRO3}",
                        "value": "@regex3",
                        "operator": "8",
                        "formulaid": "C"
                    }
                ],
                "eval_formula": "A and B and C"
            }
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-4d0af030})

[comment]: # ({new-e3de6fe8})
#### Retrieve LLD rule by URL 根据URL获取LLD规则

Retrieve LLD rule for host by rule URL field value. Only exact match of
URL string defined for LLD rule is supported.
根据主机的规则URL字段值获取LLD规则。仅返回精确匹配定义的URL字符串的规则。

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "discoveryrule.get",
    "params": {
        "hostids": "10257",
        "filter": {
            "type": "19",
            "url": "http://127.0.0.1/discoverer.php"
        }
    },
    "id": 39,
    "auth": "d678e0b85688ce578ff061bd29a20d3b"
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "itemid": "28336",
            "type": "19",
            "snmp_community": "",
            "snmp_oid": "",
            "hostid": "10257",
            "name": "API HTTP agent",
            "key_": "api_discovery_rule",
            "delay": "5s",
            "history": "90d",
            "trends": "0",
            "status": "0",
            "value_type": "4",
            "trapper_hosts": "",
            "units": "",
            "snmpv3_securityname": "",
            "snmpv3_securitylevel": "0",
            "snmpv3_authpassphrase": "",
            "snmpv3_privpassphrase": "",
            "error": "",
            "lastlogsize": "0",
            "logtimefmt": "",
            "templateid": "0",
            "valuemapid": "0",
            "params": "",
            "ipmi_sensor": "",
            "authtype": "0",
            "username": "",
            "password": "",
            "publickey": "",
            "privatekey": "",
            "mtime": "0",
            "flags": "1",
            "interfaceid": "5",
            "port": "",
            "description": "",
            "inventory_link": "0",
            "lifetime": "30d",
            "snmpv3_authprotocol": "0",
            "snmpv3_privprotocol": "0",
            "state": "0",
            "snmpv3_contextname": "",
            "jmx_endpoint": "",
            "master_itemid": "0",
            "timeout": "3s",
            "url": "http://127.0.0.1/discoverer.php",
            "query_fields": [
                {
                    "mode": "json"
                },
                {
                    "elements": "2"
                }
            ],
            "posts": "",
            "status_codes": "200",
            "follow_redirects": "1",
            "post_type": "0",
            "http_proxy": "",
            "headers": {
                "X-Type": "api",
                "Authorization": "Bearer mF_A.B5f-2.1JcM"
            },
            "retrieve_mode": "0",
            "request_method": "1",
            "ssl_cert_file": "",
            "ssl_key_file": "",
            "ssl_key_password": "",
            "verify_peer": "0",
            "verify_host": "0",
            "allow_traps": "0"
        }
    ],
    "id": 39
}
```

[comment]: # ({/new-e3de6fe8})

[comment]: # ({new-c0ae2d21})
### See also

-   [Graph
    prototype](/zh/manual/api/reference/graphprototype/object#graph_prototype)
-   [Host](/zh/manual/api/reference/host/object#host)
-   [Item
    prototype](/zh/manual/api/reference/itemprototype/object#item_prototype)
-   [LLD rule filter](object#lld_rule_filter)
-   [Trigger
    prototype](/zh/manual/api/reference/triggerprototype/object#trigger_prototype)

[comment]: # ({/new-c0ae2d21})

[comment]: # ({new-f25d02bb})
### Source

CDiscoveryRule::get() in
*frontends/php/include/classes/api/services/CDiscoveryRule.php*.

[comment]: # ({/new-f25d02bb})


[comment]: # ({new-e14efed8})
### Source

CDiscoveryRule::get() in
*ui/include/classes/api/services/CDiscoveryRule.php*.

[comment]: # ({/new-e14efed8})
