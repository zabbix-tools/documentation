[comment]: # translation:outdated

[comment]: # ({new-c5a9e121})
# > LLD rule object LLD规则对象

[comment]: # ({/new-c5a9e121})

[comment]: # ({new-c17fa830})
## > LLD 规则对象

The following objects are directly related to the `discoveryrule` API.
下面的对象直接关联到`discoveryrule`（发现规则） API。

[comment]: # ({/new-c17fa830})

[comment]: # ({new-b0412737})
### LLD rule

[comment]: # ({/new-b0412737})

[comment]: # ({new-be83ef5c})
### LLD 规则

The low-level discovery rule object has the following properties.
低级发现规则对象有如下属性。

|Property属性             T|pe类型   Des|ription说明|
|----------------------------|--------------|-------------|
|itemid|string|*(readonly)* ID of the LLD rule. LLD规则的ID|
|**delay**<br>(required)|string|Update interval of the LLD rule. Accepts seconds or time unit with suffix and with or without one or more [custom intervals](/manual/config/items/item/custom_intervals) that consist of either flexible intervals and scheduling intervals as serialized strings. Also accepts user macros. Flexible intervals could be written as two macros separated by a forward slash. Intervals are separated by a semicolon.LLD规则更新间隔。接受s或者时间单位，有或没有一个或者多个的灵活间隔和固定计划间隔作为序列化字符串组成的[custom intervals](/manual/config/items/item/custom_intervals)|
|**hostid**<br>(required)|string|ID of the host that the LLD rule belongs to. LLD规则所属的Host的ID。|
|**interfaceid**<br>(required)|string|ID of the LLD rule's host interface. Used only for host LLD rules. LLD规则所属的host的借口的ID<br><br>Optional for Zabbix agent (active), Zabbix internal, Zabbix trapper and database monitor LLD rules. Zabbix agent (active), Zabbix internal, Zabbix trapper and 数据库监控LLD规则的可选参数。|
|**key\_**<br>(required)|string|LLD rule key. LLD规则键。|
|**name**<br>(required)|string|Name of the LLD rule. LLD规则名称。|
|**type**<br>(required)|integer|Type of the LLD rule. LLD规则类型。<br><br>Possible values: 可能的值：<br>0 - Zabbix agent;<br>1 - SNMPv1 agent;<br>2 - Zabbix trapper;<br>3 - simple check;<br>4 - SNMPv2 agent;<br>5 - Zabbix internal;<br>6 - SNMPv3 agent;<br>7 - Zabbix agent (active);<br>10 - external check;<br>11 - database monitor;<br>12 - IPMI agent;<br>13 - SSH agent;<br>14 - TELNET agent;<br>16 - JMX agent;<br>19 - HTTP agent;|
|**url**<br>(required)|string|URL string, required for HTTP agent LLD rule. Supports user macros, {HOST.IP}, {HOST.CONN}, {HOST.DNS}, {HOST.HOST}, {HOST.NAME}, {ITEM.ID}, {ITEM.KEY}. URL字符串，HTTP agent LLD rule要求有。支持用户宏， {HOST.IP}, {HOST.CONN}, {HOST.DNS}, {HOST.HOST}, {HOST.NAME}, {ITEM.ID}, {ITEM.KEY}。|
|allow\_traps|integer|HTTP agent LLD rule field. Allow to populate value as in trapper item type also.HTTP agent LLD规则字段。在陷阱监控项类型中也允许填充值<br><br>0 - *(default)* Do not allow to accept incoming data.0 - *(默认)*不允许接受输入数据<br>1 - Allow to accept incoming data.1 - 允许输入数据|
|authtype|integer|Used only by SSH agent or HTTP agent LLD rules. 只能被SSH agent或HTTP agent使用<br><br>SSH agent authentication method possible values: SSH agent认证方法可能的值：<br>0 - *(default)* password;<br>1 - public key.<br><br>HTTP agent authentication method possible values:HTTP agent认证方法可能的值：<br>0 - *(default)* none<br>1 - basic<br>2 - NTLM|
|description|string|Description of the LLD rule. LLD规则说明。|
|error|string|*(readonly)* Error text if there are problems updating the LLD rule. 如果更新LLD规则出问题时的错误文本。|
|follow\_redirects|integer|HTTP agent LLD rule field. Follow respose redirects while pooling data.HTTP agentLLD规则字段。当合并数据时进行重定向。<br><br>0 - Do not follow redirects.0 - 不跟随重定向。<br>1 - *(default)* Follow redirects. 1 - *(default)*跟随重定向。|
|headers|object|HTTP agent LLD rule field. Object with HTTP(S) request headers, where header name is used as key and header value as value. HTTP agent LLD规则字段。该对象带有HTTP(S)已键为名称，包头的值作为值的请求头。<br><br>Example:<br>{ "User-Agent": "Zabbix" }|
|http\_proxy|string|HTTP agent LLD rule field. HTTP(S) proxy connection string. HTTP agent LLD规则字段。HTTP(S) proxy连接字符串。|
|ipmi\_sensor|string|IPMI sensor. Used only by IPMI LLD rules. IPMI sensor。只用于IPMILLD规则|
|jmx\_endpoint|string|JMX agent custom connection string. JMX agent自定义连接字符串。<br><br>Default value: 默认值：<br>service:jmx:rmi:///jndi/rmi://{HOST.CONN}:{HOST.PORT}/jmxrmi|
|lifetime|string|Time period after which items that are no longer discovered will be deleted. Accepts seconds, time unit with suffix and user macro. 不在用于发现的item被删除的的时间周期。<br><br>Default: `30d`.|
|output\_format|integer|HTTP agent LLD rule field. Should response converted to JSON.HTTP agent LLD规则字段。应返回传递给JSON.<br><br>0 - *(default)* Store raw.<br>1 - Convert to JSON.|
|params|string|Additional parameters depending on the type of the LLD rule: 依赖于LLD规则类型的其他参数：<br>- executed script for SSH and Telnet LLD rules;- 为SSH何Telnet LLD规则执行脚本；<br>- SQL query for database monitor LLD rules;- 数据库监控LLD规则的SQL查询；<br>- formula for calculated LLD rules. - 计算类的LLD规则公式。|
|password|string|Password for authentication. Used by simple check, SSH, Telnet, database monitor, JMX and HTTP agent LLD rules. 认证密码。用于simple check, SSH, Telnet, database monitor, JMX and HTTP agent LLD 规则。|
|port|string|Port used by the LLD rule. Used only by SNMP LLD rules. LLD规则使用的端口。仅SNMP LLD规则使用|
|post\_type|integer|HTTP agent LLD rule field. Type of post data body stored in posts property.HTTP agent LLD 规则字段。post数据body部分存储在posts属性中的类型。<br><br>0 - *(default)* Raw data.<br>2 - JSON data.<br>3 - XML data.|
|posts|string|HTTP agent LLD rule field. HTTP(S) request body data. Used with post\_type. HTTP agent LLD规则字段。HTTP(S)请求body数据，在post\_type中使用。|
|privatekey|string|Name of the private key file.|
|publickey|string|Name of the public key file. 公共键文件的名称。|
|query\_fields|array|HTTP agent LLD rule field. Query parameters. Array of objects with 'key':'value' pairs, where value can be empty string. HTTP agent LLD规则字段。查询参数。带有'key':'value' 键值对的数组对象，值可以为空。|
|request\_method|integer|HTTP agent LLD rule field. Type of request method. HTTP agent LLD规则字段。请求方法类型。<br><br>0 - GET<br>1 - *(default)* POST<br>2 - PUT<br>3 - HEAD|
|retrieve\_mode|integer|HTTP agent LLD rule field. What part of response should be stored. HTTP agent LLD规则字段。指明哪部分响应应被存储起来。<br><br>0 - *(default)* Body.<br>1 - Headers.<br>2 - Both body and headers will be stored.<br><br>For request\_method HEAD only 1 is allowed value.|
|snmp\_community|string|SNMP community.<br><br>Required for SNMPv1 and SNMPv2 LLD rules.|
|snmp\_oid|string|SNMP OID.|
|snmpv3\_authpassphrase|string|SNMPv3 auth passphrase. Used only by SNMPv3 LLD rules. SNMPv3认证密码。仅在SNMPv3 LLD规则中使用。|
|snmpv3\_authprotocol|integer|SNMPv3 authentication protocol. Used only by SNMPv3 LLD rules. SNMPv3认证协议。仅在SNMPv3 LLD规则中使用。<br><br>Possible values:<br>0 - *(default)* MD5;<br>1 - SHA.|
|snmpv3\_contextname|string|SNMPv3 context name. Used only by SNMPv3 checks. SNMPv3文本名称。仅在SNMPv3检查中使用。|
|snmpv3\_privpassphrase|string|SNMPv3 priv passphrase. Used only by SNMPv3 LLD rules. SNMPv3秘钥。仅在SNMPv3 LLD规则使用。|
|snmpv3\_privprotocol|integer|SNMPv3 privacy protocol. Used only by SNMPv3 LLD rules. SNMPv3私密协议。仅在SNMPv3 LLD规则使用。<br><br>Possible values:<br>0 - *(default)* DES;<br>1 - AES.|
|snmpv3\_securitylevel|integer|SNMPv3 security level. Used only by SNMPv3 LLD rules. SNMPv3安全等级。仅在SNMPv3 LLD规则使用。<br><br>Possible values:<br>0 - noAuthNoPriv;<br>1 - authNoPriv;<br>2 - authPriv.|
|snmpv3\_securityname|string|SNMPv3 security name. Used only by SNMPv3 LLD rules. SNMPv3安全名称。仅在SNMPv3 LLD规则使用。|
|ssl\_cert\_file|string|HTTP agent LLD rule field. Public SSL Key file path. HTTP agent LLD规则字段。公共SSL键文件路径。|
|ssl\_key\_file|string|HTTP agent LLD rule field. Private SSL Key file path. HTTP agent LLD规则字段。私有SSL键文件路径。|
|ssl\_key\_password|string|HTTP agent LLD rule field. Password for SSL Key file. HTTP agent LLD规则字段。SSL键文件密码。|
|state|integer|*(readonly)* State of the LLD rule.<br><br>Possible values:<br>0 - *(default)* normal;<br>1 - not supported.|
|status|integer|Status of the LLD rule.<br><br>Possible values:<br>0 - *(default)* enabled LLD rule;<br>1 - disabled LLD rule.|
|status\_codes|string|HTTP agent LLD rule field. Ranges of required HTTP status codes separated by commas. Also supports user macros as part of comma separated list. HTTP agent LLD规则字段。以逗号分隔的HTTP要求的状态码范围。<br><br>Example: 200,200-{$M},{$M},200-400|
|templateid|string|(readonly) ID of the parent template LLD rule. （只读）父模板LLD规则的ID。|
|timeout|string|HTTP agent LLD rule field. Item data polling request timeout. Support user macros. HTTP agent LLD规则字段。Item数据轮训请求超时时间。知识用户宏。<br><br>default: 3s<br>maximum value: 60s|
|trapper\_hosts|string|Allowed hosts. Used by trapper LLD rules or HTTP agent LLD rules. 允许的主机。用于trapper LLD规则或HTTP agent LLD规则。|
|username|string|Username for authentication. Used by simple check, SSH, Telnet, database monitor, JMX and HTTP agent LLD rules. 认证的用户名。用于simple check, SSH, Telnet, database monitor, JMX and HTTP agent LLD 规则<br><br>Required by SSH and Telnet LLD rules. SSH 和 Telnet LLD 规则要求。|
|verify\_host|integer|HTTP agent LLD rule field. Validate host name in URL is in Common Name field or a Subject Alternate Name field of host certificate. HTTP agent LLD规则字段。URL中的主机名处于通用名称字段或主机证书的主题备用名称字段的合法性。<br><br>0 - *(default)* Do not validate.<br>1 - Validate.|
|verify\_peer|integer|HTTP agent LLD rule field. Validate is host certificate authentic. HTTP agent LLD规则字段。主机认证证书合法性。<br><br>0 - *(default)* Do not validate.<br>1 - Validate.|

[comment]: # ({/new-be83ef5c})

[comment]: # ({new-4a2b52b9})
### LLD rule filter

[comment]: # ({/new-4a2b52b9})

[comment]: # ({new-14dc55c1})
### LLD 规则 过滤器

The LLD rule filter object defines a set of conditions that can be used
to filter discovered objects. It has the following properties:
LLD规则筛选器对象定义一套能被用于过滤器发现对象的条件。它包含如下属性：

|Property|Type|Description|
|--------|----|-----------|
|**conditions**<br>(required)|array|Set of filter conditions to use for filtering results.|
|**evaltype**<br>(required)|integer|Filter condition evaluation method.<br><br>Possible values:<br>0 - and/or;<br>1 - and;<br>2 - or;<br>3 - custom expression.|
|eval\_formula|string|*(readonly)* Generated expression that will be used for evaluating filter conditions. The expression contains IDs that reference specific filter conditions by its `formulaid`. The value of `eval_formula` is equal to the value of `formula` for filters with a custom expression. 生成的表达式将用于评估过滤器条件。表达式包含通过其“ormulaid”引用特定筛选条件的ID。`eval_formula` 的值与`formula` 的值相等|
|formula|string|User-defined expression to be used for evaluating conditions of filters with a custom expression. The expression must contain IDs that reference specific filter conditions by its `formulaid`. The IDs used in the expression must exactly match the ones defined in the filter conditions: no condition can remain unused or omitted. 用户定义表达式，用于评估具有自定义表达式的筛选器的条件。表达式必须包含通过其“公式辅助”引用特定筛选条件的ID。表达式中使用的ID必须与筛选条件中定义的ID完全匹配：没有条件可以保持未使用或省略。<br><br>Required for custom expression filters.|

[comment]: # ({/new-14dc55c1})

[comment]: # ({new-2f32e5f0})
#### LLD rule filter condition

[comment]: # ({/new-2f32e5f0})

[comment]: # ({new-573b52c4})
#### LLD rule 过滤器条件

The LLD rule filter condition object defines a separate check to perform
on the value of an LLD macro. It has the following properties:
LLD规则过滤器条件对象定义对LLD宏的值执行的单独检查：

|Property|Type|Description|
|--------|----|-----------|
|**macro**<br>(required)|string|LLD macro to perform the check on.|
|**value**<br>(required)|string|Value to compare with.|
|formulaid|string|Arbitrary unique ID that is used to reference the condition from a custom expression. Can only contain capital-case letters. The ID must be defined by the user when modifying filter conditions, but will be generated anew when requesting them afterward. 用于从自定义表达式引用条件的任意唯一ID。只能包含大写字母。在修改过滤条件时，ID必须由用户定义，但在请求之后，将重新生成ID。|
|operator|integer|Condition operator.<br><br>Possible values:<br>8 - *(default)* matches regular expression;<br>9 - does not match regular expression.|

::: notetip
To better understand how to use filters with various
types of expressions, see examples on the
[discoveryrule.get](get#retrieving_filter_conditions) and
[discoveryrule.create](create#using_a_custom_expression_filter) method
pages.
:::

[comment]: # ({/new-573b52c4})












[comment]: # ({new-e47f6c6a})
#### LLD rule override filter condition

The LLD rule override filter condition object defines a separate check
to perform on the value of an LLD macro. It has the following
properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**macro**<br>(required)|string|LLD macro to perform the check on.|
|**value**<br>(required)|string|Value to compare with.|
|formulaid|string|Arbitrary unique ID that is used to reference the condition from a custom expression. Can only contain capital-case letters. The ID must be defined by the user when modifying filter conditions, but will be generated anew when requesting them afterward.|
|operator|integer|Condition operator.<br><br>Possible values:<br>8 - *(default)* matches regular expression;<br>9 - does not match regular expression;<br>12 - exists;<br>13 - does not exist.|

[comment]: # ({/new-e47f6c6a})

[comment]: # ({new-e7b08f3c})
#### LLD rule override operation

The LLD rule override operation is combination of conditions and actions
to perform on the prototype object. It has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**operationobject**<br>(required)|integer|Type of discovered object to perform the action.<br><br>Possible values:<br>0 - Item prototype;<br>1 - Trigger prototype;<br>2 - Graph prototype;<br>3 - Host prototype.|
|operator|integer|Override condition operator.<br><br>Possible values:<br>0 - *(default)* equals;<br>1 - does not equal;<br>2 - contains;<br>3 - does not contain;<br>8 - matches;<br>9 - does not match.|
|value|string|Pattern to match item, trigger, graph or host prototype name depending on selected object.|
|opstatus|object|Override operation status object for item, trigger and host prototype objects.|
|opdiscover|object|Override operation discover status object (all object types).|
|opperiod|object|Override operation period (update interval) object for item prototype object.|
|ophistory|object|Override operation history object for item prototype object.|
|optrends|object|Override operation trends object for item prototype object.|
|opseverity|object|Override operation severity object for trigger prototype object.|
|optag|array|Override operation tag object for trigger and host prototype objects.|
|optemplate|array|Override operation template object for host prototype object.|
|opinventory|object|Override operation inventory object for host prototype object.|

[comment]: # ({/new-e7b08f3c})

[comment]: # ({new-52d3b579})
##### LLD rule override operation status

LLD rule override operation status that is set to discovered object. It
has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**status**<br>(required)|integer|Override the status for selected object.<br><br>Possible values:<br>0 - Create enabled;<br>1 - Create disabled.|

[comment]: # ({/new-52d3b579})

[comment]: # ({new-ff620350})
##### LLD rule override operation discover

LLD rule override operation discover status that is set to discovered
object. It has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**discover**<br>(required)|integer|Override the discover status for selected object.<br><br>Possible values:<br>0 - Yes, continue discovering the objects;<br>1 - No, new objects will not be discovered and existing ones will me marked as lost.|

[comment]: # ({/new-ff620350})

[comment]: # ({new-6e469c1b})
##### LLD rule override operation period

LLD rule override operation period is an update interval value (supports
custom intervals) that is set to discovered item. It has the following
properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**delay**<br>(required)|string|Override the update interval of the item prototype. Accepts seconds or a time unit with suffix (30s,1m,2h,1d) as well as flexible and scheduling intervals and user macros or LLD macros. Multiple intervals are separated by a semicolon.|

[comment]: # ({/new-6e469c1b})

[comment]: # ({new-ff97489f})
##### LLD rule override operation history

LLD rule override operation history value that is set to discovered
item. It has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**history**<br>(required)|string|Override the history of item prototype which is a time unit of how long the history data should be stored. Also accepts user macro and LLD macro.|

[comment]: # ({/new-ff97489f})

[comment]: # ({new-10b52601})
##### LLD rule override operation trends

LLD rule override operation trends value that is set to discovered item.
It has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**trends**<br>(required)|string|Override the trends of item prototype which is a time unit of how long the trends data should be stored. Also accepts user macro and LLD macro.|

[comment]: # ({/new-10b52601})

[comment]: # ({new-991f3bdc})
##### LLD rule override operation severity

LLD rule override operation severity value that is set to discovered
trigger. It has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**severity**<br>(required)|integer|Override the severity of trigger prototype.<br><br>Possible values are: 0 - *(default)* not classified;<br>1 - information;<br>2 - warning;<br>3 - average;<br>4 - high;<br>5 - disaster.|

[comment]: # ({/new-991f3bdc})

[comment]: # ({new-0693b8f3})
##### LLD rule override operation tag

LLD rule override operation tag object contains tag name and value that
are set to discovered object. It has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**tag**<br>(required)|string|New tag name.|
|value|string|New tag value.|

[comment]: # ({/new-0693b8f3})

[comment]: # ({new-5eebc05a})
##### LLD rule override operation template

LLD rule override operation template object that is linked to discovered
host. It has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**templateid**<br>(required)|string|Override the template of host prototype linked templates.|

[comment]: # ({/new-5eebc05a})

[comment]: # ({new-9c5cc3d1})
##### LLD rule override operation inventory

LLD rule override operation inventory mode value that is set to
discovered host. It has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**inventory\_mode**<br>(required)|integer|Override the host prototype inventory mode.<br><br>Possible values are:<br>-1 - disabled;<br>0 - *(default)* manual;<br>1 - automatic.|

[comment]: # ({/new-9c5cc3d1})
