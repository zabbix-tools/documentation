[comment]: # translation:outdated

[comment]: # ({new-293a482d})
# discoveryrule.delete

[comment]: # ({/new-293a482d})

[comment]: # ({new-1760a96c})
### Description 说明

`object discoveryrule.delete(array lldRuleIds)`

This method allows to delete LLD rules. 此方法允许删除LLD规则。

[comment]: # ({/new-1760a96c})

[comment]: # ({new-27340eaa})
### Parameters 参数

`(array)` IDs of the LLD rules to delete.
`(array)`要删除的LLD规则的IDs。

[comment]: # ({/new-27340eaa})

[comment]: # ({new-ae1ac187})
### Return values返回值

`(object)` Returns an object containing the IDs of the deleted LLD rules
under the `itemids` property.
`(object)`在`itemids`下返回一个包含被删除的LLD规则的IDs。

[comment]: # ({/new-ae1ac187})

[comment]: # ({new-b41637d2})
### Examples示例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-98660baf})
#### Deleting multiple LLD rules 删除多个LLD规则

Delete two LLD rules.删除2个LLD规则。

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "discoveryrule.delete",
    "params": [
        "27665",
        "27668"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "ruleids": [
            "27665",
            "27668"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-98660baf})

[comment]: # ({new-76a51bc0})
### Source

CDiscoveryRule::delete() in
*frontends/php/include/classes/api/services/CDiscoveryRule.php*.

[comment]: # ({/new-76a51bc0})
