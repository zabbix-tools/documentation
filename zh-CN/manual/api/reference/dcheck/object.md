[comment]: # translation:outdated

[comment]: # ({new-a9b6b9e5})
# > 对象

下列对象与 `dcheck` API 直接相关。

[comment]: # ({/new-a9b6b9e5})

[comment]: # ({new-662afbc9})
### 发现检查

发现检查对象定义了由一个网络发现规则执行的一个特定检查。其具有以下属性。

|属性                     类|描述|<|
|------------------------------|------|-|
|dcheckid|字符串   *（|读）* 发现检查的 ID。|
|druleid|字符串   *（|读）* 该检查属于的发现规则的 ID。|
|key\_|字符串   此属|的值根据检查的类型而不同：<br>- 查询 Zabbix 客户端检查的键值，需要的；<br>- SNMPv1，SNMPv2 和 SNMPv3 检查所需的 SNMP OID，需要的。|
|ports|字符串   用逗|分隔的一个或几个端口范围。用于除 ICMP 之外的所有检查。<br><br>默认：0。|
|snmp\_community|字符串   SN|P 社区字符串。<br><br>SNMPv1 和 SNMPv2 客户端检查需要使用。|
|snmpv3\_authpassphrase|字符串   用于|SNMPv3 客户端检查的身份验证密码，安全级别可设置为 *authNoPriv* 或 *authPriv*。|
|snmpv3\_authprotocol|整数     用|SNMPv3 客户端检查的身份验证协议，安全级别可设置为 *authNoPriv* 或 *authPriv*。<br><br>可能的值：<br>0 - *（默认）* MD5；<br>1 - SHA。|
|snmpv3\_contextname|字符串   SN|Pv3 环境名称。只用于 SNMPv3 检查。|
|snmpv3\_privpassphrase|字符串   用于|SNMPv3 客户端检查的隐私验证密码，安全级别可设置为 *authPriv*。|
|snmpv3\_privprotocol|整数     用|SNMPv3 客户端检查的隐私验证协议，安全级别可设置为 *authPriv*。<br><br>可能的值：<br>0 - *（默认）* DES；<br>1 - AES。|
|snmpv3\_securitylevel|字符串   用于|SNMPv3 客户端检查的安全级别。<br><br>可能的值：<br>0 - noAuthNoPriv；<br>1 - authNoPriv；<br>2 - authPriv。|
|snmpv3\_securityname|字符串   用于|SNMPv3 客户端检查的安全名称。|
|**type**<br>（需要的）|整数     检<br>|的类型。<br>可能的值：<br>0 - SSH；<br>1 - LDAP；<br>2 - SMTP；<br>3 - FTP；<br>4 - HTTP；<br>5 - POP；<br>6 - NNTP；<br>7 - IMAP；<br>8 - TCP；<br>9 - Zabbix 客户端；<br>10 - SNMPv1 客户端；<br>11 - SNMPv2 客户端；<br>12 - ICMP ping；<br>13 - SNMPv3 客户端；<br>14 - HTTPS；<br>15 - Telnet。|
|uniq|整数     是|将此检查作为设备惟一性条件。对于发现规则，只能配置一个唯一的检查。用于 Zabbix 客户端、SNMPv1、SNMPv2 和 SNMPv3 等客户端检查。<br><br>可能的值：<br>0 - *（默认）* 不使用该检查作为惟一条件；<br>1 - 使用该检查作为惟一条件。|

[comment]: # ({/new-662afbc9})
