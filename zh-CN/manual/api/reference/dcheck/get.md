[comment]: # translation:outdated

[comment]: # ({new-25145295})
# 获取

[comment]: # ({/new-25145295})

[comment]: # ({new-cf2cd01b})
### 描述

`integer/array dcheck.get(object parameters)`

这个方法允许根据给定的参数检索发现检查。

[comment]: # ({/new-cf2cd01b})

[comment]: # ({new-3db1bd90})
### 参数

`(object)` 定义需要输出的参数。

这个方法支持以下参数。

|参数                     类|描述|<|
|------------------------------|------|-|
|dcheckids|字符串/数组   只返回拥|给定 ID 的发现检查。|
|druleids|字符串/数组   只返回发|检查，该检查属于给定的`发现规则`。|
|dserviceids|字符串/数组   只返回发|检查，该检查已检测到给定的`已发现服务`。|
|sortfield|字符串/数组   根据给定|属性对结果进行排序。<br><br>可能的值有：`dcheckid` 和 `druleid`。|
|countOutput|布尔值        在[|用评论](/manual/api/reference_commentary#common_get_method_parameters)中详细描述了所有 `get` 方法的常见参数。|
|editable|布尔值        ::|<|
|excludeSearch|布尔值        ::|<|
|filter|对象          :|:|
|limit|整数          :|:|
|output|查询          :|:|
|preservekeys|布尔值        ::|<|
|search|对象          :|:|
|searchByAny|布尔值        ::|<|
|searchWildcardsEnabled|布尔值        ::|<|
|sortorder|字符串/数组   :::|<|
|startSearch|布尔值        ::|<|

[comment]: # ({/new-3db1bd90})

[comment]: # ({new-7223bab1})
### 返回值

`(integer/array)` 返回：

-   一个对象数组；
-   如果使用了 `countOutput` 参数，被检索的对象的数量。

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### 示例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-90f6afd9})
#### 为一个发现规则检索发现检查

检索被发现规则 "6" 使用的所有发现检查。

请求：

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "dcheck.get",
    "params": {
        "output": "extend",
        "dcheckids": "6"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

响应：

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "dcheckid": "6",
            "druleid": "4",
            "type": "3",
            "key_": "",
            "snmp_community": "",
            "ports": "21",
            "snmpv3_securityname": "",
            "snmpv3_securitylevel": "0",
            "snmpv3_authpassphrase": "",
            "snmpv3_privpassphrase": "",
            "uniq": "0",
            "snmpv3_authprotocol": "0",
            "snmpv3_privprotocol": "0"
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-90f6afd9})

[comment]: # ({new-d1a6b685})
### 来源

CDCheck::get() in
*frontends/php/include/classes/api/services/CDCheck.php*.

[comment]: # ({/new-d1a6b685})
