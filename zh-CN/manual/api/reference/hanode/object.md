[comment]: # translation:outdated

[comment]: # ({new-bf586210})
# > High availability node object

The following object is related to operating a High availability cluster
of Zabbix servers.

[comment]: # ({/new-bf586210})

[comment]: # ({new-9cc6b816})
### High availability node

::: noteclassic
Nodes are created by the Zabbix server and cannot be
modified via the API.
:::

The High availability node object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|ha\_nodeid|string|ID of the node.|
|name|string|Name assigned to the node, using the HANodeName configuration entry of zabbix\_server.conf. Empty for a server running in standalone mode.|
|address|string|IP or DNS name where the node connects from.|
|port|integer|Port on which the node is running.|
|lastaccess|integer|Heartbeat time, t.i. time of last update from the node. UTC timestamp.|
|status|integer|State of the node.<br><br>Possible values:<br>0 - standby;<br>1 - stopped manually;<br>2 - unavailable;<br>3 - active.|

[comment]: # ({/new-9cc6b816})
