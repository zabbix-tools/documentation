[comment]: # translation:outdated

[comment]: # ({new-619023b5})
# role.get

[comment]: # ({/new-619023b5})

[comment]: # ({new-ed6f4d70})
### Description

`integer/array role.get(object parameters)`

The method allows to retrieve roles according to the given parameters.

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-ed6f4d70})

[comment]: # ({new-3b10ad42})
### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|roleids|string/array|Return only roles with the given IDs.|
|selectRules|query|Return role rules in the [rules](/manual/api/reference/role/object#role_rules) property.|
|selectUsers|query|Select [users](/manual/api/reference/user/object) this role is assigned to.|
|sortfield|string/array|Sort the result by the given properties.<br><br>Possible values are: `roleid`, `name`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters) page.|
|editable|boolean|^|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

[comment]: # ({/new-3b10ad42})

[comment]: # ({new-7223bab1})
### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-ac3322b8})
#### Retrieving role data

Retrieve "Super admin role" role data and its access rules.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "role.get",
    "params": [
        "output": "extend",
        "selectRules": "extend",
        "roleids": "3"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "roleid": "3",
            "name": "Super admin role",
            "type": "3",
            "readonly": "1",
            "rules": {
                "ui": [
                    {
                        "name": "monitoring.dashboard",
                        "status": "1"
                    },
                    {
                        "name": "monitoring.problems",
                        "status": "1"
                    },
                    {
                        "name": "monitoring.hosts",
                        "status": "1"
                    },
                    {
                        "name": "monitoring.latest_data",
                        "status": "1"
                    },
                    {
                        "name": "monitoring.maps",
                        "status": "1"
                    },
                    {
                        "name": "monitoring.services",
                        "status": "1"
                    },
                    {
                        "name": "inventory.overview",
                        "status": "1"
                    },
                    {
                        "name": "inventory.hosts",
                        "status": "1"
                    },
                    {
                        "name": "reports.availability_report",
                        "status": "1"
                    },
                    {
                        "name": "reports.top_triggers",
                        "status": "1"
                    },
                    {
                        "name": "monitoring.discovery",
                        "status": "1"
                    },
                    {
                        "name": "reports.notifications",
                        "status": "1"
                    },
                    {
                        "name": "reports.scheduled_reports",
                        "status": "1"
                    },
                    {
                        "name": "configuration.host_groups",
                        "status": "1"
                    },
                    {
                        "name": "configuration.templates",
                        "status": "1"
                    },
                    {
                        "name": "configuration.hosts",
                        "status": "1"
                    },
                    {
                        "name": "configuration.maintenance",
                        "status": "1"
                    },
                    {
                        "name": "configuration.actions",
                        "status": "1"
                    },
                    {
                        "name": "configuration.discovery",
                        "status": "1"
                    },
                    {
                        "name": "reports.system_info",
                        "status": "1"
                    },
                    {
                        "name": "reports.audit",
                        "status": "1"
                    },
                    {
                        "name": "reports.action_log",
                        "status": "1"
                    },
                    {
                        "name": "configuration.event_correlation",
                        "status": "1"
                    },
                    {
                        "name": "administration.general",
                        "status": "1"
                    },
                    {
                        "name": "administration.proxies",
                        "status": "1"
                    },
                    {
                        "name": "administration.authentication",
                        "status": "1"
                    },
                    {
                        "name": "administration.user_groups",
                        "status": "1"
                    },
                    {
                        "name": "administration.user_roles",
                        "status": "1"
                    },
                    {
                        "name": "administration.users",
                        "status": "1"
                    },
                    {
                        "name": "administration.media_types",
                        "status": "1"
                    },
                    {
                        "name": "administration.scripts",
                        "status": "1"
                    },
                    {
                        "name": "administration.queue",
                        "status": "1"
                    }
                ],
                "ui.default_access": "1",
                "services.read.mode": "1",
                "services.read.list": [],
                "services.read.tag": {
                    "tag": "",
                    "value": ""
                },
                "services.write.mode": "1",
                "services.write.list": [],
                "services.write.tag": {
                    "tag": "",
                    "value": ""
                },
                "modules": [],
                "modules.default_access": "1",
                "api.access": "1",
                "api.mode": "0",
                "api": [],
                "actions": [
                    {
                        "name": "edit_dashboards",
                        "status": "1"
                    },
                    {
                        "name": "edit_maps",
                        "status": "1"
                    },
                    {
                        "name": "acknowledge_problems",
                        "status": "1"
                    },
                    {
                        "name": "close_problems",
                        "status": "1"
                    },
                    {
                        "name": "change_severity",
                        "status": "1"
                    },
                    {
                        "name": "add_problem_comments",
                        "status": "1"
                    },
                    {
                        "name": "execute_scripts",
                        "status": "1"
                    },
                    {
                        "name": "manage_api_tokens",
                        "status": "1"
                    },
                    {
                        "name": "edit_maintenance",
                        "status": "1"
                    },
                    {
                        "name": "manage_scheduled_reports",
                        "status": "1"
                    }
                ],
                "actions.default_access": "1"
            }
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-ac3322b8})

[comment]: # ({new-f3933c89})
### See also

-   [Role rules](/manual/api/reference/role/object#role_rules)
-   [User](/manual/api/reference/user/object#user)

[comment]: # ({/new-f3933c89})

[comment]: # ({new-780eeb68})
### Source

CRole::get() in *ui/include/classes/api/services/CRole.php*.

[comment]: # ({/new-780eeb68})
