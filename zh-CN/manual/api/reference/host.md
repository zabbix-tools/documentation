[comment]: # translation:outdated

[comment]: # ({new-6a1da71f})
# 主机

这个类是设计用于处理主机

对象引用:\

-   [Host](/zh/manual/api/reference/host/object#host)
-   [Host
    inventory](/zh/manual/api/reference/host/object#host_inventory)

相关方法:\

-   [host.create](/zh/manual/api/reference/host/create) - 创建新的主机
-   [host.delete](/zh/manual/api/reference/host/delete) - 删除主机
-   [host.get](/zh/manual/api/reference/host/get) - 获取主机信息
-   [host.massadd](/zh/manual/api/reference/host/massadd) -
    给主机添加相关对象
-   [host.massremove](/zh/manual/api/reference/host/massremove) -
    删除主机相关对象
-   [host.massupdate](/zh/manual/api/reference/host/massupdate) -
    替换或移除主机相关对象
-   [host.update](/zh/manual/api/reference/host/update) - 更新主机

[comment]: # ({/new-6a1da71f})
