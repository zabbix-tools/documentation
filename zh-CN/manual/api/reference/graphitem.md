[comment]: # translation:outdated

[comment]: # ({new-ba6b9d7f})
# Graph item 图表监控项

### 图表监控项

This class is designed to work with hosts. 这个类用于配合主机使用。

Object references:\

-   [Graph item](/manual/api/reference/graphitem/object#graph_item)

对象引用:\

-   [图表监控项](/zh/manual/api/reference/graphitem/object#graph_item)

Available methods:\

-   [graphitem.get](/manual/api/reference/graphitem/get) - retrieving
    graph items

可用方法:\

-   [graphitem.get](/zh/manual/api/reference/graphitem/get) -
    获取图表监控项

[comment]: # ({/new-ba6b9d7f})
