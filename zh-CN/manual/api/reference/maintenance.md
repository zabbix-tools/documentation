[comment]: # translation:outdated

[comment]: # ({new-81ae720b})
# Maintenance 维护模式

This class is designed to work with maintenances. 此类设计用于维护模式。

Object references: 对象引用\

-   [Maintenance](/manual/api/reference/maintenance/object#maintenance)
-   [Time period](/manual/api/reference/maintenance/object#time_period)

Available methods: 可用的方法\

-   [maintenance.create](/manual/api/reference/maintenance/create) -
    creating new maintenances 创建新的维护模式
-   [maintenance.delete](/manual/api/reference/maintenance/delete) -
    deleting maintenances 删除维护模式
-   [maintenance.get](/manual/api/reference/maintenance/get) -
    retrieving maintenances 获取维护模式
-   [maintenance.update](/manual/api/reference/maintenance/update) -
    updating maintenances 更新维护模式

[comment]: # ({/new-81ae720b})
