[comment]: # translation:outdated

[comment]: # ({new-62384434})
# Graph prototype 图形原型

### 图形原型

This class is designed to work with graph prototypes.
这个类用于配合图形原型使用。

Object references:\

-   [Graph
    prototype](/manual/api/reference/graphprototype/object#graph_prototype)

对象引用:\

-   [图形原型](/zh/manual/api/reference/graphprototype/object#graph_prototype)

Available methods:\

-   [graphprototype.create](/manual/api/reference/graphprototype/create) -
    creating new graph prototypes
-   [graphprototype.delete](/manual/api/reference/graphprototype/delete) -
    deleting graph prototypes
-   [graphprototype.get](/manual/api/reference/graphprototype/get) -
    retrieving graph prototypes
-   [graphprototype.update](/manual/api/reference/graphprototype/update) -
    updating graph prototypes

可用方法:\

-   [graphprototype.create](/zh/manual/api/reference/graphprototype/create) -
    新建一个图形原型
-   [graphprototype.delete](/zh/manual/api/reference/graphprototype/delete) -
    删除一个图形原型
-   [graphprototype.get](/zh/manual/api/reference/graphprototype/get) -
    获取一个图形原型
-   [graphprototype.update](/zh/manual/api/reference/graphprototype/update) -
    更新一个图形原型

[comment]: # ({/new-62384434})
