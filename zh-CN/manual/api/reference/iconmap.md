[comment]: # translation:outdated

[comment]: # ({new-5ee95992})
# 图标映射

这个类被设计用来处理图标映射

对象引用:\

-   [Icon map](/manual/api/reference/iconmap/object#icon_map)
-   [Icon mapping](/manual/api/reference/iconmap/object#icon_mapping)

可用的方法:\

-   [iconmap.create](/manual/api/reference/iconmap/create) -
    创建新的图标映射
-   [iconmap.delete](/manual/api/reference/iconmap/delete) -
    删除图标映射图
-   [iconmap.get](/manual/api/reference/iconmap/get) - 获取图标映射
-   [iconmap.update](/manual/api/reference/iconmap/update) -
    更新图标映射

[comment]: # ({/new-5ee95992})
