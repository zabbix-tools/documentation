[comment]: # translation:outdated

[comment]: # ({new-66c5a546})
# 批量创建

[comment]: # ({/new-66c5a546})

[comment]: # ({new-55030171})
### 描述

`object host.massadd(object parameters)`

这个方法允许同时向所有给定的主机添加多个相关的对象

[comment]: # ({/new-55030171})

[comment]: # ({new-c25e2be4})
### 参数

`(object)` 参数包含要更新主机的ID和添加到所有主机的对象.

此方法接受如下参数：

|参数         类|描述|<|
|------------------|------|-|
|**hosts**<br>(必选)|对象/数组   要更新<br>|主机.<br>主机必须已定义过`hostid`属性.|
|groups|对象/数组   添加到|定主机的主机组.<br><br>主机组必须已定义过`groupid`属性.|
|interfaces|对象/数组   为指定|机创建主机接口.|
|macros|对象/数组   为指定|机创建用户宏.|
|templates|对象/数组   为指定|机关联模板.<br><br>模板必须已定义过`templateid`属性.|

[comment]: # ({/new-c25e2be4})

[comment]: # ({new-d166b99b})
### 返回值

`(object)` 在`hostids`属性下返回包含已更新主机ID的对象.

[comment]: # ({/new-d166b99b})

[comment]: # ({new-b41637d2})
### 示例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-453a1298})
#### 添加宏

给两个主机添加两个宏

请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.massadd",
    "params": {
        "hosts": [
            {
                "hostid": "10160"
            },
            {
                "hostid": "10167"
            }
        ],
        "macros": [
            {
                "macro": "{$TEST1}",
                "value": "MACROTEST1"
            },
            {
                "macro": "{$TEST2}",
                "value": "MACROTEST2"
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "hostids": [
            "10160",
            "10167"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-453a1298})

[comment]: # ({new-ae4115e5})
### 参考

-   [host.update](update)
-   [Host group](/zh/manual/api/reference/hostgroup/object#host_group)
-   [Template](/zh/manual/api/reference/template/object#template)
-   [User
    macro](/zh/manual/api/reference/usermacro/object#hosttemplate_level_macro)
-   [Host
    interface](/zh/manual/api/reference/hostinterface/object#host_interface)

[comment]: # ({/new-ae4115e5})

[comment]: # ({new-d5e419cc})
### 来源

CHost::massAdd() in
*frontends/php/include/classes/api/services/CHost.php*.

[comment]: # ({/new-d5e419cc})
