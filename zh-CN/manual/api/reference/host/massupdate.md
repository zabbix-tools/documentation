[comment]: # translation:outdated

[comment]: # ({new-4a6b3375})
# 批量更新

[comment]: # ({/new-4a6b3375})

[comment]: # ({new-10cbc072})
### 描述

`object host.massupdate(object parameters)`

此方法允许同时对多个主机替换或移除相关对象和更新属性

[comment]: # ({/new-10cbc072})

[comment]: # ({new-6d664071})
### 参数

`(object)` 参数包含更新主机的ID和需要更新的属性.

另外，对于[标准的主机属性](object#host)，此方法可以接受如下参数:

|参数               类|描述|<|
|------------------------|------|-|
|**hosts**<br>(必选)|对象/数组   要更新<br>|主机<br>机必须已定义过`hostid`属性.|
|groups|对象/数组   替换当|主机所属主机组.<br><br>主机组必须已定义过`groupid`属性.|
|interfaces|对象/数组   在指定|机上替换当前主机接口.|
|inventory|对象        主|资产清单属性.<br><br>使用参数`inventory`无法更新主机资产清单模式，用参数`inventory_mode`替换.|
|inventory\_mode|整数        主|资产清单群体模式.<br><br>参考 [host inventory object page](object#host_inventory) 获取支持的资产清单模式列表.|
|macros|对象/数组   在指定|机中替换当前用户宏.|
|templates|对象/数组   在指定|机中替换当前链接的模板.<br><br>模板必须已定义过`templateid`属性.|
|templates\_clear|对象/数组   移除给|主机的模板关联，并清空与该模板关联的数据.<br><br>模板必须已定义过templateid属性.|

[comment]: # ({/new-6d664071})

[comment]: # ({new-d166b99b})
### 返回值

`(object)` 在`hostids`属性中返回包含已更新主机ID对象.

[comment]: # ({/new-d166b99b})

[comment]: # ({new-b41637d2})
### 示例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-4a8d8b94})
#### 启用多个主机

启用两个主机，将status设置为0

请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.massupdate",
    "params": {
        "hosts": [
            {
                "hostid": "69665"
            },
            {
                "hostid": "69666"
            }
        ],
        "status": 0
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "hostids": [
            "69665",
            "69666"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-4a8d8b94})

[comment]: # ({new-4b09fb40})
### 参考

-   [host.update](update)
-   [host.massadd](massadd)
-   [host.massremove](massremove)
-   [Host group](/manual/api/reference/hostgroup/object#host_group)
-   [Template](/manual/api/reference/template/object#host_group)
-   [User
    macro](/manual/api/reference/usermacro/object#hosttemplate_level_macro)
-   [Host
    interface](/manual/api/reference/hostinterface/object#host_interface)

[comment]: # ({/new-4b09fb40})

[comment]: # ({new-ad74e98b})
### 来源

CHost::massUpdate() in
*frontends/php/include/classes/api/services/CHost.php*.

[comment]: # ({/new-ad74e98b})
