[comment]: # translation:outdated

[comment]: # ({new-c572a02a})
# 更新

[comment]: # ({/new-c572a02a})

[comment]: # ({new-12e31652})
### 描述

`object host.update(object/array hosts)`

该方法用来更新已存在的主机

[comment]: # ({/new-12e31652})

[comment]: # ({new-3a8eac6a})
### 参数

`(object/array)` 要更新的主机属性.

每个主机的`hostid`属性必须已定义过，其他属性为可选项.只会更新指定的属性，其他属性保持不变.另外，对于[标准主机属性](object#host)，此方法接受如下参数:

|参数               类|描述|<|
|------------------------|------|-|
|groups|对象/数组   替换主|当前归属的组.<br><br>主机组的`groupid`必须已定义.|
|interfaces|对象/数组   替换当|主机接口.|
|inventory|对象        主|资产清单属性.|
|macros|对象/数组   替换当|用户宏.|
|templates|对象/数组   替换当|链接的模板. 模板没有传递仅删除链接.<br><br>模板必须已定义过`templateid`属性|
|templates\_clear|对象/数组   从主机|删除模板链接并清除.<br><br>模板必须已定义过`templateid`属性.|

<note
tip>相对于Zabbix前端，当`name`和`host`一致，更新`host`的时候不会自动更新`name`.两个属性需要明确的更新.
这两个属性都需要显式地更新.
:::

[comment]: # ({/new-3a8eac6a})

[comment]: # ({new-d166b99b})
### 返回值

`(object)` 在`hostids`属性中返回包含已更新主机ID对象.

[comment]: # ({/new-d166b99b})

[comment]: # ({new-b41637d2})
### 示例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-ce50c453})
#### 启用主机

启用主机，将status设置为0

请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.update",
    "params": {
        "hostid": "10126",
        "status": 0
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "hostids": [
            "10126"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-ce50c453})

[comment]: # ({new-bea1db7e})
#### 删除模板链接

从主机中删除链接并清除两个模板.

请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.update",
    "params": {
        "hostid": "10126",
        "templates_clear": [
            {
                "templateid": "10124"
            },
            {
                "templateid": "10125"
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "hostids": [
            "10126"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-bea1db7e})

[comment]: # ({new-98dcfd68})
#### 更新主机宏

用两个新的宏替换主机所有的宏.

请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.update",
    "params": {
        "hostid": "10126",
        "macros": [
            {
                "macro": "{$PASS}",
                "value": "password"
            },
            {
                "macro": "{$DISC}",
                "value": "sda"
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "hostids": [
            "10126"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-98dcfd68})

[comment]: # ({new-da99f3b8})
#### 更新主机资产清单

更改资产清单模式并添加地点

请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.update",
    "params": {
        "hostid": "10387",
        "inventory_mode": 0,
        "inventory": {
            "location": "Latvia, Riga"
        }
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "hostids": [
            "10387"
        ]
    },
    "id": 2
}
```

[comment]: # ({/new-da99f3b8})

[comment]: # ({new-5cc5d950})
### 参考

-   [host.massadd](massadd)
-   [host.massupdate](massupdate)
-   [host.massremove](massremove)
-   [Host group](/manual/api/reference/hostgroup/object#host_group)
-   [Template](/manual/api/reference/template/object#template)
-   [User
    macro](/manual/api/reference/usermacro/object#hosttemplate_level_macro)
-   [Host
    interface](/manual/api/reference/hostinterface/object#host_interface)
-   [Host inventory](object#host_inventory)

[comment]: # ({/new-5cc5d950})

[comment]: # ({new-ae2d89bf})
#### Updating discovered host macros

Convert discovery rule created "automatic" macro to "manual" and change its value to "new-value".

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.update",
    "params": {
        "hostid": "10387",
        "macros": {
            "hostmacroid": "5541",
            "value": "new-value",
            "automatic": "0"
        }
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "hostids": [
            "10387"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-ae2d89bf})

[comment]: # ({new-f620e859})
#### Updating host encryption

Update the host "10590" to use PSK encryption only for connections from host to Zabbix server, and change the PSK identity and PSK key.
Note that the host has to be [pre-configured to use PSK](/manual/encryption/using_pre_shared_keys#configuring-psk-for-server-agent-communication-example).

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "host.update",
    "params": {
        "hostid": "10590",
        "tls_connect": 1,
        "tls_accept": 2,
        "tls_psk_identity": "PSK 002",
        "tls_psk": "e560cb0d918d26d31b4f642181f5f570ad89a390931102e5391d08327ba434e9"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "hostids": [
            "10590"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-f620e859})

[comment]: # ({new-23501347})
### 来源

CHost::update() in
*frontends/php/include/classes/api/services/CHost.php*.

[comment]: # ({/new-23501347})


[comment]: # ({new-a39b5a01})
### Source

CHost::update() in *ui/include/classes/api/services/CHost.php*.

[comment]: # ({/new-a39b5a01})
