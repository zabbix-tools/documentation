[comment]: # translation:outdated

[comment]: # ({new-2a0a5084})
# 批量删除

[comment]: # ({/new-2a0a5084})

[comment]: # ({new-9c943195})
### 描述

`object host.massremove(object parameters)`

这个方法允许同时向所有给定的主机移除相关的对象

[comment]: # ({/new-9c943195})

[comment]: # ({new-689d39a3})
### 参数

`(object)` 参数包含要更主机的ID和应该移除的对象.

|参数                 类|描述|<|
|--------------------------|------|-|
|**hostids**<br>(必选)|字符串/数组   要更新的|机的ID.|
|groupids|字符串/数组   移除给定|机的主机组.|
|interfaces|对象/数组     移除给|主机的主机接口.<br><br>主机接口对象必须已定义`ip`, `dns` and `port`属性.|
|macros|字符串/数组   移除给定|机的用户宏.|
|templateids|字符串/数组   移除给定|机的模板关联.|
|templateids\_clear|字符串/数组   移除给定|机的模板关联，并清空与该模板关联的数据.|

[comment]: # ({/new-689d39a3})

[comment]: # ({new-d166b99b})
### 返回值

`(object)` 在`hostids`属性中返回包含已更新主机ID对象.

[comment]: # ({/new-d166b99b})

[comment]: # ({new-b41637d2})
### 示例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-8c635a7b})
#### 删除模板链接

从两个主机中删除一个模板链接并且删除所有模板实体

请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.massremove",
    "params": {
        "hostids": ["69665", "69666"],
        "templateids_clear": "325"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "hostids": [
            "69665",
            "69666"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-8c635a7b})

[comment]: # ({new-abd9a8d3})
### 参考

-   [host.update](update)
-   [User
    macro](/manual/api/reference/usermacro/object#hosttemplate_level_macro)
-   [Host
    interface](/manual/api/reference/hostinterface/object#host_interface)

[comment]: # ({/new-abd9a8d3})

[comment]: # ({new-bda21fe6})
### 来源

CHost::massRemove() in
*frontends/php/include/classes/api/services/CHost.php*.

[comment]: # ({/new-bda21fe6})
