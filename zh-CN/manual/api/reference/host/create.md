[comment]: # translation:outdated

[comment]: # ({new-ce3b3e73})
# 创建

[comment]: # ({/new-ce3b3e73})

[comment]: # ({new-2e20b613})
### 描述

`对象 host.create(对象/数组 hosts)`

这个方法可以用来创建主机.

[comment]: # ({/new-2e20b613})

[comment]: # ({new-48df9d2f})
### 参数

`(对象/数组)` 要创建的主机.

另外，对于[标准的主机属性](object#host)，该方法接受下列参数

|属性              类|描述|<|
|-----------------------|------|-|
|**groups**<br>(必选)|对象/数组   添加主<br>|的主机组.<br>主机组必须已定义`groupid`属性.|
|**interfaces**<br>(必选)|对象/数组   为主机|建的接口.|
|templates|对象/数组   链接到|机的模板.<br><br>模板必须已定义过`templateid`属性.|
|macros|对象/数组   为主机|建的用户宏.|
|inventory|对象        主|资产清单属性.|

[comment]: # ({/new-48df9d2f})

[comment]: # ({new-de799b9d})
### 返回值

`(对象)` 返回包含已创建主机ID的属性hostid
,返回ID的顺序与传入主机的顺序一致

[comment]: # ({/new-de799b9d})

[comment]: # ({new-b41637d2})
### 示例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-26320041})
#### 创建主机

Create a host called "Linux server" with an IP interface, add it to a
group, link a template to it and set the MAC addresses in the host
inventory.

创建一个具有IP接口的“Linux
Server”主机，将其添加到主机组中，链接一个模板并且把MAC地址设置到主机资产清单里
请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.create",
    "params": {
        "host": "Linux server",
        "interfaces": [
            {
                "type": 1,
                "main": 1,
                "useip": 1,
                "ip": "192.168.3.1",
                "dns": "",
                "port": "10050"
            }
        ],
        "groups": [
            {
                "groupid": "50"
            }
        ],
        "templates": [
            {
                "templateid": "20045"
            }
        ],
        "inventory_mode": 0,
        "inventory": {
            "macaddress_a": "01234",
            "macaddress_b": "56768"
        }
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "hostids": [
            "107819"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-26320041})

[comment]: # ({new-9572ca08})
### 参考

-   [Host group](/zh/manual/api/reference/hostgroup/object#host_group)
-   [Template](/zh/manual/api/reference/template/object#template)
-   [User
    macro](/zh/manual/api/reference/usermacro/object#hosttemplate_level_macro)
-   [Host
    interface](/zh/manual/api/reference/hostinterface/object#host_interface)
-   [Host inventory](object#host_inventory)

[comment]: # ({/new-9572ca08})

[comment]: # ({new-4c198338})
#### Creating a host with PSK encryption

Create a host called "PSK host" with PSK encryption configured.
Note that the host has to be [pre-configured to use PSK](/manual/encryption/using_pre_shared_keys#configuring-psk-for-server-agent-communication-example).

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "host.create",
    "params": {
        "host": "PSK host",
        "interfaces": [
            {
                "type": 1,
                "ip": "192.168.3.1",
                "dns": "",
                "port": "10050",
                "useip": 1,
                "main": 1
            }
        ],
        "groups": [
            {
                "groupid": "2"
            }
        ],
        "tls_accept": 2,
        "tls_connect": 2,
        "tls_psk_identity": "PSK 001",
        "tls_psk": "1f87b595725ac58dd977beef14b97461a7c1045b9a1c963065002c5473194952"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "hostids": [
            "10590"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-4c198338})

[comment]: # ({new-203ca033})
### 来源

CHost::create() in
*frontends/php/include/classes/api/services/CHost.php*.

[comment]: # ({/new-203ca033})


[comment]: # ({new-f7e47995})
### Source

CHost::create() in *ui/include/classes/api/services/CHost.php*.

[comment]: # ({/new-f7e47995})
