[comment]: # translation:outdated

[comment]: # ({new-65bbffce})
# 获取

[comment]: # ({/new-65bbffce})

[comment]: # ({new-93035b19})
### 描述

`integer/array host.get(object parameters)`

此方法允许根据指定的参数获取主机.

[comment]: # ({/new-93035b19})

[comment]: # ({new-77a8fabb})
### 参数

`(object)` 定义期望输出的参数.

该方法支持以下参数.

|参数                         类|描述|<|
|----------------------------------|------|-|
|groupids|字符串/数组   返回指定|机组的主机.|
|applicationids|字符串/数组   返回指定|用集的主机.|
|dserviceids|字符串/数组   返回与指|自动发现服务相关的主机.|
|graphids|字符串/数组   返回包含|指定图表的主机.|
|hostids|字符串/数组   返回指定|机ID的主机.|
|httptestids|字符串/数组   返回指定|页监测的主机.|
|interfaceids|字符串/数组   返回指定|口的主机.|
|itemids|字符串/数组   返回指定|控项的主机.|
|maintenanceids|字符串/数组   返回指定|护的主机.|
|monitored\_hosts|标识          返|被监控的主机.|
|proxy\_hosts|标识          返|代理服务器.|
|proxyids|字符串/数组   返回被代|服务器监控的主机.|
|templated\_hosts|标识          返|主机和模板.|
|templateids|字符串/数组   返回使用|定模板的主机.|
|triggerids|字符串/数组   返回指定|发器的主机.|
|with\_items|标识          返|含有监控项的主机.<br><br>覆盖`with_monitored_items` 和 `with_simple_graph_items` 参数.|
|with\_applications|标识          返|含有应用集的主机.|
|with\_graphs|标识          返|含有图表的主机.|
|with\_httptests|标识          返|含有web监测的主机.<br><br>覆盖`with_monitored_httptests` 参数.|
|with\_monitored\_httptests|标识          返|含有启动网页监测的主机.|
|with\_monitored\_items|标识          返|启用监控项的主机.<br><br>覆盖 `with_simple_graph_items` 参数.|
|with\_monitored\_triggers|标识          返|启用触发器的主机.所有在触发器中使用到的监控项必须也要启用.|
|with\_simple\_graph\_items|标识          返|含有数字类信息监控项的主机.|
|with\_triggers|标识          返|含有触发器的主机.<br><br>覆盖 `with_monitored_triggers` 参数.|
|withInventory|标识          返|含有资产清单数据的主机.|
|selectGroups|查询          在|groups`属性中返回主机所属的主机组.|
|selectApplications|查询          在|applications`属性中返回来自主机的应用集.<br><br>支持`count`.|
|selectDiscoveries|查询          在|discoveries`属性中返回来自主机的底层自动发现.<br><br>支持`count`.|
|selectDiscoveryRule|查询          在|discoveryRule`属性中返回创建主机的底层自动发现规则.|
|selectGraphs|查询          在|graphs`属性中返回来自主机的图表.<br><br>支持`count`.|
|selectHostDiscovery|查询          在|hostDiscovery`属性中返回主机自动发现对象.<br><br>主机自动发现对象将一个自动发现的主机和一个原型主机连接起来，或者把一个原型主机和一个底层自动发现规则连接起来，并且含有以下属性：<br>`host` - *(字符串)* 主机原型的主机;<br>`hostid` - *(字符串)* 主机原型和自动发现主机的ID;<br>`parent_hostid` - *(字符串)* 已经创建主机的主机原型ID;<br>`parent_itemid` - *(字符串)* 创建自动发现主机的底层自动发现规则ID;<br>`lastcheck` - *(时间戳)* 最近一次发现主机的时间;<br>`ts_delete` - *(时间戳)* 当不再自动发现的主机将被删除的时间.|
|selectHttpTests|查询          在|httpTests`属性中返回主机的web场景.<br><br>支持 `count`.|
|selectInterfaces|查询          在|interfaces`属性中返回主机的接口.<br><br>支持 `count`.|
|selectInventory|查询          在|inventory`属性中返回主机清单.|
|selectItems|查询          在|item`属性中返回主机监控项.<br><br>支持 `count`.|
|selectMacros|查询          在|macros`属性中返回主机宏.|
|selectParentTemplates|查询          在|parentTemplates`属性中返回主机连接的模板.<br><br>支持 `count`.|
|selectScreens|查询          在|screens`属性中返回主机的屏幕.<br><br>支持`count`.|
|selectTriggers|查询          在|triggers`属性中返回主机的触发器.<br><br>支持 `count`.|
|filter|对象          仅|回完全匹配指定筛选后的结果.<br><br>接受数组，键为属性名，值为一个单一值或者一个要匹配的数组.<br><br>允许通过接口属性进行过滤.|
|limitSelects|整数          限|由子查询返回的记录数量.<br><br>适用于以下子查询:<br>`selectParentTemplates` - 结果将按照`host`排序;<br>`selectInterfaces`;<br>`selectItems` - 按`name`排序;<br>`selectDiscoveries` - 按`name`排序;<br>`selectTriggers` - 按`description`排序;<br>`selectGraphs` - 按`name`排序;<br>`selectApplications` - 按`name`排序;<br>`selectScreens` - 按`name`排序.|
|search|对象          返|与通配符相匹配的结果.<br><br>接受数组，键为属性名，值为待匹配搜索的字符串 . 如果没有指定的额外选项，将会以LIKE “%…%”方式执行搜索.<br><br>允许通过接口属性搜索，仅对文本域产生影响.|
|searchInventory|对象          仅|回与指定通配符搜索资产清单数据匹配的主机.<br><br>这个参数同时受`search`参数影响.|
|sortfield|字符串/数组   结果按给|的属性进行排序.<br><br>可能值: `hostid`, `host`, `name`, `status`.|
|countOutput|布尔值        以下|是与[reference commentary](/zh/manual/api/reference_commentary#common_get_method_parameters)中详细描述的`get`方法相同的参数.|
|editable|布尔值        ::|<|
|excludeSearch|布尔值        ::|<|
|limit|整数          :|:|
|output|查询          :|:|
|preservekeys|布尔值        ::|<|
|searchByAny|布尔值        ::|<|
|searchWildcardsEnabled|布尔值        ::|<|
|sortorder|字符串/数组   :::|<|
|startSearch|布尔值        ::|<|

[comment]: # ({/new-77a8fabb})

[comment]: # ({new-7223bab1})
### 返回值

`(integer/array)` 返回其中之一:

-   一组对象;
-   如果使用了`countOutput`参数，则返回获取的对象数量.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### 示例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-c6ea3cce})
#### 通过名称获取数据

获取所有关于"Zabbix server"和"Linux server"两个主机的数据 请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.get",
    "params": {
        "filter": {
            "host": [
                "Zabbix server",
                "Linux server"
            ]
        }
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "maintenances": [],
            "hostid": "10160",
            "proxy_hostid": "0",
            "host": "Zabbix server",
            "status": "0",
            "disable_until": "0",
            "error": "",
            "available": "0",
            "errors_from": "0",
            "lastaccess": "0",
            "ipmi_authtype": "-1",
            "ipmi_privilege": "2",
            "ipmi_username": "",
            "ipmi_password": "",
            "ipmi_disable_until": "0",
            "ipmi_available": "0",
            "snmp_disable_until": "0",
            "snmp_available": "0",
            "maintenanceid": "0",
            "maintenance_status": "0",
            "maintenance_type": "0",
            "maintenance_from": "0",
            "ipmi_errors_from": "0",
            "snmp_errors_from": "0",
            "ipmi_error": "",
            "snmp_error": "",
            "jmx_disable_until": "0",
            "jmx_available": "0",
            "jmx_errors_from": "0",
            "jmx_error": "",
            "name": "Zabbix server",
            "description": "The Zabbix monitoring server.",
            "tls_connect": "1",
            "tls_accept": "1",
            "tls_issuer": "",
            "tls_subject": "",
            "tls_psk_identity": "",
            "tls_psk": ""
        },
        {
            "maintenances": [],
            "hostid": "10167",
            "proxy_hostid": "0",
            "host": "Linux server",
            "status": "0",
            "disable_until": "0",
            "error": "",
            "available": "0",
            "errors_from": "0",
            "lastaccess": "0",
            "ipmi_authtype": "-1",
            "ipmi_privilege": "2",
            "ipmi_username": "",
            "ipmi_password": "",
            "ipmi_disable_until": "0",
            "ipmi_available": "0",
            "snmp_disable_until": "0",
            "snmp_available": "0",
            "maintenanceid": "0",
            "maintenance_status": "0",
            "maintenance_type": "0",
            "maintenance_from": "0",
            "ipmi_errors_from": "0",
            "snmp_errors_from": "0",
            "ipmi_error": "",
            "snmp_error": "",
            "jmx_disable_until": "0",
            "jmx_available": "0",
            "jmx_errors_from": "0",
            "jmx_error": "",
            "name": "Linux server",
            "description": "",
            "tls_connect": "1",
            "tls_accept": "1",
            "tls_issuer": "",
            "tls_subject": "",
            "tls_psk_identity": "",
            "tls_psk": ""
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-c6ea3cce})

[comment]: # ({new-9078aff7})
#### 获取主机组

获取主机"Zabbix server"所属的主机组，并不检索主机本身的详细信息

请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.get",
    "params": {
        "output": ["hostid"],
        "selectGroups": "extend",
        "filter": {
            "host": [
                "Zabbix server"
            ]
        }
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 2
}
```

响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "hostid": "10085",
            "groups": [
                {
                    "groupid": "2",
                    "name": "Linux servers",
                    "internal": "0",
                    "flags": "0"
                },
                {
                    "groupid": "4",
                    "name": "Zabbix servers",
                    "internal": "0",
                    "flags": "0"
                }
            ]
        }
    ],
    "id": 2
}
```

[comment]: # ({/new-9078aff7})

[comment]: # ({new-59c8a46a})
#### 获取关联的模板

获取主机"10084"关联的模板的ID和名称 请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.get",
    "params": {
        "output": ["hostid"],
        "selectParentTemplates": [
            "templateid",
            "name"
        ],
        "hostids": "10084"
    },
    "id": 1,
    "auth": "70785d2b494a7302309b48afcdb3a401"
}
```

响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "hostid": "10084",
            "parentTemplates": [
                {
                    "name": "Template OS Linux",
                    "templateid": "10001"
                },
                {
                    "name": "Template App Zabbix Server",
                    "templateid": "10047"
                }
            ]
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-59c8a46a})

[comment]: # ({new-8be1d99c})
#### 根据主机资产清单数据进行检索

获取主机清单中"OS"字段包含"Linux"的主机 请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.get",
    "params": {
        "output": [
            "host"
        ],
        "selectInventory": [
            "os"
        ],
        "searchInventory": {
            "os": "Linux"
        }
    },
    "id": 2,
    "auth": "7f9e00124c75e8f25facd5c093f3e9a0"
}
```

响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "hostid": "10084",
            "host": "Zabbix server",
            "inventory": {
                "os": "Linux Ubuntu"
            }
        },
        {
            "hostid": "10107",
            "host": "Linux server",
            "inventory": {
                "os": "Linux Mint"
            }
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-8be1d99c})

[comment]: # ({new-b4e37e5e})
### 参考

-   [Host group](/zh/manual/api/reference/hostgroup/object#host_group)
-   [Template](/zh/manual/api/reference/template/object#template)
-   [User
    macro](/zh/manual/api/reference/usermacro/object#hosttemplate_level_macro)
-   [Host
    interface](/zh/manual/api/reference/hostinterface/object#host_interface)

[comment]: # ({/new-b4e37e5e})

[comment]: # ({new-9ebae84e})
### 来源

CHost::get() in *frontends/php/include/classes/api/services/CHost.php*.

[comment]: # ({/new-9ebae84e})




[comment]: # ({new-219d4ee1})
#### Searching hosts by problem severity

Retrieve hosts that have "Disaster" problems.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.get",
    "params": {
        "output": ["name"],
        "severities": 5
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "hostid": "10160",
            "name": "Zabbix server"
        }
    ],
    "id": 1
}
```

Retrieve hosts that have "Average" and "High" problems.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.get",
    "params": {
        "output": ["name"],
        "severities": [3, 4]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "hostid": "20170",
            "name": "Database"
        },
        {
            "hostid": "20183",
            "name": "workstation"
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-219d4ee1})

[comment]: # ({new-06c7fe93})
### See also

-   [Host group](/manual/api/reference/hostgroup/object#host_group)
-   [Template](/manual/api/reference/template/object#template)
-   [User
    macro](/manual/api/reference/usermacro/object#hosttemplate_level_macro)
-   [Host
    interface](/manual/api/reference/hostinterface/object#host_interface)

[comment]: # ({/new-06c7fe93})

[comment]: # ({new-55c08f7a})
### Source

CHost::get() in *ui/include/classes/api/services/CHost.php*.

[comment]: # ({/new-55c08f7a})
