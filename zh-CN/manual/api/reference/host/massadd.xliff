<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="zh-CN" datatype="plaintext" original="manual/api/reference/host/massadd.md">
    <body>
      <trans-unit id="66c5a546" xml:space="preserve">
        <source># host.massadd</source>
      </trans-unit>
      <trans-unit id="55030171" xml:space="preserve">
        <source>### Description

`object host.massadd(object parameters)`

This method allows to simultaneously add multiple related objects to all
the given hosts.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::</source>
      </trans-unit>
      <trans-unit id="c25e2be4" xml:space="preserve">
        <source>### Parameters

`(object)` Parameters containing the IDs of the hosts to update and the
objects to add to all the hosts.

The method accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|hosts|object/array|Hosts to be updated.&lt;br&gt;&lt;br&gt;The hosts must have the `hostid` property defined.&lt;br&gt;&lt;br&gt;[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):&lt;br&gt;- *required*|
|groups|object/array|Host groups to add to the given hosts.&lt;br&gt;&lt;br&gt;The host groups must have the `groupid` property defined.|
|interfaces|object/array|[Host interfaces](/manual/api/reference/hostinterface/object) to be created for the given hosts.|
|macros|object/array|[User macros](/manual/api/reference/usermacro/object) to be created for the given hosts.|
|templates|object/array|Templates to link to the given hosts.&lt;br&gt;&lt;br&gt;The templates must have the `templateid` property defined.|</source>
      </trans-unit>
      <trans-unit id="d166b99b" xml:space="preserve">
        <source>### Return values

`(object)` Returns an object containing the IDs of the updated hosts
under the `hostids` property.</source>
      </trans-unit>
      <trans-unit id="b41637d2" xml:space="preserve">
        <source>### Examples</source>
      </trans-unit>
      <trans-unit id="453a1298" xml:space="preserve">
        <source>#### Adding macros

Add two new macros to two hosts.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "host.massadd",
    "params": {
        "hosts": [
            {
                "hostid": "10160"
            },
            {
                "hostid": "10167"
            }
        ],
        "macros": [
            {
                "macro": "{$TEST1}",
                "value": "MACROTEST1"
            },
            {
                "macro": "{$TEST2}",
                "value": "MACROTEST2",
                "description": "Test description"
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "hostids": [
            "10160",
            "10167"
        ]
    },
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="ae4115e5" xml:space="preserve">
        <source>### See also

-   [host.update](update)
-   [Host group](/manual/api/reference/hostgroup/object#host_group)
-   [Template](/manual/api/reference/template/object#template)
-   [User
    macro](/manual/api/reference/usermacro/object#hosttemplate_level_macro)
-   [Host
    interface](/manual/api/reference/hostinterface/object#host_interface)</source>
      </trans-unit>
      <trans-unit id="d5e419cc" xml:space="preserve">
        <source>### Source

CHost::massAdd() in *ui/include/classes/api/services/CHost.php*.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
