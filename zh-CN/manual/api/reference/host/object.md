[comment]: # translation:outdated

[comment]: # ({new-b7ea8d04})
# > 主机对象

下列是与主机相关的对象

[comment]: # ({/new-b7ea8d04})

[comment]: # ({new-2f6c5c76})
### 主机

主机对象具有以下属性:

|属性                   类|描述|<|
|----------------------------|------|-|
|hostid|字符串   *(|读)* 主机的ID.|
|**host**<br>(必选)|字符串   主机|正式名称.|
|available|整数     *|只读）*Zabbix agent的可用性.<br><br>可能的值为:<br>0 - 默认）未知<br>1 - 可用;<br>2 - 不可用.|
|description|文本     主|说明.|
|disable\_until|时间戳   *(|读)* 不可用状态Agent的下一次轮训时间.|
|error|字符串   *(|读)* Agent不可用时的错误信息|
|errors\_from|时间戳   *(|读)* Agent不可用时的时间.|
|flags|整数     *|只读)* 主机的来源.<br><br>可能的值:<br>0 - 普通主机;<br>4 - 自动发现的主机.|
|inventory\_mode|整数     主|资产清单模式.<br><br>可能的值:<br>-1 - 禁用;<br>0 - *(默认)* 手动;<br>1 - 自动.|
|ipmi\_authtype|整数     I|MI 认证算法.<br><br>可能的值:<br>-1 - *(默认)* 默认;<br>0 - 无;<br>1 - MD2;<br>2 - MD5<br>4 - straight;<br>5 - OEM;<br>6 - RMCP+.|
|ipmi\_available|整数     *|只读)* IPMI agent的可用性.<br><br>可能的值:<br>0 - *(默认)* 未知;<br>1 - 可用;<br>2 - 不可用.|
|ipmi\_disable\_until|时间戳   *(|读)* 不可用状态IPMI agent的下一次轮训时间.|
|ipmi\_error|字符串   *(|读)* IPMI agent不可用时的错误信息.|
|ipmi\_errors\_from|时间戳   *(|读)* IPMI agent不可用时的时间.|
|ipmi\_password|字符串   IP|I 密码.|
|ipmi\_privilege|整数     I|MI 权限等级.<br><br>可能的值:<br>1 - 回调;<br>2 - *(默认)* 用户;<br>3 - 操作员;<br>4 - 管理员;<br>5 - OEM原厂.|
|ipmi\_username|整数     I|MI 用户名.|
|jmx\_available|整数     *|只读)* JMX agent的可用性.<br><br>可能的值:<br>0 - *(默认)* 未知;<br>1 - 可用;<br>2 - 不可用.|
|jmx\_disable\_until|时间戳   *(|读)* 不可用状态JMX agent的下一次轮训时间.|
|jmx\_error|字符串   *(|读)* JMX agent不可用时的错误信息.|
|jmx\_errors\_from|时间戳   *(|读)* JMX agent不可用时的时间.|
|maintenance\_from|时间戳   *(|读)* 有效维护的开始时间.|
|maintenance\_status|整数     *|只读)* 有效维护的状态.<br><br>可能的值:<br>0 - *(默认)* 没有维护;<br>1 - 有效维护.|
|maintenance\_type|整数     *|只读)* 有效维护类型.<br><br>可能的值:<br>0 - *(默认)* 维护期间搜集数据;<br>1 - 维护期间不搜集数据.|
|maintenanceid|字符串   *(|读)* 目前对主机生效的维护的ID.|
|name|字符串   主机|.<br><br>默认: `host` 属性值.|
|proxy\_hostid|字符串   ID|of the proxy that is used to monitor the host. 用于监控主机的Proxy服务器的hostid|
|snmp\_available|整数     *|只读)* SNMP agent的可用性.<br><br>可能的值:<br>0 - *(默认)* 未知;<br>1 - 可用;<br>2 - 不可用.|
|snmp\_disable\_until|时间戳   *(|读)* 不可用状态SNMP agent的下一次轮训时间.|
|snmp\_error|字符串   *(|读)* SNMP agent不可用时的错误信息.|
|snmp\_errors\_from|时间戳   *(|读)* SNMP agent不可用时的时间.|
|status|整数     主|的状态.<br><br>可能的值:<br>0 - *(默认)* 已监控的主机;<br>1 - 未监控的主机.|
|tls\_connect|整数     到|机的连接. //<br><br>可能的值:<br>1 - *(默认)* 没有加密;<br>2 - PSK;<br>4 - 证书.|
|tls\_accept|整数     来|主机的连接. //<br><br>可能的值:<br>1 - *(默认)* 没有加密;<br>2 - PSK;<br>4 - 证书.|
|tls\_issuer|字符串   证书|行机构.|
|tls\_subject|字符串   证书|主题.|
|tls\_psk\_identity|字符串   PS|认证. 如果`tls_connect` 或 `tls_accept`启用了PSK,那么该选项是必选.|
|tls\_psk|字符串   PS|至少需要32位16进制数字构成.如果`tls_connect` 或 `tls_accept`启用了PSK,那么该选项是必选.|

[comment]: # ({/new-2f6c5c76})

[comment]: # ({new-859da78d})
### 主机资产清单

主机资产对象具有以下属性:

<note
tip>每一个属性拥有自己唯一的ID编号，用于将主机资产清单字段和事项关联在一起.
:::

|ID|属性                 类|描述|<|
|--|--------------------------|------|-|
|4|alias|字符串   别名|<|
|11|asset\_tag|字符串   资产|签.|
|28|chassis|字符串   机架|<|
|23|contact|字符串   联系|.|
|32|contract\_number|字符串   联系|码.|
|47|date\_hw\_decomm|字符串   硬件|汰时间.|
|46|date\_hw\_expiry|字符串   硬件|保过期时间.|
|45|date\_hw\_install|字符串   硬件|装时间.|
|44|date\_hw\_purchase|字符串   硬件|买时间.|
|34|deployment\_status|字符串   部署|态.|
|14|hardware|字符串   硬件|备.|
|15|hardware\_full|字符串   硬件|备详情.|
|39|host\_netmask|字符串   主机|网掩码.|
|38|host\_networks|字符串   主机|络.|
|40|host\_router|字符串   主机|由.|
|30|hw\_arch|字符串   硬件|构.|
|33|installer\_name|字符串   安装|员姓名.|
|24|location|字符串   地点|<|
|25|location\_lat|字符串   纬度|置.|
|26|location\_lon|字符串   经度|置.|
|12|macaddress\_a|字符串   MA|地址一.|
|13|macaddress\_b|字符串   MA|地址二.|
|29|model|字符串   型号|<|
|3|name|字符串   名称|<|
|27|notes|字符串   注释|<|
|41|oob\_ip|字符串   带外|P地址.|
|42|oob\_netmask|字符串   带外|机子网掩码.|
|43|oob\_router|字符串   带外|由.|
|5|os|字符串   操作|统名称.|
|6|os\_full|字符串   详尽|作系统名称.|
|7|os\_short|字符串   简短|作系统名称.|
|61|poc\_1\_cell|字符串   主P|C移动号码.|
|58|poc\_1\_email|字符串   主P|C邮箱.|
|57|poc\_1\_name|字符串   主P|C名称.|
|63|poc\_1\_notes|字符串   主P|C注释.|
|59|poc\_1\_phone\_a|字符串   主P|C电话一.|
|60|poc\_1\_phone\_b|字符串   主P|C电话二.|
|62|poc\_1\_screen|字符串   主P|C显示名.|
|68|poc\_2\_cell|字符串   次P|C移动号码.|
|65|poc\_2\_email|字符串   次P|C邮箱.|
|64|poc\_2\_name|字符串   次P|C名称.|
|70|poc\_2\_notes|字符串   次P|C注释.|
|66|poc\_2\_phone\_a|字符串   次P|C电话一.|
|67|poc\_2\_phone\_b|字符串   次P|C电话二.|
|69|poc\_2\_screen|字符串   次P|C显示名.|
|8|serialno\_a|字符串   序列|一.|
|9|serialno\_b|字符串   序列|二.|
|48|site\_address\_a|字符串   所在|址一.|
|49|site\_address\_b|字符串   所在|址二.|
|50|site\_address\_c|字符串   所在|址三.|
|51|site\_city|字符串   所在|市.|
|53|site\_country|字符串   所在|家.|
|56|site\_notes|字符串   所在|注解.|
|55|site\_rack|字符串   所在|机柜位置.|
|52|site\_state|字符串   所在|说明.|
|54|site\_zip|字符串   所在|邮政编码.|
|16|software|字符串   软件|<|
|18|software\_app\_a|字符串   应用|件一.|
|19|software\_app\_b|字符串   应用|件二.|
|20|software\_app\_c|字符串   应用|件三.|
|21|software\_app\_d|字符串   应用|件四.|
|22|software\_app\_e|字符串   应用|件五.|
|17|software\_full|字符串   软件|情.|
|10|tag|字符串   标签|<|
|1|type|字符串   类型|<|
|2|type\_full|字符串   具体|型.|
|35|url\_a|字符串   网址|.|
|36|url\_b|字符串   网址|.|
|37|url\_c|字符串   网址|.|
|31|vendor|字符串   供应|.|

[comment]: # ({/new-859da78d})


[comment]: # ({new-0ae1807e})
### Host tag

The host tag object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**tag**<br>(required)|string|Host tag name.|
|value|string|Host tag value.|

[comment]: # ({/new-0ae1807e})
