[comment]: # translation:outdated

[comment]: # ({new-dd2b9519})
# 删除

[comment]: # ({/new-dd2b9519})

[comment]: # ({new-8f1cd3fa})
### 描述

`object host.delete(array hosts)`

该方法允许删除主机

[comment]: # ({/new-8f1cd3fa})

[comment]: # ({new-cede3d95})
### 参数

`(array)` 要删除的主机的ID.

[comment]: # ({/new-cede3d95})

[comment]: # ({new-54b4a855})
### 返回值

`(object)` 返回值包含已删除主机ID的`hostid`属性.

[comment]: # ({/new-54b4a855})

[comment]: # ({new-b41637d2})
### 示例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-ef767d02})
#### 删除多个主机

删除两个主机.

请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.delete",
    "params": [
        "13",
        "32"
    ],
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "hostids": [
            "13",
            "32"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-ef767d02})

[comment]: # ({new-9d5f3989})
### 来源

CHost::delete() in
*frontends/php/include/classes/api/services/CHost.php*.

[comment]: # ({/new-9d5f3989})
