[comment]: # translation:outdated

[comment]: # ({new-01e2c406})
# > 对象

下列对象与 `dhost` API 直接相关。

[comment]: # ({/new-01e2c406})

[comment]: # ({new-507852d3})
### 发现服务

::: noteclassic
发现的服务是由 Zabbix 服务器创建的，不能通过 API
进行修改。
:::

被发现的服务对象包含由一个主机上的网络发现规则发现的服务的信息。其具有以下属性。

|属性         类|描述|<|
|------------------|------|-|
|dserviceid|字符串   发现|服务的 ID。|
|dcheckid|字符串   用于|测服务的发现规则的 ID。|
|dhostid|字符串   运行|服务的已发现的主机的 ID。|
|dns|字符串   运行|服务的主机的 DNS。|
|ip|字符串   运行|服务的主机的 IP 地址。|
|lastdown|时间戳   发现|服务最后异常的时间。|
|lastup|时间戳   发现|服务最后正常的时间。|
|port|整数     服|端口号。|
|status|整数     服|的状态。<br><br>可能的值：<br>0 - 服务正常；<br>1 - 服务异常。|
|value|字符串   当执|Zabbix 客户端、SNMPv1、SNMPv2 或 SNMPv3 等发现检查时，服务返回的值。|

[comment]: # ({/new-507852d3})
