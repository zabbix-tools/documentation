[comment]: # translation:outdated

[comment]: # ({new-30b4a228})
# 获取

[comment]: # ({/new-30b4a228})

[comment]: # ({new-84c200be})
### 描述

`integer/array dservice.get(object parameters)`

这个方法允许根据给定的参数检索发现的服务。

[comment]: # ({/new-84c200be})

[comment]: # ({new-cdd07a5d})
### 参数

`(object)` 定义需要输出的参数。

这个方法支持以下参数。

|参数                     类|描述|<|
|------------------------------|------|-|
|dserviceids|字符串/数组   只返回拥|给定 ID 的被发现服务。|
|dhostids|字符串/数组   只返回被|现的服务，该服务属于给定的被发现主机。|
|dcheckids|字符串/数组   只返回由|定的发现检查检测到的已发现的服务。|
|druleids|字符串/数组   只返回被|定的发现规则检测到的服务。|
|selectDRules|查询          返|发现规则，该规则规定被发现服务在 `drules` 属性中以数组形式存在。|
|selectDHosts|查询          返|服务属于的已发现主机，该主机在 `dhosts` 属性中以数组形式存在。|
|selectHosts|查询          返|与 `hosts` 属性中的服务具有相同 IP 地址的主机。<br><br>支持 `count`。|
|limitSelects|整数          限|子选择返回的记录数量。<br><br>适用于下列子选择：<br>`selectHosts` - 结果将按 `hostid` 排序。|
|sortfield|字符串/数组   根据给定|属性对结果进行排序。<br><br>可能的值有： `dserviceid`， `dhostid` 和 `ip`。|
|countOutput|布尔值        在[|用评论](/manual/api/reference_commentary#common_get_method_parameters)中详细描述了所有 `get` 方法的常见参数。|
|editable|布尔值        ::|<|
|excludeSearch|布尔值        ::|<|
|filter|对象          :|:|
|limit|整数          :|:|
|output|查询          :|:|
|preservekeys|布尔值        ::|<|
|search|对象          :|:|
|searchByAny|布尔值        ::|<|
|searchWildcardsEnabled|布尔值        ::|<|
|sortorder|字符串/数组   :::|<|
|startSearch|布尔值        ::|<|

[comment]: # ({/new-cdd07a5d})

[comment]: # ({new-7223bab1})
### 返回值

`(integer/array)` 返回：

-   一个对象数组；
-   如果使用了 `countOutput` 参数，被检索的对象的数量。

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### 示例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-09e9b2a3})
#### 检索在主机上发现的服务

检索在被发现主机 "11" 上发现的所有被发现的服务。

请求：

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "dservice.get",
    "params": {
        "output": "extend",
        "dhostids": "11"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

响应：

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "dserviceid": "12",
            "dhostid": "11",
            "value": "",
            "port": "80",
            "status": "1",
            "lastup": "0",
            "lastdown": "1348650607",
            "dcheckid": "5",
            "ip": "192.168.1.134",
            "dns": "john.local"
        },
        {
            "dserviceid": "13",
            "dhostid": "11",
            "value": "",
            "port": "21",
            "status": "1",
            "lastup": "0",
            "lastdown": "1348650610",
            "dcheckid": "6",
            "ip": "192.168.1.134",
            "dns": "john.local"
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-09e9b2a3})

[comment]: # ({new-2647a177})
### 参见

-   [发现主机](/manual/api/reference/dhost/object#discovered_host)
-   [检查发现](/manual/api/reference/dcheck/object#discovery_check)
-   [主机](/manual/api/reference/host/object#host)

[comment]: # ({/new-2647a177})

[comment]: # ({new-76ef9e21})
### 来源

CDService::get() in
*frontends/php/include/classes/api/services/CDService.php*.

[comment]: # ({/new-76ef9e21})
