[comment]: # translation:outdated

[comment]: # ({new-001bbea1})
# > 对象

::: noteclassic
problems are created by the Zabbix server and cannot be
modified via the API.
:::

::: noteclassic
问题是由Zabbix服务器创建的，不能通过API进行修改。
:::

The problem object has the following properties. 问题对象拥有以下属性

|Property|Type|Description|
|--------|----|-----------|
|eventid|string|ID of the problem event. 问题事件的ID|
|source|integer|Type of the problem event.<br><br>Possible values:<br>0 - event created by a trigger;<br>3 - internal event. 问题事件类型。<br>\\\\可能的值：\\\\0-触发器创建的时间<br>3-内部事件|
|object|integer|Type of object that is related to the problem event.<br><br>Possible values for trigger events:<br>0 - trigger.<br><br>Possible values for internal events:<br>0 - trigger;<br>4 - item;<br>5 - LLD rule. 与问题事件相关的对象类型。<br><br>触发器时间可能的值：<br>0-触发器<br><br>内部事件可能的值：<br>0-触发器 4-监控项 5-LLD规则|
|objectid|string|ID of the related object. 关联对象的ID|
|clock|timestamp|Time when the problem event was created. 问题事件创建的时间|
|ns|integer|Nanoseconds when the problem event was created. 问题事件创建的纳秒时间|
|r\_eventid|string|Recovery event ID. 恢复时间的ID|
|r\_clock|timestamp|Time when the recovery event was created. 恢复事件创建的时间|
|r\_ns|integer|Nanoseconds when the recovery event was created. 恢复事件创建的纳秒时间|
|correlationid|string|Correlation rule ID if this event was recovered by global correlation rule. 事件被全局的关联规则恢复，关联规则的ID|
|userid|string|User ID if the problem was manually closed. 手动关闭问题的用户ID|
|name|string|Resolved problem name. 解决问题名称|
|acknowledged|integer|Acknowledge state for problem.<br><br>Possible values:<br>0 - not acknowledged;<br>1 - acknowledged. 问题知晓状态<br>\\\\可能的值：<br>0-不知道 1-知道|
|severity|integer|Problem current severity.<br><br>Possible values:<br>0 - not classified;<br>1 - information;<br>2 - warning;<br>3 - average;<br>4 - high;<br>5 - disaster. 问题当前级别<br>\\\\可能的值：<br>0-未定义 1-信息 2-警告 3-一般严重 4-严重 5-灾难|

[comment]: # ({/new-001bbea1})

[comment]: # ({new-1ce0b7ed})
### Problem

::: noteclassic
Problems are created by the Zabbix server and cannot be
modified via the API.
:::

The problem object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|eventid|string|ID of the problem event.|
|source|integer|Type of the problem event.<br><br>Possible values:<br>0 - event created by a trigger;<br>3 - internal event;<br>4 - event created on service status update.|
|object|integer|Type of object that is related to the problem event.<br><br>Possible values if `source` is set to "event created by a trigger":<br>0 - trigger.<br><br>Possible values if `source` is set to "internal event":<br>0 - trigger;<br>4 - item;<br>5 - LLD rule.<br><br>Possible values if `source` is set to "event created on service status update":<br>6 - service.|
|objectid|string|ID of the related object.|
|clock|timestamp|Time when the problem event was created.|
|ns|integer|Nanoseconds when the problem event was created.|
|r\_eventid|string|Recovery event ID.|
|r\_clock|timestamp|Time when the recovery event was created.|
|r\_ns|integer|Nanoseconds when the recovery event was created.|
|cause_eventid|string|Cause event ID.|
|correlationid|string|Correlation rule ID if this event was recovered by global correlation rule.|
|userid|string|User ID if the problem was manually closed.|
|name|string|Resolved problem name.|
|acknowledged|integer|Acknowledge state for problem.<br><br>Possible values:<br>0 - not acknowledged;<br>1 - acknowledged.|
|severity|integer|Problem current severity.<br><br>Possible values:<br>0 - not classified;<br>1 - information;<br>2 - warning;<br>3 - average;<br>4 - high;<br>5 - disaster.|
|suppressed|integer|Whether the problem is suppressed.<br><br>Possible values:<br>0 - problem is in normal state;<br>1 - problem is suppressed.|
|opdata|string|Operational data with expanded macros.|
|urls|array|Active [media type URLs](/manual/api/reference/problem/object#media-type-urls).|

[comment]: # ({/new-1ce0b7ed})



[comment]: # ({new-acd9b507})
### Problem tag

The problem tag object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|tag|string|Problem tag name.|
|value|string|Problem tag value.|

[comment]: # ({/new-acd9b507})

[comment]: # ({new-52ff7df7})
### Media type URLs

Object with media type url have the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|name|string|Media type defined URL name.|
|url|string|Media type defined URL value.|

Results will contain entries only for active media types with enabled
event menu entry. Macro used in properties will be expanded, but if one
of properties contain non expanded macro both properties will be
excluded from results. Supported macros described on
[page](/manual/appendix/macros/supported_by_location).

[comment]: # ({/new-52ff7df7})
