[comment]: # translation:outdated

[comment]: # ({new-48cd3524})
# 获取

[comment]: # ({/new-48cd3524})

[comment]: # ({new-a18b2713})
### Description 描述

`integer/array problem.get(object parameters)`

The method allows to retrieve problems according to the given
parameters. 此方法允许根据给定参数检索符合条件的问题

[comment]: # ({/new-a18b2713})

[comment]: # ({new-98f39c6a})
### Parameters 参数

`(object)` Parameters defining the desired output. `(object)`
定义所需输出的参数

The method supports the following parameters. 此方法支持一下参数

|Parameter|Type|Description|
|---------|----|-----------|
|eventids|string/array|Return only problems with the given IDs. 仅返回所给IDs的问题|
|groupids|string/array|Return only problems created by objects that belong to the given host groups. 仅返回所属给定主机组对象的问题|
|hostids|string/array|Return only problems created by objects that belong to the given hosts. 仅返回所给定主机对象的问题|
|objectids|string/array|Return only problems created by the given objects. 仅返回所给对象创建的问题|
|applicationids|string/array|Return only problems created by objects that belong to the given applications. Applies only if object is trigger or item. 只返回属于给定应用程序的对象创建的问题。仅当对象是触发器或监控项时才应用。|
|source|integer|Return only problems with the given type.<br><br>Refer to the [problem event object page](object#problem) for a list of supported event types.<br><br>Default: 0 - problem created by a trigger. 只返回给定类型的问题<br><br>跳转到[problem event object page](object#problem) 用于支持事件类型的列表。<br>\\\\默认：0 - 触发器创建的问题|
|object|integer|Return only problems created by objects of the given type.<br><br>Refer to the [problem event object page](object#problem) for a list of supported object types.<br><br>Default: 0 - trigger. 只返回由给定类型的对象创建的问题<br>\\\\跳转到 [problem event object page](object#problem) 用于支持事件类型的列表<br>\\\\默认：0-触发器|
|acknowledged|boolean|`true` - return acknowledged problems only;<br>`false` - unacknowledged only. `true`-返回已知晓的问题<br>返回未知晓的问题|
|severities|integer/array|Return only problems with given event severities. Applies only if object is trigger. 只返回给定事件严重程度的问题。仅当对象是触发器时才应用。|
|evaltype|integer|Rules for tag searching.<br><br>Possible values:<br>0 - (default) And/Or;<br>2 - Or. 规则标签搜索。<br>\\\\可能的值：<br>0 - (默认)与/或 ；2 - 或|
|tags|array of objects|Return only problems with given tags. Exact match by tag and case-insensitive search by value and operator.<br>Format: `[{"tag": "<tag>", "value": "<value>", "operator": "<operator>"}, ...]`.<br>An empty array returns all problems.<br><br>Possible operator types:<br>0 - (default) Like;<br>1 - Equal. 只返回给定标签的问题。按标记精确匹配，按值和运算符不区分大小写搜索。<br>格式：`[{"tag": "<tag>", "value": "<value>", "operator": "<operator>"}, ...]`.<br>空数组返回所有问题<br><br>可能的分隔类型：<br>0 - (默认) 相似 1 - 相等|
|recent|string|true - return PROBLEM and recently RESOLVED problems (depends on Display OK triggers for N seconds)<br>Default: false - UNRESOLVED problems only true - 返回问题和最近已解决的问题（依赖于最近N秒显示OK的触发器）<br>默认：false - 仅真正未处理的问题|
|eventid\_from|string|Return only problems with IDs greater or equal to the given ID. 只返回ID大于或等于给定ID的问题。|
|eventid\_till|string|Return only problems with IDs less or equal to the given ID. 只返回ID小于或等于给定ID的问题。|
|time\_from|timestamp|Return only problems that have been created after or at the given time. 仅返回问题创建时间在所给时间之后的问题|
|time\_till|timestamp|Return only problems that have been created before or at the given time. 仅返回问题创建时间在所给时间之前的问题|
|selectAcknowledges|query|Return problem's updates in the `acknowledges` property. Problem updates are sorted in reverse chronological order.<br><br>The problem update object has the following properties:<br>`acknowledgeid` - `(string)` update's ID;<br>`userid` - `(string)` ID of the user that updated the event;<br>`eventid` - `(string)` ID of the updated event;<br>`clock` - `(timestamp)` time when the event was updated;<br>`message` - `(string)` text of the message;<br>`action` - `(integer)` update action that was performed see [event.acknowledge](/manual/api/reference/event/acknowledge);<br>`old_severity` - `(integer)` event severity before this update action;<br>`new_severity` - `(integer)` event severity after this update action;<br><br>Supports `count`. 在`acknowledges`属性中返回问题的更新。问题更新按时间倒序排序。<br><br>问题更新对象拥有以下属性：`acknowledgeid` - `(string)` 更新的ID<br>'userid'' - `(string)` 更新事件的用户的ID; `eventid` - `(string)` 更新时间的ID<br>`clock` - `(timestamp)` 事件更新的时间<br>`message` - `(string)` 文本信息<br>`action` - `(integer)` 更新动作请参照 [event.acknowledge](/manual/api/reference/event/acknowledge);<br>`old_severity` - `(integer)` 更新动作前事件的级别<br>`new_severity` - `(integer)` 更新动作后事件级别|
|selectTags|query|Return problem's tags. Output format: `[{"tag": "<tag>", "value": "<value>"}, ...]`. 返回问题标签，输出格式：`[{"tag": "<tag>", "value": "<value>"}, ...]`|
|sortfield|string/array|Sort the result by the given properties.<br><br>Possible values are: `eventid`. 根据给定的属性对结果进行排序。<br><br>可能的的值：`eventid`|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters) page. 这个属性使用所有的`get`方法，详细定义在[reference commentary](/manual/api/reference_commentary#common_get_method_parameters)页|
|editable|boolean|^|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

[comment]: # ({/new-98f39c6a})

[comment]: # ({new-7223bab1})
### Return values 返回值

`(integer/array)` Returns either:

-   an array of objects;
-   一个数组对象
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.
-   返回检索到对象的数量，如果`countOutput`参数被引用

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Examples 示例如下

[comment]: # ({/new-b41637d2})

[comment]: # ({new-4d83529d})
#### Retrieving trigger problem events 返回触发器问题事件

Retrieve recent events from trigger "15112."
返回最近触发器id是15112的事件

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "problem.get",
    "params": {
        "output": "extend",
        "selectAcknowledges": "extend",
        "selectTags": "extend",
        "objectids": "15112",
        "recent": "true",
        "sortfield": ["eventid"],
        "sortorder": "DESC"
    },
    "auth": "67f45d3eb1173338e1b1647c4bdc1916",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "eventid": "1245463",
            "source": "0",
            "object": "0",
            "objectid": "15112",
            "clock": "1472457242",
            "ns": "209442442",
            "r_eventid": "1245468",
            "r_clock": "1472457285",
            "r_ns": "125644870",
            "correlationid": "0",
            "userid": "1",
            "name": "Zabbix agent on localhost is unreachable for 5 minutes",
            "acknowledged": "1",
            "severity": "3",
            "acknowledges": [
                {
                    "acknowledgeid": "14443",
                    "userid": "1",
                    "eventid": "1245463",
                    "clock": "1472457281",
                    "message": "problem solved",
                    "action": "6",
                    "old_severity": "0",
                    "new_severity": "0"
                }
            ],
            "tags": [
                {
                    "tag": "test tag",
                    "value": "test value"
                }
            ]
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-4d83529d})

[comment]: # ({new-by})
#### Retrieving problems acknowledged by specified user

Retrieving problems acknowledged by user with ID=10

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "problem.get",
    "params": {
        "output": "extend",
        "selectAcknowledges": ["userid", "action"],
        "filter": {
            "action": 2,
            "action_userid": 10
        },
        "sortfield": ["eventid"],
        "sortorder": "DESC"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": [
        {
            "eventid": "1248566",
            "source": "0",
            "object": "0",
            "objectid": "15142",
            "clock": "1472457242",
            "ns": "209442442",
            "r_eventid": "1245468",
            "r_clock": "1472457285",
            "r_ns": "125644870",
            "correlationid": "0",
            "userid": "10",
            "name": "Zabbix agent on localhost is unreachable for 5 minutes",
            "acknowledged": "1",
            "severity": "3",
            "cause_eventid": "0",
            "opdata": "",
            "acknowledges": [
                {
                    "userid": "10",
                    "action": "2"
                }
            ],
            "suppressed": "0"
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-by})

[comment]: # ({new-c6eb3ba0})
### See also

-   [Alert](/manual/api/reference/alert/object)
-   [Item](/manual/api/reference/item/object)
-   [Host](/manual/api/reference/host/object)
-   [LLD rule](/manual/api/reference/discoveryrule/object#lld_rule)
-   [Trigger](/manual/api/reference/trigger/object)

[comment]: # ({/new-c6eb3ba0})

[comment]: # ({new-15f9267d})
### Source

CEvent::get() in
*frontends/php/include/classes/api/services/CProblem.php*.

[comment]: # ({/new-15f9267d})
