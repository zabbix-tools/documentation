<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="zh-CN" datatype="plaintext" original="manual/api/reference/problem/object.md">
    <body>
      <trans-unit id="001bbea1" xml:space="preserve">
        <source># &gt; Problem object

The following objects are directly related to the `problem` API.</source>
      </trans-unit>
      <trans-unit id="1ce0b7ed" xml:space="preserve">
        <source>### Problem

::: noteclassic
Problems are created by the Zabbix server and cannot be
modified via the API.
:::

The problem object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|eventid|string|ID of the problem event.|
|source|integer|Type of the problem event.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - event created by a trigger;&lt;br&gt;3 - internal event;&lt;br&gt;4 - event created on service status update.|
|object|integer|Type of object that is related to the problem event.&lt;br&gt;&lt;br&gt;Possible values if `source` is set to "event created by a trigger":&lt;br&gt;0 - trigger.&lt;br&gt;&lt;br&gt;Possible values if `source` is set to "internal event":&lt;br&gt;0 - trigger;&lt;br&gt;4 - item;&lt;br&gt;5 - LLD rule.&lt;br&gt;&lt;br&gt;Possible values if `source` is set to "event created on service status update":&lt;br&gt;6 - service.|
|objectid|string|ID of the related object.|
|clock|timestamp|Time when the problem event was created.|
|ns|integer|Nanoseconds when the problem event was created.|
|r\_eventid|string|Recovery event ID.|
|r\_clock|timestamp|Time when the recovery event was created.|
|r\_ns|integer|Nanoseconds when the recovery event was created.|
|cause_eventid|string|Cause event ID.|
|correlationid|string|Correlation rule ID if this event was recovered by global correlation rule.|
|userid|string|User ID if the problem was manually closed.|
|name|string|Resolved problem name.|
|acknowledged|integer|Acknowledge state for problem.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - not acknowledged;&lt;br&gt;1 - acknowledged.|
|severity|integer|Problem current severity.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - not classified;&lt;br&gt;1 - information;&lt;br&gt;2 - warning;&lt;br&gt;3 - average;&lt;br&gt;4 - high;&lt;br&gt;5 - disaster.|
|suppressed|integer|Whether the problem is suppressed.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - problem is in normal state;&lt;br&gt;1 - problem is suppressed.|
|opdata|string|Operational data with expanded macros.|
|urls|array|Active [media type URLs](/manual/api/reference/problem/object#media-type-url).|</source>
      </trans-unit>
      <trans-unit id="acd9b507" xml:space="preserve">
        <source>### Problem tag

The problem tag object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|tag|string|Problem tag name.|
|value|string|Problem tag value.|</source>
      </trans-unit>
      <trans-unit id="52ff7df7" xml:space="preserve">
        <source>### Media type URL

The media type URL object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|name|string|Media type defined URL name.|
|url|string|Media type defined URL value.|

Results will contain entries only for active media types with enabled event menu entry.
Macro used in properties will be expanded, but if one of the properties contains an unexpanded macro, both properties will be excluded from results.
For supported macros, see [*Supported macros*](/manual/appendix/macros/supported_by_location).</source>
      </trans-unit>
    </body>
  </file>
</xliff>
