[comment]: # translation:outdated

[comment]: # ({new-52ad42a2})
# Configuration 配置

这个类用于导入和导出 Zabbix 的配置数据。

相关方法:\

-   [configuration.export](/zh/manual/api/reference/configuration/export) -
    导出配置
-   [configuration.import](/zh/manual/api/reference/configuration/import) -
    导入配置

This class is designed to export and import Zabbix configuration data.

Available methods:\

-   [configuration.export](/manual/api/reference/configuration/export) -
    exporting the configuration
-   [configuration.import](/manual/api/reference/configuration/import) -
    importing the configuration

[comment]: # ({/new-52ad42a2})
