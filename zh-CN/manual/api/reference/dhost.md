[comment]: # translation:outdated

[comment]: # ({new-e35cf787})
# 发现主机

这个类是设计用于`发现主机`。

对象引用：\

-   [发现主机](/manual/api/reference/dhost/object#discovered_host)

可用的方法：\

-   [dhost.get](/manual/api/reference/dhost/get) - 获取已发现的主机。

[comment]: # ({/new-e35cf787})
