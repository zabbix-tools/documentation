[comment]: # translation:outdated

[comment]: # ({new-41154bde})
# > 值映射对象

以下对象与`VALUE`API直接相关。

[comment]: # ({/new-41154bde})

[comment]: # ({new-65471f47})
## > Value map object

The following objects are directly related to the `valuemap` API.

[comment]: # ({/new-65471f47})

[comment]: # ({new-145a9918})
### 值映射

值映射对象具有以下属性。

|属性            类|说明|<|
|---------------------|------|-|
|valuemapid|string|*(readonly)* 值映射的ID|
|**name**<br>(required)|string|值映射的名称。|
|**mappings**<br>(required)|array|值映射当前映射值。值映射对象[object\#value\_mappings细节描述如下](object#value_mappings细节描述如下).|

### Value map

The value map object has the following properties.

|Property|Type|Description|
|--------|----|-----------|
|valuemapid|string|*(readonly)* ID of the value map.|
|**name**<br>(required)|string|Name of the value map.|
|**mappings**<br>(required)|array|Value mappings for current value map. The mapping object is [described in detail below](object#value_mappings).|

### 价值映射

值映射对象定义值映射的映射值。 它具有以下属性。

|属性            类|说明|<|
|---------------------|------|-|
|**value**<br>(required)|string|原值。|
|**newvalue**<br>(required)|string|原始值映射到的值。|

### Value mappings

The value mappings object defines value mappings of the value map. It
has the following properties.

|Property|Type|Description|
|--------|----|-----------|
|**value**<br>(required)|string|Original value.|
|**newvalue**<br>(required)|string|Value to which the original value is mapped to.|

[comment]: # ({/new-145a9918})
