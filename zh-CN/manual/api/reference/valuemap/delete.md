[comment]: # translation:outdated

[comment]: # ({new-a1e429c6})
# valuemap.delete

[comment]: # ({/new-a1e429c6})

[comment]: # ({new-b73af783})
### 说明

`object valuemap.delete(array valuemapids)`

此方法允许删除值映射。

[comment]: # ({/new-b73af783})

[comment]: # ({new-db6b3809})
### Description

`object valuemap.delete(array valuemapids)`

This method allows to delete value maps.

[comment]: # ({/new-db6b3809})

[comment]: # ({new-59feda24})
### 参数

`(array)` 要被删除的映射的ID。

[comment]: # ({/new-59feda24})

[comment]: # ({new-b41637d2})
### Parameters

`(array)` IDs of the value maps to delete.

[comment]: # ({/new-b41637d2})

[comment]: # ({new-bf1c93a7})
### 返回值

`(object)` 返回一个对象，该对象包含“VALUE”属性下的已删除值映射的ID。

[comment]: # ({/new-bf1c93a7})

[comment]: # ({new-908d048e})
### Return values

`(object)` Returns an object containing the IDs of the deleted value
maps under the `valuemapids` property.

### 示例

### Examples

#### 删除多个值映射

删除2个值映射。

#### Deleting multiple value maps

Delete two value maps.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "valuemap.delete",
    "params": [
        "1",
        "2"
    ],
    "auth": "57562fd409b3b3b9a4d916d45207bbcb",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "valuemapids": [
            "1",
            "2"
        ]
    },
    "id": 1
}
```

### 来源

CValueMap::delete() in
*frontends/php/include/classes/api/services/CValueMap.php*.

[comment]: # ({/new-908d048e})
