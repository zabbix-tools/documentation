[comment]: # translation:outdated

[comment]: # ({new-6b085503})
# valuemap.update

[comment]: # ({/new-6b085503})

[comment]: # ({new-ab463b0c})
### 说明

`object valuemap.update(object/array valuemaps)`

该方法允许更新现有的值映射。

[comment]: # ({/new-ab463b0c})

[comment]: # ({new-0094db2a})
### Description

`object valuemap.update(object/array valuemaps)`

This method allows to update existing value maps.

[comment]: # ({/new-0094db2a})

[comment]: # ({new-0dbe7e67})
### 参数

`(object/array)` 要更新的[值映射特性](object#value_map)。

必须为每个值映射定义`valuemapid`属性，所有其他属性都是可选的。
只有通过的属性将被更新，所有其他属性将保持不变。

[comment]: # ({/new-0dbe7e67})

[comment]: # ({new-b41637d2})
### Parameters

`(object/array)` [Value map properties](object#value_map) to be updated.

The `valuemapid` property must be defined for each value map, all other
properties are optional. Only the passed properties will be updated, all
others will remain unchanged.

[comment]: # ({/new-b41637d2})

[comment]: # ({new-571fcc62})
### 返回值

`（object）`返回一个对象，它包含`valuemapids`属性下更新的值映射的ID。

[comment]: # ({/new-571fcc62})

[comment]: # ({new-7b3b6bc4})
### Return values

`(object)` Returns an object containing the IDs of the updated value
maps under the `valuemapids` property.

[comment]: # ({/new-7b3b6bc4})

[comment]: # ({new-fc02e5cf})
### 示例

### Examples

#### 更改值映射名称

将值映射名称更改为“设备状态”

#### Changing value map name

Change value map name to "Device status".

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "valuemap.update",
    "params": {
        "valuemapid": "2",
        "name": "Device status"
    },
    "auth": "57562fd409b3b3b9a4d916d45207bbcb",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "valuemapids": [
            "2"
        ]
    },
    "id": 1
}
```

#### Changing mappings for one value map.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "valuemap.update",
    "params": {
        "valuemapid": "2",
        "mappings": [
            {
                "value": "0",
                "newvalue": "Online"
            },
            {
                "value": "1",
                "newvalue": "Offline"
            }
        ]
    },
    "auth": "57562fd409b3b3b9a4d916d45207bbcb",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "valuemapids": [
            "2"
        ]
    },
    "id": 1
}
```

### 来源

### Source

CValueMap::update() in
*frontends/php/include/classes/api/services/CValueMap.php*.

[comment]: # ({/new-fc02e5cf})
