[comment]: # translation:outdated

[comment]: # ({new-c9bc126a})
# 创建

[comment]: # ({/new-c9bc126a})

[comment]: # ({new-0e3179d3})
### Description 说明

`object task.create(object task)`

This method allows to create new task. 该方法允许创建新的任务。

[comment]: # ({/new-0e3179d3})

[comment]: # ({new-2574c234})
### Parameters 参数

`(object)` A task to create. `(object)`需要创建的任务。 The method
accepts the following parameters. 此方法接受以下参数。

|Parameter 参数    T|pe 类型                  Des|ription 说明|
|---------------------|------------------------------|--------------|
|**type**<br>(required 必须)|integer 整数型             Ta<br>|k type. 任务类型。<br>Possible values: 许可值：<br>6 - Check now. 正在核实。|
|**itemids**<br>(required 必须)|string/array 字符串/数组   IDs<br>|f items and low-level discovery rules. 监控项和低级别发现规则的ID。<br>Item or discovery rule must of the following type: 监控项或自动发现规则必须是以下类型：<br>0 - Zabbix agent; Zabbix agent；<br>1 - SNMPv1 agent; SNMPv1客户端；<br>3 - simple check; 简单检查；<br>4 - SNMPv2 agent; SNMPv2客户端；<br>5 - Zabbix internal; Zabbix内部；<br>6 - SNMPv3 agent; SNMPv3客户端；<br>8 - Zabbix aggregate; Zabbix整合；<br>10 - external check; 外部检查；<br>11 - database monitor; 数据库监控；<br>12 - IPMI agent; IPMI客户端；<br>13 - SSH agent; SSH客户端；<br>14 - TELNET agent; TELNET客户端；<br>15 - calculated; 计算项；<br>16 - JMX agent. JMX客户端。|

[comment]: # ({/new-2574c234})

[comment]: # ({new-918de761})
If item or discovery ruls is of type *Dependent item*, then top level master item must be of type:
-   Zabbix agent
-   SNMPv1/v2/v3 agent
-   Simple check
-   Internal check
-   External check
-   Database monitor
-   HTTP agent
-   IPMI agent
-   SSH agent
-   TELNET agent
-   Calculated check
-   JMX agent

[comment]: # ({/new-918de761})

[comment]: # ({new-ab87ce2a})
### Return values 返回值

`(object)` Returns an object containing the IDs of the created tasks
under the `taskids` property. One task is created for each item and
low-level discovery rule. The order of the returned IDs matches the
order of the passed `itemids`.
`(object)`返回一个对象，该对象包含在`taskids`属性中已创建任务的ID。为每个监控项和低级别发现规则创建的任务，返回ID的顺序与传递`itemids`的顺序相匹配。

[comment]: # ({/new-ab87ce2a})

[comment]: # ({new-b41637d2})
### Examples 范例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-5c5fa6f1})
#### Creating a task 创建任务

Create a task `check now` for two items. One is an item, the other is a
low-level discovery rule.
为两个项目，其中一个是监控项，另外一个低级别发现规则，创建一个`check now`任务。

Request 请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "task.create",
    "params": {
        "type": "6",
        "itemids": ["10092", "10093"],
    },
    "auth": "700ca65537074ec963db7efabda78259",
    "id": 1
}
```

Response 响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "taskids": [
            "1",
            "2"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-5c5fa6f1})

[comment]: # ({new-d87c906b})
### Source 源码

CTask::create() in
*frontends/php/include/classes/api/services/CTask.php*.
CTask::create()方法可在*frontends/php/include/classes/api/services/CTask.php*中参考。

[comment]: # ({/new-d87c906b})


[comment]: # ({new-be350bd3})
### Source

CTask::create() in *ui/include/classes/api/services/CTask.php*.

[comment]: # ({/new-be350bd3})
