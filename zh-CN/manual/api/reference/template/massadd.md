[comment]: # translation:outdated

[comment]: # ({new-8c618db1})
# 批量添加

[comment]: # ({/new-8c618db1})

[comment]: # ({new-399b9834})
### Description 说明

`object template.massadd(object parameters)`

This method allows to simultaneously add multiple related objects to the
given templates.
此方法允许同时替换或删除相关对象并更新多个模板上的属性。

[comment]: # ({/new-399b9834})

[comment]: # ({new-4167b841})
### Parameters 参数

`(object)` Parameters containing the IDs of the templates to update and
the objects to add to the templates.
`(object)`参数包含需要更新的模板ID以及添加到模板的对象。 The method
accepts the following parameters. 该方法接受以下参数。

|Parameter 参数    T|pe 类型                Des|ription 说明|
|---------------------|----------------------------|--------------|
|**templates**<br>(required 必须)|object/array 对象/数组   Tem<br>|lates to be updated. 需要更新的模板。<br>The templates must have the `templateid` property defined. 模板必须定义`templateid`属性。|
|groups|object/array 对象/数组   Hos|groups to add the given templates to. 主机组添加指定的模板。<br><br>The host groups must have the `groupid` property defined. 主机组必须定义`groupid`属性。|
|hosts|object/array 对象/数组   Hos|s and templates to link the given templates to. 将主机和模板链接到指定的模板中。<br><br>The hosts must have the `hostid` property defined. 主机必须定义`hostid`属性。|
|macros|object/array 对象/数组   Use|macros to be created for the given templates. 为指定的模板创建用户宏。|
|templates\_link|object/array 对象/数组   Tem|lates to link to the given templates. 将模板链接到指定模板。<br><br>The templates must have the `templateid` property defined. 模板必须定义`templateid`属性。|

[comment]: # ({/new-4167b841})

[comment]: # ({new-dcba01c8})
### Return values 返回值

`(object)` Returns an object containing the IDs of the updated templates
under the `templateids` property.
`(object)`返回一个对象，此对象包含在`templateids`属性中已更新模板的ID。

[comment]: # ({/new-dcba01c8})

[comment]: # ({new-b41637d2})
### Examples 范例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-f8fe44e4})
#### Adding templates to a group 添加模板到组

Add two templates to the host group "2".
添加两个模板到ID为"2"的主机组中。

Request 请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "template.massadd",
    "params": {
        "templates": [
            {
                "templateid": "10085"
            },
            {
                "templateid": "10086"
            }
        ],
        "groups": [
            {
                "groupid": "2"
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response 响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "templateids": [
            "10085",
            "10086"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-f8fe44e4})

[comment]: # ({new-7dbed7e5})
#### Link two templates to a template

Link templates "10106" and "10104" to template "10073".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "template.massadd",
    "params": {
        "templates": [
            {
                "templateid": "10073"
            }
        ],
        "templates_link": [
            {
                "templateid": "10106"
            },
            {
                "templateid": "10104"
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "templateids": [
            "10073"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-7dbed7e5})


[comment]: # ({new-bd8dd370})
### See also 参考

-   [template.update](update)
-   [Host](/zh/manual/api/reference/host/object#host)
-   [Host group](/zh/manual/api/reference/hostgroup/object#host_group)
-   [User
    macro](/zh/manual/api/reference/usermacro/object#hosttemplate_level_macro)

[comment]: # ({/new-bd8dd370})

[comment]: # ({new-2f7abcb1})
### Source 源码

CTemplate::massAdd() in
*frontends/php/include/classes/api/services/CTemplate.php*.
CTemplate::massAdd()方法可在//frontends/php/include/classes/api/services/CTemplate.php中参考。

[comment]: # ({/new-2f7abcb1})
