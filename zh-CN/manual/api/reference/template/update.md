[comment]: # translation:outdated

[comment]: # ({new-36f834f7})
# 更新

[comment]: # ({/new-36f834f7})

[comment]: # ({new-f8425904})
### Description 说明

`object template.update(object/array templates)`

This method allows to update existing templates.
此方法允许更新现有模板。

[comment]: # ({/new-f8425904})

[comment]: # ({new-5d06cf34})
### Parameters 参数

`(object/array)` Template properties to be updated.
`(object/array)`需要被更新的模板属性。

The `templateid` property must be defined for each template, all other
properties are optional. Only the given properties will be updated, all
others will remain unchanged.
必须为每个模板定义`templateid`属性，所有其他属性都是可选的。只有给定的属性将被更新，所有其他属性将保持不变。
Additionally to the [standard template properties](object#template), the
method accepts the following parameters. 除[standard template
properties](object#template)之外，该方法接受以下参数。

|Parameter 参数     T|pe 类型                Des|ription 说明|
|----------------------|----------------------------|--------------|
|groups|object/array 对象/数组   Hos|groups to replace the current host groups the templates belong to. 替换所属模板的当前主机组。<br><br>The host groups must have the `groupid` property defined. 主机组必须已定义`groupid`属性。|
|hosts|object/array 对象/数组   Hos|s and templates to replace the ones the templates are currently linked to. 替换当前链接模板的主机和模板。<br><br>Both hosts and templates must use the `hostid` property to pass an ID. 主机和模板都必须使用`hostid`属性传递唯一ID。|
|macros|object/array 对象/数组   Use|macros to replace the current user macros on the given templates. 替换指定模板上的当前用户宏。|
|templates|object/array 对象/数组   Tem|lates to replace the currently linked templates. Templates that are not passed are only unlinked. 用于替换当前链接的模板，未通过的模板只是被取消链接。<br><br>The templates must have the `templateid` property defined. 模板必须已定义`templateid`属性。|
|templates\_clear|object/array 对象/数组   Tem|lates to unlink and clear from the given templates. 从指定模板中取消链接并清除数据。<br><br>The templates must have the `templateid` property defined. 模板必须已定义`templateid`属性。|

[comment]: # ({/new-5d06cf34})

[comment]: # ({new-dcba01c8})
### Return values 返回值

`(object)` Returns an object containing the IDs of the updated templates
under the `templateids` property.
`(object)`返回一个对象，此对象包含在`templateids`属性中已更新模板的ID。

[comment]: # ({/new-dcba01c8})

[comment]: # ({new-b41637d2})
### Examples 范例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-0080b19f})
#### Renaming a template 重命名模板

Rename the template to "Template OS Linux". 将模板重命名为"Template OS
Linux"。

Request 请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "template.update",
    "params": {
        "templateid": "10086",
        "name": "Template OS Linux"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response 响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "templateids": [
            "10086"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-0080b19f})

[comment]: # ({new-e7972b69})
### Source 源码

CTemplate::update() in
*frontends/php/include/classes/api/services/CTemplate.php*.
CTemplate::update()方法可在*frontends/php/include/classes/api/services/CTemplate.php*中参考。

[comment]: # ({/new-e7972b69})


[comment]: # ({new-eb72cc1c})
### Source

CTemplate::update() in *ui/include/classes/api/services/CTemplate.php*.

[comment]: # ({/new-eb72cc1c})
