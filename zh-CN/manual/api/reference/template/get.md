[comment]: # translation:outdated

[comment]: # ({new-896915aa})
# 获取

[comment]: # ({/new-896915aa})

[comment]: # ({new-4e400711})
### Description 说明

`integer/array template.get(object parameters)`

The method allows to retrieve templates according to the given
parameters. 此方法允许根据指定的参数检索模板。

[comment]: # ({/new-4e400711})

[comment]: # ({new-97f0e40b})
### Parameters 参数

`(object)` Parameters defining the desired output.
`(object)`定义需要输出的参数。

The method supports the following parameters. 此方法支持以下参数。

|Parameter 参数           T|pe 类型                  Des|ription 说明|
|----------------------------|------------------------------|--------------|
|templateids|string/array 字符串/数组   Retu|n only templates with the given template IDs. 只返回指定模板ID的模板。|
|groupids|string/array 字符串/数组   Retu|n only templates that belong to the given host groups. 只返回指定主机组所属的模板。|
|parentTemplateids|string/array 字符串/数组   Retu|n only templates that are children of the given templates. 只返回指定子类模板的模板。|
|hostids|string/array 字符串/数组   Retu|n only templates that are linked to the given hosts. 只返回被链接到指定主机的模板。|
|graphids|string/array 字符串/数组   Retu|n only templates that contain the given graphs. 只返回包含指定图形的模板。|
|itemids|string/array 字符串/数组   Retu|n only templates that contain the given items. 只返回包含指定监控项的模板。|
|triggerids|string/array 字符串/数组   Retu|n only templates that contain the given triggers. 只返回包含指定触发器的模板。|
|with\_items|flag 标记                  R|turn only templates that have items. 只返回具有监控项的模板。|
|with\_triggers|flag 标记                  R|turn only templates that have triggers. 只返回具有触发器的模板。|
|with\_graphs|flag 标记                  R|turn only templates that have graphs. 只返回具有图形的模板。|
|with\_httptests|flag 标记                  R|turn only templates that have web scenarios. 只返回具有web场景的模板。|
|selectGroups|query 查询                 R|turn the host groups that the template belongs to in the `groups` property. 从`groups`属性中返回所属模板的主机组。|
|selectHosts|query 查询                 R|turn the hosts that are linked to the template in the `hosts` property. 从`hosts`属性中返回被链接模板的主机。<br><br>Supports `count`. 支持`count`。|
|selectTemplates|query 查询                 R|turn the child templates in the `templates` property. 从`templates`属性中返回子类模板。<br><br>Supports `count`. 支持`count`。|
|selectParentTemplates|query 查询                 R|turn the parent templates in the `parentTemplates` property. 从`parentTemplates`属性中返回父类模板。<br><br>Supports `count`. 支持`count`。|
|selectHttpTests|query 查询                 R|turn the web scenarios from the template in the `httpTests` property. 从`httpTests`属性中返回来自模板的web场景。<br><br>Supports `count`. 支持`count`。|
|selectItems|query 查询                 R|turn items from the template in the `items` property. 从`items`属性中返回来自模板的监控项。<br><br>Supports `count`. 支持`count`。|
|selectDiscoveries|query 查询                 R|turn low-level discoveries from the template in the `discoveries` property. 从`discoveries`属性中返回来自模板的低级别发现。<br><br>Supports `count`. 支持`count`。|
|selectTriggers|query 查询                 R|turn triggers from the template in the `triggers` property. 从`triggers`属性中返回来自模板的触发器。<br><br>Supports `count`. 支持`count`。|
|selectGraphs|query 查询                 R|turn graphs from the template in the `graphs` property. 从`graphs`属性中返回来自模板的图表。<br><br>Supports `count`. 支持`count`。|
|selectApplications|query 查询                 R|turn applications from the template in the `applications` property. 从`applications`属性中返回来自模板的应用。<br><br>Supports `count`. 支持`count`。|
|selectMacros|query 查询                 R|turn the macros from the template in the `macros` property. 从`macros`属性中返回来自模板的宏。|
|selectScreens|query 查询                 R|turn screens from the template in the `screens` property. 从`screens`属性中返回来自模板的聚合图形。<br><br>Supports `count`. 支持`count`。|
|limitSelects|integer 整数型             Li|its the number of records returned by subselects. 限制子查询返回的记录数。<br><br>Applies to the following subselects: 应用于以下子查询：<br>`selectTemplates` - results will be sorted by `name`; 结果将以`name`排序；<br>`selectHosts` - sorted by `host`; 以`host`排序；<br>`selectParentTemplates` - sorted by `host`; 以`host`排序；<br>`selectItems` - sorted by `name`; 以`name`排序；<br>`selectDiscoveries` - sorted by `name`; 以`name`排序；<br>`selectTriggers` - sorted by `description`; 以`description`排序；<br>`selectGraphs` - sorted by `name`; 以`name`排序；<br>`selectApplications` - sorted by `name`; 以`name`排序；<br>`selectScreens` - sorted by `name`. 以`name`排序；|
|sortfield|string/array 字符串/数组   Sort|the result by the given properties. 根据给定的属性为结果排序。<br><br>Possible values are: `hostid`, `host`, `name`, `status`. 许可值为：`hostid`,`host`,`name`,`status`。|
|countOutput|boolean 布尔值             Th|se parameters being common for all `get` methods are described in detail in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters). 这些参数十分普遍，适用所有`get`方法，详情参见[reference commentary](/zh/manual/api/reference_commentary#common_get_method_parameters)。|
|editable|boolean 布尔值             ::|<|
|excludeSearch|boolean 布尔值             ::|<|
|filter|object 对象                :|:|
|limit|integer 整数型             ::|<|
|output|query 查询                 :|:|
|preservekeys|boolean 布尔值             ::|<|
|search|object 对象                :|:|
|searchByAny|boolean 布尔值             ::|<|
|searchWildcardsEnabled|boolean 布尔值             ::|<|
|sortorder|string/array 字符串/数组   :::|<|
|startSearch|boolean 布尔值             ::|<|

[comment]: # ({/new-97f0e40b})

[comment]: # ({new-7223bab1})
### Return values 返回值

`(integer/array)` Returns either: `(integer/array)`返回两者其中任一：

-   an array of objects; 一组对象；
-   the count of retrieved objects, if the `countOutput` parameter has
    been used. 如果已经使用了`countOutput`参数，则检索对象的计数。

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Examples 范例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-7e546e9a})
#### Retrieving templates by name 按名称检索模板

Retrieve all data about two templates named "Template OS Linux" and
"Template OS Windows". 检索名称为"Template OS Linux"和"Template OS
Windows"这两个模板的所有数据。 Request 请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "template.get",
    "params": {
        "output": "extend",
        "filter": {
            "host": [
                "Template OS Linux",
                "Template OS Windows"
            ]
        }
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response 响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "proxy_hostid": "0",
            "host": "Template OS Linux",
            "status": "3",
            "disable_until": "0",
            "error": "",
            "available": "0",
            "errors_from": "0",
            "lastaccess": "0",
            "ipmi_authtype": "0",
            "ipmi_privilege": "2",
            "ipmi_username": "",
            "ipmi_password": "",
            "ipmi_disable_until": "0",
            "ipmi_available": "0",
            "snmp_disable_until": "0",
            "snmp_available": "0",
            "maintenanceid": "0",
            "maintenance_status": "0",
            "maintenance_type": "0",
            "maintenance_from": "0",
            "ipmi_errors_from": "0",
            "snmp_errors_from": "0",
            "ipmi_error": "",
            "snmp_error": "",
            "jmx_disable_until": "0",
            "jmx_available": "0",
            "jmx_errors_from": "0",
            "jmx_error": "",
            "name": "Template OS Linux",
            "flags": "0",
            "templateid": "10001",
            "description": "",
            "tls_connect": "1",
            "tls_accept": "1",
            "tls_issuer": "",
            "tls_subject": "",
            "tls_psk_identity": "",
            "tls_psk": ""
        },
        {
            "proxy_hostid": "0",
            "host": "Template OS Windows",
            "status": "3",
            "disable_until": "0",
            "error": "",
            "available": "0",
            "errors_from": "0",
            "lastaccess": "0",
            "ipmi_authtype": "0",
            "ipmi_privilege": "2",
            "ipmi_username": "",
            "ipmi_password": "",
            "ipmi_disable_until": "0",
            "ipmi_available": "0",
            "snmp_disable_until": "0",
            "snmp_available": "0",
            "maintenanceid": "0",
            "maintenance_status": "0",
            "maintenance_type": "0",
            "maintenance_from": "0",
            "ipmi_errors_from": "0",
            "snmp_errors_from": "0",
            "ipmi_error": "",
            "snmp_error": "",
            "jmx_disable_until": "0",
            "jmx_available": "0",
            "jmx_errors_from": "0",
            "jmx_error": "",
            "name": "Template OS Windows",
            "flags": "0",
            "templateid": "10081",
            "description": "",
            "tls_connect": "1",
            "tls_accept": "1",
            "tls_issuer": "",
            "tls_subject": "",
            "tls_psk_identity": "",
            "tls_psk": ""
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-7e546e9a})

[comment]: # ({new-84d0cbf3})
#### Retrieving template groups

Retrieve template groups that the template "Linux by Zabbix agent" is a member of.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "template.get",
    "params": {
        "output": ["hostid"],
        "selectTemplateGroups": "extend",
        "filter": {
            "host": [
                "Linux by Zabbix agent"
            ]
        }
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": [
        {
            "templateid": "10001",
            "templategroups": [
                {
                    "groupid": "10",
                    "name": "Templates/Operating systems",
                    "uuid": "846977d1dfed4968bc5f8bdb363285bc"
                }
            ]
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-84d0cbf3})

[comment]: # ({new-0fe2d603})
### See also 参考

-   [Host group](/zh/manual/api/reference/hostgroup/object#host_group)
-   [Template](object#template)
-   [User
    macro](/zh/manual/api/reference/usermacro/object#hosttemplate_level_macro)
-   [Host
    interface](/zh/manual/api/reference/hostinterface/object#host_interface)

[comment]: # ({/new-0fe2d603})

[comment]: # ({new-ee3fc022})
### Source 源码

CTemplate::get() in
*frontends/php/include/classes/api/services/CTemplate.php*.
CTemplate::get()方法可在frontends/php/include/classes/api/services/CTemplate.php中参考。

[comment]: # ({/new-ee3fc022})


[comment]: # ({new-4fdbde5d})
### Source

CTemplate::get() in *ui/include/classes/api/services/CTemplate.php*.

[comment]: # ({/new-4fdbde5d})
