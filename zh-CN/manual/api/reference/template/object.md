[comment]: # translation:outdated

[comment]: # ({new-ac6966b9})
# > 对象

The following objects are directly related to the `template` API.
以下对象与API`模板`直接相关。

[comment]: # ({/new-ac6966b9})

[comment]: # ({new-ccfaad06})
### Template 模板

The template object has the following properties. 模板对象具有以下属性。

|Property 参数     T|pe 类型       Des|ription 说明|
|---------------------|-------------------|--------------|
|templateid|string 字符串   *(|eadonly 只读)* ID of the template. 模板ID。|
|**host**<br>(required 必须)|string 字符串   Te|hnical name of the template. 模板的正式名称。|
|description|text 文本       D|scription of the template. 模板说明。|
|name|string 字符串   Vi|ible name of the host. 主机的可见名称。<br><br>Default: `host` property value. 默认：`主机`的属性值。|

[comment]: # ({/new-ccfaad06})


[comment]: # ({new-2248aef2})
### Template tag

The template tag object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**tag**<br>(required)|string|Template tag name.|
|value|string|Template tag value.|

[comment]: # ({/new-2248aef2})
