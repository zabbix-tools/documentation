[comment]: # translation:outdated

[comment]: # ({new-e42e8a0a})
# 批量更新

[comment]: # ({/new-e42e8a0a})

[comment]: # ({new-cf27d8f8})
### Description 说明

`object template.massupdate(object parameters)`

This method allows to simultaneously replace or remove related objects
and update properties on multiple templates.
此方法允许同时替换或删除相关对象并更新多个模板上的属性。

[comment]: # ({/new-cf27d8f8})

[comment]: # ({new-1f7d5759})
### Parameters 参数

`(object)` Parameters containing the IDs of the templates to update and
the properties that should be updated.
`(object)`参数包含需要更新的模板ID以及需要更新的属性。 Additionally to
the [standard template properties](object#template), the method accepts
the following parameters. 除[standard template
properties](object#template)之外，该方法接受以下参数。

|Parameter 参数     T|pe 类型                Des|ription 说明|
|----------------------|----------------------------|--------------|
|**templates**<br>(required 必须)|object/array 对象/数组   Tem<br>|lates to be updated. 需要更新的模板。<br>The templates must have the `templateid` property defined. 模板必须已定义`templateid`属性。|
|groups|object/array 对象/数组   Hos|groups to replace the current host groups the templates belong to. 替换所属模板的当前主机组。<br><br>The host groups must have the `groupid` property defined. 主机组必须已定义`groupid`属性。|
|hosts|object/array 对象/数组   Hos|s and templates to replace the ones the templates are currently linked to. 替换当前链接模板的主机和模板。<br><br>Both hosts and templates must use the `hostid` property to pass an ID. 主机和模板都必须使用`hostid`属性传递唯一ID。|
|macros|object/array 对象/数组   Use|macros to replace the current user macros on the given templates. 替换指定模板上的当前用户宏。|
|templates\_clear|object/array 对象/数组   Tem|lates to unlink and clear from the given templates. 从指定模板中取消链接并清除数据。<br><br>The templates must have the `templateid` property defined. 模板必须已定义`templateid`属性。|
|templates\_link|object/array 对象/数组   Tem|lates to replace the currently linked templates. 替换当前链接的模板。<br><br>The templates must have the `templateid` property defined. 模板必须已定义`templateid`属性。|

[comment]: # ({/new-1f7d5759})

[comment]: # ({new-dcba01c8})
### Return values 返回值

`(object)` Returns an object containing the IDs of the updated templates
under the `templateids` property.
`(object)`返回一个对象，此对象包含在`templateids`中已更新模板的ID。

[comment]: # ({/new-dcba01c8})

[comment]: # ({new-b41637d2})
### Examples 范例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-d18b091c})
#### Unlinking a template

Unlink and clear template "10091" from the given templates.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "template.massupdate",
    "params": {
        "templates": [
            {
                "templateid": "10085"
            },
            {
                "templateid": "10086"
            }
        ],
        "templates_clear": [
            {
                "templateid": "10091"
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "templateids": [
            "10085",
            "10086"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-d18b091c})

[comment]: # ({new-0ecefb75})
#### Replacing host groups 替换主机组

Unlink and clear template "10091" from the given templates.
从指定的模板中取消链接并清除ID为"10091"的模板。

Request 请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "template.massupdate",
    "params": {
        "templates": [
            {
                "templateid": "10085"
            },
            {
                "templateid": "10086"
            }
        ],
        "templates_clear": [
            {
                "templateid": "10091"
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response 响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "templateids": [
            "10085",
            "10086"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-0ecefb75})

[comment]: # ({new-5d283e35})
### See also 参考

-   [template.update](update)
-   [template.massadd](massadd)
-   [Host group](/zh/manual/api/reference/hostgroup/object#host_group)
-   [User
    macro](/zh/manual/api/reference/usermacro/object#hosttemplate_level_macro)

[comment]: # ({/new-5d283e35})

[comment]: # ({new-b5f8dcd9})
### Source 源码

CTemplate::massUpdate() in
*frontends/php/include/classes/api/services/CTemplate.php*.
CTemplate::massUpdate()方法可在*frontends/php/include/classes/api/services/CTemplate.php*中参考。

[comment]: # ({/new-b5f8dcd9})
