[comment]: # translation:outdated

[comment]: # ({new-7f5b147d})
# 创建

[comment]: # ({/new-7f5b147d})

[comment]: # ({new-d9142f8c})
### Description 说明

`object template.create(object/array templates)`

This method allows to create new templates. 此方法允许创建新模板。

[comment]: # ({/new-d9142f8c})

[comment]: # ({new-95e9c6ad})
### Parameters 参数

`(object/array)` Templates to create. 创建模板。

Additionally to the [standard template properties](object#template), the
method accepts the following parameters.
除了[标准模板属性](object#模板)之外，该方法接受以下属性。

|Parameter 参数    T|pe 类型                Des|ription 说明|
|---------------------|----------------------------|--------------|
|**groups**<br>(required 必须)|object/array 对象/数组   Hos<br>|groups to add the template to. 模板添加到主机组。<br>The host groups must have the `groupid` property defined. 主机组必须定义`groupid`属性。|
|templates|object/array 对象/数组   Tem|lates to be linked to the template. 被链接到模板的模板。<br><br>The templates must have the `templateid` property defined. 模板必须定义`templateid`属性。|
|macros|object/array 对象/数组   Use|macros to be created for the template. 为模板创建的用户宏。|
|hosts|object/array 对象/数组   Hos|s to link the template to. 链接到模板的主机。<br><br>The hosts must have the `hostid` property defined. 主机必须定义`hostid`属性。|

[comment]: # ({/new-95e9c6ad})

[comment]: # ({new-68ed5aa8})
### Return values 返回值

`(object)` Returns an object containing the IDs of the created templates
under the `templateids` property. The order of the returned IDs matches
the order of the passed templates.
`(object)`返回一个对象，包含`templateids`属性中创建的模板ID，返回ID的顺序与传递模板的顺序一致。

[comment]: # ({/new-68ed5aa8})

[comment]: # ({new-b41637d2})
### Examples 范例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-104d61d1})
#### Creating a template 创建模板

Create a template and link it to two hosts.
创建一个模板并将其链接到两台主机上。

Request 请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "template.create",
    "params": {
        "host": "Linux template",
        "groups": {
            "groupid": 1
        },
        "hosts": [
            {
                "hostid": "10084"
            },
            {
                "hostid": "10090"
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response 响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "templateids": [
            "10086"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-104d61d1})

[comment]: # ({new-15290398})
### Source 源码

CTemplate::create() in
*frontends/php/include/classes/api/services/CTemplate.php*.
CTemplate::create()方法可在*frontends/php/include/classes/api/services/CTemplate.php*中参考。

[comment]: # ({/new-15290398})
