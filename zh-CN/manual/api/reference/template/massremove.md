[comment]: # translation:outdated

[comment]: # ({new-ae0db7b8})
# 批量删除

[comment]: # ({/new-ae0db7b8})

[comment]: # ({new-ba911402})
### Description 说明

`object template.massremove(object parameters)`

This method allows to remove related objects from multiple templates.
方法允许从多个模板中删除相关对象。

[comment]: # ({/new-ba911402})

[comment]: # ({new-bd7f8b44})
### Parameters 参数

`(object)` Parameters containing the IDs of the templates to update and
the objects that should be removed.
`(object)`参数包含需要更新的模板ID以及需要删除的对象。

|Parameter 参数       T|pe 类型                  Des|ription 说明|
|------------------------|------------------------------|--------------|
|**templateids**<br>(required 必须)|string/array 字符串/数组   IDs|f the templates to be updated. 将要更新的模板ID。|
|groupids|string/array 字符串/数组   Host|groups to remove the given templates from. 从指定的模板中删除主机组。|
|hostids|string/array 字符串/数组   Host|or templates to unlink the given templates from (downstream). 从主机或模板中取消指定模板（下游）的链接。|
|macros|string/array 字符串/数组   User|macros to delete from the given templates. 删除指定模板的用户宏。|
|templateids\_clear|string/array 字符串/数组   Temp|ates to unlink and clear from the given templates (upstream). 从指定模板（上游）中取消模板链接并清除数据。|
|templateids\_link|string/array 字符串/数组   Temp|ates to unlink from the given templates (upstream). 从指定模板（上游）中取消模板链接。|

[comment]: # ({/new-bd7f8b44})

[comment]: # ({new-dcba01c8})
### Return values 返回值

`(object)` Returns an object containing the IDs of the updated templates
under the `templateids` property.
`(object)`返回一个对象，此对象包含在`templateids`中已更新模板的ID。

[comment]: # ({/new-dcba01c8})

[comment]: # ({new-b41637d2})
### Examples 范例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-20c1ddc2})
#### Removing templates from a group 从组中删除模板

Remove two templates from group "2". 从ID为"2"的组中删除两个模板。

Request 请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "template.massremove",
    "params": {
        "templateids": [
            "10085",
            "10086"
        ],
        "groupids": "2"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response 响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "templateids": [
            "10085",
            "10086"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-20c1ddc2})

[comment]: # ({new-6459bc33})
#### Unlinking templates from a host

Unlink templates "10106" and "10104" from template "10085".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "template.massremove",
    "params": {
        "templateids": "10085",
        "templateids_link": [
            "10106",
            "10104"
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "templateids": [
            "10085"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-6459bc33})


[comment]: # ({new-186aec66})
### See also 参考

-   [template.update](update)
-   [User
    macro](/zh/manual/api/reference/usermacro/object#hosttemplate_level_macro)

[comment]: # ({/new-186aec66})

[comment]: # ({new-aba9c6f9})
### Source 源码

CTemplate::massRemove() in
*frontends/php/include/classes/api/services/CTemplate.php*.
CTemplate::massRemove()方法可在*frontends/php/include/classes/api/services/CTemplate.php*中参考。

[comment]: # ({/new-aba9c6f9})
