[comment]: # translation:outdated

[comment]: # ({new-a7242998})
# 删除

[comment]: # ({/new-a7242998})

[comment]: # ({new-484b4ad2})
### Description 说明

`object template.delete(array templateIds)`

This method allows to delete templates. 此方法允许删除模板。

[comment]: # ({/new-484b4ad2})

[comment]: # ({new-1fbebaa3})
### Parameters 参数

`(array)` IDs of the templates to delete. `(array)`需要删除的模板ID。

[comment]: # ({/new-1fbebaa3})

[comment]: # ({new-fdd33aa6})
### Return values 返回值

`(object)` Returns an object containing the IDs of the deleted templates
under the `templateids` property.
`(object)`返回一个对象，包含`templateids`属性中被删除模板的ID。

[comment]: # ({/new-fdd33aa6})

[comment]: # ({new-b41637d2})
### Examples 范例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-3c8960bb})
#### Deleting multiple templates 删除多个模板

Delete two templates. 删除两个模板。

Request 请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "template.delete",
    "params": [
        "13",
        "32"
    ],
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response 响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "templateids": [
            "13",
            "32"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-3c8960bb})

[comment]: # ({new-bfde186e})
### Source 源码

CTemplate::delete() in
*frontends/php/include/classes/api/services/CTemplate.php*.
CTemplate::delete()方法可在*frontends/php/include/classes/api/services/CTemplate.php*中参考。

[comment]: # ({/new-bfde186e})
