[comment]: # translation:outdated

[comment]: # ({new-c71a2e54})
# 更新

[comment]: # ({/new-c71a2e54})

[comment]: # ({new-b9562236})
### 描述

`object correlation.update(object/array correlations)`

这个方法允许更新已存在的`联系`。

[comment]: # ({/new-b9562236})

[comment]: # ({new-326bc80e})
### 参数

`(object/array)` 要更新的`联系`的属性。

必须为每个`联系`定义 `correlationid`
属性，其它的属性都是可选的。只有传递的属性会被更新，其它属性都将保持不变。

另外，对于[标准联系属性](object#correlation)，该方法接受以下参数。

|参数         类|描述|<|
|------------------|------|-|
|filter|对象   替|当前筛选的联系筛选对象。|
|operations|数组   替|已存在的操作的联系操作。|

[comment]: # ({/new-326bc80e})

[comment]: # ({new-13e1dc13})
### 返回值

`(object)` 返回一个对象，该对象包含 “correlationids”
属性下更新的`联系`的 ID。

[comment]: # ({/new-13e1dc13})

[comment]: # ({new-b41637d2})
### 示例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-67bb2dfb})
#### 禁用联系

请求：

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "correlation.update",
    "params": {
        "correlationid": "1",
        "status": "1"
    },
    "auth": "343baad4f88b4106b9b5961e77437688",
    "id": 1
}
```

响应：

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "correlationids": [
            "1"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-67bb2dfb})

[comment]: # ({new-d6ef1778})
#### 替代条件，但评估方法不变

请求：

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "correlation.update",
    "params": {
        "correlationid": "1",
        "filter": {
            "conditions": [
                {
                    "type": 3,
                    "oldtag": "error",
                    "newtag": "ok"
                }
            ]
        }
    },
    "auth": "343baad4f88b4106b9b5961e77437688",
    "id": 1
}
```

响应：

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "correlationids": [
            "1"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-d6ef1778})

[comment]: # ({new-e7d15410})
### 参见

-   [联系过滤](object#correlation_filter)
-   [联系操作](object#correlation_operation)

[comment]: # ({/new-e7d15410})

[comment]: # ({new-40fa665f})
### 来源

CCorrelation::update() in
*frontends/php/include/classes/api/services/CCorrelation.php*.

[comment]: # ({/new-40fa665f})
