[comment]: # translation:outdated

[comment]: # ({new-7416ad19})
# 获取

[comment]: # ({/new-7416ad19})

[comment]: # ({new-858936aa})
### 描述

`integer/array correlation.get(object parameters)`

这个方法允许根据给定的参数检索`联系`。

[comment]: # ({/new-858936aa})

[comment]: # ({new-06a650a0})
### 参数

`(object)` 定义需要输出的参数。

这个方法支持以下参数。

|参数                     类|描述|<|
|------------------------------|------|-|
|correlationids|字符串/数组   只返回拥|给定 ID 的`联系`。|
|selectFilter|查询          返|`filter` 属性中的联系过滤。|
|selectOperations|查询          返|`operations` 属性中的联系操作。|
|sortfield|字符串/数组   根据给定|属性对结果进行排序。<br><br>可能的值有： `correlationid`，`name` 和 `status`。|
|countOutput|布尔值        在[|用评论](/manual/api/reference_commentary#common_get_method_parameters)中详细描述了所有 `get` 方法的常见参数。|
|editable|布尔值        ::|<|
|excludeSearch|布尔值        ::|<|
|filter|对象          :|:|
|limit|整数          :|:|
|output|查询          :|:|
|preservekeys|布尔值        ::|<|
|search|对象          :|:|
|searchByAny|布尔值        ::|<|
|searchWildcardsEnabled|布尔值        ::|<|
|sortorder|字符串/数组   :::|<|
|startSearch|布尔值        ::|<|

[comment]: # ({/new-06a650a0})

[comment]: # ({new-7223bab1})
### 返回值

`(integer/array)` 返回：

-   一个对象数组；
-   如果使用了 `countOutput` 参数，被检索的对象的数量。

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### 示例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-d14c4f20})
#### 检索联系

检索所有具有相关条件和操作的已配置过的`联系`。过滤使用 “AND/OR”
的评估类型，因此 `formula` 属性为空，且 `eval_formula` 将自动生成。

请求：

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "correlation.get",
    "params": {
        "output": "extend",
        "selectOperations": "extend",
        "selectFilter": "extend"
    },
    "auth": "343baad4f88b4106b9b5961e77437688",
    "id": 1
}
```

响应：

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "correlationid": "1",
            "name": "Correlation 1",
            "description": "",
            "status": "0",
            "filter": {
                "evaltype": "0",
                "formula": "",
                "conditions": [
                    {
                        "type": "3",
                        "oldtag": "error",
                        "newtag": "ok",
                        "formulaid": "A"
                    }
                ],
                "eval_formula": "A"
            },
            "operations": [
                {
                    "type": "0"
                }
            ]
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-d14c4f20})

[comment]: # ({new-e7d15410})
### 参见

-   [联系过滤](object#correlation_filter)
-   [联系操作](object#correlation_operation)

[comment]: # ({/new-e7d15410})

[comment]: # ({new-bbab04bd})
### 来源

CCorrelation::get() in
*frontends/php/include/classes/api/services/CCorrelation.php*.

[comment]: # ({/new-bbab04bd})
