[comment]: # translation:outdated

[comment]: # ({new-89029829})
# 删除

[comment]: # ({/new-89029829})

[comment]: # ({new-4922e104})
### 描述

`object correlation.delete(array correlationids)`

这个方法允许删除联系。

[comment]: # ({/new-4922e104})

[comment]: # ({new-b445698c})
### 参数

`(array)` 要删除的`联系`的 ID。

[comment]: # ({/new-b445698c})

[comment]: # ({new-855af217})
### 返回值

`(object)` 返回一个对象，该对象包含 “correlationids”
属性下删除的`联系`的 ID。

[comment]: # ({/new-855af217})

[comment]: # ({new-c9f65268})
### 示例

[comment]: # ({/new-c9f65268})

[comment]: # ({new-bfc49991})
#### 删除多个联系

删除 2 个联系。

请求：

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "correlation.delete",
    "params": [
        "1",
        "2"
    ],
    "auth": "343baad4f88b4106b9b5961e77437688",
    "id": 1
}
```

响应：

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "correlaionids": [
            "1",
            "2"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-bfc49991})

[comment]: # ({new-b0713c0f})
### 来源

CCorrelation::delete() in
*frontends/php/include/classes/api/services/CCorrelation.php*.

[comment]: # ({/new-b0713c0f})
