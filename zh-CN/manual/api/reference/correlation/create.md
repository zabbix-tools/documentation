[comment]: # translation:outdated

[comment]: # ({new-fbeccd23})
# 创建

[comment]: # ({/new-fbeccd23})

[comment]: # ({new-c06fb521})
### 描述

`object correlation.create(object/array correlations)`

这种方法允许创建新的`联系`。

[comment]: # ({/new-c06fb521})

[comment]: # ({new-8a55e636})
### 参数

`(object/array)` 要创建的`联系`。

另外，对于[标准联系属性](object#correlation)，该方法还接受以下参数。

|参数              类|描述|<|
|-----------------------|------|-|
|**operations**<br>（需要的）|数组   与|建`联系`相关的操作。|
|**filter**<br>（需要的）|对象   与|联系`相关的过滤对象。|

[comment]: # ({/new-8a55e636})

[comment]: # ({new-88660193})
### 返回值

`(object)` 返回一个对象，该对象包含 “correlationids”
属性下创建的`联系`的 ID。返回的 ID 的顺序与所传递的`联系`的顺序相匹配。

[comment]: # ({/new-88660193})

[comment]: # ({new-b41637d2})
### 示例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-7325d5f8})
#### 创建一个新的事件标签联系

使用具有一个条件和一个操作的评估方法 `AND/OR`
创建一个`联系`。默认情况下，这个`联系`将被启用。

请求：

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "correlation.create",
    "params": {
        "name": "new event tag correlation",
        "filter": {
            "evaltype": 0,
            "conditions": [
                {
                    "type": 1,
                    "tag": "ok"
                }
            ]
        },
        "operations": [
            {
                "type": 0
            }
        ]
    },
    "auth": "343baad4f88b4106b9b5961e77437688",
    "id": 1
}
```

响应：

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "correlationids": [
            "1"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-7325d5f8})

[comment]: # ({new-a12df7f6})
#### 使用一个自定义表达式过滤

创建使用自定义筛选条件的`联系`。公式 id A 或 B
是任意选择的。条件类型为“主机组”，操作符为 “<>”。

请求：

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "correlation.create",
    "params": {
        "name": "new host group correlation",
        "description": "a custom description",
        "status": 0,
        "filter": {
            "evaltype": 3,
            "formula": "A or B",
            "conditions": [
                {
                    "type": 2,
                    "operator": 1,
                    "formulaid": "A"
                },
                {
                    "type": 2,
                    "operator": 1,
                    "formulaid": "B"
                }
            ]
        },
        "operations": [
            {
                "type": 1
            }
        ]
    },
    "auth": "343baad4f88b4106b9b5961e77437688",
    "id": 1
}
```

响应：

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "correlationids": [
            "2"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-a12df7f6})

[comment]: # ({new-e7d15410})
### 参见

-   [联系过滤](object#correlation_filter)
-   [联系操作](object#correlation_operation)

[comment]: # ({/new-e7d15410})

[comment]: # ({new-934ba89b})
### 来源

CCorrelation::create() in
*frontends/php/include/classes/api/services/CCorrelation.php*.

[comment]: # ({/new-934ba89b})
