[comment]: # translation:outdated

[comment]: # ({new-06845577})
# Action 动作

这个类用于操作动作。

对象引用:\

-   [动作](/zh/manual/api/reference/action/object#action)
-   [触发动作需要的条件](/zh/manual/api/reference/action/object#action_condition)
-   [动作触发后联动的操作](/zh/manual/api/reference/action/object#action_operation)

相关方法:\

-   [action.create](/zh/manual/api/reference/action/create) -
    创建新的动作
-   [action.delete](/zh/manual/api/reference/action/delete) - 删除动作
-   [action.get](/zh/manual/api/reference/action/get) - 检索动作
-   [action.update](/zh/manual/api/reference/action/update) - 更新动作

This class is designed to work with actions.

Object references:\

-   [Action](/manual/api/reference/action/object#action)
-   [Action
    condition](/manual/api/reference/action/object#action_condition)
-   [Action
    operation](/manual/api/reference/action/object#action_operation)

Available methods:\

-   [action.create](/manual/api/reference/action/create) - create new
    actions
-   [action.delete](/manual/api/reference/action/delete) - delete
    actions
-   [action.get](/manual/api/reference/action/get) - retrieve actions
-   [action.update](/manual/api/reference/action/update) - update
    actions

[comment]: # ({/new-06845577})
