[comment]: # translation:outdated

[comment]: # ({new-ca37dbd6})
# 任务

This class is designed to work with tasks. 此类用于管理任务。

Available methods: 可用方法：\

-   [task.create](/zh/manual/api/reference/task/create) - creating new
    tasks 创建新的任务。

[comment]: # ({/new-ca37dbd6})
