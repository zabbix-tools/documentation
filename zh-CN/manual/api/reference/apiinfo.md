[comment]: # translation:outdated

[comment]: # ({new-7fceb632})
# API 信息

这个类用于检索 API 相关信息

相关方法:\

-   [apiinfo.version](/zh/manual/api/reference/apiinfo/version) - 获取
    Zabbix API 版本

[comment]: # ({/new-7fceb632})
