[comment]: # translation:outdated

[comment]: # ({new-b1a4e412})
# 联系

这个类是设计用于`联系`。

对象引用：\

-   [联系](/manual/api/reference/correlation/object#correlation)

可用的方法：\

-   [correlation.create](/manual/api/reference/correlation/create) -
    创建新的联系
-   [correlation.delete](/manual/api/reference/correlation/delete) -
    删除联系
-   [correlation.get](/manual/api/reference/correlation/get) - 获取联系
-   [correlation.update](/manual/api/reference/correlation/update) -
    更新联系

[comment]: # ({/new-b1a4e412})
