[comment]: # translation:outdated

[comment]: # ({new-a68f24c8})
# Alert-告警

这个对象用于告警模块。

对象引用:\

-   [Alert](/zh/manual/api/reference/alert/object#alert)

相关方法::\

-   [alert.get](/zh/manual/api/reference/alert/get) - 获取告警

This class is designed to work with alerts.

Object references:\

-   [Alert](/manual/api/reference/alert/object#alert)

Available methods:\

-   [alert.get](/manual/api/reference/alert/get) - retrieve alerts

[comment]: # ({/new-a68f24c8})
