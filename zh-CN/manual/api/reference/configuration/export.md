[comment]: # translation:outdated

[comment]: # ({new-690a015b})
# configuration.export

[comment]: # ({/new-690a015b})

[comment]: # ({new-98d36809})
### 说明

`string configuration.export(object parameters)`

此方法允许将配置数据导出并序列化为字符串。

[comment]: # ({/new-98d36809})

[comment]: # ({new-ef6ad2c8})
### 参数

`(object)` 参数定义了导出的对象以及使用的格式。

|参数           类|说明|<|
|--------------------|------|-|
|**format**<br>(必须)|string<br>|导出数据的格式。<br>可能的值为:<br>`json` - JSON;<br>`xml` - XML.|
|**options**<br>(必须)|object<br>|导出的对象。<br>`options` 对象有以下参数<br>`groups` - `(array)` 主机组 ID 的导出;<br>`hosts` - `(array)` 主机 ID 的导出;<br>`images` - `(array)` 图表 ID 的导出;<br>`maps` - `(array)` 拓扑图 ID 的导出.<br>`screens` - `(array)` 屏幕 ID 的导出;<br>`templates` - `(array)` 模板 ID 的导出;<br>`valueMaps` - `(array)` 值映射 ID 的导出;<br>|

[comment]: # ({/new-ef6ad2c8})

[comment]: # ({new-0bfd9762})
### 返回值

`(string)` 返回一个包含请求配置数据的序列化字符串

[comment]: # ({/new-0bfd9762})

[comment]: # ({new-b41637d2})
### 范例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-5c446795})
#### 导出一个主机

导出一个 XML 字符串的主机配置。

请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "configuration.export",
    "params": {
        "options": {
            "hosts": [
                "10161"
            ]
        },
        "format": "xml"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<zabbix_export><version>4.0</version><date>2018-03-29T06:54:34Z</date><groups><group><name>Zabbix servers</name></group></groups><hosts><host><host>Export host</host><name>Export host</name><description/><proxy/><status>0</status><ipmi_authtype>-1</ipmi_authtype><ipmi_privilege>2</ipmi_privilege><ipmi_username/><ipmi_password/><tls_connect>1</tls_connect><tls_accept>1</tls_accept><tls_issuer/><tls_subject/><tls_psk_identity/><tls_psk/><templates/><groups><group><name>Zabbix servers</name></group></groups><interfaces><interface><default>1</default><type>1</type><useip>1</useip><ip>127.0.0.1</ip><dns/><port>10050</port><bulk>1</bulk><interface_ref>if1</interface_ref></interface></interfaces><applications><application><name>Application</name></application></applications><items><item><name>Item</name><type>0</type><snmp_community/><snmp_oid/><key>item.key</key><delay>30s</delay><history>90d</history><trends>365d</trends><status>0</status><value_type>3</value_type><allowed_hosts/><units/><snmpv3_contextname/><snmpv3_securityname/><snmpv3_securitylevel>0</snmpv3_securitylevel><snmpv3_authprotocol>0</snmpv3_authprotocol><snmpv3_authpassphrase/><snmpv3_privprotocol>0</snmpv3_privprotocol><snmpv3_privpassphrase/><params/><ipmi_sensor/><authtype>0</authtype><username/><password/><publickey/><privatekey/><port/><description/><inventory_link>0</inventory_link><applications><application><name>Application</name></application></applications><valuemap><name>Host status</name></valuemap><logtimefmt/><preprocessing/><jmx_endpoint/><timeout>3s</timeout><url/><query_fields/><posts/><status_codes>200</status_codes><follow_redirects>1</follow_redirects><post_type>0</post_type><http_proxy/><headers/><retrieve_mode>0</retrieve_mode><request_method>1</request_method><output_format>0</output_format><allow_traps>0</allow_traps><ssl_cert_file/><ssl_key_file/><ssl_key_password/><verify_peer>0</verify_peer><verify_host>0</verify_host><master_item/><interface_ref>if1</interface_ref></item></items><discovery_rules/><httptests/><macros/><inventory/></host></hosts><value_maps><value_map><name>Host status</name><mappings><mapping><value>0</value><newvalue>Up</newvalue></mapping><mapping><value>2</value><newvalue>Unreachable</newvalue></mapping></mappings></value_map></value_maps></zabbix_export>\n",
    "id": 1
}
```

[comment]: # ({/new-5c446795})

[comment]: # ({new-a3a5fdbf})
### 来源

CConfiguration::export() in
*frontends/php/include/classes/api/services/CConfiguration.php*.

### Description

`string configuration.export(object parameters)`

This method allows to export configuration data as a serialized string.

### Parameters

`(object)` Parameters defining the objects to be exported and the format
to use.

|Parameter|Type|Description|
|---------|----|-----------|
|**format**<br>(required)|string|Format in which the data must be exported.<br><br>Possible values:<br>`json` - JSON;<br>`xml` - XML.|
|**options**<br>(required)|object|Objects to be exported.<br><br>The `options` object has the following parameters:<br>`groups` - `(array)` IDs of host groups to export;<br>`hosts` - `(array)` IDs of hosts to export;<br>`images` - `(array)` IDs of images to export;<br>`maps` - `(array)` IDs of maps to export.<br>`screens` - `(array)` IDs of screens to export;<br>`templates` - `(array)` IDs of templates to export;<br>`valueMaps` - `(array)` IDs of value maps to export;<br>|

### Return values

`(string)` Returns a serialized string containing the requested
configuration data.

### Examples

#### Exporting a host

Export the configuration of a host as an XML string.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "configuration.export",
    "params": {
        "options": {
            "hosts": [
                "10161"
            ]
        },
        "format": "xml"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<zabbix_export><version>4.0</version><date>2018-03-29T06:54:34Z</date><groups><group><name>Zabbix servers</name></group></groups><hosts><host><host>Export host</host><name>Export host</name><description/><proxy/><status>0</status><ipmi_authtype>-1</ipmi_authtype><ipmi_privilege>2</ipmi_privilege><ipmi_username/><ipmi_password/><tls_connect>1</tls_connect><tls_accept>1</tls_accept><tls_issuer/><tls_subject/><tls_psk_identity/><tls_psk/><templates/><groups><group><name>Zabbix servers</name></group></groups><interfaces><interface><default>1</default><type>1</type><useip>1</useip><ip>127.0.0.1</ip><dns/><port>10050</port><bulk>1</bulk><interface_ref>if1</interface_ref></interface></interfaces><applications><application><name>Application</name></application></applications><items><item><name>Item</name><type>0</type><snmp_community/><snmp_oid/><key>item.key</key><delay>30s</delay><history>90d</history><trends>365d</trends><status>0</status><value_type>3</value_type><allowed_hosts/><units/><snmpv3_contextname/><snmpv3_securityname/><snmpv3_securitylevel>0</snmpv3_securitylevel><snmpv3_authprotocol>0</snmpv3_authprotocol><snmpv3_authpassphrase/><snmpv3_privprotocol>0</snmpv3_privprotocol><snmpv3_privpassphrase/><params/><ipmi_sensor/><authtype>0</authtype><username/><password/><publickey/><privatekey/><port/><description/><inventory_link>0</inventory_link><applications><application><name>Application</name></application></applications><valuemap><name>Host status</name></valuemap><logtimefmt/><preprocessing/><jmx_endpoint/><timeout>3s</timeout><url/><query_fields/><posts/><status_codes>200</status_codes><follow_redirects>1</follow_redirects><post_type>0</post_type><http_proxy/><headers/><retrieve_mode>0</retrieve_mode><request_method>1</request_method><output_format>0</output_format><allow_traps>0</allow_traps><ssl_cert_file/><ssl_key_file/><ssl_key_password/><verify_peer>0</verify_peer><verify_host>0</verify_host><master_item/><interface_ref>if1</interface_ref></item></items><discovery_rules/><httptests/><macros/><inventory/></host></hosts><value_maps><value_map><name>Host status</name><mappings><mapping><value>0</value><newvalue>Up</newvalue></mapping><mapping><value>2</value><newvalue>Unreachable</newvalue></mapping></mappings></value_map></value_maps></zabbix_export>\n",
    "id": 1
}
```

### Source

CConfiguration::export() in
*frontends/php/include/classes/api/services/CConfiguration.php*.

[comment]: # ({/new-a3a5fdbf})
