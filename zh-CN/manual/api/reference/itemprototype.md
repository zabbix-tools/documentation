[comment]: # translation:outdated

[comment]: # ({new-7e2bad53})
# Item prototype 监控项原型

## Item 原型

This class is designed to work with item prototypes.
此类旨辅助Item原型的使用。

Object references:\
对象引用\

-   [Item
    prototype](/zh/manual/api/reference/itemprototype/object#item_prototype)

Available methods:\
可用方法：\

-   [itemprototype.create](/manual/api/reference/itemprototype/create) -
    creating new item prototypes
-   [itemprototype.create](/manual/api/reference/itemprototype/create) -
    创建新监控项原型
-   [itemprototype.delete](/manual/api/reference/itemprototype/delete) -
    deleting item prototypes
-   [itemprototype.delete](/manual/api/reference/itemprototype/delete) -
    删除监控项原型
-   [itemprototype.get](/manual/api/reference/itemprototype/get) -
    retrieving item prototypes
-   [itemprototype.get](/manual/api/reference/itemprototype/get) -
    获取监控项原型
-   [itemprototype.update](/manual/api/reference/itemprototype/update) -
    updating item prototypes
-   [itemprototype.update](/manual/api/reference/itemprototype/update) -
    更新监控项原型

[comment]: # ({/new-7e2bad53})
