[comment]: # translation:outdated

[comment]: # ({new-14213bb4})
# 发现服务

这个类被设计用于`发现服务`。

对象引用：\

-   [发现服务](/manual/api/reference/dservice/object#discovered_service)

可用的方法：\

-   [dservice.get](/manual/api/reference/dservice/get) -
    获取已发现的服务。

[comment]: # ({/new-14213bb4})
