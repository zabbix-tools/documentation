[comment]: # translation:outdated

[comment]: # ({new-edb56cfa})
# usermacro.delete

[comment]: # ({/new-edb56cfa})

[comment]: # ({new-5eb2a780})
### 说明

`object usermacro.delete(array hostMacroIds)`

[comment]: # ({/new-5eb2a780})

[comment]: # ({new-dd1b319f})
### Description

`object usermacro.delete(array hostMacroIds)`

This method allows to delete host macros.

[comment]: # ({/new-dd1b319f})

[comment]: # ({new-cd694080})
### 参数

`(array)` 要删除的主机宏的ID。

[comment]: # ({/new-cd694080})

[comment]: # ({new-b41637d2})
### Parameters

`(array)` IDs of the host macros to delete.

[comment]: # ({/new-b41637d2})

[comment]: # ({new-b2f5bee1})
### 返回值

`(object)`返回一个包含“hostMacs”属性下删除的主机宏ID的对象。

[comment]: # ({/new-b2f5bee1})

[comment]: # ({new-9d01c95a})
### Return values

`(object)` Returns an object containing the IDs of the deleted host
macros under the `hostmacroids` property.

### 示例

### Examples

#### 删除多个主机宏

删除2个主机宏

#### Deleting multiple host macros

Delete two host macros.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "usermacro.delete",
    "params": [
        "32",
        "11"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "hostmacroids": [
            "32",
            "11"
        ]
    },
    "id": 1
}
```

### 来源

CUserMacro::delete() in
*frontends/php/include/classes/api/services/CUserMacro.php*.

[comment]: # ({/new-9d01c95a})
