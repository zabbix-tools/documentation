[comment]: # translation:outdated

[comment]: # ({new-02ea8022})
# usermacro.get

[comment]: # ({/new-02ea8022})

[comment]: # ({new-28befc82})
### 说明

`integer/array usermacro.get(object parameters)`
该方法允许根据给定的参数检索主机和全局宏。

[comment]: # ({/new-28befc82})

[comment]: # ({new-141835f9})
### Description

`integer/array usermacro.get(object parameters)`

The method allows to retrieve host and global macros according to the
given parameters.

[comment]: # ({/new-141835f9})

[comment]: # ({new-7223bab1})
### 参数

`(object)` 定义所需输出的参数。 该方法支持以下参数。

|属性                     类|说明|<|
|------------------------------|------|-|
|globalmacro|flag|返回全局宏而不是主机宏。|
|globalmacroids|string/array|仅返回具有给定ID的全局宏。|
|groupids|string/array|只返回属于主机的主机宏或来自给定主机组的模板。|
|hostids|string/array|仅返回属于给定主机的主机宏。|
|hostmacroids|string/array|只返回具有给定ID的主机宏。|
|templateids|string/array|只返回属于给定模板的主机宏。|
|selectGroups|query|在`groups`属性中返回主机宏所属的主机组。<br><br>仅在检索主机宏时使用。|
|selectHosts|query|在`hosts`属性中返回主机宏所属的主机。<br><br>仅在检索主机宏时使用。|
|selectTemplates|query|在`template`属性中返回主机宏所属的模板。<br><br>仅在检索主机宏时使用。|
|sortfield|string/array|按照给定的属性对结果进行排序。<br><br>可能的值：`macro`。|
|countOutput|boolean|这些参数对于所有的“获取”方法是常见的，在页 [参考评论](/manual/api/reference_commentary#common_get_method_parameters) page. 中有详细描述。|
|editable|boolean|^|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|Type|Description|
|---------|----|-----------|
|globalmacro|flag|Return global macros instead of host macros.|
|globalmacroids|string/array|Return only global macros with the given IDs.|
|groupids|string/array|Return only host macros that belong to hosts or templates from the given host groups.|
|hostids|string/array|Return only macros that belong to the given hosts or templates.|
|hostmacroids|string/array|Return only host macros with the given IDs.|
|selectGroups|query|Return host groups that the host macro belongs to in the `groups` property.<br><br>Used only when retrieving host macros.|
|selectHosts|query|Return hosts that the host macro belongs to in the `hosts` property.<br><br>Used only when retrieving host macros.|
|selectTemplates|query|Return templates that the host macro belongs to in the `templates` property.<br><br>Used only when retrieving host macros.|
|sortfield|string/array|Sort the result by the given properties.<br><br>Possible value: `macro`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters) page.|
|editable|boolean|^|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

[comment]: # ({/new-b41637d2})

[comment]: # ({new-7089e052})
### 返回值

`(integer/array)` 返回：

-   一组对象;
-   如果已经使用“countOutput”参数，则检索到的对象的计数。

[comment]: # ({/new-7089e052})

[comment]: # ({new-29b64a7a})
### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.

[comment]: # ({/new-29b64a7a})

[comment]: # ({new-26db0ec2})
### 示例

### Examples

#### 检索主机的主机宏

检索主机 "10198" 定义的所有主机宏。

#### Retrieving host macros for a host

Retrieve all host macros defined for host "10198".

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "usermacro.get",
    "params": {
        "output": "extend",
        "hostids": "10198"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "hostmacroid": "9",
            "hostid": "10198",
            "macro": "{$INTERFACE}",
            "value": "eth0"
        },
        {
            "hostmacroid": "11",
            "hostid": "10198",
            "macro": "{$SNMP_COMMUNITY}",
            "value": "public"
        }
    ],
    "id": 1
}
```

#### 检索全局宏

检索所有全局宏。

#### Retrieving global macros

Retrieve all global macros.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "usermacro.get",
    "params": {
        "output": "extend",
        "globalmacro": true
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "globalmacroid": "6",
            "macro": "{$SNMP_COMMUNITY}",
            "value": "public"
        }
    ],
    "id": 1
}
```

### 来源

CUserMacro::get() in
*frontends/php/include/classes/api/services/CUserMacro.php*.

[comment]: # ({/new-26db0ec2})
