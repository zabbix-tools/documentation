[comment]: # translation:outdated

[comment]: # ({new-e91a170d})
# usermacro.deleteglobal

[comment]: # ({/new-e91a170d})

[comment]: # ({new-f33727ba})
### 说明

`object usermacro.deleteglobal(array globalMacroIds)`
此方法允许删除全局宏。

[comment]: # ({/new-f33727ba})

[comment]: # ({new-0a9d3dbc})
### Description

`object usermacro.deleteglobal(array globalMacroIds)`

This method allows to delete global macros.

[comment]: # ({/new-0a9d3dbc})

[comment]: # ({new-30415b7d})
### 参数

`(array)` 要删除的全局宏的ID。

[comment]: # ({/new-30415b7d})

[comment]: # ({new-b41637d2})
### Parameters

`(array)` IDs of the global macros to delete.

[comment]: # ({/new-b41637d2})

[comment]: # ({new-b377e629})
### 返回值

`(object)` 返回包含“globalmacroids”属性下删除的全局宏ID的对象。

[comment]: # ({/new-b377e629})

[comment]: # ({new-331322ff})
### Return values

`(object)` Returns an object containing the IDs of the deleted global
macros under the `globalmacroids` property.

### 示例

### Examples

#### 删除多个全局宏

删除2个主机宏。

#### Deleting multiple global macros

Delete two global macros.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "usermacro.deleteglobal",
    "params": [
        "32",
        "11"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "globalmacroids": [
            "32",
            "11"
        ]
    },
    "id": 1
}
```

### 来源

CUserMacro::deleteGlobal() in
*frontends/php/include/classes/api/services/CUserMacro.php*.

[comment]: # ({/new-331322ff})
