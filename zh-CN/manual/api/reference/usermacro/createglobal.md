[comment]: # translation:outdated

[comment]: # ({new-f1e165e2})
# usermacro.createglobal

[comment]: # ({/new-f1e165e2})

[comment]: # ({new-2655f9fe})
### 说明

`object usermacro.createglobal(object/array globalMacros)`

此方法允许创建新的全局宏。

[comment]: # ({/new-2655f9fe})

[comment]: # ({new-3bf123cd})
### Description

`object usermacro.createglobal(object/array globalMacros)`

This method allows to create new global macros.

[comment]: # ({/new-3bf123cd})

[comment]: # ({new-0b8f323a})
### 参数

`(object/array)`要创建的全局宏。

该方法接受具有[标准全局宏属性](object#global_macro).的全局宏。

[comment]: # ({/new-0b8f323a})

[comment]: # ({new-b41637d2})
### Parameters

`(object/array)` Global macros to create.

The method accepts global macros with the [standard global macro
properties](object#global_macro).

[comment]: # ({/new-b41637d2})

[comment]: # ({new-752894f1})
### 返回值

`(object)`返回包含`globalmacroids`属性下创建的全局宏的ID的对象。
返回的ID的顺序与传递的全局宏的顺序相匹配。

[comment]: # ({/new-752894f1})

[comment]: # ({new-7b4f640f})
### Return values

`(object)` Returns an object containing the IDs of the created global
macros under the `globalmacroids` property. The order of the returned
IDs matches the order of the passed global macros.

### 示例

### Examples

#### 创建一个全局宏

创建一个宏 "{$SNMP\_COMMUNITY}" 值为 "public".

#### Creating a global macro

Create a global macro "{$SNMP\_COMMUNITY}" with value "public".

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "usermacro.createglobal",
    "params":  {
        "macro": "{$SNMP_COMMUNITY}",
        "value": "public"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "globalmacroids": [
            "6"
        ]
    },
    "id": 1
}
```

### 来源

CUserMacro::createGlobal() in
*frontends/php/include/classes/api/services/CUserMacro.php*.

[comment]: # ({/new-7b4f640f})
