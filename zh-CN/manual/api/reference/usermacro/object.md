[comment]: # translation:outdated

[comment]: # ({new-28092341})
# > 用户宏对象

以下对象与“usermacro”API直接相关。

[comment]: # ({/new-28092341})

[comment]: # ({new-e654cd12})
### 全局宏

全局宏对象具有以下属性。

|属性            类|说明|<|
|---------------------|------|-|
|globalmacroid|string|*(readonly)* 全局宏的ID。|
|**macro**<br>(required)|string|宏字符串。|
|**value**<br>(required)|string|宏的价值。|

[comment]: # ({/new-e654cd12})

[comment]: # ({new-5a3a9f9f})
## > User macro object

The following objects are directly related to the `usermacro` API.

### Global macro

The global macro object has the following properties.

|Property|Type|Description|
|--------|----|-----------|
|globalmacroid|string|*(readonly)* ID of the global macro.|
|**macro**<br>(required)|string|Macro string.|
|**value**<br>(required)|string|Value of the macro.|

### 主机宏

主机宏对象定义主机或模板上可用的宏。 它具有以下属性。

|属性          类|说明|<|
|-------------------|------|-|
|hostmacroid|string|*(readonly)* 主机宏的ID。|
|**hostid**<br>(required)|string|宏所属主机的ID。|
|**macro**<br>(required)|string|宏字符串。|
|**value**<br>(required)|string|宏的值。|

### Host macro

The host macro object defines a macro available on a host or template.
It has the following properties.

|Property|Type|Description|
|--------|----|-----------|
|hostmacroid|string|*(readonly)* ID of the host macro.|
|**hostid**<br>(required)|string|ID of the host that the macro belongs to.|
|**macro**<br>(required)|string|Macro string.|
|**value**<br>(required)|string|Value of the macro.|

[comment]: # ({/new-5a3a9f9f})
