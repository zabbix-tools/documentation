[comment]: # translation:outdated

[comment]: # ({new-83b3ff24})
# usermacro.create

[comment]: # ({/new-83b3ff24})

[comment]: # ({new-db3d5c68})
### 说明

`object usermacro.create(object/array hostMacros)`

此方法允许创建新的主机宏。

[comment]: # ({/new-db3d5c68})

[comment]: # ({new-7c7df9b5})
### Description

`object usermacro.create(object/array hostMacros)`

This method allows to create new host macros.

[comment]: # ({/new-7c7df9b5})

[comment]: # ({new-bd0c3c49})
### 参数

`(object/array)`要创建的主机宏。

该方法接受有[标准主机宏属性](object#host_macro)的主机宏.

[comment]: # ({/new-bd0c3c49})

[comment]: # ({new-b41637d2})
### Parameters

`(object/array)` Host macros to create.

The method accepts host macros with the [standard host macro
properties](object#host_macro).

[comment]: # ({/new-b41637d2})

[comment]: # ({new-7c0da88a})
### 返回值

`(object)` 返回包含“hostMacroids”属性下创建的主机宏的ID的对象。
返回的ID的顺序与传递的主机宏的顺序相匹配。

[comment]: # ({/new-7c0da88a})

[comment]: # ({new-084b8f3c})
### Return values

`(object)` Returns an object containing the IDs of the created host
macros under the `hostmacroids` property. The order of the returned IDs
matches the order of the passed host macros.

### 示例

#### 创建主机宏

在主机"10198"创建主机宏 "{$SNMP\_COMMUNITY}"值为"public"。

#### Creating a host macro

Creat a host macro "{$SNMP\_COMMUNITY}" with the value "public" on host
"10198".

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "usermacro.create",
    "params": {
        "hostid": "10198",
        "macro": "{$SNMP_COMMUNITY}",
        "value": "public"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "hostmacroids": [
            "11"
        ]
    },
    "id": 1
}
```

### 来源

CUserMacro::create() in
*frontends/php/include/classes/api/services/CUserMacro.php*.

[comment]: # ({/new-084b8f3c})
