[comment]: # translation:outdated

[comment]: # ({new-2ed6e81f})
# usermacro.update

[comment]: # ({/new-2ed6e81f})

[comment]: # ({new-23e3a148})
### 说明

`object usermacro.update(object/array hostMacros)`

此方法允许更新现有的主机宏。

[comment]: # ({/new-23e3a148})

[comment]: # ({new-f8ce947c})
### Description

`object usermacro.update(object/array hostMacros)`

This method allows to update existing host macros.

[comment]: # ({/new-f8ce947c})

[comment]: # ({new-5e19fc38})
### 参数

`(object/array)` 要更新的[主机宏属性](object#host_macro)。

必须为每个主机宏定义`hostmacroid`属性，所有其他属性都是可选的。
只有通过的属性将被更新，所有其他属性将保持不变。

[comment]: # ({/new-5e19fc38})

[comment]: # ({new-b41637d2})
### Parameters

`(object/array)` [Host macro properties](object#host_macro) to be
updated.

The `hostmacroid` property must be defined for each host macro, all
other properties are optional. Only the passed properties will be
updated, all others will remain unchanged.

[comment]: # ({/new-b41637d2})

[comment]: # ({new-bc8ce678})
### 返回值

`(object)` 返回包含`hostMacroids`属性下更新的主机宏的ID的对象。

[comment]: # ({/new-bc8ce678})

[comment]: # ({new-129f453e})
#### Change macro value that was created by discovery rule

Convert discovery rule created "automatic" macro to "manual" and change its value to "new-value".

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "usermacro.update",
    "params": {
        "hostmacroid": "1",
        "value": "new-value",
        "automatic": "0"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "hostmacroids": [
            "1"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-129f453e})

[comment]: # ({new-2eddd7d2})
### Return values

`(object)` Returns an object containing the IDs of the updated host
macros under the `hostmacroids` property.

### 示例

### Examples

#### 更改主机宏的值

更改主机宏的值为 "public".

#### Changing the value of a host macro

Change the value of a host macro to "public".

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "usermacro.update",
    "params": {
        "hostmacroid": "1",
        "value": "public"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "hostmacroids": [
            "1"
        ]
    },
    "id": 1
}
```

### 来源

CUserMacro::update() in
*frontends/php/include/classes/api/services/CUserMacro.php*.

[comment]: # ({/new-2eddd7d2})
