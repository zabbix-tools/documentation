[comment]: # translation:outdated

[comment]: # ({new-a16741b3})
# usermacro.updateglobal

[comment]: # ({/new-a16741b3})

[comment]: # ({new-19b16b0b})
### 说明

`object usermacro.updateglobal(object/array globalMacros)`

此方法允许更新现有的全局宏。

[comment]: # ({/new-19b16b0b})

[comment]: # ({new-be7c969e})
### Description

`object usermacro.updateglobal(object/array globalMacros)`

This method allows to update existing global macros.

[comment]: # ({/new-be7c969e})

[comment]: # ({new-9c6d43ad})
### 参数

`(object/array)` 要更新的[全局宏属性](object#global_macro)。

必须为每个全局宏定义`globalmacroid`属性，所有其他属性都是可选的。
只有通过的属性将被更新，所有其他属性将保持不变。

[comment]: # ({/new-9c6d43ad})

[comment]: # ({new-b41637d2})
### Parameters

`(object/array)` [Global macro properties](object#global_macro) to be
updated.

The `globalmacroid` property must be defined for each global macro, all
other properties are optional. Only the passed properties will be
updated, all others will remain unchanged.

[comment]: # ({/new-b41637d2})

[comment]: # ({new-bd208b05})
### 返回值

`(object)` 返回包含“globalmacroids”属性下更新的全局宏的ID的对象。

[comment]: # ({/new-bd208b05})

[comment]: # ({new-85ca09bc})
### Return values

`(object)` Returns an object containing the IDs of the updated global
macros under the `globalmacroids` property.

### 示例

### Examples

#### 更改全局宏的值

将全局宏的值更改为“public”。

#### Changing the value of a global macro

Change the value of a global macro to "public".

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "usermacro.updateglobal",
    "params": {
        "globalmacroid": "1",
        "value": "public"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "globalmacroids": [
            "1"
        ]
    },
    "id": 1
}
```

### 来源

CUserMacro::updateGlobal() in
*frontends/php/include/classes/api/services/CUserMacro.php*.

[comment]: # ({/new-85ca09bc})
