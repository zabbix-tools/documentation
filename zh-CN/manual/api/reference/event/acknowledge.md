[comment]: # translation:outdated

[comment]: # ({new-2ccfd922})
# event.acknowledge

[comment]: # ({/new-2ccfd922})

[comment]: # ({new-eed7fdda})
### Description

[comment]: # ({/new-eed7fdda})

[comment]: # ({new-32560b88})
### 描述

`object event.acknowledge(object/array parameters)`
`对象 event.acknowledge(object/array parameters)`

This method allows to update events. Following update actions can be
performed:

-   Close event. If event is already resolved, this action will be
    skipped.
-   Acknowledge event. If event is already acknowledged, this action
    will be skipped.
-   Add message.
-   Change event severity. If event already has same severity, this
    action will be skipped.

此方法用于更新事件，可以执行以下更新操作:

-   关闭事件. 如果事件已经解决，此操作将会被跳过.
-   确认事件. 如果事件已经被确认，此操作将会被跳过.
-   增加消息.
-   更改事件严重等级. 如果事件已经拥有相同的严重等级，此操作将会被跳过.

::: noteimportant
Only trigger events can be updated.\
Only problem events can be updated.\
Read/Write rights for trigger are required to close the event or to
change event's severity.\
To close event, it should be allowed in trigger.
::: <note
important>只有触发器事件可以被更新.\
只有问题事件可以被更新.\
关闭事件或者更改事件的严重等级需要具有对触发器的读写权限.\
为了可以关闭事件，你应该在触发器中配置'允许手动关闭'.
:::

[comment]: # ({/new-32560b88})

[comment]: # ({new-94f3ab84})
### Parameters

[comment]: # ({/new-94f3ab84})

[comment]: # ({new-b41637d2})
### 参数

`(object/array)` Parameters containing the IDs of the events and update
operations that should be performed. `(对象/数组)`
包含事件ID和应执行的更新操作的参数.

|Parameter|Type|Description|
|---------|----|-----------|
|**eventids**<br>(required)|string/object|IDs of the events to acknowledge.|
|**action**<br>(required)|integer|Event update action(s). This is bitmask field, any combination of values are acceptable.<br><br>Possible values:<br>1 - close problem;<br>2 - acknowledge event;<br>4 - add message;<br>8 - change severity.|
|message|string|Text of the message.<br>**Required**, if action contains 'add message' flag.|
|severity|integer|New severity for events.<br>**Required**, if action contains 'change severity' flag.<br><br>Possible values:<br>0 - not classified;<br>1 - information;<br>2 - warning;<br>3 - average;<br>4 - high;<br>5 - disaster.|

|参数            类|描述|<|
|---------------------|------|-|
|**eventids**<br>(必选)|string/object|确认事件的ID.|
|**action**<br>(必选)|integer<br>|更新事件的操作. 这是位掩码字段，可接受以下任何值的组合.<br>可能值:<br>1 - 关闭问题;<br>2 - 确认事件;<br>4 - 增加消息;<br>8 - 更改严重等级.|
|message|string|消息文本.<br>如果操作包含'增加消息'标志，此选项**必选**.|
|severity|integer|事件的新的严重等级.<br>如果操作包含'更改严重等级'标志，此选项**必选**.<br><br>可能值:<br>0 - 未分类;<br>1 - 信息;<br>2 - 警告;<br>3 - 一般严重;<br>4 - 严重;<br>5 - 灾难.|

[comment]: # ({/new-b41637d2})

[comment]: # ({new-9e923e4c})
### Return values

[comment]: # ({/new-9e923e4c})

[comment]: # ({new-94e25d7e})
### 返回值

`(object)` Returns an object containing the IDs of the updated events
under the `eventids` property. `(对象)`
在`eventids`属性下，返回一个包含被更新事件的ID.

[comment]: # ({/new-94e25d7e})

[comment]: # ({new-aadd99f7})
### Examples

### 例子

#### Acknowledging an event

#### 确认一个事件

Acknowledge a single event and leave a message. 确认一个事件并留下消息

Request: 请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "event.acknowledge",
    "params": {
        "eventids": "20427",
        "action": 6,
        "message": "Problem resolved."
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response: 响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "eventids": [
            "20427"
        ]
    },
    "id": 1
}
```

#### Changing event's severity

#### 更改事件的严重等级

Change multiple event's severity and leave a message.
更改多个事件的严重等级并留下消息.

Request: 请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "event.acknowledge",
    "params": {
        "eventids": ["20427", "20428"],
        "action": 12,
        "message": "Maintenance required to fix it.",
        "severity": 4
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response: 响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "eventids": [
            "20427",
            "20428"
        ]
    },
    "id": 1
}
```

### Source

### 来源

CEvent::acknowledge() in
*frontends/php/include/classes/api/services/CEvent.php*.

[comment]: # ({/new-aadd99f7})
