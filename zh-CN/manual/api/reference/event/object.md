[comment]: # translation:outdated

[comment]: # ({new-bef38847})
# > Event object

The following objects are directly related to the `event` API.
以下对象与 `event[事件]` API直接相关

[comment]: # ({/new-bef38847})

[comment]: # ({new-355941f0})
### Event

[comment]: # ({/new-355941f0})

[comment]: # ({new-b89991b4})
### 事件

::: noteclassic
Events are created by the Zabbix server and cannot be
modified via the API.
::: 

::: noteclassic
事件是由Zabbix
server创建，并且不能通过API进行修改。
:::

The event object has the following properties. 事件对象具有以下属性：

|Property|Type|Description|
|--------|----|-----------|
|eventid|string|ID of the event.|
|source|integer|Type of the event.<br><br>Possible values:<br>0 - event created by a trigger;<br>1 - event created by a discovery rule;<br>2 - event created by active agent auto-registration;<br>3 - internal event.|
|object|integer|Type of object that is related to the event.<br><br>Possible values for trigger events:<br>0 - trigger.<br><br>Possible values for discovery events:<br>1 - discovered host;<br>2 - discovered service.<br><br>Possible values for auto-registration events:<br>3 - auto-registered host.<br><br>Possible values for internal events:<br>0 - trigger;<br>4 - item;<br>5 - LLD rule.|
|objectid|string|ID of the related object.|
|acknowledged|integer|Whether the event has been acknowledged.|
|clock|timestamp|Time when the event was created.|
|ns|integer|Nanoseconds when the event was created.|
|name|string|Resolved event name.|
|value|integer|State of the related object.<br><br>Possible values for trigger events:<br>0 - OK;<br>1 - problem.<br><br>Possible values for discovery events:<br>0 - host or service up;<br>1 - host or service down;<br>2 - host or service discovered;<br>3 - host or service lost.<br><br>Possible values for internal events:<br>0 - "normal" state;<br>1 - "unknown" or "not supported" state.<br><br>This parameter is not used for active agent auto-registration events.|
|severity|integer|Event current severity.<br><br>Possible values:<br>0 - not classified;<br>1 - information;<br>2 - warning;<br>3 - average;<br>4 - high;<br>5 - disaster.|
|r\_eventid|string|Recovery event ID|
|c\_eventid|string|Problem event ID who generated OK event|
|correlationid|string|Correlation ID|
|userid|string|User ID if the event was manually closed.|

|属性            类|描述|<|
|---------------------|------|-|
|eventid|string|事件的ID|
|source|integer|事件的类型<br><br>可能的值:<br>0 - 由触发器创建的事件;<br>1 - 由发现规则创建的事件;<br>2 - 活动代理自动注册的事件;<br>3 - 内部事件.|
|object|integer|与事件相关的对象类型.<br><br>触发器事件的可能值:<br>0 - 触发器.<br><br>发现事件的可能值:<br>1 - 发现主机;<br>2 - 发现服务.<br><br>自动注册事件的可能值:<br>3 - 自动注册的主机.<br><br>内部事件的可能值:<br>0 - 触发器;<br>4 - 监控项;<br>5 - 低级别发现(LLD)规则.|
|objectid|string|相关对象的ID.|
|acknowledged|integer|事件是否被确认.|
|clock|timestamp|事件的创建时间.|
|ns|integer|事件的创建时间(纳秒).|
|name|string|已解决事件的名称.|
|value|integer|相关对象的状态.<br><br>触发器事件的可能值:<br>0 - 正常;<br>1 - 异常.<br><br>发现事件的可能值:<br>0 - 主机或服务正常;<br>1 - 主机或服务故障;<br>2 - 主机或服务已发现;<br>3 - 主机或服务丢失.<br><br>内部事件的可能值:<br>0 - "正常"状态;<br>1 - "未知"或"不支持"状态.<br><br>此参数不用于活动代理自动注册事件.|
|severity|integer|当前事件的严重等级.<br><br>可能值:<br>0 - 未分类;<br>1 - 信息;<br>2 - 警告;<br>3 - 一般严重;<br>4 - 严重;<br>5 - 灾难.|
|r\_eventid|string|恢复事件的ID|
|c\_eventid|string|生成OK事件的问题事件ID|
|correlationid|string|关联ID|
|userid|string|手动关闭事件的用户的ID.|

[comment]: # ({/new-b89991b4})


[comment]: # ({new-52ff7df7})
### Media type URLs

Object with media type url have the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|name|string|Media type defined URL name.|
|url|string|Media type defined URL value.|

Results will contain entries only for active media types with enabled
event menu entry. Macro used in properties will be expanded, but if one
of properties contain non expanded macro both properties will be
excluded from results. Supported macros described on
[page](/manual/appendix/macros/supported_by_location).

[comment]: # ({/new-52ff7df7})
