[comment]: # translation:outdated

[comment]: # ({new-9b6159cd})
# 检查发现

这个类是设计用于`检查发现`。

对象引用：\

-   [检查发现](/manual/api/reference/dcheck/object#discovery_check)

可用的方法：\

-   [dcheck.get](/manual/api/reference/dcheck/get) - 获取检查发现。

[comment]: # ({/new-9b6159cd})
