[comment]: # translation:outdated

[comment]: # ({new-0ec30c22})
# item.update

[comment]: # ({/new-0ec30c22})

[comment]: # ({new-38961105})
### Description 说明

`object item.update(object/array items)`

This method allows to update existing items.
此方法允许更新已存在的监控项。

::: noteclassic
Web items cannot be updated via the Zabbix API.
WEB监控项不能通过Zabbix API更新。
:::

[comment]: # ({/new-38961105})

[comment]: # ({new-4e151fd6})
### Parameters 参数

`(object/array)` Item properties to be updated.
`(object/array)`要更新的监控项的属性。

The `itemid` property must be defined for each item, all other
properties are optional. Only the passed properties will be updated, all
others will remain unchanged.
每个的监控项的`itemid`属性必须被定义，其他属性可选。只有被传递的属性才会更新，其他所有属性保持不变。

Additionally to the [standard item properties](object#item), the method
accepts the following parameters. 另外见[standard item
properties](object#item)，此方法接受如下参数。

|Parameter|Type|Description|
|---------|----|-----------|
|applications|array|IDs of the applications to replace the current applications. 要替换当前应用的应用的ID。|
|preprocessing|array|Item preprocessing options to replace the current preprocessing options. 要替换的当前监控项预处理选项。|

[comment]: # ({/new-4e151fd6})

[comment]: # ({new-aefff750})
### Return values 返回值

`(object)` Returns an object containing the IDs of the updated items
under the `itemids` property. `(object)`
在`itemids`属性下返回已被更新的监控项的对象的IDs。

[comment]: # ({/new-aefff750})

[comment]: # ({new-b41637d2})
### Examples 示例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-5388578b})
#### Enabling an item 启用一个监控项

Enable an item, that is, set its status to "0".
启用一个监控项就是设置他的status属性为"0"。

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "item.update",
    "params": {
        "itemid": "10092",
        "status": 0
    },
    "auth": "700ca65537074ec963db7efabda78259",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "10092"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-5388578b})

[comment]: # ({new-0bf89382})
#### Update dependent item 更新依赖监控项

Update Dependent item name and Master item ID. Only dependencies on same
host are allowed, therefore Master and Dependent item should have same
hostid.
更新依赖监控项名称和主监控项的ID。只有同一个主机上的依赖监控项才允许，因此主监控项和依赖监控项应有相同的hostid。

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "item.update",
    "params": {
        "name": "Dependent item updated name",
        "master_itemid": "25562",
        "itemid": "189019"
    },
    "auth": "700ca65537074ec963db7efabda78259",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "189019"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-0bf89382})

[comment]: # ({new-8f365363})
#### Update HTTP agent item 更新 HTTP agent 监控项

Enable item value trapping. 启用监控项的trapping值。

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "item.update",
    "params": {
        "itemid": "23856",
        "allow_traps": "1"
    },
    "auth": "700ca65537074ec963db7efabda78259",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "23856"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-8f365363})

[comment]: # ({new-44f323b8})
### Source

CItem::update() in
*frontends/php/include/classes/api/services/CItem.php*.

[comment]: # ({/new-44f323b8})



[comment]: # ({new-dcfb98a0})
#### Updating a script item

Update a script item with a different script and remove unnecessary
parameters that were used by previous script.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "item.update",
    "params": {
        "itemid": "23865",
        "parameters": [],
        "script": "Zabbix.Log(3, 'Log test');\nreturn 1;"
    },
    "auth": "700ca65537074ec963db7efabda78259",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "23865"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-dcfb98a0})

[comment]: # ({new-646fe32a})
### Source

CItem::update() in *ui/include/classes/api/services/CItem.php*.

[comment]: # ({/new-646fe32a})
