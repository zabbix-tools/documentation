[comment]: # translation:outdated

[comment]: # ({new-cd0e20e4})
# item.delete

[comment]: # ({/new-cd0e20e4})

[comment]: # ({new-16951644})
### Description 说明

`object item.delete(array itemIds)`

This method allows to delete items. 此方法允许删除监控项。

::: noteclassic
Web items cannot be deleted via the Zabbix
API.WEB监控项不能通过Zabbix API删除。
:::

[comment]: # ({/new-16951644})

[comment]: # ({new-3574199c})
### Parameters 参数

`(array)` IDs of the items to delete. `(array)` 要删除的监控下的IDs。

[comment]: # ({/new-3574199c})

[comment]: # ({new-b676662e})
### Return values 返回值

`(object)` Returns an object containing the IDs of the deleted items
under the `itemids` property. `(object)`
在`itemids`属性下返回一个包含已被删除的监控项的IDs的对象。

[comment]: # ({/new-b676662e})

[comment]: # ({new-b41637d2})
### Examples 示例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-92fe5da6})
#### Deleting multiple items 删除多个监控项

Delete two items.\
Dependent items and item prototypes are removed automatically if master
item is deleted.
删除2个监控项。\\\\如果主监控项被删除，依赖监控项和监控项原型也会被自动删除。

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "item.delete",
    "params": [
        "22982",
        "22986"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "22982",
            "22986"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-92fe5da6})

[comment]: # ({new-4c7d30be})
### Source

CItem::delete() in
*frontends/php/include/classes/api/services/CItem.php*.

[comment]: # ({/new-4c7d30be})
