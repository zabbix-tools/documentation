[comment]: # translation:outdated

[comment]: # ({new-58fbd3bc})
# item.get

[comment]: # ({/new-58fbd3bc})

[comment]: # ({new-673edf7f})
### Description 说明

`integer/array item.get(object parameters)`

The method allows to retrieve items according to the given parameters.
此方法允许根据给定的参数获取监控项。

[comment]: # ({/new-673edf7f})

[comment]: # ({new-cc946363})
### Parameters 参数

`(object)` Parameters defining the desired output. `(object)`
参数定义期望输出。

The method supports the following parameters. 此方法支持如下参数。

|Parameter|Type|Description|
|---------|----|-----------|
|itemids|string/array|Return only items with the given IDs. 返回给定IDs的监控项。|
|groupids|string/array|Return only items that belong to the hosts from the given groups. 返回属于给定组的主机的监控项。|
|templateids|string/array|Return only items that belong to the given templates. 返回属于给定模板的监控项。|
|hostids|string/array|Return only items that belong to the given hosts. 返回属于给定主机的监控项。|
|proxyids|string/array|Return only items that are monitored by the given proxies. 返回被给定代理监控的监控项。|
|interfaceids|string/array|Return only items that use the given host interfaces. 返回使用给定主机接口的监控项。|
|graphids|string/array|Return only items that are used in the given graphs. 返回在给定图表中使用的监控项。|
|triggerids|string/array|Return only items that are used in the given triggers. 返回给定触发器所使用的监控项。|
|applicationids|string/array|Return only items that belong to the given applications. 返回属于给定应用的监控项。|
|webitems|flag|Include web items in the result. 返回结果中包含web监控项。|
|inherited|boolean|If set to `true` return only items inherited from a template. 如果设为`true`，返回继承自某个模板的监控项。|
|templated|boolean|If set to `true` return only items that belong to templates. 如果设为`true`，返回属于某个模板的监控项。|
|monitored|boolean|If set to `true` return only enabled items that belong to monitored hosts. 如果设为`true`，返回属于已监控主机的已启用的监控项。|
|group|string|Return only items that belong to a group with the given name. 返回属于给定组名的监控项。|
|host|string|Return only items that belong to a host with the given name. 返回给定主机名的监控项。|
|application|string|Return only items that belong to an application with the given name. 返回属于给定应用名的监控项。|
|with\_triggers|boolean|If set to `true` return only items that are used in triggers. 如果设为`true`，返回在触发器中使用的监控项。|
|selectHosts|query|Returns the host that the item belongs to as an array in the `hosts` property. 在`host`属性中已数组的形式返回监控项所属的主机。|
|selectInterfaces|query|Returns the host interface used by the item as an array in the `interfaces` property. 在'interfaces''属性中返回在主机接口中使用的监控项。|
|selectTriggers|query|Return triggers that the item is used in in the `triggers` property. 在`triggers`属性中返回使用该监控项的触发器。<br><br>Supports `count`.|
|selectGraphs|query|Return graphs that contain the item in the `graphs` property. 在`graphs`属性中返回包含该监控项的图表。<br><br>Supports `count`.|
|selectApplications|query|Return the applications that the item belongs to in the `applications` property. 在`application`属性中返回监控项所属的应用。|
|selectDiscoveryRule|query|Return the LLD rule that created the item in the `discoveryRule` property. 在`discoveryRule`属性中返回创建该监控项的LLD规则。|
|selectItemDiscovery|query|Return the item discovery object in the `itemDiscovery` property. The item discovery object links the item to an item prototype from which it was created. 在`itemDiscovery`属性中返回监控项发现对象。该监控项发现对象连接该监控项到监控项原型。<br><br>It has the following properties: 它有如下属性：<br>`itemdiscoveryid` - `(string)` ID of the item discovery;<br>`itemid` - `(string)` ID of the discovered item;<br>`parent_itemid` - `(string)` ID of the item prototype from which the item has been created;<br>`key_` - `(string)` key of the item prototype;<br>`lastcheck` - `(timestamp)` time when the item was last discovered;<br>`ts_delete` - `(timestamp)` time when an item that is no longer discovered will be deleted.|
|selectPreprocessing|query|Return item preprocessing options in `preprocessing` property.<br><br>It has the following properties:<br>`type` - `(string)` The preprocessing option types:<br>1 - Custom multiplier;<br>2 - Right trim;<br>3 - Left trim;<br>4 - Trim;<br>5 - Regular expression matching;<br>6 - Boolean to decimal;<br>7 - Octal to decimal;<br>8 - Hexadecimal to decimal;<br>9 - Simple change;<br>10 - Change per second.<br><br>`params` - `(string)` Additional parameters used by preprocessing option. Multiple parameters are separated by LF (\\n)character.|
|filter|object|Return only those results that exactly match the given filter. 返回紧缺匹配给定筛选条件的结果。<br><br>Accepts an array, where the keys are property names, and the values are either a single value or an array of values to match against. 接受数组，数组的键为属性名，值为要匹配的一个值或者值数组。<br><br>Supports additional filters: 支持附加筛选条件：<br>`host` - technical name of the host that the item belongs to.|
|limitSelects|integer|Limits the number of records returned by subselects. 限制子查询所返回的结果的数量。<br><br>Applies to the following subselects: 应用到如下子查询：<br>`selectGraphs` - results will be sorted by `name`;<br>`selectTriggers` - results will be sorted by `description`.|
|sortfield|string/array|Sort the result by the given properties.<br><br>Possible values are: `itemid`, `name`, `key_`, `delay`, `history`, `trends`, `type` and `status`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters) page.|
|editable|boolean|^|
|excludeSearch|boolean|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

[comment]: # ({/new-cc946363})

[comment]: # ({new-7223bab1})
### Return values 返回值

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Examples 示例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-59b84d93})
#### Finding items by key 根据key查找监控项

Retrieve all items from host with ID "10084" that have the word "system"
in the key and sort them by name.
从ID为"10084"的主机获取key带有"system"的监控项，并以名称排序。

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "item.get",
    "params": {
        "output": "extend",
        "hostids": "10084",
        "search": {
            "key_": "system"
        },
        "sortfield": "name"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "itemid": "23298",
            "type": "0",
            "snmp_community": "",
            "snmp_oid": "",
            "hostid": "10084",
            "name": "Context switches per second",
            "key_": "system.cpu.switches",
            "delay": "1m",
            "history": "7d",
            "trends": "365d",
            "lastvalue": "2552",
            "lastclock": "1351090998",
            "prevvalue": "2641",
            "state": "0",
            "status": "0",
            "value_type": "3",
            "trapper_hosts": "",
            "units": "sps",
            "snmpv3_securityname": "",
            "snmpv3_securitylevel": "0",
            "snmpv3_authpassphrase": "",
            "snmpv3_privpassphrase": "",
            "snmpv3_authprotocol": "0",
            "snmpv3_privprotocol": "0",
            "snmpv3_contextname": "",
            "error": "",
            "lastlogsize": "0",
            "logtimefmt": "",
            "templateid": "22680",
            "valuemapid": "0",
            "params": "",
            "ipmi_sensor": "",
            "authtype": "0",
            "username": "",
            "password": "",
            "publickey": "",
            "privatekey": "",
            "mtime": "0",
            "lastns": "564054253",
            "flags": "0",
            "interfaceid": "1",
            "port": "",
            "description": "",
            "inventory_link": "0",
            "lifetime": "0s",
            "evaltype": "0",
            "jmx_endpoint": "",
            "master_itemid": "0",
            "timeout": "3s",
            "url": "",
            "query_fields": [],
            "posts": "",
            "status_codes": "200",
            "follow_redirects": "1",
            "post_type": "0",
            "http_proxy": "",
            "headers": [],
            "retrieve_mode": "0",
            "request_method": "1",
            "output_format": "0",
            "ssl_cert_file": "",
            "ssl_key_file": "",
            "ssl_key_password": "",
            "verify_peer": "0",
            "verify_host": "0",
            "allow_traps": "0"
        },
        {
            "itemid": "23299",
            "type": "0",
            "snmp_community": "",
            "snmp_oid": "",
            "hostid": "10084",
            "name": "CPU $2 time",
            "key_": "system.cpu.util[,idle]",
            "delay": "1m",
            "history": "7d",
            "trends": "365d",
            "lastvalue": "86.031879",
            "lastclock": "1351090999",
            "prevvalue": "85.306944",
            "state": "0",
            "status": "0",
            "value_type": "0",
            "trapper_hosts": "",
            "units": "%",
            "snmpv3_securityname": "",
            "snmpv3_securitylevel": "0",
            "snmpv3_authpassphrase": "",
            "snmpv3_privpassphrase": "",
            "snmpv3_authprotocol": "0",
            "snmpv3_privprotocol": "0",
            "snmpv3_contextname": "",
            "error": "",
            "lastlogsize": "0",
            "logtimefmt": "",
            "templateid": "17354",
            "valuemapid": "0",
            "params": "",
            "ipmi_sensor": "",
            "authtype": "0",
            "username": "",
            "password": "",
            "publickey": "",
            "privatekey": "",
            "mtime": "0",
            "lastns": "564256864",
            "flags": "0",
            "interfaceid": "1",
            "port": "",
            "description": "The time the CPU has spent doing nothing.",
            "inventory_link": "0",
            "lifetime": "0s",
            "evaltype": "0",
            "jmx_endpoint": "",
            "master_itemid": "0",
            "timeout": "3s",
            "url": "",
            "query_fields": [],
            "posts": "",
            "status_codes": "200",
            "follow_redirects": "1",
            "post_type": "0",
            "http_proxy": "",
            "headers": [],
            "retrieve_mode": "0",
            "request_method": "1",
            "output_format": "0",
            "ssl_cert_file": "",
            "ssl_key_file": "",
            "ssl_key_password": "",
            "verify_peer": "0",
            "verify_host": "0",
            "allow_traps": "0"
        },
        {
            "itemid": "23300",
            "type": "0",
            "snmp_community": "",
            "snmp_oid": "",
            "hostid": "10084",
            "name": "CPU $2 time",
            "key_": "system.cpu.util[,interrupt]",
            "history": "7d",
            "trends": "365d",
            "lastvalue": "0.008389",
            "lastclock": "1351091000",
            "prevvalue": "0.000000",
            "state": "0",
            "status": "0",
            "value_type": "0",
            "trapper_hosts": "",
            "units": "%",
            "snmpv3_securityname": "",
            "snmpv3_securitylevel": "0",
            "snmpv3_authpassphrase": "",
            "snmpv3_privpassphrase": "",
            "snmpv3_authprotocol": "0",
            "snmpv3_privprotocol": "0",
            "snmpv3_contextname": "",
            "error": "",
            "lastlogsize": "0",
            "logtimefmt": "",
            "templateid": "22671",
            "valuemapid": "0",
            "params": "",
            "ipmi_sensor": "",
            "authtype": "0",
            "username": "",
            "password": "",
            "publickey": "",
            "privatekey": "",
            "mtime": "0",
            "lastns": "564661387",
            "flags": "0",
            "interfaceid": "1",
            "port": "",
            "description": "The amount of time the CPU has been servicing hardware interrupts.",
            "inventory_link": "0",
            "lifetime": "0s",
            "evaltype": "0",
            "jmx_endpoint": "",
            "master_itemid": "0",
            "timeout": "3s",
            "url": "",
            "query_fields": [],
            "posts": "",
            "status_codes": "200",
            "follow_redirects": "1",
            "post_type": "0",
            "http_proxy": "",
            "headers": [],
            "retrieve_mode": "0",
            "request_method": "1",
            "output_format": "0",
            "ssl_cert_file": "",
            "ssl_key_file": "",
            "ssl_key_password": "",
            "verify_peer": "0",
            "verify_host": "0",
            "allow_traps": "0"
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-59b84d93})

[comment]: # ({new-674b0856})
#### Finding dependent items by key 根据key查找依赖监控项

Retrieve all dependent items from host with ID "10116" that have the
word "apache" in the key.
从ID未"10116"的主机中获取key名包含"apache"的依赖监控项。

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "item.get",
    "params": {
        "output": "extend",
        "hostids": "10116",
        "search": {
            "key_": "apache"
        },
        "filter": {
            "type": "18"
        }
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "itemid": "25550",
            "type": "18",
            "snmp_community": "",
            "snmp_oid": "",
            "hostid": "10116",
            "name": "Days",
            "key_": "apache.status.uptime.days",
            "delay": "",
            "history": "90d",
            "trends": "365d",
            "status": "0",
            "value_type": "3",
            "trapper_hosts": "",
            "units": "",
            "snmpv3_securityname": "",
            "snmpv3_securitylevel": "0",
            "snmpv3_authpassphrase": "",
            "snmpv3_privpassphrase": "",
            "formula": "",
            "error": "",
            "lastlogsize": "0",
            "logtimefmt": "",
            "templateid": "0",
            "valuemapid": "0",
            "params": "",
            "ipmi_sensor": "",
            "authtype": "0",
            "username": "",
            "password": "",
            "publickey": "",
            "privatekey": "",
            "mtime": "0",
            "flags": "0",
            "interfaceid": "0",
            "port": "",
            "description": "",
            "inventory_link": "0",
            "lifetime": "30d",
            "snmpv3_authprotocol": "0",
            "snmpv3_privprotocol": "0",
            "state": "0",
            "snmpv3_contextname": "",
            "evaltype": "0",
            "master_itemid": "25545",
            "jmx_endpoint": "",
            "timeout": "3s",
            "url": "",
            "query_fields": [],
            "posts": "",
            "status_codes": "200",
            "follow_redirects": "1",
            "post_type": "0",
            "http_proxy": "",
            "headers": [],
            "retrieve_mode": "0",
            "request_method": "1",
            "output_format": "0",
            "ssl_cert_file": "",
            "ssl_key_file": "",
            "ssl_key_password": "",
            "verify_peer": "0",
            "verify_host": "0",
            "allow_traps": "0",
            "lastclock": "0",
            "lastns": "0",
            "lastvalue": "0",
            "prevvalue": "0"
        },
        {
            "itemid": "25555",
            "type": "18",
            "snmp_community": "",
            "snmp_oid": "",
            "hostid": "10116",
            "name": "Hours",
            "key_": "apache.status.uptime.hours",
            "delay": "0",
            "history": "90d",
            "trends": "365d",
            "status": "0",
            "value_type": "3",
            "trapper_hosts": "",
            "units": "",
            "snmpv3_securityname": "",
            "snmpv3_securitylevel": "0",
            "snmpv3_authpassphrase": "",
            "snmpv3_privpassphrase": "",
            "formula": "",
            "error": "",
            "lastlogsize": "0",
            "logtimefmt": "",
            "templateid": "0",
            "valuemapid": "0",
            "params": "",
            "ipmi_sensor": "",
            "authtype": "0",
            "username": "",
            "password": "",
            "publickey": "",
            "privatekey": "",
            "mtime": "0",
            "flags": "0",
            "interfaceid": "0",
            "port": "",
            "description": "",
            "inventory_link": "0",
            "lifetime": "30d",
            "snmpv3_authprotocol": "0",
            "snmpv3_privprotocol": "0",
            "state": "0",
            "snmpv3_contextname": "",
            "evaltype": "0",
            "master_itemid": "25545",
            "jmx_endpoint": "",
            "timeout": "3s",
            "url": "",
            "query_fields": [],
            "posts": "",
            "status_codes": "200",
            "follow_redirects": "1",
            "post_type": "0",
            "http_proxy": "",
            "headers": [],
            "retrieve_mode": "0",
            "request_method": "1",
            "output_format": "0",
            "ssl_cert_file": "",
            "ssl_key_file": "",
            "ssl_key_password": "",
            "verify_peer": "0",
            "verify_host": "0",
            "allow_traps": "0",
            "lastclock": "0",
            "lastns": "0",
            "lastvalue": "0",
            "prevvalue": "0"
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-674b0856})

[comment]: # ({new-94dc4b21})
#### Find HTTP agent item 查找HTTP agent 监控项

Find HTTP agent item with post body type XML for specific host id.
根据定义的主机id来查找带有XML post报文类型的监控项。

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "item.get",
    "params": {
        "hostids": "10255",
        "filter": {
            "type": "19",
            "post_type": "3"
        }
    },
    "id": 3,
    "auth": "d678e0b85688ce578ff061bd29a20d3b"
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "itemid": "28252",
            "type": "19",
            "snmp_community": "",
            "snmp_oid": "",
            "hostid": "10255",
            "name": "template item",
            "key_": "ti",
            "delay": "30s",
            "history": "90d",
            "trends": "365d",
            "status": "0",
            "value_type": "3",
            "trapper_hosts": "",
            "units": "",
            "snmpv3_securityname": "",
            "snmpv3_securitylevel": "0",
            "snmpv3_authpassphrase": "",
            "snmpv3_privpassphrase": "",
            "formula": "",
            "error": "",
            "lastlogsize": "0",
            "logtimefmt": "",
            "templateid": "0",
            "valuemapid": "0",
            "params": "",
            "ipmi_sensor": "",
            "authtype": "0",
            "username": "",
            "password": "",
            "publickey": "",
            "privatekey": "",
            "mtime": "0",
            "flags": "0",
            "interfaceid": "0",
            "port": "",
            "description": "",
            "inventory_link": "0",
            "lifetime": "30d",
            "snmpv3_authprotocol": "0",
            "snmpv3_privprotocol": "0",
            "state": "0",
            "snmpv3_contextname": "",
            "evaltype": "0",
            "jmx_endpoint": "",
            "master_itemid": "0",
            "timeout": "3s",
            "url": "localhost",
            "query_fields": [
                {
                    "mode": "xml"
                }
            ],
            "posts": "<body>\r\n<![CDATA[{$MACRO}<foo></bar>]]>\r\n</body>",
            "status_codes": "200",
            "follow_redirects": "0",
            "post_type": "3",
            "http_proxy": "",
            "headers": [],
            "retrieve_mode": "1",
            "request_method": "3",
            "output_format": "0",
            "ssl_cert_file": "",
            "ssl_key_file": "",
            "ssl_key_password": "",
            "verify_peer": "0",
            "verify_host": "0",
            "allow_traps": "0",
            "lastclock": "0",
            "lastns": "0",
            "lastvalue": "0",
            "prevvalue": "0"
        }
    ],
    "id": 3
}
```

[comment]: # ({/new-94dc4b21})

[comment]: # ({new-f4b01bbe})
### Source

CItem::get() in *ui/include/classes/api/services/CItem.php*.

[comment]: # ({/new-f4b01bbe})

[comment]: # ({new-d3276b35})
### See also

-   [Application](/manual/api/reference/application/object#application)
-   [Discovery
    rule](/manual/api/reference/discoveryrule/object#discovery_rule)
-   [Graph](/manual/api/reference/graph/object#graph)
-   [Host](/manual/api/reference/host/object#host)
-   [Host
    interface](/manual/api/reference/hostinterface/object#host_interface)
-   [Trigger](/manual/api/reference/trigger/object#trigger)

[comment]: # ({/new-d3276b35})

[comment]: # ({new-a6ef1d0f})
### Source

CItem::get() in *frontends/php/include/classes/api/services/CItem.php*.

[comment]: # ({/new-a6ef1d0f})

