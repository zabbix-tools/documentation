[comment]: # translation:outdated

[comment]: # ({new-6bb9f60f})
# > Item object 监控项对象

The following objects are directly related to the `item` API. 以下对象与
“item” API直接相关。

[comment]: # ({/new-6bb9f60f})

[comment]: # ({new-385ac8b5})
### Item

[comment]: # ({/new-385ac8b5})

[comment]: # ({new-c1b98afa})
### 监控项

::: noteclassic
Web items cannot be directly created, updated or deleted via
the Zabbix API.
::: 

::: noteclassic
Web 监控项无法通过 Zabbix API
直接创建，更新或删除。
:::

The item object has the following properties. 监控项对象具有以下属性。

|Property 属性   T|pe 类型   Des|ription 说明|
|-------------------|---------------|--------------|

|   |   |   |   |
|---|---|---|---|
|itemid|string|*(readonly)* ID of the item. 监控项ID。|<|
|**delay**<br>(required)|string|Update interval of the item. Accepts seconds or time unit with suffix and with or without one or more [custom intervals](/manual/config/items/item/custom_intervals) that consist of either flexible intervals and scheduling intervals as serialized strings. Also accepts user macros. Flexible intervals could be written as two macros separated by a forward slash. Intervals are separated by a semicolon. 更新监控项的时间间隔。 接受具有后缀的秒或时间单位，并且具有或不具有由灵活间隔和调度间隔组成的一个或多个自定义间隔作为串行化字符串。也接受用户宏。 灵活的间隔可以写成两个由正斜杠分隔的宏。 间隔用分号分隔。<br><br>Optional for Zabbix trapper or Dependent item.|<|
|**hostid**<br>(required)|string|ID of the host that the item belongs to. 该监控项所属的主机 ID。<br><br>For update operations this field is *readonly*.|<|
|**interfaceid**<br>(required)|string|ID of the item's host interface. Used only for host items. 监控项主机接口的ID。 仅用于主机项。<br><br>Optional for Zabbix agent (active), Zabbix internal, Zabbix trapper, Dependent item, Zabbix aggregate, database monitor and calculated items. 适用于Zabbix代理（活动），Zabbix内部，Zabbix陷阱，依赖项，Zabbix聚合，数据库监控和计算项|<|
|<|**key\_**<br>(required)|string|Item key.|
|**name**<br>(required)|string|Name of the item.|<|
|**type**<br>(required)|integer|Type of the item.<br><br>Possible values:<br>0 - Zabbix agent;<br>1 - SNMPv1 agent;<br>2 - Zabbix trapper;<br>3 - simple check;<br>4 - SNMPv2 agent;<br>5 - Zabbix internal;<br>6 - SNMPv3 agent;<br>7 - Zabbix agent (active);<br>8 - Zabbix aggregate;<br>9 - web item;<br>10 - external check;<br>11 - database monitor;<br>12 - IPMI agent;<br>13 - SSH agent;<br>14 - TELNET agent;<br>15 - calculated;<br>16 - JMX agent;<br>17 - SNMP trap;<br>18 - Dependent item;<br>19 - HTTP agent;|<|
|**url**<br>(required)|string|URL string, required only for HTTP agent item type. Supports user macros, {HOST.IP}, {HOST.CONN}, {HOST.DNS}, {HOST.HOST}, {HOST.NAME}, {ITEM.ID}, {ITEM.KEY}. URL字符串，仅HTTP agent监控项类型需要。支持用户宏，{HOST.IP}, {HOST.CONN}, {HOST.DNS}, {HOST.HOST},{HOST.NAME}, {ITEM.ID}, {ITEM.KEY}。|<|
|**value\_type**<br>(required)|integer|Type of information of the item. 监控项信息的类型。<br><br>Possible values:<br>0 - numeric float;<br>1 - character;<br>2 - log;<br>3 - numeric unsigned;<br>4 - text.|<|
|allow\_traps|integer|HTTP agent item field. Allow to populate value as in trapper item type also. HTTP agent监控项字段。允许和trapper监控项一样的填充值。<br><br>0 - *(default)* Do not allow to accept incoming data.<br>1 - Allow to accept incoming data.|<|
|authtype|integer|Used only by SSH agent items or HTTP agent items. 仅在SSH agent items or HTTP agent items中使用。<br><br>SSH agent authentication method possible values:<br>0 - *(default)* password;<br>1 - public key.<br><br>HTTP agent authentication method possible values:<br>0 - *(default)* none<br>1 - basic<br>2 - NTLM|<|
|description|string|Description of the item. 监控项说明。|<|
|error|string|*(readonly)* Error text if there are problems updating the item. 当更新监控项出错时的错误文本。|<|
|flags|integer|*(readonly)* Origin of the item.<br><br>Possible values:<br>0 - a plain item;<br>4 - a discovered item.|<|
|follow\_redirects|integer|HTTP agent item field. Follow respose redirects while pooling data. HTTP agent 监控项字段。合并数据时跟随重定向。<br><br>0 - Do not follow redirects.<br>1 - *(default)* Follow redirects.|<|
|headers|object|HTTP agent item field. Object with HTTP(S) request headers, where header name is used as key and header value as value. HTTP agent 监控项字段。带有HTTP(S)请求报头的对象，报头名为键名，报头值为值。<br><br>Example:<br>{ "User-Agent": "Zabbix" }|<|
|history|string|A time unit of how long the history data should be stored. Also accepts user macro. 一个历史数据被保存的时长的时间单位。接受用户宏。<br><br>Default: 90d.|<|
|http\_proxy|string|HTTP agent item field. HTTP(S) proxy connection string. HTTP agent 监控项字段。HTTP(S)代理连接字符串。|<|
|inventory\_link|integer|ID of the host inventory field that is populated by the item. 监控项填充的主机资产的ID。<br><br>Refer to the [host inventory page](/manual/api/reference/host/object#host_inventory) for a list of supported host inventory fields and their IDs.<br><br>Default: 0.|<|
|ipmi\_sensor|string|IPMI sensor. Used only by IPMI items. IPMI传感器。仅用于IPMI监控项。|<|
|jmx\_endpoint|string|JMX agent custom connection string. JMX agent自定义的连接字符串。<br><br>Default value:<br>service:jmx:rmi:///jndi/rmi://{HOST.CONN}:{HOST.PORT}/jmxrmi|<|
|lastclock|timestamp|*(readonly)* Time when the item was last updated. 监控项最后被更新的时间。<br><br>This property will only return a value for the period configured in [ZBX\_HISTORY\_PERIOD](/manual/web_interface/definitions).|<|
|lastns|integer|*(readonly)* Nanoseconds when the item was last updated. 监控项最后被更新的纳秒。<br><br>This property will only return a value for the period configured in [ZBX\_HISTORY\_PERIOD](/manual/web_interface/definitions).|<|
|lastvalue|string|*(readonly)* Last value of the item. 监控项最新的值。<br><br>This property will only return a value for the period configured in [ZBX\_HISTORY\_PERIOD](/manual/web_interface/definitions).|<|
|logtimefmt|string|Format of the time in log entries. Used only by log items. 日志条目的时间格式。仅用于日志监控项。|<|
|master\_itemid|integer|Master item ID.<br>Recursion up to 3 dependent items and maximum count of dependent items equal to 999 are allowed. 允许多达3个依赖监控项的递归和监控项的最大计数等于999<br><br>Required by Dependent items.|<|
|mtime|timestamp|Time when the monitored log file was last updated. Used only by log items. 被监控的日志文件最后一次被更新的时间。仅泳衣日志监控项。|<|
|output\_format|integer|HTTP agent item field. Should response converted to JSON. HTTP agent监控项字段。返回数据应被转换成JSON。<br><br>0 - *(default)* Store raw.<br>1 - Convert to JSON.|<|
|params|string|Additional parameters depending on the type of the item: 取决于监控项类型的附加参数：<br>- executed script for SSH and Telnet items;<br>- SQL query for database monitor items;<br>- formula for calculated items.|<|
|password|string|Password for authentication. Used by simple check, SSH, Telnet, database monitor, JMX and HTTP agent items. 认证的密码。用于simple check, SSH, Telnet, database monitor, JMX and HTTP agent items.<br>When used by JMX, username should also be specified together with password or both properties should be left blank. 当贝JMX使用时，用户名应和密码一起提供，或者同时留空。|<|
|port|string|Port monitored by the item. Used only by SNMP items. 监控项监控的端口。仅用于SMNP监控项。|<|
|post\_type|integer|HTTP agent item field. Type of post data body stored in posts property. HTTP agent字段。存储在post属性的post的数据类型。<br><br>0 - *(default)* Raw data.<br>2 - JSON data.<br>3 - XML data.|<|
|posts|string|HTTP agent item field. HTTP(S) request body data. Used with post\_type. HTTP agent字段。HTTP(S)请求报文。仅用于post\_type。|<|
|prevvalue|string|*(readonly)* Previous value of the item. 监控项的前一个值。<br><br>This property will only return a value for the period configured in [ZBX\_HISTORY\_PERIOD](/manual/web_interface/definitions).|<|
|privatekey|string|Name of the private key file. 私钥文件名。|<|
|publickey|string|Name of the public key file. 公钥的文件名。|<|
|query\_fields|array|HTTP agent item field. Query parameters. Array of objects with 'key':'value' pairs, where value can be empty string. HTTP agent监控项字段。查询参数。带有键值对的数组对象，值可为空。|<|
|request\_method|integer|HTTP agent item field. Type of request method. HTTP agent监控项字段。请求方法的类型。<br><br>0 - GET<br>1 - *(default)* POST<br>2 - PUT<br>3 - HEAD|<|
|retrieve\_mode|integer|HTTP agent item field. What part of response should be stored. HTTP agent监控项字段。被存储的响应的部分。<br><br>0 - *(default)* Body.<br>1 - Headers.<br>2 - Both body and headers will be stored.<br><br>For request\_method HEAD only 1 is allowed value.|<|
|snmp\_community|string|SNMP community. Used only by SNMPv1 and SNMPv2 items. SNMP community。仅用于SNMPv1 and SNMPv2 监控项。|<|
|snmp\_oid|string|SNMP OID.|<|
|snmpv3\_authpassphrase|string|SNMPv3 auth passphrase. Used only by SNMPv3 items. SNMPv3认证密码。仅用于SNMPv3监控项。|<|
|snmpv3\_authprotocol|integer|SNMPv3 authentication protocol. Used only by SNMPv3 items. SNMPv3认证协议。仅用于SNMPv3监控项。<br><br>Possible values:<br>0 - *(default)* MD5;<br>1 - SHA.|<|
|snmpv3\_contextname|string|SNMPv3 context name. Used only by SNMPv3 items. SNMPv3文本名称。仅用于SNMPv3监控项。|<|
|snmpv3\_privpassphrase|string|SNMPv3 priv passphrase. Used only by SNMPv3 items. SNMPv3私钥。仅用于SNMPv3监控项。|<|
|snmpv3\_privprotocol|integer|SNMPv3 privacy protocol. Used only by SNMPv3 items. SNMPv3文私密协议。仅用于SNMPv3监控项。<br><br>Possible values:<br>0 - *(default)* DES;<br>1 - AES.|<|
|snmpv3\_securitylevel|integer|SNMPv3 security level. Used only by SNMPv3 items. SNMPv3安全等级。仅用于SNMPv3监控项。<br><br>Possible values:<br>0 - noAuthNoPriv;<br>1 - authNoPriv;<br>2 - authPriv.|<|
|snmpv3\_securityname|string|SNMPv3 security name. Used only by SNMPv3 items. SNMPv3安全名称。仅用于SNMPv3监控项。|<|
|ssl\_cert\_file|string|HTTP agent item field. Public SSL Key file path. HTTP agent监控项字段。公共SSL 秘钥的文件路径。|<|
|ssl\_key\_file|string|HTTP agent item field. Private SSL Key file path. HTTP agent监控项字段。私有SLL秘钥的文件路径。|<|
|ssl\_key\_password|string|HTTP agent item field. Password for SSL Key file. HTTP agent监控项字段。SSL 秘钥的文件密码。|<|
|state|integer|*(readonly)* State of the item.<br><br>Possible values:<br>0 - *(default)* normal;<br>1 - not supported.|<|
|status|integer|Status of the item.<br><br>Possible values:<br>0 - *(default)* enabled item;<br>1 - disabled item.|<|
|status\_codes|string|HTTP agent item field. Ranges of required HTTP status codes separated by commas. Also supports user macros as part of comma separated list. HTTP agent监控项字段。以逗号分隔的HTTP 状态码的范围。也支持作为逗号分隔的用户宏列表。<br><br>Example: 200,200-{$M},{$M},200-400|<|
|templateid|string|(readonly) ID of the parent template item. （只读）父模板的ID。|<|
|timeout|string|HTTP agent item field. Item data polling request timeout. Support user macros. HTTP agent监控项字段。监控项数据轮询超时时间。支持用户宏。<br><br>default: 3s<br>maximum value: 60s|<|
|trapper\_hosts|string|Allowed hosts. Used by trapper items or HTTP agent items. 接受的主机。仅用于trapper监控项或者HTTP agent监控项。|<|
|trends|string|A time unit of how long the trends data should be stored. Also accepts user macro. 时间单位，数据数据被保存的时间长度。也接受用户宏。<br><br>Default: 365d.|<|
|units|string|Value units. 值的单位。|<|
|username|string|Username for authentication. Used by simple check, SSH, Telnet, database monitor, JMX and HTTP agent items. 认证的用户名。用于simple check, SSH, Telnet, database monitor, JMX and HTTP agent 监控项。<br><br>Required by SSH and Telnet items. SSH and Telnet items 要求提供。<br>When used by JMX, password should also be specified together with username or both properties should be left blank. 当被JMX使用时，密码也要和用户名一起被提供或者一起留空。|<|
|valuemapid|string|ID of the associated value map. 关联映射值的ID。|<|
|verify\_host|integer|HTTP agent item field. Validate host name in URL is in Common Name field or a Subject Alternate Name field of host certificate. HTTP agent字段。验证URL中的主机名处于通用名称字段或主机证书的主题备用名称字段<br><br>0 - *(default)* Do not validate.<br>1 - Validate.|<|
|verify\_peer|integer|HTTP agent item field. Validate is host certificate authentic. HTTP agent字段。验证主机的合法性。<br><br>0 - *(default)* Do not validate.<br>1 - Validate.|<|

[comment]: # ({/new-c1b98afa})

[comment]: # ({new-2169bb78})
### Item preprocessing 监控项预处理

The item preprocessing object has the following properties.
监控项预处理对象有如下属性。

|Property|Type|Description|
|--------|----|-----------|
|**type**<br>(required)|integer|The preprocessing option type.<br><br>Possible values:<br>1 - Custom multiplier;<br>2 - Right trim;<br>3 - Left trim;<br>4 - Trim;<br>5 - Regular expression matching;<br>6 - Boolean to decimal;<br>7 - Octal to decimal;<br>8 - Hexadecimal to decimal;<br>9 - Simple change;<br>10 - Change per second.|
|**params**<br>(required)|string|Additional parameters used by preprocessing option. Multiple parameters are separated by LF (\\n) character.|

[comment]: # ({/new-2169bb78})
