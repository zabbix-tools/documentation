[comment]: # translation:outdated

[comment]: # ({new-959fe29c})
# item.create

[comment]: # ({/new-959fe29c})

[comment]: # ({new-e3078f54})
### Description 说明

`object item.create(object/array items)`

This method allows to create new items. 此方法允许创建监控项。

::: noteclassic
Web items cannot be created via the Zabbix API. WEB
监控项不能通过Zabbix API创建。
:::

[comment]: # ({/new-e3078f54})

[comment]: # ({new-777e8bec})
### Parameters 参数

`(object/array)` Items to create. `(object/array)` 要创建的监控项。

Additionally to the [standard item properties](object#host), the method
accepts the following parameters. 另外见[standard item
properties](object#host)，此方法接受如下参数。

|Parameter|Type|Description|
|---------|----|-----------|
|applications|array|IDs of the applications to add the item to. 要添加到监控项的应用IDs。|
|preprocessing|array|Item preprocessing options. 监控项预处理选项。|

[comment]: # ({/new-777e8bec})

[comment]: # ({new-632959fe})
### Return values 返回值

`(object)` Returns an object containing the IDs of the created items
under the `itemids` property. The order of the returned IDs matches the
order of the passed items. `(object)`
在`itemids`属性下返回包含已创建的监控项的对象的IDs。返回的IDs的顺序与传递的监控项的IDs的顺序一致。

[comment]: # ({/new-632959fe})

[comment]: # ({new-b41637d2})
### Examples 示例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-6ffb1f45})
#### Creating an item 创建一个监控项

Create a numeric Zabbix agent item to monitor free disk space on host
with ID "30074" and add it to two applications. 创建一个数字类型的Zabbix
agent监控项监控ID为"30074"的主机的可用磁盘空间并添加到2个应用。

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "item.create",
    "params": {
        "name": "Free disk space on $1",
        "key_": "vfs.fs.size[/home/joe/,free]",
        "hostid": "30074",
        "type": 0,
        "value_type": 3,
        "interfaceid": "30084",
        "applications": [
            "609",
            "610"
        ],
        "delay": "30s"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "24758"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-6ffb1f45})

[comment]: # ({new-0f29886e})
#### Creating a host inventory item 创建一个主机清单监控项

Create a Zabbix agent item to populate the host's "OS" inventory field.
创建一个Zabbix agent监控项填充主机的"OS"清单字段。

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "item.create",
    "params": {
        "name": "uname",
        "key_": "system.uname",
        "hostid": "30021",
        "type": 0,
        "interfaceid": "30007",
        "value_type": 1,
        "delay": "10s",
        "inventory_link": 5
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "24759"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-0f29886e})

[comment]: # ({new-f45b1a9a})
#### Creating an item with preprocessing 创建带有预处理的监控项

Create an item using custom multiplier. 使用自定义乘法器创建监控项。

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "item.create",
    "params": {
        "name": "Device uptime",
        "key_": "sysUpTime",
        "hostid": "11312",
        "type": 4,
        "snmp_community": "{$SNMP_COMMUNITY}",
        "snmp_oid": "SNMPv2-MIB::sysUpTime.0",
        "value_type": 1,
        "delay": "60s",
        "units": "uptime",
        "interfaceid": "1156",
        "preprocessing": [
            {
                "type": "1",
                "params": "0.01"
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "44210"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-f45b1a9a})

[comment]: # ({new-6a32a64f})
#### Creating dependent item 创建依赖监控项

Create a dependent item for the master item with ID 24759. Only
dependencies on the same host are allowed, therefore master and the
dependent item should have the same hostid.
为ID为24759的主监控项创建依赖监控项。仅依同一主机的以来监控项被允许，因此主监控项和依赖监控应有相同的hostid。

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "item.create",
    "params": {
      "hostid": "30074",
      "name": "Dependent test item",
      "key_": "dependent.item",
      "type": "18",
      "master_itemid": "24759",
      "value_type": "2"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "44211"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-6a32a64f})

[comment]: # ({new-70eb9eae})
#### Create HTTP agent item 创建HTTP agent监控项

Create POST request method item with JSON response preprocessing.
创建带有JSON响应预处理的POST请求的方法监控项。

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "item.create",
    "params": {
        "url":"http://127.0.0.1/http.php",
        "query_fields": [
            {
                "mode":"json"
            },
            {
                "min": "10"
            },
            {
                "max": "100"
            }
        ],
        "interfaceid": "1",
        "type":"19",
        "hostid":"10254",
        "delay":"5s",
        "key_":"json",
        "name":"http agent example JSON",
        "value_type":"0",
        "output_format":"1",
        "preprocessing": [
            {
                "type": "12",
                "params": "$.random"
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 2
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "23865"
        ]
    },
    "id": 3
}
```

[comment]: # ({/new-70eb9eae})

[comment]: # ({new-313c9999})
### Source

CItem::create() in
*frontends/php/include/classes/api/services/CItem.php*.

[comment]: # ({/new-313c9999})


[comment]: # ({new-ecb97b1d})
### Source

CItem::create() in *ui/include/classes/api/services/CItem.php*.

[comment]: # ({/new-ecb97b1d})
