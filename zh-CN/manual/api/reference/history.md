[comment]: # translation:outdated

[comment]: # ({new-6525f8c0})
# 历史

这个类是设计用于处理历史数据

对象引用:\

-   [History](/manual/api/reference/history/object#history)

可用方法:\

-   [history.get](/manual/api/reference/history/get) - 获取历史数据.

[comment]: # ({/new-6525f8c0})
