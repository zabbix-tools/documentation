[comment]: # translation:outdated

[comment]: # ({new-2fbbc9a6})
# > 对象

The following objects are directly related to the `mediatype` API.
以下对象是直接关联到`mediatype`接口

[comment]: # ({/new-2fbbc9a6})

[comment]: # ({new-4630c87a})
### Media type 媒介类型

The media type object has the following properties.
媒介类型参数拥有以下参数

|Property|Type|Description|
|--------|----|-----------|
|mediatypeid|string|*(readonly)* ID of the media type. 媒介类型ID|
|**description**<br>(required)|string|Name of the media type. 媒介类型名称|
|**type**<br>(required)|integer|Transport used by the media type.<br><br>Possible values:<br>0 - e-mail;<br>1 - script;<br>2 - SMS;<br>3 - Jabber;<br>100 - Ez Texting. 媒介类型的传输方式<br><br>可能的值：<br>0-电子邮件 1-脚本 2-SMS 3-Jabber 100-Ez Texting。|
|exec\_path|string|For script media types `exec_path` contains the name of the executed script.<br><br>For Ez Texting `exec_path` contains the message text limit.<br>Possible text limit values:<br>0 - USA (160 characters);<br>1 - Canada (136 characters).<br><br>Required for script and Ez Texting media types.<br>对于脚本媒体类型，“exec\_path”包含已执行脚本的名称。<br><br>对于z Texting `exec_path`包含了消息文本的限制<br>可能的文本限定值 ：0- USA (160 characters) 1 - Canada (136 characters).<br><br>用于脚本和Ez短信媒体类型。|
|gsm\_modem|string|Serial device name of the GSM modem.<br><br>Required for SMS media types. GSM调制解调器的串行设备名称。<br><br>用于SMS媒介类型|
|passwd|string|Authentication password.<br><br>Required for Jabber and Ez Texting media types. 认证的密码<br>\\\\用于Jabber和Ez Texting媒介类型|
|smtp\_email|string|Email address from which notifications will be sent.<br><br>Required for email media types. 发送通知的电子邮件地址。<br><br>用于电子邮件媒介类型|
|smtp\_helo|string|SMTP HELO.<br><br>Required for email media types. SMTP HELO<br><br>用于电子邮件媒介类型|
|smtp\_server|string|SMTP server.<br><br>Required for email media types. SMTP server.<br>\\\\用于电子邮件媒介类型|
|status|integer|Whether the media type is enabled.<br><br>Possible values:<br>0 - *(default)* enabled;<br>1 - disabled. 媒介类型是否是启用的<br>\\\\可能的值：<br>0-启用（默认） 1-禁用|
|username|string|Username or Jabber identifier.<br><br>Required for Jabber and Ez Texting media types. 用户名或Jabber标识符<br><br>用于Jabber and Ez Texting媒介类型|
|exec\_params|string|Script parameters.<br><br>Each parameter ends with a new line feed. 脚本参数。<br><br>每个参数以新的行提要结束|
|maxsessions|integer|The maximum number of alerts that can be processed in parallel.<br><br>Possible values for SMS:<br>1 - *(default)*<br><br>Possible values for other media types:<br>0-100 可以并行处理的警报的最大数量。<br><br>SMS可能的值：1（默认的）\\\\其他媒介类型可能的值：0-100|
|maxattempts|integer|The maximum number of attempts to send an alert.<br><br>Possible values:<br>1-10<br><br>Default value:<br>3 发送警报的最大尝试次数。<br>\\\\可能的值：<br><br>1-10，默认是3|
|attempt\_interval|string|The interval between retry attempts. Accepts seconds and time unit with suffix.<br><br>Possible values:<br>0-60s<br><br>Default value:<br>10s 重试尝试之间的间隔。接收带后缀的秒和时间单位。<br><br>可能的值：0\~60s 默认是：10s|

[comment]: # ({/new-4630c87a})



[comment]: # ({new-46e1e59a})
### Webhook parameters

Parameters passed to webhook script when it is called, have the
following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**name**<br>(required)|string|Parameter name.|
|value|string|Parameter value, support macros. Supported macros described on [page](/manual/appendix/macros/supported_by_location).|

[comment]: # ({/new-46e1e59a})

[comment]: # ({new-parameters})
### Script parameters

Parameters passed to a script when it is being called have the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|**sortorder**<br>(required)|integer|The order in which the parameters will be passed to the script as command-line arguments. Starting with 0 as the first one.|
|value|string|Parameter value, supports macros.<br>Supported macros are described on the [Supported macros](/manual/appendix/macros/supported_by_location) page.|

[comment]: # ({/new-parameters})

[comment]: # ({new-9c81491f})
### Message template

The message template object defines a template that will be used as a
default message for action operations to send a notification. It has the
following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**eventsource**<br>(required)|integer|Event source.<br><br>Possible values:<br>0 - triggers;<br>1 - discovery;<br>2 - autoregistration;<br>3 - internal;<br>4 - services.|
|**recovery**<br>(required)|integer|Operation mode.<br><br>Possible values:<br>0 - operations;<br>1 - recovery operations;<br>2 - update operations.|
|subject|string|Message subject.|
|message|string|Message text.|

[comment]: # ({/new-9c81491f})
