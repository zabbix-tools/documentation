[comment]: # translation:outdated

[comment]: # ({new-03fad7e1})
# 删除

[comment]: # ({/new-03fad7e1})

[comment]: # ({new-3ee75870})
### Description 描述

`object mediatype.delete(array mediaTypeIds)`

This method allows to delete media types. 此方法适合删除媒介类型

[comment]: # ({/new-3ee75870})

[comment]: # ({new-854e3ea2})
### Parameters 参数

`(array)` IDs of the media types to delete. `(array)`
要删除媒介类型的IDS

[comment]: # ({/new-854e3ea2})

[comment]: # ({new-ade2598d})
### Return values 返回值

`(object)` Returns an object containing the IDs of the deleted media
types under the `mediatypeids` property. `(object)`
返回一个对象，该对象包含`mediatypeids`属性下已删除的媒体类型的id。

[comment]: # ({/new-ade2598d})

[comment]: # ({new-b41637d2})
### Examples 示例如下

[comment]: # ({/new-b41637d2})

[comment]: # ({new-09d9ff4a})
#### Deleting multiple media types 删除多个媒介类型

Delete two media types. 删除2个媒介类型

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "mediatype.delete",
    "params": [
        "3",
        "5"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "mediatypeids": [
            "3",
            "5"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-09d9ff4a})

[comment]: # ({new-68bd428e})
### Source

CMediaType::delete() in
*frontends/php/include/classes/api/services/CMediaType.php*.

[comment]: # ({/new-68bd428e})
