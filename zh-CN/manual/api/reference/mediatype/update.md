[comment]: # translation:outdated

[comment]: # ({new-97b0e55a})
# 更新

[comment]: # ({/new-97b0e55a})

[comment]: # ({new-4cbfe789})
### Description 描述

`object mediatype.update(object/array mediaTypes)`

This method allows to update existing media types.
此方法允许更新已存在的媒介类型。

[comment]: # ({/new-4cbfe789})

[comment]: # ({new-7db8f4bd})
### Parameters 参数

`(object/array)` [Media type properties](object#media_type) to be
updated.

The `mediatypeid` property must be defined for each media type, all
other properties are optional. Only the passed properties will be
updated, all others will remain unchanged.
`mediatypeid`参数需要被每个每个类型所定义，其他的属性都是可选的。仅仅传递的属性会被更新，其他的属性将会保持不变

[comment]: # ({/new-7db8f4bd})

[comment]: # ({new-665d000d})
### Return values 返回值

`(object)` Returns an object containing the IDs of the updated media
types under the `mediatypeids` property.
`(object)`返回包含`mediatypeids`属性下所更新IDs的对象。

[comment]: # ({/new-665d000d})

[comment]: # ({new-b41637d2})
### Examples 示例如下

[comment]: # ({/new-b41637d2})

[comment]: # ({new-8d3ba0cc})
#### Enabling a media type 启用一个媒介类型

Enable a media type, that is, set its status to 0.
启用一个媒介类型，就是设置他的status属性是0.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "mediatype.update",
    "params": {
        "mediatypeid": "6",
        "status": 0
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "mediatypeids": [
            "6"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-8d3ba0cc})

[comment]: # ({new-b9cdc31e})
### Source

CMediaType::update() in
*frontends/php/include/classes/api/services/CMediaType.php*.

[comment]: # ({/new-b9cdc31e})
