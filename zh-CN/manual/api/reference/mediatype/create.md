[comment]: # translation:outdated

[comment]: # ({new-fb5f6958})
# 创建

[comment]: # ({/new-fb5f6958})

[comment]: # ({new-34dd1ecc})
### Description 描述

`object mediatype.create(object/array mediaTypes)`

This method allows to create new media types. 此方法允许创建新的媒介类型

[comment]: # ({/new-34dd1ecc})

[comment]: # ({new-dbb8bb8d})
### Parameters 参数

`(object/array)` Media types to create. `(object/array)`创建媒介类型

The method accepts media types with the [standard media type
properties](object#media_type). 该方法接受媒介类型关于 [standard media
type properties](object#media_type).

[comment]: # ({/new-dbb8bb8d})

[comment]: # ({new-b1eed639})
### Return values 返回值

`(object)` Returns an object containing the IDs of the created media
types under the `mediatypeids` property. The order of the returned IDs
matches the order of the passed media types. `(object)`
返回一个包含在“mediatypeids”属性下创建的媒体类型的ids的对象，返回id的顺序与传递的媒介类型的顺序匹配。

[comment]: # ({/new-b1eed639})

[comment]: # ({new-b41637d2})
### Examples 示例如下

[comment]: # ({/new-b41637d2})

[comment]: # ({new-e61703a8})
#### Creating a media type 创建一个媒介类型

Create a new e-mail media type. 创建一个新的邮件媒介类型

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "mediatype.create",
    "params": {
        "description": "E-mail",
        "type": 0,
        "smtp_server": "rootmail@company.com",
        "smtp_helo": "company.com",
        "smtp_email": "zabbix@company.com"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "mediatypeids": [
            "7"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-e61703a8})

[comment]: # ({new-1711e041})
#### Creating a media type with custom options 创建具有自定义选项的媒体类型

Create a new script media type with custom value for number of attempts
and interval between them.
创建一个具有自定义值的新脚本媒体类型，用于尝试次数和尝试间隔。

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "mediatype.create",
    "params": {
        "type": 1,
        "description": "Push notifications",
        "exec_path": "push-notification.sh",
        "exec_params": "{ALERT.SENDTO}\n{ALERT.SUBJECT}\n{ALERT.MESSAGE}\n",
        "maxattempts": "5",
        "attempt_interval": "11s"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "mediatypeids": [
            "8"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-1711e041})

[comment]: # ({new-9932dd76})
### Source

CMediaType::create() in
*frontends/php/include/classes/api/services/CMediaType.php*.

[comment]: # ({/new-9932dd76})


[comment]: # ({new-06979650})
### Source

CMediaType::create() in
*ui/include/classes/api/services/CMediaType.php*.

[comment]: # ({/new-06979650})
