[comment]: # translation:outdated

[comment]: # ({new-8102730f})
# drule.create

[comment]: # ({/new-8102730f})

[comment]: # ({new-9e7d03a2})
### Description

[comment]: # ({/new-9e7d03a2})

[comment]: # ({new-7afdb3b3})
### 描述

`object drule.create(object/array discroveryRules)`
`对象 drule.create(object/array discroveryRules)`

This method allows to create new discrovery rules.
该方法用于创建新的发现规则。

[comment]: # ({/new-7afdb3b3})

[comment]: # ({new-2eb6d0d5})
### Parameters

[comment]: # ({/new-2eb6d0d5})

[comment]: # ({new-b41637d2})
### 参数

`(object/array)` Discrovery rules to create. `(对象/数组)`
要创建的发现规则。

Additionally to the [standard discrovery rule
properties](object#discrovery_rule), the method accepts the following
parameters.
除了[标准的发现规则属性](object#discrovery_rule)之外，该方法还接受以下参数：

|Parameter|Type|Description|
|---------|----|-----------|
|**dchecks**<br>(required)|array|Discovery checks to create for the discovery rule.|
|参数           类|描述|<|
|**dchecks**<br>(必选)|array|为发现规则创建发现检查|

[comment]: # ({/new-b41637d2})

[comment]: # ({new-b03b87c1})
### Return values

[comment]: # ({/new-b03b87c1})

[comment]: # ({new-54664cce})
### 返回值

`(object)` Returns an object containing the IDs of the created
discrovery rules under the `druleids` property. The order of the
returned IDs matches the order of the passed discrovery rules. `(对象)`
在 `druleids` 属性下，返回一个包含已创建的发现规则的ID的对象。
返回的ID的顺序与传递的发现规则的顺序相匹配。

[comment]: # ({/new-54664cce})

[comment]: # ({new-1333854c})
### Examples

### 例子

#### Create a discovery rule

#### 创建发现规则

Create a discovery rule to find machines running the Zabbix agent in the
local network. The rule must use a single Zabbix agent check on port
10050. 创建一个发现规则，用于发现在本地网络中运行Zabbix Agent的主机.
此规则必须用在在10050端口运行的Zabbix
agent下【备注zmhuang:怀疑原文语法有错误 The rule must use in a single
Zabbix agent checked on port 10050】

Request: 请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "drule.create",
    "params": {
        "name": "Zabbix agent discovery",
        "iprange": "192.168.1.1-255",
        "dchecks": [
            {
                "type": "9",
                "key_": "system.uname",
                "ports": "10050",
                "uniq": "0"
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response: 响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "druleids": [
            "6"
        ]
    },
    "id": 1
}
```

### See also

### 参考

-   [Discovery
    check](/manual/api/reference/dcheck/object#discovery_check)
-   [发现检查](/zh/manual/api/reference/dcheck/object#discovery_check)

### Source

### 来源

CDRule::create() in
*frontends/php/include/classes/api/services/CDRule.php*.

[comment]: # ({/new-1333854c})
