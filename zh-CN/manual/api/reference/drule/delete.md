[comment]: # translation:outdated

[comment]: # ({new-f125871b})
# drule.delete

[comment]: # ({/new-f125871b})

[comment]: # ({new-2430e072})
### Description

[comment]: # ({/new-2430e072})

[comment]: # ({new-5b654054})
### 描述

`object drule.delete(array discoveryRuleIds)`
`对象 drule.delete(array discoveryRuleIds)`

This method allows to delete discovery rules. 该方法用于删除发现规则。

[comment]: # ({/new-5b654054})

[comment]: # ({new-35b8a89c})
### Parameters

[comment]: # ({/new-35b8a89c})

[comment]: # ({new-b41637d2})
### 参数

`(array)` IDs of the discovery rules to delete. `(数组)`
要删除的发现规则的ID

[comment]: # ({/new-b41637d2})

[comment]: # ({new-4df0ae86})
### Return values

[comment]: # ({/new-4df0ae86})

[comment]: # ({new-89517129})
### 返回值

`(object)` Returns an object containing the IDs of the deleted discovery
rules under the `druleids` property. `(对象)` 在 `druleids`
属性下，返回包含已删除的发现规则的ID的对象.

### Examples

### 例子

#### Delete multiple discovery rules

#### 删除多个发现规则

Delete two discovery rules. 删除两个发现规则

Request: 请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "drule.delete",
    "params": [
        "4",
        "6"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Response: 响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "druleids": [
            "4",
            "6"
        ]
    },
    "id": 1
}
```

### Source

### 来源

CDRule::delete() in
*frontends/php/include/classes/api/services/CDRule.php*.

[comment]: # ({/new-89517129})
