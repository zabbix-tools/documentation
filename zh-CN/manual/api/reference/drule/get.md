[comment]: # translation:outdated

[comment]: # ({new-dbbaf81a})
# drule.get

[comment]: # ({/new-dbbaf81a})

[comment]: # ({new-bbd48f20})
### Description

[comment]: # ({/new-bbd48f20})

[comment]: # ({new-0b971cc5})
### 描述

`integer/array drule.get(object parameters)`
`整数/数组 drule.get(object parameters)`

The method allows to retrieve discovery rules according to the given
parameters. 该方法用于根据给定的参数获取发现规则。

[comment]: # ({/new-0b971cc5})

[comment]: # ({new-7223bab1})
### Parameters

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### 参数

`(object)` Parameters defining the desired output. `(对象)`
定义所需输出的参数.

The method supports the following parameters. 该方法支持以下参数。

|Parameter|Type|Description|
|---------|----|-----------|
|dhostids|string/array|Return only discovery rules that created the given discovered hosts.|
|druleids|string/array|Return only discovery rules with the given IDs.|
|dserviceids|string/array|Return only discovery rules that created the given discovered services.|
|selectDChecks|query|Return discovery checks used by the discovery rule in the `dchecks` property.<br><br>Supports `count`.|
|selectDHosts|query|Return the discovered hosts that the discovery rule created in the `dhosts` property.<br><br>Supports `count`.|
|limitSelects|integer|Limits the number of records returned by subselects.<br><br>Applies to the following subselects:<br>`selectDChecks` - results will be sorted by `dcheckid`;<br>`selectDHosts` - results will be sorted by `dhostsid`.|
|sortfield|string/array|Sort the result by the given properties.<br><br>Possible values are: `druleid` and `name`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters).|
|editable|boolean|^|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

|参数                     类|描述|<|
|------------------------------|------|-|
|dhostids|string/array|仅返回创建给定已发现主机的发现规则|
|druleids|string/array|仅返回给定ID的发现规则|
|dserviceids|string/array|仅返回创建给定已发现服务的发现规则|
|selectDChecks|query|在`dchecks` 属性下，返回被发现规则使用的发现检查<br><br>支持`count`.|
|selectDHosts|query|在`dhosts`属性下，返回发现规则创建的发现主机<br><br>支持`count`.|
|limitSelects|integer|限制子选项返回的记录数<br><br>适用于以下子选项:<br>`selectDChecks` - 结果按`dcheckid`排序;<br>`selectDHosts` - 结果按`dhostsid`排序|
|sortfield|string/array|结果按给定属性排序.<br><br>可能的值: `druleid`和`name`.|
|countOutput|boolean|以下参数为`get`方法通常参数，在[参考注释](/manual/api/reference_commentary#common_get_method_parameters)有详细说明|
|editable|boolean|^|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

[comment]: # ({/new-b41637d2})

[comment]: # ({new-153ca11b})
### Return values

[comment]: # ({/new-153ca11b})

[comment]: # ({new-f2f964c1})
### 返回值

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.

`(整数/数组)` 返回:

-   对象数据;
-   如果`countOutput`被使用，返回获取对象的计数.

[comment]: # ({/new-f2f964c1})

[comment]: # ({new-365d15ae})
### Examples

### 例子

#### Retrieve all discovery rules

#### 获取所有发现规则

Retrieve all configured discovery rules and the discovery checks they
use. 获取所有已配置的发现规则及使用的发现检查。

Request: 请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "drule.get",
    "params": {
        "output": "extend",
        "selectDChecks": "extend"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response: 响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "druleid": "2",
            "proxy_hostid": "0",
            "name": "Local network",
            "iprange": "192.168.3.1-255",
            "delay": "5s",
            "nextcheck": "1348754327",
            "status": "0",
            "dchecks": [
                {
                    "dcheckid": "7",
                    "druleid": "2",
                    "type": "3",
                    "key_": "",
                    "snmp_community": "",
                    "ports": "21",
                    "snmpv3_securityname": "",
                    "snmpv3_securitylevel": "0",
                    "snmpv3_authpassphrase": "",
                    "snmpv3_privpassphrase": "",
                    "uniq": "0",
                    "snmpv3_authprotocol": "0",
                    "snmpv3_privprotocol": "0"
                },
                {
                    "dcheckid": "8",
                    "druleid": "2",
                    "type": "4",
                    "key_": "",
                    "snmp_community": "",
                    "ports": "80",
                    "snmpv3_securityname": "",
                    "snmpv3_securitylevel": "0",
                    "snmpv3_authpassphrase": "",
                    "snmpv3_privpassphrase": "",
                    "uniq": "0",
                    "snmpv3_authprotocol": "0",
                    "snmpv3_privprotocol": "0"
                }
            ]
        },
        {
            "druleid": "6",
            "proxy_hostid": "0",
            "name": "Zabbix agent discovery",
            "iprange": "192.168.1.1-255",
            "delay": "1h",
            "nextcheck": "0",
            "status": "0",
            "dchecks": [
                {
                    "dcheckid": "10",
                    "druleid": "6",
                    "type": "9",
                    "key_": "system.uname",
                    "snmp_community": "",
                    "ports": "10050",
                    "snmpv3_securityname": "",
                    "snmpv3_securitylevel": "0",
                    "snmpv3_authpassphrase": "",
                    "snmpv3_privpassphrase": "",
                    "uniq": "0",
                    "snmpv3_authprotocol": "0",
                    "snmpv3_privprotocol": "0"
                }
            ]
        }
    ],
    "id": 1
}
```

### See also

### 参考

-   [Discovered
    host](/manual/api/reference/dhost/object#discovered_host)
-   [Discovery
    check](/manual/api/reference/dcheck/object#discovery_check)
-   [已发现主机](/zh/manual/api/reference/dhost/object#discovered_host)
-   [发现检查](/zh/manual/api/reference/dcheck/object#discovery_check)

### Source

### 来源

CDRule::get() in
*frontends/php/include/classes/api/services/CDRule.php*.

[comment]: # ({/new-365d15ae})
