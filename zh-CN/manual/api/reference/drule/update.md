[comment]: # translation:outdated

[comment]: # ({new-0a1af107})
# drule.update

[comment]: # ({/new-0a1af107})

[comment]: # ({new-ac17a9e2})
### Description

[comment]: # ({/new-ac17a9e2})

[comment]: # ({new-0dd5b0c8})
### 描述

`object drule.update(object/array discoveryRules)`
`对象 drule.update(object/array discoveryRules)`

This method allows to update existing discovery rules.
该方法用于更新已存在的发现规则。

[comment]: # ({/new-0dd5b0c8})

[comment]: # ({new-495f933e})
### Parameters

[comment]: # ({/new-495f933e})

[comment]: # ({new-b41637d2})
### 参数

`(object/array)` Discovery rule properties to be updated. `(对象/数组)`
要更新的发现规则属性

The `druleid` property must be defined for each discovery rule, all
other properties are optional. Only the passed properties will be
updated, all others will remain unchanged. 必须为每条发现规则定义
`druleid` 属性,其它属性是可选的.
只有传参进去的属性才会被更新，其它属性不变.

Additionally to the [standard discovery rule
properties](object#discovery_rule), the method accepts the following
parameters. 除了[标准的发现规则属性](object#discovery_rule)外,
该方法还接受以下参数.

|Parameter|Type|Description|
|---------|----|-----------|
|dchecks|array|Discovery checks to replace existing checks.|

|参数      类|描述|<|
|---------------|------|-|
|dchecks|array|替代已存在的发现检查.|

[comment]: # ({/new-b41637d2})

[comment]: # ({new-11119b8e})
### Return values

[comment]: # ({/new-11119b8e})

[comment]: # ({new-54664cce})
### 返回值

`(object)` Returns an object containing the IDs of the updated discovery
rules under the `druleids` property. `(对象)` 在 `druleids`
属性下，返回包含已更新的发现规则的ID对象.

[comment]: # ({/new-54664cce})

[comment]: # ({new-d9252108})
### Examples

### 例子

#### Change the IP range of a discovery rule

#### 更改发现规则的IP范围

Change the IP range of a discovery rule to "192.168.2.1-255".
将发现规则的IP范围更改为192.168.2.1-255

Request: 请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "drule.update",
    "params": {
        "druleid": "6",
        "iprange": "192.168.2.1-255"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response: 响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "druleids": [
            "6"
        ]
    },
    "id": 1
}
```

### See also

### 参考

-   [Discovery
    check](/manual/api/reference/dcheck/object#discovery_check)
-   [发现检查](/zh/manual/api/reference/dcheck/object#discovery_check)

### Source

### 来源

CDRule::update() in
*frontends/php/include/classes/api/services/CDRule.php*.

[comment]: # ({/new-d9252108})
