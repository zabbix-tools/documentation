[comment]: # translation:outdated

[comment]: # ({new-141f308b})
# > Discovery rule object

[comment]: # ({/new-141f308b})

[comment]: # ({new-8884a1d1})
## > 发现规则对象

The following objects are directly related to the `drule` API. 以下是和
`drule` API相关的对象。

### Discovery rule

### 发现规则

The discovery rule object defines a network discovery rule. It has the
following properties. 发现规则对象用于定义网络发现规则.它有如下属性:

|Property|Type|Description|
|--------|----|-----------|
|druleid|string|*(readonly)* ID of the discovery rule.|
|**iprange**<br>(required)|string|One or several IP ranges to check separated by commas.<br><br>Refer to the [network discovery configuration](/manual/discovery/network_discovery/rule) section for more information on supported formats of IP ranges.|
|**name**<br>(required)|string|Name of the discovery rule.|
|delay|string|Execution interval of the discovery rule. Accepts seconds, time unit with suffix and user macro.<br><br>Default: 1h.|
|nextcheck|timestamp|*(readonly)* Time when the discovery rule will be executed next.|
|proxy\_hostid|string|ID of the proxy used for discovery.|
|status|integer|Whether the discovery rule is enabled.<br><br>Possible values:<br>0 - *(default)* enabled;<br>1 - disabled.|

|属性            类|描述|<|
|---------------------|------|-|
|druleid|string|*(只读)* 发现规则的ID|
|**iprange**<br>(必选)|string<br>|一个或多个要检查的IP范围，用逗号进行分隔。<br>更多有关IP范围的支持格式的信息，请参考[网络发现规则配置](/zh/manual/discovery/network_discovery/rule)。|
|**name**<br>(必选)|string|发现规则名称|
|delay|string|发现规则的执行间隔。支持秒、用户宏以及带后缀的时间单位.<br><br>默认: 1h.|
|nextcheck|timestamp|*(只读)* 发现规则下一次执行的时间。|
|proxy\_hostid|string|用于发现的proxy的ID.|
|status|integer|发现规则是否启用.<br><br>可选值:<br>0 - *(默认)* 启用;<br>1 - 禁用.|

[comment]: # ({/new-8884a1d1})
