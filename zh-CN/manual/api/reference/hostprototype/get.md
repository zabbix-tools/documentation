[comment]: # translation:outdated

[comment]: # ({new-31d5554a})
# 获取

[comment]: # ({/new-31d5554a})

[comment]: # ({new-d1534f8a})
### 描述

`integer/array hostprototype.get(object parameters)`

该方法允许根据给定的参数获取主机原型记录.

[comment]: # ({/new-d1534f8a})

[comment]: # ({new-0d6ab1f2})
### 参数

`(对象)` 定义要输出的参数.

该方法支持如下属性:

|参数                     类|描述|<|
|------------------------------|------|-|
|hostids|字符串/数组   返回给定|D的主机原型.|
|discoveryids|字符串/数组   返回归属|定LLD规则的主机原型.|
|inherited|布尔值        如果|置为true,只返回模板从模板继承的项目.|
|selectDiscoveryRule|查询          在|discoveryRule`属性中返回主机原型归属的LLD规则.|
|selectGroupLinks|查询          在|groupLinks`属性中返回主机原型的组连接.|
|selectGroupPrototypes|查询          在|groupPrototypes`属性中返回主机原型的组原型.|
|selectInventory|查询          在|inventory`属性中返回主机原型资产信息.|
|selectParentHost|查询          在|parentHost`属性中返回主机原型所属的主机.|
|selectTemplates|查询          在|templates`属性中返回连接到主机原型的模板.|
|sortfield|字符串/数组   按照给定|属性对结果进行排序.<br><br>可能的值: `hostid`, `host`, `name` and `status`.|
|countOutput|布尔值        这些|数对于所有get方法都是通用的，详情可参考[Generic Zabbix API information](/manual/api/reference_commentary#common_get_method_parameters).|
|editable|布尔值        ::|<|
|excludeSearch|布尔值        ::|<|
|filter|对象          :|:|
|limit|整数          :|:|
|output|查询          :|:|
|preservekeys|布尔值        ::|<|
|search|对象          :|:|
|searchByAny|布尔值        ::|<|
|searchWildcardsEnabled|布尔值        ::|<|
|sortorder|字符串/数组   :::|<|
|startSearch|布尔值        ::|<|

[comment]: # ({/new-0d6ab1f2})

[comment]: # ({new-7223bab1})
### 返回值

`(整数/数组)` 返回:

-   一组对象;
-   如果设置了`countOutput`参数,则返回对象的数量.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### 示例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-a91c8b17})
#### 从LLD规则中获取主机原型

从LLD规则中获取所有主机原型及其组链接和组原型

请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "hostprototype.get",
    "params": {
        "output": "extend",
        "selectGroupLinks": "extend",
        "selectGroupPrototypes": "extend",
        "discoveryids": "23554"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "hostid": "10092",
            "host": "{#HV.UUID}",
            "status": "0",
            "name": "{#HV.NAME}",
            "templateid": "0",
            "tls_connect": "1",
            "tls_accept": "1",
            "tls_issuer": "",
            "tls_subject": "",
            "tls_psk_identity": "",
            "tls_psk": "",
            "groupLinks": [
                {
                    "group_prototypeid": "4",
                    "hostid": "10092",
                    "groupid": "7",
                    "templateid": "0"
                }
            ],
            "groupPrototypes": [
                {
                    "group_prototypeid": "7",
                    "hostid": "10092",
                    "name": "{#CLUSTER.NAME}",
                    "templateid": "0"
                }
            ]
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-a91c8b17})

[comment]: # ({new-749ec170})
### 参考

-   [Group link](object#group_link)
-   [Group prototype](object#group_prototype)
-   [Host prototype inventory](object#host_prototype_inventory)

[comment]: # ({/new-749ec170})

[comment]: # ({new-437ab274})
### 来源

CHostPrototype::get() in
*frontends/php/include/classes/api/services/CHostPrototype.php*.

[comment]: # ({/new-437ab274})
