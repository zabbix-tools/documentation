[comment]: # translation:outdated

[comment]: # ({new-c0df6427})
# 更新

[comment]: # ({/new-c0df6427})

[comment]: # ({new-796670ab})
### 描述

`object hostprototype.update(object/array hostPrototypes)`

此方法允许更新已存在的主机原型.

[comment]: # ({/new-796670ab})

[comment]: # ({new-50727275})
### 参数

`(对象/数组)` 要更新的主机原型属性.

必须为每个主机原型定义`hostid`属性，所有其他属性都是可选的。
只有过期的属性将被更新，所有其他属性将保持不变.
除[标准主机原型属性](object#host_prototype)外，该方法还接受以下参数:

|参数              类|描述|<|
|-----------------------|------|-|
|groupLinks|数组        组|接来替换主机原型上的当前组链接.|
|groupPrototypes|数组        组|型替主机原型中已存在的组原型.|
|inventory|对象        主|原型资产属性.|
|templates|对象/数组   替换当|已连接的模板的模板.<br><br>模板必须已定义`templateid`属性.|

[comment]: # ({/new-50727275})

[comment]: # ({new-c97defa2})
### 返回值

`(对象)` 在`hostids`属性中放回已更新主机原型ID的对象.

[comment]: # ({/new-c97defa2})

[comment]: # ({new-b41637d2})
### 示例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-9e34a671})
#### 禁用主机原型

通过将status状态设置为1，可禁用主机原型.

请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "hostprototype.update",
    "params": {
        "hostid": "10092",
        "status": 1
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "hostids": [
            "10092"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-9e34a671})

[comment]: # ({new-440003e2})
### 参考

-   [Group link](object#group_link)
-   [Group prototype](object#group_prototype)
-   [Host prototype inventory](object#host_prototype_inventory)

[comment]: # ({/new-440003e2})

[comment]: # ({new-7a7b18a4})
### 来源

CHostPrototype::update() in
*frontends/php/include/classes/api/services/CHostPrototype.php*.

[comment]: # ({/new-7a7b18a4})



[comment]: # ({new-c4a38bf3})
### See also

-   [Group link](object#group_link)
-   [Group prototype](object#group_prototype)
-   [Host prototype tag](object#host_prototype_tag)
-   [Custom interface](object#custom_interface)
-   [User
    macro](/manual/api/reference/usermacro/object#hosttemplate_level_macro)

[comment]: # ({/new-c4a38bf3})

[comment]: # ({new-a380e3fb})
### Source

CHostPrototype::update() in
*ui/include/classes/api/services/CHostPrototype.php*.

[comment]: # ({/new-a380e3fb})
