[comment]: # translation:outdated

[comment]: # ({new-6e26e29b})
# > 主机原型对象

以下对象与`主机原型`API直接相关.

[comment]: # ({/new-6e26e29b})

[comment]: # ({new-e69daa4d})
### 主机原型

主机原型对象具有以下属性:

|属性                 类|描述|<|
|--------------------------|------|-|
|hostid|字符串   *(|读)* 主机原型的ID.|
|**host**<br>(必选)|字符串   主机|型的技术名称.|
|name|字符串   主机|型的可见名称.<br><br>默认: `host` 属性的值.|
|status|整数     主|原型的状态.<br><br>可能的值:<br>0 - *(默认)* 被监控的主机;<br>1 - 不受监控的主机.|
|templateid|字符串   *(|读)* 父模板主机原型的ID.|
|tls\_connect|整数     到|机的连接.<br><br>可能的值:<br>1 - *(默认)* 无加密 ;<br>2 - PSK;<br>4 - 证书.|
|tls\_accept|整数     来|主机的连接.<br><br>可能的值:<br>1 - *(默认)* 无加密 ;<br>2 - PSK;<br>4 - 证书.|
|tls\_issuer|字符串   证书|行者.|
|tls\_subject|字符串   证书|体.|
|tls\_psk\_identity|字符串   PS|身份 如果`tls_connect`或`tls_accept`启用了PSK，则必需.|
|tls\_psk|字符串   预共|密钥，至少32位十六进制数字。 如果`tls_connect`或`tls_accept`启用了PSK，则必需.|

[comment]: # ({/new-e69daa4d})

[comment]: # ({new-e3297546})
### 主机原型资产

主机原型资产对象有以下属性:

|属性              类|描述|<|
|-----------------------|------|-|
|inventory\_mode|整数   主|原型资产模式.<br><br>可能的值:<br>-1 - 禁用;<br>0 - *(默认)* 手动;<br>1 - 自动.|

[comment]: # ({/new-e3297546})

[comment]: # ({new-dabb4716})
### 组链接

组链接对象将主机原型与主机组链接，并具有以下属性:

|属性                 类|描述|<|
|--------------------------|------|-|
|group\_prototypeid|字符串   *(|读)* 组链接的ID.|
|**groupid**<br>(必选)|字符串   主机|的ID.|
|hostid|字符串   *(|读)* 主机原型的ID|
|templateid|字符串   *(|读)* 父模板组链接的ID.|

[comment]: # ({/new-dabb4716})

[comment]: # ({new-d42c6308})
### 组原型

组原型对象定义将为已发现的主机创建的组，并具有以下属性:

|属性                 类|描述|<|
|--------------------------|------|-|
|group\_prototypeid|字符串   *(|读)* 组原型的ID.|
|**name**<br>(必选)|字符串   组原|的名称.|
|hostid|字符串   *(|读)* 主机原型的ID|
|templateid|字符串   *(|读)* 父模板组原型的ID.|

[comment]: # ({/new-d42c6308})

[comment]: # ({new-ed8d8550})
### Custom interface

The custom interface object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|dns|string|DNS name used by the interface.<br><br>**Required** if the connection is made via DNS. Can contain macros.|
|ip|string|IP address used by the interface.<br><br>**Required** if the connection is made via IP. Can contain macros.|
|**main**<br>(required)|integer|Whether the interface is used as default on the host. Only one interface of some type can be set as default on a host.<br><br>Possible values are:<br>0 - not default;<br>1 - default.|
|**port**<br>(required)|string|Port number used by the interface. Can contain user and LLD macros.|
|**type**<br>(required)|integer|Interface type.<br><br>Possible values are:<br>1 - agent;<br>2 - SNMP;<br>3 - IPMI;<br>4 - JMX.<br>|
|**useip**<br>(required)|integer|Whether the connection should be made via IP.<br><br>Possible values are:<br>0 - connect using host DNS name;<br>1 - connect using host IP address for this host interface.|
|details|array|Additional object for interface. **Required** if interface 'type' is SNMP.|

[comment]: # ({/new-ed8d8550})



[comment]: # ({new-6ea1b012})
### Custom interface details

The details object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**version**<br>(required)|integer|SNMP interface version.<br><br>Possible values are:<br>1 - SNMPv1;<br>2 - SNMPv2c;<br>3 - SNMPv3|
|bulk|integer|Whether to use bulk SNMP requests.<br><br>Possible values are:<br>0 - don't use bulk requests;<br>1 - *(default)* - use bulk requests.|
|community|string|SNMP community. Used only by SNMPv1 and SNMPv2 interfaces.|
|securityname|string|SNMPv3 security name. Used only by SNMPv3 interfaces.|
|securitylevel|integer|SNMPv3 security level. Used only by SNMPv3 interfaces.<br><br>Possible values are:<br>0 - *(default)* - noAuthNoPriv;<br>1 - authNoPriv;<br>2 - authPriv.|
|authpassphrase|string|SNMPv3 authentication passphrase. Used only by SNMPv3 interfaces.|
|privpassphrase|string|SNMPv3 privacy passphrase. Used only by SNMPv3 interfaces.|
|authprotocol|integer|SNMPv3 authentication protocol. Used only by SNMPv3 interfaces.<br><br>Possible values are:<br>0 - *(default)* - MD5;<br>1 - SHA1;<br>2 - SHA224;<br>3 - SHA256;<br>4 - SHA384;<br>5 - SHA512.|
|privprotocol|integer|SNMPv3 privacy protocol. Used only by SNMPv3 interfaces.<br><br>Possible values are:<br>0 - *(default)* - DES;<br>1 - AES128;<br>2 - AES192;<br>3 - AES256;<br>4 - AES192C;<br>5 - AES256C.|
|contextname|string|SNMPv3 context name. Used only by SNMPv3 interfaces.|

[comment]: # ({/new-6ea1b012})
