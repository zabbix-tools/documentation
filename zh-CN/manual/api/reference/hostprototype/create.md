[comment]: # translation:outdated

[comment]: # ({new-984854ac})
# 创建

[comment]: # ({/new-984854ac})

[comment]: # ({new-3ddb6b5d})
### 描述

`object hostprototype.create(object/array hostPrototypes)`

此方法允许创建新的主机原型.

[comment]: # ({/new-3ddb6b5d})

[comment]: # ({new-c4127cc1})
### 参数

`(对象/数组)` 要创建的主机原型.

除[标准主机原型属性](object#host_prototype)之外，该方法接受以下参数.

|参数              类|描述|<|
|-----------------------|------|-|
|**groupLinks**<br>(必选)|数组        要|主机原型创建的组链接.|
|**ruleid**<br>(必选)|字符串      主机|型所属的LLD规则的ID.|
|groupPrototypes|数组        将|主机原型创建的组原型.|
|inventory|对象        主|原型资产属性.|
|templates|对象/数组   连接到|机原型的模板.<br><br>模板必须已定义`templateid`属性.|

[comment]: # ({/new-c4127cc1})

[comment]: # ({new-b9613119})
### 返回值

`(object)`
在`hostids`属性中返回已创建主机原型ID的对象,返回ID的顺序与传入主机原型的顺序一致.

[comment]: # ({/new-b9613119})

[comment]: # ({new-b41637d2})
### 示例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-0ce023ac})
#### 创建主机原型

使用组原型`{＃HV.NAME}`为LLD规则`23542`,创建主机原型`{＃VM.NAME}`,连接到主机组`2`

请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "hostprototype.create",
    "params": {
        "host": "{#VM.NAME}",
        "ruleid": "23542",
        "groupLinks": [
            {
                "groupid": "2"
            }
        ],
        "groupPrototypes": [
            {
                "name": "{#HV.NAME}"
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "hostids": [
            "10103"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-0ce023ac})

[comment]: # ({new-c4a38bf3})
### 参考

-   [Group link](object#group_link)
-   [Group prototype](object#group_prototype)
-   [Host prototype inventory](object#host_prototype_inventory)

[comment]: # ({/new-c4a38bf3})

[comment]: # ({new-4d10a354})
### 来源

CHostPrototype::create() in
*frontends/php/include/classes/api/services/CHostPrototype.php*.

[comment]: # ({/new-4d10a354})
