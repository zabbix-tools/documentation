[comment]: # translation:outdated

[comment]: # ({new-8f15992f})
# 删除

[comment]: # ({/new-8f15992f})

[comment]: # ({new-f7c792a6})
### 描述

`object hostprototype.delete(array hostPrototypeIds)`

该方法允许删除主机原型.

[comment]: # ({/new-f7c792a6})

[comment]: # ({new-65284f99})
### 参数

`(数组)` 要删除主机原型的ID.

[comment]: # ({/new-65284f99})

[comment]: # ({new-78e6f07c})
### 返回值

`(对象)` 在`hostids`属性中返回已删除主机原型ID的对象.

[comment]: # ({/new-78e6f07c})

[comment]: # ({new-b41637d2})
### 示例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-48be53a2})
#### 删除多个主机原型

删除两个主机原型

请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "hostprototype.delete",
    "params": [
        "10103",
        "10105"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "hostids": [
            "10103",
            "10105"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-48be53a2})

[comment]: # ({new-dd993f84})
### 来源

CHostPrototype::delete() in
*frontends/php/include/classes/api/services/CHostPrototype.php*.

[comment]: # ({/new-dd993f84})
