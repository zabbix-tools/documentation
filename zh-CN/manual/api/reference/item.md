[comment]: # translation:outdated

[comment]: # ({new-3816cd64})
# Item 监控项

## 监控项

This class is designed to work with items. 此类用于管理监控项。

Object references:\
对象引用：\

-   [Item](/manual/api/reference/item/object#item)
-   [Item](/zh/manual/api/reference/item/object#item)

Available methods:\
可用的方法：\

-   [item.create](/manual/api/reference/item/create) - creating new
    items
-   [item.create](/zh/manual/api/reference/item/create) - 创建新监控项

```{=html}
<!-- -->
```
-   [item.delete](/manual/api/reference/item/delete) - deleting items
-   [item.delete](/zh/manual/api/reference/item/delete) - 删除监控项

```{=html}
<!-- -->
```
-   [item.get](/manual/api/reference/item/get) - retrieving items
-   [item.get](/zh/manual/api/reference/item/get) - 检索监控项

```{=html}
<!-- -->
```
-   [item.update](/manual/api/reference/item/update) - updating items
-   [item.update](/zh/manual/api/reference/item/update) - 更新监控项

[comment]: # ({/new-3816cd64})
