[comment]: # translation:outdated

[comment]: # ({new-c179cb27})
# 创建

[comment]: # ({/new-c179cb27})

[comment]: # ({new-00970602})
### 描述

`object dashboard.create(object/array dashboards)`

这个方法允许创建新的`仪表板`。

[comment]: # ({/new-00970602})

[comment]: # ({new-df2301b4})
### 参数

`(object/array)` 要创建的`仪表板`。

另外，对于[标准仪表板属性](object#dashboard)，该方法还接受以下参数。

|参数         类|描述|<|
|------------------|------|-|
|widgets|数组   将|仪表板创建的[仪表板小组件](object#dashboard_widget)。|
|users|数组   将|仪表板上创建的[仪表板用户](object#dashboard_user)共享。|
|userGroups|数组   将|仪表板上创建的[仪表板用户组](object#dashboard_user_group)共享。|

[comment]: # ({/new-df2301b4})

[comment]: # ({new-f460a18e})
### 返回值

`(object)` 返回一个对象，该对象包含 `dashboardids`
属性下创建的`仪表板`的 ID。返回的 ID
的顺序与所传递的`仪表板`的顺序相匹配。

[comment]: # ({/new-f460a18e})

[comment]: # ({new-b41637d2})
### 示例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-b33dc2ee})
#### 创建一个仪表板

创建一个名为 “My dashboard”
的`仪表板`，其中有一个带有标签的问题小部件，并使用了两种类型的共享（用户组和用户）。

请求：

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "dashboard.create",
    "params": {
        "name": "My dashboard",
        "widgets": [
            {
                "type": "problems",
                "x": 0,
                "y": 0,
                "width": 6,
                "height": 5,
                "fields": [
                    {
                        "type": 1,
                        "name": "tags.tag.0",
                        "value": "service"
                    },
                    {
                        "type": 1,
                        "name": "tags.value.0",
                        "value": "zabbix_server"
                    }
                ]
            }
        ],
        "userGroups": [
            {
                "usrgrpid": "7",
                "permission": "2"
            }
        ],
        "users": [
            {
                "userid": "4",
                "permission": "3"
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

响应：

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "dashboardids": [
            "2"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-b33dc2ee})

[comment]: # ({new-299ae089})
### 参见

-   [仪表板小组件](object#dashboard_widget)
-   [仪表板小组件字段](object#dashboard_widget_field)
-   [仪表板用户](object#dashboard_user)
-   [仪表板用户组](object#dashboard_user_group)

[comment]: # ({/new-299ae089})

[comment]: # ({new-7d488f32})
### 来源

CDashboard::create() in
*frontends/php/include/classes/api/services/CDashboard.php*.

[comment]: # ({/new-7d488f32})
