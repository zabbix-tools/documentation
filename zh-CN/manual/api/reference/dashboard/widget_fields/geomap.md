[comment]: # translation:outdated

[comment]: # ({new-f2076b88})
# 7 Geomap

[comment]: # ({/new-f2076b88})

[comment]: # ({new-f4e7e64a})
### Description

These parameters and the possible property values for the respective dashboard widget field objects allow to configure
the [*Geomap*](/manual/web_interface/frontend_sections/dashboards/widgets/geomap) widget in `dashboard.create` and `dashboard.update` methods.

[comment]: # ({/new-f4e7e64a})

[comment]: # ({new-90ce35af})
### Parameters

The following parameters are supported for the *Geomap* widget.

|Parameter|<|[type](/manual/api/reference/dashboard/object#dashboard-widget-field)|name|value|
|-|--------|--|--------|-------------------------------|
|*Refresh interval*|<|0|rf_rate|0 - No refresh;<br>10 - 10 seconds;<br>30 - 30 seconds;<br>60 - *(default)* 1 minute;<br>120 - 2 minutes;<br>600 - 10 minutes;<br>900 - 15 minutes.|
|*Host groups*|<|2|groupids|[Host group](/manual/api/reference/hostgroup/get) ID.<br><br>Note: To configure multiple host groups, create a dashboard widget field object for each host group.|
|*Hosts*|<|3|hostids|[Host](/manual/api/reference/host/get) ID.<br><br>Note: To configure multiple hosts, create a dashboard widget field object for each host. For multiple hosts, the parameter *Host groups* must either be not configured at all or configured with at least one host group that the configured hosts belong to.|
|*Tags* (the number in the property name (e.g. tags.tag.0) references tag order in the tag evaluation list)|<|<|<|<|
|<|*Evaluation type*|0|evaltype|0 - *(default)* And/Or;<br>2 - Or.|
|^|*Tag name*|1|tags.tag.0|Any string value.<br><br>Parameter *Tag name* required if configuring *Tags*.|
|^|*Operator*|0|tags.operator.0|0 - Contains;<br>1 - Equals;<br>2 - Does not contain;<br>3 - Does not equal;<br>4 - Exists;<br>5 - Does not exist.<br><br>Parameter *Operator* required if configuring *Tags*.|
|^|*Tag value*|1|tags.value.0|Any string value.<br><br>Parameter *Tag value* required if configuring *Tags*.|
|*Initial view*|<|1|default_view|Comma separated *latitude*, *longitude*, *zoom level* (*optional*, valid values range from 0-30).<br>Example: `40.6892494,-74.0466891,10`.|

[comment]: # ({/new-90ce35af})

[comment]: # ({new-7eae8154})
### Examples

The following examples aim to only describe the configuration of the dashboard widget field objects for the *Geomap* widget.
For more information on configuring a dashboard, see [`dashboard.create`](/manual/api/reference/dashboard/create).

[comment]: # ({/new-7eae8154})

[comment]: # ({new-a5de665c})
#### Configuring a *Geomap* widget

Configure a *Geomap* widget that displays hosts from host groups "2" and "22" based on the following tag configuration:
tag with the name "component" contains value "node", and tag with the name "location" equals value "New York".
In addition, set the map initial view to coordinates "40.6892494" (latitude), "-74.0466891" (longitude) with the zoom level "10".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "dashboard.create",
    "params": {
        "name": "My dashboard",
        "display_period": 30,
        "auto_start": 1,
        "pages": [
            {
                "widgets": [
                    {
                        "type": "geomap",
                        "name": "Geomap",
                        "x": 0,
                        "y": 0,
                        "width": 12,
                        "height": 5,
                        "view_mode": 0,
                        "fields": [
                            {
                                "type": 2,
                                "name": "groupids",
                                "value": 22
                            },
                            {
                                "type": 2,
                                "name": "groupids",
                                "value": 2
                            },
                            {
                                "type": 1,
                                "name": "default_view",
                                "value": "40.6892494,-74.0466891,10"
                            },
                            {
                                "type": 1,
                                "name": "tags.tag.0",
                                "value": "component"
                            },
                            {
                                "type": 0,
                                "name": "tags.operator.0",
                                "value": 0
                            },
                            {
                                "type": 1,
                                "name": "tags.value.0",
                                "value": "node"
                            },
                            {
                                "type": 1,
                                "name": "tags.tag.1",
                                "value": "location"
                            },
                            {
                                "type": 0,
                                "name": "tags.operator.1",
                                "value": 1
                            },
                            {
                                "type": 1,
                                "name": "tags.value.1",
                                "value": "New York"
                            }
                        ]
                    }
                ]
            }
        ],
        "userGroups": [
            {
                "usrgrpid": 7,
                "permission": 2
            }
        ],
        "users": [
            {
                "userid": 1,
                "permission": 3
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "dashboardids": [
            "3"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-a5de665c})

[comment]: # ({new-cc20c3df})
### See also

-   [Dashboard widget field](/manual/api/reference/dashboard/object#dashboard-widget-field)
-   [`dashboard.create`](/manual/api/reference/dashboard/create)
-   [`dashboard.update`](/manual/api/reference/dashboard/update)

[comment]: # ({/new-cc20c3df})
