[comment]: # translation:outdated

[comment]: # ({new-66fb9690})
# 更新

[comment]: # ({/new-66fb9690})

[comment]: # ({new-f235159f})
### 描述

`object dashboard.update(object/array dashboards)`

这个方法允许更新已存在的`仪表板`。

[comment]: # ({/new-f235159f})

[comment]: # ({new-62947aa0})
### 参数

`(object/array)` 要更新的`仪表板`的属性。

必须为每个仪表板定义 `dashboardid`
属性，其它的属性都是可选的。只有传递的属性会被更新，其它属性都将保持不变。

另外，对于[标准仪表板属性](object#dashboard)，该方法接受以下参数。

|参数         类|描述|<|
|------------------|------|-|
|widgets|数组   替|已存在的仪表板小部件的[仪表板小组件](object#dashboard_widget)。<br><br>表板小部件由`widgetid`属性更新。将创建没有 `widgetid` 属性的小部件。|
|users|数组   替|已存在的部件的[仪表板用户](object#dashboard_user) 共享。|
|userGroups|数组   替|已存在的部件的[仪表板用户组](object#dashboard_user_group)共享。|

[comment]: # ({/new-62947aa0})

[comment]: # ({new-28e89b38})
### 返回值

`(object)` 返回一个对象，该对象包含 `dashboardids`
属性下更新的`仪表板`的 ID。

[comment]: # ({/new-28e89b38})

[comment]: # ({new-b41637d2})
### 示例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-7fcf7de4})
#### 重命名一个仪表板

将一个仪表板重命名为“SQL server 状态”。

请求：

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "dashboard.update",
    "params": {
        "dashboardid": "2",
        "name": "SQL server status"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

响应：

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "dashboardids": [
            "2"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-7fcf7de4})

[comment]: # ({new-b8348b8a})
#### 改变仪表板的属主

仅供管理员和超级管理员使用。

请求：

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "dashboard.update",
    "params": {
        "dashboardid": "2",
        "userid": "1"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 2
}
```

响应：

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "dashboardids": [
            "2"
        ]
    },
    "id": 2
}
```

[comment]: # ({/new-b8348b8a})

[comment]: # ({new-e4a57dd3})
### 参见

-   [仪表板小组件](object#dashboard_widget)
-   [仪表板小组件字段](object#dashboard_widget_field)
-   [仪表板用户](object#dashboard_user)
-   [仪表板用户组](object#dashboard_user_group)

[comment]: # ({/new-e4a57dd3})

[comment]: # ({new-299ae089})
### 来源

CDashboard::update() in
*frontends/php/include/classes/api/services/CDashboard.php*.

[comment]: # ({/new-299ae089})


[comment]: # ({new-456ac32b})
### Source

CDashboard::update() in
*ui/include/classes/api/services/CDashboard.php*.

[comment]: # ({/new-456ac32b})
