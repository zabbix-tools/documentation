[comment]: # translation:outdated

[comment]: # ({new-a79e0e24})
# > 对象

下列对象与`仪表板` API 直接相关。

[comment]: # ({/new-a79e0e24})

[comment]: # ({new-63e4049e})
### 仪表板

仪表板对象具有以下属性：

|属性          类|描述|<|
|-------------------|------|-|
|dashboardid|字符串   *（|读）* 仪表板 ID。|
|**name**<br>（需要的）|字符串   仪表|名称。|
|userid|字符串   仪表|属主的用户 ID。|
|private|整数     仪|板共享的类型。<br><br>可能的值：<br>0 - 公用仪表板；<br>1 - *（默认的）* 私有仪表板。|

[comment]: # ({/new-63e4049e})

[comment]: # ({new-b88f0a6a})
### 仪表板小部件

仪表板小部件对象具有以下属性：

|属性         类|描述|<|
|------------------|------|-|
|widgetid|字符串   *（|读）* 仪表板小部件的 ID。|
|**type**<br>（需要的）|字符串   仪表<br>|小部件的类型。<br>可能的值：<br>actionlog - 动作记录；<br>clock - 时钟；<br>dataover - 数据预览；<br>discovery - 发现状态；<br>favgraphs - 常用的图形；<br>favmaps - 常用的拓扑图；<br>favscreens - 常用的聚合图形；<br>graph - 图形；<br>problemhosts - 有问题的主机；<br>map - 拓扑图；<br>navtree - 拓扑图导航树；<br>plaintext - 纯文本；<br>problems - 问题；<br>systeminfo - 系统信息；<br>problemsbysv - 问题的严重性；<br>trigover - 触发器预览；<br>url - URL；<br>web - Web监控；|
|name|字符串   自定|的小部件名称。|
|x|整数     仪|板左侧的水平位置。<br><br>有效值范围从 0 到 11。|
|y|整数     仪|板顶部的垂直位置。<br><br>有效值范围从 0 到 63。|
|width|整数     小|件的宽度。<br><br>有效值范围从 1 到 12。|
|height|整数     小|件的高度；<br><br>有效值范围从 1 到 32。|
|fields|数组     [|表板小组件字段](object#dashboard_widget_field)对象的数组。|

[comment]: # ({/new-b88f0a6a})

[comment]: # ({new-2938b685})
### 仪表板小部件字段

仪表板小部件字段对象具有以下属性：

|属性         类|描述|<|
|------------------|------|-|
|**type**<br>（需要的）|整数     小<br>|件字段的值。<br>可能的值：<br>0 - 整数；<br>1 - 字符串；<br>2 - 主机组；<br>3 - 主机；<br>4 - 监控项；<br>6 - 图形；<br>8 - 拓扑图；|
|name|字符串   小部|字段的名称。|
|**value**<br>（需要的）|混合型   取决|类型的小部件字段值。|

[comment]: # ({/new-2938b685})

[comment]: # ({new-5b20bc04})
### 仪表板用户组

基于用户组的仪表板权限列表。其具有以下属性：

|属性              类|描述|<|
|-----------------------|------|-|
|**usrgrpid**<br>（需要的）|字符串   用户|ID。|
|**permission**<br>（需要的）|整数     权<br>|级别的类型。<br>可能的值：<br>2 - 只读；<br>3 - 读-写。|

[comment]: # ({/new-5b20bc04})

[comment]: # ({new-5c8b3411})
### 仪表板用户

基于用户的仪表板权限列表。其具有以下属性：

|属性                            类|描述|<|
|-------------------------------------|------|-|
|**userid**<br>（需要的）|字符串   用户|ID。|
|**permission** \\\\（需要的）   整数|权限级别的类|。<br><br>可能的值：<br>2 - 只读；<br>3 - 读-写。|

[comment]: # ({/new-5c8b3411})


[comment]: # ({new-d34afa0c})
### Dashboard user

List of dashboard permissions based on users. It has the following
properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**userid**<br>(required)|string|User ID.|
|**permission**<br>(required)|integer|Type of permission level.<br><br>Possible values:<br>2 - read only;<br>3 - read-write;|

[comment]: # ({/new-d34afa0c})
