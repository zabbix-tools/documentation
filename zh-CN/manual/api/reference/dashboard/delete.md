[comment]: # translation:outdated

[comment]: # ({new-faff6037})
# 删除

[comment]: # ({/new-faff6037})

[comment]: # ({new-6f29484b})
### 描述

`object dashboard.delete(array dashboardids)`

这个方法允许删除`仪表板`。

[comment]: # ({/new-6f29484b})

[comment]: # ({new-4fd471a4})
### 参数

`(array)` 要删除的`仪表板`的 ID。

[comment]: # ({/new-4fd471a4})

[comment]: # ({new-c04394d8})
### 返回值

`(object)` 返回一个对象，该对象包含 `dashboardids`
属性下删除的`仪表板`的 ID。

[comment]: # ({/new-c04394d8})

[comment]: # ({new-b41637d2})
### 示例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-4d5d4a5d})
#### 删除多个仪表板

删除 2 个仪表板

请求：

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "dashboard.delete",
    "params": [
        "2",
        "3"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

响应：

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "dashboardids": [
            "2",
            "3"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-4d5d4a5d})

[comment]: # ({new-2bc6b04b})
### 来源

CDashboard::delete() in
*frontends/php/include/classes/api/services/CDashboard.php*.

[comment]: # ({/new-2bc6b04b})
