[comment]: # translation:outdated

[comment]: # ({new-f7884974})
# 获取

[comment]: # ({/new-f7884974})

[comment]: # ({new-8d36c9b5})
### 描述

`integer/array dashboard.get(object parameters)`

这个方法允许根据给定的参数检索`仪表板`。

[comment]: # ({/new-8d36c9b5})

[comment]: # ({new-342bbd26})
### 参数

`(object)` 定义需要输出的参数。

这个方法支持以下参数。

|参数                     类|描述|<|
|------------------------------|------|-|
|dashboardids|字符串/数组   只返回拥|给定 ID 的仪表板。|
|selectWidgets|查询          返|有 `widgets` 属性，并在仪表板中使用的小部件。|
|selectUsers|查询          返|在 `users` 属性中共享仪表板的用户。|
|selectUserGroups|查询          返|在 `userGroups` 属性中共享仪表板的用户组。|
|sortfield|字符串/数组   根据给定|属性对结果进行排序。<br><br>可能的值有：`dashboardid`。|
|countOutput|布尔值        在[|用评论](/manual/api/reference_commentary#common_get_method_parameters)中详细描述了所有 `get` 方法的常见参数。|
|editable|布尔值        ::|<|
|excludeSearch|布尔值        ::|<|
|filter|对象          :|:|
|limit|整数          :|:|
|output|查询          :|:|
|preservekeys|布尔值        ::|<|
|search|对象          :|:|
|searchByAny|布尔值        ::|<|
|searchWildcardsEnabled|布尔值        ::|<|
|sortorder|字符串/数组   :::|<|
|startSearch|布尔值        ::|<|

[comment]: # ({/new-342bbd26})

[comment]: # ({new-7223bab1})
### 返回值

`(integer/array)` 返回：

-   一个对象数组；
-   如果使用了 `countOutput` 参数，被检索的对象的数量。

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### 示例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-84a10cd6})
#### 通过 ID 检索一个仪表板

检索仪表板 "1" 和 "2" 的所有数据。

请求：

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "dashboard.get",
    "params": {
        "output": "extend",
        "selectWidgets": "extend",
        "selectUsers": "extend",
        "selectUserGroups": "extend",
        "dashboardids": [
            "1",
            "2"
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

响应：

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "dashboardid": "1",
            "name": "Dashboard",
            "userid": "1",
            "private": "0",
            "users": [],
            "userGroups": [],
            "widgets": [
                {
                    "widgetid": "9",
                    "type": "systeminfo",
                    "name": "",
                    "x": "6",
                    "y": "8",
                    "width": "6",
                    "height": "5",
                    "fields": []
                },
                {
                    "widgetid": "8",
                    "type": "problemsbysv",
                    "name": "",
                    "x": "6",
                    "y": "4",
                    "width": "6",
                    "height": "4",
                    "fields": []
                },
                {
                    "widgetid": "7",
                    "type": "problemhosts",
                    "name": "",
                    "x": "6",
                    "y": "0",
                    "width": "6",
                    "height": "4",
                    "fields": []
                },
                {
                    "widgetid": "6",
                    "type": "discovery",
                    "name": "",
                    "x": "3",
                    "y": "9",
                    "width": "3",
                    "height": "4",
                    "fields": []
                },
                {
                    "widgetid": "5",
                    "type": "web",
                    "name": "",
                    "x": "0",
                    "y": "9",
                    "width": "3",
                    "height": "4",
                    "fields": []
                },
                {
                    "widgetid": "4",
                    "type": "problems",
                    "name": "",
                    "x": "0",
                    "y": "3",
                    "width": "6",
                    "height": "6",
                    "fields": []
                },
                {
                    "widgetid": "3",
                    "type": "favmaps",
                    "name": "",
                    "x": "4",
                    "y": "0",
                    "width": "2",
                    "height": "3",
                    "fields": []
                },
                {
                    "widgetid": "2",
                    "type": "favscreens",
                    "name": "",
                    "x": "2",
                    "y": "0",
                    "width": "2",
                    "height": "3",
                    "fields": []
                },
                {
                    "widgetid": "1",
                    "type": "favgraphs",
                    "name": "",
                    "x": "0",
                    "y": "0",
                    "width": "2",
                    "height": "3",
                    "fields": []
                }
            ]
        },
        {
            "dashboardid": "2",
            "name": "My dashboard",
            "userid": "1",
            "private": "1",
            "users": [
                {
                    "userid": "4",
                    "permission": "3"
                }
            ],
            "userGroups": [
                {
                    "usrgrpid": "7",
                    "permission": "2"
                }
            ],
            "widgets": [
                {
                    "widgetid": "10",
                    "type": "problems",
                    "name": "",
                    "x": "0",
                    "y": "0",
                    "width": "6",
                    "height": "5",
                    "fields": [
                        {
                            "type": "2",
                            "name": "groupids",
                            "value": "4"
                        }
                    ]
                }
            ]
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-84a10cd6})

[comment]: # ({new-299ae089})
### 参见

-   [仪表板小组件](object#dashboard_widget)
-   [仪表板小组件字段](object#dashboard_widget_field)
-   [仪表板用户](object#dashboard_user)
-   [仪表板用户组](object#dashboard_user_group)

[comment]: # ({/new-299ae089})

[comment]: # ({new-75a9042e})
### 来源

CDashboard::get() in
*frontends/php/include/classes/api/services/CDashboard.php*.

[comment]: # ({/new-75a9042e})
