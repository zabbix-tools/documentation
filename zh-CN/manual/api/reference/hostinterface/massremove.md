[comment]: # translation:outdated

[comment]: # ({new-9230837a})
# 批量删除

[comment]: # ({/new-9230837a})

[comment]: # ({new-4559aff6})
### 描述

`object hostinterface.massremove(object parameters)`

此方法允许删除给定主机的主机接口.

[comment]: # ({/new-4559aff6})

[comment]: # ({new-8970fdbb})
### 参数

`(对象)` 包含要更新的主机的ID和要删除的接口的参数.

|参数              类|描述|<|
|-----------------------|------|-|
|**hostids**<br>(必选)|对象/数组   要更新|主机ID.|
|**interfaces**<br>(必选)|对象/数组   从给定<br>|主机中删除主机接口.<br>主机接口对象必须已定义ip,dns和port属性|

[comment]: # ({/new-8970fdbb})

[comment]: # ({new-61a8b976})
### 返回值

`(对象)` 在`interfaceids`属性中返回已删除主机接口ID的对象.

[comment]: # ({/new-61a8b976})

[comment]: # ({new-b41637d2})
### 示例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-cceffaf2})
#### 删除接口

从给定的两台主机中删除"127.0.0.1" SNMP接口.

请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "hostinterface.massremove",
    "params": {
        "hostids": [
            "30050",
            "30052"
        ],
        "interfaces": {
            "dns": "",
            "ip": "127.0.0.1",
            "port": "161"
        }
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "interfaceids": [
            "30069",
            "30070"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-cceffaf2})

[comment]: # ({new-a3245281})
### 参考

-   [hostinterface.delete](delete)
-   [host.massremove](/manual/api/reference/host/massremove)

```{=html}
<!-- -->
```
    * 

[comment]: # ({/new-a3245281})

[comment]: # ({new-6853cf7a})
### 来源

CHostInterface::massRemove() in
*frontends/php/include/classes/api/services/CHostInterface.php*.

[comment]: # ({/new-6853cf7a})
