[comment]: # translation:outdated

[comment]: # ({new-59b410c0})
# 更新

[comment]: # ({/new-59b410c0})

[comment]: # ({new-253ed57f})
### 描述

`object hostinterface.update(object/array hostInterfaces)`

此方法允许更新已存在的主机接口

[comment]: # ({/new-253ed57f})

[comment]: # ({new-a99ac0ad})
### 参数

`(对象/数组)` 要更新的[主机接口属性](object#host_interface).

必须为每个主机接口定义`interfaceid`属性，所有其他属性都是可选的。
只有给定的属性将被更新，所有其他属性将保持不变.

[comment]: # ({/new-a99ac0ad})

[comment]: # ({new-d3be3202})
### 返回值

`(对象)` 在`interfaceids`属性中返回已更新主机接口ID的对象.

[comment]: # ({/new-d3be3202})

[comment]: # ({new-b41637d2})
### 示例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-eb8b35e8})
#### 更改主机接口端口

更改主机接口的端口.

请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "hostinterface.update",
    "params": {
        "interfaceid": "30048",
        "port": "30050"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "interfaceids": [
            "30048"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-eb8b35e8})

[comment]: # ({new-97e4c8d0})
### 来源

CHostInterface::update() in
*frontends/php/include/classes/api/services/CHostInterface.php*.

[comment]: # ({/new-97e4c8d0})
