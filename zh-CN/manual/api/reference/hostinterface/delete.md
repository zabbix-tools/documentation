[comment]: # translation:outdated

[comment]: # ({new-fc20d2a9})
# 删除

[comment]: # ({/new-fc20d2a9})

[comment]: # ({new-6a90d1d2})
### 描述

`object hostinterface.delete(array hostInterfaceIds)`

此方法允许删除主机接口

[comment]: # ({/new-6a90d1d2})

[comment]: # ({new-617ba57c})
### 参数

`(数组)` 要删除主机接口的ID.

[comment]: # ({/new-617ba57c})

[comment]: # ({new-61a8b976})
### 返回值

`(对象)` 在`interfaceids`属性中返回已删除主机接口ID的对象.

[comment]: # ({/new-61a8b976})

[comment]: # ({new-b41637d2})
### 示例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-91934c15})
#### 删除主机接口

删除ID为30062的主机接口.

请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "hostinterface.delete",
    "params": [
        "30062"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "interfaceids": [
            "30062"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-91934c15})

[comment]: # ({new-a3d1e725})
### 参考

-   [hostinterface.massremove](massremove)
-   [host.massremove](/manual/api/reference/host/massremove)

[comment]: # ({/new-a3d1e725})

[comment]: # ({new-cf54c99c})
### 来源

CHostInterface::delete() in
*frontends/php/include/classes/api/services/CHostInterface.php*.

[comment]: # ({/new-cf54c99c})
