[comment]: # translation:outdated

[comment]: # ({new-0bfa3a60})
# 创建

[comment]: # ({/new-0bfa3a60})

[comment]: # ({new-e87d74af})
### 描述

`object hostinterface.create(object/array hostInterfaces)`

该方法允许创建新的主机接口.

[comment]: # ({/new-e87d74af})

[comment]: # ({new-e81b8e01})
### 参数

`(对象/数组)`
创建主机接口,该方法接受[标准主机接口属性](object#host_interface)的主机接口.

[comment]: # ({/new-e81b8e01})

[comment]: # ({new-4b59ae6b})
### 返回值

`(对象)`
在`interfaceids`属性中返回已创建主机接口ID的对象.返回的ID顺序与传入的主机接口顺序保持一致.

[comment]: # ({/new-4b59ae6b})

[comment]: # ({new-b41637d2})
### 示例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-150295f4})
#### 创建主机接口

给ID为30052主机创建辅助IP代理接口

请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "hostinterface.create",
    "params": {
        "hostid": "30052",
        "dns": "",
        "ip": "127.0.0.1",
        "main": 0,
        "port": "10050",
        "type": 1,
        "useip": 1
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "interfaceids": [
            "30062"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-150295f4})

[comment]: # ({new-c22176c4})
### 参考

-   [hostinterface.massadd](massadd)
-   [host.massadd](/manual/api/reference/host/massadd)

[comment]: # ({/new-c22176c4})

[comment]: # ({new-9afed0b0})
### 来源

CHostInterface::create() in
*frontends/php/include/classes/api/services/CHostInterface.php*.

[comment]: # ({/new-9afed0b0})


[comment]: # ({new-5b41f219})
### Source

CHostInterface::create() in
*ui/include/classes/api/services/CHostInterface.php*.

[comment]: # ({/new-5b41f219})
