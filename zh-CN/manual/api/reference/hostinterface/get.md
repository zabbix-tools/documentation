[comment]: # translation:outdated

[comment]: # ({new-25793162})
# 获取

[comment]: # ({/new-25793162})

[comment]: # ({new-29074792})
### 描述

`integer/array hostinterface.get(object parameters)`

此方法允许获取给定参数的主机接口记录

[comment]: # ({/new-29074792})

[comment]: # ({new-a40e6018})
### 参数

`(对象)` 定义期望输出的参数.

该方法支持以下参数.

|参数                     类|描述|<|
|------------------------------|------|-|
|hostids|字符串/数组   返回给定|机使用的主机接口.|
|interfaceids|字符串/数组   返回给定|D的主机接口.|
|itemids|字符串/数组   返回给定|目的主机接口.|
|triggerids|字符串/数组   返回给定|发器中项目使用的主机接口.|
|selectItems|查询          返|`items`属性中使用接口的监控项.<br><br>支持 `count`.|
|selectHosts|查询          返|`hosts`属性中使用接口作为数组的主机.|
|limitSelects|整数          限|子选择返回的记录数.<br><br>适用于以下子选项:<br>`selectItems`.|
|sortfield|字符串/数组   按照给定|属性对结果进行排序.<br><br>可能的值: `interfaceid`, `dns`, `ip`.|
|countOutput|布尔值        这些|数对于所有get方法都是通用的，详情可参考 [reference commentary](/manual/api/reference_commentary#common_get_method_parameters).|
|editable|布尔值        ::|<|
|excludeSearch|布尔值        ::|<|
|filter|对象          :|:|
|limit|整数          :|:|
|nodeids|字符串/数组   :::|<|
|output|查询          :|:|
|preservekeys|布尔值        ::|<|
|search|对象          :|:|
|searchByAny|布尔值        ::|<|
|searchWildcardsEnabled|布尔值        ::|<|
|sortorder|字符串/数组   :::|<|
|startSearch|布尔值        ::|<|

[comment]: # ({/new-a40e6018})

[comment]: # ({new-7223bab1})
### 返回值

`(整数/数组)` 返回:

-   一组对象;
-   如果设置了`countOutput`参数,则返回获取到的对象数量.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### 示例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-cbd2e952})
#### 获取主机接口

获取ID为''30057'的主机使用的接口的所有数据.

请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "hostinterface.get",
    "params": {
        "output": "extend",
        "hostids": "30057"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "interfaceid": "30050",
            "hostid": "30057",
            "main": "1",
            "type": "1",
            "useip": "1",
            "ip": "127.0.0.1",
            "dns": "",
            "port": "10050",
            "bulk": "1"
        },
        {
            "interfaceid": "30067",
            "hostid": "30057",
            "main": "0",
            "type": "1",
            "useip": "0",
            "ip": "",
            "dns": "localhost",
            "port": "10050",
            "bulk": "1"
        },
        {
            "interfaceid": "30068",
            "hostid": "30057",
            "main": "1",
            "type": "2",
            "useip": "1",
            "ip": "127.0.0.1",
            "dns": "",
            "port": "161",
            "bulk": "1"
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-cbd2e952})

[comment]: # ({new-1c57700e})
### 参考

-   [Host](/manual/api/reference/host/object#host)
-   [Item](/manual/api/reference/item/object#item)

[comment]: # ({/new-1c57700e})

[comment]: # ({new-d0cee877})
### 来源

CHostInterface::get() in
*frontends/php/include/classes/api/services/CHostInterface.php*.

[comment]: # ({/new-d0cee877})
