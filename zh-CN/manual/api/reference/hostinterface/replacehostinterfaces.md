[comment]: # translation:outdated

[comment]: # ({new-2898fc4b})
# 替换

[comment]: # ({/new-2898fc4b})

[comment]: # ({new-f14e7778})
### 描述

`object hostinterface.replacehostinterfaces(object parameters)`

此方法允许给指定主机替换所有主机接口.

[comment]: # ({/new-f14e7778})

[comment]: # ({new-f5cf0b40})
### 参数

`(对象)` 包含要更新的主机ID和新主机接口的参数.

|参数              类|描述|<|
|-----------------------|------|-|
|**hostid**<br>(必选)|字符串      要更|主机的ID.|
|**interfaces**<br>(必须)|对象/数组   替换当|主机接口的主机接口.|

[comment]: # ({/new-f5cf0b40})

[comment]: # ({new-88283807})
### 返回值

`(对象)` 在`interfaceids`属性中返回已创建主机接口ID的对象.

[comment]: # ({/new-88283807})

[comment]: # ({new-b41637d2})
### 示例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-98e67db1})
#### 更换主机接口

用单个代理接口替换所有主机接口.

请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "hostinterface.replacehostinterfaces",
    "params": {
        "hostid": "30052",
        "interfaces": {
            "dns": "",
            "ip": "127.0.0.1",
            "main": 1,
            "port": "10050",
            "type": 1,
            "useip": 1
        }
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "interfaceids": [
            "30081"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-98e67db1})

[comment]: # ({new-272c42ef})
### 参考

-   [host.update](/manual/api/reference/host/update)
-   [host.massupdate](/manual/api/reference/host/massupdate)

[comment]: # ({/new-272c42ef})

[comment]: # ({new-9d1e0b55})
### 来源

CHostInterface::replaceHostInterfaces() in
*frontends/php/include/classes/api/services/CHostInterface.php*.

[comment]: # ({/new-9d1e0b55})
