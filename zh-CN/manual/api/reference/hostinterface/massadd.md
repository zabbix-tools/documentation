[comment]: # translation:outdated

[comment]: # ({new-fff19f5f})
# 批量添加

[comment]: # ({/new-fff19f5f})

[comment]: # ({new-30999da7})
### 描述

`object hostinterface.massadd(object parameters)`

该方法允许同时向多个主机添加主机接口.

[comment]: # ({/new-30999da7})

[comment]: # ({new-35c0864e})
### 参数

`(object)` 包含要在给定主机上创建的主机接口的参数.

该方法接受以下参数:

|参数              类|描述|<|
|-----------------------|------|-|
|**hosts**<br>(必选)|对象/数组   要更新<br>|主机.<br>主机必须已定义`hostid`属性.|
|**interfaces**<br>(必选)|对象/数组   在给定|主机上创建主机接口.|

[comment]: # ({/new-35c0864e})

[comment]: # ({new-88283807})
### 返回值

`(object)` 在`iterfaceids`属性中返回包含已创建主机接口ID的对象.

[comment]: # ({/new-88283807})

[comment]: # ({new-b41637d2})
### 示例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-ad87458a})
#### 创建接口

给两个主机创建接口.

请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "hostinterface.massadd",
    "params": {
        "hosts": [
            {
                "hostid": "30050"
            },
            {
                "hostid": "30052"
            }
        ],
        "interfaces": {
            "dns": "",
            "ip": "127.0.0.1",
            "main": 0,
            "port": "10050",
            "type": 1,
            "useip": 1
        }
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "interfaceids": [
            "30069",
            "30070"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-ad87458a})

[comment]: # ({new-a2a46209})
### 参考

-   [hostinterface.create](create)
-   [host.massadd](/manual/api/reference/host/massadd)
-   [Host](/manual/api/reference/host/object#host)

[comment]: # ({/new-a2a46209})

[comment]: # ({new-9b65afcc})
### 来源

CHostInterface::massAdd() in
*frontends/php/include/classes/api/services/CHostInterface.php*.

[comment]: # ({/new-9b65afcc})
