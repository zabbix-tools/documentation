[comment]: # translation:outdated

[comment]: # ({new-7ed888d2})
# > 主机接口对象

以下对象与`hostinterface`API直接相关.

[comment]: # ({/new-7ed888d2})

[comment]: # ({new-ce77a3b0})
### 主机接口

主机接口对象具有以下属性.

::: noteimportant
请注意，IP和DNS都是必需的。
如果您不想使用DNS，请将其设置为空字符串.
:::

|属性          类|描述|<|
|-------------------|------|-|
|interfaceid|字符串   *(|读)* 接口ID.|
|**dns**<br>(必选)|字符     接<br>|使用的DNS名称.<br>如果通过IP连接，可以设置为空.|
|**hostid**<br>(必选)|字符     接|归属的主机ID.|
|**ip**<br>(必选)|字符     接<br>|使用的IP地址.<br>如果通过DNS域名连接，可以设置为空.|
|**main**<br>(必选)|整数     该<br>|口是否在主机上用作默认接口. 主机上只能有一种类型的接口作为默认设置.<br>可能的值:<br>0 - 不是默认;<br>1 - 默认.|
|**port**<br>(必选)|字符     接|使用的端口号,可以包含用户宏.|
|**type**<br>(必选)|整数     接<br>|类型.<br>可能的值:<br>1 - agent;<br>2 - SNMP;<br>3 - IPMI;<br>4 - JMX.<br>|
|**useip**<br>(必选)|整数     是<br>|应通过IP进行连接.<br>可能的值:<br>0 - 使用主机DNS名称连接;<br>1 - 使用该主机接口的主机IP地址进行连接.|
|bulk|整数     是|使用批量SNMP请求.<br><br>可能的值:<br>0 - 不要使用批量请求;<br>1 - *(默认)* 使用批量请求.|

[comment]: # ({/new-ce77a3b0})


[comment]: # ({new-3bb71fcc})
### Details tag

The details object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**version**<br>(required)|integer|SNMP interface version.<br><br>Possible values are:<br>1 - SNMPv1;<br>2 - SNMPv2c;<br>3 - SNMPv3|
|bulk|integer|Whether to use bulk SNMP requests.<br><br>Possible values are:<br>0 - don't use bulk requests;<br>1 - (default) - use bulk requests.|
|community|string|SNMP community (required). Used only by SNMPv1 and SNMPv2 interfaces.|
|securityname|string|SNMPv3 security name. Used only by SNMPv3 interfaces.|
|securitylevel|integer|SNMPv3 security level. Used only by SNMPv3 interfaces.<br><br>Possible values are:<br>0 - (default) - noAuthNoPriv;<br>1 - authNoPriv;<br>2 - authPriv.|
|authpassphrase|string|SNMPv3 authentication passphrase. Used only by SNMPv3 interfaces.|
|privpassphrase|string|SNMPv3 privacy passphrase. Used only by SNMPv3 interfaces.|
|authprotocol|integer|SNMPv3 authentication protocol. Used only by SNMPv3 interfaces.<br><br>Possible values are:<br>0 - (default) - MD5;<br>1 - SHA1;<br>2 - SHA224;<br>3 - SHA256;<br>4 - SHA384;<br>5 - SHA512.|
|privprotocol|integer|SNMPv3 privacy protocol. Used only by SNMPv3 interfaces.<br><br>Possible values are:<br>0 - (default) - DES;<br>1 - AES128;<br>2 - AES192;<br>3 - AES256;<br>4 - AES192C;<br>5 - AES256C.|
|contextname|string|SNMPv3 context name. Used only by SNMPv3 interfaces.|

[comment]: # ({/new-3bb71fcc})
