[comment]: # translation:outdated

[comment]: # ({new-4fff4611})
# 问题

This class is designed to work with problems. 这个类设计用于描述问题。

Object references:\

-   [Problem](/manual/api/reference/problem/object#problem)

Available methods:\

-   [problem.get](/manual/api/reference/problem/get) - retrieving
    problems

[comment]: # ({/new-4fff4611})
