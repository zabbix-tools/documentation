[comment]: # translation:outdated

[comment]: # ({new-bce5854d})
# 删除

[comment]: # ({/new-bce5854d})

[comment]: # ({new-adb8b388})
### Description 说明

`object trigger.delete(array triggerIds)`

This method allows to delete triggers. 此方法允许删除触发器。

[comment]: # ({/new-adb8b388})

[comment]: # ({new-fc913e85})
### Parameters 参数

`(array)` IDs of the triggers to delete. `(array)`需要删除的触发器ID。

[comment]: # ({/new-fc913e85})

[comment]: # ({new-a2fb984a})
### Return values 返回值

`(object)` Returns an object containing the IDs of the deleted triggers
under the `triggerids` property.
`(object)`返回一个对象，该对象包含在`triggerids`属性中已删除触发器的ID。

[comment]: # ({/new-a2fb984a})

[comment]: # ({new-b41637d2})
### Examples 范例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-752e2ebb})
#### Delete multiple triggers 删除多个触发器

Delete two triggers. 删除两个触发器。

Request 请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "trigger.delete",
    "params": [
        "12002",
        "12003"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Response 响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "triggerids": [
            "12002",
            "12003"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-752e2ebb})

[comment]: # ({new-087c0548})
### Source 源码

CTrigger::delete() in
*frontends/php/include/classes/api/services/CTrigger.php*.
CTrigger::delete()方法可在*frontends/php/include/classes/api/services/CTrigger.php*中参考。

[comment]: # ({/new-087c0548})
