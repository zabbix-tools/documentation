[comment]: # translation:outdated

[comment]: # ({new-fc5b81d2})
# 获取

[comment]: # ({/new-fc5b81d2})

[comment]: # ({new-e77e3d1e})
### Description 说明

`integer/array trigger.get(object parameters)`

The method allows to retrieve triggers according to the given
parameters. 此方法允许根据指定的参数检索触发器。

[comment]: # ({/new-e77e3d1e})

[comment]: # ({new-44938a82})
### Parameters 参数

`(object)` Parameters defining the desired output.
`(object)`定义需要输出的参数。 The method supports the following
parameters. 该方法支持以下参数。

|Parameter 参数                T|pe 类型                  Des|ription 说明|
|---------------------------------|------------------------------|--------------|
|triggerids|string/array 字符串/数组   Retu|n only triggers with the given IDs. 仅返回指定ID的触发器。|
|groupids|string/array 字符串/数组   Retu|n only triggers that belong to hosts from the given host groups. 仅返回来自指定主机组中所属主机的触发器。|
|templateids|string/array 字符串/数组   Retu|n only triggers that belong to the given templates. 仅返回指定模板所属的触发器。|
|hostids|string/array 字符串/数组   Retu|n only triggers that belong to the given hosts. 仅返回指定主机所属的触发器。|
|itemids|string/array 字符串/数组   Retu|n only triggers that contain the given items. 仅返回包含指定监控项的触发器。|
|applicationids|string/array 字符串/数组   Retu|n only triggers that contain items from the given applications. 仅返回来自指定应用集中包含监控项的触发器。|
|functions|string/array 字符串/数组   Retu|n only triggers that use the given functions. 仅返回使用指定函数的触发器。<br><br>Refer to the [supported trigger functions](/manual/appendix/triggers/functions) page for a list of supported functions. 有关支持的功能列表，请参阅[supported trigger functions](/zh/manual/appendix/triggers/functions)页面。|
|group|string 字符串              Re|urn only triggers that belong to hosts from the host group with the given name. 仅返回来自指定名称的主机组中所属主机的触发器。|
|host|string 字符串              Re|urn only triggers that belong to host with the given name. 仅返回指定名称的所属主机的触发器。|
|inherited|boolean 布尔值             If|set to `true` return only triggers inherited from a template. 仅返回从模板继承的触发器，如果设置为`true`。|
|templated|boolean 布尔值             If|set to `true` return only triggers that belong to templates. 仅返回所属模板的触发器，如果设置为`true`。|
|monitored|flag 标记                  R|turn only enabled triggers that belong to monitored hosts and contain only enabled items. 仅返回所属被监控主机的已启用触发器，并包含已启用的监控项。|
|active|flag 标记                  R|turn only enabled triggers that belong to monitored hosts. 仅返回所属被监控主机的已启用触发器。|
|maintenance|boolean 布尔值             If|set to `true` return only enabled triggers that belong to hosts in maintenance. 仅返回在维护中所属主机的已启用触发器，如果设置为`true`。|
|withUnacknowledgedEvents|flag 标记                  R|turn only triggers that have unacknowledged events. 仅返回事件未确认的触发器。|
|withAcknowledgedEvents|flag 标记                  R|turn only triggers with all events acknowledged. 仅返回所有事件已确认的触发器。|
|withLastEventUnacknowledged|flag 标记                  R|turn only triggers with the last event unacknowledged. 仅返回最后一个未确认事件的触发器。|
|skipDependent|flag 标记                  S|ip triggers in a problem state that are dependent on other triggers. 依赖其他触发器的触发器处在故障状态时就跳过。Note that the other triggers are ignored if disabled, have disabled items or disabled item hosts. 请注意，如果依赖触发器被禁用，或监控项被禁用，或监控项主机被禁用，那么触发将被忽略。|
|lastChangeSince|timestamp 时间戳           Re|urn only triggers that have changed their state after the given time. 仅返回指定时间之后变更状态的触发器。|
|lastChangeTill|timestamp 时间戳           Re|urn only triggers that have changed their state before the given time. 仅返回指定时间之前变更状态的触发器。|
|only\_true|flag 标记                  R|turn only triggers that have recently been in a problem state. 仅返回最近处于故障状态的触发器。|
|min\_severity|integer 整数型             Re|urn only triggers with severity greater or equal than the given severity. 仅返回严重级别大于或等于指定严重级别的触发器。|
|expandComment|flag 标记                  E|pand macros in the trigger description. 展开触发器描述中的宏。|
|expandDescription|flag 标记                  E|pand macros in the name of the trigger. 展开触发器名称中的宏。|
|expandExpression|flag 标记                  E|pand functions and macros in the trigger expression. 展开在触发器表达式中的函数和宏。|
|selectGroups|query 查询                 R|turn the host groups that the trigger belongs to in the `groups` property. 返回在`groups`属性中触发器所属的主机组。|
|selectHosts|query 查询                 R|turn the hosts that the trigger belongs to in the `hosts` property. 返回在`hosts`属性中触发器所属的主机。|
|selectItems|query 查询                 R|turn items contained by the trigger in the `items` property. 返回在`items`属性中触发器所包含的监控项。|
|selectFunctions|query 查询                 R|turn functions used in the trigger in the `functions` property. 返回在`functions`属性中在触发器中使用的函数。<br><br>The function objects represents the functions used in the trigger expression and has the following properties: 函数对象代表使用在触发器表达式中的函数，并具有以下属性：<br>`functionid` - *(string 字符串)* ID of the function; 函数的ID；<br>`itemid` - *(string 字符串)* ID of the item used in the function; 使用在函数中的监控项ID。<br>`function` - *(string 字符串)* name of the function; 函数的名称；<br>`parameter` - *(string 字符串)* parameter passed to the function. 传递给函数的参数。|
|selectDependencies|query 查询                 R|turn triggers that the trigger depends on in the `dependencies` property. 返回在`dependencies`属性中依赖触发的触发器。|
|selectDiscoveryRule|query 查询                 R|turn the low-level discovery rule that created the trigger. 返回创建了触发器的低级别发现规则。|
|selectLastEvent|query 查询                 R|turn the last significant trigger event in the `lastEvent` property. 返回在`lastEvent`属性中最后一个重要触发事件。|
|selectTags|query 查询                 R|turn the trigger tags in `tags` property. 返回在`tags`属性中触发器标签。|
|selectTriggerDiscovery|query 查询                 R|turn the trigger discovery object in the `triggerDiscovery` property. 返回在`triggerDiscovery`属性中触发器发现对象。The trigger discovery objects links the trigger to a trigger prototype from which it was created. 触发器发现对象将触发器链接到创建它的触发器原型上。<br><br>It has the following properties: 触发器发现对象具有以下属性：<br>`parent_triggerid` - `(string 字符串)` ID of the trigger prototype from which the trigger has been created. 创建触发器的触发器原型ID。|
|filter|object 对象                R|turn only those results that exactly match the given filter. 仅返回与指定筛选完全匹配的结果。<br><br>Accepts an array, where the keys are property names, and the values are either a single value or an array of values to match against. 接受一个数组，其中键为属性名称，值为单个值或要匹配值的数组。<br><br>Supports additional filters: 支持额外的筛选：<br>`host` - technical name of the host that the trigger belongs to; 触发器所属主机的正式名称。<br>`hostid` - ID of the host that the trigger belongs to. 触发器所属主机的ID。|
|limitSelects|integer 整数型             Li|its the number of records returned by subselects. 限制子查询返回的记录数量。<br><br>Applies to the following subselects: 适用于以下子查询：<br>`selectHosts` - results will be sorted by `host`. 以`host`分类结果。|
|sortfield|string/array 字符串/数组   [Sor|](/manual/api/reference_commentary#common_get_method_parameters) the result by the given properties. 由指定属性分类结果。<br><br>Possible values are: `triggerid`, `description`, `status`, `priority`, `lastchange` and `hostname`. 许可值为：`triggerid`, `description`, `status`, `priority`, `lastchange`和`hostname`。|
|countOutput|boolean 布尔值             Th|se parameters being common for all `get` methods are described in detail in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters) page. 这些参数十分普遍，适用于所有`get`方法，详情可参考[reference commentary](/zh/manual/api/reference_commentary#common_get_method_parameters)。|
|editable|boolean 布尔值             ::|<|
|excludeSearch|boolean 布尔值             ::|<|
|limit|integer 整数型             ::|<|
|output|query 查询                 :|:|
|preservekeys|boolean 布尔值             ::|<|
|search|object 对象                :|:|
|searchByAny|boolean 布尔值             ::|<|
|searchWildcardsEnabled|boolean 布尔值             ::|<|
|sortorder|string/array 字符串/数组   :::|<|
|startSearch|boolean 布尔值             ::|<|

[comment]: # ({/new-44938a82})

[comment]: # ({new-7223bab1})
### Return values 返回值

`(integer/array)` Returns either: 返回两者其中任一：

-   an array of objects; 一组对象；
-   the count of retrieved objects, if the `countOutput` parameter has
    been used. 如果已经使用了`countOutput`参数，则检索对象的计数。

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Examples 范例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-3d27e76d})
#### Retrieving data by trigger ID 根据触发器ID检索数据

Retrieve all data and the functions used in trigger "14062".
检索触发器"14062"中使用的所有数据和功能。

Request 请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "trigger.get",
    "params": {
        "triggerids": "14062",
        "output": "extend",
        "selectFunctions": "extend"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response 响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "functions": [
                {
                    "functionid": "13513",
                    "itemid": "24350",
                    "function": "diff",
                    "parameter": "0"
                }
            ],
            "triggerid": "14062",
            "expression": "{13513}>0",
            "description": "/etc/passwd has been changed on {HOST.NAME}",
            "url": "",
            "status": "0",
            "value": "0",
            "priority": "2",
            "lastchange": "0",
            "comments": "",
            "error": "",
            "templateid": "10016",
            "type": "0",
            "state": "0",
            "flags": "0",
            "recovery_mode": "0",
            "recovery_expression": "",
            "correlation_mode": "0",
            "correlation_tag": "",
            "manual_close": "0"
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-3d27e76d})

[comment]: # ({new-8a31301f})
#### Retrieving triggers in problem state 检索在故障状态的触发器

Retrieve the ID, name and severity of all triggers in problem state and
sort them by severity in descending order.
检索在问题状态下的所有触发器的ID，名称和严重性，并按严重性级别按降序分类。

Request 请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "trigger.get",
    "params": {
        "output": [
            "triggerid",
            "description",
            "priority"
        ],
        "filter": {
            "value": 1
        },
        "sortfield": "priority",
        "sortorder": "DESC"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response 响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "triggerid": "13907",
            "description": "Zabbix self-monitoring processes < 100% busy",
            "priority": "4"
        },
        {
            "triggerid": "13824",
            "description": "Zabbix discoverer processes more than 75% busy",
            "priority": "3"
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-8a31301f})

[comment]: # ({new-8005126d})
#### Retrieving a specific trigger with tags 使用标签检索特定的触发器

Retrieve a specific trigger with tags. 使用标签检索特定的触发器。

Request 请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "trigger.get",
    "params": {
        "output": [
            "triggerid",
            "description"
        ],
        "selectTags": "extend",
        "triggerids": [
            "17578"
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response 响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "triggerid": "17370",
            "description": "Service status",
            "tags": [
                {
                    "tag": "service",
                    "value": "{{ITEM.VALUE}.regsub(\"Service (.*) has stopped\", \"\\1\")}"
                },
                {
                    "tag": "error",
                    "value": ""
                }
            ]
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-8005126d})

[comment]: # ({new-222d46a0})
### See also 参考

-   [Discovery
    rule](/zh/manual/api/reference/discoveryrule/object#discovery_rule)
-   [Item](/zh/manual/api/reference/item/object#item)
-   [Host](/zh/manual/api/reference/host/object#host)
-   [Host group](/zh/manual/api/reference/hostgroup/object#host_group)

[comment]: # ({/new-222d46a0})

[comment]: # ({new-07515392})
### Source 源码

CTrigger::get() in
*frontends/php/include/classes/api/services/CTrigger.php*.
CTrigger::get()方法可在*frontends/php/include/classes/api/services/CTrigger.php*中参考。

[comment]: # ({/new-07515392})
