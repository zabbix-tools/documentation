[comment]: # translation:outdated

[comment]: # ({new-56486046})
# 创建

[comment]: # ({/new-56486046})

[comment]: # ({new-8efa90c6})
### Description 说明

`object trigger.create(object/array triggers)`

This method allows to create new triggers. 此方法允许创建新的触发器。

[comment]: # ({/new-8efa90c6})

[comment]: # ({new-c2c3e90f})
### Parameters 参数

`(object/array)` Triggers to create. `(object/array)`需要创建的触发器。
Additionally to the [standard trigger properties](object#trigger) the
method accepts the following parameters. 除[standard trigger
properties](object#trigger)之外，该方法接受以下参数。

|Parameter 参数   T|pe 类型    Des|ription 说明|
|--------------------|----------------|--------------|
|dependencies|array 数组   T|iggers that the trigger is dependent on. 依赖触发的触发器。<br><br>The triggers must have the `triggerid` property defined. 触发器必须已定义`triggerid`属性。|
|tags|array 数组   T|igger tags. 触发器标签。|

::: noteimportant
The trigger expression has to be given in its
expanded form. 指定的触发器表达式必须为展开式。
:::

[comment]: # ({/new-c2c3e90f})

[comment]: # ({new-4938d22d})
### Return values 返回值

`(object)` Returns an object containing the IDs of the created triggers
under the `triggerids` property. The order of the returned IDs matches
the order of the passed triggers.
`(object)`返回一个对象，该对象包含在`triggerids`属性中已创建触发器的ID，返回ID的顺序与传递触发器的顺序相匹配。

[comment]: # ({/new-4938d22d})

[comment]: # ({new-b41637d2})
### Examples 范例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-f57689d3})
#### Creating a trigger 创建触发器

Create a trigger with a single trigger dependency.
创建具有单个触发依赖关系的触发器。

Request 请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "trigger.create",
    "params": [
        {
            "description": "Processor load is too high on {HOST.NAME}",
            "expression": "{Linux server:system.cpu.load[percpu,avg1].last()}>5",
            "dependencies": [
                {
                    "triggerid": "17367"
                }
            ]
        },
        {
            "description": "Service status",
            "expression": "{Linux server:log[/var/log/system,Service .* has stopped].strlen()}<>0",
            "dependencies": [
                {
                    "triggerid": "17368"
                }
            ],
            "tags": [
                {
                    "tag": "service",
                    "value": "{{ITEM.VALUE}.regsub(\"Service (.*) has stopped\", \"\\1\")}"
                },
                {
                    "tag": "error",
                    "value": ""
                }
            ]
        }
    ],
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response 响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "triggerids": [
            "17369",
            "17370"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-f57689d3})

[comment]: # ({new-7d49da78})
### Source 源码

CTrigger::create() in
*frontends/php/include/classes/api/services/CTrigger.php*.
CTrigger::create()方法可在*frontends/php/include/classes/api/services/CTrigger.php*中参考。

[comment]: # ({/new-7d49da78})
