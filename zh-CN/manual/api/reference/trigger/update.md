[comment]: # translation:outdated

[comment]: # ({new-a7557797})
# 更新

[comment]: # ({/new-a7557797})

[comment]: # ({new-476a7855})
### Description 说明

`object trigger.update(object/array triggers)`

This method allows to update existing triggers.
此方法用于更新目前的触发器。

[comment]: # ({/new-476a7855})

[comment]: # ({new-b918ffdf})
### Parameters 参数

`(object/array)` Trigger properties to be updated.
`(object/array)`需要更新的触发器属性。 The `triggerid` property must be
defined for each trigger, all other properties are optional. Only the
passed properties will be updated, all others will remain unchanged.
`triggerid`属性必须在每个应用集中已定义，其他所有属性为可选项。只有传递过去的属性会被更新，其他所有属性仍然保持不变。
Additionally to the [standard trigger properties](object#trigger) the
method accepts the following parameters. 除[standard trigger
properties](object#trigger)之外，该方法接受以下参数。

|Parameter 参数   T|pe 类型    Des|ription 说明|
|--------------------|----------------|--------------|
|dependencies|array 数组   T|iggers that the trigger is dependent on. 依赖触发的触发器。<br><br>The triggers must have the `triggerid` property defined. 触发器必须已定义`triggerid`属性。|
|tags|array 数组   T|igger tags. 触发器标签。|

::: noteimportant
The trigger expression has to be given in its
expanded form. 指定的触发器表达式必须为展开式。
:::

[comment]: # ({/new-b918ffdf})

[comment]: # ({new-18d0cc04})
### Return values 返回值

`(object)` Returns an object containing the IDs of the updated triggers
under the `triggerids` property.
`(object)`返回一个对象，该对象包含在`triggerids`属性中已更新触发器的ID。

[comment]: # ({/new-18d0cc04})

[comment]: # ({new-b41637d2})
### Examples 范例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-07c64481})
#### Enabling a trigger 启用触发器

Enable a trigger, that is, set its status to 0.
启用触发器，即将其状态设置为0。

Request 请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "trigger.update",
    "params": {
        "triggerid": "13938",
        "status": 0
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response 响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "triggerids": [
            "13938"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-07c64481})

[comment]: # ({new-b2bab7ce})
#### Replacing triggers tags 替换触发器标签

Replace tags for trigger. 为触发器替换标签。

Request 请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "trigger.update",
    "params": {
        "triggerid": "13938",
        "tags": [
            {
                "tag": "service",
                "value": "{{ITEM.VALUE}.regsub(\"Service (.*) has stopped\", \"\\1\")}"
            },
            {
                "tag": "error",
                "value": ""
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response 响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "triggerids": [
            "13938"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-b2bab7ce})

[comment]: # ({new-db463204})
#### Replacing dependencies

Replace dependencies for trigger.

Request:

```json
{
    "jsonrpc": "2.0",
    "method": "trigger.update",
    "params": {
        "triggerid": "22713",
        "dependencies": [
            {
                "triggerid": "22712"
            },
            {
                "triggerid": "22772"
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "triggerids": [
            "22713"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-db463204})


[comment]: # ({new-180a8172})
### Source 源码

CTrigger::update() in
*frontends/php/include/classes/api/services/CTrigger.php*.
CTrigger::update()方法可在*frontends/php/include/classes/api/services/CTrigger.php*中参考。

[comment]: # ({/new-180a8172})
