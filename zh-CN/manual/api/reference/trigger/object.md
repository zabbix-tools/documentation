[comment]: # translation:outdated

[comment]: # ({new-8bbba5df})
# > 对象

The following objects are directly related to the `trigger` API.
以下对象与`trigger`API直接相关。

[comment]: # ({/new-8bbba5df})

[comment]: # ({new-db828bca})
### Trigger 触发器

The trigger object has the following properties.
触发器对象具有以下属性。

|Property 属性          T|pe 类型          Des|ription 说明|
|--------------------------|----------------------|--------------|
|triggerid|string 字符串      *(|eadonly 只读)* ID of the trigger. 触发器的ID。|
|**description**<br>(required 必须)|string 字符串      Na|e of the trigger. 触发器的名称。|
|**expression**<br>(required 必须)|string 字符串      Re|uced trigger expression. 生成的触发表达式。|
|comments|string 字符串      Ad|itional description of the trigger. 触发器的附加说明。|
|error|string 字符串      *(|eadonly 只读)* Error text if there have been any problems when updating the state of the trigger. 错误概述，如果在更新触发器的状态时出现任何问题。|
|flags|integer 整数型     *(|eadonly 只读)* Origin of the trigger. 原始触发器。<br><br>Possible values are: 许可值为：<br>0 - *(default 默认)* a plain trigger; 普通触发器；<br>4 - a discovered trigger. 自动发现的触发器。|
|lastchange|timestamp 时间戳   *(|eadonly 只读)* Time when the trigger last changed its state. 触发器最后更改其状态的时间。|
|priority|integer 整数型     Se|erity of the trigger. 触发器的严重性级别。<br><br>Possible values are: 许可值为：<br>0 - *(default 默认)* not classified; 未分类；<br>1 - information; 信息；<br>2 - warning; 警告；<br>3 - average; 一般严重；<br>4 - high; 严重；<br>5 - disaster. 灾难。|
|state|integer 整数型     *(|eadonly 只读)* State of the trigger. 触发器的状态。<br><br>Possible values: 许可值：<br>0 - *(default 默认)* trigger state is up to date; 触发器状态是最新的；<br>1 - current trigger state is unknown. 当前的触发器状态是未知的。|
|status|integer 整数型     Wh|ther the trigger is enabled or disabled. 触发器是否处于启用状态或禁用状态。<br><br>Possible values are: 许可值为：<br>0 - *(default 默认)* enabled; 启用；<br>1 - disabled. 禁用。|
|templateid|string 字符串      *(|eadonly 只读)* ID of the parent template trigger. 父触发器模板ID。|
|type|integer 整数型     Wh|ther the trigger can generate multiple problem events. 触发器是否能够生成多个故障事件。<br><br>Possible values are: 许可值为：<br>0 - *(default 默认)* do not generate multiple events; 不生成多个事件。<br>1 - generate multiple events. 生成多个事件。|
|url|string 字符串      UR|associated with the trigger. 与触发器相关联的URL。|
|value|integer 整数型     *(|eadonly 只读)* Whether the trigger is in OK or problem state. 触发器是否处于正常或故障状态。<br><br>Possible values are: 许可值为：<br>0 - *(default 默认)* OK; 正常；<br>1 - problem. 故障。|
|recovery\_mode|integer 整数型     OK|event generation mode. 事件恢复生成模式。<br><br>Possible values are: 许可值为：<br>0 - *(default 默认)* Expression; 表达式；<br>1 - Recovery expression; 恢复表达式；<br>2 - None. 无。|
|recovery\_expression|string 字符串      Re|uced trigger recovery expression. 生成的触发恢复表达式。|
|correlation\_mode|integer 整数型     OK|event closes. 事件恢复关闭。<br><br>Possible values are: 许可值为：<br>0 - *(default 默认)* All problems; 所有故障；<br>1 - All problems if tag values match. 与标签值匹配的所有故障。|
|correlation\_tag|string 字符串      Ta|for matching. 用于匹配的标签。|
|manual\_close|integer 整数型     Al|ow manual close. 允许手动关闭。<br><br>Possible values are: 许可值为：<br>0 - *(default 默认)* No; 不允许；<br>1 - Yes. 允许。|

[comment]: # ({/new-db828bca})

[comment]: # ({new-0188f389})
### Trigger tag

The trigger tag object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**tag**<br>(required)|string|Trigger tag name.|
|value|string|Trigger tag value.|

[comment]: # ({/new-0188f389})

