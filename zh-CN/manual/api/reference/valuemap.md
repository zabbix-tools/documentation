[comment]: # translation:outdated

[comment]: # ({new-09ff1eef})
# Value map \[值映射\]

此类用于值映射的使用

对象引用：\

-   [Value map](/manual/api/reference/valuemap/object#value_map)

可用的方法：\

-   [valuemap.create](/manual/api/reference/valuemap/create) -
    创建新的value maps
-   [valuemap.delete](/manual/api/reference/valuemap/delete) - 删除value
    maps
-   [valuemap.get](/manual/api/reference/valuemap/get) - 检索value maps
-   [valuemap.update](/manual/api/reference/valuemap/update) - 更新
    value maps

```{=html}
<!-- -->
```
    * 

## Value map

This class is designed to work with value maps.

Object references:\

-   [Value map](/manual/api/reference/valuemap/object#value_map)

Available methods:\

-   [valuemap.create](/manual/api/reference/valuemap/create) - creating
    new value maps
-   [valuemap.delete](/manual/api/reference/valuemap/delete) - deleting
    value maps
-   [valuemap.get](/manual/api/reference/valuemap/get) - retrieving
    value maps
-   [valuemap.update](/manual/api/reference/valuemap/update) - updating
    value maps

[comment]: # ({/new-09ff1eef})
