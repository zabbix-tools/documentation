[comment]: # translation:outdated

[comment]: # ({new-f5d7e62f})
# 创建

[comment]: # ({/new-f5d7e62f})

[comment]: # ({new-c44d5a50})
### Description 说明

`object service.create(object/array services)`

This method allows to create new services. 此方法允许创建新的服务。

[comment]: # ({/new-c44d5a50})

[comment]: # ({new-69be5e01})
### Parameters 参数

`(object/array)` services to create. `(object/array)`创建服务。

Additionally to the [standard service properties](object#service), the
method accepts the following parameters. 除[standard service
properties](object#service)之外，该方法接受以下参数。

|Parameter 参数   T|pe 类型       Des|ription 说明|
|--------------------|-------------------|--------------|
|dependencies|array 数组      S|rvice dependencies. 服务依赖。<br><br>Each service dependency has the following parameters: 每个服务依赖项具有以下参数:<br>- `dependsOnServiceid` - *(string 字符串)* ID of an service the service depends on, that is, the child service. 被子服务依赖的服务ID。<br>- `soft` - *(integer 整数型)* type of service dependency; refer to the [service dependency object page](object#service_dependency) for more information on dependency types. 有关依赖关系类型的更多信息，请参阅[service dependency object page](object#service_dependency)|
|parentid|string 字符串   ID|of a hard-linked parent service. 硬链接的父服务的ID。|
|times|array 数组      S|rvice times to be created for the service. 为服务创建的服务时间。|

[comment]: # ({/new-69be5e01})

[comment]: # ({new-f5eb6cfd})
### Return values 返回值

`(object)` Returns an object containing the IDs of the created services
under the `serviceids` property. The order of the returned IDs matches
the order of the passed services.
`(object)`返回一个对象，该对象包含在`serviceids`属性中已创建服务的ID。返回ID的顺序与传递服务的顺序相匹配。

[comment]: # ({/new-f5eb6cfd})

[comment]: # ({new-b41637d2})
### Examples 范例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-fc6bed3c})
#### Creating an service 创建服务

Create an service that will be switched to problem state, if at least
one child has a problem. SLA calculation will be on and the minimum
acceptable SLA is 99.99%.
创建一个至少有一个子服务有问题，将被切换到问题状态的服务。SLA计算将打开并且SLA最低可接受99.99%。
Request 请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "service.create",
    "params": {
        "name": "Server 1",
        "algorithm": 1,
        "showsla": 1,
        "goodsla": 99.99,
        "sortorder": 1
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response 响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "serviceids": [
            "5"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-fc6bed3c})

[comment]: # ({new-fda6729e})
### Source 源码

CService::create() in
*frontends/php/include/classes/api/services/CService.php*.
CService::create()方法可在*frontends/php/include/classes/api/services/CService.php*中参考。

[comment]: # ({/new-fda6729e})
