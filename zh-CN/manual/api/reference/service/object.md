[comment]: # translation:outdated

[comment]: # ({new-b1a89f1b})
# > 对象

The following objects are directly related to the `service` API.
以下对象与`service`API直接相关。

[comment]: # ({/new-b1a89f1b})

[comment]: # ({new-5d7a3d70})
### Service 服务

The service object has the following properties. 服务对象具有以下属性。

|Property 属性     T|pe 类型        Des|ription 说明|
|---------------------|--------------------|--------------|
|serviceid|string 字符串    *(|eadonly 只读)* ID of the service. 服务的ID。|
|**algorithm**<br>(required 必须)|integer 整数型   Al<br>|orithm used to calculate the state of the service. 用于计算服务状态的算法。<br>Possible values: 许可值：<br>0 - do not calculate; 不计算；<br>1 - problem 问题, if at least one child has a problem; 如果至少有一个子项有问题。<br>2 - problem问题, if all children have problems. 如果所有子项都有问题。|
|**name**<br>(required 必须)|string 字符串    Na|e of the service. 服务的名称。|
|**showsla**<br>(required 必须)|integer 整数型   Wh<br>|ther SLA should be calculated. 是否应计算SLA。<br>Possible values: 许可值：<br>0 - do not calculate; 不计算；<br>1 - calculate. 计算。|
|**sortorder**<br>(required 必须)|integer 整数型   Po|ition of the service used for sorting. 用于排序服务的位置。|
|goodsla|float 浮点数     Mi|imum acceptable SLA value. If the SLA drops lower, the service is considered to be in problem state. 最低可接受的SLA值，如果SLA降低，则该服务被认为处于有问题状态。<br><br>Default: 99.9. 默认：99.9。|
|status|integer 整数型   *(|eadonly 只读)* Whether the service is in OK or problem state. 服务是否处于正常或问题状态。<br><br>If the service is in problem state, `status` is equal either to: 如果服务处于问题状态，`status`相当于以下情况之一：<br>- the priority of the linked trigger if it is set to 2, "Warning" or higher (priorities 0, "Not classified" and 1, "Information" are ignored); 优先级设置为2，"Warning"或更高级别所链接的触发器告警（忽略优先级0，"Not classified"和1，"Information"）<br>- the highest status of a child service in problem state. 其中一个最高级别状态的子服务处于问题中。<br><br>If the service is in OK state, `status` is equal to 0. 如果服务是正常状态，那么`status`等于0。|
|triggerid|string 字符串    Tr|gger associated with the service. Can only be set for services that don't have children. 与服务相关联的触发器只能设置在没有子项的服务上。<br><br>Default: 0 默认：0|

[comment]: # ({/new-5d7a3d70})

[comment]: # ({new-c2041847})
### Service time 服务时间

The service time object defines periods, when an service is scheduled to
be up or down. It has the following properties.
当一个服务按照计划上线或下线时，服务时间对象可定义周期。服务时间对象具有以下属性。

|Property 属性     T|pe 类型        Des|ription 说明|
|---------------------|--------------------|--------------|
|timeid|string 字符串    *(|eadonly 只读)* ID of the service time. 服务时间的ID。|
|**serviceid**<br>(required 必须)|string 字符串    ID<br>|of the service. 服务的ID。<br>Cannot be updated. 不可更新。|
|**ts\_from**<br>(required 必须)|integer 整数型   Ti<br>|e when the service time comes into effect. 服务时间生效的时间。<br>For onetime downtimes `ts_from` must be set as a Unix timestamp, for other types - as a specific time in a week, in seconds, for example, 90000 for Tue, 2:00 AM. 对于一次性停机时间，`ts_from`必须设置为Unix时间戳，对于其他类型的事件 —— 设置为一周中的特定时间，以秒为单位，例如，90000代表星期二，凌晨2:00。|
|**ts\_to**<br>(required 必须)|integer 整数型   Ti<br>|e when the service time ends. 服务时间关闭的时间。<br>For onetime uptimes `ts_to` must be set as a Unix timestamp, for other types - as a specific time in a week, in seconds, for example, 90000 for Tue, 2:00 AM. 对于一次性开机时间，`ts_to`必须设置为Unix时间戳，对于其他类型的事件 —— 设置为一周中的特定时间，以秒为单位，例如，90000代表星期二，凌晨2:00。|
|**type**<br>(required 必须)|integer 整数型   Se<br>|vice time type. 服务时间类型<br>Possible values: 许可值：<br>0 - planned uptime, repeated every week; 计划开机，每周重复；<br>1 - planned downtime, repeated every week; 计划停机，每周重复；<br>2 - one-time downtime. 一次性停机。|
|note|string 字符串    Ad|itional information about the service time. 有关服务时间的附加信息。|

[comment]: # ({/new-c2041847})

[comment]: # ({new-c301cfb2})
### Service dependency 服务依赖

The service dependency object represents a dependency between services.
It has the following properties.
服务依赖对象表示服务之间的依赖关系，它具有以下属性。

|Property 属性        T|pe 类型        Des|ription 说明|
|------------------------|--------------------|--------------|
|linkid|string 字符串    *(|eadonly 只读)* ID of the service dependency. 服务依赖的ID。|
|**servicedownid**<br>(required 必须)|string 字符串    ID|of the service, that a service depends on, that is, the child service. An service can have multiple children. 被子服务依赖的服务ID，一个服务可以有多个子服务。|
|**serviceupid**<br>(required 必须)|string 字符串    ID|of the service, that is dependent on a service, that is, the parent service. An service can have multiple parents forming a directed graph. 依赖于父服务的服务ID，一个服务可以有多个父服务，从而形成一张定向图表。|
|**soft**<br>(required 必须)|integer 整数型   Ty<br>|e of dependency between services. 服务之间的依赖关系类型。<br>Possible values: 许可值：<br>0 - hard dependency; 硬依赖；<br>1 - soft dependency. 软依赖。<br><br>An service can have only one hard-dependent parent. This attribute has no effect on status or SLA calculation and is only used to create a core service tree. Additional parents can be added as soft dependencies forming a graph. 一个服务只能有一个强依赖的父服务。该属性对状态或SLA计算没有影响，仅用于创建核心服务树。 新增的父服务可以作为形成图形的软依赖添加。<br><br>An service can not be deleted if it has hard-dependent children. 如果服务有硬依赖子服务，则无法删除该服务。|

[comment]: # ({/new-c301cfb2})




[comment]: # ({new-e6daa7b4})
### Service alarm

::: noteclassic
Service alarms cannot be directly created, updated or
deleted via the Zabbix API.
:::

The service alarm objects represents an service's state change. It has
the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|clock|timestamp|Time when the service state change has happened.|
|value|integer|Status of the service.<br><br>Refer to the [service status property](object#service) for a list of possible values.|

[comment]: # ({/new-e6daa7b4})

[comment]: # ({new-6b88d3dc})
### Problem tag

Problem tags allow linking services with problem events. The problem tag
object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**tag**<br>(required)|string|Problem tag name.|
|operator|integer|Mapping condition operator.<br><br>Possible values:<br>0 - *(default)* equals;<br>2 - like.|
|value|string|Problem tag value.|

[comment]: # ({/new-6b88d3dc})
