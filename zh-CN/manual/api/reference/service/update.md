[comment]: # translation:outdated

[comment]: # ({new-cd4a930c})
# 更新

[comment]: # ({/new-cd4a930c})

[comment]: # ({new-79afe9b7})
### Description 说明

`object service.update(object/array services)`

This method allows to update existing services. 此方法允许更新现有服务。

[comment]: # ({/new-79afe9b7})

[comment]: # ({new-5fb5ebb3})
### Parameters 参数

`(object/array)` service properties to be updated.
`(object/array)`需要更新的服务属性。 The `serviceid` property must be
defined for each service, all other properties are optional. Only the
passed properties will be updated, all others will remain unchanged.
必须为每个服务定义`serviceid`属性，所有其他属性为可选项。只有通过的属性会被更新，所有其他属性将保持不变。
Additionally to the [standard service properties](object#service), the
method accepts the following parameters. 除[standard service
properties](object#service)之外，该方法接受以下参数。

|Parameter 参数   T|pe 类型       Des|ription 说明|
|--------------------|-------------------|--------------|
|dependencies|array 数组      S|rvice dependencies to replace the current service dependencies. 用来替换当前内容的服务依赖关系。<br><br>Each service dependency has the following parameters: 每个服务依赖项具有以下参数：<br>- `dependsOnServiceid` - *(string 字符串)* ID of an service the service depends on, that is, the child service. 依赖服务的服务，即子服务的ID。<br>- `soft` - *(integer 整数型)* type of service dependency; 服务依赖类型；refer to the [service dependency object page](object#service_dependency) for more information on dependency types. 有关依赖关系类型的更多信息，请参阅[service dependency object page](object#service_dependency)。|
|parentid|string 字符串   ID|of a hard-linked parent service. 硬链接的父服务ID。|
|times|array 数组      S|rvice times to replace the current service times. 用来替换当前内容的服务时间。|

[comment]: # ({/new-5fb5ebb3})

[comment]: # ({new-9465ee4d})
### Return values 返回值

`(object)` Returns an object containing the IDs of the updated services
under the `serviceids` property.
`(object)`返回一个对象，该对象包含在`serviceids`属性中已更新服务的ID。

[comment]: # ({/new-9465ee4d})

[comment]: # ({new-b41637d2})
### Examples 范例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-b9918036})
#### Setting the parent of an service 设置父服务

Make service "3" the hard-linked parent of service "5".
使服务"3"硬链接于父服务"5"。

Request 请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "service.update",
    "params": {
        "serviceid": "5",
        "parentid": "3"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response 响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "serviceids": [
            "5"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-b9918036})

[comment]: # ({new-263b1baa})
### See also 参考

-   [service.adddependencies](adddependencies)
-   [service.addtimes](addtimes)
-   [service.deletedependencies](deletedependencies)
-   [service.deletetimes](deletetimes)

[comment]: # ({/new-263b1baa})

[comment]: # ({new-fd1b6894})
### Source 源码

CService::update() in
*frontends/php/include/classes/api/services/CService.php*.
CService::update()方法可在*frontends/php/include/classes/api/services/CService.php*中参考。

[comment]: # ({/new-fd1b6894})
