[comment]: # translation:outdated

[comment]: # ({new-2587902b})
# 获取

[comment]: # ({/new-2587902b})

[comment]: # ({new-bfd5de7f})
### Description 说明

`integer/array service.get(object parameters)`

The method allows to retrieve services according to the given
parameters. 此方法允许根据给定的参数检索服务。

[comment]: # ({/new-bfd5de7f})

[comment]: # ({new-e7637a1d})
### Parameters 参数

`(object)` Parameters defining the desired output.
`(object)`定义所需输出的参数。

The method supports the following parameters. 该方法支持以下参数。

|Parameter 参数             T|pe 类型                  Des|ription 说明|
|------------------------------|------------------------------|--------------|
|serviceids|string/array 字符串/数组   Retu|n only services with the given IDs. 仅返回拥有指定ID的服务。|
|parentids|string/array 字符串/数组   Retu|n only services with the given hard-dependent parent services. 仅返回拥有指定硬依赖父服务的服务。|
|childids|string/array 字符串/数组   Retu|n only services that are hard-dependent on the given child services. 仅返回在指定子服务上有硬依赖的服务。|
|selectParent|query 查询                 R|turn the hard-dependent parent service in the `parent` property. 返回`parent`属性中的硬依赖父服务。|
|selectDependencies|query 查询                 R|turn child service dependencies in the `dependencies` property. 返回在`dependencies`属性中有依赖的子服务。|
|selectParentDependencies|query 查询                 R|turn parent service dependencies in the `parentDependencies` property. 返回在`parentDependencies`属性中有依赖的父服务。|
|selectTimes|query 查询                 R|turn service times in the `times` property. 返回在`times`属性中的服务时间。|
|selectAlarms|query 查询                 R|turn service alarms in the `alarms` property. 返回在`alarms`属性中的服务告警。|
|selectTrigger|query 查询                 R|turn the associated trigger in the `trigger` property. 返回在`trigger`属性中的关联触发器。|
|sortfield|string/array 字符串/数组   Sort|the result by the given properties. 按指定的属性对结果分类。<br><br>Possible values are: `name` and `sortorder`. 许可值是：`name`和`sortorder`。|
|countOutput|boolean 布尔值             Th|se parameters being common for all `get` methods are described in detail in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters). 这些参数非常普遍，适用于所有`get`方法，具体描述详见于[reference commentary](/zh/manual/api/reference_commentary#common_get_method_parameters)。|
|editable|boolean 布尔值             ::|<|
|excludeSearch|boolean 布尔值             ::|<|
|filter|object 对象                :|:|
|limit|integer 整数型             ::|<|
|output|query 查询                 :|:|
|preservekeys|boolean 布尔值             ::|<|
|search|object 对象                :|:|
|searchByAny|boolean 布尔值             ::|<|
|searchWildcardsEnabled|boolean 布尔值             ::|<|
|sortorder|string/array 字符串/数组   :::|<|
|startSearch|boolean 布尔值             ::|<|

[comment]: # ({/new-e7637a1d})

[comment]: # ({new-7223bab1})
### Return values 返回值

`(integer/array)` Returns either: 返回两者其中任一：

-   an array of objects; 一组对象；
-   the count of retrieved objects, if the `countOutput` parameter has
    been used. 如果已经使用了`countOutput`参数，则检索对象的计数。

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Examples 范例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-cd4e4011})
#### Retrieving all services 检索所有服务

Retrieve all data about all services and their dependencies.
检索有关所有服务及其依赖关系的所有数据。

Request 请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "service.get",
    "params": {
        "output": "extend",
        "selectDependencies": "extend"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response 响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "serviceid": "2",
            "name": "Server 1",
            "status": "0",
            "algorithm": "1",
            "triggerid": "0",
            "showsla": "1",
            "goodsla": "99.9000",
            "sortorder": "0",
            "dependencies": []
        },
        {
            "serviceid": "3",
            "name": "Data center 1",
            "status": "0",
            "algorithm": "1",
            "triggerid": "0",
            "showsla": "1",
            "goodsla": "99.9000",
            "sortorder": "0",
            "dependencies": [
                {
                    "linkid": "11",
                    "serviceupid": "3",
                    "servicedownid": "2",
                    "soft": "0",
                    "sortorder": "0",
                    "serviceid": "2"
                },
                {
                    "linkid": "10",
                    "serviceupid": "3",
                    "servicedownid": "5",
                    "soft": "0",
                    "sortorder": "1",
                    "serviceid": "5"
                }
            ]
        },
        {
            "serviceid": "5",
            "name": "Server 2",
            "status": "0",
            "algorithm": "1",
            "triggerid": "0",
            "showsla": "1",
            "goodsla": "99.9900",
            "sortorder": "1",
            "dependencies": []
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-cd4e4011})

[comment]: # ({new-05e3cf18})
### Source 源码

CService::get() in
*frontends/php/include/classes/api/services/CService.php*.
CService::get()方法可在*frontends/php/include/classes/api/services/CService.php*中参考。

[comment]: # ({/new-05e3cf18})
