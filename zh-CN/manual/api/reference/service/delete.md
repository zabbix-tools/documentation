[comment]: # translation:outdated

[comment]: # ({new-bf12fc32})
# 删除

[comment]: # ({/new-bf12fc32})

[comment]: # ({new-8ea5261f})
### Description 说明

`object service.delete(array serviceIds)`

This method allows to delete services. 此方法允许删除服务。

Services with hard-dependent child services cannot be deleted.
与子级服务有硬依赖关系的服务无法被删除。

[comment]: # ({/new-8ea5261f})

[comment]: # ({new-7af05f92})
### Parameters 参数

`(array)` IDs of the services to delete. `(array)`要删除的服务ID。

[comment]: # ({/new-7af05f92})

[comment]: # ({new-23960c29})
### Return values 返回值

`(object)` Returns an object containing the IDs of the deleted services
under the `serviceids` property.
`(object)`返回一个对象，该对象包含在`serviceids`属性中被删除服务的ID。

[comment]: # ({/new-23960c29})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-5036ec70})
#### Deleting multiple services 删除多个服务

Delete two services. 删除两个服务。

Request 请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "service.delete",
    "params": [
        "4",
        "5"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Response 响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "serviceids": [
            "4",
            "5"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-5036ec70})

[comment]: # ({new-c99e5046})
### Source 源码

CService::delete() in
*frontends/php/include/classes/api/services/CService.php*.
CService::delete()方法可在*frontends/php/include/classes/api/services/CService.php*中参考。

[comment]: # ({/new-c99e5046})
