[comment]: # translation:outdated

[comment]: # ({new-bc1a928b})
# 删除

[comment]: # ({/new-bc1a928b})

[comment]: # ({new-24398e69})
### Description 描述

`object map.delete(array mapIds)`

This method allows to delete maps. 这个方法允许删除拓扑图

[comment]: # ({/new-24398e69})

[comment]: # ({new-57a8b77a})
### Parameters 参数

`(array)` IDs of the maps to delete. 需要删除拓扑图的IDS

[comment]: # ({/new-57a8b77a})

[comment]: # ({new-f04e039f})
### Return values 返回值

`(object)` Returns an object containing the IDs of the deleted maps
under the `sysmapids` property.
返回包含“sysmapid”属性下的已删除拓扑图的IDS的对象。

[comment]: # ({/new-f04e039f})

[comment]: # ({new-b41637d2})
### Examples 示例如下

[comment]: # ({/new-b41637d2})

[comment]: # ({new-87539545})
#### Delete multiple maps 删除多个拓扑图

Delete two maps.删除2个

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "map.delete",
    "params": [
        "12",
        "34"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "sysmapids": [
            "12",
            "34"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-87539545})

[comment]: # ({new-4880bc2b})
### Source

CMap::delete() in *frontends/php/include/classes/api/services/CMap.php*.

[comment]: # ({/new-4880bc2b})
