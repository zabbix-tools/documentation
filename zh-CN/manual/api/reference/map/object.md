[comment]: # translation:outdated

[comment]: # ({new-cdacabd4})
# > 对象

The following objects are directly related to the `map` API.
以下内容是关于拓扑图接口。

[comment]: # ({/new-cdacabd4})

[comment]: # ({new-62f73225})
### Map 拓扑图

拓扑图对象具有以下属性 The map object has the following properties.

|Property|Type|Description|
|--------|----|-----------|
|sysmapid|string|*(readonly)* ID of the map. 拓扑图id|
|**height**<br>(required)|integer|Height of the map in pixels. 拓扑图画布高度|
|**name**<br>(required)|string|Name of the map. 拓扑图名称|
|**width**<br>(required)|integer|Width of the map in pixels.拓扑图宽度|
|backgroundid|string|ID of the image used as the background for the map.拓扑图背景图像id|
|expand\_macros|integer|Whether to expand macros in labels when configuring the map.<br><br>Possible values:<br>0 - *(default)* do not expand macros;<br>1 - expand macros. 配置拓扑图时是否展开标签中的宏<br><br>可能的值:<br>0 - *(默认)* 不展开;<br>1 - 展开.|
|expandproblem|integer|Whether the the problem trigger will be displayed for elements with a single problem.<br><br>Possible values:<br>0 - always display the number of problems;<br>1 - *(default)* display the problem trigger if there's only one problem. 如果只有一个触发器告警是否显示详情，<br>\\\\可能的值：<br>0是只显示数目，\\\\1是显示触发器详情|
|grid\_align|integer|Whether to enable grid aligning.<br><br>Possible values:<br>0 - disable grid aligning;<br>1 - *(default)* enable grid aligning. 是否启用网格对齐<br>\\\\可能的值：<br>0是不用<br>1是使用|
|grid\_show|integer|Whether to show the grid on the map.<br><br>Possible values:<br>0 - do not show the grid;<br>1 - *(default)* show the grid. 是否显示拓扑图网格<br>\\\\可能的值：<br>0是不显示<br>1是显示|
|grid\_size|integer|Size of the map grid in pixels.<br><br>Supported values: 20, 40, 50, 75 and 100.<br><br>Default: 50. 拓扑图网格的大小（以像素为单位）<br><br>支持20, 40, 50, 75，100像素，<br>\\\\默认是50|
|highlight|integer|Whether icon highlighting is enabled.<br><br>Possible values:<br>0 - highlighting disabled;<br>1 - *(default)* highlighting enabled. 是否启用图标高亮显示，<br>\\\\可能的值：<br>0是不用<br>1是使用|
|iconmapid|string|ID of the icon map used on the map. 拓扑图使用图表的ID|
|label\_format|integer|Whether to enable advanced labels.<br><br>Possible values:<br>0 - *(default)* disable advanced labels;<br>1 - enable advanced labels. 是否启用高级标签<br>\\\\可能的值：<br>0是不用<br>1是使用|
|label\_location|integer|Location of the map element label.<br><br>Possible values:<br>0 - *(default)* bottom;<br>1 - left;<br>2 - right;<br>3 - top. 拓扑图标签的位置<br>\\\\可能的值：<br>0是底部<br>1是左边<br>2是右边<br>3是顶部|
|label\_string\_host|string|Custom label for host elements.<br><br>Required for maps with custom host label type. 主机元素自定义标签 ；需要拓扑图中的主机自定义标签类型|
|label\_string\_hostgroup|string|Custom label for host group elements.<br><br>Required for maps with custom host group label type. 主机组元素自定义标签 ；需要拓扑图中的主机组自定义标签类型|
|label\_string\_image|string|Custom label for image elements.<br><br>Required for maps with custom image label type. 图像元素的自定义标签。|
|label\_string\_map|string|Custom label for map elements.<br><br>Required for maps with custom map label type. 拓扑图元素自定义标签|
|label\_string\_trigger|string|Custom label for trigger elements.<br><br>Required for maps with custom trigger label type. 触发器元素自定义标签|
|label\_type|integer|Map element label type.<br><br>Possible values:<br>0 - label;<br>1 - IP address;<br>2 - *(default)* element name;<br>3 - status only;<br>4 - nothing. 拓扑图元素的标签类型 可能的类型：0：标签 1：IP地址 2：元素名称（默认） 3：状态 4：没有|
|label\_type\_host|integer|Label type for host elements.<br><br>Possible values:<br>0 - label;<br>1 - IP address;<br>2 - *(default)* element name;<br>3 - status only;<br>4 - nothing;<br>5 - custom. 主机元素的标签类型 可能的值：0：标签 1：ip地址 2：元素名称（默认）3：状态 4：没有 5：自定义|
|label\_type\_hostgroup|integer|Label type for host group elements.<br><br>Possible values:<br>0 - label;<br>2 - *(default)* element name;<br>3 - status only;<br>4 - nothing;<br>5 - custom. 主机组元素的标签类型 可能的值：0：标签 1：ip地址 2：元素名称（默认）3：状态 4：没有 5：自定义|
|label\_type\_image|integer|Label type for host group elements.<br><br>Possible values:<br>0 - label;<br>2 - *(default)* element name;<br>4 - nothing;<br>5 - custom. 图像元素的标签类型 可能的值：0：标签 1：ip地址 2：元素名称（默认）3：状态 4：没有 5：自定义|
|label\_type\_map|integer|Label type for map elements.<br><br>Possible values:<br>0 - label;<br>2 - *(default)* element name;<br>3 - status only;<br>4 - nothing;<br>5 - custom. 拓扑图元素的标签类型 可能的值：0：标签 1：ip地址 2：元素名称（默认）3：状态 4：没有 5：自定义|
|label\_type\_trigger|integer|Label type for trigger elements.<br><br>Possible values:<br>0 - label;<br>2 - *(default)* element name;<br>3 - status only;<br>4 - nothing;<br>5 - custom. 触发器元素的标签类型 可能的值：0：标签 1：ip地址 2：元素名称（默认）3：状态 4：没有 5：自定义|
|markelements|integer|Whether to highlight map elements that have recently changed their status.<br><br>Possible values:<br>0 - *(default)* do not highlight elements;<br>1 - highlight elements. 是否突出显示最近更改其状态的拓扑图元素 可能的值：0：不高亮 1：显示高亮|
|severity\_min|integer|Minimum severity of the triggers that will be displayed on the map.<br><br>Refer to the [trigger "severity" property](/manual/api/reference/trigger/object#trigger) for a list of supported trigger severities. 显示在拓扑图上的严重程度最小触发器。参考[trigger "severity" property](/manual/api/reference/trigger/object#trigger)，获取支持的触发器严重程度列表。|
|show\_unack|integer|How problems should be displayed.<br><br>Possible values:<br>0 - *(default)* display the count of all problems;<br>1 - display only the count of unacknowledged problems;<br>2 - display the count of acknowledged and unacknowledged problems separately. 如何显示问题， 可能的值： 0：显示所有问题的总数 1：仅显示未确认问题的总数 2：分别显示已确认和未确认的数目|
|userid|string|Map owner user ID. 拓扑图所有用户的id|
|private|integer|Type of map sharing.<br><br>Possible values:<br>0 - public map;<br>1 - *(default)* private map. 拓扑图的共享类型 可能的值：0：公共的拓扑图 1：私有的拓扑图|

[comment]: # ({/new-62f73225})

[comment]: # ({new-c7b92dfc})
### Map element 拓扑图元素

The map element object defines an object displayed on a map. It has the
following properties.
拓扑图元素对象定义显示在拓扑图上的对象。它具有以下属性。

|Property|Type|Description|
|--------|----|-----------|
|selementid|string|*(readonly)* ID of the map element. 拓扑图元素的id|
|**elements**<br>(required)|array|Element data object. Required for host, host group, trigger and map type elements.元素数据对象。需要主机、主机组、触发器和拓扑图类型元素。|
|**elementtype**<br>(required)|integer|Type of map element.<br><br>Possible values:<br>0 - host;<br>1 - map;<br>2 - trigger;<br>3 - host group;<br>4 - image. 拓扑图元素类型<br><br>可能的值：\\\\0-主机 1-拓扑图 2-触发器 3-主机组 4-图像|
|**iconid\_off**<br>(required)|string|ID of the image used to display the element in default state. 用于在默认状态下显示元素的图像的ID。|
|areatype|integer|How separate host group hosts should be displayed.<br><br>Possible values:<br>0 - *(default)* the host group element will take up the whole map;<br>1 - the host group element will have a fixed size. 应该如何显示独立的主机组主机。 \\\\可能的值<br>0-主机组元素占用整个拓扑图 1-主机组元素的大小是固定的|
|application|string|Name of the application to display problems from. Used only for host and host group map elements. 显示问题的应用程序的名称。只用于主机和主机组映射元素。|
|elementsubtype|integer|How a host group element should be displayed on a map.<br><br>Possible values:<br>0 - *(default)* display the host group as a single element;<br>1 - display each host in the group separately. 一个主机组元素如何显示在拓扑图上<br><br>可能的值：<br>0- 显示主机组作为一个单独的元素 1-分别显示组中的每个主机|
|height|integer|Height of the fixed size host group element in pixels.<br><br>Default: 200. 固定大小的主机组元素的高度(以像素为单位)。<br><br>默认是：200|
|iconid\_disabled|string|ID of the image used to display disabled map elements. Unused for image elements. 用于显示禁用映射元素的图像的ID。未使用的图像元素。|
|iconid\_maintenance|string|ID of the image used to display map elements in maintenance. Unused for image elements. 用于显示维护中的拓扑图元素的图像的ID。未使用的图像元素。|
|iconid\_on|string|ID of the image used to display map elements with problems. Unused for image elements. 用于显示有问题的拓扑图元素的图像的ID。未使用的图像元素。|
|label|string|Label of the element. 元素的标签|
|label\_location|integer|Location of the map element label.<br><br>Possible values:<br>-1 - *(default)* default location;<br>0 - bottom;<br>1 - left;<br>2 - right;<br>3 - top. 映射元素标签的位置<br>\\\\可能的值：<br>-1: 默认的位置 0-底部 1-左边 2-右边 3-上边|
|permission|integer|Type of permission level.<br><br>Possible values:<br>-1 - none;<br>2 - read only;<br>3 - read-write. 类型的权限级别<br><br>可能的值： -1:-没有权限 2-只读权限 3-读写权限|
|sysmapid|string|*(readonly)* ID of the map that the element belongs to. 元素所述拓扑图的ID|
|urls|array|Map element URLs.<br><br>The map element URL object is [described in detail below](object#map_element_url). 拓扑图元素的URLS<br>\\\<br>|
|use\_iconmap|integer|Whether icon mapping must be used for host elements.<br><br>Possible values:<br>0 - do not use icon mapping;<br>1 - *(default)* use icon mapping. 是否必须为主机元素使用图标映射。<br>\\\\可能的值：<br>0-不使用图标映射 1-使用图表映射（默认的）|
|viewtype|integer|Host group element placing algorithm.<br><br>Possible values:<br>0 - *(default)* grid. 主机组元素放置算法<br>\\\\可能的值：<br>0-网格|
|width|integer|Width of the fixed size host group element in pixels.<br><br>Default: 200. 主机组元素固定的像素宽度。<br><br>默认是：200|
|x|integer|X-coordinates of the element in pixels.<br><br>Default: 0. 元素的x坐标，单位为像素。<br><br>默认是：0|
|y|integer|Y-coordinates of the element in pixels.<br><br>Default: 0. 元素的y坐标，单位为像素。<br><br>默认是：0|

[comment]: # ({/new-c7b92dfc})

[comment]: # ({new-305dd3e0})
#### Map element Host 拓扑图元素的主机

The map element Host object defines one host element.
拓扑图元素中的主机对象定义是一个主机元素

|Property|Type|Description|
|--------|----|-----------|
|hostid|string|Host ID|

[comment]: # ({/new-305dd3e0})

[comment]: # ({new-e1118d67})
#### Map element Host group 拓扑图元素中的主机组

The map element Host group object defines one host group element.
拓扑图元素中的主机组对象定义是一个主机组元素。

|Property|Type|Description|
|--------|----|-----------|
|groupid|string|Host group ID|

[comment]: # ({/new-e1118d67})

[comment]: # ({new-2d745dcc})
#### Map element Map 拓扑图元素中的拓扑图

The map element Map object defines one map element.
拓扑图元素中的拓扑图对象默认是一个拓扑图元素

|Property|Type|Description|
|--------|----|-----------|
|sysmapid|string|Map ID|

[comment]: # ({/new-2d745dcc})

[comment]: # ({new-0283fd46})
#### Map element Trigger 拓扑图元素中的触发器

The map element Trigger object defines one or more trigger elements.
拓扑图元素中的触发器对象定义的是一个或者多个触发器元素

|Property|Type|Description|
|--------|----|-----------|
|triggerid|string|Trigger ID|

[comment]: # ({/new-0283fd46})

[comment]: # ({new-a7495119})
#### Map element URL拓扑图元素中的URL

The map element URL object defines a clickable link that will be
available for a specific map element. It has the following properties:
拓扑图元素URL对象定义了一个可单击的链接，该链接将对特定的map元素可用。它具有以下特性:

|Property|Type|Description|
|--------|----|-----------|
|sysmapelementurlid|string|*(readonly)* ID of the map element URL.|
|**name**<br>(required)|string|Link caption.|
|**url**<br>(required)|string|Link URL.|
|selementid|string|ID of the map element that the URL belongs to.|

[comment]: # ({/new-a7495119})

[comment]: # ({new-5a112b10})
### Map link 拓扑图关联

The map link object defines a link between two map elements. It has the
following properties.
拓扑图链接对象定义两个映射元素之间的链接。它具有以下属性。

|Property|Type|Description|
|--------|----|-----------|
|linkid|string|*(readonly)* ID of the map link.|
|**selementid1**<br>(required)|string|ID of the first map element linked on one end. 在一端连接的第一个拓扑图元素的ID。|
|**selementid2**<br>(required)|string|ID of the first map element linked on the other end. 另一端连接的第一个拓扑图元素的ID。|
|color|string|Line color as a hexadecimal color code.<br><br>Default: `000000`. 行颜色作为十六进制颜色代码。<br><br>默认是：“000000”|
|drawtype|integer|Link line draw style.<br><br>Possible values:<br>0 - *(default)* line;<br>2 - bold line;<br>3 - dotted line;<br>4 - dashed line. 链接线画的风格。<br><br>可能的值：0-线（默认）2-粗线 3-点线 4-虚线|
|label|string|Link label. 行标签|
|linktriggers|array|Map link triggers to use as link status indicators.<br><br>The map link trigger object is [described in detail below](object#map_link_trigger). 拓扑图链接触发器用作链接状态指示器。|
|permission|integer|Type of permission level.<br><br>Possible values:<br>-1 - none;<br>2 - read only;<br>3 - read-write. 权限等级类型<br>\\\\可能的值： -1-没有 2-只读 3-可读可写|
|sysmapid|string|ID of the map the link belongs to. 该关联所属拓扑图ID|

[comment]: # ({/new-5a112b10})

[comment]: # ({new-1e44fd2a})
#### Map link trigger 拓扑图关联触发器

The map link trigger object defines a map link status indicator based on
the state of a trigger. It has the following properties:
拓扑图链接触发器对象根据触发器的状态定义一个拓扑图链接状态指示器。它具有以下特性:

|Property|Type|Description|
|--------|----|-----------|
|linktriggerid|string|*(readonly)* ID of the map link trigger.|
|**triggerid**<br>(reqiuired)|string|ID of the trigger used as a link indicator.|
|color|string|Indicator color as a hexadecimal color code.<br><br>Default: `DD0000`.|
|drawtype|integer|Indicator draw style.<br><br>Possible values:<br>0 - *(default)* line;<br>2 - bold line;<br>3 - dotted line;<br>4 - dashed line. 指标画的风格<br>\\\\可能的值： 0-线（默认）2-粗线 3-点线 4-虚线|
|linkid|string|ID of the map link that the link trigger belongs to.关联触发器所属拓扑图ID|

[comment]: # ({/new-1e44fd2a})

[comment]: # ({new-219c2bce})
### Map URL 拓扑图URL

The map URL object defines a clickable link that will be available for
all elements of a specific type on the map. It has the following
properties:
拓扑图URL对象定义了一个可单击的链接，该链接可用于映射上特定类型的所有元素。它具有以下特性:

|Property|Type|Description|
|--------|----|-----------|
|sysmapurlid|string|*(readonly)* ID of the map URL. 拓扑图URL ID|
|**name**<br>(required)|string|Link caption. 链接标题。|
|**url**<br>(required)|string|Link URL. 链接URL|
|elementtype|integer|Type of map element for which the URL will be available.<br><br>Refer to the [map element "type" property](object#map_element) for a list of supported types.<br><br>Default: 0. 拓扑图元素可用URL类型<br><br>默认：0|
|sysmapid|string|ID of the map that the URL belongs to. 所属URL的拓扑图ID|

[comment]: # ({/new-219c2bce})

[comment]: # ({new-96c2bee0})
### Map user 拓扑图用户

List of map permissions based on users. It has the following properties:
基于用户的拓扑图权限列表。它具有以下特性:

|Property|Type|Description|
|--------|----|-----------|
|sysmapuserid|string|*(readonly)* ID of the map user. 拓扑图用户ID|
|**userid**<br>(required)|string|User ID.|
|**permission**<br>(required)|integer|Type of permission level.<br><br>Possible values:<br>2 - read only;<br>3 - read-write; 权限等级类型<br>\\\\可能的值： -1-没有 2-只读 3-可读可写|

[comment]: # ({/new-96c2bee0})

[comment]: # ({new-942f03ad})
### Map user group 拓扑图用户组

List of map permissions based on user groups. It has the following
properties: 基于用户组的拓扑图权限列表。它具有以下特性:

|Property|Type|Description|
|--------|----|-----------|
|sysmapusrgrpid|string|*(readonly)* ID of the map user group. 拓扑图用户组的ID|
|**usrgrpid**<br>(required)|string|User group ID.|
|**permission**<br>(required)|integer|Type of permission level.<br><br>Possible values:<br>2 - read only;<br>3 - read-write;权限等级类型<br>\\\\可能的值： -1-没有 2-只读 3-可读可写|

[comment]: # ({/new-942f03ad})

[comment]: # ({new-f6e63a8f})
### Map shapes 地图形状

The map shape object defines an geometric shape (with or without text)
displayed on a map. It has the following properties:
拓扑图形状对象定义了显示在拓扑图上的几何形状(包含或不包含文本)。它具有以下特性:

|Property|Type|Description|
|--------|----|-----------|
|sysmap\_shapeid|string|*(readonly)* ID of the map shape element. 拓扑图形状元素的ID|
|**type** (required)|integer|Type of map shape element.<br><br>Possible values:<br>0 - rectangle;<br>1 - ellipse.<br><br>Property is required when new shapes are created. 拓扑图形状元素的类型<br><br>可能的值：<br>0-矩形 1-椭圆<br><br>创建新形状时需要属性。|
|x|integer|X-coordinates of the shape in pixels.<br><br>Default: 0. 元素的x坐标，单位为像素。<br><br>默认是：0|
|y|integer|Y-coordinates of the shape in pixels.<br><br>Default: 0. 元素的y坐标，单位为像素。<br><br>默认是：0|
|width|integer|Width of the shape in pixels.<br><br>Default: 200. 以像素为单位的形状宽度。<br>\\\\默认是：200|
|height|integer|Height of the shape in pixels.<br><br>Default: 200. 以像素为单位的形状高度。<br>\\\\默认是：200|
|text|string|Text of the shape. 文本的形状。|
|font|integer|Font of the text within shape.<br><br>Possible values:<br>0 - Georgia, serif<br>1 - “Palatino Linotype”, “Book Antiqua”, Palatino, serif<br>2 - “Times New Roman”, Times, serif<br>3 - Arial, Helvetica, sans-serif<br>4 - “Arial Black”, Gadget, sans-serif<br>5 - “Comic Sans MS”, cursive, sans-serif<br>6 - Impact, Charcoal, sans-serif<br>7 - “Lucida Sans Unicode”, “Lucida Grande”, sans-serif<br>8 - Tahoma, Geneva, sans-serif<br>9 - “Trebuchet MS”, Helvetica, sans-serif<br>10 - Verdana, Geneva, sans-serif<br>11 - “Courier New”, Courier, monospace<br>12 - “Lucida Console”, Monaco, monospace<br><br>Default: 9. Font of the text within shape.<br><br>可能的值：<br>0 - Georgia, serif<br>1 - “Palatino Linotype”, “Book Antiqua”, Palatino, serif<br>2 - “Times New Roman”, Times, serif<br>3 - Arial, Helvetica, sans-serif<br>4 - “Arial Black”, Gadget, sans-serif<br>5 - “Comic Sans MS”, cursive, sans-serif<br>6 - Impact, Charcoal, sans-serif<br>7 - “Lucida Sans Unicode”, “Lucida Grande”, sans-serif<br>8 - Tahoma, Geneva, sans-serif<br>9 - “Trebuchet MS”, Helvetica, sans-serif<br>10 - Verdana, Geneva, sans-serif<br>11 - “Courier New”, Courier, monospace<br>12 - “Lucida Console”, Monaco, monospace 默认是：9|
|font\_size|integer|Font size in pixels.<br><br>Default: 11. 字体大小，单位是像素<br><br>默认：11|
|font\_color|string|Font color.<br><br>Default: '000000'. 字体颜色<br><br>默认是：“000000”|
|text\_halign|integer|Horizontal alignment of text.<br><br>Possible values:<br>0 - center;<br>1 - left;<br>2 - right.<br><br>Default: 0. 水平对齐的文本<br>\\\\可能的值：<br>0-中间（默认） 1-左边 2-右边|
|text\_valign|integer|Vertical alignment of text.<br><br>Possible values:<br>0 - middle;<br>1 - top;<br>2 - bottom.<br><br>Default: 0. 垂直对齐文本<br>\\\\可能的值：<br>0-中间（默认） 1-顶部 2-底部|
|border\_type|integer|Type of the border.<br><br>Possible values:<br>0 - none;<br>1 - `—————`;<br>2 - `·····`;<br>3 - `- - -`.<br><br>Default: 0. 边界类型<br><br>可能的值：<br>0-没有（默认） 1 - `—————` 2 - `·····` 3 - `- - -`|
|border\_width|integer|Width of the border in pixels.<br><br>Default: 0. 边框的宽度，以像素为单位<br>\\\\默认：0|
|border\_color|string|Border color.<br><br>Default: '000000'. 边界的颜色<br>\\\\默认：‘000000’|
|background\_color|string|Background color (fill color).<br><br>Default: `(empty)`. 背景颜色（填充颜色）<br><br>默认是：无|
|zindex|integer|Value used to order shapes (z-index).<br><br>Default: 0. 用于定制形状的值(z-index)。<br><br>默认是：0|

[comment]: # ({/new-f6e63a8f})

[comment]: # ({new-9d0acde7})
### Map lines

The map line object defines an line displayed on a map. It has the
following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|sysmap\_shapeid|string|*(readonly)* ID of the map shape element.|
|x1|integer|X-coordinates of the line point 1 in pixels.<br><br>Default: 0.|
|y1|integer|Y-coordinates of the line point 1 in pixels.<br><br>Default: 0.|
|x2|integer|X-coordinates of the line point 2 in pixels.<br><br>Default: 200.|
|y2|integer|Y-coordinates of the line point 2 in pixels.<br><br>Default: 200.|
|line\_type|integer|Type of the lines.<br><br>Possible values:<br>0 - none;<br>1 - `—————`;<br>2 - `·····`;<br>3 - `- - -`.<br><br>Default: 0.|
|line\_width|integer|Width of the lines in pixels.<br><br>Default: 0.|
|line\_color|string|Line color.<br><br>Default: '000000'.|
|zindex|integer|Value used to order all shapes and lines (z-index).<br><br>Default: 0.|

[comment]: # ({/new-9d0acde7})

[comment]: # ({new-b1536eee})
### Map lines 拓扑图线

The map line object defines an line displayed on a map. It has the
following properties:
拓扑图线对象定义显示在拓扑图上的行。它具有以下特性:

|Property|Type|Description|
|--------|----|-----------|
|sysmap\_shapeid|string|*(readonly)* ID of the map shape element. 拓扑图形状元素的ID|
|x1|integer|X-coordinates of the line point 1 in pixels.<br><br>Default: 0. 以像素为单位的直线点1的x坐标。<br>\\\\默认是：0|
|y1|integer|Y-coordinates of the line point 1 in pixels.<br><br>Default: 0. 以像素为单位的直线点1的y坐标。<br>\\\\默认是：0|
|x2|integer|X-coordinates of the line point 2 in pixels.<br><br>Default: 200. 以像素为单位的直线点2的x坐标。<br>\\\\默认是：200|
|y2|integer|Y-coordinates of the line point 2 in pixels.<br><br>Default: 200. 以像素为单位的直线点2的x坐标。<br>\\\\默认是：200|
|line\_type|integer|Type of the border.<br><br>Possible values:<br>0 - none;<br>1 - `—————`;<br>2 - `·····`;<br>3 - `- - -`.<br><br>Default: 0. 边界类型<br><br>可能的值：<br>0-没有（默认） 1 - `—————` 2 - `·····` 3 - `- - -`|
|line\_width|integer|Width of the border in pixels.<br><br>Default: 0. 边框的宽度，以像素为单位<br>\\\\默认：0|
|line\_color|string|Border color.<br><br>Default: '000000'. 边界的颜色<br>\\\\默认：‘000000’|
|zindex|integer|Value used to order shapes (z-index).<br><br>Default: 0. 用于定制形状的值(z-index)。<br><br>默认是：0|

[comment]: # ({/new-b1536eee})

