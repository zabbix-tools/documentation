[comment]: # translation:outdated

[comment]: # ({new-af3de79b})
# > Module object

The following objects are directly related to the `module` API.

[comment]: # ({/new-af3de79b})

[comment]: # ({new-4b252ad6})
### Module

The module object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|-|-------|
|moduleid|string|*(readonly)* ID of the module as stored in the database.|
|**id**<br>(required)|string|Unique module ID as defined by a developer in the [manifest.json](/manual/extensions/frontendmodules#reference) file of the module.<br><br>Possible values for built-in modules:<br>see property "type" description in [Dashboard widget](/manual/api/reference/dashboard/object#dashboard-widget).|
|**relative_path**<br>(required)|string|Path to the directory of the module relative to the directory of the Zabbix frontend.<br><br>Possible values:<br>`widgets/*` - for built-in widget modules;<br>`modules/*` - for third-party modules.|
|status|integer|Whether the module is enabled or disabled.<br><br>Possible values:<br>0 - *(default)* Disabled;<br>1 - Enabled.|
|config|object|[Module configuration](/manual/extensions/frontendmodules#reference).|

Note that for some methods (update, delete) the required/optional parameter combination is different.

[comment]: # ({/new-4b252ad6})
