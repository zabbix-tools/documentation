[comment]: # translation:outdated

[comment]: # ({new-46b153da})
# 删除

[comment]: # ({/new-46b153da})

[comment]: # ({new-60f587fa})
### 描述

`object image.delete(array imageIds)`

此方法允许删除图像.

[comment]: # ({/new-60f587fa})

[comment]: # ({new-c7ef61f7})
### 参数

`(数组)` 要删除的图像ID.

[comment]: # ({/new-c7ef61f7})

[comment]: # ({new-4b3e57f6})
### 返回值

`(对象)` 在`imageids`属性中返回已删除图像ID的对象.

[comment]: # ({/new-4b3e57f6})

[comment]: # ({new-b41637d2})
### 示例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-925dee02})
#### 删除多个图像

删除两个图像.

请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "image.delete",
    "params": [
        "188",
        "192"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "imageids": [
            "188",
            "192"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-925dee02})

[comment]: # ({new-dfdead8e})
### 来源

CImage::delete() in
*frontends/php/include/classes/api/services/CImage.php*.

[comment]: # ({/new-dfdead8e})
