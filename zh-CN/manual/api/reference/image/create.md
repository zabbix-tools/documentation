[comment]: # translation:outdated

[comment]: # ({new-6ea25642})
# 创建

[comment]: # ({/new-6ea25642})

[comment]: # ({new-565b9f0c})
### 描述

`object image.create(object/array images)`

该方法允许创建新的图像.

[comment]: # ({/new-565b9f0c})

[comment]: # ({new-8de82e8e})
### 参数

`(对象/数组)` 要创建的图像.

除[标准图像属性](object#image)了外，该方法接受以下参数:

|属性         类|说明|<|
|------------------|------|-|
|**image**<br>(必选)|字符串   Ba|e64编码图像,编码图像的最大大小为1 MB.|

[comment]: # ({/new-8de82e8e})

[comment]: # ({new-ce55a727})
### 返回值

`(对象)`
返回一个包含“imageid”属性下创建的图像ID的对象。返回的ID的顺序与传递的图像的顺序相匹配.

[comment]: # ({/new-ce55a727})

[comment]: # ({new-b41637d2})
### 示例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-1de081cc})
#### 创建图像

创建一个云图标.

请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "image.create",
    "params": {
        "imagetype": 1,
        "name": "Cloud_(24)",
        "image": "iVBORw0KGgoAAAANSUhEUgAAABgAAAANCAYAAACzbK7QAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAACmAAAApgBNtNH3wAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAIcSURBVDjLrZLbSxRRHMdPKiEiRQ89CD0s+N5j9BIMEf4Hg/jWexD2ZEXQbC9tWUFZimtLhswuZiVujK1UJmYXW9PaCUdtb83enL3P7s6ss5f5dc7EUsmqkPuFH3M4/Ob7+V0OAgC0UyDENFEU03rh1uNOs/lFG75o2i2/rkd9Y3Tgyj3HiaezbukdH9A/rP4E9vWi0u+Y4fuGnMf3DRgYc3Z/84YrQSkD3mgKhFAC+KAEK74Y2Lj3MjPoOokQ3Xyx/1GHeXCifbfO6lRPH/wi+AvZQhGSsgKxdB5CCRkCGPbDgMXBMbukTc4vK5/WRHizsq7fZl2LFuvE4T0BZDTXHtgv4TNUqlUolsqQL2qQwbDEXzBBTIJ7I4y/cfAENmHZF4XrY9Mc+X9HAFmoyXS2ddy1IOg6/KNyBcM0DFP/wFZFCcOy4N9Mw0YkCTOfhdL5AfZQXQBFn2t/ODXHC8FYVcoWjNEQ03qqwTJ5FdI44jg/msoB2Zd5ZKq3q6evA1FUS60bYyyj3AJf3V72HiLZJQxTtRLk1C2IYEg4mTNg63hPd1mOJd7Ict911OMNlWEf0nFxpCt16zcshTuLpGSwDDuPIfv0xzNyQYVGicC0cgUUDLM6Xp02lvvW/V2EBssnxlSGmWsxljw0znV9XfPLjTCW84r+cn7Jc8c2eWrbM6Wbe6/aTJbhJ/TNkWc9/xXW592Xb9iPkKnUfH8BKdLgFy0lDyQAAAAASUVORK5CYII="
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "imageids": [
            "188"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-1de081cc})

[comment]: # ({new-c146c166})
### 来源

CImage::create() in
*frontends/php/include/classes/api/services/CImage.php*.

[comment]: # ({/new-c146c166})
