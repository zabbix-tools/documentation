[comment]: # translation:outdated

[comment]: # ({new-554f8ab9})
# 获取

[comment]: # ({/new-554f8ab9})

[comment]: # ({new-ef6436a6})
### 描述

`integer/array image.get(object parameters)`

该方法允许根据给定的参数获取图像记录.

[comment]: # ({/new-ef6436a6})

[comment]: # ({new-ac7ee93f})
### 参数

`(对象)` 定义要输出的参数.

该方法支持如下参数:

|属性                     类|描述|<|
|------------------------------|------|-|
|imageids|字符串/数组   返回具有|定ID的图像.|
|sysmapids|字符串/数组   返回给定|扑上使用的图像.|
|select\_image|标识          返|“image”属性中的Base64编码图像.|
|sortfield|字符串/数组   按照给定|属性对结果进行排序.<br><br>可能的值: `imageid` 和 `name`.|
|countOutput|布尔值        这些|数对于所有get方法都是通用的，详情可参考[reference commentary](/manual/api/reference_commentary#common_get_method_parameters).|
|editable|布尔值        ::|<|
|excludeSearch|布尔值        ::|<|
|filter|对象          :|:|
|limit|整数          :|:|
|output|查询          :|:|
|preservekeys|布尔值        ::|<|
|search|对象          :|:|
|searchByAny|布尔值        ::|<|
|searchWildcardsEnabled|布尔值        ::|<|
|sortorder|字符串/数组   :::|<|
|startSearch|布尔值        ::|<|

[comment]: # ({/new-ac7ee93f})

[comment]: # ({new-7223bab1})
### 返回值

`(整数/数组)` 返回:

-   一组对象;
-   如果设置了参数`countOutput`,则返回对象的数量.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### 示例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-d6cd6c58})
#### 获取图像

获取ID为2的图像的所有数据。.

请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "image.get",
    "params": {
        "output": "extend",
        "select_image": true,
        "imageids": "2"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "imageid": "2",
            "imagetype": "1",
            "name": "Cloud_(24)",
            "image": "iVBORw0KGgoAAAANSUhEUgAAABgAAAANCAYAAACzbK7QAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAACmAAAApgBNtNH3wAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAIcSURBVDjLrZLbSxRRHMdPKiEiRQ89CD0s+N5j9BIMEf4Hg/jWexD2ZEXQbC9tWUFZimtLhswuZiVujK1UJmYXW9PaCUdtb83enL3P7s6ss5f5dc7EUsmqkPuFH3M4/Ob7+V0OAgC0UyDENFEU03rh1uNOs/lFG75o2i2/rkd9Y3Tgyj3HiaezbukdH9A/rP4E9vWi0u+Y4fuGnMf3DRgYc3Z/84YrQSkD3mgKhFAC+KAEK74Y2Lj3MjPoOokQ3Xyx/1GHeXCifbfO6lRPH/wi+AvZQhGSsgKxdB5CCRkCGPbDgMXBMbukTc4vK5/WRHizsq7fZl2LFuvE4T0BZDTXHtgv4TNUqlUolsqQL2qQwbDEXzBBTIJ7I4y/cfAENmHZF4XrY9Mc+X9HAFmoyXS2ddy1IOg6/KNyBcM0DFP/wFZFCcOy4N9Mw0YkCTOfhdL5AfZQXQBFn2t/ODXHC8FYVcoWjNEQ03qqwTJ5FdI44jg/msoB2Zd5ZKq3q6evA1FUS60bYyyj3AJf3V72HiLZJQxTtRLk1C2IYEg4mTNg63hPd1mOJd7Ict911OMNlWEf0nFxpCt16zcshTuLpGSwDDuPIfv0xzNyQYVGicC0cgUUDLM6Xp02lvvW/V2EBssnxlSGmWsxljw0znV9XfPLjTCW84r+cn7Jc8c2eWrbM6Wbe6/aTJbhJ/TNkWc9/xXW592Xb9iPkKnUfH8BKdLgFy0lDyQAAAAASUVORK5CYII="
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-d6cd6c58})

[comment]: # ({new-ade21c34})
### 来源

CImage::get() in
*frontends/php/include/classes/api/services/CImage.php*.

[comment]: # ({/new-ade21c34})
