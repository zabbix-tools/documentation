[comment]: # translation:outdated

[comment]: # ({new-d229f871})
# 更新

[comment]: # ({/new-d229f871})

[comment]: # ({new-71e9b7b3})
### 描述

`object image.update(object/array images)`

该方法允许对已存在的图片进行更新.

[comment]: # ({/new-71e9b7b3})

[comment]: # ({new-b1c7183d})
### 参数

`(对象/数组)` 要更新的图像属性

必须为每个图像定义`imageid`属性，所有其他属性都是可选的。
只有通过的属性将被更新，所有其他属性将保持不变

除了[标准图像属性](object#image)外，该方法接受以下参数：

|参数    类|描述|<|
|-------------|------|-|
|image|字符串   Ba|e64编码图像。 编码图像的最大大小为1 MB.|

[comment]: # ({/new-b1c7183d})

[comment]: # ({new-a4a768f8})
### 返回值

`(对象)` 在`imageids`属性中返回已更新图像ID的对象.

[comment]: # ({/new-a4a768f8})

[comment]: # ({new-b41637d2})
### 示例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-4aaa0fb1})
#### 重命名图像

将图像重命名为"Cloud icon".

请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "image.update",
    "params": {
        "imageid": "2",
        "name": "Cloud icon"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "imageids": [
            "2"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-4aaa0fb1})

[comment]: # ({new-5590c086})
### 来源

CImage::update() in
*frontends/php/include/classes/api/services/CImage.php*.

[comment]: # ({/new-5590c086})
