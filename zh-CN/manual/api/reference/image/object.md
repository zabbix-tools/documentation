[comment]: # translation:outdated

[comment]: # ({new-e9ddc4ef})
# > 图像对象

以下对象是和`image` API直接相关.

[comment]: # ({/new-e9ddc4ef})

[comment]: # ({new-eee50efa})
### 图像

图像对象具有以下属性:

|属性        类|描述|<|
|-----------------|------|-|
|imageid|字符串   *(|读)* 图像的ID.|
|**name**<br>(必选)|字符串   图像|名称.|
|imagetype|整型     图|类型.<br><br>可能的值:<br>1 - *(默认)* 图标;<br>2 - 背景图.|

[comment]: # ({/new-eee50efa})
