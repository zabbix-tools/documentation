[comment]: # translation:outdated

[comment]: # ({new-2593ef3e})
# LLD rule LLD规则

## LLD 规则

This class is designed to work with low level discovery rules.
此类设计用于低级发现规则。

Object references:\
对象参考:\

-   [LLD rule](/manual/api/reference/discoveryrule/object#lld_rule)

Available methods:\

-   [discoveryrule.copy](/manual/api/reference/discoveryrule/copy) -
    copying LLD rules 复制LLD规则
-   [discoveryrule.create](/manual/api/reference/discoveryrule/create) -
    creating new LLD rules 创建新的LLD规则
-   [discoveryrule.delete](/manual/api/reference/discoveryrule/delete) -
    deleting LLD rules 删除LLD规则
-   [discoveryrule.get](/manual/api/reference/discoveryrule/get) -
    retrieving LLD rules
-   [discoveryrule.update](/manual/api/reference/discoveryrule/update) -
    updating LLD rules 更新LLD规则

[comment]: # ({/new-2593ef3e})
