[comment]: # translation:outdated

[comment]: # ({new-34f25b55})
# apiinfo.version

[comment]: # ({/new-34f25b55})

[comment]: # ({new-bc32199a})
### 说明

`string apiinfo.version(array)`

该方法用于获取 Zabbix API 版本。

[comment]: # ({/new-bc32199a})

[comment]: # ({new-4fa8a419})
### 参数

::: noteimportant
此方法可用于未经身份验证的用户，必须在发送
JSON-RPC 请求中不加`auth`参数的情况下调用。
:::

`(array)` 该方法接受一个空的数组。

[comment]: # ({/new-4fa8a419})

[comment]: # ({new-53521d11})
### 返回值

`(string)` 返回 Zabbix API 的版本。

::: notetip
从 Zabbix 2.0.4 版本开始，API 的版本与 Zabbix
的版本相匹配。
:::

[comment]: # ({/new-53521d11})

[comment]: # ({new-b41637d2})
### 范例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-6ba4bdd0})
#### 获取 API 版本

获取 Zabbix API 版本。

请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "apiinfo.version",
    "params": [],
    "id": 1
}
```

响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": "4.0.0",
    "id": 1
}
```

[comment]: # ({/new-6ba4bdd0})

[comment]: # ({new-25499b5e})
### 来源

CAPIInfo::version() in
*frontends/php/include/classes/api/services/CAPIInfo.php*.

### Description

`string apiinfo.version(array)`

This method allows to retrieve the version of the Zabbix API.

### Parameters

::: noteimportant
This method is available to unauthenticated users
and must be called without the `auth` parameter in the JSON-RPC
request.
:::

`(array)` The method accepts an empty array.

### Return values

`(string)` Returns the version of the Zabbix API.

::: notetip
Starting from Zabbix 2.0.4 the version of the API
matches the version of Zabbix.
:::

### Examples

#### Retrieving the version of the API

Retrieve the version of the Zabbix API.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "apiinfo.version",
    "params": [],
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": "4.0.0",
    "id": 1
}
```

### Source

CAPIInfo::version() in
*frontends/php/include/classes/api/services/CAPIInfo.php*.

[comment]: # ({/new-25499b5e})
