[comment]: # translation:outdated

[comment]: # ({new-915ea6d8})
# > 对象

The following objects are directly related to the `trend` API.
以下对象与`trend` API直接相关。 

::: noteclassic
Trend objects differ
depending on the item's type of information. They are created by the
Zabbix server and cannot be modified via the API.
趋势对象根据监控项类型的信息而有所不同，它们由Zabbix
server创建，不能通过API进行修改。
:::

[comment]: # ({/new-915ea6d8})

[comment]: # ({new-bf6c9e59})
### Float trend 浮点型趋势

The float trend object has the following properties.
浮点型趋势对象具有以下属性。

|Property 属性   T|pe 类型          Des|ription 说明|
|-------------------|----------------------|--------------|
|clock|timestamp 时间戳   Ti|e when that value was received. 收取该值的时间。|
|itemid|string 字符串      ID|of the related item. 相关监控项ID。|
|num|integer 整数型     Nu|ber of values within this hour. 在该小时内值的数量。|
|value\_min|float 浮点型       Ho|rly minimum value. 每小时最小值。|
|value\_avg|float 浮点型       Ho|rly average value. 每小时平均值。|
|value\_max|float 浮点型       Ho|rly maximum value. 每小时最大值。|

[comment]: # ({/new-bf6c9e59})

[comment]: # ({new-fa526f68})
### Integer trend 整数型趋势

The integer trend object has the following properties.
整数型趋势对象具有以下属性。

|Property 属性   T|pe 类型          Des|ription 说明|
|-------------------|----------------------|--------------|
|clock|timestamp 时间戳   Ti|e when that value was received. 收取该值的时间。|
|itemid|string 字符串      ID|of the related item. 相关监控项ID。|
|num|integer 整数型     Nu|ber of values within this hour. 在该小时内值的数量。|
|value\_min|integer 整数型     Ho|rly minimum value. 每小时最小值。|
|value\_avg|integer 整数型     Ho|rly average value. 每小时平均值。|
|value\_max|integer 整数型     Ho|rly maximum value. 每小时最大值。|

[comment]: # ({/new-fa526f68})
