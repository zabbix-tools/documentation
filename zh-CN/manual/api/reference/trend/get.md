[comment]: # translation:outdated

[comment]: # ({new-669b84c5})
# 获取

[comment]: # ({/new-669b84c5})

[comment]: # ({new-b49a69bd})
### Description 说明

`integer/array trend.get(object parameters)`

The method allows to retrieve trend data according to the given
parameters. 该方法用于根据指定的参数检索趋势数据。

[comment]: # ({/new-b49a69bd})

[comment]: # ({new-e33350ec})
### Parameters 参数

`(object)` Parameters defining the desired output.
`(object)`定义所需输出的参数。 The method supports the following
parameters. 该方法提供以下参数。

|Parameter 参数   T|pe 类型                  Des|ription 说明|
|--------------------|------------------------------|--------------|
|itemids|string/array 字符串/数组   Retu|n only trends with the given item IDs. 仅返回指定监控项ID的趋势。|
|time\_from|timestamp 时间戳           Re|urn only values that have been collected after or at the given time. 仅返回指定时间（包含）之后已采集的值。|
|time\_till|timestamp 时间戳           Re|urn only values that have been collected before or at the given time. 仅返回指定时间（包含）之前已采集的值。|
|countOutput|boolean 布尔值             Co|nt the number of retrieved objects. 计算检索对象的数量。|
|limit|integer 整数型             Li|it the amount of retrieved objects. 限制检索对象的数量。|
|output|query 查询                 S|t fields to output. 输出设置的字段。|

[comment]: # ({/new-e33350ec})

[comment]: # ({new-7223bab1})
### Return values 返回值

`(integer/array)` Returns either: 返回两者其中任一：

-   an array of objects; 一组对象；
-   the count of retrieved objects, if the `countOutput` parameter has
    been used. 如果已经使用了`countOutput`参数，则检索对象的计数。

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Examples 范例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-98fd9c3f})
#### Retrieving item trend data 检索监控项趋势数据

Request 请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "trend.get",
    "params": {
        "output": [
            "itemid",
            "clock",
            "num",
            "value_min",
            "value_avg",
            "value_max",
        ],
        "itemids": [
            "23715"
        ],
        "limit": "1"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response 响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "itemid": "23715",
            "clock": "1446199200",
            "num": "60",
            "value_min": "0.1650",
            "value_avg": "0.2168",
            "value_max": "0.3500",
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-98fd9c3f})

[comment]: # ({new-852a9753})
### Source 源码

CTrend::get() in
*frontends/php/include/classes/api/services/CTrend.php*.
CTrend::get()方法可在*frontends/php/include/classes/api/services/CTrend.php*中参考。

[comment]: # ({/new-852a9753})
