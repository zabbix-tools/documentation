[comment]: # translation:outdated

[comment]: # ({new-4312b82f})
# Event 事件

## 事件

This class is designed to work with events. 这个类用于配合事件使用

Object references:\

-   [Event](/manual/api/reference/event/object#host)

对象引用:\

-   [事件](/zh/manual/api/reference/event/object#host)

Available methods:\

-   [event.get](/manual/api/reference/event/get) - retrieving events
-   [event.acknowledge](/manual/api/reference/event/acknowledge) -
    acknowledging events

可用方法:\

-   [event.get](/zh/manual/api/reference/event/get) - 获取事件
-   [event.acknowledge](/zh/manual/api/reference/event/acknowledge) -
    确认事件

[comment]: # ({/new-4312b82f})
