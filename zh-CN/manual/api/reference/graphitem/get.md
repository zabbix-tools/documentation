[comment]: # translation:outdated

[comment]: # ({new-43e64f56})
# graphitem.get

[comment]: # ({/new-43e64f56})

[comment]: # ({new-fe22ea70})
### Description

[comment]: # ({/new-fe22ea70})

[comment]: # ({new-d30698ba})
### 描述

`integer/array graphitem.get(object parameters)`
`整数/数组 graphitem.get(object parameters)`

The method allows to retrieve graph items according to the given
parameters. 此方法用于根据给定参数来获取图表监控项。

[comment]: # ({/new-d30698ba})

[comment]: # ({new-7223bab1})
### Parameters

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### 参数

`(object)` Parameters defining the desired output. `(对象)`
定义所需输出的参数。

The method supports the following parameters. 此方法支持以下参数：

|Parameter|Type|Description|
|---------|----|-----------|
|gitemids|string/array|Return only graph items with the given IDs.|
|graphids|string/array|Return only graph items that belong to the given graphs.|
|itemids|string/array|Return only graph items with the given item IDs.|
|type|integer|Return only graph items with the given type.<br><br>Refer to the [graph item object page](object#graph_item) for a list of supported graph item types.|
|selectGraphs|query|Return the graph that the item belongs to as an array in the `graphs` property.|
|sortfield|string/array|Sort the result by the given properties.<br><br>Possible values are: `gitemid`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters) page.|
|editable|boolean|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|sortorder|string/array|^|

|参数           类|描述|<|
|--------------------|------|-|
|gitemids|string/array|仅返回给定ID的图表监控项|
|graphids|string/array|仅返回属于给定图表的图表监控项|
|itemids|string/array|仅返回具有给定监控项ID的图表监控项|
|type|integer|仅返回给定类型的图表监控项<br><br>有关支持的图表监控项的类型，请参考[图表监控项对象](object#graph_item).|
|selectGraphs|query|在 `graphs(图表)`属性下，以数组的形式返回监控项所属的图表|
|sortfield|string/array|根据给定属性对结果进行排序.<br><br>可能值: `gitemid`.|
|countOutput|boolean|以下参数为get方法通常参数，在[参考注释](/zh/manual/api/reference_commentary#common_get_method_parameters)有详细说明.|
|editable|boolean|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|sortorder|string/array|^|

[comment]: # ({/new-b41637d2})

[comment]: # ({new-37da5f20})
### Return values

[comment]: # ({/new-37da5f20})

[comment]: # ({new-09a26e71})
### 返回值

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.

`(整数/数组)` 返回:

-   一个数组对象;
-   如果使用了 `countOutput` 参数，返回获取的对象的数量.

[comment]: # ({/new-09a26e71})

[comment]: # ({new-8d13cada})
### Examples

### 例子

#### Retrieving graph items from a graph

#### 从图表中获取图表监控项

Retrieve all graph items used in a graph with additional information
about the item and the host.
获取图表中使用的所有图表监控项以及有关项目和主机的其他信息

Request: 请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "graphitem.get",
    "params": {
        "output": "extend",
        "graphids": "387"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response: 响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "gitemid": "1242",
            "graphid": "387",
            "itemid": "22665",
            "drawtype": "1",
            "sortorder": "1",
            "color": "FF5555",
            "yaxisside": "0",
            "calc_fnc": "2",
            "type": "0",
            "key_": "system.cpu.util[,steal]",
            "hostid": "10001",
            "flags": "0",
            "host": "Template OS Linux"
        },
        {
            "gitemid": "1243",
            "graphid": "387",
            "itemid": "22668",
            "drawtype": "1",
            "sortorder": "2",
            "color": "55FF55",
            "yaxisside": "0",
            "calc_fnc": "2",
            "type": "0",
            "key_": "system.cpu.util[,softirq]",
            "hostid": "10001",
            "flags": "0",
            "host": "Template OS Linux"
        },
        {
            "gitemid": "1244",
            "graphid": "387",
            "itemid": "22671",
            "drawtype": "1",
            "sortorder": "3",
            "color": "009999",
            "yaxisside": "0",
            "calc_fnc": "2",
            "type": "0",
            "key_": "system.cpu.util[,interrupt]",
            "hostid": "10001",
            "flags": "0",
            "host": "Template OS Linux"
        }
    ],
    "id": 1
}
```

### See also

### 参考

-   [Graph](/manual/api/reference/graph/object#graph)
-   [图表](/zh/manual/api/reference/graph/object#graph)

### Source

### 来源

CGraphItem::get() in
*frontends/php/include/classes/api/services/CGraphItem.php*.

[comment]: # ({/new-8d13cada})
