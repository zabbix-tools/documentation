[comment]: # translation:outdated

[comment]: # ({new-168caa7a})
# > Graph item object

[comment]: # ({/new-168caa7a})

[comment]: # ({new-f6083c57})
## > 图表监控项对象

The following objects are directly related to the `graphitem` API.
以下对象与 `graphitem` API直接相关

### Graph item

### 图表监控项

::: noteclassic
Graph items can only be modified via the `graph`
API.
::: 

::: noteclassic
图表监控项只能通过 `graph`
API进行修改.
:::

The graph item object has the following properties.
图表监控项具有以下属性:

|Property|Type|Description|
|--------|----|-----------|
|gitemid|string|*(readonly)* ID of the graph item.|
|**color**<br>(required)|string|Graph item's draw color as a hexadecimal color code.|
|**itemid**<br>(required)|string|ID of the item.|
|calc\_fnc|integer|Value of the item that will be displayed.<br><br>Possible values:<br>1 - minimum value;<br>2 - *(default)* average value;<br>4 - maximum value;<br>7 - all values;<br>9 - last value, used only by pie and exploded graphs.|
|drawtype|integer|Draw style of the graph item.<br><br>Possible values:<br>0 - *(default)* line;<br>1 - filled region;<br>2 - bold line;<br>3 - dot;<br>4 - dashed line;<br>5 - gradient line.|
|graphid|string|ID of the graph that the graph item belongs to.|
|sortorder|integer|Position of the item in the graph.<br><br>Default: starts with 0 and increases by one with each entry.|
|type|integer|Type of graph item.<br><br>Possible values:<br>0 - *(default)* simple;<br>2 - graph sum, used only by pie and exploded graphs.|
|yaxisside|integer|Side of the graph where the graph item's Y scale will be drawn.<br><br>Possible values:<br>0 - *(default)* left side;<br>1 - right side.|

|属性          类|描述|<|
|-------------------|------|-|
|gitemid|string|*(必选)* 图表监控项的ID.|
|**color**<br>(必选)|string|绘制图形项目的颜色，使用十六进制码表示.|
|**itemid**<br>(必选)|string|监控项的ID.|
|calc\_fnc|integer|监控项显示的值.<br><br>可用值:<br>1 - 最小值;<br>2 - *(默认)* 平均值;<br>4 - 最大值;<br>7 - 所有值;<br>9 - 最新的值，仅适用于饼图以及分散饼图.|
|drawtype|integer|用于绘制图表监控的线形.<br><br>可用值:<br>0 - *(默认)* 实线;<br>1 - 面积图（填满的区域);<br>2 - 粗实线;<br>3 - 点;<br>4 - 虚线;<br>5 - 梯度线.|
|graphid|string|图表监控项所属的图表的ID.|
|sortorder|integer|图表中监控项的排序.<br><br>默认从0开始，每增加一个加1.|
|type|integer|图表监控项的类型.<br><br>可用值:<br>0 - *(默认)* 简单图形;<br>2 - 汇总图形, 仅用于饼图和分散饼图.|
|yaxisside|integer|Side of the graph where the graph item's Y scale will be drawn图表监控项的Y轴画在图表的那一侧:<br><br>可用值:<br>0 - *(默认)* 左侧;<br>1 - 右侧.|

[comment]: # ({/new-f6083c57})
