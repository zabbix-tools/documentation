[comment]: # translation:outdated

[comment]: # ({new-8eb68faf})
# 主机组

该类用于管理主机组.

对象引用:\

-   [Host group](/manual/api/reference/hostgroup/object#host_group)

可用的方法:\

-   [hostgroup.create](/manual/api/reference/hostgroup/create) -
    创建新的主机组
-   [hostgroup.delete](/manual/api/reference/hostgroup/delete) -
    删除主机组
-   [hostgroup.get](/manual/api/reference/hostgroup/get) - 获取主机组
-   [hostgroup.massadd](/manual/api/reference/hostgroup/massadd) -
    给主机组添加相关的对象
-   [hostgroup.massremove](/manual/api/reference/hostgroup/massremove) -
    删除主机组相关对象
-   [hostgroup.massupdate](/manual/api/reference/hostgroup/massupdate) -
    替换或删除主机组相关对象
-   [hostgroup.update](/manual/api/reference/hostgroup/update) -
    更新主机组

[comment]: # ({/new-8eb68faf})
