[comment]: # translation:outdated

[comment]: # ({new-37c3aad7})
# 获取

[comment]: # ({/new-37c3aad7})

[comment]: # ({new-1309d6bb})
### 描述

`integer/array dhost.get(object parameters)`

这个方法允许根据给定的参数检索`发现的主机`。

[comment]: # ({/new-1309d6bb})

[comment]: # ({new-061d7cf0})
### 参数

`(object)` 定义需要输出的参数。

这个方法支持以下参数。

|参数                     类|描述|<|
|------------------------------|------|-|
|dhostids|字符串/数组   只返回拥|给定 ID 的被发现主机。|
|druleids|字符串/数组   只返回由|定的发现规则创建的`已发现主机`。|
|dserviceids|字符串/数组   只返回运|指定服务的`已发现主机`。|
|selectDRules|查询          返|发现规则，该规则规定被发现主机在 `drules` 属性中以数组形式存在。|
|selectDServices|查询          返|已发现服务，该服务运行在 `dservices` 属性中的主机上。<br><br>支持 `count`。|
|limitSelects|整数          限|子选择返回的记录数量。<br><br>适用于下列子选择：<br>`selectDServices` - 结果将按 `dserviceid` 排序。|
|sortfield|字符串/数组   根据给定|属性对结果进行排序。<br><br>可能的值有：`dhostid` 和 `druleid`。|
|countOutput|布尔值        在[|用评论](/manual/api/reference_commentary#common_get_method_parameters)中详细描述了所有 `get` 方法的常见参数。|
|editable|布尔值        ::|<|
|excludeSearch|布尔值        ::|<|
|filter|对象          :|:|
|limit|整数          :|:|
|output|查询          :|:|
|preservekeys|布尔值        ::|<|
|search|对象          :|:|
|searchByAny|布尔值        ::|<|
|searchWildcardsEnabled|布尔值        ::|<|
|sortorder|字符串/数组   :::|<|
|startSearch|布尔值        ::|<|

[comment]: # ({/new-061d7cf0})

[comment]: # ({new-7223bab1})
### 返回值

`(integer/array)` 返回：

-   一个对象数组；
-   如果使用了 `countOutput` 参数，被检索的对象的数量。

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### 示例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-4b9c1e99})
#### 通过发现规则检索发现的主机

检索通过发现规则 "4" 发现的所有正在运行的主机和发现的服务。

请求：

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "dhost.get",
    "params": {
        "output": "extend",
        "selectDServices": "extend",
        "druleids": "4"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

响应：

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "dservices": [
                {
                    "dserviceid": "1",
                    "dhostid": "1",
                    "type": "4",
                    "key_": "",
                    "value": "",
                    "port": "80",
                    "status": "0",
                    "lastup": "1337697227",
                    "lastdown": "0",
                    "dcheckid": "5",
                    "ip": "192.168.1.1",
                    "dns": "station.company.lan"
                }
            ],
            "dhostid": "1",
            "druleid": "4",
            "status": "0",
            "lastup": "1337697227",
            "lastdown": "0"
        },
        {
            "dservices": [
                {
                    "dserviceid": "2",
                    "dhostid": "2",
                    "type": "4",
                    "key_": "",
                    "value": "",
                    "port": "80",
                    "status": "0",
                    "lastup": "1337697234",
                    "lastdown": "0",
                    "dcheckid": "5",
                    "ip": "192.168.1.4",
                    "dns": "john.company.lan"
                }
            ],
            "dhostid": "2",
            "druleid": "4",
            "status": "0",
            "lastup": "1337697234",
            "lastdown": "0"
        },
        {
            "dservices": [
                {
                    "dserviceid": "3",
                    "dhostid": "3",
                    "type": "4",
                    "key_": "",
                    "value": "",
                    "port": "80",
                    "status": "0",
                    "lastup": "1337697234",
                    "lastdown": "0",
                    "dcheckid": "5",
                    "ip": "192.168.1.26",
                    "dns": "printer.company.lan"
                }
            ],
            "dhostid": "3",
            "druleid": "4",
            "status": "0",
            "lastup": "1337697234",
            "lastdown": "0"
        },
        {
            "dservices": [
                {
                    "dserviceid": "4",
                    "dhostid": "4",
                    "type": "4",
                    "key_": "",
                    "value": "",
                    "port": "80",
                    "status": "0",
                    "lastup": "1337697234",
                    "lastdown": "0",
                    "dcheckid": "5",
                    "ip": "192.168.1.7",
                    "dns": "mail.company.lan"
                }
            ],
            "dhostid": "4",
            "druleid": "4",
            "status": "0",
            "lastup": "1337697234",
            "lastdown": "0"
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-4b9c1e99})

[comment]: # ({new-e6b09247})
### 参见

-   [发现服务](/manual/api/reference/dservice/object#discovered_service)
-   [发现规则](/manual/api/reference/drule/object#discovery_rule)

[comment]: # ({/new-e6b09247})

[comment]: # ({new-8dc0c01a})
### 来源

CDHost::get() in
*frontends/php/include/classes/api/services/CDHost.php*.

[comment]: # ({/new-8dc0c01a})
