[comment]: # translation:outdated

[comment]: # ({new-854803de})
# > 对象

下列对象与 `dhost` API 直接相关。

[comment]: # ({/new-854803de})

[comment]: # ({new-236628fe})
### 发现主机

::: noteclassic
发现的主机是由 Zabbix 服务器创建的，不能通过 API
进行修改。
:::

发现的主机对象包含一个被网络发现规则发现的主机的信息。其具有以下属性。

|属性       类|描述|<|
|----------------|------|-|
|dhostid|字符串   发现|主机的 ID。|
|druleid|字符串   用于|测主机的发现规则的 ID。|
|lastdown|时间戳   发现|主机最后异常的时间。|
|lastup|时间戳   发现|主机最后正常的时间。|
|status|整数     发|的主机是正常还是异常。如果一个主机至少还有一个活动的发现服务，那么它就是正常的。<br><br>可能的值：<br>0 - 主机正常；<br>1 - 主机异常。|

[comment]: # ({/new-236628fe})
