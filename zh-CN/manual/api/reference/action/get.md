[comment]: # translation:outdated

[comment]: # ({new-2293b018})
# action.get

[comment]: # ({/new-2293b018})

[comment]: # ({new-42834e1b})
### 说明

`integer/array action.get(object parameters)`

该方法允许根据给定的参数检索动作。

[comment]: # ({/new-42834e1b})

[comment]: # ({new-f6c19c90})
### 参数

`(object)` 定义期望输出的参数。

该方法支持以下参数。

|参数                          类|说明|<|
|-----------------------------------|------|-|
|actionids|string/array|只返回给定 ID 的动作。|
|groupids|string/array|只返回在操作条件下使用给定主机组的动作。|
|hostids|string/array|只返回在操作条件下使用给定主机的动作。|
|triggerids|string/array|只返回在操作条件下使用给定触发器的动作。|
|mediatypeids|string/array|只返回使用给定媒体类型发送消息的动作。|
|usrgrpids|string/array|仅返回配置为向给定用户组发送消息的动作。|
|userids|string/array|仅返回配置为向给定用户发送消息的动作。|
|scriptids|string/array|只返回配置为运行给定脚本的动作。|
|selectFilter|query|返回 `filter` 属性中的动作筛选器。|
|selectOperations|query|在 `操作` 属性中返回操作操作。|
|selectRecoveryOperations|query|在 `恢复操作` 属性中返回动作恢复操作。|
|selectAcknowledgeOperations|query|在 `确认操作` 属性中返回动作确认操作。|
|sortfield|string/array|根据给定的属性排序结果。<br><br>可能的值是: `actionid`, `name` and `status`.|
|countOutput|boolean|这些参数对于所有 `get` 方法都是常见的。在 [reference commentary](/manual/api/reference_commentary#common_get_method_parameters).|
|editable|boolean|^|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

[comment]: # ({/new-f6c19c90})

[comment]: # ({new-7223bab1})
### 返回值

`(integer/array)` 也返回:

-   对象数组;
-   如果使用了 `curtOutlook` 参数，则检索对象的计数。

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### 范例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-f7d2e806})
#### 检索发现动作

Retrieve all configured discovery actions together with action
conditions and operations. The filter uses the "and" evaluation type, so
the `formula` property is empty and `eval_formula` is generated
automatically. 检索所有配置的发现动作以及操作条件和操作。筛选器使用
`and` 评估类型，因此 `formula` 属性为空，自动生成 `eval_formula` 。

请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "action.get",
    "params": {
        "output": "extend",
        "selectOperations": "extend",
        "selectRecoveryOperations": "extend",
        "selectFilter": "extend",
        "filter": {
            "eventsource": 1
        }
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "actionid": "2",
            "name": "Auto discovery. Linux servers.",
            "eventsource": "1",
            "status": "1",
            "esc_period": "0s",
            "def_shortdata": "",
            "def_longdata": "",
            "r_shortdata": "",
            "r_longdata": "",
            "pause_suppressed": "1",
            "filter": {
                "evaltype": "0",
                "formula": "",
                "conditions": [
                    {
                        "conditiontype": "10",
                        "operator": "0",
                        "value": "0",
                        "value2": "",
                        "formulaid": "B"
                    },
                    {
                        "conditiontype": "8",
                        "operator": "0",
                        "value": "9",
                        "value2": "",
                        "formulaid": "C"
                    },
                    {
                        "conditiontype": "12",
                        "operator": "2",
                        "value": "Linux",
                        "value2": "",
                        "formulaid": "A"
                    }
                ],
                "eval_formula": "A and B and C"
            },
            "operations": [
                {
                    "operationid": "1",
                    "actionid": "2",
                    "operationtype": "6",
                    "esc_period": "0s",
                    "esc_step_from": "1",
                    "esc_step_to": "1",
                    "evaltype": "0",
                    "opconditions": [],
                    "optemplate": [
                        {
                            "operationid": "1",
                            "templateid": "10001"
                        }
                    ]
                },
                {
                    "operationid": "2",
                    "actionid": "2",
                    "operationtype": "4",
                    "esc_period": "0s",
                    "esc_step_from": "1",
                    "esc_step_to": "1",
                    "evaltype": "0",
                    "opconditions": [],
                    "opgroup": [
                        {
                            "operationid": "2",
                            "groupid": "2"
                        }
                    ]
                }
            ],
            "recoveryOperations": [
                {
                    "operationid": "585",
                    "actionid": "2",
                    "operationtype": "11",
                    "evaltype": "0",
                    "opconditions": [],
                    "opmessage": {
                        "operationid": "585",
                        "default_msg": "1",
                        "subject": "{TRIGGER.STATUS}: {TRIGGER.NAME}",
                        "message": "Trigger: {TRIGGER.NAME}\r\nTrigger status: {TRIGGER.STATUS}\r\nTrigger severity: {TRIGGER.SEVERITY}\r\nTrigger URL: {TRIGGER.URL}\r\n\r\nItem values:\r\n\r\n1. {ITEM.NAME1} ({HOST.NAME1}:{ITEM.KEY1}): {ITEM.VALUE1}\r\n2. {ITEM.NAME2} ({HOST.NAME2}:{ITEM.KEY2}): {ITEM.VALUE2}\r\n3. {ITEM.NAME3} ({HOST.NAME3}:{ITEM.KEY3}): {ITEM.VALUE3}\r\n\r\nOriginal event ID: {EVENT.ID}",
                        "mediatypeid": "0"
                    }
                }
            ],
            "acknowledgeOperations": [
                {
                    "operationid": "585",
                    "operationtype": "12",
                    "evaltype": "0",
                    "opmessage": {
                        "default_msg": "1",
                        "subject": "Acknowledged: {TRIGGER.NAME}",
                        "message": "{USER.FULLNAME} acknowledged problem at {ACK.DATE} {ACK.TIME} with the following message:\r\n{ACK.MESSAGE}\r\n\r\nCurrent problem status is {EVENT.STATUS}",
                        "mediatypeid": "0"
                    }
                },
                {
                    "operationid": "586",
                    "operationtype": "0",
                    "evaltype": "0",
                    "opmessage": {
                        "default_msg": "1",
                        "subject": "Acknowledged: {TRIGGER.NAME}",
                        "message": "{USER.FULLNAME} acknowledged problem at {ACK.DATE} {ACK.TIME} with the following message:\r\n{ACK.MESSAGE}\r\n\r\nCurrent problem status is {EVENT.STATUS}",
                        "mediatypeid": "0"
                    },
                    "opmessage_grp": [
                        {
                            "usrgrpid": "7"
                        }
                    ],
                    "opmessage_usr": []
                },
                {
                    "operationid": "587",
                    "operationtype": "1",
                    "evaltype": "0",
                    "opcommand": {
                        "type": "0",
                        "scriptid": "0",
                        "execute_on": "0",
                        "port": "",
                        "authtype": "0",
                        "username": "",
                        "password": "",
                        "publickey": "",
                        "privatekey": "",
                        "command": "notify.sh"
                    },
                    "opcommand_hst": [
                        {
                            "hostid": "0"
                        }
                    ],
                    "opcommand_grp": []
                }
            ]
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-f7d2e806})

[comment]: # ({new-d96da9cf})
### 参见

-   [Action filter](object#action_filter)
-   [Action operation](object#action_operation)

[comment]: # ({/new-d96da9cf})

[comment]: # ({new-755496f7})
### 来源

CAction::get() in
*frontends/php/include/classes/api/services/CAction.php*.

[comment]: # ({/new-755496f7})

[comment]: # ({new-025b824e})
### Description

`integer/array action.get(object parameters)`

The method allows to retrieve actions according to the given parameters.

### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|Type|Description|
|---------|----|-----------|
|actionids|string/array|Return only actions with the given IDs.|
|groupids|string/array|Return only actions that use the given host groups in action conditions.|
|hostids|string/array|Return only actions that use the given hosts in action conditions.|
|triggerids|string/array|Return only actions that use the given triggers in action conditions.|
|mediatypeids|string/array|Return only actions that use the given media types to send messages.|
|usrgrpids|string/array|Return only actions that are configured to send messages to the given user groups.|
|userids|string/array|Return only actions that are configured to send messages to the given users.|
|scriptids|string/array|Return only actions that are configured to run the given scripts.|
|selectFilter|query|Returns the action filter in the `filter` property.|
|selectOperations|query|Return action operations in the `operations` property.|
|selectRecoveryOperations|query|Return action recovery operations in the `recoveryOperations` property.|
|selectAcknowledgeOperations|query|Return action acknowledge operations in the `acknowledgeOperations` property.|
|sortfield|string/array|Sort the result by the given properties.<br><br>Possible values are: `actionid`, `name` and `status`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters).|
|editable|boolean|^|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.

### Examples

#### Retrieve discovery actions

Retrieve all configured discovery actions together with action
conditions and operations. The filter uses the "and" evaluation type, so
the `formula` property is empty and `eval_formula` is generated
automatically.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "action.get",
    "params": {
        "output": "extend",
        "selectOperations": "extend",
        "selectRecoveryOperations": "extend",
        "selectFilter": "extend",
        "filter": {
            "eventsource": 1
        }
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "actionid": "2",
            "name": "Auto discovery. Linux servers.",
            "eventsource": "1",
            "status": "1",
            "esc_period": "0s",
            "def_shortdata": "",
            "def_longdata": "",
            "r_shortdata": "",
            "r_longdata": "",
            "pause_suppressed": "1",
            "filter": {
                "evaltype": "0",
                "formula": "",
                "conditions": [
                    {
                        "conditiontype": "10",
                        "operator": "0",
                        "value": "0",
                        "value2": "",
                        "formulaid": "B"
                    },
                    {
                        "conditiontype": "8",
                        "operator": "0",
                        "value": "9",
                        "value2": "",
                        "formulaid": "C"
                    },
                    {
                        "conditiontype": "12",
                        "operator": "2",
                        "value": "Linux",
                        "value2": "",
                        "formulaid": "A"
                    }
                ],
                "eval_formula": "A and B and C"
            },
            "operations": [
                {
                    "operationid": "1",
                    "actionid": "2",
                    "operationtype": "6",
                    "esc_period": "0s",
                    "esc_step_from": "1",
                    "esc_step_to": "1",
                    "evaltype": "0",
                    "opconditions": [],
                    "optemplate": [
                        {
                            "operationid": "1",
                            "templateid": "10001"
                        }
                    ]
                },
                {
                    "operationid": "2",
                    "actionid": "2",
                    "operationtype": "4",
                    "esc_period": "0s",
                    "esc_step_from": "1",
                    "esc_step_to": "1",
                    "evaltype": "0",
                    "opconditions": [],
                    "opgroup": [
                        {
                            "operationid": "2",
                            "groupid": "2"
                        }
                    ]
                }
            ],
            "recoveryOperations": [
                {
                    "operationid": "585",
                    "actionid": "2",
                    "operationtype": "11",
                    "evaltype": "0",
                    "opconditions": [],
                    "opmessage": {
                        "operationid": "585",
                        "default_msg": "1",
                        "subject": "{TRIGGER.STATUS}: {TRIGGER.NAME}",
                        "message": "Trigger: {TRIGGER.NAME}\r\nTrigger status: {TRIGGER.STATUS}\r\nTrigger severity: {TRIGGER.SEVERITY}\r\nTrigger URL: {TRIGGER.URL}\r\n\r\nItem values:\r\n\r\n1. {ITEM.NAME1} ({HOST.NAME1}:{ITEM.KEY1}): {ITEM.VALUE1}\r\n2. {ITEM.NAME2} ({HOST.NAME2}:{ITEM.KEY2}): {ITEM.VALUE2}\r\n3. {ITEM.NAME3} ({HOST.NAME3}:{ITEM.KEY3}): {ITEM.VALUE3}\r\n\r\nOriginal event ID: {EVENT.ID}",
                        "mediatypeid": "0"
                    }
                }
            ],
            "acknowledgeOperations": [
                {
                    "operationid": "585",
                    "operationtype": "12",
                    "evaltype": "0",
                    "opmessage": {
                        "default_msg": "1",
                        "subject": "Acknowledged: {TRIGGER.NAME}",
                        "message": "{USER.FULLNAME} acknowledged problem at {ACK.DATE} {ACK.TIME} with the following message:\r\n{ACK.MESSAGE}\r\n\r\nCurrent problem status is {EVENT.STATUS}",
                        "mediatypeid": "0"
                    }
                },
                {
                    "operationid": "586",
                    "operationtype": "0",
                    "evaltype": "0",
                    "opmessage": {
                        "default_msg": "1",
                        "subject": "Acknowledged: {TRIGGER.NAME}",
                        "message": "{USER.FULLNAME} acknowledged problem at {ACK.DATE} {ACK.TIME} with the following message:\r\n{ACK.MESSAGE}\r\n\r\nCurrent problem status is {EVENT.STATUS}",
                        "mediatypeid": "0"
                    },
                    "opmessage_grp": [
                        {
                            "usrgrpid": "7"
                        }
                    ],
                    "opmessage_usr": []
                },
                {
                    "operationid": "587",
                    "operationtype": "1",
                    "evaltype": "0",
                    "opcommand": {
                        "type": "0",
                        "scriptid": "0",
                        "execute_on": "0",
                        "port": "",
                        "authtype": "0",
                        "username": "",
                        "password": "",
                        "publickey": "",
                        "privatekey": "",
                        "command": "notify.sh"
                    },
                    "opcommand_hst": [
                        {
                            "hostid": "0"
                        }
                    ],
                    "opcommand_grp": []
                }
            ]
        }
    ],
    "id": 1
}
```

### See also

-   [Action filter](object#action_filter)
-   [Action operation](object#action_operation)

### Source

CAction::get() in
*frontends/php/include/classes/api/services/CAction.php*.

[comment]: # ({/new-025b824e})
