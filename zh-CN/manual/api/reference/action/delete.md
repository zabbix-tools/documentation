[comment]: # translation:outdated

[comment]: # ({new-3d0c523c})
# action.delete

[comment]: # ({/new-3d0c523c})

[comment]: # ({new-c056f978})
### 说明

`object action.delete(array actionIds)`

此方法用于删除动作。

[comment]: # ({/new-c056f978})

[comment]: # ({new-f66aff82})
### 参数

`(array)` 要删除的动作的 ID。

[comment]: # ({/new-f66aff82})

[comment]: # ({new-04c6deae})
### 返回值

`(object)` 返回一个对象，该对象在 `actionids` 属性下包含要删除的动作的
ID。

[comment]: # ({/new-04c6deae})

[comment]: # ({new-b41637d2})
### 范例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-36abb590})
#### 删除多个动作

删除两个动作。

请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "action.delete",
    "params": [
        "17",
        "18"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "actionids": [
            "17",
            "18"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-36abb590})

[comment]: # ({new-70602a31})
### 来源

CAction::delete() in
*frontends/php/include/classes/api/services/CAction.php*.

### Description

`object action.delete(array actionIds)`

This method allows to delete actions.

### Parameters

`(array)` IDs of the actions to delete.

### Return values

`(object)` Returns an object containing the IDs of the deleted actions
under the `actionids` property.

### Examples

#### Delete multiple actions

Delete two actions.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "action.delete",
    "params": [
        "17",
        "18"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "actionids": [
            "17",
            "18"
        ]
    },
    "id": 1
}
```

### Source

CAction::delete() in
*frontends/php/include/classes/api/services/CAction.php*.

[comment]: # ({/new-70602a31})
