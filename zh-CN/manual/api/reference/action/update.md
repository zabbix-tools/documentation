[comment]: # translation:outdated

[comment]: # ({new-e4b5aac4})
# action.update

[comment]: # ({/new-e4b5aac4})

[comment]: # ({new-b1673b49})
### 说明

`object action.update(object/array actions)`

此方法允许更新现有的动作。

[comment]: # ({/new-b1673b49})

[comment]: # ({new-703e85ce})
### 参数

`(object/array)` 要更新的动作属性。

必须为每个动作定义 `actionid`
属性，所有其他属性都是可选的。只有通过的属性将被更新，所有其他属性将保持不变。

除此之外 [standard action properties](object#action),
该方法接受以下参数。

|参数                      类|说明|<|
|-------------------------------|------|-|
|filter|object|动作筛选器对象以替换当前筛选器。|
|operations|array|动作操作替换现有操作。|
|recovery\_operations|array|动作恢复操作以替换现有恢复操作。|
|acknowledge\_operations|array|动作确认操作以替换现有的确认操作。|

[comment]: # ({/new-703e85ce})

[comment]: # ({new-8cbaa103})
### 返回值

`(object)` 返回一个对象，该对象在 `actionids` 属性下包含要更新动作的
ID。

[comment]: # ({/new-8cbaa103})

[comment]: # ({new-b41637d2})
### 范例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-77b78ec5})
#### 禁用动作

禁用动作，也就是说，将其状态设置为 `1`。

请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "action.update",
    "params": {
        "actionid": "2",
        "status": "1"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "actionids": [
            "2"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-77b78ec5})

[comment]: # ({new-755496f7})
### 参见

-   [Action filter](object#action_filter)
-   [Action operation](object#action_operation)

[comment]: # ({/new-755496f7})

[comment]: # ({new-df9f1d35})
### 来源

CAction::update() in
*frontends/php/include/classes/api/services/CAction.php*.

### Description

`object action.update(object/array actions)`

This method allows to update existing actions.

### Parameters

`(object/array)` Action properties to be updated.

The `actionid` property must be defined for each action, all other
properties are optional. Only the passed properties will be updated, all
others will remain unchanged.

Additionally to the [standard action properties](object#action), the
method accepts the following parameters.

|Parameter|Type|Description|
|---------|----|-----------|
|filter|object|Action filter object to replace the current filter.|
|operations|array|Action operations to replace existing operations.|
|recovery\_operations|array|Action recovery operations to replace existing recovery operations.|
|acknowledge\_operations|array|Action acknowledge operations to replace existing acknowledge operations.|

### Return values

`(object)` Returns an object containing the IDs of the updated actions
under the `actionids` property.

### Examples

#### Disable action

Disable action, that is, set its status to "1".

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "action.update",
    "params": {
        "actionid": "2",
        "status": "1"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "actionids": [
            "2"
        ]
    },
    "id": 1
}
```

### See also

-   [Action filter](object#action_filter)
-   [Action operation](object#action_operation)

### Source

CAction::update() in
*frontends/php/include/classes/api/services/CAction.php*.

[comment]: # ({/new-df9f1d35})
