[comment]: # translation:outdated

[comment]: # ({new-c7f202d4})
# action.create

[comment]: # ({/new-c7f202d4})

[comment]: # ({new-71c93aa0})
### 说明

`object action.create(object/array actions)`

此方法用于创建新动作。

[comment]: # ({/new-71c93aa0})

[comment]: # ({new-5a9be3c9})
### 参数

`(object/array)` 创建新动作

除此之外 [标准动作属性](object#action), 该方法接受以下参数。

|参数                      类|说明|<|
|-------------------------------|------|-|
|filter|object|动作的动作过滤器对象。|
|operations|array|为动作创建的动作操作。|
|recovery\_operations|array|为动作创建动作恢复操作。|
|acknowledge\_operations|array|为动作创建动作确认操作。|

[comment]: # ({/new-5a9be3c9})

[comment]: # ({new-2830affd})
### 返回值

`(object)` 返回一个对象，其中 `actionids` 属性下包含已创建动作的 ID。
返回的 ID 的顺序与传递的操作的顺序相匹配。

[comment]: # ({/new-2830affd})

[comment]: # ({new-b41637d2})
### 范例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-afc96865})
#### 创建触发器动作

创建一个动作，动作如下描述，当主机 `30045` ，它的触发器中的 `memory`
进入问题状态时。该动作必须首先向用户组 `7`
中的所有用户发送消息。如果事件在 4 分钟内未被解决，它将在 `2`
组中的所有主机上运行脚本
`3`。在触发恢复中，它将通知所有接收到关于该问题的消息的用户。在触发器确认中，带有自定义主体和主体的消息将通过所有媒体类型发送给所有确认和评论的所有人。

请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "action.create",
    "params": {
        "name": "Trigger action",
        "eventsource": 0,
        "status": 0,
        "esc_period": "2m",
        "def_shortdata": "{TRIGGER.NAME}: {TRIGGER.STATUS}",
        "def_longdata": "{TRIGGER.NAME}: {TRIGGER.STATUS}\r\nLast value: {ITEM.LASTVALUE}\r\n\r\n{TRIGGER.URL}",
        "filter": {
            "evaltype": 0,
            "conditions": [
                {
                    "conditiontype": 1,
                    "operator": 0,
                    "value": "10084"
                },
                {
                    "conditiontype": 3,
                    "operator": 2,
                    "value": "memory"
                }
            ]
        },
        "operations": [
            {
                "operationtype": 0,
                "esc_period": "0s",
                "esc_step_from": 1,
                "esc_step_to": 2,
                "evaltype": 0,
                "opmessage_grp": [
                    {
                        "usrgrpid": "7"
                    }
                ],
                "opmessage": {
                    "default_msg": 1,
                    "mediatypeid": "1"
                }
            },
            {
                "operationtype": 1,
                "esc_step_from": 3,
                "esc_step_to": 4,
                "evaltype": 0,
                "opconditions": [
                    {
                        "conditiontype": 14,
                        "operator": 0,
                        "value": "0"
                    }
                ],
                "opcommand_grp": [
                    {
                        "groupid": "2"
                    }
                ],
                "opcommand": {
                    "type": 4,
                    "scriptid": "3"
                }
            }
        ],
        "recovery_operations": [
            {
                "operationtype": "11",
                "opmessage": {
                    "default_msg": 1
                }
            }    
        ],
        "acknowledge_operations": [
            {
                "operationtype": "12",
                "opmessage": {
                    "message": "Custom acknowledge operation message body",
                    "subject": "Custom acknowledge operation message subject"
                }
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "actionids": [
            "17"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-afc96865})

[comment]: # ({new-53bdbb4b})
#### 创建发现动作

创建一个将发现的主机链接到模板 `30085` 的动作。

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "action.create",
    "params": {
        "name": "Discovery action",
        "eventsource": 1,
        "status": 0,
        "esc_period": "0s",
        "filter": {
            "evaltype": 0,
            "conditions": [
                {
                    "conditiontype": 21,
                    "value": "1"
                },
                {
                    "conditiontype": 10,
                    "value": "2"
                }
            ]
        },
        "operations": [
            {
                "esc_step_from": 1,
                "esc_period": "0s",
                "optemplate": [
                    {
                        "templateid": "10091"
                    }
                ],
                "operationtype": 6,
                "esc_step_to": 1
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "actionids": [
            "18"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-53bdbb4b})

[comment]: # ({new-de84bbc4})
#### 使用自定义表达式筛选器

创建使用自定义筛选器条件的触发器动作。该动作必须为每个触发器发送一个消息，其严重程度高于或等于主机
`10084` 和 `10106` 的 `警告`。公式 ID `A`、`B`和 `C` 都是任意选择的。

请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "action.create",
    "params": {
        "name": "Trigger action",
        "eventsource": 0,
        "status": 0,
        "esc_period": "2m",
        "def_shortdata": "{TRIGGER.NAME}: {TRIGGER.STATUS}",
        "def_longdata": "{TRIGGER.NAME}: {TRIGGER.STATUS}\r\nLast value: {ITEM.LASTVALUE}\r\n\r\n{TRIGGER.URL}",
        "filter": {
            "evaltype": 3,
            "formula": "A and (B or C)",
            "conditions": [
                {
                    "conditiontype": 4,
                    "operator": 5,
                    "value": "2",
                    "formulaid": "A"
                },
                {
                    "conditiontype": 1,
                    "operator": 0,
                    "value": "10084",
                    "formulaid": "B"
                },
                {
                    "conditiontype": 1,
                    "operator": 0,
                    "value": "10106",
                    "formulaid": "C"
                }
            ]
        },
        "operations": [
            {
                "operationtype": 0,
                "esc_period": "0s",
                "esc_step_from": 1,
                "esc_step_to": 2,
                "evaltype": 0,
                "opmessage_grp": [
                    {
                        "usrgrpid": "7"
                    }
                ],
                "opmessage": {
                    "default_msg": 1,
                    "mediatypeid": "1"
                }
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "actionids": [
            "18"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-de84bbc4})

[comment]: # ({new-b81a306b})
### 参见

-   [Action filter](object#action_filter)
-   [Action operation](object#action_operation)

[comment]: # ({/new-b81a306b})

[comment]: # ({new-755496f7})
### 来源

CAction::create() in
*frontends/php/include/classes/api/services/CAction.php*.

[comment]: # ({/new-755496f7})

[comment]: # ({new-32335876})
### Description

`object action.create(object/array actions)`

This method allows to create new actions.

### Parameters

`(object/array)` Actions to create.

Additionally to the [standard action properties](object#action), the
method accepts the following parameters.

|Parameter|Type|Description|
|---------|----|-----------|
|filter|object|Action filter object for the action.|
|operations|array|Action operations to create for the action.|
|recovery\_operations|array|Action recovery operations to create for the action.|
|acknowledge\_operations|array|Action acknowledge operations to create for the action.|

### Return values

`(object)` Returns an object containing the IDs of the created actions
under the `actionids` property. The order of the returned IDs matches
the order of the passed actions.

### Examples

#### Create a trigger action

Create an action that will be run when a trigger from host "30045" that
has the word "memory" in its name goes into problem state. The action
must first send a message to all users in user group "7". If the event
is not resolved in 4 minutes, it will run script "3" on all hosts in
group "2". On trigger recovery it will notify all users who received any
messages regarding the problem before. On trigger acknowledge, message
with custom subject and body will be sent to all who left
acknowledgements and comments via all media types.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "action.create",
    "params": {
        "name": "Trigger action",
        "eventsource": 0,
        "status": 0,
        "esc_period": "2m",
        "def_shortdata": "{TRIGGER.NAME}: {TRIGGER.STATUS}",
        "def_longdata": "{TRIGGER.NAME}: {TRIGGER.STATUS}\r\nLast value: {ITEM.LASTVALUE}\r\n\r\n{TRIGGER.URL}",
        "filter": {
            "evaltype": 0,
            "conditions": [
                {
                    "conditiontype": 1,
                    "operator": 0,
                    "value": "10084"
                },
                {
                    "conditiontype": 3,
                    "operator": 2,
                    "value": "memory"
                }
            ]
        },
        "operations": [
            {
                "operationtype": 0,
                "esc_period": "0s",
                "esc_step_from": 1,
                "esc_step_to": 2,
                "evaltype": 0,
                "opmessage_grp": [
                    {
                        "usrgrpid": "7"
                    }
                ],
                "opmessage": {
                    "default_msg": 1,
                    "mediatypeid": "1"
                }
            },
            {
                "operationtype": 1,
                "esc_step_from": 3,
                "esc_step_to": 4,
                "evaltype": 0,
                "opconditions": [
                    {
                        "conditiontype": 14,
                        "operator": 0,
                        "value": "0"
                    }
                ],
                "opcommand_grp": [
                    {
                        "groupid": "2"
                    }
                ],
                "opcommand": {
                    "type": 4,
                    "scriptid": "3"
                }
            }
        ],
        "recovery_operations": [
            {
                "operationtype": "11",
                "opmessage": {
                    "default_msg": 1
                }
            }    
        ],
        "acknowledge_operations": [
            {
                "operationtype": "12",
                "opmessage": {
                    "message": "Custom acknowledge operation message body",
                    "subject": "Custom acknowledge operation message subject"
                }
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "actionids": [
            "17"
        ]
    },
    "id": 1
}
```

#### Create a discovery action

Create an action that will link discovered hosts to template "30085".

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "action.create",
    "params": {
        "name": "Discovery action",
        "eventsource": 1,
        "status": 0,
        "esc_period": "0s",
        "filter": {
            "evaltype": 0,
            "conditions": [
                {
                    "conditiontype": 21,
                    "value": "1"
                },
                {
                    "conditiontype": 10,
                    "value": "2"
                }
            ]
        },
        "operations": [
            {
                "esc_step_from": 1,
                "esc_period": "0s",
                "optemplate": [
                    {
                        "templateid": "10091"
                    }
                ],
                "operationtype": 6,
                "esc_step_to": 1
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "actionids": [
            "18"
        ]
    },
    "id": 1
}
```

#### Using a custom expression filter

Create a trigger action that will use a custom filter condition. The
action must send a message for each trigger with severity higher or
equal to "Warning" for hosts "10084" and "10106". The formula IDs "A",
"B" and "C" have been chosen arbitrarily.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "action.create",
    "params": {
        "name": "Trigger action",
        "eventsource": 0,
        "status": 0,
        "esc_period": "2m",
        "def_shortdata": "{TRIGGER.NAME}: {TRIGGER.STATUS}",
        "def_longdata": "{TRIGGER.NAME}: {TRIGGER.STATUS}\r\nLast value: {ITEM.LASTVALUE}\r\n\r\n{TRIGGER.URL}",
        "filter": {
            "evaltype": 3,
            "formula": "A and (B or C)",
            "conditions": [
                {
                    "conditiontype": 4,
                    "operator": 5,
                    "value": "2",
                    "formulaid": "A"
                },
                {
                    "conditiontype": 1,
                    "operator": 0,
                    "value": "10084",
                    "formulaid": "B"
                },
                {
                    "conditiontype": 1,
                    "operator": 0,
                    "value": "10106",
                    "formulaid": "C"
                }
            ]
        },
        "operations": [
            {
                "operationtype": 0,
                "esc_period": "0s",
                "esc_step_from": 1,
                "esc_step_to": 2,
                "evaltype": 0,
                "opmessage_grp": [
                    {
                        "usrgrpid": "7"
                    }
                ],
                "opmessage": {
                    "default_msg": 1,
                    "mediatypeid": "1"
                }
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "actionids": [
            "18"
        ]
    },
    "id": 1
}
```

### See also

-   [Action filter](object#action_filter)
-   [Action operation](object#action_operation)

### Source

CAction::create() in
*frontends/php/include/classes/api/services/CAction.php*.

[comment]: # ({/new-32335876})
