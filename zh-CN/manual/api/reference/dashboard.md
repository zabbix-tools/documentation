[comment]: # translation:outdated

[comment]: # ({new-03e77d6d})
# 仪表板

这个类被设计用于`仪表板`。

对象引用：\

-   [仪表板](/manual/api/reference/dashboard/object#dashboard)
-   [仪表板小组件](/manual/api/reference/dashboard/object#dashboard_widget)
-   [仪表板小组件字段](/manual/api/reference/dashboard/object#dashboard_widget_field)
-   [仪表板用户组](/manual/api/reference/dashboard/object#dashboard_user_group)
-   [仪表板用户](/manual/api/reference/dashboard/object#dashboard_user)

可用的方法：\

-   [dashboard.create](/manual/api/reference/dashboard/create) -
    创建新的仪表板
-   [dashboard.delete](/manual/api/reference/dashboard/delete) -
    删除仪表板
-   [dashboard.get](/manual/api/reference/dashboard/get) - 检索仪表板
-   [dashboard.update](/manual/api/reference/dashboard/update) -
    更新仪表板

[comment]: # ({/new-03e77d6d})
