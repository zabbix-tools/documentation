[comment]: # translation:outdated

[comment]: # ({new-1b9515ab})
# itemprototype.delete

[comment]: # ({/new-1b9515ab})

[comment]: # ({new-ee2fdfa0})
### Description

[comment]: # ({/new-ee2fdfa0})

[comment]: # ({new-7755a7b3})
### 说明

`object itemprototype.delete(array itemPrototypeIds)`

This method allows to delete item prototypes. 此方法允许删除item
prototypes。

[comment]: # ({/new-7755a7b3})

[comment]: # ({new-db63ca62})
### Parameters

[comment]: # ({/new-db63ca62})

[comment]: # ({new-b41637d2})
### 参数

`(array)` IDs of the item prototypes to delete. `(array)` 要删除的item
prototypes IDs.

[comment]: # ({/new-b41637d2})

[comment]: # ({new-0c135019})
### Return values

[comment]: # ({/new-0c135019})

[comment]: # ({new-8cee4f1f})
### 返回值

`(object)` Returns an object containing the IDs of the deleted item
prototypes under the `prototypeids` property. `(object)` `prototypeids`
属性下在返回一个带有被删除的item prototypes的IDs.

### Examples

### 示例

#### Deleting multiple item prototypes

#### 删除多个 item prototypes

Delete two item prototypes.\
Dependent item prototypes are removed automatically if master item or
item prototype is deleted. 删除2个item prototypes。如果主item或者item
prototype 被删除，，依赖其的item prototype 也会被删除。

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "itemprototype.delete",
    "params": [
        "27352",
        "27356"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "prototypeids": [
            "27352",
            "27356"
        ]
    },
    "id": 1
}
```

### Source

### 源

CItemPrototype::delete() in
*frontends/php/include/classes/api/services/CItemPrototype.php*.

[comment]: # ({/new-8cee4f1f})
