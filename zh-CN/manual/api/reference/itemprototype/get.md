[comment]: # translation:outdated

[comment]: # ({new-8246351f})
# itemprototype.get

[comment]: # ({/new-8246351f})

[comment]: # ({new-5f97ef4a})
### Description

[comment]: # ({/new-5f97ef4a})

[comment]: # ({new-8d6ecc79})
### 说明

`integer/array itemprototype.get(object parameters)`

The method allows to retrieve item prototypes according to the given
parameters. 此方法可以根据提供的参数获取item prototypes。

[comment]: # ({/new-8d6ecc79})

[comment]: # ({new-7223bab1})
### Parameters

`(object)` Parameters defining the desired output. `(object)`
参数定义期望输出

The method supports the following parameters. 此方法提供以下参数。

|Parameter|Type|Description|
|---------|----|-----------|
|discoveryids|string/array|Return only item prototypes that belong to the given LLD rules. 只返回属于给定LLD规则的item prototypes。|
|graphids|string/array|Return only item prototypes that are used in the given graph prototypes.只返回在给定图标原型中使用的item prototypes。|
|hostids|string/array|Return only item prototypes that belong to the given hosts. 只返回属于给定host的item prototypes。|
|inherited|boolean|If set to `true` return only item prototypes inherited from a template.如果设为"true"，返回继承自某个template的 item prototypes。|
|itemids|string/array|Return only item prototypes with the given IDs. 返回给定IDS的item prototypes。|
|monitored|boolean|If set to `true` return only enabled item prototypes that belong to monitored hosts.如果设为"true"，只返回已启动的属于已监控主机的item prototypes。|
|templated|boolean|If set to `true` return only item prototypes that belong to templates.如果设为"true"，只发挥属于给定模板的item prototypes。|
|templateids|string/array|Return only item prototypes that belong to the given templates. 只返回属于给定模板的item prototypes。|
|triggerids|string/array|Return only item prototypes that are used in the given trigger prototypes.只返回使用在给定trigger prototypes的item prototypes。|
|selectApplications|query|Return applications that the item prototype belongs to in the `applications` property.在`applications`属性中返回item prototype所属的applications。|
|selectApplicationPrototypes|query|Return application prototypes linked to item prototype in `applicationPrototypes` property. 只返回被连接到`applicationPrototypes`属性中的item prototype的application prototypes。|
|selectDiscoveryRule|query|Return the low-level discovery rule that the graph prototype belongs to in the `discoveryRule` property.在`discoveryRule` 属性中返回图表原型所属的低级发现规则。|
|selectGraphs|query|Return graph prototypes that the item prototype is used in in the `graphs` property. 在`graphs`属性中返回被item prototype使用的graph prototypes。<br><br>Supports `count`. 支持`count`。|
|selectHosts|query|Returns the host that the item prototype belongs to as an array in the `hosts` property.在`hosts`属性中以数组的形式返回item prototype所属的host。|
|selectTriggers|query|Return trigger prototypes that the item prototype is used in in the `triggers` property. 在`triggers`属性中返回item prototype被使用的trigger prototypes。<br><br>Supports `count`. 支持`count`。|
|selectPreprocessing|query|Return item preprocessing options in `preprocessing` property.在`preprocessing`属性中返回项item preprocessing选项。<br><br>It has the following properties: 它有如下属性<br>`type` - `(string)` The preprocessing option types:preprocessing 选项类型：<br>1 - Custom multiplier;<br>2 - Right trim;<br>3 - Left trim;<br>4 - Trim;<br>5 - Regular expression matching;<br>6 - Boolean to decimal;<br>7 - Octal to decimal;<br>8 - Hexadecimal to decimal;<br>9 - Simple change;<br>10 - Change per second.<br><br>`params` - `(string)` Additional parameters used by preprocessing option. Multiple parameters are separated by LF (\\n)character.|
|filter|object|Return only those results that exactly match the given filter.只返回精确匹配筛选条件的结果。<br><br>Accepts an array, where the keys are property names, and the values are either a single value or an array of values to match against. 接受一个数组，数组键为属性名称，值为单个值或者数组。<br><br>Supports additional filters: 支持可选筛选条件：<br>`host` - technical name of the host that the item prototype belongs to. `host` - item prototype 所属的主机的技术名称。|
|limitSelects|integer|Limits the number of records returned by subselects. 限制子选择返回的记录数。<br><br>Applies to the following subselects:应用于如下子选择：<br>`selectGraphs` - results will be sorted by `name`;<br>`selectTriggers` - results will be sorted by `description`.|
|sortfield|string/array|Sort the result by the given properties. 根据给定的属性排序<br><br>Possible values are:可能的值有： `itemid`, `name`, `key_`, `delay`, `type` and `status`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters). 这些参数对于所有在[reference commentary](/zh/manual/api/reference_commentary#common_get_method_parameters)详细描述的"get"方法都是通用的。|
|editable|boolean|^|
|excludeSearch|boolean|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Return values

[comment]: # ({/new-b41637d2})

[comment]: # ({new-92651f4c})
### 返回值

`(integer/array)` Returns either:

-   an array of objects;对象数组；
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.已获取到的对象的数量，如果`countOutput`参数被使用。

[comment]: # ({/new-92651f4c})

[comment]: # ({new-acb23a7a})
### Examples

[comment]: # ({/new-acb23a7a})

[comment]: # ({new-18563427})
### 示例

[comment]: # ({/new-18563427})

[comment]: # ({new-b9b138b8})
#### Retrieving item prototypes from an LLD rule

[comment]: # ({/new-b9b138b8})

[comment]: # ({new-e1523143})
#### 获取 item prototypes

Retrieve all item prototypes from an LLD rule. 从LLD规则中获取所有item
prototypes

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "itemprototype.get",
    "params": {
        "output": "extend",
        "discoveryids": "27426"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "itemid": "23077",
            "type": "0",
            "snmp_community": "",
            "snmp_oid": "",
            "hostid": "10079",
            "name": "Incoming network traffic on $1",
            "key_": "net.if.in[en0]",
            "delay": "1m",
            "history": "1w",
            "trends": "365d",
            "status": "0",
            "value_type": "3",
            "trapper_hosts": "",
            "units": "bps",
            "snmpv3_securityname": "",
            "snmpv3_securitylevel": "0",
            "snmpv3_authpassphrase": "",
            "snmpv3_privpassphrase": "",
            "formula": "",
            "error": "",
            "lastlogsize": "0",
            "logtimefmt": "",
            "templateid": "0",
            "valuemapid": "0",
            "params": "",
            "ipmi_sensor": "",
            "authtype": "0",
            "username": "",
            "password": "",
            "publickey": "",
            "privatekey": "",
            "mtime": "0",
            "flags": "0",
            "interfaceid": "0",
            "port": "",
            "description": "",
            "inventory_link": "0",
            "lifetime": "30d",
            "snmpv3_authprotocol": "0",
            "snmpv3_privprotocol": "0",
            "state": "0",
            "snmpv3_contextname": "",
            "evaltype": "0",
            "jmx_endpoint": "",
            "master_itemid": "0",
            "timeout": "3s",
            "url": "",
            "query_fields": [],
            "posts": "",
            "status_codes": "200",
            "follow_redirects": "1",
            "post_type": "0",
            "http_proxy": "",
            "headers": [],
            "retrieve_mode": "0",
            "request_method": "1",
            "output_format": "0",
            "ssl_cert_file": "",
            "ssl_key_file": "",
            "ssl_key_password": "",
            "verify_peer": "0",
            "verify_host": "0",
            "allow_traps": "0",
            "lastclock": "0",
            "lastns": "0",
            "lastvalue": "0",
            "prevvalue": "0"
        },
        {
            "itemid": "10010",
            "type": "0",
            "snmp_community": "",
            "snmp_oid": "",
            "hostid": "10001",
            "name": "Processor load (1 min average per core)",
            "key_": "system.cpu.load[percpu,avg1]",
            "delay": "1m",
            "history": "1w",
            "trends": "365d",
            "status": "0",
            "value_type": "0",
            "trapper_hosts": "",
            "units": "",
            "snmpv3_securityname": "",
            "snmpv3_securitylevel": "0",
            "snmpv3_authpassphrase": "",
            "snmpv3_privpassphrase": "",
            "formula": "",
            "error": "",
            "lastlogsize": "0",
            "logtimefmt": "",
            "templateid": "0",
            "valuemapid": "0",
            "params": "",
            "ipmi_sensor": "",
            "authtype": "0",
            "username": "",
            "password": "",
            "publickey": "",
            "privatekey": "",
            "mtime": "0",
            "flags": "0",
            "interfaceid": "0",
            "port": "",
            "description": "The processor load is calculated as system CPU load divided by number of CPU cores.",
            "inventory_link": "0",
            "lifetime": "0",
            "snmpv3_authprotocol": "0",
            "snmpv3_privprotocol": "0",
            "state": "0",
            "snmpv3_contextname": "",
            "evaltype": "0",
            "jmx_endpoint": "",
            "master_itemid": "0",
            "timeout": "3s",
            "url": "",
            "query_fields": [],
            "posts": "",
            "status_codes": "200",
            "follow_redirects": "1",
            "post_type": "0",
            "http_proxy": "",
            "headers": [],
            "retrieve_mode": "0",
            "request_method": "1",
            "output_format": "0",
            "ssl_cert_file": "",
            "ssl_key_file": "",
            "ssl_key_password": "",
            "verify_peer": "0",
            "verify_host": "0",
            "allow_traps": "0",
            "lastclock": "0",
            "lastns": "0",
            "lastvalue": "0",
            "prevvalue": "0"
        }
    ],
    "id": 1
}
```

#### Finding dependent item

#### 查找依赖的item

Find one Dependent item for item with ID "25545".
为ID为"25545"的item查找一个赖的 item。

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "item.get",
    "params": {
        "output": "extend",
        "filter": {
            "type": "18",
            "master_itemid": "25545"
        },
        "limit": "1"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "itemid": "25547",
            "type": "18",
            "snmp_community": "",
            "snmp_oid": "",
            "hostid": "10116",
            "name": "Seconds",
            "key_": "apache.status.uptime.seconds",
            "delay": "0",
            "history": "90d",
            "trends": "365d",
            "status": "0",
            "value_type": "3",
            "trapper_hosts": "",
            "units": "",
            "snmpv3_securityname": "",
            "snmpv3_securitylevel": "0",
            "snmpv3_authpassphrase": "",
            "snmpv3_privpassphrase": "",
            "formula": "",
            "error": "",
            "lastlogsize": "0",
            "logtimefmt": "",
            "templateid": "0",
            "valuemapid": "0",
            "params": "",
            "ipmi_sensor": "",
            "authtype": "0",
            "username": "",
            "password": "",
            "publickey": "",
            "privatekey": "",
            "mtime": "0",
            "flags": "0",
            "interfaceid": "0",
            "port": "",
            "description": "",
            "inventory_link": "0",
            "lifetime": "30d",
            "snmpv3_authprotocol": "0",
            "snmpv3_privprotocol": "0",
            "state": "0",
            "snmpv3_contextname": "",
            "evaltype": "0",
            "master_itemid": "25545",
            "jmx_endpoint": "",
            "master_itemid": "0",
            "timeout": "3s",
            "url": "",
            "query_fields": [],
            "posts": "",
            "status_codes": "200",
            "follow_redirects": "1",
            "post_type": "0",
            "http_proxy": "",
            "headers": [],
            "retrieve_mode": "0",
            "request_method": "1",
            "output_format": "0",
            "ssl_cert_file": "",
            "ssl_key_file": "",
            "ssl_key_password": "",
            "verify_peer": "0",
            "verify_host": "0",
            "allow_traps": "0",
            "lastclock": "0",
            "lastns": "0",
            "lastvalue": "0",
            "prevvalue": "0"
        }
    ],
    "id": 1
}
```

#### Find HTTP agent item prototype

#### 查找 HTTP agent item prototype

Find HTTP agent item prototype with request method HEAD for specific
host id. 为请求方法头定义的host id查找HTTP agent item prototype。

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "itemprototype.get",
    "params": {
        "hostids": "10254",
        "filter": {
            "type": "19",
            "request_method": "3"
        }
    },
    "id": 17,
    "auth": "d678e0b85688ce578ff061bd29a20d3b"
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "itemid": "28257",
            "type": "19",
            "snmp_community": "",
            "snmp_oid": "",
            "hostid": "10254",
            "name": "discovered",
            "key_": "item[{#INAME}]",
            "delay": "{#IUPDATE}",
            "history": "90d",
            "trends": "30d",
            "status": "0",
            "value_type": "3",
            "trapper_hosts": "",
            "units": "",
            "snmpv3_securityname": "",
            "snmpv3_securitylevel": "0",
            "snmpv3_authpassphrase": "",
            "snmpv3_privpassphrase": "",
            "formula": "",
            "error": "",
            "lastlogsize": "0",
            "logtimefmt": "",
            "templateid": "28255",
            "valuemapid": "0",
            "params": "",
            "ipmi_sensor": "",
            "authtype": "0",
            "username": "",
            "password": "",
            "publickey": "",
            "privatekey": "",
            "mtime": "0",
            "flags": "2",
            "interfaceid": "2",
            "port": "",
            "description": "",
            "inventory_link": "0",
            "lifetime": "30d",
            "snmpv3_authprotocol": "0",
            "snmpv3_privprotocol": "0",
            "state": "0",
            "snmpv3_contextname": "",
            "evaltype": "0",
            "jmx_endpoint": "",
            "master_itemid": "0",
            "timeout": "3s",
            "url": "{#IURL}",
            "query_fields": [],
            "posts": "",
            "status_codes": "",
            "follow_redirects": "0",
            "post_type": "0",
            "http_proxy": "",
            "headers": [],
            "retrieve_mode": "0",
            "request_method": "3",
            "output_format": "0",
            "ssl_cert_file": "",
            "ssl_key_file": "",
            "ssl_key_password": "",
            "verify_peer": "0",
            "verify_host": "0",
            "allow_traps": "0"
        }
    ],
    "id": 17
}
```

### See also

#### 参考其他

-   [Application](/zh/manual/api/reference/application/object#application)
-   [Host](/zh/manual/api/reference/host/object#host)
-   [Graph
    prototype](/zh/manual/api/reference/graphprototype/object#graph_prototype)
-   [Trigger
    prototype](/zh/manual/api/reference/triggerprototype/object#trigger_prototype)

### Source

### 源

CItemPrototype::get() in
*frontends/php/include/classes/api/services/CItemPrototype.php*.

[comment]: # ({/new-e1523143})
