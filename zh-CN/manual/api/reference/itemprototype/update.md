[comment]: # translation:outdated

[comment]: # ({new-cc00d3db})
# itemprototype.update

[comment]: # ({/new-cc00d3db})

[comment]: # ({new-63442cbb})
### Description

[comment]: # ({/new-63442cbb})

[comment]: # ({new-16fcb884})
### 说明

`object itemprototype.update(object/array itemPrototypes)`

This method allows to update existing item prototypes.
此方法允许更新存在的item prototypes（监控项原型）。

[comment]: # ({/new-16fcb884})

[comment]: # ({new-f7f1feb9})
### Parameters

[comment]: # ({/new-f7f1feb9})

[comment]: # ({new-b41637d2})
### 参数

`(object/array)` Item prototype properties to be updated.Item prototype
要更新的属性。

The `itemid` property must be defined for each item prototype, all other
properties are optional. Only the passed properties will be updated, all
others will remain unchanged.Item prototype 的
`itemid`的属性必须定义，所有其他属性为可选。只用被传递的属性才会被更新，所有其他未被传递的属性保持不变。

Additionally to the [standard item prototype
properties](object#item_prototype), the method accepts the following
parameters.此方法接受如下参数。

|Parameter|Type|Description 说明|
|---------|----|------------------|
|applications|array|IDs of the applications to replace the current applications. 要替换当前应用程序的应用程序的IDS。|
|applicationPrototypes|array|Names of the application prototypes to replace the current application prototypes. 要替换当前应用程序原型的应用程序原型名称。|
|preprocessing|array|Item prototype preprocessing options to replace the current preprocessing options. 要替换当前预处理选项的item prototype的预处理选项。|

[comment]: # ({/new-b41637d2})

[comment]: # ({new-48cea947})
### Return values

[comment]: # ({/new-48cea947})

[comment]: # ({new-e1d8fc73})
### 返回值

`(object)` Returns an object containing the IDs of the updated item
prototypes under the `itemids` property.
`(object)`在`itemids`属性中返回一个包含已被更新的item
prototypes的IDs对象。

[comment]: # ({/new-e1d8fc73})

[comment]: # ({new-d7fe7206})
### Examples

[comment]: # ({/new-d7fe7206})

[comment]: # ({new-e5fc44e0})
### 示例

[comment]: # ({/new-e5fc44e0})

[comment]: # ({new-15ba4525})
#### Changing the interface of an item prototype

[comment]: # ({/new-15ba4525})

[comment]: # ({new-bc6956c2})
#### 改变 item prototype 的接口

Change the host interface that will be used by discovered items.
改变将被用于发现监控项的主机接口。

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "itemprototype.update",
    "params": {
        "itemid": "27428",
        "interfaceid": "132"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "27428"
        ]
    },
    "id": 1
}
```

#### Update dependent item prototype

#### 更新依赖的item prototype

Update Dependent item prototype with new Master item prototype ID. Only
dependencies on same host (template/discovery rule) are allowed,
therefore Master and Dependent item should have same hostid and ruleid.
以新的主item prototype的ID来更新依赖的 item
prototype。只有在相同host(template/discovery
rule)的依赖才允许，因此主监控项和依赖监控项应有相同hostid和ruleid。

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "itemprototype.update",
    "params": {
        "master_itemid": "25570",
        "itemid": "189030"
    },
    "auth": "700ca65537074ec963db7efabda78259",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "189030"
        ]
    },
    "id": 1
}
```

#### Update HTTP agent item prototype

#### 更新 HTTP agent item prototype

Change query fields and remove all custom headers.
改变查询字段并移除所有自定义请求头。

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "itemprototype.update",
    "params": {
        "itemid":"28305",
        "query_fields": [
            {
                "random": "qwertyuiopasdfghjklzxcvbnm"
            }
        ],
        "headers": []
    }
    "auth": "700ca65537074ec963db7efabda78259",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "28305"
        ]
    },
    "id": 1
}
```

### Source

### 源

CItemPrototype::update() in
*frontends/php/include/classes/api/services/CItemPrototype.php*.

[comment]: # ({/new-bc6956c2})
