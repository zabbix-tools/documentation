[comment]: # translation:outdated

[comment]: # ({new-dbb7dd13})
# > Item prototype object

The following objects are directly related to the `itemprototype` API.
如下对象与`itemprototype` API直接相关。

[comment]: # ({/new-dbb7dd13})

[comment]: # ({new-36a2631c})
### Item prototype 监控项原型

The item prototype object has the following properties.
监控项原型有如下属性。

|Property|Type|Description|<|
|--------|----|-----------|-|
|itemid|string|*(readonly)* ID of the item prototype. 监控项原型的ID。|<|
|**delay**<br>(required)|string|Update interval of the item prototype. Accepts seconds or time unit with suffix and with or without one or more [custom intervals](/zh/manual/config/items/item/custom_intervals) that consist of either flexible intervals and scheduling intervals as serialized strings. Also accepts user macros and LLD macros. Flexible intervals could be written as two macros separated by a forward slash. Intervals are separated by a semicolon.监控项原型的更新时间间隔。接受具有后缀的秒或时间单位，并且具有或不具有由灵活间隔和调度间隔组成的一个或多个自定义间隔作为串行化字符串。还接受用户宏和LLD宏。 灵活的间隔可以写成两个由正斜杠分隔的宏。 间隔用分号分隔。<br><br>Optional for Zabbix trapper or Dependent item.|<|
|**hostid**<br>(required)|string|ID of the host that the item prototype belongs to. 监控项原型所属的主机的ID。<br><br>For update operations this field is *readonly*.|<|
|**ruleid**<br>(required)|string|ID of the LLD rule that the item belongs to. 监控项所属的LLD（低级别发现）的ID<br><br>For update operations this field is *readonly*.|<|
|**interfaceid**<br>(required)|string|ID of the item prototype's host interface. Used only for host item prototypes. 监控项原型的主机的接口的ID。仅用于主机监控项原型。<br><br>Optional for Zabbix agent (active), Zabbix internal, Zabbix trapper, Dependent item, Zabbix aggregate, database monitor and calculated item prototypes.|<|
|**key\_**<br>(required)|string|Item prototype key. 监控项原型的键。|<|
|**name**<br>(required)|string|Name of the item prototype. 监控项原型的名称。|<|
|**type**<br>(required)|integer|Type of the item prototype. 监控项原型的类型。<br><br>Possible values: 可能的值：<br>0 - Zabbix agent;<br>1 - SNMPv1 agent;<br>2 - Zabbix trapper;<br>3 - simple check;<br>4 - SNMPv2 agent;<br>5 - Zabbix internal;<br>6 - SNMPv3 agent;<br>7 - Zabbix agent (active);<br>8 - Zabbix aggregate;<br>10 - external check;<br>11 - database monitor;<br>12 - IPMI agent;<br>13 - SSH agent;<br>14 - TELNET agent;<br>15 - calculated;<br>16 - JMX agent;<br>17 - SNMP trap;<br>18 - Dependent item;<br>19 - HTTP agent;<br>|<|
|**url**<br>(required)|string|URL string required only for HTTP agent item prototypes. Supports LLD macros, user macros, {HOST.IP}, {HOST.CONN}, {HOST.DNS}, {HOST.HOST}, {HOST.NAME}, {ITEM.ID}, {ITEM.KEY}. 仅在HTTP agent监控项原型有要求的URL字符串。支持LLD macros, user macros, {HOST.IP}, {HOST.CONN}, {HOST.DNS}, {HOST.HOST}, {HOST.NAME}, {ITEM.ID}, {ITEM.KEY}。|<|
|**value\_type**<br>(required)|integer|Type of information of the item prototype. 监控项原型信息类型。<br><br>Possible values: 可能的值：<br>0 - numeric float;<br>1 - character;<br>2 - log;<br>3 - numeric unsigned;<br>4 - text.|<|
|allow\_traps|integer|HTTP agent item prototype field. Allow to populate value as in trapper item type also. HTTP agent监控项原型字段。允许像trapper监控项一样的填充值。<br><br>0 - *(default)* Do not allow to accept incoming data.<br>1 - Allow to accept incoming data.|<|
|authtype|integer|Used only by SSH agent item prototypes or HTTP agent item prototypes. 仅用于SSH agent 监控项原型或者 HTTP agent 监控项原型。<br><br>SSH agent authentication method possible values: SSH agent认证方法可能的值：<br>0 - *(default)* password;<br>1 - public key.<br><br>HTTP agent authentication method possible values:<br>0 - *(default)* none<br>1 - basic<br>2 - NTLM|<|
|description|string|Description of the item prototype. 监控项原型的说明。|<|
|follow\_redirects|integer|HTTP agent item prototype field. Follow respose redirects while pooling data. HTTP agent监控项原型字段。当合并数据时跟随重定向。<br><br>0 - Do not follow redirects.<br>1 - *(default)* Follow redirects.|<|
|headers|object|HTTP agent item prototype field. Object with HTTP(S) request headers, where header name is used as key and header value as value. HTTP agent监控项原型字段。 带有HTTP(S)的报头，名称是键，报文的值是键的值。<br><br>[Example:示例](Example:示例)：<br>{ "User-Agent": "Zabbix" }|<|
|history|string|A time unit of how long the history data should be stored. Also accepts user macro and LLD macro. 历史数据应被保存的时间。接受用户宏和LLD宏。<br><br>Default: 90d.|<|
|http\_proxy|string|HTTP agent item prototype field. HTTP(S) proxy connection string. HTTP agent监控项原型字段。HTTP(S)代理连接字符串。|<|
|ipmi\_sensor|string|IPMI sensor. Used only by IPMI item prototypes. IPMI传感器，仅用于IPMI监控项原型。|<|
|jmx\_endpoint|string|JMX agent custom connection string. JMX agent自定义的连接字符串。<br><br>Default value:<br>service:jmx:rmi:///jndi/rmi://{HOST.CONN}:{HOST.PORT}/jmxrmi|<|
|logtimefmt|string|Format of the time in log entries. Used only by log item prototypes. 日志条目的时间格式。仅用于日志监控项原型。|<|
|master\_itemid|integer|Master item ID. 主监控项ID。\                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        递归3层Recursion up to 3 dependent items and item prototypes and maximum count of dependent items and item prototypes equal to 999 are allowed.<br><br>Required by Dependent items.|赖监控项和监控项原型，最大数目的监控项和监控项原型等999是允许的。|
|output\_format|integer|HTTP agent item prototype field. Should response converted to JSON. HTTP agent监控项原型字段。返回数据应被转换为JSON格式。<br><br>0 - *(default)* Store raw.<br>1 - Convert to JSON.|<|
|params|string|Additional parameters depending on the type of the item prototype: 附加参数依赖于监控项原型的类型：<br>- executed script for SSH and Telnet item prototypes;<br>- SQL query for database monitor item prototypes;<br>- formula for calculated item prototypes.|<|
|password|string|Password for authentication. Used by simple check, SSH, Telnet, database monitor, JMX and HTTP agent item prototypes. 认证的密码。用于simple check, SSH, Telnet, database monitor, JMX and HTTP agent 监控项原型。|<|
|port|string|Port monitored by the item prototype. Used only by SNMP items prototype. 监控的监控项原型的端口。仅用于SNMP监控项原型。|<|
|post\_type|integer|HTTP agent item prototype field. Type of post data body stored in posts property. HTTP agent监控项原型字段。存储在post属性的post数据体的类型。<br><br>0 - *(default)* Raw data.<br>2 - JSON data.<br>3 - XML data.|<|
|posts|string|HTTP agent item prototype field. HTTP(S) request body data. Used with post\_type. HTTP agent监控项原型字段。HTTP(S) 请求报文数据。用于post\_data。|<|
|privatekey|string|Name of the private key file. 私钥文件名。|<|
|publickey|string|Name of the public key file. 公钥文件名。|<|
|query\_fields|array|HTTP agent item prototype field. Query parameters. Array of objects with 'key':'value' pairs, where value can be empty string. HTTP agent监控项原型字段。查询参数。带有键值对的数组对象，值可以为空字符串。|<|
|request\_method|integer|HTTP agent item prototype field. Type of request method. HTTP agent监控项原型字段。请求方法类型。<br><br>0 - GET<br>1 - *(default)* POST<br>2 - PUT<br>3 - HEAD|<|
|retrieve\_mode|integer|HTTP agent item prototype field. What part of response should be stored. HTTP agent监控项原型字段。指定那一部分的响应应该被存储。<br><br>0 - *(default)* Body.<br>1 - Headers.<br>2 - Both body and headers will be stored.<br><br>For request\_method HEAD only 1 is allowed value.|<|
|snmp\_community|string|SNMP community.<br><br>Used only by SNMPv1 and SNMPv2 item prototypes. 仅用于SNMPv1和SNMPv2监控项原型。|<|
|snmp\_oid|string|SNMP OID.|<|
|snmpv3\_authpassphrase|string|SNMPv3 auth passphrase. Used only by SNMPv3 item prototypes. SNMPv3认证密码。仅用于SNMPv3监控项原型。|<|
|snmpv3\_authprotocol|integer|SNMPv3 authentication protocol. Used only by SNMPv3 items. SNMPv3认证协议。仅用于SNMPv3监控项原型。<br><br>Possible values:<br>0 - *(default)* MD5;<br>1 - SHA.|<|
|snmpv3\_contextname|string|SNMPv3 context name. Used only by SNMPv3 item prototypes. SNMPv3文本名称。仅用于SNMPv3监控项原型。|<|
|snmpv3\_privpassphrase|string|SNMPv3 priv passphrase. Used only by SNMPv3 item prototypes. SNMPv3私有密码。仅用于SNMPv3监控项原型。|<|
|snmpv3\_privprotocol|integer|SNMPv3 privacy protocol. Used only by SNMPv3 items. SNMPv3私有协议。仅用于SNMPv3监控项原型。<br><br>Possible values:<br>0 - *(default)* DES;<br>1 - AES.|<|
|snmpv3\_securitylevel|integer|SNMPv3 security level. Used only by SNMPv3 item prototypes. SNMPv3安全等级。仅用于SNMPv3监控项原型。<br><br>Possible values:<br>0 - noAuthNoPriv;<br>1 - authNoPriv;<br>2 - authPriv.|<|
|snmpv3\_securityname|string|SNMPv3 security name. Used only by SNMPv3 item prototypes. SNMPv3认安全名称。仅用于SNMPv3监控项原型。|<|
|ssl\_cert\_file|string|HTTP agent item prototype field. Public SSL Key file path. HTTP agent监控项原型字段。公共SSL key文件路径。|<|
|ssl\_key\_file|string|HTTP agent item prototype field. Private SSL Key file path. HTTP agent监控项原型字段。私有SSL key文件路径。|<|
|ssl\_key\_password|string|HTTP agent item prototype field. Password for SSL Key file. HTTP agent监控项原型字段。SSL key文件的密码。|<|
|status|integer|Status of the item prototype. 监控项原型的状态。<br><br>Possible values: 可能的值。<br>0 - *(default)* enabled item prototype;<br>1 - disabled item prototype;<br>3 - unsupported item prototype.|<|
|status\_codes|string|HTTP agent item prototype field. Ranges of required HTTP status codes separated by commas. Also supports user macros or LLD macros as part of comma separated list. HTTP agent监控项原型字段。以逗号分隔的要求的HTTP状态码的范围。也接受用户宏和LLD宏。<br><br>Example: 200,200-{$M},{$M},200-400|<|
|templateid|string|(readonly) ID of the parent template item prototype. （只读）父模板的监控项原型的ID。|<|
|timeout|string|HTTP agent item prototype field. Item data polling request timeout. Support user macros and LLD macros. HTTP agent监控项原型字段。监控项数据合并请求超时时间。支持用户宏和LLD宏。<br><br>default: 3s<br>maximum value: 60s|<|
|trapper\_hosts|string|Allowed hosts. Used by trapper item prototypes or HTTP item prototypes.允许主机。用于trapp监控项原型或者HTTP监控项原型。|<|
|trends|string|A time unit of how long the trends data should be stored. Also accepts user macro and LLD macro. 趋势数据被保存的时间。也接受用户宏和LLD宏。<br><br>Default: 365d.|<|
|units|string|Value units.|<|
|username|string|Username for authentication. Used by simple check, SSH, Telnet, database monitor, JMX and HTTP agent item prototypes. 认证的用户名。用于simple check, SSH, Telnet, database monitor, JMX and HTTP agent 监控项原型。<br><br>Required by SSH and Telnet item prototypes. SSH 和 Telnet 监控项原型要求。|<|
|valuemapid|string|ID of the associated value map. 相关值映射的ID。|<|
|verify\_host|integer|HTTP agent item prototype field. Validate host name in URL is in Common Name field or a Subject Alternate Name field of host certificate. HTTP agent监控项原型字段。验证URL中的主机名在主机证书中的通用名字段或者备用字段。<br><br>0 - *(default)* Do not validate.<br>1 - Validate.<br>|<|
|verify\_peer|integer|HTTP agent item prototype field. Validate is host certificate authentic. HTTP agent监控项原型字段。主机合法性认证。<br><br>0 - *(default)* Do not validate.<br>1 - Validate.|<|

[comment]: # ({/new-36a2631c})



[comment]: # ({new-66f440ed})
### Item prototype tag

The item prototype tag object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**tag**<br>(required)|string|Item prototype tag name.|
|value|string|Item prototype tag value.|

[comment]: # ({/new-66f440ed})

[comment]: # ({new-ad34a8b2})
### Item prototype preprocessing

The item prototype preprocessing object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**type**<br>(required)|integer|The preprocessing option type.<br><br>Possible values:<br>1 - Custom multiplier;<br>2 - Right trim;<br>3 - Left trim;<br>4 - Trim;<br>5 - Regular expression matching;<br>6 - Boolean to decimal;<br>7 - Octal to decimal;<br>8 - Hexadecimal to decimal;<br>9 - Simple change;<br>10 - Change per second;<br>11 - XML XPath;<br>12 - JSONPath;<br>13 - In range;<br>14 - Matches regular expression;<br>15 - Does not match regular expression;<br>16 - Check for error in JSON;<br>17 - Check for error in XML;<br>18 - Check for error using regular expression;<br>19 - Discard unchanged;<br>20 - Discard unchanged with heartbeat;<br>21 - JavaScript;<br>22 - Prometheus pattern;<br>23 - Prometheus to JSON;<br>24 - CSV to JSON;<br>25 - Replace;<br>26 - Check unsupported;<br>27 - XML to JSON.|
|**params**<br>(required)|string|Additional parameters used by preprocessing option. Multiple parameters are separated by LF (\\n) character.|
|**error\_handler**<br>(required)|integer|Action type used in case of preprocessing step failure.<br><br>Possible values:<br>0 - Error message is set by Zabbix server;<br>1 - Discard value;<br>2 - Set custom value;<br>3 - Set custom error message.|
|**error\_handler\_params**<br>(required)|string|Error handler parameters. Used with `error_handler`.<br><br>Must be empty, if `error_handler` is 0 or 1.<br>Can be empty if, `error_handler` is 2.<br>Cannot be empty, if `error_handler` is 3.|

The following parameters and error handlers are supported for each
preprocessing type.

|Preprocessing type|Name|Parameter 1|Parameter 2|Parameter 3|Supported error handlers|
|------------------|----|-----------|-----------|-----------|------------------------|
|1|Custom multiplier|number^1,\ 6^|<|<|0, 1, 2, 3|
|2|Right trim|list of characters^2^|<|<|<|
|3|Left trim|list of characters^2^|<|<|<|
|4|Trim|list of characters^2^|<|<|<|
|5|Regular expression|pattern^3^|output^2^|<|0, 1, 2, 3|
|6|Boolean to decimal|<|<|<|0, 1, 2, 3|
|7|Octal to decimal|<|<|<|0, 1, 2, 3|
|8|Hexadecimal to decimal|<|<|<|0, 1, 2, 3|
|9|Simple change|<|<|<|0, 1, 2, 3|
|10|Change per second|<|<|<|0, 1, 2, 3|
|11|XML XPath|path^4^|<|<|0, 1, 2, 3|
|12|JSONPath|path^4^|<|<|0, 1, 2, 3|
|13|In range|min^1,\ 6^|max^1,\ 6^|<|0, 1, 2, 3|
|14|Matches regular expression|pattern^3^|<|<|0, 1, 2, 3|
|15|Does not match regular expression|pattern^3^|<|<|0, 1, 2, 3|
|16|Check for error in JSON|path^4^|<|<|0, 1, 2, 3|
|17|Check for error in XML|path^4^|<|<|0, 1, 2, 3|
|18|Check for error using regular expression|pattern^3^|output^2^|<|0, 1, 2, 3|
|19|Discard unchanged|<|<|<|<|
|20|Discard unchanged with heartbeat|seconds^5,\ 6^|<|<|<|
|21|JavaScript|script^2^|<|<|<|
|22|Prometheus pattern|pattern^6,\ 7^|output^6,\ 8^|<|0, 1, 2, 3|
|23|Prometheus to JSON|pattern^6,\ 7^|<|<|0, 1, 2, 3|
|24|CSV to JSON|character^2^|character^2^|0,1|0, 1, 2, 3|
|25|Replace|search string^2^|replacement^2^|<|<|
|26|Check unsupported|<|<|<|1, 2, 3|
|27|XML to JSON|<|<|<|0, 1, 2, 3|

^1^ integer or floating-point number\
^2^ string\
^3^ regular expression\
^4^ JSONPath or XML XPath\
^5^ positive integer (with support of time suffixes, e.g. 30s, 1m, 2h,
1d)\
^6^ user macro, LLD macro\
^7^ Prometheus pattern following the syntax:
`<metric name>{<label name>="<label value>", ...} == <value>`. Each
Prometheus pattern component (metric, label name, label value and metric
value) can be user macro or LLD macro.\
^8^ Prometheus output following the syntax: `<label name>`.

[comment]: # ({/new-ad34a8b2})
