[comment]: # translation:outdated

[comment]: # ({new-450c25c2})
# itemprototype.create

[comment]: # ({/new-450c25c2})

[comment]: # ({new-0ad3a6f6})
### Description

[comment]: # ({/new-0ad3a6f6})

[comment]: # ({new-56c4ce2f})
### 说明

`object itemprototype.create(object/array itemPrototypes)`

This method allows to create new item prototypes. 此方法用于创建新的item
prototypes（监控项原型）。

[comment]: # ({/new-56c4ce2f})

[comment]: # ({new-4682a6ee})
### Parameters

[comment]: # ({/new-4682a6ee})

[comment]: # ({new-b41637d2})
### 参数

`(object/array)` Item prototype to create. `(object/array)`
需要创建的item prototypes。

Additionally to the [standard item prototype
properties](object#item_prototype), the method accepts the following
parameters. 除标准项原型属性外，该方法还接受以下参数。

|Parameter|Type|Description|
|---------|----|-----------|
|**ruleid**<br>(required)|string|ID of the LLD rule that the item belongs to. 该项所属的LLD规则的ID。|
|applications|array|IDs of applications to be assigned to the discovered items.要分配给自动发现item的应用程序的ID。|
|applicationPrototypes|array|Names of application prototypes to be assigned to the item prototype. 要分配给item prototypes的应用程序原型的名称。|
|preprocessing|array|Item prototype preprocessing options. Item prototype 预处理选项。|

[comment]: # ({/new-b41637d2})

[comment]: # ({new-9956fee4})
### Return values

[comment]: # ({/new-9956fee4})

[comment]: # ({new-d22f43ae})
### 返回值

`(object)` Returns an object containing the IDs of the created item
prototypes under the `itemids` property. The order of the returned IDs
matches the order of the passed item prototypes. `(object)`
返回一个对象，该对象ID包含在“itemid”属性中。 返回的ID的顺序与传递的item
prototypes的顺序相对应。

[comment]: # ({/new-d22f43ae})

[comment]: # ({new-76dad6d6})
### Examples

[comment]: # ({/new-76dad6d6})

[comment]: # ({new-94e7709f})
### 示例

[comment]: # ({/new-94e7709f})

[comment]: # ({new-9e8477fc})
#### Creating an item prototype

Create an item prototype to monitor free disc space on a discovered file
system. Discovered items should be numeric Zabbix agent items updated
every 30 seconds. 创建一个item
prototype去监控自动发现的文件系统上的磁盘空间。Discovered items
应该每30秒更新数字化的Zabbix agent监控项。

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "itemprototype.create",
    "params": {
        "name": "Free disk space on $1",
        "key_": "vfs.fs.size[{#FSNAME},free]",
        "hostid": "10197",
        "ruleid": "27665",
        "type": 0,
        "value_type": 3,
        "interfaceid": "112",
        "delay": "30s"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "27666"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-9e8477fc})

[comment]: # ({new-4f193e2e})
#### Creating an item prototype with preprocessing

#### 创建一个预处理的 item prototype

Create an item using change per second and a custom multiplier as a
second step. 创建一个使用每秒变化并带有自定义乘法器作为第二部的监控项。

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "itemprototype.create",
    "params": {
        "name": "Incoming network traffic on $1",
        "key_": "net.if.in[{#IFNAME}]",
        "hostid": "10001",
        "ruleid": "27665",
        "type": 0,
        "value_type": 3,
        "delay": "60s",
        "units": "bps",
        "interfaceid": "1155",
        "preprocessing": [
            {
                "type": "10",
                "params": ""
            },
            {
                "type": "1",
                "params": "8"
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "44211"
        ]
    },
    "id": 1
}
```

#### Creating dependent item prototype

#### 创建依赖item prototype

Create Dependent item prototype for Master item prototype with ID 44211.
Only dependencies on same host (template/discovery rule) are allowed,
therefore Master and Dependent item should have same hostid and ruleid.
为ID为44211的主item prototype创建一个依赖item
prototype。只有在同一个主机的(template/discovery
rule)依赖才可以被接受，因此主 item prototype 和依赖 item prototype
应该拥有相同的hostid和ruleid。

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "itemprototype.create",
    "params": {
      "hostid": "10001",
      "ruleid": "27665",
      "name": "Dependent test item prototype",
      "key_": "dependent.prototype",
      "type": "18",
      "master_itemid": "44211",
      "value_type": "3"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "44212"
        ]
    },
    "id": 1
}
```

#### Create HTTP agent item prototype

#### 创建HTTP agent item prototype

Create item prototype with URL using user macro, query fields and custom
headers. 创建带有URL使用用户宏，查询字段和自定义选项的item prototype。

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "itemprototype.create",
    "params": {
        "type": "19",
        "hostid": "10254",
        "ruleid":"28256",
        "interfaceid":"2",
        "name": "api item prototype example",
        "key_": "api_http_item",
        "value_type": "3",
        "url": "{$URL_PROTOTYPE}",
        "query_fields": [
            {
                "min": "10"
            },
            {
                "max": "100"
            }
        ],
        "headers": {
            "X-Source": "api"
        },
        "delay":"35"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "28305"
        ]
    },
    "id": 1
}
```

### Source

### 源

CItemPrototype::create() in
*frontends/php/include/classes/api/services/CItemPrototype.php*.

[comment]: # ({/new-4f193e2e})
