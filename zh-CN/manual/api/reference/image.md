[comment]: # translation:outdated

[comment]: # ({new-f759709d})
# 图像

此类被设计用于管理图像.

对象引用:\

-   [Image](/manual/api/reference/image/object#image)

可用的方法:\

-   [image.create](/manual/api/reference/image/create) - 创建新图像
-   [image.delete](/manual/api/reference/image/delete) - 删除图像
-   [image.get](/manual/api/reference/image/get) - 获取图像
-   [image.update](/manual/api/reference/image/update) - 更新图像

[comment]: # ({/new-f759709d})
