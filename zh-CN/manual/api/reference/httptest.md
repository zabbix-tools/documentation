[comment]: # translation:outdated

[comment]: # ({new-7aa664d0})
# Web scenario \[Web场景\]

此类用于Web场景的使用。

对象引用：\

-   [Web scenario](/manual/api/reference/httptest/object#web_scenario)
-   [Scenario step](/manual/api/reference/httptest/object#scenario_step)

可用的方法：\

-   [httptest.create](/manual/api/reference/httptest/create) -
    创建新的Web场景
-   [httptest.delete](/manual/api/reference/httptest/delete) -
    删除Web场景
-   [httptest.get](/manual/api/reference/httptest/get) - 获取Web场景
-   [httptest.update](/manual/api/reference/httptest/update) -
    更新Web场景

## Web scenario

This class is designed to work with web scenarios.

Object references:\

-   [Web scenario](/manual/api/reference/httptest/object#web_scenario)
-   [Scenario step](/manual/api/reference/httptest/object#scenario_step)

Available methods:\

-   [httptest.create](/manual/api/reference/httptest/create) - creating
    new web scenarios
-   [httptest.delete](/manual/api/reference/httptest/delete) - deleting
    web scenarios
-   [httptest.get](/manual/api/reference/httptest/get) - retrieving web
    scenarios
-   [httptest.update](/manual/api/reference/httptest/update) - updating
    web scenarios

[comment]: # ({/new-7aa664d0})
