[comment]: # translation:outdated

[comment]: # ({new-8e76cf8c})
# 获取

[comment]: # ({/new-8e76cf8c})

[comment]: # ({new-04265e54})
### 描述

`integer/array iconmap.get(object parameters)`

此方法允许根据指定的参数获取图标拓扑图.

[comment]: # ({/new-04265e54})

[comment]: # ({new-efcde208})
### 参数

`(对象)` 定义要输出的参数.

该方法支持以下参数:

|参数                     类|描述|<|
|------------------------------|------|-|
|iconmapids|字符串/数组   返回指定|D的图标拓扑图.|
|sysmapids|字符串/数组   返回在指|拓扑图中使用的图标拓扑图.|
|selectMappings|查询          返|在mappings属性中使用的图标映射.|
|sortfield|字符串/数组   根据指定|属性将结果排序.<br><br>可能的值: `iconmapid` 和 `name`.|
|countOutput|布尔值        这些|数对于所有get方法都是通用的，详情可参考[reference commentary](/manual/api/reference_commentary#common_get_method_parameters).|
|editable|布尔值        ::|<|
|excludeSearch|布尔值        ::|<|
|filter|对象          :|:|
|limit|integer|^|
|output|查询          :|:|
|preservekeys|布尔值        ::|<|
|search|对象          :|:|
|searchByAny|布尔值        ::|<|
|searchWildcardsEnabled|布尔值        ::|<|
|sortorder|字符串/数组   :::|<|
|startSearch|布尔值        ::|<|

[comment]: # ({/new-efcde208})

[comment]: # ({new-7223bab1})
### 返回值

`(整数/数组)` 返回:

-   一组对象;
-   如果设置了`countOutput`参数,则返回对象数量.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### 示例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-016ff9fc})
#### 获取一个图标拓扑图

获取所有关于ID为3的图标拓扑图数据.

请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "iconmap.get",
    "params": {
        "iconmapids": "3",
        "output": "extend",
        "selectMappings": "extend"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "mappings": [
                {
                    "iconmappingid": "3",
                    "iconmapid": "3",
                    "iconid": "6",
                    "inventory_link": "1",
                    "expression": "server",
                    "sortorder": "0"
                },
                {
                    "iconmappingid": "4",
                    "iconmapid": "3",
                    "iconid": "10",
                    "inventory_link": "1",
                    "expression": "switch",
                    "sortorder": "1"
                }
            ],
            "iconmapid": "3",
            "name": "Host type icons",
            "default_iconid": "2"
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-016ff9fc})

[comment]: # ({new-8df05cf6})
### 参考

-   [Icon mapping](object#icon_mapping)

[comment]: # ({/new-8df05cf6})

[comment]: # ({new-2420d0f0})
### 来源

CIconMap::get() in
*frontends/php/include/classes/api/services/CIconMap.php*.

[comment]: # ({/new-2420d0f0})
