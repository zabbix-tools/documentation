[comment]: # translation:outdated

[comment]: # ({new-5c9f356a})
# 删除

[comment]: # ({/new-5c9f356a})

[comment]: # ({new-5e250e88})
### 描述

`object iconmap.delete(array iconMapIds)`

此方法允许删除图标拓扑图.

[comment]: # ({/new-5e250e88})

[comment]: # ({new-f911029a})
### 参数

`(数组)` 需要删除的图标拓扑图ID.

[comment]: # ({/new-f911029a})

[comment]: # ({new-44afa603})
### 返回值

`(对象)` 返回一个对象其中包含在iconmapids属性下的已删除图标拓扑图ID.

[comment]: # ({/new-44afa603})

[comment]: # ({new-b41637d2})
### 示例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-c7690d07})
#### 删除多个图标拓扑图

删除两个图标拓扑图.

请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "iconmap.delete",
    "params": [
        "2",
        "5"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "iconmapids": [
            "2",
            "5"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-c7690d07})

[comment]: # ({new-6198efd4})
### 来源

CIconMap::delete() in
*frontends/php/include/classes/api/services/CIconMap.php*.

[comment]: # ({/new-6198efd4})
