[comment]: # translation:outdated

[comment]: # ({new-b72559a5})
# > 图标映射对象

以下是和`iconmap` API相关的方法

[comment]: # ({/new-b72559a5})

[comment]: # ({new-74882a27})
### 图标拓扑

图标映射对象有以下属性：

|属性                   类|描述|<|
|----------------------------|------|-|
|iconmapid|字符串   *(|读)* 图标映射的ID.|
|**default\_iconid**<br>(必选)|字符串   默认|标的ID.|
|**name**<br>(必选)|字符串   图标|射的名称.|

[comment]: # ({/new-74882a27})

[comment]: # ({new-e88b40af})
### 图标映射

图标映射对象定义了一个具体的图标，给具有特定资产清单字段值的主机使用.图标映射有以下属性:

|属性                   类|描述|<|
|----------------------------|------|-|
|iconmappingid|字符串   *(|读)* 图标拓扑图ID.|
|**iconid**<br>(必选)|字符串   被图|映射使用到的图标ID.|
|**expression**<br>(必选)|字符串   使资|清单字段匹配的表达式.|
|**inventory\_link**<br>(必选)|整数     主<br>|资产清单字段ID.<br>参考 [host inventory object](/manual/api/reference/host/object#host_inventory) 支持的资产清单字段列表.|
|iconmapid|字符串   *(|读)* 图标映射所属的图标拓扑图ID.|
|sortorder|整数     *|只读)* 在图标拓扑图中图标映射的位置.|

[comment]: # ({/new-e88b40af})
