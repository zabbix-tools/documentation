[comment]: # translation:outdated

[comment]: # ({new-94ac227e})
# 创建

[comment]: # ({/new-94ac227e})

[comment]: # ({new-ac3800fa})
### 描述

`object iconmap.create(object/array iconMaps)`

此方法允许创建新的图标拓扑

[comment]: # ({/new-ac3800fa})

[comment]: # ({new-ca8671b9})
### 参数

`(对象/数组)` 要创建的图标拓扑.

另外，对于[标准图标拓扑图属性](object#icon_map)，此方法接受以下参数:

|参数            类|描述|<|
|---------------------|------|-|
|**mappings**<br>(必选)|数组   为|标拓扑创建图标映射.|

[comment]: # ({/new-ca8671b9})

[comment]: # ({new-9dbb2dc6})
### 返回值

`(对象)`
返回一个对象其中包含在iconmapids属性下已创建图标拓扑图的ID。返回ID的命令与传递图标拓扑图的命令匹配.

[comment]: # ({/new-9dbb2dc6})

[comment]: # ({new-b41637d2})
### 示例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-39a9c07f})
#### 创建一个图标拓扑图

创建一个图标拓扑图来显示不同类型的主机.

请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "iconmap.create",
    "params": {
        "name": "Type icons",
        "default_iconid": "2",
        "mappings": [
            {
                "inventory_link": 1,
                "expression": "server",
                "iconid": "3"
            },
            {
                "inventory_link": 1,
                "expression": "switch",
                "iconid": "4"
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "iconmapids": [
            "2"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-39a9c07f})

[comment]: # ({new-8df05cf6})
### 参考

-   [Icon mapping](object#icon_mapping)

[comment]: # ({/new-8df05cf6})

[comment]: # ({new-25aae98d})
### 来源

CIconMap::create() in
*frontends/php/include/classes/api/services/CIconMap.php*.

[comment]: # ({/new-25aae98d})
