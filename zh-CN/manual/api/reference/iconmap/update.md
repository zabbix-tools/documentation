[comment]: # translation:outdated

[comment]: # ({new-b7611ecd})
# 更新

[comment]: # ({/new-b7611ecd})

[comment]: # ({new-15ec7450})
### 描述

`object iconmap.update(object/array iconMaps)`

此方法允许更新已存在的图标拓扑.

[comment]: # ({/new-15ec7450})

[comment]: # ({new-6f92ef73})
### 参数

`(对象/数组)` 要更新的图标拓扑的属性.

每一个图标拓扑图的iconmapid属性必须已定义过，其他属性为可选项。仅被传递的属性会被更新，其他属性保持不变.

|参数       类|描述|<|
|----------------|------|-|
|mappings|数组   替|当前图标映射.|

[comment]: # ({/new-6f92ef73})

[comment]: # ({new-a1dbbc92})
### 返回值

`(object)` 返回一个对象其中包含在`iconmapids`属性下已更新图标拓扑图的ID.

[comment]: # ({/new-a1dbbc92})

[comment]: # ({new-b41637d2})
### 示例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-7013c996})
#### 重命名图标拓扑图

将图标拓扑图重命名为"OS icons".

请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "iconmap.update",
    "params": {
        "iconmapid": "1",
        "name": "OS icons"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "iconmapids": [
            "1"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-7013c996})

[comment]: # ({new-8df05cf6})
### 参考

-   [Icon mapping](object#icon_mapping)

[comment]: # ({/new-8df05cf6})

[comment]: # ({new-f35a7b0b})
### 来源

CIconMap::update() in
*frontends/php/include/classes/api/services/CIconMap.php*.

[comment]: # ({/new-f35a7b0b})
