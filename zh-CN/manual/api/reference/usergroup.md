[comment]: # translation:outdated

[comment]: # ({new-f92dbd28})
# User group \[用户组\]

This class is designed to work with user groups.

Object references:\

-   [User group](/manual/api/reference/usergroup/object#user_group)

Available methods:\

-   [usergroup.create](/manual/api/reference/usergroup/create) -
    creating new user groups
-   [usergroup.delete](/manual/api/reference/usergroup/delete) -
    deleting user groups
-   [usergroup.get](/manual/api/reference/usergroup/get) - retrieving
    user groups
-   [usergroup.update](/manual/api/reference/usergroup/update) -
    updating user groups

[comment]: # ({/new-f92dbd28})
