[comment]: # translation:outdated

[comment]: # ({new-517ea62c})
# 主机原型

该类被设计用来处理主机原型.

对象引用:\

-   [Host
    prototype](/manual/api/reference/hostprototype/object#host_prototype)
-   [Host prototype
    inventory](/manual/api/reference/hostprototype/object#host_prototype_inventory)
-   [Group link](/manual/api/reference/hostprototype/object#group_link)
-   [Group
    prototype](/manual/api/reference/hostprototype/object#group_prototype)

可用方法:\

-   [hostprototype.create](/manual/api/reference/hostprototype/create) -
    创建新的主机原型
-   [hostprototype.delete](/manual/api/reference/hostprototype/delete) -
    删除主机原型
-   [hostprototype.get](/manual/api/reference/hostprototype/get) -
    获取主机原型
-   [hostprototype.update](/manual/api/reference/hostprototype/update) -
    更新主机原型

[comment]: # ({/new-517ea62c})
