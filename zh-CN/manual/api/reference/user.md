[comment]: # translation:outdated

[comment]: # ({new-392125b2})
# User \[用户\]

该类用于用户的使用.

对象引用:\

-   [User](/manual/api/reference/user/object#user)

可用的方法:\

-   [user.create](/manual/api/reference/user/create) - 创建新用户
-   [user.delete](/manual/api/reference/user/delete) - 删除用户
-   [user.get](/manual/api/reference/user/get) - 检索用户
-   [user.login](/manual/api/reference/user/login) - 登录到API
-   [user.logout](/manual/api/reference/user/logout) - 退出API
-   [user.update](/manual/api/reference/user/update) - 更新用户

[comment]: # ({/new-392125b2})
