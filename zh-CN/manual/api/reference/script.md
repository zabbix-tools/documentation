[comment]: # translation:outdated

[comment]: # ({new-d373938d})
# Script 脚本

This class is designed to work with scripts. 这个类设计工作于脚本。

Object references:\

-   [Script](/manual/api/reference/script/object#script)

Available methods:\

-   [script.create](/manual/api/reference/script/create) - create new
    scripts
-   [script.delete](/manual/api/reference/script/delete) - delete
    scripts
-   [script.execute](/manual/api/reference/script/execute) - run scripts
-   [script.get](/manual/api/reference/script/get) - retrieve scripts
-   [script.getscriptsbyhosts](/manual/api/reference/script/getscriptsbyhosts) -
    retrieve scripts for hosts
-   [script.update](/manual/api/reference/script/update) - update
    scripts

[comment]: # ({/new-d373938d})
