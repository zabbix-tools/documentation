[comment]: # translation:outdated

[comment]: # ({new-ad84ea6b})
# 获取

[comment]: # ({/new-ad84ea6b})

[comment]: # ({new-385acf3d})
### 描述

`整数/数组 history.get(object parameters)`

该方法允许根据给定的参数检索历史数据 参考: [known
issues](/manual/installation/known_issues#api)

[comment]: # ({/new-385acf3d})

[comment]: # ({new-ddac6e9c})
### 参数

`(object)` 定义期望输出的参数.

该方法支持以下参数：

|参数                     类|描述|<|
|------------------------------|------|-|
|history|整数          要|回的历史对象类型.<br><br>可能的值:<br>0 - 数字浮点;<br>1 - 字符串;<br>2 - 日志;<br>3 - 无符号数字;<br>4 - 文本.<br><br>默认值: 3.|
|hostids|字符串/数组   只返回给|主机的历史记录|
|itemids|字符串/数组   只返回给|监控项的历史记录|
|time\_from|时间戳        仅返|在给定时间时或之后收到的值|
|time\_till|时间戳        仅返|在给定时间时或之前收到的值|
|sortfield|字符串/数组   按照给定|属性对结果进行排序<br><br>可能的值: `itemid` 或 `clock`|
|countOutput|布尔值        这些|数对于所有get方法都是通用的，详细描述可参考： [reference commentary](/manual/api/reference_commentary#common_get_method_parameters)|
|editable|布尔值        ::|<|
|excludeSearch|布尔值        ::|<|
|filter|对象          :|:|
|limit|整数          :|:|
|output|查询          :|:|
|search|对象          :|:|
|searchByAny|布尔值        ::|<|
|searchWildcardsEnabled|布尔值        ::|<|
|sortorder|字符串/数组   :::|<|
|startSearch|布尔值        ::|<|

[comment]: # ({/new-ddac6e9c})

[comment]: # ({new-7223bab1})
### 返回值

`(整数/数组)` 返回:

-   一组对象
-   如果使用了`countOutput`参数，返回检索对象的数量

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### 示例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-cf7bb886})
#### 获取监控项历史数据

从数字(浮点)监控项中获取最近10条数据

请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "history.get",
    "params": {
        "output": "extend",
        "history": 0,
        "itemids": "23296",
        "sortfield": "clock",
        "sortorder": "DESC",
        "limit": 10
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "itemid": "23296",
            "clock": "1351090996",
            "value": "0.0850",
            "ns": "563157632"
        },
        {
            "itemid": "23296",
            "clock": "1351090936",
            "value": "0.1600",
            "ns": "549216402"
        },
        {
            "itemid": "23296",
            "clock": "1351090876",
            "value": "0.1800",
            "ns": "537418114"
        },
        {
            "itemid": "23296",
            "clock": "1351090816",
            "value": "0.2100",
            "ns": "522659528"
        },
        {
            "itemid": "23296",
            "clock": "1351090756",
            "value": "0.2150",
            "ns": "507809457"
        },
        {
            "itemid": "23296",
            "clock": "1351090696",
            "value": "0.2550",
            "ns": "495509699"
        },
        {
            "itemid": "23296",
            "clock": "1351090636",
            "value": "0.3600",
            "ns": "477708209"
        },
        {
            "itemid": "23296",
            "clock": "1351090576",
            "value": "0.3750",
            "ns": "463251343"
        },
        {
            "itemid": "23296",
            "clock": "1351090516",
            "value": "0.3150",
            "ns": "447947017"
        },
        {
            "itemid": "23296",
            "clock": "1351090456",
            "value": "0.2750",
            "ns": "435307141"
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-cf7bb886})

[comment]: # ({new-621f421c})
### 来源

CHistory::get() in
*frontends/php/include/classes/api/services/CHistory.php*.

[comment]: # ({/new-621f421c})
