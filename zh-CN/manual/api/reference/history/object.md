[comment]: # translation:outdated

[comment]: # ({new-57ba3541})
# > 历史对象

下列是与`history` API相关的对象。

::: noteclassic
历史对象会因为item的数据类型而有所不同.它们都是Zabbix
Server创建的，不能通过API进行修改
:::

[comment]: # ({/new-57ba3541})

[comment]: # ({new-399f1014})
### 浮点类型

浮点类型的历史对象具有以下属性：

|属性     类|描述|<|
|--------------|------|-|
|clock|时间戳   获取|的时间|
|itemid|字符串   相关|tem的ID|
|ns|整数     获|值时的纳秒|
|value|浮点     获|到的值|

[comment]: # ({/new-399f1014})

[comment]: # ({new-3e505499})
### 整数类型

整数类型的历史对象具有以下属性：

|属性     类|描述|<|
|--------------|------|-|
|clock|时间戳   获取|的时间|
|itemid|字符串   相关|tem的ID|
|ns|整数     获|值时的纳秒|
|value|整数     获|到的值|

[comment]: # ({/new-3e505499})

[comment]: # ({new-11561120})
### 字符串类型

字符串类型的历史对象具有以下属性：

|属性     类|描述|<|
|--------------|------|-|
|clock|时间戳   获取|的时间|
|itemid|字符串   相关|tem的ID|
|ns|整数     获|值时的纳秒|
|value|字符串   获取|的值|

[comment]: # ({/new-11561120})

[comment]: # ({new-8e4826ab})
### 文本类型

文本类型的历史对象具有以下属性：

|属性     类|描述|<|
|--------------|------|-|
|id|字符串   历史|录条目的ID|
|clock|时间戳   获取|的时间|
|itemid|字符串   相关|tem的ID|
|ns|整数     获|值时的纳秒|
|value|文本     获|到的值|

[comment]: # ({/new-8e4826ab})

[comment]: # ({new-0df964c4})
### 日志类型

日志类型的历史对象具有以下属性：

|属性         类|描述|<|
|------------------|------|-|
|id|字符串   历史|录条目的ID|
|clock|时间戳   获取|的时间|
|itemid|字符串   相关|tem的ID|
|logeventid|整数     W|ndows事件日志条目ID|
|ns|整数     获|值时的纳秒|
|severity|整数     W|ndows事件日志条目级别|
|source|字符串   Wi|dows事件日志条目源|
|timestamp|时间戳   Wi|dows事件日志输入时间|
|value|文本     获|到的值|

[comment]: # ({/new-0df964c4})
