[comment]: # translation:outdated

[comment]: # ({new-be32ecd0})
# 趋势

This class is designed to work with trend data. 此类用于处理趋势数据。

Object references: 对象引用：\

-   [Trend](/zh/manual/api/reference/trend/object#trend)

Available methods: 可用方法：\

-   [trend.get](/zh/manual/api/reference/trend/get) - retrieving trends
    检索趋势数据。

[comment]: # ({/new-be32ecd0})
