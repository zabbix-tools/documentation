[comment]: # translation:outdated

[comment]: # ({new-8674a4c3})
# Discovery rule 发现规则

## 发现规则

This class is designed to work with network discovery rules.
该类用于处理网络发现规则。

::: notetip
This API is meant to work with network discovery rules.
For the low-level discovery rules see the [LLD rule
API](discoveryrule).
::: <note
tip>此API旨在处理网络发现规则。对于低级别发现规则，请参考
[低级别发现规则API](discoveryrule).
:::

Object references:\
对象引用:\

-   [Discovery rule](/manual/api/reference/drule/object#discovery_rule)
-   [发现规则](/zh/manual/api/reference/drule/object#discovery_rule)

Available methods:\
可用方法:\

-   [drule.create](/manual/api/reference/drule/create) - create new
    discovery rules
-   [drule.delete](/manual/api/reference/drule/delete) - delete
    discovery rules
-   [drule.get](/manual/api/reference/drule/get) - retrieve discovery
    rules
-   [drule.update](/manual/api/reference/drule/update) - update
    discovery rules
-   [drule.create](/zh/manual/api/reference/drule/create) - 创建发现规则
-   [drule.delete](/zh/manual/api/reference/drule/delete) - 删除发现规则
-   [drule.get](/zh/manual/api/reference/drule/get) - 获取发现规则
-   [drule.update](/zh/manual/api/reference/drule/update) - 更新发现规则

[comment]: # ({/new-8674a4c3})
