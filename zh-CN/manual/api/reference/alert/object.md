[comment]: # translation:outdated

[comment]: # ({new-05585755})
# > Alert object

以下是 `alert` API 的使用方法

[comment]: # ({/new-05585755})

[comment]: # ({new-933fed24})
### 告警

::: noteclassic
Alerts 是由 Zabbix server 创建，无法通过 API
修改。
:::

alert 对象包含有关某些 action 操作是否已成功执行的信息，它具有以下特性。

|特性            类|描述|<|
|---------------------|------|-|
|alertid|string|Alert ID 值。|
|actionid|string|Alert 生成的 Action ID.|
|alerttype|integer|Alert 类型。<br><br>可能的值：<br>0 - 信息;<br>1 - 远程命令。|
|clock|timestamp|Alert 生成的时间。|
|error|string|Alert 发送信息或者执行一个命令产生的报错信息。|
|esc\_step|integer|生成 Alert 后 Action 的处理步骤。|
|eventid|string|触发 Action 的事件 ID。|
|mediatypeid|string|用于发送消息的报警媒介类型的 ID。|
|message|text|消息文本。用于消息告警。|
|retries|integer|Zabbix 尝试发送消息的次数。|
|sendto|string|地址，用户名或接收者的其他标识符。用于消息告警。|
|status|integer|显示 action 操作是否已执行成功的状态。<br><br>消息告警的可能值：<br>0 - 消息未发送。<br>1 - 消息已发送。<br>2 - 经多次重试后失败。<br>3 - action 管理器尚未处理新警报。<br><br>命令告警的可能值：<br>0 - 命令没有运行。<br>1 - 命令运行成功。<br>2 - 尝试在 Zabbix agent 上运行命令但不可用。|
|subject|string|消息主题。用于消息告警。|
|userid|string|邮件发送到的用户的 ID。|
|p\_eventid|string|生成告警的异常事件 ID。|
|acknowledgeid|string|生成告警的确认 ID。|

The following objects are directly related to the `alert` API.

### Alert

::: noteclassic
Alerts are created by the Zabbix server and cannot be
modified via the API.
:::

The alert object contains information about whether certain action
operations have been executed successfully. It has the following
properties.

|Property|Type|Description|
|--------|----|-----------|
|alertid|string|ID of the alert.|
|actionid|string|ID of the action that generated the alert.|
|alerttype|integer|Alert type.<br><br>Possible values:<br>0 - message;<br>1 - remote command.|
|clock|timestamp|Time when the alert was generated.|
|error|string|Error text if there are problems sending a message or running a command.|
|esc\_step|integer|Action escalation step during which the alert was generated.|
|eventid|string|ID of the event that triggered the action.|
|mediatypeid|string|ID of the media type that was used to send the message.|
|message|text|Message text. Used for message alerts.|
|retries|integer|Number of times Zabbix tried to send the message.|
|sendto|string|Address, user name or other identifier of the recipient. Used for message alerts.|
|status|integer|Status indicating whether the action operation has been executed successfully.<br><br>Possible values for message alerts:<br>0 - message not sent.<br>1 - message sent.<br>2 - failed after a number of retries.<br>3 - new alert is not yet processed by alert manager.<br><br>Possible values for command alerts:<br>0 - command not run.<br>1 - command run.<br>2 - tried to run the command on the Zabbix agent but it was unavailable.|
|subject|string|Message subject. Used for message alerts.|
|userid|string|ID of the user that the message was sent to.|
|p\_eventid|string|ID of problem event, which generated the alert.|
|acknowledgeid|string|ID of acknowledgement, which generated the alert.|

[comment]: # ({/new-933fed24})
