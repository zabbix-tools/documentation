[comment]: # translation:outdated

[comment]: # ({new-c40e8cd1})
# usergroup.create

[comment]: # ({/new-c40e8cd1})

[comment]: # ({new-c74b5711})
### 说明

`object usergroup.create(object/array userGroups)`
此方法允许创建新的用户组。

[comment]: # ({/new-c74b5711})

[comment]: # ({new-3b20a75b})
### Description

`object usergroup.create(object/array userGroups)`

This method allows to create new user groups.

[comment]: # ({/new-3b20a75b})

[comment]: # ({new-19e4f0cc})
### 参数

`(object/array)` 要创建的用户组。

除了[标准用户组属性](object#user_group)之外, 该方法接受以下参数。

|属性           类|说明|<|
|--------------------|------|-|
|rights|object/array|分配给组的权限|
|tag\_filters|array|基于标签的权限分配给组|
|userids|string/array|要添加到用户组的用户的ID。|

[comment]: # ({/new-19e4f0cc})

[comment]: # ({new-b41637d2})
### Parameters

`(object/array)` User groups to create.

Additionally to the [standard user group properties](object#user_group),
the method accepts the following parameters.

|Parameter|Type|Description|
|---------|----|-----------|
|rights|object/array|Permissions to assign to the group|
|tag\_filters|array|Tag based permissions to assign to the group|
|userids|string/array|IDs of users to add to the user group.|

[comment]: # ({/new-b41637d2})

[comment]: # ({new-61abd39a})
### 返回值

`(object)` 返回包含“usrgrpids”属性下创建的用户组的ID的对象。
返回的ID的顺序与传递的用户组的顺序相匹配。

[comment]: # ({/new-61abd39a})

[comment]: # ({new-d5c7ed4f})
### Return values

`(object)` Returns an object containing the IDs of the created user
groups under the `usrgrpids` property. The order of the returned IDs
matches the order of the passed user groups.

[comment]: # ({/new-d5c7ed4f})

[comment]: # ({new-2d7f8c71})
### 示例

#### 创建一个用户组

创建一个用户组，拒绝访问主机组“2”，并向其添加用户。

### Examples

#### Creating a user group

Create a user group, which denies access to host group "2", and add a
user to it.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "usergroup.create",
    "params": {
        "name": "Operation managers",
        "rights": {
            "permission": 0,
            "id": "2"
        },
        "userids": "12"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "usrgrpids": [
            "20"
        ]
    },
    "id": 1
}
```

### 参见

-   [Permission](object#permission)

### 来源

CUserGroup::create() in
*frontends/php/include/classes/api/services/CUserGroup.php*.

### See also

-   [Permission](object#permission)

### Source

CUserGroup::create() in
*frontends/php/include/classes/api/services/CUserGroup.php*.

[comment]: # ({/new-2d7f8c71})
