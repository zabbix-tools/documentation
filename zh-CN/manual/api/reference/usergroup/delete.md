[comment]: # translation:outdated

[comment]: # ({new-7e2bc778})
# usergroup.delete

[comment]: # ({/new-7e2bc778})

[comment]: # ({new-c1ef47a5})
### 说明

`object usergroup.delete(array userGroupIds)`

此方法允许删除用户组。

[comment]: # ({/new-c1ef47a5})

[comment]: # ({new-22b6416d})
### 参数

`(array)` 要删除的用户组的ID。

[comment]: # ({/new-22b6416d})

[comment]: # ({new-dde57d64})
### 返回值

`(object)` 返回包含“usrgrpids”属性下删除的用户组的ID的对象。

[comment]: # ({/new-dde57d64})

[comment]: # ({new-b41637d2})
### Description

`object usergroup.delete(array userGroupIds)`

This method allows to delete user groups.

[comment]: # ({/new-b41637d2})

[comment]: # ({new-156418d8})
### Parameters

`(array)` IDs of the user groups to delete.

[comment]: # ({/new-156418d8})

[comment]: # ({new-9a88f22f})
### Return values

`(object)` Returns an object containing the IDs of the deleted user
groups under the `usrgrpids` property.

### 示例

#### 删除多个用户组

### Examples

#### Deleting multiple user groups

删除2个用户。

Delete two user groups.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "usergroup.delete",
    "params": [
        "20",
        "21"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "usrgrpids": [
            "20",
            "21"
        ]
    },
    "id": 1
}
```

### 来源

CUserGroup::delete() in
*frontends/php/include/classes/api/services/CUserGroup.php*.

[comment]: # ({/new-9a88f22f})
