[comment]: # translation:outdated

[comment]: # ({new-119c5103})
# > 用户组对象

以下对象与 `usergroup` 直接相关。

[comment]: # ({/new-119c5103})

[comment]: # ({new-3c2622e5})
## > User group object

The following objects are directly related to the `usergroup` API.

[comment]: # ({/new-3c2622e5})

[comment]: # ({new-ba46c802})
### 用户组

用户组对象具有以下属性。

|属性            类|说明|<|
|---------------------|------|-|
|usrgrpid|string|*(readonly)* 用户组的ID。|
|**name**<br>(required)|string|用户组的名称。|
|debug\_mode|integer|是否启用或禁用调试模式。<br><br>可能的值:<br>0 - *(default)* 禁用;<br>1 - 启用。|
|gui\_access|integer|组中用户的前端身份验证方法。<br><br>可能的值:<br>0 - *(default)* 使用系统默认身份验证方法;<br>1 - 使用内部认证;<br>2 - 禁止访问前端。|
|users\_status|integer|用户组是启用还是禁用。<br><br>可能的值:<br>0 - *(default)* 启用;<br>1 - 禁用。|

[comment]: # ({/new-ba46c802})

[comment]: # ({new-33ad932c})
### User group

The user group object has the following properties.

|Property|Type|Description|
|--------|----|-----------|
|usrgrpid|string|*(readonly)* ID of the user group.|
|**name**<br>(required)|string|Name of the user group.|
|debug\_mode|integer|Whether debug mode is enabled or disabled.<br><br>Possible values are:<br>0 - *(default)* disabled;<br>1 - enabled.|
|gui\_access|integer|Frontend authentication method of the users in the group.<br><br>Possible values:<br>0 - *(default)* use the system default authentication method;<br>1 - use internal authentication;<br>2 - disable access to the frontend.|
|users\_status|integer|Whether the user group is enabled or disabled.<br><br>Possible values are:<br>0 - *(default)* enabled;<br>1 - disabled.|

### 权限

权限对象具有以下属性。

|属性              类|说明|<|
|-----------------------|------|-|
|**id**<br>(required)|string|要添加权限的主机组的ID。|
|**permission**<br>(required)|integer|访问到主机组的级别。 \\\\可能的值：<br>0 - 拒绝访问;<br>2 - 只读访问;<br>3 -读写访问。|

### Permission

The permission object has the following properties.

|Property|Type|Description|
|--------|----|-----------|
|**id**<br>(required)|string|ID of the host group to add permission to.|
|**permission**<br>(required)|integer|Access level to the host group.<br><br>Possible values:<br>0 - access denied;<br>2 - read-only access;<br>3 - read-write access.|

### 基于标签的权限

基于标签的权限对象具有以下属性。

|属性           类|说明|<|
|--------------------|------|-|
|**groupid**<br>(required)|string|要添加权限的主机组的ID。|
|**tag**|string|标签名。|
|**value**|string|标签值。|

### Tag based permission

The tag based permission object has the following properties.

|Property|Type|Description|
|--------|----|-----------|
|**groupid**<br>(required)|string|ID of the host group to add permission to.|
|**tag**|string|Tag name.|
|**value**|string|Tag value.|

[comment]: # ({/new-33ad932c})
