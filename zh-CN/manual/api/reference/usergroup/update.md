[comment]: # translation:outdated

[comment]: # ({new-9d4a1084})
# usergroup.update

[comment]: # ({/new-9d4a1084})

[comment]: # ({new-9fdfde6c})
### 说明

`object usergroup.update(object/array userGroups)`

此方法允许更新现有的用户组。

[comment]: # ({/new-9fdfde6c})

[comment]: # ({new-93a5d635})
### Description

`object usergroup.update(object/array userGroups)`

This method allows to update existing user groups.

[comment]: # ({/new-93a5d635})

[comment]: # ({new-c04afd7f})
### 参数

`(object/array)`要更新的用户组属性。

必须为每个用户组定义“usrgrpid”属性，所有其他属性都是可选的。
只有通过的属性将被更新，所有其他属性将保持不变。

除了[标准用户组属性](object#user_group)之外, 该方法接受以下参数。

|属性           类|说明|<|
|--------------------|------|-|
|rights|object/array|更改分配给用户组的当前权限的权限。|
|tag\_filters|array|基于标记的权限以分配给组。|
|userids|string/array|用户的ID替换组中的用户。|

[comment]: # ({/new-c04afd7f})

[comment]: # ({new-b41637d2})
### Parameters

`(object/array)` User group properties to be updated.

The `usrgrpid` property must be defined for each user group, all other
properties are optional. Only the passed properties will be updated, all
others will remain unchanged.

Additionally to the [standard user group properties](object#user_group),
the method accepts the following parameters.

|Parameter|Type|Description|
|---------|----|-----------|
|rights|object/array|Permissions to replace the current permissions assigned to the user group.|
|tag\_filters|array|Tag based permissions to assign to the group|
|userids|string/array|IDs of the users to replace the users in the group.|

[comment]: # ({/new-b41637d2})

[comment]: # ({new-e1a75e7b})
### 返回值

`(object)` 返回包含“usrgrpids”属性下更新的用户组的ID的对象。

[comment]: # ({/new-e1a75e7b})

[comment]: # ({new-d5c7ed4f})
### Return values

`(object)` Returns an object containing the IDs of the updated user
groups under the `usrgrpids` property.

[comment]: # ({/new-d5c7ed4f})

[comment]: # ({new-09e213c5})
### 示例

#### 禁用用户组

禁用用户组。

### Examples

#### Disabling a user group

Disable a user group.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "usergroup.update",
    "params": {
        "usrgrpid": "17",
        "users_status": "1"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "usrgrpids": [
            "17"
        ]
    },
    "id": 1
}
```

### 参考

-   [Permission](object#permission)

### 来源

CUserGroup::update() in
*frontends/php/include/classes/api/services/CUserGroup.php*.

[comment]: # ({/new-09e213c5})
