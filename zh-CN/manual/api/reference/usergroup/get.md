[comment]: # translation:outdated

[comment]: # ({new-ce8d2bd2})
# usergroup.get

[comment]: # ({/new-ce8d2bd2})

[comment]: # ({new-ef79b8a9})
### 说明

`integer/array usergroup.get(object parameters)`

该方法允许根据给定的参数检索用户组。

[comment]: # ({/new-ef79b8a9})

[comment]: # ({new-4be9c06e})
### Description

`integer/array usergroup.get(object parameters)`

The method allows to retrieve user groups according to the given
parameters.

[comment]: # ({/new-4be9c06e})

[comment]: # ({new-7223bab1})
### 参数

`(object)` 定义所需输出的参数。

该方法支持以下参数。

|属性                     类|说明|<|
|------------------------------|------|-|
|status|integer|只返回具有给定状态的用户组。<br><br>请参阅[用户组页面](object#user_group)以获取支持的状态列表。|
|userids|string/array|只返回包含给定用户的用户组。|
|usrgrpids|string/array|只返回具有给定ID的用户组。|
|with\_gui\_access|integer|只返回具有给定前端身份验证方法的用户组。<br><br>有关支持的方法的列表，请参阅 [用户组页面](object#user_group)。|
|selectTagFilters|query|Return user group tag based permissions in the `tag_filters` property.<br><br>It has the following properties:<br>`groupid` - (string) ID of the host group;<br>`tag` - (string) tag name;<br>`value` - (string) tag value.|
|selectUsers|query|在“users”属性中返回用户组中的用户。|
|selectRights|query|在“权限”属性中返回用户组权限。<br><br>它具有以下属性：<br>`权限` - （整数）访问级别到主机组;<br>`id` - （string）主机组的ID。<br><br>有关主机组的访问级别列表，请参阅[用户组页面](object#permission)。|
|limitSelects|integer|限制子选择返回的记录数。|
|sortfield|string/array|按照给定的属性对结果进行排序。<br><br>可能的值为：`usrgrpid`，`name`。|
|countOutput|flag|[参考文献](/manual/api/reference_commentary#common_get_method_parameters)中详细描述了所有“获得”方法的常用参数。.|
|editable|boolean|^|
|excludeSearch|flag|^|
|filter|object|^|
|limit|integer|^|
|output|query|^|
|preservekeys|flag|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|flag|^|

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|Type|Description|
|---------|----|-----------|
|status|integer|Return only user groups with the given status.<br><br>Refer to the [user group page](object#user_group) for a list of supported statuses.|
|userids|string/array|Return only user groups that contain the given users.|
|usrgrpids|string/array|Return only user groups with the given IDs.|
|with\_gui\_access|integer|Return only user groups with the given frontend authentication method.<br><br>Refer to the [user group page](object#user_group) for a list of supported methods.|
|selectTagFilters|query|Return user group tag based permissions in the `tag_filters` property.<br><br>It has the following properties:<br>`groupid` - (string) ID of the host group;<br>`tag` - (string) tag name;<br>`value` - (string) tag value.|
|selectUsers|query|Return the users from the user group in the `users` property.|
|selectRights|query|Return user group rights in the `rights` property.<br><br>It has the following properties:<br>`permission` - (integer) access level to the host group;<br>`id` - (string) ID of the host group.<br><br>Refer to the [user group page](object#permission) for a list of access levels to host groups.|
|limitSelects|integer|Limits the number of records returned by subselects.|
|sortfield|string/array|Sort the result by the given properties.<br><br>Possible values are: `usrgrpid`, `name`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters).|
|editable|boolean|^|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

[comment]: # ({/new-b41637d2})

[comment]: # ({new-62cd5d8e})
### 返回值

`(integer/array)` 返回：

-   一组对象;
-   如果已经使用“countOutput”参数，则检索到的对象的计数。

[comment]: # ({/new-62cd5d8e})

[comment]: # ({new-039ccba1})
### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.

[comment]: # ({/new-039ccba1})

[comment]: # ({new-e72b2ac9})
### 示例

#### 检索已启用的用户组

检索所有已启用的用户组。

### Examples

#### Retrieving enabled user groups

Retrieve all enabled user groups.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "usergroup.get",
    "params": {
        "output": "extend",
        "status": 0
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "usrgrpid": "7",
            "name": "Zabbix administrators",
            "gui_access": "0",
            "users_status": "0",
            "debug_mode": "1"
        },
        {
            "usrgrpid": "8",
            "name": "Guests",
            "gui_access": "0",
            "users_status": "0",
            "debug_mode": "0"
        },
        {
            "usrgrpid": "11",
            "name": "Enabled debug mode",
            "gui_access": "0",
            "users_status": "0",
            "debug_mode": "1"
        },
        {
            "usrgrpid": "12",
            "name": "No access to the frontend",
            "gui_access": "2",
            "users_status": "0",
            "debug_mode": "0"
        },
        {
            "usrgrpid": "14",
            "name": "Read only",
            "gui_access": "0",
            "users_status": "0",
            "debug_mode": "0"
        },
        {
            "usrgrpid": "18",
            "name": "Deny",
            "gui_access": "0",
            "users_status": "0",
            "debug_mode": "0"
        }
    ],
    "id": 1
}
```

### 参见

-   [User](/manual/api/reference/user/object#user)

### 来源

CUserGroup::get() in
*frontends/php/include/classes/api/services/CUserGroup.php*.

[comment]: # ({/new-e72b2ac9})
