[comment]: # translation:outdated

[comment]: # ({new-c7072ca6})
# 获取

[comment]: # ({/new-c7072ca6})

[comment]: # ({new-12701833})
### Description 说明

`integer/array triggerprototype.get(object parameters)`

The method allows to retrieve trigger prototypes according to the given
parameters. 此方法允许根据指定的参数检索触发器原型。

[comment]: # ({/new-12701833})

[comment]: # ({new-14171778})
### Parameters 参数

`(object)` Parameters defining the desired output.
`(object)`定义需要输出的参数。 The method supports the following
parameters. 该方法支持以下参数。

|Parameter 参数           T|pe 类型                  Des|ription 说明|
|----------------------------|------------------------------|--------------|
|active|flag 标记                  R|turn only enabled trigger prototypes that belong to monitored hosts. 仅返回所属被监控主机的已启用触发器原型。|
|applicationids|string/array 字符串/数组   Retu|n only trigger prototypes that contain items from the given applications. 仅返回来自指定应用集中包含监控项的触发器原型。|
|discoveryids|string/array 字符串/数组   Retu|n only trigger prototypes that belong to the given LLD rules. 仅返回所属指定低级别发现规则的触发器原型。|
|functions|string/array 字符串/数组   Retu|n only triggers that use the given functions. 仅返回使用指定函数的触发器。<br><br>Refer to the [supported trigger functions](/manual/appendix/triggers/functions) page for a list of supported functions. 有关支持的功能列表，请参阅[supported trigger functions](/zh/manual/appendix/triggers/functions) 页面。|
|group|string 字符串              Re|urn only trigger prototypes that belong to hosts from the host groups with the given name. 仅返回来自指定名称的主机组中所属主机的触发器原型。|
|groupids|string/array 字符串/数组   Retu|n only trigger prototypes that belong to hosts from the given host groups. 仅返回来自指定主机组中所属主机的触发器原型。|
|host|string 字符串              Re|urn only trigger prototypes that belong to hosts with the given name. 仅返回指定名称的所属主机的触发器原型。|
|hostids|string/array 字符串/数组   Retu|n only trigger prototypes that belong to the given hosts. 仅返回指定主机所属的触发器原型。|
|inherited|boolean 布尔值             If|set to `true` return only trigger prototypes inherited from a template. 仅返回从模板继承的触发器原型，如果设置为`true`。|
|maintenance|boolean 布尔值             If|set to `true` return only enabled trigger prototypes that belong to hosts in maintenance. 仅返回在维护中所属主机的已启用触发器原型，如果设置为`true`。|
|min\_severity|integer 整数型             Re|urn only trigger prototypes with severity greater or equal than the given severity. 仅返回严重级别大于或等于指定严重级别的触发器原型。|
|monitored|flag 标记                  R|turn only enabled trigger prototypes that belong to monitored hosts and contain only enabled items. 仅返回所属被监控主机的已启用触发器原型，并包含已启用的监控项。|
|templated|boolean 布尔值             If|set to `true` return only trigger prototypes that belong to templates. 仅返回所属模板的触发器原型，如果设置为`true`。|
|templateids|string/array 字符串/数组   Retu|n only trigger prototypes that belong to the given templates. 仅返回指定模板所属的触发器原型。|
|triggerids|string/array 字符串/数组   Retu|n only trigger prototypes with the given IDs. 仅返回指定ID的触发器原型。|
|expandExpression|flag 标记                  E|pand functions and macros in the trigger expression. 展开在触发器原型表达式中的函数和宏。|
|selectDiscoveryRule|query 查询                 R|turn the LLD rule that the trigger prototype belongs to. 返回触发器原型所属的低级别发现规则。|
|selectFunctions|query 查询                 R|turn functions used in the trigger prototype in the `functions` property. 返回在`functions`属性中在触发器中使用的函数。<br><br>The function objects represents the functions used in the trigger expression and has the following properties: 函数对象代表使用在触发器表达式中的函数，并具有以下属性：<br>`functionid` - *(string 字符串)* ID of the function; 函数的ID；<br>`itemid` - *(string 字符串)* ID of the item used in the function; 使用在函数中的监控项ID；<br>`function` - *(string 字符串)* name of the function; 函数的名称；<br>`parameter` - *(string 字符串)* parameter passed to the function. 传递给函数的参数。|
|selectGroups|query 查询                 R|turn the host groups that the trigger prototype belongs to in the `groups` property. 返回在`groups`属性中触发器原型所属的主机组。|
|selectHosts|query 查询                 R|turn the hosts that the trigger prototype belongs to in the `hosts` property. 返回在`hosts`属性中触发器所属的主机。|
|selectItems|query 查询                 R|turn items and item prototypes used the trigger prototype in the `items` property. 返回在`items`属性中触发器所包含的监控项。|
|selectDependencies|query 查询                 R|turn trigger prototypes and triggers that the trigger prototype depends on in the `dependencies` property. 返回在`dependencies`属性中依赖触发器原型的触发器原型和触发器。|
|selectTags|query 查询                 R|turn the trigger prototype tags in `tags` property. 返回在`tags`属性中触发器原型标签。|
|filter|object 对象                R|turn only those results that exactly match the given filter. 仅返回与指定筛选完全匹配的结果。<br><br>Accepts an array, where the keys are property names, and the values are either a single value or an array of values to match against. 接受一个数组，其中键为属性名称，值为单个值或要匹配值的数组。<br><br>Supports additional filters: 支持额外的筛选：<br>`host` - technical name of the host that the trigger prototype belongs to; 触发器原型所属主机的正式名称。<br>`hostid` - ID of the host that the trigger prototype belongs to. 触发器原型所属主机的ID。|
|limitSelects|integer 整数型             Li|its the number of records returned by subselects. 限制子查询返回的记录数量。<br><br>Applies to the following subselects: 适用于以下子查询：<br>`selectHosts` - results will be sorted by `host`. 以`host`分类结果。|
|sortfield|string/array 字符串/数组   Sort|the result by the given properties. 由指定属性分类结果。<br><br>Possible values are: `triggerid`, `description`, `status` and `priority`. 许可值为：`triggerid`, `description`, `status`和`priority`。|
|countOutput|boolean 布尔值             Th|se parameters being common for all `get` methods are described in detail in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters). 这些参数十分普遍，适用于所有get方法，详情可参考[reference commentary](/zh/manual/api/reference_commentary#common_get_method_parameters)。|
|editable|boolean 布尔值             ::|<|
|excludeSearch|boolean 布尔值             ::|<|
|limit|integer 整数型             ::|<|
|output|query 查询                 :|:|
|preservekeys|boolean 布尔值             ::|<|
|search|object 对象                :|:|
|searchByAny|boolean 布尔值             ::|<|
|searchWildcardsEnabled|boolean 布尔值             ::|<|
|sortorder|string/array 字符串/数组   :::|<|
|startSearch|boolean 布尔值             ::|<|

[comment]: # ({/new-14171778})

[comment]: # ({new-7223bab1})
### Return values 返回值

`(integer/array)` Returns either: 返回两者其中任一：

-   an array of objects; 一组对象；
-   the count of retrieved objects, if the `countOutput` parameter has
    been used. 如果已经使用了`countOutput`参数，则检索对象的计数。

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Examples 范例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-81de28c1})
#### Retrieve trigger prototypes from an LLD rule 从低级别发现规则中检索触发器原型

Retrieve all trigger prototypes and their functions from an LLD rule.
从低级别发现规则中检索所有的触发器原型和相关函数。

Request 请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "triggerprototype.get",
    "params": {
        "output": "extend",
        "selectFunctions": "extend",
        "discoveryids": "22450"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response 响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "functions": [
                {
                    "functionid": "12598",
                    "itemid": "22454",
                    "function": "last",
                    "parameter": "0"
                }
            ],
            "triggerid": "13272",
            "expression": "{12598}<20",
            "description": "Free inodes is less than 20% on volume {#FSNAME}",
            "url": "",
            "status": "0",
            "priority": "2",
            "comments": "",
            "templateid": "0",
            "type": "0",
            "flags": "2",
            "recovery_mode": "0",
            "recovery_expression": "",
            "correlation_mode": "0",
            "correlation_tag": "",
            "manual_close": "0"
        },
        {
            "functions": [
                {
                    "functionid": "13500",
                    "itemid": "22686",
                    "function": "last",
                    "parameter": "0"
                }
            ],
            "triggerid": "13266",
            "expression": "{13500}<201",
            "description": "Free disk space is less than 20% on volume {#FSNAME}",
            "url": "",
            "status": "0",
            "priority": "2",
            "comments": "",
            "templateid": "0",
            "type": "0",
            "flags": "2",
            "recovery_mode": "0",
            "recovery_expression": "",
            "correlation_mode": "0",
            "correlation_tag": "",
            "manual_close": "0"
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-81de28c1})

[comment]: # ({new-2cb80f2d})
#### Retrieving a specific trigger prototype with tags 根据标签检索特定的触发器原型

Request 请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "triggerprototype.get",
    "params": {
        "output": [
            "triggerid",
            "description"
        ]
        "selectTags": "extend",
        "triggerids": [
            "17373"
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response 响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "triggerid": "17373",
            "description": "Free disk space is less than 20% on volume {#FSNAME}",
            "tags": [
                {
                    "tag": "volume",
                    "value": "{#FSNAME}"
                },
                {
                    "tag": "type",
                    "value": "{#FSTYPE}"
                }
            ]
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-2cb80f2d})

[comment]: # ({new-222d46a0})
### See also 参考

-   [Discovery
    rule](/zh/manual/api/reference/discoveryrule/object#discovery_rule)
-   [Item](/zh/manual/api/reference/item/object#item)
-   [Host](/zh/manual/api/reference/host/object#host)
-   [Host group](/zh/manual/api/reference/hostgroup/object#host_group)

[comment]: # ({/new-222d46a0})

[comment]: # ({new-1136c1c7})
### Source 源码

CTriggerPrototype::get() in
*frontends/php/include/classes/api/services/CTriggerPrototype.php*.
CTriggerPrototype::get()方法可在*frontends/php/include/classes/api/services/CTriggerPrototype.php*中参考。

[comment]: # ({/new-1136c1c7})
