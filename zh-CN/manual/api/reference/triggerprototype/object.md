[comment]: # translation:outdated

[comment]: # ({new-86ab9f61})
# > 对象

The following objects are directly related to the `triggerprototype`
API. 以下对象与`triggerprototype`API直接相关。

[comment]: # ({/new-86ab9f61})

[comment]: # ({new-bdd02d4b})
### Trigger 触发器

The trigger prototype object has the following properties.
触发器原型对象包含以下属性。

|Property 属性          T|pe 类型        Des|ription 说明|
|--------------------------|--------------------|--------------|
|triggerid|string 字符串    *(|eadonly 只读)* ID of the trigger prototype. 触发器原型的ID。|
|**description**<br>(required 必须)|string 字符串    Na|e of the trigger prototype. 触发器原型的名称。|
|**expression**<br>(required 必须)|string 字符串    Re|uced trigger expression. 生成的触发器表达式。|
|comments|string 字符串    Ad|itional comments to the trigger prototype. 触发器原型的附加注释。|
|priority|integer 整数型   Se|erity of the trigger prototype. 触发器原型的严重级别。<br><br>Possible values: 许可值：<br>0 - *(default 默认)* not classified; 未分类；<br>1 - information; 信息；<br>2 - warning; 警告；<br>3 - average; 一般严重；<br>4 - high; 严重；<br>5 - disaster. 灾难。|
|status|integer 整数型   Wh|ther the trigger prototype is enabled or disabled. 触发器原型是否在启用状态或禁用状态。<br><br>Possible values: 许可值：<br>0 - *(default 默认)* enabled; 已启用；<br>1 - disabled. 已禁用。|
|templateid|string 字符串    *(|eadonly 只读)* ID of the parent template trigger prototype. 触发器原型父模板的ID。|
|type|integer 整数型   Wh|ther the trigger prototype can generate multiple problem events. 触发器原型是否可以生成多个异常事件。<br><br>Possible values: 许可值：<br>0 - *(default 默认)* do not generate multiple events; 不生成多个事件；<br>1 - generate multiple events. 生成多个事件。|
|url|string 字符串    UR|associated with the trigger prototype. 关联到触发器原型的URL。|
|recovery\_mode|integer 整数型   OK|event generation mode. 正常事件生成模式。<br><br>Possible values are: 许可值为：<br>0 - *(default 默认)* Expression; 表达式；<br>1 - Recovery expression; 恢复表达式；<br>2 - None. 无。|
|recovery\_expression|string 字符串    Re|uced trigger recovery expression. 生成的触发器恢复表达式。|
|correlation\_mode|integer 整数型   OK|event closes. 正常事件关闭。<br><br>Possible values are: 许可值为：<br>0 - *(default 默认)* All problems; 所有异常；<br>1 - All problems if tag values match. 匹配标签值的所有异常。|
|correlation\_tag|string 字符串    Ta|for matching. 匹配的标签。|
|manual\_close|integer 整数型   Al|ow manual close. 允许手动关闭。<br><br>Possible values are: 许可值为：<br>0 - *(default 默认)* No; 不允许；<br>1 - Yes. 允许。|

[comment]: # ({/new-bdd02d4b})


[comment]: # ({new-efc78bd6})
### Trigger prototype tag

The trigger prototype tag object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**tag**<br>(required)|string|Trigger prototype tag name.|
|value|string|Trigger prototype tag value.|

[comment]: # ({/new-efc78bd6})
