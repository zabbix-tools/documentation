[comment]: # translation:outdated

[comment]: # ({new-be70f0cc})
# 更新

[comment]: # ({/new-be70f0cc})

[comment]: # ({new-842b5955})
### Description 说明

`object triggerprototype.update(object/array triggerPrototypes)`

This method allows to update existing trigger prototypes.
此方法允许更新已有的触发器原型。

[comment]: # ({/new-842b5955})

[comment]: # ({new-827e4920})
### Parameters 参数

`(object/array)` [Trigger prototype
properties](object#trigger_prototype) to be updated.
`(object/array)`需要更新的触发器原型[Trigger prototype
properties](object#trigger_prototype)。 The `triggerid` property must be
defined for each trigger prototype, all other properties are optional.
Only the passed properties will be updated, all others will remain
unchanged.
`triggerid`属性必须在每个触发器原型中已定义，其他所有属性为可选项。只有传递过去的属性会被更新，其他所有属性仍然保持不变。
Additionally to the [standard trigger prototype
properties](object#trigger_prototype) the method accepts the following
parameters. 除[standard trigger prototype
properties](object#trigger_prototype)之外，该方法接受以下参数。

|Parameter 参数   T|pe 类型    Des|ription 说明|
|--------------------|----------------|--------------|
|dependencies|array 数组   T|iggers and trigger prototypes that the trigger prototype is dependent on. 依赖触发器原型的触发器和触发器原型。<br><br>The triggers must have the `triggerid` property defined. 触发器必须已定义`triggerid`属性。|
|tags|array 数组   T|igger prototype tags. 触发器标签。|

::: noteimportant
The trigger expression has to be given in its
expanded form and must contain at least one item prototype.
指定的触发器表达式必须为展开式，并且必须包含至少一个监控项原型。
:::

[comment]: # ({/new-827e4920})

[comment]: # ({new-200d7b9d})
### Return values 返回值

`(object)` Returns an object containing the IDs of the updated trigger
prototypes under the `triggerids` property.
`(object)`返回一个对象，该对象包含在`triggerids`属性中已更新触发器原型的ID。

[comment]: # ({/new-200d7b9d})

[comment]: # ({new-b41637d2})
### Examples 范例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-bcca41dc})
#### Enabling a trigger prototype 启用触发器原型

Enable a trigger prototype, that is, set its status to 0.
启用一个触发器原型，即将其状态设置为0。

Request 请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "triggerprototype.update",
    "params": {
        "triggerid": "13938",
        "status": 0
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response 响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "triggerids": [
            "13938"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-bcca41dc})

[comment]: # ({new-724af3ce})
#### Replacing trigger prototype tags 替换触发器原型标签

Replace tags for one trigger prototype. 为触发器原型替换标签。

Request 请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "triggerprototype.update",
    "params": {
        "triggerid": "17373",
        "tags": [
            {
                "tag": "volume",
                "value": "{#FSNAME}"
            },
            {
                "tag": "type",
                "value": "{#FSTYPE}"
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response 响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "triggerids": [
            "17373"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-724af3ce})

[comment]: # ({new-585ae96a})
### Source 源码

CTriggerPrototype::update() in
*frontends/php/include/classes/api/services/CTriggerPrototype.php*.
CTriggerPrototype::update()方法可在*frontends/php/include/classes/api/services/CTriggerPrototype.php*中参考。

[comment]: # ({/new-585ae96a})
