[comment]: # translation:outdated

[comment]: # ({new-c233abea})
# 删除

[comment]: # ({/new-c233abea})

[comment]: # ({new-4ed080b7})
### Description 说明

`object triggerprototype.delete(array triggerPrototypeIds)`

This method allows to delete trigger prototypes.
此方法允许删除触发器原型。

[comment]: # ({/new-4ed080b7})

[comment]: # ({new-93be9503})
### Parameters 参数

`(array)` IDs of the trigger prototypes to delete.
`(array)`需要删除的触发器原型ID。

[comment]: # ({/new-93be9503})

[comment]: # ({new-91b09722})
### Return values 返回值

`(object)` Returns an object containing the IDs of the deleted trigger
prototypes under the `triggerids` property.
`(object)`返回一个对象，该对象包含在`triggerids`属性中已删除触发器原型的ID。

[comment]: # ({/new-91b09722})

[comment]: # ({new-b41637d2})
### Examples 范例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-ed4ab6a8})
#### Deleting multiple trigger prototypes 删除多个触发器原型

Delete two trigger prototypes. 删除两个触发器原型。

Request 请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "triggerprototype.delete",
    "params": [
        "12002",
        "12003"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Response 响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "triggerids": [
            "12002",
            "12003"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-ed4ab6a8})

[comment]: # ({new-c4ac1758})
### Source 源码

CTriggerPrototype::delete() in
*frontends/php/include/classes/api/services/CTriggerPrototype.php*.
CTriggerPrototype::delete()方法可在*frontends/php/include/classes/api/services/CTriggerPrototype.php*中参考。

[comment]: # ({/new-c4ac1758})
