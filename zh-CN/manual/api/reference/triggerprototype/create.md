[comment]: # translation:outdated

[comment]: # ({new-49f9e765})
# 创建

[comment]: # ({/new-49f9e765})

[comment]: # ({new-c66bc66b})
### Description

`object triggerprototype.create(object/array triggerPrototypes)`

This method allows to create new trigger prototypes.

[comment]: # ({/new-c66bc66b})

[comment]: # ({new-e93145a6})
### Parameters 参数

`(object/array)` Trigger prototypes to create.
`(object/array)`需要创建的触发器原型。 Additionally to the [standard
trigger prototype properties](object#trigger_prototype) the method
accepts the following parameters. 除[standard trigger prototype
properties](object#trigger_prototype)之外，此方法还接受以下参数。

|Parameter 参数   T|pe 类型    Des|ription 说明|
|--------------------|----------------|--------------|
|dependencies|array 数组   T|iggers and trigger prototypes that the trigger prototype is dependent on. 依赖触发器原型的触发器和触发器原型。<br><br>The triggers must have the `triggerid` property defined. 触发器必须已定义`triggerid`属性。|
|tags|array 数组   T|igger prototype tags. 触发器原型标签。|

::: noteimportant
The trigger expression has to be given in its
expanded form and must contain at least one item prototype.
指定的触发器表达式必须为展开式，并且必须包含至少一个监控项原型。
:::

[comment]: # ({/new-e93145a6})

[comment]: # ({new-8d1f4c4d})
### Return values 返回值

`(object)` Returns an object containing the IDs of the created trigger
prototypes under the `triggerids` property. The order of the returned
IDs matches the order of the passed trigger prototypes.
`(object)`返回一个对象，该对象包含在`triggerids`属性中已创建触发器原型的ID，返回ID的顺序与传递触发器原型的顺序相匹配。

[comment]: # ({/new-8d1f4c4d})

[comment]: # ({new-b41637d2})
### Examples 范例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-87a61295})
#### Creating a trigger prototype 创建触发器原型

Create a trigger prototype to detect when a file system has less than
20% free disk space. 创建一个触发器原型来检测磁盘剩余空间是否小于20%。

Request 请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "triggerprototype.create",
    "params": {
        "description": "Free disk space is less than 20% on volume {#FSNAME}",
        "expression": "{Zabbix server:vfs.fs.size[{#FSNAME},pfree].last()}<20",
        "tags": [
            {
                "tag": "volume",
                "value": "{#FSNAME}"
            },
            {
                "tag": "type",
                "value": "{#FSTYPE}"
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response 响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "triggerids": [
            "17372"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-87a61295})

[comment]: # ({new-43cf23db})
### Source 源码

CTriggerPrototype::create() in
*frontends/php/include/classes/api/services/CTriggerPrototype.php*.
CTriggerPrototype::create()方法可在*frontends/php/include/classes/api/services/CTriggerPrototype.php*中参考。

[comment]: # ({/new-43cf23db})
