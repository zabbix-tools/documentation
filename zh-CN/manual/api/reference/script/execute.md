[comment]: # translation:outdated

[comment]: # ({new-3a2a8d58})
# 脚本执行

[comment]: # ({/new-3a2a8d58})

[comment]: # ({new-60a9dbe3})
### Description 描述

`object script.execute(object parameters)`

This method allows to run a script on a host.
此方法允许在一个主机上运行一个脚本

[comment]: # ({/new-60a9dbe3})

[comment]: # ({new-e4fa8ac7})
### Parameters 参数

`(object)` Parameters containing the ID of the script to run and the ID
of the host. `(object)` 参数包含要运行脚本的id和主机的id

|Parameter|Type|Description|
|---------|----|-----------|
|**hostid**<br>(required)|string|ID of the host to run the script on. 要运行脚本的主机id|
|**scriptid**<br>(required)|string|ID of the script to run. 要运行脚本的脚本id|

[comment]: # ({/new-e4fa8ac7})

[comment]: # ({new-dde54e60})
### Return values 返回值

`(object)` Returns the result of script execution. `(object)`
返回脚本执行的结果

|Property|Type|Description|
|--------|----|-----------|
|response|string|Whether the script was run successfully.<br><br>Possible values: `success` or `failed`. 脚本是否执行成功<br><br>可能的值 ：`成功` 或 `失败`|
|value|string|Script output. 脚本的输出结果|

[comment]: # ({/new-dde54e60})

[comment]: # ({new-b41637d2})
### Examples 示例如下

[comment]: # ({/new-b41637d2})

[comment]: # ({new-03cb1cf3})
#### Run a script 运行一个脚本

Run a "ping" script on a host. 在一个主机上运行一个 "ping"脚本

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "script.execute",
    "params": {
        "scriptid": "1",
        "hostid": "30079"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "response": "success",
        "value": "PING 127.0.0.1 (127.0.0.1) 56(84) bytes of data.\n64 bytes from 127.0.0.1: icmp_req=1 ttl=64 time=0.074 ms\n64 bytes from 127.0.0.1: icmp_req=2 ttl=64 time=0.030 ms\n64 bytes from 127.0.0.1: icmp_req=3 ttl=64 time=0.030 ms\n\n--- 127.0.0.1 ping statistics ---\n3 packets transmitted, 3 received, 0% packet loss, time 1998ms\nrtt min/avg/max/mdev = 0.030/0.044/0.074/0.022 ms\n"
    },
    "id": 1
}
```

[comment]: # ({/new-03cb1cf3})

[comment]: # ({new-baebff97})
### Source

CScript::execute() in
*frontends/php/include/classes/api/services/CScript.php*.

[comment]: # ({/new-baebff97})


[comment]: # ({new-e7dc45a5})
### Source

CScript::execute() in *ui/include/classes/api/services/CScript.php*.

[comment]: # ({/new-e7dc45a5})
