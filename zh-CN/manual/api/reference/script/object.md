[comment]: # translation:outdated

[comment]: # ({new-8b1700c1})
# > 对象

The following objects are directly related to the `script` API.
以下对象直接关联到`script` API.

[comment]: # ({/new-8b1700c1})

[comment]: # ({new-9c6db006})
### 脚本

The script object has the following properties. 这个脚本对象拥有以下属性

|Property|Type|Description|
|--------|----|-----------|
|scriptid|string|*(readonly)* ID of the script. 脚本的ID|
|**command**<br>(required)|string|Command to run. 运行命令|
|**name**<br>(required)|string|Name of the script. 脚本名称|
|confirmation|string|Confirmation pop up text. The pop up will appear when trying to run the script from the Zabbix frontend. 确认弹出文本信息，如果尝试在zabbix界面运行脚本，将会弹出文本信息。|
|description|string|Description of the script. 脚本描述|
|execute\_on|integer|Where to run the script.<br><br>Possible values:<br>0 - run on Zabbix agent;<br>1 - run on Zabbix server.<br>2 - *(default)* run on Zabbix server (proxy). 哪儿可以去运行这个脚本<br><br>可能的值：<br>0 - 运行在zabbix agent 1 - 运行在zabbix server 2 - *(默认)* 运行在zabbix server或zabbix proxy|
|groupid|string|ID of the host group that the script can be run on. If set to 0, the script will be available on all host groups.<br><br>Default: 0. 可以运行脚本主机组的ID，如果设置为0，这个脚本适用于所有的主机组|
|host\_access|integer|Host permissions needed to run the script.<br><br>Possible values:<br>2 - *(default)* read;<br>3 - write. 运行脚本主机的权限<br><br>可能的值 ：<br>2 - *(默认)* 读 3 - 写|
|type|integer|Script type.<br><br>Possible values:<br>0 - *(default)* script;<br>1 - IPMI. 脚本类型<br><br>可能的值：<br>0 - *(默认)* 脚本 1 - IPMI|
|usrgrpid|string|ID of the user group that will be allowed to run the script. If set to 0, the script will be available for all user groups.<br><br>Default: 0. 允许运行脚本的用户组的ID，如果设置为0，这个脚本适用于所有的的用户组|

[comment]: # ({/new-9c6db006})




[comment]: # ({new-05b0e37d})
### Webhook parameters

Parameters passed to webhook script when it is called have the following
properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**name**<br>(required)|string|Parameter name.|
|value|string|Parameter value. Supports [macros](/manual/appendix/macros/supported_by_location).|

[comment]: # ({/new-05b0e37d})

[comment]: # ({new-587d2fd0})
### Debug

Debug information of executed webhook script. The debug object has the
following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|logs|array|Array of [log entries](/manual/api/reference/script/object#Log entry).|
|ms|string|Script execution duration in milliseconds.|

[comment]: # ({/new-587d2fd0})

[comment]: # ({new-8ea23127})
### Log entry

The log entry object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|level|integer|Log level.|
|ms|string|The time elapsed in milliseconds since the script was run before log entry was added.|
|message|string|Log message.|

[comment]: # ({/new-8ea23127})
