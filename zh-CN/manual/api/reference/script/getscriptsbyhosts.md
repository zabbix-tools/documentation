[comment]: # translation:outdated

[comment]: # ({new-e5a46c74})
# script.getscriptsbyhosts 通过主机获取脚本

[comment]: # ({/new-e5a46c74})

[comment]: # ({new-dfaa829b})
### Description 描述

`object script.getscriptsbyhosts(array hostIds)`

This method allows to retrieve scripts available on the given hosts.
此方法允许检索适用所给主机的脚本

[comment]: # ({/new-dfaa829b})

[comment]: # ({new-080ad0f7})
### Parameters 参数

`(string/array)` IDs of hosts to return scripts for. `(string/array)`
主机IDs

[comment]: # ({/new-080ad0f7})

[comment]: # ({new-1c891d1a})
### Return values 返回值

`(object)` Returns an object with host IDs as properties and arrays of
available scripts as values. `(object)`
返回一个对象，该对象的主机id作为属性，而可用脚本的数组作为值。 <note
tip>The method will automatically expand macros in the `confirmation`
text.
::: <note
tip>该方法将在`confirmation`文本中自动扩展宏。
:::

[comment]: # ({/new-1c891d1a})

[comment]: # ({new-b41637d2})
### Examples 示例如下

[comment]: # ({/new-b41637d2})

[comment]: # ({new-4b630539})
#### Retrieve scripts by host IDs 通过主机的ids检索脚本

Retrieve all scripts available on hosts "30079" and "30073".
检索所有适用于主机"30079" 和"30073" 的脚本。

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "script.getscriptsbyhosts",
    "params": [
        "30079",
        "30073"
    ],
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "30079": [
            {
                "scriptid": "3",
                "name": "Detect operating system",
                "command": "sudo /usr/bin/nmap -O {HOST.CONN} 2>&1",
                "host_access": "2",
                "usrgrpid": "7",
                "groupid": "0",
                "description": "",
                "confirmation": "",
                "type": "0",
                "execute_on": "1",
                "hostid": "10001"
            },
            {
                "scriptid": "1",
                "name": "Ping",
                "command": "/bin/ping -c 3 {HOST.CONN} 2>&1",
                "host_access": "2",
                "usrgrpid": "0",
                "groupid": "0",
                "description": "",
                "confirmation": "",
                "type": "0",
                "execute_on": "1",
                "hostid": "10001"
            },
            {
                "scriptid": "2",
                "name": "Traceroute",
                "command": "/usr/bin/traceroute {HOST.CONN} 2>&1",
                "host_access": "2",
                "usrgrpid": "0",
                "groupid": "0",
                "description": "",
                "confirmation": "",
                "type": "0",
                "execute_on": "1",
                "hostid": "10001"
            }
        ],
        "30073": [
            {
                "scriptid": "3",
                "name": "Detect operating system",
                "command": "sudo /usr/bin/nmap -O {HOST.CONN} 2>&1",
                "host_access": "2",
                "usrgrpid": "7",
                "groupid": "0",
                "description": "",
                "confirmation": "",
                "type": "0",
                "execute_on": "1",
                "hostid": "10001"
            },
            {
                "scriptid": "1",
                "name": "Ping",
                "command": "/bin/ping -c 3 {HOST.CONN} 2>&1",
                "host_access": "2",
                "usrgrpid": "0",
                "groupid": "0",
                "description": "",
                "confirmation": "",
                "type": "0",
                "execute_on": "1",
                "hostid": "10001"
            },
            {
                "scriptid": "2",
                "name": "Traceroute",
                "command": "/usr/bin/traceroute {HOST.CONN} 2>&1",
                "host_access": "2",
                "usrgrpid": "0",
                "groupid": "0",
                "description": "",
                "confirmation": "",
                "type": "0",
                "execute_on": "1",
                "hostid": "10001"
            }
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-4b630539})

[comment]: # ({new-a62657e4})
### Source

CScript::getScriptsByHosts() in
*frontends/php/include/classes/api/services/CScript.php*.

[comment]: # ({/new-a62657e4})
