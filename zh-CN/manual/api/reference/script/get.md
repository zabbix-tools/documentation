[comment]: # translation:outdated

[comment]: # ({new-cd91b3fe})
# 获取

[comment]: # ({/new-cd91b3fe})

[comment]: # ({new-96ec37bc})
### Description 描述

`integer/array script.get(object parameters)`

The method allows to retrieve scripts according to the given parameters.
此方法允许检索符合所给参数的脚本

[comment]: # ({/new-96ec37bc})

[comment]: # ({new-c425a65a})
### Parameters 参数

`(object)` Parameters defining the desired output. `(object)`
定义所需输出的参数。

The method supports the following parameters. 此方法支持以下参数

|Parameter|Type|Description|
|---------|----|-----------|
|groupids|string/array|Return only scripts that can be run on the given host groups. 仅能运行在所给主机组的脚本|
|hostids|string/array|Return only scripts that can be run on the given hosts. 仅能运行在所给主机的脚本|
|scriptids|string/array|Return only scripts with the given IDs. 仅返回所给IDs的脚本|
|usrgrpids|string/array|Return only scripts that can be run by users in the given user groups. 仅返回所给用户组可以运行的脚本|
|selectGroups|query|Return host groups that the script can be run on in the `groups` property. 返回可以在`groups`属性中运行脚本的主机组。|
|selectHosts|query|Return hosts that the script can be run on in the `hosts` property. 返回可以在`hosts`属性中运行脚本的主机组。|
|sortfield|string/array|Sort the result by the given properties.<br><br>Possible values are: `scriptid` and `name`. 根据所给参数对参数进行排序<br>\\\\可能的值：`scriptid` 和`name`|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters).|
|editable|boolean|^|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

[comment]: # ({/new-c425a65a})

[comment]: # ({new-7223bab1})
### Return values 返回值

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.
-   一个数组对象
-   检索到对象的数目，如果`countOutput`参数被使用

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Examples 示例如下

[comment]: # ({/new-b41637d2})

[comment]: # ({new-6d428f32})
#### Retrieve all scripts 检索所有脚本

Retrieve all configured scripts. 检索所有的已确认的脚本

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "script.get",
    "params": {
        "output": "extend"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [    
        {
            "scriptid": "1",
            "name": "Ping",
            "command": "/bin/ping -c 3 {HOST.CONN} 2>&1",
            "host_access": "2",
            "usrgrpid": "0",
            "groupid": "0",
            "description": "",
            "confirmation": "",
            "type": "0",
            "execute_on": "1"
        },
        {
            "scriptid": "2",
            "name": "Traceroute",
            "command": "/usr/bin/traceroute {HOST.CONN} 2>&1",
            "host_access": "2",
            "usrgrpid": "0",
            "groupid": "0",
            "description": "",
            "confirmation": "",
            "type": "0",
            "execute_on": "1"
        },
        {
            "scriptid": "3",
            "name": "Detect operating system",
            "command": "sudo /usr/bin/nmap -O {HOST.CONN} 2>&1",
            "host_access": "2",
            "usrgrpid": "7",
            "groupid": "0",
            "description": "",
            "confirmation": "",
            "type": "0",
            "execute_on": "1"
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-6d428f32})

[comment]: # ({new-b0b740ec})
### See also

-   [Host](/manual/api/reference/host/object#object_details)
-   [Host group](/manual/api/reference/hostgroup/object#object_details)

[comment]: # ({/new-b0b740ec})

[comment]: # ({new-90dfc753})
### Source

CScript::get() in
*frontends/php/include/classes/api/services/CScript.php*.

[comment]: # ({/new-90dfc753})
