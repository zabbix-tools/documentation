[comment]: # translation:outdated

[comment]: # ({new-ceca660a})
# 删除

[comment]: # ({/new-ceca660a})

[comment]: # ({new-db451e99})
### Description 描述

`object script.delete(array scriptIds)`

This method allows to delete scripts. 此方法允许去删除脚本

[comment]: # ({/new-db451e99})

[comment]: # ({new-17e2e442})
### Parameters 参数

`(array)` IDs of the scripts to delete. `(array)`返回删除脚本的IDs

[comment]: # ({/new-17e2e442})

[comment]: # ({new-64908342})
### Return values 返回值

`(object)` Returns an object containing the IDs of the deleted scripts
under the `scriptids` property. `(object)`
返回一个对象包含在`scriptids`属性之下删除的脚本

[comment]: # ({/new-64908342})

[comment]: # ({new-b41637d2})
### Examples 示例如下

[comment]: # ({/new-b41637d2})

[comment]: # ({new-8184d373})
#### Delete multiple scripts 删除多个脚本

Delete two scripts. 删除两个脚本。

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "script.delete",
    "params": [
        "3",
        "4"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "scriptids": [
            "3",
            "4"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-8184d373})

[comment]: # ({new-ccc98e5e})
### Source

CScript::delete() in
*frontends/php/include/classes/api/services/CScript.php*.

[comment]: # ({/new-ccc98e5e})
