[comment]: # translation:outdated

[comment]: # ({new-4f2ac53b})
# 更新

[comment]: # ({/new-4f2ac53b})

[comment]: # ({new-3bbc6786})
### Description 描述

`object script.update(object/array scripts)`

This method allows to update existing scripts. 此方法更新已存在的脚本

[comment]: # ({/new-3bbc6786})

[comment]: # ({new-8f315bab})
### Parameters 参数

`(object/array)` [Script properties](object#script) to be updated.

The `scriptid` property must be defined for each script, all other
properties are optional. Only the passed properties will be updated, all
others will remain unchanged.
`scriptid`属性必须被每个脚本定义，其他属性是可选择的。仅仅传递的参数会被更新，其他参数将保持不变。

[comment]: # ({/new-8f315bab})

[comment]: # ({new-5062432a})
### Return values 返回值

`(object)` Returns an object containing the IDs of the updated scripts
under the `scriptids` property.
`(object)`返回一个对象包含在`scriptids`属性下更新脚本的IDs

[comment]: # ({/new-5062432a})

[comment]: # ({new-b41637d2})
### Examples 示例如下

[comment]: # ({/new-b41637d2})

[comment]: # ({new-4f3506ff})
#### Change script command 改变一个脚本的命令

Change the command of the script to "/bin/ping -c 10 {HOST.CONN}
2>&1". 改变一个脚本的命令为"/bin/ping -c 10 {HOST.CONN} 2>&1".

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "script.update",
    "params": {
        "scriptid": "1",
        "command": "/bin/ping -c 10 {HOST.CONN} 2>&1"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "scriptids": [
            "1"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-4f3506ff})

[comment]: # ({new-cded8a11})
### Source

CScript::update() in
*frontends/php/include/classes/api/services/CScript.php*.

[comment]: # ({/new-cded8a11})
