[comment]: # translation:outdated

[comment]: # ({new-084d9051})
# user.update

[comment]: # ({/new-084d9051})

[comment]: # ({new-dbc24bad})
### Description

`object user.update(object/array users)`

这个方法允许更新存在的用户。

[comment]: # ({/new-dbc24bad})

[comment]: # ({new-3b478f18})
### Description

`object user.update(object/array users)`

This method allows to update existing users.

[comment]: # ({/new-3b478f18})

[comment]: # ({new-4c3fb877})
### Parameters

`(object/array)` 需要更新的用户属性。

必须为每个用户定义`userid`属性，所有其他属性都是可选的。只有传递的属性将被更新，其他所有的属性将保持不变。

此外, [standard user properties](object#user), 该方法接受以下参数。

|属性           类|说明|<|
|--------------------|------|-|
|passwd|string|用户的密码。|
|usrgrps|array|用户组来替换现有的用户组。<br><br>用户组ID必须是存在的 `usrgrpid`。|
|user\_medias|array|新的媒体用于替换旧的。|

[comment]: # ({/new-4c3fb877})

[comment]: # ({new-b41637d2})
### Parameters

`(object/array)` User properties to be updated.

The `userid` property must be defined for each user, all other
properties are optional. Only the passed properties will be updated, all
others will remain unchanged.

Additionally to the [standard user properties](object#user), the method
accepts the following parameters.

|Parameter|Type|Description|
|---------|----|-----------|
|passwd|string|User's password.|
|usrgrps|array|User groups to replace existing user groups.<br><br>The user groups must have the `usrgrpid` property defined.|
|user\_medias|array|Medias to replace existing medias.|

[comment]: # ({/new-b41637d2})

[comment]: # ({new-c0483e48})
### 返回值

`(object)` 在`userids`属性下,返回包含更新用户id对象。

[comment]: # ({/new-c0483e48})

[comment]: # ({new-7ae4c491})
### Return values

`(object)` Returns an object containing the IDs of the updated users
under the `userids` property.

[comment]: # ({/new-7ae4c491})

[comment]: # ({new-1083dc26})
### 示例

[comment]: # ({/new-1083dc26})

[comment]: # ({new-bbd5a07f})
### Examples

#### Renaming a user

把一个用户重命名为 John Doe.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "user.update",
    "params": {
        "userid": "1",
        "name": "John",
        "surname": "Doe"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "userids": [
            "1"
        ]
    },
    "id": 1
}
```

### 来源

CUser::update() in
*frontends/php/include/classes/api/services/CUser.php*.

[comment]: # ({/new-bbd5a07f})
