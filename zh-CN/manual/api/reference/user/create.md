[comment]: # translation:outdated

[comment]: # ({new-a79da37c})
# user.create

[comment]: # ({/new-a79da37c})

[comment]: # ({new-7dc5480b})
### 描述

`object user.create(object/array users)`

此方法允许创建新的用户。

[comment]: # ({/new-7dc5480b})

[comment]: # ({new-9e6b7fba})
### Description

`object user.create(object/array users)`

This method allows to create new users.

[comment]: # ({/new-9e6b7fba})

[comment]: # ({new-7e32f5f9})
### Parameters

`(object/array)` 要创建的用户.

该方法接受有 [标准用户属性](object#user)的用户。

|属性           类|说明|<|
|--------------------|------|-|
|**passwd**<br>*(required)*|string|用户密码。|
|**usrgrps**<br>(required)|array|用户添加到的组。<br><br>用户组必须有存在的 `usrgrpid` 属性定义。|
|user\_medias|array|为用户创建媒体。|

[comment]: # ({/new-7e32f5f9})

[comment]: # ({new-b41637d2})
### Parameters

`(object/array)` Users to create.

Additionally to the [standard user properties](object#user), the method
accepts the following parameters.

|Parameter|Type|Description|
|---------|----|-----------|
|**passwd**<br>*(required)*|string|User's password.|
|**usrgrps**<br>(required)|array|User groups to add the user to.<br><br>The user groups must have the `usrgrpid` property defined.|
|user\_medias|array|Medias to create for the user.|

[comment]: # ({/new-b41637d2})

[comment]: # ({new-0df8c129})
### 返回值

`(object)`返回一个包含创建值的ID的对象映射 `userids`
属性。返回的ID的顺序与传递的用户的顺序相匹配。

[comment]: # ({/new-0df8c129})

[comment]: # ({new-130699eb})
### Return values

`(object)` Returns an object containing the IDs of the created users
under the `userids` property. The order of the returned IDs matches the
order of the passed users.

[comment]: # ({/new-130699eb})

[comment]: # ({new-61de33e2})
### 示例

### Examples

#### 创建一个用户

创建一个新用户, 把用户加入用户组同时添加用户媒体。

#### Creating a user

Create a new user, add him to a user group and create a new media for
him.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "user.create",
    "params": {
        "alias": "John",
        "passwd": "Doe123",
        "usrgrps": [
            {
                "usrgrpid": "7"
            }
        ],
        "user_medias": [
            {
                "mediatypeid": "1",
                "sendto": [
                    "support@company.com"
                ],
                "active": 0,
                "severity": 63,
                "period": "1-7,00:00-24:00"
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "userids": [
            "12"
        ]
    },
    "id": 1
}
```

### 参考

-   [Media](/manual/api/reference/user/object#media)
-   [User group](/manual/api/reference/usergroup/object#user_group)

### 来源

CUser::create() in
*frontends/php/include/classes/api/services/CUser.php*.

[comment]: # ({/new-61de33e2})
