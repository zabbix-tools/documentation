[comment]: # translation:outdated

[comment]: # ({new-2485e0f7})
# user.get

[comment]: # ({/new-2485e0f7})

[comment]: # ({new-36f89e39})
### Description

`integer/array user.get(object parameters)`
此方法允许根据给定的参数检索用户。

[comment]: # ({/new-36f89e39})

[comment]: # ({new-888512e9})
### Description

`integer/array user.get(object parameters)`

The method allows to retrieve users according to the given parameters.

[comment]: # ({/new-888512e9})

[comment]: # ({new-7223bab1})
### Parameters

`(object)` 定义所需输出的参数。

该方法支持以下参数。

|属性                     类|说明|<|
|------------------------------|------|-|
|mediaids|string/array|只返回用户给定媒体。|
|mediatypeids|string/array|只返回用户给定媒体类型。|
|userids|string/array|只返回用户给定ID。|
|usrgrpids|string/array|只返回用户给定用户组ID。|
|getAccess|flag|添加关于用户权限附加信息。<br><br>为每个用户添加以下属性:<br>`gui_access` - *(integer)* 用户的前端认证方法。 参考 `gui_access` 的属性 关于[用户组对象](/manual/api/reference/usergroup/object#user_group) 列出可能的值。<br>`debug_mode` - *(integer)* 表明是否为用户启用了调试功能。 可能的值: 0 - 禁用调试, 1 - 开启调试。<br>`users_status` - *(integer)* 表示用户是否禁用。 可能的值: 0 - 用户可用, 1 - 用户禁用。|
|selectMedias|query|在`medias` 属性返回用户使用的媒体。|
|selectMediatypes|query|在 `mediatypes` 属性返回用户使用的媒体类型。|
|selectUsrgrps|query|在`usrgrps` 属性返回用户所属的组|
|sortfield|string/array|根据给定的属性对结果进行排序。<br><br>可能的值 : `userid` and `alias`.|
|countOutput|boolean|这些参数对于所有的`get`方法是常见的，在 [reference commentary](/manual/api/reference_commentary#common_get_method_parameters)中有详细描述.|
|editable|boolean|^|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### 返回值

`(integer/array)`返回:

-   一个对象数组;
-   检索对象的计数, 如果 `countOutput` 参数被使用。

[comment]: # ({/new-b41637d2})

[comment]: # ({new-7b5c610b})
### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|Type|Description|
|---------|----|-----------|
|mediaids|string/array|Return only users that use the given media.|
|mediatypeids|string/array|Return only users that use the given media types.|
|userids|string/array|Return only users with the given IDs.|
|usrgrpids|string/array|Return only users that belong to the given user groups.|
|getAccess|flag|Adds additional information about user permissions.<br><br>Adds the following properties for each user:<br>`gui_access` - *(integer)* user's frontend authentication method. Refer to the `gui_access` property of the [user group object](/manual/api/reference/usergroup/object#user_group) for a list of possible values.<br>`debug_mode` - *(integer)* indicates whether debug is enabled for the user. Possible values: 0 - debug disabled, 1 - debug enabled.<br>`users_status` - *(integer)* indicates whether the user is disabled. Possible values: 0 - user enabled, 1 - user disabled.|
|selectMedias|query|Return media used by the user in the `medias` property.|
|selectMediatypes|query|Return media types used by the user in the `mediatypes` property.|
|selectUsrgrps|query|Return user groups that the user belongs to in the `usrgrps` property.|
|sortfield|string/array|Sort the result by the given properties.<br><br>Possible values are: `userid` and `alias`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters).|
|editable|boolean|^|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

[comment]: # ({/new-7b5c610b})

[comment]: # ({new-d85f9654})
### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.

[comment]: # ({/new-d85f9654})

[comment]: # ({new-8d892781})
### 示例

[comment]: # ({/new-8d892781})

[comment]: # ({new-842f2a42})
### Examples

#### 检索用户

#### Retrieving users

检索所有已配置的用户。

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "user.get",
    "params": {
        "output": "extend"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "userid": "1",
            "alias": "Admin",
            "name": "Zabbix",
            "surname": "Administrator",
            "url": "",
            "autologin": "1",
            "autologout": "0s",
            "lang": "ru_RU",
            "refresh": "0s",
            "type": "3",
            "theme": "default",
            "attempt_failed": "0",
            "attempt_ip": "",
            "attempt_clock": "0",
            "rows_per_page": "50"
        },
        {
            "userid": "2",
            "alias": "guest",
            "name": "Default2",
            "surname": "User",
            "url": "",
            "autologin": "0",
            "autologout": "15m",
            "lang": "en_GB",
            "refresh": "30s",
            "type": "1",
            "theme": "default",
            "attempt_failed": "0",
            "attempt_ip": "",
            "attempt_clock": "0",
            "rows_per_page": "50"
        }
    ],
    "id": 1
}
```

### 参考

-   [媒体](/manual/api/reference/user/object#media)
-   [媒体类型](/manual/api/reference/mediatype/object#media_type)
-   [用户组](/manual/api/reference/usergroup/object#user_group)

### 来源

CUser::get() in *frontends/php/include/classes/api/services/CUser.php*.

[comment]: # ({/new-842f2a42})
