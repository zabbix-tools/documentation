[comment]: # translation:outdated

[comment]: # ({new-19701d17})
# user.login

[comment]: # ({/new-19701d17})

[comment]: # ({new-c647dda4})
### 说明

`string/object user.login(object parameters)`

这个方法用户登录API和获取一个认证票据。

::: notewarning
当使用这个方法的时候, 你必须使用
[user.logout](/manual/api/reference/user/logout)方法，防止产生大量的开放会话记录。.
:::

[comment]: # ({/new-c647dda4})

[comment]: # ({new-68f0a066})
### Description

`string/object user.login(object parameters)`

This method allows to log in to the API and generate an authentication
token.

::: notewarning
When using this method, you also need to do
[user.logout](/manual/api/reference/user/logout) to prevent the
generation of a large number of open session records.
:::

[comment]: # ({/new-68f0a066})

[comment]: # ({new-bc924c62})
### 参数

<note
important>这种方法对于未经身份验证的用户是可用的，并且必须在JSON-RPC请求中没有`auth`数调用。
:::

`(object)` 包含用户名和密码的参数。

该方法接受以下参数。

|属性            类|说明|<|
|---------------------|------|-|
|**password**<br>(required)|string|用户密码。 未使用的HTTP身份验证。|
|**user**<br>(required)|string|用户名。|
|userData|flag|R返回关于已认证用户的信息。|

<note
important>当使用HTTP认证时，API请求中的用户名必须与`授权`头中使用的名称相匹配。密码将不会被验证，并且可以省略。
:::

[comment]: # ({/new-bc924c62})

[comment]: # ({new-b41637d2})
### Parameters

::: noteimportant
This method is available to unauthenticated users
and must be called without the `auth` parameter in the JSON-RPC
request.
:::

`(object)` Parameters containing the user name and password.

The method accepts the following parameters.

|Parameter|Type|Description|
|---------|----|-----------|
|**password**<br>(required)|string|User password. Unused for HTTP authentication.|
|**user**<br>(required)|string|User name.|
|userData|flag|Return information about the authenticated user.|

::: noteimportant
When using HTTP authentication, the user name in
the API request must match the one used in the `Authorization` header.
The password will not be validated and can be omitted.
:::

[comment]: # ({/new-b41637d2})

[comment]: # ({new-46abbe5a})
### 返回值

`(string/object)`
如果使用`userDat`参数，则返回包含关于经过身份验证用户信息的对象。

关于[standard user properties](object#user), 返回以下信息:

|属性          类|说明|<|
|-------------------|------|-|
|debug\_mode|boolean|是否为用户启用了调试模式。|
|gui\_access|integer|用户的身份验证方法到前端。<br><br>Refer to the `gui_access` property of the [user group object](/manual/api/reference/usergroup/object#user_group) for a list of possible values.|
|sessionid|string|身份验证令牌，必须在下列API请求中使用。|
|userip|string|用户的IP地址。|

<note
tip>如果一个用户在一次或多次失败的尝试之后成功地进行了身份验证，该方法将返回`attempt_clock`、`尝试失败`和`尝试ip`属性的当前值，然后重新设置它们。
:::

如果不使用`userData`参数，该方法将返回身份验证令牌。

<note
tip>所生成的认证令牌必须存储，并在以下JSON-RPC请求的`auth`参数中使用。在使用HTTP认证时也需要它。
:::

[comment]: # ({/new-46abbe5a})

[comment]: # ({new-8448c8ab})
### Return values

`(string/object)` If the `userData` parameter is used, returns an object
containing information about the authenticated user.

Additionally to the [standard user properties](object#user), the
following information is returned:

|Property|Type|Description|
|--------|----|-----------|
|debug\_mode|boolean|Whether debug mode is enabled for the user.|
|gui\_access|integer|User's authentication method to the frontend.<br><br>Refer to the `gui_access` property of the [user group object](/manual/api/reference/usergroup/object#user_group) for a list of possible values.|
|sessionid|string|Authentication token, which must be used in the following API requests.|
|userip|string|IP address of the user.|

::: notetip
If a user has been successfully authenticated after one
or more failed attempts, the method will return the current values for
the `attempt_clock`, `attempt_failed` and `attempt_ip` properties and
then reset them.
:::

If the `userData` parameter is not used, the method returns an
authentication token.

::: notetip
The generated authentication token should be remembered
and used in the `auth` parameter of the following JSON-RPC requests. It
is also required when using HTTP authentication.
:::

[comment]: # ({/new-8448c8ab})

[comment]: # ({new-22a14fca})
### 示例

[comment]: # ({/new-22a14fca})

[comment]: # ({new-7fd9f53c})
### Examples

#### 认证一个用户

#### Authenticating a user

认证一个用户

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "user.login",
    "params": {
        "user": "Admin",
        "password": "zabbix"
    },
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": "0424bd59b807674191e7d77572075f33",
    "id": 1
}
```

#### Requesting authenticated user's information

Authenticate and return additional information about the user.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "user.login",
    "params": {
        "user": "Admin",
        "password": "zabbix",
        "userData": true
    },
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "userid": "1",
        "alias": "Admin",
        "name": "Zabbix",
        "surname": "Administrator",
        "url": "",
        "autologin": "1",
        "autologout": "0",
        "lang": "ru_RU",
        "refresh": "0",
        "type": "3",
        "theme": "default",
        "attempt_failed": "0",
        "attempt_ip": "127.0.0.1",
        "attempt_clock": "1355919038",
        "rows_per_page": "50",
        "debug_mode": true,
        "userip": "127.0.0.1",
        "sessionid": "5b56eee8be445e98f0bd42b435736e42",
        "gui_access": "0"
    },
    "id": 1
}
```

### 参考

-   [user.logout](logout)

### 来源

CUser::login() in
*frontends/php/include/classes/api/services/CUser.php*.

[comment]: # ({/new-7fd9f53c})
