[comment]: # translation:outdated

[comment]: # ({new-860084e0})
# user.logout

[comment]: # ({/new-860084e0})

[comment]: # ({new-d6ff52eb})
### 说明

`string/object user.logout(array)`

这个方法用于用户登出API与使当前认证令牌失效。

[comment]: # ({/new-d6ff52eb})

[comment]: # ({new-4fa8a419})
### Description

`string/object user.logout(array)`

This method allows to log out of the API and invalidates the current
authentication token.

[comment]: # ({/new-4fa8a419})

[comment]: # ({new-66018290})
### 参数

`(array)` 这个方法接受一个空数组。

[comment]: # ({/new-66018290})

[comment]: # ({new-b41637d2})
### Parameters

`(array)` The method accepts an empty array.

[comment]: # ({/new-b41637d2})

[comment]: # ({new-b5326b2b})
### 返回值

`(boolean)`如果用户已成功注销，则返回`true`。

[comment]: # ({/new-b5326b2b})

[comment]: # ({new-02f79c40})
### Return values

`(boolean)` Returns `true` if the user has been logged out successfully.

[comment]: # ({/new-02f79c40})

[comment]: # ({new-7fd9f53c})
### 示例

### Examples

#### 登出

#### Logging out

从API登出。

Log out from the API.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "user.logout",
    "params": [],
    "id": 1,
    "auth": "16a46baf181ef9602e1687f3110abf8a"
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": true,
    "id": 1
}
```

### 参考

-   [user.login](login)

### 来源

CUser::login() in
*frontends/php/include/classes/api/services/CUser.php*.

[comment]: # ({/new-7fd9f53c})
