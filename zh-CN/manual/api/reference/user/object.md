[comment]: # translation:outdated

[comment]: # ({new-0834cf88})
# > 用户对象

以下对象与 `user` API直接相关.

[comment]: # ({/new-0834cf88})

[comment]: # ({new-0ecc45be})
## > User object

The following objects are directly related to the `user` API.

[comment]: # ({/new-0ecc45be})

[comment]: # ({new-1ad487be})
### 用户

用户对象具有以下属性。

|属性              类|说明|<|
|-----------------------|------|-|
|userid|string|*(readonly)* 用户的ID。|
|**alias**<br>*(required)*|string|用户别名。|
|attempt\_clock|timestamp|*(readonly)* 最近一次登录失败的时间。|
|attempt\_failed|integer|*(readonly)* 最近失败的登录尝试次数。|
|attempt\_ip|string|*(readonly)* 最近一次失败的登录来源IP地址。|
|autologin|integer|允许自动登录。<br><br>可能的值:<br>0 - *(default)* 禁止自动登录;<br>1 -允许自动登录。|
|autologout|string|会话过期时间。 接受具有后缀的秒或时间单位。 如果设置为 0s, 用户登录会话永远不会过期。<br><br>默认: 15m.|
|lang|string|用户默认语言代码<br><br>默认: `en_GB`。|
|name|string|用户名.|
|refresh|string|自动刷新时间间隔. 接受具有后缀的秒或时间单位。<br><br>默认: 30s.|
|rows\_per\_page|integer|每页显示的对象行数。<br><br>Default: 50.|
|surname|string|姓。|
|theme|string|用户的主题。<br><br>可能的值:<br>`default` - *(default)* system default;<br>`blue-theme` - Blue;<br>`dark-theme` - Dark.|
|type|integer|用户类型。<br><br>Possible values:<br>1 - *(default)* Zabbix user;<br>2 - Zabbix admin;<br>3 - Zabbix super admin.|
|url|string|在登录后将用户重定向到页面的URL。|

### User

The user object has the following properties.

|Property|Type|Description|
|--------|----|-----------|
|userid|string|*(readonly)* ID of the user.|
|**alias**<br>*(required)*|string|User alias.|
|attempt\_clock|timestamp|*(readonly)* Time of the last unsuccessful login attempt.|
|attempt\_failed|integer|*(readonly)* Recent failed login attempt count.|
|attempt\_ip|string|*(readonly)* IP address from where the last unsuccessful login attempt came from.|
|autologin|integer|Whether to enable auto-login.<br><br>Possible values:<br>0 - *(default)* auto-login disabled;<br>1 - auto-login enabled.|
|autologout|string|User session life time. Accepts seconds and time unit with suffix. If set to 0s, the session will never expire.<br><br>Default: 15m.|
|lang|string|Language code of the user's language.<br><br>Default: `en_GB`.|
|name|string|Name of the user.|
|refresh|string|Automatic refresh period. Accepts seconds and time unit with suffix.<br><br>Default: 30s.|
|rows\_per\_page|integer|Amount of object rows to show per page.<br><br>Default: 50.|
|surname|string|Surname of the user.|
|theme|string|User's theme.<br><br>Possible values:<br>`default` - *(default)* system default;<br>`blue-theme` - Blue;<br>`dark-theme` - Dark.|
|type|integer|Type of the user.<br><br>Possible values:<br>1 - *(default)* Zabbix user;<br>2 - Zabbix admin;<br>3 - Zabbix super admin.|
|url|string|URL of the page to redirect the user to after logging in.|

### 媒体

媒体对象具有以下属性。

|属性               类|说明|<|
|------------------------|------|-|
|**mediatypeid**<br>(required)|string|用于媒体的媒体类型ID|
|**sendto**<br>(required)|string/array|地址, 用户名或者接收方的其他标识符。<br><br>如果类型是 [Media type](/manual/api/reference/mediatype/object#mediatype) 电子邮件, 值被设置为数组。 其他类型 [Media types](/manual/api/reference/mediatype/object#mediatype),值被设置为字符串。|
|**active**|integer|是否启用媒体。<br><br>可能的值:<br>0 - *(default)* enabled;<br>1 - disabled.|
|**severity**|integer|触发发送通知告警级别。<br><br>Severities are stored in binary form with each bit representing the corresponding severity. For example, 12 equals 1100 in binary and means, that notifications will be sent from triggers with severities warning and average.<br><br>Refer to the [trigger object page](/manual/api/reference/trigger/object#trigger) for a list of supported trigger severities.<br><br>Default: 63|
|**period**|string|当通知可以作为 [time period](/manual/appendix/time_period) 发送或者用分号隔开用户宏。<br><br>Default: 1-7,00:00-24:00|

### Media

The media object has the following properties.

|Property|Type|Description|
|--------|----|-----------|
|**mediatypeid**<br>(required)|string|ID of the media type used by the media.|
|**sendto**<br>(required)|string/array|Address, user name or other identifier of the recipient.<br><br>If type of [Media type](/manual/api/reference/mediatype/object#mediatype) is e-mail, values are represented as array. For other types of [Media types](/manual/api/reference/mediatype/object#mediatype), value is represented as a string.|
|**active**|integer|Whether the media is enabled.<br><br>Possible values:<br>0 - *(default)* enabled;<br>1 - disabled.|
|**severity**|integer|Trigger severities to send notifications about.<br><br>Severities are stored in binary form with each bit representing the corresponding severity. For example, 12 equals 1100 in binary and means, that notifications will be sent from triggers with severities warning and average.<br><br>Refer to the [trigger object page](/manual/api/reference/trigger/object#trigger) for a list of supported trigger severities.<br><br>Default: 63|
|**period**|string|Time when the notifications can be sent as a [time period](/manual/appendix/time_period) or user macros separated by a semicolon.<br><br>Default: 1-7,00:00-24:00|

[comment]: # ({/new-1ad487be})
