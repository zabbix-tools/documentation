[comment]: # translation:outdated

[comment]: # ({new-bab9094f})
# user.delete

[comment]: # ({/new-bab9094f})

[comment]: # ({new-1f1b6511})
### 说明

`object user.delete(array users)`

此方法允许删除用户。

[comment]: # ({/new-1f1b6511})

[comment]: # ({new-213fafcd})
### Description

`object user.delete(array users)`

This method allows to delete users.

[comment]: # ({/new-213fafcd})

[comment]: # ({new-17d395f4})
### Parameters

`(array)` 要删除用户ID。

[comment]: # ({/new-17d395f4})

[comment]: # ({new-b41637d2})
### Parameters

`(array)` IDs of users to delete.

[comment]: # ({/new-b41637d2})

[comment]: # ({new-783cb2af})
### Return values

`(object)` 返回一个包含 `userids` 属性下删除用户ID的对象。

[comment]: # ({/new-783cb2af})

[comment]: # ({new-8c5510d6})
### Return values

`(object)` Returns an object containing the IDs of the deleted users
under the `userids` property.

### 示例

### Examples

#### 删除多个用户

删除2个用户。

#### Deleting multiple users

Delete two users.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "user.delete",
    "params": [
        "1",
        "5"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "userids": [
            "1",
            "5"
        ]
    },
    "id": 1
}
```

### 来源

CUser::delete() in
*frontends/php/include/classes/api/services/CUser.php*.

[comment]: # ({/new-8c5510d6})
