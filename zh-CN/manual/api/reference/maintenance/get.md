[comment]: # translation:outdated

[comment]: # ({new-0d3af930})
# maintenance.get

[comment]: # ({/new-0d3af930})

[comment]: # ({new-a04dd4c9})
### Description 说明

`integer/array maintenance.get(object parameters)`

The method allows to retrieve maintenances according to the given
parameters. 此方法用于根据给定参数获取维护模式。

[comment]: # ({/new-a04dd4c9})

[comment]: # ({new-9ca798d7})
### Parameters 参数

`(object)` Parameters defining the desired output. `(object)`
定义期望输出的参数。

The method supports the following parameters. 此方法支持如下参数。

|Parameter|Type|Description|<|
|---------|----|-----------|-|
|groupids|string/array|Return only maintenances that are assigned to the given host groups. 仅返回指定到给定主机组的维护模式。|<|
|hostids|string/array|Return only maintenances that are assigned to the given hosts. 仅返回指定到给定主机的维护模式。|<|
|maintenanceids|string/array|Return only maintenances with the given IDs. 仅返回给定IDs的维护模式。|<|
|selectGroups|query|Return host groups assigned to the maintenance in the `groups` property. 在`group`属性中返回维护模式所指定的主机组。|<|
|selectHosts|query|Return hosts assigned to the maintenance in the `hosts` property. 在`host`属性中返回维护模式所指定的主机。|<|
|selectTimeperiods|query|Return the maintenance's time periods in the `timeperiods` property. 在`timeperiods`属性中返回维护模式的时间周期。|<|
|sortfield|string/array|Sort the result by the given properties. 根据给定的属性记性排序。<br><br>Possible values are: `maintenanceid`, `name` and `maintenance_type`. 可能的值有：, `name` and `maintenance_type`。|<|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/zh/manual/api/reference_commentary#common_get_method_parameters). 这些参数在[reference commentary](/zh/manual/api/reference_commentary#common_get_method_parameters)中详细描述的所有`get`方法是通用的。|<|
|editable|boolean|^|<|
|excludeSearch|boolean|^|<|
|filter|object|^|<|
|limit|integer|^|<|
|output|query|^|<|
|preservekeys|boolean|^|<|
|search|object|^|<|
|searchByAny|boolean|^|<|
|searchWildcardsEnabled|boolean|^|<|
|sortorder|string/array|^|<|
|startSearch|boolean|^|<|

[comment]: # ({/new-9ca798d7})

[comment]: # ({new-7223bab1})
### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Examples 示例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-0b1cb387})
#### Retrieving maintenances 获取维护模式

Retrieve all configured maintenances, and the data about the assigned
host groups, hosts and defined time periods.
获取所有配置的维护模式，以及关于指定主机组、主机和定义的时间周期数据。

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "maintenance.get",
    "params": {
        "output": "extend",
        "selectGroups": "extend",
        "selectTimeperiods": "extend"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "maintenanceid": "3",
            "name": "Sunday maintenance",
            "maintenance_type": "0",
            "description": "",
            "active_since": "1358844540",
            "active_till": "1390466940",
            "groups": [
                {
                    "groupid": "4",
                    "name": "Zabbix servers",
                    "internal": "0"
                }
            ],
            "timeperiods": [
                {
                    "timeperiodid": "4",
                    "timeperiod_type": "3",
                    "every": "1",
                    "month": "0",
                    "dayofweek": "1",
                    "day": "0",
                    "start_time": "64800",
                    "period": "3600",
                    "start_date": "2147483647"
                }
            ]
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-0b1cb387})

[comment]: # ({new-dbcf678a})
### See also

-   [Host](/zh/manual/api/reference/host/object#host)
-   [Host group](/zh/manual/api/reference/hostgroup/object#host_group)
-   [Time period](object#time_period)

[comment]: # ({/new-dbcf678a})

[comment]: # ({new-50ed5be3})
### Source

CMaintenance::get() in
*frontends/php/include/classes/api/services/CMaintenance.php*.

[comment]: # ({/new-50ed5be3})
