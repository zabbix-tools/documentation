[comment]: # translation:outdated

[comment]: # ({new-f47b150e})
# maintenance.create

[comment]: # ({/new-f47b150e})

[comment]: # ({new-e8498ebc})
### Description 说明

`object maintenance.create(object/array maintenances)`

This method allows to create new maintenances.
此方法允许创建新的维护模式。

[comment]: # ({/new-e8498ebc})

[comment]: # ({new-6c14daac})
### Parameters 参数

`(object/array)` Maintenances to create.

`(object/array)`要创建的维护模式。

Additionally to the [standard maintenance
properties](object#maintenance), the method accepts the following
parameters. 另外见[standard maintenance
properties](object#maintenance)，此方法接受如下参数。

|Parameter|Type|Description|
|---------|----|-----------|
|**groupids**<br>(required)|array|IDs of the host groups that will undergo maintenance. 要执行维护模式的主机组IDs。|
|**hostids**<br>(required)|array|IDs of the hosts that will undergo maintenance. 要执行维护模式的主机的IDs。|
|**timeperiods**<br>(required)|array|Maintenance time periods. 维护模式时间周期。|

::: noteimportant
At least one host or host group must be defined
for each
maintenance.每个维护模式至少一个主机或主机组被定义。
:::

[comment]: # ({/new-6c14daac})

[comment]: # ({new-48c4869c})
### Return values 返回值

`(object)` Returns an object containing the IDs of the created
maintenances under the `maintenanceids` property. The order of the
returned IDs matches the order of the passed maintenances. `(object)`
在`maintenanceids`属性中返回一个包含所有已被创建的维护模式的对象的ID。返回的IDs的排序与传递的维护模式的IDs顺序一致。

[comment]: # ({/new-48c4869c})

[comment]: # ({new-b41637d2})
### Examples 示例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-bb812c67})
#### Creating a maintenance 创建一个维护模式

Create a maintenance with data collection for host group "2". It must be
active from 22.01.2013 till 22.01.2014, come in effect each Sunday at
18:00 and last for one hour. 为主机组"2"以with data
collection(持续收集数据)模式创建一个维护模式。该维护模式生效于22.01.2013
到 22.01.2014，每周六的18:00生效，并持续1个小时。

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "maintenance.create",
    "params": {
        "name": "Sunday maintenance",
        "active_since": 1358844540,
        "active_till": 1390466940,
        "groupids": [
            "2"
        ],
        "timeperiods": [
            {
                "timeperiod_type": 3,
                "every": 1,
                "dayofweek": 64,
                "start_time": 64800,
                "period": 3600
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "maintenanceids": [
            "3"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-bb812c67})

[comment]: # ({new-82b3ef0e})
### See also

-   [Time period](object#time_period)

[comment]: # ({/new-82b3ef0e})

[comment]: # ({new-3e3a556f})
### Source

CMaintenance::create() in
*frontends/php/include/classes/api/services/CMaintenance.php*.

[comment]: # ({/new-3e3a556f})
