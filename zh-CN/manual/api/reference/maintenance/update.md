[comment]: # translation:outdated

[comment]: # ({new-8c711e69})
# maintenance.update

[comment]: # ({/new-8c711e69})

[comment]: # ({new-292dcffa})
### Description 说明

`object maintenance.update(object/array maintenances)`

This method allows to update existing maintenances.
此方法允许更新已存在的维护模式。

[comment]: # ({/new-292dcffa})

[comment]: # ({new-678c22b3})
### Parameters 参数

`(object/array)` Maintenance properties to be updated.
`(object/array)`要更新的维护模式的属性。

The `maintenanceid` property must be defined for each maintenance, all
other properties are optional. Only the passed properties will be
updated, all others will remain unchanged.
每一个维护模式的`maintenanceid`属性必须被定义，其他所有属性均为可选。只有被传递的属性才会被更新，所有它属性保持不变。

Additionally to the [standard maintenance
properties](object#maintenance), the method accepts the following
parameters. 另外见[standard maintenance
properties](/zh/object#maintenance),此方法接受如下参数。

|Parameter|Type|Description|
|---------|----|-----------|
|groupids|array|IDs of the host groups to replace the current groups. 要替换的当前主机组的主机组IDs。|
|hostids|array|IDs of the hosts to replace the current hosts. 要替换当前主机的主机IDs。|
|timeperiods|array|Maintenance time periods to replace the current periods. 要替换当前维护模式时间周期的时间周期。|

::: noteimportant
At least one host or host group must be defined
for each maintenance.
每一个维护模式至少一个主机或者一个主机组被定义。
:::

[comment]: # ({/new-678c22b3})

[comment]: # ({new-b9ffa219})
### Return values 返回值

`(object)` Returns an object containing the IDs of the updated
maintenances under the `maintenanceids` property. `(object)`
在`maintenanceids`属性中返回一个包含已被更新的维护模式的IDs的对象。

[comment]: # ({/new-b9ffa219})

[comment]: # ({new-b41637d2})
### Examples 示例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-25215f8d})
#### Assigning different hosts 指定不同的主机

Replace the hosts currently assigned to maintenance "3" with two
different ones. 用两个不同的主机替换当前分配给维护“3”的主机。

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "maintenance.update",
    "params": {
        "maintenanceid": "3",
        "hostids": [
            "10085",
            "10084"
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "maintenanceids": [
            "3"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-25215f8d})

[comment]: # ({new-82b3ef0e})
### See also

-   [Time period](object#time_period)

[comment]: # ({/new-82b3ef0e})

[comment]: # ({new-63ad1661})
### Source

CMaintenance::update() in
*frontends/php/include/classes/api/services/CMaintenance.php*.

[comment]: # ({/new-63ad1661})
