[comment]: # translation:outdated

[comment]: # ({new-0aafdb81})
# maintenance.delete

[comment]: # ({/new-0aafdb81})

[comment]: # ({new-5afbd3a3})
### Description 说明

`object maintenance.delete(array maintenanceIds)`

This method allows to delete maintenances. 此方法允许删除维护模式。

[comment]: # ({/new-5afbd3a3})

[comment]: # ({new-9828d580})
### Parameters 参数

`(array)` IDs of the maintenances to delete. `(array)`
要删除的维护模式的IDs。

[comment]: # ({/new-9828d580})

[comment]: # ({new-1bb5e665})
### Return values 返回值

`(object)` Returns an object containing the IDs of the deleted
maintenances under the `maintenanceids` property. `(object)`
在`maintenanceids`属性下返回包含已被删除的维护模式的ID对象。

[comment]: # ({/new-1bb5e665})

[comment]: # ({new-b41637d2})
### Examples 示例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-9baa4c26})
#### Deleting multiple maintenances 删除多个维护模式

Delete two maintenanaces. 删除2个维护模式。

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "maintenance.delete",
    "params": [
        "3",
        "1"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "maintenanceids": [
            "3",
            "1"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-9baa4c26})

[comment]: # ({new-87f95129})
### Source

CMaintenance::delete() in
*frontends/php/include/classes/api/services/CMaintenance.php*.

[comment]: # ({/new-87f95129})
