[comment]: # translation:outdated

[comment]: # ({new-c9539601})
# > Maintenance object 维护模式对象

The following objects are directly related to the `maintenance` API.
如下对象与`maintenance`API关联。

[comment]: # ({/new-c9539601})

[comment]: # ({new-6c1a70bc})
### Maintenance 维护模式

The maintenance object has the following properties.
维护模式对象有如下属性。

|Property|Type|Description|
|--------|----|-----------|
|maintenanceid|string|*(readonly)* ID of the maintenance. 维护模式的ID。|
|**name**<br>(required)|string|Name of the maintenance. 维护模式的名称。|
|**active\_since**<br>(required)|timestamp|Time when the maintenance becomes active. 维护模式生效的时刻。|
|**active\_till**<br>(required)|timestamp|Time when the maintenance stops being active. 维护模式失效的时刻。|
|description|string|Description of the maintenance. 维护模式说明。|
|maintenance\_type|integer|Type of maintenance. 维护模式类型。<br><br>Possible values: 可能的值：<br>0 - *(default)* with data collection;<br>1 - without data collection.|

[comment]: # ({/new-6c1a70bc})

[comment]: # ({new-506f52aa})
### Time period 时间周期

The time period object is used to define periods when the maintenance
must come into effect. It has the following properties.
时间周期对象用于定义维护模式生效的时间周期。它有如下属性。

|Property|Type|Description|
|--------|----|-----------|
|timeperiodid|string|*(readonly)* ID of the maintenance. 维护模式ID。|
|day|integer|Day of the month when the maintenance must come into effect. 维护模式生效的月份天次。<br><br>Required only for monthly time periods. 月份时间周期要求。|
|dayofweek|integer|Days of the week when the maintenance must come into effect. 维护模式生效的周次。<br><br>Days are stored in binary form with each bit representing the corresponding day. For example, 4 equals 100 in binary and means, that maintenance will be enabled on Wednesday. 日期以二进制形式存储，每个比特代表对应的一天。例如，4在二进制中等于100，意味着星期三将启用维护。<br><br>Used for weekly and monthly time periods. Required only for weekly time periods. 用于周或月时间周期。仅周时间周期要求。|
|every|integer|For daily and weekly periods `every` defines day or week intervals at which the maintenance must come into effect. 对于天或者周的周期`every`定义维护模式生效的天或者周间隔。<br><br>For monthly periods `every` defines the week of the month when the maintenance must come into effect. 对于月周期`every`定义该月维护模式生效的周次。<br>Possible values: 可能的值：<br>1 - first week;<br>2 - second week;<br>3 - third week;<br>4 - fourth week;<br>5 - last week.|
|month|integer|Months when the maintenance must come into effect. 维护模式必须生效的月份。<br><br>Months are stored in binary form with each bit representing the corresponding month. For example, 5 equals 101 in binary and means, that maintenance will be enabled in January and March. 月份以二进制形式存储，每个位代表相应月份。例如，5在二进制中等于101，意味着维护将在一月和3月启用。<br><br>Required only for monthly time periods. 只有月时间周期要求。|
|period|integer|Time of day when the maintenance starts in seconds. 维护模式周期的时间（秒）。<br><br>Default: 3600.|
|start\_date|timestamp|Date when the maintenance period must come into effect. 维护模式必须生效的日期。<br><br>Required only for one time periods.<br><br>Default: current date.|
|start\_time|integer|Time of day when the maintenance starts in seconds. 一天内维护模式开始的时刻。<br><br>Required for daily, weekly and monthly periods.天、周、月周期要求。|
|timeperiod\_type|integer|Type of time period. 时间周期类型。<br><br>Possible values: 可能的值：<br>0 - *(default)* one time only;<br>2 - daily;<br>3 - weekly;<br>4 - monthly.|

[comment]: # ({/new-506f52aa})


[comment]: # ({new-2913a3ae})
### Problem tag

The problem tag object is used to define which problems must be
suppressed when the maintenance comes into effect. It has the following
properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**tag**<br>(required)|string|Problem tag name.|
|operator|integer|Condition operator.<br><br>Possible values:<br>0 - Equals;<br>2 - *(default)* Contains.|
|value|string|Problem tag value.|

[comment]: # ({/new-2913a3ae})
