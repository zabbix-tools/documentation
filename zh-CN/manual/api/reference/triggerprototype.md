[comment]: # translation:outdated

[comment]: # ({new-2d53bfd4})
# 触发器原型

This class is designed to work with trigger prototypes.
此类用于管理触发器原型。

Object references: 对象引用：\

-   [Trigger
    prototype](/manual/api/reference/triggerprototype/object#trigger_prototype)

Available methods: 可用方法：\

-   [triggerprototype.create](/zh/manual/api/reference/triggerprototype/create) -
    creating new trigger prototypes 创建新的触发器原型
-   [triggerprototype.delete](/zh/manual/api/reference/triggerprototype/delete) -
    deleting trigger prototypes 删除触发器原型
-   [triggerprototype.get](/zh/manual/api/reference/triggerprototype/get) -
    retrieving trigger prototypes 检索触发器原型
-   [triggerprototype.update](/zh/manual/api/reference/triggerprototype/update) -
    updating trigger prototypes 更新触发器原型

[comment]: # ({/new-2d53bfd4})
