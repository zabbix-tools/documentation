[comment]: # translation:outdated

[comment]: # ({new-a17bb95b})
# > Web场景对象

以下对象与`webcheck`API直接相关。

[comment]: # ({/new-a17bb95b})

[comment]: # ({new-e60908bb})
## > Web scenario object

The following objects are directly related to the `webcheck` API.

[comment]: # ({/new-e60908bb})

[comment]: # ({new-c7ab7fb0})
### Web场景

Web场景对象具有以下属性。

|属性                 类|说明|<|
|--------------------------|------|-|
|httptestid|string|*(readonly)* Web场景的ID|
|**hostid**<br>(required)|string|Web场景所属主机的ID。|
|**name**<br>(required)|string|Web场景的名称|
|agent|string|将由Web场景使用的用户代理字符串。<br><br>默认: Zabbix|
|applicationid|string|Web场景所属应用程序的ID。|
|authentication|integer|将由Web场景使用的身份验证方法。<br><br>可能的值：<br>0 - *(默认)* 无;<br>1 - 基本的HTTP认证;<br>2 - NTLM身份验证|
|delay|string|Web场景的执行间隔。 接受秒，时间单位后缀和用户宏。<br><br>默认: 1m.|
|headers|string|执行请求时将发送的HTTP标题。|
|http\_password|string|用于认证的密码。<br><br>对于具有基本HTTP或NTLM身份验证的Web场景是必需的.|
|http\_proxy|string|将由Web场景使用的代理*http://\[username\[:password\]@\]proxy.example.com\[:port\]*.|
|http\_user|string|用于认证的用户名<br><br>对于具有基本HTTP或NTLM身份验证的Web场景，必需。|
|nextcheck|timestamp|*(readonly)*下一个Web场景执行的时间。|
|retries|integer|Web场景在失败之前尝试执行每个步骤的次数。<br><br>默认: 1.|
|ssl\_cert\_file|string|用于客户端身份验证的SSL证书文件的名称（必须为PEM格式）。|
|ssl\_key\_file|string|用于客户端认证的SSL私钥文件的名称（必须为PEM格式）。|
|ssl\_key\_password|string|SSL私钥密码。|
|status|integer|是否启用了Web方案。<br><br>可能的值：<br>0 - *(默认)* 启用;<br>1 -禁用.|
|templateid|string|*(readonly)* 父模板Web方案的ID。|
|variables|string|Web场景变量。|
|verify\_host|integer|验证SSL证书中指定的主机名是否与场景中使用的主机名相匹配。<br><br>可能的值：<br>0 - *(默认)* 跳过主机验证;<br>1 - 验证主机。|
|verify\_peer|integer|是否验证Web服务器的SSL证书。<br>\\\\可能的值：<br>0 - *(默认)* 跳过对等验证;<br>1 - 验证对等|

[comment]: # ({/new-c7ab7fb0})

[comment]: # ({new-eb20f47d})
### Web scenario

The web scenario object has the following properties.

|Property|Type|Description|
|--------|----|-----------|
|httptestid|string|*(readonly)* ID of the web scenario.|
|**hostid**<br>(required)|string|ID of the host that the web scenario belongs to.|
|**name**<br>(required)|string|Name of the web scenario.|
|agent|string|User agent string that will be used by the web scenario.<br><br>Default: Zabbix|
|applicationid|string|ID of the application that the web scenario belongs to.|
|authentication|integer|Authentication method that will be used by the web scenario.<br><br>Possible values:<br>0 - *(default)* none;<br>1 - basic HTTP authentication;<br>2 - NTLM authentication.|
|delay|string|Execution interval of the web scenario. Accepts seconds, time unit with suffix and user macro.<br><br>Default: 1m.|
|headers|string *(deprecated)*<br>array of [HTTP fields](/manual/api/reference/httptest/object#HTTP field)|HTTP headers that will be sent when performing a request.|
|http\_password|string|Password used for authentication.<br><br>Required for web scenarios with basic HTTP or NTLM authentication.|
|http\_proxy|string|Proxy that will be used by the web scenario given as *http://\[username\[:password\]@\]proxy.example.com\[:port\]*.|
|http\_user|string|User name used for authentication.<br><br>Required for web scenarios with basic HTTP or NTLM authentication.|
|nextcheck|timestamp|*(readonly)* Time of the next web scenario execution.|
|retries|integer|Number of times a web scenario will try to execute each step before failing.<br><br>Default: 1.|
|ssl\_cert\_file|string|Name of the SSL certificate file used for client authentication (must be in PEM format).|
|ssl\_key\_file|string|Name of the SSL private key file used for client authentication (must be in PEM format).|
|ssl\_key\_password|string|SSL private key password.|
|status|integer|Whether the web scenario is enabled.<br><br>Possible values are:<br>0 - *(default)* enabled;<br>1 - disabled.|
|templateid|string|*(readonly)* ID of the parent template web scenario.|
|variables|string *(deprecated)*<br>array of [HTTP fields](/manual/api/reference/httptest/object#HTTP field)|Web scenario variables.|
|verify\_host|integer|Whether to verify that the host name specified in the SSL certificate matches the one used in the scenario.<br><br>Possible values are:<br>0 - *(default)* skip host verification;<br>1 - verify host.|
|verify\_peer|integer|Whether to verify the SSL certificate of the web server.<br><br>Possible values are:<br>0 - *(default)* skip peer verification;<br>1 - verify peer.|

[comment]: # ({/new-eb20f47d})

[comment]: # ({new-d69e8b7b})
### 场景步骤

场景步骤对象定义特定的Web场景检查。 它具有以下属性。

|属性                类|说明|<|
|-------------------------|------|-|
|httpstepid|string|*(readonly)* 情景步骤的ID|
|**name**<br>(required)|string|场景步骤的名称。|
|**no**<br>(required)|integer|Web场景中步骤的序列号。|
|**url**<br>(required)|string|要检查的URL。|
|follow\_redirects|integer|是否遵循HTTP重定向<br><br>可能的值：<br>0 - 不要重新导向;<br>1 - *(default)* 遵循重定向|
|headers|string *(deprecated)*<br>array of [HTTP fields](/manual/api/reference/httptest/object#HTTP field)|执行请求时将发送的HTTP headers。场景步骤headers将覆盖Web场景指定的HTTP headers。|
|httptestid|string|*(readonly)* 该步骤所属的Web方案的ID。|
|posts|string<br>array of [HTTP fields](/manual/api/reference/httptest/object#HTTP field)|HTTP POST字符串（原始POST数据）或者一个 [HTTP字段](/manual/api/reference/httptest/object#HTTP field) 数组 (来自字段数据)。|
|required|string|必须在响应中存在的文本。|
|retrieve\_mode|integer|方案步骤必须检索的HTTP响应的一部分。<br>\\\\可能的值：<br>0 - *(default)* 仅有文体;<br>1 - 仅有标题.|
|status\_codes|string|所需HTTP状态代码的范围用逗号分隔。|
|timeout|string|请求超时（秒）。接受秒数，带后缀的时间单位和用户宏。<br><br>默认: 15s.|
|variables|string *(deprecated)*<br>array of [HTTP字段](/manual/api/reference/httptest/object#HTTP field)|场景步骤变量。|
|query\_fields|array of [HTTP字段](/manual/api/reference/httptest/object#HTTP field)      查|字段 - 在执行请求时将添加到URL [HTTP字段](/manual/api/reference/httptest/object#HTTP field)|

<note
important>对于Web场景和Web场景步骤对象的`headers`和`variables`字段，都允许使用[HTTP字段](/manual/api/reference/httptest/object#HTTP field)类型的字符串和数组。\
不推荐使用 `headers`和 `variables`
的字符串数据类型，将来的版本将删除它们。
:::

### Scenario step

The scenario step object defines a specific web scenario check. It has
the following properties.

|Property|Type|Description|
|--------|----|-----------|
|httpstepid|string|*(readonly)* ID of the scenario step.|
|**name**<br>(required)|string|Name of the scenario step.|
|**no**<br>(required)|integer|Sequence number of the step in a web scenario.|
|**url**<br>(required)|string|URL to be checked.|
|follow\_redirects|integer|Whether to follow HTTP redirects.<br><br>Possible values are:<br>0 - don't follow redirects;<br>1 - *(default)* follow redirects.|
|headers|string *(deprecated)*<br>array of [HTTP fields](/manual/api/reference/httptest/object#HTTP field)|HTTP headers that will be sent when performing a request. Scenario step headers will overwrite headers specified for the web scenario.|
|httptestid|string|*(readonly)* ID of the web scenario that the step belongs to.|
|posts|string<br>array of [HTTP fields](/manual/api/reference/httptest/object#HTTP field)|HTTP POST variables as a string (raw post data) or as an array of [HTTP fields](/manual/api/reference/httptest/object#HTTP field) (form field data).|
|required|string|Text that must be present in the response.|
|retrieve\_mode|integer|Part of the HTTP response that the scenario step must retrieve.<br><br>Possible values are:<br>0 - *(default)* only body;<br>1 - only headers.|
|status\_codes|string|Ranges of required HTTP status codes separated by commas.|
|timeout|string|Request timeout in seconds. Accepts seconds, time unit with suffix and user macro.<br><br>Default: 15s.|
|variables|string *(deprecated)*<br>array of [HTTP fields](/manual/api/reference/httptest/object#HTTP field)|Scenario step variables.|
|query\_fields|array of [HTTP fields](/manual/api/reference/httptest/object#HTTP field)|Query fields - array of [HTTP fields](/manual/api/reference/httptest/object#HTTP field) that will be added to URL when performing a request|

::: noteimportant
Both string and array of [HTTP
fields](/manual/api/reference/httptest/object#HTTP field) types are
allowed for `headers` and `variables` fields of both web scenario and
web scenario step object.\
String data type for `headers` and `variables` is deprecated and will be
removed in future versions.
:::

### HTTP 字段

HTTP字段对象定义名称和值，用于指定查询字段数据的变量，HTTP标头，POST表单字段数据。它具有以下属性。

|属性         类|说明|<|
|------------------|------|-|
|**name**<br>(required)|string|header / variable / POST 或者 GET 字段的名称。|
|**value**<br>(required)|string|header / variable / POST 或者 GET 字段的值。|

### HTTP field

The HTTP field object defines a name and value that is used to specify
variable, HTTP header, POST form field data of query field data. It has
the following properties.

|Property|Type|Description|
|--------|----|-----------|
|**name**<br>(required)|string|Name of header / variable / POST or GET field.|
|**value**<br>(required)|string|Value of header / variable / POST or GET field.|

[comment]: # ({/new-d69e8b7b})
