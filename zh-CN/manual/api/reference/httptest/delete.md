[comment]: # translation:outdated

[comment]: # ({new-629f1e7c})
# httptest.delete

[comment]: # ({/new-629f1e7c})

[comment]: # ({new-67a29a65})
### 说明

`object httptest.delete(array webScenarioIds)`

此方法允许删除Web场景。

[comment]: # ({/new-67a29a65})

[comment]: # ({new-16d66a94})
### Description

`object httptest.delete(array webScenarioIds)`

This method allows to delete web scenarios.

[comment]: # ({/new-16d66a94})

[comment]: # ({new-fd0832d6})
### 参数

`(array)` 要删除的网络场景的ID。

[comment]: # ({/new-fd0832d6})

[comment]: # ({new-b41637d2})
### Parameters

`(array)` IDs of the web scenarios to delete.

[comment]: # ({/new-b41637d2})

[comment]: # ({new-4924488e})
### 返回值

`(object)` 返回包含`httptestids`属性下删除的Web方案的ID的对象。

[comment]: # ({/new-4924488e})

[comment]: # ({new-0ba69a51})
### Return values

`(object)` Returns an object containing the IDs of the deleted web
scenarios under the `httptestids` property.

### 示例

#### 删除多个Web场景

删除2个Web场景

### Examples

#### Deleting multiple web scenarios

Delete two web scenarios.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "httptest.delete",
    "params": [
        "2",
        "3"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "httptestids": [
            "2",
            "3"
        ]
    },
    "id": 1
}
```

### 来源

### Source

CHttpTest::delete() in
*frontends/php/include/classes/api/services/CHttpTest.php*.

[comment]: # ({/new-0ba69a51})
