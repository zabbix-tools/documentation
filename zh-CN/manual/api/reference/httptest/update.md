[comment]: # translation:outdated

[comment]: # ({new-c289fd9d})
# httptest.update

[comment]: # ({/new-c289fd9d})

[comment]: # ({new-b09ad465})
### Description

`object httptest.update(object/array webScenarios)`
此方法允许更新现有的Web场景。

[comment]: # ({/new-b09ad465})

[comment]: # ({new-83b60d28})
### Description

`object httptest.update(object/array webScenarios)`

This method allows to update existing web scenarios.

[comment]: # ({/new-83b60d28})

[comment]: # ({new-02282fdb})
### 参数

`(object/array)`要更新的Web场景属性。

必须为每个Web场景定义`httptestid`属性，所有其他属性都是可选的。
只有通过的属性将被更新，所有其他属性将保持不变
除了[标准Web场景属性](object#web_scenario)外, 该方法接受以下参数。

|参数    类|说明|<|
|-------------|------|-|
|steps|array|用来替代现有的步骤的方案步骤。|

[comment]: # ({/new-02282fdb})

[comment]: # ({new-b41637d2})
### Parameters

`(object/array)` Web scenario properties to be updated.

The `httptestid` property must be defined for each web scenario, all
other properties are optional. Only the passed properties will be
updated, all others will remain unchanged.

Additionally to the [standard web scenario
properties](object#web_scenario), the method accepts the following
parameters.

|Parameter|Type|Description|
|---------|----|-----------|
|steps|array|Scenario steps to replace existing steps.|

[comment]: # ({/new-b41637d2})

[comment]: # ({new-4c3bf00e})
### 返回值

[comment]: # ({/new-4c3bf00e})

[comment]: # ({new-e8ca015f})
### Return values

`(object)` Returns an object containing the IDs of the updated web
scenarios under the `httptestid` property.

[comment]: # ({/new-e8ca015f})

[comment]: # ({new-628fc1ff})
### Examples

#### Enabling a web scenario

Enable a web scenario, that is, set its status to "0".

### 示例

#### 启用Web方案

启用Web方案，即将其状态设置为“0”。

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "httptest.update",
    "params": {
        "httptestid": "5",
        "status": 0
    },
    "auth": "700ca65537074ec963db7efabda78259",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "httptestids": [
            "5"
        ]
    },
    "id": 1
}
```

### 参考

### See also

-   [Scenario step](object#scenario_step)

### 来源

CHttpTest::update() in
*frontends/php/include/classes/api/services/CHttpTest.php*.

[comment]: # ({/new-628fc1ff})
