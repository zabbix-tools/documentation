[comment]: # translation:outdated

[comment]: # ({new-3c277f74})
# httptest.create

[comment]: # ({/new-3c277f74})

[comment]: # ({new-c4bddb28})
### 说明

`object httptest.create(object/array webScenarios)`

此方法允许创建新的Web场景。

::: noteclassic
创建Web场景将自动创建一组[web监控项](/manual/web_monitoring/items).
:::

[comment]: # ({/new-c4bddb28})

[comment]: # ({new-5bd5a38b})
### Description

`object httptest.create(object/array webScenarios)`

This method allows to create new web scenarios.

::: noteclassic
Creating a web scenario will automatically create a set of
[web monitoring items](/manual/web_monitoring/items).
:::

[comment]: # ({/new-5bd5a38b})

[comment]: # ({new-0f4da548})
### 参数

`(object/array)` 要创建的Web场景。

除了 [标准Web场景属性](object#web_scenario)之外, 该方法接受以下参数

|参数         类|说明|<|
|------------------|------|-|
|**steps**<br>(required)|array|Web方案步骤。|

[comment]: # ({/new-0f4da548})

[comment]: # ({new-b41637d2})
### Parameters

`(object/array)` Web scenarios to create.

Additionally to the [standard web scenario
properties](object#web_scenario), the method accepts the following
parameters.

|Parameter|Type|Description|
|---------|----|-----------|
|**steps**<br>(required)|array|Web scenario steps.|

[comment]: # ({/new-b41637d2})

[comment]: # ({new-0b10bb11})
### 返回值

`(object)`返回一个包含“httptestids”属性下创建的Web场景的ID的对象。
返回的ID的顺序与传递的Web方案的顺序相匹配。

[comment]: # ({/new-0b10bb11})

[comment]: # ({new-e8ca015f})
### Return values

`(object)` Returns an object containing the IDs of the created web
scenarios under the `httptestids` property. The order of the returned
IDs matches the order of the passed web scenarios.

[comment]: # ({/new-e8ca015f})

[comment]: # ({new-6b938292})
### 示例

### Examples

#### 创建Web场景

创建一个Web场景来监视公司主页。
该方案将有两个步骤，以检查主页和“关于”页面，并确保它们返回HTTP状态代码200。

#### Creating a web scenario

Create a web scenario to monitor the company home page. The scenario
will have two steps, to check the home page and the "About" page and
make sure they return the HTTP status code 200.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "httptest.create",
    "params": {
        "name": "Homepage check",
        "hostid": "10085",
        "steps": [
            {
                "name": "Homepage",
                "url": "http://mycompany.com",
                "status_codes": "200",
                "no": 1
            },
            {
                "name": "Homepage / About",
                "url": "http://mycompany.com/about",
                "status_codes": "200",
                "no": 2
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "httptestids": [
            "5"
        ]
    },
    "id": 1
}
```

### 参见

### See also

-   [Scenario step](object#scenario_step)

### 来源

### Source

CHttpTest::create() in
*frontends/php/include/classes/api/services/CHttpTest.php*.

[comment]: # ({/new-6b938292})
