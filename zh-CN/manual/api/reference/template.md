[comment]: # translation:outdated

[comment]: # ({new-4f0fdd87})
# 模板

This class is designed to work with templates. 此类用于管理模板。 Object
references: 对象引用:\

-   [模板](/zh/manual/api/reference/template/object#template)

Available methods: 可用方法:\

-   [template.create](/manual/api/reference/template/create) - creating
    new templates 创建新模板
-   [template.delete](/manual/api/reference/template/delete) - deleting
    templates 删除模板
-   [template.get](/manual/api/reference/template/get) - retrieving
    templates 检索模板
-   [template.massadd](/manual/api/reference/template/massadd) - adding
    related objects to templates 添加相关对象到模板中
-   [template.massremove](/manual/api/reference/template/massremove) -
    removing related objects from templates 从模板中删除相关对象
-   [template.massupdate](/manual/api/reference/template/massupdate) -
    replacing or removing related objects from templates
    从模板中替换或删除相关对象
-   [template.update](/manual/api/reference/template/update) - updating
    templates 更新模板

[comment]: # ({/new-4f0fdd87})
