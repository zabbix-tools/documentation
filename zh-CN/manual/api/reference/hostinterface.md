[comment]: # translation:outdated

[comment]: # ({new-6bf804b5})
# 主机接口

这个类是设计用于处理主机接口.

对象引用:\

-   [Host
    interface](/manual/api/reference/hostinterface/object#host_interface)

可用方法:\

-   [hostinterface.create](/manual/api/reference/hostinterface/create) -
    创建新的主机接口
-   [hostinterface.delete](/manual/api/reference/hostinterface/delete) -
    删除主机接口
-   [hostinterface.get](/manual/api/reference/hostinterface/get) -
    获取主机接口
-   [hostinterface.massadd](/manual/api/reference/hostinterface/massadd) -
    批量添加主机接口
-   [hostinterface.massremove](/manual/api/reference/hostinterface/massremove) -
    批量
-   [hostinterface.replacehostinterfaces](/manual/api/reference/hostinterface/replacehostinterfaces) -
    替换主机接口
-   [hostinterface.update](/manual/api/reference/hostinterface/update) -
    更新主机接口

[comment]: # ({/new-6bf804b5})
