[comment]: # translation:outdated

[comment]: # ({new-bf4e6957})
# User macro \[用户宏\]

此类用于全局宏的使用。

对象引用：\

-   [Global macro](/manual/api/reference/usermacro/object#global_macro)
-   [Host macro](/manual/api/reference/usermacro/object#host_macro)

可用的方法：\

-   [usermacro.create](/manual/api/reference/usermacro/create) -
    创建新的主机宏
-   [usermacro.createglobal](/manual/api/reference/usermacro/createglobal) -
    创建新的全局宏
-   [usermacro.delete](/manual/api/reference/usermacro/delete) -
    删除主机宏
-   [usermacro.deleteglobal](/manual/api/reference/usermacro/deleteglobal) -
    删除全局宏
-   [usermacro.get](/manual/api/reference/usermacro/get) -
    检索主机和全局宏
-   [usermacro.update](/manual/api/reference/usermacro/update) -
    更新主机宏
-   [usermacro.updateglobal](/manual/api/reference/usermacro/updateglobal) -
    更新全局宏

## User macro

This class is designed to work with host and global macros.

Object references:\

-   [Global macro](/manual/api/reference/usermacro/object#global_macro)
-   [Host macro](/manual/api/reference/usermacro/object#host_macro)

Available methods:\

-   [usermacro.create](/manual/api/reference/usermacro/create) -
    creating new host macros
-   [usermacro.createglobal](/manual/api/reference/usermacro/createglobal) -
    creating new global macros
-   [usermacro.delete](/manual/api/reference/usermacro/delete) -
    deleting host macros
-   [usermacro.deleteglobal](/manual/api/reference/usermacro/deleteglobal) -
    deleting global macros
-   [usermacro.get](/manual/api/reference/usermacro/get) - retrieving
    host and global macros
-   [usermacro.update](/manual/api/reference/usermacro/update) -
    updating host macros
-   [usermacro.updateglobal](/manual/api/reference/usermacro/updateglobal) -
    updating global macros

[comment]: # ({/new-bf4e6957})
