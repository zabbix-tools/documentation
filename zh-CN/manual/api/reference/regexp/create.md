[comment]: # translation:outdated

[comment]: # ({new-acf00dc8})
# regexp.create

[comment]: # ({/new-acf00dc8})

[comment]: # ({new-3c60fab5})
### Description

`object regexp.create(object/array regularExpressions)`

This method allows to create new global regular expressions.

::: noteclassic
This method is only available to *Super admin* user types.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-3c60fab5})

[comment]: # ({new-f518ef9f})
### Parameters

`(object/array)` Regular expressions to create.

Additionally to the [standard properties](object), the method accepts
the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|expressions|array|[Expressions](/manual/api/reference/regexp/object#expressions_object) options.|

[comment]: # ({/new-f518ef9f})

[comment]: # ({new-f0011f1b})
### Return values

`(object)` Returns an object containing the IDs of the created regular
expressions under the `regexpids` property.

[comment]: # ({/new-f0011f1b})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-3d653b4d})
#### Creating a new global regular expression.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "regexp.create",
    "params": {
      "name": "Storage devices for SNMP discovery",
      "test_string": "/boot",
      "expressions": [
        {
          "expression": "^(Physical memory|Virtual memory|Memory buffers|Cached memory|Swap space)$",
          "expression_type": "4",
          "case_sensitive": "1"
        }
      ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "regexpids": [
            "16"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-3d653b4d})

[comment]: # ({new-cf9f5f19})
### Source

CRegexp::create() in *ui/include/classes/api/services/CRegexp.php*.

[comment]: # ({/new-cf9f5f19})
