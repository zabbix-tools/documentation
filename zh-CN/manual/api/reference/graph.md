[comment]: # translation:outdated

[comment]: # ({new-b8d971b2})
# Graph 图表

## 图表

This class is designed to work with items. 这个类用于配合监控项使用

Object references:\

-   [Graph](/manual/api/reference/graph/object#graph)

参考对象:\

-   [图表](/zh/manual/api/reference/graph/object#graph)

Available methods:\

-   [graph.create](/manual/api/reference/graph/create) - creating new
    graphs
-   [graph.delete](/manual/api/reference/graph/delete) - deleting graphs
-   [graph.get](/manual/api/reference/graph/get) - retrieving graphs
-   [graph.update](/manual/api/reference/graph/update) - updating graphs

可用方法:\

-   [graph.create](/zh/manual/api/reference/graph/create) - 创建新的图表
-   [graph.delete](/zh/manual/api/reference/graph/delete) - 删除图表
-   [graph.get](/zh/manual/api/reference/graph/get) - 获取图表
-   [graph.update](/zh/manual/api/reference/graph/update) - 更新图表

[comment]: # ({/new-b8d971b2})
