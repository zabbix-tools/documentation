[comment]: # translation:outdated

[comment]: # ({new-92a86fab})
# 更新

[comment]: # ({/new-92a86fab})

[comment]: # ({new-381a99b9})
### Description 描述

`object proxy.update(object/array proxies)`

This method allows to update existing proxies.
此方法允许更新已存在的代理

[comment]: # ({/new-381a99b9})

[comment]: # ({new-4d6a0721})
### Parameters 参数

`(object/array)` Proxy properties to be updated.
`(object/array)`代理参数被更新

The `proxyid` property must be defined for each proxy, all other
properties are optional. Only the passed properties will be updated, all
others will remain unchanged.
每个主机必须定义`proxyid`参数，其他参数是可选的。仅仅传递的参数会被更新，其他的参数将保持不变。

Additionally to the [standard proxy properties](object#proxy), the
method accepts the following parameters. 此外[standard proxy
properties](object#proxy)，此方法接受以下参数

|Parameter|Type|Description|
|---------|----|-----------|
|hosts|array|Hosts to be monitored by the proxy. If a host is already monitored by a different proxy, it will be reassigned to the current proxy.<br><br>The hosts must have the `hostid` property defined. 代理监视的主机。如果一个主机已经被一个不同的代理监控，他将会重新分配到当前的代理<br>\\\\主机必须拥有`hostid`属性|
|interface|object|Host interface to replace the existing interface for the passive proxy. 主机接口将会替换已存在的主机接口用于被动代理|

[comment]: # ({/new-4d6a0721})

[comment]: # ({new-bab02241})
### Return values 返回值

`(object)` Returns an object containing the IDs of the updated proxies
under the `proxyids` property.
`(object)`返回一个对象，该对象包含`proxyids`属性下更新的代理的id。

[comment]: # ({/new-bab02241})

[comment]: # ({new-b41637d2})
### Examples 示例如下

[comment]: # ({/new-b41637d2})

[comment]: # ({new-3613ce9a})
#### Change hosts monitored by a proxy 改变一个主机的代理

Update the proxy to monitor the two given hosts.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "proxy.update",
    "params": {
        "proxyid": "10293",
        "hosts": [
            "10294",
            "10295"
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "proxyids": [
            "10293"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-3613ce9a})

[comment]: # ({new-f06b99dc})
#### Change proxy status 改变代理的状态

Change the proxy to an active proxy and rename it to "Active proxy".
改变代理的模式是主动模式，并且重命名为"Active proxy".

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "proxy.update",
    "params": {
        "proxyid": "10293",
        "host": "Active proxy",
        "status": "5"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "proxyids": [
            "10293"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-f06b99dc})

[comment]: # ({new-273e0fd8})
### See also

-   [Host](/manual/api/reference/host/object#host)
-   [Proxy interface](object#proxy_interface)

[comment]: # ({/new-273e0fd8})

[comment]: # ({new-8237a78a})
### Source

CProxy::update() in
*frontends/php/include/classes/api/services/CProxy.php*.

[comment]: # ({/new-8237a78a})
