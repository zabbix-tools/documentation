[comment]: # translation:outdated

[comment]: # ({new-226837df})
# 删除

[comment]: # ({/new-226837df})

[comment]: # ({new-a16e4b7f})
### Description 描述

`object proxy.delete(array proxies)`

This method allows to delete proxies. 此方法允许删除代理

[comment]: # ({/new-a16e4b7f})

[comment]: # ({new-0c678a75})
### Parameters 参数

`(array)` IDs of proxies to delete. `(array)` 删除代理的IDs

[comment]: # ({/new-0c678a75})

[comment]: # ({new-426b06a5})
### Return values 返回值

`(object)` Returns an object containing the IDs of the deleted proxies
under the `proxyids` property. `(object)`
返回在`proxyids`属性下包含已删除代理的id的对象。

[comment]: # ({/new-426b06a5})

[comment]: # ({new-b41637d2})
### Examples 示例如下

[comment]: # ({/new-b41637d2})

[comment]: # ({new-dcfc3683})
#### Delete multiple proxies 删除多个代理

Delete two proxies. 删除两个代理

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "proxy.delete",
    "params": [
        "10286",
        "10285"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "proxyids": [
            "10286",
            "10285"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-dcfc3683})

[comment]: # ({new-4dd75afc})
### Source

CProxy::delete() in
*frontends/php/include/classes/api/services/CProxy.php*.

[comment]: # ({/new-4dd75afc})
