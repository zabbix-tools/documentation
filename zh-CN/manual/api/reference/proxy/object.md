[comment]: # translation:outdated

[comment]: # ({new-7b0425dd})
# > 代理对象

The following objects are directly related to the `proxy` API.
以下对象直接关系到`proxy`API

[comment]: # ({/new-7b0425dd})

[comment]: # ({new-52ccb0be})
### 代理

The proxy object has the following properties. 代理对象拥有以下属性

|Property|Type|Description|
|--------|----|-----------|
|proxyid|string|*(readonly)* ID of the proxy. 代理的id|
|**host**<br>(required)|string|Name of the proxy. 代理的名称|
|**status**<br>(required)|integer|Type of proxy.<br><br>Possible values:<br>5 - active proxy;<br>6 - passive proxy. 代理的类型<br><br>可能的值：<br>5 - 主动代理 6 - 被动代理|
|description|text|Description of the proxy. 代理的描述|
|lastaccess|timestamp|*(readonly)* Time when the proxy last connected to the server. 上一次代理连接zabbix server的时间|
|tls\_connect|integer|Connections to host.<br><br>Possible values are:<br>1 - *(default)* No encryption;<br>2 - PSK;<br>4 - certificate. 连接主机<br><br>可能的值：<br>1 - *(default)* 非加密 2 - 共享密钥（PSK） 4 - 证书|
|tls\_accept|integer|Connections from host.<br><br>Possible bitmap values are:<br>1 - *(default)* No encryption;<br>2 - PSK;<br>4 - certificate. 从代理连接<br><br>可能的值：<br>1 - *(default)* 非加密 2 - 共享密钥（PSK） 4 - 证书|
|tls\_issuer|string|Certificate issuer. 证书发行者|
|tls\_subject|string|Certificate subject. 证书问题|
|tls\_psk\_identity|string|PSK identity. Required if either `tls_connect` or `tls_accept` has PSK enabled. 共享密钥（PSK）的身份，如果`tls_connect` 或 `tls_accept`都启用了PSK，则需要使用。|
|tls\_psk|string|The preshared key, at least 32 hex digits. Required if either `tls_connect` or `tls_accept` has PSK enabled. 预共享密钥，至少32位十六进制数字。如果`tls_connect` 或 `tls_accept`都启用了PSK，则需要使用。|
|auto\_compress|integer|*(readonly)* Indicates if communication between Zabbix server and proxy is compressed.<br><br>Possible values are:<br>0 - No compression;<br>1 - Compression enabled; *只读* 指示Zabbix服务器和代理之间的通信是否被压缩。<br>\\\\可能的值：<br>0 - 不压缩<br>1 - 压缩|

[comment]: # ({/new-52ccb0be})

[comment]: # ({new-4e21547f})
### Proxy interface 代理接口

The proxy interface object defines the interface used to connect to a
passive proxy. It has the following properties.
代理接口对象默认接口用于连接被动代理。他有一下属性

|Property|Type|Description|
|--------|----|-----------|
|interfaceid|string|*(readonly)* ID of the interface. 接口的ID|
|**dns**<br>(required)|string|DNS name to connect to.<br><br>Can be empty if connections are made via IP address. 连接的DNS名称<br>\\\\如果通过IP地址进行连接，可以为空|
|**ip**<br>(required)|string|IP address to connect to.<br><br>Can be empty if connections are made via DNS names. 连接到IP地址<br><br>如果通过DNS名称连接可以为空|
|**port**<br>(required)|string|Port number to connect to.|
|**useip**<br>(required)|integer|Whether the connection should be made via IP address.<br><br>Possible values are:<br>0 - connect using DNS name;<br>1 - connect using IP address. 是否应该通过IP地址进行连接。<br><br>可能的值：<br>0 - 用DNS名称链接 1 - 用IP地址连接|
|hostid|string|*(readonly)* ID of the proxy the interface belongs to. // (只读) // 接口所属的代理的ID。|

[comment]: # ({/new-4e21547f})
