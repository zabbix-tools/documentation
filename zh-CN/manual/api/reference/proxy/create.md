[comment]: # translation:outdated

[comment]: # ({new-07099686})
# 创建

[comment]: # ({/new-07099686})

[comment]: # ({new-e0abed38})
### Description 描述

`object proxy.create(object/array proxies)`

This method allows to create new proxies. 此方法用于创建新的代理

[comment]: # ({/new-e0abed38})

[comment]: # ({new-fc26a415})
### Parameters 参数

`(object/array)` Proxies to create. `(object/array)` 创建代理

Additionally to the [standard proxy properties](object#proxy), the
method accepts the following parameters. 此外[standard proxy
properties](object#proxy)，此方法接受以下参数。

|Parameter|Type|Description|
|---------|----|-----------|
|hosts|array|Hosts to be monitored by the proxy. If a host is already monitored by a different proxy, it will be reassigned to the current proxy.<br><br>The hosts must have the `hostid` property defined. 由代理监视的主机。如果一个主机已经被另一个代理监视，那么它将被重新分配给当前代理。<br><br>此主机必须拥有`hostid`属性|
|interface|object|Host interface to be created for the passive proxy.<br><br>Required for passive proxies. 创建主机接口用于被动代理<br><br>被动代理的需求|

[comment]: # ({/new-fc26a415})

[comment]: # ({new-606a1465})
### Return values 返回值

`(object)` Returns an object containing the IDs of the created proxies
under the `proxyids` property. The order of the returned IDs matches the
order of the passed proxies.

`(object)`返回一个对象，该对象包含在`proxyids`属性下创建的代理的id。返回的id的顺序与所传递的代理的顺序相匹配。

[comment]: # ({/new-606a1465})

[comment]: # ({new-b41637d2})
### Examples 示例如下

[comment]: # ({/new-b41637d2})

[comment]: # ({new-86fb9238})
#### Create an active proxy 创建一个主动的代理

Create an action proxy "Active proxy" and assign a host to be monitored
by it. 创一个动作代理"Active proxy"，并分配一个由其监控的主机

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "proxy.create",
    "params": {
        "host": "Active proxy",
        "status": "5",
        "hosts": [
            {
                "hostid": "10279"
            }
        ]
    },
    "auth": "ab9638041ec6922cb14b07982b268f47",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "proxyids": [
            "10280"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-86fb9238})

[comment]: # ({new-8e1098f9})
#### Create a passive proxy 创建一个反向代理

Create a passive proxy "Passive proxy" and assign two hosts to be
monitored by it. 创建一个反向代理"Passive
proxy"，并分配2个由其监控的主机。

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "proxy.create",
    "params": {
        "host": "Passive proxy",
        "status": "6",
        "interface": {
            "ip": "127.0.0.1",
            "dns": "",
            "useip": "1",
            "port": "10051"
        },
        "hosts": [
            {
                "hostid": "10192"
            },
            {
                "hostid": "10139"
            }
        ]
    },
    "auth": "ab9638041ec6922cb14b07982b268f47",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "proxyids": [
            "10284"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-8e1098f9})

[comment]: # ({new-273e0fd8})
### See also

-   [Host](/manual/api/reference/host/object#host)
-   [Proxy interface](object#proxy_interface)

[comment]: # ({/new-273e0fd8})

[comment]: # ({new-453e4545})
### Source

CProxy::create() in
*frontends/php/include/classes/api/services/CProxy.php*.

[comment]: # ({/new-453e4545})
