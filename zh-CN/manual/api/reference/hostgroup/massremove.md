[comment]: # translation:outdated

[comment]: # ({new-8703e8df})
# 批量删除

[comment]: # ({/new-8703e8df})

[comment]: # ({new-bcf1ded4})
### 描述

`object hostgroup.massremove(object parameters)`

此方法允许从多个主机组中删除相关对象

[comment]: # ({/new-bcf1ded4})

[comment]: # ({new-6b7b7475})
### 参数

`(object)` 含要更新的主机组的ID和应该删除的对象的参数.

|参数            类|描述|<|
|---------------------|------|-|
|**groupids**<br>(必选)|字符串/数组   要更新主|组的ID.|
|hostids|字符串/数组   要从所有|机组中删除的主机.|
|templateids|字符串/数组   要从所有|机组中删除的模板.|

[comment]: # ({/new-6b7b7475})

[comment]: # ({new-736f3b05})
### 返回值

`(object)` 在`groupids`属性中返回已更新主机组ID的对象.

[comment]: # ({/new-736f3b05})

[comment]: # ({new-b41637d2})
### 示例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-cf36d4e8})
#### 从主机组中删除主机

从给定的主机组中删除两个主机

请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "hostgroup.massremove",
    "params": {
        "groupids": [
            "5",
            "6"
        ],
        "hostids": [
            "30050",
            "30001"
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "groupids": [
            "5",
            "6"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-cf36d4e8})

[comment]: # ({new-3700b99d})
### 来源

CHostGroup::massRemove() in
*frontends/php/include/classes/api/services/CHostGroup.php*.

[comment]: # ({/new-3700b99d})
