[comment]: # translation:outdated

[comment]: # ({new-524e7e9d})
# > 主机组对象

以下对象是和`hostgroup`直接相关的API

[comment]: # ({/new-524e7e9d})

[comment]: # ({new-52ddc167})
### 主机组

主机组对象有以下属性.

|属性        类|描述|<|
|-----------------|------|-|
|groupid|字符串   *(|读)* 主机组的ID.|
|**name**<br>(必选)|字符串   主机|的名称.|
|flags|整数     *|只读)* 主机组的来源.<br><br>可能值:<br>0 - 普通的主机组;<br>4 - 被发现的主机组.|
|internal|整数     *|只读)* 该组是否由系统内部使用,内部组无法被删除.<br><br>可能值:<br>0 - *(默认)* 不是内部;<br>1 - 内部.|

[comment]: # ({/new-52ddc167})
