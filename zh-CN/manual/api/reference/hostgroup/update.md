[comment]: # translation:outdated

[comment]: # ({new-259f747f})
# 更新

[comment]: # ({/new-259f747f})

[comment]: # ({new-32bf01b5})
### 描述

`object hostgroup.update(object/array hostGroups)`

此方法允许对已存在的主机组进行更新操作

[comment]: # ({/new-32bf01b5})

[comment]: # ({new-9c20e34d})
### 参数

`(object/array)` 要更新的[主机组属性](object#host_group).

必须为每个主机组定义`groupid`属性，所有其他属性都是可选的。只有给定的属性将被更新，所有其他属性将保持不变.

[comment]: # ({/new-9c20e34d})

[comment]: # ({new-736f3b05})
### 返回值

`(object)` 在`groupids`属性中返回包含已更新主机组ID的对象.

[comment]: # ({/new-736f3b05})

[comment]: # ({new-b41637d2})
### 示例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-a296ad88})
#### 重命名主机组

重命名`Linux hosts`主机组

请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "hostgroup.update",
    "params": {
        "groupid": "7",
        "name": "Linux hosts"
    },
    "auth": "700ca65537074ec963db7efabda78259",
    "id": 1
}
```

响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "groupids": [
            "7"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-a296ad88})

[comment]: # ({new-b75d89b4})
### 来源

CHostGroup::update() in
*frontends/php/include/classes/api/services/CHostGroup.php*.

[comment]: # ({/new-b75d89b4})
