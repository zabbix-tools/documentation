[comment]: # translation:outdated

[comment]: # ({new-5ebe3ff8})
# 创建

[comment]: # ({/new-5ebe3ff8})

[comment]: # ({new-06c5b0b6})
### 描述

`object hostgroup.create(object/array hostGroups)`

此方法允许创建新的主机组.

[comment]: # ({/new-06c5b0b6})

[comment]: # ({new-7c6d41ff})
### 参数

`(object/array)`
创建主机组.该方法接受具有[标准主机组属性](object#host_group)的主机组.

[comment]: # ({/new-7c6d41ff})

[comment]: # ({new-e3c164f1})
### 返回值

`(object)`
在`groupids`属性下返回包含已创建主机组ID的对象.返回主机组ID的顺序与传入的主机组顺序一致.

[comment]: # ({/new-e3c164f1})

[comment]: # ({new-b41637d2})
### 示例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-37cc26c2})
#### 创建主机组

创建名为"Linux servers"的主机组.

请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "hostgroup.create",
    "params": {
        "name": "Linux servers"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "groupids": [
            "107819"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-37cc26c2})

[comment]: # ({new-df4dc6d7})
### 来源

CHostGroup::create() in
*frontends/php/include/classes/api/services/CHostGroup.php*.

[comment]: # ({/new-df4dc6d7})
