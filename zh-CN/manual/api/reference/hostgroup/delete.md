[comment]: # translation:outdated

[comment]: # ({new-6a96957a})
# 删除

[comment]: # ({/new-6a96957a})

[comment]: # ({new-3c7f9c00})
### 描述

`object hostgroup.delete(array hostGroupIds)`

此方法允许删除主机组.

如果主机组有以下情况，则不能被删除:

-   包含仅属于该主机组的主机;
-   被标记为内部;
-   被主机原型引用;
-   在全局脚本中使用;
-   在相关条件下使用.

[comment]: # ({/new-3c7f9c00})

[comment]: # ({new-af1f0187})
### 参数

`(array)` 要删除主机组的ID.

[comment]: # ({/new-af1f0187})

[comment]: # ({new-288daefc})
### 返回值

`(object)` 在`groupids`属性中返回包含已删主机组ID的对象.

[comment]: # ({/new-288daefc})

[comment]: # ({new-b41637d2})
### 示例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-a6d8d11e})
#### 删除多个主机组

删除两个主机组.

请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "hostgroup.delete",
    "params": [
        "107824",
        "107825"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "groupids": [
            "107824",
            "107825"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-a6d8d11e})

[comment]: # ({new-f496d50e})
### 来源

CHostGroup::delete() in
*frontends/php/include/classes/api/services/CHostGroup.php*.

[comment]: # ({/new-f496d50e})
