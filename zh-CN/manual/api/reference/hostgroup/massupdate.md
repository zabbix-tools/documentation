[comment]: # translation:outdated

[comment]: # ({new-c07c9092})
# 批量更新

[comment]: # ({/new-c07c9092})

[comment]: # ({new-0ec0a392})
### 描述

`object hostgroup.massupdate(object parameters)`

该方法允许对多个主机组批量替换或删除相关对象

[comment]: # ({/new-0ec0a392})

[comment]: # ({new-d4028a54})
### 参数

`(object)` 包含要更新的主机组的ID和应更新的对象的参数.

|参数          类|描述|<|
|-------------------|------|-|
|**groups**<br>(必选)|对象/数组   要更新<br>|主机组.<br>主机组必须已定义`groupid`属性.|
|hosts|对象/数组   替换给|主机组上当前主机的主机.<br><br>主机必须已定义`hostid`属性.|
|templates|对象/数组   替换给|主机组上当前模板的模板.<br><br>模板必须已定义`templateid`属性.|

[comment]: # ({/new-d4028a54})

[comment]: # ({new-736f3b05})
### 返回值

`(object)` 在`groupids`属性中返回包含已更新主机组ID的对象.

[comment]: # ({/new-736f3b05})

[comment]: # ({new-b41637d2})
### 示例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-b4adf299})
#### 替换主机组中的主机

替换主机组ID的所有主机

请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "hostgroup.massupdate",
    "params": {
        "groups": [
            {
                "groupid": "6"
            }
        ],
        "hosts": [
            {
                "hostid": "30050"
            }
        ]
    },
    "auth": "f223adf833b2bf2ff38574a67bba6372",
    "id": 1
}
```

响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "groupids": [
            "6",
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-b4adf299})

[comment]: # ({new-a1410648})
### 参考

-   [hostgroup.update](update)
-   [hostgroup.massadd](massadd)
-   [Host](/manual/api/reference/host/object#host)
-   [Template](/manual/api/reference/template/object#template)

[comment]: # ({/new-a1410648})

[comment]: # ({new-243072aa})
### 来源

CHostGroup::massUpdate() in
*frontends/php/include/classes/api/services/CHostGroup.php*.

[comment]: # ({/new-243072aa})
