[comment]: # translation:outdated

[comment]: # ({new-3f9b711d})
# 批量添加

[comment]: # ({/new-3f9b711d})

[comment]: # ({new-30b8c9f4})
### 描述

`object hostgroup.massadd(object parameters)`

此方法允许给指定的主机组批量添加多个相关对象

[comment]: # ({/new-30b8c9f4})

[comment]: # ({new-b8ff6430})
### 参数

`(object)`包含要更新的主机组的ID和要添加到所有主机组的对象的参数.

该方法接受如下参数.

|参数          类|描述|<|
|-------------------|------|-|
|**groups**<br>(必选)|对象/数组   要更新<br>|主机组.<br>主机组必须已定义`groupid`属性.|
|hosts|对象/数组   添加到|有主机组的主机.<br><br>主机必须已定义`hostid`属性.|
|templates|对象/数组   添加到|有主机组的模板.<br><br>模板必须已定义`templateid`属性.|

[comment]: # ({/new-b8ff6430})

[comment]: # ({new-736f3b05})
### 返回值

`(object)` 在`groupids`属性中返回已更新主机组ID的对象.

[comment]: # ({/new-736f3b05})

[comment]: # ({new-b41637d2})
### 示例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-7698103b})
#### 给主机组添加主机

给ID为5和6的主机组添加两个主机

请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "hostgroup.massadd",
    "params": {
        "groups": [
            {
                "groupid": "5"
            },
            {
                "groupid": "6"
            }
        ],
        "hosts": [
            {
                "hostid": "30050"
            },
            {
                "hostid": "30001"
            }
        ]
    },
    "auth": "f223adf833b2bf2ff38574a67bba6372",
    "id": 1
}
```

响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "groupids": [
            "5",
            "6"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-7698103b})

[comment]: # ({new-e5f0abe1})
### 参考

-   [Host](/manual/api/reference/host/object#host)
-   [Template](/manual/api/reference/template/object#template)

[comment]: # ({/new-e5f0abe1})

[comment]: # ({new-19273fb6})
### 来源

CHostGroup::massAdd() in
*frontends/php/include/classes/api/services/CHostGroup.php*.

[comment]: # ({/new-19273fb6})
