[comment]: # translation:outdated

[comment]: # ({new-f8d4c2e5})
# 获取

[comment]: # ({/new-f8d4c2e5})

[comment]: # ({new-08ff22f2})
### 描述

`整数/数组 hostgroup.get(object parameters)`

该方法允许根据指定的参数获取主机组

[comment]: # ({/new-08ff22f2})

[comment]: # ({new-7f042696})
### 参数

`(object)` 定义期望输出的参数.

该方法支持以下参数:

|参数                          类|描述|<|
|-----------------------------------|------|-|
|graphids|字符串/数组   返回包含|有给定图表的主机或模板的主机组.|
|groupids|字符串/数组   返回给定|机组ID的主机组.|
|hostids|字符串/数组   返回包含|定主机的主机组.|
|maintenanceids|字符串/数组   返回受指|维护影响的主机组.|
|monitored\_hosts|标识          返|包含受监视主机的主机组.|
|real\_hosts|标识          返|包含主机的主机组.|
|templated\_hosts|标识          返|包含模板的主机组.|
|templateids|字符串/数组   返回包含|定模板的主机组.|
|triggerids|字符串/数组   返回包含|定触发器的主机或模板的主机组.|
|with\_applications|标识          返|给定应用集包含主机的主机组.|
|with\_graphs|标识          返|给定图表包含主机的主机组.|
|with\_hosts\_and\_templates|标识          返|包含主机*或* 模板的主机组.|
|with\_httptests|标识          返|给定web检查包含主机的主机组.<br><br>覆盖`with_monitored_httptests` 参数.|
|with\_items|标识          返|给定监控项包含主机或模板的主机组.<br><br>覆盖`with_monitored_items`和`with_simple_graph_items`参数.|
|with\_monitored\_httptests|标识          返|启用web检查包含主机的主机组.|
|with\_monitored\_items|标识          返|给定启动监控项包含主机或模板的主机组.<br><br>覆盖`with_simple_graph_items` 参数.|
|with\_monitored\_triggers|标识          返|给定启用触发器包含主机的主机组. 触发器中使用的监控项必须事先已经启用.|
|with\_simple\_graph\_items|标识          返|给定数字型监控项包含主机的主机组.|
|with\_triggers|标识          返|给定触发器包含主机的主机组.<br><br>覆盖`with_monitored_triggers`参数.|
|selectDiscoveryRule|查询          在|discoveryRule`属性中返回创建主机组的发现规则.|
|selectGroupDiscovery|查询          在|groupDiscovery`属性中返回主机组发现对象.<br><br>主机组发现对象将发现的主机组链接到主机组原型，并具有以下属性:<br>`groupid` - `(字符串)` I已经发现主机组的ID;<br>`lastcheck` - `(时间戳)` 主机组最后一次被发现的时间;<br>`name` - `(字符串)` 主机组原型的名称;<br>`parent_group_prototypeid` - `(字符串)` 创建主机组的主机组原型的ID;<br>`ts_delete` - `(时间戳)` 主机组不再被发现删除的时间.|
|selectHosts|查询          在|hosts`属性中返回归属主机组的主机.<br><br>支持 `count`.|
|selectTemplates|查询          在|tempaltes`属性中返回归属主机组的模板.<br><br>支持 `count`.|
|limitSelects|整数          限|子选择返回的记录数.<br><br>适用于以下子选项:<br>`selectHosts` - 结果将按`host`排序;<br>`selectTemplates` - 结果将按`host`排序.|
|sortfield|字符串/数组   根据给定|属性排序.<br><br>可能值: `groupid`, `name`.|
|countOutput|布尔值        这些|数对于所有`get`方法都是通用的，详情可参考[reference commentary](/manual/api/reference_commentary#common_get_method_parameters).|
|editable|布尔值        ::|<|
|excludeSearch|布尔值        ::|<|
|filter|对象          :|:|
|limit|整数          :|:|
|output|查询          :|:|
|preservekeys|布尔值        ::|<|
|search|对象          :|:|
|searchByAny|布尔值        ::|<|
|searchWildcardsEnabled|布尔值        ::|<|
|sortorder|字符串/数组   :::|<|
|startSearch|布尔值        ::|<|

[comment]: # ({/new-7f042696})

[comment]: # ({new-7223bab1})
### 返回值

`(整数/数组)` 返回:

-   一组对象;
-   如果使用了`countOutput`参数,返回对象的数量.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### 示例

[comment]: # ({/new-b41637d2})

[comment]: # ({new-8ed2756a})
#### 根据名称获取数据

获取所有关于主机组`Zabbix servers`和`Linux servers`的数据

请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "hostgroup.get",
    "params": {
        "output": "extend",
        "filter": {
            "name": [
                "Zabbix servers",
                "Linux servers"
            ]
        }
    },
    "auth": "6f38cddc44cfbb6c1bd186f9a220b5a0",
    "id": 1
}
```

响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "groupid": "2",
            "name": "Linux servers",
            "internal": "0"
        },
        {
            "groupid": "4",
            "name": "Zabbix servers",
            "internal": "0"
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-8ed2756a})

[comment]: # ({new-e5f0abe1})
### 参考

-   [Host](/manual/api/reference/host/object#host)
-   [Template](/manual/api/reference/template/object#template)

[comment]: # ({/new-e5f0abe1})

[comment]: # ({new-1e72ea39})
### 来源

CHostGroup::get() in
*frontends/php/include/classes/api/services/CHostGroup.php*.

[comment]: # ({/new-1e72ea39})
