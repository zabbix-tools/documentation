[comment]: # translation:outdated

[comment]: # ({new-6a93039d})
# graphprototype.get

[comment]: # ({/new-6a93039d})

[comment]: # ({new-27b69e53})
### Description

[comment]: # ({/new-27b69e53})

[comment]: # ({new-de0f74af})
### 描述

`integer/array graphprototype.get(object parameters)`
`整数/数组 graphprototype.get(object parameters)`

The method allows to retrieve graph prototypes according to the given
parameters. 此方法用于根据给定的参数来获取图形原型

[comment]: # ({/new-de0f74af})

[comment]: # ({new-7223bab1})
### Parameters

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### 参数

`(object)` Parameters defining the desired output. `(对象)`
定义所需输出的参数.

The method supports the following parameters. 此方法支持以下参数:

|Parameter|Type|Description|
|---------|----|-----------|
|discoveryids|string/array|Return only graph prototypes that belong to the given discovery rules.|
|graphids|string/array|Return only graph prototypes with the given IDs.|
|groupids|string/array|Return only graph prototypes that belong to hosts in the given host groups.|
|hostids|string/array|Return only graph prototypes that belong to the given hosts.|
|inherited|boolean|If set to `true` return only graph prototypes inherited from a template.|
|itemids|string/array|Return only graph prototypes that contain the given item prototypes.|
|templated|boolean|If set to `true` return only graph prototypes that belong to templates.|
|templateids|string/array|Return only graph prototypes that belong to the given templates.|
|selectDiscoveryRule|query|Return the LLD rule that the graph prototype belongs to in the `discoveryRule` property.|
|selectGraphItems|query|Return the graph items used in the graph prototype in the `gitems` property.|
|selectGroups|query|Return the host groups that the graph prototype belongs to in the `groups` property.|
|selectHosts|query|Return the hosts that the graph prototype belongs to in the `hosts` property.|
|selectItems|query|Return the items and item prototypes used in the graph prototype in the `items` property.|
|selectTemplates|query|Return the templates that the graph prototype belongs to in the `templates` property.|
|filter|object|Return only those results that exactly match the given filter.<br><br>Accepts an array, where the keys are property names, and the values are either a single value or an array of values to match against.<br><br>Supports additional filters:<br>`host` - technical name of the host that the graph prototype belongs to;<br>`hostid` - ID of the host that the graph prototype belongs to.|
|sortfield|string/array|Sort the result by the given properties.<br><br>Possible values are: `graphid`, `name` and `graphtype`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters).|
|editable|boolean|^|
|excludeSearch|boolean|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

|参数                     类|描述|<|
|------------------------------|------|-|
|discoveryids|string/array|仅返回属于给定自动发现规则的图形原型.|
|graphids|string/array|仅返回含有给定ID的图形原型.|
|groupids|string/array|仅返回属于给定主机组的主机的图形原型.|
|hostids|string/array|仅返回属于给定主机的图形原型.|
|inherited|boolean|如果设置此参数为 `true` ，则仅返回从模板继承的图形原型.|
|itemids|string/array|仅返回包含给定监控项原型的图形原型.|
|templated|boolean|如果设置此参数为 `true` ，则仅返回属于模板的图形原型.|
|templateids|string/array|仅返回属于给定模板的图形原型.|
|selectDiscoveryRule|query|在 `discoveryRule` 属性下，返回图形原型所属的低级别发现规则.|
|selectGraphItems|query|在 `gitems` 属性下，返回在图形原型中使用的图表监控项.|
|selectGroups|query|在 `groups` 属性下，返回图形原型所属的主机组.|
|selectHosts|query|在 `hosts` 属性下，返回图形原型所属的主机.|
|selectItems|query|在 `items` 属性下，返回在图形原型中使用的监控项以及监控项原型.|
|selectTemplates|query|在 `templates` 属性下，返回图形原型所属的模板.|
|filter|object|仅返回精确匹配给定过滤器的结果.<br><br>接受一个数组，其中键是属性名称，值是单个值或要匹配的值的数组.<br><br>支持的额外的过滤器:<br>`host` - 图型原型所属主机的技术名称.<br>`hostid` - 图形原型所属主机的ID.|
|sortfield|string/array|根据给定属性对结果进行排序.<br><br>可能值: `graphid`, `name` 以及 `graphtype`.|
|countOutput|boolean|以下参数为 `get` 方法通常参数，在[参考注释](/zh/manual/api/reference_commentary#common_get_method_parameters)有详细说明...|
|editable|boolean|^|
|excludeSearch|boolean|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

[comment]: # ({/new-b41637d2})

[comment]: # ({new-62edff03})
### Return values

[comment]: # ({/new-62edff03})

[comment]: # ({new-e4f96e80})
### 返回值

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.

`(整数/数组)` 返回:

-   一个数组对象;
-   如果使用了 `countOutput` 参数，返回获取的对象的数量..

[comment]: # ({/new-e4f96e80})

[comment]: # ({new-fc80fda8})
### Examples

### 例子

#### Retrieving graph prototypes from a LLD rule

#### 从低级别发现规则获取图形原型

Retrieve all graph prototypes from an LLD rule.
从低级别发现规则获取所有图形原型。

Request: 请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "graphprototype.get",
    "params": {
        "output": "extend",
        "discoveryids": "27426"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response: 响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "graphid": "1017",
            "parent_itemid": "27426",
            "name": "Disk space usage {#FSNAME}",
            "width": "600",
            "height": "340",
            "yaxismin": "0.0000",
            "yaxismax": "0.0000",
            "templateid": "442",
            "show_work_period": "0",
            "show_triggers": "0",
            "graphtype": "2",
            "show_legend": "1",
            "show_3d": "1",
            "percent_left": "0.0000",
            "percent_right": "0.0000",
            "ymin_type": "0",
            "ymax_type": "0",
            "ymin_itemid": "0",
            "ymax_itemid": "0"
        }
    ],
    "id": 1
}
```

### See also

### 参考

-   [Discovery
    rule](/manual/api/reference/discoveryrule/object#discovery_rule)
-   [Graph item](/manual/api/reference/graphitem/object#graph_item)
-   [Item](/manual/api/reference/item/object#item)
-   [Host](/manual/api/reference/host/object#host)
-   [Host group](/manual/api/reference/hostgroup/object#host_group)
-   [Template](/manual/api/reference/template/object#template)

```{=html}
<!-- -->
```
-   [发现规则](/zh/manual/api/reference/discoveryrule/object#discovery_rule)
-   [图表监控项](/zh/manual/api/reference/graphitem/object#graph_item)
-   [监控项](/zh/manual/api/reference/item/object#item)
-   [主机](/zh/manual/api/reference/host/object#host)
-   [主机组](/zh/manual/api/reference/hostgroup/object#host_group)
-   [模板](/zh/manual/api/reference/template/object#template)

### Source

### 来源

CGraphPrototype::get() in
*frontends/php/include/classes/api/services/CGraphPrototype.php*.

[comment]: # ({/new-fc80fda8})
