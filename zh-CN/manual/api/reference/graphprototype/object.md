[comment]: # translation:outdated

[comment]: # ({new-07413af4})
# > Graph prototype object

The following objects are directly related to the `graphprototype` API.
以下对象与 `graphprototype` API直接相关。

[comment]: # ({/new-07413af4})

[comment]: # ({new-c613d83a})
### Graph prototype

### 图形原型

The graph prototype object has the following properties.
图形原型对象具有以下属性:

|Property|Type|Description|
|--------|----|-----------|
|graphid|string|*(readonly)* ID of the graph prototype.|
|**height**<br>(required)|integer|Height of the graph prototype in pixels.|
|**name**<br>(required)|string|Name of the graph prototype.|
|**width**<br>(required)|integer|Width of the graph prototype in pixels.|
|graphtype|integer|Graph prototypes's layout type.<br><br>Possible values:<br>0 - *(default)* normal;<br>1 - stacked;<br>2 - pie;<br>3 - exploded.|
|percent\_left|float|Left percentile.<br><br>Default: 0.|
|percent\_right|float|Right percentile.<br><br>Default: 0.|
|show\_3d|integer|Whether to show discovered pie and exploded graphs in 3D.<br><br>Possible values:<br>0 - *(default)* show in 2D;<br>1 - show in 3D.|
|show\_legend|integer|Whether to show the legend on the discovered graph.<br><br>Possible values:<br>0 - hide;<br>1 - *(default)* show.|
|show\_work\_period|integer|Whether to show the working time on the discovered graph.<br><br>Possible values:<br>0 - hide;<br>1 - *(default)* show.|
|templateid|string|*(readonly)* ID of the parent template graph prototype.|
|yaxismax|float|The fixed maximum value for the Y axis.|
|yaxismin|float|The fixed minimum value for the Y axis.|
|ymax\_itemid|string|ID of the item that is used as the maximum value for the Y axis.|
|ymax\_type|integer|Maximum value calculation method for the Y axis.<br><br>Possible values:<br>0 - *(default)* calculated;<br>1 - fixed;<br>2 - item.|
|ymin\_itemid|string|ID of the item that is used as the minimum value for the Y axis.|
|ymin\_type|integer|Minimum value calculation method for the Y axis.<br><br>Possible values:<br>0 - *(default)* calculated;<br>1 - fixed;<br>2 - item.|

|属性                 类|描述|<|
|--------------------------|------|-|
|graphid|string|*(只读)* 图形原型的ID.|
|**height**<br>(必选)|integer|图形原型的高度（单位：像素）.|
|**name**<br>(必选)|string|图形原型的名称.|
|**width**<br>(必选)|integer|图形原型的宽度（单位：像素）|
|graphtype|integer|图形原型布局类型 .<br><br>可能值:<br>0 - *(默认)* 常规;<br>1 - 堆积图;<br>2 - 饼图;<br>3 - 分散饼图.|
|percent\_left|float|左侧百分比线.<br><br>默认: 0.|
|percent\_right|float|右侧百分比线.<br><br>默认: 0.|
|show\_3d|integer|否使用3D形式显示被发现的饼图和分散饼图.<br><br>可能值:<br>0 - *(默认)* 以2D形式展示;<br>1 - 以3D形式展示.|
|show\_legend|integer|是否在被发现的图表上显示图例.<br><br>可能值:<br>0 - 隐藏;<br>1 - *(默认)* 显示.|
|show\_work\_period|integer|是否在发现的图表上显示工作时间.<br><br>可能值:<br>0 - 隐藏;<br>1 - *(默认)* 显示.|
|templateid|string|*(只读)* 图形原形的父模板的ID.|
|yaxismax|float|Y轴的固定最大值.|
|yaxismin|float|Y轴的固定最小值.|
|ymax\_itemid|string|用于作为Y轴最大值的监控项ID.|
|ymax\_type|integer|Y轴最大值的计算方式.<br><br>可能值:<br>0 - *(默认)* 计算的;<br>1 - 固定的;<br>2 - 监控项.|
|ymin\_itemid|string|用于作为Y轴最小值的监控项ID.|
|ymin\_type|integer|Y轴最小值的计算方式.<br><br>可能值:<br>0 - *(默认)* 计算的;<br>1 - 固定的;<br>2 - 监控项.|

[comment]: # ({/new-c613d83a})
