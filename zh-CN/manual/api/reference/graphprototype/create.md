[comment]: # translation:outdated

[comment]: # ({new-576421be})
# graphprototype.create

[comment]: # ({/new-576421be})

[comment]: # ({new-fc8d3493})
### Description

[comment]: # ({/new-fc8d3493})

[comment]: # ({new-9beca929})
### 描述

`object graphprototype.create(object/array graphPrototypes)`
`对象 graphprototype.create(object/array graphPrototypes)`

This method allows to create new graph prototypes.
此方法用于创建新的图形原型

[comment]: # ({/new-9beca929})

[comment]: # ({new-f1317691})
### Parameters

[comment]: # ({/new-f1317691})

[comment]: # ({new-b41637d2})
### 参数

`(object/array)` Graph prototypes to create. `(对象/数组)`
将要创建的图形原型

Additionally to the [standard graph prototype
properties](object#graph_prototype), the method accepts the following
parameters. 除了[标准图形原型参数](object#graph_prototype)外,
此方法还接受以下参数：

|Parameter|Type|Description|
|---------|----|-----------|
|**gitems**<br>(required)|array|Graph items to be created for the graph prototypes. Graph items can reference both items and item prototypes, but at least one item prototype must be present.|

|参数          类|描述|<|
|-------------------|------|-|
|**gitems**<br>(必选)|array|创建到图形原型中的图表监控项. 图表监控项能同时被监控项与监控项原型检索到，但必须至少有一个监控项原型。|

[comment]: # ({/new-b41637d2})

[comment]: # ({new-24ec4c22})
### Return values

[comment]: # ({/new-24ec4c22})

[comment]: # ({new-88266563})
### 返回值

`(object)` Returns an object containing the IDs of the created graph
prototypes under the `graphids` property. The order of the returned IDs
matches the order of the passed graph prototypes. `(对象)`
在`graphids`属性下，返回一个包含已被创建的图形原型ID的对象。返回的ID的顺序与传递的图形原型的顺序相匹配。

[comment]: # ({/new-88266563})

[comment]: # ({new-c4b4834a})
### Examples

### 例子

#### Creating a graph prototype

#### 创建一个图形原型

Create a graph prototype with two items.
创建一个含有两个监控项的图形原型。

Request: 请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "graphprototype.create",
    "params": {
        "name": "Disk space usage {#FSNAME}",
        "width": 900,
        "height": 200,
        "gitems": [
            {
                "itemid": "22828",
                "color": "00AA00"
            },
            {
                "itemid": "22829",
                "color": "3333FF"
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response: 响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "graphids": [
            "652"
        ]
    },
    "id": 1
}
```

### See also

### 参考

-   [Graph item](/manual/api/reference/graphitem/object#graph_item)
-   [图表监控项](/zh/manual/api/reference/graphitem/object#graph_item)

### Source

### 来源

CGraphPrototype::create() in
*frontends/php/include/classes/api/services/CGraphPrototype.php*.

[comment]: # ({/new-c4b4834a})
