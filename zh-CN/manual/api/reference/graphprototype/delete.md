[comment]: # translation:outdated

[comment]: # ({new-0a247a7d})
# graphprototype.delete

[comment]: # ({/new-0a247a7d})

[comment]: # ({new-8256b270})
### Description

[comment]: # ({/new-8256b270})

[comment]: # ({new-3e4a53cc})
### 描述

`object graphprototype.delete(array graphPrototypeIds)`
`对象 graphprototype.delete(array graphPrototypeIds)`

This method allows to delete graph prototypes. 此方法用于删除图形原型

[comment]: # ({/new-3e4a53cc})

[comment]: # ({new-82123f70})
### Parameters

[comment]: # ({/new-82123f70})

[comment]: # ({new-b41637d2})
### 参数

`(array)` IDs of the graph prototypes to delete. `(数组)`
需要删除的图形原型的ID

[comment]: # ({/new-b41637d2})

[comment]: # ({new-d24b4f06})
### Return values

[comment]: # ({/new-d24b4f06})

[comment]: # ({new-21f88b2f})
### 返回值

`(object)` Returns an object containing the IDs of the deleted graph
prototypes under the `graphids` property. `(对象)` 在 `graphids`
属性下，返回一个包含已经删除的图形原型的ID的对象。

### Examples

### 例子

#### Deleting multiple graph prototypes

#### 删除多个图形原型

Delete two graph prototypes. 删除两个图形原型

Request: 请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "graphprototype.delete",
    "params": [
        "652",
        "653"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Response: 响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "graphids": [
            "652",
            "653"
        ]
    },
    "id": 1
}
```

### Source

### 来源

CGraphPrototype::delete() in
*frontends/php/include/classes/api/services/CGraphPrototype.php*.

[comment]: # ({/new-21f88b2f})
