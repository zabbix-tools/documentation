[comment]: # translation:outdated

[comment]: # ({new-a23a9b82})
# graphprototype.update

[comment]: # ({/new-a23a9b82})

[comment]: # ({new-5ac42d98})
### Description

[comment]: # ({/new-5ac42d98})

[comment]: # ({new-c4171dc6})
### 描述

`object graphprototype.update(object/array graphPrototypes)`
`对象 graphprototype.update(object/array graphPrototypes)`

This method allows to update existing graph prototypes.
此方法用于更新已存在的图形原型。

[comment]: # ({/new-c4171dc6})

[comment]: # ({new-8d626778})
### Parameters

[comment]: # ({/new-8d626778})

[comment]: # ({new-b41637d2})
### 参数

`(object/array)` Graph prototype properties to be updated. `(对象/数组)`
需要更新的图形原型.

The `graphid` property must be defined for each graph prototype, all
other properties are optional. Only the passed properties will be
updated, all others will remain unchanged. `graphid`
属性必须定义，其它属性均为可选。只有被传递的属性会被更新，其它都会保持不变。

Additionally to the [standard graph prototype
properties](object#graph_prototype), the method accepts the following
parameters. 除了
[标准图形原型属性](object#graph_prototype)外，此方法还接受以下参数：

|Parameter|Type|Description|
|---------|----|-----------|
|gitems|array|Graph items to replace existing graph items. If a graph item has the `gitemid` property defined it will be updated, otherwise a new graph item will be created.|

|参数     类|描述|<|
|--------------|------|-|
|gitems|array|用于替换现有图形监控项的图形监控项. 如果图表项定义了 `gitemid` 属性，它将被更新，否则将创建一个新的图形监控项.|

[comment]: # ({/new-b41637d2})

[comment]: # ({new-c044eff0})
### Return values

[comment]: # ({/new-c044eff0})

[comment]: # ({new-743007eb})
### 返回值

`(object)` Returns an object containing the IDs of the updated graph
prototypes under the `graphids` property. `(对象)` 在 `graphids`
属性下，返回一个已更新的图形原型的对象的ID。

### Examples

### 例子

#### Changing the size of a graph prototype

#### 更新图形原型大小

Change the size of a graph prototype to 1100 to 400 pixels.
将图形原型的大小从1100更新为400(单位：像素)

Request: 请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "graphprototype.update",
    "params": {
        "graphid": "439",
        "width": 1100,
        "height": 400
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response: 响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "graphids": [
            "439"
        ]
    },
    "id": 1
}
```

### Source

### 来源

CGraphPrototype::update() in
*frontends/php/include/classes/api/services/CGraphPrototype.php*.

[comment]: # ({/new-743007eb})
