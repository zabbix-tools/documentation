[comment]: # translation:outdated

[comment]: # ({new-830462e8})
# 服务

This class is designed to work with services. 该类用于配合服务使用。

Object references 对象引用:\

-   [Service](/zh/manual/api/reference/service/object#service)
-   [Service time](/zh/manual/api/reference/service/object#service_time)
-   [Service
    dependency](/zh/manual/api/reference/service/object#service_dependency)
-   [Service
    alarm](/zh/manual/api/reference/service/object#service_alarm)

Available methods 可用方法:\

-   [service.adddependencies](/zh/manual/api/reference/service/adddependencies) -
    adding dependencies between IT services 增加IT服务之间的依赖关系
-   [service.addtimes](/zh/manual/api/reference/service/addtimes) -
    adding service times 增加服务时间
-   [service.create](/zh/manual/api/reference/service/create) - creating
    new IT services 创建新的IT服务
-   [service.delete](/zh/manual/api/reference/service/delete) - deleting
    IT services 删除IT服务
-   [service.deletedependencies](/zh/manual/api/reference/service/deletedependencies) -
    deleting dependencies between IT services 删除IT服务之间的依赖关系
-   [service.deletetimes](/zh/manual/api/reference/service/deletetimes) -
    deleting service times 删除服务时间
-   [service.get](/zh/manual/api/reference/service/get) - retrieving IT
    services 检索IT服务
-   [service.getsla](/zh/manual/api/reference/service/getsla) -
    retrieving availability information about IT services
    检索有关IT服务的可用性信息
-   [service.update](/zh/manual/api/reference/service/update) - updating
    IT services 更新IT服务

[comment]: # ({/new-830462e8})
