[comment]: # translation:outdated

[comment]: # ({new-c0099aca})
# > Graph object

[comment]: # ({/new-c0099aca})

[comment]: # ({new-9849a58f})
## > 图表对象

The following objects are directly related to the `graph` API.
以下对象与 `图表` API直接相关。

### Graph

### 图表

The graph object has the following properties. 图表对象具有以下属性:

|Property|Type|Description|
|--------|----|-----------|
|graphid|string|*(readonly)* ID of the graph.|
|**height**<br>(required)|integer|Height of the graph in pixels.|
|**name**<br>(required)|string|Name of the graph|
|**width**<br>(required)|integer|Width of the graph in pixels.|
|flags|integer|*(readonly)* Origin of the graph.<br><br>Possible values are:<br>0 - *(default)* a plain graph;<br>4 - a discovered graph.|
|graphtype|integer|Graph's layout type.<br><br>Possible values:<br>0 - *(default)* normal;<br>1 - stacked;<br>2 - pie;<br>3 - exploded.|
|percent\_left|float|Left percentile.<br><br>Default: 0.|
|percent\_right|float|Right percentile.<br><br>Default: 0.|
|show\_3d|integer|Whether to show pie and exploded graphs in 3D.<br><br>Possible values:<br>0 - *(default)* show in 2D;<br>1 - show in 3D.|
|show\_legend|integer|Whether to show the legend on the graph.<br><br>Possible values:<br>0 - hide;<br>1 - *(default)* show.|
|show\_work\_period|integer|Whether to show the working time on the graph.<br><br>Possible values:<br>0 - hide;<br>1 - *(default)* show.|
|templateid|string|*(readonly)* ID of the parent template graph.|
|yaxismax|float|The fixed maximum value for the Y axis.<br><br>Default: 100.|
|yaxismin|float|The fixed minimum value for the Y axis.<br><br>Default: 0.|
|ymax\_itemid|string|ID of the item that is used as the maximum value for the Y axis.|
|ymax\_type|integer|Maximum value calculation method for the Y axis.<br><br>Possible values:<br>0 - *(default)* calculated;<br>1 - fixed;<br>2 - item.|
|ymin\_itemid|string|ID of the item that is used as the minimum value for the Y axis.|
|ymin\_type|integer|Minimum value calculation method for the Y axis.<br><br>Possible values:<br>0 - *(default)* calculated;<br>1 - fixed;<br>2 - item.|

|属性                 类|描述|<|
|--------------------------|------|-|
|graphid|string|*(只读)* 图表的ID.|
|**height**<br>(必选)|integer|图表的高度(单位:像素).|
|**name**<br>(必选)|string|图表的名称|
|**width**<br>(必选)|integer|图表的宽度(单位:像素).|
|flags|integer|*(readonly)* 图表的来源.<br><br>可用值:<br>0 - *(默认)* 简单的图表;<br>4 - 发现的图表.|
|graphtype|integer|图表的类型.<br><br>可能值:<br>0 - *(默认)* 常规;<br>1 - 堆积图;<br>2 - 饼图;<br>3 - 分散饼图.|
|percent\_left|float|百分比线(左).<br><br>默认: 0.|
|percent\_right|float|百分比线(右).<br><br>默认: 0.|
|show\_3d|integer|是否以3D形式展示饼图和分散饼图.<br><br>可用值:<br>0 - *(默认)* 以2D展示;<br>1 - 以3D展示.|
|show\_legend|integer|是否在图表上显示图例.<br><br>可用值:<br>0 - 隐藏;<br>1 - *(默认)* 显示.|
|show\_work\_period|integer|是否在图表上显示工作时间.<br><br>可用值:<br>0 - 隐藏;<br>1 - *(默认)* 显示.|
|templateid|string|*(只读)* 父模板图表的ID.|
|yaxismax|float|Y轴的固定最大值.<br><br>默认: 100.|
|yaxismin|float|Y轴的固定最小值.<br><br>默认: 0.|
|ymax\_itemid|string|用于作为Y轴最大值的监控项ID.|
|ymax\_type|integer|Y轴最大值的计算方式.<br><br>可用值:<br>0 - *(默认)* 可计算的;<br>1 - 固定的;<br>2 - 监控项.|
|ymin\_itemid|string|用于作为Y轴最小值的监控项ID.|
|ymin\_type|integer|Y轴最小值的计算方式.<br><br>可用值:<br>0 - *(默认)* 可计算的;<br>1 - 固定的;<br>2 - 监控项.|

[comment]: # ({/new-9849a58f})
