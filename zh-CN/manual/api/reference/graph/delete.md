[comment]: # translation:outdated

[comment]: # ({new-cf547db2})
# graph.delete

[comment]: # ({/new-cf547db2})

[comment]: # ({new-c740de99})
### Description

[comment]: # ({/new-c740de99})

[comment]: # ({new-54514063})
### 描述

`object graph.delete(array graphIds)`
`对象 graph.delete(array graphIds)`

This method allows to delete graphs. 此方法用于删除图表.

[comment]: # ({/new-54514063})

[comment]: # ({new-eb5c8bd8})
### Parameters

[comment]: # ({/new-eb5c8bd8})

[comment]: # ({new-b41637d2})
### 参数

`(array)` IDs of the graphs to delete. `(数组)` 要删除的图表的ID.

[comment]: # ({/new-b41637d2})

[comment]: # ({new-3a78f2ec})
### Return values

[comment]: # ({/new-3a78f2ec})

[comment]: # ({new-e06af123})
### 返回值

`(object)` Returns an object containing the IDs of the deleted graphs
under the `graphids` property. `(对象)` 在 `graphids`
属性下，返回一个包含已删除图表的对象.

### Examples

### 例子

#### Deleting multiple graphs

#### 删除多个图表

Delete two graphs. 删除两个图表

Request: 请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "graph.delete",
    "params": [
        "652",
        "653"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Response: 响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "graphids": [
            "652",
            "653"
        ]
    },
    "id": 1
}
```

### Source

### 来源

CGraph::delete() in
*frontends/php/include/classes/api/services/CGraph.php*.

[comment]: # ({/new-e06af123})
