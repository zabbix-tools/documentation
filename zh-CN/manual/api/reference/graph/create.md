[comment]: # translation:outdated

[comment]: # ({new-000fdd04})
# graph.create

[comment]: # ({/new-000fdd04})

[comment]: # ({new-e0ee1e5d})
### Description

[comment]: # ({/new-e0ee1e5d})

[comment]: # ({new-fc8b02f4})
### 描述

`object graph.create(object/array graphs)`
`对象 graph.create(object/array graphs)`

This method allows to create new graphs. 此方法用于创建新的图表

[comment]: # ({/new-fc8b02f4})

[comment]: # ({new-d64b873d})
### Parameters

[comment]: # ({/new-d64b873d})

[comment]: # ({new-b41637d2})
### 参数

`(object/array)` Graphs to create. `(对象/数组)` 要创建的图表.

Additionally to the [standard graph properties](object#graph), the
method accepts the following parameters. 除了
[标准图表属性](object#graph) 外，此方法还接受以下参数。

|Parameter|Type|Description|
|---------|----|-----------|
|**gitems**<br>(required)|array|Graph items to be created for the graph.|

|参数          类|描述|<|
|-------------------|------|-|
|**gitems**<br>(必选)|array|创建到图表中的监控项.|

[comment]: # ({/new-b41637d2})

[comment]: # ({new-cd12048c})
### Return values

[comment]: # ({/new-cd12048c})

[comment]: # ({new-88266563})
### 返回值

`(object)` Returns an object containing the IDs of the created graphs
under the `graphids` property. The order of the returned IDs matches the
order of the passed graphs. `(对象)` 在 `graphids`
属性下，返回一个包含已创建图表ID的对象.
返回ID的顺序与传递图表的顺序想在匹配.

[comment]: # ({/new-88266563})

[comment]: # ({new-4e535c8e})
### Examples

### 例子

#### Creating a graph

#### 创建一个图表

Create a graph with two items. 创建一个包含两个监控项的图表.

Request: 请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "graph.create",
    "params": {
        "name": "MySQL bandwidth",
        "width": 900,
        "height": 200,
        "gitems": [
            {
                "itemid": "22828",
                "color": "00AA00"
            },
            {
                "itemid": "22829",
                "color": "3333FF"
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response: 响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "graphids": [
            "652"
        ]
    },
    "id": 1
}
```

### See also

### 参考

-   [Graph item](/manual/api/reference/graphitem/object#graph_item)
-   [图表监控项](/zh/manual/api/reference/graphitem/object#graph_item)

### Source

### 来源

CGraph::create() in
*frontends/php/include/classes/api/services/CGraph.php*.

[comment]: # ({/new-4e535c8e})
