[comment]: # translation:outdated

[comment]: # ({new-cdc53a5f})
# graph.get

[comment]: # ({/new-cdc53a5f})

[comment]: # ({new-d2f8406d})
### Description

[comment]: # ({/new-d2f8406d})

[comment]: # ({new-5b1c5b22})
### 描述

`integer/array graph.get(object parameters)`
`整数/数组 graph.get(object parameters)`

The method allows to retrieve graphs according to the given parameters.
此方法用于根据给定的参数来获取图表。

[comment]: # ({/new-5b1c5b22})

[comment]: # ({new-7223bab1})
### Parameters

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### 参数

`(object)` Parameters defining the desired output. `(对象)`
定义所需输出的参数.

The method supports the following parameters. 此方法支持以下参数

|Parameter|Type|Description|
|---------|----|-----------|
|graphids|string/array|Return only graphs with the given IDs.|
|groupids|string/array|Return only graphs that belong to hosts in the given host groups.|
|templateids|string/array|Return only graph that belong to the given templates.|
|hostids|string/array|Return only graphs that belong to the given hosts.|
|itemids|string/array|Return only graphs that contain the given items.|
|templated|boolean|If set to `true` return only graphs that belong to templates.|
|inherited|boolean|If set to `true` return only graphs inherited from a template.|
|expandName|flag|Expand macros in the graph name.|
|selectGroups|query|Return the host groups that the graph belongs to in the `groups` property.|
|selectTemplates|query|Return the templates that the graph belongs to in the `templates` property.|
|selectHosts|query|Return the hosts that the graph belongs to in the `hosts` property.|
|selectItems|query|Return the items used in the graph in the `items` property.|
|selectGraphDiscovery|query|Return the graph discovery object in the `graphDiscovery` property. The graph discovery objects links the graph to a graph prototype from which it was created.<br><br>It has the following properties:<br>`graphid` - `(string)` ID of the graph;<br>`parent_graphid` - `(string)` ID of the graph prototype from which the graph has been created.|
|selectGraphItems|query|Return the graph items used in the graph in the `gitems` property.|
|selectDiscoveryRule|query|Return the low-level discovery rule that created the graph in the `discoveryRule` property.|
|filter|object|Return only those results that exactly match the given filter.<br><br>Accepts an array, where the keys are property names, and the values are either a single value or an array of values to match against.<br><br>Supports additional filters:<br>`host` - technical name of the host that the graph belongs to;<br>`hostid` - ID of the host that the graph belongs to.|
|sortfield|string/array|Sort the result by the given properties.<br><br>Possible values are: `graphid`, `name` and `graphtype`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters) page.|
|editable|boolean|^|
|excludeSearch|boolean|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

|参数                     类|描述|<|
|------------------------------|------|-|
|graphids|string/array|仅返回含有给定ID的图表.|
|groupids|string/array|仅返回属于给定主机组的主机的图表.|
|templateids|string/array|仅返回属于给定模板的图表.|
|hostids|string/array|仅返回属于给定主机的图表.|
|itemids|string/array|仅返回包含给定监控项的图表.|
|templated|boolean|如果设置为 `真(true)`,仅返回属于模板的图表.|
|inherited|boolean|如果设置为 `真(true)`,仅返回从模板继承的图表.|
|expandName|flag|在图表名称中展开宏.|
|selectGroups|query|在 `groups` 属性下，返回图表所属的主机组.|
|selectTemplates|query|在 `templates` 属性下，返回图表所属的模板.|
|selectHosts|query|在 `hosts` 属性下，返回图表所属的主机.|
|selectItems|query|在 `items` 属性下，返回图表使用的监控项.|
|selectGraphDiscovery|query|在 `graphDiscovery` 属性下，返回图表发现对象. 图表发现对象将图表链接到创建它的图表原型.<br><br>它具有以下参数:<br>`graphid` - `(string)` 图表的ID;<br>`parent_graphid` - `(string)` 已创建图表的图表原型的ID.|
|selectGraphItems|query|在 `gitems` 属性下，返回图表所使用的图表监控项.|
|selectDiscoveryRule|query|在 `discoveryRule` 属性下，返回创建此图表的低级别发现规则.|
|filter|object|仅返回完全匹配给定过滤规则的结果.<br><br>接受一个数组，其中键是属性名称，值是单个值或要匹配的值数组.<br><br>支持额外的过滤器:<br>`host` - 图表所属主机的名称;<br>`hostid` - 图表所属主机的ID.|
|sortfield|string/array|按给定属性将结果排序.<br><br>可能值: `graphid`, `name` and `graphtype`.|
|countOutput|boolean|以下参数为get方法通常参数，在[参考注释](/zh/manual/api/reference_commentary#common_get_method_parameters)有详细说明..|
|editable|boolean|^|
|excludeSearch|boolean|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

[comment]: # ({/new-b41637d2})

[comment]: # ({new-e7fae381})
### Return values

[comment]: # ({/new-e7fae381})

[comment]: # ({new-e4f96e80})
### 返回值

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.

`(整数/级数)` 返回:

-   一个数组对象;
-   如果使用了 `countOutput` 参数，返回获取的对象的数量.

[comment]: # ({/new-e4f96e80})

[comment]: # ({new-6f7b2d0a})
### Examples

### 例子

#### Retrieving graphs from hosts

#### 从主机中获取图表

Retrieve all graphs from host "10107" and sort them by name.
从主机"10107"中获取所有图表，并依据名称进行排序。

Request: 请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "graph.get",
    "params": {
        "output": "extend",
        "hostids": 10107,
        "sortfield": "name"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response: 响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "graphid": "612",
            "name": "CPU jumps",
            "width": "900",
            "height": "200",
            "yaxismin": "0.0000",
            "yaxismax": "100.0000",
            "templateid": "439",
            "show_work_period": "1",
            "show_triggers": "1",
            "graphtype": "0",
            "show_legend": "1",
            "show_3d": "0",
            "percent_left": "0.0000",
            "percent_right": "0.0000",
            "ymin_type": "0",
            "ymax_type": "0",
            "ymin_itemid": "0",
            "ymax_itemid": "0",
            "flags": "0"
        },
        {
            "graphid": "613",
            "name": "CPU load",
            "width": "900",
            "height": "200",
            "yaxismin": "0.0000",
            "yaxismax": "100.0000",
            "templateid": "433",
            "show_work_period": "1",
            "show_triggers": "1",
            "graphtype": "0",
            "show_legend": "1",
            "show_3d": "0",
            "percent_left": "0.0000",
            "percent_right": "0.0000",
            "ymin_type": "1",
            "ymax_type": "0",
            "ymin_itemid": "0",
            "ymax_itemid": "0",
            "flags": "0"
        },
        {
            "graphid": "614",
            "name": "CPU utilization",
            "width": "900",
            "height": "200",
            "yaxismin": "0.0000",
            "yaxismax": "100.0000",
            "templateid": "387",
            "show_work_period": "1",
            "show_triggers": "0",
            "graphtype": "1",
            "show_legend": "1",
            "show_3d": "0",
            "percent_left": "0.0000",
            "percent_right": "0.0000",
            "ymin_type": "1",
            "ymax_type": "1",
            "ymin_itemid": "0",
            "ymax_itemid": "0",
            "flags": "0"
        },
        {
            "graphid": "645",
            "name": "Disk space usage /",
            "width": "600",
            "height": "340",
            "yaxismin": "0.0000",
            "yaxismax": "0.0000",
            "templateid": "0",
            "show_work_period": "0",
            "show_triggers": "0",
            "graphtype": "2",
            "show_legend": "1",
            "show_3d": "1",
            "percent_left": "0.0000",
            "percent_right": "0.0000",
            "ymin_type": "0",
            "ymax_type": "0",
            "ymin_itemid": "0",
            "ymax_itemid": "0",
            "flags": "4"
        }
    ],
    "id": 1
}
```

### See also

### 参考

-   [Discovery
    rule](/manual/api/reference/discoveryrule/object#discovery_rule)
-   [Graph item](/manual/api/reference/graphitem/object#graph_item)
-   [Item](/manual/api/reference/item/object#item)
-   [Host](/manual/api/reference/host/object#host)
-   [Host group](/manual/api/reference/hostgroup/object#host_group)
-   [Template](/manual/api/reference/template/object#template)
-   [发现规则](/zh/manual/api/reference/discoveryrule/object#discovery_rule)
-   [图表监控项](/zh/manual/api/reference/graphitem/object#graph_item)
-   [监控项](/zh/manual/api/reference/item/object#item)
-   [主机](/zh/manual/api/reference/host/object#host)
-   [主机组](/zh/manual/api/reference/hostgroup/object#host_group)
-   [模板](/zh/manual/api/reference/template/object#template)

### Source

### 来源

CGraph::get() in
*frontends/php/include/classes/api/services/CGraph.php*.

[comment]: # ({/new-6f7b2d0a})
