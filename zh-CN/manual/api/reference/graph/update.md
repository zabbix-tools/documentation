[comment]: # translation:outdated

[comment]: # ({new-d2516189})
# graph.update

[comment]: # ({/new-d2516189})

[comment]: # ({new-992bc0de})
### Description

[comment]: # ({/new-992bc0de})

[comment]: # ({new-72c99624})
### 描述

`object graph.update(object/array graphs)`
`对象 graph.update(object/array graphs)`

This method allows to update existing graphs.
此方法用于更新已存在的图表。

[comment]: # ({/new-72c99624})

[comment]: # ({new-9a0b4500})
### Parameters

[comment]: # ({/new-9a0b4500})

[comment]: # ({new-b41637d2})
### 参数

`(object/array)` Graph properties to be updated. `(对象/数组)`
要更新的图表属性.

The `graphid` property must be defined for each graph, all other
properties are optional. Only the passed properties will be updated, all
others will remain unchanged. 每一个图表都必须定义`graphid` 属性,
其它属性均为可选项. 只有被传递的属性会被更新, 其他属性将保持不变.

Additionally to the [standard graph properties](object#graph) the method
accepts the following parameters. 除了 [标准图表属性](object#graph)
之外，此方法还接受以下参数.

|Parameter|Type|Description|
|---------|----|-----------|
|gitems|array|Graph items to replace existing graph items. If a graph item has the `gitemid` property defined it will be updated, otherwise a new graph item will be created.|

|参数     类|描述|<|
|--------------|------|-|
|gitems|array|替换已存在图表监控项的图表监控项. 如果一个图表监控项的 `gitemid` 属性已经被定义，那么它将会被更新, 否则将会创建一个新的图表监控项.|

[comment]: # ({/new-b41637d2})

[comment]: # ({new-7b33f5ce})
### Return values

[comment]: # ({/new-7b33f5ce})

[comment]: # ({new-84b1446b})
### 返回值

`(object)` Returns an object containing the IDs of the updated graphs
under the `graphids` property. `(对象)` 在 `graphids`
属性下，返回一个包含已更新图表的ID的对象。

### Examples

### 例子

#### Setting the maximum for the Y scale

#### 设置Y刻度的最大值

Set the the maximum of the Y scale to a fixed value of 100.
设置Y刻度的最大值为固定值100.

Request: 请求:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "graph.update",
    "params": {
        "graphid": "439",
        "ymax_type": 1,
        "yaxismax": 100
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response: 响应:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "graphids": [
            "439"
        ]
    },
    "id": 1
}
```

### Source

### 来源

CGraph::update() in
*frontends/php/include/classes/api/services/CGraph.php*.

[comment]: # ({/new-84b1446b})
