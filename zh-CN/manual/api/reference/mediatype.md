[comment]: # translation:outdated

[comment]: # ({new-6bf4ac44})
# 媒介类型

This class is designed to work with media types.
这个类设计用来处理媒介类型。

Object references:\

-   [Media type](/manual/api/reference/mediatype/object#media_type)

Available methods:\

-   [mediatype.create](/manual/api/reference/mediatype/create) -
    creating new media types
-   [mediatype.delete](/manual/api/reference/mediatype/delete) -
    deleting media types
-   [mediatype.get](/manual/api/reference/mediatype/get) - retrieving
    media types
-   [mediatype.update](/manual/api/reference/mediatype/update) -
    updating media types

[comment]: # ({/new-6bf4ac44})
