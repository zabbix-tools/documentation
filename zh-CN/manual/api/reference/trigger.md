[comment]: # translation:outdated

[comment]: # ({new-9dae9ddc})
# 触发器

This class is designed to work with triggers. 此类用于管理触发器。

Object references: 对象引用：\

-   [Trigger](/zh/manual/api/reference/trigger/object#trigger)

Available methods: 可用方法：\

-   [trigger.adddependencies](/zh/manual/api/reference/trigger/adddependencies) -
    adding new trigger dependencies 添加新的触发器依赖
-   [trigger.create](/zh/manual/api/reference/trigger/create) - creating
    new triggers 创建新的触发器
-   [trigger.delete](/zh/manual/api/reference/trigger/delete) - deleting
    triggers 删除触发器
-   [trigger.deletedependencies](/zh/manual/api/reference/trigger/deletedependencies) -
    deleting trigger dependencies 删除触发器依赖
-   [trigger.get](/zh/manual/api/reference/trigger/get) - retrieving
    triggers 检索触发器
-   [trigger.update](/zh/manual/api/reference/trigger/update) - updating
    triggers 更新触发器

[comment]: # ({/new-9dae9ddc})
