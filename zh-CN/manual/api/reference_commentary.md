[comment]: # translation:outdated

[comment]: # ({new-6d056871})
# Appendix 1. Reference commentary

[comment]: # ({/new-6d056871})

[comment]: # ({new-6594bdc4})
### Notation

[comment]: # ({/new-6594bdc4})

[comment]: # ({new-2df08058})
#### Data types

The Zabbix API supports the following data types:

|Type|Description|
|----|-----------|
|boolean|A boolean value, accepts either `true` or `false`.|
|flag|The value is considered to be `true` if it is passed and not equal to `null` and `false` otherwise.|
|integer|A whole number.|
|float|A floating point number.|
|string|A text string.|
|text|A longer text string.|
|timestamp|A Unix timestamp.|
|array|An ordered sequence of values, that is, a plain array.|
|object|An associative array.|
|query|A value which defines, what data should be returned.<br><br>Can be defined as an array of property names to return only specific properties, or as one of the predefined values:<br>`extend` - returns all object properties;<br>`count` - returns the number of retrieved records, supported only by certain subselects.|

[comment]: # ({/new-2df08058})

[comment]: # ({new-a2c16a18})
#### Property labels

Some of the objects properties are marked with short labels to describe
their behavior. The following labels are used:

-   *readonly* - the value of the property is set automatically and
    cannot be defined or changed by the client;
-   *constant* - the value of the property can be set when creating an
    object, but cannot be changed after.

[comment]: # ({/new-a2c16a18})

[comment]: # ({new-f255da9d})
#### Parameter behavior

Some of the operation parameters are marked with short labels to describe their behavior for the operation.
The following labels are used:

-   *read-only* - the value of the parameter is set automatically and cannot be defined or changed by the user, even in some specific conditions
    (e.g., *read-only* for inherited objects or discovered objects);
-   *write-only* - the value of the parameter can be set, but cannot be accessed after;
-   *supported* - the value of the parameter is not required to be set, but is allowed to be set in some specific conditions
    (e.g., *supported* if `status` of Proxy object is set to "passive proxy");
-   *required* - the value of the parameter is required to be set.

Parameters that are not marked with labels are optional.

[comment]: # ({/new-f255da9d})

[comment]: # ({new-5e99748d})
### Reserved ID value "0"

Reserved ID value "0" can be used to filter elements and to remove
referenced objects. For example, to remove a referenced proxy from a
host, proxy\_hostid should be set to 0 ("proxy\_hostid": "0") or to
filter hosts monitored by server option proxyids should be set to 0
("proxyids": "0").

[comment]: # ({/new-5e99748d})

[comment]: # ({new-b4e0f5ab})
### Common "get" method parameters

The following parameters are supported by all `get` methods:

|Parameter|Type|Description|
|---------|----|-----------|
|countOutput|boolean|Return the number of records in the result instead of the actual data.|
|editable|boolean|If set to `true` return only objects that the user has write permissions to.<br><br>Default: `false`.|
|excludeSearch|boolean|Return results that do not match the criteria given in the `search` parameter.|
|filter|object|Return only those results that exactly match the given filter.<br><br>Accepts an array, where the keys are property names, and the values are either a single value or an array of values to match against.<br><br>Doesn't work for `text` fields.|
|limit|integer|Limit the number of records returned.|
|output|query|Object properties to be returned.<br><br>Default: `extend`.|
|preservekeys|boolean|Use IDs as keys in the resulting array.|
|search|object|Return results that match the given wildcard search (case-insensitive).<br><br>Accepts an array, where the keys are property names, and the values are strings to search for. If no additional options are given, this will perform a `LIKE "%…%"` search.<br><br>Works only for `string` and `text` fields.|
|searchByAny|boolean|If set to `true` return results that match any of the criteria given in the `filter` or `search` parameter instead of all of them.<br><br>Default: `false`.|
|searchWildcardsEnabled|boolean|If set to `true` enables the use of "\*" as a wildcard character in the `search` parameter.<br><br>Default: `false`.|
|sortfield|string/array|Sort the result by the given properties. Refer to a specific API get method description for a list of properties that can be used for sorting. Macros are not expanded before sorting.|
|sortorder|string/array|Order of sorting. If an array is passed, each value will be matched to the corresponding property given in the `sortfield` parameter.<br><br>Possible values are:<br>`ASC` - ascending;<br>`DESC` - descending.|
|startSearch|boolean|The `search` parameter will compare the beginning of fields, that is, perform a `LIKE "…%"` search instead.<br><br>Ignored if `searchWildcardsEnabled` is set to `true`.|

[comment]: # ({/new-b4e0f5ab})




[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})


[comment]: # ({new-7a121fac})
#### User permission check

Does the user have permission to write to hosts whose names begin with
"MySQL" or "Linux" ?

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.get",
    "params": {
        "countOutput": true,
        "search": {
            "host": ["MySQL", "Linux"]
        },
        "editable": true,
        "startSearch": true,
        "searchByAny": true
    },
    "auth": "766b71ee543230a1182ca5c44d353e36",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": "0",
    "id": 1
}
```

::: noteclassic
Zero result means no hosts with read/write
permissions.
:::

[comment]: # ({/new-7a121fac})

[comment]: # ({new-ea3cccd8})
#### Mismatch counting

Count the number of hosts whose names do not contain the substring
"ubuntu"

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.get",
    "params": {
        "countOutput": true,
        "search": {
            "host": "ubuntu"
        },
        "excludeSearch": true
    },
    "auth": "766b71ee543230a1182ca5c44d353e36",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": "44",
    "id": 1
}
```

[comment]: # ({/new-ea3cccd8})

[comment]: # ({new-e911c08b})
#### Searching for hosts using wildcards

Find hosts whose name contains word "server" and have interface ports
"10050" or "10071". Sort the result by host name in descending order and
limit it to 5 hosts.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.get",
    "params": {
        "output": ["hostid", "host"],
        "selectInterfaces": ["port"],
        "filter": {
            "port": ["10050", "10071"]
        },
        "search": {
            "host": "*server*"
        },
        "searchWildcardsEnabled": true,
        "searchByAny": true,
        "sortfield": "host",
        "sortorder": "DESC",
        "limit": 5
    },
    "auth": "766b71ee543230a1182ca5c44d353e36",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "hostid": "50003",
            "host": "WebServer-Tomcat02",
            "interfaces": [
                {
                    "port": "10071"
                }
            ]
        },
        {
            "hostid": "50005",
            "host": "WebServer-Tomcat01",
            "interfaces": [
                {
                    "port": "10071"
                }
            ]
        },
        {
            "hostid": "50004",
            "host": "WebServer-Nginx",
            "interfaces": [
                {
                    "port": "10071"
                }
            ]
        },
        {
            "hostid": "99032",
            "host": "MySQL server 01",
            "interfaces": [
                {
                    "port": "10050"
                }
            ]
        },
        {
            "hostid": "99061",
            "host": "Linux server 01",
            "interfaces": [
                {
                    "port": "10050"
                }
            ]
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-e911c08b})

[comment]: # ({new-8ce6f554})
#### Searching for hosts using wildcards with "preservekeys"

If you add the parameter "preservekeys" to the previous request, the
result is returned as an associative array, where the keys are the id of
the objects.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.get",
    "params": {
        "output": ["hostid", "host"],
        "selectInterfaces": ["port"],
        "filter": {
            "port": ["10050", "10071"]
        },
        "search": {
            "host": "*server*"
        },
        "searchWildcardsEnabled": true,
        "searchByAny": true,
        "sortfield": "host",
        "sortorder": "DESC",
        "limit": 5,
        "preservekeys": true
    },
    "auth": "766b71ee543230a1182ca5c44d353e36",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "50003": {
            "hostid": "50003",
            "host": "WebServer-Tomcat02",
            "interfaces": [
                {
                    "port": "10071"
                }
            ]
        },
        "50005": {
            "hostid": "50005",
            "host": "WebServer-Tomcat01",
            "interfaces": [
                {
                    "port": "10071"
                }
            ]
        },        
        "50004": {
            "hostid": "50004",
            "host": "WebServer-Nginx",
            "interfaces": [
                {
                    "port": "10071"
                }
            ]
        },
        "99032": {
            "hostid": "99032",
            "host": "MySQL server 01",
            "interfaces": [
                {
                    "port": "10050"
                }
            ]
        },
        "99061": {
            "hostid": "99061",
            "host": "Linux server 01",
            "interfaces": [
                {
                    "port": "10050"
                }
            ]
        }
    },
    "id": 1
}
```

[comment]: # ({/new-8ce6f554})
