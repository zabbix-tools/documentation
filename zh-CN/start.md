[comment]: # translation:outdated

[comment]: # ({new-63820343})
# Zabbix documentation in Chinese

These pages contain official Zabbix documentation.

Use the sidebar navigation to browse documentation pages.

To be able to watch pages, log in with your [Zabbix
forums](http://www.zabbix.com/forum/) username and password.

[comment]: # ({/new-63820343})

