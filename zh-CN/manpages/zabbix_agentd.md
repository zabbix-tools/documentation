[comment]: # translation:outdated

[comment]: # ({new-2c290a27})
# zabbix\_agentd

Section: Maintenance Commands (8)\
Updated: 2016-01-13\
[Index](#index) [Return to Main Contents](/documentation/3.0/manpages)

------------------------------------------------------------------------

[ ]{#lbAB}

[comment]: # ({/new-2c290a27})

[comment]: # ({new-75de926e})
## NAME

zabbix\_agentd - Zabbix agent daemon [ ]{#lbAC}

[comment]: # ({/new-75de926e})

[comment]: # ({new-b8c27067})
## SYNOPSIS

**zabbix\_agentd** \[**-c** *config-file*\]\
**zabbix\_agentd** \[**-c** *config-file*\] **-p**\
**zabbix\_agentd** \[**-c** *config-file*\] **-t** *item-key*\
**zabbix\_agentd** \[**-c** *config-file*\] **-R** *runtime-option*\
**zabbix\_agentd -h**\
**zabbix\_agentd -V** [ ]{#lbAD}

[comment]: # ({/new-b8c27067})

[comment]: # ({new-ecdda52c})
## DESCRIPTION

**zabbix\_agentd** is a daemon for monitoring of various server
parameters. [ ]{#lbAE}

[comment]: # ({/new-ecdda52c})

[comment]: # ({new-63567f56})
## OPTIONS

**-c**, **--config** *config-file*  
Use the alternate *config-file* instead of the default one. Path to the
file should be specified.

**-f**, **--foreground**  
Run Zabbix agent in foreground.

**-R**, **--runtime-control** *runtime-option*  
Perform administrative functions according to *runtime-option*.

[ ]{#lbAF}

[comment]: # ({/new-63567f56})

[comment]: # ({new-5333e0f4})
### 

  
Runtime control options

  
**log\_level\_increase**\[=*target*\]  
Increase log level, affects all processes if target is not specified

  
**log\_level\_decrease**\[=*target*\]  
Decrease log level, affects all processes if target is not specified

[ ]{#lbAG}

[comment]: # ({/new-5333e0f4})

[comment]: # ({new-199def5e})
### 

  
Log level control targets

  
*pid*  
Process identifier

  
*process-type*  
All processes of specified type (e.g., listener)

  
*process-type,N*  
Process type and number (e.g., listener,3)

```{=html}
<!-- -->
```
**-p**, **--print**  
Print known items and exit. For each item either generic defaults are
used, or specific defaults for testing are supplied. These defaults are
listed in square brackets as item key parameters. Returned values are
enclosed in square brackets and prefixed with the type of the returned
value, separated by a pipe character. For user parameters type is always
**t**, as the agent can not determine all possible return values. Items,
displayed as working, are not guaranteed to work from the Zabbix server
or zabbix\_get when querying a running agent daemon as permissions or
environment may be different. Returned value types are:

  
d  
Number with a decimal part.

  
m  
Not supported. This could be caused by querying an item that only works
in the active mode like a log monitoring item or an item that requires
multiple collected values. Permission issues or incorrect user
parameters could also result in the not supported state.

  
s  
Text. Maximum length not limited.

  
t  
Text. Same as **s**.

  
u  
Unsigned integer.

**-t**, **--test** *item-key*  
Test single item and exit. See **--print** for output description.

**-h**, **--help**  
Display this help and exit.

**-V**, **--version**  
Output version information and exit.

[ ]{#lbAH}

[comment]: # ({/new-199def5e})

[comment]: # ({new-fcfd4796})
## FILES

*/usr/local/etc/zabbix\_agentd.conf*  
Default location of Zabbix agent configuration file (if not modified
during compile time).

[ ]{#lbAI}

[comment]: # ({/new-fcfd4796})

[comment]: # ({new-539868a7})
## SEE ALSO

**[zabbix\_get](zabbix_get)**(8), **[zabbix\_proxy](zabbix_proxy)**(8),
**[zabbix\_sender](zabbix_sender)**(8),
**[zabbix\_server](zabbix_server)**(8) [ ]{#lbAJ}

[comment]: # ({/new-539868a7})

[comment]: # ({new-e55ed07e})
## AUTHOR

Alexei Vladishev <<alex@zabbix.com>>

------------------------------------------------------------------------

[ ]{#index}

[comment]: # ({/new-e55ed07e})

[comment]: # ({new-6ea4e50b})
## Index

[NAME](#lbAB)

[SYNOPSIS](#lbAC)

[DESCRIPTION](#lbAD)

[OPTIONS](#lbAE)

[](#lbAF)

[](#lbAG)

  

[FILES](#lbAH)

[SEE ALSO](#lbAI)

[AUTHOR](#lbAJ)

------------------------------------------------------------------------

This document was created by man2html, using the manual pages.\
Time: 08:31:40 GMT, January 19, 2016

[comment]: # ({/new-6ea4e50b})
