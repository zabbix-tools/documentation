# Zabbix Kullanım Kılavuzu

Zabbix 3.4 yazılımının kullanım kılavuzuna hoş geldiniz. Bu sayfalar,
kullanıcıların en basitten en karmaşığa kadar tüm izleme görevlerini
Zabbix ile başarıyla yönetmelerine yardımcı olmak için oluşturulmuştur.
