# -\#4 Zabbix'e genel bakış

#### Mimari

Zabbix, sorumlulukları aşağıda özetlenen birkaç önemli yazılım
bileşeninden oluşur.

##### Sunucu

[Zabbix sunucusu](/manual/concepts/server), aracıların
kullanılabilirlik, bütünlük bilgileri ve istatistikleri rapor ettiği
merkezi bileşendir. Sunucu, tüm yapılandırma, istatistik ve operasyonel
verilerinin saklandığı merkezi depodur.

##### Veritabanı deposu

Zabbix tarafından toplanan verilerin yanı sıra tüm yapılandırma
bilgileri bir veritabanında saklanır.

##### Web arabirimi

Zabbix'e her yerden ve herhangi bir platformdan kolayca erişmek için web
tabanlı arayüz sunulmuştur. Arabirim, Zabbix sunucusunun bir parçasıdır
ve genellikle (ancak zorunlu olmayarak) sunucuyu çalıştıran makineyle
aynı fiziksel makinede çalışır.

##### Proxy

[Zabbix proxy](/manual/concepts/proxy), Zabbix sunucusu adına performans
ve kullanılabilirlik verileri toplayabilir. Proxy, Zabbix dağıtımının
isteğe bağlı bir parçasıdır; bununla birlikte, tek bir Zabbix
sunucusunun yükünü dağıtmak çok yararlı olabilir.

##### Aracı

[Zabbix aracıları](/manual/concepts/agent), yerel kaynakları ve
uygulamaları aktif olarak izlemek ve toplanan verileri Zabbix sunucusuna
raporlamak için izleme hedeflerine dağıtılır.

#### Veri akışı

Ek olarak, bir adım geri gitmeniz ve Zabbix içindeki genel veri akışına
bir göz atmanız önemlidir. Veri toplayan bir öğe yaratmak için önce bir
ana makine oluşturmalısınız. Zabbix spektrumunun diğer ucuna geçmek için
önce tetikleyici oluşturacak bir öğeye sahip olmalısınız. Bir eylem
yaratmak için bir tetikleyicinin olması gerekir. Bu nedenle, *X
sunucusundaki* CPU yükünüz çok yüksekken uyarı almak istiyorsanız,
öncelikle *Sunucu X* için bir ana bilgisayar girişi, ardından CPU'sunu
izlemek için bir öğe oluşturmanız gerekir; ardından CPU yükü çok
yüksekse etkinleştirilen bir tetikleyicinin oluşturulması takip eder,
bunu da size bir e-posta gönderen eylem izler. Bu, bir sürü adım gibi
gözükse de, şablon kullanımıyla aslında öyle değildir. Bu tasarım
sayesinde çok esnek bir kurulum oluşturmak mümkündür.
