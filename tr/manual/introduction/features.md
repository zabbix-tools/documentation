# - \#3 Zabbix özellikleri

#### Genel bakış

Zabbix, tek bir pakette çok çeşitli özellikler sunan epey entegre bir ağ
izleme çözümüdür.

**[Veri toplama](/manual/config/items)**

-   Kullanılabilirlik ve performans denetimleri
-   SNMP desteği (Hem sorgulama hem de yakalama), IPMI, JMX, VMware
    izleme
-   Özel kontroller
-   İstenen verileri belirli aralıklarla toplama
-   Sunucu/proxy ve aracılar (agents) tarafından gerçekleştirilir

**[Esnek eşik tanımları](/manual/config/triggers)**

-   Arka uç veritabanındaki değerleri referans alarak, tetikleyiciler
    olarak adlandırılan çok esnek eşik değerleri tanımlayabilirsiniz

**[Hayli yapılandırılabilir uyarılar](/manual/config/notifications)**

-   Bildirim gönderimleri, eskalasyon (yükseltme) planı, alıcı, ortam
    türü için özelleştirilebilir
-   Makro değişkenleri kullanılarak bildirimler anlamlı ve yararlı hale
    getirilebilir
-   Uzak komutları da içeren otomatik eylemler

**[Gerçek zamanlı
çizelgeler](/manual/config/visualisation/graphs/simple)**

-   İzlenen öğeler, yerleşik çizelge işlevselliği kullanılarak anında
    çizelge haline getirilir

**[Web izleme yetenekleri](/manual/web_monitoring)**

-   Zabbix, bir web sitesinde simüle edilmiş fare tıklamalarının yolunu
    izleyebilir, işlevsellik ve yanıt süresini kontrol edebilir

**[Geniş görselleştirme seçenekleri](/manual/config/visualisation)**

-   Birden çok öğeyi tek bir görünümde birleştirebilen özel çizelgeler
    oluşturma becerisi
-   Ağ haritaları
-   Gösterge paneli stilinde genel bakış için özel ekranlar ve slayt
    gösterileri
-   Raporlar
-   İzlenen kaynakların üst düzey (iş) görüntüsü

**[Geçmiş veri
saklama](/manual/installation/requirements#database_size)**

-   Veriler bir veritabanında saklanır
-   Yapılandırılabilir geçmiş
-   Dahili temizlik prosedürü

**[Kolay yapılandırma](/manual/config/hosts)**

-   İzlenen cihazları ana bilgisayar olarak ekleme
-   Ana bilgisayarlar bir kez veritabanında olmasıyla izleme için alınır
-   Şablonları izlenen cihazlara uygulama

**[Şablon kullanımı](/manual/config/templates)**

-   Şablonlarda gruplama denetimleri
-   Diğer şablonlardan kalıt alabilir şablonlar

**[Ağ keşfi](/manual/discovery)**

-   Ağ aygıtlarının otomatik keşfi
-   Aracı otomatik kayıt
-   Dosya sistemlerinin, ağ arayüzlerinin ve SNMP OID'lerinin keşfi

**[Hızlı web arayüzü](/manual/web_interface)**

-   PHP'de yazılmış web tabanlı bir önyüz
-   Her yerden erişilebilir
-   İşlem yolunuz boyunca tıklanabilir
-   Denetim günlüğü

**[Zabbix API'si](/manual/api)**

-   Zabbix API, toplu manipülasyonlar, üçüncü parti yazılım entegrasyonu
    ve diğer amaçlar için Zabbix'e programlanabilir arabirim sağlar.

**[İzin sistemleri](/manual/config/users_and_usergroups)**

-   Güvenli kullanıcı kimlik doğrulaması
-   Bazı kullanıcılar belirli görüntülemelerle sınırlandırılabilir

**[Tam özellikli ve kolayca genişletilebilir
aracı](/manual/concepts/agent)**

-   İzleme hedeflerine yayılmış
-   Hem Linux hem de Windows üzerinde yayılabilir

**[İkili artalan süreçleri](/manual/concepts/server)**

-   Performans ve küçük bellek alanı için C yazılmış
-   Kolayca taşınabilir

**[Karmaşık ortamlara hazır](/manual/distributed_monitoring)**

-   Uzaktan izleme, bir Zabbix proxy kullanılarak kolaylaştırılır
