# 1 Kullanım kılavuzu yapısı

#### Yapı

Bu Zabbix 3.4 kullanım kılavuzunun içeriği, belirli ilgi alanlarına
kolay erişim sağlamak için bölümlere ve alt bölümlere ayrılmıştır.

İlgili bölümlere gittiğinizde, alt bölümlere ve tek tek sayfalara dahil
olan içeriğin tamamını ortaya çıkarmak için bölüm klasörlerini
genişlettiğinizden emin olun.

Konuyla ilişkili bilgilerin kullanıcıların gözünden kaçmaması için
mümkün olduğunca alakalı içerik sayfalarına bağlantı verilmektedir.

#### Bölümler

[Giriş](about), mevcut Zabbix yazılımı hakkında genel bilgi sağlar. Bu
bölümün okunması sizi Zabbix'i seçmek için bazı iyi nedenler hakkında
bilgilendirir.

[Zabbix kavramları](/manual/concepts), Zabbix'te kullanılan
terminolojiyi açıklar ve Zabbix bileşenleri hakkında ayrıntılar sunar.

[Kurulum](/manual/installation/getting_zabbix) ve [Hızlı
Başlangıç](/manual/quickstart/item) bölümleri, Zabbix'i kullanmaya
başlamanıza yardımcı olacaktır. [Zabbix cihazı](/manual/appliance),
Zabbix'i kullanmanın nasıl bir tat verdiğini görmek için hızlı bir
alternatiftir.

[Yapılandırma](/manual/config), bu kılavuzdaki en büyük ve çok önemli
bölümlerden biridir. Zabbix'i ortamınızı izlemek için nasıl
kuracağınıza, ana bilgisayarları ayarlamaya, ana verileri elde etmeye,
veri görüntülemeye ve sorun durumunda çalıştırılacak uzaktan komutların
yapılandırılmasına kadar pek çok temel öneri içerir.

[BT hizmetleri](/manual/it_services) bölümünde, izleme ortamınıza
ilişkin üst düzey bir genel bakış için Zabbix'in nasıl kullanılacağı
ayrıntılı olarak açıklanmaktadır.

[Web izlemesi](/manual/web_monitoring), web sitelerinin
kullanılabilirliğini nasıl izleyeceğinizi öğrenmenize yardımcı olur.

[Sanal makine izlemesi](/manual/vm_monitoring), VMware ortamının
izlemesini yapılandırmak için neler yapılacağını gösterir.

[Maintenance](/manual/maintenance), [Regular
expressions](/manual/regular_expressions), [Event
acknowledgement](/manual/acknowledges) and [XML
export/import](/manual/xml_export_import) are further sections that
reveal how to use these various aspects of Zabbix software.
[Bakım](/manual/maintenance), [Düzenli
ifadeler](/manual/regular_expressions), [Olay
bildirimi](/manual/acknowledges) ve [XML dışarı aktarma/içeri
aktarma](/manual/xml_export_import), Zabbix yazılımının bu yönlerinin
nasıl kullanılacağını ortaya koyan diğer bölümlerdir.

[Keşif](/manual/discovery), ağ aygıtlarının, etkin aracıların, dosya
sistemlerinin, ağ arabirimlerinin vb. otomatik keşfini kurmak için
yönergeler içerir.

[Dağıtılmış izleme](/manual/distributed_monitoring), Zabbix'in daha
büyük ve karmaşık ortamlarda kullanılması olanaklarıyla ilgilenmektedir.

[Şifreleme](/manual/encryption), Zabbix bileşenleri arasındaki iletişimi
şifreleme olanaklarını açıklar.

[Web arayüzü](/manual/web_interface), Zabbix'in web arayüzünü kullanmak
için özel bilgiler içerir.

[API](/manual/api) bölümü, Zabbix API'siyle (Application Programming
Interface, Uygulama Programlama Arayüzü) çalışma hakkında ayrıntılar
sunar.

Teknik bilgilerin ayrıntılı listeleri [Ekler](/manual/appendix)
bölümünde bulunmaktadır. Burada ayrıca bir SSS (Sıkça Sorulan Sorular)
bölümü de bulacaksınız.
