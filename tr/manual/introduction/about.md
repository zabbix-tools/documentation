# - \#2 Zabbix Nedir?

#### Genel bakış

Zabbix, Alexei Vladishev tarafından oluşturuldu ve şu anda Zabbix SIA
tarafından aktif olarak geliştiriliyor ve destekleniyor.

Zabbix, hızlı ve güvenilir bir açık kaynaklı dağıtılmış izleme
çözümüdür.

Zabbix, bir ağın sayısız parametrelerini ve sunucuların sağlık ve
bütünlüğünü izleyen bir yazılımdır. Zabbix, kullanıcıların neredeyse her
olay için e-posta tabanlı uyarılar yapılandırabildiği esnek bir bildirim
mekanizması kullanır. Bu, sunucu sorunlarına hızlı tepki verme olanağı
verir. Zabbix, depolanan verilere dayalı mükemmel raporlama ve veri
görselleştirme özellikleri sunar. Bu, Zabbix'i kapasite planlaması için
ideal kılar.

Zabbix hem sorgulama hem de yakalama (izleme) işlemlerini
desteklemektedir. Tüm Zabbix raporları ve istatistikleriyle yapılandırma
parametrelerine web tabanlı bir önyüz üzerinden erişilir. Web tabanlı
bir önyüz, ağınızın durumunu ve sunucularınızın sağlığını herhangi bir
yerden değerlendirebilmenizi sağlar. Doğru yapılandırılmış Zabbix, BT
altyapısının izlenmesinde önemli bir rol oynayabilir. Bu, birkaç
sunuculu küçük kuruluşlar için de, çok sayıda sunucusu olan büyük
şirketler için de geçerlidir.

Zabbix ücretsizdir. Zabbix, GPL (GNU General Public License) Genel Kamu
Lisansı'nın 2. sürümü altında yazılmış ve dağıtılmıştır. Bu, kaynak
kodunun serbestçe dağıtıldığı ve halka açık olduğu anlamına gelmektedir.

Zabbix Şirketi tarafından [ticari
destek](http://www.zabbix.com/support.php) sağlanmaktadır.

[Zabbix özellikleri](features) hakkında daha çok bilgi edinin.

#### Zabbix kullanıcıları

Farklı büyüklükteki birçok organizasyon, birincil izleme platformu
olarak Zabbix'e güveniyor.
