FIXME **This page is not fully translated, yet. Please help completing
the translation.**\
*(remove this paragraph once the translation is finished)*

# 6. Zabbix aygıtı

#### Genel bakış

Elle kurmaya veya mevcut bir sunucuyu Zabbix için yeniden ayarlamaya
alternatif olarak kullanıcılar, bir Zabbix cihazı veya Zabbix cihazı
kurulum imajı da
[indirebilir](http://www.zabbix.com/download.php#appliance). Zabbix
sunucusu (MySQL), Zabbix sunucusu (PostgreSQL), Zabbix vekil sunucusu
(MySQL) ve Zabbix vekil sunucusunun (SQLite 3) anında kurulumu için
Zabbix cihaz kurulum CD'si kullanılabilir.

Zabbix Cihazır sanal makineleri, MySQL destekli Zabbix sunucusu
hazırlamışlardır. Zabbix cihazı kurulum CD'si kullanılarak
oluşturulmuştur.

|<|
|<|
|-|

|<|
|<|
|-|

Zabbix cihaz ve kurulum CD'si sürümleri aşağıdaki Ubuntu sürümlerine
dayanmaktadır:

|Zabbix cihaz sürümü|Ubuntu sürümü|
|----------------------|----------------|
|3.0.0|14.04.3|

Zabbix cihazı aşağıdaki biçimlerde mevcuttur:

-   vmdk (VMware/Virtualbox)
-   OVF (Open Virtualisation Format, Açık Sanallaştırma Biçimi)
-   KVM
-   HDD/flash imajı, USB bellek
-   Cannlı CD/DVD
-   Xen guest
-   Microsoft VHD (Azure)
-   Microsoft VHD (Hyper-V)

Başlamak için cihazı açın ve tarayıcınızı DHCP üzerinden aldığı IP'ye
yönlendirin: http://<host\_ip>/zabbix

Zabbix sunucusu yapılandırılmış ve MySQL ile çalışıyordur, ayrıca önyüz
de mevcut durumdadır.

Cihaz, Preseed dosyaları denilen standart Ubuntu/Debian özelliğini
kullanarak inşa edilmiştir.

#### - Ubuntu yapılandırmasında yapılan değişiklikler

Temel Ubuntu yapılandırmasında bazı değişiklikler uygulanmaktadır.

##### - Depolar

Resmi Zabbix
[deposu](/manual/installation/install_from_packages#debianubuntu)
*/etc/apt/sources.list* dosyasına eklenmiştir:

    ## Zabbix repository
    deb http://repo.zabbix.com/zabbix/3.0/ubuntu trusty main
    deb-src http://repo.zabbix.com/zabbix/3.0/ubuntu trusty main

##### - Güvenlik duvarı

Cihaz, önceden tanımlanmış kurallara sahip iptables güvenlik duvarı
kullanır:

-   Açık SSH portu (22 TCP);
-   Açık Zabbix aracısı (10050 TCP) ve Zabbix trapper (10051 TCP)
    portları;
-   Açık HTTP (80 TCP) ve HTTPS (443 TCP) portları;
-   Açık SNMP trap portu (162 UDP);
-   Açık NTP portuna giden bağlantılar (53 UDP);
-   Saniyede 5 paketle sınırlı ICMP paketleri;
-   Diğer tüm gelen bağlantılar kapatıldı.

##### - Ek paketler

Zabbix ile çalışmayı ve genel olarak daha kolay izlemeyi sağlayabilecek
çeşitli temel araçlar eklendi:

-   iptables-persistent
-   mc
-   htop
-   snmptrapfmt
-   snmp-mibs-downloader

Bu paketlerin bazıları Zabbix tarafından kullanılmaktadır, bazıları da
kullanıcıların cihaz ayarlarını yapılandırmasına ve yönetmesine yardımcı
olmak için kurulmuştur.

##### - Statik bir IP adresleri kullanma

Öntanımlı olarak, cihaz IP adresini elde etmek için DHCP kullanır.
Statik bir IP adresi belirtmek için:

-   root kullanıcısı olarak oturum açın;
-   Favori metin düzenleyicinizde */etc/network/interfaces* dosyasını
    açın;
-   *iface eth0 inet dhcp* → *iface eth0 inet static*
-   *iface eth0 inet static* ifadesinden sonra aşağıdakileri ekleyin:
    -   *address <cihazın IP adresi>*
    -   *netmask <ağ maskesi>*
    -   *gateway <ağ geçidi adresiniz>*
-   Şu komutları çalıştırın: **sudo ifdown eth0 && sudo ifup eth0**.

::: noteclassic
Diğer olası seçenekler hakkında daha çok bilgi için resmi
Ubuntu
[belgelerine](https://help.ubuntu.com/lts/serverguide/network-configuration.html)
bakın.
:::

DNS'yi yapılandırmak için */etc/resolv.conf* dosyasında nameserver
girdilerini ekleyin, her ad sunucusunu ayrı satırda belirtin:
**nameserver 192.168.1.2**.

##### - Saat dilimini değiştirme

Öntanımlı olarak cihaz, sistem saati için UTC kullanır. Saat dilimini
değiştirmek için uygun dosyayı */usr/share/zoneinfo* dizininden
*/etc/localtime* dizinine kopyalayın, örneğin:

    cp /usr/share/zoneinfo/Europe/Riga /etc/localtime

##### - Yerel değişiklikleri

Cihaz birkaç yerel ayar değişikliği içerir:

-   İçerilen diller: *en\_US.UTF-8*, *ru\_RU.UTF-8*, *ja\_JP.UTF-8*,
    *cs\_CZ.UTF-8*, *ko\_KR.UTF-8*, *it\_IT.UTF-8*,\
    *pt\_BR.UTF-8*, *sk\_SK.UTF-8*, *uk\_UA.UTF-8*, *fr\_FR.UTF-8*,
    *pl.UTF-8*;
-   Öntanımlı yerel: *en\_US.UTF-8*.

Bu değişiklikler çok dilli bir Zabbix web arayüzünü desteklemek için
gereklidir.

##### - Diğer değişiklikler

-   Ağ, IP adresini elde etmek için DHCP kullanacak şekilde
    yapılandırılmıştır;
-   **fping** aracının izinleri 4710 olarak belirlenmiştir ve
    **zabbix** - suid grubuna aittir ve yalnızca zabbix grubu tarafından
    kullanılmasına izin verilmiştir;
-   ntpd, genel pool sunucularıyla senkronizasyon yapmak üzere
    yapılandırılmıştır: *ntp.ubuntu.com*;
-   LVM disk birimi ext4 dosya sistemiyle birlikte kullanılır.
-   "*UseDNS no*" ifadesi */etc/ssh/sshd\_config* SSH sunucusu
    yapılandırma dosyasına eklenerek uzun SSH bağlantı beklemeleri
    engellenir;
-   snmpd artalan süreci */etc/default/snmpd* yapılandırma dosyası
    kullanılarak devre dışı bırakıldı.

#### - Zabbix yapılandırması

Appliance Zabbix setup has the following passwords and other
configuration changes: Cihaz Zabbix kurulumunda aşağıdaki parolalar ve
diğer yapılandırma değişiklikleri vardır:

##### - Kimlik bilgileri (kullanıcı\_girişi:parola)

Sistem:

-   appliance:zabbix

Veritabanı:

-   root:<rastgele>
-   zabbix:<rastgele>

::: noteclassic
Veritabanı parolaları kurulum işlemi sırasında rastgele
oluşturulur.\
Root parolası /root/.my.cnf dosyasında saklanır; "root" hesabı altında
bir parola girilmesi gerekmez.
:::

Zabbix önyüzü:

-   Admin:zabbix

Veritabanı kullanıcı parolasını değiştirmek için aşağıdaki konumlarda
değiştirilmesi gerekiyor:

-   MySQL;
-   /etc/zabbix/zabbix\_server.conf;
-   /etc/zabbix/web/zabbix.conf.php.

##### - Dosya konumları

-   Yapılandırma dosyaları **/etc/zabbix** içinde yer alır.
-   Zabbix sunucu, vekil sunucu ve aracı kayıt dosyaları
    **/var/log/zabbix** içinde yer alır.
-   Zabbix frontend is placed in **/usr/share/zabbix**.
-   Home directory for user **zabbix** is **/var/lib/zabbix**.

##### - Changes to Zabbix configuration

-   Server name for Zabbix frontend is set to "Zabbix Appliance";
-   Frontend timezone is set to Europe/Riga (this can be modified in
    **/etc/apache2/conf-available/zabbix.conf**);

##### - Preserving configuration

If you are running a Live CD/DVD version of the appliance or for some
other reason cannot have persistent storage, you can create a backup of
the whole database, including all configuration and gathered data.

To create the backup, run:

    sudo mysqldump zabbix | bzip2 -9 > dbdump.bz2

Now you can transfer the **dbdump.bz2** file to another machine.

To restore from the backup, transfer it to the appliance and execute:

    bzcat dbdump.bz2 | sudo mysql zabbix

::: noteimportant
Make sure that Zabbix server is stopped while
performing the restore.
:::

#### - Frontend access

Access to frontend by default is allowed from everywhere.

The frontend can be accessed *http://<host>/zabbix*.

This can be customised in **/etc/apache2/conf-available/zabbix.conf**.
You have to restart the webserver after modifying this file. To do so,
log in using SSH as **root** user and execute:

    service apache2 restart

#### - Firewall

By default, only the ports listed in changes are open. To open
additional ports just modify "*/etc/iptables/rules.v4*" or
"*/etc/iptables/rules.v6*" files and reload firewall rules:

    service iptables-persistent reload

#### - Monitoring capabilities

Zabbix installation is provided with the support for the following:

-   SNMP
-   IPMI
-   Web monitoring
-   VMware monitoring
-   Jabber notifications
-   EZ Texting notifications
-   ODBC
-   SSH2
-   IPv6
-   SNMP Traps
-   Zabbix Java Gateway

#### - SNMP traps

Zabbix appliance uses *snmptrapfmt* to handle SNMP traps. It is
configured to receive all traps from everywhere.

Authentication is not required. If you would like to enable
authentication, you need to change the */etc/snmp/snmptrapd.conf* file
and specify required auth settings.

All traps are stored in the */var/log/zabbix/snmptrapfmt.log* file. It
is rotated by logrotate before reaching 2GB file size.

#### - Upgrading

The appliance Zabbix packages may be upgraded. To do so, run:

    sudo apt-get --only-upgrade install zabbix*

#### - Naming, init and other scripts

Appropriate init scripts are provided. To control Zabbix server, use any
of these:

    service zabbix-server status

Replace **server** with **agent** for Zabbix agent daemon or with
**proxy** for Zabbix proxy daemon.

##### - Increasing available diskspace

::: notewarning
Create a backup of all data before attempting any of
the steps.
:::

Available diskspace on the appliance might not be sufficient. In that
case it is possible to expand the disk. To do so, first expand the block
device in your virtualization environment, then follow these steps.

Start *fdisk* to change the partition size. As *root*, execute:

    fdisk /dev/sda

This will start *fdisk* on disk *sda*. Next, switch to sectors by
issuing:

    u

::: noteimportant
Don't disable DOS compatibility mode by entering
**c**. Proceeding with it disabled will damage the
partition.
:::

Then delete the existing partition and create a new one with the desired
size. In the majority of cases you will accept the available maximum,
which will expand the filesystem to whatever size you made available for
the virtual disk. To do so, enter the following sequence in fdisk
prompt:

    d
    n
    p
    1
    (accept default 63)
    (accept default max)

If you wish to leave some space for additional partitions (swap etc),
you can enter another value for *last sector*. When done, save the
changes by issuing:

    w

After partition creation (new disk or extended existing) create physical
volume:

    pvcreate /dev/sdb1

::: notewarning
Partition name /dev/sdb1 is used in the example; in
your case disk name and partition number could be different. You can
check partition number using *fdisk -l /dev/sdb* command.
:::

Check newly created physical volume:

    pvdisplay /dev/sdb1

Check available physical volumes. There must be 2 volumes zabbix-vg and
newly created:

    pvs

Extend your existing volume group with the newly created physical
volume:

    vgextend zabbix-vg /dev/sdb1

Check "zabbix-vg" volume group:

    vgdisplay

Now extend your logical volume with the free PE space:

    lvextend -l +100%FREE /dev/mapper/zabbix--vg-root

Resize your root volume (can be done on a live sysyem):

    resize2fs /dev/mapper/zabbix--vg-root

Reboot the virtual machine (as the partition we modified is in use
currently). That's it, filesystem should be grown to the partition size
now. Check "/dev/mapper/zabbix--vg-root" volume:

    df -h

#### - Format-specific notes

##### - Xen

**Converting image for XenServer**

To use Xen images with Citrix Xenserver you have to convert the disk
image. To do so:

-   Create a virtual disk, which is at least as large as the image
-   Find out the UUID for this disk

```{=html}
<!-- -->
```
    xe vdi-list params=all

-   If there are lots of disks, they can be filtered by the name
    parameter *name-label*, as assigned when creating the virtual disk
-   Import the image

```{=html}
<!-- -->
```
    xe vdi-import filename="image.raw" uuid="<UUID>"

*Instructions from Brian Radford blog*.

##### - VMware

The images in *vmdk* format are usable directly in VMware Player, Server
and Workstation products. For use in ESX, ESXi and vSphere they must be
converted using [VMware
converter](http://www.vmware.com/products/converter/).

##### - HDD/flash image (raw)

    dd if=./zabbix_appliance_3.2.0_x86_64.raw of=/dev/sdc bs=4k conv=fdatasync

Replace */dev/sdc* with your Flash/HDD disk device.

#### - Known issues
