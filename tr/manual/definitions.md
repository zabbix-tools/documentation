# 2. Tanımlar

#### Genel bakış

Bu bölümde, Zabbix'te yaygın olarak kullanılan bazı terimlerin
anlamlarını öğrenebilirsiniz.

#### Tanımlar

***[ana makine](/manual/config/hosts)***

\- *IP/DNS ile izlemek istediğiniz ağa bağlı bir cihaz.*

***[ana makine grubu](/manual/config/hosts)***

\- *ana makinelerin mantıksal bir gruplandırması; Ana makineler ve
şablonlar içerebilir. Ana makine grubundaki ana makineler ve şablonlar
hiçbir şekilde birbirine bağlı değildir. Ana makine grupları, farklı
kullanıcı grupları için ana makinelere erişim hakları atanırken
kullanılır.*

***[öğe](/manual/config/items)***

\- *bir ama makineden almak istediğiniz belli bir veri parçası, bir
metrik veri.*

***[tetikleyici](/manual/config/triggers)***

\- *bir problem eşiğini tanımlayan ve öğelerden alınan verilerin
"değerlendirilmesi" için kullanılan mantıksal bir ifade*

Alınan veriler eşiğin üstündeyse, tetikleyiciler 'Tamam' durumundan bir
'Sorun' durumuna geçer. Alınan veriler eşiğin altında olduğunda,
tetikleyiciler 'Tamam' durumunda kalır/durumuna döner.

***[olay](/manual/config/events)***

\- *bir tetiğin değişen durumu veya keşif/aracı otomatik kaydının
gerçekleşmesi gibi dikkati hak eden tek bir meydana geliş*

***[problem](/manual/web_interface/frontend_sections/monitoring/problems)***

\- *"Problem" durumunda olan bir tetikleyici*

***[eylem](/manual/config/notifications/action)***

\- *önceden tanımlanmış bir olaya tepki yöntemi.*

Bir eylem, işlemlerden (örneğin bir bildirim gönderme) ve koşullardan
(işlem gerçekleştirildiğinde) oluşur

***[eskalasyon](/manual/config/notifications/action/escalations)***

\- *bir eylem içinde işlemleri yürütmek için özel bir senaryo; bir
bildirim gönderme/uzak komutları çalıştırma dizisi*

***[medya](/manual/config/notifications/media)***

\- *bildirim gönderme aracı; teslimat kanalı*

***[bildirim](/manual/config/notifications/action/operation/message)***

\- *seçilen medya kanalı üzerinden kullanıcıya gönderilen bir etkinlikle
ilgili ileti*

***[uzaktan
komut](/manual/config/notifications/action/operation/remote_command)***

\- *belli bir koşulda izlenen bir ana bilgisayarda otomatik olarak
çalıştırılan önceden tanımlanmış bir komut*

***[şablon](/manual/config/templates)***

\- *bir veya birkaç ana makineye uygulanmaya hazır bir grup varlık
(öğeler, tetikleyiciler, çizelgeler, ekranlar, uygulamalar, düşük seviye
keşif kuralları, web senaryoları)*

Şablonların işi bir ana bilgisayarda izleme görevlerinin dağıtımını
hızlandırmak ve ayrıca izleme görevlerine toplu değişiklikler uygulamayı
kolaylaştırmaktır. Şablonlar doğrudan tek tek ana makinelere bağlanır.

***[uygulama](/manual/config/items/applications)***

\- *öğelerin mantıksal bir grupta gruplanması*

***[web senaryosu](/manual/web_monitoring)***

\- *bir web sitesinin kullanılabilirliğini kontrol etmek için bir veya
birkaç HTTP isteği*

***[önyüz](/manual/introduction/overview#architecture)***

\- *Zabbix ile sunulan web arayüzü*

***[Zabbix API'si](/manual/api)***

\- *Zabbix API, Zabbix nesnelerini (ana makineler, öğeler, çizelgeler ve
diğerlerini) oluşturmak, güncellemek ve almak için JSON RPC protokolünü
kullanmanıza veya başka herhangi bir özel görevi gerçekleştirmenize
olanak tanır*

***[Zabbix sunucusu](/manual/concepts/server)***

\- *İzleme yapan, Zabbix proxyleri ve aracılarıyla etkileşimde bulunan,
tetikleyicileri hesaplayan, bildirim gönderen, merkezi bir Zabbix
yazılımı sürecidir; verilerin merkezi bir deposu*

***[Zabbix aracısı](/manual/concepts/agent)***

\- *Yerel kaynakları ve uygulamaları aktif olarak izlemek için izlenen
hedefler üzerinde konuşlandırılmış bir süreç*

***[Zabbix proxy](/manual/concepts/proxy)***

\- *Zabbix sunucusu adına veri toplayabilecek, sunucudan biraz işlem
yükü alabilen bir süreç*
