# Geçmiş servisi

#### Genel bakış

Zabbix 3.4'ten itibaren, isteğe bağlı olarak **tarihle ilgili veriler**
için alternatif bir depolama arka ucu kullanılabilir.

Bu seçeneğin avantajından yararlanmak için, kendi geçmiş hizmetinizi
yazmalı ve sonra bu servisin kullanımını Zabbix sunucu yapılandırmasında
belirtmelisimiz.

Öntanımlı olarak, **tüm** geçmiş değerleri, her farklı tür (imzasız
sayılar, kayan noktalı sayılar, stringler, metinler ve kayıtlar) için
geçmiş hizmetine gönderilir. Bu davranış, aşağıda açıklanan ek bir
yapılandırma parametresiyle değiştirilebilir.

Geçmiş verileri, yeni gerçekleştirilen REST API'si aracılığıyla
yapılandırılmış geçmiş hizmetine gönderilir ve oradan alınır. Geçmiş
servisinin verileri nerede ve nasıl depoladığı geçmiş servisinin
kendisine bağlıdır.

Zabbix tarafından yönetilen diğer tüm verilerin (tetikleyiciler, olaylar
vb.) Zabbix veritabanında saklanmaya devam edeceğini unutmayın.

::: notewarning
Zabbix 3.4'te bu özellik deneyseldir, dolayısıyla
üretim ortamı için kullanılması önerilmez.
:::

\*\* Neden kendi geçmiş hizmetinizi yazasınız? \*\*

Kullanıcıların kendi geçmişi hizmetlerini yazmalarına izin vermenin
başlıca yararı, geçmiş verilerini Zabbix tarafından desteklenenlerin
dışında depolama motorlarına depolamak için daha büyük bir esneklik ve
kullanımı kolay bir API'dir. Bu, kullanıcıların verileri örneğin bir
zaman serisi veritabanında (Whisperer, RRD, InfluxDB), NoSQL
veritabanında (MongoDB, Apache Cassandra) veya Elasticsearch gibi bir
arama motorunda depolamasına olanak tanır.

Geçmiş hizmetinin sorgulanması ve gösterge panosunda veri gösterilmesi
için (Grafana gibi) üçüncü taraf araçları yapılandırmak da mümkündür.
Bütün bunlar, bir REST API'sinin sağlayabileceği esneklik sayesinde
gerçekleşir.

#### Zabbix sunucusunu yapılandırma

Geçmiş hizmeti özelliğini kullanmak için:

-   Zabbix sunucusu libCURL desteğiyle derlenmeli ve kurulmalıdır;

Bir kullanıcının geçmiş hizmeti özelliğini libCURL desteği olmadan
kullanmaya çalışması durumunda, Zabbix sunucusu başlangıçta bir hata
mesajı gösterecek ve çıkacaktır.

::: noteimportant
LibCURL, (OpenSSL veya mbed TLS gibi bir
kitaplığın TLS desteğini kullanarak) https URL'lerini kullanabilir olsa
bile, bu Zabbix 3.4 için test edilmemiştir. Şifreleme desteği, Zabbix
4.0 için üretim amaçlı olarak eklenecek ve test
edilecektir.
:::

-   Zabbix sunucusu geçmiş hizmetiyle iletişim kurabilmelidir;
-   HistoryServiceURL parametresi Zabbix sunucu yapılandırmasında
    belirtilmelidir (öntanımlı olarak boştur).
-   İsteğe bağlı olarak, HistoryServiceTypes parametresi Zabbix sunucu
    yapılandırmasında belirtilebilir (öntanımlı olarak boştur).

Geçmiş hizmeti, yeni isteğe bağlı HistoryServiceURL yapılandırma
parametresinde belirtilmiştir. Parametre, formdaki bir URL olmalıdır:

          http://my.history.service[:port]/path/to/service/root

Bu tarih servisinin kökünü tanımlar. Zabbix sunucusu daha sonra bu
URL'yi uygun API sürümüyle tamamlar ve eksik çağırır. Yani, örneğin:

          http://my.history.service:8080/zabbix/v1/history/float

Burada "http://my.history.service:8080/zabbix" hizmetin köküdür ve
"/v1/history/float", Zabbix sunucusu tarafından REST API çağrısını
gerçekleştirirken eklenecek kısmıdır.

HistoryServiceTypes parametresi, geçmiş hizmetine hangi tür veri
gönderileceğini daha iyi kontrol etmek için kullanılabilir. Belli bir
kurulumda, bir kullanıcı, "işaretsiz sayıları" ve "kayan noktalı
sayıları", InfluxDB gibi bir zaman serisi veritabanında ve "kayıt",
"metin" ve "karakter" gibi veri türlerini zabbix veritabanında depolamak
isteyebilir. Bunu yapmak için HistoryServiceTypes parametresini
aşağıdaki gibi ayarlayın:

          HistoryServiceTypes=unum,float

Parametre ayarlanmazsa ve geçmiş hizmeti kullanılırsa öntanımlı değer şu
şekilde olacaktır:

          HistoryServiceTypes=unum,float,char,log,text

Böylece, tüm türler için tüm değerler geçmiş hizmetine gönderilir.

#### API sürümü

Bu kılavuzda API sürüm 1 açıklanmıştır. Her bir API, yolun ilk kısmı
olarak bir 'API sürümü' tanımlayıcısı ile başlamalıdır. Bu sürümde yol
şöyle olmalıdır:

      POST v1/history/<değer_türü>

Zabbix 3.4 için deneysel olan API, Zabbix 4.0'dan önce değiştirilebilir.
Sürüm Zabbix 4.0 için de "v1" olarak kalacak.

### API'lerin LİSTESİ

Burada bildirilen API'ler, geçmiş servisi ile Zabbix sunucusu arasındaki
doğru entegrasyon için gereklidir. Kullanıcılar, diğer programlarla daha
kolay entegrasyon sağlamak için geçmiş hizmetinin sunduğu API'leri
genişletebilir.

API'ler aşağıda listelenmiştir:

|Geçmiş|Açıklama|
|--------|----------|
|POST v1/history/<değer türü>|Geçmiş servisine öğe verilerini kaydetme|
|GET v1/history/<değer türü>/<itemid>?count=<sayı>&start=<saniye>&end=<saniye>|Tanımlı bir zaman damgası veya zaman aralığı öğesi için bir veya daha çok geçmiş değeri alın|

**Değer türleri**

URL'lerin bir parçası olarak 'değer türü' belirtilmiş olması gereken tüm
isteklerin aşağıdaki değerlerden birine sahip olması gerekir:

-   `unum` - işaretsiz tam sayı
-   `float` - kayan noktalı değer
-   `char` - karakter
-   `log` - kayıt
-   `text` - metin

##### Hata kodları

|Kod|Etken|
|---|-----|
|200|Tamam|
|201|Oluşturuldu|
|400|Kötü istek. Hatalı biçimlendirilmiş JSON|
|404|Bulunamadı. Varolmayan bir API için bir istek gerçekleştirildi|
|408|Zaman aşımı. İstemci isteği yerine getirmesi çok uzun sürdü|
|500|Dahili sunucu hatası.|

##### Geçmiş

**POST**

    POST v1/history/<veri türü>

Geçmiş servisine öğe geçmiş verilerini saklar

İstek gövdesi

-   itemid: saklanacak öğenin kimlik bilgileri.
-   value: Öğe değeri.
-   ttl (isteğe bağlı): (saniye olarak) değerin yaşama süresi.
-   sec: saniye hassasiyetli zaman damgası.
-   ns: nanosaniye hassasiyetli alan.

"log" değeri türü için "value" alanı, aşağıdakilerden oluşan bir JSON
alt nesnesi olacaktır:

-   timestamp (tam sayı): kayıt damgası.
-   source (64 karakter): Günlük kaydının kaynağı.
-   severity (tamsayı): Günlük kaydının önem düzeyi.
-   logeventid (tamsayı): Günlüğe kaydedilen olay için tanımlayıcı.
-   value (string): Gerçek satır kayıt değeri.

"log" türünün dışındaki her şey için istek:

    [{
            "itemid": 1234,
            "value": "abc",
            "sec": 0,
            "ns": 0
    }]

"log" tipi için istek:

    [{
            "itemid": 1234,
            "value": "{
                "timestamp": 0,
                "source": "abc",
                "severity": 0,
                "logeventid": 0,
                "value": "abc"
            },
            "sec": 0,
            "ns": 0
    }]

Veriler başarıyla saklanırsa API, bir 201 (oluşturuldu) HTTP kodu
döndürmek ZORUNDADIR. Diğer hatalar için
[yukarıya](/manual/concepts/server/history_service#error_codes) bakın.

**GET**

    GET v1/history/<değer türü>/<itemid>?count=<sayı>&start=<saniye>&end=<saniye>

Tanımlanmış bir zaman damgası veya zaman aralığı için bir öğenin bir
veya daha çok geçmiş değerini alır.

Parametreler:

-   count (isteğe bağlı): Alınacak öğe sayısı.
-   start (isteğe bağlı): bir zaman aralığının başlangıcı için zaman
    damgası.
-   end (isteğe bağlı): aralığın bitişi için zaman damgası.

Yanıt gövdesi: öğe değerlerinin JSON dizisi

-   value: Değerin kendisi (değer sözdizimi, desteklenen JSON türleriyle
    aynı olacaktır).
-   sec: saniye hassasiyetli zaman damgası.
-   ns: nanosaniye hassasiyetli alanı.

```{=html}
<!-- -->
```
    [{
            "value": "abc",
            "sec": 0,
            "ns": 0
    }]

"log" değeri türü için "value" alanı, aşağıdaki JSON alt nesnelerinden
biri olacaktır:

-   timestamp (tam sayı): Kayıt zaman damgası.
-   source (64 karakter): Günlük kaydının kaynağı.
-   severity (tamsayı): Günlük kaydının önem düzeyi.
-   logeventid (integer): Kaydedilen olay için tanımlayıcı.
-   value (string): Gerçek satır kayıt değeri.

```{=html}
<!-- -->
```
    [{
            "value": "{
                "timestamp": 0,
                "source": "abc",
                "severity": 0,
                "logeventid": 0,
                "value": "abc"
            },
            "sec": 0,
            "ns": 0
    }]

Davranış:

API, hangi parametrelerin belirtildiğine bağlı olarak farklı davranıyor
olacak. Aşağıdaki tabloda, parametrelerin belirtildiği tüm durumların ve
kombinasyonlarının davranışları gösterilmektedir. Belirtilen bir
parametre 'X' ile işaretlenmiştir:

|başlangıç|bitiş|sayı|davranış|
|------------|------|-----|----------|
|<|<|<|Öğe için saklanan tüm geçmiş değerlerini al.|
|X|<|<|**Başladıktan** sonra saklanan tüm geçmiş değerini al|
|<|X|<|**Bitmeden** önce saklanan tüm geçmiş değerini al|
|X|X|<|**Başlangıç** ve **bitiş** arasında saklanan geçmiş değerlerinin tümünü al|
|<|<|X|Öğe için kayıtlı geçmiş değerlerini belirtilen **sayıya** kadar al.|
|X|<|X|**Başladıktan** sonra saklanan geçmiş değerini belirtilen **sayıya** kadar al|
|<|X|X|**Bitmeden** önce saklanan geçmiş değerini belirtilen **sayıya** kadar al|
|X|X|X|**Başlangıç** ve **bitiş** arasında saklanan geçmiş değerlerini belirtilen **sayıya** kadar al|

Elde edilen değerlerin **saniye** alanına dayalı olarak sıralanması
zorunludur.

Hata kodları için
[yukarıya](/manual/concepts/server/history_service#error_codes) bakın.
