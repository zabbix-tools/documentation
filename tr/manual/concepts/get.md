# - \#6 Alıcı

#### Genel bakış

Zabbix get, Zabbix aracısıyla iletişim kurmak ve aracıdan gerekli
bilgileri almak için kullanılabilen bir komut satırı aracıdır.

Araç, genellikle Zabbix aracısının sorunlarını gidermek için kullanılır.

#### Zabbix get'i çalıştırmak

Aracıdan işlemci yükü değeri elde etmek için UNIX altında Zabbix get
çalıştırma örneği:

    shell> cd bin
    shell> ./zabbix_get -s 127.0.0.1 -p 10050 -k system.cpu.load[all,avg1]

Bir web sitesinden bir stringi yakalamak için diğer bir Zabbix get
örneği:

    shell> cd bin
    shell> ./zabbix_get -s 192.168.1.1 -p 10050 -k "web.page.regexp[www.zabbix.com,,,\"USA: ([a-zA-Z0-9.-]+)\",,\1]"

Buradaki öğe anahtarının bir boşluk içerdiğini unutmayın; bu yüzden öğe
anahtarını kabukta yazmak için tırnak işaretleri kullanılır. Tırnaklar
öğe anahtarının bir parçası değildir; kabuk tarafından kırpılacaktır ve
Zabbix aracısına geçmeyeceklerdir.

Zabbix get, aşağıdaki komut satırı parametrelerini kabul eder:

      -s --host <ana makine ismi veya IP> Bir ana bilgisayarın adını veya IP adresini belirtir.
      -p --port <port numarası>           Ana bilgisayarda çalışan aracının port numarasını belirtir. Öntanımlı 10050'dir.
      -I --source-address <IP adresi>     Kaynak IP adresini belirtir.
      -k --key <öğe anahtarı>             Değeri alınacak öğenin anahtarını belirtir.
      -h --help                           Bu yardımı verir.
      -V --version                        Sürüm  numarasını gösterir.

Daha çok bilgi için [Zabbix get'in man sayfasına](/manpages/zabbix_get)
bakınız.

Zabbix get, Windows'ta da benzer şekilde çalıştırılabilir:

    zabbix_get.exe [seçenekler]
