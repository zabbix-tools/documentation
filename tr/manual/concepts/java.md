# - \#4 Java ağ geçidi

#### Genel bakış

JMX uygulamalarının izlenmesi için yerel destek, Zabbix 2.0'dan itibaren
kullanılabilen "Zabbix Java ağ geçidi" adlı bir Zabbix artalan süreci
şeklinde bulunur. Zabbix Java ağ geçidi, Java ile yazılmış bir artalan
sürecidir. Zabbix sunucusu, bir ana bilgisayarda belirli bir JMX
sayacının değerini bulmak için ilgilenilen uygulamayı uzaktan sorgulamak
için [JMX yönetim
API'sini](http://java.sun.com/javase/technologies/core/mntr-mgmt/javamanagement/)
kullanan Zabbix Java ağ geçidini sorgular. Uygulamanın herhangi bir ek
yazılıma ihtiyacı yoktur, sadece komut satırında
-Dcom.sun.management.jmxremote seçeneğiyle başlatılması gerekir.

Java ağ geçidi, Zabbix sunucusundan veya proxysinden gelen bağlantıları
kabul eder ve yalnızca bir "pasif proxy" olarak kullanılabilir. Zabbix
proxynin aksine, Zabbix proxyden de kullanılabilir (Zabbix proxyler
zincirlenemez). Her bir Java ağ geçidine erişim doğrudan Zabbix sunucu
veya proxy yapılandırma dosyasında yapılandırılır, bu nedenle her Zabbix
sunucu veya Zabbix proxysi için yalnızca bir Java ağ geçidi
yapılandırılabilir. Bir ana makine **JMX aracısı** ve diğer türlerden
öğeler içeriyorsa, Java ağ geçidine bilgi çekmek için yalnızca **JMX
aracı** öğeleri geçebilir.

Java ağ geçidi üzerinden bir öğe güncelleneceğinde, Zabbix sunucusu veya
proxysi, Java ağ geçidine bağlanıp değer isteyecek ve Java ağ geçidi de
sırayla bilgiyi çekip sunucuya veya proxyye geri gönderecektir. Bu
nedenle, Java ağ geçidi herhangi bir değeri önbelleğe almaz.

Zabbix sunucusu ve proxysi, Java ağ geçidine bağlanan ve
**StartJavaPollers** seçeneğiyle kontrol edilen belirli bir süreç türüne
sahiptir. Dahili olarak, Java ağ geçidi, **START\_POLLERS** seçeneğiyle
kontrol edilen birden çok iş parçacığı başlatır. Sunucu tarafında, bir
bağlantı **Timeout** (Zaman Aşımı) süresinden uzun sürerse
sonlandırılacaktır, ancak Java ağ geçidi hala JMX sayacından değer
almakla meşgul olabilir. Bunu çözmek için, Zabbix 2.0.15, Zabbix 2.2.10
ve Zabbix 2.4.5'ten beri Java ağ geçidinde JMX ağ işlemleri için zaman
aşımını ayarlamayı sağlayan **TIMEOUT** seçeneği bulunmaktadır.

Zabbix sunucusu veya proxysi, istekleri mümkün olduğunca tek bir JMX
hedefinde sorgulamaya çalışacak (öğe aralıklarından etkilenir) ve daha
iyi performans için onları tek bir bağlantıda Java Ağ Geçidine
gönderilecektir.

**StartJavaPollers** değerinin **START\_POLLERS** değerinden küçük veya
ona eşit olması önerilir, aksi halde Java ağ geçidinde gelen isteklere
hizmet etmek için hiçbir iş parçacığı bulunmadığı durumlar olabilir.

Aşağıdaki bölümlerde Zabbix Java ağ geçidininin nasıl alınıp
çalıştırılacağı, JMX izleme için Zabbix Java ağ geçidini kullanmada
Zabbix sunucusunun (veya Zabbix proxysinin) nasıl yapılandırılacağı ve
Zabbix grafik kullanıcı arayüzünde belirli JMX sayaçlarına karşılık
gelen Zabbix öğelerinin nasıl yapılandırılacağı anlatılmaktadır.

#### - Java ağ geçidini alma

Java ağ geçidini almanın iki yolu vardır. Birincisi, Java ağ geçidi
paketini Zabbix web sitesinden indirmek ve diğeri de Java ağ geçidini
kaynaktan derlemektir.

##### - Zabbix web sitesinden indirme

Zabbix Java ağ geçidi paketleri (RHEL, Debian, Ubuntu için)
<http://www.zabbix.com/download.php> adresinden indirilebilir.

##### - Kaynaktan derleme

Java ağ geçidini derlemek için önce `./configure` betiğini
`--enable-java` seçeneğiyle çalıştırın. Java ağ geçidinin kurulumu
yalnızca tek bir yürütülebilir dosya değil bütün bir dizin ağacı
oluşturacağı için öntanımlı /usr/local dışında bir yükleme yolu
ayarlamak için `--prefix` seçeneğini kullanmanız önerilir.

    $ ./configure --enable-java --prefix=$PREFIX

Java ağ geçidini bir JAR dosyasına derlemek ve paketlemek için `make`
komutunu çalıştırın. Bu adım için yolunuzda `javac` ve `jar`
yürütülebilir dosyalarının olması gerektiğini unutmayın.

    $ make

Şimdi src/zabbix\_java/bin dizini içinde
zabbix-java-gateway-$VERSION.jar dosyanız var. Eğer Java ağ geçidini,
dağıtım dizinindeki src/zabbix\_java dosyasından çalıştırmak konusunda
rahatsanız, Java ağ geçidini yapılandırma ve çalıştırma yönergelerine
geçebilirsiniz. Aksi takdirde, yeterli ayrıcalıklara sahip olduğunuzdan
emin olun ve `make install` komutunu çalıştırın.

    $ make install

#### - Java ağ geçidi dağıtımındaki dosyalara genel bakış

Java ağ geçidini nasıl elde ettiğinize bakılmaksızın,
$PREFIX/sbin/zabbix\_java altında kabuk betikleri, JAR ve yapılandırma
dosyalarının bir derlemesine sahip olarak bitirmiş olmalısınız. Bu
dosyaların rolü aşağıda özetlenmiştir.

    bin/zabbix-java-gateway-$VERSION.jar

Java ağ geçidi JAR dosyasının kendisi.

    lib/logback-core-0.9.27.jar
    lib/logback-classic-0.9.27.jar
    lib/slf4j-api-1.6.1.jar
    lib/android-json-4.3_r3.1.jar

Java ağ geçidi bağımlılıkları: [Logback](http://logback.qos.ch/),
[SLF4J](http://www.slf4j.org/) ve [Android
JSON](https://android.googlesource.com/platform/libcore/+/master/json)
kitaplığı.

    lib/logback.xml  
    lib/logback-console.xml

Logback yapılandırma dosyaları.

    shutdown.sh  
    startup.sh

Java ağ geçidinin başlatılması ve durdurulması için uygun komut
dosyaları.

    settings.sh

Yukarıdaki başlatma ve kapatma komut dosyalarının kaynak aldığı
yapılandırma dosyası.

#### - Java ağ geçidinin yapılandırılması ve çalıştırılması

Öntanımlı olarak, Java ağ geçidi, 10052 numaralı bağlantı noktasını
dinler. Java ağ geçidini farklı bir bağlantı noktasında çalıştırmayı
planlıyorsanız bunu settings.sh betiğinde belirtebilirsiniz. Bunun ve
diğer seçeneklerin nasıl belirtileceğini öğrenmek için [Java gateway
yapılandırma dosyasının](/manual/appendix/config/zabbix_java)
açıklamasına bakın.

::: notewarning
10052 numaralı bağlantı noktası [IANA tarafından
kayıtlı
değildir](http://www.iana.org/assignments/service-names-port-numbers/service-names-port-numbers.txt).
:::

Ayarlarla ilgili içiniz rahat ettiğinde, başlatma betiğini çalıştırarak
Java ağ geçidini başlatabilirsiniz:

    $ ./startup.sh

Aynı şekilde, Java ağ geçidine artık ihtiyacınız olmadığında, durdurmak
için kapatma betiğini çalıştırın:

    $ ./shutdown.sh

Sunucu veya proxynin aksine Java ağ geçidi hafiftir ve bir veritabanına
ihtiyaç duymadığını unutmayın.

#### - Sunucuyu Java ağ geçidiyle kullanmak üzere yapılandırma

Şimdi Java ağ geçidi çalışıyor, Zabbix sunucuya Zabbix Java ağ geçidini
nerede bulacağını söylemeniz gerekiyor. Bu, JavaGateway ve
JavaGatewayPort parametrelerini [sunucu yapılandırma
dosyasında](/manual/appendix/config/zabbix_server) belirterek yapılır.
JMX uygulamasının çalıştığı ana makine Zabbix proxy tarafından
izlenirse, bağlantı parametrelerini [proxy yapılandırma
dosyasında](/manual/appendix/config/zabbix_proxy) belirtin.

    JavaGateway=192.168.3.14
    JavaGatewayPort=10052

Öntanımlı olarak sunucu, JMX izlemeyle ilgili herhangi bir süreç
başlatmaz. Bununla birlikte, bunu kullanmak isterseniz, önceden
çatallanmış (pre-forked) Java sorgulayıcı örneklerini belirtmeniz
gerekir. Bunu, normal sorgulayıcıları ve izleyicileri belirttiğiniz gibi
yapabilirsiniz.

    StartJavaPollers=5

Ayarladıktan sonra sunucuyu veya proxyyi yeniden başlatmayı unutmayın.

#### - Java ağ geçidinin hata ayıklanması

Java ağ geçidinde herhangi bir sorun olduğunda veya önyüzdeki bir öğe
hakkında gördüğünüz hata iletisinin yeterince açıklayıcı olmadığı
durumlarda, Java ağ geçidi kayıt dosyasına bakmak isteyebilirsiniz.

Öntanımlı olarak Java ağ geçidi, etkinliklerini "info" kayıt seviyesiyle
/tmp/zabbix\_java.log dosyasına kaydeder. Bazen bu bilgi yeterli
değildir ve “debug” kayıt seviyesinde bilgiye ihtiyaç vardır. Kayıt
seviyesini artırmak için lib/logback.xml dosyasını değiştirin ve
<root> etiketinin level özniteliğini "debug" olarak değiştirin:

    <root level="debug">
      <appender-ref ref="FILE" />
    </root>

Zabbix sunucusunun veya Zabbix proxysinin aksine, logback.xml dosyasını
değiştirdikten sonra Zabbix Java ağ geçidini yeniden başlatmaya gerek
olmadığını unutmayın, logback.xml'deki değişiklikler otomatik olarak
alınır. Hata ayıklama işlemini tamamladığınızda kaydetme seviyesini
"bilgi" seviyesine döndürebilirsiniz.

Farklı bir dosyaya veya veritabanı gibi tamamen farklı bir ortama oturum
açmak isterseniz, ihtiyaçlarınızı karşılamak için logback.xml dosyasını
ayarlayın. Daha çok bilgi için [Logback El
Kitabına](http://logback.qos.ch/manual/) bakınız.

Bazen hata ayıklama amacıyla, Java ağ geçidini bir artalan süreci yerine
bir konsol uygulaması olarak çalıştırmak kullanışlıdır. Bunu yapmak
için, settings.sh dosyasındaki PID\_FILE değişkenini yorum haline
çevirin. PID\_FILE atlanırsa, startup.sh betiği Java ağ geçidini bir
konsol uygulaması olarak başlatır ve Logback'e lib/logback-console.xml
dosyasını kullandırtarak yalnızca konsolda kayıtları göstermesini değil,
aynı zamanda "hata ayıklama" seviyesiyle günlük dosyasına da
kaydetmesini sağlar.

Finally, note that since Java gateway uses SLF4J for logging, you can
replace Logback with the framework of your choice by placing an
appropriate JAR file in lib directory. See [SLF4J
Manual](http://www.slf4j.org/manual.html) for more details. Son olarak,
Java ağ geçidi günlüğe kaydetme için SLF4J kullandığından, lib dizininde
uygun bir JAR dosyası yerleştirerek Logback'i seçtiğiniz başka bir
çatıyla değiştirebilirsiniz. Daha çok ayrıntı için [SLF4J El
Kitabına](http://www.slf4j.org/manual.html) bakın.
