# - \#2 Aracı

#### Genel bakış

Zabbix aracısı, yerel kaynakları ve uygulamaları (sabit diskler, bellek,
işlemci istatistiği vb.) aktif olarak izlemek için bir izleme hedefi
üzerinde konuşlandırılır.

Aracı, operasyonel bilgileri lokal olarak toplar ve daha sonraki
işlemler için verileri Zabbix sunucusuna rapor eder. Zabbix sunucusu
(bir sabit disk tam dolu çalışıyorken veya çökmüş servis süreci gibi)
bir arıza durumunda başarısızlığı bildiren belirli bir makinenin
yöneticilerini aktif olarak uyarabilir.

Zabbix aracıları, istatistiki bilgileri toplamak için yerel sistem
çağrıları kullandığından son derece etkilidirler.

##### Pasif ve aktif kontroller

Zabbix aracıları pasif ve aktif kontroller gerçekleştirebilir.

Bir [pasif
kontrolde](/manual/appendix/items/activepassive#passive_checks), aracı
bir veri talebine yanıt verir. Zabbix sunucusu (veya proxy) veri ister,
örneğin CPU yükünü ve Zabbix aracısı sonucu geri gönderir.

[Aktif kontroller](/manual/appendix/items/activepassive#active_checks)
daha karmaşık işlem gerektirir. Aracı, bağımsız işlem için önce Zabbix
sunucusundan bir öğe listesi almalıdır. Ardından, yeni değerleri
sunucuya periyodik olarak gönderecektir.

Pasif mi aktif mi kontrol gerçekleştirileceği ilgili izleme [öğe
türünü](/manual/config/items/itemtypes/zabbix_agent) seçerek
yapılandırılır. Zabbix aracısı 'Zabbix agent' veya 'Zabbix agent
(active)' türündeki öğeleri işler.

#### Desteklenen platformlar

Zabbix aracısı aşağıdakiler için desteklenmektedir:

-   Linux
-   IBM AIX
-   FreeBSD
-   NetBSD
-   OpenBSD
-   HP-UX
-   Mac OS X
-   Solaris: 9, 10, 11
-   Windows: 2000'den sonraki tüm masaüstü ve sunucu sürümleri

#### UNIX benzeri sistemlerde aracı

UNIX benzeri sistemlerde Zabbix aracıları, izlenen ana bilgisayarda
çalıştırılır.

##### Kurulum

Zabbix aracısını paket olarak nasıl kuracağınıza ilişkin yönergeler için
[paket yükleme](/manual/installation/install_from_packages) bölümüne
bakın.

Paketleri kullanmak istemiyorsanız alternatif olarak [elle
kurulum](/manual/installation/install#installing_zabbix_daemons)
yönergelerine bakın.

::: noteimportant
Genel olarak, 32bit Zabbix aracıları 64bit
sistemlerde çalışacaktır, ancak bazı durumlarda başarısız
olabilirler.
:::

##### Paket olarak kurulmuşsa

Zabbix aracı bir artalan süreci olarak çalışır. Aracı, aşağıdakileri
uygulayarak başlatılabilir:

    shell> service zabbix-agent start

Bu, GNU/Linux sistemlerinin çoğunda çalışır. Diğer sistemlerde,
aşağıdaki komutu çalıştırmanız gerekebilir:

    shell> /etc/init.d/zabbix-agent start

Benzer şekilde, Zabbix aracısının durumunu durdurma/yeniden
başlatma/izleme için aşağıdaki komutları kullanın:

    shell> service zabbix-agent stop
    shell> service zabbix-agent restart
    shell> service zabbix-agent status

##### Elle başlatma

Yukarıdaki işe yaramazsa elle başlatma yapmanız gerekir. zabbix\_agentd
çalıştırılabilir dosyasının yolunu bulun ve onu çalıştırın:

    shell> zabbix_agentd

#### Windows sistemlerde aracı

Windows'taki Zabbix aracıları bir Windows hizmeti olarak çalışır.

##### Hazırlık

Zabbix aracısı bir zip arşivi olarak dağıtılır. Arşivi indirdikten sonra
arşivi açmanız gerekir. Zabbix aracısını ve konfigürasyon dosyasını
saklamak için herhangi bir klasörü seçin, örneğin:

    C:\zabbix

bin\\zabbix\_agentd.exe ve conf\\zabbix\_agentd.conf dosyalarını
c:\\zabbix altına kopyalayın.

c:\\zabbix\\zabbix\_agentd.conf dosyasını ihtiyaçlarınıza göre
düzenleyin ve doğru bir "Hostname" parametresi belirtmeyi unutmayın.

##### Kurulum

Bunlar yapıldıktan sonra, Zabbix aracısını Windows hizmeti olarak kurmak
için aşağıdaki komutu kullanın:

    C:\> c:\zabbix\zabbix_agentd.exe -c c:\zabbix\zabbix_agentd.conf -i

Artık "Zabbix aracı" hizmetini diğer Windows hizmetleri gibi
yapılandırabilmelisiniz.

Windows'ta Zabbix aracısını kurma ve çalıştırma hakkında [daha çok
ayrıntıya](/manual/appendix/install/windows_agent#installing_agent_as_windows_service)
bakın.

#### Diğer aracı seçenekleri

Aracının birden çok örneğini bir ana bilgisayarda çalıştırmak mümkündür.
Tek bir örnek öntanımlı yapılandırma dosyasını veya komut satırında
belirtilen bir yapılandırma dosyasını kullanabilir. Birden çok örneğin
olması durumunda, her aracının kendi yapılandırma dosyası olmalıdır
(örneklerden biri öntanımlı yapılandırma dosyasını kullanabilir).

Aşağıdaki komut satırı parametreleri Zabbix aracısıyla kullanılabilir:

|**Parametre**|**Açıklama**|
|-------------|--------------|
|**UNIX ve Windows aracısı**|<|
|-c --config <yapılandırma-dosyası>|Yapılandırma dosyasının yolu.<br>Öntanımlı olmayan bir yapılandırma dosyası belirtmek için bu seçeneği kullanabilirsiniz.<br>UNIX'te öntanımlı değer /usr/local/etc/zabbix\_agentd.conf veya [derleme zamanı](/manual/installation/install#installing_zabbix_daemons) değişkenleri *--sysconfdir* veya *--prefix* tarafından ayarlandığı gibidir<br>Windows'ta öntanımlı değer c:\\zabbix\_agentd.conf'dur.|
|-p --print|Bilinen öğeleri bastırır ve çıkar.<br>*Not*: [kullanıcı parametresi](/manual/config/items/userparameters) sonuçlarını da döndürmek için yapılandırma dosyasını belirtmelisiniz (öntanımlı konumda değilse).|
|-t --test <öğe anahtarı>|Belirtilen öğeyi test eder ve çıkar.<br>*Not*: [kullanıcı parametresi](/manual/config/items/userparameters) sonuçlarını da döndürmek için yapılandırma dosyasını belirtmelisiniz (öntanımlı konumda değilse).|
|-h --help|Yardım bilgilerini gösterir|
|-V --version|Sürüm numarasını gösterir|
|**Yalnızca UNIX aracısı**|<|
|-R --runtime-control <seçenek>|Yönetimsel fonksiyonları yerine getirir. [Çalışma zamanı kontrolüne](/manual/concepts/agent#runtime_control) bakın.|
|**Yalnızca Windows aracısı**|<|
|-m --multiple-agents|Birden çok aracı örneği kullanın (-i, -d, -s, -x fonksiyonlarıyla).<br>Örneklerin hizmet adlarını ayırt etmek için her hizmet adı belirtilen yapılandırma dosyasındaki Ana Bilgisayar adı değerini içerir.|
|**Yalnızca Windows aracısı (fonksiyonlar)**|<|
|-i --install|Zabbix Windows aracısını hizmet olarak yükle|
|-d --uninstall|Zabbix Windows aracı hizmetini kaldır|
|-s --start|Zabbix Windows aracı hizmetini başlat|
|-x --stop|Zabbix Windows aracı hizmetini durdur|

Komut satırı parametrelerini kullanmayla ilgili belirli **örnekler**:

-   Bütün yerleşik aracı öğelerini değerleriyle yazdırma
-   Belirtilen yapılandırma dosyasında tanımlanan "mysql.ping"
    anahtarıyla bir kullanıcı parametresini test etme
-   Windows için yapılandırma dosyasının öntanımlı yolu
    c:\\zabbix\_agentd.conf olan bir "Zabbix Aracısı" hizmeti yüklemek
-   Aracı çalıştırılabilir dosyasıyla aynı klasörde bulunan
    zabbix\_agentd.conf yapılandırma dosyasını kullanarak Windows için
    bir “Zabbix Agent \[Hostname\]” hizmeti yükleyerek ve hizmet adını
    yapılandırma dosyasından Hostname değeriyle uzatarak benzersiz yapın

```{=html}
<!-- -->
```
    shell> zabbix_agentd --print
    shell> zabbix_agentd -t "mysql.ping" -c /etc/zabbix/zabbix_agentd.conf
    shell> zabbix_agentd.exe -i
    shell> zabbix_agentd.exe -i -m -c zabbix_agentd.conf

##### Çalışma zamanı kontrolü

Çalışma zamanı kontrol seçenekleriyle aracı süreçlerinin kayıt
seviyesini değiştirebilirsiniz.

|Seçenek|Açıklama|Hedef|
|--------|----------|-----|
|log\_level\_increase\[=<hedef>\]|Kayıt seviyesini artırır.<br>Hedef belirtilmemişse, tüm işlemler etkilenir.|Hedef şu şekilde belirtilebilir:<br>`pid` - süreç tanımlayıcısı (1 - 65535)<br>`süreç türü` - belirtilen türdeki tüm işlemler (örneğin, poller)<br>`süreç türü,N` - süreç türü ve numarası (ör. poller,3)|
|log\_level\_decrease\[=<hedef>\]|Kayıt seviyesini azaltır.<br>Hedef belirtilmemişse, tüm işlemler etkilenir.|^|

Tek bir aracının kayıt seviyesini değiştirmek için kullanılabilir
PID'lerin aralığının 1 ila 65535 arasında olduğunu unutmayın. Büyük
PID'li sistemlerde, <süreç türü,N> hedefi tek bir işlemin kayıt
seviyesini değiştirmek için kullanılabilir.

Örnekler:

-   Tüm süreçlerin kayıt seviyesini arttırma
-   Ikinci dinleyici sürecinin kayıt seviyesini arttırma
-   PID değeri 1234 olan sürecin kayıt seviyesini arttırma
-   Tüm etkin kontrol süreçlerinin kayıt seviyesini azaltma

```{=html}
<!-- -->
```
    shell> zabbix_agentd -R log_level_increase
    shell> zabbix_agentd -R log_level_increase=listener,2
    shell> zabbix_agentd -R log_level_increase=1234
    shell> zabbix_agentd -R log_level_decrease="active checks"

::: noteclassic
Çalışma zamanı kontrolü, OpenBSD, NetBSD ve Windows'ta
desteklenmiyor.
:::

#### Süreç kullanıcısı

UNIX üzerindeki Zabbix aracıları, root olmayan bir kullanıcı tarafından
çalıştırılacak şekilde tasarlanmıştır. root olmadığı sürece hangi
kullanıcı çalıştırırsa çalıştırsın açılacaktır. Böylece, aracıyı
herhangi bir sorun olmadan root olmayan herhangi bir kullanıcı olarak
çalıştırabilirsiniz.

root' olarak çalıştırmayı deneyecekseniz, sisteminizde bulunması
gereken, doğrudan kodlanmış bir 'zabbix' kullanıcısına geçecektir.
Yalnızca sunucu yapılandırma dosyasındaki 'AllowRoot' parametresini buna
göre değiştirirseniz, sunucuyu 'root' olarak çalıştırabilirsiniz.

#### Yapılandırma dosyası

Zabbix aracısını yapılandırmayla ilgili ayrıntılar için
[zabbix\_agentd](/manual/appendix/config/zabbix_agentd) veya [Windows
agent](/manual/appendix/config/zabbix_agentd_win)'in yapılandırma
dosyası seçeneklerine bakın.

#### Çıkış kodu

Sürüm 2.2'den önce, Zabbix aracıları, başarılı çıkış durumunda 0, arıza
durumunda 255 döndürürdü. Sürüm 2.2 ve üzeri sürümlerden başlayarak
Zabbix aracı başarılı çıkış durumunda 0, başarısızlık durumunda 1
döndürür.
