# - \#5 Gönderici

#### Genel bakış

Zabbix sender, performans verilerini işleme amacıyla Zabbix sunucusuna
göndermekte kullanılabilen bir komut satırı aracıdır.

Betik genellikle kullanılabilirlik ve performans verilerini periyodik
olarak göndermek için uzun süren kullanıcı betiklerinde kullanılır.

#### Zabbix sender'ı çalıştırmak

Zabbix UNIX göndericisinin çalıştırılmasına bir örnek:

    shell> cd bin
    shell> ./zabbix_sender -z zabbix -s "Linux DB3" -k db.connections -o 43

burada:

-   z - Zabbix sunucu ana bilgisayarı (IP adresi de kullanılabilir)
-   s - izlenen ana bilgisayarın teknik adı (Zabbix önyüzünde
    kaydedildiği şekliyle)
-   k - öğe anahtarı
-   o - gönderilecek değer

::: noteimportant
Boşluk içeren seçenekler, çift tırnak kullanılarak
yazılmalıdır.
:::

Zabbix sender, bir girdi dosyasından birden çok değer göndermek için
kullanılabilir. Daha çok bilgi için [Zabbix sender man
sayfasına](/manpages/zabbix_sender) bakın.

Zabbix semder, (hem UNIX benzeri sistemler hem de Windows için) dosyanın
başında bayt düzen işareti (BOM) olmayan UTF-8 kodlamalı stringleri
kabul eder.

Windows'taki Zabbix göndericisi de benzer şekilde çalıştırılabilir:

    zabbix_sender.exe [seçenekler]

Zabbix 1.8.4'ten itibaren, zabbix\_sender gerçek zamanlı gönderme
senaryoları, arka arkaya geçirilen birden çok değeri toplamak ve bunları
tek bir bağlantıda sunucuya göndermek üzere geliştirildi. Önceki
değerden 0.2 saniyeden daha geç olmayan bir değer aynı yığın içine
konabilir, ancak maksimum havuzlama (ortaklama) süresi hala 1 saniyedir.

::: noteclassic
Belirtilen yapılandırma dosyasında geçersiz
(*parametre=değer* notasyonunu izlemeyen) parametre girişi mevcutsa
Zabbix sender sonlandırılacaktır.
:::
