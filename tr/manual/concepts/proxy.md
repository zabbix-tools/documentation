# - \#3 Proxy

#### Genel bakış

Zabbix proxysi, bir veya daha çok izlenen aygıta ait izleme verilerini
toplayıp Zabbix sunucusuna gönderen, gerçekte sunucu adına çalışan bir
süreçtir. Toplanan tüm veriler yerel olarak arabelleğe alınır ve
proxynin ait olduğu Zabbix sunucusuna aktarılır.

Bir proxyyi dağıtmak isteğe bağlıdır, ancak tek bir Zabbix sunucusunun
yükünü dağıtmak için çok yararlı olabilir. Yalnızca proxyler veri
toplayırsa, sunucu üzerindeki işlemlerin CPU kullanımı ve G/Ç'ı daha az
olur.

Zabbix proxy; uzaktaki yerleri, şubeleri ve ağları yerel yöneticiler
olmadan merkezi olarak izlemek için ideal bir çözümdür.

Zabbix proxysi ayrı bir veritabanı gerektirir.

::: noteimportant
Zabbix proxy ile desteklenen veritabanlarının
SQLite, MySQL ve PostgreSQL olduğunu unutmayın. Oracle veya IBM DB2'yi
kullanmak kendi sorumluluğunuzdadır ve düşük düzeyli keşif kurallarının
[dönüş değerlerinde](/manual/discovery/low_level_discovery#overview)
olduğu gibi bazı sınırlamalar içerebilir.
:::

Ayrıca bkz: [Proxyleri dağıtık bir ortamda
kullanma](/manual/distributed_monitoring/proxies)

#### Proxy süreci

##### Paket olarak kurulmuşsa

Zabbix proxysi bir artalan süreci olarak çalışır. Proxy, aşağıdaki komut
uygulanarak başlatılabilir:

    shell> service zabbix-proxy start

Bu, GNU/Linux sistemlerinin çoğunda çalışır. Diğer sistemlerde aşağıdaki
komutu çalıştırmanız gerekebilir:

    shell> /etc/init.d/zabbix-proxy start

Benzer şekilde, Zabbix proxysinin durumunu durdurma/yeniden
başlatma/izleme için aşağıdaki komutları kullanın:

    shell> service zabbix-proxy stop
    shell> service zabbix-proxy restart
    shell> service zabbix-proxy status

##### Elle başlatma

Yukarıdakiler işe yaramazsa elle başlatma yapmanız gerekir.
zabbix\_proxy çalıştırılabilir dosyasının yolunu bulun ve onu
çalıştırın:

    shell> zabbix_proxy

Zabbix proxy ile aşağıdaki komut satırı parametrelerini
kullanabilirsiniz:

    -c --config <dosya>              yapılandırma dosyası yolu
    -R --runtime-control <seçenek>   yönetimsel fonksiyonlar gerçekleştirir
    -h --help                        bu yardımı verir
    -V --version                     sürüm numarasını gösterir

::: noteclassic
Çalışma zamanı kontrolü, OpenBSD ve NetBSD'de
desteklenmiyor.
:::

Zabbix proxysinin komut satırı parametreleriyle çalıştırılmasına
örnekler::

    shell> zabbix_proxy -c /usr/local/etc/zabbix_proxy.conf
    shell> zabbix_proxy --help
    shell> zabbix_proxy -V

##### Çalışma zamanı kontrolü

Çalışma zamanı kontrol seçenekleri:

|Seçenek|Açıklama|Hedef|
|--------|----------|-----|
|config\_cache\_reload|Yapılandırma önbelleğini yeniden yükle. Önbellek o anda yüklenmişse yok sayılır.<br>Aktif Zabbix proxysi Zabbix sunucusuna bağlanacak ve yapılandırma verilerini isteyecektir.|<|
|housekeeper\_execute|Temizlik prosedürünü başlatır. Temizlik işlemi halen devam ediyorsa yok sayılır.|<|
|log\_level\_increase\[=<**hedef**>\]|Kayıt seviyesini artırır, hedef belirtilmemişse tüm işlemleri etkiler.|**pid** - süreç tanımlayıcısı (1'den 65535'e)<br>**süreç türü** - Belirtilen türdeki tüm işlemler (örneğin, poller)<br>**süreç türü,N** - Süreç türü ve numarası (ör. poller, 3)|
|log\_level\_decrease\[=<**hedef**>\]|Kayıt seviyesini azaltır, hedef belirtilmemişse tüm işlemleri etkiler.|^|

Tek bir Zabbx sürecinin kayıt seviyesini değiştirmek için izin verilen
PID aralığı 1'den 65535'e kadardır. Büyük PID'li sistemlerde <süreç
türü,N> hedef seçeneği tek bir sürecin kayıt seviyesini değiştirmek
için kullanılabilir.

Proxy yapılandırma önbelleğini yeniden yüklemek için çalışma zamanı
denetimini kullanma örneği:

    shell> zabbix_proxy -c /usr/local/etc/zabbix_proxy.conf -R config_cache_reload

Temizlikçinin çalıştırılmasını tetiklemek için çalışma zamanı denetimini
kullanma örneği

    shell> zabbix_proxy -c /usr/local/etc/zabbix_proxy.conf -R housekeeper_execute

Kayıt seviyesini değiştirmek için çalışma zamanı denetimi kullanma
örnekleri:

    Tüm süreçlerin kayıt seviyesini artırma:
    shell> zabbix_proxy -c /usr/local/etc/zabbix_proxy.conf -R log_level_increase

    İkinci poller sürecinin kayıt seviyesini artırma:
    shell> zabbix_proxy -c /usr/local/etc/zabbix_proxy.conf -R log_level_increase=poller,2

    PID değeri 1234 olan sürecin kayıt seviyesini artırma:
    shell> zabbix_proxy -c /usr/local/etc/zabbix_proxy.conf -R log_level_increase=1234

    Tüm http poller süreçlerinin kayıt seviyesini düşürme:
    shell> zabbix_proxy -c /usr/local/etc/zabbix_proxy.conf -R log_level_decrease="http poller"

##### Süreç kullanıcısı

Zabbix proxy, root olmayan bir kullanıcı tarafından çalıştırılacak
şekilde tasarlanmıştır. root olmadığı sürece hangi kullanıcı
çalıştırırsa çalıştırsın açılacaktır. Böylece, proxyyi herhangi bir
sorun olmadan root olmayan herhangi bir kullanıcı olarak
çalıştırabilirsiniz.

'root' olarak çalıştırmayı deneyecekseniz, sisteminizde bulunması
gereken, doğrudan kodlanmış bir 'zabbix' kullanıcısına geçecektir.
Yalnızca proxy yapılandırma dosyasındaki 'AllowRoot' parametresini buna
göre değiştirirseniz, proxyyi 'root' olarak çalıştırabilirsiniz.

##### Yapılandırma dosyası

zabbix\_proxy'yi yapılandırmayla ilgili ayrıntılar için [yapılandırma
dosyası](/manual/appendix/config/zabbix_proxy) seçeneklerine bakın.

#### Desteklenen platformlar

Zabbix proxy, Zabbix sunucusunun tüm [desteklendiği
platformlarda](/manual/concepts/server#supported_platforms) çalışır.
