# - \#1 Sunucu

#### Genel bakış

Zabbix sunucu, Zabbix yazılımının merkezi sürecidir.

Sunucu, veri sorgulaması ve izlemesi yapar, tetikleyicileri hesaplar,
kullanıcılara bildirim gönderir. Zabbix aracıları ve proxylerinin,
sistemlerin kullanılabilirliği ve bütünlüğü hakkında veri rapor
ettikleri merkezi bileşendir. Sunucu, (web sunucuları ve posta
sunucuları gibi) basit servis denetimleri kullanarak ağa bağlı
hizmetleri uzaktan kontrol edebilir.

Sunucu, tüm yapılandırma, istatistiki ve operasyonel verilerin
depolandığı merkezi depodur ve Zabbix'te, izlenen sistemlerden herhangi
birinde sorunlar oluştuğunda yöneticileri aktif olarak uyaracak bir
varlıktır.

Temel bir Zabbix sunucusunun işleyişi üç ayrı bileşene ayrılır. Bunlar:
Zabbix sunucusu, web önyüzü ve veritabanı deposudur.

Zabbix için yapılandırma bilgilerinin tümü, hem sunucu hem de web
önyüzünün etkileşimde olduğu veritabanında saklanır. Örneğin, web
önyüzünü (veya API) kullanarak yeni bir öğe oluşturduğunuzda,
veritabanındaki öğeler tablosuna eklenir. Daha sonra Zabbix sunucusu
dakikada yaklaşık bir kez, öğeler tablosunu etkin olan öğelerin bir
listesi için sorgular ve ardından bu, Zabbix sunucusu içindeki bir
önbellekte saklanır. Zabbix önyüzünde yapılan değişikliklerin en son
veri bölümünde görünmesinin iki dakika sürebilmesinin nedeni budur.

#### Sunucu süreci

##### Paket olarak kurulmuşsa

Zabbix sunucusu bir artalan süreci olarak çalışır. Sunucu aşağıdakiler
uygulanarak başlatılabilir:

    shell> service zabbix-server start

Bu, GNU/Linux sistemlerinin çoğunda çalışır. Diğer sistemlerde,
aşağıdaki komutu çalıştırmanız gerekebilir:

    shell> /etc/init.d/zabbix-server start

Benzer şekilde, durdurma/yeniden başlatma/durum görüntüleme için
aşağıdaki komutları kullanın:

    shell> service zabbix-server stop
    shell> service zabbix-server restart
    shell> service zabbix-server status

##### Elle başlatma

Yukarıdakiler işe yaramazsa elle başlatma yapmanız gerekir.
zabbix\_server çalıştırılabilir dosyasının yolunu bulun ve onu
çalıştırın:

    shell> zabbix_server

Zabbix sunucu ile aşağıdaki komut satırı parametrelerini
kullanabilirsiniz:

    -c --config <dosya>             yapılandırma dosyası yolu  (öntanımlı olarak /usr/local/etc/zabbix_server.conf)
    -R --runtime-control <seçenek>  yönetimsel fonksiyonları gerçekleştirir
    -h --help                       bu yardımı verir
    -V --version                    sürüm numarasını gösterir

::: noteclassic
Çalışma zamanı kontrolü, OpenBSD ve NetBSD'de
desteklenmiyor.
:::

Zabbix sunucusunu komut satırı parametreleriyle çalıştırma örnekleri:

    shell> zabbix_server -c /usr/local/etc/zabbix_server.conf
    shell> zabbix_server --help
    shell> zabbix_server -V

##### Çalışma zamanı kontrolü

Çalışma zamanı kontrol seçenekleri:

|Seçenek|Açıklama|Hedef|
|--------|----------|-----|
|config\_cache\_reload|Yapılandırma önbelleğini yeniden yükle. Önbellek o anda yüklenmişse yok sayılır.|<|
|housekeeper\_execute|Temizlik prosedürünü başlatır. Temizlik işlemi halen devam ediyorsa yok sayılır.|<|
|log\_level\_increase\[=<**hedef**>\]|Kayıt seviyesini artırır, hedef belirtilmemişse tüm işlemleri etkiler.|**pid** - süreç tanımlayıcısı (1'den 65535'e)<br>**süreç türü** - Belirtilen türdeki tüm işlemler (örneğin, poller)<br>**süreç türü,N** - Süreç türü ve numarası (ör. poller, 3)|
|log\_level\_decrease\[=<**hedef**>\]|Kayıt seviyesini azaltır, hedef belirtilmemişse tüm işlemleri etkiler.|^|

Tek bir Zabbx işleminin kayıt seviyesini değiştirmek için izin verilen
PID aralığı 1'den 65535'e kadardır. Büyük PID'li sistemlerde <süreç
türü,N> hedef seçeneği tek bir sürecin kayıt seviyesini değiştirmek
için kullanılabilir.

Sunucu yapılandırma önbelleğini yeniden yüklemek için çalışma zamanı
denetimi kullanma örneği:

    shell> zabbix_server -c /usr/local/etc/zabbix_server.conf -R config_cache_reload

Temizlikçinin çalıştırılmasını tetiklemek için çalışma zamanı denetimini
kullanma örneği:

    shell> zabbix_server -c /usr/local/etc/zabbix_server.conf -R housekeeper_execute

Kayıt seviyesini değiştirmek için çalışma zamanı denetimini kullanma
örnekleri:

    Tüm süreçlerin kayıt seviyesini artırır:
    shell> zabbix_server -c /usr/local/etc/zabbix_server.conf -R log_level_increase

    İkinci sorgulayıcı sürecinin kayıt seviyesini artırır:
    shell> zabbix_server -c /usr/local/etc/zabbix_server.conf -R log_level_increase=poller,2

    PID'i 1234 olan sürecin kayıt seviyesini artırır:
    shell> zabbix_server -c /usr/local/etc/zabbix_server.conf -R log_level_increase=1234

    Tüm http sorgulayıcı süreçlerinin kayıt seviyesini düşürür:
    shell> zabbix_server -c /usr/local/etc/zabbix_server.conf -R log_level_decrease="http poller"

##### Süreç kullanıcısı

Zabbix sunucusu, root olmayan bir kullanıcı tarafından çalıştırılacak
şekilde tasarlanmıştır. root olmadığı sürece hangi kullanıcı
çalıştırırsa çalıştırsın açılacaktır. Böylece, sunucuyu herhangi bir
sorun olmadan root olmayan herhangi bir kullanıcı olarak
çalıştırabilirsiniz.

'root' olarak çalıştırmayı deneyecekseniz, sisteminizde
[bulunması](/manual/installation/install) gereken, doğrudan kodlanmış
bir 'zabbix' kullanıcısına geçecektir. Yalnızca sunucu yapılandırma
dosyasındaki 'AllowRoot' parametresini buna göre değiştirirseniz,
sunucuyu 'root' olarak çalıştırabilirsiniz.

Zabbix sunucusu ve [aracısı](agent) aynı makinede çalışıyorsa, sunucuyu
çalıştırmak için aracıyı çalıştırandan farklı bir kullanıcı kullanılması
önerilir. Aksi takdirde yani her ikisi de aynı kullanıcı olarak
çalıştırılırsa aracı, sunucu yapılandırma dosyasına erişebilir ve
Zabbix'teki herhangi bir Yönetici kullanıcısı, örneğin veritabanı
parolasını kolayca elde edebilir.

##### Yapılandırma dosyası

zabbix\_server'ı yapılandırmayla ilgili ayrıntılar için [yapılandırma
dosyası](/manual/appendix/config/zabbix_server) seçeneklerine bakın.

##### Başlatma betikleri

Betikler, sistemin başlatılması/kapatılması sırasında otomatik olarak
Zabbix süreçlerini başlatmak/durdurmak için kullanılır. Betikler,
misc/init.d dizini altında bulunur.

#### Desteklenen platformlar

Güvenlik gereksinimleri ve sunucu işleminin kritik doğası gereği UNIX;
gerekli performansı, hataya dayanıklılığı ve esnekliği sürekli olarak
sunabilen tek işletim sistemidir. Zabbix, piyasanın öncü sürümlerinde
çalışır.

Zabbix sunucusu aşağıdaki platformlarda test edilmiştir:

-   Linux
-   Solaris
-   AIX
-   HP-UX
-   Mac OS X
-   FreeBSD
-   OpenBSD
-   NetBSD
-   SCO Open Server
-   Tru64/OSF1

::: noteclassic
Zabbix, diğer Unix benzeri işletim sistemlerinde de
çalışabilir.
:::
