# 1 Red Hat Enterprise Linux/CentOS

#### Genel bakış

Resmi Zabbix paketleri RHEL 7, Oracle Linux 7 ve CentOS 7 için
kullanılabilir.

Bazı aracı ve proxy paketleri [RHEL
5](http://repo.zabbix.com/zabbix/3.4/rhel/5/x86_64) ve [RHEL
6](http://repo.zabbix.com/zabbix/3.4/rhel/6/x86_64/) için de
kullanılabilir.

#### Depo kurulumu

Depo yapılandırma paketini yükleyin. Bu paket yum (yazılım paket
yöneticisi) yapılandırma dosyalarını içerir.

    # rpm -ivh http://repo.zabbix.com/zabbix/3.4/rhel/7/x86_64/zabbix-release-3.4-1.el7.noarch.rpm

#### Sunucu/proxy kurulumu

Sunucuyu MySQL ile kurmak için:

    # yum install zabbix-server-mysql zabbix-web-mysql

Proxyyi MySQL ile kurmak için:

    # yum install zabbix-proxy-mysql

Komuttaki 'mysql' ifadesini PostgreSQL'i kullanıyorsanız 'pgsql' ile
veya SQLite3 kullanıyorsanız (yalnızca proxyde) 'sqlite' ile değiştirin.

\*\* Veritabanı oluşturma \*\*

Zabbix [sunucu](/manual/concepts/server) ve
[proxy](/manual/concepts/proxy) artalan süreçleri için bir veritabanı
gereklidir. Zabbix [aracısını](/manual/concepts/agent) çalıştırmak için
gerekli değildir.

::: notewarning
Zabbix sunucusu ve proxysi aynı ana bilgisayara
kurulursa, veritabanları farklı isimlerle oluşturulmalıdır!
:::

MySQL/PostgreSQL için sunulan [veritabanı oluşturma
betiklerini](/manual/appendix/install/db_scripts) kullanarak
veritabanını oluşturun.

Şimdi MySQL ile sunucu için ilk şema ve verileri içe aktarın:

    # zcat /usr/share/doc/zabbix-server-mysql-3.4.0/create.sql.gz | mysql -uzabbix -p zabbix

Yeni oluşturulan veritabanı parolanızı girmeniz istenir.

PostgreSQL ile:

    # zcat /usr/share/doc/zabbix-server-pgsql-3.4.0/create.sql.gz | psql -U <username> zabbix

Proxy için ilk şemayı içe aktarın:

    # zcat /usr/share/doc/zabbix-proxy-mysql-3.4.0/schema.sql.gz | mysql -uzabbix -p zabbix

PostgreSQL (veya SQLite) ile proxy için:

    # zcat /usr/share/doc/zabbix-proxy-pgsql-3.4.0/schema.sql.gz | psql -U <username> zabbix
    # zcat /usr/share/doc/zabbix-proxy-sqlite-3.4.0/schema.sql.gz | sqlite3 zabbix.db

Komutlara doğru Zabbix sürümünü eklediğinizden emin olun (öntanımlı
olarak `3.4.0`). Paketinizin doğru sunucu/proxy sürümünü kontrol etmek
için şunu çalıştırın:

    # rpm -q zabbix-server-mysql
    # rpm -q zabbix-proxy-mysql

Komuttaki 'mysql' ifadesini PostgreSQL'i kullanıyorsanız 'pgsql' ile
veya SQLite3 kullanıyorsanız (yalnızca proxyde) 'sqlite' ile değiştirin.

\*\* Zabbix sunucu/proxy için veritabanını yapılandırma \*\*

Oluşturulan veritabanını kullanmak için zabbix\_server.conf veya
zabbix\_proxy.conf dosyalarını düzenleyin. Örneğin:

    # vi /etc/zabbix/zabbix_server.conf
    DBHost=localhost
    DBName=zabbix
    DBUser=zabbix
    DBPassword=<parola>

DBPassword değerinde MySQL için Zabbix veritabanı parolasını; PosgreSQL
için PosgreSQL kullanıcı parolasını kullanın.

`DBHost=`'u PostgreSQL ile kullanın. `DBHost=localhost` (veya bir IP
adresi) öntanımlı ayarını tutmak isteyebilirsiniz, ancak bu
PostgreSQL'in Zabbix'e bağlanmak için bir ağ soketi kullanmasını neden
olacaktır. Yönergeler için aşağıdaki **SELinux yapılandırmasına** bakın.

\*\* Zabbix sunucu sürecini başlatma \*\*

Zabbix sunucu sürecini başlatmanın ve sistem önyüklenmesiyle birlikte
başlamasının sağlanmasının zamanı geldi:

    # systemctl start zabbix-server
    # systemctl enable zabbix-server

Zabbix proxy sürecini başlatmak için 'zabbix-server' öğesini
'zabbix-proxy' ile değiştirin.

\*\* Zabbix önyüzü için PHP yapılandırması \*\*

Zabbix önyüzünün Apache yapılandırma dosyası
/etc/httpd/conf.d/zabbix.conf dosyasında bulunmaktadır. Bazı PHP
ayarları halihazırda yapılandırılmıştır. Ancak, "date.timezone" ayarını
yorumdan çıkarmanız ve sizin için [doğru saat dilimini
ayarlamanız](http://php.net/manual/en/timezones.php) gereklidir.

    php_value max_execution_time 300
    php_value memory_limit 128M
    php_value post_max_size 16M
    php_value upload_max_filesize 2M
    php_value max_input_time 300
    php_value always_populate_raw_post_data -1
    # php_value date.timezone Europe/Riga

##### SELinux yapılandırması

SELinux durumu enforcing modda etkinse, Zabbix önyüzünün sunucuya
başarıyla bağlanmasını sağlamak için aşağıdaki komutu çalıştırmanız
gerekir:

    # setsebool -P httpd_can_connect_zabbix on

PostgreSQL ile eğer 'localhost' veya bir IP adresi `DBHost=` için
ayarlanmışsa, zabbix\_server.conf içinde, Zabbix önyüzü ve veritabanı
arasında da bağlantıya izin vermeniz gerekir:

    # setsebool -P httpd_can_network_connect_db on

Önyüz ve SELinux yapılandırması yapıldığında, Apache web sunucusunu
yeniden başlatmanız gerekir:

    # systemctl start httpd

\*\* Önyüz kurulumu \*\*

Şimdi, yeni kurulan Zabbix'e erişmenizi sağlayacak olan [önyüz yükleme
adımlarına](/manual/installation/install#installing_frontend) devam
etmeye hazırsınız.

::: noteclassic
Zabbix resmi deposu; fping, iksemel, libssh2 paketlerini de
sunmaktadır. Bu paketler
*[non-supported](http://repo.zabbix.com/non-supported/)* dizininde
bulunur.
:::

#### Aracı kurulumu

Aracıyı kurmak için şunu çalıştırın:

    # yum install zabbix-agent

Aracıyı başlatmak için şunu çalıştırın:

    # service zabbix-agent start
