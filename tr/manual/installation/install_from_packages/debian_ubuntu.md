# 2 Debian/Ubuntu

#### Genel bakış

Resmi Zabbix paketleri aşağıdakiler için mevcuttur:

-   Debian 9 (Stretch)
-   Debian 8 (Jessie)
-   Debian 7 (Wheezy)
-   Ubuntu 16.04 (Xenial Xerus) LTS
-   Ubuntu 14.04 (Trusty Tahr) LTS

#### Depo kurulumu

Depo yapılandırma paketini yükleyin. Bu pakette apt (yazılım paket
yöneticisi) yapılandırma dosyaları bulunur.

Debian **9** için şunları çalıştırın:

    # wget http://repo.zabbix.com/zabbix/3.4/debian/pool/main/z/zabbix-release/zabbix-release_3.4-1+stretch_all.deb
    # dpkg -i zabbix-release_3.4-1+stretch_all.deb
    # apt-get update

Debian 8 için 'stretch' yerine 'jessie' yazınız. Debian 7 için
komutlarda 'stretch' yerine 'wheezy' kullanın.

Ubuntu **16.04** için şunları çalıştırın:

    # wget http://repo.zabbix.com/zabbix/3.4/ubuntu/pool/main/z/zabbix-release/zabbix-release_3.4-1+xenial_all.deb
    # dpkg -i zabbix-release_3.4-1+xenial_all.deb
    # apt-get update

Ubuntu 14.04 için komutlarda 'xenial' yerine 'trusty' ifadesini
kullanın.

#### Sunucu/vekil sunucu kurulumu

Sunucuyu MySQL ile kurmak için:

    # apt-get install zabbix-server-mysql zabbix-frontend-php

Vekil sunucuyu MySQL ile kurmak için:

    # apt-get install zabbix-proxy-mysql

PostgreSQL kullanıyorsanız komuttaki 'mysql' ifadesini 'pgsql' ile veya
SQLite3 (yalnızca vekil sunucu için) kullanıyorsanız 'sqlite' ile
değiştirin.

\*\* Veritabanı oluşturma \*\*

Zabbix [sunucu](/manual/concepts/server) ve [vekil
sunucu](/manual/concepts/proxy) artalan süreci için bir veritabanı
gerekmektedir. Zabbix [aracısını](/manual/concepts/agent) çalıştırmak
için gerekmemektedir.

::: notewarning
Zabbix sunucusu ve vekil sunucusu aynı bilgisayara
kurulursa veritabanları farklı isimlerle oluşturulmalıdır!
:::

MySQL/PostgreSQL için sağlanan [veritabanı oluşturma
betiklerini](/manual/appendix/install/db_scripts) kullanarak
veritabanını oluşturun.

Şimdi MySQL kullanan sunucu için ilk şema ve verileri içe aktarın:

    # zcat /usr/share/doc/zabbix-server-mysql/create.sql.gz | mysql -uzabbix -p zabbix

Yeni oluşturulan veritabanı parolanızı girmeniz istenecektir.

PostgreSQL için:

    # zcat /usr/share/doc/zabbix-server-pgsql/create.sql.gz | psql -U <username> zabbix

Vekil sunucu için ilk şemayı içe aktarın:

    # zcat /usr/share/doc/zabbix-proxy-mysql/schema.sql.gz | mysql -uzabbix -p zabbix

PostgreSQL (veya SQLite) kullanan vekil sunucu için:

    # zcat /usr/share/doc/zabbix-proxy-pgsql/schema.sql.gz | psql -U <username> zabbix
    # zcat /usr/share/doc/zabbix-proxy-sqlite/schema.sql.gz | sqlite3 zabbix.db

\*\* Zabbix sunucusu/vekil sunucusu için veritabanı yapılandırma \*\*

Oluşturulan veritabanını kullanmak için zabbix\_server.conf veya
zabbix\_proxy.conf dosyalarını düzenleyin. Örneğin:

    # vi /etc/zabbix/zabbix_server.conf
    DBHost=localhost
    DBName=zabbix
    DBUser=zabbix
    DBPassword=<password>

DBPassword'ta MySQL için Zabbix veritabanı parolasını; PostgreSQL için
PosgreSQL kullanıcı parolasını kullanın.

PostgreSQL ile `DBHost=` kullanın. `DBHost=localhost` (veya bir IP
adresi) öntanımlı ayarını korumak isteyebilirsiniz, ancak bu,
PostgreSQL'in Zabbix'e bağlanmak için bir ağ soketi kullanmasına neden
olacaktır. RHEL/CentOS yönergeleri için [ilgili
bölüme](/manual/installation/install_from_packages/rhel_centos#selinux_configuration)
bakınız.

\*\* Zabbix sunucu sürecini başlatma \*\*

It's time to start Zabbix server process and make it start at system
boot: Şimdi Zabbix sunucu sürecini başlatma ve sistem önyüklemesinde
otomatik olarak çalıştırılmasını sağlama zamanı geldi:

    # service zabbix-server start
    # update-rc.d zabbix-server enable

Zabbix vekil sunucusu sürecini başlatmak için 'zabbix-server' öğesini
'zabbix-proxy' ile değiştirin.

\*\* Zabbix önyüzü için PHP yapılandırması \*\*

Zabbix önyüzünün Apache yapılandırma dosyası
/etc/apache2/conf-enabled/zabbix.conf yolunda bulunur. Bazı PHP ayarları
halihazırda yapılandırılmıştır. Bununla birlikte, "date.timezone"
ayarının yorumdan çıkarılması ve [doğru saat diliminin
ayarlanması](http://php.net/manual/en/timezones.php) gerekmektedir.

    php_value max_execution_time 300
    php_value memory_limit 128M
    php_value post_max_size 16M
    php_value upload_max_filesize 2M
    php_value max_input_time 300
    php_value always_populate_raw_post_data -1
    # php_value date.timezone Europe/Riga

##### SELinux yapılandırması

RHEL/CentOS için [ilgili
bölüme](/manual/installation/install_from_packages/rhel_centos#selinux_configuration)
bakınız.

Önyüz ve SELinux yapılandırması bittiğinde, Apache web sunucusunu
yeniden başlatmanız gerekir:

    # service apache2 restart

\*\* Önyüz kurulumu \*\*

Şimdi, yeni kurulmuş olan Zabbix'e erişmenize izin verecek olan [önyüz
yükleme adımmları](/manual/installation/install#installing_frontend) ile
devam etmeye hazırsınız.

#### Aracı kurulumu

Aracıyı kurmak için aşağıdakini çalıştırın:

    # apt-get install zabbix-agent

Aracıyı çalıştırmak için şu komutu çalıştırın:

    # service zabbix-agent start
