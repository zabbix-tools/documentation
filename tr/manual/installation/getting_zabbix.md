# - \#1 Zabbix'i alma

#### Genel bakış

Zabbix'i almak için dört yol vardır:

-   [Dağıtım
    paketlerinden](install_from_packages#From_distribution_packages)
    kurun
-   En son kaynak kod arşivini indirin ve [kendiniz
    derleyin](install#Installation_from_sources).
-   [Konteynırlardan](containers) yükleyin
-   [Sanal cihazı](/manual/appliance) indirin

En son kaynak kodları veya sanal cihazı indirmek için, en son sürümlere
doğrudan bağlantılar sunulan [Zabbix indirme
sayfasına](http://www.zabbix.com/download.php) gidin. Eski sürümleri
indirmek için, kararlı sürüm indirmeleri altındaki bağlantılara bakın.
