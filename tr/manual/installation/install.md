# 3 Kaynaklardan kurulum

Kaynaklardan derleyerek Zabbix'in en son sürümünü edinebilirsiniz.

Zabbix'i kaynaklardan kurmak için adım adım bir eğitim burada
verilmektedir.

#### - Zabbix artalan süreçlerini kurma

##### 1 Kaynak arşivini indirme

[Zabbix indirme sayfasına](http://www.zabbix.com/download.php) gidin ve
kaynak arşivini indirin. İndirildikten sonra, aşağıdakileri yaparak
kaynakları açın:

    $ tar -zxvf zabbix-5.2.0.tar.gz

::: notetip
Komutta doğru Zabbix sürümünü girin. İndirilen arşivin
adıyla eşleşmelidir.
:::

##### 2 Kullanıcı hesabı oluşturma

Zabbix artalan süreçlerinin tümü için ayrıcalıksız bir kullanıcı
gereklidir. Bir Zabbix artalan süreci, ayrıcalıksız bir kullanıcı
hesabından başlatılırsa o kullanıcı olarak çalışacaktır.

Bununla birlikte, bir artalan süreci bir 'root' hesabından başlatılırsa,
mevcut olması gereken bir 'zabbix' kullanıcı hesabına geçer. Linux
sistemlerinde (kendi grubu olan "zabbix" içinde) böyle bir kullanıcı
hesabı oluşturmak için şunu çalıştırın:

    groupadd zabbix
    useradd -g zabbix zabbix

Zabbix önyüzü kurulumu için ayrı bir kullanıcı hesabı gerekli değildir.

Zabbix [sunucusu](/manual/concepts/server) ve
[aracısı](/manual/concepts/agent) aynı makinede çalışıyorsa, sunucuyu
çalıştırmak için aracıyı çalıştırmak için kullanılandan farklı bir
kullanıcı önerilir. Aksi takdirde yani her ikisi de aynı kullanıcı
olarak çalıştırılırsa aracı, sunucu yapılandırma dosyasına erişebilir ve
Zabbix'teki herhangi bir Yönetici seviyesindeki kullanıcı, örneğin
veritabanı parolasını kolayca alabilir.

::: noteimportant
Zabbix'i `root`, `bin` veya özel haklara sahip
herhangi bir hesapta çalıştırmak bir güvenlik riskidir.
:::

##### 3 Zabbix veritabanı oluşturma

Zabbix [sunucusu](/manual/concepts/server),
[proxy](/manual/concepts/proxy) artalan süreçleri ve ayrıca Zabbix
önyüzü için bir veritabanı gerekiyor. Zabbix
[aracısını](/manual/concepts/agent) çalıştırmak için gerekli değildir.

Veritabanı şeması oluşturmak ve veri kümesini eklemek için SQL
[betikleri sağlanmaktadır](/manual/appendix/install/db_scripts). Zabbix
proxy veritabanı yalnızca şemaya ihtiyaç duyarken Zabbix sunucu
veritabanı şemanın üstündeki veri kümesini de gerektirir.

Bir Zabbix veritabanı oluşturduktan sonra, Zabbix'i derlemek için
aşağıdaki adımlara geçin.

##### 4 Kaynakları yapılandırma

Kaynakları bir Zabbix sunucusu veya proxy için yapılandırırken,
kullanılacak veritabanı türünü belirtmeniz gerekir. Bir sunucu veya
proxy süreciyle aynı anda yalnızca bir veritabanı türü derlenebilir.

Desteklenen tüm yapılandırma seçeneklerini görmek için, çıkarılan Zabbix
kaynak dizininin içinden şunu çalıştırın:

    ./configure --help

Bir Zabbix sunucusu ve aracısı için kaynakları yapılandırırken
aşağıdakine benzer bir işlem yapabilirsiniz:

    ./configure --enable-server --enable-agent --with-mysql --enable-ipv6 --with-net-snmp --with-libcurl --with-libxml2

::: noteclassic
Zabbix 3.0.0'dan beri desteklenen, SMTP kimlik doğrulaması
için cURL 7.20.0 veya daha üstü ve --with-libcurl yapılandırma seçeneği
gereklidir.\
--with-libcurl ve --with-libxml2 yapılandırma seçenekleri, Zabbix
2.2.0'dan beri desteklenen sanal makine izleme için
gereklidir.
:::

Bir Zabbix sunucusunun kaynaklarını (PostgreSQL vb. ile) yapılandırmak
için aşağıdakileri çalıştırabilirsiniz:

    ./configure --enable-server --with-postgresql --with-net-snmp

Bir Zabbix proxysinin kaynaklarını (SQLite vb. ile) yapılandırmak için
aşağıdakileri çalıştırabilirsiniz:

    ./configure --prefix=/usr --enable-proxy --with-net-snmp --with-sqlite3 --with-ssh2

Bir Zabbix aracısının kaynaklarını yapılandırmak için aşağıdakileri
çalıştırabilirsiniz:

    ./configure --enable-agent

Kütüphaneleri statik olarak bağlamak için --enable-static bayrağını
kullanabilirsiniz. Derlenmiş ikili dosyaları farklı sunucular arasında
dağıtmayı planlıyorsanız, bu ikili dosyaların gerekli arşiv olmadan
çalışması için bu bayrağı kullanmanız gerekir. --enable-static,
[Solaris'te
çalışmaz.](http://blogs.sun.com/rie/entry/static_linking_where_did_it).

::: noteimportant
 Sunucu inşa edilirken --enable-static seçeneğinin
kullanılması önerilmez.// //

Sunucuyu statik olarak inşa etmek için, ihtiyaç duyulan her harici
kütüphanenin statik bir sürümüne sahip olmalısınız. Yapılandırma
betiğinde bunun için sıkı bir kontrol yoktur. 
:::

::: noteclassic
Komut satırı araçları zabbix\_get ve zabbix\_sender,
--enable-agent seçeneği kullanıldığında derlenir.
:::

::: noteclassic
 Öntanımlı konumda bulunmayan bir MySQL istemci
kütüphanesini seçmek için MySQL yapılandırma dosyasına isteğe bağlı bir
yol ekleyin: --with-mysql=/<dosya\_yolu>/mysql\_config

Aynı sistemde MySQL'in yanında MySQL veya MariaDB'nin farklı birkaç
sürümü yüklü olduğunda yararlıdır. 
:::

::: noteclassic
CLI API'sinin konumunu belirtmek için --with-ibm-db2
bayrağını kullanın.\
OCI API'sinin konumunu belirtmek için --with-oracle bayrağını
kullanın.
:::

For encryption support see [Compiling Zabbix with encryption
support](/manual/encryption#compiling_zabbix_with_encryption_support).
Şifreleme desteği için [Zabbix'i şifreleme desteğiyle derleme bölümüne
bakın](/manual/encryption#compiling_zabbix_with_encryption_support).

##### 5 Her şeyi inşa edip ve yükleme

::: noteclassic
git'den yükleme yapılıyorsa, önce aşağıdakini çalıştırmanız
gerekir:

`$ make dbschema` 
:::

    make install

Bu adım, yeterli izinlere sahip bir kullanıcı (genellikle 'root' olarak
veya `sudo` kullanılarak) çalıştırılmalıdır.

`make install`'u çalıştırmak öntanımlı olarak artalan süreci
çalıştırılabilir dosyalarını (zabbix\_server, zabbix\_agentd,
zabbix\_proxy) /usr/local/sbin içine ve istemci ikili dosyalarını
(zabbix\_get, zabbix\_sender) /usr/local/bin dizinine yükleyecektir.

::: noteclassic
/usr/local'den farklı bir konum belirtmek için kaynak
yapılandırma işleminin önceki adımında --prefix anahtarını kullanın,
örneğin --prefix=/home/zabbix şeklinde. Bu durumda artalan süreci ikili
dosyaları <prefix>/sbin altına kurulurken, araçlar
<prefix>/bin altına kurulacaktır. Man yardım sayfaları da
<prefix>/share altına yüklenecektir.
:::

##### 6 Yapılandırma dosyalarını inceleme ve düzenleme

-   **/usr/local/etc/zabbix\_agentd.conf** Zabbix aracısının
    yapılandırma dosyasını düzenleme

Bu dosyayı, zabbix\_agentd yüklü olan her ana bilgisayar için
yapılandırmanız gerekir.

Dosyada Zabbix sunucu **IP adresini** belirtmelisiniz. Diğer ana
bilgisayarlardan gelen bağlantılar reddedilecektir.

-   **/usr/local/etc/zabbix\_server.conf** Zabbix sunucu yapılandırma
    dosyasını düzenleme

Veritabanı adını, kullanıcıyı ve parolayı (eğer varsa) belirtmelisiniz.

Geri kalan parametreler, küçük bir kurulumunuz varsa (en çok on ana
bilgisayara kadar izleme yapıyorsanız) öntanımlı değerleriyle size
uyacaktır. Zabbix sunucusunun (veya proxysinin) performansını en üst
düzeye çıkarmak isterseniz öntanımlı parametreleri değiştirmeniz
gerekir. Daha çok ayrıntı için [performans
ayarlama](/manual/appendix/performance_tuning) bölümüne bakın.

-   Zabbix proxyyi kurduysanız, **/usr/local/etc/zabbix\_proxy.conf**
    proxy yapılandırma dosyasını düzenleme

Veritabanı adı, kullanıcı ve parola (eğer varsa) bilgisinin yanı sıra
Sunucu IP adresini ve proxy ana makine adını (sunucu tarafından
bilinmesi gerekir) belirtmelisiniz.

::: noteclassic
SQLite ile veritabanı dosyasının tam yolu belirtilmelidir;
DB kullanıcısı ve parolası gerekli değildir.
:::

##### 7 Artalan süreçlerini çalıştırma

Sunucu tarafında zabbix\_server'ı çalıştırın.

    shell> zabbix_server

::: noteclassic
Sisteminizin 36 MB (veya biraz daha çok) paylaşımlı belleğin
tahsisine izin verdiğinden emin olun, aksi takdirde sunucu
başlatılamayabilir ve sunucu günlüğü dosyasında "<önbellek türü>
için paylaşımlı bellek tahsis edilemiyor" mesajı görürsünüz. Bu,
FreeBSD, Solaris 8'de olabilir.\
Paylaşımlı belleğin nasıl yapılacağını öğrenmek için bu sayfanın
altındaki ["Ayrıca bkz"](#see_also) bölümüne bakın.
:::

İzlenen tüm ana makinelerde zabbix\_agentd'yi çalıştırın.

    shell> zabbix_agentd

::: noteclassic
Sisteminizin 2MB paylaşımlı belleğin tahsisine izin
verdiğinden emin olun, aksi takdirde aracı başlatılamayabilir ve aracı
kayıt dosyasında "Toplayıcı için paylaşımlı bellek tahsis edilemiyor"
ifadesini görürsünüz. Bu, Solaris 8'de olabilir.
:::

Zabbix proxyyi kurduysanız zabbix\_proxy'yi çalıştırın.

    shell> zabbix_proxy

#### - Zabbix web arayüzünü kurma

##### PHP dosyalarını kopyalama

Zabbix önyüzü PHP ile yazılmıştır, dolayısıyla PHP destekli bir web
sunucusu çalıştırmak gerekir. Kurulum, yalnızca PHP dosyalarını
frontends/php'den web sunucusunun HTML belgeleri dizinine kopyalayarak
yapılır.

Apache web sunucuları için HTML belge dizinlerinin yaygın konumları
arasında aşağıdakiler yer alır:

-   /usr/local/apache2/htdocs (Apache'yi kaynaktan yüklerken varsayılan
    dizin)
-   /srv/www/htdocs (OpenSUSE, SLES)
-   /var/www/html (Fedora, RHEL, CentOS)
-   /var/www (Debian, Ubuntu)

HTML kök dizini yerine bir alt dizin kullanılması önerilir. Bir alt
dizin oluşturmak ve Zabbix önyüz dosyalarını içine kopyalamak için o
anki dizini değiştirerek aşağıdaki komutları uygulayın:

    mkdir <htdocs>/zabbix
    cd frontends/php
    cp -a . <htdocs>/zabbix

git'den yükleme yapıyor ve İngilizce'den başka herhangi bir dili
kullanmayı planlıyorsanız çeviri dosyalarını oluşturmanız gerekir. Bunu
yapmak için aşağıdaki komutu çalıştırın:

    locale/make_mo.sh

gettext paketinden `msgfmt` aracı gerekir.

::: noteclassic
Ayrıca, İngilizce'den başka herhangi bir dili kullanmak
için, o dilin yerel dosyaları web sunucusuna yüklenmelidir. Eğer
gerekirse nasıl kurulacağını öğrenmek için "Kullanıcı profili"
sayfasındaki ["Ayrıca
bkz."](/manual/web_interface/user_profile#see_also) bölümüne
bakın.
:::

##### Önyüzü yükleme

##### Adım 1

Tarayıcınızda Zabbix URL'sini açın:
http://<sunucu\_ip'si\_veya\_adı>/zabbix

Önyüz yükleme sihirbazının ilk ekranını görmelisiniz.

![](../../../assets/en/manual/installation/install_1.png){width="550"}

##### Adım 2

Tüm yazılım ön koşullarının karşılandığından emin olun.

![](../../../assets/en/manual/installation/install_2.png){width="550"}

|Önkoşul|Minimum değer|Açıklama|
|---------|--------------|----------|
|*PHP version*|5.4.0|<|
|*PHP memory\_limit seçeneği*|128MB|php.ini'de:<br>memory\_limit = 128M|
|*PHP post\_max\_size seçeneği*|16MB|php.ini'de:<br>post\_max\_size = 16M|
|*PHP upload\_max\_filesize seçeneği*|2MB|php.ini'de:<br>upload\_max\_filesize = 2M|
|*PHP max\_execution\_time seçeneği*|300 saniye (0 ve -1 değerlerine izin verilir)|php.ini'de:<br>max\_execution\_time = 300|
|*PHP max\_input\_time seçeneği*|300 saniye (0 ve -1 değerlerine izin verilir)|php.ini'de:<br>max\_input\_time = 300|
|*PHP session.auto\_start seçeneği*|devre dışı olmalıdır|php.ini'de:<br>session.auto\_start = 0|
|*Database support*|MySQL, Oracle, PostgreSQL, IBM DB2'den biri|Aşağıdaki modüllerden birinin yüklü olması gerekir:<br>mysql, oci8, pgsql, ibm\_db2|
|*bcmath*|<|php-bcmath|
|*mbstring*|<|php-mbstring|
|*PHP mbstring.func\_overload seçeneği*|devre dışı olmalıdır|php.ini'de:<br>mbstring.func\_overload = 0|
|*PHP always\_populate\_raw\_post\_data seçeneği*|devre dışı olmalıdır|Yalnızca PHP 5.6.0 veya daha yeni sürümleri için gereklidir.<br>php.ini'de:<br>always\_populate\_raw\_post\_data = -1|
|*sockets*|<|php-net-socket. Kullanıcı betik desteği için gereklidir.|
|*gd*|2.0 or higher|php-gd. PHP GD uzantısı PNG görüntülerini (*--with-png-dir*), JPEG görüntülerini (*--with-jpeg-dir*) ve FreeType 2'yi (*--with-freetype-dir*) desteklemelidir.|
|*libxml*|2.6.15|php-xml veya php5-dom|
|*xmlwriter*|<|php-xmlwriter|
|*xmlreader*|<|php-xmlreader|
|*ctype*|<|php-ctype|
|*session*|<|php-session|
|*gettext*|<|php-gettext<br>Zabbix 2.2.1'den itibaren PHP gettext uzantısı, Zabbix'i kurmak için zorunlu bir gereklilik değildir. Gettext kurulu değilse, önyüz her zamanki gibi çalışacaktır, ancak çeviriler mevcut olmayacaktır.|

İsteğe bağlı ön koşullar da listede mevcut olabilir. Başarısız bir
isteğe bağlı önkoşul turuncu renkte görüntülenir ve bir *Uyarı*
durumundadır. Başarısız bir isteğe bağlı önkoşul ile kurulum devam
edebilir.

::: noteimportant
Apache kullanıcısını veya kullanıcı grubunu
değiştirmeye ihtiyaç duyulursa oturum dizini için izinlerin doğrulanması
gerekir. Aksi takdirde Zabbix kurulumu devam edemeyebilir.
:::

##### Adım 3

Veritabanına bağlanmayla ilgili ayrıntıları girin. Zabbix veritabanı
halihazırda oluşturulmuş olmalıdır.

![](../../../assets/en/manual/installation/install_3.png){width="550"}

##### Adım 4

Zabbix sunucu ayrıntılarını girin.

![](../../../assets/en/manual/installation/install_4.png){width="550"}

##### Adım 5

Ayar özetlerini inceleyin.

![](../../../assets/en/manual/installation/install_5.png){width="550"}

##### Adım 6

Yapılandırma dosyasını indirin ve Zabbix PHP dosyalarını kopyaladığınız
web sunucusu HTML belgeleri alt dizininde conf/ altına yerleştirin.

![](../../../assets/en/manual/installation/install_6.png){width="550"}

![](../../../assets/en/manual/installation/saving_zabbix_conf.png){width="350"}

::: notetip
webserver kullanıcısının conf/ dizinine yazma erişimine
sahip olmasının sağlanmasıyla yapılandırma dosyası otomatik olarak
kaydedilir ve bir sonraki adıma hemen geçilebilir.
:::

##### Adım 7

Yüklemeyi bitirin.

![](../../../assets/en/manual/installation/install_7.png){width="550"}

##### Adım 8

Zabbix önyüzü hazır! Öntanımlı kullanıcı adı **Admin**, parola
**zabbix**'tir.

![](../../../assets/en/manual/quickstart/login.png){width="350"}

[Zabbix'e başlangıç](/manual/quickstart/login)'a geçin.

### Ayrıca bakınız

1.  [Zabbix artalan süreçleri için paylaşımlı hafıza nasıl
    ayarlanır?](http://www.zabbix.org/wiki/How_to/configure_shared_memory)
