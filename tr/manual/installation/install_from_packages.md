# 4 Paketlerden kurulum

#### Dağıtım paketlerinden

Birkaç popüler işletim sistemi dağıtımı Zabbix paketleri sağlar. Bu
paketleri, Zabbix'i kurmak için kullanabilirsiniz.

::: noteclassic
İşletim sistemi dağıtımları depolarında Zabbix'in en son
sürümüne sahip olmayabilir.
:::

#### Zabbix resmi depolarından

Zabbix SIA, aşağıdakiler için resmi RPM ve DEB paketleri sağlar:

-   [Red Hat Enterprise
    Linux/CentOS](/manual/installation/install_from_packages/rhel_centos)
-   [Debian/Ubuntu](/manual/installation/install_from_packages/debian_ubuntu)

Paket dosyaları [repo.zabbix.com](http://repo.zabbix.com/) adresinden
edinilebilir. Sunucuda yum ve apt depoları da mevcuttur. Zabbix'i
paketlerden kurmak için adım adım bir eğitim, buradaki alt sayfalarda
verilmektedir.
