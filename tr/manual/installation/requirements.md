# - \#2 Gereksinimler

#### Donanım

##### Bellek

Zabbix hem fiziksel hem de disk belleği gerektirir. 128 MB fiziksel
bellek ve 256 MB boş disk alanı iyi bir başlangıç noktası olabilir.
Bununla birlikte, gerekli disk belleği miktarı, izlenen ana
bilgisayarların ve parametrelerin sayısına bağlıdır. İzlenen
parametrelerin uzun bir geçmişini tutmayı planlıyorsanız, geçmişi
veritabanında depolamaya yeterli alana sahip olmak için en azından
birkaç gigabayt düşünmelisiniz.

Her Zabbix artalan süreci bir veritabanı sunucusuna birkaç bağlantı
gerektirir. Bağlantı için ayrılan bellek miktarı, veritabanı alt yapısı
yapılandırmasına bağlıdır.

::: noteclassic
Ne kadar çok fiziksel belleğe sahip olursanız, veritabanı
(ve dolayısıyla Zabbix) o kadar hızlı çalışır!
:::

##### İşlemci

Zabbix ve özellikle Zabbix veritabanı, izlenen parametre sayısına ve
seçilen veritabanı motoruna bağlı olarak önemli miktarda CPU kaynağı
gerektirebilir.

##### Diğer donanımlar

Zabbix'te SMS bildirim desteğini kullanmak için bir seri iletişim portu
ve bir seri GSM modem gereklidir. USB'den seriye dönüştürücü de
çalışacaktır.

##### Donanım yapılandırmasına örnekler

Tabloda çeşitli donanım yapılandırma örnekleri verilmektedir:

|İsim|Platform|CPU/Bellek|Veritabanı|İzlenen ana makineler|
|-----|--------|----------|-----------|----------------------|
|*Küçük*|CentOS|Sanal Makine|MySQL InnoDB|100|
|*Orta*|CentOS|2 CPU çekirdeği/2GB|MySQL InnoDB|500|
|*Büyük*|RedHat Enterprise Linux|4 CPU çekirdeği/8GB|RAID10 MySQL InnoDB veya PostgreSQL|>1000|
|*Çok büyük*|RedHat Enterprise Linux|8 CPU çekirdeği/16GB|Fast RAID10 MySQL InnoDB veya PostgreSQL|>10000|

::: noteclassic
Gerçek yapılandırma aktif öğe sayısına ve yenileme hızına
çok fazla bağlıdır. Büyük kurulumlar için veritabanını ayrı bir makinede
çalıştırmanız şiddetle tavsiye edilir.
:::

#### Desteklenen platformlar

Güvenlik gereksinimleri ve sunucu işleminin kritik doğası gereği UNIX;
gerekli performansı, hataya dayanıklılığı ve esnekliği sürekli olarak
sunabilen tek işletim sistemidir. Zabbix, piyasanın öncü sürümlerinde
çalışır.

Zabbix aşağıdaki platformlarda test edilmiştir:

-   Linux
-   IBM AIX
-   FreeBSD
-   NetBSD
-   OpenBSD
-   HP-UX
-   Mac OS X
-   Solaris
-   Windows: 2000'den sonraki tüm masaüstü ve sunucu sürümleri (yalnızca
    Zabbix aracısı)

::: noteclassic
Zabbix, diğer Unix benzeri işletim sistemlerinde de
çalışabilir.
:::

::: noteimportant
Zabbix, eğer şifreleme desteğiyle derlenmişse
çekirdek dökümlerini devre dışı bırakır ve sistem çekirdek dökümlerini
devre dışı bırakmaya izin vermiyorsa başlatılmaz.
:::

#### Yazılım

Zabbix modern bir Apache web sunucusu, önde gelen veritabanı motorları
ve PHP betik dili etrafında inşa edilmiştir.

##### Veritabanı Yönetim sistemi

|Yazılım|Sürüm|Yorumlar|
|---------|-------|--------|
|*MySQL*|5.0.3 veya üstü|MySQL, Zabbix arka uç veritabanı olarak kullanılıyorsa gereklidir. InnoDB motoru gereklidir.|
|*Oracle*|10g veya üstü|Oracle, Zabbix arka uç veritabanı olarak kullanılıyorsa gereklidir.|
|*PostgreSQL*|8.1 veya üstü|PostgreSQL'in Zabbix arka uç veritabanı olarak kullanılması durumunda gereklidir.<br>En azından, [daha iyi VACUUM performansı sunan](http://www.postgresql.org/docs/8.3/static/release-8-3.html) PostgreSQL 8.3 kullanmanız önerilir.|
|*IBM DB2*|9.7 veya üstü|IBM DB2, Zabbix arka uç veritabanı olarak kullanılıyorsa gereklidir.|
|*SQLite*|3.3.5 veya üstü|SQLite yalnızca Zabbix proxyleriyle desteklenir. SQLite Zabbix proxy veritabanı olarak kullanılıyorsa gereklidir.|

::: noteimportant
IBM DB2 desteği deneyseldir!
:::

##### Önyüz

Zabbix önyüzünü çalıştırmak için aşağıdaki yazılımlar gereklidir:

|Yazılım|Sürüm|Yorumlar|
|---------|-------|--------|
|*Apache*|1.3.12 veya üstü|<|
|*PHP*|5.4.0 veya üstü|<|
|PHP eklentileri:|<|<|
|*gd*|2.0 veya üstü|PHP GD uzantısı PNG görüntülerini (*--with-png-dir*), JPEG görüntülerini (*--with-jpeg-dir*) ve FreeType 2'yi (*--with-freetype-dir*) desteklemelidir.|
|*bcmath*|<|php-bcmath (*--enable-bcmath*)|
|*ctype*|<|php-ctype (*--enable-ctype*)|
|*libXML*|2.6.15 veya üstü|php-xml veya php5-dom, eğer dağıtıcı tarafından ayrı bir pakette sunulursa.|
|*xmlreader*|<|php-xmlreader, eğer dağıtıcı tarafından ayrı bir pakette sunulursa.|
|*xmlwriter*|<|php-xmlwriter, eğer dağıtıcı tarafından ayrı bir pakette sunulursa.|
|*session*|<|php-session, eğer dağıtıcı tarafından ayrı bir pakette sunulursa.|
|*sockets*|<|php-net-socket (*--enable-sockets*). Kullanıcı betik desteği için gereklidir.|
|*mbstring*|<|php-mbstring (*--enable-mbstring*)|
|*gettext*|<|php-gettext (*--with-gettext*). Çevirilerin çalışması için gereklidir.|
|*ldap*|<|php-ldap. Yalnızca önyüzde LDAP kimlik doğrulaması kullanılıyorsa gereklidir.|
|*ibm\_db2*|<|IBM DB2, Zabbix arka uç veritabanı olarak kullanılıyorsa gereklidir.|
|*mysqli*|<|MySQL, Zabbix arka uç veritabanı olarak kullanılıyorsa gereklidir.|
|*oci8*|<|Oracle, Zabbix arka uç veritabanı olarak kullanılıyorsa gereklidir.|
|*pgsql*|<|PostgreSQL, Zabbix arka uç veritabanı olarak kullanılıyorsa gereklidir.|

::: noteclassic
Zabbix; Apache, MySQL, Oracle ve PostgreSQL'in önceki
sürümleri üzerinde de çalışabilir.
:::

::: noteimportant
Öntanımlı DejaVu'dan farklı yazı tipleri için, PHP
fonksiyonu
[imagerotate](http://php.net/manual/en/function.imagerotate.php)
gerekebilir. Eksikse, bu yazı tipleri İzleme → Genel Bakış başlığında
(header) ve diğer konumlara yanlış işlenebilir. Bu fonksiyon yalnızca,
PHP, kendisiyle birlikte gelen GD ile derlenmişse kullanılabilir; bu
Debian ve diğer dağıtımlar için söz konusu değildir.
:::

##### İstemci tarafında WEB tarayıcısı

Çerezler ve Java Betikleri etkinleştirilmelidir.\
Google Chrome, Mozilla Firefox, Microsoft Internet Explorer ve Opera'nın
son sürümleri desteklenmektedir. Diğer tarayıcılar (Apple Safari,
Konqueror) da Zabbix'te çalışabilir.

##### Sunucu

|Gereksinim|Açıklama|
|----------|----------|
|*OpenIPMI*|IPMI desteği için gereklidir.|
|*libevent*|IPMI izlemesi için gereklidir. Sürüm 1.4 veya daha üstü.|
|*libssh2*|SSH desteği için gereklidir. Sürüm 1.0 veya üstü.|
|*fping*|[ICMP ping öğeleri](/manual/config/items/itemtypes/simple_checks#icmp_pings) için gereklidir.|
|*libcurl*|RWeb izleme, VMware izleme ve SMTP kimlik doğrulaması için gereklidir. SMTP kimlik doğrulaması için, sürüm 7.20.0 veya üstü gereklidir.|
|*libiksemel*|Jabber desteği için gereklidir.|
|*libxml2*|VMware izleme için gereklidir.|
|*net-snmp*|SNMP desteği için gereklidir.|
|*libpcre3*|PCRE düzenli ifade desteği için [PCRE](https://en.wikipedia.org/wiki/Perl_Compatible_Regular_Expressions) kütüphanesi gereklidir.<br>Adlandırma GNU/Linux dağıtımına bağlı olarak farklılık gösterebilir. PCRE2 kitaplığının kullanılmadığını unutmayın.|

##### Java ağ geçidi

Zabbix'i kaynak kod deposundan veya bir arşivden edindiyseniz, gerekli
bağımlılıklar zaten kaynak ağacına dahil edilmiştir.

Zabbix'i dağıtım paketinizden edindiyseniz paketleme sistemi tarafından
zaten gerekli bağımlılıklar sağlanmış demektir.

Yukarıdaki her iki durumda da yazılım kullanıma hazırdır ve ek bir
indirme gerekmemektedir.

Bununla birlikte, bu bağımlılıkların kendi sürümlerinizi sunmak
isterseniz (örneğin, bir Linux dağıtımı için bir paket hazırlıyorsanız),
Java ağ geçidinin birlikte çalıştığı kütüphane sürümlerinin listesi
aşağıda verilmiştir. Zabbix, bu kitaplıkların diğer sürümleriyle de
çalışabilir.

Aşağıdaki tabloda, şu andaki orijinal kodda Java ağ geçidi ile
paketlenmiş JAR dosyaları listelenmiştir:

|Kütüphane|Lisans|İnternet sitesi|Yorumlar|
|-----------|------|----------------|--------|
|*logback-core-0.9.27.jar*|EPL 1.0, LGPL 2.1|<http://logback.qos.ch/>|0.9.27, 1.0.13 ve 1.1.1 ile test edilmiştir.|
|*logback-classic-0.9.27.jar*|EPL 1.0, LGPL 2.1|<http://logback.qos.ch/>|0.9.27, 1.0.13 ve 1.1.1 ile test edilmiştir.|
|*slf4j-api-1.6.1.jar*|MIT Lisansı|<http://www.slf4j.org/>|1.6.1, 1.6.6, ve 1.7.6 ile test edilmiştir.|
|*android-json-4.3\_r3.1.jar*|Apache Lisansı 2.0|<https://android.googlesource.com/platform/libcore/+/master/json>|2.3.3\_r1.1 ve 4.3\_r3.1 ile test edilmiştir. JAR dosyası oluşturmayla ilgili yönergeler için src/zabbix\_java/lib/README'ye bakınız.|

Java ağ geçidi, Java 1.6 ve üstüyle derlenir ve çalışır. Diğerleri için
ağ geçidinin önceden derlenmiş bir sürümünü sağlayanların derleme için
Java 1.6 kullanması önerilir; böylece Java'nın en son sürümlerine kadar
tüm sürümlerinde çalışır.

#### Veritabanı boyutu

Zabbix yapılandırma verileri sabit bir disk alanı miktarı gerektirir ve
bu çok fazla artmaz.

Zabbix veritabanı boyutu, esas olarak, depolanan geçmiş verilerin
miktarını tanımlayan şu değişkenlere bağlıdır:

-   Saniyede işlenen değerlerin sayısı

Bu, Zabbix sunucusunun her saniyede aldığı yeni değerlerin ortalama
sayısıdır. Örneğin, 60 saniye yenileme hızıyla izleme için 3000 öğeye
sahipsek saniyedeki değerlerin sayısı 3000/60 = **50** olarak
hesaplanır.

Bu, her saniyede 50 yeni değerin Zabbix veritabanına eklendiği anlamına
geliyor.

-   Geçmiş için temizlik ayarları

Zabbix değerleri normalde birkaç hafta veya aylarca olmak üzere belli
bir süre tutar. Her yeni değer, veri ve indeks için belirli bir disk
alanı gerektirir.

Bu nedenle, 30 günlük geçmiş tutarsak ve saniyede 50 değer alırsak
toplam değer sayısı (**30**\*24\*3600)\* **50** = 129.600.000 ve bu da
yaklaşık 130M değerdir.

Kullanılan veritabanı motoruna, alınan değerlerin türüne (kayan noktalı
sayılar, tam sayılar, stringler, kayıt dosyaları vb.) bağlı olarak, tek
bir değeri tutmak için disk alanı 40 bayttan yüzlerce bayta kadar
değişebilir. Normal olarak sayısal öğeler için değer başına yaklaşık 90
bayttır. Bizim durumumuzda, 130M değerlerin 130M \* 90 bayt = **10.9GB**
disk alanı gerektirdiği anlamına geliyor.

::: noteclassic
Metin/kayıt öğesi değerlerinin boyutu tam olarak tahmin
edilemez, ancak değer başına yaklaşık 500 bayt
bekleyebilirsiniz.
:::

-   Eğilimler için temizlik ayarı

Zabbix, liste **eğilimlerindeki** her bir öğe için 1 saatlik
max/min/avg/count (maksimum/minimum/ortalama/sayı) değer setini saklar.
Veriler, eğilimler ve uzun dönemli çizimler için kullanılır. Bir saatlik
süre özelleştirilemez.

Zabbix veritabanı, veritabanı türüne bağlı olarak her toplamda yaklaşık
90 bayt gerektirir. Varsayalım trend verilerini 5 yıl boyunca muhafaza
etmek istiyoruz. 3000 öğe için değerler yılda 3000\*24\*365\* **90** =
**2.2GB** ve 5 yılda **11GB** gerektirir.

-   Olaylar için temizlik ayarları

Her Zabbix olayı yaklaşık 170 bayt disk alanı gerektirir. Günlük olarak
Zabbix tarafından üretilen olay sayısını tahmin etmek zor. En kötü
senaryoda Zabbix'in saniyede bir olay ürettiğini varsayabiliriz.

Bu, 3 yıllık etkinlikleri tutmak istiyorsak, bunun için
**3**\*365\*24\*3600\* **170** = **15GB** gerekeceği anlamına gelir.

Tabloda, Zabbix sisteminde gerekli disk alanını hesaplamak için
kullanılabilecek formüller bulunur:

|Parametre|Gerekli disk alanı formülü (bayt cinsinden)|
|---------|----------------------------------------------|
|*Zabbix yapılandırması*|Sabit boyut. Normal olarak 10MB veya daha az.|
|*Geçmiş*|Gün \* (öğe / yenileme oranı) \* 24 \* 3600 \* bayt<br>items : öğe: öğe sayısı<br>gün: geçmişi tutulacak gün sayısı<br>yenileme hızı: öğelerin ortalama yenileme hızı<br>bayt: tek değeri tutmak için gereken bayt sayısı, veritabanı motoruna bağlı, normalde \~90 bayt.|
|*Eğilimler*|gün \* (öğe / 3600) \* 24 \* 3600 \* bayt<br>öğe: öğe sayısı<br>gün: geçmişi tutulacak gün sayısı<br>bayt: tek bir eğilimi tutmak için gerekli bayt sayısı, veritabanı motoruna bağlı, normalde \~90 bayt.|
|*Olaylar*|gün \* olay \* 24 \* 3600 \* bayt<br>olay: saniyedeki olay sayısı. En kötü senaryoda saniyede bir (1) olay.<br>gün: geçmişi tutulacak gün sayısı<br>Bayt: tek bir olay tutmak için gerekli bayt sayısı, veritabanı motoru bağlı, normalde \~170 bayt.|

::: noteclassic
Sayısal öğeler için \~90 bayt, olaylar için \~170 bayt gibi
ortalama değerler, gerçek zamanlı istatistiklerden bir MySQL arka uç
veritabanı kullanılarak toplanmıştır.
:::

Yani, toplam gerekli disk alanı aşağıdaki gibi hesaplanabilir:\
**Yapılandırma + Geçmiş + Eğilimler + Olaylar**\
Disk alanı Zabbix kurulumundan hemen sonra kullanılmayacaktır.
Veritabanı boyutu büyüyecek, bir noktada da temizlik ayarlarına bağlı
olarak büyümeyi durduracaktır.

#### Zaman senkronizasyonu

Zabbix'in çalıştığı sunucuda hassas sistem tarihinin olması çok
önemlidir. [ntpd](http://www.ntp.org/), ana bilgisayarın zamanını diğer
makinelerin saatiyle senkronize eden en popüler sunucudur. Üzerinde
Zabbix bileşenleri çalışan tüm sistemlerde senkronize edilmiş sistem
tarihinin korunması şiddetle önerilir.

Saat senkronize edilmezse Zabbix, veri bağlantısı kurulduktan sonra
istemci/sunucu zaman damgalarını alarak ve alınan öğe değeri zaman
damgalarını istemci-sunucu saat farkına göre ayarlayarak toplanan
verilerin zaman damgalarını Zabbix sunucu zamanına dönüştürecektir.
Basit tutmak ve olası komplikasyonları önlemek için bağlantı gecikmesi
göz ardı edilir. Bu nedenle bağlantı gecikmesi, aktif bağlantılardan
(aktif aracı, aktif proxy, gönderici) elde edilen verilerin zaman
damgalarına eklenir ve pasif bağlantılardan (pasif proxy) elde
edilenlerin zaman damgalarından çıkarılır. Diğer tüm kontroller sunucu
zamanında yapılır ve zaman damgaları ayarlanmaz.
