FIXME **This page is not fully translated, yet. Please help completing
the translation.**\
*(remove this paragraph once the translation is finished)*

# 5 Konteynırlardan kurulum

### Docker

Zabbix, dağıtım ve güncelleme prosedürünü hızlandırmak için her bir
Zabbix bileşeninin taşınabilir ve kendine yeterli konteynırını
[Docker](https://www.docker.com) imajları olarak sağlar.

Zabbix bileşenleri MySQL ve PostgreSQL veritabanı desteği, Apache2 ve
Nginx web sunucusu desteği ile birlikte gelir. Bu imajlar farklı
imajlara ayrılmıştır.

#### Docker temel imajları

Zabbix bileşenleri Ubuntu ve Alpine Linux temel imajlarında
sağlanmaktadır:

|   |   |
|---|---|
|İmaj|Sürüm|
|[alpine](https://hub.docker.com/_/alpine/)|son|
|[ubuntu](https://hub.docker.com/_/ubuntu/)|trusty|

Tüm imajlar, temel imajlar güncellenirse en yeni imajları yeniden inşa
edecek şekilde yapılandırılmıştır.

#### Docker dosya kaynakları

Herkes, [github.com](https://github.com/) üzerindeki Zabbix [resmi
deposunu](https://github.com/zabbix/zabbix-docker) kullanarak Docker
dosya değişikliklerini izleyebilir. Projeyi çatallayabilir veya resmi
Docker dosyalarına dayalı olarak kendi imajlarınızı oluşturabilirsiniz.

#### Yapı

Tüm Zabbix bileşenleri aşağıdaki Docker depoları içinde bulunur:

-   Zabbix aracısı -
    [zabbix/zabbix-agent](https://hub.docker.com/r/zabbix/zabbix-agent/)
-   Zabbix sunucusu
    -   MySQL veritabanı destekli Zabbix sunucusu -
        [zabbix/zabbix-server-mysql](https://hub.docker.com/r/zabbix/zabbix-server-mysql/)
    -   PostgreSQL veritabanı destekli Zabbix sunucusu -
        [zabbix/zabbix-server-pgsql](https://hub.docker.com/r/zabbix/zabbix-server-pgsql/)
-   Zabbix web-arayüzü
    -   MySQL veritabanı destekli ve Apache2 web sunucusuna dayalı
        Zabbix web-arayüzü -
        [zabbix/zabbix-web-apache-mysql](https://hub.docker.com/r/zabbix/zabbix-web-apache-mysql/)
    -   MySQL veritabanı destekli ve Nginx web sunucusuna dayalı Zabbix
        web-arayüzü -
        [zabbix/zabbix-web-nginx-mysql](https://hub.docker.com/r/zabbix/zabbix-web-nginx-mysql/)
    -   PostgreSQL veritabanı destekli ve Nginx web sunucusuna dayalı
        Zabbix web-arayüzü -
        [zabbix/zabbix-web-nginx-pgsql](https://hub.docker.com/r/zabbix/zabbix-web-nginx-pgsql/)
-   Zabbix vekil sunucusu
    -   SQLite3 veritabanı destekli Zabbix vekil sunucusu -
        [zabbix/zabbix-proxy-sqlite3](https://hub.docker.com/r/zabbix/zabbix-proxy-sqlite3/)
    -   MySQL veritabanı destekli Zabbix vekil sunucusu -
        [zabbix/zabbix-proxy-mysql](https://hub.docker.com/r/zabbix/zabbix-proxy-mysql/)
-   Zabbix Java Ağ Geçidi -
    [zabbix/zabbix-java-gateway](https://hub.docker.com/r/zabbix/zabbix-java-gateway/)

Ayrıca SNMP trap desteği de vardır. Yalnızca Ubuntu Trusty'e dayalı ek
depo
([zabbix/zabbix-snmptraps](https://hub.docker.com/r/zabbix/zabbix-snmptraps/))
olarak sağlanmaktadır. Zabbix sunucusu ve Zabbix vekil sunucusu ile
bağlantılı olabilir.

#### Sürümler

Her Zabbix bileşenleri deposu aşağıdaki etiketleri içerir:

-   `latest` - Alpine Linux imajı temelli bir Zabbix bileşeninin en son
    kararlı sürümü
-   `alpine-latest` - Alpine Linux imajı temelli bir Zabbix bileşeninin
    en son kararlı sürümü
-   `ubuntu-latest` - Ubuntu imajı temelli bir Zabbix bileşeninin en son
    kararlı sürümü
-   `alpine-3.2-latest` - Alpine Linux imajı temelli bir Zabbix 3.2
    bileşeninin en son alt sürümü
-   `ubuntu-3.2-latest` - Ubuntu imajı temelli bir Zabbix 3.2
    bileşeninin en son alt sürümü
-   `alpine-3.2.*` - Alpine Linux imajı temelli bir Zabbix 3.2
    bileşeninin farklı alt sürümleri, burada `*` Zabbix bileşeninin alt
    sürümüdür
-   `ubuntu-3.2.*` - Ubuntu imajı temelli bir Zabbix 3.2 bileşeninin
    farklı alt sürümleri, burada `*` Zabbix bileşeninin alt sürümüdür

#### Kullanım

##### Ortam değişkenleri

Tüm Zabbix bileşen imajları yapılandırmayı kontrol etmek için ortam
değişkenleri sağlar. Bu ortam değişkenleri, her bir bileşen deposunda
listelenir. Bu ortam değişkenleri, Zabbix yapılandırma dosyalarından
gelen seçeneklerdir ancak adlandırma yöntemi farklıdır. Örneğin,
`ZBX_LOGSLOWQUERIES` değişkeni Zabbix sunucusu ve Zabbix vekil sunucusu
yapılandırma dosyalarındaki `LogSlowQueries` değişkenine eşittir.

::: noteimportant
 Bazı yapılandırma seçeneklerinin değiştirilmesine
izin verilmez. Örneğin, `PIDFile` ve `LogType`.
:::

Bazı bileşenler, resmi Zabbix yapılandırma dosyalarında bulunmayan özel
ortam değişkenlerine sahiptir:

|   |   |   |
|---|---|---|
|**Değişken**|**Bileşenler**|**Açıklama**|
|`DB_SERVER_HOST`|Sunucu<br>Vekil sunucu<br>Web arayüzü|Bu değişken, MySQL veya PostgreSQL sunucusunun IP veya DNS adıdır.<br>Öntanımlı değer, MySQL ve PostgreSQL için sırasıyla `mysql-server` ve `postgres-server` adlarıdır|
|`DB_SERVER_PORT`|Sunucu<br>Vekil sunucu<br>Web arayüzü|Bu değişken, MySQL veya PostgreSQL sunucusunun portudur.<br>Öntanımlı olarak, değer sırasıyla '3306' veya '5432' şeklindedir.|
|`MYSQL_USER`|Sunucu<br>Vekil sunucu<br>Web arayüzü|MySQL veritabanı kullanıcısı.<br>Öntanımlı olarak değer 'zabbix' şeklindedir.|
|`MYSQL_PASSWORD`|Sunucu<br>Vekil sunucu<br>Web arayüzü|MySQL veritabanı parolası.<br>Öntanımlı olarak değer 'zabbix' şeklindedir.|
|`MYSQL_DATABASE`|Sunucu<br>Vekil sunucu<br>Web arayüzü|Zabbix veritabanı adı.<br>Öntanımlı olarak değer, Zabbix sunucusu için 'zabbix' ve Zabbix vekil sunucusu için 'zabbix\_proxy' şeklindedir.|
|`POSTGRES_USER`|Sunucu<br>Web arayüzü|PostgreSQL veritabanı kullanıcısı.<br>Öntanımlı olarak değer 'zabbix' şeklindedir.|
|`POSTGRES_PASSWORD`|Sunucu<br>Web arayüzü|PostgreSQL veritabanı parolası.<br>Öntanımlı olarak değer 'zabbix' şeklindedir.|
|`POSTGRES_DB`|Sunucu<br>Web arayüzü|Zabbix veritabanı adı.<br>Öntanımlı olarak değer, Zabbix sunucusu için 'zabbix' ve Zabbix vekil sunucusu için 'zabbix\_proxy' şeklindedir.|
|`TZ`|Web arayüzü|PHP biçiminde saat dilimi. Desteklenen saat dilimlerinin tam listesi [php.net](http://php.net/manual/en/timezones.php) üzerinden edinilebilir.<br>Öntanımlı olarak, değer 'Europe/Riga' şeklindedir.|
|`ZBX_SERVER_NAME`|Web arayüzü|Web arayüzünün sağ üst köşesindeki görünür Zabbix kurulum adı.<br>Öntanımlı olarak, değer 'Zabbix Docker' şeklindedir|
|`ZBX_JAVAGATEWAY_ENABLE`|Sunucu<br>Vekil sunucu|Java ile ilgili kontrolleri toplamak için Zabbix Java ağ geçidiyle iletişim kurmayı sağlar.<br>Öntanımlı olarak, değer "false" şeklindedir|
|`ZBX_ENABLE_SNMP_TRAPS`|Sunucu<br>Vekil sunucu|SNMP trap özelliğini etkinleştirir. It requires Zabbix sunucusu veya Zabbix vekil sunucusu için **zabbix-snmptraps** örneği ve */var/lib/zabbix/snmptraps* paylaşımlı disk birimi gerektirir.|

##### Disk birimleri

İmajlar bazı bağlantı noktalarını kullanmaya izin verir. Bu bağlantı
noktaları farklıdır ve Zabbix bileşen türüne göre değişir:

|   |   |
|---|---|
|**Disk birimi**|**Açıklama**|
|**Zabbix aracısı**|<|
|*/etc/zabbix/zabbix\_agentd.d*|Bu disk birimi, *\*.conf* dosyalarını içermesine ve `UserParameter` özelliğini kullanarak Zabbix aracısının genişletilmesine izin verir|
|*/var/lib/zabbix/modules*|Bu disk birimi, ek modülleri yüklemeye ve [LoadModule](/manual/config/items/loadablemodules) özelliğini kullanarak Zabbix aracısının genişletilmesine izin verir|
|*/var/lib/zabbix/enc*|Bu disk birimi, TLS ile ilgili dosyaları depolamak için kullanılır. Bu dosya isimleri, `ZBX_TLSCAFILE`, `ZBX_TLSCRLFILE`, `ZBX_TLSKEY_FILE` ve `ZBX_TLSPSKFILE` ortam değişkenleri kullanılarak belirtilir|
|**Zabbix server**|<|
|*/usr/lib/zabbix/alertscripts*|The volume is used for custom alert scripts. It is the `AlertScriptsPath` parameter in [zabbix\_server.conf](/manual/appendix/config/zabbix_server)|
|*/usr/lib/zabbix/externalscripts*|The volume is used by [external checks](/manual/config/items/itemtypes/external). It is the `ExternalScripts` parameter in [zabbix\_server.conf](/manual/appendix/config/zabbix_server)|
|*/var/lib/zabbix/modules*|The volume allows to load additional modules and extend Zabbix server using the [LoadModule](/manual/config/items/loadablemodules) feature|
|*/var/lib/zabbix/enc*|The volume is used to store TLS related files. These file names are specified using `ZBX_TLSCAFILE`, `ZBX_TLSCRLFILE`, `ZBX_TLSKEY_FILE` and `ZBX_TLSPSKFILE` environment variables|
|*/var/lib/zabbix/ssl/certs*|The volume is used as location of SSL client certificate files for client authentication. It is the `SSLCertLocation` parameter in zabbix\_server.conf|
|*/var/lib/zabbix/ssl/keys*|The volume is used as location of SSL private key files for client authentication. It is the `SSLKeyLocation` parameter in [zabbix\_server.conf](/manual/appendix/config/zabbix_server)|
|*/var/lib/zabbix/ssl/ssl\_ca*|The volume is used as location of certificate authority (CA) files for SSL server certificate verification. It is the `SSLCALocation` parameter in [zabbix\_server.conf](/manual/appendix/config/zabbix_server)|
|*/var/lib/zabbix/snmptraps*|The volume is used as location of snmptraps.log file. It could be shared by zabbix-snmptraps container and inherited using the volumes\_from Docker option while creating a new instance of Zabbix server. SNMP trap processing feature could be enabled by using shared volume and switching the `ZBX_ENABLE_SNMP_TRAPS` environment variable to 'true'|
|*/var/lib/zabbix/mibs*|The volume allows to add new MIB files. It does not support subdirectories, all MIBs must be placed in `/var/lib/zabbix/mibs`|
|**Zabbix proxy**|<|
|*/usr/lib/zabbix/externalscripts*|The volume is used by [external checks](/manual/config/items/itemtypes/external). It is the `ExternalScripts` parameter in [zabbix\_proxy.conf](/manual/appendix/config/zabbix_proxy)|
|*/var/lib/zabbix/modules*|The volume allows to load additional modules and extend Zabbix server using the [LoadModule](/manual/config/items/loadablemodules) feature|
|*/var/lib/zabbix/enc*|The volume is used to store TLS related files. These file names are specified using `ZBX_TLSCAFILE`, `ZBX_TLSCRLFILE`, `ZBX_TLSKEY_FILE` and `ZBX_TLSPSKFILE` environment variables|
|*/var/lib/zabbix/ssl/certs*|The volume is used as location of SSL client certificate files for client authentication. It is the `SSLCertLocation` parameter in [zabbix\_proxy.conf](/manual/appendix/config/zabbix_proxy)|
|*/var/lib/zabbix/ssl/keys*|The volume is used as location of SSL private key files for client authentication. It is the `SSLKeyLocation` parameter in [zabbix\_proxy.conf](/manual/appendix/config/zabbix_proxy)|
|*/var/lib/zabbix/ssl/ssl\_ca*|The volume is used as location of certificate authority (CA) files for SSL server certificate verification. It is the `SSLCALocation` parameter in [zabbix\_proxy.conf](/manual/appendix/config/zabbix_proxy)|
|*/var/lib/zabbix/snmptraps*|The volume is used as location of snmptraps.log file. It could be shared by the zabbix-snmptraps container and inherited using the volumes\_from Docker option while creating a new instance of Zabbix server. SNMP trap processing feature could be enabled by using shared volume and switching the `ZBX_ENABLE_SNMP_TRAPS` environment variable to 'true'|
|*/var/lib/zabbix/mibs*|The volume allows to add new MIB files. It does not support subdirectories, all MIBs must be placed in `/var/lib/zabbix/mibs`|
|**Zabbix web interface based on Apache2 web server**|<|
|*/etc/ssl/apache2*|The volume allows to enable HTTPS for Zabbix web interface. The volume must contain the two `ssl.crt` and `ssl.key` files prepared for Apache2 SSL connections|
|**Zabbix web interface based on Nginx web server**|<|
|*/etc/ssl/nginx*|The volume allows to enable HTTPS for Zabbix web interface. The volume must contain the two `ssl.crt`, `ssl.key` files and `dhparam.pem` prepared for Nginx SSL connections|
|**Zabbix snmptraps**|<|
|*/var/lib/zabbix/snmptraps*|The volume contains the `snmptraps.log` log file named with received SNMP traps|
|*/var/lib/zabbix/mibs*|The volume allows to add new MIB files. It does not support subdirectories, all MIBs must be placed in `/var/lib/zabbix/mibs`|

For additional information use Zabbix official repositories in Docker
Hub.

##### Kullanım örnekleri

\*\* Örnek 1 \*\*

Bu örnek, MySQL veritabanı desteği olan Zabbix sunucusunu, Nginx web
sunucusuna dayanan Zabbix web arayüzü ve Zabbix Java ağ geçidinin nasıl
çalıştırılacağını göstermektedir.

1\. Boş MySQL sunucu örneği çalıştırın

    # docker run --name mysql-server -t \
          -e MYSQL_DATABASE="zabbix" \
          -e MYSQL_USER="zabbix" \
          -e MYSQL_PASSWORD="zabbix_pwd" \
          -e MYSQL_ROOT_PASSWORD="root_pwd" \
          -d mysql:5.7
          --character-set-server=utf8 --collation-server=utf8_bin

2\. Zabbix Java ağ geçidi örneği çalıştırın

    # docker run --name zabbix-java-gateway -t \
          -d zabbix/zabbix-java-gateway:latest

3\. Zabbix sunucu örneği çalıştırın ve bu örneği oluşturulan MySQL
sunucu örneğiyle bağlayın

    # docker run --name zabbix-server-mysql -t \
          -e DB_SERVER_HOST="mysql-server" \
          -e MYSQL_DATABASE="zabbix" \
          -e MYSQL_USER="zabbix" \
          -e MYSQL_PASSWORD="zabbix_pwd" \
          -e MYSQL_ROOT_PASSWORD="root_pwd" \
          -e ZBX_JAVAGATEWAY="zabbix-java-gateway" \
          --link mysql-server:mysql \
          --link zabbix-java-gateway:zabbix-java-gateway \
          -p 10051:10051 \
          -d zabbix/zabbix-server-mysql:latest

::: noteclassic
Zabbix server instance exposes 10051/TCP port (Zabbix
trapper) to host machine.
:::

4\. Start Zabbix web interface and link the instance with created MySQL
server and Zabbix server instances

    # docker run --name zabbix-web-nginx-mysql -t \
          -e DB_SERVER_HOST="mysql-server" \
          -e MYSQL_DATABASE="zabbix" \
          -e MYSQL_USER="zabbix" \
          -e MYSQL_PASSWORD="zabbix_pwd" \
          -e MYSQL_ROOT_PASSWORD="root_pwd" \
          --link mysql-server:mysql \
          --link zabbix-server-mysql:zabbix-server \
          -p 80:80 \
          -d zabbix/zabbix-web-nginx-mysql:latest

::: noteclassic
Zabbix web interface instance exposes 80/TCP port (HTTP) to
host machine.
:::

\*\* Example 2 \*\*

The example demonstrates how to run Zabbix server with PostgreSQL
database support, Zabbix web interface based on the Nginx web server and
SNMP trap feature.

1\. Start empty PostgreSQL server instance

    # docker run --name postgres-server -t \
          -e POSTGRES_USER="zabbix" \
          -e POSTGRES_PASSWORD="zabbix" \
          -e POSTGRES_DB="zabbix_pwd" \
          -d postgres:latest

2\. Start Zabbix snmptraps instance

    # docker run --name zabbix-snmptraps -t \
          -v /zbx_instance/snmptraps:/var/lib/zabbix/snmptraps:rw \
          -v /var/lib/zabbix/mibs:/usr/share/snmp/mibs:ro \
          -p 162:162/udp \
          -d zabbix/zabbix-snmptraps:latest

::: noteclassic
Zabbix snmptrap instance exposes the 162/UDP port (SNMP
traps) to host machine.
:::

3\. Start Zabbix server instance and link the instance with created
PostgreSQL server instance

    # docker run --name zabbix-server-pgsql -t \
          -e DB_SERVER_HOST="postgres-server" \
          -e POSTGRES_USER="zabbix" \
          -e POSTGRES_PASSWORD="zabbix" \
          -e POSTGRES_DB="zabbix_pwd" \
          -e ZBX_ENABLE_SNMP_TRAPS="true" \
          --link postgres-server:postgres \
          -p 10051:10051 \
          --volumes-from zabbix-snmptraps \
          -d zabbix/zabbix-server-pgsql:latest

::: noteclassic
Zabbix server instance exposes the 10051/TCP port (Zabbix
trapper) to host machine.
:::

4\. Start Zabbix web interface and link the instance with created
PostgreSQL server and Zabbix server instances

    # docker run --name zabbix-web-nginx-pgsql -t \
          -e DB_SERVER_HOST="postgres-server" \
          -e POSTGRES_USER="zabbix" \
          -e POSTGRES_PASSWORD="zabbix" \
          -e POSTGRES_DB="zabbix_pwd" \
          --link postgres-server:postgres \
          --link zabbix-server-pgsql:zabbix-server \
          -p 443:443 \
          -v /etc/ssl/nginx:/etc/ssl/nginx:ro \
          -d zabbix/zabbix-web-nginx-pgsql:latest

::: noteclassic
Zabbix web interface instance exposes the 443/TCP port
(HTTPS) to host machine.\
Directory */etc/ssl/nginx* must contain certificate with required
name.
:::

### Docker Compose

Zabbix provides compose files also for defining and running
multi-container Zabbix components in Docker. These compose files are
available in Zabbix docker official repository on github.com:
<https://github.com/zabbix/zabbix-docker>. These compose files are added
as examples, they are overloaded. For example, they contain proxies with
MySQL and SQLite3 support.

There are a few different versions of compose files:

|   |   |
|---|---|
|**File name**|**Description**|
|`docker-compose_v2_alpine_mysql_latest.yaml`|The compose file runs the latest version of Zabbix 3.2 components on Alpine Linux with MySQL database support.|
|`docker-compose_v2_alpine_mysql_local.yaml`|The compose file locally builds the latest version of Zabbix 3.2 and runs Zabbix components on Alpine Linux with MySQL database support.|
|`docker-compose_v2_alpine_pgsql_latest.yaml`|The compose file runs the latest version of Zabbix 3.2 components on Alpine Linux with PostgreSQL database support.|
|`docker-compose_v2_alpine_pgsql_local.yaml`|The compose file locally builds the latest version of Zabbix 3.2 and runs Zabbix components on Alpine Linux with PostgreSQL database support.|
|`docker-compose_v2_ubuntu_mysql_latest.yaml`|The compose file runs the latest version of Zabbix 3.2 components on Ubuntu 14.04 with MySQL database support.|
|`docker-compose_v2_ubuntu_mysql_local.yaml`|The compose file locally builds the latest version of Zabbix 3.2 and runs Zabbix components on Ubuntu 14.04 with MySQL database support.|
|`docker-compose_v2_ubuntu_pgsql_latest.yaml`|The compose file runs the latest version of Zabbix 3.2 components on Ubuntu 14.04 with PostgreSQL database support.|
|`docker-compose_v2_ubuntu_pgsql_local.yaml`|The compose file locally builds the latest version of Zabbix 3.2 and runs Zabbix components on Ubuntu 14.04 with PostgreSQL database support.|

::: noteimportant
Available Docker compose files support only
version 2 of Docker Compose.
:::

#### Storage

Compose files are configured to support local storage on a host machine.
Docker Compose will create a `zbx_env` directory in the folder with the
compose file when you run Zabbix components using the compose file. The
directory will contain the same structure as described above in the
[Volumes](#Volumes) section and directory for database storage.

There are also volumes in read-only mode for `/etc/localtime` and
`/etc/timezone` files.

#### Ortam dosyaları

In the same directory with compose files on github.com you can find
files with default environment variables for each component in compose
file. These environment files are named like `.env_<type of component>`.

#### Örnekler

\*\* Örnek 1 \*\*

    # docker-compose -f ./docker-compose_v2_alpine_mysql_latest.yaml up -d

Komut, her Zabbix bileşeni için en son Zabbix 3.2 imajlarını indirecek
ve bunları detach kipinde çalıştıracaktır.

::: noteimportant
github.com'daki resmi Zabbix deposundan
`.env_<bileşen türü>` dosyalarını indirmeyi unutmayın.
:::

\*\* Örnek 2 \*\*

    # docker-compose -f ./docker-compose_v2_ubuntu_mysql_local.yaml up -d

Komut, Ubuntu 14.04 temel imajını indirecek, sonra Zabbix 3.2
bileşenlerini yerel olarak inşa edecek ve bunları detach kipinde
çalıştıracaktır.
