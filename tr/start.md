# Türkçe Zabbix belgeleri

Bu sayfalarda resmi Zabbix belgeleri bulunmaktadır.

Belgelendirme sayfalarına göz atmak için kenar çubuğunu kullanın.

Sayfaları izleyebilmek için [Zabbix
forumlarındaki](http://www.zabbix.com/forum/) kullanıcı adı ve
parolanızla giriş yapın.

[comment]: # aside:35
