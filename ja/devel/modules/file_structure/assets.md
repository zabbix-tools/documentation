[comment]: # translation:outdated

[comment]: # aside:4

[comment]: # ({new-cd954d56})

# Assets

The folder *assets* may contain any files and subfolders that do not belong to other directories. You can use it for:

- JavaScript styles (must be inside *[assets/js](#assetsjs)*);
- CSS styles (must be inside *[assets/css](#assetscss)*);
- Images;
- Fonts;
- Anything else you need to include.

[comment]: # ({/new-cd954d56})

[comment]: # ({new-08585fb9})

### assets/js

*assets/js* directory is reserved and should only contain JavaScript files.
To be used by the widget, specify these files in the *[manifest.json](/manifest)*.

For example:

```json
"assets": {
    "js": ["class.widget.js"]
}
```

[comment]: # ({/new-08585fb9})

[comment]: # ({new-18641225})

### assets/css

*assets/css* is reserved and should only contain CSS style files.
To be used by the widget, specify these files in the *[manifest.json](/manifest)*.

For example:

```json
"assets": {
    "css": ["mywidget.css"]
}
```

[comment]: # ({/new-18641225})

[comment]: # ({new-aedbe0c5})

##### CSS styles

CSS files may contain a custom attribute `theme` to define different style for a specific frontend themes.

Available themes and their attribute values:

-   **Blue** - [theme='blue-theme']
-   **Dark** - [theme='dark-theme']
-   **High-contrast light** - [theme='hc-light']
-   **High-contrast dark** - [theme='hc-dark']

Example:

```css
.widget {
    background-color: red;
}
 
[theme='dark-theme'] .widget {
    background-color: green;
}
```

[comment]: # ({/new-aedbe0c5})
