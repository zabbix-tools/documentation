[comment]: # translation:outdated

[comment]: # ({new-ae6d2ebd})
# 3 Hosts

[comment]: # ({/new-ae6d2ebd})

[comment]: # ({new-907c3faa})
#### Overview

Hosts are [exported](/manual/xml_export_import) with many related
objects and object relations.

Host export contains:

-   linked host groups
-   host data
-   template linkage
-   host group linkage
-   host interfaces
-   directly linked items
-   directly linked triggers
-   directly linked graphs
-   directly linked discovery rules with all prototypes
-   directly linked web scenarios
-   host macros
-   host inventory data
-   value maps

[comment]: # ({/new-907c3faa})

[comment]: # ({new-55d6da26})
#### Exporting

To export hosts, do the following:

-   Go to: *Configuration* → *Hosts*
-   Mark the checkboxes of the hosts to export
-   Click on *Export* below the list

![](../../../assets/en/manual/xml_export_import/export_hosts.png)

Depending on the selected format, hosts are exported to a local file
with a default name:

-   *zabbix\_export\_hosts.yaml* - in YAML export (default option for
    export)
-   *zabbix\_export\_hosts.xml* - in XML export
-   *zabbix\_export\_hosts.json* - in JSON export

[comment]: # ({/new-55d6da26})

[comment]: # ({new-158cdcbf})
#### Importing

To import hosts, do the following:

-   Go to: *Configuration* → *Hosts*
-   Click on *Import* to the right
-   Select the import file
-   Mark the required options in import rules
-   Click on *Import*

![](../../../assets/en/manual/xml_export_import/import_hosts.png)

A success or failure message of the import will be displayed in the
frontend.

Import rules:

|Rule|Description|
|----|-----------|
|*Update existing*|Existing elements will be updated with data taken from the import file. Otherwise they will not be updated.|
|*Create new*|The import will add new elements using data from the import file. Otherwise it will not add them.|
|*Delete missing*|The import will remove existing elements not present in the import file. Otherwise it will not remove them.<br>If *Delete missing* is marked for template linkage, existing template linkage not present in the import file will be removed from the host along with all entities inherited from the potentially unlinked templates (items, triggers, etc).|

[comment]: # ({/new-158cdcbf})

[comment]: # ({new-93059e09})
#### Export format

Export format in YAML:

``` {.yaml}
zabbix_export:
  version: '6.0'
  date: '2021-09-28T12:20:07Z'
  groups:
    -
      uuid: f2481361f99448eea617b7b1d4765566
      name: 'Discovered hosts'
    -
      uuid: 6f6799aa69e844b4b3918f779f2abf08
      name: 'Zabbix servers'
  hosts:
    -
      host: 'Zabbix server 1'
      name: 'Main Zabbix server'
      templates:
        -
          name: 'Linux by Zabbix agent'
        -
          name: 'Zabbix server health'
      groups:
        -
          name: 'Discovered hosts'
        -
          name: 'Zabbix servers'
      interfaces:
        -
          ip: 192.168.1.1
          interface_ref: if1
      items:
        -
          name: 'Zabbix trap'
          type: TRAP
          key: trap
          delay: '0'
          history: 1w
          preprocessing:
            -
              type: MULTIPLIER
              parameters:
                - '8'
          tags:
            -
              tag: Application
              value: 'Zabbix server'
          triggers:
            -
              expression: 'last(/Zabbix server 1/trap)=0'
              name: 'Last value is zero'
              priority: WARNING
              tags:
                -
                  tag: Process
                  value: 'Internal test'
      tags:
        -
          tag: Process
          value: Zabbix
      macros:
        -
          macro: '{$HOST.MACRO}'
          value: '123'
        -
          macro: '{$PASSWORD1}'
          type: SECRET_TEXT
      inventory:
        type: 'Zabbix server'
        name: yyyyyy-HP-Pro-3010-Small-Form-Factor-PC
        os: 'Linux yyyyyy-HP-Pro-3010-Small-Form-Factor-PC 4.4.0-165-generic #193-Ubuntu SMP Tue Sep 17 17:42:52 UTC 2019 x86_64'
      inventory_mode: AUTOMATIC
  graphs:
    -
      name: 'CPU utilization server'
      show_work_period: 'NO'
      show_triggers: 'NO'
      graph_items:
        -
          drawtype: FILLED_REGION
          color: FF5555
          item:
            host: 'Zabbix server 1'
            key: 'system.cpu.util[,steal]'
        -
          sortorder: '1'
          drawtype: FILLED_REGION
          color: 55FF55
          item:
            host: 'Zabbix server 1'
            key: 'system.cpu.util[,softirq]'
        -
          sortorder: '2'
          drawtype: FILLED_REGION
          color: '009999'
          item:
            host: 'Zabbix server 1'
            key: 'system.cpu.util[,interrupt]'
        -
          sortorder: '3'
          drawtype: FILLED_REGION
          color: '990099'
          item:
            host: 'Zabbix server 1'
            key: 'system.cpu.util[,nice]'
        -
          sortorder: '4'
          drawtype: FILLED_REGION
          color: '999900'
          item:
            host: 'Zabbix server 1'
            key: 'system.cpu.util[,iowait]'
        -
          sortorder: '5'
          drawtype: FILLED_REGION
          color: '990000'
          item:
            host: 'Zabbix server 1'
            key: 'system.cpu.util[,system]'
        -
          sortorder: '6'
          drawtype: FILLED_REGION
          color: '000099'
          calc_fnc: MIN
          item:
            host: 'Zabbix server 1'
            key: 'system.cpu.util[,user]'
        -
          sortorder: '7'
          drawtype: FILLED_REGION
          color: '009900'
          item:
            host: 'Zabbix server 1'
            key: 'system.cpu.util[,idle]'

```

[comment]: # ({/new-93059e09})

[comment]: # ({new-2c61d3f8})
#### Element tags

Element tag values are explained in the table below.

[comment]: # ({/new-2c61d3f8})

[comment]: # ({new-eedfe2b9})
##### Host tags

|Element|Element property|Required|Type|Range^**[1](#footnotes)**^|Description|
|-------|----------------|--------|----|--------------------------|-----------|
|groups|<|x|<|<|Root element for host groups.|
|<|name|x|`string`|<|Host group name.|
|hosts|<|\-|<|<|Root element for hosts.|
|<|host|x|`string`|<|Unique host name.|
|<|name|\-|`string`|<|Visible host name.|
|<|description|\-|`text`|<|Host description.|
|<|status|\-|`string`|0 - ENABLED (default)<br>1 - DISABLED|Host status.|
|<|ipmi\_authtype|\-|`string`|-1 - DEFAULT (default)<br>0 - NONE<br>1 - MD2<br>2 - MD5<br>4 - STRAIGHT<br>5 - OEM<br>6 - RMCP\_PLUS|IPMI session authentication type.|
|<|ipmi\_privilege|\-|`string`|1 - CALLBACK<br>2 - USER (default)<br>3 - OPERATOR<br>4 - ADMIN<br>5 - OEM|IPMI session privilege level.|
|<|ipmi\_username|\-|`string`|<|Username for IPMI checks.|
|<|ipmi\_password|\-|`string`|<|Password for IPMI checks.|
|proxy|<|\-|<|<|Proxy.|
|<|name|x|`string`|<|Name of the proxy (if any) that monitors the host.|
|templates|<|\-|<|<|Root element for linked templates.|
|<|name|x|`string`|<|Template name.|
|interfaces|<|\-|<|<|Root element for host interfaces.|
|<|default|\-|`string`|0 - NO<br>1 - YES (default)|Whether this is the primary host interface.<br>There can be only one primary interface of one type on a host.|
|<|type|\-|`string`|1 - ZABBIX (default)<br>2 - SNMP<br>3 - IPMI<br>4 - JMX|Interface type.|
|<|useip|\-|`string`|0 - NO<br>1 - YES (default)|Whether to use IP as the interface for connecting to the host (if not, DNS will be used).|
|<|ip|\-|`string`|<|IP address, can be either IPv4 or IPv6.<br><br>Required if the connection is made via IP.|
|<|dns|\-|`string`|<|DNS name.<br><br>Required if the connection is made via DNS.|
|<|port|\-|`string`|<|Port number. Supports user macros.|
|<|interface\_ref|x|`string`|Format: `if<N>`|Interface reference name to be used in items.|
|details|<|\-|<|<|Root element for interface details.|
|<|version|\-|`string`|1 - SNMPV1<br>2 - SNMP\_V2C (default)<br>3 - SNMP\_V3|Use this SNMP version.|
|<|community|\-|`string`|<|SNMP community.<br><br>Required by SNMPv1 and SNMPv2 items.|
|<|contextname|\-|`string`|<|SNMPv3 context name.<br><br>Used only by SNMPv3 items.|
|<|securityname|\-|`string`|<|SNMPv3 security name.<br><br>Used only by SNMPv3 items.|
|<|securitylevel|\-|`string`|0 - NOAUTHNOPRIV (default)<br>1 - AUTHNOPRIV<br>2 - AUTHPRIV|SNMPv3 security level.<br><br>Used only by SNMPv3 items.|
|<|authprotocol|\-|`string`|0 - MD5 (default)<br>1 - SHA1<br>2 - SHA224<br>3 - SHA256<br>4 - SHA384<br>5 - SHA512|SNMPv3 authentication protocol.<br><br>Used only by SNMPv3 items.|
|<|authpassphrase|\-|`string`|<|SNMPv3 authentication passphrase.<br><br>Used only by SNMPv3 items.|
|<|privprotocol|\-|`string`|0 - DES (default)<br>1 - AES128<br>2 - AES192<br>3 - AES256<br>4 - AES192C<br>5 - AES256C|SNMPv3 privacy protocol.<br><br>Used only by SNMPv3 items.|
|<|privpassphrase|\-|`string`|<|SNMPv3 privacy passphrase.<br><br>Used only by SNMPv3 items.|
|<|bulk|\-|`string`|0 - NO<br>1 - YES (default)|Use bulk requests for SNMP.|
|items|<|\-|<|<|Root element for items.|
|<|*For item element tag values, see host [item](/manual/xml_export_import/hosts#host_item_tags) tags.*|<|<|<|<|
|tags|<|\-|<|<|Root element for host tags.|
|<|tag|x|`string`|<|Tag name.|
|<|value|\-|`string`|<|Tag value.|
|macros|<|\-|<|<|Root element for macros.|
|<|macro|x|<|<|User macro name.|
|<|type|\-|`string`|0 - TEXT (default)<br>1 - SECRET\_TEXT<br>2 - VAULT|Type of the macro.|
|<|value|\-|`string`|<|User macro value.|
|<|description|\-|`string`|<|User macro description.|
|inventory|<|\-|<|<|Root element for host inventory.|
|<|<inventory\_property>|\-|<|<|Individual inventory property.<br><br>All available inventory properties are listed under the respective tags, e.g. <type>, <name>, <os> (see example above).|
|inventory\_mode|<|\-|`string`|-1 - DISABLED<br>0 - MANUAL (default)<br>1 - AUTOMATIC|Inventory mode.|
|valuemaps|<|\-|<|<|Root element for host value maps.|
|<|name|x|`string`|<|Value map name.|
|<|mapping|\-|<|<|Root element for mappings.|
|<|value|x|`string`|<|Value of a mapping.|
|<|newvalue|x|`string`|<|New value of a mapping.|

[comment]: # ({/new-eedfe2b9})

[comment]: # ({new-8a2e4e2f})
##### Host item tags

|Element|Element property|Required|Type|Range^**[1](#footnotes)**^|Description|
|-------|----------------|--------|----|--------------------------|-----------|
|items|<|\-|<|<|Root element for items.|
|<|name|x|`string`|<|Item name.|
|<|type|\-|`string`|0 - ZABBIX\_PASSIVE (default)<br>2 - TRAP<br>3 - SIMPLE<br>5 - INTERNAL<br>7 - ZABBIX\_ACTIVE<br>10 - EXTERNAL<br>11 - ODBC<br>12 - IPMI<br>13 - SSH<br>14 - TELNET<br>15 - CALCULATED<br>16 - JMX<br>17 - SNMP\_TRAP<br>18 - DEPENDENT<br>19 - HTTP\_AGENT<br>20 - SNMP\_AGENT<br>21 - ITEM\_TYPE\_SCRIPT|Item type.|
|<|snmp\_oid|\-|`string`|<|SNMP object ID.<br><br>Required by SNMP items.|
|<|key|x|`string`|<|Item key.|
|<|delay|\-|`string`|Default: 1m|Update interval of the item.<br><br>Note that `delay` will be always '0' for trapper items.<br><br>Accepts seconds or a time unit with suffix (30s, 1m, 2h, 1d).<br>Optionally one or more [custom intervals](/manual/config/items/item/custom_intervals) can be specified either as flexible intervals or scheduling.<br>Multiple intervals are separated by a semicolon.<br>User macros may be used. A single macro has to fill the whole field. Multiple macros in a field or macros mixed with text are not supported.<br>Flexible intervals may be written as two macros separated by a forward slash (e.g. `{$FLEX_INTERVAL}/{$FLEX_PERIOD}`).|
|<|history|\-|`string`|Default: 90d|A time unit of how long the history data should be stored. Time unit with suffix, user macro or LLD macro.|
|<|trends|\-|`string`|Default: 365d|A time unit of how long the trends data should be stored. Time unit with suffix, user macro or LLD macro.|
|<|status|\-|`string`|0 - ENABLED (default)<br>1 - DISABLED|Item status.|
|<|value\_type|\-|`string`|0 - FLOAT<br>1 - CHAR<br>2 - LOG<br>3 - UNSIGNED (default)<br>4 - TEXT|Received value type.|
|<|allowed\_hosts|\-|`string`|<|List of IP addresses (comma delimited) of hosts allowed sending data for the item.<br><br>Used by trapper and HTTP agent items.|
|<|units|\-|`string`|<|Units of returned values (bps, B, etc).|
|<|params|\-|`text`|<|Additional parameters depending on the type of the item:<br>- executed script for Script, SSH and Telnet items;<br>- SQL query for database monitor items;<br>- formula for calculated items.|
|<|ipmi\_sensor|\-|`string`|<|IPMI sensor.<br><br>Used only by IPMI items.|
|<|authtype|\-|`string`|Authentication type for SSH agent items:<br>0 - PASSWORD (default)<br>1 - PUBLIC\_KEY<br><br>Authentication type for HTTP agent items:<br>0 - NONE (default)<br>1 - BASIC<br>2 - NTLM|Authentication type.<br><br>Used only by SSH and HTTP agent items.|
|<|username|\-|`string`|<|Username for authentication.<br>Used by simple check, SSH, Telnet, database monitor, JMX and HTTP agent items.<br><br>Required by SSH and Telnet items.<br>When used by JMX agent, password should also be specified together with the username or both properties should be left blank.|
|<|password|\-|`string`|<|Password for authentication.<br>Used by simple check, SSH, Telnet, database monitor, JMX and HTTP agent items.<br><br>When used by JMX agent, username should also be specified together with the password or both properties should be left blank.|
|<|publickey|\-|`string`|<|Name of the public key file.<br><br>Required for SSH agent items.|
|<|privatekey|\-|`string`|<|Name of the private key file.<br><br>Required for SSH agent items.|
|<|description|\-|`text`|<|Item description.|
|<|inventory\_link|\-|`string`|0 - NONE<br><br>Capitalized host inventory field name. For example:<br>4 - ALIAS<br>6 - OS\_FULL<br>14 - HARDWARE<br>etc.|Host inventory field that is populated by the item.<br><br>Refer to the [host inventory page](/manual/api/reference/host/object#host_inventory) for a list of supported host inventory fields and their IDs.|
|<|logtimefmt|\-|`string`|<|Format of the time in log entries.<br>Used only by log items.|
|<|interface\_ref|\-|`string`|Format: `if<N>`|Reference to the host interface.|
|<|jmx\_endpoint|\-|`string`|<|JMX endpoint.<br><br>Used only by JMX agent items.|
|<|url|\-|`string`|<|URL string.<br><br>Required only for HTTP agent items.|
|<|allow\_traps|\-|`string`|0 - NO (default)<br>1 - YES|Allow to populate value as in a trapper item.<br><br>Used only by HTTP agent items.|
|<|follow\_redirects|\-|`string`|0 - NO<br>1 - YES (default)|Follow HTTP response redirects while pooling data.<br><br>Used only by HTTP agent items.|
|headers|<|\-|<|<|Root element for HTTP(S) request headers, where header name is used as key and header value as value.<br>Used only by HTTP agent items.|
|<|name|x|`string`|<|Header name.|
|<|value|x|`string`|<|Header value.|
|<|http\_proxy|\-|`string`|<|HTTP(S) proxy connection string.<br><br>Used only by HTTP agent items.|
|<|output\_format|\-|`string`|0 - RAW (default)<br>1 - JSON|How to process response.<br><br>Used only by HTTP agent items.|
|<|post\_type|\-|`string`|0 - RAW (default)<br>2 - JSON<br>3 - XML|Type of post data body.<br><br>Used only by HTTP agent items.|
|<|posts|\-|`string`|<|HTTP(S) request body data.<br><br>Used only by HTTP agent items.|
|query\_fields|<|\-|<|<|Root element for query parameters.<br><br>Used only by HTTP agent items.|
|<|name|x|`string`|<|Parameter name.|
|<|value|\-|`string`|<|Parameter value.|
|<|request\_method|\-|`string`|0 - GET (default)<br>1 - POST<br>2 - PUT<br>3 - HEAD|Request method.<br><br>Used only by HTTP agent items.|
|<|retrieve\_mode|\-|`string`|0 - BODY (default)<br>1 - HEADERS<br>2 - BOTH|What part of response should be stored.<br><br>Used only by HTTP agent items.|
|<|ssl\_cert\_file|\-|`string`|<|Public SSL Key file path.<br><br>Used only by HTTP agent items.|
|<|ssl\_key\_file|\-|`string`|<|Private SSL Key file path.<br><br>Used only by HTTP agent items.|
|<|ssl\_key\_password|\-|`string`|<|Password for SSL Key file.<br><br>Used only by HTTP agent items.|
|<|status\_codes|\-|`string`|<|Ranges of required HTTP status codes separated by commas. Supports user macros.<br>Example: 200,200-{$M},{$M},200-400<br><br>Used only by HTTP agent items.|
|<|timeout|\-|`string`|<|Item data polling request timeout. Supports user macros.<br><br>Used by HTTP agent and Script items.|
|<|verify\_host|\-|`string`|0 - NO (default)<br>1 - YES|Validate if host name in URL is in Common Name field or a Subject Alternate Name field of host certificate.<br><br>Used only by HTTP agent items.|
|<|verify\_peer|\-|`string`|0 - NO (default)<br>1 - YES|Validate if host certificate is authentic.<br><br>Used only by HTTP agent items.|
|parameters|<|\-|<|<|Root element for user-defined parameters.<br><br>Used only by Script items.|
|<|name|x|`string`|<|Parameter name.<br><br>Used only by Script items.|
|<|value|\-|`string`|<|Parameter value.<br><br>Used only by Script items.|
|value map|<|\-|<|<|Value map.|
|<|name|x|`string`|<|Name of the value map to use for the item.|
|preprocessing|<|\-|<|<|Root element for item value preprocessing.|
|step|<|\-|<|<|Individual item value preprocessing step.|
|<|type|x|`string`|1 - MULTIPLIER<br>2 - RTRIM<br>3 - LTRIM<br>4 - TRIM<br>5 - REGEX<br>6 - BOOL\_TO\_DECIMAL<br>7 - OCTAL\_TO\_DECIMAL<br>8 - HEX\_TO\_DECIMAL<br>9 - SIMPLE\_CHANGE (calculated as (received value-previous value))<br>10 - CHANGE\_PER\_SECOND (calculated as (received value-previous value)/(time now-time of last check))<br>11 - XMLPATH<br>12 - JSONPATH<br>13 - IN\_RANGE<br>14 - MATCHES\_REGEX<br>15 - NOT\_MATCHES\_REGEX<br>16 - CHECK\_JSON\_ERROR<br>17 - CHECK\_XML\_ERROR<br>18 - CHECK\_REGEX\_ERROR<br>19 - DISCARD\_UNCHANGED<br>20 - DISCARD\_UNCHANGED\_HEARTBEAT<br>21 - JAVASCRIPT<br>22 - PROMETHEUS\_PATTERN<br>23 - PROMETHEUS\_TO\_JSON<br>24 - CSV\_TO\_JSON<br>25 - STR\_REPLACE<br>26 - CHECK\_NOT\_SUPPORTED<br>27 - XML\_TO\_JSON|Type of the item value preprocessing step.|
|<|parameters|\-|<|<|Root element for parameters of the item value preprocessing step.|
|<|parameter|x|`string`|<|Individual parameter of the item value preprocessing step.|
|<|error\_handler|\-|`string`|0 - ORIGINAL\_ERROR (default)<br>1 - DISCARD\_VALUE<br>2 - CUSTOM\_VALUE<br>3 - CUSTOM\_ERROR|Action type used in case of preprocessing step failure.|
|<|error\_handler\_params|\-|`string`|<|Error handler parameters used with 'error\_handler'.|
|master\_item|<|\-|<|<|Individual item master item.<br><br>Required by dependent items.|
|<|key|x|`string`|<|Dependent item master item key value.<br><br>Recursion up to 3 dependent items and maximum count of dependent items equal to 29999 are allowed.|
|triggers|<|\-|<|<|Root element for simple triggers.|
|<|*For trigger element tag values, see host [trigger tags](/manual/xml_export_import/hosts#host_trigger_tags).*|<|<|<|<|
|tags|<|\-|<|<|Root element for item tags.|
|<|tag|x|`string`|<|Tag name.|
|<|value|\-|`string`|<|Tag value.|

[comment]: # ({/new-8a2e4e2f})

[comment]: # ({new-19e8eae6})
##### Host low-level discovery rule tags

|Element|Element property|Required|Type|Range^**[1](#footnotes)**^|Description|
|-------|----------------|--------|----|--------------------------|-----------|
|discovery\_rules|<|\-|<|<|Root element for low-level discovery rules.|
|<|*For most of the element tag values, see element tag values for a regular item. Only the tags that are specific to low-level discovery rules, are described below.*|<|<|<|<|
|<|type|\-|`string`|0 - ZABBIX\_PASSIVE (default)<br>2 - TRAP<br>3 - SIMPLE<br>5 - INTERNAL<br>7 - ZABBIX\_ACTIVE<br>10 - EXTERNAL<br>11 - ODBC<br>12 - IPMI<br>13 - SSH<br>14 - TELNET<br>16 - JMX<br>18 - DEPENDENT<br>19 - HTTP\_AGENT<br>20 - SNMP\_AGENT|Item type.|
|<|lifetime|\-|`string`|Default: 30d|Time period after which items that are no longer discovered will be deleted. Seconds, time unit with suffix or user macro.|
|filter|<|<|<|<|Individual filter.|
|<|evaltype|\-|`string`|0 - AND\_OR (default)<br>1 - AND<br>2 - OR<br>3 - FORMULA|Logic to use for checking low-level discovery rule filter conditions.|
|<|formula|\-|`string`|<|Custom calculation formula for filter conditions.|
|conditions|<|\-|<|<|Root element for filter conditions.|
|<|macro|x|`string`|<|Low-level discovery macro name.|
|<|value|\-|`string`|<|Filter value: regular expression or global regular expression.|
|<|operator|\-|`string`|8 - MATCHES\_REGEX (default)<br>9 - NOT\_MATCHES\_REGEX|Condition operator.|
|<|formulaid|x|`character`|<|Arbitrary unique ID that is used to reference a condition from the custom expression. Can only contain capital-case letters. The ID must be defined by the user when modifying filter conditions, but will be generated anew when requesting them afterward.|
|lld\_macro\_paths|<|\-|<|<|Root element for LLD macro paths.|
|<|lld\_macro|x|`string`|<|Low-level discovery macro name.|
|<|path|x|`string`|<|Selector for value which will be assigned to the corresponding macro.|
|preprocessing|<|\-|<|<|LLD rule value preprocessing.|
|step|<|\-|<|<|Individual LLD rule value preprocessing step.|
|<|*For most of the element tag values, see element tag values for a host item value preprocessing. Only the tags that are specific to low-level discovery value preprocessing, are described below.*|<|<|<|<|
|<|type|x|`string`|5 - REGEX<br>11 - XMLPATH<br>12 - JSONPATH<br>15 - NOT\_MATCHES\_REGEX<br>16 - CHECK\_JSON\_ERROR<br>17 - CHECK\_XML\_ERROR<br>20 - DISCARD\_UNCHANGED\_HEARTBEAT<br>21 - JAVASCRIPT<br>23 - PROMETHEUS\_TO\_JSON<br>24 - CSV\_TO\_JSON<br>25 - STR\_REPLACE<br>27 - XML\_TO\_JSON|Type of the item value preprocessing step.|
|trigger\_prototypes|<|\-|<|<|Root element for trigger prototypes.|
|<|*For trigger prototype element tag values, see regular [host trigger](/manual/xml_export_import/hosts#host_trigger_tags) tags.*|<|<|<|<|
|graph\_prototypes|<|\-|<|<|Root element for graph prototypes.|
|<|*For graph prototype element tag values, see regular [host graph](/manual/xml_export_import/hosts#host_graph_tags) tags.*|<|<|<|<|
|host\_prototypes|<|\-|<|<|Root element for host prototypes.|
|<|*For host prototype element tag values, see regular [host](/manual/xml_export_import/hosts#host_tags) tags.*|<|<|<|<|
|item\_prototypes|<|\-|<|<|Root element for item prototypes.|
|<|*For item prototype element tag values, see regular [host item](/manual/xml_export_import/hosts#host_item_tags) tags.*|<|<|<|<|
|master\_item|<|\-|<|<|Individual item prototype master item/item prototype data.|
|<|key|x|`string`|<|Dependent item prototype master item/item prototype key value.<br><br>Required for a dependent item.|

[comment]: # ({/new-19e8eae6})

[comment]: # ({new-5ff5f79a})
##### Host trigger tags

|Element|Element property|Required|Type|Range^**[1](#footnotes)**^|Description|
|-------|----------------|--------|----|--------------------------|-----------|
|triggers|<|\-|<|<|Root element for triggers.|
|<|expression|x|`string`|<|Trigger expression.|
|<|recovery\_mode|\-|`string`|0 - EXPRESSION (default)<br>1 - RECOVERY\_EXPRESSION<br>2 - NONE|Basis for generating OK events.|
|<|recovery\_expression|\-|`string`|<|Trigger recovery expression.|
|<|name|x|`string`|<|Trigger name.|
|<|correlation\_mode|\-|`string`|0 - DISABLED (default)<br>1 - TAG\_VALUE|Correlation mode (no event correlation or event correlation by tag).|
|<|correlation\_tag|\-|`string`|<|The tag name to be used for event correlation.|
|<|url|\-|`string`|<|URL associated with the trigger.|
|<|status|\-|`string`|0 - ENABLED (default)<br>1 - DISABLED|Trigger status.|
|<|priority|\-|`string`|0 - NOT\_CLASSIFIED (default)<br>1 - INFO<br>2 - WARNING<br>3 - AVERAGE<br>4 - HIGH<br>5 - DISASTER|Trigger severity.|
|<|description|\-|`text`|<|Trigger description.|
|<|type|\-|`string`|0 - SINGLE (default)<br>1 - MULTIPLE|Event generation type (single problem event or multiple problem events).|
|<|manual\_close|\-|`string`|0 - NO (default)<br>1 - YES|Manual closing of problem events.|
|dependencies|<|\-|<|<|Root element for dependencies.|
|<|name|x|`string`|<|Dependency trigger name.|
|<|expression|x|`string`|<|Dependency trigger expression.|
|<|recovery\_expression|\-|`string`|<|Dependency trigger recovery expression.|
|tags|<|\-|<|<|Root element for event tags.|
|<|tag|x|`string`|<|Tag name.|
|<|value|\-|`string`|<|Tag value.|

[comment]: # ({/new-5ff5f79a})

[comment]: # ({new-0a8a430a})
##### Host graph tags

|Element|Element property|Required|Type|Range^**[1](#footnotes)**^|Description|
|-------|----------------|--------|----|--------------------------|-----------|
|graphs|<|\-|<|<|Root element for graphs.|
|<|name|x|`string`|<|Graph name.|
|<|width|\-|`integer`|20-65535 (default: 900)|Graph width, in pixels. Used for preview and for pie/exploded graphs.|
|<|height|\-|`integer`|20-65535 (default: 200)|Graph height, in pixels. Used for preview and for pie/exploded graphs.|
|<|yaxismin|\-|`double`|Default: 0|Value of Y axis minimum.<br><br>Used if 'ymin\_type\_1' is FIXED.|
|<|yaxismax|\-|`double`|Default: 0|Value of Y axis maximum.<br><br>Used if 'ymax\_type\_1' is FIXED.|
|<|show\_work\_period|\-|`string`|0 - NO<br>1 - YES (default)|Highlight non-working hours.<br><br>Used by normal and stacked graphs.|
|<|show\_triggers|\-|`string`|0 - NO<br>1 - YES (default)|Display simple trigger values as a line.<br><br>Used by normal and stacked graphs.|
|<|type|\-|`string`|0 - NORMAL (default)<br>1 - STACKED<br>2 - PIE<br>3 - EXPLODED|Graph type.|
|<|show\_legend|\-|`string`|0 - NO<br>1 - YES (default)|Display graph legend.|
|<|show\_3d|\-|`string`|0 - NO (default)<br>1 - YES|Enable 3D style.<br><br>Used by pie and exploded pie graphs.|
|<|percent\_left|\-|`double`|Default:0|Show the percentile line for left axis.<br><br>Used only for normal graphs.|
|<|percent\_right|\-|`double`|Default:0|Show the percentile line for right axis.<br><br>Used only for normal graphs.|
|<|ymin\_type\_1|\-|`string`|0 - CALCULATED (default)<br>1 - FIXED<br>2 - ITEM|Minimum value of Y axis.<br><br>Used by normal and stacked graphs.|
|<|ymax\_type\_1|\-|`string`|0 - CALCULATED (default)<br>1 - FIXED<br>2 - ITEM|Maximum value of Y axis.<br><br>Used by normal and stacked graphs.|
|ymin\_item\_1|<|\-|<|<|Individual item details.<br><br>Required if 'ymin\_type\_1' is ITEM.|
|<|host|x|`string`|<|Item host.|
|<|key|x|`string`|<|Item key.|
|ymax\_item\_1|<|\-|<|<|Individual item details.<br><br>Required if 'ymax\_type\_1' is ITEM.|
|<|host|x|`string`|<|Item host.|
|<|key|x|`string`|<|Item key.|
|graph\_items|<|x|<|<|Root element for graph items.|
|<|sortorder|\-|`integer`|<|Draw order. The smaller value is drawn first. Can be used to draw lines or regions behind (or in front of) another.|
|<|drawtype|\-|`string`|0 - SINGLE\_LINE (default)<br>1 - FILLED\_REGION<br>2 - BOLD\_LINE<br>3 - DOTTED\_LINE<br>4 - DASHED\_LINE<br>5 - GRADIENT\_LINE|Draw style of the graph item.<br><br>Used only by normal graphs.|
|<|color|\-|`string`|<|Element color (6 symbols, hex).|
|<|yaxisside|\-|`string`|0 - LEFT (default)<br>1 - RIGHT|Side of the graph where the graph item's Y scale will be drawn.<br><br>Used by normal and stacked graphs.|
|<|calc\_fnc|\-|`string`|1 - MIN<br>2 - AVG (default)<br>4 - MAX<br>7 - ALL (minimum, average and maximum; used only by simple graphs)<br>9 - LAST (used only by pie and exploded pie graphs)|Data to draw if more than one value exists for an item.|
|<|type|\-|`string`|0 - SIMPLE (default)<br>2 - GRAPH\_SUM (value of the item represents the whole pie; used only by pie and exploded pie graphs)|Graph item type.|
|item|<|x|<|<|Individual item.|
|<|host|x|`string`|<|Item host.|
|<|key|x|`string`|<|Item key.|

[comment]: # ({/new-0a8a430a})

[comment]: # ({new-cae5eb15})
##### Host web scenario tags

|Element|Element property|Required|Type|Range^**[1](#footnotes)**^|Description|
|-------|----------------|--------|----|--------------------------|-----------|
|httptests|<|\-|<|<|Root element for web scenarios.|
|<|name|x|`string`|<|Web scenario name.|
|<|delay|\-|`string`|Default: 1m|Frequency of executing the web scenario. Seconds, time unit with suffix or user macro.|
|<|attempts|\-|`integer`|1-10 (default: 1)|The number of attempts for executing web scenario steps.|
|<|agent|\-|`string`|Default: Zabbix|Client agent. Zabbix will pretend to be the selected browser. This is useful when a website returns different content for different browsers.|
|<|http\_proxy|\-|`string`|<|Specify an HTTP proxy to use, using the format: `http://[username[:password]@]proxy.example.com[:port]`|
|variables|<|\-|<|<|Root element for scenario-level variables (macros) that may be used in scenario steps.|
|<|name|x|`text`|<|Variable name.|
|<|value|x|`text`|<|Variable value.|
|headers|<|\-|<|<|Root element for HTTP headers that will be sent when performing a request. Headers should be listed using the same syntax as they would appear in the HTTP protocol.|
|<|name|x|`text`|<|Header name.|
|<|value|x|`text`|<|Header value.|
|<|status|\-|`string`|0 - ENABLED (default)<br>1 - DISABLED|Web scenario status.|
|<|authentication|\-|`string`|0 - NONE (default)<br>1 - BASIC<br>2 - NTLM|Authentication method.|
|<|http\_user|\-|`string`|<|User name used for basic, HTTP or NTLM authentication.|
|<|http\_password|\-|`string`|<|Password used for basic, HTTP or NTLM authentication.|
|<|verify\_peer|\-|`string`|0 - NO (default)<br>1 - YES|Verify the SSL certificate of the web server.|
|<|verify\_host|\-|`string`|0 - NO (default)<br>1 - YES|Verify that the Common Name field or the Subject Alternate Name field of the web server certificate matches.|
|<|ssl\_cert\_file|\-|`string`|<|Name of the SSL certificate file used for client authentication (must be in PEM format).|
|<|ssl\_key\_file|\-|`string`|<|Name of the SSL private key file used for client authentication (must be in PEM format).|
|<|ssl\_key\_password|\-|`string`|<|SSL private key file password.|
|steps|<|x|<|<|Root element for web scenario steps.|
|<|name|x|`string`|<|Web scenario step name.|
|<|url|x|`string`|<|URL for monitoring.|
|query\_fields|<|\-|<|<|Root element for query fields - an array of HTTP fields that will be added to the URL when performing a request.|
|<|name|x|`string`|<|Query field name.|
|<|value|\-|`string`|<|Query field value.|
|posts|<|\-|<|<|HTTP POST variables as a string (raw post data) or as an array of HTTP fields (form field data).|
|<|name|x|`string`|<|Post field name.|
|<|value|x|`string`|<|Post field value.|
|variables|<|\-|<|<|Root element of step-level variables (macros) that should be applied after this step.<br><br>If the variable value has a 'regex:' prefix, then its value is extracted from the data returned by this step according to the regular expression pattern following the 'regex:' prefix|
|<|name|x|`string`|<|Variable name.|
|<|value|x|`string`|<|Variable value.|
|headers|<|\-|<|<|Root element for HTTP headers that will be sent when performing a request. Headers should be listed using the same syntax as they would appear in the HTTP protocol.|
|<|name|x|`string`|<|Header name.|
|<|value|x|`string`|<|Header value.|
|<|follow\_redirects|\-|`string`|0 - NO<br>1 - YES (default)|Follow HTTP redirects.|
|<|retrieve\_mode|\-|`string`|0 - BODY (default)<br>1 - HEADERS<br>2 - BOTH|HTTP response retrieve mode.|
|<|timeout|\-|`string`|Default: 15s|Timeout of step execution. Seconds, time unit with suffix or user macro.|
|<|required|\-|`string`|<|Text that must be present in the response. Ignored if empty.|
|<|status\_codes|\-|`string`|<|A comma delimited list of accepted HTTP status codes. Ignored if empty. For example: 200-201,210-299|
|tags|<|\-|<|<|Root element for web scenario tags.|
|<|tag|x|`string`|<|Tag name.|
|<|value|\-|`string`|<|Tag value.|

[comment]: # ({/new-cae5eb15})

[comment]: # ({new-869bd76e})
##### Footnotes

^**1**^ For string values, only the string will be exported (e.g.
"ZABBIX\_ACTIVE") without the numbering used in this table. The numbers
for range values (corresponding to the API values) in this table is used
for ordering only.

[comment]: # ({/new-869bd76e})
