[comment]: # translation:outdated

[comment]: # ({new-e8fbaf7c})
# 2 Templates

[comment]: # ({/new-e8fbaf7c})

[comment]: # ({new-02fec82c})
#### Overview

Templates are [exported](/manual/xml_export_import) with many related
objects and object relations.

Template export contains:

-   linked host groups
-   template data
-   linkage to other templates
-   linkage to host groups
-   directly linked items
-   directly linked triggers
-   directly linked graphs
-   directly linked dashboards
-   directly linked discovery rules with all prototypes
-   directly linked web scenarios
-   value maps

[comment]: # ({/new-02fec82c})

[comment]: # ({new-ca3e35f8})
#### Exporting

To export templates, do the following:

-   Go to: *Configuration* → *Templates*
-   Mark the checkboxes of the templates to export
-   Click on *Export* below the list

![](../../../assets/en/manual/xml_export_import/export_templates.png)

Depending on the selected format, templates are exported to a local file
with a default name:

-   *zabbix\_export\_templates.yaml* - in YAML export (default option
    for export)
-   *zabbix\_export\_templates.xml* - in XML export
-   *zabbix\_export\_templates.json* - in JSON export

[comment]: # ({/new-ca3e35f8})

[comment]: # ({new-78f6f939})
#### Importing

To import templates, do the following:

-   Go to: *Configuration* → *Templates*
-   Click on *Import* to the right
-   Select the import file
-   Mark the required options in import rules
-   Click on *Import*

![](../../../assets/en/manual/xml_export_import/import_templates.png)

All mandatory input fields are marked with a red asterisk.

Import rules:

|Rule|Description|
|----|-----------|
|*Update existing*|Existing elements will be updated with data taken from the import file. Otherwise, they will not be updated.|
|*Create new*|The import will add new elements using data from the import file. Otherwise, it will not add them.|
|*Delete missing*|The import will remove existing elements not present in the import file. Otherwise, it will not remove them.<br>If *Delete missing* is marked for template linkage, existing template linkage not present in the import file will be removed from the template along with all entities inherited from the potentially unlinked templates (items, triggers, etc).|

On the next screen, you will be able to view the content of a template
being imported. If this is a new template all elements will be listed in
green. If updating an existing template, new template elements are
highlighted in green; removed template elements are highlighted in red;
elements that have not changed are listed on a gray background.

![](../../../assets/en/manual/xml_export_import/import_templates1.png){width="600"}

The menu on the left can be used to navigate through the list of
changes. Section *Updated* highlights all changes made to existing
template elements. Section *Added* lists new template elements. The
elements in each section are grouped by element type; press on the gray
arrow down to expand or collapse the group of elements.

![](../../../assets/en/manual/xml_export_import/templates_import_menu.png)

Review template changes, then press *Import* to perform template import.
A success or failure message of the import will be displayed in the
frontend.

[comment]: # ({/new-78f6f939})

[comment]: # ({new-c04b1514})
#### Export format

Export format in YAML:

``` {.yaml}
zabbix_export:
  version: '6.0'
  date: '2021-08-31T12:40:55Z'
  groups:
    -
      uuid: a571c0d144b14fd4a87a9d9b2aa9fcd6
      name: Templates/Applications
  templates:
    -
      uuid: 56079badd056419383cc26e6a4fcc7e0
      template: VMware
      name: VMware
      description: |
        You can discuss this template or leave feedback on our forum https://www.zabbix.com/forum/zabbix-suggestions-and-feedback/
        
        Template tooling version used: 0.38
      templates:
        -
          name: 'VMware macros'
      groups:
        -
          name: Templates/Applications
      items:
        -
          uuid: 5ce209f4d94f460488a74a92a52d92b1
          name: 'VMware: Event log'
          type: SIMPLE
          key: 'vmware.eventlog[{$VMWARE.URL},skip]'
          history: 7d
          trends: '0'
          value_type: LOG
          username: '{$VMWARE.USERNAME}'
          password: '{$VMWARE.PASSWORD}'
          description: 'Collect VMware event log. See also: https://www.zabbix.com/documentation/6.0/manual/config/items/preprocessing/examples#filtering_vmware_event_log_records'
          tags:
            -
              tag: Application
              value: VMware
        -
          uuid: ee2edadb8ce943ef81d25dbbba8667a4
          name: 'VMware: Full name'
          type: SIMPLE
          key: 'vmware.fullname[{$VMWARE.URL}]'
          delay: 1h
          history: 7d
          trends: '0'
          value_type: CHAR
          username: '{$VMWARE.USERNAME}'
          password: '{$VMWARE.PASSWORD}'
          description: 'VMware service full name.'
          preprocessing:
            -
              type: DISCARD_UNCHANGED_HEARTBEAT
              parameters:
                - 1d
          tags:
            -
              tag: Application
              value: VMware
        -
          uuid: a0ec9145f2234fbea79a28c57ebdb44d
          name: 'VMware: Version'
          type: SIMPLE
          key: 'vmware.version[{$VMWARE.URL}]'
          delay: 1h
          history: 7d
          trends: '0'
          value_type: CHAR
          username: '{$VMWARE.USERNAME}'
          password: '{$VMWARE.PASSWORD}'
          description: 'VMware service version.'
          preprocessing:
            -
              type: DISCARD_UNCHANGED_HEARTBEAT
              parameters:
                - 1d
          tags:
            -
              tag: Application
              value: VMware
      discovery_rules:
        -
          uuid: 16ffc933cce74cf28a6edf306aa99782
          name: 'Discover VMware clusters'
          type: SIMPLE
          key: 'vmware.cluster.discovery[{$VMWARE.URL}]'
          delay: 1h
          username: '{$VMWARE.USERNAME}'
          password: '{$VMWARE.PASSWORD}'
          description: 'Discovery of clusters'
          item_prototypes:
            -
              uuid: 46111f91dd564a459dbc1d396e2e6c76
              name: 'VMware: Status of "{#CLUSTER.NAME}" cluster'
              type: SIMPLE
              key: 'vmware.cluster.status[{$VMWARE.URL},{#CLUSTER.NAME}]'
              history: 7d
              username: '{$VMWARE.USERNAME}'
              password: '{$VMWARE.PASSWORD}'
              description: 'VMware cluster status.'
              valuemap:
                name: 'VMware status'
              tags:
                -
                  tag: Application
                  value: VMware
        -
          uuid: 8fb6a45cbe074b0cb6df53758e2c6623
          name: 'Discover VMware datastores'
          type: SIMPLE
          key: 'vmware.datastore.discovery[{$VMWARE.URL}]'
          delay: 1h
          username: '{$VMWARE.USERNAME}'
          password: '{$VMWARE.PASSWORD}'
          item_prototypes:
            -
              uuid: 4b61838ba4c34e709b25081ae5b059b5
              name: 'VMware: Average read latency of the datastore {#DATASTORE}'
              type: SIMPLE
              key: 'vmware.datastore.read[{$VMWARE.URL},{#DATASTORE},latency]'
              history: 7d
              username: '{$VMWARE.USERNAME}'
              password: '{$VMWARE.PASSWORD}'
              description: 'Amount of time for a read operation from the datastore (milliseconds).'
              tags:
                -
                  tag: Application
                  value: VMware
            -
              uuid: 5355c401dc244bc588ccd18767577c93
              name: 'VMware: Free space on datastore {#DATASTORE} (percentage)'
              type: SIMPLE
              key: 'vmware.datastore.size[{$VMWARE.URL},{#DATASTORE},pfree]'
              delay: 5m
              history: 7d
              value_type: FLOAT
              units: '%'
              username: '{$VMWARE.USERNAME}'
              password: '{$VMWARE.PASSWORD}'
              description: 'VMware datastore space in percentage from total.'
              tags:
                -
                  tag: Application
                  value: VMware
            -
              uuid: 84f13c4fde2d4a17baaf0c8c1eb4f2c0
              name: 'VMware: Total size of datastore {#DATASTORE}'
              type: SIMPLE
              key: 'vmware.datastore.size[{$VMWARE.URL},{#DATASTORE}]'
              delay: 5m
              history: 7d
              units: B
              username: '{$VMWARE.USERNAME}'
              password: '{$VMWARE.PASSWORD}'
              description: 'VMware datastore space in bytes.'
              tags:
                -
                  tag: Application
                  value: VMware
            -
              uuid: 540cd0fbc56c4b8ea19f2ff5839ce00d
              name: 'VMware: Average write latency of the datastore {#DATASTORE}'
              type: SIMPLE
              key: 'vmware.datastore.write[{$VMWARE.URL},{#DATASTORE},latency]'
              history: 7d
              username: '{$VMWARE.USERNAME}'
              password: '{$VMWARE.PASSWORD}'
              description: 'Amount of time for a write operation to the datastore (milliseconds).'
              tags:
                -
                  tag: Application
                  value: VMware
        -
          uuid: a5bc075e89f248e7b411d8f960897a08
          name: 'Discover VMware hypervisors'
          type: SIMPLE
          key: 'vmware.hv.discovery[{$VMWARE.URL}]'
          delay: 1h
          username: '{$VMWARE.USERNAME}'
          password: '{$VMWARE.PASSWORD}'
          description: 'Discovery of hypervisors.'
          host_prototypes:
            -
              uuid: 051a1469d4d045cbbf818fcc843a352e
              host: '{#HV.UUID}'
              name: '{#HV.NAME}'
              group_links:
                -
                  group:
                    name: Templates/Applications
              group_prototypes:
                -
                  name: '{#CLUSTER.NAME}'
                -
                  name: '{#DATACENTER.NAME}'
              templates:
                -
                  name: 'VMware Hypervisor'
              macros:
                -
                  macro: '{$VMWARE.HV.UUID}'
                  value: '{#HV.UUID}'
                  description: 'UUID of hypervisor.'
              custom_interfaces: 'YES'
              interfaces:
                -
                  ip: '{#HV.IP}'
        -
          uuid: 9fd559f4e88c4677a1b874634dd686f5
          name: 'Discover VMware VMs'
          type: SIMPLE
          key: 'vmware.vm.discovery[{$VMWARE.URL}]'
          delay: 1h
          username: '{$VMWARE.USERNAME}'
          password: '{$VMWARE.PASSWORD}'
          description: 'Discovery of guest virtual machines.'
          host_prototypes:
            -
              uuid: 23b9ae9d6f33414880db1cb107115810
              host: '{#VM.UUID}'
              name: '{#VM.NAME}'
              group_links:
                -
                  group:
                    name: Templates/Applications
              group_prototypes:
                -
                  name: '{#CLUSTER.NAME} (vm)'
                -
                  name: '{#DATACENTER.NAME}/{#VM.FOLDER} (vm)'
                -
                  name: '{#HV.NAME}'
              templates:
                -
                  name: 'VMware Guest'
              macros:
                -
                  macro: '{$VMWARE.VM.UUID}'
                  value: '{#VM.UUID}'
                  description: 'UUID of guest virtual machine.'
              custom_interfaces: 'YES'
              interfaces:
                -
                  ip: '{#VM.IP}'
      valuemaps:
        -
          uuid: 3c59c22905054d42ac4ee8b72fe5f270
          name: 'VMware status'
          mappings:
            -
              value: '0'
              newvalue: gray
            -
              value: '1'
              newvalue: green
            -
              value: '2'
              newvalue: yellow
            -
              value: '3'
              newvalue: red
```

[comment]: # ({/new-c04b1514})

[comment]: # ({new-2c61d3f8})
#### Element tags

Element tag values are explained in the table below.

[comment]: # ({/new-2c61d3f8})

[comment]: # ({new-bdfd38fc})
##### Template tags

|Element|Element property|Required|Type|Range|Description|
|-------|----------------|--------|----|-----|-----------|
|templates|<|\-|<|<|Root element for templates.|
|<|uuid|x|`string`|<|Unique identifier for this template.|
|<|template|x|`string`|<|Unique template name.|
|<|name|\-|`string`|<|Visible template name.|
|<|description|\-|`text`|<|Template description.|
|groups|<|x|<|<|Root element for template host groups.|
|<|uuid|x|`string`|<|Unique identifier for this host group.|
|<|name|x|`string`|<|Host group name.|
|templates|<|\-|<|<|Root element for linked templates.|
|<|name|x|`string`|<|Template name.|
|tags|<|\-|<|<|Root element for template tags.|
|<|tag|x|`string`|<|Tag name.|
|<|value|\-|`string`|<|Tag value.|
|macros|<|\-|<|<|Root element for template user macros.|
|<|macro|x|`string`|<|User macro name.|
|<|type|\-|`string`|0 - TEXT (default)<br>1 - SECRET\_TEXT<br>2 - VAULT|Type of the macro.|
|<|value|\-|`string`|<|User macro value.|
|<|description|\-|`string`|<|User macro description.|
|valuemaps|<|\-|<|<|Root element for template value maps.|
|<|uuid|x|`string`|<|Unique identifier for this value map.|
|<|name|x|`string`|<|Value map name.|
|<|mapping|\-|<|<|Root element for mappings.|
|<|value|x|`string`|<|Value of a mapping.|
|<|newvalue|x|`string`|<|New value of a mapping.|

[comment]: # ({/new-bdfd38fc})

[comment]: # ({new-18631c1b})
##### Template item tags

|Element|Element property|Required|Type|Range^**[1](#footnotes)**^|Description|
|-------|----------------|--------|----|--------------------------|-----------|
|items|<|\-|<|<|Root element for items.|
|<|uuid|x|`string`|<|Unique identifier for the item.|
|<|name|x|`string`|<|Item name.|
|<|type|\-|`string`|0 - ZABBIX\_PASSIVE (default)<br>2 - TRAP<br>3 - SIMPLE<br>5 - INTERNAL<br>7 - ZABBIX\_ACTIVE<br>10 - EXTERNAL<br>11 - ODBC<br>12 - IPMI<br>13 - SSH<br>14 - TELNET<br>15 - CALCULATED<br>16 - JMX<br>17 - SNMP\_TRAP<br>18 - DEPENDENT<br>19 - HTTP\_AGENT<br>20 - SNMP\_AGENT<br>21 - ITEM\_TYPE\_SCRIPT|Item type.|
|<|snmp\_oid|\-|`string`|<|SNMP object ID.<br><br>Required by SNMP items.|
|<|key|x|`string`|<|Item key.|
|<|delay|\-|`string`|Default: 1m|Update interval of the item.<br><br>Accepts seconds or a time unit with suffix (30s, 1m, 2h, 1d).<br>Optionally one or more [custom intervals](/manual/config/items/item/custom_intervals) can be specified either as flexible intervals or scheduling.<br>Multiple intervals are separated by a semicolon.<br>User macros may be used. A single macro has to fill the whole field. Multiple macros in a field or macros mixed with text are not supported.<br>Flexible intervals may be written as two macros separated by a forward slash (e.g. `{$FLEX_INTERVAL}/{$FLEX_PERIOD}`).|
|<|history|\-|`string`|Default: 90d|A time unit of how long the history data should be stored. Time unit with suffix, user macro or LLD macro.|
|<|trends|\-|`string`|Default: 365d|A time unit of how long the trends data should be stored. Time unit with suffix, user macro or LLD macro.|
|<|status|\-|`string`|0 - ENABLED (default)<br>1 - DISABLED|Item status.|
|<|value\_type|\-|`string`|0 - FLOAT<br>1 - CHAR<br>2 - LOG<br>3 - UNSIGNED (default)<br>4 - TEXT|Received value type.|
|<|allowed\_hosts|\-|`string`|<|List of IP addresses (comma delimited) of hosts allowed sending data for the item.<br><br>Used by trapper and HTTP agent items.|
|<|units|\-|`string`|<|Units of returned values (bps, B, etc).|
|<|params|\-|`text`|<|Additional parameters depending on the type of the item:<br>- executed script for Script, SSH and Telnet items;<br>- SQL query for database monitor items;<br>- formula for calculated items.|
|<|ipmi\_sensor|\-|`string`|<|IPMI sensor.<br><br>Used only by IPMI items.|
|<|authtype|\-|`string`|Authentication type for SSH agent items:<br>0 - PASSWORD (default)<br>1 - PUBLIC\_KEY<br><br>Authentication type for HTTP agent items:<br>0 - NONE (default)<br>1 - BASIC<br>2 - NTLM|Authentication type.<br><br>Used only by SSH and HTTP agent items.|
|<|username|\-|`string`|<|Username for authentication.<br>Used by simple check, SSH, Telnet, database monitor, JMX and HTTP agent items.<br><br>Required by SSH and Telnet items.<br>When used by JMX agent, password should also be specified together with the username or both properties should be left blank.|
|<|password|\-|`string`|<|Password for authentication.<br>Used by simple check, SSH, Telnet, database monitor, JMX and HTTP agent items.<br><br>When used by JMX agent, username should also be specified together with the password or both properties should be left blank.|
|<|publickey|\-|`string`|<|Name of the public key file.<br><br>Required for SSH agent items.|
|<|privatekey|\-|`string`|<|Name of the private key file.<br><br>Required for SSH agent items.|
|<|port|\-|`string`|<|Custom port monitored by the item.<br>Can contain user macros.<br><br>Used only by SNMP items.|
|<|description|\-|`text`|<|Item description.|
|<|inventory\_link|\-|`string`|0 - NONE<br><br>Capitalized host inventory field name. For example:<br>4 - ALIAS<br>6 - OS\_FULL<br>14 - HARDWARE<br>etc.|Host inventory field that is populated by the item.<br><br>Refer to the [host inventory page](/manual/api/reference/host/object#host_inventory) for a list of supported host inventory fields and their IDs.|
|<|logtimefmt|\-|`string`|<|Format of the time in log entries.<br>Used only by log items.|
|<|jmx\_endpoint|\-|`string`|<|JMX endpoint.<br><br>Used only by JMX agent items.|
|<|url|\-|`string`|<|URL string.<br><br>Required only for HTTP agent items.|
|<|allow\_traps|\-|`string`|0 - NO (default)<br>1 - YES|Allow to populate value as in a trapper item.<br><br>Used only by HTTP agent items.|
|<|follow\_redirects|\-|`string`|0 - NO<br>1 - YES (default)|Follow HTTP response redirects while pooling data.<br><br>Used only by HTTP agent items.|
|headers|<|\-|<|<|Root element for HTTP(S) request headers, where header name is used as key and header value as value.<br>Used only by HTTP agent items.|
|<|name|x|`string`|<|Header name.|
|<|value|x|`string`|<|Header value.|
|<|http\_proxy|\-|`string`|<|HTTP(S) proxy connection string.<br><br>Used only by HTTP agent items.|
|<|output\_format|\-|`string`|0 - RAW (default)<br>1 - JSON|How to process response.<br><br>Used only by HTTP agent items.|
|<|post\_type|\-|`string`|0 - RAW (default)<br>2 - JSON<br>3 - XML|Type of post data body.<br><br>Used only by HTTP agent items.|
|<|posts|\-|`string`|<|HTTP(S) request body data.<br><br>Used only by HTTP agent items.|
|query\_fields|<|\-|<|<|Root element for query parameters.<br><br>Used only by HTTP agent items.|
|<|name|x|`string`|<|Parameter name.|
|<|value|\-|`string`|<|Parameter value.|
|<|request\_method|\-|`string`|0 - GET (default)<br>1 - POST<br>2 - PUT<br>3 - HEAD|Request method.<br><br>Used only by HTTP agent items.|
|<|retrieve\_mode|\-|`string`|0 - BODY (default)<br>1 - HEADERS<br>2 - BOTH|What part of response should be stored.<br><br>Used only by HTTP agent items.|
|<|ssl\_cert\_file|\-|`string`|<|Public SSL Key file path.<br><br>Used only by HTTP agent items.|
|<|ssl\_key\_file|\-|`string`|<|Private SSL Key file path.<br><br>Used only by HTTP agent items.|
|<|ssl\_key\_password|\-|`string`|<|Password for SSL Key file.<br><br>Used only by HTTP agent items.|
|<|status\_codes|\-|`string`|<|Ranges of required HTTP status codes separated by commas. Supports user macros.<br>Example: 200,200-{$M},{$M},200-400<br><br>Used only by HTTP agent items.|
|<|timeout|\-|`string`|<|Item data polling request timeout. Supports user macros.<br><br>Used by HTTP agent and Script items.|
|<|verify\_host|\-|`string`|0 - NO (default)<br>1 - YES|Validate if host name in URL is in Common Name field or a Subject Alternate Name field of host certificate.<br><br>Used only by HTTP agent items.|
|<|verify\_peer|\-|`string`|0 - NO (default)<br>1 - YES|Validate if host certificate is authentic.<br><br>Used only by HTTP agent items.|
|parameters|<|\-|<|<|Root element for user-defined parameters.<br><br>Used only by Script items.|
|<|name|x|`string`|<|Parameter name.<br><br>Used only by Script items.|
|<|value|\-|`string`|<|Parameter value.<br><br>Used only by Script items.|
|value map|<|\-|<|<|Value map.|
|<|name|x|`string`|<|Name of the value map to use for the item.|
|preprocessing|<|\-|<|<|Root element for item value preprocessing.|
|step|<|\-|<|<|Individual item value preprocessing step.|
|<|type|x|`string`|1 - MULTIPLIER<br>2 - RTRIM<br>3 - LTRIM<br>4 - TRIM<br>5 - REGEX<br>6 - BOOL\_TO\_DECIMAL<br>7 - OCTAL\_TO\_DECIMAL<br>8 - HEX\_TO\_DECIMAL<br>9 - SIMPLE\_CHANGE (calculated as (received value-previous value))<br>10 - CHANGE\_PER\_SECOND (calculated as (received value-previous value)/(time now-time of last check))<br>11 - XMLPATH<br>12 - JSONPATH<br>13 - IN\_RANGE<br>14 - MATCHES\_REGEX<br>15 - NOT\_MATCHES\_REGEX<br>16 - CHECK\_JSON\_ERROR<br>17 - CHECK\_XML\_ERROR<br>18 - CHECK\_REGEX\_ERROR<br>19 - DISCARD\_UNCHANGED<br>20 - DISCARD\_UNCHANGED\_HEARTBEAT<br>21 - JAVASCRIPT<br>22 - PROMETHEUS\_PATTERN<br>23 - PROMETHEUS\_TO\_JSON<br>24 - CSV\_TO\_JSON<br>25 - STR\_REPLACE<br>26 - CHECK\_NOT\_SUPPORTED|Type of the item value preprocessing step.|
|<|parameters|\-|<|<|Root element for parameters of the item value preprocessing step.|
|<|parameter|x|`string`|<|Individual parameter of the item value preprocessing step.|
|<|error\_handler|\-|`string`|0 - ORIGINAL\_ERROR (default)<br>1 - DISCARD\_VALUE<br>2 - CUSTOM\_VALUE<br>3 - CUSTOM\_ERROR|Action type used in case of preprocessing step failure.|
|<|error\_handler\_params|\-|`string`|<|Error handler parameters used with 'error\_handler'.|
|master\_item|<|\-|<|<|Individual item master item.<br><br>Required by dependent items.|
|<|key|x|`string`|<|Dependent item master item key value.<br><br>Recursion up to 3 dependent items and maximum count of dependent items equal to 29999 are allowed.|
|triggers|<|\-|<|<|Root element for simple triggers.|
|<|*For trigger element tag values, see template [trigger tags](/manual/xml_export_import/templates#template_trigger_tags).*|<|<|<|<|
|tags|<|\-|<|<|Root element for item tags.|
|<|tag|x|`string`|<|Tag name.|
|<|value|\-|`string`|<|Tag value.|

[comment]: # ({/new-18631c1b})

[comment]: # ({new-2fdd304e})
##### Template low-level discovery rule tags

|Element|Element property|Required|Type|Range|Description|
|-------|----------------|--------|----|-----|-----------|
|discovery\_rules|<|\-|<|<|Root element for low-level discovery rules.|
|<|*For most of the element tag values, see element tag values for a regular item. Only the tags that are specific to low-level discovery rules, are described below.*|<|<|<|<|
|<|type|\-|`string`|0 - ZABBIX\_PASSIVE (default)<br>2 - TRAP<br>3 - SIMPLE<br>5 - INTERNAL<br>7 - ZABBIX\_ACTIVE<br>10 - EXTERNAL<br>11 - ODBC<br>12 - IPMI<br>13 - SSH<br>14 - TELNET<br>16 - JMX<br>18 - DEPENDENT<br>19 - HTTP\_AGENT<br>20 - SNMP\_AGENT|Item type.|
|<|lifetime|\-|`string`|Default: 30d|Time period after which items that are no longer discovered will be deleted. Seconds, time unit with suffix or user macro.|
|filter|<|<|<|<|Individual filter.|
|<|evaltype|\-|`string`|0 - AND\_OR (default)<br>1 - AND<br>2 - OR<br>3 - FORMULA|Logic to use for checking low-level discovery rule filter conditions.|
|<|formula|\-|`string`|<|Custom calculation formula for filter conditions.|
|conditions|<|\-|<|<|Root element for filter conditions.|
|<|macro|x|`string`|<|Low-level discovery macro name.|
|<|value|\-|`string`|<|Filter value: regular expression or global regular expression.|
|<|operator|\-|`string`|8 - MATCHES\_REGEX (default)<br>9 - NOT\_MATCHES\_REGEX|Condition operator.|
|<|formulaid|x|`character`|<|Arbitrary unique ID that is used to reference a condition from the custom expression. Can only contain capital-case letters. The ID must be defined by the user when modifying filter conditions, but will be generated anew when requesting them afterward.|
|lld\_macro\_paths|<|\-|<|<|Root element for LLD macro paths.|
|<|lld\_macro|x|`string`|<|Low-level discovery macro name.|
|<|path|x|`string`|<|Selector for value which will be assigned to the corresponding macro.|
|preprocessing|<|\-|<|<|LLD rule value preprocessing.|
|step|<|\-|<|<|Individual LLD rule value preprocessing step.|
|<|*For most of the element tag values, see element tag values for a template item value preprocessing. Only the tags that are specific to template low-level discovery value preprocessing, are described below.*|<|<|<|<|
|<|type|x|`string`|5 - REGEX<br>11 - XMLPATH<br>12 - JSONPATH<br>15 - NOT\_MATCHES\_REGEX<br>16 - CHECK\_JSON\_ERROR<br>17 - CHECK\_XML\_ERROR<br>20 - DISCARD\_UNCHANGED\_HEARTBEAT<br>21 - JAVASCRIPT<br>23 - PROMETHEUS\_TO\_JSON<br>24 - CSV\_TO\_JSON<br>25 - STR\_REPLACE|Type of the item value preprocessing step.|
|trigger\_prototypes|<|\-|<|<|Root element for trigger prototypes.|
|<|*For trigger prototype element tag values, see regular [template trigger](/manual/xml_export_import/templates#template_trigger_tags) tags.*|<|<|<|<|
|graph\_prototypes|<|\-|<|<|Root element for graph prototypes.|
|<|*For graph prototype element tag values, see regular [template graph](/manual/xml_export_import/templates#template_graph_tags) tags.*|<|<|<|<|
|host\_prototypes|<|\-|<|<|Root element for host prototypes.|
|<|*For host prototype element tag values, see regular [host](/manual/xml_export_import/hosts#host_tags) tags.*|<|<|<|<|
|item\_prototypes|<|\-|<|<|Root element for item prototypes.|
|<|*For item prototype element tag values, see regular [template item](/manual/xml_export_import/templates#template_item_tags) tags.*|<|<|<|<|
|master\_item|<|\-|<|<|Individual item prototype master item/item prototype data.|
|<|key|x|`string`|<|Dependent item prototype master item/item prototype key value.<br><br>Required for a dependent item.|

[comment]: # ({/new-2fdd304e})

[comment]: # ({new-8aada697})
##### Template trigger tags

|Element|Element property|Required|Type|Range^**[1](#footnotes)**^|Description|
|-------|----------------|--------|----|--------------------------|-----------|
|triggers|<|\-|<|<|Root element for triggers.|
|<|uuid|x|`string`|<|Unique identifier for this trigger.|
|<|expression|x|`string`|<|Trigger expression.|
|<|recovery\_mode|\-|`string`|0 - EXPRESSION (default)<br>1 - RECOVERY\_EXPRESSION<br>2 - NONE|Basis for generating OK events.|
|<|recovery\_expression|\-|`string`|<|Trigger recovery expression.|
|<|name|x|`string`|<|Trigger name.|
|<|correlation\_mode|\-|`string`|0 - DISABLED (default)<br>1 - TAG\_VALUE|Correlation mode (no event correlation or event correlation by tag).|
|<|correlation\_tag|\-|`string`|<|The tag name to be used for event correlation.|
|<|url|\-|`string`|<|URL associated with the trigger.|
|<|status|\-|`string`|0 - ENABLED (default)<br>1 - DISABLED|Trigger status.|
|<|priority|\-|`string`|0 - NOT\_CLASSIFIED (default)<br>1 - INFO<br>2 - WARNING<br>3 - AVERAGE<br>4 - HIGH<br>5 - DISASTER|Trigger severity.|
|<|description|\-|`text`|<|Trigger description.|
|<|type|\-|`string`|0 - SINGLE (default)<br>1 - MULTIPLE|Event generation type (single problem event or multiple problem events).|
|<|manual\_close|\-|`string`|0 - NO (default)<br>1 - YES|Manual closing of problem events.|
|dependencies|<|\-|<|<|Root element for dependencies.|
|<|name|x|`string`|<|Dependency trigger name.|
|<|expression|x|`string`|<|Dependency trigger expression.|
|<|recovery\_expression|\-|`string`|<|Dependency trigger recovery expression.|
|tags|<|\-|<|<|Root element for trigger tags.|
|<|tag|x|`string`|<|Tag name.|
|<|value|\-|`string`|<|Tag value.|

[comment]: # ({/new-8aada697})

[comment]: # ({new-eec993d5})
##### Template graph tags

|Element|Element property|Required|Type|Range^**[1](#footnotes)**^|Description|
|-------|----------------|--------|----|--------------------------|-----------|
|graphs|<|\-|<|<|Root element for graphs.|
|<|uuid|x|`string`|<|Unique identifier for this graph.|
|<|name|x|`string`|<|Graph name.|
|<|width|\-|`integer`|20-65535 (default: 900)|Graph width, in pixels. Used for preview and for pie/exploded graphs.|
|<|height|\-|`integer`|20-65535 (default: 200)|Graph height, in pixels. Used for preview and for pie/exploded graphs.|
|<|yaxismin|\-|`double`|Default: 0|Value of Y axis minimum.<br><br>Used if 'ymin\_type\_1' is FIXED.|
|<|yaxismax|\-|`double`|Default: 0|Value of Y axis maximum.<br><br>Used if 'ymax\_type\_1' is FIXED.|
|<|show\_work\_period|\-|`string`|0 - NO<br>1 - YES (default)|Highlight non-working hours.<br><br>Used by normal and stacked graphs.|
|<|show\_triggers|\-|`string`|0 - NO<br>1 - YES (default)|Display simple trigger values as a line.<br><br>Used by normal and stacked graphs.|
|<|type|\-|`string`|0 - NORMAL (default)<br>1 - STACKED<br>2 - PIE<br>3 - EXPLODED|Graph type.|
|<|show\_legend|\-|`string`|0 - NO<br>1 - YES (default)|Display graph legend.|
|<|show\_3d|\-|`string`|0 - NO (default)<br>1 - YES|Enable 3D style.<br><br>Used by pie and exploded pie graphs.|
|<|percent\_left|\-|`double`|Default:0|Show the percentile line for left axis.<br><br>Used only for normal graphs.|
|<|percent\_right|\-|`double`|Default:0|Show the percentile line for right axis.<br><br>Used only for normal graphs.|
|<|ymin\_type\_1|\-|`string`|0 - CALCULATED (default)<br>1 - FIXED<br>2 - ITEM|Minimum value of Y axis.<br><br>Used by normal and stacked graphs.|
|<|ymax\_type\_1|\-|`string`|0 - CALCULATED (default)<br>1 - FIXED<br>2 - ITEM|Maximum value of Y axis.<br><br>Used by normal and stacked graphs.|
|ymin\_item\_1|<|\-|<|<|Individual item details.<br><br>Required if 'ymin\_type\_1' is ITEM.|
|<|host|x|`string`|<|Item host.|
|<|key|x|`string`|<|Item key.|
|ymax\_item\_1|<|\-|<|<|Individual item details.<br><br>Required if 'ymax\_type\_1' is ITEM.|
|<|host|x|`string`|<|Item host.|
|<|key|x|`string`|<|Item key.|
|graph\_items|<|x|<|<|Root element for graph items.|
|<|sortorder|\-|`integer`|<|Draw order. The smaller value is drawn first. Can be used to draw lines or regions behind (or in front of) another.|
|<|drawtype|\-|`string`|0 - SINGLE\_LINE (default)<br>1 - FILLED\_REGION<br>2 - BOLD\_LINE<br>3 - DOTTED\_LINE<br>4 - DASHED\_LINE<br>5 - GRADIENT\_LINE|Draw style of the graph item.<br><br>Used only by normal graphs.|
|<|color|\-|`string`|<|Element color (6 symbols, hex).|
|<|yaxisside|\-|`string`|0 - LEFT (default)<br>1 - RIGHT|Side of the graph where the graph item's Y scale will be drawn.<br><br>Used by normal and stacked graphs.|
|<|calc\_fnc|\-|`string`|1 - MIN<br>2 - AVG (default)<br>4 - MAX<br>7 - ALL (minimum, average and maximum; used only by simple graphs)<br>9 - LAST (used only by pie and exploded pie graphs)|Data to draw if more than one value exists for an item.|
|<|type|\-|`string`|0 - SIMPLE (default)<br>2 - GRAPH\_SUM (value of the item represents the whole pie; used only by pie and exploded pie graphs)|Graph item type.|
|item|<|x|<|<|Individual item.|
|<|host|x|`string`|<|Item host.|
|<|key|x|`string`|<|Item key.|

[comment]: # ({/new-eec993d5})

[comment]: # ({new-fd8f09c1})
##### Template web scenario tags

|Element|Element property|Required|Type|Range^**[1](#footnotes)**^|Description|
|-------|----------------|--------|----|--------------------------|-----------|
|httptests|<|\-|<|<|Root element for web scenarios.|
|<|uuid|x|`string`|<|Unique identifier for this web scenario.|
|<|name|x|`string`|<|Web scenario name.|
|<|delay|\-|`string`|Default: 1m|Frequency of executing the web scenario. Seconds, time unit with suffix or user macro.|
|<|attempts|\-|`integer`|1-10 (default: 1)|The number of attempts for executing web scenario steps.|
|<|agent|\-|`string`|Default: Zabbix|Client agent. Zabbix will pretend to be the selected browser. This is useful when a website returns different content for different browsers.|
|<|http\_proxy|\-|`string`|<|Specify an HTTP proxy to use, using the format: `http://[username[:password]@]proxy.example.com[:port]`|
|variables|<|\-|<|<|Root element for scenario-level variables (macros) that may be used in scenario steps.|
|<|name|x|`text`|<|Variable name.|
|<|value|x|`text`|<|Variable value.|
|headers|<|\-|<|<|Root element for HTTP headers that will be sent when performing a request. Headers should be listed using the same syntax as they would appear in the HTTP protocol.|
|<|name|x|`text`|<|Header name.|
|<|value|x|`text`|<|Header value.|
|<|status|\-|`string`|0 - ENABLED (default)<br>1 - DISABLED|Web scenario status.|
|<|authentication|\-|`string`|0 - NONE (default)<br>1 - BASIC<br>2 - NTLM|Authentication method.|
|<|http\_user|\-|`string`|<|User name used for basic, HTTP or NTLM authentication.|
|<|http\_password|\-|`string`|<|Password used for basic, HTTP or NTLM authentication.|
|<|verify\_peer|\-|`string`|0 - NO (default)<br>1 - YES|Verify the SSL certificate of the web server.|
|<|verify\_host|\-|`string`|0 - NO (default)<br>1 - YES|Verify that the Common Name field or the Subject Alternate Name field of the web server certificate matches.|
|<|ssl\_cert\_file|\-|`string`|<|Name of the SSL certificate file used for client authentication (must be in PEM format).|
|<|ssl\_key\_file|\-|`string`|<|Name of the SSL private key file used for client authentication (must be in PEM format).|
|<|ssl\_key\_password|\-|`string`|<|SSL private key file password.|
|steps|<|x|<|<|Root element for web scenario steps.|
|<|name|x|`string`|<|Web scenario step name.|
|<|url|x|`string`|<|URL for monitoring.|
|query\_fields|<|\-|<|<|Root element for query fields - an array of HTTP fields that will be added to the URL when performing a request.|
|<|name|x|`string`|<|Query field name.|
|<|value|\-|`string`|<|Query field value.|
|posts|<|\-|<|<|HTTP POST variables as a string (raw post data) or as an array of HTTP fields (form field data).|
|<|name|x|`string`|<|Post field name.|
|<|value|x|`string`|<|Post field value.|
|variables|<|\-|<|<|Root element of step-level variables (macros) that should be applied after this step.<br><br>If the variable value has a 'regex:' prefix, then its value is extracted from the data returned by this step according to the regular expression pattern following the 'regex:' prefix|
|<|name|x|`string`|<|Variable name.|
|<|value|x|`string`|<|Variable value.|
|headers|<|\-|<|<|Root element for HTTP headers that will be sent when performing a request. Headers should be listed using the same syntax as they would appear in the HTTP protocol.|
|<|name|x|`string`|<|Header name.|
|<|value|x|`string`|<|Header value.|
|<|follow\_redirects|\-|`string`|0 - NO<br>1 - YES (default)|Follow HTTP redirects.|
|<|retrieve\_mode|\-|`string`|0 - BODY (default)<br>1 - HEADERS<br>2 - BOTH|HTTP response retrieve mode.|
|<|timeout|\-|`string`|Default: 15s|Timeout of step execution. Seconds, time unit with suffix or user macro.|
|<|required|\-|`string`|<|Text that must be present in the response. Ignored if empty.|
|<|status\_codes|\-|`string`|<|A comma delimited list of accepted HTTP status codes. Ignored if empty. For example: 200-201,210-299|
|tags|<|\-|<|<|Root element for web scenario tags.|
|<|tag|x|`string`|<|Tag name.|
|<|value|\-|`string`|<|Tag value.|

[comment]: # ({/new-fd8f09c1})

[comment]: # ({new-51ede6fc})
##### Template dashboard tags

|Element|Element property|Required|Type|Range^**[1](#footnotes)**^|Description|
|-------|----------------|--------|----|--------------------------|-----------|
|dashboards|<|\-|<|<|Root element for template dashboards.|
|<|uuid|x|`string`|<|Unique identifier for this dashboard.|
|<|name|x|`string`|<|Template dashboard name.|
|<|display period|\-|`integer`|<|Display period of dashboard pages.|
|<|auto\_start|\-|`string`|0 - no<br>1 - yes|Slideshow auto start.|
|pages|<|\-|<|<|Root element for template dashboard pages.|
|<|name|\-|`string`|<|Page name.|
|<|display period|\-|`integer`|<|Page display period.|
|<|sortorder|\-|`integer`|<|Page sorting order.|
|widgets|<|\-|<|<|Root element for template dashboard widgets.|
|<|type|x|`string`|<|Widget type.|
|<|name|\-|`string`|<|Widget name.|
|<|x|\-|`integer`|0-23|Horizontal position from the left side of the template dashboard.|
|<|y|\-|`integer`|0-62|Vertical position from the top of the template dashboard.|
|<|width|\-|`integer`|1-24|Widget width.|
|<|height|\-|`integer`|2-32|Widget height.|
|<|hide\_header|\-|`string`|0 - no<br>1 - yes|Hide widget header.|
|fields|<|\-|<|<|Root element for the template dashboard widget fields.|
|<|type|x|`string`|0 - INTEGER<br>1 - STRING<br>3 - HOST<br>4 - ITEM<br>5 - ITEM\_PROTOTYPE<br>6 - GRAPH<br>7 - GRAPH\_PROTOTYPE|Widget field type.|
|<|name|x|`string`|<|Widget field name.|
|<|value|x|mixed|<|Widget field value, depending on the field type.|

[comment]: # ({/new-51ede6fc})

[comment]: # ({new-869bd76e})
##### Footnotes

^**1**^ For string values, only the string will be exported (e.g.
"ZABBIX\_ACTIVE") without the numbering used in this table. The numbers
for range values (corresponding to the API values) in this table is used
for ordering only.

[comment]: # ({/new-869bd76e})
