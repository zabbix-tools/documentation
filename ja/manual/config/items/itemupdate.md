[comment]: # translation:outdated

[comment]: # ({new-109e2be5})
# 8 Mass update

[comment]: # ({/new-109e2be5})

[comment]: # ({new-7958bf1e})
#### Overview

Sometimes you may want to change some attribute for a number of items at
once. Instead of opening each individual item for editing, you may use
the mass update function for that.

[comment]: # ({/new-7958bf1e})

[comment]: # ({new-69655416})
#### Using mass update

To mass-update some items, do the following:

-   Mark the checkboxes of the items to update in the list
-   Click on *Mass update* below the list
-   Navigate to the tab with required attributes (*Item*, *Tags* or
    *Preprocessing*)
-   Mark the checkboxes of the attributes to update
-   Enter new values for the attributes

![](../../../../assets/en/manual/config/items/item_mass.png)

![](../../../../assets/en/manual/config/items/item_mass_b.png)

The *Tags* option allows to:

-   *Add* - add specified tags to the items (tags with the same name,
    but different values are not considered 'duplicates' and can be
    added to the same host).
-   *Replace* - remove the specified tags and add tags with new values
-   *Remove* - remove specified tags from the items

User macros, {INVENTORY.\*} macros, {HOST.HOST}, {HOST.NAME},
{HOST.CONN}, {HOST.DNS}, {HOST.IP}, {HOST.PORT} and {HOST.ID} macros are
supported in tags.

![](../../../../assets/en/manual/config/items/item_mass_c.png)

When done, click on *Update*.

[comment]: # ({/new-69655416})
