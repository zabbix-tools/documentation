[comment]: # translation:outdated

[comment]: # ({new-d1352224})
# 17 Prometheus checks

[comment]: # ({/new-d1352224})

[comment]: # ({new-06c90457})
#### Overview

Zabbix can query metrics exposed in the Prometheus line format.

Two steps are required to start gathering Prometheus data:

-   an [HTTP master item](/manual/config/items/itemtypes/http) pointing
    to the appropriate data endpoint, e.g.
    `https://<prometheus host>/metrics`
-   dependent items using a Prometheus preprocessing option to query
    required data from the metrics gathered by the master item

There are two Prometheus data preprocessing options:

-   *Prometheus pattern* - used in normal items to query Prometheus data
-   *Prometheus to JSON* - used in normal items and for low-level
    discovery. In this case queried Prometheus data are returned in a
    JSON format.

[comment]: # ({/new-06c90457})

[comment]: # ({new-4473dc29})

##### Bulk processing

Bulk processing is supported for dependent items. To enable caching and indexing, 
the *Prometheus pattern* preprocessing must be the **first** preprocessing step. 
When *Prometheus pattern* is first preprocessing step then the parsed Prometheus 
data is cached and indexed by the first `<label>==<value>` condition in the 
*Prometheus pattern* preprocessing step. This cache is reused when processing 
other dependent items in this batch. For optimal performance, the first label 
should be the one with most different values.

If there is other preprocessing to be done before the first step, it should be 
moved either to the master item or to a new dependent item which would be used as 
the master item for the dependent items.

[comment]: # ({/new-4473dc29})

[comment]: # ({new-73ac7ad6})
#### Configuration

Providing you have the HTTP master item configured, you need to create a
[dependent item](/manual/config/items/itemtypes/dependent_items) that
uses a Prometheus preprocessing step:

-   Enter general dependent item parameters in the configuration form
-   Go to the Preprocessing tab
-   Select a Prometheus preprocessing option (*Prometheus pattern* or
    *Prometheus to JSON*)

![](../../../../../assets/en/manual/config/items/itemtypes/prometheus_item.png)

|Parameter|Description|Examples|
|---------|-----------|--------|
|*Pattern*|To define the required data pattern you may use a query language that is similar to Prometheus query language (see [comparison table](#query_language_comparison)), e.g.:<br><metric name> - select by metric name<br>{\_\_name\_\_="<metric name>"} - select by metric name<br>{\_\_name\_\_=\~"<regex>"} - select by metric name matching a regular expression<br>{<label name>="<label value>",...} - select by label name<br>{<label name>=\~"<regex>",...} - select by label name matching a regular expression<br>{\_\_name\_\_=\~".\*"}==<value> - select by metric value<br>Or a combination of the above:<br><metric name>{<label1 name>="<label1 value>",<label2 name>=\~"<regex>",...}==<value><br><br>Label value can be any sequence of UTF-8 characters, but the backslash, double-quote and line feed characters have to be escaped as `\\`, `\"` and `\n` respectively; other characters shall not be escaped.|*wmi\_os\_physical\_memory\_free\_bytes*<br>*cpu\_usage\_system{cpu="cpu-total"}*<br>*cpu\_usage\_system{cpu=\~".\*"}*<br>*cpu\_usage\_system{cpu="cpu-total",host=\~".\*"}*<br>*wmi\_service\_state{name="dhcp"}==1*<br>*wmi\_os\_timezone{timezone=\~".\*"}==1*|
|*Output*|Define label name (optional). In this case the value corresponding to the label name is returned.<br>This field is only available for the *Prometheus pattern* option.|<|

[comment]: # ({/new-73ac7ad6})

[comment]: # ({new-8902eb84})
#### Prometheus to JSON

Data from Prometheus can be used for low-level discovery. In this case
data in JSON format are needed and the *Prometheus to JSON*
preprocessing option will return exactly that.

For more details, see [Discovery using Prometheus
data](/manual/discovery/low_level_discovery/examples/prometheus).

[comment]: # ({/new-8902eb84})

[comment]: # ({new-2fbe720b})
#### Query language comparison

The following table lists differences and similarities between PromQL
and Zabbix Prometheus preprocessing query language.

|<|[PromQL instant vector selector](https://prometheus.io/docs/prometheus/latest/querying/basics/#instant-vector-selectors)|Zabbix Prometheus preprocessing|
|-|------------------------------------------------------------------------------------------------------------------------|-------------------------------|
|**Differences**|<|<|
|Query target|Prometheus server|Plain text in Prometheus exposition format|
|Returns|Instant vector|Metric or label value (Prometheus pattern)<br>Array of metrics for single value in JSON (Prometheus to JSON)|
|Label matching operators|**=**, **!=**, **=\~**, **!\~**|**=**, **!=**, **=\~**, **!\~**|
|Regular expression used in label or metric name matching|RE2|PCRE|
|Comparison operators|See [list](https://prometheus.io/docs/prometheus/latest/querying/operators/#comparison-binary-operators)|Only **==** (equal) is supported for value filtering|
|**Similarities**|<|<|
|Selecting by metric name that equals string|<metric name> or {\_\_name\_\_="<metric name>"}|<metric name> or {\_\_name\_\_="<metric name>"}|
|Selecting by metric name that matches regular expression|{\_\_name\_\_=\~"<regex>"}|{\_\_name\_\_=\~"<regex>"}|
|Selecting by <label name> value that equals string|{<label name>="<label value>",...}|{<label name>="<label value>",...}|
|Selecting by <label name> value that matches regular expression|{<label name>=\~"<regex>",...}|{<label name>=\~"<regex>",...}|
|Selecting by value that equals string|{\_\_name\_\_=\~".\*"} == <value>|{\_\_name\_\_=\~".\*"} == <value>|

[comment]: # ({/new-2fbe720b})
