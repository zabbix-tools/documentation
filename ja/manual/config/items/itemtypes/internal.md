[comment]: # attributes: notoc

[comment]: # translation:outdated

[comment]: # ({new-8981e17d})
# 8 Internal checks

[comment]: # ({/new-8981e17d})

[comment]: # ({new-34928067})
#### Overview

Internal checks allow to monitor the internal processes of Zabbix. In
other words, you can monitor what goes on with Zabbix server or Zabbix
proxy.

Internal checks are calculated:

-   on Zabbix server - if the host is monitored by server
-   on Zabbix proxy - if the host is monitored by proxy

Internal checks are processed by server or proxy regardless of host
maintenance status.

To use this item, choose the **Zabbix internal** item type.

::: notetip
Internal checks are processed by Zabbix
pollers.
:::

[comment]: # ({/new-34928067})

[comment]: # ({new-5d8f3b63})
#### Performance

Using some internal items may negatively affect performance. These items are:

-   `zabbix[host,,items]`
-   `zabbix[host,,items_unsupported]`
-   `zabbix[hosts]`
-   `zabbix[items]`
-   `zabbix[items_unsupported]`
-   `zabbix[queue]`
-   `zabbix[required_performance]`
-   `zabbix[stats,,,queue]`
-   `zabbix[triggers]`

The [System information](/manual/web_interface/frontend_sections/reports/status_of_zabbix) and [Queue](/manual/web_interface/frontend_sections/administration/queue) 
frontend sections are also affected.

[comment]: # ({/new-5d8f3b63})

[comment]: # ({new-74b446f4})
#### Supported checks

-   Parameters without angle brackets are constants - for example,
    'host' and 'available' in `zabbix[host,<type>,available]`. Use them
    in the item key *as is*.
-   Values for items and item parameters that are "not supported on
    proxy" can only be gathered if the host is monitored by server. And
    vice versa, values "not supported on server" can only be gathered if
    the host is monitored by proxy.

|Key|<|<|<|<|<|
|---|-|-|-|-|-|
|▲|Description|<|<|Return value|Comments|
|zabbix\[boottime\]|<|<|<|<|<|
|<|Startup time of Zabbix server or Zabbix proxy process in seconds.|<|<|Integer.|<|
|zabbix\[cluster,discovery,nodes\]|<|<|<|<|<|
|<|Discover [high availability cluster](/manual/concepts/server/ha) nodes.|<|<|JSON.|This item can be used in low-level discovery.|
|zabbix\[history\]|<|<|<|<|<|
|<|Number of values stored in the HISTORY table.|<|<|Integer.|Do not use if MySQL InnoDB, Oracle or PostgreSQL is used!<br>*(not supported on proxy)*|
|zabbix\[history\_log\]|<|<|<|<|<|
|<|Number of values stored in the HISTORY\_LOG table.|<|<|Integer.|Do not use if MySQL InnoDB, Oracle or PostgreSQL is used!<br>*(not supported on proxy)*|
|zabbix\[history\_str\]|<|<|<|<|<|
|<|Number of values stored in the HISTORY\_STR table.|<|<|Integer.|Do not use if MySQL InnoDB, Oracle or PostgreSQL is used!<br>*(not supported on proxy)*|
|zabbix\[history\_text\]|<|<|<|<|<|
|<|Number of values stored in the HISTORY\_TEXT table.|<|<|Integer.|Do not use if MySQL InnoDB, Oracle or PostgreSQL is used!<br>*(not supported on proxy)*|
|zabbix\[history\_uint\]|<|<|<|<|<|
|<|Number of values stored in the HISTORY\_UINT table.|<|<|Integer.|Do not use if MySQL InnoDB, Oracle or PostgreSQL is used!<br>This item is supported since Zabbix 1.8.3.<br>*(not supported on proxy)*|
|zabbix\[host,,items\]|<|<|<|<|<|
|<|Number of enabled items (supported and not supported) on the host.|<|<|Integer.|This item is supported since Zabbix 3.0.0.|
|zabbix\[host,,items\_unsupported\]|<|<|<|<|<|
|<|Number of enabled unsupported items on the host.|<|<|Integer.|This item is supported since Zabbix 3.0.0.\*|
|zabbix\[host,,maintenance\]|<|<|<|<|<|
|<|Current maintenance status of a host.|<|<|0 - host in normal state,<br>1 - host in maintenance with data collection,<br>2 - host in maintenance without data collection.|This item is always processed by Zabbix server regardless of host location (on server or proxy). The proxy will not receive this item with configuration data.<br>The second parameter must be empty and is reserved for future use.|
|zabbix\[host,discovery,interfaces\]|<|<|<|<|<|
|<|Details of all configured interfaces of the host in Zabbix frontend.|<|<|JSON object.|This item can be used in [low-level discovery](/manual/discovery/low_level_discovery/examples/host_interfaces).<br>This item is supported since Zabbix 3.4.0.<br>*(not supported on proxy)*|
|zabbix\[host,<type>,available\]|<|<|<|<|<|
|<|Availability of the main interface of a particular type of checks on the host.|<|<|0 - not available, 1 - available, 2 - unknown.|Valid **types** are:<br>*agent*, *snmp*, *ipmi*, *jmx*<br><br>The item value is calculated according to configuration parameters regarding host [unreachability/unavailability](/manual/appendix/items/unreachability).<br><br>This item is supported since Zabbix 2.0.0.<br>|
|zabbix\[hosts\]|<|<|<|<|<|
|<|Number of monitored hosts.|<|<|Integer.|<|
|zabbix\[items\]|<|<|<|<|<|
|<|Number of enabled items (supported and not supported).|<|<|Integer.|<|
|zabbix\[items\_unsupported\]|<|<|<|<|<|
|<|Number of not supported items.|<|<|Integer.|<|
|zabbix\[java,,<param>\]|<|<|<|<|<|
|<|Information about Zabbix Java gateway.|<|<|If <param> is *ping*, "1" is returned. Can be used to check Java gateway availability using nodata() trigger function.<br><br>If <param> is *version*, version of Java gateway is returned. Example: "2.0.0".|Valid values for **param** are:<br>*ping*, *version*<br><br>Second parameter must be empty and is reserved for future use.|
|zabbix\[lld\_queue\]|<|<|<|<|<|
|<|Count of values enqueued in the low-level discovery processing queue.|<|<|Integer.|This item can be used to monitor the low-level discovery processing queue length.<br><br>This item is supported since Zabbix 4.2.0.|
|zabbix\[preprocessing\_queue\]|<|<|<|<|<|
|<|Count of values enqueued in the preprocessing queue.|<|<|Integer.|This item can be used to monitor the preprocessing queue length.<br><br>This item is supported since Zabbix 3.4.0.|
|zabbix\[process,<type>,<mode>,<state>\]|<|<|<|<|<|
|<|Time a particular Zabbix process or a group of processes (identified by <type> and <mode>) spent in <state> in percentage. It is calculated for the last minute only.<br><br>If <mode> is Zabbix process number that is not running (for example, with 5 pollers running <mode> is specified to be 6), such an item will turn into unsupported state.<br>Minimum and maximum refers to the usage percentage for a single process. So if in a group of 3 pollers usage percentages per process were 2, 18 and 66, min would return 2 and max would return 66.<br>Processes report what they are doing in shared memory and the self-monitoring process summarizes that data each second. State changes (busy/idle) are registered upon change - thus a process that becomes busy registers as such and doesn't change or update the state until it becomes idle. This ensures that even fully hung processes will be correctly registered as 100% busy.<br>Currently, "busy" means "not sleeping", but in the future additional states might be introduced - waiting for locks, performing database queries, etc.<br>On Linux and most other systems, resolution is 1/100 of a second.|<|<|Percentage of time.<br>Float.|Supported **types** of [server processes](/manual/concepts/server#server_process_types):<br>*alert manager*, *alert syncer*, *alerter*, *availability manager*, *configuration syncer*, *discoverer*, *escalator*, *history poller*, *history syncer*, *housekeeper*, *http poller*, *icmp pinger*, *ipmi manager*, *ipmi poller*, *java poller*, *lld manager*, *lld worker*, *poller*, *preprocessing manager*, *preprocessing worker*, *proxy poller*, *self-monitoring*, *snmp trapper*, *task manager*, *timer*, *trapper*, *unreachable poller*, *vmware collector*<br><br>Supported **types** of [proxy processes](/manual/concepts/proxy#proxy_process_types):<br>*availability manager*, *configuration syncer*, *data sender*, *discoverer*, *heartbeat sender*, *history poller*, *history syncer*, *housekeeper*, *http poller*, *icmp pinger*, *ipmi manager*, *ipmi poller*, *java poller*, *poller*, *preprocessing manager*, *preprocessing worker*, *self-monitoring*, *snmp trapper*, *task manager*, *trapper*, *unreachable poller*, *vmware collector*<br><br>Valid **modes** are:<br>*avg* - average value for all processes of a given type (default)<br>*count* - returns number of forks for a given process type, <state> should not be specified<br>*max* - maximum value<br>*min* - minimum value<br>*<process number>* - process number (between 1 and the number of pre-forked instances). For example, if 4 trappers are running, the value is between 1 and 4.<br><br>Valid **states** are:<br>*busy* - process is in busy state, for example, processing request (default).<br>*idle* - process is in idle state doing nothing.<br><br>Examples:<br>=> zabbix\[process,poller,avg,busy\] → average time of poller processes spent doing something during the last minute<br>=> zabbix\[process,"icmp pinger",max,busy\] → maximum time spent doing something by any ICMP pinger process during the last minute<br>=> zabbix\[process,"history syncer",2,busy\] → time spent doing something by history syncer number 2 during the last minute<br>=> zabbix\[process,trapper,count\] → amount of currently running trapper processes|
|zabbix\[proxy,<name>,<param>\]|<|<|<|<|<|
|<|Information about Zabbix proxy.|<|<|Integer.|**name**: proxy name<br><br>Valid values for **param** are:<br>*lastaccess* - timestamp of last heart beat message received from proxy<br>*delay* - how long collected values are unsent, calculated as "proxy delay" (difference between the current proxy time and the timestamp of the oldest unsent value on proxy) + ("current server time" - "proxy lastaccess")<br><br>Example:<br>=> zabbix\[proxy,"Germany",lastaccess\]<br><br>`fuzzytime()` [function](/manual/appendix/functions/history) can be used to check availability of proxies.<br>This item is always processed by Zabbix server regardless of host location (on server or proxy).|
|zabbix\[proxy\_history\]|<|<|<|<|<|
|<|Number of values in the proxy history table waiting to be sent to the server.|<|<|Integer.|*(not supported on server)*|
|zabbix\[queue,<from>,<to>\]|<|<|<|<|<|
|<|Number of monitored items in the queue which are delayed at least by <from> seconds but less than by <to> seconds.|<|<|Integer.|**from** - default: 6 seconds<br>**to** - default: infinity<br>[Time-unit symbols](/manual/appendix/suffixes) (s,m,h,d,w) are supported for these parameters.|
|zabbix\[rcache,<cache>,<mode>\]|<|<|<|<|<|
|<|Availability statistics of Zabbix configuration cache.|<|<|Integer (for size); float (for percentage).|**cache**: *buffer*<br><br>Valid **modes** are:<br>*total* - total size of buffer<br>*free* - size of free buffer<br>*pfree* - percentage of free buffer<br>*used* - size of used buffer<br>*pused* - percentage of used buffer<br><br>*pused* mode is supported since Zabbix 4.0.0.|
|zabbix\[requiredperformance\]|<|<|<|<|<|
|<|Required performance of Zabbix server or Zabbix proxy, in new values per second expected.|<|<|Float.|Approximately correlates with "Required server performance, new values per second" in *Reports → [System information](/manual/web_interface/frontend_sections/reports/status_of_zabbix)*.|
|zabbix\[stats,<ip>,<port>\]|<|<|<|<|<|
|<|Remote Zabbix server or proxy internal metrics.|<|<|JSON object.|**ip** - IP/DNS/network mask list of servers/proxies to be remotely queried (default is 127.0.0.1)<br>**port** - port of server/proxy to be remotely queried (default is 10051)<br><br>Note that the stats request will only be accepted from the addresses listed in the 'StatsAllowedIP' [server](/manual/appendix/config/zabbix_server)/[proxy](/manual/appendix/config/zabbix_proxy) parameter on the target instance.<br><br>A selected set of internal metrics is returned by this item. For details, see [Remote monitoring of Zabbix stats](/manual/appendix/items/remote_stats#exposed_metrics).<br><br>Supported since 4.2.0.|
|zabbix\[stats,<ip>,<port>,queue,<from>,<to>\]|<|<|<|<|<|
|<|Remote Zabbix server or proxy internal queue metrics (see `zabbix[queue,<from>,<to>]`).|<|<|JSON object.|**ip** - IP/DNS/network mask list of servers/proxies to be remotely queried (default is 127.0.0.1)<br>**port** - port of server/proxy to be remotely queried (default is 10051)<br>**from** - delayed by at least (default is 6 seconds)<br>**to** - delayed by at most (default is infinity)<br><br>Note that the stats request will only be accepted from the addresses listed in the 'StatsAllowedIP' [server](/manual/appendix/config/zabbix_server)/[proxy](/manual/appendix/config/zabbix_proxy) parameter on the target instance.<br><br>Supported since 4.2.0.|
|zabbix\[tcache,cache,<parameter>\]|<|<|<|<|<|
|<|Effectiveness statistics of the Zabbix trend function cache.|<|<|Integer (for size); float (for percentage).|Valid **parameters** are:<br>*all* - total cache requests (default)<br>*hits* - cache hits<br>*phits* - percentage of cache hits<br>*misses* - cache misses<br>*pmisses* - percentage of cache misses<br>*items* - the number of cached items<br>*requests* - the number of cached requests<br>*pitems* - percentage of cached items from cached items + requests. Low percentage most likely means that the cache size can be reduced.<br><br>Supported since 5.4.0.<br><br>*(not supported on proxy)*|
|zabbix\[trends\]|<|<|<|<|<|
|<|Number of values stored in the TRENDS table.|<|<|Integer.|Do not use if MySQL InnoDB, Oracle or PostgreSQL is used!<br>*(not supported on proxy)*|
|zabbix\[trends\_uint\]|<|<|<|<|<|
|<|Number of values stored in the TRENDS\_UINT table.|<|<|Integer.|Do not use if MySQL InnoDB, Oracle or PostgreSQL is used!<br>This item is supported since Zabbix 1.8.3.<br>*(not supported on proxy)*|
|zabbix\[triggers\]|<|<|<|<|<|
|<|Number of enabled triggers in Zabbix database, with all items enabled on enabled hosts.|<|<|Integer.|*(not supported on proxy)*|
|zabbix\[uptime\]|<|<|<|<|<|
|<|Uptime of Zabbix server or Zabbix proxy process in seconds.|<|<|Integer.|<|
|zabbix\[vcache,buffer,<mode>\]|<|<|<|<|<|
|<|Availability statistics of Zabbix value cache.|<|<|Integer (for size); float (for percentage).|Valid **modes** are:<br>*total* - total size of buffer<br>*free* - size of free buffer<br>*pfree* - percentage of free buffer<br>*used* - size of used buffer<br>*pused* - percentage of used buffer<br><br>*(not supported on proxy)*|
|zabbix\[vcache,cache,<parameter>\]|<|<|<|<|<|
|<|Effectiveness statistics of Zabbix value cache.|<|<|Integer.<br><br>With the *mode* parameter:<br>0 - normal mode,<br>1 - low memory mode|Valid **parameter** values are:<br>*requests* - total number of requests<br>*hits* - number of cache hits (history values taken from the cache)<br>*misses* - number of cache misses (history values taken from the database)<br>*mode* - value cache operating mode<br><br>This item is supported since Zabbix 2.2.0 and the *mode* parameter since Zabbix 3.0.0.<br>*(not supported on proxy)*<br><br>Once the low memory mode has been switched on, the value cache will remain in this state for 24 hours, even if the problem that triggered this mode is resolved sooner.<br><br>You may use this key with the *Change per second* preprocessing step in order to get values per second statistics.|
|zabbix\[version\]|<|<|<|<|<|
|<|Version of Zabbix server or proxy.|<|<|String.|This item is supported since Zabbix 5.0.0.<br><br>Example of return value: 5.0.0beta1|
|zabbix\[vmware,buffer,<mode>\]|<|<|<|<|<|
|<|Availability statistics of Zabbix vmware cache.|<|<|Integer (for size); float (for percentage).|Valid **modes** are:<br>*total* - total size of buffer<br>*free* - size of free buffer<br>*pfree* - percentage of free buffer<br>*used* - size of used buffer<br>*pused* - percentage of used buffer|
|zabbix\[wcache,<cache>,<mode>\]|<|<|<|<|<|
|<|Statistics and availability of Zabbix write cache.|<|<|<|Specifying <cache> is mandatory.|
|<|**Cache**|**Mode**|<|<|<|
|^|values|all<br>*(default)*|Total number of values processed by Zabbix server or Zabbix proxy, except unsupported items.|Integer.|Counter.<br>You may use this key with the *Change per second* preprocessing step in order to get values per second statistics.|
|^|^|float|Number of processed float values.|Integer.|Counter.|
|^|^|uint|Number of processed unsigned integer values.|Integer.|Counter.|
|^|^|str|Number of processed character/string values.|Integer.|Counter.|
|^|^|log|Number of processed log values.|Integer.|Counter.|
|^|^|text|Number of processed text values.|Integer.|Counter.|
|^|^|not supported|Number of times item processing resulted in item becoming unsupported or keeping that state.|Integer.|Counter.<br>|
|^|history|pfree<br>*(default)*|Percentage of free history buffer.|Float.|History cache is used to store item values. A low number indicates performance problems on the database side.|
|^|^|free|Size of free history buffer.|Integer.|<|
|^|^|total|Total size of history buffer.|Integer.|<|
|^|^|used|Size of used history buffer.|Integer.|<|
|^|^|pused|Percentage of used history buffer.|Float.|*pused* mode is supported since Zabbix 4.0.0.|
|^|index|pfree<br>*(default)*|Percentage of free history index buffer.|Float.|History index cache is used to index values stored in history cache.<br>*Index* cache is supported since Zabbix 3.0.0.|
|^|^|free|Size of free history index history buffer.|Integer.|<|
|^|^|total|Total size of history index history buffer.|Integer.|<|
|^|^|used|Size of used history index history buffer.|Integer.|<|
|^|^|pused|Percentage of used history index buffer.|Float.|*pused* mode is supported since Zabbix 4.0.0.|
|^|trend|pfree<br>*(default)*|Percentage of free trend cache.|Float.|Trend cache stores aggregate for the current hour for all items that receive data.<br>*(not supported on proxy)*|
|^|^|free|Size of free trend buffer.|Integer.|*(not supported on proxy)*|
|^|^|total|Total size of trend buffer.|Integer.|*(not supported on proxy)*|
|^|^|used|Size of used trend buffer.|Integer.|*(not supported on proxy)*|
|^|^|pused|Percentage of used trend buffer.|Float.|*(not supported on proxy)*<br><br>*pused* mode is supported since Zabbix 4.0.0.|

[comment]: # ({/new-74b446f4})

[comment]: # ({new-0f6066ab})

### Item key details

-   Parameters without angle brackets are constants - for example,
    'host' and 'available' in `zabbix[host,<type>,available]`. Use them in the item key *as is*.
-   Values for items and item parameters that are "not supported on proxy" can only be retrieved if the host is monitored by server. And vice versa, values "not supported on server" can only be retrieved if the host is monitored by proxy.

[comment]: # ({/new-0f6066ab})

[comment]: # ({new-e52bbd1a})

##### zabbix[boottime] {#boottime}

<br>
The startup time of Zabbix server or Zabbix proxy process in seconds.<br>
Return value: *Integer*.

[comment]: # ({/new-e52bbd1a})

[comment]: # ({new-d1cde038})

##### zabbix[cluster,discovery,nodes] {#cluster.discovery}

<br>
Discovers the [high availability cluster](/manual/concepts/server/ha) nodes.<br>
Return value: *JSON object*.

This item can be used in low-level discovery.

[comment]: # ({/new-d1cde038})

[comment]: # ({new-09c4f0cf})

##### zabbix[connector_queue] {#connector.queue}

<br>
The count of values enqueued in the connector queue.<br>
Return value: *Integer*.

This item is supported since Zabbix 6.4.0.

[comment]: # ({/new-09c4f0cf})

[comment]: # ({new-fcd989ee})

##### zabbix[discovery_queue] {#discovery.queue}

<br>
The count of network checks enqueued in the discovery queue.<br>
Return value: *Integer*.

[comment]: # ({/new-fcd989ee})

[comment]: # ({new-f065c85f})

##### zabbix[host,,items] {#host.items}

<br>
The number of enabled items (supported and not supported) on the host.<br>
Return value: *Integer*.

[comment]: # ({/new-f065c85f})

[comment]: # ({new-31a0bdcc})

##### zabbix[host,,items_unsupported] {#host.items.unsupported}

<br>
The number of enabled unsupported items on the host.<br>
Return value: *Integer*.

[comment]: # ({/new-31a0bdcc})

[comment]: # ({new-6220db31})

##### zabbix[host,,maintenance] {#maintenance}

<br>
The current maintenance status of the host.<br>
Return values: *0* - normal state; *1* - maintenance with data collection; *2* - maintenance without data collection.

Comments:

-   This item is always processed by Zabbix server regardless of the host location (on server or proxy). The proxy will not receive this item with configuration data. 
-   The second parameter must be empty and is reserved for future use.

[comment]: # ({/new-6220db31})

[comment]: # ({new-ec7187d4})

##### zabbix[host,active_agent,available] {#active.available}

<br>
The availability of active agent checks on the host.<br>
Return values: *0* - unknown; *1* - available; *2* - not available.

[comment]: # ({/new-ec7187d4})

[comment]: # ({new-48ad1e26})

##### zabbix[host,discovery,interfaces] {#discovery.interfaces}

<br>
The details of all configured interfaces of the host in Zabbix frontend.<br>
Return value: *JSON object*.

Comments:

-   This item can be used in [low-level discovery](/manual/discovery/low_level_discovery/examples/host_interfaces);
-   This item is not supported on Zabbix proxy.

[comment]: # ({/new-48ad1e26})

[comment]: # ({new-9752a684})

##### zabbix[host,<type>,available] {#host.available}

<br>
The availability of the main interface of a particular type of checks on the host.<br>
Return values: *0* - not available; *1* - available; *2* - unknown.

Comments:

-   Valid **types** are: *agent*, *snmp*, *ipmi*, *jmx*;
-   The item value is calculated according to the configuration parameters regarding host [unreachability/unavailability](/manual/appendix/items/unreachability).

[comment]: # ({/new-9752a684})

[comment]: # ({new-a65ef5bf})

##### zabbix[hosts] {#hosts}

<br>
The number of monitored hosts.<br>
Return value: *Integer*.

[comment]: # ({/new-a65ef5bf})

[comment]: # ({new-f291ac50})

##### zabbix[items] {#items}

<br>
The number of enabled items (supported and not supported).<br>
Return value: *Integer*.

[comment]: # ({/new-f291ac50})

[comment]: # ({new-53295b6d})

##### zabbix[items_unsupported] {#items.unsupported}

<br>
The number of unsupported items.<br>
Return value: *Integer*.

[comment]: # ({/new-53295b6d})

[comment]: # ({new-ba37fff4})

##### zabbix[java,,<param>] {#java}

<br>
The information about Zabbix Java gateway.<br>
Return values: *1* - if <param> is *ping*; *Java gateway version* -  if <param> is *version* (for example: "2.0.0").

Comments:

-   Valid values for **param** are: *ping*, *version*;
-   This item can be used to check Java gateway availability using the `nodata()` trigger function;
-   The second parameter must be empty and is reserved for future use.

[comment]: # ({/new-ba37fff4})

[comment]: # ({new-8f8235d6})

##### zabbix[lld_queue] {#lld.queue}

<br>
The count of values enqueued in the low-level discovery processing queue.<br>
Return value: *Integer*.

This item can be used to monitor the low-level discovery processing queue length.

[comment]: # ({/new-8f8235d6})

[comment]: # ({new-3e07a93d})

##### zabbix[preprocessing_queue] {#preprocessing.queue}

<br>
The count of values enqueued in the preprocessing queue.<br>
Return value: *Integer*.

This item can be used to monitor the preprocessing queue length.

[comment]: # ({/new-3e07a93d})

[comment]: # ({new-b74b9537})

##### zabbix[process,<type>,<mode>,<state>] {#process}

<br>
The percentage of time a particular Zabbix process or a group of processes (identified by \<type\> and \<mode\>) spent in \<state\>. It is calculated for the last minute only.<br>
Return value: *Float*.

Parameters:

-   **type** - for [server processes](/manual/concepts/server#server_process_types): *alert manager*, *alert syncer*, *alerter*, *availability manager*, *configuration syncer*, *connector manager*, *connector worker*, *discoverer*, *escalator*, *history poller*, *history syncer*, *housekeeper*, *http poller*, *icmp pinger*, *ipmi manager*, *ipmi poller*, *java poller*, *lld manager*, *lld worker*, *odbc poller*, *poller*, *preprocessing manager*, *preprocessing worker*, *proxy poller*, *self-monitoring*, *snmp trapper*, *task manager*, *timer*, *trapper*, *unreachable poller*, *vmware collector*;<br>for [proxy processes](/manual/concepts/proxy#proxy_process_types): *availability manager*, *configuration syncer*, *data sender*, *discoverer*, *history syncer*, *housekeeper*, *http poller*, *icmp pinger*, *ipmi manager*, *ipmi poller*, *java poller*, *odbc poller*, *poller*, *preprocessing manager*, *preprocessing worker*, *self-monitoring*, *snmp trapper*, *task manager*, *trapper*, *unreachable poller*, *vmware collector*
-   **mode** - *avg* - average value for all processes of a given type (default)<br>*count* - returns number of forks for a given process type, <state> should not be specified<br>*max* - maximum value<br>*min* - minimum value<br>*<process number>* - process number (between 1 and the number of pre-forked instances). For example, if 4 trappers are running, the value is between 1 and 4.
-   **state** - *busy* - process is in busy state, for example, the processing request (default); *idle* - process is in idle state doing nothing.

Comments:

-   If \<mode\> is a Zabbix process number that is not running (for example, with 5 pollers running the \<mode\> is specified to be 6), such an item will turn unsupported;
-   Minimum and maximum refers to the usage percentage for a single process. So if in a group of 3 pollers usage percentages per process were 2, 18 and 66, min would return 2 and max would return 66.
-   Processes report what they are doing in shared memory and the self-monitoring process summarizes that data each second. State changes (busy/idle) are registered upon change - thus a process that becomes busy registers as such and doesn't change or update the state until it becomes idle. This ensures that even fully hung processes will be correctly registered as 100% busy.
-   Currently, "busy" means "not sleeping", but in the future additional states might be introduced - waiting for locks, performing database queries, etc.
-   On Linux and most other systems, resolution is 1/100 of a second.

Examples:

    zabbix[process,poller,avg,busy] #the average time of poller processes spent doing something during the last minute
    zabbix[process,"icmp pinger",max,busy] #the maximum time spent doing something by any ICMP pinger process during the last minute
    zabbix[process,"history syncer",2,busy] #the time spent doing something by history syncer number 2 during the last minute
    zabbix[process,trapper,count] #the amount of currently running trapper processes

[comment]: # ({/new-b74b9537})

[comment]: # ({new-d90bc125})

##### zabbix[proxy,<name>,<param>] {#proxy}

<br>
The information about Zabbix proxy.<br>
Return value: *Integer*.

Parameters:

-   **name** - the proxy name;
-   **param** - *delay* - how long the collected values are unsent, calculated as "proxy delay" (difference between the current proxy time and the timestamp of the oldest unsent value on proxy) + ("current server time" - "proxy lastaccess").

[comment]: # ({/new-d90bc125})

[comment]: # ({new-fd9ea3f4})

##### zabbix[proxy,discovery] {#proxy.discovery}

<br>
The list of Zabbix proxies with name, mode, encryption, compression, version, last seen, host count, item count, required values per second (vps) and version status (current/outdated/unsupported).<br>
Return value: *JSON object*.

[comment]: # ({/new-fd9ea3f4})

[comment]: # ({new-5b463b30})

##### zabbix[proxy_history] {#proxy.history}

<br>
The number of values in the proxy history table waiting to be sent to the server.<br>
Return values: *Integer*.

This item is not supported on Zabbix server.

[comment]: # ({/new-5b463b30})

[comment]: # ({new-ebe20dec})

##### zabbix[queue,<from>,<to>] {#queue}

<br>
The number of monitored items in the queue which are delayed at least by \<from\> seconds, but less than \<to\> seconds.<br>
Return value: *Integer*.

Parameters:

-   **from** - default: 6 seconds;
-   **to** - default: infinity.

[Time-unit symbols](/manual/appendix/suffixes) (s,m,h,d,w) are supported in the parameters.

[comment]: # ({/new-ebe20dec})

[comment]: # ({new-04aafd0c})

##### zabbix[rcache,<cache>,<mode>] {#rcache}

<br>
The availability statistics of the Zabbix configuration cache.<br>
Return values: *Integer* (for size); *Float* (for percentage).

Parameters:

-   **cache** - *buffer*;
-   **mode** - *total* - the total size of buffer<br>*free* - the size of free buffer<br>*pfree* - the percentage of free buffer<br>*used* - the size of used buffer<br>*pused* - the percentage of used buffer

[comment]: # ({/new-04aafd0c})

[comment]: # ({new-f2b37a90})

##### zabbix[requiredperformance] {#required.performance}

<br>
The required performance of Zabbix server or Zabbix proxy, in new values per second expected.<br>
Return value: *Float*.

Approximately correlates with "Required server performance, new values per second" in *Reports → [System information](/manual/web_interface/frontend_sections/reports/status_of_zabbix)*.

[comment]: # ({/new-f2b37a90})

[comment]: # ({new-8054c8ab})

##### zabbix[stats,<ip>,<port>] {#stats}

<br>
The internal metrics of a remote Zabbix server or proxy.<br>
Return values: *JSON object*.

Parameters:

-   **ip** - the IP/DNS/network mask list of servers/proxies to be remotely queried (default is 127.0.0.1);
-   **port** - the port of server/proxy to be remotely queried (default is 10051).

Comments:

-   The stats request will only be accepted from the addresses listed in the 'StatsAllowedIP' [server](/manual/appendix/config/zabbix_server)/[proxy](/manual/appendix/config/zabbix_proxy) parameter on the target instance;
-   A selected set of internal metrics is returned by this item. For details, see [Remote monitoring of Zabbix stats](/manual/appendix/items/remote_stats#exposed_metrics).

[comment]: # ({/new-8054c8ab})

[comment]: # ({new-d7c2c9be})

##### zabbix[stats,<ip>,<port>,queue,<from>,<to>] {#stats.queue}

<br>
The internal queue metrics (see `zabbix[queue,<from>,<to>]`) of a remote Zabbix server or proxy.<br>
Return values: *JSON object*.

Parameters:

-   **ip** - the IP/DNS/network mask list of servers/proxies to be remotely queried (default is 127.0.0.1);
-   **port** - the port of server/proxy to be remotely queried (default is 10051);
-   **from** - delayed by at least (default is 6 seconds);
-   **to** - delayed by at most (default is infinity).

Comments:

-   The stats request will only be accepted from the addresses listed in the 'StatsAllowedIP' [server](/manual/appendix/config/zabbix_server)/[proxy](/manual/appendix/config/zabbix_proxy) parameter on the target instance;
-   A selected set of internal metrics is returned by this item. For details, see [Remote monitoring of Zabbix stats](/manual/appendix/items/remote_stats#exposed_metrics).

[comment]: # ({/new-d7c2c9be})

[comment]: # ({new-c688d590})

##### zabbix[tcache,<cache>,<parameter>] {#tcache}

<br>
The effectiveness statistics of the Zabbix trend function cache.<br>
Return values: *Integer* (for size); *Float* (for percentage).

Parameters:

-   **cache** - *buffer*;
-   **mode** - **all* - total cache requests (default)<br>*hits* - cache hits<br>*phits* - percentage of cache hits<br>*misses* - cache misses<br>*pmisses* - percentage of cache misses<br>*items* - the number of cached items<br>*requests* - the number of cached requests<br>*pitems* - percentage of cached items from cached items + requests. Low percentage most likely means that the cache size can be reduced.

This item is not supported on Zabbix proxy.

[comment]: # ({/new-c688d590})

[comment]: # ({new-9699e4ca})

##### zabbix[triggers] {#triggers}

<br>
The number of enabled triggers in Zabbix database, with all items enabled on enabled hosts.<br>
Return value: *Integer*.

This item is not supported on Zabbix proxy.

[comment]: # ({/new-9699e4ca})

[comment]: # ({new-a5c8cdec})

##### zabbix[uptime] {#uptime}

<br>
The uptime of the Zabbix server or proxy process in seconds.<br>
Return value: *Integer*.

[comment]: # ({/new-a5c8cdec})

[comment]: # ({new-304cb09c})

##### zabbix[vcache,buffer,<mode>] {#vcache}

<br>
The availability statistics of the Zabbix value cache.<br>
Return values: *Integer* (for size); *Float* (for percentage).

Parameter:

-   **mode** - *total* - the total size of buffer<br>*free* - the size of free buffer<br>*pfree* - the percentage of free buffer<br>*used* - the size of used buffer<br>*pused* - the percentage of used buffer

This item is not supported on Zabbix proxy.

[comment]: # ({/new-304cb09c})

[comment]: # ({new-1454b994})

##### zabbix[vcache,cache,<parameter>] {#vcache.parameter}

<br>
The effectiveness statistics of the Zabbix value cache.<br>
Return values: *Integer*. With the *mode* parameter returns: 0 - normal mode; 1 - low memory mode.

Parameters:

-   **parameter** - *requests* - the total number of requests<br>*hits* - the number of cache hits (history values taken from the cache)<br>*misses* - the number of cache misses (history values taken from the database)<br>*mode* - the value cache operating mode

Comments:

-   Once the low-memory mode has been switched on, the value cache will remain in this state for 24 hours, even if the problem that triggered this mode is resolved sooner;
-   You may use this key with the *Change per second* preprocessing step in order to get values-per-second statistics;
-   This item is not supported on Zabbix proxy.

[comment]: # ({/new-1454b994})

[comment]: # ({new-ddde46a0})

##### zabbix[version] {#version}

<br>
The version of Zabbix server or proxy.<br>
Return value: *String*. For example: `6.0.0beta1`.

[comment]: # ({/new-ddde46a0})

[comment]: # ({new-dda0c008})

##### zabbix[vmware,buffer,<mode>] {#vmware}

<br>
The availability statistics of the Zabbix vmware cache.<br>
Return values: *Integer* (for size); *Float* (for percentage).

Parameters:

-   **mode** - *total* - the total size of buffer<br>*free* - the size of free buffer<br>*pfree* - the percentage of free buffer<br>*used* - the size of used buffer<br>*pused* - the percentage of used buffer

[comment]: # ({/new-dda0c008})

[comment]: # ({new-9fa76fcd})

##### zabbix[wcache,<cache>,<mode>] {#wcache}

<br>
The statistics and availability of the Zabbix write cache.<br>
Return values: *Integer* (for number/size); *Float* (for percentage).

Parameters:

-   **cache** - *values*, *history*, *index*, or *trend*;
-   **mode** - (with *values*) *all* (default) - the total number of values processed by Zabbix server/proxy, except unsupported items (counter)<br>*float* - the number of processed float values (counter)<br>*uint* - the number of processed unsigned integer values (counter)<br>*str* - the number of processed character/string values (counter)<br>*log* - the number of processed log values (counter)<br>*text* - the number of processed text values (counter)<br>*not supported* - the number of times item processing resulted in item becoming unsupported or keeping that state (counter)<br>(with *history*, *index*, *trend* cache) *pfree* (default) - the percentage of free buffer<br>*total* - the total size of buffer<br>*free* - the size of free buffer<br>*used* - the size of used buffer<br>*pused* - the percentage of used buffer

Comments:

-   Specifying \<cache\> is mandatory. The `trend` cache parameter is not supported with Zabbix proxy.
-   The history cache is used to store item values. A low number indicates performance problems on the database side.
-   The history index cache is used to index the values stored in the history cache;
-   The trend cache stores the aggregate for the current hour for all items that receive data;
-   You may use the zabbix[wcache,values] key with the *Change per second* preprocessing step in order to get values-per-second statistics.

[comment]: # ({/new-9fa76fcd})
