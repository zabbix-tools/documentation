[comment]: # translation:outdated

[comment]: # ({new-f733158d})
# 9 Templates out of the box

[comment]: # ({/new-f733158d})

[comment]: # ({new-0cc95643})
#### Overview

Zabbix strives to provide a growing list of useful out-of-the-box
[templates](/manual/config/templates). Out-of-the-box templates come
preconfigured and thus are a useful way for speeding up the deployment
of monitoring jobs.

The templates are available:

-   In new installations - in *Configuration* → *Templates*;
-   If you are upgrading from previous versions, you can find these
    templates in the `templates` directory of the downloaded latest
    Zabbix version. While in *Configuration* → *Templates* you can
    import them manually from this directory.
-   It is also possible to download the template from [Zabbix git
    repository](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates)
    directly (make sure the template is compatible with your Zabbix
    version).

Please use the sidebar to access information about specific template
types and operation requirements.

See also:

-   [Template import](/manual/xml_export_import/templates#importing)
-   [Linking a
    template](/manual/config/templates/linking#linking_a_template)

[comment]: # ({/new-0cc95643})
