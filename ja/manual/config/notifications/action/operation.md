[comment]: # translation:outdated

[comment]: # ({new-531c9a02})
# 2 Operations

[comment]: # ({/new-531c9a02})

[comment]: # ({new-49564975})
#### Overview

You can define the following operations for all events:

-   send a message
-   execute a remote command

::: noteimportant
Zabbix server does not create alerts if access to
the host is explicitly "denied" for the user defined as action operation
recipient or if the user has no rights defined to the host at
all.
:::

For discovery and autoregistration events, there are additional
operations available:

-   [add
    host](/manual/config/notifications/action/operation/other#adding_host)
-   remove host
-   enable host
-   disable host
-   add to host group
-   remove from host group
-   link to template
-   unlink from template
-   set host inventory mode

[comment]: # ({/new-49564975})

[comment]: # ({new-4b8a4b65})
#### Configuring an operation

To configure an operation, go to the *Operations* tab in
[action](/manual/config/notifications/action) configuration.

![](../../../../../assets/en/manual/config/notifications/action_operation.png){width="600"}

General operation attributes:

|Parameter|<|<|<|<|Description|
|---------|-|-|-|-|-----------|
|*Default operation step duration*|<|<|<|<|Duration of one operation step by default (60 seconds to 1 week).<br>For example, an hour-long step duration means that if an operation is carried out, an hour will pass before the next step.<br>[Time suffixes](/manual/appendix/suffixes) are supported, e.g. 60s, 1m, 2h, 1d, since Zabbix 3.4.0.<br>[User macros](/manual/config/macros/user_macros) are supported, since Zabbix 3.4.0.|
|*Pause operations for suppressed problems*|<|<|<|<|Mark this checkbox to delay the start of operations for the duration of a maintenance period. When operations are started, after the maintenance, all operations are performed including those for the events during the maintenance.<br>Note that this setting affects only problem escalations; recovery and update operations will not be affected.<br>If you unmark this checkbox, operations will be executed without delay even during a maintenance period.<br>This option is not available for *Service actions*.|
|*Operations*|<|<|<|<|Action operations (if any) are displayed, with these details:<br>**Steps** - escalation step(s) to which the operation is assigned<br>**Details** - type of operation and its recipient/target.<br>The operation list also displays the media type (e-mail, SMS or script) used as well as the name and surname (in parentheses after the username) of a notification recipient.<br>**Start in** - how long after an event the operation is performed<br>**Duration (sec)** - step duration is displayed. *Default* is displayed if the step uses default duration, and a time is displayed if custom duration is used.<br>**Action** - links for editing and removing an operation are displayed.|
|*Recovery operations*|<|<|<|<|Action operations (if any) are displayed, with these details:<br>**Details** - type of operation and its recipient/target.<br>The operation list also displays the media type (e-mail, SMS or script) used as well as the name and surname (in parentheses after the username) of a notification recipient.<br>**Action** - links for editing and removing an operation are displayed.|
|*Update operations*|<|<|<|<|Action operations (if any) are displayed, with these details:<br>**Details** - type of operation and its recipient/target.<br>The operation list also displays the media type (e-mail, SMS or script) used as well as the name and surname (in parentheses after the username) of a notification recipient.<br>**Action** - links for editing and removing an operation are displayed.|

All mandatory input fields are marked with a red asterisk.

To configure details of a new operation, click on
![](../../../../../assets/en/manual/config/add_link.png) in the
Operations block. To edit an existing operation, click on
![](../../../../../assets/en/manual/config/edit_link.png) next to the
operation. A popup window will open where you can edit the operation
step details.

[comment]: # ({/new-4b8a4b65})

[comment]: # ({new-4a37f720})
#### Operation details

![](../../../../../assets/en/manual/config/operation_details.png)

|Parameter|<|<|<|Description|
|---------|-|-|-|-----------|
|*Operation*|<|<|<|Select the operation:<br>**Send message** - send message to user<br>**<remote command name>** - execute a remote command. Commands are available for execution if previously defined in [global scripts](/manual/web_interface/frontend_sections/administration/scripts#configuring_a_global_script) with *Action operation* selected as its scope.<br>More operations are available for discovery and autoregistration based events (see above).|
|*Steps*|<|<|<|Select the step(s) to assign the operation to in an [escalation](escalations) schedule:<br>**From** - execute starting with this step<br>**To** - execute until this step (0=infinity, execution will not be limited)|
|*Step duration*|<|<|<|Custom duration for these steps (0=use default step duration).<br>[Time suffixes](/manual/appendix/suffixes) are supported, e.g. 60s, 1m, 2h, 1d, since Zabbix 3.4.0.<br>[User macros](/manual/config/macros/user_macros) are supported, since Zabbix 3.4.0.<br>Several operations can be assigned to the same step. If these operations have different step duration defined, the shortest one is taken into account and applied to the step.|
|<|Operation type: [send message](/manual/config/notifications/action/operation/message)|<|<|<|
|^|*Send to user groups*|<|<|Click on *Add* to select user groups to send the message to.<br>The user group must have at least "read" [permissions](/manual/config/users_and_usergroups/permissions) to the host in order to be notified.|
|^|*Send to users*|<|<|Click on *Add* to select users to send the message to.<br>The user must have at least "read" [permissions](/manual/config/users_and_usergroups/permissions) to the host in order to be notified.|
|^|*Send only to*|<|<|Send message to all defined media types or a selected one only.|
|^|*Custom message*|<|<|If selected, the custom message can be configured.<br>For notifications about internal events via [webhooks](/manual/config/notifications/media/webhook), custom message is mandatory.|
|^|*Subject*|<|<|Subject of the custom message. The subject may contain macros. It is limited to 255 characters.|
|^|*Message*|<|<|The custom message. The message may contain macros. It is limited to certain amount of characters depending on the type of database (see [Sending message](/manual/config/notifications/action/operation/message) for more information).|
|^|Operation type: [remote command](/manual/config/notifications/action/operation/remote_command)|<|<|<|
|^|*Target list*|<|<|Select targets to execute the command on:<br>**Current host** - command is executed on the host of the trigger that caused the problem event. This option will not work if there are multiple hosts in the trigger.<br>**Host** - select host(s) to execute the command on.<br>**Host group** - select host group(s) to execute the command on. Specifying a parent host group implicitly selects all nested host groups. Thus the remote command will also be executed on hosts from nested groups.<br>A command on a host is executed only once, even if the host matches more than once (e.g. from several host groups; individually and from a host group).<br>The target list is meaningless if a custom script is executed on Zabbix server. Selecting more targets in this case only results in the script being executed on the server more times.<br>Note that for global scripts, the target selection also depends on the *Host group* setting in global script [configuration](/manual/web_interface/frontend_sections/administration/scripts#configuring_a_global_script).<br>*Target list* option is not available for *Service actions* because in this case remote commands are always executed on Zabbix server.|
|*Conditions*|<|<|<|Condition for performing the operation:<br>**Not ack** - only when the event is unacknowledged<br>**Ack** - only when the event is acknowledged.<br>*Conditions* option is not available for *Service actions*.|

When done, click on *Add* to add the operation to the list of
*Operations*.

[comment]: # ({/new-4a37f720})
