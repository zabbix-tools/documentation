[comment]: # translation:outdated

[comment]: # ({new-10ba847f})
# 3 Recovery operations

[comment]: # ({/new-10ba847f})

[comment]: # ({new-e5432fa9})
#### Overview

Recovery operations allow you to be notified when problems are resolved.

Both messages and remote commands are supported in recovery operations.
While several operations can be added, escalation is not supported - all
operations are assigned to a single step and therefore will be performed
simultaneously.

[comment]: # ({/new-e5432fa9})

[comment]: # ({new-960d3247})
#### Use cases

Some use cases for recovery operations are as follows:

1.  Notify all users that were notified on the problem

```{=html}
<!-- -->
```
       * Select 'Send recovery message' as operation type
    - Have multiple operations upon recovery: send a notification and execute a remote command
       * Add operation types for sending a message and executing a command
    - Open a ticket in external helpdesk/ticketing system and close it when the problem is resolved
       * Create an external script that communicates with the helpdesk system
       * Create an action having operation that executes this script and thus opens a ticket
       * Have a recovery operation that executes this script with other parameters and closes the ticket
       * Use the {EVENT.ID} macro to reference the original problem

[comment]: # ({/new-960d3247})

[comment]: # ({new-82501aec})
#### Configuring a recovery operation

To configure a recovery operation, go to the *Operations* tab in
[action](/manual/config/notifications/action) configuration.

![](../../../../../assets/en/manual/config/recovery_operation.png)

To configure details of a new recovery operation, click on
![](../../../../../assets/en/manual/config/add_link.png) in the Recovery
operations block. To edit an existing operation, click on
![](../../../../../assets/en/manual/config/edit_link.png) next to the
operation. A popup window will open where you can edit the operation
step details.

[comment]: # ({/new-82501aec})

[comment]: # ({new-f2127d8b})
#### Recovery operation details

![](../../../../../assets/en/manual/config/recovery_operation_details.png)

|Parameter|<|<|<|Description|
|---------|-|-|-|-----------|
|*Operation*|<|<|<|Three operation types are available for recovery events:<br>**Send message** - send recovery message to specified user<br>**Notify all involved** - send recovery message to all users who were notified on the problem event<br>**<remote command name>** - execute a remote command. Commands are available for execution if previously defined in [global scripts](/manual/web_interface/frontend_sections/administration/scripts#configuring_a_global_script) with *Action operation* selected as its scope.<br>Note that if the same recipient is defined in several operation types without specified *Custom message*, duplicate notifications are not sent.|
|<|Operation type: [send message](/manual/config/notifications/action/operation/message)|<|<|<|
|^|*Send to user groups*|<|<|Click on *Add* to select user groups to send the recovery message to.<br>The user group must have at least "read" [permissions](/manual/config/users_and_usergroups/permissions) to the host in order to be notified.|
|^|*Send to users*|<|<|Click on *Add* to select users to send the recovery message to.<br>The user must have at least "read" [permissions](/manual/config/users_and_usergroups/permissions) to the host in order to be notified.|
|^|*Send only to*|<|<|Send default recovery message to all defined media types or a selected one only.|
|^|*Custom message*|<|<|If selected, a custom message can be defined.|
|^|*Subject*|<|<|Subject of the custom message. The subject may contain macros.|
|^|*Message*|<|<|The custom message. The message may contain macros.|
|^|Operation type: [remote command](/manual/config/notifications/action/operation/remote_command)|<|<|<|
|^|*Target list*|<|<|Select targets to execute the command on:<br>**Current host** - command is executed on the host of the trigger that caused the problem event. This option will not work if there are multiple hosts in the trigger.<br>**Host** - select host(s) to execute the command on.<br>**Host group** - select host group(s) to execute the command on. Specifying a parent host group implicitly selects all nested host groups. Thus the remote command will also be executed on hosts from nested groups.<br>A command on a host is executed only once, even if the host matches more than once (e.g. from several host groups; individually and from a host group).<br>The target list is meaningless if the command is executed on Zabbix server. Selecting more targets in this case only results in the command being executed on the server more times.<br>Note that for global scripts, the target selection also depends on the *Host group* setting in global script [configuration](/manual/web_interface/frontend_sections/administration/scripts#configuring_a_global_script).|
|<|Operation type: notify all involved|<|<|<|
|^|*Custom message*|<|<|If selected, a custom message can be defined.|
|^|*Subject*|<|<|Subject of the custom message. The subject may contain macros.|
|^|*Message*|<|<|The custom message. The message may contain macros.|

All mandatory input fields are marked with a red asterisk. When done,
click on *Add* to add operation to the list of *Recovery operations*.

[comment]: # ({/new-f2127d8b})

[comment]: # ({new-7dbd8d4d})

#### Operation type: [send message](/manual/config/notifications/action/operation/message)

|Parameter|Description|
|--|--------|
|*Send to user groups*|Click on *Add* to select user groups to send the recovery message to.<br>The user group must have at least "read" [permissions](/manual/config/users_and_usergroups/permissions) to the host in order to be notified.|
|*Send to users*|Click on *Add* to select users to send the recovery message to.<br>The user must have at least "read" [permissions](/manual/config/users_and_usergroups/permissions) to the host in order to be notified.|
|*Send only to*|Send default recovery message to all defined media types or a selected one only.|
|*Custom message*|If selected, a custom message can be defined.|
|*Subject*|Subject of the custom message. The subject may contain macros.|
|*Message*|The custom message. The message may contain macros.|

[comment]: # ({/new-7dbd8d4d})

[comment]: # ({new-37fc532b})

#### Operation type: [remote command](/manual/config/notifications/action/operation/remote_command)

|Parameter|Description|
|--|--------|
|*Target list*|Select targets to execute the command on:<br>**Current host** - command is executed on the host of the trigger that caused the problem event. This option will not work if there are multiple hosts in the trigger.<br>**Host** - select host(s) to execute the command on.<br>**Host group** - select host group(s) to execute the command on. Specifying a parent host group implicitly selects all nested host groups. Thus the remote command will also be executed on hosts from nested groups.<br>A command on a host is executed only once, even if the host matches more than once (e.g. from several host groups; individually and from a host group).<br>The target list is meaningless if the command is executed on Zabbix server. Selecting more targets in this case only results in the command being executed on the server more times.<br>Note that for global scripts, the target selection also depends on the *Host group* setting in global script [configuration](/manual/web_interface/frontend_sections/administration/scripts#configuring_a_global_script).|

[comment]: # ({/new-37fc532b})

[comment]: # ({new-945f9a49})

#### Operation type: notify all involved

|Parameter|Description|
|--|--------|
|*Custom message*|If selected, a custom message can be defined.|
|*Subject*|Subject of the custom message. The subject may contain macros.|
|*Message*|The custom message. The message may contain macros.|


[comment]: # ({/new-945f9a49})
