[comment]: # translation:outdated

[comment]: # ({new-3222767b})
# 1 Configuring a template

[comment]: # ({/new-3222767b})

[comment]: # ({new-630aea00})
#### Overview

Configuring a template requires that you first create a template by
defining its general parameters and then you add entities (items,
triggers, graphs, etc.) to it.

[comment]: # ({/new-630aea00})

[comment]: # ({new-de7b2ff6})
#### Creating a template

To create a template, do the following:

-   Go to *Configuration → Templates*
-   Click on *Create template*
-   Edit template attributes

The **Templates** tab contains general template attributes.

![](../../../../assets/en/manual/config/template_a.png)

All mandatory input fields are marked with a red asterisk.

Template attributes:

|Parameter|Description|
|---------|-----------|
|*Template name*|Unique template name. Alphanumerics, spaces, dots, dashes, and underscores are allowed. However, leading and trailing spaces are disallowed.|
|*Visible name*|If you set this name, it will be the one visible in lists, maps, etc.|
|*Templates*|Link one or more "nested" templates to this template. All entities (items, triggers, graphs, etc.) will be inherited from the linked templates.<br>To link a new template, start typing the template name in the *Link new templates* field. A list of matching templates will appear; scroll down to select. Alternatively, you may click on *Select* next to the field and select templates from the list in a popup window. The templates that are selected in the *Link new templates* field will be linked to the template when the template configuration form is saved or updated.<br>To unlink a template, use one of the two options in the *Linked templates* block:<br>*Unlink* - unlink the template, but preserve its items, triggers, and graphs<br>*Unlink and clear* - unlink the template and remove all its items, triggers, and graphs|
|*Groups*|Host/template groups the template belongs to.|
|*Description*|Enter the template description.|

The **Tags** tab allows you to define template-level
[tags](/manual/config/tagging). All problems of hosts linked to this
template will be tagged with the values entered here.

![](../../../../assets/en/manual/config/template_c.png)

User macros, {INVENTORY.\*} macros, {HOST.HOST}, {HOST.NAME},
{HOST.CONN}, {HOST.DNS}, {HOST.IP}, {HOST.PORT} and {HOST.ID} macros are
supported in tags.

The **Macros** tab allows you to define template-level [user
macros](/manual/config/macros/user_macros) as a name-value pairs. Note
that macro values can be kept as plain text, secret text, or Vault
secret. Adding a description is also supported.

![](../../../../assets/en/manual/config/template_d1.png)

You may also view here macros from linked templates and global macros if
you select the *Inherited and template macros* option. That is where all
defined user macros for the template are displayed with the value they
resolve to as well as their origin.

![](../../../../assets/en/manual/config/template_d2.png)

For convenience, links to respective templates and global macro
configuration are provided. It is also possible to edit a nested
template/global macro on the template level, effectively creating a copy
of the macro on the template.

The **Value mapping** tab allows to configure human-friendly
representation of item data in [value
mappings](/manual/config/items/mapping).

Buttons:

|   |   |
|---|---|
|![](../../../../assets/en/manual/config/button_add.png)|Add the template. The added template should appear in the list.|
|![](../../../../assets/en/manual/config/button_update.png)|Update the properties of an existing template.|
|![](../../../../assets/en/manual/config/button_clone.png)|Create another template based on the properties of the current template, including the entities (items, triggers, etc) inherited from linked templates.|
|![](../../../../assets/en/manual/config/button_full.png)|Create another template based on the properties of the current template, including the entities (items, triggers, etc) both inherited from linked templates and directly attached to the current template.|
|![](../../../../assets/en/manual/config/button_delete.png)|Delete the template; entities of the template (items, triggers, etc) remain with the linked hosts.|
|![](../../../../assets/en/manual/config/button_clear.png)|Delete the template and all its entities from linked hosts.|
|![](../../../../assets/en/manual/config/button_cancel.png)|Cancel the editing of template properties.|

With a template created, it is time to add some entities to it.

::: noteimportant
Items have to be added to a template first.
Triggers and graphs cannot be added without the corresponding
item.
:::

[comment]: # ({/new-de7b2ff6})

[comment]: # ({new-0d973911})
#### Adding items, triggers, graphs

To add items to the template, do the following:

-   Go to *Configuration → Hosts* (or *Templates*)
-   Click on *Items* in the row of the required host/template
-   Mark the checkboxes of items you want to add to the template
-   Click on *Copy* below the item list
-   Select the template (or group of templates) the items should be
    copied to and click on *Copy*

All the selected items should be copied to the template.

Adding triggers and graphs is done in a similar fashion (from the list
of triggers and graphs respectively), again, keeping in mind that they
can only be added if the required items are added first.

[comment]: # ({/new-0d973911})

[comment]: # ({new-b78a3626})
#### Adding dashboards

To add dashboards to a template in *Configuration → Templates*, do the
following:

-   Click on *Dashboards* in the row of the template
-   Configure a dashboard following the guidelines of [configuring
    dashboards](/manual/web_interface/frontend_sections/monitoring/dashboard)

::: noteimportant
The widgets that can be included in a template
dashboard are: classic graph, graph prototype, clock, plain text,
URL.
:::

::: notetip
For details on accessing host dashboards that are
created from template dashboards, see the [host
dashboard](/manual/config/visualization/host_screens#accessing_host_dashboards)
section.
:::

[comment]: # ({/new-b78a3626})

[comment]: # ({new-513e93e5})
#### Configuring low-level discovery rules

See the [low-level discovery](/manual/discovery/low_level_discovery)
section of the manual.

[comment]: # ({/new-513e93e5})

[comment]: # ({new-e3935a9d})
#### Adding web scenarios

To add web scenarios to a template in *Configuration → Templates*, do
the following:

-   Click on *Web* in the row of the template
-   Configure a web scenario following the usual method of [configuring
    web scenarios](/manual/web_monitoring#configuring_a_web_scenario)

[comment]: # ({/new-e3935a9d})

[comment]: # ({new-d357b380})
#### Creating a template group

::: noteimportant
Only Super Admin users can create template groups.
:::

To create a template group in Zabbix frontend, do the following:

-   Go to: *Configuration → Template groups*
-   Click on *Create template group* in the upper right corner of the screen
-   Enter the group name in the form

![](../../../../assets/en/manual/config/template_group.png)

To create a nested template group, use the '/' forward slash separator, for example `Linux servers/Databases/MySQL`. You can create this group even if none of the two parent template groups (`Linux servers/Databases/`) exist. In this case creating these parent template groups is up to the user; they will not be created automatically.<br>Leading and trailing slashes, several slashes in a row are not allowed. Escaping of '/' is not supported.

[comment]: # ({/new-d357b380})

[comment]: # ({new-ed4ea073})

Once the group is created, you can click on the group name in the list to edit group name, clone the group or set additional option:

![](../../../../assets/en/manual/config/template_group2.png)

*Apply permissions to all subgroups* - mark this checkbox and click on *Update* to apply the same level of permissions to all nested template groups. For user groups that may have had differing [permissions](/manual/config/users_and_usergroups/usergroup#configuration) assigned to nested template groups, the permission level of the parent template group will be enforced on the nested groups. This is a one-time option that is not saved in the database.

**Permissions to nested template groups**

-   When creating a child template group to an existing parent template group,
    [user group](/manual/config/users_and_usergroups/usergroup)
    permissions to the child are inherited from the parent (for example,
    when creating `Databases/MySQL` if `Databases` already exists)
-   When creating a parent template group to an existing child template group,
    no permissions to the parent are set (for example, when creating
    `Databases` if `Databases/MySQL` already exists)

[comment]: # ({/new-ed4ea073})
