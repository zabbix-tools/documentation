[comment]: # translation:outdated

[comment]: # ({new-fb204a33})
# 9 Discovery of Windows performance counter instances

[comment]: # ({/new-fb204a33})

[comment]: # ({new-f150cb4f})
#### Overview

It is possible to
[discover](/manual/discovery/low_level_discovery#configuring_low-level_discovery)
object instances of Windows performance counters. This is useful for
multi-instance performance counters.

[comment]: # ({/new-f150cb4f})

[comment]: # ({new-b89c3c17})
#### Item key

The item to use in the [discovery
rule](/manual/discovery/low_level_discovery#discovery_rule) is

    perf_instance.discovery[object]

or, to be able to provide the object name in English only, independently
of OS localization:

    perf_instance_en.discovery[object]

For example:

    perf_instance.discovery[Processador]
    perf_instance_en.discovery[Processor]

These items are supported since Zabbix Windows agent 5.0.1.

[comment]: # ({/new-b89c3c17})

[comment]: # ({new-4407d98a})
#### Supported macros

The discovery will return all instances of the specified object in the
{\#INSTANCE} macro, which may be used in the prototypes of perf\_count
and perf\_count\_en items.

``` {.java}
[
    {"{#INSTANCE}":"0"},
    {"{#INSTANCE}":"1"},
    {"{#INSTANCE}":"_Total"}
]
```

For example, if the item key used in the discovery rule is:

    perf_instance.discovery[Processor]

you may create an item prototype:

    perf_counter["\Processor({#INSTANCE})\% Processor Time"]

Notes:

-   If the specified object is not found or does not support variable
    instances then the discovery item will become NOTSUPPORTED.
-   If the specified object supports variable instances, but currently
    does not have any instances, then an empty JSON array will be
    returned.
-   In case of duplicate instances they will be skipped.

[comment]: # ({/new-4407d98a})
