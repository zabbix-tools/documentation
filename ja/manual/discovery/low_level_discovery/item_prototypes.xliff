<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="ja" datatype="plaintext" original="manual/discovery/low_level_discovery/item_prototypes.md">
    <body>
      <trans-unit id="806f98ad" xml:space="preserve">
        <source># 1 Item prototypes

Once a rule is created, go to the items for that rule and press "Create
item prototype" to create an item prototype. 

Note how the {\#FSNAME} macro is used where a file system name is required. 
The use of a low-level discovery macro is mandatory in the item key to make sure 
that the discovery is processed correctly. When the discovery rule is processed, 
this macro will be substituted with the discovered file system.

![](../../../../assets/en/manual/discovery/low_level_discovery/item_prototype_fs.png)

Low-level discovery [macros](/manual/config/macros/lld_macros) and user
[macros](/manual/appendix/macros/supported_by_location_user) are supported
in item prototype configuration and item value preprocessing
[parameters](/manual/config/items/item#item_value_preprocessing). Note
that when used in update intervals, a single macro has to fill the whole
field. Multiple macros in one field or macros mixed with text are not
supported.

::: noteclassic
Context-specific escaping of low-level discovery macros is
performed for safe use in regular expression and XPath preprocessing
parameters.
:::

Attributes that are specific for item prototypes:

|Parameter|Description|
|--|--------|
|*Create enabled*|If checked the item will be added in an enabled state.&lt;br&gt;If unchecked, the item will be added to a discovered entity, but in a disabled state.|
|*Discover*|If checked (default) the item will be added to a discovered entity.&lt;br&gt;If unchecked, the item will not be added to a discovered entity, unless this setting is [overridden](/manual/discovery/low_level_discovery#override) in the discovery rule.|

We can create several item prototypes for each file system metric we are
interested in:

![](../../../../assets/en/manual/discovery/low_level_discovery/item_prototypes_fs.png)

Click on the three-dot icon to open the menu for the specific item prototype with these options:&lt;br&gt;
- *Create trigger prototype* - create a trigger prototype based on this item prototype
- *Trigger prototypes* - click to see a list with links to already-configured trigger prototypes of this item prototype
- *Create dependent item* - create a dependent item for this item prototype

*[Mass update](/manual/config/items/itemupdate#using_mass_update)*
option is available if you want to update properties of several item
prototypes at once.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
