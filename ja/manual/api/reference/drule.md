[comment]: # translation:outdated

[comment]: # ({new-8674a4c3})
# Discovery rule

This class is designed to work with network discovery rules.

::: notetip
This API is meant to work with network discovery rules.
For the low-level discovery rules see the [LLD rule
API](discoveryrule).
:::

Object references:\

-   [Discovery rule](/manual/api/reference/drule/object#discovery_rule)

Available methods:\

-   [drule.create](/manual/api/reference/drule/create) - create new
    discovery rules
-   [drule.delete](/manual/api/reference/drule/delete) - delete
    discovery rules
-   [drule.get](/manual/api/reference/drule/get) - retrieve discovery
    rules
-   [drule.update](/manual/api/reference/drule/update) - update
    discovery rules

[comment]: # ({/new-8674a4c3})
