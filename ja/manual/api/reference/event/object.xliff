<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="ja" datatype="plaintext" original="manual/api/reference/event/object.md">
    <body>
      <trans-unit id="bef38847" xml:space="preserve">
        <source># &gt; Event object

The following objects are directly related to the `event` API.</source>
      </trans-unit>
      <trans-unit id="355941f0" xml:space="preserve">
        <source>### Event

::: noteclassic
Events are created by the Zabbix server and cannot be
modified via the API.
:::

The event object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|eventid|string|ID of the event.|
|source|integer|Type of the event.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - event created by a trigger;&lt;br&gt;1 - event created by a discovery rule;&lt;br&gt;2 - event created by active agent autoregistration;&lt;br&gt;3 - internal event;&lt;br&gt;4 - event created on service status update.|
|object|integer|Type of object that is related to the event.&lt;br&gt;&lt;br&gt;Possible values if `source` is set to "event created by a trigger":&lt;br&gt;0 - trigger.&lt;br&gt;&lt;br&gt;Possible values if `source` is set to "event created by a discovery rule":&lt;br&gt;1 - discovered host;&lt;br&gt;2 - discovered service.&lt;br&gt;&lt;br&gt;Possible values if `source` is set to "event created by active agent autoregistration":&lt;br&gt;3 - auto-registered host.&lt;br&gt;&lt;br&gt;Possible values if `source` is set to "internal event":&lt;br&gt;0 - trigger;&lt;br&gt;4 - item;&lt;br&gt;5 - LLD rule.&lt;br&gt;&lt;br&gt;Possible values if `source` is set to "event created on service status update":&lt;br&gt;6 - service.|
|objectid|string|ID of the related object.|
|acknowledged|integer|Whether the event has been acknowledged.|
|clock|timestamp|Time when the event was created.|
|ns|integer|Nanoseconds when the event was created.|
|name|string|Resolved event name.|
|value|integer|State of the related object.&lt;br&gt;&lt;br&gt;Possible values if `source` is set to "event created by a trigger" or "event created on service status update":&lt;br&gt;0 - OK;&lt;br&gt;1 - problem.&lt;br&gt;&lt;br&gt;Possible values if `source` is set to "event created by a discovery rule":&lt;br&gt;0 - host or service up;&lt;br&gt;1 - host or service down;&lt;br&gt;2 - host or service discovered;&lt;br&gt;3 - host or service lost.&lt;br&gt;&lt;br&gt;Possible values if `source` is seto to "internal event":&lt;br&gt;0 - "normal" state;&lt;br&gt;1 - "unknown" or "not supported" state.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `source` is set to "event created by a trigger", "event created by a discovery rule", "internal event", or "event created on service status update"|
|severity|integer|Event current severity.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - not classified;&lt;br&gt;1 - information;&lt;br&gt;2 - warning;&lt;br&gt;3 - average;&lt;br&gt;4 - high;&lt;br&gt;5 - disaster.|
|r\_eventid|string|Recovery event ID.|
|c\_eventid|string|ID of the event that was used to override (close) current event under global correlation rule. See `correlationid` to identify exact correlation rule.&lt;br&gt;This parameter is only defined when the event is closed by global correlation rule.|
|cause_eventid|string|Cause event ID.|
|correlationid|string|ID of the correlation rule that generated closing of the problem.&lt;br&gt;This parameter is only defined when the event is closed by global correlation rule.|
|userid|string|User ID if the event was manually closed.|
|suppressed|integer|Whether the event is suppressed.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - event is in normal state;&lt;br&gt;1 - event is suppressed.|
|opdata|string|Operational data with expanded macros.|
|urls|array|Active [media type URLs](/manual/api/reference/problem/object#media-type-url).|</source>
      </trans-unit>
      <trans-unit id="b89991b4" xml:space="preserve">
        <source>### Event tag

The event tag object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|tag|string|Event tag name.|
|value|string|Event tag value.|</source>
      </trans-unit>
      <trans-unit id="52ff7df7" xml:space="preserve">
        <source>### Media type URL

The media type URL object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|name|string|Media type defined URL name.|
|url|string|Media type defined URL value.|

Results will contain entries only for active media types with enabled event menu entry.
Macro used in properties will be expanded, but if one of the properties contains an unexpanded macro, both properties will be excluded from results.
For supported macros, see [*Supported macros*](/manual/appendix/macros/supported_by_location).</source>
      </trans-unit>
    </body>
  </file>
</xliff>
