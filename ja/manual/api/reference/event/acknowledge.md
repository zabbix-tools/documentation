[comment]: # translation:outdated

[comment]: # ({new-2ccfd922})
# event.acknowledge

[comment]: # ({/new-2ccfd922})

[comment]: # ({new-eed7fdda})
### Description

`object event.acknowledge(object/array parameters)`

This method allows to update events. Following update actions can be
performed:

-   Close event. If event is already resolved, this action will be
    skipped.
-   Acknowledge event. If event is already acknowledged, this action
    will be skipped.
-   Add message.
-   Change event severity. If event already has same severity, this
    action will be skipped.

::: noteimportant
Only trigger events can be updated.\
Only problem events can be updated.\
Read/Write rights for trigger are required to close the event or to
change event's severity.\
To close an event, manual close should be allowed in the
trigger.
:::

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-eed7fdda})

[comment]: # ({new-32560b88})
### Parameters

`(object/array)` Parameters containing the IDs of the events and update
operations that should be performed.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|**eventids**<br>(required)|string/object|IDs of the events to acknowledge.|
|**action**<br>(required)|integer|Event update action(s). This is bitmask field, any combination of values is acceptable.<br><br>Possible values:<br>1 - close problem;<br>2 - acknowledge event;<br>4 - add message;<br>8 - change severity;<br>16 - unacknowledge event.|
|message|string|Text of the message.<br>**Required**, if action contains 'add message' flag.|
|severity|integer|New severity for events.<br>**Required**, if action contains 'change severity' flag.<br><br>Possible values:<br>0 - not classified;<br>1 - information;<br>2 - warning;<br>3 - average;<br>4 - high;<br>5 - disaster.|

[comment]: # ({/new-32560b88})

[comment]: # ({new-94f3ab84})
### Return values

`(object)` Returns an object containing the IDs of the updated events
under the `eventids` property.

[comment]: # ({/new-94f3ab84})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-9e923e4c})
#### Acknowledging an event

Acknowledge a single event and leave a message.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "event.acknowledge",
    "params": {
        "eventids": "20427",
        "action": 6,
        "message": "Problem resolved."
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "eventids": [
            "20427"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-9e923e4c})

[comment]: # ({new-94e25d7e})
#### Changing event's severity

Change severity for multiple events and leave a message.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "event.acknowledge",
    "params": {
        "eventids": ["20427", "20428"],
        "action": 12,
        "message": "Maintenance required to fix it.",
        "severity": 4
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "eventids": [
            "20427",
            "20428"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-94e25d7e})

[comment]: # ({new-aadd99f7})
### Source

CEvent::acknowledge() in *ui/include/classes/api/services/CEvent.php*.

[comment]: # ({/new-aadd99f7})
