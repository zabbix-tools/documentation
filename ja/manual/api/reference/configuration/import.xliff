<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="ja" datatype="plaintext" original="manual/api/reference/configuration/import.md">
    <body>
      <trans-unit id="1359fe15" xml:space="preserve">
        <source># configuration.import</source>
      </trans-unit>
      <trans-unit id="fe885a4a" xml:space="preserve">
        <source>### Description

`boolean configuration.import(object parameters)`

This method allows to import configuration data from a serialized string.

::: noteclassic
This method is available to users of any type.
Permissions to call the method can be revoked in user role settings.
See [User roles](/manual/web_interface/frontend_sections/users/user_roles) for more information.
:::</source>
      </trans-unit>
      <trans-unit id="147fd272" xml:space="preserve">
        <source>### Parameters

`(object)` Parameters containing the data to import and rules how the data should be handled.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|format|string|Format of the serialized string.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;`yaml` - YAML;&lt;br&gt;`xml` - XML;&lt;br&gt;`json` - JSON.&lt;br&gt;&lt;br&gt;[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):&lt;br&gt;- *required*|
|source|string|Serialized string containing the configuration data.&lt;br&gt;&lt;br&gt;[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):&lt;br&gt;- *required*|
|rules|object|Rules on how new and existing objects should be imported.&lt;br&gt;&lt;br&gt;The `rules` parameter is described in detail in the table below.&lt;br&gt;&lt;br&gt;[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):&lt;br&gt;- *required*|

::: notetip
If no rules are given, the configuration will not be updated.
:::

The `rules` object supports the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|discoveryRules|object|Rules on how to import LLD rules.&lt;br&gt;&lt;br&gt;Supported parameters:&lt;br&gt;`createMissing` - `(boolean)` if set to `true`, new LLD rules will be created; default: `false`;&lt;br&gt;`updateExisting` - `(boolean)` if set to `true`, existing LLD rules will be updated; default: `false`;&lt;br&gt;`deleteMissing` - `(boolean)` if set to `true`, LLD rules not present in the imported data will be deleted from the database; default: `false`.|
|graphs|object|Rules on how to import graphs.&lt;br&gt;&lt;br&gt;Supported parameters:&lt;br&gt;`createMissing` - `(boolean)` if set to `true`, new graphs will be created; default: `false`;&lt;br&gt;`updateExisting` - `(boolean)` if set to `true`, existing graphs will be updated; default: `false`;&lt;br&gt;`deleteMissing` - `(boolean)` if set to `true`, graphs not present in the imported data will be deleted from the database; default: `false`.|
|host_groups|object|Rules on how to import host groups.&lt;br&gt;&lt;br&gt;Supported parameters:&lt;br&gt;`createMissing` - `(boolean)` if set to `true`, new host groups will be created; default: `false`;&lt;br&gt;`updateExisting` - `(boolean)` if set to `true`, existing host groups will be updated; default: `false`.|
|template_groups|object|Rules on how to import template groups.&lt;br&gt;&lt;br&gt;Supported parameters:&lt;br&gt;`createMissing` - `(boolean)` if set to `true`, new template groups will be created; default: `false`;&lt;br&gt;`updateExisting` - `(boolean)` if set to `true`, existing template groups will be updated; default: `false`.|
|hosts|object|Rules on how to import hosts.&lt;br&gt;&lt;br&gt;Supported parameters:&lt;br&gt;`createMissing` - `(boolean)` if set to `true`, new hosts will be created; default: `false`;&lt;br&gt;`updateExisting` - `(boolean)` if set to `true`, existing hosts will be updated; default: `false`.|
|httptests|object|Rules on how to import web scenarios.&lt;br&gt;&lt;br&gt;Supported parameters:&lt;br&gt;`createMissing` - `(boolean)` if set to `true`, new web scenarios will be created; default: `false`;&lt;br&gt;`updateExisting` - `(boolean)` if set to `true`, existing web scenarios will be updated; default: `false`;&lt;br&gt;`deleteMissing` - `(boolean)` if set to `true`, web scenarios not present in the imported data will be deleted from the database; default: `false`.|
|images|object|Rules on how to import images.&lt;br&gt;&lt;br&gt;Supported parameters:&lt;br&gt;`createMissing` - `(boolean)` if set to `true`, new images will be created; default: `false`;&lt;br&gt;`updateExisting` - `(boolean)` if set to `true`, existing images will be updated; default: `false`.|
|items|object|Rules on how to import items.&lt;br&gt;&lt;br&gt;Supported parameters:&lt;br&gt;`createMissing` - `(boolean)` if set to `true`, new items will be created; default: `false`;&lt;br&gt;`updateExisting` - `(boolean)` if set to `true`, existing items will be updated; default: `false`;&lt;br&gt;`deleteMissing` - `(boolean)` if set to `true`, items not present in the imported data will be deleted from the database; default: `false`.|
|maps|object|Rules on how to import maps.&lt;br&gt;&lt;br&gt;Supported parameters:&lt;br&gt;`createMissing` - `(boolean)` if set to `true`, new maps will be created; default: `false`;&lt;br&gt;`updateExisting` - `(boolean)` if set to `true`, existing maps will be updated; default: `false`.|
|mediaTypes|object|Rules on how to import media types.&lt;br&gt;&lt;br&gt;Supported parameters:&lt;br&gt;`createMissing` - `(boolean)` if set to `true`, new media types will be created; default: `false`;&lt;br&gt;`updateExisting` - `(boolean)` if set to `true`, existing media types will be updated; default: `false`.|
|templateLinkage|object|Rules on how to import template links.&lt;br&gt;&lt;br&gt;Supported parameters:&lt;br&gt;`createMissing` - `(boolean)` if set to `true`, templates that are not linked to the host or template being imported, but are present in the imported data, will be linked; default: `false`;&lt;br&gt;`deleteMissing` - `(boolean)` if set to `true`, templates that are linked to the host or template being imported, but are not present in the imported data, will be unlinked without removing entities (items, triggers, etc.) inherited from the unlinked templates; default: `false`.|
|templates|object|Rules on how to import templates.&lt;br&gt;&lt;br&gt;Supported parameters:&lt;br&gt;`createMissing` - `(boolean)` if set to `true`, new templates will be created; default: `false`;&lt;br&gt;`updateExisting` - `(boolean)` if set to `true`, existing templates will be updated; default: `false`.|
|templateDashboards|object|Rules on how to import template dashboards.&lt;br&gt;&lt;br&gt;Supported parameters:&lt;br&gt;`createMissing` - `(boolean)` if set to `true`, new template dashboards will be created; default: `false`;&lt;br&gt;`updateExisting` - `(boolean)` if set to `true`, existing template dashboards will be updated; default: `false`;&lt;br&gt;`deleteMissing` - `(boolean)` if set to `true`, template dashboards not present in the imported data will be deleted from the database; default: `false`.|
|triggers|object|Rules on how to import triggers.&lt;br&gt;&lt;br&gt;Supported parameters:&lt;br&gt;`createMissing` - `(boolean)` if set to `true`, new triggers will be created; default: `false`;&lt;br&gt;`updateExisting` - `(boolean)` if set to `true`, existing triggers will be updated; default: `false`;&lt;br&gt;`deleteMissing` - `(boolean)` if set to `true`, triggers not present in the imported data will be deleted from the database; default: `false`.|
|valueMaps|object|Rules on how to import host or template value maps.&lt;br&gt;&lt;br&gt;Supported parameters:&lt;br&gt;`createMissing` - `(boolean)` if set to `true`, new value maps will be created; default: `false`;&lt;br&gt;`updateExisting` - `(boolean)` if set to `true`, existing value maps will be updated; default: `false`;&lt;br&gt;`deleteMissing` - `(boolean)` if set to `true`, value maps not present in the imported data will be deleted from the database; default: `false`.|</source>
      </trans-unit>
      <trans-unit id="08d02880" xml:space="preserve">
        <source>### Return values

`(boolean)` Returns `true` if importing has been successful.</source>
      </trans-unit>
      <trans-unit id="b41637d2" xml:space="preserve">
        <source>### Examples</source>
      </trans-unit>
      <trans-unit id="0801380e" xml:space="preserve">
        <source>#### Importing a template

Import the template configuration contained in the XML string.
If any items or triggers in the XML string are missing, they will be deleted from the database, and everything else will be left unchanged.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "configuration.import",
    "params": {
        "format": "xml",
        "rules": {
            "templates": {
                "createMissing": true,
                "updateExisting": true
            },
            "items": {
                "createMissing": true,
                "updateExisting": true,
                "deleteMissing": true
            },
            "triggers": {
                "createMissing": true,
                "updateExisting": true,
                "deleteMissing": true
            },
            "valueMaps": {
                "createMissing": true,
                "updateExisting": false
            }
        },
        "source": "&lt;?xml version=\"1.0\" encoding=\"UTF-8\"?&gt;\n&lt;zabbix_export&gt;&lt;version&gt;6.4&lt;/version&gt;&lt;template_groups&gt;&lt;template_group&gt;&lt;uuid&gt;7df96b18c230490a9a0a9e2307226338&lt;/uuid&gt;&lt;name&gt;Templates&lt;/name&gt;&lt;/template_group&gt;&lt;/template_groups&gt;&lt;templates&gt;&lt;template&gt;&lt;uuid&gt;5aef0444a82a4d8cb7a95dc4c0c85330&lt;/uuid&gt;&lt;template&gt;New template&lt;/template&gt;&lt;name&gt;New template&lt;/name&gt;&lt;groups&gt;&lt;group&gt;&lt;name&gt;Templates&lt;/name&gt;&lt;/group&gt;&lt;/groups&gt;&lt;items&gt;&lt;item&gt;&lt;uuid&gt;7f1e6f1e48aa4a128e5b6a958a5d11c3&lt;/uuid&gt;&lt;name&gt;Zabbix agent ping&lt;/name&gt;&lt;key&gt;agent.ping&lt;/key&gt;&lt;/item&gt;&lt;item&gt;&lt;uuid&gt;77ba228662be4570830aa3c503fcdc03&lt;/uuid&gt;&lt;name&gt;Apache server uptime&lt;/name&gt;&lt;type&gt;DEPENDENT&lt;/type&gt;&lt;key&gt;apache.server.uptime&lt;/key&gt;&lt;delay&gt;0&lt;/delay&gt;&lt;trends&gt;0&lt;/trends&gt;&lt;value_type&gt;TEXT&lt;/value_type&gt;&lt;preprocessing&gt;&lt;step&gt;&lt;type&gt;REGEX&lt;/type&gt;&lt;parameters&gt;&lt;parameter&gt;&amp;lt;dt&amp;gt;Server uptime: (.*)&amp;lt;\/dt&amp;gt;&lt;/parameter&gt;&lt;parameter&gt;\\1&lt;/parameter&gt;&lt;/parameters&gt;&lt;/step&gt;&lt;/preprocessing&gt;&lt;master_item&gt;&lt;key&gt;web.page.get[127.0.0.1/server-status]&lt;/key&gt;&lt;/master_item&gt;&lt;/item&gt;&lt;item&gt;&lt;uuid&gt;6805d4c39a624a8bab2cc8ab63df1ab3&lt;/uuid&gt;&lt;name&gt;CPU load&lt;/name&gt;&lt;key&gt;system.cpu.load&lt;/key&gt;&lt;value_type&gt;FLOAT&lt;/value_type&gt;&lt;triggers&gt;&lt;trigger&gt;&lt;uuid&gt;ab4c2526c2bc42e48a633082255ebcb3&lt;/uuid&gt;&lt;expression&gt;avg(/New template/system.cpu.load,3m)&amp;gt;2&lt;/expression&gt;&lt;name&gt;CPU load too high on 'New host' for 3 minutes&lt;/name&gt;&lt;priority&gt;WARNING&lt;/priority&gt;&lt;/trigger&gt;&lt;/triggers&gt;&lt;/item&gt;&lt;item&gt;&lt;uuid&gt;590efe5731254f089265c76ff9320726&lt;/uuid&gt;&lt;name&gt;Apache server status&lt;/name&gt;&lt;key&gt;web.page.get[127.0.0.1/server-status]&lt;/key&gt;&lt;trends&gt;0&lt;/trends&gt;&lt;value_type&gt;TEXT&lt;/value_type&gt;&lt;/item&gt;&lt;/items&gt;&lt;valuemaps&gt;&lt;valuemap&gt;&lt;uuid&gt;8fd5814c45d44a00a15ac6eaae1f3946&lt;/uuid&gt;&lt;name&gt;Zabbix agent ping&lt;/name&gt;&lt;mappings&gt;&lt;mapping&gt;&lt;value&gt;1&lt;/value&gt;&lt;newvalue&gt;Available&lt;/newvalue&gt;&lt;/mapping&gt;&lt;mapping&gt;&lt;value&gt;0&lt;/value&gt;&lt;newvalue&gt;Not available&lt;/newvalue&gt;&lt;/mapping&gt;&lt;/mappings&gt;&lt;/valuemap&gt;&lt;/valuemaps&gt;&lt;/template&gt;&lt;/templates&gt;&lt;/zabbix_export&gt;\n"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": true,
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="c5744b74" xml:space="preserve">
        <source>### Source

CConfiguration::import() in *ui/include/classes/api/services/CConfiguration.php*.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
