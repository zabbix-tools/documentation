[comment]: # translation:outdated

[comment]: # ({new-da0b13a3})
# > Regular expression object

The following objects are directly related to the `regexp` API.

[comment]: # ({/new-da0b13a3})

[comment]: # ({new-37784778})
### Regular expression

The global regular expression object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|regexpid|string|*(readonly)* ID of the regular expression.|
|**name**<br>(required)|string|Name of the regular expression.|
|test\_string|string|Test string.|

[comment]: # ({/new-37784778})

[comment]: # ({new-ecdb1dfd})
### Expressions object

The expressions object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**expression**<br>(required)|string|Regular expression.|
|**expression\_type**<br>(required)|integer|Type of Regular expression.<br><br>Possible values:<br>0 - Character string included;<br>1 - Any character string included;<br>2 - Character string not included;<br>3 - Result is TRUE;<br>4 - Result is FALSE.|
|exp\_delimiter|string|Expression delimiter. Only when `expression_type` *Any character string included*.<br><br>Default value `,`.<br><br>Possible values: `,`, `.`, `/`.|
|case\_sensitive|integer|Case sensitivity.<br><br>Default value `0`.<br><br>Possible values:<br>0 - Case insensitive;<br>1 - Case sensitive.|

[comment]: # ({/new-ecdb1dfd})
