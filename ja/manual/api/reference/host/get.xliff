<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="ja" datatype="plaintext" original="manual/api/reference/host/get.md">
    <body>
      <trans-unit id="65bbffce" xml:space="preserve">
        <source># host.get</source>
      </trans-unit>
      <trans-unit id="93035b19" xml:space="preserve">
        <source>### Description

`integer/array host.get(object parameters)`

The method allows to retrieve hosts according to the given parameters.

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::</source>
      </trans-unit>
      <trans-unit id="77a8fabb" xml:space="preserve">
        <source>### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|groupids|string/array|Return only hosts that belong to the given groups.|
|dserviceids|string/array|Return only hosts that are related to the given discovered services.|
|graphids|string/array|Return only hosts that have the given graphs.|
|hostids|string/array|Return only hosts with the given host IDs.|
|httptestids|string/array|Return only hosts that have the given web checks.|
|interfaceids|string/array|Return only hosts that use the given interfaces.|
|itemids|string/array|Return only hosts that have the given items.|
|maintenanceids|string/array|Return only hosts that are affected by the given maintenances.|
|monitored\_hosts|flag|Return only monitored hosts.|
|proxy\_hosts|flag|Return only proxies.|
|proxyids|string/array|Return only hosts that are monitored by the given proxies.|
|templated\_hosts|flag|Return both hosts and templates.|
|templateids|string/array|Return only hosts that are linked to the given templates.|
|triggerids|string/array|Return only hosts that have the given triggers.|
|with\_items|flag|Return only hosts that have items.&lt;br&gt;&lt;br&gt;Overrides the `with_monitored_items` and `with_simple_graph_items` parameters.|
|with\_item\_prototypes|flag|Return only hosts that have item prototypes.&lt;br&gt;&lt;br&gt;Overrides the `with_simple_graph_item_prototypes` parameter.|
|with\_simple\_graph\_item\_prototypes|flag|Return only hosts that have item prototypes, which are enabled for creation and have numeric type of information.|
|with\_graphs|flag|Return only hosts that have graphs.|
|with\_graph\_prototypes|flag|Return only hosts that have graph prototypes.|
|with\_httptests|flag|Return only hosts that have web checks.&lt;br&gt;&lt;br&gt;Overrides the `with_monitored_httptests` parameter.|
|with\_monitored\_httptests|flag|Return only hosts that have enabled web checks.|
|with\_monitored\_items|flag|Return only hosts that have enabled items.&lt;br&gt;&lt;br&gt;Overrides the `with_simple_graph_items` parameter.|
|with\_monitored\_triggers|flag|Return only hosts that have enabled triggers. All of the items used in the trigger must also be enabled.|
|with\_simple\_graph\_items|flag|Return only hosts that have items with numeric type of information.|
|with\_triggers|flag|Return only hosts that have triggers.&lt;br&gt;&lt;br&gt;Overrides the `with_monitored_triggers` parameter.|
|withProblemsSuppressed|boolean|Return hosts that have suppressed problems.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;`null` - *(default)* all hosts;&lt;br&gt;`true` - only hosts with suppressed problems;&lt;br&gt;`false` - only hosts with unsuppressed problems.|
|evaltype|integer|Rules for tag searching.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* And/Or;&lt;br&gt;2 - Or.|
|severities|integer/array|Return hosts that have only problems with given severities. Applies only if problem object is trigger.|
|tags|array/object|Return only hosts with given tags. Exact match by tag and case-sensitive or case-insensitive search by tag value depending on operator value.&lt;br&gt;Format: `[{"tag": "&lt;tag&gt;", "value": "&lt;value&gt;", "operator": "&lt;operator&gt;"}, ...]`.&lt;br&gt;An empty array returns all hosts.&lt;br&gt;&lt;br&gt;Possible operator values:&lt;br&gt;0 - *(default)* Contains;&lt;br&gt;1 - Equals;&lt;br&gt;2 - Not like;&lt;br&gt;3 - Not equal;&lt;br&gt;4 - Exists;&lt;br&gt;5 - Not exists.|
|inheritedTags|boolean|Return hosts that have given `tags` also in all of their linked templates. Default:&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;`true` - linked templates must also have given `tags`;&lt;br&gt;`false` - *(default)* linked template tags are ignored.|
|selectDiscoveries|query|Return a [`discoveries`](/manual/api/reference/discoveryrule/object) property with host low-level discovery rules.&lt;br&gt;&lt;br&gt;Supports `count`.|
|selectDiscoveryRule|query|Return a [`discoveryRule`](/manual/api/reference/discoveryrule/object) property with the low-level discovery rule that created the host (from host prototype in VMware monitoring).|
|selectGraphs|query|Return a [`graphs`](/manual/api/reference/graph/object) property with host graphs.&lt;br&gt;&lt;br&gt;Supports `count`.|
|selectHostDiscovery|query|Return a `hostDiscovery` property with host discovery object data.&lt;br&gt;&lt;br&gt;The host discovery object links a discovered host to a host prototype or a host prototypes to an LLD rule and has the following properties:&lt;br&gt;`host` - `(string)` host of the host prototype;&lt;br&gt;`hostid` - `(string)` ID of the discovered host or host prototype;&lt;br&gt;`parent_hostid` - `(string)` ID of the host prototype from which the host has been created;&lt;br&gt;`parent_itemid` - `(string)` ID of the LLD rule that created the discovered host;&lt;br&gt;`lastcheck` - `(timestamp)` time when the host was last discovered;&lt;br&gt;`ts_delete` - `(timestamp)` time when a host that is no longer discovered will be deleted.|
|selectHostGroups|query|Return a [`hostgroups`](/manual/api/reference/hostgroup/object) property with host groups data that the host belongs to.|
|selectHttpTests|query|Return an [`httpTests`](/manual/api/reference/httptest/object) property with host web scenarios.&lt;br&gt;&lt;br&gt;Supports `count`.|
|selectInterfaces|query|Return an [`interfaces`](/manual/api/reference/hostinterface/object) property with host interfaces.&lt;br&gt;&lt;br&gt;Supports `count`.|
|selectInventory|query|Return an [`inventory`](/manual/api/reference/host/object#host_inventory) property with host inventory data.|
|selectItems|query|Return an [`items`](/manual/api/reference/item/object) property with host items.&lt;br&gt;&lt;br&gt;Supports `count`.|
|selectMacros|query|Return a [`macros`](/manual/api/reference/usermacro/object) property with host macros.|
|selectParentTemplates|query|Return a `parentTemplates` property with [templates](/manual/api/reference/template/object) that the host is linked to.&lt;br&gt;&lt;br&gt;In addition to Template object fields, it contains `link_type` - `(integer)` the way that the template is linked to host.&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* manually linked;&lt;br&gt;1 - automatically linked by LLD.&lt;br&gt;&lt;br&gt;Supports `count`.|
|selectDashboards|query|Return a [`dashboards`](/manual/api/reference/templatedashboard/object) property.&lt;br&gt;&lt;br&gt;Supports `count`.|
|selectTags|query|Return a [`tags`](/manual/api/reference/host/object#host_tag) property with host tags.|
|selectInheritedTags|query|Return an [`inheritedTags`](/manual/api/reference/host/object#host_tag) property with tags that are on all templates which are linked to host.|
|selectTriggers|query|Return a [`triggers`](/manual/api/reference/trigger/object) property with host triggers.&lt;br&gt;&lt;br&gt;Supports `count`.|
|selectValueMaps|query|Return a [`valuemaps`](/manual/api/reference/valuemap/object) property with host value maps.|
|filter|object|Return only those results that exactly match the given filter.&lt;br&gt;&lt;br&gt;Accepts an array, where the keys are property names, and the values are either a single value or an array of values to match against.&lt;br&gt;&lt;br&gt;Allows filtering by interface properties. Doesn't work for `text` fields.|
|limitSelects|integer|Limits the number of records returned by subselects.&lt;br&gt;&lt;br&gt;Applies to the following subselects:&lt;br&gt;`selectParentTemplates` - results will be sorted by `host`;&lt;br&gt;`selectInterfaces`;&lt;br&gt;`selectItems` - sorted by `name`;&lt;br&gt;`selectDiscoveries` - sorted by `name`;&lt;br&gt;`selectTriggers` - sorted by `description`;&lt;br&gt;`selectGraphs` - sorted by `name`;&lt;br&gt;`selectDashboards` - sorted by `name`.|
|search|object|Return results that match the given pattern (case-insensitive).&lt;br&gt;&lt;br&gt;Accepts an array, where the keys are property names, and the values are strings to search for. If no additional options are given, this will perform a `LIKE "%…%"` search.&lt;br&gt;&lt;br&gt;Allows searching by interface properties. Works only for `string` and `text` fields.|
|searchInventory|object|Return only hosts that have inventory data matching the given wildcard search.&lt;br&gt;&lt;br&gt;This parameter is affected by the same additional parameters as `search`.|
|sortfield|string/array|Sort the result by the given properties.&lt;br&gt;&lt;br&gt;Possible values: `hostid`, `host`, `name`, `status`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters).|
|editable|boolean|^|
|excludeSearch|boolean|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|
|selectGroups&lt;br&gt;(deprecated)|query|This parameter is deprecated, please use `selectHostGroups` instead.&lt;br&gt;Return a [`groups`](/manual/api/reference/hostgroup/object) property with host groups data that the host belongs to.|</source>
      </trans-unit>
      <trans-unit id="7223bab1" xml:space="preserve">
        <source>### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.</source>
      </trans-unit>
      <trans-unit id="b41637d2" xml:space="preserve">
        <source>### Examples</source>
      </trans-unit>
      <trans-unit id="c6ea3cce" xml:space="preserve">
        <source>#### Retrieving data by name

Retrieve all data about two hosts named "Zabbix server" and "Linux
server".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "host.get",
    "params": {
        "filter": {
            "host": [
                "Zabbix server",
                "Linux server"
            ]
        }
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": [
        {
            "hostid": "10160",
            "proxy_hostid": "0",
            "host": "Zabbix server",
            "status": "0",
            "lastaccess": "0",
            "ipmi_authtype": "-1",
            "ipmi_privilege": "2",
            "ipmi_username": "",
            "ipmi_password": "",
            "maintenanceid": "0",
            "maintenance_status": "0",
            "maintenance_type": "0",
            "maintenance_from": "0",
            "name": "Zabbix server",
            "flags": "0",
            "description": "The Zabbix monitoring server.",
            "tls_connect": "1",
            "tls_accept": "1",
            "tls_issuer": "",
            "tls_subject": "",
            "inventory_mode": "1",
            "active_available": "1"
        },
        {
            "hostid": "10167",
            "proxy_hostid": "0",
            "host": "Linux server",
            "status": "0",
            "ipmi_authtype": "-1",
            "ipmi_privilege": "2",
            "ipmi_username": "",
            "ipmi_password": "",
            "maintenanceid": "0",
            "maintenance_status": "0",
            "maintenance_type": "0",
            "maintenance_from": "0",
            "name": "Linux server",
            "flags": "0",
            "description": "",
            "tls_connect": "1",
            "tls_accept": "1",
            "tls_issuer": "",
            "tls_subject": "",
            "inventory_mode": "1",
            "active_available": "1"
        }
    ],
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="9078aff7" xml:space="preserve">
        <source>#### Retrieving host groups

Retrieve host groups that the host "Zabbix server" is a member of.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "host.get",
    "params": {
        "output": ["hostid"],
        "selectHostGroups": "extend",
        "filter": {
            "host": [
                "Zabbix server"
            ]
        }
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": [
        {
            "hostid": "10085",
            "hostgroups": [
                {
                    "groupid": "2",
                    "name": "Linux servers",
                    "flags": "0",
                    "uuid": "dc579cd7a1a34222933f24f52a68bcd8"
                },
                {
                    "groupid": "4",
                    "name": "Zabbix servers",
                    "flags": "0",
                    "uuid": "6f6799aa69e844b4b3918f779f2abf08"
                }
            ]
        }
    ],
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="59c8a46a" xml:space="preserve">
        <source>#### Retrieving linked templates

Retrieve the IDs and names of templates linked to host "10084".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "host.get",
    "params": {
        "output": ["hostid"],
        "selectParentTemplates": [
            "templateid",
            "name"
        ],
        "hostids": "10084"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": [
        {
            "hostid": "10084",
            "parentTemplates": [
                {
                    "name": "Linux",
                    "templateid": "10001"
                },
                {
                    "name": "Zabbix Server",
                    "templateid": "10047"
                }
            ]
        }
    ],
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="8be1d99c" xml:space="preserve">
        <source>#### Searching by host inventory data

Retrieve hosts that contain "Linux" in the host inventory "OS" field.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "host.get",
    "params": {
        "output": [
            "host"
        ],
        "selectInventory": [
            "os"
        ],
        "searchInventory": {
            "os": "Linux"
        }
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": [
        {
            "hostid": "10084",
            "host": "Zabbix server",
            "inventory": {
                "os": "Linux Ubuntu"
            }
        },
        {
            "hostid": "10107",
            "host": "Linux server",
            "inventory": {
                "os": "Linux Mint"
            }
        }
    ],
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="b4e37e5e" xml:space="preserve">
        <source>#### Searching by host tags

Retrieve hosts that have tag "Host name" equal to "Linux server".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "host.get",
    "params": {
        "output": ["hostid"],
        "selectTags": "extend",
        "evaltype": 0,
        "tags": [
            {
                "tag": "Host name",
                "value": "Linux server",
                "operator": 1
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": [
        {
            "hostid": "10085",
            "tags": [
                {
                    "tag": "Host name",
                    "value": "Linux server"
                },
                {
                    "tag": "OS",
                    "value": "RHEL 7"
                }
            ]
        }
    ],
    "id": 1
}
```

Retrieve hosts that have these tags not only on host level but also in
their linked parent templates.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "host.get",
    "params": {
        "output": ["name"],
        "tags": [
            {
                "tag": "A",
                "value": "1",
                "operator": 1
            }
        ],
        "inheritedTags": true
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": [
        {
            "hostid": "10623",
            "name": "PC room 1"
        },
        {
            "hostid": "10601",
            "name": "Office"
        }
    ],
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="9ebae84e" xml:space="preserve">
        <source>#### Searching host with tags and template tags

Retrieve a host with tags and all tags that are linked to parent
templates.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "host.get",
    "params": {
        "output": ["name"],
        "hostids": 10502,
        "selectTags": ["tag", "value"],
        "selectInheritedTags": ["tag", "value"]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": [
        {
            "hostid": "10502",
            "name": "Desktop",
            "tags": [
                {
                    "tag": "A",
                    "value": "1"
                }
            ],
            "inheritedTags": [
                {
                    "tag": "B",
                    "value": "2"
                }
            ]
        }
    ],
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="219d4ee1" xml:space="preserve">
        <source>#### Searching hosts by problem severity

Retrieve hosts that have "Disaster" problems.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "host.get",
    "params": {
        "output": ["name"],
        "severities": 5
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": [
        {
            "hostid": "10160",
            "name": "Zabbix server"
        }
    ],
    "id": 1
}
```

Retrieve hosts that have "Average" and "High" problems.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "host.get",
    "params": {
        "output": ["name"],
        "severities": [3, 4]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": [
        {
            "hostid": "20170",
            "name": "Database"
        },
        {
            "hostid": "20183",
            "name": "workstation"
        }
    ],
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="06c7fe93" xml:space="preserve">
        <source>### See also

-   [Host group](/manual/api/reference/hostgroup/object#host_group)
-   [Template](/manual/api/reference/template/object#template)
-   [User macro](/manual/api/reference/usermacro/object#hosttemplate_level_macro)
-   [Host interface](/manual/api/reference/hostinterface/object#host_interface)</source>
      </trans-unit>
      <trans-unit id="55c08f7a" xml:space="preserve">
        <source>### Source

CHost::get() in *ui/include/classes/api/services/CHost.php*.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
