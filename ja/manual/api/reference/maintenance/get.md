[comment]: # translation:outdated

[comment]: # ({new-0d3af930})
# maintenance.get

[comment]: # ({/new-0d3af930})

[comment]: # ({new-a04dd4c9})
### Description

`integer/array maintenance.get(object parameters)`

The method allows to retrieve maintenances according to the given
parameters.

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-a04dd4c9})

[comment]: # ({new-9ca798d7})
### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|groupids|string/array|Return only maintenances that are assigned to the given host groups.|
|hostids|string/array|Return only maintenances that are assigned to the given hosts.|
|maintenanceids|string/array|Return only maintenances with the given IDs.|
|selectGroups|query|Return a [groups](/manual/api/reference/hostgroup/object) property with host groups assigned to the maintenance.|
|selectHosts|query|Return a [hosts](/manual/api/reference/host/object) property with hosts assigned to the maintenance.|
|selectTags|query|Return a [tags](/manual/api/reference/maintenance/object#problem_tag) property with problem tags of the maintenance.|
|selectTimeperiods|query|Return a [timeperiods](/manual/api/reference/maintenance/object#Time_period) property with time periods of the maintenance.|
|sortfield|string/array|Sort the result by the given properties.<br><br>Possible values are: `maintenanceid`, `name` and `maintenance_type`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters).|
|editable|boolean|^|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

[comment]: # ({/new-9ca798d7})

[comment]: # ({new-7223bab1})
### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-0b1cb387})
#### Retrieving maintenances

Retrieve all configured maintenances, and the data about the assigned
host groups, defined time periods and problem tags.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "maintenance.get",
    "params": {
        "output": "extend",
        "selectGroups": "extend",
        "selectTimeperiods": "extend",
        "selectTags": "extend"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "maintenanceid": "3",
            "name": "Sunday maintenance",
            "maintenance_type": "0",
            "description": "",
            "active_since": "1358844540",
            "active_till": "1390466940",
            "tags_evaltype": "0",
            "groups": [
                {
                    "groupid": "4",
                    "name": "Zabbix servers",
                    "internal": "0"
                }
            ],
            "timeperiods": [
                {
                    "timeperiodid": "4",
                    "timeperiod_type": "3",
                    "every": "1",
                    "month": "0",
                    "dayofweek": "1",
                    "day": "0",
                    "start_time": "64800",
                    "period": "3600",
                    "start_date": "2147483647"
                }
            ],
            "tags": [
                {
                    "tag": "service",
                    "operator": "0",
                    "value": "mysqld",
                },
                {
                    "tag": "error",
                    "operator": "2",
                    "value": ""
                }
            ]
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-0b1cb387})

[comment]: # ({new-dbcf678a})
### See also

-   [Host](/manual/api/reference/host/object#host)
-   [Host group](/manual/api/reference/hostgroup/object#host_group)
-   [Time period](object#time_period)

[comment]: # ({/new-dbcf678a})

[comment]: # ({new-50ed5be3})
### Source

CMaintenance::get() in
*ui/include/classes/api/services/CMaintenance.php*.

[comment]: # ({/new-50ed5be3})
