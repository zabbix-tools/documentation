<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="ja" datatype="plaintext" original="manual/api/reference/triggerprototype/get.md">
    <body>
      <trans-unit id="c7072ca6" xml:space="preserve">
        <source># triggerprototype.get</source>
      </trans-unit>
      <trans-unit id="12701833" xml:space="preserve">
        <source>### Description

`integer/array triggerprototype.get(object parameters)`

The method allows to retrieve trigger prototypes according to the given parameters.

::: noteclassic
This method is available to users of any type. Permissions to call the method can be revoked in user role settings.
See [User roles](/manual/web_interface/frontend_sections/users/user_roles) for more information.
:::</source>
      </trans-unit>
      <trans-unit id="14171778" xml:space="preserve">
        <source>### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|active|flag|Return only enabled trigger prototypes that belong to monitored hosts.|
|discoveryids|string/array|Return only trigger prototypes that belong to the given LLD rules.|
|functions|string/array|Return only triggers that use the given functions.&lt;br&gt;&lt;br&gt;Refer to the [Supported functions](/manual/appendix/functions) page for a list of supported functions.|
|group|string|Return only trigger prototypes that belong to hosts or templates from the host groups or template groups with the given name.|
|groupids|string/array|Return only trigger prototypes that belong to hosts or templates from the given host groups or template groups.|
|host|string|Return only trigger prototypes that belong to hosts with the given name.|
|hostids|string/array|Return only trigger prototypes that belong to the given hosts.|
|inherited|boolean|If set to `true` return only trigger prototypes inherited from a template.|
|maintenance|boolean|If set to `true` return only enabled trigger prototypes that belong to hosts in maintenance.|
|min\_severity|integer|Return only trigger prototypes with severity greater or equal than the given severity.|
|monitored|flag|Return only enabled trigger prototypes that belong to monitored hosts and contain only enabled items.|
|templated|boolean|If set to `true` return only trigger prototypes that belong to templates.|
|templateids|string/array|Return only trigger prototypes that belong to the given templates.|
|triggerids|string/array|Return only trigger prototypes with the given IDs.|
|expandExpression|flag|Expand functions and macros in the trigger expression.|
|selectDependencies|query|Return trigger prototypes and triggers that the trigger prototype depends on in the `dependencies` property.|
|selectDiscoveryRule|query|Return the [LLD rule](/manual/api/reference/discoveryrule/object) that the trigger prototype belongs to in the `discoveryRule` property.|
|selectFunctions|query|Return functions used in the trigger prototype in the `functions` property.&lt;br&gt;&lt;br&gt;The function objects represent the functions used in the trigger expression and has the following properties:&lt;br&gt;`functionid` - `(string)` ID of the function;&lt;br&gt;`itemid` - `(string)` ID of the item used in the function;&lt;br&gt;`function` - `(string)` name of the function;&lt;br&gt;`parameter` - `(string)` parameter passed to the function. Query parameter is replaced by `$` symbol in returned string.|
|selectHostGroups|query|Return the host groups that the trigger prototype belongs to in the [`hostgroups`](/manual/api/reference/hostgroup/object) property.|
|selectHosts|query|Return the hosts that the trigger prototype belongs to in the [`hosts`](/manual/api/reference/host/object) property.|
|selectItems|query|Return items and item prototypes used the trigger prototype in the [`items`](/manual/api/reference/item/object) property.|
|selectTags|query|Return the trigger prototype tags in [`tags`](/manual/api/reference/triggerprototype/object#Trigger_prototype_tag) property.|
|selectTemplateGroups|query|Return the template groups that the trigger prototype belongs to in the [`templategroups`](/manual/api/reference/templategroup/object) property.|
|filter|object|Return only those results that exactly match the given filter.&lt;br&gt;&lt;br&gt;Accepts an array, where the keys are property names, and the values are either a single value or an array of values to match against.&lt;br&gt;&lt;br&gt;Supports additional filters:&lt;br&gt;`host` - technical name of the host that the trigger prototype belongs to;&lt;br&gt;`hostid` - ID of the host that the trigger prototype belongs to.|
|limitSelects|integer|Limits the number of records returned by subselects.&lt;br&gt;&lt;br&gt;Applies to the following subselects:&lt;br&gt;`selectHosts` - results will be sorted by `host`.|
|sortfield|string/array|Sort the result by the given properties.&lt;br&gt;&lt;br&gt;Possible values: `triggerid`, `description`, `status`, `priority`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters).|
|editable|boolean|^|
|excludeSearch|boolean|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|
|selectGroups&lt;br&gt;(deprecated)|query|This parameter is deprecated, please use `selectHostGroups` or `selectTemplateGroups` instead.&lt;br&gt;Return the host groups and template groups that the trigger prototype belongs to in the `groups` property.|</source>
      </trans-unit>
      <trans-unit id="7223bab1" xml:space="preserve">
        <source>### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has been used.</source>
      </trans-unit>
      <trans-unit id="b41637d2" xml:space="preserve">
        <source>### Examples</source>
      </trans-unit>
      <trans-unit id="81de28c1" xml:space="preserve">
        <source>#### Retrieve trigger prototypes from an LLD rule

Retrieve all trigger prototypes and their functions from an LLD rule.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "triggerprototype.get",
    "params": {
        "output": "extend",
        "selectFunctions": "extend",
        "discoveryids": "22450"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": [
        {
            "triggerid": "13272",
            "expression": "{12598}&lt;20",
            "description": "Free inodes is less than 20% on volume {#FSNAME}",
            "url": "",
            "status": "0",
            "value": "0",
            "priority": "2",
            "lastchange": "0",
            "comments": "",
            "error": "",
            "templateid": "0",
            "type": "0",
            "state": "0",
            "flags": "2",
            "recovery_mode": "0",
            "recovery_expression": "",
            "correlation_mode": "0",
            "correlation_tag": "",
            "manual_close": "0",
            "opdata": "",
            "discover": "0",
            "event_name": "",
            "uuid": "6ce467d05e8745409a177799bed34bb3",
            "url_name": "",
            "functions": [
                {
                    "functionid": "12598",
                    "itemid": "22454",
                    "triggerid": "13272",
                    "parameter": "$",
                    "function": "last"
                }
            ]
        },
        {
            "triggerid": "13266",
            "expression": "{13500}&lt;20",
            "description": "Free disk space is less than 20% on volume {#FSNAME}",
            "url": "",
            "status": "0",
            "value": "0",
            "priority": "2",
            "lastchange": "0",
            "comments": "",
            "error": "",
            "templateid": "0",
            "type": "0",
            "state": "0",
            "flags": "2",
            "recovery_mode": "0",
            "recovery_expression": "",
            "correlation_mode": "0",
            "correlation_tag": "",
            "manual_close": "0",
            "opdata": "",
            "discover": "0",
            "event_name": "",
            "uuid": "74a1fc62bfe24b7eabe4e244c70dc384",
            "url_name": "",
            "functions": [
                {
                    "functionid": "13500",
                    "itemid": "22686",
                    "triggerid": "13266",
                    "parameter": "$",
                    "function": "last"
                }
            ]
        }
    ],
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="2cb80f2d" xml:space="preserve">
        <source>#### Retrieving a specific trigger prototype with tags

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "triggerprototype.get",
    "params": {
        "output": [
            "triggerid",
            "description"
        ]
        "selectTags": "extend",
        "triggerids": [
            "17373"
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": [
        {
            "triggerid": "17373",
            "description": "Free disk space is less than 20% on volume {#FSNAME}",
            "tags": [
                {
                    "tag": "volume",
                    "value": "{#FSNAME}"
                },
                {
                    "tag": "type",
                    "value": "{#FSTYPE}"
                }
            ]
        }
    ],
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="222d46a0" xml:space="preserve">
        <source>### See also

-   [Discovery rule](/manual/api/reference/discoveryrule/object#discovery_rule)
-   [Item](/manual/api/reference/item/object#item)
-   [Host](/manual/api/reference/host/object#host)
-   [Host group](/manual/api/reference/hostgroup/object#host_group)
-   [Template group](/manual/api/reference/templategroup/object#template_group)</source>
      </trans-unit>
      <trans-unit id="1136c1c7" xml:space="preserve">
        <source>### Source

CTriggerPrototype::get() in *ui/include/classes/api/services/CTriggerPrototype.php*.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
