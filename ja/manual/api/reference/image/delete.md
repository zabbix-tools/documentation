[comment]: # translation:outdated

[comment]: # ({new-46b153da})
# image.delete

[comment]: # ({/new-46b153da})

[comment]: # ({new-60f587fa})
### Description

`object image.delete(array imageIds)`

This method allows to delete images.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-60f587fa})

[comment]: # ({new-c7ef61f7})
### Parameters

`(array)` IDs of the images to delete.

[comment]: # ({/new-c7ef61f7})

[comment]: # ({new-4b3e57f6})
### Return values

`(object)` Returns an object containing the IDs of the deleted images
under the `imageids` property.

[comment]: # ({/new-4b3e57f6})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-925dee02})
#### Delete multiple images

Delete two images.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "image.delete",
    "params": [
        "188",
        "192"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "imageids": [
            "188",
            "192"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-925dee02})

[comment]: # ({new-dfdead8e})
### Source

CImage::delete() in *ui/include/classes/api/services/CImage.php*.

[comment]: # ({/new-dfdead8e})
