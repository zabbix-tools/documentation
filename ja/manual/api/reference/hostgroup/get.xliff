<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="ja" datatype="plaintext" original="manual/api/reference/hostgroup/get.md">
    <body>
      <trans-unit id="f8d4c2e5" xml:space="preserve">
        <source># hostgroup.get</source>
      </trans-unit>
      <trans-unit id="08ff22f2" xml:space="preserve">
        <source>### Description

`integer/array hostgroup.get(object parameters)`

The method allows to retrieve host groups according to the given
parameters.

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::</source>
      </trans-unit>
      <trans-unit id="7f042696" xml:space="preserve">
        <source>### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|graphids|string/array|Return only host groups that contain hosts with the given graphs.|
|groupids|string/array|Return only host groups with the given host group IDs.|
|hostids|string/array|Return only host groups that contain the given hosts.|
|maintenanceids|string/array|Return only host groups that are affected by the given maintenances.|
|triggerids|string/array|Return only host groups that contain hosts with the given triggers.|
|with\_graphs|flag|Return only host groups that contain hosts with graphs.|
|with\_graph\_prototypes|flag|Return only host groups that contain hosts with graph prototypes.|
|with\_hosts|flag|Return only host groups that contain hosts.|
|with\_httptests|flag|Return only host groups that contain hosts with web checks.&lt;br&gt;&lt;br&gt;Overrides the `with_monitored_httptests` parameter.|
|with\_items|flag|Return only host groups that contain hosts with items.&lt;br&gt;&lt;br&gt;Overrides the `with_monitored_items` and `with_simple_graph_items` parameters.|
|with\_item\_prototypes|flag|Return only host groups that contain hosts with item prototypes.&lt;br&gt;&lt;br&gt;Overrides the `with_simple_graph_item_prototypes` parameter.|
|with\_simple\_graph\_item\_prototypes|flag|Return only host groups that contain hosts with item prototypes, which are enabled for creation and have numeric type of information.|
|with\_monitored\_httptests|flag|Return only host groups that contain hosts with enabled web checks.|
|with\_monitored\_hosts|flag|Return only host groups that contain monitored hosts.|
|with\_monitored\_items|flag|Return only host groups that contain hosts with enabled items.&lt;br&gt;&lt;br&gt;Overrides the `with_simple_graph_items` parameter.|
|with\_monitored\_triggers|flag|Return only host groups that contain hosts with enabled triggers. All of the items used in the trigger must also be enabled.|
|with\_simple\_graph\_items|flag|Return only host groups that contain hosts with numeric items.|
|with\_triggers|flag|Return only host groups that contain hosts with triggers.&lt;br&gt;&lt;br&gt;Overrides the `with_monitored_triggers` parameter.|
|selectDiscoveryRule|query|Return a [`discoveryRule`](/manual/api/reference/drule/object) property with the LLD rule that created the host group.|
|selectGroupDiscovery|query|Return a `groupDiscovery` property with the host group discovery object.&lt;br&gt;&lt;br&gt;The host group discovery object links a discovered host group to a host group prototype and has the following properties:&lt;br&gt;`groupid` - `(string)` ID of the discovered host group;&lt;br&gt;`lastcheck` - `(timestamp)` time when the host group was last discovered;&lt;br&gt;`name` - `(string)` name of the host group prototype;&lt;br&gt;`parent_group_prototypeid` - `(string)` ID of the host group prototype from which the host group has been created;&lt;br&gt;`ts_delete` - `(timestamp)` time when a host group that is no longer discovered will be deleted.|
|selectHosts|query|Return a [`hosts`](/manual/api/reference/host/object) property with the hosts that belong to the host group.&lt;br&gt;&lt;br&gt;Supports `count`.|
|limitSelects|integer|Limits the number of records returned by subselects.&lt;br&gt;&lt;br&gt;Applies to the following subselects:&lt;br&gt;`selectHosts` - results will be sorted by `host`.|
|sortfield|string/array|Sort the result by the given properties.&lt;br&gt;&lt;br&gt;Possible values: `groupid`, `name`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters) page.|
|editable|boolean|^|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|
|monitored\_hosts&lt;br&gt;(deprecated)|flag|This parameter is deprecated, please use `with_monitored_hosts` instead.&lt;br&gt;Return only host groups that contain monitored hosts.|
|real\_hosts&lt;br&gt;(deprecated)|flag|This parameter is deprecated, please use `with_hosts` instead.&lt;br&gt;Return only host groups that contain hosts.|</source>
      </trans-unit>
      <trans-unit id="7223bab1" xml:space="preserve">
        <source>### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.</source>
      </trans-unit>
      <trans-unit id="b41637d2" xml:space="preserve">
        <source>### Examples</source>
      </trans-unit>
      <trans-unit id="8ed2756a" xml:space="preserve">
        <source>#### Retrieving data by name

Retrieve all data about two host groups named "Zabbix servers" and
"Linux servers".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "hostgroup.get",
    "params": {
        "output": "extend",
        "filter": {
            "name": [
                "Zabbix servers",
                "Linux servers"
            ]
        }
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": [
        {
            "groupid": "2",
            "name": "Linux servers",
            "internal": "0"
        },
        {
            "groupid": "4",
            "name": "Zabbix servers",
            "internal": "0"
        }
    ],
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="e5f0abe1" xml:space="preserve">
        <source>### See also

-   [Host](/manual/api/reference/host/object#host)</source>
      </trans-unit>
      <trans-unit id="1e72ea39" xml:space="preserve">
        <source>### Source

CHostGroup::get() in *ui/include/classes/api/services/CHostGroup.php*.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
