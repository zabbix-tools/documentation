<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="ja" datatype="plaintext" original="manual/api/reference/settings/object.md">
    <body>
      <trans-unit id="d8014f4f" xml:space="preserve">
        <source># &gt; Settings object

The following objects are directly related to the `settings` API.</source>
      </trans-unit>
      <trans-unit id="a08f9118" xml:space="preserve">
        <source>### Settings

The settings object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|default\_lang|string|System language by default.&lt;br&gt;&lt;br&gt;Default: `en_GB`.|
|default\_timezone|string|System time zone by default.&lt;br&gt;&lt;br&gt;Default: `system` - system default.&lt;br&gt;&lt;br&gt;For the full list of supported time zones please refer to [PHP documentation](https://www.php.net/manual/en/timezones.php).|
|default\_theme|string|Default theme.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;`blue-theme` - *(default)* Blue;&lt;br&gt;`dark-theme` - Dark;&lt;br&gt;`hc-light` - High-contrast light;&lt;br&gt;`hc-dark` - High-contrast dark.|
|search\_limit|integer|Limit for search and filter results.&lt;br&gt;&lt;br&gt;Default: 1000.|
|max\_overview\_table\_size|integer|Max number of columns and rows in Data overview and Trigger overview dashboard widgets.&lt;br&gt;&lt;br&gt;Default: 50.|
|max\_in\_table|integer|Max count of elements to show inside table cell.&lt;br&gt;&lt;br&gt;Default: 50.|
|server\_check\_interval|integer|Show warning if Zabbix server is down.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - Do not show warning;&lt;br&gt;10 - *(default)* Show warning.|
|work\_period|string|Working time.&lt;br&gt;&lt;br&gt;Default: 1-5,09:00-18:00.|
|show\_technical\_errors|integer|Show technical errors (PHP/SQL) to non-Super admin users and to users that are not part of user groups with debug mode enabled.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* Do not technical errors;&lt;br&gt;1 - Show technical errors.|
|history\_period|string|Max period to display history data in Latest data, Web, and Data overview dashboard widgets.&lt;br&gt;Accepts seconds and time unit with suffix.&lt;br&gt;&lt;br&gt;Default: 24h.|
|period\_default|string|Time filter default period.&lt;br&gt;Accepts seconds and time unit with suffix with month and year support (30s, 1m, 2h, 1d, 1M, 1y).&lt;br&gt;&lt;br&gt;Default: 1h.|
|max\_period|string|Max period for time filter.&lt;br&gt;Accepts seconds and time unit with suffix with month and year support (30s, 1m, 2h, 1d, 1M, 1y).&lt;br&gt;&lt;br&gt;Default: 2y.|
|severity\_color\_0|string|Color for "Not classified" severity as a hexadecimal color code.&lt;br&gt;&lt;br&gt;Default: 97AAB3.|
|severity\_color\_1|string|Color for "Information" severity as a hexadecimal color code.&lt;br&gt;&lt;br&gt;Default: 7499FF.|
|severity\_color\_2|string|Color for "Warning" severity as a hexadecimal color code.&lt;br&gt;&lt;br&gt;Default: FFC859.|
|severity\_color\_3|string|Color for "Average" severity as a hexadecimal color code.&lt;br&gt;&lt;br&gt;Default: FFA059.|
|severity\_color\_4|string|Color for "High" severity as a hexadecimal color code.&lt;br&gt;&lt;br&gt;Default: E97659.|
|severity\_color\_5|string|Color for "Disaster" severity as a hexadecimal color code.&lt;br&gt;&lt;br&gt;Default: E45959.|
|severity\_name\_0|string|Name for "Not classified" severity.&lt;br&gt;&lt;br&gt;Default: Not classified.|
|severity\_name\_1|string|Name for "Information" severity.&lt;br&gt;&lt;br&gt;Default: Information.|
|severity\_name\_2|string|Name for "Warning" severity.&lt;br&gt;&lt;br&gt;Default: Warning.|
|severity\_name\_3|string|Name for "Average" severity.&lt;br&gt;&lt;br&gt;Default: Average.|
|severity\_name\_4|string|Name for "High" severity.&lt;br&gt;&lt;br&gt;Default: High.|
|severity\_name\_5|string|Name for "Disaster" severity.&lt;br&gt;&lt;br&gt;Default: Disaster.|
|custom\_color|integer|Use custom event status colors.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* Do not use custom event status colors;&lt;br&gt;1 - Use custom event status colors.|
|ok\_period|string|Display OK triggers period.&lt;br&gt;Accepts seconds and time unit with suffix.&lt;br&gt;&lt;br&gt;Default: 5m.|
|blink\_period|string|On status change triggers blink period.&lt;br&gt;Accepts seconds and time unit with suffix.&lt;br&gt;&lt;br&gt;Default: 2m.|
|problem\_unack\_color|string|Color for unacknowledged PROBLEM events as a hexadecimal color code.&lt;br&gt;&lt;br&gt;Default: CC0000.|
|problem\_ack\_color|string|Color for acknowledged PROBLEM events as a hexadecimal color code.&lt;br&gt;&lt;br&gt;Default: CC0000.|
|ok\_unack\_color|string|Color for unacknowledged RESOLVED events as a hexadecimal color code.&lt;br&gt;&lt;br&gt;Default: 009900.|
|ok\_ack\_color|string|Color for acknowledged RESOLVED events as a hexadecimal color code.&lt;br&gt;&lt;br&gt;Default: 009900.|
|problem\_unack\_style|integer|Blinking for unacknowledged PROBLEM events.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - Do not show blinking;&lt;br&gt;1 - *(default)* Show blinking.|
|problem\_ack\_style|integer|Blinking for acknowledged PROBLEM events.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - Do not show blinking;&lt;br&gt;1 - *(default)* Show blinking.|
|ok\_unack\_style|integer|Blinking for unacknowledged RESOLVED events.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - Do not show blinking;&lt;br&gt;1 - *(default)* Show blinking.|
|ok\_ack\_style|integer|Blinking for acknowledged RESOLVED events.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - Do not show blinking;&lt;br&gt;1 - *(default)* Show blinking.|
|url|string|Frontend URL.|
|discovery\_groupid|integer|ID of the host group to which will be automatically placed discovered hosts.|
|default\_inventory\_mode|integer|Default host inventory mode.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;-1 - *(default)* Disabled;&lt;br&gt;0 - Manual;&lt;br&gt;1 - Automatic.|
|alert\_usrgrpid|integer|ID of the user group to which will be sending database down alarm message.&lt;br&gt;&lt;br&gt;If set to "0", the alarm message will not be sent.|
|snmptrap\_logging|integer|Log unmatched SNMP traps.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - Do not log unmatched SNMP traps;&lt;br&gt;1 - *(default)* Log unmatched SNMP traps.|
|login\_attempts|integer|Number of failed login attempts after which login form will be blocked.&lt;br&gt;&lt;br&gt;Default: 5.|
|login\_block|string|Time interval during which login form will be blocked if number of failed login attempts exceeds defined in login\_attempts field.&lt;br&gt;Accepts seconds and time unit with suffix.&lt;br&gt;&lt;br&gt;Default: 30s.|
|validate\_uri\_schemes|integer|Validate URI schemes.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - Do not validate;&lt;br&gt;1 - *(default)* Validate.|
|uri\_valid\_schemes|string|Valid URI schemes.&lt;br&gt;&lt;br&gt;Default: http,https,ftp,file,mailto,tel,ssh.|
|x\_frame\_options|string|X-Frame-Options HTTP header.&lt;br&gt;&lt;br&gt;Default: SAMEORIGIN.|
|iframe\_sandboxing\_enabled|integer|Use iframe sandboxing.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - Do not use;&lt;br&gt;1 - *(default)* Use.|
|iframe\_sandboxing\_exceptions|string|Iframe sandboxing exceptions.|
|connect\_timeout|string|Connection timeout with Zabbix server.&lt;br&gt;&lt;br&gt;Default: 3s.|
|socket\_timeout|string|Network default timeout.&lt;br&gt;&lt;br&gt;Default: 3s.|
|media\_type\_test\_timeout|string|Network timeout for media type test.&lt;br&gt;&lt;br&gt;Default: 65s.|
|item\_test\_timeout|string|Network timeout for item tests.&lt;br&gt;&lt;br&gt;Default: 60s.|
|script\_timeout|string|Network timeout for script execution.&lt;br&gt;&lt;br&gt;Default: 60s.|
|report\_test\_timeout|string|Network timeout for scheduled report test.&lt;br&gt;&lt;br&gt;Default: 60s.|
|auditlog\_enabled|integer|Enable audit logging.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - Disable;&lt;br&gt;1 - *(default)* Enable.|
|ha\_failover\_delay|string|Failover delay in seconds.&lt;br&gt;&lt;br&gt;Default: 1m.|
|geomaps\_tile\_provider|string|Geomap tile provider.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;`OpenStreetMap.Mapnik` - *(default)* OpenStreetMap Mapnik;&lt;br&gt;`OpenTopoMap` - OpenTopoMap;&lt;br&gt;`Stamen.TonerLite` - Stamen Toner Lite;&lt;br&gt;`Stamen.Terrain` - Stamen Terrain;&lt;br&gt;`USGS.USTopo` - USGS US Topo;&lt;br&gt;`USGS.USImagery` - USGS US Imagery.&lt;br&gt;&lt;br&gt;Supports empty string to specify custom values of `geomaps_tile_url`, `geomaps_max_zoom` and `geomaps_attribution`.|
|geomaps\_tile\_url|string|Geomap tile URL.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `geomaps_tile_provider` is set to empty string|
|geomaps\_max\_zoom|integer|Geomap max zoom level.&lt;br&gt;&lt;br&gt;Possible values: 0-30.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `geomaps_tile_provider` is set to empty string|
|geomaps\_attribution|string|Geomap attribution text.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `geomaps_tile_provider` is set to empty string|
|vault\_provider|integer|Vault provider.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* HashiCorp Vault;&lt;br&gt;1 - CyberArk Vault.|</source>
      </trans-unit>
    </body>
  </file>
</xliff>
