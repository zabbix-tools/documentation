<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="ja" datatype="plaintext" original="manual/api/reference/dcheck/object.md">
    <body>
      <trans-unit id="a9b6b9e5" xml:space="preserve">
        <source># &gt; Discovery check object

The following objects are directly related to the `dcheck` API.</source>
      </trans-unit>
      <trans-unit id="662afbc9" xml:space="preserve">
        <source>### Discovery check

The discovery check object defines a specific check performed by a
network discovery rule. It has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|dcheckid|string|ID of the discovery check.|
|druleid|string|ID of the discovery rule that the check belongs to.|
|key\_|string|Item key (if `type` is set to "Zabbix agent") or SNMP OID (if `type` is set to "SNMPv1 agent", "SNMPv2 agent", or "SNMPv3 agent").&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* if `type` is set to "Zabbix agent", "SNMPv1 agent", "SNMPv2 agent", or "SNMPv3 agent"|
|ports|string|One or several port ranges to check, separated by commas.&lt;br&gt;&lt;br&gt;Default: 0.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `type` is set to "SSH" (0), "LDAP" (1), "SMTP" (2), "FTP" (3), "HTTP" (4), "POP" (5), "NNTP" (6), "IMAP" (7), "TCP" (8), "Zabbix agent" (9), "SNMPv1 agent" (10), "SNMPv2 agent" (11), "SNMPv3 agent" (13), "HTTPS" (14), or "Telnet" (15)|
|snmp\_community|string|SNMP community.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* if `type` is set to "SNMPv1 agent" or "SNMPv2 agent"|
|snmpv3\_authpassphrase|string|Authentication passphrase.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `type` is set to "SNMPv3 agent" and `snmpv3_securitylevel` is set to "authNoPriv" or "authPriv"|
|snmpv3\_authprotocol|integer|Authentication protocol.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* MD5;&lt;br&gt;1 - SHA1;&lt;br&gt;2 - SHA224;&lt;br&gt;3 - SHA256;&lt;br&gt;4 - SHA384;&lt;br&gt;5 - SHA512.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `type` is set to "SNMPv3 agent" and `snmpv3_securitylevel` is set to "authNoPriv" or "authPriv"|
|snmpv3\_contextname|string|SNMPv3 context name.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `type` is set to "SNMPv3 agent"|
|snmpv3\_privpassphrase|string|Privacy passphrase.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `type` is set to "SNMPv3 agent" and `snmpv3_securitylevel` is set to "authPriv"|
|snmpv3\_privprotocol|integer|Privacy protocol.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* DES;&lt;br&gt;1 - AES128;&lt;br&gt;2 - AES192;&lt;br&gt;3 - AES256;&lt;br&gt;4 - AES192C;&lt;br&gt;5 - AES256C.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `type` is set to "SNMPv3 agent" and `snmpv3_securitylevel` is set to "authPriv"|
|snmpv3\_securitylevel|string|Security level.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - noAuthNoPriv;&lt;br&gt;1 - authNoPriv;&lt;br&gt;2 - authPriv.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `type` is set to "SNMPv3 agent"|
|snmpv3\_securityname|string|Security name.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `type` is set to "SNMPv3 agent"|
|type|integer|Type of check.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - SSH;&lt;br&gt;1 - LDAP;&lt;br&gt;2 - SMTP;&lt;br&gt;3 - FTP;&lt;br&gt;4 - HTTP;&lt;br&gt;5 - POP;&lt;br&gt;6 - NNTP;&lt;br&gt;7 - IMAP;&lt;br&gt;8 - TCP;&lt;br&gt;9 - Zabbix agent;&lt;br&gt;10 - SNMPv1 agent;&lt;br&gt;11 - SNMPv2 agent;&lt;br&gt;12 - ICMP ping;&lt;br&gt;13 - SNMPv3 agent;&lt;br&gt;14 - HTTPS;&lt;br&gt;15 - Telnet.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|
|uniq|integer|Whether to use this check as a device uniqueness criteria. Only a single unique check can be configured for a discovery rule.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* do not use this check as a uniqueness criteria;&lt;br&gt;1 - use this check as a uniqueness criteria.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `type` is set to "Zabbix agent", "SNMPv1 agent", "SNMPv2 agent", or "SNMPv3 agent"|
|host\_source|integer|Source for host name.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;1 - *(default)* DNS;&lt;br&gt;2 - IP;&lt;br&gt;3 - discovery value of this check.|
|name\_source|integer|Source for visible name.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* not specified;&lt;br&gt;1 - DNS;&lt;br&gt;2 - IP;&lt;br&gt;3 - discovery value of this check.|
|allow\_redirect|integer|Allow situation where the target being ICMP pinged responds from a different IP address.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* treat redirected responses as if the target host is down (fail);&lt;br&gt;1 - treat redirected responses as if the target host is up (success).&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `type` is set to "ICMP ping"|</source>
      </trans-unit>
    </body>
  </file>
</xliff>
