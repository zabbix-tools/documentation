[comment]: # translation:outdated

[comment]: # ({new-14e441a9})
# map.get

[comment]: # ({/new-14e441a9})

[comment]: # ({new-480bee08})
### Description

`integer/array map.get(object parameters)`

The method allows to retrieve maps according to the given parameters.

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-480bee08})

[comment]: # ({new-c0fdc6dd})
### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|sysmapids|string/array|Returns only maps with the given IDs.|
|userids|string/array|Returns only maps that belong to the given user IDs.|
|expandUrls|flag|Adds global map URLs to the corresponding map elements and expands macros in all map element URLs.|
|selectIconMap|query|Returns an [iconmap](/manual/api/reference/iconmap/object) property with the icon map used on the map.|
|selectLinks|query|Returns a [links](/manual/api/reference/map/object#Map_link) property with the map links between elements.|
|selectSelements|query|Returns a [selements](/manual/api/reference/map/object#map_element) property with the map elements.|
|selectUrls|query|Returns a [urls](/manual/api/reference/map/object#Map_URL) property with the map URLs.|
|selectUsers|query|Returns a [users](/manual/api/reference/map/object#Map_user) property with users that the map is shared with.|
|selectUserGroups|query|Returns a [userGroups](/manual/api/reference/map/object#Map_user_group) property with user groups that the map is shared with.|
|selectShapes|query|Returns a [shapes](/manual/api/reference/map/object#Map_shapes) property with the map shapes.|
|selectLines|query|Returns a [lines](/manual/api/reference/map/object#Map_lines) property with the map lines.|
|sortfield|string/array|Sort the result by the given properties.<br><br>Possible values are: `name`, `width` and `height`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters).|
|editable|boolean|^|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

[comment]: # ({/new-c0fdc6dd})

[comment]: # ({new-7223bab1})
### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-cb769d11})
#### Retrieve a map

Retrieve all data about map "3".

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "map.get",
    "params": {
        "output": "extend",
        "selectSelements": "extend",
        "selectLinks": "extend",
        "selectUsers": "extend",
        "selectUserGroups": "extend",
        "selectShapes": "extend",
        "selectLines": "extend",
        "sysmapids": "3"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "selements": [
                {
                    "selementid": "10",
                    "sysmapid": "3",
                    "elementtype": "4",
                    "evaltype": "0",
                    "iconid_off": "1",
                    "iconid_on": "0",
                    "label": "Zabbix server",
                    "label_location": "3",
                    "x": "11",
                    "y": "141",
                    "iconid_disabled": "0",
                    "iconid_maintenance": "0",
                    "elementsubtype": "0",
                    "areatype": "0",
                    "width": "200",
                    "height": "200",
                    "tags": [
                        {
                            "tag": "service",
                            "value": "mysqld",
                            "operator": "0"
                        }
                    ],
                    "viewtype": "0",
                    "use_iconmap": "1",
                    "urls": [],
                    "elements": []
                },
                {
                    "selementid": "11",
                    "sysmapid": "3",
                    "elementtype": "4",
                    "evaltype": "0",
                    "iconid_off": "1",
                    "iconid_on": "0",
                    "label": "Web server",
                    "label_location": "3",
                    "x": "211",
                    "y": "191",
                    "iconid_disabled": "0",
                    "iconid_maintenance": "0",
                    "elementsubtype": "0",
                    "areatype": "0",
                    "width": "200",
                    "height": "200",
                    "viewtype": "0",
                    "use_iconmap": "1",
                    "tags": [],
                    "urls": [],
                    "elements": []
                },
                {
                    "selementid": "12",
                    "sysmapid": "3",
                    "elementtype": "0",
                    "evaltype": "0",
                    "iconid_off": "185",
                    "iconid_on": "0",
                    "label": "{HOST.NAME}\r\n{HOST.CONN}",
                    "label_location": "0",
                    "x": "111",
                    "y": "61",
                    "iconid_disabled": "0",
                    "iconid_maintenance": "0",
                    "elementsubtype": "0",
                    "areatype": "0",
                    "width": "200",
                    "height": "200",
                    "viewtype": "0",
                    "use_iconmap": "0",
                    "tags": [],
                    "urls": [],
                    "elements": [
                        {
                            "hostid": "10084"
                        }
                    ]
                }
            ],
            "links": [
                {
                    "linkid": "23",
                    "sysmapid": "3",
                    "selementid1": "10",
                    "selementid2": "11",
                    "drawtype": "0",
                    "color": "00CC00",
                    "label": "",
                    "linktriggers": []
                }
            ],
            "users": [
                {
                    "sysmapuserid": "1",
                    "userid": "2",
                    "permission": "2"
                }
            ],
            "userGroups": [
                {
                    "sysmapusrgrpid": "1",
                    "usrgrpid": "7",
                    "permission": "2"
                }
            ],
            "shapes":[  
                {  
                    "sysmap_shapeid":"1",
                    "type":"0",
                    "x":"0",
                    "y":"0",
                    "width":"680",
                    "height":"15",
                    "text":"{MAP.NAME}",
                    "font":"9",
                    "font_size":"11",
                    "font_color":"000000",
                    "text_halign":"0",
                    "text_valign":"0",
                    "border_type":"0",
                    "border_width":"0",
                    "border_color":"000000",
                    "background_color":"",
                    "zindex":"0"
                }
            ],
            "lines":[
                {
                    "sysmap_shapeid":"2",
                    "x1": 30,
                    "y1": 10,
                    "x2": 100,
                    "y2": 50,
                    "line_type": 1,
                    "line_width": 10,
                    "line_color": "009900",
                    "zindex":"1"
                }
            ],
            "sysmapid": "3",
            "name": "Local nerwork",
            "width": "400",
            "height": "400",
            "backgroundid": "0",
            "label_type": "2",
            "label_location": "3",
            "highlight": "1",
            "expandproblem": "1",
            "markelements": "0",
            "show_unack": "0",
            "grid_size": "50",
            "grid_show": "1",
            "grid_align": "1",
            "label_format": "0",
            "label_type_host": "2",
            "label_type_hostgroup": "2",
            "label_type_trigger": "2",
            "label_type_map": "2",
            "label_type_image": "2",
            "label_string_host": "",
            "label_string_hostgroup": "",
            "label_string_trigger": "",
            "label_string_map": "",
            "label_string_image": "",
            "iconmapid": "0",
            "expand_macros": "0",
            "severity_min": "0",
            "userid": "1",
            "private": "1",
            "show_suppressed": "1"
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-cb769d11})

[comment]: # ({new-4be4cd90})
### See also

-   [Icon map](/manual/api/reference/iconmap/object#icon_map)
-   [Map element](object#map_element)
-   [Map link](object#map_link)
-   [Map URL](object#map_url)
-   [Map user](object#map_user)
-   [Map user group](object#map_user_group)
-   [Map shapes](object#map_shapes)
-   [Map lines](object#map_lines)

[comment]: # ({/new-4be4cd90})

[comment]: # ({new-d61644e7})
### Source

CMap::get() in *ui/include/classes/api/services/CMap.php*.

[comment]: # ({/new-d61644e7})
