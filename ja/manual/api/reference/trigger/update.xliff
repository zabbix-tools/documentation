<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="ja" datatype="plaintext" original="manual/api/reference/trigger/update.md">
    <body>
      <trans-unit id="a7557797" xml:space="preserve">
        <source># trigger.update</source>
      </trans-unit>
      <trans-unit id="476a7855" xml:space="preserve">
        <source>### Description

`object trigger.update(object/array triggers)`

This method allows to update existing triggers.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::</source>
      </trans-unit>
      <trans-unit id="b918ffdf" xml:space="preserve">
        <source>### Parameters

`(object/array)` Trigger properties to be updated.

The `triggerid` property must be defined for each trigger, all other
properties are optional. Only the passed properties will be updated, all
others will remain unchanged.

Additionally to the [standard trigger properties](object#trigger) the
method accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|dependencies|array|Triggers that the trigger is dependent on.&lt;br&gt;&lt;br&gt;The triggers must have the `triggerid` property defined.|
|tags|array|Trigger [tags.](/manual/api/reference/trigger/object#trigger_tag)|

::: noteimportant
The trigger expression has to be given in its
expanded form.
:::</source>
      </trans-unit>
      <trans-unit id="18d0cc04" xml:space="preserve">
        <source>### Return values

`(object)` Returns an object containing the IDs of the updated triggers
under the `triggerids` property.</source>
      </trans-unit>
      <trans-unit id="b41637d2" xml:space="preserve">
        <source>### Examples</source>
      </trans-unit>
      <trans-unit id="07c64481" xml:space="preserve">
        <source>#### Enabling a trigger

Enable a trigger, that is, set its status to "0".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "trigger.update",
    "params": {
        "triggerid": "13938",
        "status": 0
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "triggerids": [
            "13938"
        ]
    },
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="b2bab7ce" xml:space="preserve">
        <source>#### Replacing triggers tags

Replace tags for trigger.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "trigger.update",
    "params": {
        "triggerid": "13938",
        "tags": [
            {
                "tag": "service",
                "value": "{{ITEM.VALUE}.regsub(\"Service (.*) has stopped\", \"\\1\")}"
            },
            {
                "tag": "error",
                "value": ""
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "triggerids": [
            "13938"
        ]
    },
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="db463204" xml:space="preserve">
        <source>#### Replacing dependencies

Replace dependencies for trigger.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "trigger.update",
    "params": {
        "triggerid": "22713",
        "dependencies": [
            {
                "triggerid": "22712"
            },
            {
                "triggerid": "22772"
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "triggerids": [
            "22713"
        ]
    },
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="180a8172" xml:space="preserve">
        <source>### Source

CTrigger::update() in *ui/include/classes/api/services/CTrigger.php*.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
