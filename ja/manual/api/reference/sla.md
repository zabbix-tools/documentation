[comment]: # translation:outdated

[comment]: # ({new-830462e8})
# SLA

This class is designed to work with SLA (Service Level Agreement) objects used to estimate the performance of 
IT infrastructure and business services.

Object references:\

-   [SLA](/manual/api/reference/sla/object#sla)
-   [SLA schedule](/manual/api/reference/sla/object#sla-schedule)
-   [SLA excluded downtime](/manual/api/reference/sla/object#sla-excluded-downtime)
-   [SLA service tag](/manual/api/reference/sla/object#sla-service-tag)

Available methods:\

-   [sla.create](/manual/api/reference/sla/create) - creating new SLAs
-   [sla.delete](/manual/api/reference/sla/delete) - deleting SLAs
-   [sla.get](/manual/api/reference/sla/get) - retrieving SLAs
-   [sla.getsli](/manual/api/reference/sla/getsla) - retrieving availability information as Service Level Indicator (SLI)
-   [sla.update](/manual/api/reference/sla/update) - updating SLAs

[comment]: # ({/new-830462e8})
