[comment]: # translation:outdated

[comment]: # ({new-06845577})
# Action

This class is designed to work with actions.

Object references:\

-   [Action](/manual/api/reference/action/object#action)
-   [Action
    condition](/manual/api/reference/action/object#action_condition)
-   [Action
    operation](/manual/api/reference/action/object#action_operation)

Available methods:\

-   [action.create](/manual/api/reference/action/create) - create new
    actions
-   [action.delete](/manual/api/reference/action/delete) - delete
    actions
-   [action.get](/manual/api/reference/action/get) - retrieve actions
-   [action.update](/manual/api/reference/action/update) - update
    actions

[comment]: # ({/new-06845577})
