<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="ja" datatype="plaintext" original="manual/api/reference/proxy/update.md">
    <body>
      <trans-unit id="92a86fab" xml:space="preserve">
        <source># proxy.update</source>
      </trans-unit>
      <trans-unit id="381a99b9" xml:space="preserve">
        <source>### Description

`object proxy.update(object/array proxies)`

This method allows to update existing proxies.

::: noteclassic
This method is only available to *Super admin* user type. Permissions to call the method can be revoked in user role
settings. See [User roles](/manual/web_interface/frontend_sections/users/user_roles) for more information.
:::</source>
      </trans-unit>
      <trans-unit id="4d6a0721" xml:space="preserve">
        <source>### Parameters

`(object/array)` Proxy properties to be updated.

The `proxyid` property must be defined for each proxy, all other properties are optional. Only the passed properties
will be updated, all others will remain unchanged.

Additionally to the [standard proxy properties](object#proxy), the method accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|hosts|array|[Hosts](/manual/api/reference/host/object) to be monitored by the proxy. If a host is already monitored by a different proxy, it will be reassigned to the current proxy.&lt;br&gt;&lt;br&gt;The hosts must have the `hostid` property defined.|
|interface|object|Host [interface](/manual/api/reference/hostinterface/object) to replace the existing interface for the passive proxy.&lt;br&gt;&lt;br&gt;[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):&lt;br&gt;- *supported* if `status` of [Proxy object](object#proxy) is set to "passive proxy"|</source>
      </trans-unit>
      <trans-unit id="bab02241" xml:space="preserve">
        <source>### Return values

`(object)` Returns an object containing the IDs of the updated proxies under the `proxyids` property.</source>
      </trans-unit>
      <trans-unit id="b41637d2" xml:space="preserve">
        <source>### Examples</source>
      </trans-unit>
      <trans-unit id="3613ce9a" xml:space="preserve">
        <source>#### Change hosts monitored by a proxy

Update the proxy to monitor the two given hosts.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "proxy.update",
    "params": {
        "proxyid": "10293",
        "hosts": [
            {
                "hostid": "10294"
            },
            {
                "hostid": "10295"
            },
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "proxyids": [
            "10293"
        ]
    },
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="f06b99dc" xml:space="preserve">
        <source>#### Change proxy status

Change the proxy to an active proxy and rename it to "Active proxy".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "proxy.update",
    "params": {
        "proxyid": "10293",
        "host": "Active proxy",
        "status": "5"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "proxyids": [
            "10293"
        ]
    },
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="273e0fd8" xml:space="preserve">
        <source>### See also

-   [Host](/manual/api/reference/host/object#host)
-   [Proxy interface](object#proxy_interface)</source>
      </trans-unit>
      <trans-unit id="8237a78a" xml:space="preserve">
        <source>### Source

CProxy::update() in *ui/include/classes/api/services/CProxy.php*.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
