[comment]: # translation:outdated

[comment]: # ({new-4e570b8c})
# > SLA object

The following objects are directly related to the `sla` (Service Level Agreement) API.

[comment]: # ({/new-4e570b8c})

[comment]: # ({new-8451803c})
### SLA

The SLA object has the following properties.

| Property                   | [Type](/manual/api/reference_commentary#data_types) | Description                                                                                                                                                                                                                               |
|----------------------------|-----------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| slaid                      | string                                              | *(readonly)* ID of the SLA.                                                                                                                                                                                                               |
| **name**<br>(required)     | string                                              | Name of the SLA.                                                                                                                                                                                                                          |
| **period**<br>(required)   | integer                                             | Reporting period of the SLA.<br><br>Possible values:<br>0 - daily;<br>1 - weekly;<br>2 - monthly;<br>3 - quarterly;<br>4 - annually.                                                                                                      |
| **slo**<br>(required)      | float                                               | Minimum acceptable Service Level Objective expressed as a percent. If the Service Level Indicator (SLI) drops lower, the SLA is considered to be in problem/unfulfilled state.<br><br>Possible values: 0-100 (up to 4 fractional digits). |
| effective_date             | integer                                             | Effective date of the SLA.<br><br>Possible values: date timestamp in UTC.                                                                                                                                                                 |
| **timezone**<br>(required) | string                                              | Reporting time zone, for example: `Europe/London`, `UTC`.<br><br>For the full list of supported time zones please refer to [PHP documentation](https://www.php.net/manual/en/timezones.php).                                              |
| status                     | integer                                             | Status of the SLA.<br><br>Possible values:<br>0 - *(default)* enabled SLA;<br>1 - disabled SLA.                                                                                                                                           |
| description                | string                                              | Description of the SLA.                                                                                                                                                                                                                   |

[comment]: # ({/new-8451803c})

[comment]: # ({new-632f7fa7})
### SLA Schedule

The SLA schedule object defines periods where the connected service(s) are scheduled to be in working order.
It has the following properties.

[comment]: # ({/new-632f7fa7})

[comment]: # ({new-db954c0c})
| Property                       | [Type](/manual/api/reference_commentary#data_types) | Description                                                                                                                         |
|--------------------------------|-----------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------|
| **period\_from**<br>(required) | integer                                             | Starting time of the recurrent weekly period of time (inclusive).<br><br>Possible values: number of seconds (counting from Sunday). |
| **period\_to**<br>(required)   | integer                                             | Ending time of the recurrent weekly period of time (exclusive).<br><br>Possible values: number of seconds (counting from Sunday).   |

[comment]: # ({/new-db954c0c})

[comment]: # ({new-5bbe7eb6})
### SLA excluded downtime

The excluded downtime object defines periods where the connected service(s) are scheduled to be out of working order,
without affecting SLI, e.g. undergoing planned maintenance.

[comment]: # ({/new-5bbe7eb6})

[comment]: # ({new-fa71231c})
| Property                       | [Type](/manual/api/reference_commentary#data_types) | Description                                                                           |
|--------------------------------|-----------------------------------------------------|---------------------------------------------------------------------------------------|
| **name**<br>(required)         | string                                              | Name of the excluded downtime.                                                        |
| **period\_from**<br>(required) | integer                                             | Starting time of the excluded downtime (inclusive).<br><br>Possible values: timestamp. |
| **period\_to**<br>(required)   | integer                                             | Ending time of the excluded downtime (exclusive).<br><br>Possible values: timestamp.  |

[comment]: # ({/new-fa71231c})

[comment]: # ({new-9f9ccbfd})
### SLA service tag

The SLA service tag object links services to include in the calculations for the SLA. It has the following properties.

[comment]: # ({/new-9f9ccbfd})

[comment]: # ({new-e4881380})
| Property              | [Type](/manual/api/reference_commentary#data_types) | Description                                                                             |
|-----------------------|-----------------------------------------------------|-----------------------------------------------------------------------------------------|
| **tag**<br>(required) | string                                              | SLA service tag name.                                                                   |
| operator              | integer                                             | SLA service tag operator.<br><br>Possible values:<br>0 - _(default)_ equals;<br>2 - like |
| value                 | string                                              | SLA service tag value.                                                                  |

[comment]: # ({/new-e4881380})
