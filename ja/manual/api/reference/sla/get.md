[comment]: # translation:outdated

[comment]: # ({new-2587902b})
# sla.get

[comment]: # ({/new-2587902b})

[comment]: # ({new-bfd5de7f})
### Description

`integer/array sla.get(object parameters)`

The method allows to retrieve SLA objects according to the given parameters.

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-bfd5de7f})

[comment]: # ({new-e7637a1d})
### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

| Parameter               | [Type](/manual/api/reference_commentary#data_types) | Description                                                                                                                                                               |
|-------------------------|-----------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| slaids                  | string/array                                        | Return only SLAs with the given IDs.                                                                                                                                      |
| serviceids              | string/array                                        | Return only SLAs matching the specific services.                                                                                                                          |
|                         |                                                     |                                                                                                                                                                           |
| selectSchedule          | query                                               | Return a `schedule` property with SLA schedules.<br><br>Supports `count`.                                                                                                 |
| selectExcludedDowntimes | query                                               | Return an `excluded_downtimes` property with SLA excluded downtimes.<br><br>Supports `count`.                                                                             |
| selectServiceTags       | query                                               | Return a `service_tags` property with SLA service tags.<br><br>Supports `count`.                                                                                          |
|                         |                                                     |                                                                                                                                                                           |
| sortfield               | string/array                                        | Sort the result by the given properties.<br><br>Possible values are: `slaid`, `name`, `period`, `slo`, `effective_date`, `timezone`, `status` and `description`.          |
| countOutput             | boolean                                             | These parameters being common for all `get` methods are described in detail in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters). |
| editable                | boolean                                             | ^                                                                                                                                                                         |
| excludeSearch           | boolean                                             | ^                                                                                                                                                                         |
| filter                  | object                                              | ^                                                                                                                                                                         |
| limit                   | integer                                             | ^                                                                                                                                                                         |
| output                  | query                                               | ^                                                                                                                                                                         |
| preservekeys            | boolean                                             | ^                                                                                                                                                                         |
| search                  | object                                              | ^                                                                                                                                                                         |
| searchByAny             | boolean                                             | ^                                                                                                                                                                         |
| searchWildcardsEnabled  | boolean                                             | ^                                                                                                                                                                         |
| sortorder               | string/array                                        | ^                                                                                                                                                                         |
| startSearch             | boolean                                             | ^                                                                                                                                                                         |

[comment]: # ({/new-e7637a1d})

[comment]: # ({new-7223bab1})
### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has been used.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-cd4e4011})
#### Retrieving all SLAs

Retrieve all data about all SLAs and their properties.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "sla.get",
    "params": {
        "output": "extend",
        "selectSchedule": ["period_from", "period_to"],
        "selectExcludedDowntimes": ["name", "period_from", "period_to"],
        "selectServiceTags": ["tag", "operator", "value"],
        "preservekeys": true
    },
    "auth": "85dd04b94cbfad794616eb923be13c71",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "1": {
            "slaid": "1",
            "name": "Database Uptime",
            "period": "1",
            "slo": "99.9995",
            "effective_date": "1672444800",
            "timezone": "America/Toronto",
            "status": "1",
            "description": "Provide excellent uptime for main SQL database engines.",
            "service_tags": [
                {
                    "tag": "Database",
                    "operator": "0",
                    "value": "MySQL"
                },
                {
                    "tag": "Database",
                    "operator": "0",
                    "value": "PostgreSQL"
                }
            ],
            "schedule": [
                {
                    "period_from": "0",
                    "period_to": "601200"
                }
            ],
            "excluded_downtimes": [
                {
                    "name": "Software version upgrade rollout",
                    "period_from": "1648760400",
                    "period_to": "1648764900"
                }
            ]
        }
    },
    "id": 1
}
```

[comment]: # ({/new-cd4e4011})

[comment]: # ({new-05e3cf18})
### Source

CSla:get() in *ui/include/classes/api/services/CSla.php*.

[comment]: # ({/new-05e3cf18})
