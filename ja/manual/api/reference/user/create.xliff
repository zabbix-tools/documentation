<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="ja" datatype="plaintext" original="manual/api/reference/user/create.md">
    <body>
      <trans-unit id="a79da37c" xml:space="preserve">
        <source># user.create</source>
      </trans-unit>
      <trans-unit id="7dc5480b" xml:space="preserve">
        <source>### Description

`object user.create(object/array users)`

This method allows to create new users.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::

::: noteclassic
The strength of user password is validated according the
password policy rules defined by Authentication API. See [Authentication
API](/manual/api/reference/authentication) for more
information.
:::</source>
      </trans-unit>
      <trans-unit id="9e6b7fba" xml:space="preserve">
        <source>### Parameters

`(object/array)` Users to create.

Additionally to the [standard user properties](object#user), the method
accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|usrgrps|array|User [groups](/manual/api/reference/user/create) to add the user to.&lt;br&gt;&lt;br&gt;The user groups must have the `usrgrpid` property defined.|
|medias|array|User [media](/manual/api/reference/user/object#media) to be created.|</source>
      </trans-unit>
      <trans-unit id="7e32f5f9" xml:space="preserve">
        <source>### Return values

`(object)` Returns an object containing the IDs of the created users
under the `userids` property. The order of the returned IDs matches the
order of the passed users.</source>
      </trans-unit>
      <trans-unit id="b41637d2" xml:space="preserve">
        <source>### Examples</source>
      </trans-unit>
      <trans-unit id="0df8c129" xml:space="preserve">
        <source>#### Creating a user

Create a new user, add him to a user group and create a new media for
him.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "user.create",
    "params": {
        "username": "John",
        "passwd": "Doe123",
        "roleid": "5",
        "usrgrps": [
            {
                "usrgrpid": "7"
            }
        ],
        "medias": [
            {
                "mediatypeid": "1",
                "sendto": [
                    "support@company.com"
                ],
                "active": 0,
                "severity": 63,
                "period": "1-7,00:00-24:00"
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "userids": [
            "12"
        ]
    },
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="130699eb" xml:space="preserve">
        <source>### See also

-   [Authentication](/manual/api/reference/authentication)
-   [Media](/manual/api/reference/user/object#media)
-   [User group](/manual/api/reference/usergroup/object#user_group)
-   [Role](/manual/api/reference/role/object#role)</source>
      </trans-unit>
      <trans-unit id="61de33e2" xml:space="preserve">
        <source>### Source

CUser::create() in *ui/include/classes/api/services/CUser.php*.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
