[comment]: # translation:outdated

[comment]: # ({new-fb5f6958})
# mediatype.create

[comment]: # ({/new-fb5f6958})

[comment]: # ({new-34dd1ecc})
### Description

`object mediatype.create(object/array mediaTypes)`

This method allows to create new media types.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-34dd1ecc})

[comment]: # ({new-dbb8bb8d})
### Parameters

`(object/array)` Media types to create.

Additionally to the [standard media type properties](object#media_type),
the method accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|parameters|array|[Webhook parameters](object#webhook_parameters) to be created for the media type.|
|message\_templates|array|[Message templates](object#message_template) to be created for the media type.|

[comment]: # ({/new-dbb8bb8d})

[comment]: # ({new-b1eed639})
### Return values

`(object)` Returns an object containing the IDs of the created media
types under the `mediatypeids` property. The order of the returned IDs
matches the order of the passed media types.

[comment]: # ({/new-b1eed639})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-e61703a8})
#### Creating an e-mail media type

Create a new e-mail media type with a custom SMTP port and message
templates.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "mediatype.create",
    "params": {
        "type": "0",
        "name": "E-mail",
        "smtp_server": "mail.example.com",
        "smtp_helo": "example.com",
        "smtp_email": "zabbix@example.com",
        "smtp_port": "587",
        "content_type": "1",
        "message_templates": [
            {
                "eventsource": "0",
                "recovery": "0",
                "subject": "Problem: {EVENT.NAME}",
                "message": "Problem \"{EVENT.NAME}\" on host \"{HOST.NAME}\" started at {EVENT.TIME}."
            },
            {
                "eventsource": "0",
                "recovery": "1",
                "subject": "Resolved in {EVENT.DURATION}: {EVENT.NAME}",
                "message": "Problem \"{EVENT.NAME}\" on host \"{HOST.NAME}\" has been resolved at {EVENT.RECOVERY.TIME} on {EVENT.RECOVERY.DATE}."
            },
            {
                "eventsource": "0",
                "recovery": "2",
                "subject": "Updated problem in {EVENT.AGE}: {EVENT.NAME}",
                "message": "{USER.FULLNAME} {EVENT.UPDATE.ACTION} problem \"{EVENT.NAME}\" on host \"{HOST.NAME}\" at {EVENT.UPDATE.DATE} {EVENT.UPDATE.TIME}."
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "mediatypeids": [
            "7"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-e61703a8})

[comment]: # ({new-1711e041})
#### Creating a script media type

Create a new script media type with a custom value for the number of
attempts and the interval between them.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "mediatype.create",
    "params": {
        "type": "1",
        "name": "Push notifications",
        "exec_path": "push-notification.sh",
        "exec_params": "{ALERT.SENDTO}\n{ALERT.SUBJECT}\n{ALERT.MESSAGE}\n",
        "maxattempts": "5",
        "attempt_interval": "11s"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "mediatypeids": [
            "8"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-1711e041})

[comment]: # ({new-9932dd76})
#### Creating a webhook media type

Create a new webhook media type.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "mediatype.create",
    "params": {
        "type": "4",
        "name": "Webhook",
        "script": "var Webhook = {\r\n    token: null,\r\n    to: null,\r\n    subject: null,\r\n    message: null,\r\n\r\n    sendMessage: function() {\r\n        // some logic\r\n    }\r\n}",
        "parameters": [
            {
                "name": "Message",
                "value": "{ALERT.MESSAGE}"
            },
            {
                "name": "Subject",
                "value": "{ALERT.SUBJECT}"
            },
            {
                "name": "To",
                "value": "{ALERT.SENDTO}"
            },
            {
                "name": "Token",
                "value": "<Token>"
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "mediatypeids": [
            "9"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-9932dd76})

[comment]: # ({new-06979650})
### Source

CMediaType::create() in
*ui/include/classes/api/services/CMediaType.php*.

[comment]: # ({/new-06979650})
