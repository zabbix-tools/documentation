[comment]: # translation:outdated

[comment]: # ({new-068e317e})
# Regular expression

This class is designed to work with global regular expressions.

Object references:\

-   [Regular expression](/manual/api/reference/regexp/object)

Available methods:\

-   [regexp.create](/manual/api/reference/regexp/create) - creating new
    regular expressions
-   [regexp.delete](/manual/api/reference/regexp/delete) - deleting
    regular expressions
-   [regexp.get](/manual/api/reference/regexp/get) - retrieving regular
    expressions
-   [regexp.update](/manual/api/reference/regexp/update) - updating
    regular expressions

[comment]: # ({/new-068e317e})
