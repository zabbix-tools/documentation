[comment]: # translation:outdated

[comment]: # ({new-52ad42a2})
# Configuration

This class is designed to export and import Zabbix configuration data.

Available methods:\

-   [configuration.export](/manual/api/reference/configuration/export) -
    exporting the configuration
-   [configuration.import](/manual/api/reference/configuration/import) -
    importing the configuration

[comment]: # ({/new-52ad42a2})
