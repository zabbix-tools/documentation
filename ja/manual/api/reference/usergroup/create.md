[comment]: # translation:outdated

[comment]: # ({new-c40e8cd1})
# usergroup.create

[comment]: # ({/new-c40e8cd1})

[comment]: # ({new-c74b5711})
### Description

`object usergroup.create(object/array userGroups)`

This method allows to create new user groups.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-c74b5711})

[comment]: # ({new-3b20a75b})
### Parameters

`(object/array)` User groups to create.

Additionally to the [standard user group properties](object#user_group),
the method accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|rights|object/array|[Permissions](/manual/api/reference/usergroup/object#permission) to assign to the group|
|tag\_filters|array|[Tag based permissions](/manual/api/reference/usergroup/object#tag_based_permission) to assign to the group|
|users|object/array|[Users](/manual/api/reference/user/create) to add to the user group.<br><br>The user must have the `userid` property defined.|

[comment]: # ({/new-3b20a75b})

[comment]: # ({new-19e4f0cc})
### Return values

`(object)` Returns an object containing the IDs of the created user
groups under the `usrgrpids` property. The order of the returned IDs
matches the order of the passed user groups.

[comment]: # ({/new-19e4f0cc})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-61abd39a})
#### Creating a user group

Create a user group, which denies access to host group "2", and add a
user to it.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "usergroup.create",
    "params": {
        "name": "Operation managers",
        "rights": {
            "permission": 0,
            "id": "2"
        },
        "users": [
            {"userid": "12"}
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "usrgrpids": [
            "20"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-61abd39a})

[comment]: # ({new-d5c7ed4f})
### See also

-   [Permission](object#permission)

[comment]: # ({/new-d5c7ed4f})

[comment]: # ({new-2d7f8c71})
### Source

CUserGroup::create() in
*ui/include/classes/api/services/CUserGroup.php*.

[comment]: # ({/new-2d7f8c71})
