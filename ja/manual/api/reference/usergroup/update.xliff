<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="ja" datatype="plaintext" original="manual/api/reference/usergroup/update.md">
    <body>
      <trans-unit id="9d4a1084" xml:space="preserve">
        <source># usergroup.update</source>
      </trans-unit>
      <trans-unit id="9fdfde6c" xml:space="preserve">
        <source>### Description

`object usergroup.update(object/array userGroups)`

This method allows to update existing user groups.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::</source>
      </trans-unit>
      <trans-unit id="93a5d635" xml:space="preserve">
        <source>### Parameters

`(object/array)` User group properties to be updated.

The `usrgrpid` property must be defined for each user group, all other
properties are optional. Only the passed properties will be updated, all
others will remain unchanged.

Additionally to the [standard user group properties](object#user_group),
the method accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|hostgroup_rights|object/array|Host group [permissions](/manual/api/reference/usergroup/object#permission) to replace the current permissions assigned to the user group.|
|templategroup_rights|object/array|Template group [permissions](/manual/api/reference/usergroup/object#permission) to replace the current permissions assigned to the user group.|
|tag\_filters|array|[Tag-based permissions](/manual/api/reference/usergroup/object#tag_based_permission) to assign to the user group.|
|users|object/array|[Users](/manual/api/reference/user/create) to add to the user group.&lt;br&gt;&lt;br&gt;The user must have the `userid` property defined.|
|rights&lt;br&gt;(deprecated)|object/array|This parameter is deprecated, please use `hostgroup_rights` or `templategroup_rights` instead.&lt;br&gt;[Permissions](/manual/api/reference/usergroup/object#permission) to assign to the user group.|</source>
      </trans-unit>
      <trans-unit id="c04afd7f" xml:space="preserve">
        <source>### Return values

`(object)` Returns an object containing the IDs of the updated user
groups under the `usrgrpids` property.</source>
      </trans-unit>
      <trans-unit id="b41637d2" xml:space="preserve">
        <source>### Examples</source>
      </trans-unit>
      <trans-unit id="e1a75e7b" xml:space="preserve">
        <source>#### Enabling a user group and updating permissions

Enable a user group and provide read-write access for it to host groups "2" and "4".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "usergroup.update",
    "params": {
        "usrgrpid": "17",
        "users_status": "0",
        "hostgroup_rights": [
            {
                "id": "2",
                "permission": 3
            },
            {
                "id": "4",
                "permission": 3
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "usrgrpids": [
            "17"
        ]
    },
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="d5c7ed4f" xml:space="preserve">
        <source>### See also

-   [Permission](object#permission)</source>
      </trans-unit>
      <trans-unit id="09e213c5" xml:space="preserve">
        <source>### Source

CUserGroup::update() in
*ui/include/classes/api/services/CUserGroup.php*.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
