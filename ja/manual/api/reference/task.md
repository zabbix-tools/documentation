[comment]: # translation:outdated

[comment]: # ({new-ca37dbd6})
# Task

This class is designed to work with tasks (such as checking items or
low-level discovery rules without config reload).

Object references:\

-   [Task](/manual/api/reference/task/object)
-   ['Check now' request
    object](/manual/api/reference/task/object#check_now_request_object)
-   ['Diagnostic information' request
    object](/manual/api/reference/task/object#diagnostic_information_request_object)
-   [Statistic request
    object](/manual/api/reference/task/object#statistic_request_object)
-   [Statistic result
    object](/manual/api/reference/task/object#statistic_result_object)

Available methods:\

-   [task.create](/manual/api/reference/task/create) - creating new
    tasks
-   [task.get](/manual/api/reference/task/get) - retrieving tasks

[comment]: # ({/new-ca37dbd6})
