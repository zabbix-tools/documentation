[comment]: # translation:outdated

[comment]: # ({new-2135842b})
# > Role object

The following objects are directly related to the `role` API.

[comment]: # ({/new-2135842b})

[comment]: # ({new-1708e020})
### Role

The role object has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|roleid|string|*(readonly)* ID of the role.|
|**name**<br>(required)|string|Name of the role.|
|**type**<br>(required)|integer|User type.<br><br>Possible values:<br>1 - *(default)* User;<br>2 - Admin;<br>3 - Super admin.|
|readonly|integer|*(readonly)* Whether the role is readonly.<br><br>Possible values:<br>0 - *(default)* No;<br>1 - Yes.|

[comment]: # ({/new-1708e020})

[comment]: # ({new-8a9b3008})
### Role rules

The role rules object has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|ui|array|Array of the [UI element](object#ui_element) objects.|
|ui.default\_access|integer|Whether access to new UI elements is enabled.<br><br>Possible values:<br>0 - Disabled;<br>1 - *(default)* Enabled.|
|services.read.mode|integer|Read-only access to services.<br><br>Possible values:<br><br>0 - Read-only access to the services, specified by the `services.read.list` or matched by the `services.read.tag` properties.<br>1 - *(default)* Read-only access to all services.|
|services.read.list|array|Array of [Service](object#service) objects.<br><br>The specified services, including child services, will be granted a read-only access to the user role. Read-only access will not override read-write access to the services.<br><br>Only used if `services.read.mode` is set to 0.|
|services.read.tag|object|Array of [Service tag](object#service_tag) object.<br><br>The tag matched services, including child services, will be granted a read-only access to the user role. Read-only access will not override read-write access to the services.<br><br>Only used if `services.read.mode` is set to 0.|
|services.write.mode|integer|Read-write access to services.<br><br>Possible values:<br><br>0 - (default) Read-write access to the services, specified by the `services.write.list` or matched by the `services.write.tag` properties.<br>1 - Read-write access to all services.|
|services.write.list|array|Array of [Service](object#service) objects.<br><br>The specified services, including child services, will be granted a read-write access to the user role. Read-write access will override read-only access to the services.<br><br>Only used if `services.write.mode` is set to 0.|
|services.write.tag|object|Array of [Service tag](object#service_tag) object.<br><br>The tag matched services, including child services, will be granted a read-write access to the user role. Read-write access will override read-only access to the services.<br><br>Only used if `services.write.mode` is set to 0.|
|modules|array|Array of the [module](object#module) objects.|
|modules.default\_access|integer|Whether access to new modules is enabled.<br><br>Possible values:<br>0 - Disabled;<br>1 - *(default)* Enabled.|
|api.access|integer|Whether access to API is enabled.<br><br>Possible values:<br>0 - Disabled;<br>1 - *(default)* Enabled.|
|api.mode|integer|Mode for treating API methods listed in the `api` property.<br><br>Possible values:<br>0 - *(default)* Deny list;<br>1 - Allow list.|
|api|array|Array of API methods.|
|actions|array|Array of the [action](object#action) objects.|
|actions.default\_access|integer|Whether access to new actions is enabled.<br><br>Possible values:<br>0 - Disabled;<br>1 - *(default)* Enabled.|

[comment]: # ({/new-8a9b3008})

[comment]: # ({new-006cdfb5})
### UI element

The UI element object has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**name**<br>(required)|string|Name of the UI element.<br><br>Possible values for users of any type:<br>`monitoring.dashboard` - *Monitoring → Dashboard*;<br>`monitoring.problems` - *Monitoring → Problems*;<br>`monitoring.hosts` - *Monitoring → Hosts*;<br>`monitoring.overview` - *Monitoring → Overview*;<br>`monitoring.latest_data` - *Monitoring → Latest data*;<br>`monitoring.maps` - *Monitoring → Maps*;<br>`monitoring.services` - *Monitoring → Services*;<br>`inventory.overview` - *Inventory → Overview*;<br>`inventory.hosts` - *Inventory → Hosts*;<br>`reports.availability_report` - *Reports → Availability report*;<br>`reports.top_triggers` - *Reports → Triggers top 100*.<br><br>Possible values only for users of *Admin* and *Super admin* user types:<br>`monitoring.discovery` - *Monitoring → Discovery*;<br>`reports.scheduled_reports` - *Reports → Scheduled reports*;<br>`reports.notifications` - *Reports → Notifications*;<br>`configuration.host_groups` - *Configuration → Host groups*;<br>`configuration.templates` - *Configuration → Templates*;<br>`configuration.hosts` - *Configuration → Hosts*;<br>`configuration.maintenance` - *Configuration → Maintenance*;<br>`configuration.actions` - *Configuration → Actions*;<br>`configuration.discovery` - *Configuration → Discovery*.<br><br>Possible values only for users of *Super admin* user type:<br>`reports.system_info` - *Reports → System information*;<br>`reports.audit` - *Reports → Audit*;<br>`reports.action_log` - *Reports → Action log*;<br>`configuration.event_correlation` - *Configuration → Event correlation*;<br>`administration.general` - *Administration → General*;<br>`administration.proxies` - *Administration → Proxies*;<br>`administration.authentication` - *Administration → Authentication*;<br>`administration.user_groups` - *Administration → User groups*;<br>`administration.user_roles` - *Administration → User roles*;<br>`administration.users` - *Administration → Users*;<br>`administration.media_types` - *Administration → Media types*;<br>`administration.scripts` - *Administration → Scripts*;<br>`administration.queue` - *Administration → Queue*.|
|status|integer|Whether access to the UI element is enabled.<br><br>Possible values:<br>0 - Disabled;<br>1 - *(default)* Enabled.|

[comment]: # ({/new-006cdfb5})

[comment]: # ({new-ebf4bb3a})
### Service

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**serviceid**<br>(required)|string|ID of the Service.|

[comment]: # ({/new-ebf4bb3a})

[comment]: # ({new-300e6d5b})
### Service tag

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**tag**<br>(required)|string|Tag name.<br><br>If empty string is specified, the service tag will not be used for service matching.|
|value|string|Tag value.<br><br>If no value or empty string is specified, only the tag name will be used for service matching.|

[comment]: # ({/new-300e6d5b})

[comment]: # ({new-f70e75e9})
### Module

The module object has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**moduleid**<br>(required)|string|ID of the module.|
|status|integer|Whether access to the module is enabled.<br><br>Possible values:<br>0 - Disabled;<br>1 - *(default)* Enabled.|

[comment]: # ({/new-f70e75e9})

[comment]: # ({new-957a00bb})
### Action

The action object has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**name**<br>(required)|string|Name of the action.<br><br>Possible values for users of any type:<br>`edit_dashboards` - Create and edit dashboards;<br>`edit_maps` - Create and edit maps;<br>`add_problem_comments` - Add problem comments;<br>`change_severity` - Change problem severity;<br>`acknowledge_problems` - Acknowledge problems;<br>`close_problems` - Close problems;<br>`execute_scripts` - Execute scripts;<br>`manage_api_tokens` - Manage API tokens.<br><br>Possible values only for users of *Admin* and *Super admin* user types:<br>`edit_maintenance` - Create and edit maintenances;<br>`manage_scheduled_reports` - Manage scheduled reports.|
|status|integer|Whether access to perform the action is enabled.<br><br>Possible values:<br>0 - Disabled;<br>1 - *(default)* Enabled.|

[comment]: # ({/new-957a00bb})
