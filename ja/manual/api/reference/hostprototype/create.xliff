<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="ja" datatype="plaintext" original="manual/api/reference/hostprototype/create.md">
    <body>
      <trans-unit id="984854ac" xml:space="preserve">
        <source># hostprototype.create</source>
      </trans-unit>
      <trans-unit id="3ddb6b5d" xml:space="preserve">
        <source>### Description

`object hostprototype.create(object/array hostPrototypes)`

This method allows to create new host prototypes.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::</source>
      </trans-unit>
      <trans-unit id="c4127cc1" xml:space="preserve">
        <source>### Parameters

`(object/array)` Host prototypes to create.

Additionally to the [standard host prototype
properties](object#host_prototype), the method accepts the following
parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|groupLinks|array|Group [links](/manual/api/reference/hostprototype/object#group_link) to be created for the host prototype.&lt;br&gt;&lt;br&gt;[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):&lt;br&gt;- *required*|
|ruleid|string|ID of the LLD rule that the host prototype belongs to.&lt;br&gt;&lt;br&gt;[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):&lt;br&gt;- *required*|
|groupPrototypes|array|Group [prototypes](/manual/api/reference/hostprototype/object#group_prototype) to be created for the host prototype.|
|macros|object/array|[User macros](/manual/api/reference/usermacro/object) to be created for the host prototype.|
|tags|object/array|Host prototype [tags](/manual/api/reference/hostprototype/object#host_prototype_tag).|
|interfaces|object/array|Host prototype [custom interfaces](/manual/api/reference/hostprototype/object#custom_interface).|
|templates|object/array|[Templates](/manual/api/reference/template/object) to be linked to the host prototype.&lt;br&gt;&lt;br&gt;The templates must have the `templateid` property defined.|</source>
      </trans-unit>
      <trans-unit id="b9613119" xml:space="preserve">
        <source>### Return values

`(object)` Returns an object containing the IDs of the created host
prototypes under the `hostids` property. The order of the returned IDs
matches the order of the passed host prototypes.</source>
      </trans-unit>
      <trans-unit id="b41637d2" xml:space="preserve">
        <source>### Examples</source>
      </trans-unit>
      <trans-unit id="0ce023ac" xml:space="preserve">
        <source>#### Creating a host prototype

Create a host prototype "{\#VM.NAME}" on LLD rule "23542" with a group
prototype "{\#HV.NAME}", tag pair "Datacenter": "{\#DATACENTER.NAME}"
and custom SNMPv2 interface 127.0.0.1:161 with community
{$SNMP\_COMMUNITY}. Link it to host group "2".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "hostprototype.create",
    "params": {
        "host": "{#VM.NAME}",
        "ruleid": "23542",
        "custom_interfaces": "1",
        "groupLinks": [
            {
                "groupid": "2"
            }
        ],
        "groupPrototypes": [
            {
                "name": "{#HV.NAME}"
            }
        ],
        "tags": [
            {
                "tag": "Datacenter",
                "value": "{#DATACENTER.NAME}"
            }
        ],
        "interfaces": [
            {
                "main": "1",
                "type": "2",
                "useip": "1",
                "ip": "127.0.0.1",
                "dns": "",
                "port": "161",
                "details": {
                    "version": "2",
                    "bulk": "1",
                    "community": "{$SNMP_COMMUNITY}"
                }
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "hostids": [
            "10103"
        ]
    },
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="c4a38bf3" xml:space="preserve">
        <source>### See also

-   [Group link](object#group_link)
-   [Group prototype](object#group_prototype)
-   [Host prototype tag](object#host_prototype_tag)
-   [Custom interface](object#custom_interface)
-   [User
    macro](/manual/api/reference/usermacro/object#hosttemplate_level_macro)</source>
      </trans-unit>
      <trans-unit id="4d10a354" xml:space="preserve">
        <source>### Source

CHostPrototype::create() in
*ui/include/classes/api/services/CHostPrototype.php*.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
