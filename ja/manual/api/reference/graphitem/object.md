[comment]: # translation:outdated

[comment]: # ({new-168caa7a})
# > Graph item object

The following objects are directly related to the `graphitem` API.

[comment]: # ({/new-168caa7a})

[comment]: # ({new-f6083c57})
### Graph item

::: noteclassic
Graph items can only be modified via the `graph`
API.
:::

The graph item object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|gitemid|string|*(readonly)* ID of the graph item.|
|**color**<br>(required)|string|Graph item's draw color as a hexadecimal color code.|
|**itemid**<br>(required)|string|ID of the item.|
|calc\_fnc|integer|Value of the item that will be displayed.<br><br>Possible values:<br>1 - minimum value;<br>2 - *(default)* average value;<br>4 - maximum value;<br>7 - all values;<br>9 - last value, used only by pie and exploded graphs.|
|drawtype|integer|Draw style of the graph item.<br><br>Possible values:<br>0 - *(default)* line;<br>1 - filled region;<br>2 - bold line;<br>3 - dot;<br>4 - dashed line;<br>5 - gradient line.|
|graphid|string|ID of the graph that the graph item belongs to.|
|sortorder|integer|Position of the item in the graph.<br><br>Default: starts with 0 and increases by one with each entry.|
|type|integer|Type of graph item.<br><br>Possible values:<br>0 - *(default)* simple;<br>2 - graph sum, used only by pie and exploded graphs.|
|yaxisside|integer|Side of the graph where the graph item's Y scale will be drawn.<br><br>Possible values:<br>0 - *(default)* left side;<br>1 - right side.|

[comment]: # ({/new-f6083c57})
