[comment]: # translation:outdated

[comment]: # ({new-05585755})
# > Alert object

The following objects are directly related to the `alert` API.

[comment]: # ({/new-05585755})

[comment]: # ({new-933fed24})
### Alert

::: noteclassic
Alerts are created by the Zabbix server and cannot be
modified via the API.
:::

The alert object contains information about whether certain action
operations have been executed successfully. It has the following
properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|alertid|string|ID of the alert.|
|actionid|string|ID of the action that generated the alert.|
|alerttype|integer|Alert type.<br><br>Possible values:<br>0 - message;<br>1 - remote command.|
|clock|timestamp|Time when the alert was generated.|
|error|string|Error text if there are problems sending a message or running a command.|
|esc\_step|integer|Action escalation step during which the alert was generated.|
|eventid|string|ID of the event that triggered the action.|
|mediatypeid|string|ID of the media type that was used to send the message.|
|message|text|Message text. Used for message alerts.|
|retries|integer|Number of times Zabbix tried to send the message.|
|sendto|string|Address, user name or other identifier of the recipient. Used for message alerts.|
|status|integer|Status indicating whether the action operation has been executed successfully.<br><br>Possible values for message alerts:<br>0 - message not sent.<br>1 - message sent.<br>2 - failed after a number of retries.<br>3 - new alert is not yet processed by alert manager.<br><br>Possible values for command alerts:<br>0 - command not run.<br>1 - command run.<br>2 - tried to run the command on the Zabbix agent but it was unavailable.|
|subject|string|Message subject. Used for message alerts.|
|userid|string|ID of the user that the message was sent to.|
|p\_eventid|string|ID of problem event, which generated the alert.|
|acknowledgeid|string|ID of acknowledgment, which generated the alert.|

[comment]: # ({/new-933fed24})
