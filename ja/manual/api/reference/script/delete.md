[comment]: # translation:outdated

[comment]: # ({new-ceca660a})
# script.delete

[comment]: # ({/new-ceca660a})

[comment]: # ({new-db451e99})
### Description

`object script.delete(array scriptIds)`

This method allows to delete scripts.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-db451e99})

[comment]: # ({new-17e2e442})
### Parameters

`(array)` IDs of the scripts to delete.

[comment]: # ({/new-17e2e442})

[comment]: # ({new-64908342})
### Return values

`(object)` Returns an object containing the IDs of the deleted scripts
under the `scriptids` property.

[comment]: # ({/new-64908342})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-8184d373})
#### Delete multiple scripts

Delete two scripts.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "script.delete",
    "params": [
        "3",
        "4"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "scriptids": [
            "3",
            "4"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-8184d373})

[comment]: # ({new-ccc98e5e})
### Source

CScript::delete() in *ui/include/classes/api/services/CScript.php*.

[comment]: # ({/new-ccc98e5e})
