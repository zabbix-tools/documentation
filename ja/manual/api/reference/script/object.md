[comment]: # translation:outdated

[comment]: # ({new-8b1700c1})
# > Script object

The following objects are directly related to the `script` API.

[comment]: # ({/new-8b1700c1})

[comment]: # ({new-9c6db006})
### Script

The script object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|scriptid|string|*(readonly)* ID of the script.|
|**name**<br>(required)|string|Name of the script.|
|**type**<br>(required)|integer|Script type.<br><br>Possible values:<br>0 - Script;<br>1 - IPMI;<br>2 - SSH;<br>3 - Telnet;<br>5 - *(default)* Webhook.|
|**command**<br>(required)|string|Command to run.|
|scope|integer|Script scope.<br><br>Possible values:<br>1 - *default* action operation;<br>2 - manual host action;<br>4 - manual event action.|
|execute\_on|integer|Where to run the script.<br>Used if `type` is `0` (script).<br><br>Possible values:<br>0 - run on Zabbix agent;<br>1 - run on Zabbix server;<br>2 - *(default)* run on Zabbix server (proxy).|
|menu\_path|string|Folders separated by slash that form a menu like navigation in frontend when clicked on host or event.<br>Used if `scope` is `2` or `4`.|
|authtype|integer|Authentication method used for SSH script type.<br>Used if `type` is `2`.<br><br>Possible values:<br>0 - password;<br>1 - public key.|
|username|string|User name used for authentication.<br>Required if `type` is `2` or `3`.|
|password|string|Password used for SSH scripts with password authentication and Telnet scripts.<br>Used if `type` is `2` and `authtype` is `0` or `type` is `3`.|
|publickey|string|Name of the public key file used for SSH scripts with public key authentication.<br>Required if type is `2` and `authtype` is `1`.|
|privatekey|string|Name of the private key file used for SSH scripts with public key authentication.<br>Required if type is `2` and `authtype` is `1`.|
|port|string|Port number used for SSH and Telnet scripts.<br>Used if type is `2` or `3`.|
|groupid|string|ID of the host group that the script can be run on. If set to 0, the script will be available on all host groups.<br><br>Default: 0.|
|usrgrpid|string|ID of the user group that will be allowed to run the script. If set to 0, the script will be available for all user groups.<br>Used if `scope` is `2` or `4`.<br><br>Default: 0.|
|host\_access|integer|Host permissions needed to run the script.<br>Used if `scope` is `2` or `4`.<br><br>Possible values:<br>2 - *(default)* read;<br>3 - write.|
|confirmation|string|Confirmation pop up text. The pop up will appear when trying to run the script from the Zabbix frontend.<br>Used if `scope` is `2` or `4`.|
|timeout|string|Webhook script execution timeout in seconds. Time suffixes are supported, e.g. 30s, 1m.<br>Required if `type` is `5`.<br><br>Possible values:<br>1-60s<br><br>Default value:<br>30s|
|parameters|array|Array of [webhook input parameters](/manual/api/reference/script/object#Webhook parameters).<br>Used if `type` is `5`.|
|description|string|Description of the script.|

[comment]: # ({/new-9c6db006})

[comment]: # ({new-05b0e37d})
### Webhook parameters

Parameters passed to webhook script when it is called have the following
properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**name**<br>(required)|string|Parameter name.|
|value|string|Parameter value. Supports [macros](/manual/appendix/macros/supported_by_location).|

[comment]: # ({/new-05b0e37d})

[comment]: # ({new-587d2fd0})
### Debug

Debug information of executed webhook script. The debug object has the
following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|logs|array|Array of [log entries](/manual/api/reference/script/object#Log entry).|
|ms|string|Script execution duration in milliseconds.|

[comment]: # ({/new-587d2fd0})

[comment]: # ({new-8ea23127})
### Log entry

The log entry object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|level|integer|Log level.|
|ms|string|The time elapsed in milliseconds since the script was run before log entry was added.|
|message|string|Log message.|

[comment]: # ({/new-8ea23127})
