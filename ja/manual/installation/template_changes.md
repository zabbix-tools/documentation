[comment]: # translation:outdated

[comment]: # ({new-0a5cc230})
# 9 Template changes

This page lists all changes to the stock templates that are shipped with
Zabbix.

Note that upgrading to the latest Zabbix version will not automatically
upgrade the templates used. It is suggested to modify the templates in
existing installations by:

-   Downloading the latest templates from the [Zabbix Git
    repository](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates);
-   Then, while in *Configuration* → *Templates* you can import them
    manually into Zabbix. If templates with the same names already
    exist, the *Delete missing* options should be checked when importing
    to achieve a clean import. This way the old items that are no longer
    in the updated template will be removed (note that it will mean
    losing history of these old items).

[comment]: # ({/new-0a5cc230})

[comment]: # ({new-94950308})
### CHANGES IN 7.0.0

[comment]: # ({/new-94950308})

[comment]: # ({new-651539db})
#### Updated templates

**Zabbix server/proxy health**

Templates for Zabbix server/proxy health have been updated according to the [changes](/manual/introduction/whatsnew700#concurrency-in-network-discovery) 
in network discovery. The items for monitoring the discoverer process (now removed) has been replaced by items to measure the 
discovery manager and discovery worker process utilization. A new internal item has been added for monitoring the discovery queue. 

[comment]: # ({/new-651539db})








