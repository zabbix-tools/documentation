[comment]: # translation:outdated

[comment]: # ({new-7900383c})
# Upgrade from sources

[comment]: # ({/new-7900383c})

[comment]: # ({new-92c7a8e5})
#### Overview

This section provides the steps required for a successful
[upgrade](/manual/installation/upgrade) from Zabbix **5.4**.x to Zabbix
**6.0**.x using official Zabbix sources.

While upgrading Zabbix agents is not mandatory (but recommended), Zabbix
server and proxies must be of the [same major
version](/manual/appendix/compatibility). Therefore, in a server-proxy
setup, Zabbix server and all proxies have to be stopped and upgraded.
Keeping proxies running no longer will bring any benefit as during proxy
upgrade their old data will be discarded and no new data will be
gathered until proxy configuration is synced with server.

::: noteimportant
It is no longer possible to start the upgraded
server and have older, yet unupgraded proxies report data to a newer
server. This approach, which was never recommended nor supported by
Zabbix, now is officially disabled, as the server will ignore data from
unupgraded proxies.
:::

Note that with SQLite database on proxies, history data from proxies
before the upgrade will be lost, because SQLite database upgrade is not
supported and the SQLite database file has to be manually removed. When
proxy is started for the first time and the SQLite database file is
missing, proxy creates it automatically.

Depending on database size the database upgrade to version 6.0 may take
a long time.

::: notewarning
Before the upgrade make sure to read the relevant
**upgrade notes!**
:::

The following upgrade notes are available:

|Upgrade from|Read full upgrade notes|Most important changes between versions|
|------------|-----------------------|---------------------------------------|
|5.4.x|For:<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|<|
|5.2.x|For:<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|Minimum required database versions upped;<br>Aggregate items removed as a separate type.|
|5.0.x|For:<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|Minimum required PHP version upped from 7.2.0 to 7.2.5.|
|4.4.x|For:<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)|Support of IBM DB2 dropped;<br>Minimum required PHP version upped from 5.4.0 to 7.2.0;<br>Minimum required database versions upped;<br>Changed Zabbix PHP file directory.|
|4.2.x|For:<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.4/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|Jabber, Ez Texting media types removed.|
|4.0.x LTS|For:<br>Zabbix [4.2](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_420)<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.4/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|Older proxies no longer can report data to an upgraded server;<br>Newer agents no longer will be able to work with an older Zabbix server.|
|3.4.x|For:<br>Zabbix [4.0](https://www.zabbix.com/documentation/4.0/manual/installation/upgrade_notes_400)<br>Zabbix [4.2](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_420)<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.4/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|'libpthread' and 'zlib' libraries now mandatory;<br>Support for plain text protocol dropped and header is mandatory;<br>Pre-1.4 version Zabbix agents are no longer supported;<br>The Server parameter in passive proxy configuration now mandatory.|
|3.2.x|For:<br>Zabbix [3.4](https://www.zabbix.com/documentation/3.4/manual/installation/upgrade_notes_340)<br>Zabbix [4.0](https://www.zabbix.com/documentation/4.0/manual/installation/upgrade_notes_400)<br>Zabbix [4.2](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_420)<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.4/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|SQLite support as backend database dropped for Zabbix server/frontend;<br>Perl Compatible Regular Expressions (PCRE) supported instead of POSIX extended;<br>'libpcre' and 'libevent' libraries mandatory for Zabbix server;<br>Exit code checks added for user parameters, remote commands and system.run\[\] items without the 'nowait' flag as well as Zabbix server executed scripts;<br>Zabbix Java gateway has to be upgraded to support new functionality.|
|3.0.x LTS|For:<br>Zabbix [3.2](https://www.zabbix.com/documentation/3.2/manual/installation/upgrade_notes_320)<br>Zabbix [3.4](https://www.zabbix.com/documentation/3.4/manual/installation/upgrade_notes_340)<br>Zabbix [4.0](https://www.zabbix.com/documentation/4.0/manual/installation/upgrade_notes_400)<br>Zabbix [4.2](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_420)<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.4/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|Database upgrade may be slow, depending on the history table size.|
|2.4.x|For:<br>Zabbix [3.0](https://www.zabbix.com/documentation/3.0/manual/installation/upgrade_notes_300)<br>Zabbix [3.2](https://www.zabbix.com/documentation/3.2/manual/installation/upgrade_notes_320)<br>Zabbix [3.4](https://www.zabbix.com/documentation/3.4/manual/installation/upgrade_notes_340)<br>Zabbix [4.0](https://www.zabbix.com/documentation/4.0/manual/installation/upgrade_notes_400)<br>Zabbix [4.2](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_420)<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|Minimum required PHP version upped from 5.3.0 to 5.4.0<br>LogFile agent parameter must be specified|
|2.2.x LTS|For:<br>Zabbix [2.4](https://www.zabbix.com/documentation/2.4/manual/installation/upgrade_notes_240)<br>Zabbix [3.0](https://www.zabbix.com/documentation/3.0/manual/installation/upgrade_notes_300)<br>Zabbix [3.2](https://www.zabbix.com/documentation/3.2/manual/installation/upgrade_notes_320)<br>Zabbix [3.4](https://www.zabbix.com/documentation/3.4/manual/installation/upgrade_notes_340)<br>Zabbix [4.0](https://www.zabbix.com/documentation/4.0/manual/installation/upgrade_notes_400)<br>Zabbix [4.2](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_420)<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|Node-based distributed monitoring removed|
|2.0.x|For:<br>Zabbix [2.2](https://www.zabbix.com/documentation/2.2/manual/installation/upgrade_notes_220)<br>Zabbix [2.4](https://www.zabbix.com/documentation/2.4/manual/installation/upgrade_notes_240)<br>Zabbix [3.0](https://www.zabbix.com/documentation/3.0/manual/installation/upgrade_notes_300)<br>Zabbix [3.2](https://www.zabbix.com/documentation/3.2/manual/installation/upgrade_notes_320)<br>Zabbix [3.4](https://www.zabbix.com/documentation/3.4/manual/installation/upgrade_notes_340)<br>Zabbix [4.0](https://www.zabbix.com/documentation/4.0/manual/installation/upgrade_notes_400)<br>Zabbix [4.2](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_420)<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|Minimum required PHP version upped from 5.1.6 to 5.3.0;<br>Case-sensitive MySQL database required for proper server work; character set utf8 and utf8\_bin collation is required for Zabbix server to work properly with MySQL database. See [database creation scripts](/manual/appendix/install/db_scripts#mysql).<br>'mysqli' PHP extension required instead of 'mysql'|

You may also want to check the
[requirements](/manual/installation/requirements) for 6.0.

::: notetip
It may be handy to run two parallel SSH sessions during
the upgrade, executing the upgrade steps in one and monitoring the
server/proxy logs in another. For example, run
`tail -f zabbix_server.log` or `tail -f zabbix_proxy.log` in the second
SSH session showing you the latest log file entries and possible errors
in real time. This can be critical for production
instances.
:::

[comment]: # ({/new-92c7a8e5})

[comment]: # ({new-101d6faf})
#### Server upgrade process

[comment]: # ({/new-101d6faf})

[comment]: # ({new-b8308740})
##### 1 Stop server

Stop Zabbix server to make sure that no new data is inserted into
database.

[comment]: # ({/new-b8308740})

[comment]: # ({new-ab13a6a4})
##### 2 Back up the existing Zabbix database

This is a very important step. Make sure that you have a backup of your
database. It will help if the upgrade procedure fails (lack of disk
space, power off, any unexpected problem).

[comment]: # ({/new-ab13a6a4})

[comment]: # ({new-d2778675})
##### 3 Back up configuration files, PHP files and Zabbix binaries

Make a backup copy of Zabbix binaries, configuration files and the PHP
file directory.

[comment]: # ({/new-d2778675})

[comment]: # ({new-2bb75ddd})
##### 4 Install new server binaries

Use these
[instructions](/manual/installation/install#installing_zabbix_daemons)
to compile Zabbix server from sources.

[comment]: # ({/new-2bb75ddd})

[comment]: # ({new-ec6edae7})
##### 5 Review server configuration parameters

See the upgrade notes for details on [mandatory
changes](/manual/installation/upgrade_notes_600#configuration_parameters).

For new optional parameters, see the [What's
new](/manual/introduction/whatsnew600#configuration_parameters) section.

[comment]: # ({/new-ec6edae7})

[comment]: # ({new-a01e967f})
##### 6 Start new Zabbix binaries

Start new binaries. Check log files to see if the binaries have started
successfully.

Zabbix server will automatically upgrade the database. When starting up,
Zabbix server reports the current (mandatory and optional) and required
database versions. If the current mandatory version is older than the
required version, Zabbix server automatically executes the required
database upgrade patches. The start and progress level (percentage) of
the database upgrade is written to the Zabbix server log file. When the
upgrade is completed, a "database upgrade fully completed" message is
written to the log file. If any of the upgrade patches fail, Zabbix
server will not start. Zabbix server will also not start if the current
mandatory database version is newer than the required one. Zabbix server
will only start if the current mandatory database version corresponds to
the required mandatory version.

    8673:20161117:104750.259 current database version (mandatory/optional): 03040000/03040000
    8673:20161117:104750.259 required mandatory version: 03040000

Before you start the server:

-   Make sure the database user has enough permissions (create table,
    drop table, create index, drop index)
-   Make sure you have enough free disk space.

[comment]: # ({/new-a01e967f})

[comment]: # ({new-22e0e4e9})
##### 7 Install new Zabbix web interface

The minimum required PHP version is 7.2.5. Update if needed and follow
[installation instructions](/manual/installation/frontend).

[comment]: # ({/new-22e0e4e9})

[comment]: # ({new-49e4f43e})
##### 8 Clear web browser cookies and cache

After the upgrade you may need to clear web browser cookies and web
browser cache for the Zabbix web interface to work properly.

[comment]: # ({/new-49e4f43e})

[comment]: # ({new-75f5b3ed})
#### Proxy upgrade process

[comment]: # ({/new-75f5b3ed})

[comment]: # ({new-f33e6ddb})
##### 1 Stop proxy

Stop Zabbix proxy.

[comment]: # ({/new-f33e6ddb})

[comment]: # ({new-2e03c550})
##### 2 Back up configuration files and Zabbix proxy binaries

Make a backup copy of the Zabbix proxy binary and configuration file.

[comment]: # ({/new-2e03c550})

[comment]: # ({new-024201dc})
##### 3 Install new proxy binaries

Use these
[instructions](/manual/installation/install#installing_zabbix_daemons)
to compile Zabbix proxy from sources.

[comment]: # ({/new-024201dc})

[comment]: # ({new-2fbda571})
##### 4 Review proxy configuration parameters

There are no mandatory changes in this version to proxy
[parameters](/manual/appendix/config/zabbix_proxy).

[comment]: # ({/new-2fbda571})

[comment]: # ({new-520f43c6})
##### 5 Start new Zabbix proxy

Start the new Zabbix proxy. Check log files to see if the proxy has
started successfully.

Zabbix proxy will automatically upgrade the database. Database upgrade
takes place similarly as when starting [Zabbix
server](/manual/installation/upgrade#start_new_zabbix_binaries).

[comment]: # ({/new-520f43c6})

[comment]: # ({new-59736bba})
#### Agent upgrade process

::: noteimportant
Upgrading agents is not mandatory. You only need
to upgrade agents if it is required to access the new
functionality.
:::

The upgrade procedure described in this section may be used for
upgrading both the Zabbix agent and the Zabbix agent 2.

[comment]: # ({/new-59736bba})

[comment]: # ({new-02698e69})
##### 1 Stop agent

Stop Zabbix agent.

[comment]: # ({/new-02698e69})

[comment]: # ({new-50263823})
##### 2 Back up configuration files and Zabbix agent binaries

Make a backup copy of the Zabbix agent binary and configuration file.

[comment]: # ({/new-50263823})

[comment]: # ({new-467f56b3})
##### 3 Install new agent binaries

Use these
[instructions](/manual/installation/install#installing_zabbix_daemons)
to compile Zabbix agent from sources.

Alternatively, you may download pre-compiled Zabbix agents from the
[Zabbix download page](http://www.zabbix.com/download.php).

[comment]: # ({/new-467f56b3})

[comment]: # ({new-34721aad})
##### 4 Review agent configuration parameters

There are no mandatory changes in this version neither to
[agent](/manual/appendix/config/zabbix_agentd) nor to [agent
2](/manual/appendix/config/zabbix_agent2) parameters.

[comment]: # ({/new-34721aad})

[comment]: # ({new-04c253a7})
##### 5 Start new Zabbix agent

Start the new Zabbix agent. Check log files to see if the agent has
started successfully.

[comment]: # ({/new-04c253a7})

[comment]: # ({new-a06019d2})
#### Upgrade between minor versions

When upgrading between minor versions of 6.0.x (for example from 6.0.1
to 6.0.3) it is required to execute the same actions for
server/proxy/agent as during the upgrade between major versions. The
only difference is that when upgrading between minor versions no changes
to the database are made.

[comment]: # ({/new-a06019d2})
