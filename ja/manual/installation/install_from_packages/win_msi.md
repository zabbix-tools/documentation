[comment]: # translation:outdated

[comment]: # ({new-7050624a})
# 4 Windows agent installation from MSI

[comment]: # ({/new-7050624a})

[comment]: # ({new-04777b60})
#### Overview

Zabbix Windows agent can be installed from Windows MSI installer
packages (32-bit or 64-bit) available for
[download](https://www.zabbix.com/download_agents#tab:44).

A 32-bit package cannot be installed on a 64-bit Windows.

All packages come with TLS support, however, configuring TLS is
optional.

Both UI and command-line based installation is supported.

[comment]: # ({/new-04777b60})

[comment]: # ({new-0fc3b21c})
#### Installation steps

To install, double-click the downloaded MSI file.

![](../../../../assets/en/manual/installation/install_from_packages/msi0_b.png)

![](../../../../assets/en/manual/installation/install_from_packages/msi0_c.png)

Accept the license to proceed to the next step.

![](../../../../assets/en/manual/installation/install_from_packages/msi0_d.png)

Specify the following parameters.

|Parameter|Description|
|---------|-----------|
|*Host name*|Specify host name.|
|*Zabbix server IP/DNS*|Specify IP/DNS of Zabbix server.|
|*Agent listen port*|Specify agent listen port (10050 by default).|
|*Server or Proxy for active checks*|Specify IP/DNS of Zabbix server/proxy for active agent checks.|
|*Enable PSK*|Mark the checkbox to enable TLS support via pre-shared keys.|
|*Add agent location to the PATH*|Add agent location to the PATH variable.|

![](../../../../assets/en/manual/installation/install_from_packages/msi0_e.png)

Enter pre-shared key identity and value. This step is only available if
you checked *Enable PSK* in the previous step.

![](../../../../assets/en/manual/installation/install_from_packages/msi0_f.png)

Select Zabbix components to install - [Zabbix agent
daemon](/manual/concepts/agent), [Zabbix
sender](/manual/concepts/sender), [Zabbix get](/manual/concepts/get).

![](../../../../assets/en/manual/installation/install_from_packages/msi0_g.png)

Zabbix components along with the configuration file will be installed in
a *Zabbix Agent* folder in Program Files. zabbix\_agentd.exe will be set
up as Windows service with automatic startup.

![](../../../../assets/en/manual/installation/install_from_packages/msi0_h.png)

[comment]: # ({/new-0fc3b21c})

[comment]: # ({new-1ca3dd90})
#### Command-line based installation

[comment]: # ({/new-1ca3dd90})

[comment]: # ({new-89028c66})
##### Supported parameters

The following set of parameters is supported by created MSIs:

|Number|Parameter|Description|
|------|---------|-----------|
|1|LOGTYPE|<|
|2|LOGFILE|<|
|3|SERVER|<|
|4|LISTENPORT|<|
|5|SERVERACTIVE|<|
|6|HOSTNAME|<|
|7|TIMEOUT|<|
|8|TLSCONNECT|<|
|9|TLSACCEPT|<|
|10|TLSPSKIDENTITY|<|
|11|TLSPSKFILE|<|
|12|TLSPSKVALUE|<|
|13|TLSCAFILE|<|
|14|TLSCRLFILE|<|
|15|TLSSERVERCERTISSUER|<|
|16|TLSSERVERCERTSUBJECT|<|
|17|TLSCERTFILE|<|
|18|TLSKEYFILE|<|
|19|INSTALLFOLDER|<|
|20|ENABLEPATH|<|
|21|SKIP|`SKIP=fw` - do not install firewall exception rule|

To install you may run, for example:

    SET INSTALLFOLDER=C:\Program Files\za

    msiexec /l*v log.txt /i zabbix_agent-4.0.6-x86.msi /qn^
     LOGTYPE=file^
     LOGFILE="%INSTALLFOLDER%\za.log"^
     SERVER=192.168.6.76^
     LISTENPORT=12345^
     SERVERACTIVE=::1^
     HOSTNAME=myHost^
     TLSCONNECT=psk^
     TLSACCEPT=psk^
     TLSPSKIDENTITY=MyPSKID^
     TLSPSKFILE="%INSTALLFOLDER%\mykey.psk"^
     TLSCAFILE="c:\temp\f.txt1"^
     TLSCRLFILE="c:\temp\f.txt2"^
     TLSSERVERCERTISSUER="My CA"^
     TLSSERVERCERTSUBJECT="My Cert"^
     TLSCERTFILE="c:\temp\f.txt5"^
     TLSKEYFILE="c:\temp\f.txt6"^
     ENABLEPATH=1^
     INSTALLFOLDER="%INSTALLFOLDER%"^
     SKIP=fw

or

    msiexec /l*v log.txt /i zabbix_agent-4.4.0-x86.msi /qn^
     SERVER=192.168.6.76^
     TLSCONNECT=psk^
     TLSACCEPT=psk^
     TLSPSKIDENTITY=MyPSKID^
     TLSPSKVALUE=1f87b595725ac58dd977beef14b97461a7c1045b9a1c963065002c5473194952

[comment]: # ({/new-89028c66})

[comment]: # ({new-bfabef9c})
##### Examples

To install Zabbix Windows agent from the command-line, you may run, for example:

    SET INSTALLFOLDER=C:\Program Files\za

    msiexec /l*v log.txt /i zabbix_agent-7.0.0-x86.msi /qn^
     LOGTYPE=file^
     LOGFILE="%INSTALLFOLDER%\za.log"^
     SERVER=192.168.6.76^
     LISTENPORT=12345^
     SERVERACTIVE=::1^
     HOSTNAME=myHost^
     TLSCONNECT=psk^
     TLSACCEPT=psk^
     TLSPSKIDENTITY=MyPSKID^
     TLSPSKFILE="%INSTALLFOLDER%\mykey.psk"^
     TLSCAFILE="c:\temp\f.txt1"^
     TLSCRLFILE="c:\temp\f.txt2"^
     TLSSERVERCERTISSUER="My CA"^
     TLSSERVERCERTSUBJECT="My Cert"^
     TLSCERTFILE="c:\temp\f.txt5"^
     TLSKEYFILE="c:\temp\f.txt6"^
     ENABLEPATH=1^
     INSTALLFOLDER="%INSTALLFOLDER%"^
     SKIP=fw^
     ALLOWDENYKEY="DenyKey=vfs.file.contents[/etc/passwd]"

You may also run, for example:

    msiexec /l*v log.txt /i zabbix_agent-7.0.0-x86.msi /qn^
     SERVER=192.168.6.76^
     TLSCONNECT=psk^
     TLSACCEPT=psk^
     TLSPSKIDENTITY=MyPSKID^
     TLSPSKVALUE=1f87b595725ac58dd977beef14b97461a7c1045b9a1c963065002c5473194952

::: noteclassic
If both TLSPSKFILE and TLSPSKVALUE are passed, then TLSPSKVALUE will be written to TLSPSKFILE.
:::

[comment]: # ({/new-bfabef9c})
