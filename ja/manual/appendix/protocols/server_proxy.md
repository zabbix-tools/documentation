[comment]: # translation:outdated

[comment]: # ({new-7669b1c2})
# 1 Server-proxy data exchange protocol

[comment]: # ({/new-7669b1c2})

[comment]: # ({new-7382efcd})
#### Overview

Server - proxy data exchange is based on JSON format.

Request and response messages must begin with [header and data
length](/manual/appendix/protocols/header_datalen).

[comment]: # ({/new-7382efcd})

[comment]: # ({new-14661e11})
#### Passive proxy

[comment]: # ({/new-14661e11})

[comment]: # ({new-fac082ad})
##### Proxy config request

The `proxy config` request is sent by server to provide proxy
configuration data. This request is sent every `ProxyConfigFrequency`
(server configuration parameter) seconds.

|name|<|<|<|value type|description|
|----|-|-|-|----------|-----------|
|server→proxy:|<|<|<|<|<|
|**request**|<|<|<|*string*|'proxy config'|
|**<table>**|<|<|<|*object*|one or more objects with <table> data|
|<|**fields**|<|<|*array*|array of field names|
|<|<|\-|<|*string*|field name|
|<|**data**|<|<|*array*|array of rows|
|<|<|\-|<|*array*|array of columns|
|<|<|<|\-|*string*,*number*|column value with type depending on column type in database schema|
|proxy→server:|<|<|<|<|<|
|**response**|<|<|<|*string*|the request success information ('success' or 'failed')|
|**version**|<|<|<|*string*|the proxy version (<major>.<minor>.<build>)|

Example:

server→proxy:

``` {.javascript}
{
    "request": "proxy config",
    "globalmacro":{
        "fields":[
            "globalmacroid",
            "macro",
            "value"
        ],
        "data":[
            [
                2,
                "{$SNMP_COMMUNITY}",
                "public"
            ]
        ]
    },
    "hosts":{
        "fields":[
            "hostid",
            "host",
            "status",
            "ipmi_authtype",
            "ipmi_privilege",
            "ipmi_username",
            "ipmi_password",
            "name",
            "tls_connect",
            "tls_accept",
            "tls_issuer",
            "tls_subject",
            "tls_psk_identity",
            "tls_psk"
        ],
        "data":[
            [
                10001,
                "Linux",
                3,
                -1,
                2,
                "",
                "",
                "Linux",
                1,
                1,
                "",
                "",
                "",
                ""
            ],
            [
                10050,
                "Zabbix Agent",
                3,
                -1,
                2,
                "",
                "",
                "Zabbix Agent",
                1,
                1,
                "",
                "",
                "",
                ""
            ],
            [
                10105,
                "Logger",
                0,
                -1,
                2,
                "",
                "",
                "Logger",
                1,
                1,
                "",
                "",
                "",
                ""
            ]
        ]
    },
    "interface":{
        "fields":[
            "interfaceid",
            "hostid",
            "main",
            "type",
            "useip",
            "ip",
            "dns",
            "port",
            "bulk"
        ],
        "data":[
            [
                2,
                10105,
                1,
                1,
                1,
                "127.0.0.1",
                "",
                "10050",
                1
            ]
        ]
    },
    ...
}
```

proxy→server:

``` {.javascript}
{
  "response": "success",
  "version": "5.4.0"
}
```

[comment]: # ({/new-fac082ad})

[comment]: # ({new-c348e5d9})
##### Proxy request

The `proxy data` request is used to obtain host interface availability,
historical, discovery and autoregistration data from proxy. This request
is sent every `ProxyDataFrequency` (server configuration parameter)
seconds.

|name|<|value type|description|
|----|-|----------|-----------|
|server→proxy:|<|<|<|
|**request**|<|*string*|'proxy data'|
|proxy→server:|<|<|<|
|**session**|<|*string*|data session token|
|**interface availability**|<|*array*|*(optional)* array of interface availability data objects|
|<|**interfaceid**|*number*|interface identifier|
|<|**available**|*number*|Interface availability<br><br>**0**, *INTERFACE\_AVAILABLE\_UNKNOWN* - unknown<br>**1**, *INTERFACE\_AVAILABLE\_TRUE* - available<br>**2**, *INTERFACE\_AVAILABLE\_FALSE* - unavailable|
|<|**error**|*string*|Interface error message or empty string|
|**history data**|<|*array*|*(optional)* array of history data objects|
|<|**itemid**|*number*|item identifier|
|<|**clock**|*number*|item value timestamp (seconds)|
|<|**ns**|*number*|item value timestamp (nanoseconds)|
|<|**value**|*string*|*(optional)* item value|
|<|**id**|*number*|value identifier (ascending counter, unique within one data session)|
|<|**timestamp**|*number*|*(optional)* timestamp of log type items|
|<|**source**|*string*|*(optional)* eventlog item source value|
|<|**severity**|*number*|*(optional)* eventlog item severity value|
|<|**eventid**|*number*|*(optional)* eventlog item eventid value|
|<|**state**|*string*|*(optional)* item state<br>**0**, *ITEM\_STATE\_NORMAL*<br>**1**, *ITEM\_STATE\_NOTSUPPORTED*|
|<|**lastlogsize**|*number*|*(optional)* last log size of log type items|
|<|**mtime**|*number*|*(optional)* modify time of log type items|
|**discovery data**|<|*array*|*(optional)* array of discovery data objects|
|<|**clock**|*number*|the discovery data timestamp|
|<|**druleid**|*number*|the discovery rule identifier|
|<|**dcheckid**|*number*|the discovery check identifier or null for discovery rule data|
|<|**type**|*number*|the discovery check type:<br><br>**-1** discovery rule data<br>**0**, *SVC\_SSH* - SSH service check<br>**1**, *SVC\_LDAP* - LDAP service check<br>**2**, *SVC\_SMTP* - SMTP service check<br>**3**, *SVC\_FTP* - FTP service check<br>**4**, *SVC\_HTTP* - HTTP service check<br>**5**, *SVC\_POP* - POP service check<br>**6**, *SVC\_NNTP* - NNTP service check<br>**7**, *SVC\_IMAP* - IMAP service check<br>**8**, *SVC\_TCP* - TCP port availability check<br>**9**, *SVC\_AGENT* - Zabbix agent<br>**10**, *SVC\_SNMPv1* - SNMPv1 agent<br>**11**, *SVC\_SNMPv2* - SNMPv2 agent<br>**12**, *SVC\_ICMPPING* - ICMP ping<br>**13**, *SVC\_SNMPv3* - SNMPv3 agent<br>**14**, *SVC\_HTTPS* - HTTPS service check<br>**15**, *SVC\_TELNET* - Telnet availability check|
|<|**ip**|*string*|the host IP address|
|<|**dns**|*string*|the host DNS name|
|<|**port**|*number*|*(optional)* service port number|
|<|**key\_**|*string*|*(optional)* the item key for discovery check of type **9** *SVC\_AGENT*|
|<|**value**|*string*|*(optional)* value received from the service, can be empty for most of services|
|<|**status**|*number*|*(optional)* service status:<br><br>**0**, *DOBJECT\_STATUS\_UP* - Service UP<br>**1**, *DOBJECT\_STATUS\_DOWN* - Service DOWN|
|**auto registration**|<|*array*|*(optional)* array of autoregistration data objects|
|<|**clock**|*number*|the autoregistration data timestamp|
|<|**host**|*string*|the host name|
|<|**ip**|*string*|*(optional)* the host IP address|
|<|**dns**|*string*|*(optional)* the resolved DNS name from IP address|
|<|**port**|*string*|*(optional)* the host port|
|<|**host\_metadata**|*string*|*(optional)* the host metadata sent by agent (based on HostMetadata or HostMetadataItem agent configuration parameter)|
|**tasks**|<|*array*|*(optional)* array of tasks|
|<|**type**|*number*|the task type:<br><br>**0**, *ZBX\_TM\_TASK\_PROCESS\_REMOTE\_COMMAND\_RESULT* - remote command result|
|<|**status**|*number*|the remote command execution status:<br><br>**0**, *ZBX\_TM\_REMOTE\_COMMAND\_COMPLETED* - the remote command completed successfully<br>**1**, *ZBX\_TM\_REMOTE\_COMMAND\_FAILED* - the remote command failed|
|<|**error**|*string*|*(optional)* the error message|
|<|**parent\_taskid**|*number*|the parent task id|
|**more**|<|*number*|*(optional)* 1 - there are more history data to send|
|**clock**|<|*number*|*(optional)* data transfer timestamp (seconds)|
|**ns**|<|*number*|*(optional)* data transfer timestamp (nanoseconds)|
|**version**|<|*string*|the proxy version (<major>.<minor>.<build>)|
|server→proxy:|<|<|<|
|**response**|<|*string*|the request success information ('success' or 'failed')|
|**tasks**|<|*array*|*(optional)* array of tasks|
|<|**type**|*number*|the task type:<br><br>**1**, *ZBX\_TM\_TASK\_PROCESS\_REMOTE\_COMMAND* - remote command|
|<|**clock**|*number*|the task creation time|
|<|**ttl**|*number*|the time in seconds after which task expires|
|<|**commandtype**|*number*|the remote command type:<br><br>**0**, *ZBX\_SCRIPT\_TYPE\_CUSTOM\_SCRIPT* - use custom script<br>**1**, *ZBX\_SCRIPT\_TYPE\_IPMI* - use IPMI<br>**2**, *ZBX\_SCRIPT\_TYPE\_SSH* - use SSH<br>**3**, *ZBX\_SCRIPT\_TYPE\_TELNET* - use Telnet<br>**4**, *ZBX\_SCRIPT\_TYPE\_GLOBAL\_SCRIPT* - use global script (currently functionally equivalent to custom script)|
|<|**command**|*string*|the remote command to execute|
|<|**execute\_on**|*number*|the execution target for custom scripts:<br><br>**0**, *ZBX\_SCRIPT\_EXECUTE\_ON\_AGENT* - execute script on agent<br>**1**, *ZBX\_SCRIPT\_EXECUTE\_ON\_SERVER* - execute script on server<br>**2**, *ZBX\_SCRIPT\_EXECUTE\_ON\_PROXY* - execute script on proxy|
|<|**port**|*number*|*(optional)* the port for telnet and ssh commands|
|<|**authtype**|*number*|*(optional)* the authentication type for ssh commands|
|<|**username**|*string*|*(optional)* the user name for telnet and ssh commands|
|<|**password**|*string*|*(optional)* the password for telnet and ssh commands|
|<|**publickey**|*string*|*(optional)* the public key for ssh commands|
|<|**privatekey**|*string*|*(optional)* the private key for ssh commands|
|<|**parent\_taskid**|*number*|the parent task id|
|<|**hostid**|*number*|target hostid|

Example:

server→proxy:

``` {.javascript}
{
  "request": "proxy data"
}
```

proxy→server:

``` {.javascript}
{
    "session": "12345678901234567890123456789012"
    "interface availability": [
        {
            "interfaceid": 1,
            "available": 1,
            "error": ""
    },
        {
            "interfaceid": 2,
            "available": 2,
            "error": "Get value from agent failed: cannot connect to [[127.0.0.1]:10049]: [111] Connection refused"
    },
        {
            "interfaceid": 3,
            "available": 1,
            "error": ""
    },
        {
            "interfaceid": 4,
            "available": 1,
            "error": ""
    }
    ],
    "history data":[
        {
            "itemid":"12345",
            "clock":1478609647,
            "ns":332510044,
            "value":"52956612",
            "id": 1
        },
        {
            "itemid":"12346",
            "clock":1478609647,
            "ns":330690279,
            "state":1,
            "value":"Cannot find information for this network interface in /proc/net/dev.",
            "id": 2
        }
    ],
    "discovery data":[
        {
            "clock":1478608764,
            "drule":2,
            "dcheck":3,
            "type":12,
            "ip":"10.3.0.10",
            "dns":"vdebian",
            "status":1
        },
        {
            "clock":1478608764,
            "drule":2,
            "dcheck":null,
            "type":-1,
            "ip":"10.3.0.10",
            "dns":"vdebian",
            "status":1
        }
    ],
    "auto registration":[
        {
            "clock":1478608371,
            "host":"Logger1",
            "ip":"10.3.0.1",
            "dns":"localhost",
            "port":"10050"
        },
        {
            "clock":1478608381,
            "host":"Logger2",
            "ip":"10.3.0.2",
            "dns":"localhost",
            "port":"10050"
        }
    ],
    "tasks":[
        {
            "type": 0,
            "status": 0,
            "parent_taskid": 10
        },
        {
            "type": 0,
            "status": 1,
            "error": "No permissions to execute task.",
            "parent_taskid": 20
        }
    ],    
    "version":"5.4.0"
}
```

server→proxy:

``` {.javascript}
{
  "response": "success",
  "tasks":[
      {
         "type": 1,
         "clock": 1478608371,
         "ttl": 600,
         "commandtype": 2,
         "command": "restart_service1.sh",
         "execute_on": 2,
         "port": 80,
         "authtype": 0,
         "username": "userA",
         "password": "password1",
         "publickey": "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCqGKukO1De7zhZj6+H0qtjTkVxwTCpvKe",
         "privatekey": "lsuusFncCzWBQ7RKNUSesmQRMSGkVb1/3j+skZ6UtW+5u09lHNsj6tQ5QCqGKukO1De7zhd",
         "parent_taskid": 10,
         "hostid": 10070
      },
      {
         "type": 1,
         "clock": 1478608381,
         "ttl": 600,
         "commandtype": 1,
         "command": "restart_service2.sh",
         "execute_on": 0,
         "authtype": 0,
         "username": "",
         "password": "",
         "publickey": "",
         "privatekey": "",
         "parent_taskid": 20,
         "hostid": 10084
      }
  ]
}
```

[comment]: # ({/new-c348e5d9})

[comment]: # ({new-967ea060})
#### Active proxy

[comment]: # ({/new-967ea060})


[comment]: # ({new-bed30a3b})
##### Proxy config request

The `proxy config` request is sent by proxy to obtain proxy
configuration data. This request is sent every `ConfigFrequency` (proxy
configuration parameter) seconds.

|name|<|<|<|value type|description|
|----|-|-|-|----------|-----------|
|proxy→server:|<|<|<|<|<|
|**request**|<|<|<|*string*|'proxy config'|
|**host**|<|<|<|*string*|proxy name|
|**version**|<|<|<|*string*|the proxy version (<major>.<minor>.<build>)|
|server→proxy:|<|<|<|<|<|
|**request**|<|<|<|*string*|'proxy config'|
|**<table>**|<|<|<|*object*|one or more objects with <table> data|
|<|**fields**|<|<|*array*|array of field names|
|<|<|\-|<|*string*|field name|
|<|**data**|<|<|*array*|array of rows|
|<|<|\-|<|*array*|array of columns|
|<|<|<|\-|*string*,*number*|column value with type depending on column type in database schema|
|proxy→server:|<|<|<|<|<|
|**response**|<|<|<|*string*|the request success information ('success' or 'failed')|

Example:

proxy→server:

``` {.javascript}
{
  "request": "proxy config",
  "host": "Proxy #12",
  "version":"5.4.0"
}
```

server→proxy:

``` {.javascript}
{
    "globalmacro":{
        "fields":[
            "globalmacroid",
            "macro",
            "value"
        ],
        "data":[
            [
                2,
                "{$SNMP_COMMUNITY}",
                "public"
            ]
        ]
    },
    "hosts":{
        "fields":[
            "hostid",
            "host",
            "status",
            "ipmi_authtype",
            "ipmi_privilege",
            "ipmi_username",
            "ipmi_password",
            "name",
            "tls_connect",
            "tls_accept",
            "tls_issuer",
            "tls_subject",
            "tls_psk_identity",
            "tls_psk"
        ],
        "data":[
            [
                10001,
                "Linux",
                3,
                -1,
                2,
                "",
                "",
                "Linux",
                1,
                1,
                "",
                "",
                "",
                ""
            ],
            [
                10050,
                "Zabbix Agent",
                3,
                -1,
                2,
                "",
                "",
                "Zabbix Agent",
                1,
                1,
                "",
                "",
                "",
                ""
            ],
            [
                10105,
                "Logger",
                0,
                -1,
                2,
                "",
                "",
                "Logger",
                1,
                1,
                "",
                "",
                "",
                ""
            ]
        ]
    },
    "interface":{
        "fields":[
            "interfaceid",
            "hostid",
            "main",
            "type",
            "useip",
            "ip",
            "dns",
            "port",
            "bulk"
        ],
        "data":[
            [
                2,
                10105,
                1,
                1,
                1,
                "127.0.0.1",
                "",
                "10050",
                1
            ]
        ]
    },
    ...
}
```

proxy→server:

``` {.javascript}
{
  "response": "success"
}
```

[comment]: # ({/new-bed30a3b})

[comment]: # ({new-ef0ba621})
##### Proxy data request

The `proxy data` request is sent by proxy to provide host interface
availability, history, discovery and autoregistration data. This request
is sent every `DataSenderFrequency` (proxy configuration parameter)
seconds.

|name|<|value type|description|
|----|-|----------|-----------|
|proxy→server:|<|<|<|
|**request**|<|*string*|'proxy data'|
|**host**|<|*string*|the proxy name|
|**session**|<|*string*|data session token|
|**interface availability**|<|*array*|*(optional)* array of interface availability data objects|
|<|**interfaceid**|*number*|interface identifier|
|<|**available**|*number*|Interface availability<br><br>**0**, *INTERFACE\_AVAILABLE\_UNKNOWN* - unknown<br>**1**, *INTERFACE\_AVAILABLE\_TRUE* - available<br>**2**, *INTERFACE\_AVAILABLE\_FALSE* - unavailable|
|<|**error**|*string*|Interface error message or empty string|
|**history data**|<|*array*|*(optional)* array of history data objects|
|<|**itemid**|*number*|item identifier|
|<|**clock**|*number*|item value timestamp (seconds)|
|<|**ns**|*number*|item value timestamp (nanoseconds)|
|<|**value**|*string*|*(optional)* item value|
|<|**id**|*number*|value identifier (ascending counter, unique within one data session)|
|<|**timestamp**|*number*|*(optional)* timestamp of log type items|
|<|**source**|*string*|*(optional)* eventlog item source value|
|<|**severity**|*number*|*(optional)* eventlog item severity value|
|<|**eventid**|*number*|*(optional)* eventlog item eventid value|
|<|**state**|*string*|*(optional)* item state<br>**0**, *ITEM\_STATE\_NORMAL*<br>**1**, *ITEM\_STATE\_NOTSUPPORTED*|
|<|**lastlogsize**|*number*|*(optional)* last log size of log type items|
|<|**mtime**|*number*|*(optional)* modify time of log type items|
|**discovery data**|<|*array*|*(optional)* array of discovery data objects|
|<|**clock**|*number*|the discovery data timestamp|
|<|**druleid**|*number*|the discovery rule identifier|
|<|**dcheckid**|*number*|the discovery check identifier or null for discovery rule data|
|<|**type**|*number*|the discovery check type:<br><br>**-1** discovery rule data<br>**0**, *SVC\_SSH* - SSH service check<br>**1**, *SVC\_LDAP* - LDAP service check<br>**2**, *SVC\_SMTP* - SMTP service check<br>**3**, *SVC\_FTP* - FTP service check<br>**4**, *SVC\_HTTP* - HTTP service check<br>**5**, *SVC\_POP* - POP service check<br>**6**, *SVC\_NNTP* - NNTP service check<br>**7**, *SVC\_IMAP* - IMAP service check<br>**8**, *SVC\_TCP* - TCP port availability check<br>**9**, *SVC\_AGENT* - Zabbix agent<br>**10**, *SVC\_SNMPv1* - SNMPv1 agent<br>**11**, *SVC\_SNMPv2* - SNMPv2 agent<br>**12**, *SVC\_ICMPPING* - ICMP ping<br>**13**, *SVC\_SNMPv3* - SNMPv3 agent<br>**14**, *SVC\_HTTPS* - HTTPS service check<br>**15**, *SVC\_TELNET* - Telnet availability check|
|<|**ip**|*string*|the host IP address|
|<|**dns**|*string*|the host DNS name|
|<|**port**|*number*|*(optional)* service port number|
|<|**key\_**|*string*|*(optional)* the item key for discovery check of type **9** *SVC\_AGENT*|
|<|**value**|*string*|*(optional)* value received from the service, can be empty for most of services|
|<|**status**|*number*|*(optional)* service status:<br><br>**0**, *DOBJECT\_STATUS\_UP* - Service UP<br>**1**, *DOBJECT\_STATUS\_DOWN* - Service DOWN|
|**auto registration**|<|*array*|*(optional)* array of autoregistration data objects|
|<|**clock**|*number*|the autoregistration data timestamp|
|<|**host**|*string*|the host name|
|<|**ip**|*string*|*(optional)* the host IP address|
|<|**dns**|*string*|*(optional)* the resolved DNS name from IP address|
|<|**port**|*string*|*(optional)* the host port|
|<|**host\_metadata**|*string*|*(optional)* the host metadata sent by agent (based on HostMetadata or HostMetadataItem agent configuration parameter)|
|**tasks**|<|*array*|*(optional)* array of tasks|
|<|**type**|*number*|the task type:<br><br>**0**, *ZBX\_TM\_TASK\_PROCESS\_REMOTE\_COMMAND\_RESULT* - remote command result|
|<|**status**|*number*|the remote command execution status:<br><br>**0**, *ZBX\_TM\_REMOTE\_COMMAND\_COMPLETED* - the remote command completed successfully<br>**1**, *ZBX\_TM\_REMOTE\_COMMAND\_FAILED* - the remote command failed|
|<|**error**|*string*|*(optional)* the error message|
|<|**parent\_taskid**|*number*|the parent task id|
|**more**|<|*number*|*(optional)* 1 - there are more history data to send|
|**clock**|<|*number*|*(optional)* data transfer timestamp (seconds)|
|**ns**|<|*number*|*(optional)* data transfer timestamp (nanoseconds)|
|**version**|<|*string*|the proxy version (<major>.<minor>.<build>)|
|server→proxy:|<|<|<|
|**response**|<|*string*|the request success information ('success' or 'failed')|
|**tasks**|<|*array*|*(optional)* array of tasks|
|<|**type**|*number*|the task type:<br><br>**1**, *ZBX\_TM\_TASK\_PROCESS\_REMOTE\_COMMAND* - remote command|
|<|**clock**|*number*|the task creation time|
|<|**ttl**|*number*|the time in seconds after which task expires|
|<|**commandtype**|*number*|the remote command type:<br><br>**0**, *ZBX\_SCRIPT\_TYPE\_CUSTOM\_SCRIPT* - use custom script<br>**1**, *ZBX\_SCRIPT\_TYPE\_IPMI* - use IPMI<br>**2**, *ZBX\_SCRIPT\_TYPE\_SSH* - use SSH<br>**3**, *ZBX\_SCRIPT\_TYPE\_TELNET* - use Telnet<br>**4**, *ZBX\_SCRIPT\_TYPE\_GLOBAL\_SCRIPT* - use global script (currently functionally equivalent to custom script)|
|<|**command**|*string*|the remote command to execute|
|<|**execute\_on**|*number*|the execution target for custom scripts:<br><br>**0**, *ZBX\_SCRIPT\_EXECUTE\_ON\_AGENT* - execute script on agent<br>**1**, *ZBX\_SCRIPT\_EXECUTE\_ON\_SERVER* - execute script on server<br>**2**, *ZBX\_SCRIPT\_EXECUTE\_ON\_PROXY* - execute script on proxy|
|<|**port**|*number*|*(optional)* the port for telnet and ssh commands|
|<|**authtype**|*number*|*(optional)* the authentication type for ssh commands|
|<|**username**|*string*|*(optional)* the user name for telnet and ssh commands|
|<|**password**|*string*|*(optional)* the password for telnet and ssh commands|
|<|**publickey**|*string*|*(optional)* the public key for ssh commands|
|<|**privatekey**|*string*|*(optional)* the private key for ssh commands|
|<|**parent\_taskid**|*number*|the parent task id|
|<|**hostid**|*number*|target hostid|

Example:

proxy→server:

``` {.javascript}
{
    "request": "proxy data",
    "host": "Proxy #12", 
    "session": "12345678901234567890123456789012",
    "interface availability": [
        {
            "interfaceid": 1,
            "available": 1,
            "error": ""
    },
        {
            "interfaceid": 2,
            "available": 2,
            "error": "Get value from agent failed: cannot connect to [[127.0.0.1]:10049]: [111] Connection refused"
    },
        {
            "interfaceid": 3,
            "available": 1,
            "error": ""
    },
        {
            "interfaceid": 4,
            "available": 1,
            "error": ""
    }
    ],
    "history data":[
        {
            "itemid":"12345",
            "clock":1478609647,
            "ns":332510044,
            "value":"52956612",
            "id": 1
        },
        {
            "itemid":"12346",
            "clock":1478609647,
            "ns":330690279,
            "state":1,
            "value":"Cannot find information for this network interface in /proc/net/dev.",
            "id": 2
        }
    ],
    "discovery data":[
        {
            "clock":1478608764,
            "drule":2,
            "dcheck":3,
            "type":12,
            "ip":"10.3.0.10",
            "dns":"vdebian",
            "status":1
        },
        {
            "clock":1478608764,
            "drule":2,
            "dcheck":null,
            "type":-1,
            "ip":"10.3.0.10",
            "dns":"vdebian",
            "status":1
        }
    ],
    "auto registration":[
        {
            "clock":1478608371,
            "host":"Logger1",
            "ip":"10.3.0.1",
            "dns":"localhost",
            "port":"10050"
        },
        {
            "clock":1478608381,
            "host":"Logger2",
            "ip":"10.3.0.2",
            "dns":"localhost",
            "port":"10050"
        }
    ],
    "tasks":[
        {
            "type": 2,
            "clock":1478608371,
            "ttl": 600,
            "commandtype": 2,
            "command": "restart_service1.sh",
            "execute_on": 2,
            "port": 80,
            "authtype": 0,
            "username": "userA",
            "password": "password1",
            "publickey": "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCqGKukO1De7zhZj6+H0qtjTkVxwTCpvKe",
            "privatekey": "lsuusFncCzWBQ7RKNUSesmQRMSGkVb1/3j+skZ6UtW+5u09lHNsj6tQ5QCqGKukO1De7zhd",
            "parent_taskid": 10,
            "hostid": 10070
        },
        {
            "type": 2,
            "clock":1478608381,
            "ttl": 600,
            "commandtype": 1,
            "command": "restart_service2.sh",
            "execute_on": 0,
            "authtype": 0,
            "username": "",
            "password": "",
            "publickey": "",
            "privatekey": "",
            "parent_taskid": 20,
            "hostid": 10084
        }
    ],
    "tasks":[
        {
            "type": 0,
            "status": 0,
            "parent_taskid": 10
        },
        {
            "type": 0,
            "status": 1,
            "error": "No permissions to execute task.",
            "parent_taskid": 20
        }
    ], 
    "version":"5.4.0"
}
```

server→proxy:

``` {.javascript}
{
  "response": "success",
  "tasks":[
      {
         "type": 1,
         "clock": 1478608371,
         "ttl": 600,
         "commandtype": 2,
         "command": "restart_service1.sh",
         "execute_on": 2,
         "port": 80,
         "authtype": 0,
         "username": "userA",
         "password": "password1",
         "publickey": "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCqGKukO1De7zhZj6+H0qtjTkVxwTCpvKe",
         "privatekey": "lsuusFncCzWBQ7RKNUSesmQRMSGkVb1/3j+skZ6UtW+5u09lHNsj6tQ5QCqGKukO1De7zhd",
         "parent_taskid": 10,
         "hostid": 10070
      },
      {
         "type": 1,
         "clock": 1478608381,
         "ttl": 600,
         "commandtype": 1,
         "command": "restart_service2.sh",
         "execute_on": 0,
         "authtype": 0,
         "username": "",
         "password": "",
         "publickey": "",
         "privatekey": "",
         "parent_taskid": 20,
         "hostid": 10084
      }
  ]
}
```

[comment]: # ({/new-ef0ba621})
