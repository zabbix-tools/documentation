[comment]: # translation:outdated

[comment]: # ({new-df0deee4})
# 3 Zabbix sender protocol

Please refer to the [trapper item](/manual/appendix/items/trapper) page
for more information.

[comment]: # ({/new-df0deee4})
