[comment]: # attributes: notoc

[comment]: # translation:outdated

[comment]: # ({new-ee60e21e})
# 1 Aggregate functions

Except where stated otherwise, all functions listed here are supported
in:

-   [Trigger expressions](/manual/config/triggers/expression)
-   [Calculated items](/manual/config/items/itemtypes/calculated)

Aggregate functions can work with either:

-   history of items, for example, `min(/host/key,1h)`
-   [foreach functions](/manual/appendix/functions/aggregate/foreach) as
    the only parameter, for example, `min(last_foreach(/*/key))`

Some general notes on function parameters:

-   Function parameters are separated by a comma
-   Optional function parameters (or parameter parts) are indicated by
    `<` `>`
-   Function-specific parameters are described with each function
-   `/host/key` and `(sec|#num)<:time shift>` parameters must never be
    quoted

[comment]: # ({/new-ee60e21e})

[comment]: # ({new-4d343eb8})
##### Common parameters

-   `/host/key` is a common mandatory first parameter for the functions
    referencing the host item history
-   `(sec|#num)<:time shift>` is a common second parameter for the
    functions referencing the host item history, where:
    -   **sec** - maximum [evaluation
        period](/manual/config/triggers#evaluation_period) in seconds
        (time [suffixes](/manual/appendix/suffixes) can be used), or
    -   **\#num** - maximum [evaluation
        range](/manual/config/triggers#evaluation_period) in latest
        collected values (if preceded by a hash mark)
    -   **time shift** (optional) allows to move the evaluation point
        back in time. See [more
        details](/manual/config/triggers/expression#time_shift) on
        specifying time shift.

[comment]: # ({/new-4d343eb8})

[comment]: # ({new-fe53c04c})
### Function details

Some general notes on function parameters:

-   Function parameters are separated by a comma
-   Optional function parameters (or parameter parts) are indicated by
    `<` `>`
-   Function-specific parameters are described with each function
-   `/host/key` and `(sec|#num)<:time shift>` parameters must never be
    quoted

[comment]: # ({/new-fe53c04c})

[comment]: # ({new-f5993b4c})

##### avg(/host/key,(sec|#num)<:time shift>) {#avg}

The average value of an item within the defined evaluation period.<br>
Supported value types: *Float*, *Integer*.

Parameters: see [common parameters](#common-parameters).

Time shift is useful when there is a need to compare the current average value with the average value some time ago.

Examples:

    avg(/host/key,1h) #the average value for the last hour until now
    avg(/host/key,1h:now-1d) #the average value for an hour from 25 hours ago to 24 hours ago from now
    avg(/host/key,#5) #the average value of the five latest values
    avg(/host/key,#5:now-1d) #the average value of the five latest values excluding the values received in the last 24 hours

[comment]: # ({/new-f5993b4c})

[comment]: # ({new-af24d13f})

##### bucket_percentile(item filter,time period,percentage) {#bucket-percentile}

Calculates the percentile from the buckets of a histogram.<br>

Parameters: 

-   **item filter** - see [item filter](/manual/appendix/functions/aggregate/foreach#item_filter);<br>
-   **time period** - see [time period](/manual/appendix/functions/aggregate/foreach#time_period);<br>
-   **percentage** - percentage (0-100).

Comments:

-   Supported only in calculated items;
-   This function is an alias for `histogram_quantile(percentage/100, bucket_rate_foreach(item filter, time period, 1))`.

[comment]: # ({/new-af24d13f})

[comment]: # ({new-3c498650})

##### count(func_foreach(item filter,<time period>)) {#count}

The count of values in an array returned by a foreach function.<br>
Supported value type: *Integer*.

Parameters: 

-   **func_foreach** - foreach function for which the number of returned values should be counted (with supported arguments). See [foreach functions](/manual/appendix/functions/aggregate/foreach) for details.
-   **item filter** - see [item filter](/manual/appendix/functions/aggregate/foreach#item_filter);<br>
-   **time period** - see [time period](/manual/appendix/functions/aggregate/foreach#time_period).

Using **count()** with a history-related foreach function (max_foreach, avg_foreach, etc.) may lead to performance implications, whereas using **exists_foreach()**, which works only with configuration data, will not have such effect.

Examples:

    count(max_foreach(/*/net.if.in[*],1h)) #the number of net.if.in items that received data in the last hour until now


[comment]: # ({/new-3c498650})

[comment]: # ({new-f3ad7158})

##### histogram_quantile(quantile,bucket1,value1,bucket2,value2,...) {#histogram-quantile}

Calculates the φ-quantile from the buckets of a histogram.<br>

Parameters: 

-   **quantile** - 0 ≤ φ ≤ 1;<br>
-   **bucketN, valueN** - manually entered pairs (>=2) of parameters or the response of [bucket_rate_foreach](/manual/appendix/functions/aggregate/foreach).

Comments:

-   Supported only in calculated items;
-   Functionally corresponds to '[histogram\_quantile](https://prometheus.io/docs/prometheus/latest/querying/functions/#histogram_quantile)' of PromQL;
-   Returns -1 if values of the last 'Infinity' bucket (*"+inf"*) are equal to 0.

Examples:

    histogram_quantile(0.75,1.0,last(/host/rate_bucket[1.0]),"+Inf",last(/host/rate_bucket[Inf]))
    histogram_quantile(0.5,bucket_rate_foreach(//item_key,30s))

[comment]: # ({/new-f3ad7158})

[comment]: # ({new-c8e3133a})

##### item.count(item filter) {#item.count}

The count of existing items in configuration that match the filter criteria.<br>
Supported value type: *Integer*.

Parameter: 

-   **item filter** - criteria for item selection, allows referencing by host group, host, item key, and tags. Wildcards are supported. See [item filter](/manual/appendix/functions/aggregate/foreach#item_filter) for more details.<br>

Comments:

-   Supported only in calculated items;
-   Works as an alias for the *count(exists_foreach(item_filter))* function.

Examples:

    item_count(/*/agent.ping?[group="Host group 1"]) #the number of hosts with the *agent.ping* item in the "Host group 1"

[comment]: # ({/new-c8e3133a})

[comment]: # ({new-88a54099})

##### kurtosis(/host/key,(sec|#num)<:time shift>) {#kurtosis}

The "tailedness" of the probability distribution in collected values within the defined evaluation period. See also: [Kurtosis](https://en.wikipedia.org/wiki/Kurtosis).<br>
Supported value types: *Float*, *Integer*.

Parameters: see [common parameters](#common-parameters).

Example:

    kurtosis(/host/key,1h) #kurtosis for the last hour until now

[comment]: # ({/new-88a54099})

[comment]: # ({new-bfb43a27})

##### mad(/host/key,(sec|#num)<:time shift>) {#mad}

The median absolute deviation in collected values within the defined evaluation period. See also: [Median absolute deviation](https://en.wikipedia.org/wiki/Median_absolute_deviation).<br>
Supported value types: *Float*, *Integer*.

Parameters: see [common parameters](#common-parameters).

Example:

    mad(/host/key,1h) #median absolute deviation for the last hour until now

[comment]: # ({/new-bfb43a27})

[comment]: # ({new-5882010b})

##### max(/host/key,(sec|#num)<:time shift>) {#max}

The highest value of an item within the defined evaluation period.<br>
Supported value types: *Float*, *Integer*.

Parameters: see [common parameters](#common-parameters).

Example:

    max(/host/key,1h) - min(/host/key,1h) #calculate the difference between the maximum and minimum values within the last hour until now (the delta of values)

[comment]: # ({/new-5882010b})

[comment]: # ({new-ffd1e499})

##### min(/host/key,(sec|#num)<:time shift>) {#min}

The lowest value of an item within the defined evaluation period.<br>
Supported value types: *Float*, *Integer*.

Parameters: see [common parameters](#common-parameters).

Example:

    max(/host/key,1h) - min(/host/key,1h) #calculate the difference between the maximum and minimum values within the last hour until now (the delta of values)

[comment]: # ({/new-ffd1e499})

[comment]: # ({new-6b6fb273})

##### skewness(/host/key,(sec|#num)<:time shift>) {#skewness}

The asymmetry of the probability distribution in collected values within the defined evaluation period. See also: [Skewness](https://en.wikipedia.org/wiki/Skewness).<br>
Supported value types: *Float*, *Integer*.

Parameters: see [common parameters](#common-parameters).

Example:

    skewness(/host/key,1h) #the skewness for the last hour until now

[comment]: # ({/new-6b6fb273})

[comment]: # ({new-11c2ea25})

##### stddevpop(/host/key,(sec|#num)<:time shift>) {#stddevpop}

The population standard deviation in collected values within the defined evaluation period. See also: [Standard deviation](https://en.wikipedia.org/wiki/Standard_deviation).<br>
Supported value types: *Float*, *Integer*.

Parameters: see [common parameters](#common-parameters).

Example:

    stddevpop(/host/key,1h) #the population standard deviation for the last hour until now

[comment]: # ({/new-11c2ea25})

[comment]: # ({new-1db77c44})

##### stddevsamp(/host/key,(sec|#num)<:time shift>) {#stddevsamp}

The sample standard deviation in collected values within the defined evaluation period. See also: [Standard deviation](https://en.wikipedia.org/wiki/Standard_deviation).<br>
Supported value types: *Float*, *Integer*.

Parameters: see [common parameters](#common-parameters).

At least two data values are required for this function to work.

Example:

    stddevsamp(/host/key,1h) #the sample standard deviation for the last hour until now

[comment]: # ({/new-1db77c44})

[comment]: # ({new-0ecf79eb})

##### sum(/host/key,(sec|#num)<:time shift>) {#sum}

The sum of collected values within the defined evaluation period.<br>
Supported value types: *Float*, *Integer*.

Parameters: see [common parameters](#common-parameters).

Example:

    sum(/host/key,1h) #the sum of values for the last hour until [now

[comment]: # ({/new-0ecf79eb})

[comment]: # ({new-6f6556f2})

##### sumofsquares(/host/key,(sec|#num)<:time shift>) {#sumofsquares}

The sum of squares in collected values within the defined evaluation period.<br>
Supported value types: *Float*, *Integer*.

Parameters: see [common parameters](#common-parameters).

Example:

    sumofsquares(/host/key,1h) #the sum of squares for the last hour until [now

[comment]: # ({/new-6f6556f2})

[comment]: # ({new-d299bd33})

##### varpop(/host/key,(sec|#num)<:time shift>) {#varpop}

The population variance of collected values within the defined evaluation period. See also: [Variance](https://en.wikipedia.org/wiki/Variance).<br>
Supported value types: *Float*, *Integer*.

Parameters: see [common parameters](#common-parameters).

Example:

    varpop(/host/key,1h) #the population variance for the last hour until now

[comment]: # ({/new-d299bd33})

[comment]: # ({new-b1bc041c})

##### varsamp(/host/key,(sec|#num)<:time shift>) {#varsamp}

The sample variance of collected values within the defined evaluation period. See also: [Variance](https://en.wikipedia.org/wiki/Variance).<br>
Supported value types: *Float*, *Integer*.

Parameters: see [common parameters](#common-parameters).

At least two data values are required for this function to work.

Example:

    varsamp(/host/key,1h) #the sample variance for the last hour until now

[comment]: # ({/new-b1bc041c})

[comment]: # ({new-541616fc})

See [all supported functions](/manual/appendix/functions).

[comment]: # ({/new-541616fc})

