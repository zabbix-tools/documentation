[comment]: # attributes: notoc

[comment]: # translation:outdated

[comment]: # ({new-ca911365})
# 2 Bitwise functions

All functions listed here are supported in:

-   [Trigger expressions](/manual/config/triggers/expression)
-   [Calculated items](/manual/config/items/itemtypes/calculated)

Some general notes on function parameters:

-   Function parameters are separated by a comma
-   Expressions are accepted as parameters
-   Optional function parameters (or parameter parts) are indicated by
    `<` `>`

|FUNCTION|<|<|<|
|--------|-|-|-|
|<|**Description**|**Function-specific parameters**|**Comments**|
|**bitand** (value,mask)|<|<|<|
|<|Value of "bitwise AND" of an item value and mask.|**value** - value to check<br>**mask** (mandatory) - 64-bit unsigned integer (0 - 18446744073709551615)|Supported value types: int<br><br>Although the comparison is done in a bitwise manner, all the values must be supplied and are returned in decimal. For example, checking for the 3rd bit is done by comparing to 4, not 100.<br><br>Examples:<br>=> **bitand**(last(/host/key),**12**)=8 or **bitand**(last(/host/key),**12**)=4 → 3rd or 4th bit set, but not both at the same time<br>=> **bitand**(last(/host/key),**20**)=16 → 3rd bit not set and 5th bit set.|
|**bitlshift** (value,bits to shift)|<|<|<|
|<|Bitwise shift left of an item value.|**value** - value to check<br>**bits to shift** (mandatory) - number of bits to shift|Supported value types: int<br><br>Although the comparison is done in a bitwise manner, all the values must be supplied and are returned in decimal. For example, checking for the 3rd bit is done by comparing to 4, not 100.|
|**bitnot** (value)|<|<|<|
|<|Value of "bitwise NOT" of an item value.|**value** - value to check<br>|Supported value types: int<br><br>Although the comparison is done in a bitwise manner, all the values must be supplied and are returned in decimal. For example, checking for the 3rd bit is done by comparing to 4, not 100.|
|**bitor** (value,mask)|<|<|<|
|<|Value of "bitwise OR" of an item value and mask.|**value** - value to check<br>**mask** (mandatory) - 64-bit unsigned integer (0 - 18446744073709551615)|Supported value types: int<br><br>Although the comparison is done in a bitwise manner, all the values must be supplied and are returned in decimal. For example, checking for the 3rd bit is done by comparing to 4, not 100.|
|**bitrshift** (value,bits to shift)|<|<|<|
|<|Bitwise shift right of an item value.|**value** - value to check<br>**bits to shift** (mandatory) - number of bits to shift|Supported value types: int<br><br>Although the comparison is done in a bitwise manner, all the values must be supplied and are returned in decimal. For example, checking for the 3rd bit is done by comparing to 4, not 100.|
|**bitxor** (value,mask)|<|<|<|
|<|Value of "bitwise exclusive OR" of an item value and mask.|**value** - value to check<br>**mask** (mandatory) - 64-bit unsigned integer (0 - 18446744073709551615)|Supported value types: int<br><br>Although the comparison is done in a bitwise manner, all the values must be supplied and are returned in decimal. For example, checking for the 3rd bit is done by comparing to 4, not 100.|

[comment]: # ({/new-ca911365})

[comment]: # ({new-6803853f})
### Function details

Some general notes on function parameters:

-   Function parameters are separated by a comma
-   Expressions are accepted as parameters
-   Optional function parameters (or parameter parts) are indicated by
    `<` `>`

[comment]: # ({/new-6803853f})

[comment]: # ({new-7ffd1e30})

##### bitand(value,mask) {#bitand}

The value of "bitwise AND" of an item value and mask.<br>
Supported value types: *Integer*.

Parameter: 

-   **value** - the value to check;
-   **mask** (mandatory) - a 64-bit unsigned integer (0 - 18446744073709551615).

Although the comparison is done in a bitwise manner, all the values must be supplied and are returned in decimal. For example, checking for the 3rd bit is done by comparing to 4, not 100.

Examples:

    bitand(last(/host/key),12)=8 or bitand(last(/host/key),12)=4 #3rd or 4th bit set, but not both at the same time
    bitand(last(/host/key),20)=16 #3rd bit not set and 5th bit set

[comment]: # ({/new-7ffd1e30})

[comment]: # ({new-31f09935})

##### bitlshift(value,bits to shift) {#bitlshift}

The bitwise shift left of an item value.<br>
Supported value types: *Integer*.

Parameter: 

-   **value** - the value to check;
-   **bits to shift** (mandatory) - the number of bits to shift.

Although the comparison is done in a bitwise manner, all the values must be supplied and are returned in decimal. For example, checking for the 3rd bit is done by comparing to 4, not 100.

[comment]: # ({/new-31f09935})

[comment]: # ({new-d3633e68})

##### bitnot(value) {#bitnot}

The value of "bitwise NOT" of an item value.<br>
Supported value types: *Integer*.

Parameter: 

-   **value** - the value to check.

Although the comparison is done in a bitwise manner, all the values must be supplied and are returned in decimal. For example, checking for the 3rd bit is done by comparing to 4, not 100.

[comment]: # ({/new-d3633e68})

[comment]: # ({new-781b9d08})

##### bitor(value,mask) {#bitor}

The value of "bitwise OR" of an item value and mask.<br>
Supported value types: *Integer*.

Parameter: 

-   **value** - the value to check;
-   **mask** (mandatory) - a 64-bit unsigned integer (0 - 18446744073709551615).

Although the comparison is done in a bitwise manner, all the values must be supplied and are returned in decimal. For example, checking for the 3rd bit is done by comparing to 4, not 100.

[comment]: # ({/new-781b9d08})

[comment]: # ({new-ade72509})

##### bitrshift(value,bits to shift) {#bitrshift}

The bitwise shift right of an item value.<br>
Supported value types: *Integer*.

Parameter: 

-   **value** - the value to check;
-   **bits to shift** (mandatory) - the number of bits to shift.

Although the comparison is done in a bitwise manner, all the values must be supplied and are returned in decimal. For example, checking for the 3rd bit is done by comparing to 4, not 100.

[comment]: # ({/new-ade72509})

[comment]: # ({new-51a714f0})

##### bitxor(value,mask) {#bitxor}

The value of "bitwise exclusive OR" of an item value and mask.<br>
Supported value types: *Integer*.

Parameter: 

-   **value** - the value to check;
-   **mask** (mandatory) - a 64-bit unsigned integer (0 - 18446744073709551615).

Although the comparison is done in a bitwise manner, all the values must be supplied and are returned in decimal. For example, checking for the 3rd bit is done by comparing to 4, not 100.

[comment]: # ({/new-51a714f0})

[comment]: # ({new-e800d07b})

See [all supported functions](/manual/appendix/functions).

[comment]: # ({/new-e800d07b})
