[comment]: # translation:outdated

[comment]: # ({new-9884b1d2})
# 9 Zabbix agent on Microsoft Windows

[comment]: # ({/new-9884b1d2})

[comment]: # ({new-bc82b50c})
#### Configuring agent

Both generations of Zabbix agents run as a Windows service. For Zabbix
agent 2, replace *agentd* with *agent2* in the instructions below.

You can run a single instance of Zabbix agent or multiple instances of
the agent on a Microsoft Windows host. A single instance can use the
default configuration file `C:\zabbix_agentd.conf` or a configuration
file specified in the command line. In case of multiple instances each
agent instance must have its own configuration file (one of the
instances can use the default configuration file).

An example configuration file is available in Zabbix source archive as
`conf/zabbix_agentd.win.conf`.

See the [configuration file](/manual/appendix/config/zabbix_agentd_win)
options for details on configuring Zabbix Windows agent.

::: notewarning
 **Zabbix agent for Windows does not support
non-standard Windows configurations where CPUs are distributed
non-uniformly across NUMA nodes.** If logical CPUs are distributed
non-uniformly, then CPU performance metrics may not be available for
some CPUs. For example, if there are 72 logical CPUs with 2 NUMA nodes,
both nodes must have 36 CPUs each.
:::

[comment]: # ({/new-bc82b50c})

[comment]: # ({new-011f29f0})
##### Hostname parameter

To perform [active
checks](/manual/appendix/items/activepassive#active_checks) on a host
Zabbix agent needs to have the hostname defined. Moreover, the hostname
value set on the agent side should exactly match the "[Host
name](/manual/config/hosts/host)" configured for the host in the
frontend.

The hostname value on the agent side can be defined by either the
**Hostname** or **HostnameItem** parameter in the agent [configuration
file](/manual/appendix/config/zabbix_agentd_win) - or the default values
are used if any of these parameters are not specified.

The default value for **HostnameItem** parameter is the value returned
by the "system.hostname" agent key and for Windows platform it returns
the NetBIOS host name.

The default value for **Hostname** is the value returned by the
**HostnameItem** parameter. So, in effect, if both these parameters are
unspecified the actual hostname will be the host NetBIOS name; Zabbix
agent will use NetBIOS host name to retrieve the list of active checks
from Zabbix server and send results to it.

The default value for **Hostname** is the value returned by the
**HostnameItem** parameter. So, in effect, if both these parameters are
unspecified the actual hostname will be the host NetBIOS name; Zabbix
agent will use NetBIOS host name to retrieve the list of active checks
from Zabbix server and send results to it.

The "system.hostname" key supports two optional parameters - *type* and
*transform*.

*Type* parameter determines the type of the name the item should return.
Supported values:

-   *netbios* (default) - returns the NetBIOS host name which is limited
    to 15 symbols and is in the UPPERCASE only;
-   *host* - returns the full, real (case-sensitive) Windows host name;
-   *shorthost* (supported since Zabbix 5.4.7) - returns part of the
    hostname before the first dot. It will return a full string if the
    name does not contain a dot.

*Transform* parameter is supported since Zabbix 5.4.7 and allows to
specify additional transformation rule for the hostname. Supported
values:

-   *none* (default) - use the original letter case;
-   *lower* - convert the text into lowercase.

So, to simplify the configuration of zabbix\_agentd.conf file and make
it unified, two different approaches could be used.

1.  leave **Hostname** or **HostnameItem** parameters undefined and
    Zabbix agent will use NetBIOS host name as the hostname;
2.  leave **Hostname** parameter undefined and define **HostnameItem**
    like this:\
    **HostnameItem=system.hostname\[host\]** - for Zabbix agent to use
    the full, real (case sensitive) Windows host name as the hostname\
    **HostnameItem=system.hostname\[shorthost,lower\]** - for Zabbix
    agent to use only part of the hostname before the first dot,
    converted into lowercase.

Host name is also used as part of Windows service name which is used for
installing, starting, stopping and uninstalling the Windows service. For
example, if Zabbix agent configuration file specifies
`Hostname=Windows_db_server`, then the agent will be installed as a
Windows service "`Zabbix Agent [Windows_db_server]`". Therefore, to have
a different Windows service name for each Zabbix agent instance, each
instance must use a different host name.

[comment]: # ({/new-011f29f0})

[comment]: # ({new-1c178c2a})
#### Installing agent as Windows service

To install a single instance of Zabbix agent with the default
configuration file `c:\zabbix_agentd.conf`:

    zabbix_agentd.exe --install

::: noteimportant
On a 64-bit system, a 64-bit Zabbix agent version
is required for all checks related to running 64-bit processes to work
correctly.
:::

If you wish to use a configuration file other than
`c:\zabbix_agentd.conf`, you should use the following command for
service installation:

    zabbix_agentd.exe --config <your_configuration_file> --install

A full path to the configuration file should be specified.

Multiple instances of Zabbix agent can be installed as services like
this:

      zabbix_agentd.exe --config <configuration_file_for_instance_1> --install --multiple-agents
      zabbix_agentd.exe --config <configuration_file_for_instance_2> --install --multiple-agents
      ...
      zabbix_agentd.exe --config <configuration_file_for_instance_N> --install --multiple-agents

The installed service should now be visible in Control Panel.

[comment]: # ({/new-1c178c2a})

[comment]: # ({new-635edc59})
#### Starting agent

To start the agent service, you can use Control Panel or do it from
command line.

To start a single instance of Zabbix agent with the default
configuration file:

     zabbix_agentd.exe --start

To start a single instance of Zabbix agent with another configuration
file:

     zabbix_agentd.exe --config <your_configuration_file> --start

To start one of multiple instances of Zabbix agent:

     zabbix_agentd.exe --config <configuration_file_for_this_instance> --start --multiple-agents

[comment]: # ({/new-635edc59})

[comment]: # ({new-f3f0d3db})
#### Stopping agent

To stop the agent service, you can use Control Panel or do it from
command line.

To stop a single instance of Zabbix agent started with the default
configuration file:

     zabbix_agentd.exe --stop

To stop a single instance of Zabbix agent started with another
configuration file:

     zabbix_agentd.exe --config <your_configuration_file> --stop

To stop one of multiple instances of Zabbix agent:

     zabbix_agentd.exe --config <configuration_file_for_this_instance> --stop --multiple-agents

[comment]: # ({/new-f3f0d3db})

[comment]: # ({new-db2e6708})
#### Uninstalling agent Windows service

To uninstall a single instance of Zabbix agent using the default
configuration file:

       zabbix_agentd.exe --uninstall

To uninstall a single instance of Zabbix agent using a non-default
configuration file:

       zabbix_agentd.exe --config <your_configuration_file> --uninstall

To uninstall multiple instances of Zabbix agent from Windows services:

      zabbix_agentd.exe --config <configuration_file_for_instance_1> --uninstall --multiple-agents
      zabbix_agentd.exe --config <configuration_file_for_instance_2> --uninstall --multiple-agents
      ...
      zabbix_agentd.exe --config <configuration_file_for_instance_N> --uninstall --multiple-agents

[comment]: # ({/new-db2e6708})

[comment]: # ({new-b7b226d1})

#### Limitations

Zabbix agent for Windows does not support
non-standard Windows configurations where CPUs are distributed
non-uniformly across NUMA nodes. If logical CPUs are distributed
non-uniformly, then CPU performance metrics may not be available for
some CPUs. For example, if there are 72 logical CPUs with 2 NUMA nodes,
both nodes must have 36 CPUs each.

[comment]: # ({/new-b7b226d1})
