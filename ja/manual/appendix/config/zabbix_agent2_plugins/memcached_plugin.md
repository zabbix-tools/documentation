[comment]: # translation:outdated

[comment]: # ({new-88671f5d})
# 3 Memcached plugin

[comment]: # ({/new-88671f5d})

[comment]: # ({new-2abeb49a})
#### Overview

This section lists parameters supported in the Memcached Zabbix agent 2
plugin configuration file (memcached.conf). Note that:

-   The default values reflect process defaults, not the values in the
    shipped configuration files;
-   Zabbix supports configuration files only in UTF-8 encoding without
    [BOM](https://en.wikipedia.org/wiki/Byte_order_mark);
-   Comments starting with "\#" are only supported at the beginning of
    the line.

[comment]: # ({/new-2abeb49a})

[comment]: # ({new-63c43789})
#### Parameters

|Parameter|Mandatory|Range|Default|Description|
|---------|---------|-----|-------|-----------|
|Plugins.Memcached.KeepAlive|no|60-900|300|The maximum time of waiting (in seconds) before unused plugin connections are closed.|
|Plugins.Memcached.Sessions.<SessionName>.Password|no|<|<|Named session password.<br>**<SessionName>** - name of a session for using in item keys.|
|Plugins.Memcached.Sessions.<SessionName>.Uri|no|<|tcp://localhost:11211|Connection string of a named session.<br>**<SessionName>** - name of a session for using in item keys.<br><br>Should not include embedded credentials (they will be ignored).<br>Must match the URI format.<br>Supported schemes: `tcp`, `unix`; a scheme can be omitted (since version 5.2.3).<br>A port can be omitted (default=11211).<br>Examples: `tcp://localhost:11211`<br>`localhost`<br>`unix:/var/run/memcached.sock`|
|Plugins.Memcached.Sessions.<SessionName>.User|no|<|<|Named session username.<br>**<SessionName>** - name of a session for using in item keys.|
|Plugins.Memcached.Timeout|no|1-30|global timeout|Request execution timeout (how long to wait for a request to complete before shutting it down).|

See also:

-   Description of general Zabbix agent 2 configuration parameters:
    [Zabbix agent 2 (UNIX)](/manual/appendix/config/zabbix_agent2) /
    [Zabbix agent 2
    (Windows)](/manual/appendix/config/zabbix_agent2_win)
-   Instructions for configuring [plugins](/manual/config/items/plugins)

[comment]: # ({/new-63c43789})
