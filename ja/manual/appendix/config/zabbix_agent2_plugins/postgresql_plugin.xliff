<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="ja" datatype="plaintext" original="manual/appendix/config/zabbix_agent2_plugins/postgresql_plugin.md">
    <body>
      <trans-unit id="heading" xml:space="preserve">
        <source># 9 PostgreSQL plugin</source>
      </trans-unit>
      <trans-unit id="overview" xml:space="preserve">
        <source>### Overview

This section lists parameters supported in the PostgreSQL Zabbix agent 2 plugin configuration file (postgresql.conf).
 
This is a loadable plugin, which is available and fully described in the
[PostgreSQL plugin repository](https://git.zabbix.com/projects/AP/repos/postgresql/browse)

Note that:

-   The default values reflect process defaults, not the values in the
    shipped configuration files;
-   Zabbix supports configuration files only in UTF-8 encoding without
    [BOM](https://en.wikipedia.org/wiki/Byte_order_mark);
-   Comments starting with "\#" are only supported at the beginning of
    the line.</source>
      </trans-unit>
      <trans-unit id="options" xml:space="preserve">
        <source>### Options

|Parameter|Description|
|---------|-----------|
|-V --version|Print the plugin version and license information.|
|-h --help|Print help information (shorthand).|</source>
      </trans-unit>
      <trans-unit id="parameters" xml:space="preserve">
        <source>### Parameters

|Parameter|Mandatory|Range|Default|Description|
|--|--|--|--|----------|
|Plugins.PostgreSQL.CallTimeout|no|1-30|global timeout|Maximum wait time (in seconds) for a request to be completed.|
|Plugins.PostgreSQL.CustomQueriesPath|no| |disabled|Full pathname of the directory containing *.sql* files with custom queries.|
|Plugins.PostgreSQL.Default.Database|no| | |Default database for connecting to PostgreSQL; used if no value is specified in an item key or named session.|
|Plugins.PostgreSQL.Default.Password|no| | |Default password for connecting to PostgreSQL; used if no value is specified in an item key or named session.|
|Plugins.PostgreSQL.Default.TLSCAFile|no&lt;br&gt;(yes, if Plugins.PostgreSQL.Default.TLSConnect is set to one of: verify_ca, verify_full)| | |Full pathname of a file containing the top-level CA(s) certificate for peer certificate verification for encrypted communications between Zabbix agent 2 and monitored databases; used if no value is specified in a named session.|
|Plugins.PostgreSQL.Default.TLSCertFile|no&lt;br&gt;(yes, if Plugins.PostgreSQL.Default.TLSConnect is set to one of: verify_ca, verify_full)| | |Full pathname of a file containing the PostgreSQL certificate or certificate chain for encrypted communications between Zabbix agent 2 and monitored databases; used if no value is specified in a named session.|
|Plugins.PostgreSQL.Default.TLSConnect|no| | |Encryption type for communications between Zabbix agent 2 and monitored databases; used if no value is specified in a named session.&lt;br&gt;Supported values:&lt;br&gt;*required* - connect using TLS as transport mode without identity checks;&lt;br&gt;*verify\_ca* - connect using TLS and verify certificate;&lt;br&gt;*verify\_full* - connect using TLS, verify certificate and verify that database identity (CN) specified by DBHost matches its certificate.&lt;br&gt;Undefined encryption type means unencrypted connection.|
|Plugins.PostgreSQL.Default.TLSKeyFile|no&lt;br&gt;(yes, if Plugins.PostgreSQL.Default.TLSConnect is set to one of: verify_ca, verify_full)| | |Full pathname of a file containing the PostgreSQL private key for encrypted communications between Zabbix agent 2 and monitored databases; used if no value is specified in a named session.|
|Plugins.PostgreSQL.Default.Uri|no| | |Default URI for connecting to PostgreSQL; used if no value is specified in an item key or named session.&lt;br&gt;&lt;br&gt;Should not include embedded credentials (they will be ignored).&lt;br&gt;Must match the URI format.&lt;br&gt;Supported schemes: `tcp`, `unix`.&lt;br&gt;Examples: `tcp://127.0.0.1:5432`&lt;br&gt;`tcp://localhost`&lt;br&gt;`unix:/var/run/postgresql/.s.PGSQL.5432`|
|Plugins.PostgreSQL.Default.User|no| | |Default username for connecting to PostgreSQL; used if no value is specified in an item key or named session.|
|Plugins.PostgreSQL.KeepAlive|no|60-900|300|Maximum time of waiting (in seconds) before unused plugin connections are closed.|
|Plugins.PostgreSQL.Sessions.&lt;SessionName&gt;.Database|no| | |Database for session connection.&lt;br&gt;**&lt;SessionName&gt;** - define name of a session for using in item keys.|
|Plugins.PostgreSQL.Sessions.&lt;SessionName&gt;.Password|no|Must match the password format.| |Password for session connection.&lt;br&gt;**&lt;SessionName&gt;** - define name of a session for using in item keys.|
|Plugins.PostgreSQL.Sessions.&lt;SessionName&gt;.TLSCAFile|no&lt;br&gt;(yes, if Plugins.PostgreSQL.Sessions.&lt;SessionName&gt;.TLSConnect is set to one of: verify_ca, verify_full)| | |Full pathname of a file containing the top-level CA(s) certificate peer certificate verification.&lt;br&gt;**&lt;SessionName&gt;** - define name of a session for using in item keys.|
|Plugins.PostgreSQL.Sessions.&lt;SessionName&gt;.TLSCertFile|no&lt;br&gt;(yes, if Plugins.PostgreSQL.Sessions.&lt;SessionName&gt;.TLSConnect is set to one of: verify_ca, verify_full)| | |Full pathname of a file containing the PostgreSQL certificate or certificate chain.&lt;br&gt;**&lt;SessionName&gt;** - define name of a session for using in item keys.|
|Plugins.PostgreSQL.Sessions.&lt;SessionName&gt;.TLSConnect|no| | |Encryption type for PostgreSQL connection.&lt;br&gt;**&lt;SessionName&gt;** - define name of a session for using in item keys.&lt;br&gt;&lt;br&gt;Supported values:&lt;br&gt;*required* - connect using TLS as transport mode without identity checks;&lt;br&gt;*verify\_ca* - connect using TLS and verify certificate;&lt;br&gt;*verify\_full* - connect using TLS, verify certificate and verify that database identity (CN) specified by DBHost matches its certificate.&lt;br&gt;Undefined encryption type means unencrypted connection.|
|Plugins.PostgreSQL.Sessions.&lt;SessionName&gt;.TLSKeyFile|no&lt;br&gt;(yes, if Plugins.PostgreSQL.Sessions.&lt;SessionName&gt;.TLSConnect is set to one of: verify_ca, verify_full)| | |Full pathname of a file containing the PostgreSQL private key.&lt;br&gt;**&lt;SessionName&gt;** - define name of a session for using in item keys.|
|Plugins.PostgreSQL.Sessions.&lt;SessionName&gt;.Uri|no| | |Connection string of a named session.&lt;br&gt;**&lt;SessionName&gt;** - define name of a session for using in item keys.&lt;br&gt;&lt;br&gt;Should not include embedded credentials (they will be ignored).&lt;br&gt;Must match the URI format.&lt;br&gt;Supported schemes: `tcp`, `unix`.&lt;br&gt;Examples: `tcp://127.0.0.1:5432`&lt;br&gt;`tcp://localhost`&lt;br&gt;`unix:/var/run/postgresql/.s.PGSQL.5432`|
|Plugins.PostgreSQL.Sessions.&lt;SessionName&gt;.User|no| | |Named session username.&lt;br&gt;**&lt;SessionName&gt;** - define name of a session for using in item keys.|
|Plugins.PostgreSQL.System.Path|yes| | |Path to external plugin executable.|
|Plugins.PostgreSQL.Timeout|no|1-30|global timeout|Request execution timeout (how long to wait for a request to complete before shutting it down).|

See also:

-   Description of general Zabbix agent 2 configuration parameters:
    [Zabbix agent 2 (UNIX)](/manual/appendix/config/zabbix_agent2) /
    [Zabbix agent 2
    (Windows)](/manual/appendix/config/zabbix_agent2_win)
-   Instructions for configuring [plugins](/manual/extensions/plugins)</source>
      </trans-unit>
    </body>
  </file>
</xliff>
