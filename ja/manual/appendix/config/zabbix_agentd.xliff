<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="ja" datatype="plaintext" original="manual/appendix/config/zabbix_agentd.md">
    <body>
      <trans-unit id="c52b2df3" xml:space="preserve">
        <source># 3 Zabbix agent (UNIX)</source>
      </trans-unit>
      <trans-unit id="2773fc24" xml:space="preserve">
        <source>### Overview

The parameters supported by the Zabbix agent configuration file (zabbix\_agentd.conf) are listed in this section. 

The parameters are listed without additional information. Click on the parameter to see the full details.

|Parameter|Description|
|--|--------|
|[Alias](#alias)|Sets an alias for an item key.|
|[AllowKey](#allowkey)|Allow the execution of those item keys that match a pattern.|
|[AllowRoot](#allowroot)|Allow the agent to run as 'root'.|
|[BufferSend](#buffersend)|Do not keep data longer than N seconds in buffer.|
|[BufferSize](#buffersize)|The maximum number of values in the memory buffer.|
|[DebugLevel](#debuglevel)|The debug level.|
|[DenyKey](#denykey)|Deny the execution of those item keys that match a pattern.|
|[EnableRemoteCommands](#enableremotecommands)|Whether remote commands from Zabbix server are allowed.|
|[HeartbeatFrequency](#heartbeatfrequency)|The frequency of heartbeat messages in seconds.|
|[HostInterface](#hostinterface)|An optional parameter that defines the host interface.|
|[HostInterfaceItem](#hostinterfaceitem)|An optional parameter that defines an item used for getting the host interface.|
|[HostMetadata](#hostmetadata)|An optional parameter that defines the host metadata.|
|[HostMetadataItem](#hostmetadataitem)|An optional parameter that defines a Zabbix agent item used for getting the host metadata.|
|[Hostname](#hostname)|An optional parameter that defines the hostname.|
|[HostnameItem](#hostnameitem)|An optional parameter that defines a Zabbix agent item used for getting the hostname.|
|[Include](#include)|You may include individual files or all files in a directory in the configuration file.|
|[ListenBacklog](#listenbacklog)|The maximum number of pending connections in the TCP queue.|
|[ListenIP](#listenip)|A list of comma-delimited IP addresses that the agent should listen on.|
|[ListenPort](#listenport)|The agent will listen on this port for connections from the server.|
|[LoadModule](#loadmodule)|The module to load at agent startup.|
|[LoadModulePath](#loadmodulepath)|The full path to the location of agent modules.|
|[LogFile](#logfile)|The name of the log file.|
|[LogFileSize](#logfilesize)|The maximum size of the log file.|
|[LogRemoteCommands](#logremotecommands)|Enable logging of executed shell commands as warnings.|
|[LogType](#logtype)|The type of the log output.|
|[MaxLinesPerSecond](#maxlinespersecond)|The maximum number of new lines the agent will send per second to Zabbix server or proxy when processing 'log' and 'logrt' active checks.|
|[PidFile](#pidfile)|The name of the PID file.|
|[RefreshActiveChecks](#refreshactivechecks)|How often the list of active checks is refreshed.|
|[Server](#server)|A list of comma-delimited IP addresses, optionally in CIDR notation, or DNS names of Zabbix servers and Zabbix proxies.|
|[ServerActive](#serveractive)|The Zabbix server/proxy address or cluster configuration to get active checks from.|
|[SourceIP](#sourceip)|The source IP address.|
|[StartAgents](#startagents)|The number of pre-forked instances of zabbix\_agentd that process passive checks.|
|[Timeout](#timeout)|Spend no more than Timeout seconds on processing.|
|[TLSAccept](#tlsaccept)|What incoming connections to accept.|
|[TLSCAFile](#tlscafile)|The full pathname of a file containing the top-level CA(s) certificates for peer certificate verification, used for encrypted communications between Zabbix components.|
|[TLSCertFile](#tlscertfile)|The full pathname of a file containing the agent certificate or certificate chain, used for encrypted communications between Zabbix components.|
|[TLSCipherAll](#tlscipherall)|The GnuTLS priority string or OpenSSL (TLS 1.2) cipher string. Override the default ciphersuite selection criteria for certificate- and PSK-based encryption.|
|[TLSCipherAll13](#tlscipherall13)|The cipher string for OpenSSL 1.1.1 or newer in TLS 1.3. Override the default ciphersuite selection criteria for certificate- and PSK-based encryption.|
|[TLSCipherCert](#tlsciphercert)|The GnuTLS priority string or OpenSSL (TLS 1.2) cipher string. Override the default ciphersuite selection criteria for certificate-based encryption.|
|[TLSCipherCert13](#tlsciphercert13)|The cipher string for OpenSSL 1.1.1 or newer in TLS 1.3. Override the default ciphersuite selection criteria for certificate-based encryption.|
|[TLSCipherPSK](#tlscipherpsk)|The GnuTLS priority string or OpenSSL (TLS 1.2) cipher string. Override the default ciphersuite selection criteria for PSK-based encryption.|
|[TLSCipherPSK13](#tlscipherpsk13)|The cipher string for OpenSSL 1.1.1 or newer in TLS 1.3. Override the default ciphersuite selection criteria for PSK-based encryption.|
|[TLSConnect](#tlsconnect)|How the agent should connect to Zabbix server or proxy.|
|[TLSCRLFile](#tlscrlfile)|The full pathname of a file containing revoked certificates. This parameter is used for encrypted communications between Zabbix components.|
|[TLSKeyFile](#tlskeyfile)|The full pathname of a file containing the agent private key, used for encrypted communications between Zabbix components.|
|[TLSPSKFile](#tlspskfile)|The full pathname of a file containing the agent pre-shared key, used for encrypted communications with Zabbix server.|
|[TLSPSKIdentity](#tlspskidentity)|The pre-shared key identity string, used for encrypted communications with Zabbix server.|
|[TLSServerCertIssuer](#tlsservercertissuer)|The allowed server (proxy) certificate issuer.|
|[TLSServerCertSubject](#tlsservercertsubject)|The allowed server (proxy) certificate subject.|
|[UnsafeUserParameters](#unsafeuserparameters)|Allow all characters to be passed in arguments to user-defined parameters.|
|[User](#user)|Drop privileges to a specific, existing user on the system.|
|[UserParameter](#userparameter)|A user-defined parameter to monitor.|
|[UserParameterDir](#userparameterdir)|The default search path for UserParameter commands.|

All parameters are non-mandatory unless explicitly stated that the parameter is mandatory.

Note that:

-   The default values reflect daemon defaults, not the values in the
    shipped configuration files;
-   Zabbix supports configuration files only in UTF-8 encoding without
    [BOM](https://en.wikipedia.org/wiki/Byte_order_mark);
-   Comments starting with "\#" are only supported in the beginning of
    the line.</source>
      </trans-unit>
      <trans-unit id="bb271b3c" xml:space="preserve">
        <source>### Parameter details</source>
      </trans-unit>
      <trans-unit id="20d32ed1" xml:space="preserve">
        <source>##### Alias

Sets an alias for an item key. It can be used to substitute a long and complex item key with a shorter and simpler one. Multiple *Alias* parameters with the same *Alias* key may be present.&lt;br&gt;Different *Alias* keys may reference the same item key.&lt;br&gt;Aliases can be used in *HostMetadataItem* but not in *HostnameItem* parameters.

Example 1: Retrieving the ID of user 'zabbix'.

    Alias=zabbix.userid:vfs.file.regexp[/etc/passwd,"^zabbix:.:([0-9]+)",,,,\1]
    
Now the **zabbix.userid** shorthand key may be used to retrieve data.

Example 2: Getting CPU utilization with default and custom parameters.

    Alias=cpu.util:system.cpu.util
    Alias=cpu.util[*]:system.cpu.util[*]

This allows use the **cpu.util** key to get CPU utilization percentage with default parameters as well as use **cpu.util[all, idle, avg15]** to get specific data about CPU utilization.

Example 3: Running multiple [low-level discovery](/manual/discovery/low_level_discovery) rules processing the same discovery items.

    Alias=vfs.fs.discovery[*]:vfs.fs.discovery

Now it is possible to set up several discovery rules using **vfs.fs.discovery** with different parameters for each rule, e.g., **vfs.fs.discovery[foo]**, **vfs.fs.discovery[bar]**, etc.</source>
      </trans-unit>
      <trans-unit id="f52682cc" xml:space="preserve">
        <source>##### AllowKey

Allow the execution of those item keys that match a pattern. The key pattern is a wildcard expression that supports the "\*" character to match any number of any characters.&lt;br&gt;Multiple key matching rules may be defined in combination with DenyKey. The parameters are processed one by one according to their appearance order. See also: [Restricting agent checks](/manual/config/items/restrict_checks).</source>
      </trans-unit>
      <trans-unit id="a02d0dc0" xml:space="preserve">
        <source>##### AllowRoot

Allow the agent to run as 'root'. If disabled and the agent is started by 'root', the agent will try to switch to user 'zabbix' instead. Has no effect if started under a regular user.

Default: `0`&lt;br&gt;
Values: 0 - do not allow; 1 - allow</source>
      </trans-unit>
      <trans-unit id="12f60552" xml:space="preserve">
        <source>##### BufferSend

Do not keep data longer than N seconds in buffer.

Default: `5`&lt;br&gt;
Range: 1-3600</source>
      </trans-unit>
      <trans-unit id="397065ee" xml:space="preserve">
        <source>##### BufferSize

The maximum number of values in the memory buffer. The agent will send all collected data to the Zabbix server or proxy if the buffer is full.

Default: `100`&lt;br&gt;
Range: 2-65535</source>
      </trans-unit>
      <trans-unit id="e5288ea7" xml:space="preserve">
        <source>##### DebugLevel

Specify the debug level:&lt;br&gt;*0* - basic information about starting and stopping of Zabbix processes&lt;br&gt;*1* - critical information;&lt;br&gt;*2* - error information;&lt;br&gt;*3* - warnings;&lt;br&gt;*4* - for debugging (produces lots of information);&lt;br&gt;*5* - extended debugging (produces even more information).

Default: `3`&lt;br&gt;
Range: 0-5</source>
      </trans-unit>
      <trans-unit id="ef1d8b77" xml:space="preserve">
        <source>##### DenyKey

Deny the execution of those item keys that match a pattern. The key pattern is a wildcard expression that supports the "\*" character to match any number of any characters.&lt;br&gt;Multiple key matching rules may be defined in combination with AllowKey. The parameters are processed one by one according to their appearance order. See also: [Restricting agent checks](/manual/config/items/restrict_checks).</source>
      </trans-unit>
      <trans-unit id="50dcbfc8" xml:space="preserve">
        <source>##### EnableRemoteCommands

Whether remote commands from Zabbix server are allowed. This parameter is **deprecated**, use AllowKey=system.run\[\*\] or DenyKey=system.run\[\*\] instead.&lt;br&gt;It is an internal alias for AllowKey/DenyKey parameters depending on value:&lt;br&gt;0 - DenyKey=system.run\[\*\]&lt;br&gt;1 - AllowKey=system.run\[\*\]

Default: `0`&lt;br&gt;
Values: 0 - do not allow, 1 - allow</source>
      </trans-unit>
      <trans-unit id="764c3d3c" xml:space="preserve">
        <source>##### HeartbeatFrequency

The frequency of heartbeat messages in seconds. Used for monitoring the availability of active checks.&lt;br&gt;0 - heartbeat messages disabled.

Default: `60`&lt;br&gt;
Range: 0-3600</source>
      </trans-unit>
      <trans-unit id="e514f4a5" xml:space="preserve">
        <source>##### HostInterface

An optional parameter that defines the host interface. The host interface is used at host [autoregistration](/manual/discovery/auto_registration#using_dns_as_default_interface) process. If not defined, the value will be acquired from HostInterfaceItem.&lt;br&gt;The agent will issue an error and not start if the value is over the limit of 255 characters.

Range: 0-255 characters</source>
      </trans-unit>
      <trans-unit id="4965ac2b" xml:space="preserve">
        <source>##### HostInterfaceItem

An optional parameter that defines an item used for getting the host interface.&lt;br&gt;Host interface is used at host [autoregistration](/manual/discovery/auto_registration#using_dns_as_default_interface) process.&lt;br&gt;During an autoregistration request the agent will log a warning message if the value returned by the specified item is over the limit of 255 characters.&lt;br&gt;The system.run\[\] item is supported regardless of AllowKey/DenyKey values.&lt;br&gt;This option is only used when HostInterface is not defined.</source>
      </trans-unit>
      <trans-unit id="42990e42" xml:space="preserve">
        <source>##### HostMetadata

An optional parameter that defines host metadata. Host metadata is used only at host autoregistration process (active agent). If not defined, the value will be acquired from HostMetadataItem.&lt;br&gt;The agent will issue an error and not start if the specified value is over the limit of 2034 bytes or a non-UTF-8 string.

Range: 0-2034 bytes</source>
      </trans-unit>
      <trans-unit id="953ee255" xml:space="preserve">
        <source>##### HostMetadataItem

An optional parameter that defines a Zabbix agent item used for getting host metadata. This option is only used when HostMetadata is not defined. User parameters and aliases are supported. The system.run\[\] item is supported regardless of AllowKey/DenyKey values.&lt;br&gt;The HostMetadataItem value is retrieved on each autoregistration attempt and is used only at host autoregistration process (active agent).&lt;br&gt;During an autoregistration request the agent will log a warning message if the value returned by the specified item is over the limit of 65535 UTF-8 code points. The value returned by the item must be a UTF-8 string otherwise it will be ignored.</source>
      </trans-unit>
      <trans-unit id="79d09548" xml:space="preserve">
        <source>##### Hostname

A list of comma-delimited, unique, case-sensitive hostnames. Required for active checks and must match hostnames as configured on the server. The value is acquired from HostnameItem if undefined.&lt;br&gt;Allowed characters: alphanumeric, '.', ' ', '\_' and '-'. Maximum length: 128 characters per hostname, 2048 characters for the entire line.

Default: Set by HostnameItem</source>
      </trans-unit>
      <trans-unit id="14aaef49" xml:space="preserve">
        <source>##### HostnameItem

An optional parameter that defines a Zabbix agent item used for getting the host name. This option is only used when Hostname is not defined. User parameters or aliases are not supported, but the system.run\[\] item is supported regardless of AllowKey/DenyKey values.&lt;br&gt;The output length is limited to 512KB.

Default: `system.hostname`</source>
      </trans-unit>
      <trans-unit id="1e31f1b3" xml:space="preserve">
        <source>##### Include

You may include individual files or all files in a directory in the configuration file. To only include relevant files in the specified directory, the asterisk wildcard character is supported for pattern matching.&lt;br&gt;See [special notes](special_notes_include) about limitations.

Example:

    Include=/absolute/path/to/config/files/*.conf</source>
      </trans-unit>
      <trans-unit id="d49e31a2" xml:space="preserve">
        <source>##### ListenBacklog

The maximum number of pending connections in the TCP queue.&lt;br&gt;The default value is a hard-coded constant, which depends on the system.&lt;br&gt;The maximum supported value depends on the system, too high values may be silently truncated to the 'implementation-specified maximum'.

Default: `SOMAXCONN`&lt;br&gt;
Range: 0 - INT\_MAX</source>
      </trans-unit>
      <trans-unit id="62349c68" xml:space="preserve">
        <source>##### ListenIP

A list of comma-delimited IP addresses that the agent should listen on.

Default: `0.0.0.0`</source>
      </trans-unit>
      <trans-unit id="e99b72c0" xml:space="preserve">
        <source>##### ListenPort

The agent will listen on this port for connections from the server.

Default: `10050`&lt;br&gt;
Range: 1024-32767</source>
      </trans-unit>
      <trans-unit id="fb75d239" xml:space="preserve">
        <source>##### LoadModule

The module to load at agent startup. Modules are used to extend the functionality of the agent. The module must be located in the directory specified by LoadModulePath or the path must precede the module name. If the preceding path is absolute (starts with '/') then LoadModulePath is ignored.&lt;br&gt;Formats:&lt;br&gt;LoadModule=&lt;module.so&gt;&lt;br&gt;LoadModule=&lt;path/module.so&gt;&lt;br&gt;LoadModule=&lt;/abs\_path/module.so&gt;&lt;br&gt;It is allowed to include multiple LoadModule parameters.</source>
      </trans-unit>
      <trans-unit id="bd604c99" xml:space="preserve">
        <source>##### LoadModulePath

The full path to the location of agent modules. The default depends on compilation options.</source>
      </trans-unit>
      <trans-unit id="bb5252d8" xml:space="preserve">
        <source>##### LogFile

The name of the log file.

Mandatory: Yes, if LogType is set to *file*; otherwise no</source>
      </trans-unit>
      <trans-unit id="778f1edc" xml:space="preserve">
        <source>##### LogFileSize

The maximum size of a log file in MB.&lt;br&gt;0 - disable automatic log rotation.&lt;br&gt;*Note*: If the log file size limit is reached and file rotation fails, for whatever reason, the existing log file is truncated and started anew.

Default: `1`&lt;br&gt;
Range: 0-1024</source>
      </trans-unit>
      <trans-unit id="3d97cc10" xml:space="preserve">
        <source>##### LogRemoteCommands

Enable logging of the executed shell commands as warnings. Commands will be logged only if executed remotely. Log entries will not be created if system.run\[\] is launched locally by HostMetadataItem, HostInterfaceItem or HostnameItem parameters.

Default: `0`&lt;br&gt;
Values: 0 - disabled, 1 - enabled</source>
      </trans-unit>
      <trans-unit id="9d26f327" xml:space="preserve">
        <source>##### LogType

The type of the log output:&lt;br&gt;*file* - write log to the file specified by LogFile parameter;&lt;br&gt;*system* - write log to syslog;&lt;br&gt;*console* - write log to standard output.

Default: `file`</source>
      </trans-unit>
      <trans-unit id="7d39adb8" xml:space="preserve">
        <source>##### MaxLinesPerSecond

The maximum number of new lines the agent will send per second to Zabbix server or proxy when processing 'log' and 'logrt' active checks. The provided value will be overridden by the 'maxlines' parameter, provided in the 'log' or 'logrt' item key.&lt;br&gt;*Note*: Zabbix will process 10 times more new lines than set in *MaxLinesPerSecond* to seek the required string in log items.

Default: `20`&lt;br&gt;
Range: 1-1000</source>
      </trans-unit>
      <trans-unit id="8a5c9259" xml:space="preserve">
        <source>##### PidFile

The name of the PID file.

Default: `/tmp/zabbix_agentd.pid`</source>
      </trans-unit>
      <trans-unit id="b62afa60" xml:space="preserve">
        <source>##### RefreshActiveChecks

How often the list of active checks is refreshed, in seconds. Note that after failing to refresh active checks the next refresh will be attempted in 60 seconds.

Default: `5`&lt;br&gt;
Range: 1-86400</source>
      </trans-unit>
      <trans-unit id="46329efe" xml:space="preserve">
        <source>##### Server

A list of comma-delimited IP addresses, optionally in CIDR notation, or DNS names of Zabbix servers and Zabbix proxies. Incoming connections will be accepted only from the hosts listed here. If IPv6 support is enabled then '127.0.0.1', '::127.0.0.1', '::ffff:127.0.0.1' are treated equally and '::/0' will allow any IPv4 or IPv6 address. '0.0.0.0/0' can be used to allow any IPv4 address. Note, that "IPv4-compatible IPv6 addresses" (0000::/96 prefix) are supported but deprecated by [RFC4291](https://tools.ietf.org/html/rfc4291#section-2.5.5). Spaces are allowed.

Example: 

    Server=127.0.0.1,192.168.1.0/24,::1,2001:db8::/32,zabbix.example.com

Mandatory: yes, if StartAgents is not explicitly set to 0</source>
      </trans-unit>
      <trans-unit id="d6a09b20" xml:space="preserve">
        <source>##### ServerActive

The Zabbix server/proxy address or cluster configuration to get active checks from. The server/proxy address is an IP address or DNS name and optional port separated by colon.&lt;br&gt;Cluster configuration is one or more server addresses separated by semicolon. Multiple Zabbix servers/clusters and Zabbix proxies can be specified, separated by comma. More than one Zabbix proxy should not be specified from each Zabbix server/cluster. If Zabbix proxy is specified then Zabbix server/cluster for that proxy should not be specified.&lt;br&gt;Multiple comma-delimited addresses can be provided to use several independent Zabbix servers in parallel. Spaces are allowed.&lt;br&gt;If the port is not specified, default port is used.&lt;br&gt;IPv6 addresses must be enclosed in square brackets if port for that host is specified. If port is not specified, square brackets for IPv6 addresses are optional.&lt;br&gt;If this parameter is not specified, active checks are disabled.

Example for Zabbix proxy: 

    ServerActive=127.0.0.1:10051

Example for multiple servers: 

    ServerActive=127.0.0.1:20051,zabbix.domain,[::1]:30051,::1,[12fc::1]

Example for high availability:

    ServerActive=zabbix.cluster.node1;zabbix.cluster.node2:20051;zabbix.cluster.node3

Example for high availability with two clusters and one server:

    ServerActive=zabbix.cluster.node1;zabbix.cluster.node2:20051,zabbix.cluster2.node1;zabbix.cluster2.node2,zabbix.domain</source>
      </trans-unit>
      <trans-unit id="020cc066" xml:space="preserve">
        <source>##### SourceIP

The source IP address for:&lt;br&gt;- outgoing connections to Zabbix server or Zabbix proxy;&lt;br&gt;- making connections while executing some items (web.page.get, net.tcp.port, etc.).</source>
      </trans-unit>
      <trans-unit id="6e2d3567" xml:space="preserve">
        <source>##### StartAgents

The number of pre-forked instances of zabbix\_agentd that process passive checks. If set to 0, passive checks are disabled and the agent will not listen on any TCP port.

Default: `3`&lt;br&gt;
Range: 0-100</source>
      </trans-unit>
      <trans-unit id="ee64bcdf" xml:space="preserve">
        <source>##### Timeout

Spend no more than Timeout seconds on processing.

Default: `3`&lt;br&gt;
Range: 1-30</source>
      </trans-unit>
      <trans-unit id="849f8496" xml:space="preserve">
        <source>##### TLSAccept

What incoming connections to accept. Used for a passive checks. Multiple values can be specified, separated by comma:&lt;br&gt;*unencrypted* - accept connections without encryption (default)&lt;br&gt;*psk* - accept connections with TLS and a pre-shared key (PSK)&lt;br&gt;*cert* - accept connections with TLS and a certificate

Mandatory: yes, if TLS certificate or PSK parameters are defined (even for *unencrypted* connection); otherwise no</source>
      </trans-unit>
      <trans-unit id="336a620d" xml:space="preserve">
        <source>##### TLSCAFile

The full pathname of the file containing the top-level CA(s) certificates for peer certificate verification, used for encrypted communications between Zabbix components.</source>
      </trans-unit>
      <trans-unit id="185ad3c6" xml:space="preserve">
        <source>##### TLSCertFile

The full pathname of the file containing the agent certificate or certificate chain, used for encrypted communications with Zabbix components.</source>
      </trans-unit>
      <trans-unit id="3e9bf2b8" xml:space="preserve">
        <source>##### TLSCipherAll

The GnuTLS priority string or OpenSSL (TLS 1.2) cipher string. Override the default ciphersuite selection criteria for certificate- and PSK-based encryption.

Example:

    TLS_AES_256_GCM_SHA384:TLS_CHACHA20_POLY1305_SHA256:TLS_AES_128_GCM_SHA256</source>
      </trans-unit>
      <trans-unit id="03e7f520" xml:space="preserve">
        <source>##### TLSCipherAll13

The cipher string for OpenSSL 1.1.1 or newer in TLS 1.3. Override the default ciphersuite selection criteria for certificate- and PSK-based encryption.

Example for GnuTLS: 

    NONE:+VERS-TLS1.2:+ECDHE-RSA:+RSA:+ECDHE-PSK:+PSK:+AES-128-GCM:+AES-128-CBC:+AEAD:+SHA256:+SHA1:+CURVE-ALL:+COMP-NULL::+SIGN-ALL:+CTYPE-X.509

Example for OpenSSL: 

    EECDH+aRSA+AES128:RSA+aRSA+AES128:kECDHEPSK+AES128:kPSK+AES128</source>
      </trans-unit>
      <trans-unit id="f7468437" xml:space="preserve">
        <source>##### TLSCipherCert

The GnuTLS priority string or OpenSSL (TLS 1.2) cipher string. Override the default ciphersuite selection criteria for certificate-based encryption.

Example for GnuTLS: 

    NONE:+VERS-TLS1.2:+ECDHE-RSA:+RSA:+AES-128-GCM:+AES-128-CBC:+AEAD:+SHA256:+SHA1:+CURVE-ALL:+COMP-NULL:+SIGN-ALL:+CTYPE-X.509

Example for OpenSSL: 

    EECDH+aRSA+AES128:RSA+aRSA+AES128</source>
      </trans-unit>
      <trans-unit id="717d9f51" xml:space="preserve">
        <source>##### TLSCipherCert13

The cipher string for OpenSSL 1.1.1 or newer in TLS 1.3. Override the default ciphersuite selection criteria for certificate-based encryption.</source>
      </trans-unit>
      <trans-unit id="3a633a0a" xml:space="preserve">
        <source>##### TLSCipherPSK

The GnuTLS priority string or OpenSSL (TLS 1.2) cipher string. Override the default ciphersuite selection criteria for PSK-based encryption.

Example for GnuTLS: 

    NONE:+VERS-TLS1.2:+ECDHE-PSK:+PSK:+AES-128-GCM:+AES-128-CBC:+AEAD:+SHA256:+SHA1:+CURVE-ALL:+COMP-NULL:+SIGN-ALL

Example for OpenSSL: 

    kECDHEPSK+AES128:kPSK+AES128</source>
      </trans-unit>
      <trans-unit id="8208c770" xml:space="preserve">
        <source>##### TLSCipherPSK13

The cipher string for OpenSSL 1.1.1 or newer in TLS 1.3. Override the default ciphersuite selection criteria for PSK-based encryption.

Example:

    TLS_CHACHA20_POLY1305_SHA256:TLS_AES_128_GCM_SHA256</source>
      </trans-unit>
      <trans-unit id="b1151069" xml:space="preserve">
        <source>##### TLSConnect

How the agent should connect to Zabbix server or proxy. Used for active checks. Only one value can be specified:&lt;br&gt;*unencrypted* - connect without encryption (default)&lt;br&gt;*psk* - connect using TLS and a pre-shared key (PSK)&lt;br&gt;*cert* - connect using TLS and a certificate

Mandatory: yes, if TLS certificate or PSK parameters are defined (even for *unencrypted* connection); otherwise no</source>
      </trans-unit>
      <trans-unit id="28aef301" xml:space="preserve">
        <source>##### TLSCRLFile

The full pathname of the file containing revoked certificates. This parameter is used for encrypted communications between Zabbix components.</source>
      </trans-unit>
      <trans-unit id="cbcbba0a" xml:space="preserve">
        <source>##### TLSKeyFile

The full pathname of the file containing the agent private key, used for encrypted communications between Zabbix components.</source>
      </trans-unit>
      <trans-unit id="845f5b70" xml:space="preserve">
        <source>##### TLSPSKFile

The full pathname of the file containing the agent pre-shared key, used for encrypted communications with Zabbix server.</source>
      </trans-unit>
      <trans-unit id="b02e4942" xml:space="preserve">
        <source>##### TLSPSKIdentity

The pre-shared key identity string, used for encrypted communications with Zabbix server.</source>
      </trans-unit>
      <trans-unit id="7a78525c" xml:space="preserve">
        <source>##### TLSServerCertIssuer

The allowed server (proxy) certificate issuer.</source>
      </trans-unit>
      <trans-unit id="c8d1ac8b" xml:space="preserve">
        <source>##### TLSServerCertSubject

The allowed server (proxy) certificate subject.</source>
      </trans-unit>
      <trans-unit id="676eb24d" xml:space="preserve">
        <source>##### UnsafeUserParameters

Allow all characters to be passed in arguments to user-defined parameters. The following characters are not allowed: \\ ' " \` \* ? \[ \] { } \~ $ ! &amp; ; ( ) &lt; &gt; \| \# @&lt;br&gt;Additionally, newline characters are not allowed.

Default: `0`&lt;br&gt;
Values: 0 - do not allow, 1 - allow</source>
      </trans-unit>
      <trans-unit id="e9779b31" xml:space="preserve">
        <source>##### User

Drop privileges to a specific, existing user on the system.&lt;br&gt;Only has effect if run as 'root' and AllowRoot is disabled.

Default: `zabbix`</source>
      </trans-unit>
      <trans-unit id="1591e129" xml:space="preserve">
        <source>##### UserParameter

A user-defined parameter to monitor. There can be several user-defined parameters.&lt;br&gt;Format: UserParameter=&lt;key&gt;,&lt;shell command&gt;&lt;br&gt;Note that the shell command must not return empty string or EOL only. Shell commands may have relative paths, if the UserParameterDir parameter is specified.

Example:

    UserParameter=system.test,who|wc -l
    UserParameter=check_cpu,./custom_script.sh</source>
      </trans-unit>
      <trans-unit id="493da8fd" xml:space="preserve">
        <source>##### UserParameterDir

The default search path for UserParameter commands. If used, the agent will change its working directory to the one specified here before executing a command. Thereby, UserParameter commands can have a relative `./` prefix instead of a full path.&lt;br&gt;Only one entry is allowed.

Example:

    UserParameterDir=/opt/myscripts</source>
      </trans-unit>
      <trans-unit id="9170c364" xml:space="preserve">
        <source>#### See also

1.  [Differences in the Zabbix agent configuration for active and
    passive checks starting from version
    2.0.0](http://blog.zabbix.com/multiple-servers-for-active-agent-sure/858)</source>
      </trans-unit>
    </body>
  </file>
</xliff>
