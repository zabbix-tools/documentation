[comment]: # translation:outdated

[comment]: # ({new-0429da2a})
# 2 Sound in browsers

[comment]: # ({/new-0429da2a})

[comment]: # ({new-ca9dfcae})
#### Overview

Sound is used in [global
notifications](/manual/web_interface/user_profile/global_notifications).

For the sounds to be played in Zabbix frontend, *Frontend messaging*
must be enabled in the user profile *Messaging* tab, with all trigger
severities checked, and sounds should also be enabled in the global
notification pop-up window.

If for some reasons audio cannot be played on the device, the
![](../../../../assets/en/manual/config/mute_icon.png) button in the
global notification pop-up window will permanently remain in the "mute"
state and the message "Cannot support notification audio for this
device." will be displayed upon hovering over the
![](../../../../assets/en/manual/config/mute_icon.png) button.

Sounds, including the default audio clips, are supported in MP3 format
only.

The sounds of Zabbix frontend have been successfully tested in recent
Firefox/Opera browsers on Linux and Chrome, Firefox, Microsoft Edge,
Opera and Safari browsers on Windows.

::: noteimportant
The auto play of sounds may be disabled in recent
browser versions by default. In this case, you need to change this
setting manually.
:::

[comment]: # ({/new-ca9dfcae})
