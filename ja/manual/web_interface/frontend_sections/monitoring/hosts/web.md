[comment]: # translation:outdated

[comment]: # ({new-cd71b995})
# 2 Web scenarios

[comment]: # ({/new-cd71b995})

[comment]: # ({new-6c7a491c})
#### Overview

Host [web scenario](/manual/web_monitoring) information can be accessed
from *Monitoring → Hosts* by clicking on Web for the respective host.

![](../../../../../../assets/en/manual/web_interface/web_monitoring.png){width="600"}

Data of disabled hosts is also accessible. The name of a disabled host
is listed in red.

The maximum number of scenarios displayed per page depends on the *Rows
per page* user profile
[setting](/manual/web_interface/user_profile#configuration).

By default, only values that fall within the last 24 hours are
displayed. This limit has been introduced with the aim of improving
initial loading times for large pages of latest data. You can extend
this time period by changing the value of *Max history display period*
parameter in the
*[Administration→General](/manual/web_interface/frontend_sections/administration/general#gui)*
menu section.

The scenario name is link to more detailed statistics about it:

![](../../../../../../assets/en/manual/web_monitoring/scenario_details2.png){width="600"}

[comment]: # ({/new-6c7a491c})

[comment]: # ({new-6e8379a0})
##### Using filter

The page shows a list of all web scenarios of the selected host. To view
web scenarios for another host or host group without returning to the
*Monitoring → Hosts* page, select that host or group in the filter. You
may also filter scenarios based on tags.

[comment]: # ({/new-6e8379a0})

[comment]: # ({new-84099469})
##### Buttons

View mode buttons being common for all sections are described on the
[Monitoring](/manual/web_interface/frontend_sections/monitoring#view_mode_buttons)
page.

[comment]: # ({/new-84099469})
