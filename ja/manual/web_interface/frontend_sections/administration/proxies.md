[comment]: # translation:outdated

[comment]: # ({new-000d2902})
# 2 Proxies

[comment]: # ({/new-000d2902})

[comment]: # ({new-6100a7eb})
#### Overview

In the *Administration → Proxies* section proxies for [distributed
monitoring](/manual/distributed_monitoring) can be configured in the
Zabbix frontend.

[comment]: # ({/new-6100a7eb})

[comment]: # ({new-d8b90929})
#### Proxies

A listing of existing proxies with their details is displayed.

![](../../../../../assets/en/manual/web_interface/proxies1.png){width="600"}

Displayed data:

|Column|Description|
|------|-----------|
|*Name*|Name of the proxy. Clicking on the proxy name opens the proxy [configuration form](/manual/distributed_monitoring/proxies#configuration).|
|*Mode*|Proxy mode is displayed - *Active* or *Passive*.|
|*Encryption*|Encryption status for connections from the proxy is displayed:<br>**None** - no encryption<br>**PSK** - using pre-shared key<br>**Cert** - using certificate<br>|
|*Last seen (age)*|The time when the proxy was last seen by the server is displayed.|
|*Host count*|The number of enabled hosts assigned to the proxy is displayed.|
|*Item count*|The number of enabled items on enabled hosts assigned to the proxy is displayed.|
|*Required performance (vps)*|Required proxy performance is displayed (the number of values that need to be collected per second).|
|*Hosts*|All hosts monitored by the proxy are listed. Clicking on the host name opens the host configuration form.|

To configure a new proxy, click on the *Create proxy* button in the top
right-hand corner.

[comment]: # ({/new-d8b90929})

[comment]: # ({new-1079c528})
##### Mass editing options

Buttons below the list offer some mass-editing options:

-   *Enable hosts* - change the status of hosts monitored by the proxy
    to *Monitored*
-   *Disable hosts* - change the status of hosts monitored by the proxy
    to *Not monitored*
-   *Delete* - delete the proxies

To use these options, mark the checkboxes before the respective proxies,
then click on the required button.

[comment]: # ({/new-1079c528})

[comment]: # ({new-de1da12a})
##### Using filter

You can use the filter to display only the proxies you are interested
in. For better search performance, data is searched with macros
unresolved.

The *Filter* link is available above the list of proxies. If you click
on it, a filter becomes available where you can filter proxies by name
and mode.

![](../../../../../assets/en/manual/web_interface/proxies_filter1.png){width="600"}

[comment]: # ({/new-de1da12a})
