[comment]: # translation:outdated

[comment]: # ({new-fa269a08})
# 3 SLA

[comment]: # ({/new-fa269a08})

[comment]: # ({new-1d46f9b5})
### Overview

In this section you can configure SLAs.

[comment]: # ({/new-1d46f9b5})

[comment]: # ({new-424c7051})
### SLAs

![](../../../../../assets/en/manual/web_interface/sla_list.png){width="600"}

A list of the configured SLAs is displayed. *Note* that only the SLAs 
related to services accessible to the user will be displayed (as read-only, 
unless *Manage SLA* is enabled for the user role).


Displayed data:

|Parameter|Description|
|---------|-----------|
|*Name*|The SLA name is displayed.<br>The name is a link to [SLA configuration](#configuration).<br> |
|*SLO*|The service level objective (SLO) is displayed. |
|*Effective date*|The date of starting SLA calculation is displayed. |
|*Reporting period*|The period used in the SLA report is displayed - *daily*, *weekly*, *monthly*, *quarterly*, or *annually*. |
|*Time zone*|The SLA time zone is displayed. |
|*Schedule*|The SLA schedule is displayed - 24x7 or custom. |
|*SLA report*|Click on the link to see the SLA report for this SLA. |
|*Status*|The SLA status is displayed - enabled or disabled. |

[comment]: # ({/new-424c7051})



