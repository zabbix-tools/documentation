[comment]: # translation:outdated

[comment]: # ({new-093ad1f3})
# 3 Data overview

[comment]: # ({/new-093ad1f3})

[comment]: # ({new-3c27cf5e})
:::noteimportant
This widget is deprecated and will be removed in the upcoming major release.
:::

[comment]: # ({/new-3c27cf5e})

[comment]: # ({new-fb44b8b3})
#### Overview

In the data overview widget, you can display the latest data for a group
of hosts.

The color of problem items is based on the problem severity color, which
can be adjusted in the [problem
update](/manual/acknowledgment#updating_problems) screen.

By default, only values that fall within the last 24 hours are
displayed. This limit has been introduced with the aim of improving
initial loading times for large pages of latest data. This limit is
configurable in *Administration* → *General* →
*[GUI](/manual/web_interface/frontend_sections/administration/general#gui)*,
using the *Max history display period* option.

Clicking on a piece of data offers links to some predefined graphs or
latest values.

Note that 50 records are displayed by default (configurable in
*Administration* → *General* →
*[GUI](/manual/web_interface/frontend_sections/administration/general#gui)*,
using the *Max number of columns and rows in overview tables* option).
If more records exist than are configured to display, a message is
displayed at the bottom of the table, asking to provide more specific
filtering criteria. There is no pagination.

[comment]: # ({/new-fb44b8b3})

[comment]: # ({new-f4866132})
#### Configuration

To configure, select *Data overview* as type:

![](../../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/data_overview.png)

In addition to the parameters that are [common](/manual/web_interface/frontend_sections/dashboards/widgets#common-parameters) 
for all widgets, you may set the following specific options:

|   |   |
|--|--------|
|*Host groups*|Select host groups. This field is auto-complete so starting to type the name of a group will offer a dropdown of matching groups. Scroll down to select. Click on 'x' to remove the selected.|
|*Hosts*|Select hosts. This field is auto-complete so starting to type the name of a host will offer a dropdown of matching hosts. Scroll down to select. Click on 'x' to remove the selected.|
|*Tags*|Specify tags to limit the number of item data displayed in the widget. It is possible to include as well as exclude specific tags and tag values. Several conditions can be set. Tag name matching is always case-sensitive.<br>There are several operators available for each condition:<br>**Exists** - include the specified tag names<br>**Equals** - include the specified tag names and values (case-sensitive)<br>**Contains** - include the specified tag names where the tag values contain the entered string (substring match, case-insensitive)<br>**Does not exist** - exclude the specified tag names<br>**Does not equal** - exclude the specified tag names and values (case-sensitive)<br>**Does not contain** - exclude the specified tag names where the tag values contain the entered string (substring match, case-insensitive)<br>There are two calculation types for conditions:<br>**And/Or** - all conditions must be met, conditions having the same tag name will be grouped by the Or condition<br>**Or** - enough if one condition is met|
|*Show suppressed problems*|Mark the checkbox to display problems that would otherwise be suppressed (not shown) because of host maintenance.|
|*Hosts location*|Select host location - left or top.|

[comment]: # ({/new-f4866132})
