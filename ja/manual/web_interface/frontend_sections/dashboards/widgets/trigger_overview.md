[comment]: # translation:outdated


[comment]: # ({new-da50605d})
# 22 Trigger overview

[comment]: # ({/new-da50605d})

[comment]: # ({new-8f45cbdc})
#### Overview

In the trigger overview widget, you can display the trigger states for a
group of hosts.

-   The trigger states are displayed as colored blocks (the color of
    problem triggers depends on the problem severity color, which can be
    adjusted in the [problem
    update](/manual/acknowledgment#updating_problems) screen). Note that
    recent trigger changes (within the last 2 minutes) will be displayed
    as blinking blocks.
-   Blue up and down arrows indicate triggers that have dependencies. On
    mouseover, dependency details are revealed.
-   A checkbox icon indicates acknowledged problems. All problems or
    resolved problems of the trigger must be acknowledged for this icon
    to be displayed.

Clicking on a trigger block provides context-dependent links to problem
events of the trigger, the problem acknowledgment screen, trigger
configuration, trigger URL or a simple graph/latest values list.

Note that 50 records are displayed by default (configurable in
*Administration* → *General* →
*[GUI](/manual/web_interface/frontend_sections/administration/general#gui)*,
using the *Max number of columns and rows in overview tables* option).
If more records exist than are configured to display, a message is
displayed at the bottom of the table, asking to provide more specific
filtering criteria. There is no pagination.

[comment]: # ({/new-8f45cbdc})

[comment]: # ({new-d9e485e9})
#### Configuration

To configure, select *Trigger overview* as type:

![](../../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/trigger_overview.png)

In addition to the parameters that are [common](/manual/web_interface/frontend_sections/dashboards/widgets#common-parameters) 
for all widgets, you may set the following specific options:

|   |   |
|--|--------|
|*Show*|Filter by problem status:<br>**Recent problems** - unresolved and recently resolved problems are displayed (default)<br>**Problems** - unresolved problems are displayed<br>**Any** - history of all events is displayed|
|*Host groups*|Select the host group(s). This field is auto-complete so starting to type the name of a group will offer a dropdown of matching groups.|
|*Hosts*|Select hosts. This field is auto-complete so starting to type the name of a host will offer a dropdown of matching hosts. Scroll down to select. Click on 'x' to remove the selected.|
|*Tags*|Specify tags to limit the number of item and trigger data displayed in the widget. It is possible to include as well as exclude specific tags and tag values. Several conditions can be set. Tag name matching is always case-sensitive.<br>There are several operators available for each condition:<br>**Exists** - include the specified tag names<br>**Equals** - include the specified tag names and values (case-sensitive)<br>**Contains** - include the specified tag names where the tag values contain the entered string (substring match, case-insensitive)<br>**Does not exist** - exclude the specified tag names<br>**Does not equal** - exclude the specified tag names and values (case-sensitive)<br>**Does not contain** - exclude the specified tag names where the tag values contain the entered string (substring match, case-insensitive)<br>There are two calculation types for conditions:<br>**And/Or** - all conditions must be met, conditions having the same tag name will be grouped by the Or condition<br>**Or** - enough if one condition is met|
|*Show suppressed problems*|Mark the checkbox to display problems that would otherwise be suppressed (not shown) because of host maintenance.|
|*Hosts location*|Select host location - left or top.|

[comment]: # ({/new-d9e485e9})
