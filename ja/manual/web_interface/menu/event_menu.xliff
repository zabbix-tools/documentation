<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="ja" datatype="plaintext" original="manual/web_interface/menu/event_menu.md">
    <body>
      <trans-unit id="e923eaa6" xml:space="preserve">
        <source>
# 1 Event context menu</source>
      </trans-unit>
      <trans-unit id="17351e40" xml:space="preserve">
        <source>
## Overview

The event context menu provides shortcuts for executing frequently required actions or navigating to UI sections related to an event.

![](../../../../assets/en/manual/web_interface/event_context_menu.png)</source>
      </trans-unit>
      <trans-unit id="2adcfe6d" xml:space="preserve">
        <source>
## Content

The event context menu has four sections: *View*, *Configuration*, *Problem*, *Links*, and *Scripts*.
For the entities that are not configured, links are disabled and displayed in gray.
The sections *Scripts* and *Links* are displayed if their entities are configured.

The *View* section contains links to:

-  **Problems** - opens the list of unresolved problems of the underlying trigger;
-  **Update problem** - opens the [problem update](manual/acknowledgment#updating-problems) screen;
-  **History** - leads to the *Latest data* graph/item history for the underlying item(s).
   If a trigger uses several items, links will be available for each of them.

The *Configuration* section contains links to the configuration of:

-  **Trigger** that fired the problem;
-  **Items** used in the trigger expression.

::: notetip
Note that configuration section is available only for Admin and Super admin users.
:::

The *Problem* section contains the options to:

-  **Mark as cause** - mark the problem as cause;
-  **Mark selected as symptoms** - mark the selected problems as symptoms of this problem.

The *Links* section contains links to:

-  access a configured [trigger URL](/manual/config/triggers/trigger#configuration);
-  access custom links configured in [Global scripts](/manual/web_interface/frontend_sections/alerts/scripts)
  (with scope 'Manual event action' and type 'URL');
- access a configured external ticket for the problem (see the *Include event menu entry* option when configuring [webhooks](/manual/config/notifications/media/webhook).

The *Scripts* section contains links to execute a global [script](/manual/web_interface/frontend_sections/alerts/scripts)
(with scope *Manual event action*).
This feature may be handy for running scripts used for managing problem tickets in external systems.</source>
      </trans-unit>
      <trans-unit id="25185444" xml:space="preserve">
        <source>
## Supported locations

The event context menu is accessible by clicking on a problem or event name in various frontend sections, for example:

- Dashboards [widgets](/manual/web_interface/frontend_sections/dashboards/widgets),
  such as *Problems* widget, *Trigger overview* widget, etc.
- Monitoring → [Problems](/manual/web_interface/frontend_sections/monitoring/problems)
- Monitoring → [Problems](/manual/web_interface/frontend_sections/monitoring/problems) → Event details
- Reports → [Triggers top 100](/manual/web_interface/frontend_sections/reports/triggers_top) (global scripts and access
  to external ticket are not supported in this location)</source>
      </trans-unit>
    </body>
  </file>
</xliff>
