[comment]: # translation:outdated

[comment]: # ({new-5bf78e6f})

# 3 Item context menu

[comment]: # ({/new-5bf78e6f})

[comment]: # ({new-5b802363})

## Overview

The item context menu provides shortcuts for executing frequently required actions or navigating to UI sections related to an item.

![](../../../../assets/en/manual/web_interface/latest%20data_itemname_item_context_menu.png)

[comment]: # ({/new-5b802363})

[comment]: # ({new-e8f22467})

### Content

The item context menu has three sections: *View*, *Configuration*, and *Actions*.

*View* section contains links to:

-  **Latest data** - opens the *Latest data* section filtered by the current host and item.
-  **Graph** - opens simple graph of the current item.
-  **Values** - opens the list of all values received for the current item within the past 60 minutes.
-  **500 latest values** - opens the list of 500 latest values for the current item.

*Configuration* section contains links to:

-  **Item** - opens the configuration form of the current item.
-  **Triggers** - displays a list of existing triggers based on the current item;
   clicking on a trigger name opens the trigger's configuration form.
-  **Create trigger** - opens a trigger configuration form to create a new trigger based on the current item.
-  **Create dependent item** - opens an item configuration form to create a dependent item for the current item.
-  **Create dependent discovery rule** - opens a discovery rule configuration form
   to create a low-level discovery rule for the current item.

Links to *Create dependent item* and *Create dependent discovery rule* are enabled only when the item context menu is
accessed from *Data collection* section.

::: notetip
Note that configuration section is available only for Admin and Super admin users.
:::

*Actions* section contains a link to **Execute now** - the option to [execute a check](/manual/config/items/check_now)
for a new item value immediately.

[comment]: # ({/new-e8f22467})

[comment]: # ({new-3ad956d9})

#### Supported locations

The item context menu is accessible by clicking on an item name in various frontend sections, for example:

- Monitoring → [Latest data](/manual/web_interface/frontend_sections/monitoring/latest_data)
- Data Collection → Hosts → [Items](/manual/web_interface/frontend_sections/data_collection/hosts/items)
- Data collection → Hosts → Discovery rules →
[Item prototypes](/manual/web_interface/frontend_sections/data_collection/hosts/discovery/item_prototypes)

[comment]: # ({/new-3ad956d9})
