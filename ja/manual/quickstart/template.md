[comment]: # translation:outdated

[comment]: # ({new-ffc9e78e})
# 6 New template

[comment]: # ({/new-ffc9e78e})

[comment]: # ({new-3bb3ba5d})
#### Overview

In this section you will learn how to set up a template.

Previously we learned how to set up an item, a trigger and how to get a
problem notification for the host.

While all of these steps offer a great deal of flexibility in
themselves, it may appear like a lot of steps to take if needed for,
say, a thousand hosts. Some automation would be handy.

This is where templates come to help. Templates allow to group useful
items, triggers and other entities so that those can be reused again and
again by applying to hosts in a single step.

When a template is linked to a host, the host inherits all entities of
the template. So, basically a pre-prepared bunch of checks can be
applied very quickly.

[comment]: # ({/new-3bb3ba5d})

[comment]: # ({new-f182767e})
#### Adding template

To start working with templates, we must first create one. To do that,
in *Configuration → Templates* click on *Create template*. This will
present us with a template configuration form.\
![](../../../assets/en/manual/quickstart/new_template.png)

All mandatory input fields are marked with a red asterisk.

The required parameters to enter here are:

***Template name***

-   Enter a template name. Alpha-numericals, spaces and underscores are
    allowed.

***Groups***

-   Select one or several groups by clicking *Select* button. The
    template must belong to a group.

When done, click *Add*. Your new template should be visible in the list
of templates.

![](../../../assets/en/manual/quickstart/template_list.png){width="600"}

As you may see, the template is there, but it holds nothing in it - no
items, triggers or other entities.

[comment]: # ({/new-f182767e})

[comment]: # ({new-ad341c27})
#### Adding item to template

To add an item to the template, go to the item list for 'New host'. In
*Configuration → Hosts* click on *Items* next to 'New host'.

Then:

-   mark the checkbox of the 'CPU Load' item in the list
-   click on *Copy* below the list
-   select the template to copy item to

![](../../../assets/en/manual/quickstart/copy_to_template.png)

All mandatory input fields are marked with a red asterisk.

-   click on *Copy*

If you now go to *Configuration → Templates*, 'New template' should have
one new item in it.

We will stop at one item only for now, but similarly you can add any
other items, triggers or other entities to the template until it's a
fairly complete set of entities for given purpose (monitoring OS,
monitoring single application).

[comment]: # ({/new-ad341c27})

[comment]: # ({new-230cf552})
#### Linking template to host

With a template ready, it only remains to add it to a host. For that, go
to *Configuration → Hosts*, click on 'New host' to open its property
form and find the **Templates** field.

Start typing *New template* in the *Templates* field. The name of
template we have created should appear in the dropdown list. Scroll down
to select. See that it appears in the *Templates* field.

![](../../../assets/en/manual/quickstart/link_template.png)

Click *Update* in the form to save the changes. The template is now
added to the host, with all entities that it holds.

As you may have guessed, this way it can be applied to any other host as
well. Any changes to the items, triggers and other entities at the
template level will propagate to the hosts the template is linked to.

[comment]: # ({/new-230cf552})

[comment]: # ({new-c7f1a5c4})
##### Linking pre-defined templates to hosts

As you may have noticed, Zabbix comes with a set of predefined templates
for various OS, devices and applications. To get started with monitoring
very quickly, you may link the appropriate one of them to a host, but
beware that these templates need to be fine-tuned for your environment.
Some checks may not be needed, and polling intervals may be way too
frequent.

More information about [templates](/manual/config/templates) is
available.

[comment]: # ({/new-c7f1a5c4})
