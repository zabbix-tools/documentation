<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="ja" datatype="plaintext" original="manpages/zabbix_server.md">
    <body>
      <trans-unit id="fdbd84bb" xml:space="preserve">
        <source># zabbix\_server

Section: Maintenance Commands (8)\
Updated: 2020-09-04\
[Index](#index) [Return to Main Contents](/manpages)

------------------------------------------------------------------------

[ ]{#lbAB}</source>
      </trans-unit>
      <trans-unit id="1090a00e" xml:space="preserve">
        <source>## NAME

zabbix\_server - Zabbix server daemon [ ]{#lbAC}</source>
      </trans-unit>
      <trans-unit id="2a5507db" xml:space="preserve">
        <source>## SYNOPSIS

**zabbix\_server** \[**-c** *config-file*\]\
**zabbix\_server** \[**-c** *config-file*\] **-R** *runtime-option*\
**zabbix\_server -h**\
**zabbix\_server -V** [ ]{#lbAD}</source>
      </trans-unit>
      <trans-unit id="f39aa640" xml:space="preserve">
        <source>## DESCRIPTION

**zabbix\_server** is the core daemon of Zabbix software. [ ]{#lbAE}</source>
      </trans-unit>
      <trans-unit id="455125db" xml:space="preserve">
        <source>## OPTIONS

**-c**, **--config** *config-file*  
Use the alternate *config-file* instead of the default one.

**-f**, **--foreground**  
Run Zabbix server in foreground.

**-R**, **--runtime-control** *runtime-option*  
Perform administrative functions according to *runtime-option*.

**-h**, **--help**  
Display this help and exit.

**-V**, **--version**  
Output version information and exit.


Examples of running Zabbix server with command line parameters:
```yaml
zabbix_server -c /usr/local/etc/zabbix_server.conf
zabbix_server --help
zabbix_server -V
```

[ ]{#lbAF}</source>
      </trans-unit>
      <trans-unit id="d7d7f728" xml:space="preserve">
        <source>
##### RUNTIME CONTROL  
Runtime control options:
  
**config\_cache\_reload**  
Reload configuration cache. Ignored if cache is being currently loaded.
Default configuration file (unless **-c** option is specified) will be
used to find PID file and signal will be sent to process, listed in PID
file.

**snmp\_cache\_reload**  
Reload SNMP cache.

**housekeeper\_execute**  
Execute the housekeeper. Ignored if housekeeper is being currently
executed.

**trigger_housekeeper\_execute**  
Start the trigger housekeeping procedure. Ignored if the trigger housekeeping procedure is currently in progress.
  
**diaginfo**\[=*section*\]  
Log internal diagnostic information of the specified section. Section
can be *historycache*, *preprocessing*, *alerting*, *lld*, *valuecache*.
By default diagnostic information of all sections is logged.

**ha_status**  
Log high availability (HA) cluster status.</source>
      </trans-unit>
      <trans-unit id="800a1525" xml:space="preserve">
        <source>
**ha\_remove\_node**\[=*target*\]  
Remove the high availability (HA) node specified by its name or ID.
Note that active/standby nodes cannot be removed.

**ha\_set\_failover\_delay**\[=*delay*\]  
Set high availability (HA) failover delay.
Time suffixes are supported, e.g. 10s, 1m.

**proxy\_config\_cache\_reload**\[=*target*\]
Reload proxy configuration cache.

**secrets\_reload**  
Reload secrets from Vault.

**service\_cache\_reload**  
Reload the service manager cache.

**snmp\_cache\_reload**  
Reload SNMP cache, clear the SNMP properties (engine time, engine boots, engine id, credentials) for all hosts.

**prof\_enable**\[=*target*\]  
Enable profiling.
Affects all processes if target is not specified.
Enabled profiling provides details of all rwlocks/mutexes by function name.
Supported since Zabbix 6.0.13.

**prof\_disable**\[=*target*\]  
Disable profiling.
Affects all processes if target is not specified.
Supported since Zabbix 6.0.13.
  
**log\_level\_increase**\[=*target*\]  
Increase log level, affects all processes if target is not specified

  
**log\_level\_decrease**\[=*target*\]  
Decrease log level, affects all processes if target is not specified

[ ]{#lbAG}</source>
      </trans-unit>
      <trans-unit id="858c64d1" xml:space="preserve">
        <source>### 

  
Log level control targets

  
*process-type*  
All processes of specified type (alerter, alert manager,
configuration syncer, discoverer, escalator, history syncer,
housekeeper, http poller, icmp pinger, ipmi manager, ipmi poller,
java poller, lld manager, lld worker, poller, preprocessing manager,
preprocessing worker, proxy poller, self-monitoring, snmp trapper,
task manager, timer, trapper, unreachable poller, vmware collector)

  
*process-type,N*  
Process type and number (e.g., poller,3)

  
*pid*  
Process identifier, up to 65535. For larger values specify target as
"process-type,N"

```{=html}
&lt;!-- --&gt;
```

[ ]{#lbAH}</source>
      </trans-unit>
      <trans-unit id="00ee0d1e" xml:space="preserve">
        <source>## FILES

*/usr/local/etc/zabbix\_server.conf*  
Default location of Zabbix server configuration file (if not modified
during compile time).

[ ]{#lbAI}</source>
      </trans-unit>
      <trans-unit id="d095490c" xml:space="preserve">
        <source>## SEE ALSO

Documentation &lt;https://www.zabbix.com/manuals&gt;

**[zabbix\_agentd](zabbix_agentd)**(8),
**[zabbix\_get](zabbix_get)**(1), **[zabbix\_proxy](zabbix_proxy)**(8),
**[zabbix\_sender](zabbix_sender)**(1), **[zabbix\_js](zabbix_js)**(1),
**[zabbix\_agent2](zabbix_agent2)**(8) [ ]{#lbAJ}</source>
      </trans-unit>
      <trans-unit id="e55ed07e" xml:space="preserve">
        <source>## AUTHOR

Alexei Vladishev &lt;&lt;alex@zabbix.com&gt;&gt;

------------------------------------------------------------------------

[ ]{#index}</source>
      </trans-unit>
      <trans-unit id="b105a0f1" xml:space="preserve">
        <source>## Index

[NAME](#lbAB)

[SYNOPSIS](#lbAC)

[DESCRIPTION](#lbAD)

[OPTIONS](#lbAE)

[](#lbAF)

[](#lbAG)

  

[FILES](#lbAH)

[SEE ALSO](#lbAI)

[AUTHOR](#lbAJ)

------------------------------------------------------------------------

This document was created on: 16:12:14 GMT, September 04, 2020</source>
      </trans-unit>
    </body>
  </file>
</xliff>
