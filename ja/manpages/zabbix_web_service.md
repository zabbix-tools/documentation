[comment]: # translation:outdated

[comment]: # ({new-d1b7141f})
# zabbix\_web\_service

Section: Maintenance Commands (8)\
Updated: 2019-01-29\
[Index](#index) [Return to Main Contents](/documentation/6.0/manpages)

------------------------------------------------------------------------

[ ]{#lbAB}

[comment]: # ({/new-d1b7141f})

[comment]: # ({new-6319fb62})
## NAME

zabbix\_web\_service - Zabbix web service [ ]{#lbAC}

[comment]: # ({/new-6319fb62})

[comment]: # ({new-93a3222a})
## SYNOPSIS

**zabbix\_web\_service** \[**-c** *config-file*\]\
**zabbix\_web\_service -h**\
**zabbix\_web\_service -V** [ ]{#lbAD}

[comment]: # ({/new-93a3222a})

[comment]: # ({new-c1ed92af})
## DESCRIPTION

**zabbix\_web\_service** is an application for providing web services to
Zabbix components. [ ]{#lbAE}

[comment]: # ({/new-c1ed92af})

[comment]: # ({new-98c2799b})
## OPTIONS

**-c**, **--config** *config-file*  
Use the alternate *config-file* instead of the default one.

**-h**, **--help**  
Display this help and exit.

**-V**, **--version**  
Output version information and exit.

[ ]{#lbAF}

[comment]: # ({/new-98c2799b})

[comment]: # ({new-440875a7})
## FILES

*/usr/local/etc/zabbix\_web\_service.conf*  
Default location of Zabbix web service configuration file (if not
modified during compile time).

[ ]{#lbAG}

[comment]: # ({/new-440875a7})

[comment]: # ({new-4f67430a})
## SEE ALSO

Documentation <https://www.zabbix.com/manuals>

**[zabbix\_agentd](zabbix_agentd)**(8),
**[zabbix\_get](zabbix_get)**(1), **[zabbix\_proxy](zabbix_proxy)**(8),
**[zabbix\_sender](zabbix_sender)**(1),
**[zabbix\_server](zabbix_server)**(8), **[zabbix\_js](zabbix_js)**(1),
**[zabbix\_agent2](zabbix_agent2)**(8) [ ]{#lbAH}

[comment]: # ({/new-4f67430a})

[comment]: # ({new-96162c7c})
## AUTHOR

Zabbix LLC

------------------------------------------------------------------------

[ ]{#index}

[comment]: # ({/new-96162c7c})

[comment]: # ({new-ea1c0f43})
## Index

[NAME](#lbAB)  

[SYNOPSIS](#lbAC)  

[DESCRIPTION](#lbAD)  

[OPTIONS](#lbAE)  

[FILES](#lbAF)  

[SEE ALSO](#lbAG)  

[AUTHOR](#lbAH)  

------------------------------------------------------------------------

This document was created by [man2html](/documentation/6.0/manpages),
using the manual pages.\
Time: 12:58:30 GMT, June 11, 2021

[comment]: # ({/new-ea1c0f43})
