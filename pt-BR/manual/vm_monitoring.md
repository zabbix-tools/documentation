[comment]: # translation:outdated

[comment]: # ({new-3ca85094})
# 9. Monitoramento de máquinas virtuais

[comment]: # ({/new-3ca85094})

[comment]: # ({new-621e23e6})
#### Visão geral

O suporte a monitoramento de ambientes VMWare está disponível desde a
versão 2.2.0 do Zabbix.

O Zabbix pode utilizar-se de regras de LLD para descobrir
automaticamente os hipervisores VMware e as máquinas virtualizadas para
criar hosts para monitora-las, usando protótipos de host definidos pelo
usuário.

A configuração padrão do Zabbix já vem com vários templates prontos para
o uso para monitorar o VMWare vCenter ou ESX.

A versão mínima do VMware vCenter our vSphere é a 5.1.

[comment]: # ({/new-621e23e6})

[comment]: # ({new-4c2e8939})
#### Detalhes

O monitoramento de máquinas virtuais é feito em dois passos:

     * Os dados sobre a máquina virtual são coletados através do processo //vmware collector// do Zabbix. Este processo obterá a informação necessária a partir dos 'web services' do VMWare através do protocolo SOAP, irá pré-processar os dados e deixar na memória compartilhada do Zabbix Server
     * Então estes dados podem ser recuperados pelos processos de verificação simples do Zabbix (poolers) através das  [[pt:manual:config:items:itemtypes:simple_checks:vmware_keys|chaves VMware]].

A partir do Zabbix 2.4.4 a coleta é dividida em dois tipos: dados de
configuração VMware e dados de performance VMware. Os dois tipos são
coletados de forma independente pelo *vmware collectors*. Por causa
disso é recomendável habilitar mais de um coletor para os serviços
VMware. De outra forma a recuperação das estatísticas dos coletores de
performance poderá ser atrasada pela coleta de configuração (que
normalmente demora um pouco para grandes ambientes).

Atualmente apenas as estatísticas de *datastore, interfaces de rede,
disco e contadores personalizados* são feitas pelo coletor de
performance do VMWare.

[comment]: # ({/new-4c2e8939})

[comment]: # ({new-68c3f7d5})
#### Configuração

Para a monitoração da máquina virtual funcionar o Zabbix precisa ser
[compilado](/pt/manual/installation/install#configure_the_sources) com
os parâmetros --with-libxml2 e --with-libcurl .

As seguintes opções de configuração podem ser utilizadas para otimizar o
monitoramento de máquinas virtuais:

|Opção|Valor|<|Descrição|
|-------|-----|-|-----------|
|<|Intervalo|Padrão|<|
|**StartVMwareCollectors**|0-250|0|Quantidade de instâncias pré-alocadas do *vmware collector*.<br>Este valor depende da quantiadde de serviços VMware que você precisa monitorar. Na maioria dos casos:<br>*servicenum < StartVMwareCollectors < (servicenum \* 2)*<br>onde *servicenum* é a quantidade de serviços VMware. Ex. se você tiver 1 serviço VMware a monitorar, configure **StartVMwareCollectors** para '2', se tiver 3 serviços VMware, defina como '5'.<br>Observe que na maiora dos casos este valor não deve ser menor que 2 e não deve ser 2 vezes maior que a quantidade de serviços VMware a se monitorar. Tenha em mente também que este valor também depende do tamanho do seu ambiente VMWare, da configuração presente em **VMwareFrequency** e **VMwarePerfFrequency**.|
|**VMwareCacheSize**|256K-2G|8M|Memória compartilhada para armazenar dados do VMWare.<br>Uma verificação interna do VMware zabbix\[vmware,buffer,...\] pode ser utilizada para acompanhar o uso de cache no VMWare (consulte [Verificações internas](/pt/manual/config/items/itemtypes/internal)).<br>Observe que a memória compartilahda não é alocada se não existirem instâncias do *vmware collector* configuradas para iniciar (ex. **StartVMwareCollectors=0**).|
|**VMwareFrequency**|10-86400|60|Atraso em segundos entre a coleta de dados de um serviço VMWare.<br>Este atraso pode ser definido para o menor período de atraso de um item VMware monitorado.|
|**VMwarePerfFrequency**|10-86400|60|Atraso em segundos entre a coleta de performance em um mesmo serviço VMWare.<br>Este atraso pode ser definido como o menor intervalo de coleta de um item de monitoração VMWare.<br>Disponível desde o Zabbix 2.2.9, 2.4.4|
|**VMwareTimeout**|1-300|10|Valor máximo em segundos que o 'vmware collectior' irá esperar pela resposta de um serviço VMWare (vCenter ou ESX hypervisor).<br>Disponível desde o Zabbix 2.2.9, 2.4.4|

[comment]: # ({/new-68c3f7d5})

[comment]: # ({new-e5966479})
#### Descoberta

O Zabbix pode usar as regras de LLD para descobrir automaticamente os
hipervisores e máquinas virtuais VMware.

![](../../assets/en/manual/vm_monitoring/vm_hypervisor_lld.png)

A chave da regra de descoberta na imagem acima é
*vmware.hv.discovery\[{$URL}\]*.

[comment]: # ({/new-e5966479})


[comment]: # ({new-2f026fa1})
#### Templates prontos para o uso

A configuração padrão do Zabbix vem com diversos templates prontos para
a monitoração do VMware vCenter ou ESX.

Estes tempaltes contêm regras de descobertas e algumas verificações para
monitorar instalações virtuais.

Observe que o template "*Template Virt VMware*" pode ser utilizado com o
VMware vCenter e ESX hypervisor. O template "*Template Virt VMware
Hypervisor*" e "*Template Virt VMware Guest*" podem ser utilizados para
descobertas e normalmente não estão associados ao host.

![](../../assets/en/manual/vm_monitoring/vm_templates.png)

::: noteclassic
Se o seu servidor for atualizado de uma versão pre-2.2 e não
tiver estes templates, você poderá adquirilos e importa-los a partir da
página de templates [oficiais da
comunidade](http://www.zabbix.org/wiki/Zabbix_Templates/Official_Templates).
De qualquer forma estse templates possuem dependências com os
mapeamentos de valores *VMware VirtualMachinePowerState* e *VMware
status*, então será necessário primeiro criar estes mapeamentos usando
um [script
SQL](https://www.zabbix.org/wiki/Zabbix_Templates/SQLs_for_Official_Templates),
manualmente ou importando um XML ou importando um XML) antes de importar
o template.
:::

[comment]: # ({/new-2f026fa1})

[comment]: # ({new-317ec2ff})
#### Configuração do Host

Para usar as verificações simples do VMware o host precisa ter ter as
seguintes macros definidas:

-   **{$URL}** - URL de serviço do VMware (vCenter ou ESX hypervisor)
    SDK URL (<https://servername/sdk>).
-   **{$USERNAME}** - Usuário para acesso ao serviço VMware
-   **{$PASSWORD}** - Senha para o serviço VMware

[comment]: # ({/new-317ec2ff})

[comment]: # ({new-96cbfb15})
#### Exemplo

O fluxo a seguir demonstra quão rápido pode ser configurar a monitoração
VMWare a partir do Zabbix:

-   compile o Zabbix Server com as opções necessárias (--with-libxml2 e
    --with-libcurl)
-   defina a opção **StartVMwareCollectors** no Zabbix Server para 1 ou
    mais
-   crie um novo host
-   crie as macros de host necessárias para autentiação VMWare:

```{=html}
<!-- -->
```
        {{..:..:assets:en:manual:vm_monitoring:vm_host_macros.png|}}
    * Associe o host com o tempalte //VMware service//: 
        {{..:..:assets:en:manual:vm_monitoring:vm_host_templates.png|}}
    * Clique no botão //Adicionar// logo abaixo do formulário do host

[comment]: # ({/new-96cbfb15})

[comment]: # ({new-44948bb1})
#### Monitoração expandida

A data coletada pelo *VMware collector* pode ser registrada para
detalhes de depuração usando o debug level 5. Este nível pode ser
definido em nos arquivos de configuração do
[servidor](/pt/manual/appendix/config/zabbix_server) e [do
proxy](/pt/manual/appendix/config/zabbix_proxy) ou usando os controles
em tempo de execução (`-R log_level_increase="vmware collector,N"`, onde
N é o número do processo). Os exemplos a seguir demonstram como a
extensão do nível de debug para o nível 4:

    Aumenta o nível de log de todos os coletores vmware:
    shell> zabbix_server -R log_level_increase="vmware collector"

    Aumenta o nível do segundo coletor VMWare:
    shell> zabbix_server -R log_level_increase="vmware collector,2"

Se a monitoração estendida do dados do coletor VMware não for mais
necessária pode-se parar usando `-R log_level_decrease`.

[comment]: # ({/new-44948bb1})


[comment]: # ({new-f6f9d3ef})
#### Troubleshooting

-   In case of unavailable metrics, please make sure if they are not
    made unavailable or turned off by default in recent VMware vSphere
    versions or if some limits are not placed on performance-metric
    database queries. See
    [ZBX-12094](https://support.zabbix.com/browse/ZBX-12094) for
    additional details.

```{=html}
<!-- -->
```
-   In case of *'config.vpxd.stats.maxQueryMetrics' is invalid or
    exceeds the maximum number of characters permitted\*\** error, add a
    `config.vpxd.stats.maxQueryMetrics` parameter to the vCenter Server
    settings. The value of this parameter should be the same as the
    value of `maxQuerysize` in VMware's *web.xml*. See this VMware
    knowledge base [article](https://kb.vmware.com/s/article/2107096)
    for details.

[comment]: # ({/new-f6f9d3ef})
