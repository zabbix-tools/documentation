[comment]: # translation:outdated

[comment]: # ({new-5c86be0b})
# 17. Interface web

[comment]: # ({/new-5c86be0b})

[comment]: # ({new-8e039e89})
#### Visão Geral

Para possibilitar um fácil acesso a partir de qualquer plataforma o
gerenciamento do Zabbix pode ser feito a partir de uma interface web e
navegadores.

::: noteclassic
Tentar o acesso à duas interfaces web do Zabbix no mesmo
host, mas em portas diferentes, poderá falhar. Ao logar na segunda
instância a sessão com a primeira poderá ser terminada.
:::

[comment]: # ({/new-8e039e89})

[comment]: # ({new-278be5f6})
### Frontend help

A help link ![](../../assets/en/manual/web_interface/help_link.png) is provided in Zabbix frontend forms with direct links to the corresponding parts of the documentation.

[comment]: # ({/new-278be5f6})
