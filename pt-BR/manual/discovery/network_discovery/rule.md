[comment]: # translation:outdated

[comment]: # ({new-bd6224b0})
# Configurando uma regra de descoberta de rede

[comment]: # ({/new-bd6224b0})

[comment]: # ({new-466f5982})
#### Visão geral

As regras de descoberta de rede são utilizadas pelo Zabbix para
descobrir hosts e serviços:

-   Acesse *Configuração → Descoberta*
-   Clicar em *Criar regra de descoberta* (ou no nome de uma regra já
    existente)
-   Edite as propriedades da regra

[comment]: # ({/new-466f5982})

[comment]: # ({new-f020a6f3})
#### Atributos da regra

![](../../../../assets/en/manual/discovery/network_discovery/d_rule.png)

|Parâmetro|Descrição|
|----------|-----------|
|*Nome*|Nome único da regra. Por exemplo, "Local network".|
|*Descoberto por proxy*|Quem executa a descoberta:<br>**nenhum proxy** - o Zabbix server fará a descoberta<br>**<proxy name>** - este servidor proxy fará a descoberta|
|*Intervalo de IPs*|Intervalo de IPs da descoberta. Pode usar um dos seguintes formatos:<br>IP único: 192.168.1.33<br>Intervalo de endereços IP: 192.168.1-10.1-255. O intervalo é limitado pela total de endereços coberto (menos que 64K).<br>Máscara de IP: 192.168.4.0/24<br>Suporte para máscara de IP:<br>/16 - /30 para endereços IPv4<br>/112 - /128 para endereços IPv6<br>Lista: 192.168.1.1-255, 192.168.2.1-100, 192.168.2.200, 192.168.4.0/24<br>Desde o Zabbix 3.0.0 este campo suporta espaços, tabulação e múltiplas linhas.|
|*Atraso (em segundos)*|Este parâmetro define a frequência de execução da regra.<br>O atraso é medido após o final da execução da última instância de descoberta, então não existe sobreposição.|
|*Checagens*|O Zabbix usa isso para definir os testes da descoberta.<br>Verificações suportadas: SSH, LDAP, SMTP, FTP, HTTP, HTTPS, POP, NNTP, IMAP, TCP, Telnet, Zabbix agent, SNMPv1 agent, SNMPv2 agent, SNMPv3 agent, ICMP ping.<br>A descoberta baseada em protocolosusa a funcionalidade **net.tcp.service\[\]** para testar cada host, exceto para verificações SNMP onde são consultados OIDs. O Zabbix Agent é testado com a solicitação do valor de um item. Consulte também o manual de [itens do agente](/pt/manual/config/items/itemtypes/zabbix_agent) para mais detalhes.<br>O parâmetro *Range de portas* pode ser um destes:<br>Porta única: 22<br>Intervalo de portas: 22-45<br>Lista: 22-45,55,60-70|
|*Critério de unicidade do dispositivo*|O critério de unicidade pode ser:<br>**Endereço IP** - o critério para o cadastro de um novo host será a existência ou não do endereço de IP na base do Zabbix.<br>**<resultado da verificacao de agente>** - o resultado de uma verificação SNMP ou Zabbix Agent.|
|*Ativo*|Marque esta opção para ativar a regra.<br>Se não estiver marcado, a regra não será executada.|

[comment]: # ({/new-f020a6f3})

[comment]: # ({new-cd84714a})
#### Mudando as configurações de proxy

Desde o Zabbix 2.2.0 os hosts descobertos por proxies diferentes sempre
serão tratados como hosts diferentes. Isso permite executar as regras de
descoberta em intervalos de IP iguais que estão em redes diferentes.
Entretanto, a mudança de um proxy para uma subnet já monitorada pode ser
complicada pois as modificações de proxy tem que ser alteradas em todos
os hosts descobertos.

Por exemplo, os passos para substituir um proxy em uma regra de
descoberta:

1.  desativar a regra de descoberta
2.  sincronizar a configuração do proxy
3.  substituir o proxy na regra de descoberta
4.  substituir o proxy em todos os hosts descobertos por esta regra
5.  ativar a regra de descoberta

[comment]: # ({/new-cd84714a})

[comment]: # ({new-78f4f491})
#### Um cenário real

Neste exemplo nós vamos configurar uma regra de descoberta para uma rede
local com o intervalo de IPs: 192.168.1.1-192.168.1.254.

No nosso cenário precisamos:

-   descobrir os hosts que tem um agente zabbix em execução
-   executar a regra a cada 10 minutos
-   adicionar o host na monitoração se ele estiver no ar a mais de 1
    hora
-   remover o host se ele não for visto a mais de 24 horas
-   adicionar servidores Linux no grupo "Linux servers"
-   adicionar servidores Windows no grupo "Windows servers"
-   associar os hosts Linux ao template *Template OS Linux*
-   associar os hosts Windows ao template *Template OS Windows*

[comment]: # ({/new-78f4f491})

[comment]: # ({new-b8e28285})
##### Passo 1

Definindo uma regra de descoberta de rede para nosso intervalo de IPs

![](../../../../assets/en/manual/discovery/network_discovery/discovery.png)

O Zabbix irá tentar descobrir os hosts dentro do intervalo de IPs
192.168.1.1-192.168.1.254 ao se conectar ao Zabbix Agente recuperar o
valor da chave de item **system.uname**. O valor recebido pelo agente
poderá ser utilizado para aplicar ações diferentes para sistemas
operacionais diferentes. Por exemplo, associar os servidores Windows ao
`Template OS Windows`, e os servidores Linux ao `Template OS Linux`.

A regra será executada a cada 10 minutos (600 segundos).

Quando a regra for adicionada, o Zabbix irá iniciar automaticamente e
gerar os eventos de descoberta para o processamento.

[comment]: # ({/new-b8e28285})

[comment]: # ({new-b1d8800e})
##### Passo 2

Definindo uma [ação](/pt/manual/config/notifications/action) para
adicionar os servidores Linux descobertos ao grupo e template correto.

![](../../../../assets/en/manual/discovery/network_discovery/disc_action_1.png)

A ação será ativada se:

-   o serviço "Zabbix agent" estiver "up"
-   o valor retornado por `system.uname` (a chave do Zabbix Agent será
    usada na definição da regra) contiver "Linux"
-   O tempo "no ar" for superior a 1 hora (3600 segundos) ou mais

![](../../../../assets/en/manual/discovery/network_discovery/disc_action_1b.png)

A ação vai executar as seguintes operações:

-   adicionar os hosts descobertos ao grupo "Linux servers" (e também
    vai adicionar o host se ele ainda não existir)
-   associar o host ao template "Template OS Linux". O Zabbix vai
    começar automaticamente a monitorar o host usando os itens e
    triggers definidos no template "Template OS Linux".

[comment]: # ({/new-b1d8800e})

[comment]: # ({new-79eefb1b})
##### Passo 3

Definindo uma ação para adicionar os servidores Windows em seus
respectivos grupos e templates.

![](../../../../assets/en/manual/discovery/network_discovery/disc_action_2.png)

![](../../../../assets/en/manual/discovery/network_discovery/disc_action_2b.png)

##### Passo 4

Definindo uma ação para remover os hosts "perdidos".

![](../../../../assets/en/manual/discovery/network_discovery/disc_action_3.png)

![](../../../../assets/en/manual/discovery/network_discovery/disc_action_3b.png)

O servidor será removido se o serviço "Zabbix agent" estiver fora do ar
por mais de 24 horas (86400 segundos).

[comment]: # ({/new-79eefb1b})
