[comment]: # translation:outdated

[comment]: # ({new-1cd8c123})
# 2 Discovery of network interfaces

In a similar way as [file
systems](/manual/discovery/low_level_discovery#configuring_low-level_discovery)
are discovered, it is possible to also discover network interfaces.

[comment]: # ({/new-1cd8c123})

[comment]: # ({new-da8cf30d})
#### Item key

The item key to use in the [discovery
rule](/manual/discovery/low_level_discovery#discovery_rule) is

    net.if.discovery

This item is supported since Zabbix agent 2.0.

[comment]: # ({/new-da8cf30d})

[comment]: # ({new-d68acdf2})
#### Supported macros

You may use the {\#IFNAME} macro in the discovery rule
[filter](/manual/discovery/low_level_discovery#filter) and prototypes of
items, triggers and graphs.

Examples of item prototypes that you might wish to create based on
"net.if.discovery":

-   "net.if.in\[{\#IFNAME},bytes\]",
-   "net.if.out\[{\#IFNAME},bytes\]".

Note that on Windows {\#IFGUID} is also returned.

[comment]: # ({/new-d68acdf2})
