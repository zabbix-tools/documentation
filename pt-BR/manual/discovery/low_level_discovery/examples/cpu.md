[comment]: # translation:outdated

[comment]: # ({new-c2b618f4})
# 3 Discovery of CPUs and CPU cores

In a similar way as [file
systems](/manual/discovery/low_level_discovery#configuring_low-level_discovery)
are discovered, it is possible to also discover CPUs and CPU cores.

[comment]: # ({/new-c2b618f4})

[comment]: # ({new-3494efeb})
#### Item key

The item key to use in the [discovery
rule](/manual/discovery/low_level_discovery#discovery_rule) is

    system.cpu.discovery

This item is supported since Zabbix agent 2.4.

[comment]: # ({/new-3494efeb})

[comment]: # ({new-33246748})
#### Supported macros

This discovery key returns two macros - {\#CPU.NUMBER} and
{\#CPU.STATUS} identifying the CPU order number and status respectively.
Note that a clear distinction cannot be made between actual, physical
processors, cores and hyperthreads. {\#CPU.STATUS} on Linux, UNIX and
BSD systems returns the status of the processor, which can be either
"online" or "offline". On Windows systems, this same macro may represent
a third value - "unknown" - which indicates that a processor has been
detected, but no information has been collected for it yet.

CPU discovery relies on the agent's collector process to remain
consistent with the data provided by the collector and save resources on
obtaining the data. This has the effect of this item key not working
with the test (-t) command line flag of the agent binary, which will
return a NOT\_SUPPORTED status and an accompanying message indicating
that the collector process has not been started.

Item prototypes that can be created based on CPU discovery include, for
example:

-   `system.cpu.util[{#CPU.NUMBER},<type>,<mode>]`
-   `system.hw.cpu[{#CPU.NUMBER},<info>]`

For detailed item key description, see [Zabbix agent item
keys](/manual/config/items/itemtypes/zabbix_agent#supported_item_keys).

[comment]: # ({/new-33246748})
