[comment]: # translation:outdated

[comment]: # ({new-5067cf1d})
# 1 Discovery of mounted filesystems

[comment]: # ({/new-5067cf1d})

[comment]: # ({new-fe14cbff})
#### Overview

It is possible to discover mounted filesystems and their properties
(mountpoint name, mountpoint type, filesystem size and inode
statistics).

To do that, you may use a combination of:

-   the `vfs.fs.get` agent item as the master item
-   dependent low-level discovery rule and item prototypes

[comment]: # ({/new-fe14cbff})

[comment]: # ({new-5d32b87c})
#### Configuration

[comment]: # ({/new-5d32b87c})

[comment]: # ({new-194f3d7a})
##### Master item

Create a Zabbix agent item using the following key:

    vfs.fs.get

![](../../../../../assets/en/manual/discovery/low_level_discovery/fs_get_item.png)

Set the type of information to "Text" for possibly big JSON data.

The data returned by this item will contain something like the following
for a mounted filesystem:

``` {.java}
{
  "fsname": "/",
  "fstype": "rootfs",
  "bytes": {
    "total": 1000,
    "free": 500,
    "used": 500,
    "pfree": 50.00,
    "pused": 50.00
  },
  "inodes": {
    "total": 1000,
    "free": 500,
    "used": 500,
    "pfree": 50.00,
    "pused": 50.00
  }
}
```

[comment]: # ({/new-194f3d7a})

[comment]: # ({new-c379fb62})
##### Dependent LLD rule

Create a low-level discovery rule as "Dependent item" type:

![](../../../../../assets/en/manual/discovery/low_level_discovery/fs_get_lld.png)

As master item select the `vfs.fs.get` item we created.

In the "LLD macros" tab define custom macros with the corresponding
JSONPath:

![](../../../../../assets/en/manual/discovery/low_level_discovery/fs_get_lld_b.png)

[comment]: # ({/new-c379fb62})

[comment]: # ({new-2011da1c})
##### Dependent item prototype

Create an item prototype with "Dependent item" type in this LLD rule. As
master item for this prototype select the `vfs.fs.get` item we created.

![](../../../../../assets/en/manual/discovery/low_level_discovery/fs_get_prototype.png)

Note the use of custom macros in the item prototype name and key:

-   *Name*: Free disk space on {\#FSNAME}, type: {\#FSTYPE}
-   *Key*: Free\[{\#FSNAME}\]

As type of information, use:

-   *Numeric (unsigned)* for metrics like 'free', 'total', 'used'
-   *Numeric (float)* for metrics like 'pfree', 'pused' (percentage)

In the item prototype "Preprocessing" tab select JSONPath and use the
following JSONPath expression as parameter:

    $.[?(@.fsname=='{#FSNAME}')].bytes.free.first()

![](../../../../../assets/en/manual/discovery/low_level_discovery/fs_get_prototype_b.png)

When discovery starts, one item per each mountpoint will be created.
This item will return the number of free bytes for the given mountpoint.

[comment]: # ({/new-2011da1c})
