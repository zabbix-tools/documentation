<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="pt-BR" datatype="plaintext" original="manual/discovery/low_level_discovery/examples/mounted_filesystems.md">
    <body>
      <trans-unit id="5067cf1d" xml:space="preserve">
        <source># 1 Discovery of mounted filesystems</source>
      </trans-unit>
      <trans-unit id="fe14cbff" xml:space="preserve">
        <source>#### Overview

It is possible to discover mounted filesystems and their properties:

-   mountpoint name 
-   mountpoint type 
-   filesystem size 
-   inode statistics
-   mount options

To do that, you may use a combination of:

-   the `vfs.fs.get` agent item as the master item
-   dependent low-level discovery rule and item prototypes</source>
      </trans-unit>
      <trans-unit id="5d32b87c" xml:space="preserve">
        <source>#### Configuration</source>
      </trans-unit>
      <trans-unit id="194f3d7a" xml:space="preserve">
        <source>##### Master item

Create a Zabbix agent item using the following key:

    vfs.fs.get

![](../../../../../assets/en/manual/discovery/low_level_discovery/fs_get_item.png)

Set the type of information to "Text" for possibly big JSON data.

The data returned by this item will contain something like the following
for a mounted filesystem:

``` {.java}
[
  {
    "fsname": "/",
    "fstype": "ext4",
    "bytes": {
      "total": 249405239296,
      "free": 24069537792,
      "used": 212595294208,
      "pfree": 10.170306,
      "pused": 89.829694
    },
    "inodes": {
      "total": 15532032,
      "free": 12656665,
      "used": 2875367,
      "pfree": 81.487503,
      "pused": 18.512497
    },
    "options": "rw,noatime,errors=remount-ro"
  }
]
```</source>
      </trans-unit>
      <trans-unit id="c379fb62" xml:space="preserve">
        <source>##### Dependent LLD rule

Create a low-level discovery rule as "Dependent item" type:

![](../../../../../assets/en/manual/discovery/low_level_discovery/fs_get_lld.png)

As master item select the `vfs.fs.get` item we created.

In the "LLD macros" tab define custom macros with the corresponding
JSONPath:

![](../../../../../assets/en/manual/discovery/low_level_discovery/fs_get_lld_b.png)

In the "Filters" tab you may add a regular expression that filters only **read-write** filesystems:

![](../../../../../assets/en/manual/discovery/low_level_discovery/fs_get_lld_c.png)</source>
      </trans-unit>
      <trans-unit id="2011da1c" xml:space="preserve">
        <source>##### Dependent item prototype

Create an item prototype with "Dependent item" type in this LLD rule. As
master item for this prototype select the `vfs.fs.get` item we created.

![](../../../../../assets/en/manual/discovery/low_level_discovery/fs_get_prototype.png)

Note the use of custom macros in the item prototype name and key:

-   *Name*: Free disk space on {\#FSNAME}, type: {\#FSTYPE}
-   *Key*: Free\[{\#FSNAME}\]

As type of information, use:

-   *Numeric (unsigned)* for metrics like 'free', 'total', 'used'
-   *Numeric (float)* for metrics like 'pfree', 'pused' (percentage)

In the item prototype "Preprocessing" tab select JSONPath and use the
following JSONPath expression as parameter:

    $.[?(@.fsname=='{#FSNAME}')].bytes.free.first()

![](../../../../../assets/en/manual/discovery/low_level_discovery/fs_get_prototype_b.png)

When discovery starts, one item per each mountpoint will be created.
This item will return the number of free bytes for the given mountpoint.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
