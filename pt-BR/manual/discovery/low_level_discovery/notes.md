[comment]: # translation:outdated

[comment]: # ({new-54ecc26e})
# Notas sobre o processo criação de aplicações a partir de LLD

[comment]: # ({/new-54ecc26e})

[comment]: # ({new-40e2ce9f})
#### Descoberta de aplicações

Os protótipos de aplicações suportam macros LLD.

Um protótipo de aplicação pode ser utilizado em diversos protótipos de
itens dentro da mesma regra.

Assim como outras entidades, as aplicações respeitam o tempo de vida
definido na regra de descoberta (configuração 'manter recursos perdidos
por' ) - eles serão excluídos após o recurso que originou a sua criação
não ter sido mais localizado por determinado tempo.

Se um protótipo de aplicação da regra de descoberta A descobrir uma
aplicação que já foi criada por esta mesma regra, isso causará erro.
Apenas protótipos de aplicações em diferentes regras de descoberta serão
aceitos.

[comment]: # ({/new-40e2ce9f})



[comment]: # ({new-071e9701})
#### Multiple LLD rules for the same item

Since Zabbix agent version 3.2 it is possible to define several
low-level discovery rules with the same discovery item.

To do that you need to define the Alias agent
[parameter](/manual/appendix/config/zabbix_agentd), allowing to use
altered discovery item keys in different discovery rules, for example
`vfs.fs.discovery[foo]`, `vfs.fs.discovery[bar]`, etc.

[comment]: # ({/new-071e9701})

[comment]: # ({new-a8d6d602})
#### Data limits for return values

There is no limit for low-level discovery rule JSON data if it is
received directly by Zabbix server, because return values are processed
without being stored in a database. There's also no limit for custom
low-level discovery rules, however, if it is intended to acquire custom
LLD data using a user parameter, then the user parameter return value
limit applies (512 KB).

If data has to go through Zabbix proxy it has to store this data in
database so [database
limits](/manual/config/items/item#text_data_limits) apply.

[comment]: # ({/new-a8d6d602})
