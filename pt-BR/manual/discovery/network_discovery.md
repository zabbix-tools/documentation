[comment]: # translation:outdated

[comment]: # ({new-57ae64ac})
# 1 Descoberta de redes

[comment]: # ({/new-57ae64ac})

[comment]: # ({new-2330e6f5})
#### Visão geral

O Zabbix oferta a funcionalidade de descoberta automática de rede de
forma flexível e eficiente.

Com este recurso você poderá:

-   acelerar a configuração dos hosts no Zabbix
-   simplificar a administração
-   usar o Zabbix para detectar modificações em tempo hábil sem uso
    excessivo de mão de obra administrativa

As regras de descoberta de rede no Zabbix são baseadas em até 4 grupos
de informação:

-   Ranges de IP
-   Disponibilidade de um serviço externo (FTP, SSH, WEB, POP3, IMAP,
    TCP, etc)
-   Informação recebida do Zabbix Agent
-   Informação recebida do SNMP Agent

Ele NÃO provê:

-   Descoberta da topologia da rede

A descoberta de rede pode ser definida em duas fases: descoberta e
ações.

[comment]: # ({/new-2330e6f5})

[comment]: # ({new-d3509504})
#### Descoberta

De forma periódica o Zabbix varre um range de IP definido na [regra de
descoberta de rede](/pt/manual/discovery/network_discovery/rule). O
intervalo entre verificações é configurável para cada regra de
descoberta.

Observe que uma regra de descoberta sempre utilizará um único processo
de descoberta no sistema operacional. O trabalho de varredura do range e
IPs não será dividido entre os múltiplos processos de descoberta.

Cada regra de descoberta possui um conjunto de verificações executadas
contra cada IP do range.

::: noteclassic
O processo de descoberta é verificado de forma independente
das outras verificações. Se alguma verificação não encontrar um serviço,
ou falhar, as outras verificações continuam sendo
executadas.
:::

Cada verificação de um serviço e de um host (IP) concluída pelo processo
de descoberta irá gerar um evento de descoberta.

|Evento|Verificação do resultado do serviço|
|------|--------------------------------------|
|*Serviço de descoberta*|O serviço está 'up' após ter estado 'down' ou foi descoberto pela primeira vez.|
|*Serviço Up*|O serviço está 'up', a mais de uma verificação consecutiva.|
|*Serviço Perdido*|O serviço está 'down' após ter estado down 'up'.|
|*Serviço Down*|O serviço está 'down', a mais de uma verificação consecutiva.|
|*Host Descoberto*|Pelo menos um serviço no host está 'up' após todos os serviços dele terem estado 'down'.|
|*Host Up*|Pelo menos um serviço do host está 'up', a mais de uma verificação consecutiva.|
|*Host Perdido*|Todos os serviços do host estão 'down' após pelo menos um ter estado 'up'.|
|*Host Down*|Todos os serviços do host estão 'down', a mais de uma verificação consecutiva.|

[comment]: # ({/new-d3509504})

[comment]: # ({new-4490d521})
#### Ações

Os eventos de descoberta podem iniciar importantes
[ações](/pt/manual/config/notifications/action), tais como:

-   Envio de notificações
-   Adicionar/Remover hosts
-   Ativar/Desativar hosts
-   Adicionar/Remover hosts a um grupo
-   Associar/Desassociar um host a um template
-   Executar comandos remotos

Estas ações podem ser configuradas respeitando o tipo de dispositivo,
IP, status, tempo up/down, etc. Para maiores detalhes sobre como
configurar ações para regras de descoberta de rede, veja os manuais de
[uso de ações](/pt/manual/config/notifications/action/operation) e
[condições de
ativação](/pt/manual/config/notifications/action/conditions).

[comment]: # ({/new-4490d521})

[comment]: # ({new-7c3012be})
##### Criação de host

Um host será adicionado se uma operação de *Adicionar Host* tiver sido
definida. Caso exista alguma definida que, para ser executada com
sucesso seja pré-requisito a existência do host, o mesmo também será
automaticamente adicionado. Qualquer das operações a seguir irá causar a
adição do host no caso de inexistência do mesmo:

-   habilitar host
-   desabilitar host
-   adicionar o host a um grupo
-   associar um template a um host

Quando os hosts são adicionados o nome do host será o 'hostname'
recebido através de consulta reversa, caso a consulta falhe o nome será
o endereço IP descoberto. A consulta pode ser feita pelo Zabbix Server
ou pelo Zabbix Proxy, dependendo de quem executou a regra de descoberta.
Se a consulta de nome falhar no proxy, ela repetida no Zabbix Server. Se
já existir um host com o mesmo nome o novo host receberá o apêndice
**\_2** ao final do nome (ou **\_3** se o **\_2** já existir e assim
sucessivamente).

Os hosts criados são adicionados ao grupo *Discovered hosts* (este é um
grupo padrão, é possível modificar esta configuração acessando
*Administração* → *Geral* →
*[Outros](/pt/manual/web_interface/frontend_sections/administration/general#other_parameters)*).
Se você quiser que o host esteja em outro grupo que não este basta
adicionar uma operação do tipo *Remover do grupo de hosts*
(especificando como grupo o "Discovered hosts") e também adicionar uma
operação *Adicionar ao grupo de hosts* (especificando outro grupo), isso
é necessário pois todo host deve estar em pelo menos um grupo.

[comment]: # ({/new-7c3012be})

[comment]: # ({new-cdaa96cf})
##### Remoção de hosts

A partir do Zabbix 2.4.0, hosts criados a partir de regras de descoberta
de rede, podem ser automaticamente excluídos se deixarem de ser
percebidos pelas regras de descoberta de rede. Os hosts são excluídos
automaticamente.

[comment]: # ({/new-cdaa96cf})

[comment]: # ({new-cc30860b})
##### Criação da interface para hosts descobertos

Quando os hosts são adicionados por resultado de uma regra de descoberta
de rede, suas interfaces serão criadas de acordo com as regras a seguir:

-   Se o serviço detectado a partir de uma verificação SNMP bem
    sucedida, uma interface SNMP será criada
-   Se o host responder ta um teste de SNMP e a uma chave do Zabbix
    Agent, será criada uma interface de cada tipo
-   Se o critério de unicidade for o resultado de uma consulta ao Zabbix
    Agent ou ao SNMP Agent, a interface padrão será do tipo da primeira
    chave verificada. Outros endereços IPs que responderem à mesma
    descoberta serão adicionados como interfaces adicionais.
-   Se um host responder apenas a consulta ao Zabbix Agent, será criada
    apenas a interface do tipo Zabbix Agent. Se ele passar a responder
    também à consulta SNMP, interface adicional será criada.
-   Se três hosts em separado já tiverem sido criados e forem
    descobertos a partir do critério de unicidade de "IP" e, em seguida,
    a regra de descoberta for modificada de forma que os três possuam o
    mesmo critério de unicidade, os endereços IP de B e C serão
    adicionados como interfaces em A, o primeiro host. Os hosts B e C
    permanecerão sem alterações. Em *Monitoramento → Descoberta* as
    interfaces adicionadas serão apresentadas na coluna "Dispositivo
    descoberto", com fonte em negrito e identada, mas a coluna "Host
    monitorado" irá apresentar somente o host A, o primeiro host criado.
    O tempo com o "Online/Offline" não será medido para os IPs
    adicionais pois eles serão considerados apenas como interfaces
    adicionais.

[comment]: # ({/new-cc30860b})



[comment]: # ({new-9b18af68})
##### Interface creation when adding hosts

When hosts are added as a result of network discovery, they get
interfaces created according to these rules:

-   the services detected - for example, if an SNMP check succeeded, an
    SNMP interface will be created
-   if a host responded both to Zabbix agent and SNMP requests, both
    types of interfaces will be created
-   if uniqueness criteria are Zabbix agent or SNMP-returned data, the
    first interface found for a host will be created as the default one.
    Other IP addresses will be added as additional interfaces.
-   if a host responded to agent checks only, it will be created with an
    agent interface only. If it would start responding to SNMP later,
    additional SNMP interfaces would be added.
-   if 3 separate hosts were initially created, having been discovered
    by the "IP" uniqueness criteria, and then the discovery rule is
    modified so that hosts A, B and C have identical uniqueness criteria
    result, B and C are created as additional interfaces for A, the
    first host. The individual hosts B and C remain. In *Monitoring →
    Discovery* the added interfaces will be displayed in the "Discovered
    device" column, in black font and indented, but the "Monitored host"
    column will only display A, the first created host.
    "Uptime/Downtime" is not measured for IPs that are considered to be
    additional interfaces.

[comment]: # ({/new-9b18af68})

[comment]: # ({new-883e6b92})
#### Changing proxy setting

The hosts discovered by different proxies are always treated as
different hosts. While this allows to perform discovery on matching IP
ranges used by different subnets, changing proxy for an already
monitored subnet is complicated because the proxy changes must be also
applied to all discovered hosts.

For example the steps to replace proxy in a discovery rule:

1.  disable discovery rule
2.  sync proxy configuration
3.  replace the proxy in the discovery rule
4.  replace the proxy for all hosts discovered by this rule
5.  enable discovery rule

[comment]: # ({/new-883e6b92})
