[comment]: # translation:outdated

[comment]: # ({new-1c2455b7})
# 2 Autorregistro do agente ativo

[comment]: # ({/new-1c2455b7})

[comment]: # ({new-81edbbe2})
#### Visão geral

É possível permitir que um agente ativo do Zabbix se auto registre, após
isso o servidor inicia automaticamente a sua monitoração. Desta forma os
novos hosts poderão ser adicionados para monitorar sem precisar de
configuração manual no servidor.

O processo de autorregistro pode ocorrer quando um agente até então
desconhecido requisita a lista de monitoração a executar.

Esta funcionalidade é muito útil para monitorar automaticamente novos
nós na nuvem. Assim que um novo nó e criado na nuvem o Zabbix passa a
monitorar automaticamente os dados de performance e disponibilidade do
novo host.

O autorregistro de um agente ativo também suporta a adição de hosts
monitorados da forma passiva. Quando o agente ativo requisita a lista de
verificações a executar ele fornece o seu 'ListenIP' e 'ListenPort',
conforme estiverem definidos em seu arquivo de configuração, estes dados
são enviados ao servidor. (Se existirem vários IPs o primeiro será
enviado ao servidor.)

No lado do servidor, quando adicionar o novo host autorregistrado,
utiliza o IP recebido e porta para configurar o agente. Se não for
fornecido um endereço IP será utilizado o ip de origem da conexão. Se
não for fornecida uma porta, será utilizada a porta 10050.

[comment]: # ({/new-81edbbe2})

[comment]: # ({new-5d32b87c})
#### Comfiguração

[comment]: # ({/new-5d32b87c})

[comment]: # ({new-bf3762f0})
##### Definir o servidor

Certifique-se que você tem o Zabbix Server identificado no [arquivo de
configuração do agente](/pt/manual/appendix/config/zabbix_agentd) -
zabbix\_agentd.conf

    ServerActive=10.0.0.1

Se você não tiver especificado o parâmetro *Hostname* no
`zabbix_agentd.conf`, o nome de sistema do agente será utilizado pelo
servidor para nomear o host. O nome do sistema no linux pode ser obtido
através do comando 'hostname'.

Reinicie o agente após modificar o seu arquivo de configuração.

[comment]: # ({/new-bf3762f0})

[comment]: # ({new-7b103292})
##### Ação para um registro de agente ativo

Quando um servidor recebe uma requisição de autorregistro ele chamará
uma [ação](/pt/manual/config/notifications/action). Uma ação com origem
de evento em "autorregistro" precisa ser configurada para permitir o
autorregistro.

::: noteclassic
Configurar uma [regra de descoberta de
rede](network_discovery) não é obrigatória para ter o autorregistro de
agentes ativos.
:::

Na interface web, acesse *Configuração → Ações*, selecione
*Autorregistro* como origem do evento e clique no botão *Criar ação*:

-   Na aba **Ação**, informe um nome para a ação
-   Na aba **Condições** defina quais condições serão necessárias para o
    autorregistro (opcional). Se você utilizar uma condição baseada nos
    metadados do host, consulte a seção a seguir.
-   Na aba **Operações** adicione as operações necessárias (por exemplo,
    'Adicionar host', 'Adicionar host a grupo' (por exemplo, *Discovered
    hosts*), 'associar com templates', etc.

::: notetip
Se os hosts se registraram automaticamente, então
provavelmente eles suportam a monitoração ativa (muito útil para hosts
que estão em subredes protegidas - DMZs) e pode ser necessário o uso de
um template específico para monitoração ativa como o
*Template\_Linux-active*.
:::

[comment]: # ({/new-7b103292})

[comment]: # ({new-66cac4ef})
#### Usando o metadado do host

Quando um agente envia um pedido de autorregistro ao servidor ele envia
junto o seu 'hostname'. Em alguns casos (por exemplos nós na nuvem da
Amazon) apenas o 'hostname' não será suficiente para o Zabbix Server
diferenciar os hosts descoebrtos. O metadado do hotst poderá conter
opcionalmente outras informações para identificar o agente junto ao
servidor.

O metadado do host é configurado do lado do agente no seu [arquivo de
configuração](/pt/manual/appendix/config/zabbix_agentd) -
zabbix\_agentd.conf. Existem dois parâmetros que podem ser utilizados
para definir o metadado do host:

    HostMetadata
    HostMetadataItem

Veja a descrição de ambos no link abaixo.

<note:important>Uma tentativa de autorregistro irá ocorrer toda
vez que um agente ativo enviar uma solicitação de atualização da lista
de itens a monitorar. O intervalo entre as requisições da lista de itens
a monitorar é definida pelo parâmetro
[RefreshActiveChecks](/pt/manual/appendix/config/zabbix_agentd) no
arquivo de configuração do agente. A primeira requisição é feita
imediatamente após o agente ser carregado.
:::

[comment]: # ({/new-66cac4ef})

[comment]: # ({new-9c275675})
##### Exemplo 1

Usando o metadado para dinstinguir hosts Linux e Windows.

Digamos que você queira que os hosts se autorregistrem no Zabbix Server.
Você tem agentes ativos (consulte a seção de "Configuração" acima) na
sua rede. Existem servidores com sistema operacional Windows e Linux e
você possui um template para monitorar cada um deles ( "Template OS
Linux" e "Template OS Windows"). Logo o registro de um host deverá ser
compatível com o template de monitoração a ser utilizado. Por padrão
apenas o nome do host será enviado para o Zabbix Server durante o
processo de autorregistro, mas para que seja possível a configuração do
tempalte correto será necessário também que o metadado seja enviado.

[comment]: # ({/new-9c275675})

[comment]: # ({new-af247ed6})
##### Configuração do agente

A primeira coisa a fazer é configurar os agentes, adicione a linha a
seguir em seus arquivos de configuração:

    HostMetadataItem=system.uname

Desta forma você terá certeza que o metadado do host contenha "Linux" ou
"Windows", dependendo de seu sistema operacional. Um exemplo de metadado
de host neste caso:

    Linux: Linux server3 3.2.0-4-686-pae #1 SMP Debian 3.2.41-2 i686 GNU/Linux
    Windows: Windows WIN-0PXGGSTYNHO 6.0.6001 Windows Server 2008 Service Pack 1 Intel IA-32

Não esqueça de reiniciar o agente após alterar sua configuração e antes
de testar os valores.

[comment]: # ({/new-af247ed6})

[comment]: # ({new-070aaeb4})
##### Configuração na interface web

Agora será necessário configurar a interface web. Crie duas ações, a
primeira::

-   Nome: Linux host autoregistration
-   Condições: Metadado do host como *Linux*
-   Operações: Associar aos templates: Template OS Linux

::: noteclassic
Você pode ignorar a operação de "Adicionar host" neste caso
pois a tarefa de "Associar host a template" irá fazer isso
automaticamente.
:::

A segunda ação:

-   Nome: Windows host autoregistration
-   Condições: Metadado do host como *Windows*
-   Operações: Associar aos templates: Template OS Windows

[comment]: # ({/new-070aaeb4})

[comment]: # ({new-9543f562})
##### Exemplo 2

Utilizando o metadado do host para evitar que hosts indesejados sejam
registrados.

[comment]: # ({/new-9543f562})

[comment]: # ({new-110dbc2b})
##### Configuração do agente

Adicione a linha a seguir no arquivo de configuração do agente:

    HostMetadata: Linux    21df83bf21bf0be663090bb8d4128558ab9b95fba66a6dbf834f8b91ae5e08ae

Com esta descrição queremos indicar que o sistema operacional é o
"Linux" e o resto do texto é uma chave secreta.

Não esqueça de reiniciar o agente após alterar seu arquivo de
configuração.

[comment]: # ({/new-110dbc2b})

[comment]: # ({new-85f695d6})
##### Configuração na interface web

Crie uma ação na interface web, usando uma condição que verifique se a
chave secreta está presente no metadado do host:

-   Name: Auto registration action Linux
-   Condições:

```{=html}
<!-- -->
```
        * Tipo do cálculo: E
        * Condição (A): Metadado do host como //Linux//
        * Condição (B): Metadado do host como //21df83bf21bf0be663090bb8d4128558ab9b95fba66a6dbf834f8b91ae5e08ae//
    * Operações: 
        * Enviar mensagem para os usuários: Admin através de todas as mídias
        * Adicionar hosts aos grupos: Linux servers
        * Associar com templates: Template OS Linux

Observe que este método sozinho não provê forte proteção pois os dados
são transmitidos em texto plano.

[comment]: # ({/new-85f695d6})





[comment]: # ({new-c5ffa817})
##### Frontend configuration

Create an action in the frontend, using some hard-to-guess secret code
to disallow unwanted hosts:

-   Name: Autoregistration action Linux
-   Conditions:

```{=html}
<!-- -->
```
        * Type of calculation: AND
        * Condition (A): Host metadata contains //Linux//
        * Condition (B): Host metadata contains //21df83bf21bf0be663090bb8d4128558ab9b95fba66a6dbf834f8b91ae5e08ae//
    * Operations: 
        * Send message to users: Admin via all media
        * Add to host groups: Linux servers
        * Link to templates: Linux

Please note that this method alone does not provide strong protection
because data is transmitted in plain text. Configuration cache reload is
required for changes to have an immediate effect.

[comment]: # ({/new-c5ffa817})

[comment]: # ({new-9fca761d})
##### Agent configuration

Add the next line to the agent configuration file:

    HostMetadata=Linux    21df83bf21bf0be663090bb8d4128558ab9b95fba66a6dbf834f8b91ae5e08ae

where "Linux" is a platform, and the rest of the string is the
hard-to-guess secret text.

Do not forget to restart the agent after making any changes to the
configuration file.

*Step 2*

It is possible to add additional monitoring for an already registered
host.

[comment]: # ({/new-9fca761d})

[comment]: # ({new-6eb51339})
##### Frontend configuration

Update the action in the frontend:

-   Name: Autoregistration action Linux
-   Conditions:

```{=html}
<!-- -->
```
        * Type of calculation: AND
        * Condition (A): Host metadata contains Linux
        * Condition (B): Host metadata contains 21df83bf21bf0be663090bb8d4128558ab9b95fba66a6dbf834f8b91ae5e08ae
    * Operations:
        * Send message to users: Admin via all media
        * Add to host groups: Linux servers
        * Link to templates: Linux
        * Link to templates: MySQL by Zabbix Agent

[comment]: # ({/new-6eb51339})

[comment]: # ({new-15c86816})
##### Agent configuration

Update the next line in the agent configuration file:

    HostMetadata=MySQL on Linux 21df83bf21bf0be663090bb8d4128558ab9b95fba66a6dbf834f8b91ae5e08ae

Do not forget to restart the agent after making any changes to the
configuration file.

[comment]: # ({/new-15c86816})
