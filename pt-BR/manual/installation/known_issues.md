[comment]: # translation:outdated

[comment]: # ({a72e64bf-97796c37})
# 8 Problemas conhecidos

[comment]: # ({/a72e64bf-97796c37})

[comment]: # ({new-e338613a})
#### Verificações IPMI

As verificações IPMI não irão funcionar com a biblioteca padrão do
Debian/Ubuntu. Para ajustar isso recompile a biblioteca OpenIPMI
habilitando o OpenSSL, conforme discutido em
[ZBX-6139](https://support.zabbix.com/browse/ZBX-6139).

[comment]: # ({/new-e338613a})


[comment]: # ({new-68c1ee03})
#### Verificações SSH

Algumas distribuições Linux como o Debian e Ubuntu não suportam chaves
privadas criptografadas (com frase) se a biblioteca libssh2 for
instalada a partir dos pacotes. Favor consultar
[ZBX-4850](https://support.zabbix.com/browse/ZBX-4850) para mais
detalhes.

[comment]: # ({/new-68c1ee03})

[comment]: # ({new-5c2a7ea7})
#### Verificações ODBC

O Zabbix Server ou Zabbix Proxy que utilizar o MySQL como seu banco de
dados poderá não funcionar corretamente com a biblioteca ODBC para MySQL
devido a um [BUG de envio de
dados](https://bugs.mysql.com/bug.php?id=73709). Favor consultar
[ZBX-7665](https://support.zabbix.com/browse/ZBX-7665) para mais
detalhes e alternativas disponíveis.

[comment]: # ({/new-5c2a7ea7})

[comment]: # ({new-40e33d04})
#### Verificações HTTPS

Cenários web utilizando o protocolo HTTPS e as verificações de agente
`​net.tcp.service[https...]`​ e `​net.tcp.service.perf[https...]`​
poderão falhar se o servidor de destino estiver configurado para não
permitir o protocolo TLS em versão v1.0 ou anterior. ​ Consulte
[ZBX-9879](https://​support.zabbix.com/​browse/​ZBX-9879) para maiores
informações sobre alternativas disponíveis.

[comment]: # ({/new-40e33d04})

[comment]: # ({new-40ea4656})

#### Possible deadlocks with MySQL/MariaDB

When running under high load, and with more than one LLD worker involved, it is possible to run into a deadlock caused by an InnoDB error related to the row-locking strategy (see [upstream bug](https://github.com/mysql/mysql-server/commit/7037a0bdc83196755a3bf3e935cfb3c0127715d5)). The error has been fixed in MySQL since 8.0.29, but not in MariaDB. For more details, see [ZBX-21506](https://support.zabbix.com/browse/ZBX-21506).

[comment]: # ({/new-40ea4656})

[comment]: # ({new-70c19e71})
#### Gráficos

Modificações no horário de verão (DST) poderão resultar em
irregularidades ao apresentar as legendas do eixo X (duplicação de
datas, datas ausentes, etc).

[comment]: # ({/new-70c19e71})



















[comment]: # ({cdb48237-215c95a7})

#### Intervalo do tipo Numeric (float) com PostgreSQL 11 ou anterior

PostgreSQL 11 e versões anteriores suportam valores de ponto flutuante apenas 
no intervalo aproximado de -1.34E-154 a 1.34E+154.

[comment]: # ({/cdb48237-215c95a7})









[comment]: # ({f415e6a4-dfc40df7})

#### NetBSD 8.0 e mais recente

Vários processos do Zabbix podem quebrar aleatoriamente na inicialização 
nas versões NetBSD 8.X e 9.X. Isto se deve ao valor padrão de stack ser 
muito pequeno (4MB), o qual deve ser aumentado executando:

    ulimit -s 10240

Para mais informações, por favor consulte o relatório de problema associado:
[ZBX-18275](https://support.zabbix.com/browse/ZBX-18275).

[comment]: # ({/f415e6a4-dfc40df7})

[comment]: # ({06ee5e8a-3cf04fe3})

#### Verificações IPMI

As verificações IPMI não funcionarão com o pacote de biblioteca 
padrão OpenIPMI no Debian anterior a 9 (stretch) e Ubuntu anterior 
a 16.04 (xenial). Para corrigir isto, recompile a biblioteca OpenIPMI 
com OpenSSL habilitado conforme discutido em [ZBX-6139](https://support.zabbix.com/browse/ZBX-6139).

[comment]: # ({/06ee5e8a-3cf04fe3})

[comment]: # ({32af55bb-8c5cdd23})

#### Verificações SSH

Algumas distribuições Linux como o Debian e Ubuntu não suportam
chaves privadas criptografadas (com senha) se a biblioteca libssh2 
tiver sido instalada a partir de pacotes. Por favor consulte [ZBX-4850](https://support.zabbix.com/browse/ZBX-4850) 
para mais detalhes.

Quando usando libssh 0.9.x no CentOS 8 com OpenSSH 8, verificações 
SSH podem ocasionalmente reportar "Impossível ler dados do servidor SSH" 
(Cannot read data from SSH server). Isto é causado por um [problema](https://gitlab.com/libssh/libssh-mirror/-/merge_requests/101)
([relatório mais detalhado](https://bugs.libssh.org/T231)) com a libssh. É esperado que o erro tenha 
sido corrigido pela publicação da versão estável libssh 0.9.5. Veja também 
[ZBX-17756](https://support.zabbix.com/browse/ZBX-17756) para detalhes.

[comment]: # ({/32af55bb-8c5cdd23})

[comment]: # ({c9767631-0c2fc2b9})

#### Verificações ODBC

-   O driver MySQL unixODBC não deve ser usado com Zabbix Server ou
    Zabbix Proxy compilados com biblioteca de conector MariaDB e 
    vice-versa. Se possível é também melhor evitar usar o mesmo conector
    que o driver devido um [bug de upstream](https://bugs.mysql.com/bug.php?id=73709). Configuração sugerida:

```{=html}
<!-- -->
```
    PostgreSQL, SQLite or Oracle connector → MariaDB or MySQL unixODBC driver
    MariaDB connector → MariaDB unixODBC driver
    MySQL connector → MySQL unixODBC driver

Por favor consulte [ZBX-7665](https://support.zabbix.com/browse/ZBX-7665) para mais informações e correções disponíveis.

-   Dados XML consultados a partir de Microsoft SQL Server podem ficar 
    truncados de várias formas nos sistemas Linux and UNIX.

```{=html}
<!-- -->
```
-   Tem sido observado que o uso de verificações ODBC no CentOS 8 para
    monitoramento de banco de dados Oracle utilizando Oracle Instant Client 
    para Linux 11.2.0.4.0 provoca quebra do Zabbix Server. Este problema
    pode ser resolvido pela atualização do Oracle Instant Client para 12.1.0.2.0, 
    12.2.0.1.0, 18.5.0.0.0 ou 19. Veja também [ZBX-18402](https://support.zabbix.com/browse/ZBX-18402).

[comment]: # ({/c9767631-0c2fc2b9})

[comment]: # ({c482c011-1db730d3})

#### Parâmetro método de requisição incorreto nos itens

O parâmetro método de requisição, usado apenas em verificações HTTP, 
pode estar incorretamente configurado como '1', um valor não-padrão para 
todos os itens, como resultado da atualização a partir de uma versão pré-4.0
do Zabbix. Para detalhes em como corrigir esta situação, consulte [ZBX-19308](https://support.zabbix.com/browse/ZBX-19308).

[comment]: # ({/c482c011-1db730d3})


[comment]: # ({5667a4a0-4713dff4})

#### Monitoramento Web e agente HTTP

O Zabbix Server causa vazamento de memória (memory leak) no CentOS 6, 
CentOS 7 e possivelmente outras distribuições Linux relacionadas devido a 
um [bug de upstream](https://bugzilla.redhat.com/show_bug.cgi?id=1057388) quando a opção "SSL verify peer" está habilitada nos 
cenários web ou agente HTTP. Por favor consulte [ZBX-10486](https://support.zabbix.com/browse/ZBX-10486) para mais 
informações e correções disponíveis.

[comment]: # ({/5667a4a0-4713dff4})

[comment]: # ({8ef41b3b-9cd7efe1})

#### Verificações simples

Há um bug nas versões de **fping** anteriores a v3.10 que gerencia 
incorretamente pacote de resposta duplicados. Isto pode causar 
resultados inesperados para itens `icmpping`, `icmppingloss` e
`icmppingsec`. É recomendado utilizar a versão mais recente do
**fping** disponível. Por favor consulte [ZBX-11726](https://support.zabbix.com/browse/ZBX-11726) para mais detalhes.

[comment]: # ({/8ef41b3b-9cd7efe1})

[comment]: # ({76e95fa0-a4574c73})

#### Verificações SNMP

Se o sistema operacional OpenBSD é utilizado, um bug de corrompimento
de memória (use-after-free) na biblioteca Net-SNMP até a versão 5.7.3 
pode causar quebra do Zabbix Server se o parâmetro SourceIP estiver
configurado no arquivo de configuração do Server. Como contingência, 
por favor não configure o parâmetro SourceIP. O mesmo problema se
aplica também ao Linux, mas ele não causa a parada do Zabbix Server.
Um pacote de correção local para o net-snmp no OpenBSD foi aplicado e 
será publicado com o OpenBSD 6.3.

[comment]: # ({/76e95fa0-a4574c73})

[comment]: # ({8dd00fcf-d699f9d6})

#### Picos nos dados SNMP

Picos nos dados SNMP têm sido observados e podem estar 
relacionados a certos fatores físicos como picos de tensão na 
rede. Consulte [ZBX-14318](https://support.zabbix.com/browse/ZBX-14318) para mais detalhes.

[comment]: # ({/8dd00fcf-d699f9d6})

[comment]: # ({0872574d-7aeb682d})

#### SNMP traps

O pacote "net-snmp-perl", necessário para SNMP traps, foi removido
no RHEL/CentOS 8.0-8.2; readicionado no RHEL 8.3.

Então se você estiver usando o RHEL 8.0-8.2, a melhor solução é atualizar
para RHEL 8.3; ou você pode utilizar um pacote do EPEL.

Por favor veja também o [ZBX-17192](https://support.zabbix.com/browse/ZBX-17192) para mais informações.

[comment]: # ({/0872574d-7aeb682d})

[comment]: # ({8bc58516-f46cb486})

#### Problema com processos de alerta no CentOS/RHEL 7

Problemas com instâncias do processo de alerta do Zabbix Server 
foram encontrados no CentOS/RHEL 7. Por favor consulte [ZBX-10461](https://support.zabbix.com/browse/ZBX-10461) 
para detalhes.

[comment]: # ({/8bc58516-f46cb486})

[comment]: # ({382c8943-8ffad918})

#### Compilando Zabbix Agent no HP-UX

Se você instalar a biblioteca PCRE de um site popular de pacotes 
para HP-UX <http://hpux.connect.org.uk>, por exemplo usando o 
arquivo `pcre-8.42-ia64_64-11.31.depot`, você obtém apenas a 
versão 64-bit da biblioteca instalada no diretório /usr/local/lib/hpux64.

Neste caso, para uma compilação bem-sucedida do Agent são 
necessárias opções customizadas no script de "configure", p.e.:

    CFLAGS="+DD64" ./configure --enable-agent --with-libpcre-include=/usr/local/include --with-libpcre-lib=/usr/local/lib/hpux64

[comment]: # ({/382c8943-8ffad918})

[comment]: # ({c79a4088-6e1fb8fe})

#### Variação de idiomas (locales) no Frontend

Foi observado que as localizações (idiomas) do Frontend podem
sofrer alteração sem lógica aparente, p.e. algumas páginas (ou 
partes delas) são mostradas em um idioma enquanto outras páginas 
(ou partes delas) são mostradas em um idioma diferente. 
Tipicamente o problema pode aparecer quanto há vários usuários, 
alguns dos quais utilizam um idioma, enquanto outros usam outro.

Uma contingência conhecida a este problema é desabilitar a função 
de multithreading no PHP e Apache.

O problema está relacionado a como as configurações de localização
funcionam [no PHP](https://www.php.net/manual/en/function.setlocale): informações de localização são mantidas por 
processo, não por thread. Assim, em um ambiente multi-thread, quando 
há vários projetos sendo executados pelo mesmo processo Apache, é 
possível que a localização seja modificada em outra thread e que isto 
afete como os dados podem ser processados na thread do Zabbix.

Para mais informações, por favor consulte os relatórios de problema 
relacionados:

-   [ZBX-10911](https://support.zabbix.com/browse/ZBX-10911) (Problema com troca de idiomas (locales) no Frontend)
-   [ZBX-16297](https://support.zabbix.com/browse/ZBX-16297) (Problema com processamento de número em gráficos
usando a função `bcdiv` das funções BC Math)

[comment]: # ({/c79a4088-6e1fb8fe})

[comment]: # ({75a5c0aa-81fe18ae})

#### Configuração opcache no PHP 7.3

Se "opcache" está habilitado na configuração do PHP 7.3, o Zabbix Frontend
pode mostrar uma tela em branco quando carregado pela primeira vez. Este é 
um [bug do PHP](https://bugs.php.net/bug.php?id=78015) registrado. Para contornar este problema, por favor configure 
o parâmetro "opcache.optimization\_level" para `0x7FFFBFDF` no arquivo de 
configuração do PHP (arquivo php.ini).

[comment]: # ({/75a5c0aa-81fe18ae})

[comment]: # ({cd27ecb2-4f3b73ce})

#### Gráficos

Mudanças no horário de verão (DST) resultam em irregularidades 
ao apresentar as legendas do eixo X (duplicação de datas, datas ausentes, etc).

[comment]: # ({/cd27ecb2-4f3b73ce})

[comment]: # ({4fcffe8b-357fdb5b})

#### Monitoramento de arquivo de log

Os itens `log[]` e `logrt[]` releem repetidamente o arquivo de log desde
o início se o sistema de arquivos estiver 100% ocupado e o arquivo de log 
estiver sendo anexado (veja [ZBX-10884](https://support.zabbix.com/browse/ZBX-10884) para mais informações).

[comment]: # ({/4fcffe8b-357fdb5b})

[comment]: # ({new-82ff58c2})
#### Slow MySQL queries

Zabbix server generates slow select queries in case of non-existing
values for items. This is caused by a known
[issue](https://bugs.mysql.com/bug.php?id=74602) in MySQL 5.6/5.7
versions. A workaround to this is disabling the
index\_condition\_pushdown optimizer in MySQL. For an extended
discussion, see
[ZBX-10652](https://support.zabbix.com/browse/ZBX-10652).

[comment]: # ({/new-82ff58c2})


[comment]: # ({new-7ed39efb})
#### Slow configuration sync with Oracle

Configuration sync might be slow in Zabbix 6.0 installations with Oracle DB that have high number of items and item preprocessing steps.
This is caused by the Oracle database engine speed processing *nclob* type fields.

To improve performance, you can convert the field types from *nclob* to *nvarchar2* by manually applying the database patch [items_nvarchar_prepare.sql](/../assets/en/manual/installation/items_nvarchar_prepare.sql).
Note that this conversion will reduce the maximum field size limit from 65535 bytes to 4000 bytes
for item preprocessing parameters and item parameters such as *Description*, Script item's field *Script*,
HTTP agent item's fields *Request body* and *Headers*, Database monitor item's field *SQL query*.
Queries to determine template names that need to be deleted before applying the patch are provided in the patch as a comment. Alternatively, if MAX_STRING_SIZE is set you can change *nvarchar2(4000)* to *nvarchar2(32767)* in the patch queries to set the 32767 bytes field size limit.

For an extended discussion, see [ZBX-22363](https://support.zabbix.com/browse/ZBX-22363).

[comment]: # ({/new-7ed39efb})

[comment]: # ({b30a2899-b393528e})

#### Login na API

Um grande número de sessões de usuários abertas pode ser criado
quando usando scripts customizados com o [método](/manual/api/reference/user/login) `user.login` 
sem um `user.logout` em seguida.

[comment]: # ({/b30a2899-b393528e})

[comment]: # ({2a226a6b-17c4463f})

#### Problems com IPv6 em SNMPv3 traps

Devido um bug no net-snmp, endereços IPv6 podem não ser corretamente
apresentados quando usando SNMPv3 nas SNMP traps. Para mais detalhes
e uma possível contingência, consulte [ZBX-14541](https://support.zabbix.com/browse/ZBX-14541).

[comment]: # ({/2a226a6b-17c4463f})

[comment]: # ({9a743886-d77627ce})

#### Endereço IPv6 encurtado em falha de login

Uma tentiva de login falha mostrará apenas os primeiros 39 caracteres
de um endereço IP armazenado pois este é o limite de caracteres no 
campo do banco de dados. Isto significa que endereços IPv6 com mais 
de 39 caracteres serão apresetados de forma incompleta.

[comment]: # ({/9a743886-d77627ce})

[comment]: # ({f0e36a0f-57420738})

#### Verificações do Zabbix Agent no Windows

Valores de DNS inexistentes configurados no parâmetro `Server` do 
arquivo de configuração do Zabbix Agent (zabbix\_agentd.conf) podem 
elevar o tempo de resposta do Zabbix Agent no Windows. Isto ocorre 
porque o serviço de cache de DNS do Windows não armazena respostas 
negativas para endereços IPv4. No entando, para endereços IPv6 as 
respostas negativas são armazenadas, então uma possível contingência 
para este problema seja desabilitar o protocolo IPv4 na máquina.

[comment]: # ({/f0e36a0f-57420738})

[comment]: # ({f2310323-40001075})

#### Exportar/importar YAML

Há alguns problemas conhecidos com [exportação/importação](/manual/xml_export_import) de YAML:

-   Mensagens de erro não são traduzíveis;
-   JSON válido com uma extensão de arquivo .yaml algumas vezes não pode ser importado;
-   Formatos de data amigáveis não quotados (aspas) são automaticamente convertidos para formato Unix.

[comment]: # ({/f2310323-40001075})

[comment]: # ({345c5b66-fcbf4bce})

#### Assitente de instalação no SUSE com NGINX e php-fpm

O assistente de instalação do Frontend não pode salvar o arquivo de configuração
no SUSE com NGINX + php-fpm. Isto é causado por uma configuração no serviço 
/usr/lib/systemd/system/php-fpm.service, que impede o Zabbix de gravar em /etc. 
(introduzido no [PHP 7.4](https://bugs.php.net/bug.php?id=72510)).

Há dois contornos disponíveis:

-   Configure a opção [ProtectSystem](https://www.freedesktop.org/software/systemd/man/systemd.exec.html#ProtectSystem=) para 'true' em vez de 'full' na unidade 
systemd do php-fpm.
-   Salve manualmente o arquivo /etc/zabbix/web/zabbix.conf.php.

[comment]: # ({/345c5b66-fcbf4bce})

[comment]: # ({ba1a34d4-8009b04b})

#### Chromium para web service do Zabbix no Ubuntu 20

Apesar de em muitos casos o Zabbix Web Server poder rodar com Chromium, 
no Ubuntu 20.04 o uso do Chromium causa o seguinte erro:

    Cannot fetch data: chrome failed to start:cmd_run.go:994:
    WARNING: cannot create user data directory: cannot create 
    "/var/lib/zabbix/snap/chromium/1564": mkdir /var/lib/zabbix: permission denied
    Sorry, home directories outside of /home are not currently supported. See https://forum.snapcraft.io/t/11209 for details.
    ----------------------------
    Impossível buscar dados: chrome falhou ao iniciar:cmd_run.go:994:
    ATENÇÃO: impossível criar diretório de dados do usuário: impossível criar
    "/var/lib/zabbix/snap/chromium/1564": mkdir /var/lib/zabbix: permissão negada
    Desculpe, diretórios de usuário fora de /home não são suportados atualmente. Consulte https://forum.snapcraft.io/t/11209 para detalhes.

Este erro ocorre porque `/var/lib/zabbix` é usado como diretório home para 
o usuário 'zabbix'.

[comment]: # ({/ba1a34d4-8009b04b})

[comment]: # ({14492c97-1f99c5d8})

#### Códigos de erro MySQL customizados

Se o Zabbix é usado com instalação do MySQL no Azure, uma mensagem
de erro pouco clara *\[9002\] Some errors occurred* pode aparecer nos logs do 
Zabbix. Este erro genérico é enviado ao Zabbix Server ou Proxy pelo banco
de dados. Para obter mais informações sobre a causa do erro, consulte os 
logs do Azure.

[comment]: # ({/14492c97-1f99c5d8})

[comment]: # ({932ad6a0-eb422070})

#### Expressões regulares inválidas após mudança para PCRE2 

No Zabbix 6.0 foi adicionado suporte a PCRE2. Mesmo que o PCRE ainda 
seja suportado, alguns pacotes de instalação do Zabbix (conforme listagem
abaixo) foram atualizados para usar PCRE2. Enquanto entrega muitos 
benefícios, a mudança para PCRE2 pode incorrer em padrões de regexp PCRE 
existentes se tornarem inválidos ou se comportarem de forma diferente. 

Em particular, isto afeta o padrão *\^[\\w-\\.]*. Para tornar esta regexp válida novamente
sem afetar a semântica, altere a expressão para *\^[-\\w\\.]* . Isto ocorre devido ao 
fato do PCRE2 tratar o sinal de traço como delimitador, criando um intervalo 
dentro de uma classe de caracter. 

Os seguintes pacotes de instalação do Zabbix foram atualizados e agora 
utilizam PCRE2:  
  - RHEL/CentOS 7 e mais recente, 
  - SLES (todas as versões), 
  - Debian 9 e mais recente, 
  - Ubuntu 16.04 e mais recente.

[comment]: # ({/932ad6a0-eb422070})

[comment]: # ({new-093b78e2})
#### Geomap widget error 

The maps in the Geomap widget may not load correctly, if you have upgraded from an older Zabbix version with NGINX and didn't switch to the new NGINX configuration file during the upgrade. 

To fix the issue, you can  discard the old configuration file, use the configuration file from the current version package and reconfigure it as described in the [download instructions](https://www.zabbix.com/download?zabbix=6.0&os_distribution=red_hat_enterprise_linux&os_version=8&db=mysql&ws=nginx) in section *e. Configure PHP for Zabbix frontend*.

Alternatively, you can manually edit an existing NGINX configuration file (typically, */etc/zabbix/nginx.conf*). To do so, open the file and locate the following block: 

    location ~ /(api\/|conf[^\.]|include|locale|vendor) {
            deny            all;
            return          404;
    }

Then, replace this block with: 

    location ~ /(api\/|conf[^\.]|include|locale) {
            deny            all;
            return          404;
    }

    location /vendor {
            deny            all;
            return          404;
    }


[comment]: # ({/new-093b78e2})
