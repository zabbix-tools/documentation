[comment]: # translation:outdated

[comment]: # ({f51c369a-e2c1904c})
# 4 Instalação via pacote

[comment]: # ({/f51c369a-e2c1904c})

[comment]: # ({000dbb20-8d855b7c})
#### From Zabbix official repository

Zabbix SIA fornece pacotes oficiais para RPM and DEB:

-   [Red Hat Enterprise
    Linux/CentOS](/manual/installation/install_from_packages/rhel_centos)
-   [Debian/Ubuntu/Raspbian](/manual/installation/install_from_packages/debian_ubuntu)
-   [SUSE Linux Enterprise
    Server](/manual/installation/install_from_packages/suse)

Pacotes para yum/dnf, apt e zypper e outras várias distribuições de sistemas operacionais estão disponíveis em
[repo.zabbix.com](https://repo.zabbix.com/).

Nota, embora algumas distribuições de sistema operacional (em particular distribuições baseadas em Debian) forneçam seus próprios pacotes, esses pacotes não são suportados pela Zabbix. 
Pacotes fornecidos por terceiros podem ser disponibilizados com atraso, não ter os recusos mais recentes e não conter correções de bugs.
Então é recomendado o uso somente de pacotes oficiais [repo.zabbix.com](https://repo.zabbix.com/). Se você já utilizou pacotes não oficiais, veja essa nota [upgrading Zabbix packages from OS repositories](/manual/installation/upgrade/packages#zabbix_packages_from_os_repositories).

[comment]: # ({/000dbb20-8d855b7c})
