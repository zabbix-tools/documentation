[comment]: # translation:outdated

[comment]: # ({1064d1bb-490a37c2})

# Atualização a partir de pacotes

[comment]: # ({/1064d1bb-490a37c2})

[comment]: # ({a554cbc2-0fa07e9f})
#### Visão geral

Esta seção provê os passos necessários para uma [atualização](/manual/installation/upgrade) bem-sucedida
utilizando os pacotes oficiais RPM e DEB fornecidos pela Zabbix para:

-   [Red Hat Enterprise Linux/CentOS](/manual/installation/upgrade/packages/rhel_centos)
-   [Debian/Ubuntu](/manual/installation/upgrade/packages/debian_ubuntu)

[comment]: # ({/a554cbc2-0fa07e9f})

[comment]: # ({9f835bad-69a2013d})

##### Pacotes do Zabbix nos repositórios de SO

Frequentemente, as distribuições de SO (em particular, distribuições 
baseadas em Debian) proveem seus próprios pacotes do Zabbix.\
Note que estes pacotes não são suportados pela Zabbix, eles são
tipicamente desatualizados e carecem de funcionalidades recentes
e correções de problemas. Apenas os pacotes do [repo.zabbix.com](https://repo.zabbix.com/)
são oficialmente suportados.

Se você estiver atualizando a partir de pacotes fornecidos pelas 
distribuições de SO (ou os tenha instalado em algum momento), 
siga este procedimento para alterar seu ambiente para os pacotes
oficiais da Zabbix:

1.  Sempre desinstale os pacotes antigos em primeiro lugar.
2.  Faça uma verificação por arquivos que possam ter sido
     deixados para trás depois da desinstalação.
4.  Instale os pacotes oficiais fornecidos pela Zabbix seguindo
    [as instruções de instalação](https://www.zabbix.com/download).

Nunca faça uma atualização direta, pois isso pode resultar em uma
instalação inutilizada.

[comment]: # ({/9f835bad-69a2013d})
