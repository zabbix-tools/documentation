[comment]: # translation:outdated

[comment]: # ({new-91aab3e5})
# 2 Debian/Ubuntu

[comment]: # ({/new-91aab3e5})

[comment]: # ({new-d02acea9})
#### Visão geral

Esta seção provê os passos necessários para uma [atualização](/manual/installation/upgrade) bem-sucedida
do Zabbix **5.4**.x para o Zabbix **6.0**.x usando os pacotes oficiais da Zabbix 
para Debian/Ubuntu.

Conquanto a atualização do Zabbix Agent não seja obrigatória (mas recomendada),
o Zabbix Server e Proxies devem estar na [mesma versão primária (major version)](/manual/appendix/compatibility). 
Deste modo, em um ambiente Server-Proxy, o Zabbix Server e todos os Proxies
precisam ser parados e atualizados. Manter os Proxies em execução durante a
atualização do Server não mais trará qualquer benefício, pois durante a atualização 
do Proxy seus dados antigos serão descartados e nenhum novo dado será reunido
até que a configuração do Proxy seja sincronizada com o Server.

Note que com banco de dados SQLite nos Proxies, dados históricos de antes da 
atualização serão perdidos, porque a atualização do SQLite não é suportada e o 
arquivo de banco de dados tem de ser removido manualmente. Quando o Proxy 
for iniciado pela primeira vez e o arquivo de banco de dados do SQLite estiver 
ausente, o Proxy o criará automaticamente.

Dependendo do tamanho do banco de dados a atualização para a versão 6.0
pode levar um longo tempo.

::: notewarning
Antes da atualização certifique-se de ler as **notas de atualização** relevantes!
:::

As seguintes notas de atualização estão disponíveis:

|Atualização de|Leia as notas de atualização completas|Mudanças mais importantes entre as versões|
|------------|-----------------------|---------------------------------------|
|5.4.x|Para:<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|<|
|5.2.x|Para:<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|Versões mínimas de banco de dados exigidas elevadas;<br>Itens agregados removidos como tipo separado.|
|5.0.x|Para:<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|Versão mínima requerida para PHP elevada de 7.2.0 para 7.2.5.|
|4.4.x|Para:<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)|Suporte a IBM DB2 retirado;<br>Versão mínima requerida para PHP elevada de 5.4.0 para 7.2.0;<br>Versões mínimas de banco de dados exigidas elevadas;<br>Alterado diretório de arquivos PHP do Zabbix.|
|4.2.x|Para:<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.4/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|Jabber, Ez Texting media types removidos.|
|4.0.x LTS|Para:<br>Zabbix [4.2](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_420)<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.4/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|Proxies mais antigos não podem mais enviar dados para um Server atualizado;<br>Agents mais novos não podem mais trabalhar com um Zabbix Server mais antigo.|
|3.4.x|Para:<br>Zabbix [4.0](https://www.zabbix.com/documentation/4.0/manual/installation/upgrade_notes_400)<br>Zabbix [4.2](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_420)<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.4/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|Bibliotecas 'libpthread' e 'zlib' agora obrigatórias;<br>Suporte ao protocolo de texto plano removido e cabeçalho é mandatório;<br>Versões Pré-1.4 dos Zabbix Agents não são mais suportadas;<br>Parâmetro Server na configuração de Proxy passivo agora obrigatória.|
|3.2.x|Para:<br>Zabbix [3.4](https://www.zabbix.com/documentation/3.4/manual/installation/upgrade_notes_340)<br>Zabbix [4.0](https://www.zabbix.com/documentation/4.0/manual/installation/upgrade_notes_400)<br>Zabbix [4.2](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_420)<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.4/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|Suporte a SQLite como banco de dados removido para Zabbix Server/Frontend;<br>Expressões Regulares compatíveis com Perl (PCRE) suportadas em vez do POSIX estendido;<br>Bibliotecas 'libpcre' e 'libevent' obrigatórias para Zabbix Server;<br>Códigos de saída adicionados para parâmetros de usuário, comandos remotos e itens system.run\[\] sem o marcador 'nowait' assim como scripts executados do Zabbix Server;<br>Zabbix Java Gateway tem de ser atualizado para suportar novas funcionalidades.|
|3.0.x LTS|Para:<br>Zabbix [3.2](https://www.zabbix.com/documentation/3.2/manual/installation/upgrade_notes_320)<br>Zabbix [3.4](https://www.zabbix.com/documentation/3.4/manual/installation/upgrade_notes_340)<br>Zabbix [4.0](https://www.zabbix.com/documentation/4.0/manual/installation/upgrade_notes_400)<br>Zabbix [4.2](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_420)<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.4/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|Atualização do banco de dados por ser lenta, dependendo do tamanho da tabela de histórico.|
|2.4.x|Para:<br>Zabbix [3.0](https://www.zabbix.com/documentation/3.0/manual/installation/upgrade_notes_300)<br>Zabbix [3.2](https://www.zabbix.com/documentation/3.2/manual/installation/upgrade_notes_320)<br>Zabbix [3.4](https://www.zabbix.com/documentation/3.4/manual/installation/upgrade_notes_340)<br>Zabbix [4.0](https://www.zabbix.com/documentation/4.0/manual/installation/upgrade_notes_400)<br>Zabbix [4.2](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_420)<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|Versão mínima requerida para PHP elevada de 5.3.0 para 5.4.0<br>Parâmetro LogFile do Agent deve ser especificado|
|2.2.x LTS|Para:<br>Zabbix [2.4](https://www.zabbix.com/documentation/2.4/manual/installation/upgrade_notes_240)<br>Zabbix [3.0](https://www.zabbix.com/documentation/3.0/manual/installation/upgrade_notes_300)<br>Zabbix [3.2](https://www.zabbix.com/documentation/3.2/manual/installation/upgrade_notes_320)<br>Zabbix [3.4](https://www.zabbix.com/documentation/3.4/manual/installation/upgrade_notes_340)<br>Zabbix [4.0](https://www.zabbix.com/documentation/4.0/manual/installation/upgrade_notes_400)<br>Zabbix [4.2](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_420)<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|Monitoramento distribuído baseado em Node removido|
|2.0.x|Para:<br>Zabbix [2.2](https://www.zabbix.com/documentation/2.2/manual/installation/upgrade_notes_220)<br>Zabbix [2.4](https://www.zabbix.com/documentation/2.4/manual/installation/upgrade_notes_240)<br>Zabbix [3.0](https://www.zabbix.com/documentation/3.0/manual/installation/upgrade_notes_300)<br>Zabbix [3.2](https://www.zabbix.com/documentation/3.2/manual/installation/upgrade_notes_320)<br>Zabbix [3.4](https://www.zabbix.com/documentation/3.4/manual/installation/upgrade_notes_340)<br>Zabbix [4.0](https://www.zabbix.com/documentation/4.0/manual/installation/upgrade_notes_400)<br>Zabbix [4.2](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_420)<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|Versão mínima requerida para PHP elevada de 5.1.6 para 5.3.0;<br>Banco de dados MySQL Case-sensitive requerido para correto funcionamento do Server; Codificação utf8 e utf8\_bin necessária para Zabbix Server funcionar corretamente com MySQL. Veja [criação de scripts de banco de dados](/manual/appendix/install/db_scripts#mysql).<br>Extensão 'mysqli' do PHP necessária em vez de 'mysql'|

Também pode ser de seu interesse verificar os [requisitos](/manual/installation/requirements) para a versão 6.0.

::: notetip
Pode ser útil executar duas sessões SSH paralelas durante a atualização,
executando os passos de atualização em uma e monitorando os logs do
Server/Proxy em outra. Por exemplo, a execução do comando 
`tail -f zabbix_server.log` ou `tail -f zabbix_proxy.log` na 
segunda sessão SSH lhe mostrará os últimos registros do arquivo de log
conforme eles forem sendo registrados, possibilitando a verificação de
possíveis erros em tempo real. Isto pode ser crítico para ambientes de 
produção.
:::


[comment]: # ({/new-d02acea9})

[comment]: # ({244bb190-93ff8b03})
#### Procedimento de atualização

[comment]: # ({/244bb190-93ff8b03})

[comment]: # ({1b9b8a99-f8102233})
##### 1 Pare os processos do Zabbix

Pare o Zabbix Server para certificar-se de que nenhum novo dado
está sendo gravado no banco de dados.

    # systemctl stop zabbix-server

Se atualizando o Zabbix Proxy, pare-o também.

    # systemctl stop zabbix-proxy

::: noteimportant
Não é mais possível iniciar um Zabbix Server atualizado e ter
instâncias do Zabbix Proxy mais antigas, ainda não atualizadas
reportando dados para este novo Server. Esta abordagem, que
nunca foi suportada e nem recomendada pela Zabbix, agora
está oficialmente desabilitada. O Zabbix Server irá ignorar os 
dados enviados de Proxies não atualizados.
:::

[comment]: # ({/1b9b8a99-f8102233})

[comment]: # ({6b944402-ab13a6a4})

##### 2 Faça backup do banco de dados Zabbix

Este é um passo muito importante. Certifique-se de que você tem um
backup do seu banco de dados. Ele o ajudará se o procedimento de 
atualização falhar (falta de espaço em disco, desligamento forçado,
ou qualquer outro problema inesperado).

[comment]: # ({/6b944402-ab13a6a4})

[comment]: # ({430cc7d0-6c141723})

##### 3 Backup dos arquivos de configuração, arquivos PHP e binários do Zabbix

Faça uma cópia de backup dos binários do Zabbix, arquivos de configuração
e do diretório de arquivos PHP.

Arquivos de configuração:

    # mkdir /opt/zabbix-backup/
    # cp /etc/zabbix/zabbix_server.conf /opt/zabbix-backup/
    # cp /etc/apache2/conf-enabled/zabbix.conf /opt/zabbix-backup/

Arquivos PHP e binários do Zabbix:

    # cp -R /usr/share/zabbix/ /opt/zabbix-backup/
    # cp -R /usr/share/doc/zabbix-* /opt/zabbix-backup/

[comment]: # ({/430cc7d0-6c141723})

[comment]: # ({new-31c75b55})
##### 4 Atualize o pacote de configuração de repositório

Para prosseguir com a atualização seu pacote de repositório atual tem
de ser desinstalado.

    # rm -Rf /etc/apt/sources.list.d/zabbix.list

Então instale o novo pacote de configuração de repositório.

No **Debian 10** execute:

    # wget https://repo.zabbix.com/zabbix/6.0/debian/pool/main/z/zabbix-release/zabbix-release_6.0-1+debian10_all.deb
    # dpkg -i zabbix-release_5.4-1+debian10_all.deb

No **Debian 9** execute:

    # wget https://repo.zabbix.com/zabbix/6.0/debian/pool/main/z/zabbix-release/zabbix-release_6.0-1+debian9_all.deb
    # dpkg -i zabbix-release_5.4-1+debian9_all.deb

No **Debian 8** execute:

    # wget https://repo.zabbix.com/zabbix/6.0/debian/pool/main/z/zabbix-release/zabbix-release_6.0-1+debian8_all.deb
    # dpkg -i zabbix-release_5.4-1+debian8_all.deb

No **Ubuntu 20.04** execute:

    # wget https://repo.zabbix.com/zabbix/5.4/ubuntu/pool/main/z/zabbix-release/zabbix-release_6.0-1+ubuntu20.04_all.deb
    # dpkg -i zabbix-release_5.4-1+ubuntu20.04_all.deb

No **Ubuntu 18.04** execute:

    # wget https://repo.zabbix.com/zabbix/5.4/ubuntu/pool/main/z/zabbix-release/zabbix-release_6.0-1+ubuntu18.04_all.deb
    # dpkg -i zabbix-release_6.0-1+ubuntu18.04_all.deb

No **Ubuntu 16.04** execute:

    # wget https://repo.zabbix.com/zabbix/5.4/ubuntu/pool/main/z/zabbix-release/zabbix-release_6.0-1+ubuntu16.04_all.deb
    # dpkg -i zabbix-release_5.4-1+ubuntu16.04_all.deb

No **Ubuntu 14.04** execute:

    # wget https://repo.zabbix.com/zabbix/5.4/ubuntu/pool/main/z/zabbix-release/zabbix-release_6.0-1+ubuntu14.04_all.deb
    # dpkg -i zabbix-release_6.0-1+ubuntu14.04_all.deb

Atualize as informações do repositório.

    # apt-get update



[comment]: # ({/new-31c75b55})

[comment]: # ({6c888ac5-08c7383c})
##### 5 Atualize os componentes do Zabbix

Para atualizar os componentes do Zabbix você deve executar
algo como:

    # apt-get install --only-upgrade zabbix-server-mysql zabbix-frontend-php zabbix-agent

Se usando PostgreSQL, substitua `mysql` por `pgsql` no comando. Se
atualizando o Proxy, substitua `server` por `proxy` no comando. Se
atualizando o Agent 2, substitua `zabbix-agent` por `zabbix-agent2` no
comando.

Para atualizar corretamente o Zabbix Frontend com Apache, execute também:

    # apt-get install zabbix-apache-conf

Distributions **prior to Debian 10 (buster) / Ubuntu 18.04 (bionic) /
Raspbian 10 (buster)** do not provide PHP 7.2 or newer, which is
required for Zabbix frontend 5.0. See
[information](/manual/installation/frontend/frontend_on_rhel7) about
installing Zabbix frontend on older distributions.

[comment]: # ({/6c888ac5-08c7383c})

[comment]: # ({new-455f4e97})

##### 6 Revise os parâmetros de configuração dos componentes

Consulte as notas de atualização para detalhes sobre 
[alterações obrigatórias](/manual/installation/upgrade_notes_600#configuration_parameters) (caso existam).

Para novos parâmetros opcionais, veja a seção [O que há de novo](/manual/introduction/whatsnew600#configuration_parameters).

[comment]: # ({/new-455f4e97})

[comment]: # ({e36f574d-624a8fc7})

##### 7 Inicie os processos do Zabbix

Inicie os componentes do Zabbix atualizados.

    # service zabbix-server start
    # service zabbix-proxy start
    # service zabbix-agent start
    # service zabbix-agent2 start

[comment]: # ({/e36f574d-624a8fc7})

[comment]: # ({9872b20f-49e4f43e})

##### 8 Limpe os cookies e cache do navegador

Após a atualização pode ser necessário limpar os cookies e o cache do 
navegador para que a interface web do Zabbix funcione corretamente.

[comment]: # ({/9872b20f-49e4f43e})

[comment]: # ({new-9bab02f2})
#### Atualização entre versão secundárias (minor versions)

É possível atualizar entre versões secundárias do Zabbix 6.0.x (por exemplo,
de 6.0.1 para 6.0.3). A atualização entre versões secundárias é fácil.

Para proceder com a atualização de versão secundária do Zabbix é necessário 
executar:

    $ sudo apt install --only-upgrade 'zabbix.*'

Para atualizar a versão secundária do Zabbix Server, execute:

    $ sudo apt install --only-upgrade 'zabbix-server.*'

Para atualizar a versão secundária do Zabbix Agent, utilize:

    $ sudo apt install --only-upgrade 'zabbix-agent.*'

ou, para Zabbix Agent 2:

    $ sudo apt install --only-upgrade 'zabbix-agent2.*'

[comment]: # ({/new-9bab02f2})
