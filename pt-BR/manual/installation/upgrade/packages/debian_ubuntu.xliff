<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="pt-BR" datatype="plaintext" original="manual/installation/upgrade/packages/debian_ubuntu.md">
    <body>
      <trans-unit id="91aab3e5" xml:space="preserve">
        <source># 2 Debian/Ubuntu</source>
      </trans-unit>
      <trans-unit id="d02acea9" xml:space="preserve">
        <source>#### Overview

This section provides the steps required for a successful
[upgrade](/manual/installation/upgrade) from Zabbix **6.4**.x to Zabbix
**7.0**.x using official Zabbix packages for Debian/Ubuntu.

::: notewarning
Before the upgrade make sure to read the relevant
[**upgrade notes**](/manual/installation/upgrade)!
:::

You may also want to check the
[requirements](/manual/installation/requirements) for 7.0.

::: notetip
It may be handy to run two parallel SSH sessions during
the upgrade, executing the upgrade steps in one and monitoring the
server/proxy logs in another. For example, run
`tail -f zabbix_server.log` or `tail -f zabbix_proxy.log` in the second
SSH session showing you the latest log file entries and possible errors
in real time. This can be critical for production
instances.
:::</source>
      </trans-unit>
      <trans-unit id="93ff8b03" xml:space="preserve">
        <source>#### Upgrade procedure</source>
        <target state="needs-translation">#### Procedimento de atualização</target>
      </trans-unit>
      <trans-unit id="f8102233" xml:space="preserve">
        <source>##### 1 Stop Zabbix processes

Stop Zabbix server to make sure that no new data is inserted into
database.

    service zabbix-server stop

If upgrading Zabbix proxy, stop proxy too.

    service zabbix-proxy stop</source>
        <target state="needs-translation">##### 1 Pare os processos do Zabbix

Pare o Zabbix Server para certificar-se de que nenhum novo dado
está sendo gravado no banco de dados.

    # systemctl stop zabbix-server

Se atualizando o Zabbix Proxy, pare-o também.

    # systemctl stop zabbix-proxy

::: noteimportant
Não é mais possível iniciar um Zabbix Server atualizado e ter
instâncias do Zabbix Proxy mais antigas, ainda não atualizadas
reportando dados para este novo Server. Esta abordagem, que
nunca foi suportada e nem recomendada pela Zabbix, agora
está oficialmente desabilitada. O Zabbix Server irá ignorar os 
dados enviados de Proxies não atualizados.
:::</target>
      </trans-unit>
      <trans-unit id="ab13a6a4" xml:space="preserve">
        <source>##### 2 Back up the existing Zabbix database

This is a very important step. Make sure that you have a backup of your
database. It will help if the upgrade procedure fails (lack of disk
space, power off, any unexpected problem).</source>
        <target state="needs-translation">
##### 2 Faça backup do banco de dados Zabbix

Este é um passo muito importante. Certifique-se de que você tem um
backup do seu banco de dados. Ele o ajudará se o procedimento de 
atualização falhar (falta de espaço em disco, desligamento forçado,
ou qualquer outro problema inesperado).</target>
      </trans-unit>
      <trans-unit id="6c141723" xml:space="preserve">
        <source>##### 3 Back up configuration files, PHP files and Zabbix binaries

Make a backup copy of Zabbix binaries, configuration files and the PHP
file directory.

Configuration files:

    mkdir /opt/zabbix-backup/
    cp /etc/zabbix/zabbix_server.conf /opt/zabbix-backup/
    cp /etc/apache2/conf-enabled/zabbix.conf /opt/zabbix-backup/

PHP files and Zabbix binaries:

    cp -R /usr/share/zabbix/ /opt/zabbix-backup/
    cp -R /usr/share/zabbix-* /opt/zabbix-backup/</source>
        <target state="needs-translation">
##### 3 Backup dos arquivos de configuração, arquivos PHP e binários do Zabbix

Faça uma cópia de backup dos binários do Zabbix, arquivos de configuração
e do diretório de arquivos PHP.

Arquivos de configuração:

    # mkdir /opt/zabbix-backup/
    # cp /etc/zabbix/zabbix_server.conf /opt/zabbix-backup/
    # cp /etc/apache2/conf-enabled/zabbix.conf /opt/zabbix-backup/

Arquivos PHP e binários do Zabbix:

    # cp -R /usr/share/zabbix/ /opt/zabbix-backup/
    # cp -R /usr/share/doc/zabbix-* /opt/zabbix-backup/</target>
      </trans-unit>
      <trans-unit id="31c75b55" xml:space="preserve">
        <source>##### 4 Update repository configuration package

To proceed with the update your current repository package has to be
uninstalled.

    rm -Rf /etc/apt/sources.list.d/zabbix.list

Then install the new repository configuration package.

On **Debian 12** run:

    wget https://repo.zabbix.com/zabbix/6.5/debian/pool/main/z/zabbix-release/zabbix-release_6.5-1+debian12_all.deb
    dpkg -i zabbix-release_6.5-1+debian12_all.deb

On **Debian 11** run:

    wget https://repo.zabbix.com/zabbix/6.5/debian/pool/main/z/zabbix-release/zabbix-release_6.5-1+debian11_all.deb
    dpkg -i zabbix-release_6.5-1+debian11_all.deb

On **Debian 10** run:

    wget https://repo.zabbix.com/zabbix/6.5/debian/pool/main/z/zabbix-release/zabbix-release_6.5-1+debian10_all.deb
    dpkg -i zabbix-release_6.5-1+debian10_all.deb

On **Debian 9** run:

    wget https://repo.zabbix.com/zabbix/6.5/debian/pool/main/z/zabbix-release/zabbix-release_6.5-1+debian9_all.deb
    dpkg -i zabbix-release_6.5-1+debian9_all.deb

On **Ubuntu 22.04** run:

    wget https://repo.zabbix.com/zabbix/6.5/ubuntu/pool/main/z/zabbix-release/zabbix-release_6.5-1+ubuntu22.04_all.deb
    dpkg -i zabbix-release_6.5-1+ubuntu22.04_all.deb

On **Ubuntu 20.04** run:

    wget https://repo.zabbix.com/zabbix/6.5/ubuntu/pool/main/z/zabbix-release/zabbix-release_6.5-1+ubuntu20.04_all.deb
    dpkg -i zabbix-release_6.5-1+ubuntu20.04_all.deb

On **Ubuntu 18.04** run:

    wget https://repo.zabbix.com/zabbix/6.5/ubuntu/pool/main/z/zabbix-release/zabbix-release_6.5-1+ubuntu18.04_all.deb
    dpkg -i zabbix-release_6.5-1+ubuntu18.04_all.deb

Update the repository information.

    apt-get update</source>
      </trans-unit>
      <trans-unit id="08c7383c" xml:space="preserve">
        <source>##### 5 Upgrade Zabbix components

To upgrade Zabbix components you may run something like:

    apt-get install --only-upgrade zabbix-server-mysql zabbix-frontend-php zabbix-agent

If using PostgreSQL, substitute `mysql` with `pgsql` in the command. If
upgrading the proxy, substitute `server` with `proxy` in the command. If
upgrading the Zabbix agent 2, substitute `zabbix-agent` with
`zabbix-agent2` in the command.

Then, to upgrade the web frontend with Apache correctly, also run:

    apt-get install zabbix-apache-conf</source>
        <target state="needs-translation">##### 5 Atualize os componentes do Zabbix

Para atualizar os componentes do Zabbix você deve executar
algo como:

    # apt-get install --only-upgrade zabbix-server-mysql zabbix-frontend-php zabbix-agent

Se usando PostgreSQL, substitua `mysql` por `pgsql` no comando. Se
atualizando o Proxy, substitua `server` por `proxy` no comando. Se
atualizando o Agent 2, substitua `zabbix-agent` por `zabbix-agent2` no
comando.

Para atualizar corretamente o Zabbix Frontend com Apache, execute também:

    # apt-get install zabbix-apache-conf

Distributions **prior to Debian 10 (buster) / Ubuntu 18.04 (bionic) /
Raspbian 10 (buster)** do not provide PHP 7.2 or newer, which is
required for Zabbix frontend 5.0. See
[information](/manual/installation/frontend/frontend_on_rhel7) about
installing Zabbix frontend on older distributions.</target>
      </trans-unit>
      <trans-unit id="455f4e97" xml:space="preserve">
        <source>##### 6 Review component configuration parameters

Make sure to review [Upgrade notes](/manual/installation/upgrade_notes_700) to check if any changes in the configuration parameters are required.

For new optional parameters, see the [What's
new](/manual/introduction/whatsnew700) page.</source>
      </trans-unit>
      <trans-unit id="624a8fc7" xml:space="preserve">
        <source>##### 7 Start Zabbix processes

Start the updated Zabbix components.

    service zabbix-server start
    service zabbix-proxy start
    service zabbix-agent start
    service zabbix-agent2 start</source>
        <target state="needs-translation">
##### 7 Inicie os processos do Zabbix

Inicie os componentes do Zabbix atualizados.

    # service zabbix-server start
    # service zabbix-proxy start
    # service zabbix-agent start
    # service zabbix-agent2 start</target>
      </trans-unit>
      <trans-unit id="49e4f43e" xml:space="preserve">
        <source>##### 8 Clear web browser cookies and cache

After the upgrade you may need to clear web browser cookies and web
browser cache for the Zabbix web interface to work properly.</source>
        <target state="needs-translation">
##### 8 Limpe os cookies e cache do navegador

Após a atualização pode ser necessário limpar os cookies e o cache do 
navegador para que a interface web do Zabbix funcione corretamente.</target>
      </trans-unit>
      <trans-unit id="9bab02f2" xml:space="preserve">
        <source>#### Upgrade between minor versions

It is possible to upgrade minor versions of 7.0.x (for example, from
7.0.1 to 7.0.3). It is easy.

To upgrade Zabbix minor version please run:

    sudo apt install --only-upgrade 'zabbix.*'

To upgrade Zabbix server minor version please run:

    sudo apt install --only-upgrade 'zabbix-server.*'

To upgrade Zabbix agent minor version please run:

    sudo apt install --only-upgrade 'zabbix-agent.*'

or, for Zabbix agent 2:

    sudo apt install --only-upgrade 'zabbix-agent2.*'</source>
      </trans-unit>
    </body>
  </file>
</xliff>
