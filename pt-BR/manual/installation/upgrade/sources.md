[comment]: # translation:outdated

[comment]: # ({44ca00be-7900383c})
# Atualização a partir dos fontes

[comment]: # ({/44ca00be-7900383c})

[comment]: # ({new-92c7a8e5})

#### Visão geral

Esta seção provê os passos necessários para uma [atualização](/manual/installation/upgrade) bem-sucedida
do Zabbix **5.4**.x para o Zabbix **6.0**.x usando os fontes oficiais do Zabbix.

Conquanto a atualização do Zabbix Agent não seja obrigatória (mas recomendada),
o Zabbix Server e Proxies devem estar na [mesma versão primária (major version)](/manual/appendix/compatibility). 
Deste modo, em um ambiente Server-Proxy, o Zabbix Server e todos os Proxies
precisam ser parados e atualizados. Manter os Proxies em execução durante a
atualização do Server não mais trará qualquer benefício, pois durante a atualização 
do Proxy seus dados antigos serão descartados e nenhum novo dado será reunido
até que a configuração do Proxy seja sincronizada com o Server.

::: noteimportant
Não é mais possível iniciar um Zabbix Server atualizado e ter instâncias do 
Zabbix Proxy mais antigas, ainda não atualizadas reportando dados para este 
novo Server. Esta abordagem, que nunca foi suportada e nem recomendada 
pela Zabbix, agora está oficialmente desabilitada. O Zabbix Server irá ignorar os 
dados enviados de Proxies não atualizados.
:::

Note que com banco de dados SQLite nos Proxies, dados históricos de antes da 
atualização serão perdidos, porque a atualização do SQLite não é suportada e o 
arquivo de banco de dados tem de ser removido manualmente. Quando o Proxy 
for iniciado pela primeira vez e o arquivo de banco de dados do SQLite estiver 
ausente, o Proxy o criará automaticamente.

Dependendo do tamanho do banco de dados a atualização para a versão 6.0
pode levar um longo tempo.

::: notewarning
Antes da atualização certifique-se de ler as **notas de atualização** relevantes!
:::

As seguintes notas de atualização estão disponíveis:

|Atualização de|Leia as notas de atualização completas|Mudanças mais importantes entre as versões|
|------------|-----------------------|---------------------------------------|
|5.4.x|Para:<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|<|
|5.2.x|Para:<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|Versões mínimas de banco de dados exigidas elevadas;<br>Itens agregados removidos como tipo separado.|
|5.0.x|Para:<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|Versão mínima requerida para PHP elevada de 7.2.0 para 7.2.5.|
|4.4.x|Para:<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)|Suporte a IBM DB2 retirado;<br>Versão mínima requerida para PHP elevada de 5.4.0 para 7.2.0;<br>Versões mínimas de banco de dados exigidas elevadas;<br>Alterado diretório de arquivos PHP do Zabbix.|
|4.2.x|Para:<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.4/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|Jabber, Ez Texting media types removidos.|
|4.0.x LTS|Para:<br>Zabbix [4.2](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_420)<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.4/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|Proxies mais antigos não podem mais enviar dados para um Server atualizado;<br>Agents mais novos não podem mais trabalhar com um Zabbix Server mais antigo.|
|3.4.x|Para:<br>Zabbix [4.0](https://www.zabbix.com/documentation/4.0/manual/installation/upgrade_notes_400)<br>Zabbix [4.2](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_420)<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.4/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|Bibliotecas 'libpthread' e 'zlib' agora obrigatórias;<br>Suporte ao protocolo de texto plano removido e cabeçalho é mandatório;<br>Versões Pré-1.4 dos Zabbix Agents não são mais suportadas;<br>Parâmetro Server na configuração de Proxy passivo agora obrigatória.|
|3.2.x|Para:<br>Zabbix [3.4](https://www.zabbix.com/documentation/3.4/manual/installation/upgrade_notes_340)<br>Zabbix [4.0](https://www.zabbix.com/documentation/4.0/manual/installation/upgrade_notes_400)<br>Zabbix [4.2](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_420)<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.4/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|Suporte a SQLite como banco de dados removido para Zabbix Server/Frontend;<br>Expressões Regulares compatíveis com Perl (PCRE) suportadas em vez do POSIX estendido;<br>Bibliotecas 'libpcre' e 'libevent' obrigatórias para Zabbix Server;<br>Códigos de saída adicionados para parâmetros de usuário, comandos remotos e itens system.run\[\] sem o marcador 'nowait' assim como scripts executados do Zabbix Server;<br>Zabbix Java Gateway tem de ser atualizado para suportar novas funcionalidades.|
|3.0.x LTS|Para:<br>Zabbix [3.2](https://www.zabbix.com/documentation/3.2/manual/installation/upgrade_notes_320)<br>Zabbix [3.4](https://www.zabbix.com/documentation/3.4/manual/installation/upgrade_notes_340)<br>Zabbix [4.0](https://www.zabbix.com/documentation/4.0/manual/installation/upgrade_notes_400)<br>Zabbix [4.2](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_420)<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.4/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|Atualização do banco de dados por ser lenta, dependendo do tamanho da tabela de histórico.|
|2.4.x|Para:<br>Zabbix [3.0](https://www.zabbix.com/documentation/3.0/manual/installation/upgrade_notes_300)<br>Zabbix [3.2](https://www.zabbix.com/documentation/3.2/manual/installation/upgrade_notes_320)<br>Zabbix [3.4](https://www.zabbix.com/documentation/3.4/manual/installation/upgrade_notes_340)<br>Zabbix [4.0](https://www.zabbix.com/documentation/4.0/manual/installation/upgrade_notes_400)<br>Zabbix [4.2](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_420)<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|Versão mínima requerida para PHP elevada de 5.3.0 para 5.4.0<br>Parâmetro LogFile do Agent deve ser especificado|
|2.2.x LTS|Para:<br>Zabbix [2.4](https://www.zabbix.com/documentation/2.4/manual/installation/upgrade_notes_240)<br>Zabbix [3.0](https://www.zabbix.com/documentation/3.0/manual/installation/upgrade_notes_300)<br>Zabbix [3.2](https://www.zabbix.com/documentation/3.2/manual/installation/upgrade_notes_320)<br>Zabbix [3.4](https://www.zabbix.com/documentation/3.4/manual/installation/upgrade_notes_340)<br>Zabbix [4.0](https://www.zabbix.com/documentation/4.0/manual/installation/upgrade_notes_400)<br>Zabbix [4.2](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_420)<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|Monitoramento distribuído baseado em Node removido|
|2.0.x|Para:<br>Zabbix [2.2](https://www.zabbix.com/documentation/2.2/manual/installation/upgrade_notes_220)<br>Zabbix [2.4](https://www.zabbix.com/documentation/2.4/manual/installation/upgrade_notes_240)<br>Zabbix [3.0](https://www.zabbix.com/documentation/3.0/manual/installation/upgrade_notes_300)<br>Zabbix [3.2](https://www.zabbix.com/documentation/3.2/manual/installation/upgrade_notes_320)<br>Zabbix [3.4](https://www.zabbix.com/documentation/3.4/manual/installation/upgrade_notes_340)<br>Zabbix [4.0](https://www.zabbix.com/documentation/4.0/manual/installation/upgrade_notes_400)<br>Zabbix [4.2](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_420)<br>Zabbix [4.4](https://www.zabbix.com/documentation/4.2/manual/installation/upgrade_notes_440)<br>Zabbix [5.0](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500)<br>Zabbix [5.2](https://www.zabbix.com/documentation/5.2/manual/installation/upgrade_notes_520)<br>Zabbix [5.4](https://www.zabbix.com/documentation/5.4/manual/installation/upgrade_notes_540)<br>Zabbix [6.0](/manual/installation/upgrade_notes_600)|Versão mínima requerida para PHP elevada de 5.1.6 para 5.3.0;<br>Banco de dados MySQL Case-sensitive requerido para correto funcionamento do Server; Codificação utf8 e utf8\_bin necessária para Zabbix Server funcionar corretamente com MySQL. Veja [criação de scripts de banco de dados](/manual/appendix/install/db_scripts#mysql).<br>Extensão 'mysqli' do PHP necessária em vez de 'mysql'|

Também pode ser de seu interesse verificar os [requisitos](/manual/installation/requirements) para a versão 6.0.

::: notetip
Pode ser útil executar duas sessões SSH paralelas durante a atualização,
executando os passos de atualização em uma e monitorando os logs do
Server/Proxy em outra. Por exemplo, a execução do comando 
`tail -f zabbix_server.log` ou `tail -f zabbix_proxy.log` na 
segunda sessão SSH lhe mostrará os últimos registros do arquivo de log
conforme eles forem sendo registrados, possibilitando a verificação de
possíveis erros em tempo real. Isto pode ser crítico para ambientes de 
produção.
:::


[comment]: # ({/new-92c7a8e5})

[comment]: # ({2f041cb5-101d6faf})
#### Processo de atualização do Server

[comment]: # ({/2f041cb5-101d6faf})

[comment]: # ({1a558e69-b8308740})
##### 1 Stop server

Pare o Zabbix Server para certificar-se de que nenhum novo dado
está sendo gravado no banco de dados.

[comment]: # ({/1a558e69-b8308740})

[comment]: # ({6b944402-ab13a6a4})

##### 2 Faça backup do banco de dados Zabbix

Este é um passo muito importante. Certifique-se de que você tem um
backup do seu banco de dados. Ele o ajudará se o procedimento de 
atualização falhar (falta de espaço em disco, desligamento forçado,
ou qualquer outro problema inesperado).

[comment]: # ({/6b944402-ab13a6a4})

[comment]: # ({c3437b57-d2778675})
##### 3 Backup dos arquivos de configuração, arquivos PHP e binários do Zabbix

Faça uma cópia de backup dos binários do Zabbix, arquivos de configuração
e do diretório de arquivos PHP.

[comment]: # ({/c3437b57-d2778675})

[comment]: # ({db71c32b-2bb75ddd})
##### 4 Instale os novos binários do Server

Utilize estas [instruções](/manual/installation/install#installing_zabbix_daemons) para compilar o Zabbix Server a partir dos fontes.

[comment]: # ({/db71c32b-2bb75ddd})

[comment]: # ({new-ec6edae7})

##### 5 Revise os parâmetros de configuração do Server


Consulte as notas de atualização para detalhes sobre 
[alterações obrigatórias](/manual/installation/upgrade_notes_600#configuration_parameters).

Para novos parâmetros opcionais, veja a seção [O que há de novo](/manual/introduction/whatsnew600#configuration_parameters).

[comment]: # ({/new-ec6edae7})

[comment]: # ({cb1efaca-a01e967f})

##### 6 Inicie os novos binários do Zabbix

Inicie os novos binários. Verifique os arquivos de log para garantia de 
que os binários subiram corretamente.

O Zabbix Server atualizará automaticamente o banco de dados. Quando
iniciando, o Zabbix Server informa as versões de banco de dados atual
(mandatório e opcional) e exigida. Se a versão mandatória atual é mais
antiga que a versão exigida, o Zabbix Server executa as atualizações do
banco de dados necessárias de forma automática. O andamento do processo
de atualização (porcentagem) é registrado no arquivo de log do server.
Quando a atualização estiver completa, uma mensagem de "atualização
de banco de dados completa" é registrada no log. Se algum dos passos
de atualização falhar, o Zabbix Server não inicializará. O Zabbix Server 
também não iniciará se a versão de banco de dados atual for maior que
a versão requerida. O Zabbix Server iniciará apenas se a versão atual
corresponder à versão necessária.

    8673:20161117:104750.259 current database version (mandatory/optional): 03040000/03040000
    8673:20161117:104750.259 required mandatory version: 03040000

Antes de você iniciar o Server:

-   Certifique-se de que o usuário do banco de dados possui permissões 
    suficientes (create table, drop table, create index, drop index)
-   Certifique-se de que tenha espaço em disco suficiente
- .

[comment]: # ({/cb1efaca-a01e967f})

[comment]: # ({6f661db7-22e0e4e9})

##### 7 Instale a nova interface web do Zabbix

A versão mínima do PHP exigida é a 7.2.5. Atualize se necessário 
e siga as [instruções de instalação](/manual/installation/frontend).

[comment]: # ({/6f661db7-22e0e4e9})

[comment]: # ({9872b20f-49e4f43e})

##### 8 Limpe os cookies e cache do navegador

Após a atualização pode ser necessário limpar os cookies e o cache do 
navegador para que a interface web do Zabbix funcione corretamente.

[comment]: # ({/9872b20f-49e4f43e})

[comment]: # ({97e5c3e1-75f5b3ed})
#### Processo de atualização do Proxy

[comment]: # ({/97e5c3e1-75f5b3ed})

[comment]: # ({71f99300-f33e6ddb})
##### 1 Pare o Proxy

Para continuar pare o Zabbix Proxy.

[comment]: # ({/71f99300-f33e6ddb})

[comment]: # ({eff4d226-2e03c550})

##### 2 Backup da configuração e binários do Zabbix Proxy

Faça uma cópia de backup do arquivo de configuração e dos binários do Zabbix Proxy.

[comment]: # ({/eff4d226-2e03c550})

[comment]: # ({282fcd1d-024201dc})

##### 3 Instale novo binários do Proxy

Use estas [instruções](/manual/installation/install#installing_zabbix_daemons) para compilar Zabbix Proxy a partir dos fontes.

[comment]: # ({/282fcd1d-024201dc})

[comment]: # ({47a1699d-2fbda571})

##### 4 Revise os parâmetros de configuração do Proxy

Não há alterações obrigatórias aos [parâmetros](/manual/appendix/config/zabbix_proxy) do Zabbix Proxy nesta versão.

[comment]: # ({/47a1699d-2fbda571})

[comment]: # ({ec38d8c3-520f43c6})

##### 5 Inicie o novo ZabbixPproxy

Inicie o novo Zabbix Proxy. Consulte os arquivos de log para garantir que 
Proxy iniciou corretamente.

O Zabbix proxy atualizará automaticamente o banco de dados. A atualização
do banco de dados ocorrerá de maneira semelhante à inicilização do [Zabbix Server](/manual/installation/upgrade#start_new_zabbix_binaries).

[comment]: # ({/ec38d8c3-520f43c6})

[comment]: # ({787cc56b-59736bba})

#### Processo de atualização do Agent

::: noteimportant
A atualização dos Agents não é obrigatória. Você só precisa atualizar 
os Agents se exigido para acessar novas funcionalidades.
:::

O processo de atualização descrito nesta seção pode ser usado 
para atualização tanto do Zabbix Agent quanto do Zabbix Agent 2.

[comment]: # ({/787cc56b-59736bba})

[comment]: # ({38ed13b9-02698e69})
##### 1 Pare o Agent

Para prosseguir com a atualização pare o Zabbix Agent.

[comment]: # ({/38ed13b9-02698e69})

[comment]: # ({b37b567e-50263823})

##### 2 Backup da configuração e binários do Zabbix Agent

Faça uma cópia de backup do arquivo de configuração e dos binários do 
Zabbix Agent.

[comment]: # ({/b37b567e-50263823})

[comment]: # ({f59ed404-467f56b3})

##### 3 Instale novos binários do Agent

Use estas [instruções](/manual/installation/install#installing_zabbix_daemons) para compilar o Zabbix Agent a partir dos fontes.

Alternativamente, você pode baixar os Zabbix Agents pré-compilados 
da [página de download da Zabbix](http://www.zabbix.com/download.php).

[comment]: # ({/f59ed404-467f56b3})

[comment]: # ({cd8019af-34721aad})

##### 4 Revise os parâmetros de configuração do Agent

Não há alterações obrigatórias nos parâmetros desta versão 
do [Agent](/manual/appendix/config/zabbix_agentd) e nem do [Agent 2](/manual/appendix/config/zabbix_agent2).

[comment]: # ({/cd8019af-34721aad})

[comment]: # ({b984cb77-04c253a7})

##### 5 Inicie o novo Zabbix Agent

Inicie o novo Zabbix Agent. Confirme nos arquivos de log 
se o Agent iniciou corretamente.

[comment]: # ({/b984cb77-04c253a7})

[comment]: # ({new-a06019d2})

#### Atualização entre versão secundárias (minor versions)

Quando atualizando entre versões secundárias do Zabbix 6.0.x (por exemplo de 6.0.1
para 6.0.3) é necessário executar as mesmas ações para o Zabbix Server/Proxy/Agent 
assim como durante a atualização entre versões primárias (major versions). A única
diferença é que quando atualizando entre versões secundárias nenhuma alteração de
banco de dados é feita.

[comment]: # ({/new-a06019d2})
