[comment]: # translation:outdated

[comment]: # ({33bff3ef-9cceae9f})

# 6 Instalação da interface web

Esta seção provê instruções passo a passo para a instalação da interface 
web do Zabbix. O Zabbix Frontend é escrito em PHP, então para seu
funcionamento é necessário um servidor web com suporte ao PHP.

[comment]: # ({/33bff3ef-9cceae9f})

[comment]: # ({new-0c649c0e})
:::notetip
You can find out more about setting up SSL for Zabbix frontend by referring to these [best practices](/manual/installation/requirements/best_practices).
:::

[comment]: # ({/new-0c649c0e})

[comment]: # ({e434fbb6-6b173526})

#### Tela de boas-vindas

Abra a URL do Zabbix Frontend no navegador. Se você instalou o Zabbix 
a partir dos pacotes, a URL é:

-   para Apache: *http://<server\_ip\_or\_name>/zabbix*
-   para Nginx: *http://<server\_ip\_or\_name>*

Você deverá ver a primeira tela do assistente de instalação do Frontend.

Utilize o menu *Default language* (idioma padrão) para alterar o idioma padrão do sistema e
continuar com o processo de instalação no idioma selecionado (opcional). Para mais 
informações, consulte [Instalação de idiomas adicionais para o Frontend](/manual/appendix/install/locales).

![](../../../assets/en/manual/installation/install_1.png){width="550"}

[comment]: # ({/e434fbb6-6b173526})

[comment]: # ({5b557c73-2458c643})

#### Verificação de pré-requisitos

Certifique-se de que todos os pré-requisitos são atendidos.

![](../../../assets/en/manual/installation/install_2.png){width="550"}

|Pré-requisito|Valor mínimo|Descrição|
|-------------|-------------|-----------|
|*Versão PHP*|7.2.5|<|
|*Opção PHP memory\_limit*|128MB|Em php.ini:<br>memory\_limit = 128M|
|*Opção PHP post\_max\_size*|16MB|Em php.ini:<br>post\_max\_size = 16M|
|*Opção PHP upload\_max\_filesize*|2MB|Em php.ini:<br>upload\_max\_filesize = 2M|
|*Opção PHP max\_execution\_time*|300 segundos (valores 0 e -1 são permitidos)|Em php.ini:<br>max\_execution\_time = 300|
|*Opção PHP max\_input\_time*|300 segundos (valores 0 e -1 são permitidos)|Em php.ini:<br>max\_input\_time = 300|
|*Opção PHP session.auto\_start*|Deve estar desabilitada|Em php.ini:<br>session.auto\_start = 0|
|*Suporte banco de dados*|Um de: MySQL, Oracle, PostgreSQL.|Um dos seguintes módulos deve estar instalado:<br>mysql, oci8, pgsql|
|*bcmath*|<|php-bcmath|
|*mbstring*|<|php-mbstring|
|*Opção PHP mbstring.func\_overload*|Deve estar desabilitada|Em php.ini:<br>mbstring.func\_overload = 0|
|*sockets*|<|php-net-socket. Exigido para suporte a script de usuário.|
|*gd*|2.0.28|php-gd. A extensão PHP GD deve suportar imagens PNG (*--with-png-dir*), imagens JPEG (*--with-jpeg-dir*) e FreeType 2 (*--with-freetype-dir*).|
|*libxml*|2.6.15|php-xml|
|*xmlwriter*|<|php-xmlwriter|
|*xmlreader*|<|php-xmlreader|
|*ctype*|<|php-ctype|
|*session*|<|php-session|
|*gettext*|<|php-gettext<br>Desde o Zabbix 2.2.1, a extensão PHP gettext não é um requisito obrigatório para a instalação do Zabbix. Se gettext não estiver instalado, o Frontend funcionará normalmente, porém, as traduções não estarão disponíveis.|

Pré-requisitos opcionais também podem estar presentes na lista. Um pré-requisito
opcional com falha será mostrado em laranja e apresentado como *Warning*.
Com falha em um pré-requisito opcional, a configuração pode continuar.

::: noteimportant
Se houver necessidade de alterar o usuário ou grupo de usuário do Apache, 
as permissões para a pasta de sessão devem ser verificadas.
Caso contrário, a configuração do Zabbix pode ficar impedida de continuar.
:::

[comment]: # ({/5b557c73-2458c643})

[comment]: # ({c6edf5bf-879dc06f})

#### Configuração de conexão com banco de dados

Informe os dados de conexão com o banco de dados. O banco de dados
do Zabbix já deve estar criado.

![](../../../assets/en/manual/installation/install_3a.png){width="550"}

Se a opção *Database TLS encryption* (criptografia TLS do banco de dados) 
estiver ativada, então campos adicionais para [configuração da conexão TLS](/manual/appendix/install/db_encrypt) 
com o banco de dados serão mostrados no formulário (apenas para MySQL 
ou PostgreSQL).

Se a opção HashiCorp Vault estiver selecionada para armazenamento de 
credenciais, campos adicionais estarão disponíveis para especificar o
endpoint da API do Vault, secret path e token de autenticação:

![](../../../assets/en/manual/installation/install_3b.png){width="550"}

[comment]: # ({/c6edf5bf-879dc06f})

[comment]: # ({80325b3b-98277238})

#### Configurações

Informar um nome para o Zabbix Server é opcional, no entanto, caso 
informado, ele será mostrado na barra de menu e títulos de página.

Configure o [time zone](/manual/web_interface/time_zone#overview) padrão e o tema do Frontend.

![](../../../assets/en/manual/installation/install_4.png){width="550"}

[comment]: # ({/80325b3b-98277238})

[comment]: # ({7f3513bd-42398398})

#### Resumo pré-instalação

Revise o resumo das configurações antes da instalação efetiva.

![](../../../assets/en/manual/installation/install_5.png){width="550"}

[comment]: # ({/7f3513bd-42398398})

[comment]: # ({660c150d-1124dd9e})

#### Instalação

Se instalando o Zabbix a partir dos fontes, baixe o arquivo de configuração
e salve-o dentro de conf/ no subdiretório de documentos HTML do Web 
Server onde você colocou os arquivos PHP do Zabbix.

![](../../../assets/en/manual/installation/install_6.png){width="550"}

![](../../../assets/en/manual/installation/saving_zabbix_conf.png){width="350"}

::: notetip
Garantindo que o usuário do Web Server tenha acesso de escrita
no diretório conf/ o arquivo de configuração será salvo automaticamente
e será possível continuar para o próximo passo imediatamente.
:::

Finalize a instalação.

![](../../../assets/en/manual/installation/install_7.png){width="550"}

[comment]: # ({/660c150d-1124dd9e})

[comment]: # ({acb44b32-d59dc4b9})

#### Log in

O Zabbix Frontend está pronto! O nome de usuário padrão é **Admin**, 
e senha **zabbix**.

![](../../../assets/en/manual/quickstart/login.png){width="350"}

Agora é só [começar a usar o Zabbix](/manual/quickstart/login).

[comment]: # ({/acb44b32-d59dc4b9})
