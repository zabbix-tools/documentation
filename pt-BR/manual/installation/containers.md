[comment]: # translation:outdated

[comment]: # ({39e950c3-e62e1497})
# 5 Instalação por containers

[comment]: # ({/39e950c3-e62e1497})

[comment]: # ({new-a3ac452e})

### Overview

This section describes how to deploy Zabbix with [Docker](#docker) or [Docker Compose](#docker-compose).

Zabbix officially provides:

- Separate Docker images for each Zabbix
component to run as portable and self-sufficient containers.
- Compose files for defining and running
multi-container Zabbix components in Docker.

[comment]: # ({/new-a3ac452e})

[comment]: # ({75fd7d63-f9acea87})
### Docker

A Zabbix fornece imagens em [Docker](https://www.docker.com) para cada componente Zabbix,
entregando contêineres portáveis e autossuficientes para acelerar o 
procedimento de implantação e atualização.

Os componentes Zabbix vêm com suporte aos bancos de dados MySQL e 
PostgreSQL, e suporte à Apache2 e Nginx Web Server. 
Tais soluções estão separadas em diferentes imagens.

[comment]: # ({/75fd7d63-f9acea87})

[comment]: # ({new-e498c2aa})

#### Tags

Official Zabbix component images may contain the following tags:

|Tag|Description|Example|
|--|----------|----|
|latest            | The latest stable version of a Zabbix component based on Alpine Linux image. |zabbix-agent:latest |
|<OS>-trunk | The latest nightly build of the Zabbix version that is currently being developed on a specific operating system. <br> <br> **<OS>** - the base operating system. Supported values: <br> *alpine* - Alpine Linux; <br> *ltsc2019* -  Windows 10 LTSC 2019 (agent only); <br> *ol* - Oracle Linux; <br> *ltsc2022* - Windows 11 LTSC 2022 (agent only); <br> *ubuntu* - Ubuntu  |zabbix agent:ubuntu-trunk |
|<OS>-latest     | The latest stable version of a Zabbix component on a specific operating system. <br> <br> **<OS>** - the base operating system. Supported values:  <br> *alpine* - Alpine Linux; <br> *ltsc2019* -  Windows 10 LTSC 2019 (agent only); <br> *ol* - Oracle Linux; <br> *ltsc2022* - Windows 11 LTSC 2022 (agent only); <br> *ubuntu* - Ubuntu|zabbix-agent:ol-latest |
|<OS>-X.X-latest |The latest minor version of a Zabbix component of a specific major version and operating system. <br> <br> **<OS>** - the base operating system. Supported values:  <br> *alpine* - Alpine Linux; <br> *centos* - CentOS (only for Zabbix 4.0) <br> *ltsc2019* -  Windows 10 LTSC 2019 (agent only); <br> *ol* - Oracle Linux; <br> *ltsc2022* - Windows 11 LTSC 2022 (agent only); <br> *ubuntu* - Ubuntu<br><br>**X.X** - the major Zabbix version (supported: *4.0*, *5.0*, *6.0*, *6.2*). |zabbix-agent:alpine-6.0-latest |
|<OS>-X.X.*      |The latest minor version of a Zabbix component of a specific major version and operating system. <br> <br> **<OS>** - the base operating system. Supported values:  <br> *alpine* - Alpine Linux; <br> *centos* - CentOS (only for Zabbix 4.0) <br> *ltsc2019* -  Windows 10 LTSC 2019 (agent only); <br> *ol* - Oracle Linux; <br> *ltsc2022* - Windows 11 LTSC 2022 (agent only); <br> *ubuntu* - Ubuntu<br><br>**X.X** - the major Zabbix version (supported: *4.0*, *5.0*, *6.0*, *6.2*). <br><br> **\*** - the Zabbix minor version  |zabbix-agent:alpine-6.4.1|

[comment]: # ({/new-e498c2aa})


[comment]: # ({90d2ac11-fba82ded})
#### Arquivos fonte para Docker

Todos podem acompanhar alterações nos arquivos Docker através do
[repositório oficial](https://github.com/zabbix/zabbix-docker) no [github.com](https://github.com/). Você também pode realizar um fork do 
projeto ou criar suas próprias imagens baseadas nos arquivos Docker oficiais.

[comment]: # ({/90d2ac11-fba82ded})



[comment]: # ({020699bb-0b4b68c7})
#### Uso

[comment]: # ({/020699bb-0b4b68c7})

[comment]: # ({b27e0bd7-de9f41d4})
##### Variáveis de ambiente

Todas as imagens de componentes Zabbix fornecem variáveis de ambiente
para controlar a configuração. Estas variáveis de ambiente estão listadas em
cada repositório dos componentes. Tais variáveis são opções vindas dos 
arquivos de configuração do Zabbix, mas com método de nomenclatura
diferenciado. Por exemplo, `ZBX_LOGSLOWQUERIES` é o mesmo que a opção
`LogSlowQueries` dos arquivos de configuração do Zabbix Server e Zabbix Proxy.

::: noteimportant
Algumas opções de configuração não podem ser alteradas.
Por exemplo, `PIDFile` and `LogType`.
:::

Alguns dos componentes possuem variáveis de ambiente específicas, 
que não existem nos arquivos de configuração oficiais do Zabbix:

|   |   |   |
|---|---|---|
|**Variável**|**Componentes**|**Descrição**|
|`DB_SERVER_HOST`|Server<br>Proxy<br>Web interface|Esta variável representa o IP ou nome DNS do servidor MySQL ou PostgreSQL.<br>Por padrão, seu valor é `mysql-server` ou `postgres-server` para MySQL ou PostgreSQL, respectivamente.|
|`DB_SERVER_PORT`|Server<br>Proxy<br>Web interface|Esta variável representa a porta do servidor MySQL ou PostgreSQL.<br>Por padrão, seu valor é '3306' ou '5432',  respectivamente.|
|`MYSQL_USER`|Server<br>Proxy<br>Web-interface|Usuário do banco de dados MySQL.<br>Por padrão, seu valor é 'zabbix'.|
|`MYSQL_PASSWORD`|Server<br>Proxy<br>Web interface|Senha do banco de dados MySQL.<br>Por padrão, seu valor é 'zabbix'.|
|`MYSQL_DATABASE`|Server<br>Proxy<br>Web interface|Nome do banco de dados do Zabbix.<br>Por padrão, seu valor é 'zabbix' para Zabbix Server e 'zabbix\_proxy' para Zabbix Proxy.|
|`POSTGRES_USER`|Server<br>Web interface|Usuário do banco de dados PostgreSQL.<br>Por padrão, seu valor é 'zabbix'.|
|`POSTGRES_PASSWORD`|Server<br>Web interface|Senha do banco de dados PostgreSQL.<br>Por padrão, seu valor é 'zabbix'.|
|`POSTGRES_DB`|Server<br>Web interface|Nome do banco de dados do Zabbix.<br>Por padrão, seu valor é 'zabbix' para Zabbix Server e 'zabbix\_proxy' para Zabbix Proxy.|
|`PHP_TZ`|Web-interface|Timezone no formato PHP. Lista completa de timezones suportados disponível em [php.net](http://php.net/manual/en/timezones.php).<br>Por padrão, seu valor é 'Europe/Riga'.|
|`ZBX_SERVER_NAME`|Web interface|Nome de instalação visível no canto superior direito da interface web.<br>Por padrão, seu valor é 'Zabbix Docker'|
|`ZBX_JAVAGATEWAY_ENABLE`|Server<br>Proxy|Habilita comunicação com Zabbix Java Gateway para coleta de verificações relacionadas ao Java.<br>Por padrão, seu valor é "false"|
|`ZBX_ENABLE_SNMP_TRAPS`|Server<br>Proxy|Habilita a funcionalidade SNMP Trap. Requer instância **zabbix-snmptraps** e volume */var/lib/zabbix/snmptraps* compartilhado para Zabbix Server ou Zabbix Proxy.|

[comment]: # ({/b27e0bd7-de9f41d4})

[comment]: # ({76f5f726-7d10f7dc})
##### Volumes

As imagens permitem utilizar alguns pontos de montagem. Estes pontos 
de montagem são diferentes e dependem do tipo de componente Zabbix:

|   |   |
|---|---|
|**Volume**|**Descrição**|
|**Zabbix Agent**|<|
|*/etc/zabbix/zabbix\_agentd.d*|O volume permite incluir arquivos *\*.conf* e estende o Zabbix Agent usando a funcionalidade `UserParameter`|
|*/var/lib/zabbix/modules*|O volume permite carregar módulos adicionais e estende o Zabbix Agent usando a funcionalidade [LoadModule](/manual/config/items/loadablemodules)|
|*/var/lib/zabbix/enc*|O volume é usado para armazenar arquivos relacionados a TLS. Estes nomes de arquivo são especificados usando as variáveis de ambiente `ZBX_TLSCAFILE`, `ZBX_TLSCRLFILE`, `ZBX_TLSKEY_FILE` e `ZBX_TLSPSKFILE`|
|**Zabbix Server**|<|
|*/usr/lib/zabbix/alertscripts*|O volume é usado para scripts de alerta customizados. É o parâmetro `AlertScriptsPath` no [zabbix\_server.conf](/manual/appendix/config/zabbix_server)|
|*/usr/lib/zabbix/externalscripts*|O volume é usado por [verificações externas](/manual/config/items/itemtypes/external). É o parâmetro `ExternalScripts` no [zabbix\_server.conf](/manual/appendix/config/zabbix_server)|
|*/var/lib/zabbix/modules*|O volume permite carregar módulos adicionais e estende o Zabbix Server usando a funcionalidade [LoadModule](/manual/config/items/loadablemodules)|
|*/var/lib/zabbix/enc*|O volume é usado para armazenar arquivos relacionados a TLS. Estes nomes de arquivo são especificados usando as variáveis de ambiente `ZBX_TLSCAFILE`, `ZBX_TLSCRLFILE`, `ZBX_TLSKEY_FILE` e `ZBX_TLSPSKFILE`|
|*/var/lib/zabbix/ssl/certs*|O volume é usado como local para os arquivos de certificado de cliente SSL para autenticação de cliente. É o parâmetro `SSLCertLocation` no zabbix\_server.conf|
|*/var/lib/zabbix/ssl/keys*|O volume é usado como local para os arquivos de chave privada SSL para autenticação de cliente. É o parâmetro `SSLKeyLocation` no [zabbix\_server.conf](/manual/appendix/config/zabbix_server)|
|*/var/lib/zabbix/ssl/ssl\_ca*|O volume é usado como local para arquivos de certificado de autoridade (CA) para verificação SSL de certificado de servidor. É o parâmetro `SSLCALocation` no [zabbix\_server.conf](/manual/appendix/config/zabbix_server)|
|*/var/lib/zabbix/snmptraps*|O volume é usado como local para o arquivo snmptraps.log. Ele pode ser compartilhado pelo conteiner zabbix-snmptraps e herdado usando a opção volumes\_from do Docker durante a criação de uma nova instância do Zabbix Server. A funcionalidade de processamento de SNMP Trap pode ser habilitada pelo uso do volume compartilhado e alterando a variável de ambiente `ZBX_ENABLE_SNMP_TRAPS` para 'true'|
|*/var/lib/zabbix/mibs*|O volume permite adicionar novos arquivos MIB. Ele não suporta subdiretórios, assim todas as MIBs devem estar em `/var/lib/zabbix/mibs`|
|**Zabbix Proxy**|<|
|*/usr/lib/zabbix/externalscripts*|O volume é usado por [verificações externas](/manual/config/items/itemtypes/external). É o parâmetro `ExternalScripts` no [zabbix\_proxy.conf](/manual/appendix/config/zabbix_proxy)|
|*/var/lib/zabbix/modules*|O volume permite carregar módulos adicionais e estender o Zabbix Proxy usando a funcionalidade [LoadModule](/manual/config/items/loadablemodules)|
|*/var/lib/zabbix/enc*|O volume é usado para armazenar arquivos relacionados a TLS. Estes nomes de arquivo são especificados usando as variáveis de ambiente `ZBX_TLSCAFILE`, `ZBX_TLSCRLFILE`, `ZBX_TLSKEY_FILE` e `ZBX_TLSPSKFILE`|
|*/var/lib/zabbix/ssl/certs*|O volume é usado como local para os arquivos de certificado de cliente SSL para autenticação de cliente. É o parâmetro `SSLCertLocation` no [zabbix\_proxy.conf](/manual/appendix/config/zabbix_proxy)|
|*/var/lib/zabbix/ssl/keys*|O volume é usado como local para os arquivos de chave privada SSL para autenticação de cliente. É o parâmetro `SSLKeyLocation` no [zabbix\_proxy.conf](/manual/appendix/config/zabbix_proxy)|
|*/var/lib/zabbix/ssl/ssl\_ca*|O volume é usado como local para arquivos de certificado de autoridade (CA) para verificação SSL de certificado de servidor. É o parâmetro `SSLCALocation` no [zabbix\_proxy.conf](/manual/appendix/config/zabbix_proxy)|
|*/var/lib/zabbix/snmptraps*|O volume é usado como local para o arquivo snmptraps.log. Ele pode ser compartilhado pelo conteiner zabbix-snmptraps e herdado usando a opção volumes\_from do Docker durante a criação de uma nova instância do Zabbix Server. A funcionalidade de processamento de SNMP Trap pode ser habilitada pelo uso do volume compartilhado e alterando a variável de ambiente `ZBX_ENABLE_SNMP_TRAPS` para 'true'|
|*/var/lib/zabbix/mibs*|O volume permite adicionar novos arquivos MIB. Ele não suporta subdiretórios, assim todas as MIBs devem estar em `/var/lib/zabbix/mibs`|
|**Zabbix Web Interface baseada em Apache2 Web Server**|<|
|*/etc/ssl/apache2*|O volume permite habilitar o protocolo HTTPS para o Zabbix Web Interface. O volume deve conter os dois arquivos `ssl.crt` e `ssl.key` preparados para conexões Apache2 SSL|
|**Zabbix Web Interface baseada em Nginx Web Server**|<|
|*/etc/ssl/nginx*|O volume permite habilitar o protocolo HTTPS para o Zabbix Web Interface. O volume deve conter os arquivos `ssl.crt`, `ssl.key` e `dhparam.pem` preparados para conexões Nginx SSL|
|**Zabbix snmptraps**|<|
|*/var/lib/zabbix/snmptraps*|O volume contém o arquivo de log `snmptraps.log` nomeado com os  SNMP Traps recebidos|
|*/var/lib/zabbix/mibs*|O volume permite adicionar novos arquivos MIB. Ele não suporta subdiretórios, assim todas as MIBs devem estar em `/var/lib/zabbix/mibs`|

Para informação adicional utilize os repositórios oficiais do Zabbix no Docker Hub.

[comment]: # ({/76f5f726-7d10f7dc})

[comment]: # ({c8d23948-492bd3ba})
##### Exemplos de uso

**Exemplo 1**

O exemplo demonstra como executar o Zabbix Server com suporte a banco de dados MySQL, 
Zabbix Web Interface baseada em Nginx Web Server e Zabbix Java Gateway.

1\. Crie uma rede dedicada aos contêineres dos componentes Zabbix:

    # docker network create --subnet 172.20.0.0/16 --ip-range 172.20.240.0/20 zabbix-net

2\. Inicie uma instância vazia de MySQL Server

    # docker run --name mysql-server -t \
          -e MYSQL_DATABASE="zabbix" \
          -e MYSQL_USER="zabbix" \
          -e MYSQL_PASSWORD="zabbix_pwd" \
          -e MYSQL_ROOT_PASSWORD="root_pwd" \
          --network=zabbix-net \
          -d mysql:8.0 \
          --restart unless-stopped \
          --character-set-server=utf8 --collation-server=utf8_bin \
          --default-authentication-plugin=mysql_native_password

3\. Inicie uma instância do Zabbix Java Gateway

    # docker run --name zabbix-java-gateway -t \
          --network=zabbix-net \
          --restart unless-stopped \
          -d zabbix/zabbix-java-gateway:alpine-5.4-latest

4\. Inicie a instância do Zabbix Server e a associe à instância do MySQL Server recém-criada

    # docker run --name zabbix-server-mysql -t \
          -e DB_SERVER_HOST="mysql-server" \
          -e MYSQL_DATABASE="zabbix" \
          -e MYSQL_USER="zabbix" \
          -e MYSQL_PASSWORD="zabbix_pwd" \
          -e MYSQL_ROOT_PASSWORD="root_pwd" \
          -e ZBX_JAVAGATEWAY="zabbix-java-gateway" \
          --network=zabbix-net \
          -p 10051:10051 \
          --restart unless-stopped \
          -d zabbix/zabbix-server-mysql:alpine-5.4-latest

::: noteclassic
A instância do Zabbix Server expõe a porta 10051/TCP (Zabbix
trapper) para máquina local.
:::

5\. Inicie o Zabbix Web Interface e o associe às instâncias do MySQL Server e Zabbix Server

    # docker run --name zabbix-web-nginx-mysql -t \
          -e ZBX_SERVER_HOST="zabbix-server-mysql" \
          -e DB_SERVER_HOST="mysql-server" \
          -e MYSQL_DATABASE="zabbix" \
          -e MYSQL_USER="zabbix" \
          -e MYSQL_PASSWORD="zabbix_pwd" \
          -e MYSQL_ROOT_PASSWORD="root_pwd" \
          --network=zabbix-net \
          -p 80:8080 \
          --restart unless-stopped \
          -d zabbix/zabbix-web-nginx-mysql:alpine-5.4-latest

::: noteclassic
A instância do Zabbix Web Interface expõe a porta 80/TCP (HTTP) para a máquina local.
:::

**Exemplo 2**

O exemplo demonstra como executar o Zabbix Server com suporte a PostgreSQL, 
Zabbix Web Interface baseada em Nginx Web Server e funcionalidade SNMP Trap.

1\. Crie uma rede dedicada para os contêineres dos componentes Zabbix:

    # docker network create --subnet 172.20.0.0/16 --ip-range 172.20.240.0/20 zabbix-net

2\. Inicie uma instância vazia do PostgreSQL Server

    # docker run --name postgres-server -t \
          -e POSTGRES_USER="zabbix" \
          -e POSTGRES_PASSWORD="zabbix_pwd" \
          -e POSTGRES_DB="zabbix" \
          --network=zabbix-net \
          --restart unless-stopped \
          -d postgres:latest

3\. Inicie uma instância do Zabbix snmptraps

    # docker run --name zabbix-snmptraps -t \
          -v /zbx_instance/snmptraps:/var/lib/zabbix/snmptraps:rw \
          -v /var/lib/zabbix/mibs:/usr/share/snmp/mibs:ro \
          --network=zabbix-net \
          -p 162:1162/udp \
          --restart unless-stopped \
          -d zabbix/zabbix-snmptraps:alpine-5.4-latest

::: noteclassic
A instância do Zabbix snmptrap expõe a porta 162/UDP (SNMP
traps) para a máquina local.
:::

4\. Inicie a instância do Zabbix Server e a associe com a instância do PostgreSQL Server

    # docker run --name zabbix-server-pgsql -t \
          -e DB_SERVER_HOST="postgres-server" \
          -e POSTGRES_USER="zabbix" \
          -e POSTGRES_PASSWORD="zabbix_pwd" \
          -e POSTGRES_DB="zabbix" \
          -e ZBX_ENABLE_SNMP_TRAPS="true" \
          --network=zabbix-net \
          -p 10051:10051 \
          --volumes-from zabbix-snmptraps \
          --restart unless-stopped \
          -d zabbix/zabbix-server-pgsql:alpine-5.4-latest

::: noteclassic
A instância do Zabbix Server expõe a porta 10051/TCP (Zabbix
trapper) para a máquina local.
:::

5\. Inicie o Zabbix Web Interface e o associe às instâncias do 
PostgreSQL Server e Zabbix Server

    # docker run --name zabbix-web-nginx-pgsql -t \
          -e ZBX_SERVER_HOST="zabbix-server-pgsql" \
          -e DB_SERVER_HOST="postgres-server" \
          -e POSTGRES_USER="zabbix" \
          -e POSTGRES_PASSWORD="zabbix_pwd" \
          -e POSTGRES_DB="zabbix" \
          --network=zabbix-net \
          -p 443:8443 \
          -p 80:8080 \
          -v /etc/ssl/nginx:/etc/ssl/nginx:ro \
          --restart unless-stopped \
          -d zabbix/zabbix-web-nginx-pgsql:alpine-5.4-latest

::: noteclassic
A instância do Zabbix Web Interface expõe a porta 443/TCP
(HTTPS) para a máquina local.\
O diretório */etc/ssl/nginx* deve conter certificado com o nome requerido.
:::

**Exemplo 3**

O exemplo demonstra como executar o Zabbix Server com suporte a MySQL, 
Zabbix Web Interface baseada em Nginx Web Server e Zabbix
Java Gateway usando `podman` em Red Hat 8.

1\. Crie um novo pod com o nome `zabbix` e exponha as portas (web-interface,
Zabbix server trapper):

    podman pod create --name zabbix -p 80:8080 -p 10051:10051

2\. (optional) Inicie o contêiner do Zabbix Agent no pod `zabbix`:

    podman run --name zabbix-agent \
        -eZBX_SERVER_HOST="127.0.0.1,localhost" \
        --restart=always \
        --pod=zabbix \
        -d registry.connect.redhat.com/zabbix/zabbix-agent-50:latest

3\. Crie o diretório `./mysql/` na máquina e inicie o Oracle MySQL Server 8.0:

    podman run --name mysql-server -t \
          -e MYSQL_DATABASE="zabbix" \
          -e MYSQL_USER="zabbix" \
          -e MYSQL_PASSWORD="zabbix_pwd" \
          -e MYSQL_ROOT_PASSWORD="root_pwd" \
          -v ./mysql/:/var/lib/mysql/:Z \
          --restart=always \
          --pod=zabbix \
          -d mysql:8.0 \
          --character-set-server=utf8 --collation-server=utf8_bin \
          --default-authentication-plugin=mysql_native_password

3\. Inicie o contêiner do Zabbix Server:

    podman run --name zabbix-server-mysql -t \
                      -e DB_SERVER_HOST="127.0.0.1" \
                      -e MYSQL_DATABASE="zabbix" \
                      -e MYSQL_USER="zabbix" \
                      -e MYSQL_PASSWORD="zabbix_pwd" \
                      -e MYSQL_ROOT_PASSWORD="root_pwd" \
                      -e ZBX_JAVAGATEWAY="127.0.0.1" \
                      --restart=always \
                      --pod=zabbix \
                      -d registry.connect.redhat.com/zabbix/zabbix-server-mysql-50

4\. Inicie o contêiner do Zabbix Java Gateway:

    podman run --name zabbix-java-gateway -t \
          --restart=always \
          --pod=zabbix \
          -d registry.connect.redhat.com/zabbix/zabbix-java-gateway-50

5\. Inicie o contêiner do Zabbix Web-Interface:

    podman run --name zabbix-web-mysql -t \
                      -e ZBX_SERVER_HOST="127.0.0.1" \
                      -e DB_SERVER_HOST="127.0.0.1" \
                      -e MYSQL_DATABASE="zabbix" \
                      -e MYSQL_USER="zabbix" \
                      -e MYSQL_PASSWORD="zabbix_pwd" \
                      -e MYSQL_ROOT_PASSWORD="root_pwd" \
                      --restart=always \
                      --pod=zabbix \
                      -d registry.connect.redhat.com/zabbix/zabbix-web-mysql-50

::: noteclassic
O Pod `zabbix` expõe a porta 80/TCP (HTTP) para  a máquina local 
a partir da porta 8080/TCP do contêiner `zabbix-web-mysql`.
:::

[comment]: # ({/c8d23948-492bd3ba})

[comment]: # ({135f2a9b-c443c22e})
### Docker Compose

A Zabbix também fornece arquivos compose para definição e execução
multi-container dos componentes Zabbix em Docker. Estes arquivos compose
estão disponíveis no repositório oficial do Zabbix no github.com:
<https://github.com/zabbix/zabbix-docker>. Estes arquivos compose estão
disponíveis como exemplos, e estão sobrecarregados. Por exemplo, eles
contém proxies com suporte a MySQL e SQLite3.

Há algumas versões diferentes de arquivos compose:

|   |   |
|---|---|
|**Nome do arquivo**|**Descrição**|
|`docker-compose_v3_alpine_mysql_latest.yaml`|O arquivo compose executa a versão mais recente dos componentes do Zabbix 5.4 em Alpine Linux com suporte a MySQL.|
|`docker-compose_v3_alpine_mysql_local.yaml`|O arquivo compose constrói localmente a versão mais recente do Zabbix 5.4 e executa os componentes Zabbix em Alpine Linux com suporte a MySQL.|
|`docker-compose_v3_alpine_pgsql_latest.yaml`|O arquivo compose executa a versão mais recente dos componentes do Zabbix 5.4 em Alpine Linux com suporte a PostgreSQL.|
|`docker-compose_v3_alpine_pgsql_local.yaml`|O arquivo compose constrói localmente a versão mais recente do Zabbix 5.4 e executa os componentes Zabbix em Alpine Linux com suporte a PostgreSQL.|
|`docker-compose_v3_centos_mysql_latest.yaml`|O arquivo compose executa a versão mais recente dos componentes do Zabbix 5.4 em CentOS 8 com suporte a MySQL.|
|`docker-compose_v3_centos_mysql_local.yaml`|O arquivo compose constrói localmente a versão mais recente do Zabbix 5.4 e executa os componentes Zabbix em CentOS 8 com suporte a MySQL.|
|`docker-compose_v3_centos_pgsql_latest.yaml`|O arquivo compose executa a versão mais recente dos componentes do Zabbix 5.4 em CentOS 8 com suporte a PostgreSQL.|
|`docker-compose_v3_centos_pgsql_local.yaml`|O arquivo compose constrói localmente a versão mais recente do Zabbix 5.4 e executa os componentes Zabbix em CentOS 8 com suporte a PostgreSQL.|
|`docker-compose_v3_ubuntu_mysql_latest.yaml`|O arquivo compose executa a versão mais recente dos componentes do Zabbix 5.4 em Ubuntu 20.04 com suporte a MySQL.|
|`docker-compose_v3_ubuntu_mysql_local.yaml`|O arquivo compose constrói localmente a versão mais recente do Zabbix 5.4 e executa os componentes Zabbix em Ubuntu 20.04 com suporte a MySQL.|
|`docker-compose_v3_ubuntu_pgsql_latest.yaml`|O arquivo compose executa a versão mais recente dos componentes do Zabbix 5.4 em Ubuntu 20.04 com suporte a PostgreSQL.|
|`docker-compose_v3_ubuntu_pgsql_local.yaml`|O arquivo compose constrói localmente a versão mais recente do Zabbix 5.4 e executa os componentes Zabbix em Ubuntu 20.04 com suporte a PostgreSQL.|

::: noteimportant
Os arquivos compose disponíveis suportam a versão 3
do Docker Compose.
:::

[comment]: # ({/135f2a9b-c443c22e})

[comment]: # ({cb856d88-52e39127})
#### Armazenamento

Os arquivos compose são configurados para suportar armazenamento na máquina local.
O Docker Compose criará um diretório `zbx_env` na pasta com o arquivo compose
quando você executar os componentes do Zabbix utilizando um arquivo compose.
O diretório conterá a mesma estrutura como descrita acima na seção de [Volumes](#Volumes) 
e diretório para armazenamento de banco de dados.

Há também volumes no modo somente-leitura para os arquivos `/etc/localtime` e
`/etc/timezone`.

[comment]: # ({/cb856d88-52e39127})

[comment]: # ({087eacc7-0be3c140})
#### Arquivos de ambiente

No mesmo diretório com os arquivos compose no github.com você pode encontrar
arquivos com variáveis de ambiente padrão para cada componente no arquivo compose.
Estes arquivos de ambiente são nomeados como `.env_<type of component>`.

[comment]: # ({/087eacc7-0be3c140})

[comment]: # ({823656ef-ad55959c})
#### Exemplos

**Exemplo 1**

    # git checkout 5.4
    # docker-compose -f ./docker-compose_v3_alpine_mysql_latest.yaml up -d

O comando fará o download das imagens mais recentes do Zabbix 5.4 para cada
componente Zabbix e os executará de forma individual (detach mode).

::: noteimportant
Não esqueça de baixar os arquivos `.env_<type of component>` 
do repositório oficial do Zabbix no github.com com os arquivos compose.
:::

**Exemplo 2**

    # git checkout 5.4
    # docker-compose -f ./docker-compose_v3_ubuntu_mysql_local.yaml up -d

Este comando fará o download da imagem base Ubuntu 20.04 (focal), então 
construirá os componentes do Zabbix 5.4 localmente e os executará de forma
individual (detach mode).

[comment]: # ({/823656ef-ad55959c})
