[comment]: # translation:outdated

[comment]: # ({77261bae-8d986b3b})
# 1 Obtendo o Zabbix

[comment]: # ({/77261bae-8d986b3b})

[comment]: # ({0468b110-74ef7c01})
#### Visão geral

Há quatro formas de obter o Zabbix:

-   Instale-o através dos [pacotes de distribuição](install_from_packages#From_distribution_packages)
-   Baixe o arquivo fonte mais recente e o [compile você mesmo](install#Installation_from_sources)
-   Instale a partir dos [contêineres](containers)
-   Baixe a [aplicação virtual](/manual/appliance)

Para baixar os pacotes de distribuição mais recentes, fontes 
pré-compilados ou a aplicação virtual, acesse a 
[página de download do Zabbix](https://www.zabbix.com/download), onde links diretos para as versões
mais recentes são disponilizados.

[comment]: # ({/0468b110-74ef7c01})


[comment]: # ({c5c24adf-e88f42af})
#### Obtendo código fonte do Zabbix

Há várias maneiras de obter o código fonte do Zabbix:

-   Você pode [baixar](https://www.zabbix.com/download_sources) as versões estáveis publicadas no site oficial da Zabbix
-   Você pode [baixar](https://www.zabbix.com/developers) compilações noturnas da página oficial do desenvolvedor Zabbix
-   Você pode obter a versão de desenvolvimento mais recente no repositório de códigos do Git:
    -   A principal localização do repositório completo está em
        <https://git.zabbix.com/scm/zbx/zabbix.git>
    -   As versões Master e demais publicações suportadas também são espelhadas para o Github em
        <https://github.com/zabbix/zabbix>

Um cliente Git deve ser instalado para clonar o repositório. O pacote do cliente de linha de comando
oficial é comumente chamado **git** nas distribuições. Para instalar, por exemplo, no Debian/Ubuntu, execute:

    sudo apt-get update
    sudo apt-get install git

Para pegar todo o código do Zabbix, mude para o diretório onde você deseja 
gravar o código e execute:

    git clone https://git.zabbix.com/scm/zbx/zabbix.git

[comment]: # ({/c5c24adf-e88f42af})
