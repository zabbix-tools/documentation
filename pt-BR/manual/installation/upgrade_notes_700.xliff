<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="pt-BR" datatype="plaintext" original="manual/installation/upgrade_notes_700.md">
    <body>
      <trans-unit id="2ef2416e" xml:space="preserve">
        <source># 10 Upgrade notes for 7.0.0

These notes are for upgrading from Zabbix 6.4.x to Zabbix 7.0.0. All
notes are grouped into:

-   `Critical` - the most critical information related to the upgrade
    process and the changes in Zabbix functionality
-   `Informational` - all remaining information describing the changes
    in Zabbix functionality

It is possible to upgrade to Zabbix 7.0.0 from versions before Zabbix
6.4.0. See the [upgrade procedure](/manual/installation/upgrade) section
for all relevant information about upgrading from previous Zabbix
versions.

See also [upgrade instructions](/manual/concepts/server/ha#upgrading-ha-cluster) for servers in a **high-availability** (HA) cluster.</source>
      </trans-unit>
      <trans-unit id="2a68033f" xml:space="preserve">
        <source>### Critical

To complete successful Zabbix server upgrade on MySQL and MariaDB, you may require to set `GLOBAL log_bin_trust_function_creators = 1` in MySQL 
if binary logging is enabled, there are no superuser privileges and `log_bin_trust_function_creators = 1` is not set in MySQL configuration file. 

To set the variable using the MySQL console, run:

    mysql&gt; SET GLOBAL log_bin_trust_function_creators = 1;

Once the upgrade has been successfully completed, `log_bin_trust_function_creators` can be disabled:

    mysql&gt; SET GLOBAL log_bin_trust_function_creators = 0;</source>
      </trans-unit>
      <trans-unit id="31ba4eba" xml:space="preserve">
        <source>
### Concurrency in network discovery

Previously each network discovery rule would be processed by one discoverer process. Thus all service checks within the rule were performed 
sequentially. 

In the new version the network discovery process has been reworked to allow concurrency between service checks. 
A new discovery manager process has been added along with a configurable number of discovery workers (or threads). The discovery manager 
processes discovery rules and creates a discovery job per each rule with tasks (service checks). The service checks are picked up and 
performed by the discovery workers. Only those checks that have the same IP and port are scheduled sequentially because 
some devices may not allow concurrent connections on the same port. A new zabbix[discovery_queue] internal item allows to monitor the number of discovery checks in the queue.

The [StartDiscoverers](/manual/appendix/config/zabbix_server#startdiscoverers) parameter now determines the total number of available discovery workers for discovery. The default number of StartDiscoverers has been upped from 1 to 5, and the range from 0-250 to 0-1000. The `discoverer` processes from previous Zabbix versions have been dropped.

Additionally, the number of available workers per each rule is now configurable in the frontend. This parameter is optional. During the upgrade it will be set to "One" as in previous Zabbix versions. </source>
      </trans-unit>
      <trans-unit id="8e8f1e49" xml:space="preserve">
        <source>##### API changes

See the list of [API changes](/manual/api/changes_6.4_-_7.0) in Zabbix
7.0.0.</source>
      </trans-unit>
      <trans-unit id="57e1557d" xml:space="preserve">
        <source>### Informational</source>
      </trans-unit>
      <trans-unit id="62464d3d" xml:space="preserve">
        <source>##### Icons replaced by fonts

All icons in the frontend have been switched from icon image sheets to fonts.</source>
      </trans-unit>
      <trans-unit id="c483ec8d" xml:space="preserve">
        <source>##### Configuration parameters

Default value of the [BufferSize](/manual/appendix/config/zabbix_agent2) configuration parameter for Zabbix agent 2 has been increased from 100 to 1000.
This change does not affect installations where BufferSize is explicitly set.</source>
      </trans-unit>
      <trans-unit id="cf88e75a" xml:space="preserve">
        <source>##### Aggregate calculations

Several aggregate functions have been updated. Now:

-   Aggregate functions now also support non-numeric types for calculation. This may be useful, for example, with the [count](/manual/appendix/functions/aggregate#count) and [count_foreach](/manual/appendix/functions/aggregate/foreach#additional-parameters) functions.
-   The [count](/manual/appendix/functions/aggregate#count) and [count_foreach](/manual/appendix/functions/aggregate/foreach#additional-parameters) aggregate functions support optional parameters *operator* and *pattern*, which can be used to fine-tune item filtering and only count values that match given criteria.
-   All [foreach functions](/appendix/functions/aggregate/foreach) no longer include unsupported items in the count.
-   The function **[last_foreach](/appendix/functions/aggregate/foreach#time-period)**, previously configured to ignore the time period argument, accepts it as an optional parameter.</source>
      </trans-unit>
      <trans-unit id="e970b3e1" xml:space="preserve">
        <source>##### Old numeric (float) value type dropped

Since Zabbix 5.0.0, numeric (float) data type supports precision of approximately 15 digits and range from approximately
-1.79E+308 to 1.79E+308. This is implemented by default in new installations. Whereas for upgrading existing installations
that had been created before Zabbix 5.0 a database upgrade patch is applied automatically.

::: noteimportant
The patch will alter data columns of history and
trends tables, which usually contain lots of data, therefore it is
expected to take some time to complete. Since the exact estimate depends
on server performance, database management system configuration and
version, and it cannot be predicted, it is recommended to first test the
patch outside the production environment, even though with MySQL 8.0 and MariaDB 10.5 configured by default 
the patch is known to be executed instantly for large tables due to efficient algorithm and the fact that previously 
the same double type was used but with limited precision, meaning that data itself does not need to be modified.
:::

If you use Oracle database or older version of MySQL database, as well as in case of large installations,
the patch execution can take a lot of time, and therefore it is recommended to update the data type manually
before starting the upgrade.

Please execute the appropriate patch (SQL file) for your database; you may find these scripts in
the Zabbix Git repository for:

-   [MySQL](https://git.zabbix.com/projects/ZBX/repos/zabbix/raw/database/mysql/double.sql?at=refs%2Fheads%2Frelease%2F6.4)
-   [PostgreSQL](https://git.zabbix.com/projects/ZBX/repos/zabbix/raw/database/postgresql/double.sql?at=refs%2Fheads%2Frelease%2F6.4)
-   [Oracle](https://git.zabbix.com/projects/ZBX/repos/zabbix/raw/database/oracle/double.sql?at=refs%2Fheads%2Frelease%2F6.4)

::: notewarning
Important! Run these scripts for the server database only.
:::

To apply a patch:

- Stop Zabbix server.
- Run the script for your database.
- Start Zabbix server again. 

Note that with TimescaleDB the [compression support](/manual/appendix/install/timescaledb#timescaledb_compression) must only be turned on after applying this patch.</source>
      </trans-unit>
      <trans-unit id="176664ec" xml:space="preserve">
        <source>
### Updated items

The **[wmi.get](/manual/config/items/itemtypes/zabbix_agent/win_keys#wmi-get)** and **[wmi.getall](/manual/config/items/itemtypes/zabbix_agent/win_keys#wmi-getall)** used with Zabbix agent 2 now return JSON with boolean values represented as strings (for example, `"RealTimeProtectionEnabled": "True"` instead of `"RealTimeProtectionEnabled": true` returned previously) to match the output format of these items on Zabbix agent.

The [oracle.ts.stats](/manual/config/items/itemtypes/zabbix_agent/zabbix_agent2#oracle.ts.stats) Zabbix agent 2 item has a new **conname** parameter to specify the target container name.
The JSON format of the returned data has been updated. When no **tablespace**, **type**, or **conname** is specified in the key parameters, the returned data will include an additional JSON level with the container name, allowing differentiation between containers.

For the list of item changes that do no break compatibility, see [What's new in Zabbix 7.0.0](/manual/introduction/whatsnew700#items).</source>
      </trans-unit>
    </body>
  </file>
</xliff>
