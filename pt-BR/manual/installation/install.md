[comment]: # translation:outdated

[comment]: # ({e50dda34-af2eaf2f})
# 3 Instalação a partir do código-fonte

É possível acessar a versão mais recente do Zabbix compilando-a a partir do código-fonte. 

A seguir, fornecemos um tutorial com os passos para instalação do Zabbix a partir do código-fonte.

[comment]: # ({/e50dda34-af2eaf2f})

[comment]: # ({336396b7-0040d992})
#### - Instalando daemons do Zabbix

[comment]: # ({/336396b7-0040d992})

[comment]: # ({new-8cc2c1b1})
##### 1 Download do arquivo fonte

Vá até a página de [download do Zabbix](http://www.zabbix.com/download_sources)
e baixe o arquivo fonte. Uma vez baixado, extraia os fontes, executando:

    $ tar -zxvf zabbix-6.0.0.tar.gz

::: notetip
Informe a versão correta do Zabbix no comando. Ela deve
corresponder ao nome do arquivo baixado.
:::

[comment]: # ({/new-8cc2c1b1})

[comment]: # ({d45b4342-87e1101d})
##### 2 Criar conta de usuário

Para todos os processos de daemon do Zabbix, um usuário sem privilégios
é requirido. Se um daemon Zabbix é iniciado a partir de uma conta de usuário
sem privilégios, ele será executado como tal usuário.

No entando, se um daemon é iniciado a partir de uma conta 'root', ele mudará
para uma conta de usuário 'zabbix', que deve existir. Para criar esta conta de
usuário (em seu próprio grupo, "zabbix"),

em sistemas baseados em RedHat, execute:

    groupadd --system zabbix
    useradd --system -g zabbix -d /usr/lib/zabbix -s /sbin/nologin -c "Zabbix Monitoring System" zabbix

em sistemas baseados em Debian, execute:

    addgroup --system --quiet zabbix
    adduser --quiet --system --disabled-login --ingroup zabbix --home /var/lib/zabbix --no-create-home zabbix

::: noteimportant
Os processos Zabbix não precisam de um diretório home,
sendo este o motivo de não recomendarmos a sua criação. No entanto,
se você estiver usando alguma funcionalidade que demande tal diretório
(p.e. armazenar credenciais MySQL em `$HOME/.my.cnf`), você está livre
para criá-lo usando os seguintes comandos: \
\

Em sistemas baseados em RedHat, execute:

    mkdir -m u=rwx,g=rwx,o= -p /usr/lib/zabbix
    chown zabbix:zabbix /usr/lib/zabbix

Em sistemas baseados em Debian, execute:

    mkdir -m u=rwx,g=rwx,o= -p /var/lib/zabbix
    chown zabbix:zabbix /var/lib/zabbix


:::

Não é necessária uma conta de usuário separada para
a instalação do Zabbix Frontend.

Se o Zabbix [Server](/manual/concepts/server) e [Agent](/manual/concepts/agent) são executados na mesma máquina 
é recomendado usar um usuário diferente para executar o server 
daquele usado para o agent. Caso contrário, se ambos são executados
com o mesmo usuário, o agent pode acessar os arquivos de configuração
do server e assim qualquer usuário do Zabbix com perfil de Admin
pode facilmente, por exemplo, descobrir a senha do banco de dados.

::: noteimportant
Executar o Zabbix como `root`, `bin`, ou qualquer outra
conta com permissões especiais é um risco à segurança.
:::

[comment]: # ({/d45b4342-87e1101d})

[comment]: # ({033ad05f-06256db0})
##### 3 Criar banco de dados Zabbix

Para os daemons do Zabbix [Server](/manual/concepts/server) e
[Proxy](/manual/concepts/proxy), bem como para o Zabbix Frontend, um
banco de dados é necessário. No entanto não é necessário 
para executar o Zabbix [Agent](/manual/concepts/agent).


[Scripts SQL são disponibilizados](/manual/appendix/install/db_scripts) para criação do schema 
de banco de dados e carga do conjunto de dados iniciais. 
O banco de dados do Zabbix Proxy necessita apenas do schema 
enquanto que o banco de dados do Zabbix Server requer também
o conjunto de dados junto ao schema.

Tendo criado um banco de dados para o Zabbix,
prossiga para os próximos passos de compilação do Zabbix.


[comment]: # ({/033ad05f-06256db0})

[comment]: # ({37808240-8ff1b41c})
##### 4 Configurar os fontes

Ao configurar os fontes para o Zabbix Server ou Proxy, você deve especificar 
o tipo de banco de dados a ser usado. Apenas um tipo de banco de dados 
pode ser compilado com um processo Server ou Proxy por vez.

Para ver todas as opções de configuração suportadas, dentro do diretório
de fonte do Zabbix extraído, execute:

    ./configure --help

Para configurar os fontes para o Zabbix Server e Agent, você pode executar
algo como o seguinte:

    ./configure --enable-server --enable-agent --with-mysql --enable-ipv6 --with-net-snmp --with-libcurl --with-libxml2 --with-openipmi

Para configurar os fontes para o Zabbix Server (com PostgreSQL, etc.), você
pode usar:

    ./configure --enable-server --with-postgresql --with-net-snmp

Para configurar os fontes para Zabbix Proxy (com SQLite, etc.), você pode executar:

    ./configure --prefix=/usr --enable-proxy --with-net-snmp --with-sqlite3 --with-ssh2

Para configurar os fontes para o Zabbix Agent, você pode utilizar:

    ./configure --enable-agent

ou, para Zabbix Agent 2:

    ./configure --enable-agent2

::: noteclassic
Um ambiente Go configurado com uma [versão atualmente suportada](https://golang.org/doc/devel/release#policy)
é necessário para compilar o Zabbix Agent 2. Consulte 
[golang.org](https://golang.org/doc/install) para instruções de instalação.
:::

Notas sobre as opções de compilação:

-   Os utilitários de linha de comando zabbix\_get e zabbix\_sender
    são compilados se a opção --enable-agent é usada.
-   As opções de configuração --with-libcurl e --with-libxml2 são necessárias
    para monitoramento de máquinas virtuais; --with-libcurl também é necessário
    para autenticação SMTP e [itens](/manual/config/items/itemtypes/zabbix_agent) `web.page.*` do Zabbix Agent.
    Note que o cURL na versão 7.20.0 ou maior é [necessário](/manual/installation/requirements)
    com a opção de configuração --with-libcurl.
-   O Zabbix sempre compila com a biblioteca PCRE (desde a versão 3.4.0);
    sua instalação não é opcional. --with-libpcre=\[DIR\] apenas permite o 
    direcionamento para um diretório de instalação específico, em vez de buscar 
    entre um número de lugares comuns pelos arquivos da biblioteca libpcre.
-   Você pode usar o marcador --enable-static para vincular bibliotecas de forma estática.
    Se você planeja distribuir binários compilados entre diferentes servidores,
    você deve usar este marcador para que tais binários funcionem sem as bibliotecas 
    exigidas. Note que --enable-static não funciona em [Solaris](https://docs.oracle.com/cd/E18659_01/html/821-1383/bkajp.html).
-   O uso da opção --enable-static não é recomendado quanto compilando o 
    server. Para compilar o server estaticamente, você deve ter uma versão
    estática de cada biblioteca externa necessária. Não há uma verificação 
    rigorosa quanto a isso no script de configuração.
-   Adicione um caminho opcional para o arquivo de configuração do MySQL
    --with-mysql=/<path\_to\_the\_file>/mysql\_config para selecionar
    a biblioteca de cliente MySQL desejada quando houver necessidade de utilizar uma
    que não esteja localizada no local padrão. Isto é útil quando há várias versões
    de MySQL instaladas ou um MariaDB instalado junto ao MySQL no mesmo sistema.
-   Use o marcador --with-oracle para especificar a localização da API OCI.

::: noteimportant
 Se ./configure falhar devido bibliotecas ausentes ou
alguma outra circunstância, por favor verifique o `config.log` para mais
detalhes quanto ao erro. Por exemplo, se `libssl` está ausente,  a mensagem
de erro imediata pode ser confusa:

    checking for main in -lmysqlclient... no
    configure: error: Not found mysqlclient library

Enquanto que o `config.log` possui uma descrição mais detalhada:

    /usr/bin/ld: cannot find -lssl
    /usr/bin/ld: cannot find -lcrypto


:::

Veja também:

-   [Compilando Zabbix com suporte à criptografia](/manual/encryption#compiling_zabbix_with_encryption_support)
-   [Problemas conhecidos](/manual/installation/known_issues#compiling_zabbix_agent_on_hp-ux) na compilação do Zabbix Agent em HP-UX

[comment]: # ({/37808240-8ff1b41c})

[comment]: # ({f36c02ac-08edd3dc})
##### 5 Make e install para tudo

::: noteclassic
Se instalando através do [repositório Git do Zabbix](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse), 
é necessário primeiro executar:

`$ make dbschema` 
:::

    make install

Esta etapa deve ser executada usando um usuário com permissões suficientes
(comumente 'root', ou utilizando elevação de usuário com `sudo`).

A execução do comando `make install` instalará, por padrão, os binários dos daemons
(zabbix\_server, zabbix\_agentd, zabbix\_proxy) em /usr/local/sbin e os binários
de client (zabbix\_get, zabbix\_sender) em /usr/local/bin.

::: noteclassic
Para especificar um local diferente de /usr/local, use a chave
--prefix na etapa anterior de configuração dos fontes, por exemplo
--prefix=/home/zabbix. Neste caso os binários dos daemons seriam instalados
dentro de <prefix>/sbin, enquanto que os utilitários em <prefix>/bin. 
As páginas de manual seriam instaladas em <prefix>/share.
:::

[comment]: # ({/f36c02ac-08edd3dc})

[comment]: # ({4fffa1b1-112b74f7})
##### 6 Revise e edite os arquivos de configuração

-   edite o arquivo de configuração do Zabbix Agent
    **/usr/local/etc/zabbix\_agentd.conf**

Você precisa configurar este arquivo para cada máquina com 
zabbix\_agentd instalado.

Você deve especificar o **endereço IP** do servidor Zabbix no arquivo.
Conexões de outros endereços serão negadas.

-   edite o arquivo de configuração do Zabbix Server
    **/usr/local/etc/zabbix\_server.conf**

Você deve especificar o nome do banco de dados, usuário e senha (se usando alguma).

O restante dos parâmetros o atenderão com seus valores padrão se você tiver
um ambiente pequeno (até 10 máquinas monitoradas). Todavia, você deve alterar os
parâmetros padrão se você pretende maximizar a performance do Zabbix
Server (ou Proxy). Consulte a seção [ajustes de performance](/manual/appendix/performance_tuning) para mais detalhes.

-   se você tem instalado um Zabbix Proxy, edite o arquivo de configuração
    **/usr/local/etc/zabbix\_proxy.conf**

Você deve especificar o endereço IP e o nome do servidor do Zabbix Proxy (que deve ser conhecido
pelo Zabbix Server), bem como o nome do banco de dados, usuário e senha (se usando alguma).

::: noteclassic
Com SQLite deve ser especificado o caminho completo para o arquivo de banco de dados;
Usuário e senha do banco de dados não são exigidos.
:::

[comment]: # ({/4fffa1b1-112b74f7})

[comment]: # ({f96a2ba2-bbdd82ac})
##### 7 Inicie os daemons

Execute o zabbix\_server no lado do servidor:

    shell> zabbix_server

::: noteclassic
Certifique-se de que seu sistema permita a alocação de 36MB (ou um pouco
mais) de memória compartilhada, caso contrário o Server pode não iniciar e você
verá a mensagem "Impossível alocar memória compartilhada para <tipo de cache>." 
(se log em inglês: "Cannot allocate shared memory for <type of cache>.") no arquivo de log.
Isto pode ocorrer com FreeBSD, Solaris 8.\
Verifique a seção ["Veja também"](#see_also) ao final desta página para descobrir como
configurar a memória compartilhada.
:::

Execute zabbix\_agentd em todas as máquinas monitoradas:

    shell> zabbix_agentd

::: noteclassic
Certifique-se de que seu sistema permita a alocação de 2MB de memória compartilhada, 
caso contrário o Agente pode não iniciar e você verá a mensagem "Impossível alocar memória
compartilhada para o coletor" ( se log em inglês: "Cannot allocate shared memory for collector.") 
no arquivo de log.
Isto pode ocorrer em Solaris 8.
:::

Se você tem instalado o Zabbix Proxy, execute zabbix\_proxy.

    shell> zabbix_proxy

[comment]: # ({/f96a2ba2-bbdd82ac})

[comment]: # ({dc5bd581-c9f154ca})
#### - Instalação da interface web do Zabbix

[comment]: # ({/dc5bd581-c9f154ca})

[comment]: # ({f8ad3334-ed4e56ce})
##### Copiando arquivos PHP

O frontend do Zabbix é escrito em PHP, assim, para executá-lo, é necessário 
um webserver com suporte ao PHP.  A instalação é feita com a simples cópia
dos arquivos PHP do diretório ui para o diretório de documentos HTML 
do webserver.

Locais comuns para o diretório de documentos HTML no Apache Web Server 
incluem:

-   /usr/local/apache2/htdocs (diretório padrão quando instalando o Apache
    a partir do fonte)
-   /srv/www/htdocs (OpenSUSE, SLES)
-   /var/www/html (Debian, Ubuntu, Fedora, RHEL, CentOS)

Sugere-se usar um subdiretório em vez da raíz do diretório HTML.
Para criar um subdiretório e copiar os arquivos do frontend do Zabbix
para dentro dele, execute os seguintes comandos, informando o diretório raíz
junto ao nome do subdiretório que se prentede criar:

    mkdir <htdocs>/zabbix
    cd ui
    cp -a . <htdocs>/zabbix

Para mais instruções quando usando idioma diferente do Inglês, consulte
[Instalação de idiomas de frontend adicionais](/manual/appendix/install/locales).

[comment]: # ({/f8ad3334-ed4e56ce})

[comment]: # ({11b70825-63583cbb})
##### Instalação do frontend

Por favor, consulte a página [Instalação da interface web](/manual/installation/frontend)
para se informar sobre o assitente de instalação do Zabbix frontend.

[comment]: # ({/11b70825-63583cbb})

[comment]: # ({724b5133-1a519c06})
#### 3 Instalação do Java Gateway

A instalação do Java Gateway é exigida apenas se você pretende monitorar
aplicações JMX. O Java Gateway é leve e não necessita de um banco de dados.

Para instalar a partir dos fontes, primeiro faça o [download](/manual/installation/install#download_the_source_archive) 
e então extraia o arquivo fonte.

Para compilar o Java Gateway, execute o script `./configure` com
a opção `--enable-java`. É aconselhável que você especifique `--prefix`
para direcionar a instalação para outro local diferente do padrão /usr/local,
porque a instalação criará uma árvore de diretórios completa, e não 
apenas um simples executável.

    $ ./configure --enable-java --prefix=$PREFIX

Para compilar e empacotar o Java Gateway em um arquivo JAR, execute `make`. 
Note que para este processo você necessitará dos executáveis `javac` e `jar` 
no seu path.

    $ make

Agora você tem um arquivo zabbix-java-gateway-$VERSION.jar em
src/zabbix\_java/bin. Se você se sente confortável em executar o
Java Gateway a partir de src/zabbix\_java no diretório da distribuição, 
então você pode prosseguir às instruções de configuração e execução
do [Java Gateway](/manual/concepts/java#overview_of_files_in_java_gateway_distribution).
Caso contrário, certifique-se de possuir os privilégios necessários 
e execute `make install`.

    $ make install

Prossiga até [Configuração inicial](/manual/concepts/java/from_sources) para mais detalhes na configuração e 
execução do Java Gateway.

[comment]: # ({/724b5133-1a519c06})

[comment]: # ({24ac5736-76c01064})
#### 4 Instalando o serviço web Zabbix

A instalação do serviço web Zabbix só é necessária se você quiser usar[relatório agendado](/manual/web_interface/frontend_sections/reports/scheduled).

Para instalar a partir de fontes, primeiro
[download](/manual/installation/install#download_the_source_archive) e extraia o arquivo com as fontes .

Para compilar o serviço web Zabbix, execute o script `./configure` com opção `--enable-webservice` .

::: noteclassic
 Uma versão[Go](https://golang.org/doc/install) configurada com ambiente 1.13+é necessário para construir o serviço web Zabbix.

:::

Execute zabbix\_web\_service na máquina onde o serviço web está instalado:

    shell> zabbix_web_service

Prossiga para [configuração](/manual/appendix/install/web_service) para obter mais detalhes sobre como configurar a geração de relatórios agendados.

[comment]: # ({/24ac5736-76c01064})

