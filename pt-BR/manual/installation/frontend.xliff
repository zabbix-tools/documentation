<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="pt-BR" datatype="plaintext" original="manual/installation/frontend.md">
    <body>
      <trans-unit id="9cceae9f" xml:space="preserve">
        <source># 6 Web interface installation

This section provides step-by-step instructions for installing Zabbix
web interface. Zabbix frontend is written in PHP, so to run it a PHP
supported webserver is needed.</source>
        <target state="needs-translation">
# 6 Instalação da interface web

Esta seção provê instruções passo a passo para a instalação da interface 
web do Zabbix. O Zabbix Frontend é escrito em PHP, então para seu
funcionamento é necessário um servidor web com suporte ao PHP.</target>
      </trans-unit>
      <trans-unit id="0c649c0e" xml:space="preserve">
        <source>:::notetip
You can find out more about setting up SSL for Zabbix frontend by referring to these [best practices](/manual/installation/requirements/best_practices).
:::</source>
      </trans-unit>
      <trans-unit id="6b173526" xml:space="preserve">
        <source>#### Welcome screen

Open Zabbix frontend URL in the browser. If you have installed Zabbix
from packages, the URL is:

-   for Apache: *http://&lt;server\_ip\_or\_name&gt;/zabbix*
-   for Nginx: *http://&lt;server\_ip\_or\_name&gt;*

You should see the first screen of the frontend installation wizard.

Use the *Default language* drop-down menu to change system default
language and continue the installation process in the selected language
(optional). For more information, see [Installation of additional
frontend languages](/manual/appendix/install/locales).

![](../../../assets/en/manual/installation/install_1.png){width="550"}</source>
        <target state="needs-translation">
#### Tela de boas-vindas

Abra a URL do Zabbix Frontend no navegador. Se você instalou o Zabbix 
a partir dos pacotes, a URL é:

-   para Apache: *http://&lt;server\_ip\_or\_name&gt;/zabbix*
-   para Nginx: *http://&lt;server\_ip\_or\_name&gt;*

Você deverá ver a primeira tela do assistente de instalação do Frontend.

Utilize o menu *Default language* (idioma padrão) para alterar o idioma padrão do sistema e
continuar com o processo de instalação no idioma selecionado (opcional). Para mais 
informações, consulte [Instalação de idiomas adicionais para o Frontend](/manual/appendix/install/locales).

![](../../../assets/en/manual/installation/install_1.png){width="550"}</target>
      </trans-unit>
      <trans-unit id="2458c643" xml:space="preserve">
        <source>#### Check of pre-requisites

Make sure that all software prerequisites are met.

![](../../../assets/en/manual/installation/install_2.png){width="550"}

|Pre-requisite|Minimum value|Description|
|--|--|------|
|*PHP version*|7.4.0| |
|*PHP memory\_limit option*|128MB|In php.ini:&lt;br&gt;memory\_limit = 128M|
|*PHP post\_max\_size option*|16MB|In php.ini:&lt;br&gt;post\_max\_size = 16M|
|*PHP upload\_max\_filesize option*|2MB|In php.ini:&lt;br&gt;upload\_max\_filesize = 2M|
|*PHP max\_execution\_time option*|300 seconds (values 0 and -1 are allowed)|In php.ini:&lt;br&gt;max\_execution\_time = 300|
|*PHP max\_input\_time option*|300 seconds (values 0 and -1 are allowed)|In php.ini:&lt;br&gt;max\_input\_time = 300|
|*PHP session.auto\_start option*|must be disabled|In php.ini:&lt;br&gt;session.auto\_start = 0|
|*Database support*|One of: MySQL, Oracle, PostgreSQL.|One of the following modules must be installed:&lt;br&gt;mysql, oci8, pgsql|
|*bcmath*| |php-bcmath|
|*mbstring*| |php-mbstring|
|*PHP mbstring.func\_overload option*|must be disabled|In php.ini:&lt;br&gt;mbstring.func\_overload = 0|
|*sockets*| |php-net-socket. Required for user script support.|
|*gd*|2.0.28|php-gd. PHP GD extension must support PNG images (*--with-png-dir*), JPEG (*--with-jpeg-dir*) images and FreeType 2 (*--with-freetype-dir*).|
|*libxml*|2.6.15|php-xml|
|*xmlwriter*| |php-xmlwriter|
|*xmlreader*| |php-xmlreader|
|*ctype*| |php-ctype|
|*session*| |php-session|
|*gettext*| |php-gettext&lt;br&gt;Since Zabbix 2.2.1, the PHP gettext extension is not a mandatory requirement for installing Zabbix. If gettext is not installed, the frontend will work as usual, however, the translations will not be available.|

Optional pre-requisites may also be present in the list. A failed
optional prerequisite is displayed in orange and has a *Warning* status.
With a failed optional pre-requisite, the setup may continue.

::: noteimportant
If there is a need to change the Apache user or
user group, permissions to the session folder must be verified.
Otherwise Zabbix setup may be unable to continue.
:::</source>
        <target state="needs-translation">
#### Verificação de pré-requisitos

Certifique-se de que todos os pré-requisitos são atendidos.

![](../../../assets/en/manual/installation/install_2.png){width="550"}

|Pré-requisito|Valor mínimo|Descrição|
|-------------|-------------|-----------|
|*Versão PHP*|7.2.5|&lt;|
|*Opção PHP memory\_limit*|128MB|Em php.ini:&lt;br&gt;memory\_limit = 128M|
|*Opção PHP post\_max\_size*|16MB|Em php.ini:&lt;br&gt;post\_max\_size = 16M|
|*Opção PHP upload\_max\_filesize*|2MB|Em php.ini:&lt;br&gt;upload\_max\_filesize = 2M|
|*Opção PHP max\_execution\_time*|300 segundos (valores 0 e -1 são permitidos)|Em php.ini:&lt;br&gt;max\_execution\_time = 300|
|*Opção PHP max\_input\_time*|300 segundos (valores 0 e -1 são permitidos)|Em php.ini:&lt;br&gt;max\_input\_time = 300|
|*Opção PHP session.auto\_start*|Deve estar desabilitada|Em php.ini:&lt;br&gt;session.auto\_start = 0|
|*Suporte banco de dados*|Um de: MySQL, Oracle, PostgreSQL.|Um dos seguintes módulos deve estar instalado:&lt;br&gt;mysql, oci8, pgsql|
|*bcmath*|&lt;|php-bcmath|
|*mbstring*|&lt;|php-mbstring|
|*Opção PHP mbstring.func\_overload*|Deve estar desabilitada|Em php.ini:&lt;br&gt;mbstring.func\_overload = 0|
|*sockets*|&lt;|php-net-socket. Exigido para suporte a script de usuário.|
|*gd*|2.0.28|php-gd. A extensão PHP GD deve suportar imagens PNG (*--with-png-dir*), imagens JPEG (*--with-jpeg-dir*) e FreeType 2 (*--with-freetype-dir*).|
|*libxml*|2.6.15|php-xml|
|*xmlwriter*|&lt;|php-xmlwriter|
|*xmlreader*|&lt;|php-xmlreader|
|*ctype*|&lt;|php-ctype|
|*session*|&lt;|php-session|
|*gettext*|&lt;|php-gettext&lt;br&gt;Desde o Zabbix 2.2.1, a extensão PHP gettext não é um requisito obrigatório para a instalação do Zabbix. Se gettext não estiver instalado, o Frontend funcionará normalmente, porém, as traduções não estarão disponíveis.|

Pré-requisitos opcionais também podem estar presentes na lista. Um pré-requisito
opcional com falha será mostrado em laranja e apresentado como *Warning*.
Com falha em um pré-requisito opcional, a configuração pode continuar.

::: noteimportant
Se houver necessidade de alterar o usuário ou grupo de usuário do Apache, 
as permissões para a pasta de sessão devem ser verificadas.
Caso contrário, a configuração do Zabbix pode ficar impedida de continuar.
:::</target>
      </trans-unit>
      <trans-unit id="879dc06f" xml:space="preserve">
        <source>#### Configure DB connection

Enter details for connecting to the database. Zabbix database must
already be created.

![](../../../assets/en/manual/installation/install_3a.png){width="550"}

If the *Database TLS encryption* option is checked, then additional
fields for [configuring the TLS
connection](/manual/appendix/install/db_encrypt) to the database appear
in the form (MySQL or PostgreSQL only).

If *Store credentials in* is set to HashiCorp Vault or CyberArk Vault,
additional parameters will become available:

- for [HashiCorp Vault](/manual/config/secrets/hashicorp): Vault API endpoint, secret path and authentication token;

- for [CyberArk Vault](/manual/config/secrets/cyberark): Vault API endpoint, secret query string and certificates. Upon marking *Vault certificates* checkbox, two new fields for specifying paths to SSL certificate file and SSL key file will appear.

![](../../../assets/en/manual/installation/install_3b.png){width="550"}</source>
        <target state="needs-translation">
#### Configuração de conexão com banco de dados

Informe os dados de conexão com o banco de dados. O banco de dados
do Zabbix já deve estar criado.

![](../../../assets/en/manual/installation/install_3a.png){width="550"}

Se a opção *Database TLS encryption* (criptografia TLS do banco de dados) 
estiver ativada, então campos adicionais para [configuração da conexão TLS](/manual/appendix/install/db_encrypt) 
com o banco de dados serão mostrados no formulário (apenas para MySQL 
ou PostgreSQL).

Se a opção HashiCorp Vault estiver selecionada para armazenamento de 
credenciais, campos adicionais estarão disponíveis para especificar o
endpoint da API do Vault, secret path e token de autenticação:

![](../../../assets/en/manual/installation/install_3b.png){width="550"}</target>
      </trans-unit>
      <trans-unit id="98277238" xml:space="preserve">
        <source>#### Settings

Entering a name for Zabbix server is optional, however, if submitted, it
will be displayed in the menu bar and page titles.

Set the default [time zone](/manual/web_interface/time_zone#overview)
and theme for the frontend.

![](../../../assets/en/manual/installation/install_4.png){width="550"}</source>
        <target state="needs-translation">
#### Configurações

Informar um nome para o Zabbix Server é opcional, no entanto, caso 
informado, ele será mostrado na barra de menu e títulos de página.

Configure o [time zone](/manual/web_interface/time_zone#overview) padrão e o tema do Frontend.

![](../../../assets/en/manual/installation/install_4.png){width="550"}</target>
      </trans-unit>
      <trans-unit id="42398398" xml:space="preserve">
        <source>#### Pre-installation summary

Review a summary of settings.

![](../../../assets/en/manual/installation/install_5.png){width="550"}</source>
        <target state="needs-translation">
#### Resumo pré-instalação

Revise o resumo das configurações antes da instalação efetiva.

![](../../../assets/en/manual/installation/install_5.png){width="550"}</target>
      </trans-unit>
      <trans-unit id="1124dd9e" xml:space="preserve">
        <source>#### Install

If installing Zabbix from sources, download the configuration file and
place it under conf/ in the webserver HTML documents subdirectory where
you copied Zabbix PHP files to.

![](../../../assets/en/manual/installation/install_6.png){width="550"}

![](../../../assets/en/manual/installation/saving_zabbix_conf.png){width="350"}

::: notetip
Providing the webserver user has write access to conf/
directory the configuration file would be saved automatically and it
would be possible to proceed to the next step right away.
:::

Finish the installation.

![](../../../assets/en/manual/installation/install_7.png){width="550"}</source>
        <target state="needs-translation">
#### Instalação

Se instalando o Zabbix a partir dos fontes, baixe o arquivo de configuração
e salve-o dentro de conf/ no subdiretório de documentos HTML do Web 
Server onde você colocou os arquivos PHP do Zabbix.

![](../../../assets/en/manual/installation/install_6.png){width="550"}

![](../../../assets/en/manual/installation/saving_zabbix_conf.png){width="350"}

::: notetip
Garantindo que o usuário do Web Server tenha acesso de escrita
no diretório conf/ o arquivo de configuração será salvo automaticamente
e será possível continuar para o próximo passo imediatamente.
:::

Finalize a instalação.

![](../../../assets/en/manual/installation/install_7.png){width="550"}</target>
      </trans-unit>
      <trans-unit id="d59dc4b9" xml:space="preserve">
        <source>#### Log in

Zabbix frontend is ready! The default user name is **Admin**, password
**zabbix**.

![](../../../assets/en/manual/quickstart/login.png){width="350"}

Proceed to [getting started with Zabbix](/manual/quickstart/login).</source>
        <target state="needs-translation">
#### Log in

O Zabbix Frontend está pronto! O nome de usuário padrão é **Admin**, 
e senha **zabbix**.

![](../../../assets/en/manual/quickstart/login.png){width="350"}

Agora é só [começar a usar o Zabbix](/manual/quickstart/login).</target>
      </trans-unit>
    </body>
  </file>
</xliff>
