[comment]: # translation:outdated

[comment]: # ({208aabce-22c598e9})
# Construindo Agente Zabbix 2 no Windows

[comment]: # ({/208aabce-22c598e9})

[comment]: # ({e1867ec1-84651997})
#### Visão geral

Esta seção demonstra como construir o Zabbix agent 2 (Windows) a partir de
fontes.

[comment]: # ({/e1867ec1-84651997})

[comment]: # ({87aaf70b-2a4da91c})
#### Instalando o compilador MinGW

1\. Download MinGW-w64 with SJLJ (set jump/long jump) Manipulação de exceção
e tópicos do Windows (for example
*x86\_64-8.1.0-release-win32-sjlj-rt\_v6-rev0.7z*)\
2. Extract and move to *c:\\mingw*\
3. Setup environmental variable

    @echo off
    set PATH=%PATH%;c:\mingw\bin
    cmd

Ao compilar, use o prompt do Windows em vez do terminal MSYS fornecido por
MinGW

[comment]: # ({/87aaf70b-2a4da91c})

[comment]: # ({8fce1265-dec81522})
#### Compilando bibliotecas de desenvolvimento PCRE

As instruções a seguir irão compilar e instalar o PCRE de 64 bits
bibliotecas em *c:\\dev\\pcre* e bibliotecas de 32 bits em *c:\\dev\\pcre32*:

1\. Baixe a biblioteca PCRE versão 8.XX em pcre.org
(<http://ftp.pcre.org/pub/pcre/>) e extrair\
2. Abra *cmd* e navegue até as fontes extraídas

[comment]: # ({/8fce1265-dec81522})

[comment]: # ({new-5def516d})
##### Build 64bit PCRE

1\. Delete old configuration/cache if exists:

    del CMakeCache.txt
    rmdir /q /s CMakeFiles

2\. Run cmake (CMake can be installed from
<https://cmake.org/download/>):

    cmake -G "MinGW Makefiles" -DCMAKE_C_COMPILER=gcc -DCMAKE_C_FLAGS="-O2 -g" -DCMAKE_CXX_FLAGS="-O2 -g" -DCMAKE_INSTALL_PREFIX=c:\dev\pcre

3\. Next, run:

    mingw32-make clean
    mingw32-make install

[comment]: # ({/new-5def516d})

[comment]: # ({new-04b79926})
##### Build 32bit PCRE

1\. Run:

    mingw32-make clean

2\. Delete *CMakeCache.txt*:

    del CMakeCache.txt
    rmdir /q /s CMakeFiles

3\. Run cmake:

    cmake -G "MinGW Makefiles" -DCMAKE_C_COMPILER=gcc -DCMAKE_C_FLAGS="-m32 -O2 -g" -DCMAKE_CXX_FLAGS="-m32 -O2 -g" -DCMAKE_EXE_LINKER_FLAGS="-Wl,-mi386pe" -DCMAKE_INSTALL_PREFIX=c:\dev\pcre32

4\. Next, run:

    mingw32-make install

[comment]: # ({/new-04b79926})

[comment]: # ({e578cb3e-80e7f73a})
#### Instalando bibliotecas de desenvolvimento OpenSSL
1\. Download versões 32 e 64 bit através de  <https://curl.se/windows/>\
2. Extraia os arquivos em *c:\\dev\\openssl32* e*c:\\dev\\openssl*
directories accordingly.\
3. Depois disso, remova *\*.dll.a* (bibliotecas de wrapper de chamadas dll) já que o MinGW os prioriza antes das bibliotecas estáticas.

[comment]: # ({/e578cb3e-80e7f73a})

[comment]: # ({62110f9f-f4af7b47})
#### Compilando agente Zabbix  2

[comment]: # ({/62110f9f-f4af7b47})

[comment]: # ({38e8b438-5f096f18})
##### 32 bit

Abra o ambiente MinGW (prompt de comando do Windows) e navegue até
*build/mingw* diretório de fonte  Zabbix .

Execute:

    mingw32-make clean
    mingw32-make ARCH=x86 PCRE=c:\dev\pcre32 OPENSSL=c:\dev\openssl32

[comment]: # ({/38e8b438-5f096f18})

[comment]: # ({f8fc15d0-f7876f1d})
##### 64 bit

Abra o ambiente MinGW (prompt de comando do Windows) e navegue até
*build/mingw* diretório de fonte  Zabbix .


Execute:

    mingw32-make clean
    mingw32-make PCRE=c:\dev\pcre OPENSSL=c:\dev\openssl

::: noteclassic
Ambas as versões de 32 e 64 bitspodem ser criadas em uma plataforma 64-bit, mas apenas a versão 32-bit pode criar em uma plataforma 32-bit.
Ao trabalhar na plataforma de 32 bits, siga as mesmas etapas para 64 bits pata plataforma versão  64 bits.
:::

[comment]: # ({/f8fc15d0-f7876f1d})
