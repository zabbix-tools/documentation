[comment]: # translation:outdated

[comment]: # ({46351ca1-0535e4cb})
# Criando o agente Zabbix no macOS

[comment]: # ({/46351ca1-0535e4cb})

[comment]: # ({cfe7b220-161fafaa})
#### Visão geral

Esta seção demonstra como construir os binários do agente Zabbix para macOS a partir da fonte com ou sem TLS.

[comment]: # ({/cfe7b220-161fafaa})

[comment]: # ({281d5cf4-7e7224fc})
#### Pré-requisitos

Você precisará de ferramentas de desenvolvedor de linha de comando (Xcode não é necessário), Automake, pkg-config e PCRE (v8.x) ou PCRE2 (v10.x). Se você deseja criar os binários do agente com TLS, você também precisará do OpenSSL ou GnuTLS.

Para instalar o Automake e o pkg-config, você precisará de um gerenciador de pacotes Homebrew de <https://brew.sh/>. Para instalar, abra o terminal  e execute o seguinte comando:

    $ /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

Em seguida, instale o Automake e o pkg-config:

    $ brew install automake
    $ brew install pkg-config

A prepração das bibliotecas do PCRE, OpenSSL e GnuTLS depende da maneira como eles serão vinculados ao agente.

Se você pretende executar os binários do agente em uma máquina macOS que possui estas bibliotecas, você pode utilizar bibliotecas pré-compiladas fornecidas pelo Homebrew. Normalmente, são máquinas macOS que usam o Homebrew para criar binários do agente Zabbix ou para outros fins.

Se os binários do agente forem utilizados ​​em máquinas macOS que não possuem a versão compartilhada das bibliotecas, você deve compilar bibliotecas estáticas a partir da fonte e vincular o agente Zabbix com eles.

[comment]: # ({/281d5cf4-7e7224fc})

[comment]: # ({ebb9518f-5e97a918})
#### Construindo binários do agente com bibliotecas compartilhadas

Instalar o PCRE2 (substitua *pcre2* por *pcre* nos comandos abaixo, caso necessário):

    $ brew install pcre2

Ao construir com TLS, instale OpenSSL e/ou GnuTLS::

    $ brew install openssl
    $ brew install gnutls

Baixar o fonte do Zabbix:

    $ git clone https://git.zabbix.com/scm/zbx/zabbix.git

Construir o agente sem TLS:

    $ cd zabbix
    $ ./bootstrap.sh
    $ ./configure --sysconfdir=/usr/local/etc/zabbix --enable-agent --enable-ipv6
    $ make
    $ make install

Construir o agente com OpenSSL:

    $ cd zabbix
    $ ./bootstrap.sh
    $ ./configure --sysconfdir=/usr/local/etc/zabbix --enable-agent --enable-ipv6 --with-openssl=/usr/local/opt/openssl
    $ make
    $ make install

Construir o agente com GnuTLS:

    $ cd zabbix-source/
    $ ./bootstrap.sh
    $ ./configure --sysconfdir=/usr/local/etc/zabbix --enable-agent --enable-ipv6 --with-gnutls=/usr/local/opt/gnutls
    $ make
    $ make install

[comment]: # ({/ebb9518f-5e97a918})

[comment]: # ({673a9104-90fe7eb9})
#### Construindo binários de agente com bibliotecas estáticas sem TLS

Vamos assumir que as bibliotecas estáticas do PCRE serão instaladas em
`$HOME/static-libs`. Utilizaremos o PCRE2 10.39.

    $ PCRE_PREFIX="$HOME/static-libs/pcre2-10.39"

Faça o download e construa o PCRE com suporte a propriedades Unicode:

    $ mkdir static-libs-source
    $ cd static-libs-source
    $ curl --remote-name https://github.com/PhilipHazel/pcre2/releases/download/pcre2-10.39/pcre2-10.39.tar.gz
    $ tar xf pcre2-10.39.tar.gz
    $ cd pcre2-10.39
    $ ./configure --prefix="$PCRE_PREFIX" --disable-shared --enable-static --enable-unicode-properties
    $ make
    $ make check
    $ make install

Faça o download do fonte do Zabbix e construa o agente:

    $ git clone https://git.zabbix.com/scm/zbx/zabbix.git
    $ cd zabbix
    $ ./bootstrap.sh
    $ ./configure --sysconfdir=/usr/local/etc/zabbix --enable-agent --enable-ipv6 --with-libpcre2="$PCRE_PREFIX"
    $ make
    $ make install

[comment]: # ({/673a9104-90fe7eb9})

[comment]: # ({0d15e354-b985c38b})
#### Construindo binários do agente com bibliotecas estáticas com o OpenSSL

Ao construir o OpenSSL, é recomendado executar `make test` após
construção com sucesso. Mesmo que a construção tenha sido bem-sucedida, os testes às vezes falham. Se este for o caso, os problemas devem ser pesquisados ​​e resolvidos antes de continuar.

Vamos assumir que as bibliotecas estáticas PCRE e OpenSSL serão instaladas em `$HOME/static-libs`. Utilizaremos o PCRE2 10.39 e OpenSSL 1.1.1a.

    $ PCRE_PREFIX="$HOME/static-libs/pcre2-10.39"
    $ OPENSSL_PREFIX="$HOME/static-libs/openssl-1.1.1a"

Vamos construir as bibliotecas estáticas em `static-libs-source`:

    $ mkdir static-libs-source
    $ cd static-libs-source

Faça o download e construa o PCRE com suporte a propriedades Unicode:

    $ curl --remote-name https://github.com/PhilipHazel/pcre2/releases/download/pcre2-10.39/pcre2-10.39.tar.gz
    $ tar xf pcre2-10.39.tar.gz
    $ cd pcre2-10.39
    $ ./configure --prefix="$PCRE_PREFIX" --disable-shared --enable-static --enable-unicode-properties
    $ make
    $ make check
    $ make install
    $ cd ..

Faça o download e construa o OpenSSL:

    $ curl --remote-name https://www.openssl.org/source/openssl-1.1.1a.tar.gz
    $ tar xf openssl-1.1.1a.tar.gz
    $ cd openssl-1.1.1a
    $ ./Configure --prefix="$OPENSSL_PREFIX" --openssldir="$OPENSSL_PREFIX" --api=1.1.0 no-shared no-capieng no-srp no-gost no-dgram no-dtls1-method no-dtls1_2-method darwin64-x86_64-cc
    $ make
    $ make test
    $ make install_sw
    $ cd ..

Faça o download do fonte do Zabbix e construa o agente:

    $ git clone https://git.zabbix.com/scm/zbx/zabbix.git
    $ cd zabbix
    $ ./bootstrap.sh
    $ ./configure --sysconfdir=/usr/local/etc/zabbix --enable-agent --enable-ipv6 --with-libpcre2="$PCRE_PREFIX" --with-openssl="$OPENSSL_PREFIX"
    $ make
    $ make install

[comment]: # ({/0d15e354-b985c38b})

[comment]: # ({8247eff8-8165604b})
####   Construindo binários de agentes com bibliotecas estáticas com GnuTLS

O GnuTLS depende do back-end de criptografia Nettle e da biblioteca aritmética GMP.
Em vez de usar a biblioteca GMP completa, este guia usará o mini-gmp, que é incluído na Nettle.

Ao compilar GnuTLS e Nettle, é recomendado executar `make check` após a compilação bem-sucedida. Mesmo que a construção tenha sido bem-sucedida, os testes às vezes falha. Se este for o caso, os problemas devem ser pesquisados ​​e resolvido antes de continuar.

Vamos assumir que as bibliotecas estáticas do PCRE, Nettle e GnuTLS serão instaladas em `$HOME/static-libs`. Utilizamores o PCRE2 10.39, Nettle 3.4.1 e GnuTLS 3.6.5.

    $ PCRE_PREFIX="$HOME/static-libs/pcre2-10.39"
    $ NETTLE_PREFIX="$HOME/static-libs/nettle-3.4.1"
    $ GNUTLS_PREFIX="$HOME/static-libs/gnutls-3.6.5"

Vamos construir bibliotecas estáticas em `static-libs-source`:

    $ mkdir static-libs-source
    $ cd static-libs-source

Faça o download e construa o Nettle:

    $ curl --remote-name https://ftp.gnu.org/gnu/nettle/nettle-3.4.1.tar.gz
    $ tar xf nettle-3.4.1.tar.gz
    $ cd nettle-3.4.1
    $ ./configure --prefix="$NETTLE_PREFIX" --enable-static --disable-shared --disable-documentation --disable-assembler --enable-x86-aesni --enable-mini-gmp
    $ make
    $ make check
    $ make install
    $ cd ..

Faça o download e construa o GnuTLS:

    $ curl --remote-name https://www.gnupg.org/ftp/gcrypt/gnutls/v3.6/gnutls-3.6.5.tar.xz
    $ tar xf gnutls-3.6.5.tar.xz
    $ cd gnutls-3.6.5
    $ PKG_CONFIG_PATH="$NETTLE_PREFIX/lib/pkgconfig" ./configure --prefix="$GNUTLS_PREFIX" --enable-static --disable-shared --disable-guile --disable-doc --disable-tools --disable-libdane --without-idn --without-p11-kit --without-tpm --with-included-libtasn1 --with-included-unistring --with-nettle-mini
    $ make
    $ make check
    $ make install
    $ cd ..

Faça o download do fonte do Zabbix e construa o agente:

    $ git clone https://git.zabbix.com/scm/zbx/zabbix.git
    $ cd zabbix
    $ ./bootstrap.sh
    $ CFLAGS="-Wno-unused-command-line-argument -framework Foundation -framework Security" \
    > LIBS="-lgnutls -lhogweed -lnettle" \
    > LDFLAGS="-L$GNUTLS_PREFIX/lib -L$NETTLE_PREFIX/lib" \
    > ./configure --sysconfdir=/usr/local/etc/zabbix --enable-agent --enable-ipv6 --with-libpcre2="$PCRE_PREFIX" --with-gnutls="$GNUTLS_PREFIX"
    $ make
    $ make install

[comment]: # ({/8247eff8-8165604b})
