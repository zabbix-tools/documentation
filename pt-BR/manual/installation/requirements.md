[comment]: # translation:outdated

[comment]: # ({1d3b870b-02b10f8d})
# 2 Requisitos

[comment]: # ({/1d3b870b-02b10f8d})

[comment]: # ({80330f65-80330f65})
#### Hardware

[comment]: # ({/80330f65-80330f65})

[comment]: # ({126da1a1-4ae00568})
##### Memória

O Zabbix requer memória física e de disco. 128 MB de memória física e 256 MB de espaço livre em disco pode ser um bom ponto de partida. Entretanto, a quantidade de memória de disco necessária obviamente depende do número de hosts e parâmetros que estão sendo monitorados. Se você planeja manter um longo histórico de parâmetros monitorados, deve pensar em pelo menos alguns gigabytes para ter espaço suficiente para armazenar o histórico no banco de dados. Cada processo do Zabbix daemon requer várias conexões ao servidor de banco de dados. A quantidade de memória alocada para a conexão depende da configuração do mecanismo de banco de dados. 

::: noteclassic
Quanto mais memória física você tiver, mais rápido o banco de dados (e, portanto, o Zabbix) funcionará! 
:::

[comment]: # ({/126da1a1-4ae00568})

[comment]: # ({5acf7909-7967309a})
##### CPU

O Zabbix e, especialmente, o banco de dados do Zabbix pode requerer recursos de CPU significativos dependendo do número de parâmetros monitorados e o mecanismo de banco de dados selecionada.

[comment]: # ({/5acf7909-7967309a})

[comment]: # ({94ce20d4-1cbee7bf})
##### Outro hardware

A porta de comunicação serial e o modem serial GSMA são necessários para utilizar o suporte de notificação por SMS no Zabbix. O conversor USB para serial também funcionará.

[comment]: # ({/94ce20d4-1cbee7bf})

[comment]: # ({new-42d1d93e})
#### Examples of hardware configuration

The table provides examples of hardware configuration, assuming a **Linux/BSD/Unix** platform.

These are size and hardware configuration examples to start with. Each Zabbix installation is unique. 
Make sure to benchmark the performance of your Zabbix system in a staging or development environment, 
so that you can fully understand your requirements before deploying the Zabbix installation to its 
production environment.

|Installation size|Monitored metrics^**1**^|CPU/vCPU cores|Memory<br>(GiB)|Database|Amazon EC2^**2**^|
|-|-|-|-|-|-|
|Small|1 000|2|8|MySQL Server,<br>Percona Server,<br>MariaDB Server,<br>PostgreSQL|m6i.large/m6g.large|
|Medium|10 000|4|16|MySQL Server,<br>Percona Server,<br>MariaDB Server,<br>PostgreSQL|m6i.xlarge/m6g.xlarge|
|Large|100 000|16|64|MySQL Server,<br>Percona Server,<br>MariaDB Server,<br>PostgreSQL,<br>Oracle|m6i.4xlarge/m6g.4xlarge|
|Very large|1 000 000|32|96|MySQL Server,<br>Percona Server,<br>MariaDB Server,<br>Oracle|m6i.8xlarge/m6g.8xlarge|

^**1**^ 1 metric = 1 item + 1 trigger + 1 graph<br>
^**2**^ Example with Amazon general purpose EC2 instances, using ARM64 or x86_64 architecture, a 
proper instance type like Compute/Memory/Storage optimised should be selected during Zabbix 
installation evaluation and testing before installing in its production environment.

::: noteclassic
Actual configuration depends on the number of active items
and refresh rates very much (see [database
size](/manual/installation/requirements#Database_size) section of this
page for details). It is highly recommended to run the database on a
separate box for large installations.
:::

[comment]: # ({/new-42d1d93e})



[comment]: # ({d6a07a9e-ad0c32cf})
#### Plataformas suportadas

Devido a requisitos de segurança e a natureza crítica do servidor de monitoramento, o UNIX é o único sistema operacional que consegue fornecer consistentemente a performance, tolerância a falhas e resiliência necessárias. O Zabbix opera em versões líderes de mercado.

Os componentes do Zabbix estão disponíveis e foram testados para as seguintes plataformas:

|Plataforma|Servidor|Agente|Agente2|
|--------|------|-----|------|
|Linux|x|x|x|
|IBM AIX|x|x|\-|
|FreeBSD|x|x|\-|
|NetBSD|x|x|\-|
|OpenBSD|x|x|\-|
|HP-UX|x|x|\-|
|Mac OS X|x|x|\-|
|Solaris|x|x|\-|
|Windows|\-|x|x|

::: noteclassic
O servidor Zabbix/agente Zabbix pode funcionar em outros sistemas operacionais baseados em Unix. O agente Zabbix é suportado em todos os Windows versão desktop e versões de servidor desde o XP.
:::

::: noteimportant
O Zabbix desabilita os core dumps se compilado com criptografia e não inicia se o sistema não permitir a desativação dos core dumps.
:::

[comment]: # ({/d6a07a9e-ad0c32cf})

[comment]: # ({40be4c8b-323b6241})
#### Software necessário

O Zabbix é construído em torno de servidores web modernos, principais mecanismos de banco de dados e linguagem de programação PHP.

[comment]: # ({/40be4c8b-323b6241})

[comment]: # ({new-fd454df8})
##### Sistema de gerenciamento de banco de dados

|Software|Versões suportadas|Comentários|
|--------|------------------|--------|
|*MySQL/Percona*|8.0.X|Necessário se o MySQL (ou Percona) for utilizado como o banco de dados do backend do Zabbix. O mecanismo do InnoDB é necessário. Recomendamos utilizar a biblioteca do [MariaDB Connector/C](https://downloads.mariadb.org/connector-c/) para construir o servidor/proxy.|
|*MariaDB*|10.5.00-10.6.X|O mecanismo do InnoDB é necessário. Recomendamos utilizar a biblioteca do [MariaDB Connector/C](https://downloads.mariadb.org/connector-c/) para construir o servidor/proxy.|
|*Oracle*|19c - 21c|Necessário se o Oracle for utilizado como o banco de dados do backend do Zabbix.|
|*PostgreSQL*|13.X|Necessário se o PostgreSQL for utilizado como o banco de dados do backend do Zabbix.|
|*TimescaleDB* para PostgreSQL|2.0.1-2.3|Necessário se o TimescaleDB for utilizado como o banco de dados do backend do Zabbix. Certifique-se de instalar a distribuição do TimescaleDB com a compactação suportada.|
|*SQLite*|3.3.5-3.34.X|O SQLite é suportado apenas com proxies Zabbix. Necessário se SQLite for usado como banco de dados proxy Zabbix.|

::: noteclassic
 Embora o Zabbix possa trabalhar com bancos de dados disponíveis nos sistemas operacionais, para melhor experiência, recomendamos o uso de bancos de dados instalado a partir dos repositórios oficiais do desenvolvedor de banco de dados.
:::

[comment]: # ({/new-fd454df8})

[comment]: # ({717b381e-75f0586e})
##### Frontend

A largura mínima de tela suportada para o frontend Zabbix é 1200px.

|Software|Versão|Comentários|
|--------|-------|--------|
|*Apache*|1.3.12 ou posterior|<|
|*PHP*|7.2.5 ou posterior|O PHP 8.0 não é suportado.|
|Extensões do PHP|<|<|
|*gd*|2.0.28 ou posterior|A extensão PHP GD deve suportar imagens PNG (*--with-png-dir*), JPEG (*--with-jpeg-dir*) e FreeType 2 (*--with-freetype-dir*).|
|*bcmath*|<|php-bcmath (*--enable-bcmath*)|
|*ctype*|<|php-ctype (*--enable-ctype*)|
|*libXML*|2.6.15 ou posterior|php-xml, se fornecido como um pacote separado pelo distribuidor.|
|*xmlreader*|<|php-xmlreader, se fornecido como um pacote separado pelo distribuidor.|
|*xmlwriter*|<|php-xmlwriter, se fornecido como um pacote separado pelo distribuidor.|
|*session*|<|php-session, se fornecido como um pacote separado pelo distribuidor.|
|*sockets*|<|php-net-socket (*--enable-sockets*). Necessário para suporte ao script de usuário.|
|*mbstring*|<|php-mbstring (*--enable-mbstring*)|
|*gettext*|<|php-gettext (*--with-gettext*). Necessário para que as traduções funcionem.|
|*ldap*|<|php-ldap. Necessário somente se a autenticação por LDAP for utilizada no frontend.|
|*openssl*|<|php-openssl. Necessário somente se a autenticação por SAML for utilizada no frontend.|
|*mysqli*|<|Necessário se o MySQL for utilizado como banco de dados no backend do Zabbix.|
|*oci8*|<|Necessário se o Oracle for utilizado como banco de dados no backend do Zabbix.|
|*pgsql*|<|Necessário se o PostgreSQL for utilizado como banco de dados no backend do Zabbix.|

::: noteclassic
O Zabbix também pode funcionar em versões anteriores do Apache, MySQL, Oracle e PostgreSQL.
:::

::: noteimportant
Para outras fontes além do padrão DejaVu, a função do PHP
[imagerotate](http://php.net/manual/en/function.imagerotate.php) pode ser necessária. Se estiver ausente, essas fontes podem ser renderizadas incorretamente quando um gráfico é exibido. Esta função só está disponível se o PHP for compilado com GD empacotado, o que não é o caso no Debian e outros distribuições.
:::

[comment]: # ({/717b381e-75f0586e})

[comment]: # ({6dc5a548-dda86afe})
##### Navegador da web no lado do cliente

Os Cookies e o JavaScript devem estar habilitados.

As últimas versões estáveis ​​do Google Chrome, Mozilla Firefox, Microsoft Edge, Apple Safari e Opera são suportadas.

::: notewarning
A política de mesma origem para os IFrames é implementada, o que significa que o Zabbix não pode ser inserido em frames em domínio diferente.\
\
Ainda assim, as páginas colocadas em um frame do Zabbix terão acesso ao Zabbix frontend (através de JavaScript) se a página que é inserida no frame e o frontend do Zabbix estiverem no mesmo domínio. Uma página como `http://secure-zabbix.com/cms/page.html`, se inserida nos dashboards em `http://secure-zabbix.com/zabbix/`, terá o total acesso JS ao Zabbix.
:::

[comment]: # ({/6dc5a548-dda86afe})

[comment]: # ({8b7d1f43-096d332c})
##### Servidor

Requisitos obrigatórios são necessários sempre. Os requisitos opcionais são necessários para o suporte da função específica.

|Requisito|Status|Descrição|
|-----------|------|-----------|
|*libpcre*|Obrigatório|A biblioteca PCRE é necessária para suporte ao [Perl Compatible Regular Expression](https://en.wikipedia.org/wiki/Perl_Compatible_Regular_Expressions) (PCRE).<br>A nomenclatura pode diferir dependendo da distribuição GNU/Linux, por exemplo 'libpcre3' ou 'libpcre1'. PCRE v8.x e PCRE2 v10.x (do Zabbix 6.0.0) são suportados.|
|*libevent*|^|Necessário para suporte de métrica em massa e monitoramento de IPMI. Versão 1.4 ou superior.<br>Observe que para o proxy Zabbix este requisito é opcional; é necessário para suporte de monitoramento IPMI.|
|*libpthread*|^|Required for mutex and read-write lock support.|
|*zlib*|^|Necessário para o suporte a compressão.|
|*OpenIPMI*|Optional|Necessário para suporte IPMI.|
|*libssh2* or *libssh*|^|Necessário para [SSH checks](/manual/config/items/itemtypes/ssh_checks#overview). Versão 1.0 ou superior (libssh2); 0.6.0 ou superior (libssh).<br>libssh é suportado desde o Zabbix 4.4.6.|
|*fping*|^|Necessário para [ICMP ping items](/manual/config/items/itemtypes/simple_checks#icmp_pings).|
|*libcurl*|^|Necessário para monitoramento web, monitoramento VMware, autenticação SMTP, `web.page.*`  [items](/manual/config/items/itemtypes/zabbix_agent) do agente Zabbix, itens do agente HTTP e Elasticsearch (se utilizado). Recomendado versão 7.28.0 ou superior.<br>Requisitos da versão do Libcurl:<br>- Autenticação SMTP: versão 7.20.0 ou superior<br>- Elasticsearch: versão 7.28.0 ou superior|
|*libxml2*|^|Necessário para monitoramento VMware e pré-processamento XML XPath.|
|*net-snmp*|^|Necessário para suporte SNMP. Versão 5.3.0 ou superior.|
|*GnuTLS*, *OpenSSL* ou *LibreSSL*|^|Necessário ao utilizar [encryption](/manual/encryption#compiling_zabbix_with_encryption_support).|

[comment]: # ({/8b7d1f43-096d332c})

[comment]: # ({c146db75-800c8308})
##### Agente

|Requisito|Status|Descrição|
|-----------|------|-----------|
|*libpcre*|Obrigatório|A biblioteca PCRE é obrigatória para suporte ao [Perl Compatible Regular Expression](https://en.wikipedia.org/wiki/Perl_Compatible_Regular_Expressions) (PCRE).<br>A nomenclatura pode diferir dependendo da distribuição GNU/Linux, por exemplo 'libpcre3' ou 'libpcre1'. PCRE v8.xe PCRE2 v10.x (do Zabbix 6.0.0) são suportados.|
|*GnuTLS*, *OpenSSL* ou *LibreSSL*|Opcional|Necessário ao utilizar [encryption](/manual/encryption#compiling_zabbix_with_encryption_support).<br> Em sistemas Microsoft Windows é necessário OpenSSL 1.1.1 ou posterior.|

::: noteclassic
 A partir da versão 5.0.3, o agente Zabbix não funcionará em plataformas AIX inferior as versões 6.1 TL07 / AIX 7.1 TL01. 
:::

[comment]: # ({/c146db75-800c8308})

[comment]: # ({2db3b343-4aa86212})
##### Agent 2

|Requisito|Status|Descrição|
|-----------|------|-----------|
|*libpcre*|Obrigatório|A biblioteca PCRE é obrigatória para suporte ao [Perl Compatible Regular Expression](https://en.wikipedia.org/wiki/Perl_Compatible_Regular_Expressions) (PCRE).<br>A nomenclatura pode diferir dependendo da distribuição GNU/Linux, por exemplo 'libpcre3' ou 'libpcre1'. PCRE v8.xe PCRE2 v10.x (do Zabbix 6.0.0) são suportados.|
|*OpenSSL*|Opcional|Necessário ao usar criptografia.<br>OpenSSL 1.0.1 ou posterior é necessário em plataformas UNIX.<br>A biblioteca OpenSSL deve ter o suporte PSK ativado. O LibreSSL não é suportado.<br>Nos sistemas Microsoft Windows, o OpenSSL 1.1.1 ou posterior é necessário.

[comment]: # ({/2db3b343-4aa86212})

[comment]: # ({fc8cda14-149075a3})

##### Java gateway


Se você obteve o Zabbix do repositório de origem ou de um arquivo, então as dependências necessárias já estão incluídas na árvore de origem.

Se você obteve o Zabbix do pacote de sua distribuição, então as dependências necessárias já são fornecidas pelo sistema de empacotamento.

Em ambos os casos acima, o software está pronto para ser usado e não há downloads são necessários.

Se, no entanto, você deseja fornecer suas versões dessas dependências
(por exemplo, se você estiver preparando um pacote para alguns
distribuição), abaixo está a lista de versões de biblioteca que o gateway Java é conhecido por trabalhar. O Zabbix pode funcionar com outras versões destes bibliotecas também.

A tabela a seguir lista os arquivos JAR que estão atualmente empacotados com Java gateway no código original:

|Biblioteca|Licença|Site|Comentários|
|-------|-------|-------|--------|
|*logback-core-1.2.3.jar*|EPL 1.0, LGPL 2.1|<http://logback.qos.ch/>|Testado com 0.9.27, 1.0.13, 1.1.1 e 1.2.3.|
|*logback-classic-1.2.3.jar*|EPL 1.0, LGPL 2.1|<http://logback.qos.ch/>|Testado com 0.9.27, 1.0.13, 1.1.1 e 1.2.3.|
|*slf4j-api-1.7.30.jar*|Licença MIT|<http://www.slf4j.org/>|Testado com 1.6.1, 1.6.6, 1.7.6 e 1.7.30.|
|*android-json-4.3\_r3.1.jar*|Licença Apache 2.0|<https://android.googlesource.com/platform/libcore/+/master/json>|Testado com 2.3.3\_r1.1 e 4.3\_r3.1. Verifique src/zabbix\_java/lib/README para instruções de criação de arquivo JAR.|

O Java gateway pode ser construído pode ser construído usando Oracle Java ou código aberto OpenJDK (versão 1.6 ou mais recente). Os pacotes fornecidos pelo Zabbix são compilados usando o OpenJDK. A tabela abaixo fornece informações sobre as versões do OpenJDK usadas para construir pacotes Zabbix por distribuição:

|Distribuição|Versão do OpenJDK|
|------------|---------------|
|RHEL/CentOS 8|1.8.0|
|RHEL/CentOS 7|1.8.0|
|SLES 15|11.0.4|
|SLES 12|1.8.0|
|Debian 10|11.0.8|
|Ubuntu 20.04|11.0.8|
|Ubuntu 18.04|11.0.8|

[comment]: # ({/fc8cda14-149075a3})

[comment]: # ({new-c8024c35})

#### Default port numbers

The following list of open ports per component is applicable for default configuration:

|Zabbix component|Port number|Protocol|Type of connection|
|-------|-------|-------|-------|
|Zabbix agent|10050|TCP|on demand|
|Zabbix agent 2|10050|TCP|on demand|
|Zabbix server|10051|TCP|on demand|
|Zabbix proxy|10051|TCP|on demand|
|Zabbix Java gateway|10052|TCP|on demand|
|Zabbix web service|10053|TCP|on demand|
|Zabbix frontend|80|HTTP|on demand|
|^|443|HTTPS|on demand|
|Zabbix trapper|10051 |TCP| on demand|

::: noteclassic
The port numbers should be open in firewall to enable Zabbix communications. Outgoing TCP connections usually do not require explicit firewall settings.
::: 

[comment]: # ({/new-c8024c35})




[comment]: # ({72665f11-1d73b238})

#### Tamanho do banco de dados

Os dados de configuração do Zabbix requerem uma quantidade fixa de espaço em disco e não crescem muito.

O tamanho do banco de dados Zabbix depende principalmente dessas variáveis, que definem a quantidade de dados históricos armazenados:

-   Números de valores processados por segundo

Este é o número médio de novos valores que o servidor Zabbix recebe a cada segundo. Por exemplo, se tivermos 3.000 itens para monitoramento com uma taxa de atualização de 60 segundos, o número de valores por segundo será calculado como 3.000/60 = **50**.

Isso significa que 50 novos valores são adicionados ao banco de dados Zabbix a cada segundo.

-   Configuração do Housekeeper para o histórico

Zabbix mantém valores por um período fixo de tempo, normalmente várias semanas ou meses. Cada novo valor requer uma certa quantidade de espaço em disco para dados e índice.

Então, se quisermos manter 30 dias de histórico e recebermos 50 valores por segundo, o número total de valores será em torno de (**30**\*24\*3600)\* **50** = 129.600.000, ou cerca de 130M de valores.

Dependendo do mecanismo de banco de dados usado, tipo de valores recebidos (reais, inteiros, strings, arquivos de log, etc), o espaço em disco para manter um único valor pode variar de 40 bytes a centenas de bytes. Normalmente é cerca de 90 bytes por valor para itens numéricos^**2**^. No nosso caso, significa que 130M de valores exigirão 130M \* 90 bytes = **10.9GB** de espaço em disco.

::: noteclassic
O tamanho dos items do tipo texto/log é impossível de ser previsto com exatidão, mas você esperar em torno de 500 bytes por valor.
:::

-   Configuração do Housekeeper para a estatística

O Zabbix mantém um conjunto de valores máximo/min/médio/contagem de 1 hora para cada item na tabela **trends**. Os dados são usados ​​para tendências e gráficos de longos períodos. O período de uma hora não pode ser personalizado.

O banco de dados do Zabbix, dependendo do tipo do banco de dados, requer cerca de 90 bytes para cada total. Suponha que gostaríamos de manter os dados de estatística por 5 anos. Os valores para 3.000 itens exigirão 3.000\*24\*365\* **90** = **2,2 GB** por ano ou **11 GB** por 5 anos.

-   Configuração do Housekeeper para os eventos

Cada evento Zabbix requer aproximadamente 250 bytes de espaço em disco^**1**^.
É difícil estimar o número de eventos gerados diariamente pelo Zabbix.
Na pior das hipóteses, podemos assumir que o Zabbix gera um
evento por segundo.

Para cada evento de recuperação, é criado um registro event\_recovery. Normalmente a maioria dos eventos serão recuperados para que possamos assumir um registro de event\_recovery por evento. Isso significa 80 bytes adicionais por evento.

Opcionalmente, os eventos podem ter tags, cada registro de tag requer aproximadamente 100 bytes de espaço em disco^**1**^. O número de tags por evento (\#tags) depende da configuração. Portanto, cada um precisará de mais \#tags \* 100 bytes de espaço em disco.

Isso significa que se quisermos manter 3 anos de eventos, isso exigiria
3\*365\*24\*3600\* (250+80+\#tags\*100) = **\~30GB**+\#tags\*100B disco
espaço^**2**^.

::: noteclassic
 ^**1**^ Mais quando tiver nomes de eventos não ASCII, tags e valores.

^**2**^ As aproximações de tamanho são baseadas no MySQL e podem ser diferentes para outros bancos de dados.
:::

A tabela contém fórmulas que podem ser usadas para calcular o espaço em disco necessário para o sistema Zabbix:

|Parâmetro|Fórmula para o espaço em disco necessário (em bytes)|
|---------|------------------------------------------|
|*Configuração do Zabbix*|Tamanho fixo. Normalmente 10MB ou menos.|
|*Histórico*|dias\*(itens/intervalo de atualização)\*24\*3600\*bytes<br>itens : número de itens<br>dias : número de dias para manter o histórico<br>intervalo de atualização : intervalo de atualização médio para os itens<br>bytes : número de bytes necessário para manter um valor, depende do mecanismo de banco de dados, normalmente \~90 bytes.|
|*Estatísticas*|dias\*(itens/3600)\*24\*3600\*bytes<br>itens : número de itens<br>dias : número de dias para manter o histórico<br>bytes : número de bytes necessário para manter um valor estatístico, depende do mecanismo de banco de dados, normalmente \~90 bytes.|
|*Eventos*|dias\*eventos\*24\*3600\*bytes<br>eventos : número de eventos por segundo. Um (1) evento por segundo no pior cenário.<br>dias : número de dias para manter o histórico<br>bytes : número de bytes necessários para manter um valor estatístico, depende do mecanismo de banco de dados, normalmente \~330 + número médio de tags por evento \* 100 bytes.|

Assim, o espaço total em disco necessário pode ser calculado como:\
**Configuração + Histórico + Estatísticas + Eventos**\
O espaço em disco NÃO será usado imediatamente após a instalação do Zabbix.
O tamanho do banco de dados aumentará e depois parará de crescer em algum ponto, o que depende das configurações do Housekeeper.

[comment]: # ({/72665f11-1d73b238})

[comment]: # ({1933640b-520ea0fa})
#### Sincronização de tempo

É muito importante ter a hora exata do sistema no servidor no qual o Zabbix está em execução. O [ntpd](http://www.ntp.org/) é o daemon mais popular que sincroniza a hora do host com a hora de outras máquinas. Isso é fortemente recomendado para manter a hora do sistema sincronizada em todos os sistemas nos quais os componentes do Zabbix estão sendo executados.

[comment]: # ({/1933640b-520ea0fa})

[comment]: # ({new-9e23ba76})

#### Network requirements

A following list of open ports per component is applicable for default configuration.


|Port|Components |
|------------|---------------|
|Frontend|http on 80, https on 443|
|Server|10051 (for use with active proxy/agents)|
|Active Proxy |10051 ( not sure about fetching config)|
|Passive Proxy|10051|
|Agent2|10050|
|Trapper| |
|JavaGateway|10053|
|WebService|10053|

::: noteclassic
In case the configuration is completed, the port numbers should be edited accordingly to prevent firewall from blocking communications with these ports.Outgoing TCP connections usually do not require explicit firewall settings.
::: 

[comment]: # ({/new-9e23ba76})
