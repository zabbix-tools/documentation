[comment]: # translation:outdated

[comment]: # ({03dcc6a2-7050624a})
# 4 Instalação agente Windows por MSI

[comment]: # ({/03dcc6a2-7050624a})

[comment]: # ({28bebfe9-04777b60})

#### Visão Geral
O agente Zabbix Windows pode ser instalado a partir do instalador de pacote  MSI do Windows (32 bits ou 64 bits) disponíveis em 
[download](https://www.zabbix.com/download_agents#tab:44).

O pacote 32-bit não pode ser instalado em Windows 64-bits

Todos os pacotes vêm com suporte TLS, no entanto, configurar TLS é
opcional.

Tanto a IU quanto a instalação baseada na linha de comando são suportadas.

[comment]: # ({/28bebfe9-04777b60})

[comment]: # ({f169163e-0fc3b21c})

  
#### Etapas de instalação 
Para instalar, clique duas vezes no arquivo MSI baixado.

![](../../../../assets/en/manual/installation/install_from_packages/msi0_b.png)

![](../../../../assets/en/manual/installation/install_from_packages/msi0_c.png)

Aceite a licença para prosseguir para a próxima etapa.

![](../../../../assets/en/manual/installation/install_from_packages/msi0_d.png)

Especifique os seguintes parâmetros.

|Parâmetros|Descrição|
|---------|-----------|
|*Nome do Host*|Especifique o host name.|
|*IP/DNS do servidor Zabbix *|Especifique o IP/DNS do servidor Zabbix.|
|*Porta de comunicação do agente*|Especifique a porta de comunicação (10050 por padrão).|
|*Servidor ou Proxy para ativar verificação*|Especifique o IP/DNS do servidor/proxy Zabbix para verificação ativa do agente.|
|*Habilitar PSK*|Marque como selecionado para habilitar suporte TLS via chaves pré-compartilhadas.|
|*Adicione a localização do agente PATH*|Adicione a localização do agente à variável PATH.|

![](../../../../assets/en/manual/installation/install_from_packages/msi0_e.png)

Insira a identidade e o valor da chave pré-compartilhada. Esta etapa só está disponível se você marcou Habilitar PSK na etapa anterior..

![](../../../../assets/en/manual/installation/install_from_packages/msi0_f.png)

Selecione os componentes  Zabbix para instalação - [Zabbix agent
daemon](/manual/concepts/agent), [Zabbix
sender](/manual/concepts/sender), [Zabbix get](/manual/concepts/get).

![](../../../../assets/en/manual/installation/install_from_packages/msi0_g.png)

Os componentes do Zabbix junto com o arquivo de configuração serão instalados em uma pasta * Zabbix Agent * em Arquivos de Programas. zabbix \ _agentd.exe será configurado como serviço do Windows com inicialização automática.

![](../../../../assets/en/manual/installation/install_from_packages/msi0_h.png)

[comment]: # ({/f169163e-0fc3b21c})

[comment]: # ({bf3ef5c3-1ca3dd90})
#### Instalação baseada em linha de comandos

[comment]: # ({/bf3ef5c3-1ca3dd90})

[comment]: # ({new-89028c66})

##### Parâmetros suportados

O seguinte conjunto de parâmetros é suportado pelos MSIs criados:
|Número|Parâmetros |Descrição|
|------|---------|-----------|
|1|LOGTYPE|<|
|2|LOGFILE|<|
|3|SERVER|<|
|4|LISTENPORT|<|
|5|SERVERACTIVE|<|
|6|HOSTNAME|<|
|7|TIMEOUT|<|
|8|TLSCONNECT|<|
|9|TLSACCEPT|<|
|10|TLSPSKIDENTITY|<|
|11|TLSPSKFILE|<|
|12|TLSPSKVALUE|<|
|13|TLSCAFILE|<|
|14|TLSCRLFILE|<|
|15|TLSSERVERCERTISSUER|<|
|16|TLSSERVERCERTSUBJECT|<|
|17|TLSCERTFILE|<|
|18|TLSKEYFILE|<|
|19|LISTENIP|<|
|20|HOSTINTERFACE|<|
|21|HOSTMETADATA|<|
|22|HOSTMETADATAITEM|<|
|23|STATUSPORT|apenas Agente Zabbix 2.|
|24|ENABLEPERSISTENTBUFFER| apenas Agente Zabbix 2 .|
|25|PERSISTENTBUFFERPERIOD|apenas Agente Zabbix 2.|
|26|PERSISTENTBUFFERFILE|apenas Agente Zabbix 2.|
|27|INSTALLFOLDER|<|
|28|ENABLEPATH|<|
|29|SKIP|`SKIP=fw` - não instalar regra de exceção de firewall|
|30|INCLUDE|Sequência de inclusões separadas por `;`|
|31|ALLOWDENYKEY|Sequência de "AllowKey" e "DenyKey" [parâmetros](/manual/config/items/restrict_checks) separados por `;`. Use `\\;` para escapar do delimitador.|

Para instalar você pode executar, por exemplo:

    SET INSTALLFOLDER=C:\Program Files\za

    msiexec /l*v log.txt /i zabbix_agent-6.0.0-x86.msi /qn^
     LOGTYPE=file^
     LOGFILE="%INSTALLFOLDER%\za.log"^
     SERVER=192.168.6.76^
     LISTENPORT=12345^
     SERVERACTIVE=::1^
     HOSTNAME=myHost^
     TLSCONNECT=psk^
     TLSACCEPT=psk^
     TLSPSKIDENTITY=MyPSKID^
     TLSPSKFILE="%INSTALLFOLDER%\mykey.psk"^
     TLSCAFILE="c:\temp\f.txt1"^
     TLSCRLFILE="c:\temp\f.txt2"^
     TLSSERVERCERTISSUER="My CA"^
     TLSSERVERCERTSUBJECT="My Cert"^
     TLSCERTFILE="c:\temp\f.txt5"^
     TLSKEYFILE="c:\temp\f.txt6"^
     ENABLEPATH=1^
     INSTALLFOLDER="%INSTALLFOLDER%"^
     SKIP=fw^
     ALLOWDENYKEY="DenyKey=vfs.file.contents[/etc/passwd]"

ou

    msiexec /l*v log.txt /i zabbix_agent-6.0.0-x86.msi /qn^
     SERVER=192.168.6.76^
     TLSCONNECT=psk^
     TLSACCEPT=psk^
     TLSPSKIDENTITY=MyPSKID^
     TLSPSKVALUE=1f87b595725ac58dd977beef14b97461a7c1045b9a1c963065002c5473194952

[comment]: # ({/new-89028c66})

[comment]: # ({new-bfabef9c})
##### Examples

To install Zabbix Windows agent from the command-line, you may run, for example:

    SET INSTALLFOLDER=C:\Program Files\za

    msiexec /l*v log.txt /i zabbix_agent-7.0.0-x86.msi /qn^
     LOGTYPE=file^
     LOGFILE="%INSTALLFOLDER%\za.log"^
     SERVER=192.168.6.76^
     LISTENPORT=12345^
     SERVERACTIVE=::1^
     HOSTNAME=myHost^
     TLSCONNECT=psk^
     TLSACCEPT=psk^
     TLSPSKIDENTITY=MyPSKID^
     TLSPSKFILE="%INSTALLFOLDER%\mykey.psk"^
     TLSCAFILE="c:\temp\f.txt1"^
     TLSCRLFILE="c:\temp\f.txt2"^
     TLSSERVERCERTISSUER="My CA"^
     TLSSERVERCERTSUBJECT="My Cert"^
     TLSCERTFILE="c:\temp\f.txt5"^
     TLSKEYFILE="c:\temp\f.txt6"^
     ENABLEPATH=1^
     INSTALLFOLDER="%INSTALLFOLDER%"^
     SKIP=fw^
     ALLOWDENYKEY="DenyKey=vfs.file.contents[/etc/passwd]"

You may also run, for example:

    msiexec /l*v log.txt /i zabbix_agent-7.0.0-x86.msi /qn^
     SERVER=192.168.6.76^
     TLSCONNECT=psk^
     TLSACCEPT=psk^
     TLSPSKIDENTITY=MyPSKID^
     TLSPSKVALUE=1f87b595725ac58dd977beef14b97461a7c1045b9a1c963065002c5473194952

::: noteclassic
If both TLSPSKFILE and TLSPSKVALUE are passed, then TLSPSKVALUE will be written to TLSPSKFILE.
:::

[comment]: # ({/new-bfabef9c})
