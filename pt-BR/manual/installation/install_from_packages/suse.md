[comment]: # translation:outdated

[comment]: # ({51abb0f6-51abb0f6})
# 3 SUSE Linux Enterprise Server

[comment]: # ({/51abb0f6-51abb0f6})

[comment]: # ({new-ee73b6da})
### Visão Geral

Pacotes oficiais Zabbix estão disponíveis para:

|   |   |
|---|---|
|SUSE Linux Enterprise Server 15|[Download](https://www.zabbix.com/download?zabbix=6.0&os_distribution=suse_linux_enterprise_server&os_version=15&db=mysql)|
|SUSE Linux Enterprise Server 12|[Download](https://www.zabbix.com/download?zabbix=6.0&os_distribution=suse_linux_enterprise_server&os_version=12&db=mysql)|

::: noteimportant
O Zabbix 6.0 ainda não foi lançado. Os links de 
download são para pacotes anteriores à versão 6.0.
:::

::: noteclassic
[O modo de criptografia](/manual/appendix/install/db_encrypt/mysql) *Verify CA*
não funciona em SLES 12 (todos os minor versions) 
com MySQL devido bibliotecas mais antigas de MySQL.
:::

[comment]: # ({/new-ee73b6da})

[comment]: # ({new-37fbf2db})
###  Adicionando repositório Zabbix

Instale o pacote de configuração do repositório. Este pacote contém 
arquivos de configuração do YUM *(gerenciador de pacotes).*

SLES 15:

    # rpm -Uvh --nosignature https://repo.zabbix.com/zabbix/6.0/sles/15/x86_64/zabbix-release-6.0-1.sles15.noarch.rpm
    # zypper --gpg-auto-import-keys refresh 'Zabbix Official Repository' 

SLES 12:

    # rpm -Uvh --nosignature https://repo.zabbix.com/zabbix/6.0/sles/12/x86_64/zabbix-release-6.0-1.sles12.noarch.rpm
    # zypper --gpg-auto-import-keys refresh 'Zabbix Official Repository' 

Por favor, note que o processo de web service do Zabbix
usado para a [geração de relatório agendado](/manual/web_interface/frontend_sections/reports/scheduled) requer o 
navegador Google Chrome. O navegador não está incluso
nos pacotes e deve ser instalado manualmente.

[comment]: # ({/new-37fbf2db})

[comment]: # ({e89eac32-ef4f3e50})
### Instalação Server/Frontend/Agent

Para instalar o Zabbix Server/Frontend/Agent com suporte ao MySQL:

    # zypper install zabbix-server-mysql zabbix-web-mysql zabbix-apache-conf zabbix-agent

Substitua 'apache' no comando por 'nginx' se estiver usando pacote
para Nginx Web Server. Veja também: [Configuração do Nginx
para Zabbix no SLES 12/15](/manual/appendix/install/nginx).

Substitua 'zabbix-agent' por 'zabbix-agent2' nestes comandos se
estiver usando  Zabbix Agent 2 (apenas SLES 15 SP1+).

Para instalar o Zabbix Proxy com suporte ao MySQL:

    # zypper install zabbix-proxy-mysql

Substitua 'mysql' nos comandos por 'pgsql' para usar PostgreSQL.

[comment]: # ({/e89eac32-ef4f3e50})

[comment]: # ({a76de4af-c573e862})
#### Criando Banco de Dados

Para os processos do Zabbix [Server](/manual/concepts/server) e
[Proxy](/manual/concepts/proxy) um banco de dados é exigido. 
Ele não é necessário para executar o Zabbix [Agent](/manual/concepts/agent).

::: notewarning
Bancos de dados isolados são necessários para o Zabbix Server
e Zabbix Proxy; eles não podem utilizar o mesmo banco de dados. 
Portanto, se eles estiverem instalados na mesma máquina, seus
bancos de dados devem ser criados com nomes diferentes!
:::

Crie os bancos de dados usando as instruções disponíveis para 
[MySQL](/manual/appendix/install/db_scripts#mysql) ou
[PostgreSQL](/manual/appendix/install/db_scripts#postgresql).

[comment]: # ({/a76de4af-c573e862})

[comment]: # ({55c163b3-35bc057d})

#### Importando dados

Agora importe o schema inicial e os dados para o **server** com MySQL:

    # zcat /usr/share/doc/packages/zabbix-sql-scripts/mysql/create.sql.gz | mysql -uzabbix -p zabbix

Será solicitado que você informe a senha para o banco de dados recém-criado.

Com PostgreSQL:

    # zcat /usr/share/doc/packages/zabbix-sql-scripts/postgresql/create.sql.gz | sudo -u zabbix psql zabbix

Com TimescaleDB, em adição ao comando anterior, também execute:

    # zcat /usr/share/doc/packages/zabbix-sql-scripts/postgresql/timescaledb.sql.gz | sudo -u <username> psql zabbix

::: notewarning
TimescaleDB é suportado apenas com Zabbix server.
:::

Para o proxy, importe o schema inicial:

    # zcat /usr/share/doc/packages/zabbix-sql-scripts/mysql/schema.sql.gz | mysql -uzabbix -p zabbix

Para proxy com PostgreSQL:

    # zcat /usr/share/doc/packages/zabbix-sql-scripts/postgresql/schema.sql.gz | sudo -u zabbix psql zabbix

[comment]: # ({/55c163b3-35bc057d})

[comment]: # ({87dc32b7-0ea127cd})
#### Configure o banco de dados para Zabbix Server/Proxy

Edite /etc/zabbix/zabbix\_server.conf (e zabbix\_proxy.conf) para usar
seu respectivo banco de dados. Por exemplo:

    # vi /etc/zabbix/zabbix_server.conf
    DBHost=localhost
    DBName=zabbix
    DBUser=zabbix
    DBPassword=<password>

Em DBPassword use a senha do banco de dados do Zabbix no MySQL; 
senha do usuário PostgreSQL se estiver utilizando banco de dados PostgreSQL.

Use `DBHost=` com PostgreSQL. Você poderia querer manter a configuração
padrão `DBHost=localhost` (ou um endereço IP), mas isto faria o
PostgreSQL usar um socket de rede para se conectar ao Zabbix.

[comment]: # ({/87dc32b7-0ea127cd})

[comment]: # ({5a3a79a6-d162545b})
#### Configuração do Zabbix Frontend

Dependendo do Web Server utilizado (Apache/Nginx) edite o
arquivo de configuração correspondente para o Zabbix Frontend:

-   Para o Apache encontre o arquivo de configuração em
    `/etc/apache2/conf.d/zabbix.conf`. Algumas definições de PHP já
    estão configuradas. Mas é necessário descomentar a definição "date.timezone"
    e [informar o timezone adequado](http://php.net/manual/en/timezones.php) para você.

```{=html}
<!-- -->
```
    php_value max_execution_time 300
    php_value memory_limit 128M
    php_value post_max_size 16M
    php_value upload_max_filesize 2M
    php_value max_input_time 300
    php_value max_input_vars 10000
    php_value always_populate_raw_post_data -1
    # php_value date.timezone Europe/Riga

-   O pacote zabbix-nginx-conf instala um servidor Nginx separado para o
    Zabbix Frontend. Seu arquivo de configuração está localizado em
    `/etc/nginx/conf.d/zabbix.conf`. Para o Zabbix Frontend funcionar, é
    necessário descomentar e configurar as diretivas `listen` e/ou `server_name`.

```{=html}
<!-- -->
```
    # listen 80;
    # server_name example.com;

-   O Zabbix usa seu próprio pool de conexão php-fpm dedicado com Nginx:

Ser aquivo de configuração está localizado em
`/etc/php7/fpm/php-fpm.d/zabbix.conf`. Algumas definições de PHP já
    estão configuradas. Mas é necessário descomentar a definição "date.timezone"
    e [informar o timezone adequado](http://php.net/manual/en/timezones.php) para você.

    php_value[max_execution_time] = 300
    php_value[memory_limit] = 128M
    php_value[post_max_size] = 16M
    php_value[upload_max_filesize] = 2M
    php_value[max_input_time] = 300
    php_value[max_input_vars] = 10000
    ; php_value[date.timezone] = Europe/Riga

Agora você está pronto para prosseguir com os [passos de instalação
do Frontend](/manual/installation/install#installing_frontend) que lhe permitirão o acesso ao seu Zabbix recém-instalado.

Note que um Zabbix Proxy não tem um frontend; ele se comunica apenas com o Zabbix Server.

[comment]: # ({/5a3a79a6-d162545b})

[comment]: # ({83e7ca0e-71fda19d})
#### Iniciando o processo Zabbix Server/Agent

Inicie os processos do Zabbix Server e Agent e certifique-se de que iniciem com o boot do sistema.

Com Apache Web Server:

    # systemctl restart zabbix-server zabbix-agent apache2 php-fpm
    # systemctl enable zabbix-server zabbix-agent apache2 php-fpm

Substitua 'apache2' por 'nginx' para Nginx Web Server.

[comment]: # ({/83e7ca0e-71fda19d})

[comment]: # ({ee7ad546-8e553867})
### Instalando pacotes de debuginfo

Para habilitar o repositório de debuginfo edite o arquivo */etc/zypp/repos.d/zabbix.repo*.
Altere `enabled=0` para `enabled=1` no repositório zabbix-debuginfo.

    [zabbix-debuginfo]
    name=Zabbix Official Repository debuginfo
    type=rpm-md
    baseurl=http://repo.zabbix.com/zabbix/4.5/sles/15/x86_64/debuginfo/
    gpgcheck=1
    gpgkey=http://repo.zabbix.com/zabbix/4.5/sles/15/x86_64/debuginfo/repodata/repomd.xml.key
    enabled=0
    update=1

Isto permitirá que você instale pacotes zabbix-***<component>***-debuginfo.

[comment]: # ({/ee7ad546-8e553867})
