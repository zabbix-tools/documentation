[comment]: # translation:outdated

[comment]: # ({1d1caea2-bfa3f768})
# 5 Instalação de agente Mac OS pelo PKG

[comment]: # ({/1d1caea2-bfa3f768})

[comment]: # ({c7902a54-5eec6c30})
#### Visão geral

O agente Zabbix para Mac OS pode ser instalado pelos
pacotes de instalação PKG disponíveis para [download](https://www.zabbix.com/download_agents#tab:44).
Versões com ou sem criptografia estão disponíveis.

[comment]: # ({/c7902a54-5eec6c30})

[comment]: # ({cf25cbbf-3688af62})
#### Instalando agente

O agente pode ser instalado usando a interface gráfica ou pela
linha de comando, por exemplo:

    sudo installer -pkg zabbix_agent-5.4.0-macos-amd64-openssl.pkg -target /

Certifique-se de utilizar a versão correta do pacote Zabbix no comando. Esta deve
corresponder ao nome do pacote baixado.


[comment]: # ({/cf25cbbf-3688af62})

[comment]: # ({ed5deabf-052e551c})
#### Executando o agente

O agente iniciará automaticamente após a instalação ou reinício.

Você pode editar o arquivo de configuração em
`/usr/local/etc/zabbix/zabbix_agentd.conf` se necessário.

Para iniciar o agente manualmente, você pode executar:

    sudo launchctl start com.zabbix.zabbix_agentd

Para parar o agente manualmente:

    sudo launchctl stop com.zabbix.zabbix_agentd

Durante a atualização, o arquivo de configuração existente não é sobrescrito.
Em vez disso um novo arquivo `zabbix_agentd.conf.NEW` é criado para ser usado
na revisão e atualização das configurações existentes, se necessário.
Lembre-se de reiniciar o agente após alterações manuais nos arquivos de configuração.

[comment]: # ({/ed5deabf-052e551c})

[comment]: # ({892cd194-3150eaf4})
#### Correção e remoção do agente

Esta seção lista alguns comandos úteis que podem ser usados para
solução de problemas e remoção da instalação do Zabbix Agent.

Veja se o Zabbix Agent está sendo executado:

    ps aux | grep zabbix_agentd

Veja se o Zabbix Agent foi instalado pelos pacotes:

    $ pkgutil --pkgs | grep zabbix 
    com.zabbix.pkg.ZabbixAgent

Confira os arquivos que foram instalados pelo pacote de instalação (note que
a barra inicial `/` não é mostrada nesta visualização):

    $ pkgutil --only-files --files com.zabbix.pkg.ZabbixAgent
    Library/LaunchDaemons/com.zabbix.zabbix_agentd.plist                                                                                                                                                                                                                           
    usr/local/bin/zabbix_get                                                                                                                                                                                                                                                       
    usr/local/bin/zabbix_sender                                                                                                                                                                                                                                                    
    usr/local/etc/zabbix/zabbix_agentd/userparameter_examples.conf.NEW                                                                                                                                                                                                             
    usr/local/etc/zabbix/zabbix_agentd/userparameter_mysql.conf.NEW                                                                                                                                                                                                                
    usr/local/etc/zabbix/zabbix_agentd.conf.NEW                                                                                                                                                                                                                                    
    usr/local/sbin/zabbix_agentd

Pare o Zabbix Agent se ele foi iniciado com o `launchctl`:

    sudo launchctl unload /Library/LaunchDaemons/com.zabbix.zabbix_agentd.plist

Remova os arquivos (incluindo configurações e logs) que foram instalados
com o pacote de instalação:

    sudo rm -f /Library/LaunchDaemons/com.zabbix.zabbix_agentd.plist
    sudo rm -f /usr/local/sbin/zabbix_agentd
    sudo rm -f /usr/local/bin/zabbix_get
    sudo rm -f /usr/local/bin/zabbix_sender
    sudo rm -rf /usr/local/etc/zabbix
    sudo rm -rf /var/log/zabbix

Utilize o comando forget para o Zabbix Agent:

    sudo pkgutil --forget com.zabbix.pkg.ZabbixAgent

[comment]: # ({/892cd194-3150eaf4})
