[comment]: # translation:outdated

[comment]: # ({d96d3275-d96d3275})
# 2 Debian/Ubuntu/Raspbian

[comment]: # ({/d96d3275-d96d3275})

[comment]: # ({new-9df568e9})
### Visão geral

Pacotes oficiais do Zabbix estão disponíveis para:

|   |   |
|---|---|
|Debian 10 (Buster)|[Download](https://www.zabbix.com/download?zabbix=5.4&os_distribution=debian&os_version=10_buster&db=mysql)|
|Debian 9 (Stretch)|[Download](https://www.zabbix.com/download?zabbix=5.4&os_distribution=debian&os_version=9_stretch&db=mysql)|
|Debian 8 (Jessie)|[Download](https://www.zabbix.com/download?zabbix=5.4&os_distribution=debian&os_version=8_jessie&db=mysql)|
|Ubuntu 20.04 (Focal Fossa) LTS|[Download](https://www.zabbix.com/download?zabbix=5.4&os_distribution=ubuntu&os_version=20.04_focal&db=mysql)|
|Ubuntu 18.04 (Bionic Beaver) LTS|[Download](https://www.zabbix.com/download?zabbix=5.4&os_distribution=ubuntu&os_version=18.04_bionic&db=mysql)|
|Ubuntu 16.04 (Xenial Xerus) LTS|[Download](https://www.zabbix.com/download?zabbix=5.4&os_distribution=ubuntu&os_version=16.04_xenial&db=mysql)|
|Ubuntu 14.04 (Trusty Tahr) LTS|[Download](https://www.zabbix.com/download?zabbix=5.4&os_distribution=ubuntu&os_version=14.04_trusty&db=mysql)|
|Raspbian (Buster)|[Download](https://www.zabbix.com/download?zabbix=5.4&os_distribution=raspbian&os_version=10_buster&db=mysql)|
|Raspbian (Stretch)|[Download](https://www.zabbix.com/download?zabbix=5.4&os_distribution=raspbian&os_version=9_stretch&db=mysql)|

Os pacotes estão disponíveis com qualquer banco de dados MySQL/PostgreSQL e
Suporte ao servidor da web Apache/Nginx. 

::: noteimportant
O Zabbix 6.0 ainda não foi lançado. Os links de download
levar a pacotes pré-6.0.
:::

[comment]: # ({/new-9df568e9})

[comment]: # ({new-ae631a63})
### Notas sobre a instalação

Veja o [installation
instructions](https://www.zabbix.com/download?zabbix=5.0&os_distribution=debian&os_version=10_buster&db=mysql)
por plataforma na página de download para:

- instalando o repositório
- instalando servidor / agente / frontend
- criação de banco de dados inicial, importação de dados iniciais
- configurar banco de dados para o servidor Zabbix
- configurar o PHP para o frontend Zabbix
- iniciar processos de servidor / agente
- configurando o frontend do Zabbix

Se você deseja executar o agente Zabbix como root, consulte [running agent as
root](/manual/appendix/install/run_agent_as_root).

Processo de serviço da web Zabbix, que é usado para [scheduled report
generation](/manual/web_interface/frontend_sections/reports/scheduled),
requer o navegador Google Chrome. O navegador não está incluído em
pacotes e deve ser instalado manualmente.

[comment]: # ({/new-ae631a63})

[comment]: # ({new-99a402fa})
#### Importando dados com Timescale DB

Com o TimescaleDB, além do comando de importação para PostgreSQL, também
corre:

    # zcat /usr/share/doc/zabbix-sql-scripts/postgresql/timescaledb.sql.gz | sudo -u zabbix psql zabbix

::: notewarning
TimescaleDB é compatível com o servidor Zabbix
só.
:::

[comment]: # ({/new-99a402fa})


[comment]: # ({d6578882-fa73411a})
#### Configuração SELinux

Veja [SELinux
configuration](/manual/installation/install_from_packages/rhel_centos#selinux_configuration)
para RHEL/CentOS.

Após a configuração do frontend e do SELinux, reinicie o Apache
servidor web:

    # service apache2 restart

[comment]: # ({/d6578882-fa73411a})

[comment]: # ({ad6e126e-a12da003})
### Instalação Proxy

Assim que o repositório necessário for adicionado, você pode instalar o proxy Zabbix por
Executando:

    # apt install zabbix-proxy-mysql

Substitua 'mysql' no comando por 'pgsql' para usar PostgreSQL, ou
com 'sqlite3' para usar SQLite3.

[comment]: # ({/ad6e126e-a12da003})

[comment]: # ({8544df32-fe6abb8e})
##### Criando banco de dados

[Create](/manual/appendix/install/db_scripts) um banco de dados separado para
Proxy Zabbix.

O servidor Zabbix e o proxy Zabbix não podem usar o mesmo banco de dados. Se eles são
instalado no mesmo host, o banco de dados proxy deve ter um diferente
nome.

[comment]: # ({/8544df32-fe6abb8e})

[comment]: # ({new-2ab835d7})
##### Importando dados

Importar esquema inicial:

    # zcat /usr/share/doc/zabbix-sql-scripts/mysql/schema.sql.gz | mysql -uzabbix -p zabbix

Para proxy com PostgreSQL (ou SQLite):

    # zcat /usr/share/doc/zabbix-sql-scripts/postgresql/schema.sql.gz | sudo -u zabbix psql zabbix
    # zcat /usr/share/doc/zabbix-sql-scripts/sqlite3/schema.sql.gz | sqlite3 zabbix.db

[comment]: # ({/new-2ab835d7})

[comment]: # ({64199131-d0a225c7})
##### Configurar banco de dados para Zabbix proxy

Edit zabbix\_proxy.conf:

    # vi /etc/zabbix/zabbix_proxy.conf
    DBHost=localhost
    DBName=zabbix
    DBUser=zabbix
    DBPassword=<password>

Em DBName para Zabbix proxy, use um banco de dados separado do servidor Zabbix.

Em DBPassword, use a senha do banco de dados Zabbix para MySQL; Usuário PostgreSQL
senha para PostgreSQL.

Use `DBHost =` com PostgreSQL. Você pode querer manter o padrão
definir `DBHost = localhost` (ou um endereço IP), mas isso faria
O PostgreSQL usa um soquete de rede para se conectar ao Zabbix. Consulte o
[respective
section](/manual/installation/install_from_packages/rhel_centos#selinux_configuration)
para RHEL/CentOS para obter instruções.

[comment]: # ({/64199131-d0a225c7})

[comment]: # ({90f29d4d-27de2ced})
##### Iniciando o processo de Zabbix proxy

Para iniciar um processo de Zabbix proxy e fazê-lo iniciar na inicialização do sistema:

    # systemctl restart zabbix-proxy
    # systemctl enable zabbix-proxy

[comment]: # ({/90f29d4d-27de2ced})

[comment]: # ({07c8faf5-871a973b})
##### Configuração de front-end

Um Zabbix proxy não tem front-end; ele se comunica com o Zabbix
servidor apenas.

[comment]: # ({/07c8faf5-871a973b})

[comment]: # ({e3905cb7-cd9340bd})
### Instalação do Java gateway

É necessário instalar [Java gateway](/manual/concepts/java) somente se
você deseja monitorar aplicativos JMX. O gateway Java é leve e
não requer um banco de dados.

Assim que o repositório necessário for adicionado, você pode instalar o Zabbix Java
gateway executando:

    # apt install zabbix-java-gateway

Prossiga para [setup](/manual/concepts/java/from_debian_ubuntu) para mais
detalhes sobre como configurar e executar o Java gateway.

[comment]: # ({/e3905cb7-cd9340bd})
