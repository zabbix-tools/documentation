[comment]: # translation:outdated

[comment]: # ({a948d268-7ef4b2b9})
# Boas práticas para configuração segura do Zabbix

[comment]: # ({/a948d268-7ef4b2b9})

[comment]: # ({12f1463d-b9719f19})
#### Visão geral

Esta seção contém boas práticas que devem ser observadas de modo
a configurar o Zabbix de uma forma segura.

As práticas contidas aqui não são necessárias para o funcionamento
do Zabbix. Elas são recomendadas para uma melhor segurança do
sistema.

[comment]: # ({/12f1463d-b9719f19})

[comment]: # ({new-cebf8306})
### Access control

[comment]: # ({/new-cebf8306})

[comment]: # ({4bea3022-dc572a02})

#### Princípio do menor privilégio

O princípio do menor privilégio deve ser usado todo o tempo no Zabbix.
Este princípio implica que contas de usuário (no Zabbix Frontend) ou 
usuários de processo (para Zabbix Server/Proxy ou Agent) tenham apenas
aqueles privilégios essenciais para executar as funções pretendidas.
Em outras palavras, contas de usuário devem fornecer o mínimo de 
privilégios possível, durante todo o tempo.

::: noteimportant
Fornecer permissões extras ao usuário 'zabbix' permitirá que este
acesse os arquivos de configuração e execute operações que podem
comprometer a segurança geral da infraestrutura.
:::

Quando implementando o princípio de mínimo privilégio para contas
de usuário, os [tipos de usuário do frontend](/manual/config/users_and_usergroups/permissions) do Zabbix devem ser 
levados em conta. É importante entender que enquanto um usuário do 
tipo "Admin" tem menos privilégios do um usuário do tipo "Super Admin", 
ele tem permissões administrativas que o permitem gerenciar configurações 
e executar scripts customizados.

::: noteclassic
Algumas informações estão disponíveis até mesmo para usuários
sem privilégio. Por exemplo, conquanto *Administração* → *Scripts* 
esteja disponível apenas para usuários Super Admins, os próprios
scripts estão disponíveis para recuperação através do uso da API 
do Zabbix. Limitação nas permissões dos scripts e a não adição de
informações sensíveis (como credenciais de acesso, etc) devem ser
consideradas para evitar a exposição de informações sensíveis
existentes nos scripts globais.
:::

[comment]: # ({/4bea3022-dc572a02})

[comment]: # ({cba04724-1631be73})
#### Usuário seguro para Zabbix Agent

Na configuração padrão, os processos do Zabbix Server e Zabbix Agent
compartilham um usuário 'zabbix'. Se você desejar certificar-se de que 
o Agent não tenha acesso a detalhes sensíveis na configuração do Server
(p.e. informações de login do banco de dados), o Agent deve ser executado
com um usuário diferente:

1.  Crie um usuário seguro
2.  Especifique este usuário no [arquivo de configuração](/manual/appendix/config/zabbix_agentd) (parâmetro 'User') do Agent
3.  Reinicie o Agent com privilégios de administrador. Estes privilégios serão substituídos 
    pelos privilégios do usuário especificado.

[comment]: # ({/cba04724-1631be73})

[comment]: # ({f72f937a-40e70f9e})

#### Codificação UTF-8

O UTF-8 é o único formato de codificação suportado pelo Zabbix. É conhecido por
operar sem quaisquer falhas de segurança. Usuários devem estar cientes de que há
problemas de segurança conhecidos quando usando algum dos outros formatos.

[comment]: # ({/f72f937a-40e70f9e})

[comment]: # ({new-62eeb2ec})
### Windows installer paths
When using Windows installers, it is recommended to use default paths provided by the installer as using custom paths without proper permissions could compromise the security of the installation.

[comment]: # ({/new-62eeb2ec})

[comment]: # ({3df46007-f8f5054e})

#### Configurando SSL para Zabbix Frontend

No RHEL/Centos, instale o pacote mod\_ssl:

    yum install mod_ssl

Crie diretório para as chaves SSL:

    mkdir -p /etc/httpd/ssl/private
    chmod 700 /etc/httpd/ssl/private

Crie o certificado SSL:

    openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/httpd/ssl/private/apache-selfsigned.key -out /etc/httpd/ssl/apache-selfsigned.crt

Preencha cada informação com os dados apropriados. A linha mais importante
é a que solicita o Nome Comum (Common Name). Aqui você precisa informar
o nome de domínio que deseja que seja associado ao seu servidor. Você pode
informar o endereço de IP público caso não tenha um nome de domínio. Neste
artigo nós faremos uso do nome *example.com*.

    Country Name (2 letter code) [XX]:
    State or Province Name (full name) []:
    Locality Name (eg, city) [Default City]:
    Organization Name (eg, company) [Default Company Ltd]:
    Organizational Unit Name (eg, section) []:
    Common Name (eg, your name or your server's hostname) []:example.com
    Email Address []:

Edite as configurações de SSL do Apache:

    /etc/httpd/conf.d/ssl.conf

    DocumentRoot "/usr/share/zabbix"
    ServerName example.com:443
    SSLCertificateFile /etc/httpd/ssl/apache-selfsigned.crt
    SSLCertificateKeyFile /etc/httpd/ssl/private/apache-selfsigned.key

Reinicie o serviço do Apache para aplicar as alterações:

    systemctl restart httpd.service

[comment]: # ({/3df46007-f8f5054e})

[comment]: # ({new-40cfdec2})
### Web server hardening

[comment]: # ({/new-40cfdec2})

[comment]: # ({1dfcf20d-bb3706f4})

#### Habilitando o Zabbix no diretório raíz da URL

Adicione um virtual host na configuração do Apache e configure um
redirecionamento permanente do diretório raíz (DocumentRoot) para a 
URL com SSL do Zabbix. Não esqueça de substituir *example.com* 
pelo nome real do servidor.

    /etc/httpd/conf/httpd.conf

    #Linhas adicionais

    <VirtualHost *:*>
        ServerName example.com
        Redirect permanent / https://example.com
    </VirtualHost>

Reinicie o serviço do Apache para aplicar as alterações:

    systemctl restart httpd.service && systemctl status httpd.service

[comment]: # ({/1dfcf20d-bb3706f4})

[comment]: # ({40ef8447-c7ee0bb2})

#### Habilitando HTTP Strict Transport Security (HSTS) no servidor web

Para proteger o Zabbix Frontend contra ataques de rebaixamento de protocolo,
nós recomendamos habilitar a política de [HSTS](https://en.wikipedia.org/wiki/HTTP_Strict_Transport_Security) no servidor web.

Por exemplo, para habilitar a política HSTS para seu Zabbix Frontend na
configuração do Apache:

    /etc/httpd/conf/httpd.conf

adicione a seguinte diretiva à configuração do seu virtual host:

    <VirtualHost *:443>
       Header set Strict-Transport-Security "max-age=31536000"
    </VirtualHost>

Reinicie o serviço do Apache para aplicar as alterações:

    systemctl restart httpd.service

[comment]: # ({/40ef8447-c7ee0bb2})

[comment]: # ({14aa092f-cd09dcd1})

#### Desabilitando exposição de informação do servidor web

É recomendado desabilitar todas as assinaturas do web server como 
parte do processo de garantia da segurança. O Web Server expõe sua
assinatura de software por padrão:

![](../../../../assets/en/manual/installation/requirements/software_signature.png)

A assinatura pode ser desabilitada através da adição de duas linhas no
arquivo de configuração do Apache, por exemplo::

    ServerSignature Off
    ServerTokens Prod

A assinatura do PHP (X-Powered-By HTTP header) pode ser desabilitada
pela alteração do arquivo de configuração php.ini (neste caso a assinatura
é desabilitada por padrão):

    expose_php = Off

Um reinício do Web Server é necessário para que as alterações no arquivo
de configuração sejam aplicadas.

Um nível adicional de segurança pode ser alcançado usando mod\_security
(pacote libapache2-mod-security2) com Apache. O mod\_security permite
remover toda a assinatura do server em vez de apenas remover a versão.
A assinatura pode ser alterada para qualquer valor pela alteração de 
"SecServerSignature" para um valor desejado após a instalação do
mod\_security.

Por favor, consulte a documentação do seu Web Server para encontrar
auxílio em como remover/alterar as assinaturas de software.

[comment]: # ({/14aa092f-cd09dcd1})

[comment]: # ({73bf01c0-720052da})

#### Desabilitando as páginas de erro padrão do Web Server

É recomendado desabilitar as páginas de erro padrão para evitar
exposição de informação. O Web Server usa as páginas de erro
embutidas por padrão:

![](../../../../assets/en/manual/installation/requirements/error_page_text.png)

Páginas de erro padrão devem ser substituídas/removidas como parte do 
processo de aprimoramento da segurança. A diretiva "ErrorDocument" 
pode ser usada para definir uma página/texto de erro customizado para
o Apache Web Server (usado como exemplo).

Por favor, consulte a documentação do seu Web Server para encontrar
auxílio em como substituir/remover as páginas de erro padrão.

[comment]: # ({/73bf01c0-720052da})

[comment]: # ({0f34b9ea-ba1547c0})

#### Removendo a página de teste do Web Server

É recomendado remover a página de teste do Web Server para evitar
a exposição de informações. Por padrão, o diretório raíz do Web Server
contém uma página de teste chamada index.html (usando Apache2 no 
Ubuntu como exemplo):

![](../../../../assets/en/manual/installation/requirements/test_page.png)

A página de teste deve ser removida ou tornada indisponível como parte
do processo de aprimoramento da segurança do Web Server.

[comment]: # ({/0f34b9ea-ba1547c0})

[comment]: # ({4f76fd61-7e4e4a45})

#### Configurações do Zabbix

Por padrão, o Zabbix possui a opção *X-Frame-Options HTTP response
header* configurada como `SAMEORIGIN`, significando que o conteúdo
só pode ser carregado em um frame que tenha a mesma origem da
página em si.

Os elementos do Zabbix Frontend que buscam conteúdo de URLs
externas (especificamente, o [widget de URL para dashboard](/manual/web_interface/frontend_sections/monitoring/dashboard/widgets#url))
apresentam o conteúdo encontrado em uma área isolada (sandbox) 
com todas as restrições habilitadas.

Estas configurações aprimoram a segurança do Zabbix Frontend e 
proveem proteção contra ataques do tipo XSS e clickjacking. 
Super Admins podem [modificar](/manual/web_interface/frontend_sections/administration/general#security) os parâmetros *iframe sandboxing* e 
*X-Frame-Options HTTP response header* conforme necessário.
Por favor, pese cuidadosamente os riscos e benefícios antes de
alterar as configurações padrão. Desativar completamente a função
de sandboxing ou X-Frame-Options não é recomendado.

[comment]: # ({/4f76fd61-7e4e4a45})

[comment]: # ({26c91c1d-c02c850f})

#### Zabbix Windows Agent com OpenSSL


O Zabbix Windows Agent compilado com OpenSSL tentará encontrar o 
arquivo de configuração SSL em C:\\openssl-64bit. O diretório "openssl-64bit"
na partição C: pode ser criado por usuários sem privilégio elevado.

Assim, para aprimoramento da segurança, é necessária a criação deste
diretório manualmente e então a retirada do acesso de escrita para os
usuários que não sejam administradores.

Por favor, note que os nomes do diretório serão diferentes nas versões
32-bit e 64-bit do Windows.

[comment]: # ({/26c91c1d-c02c850f})

[comment]: # ({new-6cfe5a3b})
### Cryptography

[comment]: # ({/new-6cfe5a3b})

[comment]: # ({new-3f76ae70})

#### Escondendo o arquivo com lista de senhas mais comuns

Para aumentar a complexidade de ataques de força bruta sobre senha, 
é sugerido limitar o acesso ao arquivo `ui/data/top_passwords.txt`
modificando a configuração do Web Server. Este arquivo contém uma
lista das senhas mais comuns e com contexto específico, e é usado para
prevenir os usuários de criar senhas semelhantes se o parâmetro 
*Avoid easy-to-guess passwords* estiver habilitado na [política de senha](/manual/web_interface/frontend_sections/administration/authentication#internal_authentication).

Por exemplo, no NGINX o acesso ao arquivo pode ser limitado usando a 
diretiva `location`:

    location = /data/top_passwords.txt {​​​​​​​
        deny all;
        return 404;
    }​​​​​​​

No Apache, usando o arquivo `.htacess`:

    <Files "top_passwords.txt">  
      Order Allow,Deny
      Deny from all
    </Files>

​

[comment]: # ({/new-3f76ae70})

[comment]: # ({new-ba0d4036})

#### Security vulnerabilities

##### CVE-2021-42550

In Zabbix Java gateway with logback version 1.2.7 and prior versions, an attacker with the required 
privileges to edit configuration files could craft a malicious configuration allowing to execute 
arbitrary code loaded from LDAP servers.

Vulnerability to [CVE-2021-42550](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-42550) 
has been fixed since Zabbix 5.4.9. However, as an additional security measure it is recommended 
to check permissions to the `/etc/zabbix/zabbix_java_gateway_logback.xml` file and set it read-only, 
if write permissions are available for the "zabbix" user. 

[comment]: # ({/new-ba0d4036})
