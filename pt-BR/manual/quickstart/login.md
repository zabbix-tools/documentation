[comment]: # translation:outdated

[comment]: # ({new-3f54a0e9})
# 1 Autenticando e configurando o usuário

[comment]: # ({/new-3f54a0e9})

[comment]: # ({new-f6cb6160})
#### Visão geral

Nesta sessão você aprenderá como se autenticar e configurar um usuário
de sistema no Zabbix.

[comment]: # ({/new-f6cb6160})

[comment]: # ({new-6cb1478f})
#### Autenticação

![](../../../assets/en/manual/quickstart/login.png){width="350"}

Esta é a "tela de boas vindas" do Zabbix. Informe o usuário **Admin**
com a senha **zabbix** para se autenticar como um [Super Usuário
Zabbix](/pt/manual/config/users_and_usergroups/permissions).

Uma vez conectado, você verá a mensagem 'Conectado como Admin' no canto
inferior direito da tela. O acesso aos menus *Configuração* e
*Administração* será liberado.

[comment]: # ({/new-6cb1478f})

[comment]: # ({new-ecf369b8})
##### Proteção contra ataques por força bruta

Em caso de falha em cinco tentativas consecutivas de autenticação a
Interface do Zabbix irá pausar por 30 segundos para mitigar ataque por
força bruta e ataque por dicionário.

O endereço IP de origem da tentativa de conexão será exibido para o
usuário real após uma tentativa bem sucedida de autenticação.

[comment]: # ({/new-ecf369b8})

[comment]: # ({new-6be209b3})
#### Adicionando um usuário

Para visualizar os dados dos usuários, vá para *Administração →
Usuários*.

![](../../../assets/en/manual/quickstart/userlist.png){width="600"}

Inicialmente existem dois usuários criados no Zabbix.

-   'Admin' - Super Usuário Zabbix com privilégios totais.
-   'Guest' - Usuário especial e default. Se você não está conectado
    você está utilizando o ambiente com privilégio de convidado. Por
    padrão, a conta "guest" não tem acesso a nenhum host monitorado.

Para adicionar um novo usuário, clique em *Criar usuário*.

Ao preencher o formulário certifique-se de atribuir ao usuário no mínimo
um [grupo de
usuários](/pt/manual/config/users_and_usergroups/usergroup), por exemplo
'Zabbix ​administrators'.

::: noteimportant
Este grupo não é um grupo adicionado por padrão no
Zabbix, é apenas um exemplo. Para aprender sobre como criar grupos de
usuários favor consultar [o manual de grupos de
usuários](/pt/manual/config/users_and_usergroups/usergroup).
:::

![](../../../assets/en/manual/quickstart/new_user.png)

Por padrão os novos usuários não possuem nenhum tipo de mídia (métodos
entrega de notificações) definido. Para criar um clique na aba 'Media' e
clique no link *Adicionar*.

::: noteimportant
Observe que neste momento na tela existe um botão
e um link com o texto 'Adicionar', o botão irá tentar adicionar o
usuário. E não é o que desejamos ainda.
:::

![](../../../assets/en/manual/quickstart/new_media.png)

Será apresentada um pop-up, informe um e-mail para o usuário.

Você pode definir um período (em horas) em que a mídia estará disponível
para este usuário (veja a página de [especificação de períodos de
hora](/pt/manual/appendix/time_period) para maiores detalhes), por
padrão uma mídia está sempre ativa. Você também pode personalizar os
[níveis de severidade de trigger](/pt/manual/config/triggers/severity)
para os quais esta mídia estará ativa, neste momento não faça estas
personalizações.

Clique em *Adicionar* e, após a tela recarregar, clique no botão
*Adicionar* no formulário de propriedades do usuário. O novo usuário
deverá aparecer na lista de usuários.

![](../../../assets/en/manual/quickstart/userlist2.png){width="600"}

[comment]: # ({/new-6be209b3})

[comment]: # ({new-6e8f46b1})
##### Adicionando permissões

Por padrão, um novo usuário não possui permissão para acesso a nenhum
host. Para atribuir os privilégios, clique no grupo do usuário na coluna
*Grupos* (neste caso - 'Zabbix administrators'). No formulário com as
propriedades do grupo, clique na aba *Permissões*.

![](../../../assets/en/manual/quickstart/group_permissions.png){width="600"}

Este usuário precisa ter acesso de leitura ao grupo de hosts *Linux
servers*, então clique no botão *Adicionar* situado logo abaixo da lista
'SOMENTE LEITURA'.

![](../../../assets/en/manual/quickstart/add_permissions.png)

No pop-up apresentado marque o checkbox próximo ao grupo 'Linux servers'
e clique no botão *Selecionar*. *Linux servers* deverá ser apresentado
na lista respectiva. No formulário de proprieades do grupo de usuários,
clique em *Atualizar*.

::: noteimportant
No Zabbix todas as permissões para os hosts são
atribuidas por [grupos de
usuários](/pt/manual/config/users_and_usergroups/usergroup), não pelos
usuários individuais.
:::

Pronto! Você já pode tentar se autenticar com as credenciais do novo
usuário.

[comment]: # ({/new-6e8f46b1})
