[comment]: # translation:outdated

[comment]: # ({new-8cf8bd5f})
# 3 Novo item

[comment]: # ({/new-8cf8bd5f})

[comment]: # ({new-73502ab6})
#### Visão Geral

Esta sessão irá demonstrar como criar um novo item.

Itens são o elemento básico de coleta no Zabbix. Sem eles não existe
dado pois apenas eles definem o que e quando coletar.

[comment]: # ({/new-73502ab6})

[comment]: # ({new-8cca3d66})
#### Adicionando um item

Todos os itens são agrupados em hosts (mas podem ter sido definidos em
templates). Por isso para configurar um item é necessário acessar o menu
*Configuração → Hosts* e procurar o host onde se deseja criar o item.

O link *Itens* da linha do 'Host selecionado' apresenta a quantidade de
itens nele (neste exemplo utilizaremos o 'New Host' criado no [inicio
rápido de hosts](/pt/manual/quickstart/host)) e neste momento ele não
possui itens. Clique no link *Itens* e, na sequência, clique em *Criar
item*. Será apresentado o formulário de cadastro de itens.

![](../../../assets/en/manual/quickstart/new_item.png)

No nosso item de exemplo os dados essenciais são:

***Nome***

-   Informe *CPU Load*. Este nome será apresentado nas listagens e em
    outros locais relevantes.

***Chave***

-   Informe *system.cpu.load*. Este é o nome técnico do item que
    identifica o conteúdo da informação que será coletada. Esta chave em
    particular é apenas uma das [chaves
    pré-definidas](/manual/config/items/itemtypes/zabbix_agent) que são
    suportadas por um Zabbix Agent.

***Tipo de informação***

-   Selecione *Numérico (fracionário)*. Este atributo define o formado
    do dado aguardado a cada coleta.

::: noteclassic
Você pode querer reduzir a quantidade de dados que você
retêm no [histórico de
coletas](/manual/config/items/history_and_trends), talvez para 7 ou 14
dias. Esta é uma boa prática que alivia a carga de dados armazenada no
banco de dados do Zabbix.
:::

[Outras opções](/pt/manual/config/items/item#configuration) vamos manter
no valor default por enquanto.

Quando concluir, clique em *Adicionar*. O novo item deverá aparecer na
lista de itens do host selecionado.

![](../../../assets/en/manual/quickstart/item_created.png)

[comment]: # ({/new-8cca3d66})

[comment]: # ({new-c33e07f1})
#### Consultado os dados

Uma vez que o item tenha sido adicionado, você pode desejar para
visualizar os dados coletados. Para isso, vá para *Monitoramento → Dados
Recentes*, clique no ícone **+** antes de **- other -** para visualizar
o seu item e os dados coletados.

![](../../../assets/en/manual/quickstart/latest_data.png){width="600"}

Na prática o primeiro dado pode demorar até 60 segundos para chegar.
Isso ocorre pois o Zabbix Server tem, por padrão, uma recarga de
configurações de monitoração neste período.

A coluna 'Alterar' ficará sem valor até que se passe tempo suficiente
para ocorrerem duas coletas do item.

Se você sua tela de dados recentes não estiver similar à imagem acima,
certifique-se que:

-   você informou a 'Chave' e o 'Tipo da informação' exatamente como o
    sugerido
-   tanto o Zabbix Server quanto o Zabbix Agent estão sendo executados
    neste momento
-   o status do host é 'Monitorado' e seu ícone de disponibilidade está
    verde
-   o item cadastrado está com o status: **ativo**

[comment]: # ({/new-c33e07f1})

[comment]: # ({new-7e0cbbcd})
##### Gráficos

Passado algum tempo após o item estar coletando com sucesso os dados
podemos começar a pensar em apresentações visualmente mais agradáveis. O
recurso de [gráficos
simples](/pt/manual/config/visualisation/graphs/simple) está disponível
para qualquer item numérico e não necessita de configuração adicional.
Estes gráficos são gerados em tempo de execução.

Para visualizar o gráfico, vá para *Monitoramento → Dados recentes* e
clique no link 'Gráfico' na linha do item desejado.

![](../../../assets/en/manual/quickstart/simple_graph.png){width="600"}

[comment]: # ({/new-7e0cbbcd})
