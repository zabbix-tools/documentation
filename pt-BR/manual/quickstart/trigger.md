[comment]: # translation:outdated

[comment]: # ({new-848667e6})
# 4 Nova trigger

[comment]: # ({/new-848667e6})

[comment]: # ({new-97bd6e32})
#### Visão geral

Nesta sessão apresentaremos como configurar uma trigger.

Itens apenas coletam dados. Para avaliar automaticamente os dados nós
precisamos definir uma trigger. Uma trigger contêm uma expressão que
define o limite aceitável para o dado recebido.

Se este nível for ultrapassado pelo dado recebido, a trigger irá ser
"disparada" e ir para o estado de 'Incidente', nos informando que algo
ocorreu que necessita de nossa atenção. Quando o nível for aceitável
novamente, a trigger volta para o estado de 'Ok'.

[comment]: # ({/new-97bd6e32})

[comment]: # ({new-e69902f4})
#### Adicionando uma trigger

Para configurar uma trigger para o nosso item, acesse o menu
*Configuração → Hosts*, procure pelo host 'New host' e clique no link
*Triggers* da linha do nosso host, na sequência clique no botão *Criar
trigger*. Será apresentado o formulário para cadastramento de trigger.

![](../../../assets/en/manual/quickstart/new_trigger.png)

Para a nossa trigger as informações essenciais são:

*Nome*

-   Informe *CPU load too high on 'New host' for 3 minutes*. Este será o
    nome apresentado na listagem de triggers e demais locais.

*Expressão*

-   Informe *{New host:system.cpu.load.avg(180)}>2*

Esta é a expressão da trigger, certifique-se de preencher este campo
corretamente até o último caractere. A chave aqui referenciada
(system.cpu.load) é utilizada no exemplo de item criado anteriormente.
Esta expressão em particular informa quando a carga média de CPU for
excedida, por três minutos, do valor 2. Você pode aprender mais sobre a
[syntaxe das expressões de trigger](/manual/config/triggers/expression)
neste manual.

Quando terminar, clique no botão *Adicionar*. A nova trigger deverá
aparecer na listagen de triggers do host.

[comment]: # ({/new-e69902f4})

[comment]: # ({new-7a7f221f})
#### Consultando o estado da trigger

Uma vez que a trigger tenha sido definida você poderá querer consultar o
seu estado.

Para isso, acesse *Monitoramento → Triggers*. Após aproximadamente 3
minutos (este tempo pode variar um pouco) a sua trigger deverá aparecer
lá, possivelmente com o valor 'Ok' na coluna 'Status'.

![](../../../assets/en/manual/quickstart/trigger_status.png){width="600"}

O valor piscando na coluna de status indica modificação recente no
estado, em até 30 minutos após a mudança de estado o valor continuará
piscando.

Se tiver um estado de 'Incidente' vermelho piscando, isso indica que a
carga de CPU excedeu o nível definido na trigger.

[comment]: # ({/new-7a7f221f})
