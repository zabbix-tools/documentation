[comment]: # translation:outdated

[comment]: # ({new-4c8ef099})
# 5 Recebendo notificações de problemas

[comment]: # ({/new-4c8ef099})

[comment]: # ({new-055c3bc8})
#### Visão geral

Nesta sessão iremos apresentar como configurar notificações de alertas
no Zabbix.

Os itens coletam os dados, as triggers são disparadas a partir destes
dados e expressões de validação o próximo passo nautral é ter a
funcionalidade de acionar um mecanismo de alerta que nos informe sobre
eventos importantes sem que, para isso, nós precisemos estar conectados
à interface web do Zabbix.

É isso que as notificações fazem. A notificação por e-mail é a forma
mais popular de notificações de problemas e, por isso, iremos demonstrar
aqui esta forma de notificação.

[comment]: # ({/new-055c3bc8})

[comment]: # ({new-237d8e2e})
#### Configuração de E-mail

Existem várias [formas de
notificação](/manual/config/notifications/media) pré-definidas no
Zabbix. A notificação por
[e-mail](/manual/config/notifications/media/email) é uma delas.

Para configurar as informações de envio de e-mail, acesse *Administração
→ Tipos de mídias* e clique no link *Email* na lista de tipos de mídia.

![](../../../assets/en/manual/quickstart/media_types.png){width="600"}

Será apresentado o formulário de configurações de e-mail.

![](../../../assets/en/manual/quickstart/media_type_email.png)

Informe os valores do **Servidor SMTP, SMTP helo e E-mail SMTP** de
forma a estar correto para o seu ambiente.

::: noteclassic
O campo 'E-mail SMTP' deverá ser preenchido com o endereço
de e-mail a ser utilizado para o envio das mensagens.
:::

::: noteimportant
Esta configuração exige que o servidor relay de
SMTP aceite envio de e-mail sem autenticação, para enviar e-mail
autenticado você deverá modificar o valor do campo **Segurança de
Conexão** e, provavelmente, também o valor do campo **Autenticação**
informando o usuário e senha a serem utilizados.
:::

Pressione o botão *Atualizar* quando tiver concluído a configuração.

Agora você configurou o tipo de mídia 'Email' e ele está funcional. Além
da configuração do tipo de mídia, será necessário que seja cadastrado em
cada usuário uma mídia com o seu endereço de e-mail, caso contrário as
notificações não chegarão.

[comment]: # ({/new-237d8e2e})

[comment]: # ({new-c1762601})
#### Nova ação

Entregar notificaões é uma das coisas que o recurso de
[ações](/manual/config/notifications/action) do Zabbix faz. Portanto,
para configurar uma notificação, acesse *Configuração → Ações* e clique
em *Criar ação*.

Neste formulário, informe o nome da ação, conforme imagem a seguir.

![](../../../assets/en/manual/quickstart/new_action.png)

Neste mesmo formulário são apresentadas algumas macros, tais quais
{TRIGGER.STATUS} e {TRIGGER.NAME}. Existem macros sendo utilizadas tanto
no campo *Assunto padrão* quanto no campo *Mensagem padrão*. O valor
destas macros será substituído pelo valor atual em tempo de execução.

::: noteclassic
As macros são um recurso nativo do Zabbix que serve para
dinamizar conteúdos e configurações. Consulte a documentação de
[macros](/pt/manual/appendix/macros/supported_by_location) para maiores
informações sobre quais são aplicáveis junto ao recurso de
ações.
:::

Simplificando, se nós não adicionarmos nenhuma [condição mais
específica](/pt/manual/config/notifications/action/conditions), a ação
irá ser executada sempre que ocorrer uma mudança de estado em uma
trigger (seja indo para o estado de 'Ok' ou de 'Incidente').

Além de definir o nome, o conteúdo da mensagem e as condições em que a
ação deverá ser acionada nós precisamos também definir o que deverá ser
feito. Isso é configurado na aba *Ações*. Clique no link *Nova* dentro
da caixa de operações da ação.

![](../../../assets/en/manual/quickstart/new_operation.png){width="600"}

Agora, clique no link *Adicionar* dentro da caixa *Enviar para usuários*
marque e selecione o usuário que criamos anteriormente. Selecione a
opção 'Email' como valor para o campo *Enviar apenas para*. Quando
terminar, clique no link *Adicionar* no bloco de detalhes da operação.

Isso é tudo que precisa ser configurado em uma ação simples de
notificação. Clique no botão *Adicionar* do formulário de ações.

[comment]: # ({/new-c1762601})

[comment]: # ({new-61999602})
#### Recebendo notificações

Agora que temos uma ação de envio de notificações configurada seria
interessante que consigamos receber uma. Para conseguir isso nós
poderíamos aumentar a carga de processamento no servidor monitorado para
que nossa [trigger](trigger#adding_trigger) seja acionada e nós
recebamos a notificação sobre o incidente.

Abra um shell (console) com o host monitorado e execute o comando a
seguir:

    cat /dev/urandom | md5sum

Você pode executar um ou mais [destes
processos](http://en.wikipedia.org/wiki/Md5sum).

Agora vá em *Monitoramento → Dados recentes* e veja que os valores
coletados pelo item 'CPU Load' foram aumentados. Lembre-se, para que a
trigger seja dispada o aumento de carga na CPU deverá ser maior que '2'
e deverá durar pelo menos 3 minutos. Uma vez que isso ocorra:

-   em *Monitoramento → Triggers* você poderá ver a trigger com o status
    'Incidente' piscando
-   você receberá uma notificação por e-mail

::: noteimportant
Se a notificação não funcionar:

-   verifique novamente tanto a configuração do tipo de mídia (E-mail)
    quanto as configurações da ação estão corretas
-   certifique-se que o usuário que você selecionou para receber a
    notificação tem, no mínimo, permissão para ler os dados do host que
    gerou o evento, conforme o definido no passo de *[criação do
    usuário](login#adding_user)*. O usuário deverá fazer parte do grupo
    de usuários 'Zabbix administrators' e o grupo de usuários deverá
    possuir, no mínimo, acesso de leitura ao grupo de hosts 'Linux
    servers', ao qual o host deverá pertencer.
-   Adicionalmente, você pode verificar o log de ações em *Relatórios →
    Log de ações*.


:::

[comment]: # ({/new-61999602})
