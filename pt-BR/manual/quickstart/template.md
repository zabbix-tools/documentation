[comment]: # translation:outdated

[comment]: # ({new-ffc9e78e})
# 6 Novo template

[comment]: # ({/new-ffc9e78e})

[comment]: # ({new-3bb3ba5d})
#### Visão geral

Nesta sessão você irá aprender como configurar um template.

Anteriormente você configurou um item, uma trigger e uma ação
(notificações) tendo com isso a capacidade de detecção e notificação de
incidentes.

Apesar de todos estes passos, por sí só, possibilitarem grande
flexibilidade pode ser algo extremamente trabalho de ser feito se
tivermos que repetir este passo para, digamos, 1.000 hosts. Alguma
automação aqui seria muito bem vinda.

É aqui que os templates demonstram a sua utilidade. Os templates
permitem agrupar os itens, triggers e outras entidades de forma
reutilizável e facilmente associável a hosts com um simples passo.

Quando um template é associado a um host, o host importa todas as
entidades constantes do template. Então, basicamente, temos um modelo de
monitoração rapidamente aplicável a um novo host.

Em outras ferramentas o template pode ser chamado por outros nomes, tal
qual: cartucho, feature, módulo, modelo, etc.

[comment]: # ({/new-3bb3ba5d})

[comment]: # ({new-f182767e})
#### Adicionando um template

Para começar a trabalhar com os templates nós começaremos criando um
assim como criamos o host usado nos testes anteriores. Para fazer isso
acesse *Configuração → Templates* clique em *Criar template*. Será
apresentado o formulário de configuração de templates.

![](../../../assets/en/manual/quickstart/new_template.png){width="550"}

Os parâmetros essenciais aqui são:

***Nome do template***

-   Informe o nome do template: **New template**. São aceitos caracteres
    alfanuméricos, espaços e sobrescritos.

***Grupos***

-   Selecione um ou vários grupos na caixa 'Outros grupos' (lado
    direito) e clique em **<<** para move-los para a caixa 'Nos
    grupos'. O template deverá pertencer a, no mínimo, um grupo (por
    conta das regras de permissionamento no Zabbix).

Ao finalizar clique no botão *Adicionar*. Seu novo template deverá estar
visível na lista de templates.

![](../../../assets/en/manual/quickstart/template_list.png){width="600"}

Como você pode perceber, temos um template mas nada dentro dele: nenhum
item, trigger ou outra entidade.

[comment]: # ({/new-f182767e})

[comment]: # ({new-ad341c27})
#### Adicionando um item ao template

Para adicionar um item ao template, vá a lista de itens do host 'New
host'. Em *Configuração → Hosts* clique em *Itens* da linha do host 'New
host' e:

-   selecione o item 'CPU Load' na lista de itens
-   clique no botão *Copiar* ao final da lista
-   selecione o template para o qual você deseja copiar o item 'New
    template'

![](../../../assets/en/manual/quickstart/copy_to_template.png)

-   clique em *Copiar*

Agora se você acessar *Configuração → Templates*, o template 'New
template' deverá ter um novo item cadastrado.

Neste momento vamos adicionar apenas o item, mas você poderá adaptar
este processo facilmente para copiar as demais entidades criadas no host
'New host' para o template 'New template'.

Nossa sugestão é que você prepare templates com um conjunto completo de
entidades correlatas às uma função (monitoração de S.O., monitoração de
uma aplicação, monitoração de um switch, etc).

[comment]: # ({/new-ad341c27})

[comment]: # ({new-230cf552})
#### Associando o template ao host

Com o template pronto só falta associarmos ele a um host. Para isso
acesse *Configuração → Hosts*, clique no host 'New host' para abrir o
formulário de propriedades e clique na aba **Templates**.

Agora, clique no botão *Selecionar* dentro do bloco *Vincular a novos
templates*. No pop-up apresentado clique no nome do template que
acabamos de criar ('New template'). Ao fazer isso o nome do template
aparecerá no campo *Vincular a novos templates*, clique no link
*Adicionar* (não no botão adicionar). O template deverá aparecer na
lista de templates disponível no bloco *Associado aos templates*.

![](../../../assets/en/manual/quickstart/link_template.png)

Clique no botão *Atualizar* no formulário de propriedades do host para
salvar as alterações. O template agora foi adicionado na configuração de
monitoração do host, todas as entidades dele constantes foram criadas ou
atualizadas no host.

Conforme você deve estar suspeitando, este template pode ser associado a
qualquer outro hoste e as alterações em suas entidades internas (itens,
tiggers, etc) serão automaticamente propagadas para os hosts vinculados
ao template.

[comment]: # ({/new-230cf552})

[comment]: # ({new-c7f1a5c4})
##### Associando templates pré-definidos aos hosts

Conforme você já deve ter percebido o Zabbix vem com um conjunto
pré-definido de templates para diversos sistemas operacionais,
dispositivos e aplicações. O objetivo deles é possibilitar um inicio
muito rápido de sua monitoração e você pode utiliza-los no processo
inicial de sua monitoração, entretanto, tenha em mente que estes modelos
devem ser aperfeiçoados para as necessidades do seu ambiente. É possível
que alguns deles tenham intervalos de coleta e níveis de alerta não
otimizados para um ambiente de produção.

Leia mais sobre [templates](/pt/manual/config/templates) aqui neste
manual.

[comment]: # ({/new-c7f1a5c4})
