[comment]: # translation:outdated

[comment]: # ({new-7905d51a})
# 2 Novo Host

[comment]: # ({/new-7905d51a})

[comment]: # ({new-b1060785})
#### Visão geral

Nesta sessão apresentaremos como configurar um novo host.

Um host no Zabbix é uma entidade na rede (física ou virtual) que você
deseja monitorar. A definição do que é um "host" é muito flexível. Pode
ser um servidor real, um switch, uma máquina virtual ou, até mesmo, uma
aplicação ou serviço.

[comment]: # ({/new-b1060785})

[comment]: # ({new-9c1b7280})
#### Adicionando um host

Informações sobre os hosts configurados estão disponíveis em
*Configuração → Hosts*. Durante a instalação do Zabbix foi cadastrado um
host chamado 'Zabbix server', mas vamos cadastrar outro.

Para adicionar um host, clique em *Criar host*. Será exibido um
formulário para definirmos as configurações do host.

![](../../../assets/en/manual/quickstart/new_host.png){width="600"}

Deverá ser informado no mínimo:

***Nome do Host***

-   Informe o nome do host. Caracteres alfanuméricos, espaços, pontos,
    traços e sublinhados são aceitos.

***Grupos***

-   Selecione um ou mais da lista de grupos apresentada na caixa 'Outros
    grupos' no lado direito da tela e clique no botão **<<** para
    move-lo(S) para a caixa 'Nos grupos'.

::: noteclassic
Todas as permissões de acesso são atribuídas por grupos de
hosts, não a hosts individuais. É por isso que é obrigatória a seleção
de um grupo no cadastro do host.
:::

***Endereço IP***

-   Informe o endereço IP do host. Observe que se for informado o
    endereço IP "real" (diferente de 127.0.0.1) do servidor, o endereço
    IP do Zabbix Server ou Zabbix Proxy deverá ser configurado no
    arquivo de configuração do Zabbix Agent a ser monitorado.

[Outras opções](/pt/manual/config/hosts/host#configuration) serão
mantidas inalteradas por enquanto.

Quando finalizar, clique no botão *Adicionar*. Seu novo host deverá
estar visível na lista de hosts.

::: notetip
Se o ícone *ZBX* na coluna de *Disponibilidade* estiver
vermelho, isso significa que está ocorrendo algum erro de comunicação
com o agente monitorado. Mova o cursor do mouse sobre o ícone para
visualizar a mensagem de erro. Se o ícone estiver cinza significará que
ainda não foi feita nenhuma tentativa de conexão, passados 60 segundos
com o ícone nesta cor recomenda-se que seja verificada a disponibilidade
do Zabbix Server e, em seguida, atualizar a página.
:::

[comment]: # ({/new-9c1b7280})
