[comment]: # aside: 2

[comment]: # translation:outdated

[comment]: # ({new-e6df368c})
# 1 Campos chave para descoberta de máquinas virtuais

A tabela a seguir lista os campos retornados em autobusca por máquinas
virtuais.

|**Chave do item**|<|<|
|-----------------|-|-|
|**Descrição**|**Campo**|**Conteúdo retornado**|
|vmware.cluster.discovery|<|<|
|Executa a descoberta de cluster.|{\#CLUSTER.ID}|Identificador do cluster.|
|^|{\#CLUSTER.NAME}|Nome do cluster.|
|vmware.hv.discovery|<|<|
|Executa a descoberta de hypervisor.|{\#HV.UUID}|Identificador único do hypervisor.|
|^|{\#HV.ID}|Identificador do Hypervisor (nome do objeto gerenciado 'HostSystem').|
|^|{\#HV.NAME}|Nome do hypervisor.|
|^|{\#CLUSTER.NAME}|Nome do cluster, pode retornar vazio.|
|vmware.hv.datastore.discovery|<|<|
|Executa a descoberta de 'datastore' no hypervisor. Observe que múltiplos hypervisores poderão ter o mesmo 'datastore'.|{\#DATASTORE}|Nome do 'datastore'.|
|vmware.vm.discovery|<|<|
|Executa a descoberta de máquinas virtuais.|{\#VM.UUID}|Identificador único da máquina virtual.|
|^|{\#VM.ID}|Identificador da máquina virtual (nome do objeto gerenciado 'VirtualMachine').|
|^|{\#VM.NAME}|Nome da máquina virtual.|
|^|{\#HV.NAME}|Nome do hypervisor.|
|^|{\#CLUSTER.NAME}|Nome do cluster, pode retornar vazio.|
|vmware.vm.net.if.discovery|<|<|
|Executa a descoberta de interfaces das máquinas virtuais.|{\#IFNAME}|Nome da interface de rede.|
|vmware.vm.vfs.dev.discovery|<|<|
|Executa a descoberta de discos das máquinas virtuais.|{\#DISKNAME}|Nome do dispositivo de disco.|
|vmware.vm.vfs.fs.discovery|<|<|
|Executa a descoberta de sistemas de arquivos das máquinas virtuais.|{\#FSNAME}|Nome do sistema de arquivos.|

[comment]: # ({/new-e6df368c})
