[comment]: # translation:outdated

[comment]: # ({new-f34132a6})
# 2 Cenário real de monitoração web

[comment]: # ({/new-f34132a6})

[comment]: # ({new-d43d02d4})
#### Visão geral

Esta sessão apresenta um passo a passo de um exemplo real de como a
monitoração web poderá ser utilizada.

Vamos utilizar o recurso de monitoração web para monitorar a interface
web do Zabbix. Nós precisamos saber se ela está disponível, se fornece o
conteúdo correto e se está com performance aceitável.

Para fazer isso nós precisaremos autenticar com nosso usuário e senha.

[comment]: # ({/new-d43d02d4})

[comment]: # ({new-51af514b})
#### Cenário

[comment]: # ({/new-51af514b})

[comment]: # ({new-6b72dd59})
##### Passo 1

Adicione um novo cenário web.

Nós adicionaremos um cenário para monitorar a interface web do Zabbix. O
cenário executará uma quantidade de passos.

Acesse *Configuração → Hosts*, selecione um host e clique no link *Web*
na linha do host. Na sequência clique no botão *Criar cenário web*.

![](../../../assets/en/manual/web_monitoring/new_scenario.png)

Este novo cenário web iremos chamar de *Zabbix frontend* e criar uma
nova aplicação para ele com o mesmo nome.

Observe que nós também poderemos utilizar macros, {user} e {password}.

[comment]: # ({/new-6b72dd59})

[comment]: # ({new-475cbb1a})
##### Passo 2

Defina os passos do cenário.

Clique no botão *Adicionar* na aba **Passos** para adicionar cada passo.

*Primeiro passo do cenário*

Nós iniciaremos a verificação testando se a primeira página está
respondendo corretamente. Ela deverá retornar o código HTTP 200 e conter
o texto "Zabbix SIA".

![](../../../assets/en/manual/web_monitoring/scenario_step1.png)

Quando concluir a configuração do passo, clique no botão *Adicionar* do
pop-up que foi aberto para cadastro do passo.

*Segundo passo do cenário*

O próximo passo é se autenticar na interface web do Zabbix, nós iremos
reutilizar as macros (variáveis) que definimos no nível do cenário:
{user} e {password}.

![](../../../assets/en/manual/web_monitoring/scenario_step2.png)

::: noteimportant
Observe que a interface web do Zabbix utiliza
redirecionamento através de códigos em JavaScript para redirecionar o
usuário autenticado e os próximos passos só podem ser verificados após
se autenticar. Adicionalmente, o passo de login precisa ter a URL
completa até o nome do arquivo PHP: **index.php**
:::

Todas as variáveis do post precisam ser informadas em uma única linha e
concatenadas através do símbolo **&**. Exemplo do texto para se
autenticar na interface web do Zabbix:

    name=Admin&password=zabbix&enter=Sign in

Se formos utilizar as macros neste exemplo, ficaria algo similar ao
texto a seguir:

    name={user}&password={password}&enter=Sign in

Observe que estaremos recebendo o conteúdo da variável `{sid}` (ID de
sessão), esta informação será necessária no passo 4.

*Terceiro passo do cenário*

Uma vez que estamos autenticados podemos finalmente verificar o que
desejamos. Para isso vamos verificar se o menu de administração está
visível.

![](../../../assets/en/manual/web_monitoring/scenario_step3.png)

*Quarto passo do cenário*

Até agora nós verificamos se a interface web está disponível, se
conseguimos os autenticar e se recebemos um conteúdo relevante, agora
precisamos nos desconectar da interface, de outra forma o banco de dados
do Zabbix irá acumular sessões de usuário que não serão mais utilizadas.

![](../../../assets/en/manual/web_monitoring/scenario_step4.png)

*Quinto passo do cenário*

Também podemos verificar se conseguimos concluir o processo de
desconexão ao buscar o texto **Username**.

![](../../../assets/en/manual/web_monitoring/scenario_step5.png)

*Configuração completa dos passos*

A configuração de todos os passos deverá estar similar à imagem a
seguir:

![](../../../assets/en/manual/web_monitoring/scenario_steps.png){width="600"}

[comment]: # ({/new-475cbb1a})

[comment]: # ({new-3e213338})
##### Passo 3

Salve o formulário para finalizar o cadastro do cenário web.

Você poderá consultar o resultado dos testes deste cenário acessando
*Monitoramento → Web*:

![](../../../assets/en/manual/web_monitoring/web_checks.png){width="600"}

Clique no nome do cenário desejado para ver estatísticas mais
detalhadas:

![](../../../assets/en/manual/web_monitoring/scenario_details.png){width="600"}

[comment]: # ({/new-3e213338})
