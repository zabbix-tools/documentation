[comment]: # translation:outdated

[comment]: # ({new-a3537eda})
# 1 Itens de monitoração web

[comment]: # ({/new-a3537eda})

[comment]: # ({new-7f39438f})
#### Visão geral

Alguns novos itens são automaticamente adicionados para a monitoração
dos cenário web definidos.

[comment]: # ({/new-7f39438f})

[comment]: # ({new-6bf9e7b2})
#### Itens de cenário

Logo que um cenário é criado, o Zabbix automaticamente adiciona itens
para guardar os dados coletados pelo cenário web e a aplicação
associada.

|Item|Descrição|
|----|-----------|
|*Velocidade de download do cenário <Cenário>*|Este item irá coletar informação sobre o a velocidade de download (bytes por segundo) do cenário, que é calculada baseada pela média obtida em todos os passos.<br>Chave do item: web.test.in\[<Cenário>,,bps\]<br>Tipo: *Numérico(fracionário)*|
|*Número do passo que falhou no cenário <Cenário>*|Este item irá informar o número do passo que falhou no último teste do cenário. Se todos os passos forem executados com sucesso, este item receberá o valor 0.<br>Chave do item: web.test.fail\[<Cenário>\]<br>Tipo: *Numérico (fracionário)*|
|*Última mensagem de erro no cenário <Cenário>*|Este item retorna a última mensagem de erro da última falha que ocorreu durante a execução de um teste do cenário.<br>Chave do item: web.test.error\[<Cenário>\]|

::: noteimportant
O nome real do cenário será utilizado em
substituição ao texto <Cenário>.
:::

::: noteclassic
Os itens de monitoração web são adicionados guardando o
histórico por 30 dias e as médias por 90 dias.
:::

::: noteclassic
Se o nome de um cenário contiver aspas, aspas duplas ou
colchetes eles deverão ser "escapados" na chave do item. Em outros casos
não será necessário escapar os caracteres.
:::

Estes itens podem ser utilizados na criação de triggers e definir
condições de notificação.

[comment]: # ({/new-6bf9e7b2})

[comment]: # ({new-38a780ad})
##### Exemplo 1

Para criar uma trigger alertando sobre a falha em um cenário web, você
pode definir uma expressão similar à expressão a seguir:

    {host:web.test.fail[Scenario].last()}<>0

Certifique-se de substituir o texto <Cenário> pelo nome real do
seu cenário.

[comment]: # ({/new-38a780ad})

[comment]: # ({new-1f198e97})
##### Exemplo 2

Para criar uma trigger alertando sobre lentidão na aplicação web, você
pode definir uma expressão similar à expressão a seguir::

    {host:web.test.in[Scenario,,bps].last()}<10000

Certifique-se de substituir o texto <Cenário> pelo nome real do
seu cenário.

[comment]: # ({/new-1f198e97})

[comment]: # ({new-abd173df})
#### Itens de passos do cenário

Logo que um passo do cenário é criado, o Zabbix automaticamente adiciona
itens para guardar os dados coletados pelo passo e a aplicação
associada.

|Item|Descrição|
|----|-----------|
|*Velocidade de download do passo <Passo> no cenário <Cenário>*|Este item coletará informação sobre o a velocidade de download (bytes por segundo) do passo.<br>Chave do item: web.test.in\[<Cenário>,Step,bps\]<br>Tipo: *Numérico(fracionário)*|
|*Tempo de resposta para o passo <Passo> no cenário <Cenário>*|Este item coletará informação sobre o tempo de resposta do passo em segundos. O tempo de resposta é calculado a parir do início da requisição até o momento em que toda a resposta é recebida.<br>Chave do item: web.test.time\[<Cenário>,Step,resp\]<br>Tipo: *Numérico (fracionário)*|
|*Código de resposta para o passo <Passo> no cenário <Cenário>*|Este item coletará os códigos de resposta recebidos para o teste do passo.<br>Chave do item: web.test.rspcode\[<Cenário>,Step\]<br>Tipo: *Numérico (fracionário)*|

O nome real do cenário e o nome do passo deverão ser utilizados no lugar
de <Cenário> e <Passo> respectivamente.

::: noteimportant
O nome real do cenário será utilizado em
substituição ao texto <Cenário>.
:::

::: noteclassic
Os itens dos passos de monitoração web são adicionados
guardando o histórico por 30 dias e as médias por 90 dias.
:::

Estes itens podem ser utilizados na criação de triggers e definir
condições de notificação. Por exemplo para criar um alerta informando
que o passo de login no Zabbix está lento a expressão da trigger poderia
ser algo similar à expressão a seguir:

    {zabbix:web.test.time[ZABBIX GUI,Login,resp].last()}>3

[comment]: # ({/new-abd173df})


[comment]: # ({new-fc2e76a2})
#### Scenario step items

As soon as a step is created, Zabbix automatically adds the following
items for monitoring.

|Item|Description|
|----|-----------|
|*Download speed for step <Step> of scenario <Scenario>*|This item will collect information about the download speed (bytes per second) of the step.<br>Item key: web.test.in\[Scenario,Step,bps\]<br>Type: *Numeric(float)*|
|*Response time for step <Step> of scenario <Scenario>*|This item will collect information about the response time of the step in seconds. Response time is counted from the beginning of the request until all information has been transferred.<br>Item key: web.test.time\[Scenario,Step,resp\]<br>Type: *Numeric(float)*|
|*Response code for step <Step> of scenario <Scenario>*|This item will collect response codes of the step.<br>Item key: web.test.rspcode\[Scenario,Step\]<br>Type: *Numeric(unsigned)*|

Actual scenario and step names will be used instead of "Scenario" and
"Step" respectively.

::: noteclassic
Web monitoring items are added with a 30 day history and a
90 day trend retention period.
:::

::: noteclassic
If scenario name starts with a doublequote or contains comma
or square bracket, it will be properly quoted in item keys. In other
cases no additional quoting will be performed.
:::

These items can be used to create triggers and define notification
conditions. For example, to create a "Zabbix GUI login is too slow"
trigger, you can define a trigger expression:

    last(/zabbix/web.test.time[ZABBIX GUI,Login,resp])>3

[comment]: # ({/new-fc2e76a2})
