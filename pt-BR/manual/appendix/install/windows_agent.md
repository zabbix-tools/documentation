[comment]: # translation:outdated

[comment]: # ({new-9884b1d2})
# 2 Zabbix Agent no Microsoft Windows

[comment]: # ({/new-9884b1d2})

[comment]: # ({new-bc82b50c})
#### Configurando o agente

O Zabbix Agent é executado como um serviço do Windows.

Você pode executar uma instância simples do Zabbix Agent ou múltiplas
instâncias em um host Windows. Uma instância simples pode utilizar o
arquivo de configuração em seu local padrão: `C:\zabbix_agentd.conf` ou
outra localização especificada na linha de comando. Para o caso de
múltiplas instâncias, cada instância do agente deverá ter seu próprio
arquivo de configuração (uma das instâncias pode usar o arquivo padrão).

Junto com o código fonte do Zabbix é fornecido um exemplo de arquivo de
configuração no diretório `conf/zabbix_agentd.win.conf`.

Consulte o manual de [configuração do agente no
windows](/pt/manual/appendix/config/zabbix_agentd_win) para opções e
detalhes de configuração neste sistema operacional.

[comment]: # ({/new-bc82b50c})

[comment]: # ({new-011f29f0})
##### Parâmetro Hostname

Para executar as verificações
[ativas](/pt/manual/appendix/items/activepassive#active_checks) em um
host o Zabbix Agent precisa ter seu 'hostname' definido. Além disso o
nome de host configurado no agente deverá ser exatamente igual ao nome
"[Nome de Host](/pt/manual/config/hosts/host)" configurado na interface
web do Zabbix.

O valor do 'hostname' no lado do agente pode ser definido através do
parâmetro **Hostname** ou **HostnameItem** em seu [arquivo de
configuração](/pt/manual/appendix/config/zabbix_agentd_win). Caso não
sejam definidos serão utilizados os valores padrões.

O valor padrão para o parâmetro **HostnameItem** é o valor retornado
pela chave "system.hostname" e na plataforma Windows ele irá retornar o
nome de host NetBIOS.

O valor padrão para o parâmetro **Hostname** é o valor retornado pelo
parâmetro **HostnameItem**. Logo, pra todos os efeitos, se os dois
parâmetros não forem definidos o nome atual de NetBIOS será utilizado; O
Zabbix Agent irá usar o nome de NetBIOS para solicitar a lista de
verificações ativas do Zabbix Server e para enviar os resultados.

::: noteimportant
A chave **system.hostname** sempre retornará o
nome de NetBIOS limitado a 15 caracteres e sempre em CAIXA ALTA -
independente do tamanho e dos caracteres minúsculos/maiúsculos no nome
real do host.
:::

A partir do Zabbix agent 1.8.6 em ambiente Windows a chave
"system.hostname" suporta um parâmetro opcional - *type* (tipo do nome).
O valor padrão para este parâmetro é "netbios" (para compatibilidade
retroativa) o outro valor possível é "host".

::: noteimportant
A chave **system.hostname\[host\]** sempre
retornará o o nome completo e real (sensível ao caso) do servidor
Windows.
:::

Logo, para simplificar a configuração do arquivo `zabbix_agentd.conf` e
unifica-lo, duas abordagens diferentes podem ser utilizadas.

1.  Deixe os parâmetros **Hostname** ou **HostnameItem** indefinidos
    para que o Zabbix Agent use o nome de NetBIOS como 'hostname';
2.  Deixe o parâmetro **Hostname** indefinido e defina o parâmetro
    **HostnameItem** de forma similar a esta:\
    **HostnameItem=system.hostname\[host\]**\
    e o Zabbix Agent irá utilizar o FQDN do servidor windows.

O nome de host também pode ser utilizado como parte do nome do serviço
Windows e será utilizado para instalar, iniciar, parar e desinstalar o
serviço. Por exemplo, se o arquivo de configuração define
`Hostname=Windows_db_server`, então o agente será instalado como o
serviço Windows "`Zabbix Agent [Windows_db_server]`". Logo, para ter
nomes de serviço diferentes para cada instância do Zabbix Agent, cada
instância deverá ter um nome de host diferente.

[comment]: # ({/new-011f29f0})

[comment]: # ({new-1c178c2a})
#### Instalando o agente como um serviço Windows

Para instalar uma instância simples do agente usando o arquivo padrão de
configuração `c:\zabbix_agentd.conf`:

    zabbix_agentd.exe --install

::: noteimportant
Em ambientes de 64-bit, uma versão de 64-bit do
Zabbix Agent será necessária para que todas as verificações relacionadas
a proesssos em 64-bits funcione adequadamente.
:::

Se você quiser utilizar outro arquivo de configuração que não o padrão
(`c:\zabbix_agentd.conf`), você poderá definir a sua localização durante
a instalação do serviço:

    zabbix_agentd.exe --config <your_configuration_file> --install

O caminho completo para o arquivo de configuração deverá ser fornecido.

Múltiplas instâncias do Zabbix Agent podem ser instaladas como serviços
conforme exemplo abaixo:

      zabbix_agentd.exe --config <configuration_file_for_instance_1> --install --multiple-agents
      zabbix_agentd.exe --config <configuration_file_for_instance_2> --install --multiple-agents
      ...
      zabbix_agentd.exe --config <configuration_file_for_instance_N> --install --multiple-agents

Os serviços instalados deverão estar visíveis através do panel de
controle.

[comment]: # ({/new-1c178c2a})

[comment]: # ({new-635edc59})
#### Iniciando o agente

Para iniciar o serviço do agente você pode usar o painel de controle ou
faze-lo a partir da linha de comando.

Iniciar uma instância simples do Zabbix Agent com arquivo padrão de
configuração:

     zabbix_agentd.exe --start

Iniciar uma instância simples do Zabbix Agent com arquivo de
configuração específico:

     zabbix_agentd.exe --config <your_configuration_file> --start

Iniciar múltiplas instâncias do Zabbix Agent com arquivo de configuração
específicos:

     zabbix_agentd.exe --config <configuration_file_for_this_instance> --start --multiple-agents

[comment]: # ({/new-635edc59})

[comment]: # ({new-f3f0d3db})
#### Parando o agente

Para parar o serviço do agente você pode usar o painel de controle ou
faze-lo a partir da linha de comando.

Parar uma instância simples do Zabbix Agent com arquivo padrão de
configuração:

     zabbix_agentd.exe --stop

Parar uma instância simples do Zabbix Agent com arquivo de configuração
específico:

     zabbix_agentd.exe --config <your_configuration_file> --stop

Parar múltiplas instâncias do Zabbix Agent com arquivo de configuração
específicos:

     zabbix_agentd.exe --config <configuration_file_for_this_instance> --stop --multiple-agents

[comment]: # ({/new-f3f0d3db})

[comment]: # ({new-db2e6708})
#### Desinstalar o serviço do agente

Para desinstalar uma instância simples do Zabbix Agent com arquivo
padrão de configuração:

     zabbix_agentd.exe --uninstall

Para desinstalar uma instância simples do Zabbix Agent com arquivo de
configuração específico:

     zabbix_agentd.exe --config <your_configuration_file> --uninstall

Para desinstalar múltiplas instâncias do Zabbix Agent com arquivo de
configuração específicos:

     zabbix_agentd.exe --config <configuration_file_for_instance_1> --uninstall --multiple-agents
     zabbix_agentd.exe --config <configuration_file_for_instance_2> --uninstall --multiple-agents
     ...
     zabbix_agentd.exe --config <configuration_file_for_instance_N> --uninstall --multiple-agents

[comment]: # ({/new-db2e6708})

[comment]: # ({new-b7b226d1})

#### Limitations

Zabbix agent for Windows does not support
non-standard Windows configurations where CPUs are distributed
non-uniformly across NUMA nodes. If logical CPUs are distributed
non-uniformly, then CPU performance metrics may not be available for
some CPUs. For example, if there are 72 logical CPUs with 2 NUMA nodes,
both nodes must have 36 CPUs each.

[comment]: # ({/new-b7b226d1})
