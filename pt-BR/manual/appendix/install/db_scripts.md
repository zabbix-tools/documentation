[comment]: # translation:outdated

[comment]: # ({be05df6d-7af1bbaa})
# 1 Criação do banco de dados

[comment]: # ({/be05df6d-7af1bbaa})

[comment]: # ({9fb6a725-fab90562})
#### Visão geral

Um banco de dados Zabbix deve ser criado durante a instalação
do Zabbix Server ou Proxy.

Esta seção fornece instruções para criação de um banco de dados Zabbix.
Um conjunto separado de instruções está disponível para cada
banco de dados suportado.

O padrão UTF-8 é o único suportado pelo Zabbix. É sabido trabalhar sem
quaisquer falhas de segurança. Os usuário devem estar cientes de que há
problemas de segurança conhecidos ao usar outros padrões de codificação.

[comment]: # ({/9fb6a725-fab90562})

[comment]: # ({d2c63663-fad527fc})

::: noteclassic
Se instalando a partir do [repositório Zabbix no Git](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse), você precisa executar:

`$ make dbschema` 

antes de prosseguir para os próximos passos. 
:::

[comment]: # ({/d2c63663-fad527fc})

[comment]: # ({11a1285d-1e36e539})
#### MySQL

Configurações com utf8 (aka utf8mb3) e utf8mb4 são suportadas (com collation
utf8\_bin and utf8mb4\_bin respectivamente) para Zabbix Server/Proxy trabalharem
adequadamente com banco de dados MySQL. É recomendado utilizar 
utf8mb4 para novas instalações.

    shell> mysql -uroot -p<password>
    mysql> create database zabbix character set utf8mb4 collate utf8mb4_bin;
    mysql> create user 'zabbix'@'localhost' identified by '<password>';
    mysql> grant all privileges on zabbix.* to 'zabbix'@'localhost';
    mysql> quit;

::: notewarning
Se você estiver instalando a partir dos **pacotes** do Zabbix, pare 
aqui e continue com as instruções para [RHEL/CentOS](https://www.zabbix.com/download?zabbix=5.0&os_distribution=red_hat_enterprise_linux&os_version=8&db=mysql) ou
[Debian/Ubuntu](https://www.zabbix.com/download?zabbix=5.0&os_distribution=debian&os_version=10_buster&db=mysql) para importar os dados para dentro do banco.
:::

Se você está instalando o Zabbix a partir dos fontes, continue para importar 
os dados para dentro do banco. Para um banco de dados do Zabbix Proxy, 
apenas o `schema.sql` deve ser importado (sem images.sql nem data.sql):

    shell> cd database/mysql
    shell> mysql -uzabbix -p<password> zabbix < schema.sql
    # stop here if you are creating database for Zabbix proxy
    shell> mysql -uzabbix -p<password> zabbix < images.sql
    shell> mysql -uzabbix -p<password> zabbix < data.sql

[comment]: # ({/11a1285d-1e36e539})

[comment]: # ({e470c975-61d6043c})
#### PostgreSQL

Você precisa ter um usuário do banco de dados com permissão de criação
de objetos de banco. O seguinte comando shell criará o usuário `zabbix`. 
Especifique uma senha quando solicitado e repita a senha (note, primeiro
deve ser solicitado que você informe a senha de `sudo`):

    shell> sudo -u postgres createuser --pwprompt zabbix

Agora nós vamos configurar o banco de dados `zabbix` (último parâmetro) com o
usuário criado anteriormente como proprietário (owner) (`-O zabbix`).

    shell> sudo -u postgres createdb -O zabbix -E Unicode -T template0 zabbix

::: notewarning
Se você estiver instalando  a partir dos **pacotes** Zabbix, pare
aqui e continue com as instruções para [RHEL/CentOS](https://www.zabbix.com/download?zabbix=5.0&os_distribution=red_hat_enterprise_linux&os_version=8&db=postgresql) ou
[Debian/Ubuntu](https://www.zabbix.com/download?zabbix=5.0&os_distribution=debian&os_version=10_buster&db=postgresql) para importar o schema e os dados iniciais 
para dentro do banco de dados.
:::

Se você está instalando o Zabbix a partir dos fontes, continue para importar 
o schema e os dados iniciais (assumindo que você esteja na raíz do diretório
de fontes do Zabbix). Para um banco de dados do Zabbix Proxy, apenas o
`schema.sql` deve ser importado (sem images.sql nem data.sql).

    shell> cd database/postgresql
    shell> cat schema.sql | sudo -u zabbix psql zabbix
    # stop here if you are creating database for Zabbix proxy
    shell> cat images.sql | sudo -u zabbix psql zabbix
    shell> cat data.sql | sudo -u zabbix psql zabbix

::: noteimportant
Os comandos acima são fornecidos como exemplos que funcionarão na maioria 
das instalações em GNU/Linux. Você pode usar diferentes comandos, p.e. "psql -U <username>" 
dependendo de como seu sistema/banco de dados está configurado. Se você tiver
dificuldades ao configurar o banco de dados, por favor consulte seu administrador de 
banco de dados. 
:::

[comment]: # ({/e470c975-61d6043c})

[comment]: # ({518334b1-cc68ca58})
#### TimescaleDB

Instruções para criação e configuração do TimescaleDB são fornecidas em uma [seção](/manual/appendix/install/timescaledb) separada.

[comment]: # ({/518334b1-cc68ca58})

[comment]: # ({f16eb475-7b4d56a7})
#### Oracle

Instruções para criação e configuração de banco de dados Oracle são fornecidas em uma [seção](/manual/appendix/install/oracle) separada.

[comment]: # ({/f16eb475-7b4d56a7})

[comment]: # ({2881898c-02d49e4f})
#### SQLite

O uso de SQLite é suportado apenas para **Zabbix Proxy**!

::: noteclassic
Se usando SQLite com Zabbix Proxy, o banco de dados será
automaticamente criado caso não exista.
:::

    shell> cd database/sqlite3
    shell> sqlite3 /var/lib/sqlite/zabbix.db < schema.sql

Retorne para a [seção de instalação](/manual/installation/install).

[comment]: # ({/2881898c-02d49e4f})
