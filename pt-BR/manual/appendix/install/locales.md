[comment]: # translation:outdated

[comment]: # ({new-2dc59dbb})
# 13 Additional frontend languages

[comment]: # ({/new-2dc59dbb})

[comment]: # ({new-50c452f4})
#### Overview

In order to use any other language than English in Zabbix web interface,
its locale should be installed on the web server. Additionally, the PHP
gettext extension is required for the translations to work.

[comment]: # ({/new-50c452f4})

[comment]: # ({new-bb6c0b89})
#### Installing locales

To list all installed languages, run:

    locale -a

If some languages that are needed are not listed, open the
*/etc/locale.gen* file and uncomment the required locales. Since Zabbix
uses UTF-8 encoding, you need to select locales with UTF-8 charset.

Now, run:

    locale-gen 

Restart the web server.

The locales should now be installed. It may be required to reload Zabbix
frontend page in browser using Ctrl + F5 for new languages to appear.

[comment]: # ({/new-bb6c0b89})

[comment]: # ({new-b0831966})
#### Installing Zabbix

If installing Zabbix directly from [Zabbix git
repository](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse),
translation files should be generated manually. To generate translation
files, run:

    make gettext
    locale/make_mo.sh

This step is not needed when installing Zabbix from packages or source
tar.gz files.

[comment]: # ({/new-b0831966})

[comment]: # ({b3b7ff83-5dccdfbc})

#### Selecionando um idioma

Existem várias maneiras de selecionar um idioma na interface web do Zabbix:

- Ao instalar a interface web - no frontend [assistente de instalação
     ](/manual/installation/frontend#welcome_screen). Selecionado
     idioma será definido como padrão do sistema.
- Após a instalação, o idioma padrão do sistema pode ser alterado em
     o *Administração→Geral→GUI* [menu
     seção](/manual/web_interface/frontend_sections/administration/general#gui).
- O idioma de um usuário específico pode ser alterado no [
     profil de usuário](/manual/web_interface/user_profile#user_profile).

Se uma localidade para um idioma não estiver instalada na máquina, este
idioma ficará acinzentado no seletor de idioma do Zabbix. Um ícone vermelho é
exibido ao lado do seletor de idioma se pelo menos uma nacionalidade for
ausente. Ao pressionar este ícone a seguinte mensagem será
exibido: "Você não pode escolher alguns dos idiomas, porque
a nacionalidade para eles não estão instalados no servidor web."

![locale\_warning.png](../../../../assets/en/manual/appendix/install/locale_warning.png){width="600"}

[comment]: # ({/b3b7ff83-5dccdfbc})
