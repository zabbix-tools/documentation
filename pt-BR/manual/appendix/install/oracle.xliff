<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="pt-BR" datatype="plaintext" original="manual/appendix/install/oracle.md">
    <body>
      <trans-unit id="710cea74" xml:space="preserve">
        <source># 13 Oracle database setup</source>
      </trans-unit>
      <trans-unit id="c3411fd2" xml:space="preserve">
        <source>#### Overview

This section contains instructions for creating Oracle database and
configuring connections between the database and Zabbix server, proxy,
and frontend.</source>
      </trans-unit>
      <trans-unit id="2fd36a67" xml:space="preserve">
        <source>### Database creation

We assume that a *zabbix* database user with *password* password exists
and has permissions to create database objects in ORCL service located
on the *host* Oracle database server. Zabbix requires a Unicode database
character set and a `UTF8` national character set. Check current
settings:

    sqlplus&gt; select parameter,value from v$nls_parameters where parameter='NLS_CHARACTERSET' or parameter='NLS_NCHAR_CHARACTERSET';

Now prepare the database:

    cd /path/to/zabbix-sources/database/oracle
    sqlplus zabbix/password@oracle_host/ORCL
    sqlplus&gt; @schema.sql
    # stop here if you are creating database for Zabbix proxy
    sqlplus&gt; @images.sql
    sqlplus&gt; @data.sql

::: notetip
Please set the initialization parameter
CURSOR\_SHARING=FORCE for best performance.
:::</source>
      </trans-unit>
      <trans-unit id="db4ecc98" xml:space="preserve">
        <source>### Connection set up

Zabbix supports two types of connect identifiers (connection methods):

-   Easy Connect
-   Net Service Name

Connection configuration parameters for Zabbix server and Zabbix proxy
can be set in the configuration files. Important parameters for the
server and proxy are *DBHost*, *DBUser*, *DBName* and *DBPassword*. The
same parameters are important for the frontend: *$DB\["SERVER"\]*,
*$DB\["PORT"\]*, *$DB\["DATABASE"\]*, *$DB\["USER"\]*,
*$DB\["PASSWORD"\]*.

Zabbix uses the following connection string syntax:

    {DBUser/DBPassword[@&lt;connect_identifier&gt;]} 

&lt;connect\_identifier&gt; can be specified either in the form of "Net
Service Name" or "Easy Connect".

    @[[//]Host[:Port]/&lt;service_name&gt; | &lt;net_service_name&gt;]</source>
      </trans-unit>
      <trans-unit id="ed80a373" xml:space="preserve">
        <source>#### Easy Connect

Easy Connect uses the following parameters to connect to the database:

-   *Host* - the host name or IP address of the database server computer
    (DBHost parameter in the configuration file).
-   *Port* - the listening port on the database server (DBPort parameter
    in the configuration file; if not set the default 1521 port will be
    used).
-   &lt;service\_name&gt; - the service name of the database you want to
    access (DBName parameter in the configuration file).

**Example:**

Database parameters set in the server or proxy configuration file
(zabbix\_server.conf and zabbix\_proxy.conf):

    DBHost=localhost
    DBPort=1521
    DBUser=myusername
    DBName=ORCL
    DBPassword=mypassword

Connection string used by Zabbix to establish connection:

    DBUser/DBPassword@DBHost:DBPort/DBName

During Zabbix frontend installation, set the corresponding parameters in
the *Configure DB connection* step of the setup wizard:

-   Database host: localhost
-   Database port: 1521
-   Database name: ORCL
-   User: myusername
-   Password: mypassword

![](../../../../assets/en/manual/appendix/install/oracle_easy_connect.png){width="600"}

Alternatively, these parameters can be set in the frontend configuration
file (zabbix.conf.php):

    $DB["TYPE"]                     = 'ORACLE';
    $DB["SERVER"]                   = 'localhost';
    $DB["PORT"]             = '1521';
    $DB["DATABASE"]                 = 'ORCL';
    $DB["USER"]                     = 'myusername';
    $DB["PASSWORD"]                 = 'mypassword';</source>
      </trans-unit>
      <trans-unit id="fe8fa72a" xml:space="preserve">
        <source>#### Net service name

Since Zabbix 5.4.0 it is possible to connect to Oracle by using net
service name.

&lt;net\_service\_name&gt; is a simple name for a service that resolves
to a connect descriptor.

In order to use the service name for creating a connection, this service
name has to be defined in the tnsnames.ora file located on both the
database server and the client systems. The easiest way to make sure
that the connection will succeed is to define the location of
tnsnames.ora file in the TNS\_ADMIN environment variable. The default
location of the tnsnames.ora file is:

    $ORACLE_HOME/network/admin/

A simple tnsnames.ora file example:

    ORCL =
      (DESCRIPTION =
        (ADDRESS = (PROTOCOL = TCP)(HOST = localhost)(PORT = 1521))
        (CONNECT_DATA =
          (SERVER = DEDICATED)
          (SERVICE_NAME = ORCL)
        )
      )

To set configuration parameters for the "Net Service Name" connection
method, use one of the following options:

-    Set an empty parameter DBHost and set DBName as usual:

```{=html}
&lt;!-- --&gt;
```
    DBHost=
    DBName=ORCL

-   Set both parameters and leave both empty:

```{=html}
&lt;!-- --&gt;
```
    DBHost=
    DBName=

In the second case, the TWO\_TASK environment variable has to be set. It
specifies the default remote Oracle service (service name). When this
variable is defined, the connector connects to the specified database by
using an Oracle listener that accepts connection requests. This variable
is for use on Linux and UNIX only. Use the LOCAL environment variable
for Microsoft Windows.

**Example:**

Connect to a database using Net Service Name set as ORCL and the default
port. Database parameters set in the server or proxy configuration file
(zabbix\_server.conf and zabbix\_proxy.conf):

    DBHost=
    #DBPort=
    DBUser=myusername
    DBName=ORCL
    DBPassword=mypassword

During Zabbix frontend installation, set the corresponding parameters in
the *Configure DB connection* step of the setup wizard:

-   Database host:
-   Database port: 0
-   Database name: ORCL
-   User: myusername
-   Password: mypassword

![](../../../../assets/en/manual/appendix/install/oracle_service_name.png){width="600"}

Alternatively, these parameters can be set in the frontend configuration
file (zabbix.conf.php):

    $DB["TYPE"]                     = 'ORACLE';
    $DB["SERVER"]                   = '';
    $DB["PORT"]           = '0';
    $DB["DATABASE"]                 = 'ORCL';
    $DB["USER"]                     = 'myusername';
    $DB["PASSWORD"]                 = 'mypassword';

Connection string used by Zabbix to establish connection:

    DBUser/DBPassword@ORCL</source>
      </trans-unit>
      <trans-unit id="d6cd676f" xml:space="preserve">
        <source>#### Known issues

To improve performance, you can convert the field types from *nclob* to *nvarchar2*, see [known issues](/manual/installation/known_issues#slow-configuration-sync-with-oracle).</source>
      </trans-unit>
    </body>
  </file>
</xliff>
