[comment]: # translation:outdated

[comment]: # ({new-e723903a})
# 7 Definindo períodos de tempo

[comment]: # ({/new-e723903a})

[comment]: # ({new-c717cede})
#### - Formato

Para definir um período de tempo o seguinte formato deve ser utilizado:

    d-d,hh:mm-hh:mm

Você pode definir vários períodos ao utilizar o separador ";":

    d-d,hh:mm-hh:mm;d-d,hh:mm-hh:mm...

[comment]: # ({/new-c717cede})

[comment]: # ({new-a352b97d})
#### - Descrição

|Símbolo|Descrição|
|--------|-----------|
|**d**|Dia da semana: 1 - Segunda, 2 - Terça ,... , 7 - Domingo|
|**hh**|Horas: 00-24|
|**mm**|Minutos: 00-59|

#### - Padrão

Deixar um período de tempo vazio é o mesmo que definir
`01-07,00:00-24:00`, que é o valor padrão.

::: noteimportant
O limite de um período de tempo não é incluído.
Assim, se você definir `09:00-18:00` o último momento considerado no
período de tempo será `17:59:59`. É assim que é tratado desde o Zabbix
1.8.7, e sempre foi tradado desta forma nos [horários de
trabalho](/pt/manual/web_interface/frontend_sections/administration/general#working_time).
:::

#### - Exemplos

Horário de trabalho. Segunda - Sexta das 9:00 às 18:00:

    1-5,09:00-18:00

Horário de trabalho com final de semana. Segunda - Sexta das 9:00 às
18:00 e sábados e domingos das 10:00 às 16:00:

    1-5,09:00-18:00;6-7,10:00-16:00

[comment]: # ({/new-a352b97d})
