[comment]: # translation:outdated

[comment]: # ({new-e032d93c})
# 1 Perguntas frequentes / Solução de problemas

Questões frequentes ou FAQ.

1.  Q: Eu posso descarregar/limpar a fila (no menu *Administração →
    Fila*)?\
    A: Não.
2.  Q: Como eu posso migrar de um banco de dados para outro?\
    A: Através de 'Dump' do banco apenas (para o MySQL, utilize a 'flag'
    -t ou --no-create-info), crie um novo esquema de banco de dados para
    o Zabbix e importe os dados.
3.  Q: Eu gostaria de substituir todos os espaços por sublinhados nas
    chaves do meu item, eles funcionavam em versões anteriores do Zabbix
    mas não são mais um caracter válido desde o Zabbix 1.8 (ou qualquer
    outro motivo para atualização em massa de chaves de item). Como eu
    posso fazer isso?\
    A: Você pode utilizar comandos de banco de dados para substituir os
    caracteres de espaço por sublinhados:\
    update items set key\_=replace(key\_,' ','\_');\
    As triggers não estarão aptas a utilizar estes itens sem
    modificações adicionais, e você terá que modificar manualmente
    também as referências nos seguintes módulos:\
    \* Notificações (ações)\
    \* Elementos de mapas e rótulos de link\
    \* Fórmulas de itens calculados
4.  Q: Meus gráficos tem pontos ao invés de linhas ou possuem áreas em
    branco. Por que isso ocorre?\
    A: Estão faltando dados. Isso pode ocorrer por causa de vários
    motivos - problemas de performance no Zabbix, no banco de dados, na
    rede, nos dispositivos monitorados, zoom inapropriado para o período
    de coleta...
5.  Q: Os 'daemons' do Zabbix não iniciam com a mensagem *Listener
    failed with error: socket() for \[\[-\]:10050\] failed with error
    22: Invalid argument.*\
    A: Este erro ocorre quando você tenta executar um agente compilado
    para o kernel 2.6.27 ou superior em um ambiente com kernel 2.6.26 ou
    inferior. Obseve que links estáticos não irão ajudar neste caso pois
    a chamada de sistema 'socket()' não suporta a 'flag' SOCK\_CLOEXEC
    nas versões mais antigas do kernel.
    [ZBX-3395](https://support.zabbix.com/browse/ZBX-3395)
6.  Q: Eu tentei criar um parâmetro de usuário flexivel (um que aceite
    parâmetros) com um comando que utiliza parâmetros posicionais (como
    o $1), mas não funcionou (utilizou o parâmetro do item no lugar).
    Como resolver isso?\
    A: Duplique o símbolo de dólar: **$$1**
7.  Q: Todas as caixas de seleção tem uma barra de rolagem feia no
    Opera 11. Por que isso ocorre?\
    A: É um bug conhecido nas versões 11.00 e 11.01 do ópera; consulte
    [o registro de bug](https://support.zabbix.com/browse/ZBX-3594) para
    mais detalhes.
8.  Q: Como eu posso mudar a cor de fundo de um gráfico em um tema
    customizado?\
    A: Veja o manual de [temas](/pt/manual/web_interface/theming) na
    parte sobre a tabela `graph_theme`.
9.  Q: Meu nível de debug (DebugLevel) é '4' e eu estou vendo mensagens
    "Trapper got \[\] len 0" no log do servidor e do proxy - Por quê
    isso ocorre?\
    A: Provavelmente porque a interface web está tentando se conectar
    com o servidor.
10. Q: Meu sistema está com horário adiantado e nenhum dado está
    chegando, como resolver isso?\
    A: Limpe os dados do banco em hosts.disable\_until\*,
    drules.nextcheck, httptest.nextcheck e reinicie o Servidor/Proxy.
11. Q: Valores de texto nos items na interface web (quando usando a
    macro *{ITEM.VALUE}* e em alguns outros casos) são cortados após o
    vigésimo caractere. Isso é normal?\
    A: Sim, é um limite fixo no arquivo include/items.inc.php
    atualmente.

#### Problemas de instalação

Consulte o manual de [problemas específicos de
instalação](/pt/manual/appendix/install/troubleshooting).

#### See also

\* [Troubleshooting page on
zabbix.org](https://www.zabbix.org/wiki/Troubleshooting)

[comment]: # ({/new-e032d93c})
