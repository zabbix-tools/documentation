[comment]: # attributes: notoc

[comment]: # translation:outdated

[comment]: # ({new-b4814cf1})
# 7 Zabbix agent 2 plugins

[comment]: # ({/new-b4814cf1})

[comment]: # ({new-32e5ff52})
#### Overview

This section contains descriptions of configuration file parameters for
Zabbix agent 2 plugins. Please use the sidebar to access information
about the specific plugin.

[comment]: # ({/new-32e5ff52})
