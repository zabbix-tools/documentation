[comment]: # translation:outdated

[comment]: # ({new-b03b9977})
# - \#7 Observações sobre o parâmetro "Include"

Se o parâmetro `Include` for utilizado para incluir um arquivo, o
usuário 'zabbix' deverá ter permissão para lê-lo.

Se um parâmetro `Include` for utilizado para incluir um diretório:

      - O usuário 'zabbix' deverá ter permissão para ler cada arquivo incluído.
      - Não tem uma ordem específica de inclusão dos arquivos. (Ex. os arquivos não serão incluídos em ordem alfabética). E não é possível definir em um ''Include'' a inclusão de vários arquivos individuais.
      - Todos os arquivos do diretório serão incluídos na configuração.
      - Cuidado com backups de arquivos criados automaticamente por editores de texto. Por exemplo, se você estiver editando o arquivo ''include/my_specific.conf'' e o editor gerar um arquivo de backup chamado ''include/my_specific_conf.BAK'' os dois arquivos serão adicionados. Mova o arquivo antigo ''include/my_specific.conf.BAK'' para fora do diretório a ser incluído, ou use máscara para inclusão de arquivos. No  Linux, o conteúdo dos dos diretórios a serem incluídos podem ser verificados através do comando ''ls -al''.

Se um parâmetro `Include` for utilizado para adicionar arquivos com um
padrão:

      - O usuário 'zabbix' deverá ter permissão para ler cada arquivo incluído.
      - Não tem uma ordem específica de inclusão dos arquivos. (Ex. os arquivos não serão incluídos em ordem alfabética). E não é possível definir em um ''Include'' a inclusão de vários arquivos individuais.

[comment]: # ({/new-b03b9977})



[comment]: # ({new-15833d02})
#### Overview

Additional files or directories can be included into server/proxy/agent
configuration using the `Include` parameter.

[comment]: # ({/new-15833d02})

[comment]: # ({new-c13f85cf})
#### Notes on inclusion

If the `Include` parameter is used for including a file, the file must
be readable.

If the `Include` parameter is used for including a directory:

      - All files in the directory must be readable.
      - No particular order of inclusion should be assumed (e.g. files are not included in alphabetical order). Therefore do not define one parameter in several ''Include'' files (e.g. to override a general setting with a specific one).
      - All files in the directory are included into configuration.
      - Beware of file backup copies automatically created by some text editors. For example, if editing the ''include/my_specific.conf'' file produces a backup copy ''include/my_specific_conf.BAK'' then both files will be included. Move ''include/my_specific.conf.BAK'' out of the "Include" directory. On Linux, contents of the ''Include'' directory can be checked with a ''ls -al'' command for unnecessary files.

If the `Include` parameter is used for including files using a pattern:

      - All files matching the pattern must be readable.
      - No particular order of inclusion should be assumed (e.g. files are not included in alphabetical order). Therefore do not define one parameter in several ''Include'' files (e.g. to override a general setting with a specific one).

[comment]: # ({/new-c13f85cf})
