[comment]: # attributes: notoc

[comment]: # translation:outdated

[comment]: # ({new-dbb2a1f5})
# - \#1 Zabbix Server

::: noteclassic
Os valores padrões aqui apresentados são os que são
codificados nos daemons do Zabbix, o valor em seus arquivos de
configuração padrões poderá divergir dos aqui apresentados.
:::

A lista de parâmetros suportados no arquivo de configuração do Zabbix
Server é apresentada na tabela a seguir:

|Parâmetro|Obrigatório|Range|Padrão|Descrição|
|----------|------------|-----|-------|-----------|
|AlertScriptsPath|não|<|/usr/local/share/zabbix/alertscripts|Localização dos scripts de alerta customizados (pode veriar em função da variável de compilação *datadir*).|
|AllowRoot|não|<|0|Permite que o Zabbix Server seja executado como 'root'. Se estiver desabilitado e o servidor for iniciado pelo 'root', o servidor irá tentar mudar o seu permissionamento de execução para o usuário 'zabbix'. Não tem efeito se o processo for iniciado por um usuário normal.<br>0 - não permitir<br>1 - permitir<br>Este parâmetro é suportado desde o Zabbix 2.2.0.|
|CacheSize|não|128K-8G|8M|Size of configuration cache, in bytes.<br>Shared memory size for storing host, item and trigger data.<br>Upper limit used to be 2GB before Zabbix 2.2.3.|
|CacheUpdateFrequency|não|1-3600|60|O intervalo entre atualizações do cache de dados de configuração, em segundos.|
|DBHost|não|<|localhost|Nome do host que hospeda o DB.<br>Para MySQL caso esteja como localhost ou vazio será utilizado o 'socket'. Para PostgreSQL apenas estando vazio será utilizado o 'socket'.|
|DBName|sim|<|<|Nombe do BD.<br>PAra SQLite3 o caminho para o arquivo de dados e o *DBUser* e *DBPassword* serão ignorados.|
|DBPassword|não|<|<|Senha do BD. Ignorado para SQLite.<br>Comente esta linha se não for utilizada uma senha.|
|DBPort|não|1024-65535|3306|Porta do banco de dados, quando não estiver utilizando o socket local. Ignorado para SQLite.|
|DBSchema|não|<|<|Nome do esquema de BD. Utilizado para IBM DB2 e PostgreSQL.|
|DBSocket|não|<|/tmp/mysql.sock|Caminho para o socket do MySQL.|
|DBUser|não|<|<|Usuário do BD. Ignorado para SQLite.|
|DebugLevel|não|0-5|3|Especifica o nível de debug:<br>0 - informação básica sobre carga e finalização dos processos do Zabbix<br>1 - informações críticas<br>2 - informações de erro<br>3 - avisos<br>4 - para depuração (produz muitas informações)<br>5 - para debugação extendida (produz realmente muita informação)|
|ExternalScripts|não|<|/usr/local/share/zabbix/externalscripts|Localização dos scripts externos (depende da variável de tempo de compilação *datadir*).|
|Fping6Location|não|<|/usr/sbin/fping6|Localização do fping6.<br>Certifique-se que o binário do fping6 pertence ao 'root' e tem a flag de 'SUID' definida!<br>Deixe vazio ("Fping6Location=") se o seu utilitário fping for capaz de processar endereços IPv6.|
|FpingLocation|não|<|/usr/sbin/fping|Localização do fping.<br>Certifique-se que o binário do fping6 pertence ao 'root' e tem a flag de 'SUID' definida!|
|HistoryCacheSize|não|128K-2G|8M|Tamanho do cache de histórico, em bytes.<br>Tamanho de memória compartilhada para armazenamento de dados históricos.|
|HistoryTextCacheSize|não|128K-2G|16M|Tamanho do cache de histórico de textos, em bytes.<br>Tamanho de memória compartilhada para armazenamento de dados históricos de textos.|
|HousekeepingFrequency|não|0-24|1|Intervalo entre execuções do processo de limpeza de dados (housekeeping) (em horas).<br>A limpeza de dados é o processo que remove do banco de dados as informações antigas.<br>*Nota*: Para prevenir que o 'housekeeper' de sobrecarregar o servidor (por exemplo, durante uma redução muito grande de período de armazenamento de histórico e médias), os lotes de exclusão de dados serão removidas mais do que 4 horas de informação a cada ciclo de limpeza. Então, se 'HousekeepingFrequency' for '1', não mais que 4 horas de informações antigas (começando pela informação mais antiga) será excluída por ciclo.<br>*Nota*: Para diminuir a carga sobre o Zabbix Server o processo de limpeza começará a ser executado 30 minutos após a inicialização do servidor. Logo, se o 'HousekeepingFrequency' for '1', o primeiro ciclo de limpeza irá iniciar 30 minutos após o Zabbix Server ter sido iniciado, e será repetido uma hora após. Este atraso no inicio da execução do processo de limpeza existe desde o **Zabbix 2.4.0.**<br>A partir do **Zabbix 3.0.0** é possível desabilitar a execução automática do 'housekeeping' ao definir o parâmetro 'HousekeepingFrequency' para '0'. Neste caso o processo de limpeza só será iniciado quando o parâmetro de tempo de execução *housekeeper\_execute* e o período de informação antiga for superior à quatro vezes a última limpeza, não sendo menor que 4 hours ou maior que 4 dias.|
|Include|não|<|<|Você pode incluir arquivos ou diretórios no arquivo de configuração.<br>Para incluir apenas arquivos relevantes em determinado diretório, são suportados caracteres coringas para localizar um padrão. Por exemplo: `/absolute/path/to/config/files/*.conf`. Os padrões são suportados desde o **Zabbix 2.4.0.**<br>Veja [notas adicionais](special_notes_include) sobre as limitações.|
|JavaGateway|não|<|<|Endereço IP (ou hostname) do Zabbix Java gateway.<br>Requerido apenas se os poolers Java tiverem sido iniciados.<br>Este parâmetro é suportado desde o Zabbix 2.0.0.|
|JavaGatewayPort|não|1024-32767|10052|Porta que o Zabbix Java gateway estará escutando.<br>Este parâmetro é suportado desde o Zabbix 2.0.0.|
|ListenIP|não|<|0.0.0.0|Lista separada por vírgulas com os endereços IP que o Trapper deverá escutar.<br>O trapper irá escutar em todas as interfaces se este parâmetro estier ausente.<br>Múltiplos endereços IP são suportados desde o Zabbix 1.8.3.|
|ListenPort|não|1024-32767|10051|Porta do Trapper.|
|LoadModule|não|<|<|Módulo a carregar durante a inicialização do servidor. Módulos são utilizados para extender as funcionalidades do Zabbix Server.<br>Formato: LoadModule=<module.so><br>Os módulos precisam estar localizados no diretório especificado em LoadModulePath.<br>É permitida a inclusão de vários parâmetros 'LoadModule'.|
|LoadModulePath|não|<|<|Caminho completo do local (diretório) onde estarão os módulos carregáveis do servidor.<br>O valor padrão depende de opções de compilação.|
|LogFile|não|<|<|Nome do arquivo de log.|
|LogFileSize|não|0-1024|1|Tamanho máximo do log em MB.<br>0 - desativa a rotação de log.<br>*Nota*: Se o tamanho limite do log for alcançado e a rotação falhar, por qualquer que seja o motivo, o log atual será truncado e será inicializado um novo.|
|LogSlowQueries|não|0-3600000|0|Define o tempo mínimo de execução de uma consulta SQL para que ela seja classificada como lenta e registrada em log (em milisegundos).<br>0 - não registra queries lentas.<br>Esta opção é habilitada com se *DebugLevel=3*.<br>Este parâmetro é suportado desde o Zabbix 1.8.2.|
|MaxHousekeeperDelete|não|0-1000000|5000|Não mais que 'MaxHousekeeperDelete' linhas (correspondendo a \[tablename\], \[field\], \[value\]) serão excluídas por ciclo do housekeeping.<br>SQLite3 não usa este parâmetro, exclui todas as linhas sem limitação.<br>Se definido como '0' então nenhum limite será utilizado. Neste último caso, é muito importante que você saiba o que está fazendo!<br>Este parâmetro é suportado desde o Zabbix 1.8.2 e se aplica somente a exclusão de histórico e médias de itens já excluidos.|
|PidFile|não|<|/tmp/zabbix\_server.pid|Nome do arquivo de PID.|
|ProxyConfigFrequency|não|1-604800|3600|Frequência em que o Zabbix Server envia os dados de configuração para o Proxy, em segundos. Utilizado apenas por proxies em modo passivo.<br>Este parâmetro é suportado desde o Zabbix 1.8.3.|
|ProxyDataFrequency|não|1-3600|1|Frequência em que o Zabbix Server requisita os dados históricos de um Proxy em segundos. Utilizado apenas para proxies em modo passivo.<br>Este parâmetro é suportado desde o Zabbix 1.8.3.|
|SenderFrequency|não|5-3600|30|Frequência em que o Zabbix irá tentar enviar alertas ainda pendentes (em segundos).|
|SNMPTrapperFile|não|<|/tmp/zabbix\_traps.tmp|Arquivo temporário utilizado para analisar os dados recebidos pelo daemon de traps SNMP.<br>O zabbix\_trap\_receiver.pl ou SNMPTT precisam ter este memsmo nome eu suas configurações.<br>Este parâmetro é suportado desde o Zabbix 2.0.0.|
|SourceIP|não|<|<|Endereço IP para conexões de saída.|
|SSHKeyLocation|não|<|<|Localização das chaves pública e privada para verificações SSH e ações|
|SSLCertLocation|não|<|<|Localização do certificado de cliente SSL para a autenticação de cliente.<br>Este parâmetro é utilizado na monitoração web apenas e é suportado desde o Zabbix 2.4.|
|SSLKeyLocation|não|<|<|Localização da chave privada SSL utilizada para a autenticação de cliente.<br>Este parâmetro é utilizado somente na monitoração web e é suportado desde o Zabbix 2.4.|
|SSLCALocation|não|<|<|Sobrescreve a localização da autoridade certificadora (CA) para a verificação de certificados SSL de servidor. Se não for definido, o diretório global será usado.<br>Observe que o valor deste parâmetro será definido na opção CURLOPT\_CAPATH da biblioteca libcurl. Para versões do libcurl anteriores à 7.42.0, isso só tem efeito se o libcurl for compilado com o OpenSSL. para maiores informações veja [a página do cURL](http://curl.haxx.se/libcurl/c/CURLOPT_CAPATH.html).<br>Este parâmetro é utilizado na monitoração web desde Zabbix 2.4.0 e na autenticação SMTP desde o Zabbix 3.0.0.|
|StartDBSyncers|não|1-100|4|Quantidade de processos pré-alocados dos DB Syncers.<br>O limite superior a ser utilizado era 64 antes da versão 1.8.5.<br>Este parâmetro é suportado desde o Zabbix 1.8.3.|
|StartDiscoverers|não|0-250|1|Quantidade de processos pré-alocados dos discoverers.<br>O limite superior a ser utilizado era 255 antes da versão 1.8.5.|
|StartHTTPPollers|não|0-1000|1|Quantidade de processos pré-alocados dos HTTP pollers.<br>O limite superior a ser utilizado era 255 antes da versão 1.8.5.|
|StartIPMIPollers|não|0-1000|0|Quantidade de processos pré-alocados dos IPMI pollers.<br>O limite superior a ser utilizado era 255 antes da versão 1.8.5.|
|StartJavaPollers|não|0-1000|0|Quantidade de processos pré-alocados dos Java pollers.<br>Este parâmetro é suportado desde o Zabbix 2.0.0.|
|StartPingers|não|0-1000|1|Quantidade de processos pré-alocados dos ICMP pingers.<br>O limite superior a ser utilizado era 255 antes da versão 1.8.5.|
|StartPollersUnreachable|não|0-1000|1|Quantidade de processos pré-alocados dos pollers de hosts indisponíveis (incluindo IPMI e Java).<br>Desde o **Zabbix 2.4.0**, pelo menos um poller para hosts indisponíveis precisa ser mantido em execução se existirem pollers IPMI ou Java iniciados.<br>O limite superior a ser utilizado era 255 antes da versão 1.8.5.|
|StartPollers|não|0-1000|5|Quantidade de processos pré-alocados dos pollers.<br>O limite superior a ser utilizado era 255 antes da versão 1.8.5.|
|StartProxyPollers|não|0-250|1|Quantidade de processos pré-alocados dos pollers for passive proxies.<br>O limite superior a ser utilizado era 255 antes da versão 1.8.5.<br>Este parâmetro é suportado desde o Zabbix 1.8.3.|
|StartSNMPTrapper|não|0-1|0|If set to 1, SNMP trapper process will be started.<br>Este parâmetro é suportado desde o Zabbix 2.0.0.|
|StartTimers|não|1-1000|1|Quantidade de processos pré-alocados dos timers.<br>Os processos de 'timers' são utilizados em funções de trigger baseadas em tempo e em períodos de manutenção.<br>Apenas o primeiro timer gerencia os períodos de manutenção.<br>Este parâmetro é suportado desde o Zabbix 2.2.0.|
|StartTrappers|não|0-1000|5|Quantidade de processos pré-alocados dos trappers.<br>As trappers aceitam conexões de entrada oriundas do 'Zabbix Sender', agentes e proxies ativos.<br>No mínimo um processo de trapper deverá estar em execução para apresentar a disponibilidade do servidor e visualizar a fila na interface web.<br>O limite superior a ser utilizado era 255 antes da versão 1.8.5.|
|StartVMwareCollectors|não|0-250|0|Quantidade de processos pré-alocados de coletores vmware.<br>Este parâmetro é suportado desde o Zabbix 2.2.0.|
|Timeout|não|1-30|3|Especifica o tempo máximo a se aguardar por um dado do Zabbix Agent, dispositivo SNMP ou verificação externa (em segundos).|
|TLSCAFile|não|<|<|Caminho completo para o arquivo contendo os certificados CA de alto nível para verificação das partes, utilizado para a criptografia das comunicações entre os componentes do Zabbix.|
|TLSCertFile|não|<|<|Caminho completo para o arquivo contendo o certificado do servidor ou a cadeia de certificados, utilizado para a criptografia das comunicações entre os componentes do Zabbix.|
|TLSCRLFile|não|<|<|Caminho completo para o arquivo contendo os certificados revogados. Utilizado para a criptografia das comunicações entre os componentes do Zabbix.|
|TLSKeyFile|não|<|<|Caminho completo para o arquivo contendo a chave privada do servidor, utilizado para a criptografia das comunicações entre os componentes do Zabbix.|
|TmpDir|não|<|/tmp|Diretório temporário.|
|TrapperTimeout|não|1-300|300|Define o máximo em segundos que uma trap pode levar para enviar um dado.|
|TrendCacheSize|não|128K-2G|4M|Tamanho do cache de médias, em bytes.<br>Tamanho da memória compartilhada para armazenar dados de médias.|
|UnavailableDelay|não|1-3600|60|Periodicidade de verificação de disponibilidade durante o período de [indisponibilidade](/pt/manual/appendix/items/unreachability#unavailable_host), em segundos.|
|UnreachableDelay|não|1-3600|15|Periodicidade de verificação de acessibilidade durante um período de [inacessibilidade](/pt/manual/appendix/items/unreachability#unreachable_host), em segundos.|
|UnreachablePeriod|não|1-3600|45|Após quantos segundos de [indisponibilidade](/pt/manual/appendix/items/unreachability#unreachable_host) o host será tratado como indisponível.|
|User|não|<|zabbix|Muda os permissionamentos para um usuário específico do sistema.<br>Só afeta se o processo for iniciado pelo usuário 'root' e 'AllowRoot' estiver desativado.<br>Este parâmetro é suportado desde o**Zabbix 2.4.0**.|
|ValueCacheSize|não|0,128K-64G|8M|Tamanho do cache de valores do histórico, em bytes.<br>Tamanho da memória compartilhada para cache de requisições de dados do histórico.<br>Definindo para '0' o cache será destivado (não é recomendável).<br>Quando o cache estiver lotado, será registrado no log um alerta a cada 5 minutos.<br>Este parâmetro é suportado desde o Zabbix 2.2.0.|
|VMwareCacheSize|não|256K-2G|8M|Tamanho da memória compartilhada para dados do VMware.<br>Uma verificação interna **zabbix\[vmware,buffer,...\]** poderá ser utilizada par amonitorar o uso de cache VMware (veja mais em [verificações internas](/pt/manual/config/items/itemtypes/internal)).<br>Observe que a memória compartilhada não será alocada se não existirem instâncias de coleta VMware configuradas para inicialização.<br>Este parâmetro é suportado desde o Zabbix 2.2.0.|
|VMwareFrequency|não|10-86400|60|Intervalo em segundos entre coletar um dado do serviço do VMware.<br>Este intervalo deverá ser menor do que os intervalos entre coletas de itens do VMware.<br>Este parâmetro é suportado desde o Zabbix 2.2.0.|
|VMwarePerfFrequency|não|10-86400|60|Intervalo em segundos entre a coleta de dados de performance e dados simples do VMware.<br>Este intervalo deverá ser menor do que os intervalos entre coletas de itens do VMware.<br>Este parâmetro é suportado desde o Zabbix 2.2.9, 2.4.4|
|VMwareTimeout|não|1-300|10|Tempo máximo de espera para coletar um item do VMware (vCenter ou ESX hypervisor).<br>Este parâmetro é suportado desde o Zabbix 2.2.9, 2.4.4|

::: noteclassic
O Zabbix suporta arquivos de configuração somente no formato
UTF-8 sem [BOM](https://en.wikipedia.org/wiki/Byte_order_mark).\
\
Comentários inciam com "\#" e só são suportados no início das
linhas.
:::

[comment]: # ({/new-dbb2a1f5})




[comment]: # ({new-16f1be5f})
#### Overview

This section lists parameters supported in a Zabbix server configuration
file (zabbix\_server.conf). Note that:

-   The default values reflect daemon defaults, not the values in the
    shipped configuration files;
-   Zabbix supports configuration files only in UTF-8 encoding without
    [BOM](https://en.wikipedia.org/wiki/Byte_order_mark);
-   Comments starting with "\#" are only supported in the beginning of
    the line.

[comment]: # ({/new-16f1be5f})

[comment]: # ({new-6428a44e})
### Parameter details

[comment]: # ({/new-6428a44e})


[comment]: # ({new-e273f2ed})
##### `AlertScriptsPath`
The location of custom alert scripts (depends on the *datadir* compile-time installation variable).

Default: `/usr/local/share/zabbix/alertscripts`

[comment]: # ({/new-e273f2ed})

[comment]: # ({new-1edb700a})
##### `AllowRoot`
Allow the server to run as 'root'. If disabled and the server is started by 'root', the server will try to switch to the 'zabbix' user instead. Has no effect if started under a regular user.

Default: `0` | Values: 0 - do not allow; 1 - allow

[comment]: # ({/new-1edb700a})

[comment]: # ({new-37f8828a})
##### `AllowUnsupportedDBVersions`
Allow the server to work with unsupported database versions.

Default: `0` | Values: 0 - do not allow; 1 - allow

[comment]: # ({/new-37f8828a})

[comment]: # ({new-6c704994})
##### `CacheSize`
The size of the configuration cache, in bytes. The shared memory size for storing host, item and trigger data.

Default: `32M` | Range: 128K-64G

[comment]: # ({/new-6c704994})

[comment]: # ({new-2c956137})
##### `CacheUpdateFrequency`
This parameter determines how often Zabbix will perform the configuration cache update in seconds. See also [runtime control](/manual/concepts/server#runtime-control) options.

Default: `10` | Range: 1-3600

[comment]: # ({/new-2c956137})

[comment]: # ({new-d92a7b2b})
##### `DBHost`
The database host name.<br>With MySQL `localhost` or empty string results in using a socket. With PostgreSQL only empty string results in attempt to use socket. With [Oracle](/manual/appendix/install/oracle#connection_set_up) empty string results in using the Net Service Name connection method; in this case consider using the TNS\_ADMIN environment variable to specify the directory of the tnsnames.ora file.

Default: `localhost`

[comment]: # ({/new-d92a7b2b})

[comment]: # ({new-1058c650})
##### `DBName`
The database name.<br>With [Oracle](/manual/appendix/install/oracle#connection_set_up), if the Net Service Name connection method is used, specify the service name from tnsnames.ora or set to empty string; set the TWO\_TASK environment variable if DBName is set to empty string.

Mandatory: Yes

[comment]: # ({/new-1058c650})

[comment]: # ({new-479ee610})
##### `DBPassword`
The database password. Comment this line if no password is used.

[comment]: # ({/new-479ee610})

[comment]: # ({new-1b941880})
##### `DBPort`
The database port when not using local socket.<br>With [Oracle](/manual/appendix/install/oracle#connection_set_up), if the Net Service Name connection method is used, this parameter will be ignored; the port number from the tnsnames.ora file will be used instead.

Range: 1024-65535

[comment]: # ({/new-1b941880})

[comment]: # ({new-abcff68d})
##### `DBSchema`
The database schema name. Used for PostgreSQL.

[comment]: # ({/new-abcff68d})

[comment]: # ({new-3a14461c})
##### `DBSocket`
The path to the MySQL socket file.

[comment]: # ({/new-3a14461c})

[comment]: # ({new-94782f63})
##### `DBUser`
The database user.

[comment]: # ({/new-94782f63})

[comment]: # ({new-500737d5})
##### `DBTLSConnect`
Setting this option to the following values enforces to use a TLS connection to the database:<br>*required* - connect using TLS<br>*verify\_ca* - connect using TLS and verify certificate<br>*verify\_full* - connect using TLS, verify certificate and verify that database identity specified by DBHost matches its certificate<br><br>With MySQL, starting from 5.7.11, and PostgreSQL the following values are supported: `required`, `verify\_ca`, `verify\_full`.<br>With MariaDB, starting from version 10.2.6, the `required` and `verify\_full` values are supported.<br>By default not set to any option and the behavior depends on database configuration.

[comment]: # ({/new-500737d5})

[comment]: # ({new-fc356178})
##### `DBTLSCAFile`
The full pathname of a file containing the top-level CA(s) certificates for database certificate verification.

Mandatory: no (yes, if DBTLSConnect set to *verify\_ca* or *verify\_full*)

[comment]: # ({/new-fc356178})

[comment]: # ({new-5f286da4})
##### `DBTLSCertFile`
The full pathname of a file containing the Zabbix server certificate for authenticating to database.

[comment]: # ({/new-5f286da4})

[comment]: # ({new-dd8b56f5})
##### `DBTLSKeyFile`
The full pathname of a file containing the private key for authenticating to database.

[comment]: # ({/new-dd8b56f5})

[comment]: # ({new-d8056d7a})
##### `DBTLSCipher`
The list of encryption ciphers that Zabbix server permits for TLS protocols up through TLS v1.2. Supported only for MySQL.

[comment]: # ({/new-d8056d7a})

[comment]: # ({new-a327c2f2})
##### `DBTLSCipher13`
The list of encryption ciphersuites that Zabbix server permits for the TLS v1.3 protocol. Supported only for MySQL, starting from version 8.0.16.

[comment]: # ({/new-a327c2f2})

[comment]: # ({new-d82f69fb})
##### `DebugLevel`
Specify the debug level:<br>*0* - basic information about starting and stopping of Zabbix processes<br>*1* - critical information;<br>*2* - error information;<br>*3* - warnings;<br>*4* - for debugging (produces lots of information);<br>*5* - extended debugging (produces even more information).<br>See also [runtime control](/manual/concepts/server#server_process) options.

Default: `3` | Range: 0-5

[comment]: # ({/new-d82f69fb})

[comment]: # ({new-99815de4})
##### `ExportDir`
The directory for [real-time export](/manual/appendix/install/real_time_export) of events, history and trends in newline-delimited JSON format. If set, enables the real-time export.

[comment]: # ({/new-99815de4})

[comment]: # ({new-965e757e})
##### `ExportFileSize`
The maximum size per export file in bytes. Used for rotation if `ExportDir` is set.

Default: `1G` | Range: 1M-1G

[comment]: # ({/new-965e757e})

[comment]: # ({new-5447304d})
##### `ExportType`
The list of comma-delimited entity types (events, history, trends) for [real-time export](/manual/appendix/install/real_time_export) (all types by default). Valid only if ExportDir is set.<br>*Note* that if ExportType is specified, but ExportDir is not, then this is a configuration error and the server will not start.

Example for history and trends export:

    ExportType=history,trends

Example for event export only:

    ExportType=events

[comment]: # ({/new-5447304d})

[comment]: # ({new-d0a46edf})
##### `ExternalScripts`
The location of external scripts (depends on the `datadir` compile-time installation variable).

Default: `/usr/local/share/zabbix/externalscripts`

[comment]: # ({/new-d0a46edf})

[comment]: # ({new-bdd6796f})
##### `Fping6Location`
The location of fping6. Make sure that the fping6 binary has root ownership and the SUID flag set. Make empty ("Fping6Location=") if your fping utility is capable to process IPv6 addresses.

Default: `/usr/sbin/fping6`

[comment]: # ({/new-bdd6796f})

[comment]: # ({new-025648b5})
##### `FpingLocation`
The location of fping. Make sure that the fping binary has root ownership and the SUID flag set.

Default: `/usr/sbin/fping`

[comment]: # ({/new-025648b5})

[comment]: # ({new-8bb05e99})
##### `HANodeName`
The high availability cluster node name. When empty the server is working in standalone mode and a node with empty name is created.

[comment]: # ({/new-8bb05e99})

[comment]: # ({new-c802a26c})
##### `HistoryCacheSize`
The size of the history cache, in bytes. The shared memory size for storing history data.

Default: `16M` | Range: 128K-2G

[comment]: # ({/new-c802a26c})

[comment]: # ({new-2753befa})
##### `HistoryIndexCacheSize`
The size of the history index cache, in bytes. The shared memory size for indexing the history data stored in history cache. The index cache size needs roughly 100 bytes to cache one item.

Default: `4M` | Range: 128K-2G

[comment]: # ({/new-2753befa})

[comment]: # ({new-b5dc4ecc})
##### `HistoryStorageDateIndex`
Enable preprocessing of history values in history storage to store values in different indices based on date.

Default: `0` | Values: 0 - disable; 1 - enable

[comment]: # ({/new-b5dc4ecc})

[comment]: # ({new-08949761})
##### `HistoryStorageURL`
The history storage HTTP\[S\] URL. This parameter is used for [Elasticsearch](/manual/appendix/install/elastic_search_setup) setup.

[comment]: # ({/new-08949761})

[comment]: # ({new-131c3d46})
##### `HistoryStorageTypes`
A comma-separated list of value types to be sent to the history storage. This parameter is used for [Elasticsearch](/manual/appendix/install/elastic_search_setup) setup.

Default: `uint,dbl,str,log,text`

[comment]: # ({/new-131c3d46})

[comment]: # ({new-42928b03})
##### `HousekeepingFrequency`
This parameter determines how often Zabbix will perform the housekeeping procedure in hours. Housekeeping is removing outdated information from the database.<br>*Note*: To prevent housekeeper from being overloaded (for example, when history and trend periods are greatly reduced), no more than 4 times HousekeepingFrequency hours of outdated information are deleted in one housekeeping cyc,le, for each item. Thus, if HousekeepingFrequency is 1, no more than 4 hours of outdated information (starting from the oldest entry) will be deleted per cycle.<br>*Note*: To lower load on server startup housekeeping is postponed for 30 minutes after server start. Thus, if HousekeepingFrequency is 1, the very first housekeeping procedure after server start will run after 30 minutes, and will repeat with one hour delay thereafter.<br>It is possible to disable automatic housekeeping by setting HousekeepingFrequency to 0. In this case the housekeeping procedure can only be started by *housekeeper\_execute* runtime control option and the period of outdated information deleted in one housekeeping cycle is 4 times the period since the last housekeeping cycle, but not less than 4 hours and not greater than 4 days.<br>See also [runtime control](/manual/concepts/server#server_process) options.

Default: `1` | Range: 0-24

[comment]: # ({/new-42928b03})

[comment]: # ({new-5836e8ba})
##### `Include`
You may include individual files or all files in a directory in the configuration file. To only include relevant files in the specified directory, the asterisk wildcard character is supported for pattern matching. See [special notes](special_notes_include) about limitations.

Example:

    Include=/absolute/path/to/config/files/*.conf

[comment]: # ({/new-5836e8ba})

[comment]: # ({new-97c00865})
##### `JavaGateway`
The IP address (or hostname) of Zabbix Java gateway. Only required if Java pollers are started.

[comment]: # ({/new-97c00865})

[comment]: # ({new-fc4d246a})
##### `JavaGatewayPort`
The port that Zabbix Java gateway listens on.

Default: `10052` | Range: 1024-32767

[comment]: # ({/new-fc4d246a})

[comment]: # ({new-22241946})
##### `ListenBacklog`
The maximum number of pending connections in the TCP queue.<br>The default value is a hard-coded constant, which depends on the system.<br>The maximum supported value depends on the system, too high values may be silently truncated to the 'implementation-specified maximum'.

Default: `SOMAXCONN` | Range: 0 - INT\_MAX

[comment]: # ({/new-22241946})

[comment]: # ({new-33f1aea9})
##### `ListenIP`
A list of comma-delimited IP addresses that the trapper should listen on.<br>Trapper will listen on all network interfaces if this parameter is missing.

Default: `0.0.0.0`

[comment]: # ({/new-33f1aea9})

[comment]: # ({new-98e0be1b})
##### `ListenPort`
The listen port for trapper.

Default: `10051` | Range: 1024-32767

[comment]: # ({/new-98e0be1b})

[comment]: # ({new-55309e9c})
##### `LoadModule`
The module to load at server startup. Modules are used to extend the functionality of the server.  The module must be located in the directory specified by LoadModulePath or the path must precede the module name. If the preceding path is absolute (starts with '/') then LoadModulePath is ignored.<br>Formats:<br>LoadModule=<module.so><br>LoadModule=<path/module.so><br>LoadModule=</abs\_path/module.so><br>It is allowed to include multiple LoadModule parameters.

[comment]: # ({/new-55309e9c})

[comment]: # ({new-0c0c0723})
##### `LoadModulePath`
Full path to location of server modules.<br>Default depends on compilation options.

[comment]: # ({/new-0c0c0723})

[comment]: # ({new-c650f3c9})
##### `LogFile`
Name of the log file.

Mandatory: Yes, if LogType is set to *file*; otherwise no

[comment]: # ({/new-c650f3c9})

[comment]: # ({new-9c05648a})
##### `LogFileSize`
Maximum size of the log file in MB.<br>0 - disable automatic log rotation.<br>*Note*: If the log file size limit is reached and file rotation fails, for whatever reason, the existing log file is truncated and started anew.

Default: `1` | Range: 0-1024 | Mandatory: Yes, if LogType is set to *file*; otherwise no

[comment]: # ({/new-9c05648a})

[comment]: # ({new-2f20d22f})
##### `LogSlowQueries`
Determines how long a database query may take before being logged in milliseconds.<br>0 - don't log slow queries.<br>This option becomes enabled starting with DebugLevel=3.

Default: `0` | Range: 0-3600000

[comment]: # ({/new-2f20d22f})

[comment]: # ({new-926c45e2})
##### `LogType`
Type of the log output:<br>*file* - write log to file specified by LogFile parameter;<br>*system* - write log to syslog;<br>*console* - write log to standard output.

Default: `file`

[comment]: # ({/new-926c45e2})

[comment]: # ({new-124ae689})
##### `MaxHousekeeperDelete`
No more than 'MaxHousekeeperDelete' rows (corresponding to \[tablename\], \[field\], \[value\]) will be deleted per one task in one housekeeping cycle.<br>If set to 0 then no limit is used at all. In this case you must know what you are doing, so as not to [overload the database!](zabbix_server#footnotes) **^[2](zabbix_server#footnotes)^**<br>This parameter applies only to deleting history and trends of already deleted items.

Default: `5000` | Range: 0-1000000

[comment]: # ({/new-124ae689})

[comment]: # ({new-9cfb79d4})
##### `NodeAddress`
IP or hostname with optional port to override how the frontend should connect to the server.<br>Format: \<address>\[:\<port>\]<br><br> The priority of addresses used by the frontend to specify the server **address** is:<br>- the address specified in NodeAddress (1)<br>- ListenIP (if not 0.0.0.0 or ::) (2)<br>- localhost (default) (3)<br>The priority of ports used by the frontend to specify the server **port** is:<br>- the port specified in NodeAddress (1)<br>- ListenPort (2)<br>- 10051 (default) (3)<br>See also: [HANodeName](#hanodename) parameter; [Enabling high availability](/manual/concepts/server/ha#enabling-high-availability).

Default: 'localhost:10051'

[comment]: # ({/new-9cfb79d4})

[comment]: # ({new-8153735c})
##### `PidFile`
Name of the PID file.

Default: `/tmp/zabbix\_server.pid`

[comment]: # ({/new-8153735c})

[comment]: # ({new-b0d337ac})
##### `ProblemHousekeepingFrequency`
Determines how often Zabbix will delete problems for deleted triggers in seconds.

Default: `60` | Range: 1-3600

[comment]: # ({/new-b0d337ac})

[comment]: # ({new-b3642a43})
##### `ProxyConfigFrequency`
Determines how often Zabbix server sends configuration data to a Zabbix proxy in seconds. Used only for proxies in a passive mode.

Default: `10` | Range: 1-604800

[comment]: # ({/new-b3642a43})

[comment]: # ({new-3bb5827f})
##### `ProxyDataFrequency`
Determines how often Zabbix server requests history data from a Zabbix proxy in seconds. Used only for proxies in the passive mode.

Default: `1` | Range: 1-3600

[comment]: # ({/new-3bb5827f})

[comment]: # ({new-7df5a0e9})
##### `ServiceManagerSyncFrequency`
Determines how often Zabbix will synchronize the configuration of a service manager in seconds.

Default: `60` | Range: 1-3600

[comment]: # ({/new-7df5a0e9})

[comment]: # ({new-9c385986})
##### `SNMPTrapperFile`
Temporary file used for passing data from the SNMP trap daemon to the server.<br>Must be the same as in zabbix\_trap\_receiver.pl or SNMPTT configuration file.

Default: `/tmp/zabbix\_traps.tmp`

[comment]: # ({/new-9c385986})

[comment]: # ({new-cd750f09})
##### `SocketDir`
Directory to store IPC sockets used by internal Zabbix services.

Default: `/tmp`

[comment]: # ({/new-cd750f09})

[comment]: # ({new-bf3120f2})
##### `SourceIP`
Source IP address for:<br>- outgoing connections to Zabbix proxy and Zabbix agent;<br>- agentless connections (VMware, SSH, JMX, SNMP, Telnet and simple checks);<br>- HTTP agent connections;<br>- script item JavaScript HTTP requests;<br>- preprocessing JavaScript HTTP requests;<br>- sending notification emails (connections to SMTP server);<br>- webhook notifications (JavaScript HTTP connections);<br>- connections to the Vault

[comment]: # ({/new-bf3120f2})

[comment]: # ({new-8edbdae2})
##### `SSHKeyLocation`
Location of public and private keys for SSH checks and actions.

[comment]: # ({/new-8edbdae2})

[comment]: # ({new-f14a0fc6})
##### `SSLCertLocation`
Location of SSL client certificate files for client authentication.<br>This parameter is used in web monitoring only.

[comment]: # ({/new-f14a0fc6})

[comment]: # ({new-d38d629d})
##### `SSLKeyLocation`
Location of SSL private key files for client authentication.<br>This parameter is used in web monitoring only.

[comment]: # ({/new-d38d629d})

[comment]: # ({new-d4ecaa9a})
##### `SSLCALocation`
Override the location of certificate authority (CA) files for SSL server certificate verification. If not set, system-wide directory will be used.<br>Note that the value of this parameter will be set as libcurl option CURLOPT\_CAPATH. For libcurl versions before 7.42.0, this only has effect if libcurl was compiled to use OpenSSL. For more information see [cURL web page](http://curl.haxx.se/libcurl/c/CURLOPT_CAPATH.html).<br>This parameter is used in web monitoring and in SMTP authentication.

[comment]: # ({/new-d4ecaa9a})

[comment]: # ({new-49587d38})
##### `StartAlerters`
The number of pre-forked instances of [alerters](/manual/concepts/server#server_process_types).

Default: `3` | Range: 1-100

[comment]: # ({/new-49587d38})

[comment]: # ({new-4404a76d})
##### `StartConnectors`
The number of pre-forked instances of [connector workers](/manual/concepts/server#server_process_types). The connector manager process is started automatically when a connector worker is started.

Range: 0-1000

[comment]: # ({/new-4404a76d})

[comment]: # ({new-25c85aff})
##### `StartDBSyncers`
The number of pre-forked instances of [history syncers](/manual/concepts/server#server_process_types).<br>*Note*: Be careful when changing this value, increasing it may do more harm than good. Roughly, the default value should be enough to handle up to 4000 NVPS.

Default: `4` | Range: 1-100

[comment]: # ({/new-25c85aff})

[comment]: # ({new-40a86c5a})
##### `StartDiscoverers`
The number of pre-forked instances of [discoverers](/manual/concepts/server#server_process_types).

Default: `1` | Range: 0-250

[comment]: # ({/new-40a86c5a})

[comment]: # ({new-5a8be3fd})
##### `StartEscalators`
The number of pre-forked instances of [escalators](/manual/concepts/server#server_process_types).

Default: `1` | Range: 1-100

[comment]: # ({/new-5a8be3fd})

[comment]: # ({new-97ad4fb5})
##### `StartHistoryPollers`
The number of pre-forked instances of [history pollers](/manual/concepts/server#server_process_types).<br>Only required for calculated checks.

Default: `5` | Range: 0-1000

[comment]: # ({/new-97ad4fb5})

[comment]: # ({new-565030c6})
##### `StartHTTPPollers`
The number of pre-forked instances of [HTTP pollers](/manual/concepts/server#server_process_types)**^[1](zabbix_server#footnotes)^**.

Default: `1` | Range: 0-1000

[comment]: # ({/new-565030c6})

[comment]: # ({new-70082588})
##### `StartIPMIPollers`
The number of pre-forked instances of [IPMI pollers](/manual/concepts/server#server_process_types).

Default: `0` | Range: 0-1000

[comment]: # ({/new-70082588})

[comment]: # ({new-5dbd1f62})
##### `StartJavaPollers`
The number of pre-forked instances of [Java pollers](/manual/concepts/server#server_process_types)**^[1](zabbix_server#footnotes)^**.

Default: `0` | Range: 0-1000

[comment]: # ({/new-5dbd1f62})

[comment]: # ({new-d9d80e3e})
##### `StartLLDProcessors`
The number of pre-forked instances of low-level discovery (LLD) [workers](/manual/concepts/server#server_process_types)**^[1](zabbix_server#footnotes)^**.<br>The LLD manager process is automatically started when an LLD worker is started.

Default: `2` | Range: 0-100

[comment]: # ({/new-d9d80e3e})

[comment]: # ({new-79d5cf2e})
##### `StartODBCPollers`
The number of pre-forked instances of [ODBC pollers](/manual/concepts/server#server_process_types)**^[1](zabbix_server#footnotes)^**.

Default: `1` | Range: 0-1000

[comment]: # ({/new-79d5cf2e})

[comment]: # ({new-e788444b})
##### `StartPingers`
The number of pre-forked instances of [ICMP pingers](/manual/concepts/server#server_process_types)**^[1](zabbix_server#footnotes)^**.

Default: `1` | Range: 0-1000

[comment]: # ({/new-e788444b})

[comment]: # ({new-a12742d5})
##### `StartPollersUnreachable`
The number of pre-forked instances of [pollers for unreachable hosts](/manual/concepts/server#server_process_types) (including IPMI and Java)**^[1](zabbix_server#footnotes)^**.<br>At least one poller for unreachable hosts must be running if regular, IPMI or Java pollers are started.

Default: `1` | Range: 0-1000

[comment]: # ({/new-a12742d5})

[comment]: # ({new-5a731224})
##### `StartPollers`
The number of pre-forked instances of [pollers](/manual/concepts/server#server_process_types)**^[1](zabbix_server#footnotes)^**.

Default: `5` | Range: 0-1000

[comment]: # ({/new-5a731224})

[comment]: # ({new-bdcb0d21})
##### `StartPreprocessors`
The number of pre-forked instances of preprocessing [workers](/manual/concepts/server#server_process_types)**^[1](zabbix_server#footnotes)^**.<br>The preprocessing manager process is automatically started when a preprocessor worker is started.

Default: `3` | Range: 1-1000

[comment]: # ({/new-bdcb0d21})

[comment]: # ({new-d6d62537})
##### `StartProxyPollers`
The number of pre-forked instances of [pollers for passive proxies](/manual/concepts/server#server_process_types)**^[1](zabbix_server#footnotes)^**.

Default: `1` | Range: 0-250

[comment]: # ({/new-d6d62537})

[comment]: # ({new-469b0927})
##### `StartReportWriters`
The number of pre-forked instances of [report writers](/manual/concepts/server#server_process_types).<br>If set to 0, scheduled report generation is disabled.<br>The report manager process is automatically started when a report writer is started.

Default: `0` | Range: 0-100

[comment]: # ({/new-469b0927})

[comment]: # ({new-9cccc249})
##### `StartSNMPTrapper`
If set to 1, an [SNMP trapper](/manual/concepts/server#server_process_types) process will be started.

Default: `0` | Range: 0-1

[comment]: # ({/new-9cccc249})

[comment]: # ({new-f1f30b44})
##### `StartTimers`
The number of pre-forked instances of [timers](/manual/concepts/server#server_process_types).<br>Timers process maintenance periods.

Default: `1` | Range: 1-1000

[comment]: # ({/new-f1f30b44})

[comment]: # ({new-368a443a})
##### `StartTrappers`
The number of pre-forked instances of [trappers](/manual/concepts/server#server_process_types)**^[1](zabbix_server#footnotes)^**.<br>Trappers accept incoming connections from Zabbix sender, active agents and active proxies.

Default: `5` | Range: 1-1000

[comment]: # ({/new-368a443a})

[comment]: # ({new-16e17139})
##### `StartVMwareCollectors`
The number of pre-forked [VMware collector](/manual/concepts/server#server_process_types) instances.

Default: `0` | Range: 0-250

[comment]: # ({/new-16e17139})

[comment]: # ({new-0da7c397})
##### `StatsAllowedIP`
List of comma delimited IP addresses, optionally in CIDR notation, or DNS names of external Zabbix instances. Stats request will be accepted only from the addresses listed here. If this parameter is not set no stats requests will be accepted.<br>If IPv6 support is enabled then '127.0.0.1', '::127.0.0.1', '::ffff:127.0.0.1' are treated equally and '::/0' will allow any IPv4 or IPv6 address. '0.0.0.0/0' can be used to allow any IPv4 address.

Example:

    StatsAllowedIP=127.0.0.1,192.168.1.0/24,::1,2001:db8::/32,zabbix.example.com

[comment]: # ({/new-0da7c397})

[comment]: # ({new-db780aaa})
##### `Timeout`
Specifies how long we wait for agent, SNMP device or external check in seconds.

Default: `3` | Range: 1-30

[comment]: # ({/new-db780aaa})

[comment]: # ({new-68df390b})
##### `TLSCAFile`
Full pathname of a file containing the top-level CA(s) certificates for peer certificate verification, used for encrypted communications between Zabbix components.

[comment]: # ({/new-68df390b})

[comment]: # ({new-24534d6d})
##### `TLSCertFile`
Full pathname of a file containing the server certificate or certificate chain, used for encrypted communications between Zabbix components.

[comment]: # ({/new-24534d6d})

[comment]: # ({new-0c9251b8})
##### `TLSCipherAll`
GnuTLS priority string or OpenSSL (TLS 1.2) cipher string. Override the default ciphersuite selection criteria for certificate- and PSK-based encryption.

Example:

    TLS\_AES\_256\_GCM\_SHA384:TLS\_CHACHA20\_POLY1305\_SHA256:TLS\_AES\_128\_GCM\_SHA256

[comment]: # ({/new-0c9251b8})

[comment]: # ({new-74bef172})
##### `TLSCipherAll13`
Cipher string for OpenSSL 1.1.1 or newer in TLS 1.3. Override the default ciphersuite selection criteria for certificate- and PSK-based encryption.

Example for GnuTLS: 

    NONE:+VERS-TLS1.2:+ECDHE-RSA:+RSA:+ECDHE-PSK:+PSK:+AES-128-GCM:+AES-128-CBC:+AEAD:+SHA256:+SHA1:+CURVE-ALL:+COMP-NULL::+SIGN-ALL:+CTYPE-X.509

Example for OpenSSL: 

    EECDH+aRSA+AES128:RSA+aRSA+AES128:kECDHEPSK+AES128:kPSK+AES128

[comment]: # ({/new-74bef172})

[comment]: # ({new-a0eeb337})
##### `TLSCipherCert`
GnuTLS priority string or OpenSSL (TLS 1.2) cipher string. Override the default ciphersuite selection criteria for certificate-based encryption.

Example for GnuTLS: 

    NONE:+VERS-TLS1.2:+ECDHE-RSA:+RSA:+AES-128-GCM:+AES-128-CBC:+AEAD:+SHA256:+SHA1:+CURVE-ALL:+COMP-NULL:+SIGN-ALL:+CTYPE-X.509

Example for OpenSSL: 

    EECDH+aRSA+AES128:RSA+aRSA+AES128

[comment]: # ({/new-a0eeb337})

[comment]: # ({new-07722feb})
##### `TLSCipherCert13`
Cipher string for OpenSSL 1.1.1 or newer in TLS 1.3. Override the default ciphersuite selection criteria for certificate-based encryption.

[comment]: # ({/new-07722feb})

[comment]: # ({new-b3e6744e})
##### `TLSCipherPSK`
GnuTLS priority string or OpenSSL (TLS 1.2) cipher string. Override the default ciphersuite selection criteria for PSK-based encryption.

Example for GnuTLS: 

    NONE:+VERS-TLS1.2:+ECDHE-PSK:+PSK:+AES-128-GCM:+AES-128-CBC:+AEAD:+SHA256:+SHA1:+CURVE-ALL:+COMP-NULL:+SIGN-ALL

Example for OpenSSL: 

    kECDHEPSK+AES128:kPSK+AES128

[comment]: # ({/new-b3e6744e})

[comment]: # ({new-a67b06d2})
##### `TLSCipherPSK13`
Cipher string for OpenSSL 1.1.1 or newer in TLS 1.3. Override the default ciphersuite selection criteria for PSK-based encryption.

Example:

    TLS_CHACHA20_POLY1305_SHA256:TLS_AES_128_GCM_SHA256

[comment]: # ({/new-a67b06d2})

[comment]: # ({new-7b53bee4})
##### `TLSCRLFile`
Full pathname of a file containing revoked certificates. This parameter is used for encrypted communications between Zabbix components.

[comment]: # ({/new-7b53bee4})

[comment]: # ({new-e236df69})
##### `TLSKeyFile`
Full pathname of a file containing the server private key, used for encrypted communications between Zabbix components.

[comment]: # ({/new-e236df69})

[comment]: # ({new-0b008346})
##### `TmpDir`
Temporary directory.

Default: `/tmp`

[comment]: # ({/new-0b008346})

[comment]: # ({new-621f6e9f})
##### `TrapperTimeout`
Specifies how many seconds the trapper may spend processing new data.

Default: `300` | Range: 1-300

[comment]: # ({/new-621f6e9f})

[comment]: # ({new-02c73c1a})
##### `TrendCacheSize`
Size of the trend cache, in bytes.<br>The shared memory size for storing trends data.

Default: `4M` | Range: 128K-2G

[comment]: # ({/new-02c73c1a})

[comment]: # ({new-5eeafa8d})
##### `TrendFunctionCacheSize`
Size of the trend function cache, in bytes.<br>The shared memory size for caching calculated trend function data.

Default: `4M` | Range: 128K-2G

[comment]: # ({/new-5eeafa8d})

[comment]: # ({new-f61cf9e1})
##### `UnavailableDelay`
Determines how often host is checked for availability during the [unavailability](/manual/appendix/items/unreachability#unavailable_host) period in seconds.

Default: `60` | Range: 1-3600

[comment]: # ({/new-f61cf9e1})

[comment]: # ({new-98b994ea})
##### `UnreachableDelay`
Determines how often host is checked for availability during the [unreachability](/manual/appendix/items/unreachability#unreachable_host) period in seconds.

Default: `15` | Range: 1-3600

[comment]: # ({/new-98b994ea})

[comment]: # ({new-df350752})
##### `UnreachablePeriod`
Determines after how many seconds of [unreachability](/manual/appendix/items/unreachability#unreachable_host) treats a host as unavailable.

Default: `45` | Range: 1-3600

[comment]: # ({/new-df350752})

[comment]: # ({new-a9a2ec9d})
##### `User`
Drop privileges to a specific, existing user on the system.<br>Only has effect if run as 'root' and AllowRoot is disabled.

Default: `zabbix`

[comment]: # ({/new-a9a2ec9d})

[comment]: # ({new-20ddb562})
##### `ValueCacheSize`
Size of the history value cache, in bytes.<br>The shared memory size for caching item history data requests.<br>Setting to 0 disables the value cache (not recommended).<br>When the value cache runs out of the shared memory a warning message is written to the server log every 5 minutes.

Default: `8M` | Range: 0,128K-64G

[comment]: # ({/new-20ddb562})

[comment]: # ({new-35521bcc})
##### `Vault`
Specifies the vault provider:<br>*HashiCorp* - HashiCorp KV Secrets Engine version 2<br>*CyberArk*  - CyberArk Central Credential Provider<br>Must match the vault provider set in the frontend.

Default: `HashiCorp`

[comment]: # ({/new-35521bcc})

[comment]: # ({new-664924ee})
##### `VaultDBPath`
Specifies a location, from where database credentials should be retrieved by keys. Depending on the Vault, can be vault path or query.<br> The keys used for HashiCorp are 'password' and 'username'. 

Example: 

    secret/zabbix/database

The keys used for CyberArk are 'Content' and 'UserName'.

Example: 

    AppID=zabbix_server&Query=Safe=passwordSafe;Object=zabbix_proxy_database

This option can only be used if DBUser and DBPassword are not specified.

[comment]: # ({/new-664924ee})

[comment]: # ({new-778803e5})
##### `VaultTLSCertFile`
Name of the SSL certificate file used for client authentication<br> The certificate file must be in PEM1 format. <br> If the certificate file contains also the private key, leave the SSL key file field empty. <br> The directory containing this file is specified by the configuration parameter SSLCertLocation.<br>This option can be omitted but is recommended for CyberArkCCP vault.

[comment]: # ({/new-778803e5})

[comment]: # ({new-755d874e})
##### `VaultTLSKeyFile`
Name of the SSL private key file used for client authentication. <br> The private key file must be in PEM1 format. <br> The directory containing this file is specified by the configuration parameter SSLKeyLocation. <br>This option can be omitted but is recommended for CyberArkCCP vault.

[comment]: # ({/new-755d874e})

[comment]: # ({new-cc25a6c8})
##### `VaultToken`
HashiCorp Vault authentication token that should have been generated exclusively for Zabbix server with read-only permission to the paths specified in [Vault macros](/manual/config/macros/user_macros#configuration) and read-only permission to the path specified in the optional VaultDBPath configuration parameter.<br>It is an error if VaultToken and VAULT\_TOKEN environment variable are defined at the same time.

Mandatory: Yes, if Vault is set to *HashiCorp*; otherwise no

[comment]: # ({/new-cc25a6c8})

[comment]: # ({new-f46a0763})
##### `VaultURL`
Vault server HTTP\[S\] URL. The system-wide CA certificates directory will be used if SSLCALocation is not specified.

Default: `https://127.0.0.1:8200`

[comment]: # ({/new-f46a0763})

[comment]: # ({new-854dfe73})
##### `VMwareCacheSize`
Shared memory size for storing VMware data.<br>A VMware internal check zabbix\[vmware,buffer,...\] can be used to monitor the VMware cache usage (see [Internal checks](/manual/config/items/itemtypes/internal)).<br>Note that shared memory is not allocated if there are no vmware collector instances configured to start.

Default: `8M` | Range: 256K-2G

[comment]: # ({/new-854dfe73})

[comment]: # ({new-2a646d43})
##### `VMwareFrequency`
Delay in seconds between data gathering from a single VMware service.<br>This delay should be set to the least update interval of any VMware monitoring item.

Default: `60` | Range: 10-86400

[comment]: # ({/new-2a646d43})

[comment]: # ({new-cf5957ec})
##### `VMwarePerfFrequency`
Delay in seconds between performance counter statistics retrieval from a single VMware service.<br>This delay should be set to the least update interval of any VMware monitoring [item](/manual/config/items/itemtypes/simple_checks/vmware_keys#footnotes) that uses VMware performance counters.

Default: `60` | Range: 10-86400

[comment]: # ({/new-cf5957ec})

[comment]: # ({new-eefd2428})
##### `VMwareTimeout`
The maximum number of seconds a vmware collector will wait for a response from VMware service (vCenter or ESX hypervisor).

Default: `10` | Range: 1-300

[comment]: # ({/new-eefd2428})

[comment]: # ({new-738954e5})
##### `WebServiceURL`
HTTP\[S\] URL to Zabbix web service in the format `<host:port>/report`. 

Example: 

    WebServiceURL=http://localhost:10053/report

[comment]: # ({/new-738954e5})

[comment]: # ({new-2ab44494})
##### Footnotes

^**1**^ Note that too many data gathering processes (pollers,
unreachable pollers, HTTP pollers, Java pollers, pingers, trappers,
proxypollers) together with IPMI manager, SNMP trapper and preprocessing
workers can **exhaust** the per-process file descriptor limit for the
preprocessing manager.

::: notewarning
This will cause Zabbix server to stop (usually
shortly after the start, but sometimes it can take more time). The
configuration file should be revised or the limit should be raised to
avoid this situation.
:::

^**2**^ When a lot of items are deleted it increases the load to the
database, because the housekeeper will need to remove all the history
data that these items had. For example, if we only have to remove 1 item
prototype, but this prototype is linked to 50 hosts and for every host
the prototype is expanded to 100 real items, 5000 items in total have to
be removed (1\*50\*100). If 500 is set for MaxHousekeeperDelete
(MaxHousekeeperDelete=500), the housekeeper process will have to remove
up to 2500000 values (5000\*500) for the deleted items from history and
trends tables in one cycle.

[comment]: # ({/new-2ab44494})
