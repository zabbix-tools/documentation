[comment]: # attributes: notoc

[comment]: # translation:outdated

[comment]: # ({new-8bf9c5fc})
# - \#5 Zabbix Java gateway

Se você utilizar os scripts `startup.sh` e `shutdown.sh` para iniciar o
[Zabbix Java gateway](/pt/manual/concepts/java), então você poderá
definir os parâmetros de configuração no arquivo `settings.sh`. Os
scripts de inicialização e de desligamento buscam as configurações neste
arquivo, os convertendo de variáveis shell (listadas na primeira coluna)
para propriedades Java (na segunda coluna).

Se você inicializar o 'Zabbix Java gateway' manualmente utilizando o
comando `java` diretamente, você precisará definir as propriedades Java
diretamente na linha de comando.

|Variável|Propriedade|Obrigatório|Range|Padrão|Descrição|
|---------|-----------|------------|-----|-------|-----------|
|LISTEN\_IP|zabbix.listenIP|não|<|0.0.0.0|Endereço IP a escutar.|
|LISTEN\_PORT|zabbix.listenPort|não|1024-32767|10052|Porta a escutar.|
|PID\_FILE|zabbix.pidFile|não|<|/tmp/zabbix\_java.pid|Nome do arquivo de PID. Se omitido o Java Gateway será iniciado como uma aplicação de console.|
|START\_POLLERS|zabbix.startPollers|não|1-1000|5|Quantidade de threads a iniciar.|
|TIMEOUT|zabbix.timeout|não|1-30|3|Tempo máximo a aguardar por operações de rede. Suportado desde o 2.0.15, 2.2.10 e 2.4.5.|

::: notewarning
A porta 10052 não é uma porta [registrada no
IANA](http://www.iana.org/assignments/service-names-port-numbers/service-names-port-numbers.txt).
:::

[comment]: # ({/new-8bf9c5fc})
