[comment]: # translation:outdated

[comment]: # ({new-f68118e5})
# 5 MongoDB plugin

[comment]: # ({/new-f68118e5})

[comment]: # ({new-a831e9b6})
#### Overview

This section lists parameters supported in the MongoDB Zabbix agent 2
plugin configuration file (mongodb.conf). Note that:

-   The default values reflect process defaults, not the values in the
    shipped configuration files;
-   Zabbix supports configuration files only in UTF-8 encoding without
    [BOM](https://en.wikipedia.org/wiki/Byte_order_mark);
-   Comments starting with "\#" are only supported at the beginning of
    the line.

[comment]: # ({/new-a831e9b6})

[comment]: # ({new-eaba6cbf})

#### Options

|Parameter|Description|
|---------|-----------|
|-V --version|Print the plugin version and license information.|
|-h --help|Print help information (shorthand).|

[comment]: # ({/new-eaba6cbf})

[comment]: # ({new-83498ede})
#### Parameters

|Parameter|Mandatory|Range|Default|Description|
|---------|---------|-----|-------|-----------|
|Plugins.Mongo.KeepAlive|no|60-900|300|The maximum time of waiting (in seconds) before unused plugin connections are closed.|
|Plugins.Mongo.Sessions.<SessionName>.Password|no|<|<|Named session password.<br>**<SessionName>** - name of a session for using in item keys.|
|Plugins.Mongo.Sessions.<SessionName>.Uri|no|<|<|Connection string of a named session.<br>**<SessionName>** - name of a session for using in item keys.<br><br>Should not include embedded credentials (they will be ignored).<br>Must match the URI format.<br>Only `tcp` scheme is supported; a scheme can be omitted.<br>A port can be omitted (default=27017).<br>Examples: `tcp://127.0.0.1:27017`, `tcp:localhost`, `localhost`|
|Plugins.Mongo.Sessions.<SessionName>.User|no|<|<|Named session username.<br>**<SessionName>** - name of a session for using in item keys.|
|Plugins.Mongo.Timeout|no|1-30|global timeout|Request execution timeout (how long to wait for a request to complete before shutting it down).|

See also:

-   Description of general Zabbix agent 2 configuration parameters:
    [Zabbix agent 2 (UNIX)](/manual/appendix/config/zabbix_agent2) /
    [Zabbix agent 2
    (Windows)](/manual/appendix/config/zabbix_agent2_win)
-   Instructions for configuring [plugins](/manual/config/items/plugins)

[comment]: # ({/new-83498ede})
