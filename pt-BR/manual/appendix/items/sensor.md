[comment]: # translation:outdated

[comment]: # ({new-b33b8329})
# 7 Sensores

Cada sensor do chip recebe seu próprio diretório na árvore 'sysfs'
(/sys/devices). Para localizar todos so chips sensores, a forma mais
fácil é seguir os links simbólicos de dispositivo de
/sys/class/hwmon/hwmon\*, onde \* é um número real (0,1,2,...).

A leitura dos sensores de dispositivos virtuais está localizada
/sys/class/hwmon/hwmon\*/, enquanto para dispositivos reais se localiza
e /sys/class/hwmon/hwmon\*/device. Um arquivo, Um arquivo localizado de
hwmon\* ou hwmon\*/device irá conter o nome do chip, que corresponderá
ao nome do driver de kernel utilizado com ele.

Em cada arquivo ocorre a leitura de valor de um sensor. O padrão de
nomenclatura dos arquivos em ambas as localizações é:
<type><number>\_<item>, onde

-   **type** - para chips sensores é "in" (voltagem), "temp"
    (temperatura), "fan" (fan), etc.,
-   **item** - "input" (valor medido), "max" (limite máximo), "min"
    (limite mínimo), etc.,
-   **number** - é sempre utilizado para elementos que podem aparecer
    mais de uma vez (normalmente começam com '1', exceto para voltagens
    que começam com '0'). Se os arquivos não se referem a um elemento
    específico então eles terão o nome sem o número.

A informação sobre os sensores disponíveis no host pode ser obtida
usando ferramentas de **sensor-detect** e **sensors** (pacote
lm-sensors: <http://lm-sensors.org/>). **Sensors-detect** ajuda a
determinar qual são os módulos necessários para os sensores disponíveis.
Quando os módulos são carregados o programa **sensors** pode ser
utilizado para ler todos os chips. O padrão de nomenclatura utilizado
pode ser diferente do padrão (<type><number>\_<item>
):

-   se não tiver um arquivo chamado <type><number>\_label,
    então o rótulo dentro do arquivo será utilizado ao invés de
    <type><number><item> ;
-   se não existir o arquivo <type><number>\_label, então o
    programa irá procurar dentro de '/etc/sensors.conf' (também pode ser
    em /etc/sensors3.conf, ou outro) pelo substituto do nome.

Esta nomenclatura permite que o usuário determine qual o tipo de
hardware está sendo utilizado. Se não for encontrado o arquivo
<type><number>\_label ou nenhum rótulo dentro do arquivo de
configuração o tipo do hardware poderá ser determinado pelo atributo do
nome (hwmon\*/device/name). Os nomes atuais dos sensores, que o
zabbix\_agent aceita, podem ser obtidos através do programa **sensors**
com o parâmetro '-u' (**sensors -u**).

Com o programa **sensor** os sensores disponíveis estarão separados pelo
tipo "via" (bus) (adaptador ISA, PCI, SPI, dispositivo virtual,
interface ACPI, adaptador HID).

[comment]: # ({/new-b33b8329})

[comment]: # ({new-61bf6279})
##### No Linux 2.4:

(A leitura dos sensores pode ser obtida a partir do diretório
/proc/sys/dev/sensors)

-   **device** - nome do dispositivo (se o <mode> for utilizado,
    será uma expressão regular);
-   **sensor** - nome do sensor (se o <mode> for utilizado, será
    uma expressão regular);
-   **mode** - valores possíveis: avg, max, min (se este parâmetro for
    omitido o dispositivo e o sensor serão tratados de forma textual).

Exemplo de chave: sensor\[w83781d-i2c-0-2d,temp1\]

Antes do Zabbix 1.8.4, o formato `sensor[temp1]` era utilizado.

[comment]: # ({/new-61bf6279})

[comment]: # ({new-2e1edda6})
##### No Linux 2.6+:

(A leitura dos sensores pode ser obtida a partir do diretório
/sys/class/hwmon )

-   **device** - nome do dispositivo (expressão não regular). O nome do
    dispositivo pode ser o nome atual do dispositivo (ex. 0000:00:18.3)
    ou o nome obtido usando os programas de sensor (ex..
    k8temp-pci-00c3). Cabe ao usuário escolher qual nome utilizar;
-   **sensor** - nome do sensor (expressão não regular);
-   **mode** - valores possíveis: avg, max, min (se este parâmetro for
    omitido o dispositivo e o sensor serão tratados de forma textual).

Exemplos de chave:

sensor\[k8temp-pci-00c3,temp, max\] ou sensor\[0000:00:18.3,temp1\]

sensor\[smsc47b397-isa-0880,in, avg\] ou sensor\[smsc47b397.2176,in1\]

[comment]: # ({/new-2e1edda6})

[comment]: # ({new-0cb28aa9})
#### Obtendo os nomes de sensores

Os rótulos dos sensores, assim como o apresentado pelo comando
**sensors**, não pode ser utilizado diretamente pois o padrão de
nomenclatura de cada fabricante pode ser diferente. Observe o exemplo de
saída do comando *sensors* apresentado:

    $ sensors
    in0:         +2.24 V  (min =  +0.00 V, max =  +3.32 V)   
    Vcore:       +1.15 V  (min =  +0.00 V, max =  +2.99 V)   
    +3.3V:       +3.30 V  (min =  +2.97 V, max =  +3.63 V)   
    +12V:       +13.00 V  (min =  +0.00 V, max = +15.94 V)
    M/B Temp:    +30.0°C  (low  = -127.0°C, high = +127.0°C)

Neste exemplo, somente um rótulo pode ser utilizado diretamente:

    $ zabbix_get -s 127.0.0.1 -k sensor[lm85-i2c-0-2e,in0]
    2.240000

A tentativa de usar os outros rótulos (como *Vcore* ou *+12V*) não
funcionarão.

    $ zabbix_get -s 127.0.0.1 -k sensor[lm85-i2c-0-2e,Vcore]
    ZBX_NOTSUPPORTED

Para descobrir o nome atual de cada sensor, que poderá ser utilizado
pelo Zabbix para ler as informações, execute o comando *sensors -u*. A
saída deverá ser algo similar ao exemplo a seguir:

    $ sensors -u
    ...
    Vcore:
      in1_input: 1.15
      in1_min: 0.00
      in1_max: 2.99
      in1_alarm: 0.00
    ...    
    +12V:
      in4_input: 13.00
      in4_min: 0.00
      in4_max: 15.94
      in4_alarm: 0.00
    ...

Logo, *Vcore* pode ser consultado como *in1*, e *+12V* pode ser
consultado como *in4*.[^1]

    $ zabbix_get -s 127.0.0.1 -k sensor[lm85-i2c-0-2e,in1]
    1.301000

Não apenas a voltagem (in), mas também a corrente (curr), temperatura
(temp) e velocidade do ventilador (fan) podem ser obtidas pelo Zabbix.

[^1]: conforme a
    [especificação](https://www.kernel.org/doc/Documentation/hwmon/sysfs-interface)
    estes são as voltagens nos pinos do chip.

[comment]: # ({/new-0cb28aa9})
