[comment]: # translation:outdated

[comment]: # ({new-098d0431})
# 8 Observações sobre o parâmetro memtype em itens proc.mem

[comment]: # ({/new-098d0431})

[comment]: # ({new-2840c113})
#### Visão geral

O parâmetro **memtype** é suportado no Linux, AIX, FreeBSD, e Solaris.

Os três tipos mais comuns do 'memtype' são suportados em todas as
plataformas: `pmem`, `rss` e `vsize`. Adicionalmente, tipos específicos
de cada plataforma serão nelas suportados.

[comment]: # ({/new-2840c113})

[comment]: # ({new-04a03696})
#### AIX

Os valores a seguir são suportados para o parâmetro 'memtype' nesta
plataforma.

|Valor suportado|Descrição|Fonte em estrutura procentry64|Tentativas para compatibilizar|
|---------------|-----------|------------------------------|------------------------------|
|vsize (( - valor padrão))|Tamanho da memória virtual|pi\_size|<|
|pmem|Percentual da memória real|pi\_prm|ps -o pmem|
|rss|Configuração de tamanho residente|pi\_trss + pi\_drss|ps -o rssize|
|size|Tamanho do processo (código + dados)|pi\_dvm|"ps gvw" SIZE column|
|dsize|Tamanho do dado|pi\_dsize|<|
|tsize|Tamanho de texto (código)|pi\_tsize|"ps gvw" TSIZ column|
|sdsize|Tamanho do dado na biblioteca compartilhada|pi\_sdsize|<|
|drss|Tamanho do conjunto residente de dados|pi\_drss|<|
|trss|Tamanho do conjunto residente de texto|pi\_trss|<|

[comment]: # ({/new-04a03696})

[comment]: # ({new-cc0df248})

Notes for AIX:

1. When choosing parameters for proc.mem[] item key on AIX, try to specify narrow process selection criteria. Otherwise there is a risk of getting unwanted processes counted into proc.mem[] result.

Example:
```
\$ zabbix_agentd -t proc.mem[,,,NonExistingProcess,rss]
proc.mem[,,,NonExistingProcess,rss]           [u|2879488]
```

This example shows how specifying only command line (regular expression to match) parameter results in Zabbix agent self-accounting - probably not what you want.

[comment]: # ({/new-cc0df248})

[comment]: # ({new-14c4cc2b})
2. Do not use "ps -ef" to browse processes - it shows only non-kernel processes. Use "ps -Af" to see all processes which will be seen by Zabbix agent.

3. Let's go through example of 'topasrec' how Zabbix agent proc.mem[] selects processes.

```
\$ ps -Af | grep topasrec
root 10747984        1   0   Mar 16      -  0:00 /usr/bin/topasrec  -L -s 300 -R 1 -r 6 -o /var/perf daily/ -ypersistent=1 -O type=bin -ystart_time=04:08:54,Mar16,2023
```

proc.mem[] has arguments:

```
proc.mem[<name>,<user>,<mode>,<cmdline>,<memtype>]
```

[comment]: # ({/new-14c4cc2b})

[comment]: # ({new-0be32659})

The 1st criterion is a process name (argument <name>). In our example Zabbix agent will see it as 'topasrec'. In order to match, you need to either specify 'topasrec' or to leave it empty.
The 2nd criterion is a user name (argument <user>). To match, you need to either specify 'root' or to leave it empty.
The 3rd criterion used in process selection is an argument <cmdline>. Zabbix agent will see its value as '/usr/bin/topasrec -L -s 300 -R 1 -r 6 -o /var/perf/daily/ -ypersistent=1 -O type=bin -ystart_time=04:08:54,Mar16,2023'. To match, you need to either specify a regular expression which matches this string or to leave it empty.

Arguments <mode> and <memtype> are applied after using the three criteria mentioned above. 

[comment]: # ({/new-0be32659})

[comment]: # ({new-8a700330})
#### FreeBSD

Os valores a seguir são suportados para o parâmetro 'memtype' nesta
plataforma.

|Valor suportado|Descrição|Fonte em estrutura kinfo\_proc|Tentativas para compatibilizar|
|---------------|-----------|------------------------------|------------------------------|
|vsize|Tamanho da memória virtual|kp\_eproc.e\_vm.vm\_map.size or ki\_size|ps -o vsz|
|pmem|Percentual da memória real|calculado a partir do rss|ps -o pmem|
|rss|Tamanho do conjunto residente|kp\_eproc.e\_vm.vm\_rssize or ki\_rssize|ps -o rss|
|size (( - valor padrão))|Tamanho do processo (código + dados + pilha)|tsize + dsize + ssize|<|
|tsize|Tamanho do texto de código|kp\_eproc.e\_vm.vm\_tsize or ki\_tsize|ps -o tsiz|
|dsize|Tamanho dos dado|kp\_eproc.e\_vm.vm\_dsize or ki\_dsize|ps -o dsiz|
|ssize|Tamanho da pilha|kp\_eproc.e\_vm.vm\_ssize or ki\_ssize|ps -o ssiz|

[comment]: # ({/new-8a700330})

[comment]: # ({new-f43d1dfd})
#### Linux

Os valores a seguir são suportados para o parâmetro 'memtype' nesta
plataforma.

|Valor suportado|Descrição|Fonte em /proc/<pid>/status file|
|---------------|-----------|--------------------------------------|
|vsize (( - valor padrão))|Tamanho da memória virtual|VmSize|
|pmem|Percentual da memória real|(VmRSS/total\_memory) \* 100|
|rss|Tamanho do conjunto residente|VmRSS|
|data|Tamanho do segmento de dados|VmData|
|exe|Tamanho do segmento de código|VmExe|
|hwm|Pico de tamanho do conjunto residente|VmHWM|
|lck|Tamanho da memória bloqueada|VmLck|
|lib|Tamanho das bibliotecas compartilhadas|VmLib|
|peak|Pico de tamanho de memória virtual|VmPeak|
|pin|Tamanho das páginas pinadas|VmPin|
|pte|Tamanho das entradas na tabela de páginas|VmPTE|
|size|Tamanho do código de processo + dados + pilha de segmentos|VmExe + VmData + VmStk|
|stk|Tamanho da pilha de segmentos|VmStk|
|swap|Tamanho do espaço de swap utilizado|VmSwap|

Observações para o Linux:

1.  Nem todo valor de 'memtype' será suportado em kernels antigos do
    Linux. Por exemplo, o kernel Linux 2.4 não suporta `hwm`, `pin`,
    `peak`, `pte` e `swap`.
2.  Nós temos informações que o auto-monitoramento do Zabbix Agent ativo
    identificou processos com `proc.mem[...,...,...,...,data]` mostrando
    que o valor é 4kB maior do que o apresentado em `VmData` no arquivo
    de status do agente /proc/<pid>/status . No momento do
    auto-monitoramento o segmento de dados do agente aumentou em 4kB,
    retornando em seguida para o tamanho anterior.

[comment]: # ({/new-f43d1dfd})

[comment]: # ({new-96fd9f28})
#### Solaris

Os valores a seguir são suportados para o parâmetro 'memtype' nesta
plataforma.

|Valor suportado|Descrição|Fonte em estrutura psinfo|Tentativas para compatibilizar|
|---------------|-----------|-------------------------|------------------------------|
|vsize (( - valor padrão))|Tamanho da imagem do processo|pr\_size|ps -o vsz|
|pmem|Percentual da memória real|pr\_pctmem|ps -o pmem|
|rss|Tamanho do conjunto residente<br>Que pode ser subestimado, conforme descrição do rss em "man ps".|pr\_rssize|ps -o rss|

[comment]: # ({/new-96fd9f28})


[comment]: # ({new-e0ce8129})
##### Footnotes

^**1**^ Default value.

[comment]: # ({/new-e0ce8129})
