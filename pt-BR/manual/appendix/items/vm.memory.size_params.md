[comment]: # translation:outdated

[comment]: # ({new-e8dfa374})
# 2 parâmetros vm.memory.size

-   **total** - memória física total.
-   **free** - memória disponível para ser requisitada por qualquer
    entidade.
-   **active** - memória atualmente em uso, ou utilizada recentemente, e
    por isso ainda está na RAM.
-   **inactive** - memória marcada como não utilizada.
-   **wired** - memória marcada para se manter em RAM. Nunca será movida
    para disco.
-   **pinned** - o mesmo que 'wired'.
-   **anon** - memória não associada a um arquivo (não pode ser
    recarregada a partir de um).
-   **exec** - código executável, normalmente a partir de um arquivo de
    programa.
-   **file** - cache contendo os arquivos acessados recentemente.
-   **buffers** - cache para coisas como o metadado de sistema.
-   **cached** - cache para coisas diversas.
-   **shared** - memória que pode ser acessada simultaneamente por
    múltiplos processos.
-   **used** - memória 'active' + 'wired'.
-   **pused** - percentual de memória 'active' + 'wired' em relação à
    'total'.
-   **available** - memória 'inactive' + 'cached' + 'free'.
-   **pavailable** - 'inactive' + 'cached' + 'free' em relação à
    'total'.

::: noteimportant
O total de *vm.memory.size\[used\]* e
*vm.memory.size\[available\]* não necessariamente será igual ao total.
Por exemplo, em FreeBSD: memória 'active, inactive, wired, cached' são
consideradas como utilizadas, pois elas podem armazenar informações
úteis.\
Da mesma forma as memórias 'inactive, cached, free' podem ser
consideradas como disponíveis, pois estes tipos de memórias podem ser
atribuídas imediatamente a processos que requeiram mais memória.\
Então a memória inativa é considerada como utilizada e como disponível
ao mesmo tempo. Por causa disso o item *vm.memory.size\[used\]* é
desenvolvido para propósitos informacionais apenas, enquanto o item
*vm.memory.size\[available\]* foi desenvolvido para ser utilizado em
triggers.\

:::

::: noteclassic
 Consulte a seção ["Veja também"](#see_also) ao final desta
página para mais detalhes sobre o cálculo de memória em diferentes
SOs.
:::

[comment]: # ({/new-e8dfa374})

[comment]: # ({new-0863fe63})
##### Notas de plataformas específicas

-   em Solaris **available** e **free** são o mesmo
-   em Linux **shared** funciona somente com o kernel

[comment]: # ({/new-0863fe63})

[comment]: # ({new-4ea4e379})
### Veja também

1.  [Informações detalhadas sobre cálculo de memória livre em diferentes
    SOs](http://blog.zabbix.com/when-alexei-isnt-looking#vm.memory.size)

[comment]: # ({/new-4ea4e379})


[comment]: # ({new-f80ad839})
### See also

1.  [Additional details about memory calculation in different
    OS](http://blog.zabbix.com/when-alexei-isnt-looking#vm.memory.size)

[comment]: # ({/new-f80ad839})
