[comment]: # translation:outdated

[comment]: # ({new-6f5dcc8c})
# 5 Suporte a grandes arquivos

O suporte a grandes arquivos, muitas vezes abreviado como LFS, é o termo
aplicado para a habilidade de trabalhar com arquivos maiores que 2 GB em
sistemas operacionais de 32-bit. Suportado desde o Zabbix 2.0. Esta
modificação afeta pelo menos: [monitoração de arquivos de
log](/pt/manual/config/items/itemtypes/log_items) e todos os [itens
vfs.file.\*](/pt/manual/config/items/itemtypes/zabbix_agent#supported_item_keys).
O suporte a grandes arquivos depende das capacidades do sistema no
momento da compilação do Zabbix, mas é completamente desativado no
Solaris 32-bit devido à incompatibilidade com 'procfs e swapctl'.

[comment]: # ({/new-6f5dcc8c})
