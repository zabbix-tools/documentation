[comment]: # translation:outdated

[comment]: # ({new-df5bbb96})
# 4 Codificação dos valores retornados

O Zabbix Server espera que qualquer texto retornado esteja codificado em
UTF8. Isso está relacionado a qualquer tipo de verificação: zabbix
agent, ssh, telnet, etc.

Diferentes sistemas, dispositivos e verificações podem retornar
caracteres não-ASCII no valor. Para estes casos, sempre que possível, as
chaves dos itens no Zabbix possuem um parâmetro adicional:
**<encoding>**. Este parâmetro é opcional mas pode definir que os
resutado não estará com a codificação UTF8 e poderá conter caracteres
não-ASCII. De outra forma, o resultado se torna imprevisível.

A seguir descrevemos o comportamento de diferentes bancos de dados
nestes casos.

[comment]: # ({/new-df5bbb96})

[comment]: # ({new-b4119079})
#### MySQL

Se um valor contêm caracteres não-ASCII e não está codificado com UTF8 -
este caractere e o restante serão descartados quando o BD armazenar o
valor. Não serão geradas mensagens de alerta no *zabbix\_server.log*.\
Relevante pelo menos para a versão 5.1.61 do MySQL.

[comment]: # ({/new-b4119079})

[comment]: # ({new-c305c3e1})
#### PostgreSQL

Se um valor contêm caracteres não-ASCII e não está codificado com UTF8 -
isso fará com que a consulta SQL falhe (PGRES\_FATAL\_ERROR:ERROR
invalid byte sequence for encoding) e o dado não será armazenado. Uma
mensagem de alerta apropriada será registrada no *zabbix\_server.log*.\
Relevante pelo menos para a versão 9.1.3 do PostgreSQL

[comment]: # ({/new-c305c3e1})
