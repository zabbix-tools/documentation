[comment]: # translation:outdated

[comment]: # ({new-0c4b8ed3})
# 10 Detalhes de implementação das verificações net.tcp.service ed net.udp.service

A implementação das verificações `net.tcp.service` e `net.udp.service` é
detalhada nesta página para os vários serviços definidos por seus
parâmetros.

[comment]: # ({/new-0c4b8ed3})

[comment]: # ({new-b8e37f71})
#### Parâmetros do item net.tcp.service

**ftp**

Cria uma conexão TCP e aguarda que os primeiros 4 caracteres respondidos
sejam "220 ", então envia "QUIT\\r\\n". O padrão é a porta 21.

**http**

Cria uma conexão TCP sem esperar ou enviar nada. O padrão é a porta 80.

**https**

Utiliza (e só funciona com) a `libcurl`, mas não verifica a
autenticidade nem o hostname no certificado, apenas recebe a resposta do
cabeçalho (HEAD request). O padrão é a porta 443 .

**imap**

Cria uma conexão TCP e aguarda que os primeiros 4 caracteres respondidos
sejam "\* OK", então envia "a1 LOGOUT\\r\\n". O padrão é a porta 143.

**ldap**

Abre uma conexão com o servidor LDAP e eecuta uma operação de pesquisa
com o filtro configurado para (objectClass=\*). Aguarda uma recuperação
com sucesso do primeiro atributo da primeira entidade. O padrão é a
porta 389 .

**nntp**

Cria uma conexão TCP e aguarda que os primeiros 3 caracteres respondidos
sejam "200" ou "201", então envia "QUIT\\r\\n". O padrão é a porta 119 .

**pop**

Cria uma conexão TCP e aguarda que os primeiros 3 caracteres respondidos
sejam "+OK", então envia "QUIT\\r\\n". O padrão é a porta 110 .

**smtp**

Cria uma conexão TCP e aguarda que os primeiros 3 caracteres respondidos
"220", seguidos de um espaço, fim de linha ou traço. As linhas contendo
um traço pertencem a uma resposta de várias linhas e a resposta será
re-lida enquanto não receber uma linha sem traço. Então envia
"QUIT\\r\\n". O padrão é a porta 25 .

**ssh**

Cria uma conexão TCP. Se a conexão for estabelecida, ambos os lados
fazem a troca de strings de identificação (SSH-major.minor-XXXX), onde a
'major' e 'minor' são as versões do protocolo e XXXX é um texto. O
Zabbix verifica se o texto que corresponde com a especificação e envia
de volta o texto "SSH-major.minor-zabbix\_agent\\r\\n" ou "0\\n". O
padrão é a porta 22 .

**tcp**

Cria uma conexão TCP sem aguardar ou enviar nada. Diferentemente das
outras verificações, requer a especificação de um número de porta.

**telnet**

Cria uma conexão TCP e aguarda um prompt de login (':' no final). O
padrão é a porta 23 .

[comment]: # ({/new-b8e37f71})

[comment]: # ({new-fec6c203})
#### Item net.udp.service parameters

**ntp**

Envia um pacote SNTP sobre UDP e valida a resposta conforme [RFC 4330,
seção 5](http://tools.ietf.org/html/rfc4330#section-5). O padrão é a
porta 123 .

[comment]: # ({/new-fec6c203})
