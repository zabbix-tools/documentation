[comment]: # translation:outdated

[comment]: # ({new-02f012f5})
# 6 Definições de host inalcançável/inacessível

[comment]: # ({/new-02f012f5})

[comment]: # ({new-b32082d6})
#### Visão geral

Diversos [parâmetros de
configuração](/pt/manual/appendix/config/zabbix_server) definem como o
Zabbix Server irá agir quando uma verificação de agente (Zabbix, SNMP,
IPMI, JMX) falhar e um host passar a se tornar inacessível.

[comment]: # ({/new-b32082d6})

[comment]: # ({new-e56dd03b})
#### Host inacessível

Um host é considerado como inacessível após uma falha de verificação
(erro de rede, 'timeout') com os agentes Zabbix, SNMP, IPMI ou JMX.
Observe que as **verificações ativas do Zabbix Agent não influenciam na
disponibilidade do host**.

A partir deste momento o parâmetro **UnreachableDelay** define quando o
host será verificado novamente usando um de seus itens (incluindo regras
de descoberta LLD) para confirmar o seu estado de inacessibilidade e
quais verificações serão refeitas pelos 'poolers' de inacessibilidade. O
padrão é de 15 segundos antes da próxima verificação.

No log do Zabbix server a inacessibilidade é indicada por mensagens como
estas:

    Zabbix agent item "system.cpu.load[percpu,avg1]" on host "New host" failed: first network error, wait for 15 seconds
    Zabbix agent item "system.cpu.load[percpu,avg15]" on host "New host" failed: another network error, wait for 15 seconds

Obseve que a chave exata de item que está falhando e o seu tipo são
indicados (Zabbix agent).

::: noteclassic
O parâmetro *Timeout* também afeta quão logo o host será
verificado novamente durante o período de inacessibilidade. Se o
*Timeout* é de 20 segundos e o *UnreachableDelay* de 30 segundos, a
próxima verificação ocorrerá 50 segundos após a primeira
tentativa.
:::

Observe que o Zabbix tenta diferenciar o 'timeout' de um item do
'timeout' do host. Se outro item for coletado com sucesso entre as duas
falhas de coleta do item problemático, o item problemático será marcado
como não suportado e, após a segunda falha, a acessibilidade do host não
será afetada.

O parâmetro **UnreachablePeriod** define a duração total da
inacessibilidade, seu valor padrão é de 45 segundos. O
*UnreachablePeriod* pode ser muitas vezes maior que o
*UnreachableDelay*, isso permitirá que um host seja re-verificado mais
vezes antes de se tornar inacessível.

Se um host inacessível retornar, o fato será registrado no log e sua
monitoração voltará ao normal:

    resuming Zabbix agent checks on host "New host": connection restored

[comment]: # ({/new-e56dd03b})

[comment]: # ({new-ba32db14})
#### Host indisponível

Se, após o período definido em *UnreachablePeriod*, o host não retornar,
ele será tratado como indisponível.

No log do Zabbix Server será registrada a ocorrência com mensagens como
esta:

    temporarily disabling Zabbix agent checks on host "New host": host unavailable

e na [interface
web](/pt/manual/web_interface/frontend_sections/configuration/hosts) o
ícone de disponibilidade do host para aquele tipo de interface será
alterado de verde (ou cinza) para vermelho (observe que ao passar com o
mouse sobre o ícone a descrição do erro será apresentada):

![](../../../../assets/en/manual/config/unavailable.png)

O parâmetro **UnavailableDelay** define a frequência com que um host
será verificado durante sua indisponibilidade.

Por padrão a cada 60 segundos (então neste caso a "inativação
temporária", registrada no log, indicará que será feita uma nova
verificação a cada minuto).

Quando a conexão com o host for restaurada a monitoração retornará ao
normal automaticamente e o fato será registrado no log também:

    enabling Zabbix agent checks on host "New host": host became available

[comment]: # ({/new-ba32db14})


[comment]: # ({new-36969217})
#### Unavailable interface

After the UnreachablePeriod ends and the interface has not reappeared,
the interface is treated as unavailable.

In the server log it is indicated by messages like these:

    temporarily disabling Zabbix agent checks on host "New host": interface unavailable

and in the
[frontend](/manual/web_interface/frontend_sections/configuration/hosts)
the host availability icon goes from green/gray to yellow/red (the
unreachable interface details can be seen in the hint box that is
displayed when a mouse is positioned on the host availability icon):

![](../../../../assets/en/manual/config/unavailable.png)

The **UnavailableDelay** parameter defines how often an interface is
checked during interface unavailability.

By default it is 60 seconds (so in this case "temporarily disabling",
from the log message above, will mean disabling checks for one minute).

When the connection to the interface is restored, the monitoring returns
to normal automatically, too:

    enabling Zabbix agent checks on host "New host": interface became available

[comment]: # ({/new-36969217})
