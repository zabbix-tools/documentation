[comment]: # translation:outdated

[comment]: # ({new-d53fbea3})
# 8 Execução de comandos

O Zabbix usa uma funcionalidade em comum para executar parâmetros de
usuários, comandos remotos, `system.run[]` sem a flag "nowait", scripts
(alerta, externos e globais) e outros comandos internos.

O comando/script é executado de forma similar nas plataformas UNIX e
Windows:

1.  Zabbix (um de seus processos) cria uma ponte para comunicação
2.  Zabbix configura a pote como a saída para o processo a ser criado
3.  Zabbix cria o processo filho (executa o comando/script)
4.  Um novo grupo de processos (no Unix) ou um 'job' (no Windows) é
    criado para os processos filhos
5.  O Zabbix lê do 'pipe' até que o 'timeout' seja alcançado ou que
    ninguém esteja mais gravando nele (Todos os
    gerenciadores/descritores de arquivo tiverem sido fechados). Observe
    que o processo filho pode criar mais processos e sair antes de ter
    saido ou fechado o descritor de arquivo.
6.  Se o tempo limite não tiver sido alcançado o Zabbix aguarda até que
    o processo saia ou que o timeout ocorra
7.  Neste ponto nós estamos assumindo que tudo foi executado com sucesso
    e toda a árvore de processos foi terminada

::: noteimportant
Os passos 5-7 não se referem a comandos remotos
executados com a flag "nowait".
:::

::: noteimportant
O Zabbix entende que o comando/script foi
concluido quando o processo inicial é finalizado E não existe outro
processo que continue gerenciando os descritores de arquivos abertos.
Quando o processamento é concluído todos os processos que foram criados
são terminados.
:::

Todas as aspas duplas e contrabarras no comando serão escapadas com
contrabarras e o comando será executado entre aspas duplas.

Leia mais sobre isso nos manuais de [parâmetros de
usuário](/pt/manual/config/items/userparameters), [comandos
remotos](/pt/manual/config/notifications/action/operation/remote_command),
[scripts de alerta](/pt/manual/config/notifications/media/script).

[comment]: # ({/new-d53fbea3})



[comment]: # ({new-a11705f0})
#### Execution steps

The command/script is executed similarly on both Unix and Windows
platforms:

1.  Zabbix (the parent process) creates a pipe for communication
2.  Zabbix sets the pipe as the output for the to-be-created child
    process
3.  Zabbix creates the child process (runs the command/script)
4.  A new process group (in Unix) or a job (in Windows) is created for
    the child process
5.  Zabbix reads from the pipe until timeout occurs or no one is writing
    to the other end (ALL handles/file descriptors have been closed).
    Note that the child process can create more processes and exit
    before they exit or close the handle/file descriptor.
6.  If the timeout has not been reached, Zabbix waits until the initial
    child process exits or timeout occurs
7.  If the initial child process exited and the timeout has not been
    reached, Zabbix checks exit code of the initial child process and
    compares it to 0 (non-zero value is considered as execution failure,
    only for custom alert scripts, remote commands and user scripts
    executed on Zabbix server and Zabbix proxy)
8.  At this point it is assumed that everything is done and the whole
    process tree (i.e. the process group or the job) is terminated

::: noteimportant
Zabbix assumes that a command/script has done
processing when the initial child process has exited AND no other
process is still keeping the output handle/file descriptor open. When
processing is done, ALL created processes are terminated.
:::

All double quotes and backslashes in the command are escaped with
backslashes and the command is enclosed in double quotes.

[comment]: # ({/new-a11705f0})

[comment]: # ({new-ddd381e9})
#### Exit code checking

Exit code are checked with the following conditions:

-   Only for custom alert scripts, remote commands and user scripts
    executed on Zabbix server and Zabbix proxy.
-   Any exit code that is different from 0 is considered as execution
    failure.
-   Contents of standard error and standard output for failed executions
    are collected and available in frontend (where execution result is
    displayed).
-   Additional log entry is created for remote commands on Zabbix server
    to save script execution output and can be enabled using
    LogRemoteCommands agent
    [parameter](/manual/appendix/config/zabbix_agentd).

Possible frontend messages and log entries for failed commands/scripts:

-   Contents of standard error and standard output for failed executions
    (if any).
-   "Process exited with code: N." (for empty output, and exit code not
    equal to 0).
-   "Process killed by signal: N." (for process terminated by a signal,
    on Linux only).
-   "Process terminated unexpectedly." (for process terminated for
    unknown reasons).

------------------------------------------------------------------------

Read more about:

-   [External
    checks](/manual/config/items/itemtypes/external#external_check_result)
-   [User parameters](/manual/config/items/userparameters)
-   [system.run](/manual/config/items/itemtypes/zabbix_agent) items
-   [Custom alert scripts](/manual/config/notifications/media/script)
-   [Remote
    commands](/manual/config/notifications/action/operation/remote_command)
-   [Global
    scripts](/manual/web_interface/frontend_sections/administration/scripts)

[comment]: # ({/new-ddd381e9})
