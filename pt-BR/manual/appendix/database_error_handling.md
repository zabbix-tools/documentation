[comment]: # translation:outdated

[comment]: # ({new-17a30eba})
# 12 Gerenciamento de erros de banco de dados

Se o Zabbix detectar que o seu banco de dados não está acessível ele
tentará enviar uma mensagem de notificação e continuará tentando se
conectar ao banco. Para algumas tecnologias de banco, códigos
específicos de erro são reconhecidos.

[comment]: # ({/new-17a30eba})

[comment]: # ({new-537463f7})
#### MySQL

-   CR\_CONN\_HOST\_ERROR
-   CR\_SERVER\_GONE\_ERROR
-   CR\_CONNECTION\_ERROR
-   CR\_SERVER\_LOST
-   CR\_UNKNOWN\_HOST
-   ER\_SERVER\_SHUTDOWN
-   ER\_ACCESS\_DENIED\_ERROR
-   ER\_ILLEGAL\_GRANT\_FOR\_TABLE
-   ER\_TABLEACCESS\_DENIED\_ERROR
-   ER\_UNKNOWN\_ERROR

[comment]: # ({/new-537463f7})
