[comment]: # translation:outdated

[comment]: # ({new-6537e63f})
# 1 Foreach functions

[comment]: # ({/new-6537e63f})

[comment]: # ({new-7d50e3a2})
#### Overview

Foreach functions return aggregate values from the history of multiple
items. Foreach functions are used in aggregate calculations.

The following foreach functions are supported:

|Function|Description|
|--------|-----------|
|*avg\_foreach*|Returns average values|
|*bucket\_rate\_foreach*|Returns pairs (bucket upper bound, rate value) suitable for use in the [histogram\_quantile()](/manual/appendix/functions/aggregate#aggregate_functions1) function, where "bucket upper bound" is the value of item key parameter defined by the <parameter number> [parameter](#additional_parameters).|
|*count\_foreach*|Returns the number of values|
|*exists\_foreach*|Returns the number of currently enabled items|
|*last\_foreach*|Returns last values|
|*max\_foreach*|Returns maximum values|
|*min\_foreach*|Returns minimum values|
|*sum\_foreach*|Returns the sum of values|

Foreach functions return an array of values - one for each item. Items
are selected by using filters in the first parameter.

See [aggregate
calculations](/manual/config/items/itemtypes/calculated/aggregate) for
more details on using foreach functions.

[comment]: # ({/new-7d50e3a2})

[comment]: # ({new-c9e1960d})

#### Supported functions

|Function|Description|
|--|--------|
|*avg_foreach*|Returns the average value for each item.|
|*bucket_rate_foreach*|Returns pairs (bucket upper bound, rate value) suitable for use in the [histogram\_quantile()](/manual/appendix/functions/aggregate#aggregate_functions1) function, where "bucket upper bound" is the value of item key parameter defined by the <parameter number> [parameter](#additional_parameters).|
|*count_foreach*|Returns the number of values for each item..|
|*exists_foreach*|Returns the number of currently enabled items.|
|*last_foreach*|Returns the last value for each item.|
|*max_foreach*|Returns the maximum value for each item.|
|*min_foreach*|Returns the minimum value for each item.|
|*sum_foreach*|Returns the sum of values for each item.|

[comment]: # ({/new-c9e1960d})

[comment]: # ({new-2cf639a9})
#### Parameters

Foreach functions support two common parameters - item filter and time
period:

    foreach_function(item filter,time period)

for example:

    avg_foreach(/*/mysql.qps?[group="MySQL Servers"],5m)

Some functions support additional parameters.

[comment]: # ({/new-2cf639a9})

[comment]: # ({new-737d2e38})
##### Item filter

The **first** parameter allows to filter the items we are interested in.
A complex filter may be used, referencing the item key, host group and
tags, as illustrated by the examples:

|Syntax example|Description|
|--------------|-----------|
|/host/key\[abc,\*\]|Matches similar items on this host.|
|/\*/key|Matches the same item of any host.|
|/\*/key?\[group="ABC" and tag="tagname:value"\]|Matches the same item of any host from the ABC group having 'tagname:value' tags.|
|/\*/key\[a,\*,c\]?\[(group="ABC" and tag="Tag1") or (group="DEF" and (tag="Tag2" or tag="Tag3:value"))\]|Matches similar items of any host from the ABC or DEF group with the respective tags.|

All referenced items must exist and collect data. Only enabled items on
enabled hosts are included in the calculations.

::: noteimportant
If the item key of a referenced item is changed,
the filter must be updated manually.
:::

Specifying a parent host group includes the parent group and all nested
host groups with their items.

[comment]: # ({/new-737d2e38})

[comment]: # ({new-0a9b9b28})
##### Time period

The **second** parameter allows to specify the time period for
aggregation. The time period can only be expressed as time, the amount
of values (prefixed with **\#**) is not supported.

[Supported unit symbols](/manual/appendix/suffixes) can be used in this
parameter for convenience, for example '5m' (five minutes) instead of
'300s' (300 seconds) or '1d' (one day) instead of '86400' (86400
seconds).

Time period is ignored by the server if passed with the *last\_foreach*
function and can thus be omitted:

    last_foreach(/*/key?[group="host group"])

Time period is not supported with the *exists\_foreach* function.

[comment]: # ({/new-0a9b9b28})

[comment]: # ({new-bf880e22})
##### Additional parameters

A third optional parameter is supported by the *bucket\_rate\_foreach*
function:

    bucket_rate_foreach(item filter,time period,<parameter number>)

where <parameter number> is the position of the "bucket" value in
the item key. For example, if the "bucket" value in myItem\[aaa,0.2\] is
'0.2', then its position is 2.

The default value of <parameter number> is '1'.

[comment]: # ({/new-bf880e22})

[comment]: # ({new-328844f6})
#### Behavior depending on availability

The following table illustrates how each function behaves in cases of limited availability of host/item and history data.

|Function|Disabled host|Unavailable host with data|Unavailable host without data|Disabled item|Unsupported item|Data retrieval error (SQL)|
|--|--|--|--|--|--|--|
|*avg_foreach*|ignore|return avg|ignore|ignore|ignore|ignore|
|*bucket_rate_foreach*|ignore|return bucket rate|ignore|ignore|ignore|ignore|
|*count_foreach*|ignore|return count|0|ignore|ignore|ignore|
|*exists_foreach*|ignore|1|1|ignore|1|n/a|
|*last_foreach*|ignore|return last|ignore|ignore|ignore|ignore|
|*max_foreach*|ignore|return max|ignore|ignore|ignore|ignore|
|*min_foreach*|ignore|return min|ignore|ignore|ignore|ignore|
|*sum_foreach*|ignore|return sum|ignore|ignore|ignore|ignore|

If the item is *ignored*, nothing is added to the aggregation.

[comment]: # ({/new-328844f6})
