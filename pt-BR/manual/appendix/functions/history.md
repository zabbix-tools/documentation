[comment]: # attributes: notoc

[comment]: # translation:outdated

[comment]: # ({new-7da5ea1c})
# 4 History functions

All functions listed here are supported in:

-   [Trigger expressions](/manual/config/triggers/expression)
-   [Calculated items](/manual/config/items/itemtypes/calculated)

Some general notes on function parameters:

-   Function parameters are separated by a comma
-   Optional function parameters (or parameter parts) are indicated by
    `<` `>`
-   Function-specific parameters are described with each function
-   `/host/key` and `(sec|#num)<:time shift>` parameters must never be
    quoted

[comment]: # ({/new-7da5ea1c})

[comment]: # ({new-4d343eb8})
##### Common parameters

-   `/host/key` is a common mandatory first parameter for the functions
    referencing the host item history
-   `(sec|#num)<:time shift>` is a common second parameter for the
    functions referencing the host item history, where:
    -   **sec** - maximum [evaluation
        period](/manual/config/triggers#evaluation_period) in seconds
        (time [suffixes](/manual/appendix/suffixes) can be used), or
    -   **\#num** - maximum [evaluation
        range](/manual/config/triggers#evaluation_period) in latest
        collected values (if preceded by a hash mark)
    -   **time shift** (optional) allows to move the evaluation point
        back in time. See [more
        details](/manual/config/triggers/expression#time_shift) on
        specifying time shift.

[comment]: # ({/new-4d343eb8})

[comment]: # ({new-8f3a0028})
### Function details

Some general notes on function parameters:

-   Function parameters are separated by a comma
-   Optional function parameters (or parameter parts) are indicated by
    `<` `>`
-   Function-specific parameters are described with each function
-   `/host/key` and `(sec|#num)<:time shift>` parameters must never be
    quoted

[comment]: # ({/new-8f3a0028})

[comment]: # ({new-4afc9a02})

##### change(/host/key) {#change}

The amount of difference between the previous and latest value.<br>
Supported value types: *Float*, *Integer*, *String*, *Text*, *Log*.<br>
For strings returns: 0 - values are equal; 1 - values differ.

Parameters: see [common parameters](#common-parameters).

Comments:

-   Numeric difference will be calculated, as seen with these incoming example values ('previous' and 'latest' value = difference):<br>'1' and '5' = `+4`<br>'3' and '1' = `-2`<br>'0' and '-2.5' = `-2.5`<br>
-   See also: [abs](/manual/appendix/functions/math) for comparison.

Examples:

    change(/host/key)>10

[comment]: # ({/new-4afc9a02})

[comment]: # ({new-57458a90})

##### changecount(/host/key,(sec|#num)<:time shift>,<mode>) {#changecount}

The number of changes between adjacent values within the defined evaluation period.<br>
Supported value types: *Float*, *Integer*, *String*, *Text*, *Log*.

Parameters: 

-   See [common parameters](#common-parameters);<br>
-   **mode** (must be double-quoted) - possible values: *all* - count all changes (default); *dec* - count decreases; *inc* - count increases

For non-numeric value types, the *mode* parameter is ignored.

Examples:

    changecount(/host/key,1w) #the number of value changes for the last week until now
    changecount(/host/key,#10,"inc") #the number of value increases (relative to the adjacent value) among the last 10 values
    changecount(/host/key,24h,"dec") #the number of value decreases (relative to the adjacent value) for the last 24 hours until now

[comment]: # ({/new-57458a90})

[comment]: # ({new-f9ce5dd9})

##### count(/host/key,(sec|#num)<:time shift>,<operator>,<pattern>) {#count}

The number of values within the defined evaluation period.<br>
Supported value types: *Float*, *Integer*, *String*, *Text*, *Log*.

Parameters: 

-   See [common parameters](#common-parameters);<br>
-   **operator** (must be double-quoted). Supported `operators`:<br>*eq* - equal (default)<br>*ne* - not equal<br>*gt* - greater<br>*ge* - greater or equal<br>*lt* - less<br>*le* - less or equal<br>*like* - matches if contains pattern (case-sensitive)<br>*bitand* - bitwise AND<br>*regexp* - case-sensitive match of the regular expression given in `pattern`<br>*iregexp* - case-insensitive match of the regular expression given in `pattern`<br>
-   **pattern** - the required pattern (string arguments must be double-quoted).

Comments:

-   Float items match with the precision of 2.22e-16; if database is [not upgraded](/manual/appendix/install/db_float_range) the precision is 0.000001.
-   With *bitand* as the third parameter, the fourth `pattern` parameter can be specified as two numbers, separated by '/': **number_to_compare_with/mask**. count() calculates "bitwise AND" from the value and the *mask* and compares the result to *number_to_compare_with*. If the result of "bitwise AND" is equal to *number_to_compare_with*, the value is counted.<br>If *number_to_compare_with* and *mask* are equal, only the *mask* need be specified (without '/').
-   With *regexp* or *iregexp* as the third parameter, the fourth `pattern` parameter can be an ordinary or [global](/manual/regular_expressions#global_regular_expressions) (starting with '@') regular expression. In case of global regular expressions case sensitivity is inherited from global regular expression settings. For the purpose of regexp matching, float values will always be represented with 4 decimal digits after '.'. Also note that for large numbers difference in decimal (stored in database) and binary (used by Zabbix server) representation may affect the 4th decimal digit.

Examples:

    count(/host/key,10m) #the values for the last 10 minutes until now
    count(/host/key,10m,"like","error") #the number of values for the last 10 minutes until now that contain 'error'
    count(/host/key,10m,,12) #the number of values for the last 10 minutes until now that equal '12'
    count(/host/key,10m,"gt",12) #the number of values for the last 10 minutes until now that are over '12'
    count(/host/key,#10,"gt",12) #the number of values within the last 10 values until now that are over '12'
    count(/host/key,10m:now-1d,"gt",12) #the number of values between 24 hours and 10 minutes and 24 hours ago from now that were over '12'
    count(/host/key,10m,"bitand","6/7") #the number of values for the last 10 minutes until now having '110' (in binary) in the 3 least significant bits
    count(/host/key,10m:now-1d) #the number of values between 24 hours and 10 minutes and 24 hours ago from now

[comment]: # ({/new-f9ce5dd9})

[comment]: # ({new-bf8dc20c})

##### countunique(/host/key,(sec|#num)<:time shift>,<operator>,<pattern>) {#countunique}

The number of unique values within the defined evaluation period.<br>
Supported value types: *Float*, *Integer*, *String*, *Text*, *Log*.

Parameters: 

-   See [common parameters](#common-parameters);<br>
-   **operator** (must be double-quoted). Supported `operators`:<br>*eq* - equal (default)<br>*ne* - not equal<br>*gt* - greater<br>*ge* - greater or equal<br>*lt* - less<br>*le* - less or equal<br>*like* - matches if contains pattern (case-sensitive)<br>*bitand* - bitwise AND<br>*regexp* - case-sensitive match of the regular expression given in `pattern`<br>*iregexp* - case-insensitive match of the regular expression given in `pattern`<br>
-   **pattern** - the required pattern (string arguments must be double-quoted).

Comments:

-   Float items match with the precision of 2.22e-16; if database is [not upgraded](/manual/appendix/install/db_float_range) the precision is 0.000001.
-   With *bitand* as the third parameter, the fourth `pattern` parameter can be specified as two numbers, separated by '/': **number_to_compare_with/mask**. countunique() calculates "bitwise AND" from the value and the *mask* and compares the result to *number_to_compare_with*. If the result of "bitwise AND" is equal to *number_to_compare_with*, the value is counted.<br>If *number_to_compare_with* and *mask* are equal, only the *mask* need be specified (without '/').
-   With *regexp* or *iregexp* as the third parameter, the fourth `pattern` parameter can be an ordinary or [global](/manual/regular_expressions#global_regular_expressions) (starting with '@') regular expression. In case of global regular expressions case sensitivity is inherited from global regular expression settings. For the purpose of regexp matching, float values will always be represented with 4 decimal digits after '.'. Also note that for large numbers difference in decimal (stored in database) and binary (used by Zabbix server) representation may affect the 4th decimal digit.

Examples:

    countunique(/host/key,10m) #the number of unique values for the last 10 minutes until now
    countunique(/host/key,10m,"like","error") #the number of unique values for the last 10 minutes until now that contain 'error'
    countunique(/host/key,10m,,12) #the number of unique values for the last 10 minutes until now that equal '12'
    countunique(/host/key,10m,"gt",12) #the number of unique values for the last 10 minutes until now that are over '12'
    countunique(/host/key,#10,"gt",12) #the number of unique values within the last 10 values until now that are over '12'
    countunique(/host/key,10m:now-1d,"gt",12) #the number of unique values between 24 hours and 10 minutes and 24 hours ago from now that were over '12'
    countunique(/host/key,10m,"bitand","6/7") #the number of unique values for the last 10 minutes until now having '110' (in binary) in the 3 least significant bits
    countunique(/host/key,10m:now-1d) #the number of unique values between 24 hours and 10 minutes and 24 hours ago from now

[comment]: # ({/new-bf8dc20c})

[comment]: # ({new-148d7d16})

##### find(/host/key,(sec|#num)<:time shift>,<operator>,<pattern>) {#count}

Find a value match within the defined evaluation period.<br>
Supported value types: *Float*, *Integer*, *String*, *Text*, *Log*.<br>
Returns: 1 - found; 0 - otherwise.

Parameters: 

-   See [common parameters](#common-parameters);<br>
-   **sec** or **#num** (optional) - defaults to the latest value if not specified
-   **operator** (must be double-quoted). Supported `operators`:<br>*eq* - equal (default)<br>*ne* - not equal<br>*gt* - greater<br>*ge* - greater or equal<br>*lt* - less<br>*le* - less or equal<br>*like* - matches if contains the string given in `pattern` (case-sensitive)<br>*bitand* - bitwise AND<br>*regexp* - case-sensitive match of the regular expression given in `pattern`<br>*iregexp* - case-insensitive match of the regular expression given in `pattern`<br>
-   **pattern** - the required pattern (string arguments must be double-quoted); [Perl Compatible Regular Expression](https://en.wikipedia.org/wiki/Perl_Compatible_Regular_Expressions) (PCRE) regular expression if `operator` is *regexp*, *iregexp*.

Comments:

-   If more than one value is processed, '1' is returned if there is at least one matching value.
-   With *regexp* or *iregexp* as the third parameter, the fourth `pattern` parameter can be an ordinary or [global](/manual/regular_expressions#global_regular_expressions) (starting with '@') regular expression. In case of global regular expressions case sensitivity is inherited from the global regular expression settings.

Example:

    find(/host/key,10m,"like","error") #find a value that contains 'error' within the last 10 minutes until now

[comment]: # ({/new-148d7d16})

[comment]: # ({new-0ae5f587})

##### first(/host/key,sec<:time shift>) {#first}

The first (the oldest) value within the defined evaluation period.<br>
Supported value types: *Float*, *Integer*, *String*, *Text*, *Log*.

Parameters: 

-   See [common parameters](#common-parameters).

See also [last()](#last).

Example:

    first(/host/key,1h) #retrieve the oldest value within the last hour until now

[comment]: # ({/new-0ae5f587})

[comment]: # ({new-1c3609f8})

##### fuzzytime(/host/key,sec) {#fuzzytime}

Check how much the passive agent time differs from the Zabbix server/proxy time.<br>
Supported value types: *Float*, *Integer*.<br>
Returns: 1 - difference between the passive item value (as timestamp) and Zabbix server/proxy timestamp (the clock of value collection) is less than or equal to T seconds; 0 - otherwise.

Parameters: 

-   See [common parameters](#common-parameters).

Comments:

-   Usually used with the 'system.localtime' item to check that local time is in sync with the local time of Zabbix server. *Note* that 'system.localtime' must be configured as a [passive check](/manual/appendix/items/activepassive#passive_checks).
-   Can be used also with the `vfs.file.time[/path/file,modify]` key to check that the file did not get updates for long time;
-   This function is not recommended for use in complex trigger expressions (with multiple items involved), because it may cause unexpected results (time difference will be measured with the most recent metric), e.g. in `fuzzytime(/Host/system.localtime,60s)=0 or last(/Host/trap)<>0`.

Example:

    fuzzytime(/host/key,60s)=0 #detect a problem if the time difference is over 60 seconds<br><br>

[comment]: # ({/new-1c3609f8})

[comment]: # ({new-8059d345})

##### last(/host/key,<#num<:time shift>>) {#last}

The most recent value.<br>
Supported value types: *Float*, *Integer*, *String*, *Text*, *Log*.

Parameters: 

-   See [common parameters](#common-parameters);<br>
-   **#num** (optional) - the Nth most recent value.

Comments:

-   Take note that a hash-tagged time period (#N) works differently here than with many other functions. For example: `last()` is always equal to `last(#1)`; `last(#3)` - the third most recent value (*not* three latest values);
-   Zabbix does not guarantee the exact order of values if more than two values exist within one second in history;
-   See also [first()](#first).

Example:

    last(/host/key) #retrieve the last value
    last(/host/key,#2) #retrieve the previous value
    last(/host/key,#1) <> last(/host/key,#2) #the last and previous values differ

[comment]: # ({/new-8059d345})

[comment]: # ({new-ff5dcd5e})

##### logeventid(/host/key,<#num<:time shift>>,<pattern>) {#logeventid}

Check if the event ID of the last log entry matches a regular expression.<br>
Supported value types: *Log*.<br>
Returns: 0 - does not match; 1 - matches.

Parameters: 

-   See [common parameters](#common-parameters);<br>
-   **#num** (optional) - the Nth most recent value;<br>
-   **pattern** (optional) - the regular expression describing the required pattern, [Perl Compatible Regular Expression](https://en.wikipedia.org/wiki/Perl_Compatible_Regular_Expressions) (PCRE) style (string arguments must be double-quoted).

[comment]: # ({/new-ff5dcd5e})

[comment]: # ({new-5053b990})

##### logseverity(/host/key,<#num<:time shift>>) {#logseverity}

Log severity of the last log entry.<br>
Supported value types: *Log*.<br>
Returns: 0 - default severity; N - severity (integer, useful for Windows event logs: 1 - Information, 2 - Warning, 4 - Error, 7 - Failure Audit, 8 - Success Audit, 9 - Critical, 10 - Verbose).

Parameters: 

-   See [common parameters](#common-parameters);<br>
-   **#num** (optional) - the Nth most recent value.

Zabbix takes log severity from the **Information** field of Windows event log.

[comment]: # ({/new-5053b990})

[comment]: # ({new-a9148f5c})

##### logsource(/host/key,<#num<:time shift>>,<pattern>) {#logsource}

Check if log source of the last log entry matches a regular expression.<br>
Supported value types: *Log*.<br>
Returns: 0 - does not match; 1 - matches.

Parameters: 

-   See [common parameters](#common-parameters);<br>
-   **#num** (optional) - the Nth most recent value;<br>
-   **pattern** (optional) - the regular expression describing the required pattern, [Perl Compatible Regular Expression](https://en.wikipedia.org/wiki/Perl_Compatible_Regular_Expressions) (PCRE) style (string arguments must be double-quoted).

Normally used for Windows event logs.

Example:

    logsource(/host/key,,"VMware Server")

[comment]: # ({/new-a9148f5c})

[comment]: # ({new-6a63b5c8})

##### monodec(/host/key,(sec|#num)<:time shift>,<mode>) {#monodec}

Check if there has been a monotonous decrease in values.<br>
Supported value types: *Integer*.<br>
Returns: 1 - if all elements in the time period continuously decrease; 0 - otherwise.

Parameters: 

-   See [common parameters](#common-parameters);<br>
-   **mode** (must be double-quoted) - *weak* (every value is smaller or the same as the previous one; default) or *strict* (every value has decreased).

Example:

    monodec(/Host1/system.swap.size[all,free],60s) + monodec(/Host2/system.swap.size[all,free],60s) + monodec(/Host3/system.swap.size[all,free],60s) #calculate in how many hosts there has been a decrease in free swap size

[comment]: # ({/new-6a63b5c8})

[comment]: # ({new-755c1821})

##### monoinc(/host/key,(sec|#num)<:time shift>,<mode>) {#monoinc}

Check if there has been a monotonous increase in values.<br>
Supported value types: *Integer*.<br>
Returns: 1 - if all elements in the time period continuously increase; 0 - otherwise.

Parameters: 

-   See [common parameters](#common-parameters);<br>
-   **mode** (must be double-quoted) - *weak* (every value is bigger or the same as the previous one; default) or *strict* (every value has increased).

Example:

    monoinc(/Host1/system.localtime,#3,"strict")=0 #check if the system local time has been increasing consistently

[comment]: # ({/new-755c1821})

[comment]: # ({new-178e3950})

##### nodata(/host/key,sec,<mode>) {#nodata}

Check for no data received.<br>
Supported value types: *Integer*, *Float*, *Character*, *Text*, *Log*.<br>
Returns: 1 - if no data received during the defined period of time; 0 - otherwise.

Parameters: 

-   See [common parameters](#common-parameters);<br>
-   **sec** - the period should not be less than 30 seconds because the history syncer process calculates this function only every 30 seconds; `nodata(/host/key,0)` is disallowed.
-   **mode** - if set to *strict* (double-quoted), this function will be insensitive to proxy availability (see comments for details).

Comments:

-   the 'nodata' triggers monitored by proxy are, by default, sensitive to proxy availability - if proxy becomes unavailable, the 'nodata' triggers will not fire immediately after a restored connection, but will skip the data for the delayed period. Note that for passive proxies suppression is activated if connection is restored more than 15 seconds and no less than 2 & ProxyUpdateFrequency seconds later. For active proxies suppression is activated if connection is restored more than 15 seconds later. To turn off sensitiveness to proxy availability, use the third parameter, e.g.: `nodata(/host/key,5m,"strict")`; in this case the function will fire as soon as the evaluation period (five minutes) without data has past.<br>
-   This function will display an error if, within the period of the 1st parameter:<br>- there's no data and Zabbix server was restarted<br>- there's no data and maintenance was completed<br>- there's no data and the item was added or re-enabled<br>
-   Errors are displayed in the *Info* column in trigger [configuration](/manual/web_interface/frontend_sections/data_collection/hosts/triggers);<br>
-   This function may not work properly if there are time differences between Zabbix server, proxy and agent. See also: [Time synchronization requirement](/manual/installation/requirements#time_synchronization).

[comment]: # ({/new-178e3950})

[comment]: # ({new-e8041c00})

##### percentile(/host/key,(sec|#num)<:time shift>,percentage) {#percentile}

The P-th percentile of a period, where P (percentage) is specified by the third parameter.<br>
Supported value types: *Float*, *Integer*.

Parameters: 

-   See [common parameters](#common-parameters);<br>
-   **percentage** - a floating-point number between 0 and 100 (inclusive) with up to 4 digits after the decimal point.

[comment]: # ({/new-e8041c00})

[comment]: # ({new-3b1de601})

##### rate(/host/key,sec<:time shift>) {#rate}

The per-second average rate of the increase in a monotonically increasing counter within the defined time period.<br>
Supported value types: *Float*, *Integer*.

Parameters: 

-   See [common parameters](#common-parameters).

Functionally corresponds to '[rate](https://prometheus.io/docs/prometheus/latest/querying/functions/#rate)' of PromQL.

Example:

    rate(/host/key,30s) #if the monotonic increase over 30 seconds is 20, this function will return 0.67.

[comment]: # ({/new-3b1de601})

[comment]: # ({new-ce01d749})

#### Funções do histórico

|FUNÇÃO|<|<|<|
|--------|-|-|-|
|<|**Descrição**|**Parâmetro especifico - função**|**Comentários**|
|**baselinedev** (/host/key,data period:time shift,season_unit,num_seasons)|<|<|<|
|<|Retorna o número de desvios (pelo algoritmo stddevpop) entre o último período de dados e os mesmos períodos de dados nas temporadas anteriores |**data period** -o período de coleta de dados dentro de uma estação, definido como \<N>\<time unit> onde<br>`N` - número de unidades de tempo<br>`time unit` - h (hora ), d (dia), w (semana), M (mês) ou y (ano), deve ser igual ou menor que a estação <br><br>[Time shift](/manual/config/triggers/expression#function-specific_parameters) - o deslocamento do período de tempo (veja exemplos)<br><br> **season_unit** - duração de uma temporada (h, d, w, M, y), não pode ser menor que o período de dados <br><br>**num_seasons** - número de temporadas a avaliar| Exemplos:<br>=> **baselinedev**(/host/key,1d:now/d,"M",6) →  calcular os números de desvios padrão (população) entre o dia anterior e o mesmo dia nos 6 meses anteriores. Caso a data não exista em um mês anterior, será utilizado o último dia do mês (31/jul será analisado contra 31/jan, 28/fev,... 30/junho). <br> => **baselinedev**(/host/key,1h:now/h,"d",10)  →calculando o número de desvios padrão (população) entre a hora anterior e as mesmas horas no período de dez dias antes de ontem.   |
|**baselinewma** (/host/key,data period:time shift,season_unit,num_seasons)|<|<|<|
|<|Calcula a linha de base calculando a média dos dados do mesmo período de tempo em vários períodos iguais ('estações') usando o algoritmo de média móvel ponderada.|**data period** - o período de coleta de dados dentro de uma temporada, definido como \< N>\<unidade de tempo> onde<br>`N` - número de unidades de tempo<br>`unidade de tempo` - h (hora), d (dia), w (semana), M (mês) ou y (ano ), deve ser igual ou menor que a temporada<br><br>[Time shift](/manual/config/triggers/expression#function-specific_parameters) -o deslocamento do período de tempo, define o fim do período de coleta de dados em temporadas (veja exemplos)<br><br> **season_unit** - duração de uma temporada (h, d, w, M, y), não pode ser menor que o período de dados<br><br>**num_seasons** -número de temporadas a avaliar| Exemplos:<br>=> **baselinewma**(/host/key,1h:now/h,"d",3) →  cálculo da linha de base com base na última hora completa dentro de um período de 3 dias que terminou ontem. Se "agora" for 13:30 de segunda-feira, os dados de 12:00-12:59 de sexta, sábado e domingo serão analisados. <br>=> **baselinemwa**(/host/key,2h:now/h,"d",3) →  calcular a linha de base com base nas últimas duas horas em um período de 3 dias que terminou ontem. Se "agora" for segunda-feira 13h30, os dados para 10h-11h59 de sexta, sábado e domingo serão analisados.  <br>=> **baselinewma**(/host/key,1d:now/d,"M",4) → cálculo da linha de base com base nos últimos dias dos 4 meses anteriores, excluindo o último mês completo. Se hoje for 1º de setembro, serão analisados os dados de 31 de julho, 30 de junho, 31 de maio, 30 de abril.|
|**change** (/host/key)|<|<|<|
|<|A quantidade de diferença entre o valor anterior e o mais recente.|<|Tipos de valor suportados: float, int, str, text, log<br><br>Para strings retorna:<br>0 - os valores são iguais<br>1 - os valores diferem<br><br>Exemplo:<br>=> **alterar**(/host/key)>10<br><br>Numeric difference will be calculated, as seen with these incoming example values ('previous' and 'latest' value = difference):<br>'1' and '5' = `+4`<br>'3' and '1' = `-2`<br>'0' and '-2.5' = `-2.5`<br><br>See also: [abs](/manual/appendix/functions/math) for comparison|
|**changecount** (/host/key,(sec\|\#num)<:time shift>,<mode>)|<|<|<|
|<|Número de alterações entre valores adjacentes dentro do período de avaliação definido.|Consulte [Parâmetros comuns](#common-parameters).<br><br>**mode** (opcional; deve estar entre aspas duplas)<br><br>Modos compatíveis:<br>*all* - conta todas as alterações (padrão)<br>*dec* - contagem diminui<br>*inc* - contagem aumenta|Tipos de valor suportados: float, int, str, text, log<br><br>Para tipos de valor não numéricos, o parâmetro *mode* é ignorado.<br><br>Exemplos:<br>=> ** changecount**(/host/key, 1w) → número de mudanças de valor para a última semana até [agora](/manual/config/triggers#evaluation_period)<br>=> **changecount**(/host/key,\#10,"inc") → número de aumentos de valor (em relação ao valor adjacente) entre os últimos 10 valores<br>=> **changecount**(/host/key,24h,"dec") →número de valores diminui (em relação ao valor adjacente) nas últimas 24 horas até [agora](/manual/config/triggers#evaluation_period)|
|**count** (/host/key,(sec\|\#num)<:time shift>,<operator>,<pattern>)|<|<|<|
|<|Número de valores dentro do período de avaliação definido.|Veja[parâmetros comuns](#common-parameters).<br><br>**operator** (opcional; deve estar entre aspas duplas)<br><br>Operadores suportados:<br>*eq* - igual (padrão)<br>*ne* - diferente<br>*gt* - maior<br >*ge* - maior ou igual<br>*lt* - menor<br>*le* - menor ou igual<br>*como* - corresponde se contiver padrão (diferencia maiúsculas de minúsculas)<br>*bitand* - bit a bit AND<br>*regexp* - correspondência com distinção entre maiúsculas e minúsculas da expressão regular fornecida em `pattern`<br>*iregexp* - correspondência sem distinção entre maiúsculas e minúsculas de a expressão regular fornecida em `pattern`<br><br>**pattern** (opcional) - padrão obrigatório (argumentos de string devem estar entre aspas duplas)|Tipos de valor suportados: float, integer, string, text, log<br ><br>Os itens flutuantes correspondem com a precisão de 2.22e-16; se o banco de dados for [não atualizado](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500#enabling_extended_range_of_numeric_float_values) a precisão é de  0.000001.<br><br>Com *bitand* como terceiro parâmetro, o quarto parâmetro `pattern` pode ser especificado como dois números, separados por '/': **number\_to\_compare\_with/mask**. count() calcula "and bit a bit" a partir do valor e da *mask* e compara o resultado com *number\_to\_compare\_with*. Se o resultado de "bit a bit AND" for igual a *number\_to\_compare\_with*, o valor será contado.<br>Se *number\_to\_compare\_with* e *mask* forem iguais, somente a *mask * precisa ser especificado (sem '/').<br><br>Com *regexp* ou *iregexp* como o terceiro parâmetro, o quarto parâmetro `pattern` pode ser comum ou [global](/manual/regular_expressions#global_regular_expressions) (começando com '@') expressão regular. No caso de expressões regulares globais, a distinção entre maiúsculas e minúsculas é herdada das configurações de expressões regulares globais. Para fins de correspondência regexp, os valores float sempre serão representados com 4 dígitos decimais após '.'. Observe também que, para números grandes, a diferença na representação decimal (armazenada no banco de dados) e binária (usada pelo servidor Zabbix) pode afetar o 4º dígito decimal.<br><br>Exemplos:<br>=> **count**(/ host/key,**10m**) → número de valores nos últimos 10 minutos até [agora](/manual/config/triggers#evaluation_period)<br>=> **count**(/host/key,**10m**,"like","error") → number of values for the last 10 minutes until [now](/manual/config/triggers#evaluation_period) que contém 'error'<br>=> **count**(/host/key,**10m**,,12) → número de valores nos últimos 10 minutos até[agora](/manual/config/triggers#evaluation_period) é igual a '12'<br>=> **count**(/host/key,**10m**,"gt",12) → number of values for the last 10 minutes until [now](/manual/config/triggers#evaluation_period) que acabaram '12'<br>=> **count**(/host/key,**\#10**,"gt",12) → número de valores dentro dos últimos 10 valores até [agora](/manual/config/triggers#evaluation_period) que acabaram '12'<br>=> **count**(/host/key,**10m:now-1d**,"gt",12) → número de valores entre 24 horas e 10 minutos e 24 horas atrás de[agora](/manual/config/triggers#evaluation_period) isso acabou'12'<br>=> **count**(/host/key,**10m**,"bitand","6/7") → número de valores nos últimos 10 minutos até [agora](/manual/config/triggers#evaluation_period)tendo '110' (em binário) nos 3 bits menos significativos.<br>=> **count**(/host/key,**10m:now-1d**) → número de valores entre 24 horas e 10 minutos e 24 horas atrás de [agora](/manual/config/triggers#evaluation_period)|
|**countunique** (/host/key,(sec\|\#num)<:time shift>,<operator>,<pattern>)|<|<|<|
|<|Número de valores únicos dentro do período de avaliação definido.|Consulte [parâmetros comuns](#common-parameters).<br><br>**operator** (opcional; deve estar entre aspas duplas)<br><br>Operadores suportados:<br>*eq* - igual (padrão)<br>*ne* - diferente<br>*gt* - maior<br>*ge* - maior ou igual<br>*lt* - menor<br>*le* - menor ou igual<br>*like* - corresponde se contiver padrão ( diferencia maiúsculas de minúsculas)<br>*bitand* - bit a bit AND<br>*regexp* - correspondência com distinção entre maiúsculas e minúsculas da expressão regular fornecida em `pattern`<br>*iregexp* - correspondência sem distinção entre maiúsculas e minúsculas da expressão regular fornecida em `pattern`<br><br>**pattern** (opcional) - padrão obrigatório (os argumentos de string devem estar entre aspas duplas)|Tipos de valor suportados: float, integer, string, text, log<br><br>Float os itens correspondem com a precisão de 2,22e-16; se o banco de dados for [não atualizado](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500#enabling_extended_range_of_numeric_float_values) a precisão é 0,000001.<br><br>Com *bitand* como terceiro parâmetro, o quarto parâmetro `pattern` pode ser especificado como dois números, separados por '/': **number\_to\_compare\_with/mask **. count() calcula "and bit a bit" a partir do valor e da *mask* e compara o resultado com *number\_to\_compare\_with*. Se o resultado de "bit a bit AND" for igual a *number\_to\_compare\_with*, o valor será contado.<br>Se *number\_to\_compare\_with* e *mask* forem iguais, somente a *mask * precisa ser especificado (sem '/').<br><br>Com *regexp* ou *iregexp* como o terceiro parâmetro, o quarto parâmetro `pattern` pode ser comum ou [global](/manual/regular_expressions#global_regular_expressions) (começando com '@') expressão regular. No caso de expressões regulares globais, a distinção entre maiúsculas e minúsculas é herdada das configurações de expressões regulares globais. Para fins de correspondência regexp, os valores float sempre serão representados com 4 dígitos decimais após '.'. Observe também que, para números grandes, a diferença na representação decimal (armazenada no banco de dados) e binária (usada pelo servidor Zabbix) pode afetar o 4º dígito decimal.<br><br>Exemplos:<br>=> **countunique**(/ host/key,**10m**) → número de valores únicos nos últimos 10 minutos até [agora](/manual/config/triggers#evaluation_period)<br>=> **countunique**(/host/key, **10m**,"like","error") → número de valores únicos nos últimos 10 minutos até [agora](/manual/config/triggers#evaluation_period) que contêm 'error'<br>=> ** countunique**(/host/key,**10m**,"gt",12) → número de valores únicos dos últimos 10 minutos até [agora](/manual/config/triggers#evaluation_period) que estão acima de '12 '<br>=> **countunique**(/host/key,**\#10**,"gt",12) → número de valores únicos nos últimos 10 valores até [agora](/manual/config /triggers#evaluation_period) com mais de '12'<br>=> **countunique**(/host/key,**10m:now-1d**,"gt",12) → número de valores únicos entre 24 horas e 10 minutos e 24 horas atrás de [agora](/man ual/config/triggers#evaluation_period) com mais de '12'<br>=> **countunique**(/host/key,**10m**,"bitand","6/7") → número de valores dos últimos 10 minutos até [agora](/manual/config/triggers#evaluation_period) com '110' (em binário) nos 3 bits menos significativos.<br>=> **countunique**(/host/key ,**10m:agora-1d**) → número de valores únicos entre 24 horas e 10 minutos e 24 horas atrás de [agora](/manual/config/triggers#evaluation_period)|
|**encontrar** (/host/key,<(sec\|\#num)<:time shift>>,<operator>,<pattern>)|<|<|<|
|<|Encontre uma correspondência de valor.|Consulte [parâmetros comuns](#common-parameters).<br><br>**sec** ou **\#num** (opcional) - o padrão é o valor mais recente se não especificado<br><br>**operador** (opcional; deve estar entre aspas duplas)<br><br>"operadores" suportados:<br>*eq* - igual (padrão)<br>*ne* - diferente<br>*gt* - maior<br>*ge* - maior ou igual<br>*lt* - menor<br>*le* - menor ou igual<br>*like* - valor contém a string dado em `pattern` (diferencia maiúsculas de minúsculas)<br>*bitand* - bit a bit AND<br>*regexp* - correspondência com distinção entre maiúsculas e minúsculas da expressão regular fornecida em `pattern`<br>*iregexp* - correspondência sem distinção entre maiúsculas e minúsculas da expressão regular dada em `pattern`<br><br>**pattern** - padrão obrigatório (argumentos de string devem estar entre aspas); [Expressão regular compatível com Perl](https://en.wikipedia.org/wiki/Perl_Compatible_Regular_Expressions) (PCRE) expressão regular se `operator` for *regexp*, *iregexp*.|Tipos de valor suportados: float, int, str, text, log<br><br>Retorna:<br>1 - encontrado<br>0 - caso contrário<br><br>Se mais de um valor for processado, '1' será retornado se houver pelo menos um valor correspondente.<br><br>Com *regexp* ou *iregexp* como o terceiro parâmetro, o o quarto parâmetro `pattern` pode ser uma expressão regular comum ou [global](/manual/regular_expressions#global_regular_expressions) (começando com '@'). No caso de expressões regulares globais, a distinção entre maiúsculas e minúsculas é herdada das configurações de expressões regulares globais.<br><br>Exemplo:<br>=> **find**(/host/key,**10m**,"like", "erro") → encontre um valor que contenha 'erro' nos últimos 10 minutos até [agora](/manual/config/triggers#evaluation_period)|
|**primeiro** (/host/key,sec<:time shift>)|<|<|<|
|<|O primeiro (o mais antigo) valor dentro do período de avaliação definido.|Consulte [parâmetros comuns](#common-parameters).|Tipos de valor suportados: float, int, str, text, log<br><br>Exemplo :<br>=> **first**(/host/key,**1h**) → recuperar o valor mais antigo na última hora até [agora](/manual/config/triggers#evaluation_period)<br>< br>Veja também last().|
|**fuzzytime** (/host/chave,seg)|<|<|<|
|<|Verificando o quanto o tempo do agente passivo difere do tempo do servidor/proxy Zabbix.|Consulte [common-parameters](#common parameters).|Tipos de valor suportados: float, int<br><br>Retorna:<br>1 - diferença entre o valor do item passivo (como carimbo de data/hora) e o carimbo de data/hora do servidor/proxy Zabbix é menor ou igual a T segundos<br>0 - caso contrário<br><br>Geralmente usado com o item 'system.localtime' para verificar esse local a hora está sincronizada com a hora local do servidor Zabbix. *Observe* que 'system.localtime' deve ser configurado como uma [verificação passiva](/manual/appendix/items/activepassive#passive_checks).<br>Pode ser usado também com vfs.file.time\[/path/file ,modify\] para verificar se o arquivo não recebeu atualizações por muito tempo.<br><br>Exemplo:<br>=> **fuzzytime**(/host/key,**60s**)=0 → detectar um problema se a diferença de tempo for superior a 60 segundos<br><br>Esta função não é recomendada para uso em expressões de gatilho complexas (com vários itens envolvidos), pois pode causar resultados inesperados (a diferença de tempo será medida com o métrica mais recente), por exemplo em `fuzzytime(/Host/system.localtime,60s)=0 ou last(/Host/trap)<>0`|
|**último** (/host/key,<\#num<:time shift>>)|<|<|<|
|<|O valor mais recente.|Consulte [parâmetros comuns](#common-parameters).<br><br>**\#num** (opcional) - o enésimo valor mais recente|Tipos de valor suportados: float, int, str , text, log<br><br>Observe que um período de tempo com tag de hash (\#N) funciona de maneira diferente aqui do que com muitas outras funções.<br>Por exemplo:<br>last() é sempre igual a last (\#1)<br>last(\#3) - terceiro valor mais recente (*não* três valores mais recentes)<br><br>O Zabbix não garante a ordem exata dos valores se existirem mais de dois valores em um segundo na história.<br><br>Exemplo:<br>=> **last**(/host/key) → recuperar o último valor<br>=> **last**(/host/key,* *\#2**) → recuperar o valor anterior<br>=> **last**(/host/key,**\#1**) <> **last**(/host/key,* *\#2**) → o último e o anterior são diferentes<br><br>Veja também first().|
|**logevenid** (/host/key,<\#num<:time shift>>,<pattern>)|<|<|<|
|<|Verificando se o ID do evento da última entrada de log corresponde a uma expressão regular.|Consulte [parâmetros comuns](#common-parameters).<br><br>**\#num** (opcional) - o enésimo valor mais recente< br><br>**pattern** (opcional) - expressão regular que descreve o padrão requerido, estilo [Perl Compatible Regular Expression](https://en.wikipedia.org/wiki/Perl_Compatible_Regular_Expressions) (PCRE) (os argumentos de string devem ser aspas duplas).|Tipos de valor suportados: log<br><br>Retorna:<br>0 - não corresponde<br>1 - corresponde|
|**logseverity** (/host/key,<\#num<:time shift>>)|<|<|<|
|<|Gravidade do log da última entrada de log.|Consulte [parâmetros comuns](#common-parameters).<br><br>**\#num** (opcional) - o enésimo valor mais recente|Tipos de valor suportados : log<br><br>Retorna:<br>0 - gravidade padrão<br>N - gravidade (inteiro, útil para logs de eventos do Windows: 1 - Informações, 2 - Aviso, 4 - Erro, 7 - Auditoria de falhas, 8 - Auditoria de sucesso, 9 - Crítico, 10 - Detalhado).<br>O Zabbix obtém a gravidade do log do campo **Informações** do log de eventos do Windows.|
|**logsource** (/host/key,<\#num<:time shift>>,<pattern>)|<|<|<|
|<|Verificando se a origem do log da última entrada de log corresponde a uma expressão regular.|Consulte [parâmetros comuns](#common-parameters).<br><br>**\#num** (opcional) - o enésimo número mais valor recente<br><br>**padrão** (opcional) - expressão regular que descreve o padrão necessário, estilo [Perl Compatible Regular Expression](https://en.wikipedia.org/wiki/Perl_Compatible_Regular_Expressions) (PCRE) ( argumentos de string devem estar entre aspas duplas).|Tipos de valor suportados: log<br><br>Retorna:<br>0 - não corresponde<br>1 - corresponde<br><br>Normalmente usado para logs de eventos do Windows. Por exemplo, logsource("VMware Server").|
|**monodec** (/host/key,(sec\|\#num)<:time shift>,<mode>)|<|<|<|
|<|Verifique se houve uma diminuição monótona nos valores.|Consulte [parâmetros comuns](#common-parameters).<br><br>**modo** (deve estar entre aspas duplas) - *fraco* (todo valor é menor ou igual ao anterior; padrão) ou *strict* (todos os valores diminuíram)|Tipos de valor suportados: int<br><br>Retorna 1 se todos os elementos no período de tempo diminuem continuamente, 0 caso contrário.<br ><br>Exemplo:<br>=> **monodec**(/Host1/system.swap.size\[all,free\],**60s**) + **monodec**(/Host2/system .swap.size\[all,free\],**60s**) + **monodec**(/Host3/system.swap.size\[all,free\],**60s**) - calcule em quantos hosts houve uma diminuição no tamanho do swap gratuito|
|**monoinc** (/host/key,(sec\|\#num)<:time shift>,<mode>)|<|<|<|
|<|Verifique se houve um aumento monótono nos valores.|Consulte [parâmetros comuns](#common-parameters).<br><br>**modo** (deve ser entre aspas duplas) - *fraco* ( cada valor é maior ou igual ao anterior; padrão) ou *strict* (todo valor aumentou)|Tipos de valor suportados: int<br><br>Retorna 1 se todos os elementos no período de tempo aumentarem continuamente, 0 caso contrário .<br><br>Exemplo:<br>=> **monoinc**(/Host1/system.localtime,**\#3**,"strict")=0 - verifique se o horário local do sistema está aumentando consistentemente|
|**nodata** (/host/chave,seg,<modo>)|<|<|<|
|<|Verificando se não há dados recebidos.|Consulte [parâmetros comuns](#common-parameters).<br><br>O período de **s** não deve ser inferior a 30 segundos porque o processo de sincronização de histórico calcula apenas esta função a cada 30 segundos.<br><br>nodata(/host/key,0) não é permitido.<br><br>**mode** - se definido como *strict* (entre aspas duplas), esta função será insensível à disponibilidade do proxy (consulte os comentários para obter detalhes).|Todos os tipos de valor são suportados.<br><br>Retorna:<br>1 - se nenhum dado for recebido durante o período de tempo definido<br>0 - caso contrário<br> <br>Desde o Zabbix 5.0, os gatilhos 'nodata' monitorados pelo proxy são, por padrão, sensíveis à disponibilidade do proxy - se o proxy ficar indisponível, os gatilhos 'nodata' não serão acionados imediatamente após uma conexão restaurada, mas pularão os dados para o período atrasado. Observe que, para proxies passivos, a supressão é ativada se a conexão for restaurada mais de 15 segundos e não menos de 2 e ProxyUpdateFrequency segundos depois. Para proxies ativos, a supressão é ativada se a conexão for restaurada mais de 15 segundos depois.<br><br>Para desativar a sensibilidade à disponibilidade do proxy, use o terceiro parâmetro, por exemplo: **nodata**(/host/key,** 5m**,**"estrito"**); neste caso, a função funcionará da mesma forma que antes de 5.0.0 e disparará assim que o período de avaliação (cinco minutos) sem dados tiver passado.<br><br>Observe que esta função exibirá um erro se, dentro do período do 1º parâmetro:<br>- não há dados e o servidor Zabbix foi reiniciado<br>- não há dados e a manutenção foi concluída<br>- não há dados e o item foi adicionado ou reativado<br>Os erros são exibidos na coluna *Info* no gatilho [configuração](/manual/web_interface/frontend_sections/configuration/hosts/triggers).<br><br>Esta função pode não funcionar corretamente se houver diferenças de horário entre o servidor Zabbix, proxy e agente. Consulte também: [Requisito de sincronização de tempo](/manual/installation/requirements#time_synchronization).|
|**percentil** (/host/key,(sec\|\#num)<:time shift>,percentage)|<|<|<|
|<|P-ésimo percentil de um período, em que P (porcentagem) é especificado pelo terceiro parâmetro.|Consulte [parâmetros comuns](#common-parameters).<br><br>**porcentagem** - um valor flutuante -ponto número entre 0 e 100 (inclusive) com até 4 dígitos após o ponto decimal|Tipos de valor suportados: float, int|
|**taxa** (/host/key,sec<:time shift>)|<|<|<|
|<|Taxa média por segundo do aumento em um contador monotonicamente crescente dentro do período de tempo definido.|Consulte [parâmetros comuns](#common-parameters).|Tipos de valor suportados: float, int<br><br>Funcionalmente corresponde a '[rate](https://prometheus.io/docs/prometheus/latest/querying/functions/#rate)' do PromQL.<br><br>Exemplo:<br>=> **rate** (/host/key,**30s**) → Se o aumento monotônico em 30 segundos for 20, esta função retornará 0,67.|
|**trendavg** (/host/key, time period:time shift)|<|<|<|
|<|Média de valores de tendência dentro do período de tempo definido.|**período de tempo** - o período de tempo (mínimo '1h'), definido como \<N>\<unidade de tempo> onde<br>`N` - número de unidades de tempo<br>`unidade de tempo` - h (hora), d (dia), w (semana), M (mês) ou y (ano).<br><br>[Deslocamento de tempo](/manual /config/triggers/expression#function-specific_parameters) - o deslocamento do período de tempo (veja exemplos)|Exemplos:<br>=> **trendavg**(/host/key,**1h:now/h**) → média para a hora anterior (por exemplo, 12:00-13:00)<br>=> **trendavg**(/host/key,**1h:now/h-1h**) → média para duas horas atrás ( 11:00-12:00)<br>=> **trendavg**(/host/key,**1h:now/h-2h**) → média de três horas atrás (10:00-11:00 )<br>=> **trendavg**(/host/key,**1M:now/M-1y**) → média do mês anterior um ano atrás|
|**trendcount** (/host/key, time period:time shift)|<|<|<|
|<|Número de valores de tendência recuperados com sucesso dentro do período de tempo definido.|**período de tempo** - o período de tempo (mínimo '1h'), definido como \<N>\<unidade de tempo> onde<br>`N ` - número de unidades de tempo<br>`unidade de tempo` - h (hora), d (dia), w (semana), M (mês) ou y (ano).<br><br>[Time shift]( /manual/config/triggers/expression#function-specific_parameters) - o deslocamento do período de tempo (veja exemplos)|Exemplos:<br>=> **trendcount**(/host/key,**1h:now/h** ) → contar para a hora anterior (por exemplo, 12:00-13:00)<br>=> **trendcount**(/host/key,**1h:now/h-1h**) → contar para duas horas atrás (11:00-12:00)<br>=> **trendcount**(/host/key,**1h:now/h-2h**) → contar por três horas atrás (10:00-11 :00)<br>=> **trendcount**(/host/key,**1M:now/M-1y**) → contagem para o mês anterior um ano atrás|
|**trendmax** (/host/key, time period:time shift)|<|<|<|
|<|O máximo em valores de tendência dentro do período de tempo definido.|**período de tempo** - o período de tempo (mínimo '1h'), definido como \<N>\<unidade de tempo> onde<br>`N` - número de unidades de tempo<br>`unidade de tempo` - h (hora), d (dia), w (semana), M (mês) ou y (ano).<br><br>[Deslocamento de tempo](/ manual/config/triggers/expression#function-specific_parameters) - o deslocamento do período de tempo (veja exemplos)|Exemplos:<br>=> **trendmax**(/host/key,**1h:now/h**) → máximo para a hora anterior (por exemplo, 12:00-13:00)<br>=> **trendmax**(/host/key,**1h:now/h**) - **trendmin**(/ host/key,**1h:now/h**) → calcule a diferença entre os valores máximo e mínimo (delta de tendência) para a hora anterior (12:00-13:00)<br>=> **trendmax* *(/host/key,**1h:now/h-1h**) → máximo para duas horas atrás (11:00-12:00)<br>=> **trendmax**(/host/key, **1h:agora/h-2h**) → máximo para três horas atrás (10:00-11:00)<br>=> **trendmax**(/host/key,**1M:now/M -1y**) → máximo para o mês anterior há um ano|
|**trendmin** (/host/key, time period:time shift)|<|<|<|
|<|O mínimo em valores de tendência dentro do período de tempo definido.|**período de tempo** - o período de tempo (mínimo '1h'), definido como \<N>\<unidade de tempo> onde<br>`N` - número de unidades de tempo<br>`unidade de tempo` - h (hora), d (dia), w (semana), M (mês) ou y (ano).<br><br>[Time shift](/manual/config /triggers/expression#function-specific_parameters) - o deslocamento do período de tempo (veja exemplos)|Exemplos:<br>=> **trendmin**(/host/key,**1h:now/h**) → mínimo para a hora anterior (por exemplo, 12:00-13:00)<br>=> **trendmin**(/host/key,**1h:now/h**) - **trendmin**(/host/key ,**1h:agora/h**) → calcule a diferença entre os valores máximo e mínimo (delta de tendência) para a hora anterior (12:00-13:00)<br>=> **trendmin**(/ host/key,**1h:now/h-1h**) → mínimo para duas horas atrás (11:00-12:00)<br>=> **trendmin**(/host/key,**1h :now/h-2h**) → mínimo para três horas atrás (10:00-11:00)<br>=> **trendmin**(/host/key,**1M:now/M-1y* *) → mínimo do mês anterior há um ano|
|**trendstl** (/host/key,eval period:time shift,detection period,season,<deviations>,<devalg>,<s_window>)|<|<|<|
|<|Retorna uma taxa de anomalia - um valor decimal entre 0 e 1 que é ((o número de valores de anomalia no período de detecção) / (número total de valores no período de detecção).|**período de avaliação** - o período de tempo que deve ser decomposto (mínimo '1h'), definido como \<N>\<unidade de tempo> onde<br>`N` - número de unidades de tempo<br>`unidade de tempo` - h (hora), d (dia ), w (semana), M (mês) ou y (ano).<br><br>[Time shift](/manual/config/triggers/expression#function-specific_parameters) - o deslocamento do período de tempo (veja exemplos) <br><br>**período de detecção** - o período de tempo a partir do final do período de avaliação para o qual as anomalias são calculadas (mínimo '1h', não pode ser maior que o período de avaliação), definido como \<N>\< unidade de tempo> onde<br>`N` - número de unidades de tempo<br>`unidade de tempo` - h (hora), d (dia), w (semana).<br><br>**temporada** - o menor período de tempo em que a sazonalidade (padrões repetidos) é esperada (mínimo '2h', não pode ser maior que o período eval, número de entradas no período eval deve ser maior que os dois tempos do r frequência resultante (estação/h)), definida como \<N>\<unidade de tempo> onde<br>`N` - número de unidades de tempo<br>`unidade de tempo` - h (hora), d (dia), w (semana).<br><br>**desvios** - o número de desvios (calculado com devalg) para contar como anomalia (pode ser decimal), (deve ser maior ou igual a 1, o padrão é 3) <br><br>**devalg** (deve estar entre aspas duplas) - algoritmo de desvio, pode ser *stddevpop*, *stddevsamp* ou *mad* (padrão)<br><br>**s_window** - o intervalo (em atrasos) da janela loess para extração sazonal (o padrão é 10 * número de entradas no período de avaliação + 1)|Exemplos:<br>=> **trendstl**(/host/key,**100h: now/h**,10h,2h) → analisando as últimas 100 horas de dados de tendência,<br>encontre a taxa de anomalias para as 10 horas anteriores desse período,<br>esperando que a periodicidade seja 2h,<br>a os valores da série restante do período de avaliação são considerados anomalias se atingirem o valor de 3 desvios do MAD dessa série restante<br>=> **trendstl**(/host/key,**100h:now/h-10h **,100h,2h,2.1,"louco ") → analisando o período das 100 horas anteriores de dados de tendência, começando a contar 10 horas atrás<br>encontre a taxa de anomalias para todo esse período<br>esperando que a periodicidade seja 2h,<br>os valores da série restante da avaliação período são considerados anomalias se atingirem o valor de 2,1 desvios do MAD dessa série restante<br>=> **trendstl**(/host/key,**100d:now/d-1d**,10d ,1d,4,,10) → analisando os 100 dias anteriores de dados de tendência a partir de um dia atrás,<br>encontre a taxa de anomalias para o período dos últimos 10d desse período,<br>esperando que a periodicidade seja 1d,< br>os valores da série restante do período de avaliação são considerados anomalias se atingirem o valor de 4 desvios do MAD dessa série restante,<br>substituindo o intervalo padrão da janela loess para extração sazonal de "10 * número de entradas in eval period + 1" com intervalo de 10 atrasos<br>=> **trendstl**(/host/key,**1M:now/M-1y**,1d,2h,,"stddevsamp") → mínimo para o mês anterior um ano atrás<br >analisando os dados de tendência do mês anterior a partir de um ano atrás,<br>encontre a taxa de anomalias do último dia desse período<br>esperando que a periodicidade seja 2h,<br>os valores da série restante do período de avaliação são considerados anomalias se atingirem o valor de 3 desvios do desvio padrão da amostra dessa série restante|
|**trendsum** (/host/key,time period:time shift)|<|<|<|
|<|Soma dos valores de tendência dentro do período de tempo definido.|**período de tempo** - o período de tempo (mínimo '1h'), definido como \<N>\<unidade de tempo> onde<br>`N` - número de tempo unidades<br>`unidade de tempo` - h (hora), d (dia), w (semana), M (mês) ou y (ano).<br><br>[Time shift](/manual/config/ triggers/expression#function-specific_parameters) - o deslocamento do período de tempo (veja exemplos)|Exemplos:<br>=> **trendsum**(/host/key,**1h:now/h**) → soma para o hora anterior (por exemplo, 12:00-13:00)<br>=> **trendsum**(/host/key,**1h:now/h-1h**) → soma de duas horas atrás (11:00 -12:00)<br>=> **trendsum**(/host/key,**1h:now/h-2h**) → soma de três horas atrás (10:00-11:00)<br >=> **trendsum**(/host/key,**1M:now/M-1y**) → soma para o mês anterior um ano atrás|

[comment]: # ({/new-ce01d749})
