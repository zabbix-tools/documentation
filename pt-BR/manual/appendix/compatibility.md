[comment]: # translation:outdated

[comment]: # ({new-d25f4c9e})
# 11 Compatibilidade entre versões

[comment]: # ({/new-d25f4c9e})

[comment]: # ({new-eefa9a73})
#### Agentes suportados

Para ser compatível com a versão 6.0 do Zabbix. O agente Zabbix não pode ser mais antigo que a versão 1.4 e mais recente que a 6.0

Talvez você precise revisar a configuração de agentes mais antigos, pois alguns
parâmetros mudaram, por exemplo, parâmetros relacionados a 
[logging](https://www.zabbix.com/documentation/3.0/manual/installation/upgrade_notes_300#changes_in_configuration_parameters_related_to_logging)
para versões anteriores que  3.0.

Para aproveitar ao máximo as métricas mais recentes, desempenho aprimorado e uso de memória reduzido, use o agente compatível mais recente..

[comment]: # ({/new-eefa9a73})

[comment]: # ({new-1a5df00a})
#### Servidores Proxy suportados

É necessário que o Zabbix Proxy e o Zabbix server estejam na mesma
versão. Proxies de versões anteriores não serão suportados pela versão
mais atual do Zabbix.

[comment]: # ({/new-1a5df00a})

[comment]: # ({new-5be689b0})
#### Proxies Zabbix suportados

Para ser compatível com o  Zabbix 6.0, o proxy precisa ser da mesma versão mais recente; portanto, apenas as versões de proxy Zabbix 6.0.x  funcionam com servidores Zabbix 6.0.x.

::: não importante
Não ficando possível iniciar  a atualização do servidor e ter um antigo,  o proxy desatualizado relata os dados para a vers~ao mais atual. Essa prática, nunca foi recomendada e nem apoiada pela Zabbix, agora oficialmente desabilitado, o servidor irá ignorar todos os dados recebidos de proxies desatualizados . Veja também [upgrade
procedure](/manual/installation/upgrade).
:::

Avisos sobre o uso de versões incompatíveis do daemon Zabbix são registrados.

[comment]: # ({/new-5be689b0})

[comment]: # ({new-78c6278a})

In relation to Zabbix server, proxies can be:
-   *Current* (proxy and server have the same major version);
-   *Outdated* (proxy version is older than server version, but is partially supported);
-   *Unsupported* (proxy version is older than server previous LTS release version *or* proxy version is newer than
server major version).

Examples:

|Server version|*Current* proxy version|*Outdated* proxy version|*Unsupported* proxy version|
|--------------|-----------------------|------------------------|---------------------------|
|6.4|6.4|6.0, 6.2|Older than 6.0; newer than 6.4|
|7.0|7.0|6.0, 6.2, 6.4|Older than 6.0; newer than 7.0|
|7.2|7.2|7.0|Older than 7.0; newer than 7.2|

[comment]: # ({/new-78c6278a})

[comment]: # ({new-9f9fce98})

Functionality supported by proxies:

|Proxy version|Data update|Configuration update|Tasks|
|--|--|--|----|
|*Current*|Yes|Yes|Yes|
|*Outdated*|Yes|No|[Remote commands](/manual/config/notifications/action/operation/remote_command) (e.g., shell scripts);<br>Immediate item value checks (i.e., *[Execute now](/manual/config/items/check_now)*);<br>Note: Preprocessing [tests with a real value](/manual/config/items/preprocessing#testing-real-value) are not supported.|
|*Unsupported*|No|No|No|

[comment]: # ({/new-9f9fce98})

[comment]: # ({new-f9a2762b})

Warnings about using incompatible Zabbix daemon versions are logged.

[comment]: # ({/new-f9a2762b})


[comment]: # ({new-5f9e54ca})
#### Supported XML files

XML files not older than version 1.8 are supported for import in Zabbix
6.0.

::: noteimportant
In the XML export format, trigger dependencies are
stored by name only. If there are several triggers with the same name
(for example, having different severities and expressions) that have a
dependency defined between them, it is not possible to import them. Such
dependencies must be manually removed from the XML file and re-added
after import.
:::

[comment]: # ({/new-5f9e54ca})
