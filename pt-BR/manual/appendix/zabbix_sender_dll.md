[comment]: # translation:outdated

[comment]: # ({new-a93134cd})
# 13 Biblioteca dinâmica Zabbix sender para Windows

Em um ambiente Windows as aplicações podem enviar dados diretamente ao
Zabbix server/proxy usando a biblioteca dinâmica do Zabbix Sender
(zabbix\_sender.dll) ao invés de iniciar um processo externo
(zabbix\_sender.exe).

Ela está localizada dentro de `bin\winXX\dev`. Para usa-la, inclua o
`zabbix_sender.h` e linque com a biblioteca `zabbix_sender.lib`. Um
exemplo de como a API pode ser utilizada está disponível em
build\\win32\\examples\\zabbix\_sender.

As seguintes funcionalidaes são providas por ela:

|`int zabbix_sender_send_values(const char *address, unsigned short port,const char *source, const zabbix_sender_value_t *values, int count,char **result);`{.c}|<|<|
|---------------------------------------------------------------------------------------------------------------------------------------------------------------|-|-|
|char **result);`{.c}|<|<|

As seguintes estruturas de dados são utilizadas pela biblioteca dinâmica
do Zabbix sender:

``` {.c}
typedef struct
{
    /* host name, must match the name of target host in Zabbix */
    char    *host;
    /* the item key */
    char    *key;
    /* the item value */
    char    *value;
}
zabbix_sender_value_t;

typedef struct
{
    /* number of total values processed */
    int total;
    /* number of failed values */
    int failed;
    /* time in seconds the server spent processing the sent values */
    double  time_spent;
}
zabbix_sender_info_t;
```

[comment]: # ({/new-a93134cd})
