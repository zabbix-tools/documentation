<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="pt-BR" datatype="plaintext" original="manual/appendix/protocols/zabbix_agent2_plugin.md">
    <body>
      <trans-unit id="bdae00e2" xml:space="preserve">
        <source># 4 Zabbix agent 2 plugin protocol

Zabbix agent 2 protocol is based on code, size and data model.</source>
      </trans-unit>
      <trans-unit id="a102065d" xml:space="preserve">
        <source>
### Code

|Type|Size|Comments|
|-----|-----|----------|
|Byte|	4	|Payload type, currently only JSON is supported.|</source>
      </trans-unit>
      <trans-unit id="6f328f94" xml:space="preserve">
        <source>
### Size

|Type|Size|Comments|
|-----|-----|----------|
|Byte|	4	|Size of the current payload in bytes.|</source>
      </trans-unit>
      <trans-unit id="e3974519" xml:space="preserve">
        <source>
### Payload data

|Type|Size|Comments|
|-----|-----|----------|
|Byte|	Defined by the *Size* field	|JSON formatted data.|</source>
      </trans-unit>
      <trans-unit id="265b88ca" xml:space="preserve">
        <source>##### Payload data definition

###### Common data

These parameters are present in all requests/responses:

|Name|Type|Comments|
|-----|--------|----------|
|id	  |uint32|For requests - the incrementing identifier used to link requests with responses. Unique within a request direction (i.e. from agent to plugin or from plugin to agent).&lt;br&gt;For responses - ID of the corresponding request.|
|type|uint32|The request type.|</source>
      </trans-unit>
      <trans-unit id="6bdfbea5" xml:space="preserve">
        <source>
###### Log request

A request sent by a plugin to write a log message into the agent log file.

|   |   |
|---|---|
|direction|plugin → agent|
|response|no|

Parameters specific to log requests:

|Name|Type|Comments|
|---------|--------|----------|
|severity|uint32|The message severity (log level).|
|message|string|The message to log.|

*Example:*

    {"id":0,"type":1,"severity":3,"message":"message"}</source>
      </trans-unit>
      <trans-unit id="38f1c088" xml:space="preserve">
        <source>
###### Register request

A request sent by the agent during the agent startup phase to obtain provided metrics to register a plugin.

|   |   |
|---|---|
|direction|agent → plugin|
|response|yes|

Parameters specific to register requests:

|Name|Type|Comments|
|---------|--------|----------|
|version|string|The protocol version &lt;major&gt;.&lt;minor&gt;|

*Example:*

    {"id":1,"type":2,"version":"1.0"}</source>
      </trans-unit>
      <trans-unit id="d8c62312" xml:space="preserve">
        <source>
###### Register response

Plugin's response to the register request.

|   |   |
|---|---|
|direction|plugin → agent|
|response|n/a|

Parameters specific to register responses:

|Name|Type|Comments|
|---------|--------|----------|
|name|string|The plugin name.|
|metrics|array of strings (optional)|The metrics with descriptions as used in the plugin. Returns RegisterMetrics(). Absent if error is returned.|
|interfaces|uint32 (optional)|The bit mask of plugin's supported interfaces. Absent if error is returned.|
|error|string (optional)|An error message returned if a plugin cannot be started. Absent, if metrics are returned.|

*Examples:*

    {"id":2,"type":3,"metrics":["external.test", "External exporter Test."], "interfaces": 4}

or

    {"id":2,"type":3,"error":"error message"}</source>
      </trans-unit>
      <trans-unit id="0b67f229" xml:space="preserve">
        <source>
###### Start request

A request to execute the Start function of the Runner interface.

|   |   |
|---|---|
|direction|agent → plugin|
|response|no|

The request doesn't have specific parameters, it only contains [common data](#common-data) parameters.

*Example:*

    {"id":3,"type":4}</source>
      </trans-unit>
      <trans-unit id="f3c8d489" xml:space="preserve">
        <source>
###### Terminate request

A request sent by the agent to shutdown a plugin.

|   |   |
|---|---|
|direction|agent → plugin|
|response|no|

The request doesn't have specific parameters, it only contains [common data](#common-data) parameters.

*Example:*

    {"id":3,"type":5}</source>
      </trans-unit>
      <trans-unit id="b2754963" xml:space="preserve">
        <source>
###### Export request

A request to execute the Export function of the Exporter interface.

|   |   |
|---|---|
|direction|agent → plugin|
|response|no|

Parameters specific to export requests:

|Name|Type|Comments|
|---------|--------|----------|
|key|string|The plugin key.|
|parameters|array of strings (optional)|The parameters for Export function.|

*Example:*

    {"id":4,"type":6,"key":"test.key","parameters":["foo","bar"]}</source>
      </trans-unit>
      <trans-unit id="27e2bd0c" xml:space="preserve">
        <source>
###### Export response

Response from the Export function of the Exporter interface.

|   |   |
|---|---|
|direction|plugin → agent|
|response|n/a|

Parameters specific to export responses:

|Name|Type|Comments|
|---------|--------|----------|
|value|string (optional) |Response value from the Export function. Absent, if error is returned.|
|error|string (optional) |Error message if the Export function has not been executed successfully. Absent, if value is returned.|

*Examples:*

    {"id":5,"type":7,"value":"response"}

or

    {"id":5,"type":7,"error":"error message"}</source>
      </trans-unit>
      <trans-unit id="15a7dce0" xml:space="preserve">
        <source>
###### Configure request

A request to execute the *Configure* function of the *Configurator* interface.

|   |   |
|---|---|
|direction|agent → plugin|
|response|n/a|

Parameters specific to *Configure* requests:

|Name|Type|Comments|
|---------|--------|----------|
|global_options|JSON object|JSON object containing global agent configuration options.|
|private_options|JSON object (optional)|JSON object containing private plugin configuration options, if provided.|

*Example:*

    {"id":6,"type":8,"global_options":{...},"private_options":{...}}</source>
      </trans-unit>
      <trans-unit id="14b279bc" xml:space="preserve">
        <source>
###### Validate request

A request to execute *Validate* function of the *Configurator* interface.

|   |   |
|---|---|
|direction|agent → plugin|
|response|yes|

Parameters specific to *Validate* requests:

|Name|Type|Comments|
|---------|--------|----------|
|private_options|JSON object (optional)|JSON object containing private plugin configuration options, if provided.|

*Example:*

    {"id":7,"type":9,"private_options":{...}}</source>
      </trans-unit>
      <trans-unit id="9a73001b" xml:space="preserve">
        <source>
###### Validate response

Response from *Validate* function of *Configurator* interface.

|   |   |
|---|---|
|direction|plugin → agent|
|response|n/a|

Parameters specific to *Validate* responses:

|Name|Type|Comments|
|---------|--------|----------|
|error|string (optional)|An error message returned if the Validate function is not executed successfully. Absent if executed successfully.|

*Example:*

    {"id":8,"type":10}

or

    {"id":8,"type":10,"error":"error message"}</source>
      </trans-unit>
    </body>
  </file>
</xliff>
