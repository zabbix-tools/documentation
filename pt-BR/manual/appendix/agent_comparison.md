[comment]: # translation:outdated

[comment]: # ({0082d168-28165f87})

# 18 Agente vs agente 2 comparação


Esta seção descreve as diferenças entre o Agente Zabbix e Agente Zabbix 2.

|Parâmetro|Agente Zabbix |Agente Zabbix  2|
|---------|------------|--------------|
|Linguagem de programação|C|Go com algumas partes em C|
|Daemonization|sim|apenas através de systemd (no Windows)|
|Extensões suportadas|Customização de [módulos carregáveis](/manual/config/items/loadablemodules) em C.|Customização de [plugins](/manual/config/items/plugins) em Go.|
|Requisitos |<|<|
|Plataformas suportadas|Linux, IBM AIX, FreeBSD, NetBSD, OpenBSD, HP-UX, Mac OS X, Solaris: 9, 10, 11, Windows: todas as versões desktops e servidores desde XP.|Linux, Windows: todas as versões desktops e servidores desde XP.|
|Bibliotecas de criptografia suportadas |GnuTLS 3.1.18 e mais recentes<br>OpenSSL 1.0.1, 1.0.2, 1.1.0, 1.1.1<br>LibreSSL - testado com as versões 2.7.4, 2.8.2 (aplicam-se certas limitações, veja a página [Encryption](/manual/encryption#compiling_zabbix_with_encryption_support) para mais detalhes).|Linux: OpenSSL 1.0.1 e posterior é suportado desde o Zabbix 4.4.8.<br>MS Windows: OpenSSL 1.1.1 ou posterior.<br>A biblioteca OpenSSL deve ter o suporte PSK habilitado. LibreSSL não é suportado.|
|Monitoramento de processos|<|<|
|Processo|Um processo de verificação ativo separado para cada registro de servidor/proxy.|Processo único com threads criados automaticamente.<br>O número máximo de threads é determinado pela variável de ambiente GOMAXPROCS.|
|Metricas|**UNIX**: veja a lista de suportada dos [itens](/manual/config/items/itemtypes/zabbix_agent).<br><br>**Windows**: veja uma lista adicional de itens específicos para Windows [itens](/manual/config/items/itemtypes/zabbix_agent/win_keys).|**UNIX**: Todas as métricas suportadas pelo agente Zabbix.<br>Além disso, o agente 2 fornece solução de monitoramento nativa do Zabbix para: Docker, Memcached, MySQL, PostgreSQL, Redis, systemd, e outros alvos de monitoramento - veja uma lista completa dedas especificações do agente 2 [itens](/manual/config/items/itemtypes/zabbix_agent/zabbix_agent2).<br><br>**Windows**: Todas as métricas suportadas pelo agente Zabbix, e também net.tcp.service\* verificações de HTTPS, LDAP.<br>Além disso, o agente 2 fornece solução de monitoramento nativa do Zabbix para: PostgreSQL, Redis.|
|Simultaneidade|Verificações ativas para um único servidor são executadas sequencialmente.|Verificações de diferentes plugins ou múltiplas verificações dentro de um plugin podem ser executadas simultaneamente.|
|Intervalos programados/flexíveis |Suportado apenas para verificações passivas.| Suportado para verificações passivas e ativas.|
|Traps de terceiros |não|sim|
|Recursos adicionais|<|<|
|Armazenamento persistente |não | sim|
|Arquivos persistentes para log\*\[\] métricas |sim (apenas para Unix)|não|
|Configurações de tempo limite | Definido apenas no nível do agente .|O tempo limite do plug-in pode substituir o tempo limite definido no nível do agente.|
|Alterar usuário em tempo de execução| sim (somente sistemas do tipo Unix)|não (controlado por systemd)|
|Conjunto de criptografia configuráveis ​​pelo usuário |sim|não|

**Veja também:**

-   *Descrição dos processos Zabbix*: [agente Zabbix
    ](/manual/concepts/agent), [ agente Zabbix 
    2](/manual/concepts/agent2)
-   *Parâmetros de configuração*: agente Zabbix 
    [UNIX](/manual/appendix/config/zabbix_agentd) /
    [Windows](/manual/appendix/config/zabbix_agentd_win), agente Zabbix 2
    [UNIX](/manual/appendix/config/zabbix_agent2) /
    [Windows](/manual/appendix/config/zabbix_agent2_win)

[comment]: # ({/0082d168-28165f87})
