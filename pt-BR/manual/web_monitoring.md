[comment]: # translation:outdated

[comment]: # ({new-734ebac5})
# 8. Monitoração web

[comment]: # ({/new-734ebac5})

[comment]: # ({new-f37bc18c})
#### Visão geral

Com o Zabbix você pode verificar a disponibilidade de diversos aspectos
de sites web.\

::: noteimportant
Para executar a verificação web o Zabbix Server
precisa ser
[configurado](/pt/manual/installation/install#from_the_sources) com
suporte ao cURL (libcurl).
:::

Para ativar a monitoração web você precisa definir cenários web. Um
cenário web consiste em uma ou mais requisições HTTP (passos). Os passos
são periodicamente executados pelo Zabbix Server em uma ordem
pré-definida.

Desde o Zabbix 2.2 os cenários web podem ser associados a
hosts/templates da mesma forma que os itens, triggers, etc. Isso
possibilita que eles sejam criados no nível de template e sejam
aplicados em vários hosts de uma só vez.

As seguintes informações são coletadas por qualquer cenário web:

-   velocidade média de download de todos os passos do cenário
-   número de passos com falha
-   última mensagem de erro

As seguintes informações são coletadas para cada passo de um cenário
web:

-   velocidade de download por segundo
-   tempo te resposta
-   código de resposta

O Zabbix também verifica se uma página HTML contêm determinado texto.
Ele pode executar um login simulado e seguir um caminho simulado de
cliques de mouse na página.

A monitoração web do Zabbix suporta tanto HTTP quanto HTTPS. Quando
estamos executando um cenário web, o Zabbix opcionalmente pode seguir os
redirecionamentos percebidos em um passo (veja opção *Seguir
redirecionamento*). A quantidade máxima de redirecionamentos suportado
são 10 (utilizando a opção cURL
[CURLOPT\_MAXREDIRS](http://curl.haxx.se/libcurl/c/CURLOPT_MAXREDIRS.html)).
Todos os cookies são preservados durante a execução de um cenário.

O dado coletado de um cenário em execução é guardado no banco de dados,
estes dados são automaticamente utilizados para gráficos, triggers e
notificações. Consulte também: [Itens de
Monitoração](/pt/manual/web_monitoring/items).

Consulte também [problemas
conhecidos](/pt/manual/installation/known_issues#https_checks) para a
monitoração web usando HTTPS.

[comment]: # ({/new-f37bc18c})

[comment]: # ({new-f007debe})
#### Configurando um cenário web

Para configurar um cenário web:

-   Acesse *Configuração → Hosts* (ou *Templates*)
-   Clique no link *Web* da linha do host/template
-   Clique no botão *Criar cenário web* situado no canto direito da
    barra de título
-   Informe os parâmetros do cenário

A aba **Cenário** permite configurar os parâmetros gerais de um cenário
web.

![](../../assets/en/manual/config/scenario.png)

Parâmetros gerais:

|Parâmetro|Descrição|
|----------|-----------|
|*Host*|Nome do host/template ao qual o cenário pertence.|
|*Nome*|Nome único do cenário.<br>*A partir do Zabbix 2.2*, the name may contain supported [macros](/pt/manual/appendix/macros/supported_by_location).|
|*Application*|Select an application the scenario will belong to.<br>Web scenario items will be grouped under the selected application in *Monitoring → Latest data*.|
|*New application*|Enter the name of a new application for the scenario.|
|*Update interval (in sec)*|How often the scenario will be executed, in seconds.|
|*Attempts*|The number of attempts for executing web scenario steps. In case of network problems (timeout, no connectivity, etc) Zabbix can repeat executing a step several times. The figure set will equally affect each step of the scenario. Up to 10 attempts can be specified, default value is 1.<br>*Note*: Zabbix will not repeat a step because of a wrong response code or the mismatch of a required string.<br>This parameter is supported A partir do *Zabbix 2.2*.|
|*Agent*|Select a client agent.<br>Zabbix will pretend to be the selected browser. This is useful when a website returns different content for different browsers.<br>User macros can be used in this field, *A partir do Zabbix 2.2*.|
|*HTTP proxy*|You can specify an HTTP proxy to use, using the format: *http://\[username\[:password\]@\]proxy.mycompany.com\[:port\]*<br>By default, 1080 port will be used.<br>If specified, the proxy will overwrite proxy related environment variables like http\_proxy, HTTPS\_PROXY. If not specified, the proxy will not overwrite proxy related environment variables.<br>The entered value is passed on "as is", no sanity checking takes place. You may also enter a SOCKS proxy address. If you specify the wrong protocol, the connection will fail and the item will become unsupported. With no protocol specified, the proxy will be treated as an HTTP proxy.<br>*Note*: Only simple authentication is supported with HTTP proxy.<br>User macros can be used in this field.<br>This parameter is supported A partir do *Zabbix 2.2*.|
|*Variables*|List of scenario-level variables (macros) that may be used in scenario steps (URL, Post variables).<br>They have the following format:<br>**{macro1}**=value1<br>**{macro2}**=value2<br>**{macro3}**=regex:<regular expression><br>For example:<br>{username}=Alexei<br>{password}=kj3h5kJ34bd<br>{hostid}=regex:hostid is (\[0-9\]+)<br>If the value part starts with *regex:* then the part after it will be treated as a regular expression that will search the web page and, if found, store the match in the variable. Note that at least one subgroup must be present so that the matched value can be extracted.<br>The macros can then be referenced in the steps as {username}, {password} and {hostid}. Zabbix will automatically replace them with actual values.<br>Having variables that search a webpage for a regular expression match is supported *A partir do Zabbix 2.2*.<br>`HOST.*` macros and user macros can be used in this field, *A partir do Zabbix 2.2*.<br>*Note*: Variables are not URL-encoded.|
|*Headers*|HTTP headers that will be sent when performing a request.<br>`HOST.*` macros and user macros can be used in this field.<br>Specifying custom headers is supported *A partir do Zabbix 2.4*.|
|*Enabled*|The scenario is active if this box is checked, otherwise - disabled.|

Note that when editing an existing scenario, two extra buttons are
available in the form:

|   |   |
|---|---|
|![](../../assets/en/manual/web_monitoring/scenario_edit_clone.png)|Create another scenario based on the properties of the existing one.|
|![](../../assets/en/manual/web_monitoring/scenario_edit_clear.png)|Delete history and trend data for the scenario. This will make the server perform the scenario immediately after deleting the data.|

::: notetip
If *HTTP proxy* field is left empty, another way for
using an HTTP proxy is to set proxy related environment variables.

For HTTP checks - set the **http\_proxy** environment variable for the
Zabbix server user. For example,
//http\_proxy=[http:%%//%%proxy\_ip:proxy\_port//](http:%%//%%proxy_ip:proxy_port//).

For HTTPS checks - set the **HTTPS\_PROXY** environment variable. For
example,
//HTTPS\_PROXY=[http:%%//%%proxy\_ip:proxy\_port//](http:%%//%%proxy_ip:proxy_port//).
More details are available by running a shell command: *\# man
curl*.
:::

The **Steps** tab allows you to configure the web scenario steps. To add
a web scenario step, click on *Add*.

![](../../assets/en/manual/config/scenario2.png){width="600"}

[comment]: # ({/new-f007debe})

[comment]: # ({new-74c8baab})
#### Configuring steps

![](../../assets/en/manual/config/scenario_step.png)

Step parameters:

|Parameter|Description|<|<|<|<|
|---------|-----------|-|-|-|-|
|*Name*|Unique step name.<br>*A partir do Zabbix 2.2*, o nome pode conter [macros](/pt/manual/appendix/macros/supported_by_location).|<|<|<|<|
|*URL*|URL a se conectar e receber dados, por exemplo:<br>http://www.zabbix.com<br>https://www.google.com<br>variáveis GET podem ser passadas pelo parâmetro URL.<br>*A partir do Zabbix 2.2*, este campo pode conter as [macros](/pt/manual/appendix/macros/supported_by_location) suportadas.<br>Limitado a 2048 characteres *A partir do Zabbix 2.4*.|<|<|<|<|
|*Post*|Variáveis HTTP POST, se existirem.<br>Por exemplo:<br>id=2345&userid={user}<br>Se {user} for definido como uma macro do cenário web, ela será substituída por seu valor quando o passo for executado.<br>A informação será enviada da forma que for informada, elas não serão codificadas em padrão URL.<br>*A partir do Zabbix 2.2*, este campo pode conter [macros](/pt/manual/appendix/macros/supported_by_location).|<|<|<|<|
|*Variables*|Lista das variáveis do passo (macros) que podem ser utilizadas em funções de GET e POST.<br>Variáveis em nível de passo sobrepõe as variáveis em nível de cenário.<br>Elas possuem o seguinte formato:<br>**{macro}**=value<br>**{macro}**=regex:<regular expression><br>Para mais detalhes veja a descrição de variáveis no manual de [níveis de cenário](web_monitoring#configuring_a_web_scenario).<br>O suporte para variáveis em nível de passo é suportado a partir do Zabbix 2.2*.<br>*Note*: As variáveis não são codificadas em formato de URL. \| \|*Headers//|Os headers HTTP serão enviados quando for executada uma solitação. Os headers no nível do asso irão sobrescrever os headers definidos no nível do cenário. Por exemplo, 'User-Agent:' sem dados remove 'User-Agent' definido no nível de cenário.<br>As macros `HOST.*` e as macros de usuário podem ser utilizadas neste campo.<br>A especificação de headers customizados é suportada a partir do Zabbix 2.4*. \| \|*Seguir redirecionamentos//|Marque esta opção para seguir os redirecionamentos HTTP.<br>Isso configura a opção cURL [CURLOPT\_FOLLOWLOCATION](http://curl.haxx.se/libcurl/c/CURLOPT_FOLLOWLOCATION.html).<br>Esta opção é suportada a partir do Zabbix 2.4*. \| \|*Receber apenas os cabeçalhos//|Marque esta opção para receber apenas os cabeçalhos da resposta HTTP.<br>Isso configura a opção cURL [CURLOPT\_NOBODY](http://curl.haxx.se/libcurl/c/CURLOPT_NOBODY.html).<br>Esta opção é suportada a partir do Zabbix 2.4*. \| \|*Timeout//|O zabbix não irá gastar mais tempo do que a quantidade aqui definida para processar a URL. Atualmente este parâmetro define o tempo máximo para fazer a conexão com a URL e o tempo máximo para receber a resposta. Desta forma, o Zabbix não irá gastar mais tempo que **2 x Timeout** no passo.<br>Por exemplo: 15|
|*Texto requerido*|Expressão regular com padrão desejado.<br>A não ser que o conteúdo recebido (HTML) contenha o padrão desejado o passo irá falhar. Se estiver vazio, não será feita nenhuma verificação.<br>Por exemplo:<br>Homepage of Zabbix<br>Welcome.\*admin<br>*Nota*: Refernência: [expressões regulares](regular_expressions) criadas na interface web do Zabbix não são suportadas neste campo.<br>*A partir do Zabbix 2.2*, este campo pode conter [macros](/pt/manual/appendix/macros/supported_by_location).|<|<|<|<|
|*Códigos de resposta requeridos*|Lista dos códigos HTTP esperados. Se o Zabbix receber um código que não estiver na lista o passo irá falhar.<br>Se estiver vazio, nenhuma verificação será feita.<br>Por exemplo: 200,201,210-299<br>*A partir do Zabbix 2.2*, macros de usuário poderão ser utilizadas neste campo.|<|<|<|<|

::: noteclassic
Quaisquer modificações em passos do cenário web só são
salvas quando o cenário web é salvo.
:::

Consulte também um [exemplo de real](/pt/manual/web_monitoring/example)
sobre como os passos de monitoração web podem ser configurados.

[comment]: # ({/new-74c8baab})

[comment]: # ({new-6f40ea68})
#### Configurando a autenticação

A aba **Autenticação** permite que você configure as opções de
autenticação do cenário.

![](../../assets/en/manual/config/scenario3.png)

Parâmetros de autenticação:

|Parâmetro|Descrição|<|<|<|<|
|----------|-----------|-|-|-|-|
|*Autenticação*|Opções de autenticação.<br>**None** - sem autenticação.<br>**Básico** - autenticação básica.<br>**NTLM** - autenticação NTLM ([Windows NT LAN Manager)](http://en.wikipedia.org/wiki/NTLM).<br>Selecionando um método de autenticação irão surgir dois novos campos para definir usuário e senha.<br>As macros de usuário podem ser utilizadas em nos campos de usuário e senha, *A partir do Zabbix 2.2*.|<|<|<|<|
|*SSL verify peer*|Marque a opção para verificar o certificado SSL do servidor web.<br>O certificado do servidor será automaticamente buscado a partir da localização dos certificados de autoridade (CA). Você pode substituir a localização dos CAs usando o parâmetro [SSLCALocation](/pt/manual/appendix/config/zabbix_server) do Zabbix Server/Proxy.<br>Isso configura a opção cURL [CURLOPT\_SSL\_VERIFYPEER](http://curl.haxx.se/libcurl/c/CURLOPT_SSL_VERIFYPEER.html).<br>Esta opção é suportada desde o Zabbix 2.4*. \| \|*SSL verify host//|Marque esta opção para verificar o campo *Common Name* ou o campo *Subject Alternate Name* do certificado do servidor.<br>Isso configura a opção do cURL [CURLOPT\_SSL\_VERIFYHOST](http://curl.haxx.se/libcurl/c/CURLOPT_SSL_VERIFYHOST.html).<br>Esta opção é suportada desde o Zabbix 2.4*. \| \|*SSL certificate file//|O nome do certificado SSL utilizado para a autenticação com o cliente. O arquivo de certificado precisa estar no formato PEM^1^. Se o arquivo de certificado contiver também a chave privada, deixe o campo *SSL key file* vazio. Se a chave estiver criptografada, defina a senha no campo *Senha do SSL*. O diretório contendo o arquivo é definido pelo parâmetro de configuração [SSLCertLocation](/pt/manual/appendix/config/zabbix_server).<br>As macros `HOST.*` e macros de usuário podem ser utilizadas neste campo.<br>Isso configura a opção cURL [CURLOPT\_SSLCERT](http://curl.haxx.se/libcurl/c/CURLOPT_SSLCERT.html).<br>Esta opção é suportada desde o Zabbix 2.4*. \| \|*SSL key file//|Nome da chave privada SSL utilizada para a autenticação do cliente. A chave privada precisa estar no formato PEM^1^. O diretório contendo este arquivo é definido no arquivo de configuração do Zabbix Server/Proxy no parâmetro [SSLKeyLocation](/pt/manual/appendix/config/zabbix_server).<br>As macros `HOST.*` e macros de usuário podem ser utilizadas neste campo.<br>Isso configura a opção cURL [CURLOPT\_SSLKEY](http://curl.haxx.se/libcurl/c/CURLOPT_SSLKEY.html).<br>Esta opção é suportada desde o Zabbix 2.4*. \| \|*SSL key password//|Arquivo de chave SSL privada.<br>Macros de usuário podem ser utilizadas neste campo.<br>Este campo configura a opção cURL [CURLOPT\_KEYPASSWD](http://curl.haxx.se/libcurl/c/CURLOPT_KEYPASSWD.html).<br>É suportado desde o Zabbix 2.4//.|

::: noteimportant
 \[1\] O Zabbix suporta certificado e chave
privada no formato PEM somente. Caso você possua certificado no formato
PKCS \#12 (normalmente com a extensão \*.p12 ou \*.pfx) você pode gerar
o arquivo PEM com os comandos a seguir:

    openssl pkcs12 -in ssl-cert.p12 -clcerts -nokeys -out ssl-cert.pem
    openssl pkcs12 -in ssl-cert.p12 -nocerts -nodes  -out ssl-cert.key


:::

::: noteclassic
 O Zabbix Server reconhece as modificações nos certificados
sem reinicia. 
:::

::: noteclassic
 Se você tem um certificado de cliente e uma chave privada
em um único arquivo, apenas defina isso no campo "Arquivo de certificado
SSL" e deixe o campo "SSL key file" vazio. O certificado e a chave
precisam estar no formato PEM. Combinar o certificado e a chave é fácil:

    cat client.crt client.key > client.pem


:::

[comment]: # ({/new-6f40ea68})

[comment]: # ({new-4cce2d90})
#### Apresentação

Para visualizar dados detalhados de vários cenários web, acesse
*Monitoramento → Web* ou *Dados recentes*. Clique no nome do cenário web
e veja as estatísticas detalhadas.

![](../../assets/en/manual/web_monitoring/scenario_details.png){width="600"}

Uma visão geral dos cenários de monitoração podem ser obtidos em
*Monitoramento → Dashboard*.

[comment]: # ({/new-4cce2d90})

[comment]: # ({new-ae869848})
#### Monitoramento estendido

Algumas vezes é necessário registrar o conteúdo recebido das páginas
HTML. Isso é especialmente útil nos casos de falha nos passos web. O
debug em nível 5 (trace) servirá a este propósito. Este nível pode ser
configurado nos arquivos de configuração do
[servidor](/pt/manual/appendix/config/zabbix_server) e no
[proxy](/pt/manual/appendix/config/zabbix_proxy) ou através do controle
de tempo de execução (`-R log_level_increase="http poller,N"`, onde N é
o número do processo). Os exemplos a seguir demonstram como aumentar o
nível de monitoração já iniciado :

    Aumentar o nível de log de todos os poolers HTTP:
    shell> zabbix_server -R log_level_increase="http poller"

    Aumentar o nível de log do segundo poolers HTTP:
    shell> zabbix_server -R log_level_increase="http poller,2"

Se a monitoração estendida não for mais necessária você pode reduzir o
nível usando a opção `-R log_level_decrease`.

[comment]: # ({/new-ae869848})

[comment]: # ({new-42ab5787})
#### Extended monitoring

Sometimes it is necessary to log received HTML page content. This is
especially useful if some web scenario step fails. Debug level 5 (trace)
serves that purpose. This level can be set in
[server](/manual/appendix/config/zabbix_server) and
[proxy](/manual/appendix/config/zabbix_proxy) configuration files or
using a runtime control option (`-R log_level_increase="http poller,N"`,
where N is the process number). The following examples demonstrate how
extended monitoring can be started provided debug level 4 is already
set:

    Increase log level of all http pollers:
    shell> zabbix_server -R log_level_increase="http poller"

    Increase log level of second http poller:
    shell> zabbix_server -R log_level_increase="http poller,2"

If extended web monitoring is not required it can be stopped using the
`-R log_level_decrease` option.

[comment]: # ({/new-42ab5787})
