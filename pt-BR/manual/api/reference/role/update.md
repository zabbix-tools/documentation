[comment]: # translation:outdated

[comment]: # ({new-a0f60ec7})
# role.update

[comment]: # ({/new-a0f60ec7})

[comment]: # ({new-63875138})
### Description

`object role.update(object/array roles)`

This method allows to update existing roles.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-63875138})

[comment]: # ({new-fa5c3d93})
### Parameters

`(object/array)` Role properties to be updated.

The `roleid` property must be defined for each role, all other
properties are optional. Only the passed properties will be updated, all
others will remain unchanged.

Additionally to the [standard role properties](object#role) the method
accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|rules|array|Access [rules](object#role_rules) to replace the current access rules assigned to the role.|

[comment]: # ({/new-fa5c3d93})

[comment]: # ({new-12d196f6})
### Return values

`(object)` Returns an object containing the IDs of the updated roles
under the `roleids` property.

[comment]: # ({/new-12d196f6})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-1ce1fe70})
#### Disabling ability to execute scripts

Update role with ID "5", disable ability to execute scripts.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "role.update",
    "params": [
        {
            "roleid": "5",
            "rules": {
                "actions": [
                    {
                        "name": "execute_scripts",
                        "status": "0"
                    }
                ]
            }
        }
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "roleids": [
            "5"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-1ce1fe70})

[comment]: # ({new-a6349165})
#### Limiting access to API

Update role with ID "5", deny to call any "create", "update" or "delete"
methods.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "role.update",
    "params": [
        {
            "roleid": "5",
            "rules": {
                "api.access": "1",
                "api.mode": "0",
                "api": ["*.create", "*.update", "*.delete"]
            }
        }
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "roleids": [
            "5"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-a6349165})

[comment]: # ({new-36db1bd2})
### Source

CRole::update() in *ui/include/classes/api/services/CRole.php*.

[comment]: # ({/new-36db1bd2})
