[comment]: # translation:outdated

[comment]: # ({new-cc40eabf})
# role.delete

[comment]: # ({/new-cc40eabf})

[comment]: # ({new-c4fe1cb7})
### Description

`object role.delete(array roleids)`

This method allows to delete roles.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-c4fe1cb7})

[comment]: # ({new-3eacfbc2})
### Parameters

`(array)` IDs of the roles to delete.

[comment]: # ({/new-3eacfbc2})

[comment]: # ({new-ba69c118})
### Return values

`(object)` Returns an object containing the IDs of the deleted roles
under the `roleids` property.

[comment]: # ({/new-ba69c118})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-4fc52493})
#### Deleting multiple user roles

Delete two user roles.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "role.delete",
    "params": [
        "4",
        "5"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "roleids": [
            "4",
            "5"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-4fc52493})

[comment]: # ({new-32808752})
### Source

CRole::delete() in *ui/include/classes/api/services/CRole.php*.

[comment]: # ({/new-32808752})
