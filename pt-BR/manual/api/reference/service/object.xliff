<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="pt-BR" datatype="plaintext" original="manual/api/reference/service/object.md">
    <body>
      <trans-unit id="b1a89f1b" xml:space="preserve">
        <source># &gt; Service object

The following objects are directly related to the `service` API.</source>
      </trans-unit>
      <trans-unit id="5d7a3d70" xml:space="preserve">
        <source>### Service

The service object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|serviceid|string|ID of the service.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *read-only*&lt;br&gt;- *required* for update operations|
|algorithm|integer|Status calculation rule. Only applicable if child services exist.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - set status to OK;&lt;br&gt;1 - most critical if all children have problems;&lt;br&gt;2 - most critical of child services.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* for create operations|
|name|string|Name of the service.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* for create operations|
|sortorder|integer|Position of the service used for sorting.&lt;br&gt;&lt;br&gt;Possible values: 0-999.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* for create operations|
|weight|integer|Service weight.&lt;br&gt;&lt;br&gt;Possible values: 0-1000000.&lt;br&gt;&lt;br&gt;Default: 0.|
|propagation\_rule|integer|Status propagation rule.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* propagate service status as is - without any changes;&lt;br&gt;1 - increase the propagated status by a given `propagation_value` (by 1 to 5 severities);&lt;br&gt;2 - decrease the propagated status by a given `propagation_value` (by 1 to 5 severities);&lt;br&gt;3 - ignore this service - the status is not propagated to the parent service at all;&lt;br&gt;4 - set fixed service status using a given `propagation_value`.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* if `propagation_value` is set|
|propagation\_value|integer|Status propagation value.&lt;br&gt;&lt;br&gt;Possible values if `propagation_rule` is set to "0" or "3":&lt;br&gt;0 - Not classified.&lt;br&gt;&lt;br&gt;Possible values if `propagation_rule` is set to "1" or "2":&lt;br&gt;1 - Information;&lt;br&gt;2 - Warning;&lt;br&gt;3 - Average;&lt;br&gt;4 - High;&lt;br&gt;5 - Disaster.&lt;br&gt;&lt;br&gt;Possible values if `propagation_rule` is set to "4":&lt;br&gt;-1 - OK;&lt;br&gt;0 - Not classified;&lt;br&gt;1 - Information;&lt;br&gt;2 - Warning;&lt;br&gt;3 - Average;&lt;br&gt;4 - High;&lt;br&gt;5 - Disaster.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* if `propagation_rule` is set|
|status|integer|Whether the service is in OK or problem state.&lt;br&gt;&lt;br&gt;If the service is in problem state, `status` is equal either to:&lt;br&gt;- the severity of the most critical problem;&lt;br&gt;- the highest status of a child service in problem state.&lt;br&gt;&lt;br&gt;If the service is in OK state, `status` is equal to: -1.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *read-only*|
|description|string|Description of the service.|
|uuid|string|Universal unique identifier, used for linking imported services to already existing ones. Auto-generated, if not given.|
|created_at|integer|Unix timestamp when service was created.|
|readonly|boolean|Access to the service.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - Read-write;&lt;br&gt;1 - Read-only.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *read-only*|</source>
      </trans-unit>
      <trans-unit id="c2041847" xml:space="preserve">
        <source>### Status rule

The status rule object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|type|integer|Condition for setting (New status) status.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - if at least (N) child services have (Status) status or above;&lt;br&gt;1 - if at least (N%) of child services have (Status) status or above;&lt;br&gt;2 - if less than (N) child services have (Status) status or below;&lt;br&gt;3 - if less than (N%) of child services have (Status) status or below;&lt;br&gt;4 - if weight of child services with (Status) status or above is at least (W);&lt;br&gt;5 - if weight of child services with (Status) status or above is at least (N%);&lt;br&gt;6 - if weight of child services with (Status) status or below is less than (W);&lt;br&gt;7 - if weight of child services with (Status) status or below is less than (N%).&lt;br&gt;&lt;br&gt;Where:&lt;br&gt;- N (W) is `limit_value`;&lt;br&gt;- (Status) is `limit_status`;&lt;br&gt;- (New status) is `new_status`.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|
|limit\_value|integer|Limit value.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;- for N and W: 1-100000;&lt;br&gt;- for N%: 1-100.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|
|limit\_status|integer|Limit status.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;-1 - OK;&lt;br&gt;0 - Not classified;&lt;br&gt;1 - Information;&lt;br&gt;2 - Warning;&lt;br&gt;3 - Average;&lt;br&gt;4 - High;&lt;br&gt;5 - Disaster.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|
|new\_status|integer|New status value.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - Not classified;&lt;br&gt;1 - Information;&lt;br&gt;2 - Warning;&lt;br&gt;3 - Average;&lt;br&gt;4 - High;&lt;br&gt;5 - Disaster.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|</source>
      </trans-unit>
      <trans-unit id="c301cfb2" xml:space="preserve">
        <source>### Service tag

The service tag object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|tag|string|Service tag name.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|
|value|string|Service tag value.|</source>
      </trans-unit>
      <trans-unit id="e6daa7b4" xml:space="preserve">
        <source>### Service alarm

::: noteclassic
Service alarms cannot be directly created, updated or
deleted via the Zabbix API.
:::

The service alarm objects represent a service's state change. It has
the following properties.

|Property |[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|clock|timestamp|Time when the service state change has happened.|
|value|integer|Status of the service.&lt;br&gt;&lt;br&gt;Refer to the [service status property](object#service) for a list of possible values.|</source>
      </trans-unit>
      <trans-unit id="6b88d3dc" xml:space="preserve">
        <source>### Problem tag

Problem tags allow linking services with problem events. The problem tag
object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|tag|string|Problem tag name.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|
|operator|integer|Mapping condition operator.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* equals;&lt;br&gt;2 - like.|
|value|string|Problem tag value.|</source>
      </trans-unit>
    </body>
  </file>
</xliff>
