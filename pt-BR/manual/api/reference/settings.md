[comment]: # translation:outdated

[comment]: # ({new-f805afb6})
# Settings

This class is designed to work with common administration settings.

Object references:\

-   [Settings](/manual/api/reference/settings/object#settings)

Available methods:\

-   [settings.get](/manual/api/reference/settings/get) - retrieve
    settings
-   [settings.update](/manual/api/reference/settings/update) - update
    settings

[comment]: # ({/new-f805afb6})
