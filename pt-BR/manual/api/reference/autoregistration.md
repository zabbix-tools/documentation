[comment]: # translation:outdated

[comment]: # ({new-17119dd8})
# Autoregistration

This class is designed to work with autoregistration.

Object references:\

-   [Autoregistration](/manual/api/reference/autoregistration/object#autoregistration)

Available methods:\

-   [autoregistration.get](/manual/api/reference/autoregistration/get) -
    retrieve autoregistration
-   [autoregistration.update](/manual/api/reference/autoregistration/update) -
    update autoregistration

[comment]: # ({/new-17119dd8})
