[comment]: # translation:outdated

[comment]: # ({new-6ea25642})
# image.create

[comment]: # ({/new-6ea25642})

[comment]: # ({new-565b9f0c})
### Description

`object image.create(object/array images)`

This method allows to create new images.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-565b9f0c})

[comment]: # ({new-8de82e8e})
### Parameters

`(object/array)` Images to create.

Additionally to the [standard image properties](object#image), the
method accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|**name**<br>(required)|string|Name of the image.|
|**imagetype**<br>(required)|integer|Type of image.<br><br>Possible values:<br>1 - *(default)* icon;<br>2 - background image.|
|**image**<br>(required)|string|Base64 encoded image. The maximum size of the encoded image is 1 MB. Maximum size can be adjusted by changing ZBX\_MAX\_IMAGE\_SIZE constant value.<br>Supported image formats are: PNG, JPEG, GIF.|

[comment]: # ({/new-8de82e8e})

[comment]: # ({new-ce55a727})
### Return values

`(object)` Returns an object containing the IDs of the created images
under the `imageids` property. The order of the returned IDs matches the
order of the passed images.

[comment]: # ({/new-ce55a727})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-1de081cc})
#### Create an image

Create a cloud icon.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "image.create",
    "params": {
        "imagetype": 1,
        "name": "Cloud_(24)",
        "image": "iVBORw0KGgoAAAANSUhEUgAAABgAAAANCAYAAACzbK7QAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAACmAAAApgBNtNH3wAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAIcSURBVDjLrZLbSxRRHMdPKiEiRQ89CD0s+N5j9BIMEf4Hg/jWexD2ZEXQbC9tWUFZimtLhswuZiVujK1UJmYXW9PaCUdtb83enL3P7s6ss5f5dc7EUsmqkPuFH3M4/Ob7+V0OAgC0UyDENFEU03rh1uNOs/lFG75o2i2/rkd9Y3Tgyj3HiaezbukdH9A/rP4E9vWi0u+Y4fuGnMf3DRgYc3Z/84YrQSkD3mgKhFAC+KAEK74Y2Lj3MjPoOokQ3Xyx/1GHeXCifbfO6lRPH/wi+AvZQhGSsgKxdB5CCRkCGPbDgMXBMbukTc4vK5/WRHizsq7fZl2LFuvE4T0BZDTXHtgv4TNUqlUolsqQL2qQwbDEXzBBTIJ7I4y/cfAENmHZF4XrY9Mc+X9HAFmoyXS2ddy1IOg6/KNyBcM0DFP/wFZFCcOy4N9Mw0YkCTOfhdL5AfZQXQBFn2t/ODXHC8FYVcoWjNEQ03qqwTJ5FdI44jg/msoB2Zd5ZKq3q6evA1FUS60bYyyj3AJf3V72HiLZJQxTtRLk1C2IYEg4mTNg63hPd1mOJd7Ict911OMNlWEf0nFxpCt16zcshTuLpGSwDDuPIfv0xzNyQYVGicC0cgUUDLM6Xp02lvvW/V2EBssnxlSGmWsxljw0znV9XfPLjTCW84r+cn7Jc8c2eWrbM6Wbe6/aTJbhJ/TNkWc9/xXW592Xb9iPkKnUfH8BKdLgFy0lDyQAAAAASUVORK5CYII="
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "imageids": [
            "188"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-1de081cc})

[comment]: # ({new-c146c166})
### Source

CImage::create() in *ui/include/classes/api/services/CImage.php*.

[comment]: # ({/new-c146c166})
