[comment]: # translation:outdated

[comment]: # ({new-f9e8f670})
# templategroup.delete

[comment]: # ({/new-f9e8f670})

[comment]: # ({new-b5ff62ed})
### Description

`object templategroup.delete(array templateGroupIds)`

This method allows to delete template groups.

A template group can not be deleted if it contains templates that belong to this group only.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-b5ff62ed})

[comment]: # ({new-55384a41})
### Parameters

`(array)` IDs of the template groups to delete.

[comment]: # ({/new-55384a41})

[comment]: # ({new-d49e0667})
### Return values

`(object)` Returns an object containing the IDs of the deleted template
groups under the `groupids` property.

[comment]: # ({/new-d49e0667})

[comment]: # ({new-e8653e4d})
### Examples

[comment]: # ({/new-e8653e4d})

[comment]: # ({new-1aca44e8})
#### Deleting multiple template groups

Delete two template groups.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "templategroup.delete",
    "params": [
        "107814",
        "107815"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "groupids": [
            "107814",
            "107815"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-1aca44e8})

[comment]: # ({new-918ba5f6})
### Source

CTemplateGroup::delete() in
*ui/include/classes/api/services/CTemplateGroup.php*.

[comment]: # ({/new-918ba5f6})
