[comment]: # translation:outdated

[comment]: # ({new-f343d85a})
# Token

This class is designed to work with tokens.

Object references:\

-   [Token](/manual/api/reference/token/object)

Available methods:\

-   [token.create](/manual/api/reference/token/create) - create new
    tokens
-   [token.delete](/manual/api/reference/token/delete) - delete tokens
-   [token.get](/manual/api/reference/token/get) - retrieve tokens
-   [token.update](/manual/api/reference/token/update) - update tokens
-   [token.generate](/manual/api/reference/token/generate) - generate
    tokens

[comment]: # ({/new-f343d85a})
