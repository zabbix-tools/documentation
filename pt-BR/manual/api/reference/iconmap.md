[comment]: # translation:outdated

[comment]: # ({new-5ee95992})
# Icon map

This class is designed to work with icon maps.

Object references:\

-   [Icon map](/manual/api/reference/iconmap/object#icon_map)
-   [Icon mapping](/manual/api/reference/iconmap/object#icon_mapping)

Available methods:\

-   [iconmap.create](/manual/api/reference/iconmap/create) - create new
    icon maps
-   [iconmap.delete](/manual/api/reference/iconmap/delete) - delete icon
    maps
-   [iconmap.get](/manual/api/reference/iconmap/get) - retrieve icon
    maps
-   [iconmap.update](/manual/api/reference/iconmap/update) - update icon
    maps

[comment]: # ({/new-5ee95992})
