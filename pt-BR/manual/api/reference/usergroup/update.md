[comment]: # translation:outdated

[comment]: # ({new-9d4a1084})
# usergroup.update

[comment]: # ({/new-9d4a1084})

[comment]: # ({new-9fdfde6c})
### Description

`object usergroup.update(object/array userGroups)`

This method allows to update existing user groups.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-9fdfde6c})

[comment]: # ({new-93a5d635})
### Parameters

`(object/array)` User group properties to be updated.

The `usrgrpid` property must be defined for each user group, all other
properties are optional. Only the passed properties will be updated, all
others will remain unchanged.

Additionally to the [standard user group properties](object#user_group),
the method accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|rights|object/array|[Permissions](/manual/api/reference/usergroup/object#permission) to replace the current permissions assigned to the user group.|
|tag\_filters|array|[Tag based permissions](/manual/api/reference/usergroup/object#tag_based_permission) to assign to the group.|
|users|object/array|[Users](/manual/api/reference/user/create) to add to the user group.<br><br>The user must have the `userid` property defined.|

[comment]: # ({/new-93a5d635})

[comment]: # ({new-c04afd7f})
### Return values

`(object)` Returns an object containing the IDs of the updated user
groups under the `usrgrpids` property.

[comment]: # ({/new-c04afd7f})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-e1a75e7b})
#### Disabling a user group

Disable a user group.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "usergroup.update",
    "params": {
        "usrgrpid": "17",
        "users_status": "1"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "usrgrpids": [
            "17"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-e1a75e7b})

[comment]: # ({new-d5c7ed4f})
### See also

-   [Permission](object#permission)

[comment]: # ({/new-d5c7ed4f})

[comment]: # ({new-09e213c5})
### Source

CUserGroup::update() in
*ui/include/classes/api/services/CUserGroup.php*.

[comment]: # ({/new-09e213c5})
