[comment]: # translation:outdated

[comment]: # ({new-4f2ac53b})
# script.update

[comment]: # ({/new-4f2ac53b})

[comment]: # ({new-3bbc6786})
### Description

`object script.update(object/array scripts)`

This method allows to update existing scripts.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-3bbc6786})

[comment]: # ({new-8f315bab})
### Parameters

`(object/array)` [Script properties](object#script) to be updated.

The `scriptid` property must be defined for each script, all other
properties are optional. Only the passed properties will be updated, all
others will remain unchanged. An exception is `type` property change
from 5 (Webhook) to other: the `parameters` property will be cleaned.

[comment]: # ({/new-8f315bab})

[comment]: # ({new-5062432a})
### Return values

`(object)` Returns an object containing the IDs of the updated scripts
under the `scriptids` property.

[comment]: # ({/new-5062432a})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-4f3506ff})
#### Change script command

Change the command of the script to "/bin/ping -c 10 {HOST.CONN}
2>&1".

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "script.update",
    "params": {
        "scriptid": "1",
        "command": "/bin/ping -c 10 {HOST.CONN} 2>&1"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "scriptids": [
            "1"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-4f3506ff})

[comment]: # ({new-cded8a11})
### Source

CScript::update() in *ui/include/classes/api/services/CScript.php*.

[comment]: # ({/new-cded8a11})
