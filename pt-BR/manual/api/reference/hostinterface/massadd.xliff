<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="pt-BR" datatype="plaintext" original="manual/api/reference/hostinterface/massadd.md">
    <body>
      <trans-unit id="fff19f5f" xml:space="preserve">
        <source># hostinterface.massadd</source>
      </trans-unit>
      <trans-unit id="30999da7" xml:space="preserve">
        <source>### Description

`object hostinterface.massadd(object parameters)`

This method allows to simultaneously add host interfaces to multiple
hosts.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::</source>
      </trans-unit>
      <trans-unit id="35c0864e" xml:space="preserve">
        <source>### Parameters

`(object)` Parameters containing the host interfaces to be created on
the given hosts.

The method accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|interfaces|object/array|[Host interfaces](/manual/api/reference/hostinterface/object) to create on the given hosts.&lt;br&gt;&lt;br&gt;[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):&lt;br&gt;- *required*|
|hosts|object/array|Hosts to be updated.&lt;br&gt;&lt;br&gt;The hosts must have the `hostid` property defined.&lt;br&gt;&lt;br&gt;[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):&lt;br&gt;- *required*|</source>
      </trans-unit>
      <trans-unit id="88283807" xml:space="preserve">
        <source>### Return values

`(object)` Returns an object containing the IDs of the created host
interfaces under the `interfaceids` property.</source>
      </trans-unit>
      <trans-unit id="b41637d2" xml:space="preserve">
        <source>### Examples</source>
      </trans-unit>
      <trans-unit id="ad87458a" xml:space="preserve">
        <source>#### Creating interfaces

Create an interface on two hosts.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "hostinterface.massadd",
    "params": {
        "hosts": [
            {
                "hostid": "30050"
            },
            {
                "hostid": "30052"
            }
        ],
        "interfaces": {
            "dns": "",
            "ip": "127.0.0.1",
            "main": 0,
            "port": "10050",
            "type": 1,
            "useip": 1
        }
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "interfaceids": [
            "30069",
            "30070"
        ]
    },
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="a2a46209" xml:space="preserve">
        <source>### See also

-   [hostinterface.create](create)
-   [host.massadd](/manual/api/reference/host/massadd)
-   [Host](/manual/api/reference/host/object#host)</source>
      </trans-unit>
      <trans-unit id="9b65afcc" xml:space="preserve">
        <source>### Source

CHostInterface::massAdd() in
*ui/include/classes/api/services/CHostInterface.php*.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
