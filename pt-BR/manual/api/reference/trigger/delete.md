[comment]: # translation:outdated

[comment]: # ({new-bce5854d})
# trigger.delete

[comment]: # ({/new-bce5854d})

[comment]: # ({new-adb8b388})
### Description

`object trigger.delete(array triggerIds)`

This method allows to delete triggers.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-adb8b388})

[comment]: # ({new-fc913e85})
### Parameters

`(array)` IDs of the triggers to delete.

[comment]: # ({/new-fc913e85})

[comment]: # ({new-a2fb984a})
### Return values

`(object)` Returns an object containing the IDs of the deleted triggers
under the `triggerids` property.

[comment]: # ({/new-a2fb984a})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-752e2ebb})
#### Delete multiple triggers

Delete two triggers.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "trigger.delete",
    "params": [
        "12002",
        "12003"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "triggerids": [
            "12002",
            "12003"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-752e2ebb})

[comment]: # ({new-087c0548})
### Source

CTrigger::delete() in *ui/include/classes/api/services/CTrigger.php*.

[comment]: # ({/new-087c0548})
