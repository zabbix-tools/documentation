[comment]: # translation:outdated

[comment]: # ({new-dbb7dd13})
# > Item prototype object

The following objects are directly related to the `itemprototype` API.

[comment]: # ({/new-dbb7dd13})

[comment]: # ({new-36a2631c})
### Item prototype

The item prototype object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|itemid|string|*(readonly)* ID of the item prototype.|
|**delay**<br>(required)|string|Update interval of the item prototype. Accepts seconds or a time unit with suffix (30s,1m,2h,1d).<br>Optionally one or more [custom intervals](/manual/config/items/item/custom_intervals) can be specified either as flexible intervals or scheduling.<br>Multiple intervals are separated by a semicolon.<br>User macros and LLD macros may be used. A single macro has to fill the whole field. Multiple macros in a field or macros mixed with text are not supported.<br>Flexible intervals may be written as two macros separated by a forward slash (e.g. `{$FLEX_INTERVAL}/{$FLEX_PERIOD}`).<br><br>Optional for Zabbix trapper, dependent items and for Zabbix agent (active) with `mqtt.get` key.|
|**hostid**<br>(required)|string|ID of the host that the item prototype belongs to.<br><br>For update operations this field is *readonly*.|
|**ruleid**<br>(required)|string|ID of the LLD rule that the item belongs to.<br><br>For update operations this field is *readonly*.|
|**interfaceid**<br>(required)|string|ID of the item prototype's host interface. Used only for host item prototypes.<br><br>Optional for Zabbix agent (active), Zabbix internal, Zabbix trapper, Dependent item, database monitor and calculated item prototypes.|
|**key\_**<br>(required)|string|Item prototype key.|
|**name**<br>(required)|string|Name of the item prototype.|
|**type**<br>(required)|integer|Type of the item prototype.<br><br>Possible values:<br>0 - Zabbix agent;<br>2 - Zabbix trapper;<br>3 - simple check;<br>5 - Zabbix internal;<br>7 - Zabbix agent (active);<br>10 - external check;<br>11 - database monitor;<br>12 - IPMI agent;<br>13 - SSH agent;<br>14 - TELNET agent;<br>15 - calculated;<br>16 - JMX agent;<br>17 - SNMP trap;<br>18 - Dependent item;<br>19 - HTTP agent;<br>20 - SNMP agent;<br>21 - Script.|
|**url**<br>(required)|string|URL string required only for HTTP agent item prototypes. Supports LLD macros, user macros, {HOST.IP}, {HOST.CONN}, {HOST.DNS}, {HOST.HOST}, {HOST.NAME}, {ITEM.ID}, {ITEM.KEY}.|
|**value\_type**<br>(required)|integer|Type of information of the item prototype.<br><br>Possible values:<br>0 - numeric float;<br>1 - character;<br>2 - log;<br>3 - numeric unsigned;<br>4 - text.|
|allow\_traps|integer|HTTP agent item prototype field. Allow to populate value as in trapper item type also.<br><br>0 - *(default)* Do not allow to accept incoming data.<br>1 - Allow to accept incoming data.|
|authtype|integer|Used only by SSH agent item prototypes or HTTP agent item prototypes.<br><br>SSH agent authentication method possible values:<br>0 - *(default)* password;<br>1 - public key.<br><br>HTTP agent authentication method possible values:<br>0 - *(default)* none<br>1 - basic<br>2 - NTLM<br>3 - Kerberos|
|description|string|Description of the item prototype.|
|follow\_redirects|integer|HTTP agent item prototype field. Follow response redirects while pooling data.<br><br>0 - Do not follow redirects.<br>1 - *(default)* Follow redirects.|
|headers|object|HTTP agent item prototype field. Object with HTTP(S) request headers, where header name is used as key and header value as value.<br><br>Example:<br>{ "User-Agent": "Zabbix" }|
|history|string|A time unit of how long the history data should be stored. Also accepts user macro and LLD macro.<br><br>Default: 90d.|
|http\_proxy|string|HTTP agent item prototype field. HTTP(S) proxy connection string.|
|ipmi\_sensor|string|IPMI sensor. Used only by IPMI item prototypes.|
|jmx\_endpoint|string|JMX agent custom connection string.<br><br>Default value:<br>service:jmx:rmi:///jndi/rmi://{HOST.CONN}:{HOST.PORT}/jmxrmi|
|logtimefmt|string|Format of the time in log entries. Used only by log item prototypes.|
|master\_itemid|integer|Master item ID.<br>Recursion up to 3 dependent items and item prototypes and maximum count of dependent items and item prototypes equal to 29999 are allowed.<br><br>Required by Dependent items.|
|output\_format|integer|HTTP agent item prototype field. Should response be converted to JSON.<br><br>0 - *(default)* Store raw.<br>1 - Convert to JSON.|
|params|string|Additional parameters depending on the type of the item prototype:<br>- executed script for SSH and Telnet item prototypes;<br>- SQL query for database monitor item prototypes;<br>- formula for calculated item prototypes.|
|parameters|array|Additional parameters for script item prototypes. Array of objects with 'name' and 'value' properties, where name must be unique.|
|password|string|Password for authentication. Used by simple check, SSH, Telnet, database monitor, JMX and HTTP agent item prototypes.|
|post\_type|integer|HTTP agent item prototype field. Type of post data body stored in posts property.<br><br>0 - *(default)* Raw data.<br>2 - JSON data.<br>3 - XML data.|
|posts|string|HTTP agent item prototype field. HTTP(S) request body data. Used with post\_type.|
|privatekey|string|Name of the private key file.|
|publickey|string|Name of the public key file.|
|query\_fields|array|HTTP agent item prototype field. Query parameters. Array of objects with 'key':'value' pairs, where value can be empty string.|
|request\_method|integer|HTTP agent item prototype field. Type of request method.<br><br>0 - *(default)* GET<br>1 - POST<br>2 - PUT<br>3 - HEAD|
|retrieve\_mode|integer|HTTP agent item prototype field. What part of response should be stored.<br><br>0 - *(default)* Body.<br>1 - Headers.<br>2 - Both body and headers will be stored.<br><br>For request\_method HEAD only 1 is allowed value.|
|snmp\_oid|string|SNMP OID.|
|ssl\_cert\_file|string|HTTP agent item prototype field. Public SSL Key file path.|
|ssl\_key\_file|string|HTTP agent item prototype field. Private SSL Key file path.|
|ssl\_key\_password|string|HTTP agent item prototype field. Password for SSL Key file.|
|status|integer|Status of the item prototype.<br><br>Possible values:<br>0 - *(default)* enabled item prototype;<br>1 - disabled item prototype;<br>3 - unsupported item prototype.|
|status\_codes|string|HTTP agent item prototype field. Ranges of required HTTP status codes separated by commas. Also supports user macros or LLD macros as part of comma separated list.<br><br>Example: 200,200-{$M},{$M},200-400|
|templateid|string|(readonly) ID of the parent template item prototype.|
|timeout|string|Item data polling request timeout. Used for HTTP agent and script item prototypes. Supports user macros and LLD macros.<br><br>default: 3s<br>maximum value: 60s|
|trapper\_hosts|string|Allowed hosts. Used by trapper item prototypes or HTTP item prototypes.|
|trends|string|A time unit of how long the trends data should be stored. Also accepts user macro and LLD macro.<br><br>Default: 365d.|
|units|string|Value units.|
|username|string|Username for authentication. Used by simple check, SSH, Telnet, database monitor, JMX and HTTP agent item prototypes.<br><br>Required by SSH and Telnet item prototypes.|
|uuid|string|Universal unique identifier, used for linking imported item prototypes to already existing ones. Used only for item prototypes on templates. Auto-generated, if not given.<br><br>For update operations this field is *readonly*.|
|valuemapid|string|ID of the associated value map.|
|verify\_host|integer|HTTP agent item prototype field. Validate host name in URL is in Common Name field or a Subject Alternate Name field of host certificate.<br><br>0 - *(default)* Do not validate.<br>1 - Validate.<br>|
|verify\_peer|integer|HTTP agent item prototype field. Validate is host certificate authentic.<br><br>0 - *(default)* Do not validate.<br>1 - Validate.|
|discover|integer|Item prototype discovery status.<br><br>Possible values:<br>0 - *(default)* new items will be discovered;<br>1 - new items will not be discovered and existing items will be marked as lost.|

[comment]: # ({/new-36a2631c})

[comment]: # ({new-66f440ed})
### Item prototype tag

The item prototype tag object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**tag**<br>(required)|string|Item prototype tag name.|
|value|string|Item prototype tag value.|

[comment]: # ({/new-66f440ed})

[comment]: # ({new-ad34a8b2})
### Item prototype preprocessing

The item prototype preprocessing object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**type**<br>(required)|integer|The preprocessing option type.<br><br>Possible values:<br>1 - Custom multiplier;<br>2 - Right trim;<br>3 - Left trim;<br>4 - Trim;<br>5 - Regular expression matching;<br>6 - Boolean to decimal;<br>7 - Octal to decimal;<br>8 - Hexadecimal to decimal;<br>9 - Simple change;<br>10 - Change per second;<br>11 - XML XPath;<br>12 - JSONPath;<br>13 - In range;<br>14 - Matches regular expression;<br>15 - Does not match regular expression;<br>16 - Check for error in JSON;<br>17 - Check for error in XML;<br>18 - Check for error using regular expression;<br>19 - Discard unchanged;<br>20 - Discard unchanged with heartbeat;<br>21 - JavaScript;<br>22 - Prometheus pattern;<br>23 - Prometheus to JSON;<br>24 - CSV to JSON;<br>25 - Replace;<br>26 - Check unsupported;<br>27 - XML to JSON.|
|**params**<br>(required)|string|Additional parameters used by preprocessing option. Multiple parameters are separated by LF (\\n) character.|
|**error\_handler**<br>(required)|integer|Action type used in case of preprocessing step failure.<br><br>Possible values:<br>0 - Error message is set by Zabbix server;<br>1 - Discard value;<br>2 - Set custom value;<br>3 - Set custom error message.|
|**error\_handler\_params**<br>(required)|string|Error handler parameters. Used with `error_handler`.<br><br>Must be empty, if `error_handler` is 0 or 1.<br>Can be empty if, `error_handler` is 2.<br>Cannot be empty, if `error_handler` is 3.|

The following parameters and error handlers are supported for each
preprocessing type.

|Preprocessing type|Name|Parameter 1|Parameter 2|Parameter 3|Supported error handlers|
|------------------|----|-----------|-----------|-----------|------------------------|
|1|Custom multiplier|number^1,\ 6^|<|<|0, 1, 2, 3|
|2|Right trim|list of characters^2^|<|<|<|
|3|Left trim|list of characters^2^|<|<|<|
|4|Trim|list of characters^2^|<|<|<|
|5|Regular expression|pattern^3^|output^2^|<|0, 1, 2, 3|
|6|Boolean to decimal|<|<|<|0, 1, 2, 3|
|7|Octal to decimal|<|<|<|0, 1, 2, 3|
|8|Hexadecimal to decimal|<|<|<|0, 1, 2, 3|
|9|Simple change|<|<|<|0, 1, 2, 3|
|10|Change per second|<|<|<|0, 1, 2, 3|
|11|XML XPath|path^4^|<|<|0, 1, 2, 3|
|12|JSONPath|path^4^|<|<|0, 1, 2, 3|
|13|In range|min^1,\ 6^|max^1,\ 6^|<|0, 1, 2, 3|
|14|Matches regular expression|pattern^3^|<|<|0, 1, 2, 3|
|15|Does not match regular expression|pattern^3^|<|<|0, 1, 2, 3|
|16|Check for error in JSON|path^4^|<|<|0, 1, 2, 3|
|17|Check for error in XML|path^4^|<|<|0, 1, 2, 3|
|18|Check for error using regular expression|pattern^3^|output^2^|<|0, 1, 2, 3|
|19|Discard unchanged|<|<|<|<|
|20|Discard unchanged with heartbeat|seconds^5,\ 6^|<|<|<|
|21|JavaScript|script^2^|<|<|<|
|22|Prometheus pattern|pattern^6,\ 7^|output^6,\ 8^|<|0, 1, 2, 3|
|23|Prometheus to JSON|pattern^6,\ 7^|<|<|0, 1, 2, 3|
|24|CSV to JSON|character^2^|character^2^|0,1|0, 1, 2, 3|
|25|Replace|search string^2^|replacement^2^|<|<|
|26|Check unsupported|<|<|<|1, 2, 3|
|27|XML to JSON|<|<|<|0, 1, 2, 3|

^1^ integer or floating-point number\
^2^ string\
^3^ regular expression\
^4^ JSONPath or XML XPath\
^5^ positive integer (with support of time suffixes, e.g. 30s, 1m, 2h,
1d)\
^6^ user macro, LLD macro\
^7^ Prometheus pattern following the syntax:
`<metric name>{<label name>="<label value>", ...} == <value>`. Each
Prometheus pattern component (metric, label name, label value and metric
value) can be user macro or LLD macro.\
^8^ Prometheus output following the syntax: `<label name>`.

[comment]: # ({/new-ad34a8b2})
