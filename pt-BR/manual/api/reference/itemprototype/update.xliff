<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="pt-BR" datatype="plaintext" original="manual/api/reference/itemprototype/update.md">
    <body>
      <trans-unit id="cc00d3db" xml:space="preserve">
        <source># itemprototype.update</source>
      </trans-unit>
      <trans-unit id="63442cbb" xml:space="preserve">
        <source>### Description

`object itemprototype.update(object/array itemPrototypes)`

This method allows to update existing item prototypes.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::</source>
      </trans-unit>
      <trans-unit id="16fcb884" xml:space="preserve">
        <source>### Parameters

`(object/array)` Item prototype properties to be updated.

The `itemid` property must be defined for each item prototype, all other
properties are optional. Only the passed properties will be updated, all
others will remain unchanged.

Additionally to the [standard item prototype
properties](object#item_prototype), the method accepts the following
parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|preprocessing|array|Item prototype [preprocessing](/manual/api/reference/itemprototype/object#item_prototype_preprocessing) options to replace the current preprocessing options.&lt;br&gt;&lt;br&gt;[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):&lt;br&gt;- *read-only* for inherited objects|
|tags|array|Item prototype [tags](/manual/api/reference/itemprototype/object#item_prototype_tag).|</source>
      </trans-unit>
      <trans-unit id="f7f1feb9" xml:space="preserve">
        <source>### Return values

`(object)` Returns an object containing the IDs of the updated item
prototypes under the `itemids` property.</source>
      </trans-unit>
      <trans-unit id="b41637d2" xml:space="preserve">
        <source>### Examples</source>
      </trans-unit>
      <trans-unit id="48cea947" xml:space="preserve">
        <source>#### Changing the interface of an item prototype

Change the host interface that will be used by discovered items.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "itemprototype.update",
    "params": {
        "itemid": "27428",
        "interfaceid": "132"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "27428"
        ]
    },
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="e1d8fc73" xml:space="preserve">
        <source>#### Update dependent item prototype

Update Dependent item prototype with new Master item prototype ID. Only
dependencies on same host (template/discovery rule) are allowed,
therefore Master and Dependent item should have same hostid and ruleid.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "itemprototype.update",
    "params": {
        "master_itemid": "25570",
        "itemid": "189030"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "189030"
        ]
    },
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="d7fe7206" xml:space="preserve">
        <source>#### Update HTTP agent item prototype

Change query fields and remove all custom headers.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "itemprototype.update",
    "params": {
        "itemid":"28305",
        "query_fields": [
            {
                "random": "qwertyuiopasdfghjklzxcvbnm"
            }
        ],
        "headers": []
    }
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "28305"
        ]
    },
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="e5fc44e0" xml:space="preserve">
        <source>#### Updating item preprocessing options

Update an item prototype with item preprocessing rule “Custom
multiplier”.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "itemprototype.update",
    "params": {
        "itemid": "44211",
        "preprocessing": [
            {
                "type": 1,
                "params": "4",
                "error_handler": 2,
                "error_handler_params": "5"
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "44211"
        ]
    },
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="15ba4525" xml:space="preserve">
        <source>#### Updating a script item prototype

Update a script item prototype with a different script and remove
unnecessary parameters that were used by previous script.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "itemprototype.update",
    "params": {
        "itemid": "23865",
        "parameters": [],
        "script": "Zabbix.log(3, 'Log test');\nreturn 1;"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "23865"
        ]
    },
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="bc6956c2" xml:space="preserve">
        <source>### Source

CItemPrototype::update() in
*ui/include/classes/api/services/CItemPrototype.php*.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
