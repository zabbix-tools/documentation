[comment]: # translation:outdated

[comment]: # ({new-faff6037})
# dashboard.delete

[comment]: # ({/new-faff6037})

[comment]: # ({new-6f29484b})
### Description

`object dashboard.delete(array dashboardids)`

This method allows to delete dashboards.

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-6f29484b})

[comment]: # ({new-4fd471a4})
### Parameters

`(array)` IDs of the dashboards to delete.

[comment]: # ({/new-4fd471a4})

[comment]: # ({new-c04394d8})
### Return values

`(object)` Returns an object containing the IDs of the deleted
dashboards under the `dashboardids` property.

[comment]: # ({/new-c04394d8})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-4d5d4a5d})
#### Deleting multiple dashboards

Delete two dashboards.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "dashboard.delete",
    "params": [
        "2",
        "3"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "dashboardids": [
            "2",
            "3"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-4d5d4a5d})

[comment]: # ({new-2bc6b04b})
### Source

CDashboard::delete() in
*ui/include/classes/api/services/CDashboard.php*.

[comment]: # ({/new-2bc6b04b})
