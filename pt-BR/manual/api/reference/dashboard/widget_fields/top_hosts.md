[comment]: # translation:outdated

[comment]: # ({new-978cbb20})
# 21 Top hosts

[comment]: # ({/new-978cbb20})

[comment]: # ({new-c5138763})
### Description

These parameters and the possible property values for the respective dashboard widget field objects allow to configure
the [*Top Hosts*](/manual/web_interface/frontend_sections/dashboards/widgets/top_hosts) widget in `dashboard.create` and `dashboard.update` methods.

[comment]: # ({/new-c5138763})

[comment]: # ({new-a3febace})
### Parameters

The following parameters are supported for the *Top Hosts* widget.

|Parameter|<|[type](/manual/api/reference/dashboard/object#dashboard-widget-field)|name|value|
|-|--------|--|--------|-------------------------------|
|*Refresh interval*|<|0|rf_rate|0 - No refresh;<br>10 - 10 seconds;<br>30 - 30 seconds;<br>60 - *(default)* 1 minute;<br>120 - 2 minutes;<br>600 - 10 minutes;<br>900 - 15 minutes.|
|*Host groups*|<|2|groupids|[Host group](/manual/api/reference/hostgroup/get) ID.<br><br>Note: To configure multiple host groups, create a dashboard widget field object for each host group.|
|*Hosts*|<|3|hostids|[Host](/manual/api/reference/host/get) ID.<br><br>Note: To configure multiple hosts, create a dashboard widget field object for each host. For multiple hosts, the parameter *Host groups* must either be not configured at all or configured with at least one host group that the configured hosts belong to.|
|*Host Tags* (the number in the property name (e.g. tags.tag.0) references tag order in the tag evaluation list)|<|<|<|<|
|<|*Evaluation type*|0|evaltype|0 - *(default)* And/Or;<br>2 - Or.|
|^|*Tag name*|1|tags.tag.0|Any string value.<br><br>Parameter *Tag name* required if configuring *Tags*.|
|^|*Operator*|0|tags.operator.0|0 - Contains;<br>1 - Equals;<br>2 - Does not contain;<br>3 - Does not equal;<br>4 - Exists;<br>5 - Does not exist.<br><br>Parameter *Operator* required if configuring *Tags*.|
|^|*Tag value*|1|tags.value.0|Any string value.<br><br>Parameter *Tag value* required if configuring *Tags*.|
|*Columns* (see below)|<|<|<|<|
|*Order*|<|0|order|2 - *(default)* Top N;<br>3 - Bottom N.|
|*Order column*|<|0|column|Column numeric value from the configured columns.|
|*Host count*|<|0|count|Valid values range from 1-100.<br><br>Default: 10.|

[comment]: # ({/new-a3febace})

[comment]: # ({new-8266fc04})
#### Columns

Columns have common parameters and additional parameters depending on the configuration of the parameter *Data*.

::: noteclassic
For all parameters related to columns the number in the property name (e.g. columns.name.0) references a column for which the parameter is configured.
:::

[comment]: # ({/new-8266fc04})

[comment]: # ({new-bba2be85})
The following parameters are supported for all columns.

|Parameter|[type](/manual/api/reference/dashboard/object#dashboard-widget-field)|name|value|
|-----|-|-----|-------------------|
|*Name*|1|columns.name.0|Any string value.|
|***Data***<br>(required)|0|columns.data.0|1 - Item value;<br>2 - Host name;<br>3 - Text.|
|***Base color***<br>(required)|1|columns.base_color.0|Hexadecimal color code (e.g. `FF0000`).|

[comment]: # ({/new-bba2be85})

[comment]: # ({new-e8b395eb})
##### Item value

The following parameters are supported if *Data* is set to "Item value".

::: noteclassic
The first number in the *Thresholds* property name (e.g. columnsthresholds.color.0.0) references the column for which thresholds are configured,
while the second number references threshold place in a list, sorted in ascending order.
However, if thresholds are configured in a different order, the values will be sorted in ascending order after updating widget configuration in Zabbix frontend
(e.g. `"threshold.threshold.0":"5"` → `"threshold.threshold.0":"1"`; `"threshold.threshold.1":"1"` → `"threshold.threshold.1": "5"`).
:::

|Parameter|<|[type](/manual/api/reference/dashboard/object#dashboard-widget-field)|name|value|
|-|--------|--|--------|-------------------------------|
|*Item*|<|1 |columns.item.0|Valid item name.|
|***Time shift***<br>(required)|<|1|columns.timeshift.0|Valid numeric or time string value (e.g. `3600` or `1h`).<br>You may use [time suffixes](/manual/appendix/suffixes#time-suffixes). Negative values are allowed.|
|*Aggregation function*|<|0|columns.aggregate_function.0|0 - *(default)* none;<br>1 - min;<br>2 - max;<br>3 - avg;<br>4 - count;<br>5 - sum;<br>6 - first;<br>7 - last.|
|*Aggregation interval*|<|1|columns.aggregate_interval.0|Valid time string (e.g. `3600`, `1h`, etc.).<br>You may use [time suffixes](/manual/appendix/suffixes#time-suffixes).<br><br>Parameter *Aggregation interval* not available if *Aggregation function* is set to *none*.<br><br>Default: `1h`.|
|*Display*|<|0|columns.display.0|1 - *(default)* As is;<br>2 - Bar;<br>3 - Indicators.|
|*Min*|<|1|columns.min.0|Any numeric value.<br><br>Parameter *Min* not available if *Display* is set to "As is".|
|*Max*|<|1|columns.max.0|Any numeric value.<br><br>Parameter *Max* not available if *Display* is set to "As is".|
|*History data*|<|0|columns.history.0|1 - *(default)* Auto;<br>2 - History;<br>3 - Trends.|
|*Thresholds*|<|<|<|<|
|<|*Color*|1|columnsthresholds.color.0.0|Hexadecimal color code (e.g. `FF0000`).<br><br>Default: `""` (empty).|
|^|*Threshold*|1|columnsthresholds.threshold.0.0|Any string value.|

[comment]: # ({/new-e8b395eb})

[comment]: # ({new-18fd738d})
##### Text

The following parameters are supported if *Data* is set to "Text".

|Parameter|[type](/manual/api/reference/dashboard/object#dashboard-widget-field)|name|value|
|-----|-|-----|-------------------|
|*Text*|1|columns.text.0|Any string value, including macros.<br>Supported macros: {HOST.\*}, {INVENTORY.\*}.<br><br>Parameter *Text* required if *Data* is set to "Text".|

[comment]: # ({/new-18fd738d})

[comment]: # ({new-c274247c})
### Examples

The following examples aim to only describe the configuration of the dashboard widget field objects for the *Top hosts* widget.
For more information on configuring a dashboard, see [`dashboard.create`](/manual/api/reference/dashboard/create).

[comment]: # ({/new-c274247c})

[comment]: # ({new-2e9b79c3})
#### Configuring a *Top hosts* widget

Configure a *Top hosts* widget that displays top hosts by CPU utilization in host group "4".
In addition, configure the following custom columns: "Host name", "Utilization", "1m avg", "5m avg", "15m avg", "Processes".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "dashboard.create",
    "params": {
        "name": "My dashboard",
        "display_period": 30,
        "auto_start": 1,
        "pages": [
            {
                "widgets": [
                    {
                        "type": "tophosts",
                        "name": "Top hosts",
                        "x": 0,
                        "y": 0,
                        "width": 12,
                        "height": 5,
                        "view_mode": 0,
                        "fields": [
                            {
                                "type": 2,
                                "name": "groupids",
                                "value": 4
                            },
                            {
                                "type": 1,
                                "name": "columns.name.0",
                                "value": ""
                            },
                            {
                                "type": 0,
                                "name": "columns.data.0",
                                "value": 2
                            },
                            {
                                "type": 1,
                                "name": "columns.base_color.0",
                                "value": "FFFFFF"
                            },
                            {
                                "type": 1,
                                "name": "columns.timeshift.0",
                                "value": ""
                            },
                            {
                                "type": 1,
                                "name": "columns.item.0",
                                "value": "System name"
                            },
                            {
                                "type": 1,
                                "name": "columns.name.1",
                                "value": "Utilization"
                            },
                            {
                                "type": 0,
                                "name": "columns.data.1",
                                "value": 1
                            },
                            {
                                "type": 1,
                                "name": "columns.base_color.1",
                                "value": "4CAF50"
                            },
                            {
                                "type": 1,
                                "name": "columns.timeshift.1",
                                "value": ""
                            },
                            {
                                "type": 1,
                                "name": "columns.item.1",
                                "value": "CPU utilization"
                            },
                            {
                                "type": 0,
                                "name": "columns.display.1",
                                "value": 3
                            },
                            {
                                "type": 1,
                                "name": "columns.min.1",
                                "value": "0"
                            },
                            {
                                "type": 1,
                                "name": "columns.max.1",
                                "value": "100"
                            },
                            {
                                "type": 1,
                                "name": "columnsthresholds.color.1.0",
                                "value": "FFFF00"
                            },
                            {
                                "type": 1,
                                "name": "columnsthresholds.threshold.1.0",
                                "value": "50"
                            },
                            {
                                "type": 1,
                                "name": "columnsthresholds.color.1.1",
                                "value": "FF8000"
                            },
                            {
                                "type": 1,
                                "name": "columnsthresholds.threshold.1.1",
                                "value": "80"
                            },
                            {
                                "type": 1,
                                "name": "columnsthresholds.color.1.2",
                                "value": "FF4000"
                            },
                            {
                                "type": 1,
                                "name": "columnsthresholds.threshold.1.2",
                                "value": "90"
                            },
                            {
                                "type": 1,
                                "name": "columns.name.2",
                                "value": "1m avg"
                            },
                            {
                                "type": 0,
                                "name": "columns.data.2",
                                "value": 1
                            },
                            {
                                "type": 1,
                                "name": "columns.base_color.2",
                                "value": "FFFFFF"
                            },
                            {
                                "type": 1,
                                "name": "columns.timeshift.2",
                                "value": ""
                            },
                            {
                                "type": 1,
                                "name": "columns.item.2",
                                "value": "Load average (1m avg)"
                            },
                            {
                                "type": 1,
                                "name": "columns.name.3",
                                "value": "5m avg"
                            },
                            {
                                "type": 0,
                                "name": "columns.data.3",
                                "value": 1
                            },
                            {
                                "type": 1,
                                "name": "columns.base_color.3",
                                "value": "FFFFFF"
                            },
                            {
                                "type": 1,
                                "name": "columns.timeshift.3",
                                "value": ""
                            },
                            {
                                "type": 1,
                                "name": "columns.item.3",
                                "value": "Load average (5m avg)"
                            },
                            {
                                "type": 1,
                                "name": "columns.name.4",
                                "value": "15m avg"
                            },
                            {
                                "type": 0,
                                "name": "columns.data.4",
                                "value": 1
                            },
                            {
                                "type": 1,
                                "name": "columns.base_color.4",
                                "value": "FFFFFF"
                            },
                            {
                                "type": 1,
                                "name": "columns.timeshift.4",
                                "value": ""
                            },
                            {
                                "type": 1,
                                "name": "columns.item.4",
                                "value": "Load average (15m avg)"
                            },
                            {
                                "type": 1,
                                "name": "columns.name.5",
                                "value": "Processes"
                            },
                            {
                                "type": 0,
                                "name": "columns.data.5",
                                "value": 1
                            },
                            {
                                "type": 1,
                                "name": "columns.base_color.5",
                                "value": "FFFFFF"
                            },
                            {
                                "type": 1,
                                "name": "columns.timeshift.5",
                                "value": ""
                            },
                            {
                                "type": 1,
                                "name": "columns.item.5",
                                "value": "Number of processes"
                            },
                            {
                                "type": 0,
                                "name": "column",
                                "value": 1
                            }
                        ]
                    }
                ]
            }
        ],
        "userGroups": [
            {
                "usrgrpid": 7,
                "permission": 2
            }
        ],
        "users": [
            {
                "userid": 1,
                "permission": 3
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "dashboardids": [
            "3"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-2e9b79c3})

[comment]: # ({new-d8c32fe2})
### See also

-   [Dashboard widget field](/manual/api/reference/dashboard/object#dashboard-widget-field)
-   [`dashboard.create`](/manual/api/reference/dashboard/create)
-   [`dashboard.update`](/manual/api/reference/dashboard/update)

[comment]: # ({/new-d8c32fe2})
