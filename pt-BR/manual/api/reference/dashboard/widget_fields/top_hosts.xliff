<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="pt-BR" datatype="plaintext" original="manual/api/reference/dashboard/widget_fields/top_hosts.md">
    <body>
      <trans-unit id="978cbb20" xml:space="preserve">
        <source># 21 Top hosts</source>
      </trans-unit>
      <trans-unit id="c5138763" xml:space="preserve">
        <source>### Description

These parameters and the possible property values for the respective dashboard widget field objects allow to configure
the [*Top Hosts*](/manual/web_interface/frontend_sections/dashboards/widgets/top_hosts) widget in `dashboard.create` and `dashboard.update` methods.</source>
      </trans-unit>
      <trans-unit id="a3febace" xml:space="preserve">
        <source>### Parameters

The following parameters are supported for the *Top Hosts* widget.

|Parameter|&lt;|[type](/manual/api/reference/dashboard/object#dashboard-widget-field)|name|value|
|-|--------|--|--------|-------------------------------|
|*Refresh interval*|&lt;|0|rf_rate|0 - No refresh;&lt;br&gt;10 - 10 seconds;&lt;br&gt;30 - 30 seconds;&lt;br&gt;60 - *(default)* 1 minute;&lt;br&gt;120 - 2 minutes;&lt;br&gt;600 - 10 minutes;&lt;br&gt;900 - 15 minutes.|
|*Host groups*|&lt;|2|groupids|[Host group](/manual/api/reference/hostgroup/get) ID.&lt;br&gt;&lt;br&gt;Note: To configure multiple host groups, create a dashboard widget field object for each host group.&lt;br&gt;&lt;br&gt;Parameter *Host groups* not available when configuring the widget on a [template dashboard](/manual/api/reference/templatedashboard/object).|
|*Hosts*|&lt;|3|hostids|[Host](/manual/api/reference/host/get) ID.&lt;br&gt;&lt;br&gt;Note: To configure multiple hosts, create a dashboard widget field object for each host. For multiple hosts, the parameter *Host groups* must either be not configured at all or configured with at least one host group that the configured hosts belong to.&lt;br&gt;&lt;br&gt;Parameter *Hosts* not available when configuring the widget on a [template dashboard](/manual/api/reference/templatedashboard/object).|
|*Host tags* (the number in the property name (e.g. tags.tag.0) references tag order in the tag evaluation list;&lt;br&gt;the parameter *Host tags* is not available when configuring the widget on a [template dashboard](/manual/api/reference/templatedashboard/object))|&lt;|&lt;|&lt;|&lt;|
|&lt;|*Evaluation type*|0|evaltype|0 - *(default)* And/Or;&lt;br&gt;2 - Or.|
|^|*Tag name*|1|tags.tag.0|Any string value.&lt;br&gt;&lt;br&gt;[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):&lt;br&gt;- *required* if configuring *Host tags*|
|^|*Operator*|0|tags.operator.0|0 - Contains;&lt;br&gt;1 - Equals;&lt;br&gt;2 - Does not contain;&lt;br&gt;3 - Does not equal;&lt;br&gt;4 - Exists;&lt;br&gt;5 - Does not exist.&lt;br&gt;&lt;br&gt;[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):&lt;br&gt;- *required* if configuring *Host tags*|
|^|*Tag value*|1|tags.value.0|Any string value.&lt;br&gt;&lt;br&gt;[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):&lt;br&gt;- *required* if configuring *Host tags*|
|*Columns* (see below)|&lt;|&lt;|&lt;|&lt;|
|*Order*|&lt;|0|order|2 - *(default)* Top N;&lt;br&gt;3 - Bottom N.|
|*Order column*|&lt;|0|column|Column numeric value from the configured columns.|
|*Host count*|&lt;|0|count|Valid values range from 1-100.&lt;br&gt;&lt;br&gt;Default: 10.&lt;br&gt;&lt;br&gt;Parameter *Host count* not available when configuring the widget on a [template dashboard](/manual/api/reference/templatedashboard/object).|</source>
      </trans-unit>
      <trans-unit id="8266fc04" xml:space="preserve">
        <source>#### Columns

Columns have common parameters and additional parameters depending on the configuration of the parameter *Data*.

::: noteclassic
For all parameters related to columns the number in the property name (e.g. columns.name.0) references a column for which the parameter is configured.
:::</source>
      </trans-unit>
      <trans-unit id="bba2be85" xml:space="preserve">
        <source>The following parameters are supported for all columns.

|Parameter|[type](/manual/api/reference/dashboard/object#dashboard-widget-field)|name|value|
|-----|-|-----|-------------------|
|*Name*|1|columns.name.0|Any string value.|
|*Data*|0|columns.data.0|1 - Item value;&lt;br&gt;2 - Host name;&lt;br&gt;3 - Text.&lt;br&gt;&lt;br&gt;[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):&lt;br&gt;- *required*|
|*Base color*|1|columns.base_color.0|Hexadecimal color code (e.g. `FF0000`).&lt;br&gt;&lt;br&gt;[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):&lt;br&gt;- *required*|</source>
      </trans-unit>
      <trans-unit id="e8b395eb" xml:space="preserve">
        <source>##### Item value

The following parameters are supported if *Data* is set to "Item value".

::: noteclassic
The first number in the *Thresholds* property name (e.g. columnsthresholds.color.0.0) references the column for which thresholds are configured,
while the second number references threshold place in a list, sorted in ascending order.
However, if thresholds are configured in a different order, the values will be sorted in ascending order after updating widget configuration in Zabbix frontend
(e.g. `"threshold.threshold.0":"5"` → `"threshold.threshold.0":"1"`; `"threshold.threshold.1":"1"` → `"threshold.threshold.1": "5"`).
:::

|Parameter|&lt;|[type](/manual/api/reference/dashboard/object#dashboard-widget-field)|name|value|
|-|--------|--|--------|-------------------------------|
|*Item*|&lt;|1 |columns.item.0|Valid item name.&lt;br&gt;When configuring the widget on a [template dashboard](/manual/api/reference/templatedashboard/object), only items configured on the template should be set.|
|*Time shift*|&lt;|1|columns.timeshift.0|Valid numeric or time string value (e.g. `3600` or `1h`).&lt;br&gt;You may use [time suffixes](/manual/appendix/suffixes#time-suffixes). Negative values are allowed.&lt;br&gt;&lt;br&gt;[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):&lt;br&gt;- *required*|
|*Aggregation function*|&lt;|0|columns.aggregate_function.0|0 - *(default)* none;&lt;br&gt;1 - min;&lt;br&gt;2 - max;&lt;br&gt;3 - avg;&lt;br&gt;4 - count;&lt;br&gt;5 - sum;&lt;br&gt;6 - first;&lt;br&gt;7 - last.|
|*Aggregation interval*|&lt;|1|columns.aggregate_interval.0|Valid time string (e.g. `3600`, `1h`, etc.).&lt;br&gt;You may use [time suffixes](/manual/appendix/suffixes#time-suffixes).&lt;br&gt;&lt;br&gt;Parameter *Aggregation interval* not available if *Aggregation function* is set to *none*.&lt;br&gt;&lt;br&gt;Default: `1h`.|
|*Display*|&lt;|0|columns.display.0|1 - *(default)* As is;&lt;br&gt;2 - Bar;&lt;br&gt;3 - Indicators.|
|*Min*|&lt;|1|columns.min.0|Any numeric value.&lt;br&gt;&lt;br&gt;Parameter *Min* not available if *Display* is set to "As is".|
|*Max*|&lt;|1|columns.max.0|Any numeric value.&lt;br&gt;&lt;br&gt;Parameter *Max* not available if *Display* is set to "As is".|
|*Decimal places*|&lt;|0|columns.decimal_places.0|Valid values range from 0-10.&lt;br&gt;&lt;br&gt;Default: 2.|
|*History data*|&lt;|0|columns.history.0|1 - *(default)* Auto;&lt;br&gt;2 - History;&lt;br&gt;3 - Trends.|
|*Thresholds*|&lt;|&lt;|&lt;|&lt;|
|&lt;|*Color*|1|columnsthresholds.color.0.0|Hexadecimal color code (e.g. `FF0000`).&lt;br&gt;&lt;br&gt;Default: `""` (empty).|
|^|*Threshold*|1|columnsthresholds.threshold.0.0|Any string value.|</source>
      </trans-unit>
      <trans-unit id="18fd738d" xml:space="preserve">
        <source>##### Text

The following parameters are supported if *Data* is set to "Text".

|Parameter|[type](/manual/api/reference/dashboard/object#dashboard-widget-field)|name|value|
|-----|-|-----|-------------------|
|*Text*|1|columns.text.0|Any string value, including macros.&lt;br&gt;Supported macros: {HOST.\*}, {INVENTORY.\*}.&lt;br&gt;&lt;br&gt;[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):&lt;br&gt;- *required* if *Data* is set to "Text"|</source>
      </trans-unit>
      <trans-unit id="c274247c" xml:space="preserve">
        <source>### Examples

The following examples aim to only describe the configuration of the dashboard widget field objects for the *Top hosts* widget.
For more information on configuring a dashboard, see [`dashboard.create`](/manual/api/reference/dashboard/create).</source>
      </trans-unit>
      <trans-unit id="2e9b79c3" xml:space="preserve">
        <source>#### Configuring a *Top hosts* widget

Configure a *Top hosts* widget that displays top hosts by CPU utilization in host group "4".
In addition, configure the following custom columns: "Host name", "CPU utilization in %", "1m avg", "5m avg", "15m avg", "Processes".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "dashboard.create",
    "params": {
        "name": "My dashboard",
        "display_period": 30,
        "auto_start": 1,
        "pages": [
            {
                "widgets": [
                    {
                        "type": "tophosts",
                        "name": "Top hosts",
                        "x": 0,
                        "y": 0,
                        "width": 12,
                        "height": 5,
                        "view_mode": 0,
                        "fields": [
                            {
                                "type": 2,
                                "name": "groupids",
                                "value": 4
                            },
                            {
                                "type": 1,
                                "name": "columns.name.0",
                                "value": ""
                            },
                            {
                                "type": 0,
                                "name": "columns.data.0",
                                "value": 2
                            },
                            {
                                "type": 1,
                                "name": "columns.base_color.0",
                                "value": "FFFFFF"
                            },
                            {
                                "type": 1,
                                "name": "columns.item.0",
                                "value": "System name"
                            },
                            {
                                "type": 1,
                                "name": "columns.timeshift.0",
                                "value": ""
                            },
                            {
                                "type": 1,
                                "name": "columns.name.1",
                                "value": "CPU utilization in %"
                            },
                            {
                                "type": 0,
                                "name": "columns.data.1",
                                "value": 1
                            },
                            {
                                "type": 1,
                                "name": "columns.base_color.1",
                                "value": "4CAF50"
                            },
                            {
                                "type": 1,
                                "name": "columns.item.1",
                                "value": "CPU utilization"
                            },
                            {
                                "type": 1,
                                "name": "columns.timeshift.1",
                                "value": ""
                            },
                            {
                                "type": 0,
                                "name": "columns.display.1",
                                "value": 3
                            },
                            {
                                "type": 1,
                                "name": "columns.min.1",
                                "value": "0"
                            },
                            {
                                "type": 1,
                                "name": "columns.max.1",
                                "value": "100"
                            },
                            {
                                "type": 1,
                                "name": "columnsthresholds.color.1.0",
                                "value": "FFFF00"
                            },
                            {
                                "type": 1,
                                "name": "columnsthresholds.threshold.1.0",
                                "value": "50"
                            },
                            {
                                "type": 1,
                                "name": "columnsthresholds.color.1.1",
                                "value": "FF8000"
                            },
                            {
                                "type": 1,
                                "name": "columnsthresholds.threshold.1.1",
                                "value": "80"
                            },
                            {
                                "type": 1,
                                "name": "columnsthresholds.color.1.2",
                                "value": "FF4000"
                            },
                            {
                                "type": 1,
                                "name": "columnsthresholds.threshold.1.2",
                                "value": "90"
                            },
                            {
                                "type": 1,
                                "name": "columns.name.2",
                                "value": "1m avg"
                            },
                            {
                                "type": 0,
                                "name": "columns.data.2",
                                "value": 1
                            },
                            {
                                "type": 1,
                                "name": "columns.base_color.2",
                                "value": "FFFFFF"
                            },
                            {
                                "type": 1,
                                "name": "columns.item.2",
                                "value": "Load average (1m avg)"
                            },
                            {
                                "type": 1,
                                "name": "columns.timeshift.2",
                                "value": ""
                            },
                            {
                                "type": 1,
                                "name": "columns.name.3",
                                "value": "5m avg"
                            },
                            {
                                "type": 0,
                                "name": "columns.data.3",
                                "value": 1
                            },
                            {
                                "type": 1,
                                "name": "columns.base_color.3",
                                "value": "FFFFFF"
                            },
                            {
                                "type": 1,
                                "name": "columns.item.3",
                                "value": "Load average (5m avg)"
                            },
                            {
                                "type": 1,
                                "name": "columns.timeshift.3",
                                "value": ""
                            },
                            {
                                "type": 1,
                                "name": "columns.name.4",
                                "value": "15m avg"
                            },
                            {
                                "type": 0,
                                "name": "columns.data.4",
                                "value": 1
                            },
                            {
                                "type": 1,
                                "name": "columns.base_color.4",
                                "value": "FFFFFF"
                            },
                            {
                                "type": 1,
                                "name": "columns.item.4",
                                "value": "Load average (15m avg)"
                            },
                            {
                                "type": 1,
                                "name": "columns.timeshift.4",
                                "value": ""
                            },
                            {
                                "type": 1,
                                "name": "columns.name.5",
                                "value": "Processes"
                            },
                            {
                                "type": 0,
                                "name": "columns.data.5",
                                "value": 1
                            },
                            {
                                "type": 1,
                                "name": "columns.base_color.5",
                                "value": "FFFFFF"
                            },
                            {
                                "type": 1,
                                "name": "columns.item.5",
                                "value": "Number of processes"
                            },
                            {
                                "type": 1,
                                "name": "columns.timeshift.5",
                                "value": ""
                            },
                            {
                                "type": 0,
                                "name": "columns.decimal_places.5",
                                "value": 0
                            },
                            {
                                "type": 0,
                                "name": "column",
                                "value": 1
                            }
                        ]
                    }
                ]
            }
        ],
        "userGroups": [
            {
                "usrgrpid": 7,
                "permission": 2
            }
        ],
        "users": [
            {
                "userid": 1,
                "permission": 3
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "dashboardids": [
            "3"
        ]
    },
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="d8c32fe2" xml:space="preserve">
        <source>### See also

-   [Dashboard widget field](/manual/api/reference/dashboard/object#dashboard-widget-field)
-   [`dashboard.create`](/manual/api/reference/dashboard/create)
-   [`dashboard.update`](/manual/api/reference/dashboard/update)</source>
      </trans-unit>
    </body>
  </file>
</xliff>
