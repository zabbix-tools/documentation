<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="pt-BR" datatype="plaintext" original="manual/api/reference/dashboard/widget_fields/graph_classic.md">
    <body>
      <trans-unit id="a55a0f20" xml:space="preserve">
        <source># 9 Graph (classic)</source>
      </trans-unit>
      <trans-unit id="028866a6" xml:space="preserve">
        <source>### Description

These parameters and the possible property values for the respective dashboard widget field objects allow to configure
the [*Graph (classic)*](/manual/web_interface/frontend_sections/dashboards/widgets/graph_classic) widget in `dashboard.create` and `dashboard.update` methods.</source>
      </trans-unit>
      <trans-unit id="f319d6dc" xml:space="preserve">
        <source>### Parameters

The following parameters are supported for the *Graph (classic)* widget.

|Parameter|[type](/manual/api/reference/dashboard/object#dashboard-widget-field)|name|value|
|-----|-|-----|-------------------|
|*Refresh interval*|0|rf_rate|0 - No refresh;&lt;br&gt;10 - 10 seconds;&lt;br&gt;30 - 30 seconds;&lt;br&gt;60 - *(default)* 1 minute;&lt;br&gt;120 - 2 minutes;&lt;br&gt;600 - 10 minutes;&lt;br&gt;900 - 15 minutes.|
|*Source*|0|source_type|0 - *(default)* Graph;&lt;br&gt;1 - Simple graph.|
|*Graph*|6|graphid|[Graph](/manual/api/reference/graph/get) ID.&lt;br&gt;&lt;br&gt;[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):&lt;br&gt;- *required* if *Source* is set to "Graph"|
|*Item*|4|itemid|[Item](/manual/api/reference/item/get) ID.&lt;br&gt;&lt;br&gt;[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):&lt;br&gt;- *required* if *Source* is set to "Simple graph"|
|*Show legend*|0|show_legend|0 - Disabled;&lt;br&gt;1 - *(default)* Enabled.|
|*Enable host selection*|0|dynamic|0 - *(default)* Disabled;&lt;br&gt;1 - Enabled.|</source>
      </trans-unit>
      <trans-unit id="313ae2b7" xml:space="preserve">
        <source>### Examples

The following examples aim to only describe the configuration of the dashboard widget field objects for the *Graph (classic)* widget.
For more information on configuring a dashboard, see [`dashboard.create`](/manual/api/reference/dashboard/create).</source>
      </trans-unit>
      <trans-unit id="a6ab08a1" xml:space="preserve">
        <source>#### Configuring a *Graph (classic)* widget

Configure a *Graph (classic)* widget that displays a simple graph for the item "42269".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "dashboard.create",
    "params": {
        "name": "My dashboard",
        "display_period": 30,
        "auto_start": 1,
        "pages": [
            {
                "widgets": [
                    {
                        "type": "graph",
                        "name": "Graph (classic)",
                        "x": 0,
                        "y": 0,
                        "width": 12,
                        "height": 5,
                        "view_mode": 0,
                        "fields": [
                            {
                                "type": 0,
                                "name": "source_type",
                                "value": 1
                            },
                            {
                                "type": 4,
                                "name": "itemid",
                                "value": 42269
                            }
                        ]
                    }
                ]
            }
        ],
        "userGroups": [
            {
                "usrgrpid": 7,
                "permission": 2
            }
        ],
        "users": [
            {
                "userid": 1,
                "permission": 3
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "dashboardids": [
            "3"
        ]
    },
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="c57c98c8" xml:space="preserve">
        <source>### See also

-   [Dashboard widget field](/manual/api/reference/dashboard/object#dashboard-widget-field)
-   [`dashboard.create`](/manual/api/reference/dashboard/create)
-   [`dashboard.update`](/manual/api/reference/dashboard/update)</source>
      </trans-unit>
    </body>
  </file>
</xliff>
