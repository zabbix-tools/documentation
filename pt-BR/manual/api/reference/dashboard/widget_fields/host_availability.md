[comment]: # translation:outdated

[comment]: # ({new-63c2979a})
# 11 Host availability

[comment]: # ({/new-63c2979a})

[comment]: # ({new-0a764751})
### Description

These parameters and the possible property values for the respective dashboard widget field objects allow to configure
the [*Host availability*](/manual/web_interface/frontend_sections/dashboards/widgets/host_availability) widget in `dashboard.create` and `dashboard.update` methods.

[comment]: # ({/new-0a764751})

[comment]: # ({new-4337d56d})
### Parameters

The following parameters are supported for the *Host availability* widget.

|Parameter|[type](/manual/api/reference/dashboard/object#dashboard-widget-field)|name|value|
|-----|-|-----|-------------------|
|*Refresh interval*|0|rf_rate|0 - No refresh;<br>10 - 10 seconds;<br>30 - 30 seconds;<br>60 - 1 minute;<br>120 - 2 minutes;<br>600 - 10 minutes;<br>900 - *(default)* 15 minutes.|
|*Host groups*|2|groupids|[Host group](/manual/api/reference/hostgroup/get) ID.<br><br>Note: To configure multiple host groups, create a dashboard widget field object for each host group.|
|*Interface type*|0|interface_type|0 - None;<br>1 - Zabbix agent;<br>2 - SNMP;<br>3 - IPMI;<br>4 - JMX.<br><br>Default: 1, 2, 3, 4 (all enabled).<br><br>Note: To configure multiple values, create a dashboard widget field object for each value.|
|*Layout*|0|layout|0 - *(default)* Horizontal;<br>1 - Vertical.|
|*Show hosts in maintenance*|0|maintenance|0 - *(default)* Disabled;<br>1 - Enabled.|

[comment]: # ({/new-4337d56d})

[comment]: # ({new-5da77460})
### Examples

The following examples aim to only describe the configuration of the dashboard widget field objects for the *Host availability* widget.
For more information on configuring a dashboard, see [`dashboard.create`](/manual/api/reference/dashboard/create).

[comment]: # ({/new-5da77460})

[comment]: # ({new-055ba5b7})
#### Configuring a *Host availability* widget

Configure a *Host availability* widget that displays availability information (in a vertical layout) for hosts in host group "4" with "Zabbix agent" and "SNMP" interfaces configured.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "dashboard.create",
    "params": {
        "name": "My dashboard",
        "display_period": 30,
        "auto_start": 1,
        "pages": [
            {
                "widgets": [
                    {
                        "type": "hostavail",
                        "name": "Host availability",
                        "x": 0,
                        "y": 0,
                        "width": 6,
                        "height": 3,
                        "view_mode": 0,
                        "fields": [
                            {
                                "type": 2,
                                "name": "groupids",
                                "value": 4
                            },
                            {
                                "type": 0,
                                "name": "interface_type",
                                "value": 1
                            },
                            {
                                "type": 0,
                                "name": "interface_type",
                                "value": 2
                            },
                            {
                                "type": 0,
                                "name": "layout",
                                "value": 1
                            }
                        ]
                    }
                ]
            }
        ],
        "userGroups": [
            {
                "usrgrpid": 7,
                "permission": 2
            }
        ],
        "users": [
            {
                "userid": 1,
                "permission": 3
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "dashboardids": [
            "3"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-055ba5b7})

[comment]: # ({new-3cce45df})
### See also

-   [Dashboard widget field](/manual/api/reference/dashboard/object#dashboard-widget-field)
-   [`dashboard.create`](/manual/api/reference/dashboard/create)
-   [`dashboard.update`](/manual/api/reference/dashboard/update)

[comment]: # ({/new-3cce45df})
