[comment]: # translation:outdated

[comment]: # ({new-d8b4b063})
# 20 System information

[comment]: # ({/new-d8b4b063})

[comment]: # ({new-acdd6f8c})
### Description

These parameters and the possible property values for the respective dashboard widget field objects allow to configure
the [*System Information*](/manual/web_interface/frontend_sections/dashboards/widgets/system) widget in `dashboard.create` and `dashboard.update` methods.

[comment]: # ({/new-acdd6f8c})

[comment]: # ({new-59ca3d9b})
### Parameters

The following parameters are supported for the *System Information* widget.

|Parameter|[type](/manual/api/reference/dashboard/object#dashboard-widget-field)|name|value|
|-----|-|-----|-------------------|
|*Refresh interval*|0|rf_rate|0 - No refresh;<br>10 - 10 seconds;<br>30 - 30 seconds;<br>60 - 1 minute;<br>120 - 2 minutes;<br>600 - 10 minutes;<br>900 - *(default)* 15 minutes.|
|*Show*|0|info_type|0 - *(default)* System stats;<br>1 - High availability nodes.|

[comment]: # ({/new-59ca3d9b})

[comment]: # ({new-c23d8683})
### Examples

The following examples aim to only describe the configuration of the dashboard widget field objects for the *System information* widget.
For more information on configuring a dashboard, see [`dashboard.create`](/manual/api/reference/dashboard/create).

[comment]: # ({/new-c23d8683})

[comment]: # ({new-91f3e344})
#### Configuring a *System information* widget

Configure a *System information* widget that displays system stats with a refresh interval of 10 minutes.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "dashboard.create",
    "params": {
        "name": "My dashboard",
        "display_period": 30,
        "auto_start": 1,
        "pages": [
            {
                "widgets": [
                    {
                        "type": "systeminfo",
                        "name": "System information",
                        "x": 0,
                        "y": 0,
                        "width": 12,
                        "height": 5,
                        "view_mode": 0,
                        "fields": [
                            {
                                "type": 0,
                                "name": "rf_rate",
                                "value": 600
                            }
                        ]
                    }
                ]
            }
        ],
        "userGroups": [
            {
                "usrgrpid": 7,
                "permission": 2
            }
        ],
        "users": [
            {
                "userid": 1,
                "permission": 3
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "dashboardids": [
            "3"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-91f3e344})

[comment]: # ({new-ee77e7cb})
### See also

-   [Dashboard widget field](/manual/api/reference/dashboard/object#dashboard-widget-field)
-   [`dashboard.create`](/manual/api/reference/dashboard/create)
-   [`dashboard.update`](/manual/api/reference/dashboard/update)

[comment]: # ({/new-ee77e7cb})
