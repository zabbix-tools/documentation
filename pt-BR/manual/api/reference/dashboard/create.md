[comment]: # translation:outdated

[comment]: # ({new-c179cb27})
# dashboard.create

[comment]: # ({/new-c179cb27})

[comment]: # ({new-00970602})
### Description

`object dashboard.create(object/array dashboards)`

This method allows to create new dashboards.

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-00970602})

[comment]: # ({new-df2301b4})
### Parameters

`(object/array)` Dashboards to create.

Additionally to the [standard dashboard properties](object#dashboard),
the method accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|**pages**<br>(required)|array|Dashboard [pages](object#dashboard_page) to be created for the dashboard. Dashboard pages will be ordered in the same order as specified. At least one dashboard page object is required for `pages` property.|
|users|array|Dashboard [user](object#dashboard_user) shares to be created on the dashboard.|
|userGroups|array|Dashboard [user group](object#dashboard_user_group) shares to be created on the dashboard.|

[comment]: # ({/new-df2301b4})

[comment]: # ({new-f460a18e})
### Return values

`(object)` Returns an object containing the IDs of the created
dashboards under the `dashboardids` property. The order of the returned
IDs matches the order of the passed dashboards.

[comment]: # ({/new-f460a18e})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-b33dc2ee})
#### Creating a dashboard

Create a dashboard named "My dashboard" with one Problems widget with
tags and using two types of sharing (user group and user) on a single
dashboard page.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "dashboard.create",
    "params": {
        "name": "My dashboard",
        "display_period": 30,
        "auto_start": 1,
        "pages": [
            {
                "widgets": [
                    {
                        "type": "problems",
                        "x": 0,
                        "y": 0,
                        "width": 12,
                        "height": 5,
                        "view_mode": 0,
                        "fields": [
                            {
                                "type": 1,
                                "name": "tags.tag.0",
                                "value": "service"
                            },
                            {
                                "type": 0,
                                "name": "tags.operator.0",
                                "value": 1
                            },
                            {
                                "type": 1,
                                "name": "tags.value.0",
                                "value": "zabbix_server"
                            }
                        ]
                    }
                ]
            }
        ],
        "userGroups": [
            {
                "usrgrpid": "7",
                "permission": 2
            }
        ],
        "users": [
            {
                "userid": "4",
                "permission": 3
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "dashboardids": [
            "2"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-b33dc2ee})

[comment]: # ({new-299ae089})
### See also

-   [Dashboard page](object#dashboard_page)
-   [Dashboard widget](object#dashboard_widget)
-   [Dashboard widget field](object#dashboard_widget_field)
-   [Dashboard user](object#dashboard_user)
-   [Dashboard user group](object#dashboard_user_group)

[comment]: # ({/new-299ae089})

[comment]: # ({new-7d488f32})
### Source

CDashboard::create() in
*ui/include/classes/api/services/CDashboard.php*.

[comment]: # ({/new-7d488f32})
