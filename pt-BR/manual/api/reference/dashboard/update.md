[comment]: # translation:outdated

[comment]: # ({new-66fb9690})
# dashboard.update

[comment]: # ({/new-66fb9690})

[comment]: # ({new-f235159f})
### Description

`object dashboard.update(object/array dashboards)`

This method allows to update existing dashboards.

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-f235159f})

[comment]: # ({new-62947aa0})
### Parameters

`(object/array)` Dashboard properties to be updated.

The `dashboardid` property must be specified for each dashboard, all
other properties are optional. Only the specified properties will be
updated.

Additionally to the [standard dashboard properties](object#dashboard),
the method accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|pages|array|Dashboard [pages](object#dashboard_page) to replace the existing dashboard pages.<br><br>Dashboard pages are updated by the `dashboard_pageid` property. New dashboard pages will be created for objects without `dashboard_pageid` property and the existing dashboard pages will be deleted if not reused. Dashboard pages will be ordered in the same order as specified. Only the specified properties of the dashboard pages will be updated. At least one dashboard page object is required for `pages` property.|
|users|array|Dashboard [user](object#dashboard_user) shares to replace the existing elements.|
|userGroups|array|Dashboard [user group](object#dashboard_user_group) shares to replace the existing elements.|

[comment]: # ({/new-62947aa0})

[comment]: # ({new-28e89b38})
### Return values

`(object)` Returns an object containing the IDs of the updated
dashboards under the `dashboardids` property.

[comment]: # ({/new-28e89b38})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-7fcf7de4})
#### Renaming a dashboard

Rename a dashboard to "SQL server status".

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "dashboard.update",
    "params": {
        "dashboardid": "2",
        "name": "SQL server status"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "dashboardids": [
            "2"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-7fcf7de4})

[comment]: # ({new-b8348b8a})
#### Updating dashboard pages

Rename the first dashboard page, replace widgets on the second dashboard
page and add a new page as the third one. Delete all other dashboard
pages.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "dashboard.update",
    "params": {
        "dashboardid": "2",
        "pages": [
            {
                "dashboard_pageid": 1,
                "name": 'Renamed Page'
            },
            {
                "dashboard_pageid": 2,
                "widgets": [
                    {
                        "type": "clock",
                        "x": 0,
                        "y": 0,
                        "width": 4,
                        "height": 3
                    }
                ]
            },
            {
                "display_period": 60
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "dashboardids": [
            "2"
        ]
    },
    "id": 2
}
```

[comment]: # ({/new-b8348b8a})

[comment]: # ({new-e4a57dd3})
#### Change dashboard owner

Available only for admins and super admins.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "dashboard.update",
    "params": {
        "dashboardid": "2",
        "userid": "1"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 2
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "dashboardids": [
            "2"
        ]
    },
    "id": 2
}
```

[comment]: # ({/new-e4a57dd3})

[comment]: # ({new-299ae089})
### See also

-   [Dashboard page](object#dashboard_page)
-   [Dashboard widget](object#dashboard_widget)
-   [Dashboard widget field](object#dashboard_widget_field)
-   [Dashboard user](object#dashboard_user)
-   [Dashboard user group](object#dashboard_user_group)

[comment]: # ({/new-299ae089})

[comment]: # ({new-456ac32b})
### Source

CDashboard::update() in
*ui/include/classes/api/services/CDashboard.php*.

[comment]: # ({/new-456ac32b})
