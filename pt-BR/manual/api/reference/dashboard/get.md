[comment]: # translation:outdated

[comment]: # ({new-f7884974})
# dashboard.get

[comment]: # ({/new-f7884974})

[comment]: # ({new-8d36c9b5})
### Description

`integer/array dashboard.get(object parameters)`

The method allows to retrieve dashboards according to the given
parameters.

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-8d36c9b5})

[comment]: # ({new-342bbd26})
### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|dashboardids|string/array|Return only dashboards with the given IDs.|
|selectPages|query|Return a [pages](/manual/api/reference/dashboard/object#dashboard_page) property with dashboard pages, correctly ordered.|
|selectUsers|query|Return a [users](/manual/api/reference/dashboard/object#dashboard_user) property with users that the dashboard is shared with.|
|selectUserGroups|query|Return a [userGroups](/manual/api/reference/dashboard/object#dashboard_user_group) property with user groups that the dashboard is shared with.|
|sortfield|string/array|Sort the result by the given properties.<br><br>Possible value is: `dashboardid`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters) page.|
|editable|boolean|^|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

[comment]: # ({/new-342bbd26})

[comment]: # ({new-7223bab1})
### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-84a10cd6})
#### Retrieving a dashboard by ID

Retrieve all data about dashboards "1" and "2".

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "dashboard.get",
    "params": {
        "output": "extend",
        "selectPages": "extend",
        "selectUsers": "extend",
        "selectUserGroups": "extend",
        "dashboardids": [
            "1",
            "2"
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "dashboardid": "1",
            "name": "Dashboard",
            "userid": "1",
            "private": "0",
            "display_period": "30",
            "auto_start": "1",
            "users": [],
            "userGroups": [],
            "pages": [
                {
                    "dashboard_pageid": "1",
                    "name": "",
                    "display_period": "0",
                    "widgets": [
                        {
                            "widgetid": "9",
                            "type": "systeminfo",
                            "name": "",
                            "x": "12",
                            "y": "8",
                            "width": "12",
                            "height": "5",
                            "view_mode": "0",
                            "fields": []
                        },
                        {
                            "widgetid": "8",
                            "type": "problemsbysv",
                            "name": "",
                            "x": "12",
                            "y": "4",
                            "width": "12",
                            "height": "4",
                            "view_mode": "0",
                            "fields": []
                        },
                        {
                            "widgetid": "7",
                            "type": "problemhosts",
                            "name": "",
                            "x": "12",
                            "y": "0",
                            "width": "12",
                            "height": "4",
                            "view_mode": "0",
                            "fields": []
                        },
                        {
                            "widgetid": "6",
                            "type": "discovery",
                            "name": "",
                            "x": "6",
                            "y": "9",
                            "width": "6",
                            "height": "4",
                            "view_mode": "0",
                            "fields": []
                        },
                        {
                            "widgetid": "5",
                            "type": "web",
                            "name": "",
                            "x": "0",
                            "y": "9",
                            "width": "6",
                            "height": "4",
                            "view_mode": "0",
                            "fields": []
                        },
                        {
                            "widgetid": "4",
                            "type": "problems",
                            "name": "",
                            "x": "0",
                            "y": "3",
                            "width": "12",
                            "height": "6",
                            "view_mode": "0",
                            "fields": []
                        },
                        {
                            "widgetid": "3",
                            "type": "favmaps",
                            "name": "",
                            "x": "8",
                            "y": "0",
                            "width": "4",
                            "height": "3",
                            "view_mode": "0",
                            "fields": []
                        },
                        {
                            "widgetid": "1",
                            "type": "favgraphs",
                            "name": "",
                            "x": "0",
                            "y": "0",
                            "width": "4",
                            "height": "3",
                            "view_mode": "0",
                            "fields": []
                        }
                    ]
                },
                {
                    "dashboard_pageid": "2",
                    "name": "",
                    "display_period": "0",
                    "widgets": []
                },
                {
                    "dashboard_pageid": "3",
                    "name": "Custom page name",
                    "display_period": "60",
                    "widgets": []
                }
            ]
        },
        {
            "dashboardid": "2",
            "name": "My dashboard",
            "userid": "1",
            "private": "1",
            "display_period": "60",
            "auto_start": "1",
            "users": [
                {
                    "userid": "4",
                    "permission": "3"
                }
            ],
            "userGroups": [
                {
                    "usrgrpid": "7",
                    "permission": "2"
                }
            ],
            "pages": [
                {
                    "dashboard_pageid": "4",
                    "name": "",
                    "display_period": "0",
                    "widgets": [
                        {
                            "widgetid": "10",
                            "type": "problems",
                            "name": "",
                            "x": "0",
                            "y": "0",
                            "width": "12",
                            "height": "5",
                            "view_mode": "0",
                            "fields": [
                                {
                                    "type": "2",
                                    "name": "groupids",
                                    "value": "4"
                                }
                            ]
                        }
                    ]
                }
            ]
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-84a10cd6})

[comment]: # ({new-299ae089})
### See also

-   [Dashboard page](object#dashboard_page)
-   [Dashboard widget](object#dashboard_widget)
-   [Dashboard widget field](object#dashboard_widget_field)
-   [Dashboard user](object#dashboard_user)
-   [Dashboard user group](object#dashboard_user_group)

[comment]: # ({/new-299ae089})

[comment]: # ({new-75a9042e})
### Source

CDashboard::get() in *ui/include/classes/api/services/CDashboard.php*.

[comment]: # ({/new-75a9042e})
