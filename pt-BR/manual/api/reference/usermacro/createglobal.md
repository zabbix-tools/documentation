[comment]: # translation:outdated

[comment]: # ({new-f1e165e2})
# usermacro.createglobal

[comment]: # ({/new-f1e165e2})

[comment]: # ({new-2655f9fe})
### Description

`object usermacro.createglobal(object/array globalMacros)`

This method allows to create new global macros.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-2655f9fe})

[comment]: # ({new-3bf123cd})
### Parameters

`(object/array)` Global macros to create.

The method accepts global macros with the [standard global macro
properties](object#global_macro).

[comment]: # ({/new-3bf123cd})

[comment]: # ({new-0b8f323a})
### Return values

`(object)` Returns an object containing the IDs of the created global
macros under the `globalmacroids` property. The order of the returned
IDs matches the order of the passed global macros.

[comment]: # ({/new-0b8f323a})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-752894f1})
#### Creating a global macro

Create a global macro "{$SNMP\_COMMUNITY}" with value "public".

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "usermacro.createglobal",
    "params":  {
        "macro": "{$SNMP_COMMUNITY}",
        "value": "public"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "globalmacroids": [
            "6"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-752894f1})

[comment]: # ({new-7b4f640f})
### Source

CUserMacro::createGlobal() in
*ui/include/classes/api/services/CUserMacro.php*.

[comment]: # ({/new-7b4f640f})
