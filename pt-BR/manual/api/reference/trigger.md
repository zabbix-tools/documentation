[comment]: # translation:outdated

[comment]: # ({new-9dae9ddc})
# Trigger

This class is designed to work with triggers.

Object references:\

-   [Trigger](/manual/api/reference/trigger/object#trigger)

Available methods:\

-   [trigger.adddependencies](/manual/api/reference/trigger/adddependencies) -
    adding new trigger dependencies
-   [trigger.create](/manual/api/reference/trigger/create) - creating
    new triggers
-   [trigger.delete](/manual/api/reference/trigger/delete) - deleting
    triggers
-   [trigger.deletedependencies](/manual/api/reference/trigger/deletedependencies) -
    deleting trigger dependencies
-   [trigger.get](/manual/api/reference/trigger/get) - retrieving
    triggers
-   [trigger.update](/manual/api/reference/trigger/update) - updating
    triggers

[comment]: # ({/new-9dae9ddc})
