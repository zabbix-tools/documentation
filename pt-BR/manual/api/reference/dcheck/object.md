[comment]: # translation:outdated

[comment]: # ({new-a9b6b9e5})
# > Discovery check object

The following objects are directly related to the `dcheck` API.

[comment]: # ({/new-a9b6b9e5})

[comment]: # ({new-662afbc9})
### Discovery check

The discovery check object defines a specific check performed by a
network discovery rule. It has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|dcheckid|string|*(readonly)* ID of the discovery check.|
|druleid|string|*(readonly)* ID of the discovery rule that the check belongs to.|
|key\_|string|The value of this property differs depending on the type of the check:<br>- key to query for Zabbix agent checks, required;<br>- SNMP OID for SNMPv1, SNMPv2 and SNMPv3 checks, required.|
|ports|string|One or several port ranges to check separated by commas. Used for all checks except for ICMP.<br><br>Default: 0.|
|snmp\_community|string|SNMP community.<br><br>Required for SNMPv1 and SNMPv2 agent checks.|
|snmpv3\_authpassphrase|string|Authentication passphrase used for SNMPv3 agent checks with security level set to *authNoPriv* or *authPriv*.|
|snmpv3\_authprotocol|integer|Authentication protocol used for SNMPv3 agent checks with security level set to *authNoPriv* or *authPriv*.<br><br>Possible values:<br>0 - *(default)* MD5;<br>1 - SHA1;<br>2 - SHA224;<br>3 - SHA256;<br>4 - SHA384;<br>5 - SHA512.|
|snmpv3\_contextname|string|SNMPv3 context name. Used only by SNMPv3 checks.|
|snmpv3\_privpassphrase|string|Privacy passphrase used for SNMPv3 agent checks with security level set to *authPriv*.|
|snmpv3\_privprotocol|integer|Privacy protocol used for SNMPv3 agent checks with security level set to *authPriv*.<br><br>Possible values:<br>0 - *(default)* DES;<br>1 - AES128;<br>2 - AES192;<br>3 - AES256;<br>4 - AES192C;<br>5 - AES256C.|
|snmpv3\_securitylevel|string|Security level used for SNMPv3 agent checks.<br><br>Possible values:<br>0 - noAuthNoPriv;<br>1 - authNoPriv;<br>2 - authPriv.|
|snmpv3\_securityname|string|Security name used for SNMPv3 agent checks.|
|**type**<br>(required)|integer|Type of check.<br><br>Possible values:<br>0 - SSH;<br>1 - LDAP;<br>2 - SMTP;<br>3 - FTP;<br>4 - HTTP;<br>5 - POP;<br>6 - NNTP;<br>7 - IMAP;<br>8 - TCP;<br>9 - Zabbix agent;<br>10 - SNMPv1 agent;<br>11 - SNMPv2 agent;<br>12 - ICMP ping;<br>13 - SNMPv3 agent;<br>14 - HTTPS;<br>15 - Telnet.|
|uniq|integer|Whether to use this check as a device uniqueness criteria. Only a single unique check can be configured for a discovery rule. Used for Zabbix agent, SNMPv1, SNMPv2 and SNMPv3 agent checks.<br><br>Possible values:<br>0 - *(default)* do not use this check as a uniqueness criteria;<br>1 - use this check as a uniqueness criteria.|
|host\_source|integer|Source for host name.<br><br>Possible values:<br>1 - *(default)* DNS;<br>2 - IP;<br>3 - discovery value of this check.|
|name\_source|integer|Source for visible name.<br><br>Possible values:<br>0 - *(default)* not specified;<br>1 - DNS;<br>2 - IP;<br>3 - discovery value of this check.|

[comment]: # ({/new-662afbc9})
