<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="pt-BR" datatype="plaintext" original="manual/api/reference/graphitem/get.md">
    <body>
      <trans-unit id="43e64f56" xml:space="preserve">
        <source># graphitem.get</source>
      </trans-unit>
      <trans-unit id="fe22ea70" xml:space="preserve">
        <source>### Description

`integer/array graphitem.get(object parameters)`

The method allows to retrieve graph items according to the given
parameters.

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::</source>
      </trans-unit>
      <trans-unit id="d30698ba" xml:space="preserve">
        <source>### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|graphids|string/array|Return only graph items that belong to the given graphs.|
|itemids|string/array|Return only graph items with the given item IDs.|
|type|integer|Return only graph items with the given type.&lt;br&gt;&lt;br&gt;Refer to the [graph item object page](object#graph_item) for a list of supported graph item types.|
|selectGraphs|query|Return a [`graphs`](/manual/api/reference/graph/object) property with an array of graphs that the item belongs to.|
|sortfield|string/array|Sort the result by the given properties.&lt;br&gt;&lt;br&gt;Possible values: `gitemid`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters) page.|
|editable|boolean|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|sortorder|string/array|^|</source>
      </trans-unit>
      <trans-unit id="7223bab1" xml:space="preserve">
        <source>### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.</source>
      </trans-unit>
      <trans-unit id="b41637d2" xml:space="preserve">
        <source>### Examples</source>
      </trans-unit>
      <trans-unit id="37da5f20" xml:space="preserve">
        <source>#### Retrieving graph items from a graph

Retrieve all graph items used in a graph with additional information
about the item and the host.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "graphitem.get",
    "params": {
        "output": "extend",
        "graphids": "387"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": [
        {
            "gitemid": "1242",
            "graphid": "387",
            "itemid": "22665",
            "drawtype": "1",
            "sortorder": "1",
            "color": "FF5555",
            "yaxisside": "0",
            "calc_fnc": "2",
            "type": "0",
            "key_": "system.cpu.util[,steal]",
            "hostid": "10001",
            "flags": "0",
            "host": "Linux"
        },
        {
            "gitemid": "1243",
            "graphid": "387",
            "itemid": "22668",
            "drawtype": "1",
            "sortorder": "2",
            "color": "55FF55",
            "yaxisside": "0",
            "calc_fnc": "2",
            "type": "0",
            "key_": "system.cpu.util[,softirq]",
            "hostid": "10001",
            "flags": "0",
            "host": "Linux"
        },
        {
            "gitemid": "1244",
            "graphid": "387",
            "itemid": "22671",
            "drawtype": "1",
            "sortorder": "3",
            "color": "009999",
            "yaxisside": "0",
            "calc_fnc": "2",
            "type": "0",
            "key_": "system.cpu.util[,interrupt]",
            "hostid": "10001",
            "flags": "0",
            "host": "Linux"
        }
    ],
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="09a26e71" xml:space="preserve">
        <source>### See also

-   [Graph](/manual/api/reference/graph/object#graph)</source>
      </trans-unit>
      <trans-unit id="8d13cada" xml:space="preserve">
        <source>### Source

CGraphItem::get() in *ui/include/classes/api/services/CGraphItem.php*.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
