[comment]: # translation:outdated

[comment]: # ({new-4480ea6d})
# token.generate

[comment]: # ({/new-4480ea6d})

[comment]: # ({new-3f13b874})
### Description

`object token.generate(array tokenids)`

This method allows to generate tokens.

::: noteclassic
Only *Super admin* user type is allowed to manage tokens for
other users.
:::

[comment]: # ({/new-3f13b874})

[comment]: # ({new-db2ee95a})
### Parameters

`(array)` IDs of the tokens to generate.

[comment]: # ({/new-db2ee95a})

[comment]: # ({new-101a0040})
### Return values

`(array)` Returns an array of objects containing the ID of the generated
token under the `tokenid` property and generated authorization string
under `token` property.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|tokenid|string|ID of the token.|
|token|string|The generated authorization string for this token.|

[comment]: # ({/new-101a0040})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-c0623539})
#### Generate multiple tokens

Generate two tokens.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "token.generate",
    "params": [
        "1",
        "2"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "tokenid": "1",
            "token": "bbcfce79a2d95037502f7e9a534906d3466c9a1484beb6ea0f4e7be28e8b8ce2"
        },
        {
            "tokenid": "2",
            "token": "fa1258a83d518eabd87698a96bd7f07e5a6ae8aeb8463cae33d50b91dd21bd6d"
        }
    ],
    "id": 0
}
```

[comment]: # ({/new-c0623539})

[comment]: # ({new-39ba5785})
### Source

CToken::generate() in *ui/include/classes/api/services/CToken.php*.

[comment]: # ({/new-39ba5785})
