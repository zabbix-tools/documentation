[comment]: # translation:outdated

[comment]: # ({new-3683e83d})
# > Token object

The following objects are directly related to the `token` API.

[comment]: # ({/new-3683e83d})

[comment]: # ({new-e5e50206})
### Token

The token object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|tokenid|string|*(readonly)* ID of the token.|
|**name**<br>(required)|string|Name of the token.|
|description|text|Description of the token.|
|userid|string|*(readonly for update)* A user the token has been assigned to.<br><br>*Default: current user.*|
|lastaccess|timestamp|*(readonly)* Most recent date and time the token was authenticated.<br><br>Zero if the token has never been authenticated.|
|status|integer|Token status.<br><br>Possible values:<br>0 - *(default)* enabled token;<br>1 - disabled token.|
|expires\_at|timestamp|Token expiration date and time.<br><br>Zero for never-expiring tokens.|
|created\_at|timestamp|*(readonly)* Token creation date and time.|
|creator\_userid|string|*(readonly)* The creator user of the token.|

[comment]: # ({/new-e5e50206})
