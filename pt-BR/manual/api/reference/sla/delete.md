[comment]: # translation:outdated

[comment]: # ({new-bf12fc32})
# sla.delete

[comment]: # ({/new-bf12fc32})

[comment]: # ({new-8ea5261f})
### Description

`object sla.delete(array slaids)`

This method allows to delete SLA entries.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-8ea5261f})

[comment]: # ({new-7af05f92})
### Parameters

`(array)` IDs of the SLAs to delete.

[comment]: # ({/new-7af05f92})

[comment]: # ({new-23960c29})
### Return values

`(object)` Returns an object containing the IDs of the deleted SLAs
under the `slaids` property.

[comment]: # ({/new-23960c29})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-5036ec70})
#### Deleting multiple SLAs

Delete two SLA entries.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "sla.delete",
    "params": [
        "4",
        "5"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "slaids": [
            "4",
            "5"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-5036ec70})

[comment]: # ({new-c99e5046})
### Source

CSla::delete() in *ui/include/classes/api/services/CSla.php*.

[comment]: # ({/new-c99e5046})
