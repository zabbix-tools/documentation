[comment]: # translation:outdated

[comment]: # ({new-65bbffce})
# host.get

[comment]: # ({/new-65bbffce})

[comment]: # ({new-93035b19})
### Description

`integer/array host.get(object parameters)`

The method allows to retrieve hosts according to the given parameters.

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-93035b19})

[comment]: # ({new-77a8fabb})
### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|groupids|string/array|Return only hosts that belong to the given groups.|
|dserviceids|string/array|Return only hosts that are related to the given discovered services.|
|graphids|string/array|Return only hosts that have the given graphs.|
|hostids|string/array|Return only hosts with the given host IDs.|
|httptestids|string/array|Return only hosts that have the given web checks.|
|interfaceids|string/array|Return only hosts that use the given interfaces.|
|itemids|string/array|Return only hosts that have the given items.|
|maintenanceids|string/array|Return only hosts that are affected by the given maintenances.|
|monitored\_hosts|flag|Return only monitored hosts.|
|proxy\_hosts|flag|Return only proxies.|
|proxyids|string/array|Return only hosts that are monitored by the given proxies.|
|templated\_hosts|flag|Return both hosts and templates.|
|templateids|string/array|Return only hosts that are linked to the given templates.|
|triggerids|string/array|Return only hosts that have the given triggers.|
|with\_items|flag|Return only hosts that have items.<br><br>Overrides the `with_monitored_items` and `with_simple_graph_items` parameters.|
|with\_item\_prototypes|flag|Return only hosts that have item prototypes.<br><br>Overrides the `with_simple_graph_item_prototypes` parameter.|
|with\_simple\_graph\_item\_prototypes|flag|Return only hosts that have item prototypes, which are enabled for creation and have numeric type of information.|
|with\_graphs|flag|Return only hosts that have graphs.|
|with\_graph\_prototypes|flag|Return only hosts that have graph prototypes.|
|with\_httptests|flag|Return only hosts that have web checks.<br><br>Overrides the `with_monitored_httptests` parameter.|
|with\_monitored\_httptests|flag|Return only hosts that have enabled web checks.|
|with\_monitored\_items|flag|Return only hosts that have enabled items.<br><br>Overrides the `with_simple_graph_items` parameter.|
|with\_monitored\_triggers|flag|Return only hosts that have enabled triggers. All of the items used in the trigger must also be enabled.|
|with\_simple\_graph\_items|flag|Return only hosts that have items with numeric type of information.|
|with\_triggers|flag|Return only hosts that have triggers.<br><br>Overrides the `with_monitored_triggers` parameter.|
|withProblemsSuppressed|boolean|Return hosts that have suppressed problems.<br><br>Possible values:<br>`null` - (default) all hosts;<br>`true` - only hosts with suppressed problems;<br>`false` - only hosts with unsuppressed problems.|
|evaltype|integer|Rules for tag searching.<br><br>Possible values:<br>0 - (default) And/Or;<br>2 - Or.|
|severities|integer/array|Return hosts that have only problems with given severities. Applies only if problem object is trigger.|
|tags|array/object|Return only hosts with given tags. Exact match by tag and case-sensitive or case-insensitive search by tag value depending on operator value.<br>Format: `[{"tag": "<tag>", "value": "<value>", "operator": "<operator>"}, ...]`.<br>An empty array returns all hosts.<br><br>Possible operator values:<br>0 - (default) Contains;<br>1 - Equals;<br>2 - Not like;<br>3 - Not equal<br>4 - Exists;<br>5 - Not exists.|
|inheritedTags|boolean|Return hosts that have given `tags` also in all of their linked templates. Default:<br><br>Possible values:<br>`true` - linked templates must also have given `tags`;<br>`false` - (default) linked template tags are ignored.|
|selectDiscoveries|query|Return a [discoveries](/manual/api/reference/discoveryrule/object) property with host low-level discovery rules.<br><br>Supports `count`.|
|selectDiscoveryRule|query|Return a [discoveryRule](/manual/api/reference/discoveryrule/object) property with the low-level discovery rule that created the host (from host prototype in VMware monitoring).|
|selectGraphs|query|Return a [graphs](/manual/api/reference/graph/object) property with host graphs.<br><br>Supports `count`.|
|selectGroups|query|Return a [groups](/manual/api/reference/hostgroup/object) property with host groups data that the host belongs to.|
|selectHostDiscovery|query|Return a `hostDiscovery` property with host discovery object data.<br><br>The host discovery object links a discovered host to a host prototype or a host prototypes to an LLD rule and has the following properties:<br>`host` - *(string)* host of the host prototype;<br>`hostid` - *(string)* ID of the discovered host or host prototype;<br>`parent_hostid` - *(string)* ID of the host prototype from which the host has been created;<br>`parent_itemid` - *(string)* ID of the LLD rule that created the discovered host;<br>`lastcheck` - *(timestamp)* time when the host was last discovered;<br>`ts_delete` - *(timestamp)* time when a host that is no longer discovered will be deleted.|
|selectHttpTests|query|Return an [httpTests](/manual/api/reference/httptest/object) property with host web scenarios.<br><br>Supports `count`.|
|selectInterfaces|query|Return an [interfaces](/manual/api/reference/hostinterface/object) property with host interfaces.<br><br>Supports `count`.|
|selectInventory|query|Return an [inventory](/manual/api/reference/host/object#host_inventory) property with host inventory data.|
|selectItems|query|Return an [items](/manual/api/reference/item/object) property with host items.<br><br>Supports `count`.|
|selectMacros|query|Return a [macros](/manual/api/reference/usermacro/object) property with host macros.|
|selectParentTemplates|query|Return a [parentTemplates](/manual/api/reference/template/object) property with templates that the host is linked to.<br><br>Supports `count`.|
|selectDashboards|query|Return a [dashboards](/manual/api/reference/templatedashboard/object) property.<br><br>Supports `count`.|
|selectTags|query|Return a [tags](/manual/api/reference/host/object#host_tag) property with host tags.|
|selectInheritedTags|query|Return an [inheritedTags](/manual/api/reference/host/object#host_tag) property with tags that are on all templates which are linked to host.|
|selectTriggers|query|Return a [triggers](/manual/api/reference/trigger/object) property with host triggers.<br><br>Supports `count`.|
|selectValueMaps|query|Return a [valuemaps](/manual/api/reference/valuemap/object) property with host value maps.|
|filter|object|Return only those results that exactly match the given filter.<br><br>Accepts an array, where the keys are property names, and the values are either a single value or an array of values to match against.<br><br>Allows filtering by interface properties.|
|limitSelects|integer|Limits the number of records returned by subselects.<br><br>Applies to the following subselects:<br>`selectParentTemplates` - results will be sorted by `host`;<br>`selectInterfaces`;<br>`selectItems` - sorted by `name`;<br>`selectDiscoveries` - sorted by `name`;<br>`selectTriggers` - sorted by `description`;<br>`selectGraphs` - sorted by `name`;<br>`selectDashboards` - sorted by `name`.|
|search|object|Return results that match the given wildcard search.<br><br>Accepts an array, where the keys are property names, and the values are strings to search for. If no additional options are given, this will perform a `LIKE "%…%"` search.<br><br>Allows searching by interface properties. Works only with text fields.|
|searchInventory|object|Return only hosts that have inventory data matching the given wildcard search.<br><br>This parameter is affected by the same additional parameters as `search`.|
|sortfield|string/array|Sort the result by the given properties.<br><br>Possible values are: `hostid`, `host`, `name`, `status`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters).|
|editable|boolean|^|
|excludeSearch|boolean|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

[comment]: # ({/new-77a8fabb})

[comment]: # ({new-7223bab1})
### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-c6ea3cce})
#### Retrieving data by name

Retrieve all data about two hosts named "Zabbix server" and "Linux
server".

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.get",
    "params": {
        "filter": {
            "host": [
                "Zabbix server",
                "Linux server"
            ]
        }
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "maintenances": [],
            "hostid": "10160",
            "proxy_hostid": "0",
            "host": "Zabbix server",
            "status": "0",
            "lastaccess": "0",
            "ipmi_authtype": "-1",
            "ipmi_privilege": "2",
            "ipmi_username": "",
            "ipmi_password": "",
            "ipmi_disable_until": "0",
            "snmp_disable_until": "0",
            "maintenanceid": "0",
            "maintenance_status": "0",
            "maintenance_type": "0",
            "maintenance_from": "0",
            "name": "Zabbix server",
            "description": "The Zabbix monitoring server.",
            "tls_connect": "1",
            "tls_accept": "1",
            "tls_issuer": "",
            "tls_subject": ""
        },
        {
            "maintenances": [],
            "hostid": "10167",
            "proxy_hostid": "0",
            "host": "Linux server",
            "status": "0",
            "lastaccess": "0",
            "ipmi_authtype": "-1",
            "ipmi_privilege": "2",
            "ipmi_username": "",
            "ipmi_password": "",
            "ipmi_disable_until": "0",
            "snmp_disable_until": "0",
            "maintenanceid": "0",
            "maintenance_status": "0",
            "maintenance_type": "0",
            "maintenance_from": "0",
            "name": "Linux server",
            "description": "",
            "tls_connect": "1",
            "tls_accept": "1",
            "tls_issuer": "",
            "tls_subject": ""
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-c6ea3cce})

[comment]: # ({new-9078aff7})
#### Retrieving host groups

Retrieve names of the groups host "Zabbix server" is member of, but no
host details themselves.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.get",
    "params": {
        "output": ["hostid"],
        "selectGroups": "extend",
        "filter": {
            "host": [
                "Zabbix server"
            ]
        }
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 2
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "hostid": "10085",
            "groups": [
                {
                    "groupid": "2",
                    "name": "Linux servers",
                    "internal": "0",
                    "flags": "0"
                },
                {
                    "groupid": "4",
                    "name": "Zabbix servers",
                    "internal": "0",
                    "flags": "0"
                }
            ]
        }
    ],
    "id": 2
}
```

[comment]: # ({/new-9078aff7})

[comment]: # ({new-59c8a46a})
#### Retrieving linked templates

Retrieve the IDs and names of templates linked to host "10084".

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.get",
    "params": {
        "output": ["hostid"],
        "selectParentTemplates": [
            "templateid",
            "name"
        ],
        "hostids": "10084"
    },
    "id": 1,
    "auth": "70785d2b494a7302309b48afcdb3a401"
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "hostid": "10084",
            "parentTemplates": [
                {
                    "name": "Linux",
                    "templateid": "10001"
                },
                {
                    "name": "Zabbix Server",
                    "templateid": "10047"
                }
            ]
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-59c8a46a})

[comment]: # ({new-8be1d99c})
#### Searching by host inventory data

Retrieve hosts that contain "Linux" in the host inventory "OS" field.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.get",
    "params": {
        "output": [
            "host"
        ],
        "selectInventory": [
            "os"
        ],
        "searchInventory": {
            "os": "Linux"
        }
    },
    "id": 2,
    "auth": "7f9e00124c75e8f25facd5c093f3e9a0"
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "hostid": "10084",
            "host": "Zabbix server",
            "inventory": {
                "os": "Linux Ubuntu"
            }
        },
        {
            "hostid": "10107",
            "host": "Linux server",
            "inventory": {
                "os": "Linux Mint"
            }
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-8be1d99c})

[comment]: # ({new-b4e37e5e})
#### Searching by host tags

Retrieve hosts that have tag "Host name" equal to "Linux server".

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.get",
    "params": {
        "output": ["hostid"],
        "selectTags": "extend",
        "evaltype": 0,
        "tags": [
            {
                "tag": "Host name",
                "value": "Linux server",
                "operator": 1
            }
        ]
    },
    "auth": "7f9e00124c75e8f25facd5c093f3e9a0",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "hostid": "10085",
            "tags": [
                {
                    "tag": "Host name",
                    "value": "Linux server"
                },
                {
                    "tag": "OS",
                    "value": "RHEL 7"
                }
            ]
        }
    ],
    "id": 1
}
```

Retrieve hosts that have these tags not only on host level but also in
their linked parent templates.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.get",
    "params": {
        "output": ["name"],
        "tags": [{"tag": "A", "value": "1", "operator": "0"}],
        "inheritedTags": true
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "hostid": "10623",
            "name": "PC room 1"
        },
        {
            "hostid": "10601",
            "name": "Office"
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-b4e37e5e})

[comment]: # ({new-9ebae84e})
#### Searching host with tags and template tags

Retrieve a host with tags and all tags that are linked to parent
templates.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.get",
    "params": {
        "output": ["name"],
        "hostids": 10502,
        "selectTags": ["tag", "value"],
        "selectInheritedTags": ["tag", "value"]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "hostid": "10502",
            "name": "Desktop",
            "tags": [
                {
                    "tag": "A",
                    "value": "1"
                }
            ],
            "inheritedTags": [
                {
                    "tag": "B",
                    "value": "2"
                }
            ]
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-9ebae84e})

[comment]: # ({new-219d4ee1})
#### Searching hosts by problem severity

Retrieve hosts that have "Disaster" problems.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.get",
    "params": {
        "output": ["name"],
        "severities": 5
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "hostid": "10160",
            "name": "Zabbix server"
        }
    ],
    "id": 1
}
```

Retrieve hosts that have "Average" and "High" problems.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.get",
    "params": {
        "output": ["name"],
        "severities": [3, 4]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "hostid": "20170",
            "name": "Database"
        },
        {
            "hostid": "20183",
            "name": "workstation"
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-219d4ee1})

[comment]: # ({new-06c7fe93})
### See also

-   [Host group](/manual/api/reference/hostgroup/object#host_group)
-   [Template](/manual/api/reference/template/object#template)
-   [User
    macro](/manual/api/reference/usermacro/object#hosttemplate_level_macro)
-   [Host
    interface](/manual/api/reference/hostinterface/object#host_interface)

[comment]: # ({/new-06c7fe93})

[comment]: # ({new-55c08f7a})
### Source

CHost::get() in *ui/include/classes/api/services/CHost.php*.

[comment]: # ({/new-55c08f7a})
