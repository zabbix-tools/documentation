[comment]: # translation:outdated

[comment]: # ({new-c572a02a})
# host.update

[comment]: # ({/new-c572a02a})

[comment]: # ({new-12e31652})
### Description

`object host.update(object/array hosts)`

This method allows to update existing hosts.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-12e31652})

[comment]: # ({new-3a8eac6a})
### Parameters

`(object/array)` Host properties to be updated.

The `hostid` property must be defined for each host, all other
properties are optional. Only the given properties will be updated, all
others will remain unchanged.

Note, however, that updating the host technical name will also update
the host's visible name (if not given or empty) by the host's technical
name value.

Additionally to the [standard host properties](object#host), the method
accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|groups|object/array|Host [groups](/manual/api/reference/hostgroup/object#host_group) to replace the current host groups the host belongs to.<br><br>The host groups must have the `groupid` property defined. All host groups that are not listed in the request will be unlinked.|
|interfaces|object/array|Host [interfaces](/manual/api/reference/hostinterface/object) to replace the current host interfaces.<br><br>All interfaces that are not listed in the request will be removed.|
|tags|object/array|Host [tags](/manual/api/reference/host/object#host_tag) to replace the current host tags.<br><br>All tags that are not listed in the request will be removed.|
|inventory|object|Host [inventory](/manual/api/reference/host/object#host_inventory) properties.|
|macros|object/array|[User macros](/manual/api/reference/usermacro/object) to replace the current user macros.<br><br>All macros that are not listed in the request will be removed.|
|templates|object/array|[Templates](/ manual/api/reference/template/object) to replace the currently linked templates. All templates that are not listed in the request will be only unlinked.<br><br>The templates must have the `templateid` property defined.|
|templates\_clear|object/array|[Templates](/ manual/api/reference/template/object) to unlink and clear from the host.<br><br>The templates must have the `templateid` property defined.|

::: notetip
As opposed to the Zabbix frontend, when `name` (visible
host name) is the same as `host` (technical host name), updating `host`
via API will not automatically update `name`. Both properties need to be
updated explicitly.
:::

[comment]: # ({/new-3a8eac6a})

[comment]: # ({new-d166b99b})
### Return values

`(object)` Returns an object containing the IDs of the updated hosts
under the `hostids` property.

[comment]: # ({/new-d166b99b})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-ce50c453})
#### Enabling a host

Enable host monitoring, i.e. set its status to 0.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.update",
    "params": {
        "hostid": "10126",
        "status": 0
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "hostids": [
            "10126"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-ce50c453})

[comment]: # ({new-bea1db7e})
#### Unlinking templates

Unlink and clear two templates from host.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.update",
    "params": {
        "hostid": "10126",
        "templates_clear": [
            {
                "templateid": "10124"
            },
            {
                "templateid": "10125"
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "hostids": [
            "10126"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-bea1db7e})

[comment]: # ({new-98dcfd68})
#### Updating host macros

Replace all host macros with two new ones.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.update",
    "params": {
        "hostid": "10126",
        "macros": [
            {
                "macro": "{$PASS}",
                "value": "password"
            },
            {
                "macro": "{$DISC}",
                "value": "sda",
                "description": "Updated description"
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "hostids": [
            "10126"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-98dcfd68})

[comment]: # ({new-da99f3b8})
#### Updating host inventory

Change inventory mode and add location

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.update",
    "params": {
        "hostid": "10387",
        "inventory_mode": 0,
        "inventory": {
            "location": "Latvia, Riga"
        }
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "hostids": [
            "10387"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-da99f3b8})

[comment]: # ({new-5cc5d950})
#### Updating host tags

Replace all host tags with a new one.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.update",
    "params": {
        "hostid": "10387",
        "tags": {
            "tag": "OS",
            "value": "CentOS 7"
        }
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "hostids": [
            "10387"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-5cc5d950})

[comment]: # ({new-ae2d89bf})
#### Updating discovered host macros

Convert discovery rule created "automatic" macro to "manual" and change its value to "new-value".

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.update",
    "params": {
        "hostid": "10387",
        "macros": {
            "hostmacroid": "5541",
            "value": "new-value",
            "automatic": "0"
        }
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "hostids": [
            "10387"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-ae2d89bf})

[comment]: # ({new-f620e859})
#### Updating host encryption

Update the host "10590" to use PSK encryption only for connections from host to Zabbix server, and change the PSK identity and PSK key.
Note that the host has to be [pre-configured to use PSK](/manual/encryption/using_pre_shared_keys#configuring-psk-for-server-agent-communication-example).

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "host.update",
    "params": {
        "hostid": "10590",
        "tls_connect": 1,
        "tls_accept": 2,
        "tls_psk_identity": "PSK 002",
        "tls_psk": "e560cb0d918d26d31b4f642181f5f570ad89a390931102e5391d08327ba434e9"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "hostids": [
            "10590"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-f620e859})

[comment]: # ({new-23501347})
### See also

-   [host.massadd](massadd)
-   [host.massupdate](massupdate)
-   [host.massremove](massremove)
-   [Host group](/manual/api/reference/hostgroup/object#host_group)
-   [Template](/manual/api/reference/template/object#template)
-   [User
    macro](/manual/api/reference/usermacro/object#hosttemplate_level_macro)
-   [Host
    interface](/manual/api/reference/hostinterface/object#host_interface)
-   [Host inventory](object#host_inventory)
-   [Host tag](object#host_tag)

[comment]: # ({/new-23501347})

[comment]: # ({new-a39b5a01})
### Source

CHost::update() in *ui/include/classes/api/services/CHost.php*.

[comment]: # ({/new-a39b5a01})
