[comment]: # translation:outdated

[comment]: # ({new-39f5a4d3})
# 1 Definições

[comment]: # ({/new-39f5a4d3})

[comment]: # ({new-a9697948})
#### Visão geral

Enquanto a maioria das coisas podem ser configuradas através da
interface web do Zabbix, algumas customizações só podem ser feitas ao
editar diretamente seus arquivos de configuração.

O principal arquivo é o `defines.inc.php` localizado no diretório
'/include' da interface web do Zabbix.

[comment]: # ({/new-a9697948})

[comment]: # ({new-3260c875})
#### Parâmetros

Os parâmetros a seguir podem ser interessantes para os usuários:

-   ZBX\_LOGIN\_ATTEMPTS

Quantidade de tentativas sem sucesso de login que serão permitidas à um
usuário do Zabbix antes que seja aplicado um bloqueio de login (ver
parâmetro ZBX\_LOGIN\_BLOCK). Por padrão são 5 tentativas. Uma vez que a
quantidade máxima de falhas de login for alcançada cada falha adicional
resultará em novo bloqueio. Utilizado com a [autenticação
interna](/pt/manual/web_interface/frontend_sections/administration/authentication)
apenas.

-   ZBX\_LOGIN\_BLOCK

Quantidade de segundos que um usuário será bloqueado de tentar acessar a
interface web após determinada quantidade de falhas de login (ver
parâmetro ZBX\_LOGIN\_ATTEMPTS). O padrão são 30 segundos. Utilizado com
a [autenticação
interna](/pt/manual/web_interface/frontend_sections/administration/authentication)
apenas.

-   ZBX\_PERIOD\_DEFAULT

Período padrão de um gráfico, em segundos. O padrão é uma hora.

-   ZBX\_MIN\_PERIOD

Período mínimo de um gráfico, em segundos. O padrão é uma hora.

-   ZBX\_MAX\_PERIOD

Período máximo de um gráfico, em segundos. Desde o Zabbix 1.6.7 o padrão
são 2 anos.

-   ZBX\_HISTORY\_PERIOD

Período máximo do histórico que será apresentado nas páginas de *Dados
recentes*, *Visão geral* e na *Visão geral* em uma tela, em segundos.
Por padrão são 86400 segundos (24 horas). O período passa a ser
ilimitado, se definido para 0 segundos.

-   GRAPH\_YAXIS\_SIDE\_DEFAULT

Localização padrão do eixo Y em gráficos simples e valor padrão quando
se adicionam itens em gráficos customizados. Valores possíveis: 0 -
esquerda, 1 - direita. Padrão: 0

-   DEFAULT\_LATEST\_ISSUES\_CNT

Controla quandos incidentes serão apresentados no widget do dashboard's
*Últimos n incidentes*. Por padrão são exibidos 20 incidentes.

-   SCREEN\_REFRESH\_TIMEOUT (disponível desde o Zabbix 2.0.4)

Utilizado em telas, define o tempo limite em segundos para um elemento
da tela se atualizar. Quando definido e ultrapassada a quantidade de
segundos o elemento da tela que não terminou a carga será enegrecido.
Padrão: 30

-   SCREEN\_REFRESH\_RESPONSIVENESS (disponível desde o Zabbix 2.0.4)

Utilizado em telas, define a quantidade de segundos máxima a se aguardar
antes de ignorar determinada consulta. De outra forma, se um elemento da
tela está em estado de atualização todas as demais consultas ficarão
aguardando o retorno. Com este parâmetro em uso, outro pedido de
atualização poderá ser enviado após N segundos sem ter que aguardar a
resposta do primeiro. Padrão: 10

[comment]: # ({/new-3260c875})
