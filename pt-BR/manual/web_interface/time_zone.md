[comment]: # translation:outdated

[comment]: # ({new-7eb2710c})
# 11 Time zones

[comment]: # ({/new-7eb2710c})

[comment]: # ({new-e102f05a})
#### Overview

The frontend time zone can be set globally in the frontend and adjusted
for individual users.

![](../../../assets/en/manual/web_interface/time_zone_global.png)

If *System* is selected, the web server time zone will be used for the
frontend (including the value of 'date.timezone' of php.ini, if set),
while Zabbix server will use the time zone of the machine it is running
on.

::: noteclassic
Zabbix server will only use the specified global/user
timezone when expanding macros in notifications (e.g. {EVENT.TIME} can
expand to a different time zone per user) and for the time limit when
notifications are sent (see "When active" setting in user [media
configuration](/manual/config/notifications/media#user_media)).
:::

[comment]: # ({/new-e102f05a})

[comment]: # ({93c9d41c-297606c9})
#### Configuração

Fuso horário mundial:

-   pode ser definido manualmente ao[Instalar](/manual/installation/frontend)
    ofrontend
-   pode ser alterado em *Administração* → *Geral* →
    *[GUI](/manual/web_interface/frontend_sections/administration/general#gui)*

Fuso horário em nível de usuário:

-   pode ser definido quando
    [Configurando/Atualizando](/manual/config/users_and_usergroups/user#general_attributes)
    um usuário
-   pode ser definido por cada usuário em seu [perfil de usuário](/manual/web_interface/user_profile#configuration)

[comment]: # ({/93c9d41c-297606c9})
