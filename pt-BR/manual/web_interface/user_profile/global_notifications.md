[comment]: # translation:outdated

[comment]: # ({new-4cdbbfea})
# 1 Notificações globais

[comment]: # ({/new-4cdbbfea})

[comment]: # ({new-5c281f24})
#### Visão geral

As notificações globais são uma forma de apresentar incidentes que estão
ocorrendo neste momento, independente de em qual tela do Zabbix você
esteja.

Sem as notificações globais, quando você estiver trabalhando em outras
páginas que não a página de *Status das triggers* ou *Dashboard* você
não verá nenhuma informação sobre os incidentes que estão ocorrendo.

As notificações globais envolvem tanto mensagens visuais quanto
[sonoras](sound).

[comment]: # ({/new-5c281f24})

[comment]: # ({new-35c1ef59})
#### Configuração

As notificações globais podem ser ativadas para cada usuário na aba
*Mensagens* do [perfil de
usuário](/pt/manual/web_interface/user_profile).

![profile3.png](../../../../assets/en/manual/web_interface/profile3.png)

|Parâmetro|Descrição|
|----------|-----------|
|*Ativar mensagens globais*|Marque esta opção para ativar as notificações globais.|
|*Tempo limite para a mensagem (segundos)*|Define por quanto tempo a mensagem será apresentada. Por padrão, serão apresentadas por 60 segundos.|
|*Executar som*|Você pode definir qual som será tocado.<br>**Uma vez** - o som será tocado apenas uma vez e totalmente.<br>**10 segundos** - o som será repetido por 10 segundos.<br>**Enquanto a mensagem estiver visível** - o som será repetido enquanto a mensagem estiver visível (tempo limite para a mensagem).|
|*Severidade de trigger*|Para cada nível de severidade é possível ativar a notificação e definir um som apropriado.<br>Se nenhuma severidade estiver selecionada então nenhuma mensagem será exibida.<br>As mensagens de recuperação também só serão exibidas para as severidades selecionadas e se a opção de recuperação estiver marcada. Então se você marcar a opção *Recuperação* e *Desastre*, as notificações globais serão apresentadas para incidentes e recuperações com o nível de desastre apenas.|

[comment]: # ({/new-35c1ef59})

[comment]: # ({new-933467f7})
##### Apresentação das notificações globais

Quando as mensagens chegam, elas são apresentadas em uma seção flutuante
no canto superior direito da tela. Esta seção pode ser reposicionada
verticalmente ao arrasta-la por seu título.

![global\_messages.png](../../../../assets/en/manual/web_interface/global_messages.png)

Para esta seção vários controles estão disponíveis:

-   ![](../../../../assets/en/manual/about/message_button_snooze.png)
    **Soneca** silencia o alarme atual;
-   ![](../../../../assets/en/manual/about/message_button_mute.png)
    **Silencioso/Com som** alterna entre tocar ou não os sons de
    alarmes;
-   ![](../../../../assets/en/manual/about/message_button_clear.png)
    **Limpar** remove todas as mensagens visíveis.

[comment]: # ({/new-933467f7})
