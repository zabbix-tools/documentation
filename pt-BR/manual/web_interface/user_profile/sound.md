[comment]: # translation:outdated

[comment]: # ({new-0429da2a})
# 2 Sons em browsers

[comment]: # ({/new-0429da2a})

[comment]: # ({new-ca9dfcae})
#### Visão geral

Para os sons serem tocados na interface web do Zabbix, a opção *Ativar
notificações globais* precisa estar ativa no perfil do usuário na aba
*Mensagens*, com os respectivos níveis de severidade marcados e sons
ativos nos pop-ups de notificação global.

Os sons na interface web do Zabbix foram testado com sucesso nos
seguintes navegadores web e versões, não sendo necessária nenhuma
configuração adicional:

-   Firefox 3.5.16 no Linux
-   Opera 11.01 no Linux
-   Google Chrome 9.0 no Windows
-   Firefox 3.5.16 no Windows
-   IE7 browser no Windows
-   Opera v11.01 no Windows
-   Chrome v9.0 no Windows
-   Safari v5.0 no Windows, mas o navegador requer que o *Reprodutor
    Quick Time* esteja instalado

#### Requisitos adicionais

##### Firefox v 3.5.16 no Windows

Para tocar arquivos `wav` o navegador Firefox precisa usar uma destas
aplicações:

-   Windows Media Player
-   Quick Time plug-in.

Então, em *Ferramentas → Opções → Aplicações*, em "Wave sound
(audio/wav)" configure o Windows Media Player para tocar os arquivos.

##### Safari 5.0

*Quick Time Player* é necessário.

##### Microsoft Internet Explorer

Para tocar sons no MSIE7 e no MSIE8:

-   Em *Tools → Internet Options → Advanced* ative *Play sounds in
    webpages*
-   Em *Tools → Manage Add-ons...* ative **Windows Media Player**
-   No Windows Media Player, em *Tools→Options→File Types* ative
    *Windows audio file (wav)*

No Windows Media Player, na aba Ferramentas→Opções, o "Tipo de arquivo"
só estará disponǘiel se vocẽ for um usuário "Administrador".

Uma informação adicional - se o IE não tiver o arquivo \*.wav em seu
cache local (%userprofile%\\Local Settings\\Temporary Internet Files) a
primeira vez que for chamado o som não será tocado.

##### Não funciona

Navegadores com os quais o recurso de tocar som do Zabbix não funciona:

-   Opera 10.11 no Linux.

[comment]: # ({/new-ca9dfcae})
