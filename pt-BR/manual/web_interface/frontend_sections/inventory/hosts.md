[comment]: # translation:outdated

[comment]: # ({new-cad9ee1a})
# 2 Hosts

[comment]: # ({/new-cad9ee1a})

[comment]: # ({new-50a329ce})
#### Visão geral

Através do módulo *Inventário → Hosts* é apresentada uma visão detalhada
[dos dados do inventário](/pt/manual/config/hosts/inventory).

Na barra de título é possível selecionar um grupo de hosts a apresentar.
É possível filtrar por qualquer campo do inventário para exibir apenas
os hosts de interesse.

![](../../../../../assets/en/manual/web_interface/inventory_hosts.png){width="600"}

Para apresentar todos os hosts inventariados, selecione a opção
**Todos** na caixa de seleção de grupo de hosts e limpe os filtros.

Somente hosts com inventário ativo serão apresentados neste relatório.

[comment]: # ({/new-50a329ce})

[comment]: # ({new-1226a553})
#### Detalhes do inventário

Ao clicar no nome de um host no relatório você será direcionado à um
detalhamento do inventário do mesmo.

A aba de **Visão geral** contêm algumas informações gerais sobre o host,
dados recentes e links para opções de configuração do host:

![](../../../../../assets/en/manual/web_interface/inventory_host.png)

A aba **Detalhes** apresenta todos os campos de inventário preenchidos
no host:

![](../../../../../assets/en/manual/web_interface/inventory_host2.png)

A complitude deste relatório depende de quão preenchido foi o inventário
do host. Se não existir nenhum dado no inventário (ele está ativo mas
sem dados) a aba *Detalhes* estará inativa.

[comment]: # ({/new-1226a553})
