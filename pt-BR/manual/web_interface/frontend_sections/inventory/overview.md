[comment]: # translation:outdated

[comment]: # ({new-86576837})
# 1 Visão geral

Através do módulo *Inventário → Visão geral* você pode visualizar uma
visão geral do [inventário de hosts](/pt/manual/config/hosts/inventory).

Para que a visão geral seja apresentada, selecione um grupo de host (ou
todos) e o campo do inventário que irá agrupar os dados. Será
apresentada a quantidade de hosts correspondentes a valor localizado
naquele campo do inventário.

![](../../../../../assets/en/manual/web_interface/inventory_overview.png){width="600"}

A completude de uma visão geral depende de quanta informação estiver
disponível no inventário dos hosts.

Os números na coluna *Contagem de hosts* possuem links; eles levam à um
relatório detalhado com os hosts que foram contabilizados na visão
geral.

![](../../../../../assets/en/manual/web_interface/inventory_filtered.png){width="600"}

[comment]: # ({/new-86576837})


[comment]: # ({new-8655b192})
#### Overview

The *Inventory → Overview* section provides ways of having an overview
of [host inventory](/manual/config/hosts/inventory) data.

For an overview to be displayed, choose host groups (or none) and the
inventory field by which to display data. The number of hosts
corresponding to each entry of the chosen field will be displayed.

![](../../../../../assets/en/manual/web_interface/inventory_overview.png){width="600"}

The completeness of an overview depends on how much inventory
information is maintained with the hosts.

Numbers in the *Host count* column are links; they lead to these hosts
being filtered out in the *Host Inventories* table.

![](../../../../../assets/en/manual/web_interface/inventory_filtered1.png){width="600"}

[comment]: # ({/new-8655b192})
