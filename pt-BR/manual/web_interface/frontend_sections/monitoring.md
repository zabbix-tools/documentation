[comment]: # translation:outdated

[comment]: # ({new-7b41e2d5})
# 1 Monitoramento

[comment]: # ({/new-7b41e2d5})

[comment]: # ({new-c99a8958})
#### Visão geral

O menu de monitoramento agrupa os módulos de apresentação de dados.
Qualquer que seja a informação coletada pelo Zabbix ela estará
disponível para consulta através de uma das opções deste menu.

[comment]: # ({/new-c99a8958})


[comment]: # ({new-e7927a8f})
#### View mode buttons

The following buttons located in the top right corner are common for
every section:

|   |   |
|---|---|
|![](../../../../assets/en/manual/web_interface/button_kiosk.png)|Display page in kiosk mode. In this mode only page content is displayed.<br>To exit kiosk mode, move the mouse cursor until the ![](../../../../assets/en/manual/web_interface/button_kiosk_leave.png) exit button appears and click on it. You will be taken back to normal mode.|

[comment]: # ({/new-e7927a8f})
