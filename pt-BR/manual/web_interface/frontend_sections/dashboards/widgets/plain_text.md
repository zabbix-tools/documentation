[comment]: # translation:outdated

[comment]: # ({new-c1a63b69})
# 15 Plain text

[comment]: # ({/new-c1a63b69})

[comment]: # ({new-569590b5})
#### Overview

In the plain text widget, you can display the latest item data in plain
text.

[comment]: # ({/new-569590b5})

[comment]: # ({new-e25ab8a1})
#### Configuration

To configure, select *Plain text* as type:

![](../../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/plain_text.png)

In addition to the parameters that are [common](/manual/web_interface/frontend_sections/dashboards/widgets#common-parameters) 
for all widgets, you may set the following specific options:

|   |   |
|--|--------|
|*Items*|Select the items.|
|*Items location*|Choose the location of selected items to be displayed in the widget.|
|*Show lines*|Set how many latest data lines will be displayed in the widget.|
|*Show text as HTML*|Set to display text as HTML.|
|*[Dynamic item](/manual/web_interface/frontend_sections/dashboards#dynamic_widgets)*|Set to display different data depending on the selected host.|

[comment]: # ({/new-e25ab8a1})
