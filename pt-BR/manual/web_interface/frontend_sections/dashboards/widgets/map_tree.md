[comment]: # translation:outdated

[comment]: # ({new-0f95e7b7})
# 14 Map navigation tree

[comment]: # ({/new-0f95e7b7})

[comment]: # ({new-e61997b9})
#### Overview

This widget allows building a hierarchy of existing maps while also
displaying problem statistics with each included map and map group.

It becomes even more powerful if you link the *Map* widget to the
navigation tree. In this case, clicking on a map name in the navigation
tree displays the map in full in the *Map* widget.

![](../../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/map_to_display.png){width="600"}

Statistics with the top-level map in the hierarchy display a sum of
problems of all sub-maps and their own problems.

[comment]: # ({/new-e61997b9})

[comment]: # ({new-80a3a493})
#### Configuration

To configure the navigation tree widget, select *Map navigation tree* as
type:

![](../../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/map_tree.png)

In addition to the parameters that are [common](/manual/web_interface/frontend_sections/dashboards/widgets#common-parameters) 
for all widgets, you may set the following specific options:

|   |   |
|--|--------|
|*Show unavailable maps*|Mark this checkbox to display maps that the user does not have read permission to.<br>Unavailable maps in the navigation tree will be displayed with a grayed-out icon.<br>Note that if this checkbox is marked, available sub-maps are displayed even if the parent level map is unavailable. If unmarked, available sub-maps to an unavailable parent map will not be displayed at all.<br>Problem count is calculated based on available maps and available map elements.|

[comment]: # ({/new-80a3a493})

[comment]: # ({new-947854a0})

Navigation tree elements are displayed in a list. You can:

-   drag an element (including its child elements) to a new place in the list;
-   expand or collapse an element to display or hide its child elements;
-   add a child element (with or without a linked map) to an element;
-   add multiple child elements (with linked maps) to an element;
-   edit an element;
-   remove an element (including its child elements).

![](../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/map_tree_element.png)

[comment]: # ({/new-947854a0})

[comment]: # ({new-b55502b3})
##### Element configuration

[comment]: # ({/new-b55502b3})

[comment]: # ({new-bcccd25d})

To configure a navigation tree element, either add a new element or edit an existing element.

![](../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/map_tree_element_configuration.png)

The following navigation tree element configuration parameters are available:

|   |   |
|--|--------|
|*Name*|Enter the navigation tree element name.|
|*Linked map*|Select the map to link to the navigation tree element.<br>This field is auto-complete so starting to type the name of a map will offer a dropdown of matching maps.|
|*Add submaps*|Mark this checkbox to add the [submaps](/manual/config/visualization/maps/map#adding-elements) of the linked map as child elements to the navigation tree element.|

[comment]: # ({/new-bcccd25d})
