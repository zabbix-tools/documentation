<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="pt-BR" datatype="plaintext" original="manual/web_interface/frontend_sections/dashboards/widgets/problems.md">
    <body>
      <trans-unit id="791a9202" xml:space="preserve">
        <source># 17 Problems</source>
      </trans-unit>
      <trans-unit id="a5eba2b2" xml:space="preserve">
        <source>#### Overview

In this widget you can display current problems. The information in this
widget is similar to *Monitoring* → *Problems*.</source>
      </trans-unit>
      <trans-unit id="e51a32f4" xml:space="preserve">
        <source>#### Configuration

To configure, select *Problems* as type:

![](../../../../../../assets/en/manual/web_interface/frontend_sections/dashboards/widgets/problems_widget.png){width="600"}

You can limit how many problems are displayed in the widget in various
ways - by problem status, problem name, severity, host group, host,
event tag, acknowledgment status, etc.

|Parameter|Description|
|--|--------|
|*Show*|Filter by problem status:&lt;br&gt;**Recent problems** - unresolved and recently resolved problems are displayed (default);&lt;br&gt;**Problems** - unresolved problems are displayed;&lt;br&gt;**History** - history of all events is displayed.|
|*Host groups*|Enter host groups to display problems of in the widget.&lt;br&gt;This field is auto-complete so starting to type the name of a group will offer a dropdown of matching groups.&lt;br&gt;Specifying a parent host group implicitly selects all nested host groups.&lt;br&gt;Problems from these host groups will be displayed in the widget; if no host groups are entered, problems from all host groups will be displayed.&lt;br&gt;This parameter is not available when configuring the widget on a [template dashboard](/manual/config/templates/template#adding-dashboards).|
|*Exclude host groups*|Enter host groups to hide problems of from the widget.&lt;br&gt;This field is auto-complete so starting to type the name of a group will offer a dropdown of matching groups.&lt;br&gt;Specifying a parent host group implicitly selects all nested host groups.&lt;br&gt;Problems from these host groups will not be displayed in the widget. For example, hosts 001, 002, 003 may be in Group A and hosts 002, 003 in Group B as well. If we select to *show* Group A and *exclude* Group B at the same time, only problems from host 001 will be displayed in the widget.&lt;br&gt;This parameter is not available when configuring the widget on a [template dashboard](/manual/config/templates/template#adding-dashboards).|
|*Hosts*|Enter hosts to display problems of in the widget.&lt;br&gt;This field is auto-complete so starting to type the name of a host will offer a dropdown of matching hosts.&lt;br&gt;If no hosts are entered, problems of all hosts will be displayed.&lt;br&gt;This parameter is not available when configuring the widget on a [template dashboard](/manual/config/templates/template#adding-dashboards).|
|*Problem*|You can limit the number of problems displayed by their name.&lt;br&gt;If you enter a string here, only those problems whose name contains the entered string will be displayed.&lt;br&gt;Macros are not expanded.|
|*Severity*|Mark the problem severities to be displayed in the widget.|
|*Problem tags*|Specify problem tags to limit the number of problems displayed in the widget.&lt;br&gt;It is possible to include as well as exclude specific tags and tag values. Several conditions can be set. Tag name matching is always case-sensitive.&lt;br&gt;&lt;br&gt;There are several operators available for each condition:&lt;br&gt;**Exists** - include the specified tag names;&lt;br&gt;**Equals** - include the specified tag names and values (case-sensitive);&lt;br&gt;**Contains** - include the specified tag names where the tag values contain the entered string (substring match, case-insensitive);&lt;br&gt;**Does not exist** - exclude the specified tag names;&lt;br&gt;**Does not equal** - exclude the specified tag names and values (case-sensitive);&lt;br&gt;**Does not contain** - exclude the specified tag names where the tag values contain the entered string (substring match, case-insensitive).&lt;br&gt;&lt;br&gt;There are two calculation types for conditions:&lt;br&gt;**And/Or** - all conditions must be met, conditions having the same tag name will be grouped by the *Or* condition;&lt;br&gt;**Or** - enough if one condition is met.&lt;br&gt;&lt;br&gt;When filtered, the tags specified here will be displayed first with the problem, unless overridden by the *Tag display priority* (see below) list.|
|*Show tags*|Select the number of displayed tags:&lt;br&gt;**None** - no *Tags* column;&lt;br&gt;**1** - *Tags* column contains one tag;&lt;br&gt;**2** - *Tags* column contains two tags;&lt;br&gt;**3** - *Tags* column contains three tags.&lt;br&gt;To see all tags for the problem roll your mouse over the three dots icon.|
|*Tag name*|Select tag name display mode:&lt;br&gt;**Full** - tag names and values are displayed in full;&lt;br&gt;**Shortened** - tag names are shortened to 3 symbols, but tag values are displayed in full;&lt;br&gt;**None** - only tag values are displayed; no names.|
|*Tag display priority*|Enter tag display priority for a problem, as a comma-separated list of tags.&lt;br&gt;Only tag names should be used, no values.&lt;br&gt;Example: `Services,Applications,Application`&lt;br&gt;The tags of this list will always be displayed first, overriding the natural ordering by alphabet.|
|*Show operational data*|Select the mode for displaying [operational data](/manual/web_interface/frontend_sections/monitoring/problems#operational_data_of_problems):&lt;br&gt;**None** - no operational data is displayed;&lt;br&gt;**Separately** - operational data is displayed in a separate column;&lt;br&gt;**With problem name** - append operational data to the problem name, using parentheses for the operational data.|
|*Show symptoms*|Mark the checkbox to display in its own line problems classified as symptoms.|
|*Show suppressed problems*|Mark the checkbox to display problems that would otherwise be suppressed (not shown) because of host maintenance or single [problem suppression](/manual/acknowledgment/suppression).|
|*Acknowledgement status*|Filter to display all problems, unacknowledged problems only, or acknowledged problems only. Mark the additional checkbox to filter out those problems ever acknowledged by you.|
|*Sort entries by*|Sort entries by:&lt;br&gt;**Time** (descending or ascending);&lt;br&gt;**Severity** (descending or ascending);&lt;br&gt;**Problem name** (descending or ascending);&lt;br&gt;**Host** (descending or ascending).&lt;br&gt;&lt;br&gt;Sorting entries by **Host** (descending or ascending) is not available when configuring the widget on a [template dashboard](/manual/config/templates/template#adding-dashboards).|
|*Show timeline*|Mark the checkbox to display a visual timeline.|
|*Show lines*|Specify the number of problem lines to display.|</source>
      </trans-unit>
      <trans-unit id="699e3996" xml:space="preserve">
        <source>
#### Using the widget

![](../../../../../../assets/en/manual/web_interface/frontend_sections/dashboards/problems_widget_view.png){width="600"}

Problems widget offers quick access to additional information: 

- Click on the problem date and time to view [event details](/manual/web_interface/frontend_sections/monitoring/problems#viewing-details).
- If Info column is not empty, you can hover over displayed icon to view additional details.
- Click on the host name to open the [host menu](/manual/web_interface/menu/host_menu).
- Click on the problem name to open the [event menu](/manual/web_interface/menu/event_menu).
- Hover over or click on the problem duration to view [problem event popup](#problem-event-popup).
- Press on the Yes or No in the Acknowledge (Ack) column to [update a problem](/manual/acknowledgment#updating-problems).
- Hover over or press on the gray arrow icon in Actions column to view list of executed actions.

##### Problem event popup

The problem event popup includes the list of problem events for this trigger and, if defined, the trigger description 
and a clickable URL.

![](../../../../../../assets/en/manual/web_interface/problem_event_popup.png){width="600"}

To bring up the problem event popup:

-   Roll a mouse over the problem duration in the *Duration* column of the *Problems* widget. 
    The popup disappears once you remove the mouse from the duration.
-   Click on the duration in the *Duration* column of the *Problems* widget. 
    The popup disappears only if you click on the duration again.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
