[comment]: # translation:outdated

[comment]: # ({new-38b63d3a})
# 1 Action log

[comment]: # ({/new-38b63d3a})

[comment]: # ({new-b37edab5})
#### Overview

In the action log widget, you can display details of action operations
(notifications, remote commands). It replicates information from
*Administration → Audit log*.

[comment]: # ({/new-b37edab5})

[comment]: # ({new-95422d28})
#### Configuration

To configure, select *Action log* as type:

![](../../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/action_log.png)

In addition to the parameters that are [common](/manual/web_interface/frontend_sections/dashboards/widgets#common-parameters) 
for all widgets, you may set the following specific options:

|   |   |
|--|--------|
|*Sort entries by*|Sort entries by:<br>**Time** (descending or ascending)<br>**Type** (descending or ascending)<br>**Status** (descending or ascending)<br>**Recipient** (descending or ascending).|
|*Show lines*|Set how many action log lines will be displayed in the widget.|

[comment]: # ({/new-95422d28})
