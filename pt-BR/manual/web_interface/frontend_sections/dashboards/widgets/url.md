[comment]: # translation:outdated

[comment]: # ({new-8f310831})
# 23 URL

[comment]: # ({/new-8f310831})

[comment]: # ({new-de63a307})
#### Overview

This widget displays the content retrieved from the specified URL.

[comment]: # ({/new-de63a307})

[comment]: # ({new-b6837968})
#### Configuration

To configure, select *URL* as type:

![](../../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/url.png)

In addition to the parameters that are [common](/manual/web_interface/frontend_sections/dashboards/widgets#common-parameters) 
for all widgets, you may set the following specific options:

|   |   |
|--|--------|
|*URL*|Enter the URL to display.<br>Relative paths are allowed since Zabbix 4.4.8.<br>{HOST.\*} macros are supported.|
|*[Dynamic item](/manual/web_interface/frontend_sections/dashboards#dynamic_widgets)*|Set to display different URL content depending on the selected host.<br>This can work if {HOST.\*} macros are used in the URL.|

::: noteimportant
Browsers might not load an HTTP page included in
the widget if Zabbix frontend is accessed over HTTPS.
:::

[comment]: # ({/new-b6837968})
