[comment]: # translation:outdated

[comment]: # ({new-fa0262ff})
# 1 Geral

[comment]: # ({/new-fa0262ff})

[comment]: # ({new-f36c2788})
#### Visão geral

O módulo *Administração → Geral* contêm várias telas de configurações
relacionadas à própria interface web e customização do Zabbix.

A caixa de seleção situada no canto direito da barra de título permite
alternar entre as diferentes telas de configuração.

![](../../../../../assets/en/manual/web_interface/frontend_sections/administration/general_dropdown.png){width="600"}

[comment]: # ({/new-f36c2788})

[comment]: # ({new-4513c0d7})
##### - GUI

Esta tela fornece customização de vários valores padrão para a interface
web.

![](../../../../../assets/en/manual/web_interface/frontend_sections/administration/general_gui.png)

Parâmetros de configuração:

|Parâmetro|Descrição|
|----------|-----------|
|*Tema padrão*|Tema a ser exibido por padrão aos usuários.|
|*Primeira entrada da caixa suspensa*|Define se a primeira entrada em uma caixa suspensa deverá ser *Todos* ou *Nada*.<br>Em conjunto com a opção *lembrar selecionado* marcada, o último elemento selecionado será lembrado (ao invés do valor padrão) quando você navegar para outra página.|
|*Limite de busca/Filtro de elementos*|Quantidade máxima de elementos (linhas) que serão apresentados em uma lista na interface web, como, por exemplo, em *Monitoramento → Eventos* ou *Configuração → Hosts*.<br>*Nota*: Se for definido para, por exemplo, '50', apenas os 50 primeiros elementos serão apresentados em todas as listas da interface web. Se alguma lista contiver mais de cinquenta elementos, será adicionado o sinal indicativo '+' em *"Exibindo 1 a 50 de **50+** encontrados"*. Da mesma forma, se um filtro for utilizado e continuar existindo mais de 50 correspondências, somente os 50 primeiros serão exibidos.|
|*Máximo de elementos mostrados\\\\dentro de uma célula de tabela*|Para entradas que são mostradas em uma célula de uma tabela, não serão exibidos mais registros do que o aqui definido.|
|*Ativar reconhecimento de eventos*|Este parâmetro define se o reconhecimento de eventos estará ativo na interface web.|
|*Não exibir eventos mais\\\\antigos que (em dias)*|Este parâmetro limita qual o limite máximo (em dias) de pesquisa retroativa para a tela de status de triggers. O padrão são 7 dias.|
|*Máximo de eventos por trigger a exibir*|Quantidade máxima de eventos a serem exibidos para cada trigger na tela de status das triggers. O padrão são 100.|
|*Exibir um alerta se o servidor Zabbix estiver fora do ar*|Este parâmetro ativa a exibição de uma mensagem de alerta no navegador caso o Zabbix Server esteja fora do ar. A mensagem continua visível mesmo que a tela seja rolada para baixo. Se o mouse se posicionar em cima da mensagem ela irá desaparecer temporariamente.<br>Suportado desde o Zabbix **2.0.1**.|

[comment]: # ({/new-4513c0d7})

[comment]: # ({new-2cf4d492})
##### - Limpeza de dados

O processo de limpeza de dados (housekeeper) é um processo periódico
executado pelo Zabbix Server. O processo remove as informações vencidas
e informações apagadas pelo usuário.

![](../../../../../assets/en/manual/web_interface/frontend_sections/administration/general_housekeeper.png)

Nesta seção as tarefas de limpeza de dados podem ser ativadas ou
desativadas em grupos:

-   Eventos e alertas
-   Serviços de TI
-   Auditoria
-   Sessões de usuário
-   Histórico
-   Estatísticas (médias)

Quando o processo de limpeza de dados está ativo, é possível definir o
período de retenção de dados.

Para o histórico e estatísticas existem opções adicionais: *Substituir o
período de histórico do item* e *Substituir o período de histórico das
estatísticas (médias)*. Esta opção permite que você defina de forma
global por quantos dias deverão ser mantidos os dados de
histórico/médias, estas opções sobrescrevem as configurações feitas
diretamente nos [itens](/pt/manual/config/items/item).

Também é possível sobrescrever o período de armazenamento de
histórico/médias com o processo de limpeza inativo. Assim, ao utilizar
um processo externo de limpeza de dados o período de armazenamento pode
ser definido usando o campo *Período de armazenamento de dados (em
dias)*.

O botão *Restaurar padrão* restaura as configurações desta tela ao
padrão da ferramenta.

[comment]: # ({/new-2cf4d492})



[comment]: # ({new-6905e224})
##### - Expressões regulares

Esta tela permite que você crie expressões regulares customizadas que
poderão ser utilizadas em diversos locais da interface web. Veja mais no
manual de [expressões regulares](/pt/manual/regular_expressions) .

[comment]: # ({/new-6905e224})

[comment]: # ({new-f8d0c75f})
##### - Macros

Esta tela permite que você defina macros de usuário em nível global.

![](../../../../../assets/en/manual/web_interface/frontend_sections/administration/general_macros.png)

Consulte o manual de [macros de
usuário](/pt/manual/config/macros/usermacros) para maiores detalhes.

[comment]: # ({/new-f8d0c75f})

[comment]: # ({new-b9fd47e9})
##### - Mapeamento de valores

Esta tela permite que você defina os mapeamentos de valores. Este
recurso é particulamente útil ao traduzir códigos de resposta em
representações "humanizadas" da informação.

![](../../../../../assets/en/manual/web_interface/value_maps.png){width="600"}

Consulte o manual de [mapeamento de
valores](/pt/manual/config/items/mapping) para maiores detalhes.

[comment]: # ({/new-b9fd47e9})


[comment]: # ({new-cca854b5})
##### - Severidades de trigger

Esta tela permite que você customize as [severidades de
trigger](/pt/manual/config/triggers/severity), tanto seus nomes quanto
cores.

![](../../../../../assets/en/manual/web_interface/frontend_sections/administration/general_severities.png){width="600"}

Você pode informar novos nomes e códigos de cor para os níveis de
severidade (você também pode selecionar as cores em uma paleta de
cores).

Consulte o manual de [customização de severidades de
trigger](/pt/manual/config/triggers/customseverities) para maiores
detalhes.

[comment]: # ({/new-cca854b5})

[comment]: # ({new-09d9d657})
##### - Opções de exibição de trigger

Esta tela permite que você configure como os status de trigger são
apresentados na interface web.

![](../../../../../assets/en/manual/web_interface/frontend_sections/administration/general_trigger_display.png)

Você pode customizar as cores de eventos reconhecidos/não reconhecidos,
habilitar/desabilitar a opção de piscar e modificar o tempo que deve-se
dar o destaque piscante nas mudanças de estado de trigger.

[comment]: # ({/new-09d9d657})

[comment]: # ({new-ef76d5b4})
##### - Outros parâmetros

Esta tela permite que você configure vários outros parâmetros da
interface web.

![](../../../../../assets/en/manual/web_interface/frontend_sections/administration/general_other.png)

|Parâmetro|Descrição|
|----------|-----------|
|*Atualizar itens não suportados (em segundos)*|Alguns itens passam para o estado 'não suportado' devido a erros em parâmetros de usuário ou por causa que o item não é suportado pelo agente. O Zabbix pode ser configurado para, de forma periódica, tentar novamente a coleta de um item 'não suportado'.<br>O Zabbix irá reativar o item não suportado a cada N segundos (aqui definido). Se for definido para 0, a ativação automática não irá mais ocorrer.<br>Os proxies verificam itens não suportados a cada 10 minutes, não existe possibilidade de personalização para o proxy.|
|*Grupo para hosts descobertos*|Os hosts descobertos por [regras de descoberta de rede](/pt/manual/discovery/network_discovery) e pelo processo de [autorregistro](/pt/manual/discovery/auto_registration) serão automaticamente adicionados em um grupo de host, aqui definido.|
|*Modo padrão do inventário em novos hosts*|Define o modo padrão de [inventário](/pt/manual/config/hosts/inventory) para os hosts. Este valor será utilizado toda vez que um novo host ou novo protótipo de host for criado pelo Zabbix Server ou pela interface web. O valor aqui definido pode ser sobrescrito pela operação de *Definir modo de inventário* disponível em ações com origem em descoberta/autorregistro.|
|*Grupo de usuários que receberá a\\\\mensagem de banco de dados indisponível*|Grupo de usuários que receberá a notificação de problemas com o banco de dados do Zabbix.<br>A disponibilidade do Zabbix Server depende da disponibilidade do seu banco de dados. Não é possível que ele funcione sem um banco de dados. O **Database watchdog** é um processo especial do Zabbix Server que irá alertar os usuários em caso de desastre com o banco de dados. Se o banco de dados estiver "offline", o 'watchdog' irá enviar notificação para o grupo de usuários aqui definido usando todos os tipos de mídia configurada para os usuários. O Zabbix server não irá parar; ele continuará aguardando o retorno do banco de dados para continuar o processamento.<br>*Nota*: Antes da versão 1.8.2 o 'watchdog' era suportado apenas para MySQL. Desde a versão 1.8.2, o recurso é suportado para todos os bancos de dados.|
|*Registrar traps SNMP não correspondentes*|Registrar [traps SNMP](/pt/manual/config/items/itemtypes/snmptrap) que não encontrem interfaces SNMP correspondentes.|

[comment]: # ({/new-ef76d5b4})







[comment]: # ({new-72ef5238})
#### - Modules

This section allows to administer custom [frontend
modules](/manual/modules).

![](../../../../../assets/en/manual/web_interface/frontend_sections/administration/general_modules.png){width="600"}

Click on *Scan directory* to register/unregister any custom modules.
Registered modules will appear in the list, along with their details.
Unregistered modules will be removed from the list.

You may filter modules by name or status (enabled/disabled). Click on
the module status in the list to enable/disable a module. You may also
mass enable/disable modules by selecting them in the list and then
clicking on the *Enable/Disable* buttons below the list.

[comment]: # ({/new-72ef5238})

[comment]: # ({new-0e5d17fd})
#### - Connectors

This section allows to configure connectors for Zabbix data [streaming to external systems](/manual/config/export/streaming) over HTTP.

![](../../../../../assets/en/manual/web_interface/frontend_sections/administration/general_connectors.png){width="600"}

Click on *Create connector* to configure a new [connector](/manual/config/export/streaming#configuration).

You may filter connectors by name or status (enabled/disabled). Click on the connector status in the list to enable/disable a connector. 
You may also mass enable/disable connectors by selecting them in the list and then clicking on the *Enable/Disable* buttons below the list.

[comment]: # ({/new-0e5d17fd})


[comment]: # ({new-10e8eeaa})
#### - Other parameters

This section allows configuring miscellaneous other frontend parameters.

![](../../../../../assets/en/manual/web_interface/frontend_sections/administration/general_other.png)

|Parameter|Description|
|---------|-----------|
|*Frontend URL*|URL to Zabbix web interface. This parameter is used by Zabbix web service for communication with frontend and should be specified to enable scheduled reports.|
|*Group for discovered hosts*|Hosts discovered by [network discovery](/manual/discovery/network_discovery) and [agent autoregistration](/manual/discovery/auto_registration) will be automatically placed in the host group, selected here.|
|*Default host inventory mode*|Default [mode](/manual/config/hosts/inventory) for host inventory. It will be followed whenever a new host or host prototype is created by server or frontend unless overridden during host discovery/autoregistration by the *Set host inventory mode* operation.|
|*User group for database down message*|User group for sending alarm message or 'None'.<br>Zabbix server depends on the availability of the backend database. It cannot work without a database. If the database is down, selected users can be notified by Zabbix. Notifications will be sent to the user group set here using all configured user media entries. Zabbix server will not stop; it will wait until the database is back again to continue processing.<br>Notification consists of the following content:<br>`[MySQL\|PostgreSQL\|Oracle] database <DB Name> [on <DB Host>:<DB Port>] is not available: <error message depending on the type of DBMS (database)>`<br><DB Host> is not added to the message if it is defined as an empty value and <DB Port> is not added if it is the default value ("0"). The alert manager (a special Zabbix server process) tries to establish a new connection to the database every 10 seconds. If the database is still down the alert manager repeats sending alerts, but not more often than every 15 minutes.|
|*Log unmatched SNMP traps*|Log [SNMP trap](/manual/config/items/itemtypes/snmptrap) if no corresponding SNMP interfaces have been found.|

[comment]: # ({/new-10e8eeaa})

[comment]: # ({new-1ff15253})
##### Authorization

|Parameter|Description|
|---------|-----------|
|*Login attempts*|Number of unsuccessful login attempts before the possibility to log in gets blocked.|
|*Login blocking interval*|Period of time for which logging in will be prohibited when *Login attempts* limit is exceeded.|

[comment]: # ({/new-1ff15253})

[comment]: # ({new-99496c72})
##### Storage of secrets

*Vault provider* parameter allows selecting secret management software for storing [user macro](/manual/config/macros/user_macros#configuration) values. Supported options: 
- *HashiCorp Vault* (default)
- *CyberArk Vault*

See also: [Storage of secrets](/manual/config/secrets).

[comment]: # ({/new-99496c72})

[comment]: # ({new-2b1937dd})
##### Security

|Parameter|Description|
|---------|-----------|
|*Validate URI schemes*|Uncheck the box to disable URI scheme validation against the whitelist defined in *Valid URI schemes*. (enabled by default).|
|*Valid URI schemes*|A comma-separated list of allowed URI schemes. Applies to all fields in the frontend where URIs are used (for example, map element URLs).<br>this field is editable only if *Validate URI schemes* is selected.|
|*X-Frame-Options HTTP header*|Value of HTTP X-Frame-options header. Supported values:<br>**SAMEORIGIN** (default) - the page can only be displayed in a frame on the same origin as the page itself.<br>**DENY** - the page cannot be displayed in a frame, regardless of the site attempting to do so.<br>**null** - disable X-Frame-options header (not recommended).<br>Or a list (string) of comma-separated hostnames. If a listed hostname is not among allowed, the SAMEORIGIN option is used.|
|*Use iframe sandboxing*|This parameter determines whether retrieved URL content should be put into the sandbox or not. Note, that turning off sandboxing is not recommended.|
|*Iframe sandboxing exceptions*|If sandboxing is enabled and this field is empty, all sandbox attribute restrictions apply. To disable some of the restrictions, specified them in this field. This disables only restrictions listed here, other restrictions will still be applied. See [sandbox attribute](https://www.w3.org/TR/2010/WD-html5-20100624/the-iframe-element.html#attr-iframe-sandbox) description for additional information.|

[comment]: # ({/new-2b1937dd})

[comment]: # ({new-c8d6c320})
##### Communication with Zabbix server

|Parameter|Description|
|---------|-----------|
|*Network timeout*|How many seconds to wait before closing an idle socket (if a connection to Zabbix server has been established earlier, but frontend can not finish read/send data operation during this time, the connection will be dropped). Allowed range: 1 - 300s (default: 3s).|
|*Connection timeout*|How many seconds to wait before stopping an attempt to connect to Zabbix server. Allowed range: 1 - 30s (default: 3s).|
|*Network timeout for media type test*|How many seconds to wait for a response when testing a media type. Allowed range: 1 - 300s (default: 65s).|
|*Network timeout for script execution*|How many seconds to wait for a response when executing a script. Allowed range: 1 - 300s (default: 60s).|
|*Network timeout for item test*|How many seconds to wait for returned data when testing an item. Allowed range: 1 - 300s (default: 60s).|
|*Network timeout for scheduled report test*|How many seconds to wait for returned data when testing a scheduled report. Allowed range: 1 - 300s (default: 60s).|

[comment]: # ({/new-c8d6c320})
