[comment]: # translation:outdated

[comment]: # ({new-b226bc20})
# 8 Fila

[comment]: # ({/new-b226bc20})

[comment]: # ({new-f936bb89})
#### Visão geral

O módulo *Administração → Fila* permite a consulta à fila de coletas do
Zabbix.

Preferencialmente quando você abrir este módulo tudo deveria estar
"verde", mostrando que não existem itens cuja coleta está em fila. Isso
indicaria que todos os itens estão sendo coletados sem nenhum atraso.
Entretanto, por questões de performance do servidor, de conexão,
configuração ou com os agentes, alguns itens podem ter sua coleta
atrasada e é aqui que esta situação poderá ser verificada. Para mais
detalhes consulte o manual de [filas](/pt/manual/config/items/queue).

::: noteclassic
A fila só está disponível se o Zabbix Server estiver em
execução.
:::

A partir da caixa de seleção situada no canto direito da barra de título
você pode selecionar as visões da fila:

-   visão geral
-   visão geral por proxy
-   detalhes (lista de itens em atraso)

[comment]: # ({/new-f936bb89})

[comment]: # ({new-ccd11c9f})
##### Visão geral

É fácil identificar problemas de atraso em coletas relacionados a um
determinado tipo de item.

![](../../../../../assets/en/manual/web_interface/queue.png){width="600"}

Cada linha contêm um tipo de item e cada coluna contêm a quantidade de
itens em atraso - 5-10 segundos/10-30 segundos/30-60 segundos/1-5
minutos/5-10 minutos ou mais que 10 minutos.

[comment]: # ({/new-ccd11c9f})

[comment]: # ({new-d13acc32})
##### Visão geral por proxy

Nesta tela é apresentada o resumo de coletas por proxy (ou pelo próprio
Zabbix Server).

![](../../../../../assets/en/manual/web_interface/queue_proxy.png){width="600"}

Cada linha se refere a um proxy cadastrado, e a última linha ao Zabbix
Server e cada coluna contêm a quantidade de itens em atraso - 5-10
segundos/10-30 segundos/30-60 segundos/1-5 minutos/5-10 minutos ou mais
que 10 minutos..

[comment]: # ({/new-d13acc32})

[comment]: # ({new-dcd90684})
##### Detalhes

Nesta tela cada item em atraso é apresentado.

![](../../../../../assets/en/manual/web_interface/queue_details.png){width="600"}

Na coluna *Host* o nome dos hosts monitorados por proxy estarão
prefixados com o nome do respectivo proxy (desde o Zabbix 2.4.0).

|Coluna|Descrição|
|------|-----------|
|*Verificação agendada*|Horário estimado da próxima tentativa de coleta.|
|*Atrasado por*|Quanto tempo desde a última coleta.|
|*Host*|Nome do host.|
|*Nome*|Nome do item.|

[comment]: # ({/new-dcd90684})


[comment]: # ({new-1eab4d89})
##### Possible error messages

You may encounter a situation when no data is displayed and the
following error message appears:

![](../../../../../assets/en/manual/web_interface/error_message_1.png){width="600"}

Error message in this case is the following:

    Cannot display item queue. Permission denied

This happens when PHP configuration parameters $ZBX\_SERVER\_PORT or
$ZBX\_SERVER in zabbix.conf.php point to existing Zabbix server which
uses different database.

[comment]: # ({/new-1eab4d89})
