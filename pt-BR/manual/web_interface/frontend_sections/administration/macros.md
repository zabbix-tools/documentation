[comment]: # translation:outdated

[comment]: # ({new-33bc9cef})
# 5 Macros

[comment]: # ({/new-33bc9cef})

[comment]: # ({new-b195d98e})
#### Overview

This section allows to define system-wide [user
macros](/manual/config/macros/user_macros) as name-value pairs. Note
that macro values can be kept as plain text, secret text or Vault
secret. Adding a description is also supported.

![](../../../../../assets/en/manual/web_interface/user_macros_global.png){width="600"}

[comment]: # ({/new-b195d98e})
