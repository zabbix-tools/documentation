[comment]: # translation:outdated

[comment]: # ({new-000d2902})
# 2 Proxies

[comment]: # ({/new-000d2902})

[comment]: # ({new-6100a7eb})
#### Visão geral

O módulo *Administração → Proxies* permite que você mantenha as
configurações de [monitoração
distribuída](/pt/manual/distributed_monitoring).

[comment]: # ({/new-6100a7eb})

[comment]: # ({new-d8b90929})
#### Proxies

Ao acessar o módulo será apresentada uma lista com os proxies
existentes.

![](../../../../../assets/en/manual/web_interface/proxies.png){width="600"}

|Coluna|Descrição|
|------|-----------|
|*Nome*|Nome do proxy. Clicando no nome do proxy será exibido o [formulário de configuração](/pt/manual/distributed_monitoring/proxies#configuration).|
|*Modo*|Modo do proxy - *Ativo* ou *Passivo*.|
|*Criptografia*|Status de conexão do proxy:<br>**Nenhum** - sem criptografia<br>**PSK** - usando pre-shared key<br>**Certificado** - usando certificado<br>|
|*Visto pela última vez (tempo)*|O tempo passado desde a última vez que o proxy conseguiu se comunicar com o Zabbix Server.|
|*Contagem de hosts*|Quantidade de hosts monitorados através do proxy.|
|*Contagem de Itens*|Quantidade de itens monitorados através do proxy.|
|*Performance requerida (vps)*|Performance requerida do proxy (em novos valores por segundo).|
|*Hosts*|Todos os hosts monitorados pelo proxy são listados. Clicar no nome de um host abre o seu formulário de configuração.|

Para configurar um novo proxy, clique no botão *criar proxy* situado no
canto direito da barra de título.

[comment]: # ({/new-d8b90929})

[comment]: # ({new-1079c528})
##### Atualização em massa

Os botões ao final da lista fornecem opções de atualização em massa:

-   *Ativar hosts* - modifica o status de monitoração de todos os hosts
    monitorados pelos proxies selecionados para o estado *Monitorado*
-   *Desativar hosts* - modifica o status de monitoração de todos os
    hosts monitorados pelos proxies selecionados para o estado *Não
    monitorado*
-   *Excluir* - exclui os proxies selecionados

Para utilizar estas opções, selecione os proxies e clique no botão com a
função desejada.

[comment]: # ({/new-1079c528})


[comment]: # ({new-de1da12a})
##### Using filter

You can use the filter to display only the proxies you are interested
in. For better search performance, data is searched with macros
unresolved.

The *Filter* link is available above the list of proxies. If you click
on it, a filter becomes available where you can filter proxies by name
and mode.

![](../../../../../assets/en/manual/web_interface/proxies_filter1.png){width="600"}

[comment]: # ({/new-de1da12a})
