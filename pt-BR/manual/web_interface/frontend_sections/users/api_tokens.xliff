<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="pt-BR" datatype="plaintext" original="manual/web_interface/frontend_sections/users/api_tokens.md">
    <body>
      <trans-unit id="851ae152" xml:space="preserve">
        <source># 4 API tokens</source>
      </trans-unit>
      <trans-unit id="7d7a0a5c" xml:space="preserve">
        <source>#### Overview

This section allows to create and manage API tokens.

![](../../../../../assets/en/manual/web_interface/api_tokens.png){width="600"}

You may filter API tokens by name, users to whom the tokens are
assigned, expiry date, users that created tokens, or status
(enabled/disabled). Click on the token status in the list to quickly
enable/disable a token. You may also mass enable/disable tokens by
selecting them in the list and then clicking on the *Enable/Disable*
buttons below the list.

To create a new token, press *Create API token* button at the top right
corner, then fill out the required fields in the token configuration
screen:

![](../../../../../assets/en/manual/web_interface/frontend_sections/administration/general_token_conf.png)

|Parameter|Description|
|--|--------|
|Name|Token's visible name.|
|User|User the token should be assigned to. To quickly select a user, start typing the username, first or last name, then select the required user from the auto-complete list. Alternatively, you can press the Select button and select a user from the full user list. A token can be assigned only to one user.|
|Description|Optional token description.|
|Set expiration date and time|Unmark this checkbox if a token should not have an expiry date.|
|Expiry date|Click on the calendar icon to select token expiry date or enter the date manually in a format YYYY-MM-DD hh:mm:ss|
|Enabled|Unmark this checkbox if you need to create a token in a disabled state.|

Press Add to create a token. On the next screen, copy and save in a safe
place *Auth token* value **before closing the page**, then press Close.
The token will appear in the list.

::: notewarning
 *Auth token* value cannot be viewed again later. It
is only available immediately after creating a token. If you lose a
saved token you will have to regenerate it and doing so will create a
new authorization string. 
:::

Click on the token name to edit the name, description, expiry date
settings, or token status. Note, that it is not possible to change to
which user the token is assigned. Press Update button to save changes.
If a token has been lost or exposed, you may press Regenerate button to
generate new token value. A confirmation dialog box will appear, asking
you to confirm this operation since after proceeding the previously
generated token will become invalid.

Users without access to the *Administration* menu section can see and
modify details of tokens assigned to them in the *User profile → API
tokens* [section](/manual/web_interface/user_profile#api_tokens) only if
*Manage API tokens* is allowed in their [user
role](/manual/web_interface/frontend_sections/users/user_roles)
permissions.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
