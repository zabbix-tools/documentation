<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="pt-BR" datatype="plaintext" original="manual/web_interface/frontend_sections/alerts/mediatypes.md">
    <body>
      <trans-unit id="2c958cc2" xml:space="preserve">
        <source># 2 Media types</source>
      </trans-unit>
      <trans-unit id="80d5564e" xml:space="preserve">
        <source>#### Overview

In the *Alerts → Media types* section users can configure and
maintain media type information.

Media type information contains general instructions for using a medium
as delivery channel for notifications. Specific details, such as the
individual email addresses to send a notification to are kept with
individual users.

A listing of existing media types with their details is displayed.

![](../../../../../assets/en/manual/web_interface/media_types.png){width="600"}

Displayed data:

|Column|Description|
|--|--------|
|*Name*|Name of the media type. Clicking on the name opens the media type [configuration form](/manual/config/notifications/media/email#configuration).|
|*Type*|Type of the media (email, SMS, etc) is displayed.|
|*Status*|Media type status is displayed - *Enabled* or *Disabled*.&lt;br&gt;By clicking on the status you can change it.|
|*Used in actions*|All actions where the media type is used directly (selected in the *Send only to* dropdown) are displayed. Clicking on the action name opens the action configuration form.|
|*Details*|Detailed information of the media type is displayed.|
|*Actions*|The following action is available:&lt;br&gt;**Test** - click to open a testing form where you can enter media type parameters (e.g. a recipient address with test subject and body) and send a test message to verify that the configured media type works. See also: Media type testing for [Email](/manual/config/notifications/media/email#media_type_testing), [Webhook](/manual/config/notifications/media/webhook#media_type_testing), or [Script](/manual/config/notifications/media/script#media_type_testing).|

To configure a new media type, click on the *Create media type* button
in the top right-hand corner.

To import a media type from XML, click on the *Import* button in the top
right-hand corner.</source>
      </trans-unit>
      <trans-unit id="638713d5" xml:space="preserve">
        <source>##### Mass editing options

Buttons below the list offer some mass-editing options:

-   *Enable* - change the media type status to *Enabled*
-   *Disable* - change the media type status to *Disabled*
-   *Export* - export the media types to a YAML, XML or JSON file
-   *Delete* - delete the media types

To use these options, mark the checkboxes before the respective media
types, then click on the required button.</source>
      </trans-unit>
      <trans-unit id="d07f9485" xml:space="preserve">
        <source>##### Using filter

You can use the filter to display only the media types you are
interested in. For better search performance, data is searched with
macros unresolved.

The *Filter* link is available above the list of media types. If you
click on it, a filter becomes available where you can filter media types
by name and status.

![](../../../../../assets/en/manual/web_interface/media_types_filter1.png){width="600"}</source>
      </trans-unit>
    </body>
  </file>
</xliff>
