[comment]: # translation:outdated

[comment]: # ({new-9ada3186})
# 3 Relatórios

[comment]: # ({/new-9ada3186})

[comment]: # ({new-8b50039d})
#### Visão geral

O menu relatórios agrupa as funções de relatórios pré-definidos e
customizáveis pelo usuário com foco em apresentar visões gerais os
parâmetros e status do Zabbix, triggers e dados coletados.

[comment]: # ({/new-8b50039d})
