[comment]: # translation:outdated

[comment]: # ({new-1c52788a})
# 4 Dados recentes

[comment]: # ({/new-1c52788a})

[comment]: # ({new-a86c547b})
#### Visão geral

O módulo *Monitoramento → Dados recentes* pode ser utilizado para
consultar os últimos dados adquiridos pela monitoração, assim como
acessar gráficos sobre os itens.

Quando você abre esta página pela primeira vez nenhum dado é
apresentado.

![](../../../../../assets/en/manual/web_interface/latest_data_default.png)

Para visualizar os dados, por questões de performance, é necessário que
você indique algum filtro. Para isso clique no link *Filtrar*, indique o
filtro (por exemplo um grupo de host) e clique no botão *Filtrar*.

![](../../../../../assets/en/manual/web_interface/latest_data.png){width="600"}

Na lista apresentada clique na imagem ">" exibida antes do nome do
host para que o detalhamento dos dados da aplicação (itens) seja
apresentado.

Os itens são apresentados com o seu nome, momento de última verificação,
último valor, diferença entre o último e o atual (modificação) e um link
para gráfico ou histórico dos valores do item.

*Nota*: O nome de um host inativo é apresentado em vermelho (tanto na
lista de hosts quanto na caixa de seleção). A apresentação de dados de
hosts inativos é suportada desde o Zabbix 2.2.0.

Apenas dados obtidos nas últimas 24 horas são apresentados por padrão,
este limite foi introduzido para evitar problemas de performance ao
buscar dados muito antigos. É possível modificar esta limitação ao
modificar a [constante](/pt/manual/web_interface/definitions)
ZBX\_HISTORY\_PERIOD no arquivo *include/defines.inc.php*.

**Detalhes sobre o uso do filtro**

Você pode utilizar o filtro para apresentar somente os itens que você
está interessado. O link *Filtrar* está situado na parte superior da
tabela (centralizado). Você pode utiliza-lo para filtrar os itens por
grupo de hosts, hosts, aplicações, nome do item; Você também pode marcar
para exibir itens que não tenham dados coletados.

Adicionalmente, você pode marcar a opção *Mostrar detalhes* para exibir
informação expandida sobre os itens. Detalhes como: intervalo entre
coletas, período de retenção de histórico e de médias, tipo do item e
erros (normal/não suportado). Um link para a configuração do item também
estará disponível.

![](../../../../../assets/en/manual/web_interface/latest_data2.png){width="600"}

Por padrão itens sem dados serão exibidos mas seus detalhes não.

**Gráficos para comparar itens**

Você pode marcar as opções disponíveis na segunda coluna para selecionar
diversos itens e comparar seus dados através de gráficos simples ou
através de pilha de gráficos. Para fazer isso selecione os itens de
interesse e clique no botão *Exibir pilha de gráficos* ou *Exibir
gráfico* situados ao final da lista.

**Links para o histórico/gráfico simples**

A última coluna da lista de dados provê:

-   um link para o **Histórico** (para todos os itens do tipo texto) -
    apontando para uma listagem com os *Valores/500 últimos valores*) do
    item selecionado.

```{=html}
<!-- -->
```
-   um link para um **Gráfico simples** (para todos os itens do tipo
    numérico) - apontando para um [gráfico
    simples](/pt/manual/config/visualisation/graphs/simple). Uma vez que
    o gráfico seja exibido estará disponível na barra de título no canto
    direito uma caixa de seleção com possibilidade de exibir os
    *Valores/500 últimos valores* também.

![](../../../../../assets/en/manual/web_interface/values.png){width="600"}

Os valores apresentados nesta lista são "brutos", nenhuma formatação é
aplicada.

::: noteclassic
O total de valores apresentado é modificável através da
barra de filtro, veja mais detalhes em [Administração →
Geral](/pt/manual/web_interface/frontend_sections/administration/general).
:::

[comment]: # ({/new-a86c547b})




[comment]: # ({new-84099469})
##### Buttons

View mode buttons being common for all sections are described on the
[Monitoring](/manual/web_interface/frontend_sections/monitoring#view_mode_buttons)
page.

[comment]: # ({/new-84099469})

[comment]: # ({new-dd13c026})
##### Mass actions

Buttons below the list offer mass actions with one or several selected items:

-   *Display stacked graph* - display a stacked [ad-hoc graph](/manual/config/visualization/graphs/adhoc)
-   *Display graph* - display a simple [ad-hoc graph](/manual/config/visualization/graphs/adhoc)
-   *Execute now* - execute a check for new item values immediately.
    Supported for **passive** checks only (see [more
    details](/manual/config/items/check_now)). This option is available only for hosts with read-write 
    access. Accessing this option for hosts with read-only permissions depends on the 
    [user role](/manual/web_interface/frontend_sections/administration/user_roles) option called 
    *Invoke "Execute now" on read-only hosts*.

To use these options, mark the checkboxes before the respective items,
then click on the required button.

[comment]: # ({/new-dd13c026})

[comment]: # ({new-b6e60197})
##### Using filter

You can use the filter to display only the items you are interested in.
For better search performance, data is searched with macros unresolved.

The *Filter* link is located above the table to the right. You can use
it to filter items by host group, host, a string in the item name and
tags; you can also select to display items that have no data gathered.

Specifying a parent host group implicitly selects all nested host
groups.

*Show details* allows extending displayable information on the items.
Such details as refresh interval, history and trends settings, item
type, and item errors (fine/unsupported) are displayed. A link to item
configuration is also available.

![](../../../../../assets/en/manual/web_interface/latest_data2.png){width="600"}

By default, items without data are shown but details are not displayed.
For better page performance, the *Show items without data* option is
checked and disabled if no host is selected in the filter.

**Ad-hoc graphs for comparing items**

You may use the checkbox in the first column to select several items and
then compare their data in a simple or stacked [ad-hoc
graph](/manual/config/visualization/graphs/adhoc). To do that, select
items of interest, then click on the required graph button below the
table.

**Links to value history/simple graph**

The last column in the latest value list offers:

-   a **History** link (for all textual items) - leading to listings
    (*Values/500 latest values*) displaying the history of previous item
    values.

```{=html}
<!-- -->
```
-   a **Graph** link (for all numeric items) - leading to a [simple
    graph](/manual/config/visualization/graphs/simple). However, once
    the graph is displayed, a dropdown on the upper right offers a
    possibility to switch to *Values/500 latest values* as well.

![](../../../../../assets/en/manual/web_interface/latest_values.png){width="600"}

The values displayed in this list are "raw", that is, no postprocessing
is applied.

::: noteclassic
The total amount of values displayed is defined by the value
of *Limit for search and filter results* parameter, set in
[Administration →
General](/manual/web_interface/frontend_sections/administration/general).
:::

[comment]: # ({/new-b6e60197})
