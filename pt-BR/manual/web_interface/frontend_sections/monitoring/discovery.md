[comment]: # translation:outdated

[comment]: # ({new-a637692c})
# 10 Descoberta

[comment]: # ({/new-a637692c})

[comment]: # ({new-6877d127})
#### Visão geral

Através do módulo *Monitoramento → Descoberta* é possível visualizar o
resultado das
[regras de descoberta de rede](/pt/manual/discovery/regras de descoberta de rede).
Os dispositivos descobertos são organizados por regra de descoberta.

![](../../../../../assets/en/manual/web_interface/discovery_status.png){width="600"}

Se um dispositivo descoberto estiver sendo monitorado, o nome de host
será apresentado na coluna *Host monitorado*, e o período em que o
dispositivo está em estado "descoberto" ou "perdido" será apresentado na
coluna *Uptime/Downtime*.

Após isso serão apresentadas colunas mostrando o estado de serviços
individuais para cada dispositivo descoberto. Uma dica em cada célula
irá apresentar *Uptime/Downtime* de cada serviço.

::: noteimportant
Apenas aqueles serviços que já foram encontrados
ao menos uma vez terão uma coluna apresentando seu estado.
:::

[comment]: # ({/new-6877d127})



[comment]: # ({new-84099469})
##### Buttons

View mode buttons being common for all sections are described on the
[Monitoring](/manual/web_interface/frontend_sections/monitoring#view_mode_buttons)
page.

[comment]: # ({/new-84099469})

[comment]: # ({new-8b46b0e3})
##### Using filter

You can use the filter to display only the discovery rules you are
interested in. For better search performance, data is searched with
macros unresolved.

With nothing selected in the filter, all enabled discovery rules are
displayed. To select a specific discovery rule for display, start typing
its name in the filter. All matching enabled discovery rules will be
listed for selection. More than one discovery rule can be selected.

[comment]: # ({/new-8b46b0e3})
