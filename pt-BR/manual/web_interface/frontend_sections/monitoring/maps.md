[comment]: # translation:outdated

[comment]: # ({new-7b37d2bb})
# 9 Mapas

[comment]: # ({/new-7b37d2bb})

[comment]: # ({new-20a11f83})
#### Visão geral

Através do módulo *Monitoramento → Mapas* você pode manter e visualizar
os [mapas de rede](/pt/manual/config/visualisation/maps).

Este módulo tem a característica de lembrar qual foi o último mapa que
você acessou (ou a listagem). Então quando você acessa este módulo ele
irá se configurar com o último mapa ou listagem. A partir do Zabbix 3.0
os mapas podem ser públicos ou privados, mapas públicos estão
disponíveis para todos os usuários, enquanto os privados somente para
seus proprietários e para os usuários com os quais eles foram
compartilhados.

[comment]: # ({/new-20a11f83})

[comment]: # ({new-c1cb6946})
#### Listagem dos mapas

![](../../../../../assets/en/manual/web_interface/map_list.png){width="600"}

São apresentadas as seguintes colunas:

|Coluna|Descrição|
|------|-----------|
|*Nome*|Nome único do mapa. Ao clicar no nome do mapa você é redirecionado para a [visualização](/pt/manual/web_interface/frontend_sections/monitoring/maps#viewing_maps) do mapa.|
|*Largura*|Largura do mapa.|
|*Altura*|Altura do mapa.|
|*Ações*|Estão disponíveis duas ações:<br>**Propriedades** - edita as características [gerais](/pt/manual/config/visualisation/maps/map#creating_a_map) (externas) do mapa<br>**Construtor** - acessa o modo de edição interna dos [elementos do mapa](/pt/manual/config/visualisation/maps/map#adding_elements)|

Para
[configurar](/pt/manual/config/visualisation/maps/map#creating_a_map) um
novo mapa, clique no botão *Criar mapa* na barra de título. Para
importar um mapa a partir de um arquivo XML, clique no botão *Importar*
situado ao lado.

Na parte inferior da lista de mapas existem dois botões para ações em
massa:

-   *Exportar* - exporta os mapas selecionados para um arquivo XML
-   *Excluir* - exclui os mapas selecionados

Para usar estas opções, marque os mapas desejados e clique no botão
apropriado.

[comment]: # ({/new-c1cb6946})

[comment]: # ({new-1d312fad})
#### Visualizando os mapas

Para visualizar um mapa, clique em seu nome na lista de mapas.

![](../../../../../assets/en/manual/web_interface/maps.png){width="600"}

Você também pode utilizar a caixa de seleção situada na barra de título
para selecionar o menor nível de severidade que deverá ser apresentado
nos incidentes visualizáveis pelo mapa. A severidade marcada como
*padrão* no mapa será a que foi definida nas características gerais do
mesmo. Se o mapa contiver um submapa, a navegação pelo submapa
respeitará o maior nível de severidade selecionado.

[comment]: # ({/new-1d312fad})

[comment]: # ({new-a9b8dc1b})
##### Realce de ícones

Se um elemento do mapa estiver em estado de incidente, ele terá um
círculo atrás de sí. A cor deste círculo dependerá do maior nível de
severidade de incidente atualmente ativo no host. Apenas incidentes com
nível de severidade igual ou superior ao definido para a visualização
serão apresentados. Se todos os incidentes para o elemento tiverem sido
reconhecidos, o círculo terá uma borda verde.

Adicionalmente um host em [manutenção](/pt/manual/maintenance) será
destacado com um uma área quadrada na cor laranja, e hosts com
monitoração inativa (não monitorados) serão destacados com um quadrado
cinza. O destaque de ícones será apresentado se a opção *Realce de
ícones* tiver sido marcada durante a [configuração do
mapa](/pt/manual/config/visualisation/maps/map#creating_a_map).

[comment]: # ({/new-a9b8dc1b})

[comment]: # ({new-f6436c14})
##### Indicadores de mudança recente

Triângulos vermelhos ao redor de um elemento indicam que este teve
alteração recente de status em alguma trigger, nos últimos 30 minutos.
Estes triângulos são apresentados se a opção *Destacar elementos com
modificação de status em trigger* tiver sido marcada durante a
[configuração do
mapa](/pt/manual/config/visualisation/maps/map#creating_a_map).

[comment]: # ({/new-f6436c14})

[comment]: # ({new-89ad8a4d})
##### Links

Clicar em elementos dos mapas pode abrir menus flutuantes com links.

[comment]: # ({/new-89ad8a4d})

[comment]: # ({new-0e0ddb36})
#### Controles

Quando no modo de visualização, na barra de título dos mapas existem
dois botões de controle:

-   ![](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/add_to_fav.png) -
    adiciona o mapa ao widget de favoritos no [Dashboard](dashboard)
-   ![](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/full_window.png) -
    exibe o mapa em modo de tela cheia

[comment]: # ({/new-0e0ddb36})




[comment]: # ({new-8bceddc5})
##### Buttons

Buttons to the right offer the following options:

|   |   |
|---|---|
|![](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/edit_map.png)|Go to map constructor to edit the map content.|
|![](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/add_to_fav1.png)|Add map to the favorites widget in the [Dashboard](dashboard).|
|![](../../../../../assets/en/manual/web_interface/button_add_fav2.png)|The map is in the favorites widget in the [Dashboard](dashboard). Click to remove map from the favorites widget.|

View mode buttons being common for all sections are described on the
[Monitoring](/manual/web_interface/frontend_sections/monitoring#view_mode_buttons)
page.

[comment]: # ({/new-8bceddc5})

[comment]: # ({new-15da840f})
##### Readable summary in maps

A hidden "aria-label" property is available allowing map information to
be read with a screen reader. Both general map description and
individual element description is available, in the following format:

-   for map description:
    `<Map name>, <* of * items in problem state>, <* problems in total>.`
-   for describing one element with one problem:
    `<Element type>, Status <Element status>, <Element name>, <Problem description>.`
-   for describing one element with multiple problems:
    `<Element type>, Status <Element status>, <Element name>, <* problems>.`
-   for describing one element without problems:
    `<Element type>, Status <Element status>, <Element name>.`

For example, this description is available:

    'Local network, 1 of 6 elements in problem state, 1 problem in total. Host, Status problem, My host, Free disk space is less than 20% on volume \/. Host group, Status ok, Virtual servers. Host, Status ok, Server 1. Host, Status ok, Server 2. Host, Status ok, Server 3. Host, Status ok, Server 4. '

for the following map:

![](../../../../../assets/en/manual/web_interface/map_aria_label.png){width="600"}

[comment]: # ({/new-15da840f})

[comment]: # ({new-c4642e16})
##### Referencing a network map

Network maps can be referenced by both `sysmapid` and `mapname` GET
parameters. For example,

    http://zabbix/zabbix/zabbix.php?action=map.view&mapname=Local%20network

will open the map with that name (Local network).

If both `sysmapid` (map ID) and `mapname` (map name) are specified,
`mapname` has higher priority.

[comment]: # ({/new-c4642e16})
