<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="pt-BR" datatype="plaintext" original="manual/web_interface/frontend_sections/monitoring/problems.md">
    <body>
      <trans-unit id="621a60b9" xml:space="preserve">
        <source># 1 Problems</source>
      </trans-unit>
      <trans-unit id="d98c719a" xml:space="preserve">
        <source>#### Overview

In *Monitoring → Problems* you can see what problems you currently have.
Problems are those triggers that are in the "Problem" state.

By default all new problems are classified as cause problems. It is possible to manually reclassify certain problems as symptom problem of the cause problem. For more details, see [cause and symptom events](/manual/web_interface/frontend_sections/monitoring/problems/cause_and_symptom).

![](../../../../../assets/en/manual/web_interface/problems.png){width="600"}

|Column|Description|
|--|--------|
|*Checkbox*|Checkboxes for problem selection are displayed.&lt;br&gt;Icons, next to the checkboxes, have the following meaning:&lt;br&gt;![icon\_number.png](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/icon_number.png) - the number of symptom events for the cause problem;&lt;br&gt;![icon\_expand.png](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/icon_expand.png) - expand to show symptom events;&lt;br&gt;![icon\_collapse.png](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/icon_collapse.png) - collapse to hide symptom events;&lt;br&gt;![icon\_symptom.png](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/icon_symptom.png) - this is a symptom event.|
|*Time*|Problem start time is displayed.|
|*Severity*|Problem severity is displayed.&lt;br&gt;Problem severity is originally based on the severity of the underlying problem trigger, however, after the event has happened it can be updated using the *Update problem* [screen](/manual/acknowledgment#updating_problems). Color of the problem severity is used as cell background during problem time.|
|*Recovery time*|Problem resolution time is displayed.|
|*Status*|Problem status is displayed:&lt;br&gt;**Problem** - unresolved problem&lt;br&gt;**Resolved** - recently resolved problem. You can hide recently resolved problems using the filter.&lt;br&gt;New and recently resolved problems blink for 2 minutes. Resolved problems are displayed for 5 minutes in total. Both of these values are configurable in *Administration* → *General* → *[Trigger displaying options](/manual/web_interface/frontend_sections/administration/general#trigger_displaying_options)*.|
|*Info*|A green information icon is displayed if a problem is closed by global correlation or manually when updating the problem. Rolling a mouse over the icon will display more details:&lt;br&gt;![info.png](../../../../../assets/en/manual/web_interface/info.png)&lt;br&gt;The following icon is displayed if a suppressed problem is being shown (see *Show suppressed problems* option in the filter). Rolling a mouse over the icon will display more details:&lt;br&gt;![](../../../../../assets/en/manual/web_interface/info_suppressed2.png)|
|*Host*|Problem host is displayed. &lt;br&gt;Clicking on the host name brings up the [host menu](/manual/web_interface/menu/host_menu).|
|*Problem*|Problem name is displayed.&lt;br&gt;Problem name is based on the name of the underlying problem trigger.&lt;br&gt;Macros in the trigger name are resolved at the time of the problem happening and the resolved values do not update any more.&lt;br&gt;*Note* that it is possible to append the problem name with [operational data](#operational_data_of_problems) showing some latest item values.&lt;br&gt;Clicking on the problem name brings up the [event menu](/manual/web_interface/menu/event_menu).&lt;br&gt;Hovering on the ![](../../../../../assets/en/manual/web_interface/item_description_icon.png) icon after the problem name will bring up the trigger description (for those problems that have it).|
|*Operational data*|[Operational data](#operational_data_of_problems) are displayed containing latest item values.&lt;br&gt;Operational data can be a combination of text and item value macros if configured on a trigger level. If no operational data is configured on a trigger level, the latest values of all items from the expression are displayed.&lt;br&gt;This column is only displayed if *Separately* is selected for *Show operational data* in the filter.|
|*Duration*|Problem duration is displayed.&lt;br&gt;See also: [Negative problem duration](#negative_problem_duration)|
|*Update*|Click on the *Update* link to go to the [problem update](/manual/acknowledgment#updating_problems) screen where various actions can be taken on the problem, including commenting and acknowledging the problem.|
|*Actions*|History of activities about the problem is displayed using symbolic icons:&lt;br&gt;![icon\_acknowledged\_green.png](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/icon_acknowledged_green.png) - problem has been acknowledged. This icon is always displayed first.&lt;br&gt;![icon\_comment.png](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/icon_comment.png) - comments have been made. The number of comments is also displayed.&lt;br&gt;![icon\_sev\_up1.png](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/icon_sev_up1.png) - problem severity has been increased (e.g. Information → Warning)&lt;br&gt;![icon\_sev\_down1.png](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/icon_sev_down1.png) - problem severity has been decreased (e.g. Warning → Information)&lt;br&gt;![icon\_severity\_back.png](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/icon_severity_back.png) - problem severity has been changed, but returned to the original level (e.g. Warning → Information → Warning)&lt;br&gt;![icon\_actions.png](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/icon_actions.png) - actions have been taken. The number of actions is also displayed.&lt;br&gt;![icon\_actions\_progress1.png](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/icon_actions_progress1.png) - actions have been taken, at least one is in progress. The number of actions is also displayed.&lt;br&gt;![icon\_actions\_failed.png](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/icon_actions_failed.png) - actions have been taken, at least one has failed. The number of actions is also displayed.&lt;br&gt;When rolling the mouse over the icons, popups with details about the activity are displayed. See [viewing details](#Viewing_details) to learn more about icons used in the popup for actions taken.|
|*Tags*|[Tags](/manual/config/tagging) are displayed (if any).&lt;br&gt;In addition, tags from an external ticketing system may also be displayed (see the *Process tags* option when configuring [webhooks](/manual/config/notifications/media/webhook)).|</source>
      </trans-unit>
      <trans-unit id="8fa0f11a" xml:space="preserve">
        <source>#### Operational data of problems

It is possible to display operational data for current problems, i.e.
the latest item values as opposed to the item values at the time of the
problem.

Operational data display can be configured in the filter of *Monitoring*
→ *Problems* or in the configuration of the respective [dashboard
widget](/manual/web_interface/frontend_sections/dashboards/widgets/problems),
by selecting one of the three options:

-   *None* - no operational data is displayed
-   *Separately* - operational data is displayed in a separate column

![](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/problem_live_data_b0.png){width="600"}

-   *With problem name* - operational data is appended to the problem
    name and in parentheses. Operational data are appended to the
    problem name only if the *Operational data* field is non-empty in
    the trigger configuration.

![](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/problem_live_data_a0.png){width="600"}

The content of operational data can be configured with each
[trigger](/manual/config/triggers/trigger), in the *Operational data*
field. This field accepts an arbitrary string with macros, most
importantly, the `{ITEM.LASTVALUE&lt;1-9&gt;}` macro.

`{ITEM.LASTVALUE&lt;1-9&gt;}` in this field will always resolve to the latest
values of items in the trigger expression. `{ITEM.VALUE&lt;1-9&gt;}` in this
field will resolve to the item values at the moment of trigger status
change (i.e. change into problem, change into OK, being closed manually
by a user or being closed by correlation).</source>
      </trans-unit>
      <trans-unit id="1848ebdc" xml:space="preserve">
        <source>#### Negative problem duration

It is actually possible in some common situations to have negative
problem duration i.e. when the problem resolution time is earlier than
problem creation time, e. g.:

-   If some host is monitored by proxy and a network error happens,
    leading to no data received from the proxy for a while, the
    nodata(/host/key) trigger will be fired by the server. When the
    connection is restored, the server will receive item data from the
    proxy having a time from the past. Then, the nodata(/host/key)
    problem will be resolved and it will have a negative problem
    duration;
-   When item data that resolve the problem event are sent by Zabbix
    sender and contain a timestamp earlier than the problem creation
    time, a negative problem duration will also be displayed.

::: noteclassic
Negative problem duration is not affecting [SLA
calculation](/manual/it_services) or [Availability
report](/manual/web_interface/frontend_sections/reports/availability) of
a particular trigger in any way; it neither reduces nor expands problem
time.
:::</source>
      </trans-unit>
      <trans-unit id="462c4f50" xml:space="preserve">
        <source>##### Mass editing options

Buttons below the list offer some mass-editing options:

-   *Mass update* - update the selected problems by navigating to the
    [problem update](/manual/acknowledgment#updating_problems) screen

To use this option, mark the checkboxes before the respective problems,
then click on the *Mass update* button.</source>
      </trans-unit>
      <trans-unit id="eaadfbbc" xml:space="preserve">
        <source>##### Buttons

The button to the right offers the following option:

|   |   |
|--|--------|
|![](../../../../../assets/en/manual/web_interface/button_csv.png)|Export content from all pages to a CSV file.|

View mode buttons, being common for all sections, are described on the
[Monitoring](/manual/web_interface/frontend_sections/monitoring#view_mode_buttons)
page.</source>
      </trans-unit>
      <trans-unit id="5dda91fc" xml:space="preserve">
        <source>#### Using filter

You can use the filter to display only the problems you are interested
in. For better search performance, data is searched with macros
unresolved.

The filter is located above the table. Favorite filter settings can be
saved as tabs and then quickly accessed by clicking on the [tabs above
the
filter](/manual/web_interface/frontend_sections/monitoring/problems#tabs_for_favorite_filters).

![](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/problem_filter.png){width="600"}

|Parameter|Description|
|--|--------|
|*Show*|Filter by problem status:&lt;br&gt;**Recent problems** - unresolved and recently resolved problems are displayed (default)&lt;br&gt;**Problems** - unresolved problems are displayed&lt;br&gt;**History** - history of all events is displayed|
|*Host groups*|Filter by one or more host groups.&lt;br&gt;Specifying a parent host group implicitly selects all nested host groups.|
|*Hosts*|Filter by one or more hosts.|
|*Triggers*|Filter by one or more triggers.|
|*Problem*|Filter by problem name.|
|*Severity*|Filter by trigger (problem) severity.|
|*Age less than*|Filter by how old the problem is.|
|*Show symptoms*|Mark the checkbox to display in its own line problems classified as symptoms.|
|*Show suppressed problems*|Mark the checkbox to display problems that would otherwise be suppressed (not shown) because of host maintenance or single [problem suppression](/manual/acknowledgment/suppression).|
|*Acknowledgement status*|Filter to display all problems, unacknowledged problems only, or acknowledged problems only. Mark the additional checkbox to filter out those problems ever acknowledged by you.|
|*Host inventory*|Filter by inventory type and value.|
|*Tags*|Filter by [event tag](/manual/config/tagging) name and value. It is possible to include as well as exclude specific tags and tag values. Several conditions can be set. Tag name matching is always case-sensitive.&lt;br&gt;There are several operators available for each condition:&lt;br&gt;**Exists** - include the specified tag names&lt;br&gt;**Equals** - include the specified tag names and values (case-sensitive)&lt;br&gt;**Contains** - include the specified tag names where the tag values contain the entered string (substring match, case-insensitive)&lt;br&gt;**Does not exist** - exclude the specified tag names&lt;br&gt;**Does not equal** - exclude the specified tag names and values (case-sensitive)&lt;br&gt;**Does not contain** - exclude the specified tag names where the tag values contain the entered string (substring match, case-insensitive)&lt;br&gt;There are two calculation types for conditions:&lt;br&gt;**And/Or** - all conditions must be met, conditions having the same tag name will be grouped by the Or condition&lt;br&gt;**Or** - enough if one condition is met&lt;br&gt;When filtered, the tags specified here will be displayed first with the problem, unless overridden by the *Tag display priority* (see below) list.|
|*Show tags*|Select the number of displayed tags:&lt;br&gt;**None** - no *Tags* column in *Monitoring → Problems*&lt;br&gt;**1** - *Tags* column contains one tag&lt;br&gt;**2** - *Tags* column contains two tags&lt;br&gt;**3** - *Tags* column contains three tags&lt;br&gt;To see all tags for the problem roll your mouse over the three dots icon.|
|*Tag name*|Select tag name display mode:&lt;br&gt;**Full** - tag names and values are displayed in full&lt;br&gt;**Shortened** - tag names are shortened to 3 symbols; tag values are displayed in full&lt;br&gt;**None** - only tag values are displayed; no names|
|*Tag display priority*|Enter tag display priority for a problem, as a comma-separated list of tags (for example: `Services,Applications,Application`). Tag names only should be used, no values. The tags of this list will always be displayed first, overriding the natural ordering by alphabet.|
|*Show operational data*|Select the mode for displaying [operational data](#operational_data_of_problems):&lt;br&gt;**None** - no operational data is displayed&lt;br&gt;**Separately** - operational data is displayed in a separate column&lt;br&gt;**With problem name** - append operational data to the problem name, using parentheses for the operational data|
|*Compact view*|Mark the checkbox to enable compact view.|
|*Show details*|Mark the checkbox to display underlying trigger expressions of the problems. Disabled if *Compact view* checkbox is marked.|
|*Show timeline*|Mark the checkbox to display the visual timeline and grouping. Disabled if *Compact view* checkbox is marked.|
|*Highlight whole row*|Mark the checkbox to highlight the full line for unresolved problems. The problem severity color is used for highlighting.&lt;br&gt;Enabled only if the *Compact view* checkbox is marked in the standard blue and dark themes. *Highlight whole row* is not available in the high-contrast themes.|</source>
      </trans-unit>
      <trans-unit id="6c375926" xml:space="preserve">
        <source>##### Tabs for favorite filters

Frequently used sets of filter parameters can be saved in tabs.

To save a new set of filter parameters, open the main tab, and configure
the filter settings, then press the *Save as* button. In a new popup
window, define *Filter properties*.

![problem\_filter0.png](../../../../../assets/en/manual/web_interface/filter_properties.png)

|Parameter|Description|
|--|--------|
|*Name*|The name of the filter to display in the tab list.|
|*Show number of records*|Check, if you want the number of problems to be displayed next to the tab name.|
|*Set custom time period*|Check to set specific default time period for this filter set. If set, you will only be able to change the time period for this tab by updating filter settings. For tabs without a custom time period, the time range can be changed by pressing the time selector button in the top right corner (button name depends on selected time interval: This week, Last 30 minutes, Yesterday, etc.).&lt;br&gt;This option is available only for filters in *Monitoring→Problems*.|
|*From/To*|[Time period](/manual/config/visualization/graphs/simple#time_period_selector) start and end in absolute (`Y-m-d H:i:s`) or relative time syntax (`now-1d`). Available, if *Set custom time period* is checked.|

When saved, the filter is created as a named filter tab and immediately activated.

To edit the filter properties of an existing filter, press the gear symbol
next to the active tab name.

![problem\_filter2.png](../../../../../assets/en/manual/web_interface/filter_properties_link.png)

Notes:

-   To hide the filter area, click on the name of the current tab. Click on the
    active tab name again to open the filter area again.
-   Keyboard navigation is supported: use arrows to switch between tabs,
    press *Enter* to open.
-   The left/right buttons above the filter may be used to switch between
    saved filters. Alternatively, the downward pointing button opens a drop-down 
    menu with all saved filters and you can click on the one you need.
-   Filter tabs can be re-arranged by dragging and dropping.
-   If the settings of a saved filter have been changed (but not saved), a green
    dot is displayed after the filter name. To update the filter according to the new
    settings, click on the *Update* button, which is displayed instead of the
    *Save as* button.
-   Current filter settings are remembered in the user profile. When the user
    opens the page again, the filter settings will have stayed the same.

::: noteclassic
To share filters, copy and send to others a URL of an
active filter. After opening this URL, other users will be able to save
this set of parameters as a permanent filter in their Zabbix account.\
See also: [Page
parameters](/manual/web_interface/page_parameters).
:::</source>
      </trans-unit>
      <trans-unit id="75aa326d" xml:space="preserve">
        <source>##### Filter buttons

|   |   |
|--|--------|
|![filter\_apply.png](../../../../../assets/en/manual/web_interface/filter_apply.png)|Apply specified filtering criteria (without saving).|
|![filter\_reset.png](../../../../../assets/en/manual/web_interface/filter_reset.png)|Reset current filter and return to saved parameters of the current tab. On the main tab, this will clear the filter.|
|![filter\_save\_as.png](../../../../../assets/en/manual/web_interface/filter_save_as.png)|Save current filter parameters in a new tab. Only available on the main tab.|
|![filter\_update.png](../../../../../assets/en/manual/web_interface/filter_update.png)|Replace tab parameters with currently specified parameters. Not available on the main tab.|</source>
      </trans-unit>
      <trans-unit id="bd839887" xml:space="preserve">
        <source>#### Viewing details

The times for problem start and recovery in *Monitoring → Problems* are
links. Clicking on them opens more details of the event.

![](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/event_details.png){width="600"}

Note that the problem severity may differ for the trigger and the problem
event - if it has been updated for the problem event using the *Update
problem* [screen](/manual/acknowledgment#updating_problems).

In the action list, the following icons are used to denote the activity
type:

-   ![icon\_generated.png](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/icon_generated.png) -
    problem event generated
-   ![icon\_message.png](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/icon_message.png) -
    message has been sent
-   ![icon\_acknowledged.png](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/icon_acknowledged.png) -
    problem event acknowledged
-   ![icon\_unacknowledged.png](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/icon_unacknowledged.png) -
    problem event unacknowledged
-   ![icon\_comment2.png](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/icon_comment2.png) -
    a comment has been added
-   ![icon\_sev\_up1.png](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/icon_sev_up1.png) -
    problem severity has been increased (e.g. Information → Warning)
-   ![icon\_sev\_down1.png](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/icon_sev_down1.png) -
    problem severity has been decreased (e.g. Warning → Information)
-   ![icon\_severity\_back.png](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/icon_severity_back.png) -
    problem severity has been changed, but returned to the original
    level (e.g. Warning → Information → Warning)
-   ![icon\_remote.png](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/icon_remote.png) -
    a remote command has been executed
-   ![icon\_recovery.png](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/icon_recovery.png) -
    problem event has recovered
-   ![icon\_closed.png](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/icon_closed.png) -
    the problem has been closed manually
-   ![icon\_suppression.png](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/icon_suppression.png) -
    the problem has been suppressed
-   ![icon\_unsuppressed.png](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/icon_unsuppressed.png) -
    the problem has been unsuppressed
-   ![icon\_symptom.png](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/icon_symptom.png) -
    the problem has been converted to a symptom problem
-   ![icon\_cause.png](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/icon_cause.png) -
    the problem has been converted to a cause problem</source>
      </trans-unit>
    </body>
  </file>
</xliff>
