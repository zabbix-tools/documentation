[comment]: # translation:outdated

[comment]: # ({new-7ed38ae8})
# 2 Inventário

[comment]: # ({/new-7ed38ae8})

[comment]: # ({new-65644c90})
#### Visão geral

O menu inventário fornece uma visão geral de inventário dos hosts com
possibilidade de ver visão resumida ou detalhada.

[comment]: # ({/new-65644c90})
