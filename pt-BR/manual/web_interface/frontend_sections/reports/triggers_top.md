[comment]: # translation:outdated

[comment]: # ({new-376659f4})
# 3 Top 100 de triggers

[comment]: # ({/new-376659f4})

[comment]: # ({new-af962014})
#### Visão geral

Através deste módulo *Relatórios → Top 100 de triggers* você tem acesso
às triggers que mais mudam de estado em determinado período de análise,
ordenadas por quantidade de modificações de estado.

![](../../../../../assets/en/manual/web_interface/triggers_top.png){width="600"}

#### Usando o filtro

Você pode filtrar por grupo de hosts, host, severidade de trigger,
período pré-definido de tempo ou período customizado de tempo.

Tanto a coluna de host quanto a de trigger, ao se clicar no nome,
apresentam menus flutuantes.

[comment]: # ({/new-af962014})
