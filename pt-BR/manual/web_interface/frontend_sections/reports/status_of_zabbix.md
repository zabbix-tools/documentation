[comment]: # translation:outdated

[comment]: # ({new-290553c7})
# 1 Status do Zabbix

[comment]: # ({/new-290553c7})

[comment]: # ({new-ede74e69})
#### Visão geral

Através deste módulo *Relatórios → Status do Zabbix* apresenta um resumo
do ambiente Zabbix.

![](../../../../../assets/en/manual/web_interface/report_status.png){width="600"}

Este relatório também é apresentado como um widget no
[Dashboard](/pt/manual/web_interface/frontend_sections/monitoring/dashboard).

[comment]: # ({/new-ede74e69})

[comment]: # ({new-bfa24672})
##### Dados apresentados

|Parâmetro|Valor|Detalhes|
|----------|-----|--------|
|*Zabbix está rodando*|Status do 'Zabbix Server':<br>**Sim** - o servidor está em execução<br>**Não** - o servidor não está em execução<br>*Nota:* Certifique-se de que pelo menos um processo de trapper está sendo executado no Zabbix Server, senão a interface web irá detectar errado a disponibilidade do Zabbix Server (Parâmetro **StartTrappers** no arquivo [zabbix\_server.conf](/pt/manual/appendix/config/zabbix_server) deverá ser superior a '0').|Localização e porta do Zabbix Server.|
|*Quantidade de hosts*|Quantidade de hosts cadastrados.<br>Templates são contabilizados junto com os hosts.|Quantidade de hosts monitorados/não monitorados/templates.|
|*Quantidade de itens*|Total de itens cadastrados. Apenas os itens associados a hosts são contabilizados.|Quantidade de itens monitorados/não monitorados/não suportados.|
|*Quantidade de triggers*|Total de triggers cadastradas. Apenas triggers associadas a hosts e itens ativos são contabilizadas.|habilitadas/desabilitadas \[Status da trigger - incidente/ok\])|
|*Quantidade de usuários*|Total de usuários online.|Quantidade de usuários online.|
|*Desempenho requerido do servidor, novos valores por segundo*|Performance atual esperada do servidor em itens coletados por segundo.|<|

[comment]: # ({/new-bfa24672})


[comment]: # ({new-76697959})
#### High availability nodes

If [high availability cluster](/manual/concepts/server/ha) is enabled,
then another block of data is displayed with the status of each high
availability node.

![](../../../../../assets/en/manual/web_interface/ha_nodes.png){width="600"}

Displayed data:

|Column|Description|
|------|-----------|
|*Name*|Node name, as defined in server configuration.|
|*Address*|Node IP address and port.|
|*Last access*|Time of node last access.<br>Hovering over the cell shows the timestamp of last access in long format.|
|*Status*|Node status:<br>**Active** - node is up and working<br>**Unavailable** - node hasn't been seen for more than failover delay (you may want to find out why)<br>**Stopped** - node has been stopped or couldn't start (you may want to start it or delete it)<br>**Standby** - node is up and waiting|

[comment]: # ({/new-76697959})
