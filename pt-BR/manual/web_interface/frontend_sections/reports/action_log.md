[comment]: # translation:outdated

[comment]: # ({new-7d551978})
# 5 Log de ações

[comment]: # ({/new-7d551978})

[comment]: # ({new-8be899f9})
#### Visão geral

Através deste módulo *Relatórios → Log de ações* apresenta detalhes das
operações (notificações, comandos remotos) executados quando uma ação
foi acionada.

Você pode utilizar o filtro, localizado logo abaixo da barra de título,
para filtrar as notificações enviadas para determinado usuário ou
filtrar por período.

![](../../../../../assets/en/manual/web_interface/action_log.png){width="600"}

Colunas apresentadas:

|Coluna|Descrição|
|------|-----------|
|*Data*|Registro do momento da operação.|
|*Ação*|Nome da ação que originou a operação.<br>O nome da ação é apresentado desde o Zabbix **2.4.0**.|
|*Tipo*|Tipo da operação - (por exemplo, *E-mail* ou *Command*).|
|*Destinatário(s)*|Apelido do usuário, nome e sobrenome (entre parênteses) e endereço de destino da notificação.<br>O apelido, nome e sobrenome são apresentados desde o Zabbix **2.4.0**.|
|*Mensagem*|O conteúdo da mensagem/comando remoto.|
|*Status*|Status da operação:<br>*Em progresso* - a ação está em execução ainda<br>Para ações em progresso a quantidade de tentativas restantes é apresentada (quantidade de tentativas antes da notificação falhar).<br>*Enviado* - notificação foi enviada<br>*Executado* - comando foi executado<br>*Não enviado* - a operação falhou.|
|*Informação*|Informações de erro (se existirem) a respeito da execução da operação.|

[comment]: # ({/new-8be899f9})

[comment]: # ({new-d84e7bb0})
#### Buttons

The button at the top right corner of the page offers the following option:

|   |   |
|--|--------|
|![](../../../../../assets/en/manual/web_interface/button_csv.png)|Export action log records from all pages to a CSV file. If a filter is applied, only the filtered records will be exported.<br>In the exported CSV file the columns "Recipient" and "Message" are divided into several columns - "Recipient's Zabbix username", "Recipient's name", "Recipient's surname", "Recipient", and "Subject", "Message", "Command".|

[comment]: # ({/new-d84e7bb0})

[comment]: # ({new-accea306})
#### Using filter

The filter is located below the *Action log* bar.
It can be opened and collapsed by clicking on the *Filter* tab at the top right corner of the page.

![](../../../../../assets/en/manual/web_interface/action_log_filter.png){width="600"}

You may use the filter to narrow down the records by notification recipients, actions, media types, status,
or by the message/remote command content (*Search string*).
For better search performance, data is searched with macros unresolved.

[comment]: # ({/new-accea306})

[comment]: # ({new-3eac5e48})
#### Time period selector

The [time period selector](/manual/config/visualization/graphs/simple#time_period_selector) allows to select often required periods with one mouse click.
The time period selector can be opened by clicking on the time period tab next to the filter.

[comment]: # ({/new-3eac5e48})
