[comment]: # translation:outdated

[comment]: # ({new-58cc1743})
# 6 Notificações

[comment]: # ({/new-58cc1743})

[comment]: # ({new-c29272bd})
#### Visão geral

Através deste módulo *Relatórios → Notificações* você tem acesso a um
relatório com a quantidade de mensagens enviadas para cada usuário.

A partir das caixas de seleção na barra de título você pode selecionar
um tipo de mídia (ou todos), período (em visões
diária/semanal/mensal/anual) e o ano das notificações que se deseja
visualizar.

![](../../../../../assets/en/manual/web_interface/notifications.png){width="600"}

Cada coluna apresenta os totais de um usuário do sistema.

[comment]: # ({/new-c29272bd})
