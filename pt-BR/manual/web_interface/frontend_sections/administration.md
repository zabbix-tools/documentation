[comment]: # translation:outdated

[comment]: # ({new-fa188d70})
# 5 Administração

[comment]: # ({/new-fa188d70})

[comment]: # ({new-cded1b68})
#### Visão geral

O menu de administração é para as funções administrativas do próprio
Zabbix. Este menu está acessível somente para usuários do tipo [Super
Administrador
Zabbix](/pt/manual/config/users_and_usergroups/permissions).

[comment]: # ({/new-cded1b68})
