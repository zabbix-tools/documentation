[comment]: # translation:outdated

[comment]: # ({new-d1036ff8})
# 4 Criando seu próprio tema

[comment]: # ({/new-d1036ff8})

[comment]: # ({new-cb54c172})
#### Visão geral

O Zabbix vem com uma quantidade pré-definida de temas. Você pode seguir
este passo-a-passo para criar temas adicionais. Sinta-se livre para
compartilhar o resultado do seu trabalho com a comunidade se você sentir
que criou algo bacana.

[comment]: # ({/new-cb54c172})

[comment]: # ({new-be6afd90})
##### Passo 1

Para criar o seu tema você precisa criar um arquivo CSS e salva-lo em
*styles/themes/mytheme/main.css*. Você também pode copiar um dos
arquivos fornecidos com algum dos temas padrão e modifica-lo para suas
necessidades. As regras no arquivo *main.css* irão se estender para os
arquivos CSS localizados dentro do diretório *styles*. Qualquer imagem
específica do tema deverá ser colocada no diretório
*styles/themes/mytheme/images*.

[comment]: # ({/new-be6afd90})

[comment]: # ({new-34bae249})
##### Passo 2

Adicione o seu tema na lista de temas retornado pelo método
'Z::getThemes()'. Você pode fazer isso ao sobrescrever o método
'ZBase::getThemes()' na classe 'Z'. Isso pode ser feito ao adicionar o
código abaixo no arquivo *include/classes/core/Z.php*:

      public static function getThemes() {
          return array_merge(parent::getThemes(), array(
              'mytheme' => _('My theme')
          ));
      }

::: noteimportant
Observe que o nome que você definir deve ser o
mesmo nome escolhido para o diretório onde você está salvando os
arquivos do tema.
:::

Para adicionar múltiplos temas, apenas os liste junto com o nome do
primeiro tema, conforme exemplo a seguir:

      public static function getThemes() {
          return array_merge(parent::getThemes(), array(
              'mytheme' => _('My theme'),
              'anothertheme' => _('Another theme'),
              'onemoretheme' => _('One more theme')
          ));
      }

Observe que cada tema é separado por uma vírgula.

::: notetip
Para modificar a cor dos gráficos serão necessárias
alterações no banco de dados na tabela *graph\_theme*.
:::

[comment]: # ({/new-34bae249})

[comment]: # ({new-b67ccba3})
##### Passo 3

Ativar o novo tema.

Na interface web do Zabbix, você pode definir o seu tema como padrão ou
modificar o perfil de um usuário para utiliza-lo.

Aproveite o seu novo visual!

[comment]: # ({/new-b67ccba3})
