[comment]: # translation:outdated

[comment]: # ({new-13e47fde})
# - \#6 Pesquisa global

É possível pesquisar por várias entidades na interface web do Zabbix. A
pesquisa global está localizada no canto superior direito da tela. A
caixa de pesquisa é sensível à digitação e faz uma pesquisa rápida à
medida que você digita os caracteres (apenas em hosts), além disso ao
pressionar a tecla `Enter` ou pressionar o ícone
![](../../../assets/en/manual/web_interface/search_icon.png) será
apresentada uma lista detalhada de resultados com links úteis para cada
tipo de entidade.

![](../../../assets/en/manual/web_interface/global_search_dropdown.png)

[comment]: # ({/new-13e47fde})

[comment]: # ({new-5ca91d97})
### - Entidades pesquisadas

É possível pesquisar por estas entidades, através destas propriedades:

-   Hosts
    -   Nome do host
    -   Nome visível
    -   Endereço IP
    -   Nome de DNS
-   Templates
    -   Nome
-   Grupos de hosts
    -   Nome

Os resultados estarão agrupados por cada entidade, sendo possível
recolher os blocos individuais. A quantidade de resultados de cada
entidade será apresentada, por exemplo, *Exibindo 13 de 13 encontrados*.
O máximo de resultados apresentados por cada bloco é de 100 registros.

![](../../../assets/en/manual/web_interface/global_search_results.png){width="600"}

Serão apresentados também o resumo (quantidade) das entidades associadas
a cada entidade pesquisada.

Hosts ativos são apresentados em azul e os inativos em vermelho. Se o
nome do host/template for diferente de seu nome visível o nome visível
será exibido entre parênteses.

[comment]: # ({/new-5ca91d97})

[comment]: # ({new-c51d2c6b})
### - Links disponíveis

Para cada entidade localizada estarão disponíveis links para acesso
rápido:

-   Hosts
    -   Monitoramento
        -   Dados recentes
        -   Triggers
        -   Eventos
        -   Gráficos (*desde o Zabbix 2.2*)
        -   Telas do host
        -   Cenários web (*desde o Zabbix 2.2*)
    -   Configuração
        -   Propriedades do host
        -   Aplicações
        -   Itens
        -   Triggers
        -   Gráficos
        -   Regras de descoberta (LLD) (*desde o Zabbix 2.2*)
        -   Cenários web (*desde o Zabbix 2.2*)

```{=html}
<!-- -->
```
-   Host groups
    -   Monitoramento
        -   Dados recentes
        -   Triggers
        -   Eventos
        -   Gráficos (*desde o Zabbix 2.2*)
        -   Cenários web (*desde o Zabbix 2.2*)
    -   Configuração
        -   Propriedades do grupo de hosts
        -   Hosts membros (desde o Zabbix 2.0.2)
        -   Templates membros (desde o Zabbix 2.0.2)
-   Templates
    -   Configuração
        -   Propriedades do template
        -   Aplicações
        -   Itens
        -   Triggers
        -   Gráficos
        -   Telas do template
        -   Regras de descoberta (LLD) (*desde o Zabbix 2.2*)
        -   Cenários web (*desde o Zabbix 2.2*)

[comment]: # ({/new-c51d2c6b})


[comment]: # ({new-1a40034e})
#### Links available

For each entry the following links are available:

-   Hosts
    -   Monitoring
        -   Latest data
        -   Problems
        -   Graphs
        -   Host dashboards
        -   Web scenarios
    -   Configuration
        -   Items
        -   Triggers
        -   Graphs
        -   Discovery rules
        -   Web scenarios

```{=html}
<!-- -->
```
-   Host groups
    -   Monitoring
        -   Latest data
        -   Problems
        -   Web scenarios
    -   Configuration
        -   Hosts
        -   Templates

```{=html}
<!-- -->
```
-   Templates
    -   Configuration
        -   Items
        -   Triggers
        -   Graphs
        -   Template dashboards
        -   Discovery rules
        -   Web scenarios

[comment]: # ({/new-1a40034e})
