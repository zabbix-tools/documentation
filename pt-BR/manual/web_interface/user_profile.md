[comment]: # translation:outdated

[comment]: # ({new-bba949fc})
# 3 Perfil do usuário

[comment]: # ({/new-bba949fc})

[comment]: # ({new-2f38d1c3})
#### Visão geral

Através do perfil de usuário é possível configurar algumas das
funcionalidades da interface web, tal qual o idioma da interface, tema,
quantidade de linhas apresentadas nas listagens, etc. As modificações
feitas aqui afetarão somente o usuário atual.

Para acessar o formulário de perfil do usuário clique no ícone
![](../../../assets/en/manual/web_interface/user_profile.png) do perfil
de usuário situado no canto superior direito da janela do Zabbix.

[comment]: # ({/new-2f38d1c3})

[comment]: # ({new-8fcf9738})
#### Configuração

A aba **Usuário** permite que você defina várias preferências.

![profile1.png](../../../assets/en/manual/web_interface/profile1.png)

|Parâmetro|Descrição|
|----------|-----------|
|*Senha*|Senha para acesso ao sistema (deve ser confirmada no campo *Senha (novamente*).<br>Para modificar seu valor é necessário clicar no botão *Alterar senha*, para que os campos de senha fiquem visíveis.|
|*Idioma*|Idioma da interface web do Zabbix.<br>A extensão 'php gettext' é necessária para as traduções funcionarem.|
|*Tema*|Define o padrão visual da interface web:<br>**Padrão do sistema** - usa as definições globais do Zabbix<br>**Azul** - tema padrão azul<br>**Escuro** - tema alternativo em cores escuras|
|*Login automático*|Marque esta opção se você deseja que o Zabbix lembre-se das informações de sessão fornecidas por 30 dias. Serão necessários cookies de browser para isso.|
|*Desconexão automática (min. 90 segundos)*|Marque esta opção para habilitar a desconexão automática do usuário por inatividade (menor valor = 90 segundos).|
|*Atualização da tela (em segundos)*|Defina o intervalo de atualização utilizado por mapas, telas, dados em texto plano, etc. Se definido como '0' a função é inativada.|
|*Registros por página*|Você pode determinar quantas linhas serão apresentadas a cada página em listagens.|
|*URL (após se autenticar)*|Você pode definir qual página o Zabbix irá apresentar para cada usuário após o mesmo ter iniciado uma nova sessão. É possível, por exemplo, enviar o usuário para um mapa ou para uma apresentação de slides específica.|

::: noteclassic
Se algum idioma não estiver disponível para seleção no
perfil de usuário isso estará ocorrendo devido à ausência do módulo
'locale' no servidor web. Consulte o [link](#see_also) ao final desta
página para informações sobre como instala-lo.
:::

A aba **Mídia** permite definir as
[mídias](/pt/manual/config/notifications/media) do usuário, seus tipos,
endereços e quando utiliza-los para o envio de notificações.

![profile2.png](../../../assets/en/manual/web_interface/profile2.png)

::: noteclassic
Apenas usuários [com nível de
administrador](/pt/manual/config/users_and_usergroups/permissions)
(Administrador e Super Administrador) podem mudar suas informações de
mídia.
:::

A aba de **Mensagens** permite que você configure as características de
[notificação
global](/pt/manual/web_interface/user_profile/global_notifications).

[comment]: # ({/new-8fcf9738})

[comment]: # ({new-960ba61e})
### Consulte também

1.  [Como instalar locales adicionais para habilitar novos idiomas na
    interface](http://www.zabbix.org/wiki/How_to/install_locale)

[comment]: # ({/new-960ba61e})
