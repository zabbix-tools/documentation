[comment]: # translation:outdated

[comment]: # ({new-3043f46c})
# - \#7 Parâmetros de página

[comment]: # ({/new-3043f46c})

[comment]: # ({new-79688fd8})
#### Visão geral

A interface web suporta vários parâmetros em padrão `HTTP GET` para
controlar o que será exibido. Eles podem ser passados diretamente na URL
usando o padrão `parameter=value`, separados da URL pelo símbolo de
interrogação (?) e, entre sí, pelo caractere de "E comercial" (&).

[comment]: # ({/new-79688fd8})

[comment]: # ({new-46634552})
#### Status das triggers

Módulo acessível em *Monitoramento → Triggers*, nome da página
`tr_status.php`.

::: noteimportant
Para definir o filtro o parâmetro `filter_set=1`
deverá ser informado. Campos ausentes na consulta receberão seu valor
padrão.
:::

[comment]: # ({/new-46634552})

[comment]: # ({new-3af4f65f})
##### Parâmetros genéricos

-   groupid
-   hostid
-   fullscreen

[comment]: # ({/new-3af4f65f})

[comment]: # ({new-0cb5a83c})
##### Parâmetros da página

-   show\_triggers - opção de filtro **Estado das triggers**, 1 -
    Incidentes recentes, 2 - Qualquer, 3 - Incidente
-   ack\_status - opção de filtro **Estado do reconhecimento**, 1 -
    Qualquer, 2 - Com eventos não reconhecidos, 3 - Com o último evento
    não reconhecido
-   show\_events - opção de filtro **Eventos**, 1 - Esconder tudo, 2 -
    Mostrar tudo, 3 - Mostrar não reconhecidos
-   show\_severity - opção de filtro **Severidade mínima da trigger**,
    0-5 - correspondem à severidade
-   status\_change\_days - opção de filtro **quantidade de dias da
    opção - Idade menor que**
-   status\_change - opção de filtro **Idade menor que**, 0 - inativo,
    1 - ativo (**status\_change\_days** deverá ser usado)
-   txt\_select - opção de filtro **Filtrar por nome**, texto livre
-   application - opção de filtro **Aplciação**, texto livre
-   show\_maintenance - opção de filtro **Mostrar hosts em manutenção**,
    0 - não exibir, 1 - exibir
-   show\_details - opção de filtro **Mostrar detalhes**, 0 - não
    exibir, 1 - exibir

**Filtrar por inventário de host**

Desde o Zabbix 2.4.0, as triggers também podem ser filtradas por dados
do inventário do host. A sintaxe aqui é um pouco mais complicada pois se
trata de um array de parâmetros, exemplos:

    inventory[0][field]=type_full
    inventory[0][value]=Virtual machine
    inventory[1][field]=os_full
    inventory[1][value]=Linux

Estes parâmetros deverão ser codificados no padrão de URLS, ficando de
forma similar ao exemplo a seguir:

    inventory%5B0%5D%5Bfield%5D=type_full
    inventory%5B0%5D%5Bvalue%5D=Virtual machine
    inventory%5B1%5D%5Bfield%5D=os_full
    inventory%5B1%5D%5Bvalue%5D=Linux

O códigos referentes aos campos de inventário podem ser encontrados no
manual da [API](/pt/manual/api/reference/host/object#host_inventory).

[comment]: # ({/new-0cb5a83c})
