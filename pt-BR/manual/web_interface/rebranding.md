[comment]: # translation:outdated

[comment]: # ({new-5af64792})
# 12 Rebranding

[comment]: # ({/new-5af64792})

[comment]: # ({new-0e860221})
#### Overview

There are several ways in which you can customize and rebrand your
Zabbix frontend installation:

-   replace the Zabbix logo with a desired one
-   hide links to Zabbix Support and Zabbix Integrations
-   set a custom link to the Help page
-   change copyright in the footer

[comment]: # ({/new-0e860221})

[comment]: # ({new-e820f754})
#### How to

To begin with, you need to create a PHP file and save it as
`local/conf/brand.conf.php`. The contents of the file should be the
following:

``` {.php}
<?php

return [];
```

This will hide the links to Zabbix Support and Zabbix Integrations.

[comment]: # ({/new-e820f754})

[comment]: # ({new-9245e2e7})
##### Custom logo

To use a custom **logo**, add the following line to the array from the
previous listing:

``` {.php}
'BRAND_LOGO' => '{Path to an image on the disk or URL}',
```

With the redesign of the main menu in Zabbix 5.0, there are two
additional images of the Zabbix logo that can be overridden:

-   BRAND\_LOGO\_SIDEBAR - displayed when the sidebar is expanded
-   BRAND\_LOGO\_SIDEBAR\_COMPACT - displayed when the sidebar is
    collapsed

To override:

``` {.php}
'BRAND_LOGO_SIDEBAR' => '{Path to an image on the disk or URL}',
'BRAND_LOGO_SIDEBAR_COMPACT' => '{Path to an image on the disk or URL}',
```

Any image format supported by modern browsers can be used: JPG, PNG,
SVG, BMP, WebP and GIF.

::: noteclassic
Custom logos will not be scaled, resized or modified in any
way, and will be displayed in their original sizes and proportions, but
may be cropped to fit in the corresponding place.
:::

[comment]: # ({/new-9245e2e7})

[comment]: # ({new-b118dd81})
##### Custom copyright notice

To set a custom copyright notice, add BRAND\_FOOTER to the array from
the first listing. Please be aware that HTML is not supported here.
Setting BRAND\_FOOTER to an empty string will hide the copyright notes
completely (but the footer will stay in place).

``` {.php}
'BRAND_FOOTER' => '{text}',
```

[comment]: # ({/new-b118dd81})

[comment]: # ({new-042915f3})
##### Custom help location

To replace the default Help link with a link of your choice, add
BRAND\_HELP\_URL to the array from the first listing.

``` {.php}
'BRAND_HELP_URL' => '{URL}',
```

[comment]: # ({/new-042915f3})

[comment]: # ({new-a1618b57})
##### File example

``` {.php}
<?php

return [
    'BRAND_LOGO' => './images/custom_logo.png',
    'BRAND_LOGO_SIDEBAR' => './images/custom_logo_sidebar.png',
    'BRAND_LOGO_SIDEBAR_COMPACT' => './images/custom_logo_sidebar_compact.png',
    'BRAND_FOOTER' => '© Zabbix',
    'BRAND_HELP_URL' => 'https://www.example.com/help/'
];
```

[comment]: # ({/new-a1618b57})
