[comment]: # translation:outdated

[comment]: # ({new-8fc97588})
# 2 Modo de manutenção da interface web

[comment]: # ({/new-8fc97588})

[comment]: # ({new-fca7c17f})
#### Visão geral

A interface web do Zabbix pode ser temporariamente desativada para
bloquear o acesso a ela. Isso pode ser muito útil para evitar que o
banco de dados de configuração do Zabbix seja alterado por seus
usuários, protegendo a integridade do banco.

O banco de dados do Zabbix pode ser parado durante as tarefas de
manutenção enquanto a interface web está em modo de manutenção.

Apenas usuários de determinados IPs pré-definidos conseguirão utilizar a
interface web durante o modo de manutenção.

[comment]: # ({/new-fca7c17f})

[comment]: # ({new-dcfd8b13})
#### Configuração

Para habilitar o modo de manutenção será necessário alterar o arquivo
`maintenance.inc.php` que fica localizado no diretório '/conf' dentro da
interface web do Zabbix, isso pode ser feito ao alterar as seguintes
linhas:

    // Modo de manutenção.
    define('ZBX_DENY_GUI_ACCESS', 1);

    // Array de endereços IP que tem permissão de conectar-se à interface web (opcional).
    $ZBX_GUI_ACCESS_IP_RANGE = array('127.0.0.1');

    // Mensagem de aviso de interface bloqueada para manutenção (opcional).
    $ZBX_GUI_ACCESS_MESSAGE = 'We are upgrading MySQL database till 15:00. Stay tuned...';

|Parâmetro|Detalhes|
|----------|--------|
|**ZBX\_DENY\_GUI\_ACCESS**|Controla o modo de manutenção:<br>1 – modo de manutenção ativo, inativo com qualquer outro valor|
|**ZBX\_GUI\_ACCESS\_IP\_RANGE**|Array com endereços IP que possuem permissão de conectar-se à interface web (opcional).<br>Exemplo:<br>`array('192.168.1.1', '192.168.1.2')`|
|**ZBX\_GUI\_ACCESS\_MESSAGE**|Mensagem de aviso de interface bloqueada para manutenção (opcional).|

[comment]: # ({/new-dcfd8b13})

[comment]: # ({new-ae331777})

::: notetip
Mostly the `maintenance.inc.php` file is located in `/conf` of Zabbix HTML document directory on the
web server. However, the location of the directory may differ depending on the operating system and a web server it uses.

For example, the location for:

-  SUSE and RedHat is `/etc/zabbix/web/maintenance.inc.php`.
-  Debian-based systems is `/usr/share/zabbix/conf/`.

See also [Copying PHP files](/manual/installation/install#copying-php-files). 
:::

|Parameter|Details|
|--|--------|
|**ZBX\_DENY\_GUI\_ACCESS**|Enable maintenance mode:<br>1 – maintenance mode is enabled, disabled otherwise|
|**ZBX\_GUI\_ACCESS\_IP\_RANGE**|Array of IP addresses, which are allowed to connect to frontend (optional).<br>For example:<br>`array('192.168.1.1', '192.168.1.2')`|
|**ZBX\_GUI\_ACCESS\_MESSAGE**|A message you can enter to inform users about the maintenance (optional).|

Note that the [location](/manual/installation/install#copying-php-files) of the `/conf` directory will vary based on the operating system and web server. 

[comment]: # ({/new-ae331777})

[comment]: # ({new-987fdc44})
#### Apresentação

A tela a seguir será apresentada quando usuários, que não tenham seu IP
definido no parâmetro **ZBX\_GUI\_ACCESS\_IP\_RANGE**, tentarem acessar
a interface web durante o modo de manutenção. A tela será atualizada a
cada 30 segundos para que o funcionamento da interface volte ao normal
sem precisar de intervenção direta dos usuários.

![](../../../assets/en/manual/web_interface/frontend_maintenance.png)

[comment]: # ({/new-987fdc44})
