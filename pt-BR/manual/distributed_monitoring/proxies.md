[comment]: # translation:outdated

[comment]: # ({new-0a2d9a93})
# Proxies

[comment]: # ({/new-0a2d9a93})

[comment]: # ({new-5a59cd81})
#### Visão geral

Um Proxy Zabbix pode coletar dados de performance e disponibilidade em
nome do Zabbix Server. Desta forma o proxy transfere para sí parte da
carga de processamento de coletar os dados que normalmente seria
atribuída ao Zabbix Server.

Além disso, usar um proxy é a forma mais fácil de implementar uma
monitoração com configuração centralizada mas coleta distribuída, onde
os agentes e proxies se reportam a um Zabbix Server e os dados são
armazenados de forma centralizada.

Um Zabbix proxy pode ser usado para:

-   Monitorar localizações remotas
-   Monitorar localizações com conexão instável
-   Reduzir a carga de processamento no Zabbix Server quando este tem
    que monitorar milhares de dispositivos
-   Simplificar a manutenção da monitoração distribuída

![](../../../assets/en/manual/proxies/proxy.png)

O proxy requer apenas uma conexão com o Zabbix Server. Desta forma fica
mais simples de se configurar as regras de firewall.

::: noteimportant
O Zabbix proxy precisa utilizar um banco de dados
em separado. Se ele for configurado para usar o mesmo BD do Zabbix
Server irá corromper a configuração.
:::

Todos os dados coletados pelo proxy são armazenados de forma local
temporariamente antes de transmiti-los para o Zabbix Server. Desta forma
nenhum dado será perdido se ocorrerem problemas temporários de
comunicação entre as partes. Os parâmetros *ProxyLocalBuffer* e
*ProxyOfflineBuffer* no [arquivo de configuração do
proxy](/pt/manual/appendix/config/zabbix_proxy) controlam a quantidade
de dados que pode ser guardada localmente.

O Zabbix proxy é um coletor de dados. Ele não calcula triggers, processa
eventos ou envia alertas. Para uma visão geral do que um proxy é capaz
de fazer, revise a tabela a seguir:

|Funcionalidade|<|Suportado pelo proxy|
|--------------|-|--------------------|
|Itens|<|<|
|<|*Zabbix agent checks (passivo)*|**Sim**|
|^|*Zabbix agent checks (ativo)*|**Sim** ^1^|
|^|*Simple checks*|**Sim**|
|^|*Trapper items*|**Sim**|
|^|*SNMP checks*|**Sim**|
|^|*SNMP traps*|**Sim**|
|^|*IPMI checks*|**Sim**|
|^|*JMX checks*|**Sim**|
|^|*Monitoração de logs*|**Sim**|
|^|*Verificações internas*|**Sim**|
|^|*SSH checks*|**Sim**|
|^|*Telnet checks*|**Sim**|
|^|*External checks*|**Sim**|
|Monitoração web|<|**Sim**|
|Descoberta de rede|<|**Sim**|
|Autobusca|<|**Sim**|
|Calcular triggers|<|*Não*|
|Processar eventos|<|*Não*|
|Enviar alertas|<|*Não*|
|Comandos remotos|<|*Não*|

::: noteclassic
\[1\] Para se certificar que um agente irá se comunicar com
o proxy (e não com o servidor) para a monitoração ativa, o proxy deverá
estar listado no parâmetro **ServerActive** do arquivo de configuração
do agente.
:::

[comment]: # ({/new-5a59cd81})

[comment]: # ({new-737fe0c2})
##### Protection from overloading

If Zabbix server was down for some time, and proxies have collected a lot of data, 
and then the server starts, it may get overloaded (history cache usage stays at 95-100% for some time). 
This overload could result in a performance hit, where checks are processed slower 
than they should. Protection from this scenario was implemented to avoid problems that arise 
due to overloading history cache.

When Zabbix server history cache is full the history cache write access is being 
throttled, stalling server data gathering processes. The most common history cache 
overload case is after server downtime when proxies are uploading gathered data. To avoid 
this proxy throttling was added (currently it cannot be disabled).

Zabbix sever will stop accepting data from proxies when history cache usage reaches 80%. 
Instead those proxies will be put on a throttling list. This will continue until the cache usage 
falls down to 60%. Now server will start accepting data from proxies one by one, defined by the 
throttling list. This means the first proxy that attempted to upload data during the throttling 
period will be served first and until it's done the server will not accept data from other proxies.

This throttling mode will continue until either cache usage hits 80% again or falls down to 20% 
or the throttling list is empty. In the first case the server will stop accepting proxy data again. 
In the other two cases the server will start working normally, accepting data from all proxies.

You may use the `zabbix[wcache,history,pused]` internal item to correlate this behavior of Zabbix 
server with a metric.

[comment]: # ({/new-737fe0c2})

[comment]: # ({a186b4d5-e3203196})
#### Configuração

Após [Instalado](/manual/installation/install) e
[Configurado](/manual/appendix/config/zabbix_proxy) um proxy, está na hora de configurá-lo no frontend do Zabbix.

[comment]: # ({/a186b4d5-e3203196})

[comment]: # ({new-a5555aaf})
##### Adicionando proxies

Para configurar um proxy:

-   Acesse *Administração → Proxies*
-   Clique no botão *Criar proxy*

![](../../../assets/en/manual/distributed_monitoring/proxy.png){width="600"}

|Parâmetro|Descrição|
|----------|-----------|
|*Nome do proxy*|Informe o nome do proxy. Precisa ser o mesmo que o retornado pelo parâmetro *Hostname* pelo arquivo de configuração do proxy.|
|*Modo do Proxy*|Selecione o modo do proxy.<br>**Ativo** - o proxy irá se conectar ao Zabbix Server e solicitar os dados de configuração<br>**Passivo** - O Zabbix server que se conecta ao proxy<br>*Nota* observe que dados sensíveis de configuração não podem se tornar disponíveis a partes que tenham acesso à porta de 'trapper' do Zabbix Server. Isso por que ninguém pode fingir ser um conjunto de dados de proxy e um pedido de comunicação ativa, isso não ocorre.|
|*Hosts*|Adiciona hosts para serem monitorados pelo proxy.|
|*Descrição*|Descrição do proxy.|

A aba **Criptografia** permite que as comunicações com o proxy sejam
mais seguras.

|Parâmetro|Descrição|
|----------|-----------|
|*Conexões para o host*|Como o Zabbix Server irá se conectar a um proxy passivo: Sem criptografia (padrão), PSK (pre-shared key) ou Certificado.|
|*Conexões do proxy*|Quais tipos de conexão a partir deste proxy serão aceitas. Vários tipos de conexão podem ser selecionados ao mesmo tempo (muito útil para teses e modificação de tipo de conexão). O padrão é "Sem criptografia".|
|*Emissor*|Emissor autorizado para o certificado. O certificado será validado primariamente com a CA (autoridade certificadora). Se for válido e assinado pela CA, então o campo then the *Emissor* poderá ser utilizado para restringir as CAs. Este campo é opcional e objetiva que sua instalação Zabbix suporte certificados de múltiplas autoridades certificadoras (CAs).|
|*Assunto*|Assunto permitido no certificado. O certificado será primerio validado com a CA, se válido e assinado então o campo *Assunto* poderá ser usado para só permitir certificados com determinado texto no assunto. Se este campo estiver vazio qualquer certificado assinado pela CA será aceito.|
|*Identidade PSK*|Texto de identidade PSK Pre-shared key.|
|*PSK*|Pre-shared key (texto hexadecimal). Tamanho máximo: 512 hex-digits (256-byte PSK) se o Zabbix utilizar as bibliotecas GnuTLS ou OpenSSL, 64 hex-digits (32-byte PSK) se o Zabbix utilizar a biblioteca TLS (PolarSSL). Exemplo: 1f87b595725ac58dd977beef14b97461a7c1045b9a1c963065002c5473194952|

[comment]: # ({/new-a5555aaf})

[comment]: # ({new-96c17aaf})
##### Configuração do host

Você pode associar a monitoração de um host para que seja feita através
de um proxy simplesmente modificando a [configuração do
host](/pt/manual/config/hosts/host), usando o campo *Monitorado pelo
proxy*.

![](../../../assets/en/manual/proxies/proxy_set.png)

O processo de [atualização em massa](/pt/manual/config/hosts/hostupdate)
de hosts é outra forma de especificar quais hosts serão monitorados por
um proxy.

[comment]: # ({/new-96c17aaf})
