[comment]: # translation:outdated

[comment]: # ({new-9f1711bf})
# 15. Monitoração distribuída

[comment]: # ({/new-9f1711bf})

[comment]: # ({new-e207d054})
#### Visão geral

O Zabbix fornece um método eficiente e confiável para monitorar
infraestrutura de TI distribuida: os
[proxies](/pt/manual/distributed_monitoring/proxies).

Os proxies podem ser utilizados para coletar dados locais em nome de um
Zabbix Server centralizado, transmitindo periodicamente os dados para a
central.

[comment]: # ({/new-e207d054})

[comment]: # ({cede5293-6744cab9})
#####  Recursos de Proxy 

Ao optar por usar/não um proxy, várias considerações
deverão  ser levadas em conta.

|<|Proxy|
|-|-----|
|*Leve*|**Sim**|
|*Interface gráfica*|No|
|*Serviços independentes*|**Sim**|
|*Fácil manutenção*|**Sim**|
|*Criação automática de banco de dados*^1^|**Sim**|
|*Administração local *|No|
|*Pronto para hardware incorporado*|**Sim**|
|*Conexões TCP unidirecionais*|**Sim**|
|*Configuração centralizada*|**Sim**|
|*Gera notificações*|Não|

::: noteclassic
\[1\] O recurso de criação automática de banco de dados só funciona com SQLite.Outros bancos de dados requerem uma [configuração manual.
](/manual/installation/install#requirements).
:::

[comment]: # ({/cede5293-6744cab9})
