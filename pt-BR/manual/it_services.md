[comment]: # translation:outdated

[comment]: # ({new-2ac76b92})
# 7. Serviços de TI

[comment]: # ({/new-2ac76b92})

[comment]: # ({new-90a2641a})
#### Visão geral

Os serviços de TI tem como objetivo atender quem precisa de uma visão de
alto nível (negócio) da infraestrutura monitorada. Em muitos casos, nós
não estamos interessados nos detalhes de baixo nível, tal qual uma
tendência de esgotamento de disco, alta carga de processamento, etc. O
que nos interessa é a disponibilidade de um serviço provido por nosso
setor de TI. Nós também podemos estar interessados em identificar
problemas na infraestrutura de TI, níveis de serviço, a estrutura de TI
e outras informações de alto nível.

Os serviços de TI do Zabbix conseguem atender a todas as questões acima.
A sua representação é feita de forma retroativa e com os dados
monitorados.

Uma estrutura muito simples de um serviço de TI poderia ser:

    IT Service
    |
    |-Workstations
    | |
    | |-Workstation1
    | |
    | |-Workstation2
    |
    |-Servers

Cada nó da estrutura tem o seu próprio atributo de status. O status é
calculado e propagado para os níveis superiores de acordo com o
algoritmo selecionado. No nível mais baixo dos serviços de TI estão as
triggers. O status dos nós individuais são afetados pelo estado de suas
triggers.

::: noteclassic
Observe que as triggers com severidade *Não classificada* ou
*Informação* não impactam o cálculo do SLA.
:::

[comment]: # ({/new-90a2641a})

