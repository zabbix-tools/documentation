[comment]: # translation:outdated

[comment]: # ({new-ae6d2ebd})
# Hosts

Os hosts são exportados com vários objetos associados e relacionados.

Uma exportação de host contêm:

-   dados do host
-   dados do inventário do host
-   relacionamento com grupos
-   associação com templates
-   interfaces
-   macros
-   aplicações
-   itens
-   regras de descoberta e seus protótipos

O processo de importação de hosts só adiciona associações com templates,
nunca as removendo.

``` {.xml}
<hosts>
        <host>
            <host>Zabbix server</host>
            <name>Zabbix server</name>
            <description>Zabbix monitoring server.</description>
            <proxy/>
            <status>0</status>
            <ipmi_authtype>-1</ipmi_authtype>
            <ipmi_privilege>2</ipmi_privilege>
            <ipmi_username/>
            <ipmi_password/>
            <templates/>
            <groups>
                <group>
                    <name>Zabbix servers</name>
                </group>
            </groups>
            <interfaces>
                <interface>
                    <default>1</default>
                    <type>1</type>
                    <useip>1</useip>
                    <ip>127.0.0.1</ip>
                    <dns/>
                    <port>20001</port>
                    <interface_ref>if1</interface_ref>
                </interface>
            </interfaces>
            <applications>
                <application>
                    <name>Memory</name>
                </application>
                <application>
                    <name>Zabbix agent</name>
                </application>
            </applications>
            <items>
                <item>
                    <name>Agent ping</name>
                    <type>0</type>
                    <snmp_community/>
                    <multiplier>0</multiplier>
                    <snmp_oid/>
                    <key>agent.ping</key>
                    <delay>60</delay>
                    <history>7</history>
                    <trends>365</trends>
                    <status>0</status>
                    <value_type>3</value_type>
                    <allowed_hosts/>
                    <units/>
                    <delta>0</delta>
                    <snmpv3_securityname/>
                    <snmpv3_securitylevel>0</snmpv3_securitylevel>
                    <snmpv3_authpassphrase/>
                    <snmpv3_privpassphrase/>
                    <formula>1</formula>
                    <delay_flex/>
                    <params/>
                    <ipmi_sensor/>
                    <data_type>0</data_type>
                    <authtype>0</authtype>
                    <username/>
                    <password/>
                    <publickey/>
                    <privatekey/>
                    <port/>
                    <description>The agent always returns 1 for this item. It could be used in combination with nodata() for availability check.</description>
                    <inventory_link>0</inventory_link>
                    <applications>
                        <application>
                            <name>Zabbix agent</name>
                        </application>
                    </applications>
                    <valuemap>
                        <name>Zabbix agent ping status</name>
                    </valuemap>
                    <logtimefmt/>
                    <interface_ref>if1</interface_ref>
                </item>
                <item>
                    <name>Available memory</name>
                    <type>0</type>
                    <snmp_community/>
                    <multiplier>0</multiplier>
                    <snmp_oid/>
                    <key>vm.memory.size[available]</key>
                    <delay>60</delay>
                    <history>7</history>
                    <trends>365</trends>
                    <status>0</status>
                    <value_type>3</value_type>
                    <allowed_hosts/>
                    <units>B</units>
                    <delta>0</delta>
                    <snmpv3_securityname/>
                    <snmpv3_securitylevel>0</snmpv3_securitylevel>
                    <snmpv3_authpassphrase/>
                    <snmpv3_privpassphrase/>
                    <formula>1</formula>
                    <delay_flex/>
                    <params/>
                    <ipmi_sensor/>
                    <data_type>0</data_type>
                    <authtype>0</authtype>
                    <username/>
                    <password/>
                    <publickey/>
                    <privatekey/>
                    <port/>
                    <description>Available memory is defined as free+cached+buffers memory.</description>
                    <inventory_link>0</inventory_link>
                    <applications>
                        <application>
                            <name>Memory</name>
                        </application>
                    </applications>
                    <valuemap/>
                    <logtimefmt/>
                    <interface_ref>if1</interface_ref>
                </item>
            </items>
            <discovery_rules>
                <discovery_rule>
                    <name>Mounted filesystem discovery</name>
                    <type>0</type>
                    <snmp_community/>
                    <snmp_oid/>
                    <key>vfs.fs.discovery</key>
                    <delay>3600</delay>
                    <status>0</status>
                    <allowed_hosts/>
                    <snmpv3_securityname/>
                    <snmpv3_securitylevel>0</snmpv3_securitylevel>
                    <snmpv3_authpassphrase/>
                    <snmpv3_privpassphrase/>
                    <delay_flex/>
                    <params/>
                    <ipmi_sensor/>
                    <authtype>0</authtype>
                    <username/>
                    <password/>
                    <publickey/>
                    <privatekey/>
                    <port/>
                    <filter>{#FSTYPE}:@File systems for discovery</filter>
                    <lifetime>30</lifetime>
                    <description>Discovery of file systems of different types as defined in global regular expression "File systems for discovery".</description>
                    <item_prototypes>
                        <item_prototype>
                            <name>Free disk space on $1</name>
                            <type>0</type>
                            <snmp_community/>
                            <multiplier>0</multiplier>
                            <snmp_oid/>
                            <key>vfs.fs.size[{#FSNAME},free]</key>
                            <delay>60</delay>
                            <history>7</history>
                            <trends>365</trends>
                            <status>0</status>
                            <value_type>3</value_type>
                            <allowed_hosts/>
                            <units>B</units>
                            <delta>0</delta>
                            <snmpv3_securityname/>
                            <snmpv3_securitylevel>0</snmpv3_securitylevel>
                            <snmpv3_authpassphrase/>
                            <snmpv3_privpassphrase/>
                            <formula>1</formula>
                            <delay_flex/>
                            <params/>
                            <ipmi_sensor/>
                            <data_type>0</data_type>
                            <authtype>0</authtype>
                            <username/>
                            <password/>
                            <publickey/>
                            <privatekey/>
                            <port/>
                            <description/>
                            <inventory_link>0</inventory_link>
                            <applications>
                                <application>
                                    <name>Filesystems</name>
                                </application>
                            </applications>
                            <valuemap/>
                            <logtimefmt/>
                            <application_prototypes>
                                <application_prototype>
                                    <name>{#FSNAME}</name>
                                </application_prototype>
                            </application_prototypes>
                            <interface_ref>if1</interface_ref>
                        </item_prototype>
                    </item_prototypes>
                    <trigger_prototypes>
                        <trigger_prototype>
                            <expression>{Zabbix server 2:vfs.fs.size[{#FSNAME},pfree].last()}<20</expression>
                            <name>Free disk space is less than 20% on volume {#FSNAME}</name>
                            <url/>
                            <status>0</status>
                            <priority>2</priority>
                            <description/>
                            <type>0</type>
                        </trigger_prototype>
                    </trigger_prototypes>
                    <graph_prototypes>
                        <graph_prototype>
                            <name>Disk space usage {#FSNAME}</name>
                            <width>600</width>
                            <height>340</height>
                            <yaxismin>0.0000</yaxismin>
                            <yaxismax>0.0000</yaxismax>
                            <show_work_period>0</show_work_period>
                            <show_triggers>0</show_triggers>
                            <type>2</type>
                            <show_legend>1</show_legend>
                            <show_3d>1</show_3d>
                            <percent_left>0.0000</percent_left>
                            <percent_right>0.0000</percent_right>
                            <ymin_type_1>0</ymin_type_1>
                            <ymax_type_1>0</ymax_type_1>
                            <ymin_item_1>0</ymin_item_1>
                            <ymax_item_1>0</ymax_item_1>
                            <graph_items>
                                <graph_item>
                                    <sortorder>0</sortorder>
                                    <drawtype>0</drawtype>
                                    <color>C80000</color>
                                    <yaxisside>0</yaxisside>
                                    <calc_fnc>2</calc_fnc>
                                    <type>2</type>
                                    <item>
                                        <host>Zabbix server 2</host>
                                        <key>vfs.fs.size[{#FSNAME},total]</key>
                                    </item>
                                </graph_item>
                                <graph_item>
                                    <sortorder>1</sortorder>
                                    <drawtype>0</drawtype>
                                    <color>00C800</color>
                                    <yaxisside>0</yaxisside>
                                    <calc_fnc>2</calc_fnc>
                                    <type>0</type>
                                    <item>
                                        <host>Zabbix server 2</host>
                                        <key>vfs.fs.size[{#FSNAME},free]</key>
                                    </item>
                                </graph_item>
                            </graph_items>
                        </graph_prototype>
                    </graph_prototypes>
                    <interface_ref>if1</interface_ref>
                </discovery_rule>
            </discovery_rules>
            <macros>
                <macro>
                    <macro>{$M1}</macro>
                    <value>m1</value>
                </macro>
                <macro>
                    <macro>{$M2}</macro>
                    <value>m2</value>
                </macro>
            </macros>
            <inventory/>
        </host>
    </hosts>
```

[comment]: # ({/new-ae6d2ebd})

[comment]: # ({new-907c3faa})
#### hosts/host

|Parâmetro|Tipo|Descrição|Detalhes|
|----------|----|-----------|--------|
|host|*string*|Nome do host.|<|
|name|*string*|Nome visível do host.|<|
|description|*string*|Descrição do host.|<|
|status|*int*|Status do host.|<|
|proxy|*int*|Nome do proxy utilizado para sua monitoração.|<|
|ipmi\_authtype|*int*|Tipo de autenticação IPMI.|<|
|ipmi\_privilege|*int*|Privilégio IPMI.|<|
|ipmi\_username|*string*|Usuário de autenticação no IPMI.|<|
|ipmi\_password|*string*|Senha de autenticação no IPMI.|<|

[comment]: # ({/new-907c3faa})

[comment]: # ({new-55d6da26})
#### hosts/host/groups/group

|Parâmetro|Tipo|Descrição|Detalhes|
|----------|----|-----------|--------|
|name|*string*|Nome do grupo.|<|

[comment]: # ({/new-55d6da26})

[comment]: # ({new-158cdcbf})
#### hosts/host/templates/template

|Parâmetro|Tipo|Descrição|Detalhes|
|----------|----|-----------|--------|
|name|*string*|Nome técnico do template.|<|

[comment]: # ({/new-158cdcbf})

[comment]: # ({new-93059e09})
#### hosts/host/interfaces/interface

|Nome da coluna|Tipo|Descrição|
|--------------|----|-----------|
|**default**|inteiro|Status da interface:|
|^|^|0 - Não é a interface padrão|
|^|^|1 - Interface padrão|
|**type**|inteiro|Tipo de interface:|
|^|^|1 - Agente|
|^|^|2 - SNMP|
|^|^|3 - IPMI|
|^|^|4 - JMX|
|**useip**|inteiro|Como será a conexão ao host:|
|^|^|0 – através do nome de DNS|
|^|^|1 – através do endereço IP|
|**ip**|texto|Endereço IP, pode ser no padrão IPv4 ou IPv6.|
|**dns**|texto|Nome de DNS.|
|**port**|texto|Número da porta.|
|**interface\_ref**|texto|Nome de referencia de interface a ser utilizada nos itens.|

[comment]: # ({/new-93059e09})

[comment]: # ({new-2c61d3f8})
#### hosts/host/applications/application

|Parâmetro|Tipo|Descrição|Detalhes|
|----------|----|-----------|--------|
|name|*string*|Nome da aplicação.|<|

[comment]: # ({/new-2c61d3f8})

[comment]: # ({new-eedfe2b9})
#### hosts/host/items/item

|Parâmetro|Tipo|Descrição|
|----------|----|-----------|
|type|*int*|Tipo do item:|
|^|^|0 - Agente Zabbix|
|^|^|1 - SNMPv1|
|^|^|2 - Trapper|
|^|^|3 - Verificação simples|
|^|^|4 - SNMPv2|
|^|^|5 - Interno|
|^|^|6 - SNMPv3|
|^|^|7 - Agente Zabbix Ativo|
|^|^|8 - Agregado|
|^|^|9 - Teste HTTP (passo de monitoração web)|
|^|^|10 - Externo|
|^|^|11 - Monitoração de banco de dados|
|^|^|12 - IPMI|
|^|^|13 - SSH|
|^|^|14 - telnet|
|^|^|15 - Calculado|
|^|^|16 - JMX|
|^|^|17 - trap SNMP|
|snmp\_community|*string*|Nome da comunidade SNMP|
|snmp\_oid|*string*|OID SNMP|
|port|*int*|Porta customizada do item|
|name|*string*|Nome do item|
|key|*string*|Chave do item|
|delay|*int*|Intervalo entre verificações|
|history|*int*|Tempo de retenção de dados no histórico (dias)|
|trends|*int*|Tempo de retenção de dados de médias (dias)|
|status|*int*|Status do item|
|value\_type|*int*|Tipo do dado|
|trapper\_hosts|*string*|<|
|units|*string*|Unidade do valor|
|multiplier|*int*|Multiplicador|
|delta|*int*|Guardar o delta dos valores|
|snmpv3\_securityname|*string*|SNMPv3 - nome de segurança|
|snmpv3\_securitylevel|*int*|SNMPv3 - nível de segurança|
|snmpv3\_authpassphrase|*string*|SNMPv3 - frase de autenticação|
|snmpv3\_privpassphrase|*string*|SNMPv3 - frase privada|
|formula|*string*|<|
|delay\_flex|*string*|Intervalo flexível|
|params|*string*|<|
|ipmi\_sensor|*string*|IPMI - Sensor|
|data\_type|*int*|<|
|authtype|*int*|<|
|username|*string*|<|
|password|*string*|<|
|publickey|*string*|<|
|privatekey|*string*|<|
|interface\_ref|*varchar*|Referência a interface do host|
|description|*string*|Descrição do item|
|inventory\_link|*int*|Número do campo de inventário do host associado, o campo será populado automaticamente com o valor do item|
|applications|<|Aplicações do item|
|valuemap|<|Mapeamento de valor associado ao item|
|logtimefmt|*string*|Formato de hora nas entradas do log. Usado apenas para itens de log.|

[comment]: # ({/new-eedfe2b9})

[comment]: # ({new-8a2e4e2f})
#### hosts/host/items/item/applications/application

|Parâmetro|Tipo|Descrição|Detalhes|
|----------|----|-----------|--------|
|name|*string*|Nome da aplicação.|<|

[comment]: # ({/new-8a2e4e2f})






[comment]: # ({new-19e8eae6})
##### Host low-level discovery rule tags

|Element|Element property|Required|Type|Range^**[1](#footnotes)**^|Description|
|-------|----------------|--------|----|--------------------------|-----------|
|discovery\_rules|<|\-|<|<|Root element for low-level discovery rules.|
|<|*For most of the element tag values, see element tag values for a regular item. Only the tags that are specific to low-level discovery rules, are described below.*|<|<|<|<|
|<|type|\-|`string`|0 - ZABBIX\_PASSIVE (default)<br>2 - TRAP<br>3 - SIMPLE<br>5 - INTERNAL<br>7 - ZABBIX\_ACTIVE<br>10 - EXTERNAL<br>11 - ODBC<br>12 - IPMI<br>13 - SSH<br>14 - TELNET<br>16 - JMX<br>18 - DEPENDENT<br>19 - HTTP\_AGENT<br>20 - SNMP\_AGENT|Item type.|
|<|lifetime|\-|`string`|Default: 30d|Time period after which items that are no longer discovered will be deleted. Seconds, time unit with suffix or user macro.|
|filter|<|<|<|<|Individual filter.|
|<|evaltype|\-|`string`|0 - AND\_OR (default)<br>1 - AND<br>2 - OR<br>3 - FORMULA|Logic to use for checking low-level discovery rule filter conditions.|
|<|formula|\-|`string`|<|Custom calculation formula for filter conditions.|
|conditions|<|\-|<|<|Root element for filter conditions.|
|<|macro|x|`string`|<|Low-level discovery macro name.|
|<|value|\-|`string`|<|Filter value: regular expression or global regular expression.|
|<|operator|\-|`string`|8 - MATCHES\_REGEX (default)<br>9 - NOT\_MATCHES\_REGEX|Condition operator.|
|<|formulaid|x|`character`|<|Arbitrary unique ID that is used to reference a condition from the custom expression. Can only contain capital-case letters. The ID must be defined by the user when modifying filter conditions, but will be generated anew when requesting them afterward.|
|lld\_macro\_paths|<|\-|<|<|Root element for LLD macro paths.|
|<|lld\_macro|x|`string`|<|Low-level discovery macro name.|
|<|path|x|`string`|<|Selector for value which will be assigned to the corresponding macro.|
|preprocessing|<|\-|<|<|LLD rule value preprocessing.|
|step|<|\-|<|<|Individual LLD rule value preprocessing step.|
|<|*For most of the element tag values, see element tag values for a host item value preprocessing. Only the tags that are specific to low-level discovery value preprocessing, are described below.*|<|<|<|<|
|<|type|x|`string`|5 - REGEX<br>11 - XMLPATH<br>12 - JSONPATH<br>15 - NOT\_MATCHES\_REGEX<br>16 - CHECK\_JSON\_ERROR<br>17 - CHECK\_XML\_ERROR<br>20 - DISCARD\_UNCHANGED\_HEARTBEAT<br>21 - JAVASCRIPT<br>23 - PROMETHEUS\_TO\_JSON<br>24 - CSV\_TO\_JSON<br>25 - STR\_REPLACE<br>27 - XML\_TO\_JSON|Type of the item value preprocessing step.|
|trigger\_prototypes|<|\-|<|<|Root element for trigger prototypes.|
|<|*For trigger prototype element tag values, see regular [host trigger](/manual/xml_export_import/hosts#host_trigger_tags) tags.*|<|<|<|<|
|graph\_prototypes|<|\-|<|<|Root element for graph prototypes.|
|<|*For graph prototype element tag values, see regular [host graph](/manual/xml_export_import/hosts#host_graph_tags) tags.*|<|<|<|<|
|host\_prototypes|<|\-|<|<|Root element for host prototypes.|
|<|*For host prototype element tag values, see regular [host](/manual/xml_export_import/hosts#host_tags) tags.*|<|<|<|<|
|item\_prototypes|<|\-|<|<|Root element for item prototypes.|
|<|*For item prototype element tag values, see regular [host item](/manual/xml_export_import/hosts#host_item_tags) tags.*|<|<|<|<|
|master\_item|<|\-|<|<|Individual item prototype master item/item prototype data.|
|<|key|x|`string`|<|Dependent item prototype master item/item prototype key value.<br><br>Required for a dependent item.|

[comment]: # ({/new-19e8eae6})

[comment]: # ({new-5ff5f79a})
##### Host trigger tags

|Element|Element property|Required|Type|Range^**[1](#footnotes)**^|Description|
|-------|----------------|--------|----|--------------------------|-----------|
|triggers|<|\-|<|<|Root element for triggers.|
|<|expression|x|`string`|<|Trigger expression.|
|<|recovery\_mode|\-|`string`|0 - EXPRESSION (default)<br>1 - RECOVERY\_EXPRESSION<br>2 - NONE|Basis for generating OK events.|
|<|recovery\_expression|\-|`string`|<|Trigger recovery expression.|
|<|name|x|`string`|<|Trigger name.|
|<|correlation\_mode|\-|`string`|0 - DISABLED (default)<br>1 - TAG\_VALUE|Correlation mode (no event correlation or event correlation by tag).|
|<|correlation\_tag|\-|`string`|<|The tag name to be used for event correlation.|
|<|url|\-|`string`|<|URL associated with the trigger.|
|<|status|\-|`string`|0 - ENABLED (default)<br>1 - DISABLED|Trigger status.|
|<|priority|\-|`string`|0 - NOT\_CLASSIFIED (default)<br>1 - INFO<br>2 - WARNING<br>3 - AVERAGE<br>4 - HIGH<br>5 - DISASTER|Trigger severity.|
|<|description|\-|`text`|<|Trigger description.|
|<|type|\-|`string`|0 - SINGLE (default)<br>1 - MULTIPLE|Event generation type (single problem event or multiple problem events).|
|<|manual\_close|\-|`string`|0 - NO (default)<br>1 - YES|Manual closing of problem events.|
|dependencies|<|\-|<|<|Root element for dependencies.|
|<|name|x|`string`|<|Dependency trigger name.|
|<|expression|x|`string`|<|Dependency trigger expression.|
|<|recovery\_expression|\-|`string`|<|Dependency trigger recovery expression.|
|tags|<|\-|<|<|Root element for event tags.|
|<|tag|x|`string`|<|Tag name.|
|<|value|\-|`string`|<|Tag value.|

[comment]: # ({/new-5ff5f79a})

[comment]: # ({new-0a8a430a})
##### Host graph tags

|Element|Element property|Required|Type|Range^**[1](#footnotes)**^|Description|
|-------|----------------|--------|----|--------------------------|-----------|
|graphs|<|\-|<|<|Root element for graphs.|
|<|name|x|`string`|<|Graph name.|
|<|width|\-|`integer`|20-65535 (default: 900)|Graph width, in pixels. Used for preview and for pie/exploded graphs.|
|<|height|\-|`integer`|20-65535 (default: 200)|Graph height, in pixels. Used for preview and for pie/exploded graphs.|
|<|yaxismin|\-|`double`|Default: 0|Value of Y axis minimum.<br><br>Used if 'ymin\_type\_1' is FIXED.|
|<|yaxismax|\-|`double`|Default: 0|Value of Y axis maximum.<br><br>Used if 'ymax\_type\_1' is FIXED.|
|<|show\_work\_period|\-|`string`|0 - NO<br>1 - YES (default)|Highlight non-working hours.<br><br>Used by normal and stacked graphs.|
|<|show\_triggers|\-|`string`|0 - NO<br>1 - YES (default)|Display simple trigger values as a line.<br><br>Used by normal and stacked graphs.|
|<|type|\-|`string`|0 - NORMAL (default)<br>1 - STACKED<br>2 - PIE<br>3 - EXPLODED|Graph type.|
|<|show\_legend|\-|`string`|0 - NO<br>1 - YES (default)|Display graph legend.|
|<|show\_3d|\-|`string`|0 - NO (default)<br>1 - YES|Enable 3D style.<br><br>Used by pie and exploded pie graphs.|
|<|percent\_left|\-|`double`|Default:0|Show the percentile line for left axis.<br><br>Used only for normal graphs.|
|<|percent\_right|\-|`double`|Default:0|Show the percentile line for right axis.<br><br>Used only for normal graphs.|
|<|ymin\_type\_1|\-|`string`|0 - CALCULATED (default)<br>1 - FIXED<br>2 - ITEM|Minimum value of Y axis.<br><br>Used by normal and stacked graphs.|
|<|ymax\_type\_1|\-|`string`|0 - CALCULATED (default)<br>1 - FIXED<br>2 - ITEM|Maximum value of Y axis.<br><br>Used by normal and stacked graphs.|
|ymin\_item\_1|<|\-|<|<|Individual item details.<br><br>Required if 'ymin\_type\_1' is ITEM.|
|<|host|x|`string`|<|Item host.|
|<|key|x|`string`|<|Item key.|
|ymax\_item\_1|<|\-|<|<|Individual item details.<br><br>Required if 'ymax\_type\_1' is ITEM.|
|<|host|x|`string`|<|Item host.|
|<|key|x|`string`|<|Item key.|
|graph\_items|<|x|<|<|Root element for graph items.|
|<|sortorder|\-|`integer`|<|Draw order. The smaller value is drawn first. Can be used to draw lines or regions behind (or in front of) another.|
|<|drawtype|\-|`string`|0 - SINGLE\_LINE (default)<br>1 - FILLED\_REGION<br>2 - BOLD\_LINE<br>3 - DOTTED\_LINE<br>4 - DASHED\_LINE<br>5 - GRADIENT\_LINE|Draw style of the graph item.<br><br>Used only by normal graphs.|
|<|color|\-|`string`|<|Element color (6 symbols, hex).|
|<|yaxisside|\-|`string`|0 - LEFT (default)<br>1 - RIGHT|Side of the graph where the graph item's Y scale will be drawn.<br><br>Used by normal and stacked graphs.|
|<|calc\_fnc|\-|`string`|1 - MIN<br>2 - AVG (default)<br>4 - MAX<br>7 - ALL (minimum, average and maximum; used only by simple graphs)<br>9 - LAST (used only by pie and exploded pie graphs)|Data to draw if more than one value exists for an item.|
|<|type|\-|`string`|0 - SIMPLE (default)<br>2 - GRAPH\_SUM (value of the item represents the whole pie; used only by pie and exploded pie graphs)|Graph item type.|
|item|<|x|<|<|Individual item.|
|<|host|x|`string`|<|Item host.|
|<|key|x|`string`|<|Item key.|

[comment]: # ({/new-0a8a430a})

[comment]: # ({new-cae5eb15})
##### Host web scenario tags

|Element|Element property|Required|Type|Range^**[1](#footnotes)**^|Description|
|-------|----------------|--------|----|--------------------------|-----------|
|httptests|<|\-|<|<|Root element for web scenarios.|
|<|name|x|`string`|<|Web scenario name.|
|<|delay|\-|`string`|Default: 1m|Frequency of executing the web scenario. Seconds, time unit with suffix or user macro.|
|<|attempts|\-|`integer`|1-10 (default: 1)|The number of attempts for executing web scenario steps.|
|<|agent|\-|`string`|Default: Zabbix|Client agent. Zabbix will pretend to be the selected browser. This is useful when a website returns different content for different browsers.|
|<|http\_proxy|\-|`string`|<|Specify an HTTP proxy to use, using the format: `http://[username[:password]@]proxy.example.com[:port]`|
|variables|<|\-|<|<|Root element for scenario-level variables (macros) that may be used in scenario steps.|
|<|name|x|`text`|<|Variable name.|
|<|value|x|`text`|<|Variable value.|
|headers|<|\-|<|<|Root element for HTTP headers that will be sent when performing a request. Headers should be listed using the same syntax as they would appear in the HTTP protocol.|
|<|name|x|`text`|<|Header name.|
|<|value|x|`text`|<|Header value.|
|<|status|\-|`string`|0 - ENABLED (default)<br>1 - DISABLED|Web scenario status.|
|<|authentication|\-|`string`|0 - NONE (default)<br>1 - BASIC<br>2 - NTLM|Authentication method.|
|<|http\_user|\-|`string`|<|User name used for basic, HTTP or NTLM authentication.|
|<|http\_password|\-|`string`|<|Password used for basic, HTTP or NTLM authentication.|
|<|verify\_peer|\-|`string`|0 - NO (default)<br>1 - YES|Verify the SSL certificate of the web server.|
|<|verify\_host|\-|`string`|0 - NO (default)<br>1 - YES|Verify that the Common Name field or the Subject Alternate Name field of the web server certificate matches.|
|<|ssl\_cert\_file|\-|`string`|<|Name of the SSL certificate file used for client authentication (must be in PEM format).|
|<|ssl\_key\_file|\-|`string`|<|Name of the SSL private key file used for client authentication (must be in PEM format).|
|<|ssl\_key\_password|\-|`string`|<|SSL private key file password.|
|steps|<|x|<|<|Root element for web scenario steps.|
|<|name|x|`string`|<|Web scenario step name.|
|<|url|x|`string`|<|URL for monitoring.|
|query\_fields|<|\-|<|<|Root element for query fields - an array of HTTP fields that will be added to the URL when performing a request.|
|<|name|x|`string`|<|Query field name.|
|<|value|\-|`string`|<|Query field value.|
|posts|<|\-|<|<|HTTP POST variables as a string (raw post data) or as an array of HTTP fields (form field data).|
|<|name|x|`string`|<|Post field name.|
|<|value|x|`string`|<|Post field value.|
|variables|<|\-|<|<|Root element of step-level variables (macros) that should be applied after this step.<br><br>If the variable value has a 'regex:' prefix, then its value is extracted from the data returned by this step according to the regular expression pattern following the 'regex:' prefix|
|<|name|x|`string`|<|Variable name.|
|<|value|x|`string`|<|Variable value.|
|headers|<|\-|<|<|Root element for HTTP headers that will be sent when performing a request. Headers should be listed using the same syntax as they would appear in the HTTP protocol.|
|<|name|x|`string`|<|Header name.|
|<|value|x|`string`|<|Header value.|
|<|follow\_redirects|\-|`string`|0 - NO<br>1 - YES (default)|Follow HTTP redirects.|
|<|retrieve\_mode|\-|`string`|0 - BODY (default)<br>1 - HEADERS<br>2 - BOTH|HTTP response retrieve mode.|
|<|timeout|\-|`string`|Default: 15s|Timeout of step execution. Seconds, time unit with suffix or user macro.|
|<|required|\-|`string`|<|Text that must be present in the response. Ignored if empty.|
|<|status\_codes|\-|`string`|<|A comma delimited list of accepted HTTP status codes. Ignored if empty. For example: 200-201,210-299|
|tags|<|\-|<|<|Root element for web scenario tags.|
|<|tag|x|`string`|<|Tag name.|
|<|value|\-|`string`|<|Tag value.|

[comment]: # ({/new-cae5eb15})

[comment]: # ({new-869bd76e})
##### Footnotes

^**1**^ For string values, only the string will be exported (e.g.
"ZABBIX\_ACTIVE") without the numbering used in this table. The numbers
for range values (corresponding to the API values) in this table is used
for ordering only.

[comment]: # ({/new-869bd76e})
