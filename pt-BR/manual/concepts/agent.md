[comment]: # translation:outdated

[comment]: # ({new-0f9ecee9})
# - \#3 Agente

[comment]: # ({/new-0f9ecee9})

[comment]: # ({new-f5143fcc})
#### Visão geral

O agente Zabbix é instalado no dispositivo alvo da monitoração. Possui
capacidade de monitorar de monitorar ativamente os recursos e aplicações
locais (discos e partições, memória, estatísticas do processador, etc).

O agente concentra as informações locais sobre o dispositivo monitorado
para posterior envio ao servidor ou proxy Zabbix (dependendo da
configuração). Em caso de falhas (como um disco cheio ou a interrupção
de um processo) o servidor Zabbix pode alertar ativamente os
administradores do ambiente sobre o ocorrido.

Os agentes Zabbix são extremamente eficientes pois utilizam chamadas
nativas do sistema operacional para obter as informações estatísticas.

[comment]: # ({/new-f5143fcc})

[comment]: # ({new-41e6af7c})
##### Verificações passivas e ativas

Os agentes Zabbix podem executar verificações passivas ou ativas.

Em uma [verificação
passiva](/pt/manual/appendix/items/activepassive#passive_checks) o
agente responde à uma requisição de informações. O servidor ou o proxy
Zabbix requisitam o dado toda vez que é necessário (uso de CPU, memória,
disco, etc), o agente responde com o resultado do teste solicitado.

O processo de [verificação
ativa](/pt/manual/appendix/items/activepassive#active_checks) requer um
processamento mais complexo. O agente precisa primeiro receber a lista
de itens a monitorar e o intervalo entre coletas pretendido. Esta
informação vem do servidor Zabbix através de requisições periódicas do
agente.

A verificação ativa permite que o agente continue executando o perfil de
monitoração mesmo quando o servidor Zabbix está indisponível, enviando
posteriormente e de forma retroativa o resultado dos testes.

A definição se a verificação deve ocorrer de forma passiva ou ativa é
configurada através do [tipo do
item](/pt/manual/config/items/itemtypes/zabbix_agent), na interface web
do Zabbix. Um agente Zabbix pode processar itens do tipo 'Agente Zabbix'
ou "Agente Zabbix (ativo)".

[comment]: # ({/new-41e6af7c})

[comment]: # ({new-b05a4949})
#### Plataformas suportadas

O agente Zabbix é suportado por:

-   Linux
-   IBM AIX
-   FreeBSD
-   NetBSD
-   OpenBSD
-   HP-UX
-   Mac OS X
-   Solaris: 9, 10, 11
-   Windows: 2000, Server 2003, XP, Vista, Server 2008, 7

[comment]: # ({/new-b05a4949})

[comment]: # ({new-5bbb67d9})
##### Instalação

Veja as [instruções de
instalação](/pt/manual/installation/install#installing_zabbix_daemons)
para o agente.

::: noteimportant
Em geral os agentes 32bits do Zabbix conseguirão
ser executados em ambientes 64bits, mas em alguns casos poderá ocorrer
falha.
:::

[comment]: # ({/new-5bbb67d9})

[comment]: # ({c75600fd-32937b35})
##### Instalação

Veja [instalação de pacotes
](/manual/installation/install_from_packages) localpara
instruções sobre como instalar o agente Zabbix como pacote.

Veja instruções alternativas para [instalação manual](/manual/installation/install#installing_zabbix_daemons) caso não queira instalar via pacotes.

::: Aviso importante
Em geral, Agentes Zabbix 32bit irão funcionar com sistemas 64bit
, mas podem falhar em alguns casos.
:::

[comment]: # ({/c75600fd-32937b35})

[comment]: # ({new-99f37c64})
##### Controle em tempo de execução

Opções de controle em tempo de execução:

|Opção|Descrição|Alvo|
|-------|-----------|----|
|log\_level\_increase\[=<**alvo**>\]|Aumenta o nível de log, afeta todos os processos se o alvo não for especificado.|**pid** - Identificador do processo (1 a 65535)<br>**tipo do processo** - Restringe a todos os processos de determinado tipo (Ex.: poller)<br>**tipo do processo,N** - Restringe a determinado processo de um tipo específico (Ex.: poller,3)|
|log\_level\_decrease\[=<**alvo**>\]|Reduz o nível de log, afeta todos os processos se o alvo não for especificado.|^|

O PID do processo a se modificar o nível de log deverá estar entre 1 e
65535. Em ambientes com muitos processos a modificação poderá ser feita
em um processo específico.

Exemplo de utilização do controle em tempo de execução para modificar o
nível de log:

    Increase log level of all processes:
    shell> zabbix_agentd -c /usr/local/etc/zabbix_agentd.conf -R log_level_increase

    Aumenta o nível de log do segundo processo do ouvinte (listener):
    shell> zabbix_agentd -c /usr/local/etc/zabbix_agentd.conf -R log_level_increase=listener,2

    Aumenta o nível de log do processo com PID 1234:
    shell> zabbix_agentd -c /usr/local/etc/zabbix_agentd.conf -R log_level_increase=1234

    Reduz o nível de log de todas os processos de verificação ativa:
    shell> zabbix_agentd -c /usr/local/etc/zabbix_agentd.conf -R log_level_decrease="active checks"

[comment]: # ({/new-99f37c64})

[comment]: # ({new-5e83177d})
##### Processo de usuário

O agente Zabbix foi desenhado para ser executado como um processo
“não-root”. Ele pode ser executado com a permissão do usuário que o
iniciou. Neste cenário ele irá executar sem nenhum problema.

Se você tentar inicia-lo com o usuário 'root', ele alternará seu
permissionamento de execução para o usuário 'zabbix', que deverá existir
em seu ambiente. Você só poderá rodar o Servidor Zabbix como 'root' se
modificar o parâmetro 'AllowRoot' no arquivo de configuração.

[comment]: # ({/new-5e83177d})

[comment]: # ({new-f276f35a})
##### Arquivo de configuração

Veja as opções do [arquivo de
configuração](/pt/manual/appendix/config/zabbix_agentd) para detalhes
sobre sua configuração.

[comment]: # ({/new-f276f35a})

[comment]: # ({new-23b0be18})
#### Executando o agente em ambiente Microsoft Windows

Veja o manual do [agente no
Windows](/pt/manual/appendix/install/windows_agent) para detalhes sobre
como instalar, configurar e executar o agente neste sistema operacional.

Sintaxe de linha de comando do agente no Windows:

    zabbix_agentd.exe [-c arquivo-de-configuração]
    zabbix_agentd.exe [-c arquivo-de-configuração] -p
    zabbix_agentd.exe [-c arquivo-de-configuração] -t chave-do-item
    zabbix_agentd.exe [-c arquivo-de-configuração] -i [-m]
    zabbix_agentd.exe [-c arquivo-de-configuração] -d [-m]
    zabbix_agentd.exe [-c arquivo-de-configuração] -s [-m]
    zabbix_agentd.exe [-c arquivo-de-configuração] -x [-m]
    zabbix_agentd.exe -h
    zabbix_agentd.exe -V

Os parâmetros a seguir podem ser utilizados.

Options:

      -c --config <arquivo>           caminho absoluto (completo) para o arquivo de configuração (o padrão é c:\zabbix_agentd.conf)
      -h --help            apresenta o help de parâmetros
      -V --version         apresenta o número de versão
      -p --print           apresenta todos os itens (chaves) possíveis
      -t --test <chave do item> testa um item específico e retorna o resultado

Functions:

      -i --install          Instala o serviço do agente Zabbix
      -d --uninstall        Desinstala o serviço do agente Zabbis
      -s --start            Inicia o serviço do agente Zabbix
      -x --stop             Finaliza o serviço do agente Zabbix
      -m --multiple-agents  Nome do serviço com o hostname

[comment]: # ({/new-23b0be18})

[comment]: # ({new-c7c6daac})
##### Arquivo de configuração

Veja o manual do [arquivo de
configuração](/pt/manual/appendix/config/zabbix_agentd_win) para
detalhes de opções de configuração do agente Zabbix no Windows.

[comment]: # ({/new-c7c6daac})

[comment]: # ({new-fa025a89})
#### Códigos de saída

Antes da versão 2.2 do Zabbix o agente retornava 0 em caso de sucesso e
255 em caso de falha. A partir desta versão o agente passou a retornar 0
para sucesso e 1 para falha.

[comment]: # ({/new-fa025a89})







[comment]: # ({new-6ad4cc3c})
##### Runtime control

With runtime control options you may change the log level of agent
processes.

|Option|Description|Target|
|------|-----------|------|
|log\_level\_increase\[=<target>\]|Increase log level.<br>If target is not specified, all processes are affected.|Target can be specified as:<br>**process type** - all processes of specified type (e.g., listener)<br>See all [agent process types](#agent_process_types).<br>**process type,N** - process type and number (e.g., listener,3)<br>**pid** - process identifier (1 to 65535). For larger values specify target as 'process-type,N'.|
|log\_level\_decrease\[=<target>\]|Decrease log level.<br>If target is not specified, all processes are affected.|^|
|userparameter\_reload|Reload user parameters from the current configuration file.<br>Note that UserParameter is the only agent configuration option that will be reloaded.|<|

Examples:

-   increasing log level of all processes
-   increasing log level of the third listener process
-   increasing log level of process with PID 1234
-   decreasing log level of all active check processes

```{=html}
<!-- -->
```
    shell> zabbix_agentd -R log_level_increase
    shell> zabbix_agentd -R log_level_increase=listener,3
    shell> zabbix_agentd -R log_level_increase=1234
    shell> zabbix_agentd -R log_level_decrease="active checks"

::: noteclassic
Runtime control is not supported on OpenBSD, NetBSD and
Windows.
:::

[comment]: # ({/new-6ad4cc3c})

[comment]: # ({new-929667fd})
#### Agent process types

-   `active checks` - process for performing active checks
-   `collector` - process for data collection
-   `listener` - process for listening to passive checks

The agent log file can be used to observe these process types.

[comment]: # ({/new-929667fd})

[comment]: # ({new-18ab16f1})
#### Process user

Zabbix agent on UNIX is designed to run as a non-root user. It will run
as whatever non-root user it is started as. So you can run agent as any
non-root user without any issues.

If you will try to run it as 'root', it will switch to a hardcoded
'zabbix' user, which must be present on your system. You can only run
agent as 'root' if you modify the 'AllowRoot' parameter in the agent
configuration file accordingly.

[comment]: # ({/new-18ab16f1})

[comment]: # ({new-fc6b2c61})
#### Configuration file

For details on configuring Zabbix agent see the configuration file
options for [zabbix\_agentd](/manual/appendix/config/zabbix_agentd) or
[Windows agent](/manual/appendix/config/zabbix_agentd_win).

[comment]: # ({/new-fc6b2c61})

[comment]: # ({new-7c3bd34c})
#### Locale

Note that the agent requires a UTF-8 locale so that some textual agent
items can return the expected content. Most modern Unix-like systems
have a UTF-8 locale as default, however, there are some systems where
that may need to be set specifically.

[comment]: # ({/new-7c3bd34c})

[comment]: # ({new-5581c546})
#### Exit code

Before version 2.2 Zabbix agent returned 0 in case of successful exit
and 255 in case of failure. Starting from version 2.2 and higher Zabbix
agent returns 0 in case of successful exit and 1 in case of failure.

[comment]: # ({/new-5581c546})
