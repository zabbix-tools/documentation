[comment]: # translation:outdated

[comment]: # ({new-00e36b2a})
# - \#5 Java gateway

[comment]: # ({/new-00e36b2a})

[comment]: # ({new-02637e76})
#### Visão geral

O Zabbix 2.0 inovou com o suporte nativo ao monitoramento de aplicações
Java através de JMX, este suporte foi adicionado através do componente
"Zabbix Java Gateway". Ele é um processo de background (daemon) escrito
em Java. Quando o Zabbix Server precisa coletar um item (dado) através
de um contador JMX em um host, ele solicita ao Zabbix Java Gateway, que
utiliza a [API de gerência
JMX](http://java.sun.com/javase/technologies/core/mntr-mgmt/javamanagement/)
para requisitar da aplicação o dado de interesse. A aplicação não
precisa de softwares adicionais, apenas necessita ter sido iniciada com
a opção `-Dcom.sun.management.jmxremote` no momento de sua inicialização
(linha de comando).

O Zabbix Java Gateway aceita conexões oriundas do Zabbix Server e do
Zabbix Proxy e só pode ser utilizado como um "proxy passivo". Ao
contrário do que ocorre com um Zabbix Proxy o Zabbix Java Gateway pode
estar atrás de outro proxy (um Zabbix Proxy). O acesso a cada Zabbix
Java Gateway é configurado diretamente no arquivo de configuração do
Zabbix Server ou do Zabbix Proxy e só pode existir um Zabbix Java
Gateway por Zabbix Server ou Zabbix Proxy. Se você precisar de ter mais
de um Zabbix Java Gateway em um mesmo ambiente da solução Zabbix você
precisará configurar um novo Zabbix Proxy para cada Zabbix Java Gateway.
Se um host possuir itens do tipo **JMX agent** e itens de outros tipos,
apenas os itens do tipo **JMX agent** serão solicitados ao Zabbix Java
Gateway.

O Zabbix Java Gateway não faz cache de nenhum valor coletado.

O Zabbix Server ou Zabbix Proxy tem um processo específico para se
conectar ao Zabbix Java Gateway, controlado pela opção
**StartJavaPollers**. Internamente o Zabbix Java Gateway inicia
múltiplas threads, controladas pela opção **START\_POLLERS**. No lado do
servidor, se a conexão demorar mais do que o limite em segundos da opção
**Timeout**, a requisição será terminada (abortada), mas o Zabbix Java
Gateway continuará aguardando pela coleta do contador JMX. Para resolver
isso, desde o Zabbix 2.0.15, Zabbix 2.2.10 e Zabbix 2.4.5 foi adicionada
a opção **TIMEOUT** no Zabbix Java Gateway que permite definir o tempo
máximo para as operações remotas do JMX.

O Zabbix Server ou o Zabbix Proxy irá agrupar as requisições em uma
única requisição JMX, sempre que possível (é afetado pelos intervalos
entre coletas), e enviar para o Zabbix Java Gateway em uma única conexão
visando obter melhor performance.

É recomendável configurar a opção **StartJavaPollers** com valor menor
ou igual à opção **START\_POLLERS**, de outra forma existirão situações
onde não existirão trheads disponíveis para atender às requisições.

A sessão abaixo descreve como obter e como executar o Zabbix Java
Gateway, como configurar o Zabbix Server (ou Zabbix Proxy) para usar o
Zabbix Java Gateway para monitoração JMX, e como configurar os itens do
Zabbix em sua interface web para coletar um contador JMX específico.

[comment]: # ({/new-02637e76})

[comment]: # ({new-05197937})
When an item has to be updated over Java gateway, Zabbix server or proxy
will connect to the Java gateway and request the value, which Java
gateway in turn retrieves and passes back to the server or proxy. As
such, Java gateway does not cache any values.

Zabbix server or proxy has a specific type of processes that connect to
Java gateway, controlled by the option **StartJavaPollers**. Internally,
Java gateway starts multiple threads, controlled by the
**START\_POLLERS** [option](/manual/appendix/config/zabbix_java). On the
server side, if a connection takes more than **Timeout** seconds, it
will be terminated, but Java gateway might still be busy retrieving
value from the JMX counter. To solve this, there is the **TIMEOUT**
option in Java gateway that allows to set timeout for JMX network
operations.

[comment]: # ({/new-05197937})

[comment]: # ({new-475ef799})

Zabbix server or proxy will try to pool requests to a single JMX target
together as much as possible (affected by item intervals) and send them
to the Java gateway in a single connection for better performance.

It is suggested to have **StartJavaPollers** less than or equal to
**START\_POLLERS**, otherwise there might be situations when no threads
are available in the Java gateway to service incoming requests; in such
a case Java gateway uses ThreadPoolExecutor.CallerRunsPolicy, meaning
that the main thread will service the incoming request and temporarily[label](https://git.zabbix.com/projects/WEB/repos/documentation/compare)
will not accept any new requests.

If you are trying to monitor Wildfly-based Java applications with Zabbix Java gateway, please install the latest jboss-client.jar available on the [Wildfly download page](https://www.wildfly.org/downloads/).

[comment]: # ({/new-475ef799})

[comment]: # ({f579e8ee-d8d54db7})
#### Obtendo o gateway Java

Você pode instalar o gateway Java a partir das fontes ou pacotes
baixado do [Zabbix website](http://www.zabbix.com/download.php).

Usando os links abaixo, você pode acessar informações sobre como obter e executar Zabbix Java gateway, como configurar o servidor Zabbix (ou proxy Zabbix) para utilizr Zabbix Java gateway para monitoramento JMX, e como configurar os items Zabbix  com contadores correspondentes ao  JMX .

|Instalação a partir de |Instruções|Instruções|
|-----------------|------------|------------|
|*Fontes*|[Instalação](/manual/installation/install#installing_java_gateway)|[Configuração](/manual/concepts/java/from_sources)|
|*pacotes RHEL/CentOS *|[Instalação](/manual/installation/install_from_packages/rhel_centos#java_gateway_installation)|[Configuração](/manual/concepts/java/from_rhel_centos)|
|*pacotes Debian/Ubuntu *|[Instalação](/manual/installation/install_from_packages/debian_ubuntu#java_gateway_installation)|[Configuração](/manual/concepts/java/from_debian_ubuntu)|

[comment]: # ({/f579e8ee-d8d54db7})
