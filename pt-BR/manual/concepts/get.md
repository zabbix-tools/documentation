[comment]: # translation:outdated

[comment]: # ({new-14379c17})
# - \#7 Get

[comment]: # ({/new-14379c17})

[comment]: # ({new-f50cbd4e})
#### Visão geral

O Zabbix Get é um utilitário de linha de comando que pode ser utilizado
para se comunicar com o agente de monitoração do Zabbix e requisitar um
dado do agente.

Este utilitário é normalmente utilizado em ações de desenvolvimento ou
debug de chaves no agente.

[comment]: # ({/new-f50cbd4e})

[comment]: # ({new-c625ea25})
#### Executando o Zabbix Get

Exemplo de utilização do Zabbix Get em ambiente UNIX para recuperar as
informações de carga:

    shell> cd bin
    shell> ./zabbix_get -s 127.0.0.1 -p 10050 -k system.cpu.load[all,avg1]

Exemplo de utilização do Zabbix Get para recuperar um texto de um site:

    shell> cd bin
    shell> ./zabbix_get -s 192.168.1.1 -p 10050 -k "web.page.regexp[www.zabbix.com,,,\"USA: ([a-zA-Z0-9.-]+)\",,\1]"

Observe que a chave do item contêm espaços e aspas e, por isso, a chave
tem que estar entre aspas duplas e as aspas internas estão escapadas. As
aspas externas não são partes da chave, elas são eliminadas pelo shell
ao enviar os dados para o Agente.

O Zabbix Get aceita os sequintes parâmetros:

      -s --host <nome ou IP>      Especifica o DNS ou IP do host a ser consultado.
      -p --port <porta>          Especifica o número da porta em execução pelo agente (normalmente a porta **10050**).
      -I --source-address <IP> Especifica o IP de origem (caso existam múltiplos IPs na máquina que origina a requisição).
      -k --key <chave>              Especifica a chave de item que se deseja o valor.
      -h --help                        Exibe este help.
      -V --version                     Exibe a versão do comando.

O Zabbix Get pode ser executado de forma similar no Windows:

    zabbix_get.exe [opções]

[comment]: # ({/new-c625ea25})
