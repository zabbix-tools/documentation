[comment]: # translation:outdated

[comment]: # ({new-f9458f4e})
# - \#4 Proxy

[comment]: # ({/new-f9458f4e})

[comment]: # ({new-05d8de8a})
#### Visão geral

O Zabbix Proxy é um processo que pode receber dados de um ou mais
dispositivos monitorados e enviar ao Zabbix Server, basicamente ele
funciona em nome do Zabbix Server (na visão do agente monitorado o Proxy
passa a ser o Zabbix Server). Todo os dados recebidos são armazenados
temporariamente (buferizados), transferidos ao Zabbix Server que o
Zabbix Proxy pertencer, sendo excluídos na sequência do armazenamento
temporário do Proxy.

A utilização deste componente é opcional, mas normalmente é muito
benéfica pois distribui a carga de monitoração normalmente atribuída ao
Zabbix Server. Se toda a coleta de dados for feita através de Proxies o
uso de CPU e de I/O no servidor responsável pelo Zabbix Server reduz
significativamente.

O Zabbix Proxy é a solução ideal para a monitoração centralizada de
localidades geograficamente dispersas e para redes gerenciadas
remotamente.

O Zabbix Proxy requer um banco de dados em separado (normalmente um
SQLite).

::: noteimportant
Observe que o Proxy suporta SQLite, MySQL e
PostgreSQL. O uso de Oracle ou IBM DB2 neste componente é uma escolha
com riscos e limitações seus, exemplos podem ser encontrados em [retorno
de valores](/pt/manual/discovery/low_level_discovery#overview) regras de
autobusca.
:::

Veja também: [Usando Proxies em ambientes
distribuídos](/pt/manual/distributed_monitoring/proxies)

[comment]: # ({/new-05d8de8a})

[comment]: # ({new-709824a5})
#### Processo do Proxy

O Zabbix Proxy é executado como um processo de background (Daemon). O
proxy pode ser iniciado ao executar:

    shell> cd sbin
    shell> ./zabbix_proxy

Você pode utilizar alguns parâmetros com o Zabbix Proxy:

    -c --config <arquivo>           caminho absoluto (completo) para o arquivo de configuração (o padrão é /etc/zabbix/zabbix_proxy.conf)
    -R --runtime-control <opção>    executa funções administrativas
    -h --help                       apresenta o help de parâmetros
    -V --version                    apresenta o número de versão

::: noteclassic
O controle em tempo de execução não é suportado em OpenBSD e
em NetBSD.
:::

Exemplos de linha de comando com parâmetros:

    shell> zabbix_proxy -c /usr/local/etc/zabbix_proxy.conf
    shell> zabbix_proxy --help
    shell> zabbix_proxy -V

[comment]: # ({/new-709824a5})

[comment]: # ({new-a0a6c8d1})
##### Controle em tempo de execução

Opções do controle em tempo de execução:

|Opção|Descrição|Objetivo|
|-------|-----------|--------|
|config\_cache\_reload|Atualiza o cache de configuração. O comando é ignorado se o cache já estiver atualizado.|<|
|housekeeper\_execute|Inicia o processo de limpeza de dados. Ignorado se o processo já estiver em progresso|<|
|log\_level\_increase\[=<**alvo**>\]|Aumenta o nível de log, afeta todos os processos se o alvo não for especificado.|**pid** - Identificador do processo (1 a 65535)<br>**tipo do processo** - Restringe a todos os processos de determinado tipo (Ex.: poller)<br>**tipo do processo,N** - Restringe a determinado processo de um tipo específico (Ex.: poller,3)|
|log\_level\_decrease\[=<**alvo**>\]|Reduz o nível de log, afeta todos os processos se o alvo não for especificado.|^|

O PID do processo a se modificar o nível de log deverá estar entre 1 e
65535. Em ambientes com muitos processos a modificação poderá ser feita
em um processo específico, inclusive para a alteração de nível de log de
apenas um deles.

Exemplo de utilização do controle em tempo de execução para recarregar o
cache de configuração do Zabbix Proxy:

    shell> zabbix_proxy -c /usr/local/etc/zabbix_proxy.conf -R config_cache_reload

Exemplo de utilização do controle em tempo de execução para iniciar o
housekeeper

    shell> zabbix_proxy -c /usr/local/etc/zabbix_proxy.conf -R housekeeper_execute

Exemplos de utilização do controle em tempo de execução para modificar o
nível de log:

    Aumenta o nível de log de todos os processos:
    shell> zabbix_proxy -c /usr/local/etc/zabbix_proxy.conf -R log_level_increase

    Aumenta o nível de log do segundo processo de pooler:
    shell> zabbix_proxy -c /usr/local/etc/zabbix_proxy.conf -R log_level_increase=poller,2

    Aumenta o nível de log do processo com PID 1234:
    shell> zabbix_proxy -c /usr/local/etc/zabbix_proxy.conf -R log_level_increase=1234

    Diminui o nível de log de todos os processos do pooler HTTP:
    shell> zabbix_proxy -c /usr/local/etc/zabbix_proxy.conf -R log_level_decrease="http poller"

[comment]: # ({/new-a0a6c8d1})

[comment]: # ({new-0b35181b})
##### Processo de usuário

O Zabbix Proxy foi desenhado para ser executado como um processo
"não-root". Ele pode ser executado com a permissão do usuário que o
iniciou. Neste cenário ele irá executar sem nenhum problema.

Se você tentar inicia-lo com o usuário 'root', ele irá alternar seu
permissionamento de execução para o usuário 'zabbix', que deverá
[existir](/pt/manual/installation/install) em seu ambiente. Você só
poderá rodar o Zabbix Proxy como 'root' se modificar o parâmetro
'AllowRoot' no arquivo de configuração.

[comment]: # ({/new-0b35181b})

[comment]: # ({new-ee4dd6f0})
##### Arquivo de configuração

Veja as opções do [arquivo de
configuração](/pt/manual/appendix/config/zabbix_proxy) para detalhes
sobre sua configuração.

[comment]: # ({/new-ee4dd6f0})

[comment]: # ({new-a9af55d3})
##### Scripts de inicialização

Os scripts são utilizados para iniciar automaticamente os processos do
Zabbix Proxy durante o processo de inicialização e finalização da
máquina. Atualmente (29/11/15) estes scripts não vem junto com o código
fonte da solução mas são facilmente obtidos através da cópia e edição
dos scripts do Zabbix Server localizados no diretório *misc/init.d* do
código fonte da solução.

[comment]: # ({/new-a9af55d3})

[comment]: # ({new-441f5ec7})
#### Plataformas suportadas

Devido aos requisitos de segurança e a natureza de missão crítica do
funcionamento do Zabbix Proxy, o UNIX é o único sistema operacional que
pode entregar de forma consistente o desempenho, tolerância a falhas e
resiliência necessários. O Zabbix opera como uma das soluções líderes de
mercado.

O Zabbix Proxy é testado nas seguintes plataformas:

-   Linux
-   Solaris
-   AIX
-   HP-UX
-   Mac OS X
-   FreeBSD
-   OpenBSD
-   NetBSD
-   SCO Open Server
-   Tru64/OSF1

::: noteclassic
O Zabbix pode funcionar em outros sistemas operacionais
baseados no UNIX.
:::

[comment]: # ({/new-441f5ec7})




[comment]: # ({new-2cd46511})
#### Proxy process types

-   `availability manager` - process for host availability updates
-   `configuration syncer` - process for managing in-memory cache of
    configuration data
-   `data sender` - proxy data sender
-   `discoverer` - process for discovery of devices
-   `heartbeat sender` - proxy heartbeat sender
-   `history poller` - process for handling calculated, aggregated and
    internal checks requiring a database connection
-   `history syncer` - history DB writer
-   `housekeeper` - process for removal of old historical data
-   `http poller` - web monitoring poller
-   `icmp pinger` - poller for icmpping checks
-   `ipmi manager` - IPMI poller manager
-   `ipmi poller` - poller for IPMI checks
-   `java poller` - poller for Java checks
-   `poller` - normal poller for passive checks
-   `preprocessing manager` - manager of preprocessing tasks
-   `preprocessing worker` - process for data preprocessing
-   `self-monitoring` - process for collecting internal server
    statistics
-   `snmp trapper` - trapper for SNMP traps
-   `task manager` - process for remote execution of tasks requested by
    other components (e.g. close problem, acknowledge problem, check
    item value now, remote command functionality)
-   `trapper` - trapper for active checks, traps, proxy communication
-   `unreachable poller` - poller for unreachable devices
-   `vmware collector` - VMware data collector responsible for data
    gathering from VMware services

The proxy log file can be used to observe these process types.

Various types of Zabbix proxy processes can be monitored using the
**zabbix\[process,<type>,<mode>,<state>\]** internal
[item](/manual/config/items/itemtypes/internal).

[comment]: # ({/new-2cd46511})

[comment]: # ({new-087c822f})
#### Supported platforms

Zabbix proxy runs on the same list of
[server\#supported platforms](/manual/concepts/server#supported platforms)
as Zabbix server.

[comment]: # ({/new-087c822f})

[comment]: # ({new-c703b792})
#### Locale

Note that the proxy requires a UTF-8 locale so that some textual items
can be interpreted correctly. Most modern Unix-like systems have a UTF-8
locale as default, however, there are some systems where that may need
to be set specifically.

[comment]: # ({/new-c703b792})
