[comment]: # translation:outdated

[comment]: # ({new-43ef458c})
# - \#6 Sender

[comment]: # ({/new-43ef458c})

[comment]: # ({new-836aa47c})
#### Visão geral

O Zabbix Sender é um utilitário de linha de comando que pode ser
utilizado para enviar dados para o Zabbix Server.

Situações usuais de utilização:

     * Traps de inicio ou finalização de scripts
     * Envio de métricas de negócio diretamente a partir dos sistemas que os hospedam (sem coleta periódica)
     * Envio de traps de incidentes não monitoráveis diretamente pelo Zabbix

[comment]: # ({/new-836aa47c})

[comment]: # ({new-fce06a66})
#### Executando o Zabbix Sender

Exemplo de utilização no UNIX:

    shell> cd bin
    shell> ./zabbix_sender -z zabbix -s "Linux DB3" -k db.connections -o 43

Onde:

-   z - IP ou nome do Zabbix Server que receberá o dado
-   s - Nome técnico do host monitorado pelo Zabbix (deverá estar igual
    ao registrado na interface web do Zabbix, inclusive maiúsculas e
    minúsculas)
-   k - Chave do item
-   o - Valor a enviar

::: noteimportant
As opções que contiverem espaços deverão estar
entre aspas duplas.
:::

Veja o [manual do Zabbix Sender](/pt/manpages/zabbix_sender) para
maiores informações.

O Zabbix Sender aceita strings com a codificação UTF-8 (tanto em
ambientes UNIX quanto em ambientes Windows).

De forma similar à execução no UNIX, ele poderá ser executado em
ambientes Windows:

    zabbix_sender.exe [options]

Desde a versão 1.8.4 o `zabbix_sender` foi otimizado para cenários que
exigem requisitos de tempo real e ele passou a suportar o envio de
múltiplos valores em uma única conexão com o Zabbix Server. Valores com
intervalos de 0,2 segundos podem ser adicionados na mesma pilha,
entretanto, o agrupamento máximo continua sendo de 1 segundo.

::: noteclassic
O Zabbix Sender irá terminar sua execução se um parâmetro
inválido (que não siga a notação *parameter=value* ) for especificado em
seu arquivo de configuração.
:::

[comment]: # ({/new-fce06a66})
