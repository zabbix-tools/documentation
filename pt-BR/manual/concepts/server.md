[comment]: # translation:outdated

[comment]: # ({new-cbb57ea2})
# - \#2 Servidor

[comment]: # ({/new-cbb57ea2})

[comment]: # ({new-0e549571})
#### Visão geral

O Zabbix Server é o componente central da solução.

O servidor gerencia a coleta e recebimento de dados, calcula o estado
das triggers, envia notificações aos usuários. Ele é o componente para o
qual os agentes e proxies enviam dados sobre a disponibilidade,
performance e integridade dos sistemas monitorados. O servidor também
pode executar por sí só verificações remotas nos dispositivos
monitorados, estas verificações ocorrem quando se utiliza itens do tipo
"verificação simples".

O servidor gerencia o repositório central de configuração, estatísticas
e armazenamento de dados operacionais, é ele quem irá alertar os
administradores quando os incidentes ocorrerem.

As funcionalidades básicas de uma solução de monitoração baseada em
Zabbix é distribuida em três componentes: Zabbix Server, interface web e
banco de dados (SGDB).

Todas as informações de configuração da monitoração são armazenadas no
banco de dados, tanto o Servidor quanto a Interface Web do Zabbix
interagem com o SGBD. Por exemplo, quando você utiliza a interface web
(ou a API) para adicionar itens, eles são salvos em uma tabela do SGDB.
Em paralelo a isso o Zabbix Server, uma vez a cada minuto, irá buscar,
na tabela de itens, a lista de itens que deverão ser monitorados. É por
isso que pode demorar até dois minutos para que uma modificação feita na
Interface Web comece a produzir efeitos na tela de dados recentes.

[comment]: # ({/new-0e549571})

[comment]: # ({new-c20247df})
#### Processo do Servidor

O Zabbix Server é executado como um processo de segundo plano (daemon).
O exemplo abaixo demonstra uma das formas de inicia-lo:

    shell> cd sbin
    shell> ./zabbix_server

Você pode utilizar alguns parâmetros com o Zabbix Server:

    -c --config <arquivo>           caminho absoluto (completo) para o arquivo de configuração (o padrão é /etc/zabbix/zabbix_server.conf)
    -R --runtime-control <opção>    executa funções administrativas
    -h --help                       apresenta o help de parâmetros
    -V --version                    apresenta o número de versão

::: noteclassic
O controle em tempo de execução não é suportado em OpenBSD e
em NetBSD.
:::

Exemplos de linha de comando com parâmetros:

    shell> zabbix_server -c /usr/local/etc/zabbix_server.conf
    shell> zabbix_server --help
    shell> zabbix_server -V

[comment]: # ({/new-c20247df})

[comment]: # ({new-1314fd6f})
##### Controle em tempo de execução

Opções do controle em tempo de execução:

|Opção|Descrição|Objetivo|
|-------|-----------|--------|
|config\_cache\_reload|Atualiza o cache de configuração. O comando é ignorado se o cache já estiver atualizado.|<|
|log\_level\_increase\[=<**alvo**>\]|Aumenta o nível de log, afeta todos os processos se o alvo não for especificado.|**pid** - Identificador do processo (1 a 65535)<br>**tipo do processo** - Restringe a todos os processos de determinado tipo (Ex.: poller)<br>**tipo do processo,N** - Restringe a determinado processo de um tipo específico (Ex.: poller,3)|
|log\_level\_decrease\[=<**alvo**>\]|Reduz o nível de log, afeta todos os processos se o alvo não for especificado.|^|

O PID do processo a se modificar o nível de log deverá estar entre 1 e
65535. Em ambientes com muitos processos a modificação poderá ser feita
em um processo específico.

Exemplo de utilização do controle em tempo de execução para recarregar o
cache de configuração do Zabbix Server:

    shell> zabbix_server -c /usr/local/etc/zabbix_server.conf -R config_cache_reload

Exemplos de utilização do controle em tempo de execução para modificar o
nível de log:

    Aumenta o nível de log de todos os processos:
    shell> zabbix_server -c /usr/local/etc/zabbix_server.conf -R log_level_increase

    Aumenta o nível de log do segundo processo de pooler:
    shell> zabbix_server -c /usr/local/etc/zabbix_server.conf -R log_level_increase=poller,2

    Aumenta o nível de log do processo com PID 1234:
    shell> zabbix_server -c /usr/local/etc/zabbix_server.conf -R log_level_increase=1234

    Diminui o nível de log de todos os processos do pooler HTTP:
    shell> zabbix_server -c /usr/local/etc/zabbix_server.conf -R log_level_decrease="http poller"

[comment]: # ({/new-1314fd6f})

[comment]: # ({new-bbb4c5d8})
##### Processo de usuário

O Zabbix Server foi desenhado para ser executado como um processo
"não-root". Ele pode ser executado com a permissão do usuário que o
iniciou. Neste cenário ele irá executar sem nenhum problema.

Se você tentar inicia-lo com o usuário 'root', ele irá alternar seu
permissionamento de execução para o usuário 'zabbix', que deverá
[existir](/pt/manual/installation/install) em seu ambiente. Você só
poderá rodar o Zabbix Server como 'root' se modificar o parâmetro
'AllowRoot' no arquivo de configuração.

Se o Servidor e o [Agente](agent) Zabbix estiverem em execução na mesma
máquina, recomenda-se o uso de usuários de sistema operacional
diferentes pois o agente. Esta recomendação se deve pois, tendo o mesmo
permissionamento, o Agente poderá consultar o arquivo de configuração do
servidor e obter dados sensíveis (como a senha do banco de dados
Zabbix).

[comment]: # ({/new-bbb4c5d8})

[comment]: # ({new-a339702b})
##### Arquivo de configuração

Veja as opões do [arquivo de
configuração](/pt/manual/appendix/config/zabbix_server) para detalhes
sobre sua configuração.

[comment]: # ({/new-a339702b})

[comment]: # ({new-1cb7f51c})
Example of using runtime control to reload the server configuration
cache:

    shell> zabbix_server -c /usr/local/etc/zabbix_server.conf -R config_cache_reload

Examples of using runtime control to gather diagnostic information:

    Gather all available diagnostic information in the server log file:
    shell> zabbix_server -R diaginfo

    Gather history cache statistics in the server log file:
    shell> zabbix_server -R diaginfo=historycache

Example of using runtime control to reload the SNMP cache:

    shell> zabbix_server -R snmp_cache_reload  

Example of using runtime control to trigger execution of housekeeper:

    shell> zabbix_server -c /usr/local/etc/zabbix_server.conf -R housekeeper_execute

Examples of using runtime control to change log level:

    Increase log level of all processes:
    shell> zabbix_server -c /usr/local/etc/zabbix_server.conf -R log_level_increase

    Increase log level of second poller process:
    shell> zabbix_server -c /usr/local/etc/zabbix_server.conf -R log_level_increase=poller,2

    Increase log level of process with PID 1234:
    shell> zabbix_server -c /usr/local/etc/zabbix_server.conf -R log_level_increase=1234

    Decrease log level of all http poller processes:
    shell> zabbix_server -c /usr/local/etc/zabbix_server.conf -R log_level_decrease="http poller"

Example of setting the HA failover delay to the minimum of 10 seconds:

    shell> zabbix_server -R ha_set_failover_delay=10s

[comment]: # ({/new-1cb7f51c})

[comment]: # ({new-b4f10179})
##### Scripts de inicialização

Os scripts são utilizados para iniciar automaticamente os processos do
Zabbix Server durante o processo de inicialização e finalização da
máquina. Tais scripts podem ser localizados no diretório *misc/init.d*
do código fonte da solução.

[comment]: # ({/new-b4f10179})

[comment]: # ({new-0a81f475})
#### Plataformas suportadas

Devido aos requisitos de segurança e a natureza de missão crítica do
funcionamento do Zabbix Server, o UNIX é o único sistema operacional que
pode entregar de forma consistente o desempenho, tolerância a falhas e
resiliência necessários. O Zabbix opera como uma das soluções líderes de
mercado.

O Zabbix Server é testado nas seguintes plataformas:

-   Linux
-   Solaris
-   AIX
-   HP-UX
-   Mac OS X
-   FreeBSD
-   OpenBSD
-   NetBSD
-   SCO Open Server
-   Tru64/OSF1

::: noteclassic
O Zabbix pode funcionar em outros sistemas operacionais
baseados no UNIX.
:::

[comment]: # ({/new-0a81f475})





[comment]: # ({new-49247ffc})
##### Start-up scripts

The scripts are used to automatically start/stop Zabbix processes during
system's start-up/shutdown. The scripts are located under directory
misc/init.d.

[comment]: # ({/new-49247ffc})

[comment]: # ({new-9f05badb})
#### Server process types

-   `alert manager` - alert queue manager
-   `alert syncer` - alert DB writer
-   `alerter` - process for sending notifications
-   `availability manager` - process for host availability updates
-   `configuration syncer` - process for managing in-memory cache of
    configuration data
-   `discoverer` - process for discovery of devices
-   `escalator` - process for escalation of actions
-   `history poller` - process for handling calculated, aggregated and
    internal checks requiring a database connection
-   `history syncer` - history DB writer
-   `housekeeper` - process for removal of old historical data
-   `http poller` - web monitoring poller
-   `icmp pinger` - poller for icmpping checks
-   `ipmi manager` - IPMI poller manager
-   `ipmi poller` - poller for IPMI checks
-   `java poller` - poller for Java checks
-   `lld manager` - manager process of low-level discovery tasks
-   `lld worker` - worker process of low-level discovery tasks
-   `poller` - normal poller for passive checks
-   `preprocessing manager` - manager of preprocessing tasks
-   `preprocessing worker` - process for data preprocessing
-   `problem housekeeper` - process for removing problems of deleted
    triggers
-   `proxy poller` - poller for passive proxies
-   `report manager`- manager of scheduled report generation tasks
-   `report writer` - process for generating scheduled reports
-   `self-monitoring` - process for collecting internal server
    statistics
-   `snmp trapper` - trapper for SNMP traps
-   `task manager` - process for remote execution of tasks requested by
    other components (e.g. close problem, acknowledge problem, check
    item value now, remote command functionality)
-   `timer` - timer for processing maintenances
-   `trapper` - trapper for active checks, traps, proxy communication
-   `unreachable poller` - poller for unreachable devices
-   `vmware collector` - VMware data collector responsible for data
    gathering from VMware services

The server log file can be used to observe these process types.

Various types of Zabbix server processes can be monitored using the
**zabbix\[process,<type>,<mode>,<state>\]** internal
[item](/manual/config/items/itemtypes/internal).

[comment]: # ({/new-9f05badb})

[comment]: # ({new-cdd68340})
#### Supported platforms

Due to the security requirements and mission-critical nature of server
operation, UNIX is the only operating system that can consistently
deliver the necessary performance, fault tolerance and resilience.
Zabbix operates on market leading versions.

Zabbix server is tested on the following platforms:

-   Linux
-   Solaris
-   AIX
-   HP-UX
-   Mac OS X
-   FreeBSD
-   OpenBSD
-   NetBSD
-   SCO Open Server
-   Tru64/OSF1

::: noteclassic
Zabbix may work on other Unix-like operating systems as
well.
:::

[comment]: # ({/new-cdd68340})

[comment]: # ({new-982e2546})
#### Locale

Note that the server requires a UTF-8 locale so that some textual items
can be interpreted correctly. Most modern Unix-like systems have a UTF-8
locale as default, however, there are some systems where that may need
to be set specifically.

[comment]: # ({/new-982e2546})
