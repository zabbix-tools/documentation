[comment]: # translation:outdated

[comment]: # ({new-92f30379})
# 18. API

[comment]: # ({/new-92f30379})

[comment]: # ({new-13405b49})
### Visão Geral

A API do Zabbix permite que você, de forma programática, receba e
modifica as configurações do Zabbix, além de prover acesso a dados
históricos e das médias. Ela pode ser utilizada, por exemplo, para:

-   Criar novas aplicações para trabalhar com o Zabbix;
-   Integrar o Zabbix com softwares de terceiros;
-   Automatizar tarefas rotineiras.

A API é um serviço web fornecido em conjunto com a Interface Web do
Zabbix. Esta API utiliza JSON, em sua versão 2, e duas formas de
trabalho:

-   A API é separada em dois conjuntos de métodos;
-   As requisições e respostas entre os clientes são codificadas no
    formato JSON.

Maiores informações sobre o protocolo JSON podem ser obtidas em
[JSON-RPC 2.0 specification](http://www.jsonrpc.org/specification) e na
[página do formato JSON](http://json.org/).

[comment]: # ({/new-13405b49})

[comment]: # ({new-9cb56d09})
### Estrutura

A API consiste em uma quantidade de métodos que são nominalmente
separados como APIs individuais. Cada um destes métodos executa uma
tarefa específica. Por exemplo, o método `host.create` pertence à API
*host* e é utilizada para criar novos hosts. Historicamente as APIs são
referidas como "classes".

::: notetip
A maioria das APIs possuem quatro métodos: `get`,
`create`, `update` e `delete` para, respectivamente, recuperar, criar,
atualizar e excluir dados, mas algumas das APIs podem possuir métodos
totalmente diferentes.
:::

[comment]: # ({/new-9cb56d09})

[comment]: # ({new-9cb2f5ca})
### Executando as requisições

Uma vez que você tenha configurado a interface web do Zabbix você pode
utilizar-se de requisições HTTP para invocar a API. Para fazer isso você
precisa enviar requisições no padrão HTTP POST para o arquivo
`api_jsonrpc.php` localizado na raiz da interface web. Por exemplo, se
sua instalação do Zabbix está no endereço *http://company.com/zabbix*, a
requisição HTTP para o método `apiinfo.version` pode ser algo como a
seguir:

``` {.http}
POST http://company.com/zabbix/api_jsonrpc.php HTTP/1.1
Content-Type: application/json-rpc

{"jsonrpc":"2.0","method":"apiinfo.version","id":1,"auth":null,"params":{}}
```

A requisição precisa possuir o cabeçalho com o `Content-Type` definido
com um destes valores : `application/json-rpc`, `application/json` ou
`application/jsonrequest`.

::: notetip
Você pode utilizar qualquer cliente HTTP ou ferramenta
de teste JSON-RPC para requisitar os métodos da API manualmente, mas nós
recomendamos o uso de uma das APIs [mantidas pela
comunidade](http://zabbix.org/wiki/Docs/api/libraries).
:::

[comment]: # ({/new-9cb2f5ca})

[comment]: # ({new-ec5a9f60})
### Exemplo de fluxo de trabalho

Esta seção irá demonstrar com mais detalhes, através de exemplos, como
trabalhar com a API.

[comment]: # ({/new-ec5a9f60})

[comment]: # ({new-ef3f5841})
#### Autenticação

Antes de você acessar qualquer dado dentro do Zabbix você precisa se
autenticar e obter um token de autenticação. Isso pode ser feito
utilizando o método [user.login](/pt/manual/api/reference/user/login).
Vamos supor que você queira se autenticar como um usuário comum do
Zabbix. A requisição JSON a seguir poderia fazer isso:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "user.login",
    "params": {
        "user": "Admin",
        "password": "zabbix"
    },
    "id": 1,
    "auth": null
}
```

Vamos olhar mais de perto o objeto de requisição. Ele tem as seguintes
propriedades:

-   `jsonrpc` - a versão do protocolo JSON-RPC utilizado pela API; o
    Zabbix implementa a API usando a versão 2;
-   `method` - o método a ser chamado na API;
-   `params` - os parâmetros que serão passados para o método da API;
-   `id` - um identificador arbitrário do pedido;
-   `auth` - um token de autenticação de usuário; uma vez que ainda não
    o temos, deve ser definido como `null`.

Se você oferecer as credenciais corretamente ,a resposta retornada pela
API será o token de autenticação:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": "0424bd59b807674191e7d77572075f33",
    "id": 1
}
```

O objeto de resposta contêm algumas propriedades:

-   `jsonrpc` - novamente, a versão do protocolo JSON-RPC;
-   `result` - o dado retornado pelo método;
-   `id` - identificador da requisição corrente.

[comment]: # ({/new-ef3f5841})

[comment]: # ({new-37011a71})
#### Authorization methods

[comment]: # ({/new-37011a71})

[comment]: # ({new-130188f5})
##### By "Authorization" header

All API requests require an authentication or an API token.
You can provide the credentials by using the "Authorization" request header:

```bash
curl --request POST \
  --url 'https://company.com/zabbix/ui/api_jsonrpc.php' \
  --header 'Authorization: Bearer 0424bd59b807674191e7d77572075f33'
```

[comment]: # ({/new-130188f5})

[comment]: # ({new-38cc7fd6})
##### By "auth" property

An API request can be authorized by the "auth" property.

::: noteimportant
Note that the "auth" property is deprecated. It will be removed in the future releases.
:::

```bash
curl --request POST \
  --url 'https://company.com/zabbix/ui/api_jsonrpc.php' \
  --header 'Content-Type: application/json-rpc' \
  --data '{"jsonrpc":"2.0","method":"host.get","params":{"output":["hostid"]},"auth":"0424bd59b807674191e7d77572075f33","id":1}'
```

[comment]: # ({/new-38cc7fd6})

[comment]: # ({new-8950965e})
##### By Zabbix cookie

A *"zbx_session"* cookie is used to authorize an API request from Zabbix UI performed using JavaScript (from a module or
a custom widget).

[comment]: # ({/new-8950965e})

[comment]: # ({new-f572ecc2})
#### Recuperando hosts

Agora podemos utilizar o token de autenticação para acessar dados no
Zabbix. Por exemplo, vamos utilizar o método
[host.get](/pt/manual/api/reference/host/get) para recuperar os IDs,
nomes e interfaces de todos os
[hosts](/pt/manual/api/reference/host/object) que o usuário tiver
acesso:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.get",
    "params": {
        "output": [
            "hostid",
            "host"
        ],
        "selectInterfaces": [
            "interfaceid",
            "ip"
        ]
    },
    "id": 2,
    "auth": "0424bd59b807674191e7d77572075f33"
}
```

::: noteimportant
Observe que a propriedade `auth` agora está
definida com o token de autenticação que obtivemos através do método
`user.login`.
:::

O objeto de resposta irá conter o dado sobre os hosts que foi
requisitado:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "hostid": "10084",
            "host": "Zabbix server",
            "interfaces": [
                {
                    "interfaceid": "1",
                    "ip": "127.0.0.1"
                }
            ]
        }
    ],
    "id": 2
}
```

::: notetip
Por questões de performance, é recomendado sempre
solicitar somente as propriedades que você está interessado, não
solicitar propriedades que você não irá utilizar nestes
momento.
:::

[comment]: # ({/new-f572ecc2})

[comment]: # ({new-ee2c324f})
#### Criando um novo item

Vamos criar um novo [item](/pt/manual/api/reference/item/object) no
"Zabbix server" usando os dados que obtivemos através do método anterior
( `host.get`). ISso pode ser feito através do método
[item.create](/pt/manual/api/reference/item/create):

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "item.create",
    "params": {
        "name": "Free disk space on $1",
        "key_": "vfs.fs.size[/home/joe/,free]",
        "hostid": "10084",
        "type": 0,
        "value_type": 3,
        "interfaceid": "1",
        "delay": 30
    },
    "auth": "0424bd59b807674191e7d77572075f33",
    "id": 3
}
```

Uma resposta bem sucedida irá conter o ID do novo item criado, que
poderá ser utilizado mas próximas referências:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "24759"
        ]
    },
    "id": 3
}
```

::: notetip
O método `item.create` também pode ser utilizado
múltiplos itens em uma mesma chamada, através de arrays de
dados.
:::

[comment]: # ({/new-ee2c324f})

[comment]: # ({new-5ed44978})
#### Gerenciamento de erros

Até este ponto tudo o que tentamos deve ter funcionado corretamente. Mas
o que aconteceria se nós tentássemos uma chamada incorreta à API? Vamos
tentar criar outra chamada ao método
[host.create](/pt/manual/api/reference/host/create), mas omitindo a
propriedade obrigatória `groups` .

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.create",
    "params": {
        "host": "Linux server",
        "interfaces": [
            {
                "type": 1,
                "main": 1,
                "useip": 1,
                "ip": "192.168.3.1",
                "dns": "",
                "port": "10050"
            }
        ]
    },
    "id": 3,
    "auth": "0424bd59b807674191e7d77572075f33"
}
```

A resposta desta requisição irá conter uma mensagem de erro:

``` {.java}
{
    "jsonrpc": "2.0",
    "error": {
        "code": -32602,
        "message": "Invalid params.",
        "data": "No groups for host \"Linux server\"."
    },
    "id": 3
}
```

Se um erro ocorrer, ao invés da propriedade `result`, a resposta irá
conter a propriedade `error` junto com os dados a seguir:

-   `code` - um código de erro;
-   `message` - mensagem curta do erro;
-   `data` - mensagem mais detalhada do erro.

Erros podem ocorrer em diversas situações, tal qual, uso incorreto de
valores, demora excessiva na sessão ou tentar acessar objetos
inexistentes. Sua aplicação terá que ter a habilidade de tratar
corretamente destes erros.

[comment]: # ({/new-5ed44978})

[comment]: # ({new-aa174634})
### versões da API

Para simplificar o versionamento da API, a partir do Zabbix 2.0.4, a
versão da API é igual a versão do Zabbix. Você pode utilizar o método
[apiinfo.version](/pt/manual/api/reference/apiinfo/version) para
localizar a versão da API com a qual você está trabalhando. Este método
em particular pode ser muito utilizado para ajustar a sua aplicação para
características específicas de cada versão da API.

Nós garantimos compatibilidade retroativa. Quando ocorrem
incompatibilidades entre a versão mais nova e a anterior, normalmente
nós deixamos a antiga funcionalidade como obsoleta na nova versão e a
removemos após algumas versões. Ocasionalmente, nós podemos remover
características nas versões mais novas sem prover a
retrocompatibilidade. Por isso é importante que você não mantenha o uso
de funcionalidades marcadas como obsoletas, migrando os métodos para as
suas alternativas sempre que possível.

::: notetip
Você pode seguir todas as mudanças feitas na API em [log
de alteração API](/pt/manual/api/changes_2.2_-_2.4).
:::

[comment]: # ({/new-aa174634})

[comment]: # ({new-e217b4a2})
### Outras leituras

Agora você sabe o suficiente para começar a trabalhar com a API do
Zabbix, mas não pare aqui, temos uma extensiva documentação sobre os
métodos [disponíveis](/pt/manual/api/reference).

[comment]: # ({/new-e217b4a2})




[comment]: # ({new-7665e280})
#### Error handling

Up to that point everything we've tried has worked fine. But what
happens if we try to make an incorrect call to the API? Let's try to
create another host by calling
[host.create](/manual/api/reference/host/create) but omitting the
mandatory `groups` parameter.

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.create",
    "params": {
        "host": "Linux server",
        "interfaces": [
            {
                "type": 1,
                "main": 1,
                "useip": 1,
                "ip": "192.168.3.1",
                "dns": "",
                "port": "10050"
            }
        ]
    },
    "id": 7,
    "auth": "0424bd59b807674191e7d77572075f33"
}
```

The response will then contain an error message:

``` {.java}
{
    "jsonrpc": "2.0",
    "error": {
        "code": -32602,
        "message": "Invalid params.",
        "data": "No groups for host \"Linux server\"."
    },
    "id": 7
}
```

If an error occurred, instead of the `result` property, the response
object will contain an `error` property with the following data:

-   `code` - an error code;
-   `message` - a short error summary;
-   `data` - a more detailed error message.

Errors can occur in different cases, such as, using incorrect input
values, a session timeout or trying to access unexisting objects. Your
application should be able to gracefully handle these kinds of errors.

[comment]: # ({/new-7665e280})

[comment]: # ({new-c1074d48})
### API versions

To simplify API versioning, since Zabbix 2.0.4, the version of the API
matches the version of Zabbix itself. You can use the
[apiinfo.version](/manual/api/reference/apiinfo/version) method to find
out the version of the API you're working with. This can be useful for
adjusting your application to use version-specific features.

We guarantee feature backward compatibility inside of a major version.
When making backward incompatible changes between major releases, we
usually leave the old features as deprecated in the next release, and
only remove them in the release after that. Occasionally, we may remove
features between major releases without providing any backward
compatibility. It is important that you never rely on any deprecated
features and migrate to newer alternatives as soon as possible.

::: notetip
You can follow all of the changes made to the API in the
[API changelog](/manual/api/changes_3.4_-_4.0).
:::

[comment]: # ({/new-c1074d48})

[comment]: # ({new-dfd7315f})
### Further reading

You now know enough to start working with the Zabbix API, but don't stop
here. For further reading we suggest you have a look at the [list of
available APIs](/manual/api/reference).

[comment]: # ({/new-dfd7315f})
