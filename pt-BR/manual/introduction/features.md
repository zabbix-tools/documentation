[comment]: # translation:outdated

[comment]: # ({new-09ce3b2e})
# -\#3 Funcionalidades do Zabbix

[comment]: # ({/new-09ce3b2e})

[comment]: # ({new-a70701e6})
#### Overview

Zabbix é uma solução de monitoração integrada, que provê diversos
recursos de monitoração em um único pacote.

**[A coleta de dados](/pt/manual/config/items)**

-   Verificações de disponibilidade e desempenho
-   Suporta SNMP (tanto "trapping" quanto "polling"), IPMI, JMX,
    Monitoração VMware
-   Verificações personalizadas
-   Coleta de dados com intervalos personalizados, inclusive com
    agendamento exato de momento da coleta (ex. 11:23 de uma
    segunda-feira)
-   A coleta pode ser executada pelo servidor, proxy ou pelos agentes

**[Definição de limites flexíveis](/pt/manual/config/triggers)**

-   Você pode definir limites flexíveis, chamados de triggers,
    referenciando valores do banco de dados da monitoração

**[Alertas altamente configuráveis](/pt/manual/config/notifications)**

-   O envio de notificações pode ser configurado por tipo de mídia, com
    ou sem escalonamento de destinatários
-   Notificações podem utilizar-se de valores definidos em macros
-   Podem incluir comandos remotos automáticos

**[Gráficos sob demanda (em tempo
real)](/pt/manual/config/visualisation/graphs/simple)**

-   Qualquer item numérico que armazenado pode gerar gráficos sub
    demanda, sem planejamento anterior necessário.

**[Web monitoring capabilities](/pt/manual/web_monitoring)**

-   Zabbix pode executar uma sequência de passos simulados em um site,
    verificando sua funcionalidade e tempo de resposta

**[Diversas opções de visualização](/pt/manual/config/visualisation)**

-   Capacidade de definir gráficos personalizados combinando vários
    itens em uma única apresentação
-   Mapas de rede
-   Telas customizadas e apresentações de slides para uma visualização
    em padrão de painel de controle
-   Relatórios
-   Visão de alto nível (negócio) dos recursos monitorados

**[Histórico e armazenamento de
dados](/pt/manual/installation/requirements#database_size)**

-   Os dados são armazenados em banco de dados
-   O histórico é configurável
-   Processo interno de limpeza de dados antigos

**[Configuração simplificada](/pt/manual/config/hosts)**

-   Todo elemento monitorado é um host
-   Hosts são monitorados assim que inseridos no banco de monitoração
-   É possível utilizar perfis de monitoração (templates) aos
    dispositivos monitorados (hosts)

**[Uso de templates](/pt/manual/config/templates)**

-   Agrupamento de verificações em templates
-   Os templates podem herdar propriedades de outros templates

**[Descoberta de rede](/pt/manual/discovery)**

-   Descoberta automática de dispositivos na rede
-   Autorregistro dos agentes
-   Autodescoberta de sistema de arquivos, interfaces de rede e OID SNMP

**[Interface web ágil](/pt/manual/web_interface)**

-   A interface web é escrita em PHP
-   Acessível a partir de qualquer local
-   Você pode clicar no caminho percorrido pela interface
-   Log de auditoria

**[API Zabbix](/pt/manual/api)**

-   A API Zabbix fornece interface programável para atualizações em
    massa, integração com ferramentas de terceiro e outros recursos.

**[Sistema de permissões](/pt/manual/config/users_and_usergroups)**

-   Autenticação segura dos usuários
-   Determinados usuários podem ser limitados a visualizar subconjuntos
    de funções e de hosts monitorados

**[Arquitetura de agente totalmente
expansível](/pt/manual/concepts/agent)**

-   Instalado nos dispositivos alvo da monitoração (servidores)
-   Pode ser instalado tanto em Windows quanto em Linux

**[Binários da solução (Daemons)](/pt/manual/concepts/server)**

-   Escritos em C, para alto desempenho e baixo custo de memória
-   Facilmente portáveis

**[Pronto para ambientes complexos](/pt/manual/distributed_monitoring)**

-   Monitoração remota é feita facilmente com o apoio de um [Proxy
    Zabbix](/pt/manual/concepts/proxy).

[comment]: # ({/new-a70701e6})
