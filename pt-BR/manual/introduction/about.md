[comment]: # translation:outdated

[comment]: # ({new-761c4a6f})
# -\#2 O que é o Zabbix

[comment]: # ({/new-761c4a6f})

[comment]: # ({new-319fca18})
#### Visão geral

O Zabbix foi criado por Alexei Vladishev, e atualmente é mantido e
suportado pela Zabbix SIA.

O Zabbix é uma solução de nível enterprise, de código aberto e com
suporte a monitoração distribuída.

O Zabbix é um software que monitora vários parâmetros da rede, dos
servidores e da saúde dos serviços. Utiliza-se de um mecanismo flexível
de notificação que permite configurar alertas por e-mail para
praticamente qualquer evento. As notificações permitem que se reaja
rapidamente à problemas no ambiente. O Zabbix oferece excelentes
recursos de relatórios e visualização de dados armazenados. Isso faz com
que o Zabbix seja a ferramenta ideal para planejamento de capacidade.

O Zabbix suporta tanto "pooling" quanto "trapping". Os relatórios e
estatísticas do Zabbix, e seus parâmetros de configuração, estão
acessíveis através de interface web. O uso de uma interface web garante
que você possa avaliar o estado de sua rede e a saúde de seus servidores
a partir de qualquer local. Quando corretamente configurado o Zabbix
pode desempenhar papel importante na infraestrutura de monitoramento de
TI. Estas características se aplicam tanto a pequenas organizações com
poucos servidores quanto para grandes empresas, com milhares de
servidores.

Zabbix é livre de custos. É desenvolvido e distribuído através da
licença pública GPLv2. Isso garante que seu código-fonte seja
distribuído e esteja disponível para o público em geral.

[Suporte comercial](http://www.zabbix.com/support.php) está disponível e
é fornecido pela Zabbix INC.

Leia mais sobre [funcionalidades do Zabbix](features).

[comment]: # ({/new-319fca18})

[comment]: # ({new-46ee774b})
#### Usuários do Zabbix

Ao redor do mundo, muitas organizações de diferentes tamanhos confiam no
Zabbix como sua plataforma primária de monitoração.

#### Comunidades

Existem comunidades em diversos idiomas, para o idioma português existe
a comunidade Zabbix Brasil que, apesar do nome remeter ao Brasil, atende
a toda a comunidade que tem como idioma principal o português.

Endereços

Yahoo: <https://br.groups.yahoo.com/neo/groups/zabbix-brasil/info>

Facebook: <https://www.facebook.com/groups/zabbixbrasil/>

[comment]: # ({/new-46ee774b})
