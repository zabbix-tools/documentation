[comment]: # translation:outdated

[comment]: # ({new-bade5a47})
# -\#4 Visão geral

[comment]: # ({/new-bade5a47})

[comment]: # ({new-b4d6d531})
#### Arquitetura

A solução Zabbix é composta por vários componentes de software, a
funcionalidade de cada um destes será descrita a seguir.

[comment]: # ({/new-b4d6d531})

[comment]: # ({new-5957a1ba})
##### Servidor

[Servidor Zabbix](/pt/manual/concepts/server) é o componente central da
solução e, em ambientes centralizados, os agentes enviam os dados
coletados (sobre integridade, disponibilidade e estatísticos) para ele.
Em ambientes descentralizados o envio dos dados é feito para um
componente intermediário: o proxy.

[comment]: # ({/new-5957a1ba})

[comment]: # ({new-494fb840})
##### Banco de armazenamento

Todas as informações de configuração e os dados recebidos pelo Zabbix
são armazenados em um sistema gerenciador de banco de dados (SGBD).

[comment]: # ({/new-494fb840})

[comment]: # ({new-bd637ffa})
##### Interface Web

Para acesso rápido, e a partir de qualquer dispositivo, a solução vem
com uma interface web. Normalmente esta interface é parte do mesma
máquina do Servidor Zabbix, apesar de ser possível sua instalação em
outro servidor.

::: noteclassic
Caso o servidor Zabbix seja instalado usando como banco de
dados o SQLite, passa a ser obrigatório que a interface web esteja na
mesma máquina do Servidor Zabbix.
:::

[comment]: # ({/new-bd637ffa})

[comment]: # ({new-bfd730af})
##### Proxy

O [Proxy Zabbix](/pt/manual/concepts/proxy) pode coletar dados de
desempenho e disponibilidade em nome do Servidor Zabbix. Este é um
componente opcional na implantação do Zabbix, no entanto, pode ser muito
benéfico para seu ambiente distribuir a carga de coletas entre o
Servidor Zabbix e um ou mais proxies.

[comment]: # ({/new-bfd730af})

[comment]: # ({new-b9991630})
##### Agente

O [Agente Zabbix](/pt/manual/concepts/agent) é instalado nos servidores
alvo da monitoração e pode monitorar ativamente os recursos e aplicações
locais, enviando os dados obtidos para o Servidor ou Proxy Zabbix.

[comment]: # ({/new-b9991630})

[comment]: # ({new-9ec23332})
#### Fluxo de dados

Além dos componentes do Zabbix é importante conhecer o fluxo de dados
global do Zabbix:

-   Para que se possa coletar qualquer dado (item) é preciso que seja
    criado um host.
-   Para identificar os eventos e incidentes é necessária a criação de
    triggers, e estas trabalham com os dados dos Itens.
-   Para que uma ação de notificação ou comando remoto seja executado,
    deve ocorrer uma mudança de estado em uma trigger.

Assim, se você quiser receber um alerta sobre excesso de consumo de CPU
no servidor X você precisará:

1.  Cadastrar um host para representar o servidor X;
2.  Cadastrar um item para acompanhar o consumo de CPU;
3.  Cadastrar uma trigger que alerte quando o consumo de CPU for maior
    do que o desejado;
4.  Criar uma ação que envia um e-mail, ou executa um comando remoto;

Embora pareçam muitos passos, com a utilização do recurso de templates
estes passos ficam realmente simples e, justamente por esta
característica, a configuração da monitoração do seu ambiente fica muito
simples.

[comment]: # ({/new-9ec23332})
