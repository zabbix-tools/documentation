[comment]: # translation:outdated

[comment]: # ({new-cbbea730})
# - \#1 Estrutura do manual

[comment]: # ({/new-cbbea730})

[comment]: # ({new-14bd850a})
#### Estrutura

O conteúdo deste manual é dividido em seções e subseções para prover
acesso rápido para assuntos de interesse específico.

Quando você navegar pelas seções certifique-se de expandir as as pastas
da seção para visualizar a lista completa de subseções e páginas
individuais.

Ligações cruzadas entre as páginas são fornecidas, sempre que possível,
visando certificar que nenhuma informação relevante seja perdida pelos
usuários.

[comment]: # ({/new-14bd850a})

[comment]: # ({new-4f79bd48})
#### Seções

[Introdução](about) fornece informações sobre o software Zabbix. Esta
seção deve prover bons motivos para optar pelo Zabbix.

[Conceitos](/pt/manual/concepts) explica a terminologia utilizada pelo
Zabbix e detalhes sobre seus componentes.

[Instalação](/pt/manual/installation/getting_zabbix) e [Início
rápido](/pt/manual/quickstart/item). Estas seções irão ajuda-lo a
iniciar no Zabbix. [Appliance Zabbix](/pt/manual/appliance) é uma
alternativa para um início rápido, apesar de não ser uma alternativa
para ambientes de produção.

[Configuração](/pt/manual/config) é uma das maiores e mais importantes
seções deste manual. Ela possui várias dicas essenciais para monitorar
seu ambiente, a partir da criação de hosts para obter dados essenciais,
receber notificações e comandos remotos a serem executados em casos de
problemas.

[Serviços de IT](/pt/manual/it_services) esta seção detalha como usar o
Zabbix para obter uma visão geral de alto nível do seu ambiente.

[Monitoração Web](/pt/manual/web_monitoring) deve ajuda-lo a compreender
como monitorar a disponibilidade de sitios web.

[Monitoração de máquinas virtuais](/pt/manual/vm_monitoring) demonstra
como monitorar ambientes VMWare.

[Manutenção](/pt/manual/maintenance), [Expressões
regulares](/pt/manual/regular_expressions), [Reconhecimento de
eventos](/pt/manual/acknowledges) e [Importação/Exportação
XML](/pt/manual/xml_export_import) são seções que ensinam vários
aspectos do Zabbix.

[Descoberta](/pt/manual/discovery) possui instruções para configuração
de regras de descoberta automática de redes, agentes ativos, sistemas de
arquivos, interfaces de rede, etc.

[Monitoração Distribuída](/pt/manual/distributed_monitoring) trata com
possibilidades de uso do Zabbix em ambientes maiores e mais complexos.

[Interface Web](/pt/manual/web_interface) contêm informações específicas
de uso da interface web do Zabbix.

[API](/pt/manual/api) apresenta detalhes de uso da API Zabbix.

Listas detalhadas de informações técnicas estão inclusas no
[apêndice](/pt/manual/appendix). Este é o lugar que você encontrará
também uma sessão de FAQ.

[comment]: # ({/new-4f79bd48})
