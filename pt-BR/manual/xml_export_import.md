[comment]: # translation:outdated

[comment]: # ({new-566c4be6})
# 13. Configuração de importação / exportação

[comment]: # ({/new-566c4be6})

[comment]: # ({new-265e7dfe})
#### Visão geral

A funcionalidade de importação/exportação do Zabbix permite a fácil
troca de configuração de entidades entre vários sistemas Zabbix.

Casos típicos de uso desta funcionalidade:

-   compartilhar templates ou mapas de rede - Usuários Zabbix podem
    compartilhar seus parâmetros de configuração
-   integração com ferramentas de terceiros - o formato universal XML
    faz com que a integração de importação e exportação de dados seja
    possível com ferramentas e aplicações de terceiros.

[comment]: # ({/new-265e7dfe})

[comment]: # ({new-19950447})
##### O que pode ser exportado/importado

Os objetos que podem ser exportados/importados são:

-   grupos de hosts (*apenas através da API*)
-   templates (incluindo todos as entidades diretamente associadas -
    itens, triggers, gráficos, telas, LLD e associação com templates)
-   hosts (incluindo todos as entidades diretamente associadas - itens,
    triggers, gráficos, LLD e associação com templates))
-   mapas de rede (incluindo todas as imagens relacionadas); Suportado
    desde Zabbix 1.8.2.
-   imagens
-   telas
-   mapeamento de valores

[comment]: # ({/new-19950447})

[comment]: # ({new-44b0f5b3})
##### Formato da exportação

Os dados podem ser exportados usando a interface web ou através da [API
Zabbix](/pt/manual/api/reference/configuration). Os formatos de
exportação suportados são:

-   XML - na interface web
-   XML ou JSON - através da API Zabbix

[comment]: # ({/new-44b0f5b3})

[comment]: # ({new-b87bd84d})
#### Detalhes sobre exportação

-   Todos os elementos são exportados em um único arquivo.
-   Entidades de host e de template (itens, triggers, gráficos, regras
    de descoberta) que são herdadas dos templates associados não são
    exportadas. Qualquer modificação nestas entidades no nível de host
    (por exemplo mudança no intervalo de coleta, modificar uma expressão
    regular ou adicionar protótipos em regra de descoberta) serão
    perdidas durante a exportação; quando importar, todas as entidaes de
    templates associados serão recriadas a partir do template associado
    original.
-   Entidades criadas a partir de LLD e entidades dependentes delas não
    serão exportadas. Por exemplo, uma trigger criada com um item gerado
    em uma regra de LLD não será exportada.
-   Os triggers e os gráficos que usam itens de verificação web não
    serão exportados.

[comment]: # ({/new-b87bd84d})

[comment]: # ({new-6ea18fa7})
#### Detalhes sobre importação

-   A importação aborta ao primeiro erro
-   Quando estiver atualizando imagens existentes durante a importação o
    campo "imagetype" será ignorado, ou seja, não é possível mudar o
    tipo de uma imagem durante a importação.
-   Quando estiver importando hosts/templates utilizando a opção "Apagar
    ausentes", as macros de host/template ausentes no arquivo importado
    serão excluídas também.
-   Tags vazias para itens, triggers, gráficos, host/template
    aplicações, regras de descoberta, protótipos de itens, protótipos de
    trigger, protótipos de gráficos são sem sentido.
-   A importação suporta tanto XML quanto JSON, o arquivo de importação
    deverá ter a extensão correta: .xml para XML e .json para JSON.
-   Consulte [informações de
    compatibilidade](/pt/manual/appendix/compatibility) sobre as versões
    do XML.

::: noteimportant
Antes do 2.0.1 o processo de importação possuia
uma sintaxe diferente para as tags vazias.
:::

[comment]: # ({/new-6ea18fa7})

[comment]: # ({new-790544d4})
#### Formato base do XML

``` {.xml}
<?xml version="1.0" encoding="UTF-8"?>
<zabbix_export>
    <version>2.0</version>
    <date>2012-04-18T14:07:36Z</date>
</zabbix_export>
```

    <?xml version="1.0" encoding="UTF-8"?>

Cabeçalho padrão para documento XML.

    <zabbix_export>

Elemento raiz para a exportação XML do Zabbix.

    <version>2.0</version>

Versão da exportação.

    <date>2012-04-18T14:07:36Z</date>

Data que a exportação foi criada no formato ISO 8601 long .

Outras tags dependem do que foi exportado.

[comment]: # ({/new-790544d4})



[comment]: # ({new-8c839240})
#### XML format

``` {.xml}
<?xml version="1.0" encoding="UTF-8"?>
<zabbix_export>
    <version>6.0</version>
    <date>2020-04-22T06:20:11Z</date>
</zabbix_export>
```

    <?xml version="1.0" encoding="UTF-8"?>

Default header for XML documents.

    <zabbix_export>

Root element for Zabbix XML export.

    <version>6.0</version>

Export version.

    <date>2020-04-22T06:20:11Z</date>

Date when export was created in ISO 8601 long format.

Other tags are dependent on exported objects.

[comment]: # ({/new-8c839240})

[comment]: # ({new-239421b8})
#### JSON format

``` {.json}
{
    "zabbix_export": {
        "version": "6.0",
        "date": "2020-04-22T06:20:11Z"
    }
}
```

      "zabbix_export":

Root node for Zabbix JSON export.

          "version": "6.0"

Export version.

          "date": "2020-04-22T06:20:11Z"

Date when export was created in ISO 8601 long format.

Other nodes are dependent on exported objects.

[comment]: # ({/new-239421b8})
