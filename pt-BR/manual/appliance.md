[comment]: # translation:outdated

[comment]: # ({062a749a-8c911baa})
# 6. Aplicação Zabbix

[comment]: # ({/062a749a-8c911baa})

[comment]: # ({454fcae9-cfa1a361})
#### Visão geral


Como alternativa a configurar manualmente ou reutilizar um servidor existente para o 
Zabbix, os usuários podem [baixar](http://www.zabbix.com/download_appliance) uma aplicação Zabbix (Zabbix appliance) ou uma imagem de CD de 
instalação da aplicação Zabbix.

As versões da aplicação Zabbix e CD de instalação são baseadas em CentOS 8
(x86\_64).

O CD de instalação da aplicação Zabbix pode ser usado para implantação
imediata do Zabbix Server (MySQL).

::: noteimportant
Você pode usar esta aplicação para avaliar o Zabbix.
A aplicação não objetiva o uso efetivo em produção. 
:::

[comment]: # ({/454fcae9-cfa1a361})

[comment]: # ({82ca0ede-cffa82a3})
##### Requisitos de Sistema:

-   *RAM*: 1.5 GB
-   *Espaço em disco*: no mínimo 8 GB devem ser alocados para a máquina virtual.

Menu de instalação do CD/DVD do Zabbix:

![](../../assets/en/manual/installation_cd_boot_menu1.png){width="600"}

A aplicação Zabbix contém um Zabbix Server (configurado e rodando com MySQL) 
e um frontend.

A aplicação virtual do Zabbix está disponível nos seguintes formatos:

-   VMWare (.vmx)
-   Open virtualization format (.ovf)
-   Microsoft Hyper-V 2012 (.vhdx)
-   Microsoft Hyper-V 2008 (.vhd)
-   KVM, Parallels, QEMU, USB stick, VirtualBox, Xen (.raw)
-   KVM, QEMU (.qcow2)

Para iniciar, suba (boot) a aplicação e navegue até o endereço que
a aplicação (VM) recebeu por DHCP.

::: noteimportant
O DHCP deve estar habilitado na máquina virtualizadora. 
:::

Para verificar o endereço IP dentro da máquina virtual execute:

    ip addr show

Para acessar o frontend do Zabbix, navegue até **http://<host\_ip>** (para acessar
pelo navegador da máquina virtualizadora o modo bridged deve estar habilitado nas configurações
de rede da máquina virtual).

::: notetip
Se a aplicação falhar ao iniciar no Hyper-V, você pode tentar
pressionar `Ctrl+Alt+F2` para alternar entre sessões tty.
:::

[comment]: # ({/82ca0ede-cffa82a3})

[comment]: # ({d8264102-589fd5e2})
#### - Mudanças nas configurações do CentOS 8

A aplicação é baseada em CentOS 8. Há algumas mudanças aplicadas às 
configurações base do CentOS.


[comment]: # ({/d8264102-589fd5e2})

[comment]: # ({76ce7dc5-193d3b23})
##### - Repositórios

O [repositório](/manual/installation/install_from_packages/rhel_centos) oficial do Zabbix foi adicionado ao */etc/yum.repos.d*:

    [zabbix]
    name=Zabbix Official Repository - $basearch
    baseurl=http://repo.zabbix.com/zabbix/5.2/rhel/8/$basearch/
    enabled=1
    gpgcheck=1
    gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-ZABBIX-A14FE591

[comment]: # ({/76ce7dc5-193d3b23})

[comment]: # ({b6bba1c5-589dc798})
##### - Configurações de firewall

A aplicação usa firewall iptables com regras pré-definidas:

-   Porta SSH aberta (22 TCP);
-   Portas Zabbix Agent (10050 TCP) e Zabbix Trapper (10051 TCP)
    abertas;
-   Portas HTTP (80 TCP) e HTTPS (443 TCP) abertas;
-   Portas SNMP trap aberta (162 UDP);
-   Conexões de saída para porta NTP (53 UDP) abertas;
-   Pacotes ICMP limitados a 5 pacotes por segundo;
-   Todas as outras conexões de entrada são bloquadas.

[comment]: # ({/b6bba1c5-589dc798})

[comment]: # ({487116eb-b2283a9a})
##### - Usando um endereço IP estático

Por padrão a aplicação usa DHCP para obter um endereço IP. Para especificar
um endereço IP estático:

-   Acesse como usuário root;
-   Abra o arquivo */etc/sysconfig/network-scripts/ifcfg-eth0*;
-   Altere *BOOTPROTO=dhcp* por *BOOTPROTO=none*
-   Adicione as seguintes linhas:
    -   *IPADDR=<IP address of the appliance>*
    -   *PREFIX=<CIDR prefix>*
    -   *GATEWAY=<gateway IP address>*
    -   *DNS1=<DNS server IP address>*
-   Execute o comando **systemctl restart network**.

Consulte a [documentação](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/deployment_guide/s1-networkscripts-interfaces) oficial da Red Hat se necessário.


[comment]: # ({/487116eb-b2283a9a})

[comment]: # ({972563fb-acf06935})
##### - Alterando time zone

Por padrão a aplicação utiliza UTC para o relógio do sistema. Para alterar o
time zone, copie o arquivo apropriado de */usr/share/zoneinfo* para
*/etc/localtime*, por exemplo:

    cp /usr/share/zoneinfo/Europe/Riga /etc/localtime

[comment]: # ({/972563fb-acf06935})

[comment]: # ({59df20c7-2c378c8d})
#### - Configuração Zabbix

A configuração inicial da aplicação Zabbix tem as seguintes alterações de senha e configuração:

[comment]: # ({/59df20c7-2c378c8d})

[comment]: # ({734df5c2-d39b5151})
##### - Credenciais (login:password)

Sistema:

-   root:zabbix

Zabbix Frontend:

-   Admin:zabbix

Banco de Dados:

-   root:<random>
-   zabbix:<random>

::: noteclassic
Senhas de banco de dados são geradas randomicamente durante 
o processo de instalação.\
A senha de Root é arquivada no arquivo /root/.my.cnf. Não é 
necessário informar a senha ao usar a conta "root".
:::

Para alterar a senha do usuário do banco de dados, as alterações devem ser 
feitas no seguintes locais:

-   MySQL;
-   /etc/zabbix/zabbix\_server.conf;
-   /etc/zabbix/web/zabbix.conf.php.

::: noteclassic
Usuários individuais `zabbix_srv` e `zabbix_web` são definidos
para o Server e o Frontend respectivamente. 
:::

[comment]: # ({/734df5c2-d39b5151})

[comment]: # ({730132da-f11ee379})
##### - Localização de arquivos

-   Arquivos de configuração estão localizados em **/etc/zabbix**.
-   Arquivos de log para Zabbix Server, Proxy e Agent estão localizados em
    **/var/log/zabbix**.
-   O Zabbix Frontend está localizado em **/usr/share/zabbix**.
-   O diretório Home para o usuário **zabbix** é **/var/lib/zabbix**.

[comment]: # ({/730132da-f11ee379})

[comment]: # ({dfcaf452-e00773c5})
##### - Alterações às configurações do Zabbix

-   O timezone do Frontend está configurado para Europe/Riga (isto pode ser modificado em
    **/etc/php-fpm.d/zabbix.conf**);

[comment]: # ({/dfcaf452-e00773c5})

[comment]: # ({cf55ca25-2c4e4239})
#### - Acesso ao Frontend

Por padrão, o acesso ao frontend é permitido de qualquer local.

O frontend pode ser acesso em *http://<host>*.

Isto pode ser customizado em **/etc/nginx/conf.d/zabbix.conf**. O Nginx deve
ser reiniciado após alterações neste arquivo. Para tal, acesse via SSH como
usuário **root** e execute:

    systemctl restart nginx

[comment]: # ({/cf55ca25-2c4e4239})

[comment]: # ({7b776b4b-4fbc391b})
#### - Firewall

Por padrão, apenas as portas listadas nas [mudanças de configuração](#firewall_configuration) 
acima estão abertas. Para abrir portas adicionais, modifique o arquivo 
"*/etc/sysconfig/iptables*" e recarregue as regras de firewall:

    systemctl reload iptables

[comment]: # ({/7b776b4b-4fbc391b})

[comment]: # ({cf62c3a6-bf60554b})
#### - Atualizando

Os pacotes da aplicação do Zabbix podem ser atualizados. Para fazê-lo, execute:

    dnf update zabbix*

[comment]: # ({/cf62c3a6-bf60554b})

[comment]: # ({f442198e-d167c766})
#### - Serviços de Sitema

Serviços compatíveis com Systemd estão disponíveis:

    systemctl list-units zabbix*

[comment]: # ({/f442198e-d167c766})

[comment]: # ({89087237-a582d1bf})
#### - Notas específicas de formatação

[comment]: # ({/89087237-a582d1bf})

[comment]: # ({7b3b5c8b-a45df55b})
##### - VMware

As imagens no formato *vmdk* são utilizáveis diretamente nos produtos VMware Player, 
Server e Workstation. Para uso em ESX, ESXi e vSphere elas devem ser convertidas
usando [VMware converter](http://www.vmware.com/products/converter/).

[comment]: # ({/7b3b5c8b-a45df55b})

[comment]: # ({4cdde886-7d1c1440})
##### - HDD/flash image (raw)

    dd if=./zabbix_appliance_5.2.0.raw of=/dev/sdc bs=4k conv=fdatasync

Substitua */dev/sdc* por seu dispositivo de disco Flash/HDD.

[comment]: # ({/4cdde886-7d1c1440})
