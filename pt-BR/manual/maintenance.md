[comment]: # translation:outdated

[comment]: # ({new-c86bb2b9})
# 10. Manutenção

[comment]: # ({/new-c86bb2b9})

[comment]: # ({new-e70ea305})
#### Visão geral

O Zabbix permite que você defina períodos de manutenção em hosts ou
grupos de hosts. Os períodos de manutenção podem ser de dois tipos: com
coleta de dados; sem coleta de dados.

Durante períodos de manutenção "com coleta de dados" as triggers serão
processadas e os eventos serão criados, entretanto, será possível
configurar as ações para não enviar as notificações / comandos remotos.
Isso é feito através da aba de
[condições](/pt/manual/config/notifications/action/conditions) ao
adicionar uma condição similar a esta 'Período de manutenção = não em
manutenção' - isso fará com que as notificações não sejam enviadas
durante a janela de manutenção. Se uma trigger gerar um evento durante a
manutenção, um evento adicional (o mesmo que foi criado durante a
manutenção) será criado ao final da manutenção. Desta forma, se um
incidente ocorrer durante o período de manutenção e não for resolvido, a
notificação será criada assim que o período de manutenção terminar.

Para receber uma notificação sobre incidentes não resolvidos durante o
período de manutenção você precisa remover a condição padrão que
restringe o envio durante o período de manutenção.

::: noteclassic
Se pelo menos um host (utilizado na expressão da trigger)
não estiver em modo de manutenção, o Zabbix irá enviar a notificação de
problema.
:::

O Zabbix Server precisa continuar em execução durante a manutenção. O
processo de agendamento responsável por alternar o status de manutenção
dos hosts ocorre no segundo 0 de cada minuto. Um proxy sempre irá
coletar dados independente do período de manutenção (incluindo períodos
de manutenção sem coleta de dados). O que ocorrerá é que o dado será
descartado quando for enviado para o Zabbix Server.

Quando um período de manutenção sem coleta de dados termina, as triggers
usando a função `nodata()` não irão ser executadas enquanto não for
alcançado um novo período de verificação.

Se um item de log for adicionado enquanto o host estiver em período de
manutenção e a manutenção terminar, apenas as as linhas geradas após o
fim do período de manutenção serão coletadas.

::: noteimportant
Para garantir um comportamento uniforme durante a
recorrência de períodos de manutenção (diário, semanal, mensal), é
recomendável que seja utilizado o mesmo 'timezone' em todas as partes do
Zabbix.
:::

[comment]: # ({/new-e70ea305})


[comment]: # ({new-6ef20399})
#### Configuração

Para configurar um período de manutenção:

-   Clique em *Configuração → Manutenção*
-   Clique no botão *Criar período de manutenção* (ou no nome de um
    período já cadastrado)

A aba de **Manutenção** contêm atributos gerais do período de
manutenção:

![](../../assets/en/manual/maintenance/maintenance.png)

|Parâmetro|Descrição|
|----------|-----------|
|*Nome*|Nome do período de manutenção.|
|*Tipo da manutenção*|Dois tipos de manutenção podem ser definidos:<br>**Com coleta de dados** - os dados são coletados pelo servidor durante a manutenção e as triggers serão processadas<br>**Sem coleta de dados** - os dados não serão coletados e as triggers não serão processadas durante o período de manutenção|
|*Ativo desde*|Momento em que o período de manutenção começou a estar ativo.<br>*Nota*: Apenas definir este campo não ativa um período de manutenção, é necessário configurar também na aba **Períodos**.|
|*Ativo até*|Momento em que o período de manutenção deixa de estar ativo.|
|*Descrição*|Descrição do período de manutenção.|

A aba **Períodos** permite que você defina exatamente quais dias e horas
a manutenção ocorrerá. Clique no link *Nova* para definir um novo
*Período de manutenção* flexível, onde você definirá o horário de início
e duração da manutenção em cada dia, semana, mês ou uma manutenção
única.

![](../../assets/en/manual/maintenance/maintenance2.png)

Os períodos `Diário` e `Semanal` tem em o campo *A cada dia/A cada
semana*, cujo valor padrão é '1'. Definindo ele para '2' fará com que a
mautenção ocorra a cada dois dias/semanas. O dia/semana de início
respeitará o definido em *Ativo desde*.

Por exemplo, tendo o campo *Ativo desde* definido para 2013-09-06 12:00
e uma hora de duração diaria e recorrentea cada dois dias começando às
23:00 irá resultar que o primeiro período de manutenção começe
2013-09-06 às 23:00, enquanto o segundo período de manutenção irá
ocorrer no dia 2013-09-08 às 23:00. Ou, com o mesmo valor em *Ativo
desde* uma hora de duração e recorrência a cada dois dias, mas começando
às 01:00. A primeira janela de manutenção ocorrerá no dia 2013-09-08 às
01:00, e a segunda janela de manutenção em 2013-09-10 às 01:00.

A aba de **Grupos e hosts** permite que você selecione os hosts e grupos
de hosts para a manutenção.

![](../../assets/en/manual/maintenance/maintenance3.png)

[comment]: # ({/new-6ef20399})

[comment]: # ({new-4dc2aa1c})
#### Apresentação

Um ícone alaranjado com uma chave branca indica que um host está em
manutenção, sendo visível nos módulos *Monitoramento → Dashboard*,
*Monitoramento → Triggers* e *Inventário → Hosts → Detalhes do
inventário do Host*.

![](../../assets/en/manual/maintenance/maintenance_icon.png)

Os detalhes de manutenção são apresentados quando o mouse é posicionado
sobre o ícone.

::: noteclassic
A apresentação dos hosts em manutenção no Dashboard pode ser
inativada através do filtro de Dashboard.
:::

Adicionalmente, hosts em manutenção receberão um fundo laranja no módulo
*Monitoramento → Mapas* e em *Configuração → Hosts* seus status
aparecerão como 'Em manutenção'.

[comment]: # ({/new-4dc2aa1c})

[comment]: # ({new-6aeb0a4c})

::: noteimportant
When creating a maintenance period, the [time zone](/manual/web_interface/time_zone) of the user who creates it is used.
However, when recurring maintenance periods (*Daily*, *Weekly*, *Monthly*) are scheduled, the time zone of the Zabbix server is used.
To ensure predictable behavior of recurring maintenance periods, it is required to use a common time zone for all parts of Zabbix.
:::

[comment]: # ({/new-6aeb0a4c})

[comment]: # ({new-3b17f2f3})

When done, press *Add* to add the maintenance period to the *Periods* block.

Note that Daylight Saving Time (DST) changes do not affect how long the maintenance will be.
For example, let's say that we have a two-hour maintenance configured that usually starts at 01:00 and finishes at 03:00:

-   if after one hour of maintenance (at 02:00) a DST change happens and current time changes from 02:00 to 03:00, the maintenance will continue for one more hour (till 04:00);
-   if after two hours of maintenance (at 03:00) a DST change happens and current time changes from 03:00 to 02:00, the maintenance will stop, because two hours have passed;
-   if a maintenance period starts during the hour that is skipped by a DST change, then the maintenance will not start.

If a maintenance period is set to "1 day" (the actual period of the maintenance is 24 hours, since Zabbix calculates days in hours), starts at 00:00 and finishes at 00:00 the next day:

-   the maintenance will stop at 01:00 the next day if current time changes forward one hour;
-   the maintenance will stop at 23:00 that day if current time changes back one hour.

[comment]: # ({/new-3b17f2f3})




[comment]: # ({new-924e8c68})
#### Display

[comment]: # ({/new-924e8c68})

[comment]: # ({new-0e33672d})
##### Displaying hosts in maintenance

An orange wrench icon
![](../../assets/en/manual/web_interface/frontend_sections/configuration/maintenance_wrench_icon.png)
next to the host name indicates that this host is in maintenance in:

-   *Monitoring → Dashboard*
-   *Monitoring → Problems*
-   *Inventory → Hosts → Host inventory details*
-   *Configuration → Hosts* (See 'Status' column)

![](../../assets/en/manual/maintenance/maintenance_icon.png)

Maintenance details are displayed when the mouse pointer is positioned
over the icon.

Additionally, hosts in maintenance get an orange background in
*Monitoring → Maps*.

[comment]: # ({/new-0e33672d})

[comment]: # ({new-a923614c})
##### Displaying suppressed problems

Normally problems for hosts in maintenance are suppressed, i.e. not
displayed in the frontend. However, it is also possible to configure
that suppressed problems are shown, by selecting the *Show suppressed
problems* option in these locations:

-   *Monitoring* → *Dashboard* (in *Problem hosts*, *Problems*,
    *Problems by severity*, *Trigger overview* widget configuration)
-   *Monitoring* → *Problems* (in the filter)
-   *Monitoring* → *Maps* (in map configuration)
-   Global
    [notifications](/manual/web_interface/user_profile/global_notifications)
    (in user profile configuration)

When suppressed problems are displayed, the following icon is displayed:
![](../../assets/en/manual/web_interface/icon_suppressed.png). Rolling a
mouse over the icon displays more details:

![](../../assets/en/manual/web_interface/info_suppressed2.png)

[comment]: # ({/new-a923614c})
