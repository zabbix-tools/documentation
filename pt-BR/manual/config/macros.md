[comment]: # translation:outdated

[comment]: # ({new-03ce9d2a})
# - \#8 Macros

[comment]: # ({/new-03ce9d2a})

[comment]: # ({new-4c81df7a})
#### Visão geral

O Zabbix suporta uma grande quantidade de macros que podem ser
utilizadas em diversas situações. Macros estão diponíveis e
identificáveis pela sintaxe **{MACRO}**, e seu valor dependerá do
contexto.

O uso eficiente de macros economiza muito tempo e faz com que a
configuração do Zabbix seja mais transparente.

Um dos usos típicos de uma macro é em um template. Assim uma trigger em
um template pode ser nomeado como "Processor load is too high on
{HOST.NAME}". E quando o template for associado ao host, por exemplo o
'Zabbix server', o nome da trigger será substituído para "Processor load
is too high on Zabbix server" quando a trigger for apresentada na seção
de monitoramento.

Macros podem ser utilizadas em chavse de itens. A macro pode ser
utilizada como parte do parâmetro, por exemplo
`item.key[server_{HOST.HOST}_local]`. As aspas duplas não são
necessárias pois o Zabbix irá cuidar das ambiquidades com caracteres
especiais se estiverem presentes na macro.

Veja a lista completa de [macros
suportadas](/pt/manual/appendix/macros/supported_by_location) por
localização.

Você também pode definir as suas próprias [macros de
usuário](/pt/manual/config/macros/usermacros).

[comment]: # ({/new-4c81df7a})
