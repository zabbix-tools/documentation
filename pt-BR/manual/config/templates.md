[comment]: # translation:outdated

[comment]: # ({new-bd7905d8})
# - \#6 Templates

[comment]: # ({/new-bd7905d8})

[comment]: # ({new-59f74d33})
#### Visão geral

Um template é um conjunto de entidades que pode ser associadas de forma
fácil e conveniente a vários hosts.

As entidades podem ser:

-   Itens
-   Triggers
-   Gráficos
-   Aplciações
-   Telas (*desde o Zabbix 2.0*)
-   Regras de autobusca (LLD) (*desde o Zabbix 2.0*)
-   Cenários web (*desde o Zabbix 2.2*)

Como na vida real vários hosts são idênticos (sob a ótica de
monitoração) ou muito similares, é natural que exista um conjunto de
entidades (itens, triggers, gráficos,...) que você vai criar em um host,
mas servirá também para vários outros. É claro que você pode copiar as
entidades entre os hosts, mas isso gera um bocado de trabalho manual.
Com o uso de templates tal processo é simplificado ao simplesmente
associar um host a um template, com isso o Zabbix já irá copiar todo o
perfil de monitoração necessário para o host.

Os templates também podem ser usados (e normalmente o são) para agrupar
conjuntos comuns de monitoração para aplicações ou serviços específicos
(tal qual o Apache, MySQL, PostgreSQL, Postfix...) e são associados de
forma cumulativa nos hosts.

Outro benefício do uso de templates é que se for necessária a
modificação de um determinado perfil de monitoração (por exemplo
adicionar uma nova métrica de monitoração em todos os servidores Apache)
isso poderá ser feito no nível do template que todos os hosts associados
serão alterados em conjunto.

Assim, o uso de templates é um excelente recurso para reduzir a
sobrecarca de trabalho e garantir ambientes monitorados de forma
padronizada.

Proceda para o manual de [criação e configuração de
templates](/pt/manual/config/templates/template).

[comment]: # ({/new-59f74d33})
