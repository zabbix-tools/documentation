[comment]: # translation:outdated

[comment]: # ({new-b4b0e364})
# - \#4 Eventos

[comment]: # ({/new-b4b0e364})

[comment]: # ({new-3825c91b})
#### Visão geral

Existem diversos tipos de eventos gerados no Zabbix:

-   eventos de trigger - sempre que uma trigger modifica seu estado
    (*OK→INCIDENTE→OK*)
-   eventos de descoberta - quando hosts ou serviços são percebidos
-   eventos de autorregistro - quando os agentes ativos são registrados
    automaticamente pelo servidor
-   eventos internos - quando um item/regra de autobusca passa para o
    estado de não suportado ou uma trigger passa para o estado
    'desconhecido'

::: noteclassic
Eventos internos são suportados desde o Zabbix
2.2.
:::

Eventos possuem carimbo de hora e podem ser ações básicas como o envio
de uma notificação por e-mail, etc.

Para visualizar os detalhes dos eventos na interface web, acesse
*Monitoramento* | *Eventos*. Lá você pode clicar na data e hora do
evento para visualizar seus detalhes.

Mais detalhes estão disponíveis para [cada fonte de
evento](/pt/manual/config/events/sources).

[comment]: # ({/new-3825c91b})
