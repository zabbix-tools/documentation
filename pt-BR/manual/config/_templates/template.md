[comment]: # translation:outdated

[comment]: # ({new-3222767b})
# - \#1 Configurando um template

[comment]: # ({/new-3222767b})

[comment]: # ({new-630aea00})
#### Visão geral

A configuração de um template requer que você primeiro crie uma
definição geral do template (podemos chamar de definição externa) para
depois criar suas entidades (definição interna), que podem ser items,
triggers, graphs, etc..

[comment]: # ({/new-630aea00})

[comment]: # ({new-de7b2ff6})
#### Criando um template

Para criar um template:

-   Acesse *Configuração → Templates*
-   Clique no botão *Criar template*
-   Informe os atributos do template

A aba **Template** contêm informações gerais sobre o template.

![](../../../../assets/en/manual/config/template.png){width="600"}

Atributos do template:

|Parâmetro|Descrição|
|----------|-----------|
|*Nome do template*|Nome único do template.|
|*Nome visível*|Se definido, será este o nome apresentado nas listagens, mapas, etc.|
|*Grupos*|Grupos de hosts/templates dos quais o template participa.|
|*Novo grupo*|Um novo grupo que será criado e o template irá participar (visivel somente para usuários com perfil de 'Zabbix Super Admin'.<br>Ignorado, se vazio.|
|*Hosts/Templates*|Listas dos hosts/templates associados ao template.|
|*Descrição*|Descrição adicional sobre o template.|

A aba **Associado aos templates** permite que você associe um ou mais
templates ao template atual. Assim como ocorre na associação entre hosts
e templates, a associação entre templates importa todas as entidades
existentes no template "pai" (itens, triggers, gráficos, etc.).

Para associar a um novo template, basta começar a digitar o nome do
template ao qual queremos associar no campo *Vincular a novos templates*
que será apresentada uma lista contendo os templates correspondentes aos
parâmetros da pesquisa. Selecione o template desejado da lista. Quando
todos os templates desejados forem selecionados (um a um), clique no
link *Adicionar*.

Para remover a associação entre templates, utilize uma das opções abaixo
no bloco *Associado aos tempaltes*:

-   *Desassociar* - desassocia o template, mas mantêm os itens, triggers
    e gráficos
-   *Desassociar e limpar* - desassocia o template e remove todos os
    itens, triggers e gráficos

A aba **Macros** permite que você defina, em nível de template, as
[macros de usuário](/pt/manual/config/macros/usermacros). Você também
pode ver aqui as macros dos templates associados e as macros globais e o
valor atual que será considerado neste contexto.

![](../../../../assets/en/manual/config/template_c.png){width="600"}

Para facilitar existem links para a origem das macros herdadas, assim
como um link para fácil cópia da macro herdada para a atribuição de novo
valor. Esta ação pode ser feita ao clicar no link *Alterar*.

Botões:

|   |   |
|---|---|
|![](../../../../assets/en/manual/config/button_add.png)|Adicionar o template. Após a adição o template deverá ser visível na lista.|
|![](../../../../assets/en/manual/config/button_update.png)|Atualiza as propriedades de um template já existente.|
|![](../../../../assets/en/manual/config/button_clone.png)|Cria um novo template baseado nas propriedades do template atual, incluindo suas entidades (itens, triggers, etc) que foram herdadas de associações com outros templates.|
|![](../../../../assets/en/manual/config/button_full.png)|Cria um novo template baseado nas propriedades do template atual, incluindo suas entidades (itens, triggers, etc), tanto as criadas diretamente no template quanto as que foram herdadas de associações com outros templates.|
|![](../../../../assets/en/manual/config/button_delete.png)|Exclui o template; As entidades que hosts associados herdaram não serão excluídas.|
|![](../../../../assets/en/manual/config/button_clear.png)|Exclui o template e as entidades nos hosts associados que foram herdadas dele.|
|![](../../../../assets/en/manual/config/button_cancel.png)|Cancela a edição do template.|

Com o template criado, é o momento para criar suas entidades.

::: noteimportant
Itens devem ser adicionados primeiro no template.
As triggers e gráficos não poderão ser adicionadas sem ter um item para
corresponder.
:::

[comment]: # ({/new-de7b2ff6})

[comment]: # ({new-0d973911})
#### Adicionando itens, triggers e gráficos

Para copiar itens criados em hosts/templates para o novo template:

-   Acesse *Configuração → Hosts* (ou *Templates*)
-   Clique no link *Itens* da linha do host/template onde se deseja
    criar o item
-   Marque a caixa de seleção dos itens que deseja copiar
-   Clique no botão *Copiar* na parte inferior da lista de itens
-   Selecione o template (ou grupo de templates) para onde deseja copiar
    os itens e clique no botão *Copiar*

Todos os itens selecionados serão copiados para o template.

Pode-se fazer processo similar para as triggers e gráficos (a partir da
lista de triggers e de gráficos respectivamente), novamente, tenha em
mente a adição só será possível se existirem no template de destino os
itens necessários.

[comment]: # ({/new-0d973911})

[comment]: # ({new-b78a3626})
#### Adicionando telas

Para adicionar telas ao template

-   Acesse *Configuração → Templates*
-   Clique no link *Telas* da linha do template
-   Configure a tela, conforme a forma normal de [configuração de
    telas](/pt/manual/config/visualisation/screens)

::: noteimportant
Os elementos que podem ser incluídos em telas do
template são: gráficos simples, gráficos customizados, relótio, texto
puro, URL.
:::

[comment]: # ({/new-b78a3626})

[comment]: # ({new-513e93e5})
#### Configurando regras de autobusca (LLD)

Consulte o manual de
[autobusca](/pt/manual/discovery/low_level_discovery) para maiores
informações sobre o recurso.

[comment]: # ({/new-513e93e5})

[comment]: # ({new-e3935a9d})
#### Adicionando cenários web

Para adicionar cenários web em um template

-   Acesse *Configuração → Templates*
-   Clique no link *Web* da linha do template
-   Configure o cenário web, conforme a forma normal de [configuração de
    cenários web](/pt/manual/web_monitoring#configuring_a_web_scenario)

[comment]: # ({/new-e3935a9d})

[comment]: # ({new-d357b380})
#### Creating a template group

::: noteimportant
Only Super Admin users can create template groups.
:::

To create a template group in Zabbix frontend, do the following:

-   Go to: *Configuration → Template groups*
-   Click on *Create template group* in the upper right corner of the screen
-   Enter the group name in the form

![](../../../../assets/en/manual/config/template_group.png)

To create a nested template group, use the '/' forward slash separator, for example `Linux servers/Databases/MySQL`. You can create this group even if none of the two parent template groups (`Linux servers/Databases/`) exist. In this case creating these parent template groups is up to the user; they will not be created automatically.<br>Leading and trailing slashes, several slashes in a row are not allowed. Escaping of '/' is not supported.

[comment]: # ({/new-d357b380})

[comment]: # ({new-ed4ea073})

Once the group is created, you can click on the group name in the list to edit group name, clone the group or set additional option:

![](../../../../assets/en/manual/config/template_group2.png)

*Apply permissions to all subgroups* - mark this checkbox and click on *Update* to apply the same level of permissions to all nested template groups. For user groups that may have had differing [permissions](/manual/config/users_and_usergroups/usergroup#configuration) assigned to nested template groups, the permission level of the parent template group will be enforced on the nested groups. This is a one-time option that is not saved in the database.

**Permissions to nested template groups**

-   When creating a child template group to an existing parent template group,
    [user group](/manual/config/users_and_usergroups/usergroup)
    permissions to the child are inherited from the parent (for example,
    when creating `Databases/MySQL` if `Databases` already exists)
-   When creating a parent template group to an existing child template group,
    no permissions to the parent are set (for example, when creating
    `Databases` if `Databases/MySQL` already exists)

[comment]: # ({/new-ed4ea073})
