[comment]: # translation:outdated

[comment]: # ({new-be8f2fc2})
# - \#2 Associando/Desassociando

[comment]: # ({/new-be8f2fc2})

[comment]: # ({new-63cf7d66})
#### Visão geral

O processo de associação é a forma pela qual os modelos de monitoração
(templates) são aplicados a hosts (com todas as suas entidades), assim
como o processo de desassociação é a forma de desfazer isso.

::: noteimportant
Os templates são associados diretamente aos hosts
individuais, não a grupos de hosts. Adicionar um template a um grupo de
host não gera nenhuma associação entre os hosts deste grupo e o templat.
Os grupos são utilizado apenas para separação lógica de hosts e
templates.
:::

[comment]: # ({/new-63cf7d66})

[comment]: # ({new-af0ee654})
#### Associando a um template

Para associar um template a um host

-   Acesse *Configuração → Hosts*
-   Clique no nome do host desejado e clique na aba *Templates*
-   Clique no botão *Selecionar* da caixa *Vincular a novos templates*
-   Selecione um ou mais templates no pop-up aberto
-   Clique no botão *Adicionar/Atualizar* do formulário de propriedades
    do host

O host irá herdar todas as entidades (itens, triggers, gráficos, etc) do
template.

::: noteimportant
A associação de múltiplos templates a um mesmo
host poderá falhar se existirem itens duplicados entre eles (mesma
chave).
:::

Quando as entidades (itens, triggers, gráficos etc.) são adicionadas a
partir de um template:

-   caso existam entidades idênticas no host, as mesmas serão
    atualizadas e convertidas passando a ser entidades herdadas do
    template
-   as demais entidades associadas anteriormente através de ligações com
    outros templates, não serão tocadas

Nas listagens, todas as entidades herdadas tem o nome pre-fixado com o
nome do template de origem. O nome do template é apresentado na cor
cinza com um link para acessar a entidade de origem.

Se alguma entidade (item, trigger, gráfico etc.) não for pre-fixada com
o nome do template, isso indicará que a mesma foi criada diretamente no
host.

[comment]: # ({/new-af0ee654})

[comment]: # ({new-b32f387a})
##### Critério de unicidade de entidade

Quando adicionamos entidades (itens, triggers, gráficos etc.) a partir
de um template em um host é importante saber quais serão os critérios
utilizados para identificar se as entidades pré-existentes serão
atualizadas e convertidas para entidades vinculadas. Os critérios de
unicidade são:

-   para itens - a chave do item
-   para triggers - o nome e expressão da trigger
-   para gráficos customizados - o nome e os itens associados
-   para aplicações - o nome

[comment]: # ({/new-b32f387a})

[comment]: # ({new-368f1425})
##### Associando templates a vários hosts

Existem algumas formas de associar em massa os templates a vários hosts
de uma só vez:

-   Para associar um template a vários hosts, em *Configuração →
    Templates*
-   Abra o formulário de propriedades do template
-   Selecione os hosts desejados a partir da caixa *Outro* e clique no
    botão **<<**
-   Clique no botão *Adicionar/Atualizar*

::: noteclassic
Para reverter a associação, você pode fazer de forma similar
usando o botão **>>**.
:::

Outra forma é através do recurso de [atualização em
massa](/pt/manual/config/hosts/hostupdate)

-   Acesse o recurso de hosts, em *Configuração → Hosts*
-   Marque a caixa de seleção de cada host que se deseja associar /
    desassociar a templates
-   Clique no botão **[Atualização em
    massa](/pt/manual/config/hosts/hostupdate)** na parte inferior da
    lista
-   Clique na aba **Templates** e selecione os templates desejados

![](../../../../assets/en/manual/config/host_mass_update2.png)

Selecione *Templates vinculados* e informe o nome do template
selecionando o(s) template(s) desejado(s) no menu que será apresentado.

A opção *Substituir* fará com que os hosts selecionados sejam associados
ao(s) novo(s) template(s) e que as associações anteriores (se existirem)
sejam removidas. A opção *Limpar ao desassociar* irá remover também as
entidades herdadas pelas associações que forem removidas.

::: notetip
O Zabbix provê significativa quantidade de templates
pré-definidos. Você pode utiliza-los como exemplos, referências, mas
tenha o cuidado ao utiliza-los em produção sem antes validar o que está
sendo coletado. Existem itens que podem não ser necessários em seu
ambiente, períodos de coleta e retenção que podem ser muito
agressivos.
:::

[comment]: # ({/new-368f1425})

[comment]: # ({new-4fdf985b})
##### Editando entidades associadas

Se você tentar editar um item ou trigger herdado de um template,
perceberá que várias propriedades estarão com a edição desabilitada.
Isso ocorre para garantir a padronização e respeito aos modelos
definidos. Algumas propriedades de podem ser modificadas (tal qual
habilitar/desabilitar um item, mudar o intervalo de coleta, período de
retenção, etc).

Se você precisa editar alguma propriedade que está bloqueada, você terá
que alterar isso em sua origem (no nível de template), tendo em mente
que a alteração afetará todos os hosts associados.

[comment]: # ({/new-4fdf985b})

[comment]: # ({new-2d6f7e87})
#### Desassociando um template

Para desassociar um host de um template:

-   Acesse *Configuração → Hosts*
-   Clique no nome do host que se deseja alterar e clique na aba
    *Templates*
-   Clique no link *Desassociar* ou *Desassociar e limpar* da linha do
    template que deseja remover
-   Clique no botão *Atualizar* do formulário de propriedades do host

A opção *Desassociar* apenas removerá a associação com o template,
mantendo as entidades que foram herdadas (itens, triggers, gráficos,
etc.).

A opção *Desassociar e limpar* removerá a associação com o template e as
entidades herdadas.

[comment]: # ({/new-2d6f7e87})
