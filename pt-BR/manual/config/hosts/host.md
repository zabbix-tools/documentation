[comment]: # translation:outdated

[comment]: # ({new-1da35dcd})
# 1 Configurando um host

[comment]: # ({/new-1da35dcd})

[comment]: # ({a2b140eb-0ee38f7e})
#### Visão geral

Para configurar um host no Zabbix Frontend, faça o seguinte:

-   Vá até: *Configuração → Hosts* ou *Monitoramento → Hosts*
-   Clique em *Criar host* à direita (ou no nome do host para editar
    um host existente)
-   Informe os parâmetro do host no formulário

Você também pode usar os botões *Clonar* e *Clone completo* no formulário 
de um host existente para criar um novo host. Clicar em *Clonar* manterá 
todos os parâmetros do host e templates associados (mantendo todas as 
entidades destes templates). *Clone completo* manterá, adicionalmente, entidades
diretamente associadas (aplicações, itens, triggers, gráficos, regras de descoberta
de baixo-nível e cenários web).

*Nota*: Quando um host é clonado, ele reterá todas as entidades de template
como são originalmente no template. Quaisquer alterações a estas entidades
feitas a nível de host (como intervalo de item alterado, expressão regular alterada
ou protótipos adicionados à regra de descoberta de baixo-nível) não serão clonadas
para o novo host; em vez disso elas serão como são no template.

[comment]: # ({/a2b140eb-0ee38f7e})

[comment]: # ({04162afb-e2f56cfc})

#### Configuração

A aba **Host** contém os atributos gerais do host:

![](../../../../assets/en/manual/config/host_a.png)

Todos os campos obrigatórios estão marcados com um asterisco vermelho.

|Parâmetro|<|Descrição|
|---------|-|-----------|
|*Nome do host*|<|Informe um nome de host único. Caracteres alfanuméricos, espaços, pontos, traços e sublinhados são permitidos. No entanto, espaços antes e depois do nome não são permitidos.<br>*Nota:* Com Zabbix Agent sendo executado no host que você está configurando, o [arquivo de configuração](/manual/appendix/config/zabbix_agentd) do host deve ter o parâmetro *Hostname* configurado com o mesmo valor que o nome do host informado aqui. O nome no parâmetro é necessário no processamento das [verificações ativas](/manual/appendix/items/activepassive).|
|*Nome Visível*|<|Informe um nome visível único para o host. Se você configurar este nome, ele será o nome visível nas listas, mapas, etc., em vez do nome do host do parâmetro anterior. Este atributo tem suporte a UTF-8.|
|*Templates (Modelos)*|<|Associe [templates](/manual/config/templates) ao host. Todas as entidades (itens, triggers, gráficos, etc.) serão herdadas do template.<br>Para associar um novo template, comece digitando o nome do template no campo *Vincular novos modelos*. Uma lista de templates equivalentes será mostrada; role a lista para selecionar. Alternativamente, você pode clicar em *Selecionar* próximo ao campo e selecionar templates da lista na janela apresentada. Os templates que são selecionados no campo *Vincular novos modelos* serão associados ao host quando o formulário de configuração do host for salvo ou atualizado.<br>Para desvincular um template, use uma das duas opções no bloco *Associado aos templates*:<br>*Desassociar* - desvincula o template, mas preserva seus itens, triggers e gráficos;<br>*Desassociar e limpar* - desvincula o template e remove todos os seus itens, triggers e gráficos.<br>Os nomes de template na lista são clicáveis levando até o formulário de configuração dos templates.|
|*Grupos*|<|Selecione os grupos de host aos quais o host pertence. Um host deve pertencer a um grupo no mínimo. Um novo grupo pode ser criado e vinculado ao grupo do host pela adição de um nome de grupo não existente.|
|*Interfaces*|<|Vários tipos de interface são suportados para um host: *Agent*, *SNMP*, *JMX* e *IPMI*.<br>Nenhuma interface é definida por padrão. Para adicionar uma nova interface, clique em *Adicionar* no bloco de *Interfaces*, selecione o tipo de interface e informe *IP/DNS*, *Conectado a* e *Porta*.<br>*Nota:* Interfaces que são usadas em algum item não podem ser removidas e o link *Remover* aparecerá cinza para elas.<br>Consulte [configuração de monitoramento SNMP](/manual/config/items/itemtypes/snmp#configuring_snmp_monitoring) para detalhes adicionais na configuração de uma interface SNMP (v1, v2 e v3).|
|<|*Endereço IP*|Endereço IP do host (opcional).|
|^|*Nome DNS*|Nome DNS do host (opcional).|
|^|*Conectar a*|Clicando na opção desejada você informa ao Zabbix Server o que utilizar para receber os dados dos Agents:<br>**IP** - Conecte ao endereço de IP (recomendado)<br>**DNS** - Conecte ao nome DNS|
|^|*Porta*|Número da porta TCP/UDP. Os valores padrão são: 10050 para o Zabbix Agent, 161 para SNMP Agent, 12345 para JMX e 623 para IPMI.|
|^|*Padrão*|Selecione esta opção para marcar a interface como padrão.|
|*Descrição*|<|Adicione uma descrição para o host.|
|*Monitorado por proxy*|<|O host pode ser monitorado ou pelo Zabbix Server ou por um dos Zabbix Proxies:<br>**(sem proxy)** - o host é monitorado pelo Zabbix Server<br>**Nome do proxy** - o host é monitorado pelo Zabbix Proxy "nome do proxy"|
|*Habilitado*|<|Marque esta caixa para tornar o host ativo, pronto para ser monitorado. Se desmarcado, o host não está ativo, portanto não monitorado.|

A aba **IPMI** contém os atributos de gerenciamento do IPMI.

|Parâmetro|Descrição|
|---------|-----------|
|*Algoritmo de autenticação*|Selecione o algoritmo de autenticação.|
|*Nível de acesso*|Selecione o nível de acesso.|
|*Usuário*|Nome de usuário para autenticação. Macros de usuário podem ser usadas.|
|*Senha*|Senha para autenticação. Macros de usuário podem ser usadas.|

A aba **Etiquetas** permite definir [etiquetas](/manual/config/tagging) para o nível de host. Todos os problemas deste host serão etiquetados com os valores informados aqui.

![](../../../../assets/en/manual/config/host_d.png)

Macros de usuário, macros {INVENTORY.\*}, {HOST.HOST}, {HOST.NAME},
{HOST.CONN}, {HOST.DNS}, {HOST.IP}, {HOST.PORT} e {HOST.ID} são suportadas nas etiquetas.

A aba **Macros** permite definir, no nível de host, [macros de usuário](/manual/config/macros/user_macros) como um par nome-valor. Note que os valores das macros podem ser mantidos como texto plano, texto secreto ou Vault secreto. A adição de uma descrição também é suportada.

![](../../../../assets/en/manual/config/host_e.png)

Você também observar aqui macros de template e macros de usuário globais se você selecionar a opção *Macros herdadas e do host*. É onde todas as macros de usuário definidas para o host são mostradas com o valor que receberam, assim como sua origem.

![](../../../../assets/en/manual/config/host_e2.png){width="600"}

Para conveniência, links para os respectivos templates e configuração das macros globais são fornecidos. Também é possível editar um template/macro global no nível do  host, efetivamente criando uma cópia da macro no host.

A aba **Inventário** permite adicionar manualmente informações de [inventário](inventory) para o host. Você também pode selecionar a opção de habilitar o preenchimento *Automático* do inventário, ou desabilitar o preenchimento de inventário para este host.

![](../../../../assets/en/manual/config/host_f.png)

Se o inventário está habilitado (manual ou automático), um sinal verde é mostrado junto ao nome da aba.

[comment]: # ({/04162afb-e2f56cfc})

[comment]: # ({c9d165eb-6875b717})

##### Criptografia

A aba **Criptografia** permite que você estabeleça conexões 
[criptografadas](/manual/encryption) com o host.

|Parâmetro|Descrição|
|---------|-----------|
|*Conexões com o host*|Como o Zabbix Server ou Proxy se conecta ao Zabbix Agent em um host: sem criptografia (padrão), usando PSK (chave pré-compartilhada) ou certificado.|
|*Conexões do host*|Selecione quais tipos de conexões são permitidas a partir do host (p.e. do Zabbix Agent e Zabbix Sender). Vários tipos de conexões podem ser selecionados simultaneamente (útil para testes e alteração para outro tipo de conexão). O padrão é "Sem criptografia".|
|*Emissor*|Emissor de certificado permitido. Um certificado é primeiramente validado com uma CA (autoridade de certificado). Se for válido, assinado pela CA, então o campo *Emissor* pode ser usado para futuras restrições de CAs permitidos. O objetivo de uso deste campo é para quando sua instalação do Zabbix utilizar cartificados de múltiplos CAs. Se este campo estiver vazio então nenhum CA é aceito.|
|*Segurado*|Segurado de certificado permitido. Um certificado é primeiramente validado com uma CA. Se for válido, assinado pela CA, então o campo *Segurado* pode ser usado para permitir apenas um valor (formato texto) de segurado. Se este campo estiver vazio então qualquer certificado válido assinado pela CA configurada é aceito.|
|*Identidade PSK*|Texto da identidade de chave pré-compartilhada.<br>Não coloque informações sensíveis na identidade PSK, ela é transmitida de forma não criptografada pela rede para informar um receptor qual PSK utilizar.|
|*PSK*|Chave pré-compartilhada (texto hexadecimal). Largura máxima: 512 dígitos hexadecimais (256-byte PSK) se o Zabbix usa biblioteca GnuTLS ou OpenSSL, 64 dígitos hexadecimais (32-byte PSK) se o Zabbix usa biblioteca mbed TLS (PolarSSL). Exemplo: 1f87b595725ac58dd977beef14b97461a7c1045b9a1c963065002c5473194952|

[comment]: # ({/c9d165eb-6875b717})

[comment]: # ({5c6a188a-513311ca})
##### Mapeamento de valor

A área de **Mapeamento de valor** permite configurar representações 
humanizadas dos dados de itens com [mapeamento de valor](/manual/config/items/mapping).

[comment]: # ({/5c6a188a-513311ca})

[comment]: # ({new-7cc0883e})
#### Creating a host group

::: noteimportant
Only Super Admin users can create host groups.
:::

To create a host group in Zabbix frontend, do the following:

-   Go to: *Configuration → Host groups*
-   Click on *Create host group* in the upper right corner of the screen
-   Enter the group name in the form

![](../../../../assets/en/manual/config/host_group.png)

To create a nested host group, use the '/' forward slash separator, for example `Europe/Latvia/Riga/Zabbix servers`. You can create this group even if none of the three parent host groups (`Europe/Latvia/Riga/`) exist. In this case creating these parent host groups is up to the user; they will not be created automatically.<br>Leading and trailing slashes, several slashes in a row are not allowed. Escaping of '/' is not supported.

[comment]: # ({/new-7cc0883e})

[comment]: # ({new-22cf222a})

Once the group is created, you can click on the group name in the list to edit group name, clone the group or set additional option:

![](../../../../assets/en/manual/config/host_group2.png)

*Apply permissions and tag filters to all subgroups* - mark this checkbox and click on *Update* to apply the same level of permissions/tag filters to all nested host groups. For user groups that may have had differing [permissions](/manual/config/users_and_usergroups/usergroup#configuration) assigned to nested host groups, the permission level of the parent host group will be enforced on the nested groups. This is a one-time option that is not saved in the database.

**Permissions to nested host groups**

-   When creating a child host group to an existing parent host group,
    [user group](/manual/config/users_and_usergroups/usergroup)
    permissions to the child are inherited from the parent (for example,
    when creating `Riga/Zabbix servers` if `Riga` already exists)
-   When creating a parent host group to an existing child host group,
    no permissions to the parent are set (for example, when creating
    `Riga` if `Riga/Zabbix servers` already exists)

[comment]: # ({/new-22cf222a})

