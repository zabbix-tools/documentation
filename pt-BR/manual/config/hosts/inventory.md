[comment]: # translation:outdated

[comment]: # ({new-061a30b2})
# 2 Inventário

[comment]: # ({/new-061a30b2})

[comment]: # ({20a7a296-4b8ec855})
#### Visão geral

Você pode manter o inventário de dispositivos em rede no Zabbix.

Há um menu especial *Inventário* no frontend do Zabbix. Entretanto, você
não visualizará nenhuma informação inicialmente e não é onde você insere
os dados. A criação de um inventário de dados é feita manualmente quando configurado um host
ou automaticamente através do uso de alguma opção opção de preenchimento automático.

[comment]: # ({/20a7a296-4b8ec855})

[comment]: # ({new-69c69326})
#### Construindo o inventário

[comment]: # ({/new-69c69326})

[comment]: # ({df1d075f-9a68f9a7})
##### Modo manual

Ao [configuring a host](host), na aba *Inventário*, você pode
inserir detalhes como o tipo de dispositivo, número de série, localização,
pessoa responsável, etc - dados que irão preencher as informações do inventário.

Se uma URL for incluída nas informações de inventário do host e iniciar com
'http' ou 'https', resultará em um link clicável na sessão *Inventário*.

[comment]: # ({/df1d075f-9a68f9a7})

[comment]: # ({62658cb3-a9359c4e})
##### Modo automático

O Inventário do host também pode ser preenchido automaticamente. Para que isso funcione,
na configuração do host, na aba *Inventário*, o modo de inventário 
deve ser selecionado como *Automático*.

Desta forma, você pode [configure host items](/manual/config/items/item) para
popular qualquer campo do inventário do host com seu valor, indicando o
campo de destino com seu respectivo atributo (chamado *Item que
preencherá o campo de inventário do host*) na configuração do item.

Itens que são úteis na automatização do inventário:

-   system.hw.chassis\[full|type|vendor|model|serial\] - o padrão é
    \[full\], permissões privilegiadas (root) são necessárias
-   system.hw.cpu\[all|cpunum,full|maxfreq|vendor|model|curfreq\] -
    o padrão é \[all,full\]
-   system.hw.devices\[pci|usb\] - o padrão é \[pci\]
-   system.hw.macaddr\[interface,short|full\] - o padrão é \[all,full\],
    interface é uma expressão regular (regexp)
-   system.sw.arch
-   system.sw.os\[name|short|full\] - o padrão é \[name\]
-   system.sw.packages\[package,manager,short|full\] - o padrão é
    \[all,all,full\], package é uma expressão regular (regexp)

[comment]: # ({/62658cb3-a9359c4e})

[comment]: # ({16768564-84351dfb})
##### Seleção do modo de inventário

O modo de inventário pode ser selecionado na configuração do host.

O modo de inventário por padrão, para novos hosts, é selecionado com base no
*Modo padrão do inventário em novos hosts* definido em *Administração* → *Geral* →
*[Other](/manual/web_interface/frontend_sections/administration/general#outros_parametros)*.

Para hosts adicionados a partir de regras de descoberta de rede ou ações de autorregistro, é
possível definir uma *Operação de modo de inventário* selecionando
modo manual ou automático. Esta operação sobrescreve as configurações de *Modo padrão de inventário de host*.

[comment]: # ({/16768564-84351dfb})

[comment]: # ({2df03bfb-8291099a})
#### Visão geral do inventário

Os detalhes de todos os dados presentes no inventário estão disponíveis no
menu *Inventário*.

Em *Inventário → Visão Geral* você visualizar a quantidade de hosts filtrando por vários campos
do inventário.

Em *Inventário → Hosts* você pode visualizar todos os hosts que contém informações
de inventário. Ao clicar no nome do host, serão exibidos os detalhes do inventário do host em questão.

![](../../../../assets/en/manual/web_interface/inventory_host.png){width="600"}

A aba **Visão geral** apresenta:

|Parâmetro|<|<|<|<|Descrição|
|---------|-|-|-|-|-----------|
|*Nome do host*|<|<|<|<|Nome do Host.<br>Ao clicar no nome do host é aberto o menu com os scripts definidos para ele.<br>O nome do host é exibido com um ícone laranja caso esteja em modo de manutenção.|
|*Nome visível*|<|<|<|<|Nome visível do host (se definido).|
|*Interfaces do Host<br>(Agent, SNMP, JMX, IPMI)*|<|<|<|<|Este bloco mostra os detalhes das interfaces configuradas para o host.|
|*OS*|<|<|<|<|Sistema Operacional definido no inventário (se definido).|
|*Hardware*|<|<|<|<|Descrição do hardware (se definido).|
|*Software*|<|<|<|<|Descrição do software definido (se definido).|
|*Descrição*|<|<|<|<|Descrição do Host.|
|*Monitoramento*|<|<|<|<|Links para informações de monitoramento do host: *Web*, *Dados recentes*, *Problemas*, *Gráficos*, *Dashboards*.|
|*Configuração*|<|<|<|<|Links para configurações do host: *Host*, *Aplicações*, *Items*, *Triggers*, *Gráficos*, *Descobertas*, *Web*.<br>A quantidade de entidades configuradas de cada tipo é exibida.|

A aba **Detalhes** exibe todos os campos que possuem valores (que
não estão vazios).

[comment]: # ({/2df03bfb-8291099a})

[comment]: # ({c7835de7-6a9ca6a1})
#### Macros de inventário

Existem macros de inventário de host {INVENTORY.\*} disponíveis para utilização em 
notificações, por exemplo:

"O Servidor em {INVENTORY.LOCATION1} apresenta um problema, a pessoa responsável é
{INVENTORY.CONTACT1}, número de telefone {INVENTORY.POC.PRIMARY.PHONE.A1}."

Para mais detalhes, acesse
a página [supported macro](/manual/appendix/macros/supported_by_location).

[comment]: # ({/c7835de7-6a9ca6a1})
