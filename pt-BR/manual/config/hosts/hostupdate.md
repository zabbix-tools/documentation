[comment]: # translation:outdated

[comment]: # ({new-1c6301ba})
# 3 Atualização em massa

[comment]: # ({/new-1c6301ba})

[comment]: # ({00fa55ba-7205743f})
#### Visão geral

As vezes será necessário modificar algum atributo de vários hosts de
uma só vez. Ao invés de abrir cada host individualmente para editá-lo, você pode utilizar
o recurso de atualização em massa fazer isto.

[comment]: # ({/00fa55ba-7205743f})

[comment]: # ({bc092096-929367ac})
#### Usando a atualização em massa

Para realizar a atualização em massa de alguns hosts, faça o seguinte:

-   Selecione os hosts que você deseja atualizar em [host
list](/manual/web_interface/frontend_sections/configuration/hosts)
-   Clique em *Atualização em Massa* situado abaixo da lista
-   Navegue pelas abas de atributos (*Host*, *IPMI*,
    *Tags*, *Macros*, *Inventário*, *Criptografia* or *Mapeamento de valores*)
-   Selecione o(s) atributo(s) que deseja atualizar e insira o novo valor para este(s)

![](../../../../assets/en/manual/config/hosts/host_mass.png)

As opções abaixo estarão disponíveis ao selecionar o botão correspondente para a atualização de **template** vinculado:

-   *Associar* - especifica quais templates adicionais a serem vinculados
-   *Substituir* - especifica quais templates vincular quando algum template anteriormente associado ao host for desvinculado
-   *Desassociar* - especifica quais templates serão desvinculados

Para especificar quais templates associar/desassociar, comece a digitar o nome do template
até que o recurso de autocompletar exiba uma listagem com os
templates correspondentes. Basta então selecionar o template desejado.

A opção *Limpar ao desassociar*, além de desvincular templates
associados anteriormente, permite também a remoção de todos os elementos herdados do
 template (itens, triggers, etc.).

As opções a seguir estão disponíveis ao selecionar o botão
para atualização do **Grupo do host**:

As seguintes opções estão disponíveis ao selecionar o respectivo botão para atualização do grupo de hosts:

-   *Adicionar* - permite especificar grupos de host adicionais dentre os já existentes
ou inserir um grupo totalmente novo para os hosts
-   *Substituir* - remove do host o(s) grupo(s) existente(s) e
o(s) substitui pelo(s) grupo(s) especificado(s) (existentes ou novos)
-   *Remover* - remove o host do(s) grupo(s) especificados

INCOMPLETO

[comment]: # ({/bc092096-929367ac})
