[comment]: # translation:outdated

[comment]: # ({231b4c54-94205f99})
# 2 Itens

[comment]: # ({/231b4c54-94205f99})

[comment]: # ({41cbd735-3f680eac})
#### Visão geral

Os itens são aqueles que coletam dados de um host.

Depois de configurar um host, você precisa adicionar alguns itens de monitoramento para começar a obter dados reais.

Um item é uma métrica individual. Uma maneira de adicionar rapidamente muitos itens é anexar uma das templates predefinidas a um host. No entanto, para um desempenho otimizado do sistema, você pode precisar ajustar as templates para ter apenas os itens necessários e a frequência de monitoramento necessária.

Em um item individual você especifica que tipo de dados serão coletados do hospedeiro..

Por este motivo utilize o [item key](/manual/config/items/item/key).
Assim, um item com o nome da chave **system.cpu.load** irá coletar dados da carga do processador, enquanto um item com o nome da chave **net.if.in** coletará informações de tráfego de entrada.

Para especificar outros parâmetros com a chave, inclua entre colchetes após o nome da chave. Assim, system.cpu.load**\[avg5\]** retornará a carga média do processador nos últimos 5 minutos, enquanto
net.if.in**\[eth0\]** irá exibir o tráfego de entrada na interface eth0.

::: noteclassic
Para todos os tipos de itens e chaves de itens suportados, consulte
seções de [item types.](/manual/config/items/itemtypes)
:::

Prossiga para [creating and configuring an
item](/manual/config/items/item).

[comment]: # ({/41cbd735-3f680eac})
