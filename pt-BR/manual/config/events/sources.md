[comment]: # translation:outdated

[comment]: # ({new-e9a8ab7a})
# - \#1 Origens dos eventos

[comment]: # ({/new-e9a8ab7a})

[comment]: # ({new-071dda8a})
#### - Eventos de trigger

A mudança de estados de trigger é a origem mais frequente e importante
de eventos.

Cada vez que uma trigger muda seu estado, um evento é gerado. O evento
contêm detalhes sobre o estado da mudança da trigger, quando ocorreu e
qual é o novo estado.

[comment]: # ({/new-071dda8a})

[comment]: # ({new-3ae80f24})
#### - Eventos de descoberta

O Zabbix periodicamente varre intervalos de IP definidos nas regras de
descoberta de rede. A frequência da verificação é configurável para cada
regra de forma individual. Quando um host é descoberto, um evento de
descoberta (ou diversos eventos) é gerado.

O Zabbix gera os eventos a seguir:

|Evento|Quando é gerado|
|------|----------------|
|Serviço Up|Sempre que o Zabbix detecta o serviço ativo.|
|Serviço Down|Sempre que o Zabbix não é capaz de detectar o serviço.|
|Host Up|Se pelo menos um dos serviços está UP no IP testado.|
|Host Down|Se nenhum serviço está UP no IP testado.|
|Serviço Descoberto|Se o serviço está novamente UP ou foi descoberto pela primeira vez.|
|Serviço Lost|Se o serviço está ausente, após ter sido descoberto.|
|Host Descoberto|Se o host está novamente UP ou se foi descoberto pela primeira vez.|
|Host Lost|Se o host está ausente, após ter sido descoberto.|

[comment]: # ({/new-3ae80f24})

[comment]: # ({new-5d514e59})
#### - Eventos de autorregistro para agentes ativos

O agente ativo pode criar um evento de autorregistro no Zabbix.

Se configurado, o autorregistro de um agente ativo pode ocorrer a partir
do momento em que um agente solicita ao servidor qual é o seu kit de
coletas a executar. Neste momento o Zabbix Server adiciona o host,
utilizando o endereço IP e porta de origem do agente.

Para mais informações, consulte [a página de autorregistro do agente
ativo](/pt/manual/discovery/auto_registration).

[comment]: # ({/new-5d514e59})

[comment]: # ({new-d156b0a7})
#### - Eventos Internos

Os eventos internos podem ocorrer quando:

-   o estado de um item é modificado de 'normal' para 'não suportado'
-   o estado de um item é modificado de 'não suportado' para 'normal'
-   o estado de uma regra de autobusca é modificado de 'normal' para
    'não suportado'
-   o estado de uma regra de autobusca é modificado de 'não suportado'
    para 'normal'
-   o estado de uma trigger é modificado de 'normal' para 'desconhecido'
-   o estado de uma trigger é modificado de 'desconhecido' para 'normal'

Os eventos internos são suportados desde o Zabbix 2.2. O objetivo de
introduzir os eventos internos é o de permitir que os usuários sejam
notificados quando qualquer evento interno interno ocorrer, por exemplo,
quando um item passar a para o estado de 'não suportado' e por conta
disso pare de coletar dados.

[comment]: # ({/new-d156b0a7})
