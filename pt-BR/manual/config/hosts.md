[comment]: # translation:outdated

[comment]: # ({new-31b6b7d8})
# 1 Hosts e grupos de hosts

[comment]: # ({/new-31b6b7d8})

[comment]: # ({94e9469a-61c2f2bf})

#### O que é um "host"?

Tipicamente, os hosts no Zabbix são os equipamentos que você deseja 
monitorar (servidores, estações de trabalho, switches, etc).

A criação de hosts é uma das primeiras ações de monitoramento no Zabbix. 
Por exemplo, se você quer monitorar alguns parâmetros no servidor "x", você
deve primeiro criar um host chamado, digamos, "Servidor X" e então pode 
começar a adicionar itens de monitoramento neste novo host.

Hosts são organizados em grupos de hosts.

Continue [criando e configurando um host](/manual/config/hosts/host).

[comment]: # ({/94e9469a-61c2f2bf})
