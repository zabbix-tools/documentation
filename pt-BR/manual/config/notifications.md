[comment]: # translation:outdated

[comment]: # ({new-5d9b206e})
# - \#7 Notificações sobre eventos

[comment]: # ({/new-5d9b206e})

[comment]: # ({new-1be3daa6})
#### Visão geral

A partir do momento que você configurou alguns itens e triggers os
eventos deverão estar ocorrendo como resultado das mudanças de estado
das triggers, logo é o momento certo para considerar algumas ações.

É claro que não queremos ter que conferir o estado das triggers ou os
eventos a todo momento. É bem melhor que nós recebamos notificações
quando algo significante ocorrer (incidentes de maior severidade por
exemplo). Então, quando os incidentes ocorrerem, queremos que as pessoas
corretas sejam informadas.

É por isso que o envio de notificações é o primeiro tipo de ação
oferecida pelo Zabbix. É possível definir quando e quem será notificado
quando determinado evento ocorrer.

Para ser possível o envio e recebimento de notificações você precisa
ter:

-   [definido alguma mídia](/pt/manual/config/notifications/media)
-   [configurado uma ação](/pt/manual/config/notifications/action) que
    envie uma mensagem através de uma mídia definida

Ações consistem em *condições* e *operações*. Basicamente, quando as
condições são alcançadas, as operações são executadas. As duas
principais operações são: enviar uma mensagem (notificação) e executar
um comando remoto.

Para eventos criados por ações de descoberta de rede e autorregistro,
algumas operações adicionais estão disponíveis. Elas incluem adicionar e
remover hosts, associar a template, etc.

[comment]: # ({/new-1be3daa6})
