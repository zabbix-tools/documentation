[comment]: # translation:outdated

[comment]: # ({new-ba72dbf1})
# 2 Permissões

[comment]: # ({/new-ba72dbf1})

[comment]: # ({new-a9157075})
#### Visão geral

Você pode diferenciar o permissionamento no Zabbix definidndo o tipo do
usuário e incluindo usuários não privilegiados em grupos com permissão
de acesso aos dados de hosts.

[comment]: # ({/new-a9157075})

[comment]: # ({new-ac897fff})

#### User types

Permissions in Zabbix depend, primarily, on the user type:

   - *User* - has limited access rights to menu sections (see below) and no access to any resources by default. Any permissions to host or template groups must be explicitly assigned;
   - *Admin* - has incomplete access rights to menu sections (see below). The user has no access to any host groups by default. Any permissions to host or template groups must be explicitly given;
   - *Super admin* - has access to all menu sections. The user has a read-write access to all host and template groups. Permissions cannot be revoked by denying access to specific groups.

**Menu access**

The following table illustrates access to Zabbix menu sections per user type:

|Menu section|<|User|Admin|Super admin|
|-|-------------|------|------|------|
|**Dashboards**|<|+|+|+|
|**Monitoring**|<|+|+|+|
| |*Problems*|+|+|+|
|^|*Hosts*|+|+|+|
|^|*Latest data*|+|+|+|
|^|*Maps*|+|+|+|
|^|*Discovery*| |+|+|
|**Services**|<|+|+|+|
| |*Services*|+|+|+|
|^|*SLA*| |+|+|
|^|*SLA report*|+|+|+|
|**Inventory**|<|+|+|+|
| |*Overview*|+|+|+|
|^|*Hosts*|+|+|+|
|**Reports**|<|+|+|+|
| |*System information*| | |+|
|^|*Scheduled reports*| |+|+|
|^|*Availability report*|+|+|+|
|^|*Triggers top 100*|+|+|+|
|^|*Audit log*| | |+|
|^|*Action log*| | |+|
|^|*Notifications*| |+|+|
|**Data collection**|<| |+|+|
| |*Template groups*| |+|+|
|^|*Host groups*| |+|+|
|^|*Templates*| |+|+|
|^|*Hosts*| |+|+|
|^|*Maintenance*| |+|+|
|^|*Event correlation*| | |+|
|^|*Discovery*| |+|+|
|**Alerts**|<| |+|+|
| |*Trigger actions*| |+|+|
| |*Service actions*| |+|+|
| |*Discovery actions*| |+|+|
| |*Autoregistration actions*| |+|+|
| |*Internal actions*| |+|+|
|^|*Media types*| | |+|
|^|*Scripts*| | |+|
|**Users**|<| | |+|
| |*User groups*| | |+|
|^|*User roles*| | |+|
|^|*Users*| | |+|
|^|*API tokens*| | |+|
|^|*Authentication*| | |+|
|**Administration**|<| | |+|
| |*General*| | |+|
|^|*Audit log*| | |+|
|^|*Housekeeping*| | |+|
|^|*Proxies*| | |+|
|^|*Macros*| | |+|
|^|*Queue*| | |+|

[comment]: # ({/new-ac897fff})

[comment]: # ({new-a852da1b})
#### Tipos de usuários

Os tipos de usuários definem o nível de acesso aos menus e funções
administrativas e o acesso padrão aos dados de hosts.

|Tipo de usuário|Descrição|
|----------------|-----------|
|*Usuário Zabbix*|O usuário tem acesso ao menu "Monitoramento". Um "Usuário Zabbix" não possui acesso a nenhum outro recurso por padrão. Qualquer permissão à grupos de hosts precisa ser explicitamente definido.|
|*Administrador Zabix*|O usuário tem acesso aos menus "Monitoramento" e "Configuração". Um "Administrador Zabbix" não possui acesso a nenhum outro recurso por padrão. Qualquer permissão à grupos de hosts precisa ser explicitamente definido.|
|*Super Administrador Zabbix*|O usuário tem acesso à todos os menus: "Monitoração", "Configuração" e "Administração". Um "Super Administrador Zabbix" possui acesso de "Leitura-Escrita" em todos os grupos de hosts. Ele não pode ter privilégios reduzidos sem a modificação do seu tipo.|

[comment]: # ({/new-a852da1b})

[comment]: # ({new-f38cb50e})
#### Permissões a grupos de hosts

O acesso a qualquer host no Zabbix é atribuído a grupos de usuários no
nível de grupo de hosts apenas.

Isso quer dizer que não é possível a atribuição direta a um host ou
usuário, todo o controle de acesso é feito através dos grupos.

[comment]: # ({/new-f38cb50e})
