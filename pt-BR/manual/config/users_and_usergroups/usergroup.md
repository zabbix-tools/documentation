[comment]: # translation:outdated

[comment]: # ({new-46adf407})
# 3 Grupos de usuários

[comment]: # ({/new-46adf407})

[comment]: # ({new-f58db4c9})
#### Visão geral

Os grupos de usuários permitem agrupar usuários tanto para fins de
organização quanto para o permissionamento de acesso. As permissões para
monitorar dados de hosts são atribuídas a partir dos grupos de usuários,
nunca a partir dos usuários individuais.

Muitas vezes pode ser interessante separar quais informações estarão
visíveis para cada usuário. Isso pode ser feito ao agrupa-los através
dos grupos de usuários.

Um mesmo usuário pode fazer parte de diversos grupos.

[comment]: # ({/new-f58db4c9})

[comment]: # ({new-fe6bb4a1})
#### Configuração

Para configurar um grupo de usuários:

-   Vá para *Administração → Grupos de usuários*
-   Clique em *Criar grupo de usuários* (ou no nome do grupo caso deseje
    alterar seus dados)
-   Modifique os atributos do grupo no formulário

A aba *Grupo de usuários* possui os atributos gerais de um grupo:

![](../../../../assets/en/manual/config/user_group.png){width="600"}

|Parâmetro|Descrição|
|----------|-----------|
|*Nome do grupo*|Nome único do grupo.|
|*Usuários*|O bloco **No grupo** possui a lista de membros do grupo.<br>Para adicionar usuários no grupo selecionado, os selecione no bloco *Outros grupos* e clique em **<<**.|
|*Acesso à interface web*|Como os usuários do grupo serão autenticados.<br>**Padrão do sistema** - usa o método padrão de autenticação<br>**Interno** - usa a autenticação interna do Zabbix. Ignorado se o método de autenticação da interface web for [HTTP](/manual/web_interface/frontend_sections/administration/authentication#http)<br>**Desabilitado** - o acesso à interface web pelos usuários constantes deste grupo será negado|
|*Status*|Status dos membros do grupo:<br>**Marcado** - usuários estão ativos<br>**Não marcad** - usuários estão desabilitados|
|*Modo de depuração*|Marque este checkbox para ativar o modo de depuração para os membros do grupo.|

A aba *Permissões* permite que você defina o acesso a hosts para o grupo
de usuários:

|   |   |
|---|---|
|*Compondo permissões*|Clique em *Adicionar* na lista respectiva para definir a quais grupos de hosts o grupo de usuários terá acesso e qual será o nível:<br>**LEITURA-ESCRITA** - acesso de leitura e gravação ao grupo de hosts<br>**LEITURA** – acesso somente de leitura ao grupo de hosts<br>**NEGA** – o acesso ao grupo de hosts será negado|
|*Permissões calculadas*|Dependendo do kit de permissões definido acima, as *Permissões calculadas* irão apresentar todos os grupos de hosts e hosts que o grupo de usuários terá acesso e qual o nível deste acesso:<br>**LEITURA-ESCRITA** - grupos de hosts com acesso de leitura e de escrita<br>**LEITURA** - grupos de host com acesso somente de leitura<br>**NEGA** - grupos de hosts cujo acesso será negado|

[comment]: # ({/new-fe6bb4a1})

[comment]: # ({new-e74e2150})
#### Acesso aos hosts a partir de diversos grupos

Um usuário poderá fazer parte de diversos grupos de usuários. Estes
grupos por sua vez poderão ter diferentes níveis de acesos aos hosts.

Portanto, é importante saber quais hosts um usuário normal poderá ter
acesso. Por exemplo, vamos apresentar como o acesso ao host **X** (no
grupo de hosts 1) será afetado em várias situações em que um usuário
faça parte dos grupos A e B.

-   Se o Grupo A possui somente acesso de *Leitura* ao grupo de hosts 1,
    mas o grupo B possui acesso de *Leitura-Escrita* ao grupo de hosts
    1, então o usuário possuirá acesso de **Leitura-Escrita** ao host X.

::: noteimportant
Permissões de “Leitura-Escrita” tem precedência
sobre as permissões de leitura desde a versão 2.2 do
Zabbxi.
:::

FIXME **This page is not fully translated, yet. Please help completing
the translation.**\
*(remove this paragraph once the translation is finished)*

-   No mesmo cenário acima se o host 'X', ao mesmo tempo, fizer parte do
    grupo de hosts 2 e este grupo tiver o acesso explicitamente
    **negado** no grupo A ou B, o acesso ao host 'X' será **NEGADO**,
    mesmo com a permissão de "Leitura-Escrita" definida pelo grupo de
    hosts 1.
-   Se o grupo A não possuir permissões definidas e o Grupo B possuir
    acesso de "Leitura-Escrita" ao grupo de hosts 1, então o usuário
    terá acesso ao host 'X'.
-   Se o grupo A tem tiver o acesso ao grupo de hosts 1 definido como
    "NEGA" e o grupo B tiver o acesso definido como "Leitura-Escrita" o
    usuário não terá acesso ao host 'X'.

[comment]: # ({/new-e74e2150})

[comment]: # ({new-931529bc})
#### Outros detalhes

-   Um usuário com nível **Admnistrador Zabbix** e acesso de
    "Leitura-Escrita" em um host não estará automaticamente habilitado a
    alterar as associações com templates no mesmo. Para que isso seja
    possível o usuário deverá ter permissão de "Leitura-Escrita" também
    no grupo do template que se deseja modificar a associação.
-   Um usuário com nível **Administrador Zabbix** e acesso de "Leitura"
    em um host não estará habilitado a ver a configuração do mesmo,
    entretanto, as triggers estarão disponíveis através dos serviços de
    TI.
-   Qualquer usuário poderá ver um mapa que possua somente imagens.
    Quando hosts, grupos de hosts ou triggers forem adicionadas ao
    referido mapa os permissionamento de acesso aos hosts relacionados
    será considerado. O mesmo se aplica a telas e apresentações de
    slides. Os usuários, independente de permissionamento, poderão ver
    qualquer objeto que não esteja associado, direta ou indiretamente, a
    um host.

[comment]: # ({/new-931529bc})
