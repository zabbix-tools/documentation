[comment]: # translation:outdated

[comment]: # ({new-6f10822d})
# 1 Configurando um usuário

[comment]: # ({/new-6f10822d})

[comment]: # ({new-5f8fb5e5})
#### Visão geral

Para configurar um usuário:

-   Acesse *Administração → Usuários*
-   Clique em *Criar usuário* (ou no nome do usuário para editar um
    usuário já existente)
-   Informe os atributos no formulário

[comment]: # ({/new-5f8fb5e5})

[comment]: # ({new-2b69e181})
#### Atributos gerais

A aba *Usuário* contêm os atributos gerais de um usuário:

![](../../../../assets/en/manual/config/user.png)

|Parâmetro|Descrição|
|----------|-----------|
|*Apelido*|Identificador único do usuário (será usado para o login).|
|*Nome*|Primeiro nome do usuário (opcional).<br>Se estiver preenchido, será visível em reconhecimentos e notificações.|
|*Sobrenome*|Sobrenome do usuário (opcional).<br>Se estiver preenchido, será visível em reconhecimentos e notificações.|
|*Senha*|Senha para acesso ao sistema (deve ser confirmada no campo *Senha (novamente*).<br>Se já existir uma senha definida, estará visível o botão *Alterar senha*, ao clicar os campos ficarão visíveis.|
|*Grupos*|Lista todos os [grupos de usuários](usergroup) aos quais o usuário pertença. A participação do usuário nos grupos de usuário é o que definirá quais grupos de hosts e hosts ele possuirá [permissões de acesso](permissions). Clique no botão *Adicionar* para associar o usuário a novos grupos.|
|*Idioma*|Idioma da interface web do Zabbix.<br>A extensão 'php gettext' é necessária para as traduções funcionarem.|
|*Tema*|Define o padrão visual da interface web:<br>**Padrão do sistema** - usa as definições globais do Zabbix<br>**Azul** - tema padrão azul<br>**Escuro** - tema alternativo em cores escuras|
|*Login automático*|Marque esta opção se você deseja que o Zabbix lembre-se das informações de sessão fornecidas por 30 dias. Serão necessários cookies de browser para isso.|
|*Desconexão automática (min. 90 segundos)*|Marque esta opção para habilitar a desconexão automática do usuário por inatividade (menor valor = 90 segundos).|
|*Atualização da tela (em segundos)*|Defina o intervalo de atualização utilizado por mapas, telas, dados em texto plano, etc. Se definido como '0' a função é inativada.|
|*Registros por página*|Você pode determinar quantas linhas serão apresentadas a cada página em listagens.|
|*URL (após se autenticar)*|Você pode definir qual página o Zabbix irá apresentar para cada usuário após o mesmo ter iniciado uma nova sessão. É possível, por exemplo, enviar o usuário para um mapa ou para uma apresentação de slides específica.|

[comment]: # ({/new-2b69e181})

[comment]: # ({new-f35e11e2})
#### Mídias do usuário

A aba *Mídia* contêm a lista de todas as mídias definidas para o
usuário. As mídias são utilizadas para envair notificações. Clique no
link *Adicionar* dentro do bloco *Mídia* para adicionar uma nova mídia
para o usuário.

Veja mais sobre [tipos de mídias](/pt/manual/config/notifications/media)
em seu manual.

[comment]: # ({/new-f35e11e2})

[comment]: # ({new-bc3b9c76})
#### Permissões

A aba *Permissões* contêm informação sobre:

-   O tipo do usuário (Usuário Zabbix, Administrador Zabbix, Super
    Administrador Zabbix). Os usuários não podem modificar o seu próprio
    tipo.
-   Hosts e grupos de hosts aos quais o usuário tem acesso. Os usuários
    do tipo 'Usuário Zabbix' e 'Administrador Zabbix', por padrão, não
    tem acesso a nenhum grupo ou host. Para que eles possam ter acesso é
    necessário que sejam adicionados a grupos que contenham o
    permissionamento desejado.

Veja o manual de [permissões de usuário](permissions) para maiores
detalhes.

[comment]: # ({/new-bc3b9c76})
