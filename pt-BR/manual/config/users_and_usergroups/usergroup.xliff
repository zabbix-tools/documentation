<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="pt-BR" datatype="plaintext" original="manual/config/users_and_usergroups/usergroup.md">
    <body>
      <trans-unit id="46adf407" xml:space="preserve">
        <source># 3 User groups</source>
      </trans-unit>
      <trans-unit id="f58db4c9" xml:space="preserve">
        <source>#### Overview

User groups allow to group users both for organizational purposes and
for assigning permissions to data. Permissions to viewing and configuring data of host groups and template groups are assigned to user groups, not individual users.

It may often make sense to separate what information is available for
one group of users and what - for another. This can be accomplished by
grouping users and then assigning varied permissions to host and template groups.

A user can belong to any number of groups.</source>
      </trans-unit>
      <trans-unit id="fe6bb4a1" xml:space="preserve">
        <source>#### Configuration

To configure a user group:

-   Go to *Users → User groups*
-   Click on *Create user group* (or on the group name to edit an
    existing group)
-   Edit group attributes in the form

The **User group** tab contains general group attributes:

![](../../../../assets/en/manual/config/user_group.png){width=600}

All mandatory input fields are marked with a red asterisk.

|Parameter|Description|
|--|--------|
|*Group name*|Unique group name.|
|*Users*|To add users to the group start typing the name of an existing user. When the dropdown with matching user names appears, scroll down to select.&lt;br&gt;Alternatively you may click the *Select* button to select users in a popup.|
|*Frontend access*|How the users of the group are authenticated.&lt;br&gt;**System default** - use default authentication method (set [globally](/manual/web_interface/frontend_sections/users/authentication))&lt;br&gt;**Internal** - use Zabbix internal authentication (even if LDAP authentication is used globally).&lt;br&gt;Ignored if HTTP authentication is the global default.&lt;br&gt;**LDAP** - use LDAP authentication (even if internal authentication is used globally).&lt;br&gt;Ignored if HTTP authentication is the global default.&lt;br&gt;**Disabled** - access to Zabbix frontend is forbidden for this group|
|*LDAP server*|Select which [LDAP server](/manual/web_interface/frontend_sections/users/authentication/ldap#configuration) to use to authenticate the user.&lt;br&gt;This field is enabled only if *Frontend access* is set to LDAP or System default.|
|*Enabled*|Status of user group and group members.&lt;br&gt;*Checked* - user group and users are enabled&lt;br&gt;*Unchecked* - user group and users are disabled|
|*Debug mode*|Mark this checkbox to activate [debug mode](/manual/web_interface/debug_mode) for the users.|

The **Template permissions** tab allows to specify user group access to template group (and thereby template) data:

![](../../../../assets/en/manual/config/user_group_templates.png){width="600"}

The **Host permissions** tab allows to specify user group access to host group (and thereby host) data:

![](../../../../assets/en/manual/config/user_group_hosts.png){width="600"}

**Template permissions** and **Host permissions** tabs support the same set of parameters.  

Current permissions to groups are displayed in the *Permissions* block.

If current permissions of the group are inherited by all nested groups, this is indicated after the group name ("*including subgroups*").
Note that a *Super admin* user can enforce nested groups to have the same level of permissions as the parent group; this can be done in the [host](/manual/config/hosts/host#creating_a_host_group)/[template](/manual/config/templates/template#creating_a_host_group) group configuration form.

You may change the level of access to a group:

-   **Read-write** - read-write access to a group;
-   **Read** - read-only access to a group;
-   **Deny** - access to a group denied;
-   **None** - no permissions are set.

Use the selection field below to select groups and the level of access to them.
This field is auto-complete so starting to type the name of a group will offer a dropdown of matching groups.
If you wish to see all groups, click on *Select*.
If you wish to include nested groups, mark the *Include subgroups* checkbox.
Click on ![](../../../../assets/en/manual/config/add_link.png) to add the selected groups to the list of group permissions.

:::noteimportant
Adding a parent group with the *Include subgroups* checkbox marked will override (and remove from the list) previously configured permissions of all related nested groups.
Adding a group with *None* as the level of access selected will remove the group from the list if the group is already in the list.
:::

:::noteclassic
If a user group grants **Read-write** permissions to a host, and **None** to a template, the user will not be able to edit templated items on the host, and template name will be displayed as *Inaccessible template*.
:::

The **Problem tag filter** tab allows setting tag-based permissions for user
groups to see problems filtered by tag name and value:

![](../../../../assets/en/manual/config/user_group_tags.png){width=600}

To select a host group to apply a tag filter for, click *Select* to get
the complete list of existing host groups or start to type the name of a
host group to get a dropdown of matching groups. Only host groups will be
 displayed, because problem tag filter cannot be applied to template groups.

To apply tag filters to nested host groups, mark the *Include subgroups*
checkbox.

Tag filter allows to separate the access to host group from the
possibility to see problems.

For example, if a database administrator needs to see only "MySQL"
database problems, it is required to create a user group for database
administrators first, then specify "Service" tag name and "MySQL" value.

![](../../../../assets/en/manual/config/user_group_tag_filter_2.png){width=600}

If "Service" tag name is specified and value field is left blank,
the user group will see all problems with tag name "Service" for the selected host group. 
If both tag name and value fields are
blank, but a host group is selected, the user group will see all
problems for the specified host group. 

Make sure tag name and tag value are
correctly specified, otherwise, the user group will not see
any problems.

Let's review an example when a user is a member of several user groups
selected. Filtering in this case will use OR condition for tags.

|   |   |   |   |   |   |   |
|---|---|---|---|---|---|---|
|**User group A**|&lt;|&lt;|**User group B**|&lt;|&lt;|**Visible result for a user (member) of both groups**|
|*Tag filter*|&lt;|&lt;|&lt;|&lt;|&lt;|^|
|*Host group*|*Tag name*|*Tag value*|*Host group*|*Tag name*|*Tag value*|^|
|Linux servers|Service|MySQL|Linux servers|Service|Oracle|Service: MySQL or Oracle problems visible|
|Linux servers|*blank*|*blank*|Linux servers|Service|Oracle|All problems visible|
|*not selected*|*blank*|*blank*|Linux servers|Service|Oracle|&lt;Service:Oracle&gt; problems visible|

::: noteimportant
 Adding a filter (for example, all tags in a
certain host group "Linux servers") results in not being able to
see the problems of other host groups.
:::</source>
      </trans-unit>
      <trans-unit id="e74e2150" xml:space="preserve">
        <source>#### Access from several user groups

A user may belong to any number of user groups. These groups may have
different access permissions to hosts or templates.

Therefore, it is important to know what entities an unprivileged user will
be able to access as a result. For example, let us consider how access
to host **X** (in Hostgroup 1) will be affected in various situations
for a user who is in user groups A and B.

-   If Group A has only *Read* access to Hostgroup 1, but Group B
    *Read-write* access to Hostgroup 1, the user will get **Read-write**
    access to 'X'.

::: noteimportant
“Read-write” permissions have precedence over
“Read” permissions.
:::

-   In the same scenario as above, if 'X' is simultaneously also in
    Hostgroup 2 that is **denied** to Group A or B, access to 'X' will
    be **unavailable**, despite a *Read-write* access to Hostgroup 1.
-   If Group A has no permissions defined and Group B has a *Read-write*
    access to Hostgroup 1, the user will get **Read-write** access to
    'X'.
-   If Group A has *Deny* access to Hostgroup 1 and Group B has a
    *Read-write* access to Hostgroup 1, the user will get access to 'X'
    **denied**.</source>
      </trans-unit>
      <trans-unit id="931529bc" xml:space="preserve">
        <source>#### Other details

-   An Admin level user with *Read-write* access to a host will not be
    able to link/unlink templates, if he has no access to the
    template group they belong to. With *Read* access to the template group he will
    be able to link/unlink templates to the host, however, will not see
    any templates in the template list and will not be able to operate
    with templates in other places.
-   An Admin level user with *Read* access to a host will not see the
    host in the configuration section host list; however, the host
    triggers will be accessible in IT service configuration.
-   Any non-Super Admin user (including 'guest') can see network maps as
    long as the map is empty or has only images. When hosts, host groups
    or triggers are added to the map, permissions are respected.
-   Zabbix server will not send notifications to users defined as action
    operation recipients if access to the concerned host is explicitly
    "denied".</source>
      </trans-unit>
    </body>
  </file>
</xliff>
