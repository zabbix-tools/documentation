[comment]: # translation:outdated

[comment]: # ({new-e8992262})
# - \#8 Mapeamento de valores

[comment]: # ({/new-e8992262})

[comment]: # ({new-2d2da55e})
#### Visão geral

Para uma representação mais "humana" dos dados recebidos, você poderá
utilizar mapas contendo uma relação entre os valores numéricos e textos
representativos.

O mapeamento de valores pode ser utilizado tanto na interface web do
Zabbix quanto nas notificações enviadas por email/SMS/jabber etc.

Por exemplo, um item que possa receber os valores '0' ou '1' pode
utilizar um mapeamento de valores para representar o significado destes:

-   '0' => 'Não disponível'
-   '1' => 'Disponível'

Ou, um mapeamento de valores para dados de um backup:

-   'F' → 'Full'
-   'D' → 'Diferencial'
-   'I' → 'Incremental'

Assim, quando você [configura os itens](item) é possível utilizar o
mapeamento de valores para "humanizar" a forma que é apresentado o dado
coletado. Para fazer isso, basta definir no campo *Mostrar valor* um
mapeamento de valores compatível.

::: noteclassic
O mapeamento de valores pode ser utilizado em itens com os
seguintes tipos de informação: *Numérico (inteiro sem sinal)*, *Numérico
(fracionário)* e *Caractere*.
:::

O mapeamento de valores, a partir do Zabbix 3.0, pode ser
importado/exportado. Seja de forma separada ou em conjunto com templates
e hosts.

[comment]: # ({/new-2d2da55e})

[comment]: # ({new-18d7ef55})
#### Configuração

Para definir um mapeamento de valores:

-   Acesse: *Administração → Geral*
-   Selecione *Mapeamento de valor* no combo situado no canto superior
    direito
-   Clique no botão *Criar mapeamento de valor* (ou no nome de um
    mapeamento existente)

![](../../../../assets/en/manual/config/items/value_map.png)

Parâmetros de um mapeamento de valores:

|Parâmetro|Descrição|
|----------|-----------|
|*Nome*|Identificador único de um conjunto de mapeamento de valores.|
|*Mapeamentos*|Mapeamentos individuais - pares de relação "identificador/descrição".|

Para adicionar um novo mapeamento de valores clique no botão
*Adicionar*.

[comment]: # ({/new-18d7ef55})

[comment]: # ({new-6241051d})
#### Forma de funcionamento

Por exemplo, um dos itens predefinidos do Zabbix Agent é o 'Ping to the
server (TCP)' que pode utilizar o mapeamento de valores chamado 'Service
state' para apresentar seus valores.

![](../../../../assets/en/manual/config/items/value_map2.png)

No formulário de [configuração do item](item) você pode ver a referência
do item com o mapeamento de valor através do campo *Mostrar valor*:

![](../../../../assets/en/manual/config/items/show_value.png)

Então, em *Monitoramento → Dados recentes* o mapeamento é aproveitado
para apresentar 'Up' (com o valor coletado entre parênteses).

![](../../../../assets/en/manual/config/items/value_display.png){width="600"}

Na cessão *Dados recentes* os valores apresentados estão limitados a 20
símbolos. Se o mapeamento de valores for utilizado, a regra de
encurtamento de conteúdo não será aplicada. 

::: notetip
Um valor
sendo apresentado em forma "legível" para humanos é também A value being
displayed in a human-readable form is also easier to understand when
receiving notifications.
:::

Sem um mapeamento de valores você apenas obteria isso:

![](../../../../assets/en/manual/config/items/value_display2.png){width="600"}

[comment]: # ({/new-6241051d})
