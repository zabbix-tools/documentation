[comment]: # translation:outdated

[comment]: # ({9707aa57-cd1d3c06})
# 2 Pré-processamento de valor de item

[comment]: # ({/9707aa57-cd1d3c06})

[comment]: # ({72e036f6-cb2c0d06})

#### Visão geral

O pré-processamento permite definir regras de transformação para os 
valores recebidos nos itens. Uma ou várias transformações são possíveis
antes de salvar a informação no banco de dados.

Transformações são executadas na ordem em que são definidas.
O pré-processamento é feito pelo Zabbix Server ou Proxy (se os itens são
monitorados pelo Proxy).

Note que a conversão para o tipo de valor desejado (como definido na
configuração do item) é executada no fim da série de pré-processamento;
as conversões, no entanto, também podem ocorrer caso requerido pela 
etapa de pré-processamento correspondente. Consulte [detalhes de 
pré-processamento](/manual/config/items/preprocessing/preprocessing_details) para mais informações técnicas.

Veja também: [Exemplos de uso](/manual/config/items/preprocessing/examples)

[comment]: # ({/72e036f6-cb2c0d06})

[comment]: # ({bd00e264-033c164f})

#### Configuração

Regras de pré-processamento são definidas na aba de **Pré-processamento**
na tela de [configuração](/manual/config/items/item#configuration) do item.

![](../../../../assets/en/manual/config/items/item_c.png){width="600"}

::: noteimportant
Um item se tornará [não suportado](/manual/config/items/item#unsupported_items) se qualquer uma das etapas de pré-processamento 
falhar, a menos que tenha sido especificado um tratamento de erro customizado usando 
a opção *Personalizado em caso de falha* para transformações suportadas.\
\
Para itens de log, os metadados de log (sem valor) sempre atualizarão o estado do item 
não suportado, tornando-o novamente suportado, mesmo que o erro inicial tenha
ocorrido após o recebimento do valor do log a partir do Agent.
:::

[Macros de usuário](/manual/config/macros/user_macros) e macros de usuário com contexto são suportadas nos parâmetros 
de pré-processamento de valores dos itens, incluindo código JavaScript.

::: noteclassic
O contexto é ignorado quando uma macro é substituída por seu valor.
O valor da macro é inserido no código tal como é, não sendo possível inserir caracteres
de escape adicionais após a gravação do valor no código JavaScript.
Por favor, esteja ciente que este comportamento pode causar erros de JavaScript em 
alguns casos.

:::

|Tipo|<|<|
|----|-|-|
|<|*Transformação*|Descrição|
|Texto|<|<|
|<|*Expressão regular*|Combina o valor com o <padrao> da expressão regular e substitui o valor com a <saida>. A expressão regular suporta a extração de no máximo 10 grupos capturados com a sequência \\N. Falha em combinar o valor de entrada tornará o item não suportado.<br>Parâmetros:<br>**padrão** - expressão regular<br>**saída** - modelo de formatação de saída. Uma sequência de escape \\N (onde N=1…9) é substituída pelo N-ésimo grupo correspondente. Uma sequência de escape \\0 é substituída pelo texto verificado.<br>Por favor, consulte a seção  [expressões regulares](/manual/regular_expressions#example) para alguns exemplos disponíveis.<br>Se você ativar a opção *Personalizado em caso de falha*, o item não se tornará não suportado no caso de falha em uma etapa de pré-processamento e é possível especificar opções de tratamento de erro personalizadas: seja para descartar o valor, configurar um valor específico ou configurar uma mensagem de erro específica.|
|^|*Substituir*|Encontre o texto buscado e substitua por outro (ou nada). Todas as ocorrências do texto buscado serão substituídas.<br>Parâmetros:<br>**texto buscado** - o texto para encontrar e substituir, sensível à maiúsculas e minúsculas (obrigatório)<br>**substituição** - o texto com o qual substituir o valor buscado. O texto de substituição também pode ser vazio, permitindo apagar o texto buscado quando encontrado.<br>É possível utilizar sequências de escape para buscar ou substituir quebras de página, caracter de retorno, tabulações e espaços "\\n \\r \\t \\s"; contrabarra pode ser escapada como "\\\\" e sequências de escape podem ser escapadas como "\\\\n". O escape de quebras de linha, caracter de retorno e tabulação é feito automaticamente durante descobertas de baixo-nível.|
|^|*Remover*|Remove os caracteres especificados do início e fim do valor.|
|^|*Remover à direita*|Remove os caracteres especificados do fim do valor.|
|^|*Remover à esquerda*|Remove os caracteres especificados do início do valor.|
|Dados estruturados|<|<|
|<|*XML XPath*|Extraia um valor ou fragmento de dados XML usando a funcionalidade XPath.<br>Para esta opção funcionar, o Zabbix Server deve ser compilado com suporte a libxml.<br>Exemplos:<br>`number(/document/item/value)` irá extrair `10` de `<document><item><value>10</value></item></document>`<br>`number(/document/item/@attribute)` irá extrair `10` de `<document><item attribute="10"></item></document>`<br>`/document/item` irá extrair `<item><value>10</value></item>` de `<document><item><value>10</value></item></document>`<br>Note que namespaces não são suportados.<br>Se você marcar a opção *Personalizado em caso de falha*, o item não se tornará não suportado no caso de falha em uma etapa de pré-processamento e é possível especificar opções de tratamento de erro personalizadas: seja para descartar o valor, configurar um valor específico ou configurar uma mensagem de erro específica.|
|^|*JSON Path*|Extraia um valor ou fragmento de dados JSON usando a [funcionalidade JSONPath](/manual/config/items/preprocessing/jsonpath_functionality).<br>Se você marcar a opção *Personalizado em caso de falha*, o item não se tornará não suportado no caso de falha em uma etapa de pré-processamento e é possível especificar opções de tratamento de erro personalizadas: seja para descartar o valor, configurar um valor específico ou configurar uma mensagem de erro específica.|
|^|*CSV para JSON*|Converte dados CSV para o formato JSON.<br>Para mais informações, veja: [pré-processamento CSV para JSON](/manual/config/items/preprocessing/csv_to_json#csv_header_processing).|
|^|*XML para JSON*|Converte dados no formato XML para JSON.<br>Para mais informações, veja: [Regras de serialização](/manual/config/items/preprocessing/javascript/javascript_objects#serialization_rules).<br>Se você marcar a opção *Personalizado em caso de falha*, o item não se tornará não suportado no caso de falha em uma etapa de pré-processamento e é possível especificar opções de tratamento de erro personalizadas: seja para descartar o valor, configurar um valor específico ou configurar uma mensagem de erro específica.|
|Aritmética|<|<|
|<|*Multiplicador customizado*|Multiplica o valor pelo número inteiro ou ponto flutuante especificado.<br>Use esta opção para converter valores recebidos em KB, MBps, etc. em B, Bps. Caso contrário o Zabbix não consegue configurar os [prefixos](/manual/appendix/suffixes) (K, M, G, etc.) corretamente.<br>*Note* que se o tipo de informação do item é *Numérico (unsigned)*, valores de entrada com uma parte fracionada serão encurtados (p.e. '0.9' se tornará '0') antes que o multiplicador customizado seja aplicado.<br>Suportado: notação científica, por exemplo, `1e+70` (desde a versão 2.2); macros de usuário e macros LLD (desde a versão 4.0); textos que incluem macros, por exemplo, `{#MACRO}e+10`, `{$MACRO1}e+{$MACRO2}` (desde a versão 5.2.3)<br>As macros devem resolver para um número inteiro ou ponto flutuante.<br>Se você marcar a opção *Personalizado em caso de falha*, o item não se tornará não suportado no caso de falha em uma etapa de pré-processamento e é possível especificar opções de tratamento de erro personalizadas: seja para descartar o valor, configurar um valor específico ou configurar uma mensagem de erro específica.|
|Mudança|<|<|
|<|*Mudança simples*|Calcula a diferença entre os valores atual e anterior.<br>Calculado como  **valor** - **valor\_anterior**, onde<br>*valor* - valor atual; *valor\_anterior* - valor recebido anteriormente<br>Esta opção pode ser útil para medir um valor em constante crescimento. Se o valor atual é menor que o valor recebido anteriormente, o Zabbix descarta essa diferença (não armazena nada) e aguarda um novo valor.<br>Apenas uma operação de mudança por item é permitida.<br>Se você marcar a opção *Personalizado em caso de falha*, o item não se tornará não suportado no caso de falha em uma etapa de pré-processamento e é possível especificar opções de tratamento de erro personalizadas: seja para descartar o valor, configurar um valor específico ou configurar uma mensagem de erro específica.|
|^|*Mudança por segundo*|Calcula a mudança de valor (diferença entre os valores atual e anterior) em velocidade por segundo.<br>Calculado como (**valor** - **valor\_anterior**)/(**tempo** - **tempo\_anterior**), onde<br>*valor* - valor atual; *valor\_anterior* - valor recebido anteriormente; *tempo* - registro de data e hora atual; *tempo\_anterior* - registro de data e hora do valor recebido anteriormente.<br>Esta opção é extremamente útil para obter a velocidade por segundo de um valor em constante crescimento. Se o valor atual for menor que o valor anterior, o Zabbix descarta a diferença (não armazena nada) e aguarda por outro valor. Isto ajuda a trabalhar corretamente com, por exemplo, encapsulamento (estouro) de contadores SNMP 32-bit.<br>*Nota*: Como este cálculo pode produzir números de ponto flutuante, é recomendado configurar o 'Tipo de informação' para *Numérico (float)*, mesmo que os números brutos de entrada sejam inteiros. Isto é especialmente relevante para números pequenos onde a parte decimal é importante. Se os valores de ponto flutuante são grandes e podem exceder o tamanho do campo 'float' incorrendo na possível perda de todo o valor, é sugerido usar o tipo *Numérico (unsigned)* e assim descartar apenas a parter decimal.<br>Apenas uma operação de mudança por item é permitida.<br>Se você marcar a opção *Personalizado em caso de falha*, o item não se tornará não suportado no caso de falha em uma etapa de pré-processamento e é possível especificar opções de tratamento de erro personalizadas: seja para descartar o valor, configurar um valor específico ou configurar uma mensagem de erro específica.|
|Sistemas numéricos|<|<|
|<|*Booleano para decimal*|Converte um valor do formato booleano para decimal. A representação textual é traduzida para 0 ou 1. Assim, 'TRUE' é armazenado como 1 e 'FALSE' é armazenado como 0. Todos os valores são avaliados de forma não sensível a maiúsculas e minúsculas. Atualmente os valores reconhecidos são, para:<br>*TRUE* - true, t, yes, y, on, up, running, enabled, available, ok, master<br>*FALSE* - false, f, no, n, off, down, unused, disabled, unavailable, err, slave<br>Adicionalmente, qualquer valor numérico diferente de zero é considerado ser TRUE e zero é considerado ser FALSE.<br>Se você marcar a opção *Personalizado em caso de falha*, o item não se tornará não suportado no caso de falha em uma etapa de pré-processamento e é possível especificar opções de tratamento de erro personalizadas: seja para descartar o valor, configurar um valor específico ou configurar uma mensagem de erro específica.|
|^|*Octal para decimal*|Converte um valor do formato octal para decimal.<br>Se você marcar a opção *Personalizado em caso de falha*, o item não se tornará não suportado no caso de falha em uma etapa de pré-processamento e é possível especificar opções de tratamento de erro personalizadas: seja para descartar o valor, configurar um valor específico ou configurar uma mensagem de erro específica.|
|^|*Hexadecimal para decimal*|Converte um valor do formato hexadecimal para decimal.<br>Se você marcar a opção *Personalizado em caso de falha*, o item não se tornará não suportado no caso de falha em uma etapa de pré-processamento e é possível especificar opções de tratamento de erro personalizadas: seja para descartar o valor, configurar um valor específico ou configurar uma mensagem de erro específica.|
|Scripts personalizados|<|<|
|<|*JavaScript*|Insira o código JavaScript no bloco que aparece quando clicando no campo parâmetro ou sobre o ícone de edição (lápis).<br>Note que o tamanho disponível para o código JavaScript depende do [banco de dados usado](#custom_script_limit).<br>Para mais informações, veja: [Pré-processamento Javascript](/manual/config/items/preprocessing/javascript).|
|Validação|<|<|
|<|*Dentro do intervalo*|Define um intervalo dentro do qual um valor deve estar pela definição dos valores mínimo/máximo (inclusive).<br>Valores numéricos são aceitos (incluindo qualquer quantidade de dígitos, partes  decimal e exponencial opcionais e valores negativos). Macros de usuário e descoberta de baixo-nível podem ser utilizadas. O valor mínimo deve ser menor que o valor máximo.<br>Ao menos um valor deve existir.<br>Se você marcar a opção *Personalizado em caso de falha*, o item não se tornará não suportado no caso de falha em uma etapa de pré-processamento e é possível especificar opções de tratamento de erro personalizadas: seja para descartar o valor, configurar um valor específico ou configurar uma mensagem de erro específica.|
|^|*Corresponde com expressão regular*|Especifique uma expressão regular com a qual um valor deve corresponder.<br>Se você marcar a opção *Personalizado em caso de falha*, o item não se tornará não suportado no caso de falha em uma etapa de pré-processamento e é possível especificar opções de tratamento de erro personalizadas: seja para descartar o valor, configurar um valor específico ou configurar uma mensagem de erro específica.|
|^|*Não corresponde com expressão regular*|Especifique uma expressão regular com a qual um valor não deve corresponder.<br>Se você marcar a opção *Personalizado em caso de falha*, o item não se tornará não suportado no caso de falha em uma etapa de pré-processamento e é possível especificar opções de tratamento de erro personalizadas: seja para descartar o valor, configurar um valor específico ou configurar uma mensagem de erro específica.|
|^|*Verifica erro em JSON*|Verifica mensagem de erro no nível de aplicação localizada no JSONpath. Encerra o processamento se tiver sucesso e a mensagem não estiver vazia; caso contrário, continua processando com o valor existente antes desta etapa de pré-processamento. Note que estes erros de serviços externos são reportados ao usuário como são, sem adicionar informações da etapa de pré-processamento.<br>Nenhum erro será reportado em caso de falha ao analisar um JSON inválido.<br>Se você marcar a opção *Personalizado em caso de falha*, o item não se tornará não suportado no caso de falha em uma etapa de pré-processamento e é possível especificar opções de tratamento de erro personalizadas: seja para descartar o valor, configurar um valor específico ou configurar uma mensagem de erro específica.|
|^|*Verifica erro em XML*|Verifica mensagem de erro no nível de aplicação localizada no XPath. Encerra o processamento se tiver sucesso e a mensagem não estiver vazia; caso contrário, continua processando com o valor existente antes desta etapa de pré-processamento. Note que estes erros de serviços externos são resportados ao usuário como são, sem adicionar informações da etapa de pré-processamento.<br>Nenhum erro será reportado em caso de falha ao analisar o XML.<br>Se você marcar a opção *Personalizado em caso de falha*, o item não se tornará não suportado no caso de falha em uma etapa de pré-processamento e é possível especificar opções de tratamento de erro personalizadas: seja para descartar o valor, configurar um valor específico ou configurar uma mensagem de erro específica.|
|^|*Verifica erro usando expressão regular*|Verifica mensagem de erro no nível de aplicação usando expressão regular. Encerra o processamento se tiver sucesso e a mensagem não estiver vazia; caso contrário, continua processando com o valor existente antes desta etapa de pré-processamento. Note que estes erros de serviços externos são resportados ao usuário como são, sem adicionar informações da etapa de pré-processamento.<br>Parâmetros:<br>**padrão** - expressão regular<br>**saída** - modelo de formatação de saída. Uma sequência de escape \\N (onde N=1…9) é substituída pelo N-ésimo grupo correspondente. Uma sequência de escape \\0 é substituída pelo texto verificadot.<br>Se você marcar a opção *Personalizado em caso de falha*, o item não se tornará não suportado no caso de falha em uma etapa de pré-processamento e é possível especificar opções de tratamento de erro personalizadas: seja para descartar o valor, configurar um valor específico ou configurar uma mensagem de erro específica.|
|^|*Verifica valor não suportado*|Verifica se há erro na recepção do valor de um item. Normalmente isto faria o item se tornar não suportado, mas você pode modificar este comportamento configurando as opções de tratamento *Personalizado em caso de falha*: seja para descartar o valor, configurar um valor específico (neste caso o item continua suportado e o valor pode ser usado nos gatilhos (triggers)) ou configurar uma mensagem de erro específica. Note que para esta etapa de pré-processamento, a opção *Personalizado em caso de falha* aparecerá em cinza e sempre selecionada.<br>Esta etapa é sempre executada como a primeira etapa de pré-processamento e é colocada acima de todas as outras após salvar as alterações no item. Ela pode ser usada apenas uma vez.<br>Suportado desde a versão 5.2.0.|
|Supressão|<|<|
|<|*Descartar inalterado*|Descarta um valor caso ele não tenha mudado.<br>Se um valor é descartado, ele não é salvo no banco de dados e o Zabbix Server não tem conhecimento de que este valor foi recebido. Nenhum gatilho será avaliado, como resultado, nenhum problema será criado/resolvido para os gatilhos relacionados. Funções funcionarão baseadas apenas nos dados salvos no banco de dados. Como tendências são contruídas baseadas nos dados do banco de dados, se não há valor salvo por uma hora então também não haverá dados de tendência para aquela hora.<br>Apenas uma opção de supressão pode ser especificada para um item.<br>*Note* que é possível para itens monitorados pelo Zabbix Proxy que diferenças de valores muito pequenas (menos de 0.000001) sejam corretamente não descartadas pelo Proxy, mas armazenadas no histórico como sendo de mesmo valor se o banco de dados do Zabbix Server [não tiver sido atualizado](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500#enabling_extended_range_of_numeric_float_values).|
|^|*Descartar inalterado com intervalo*|Descarta um valor caso ele não tenha mudado dentro de um período de tempo definido (em segundos).<br>Valores inteiros positivos são suportados para especificar os segundos (mínimo - 1 segundo). Sufixos de tempo podem ser usados neste campo (p.e. 30s, 1m, 2h, 1d). Macros de usuário e descoberta de baixo-nível podem ser usadas neste campo.<br>Se um valor é descartado, ele não é salvo no banco de dados e o Zabbix Server não tem conhecimento de que este valor foi recebido. Nenhum gatilho será avaliado, como resultado, nenhum problema será criado/resolvido para os gatilhos relacionados. Funções funcionarão baseadas apenas nos dados salvos no banco de dados. Como tendências são contruídas baseadas nos dados do banco de dados, se não há valor salvo por uma hora então também não haverá dados de tendência para aquela hora.<br>Apenas uma opção de supressão pode ser especificada para um item.<br>*Note* que é possível para itens monitorados pelo Zabbix Proxy que diferenças de valores muito pequenas (menos de 0.000001) sejam corretamente não descartadas pelo Proxy, mas armazenadas no histórico como sendo de mesmo valor se o banco de dados do Zabbix Server [não tiver sido atualizado](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500#enabling_extended_range_of_numeric_float_values).|
|Prometheus|<|<|
|<|*Padrão Prometheus*|Use a seguinte instrução para extrair os dados necessários das métricas do Prometheus.<br>Consulte [verificações Prometheus](/manual/config/items/itemtypes/prometheus) para mais detalhes.|
|^|*Prometheus para JSON*|Converte métricas Prometheus para JSON.<br>Consulte [verificações Prometheus](/manual/config/items/itemtypes/prometheus) para mais detalhes.|

::: noteimportant
 Para as etapas de pré-processamento de alteração e supressão o
Zabbix tem que se recordar do último valor para calcular/comparar com
o novo valor conforme necessidade. Estes últimos valores são tratados
pelo gerenciador de pré-processamento. Se o Zabbix Server ou Proxy for
reiniciado ou houver qualquer mudança nas etapas de pré-processamento,
o último valor do item correspondente é resetado, resultando em:

-   para as etapas *Alterações simples*, *Alterações por segundo* - o próximo 
    valor será ignorado pois não há valor anterior para cálculo da alteração;
-   para as etapas *Descartar inalterado*, *Descartar inalterado com intervalo* -
    o próximo valor nunca será descartado, mesmo que tivesse de ser 
    descartado por causa das regras de descarte.


:::

O parâmetro *Tipo de informação* é apresentado no fundo da página quando
ao menos uma etapa de pré-processamento for definida. Se necessário, é
possível alterar o tipo de informação sem sair da tela de *Pré-processamento*. 
Consulte [Criando um item](/manual/config/items/item) para uma descrição detalhada do parâmetro.

::: notetip
Se você usa um multiplicador customizado ou armazena valores como
*Alterações por segundo* para itens com o tipo de informação configurado
para *Numérico (unsigned)* e o valor calculado resultante for na verdade 
um número de ponto flutuante, o valor calculado ainda é aceito como um
valor correto pela retirada da parte decimal e armazenamento do valor 
como um inteiro.
:::



[comment]: # ({/bd00e264-033c164f})

[comment]: # ({57ae62c4-ad67af50})
#### Testes

O teste das etapas de pré-processamento é útil para certificar-se de que 
segmentos complexos de pré-processamento entreguem os resultados que
são esperados, sem aguardar que o valor do item seja recebido e pré-processado.

![](../../../../assets/en/manual/config/items/test_item_steps.png){width="600"}

É possível testar:

-   contra um valor hipotético
-   contra um valor real de um host

Cada etapa de pré-processamento pode ser testada individualmente
assim como todas as etapas podem ser testadas juntas. Quando você
clica no botão *Testar* ou *Testar todas as etapas* na aba de Ações,
uma janela de teste é aberta.

[comment]: # ({/57ae62c4-ad67af50})

[comment]: # ({8b3e7abb-45423233})

##### Testando valor hipotético

![](../../../../assets/en/manual/config/items/test_item_p.png){width="600"}

|Parâmetro|Descrição|
|---------|-----------|
|*Obter valor do host*|Se você deseja testar um valor hipotético, deixe esta opção desmarcada.<br>Veja também: [Testando valor real](#testing_real_value).|
|*Valor*|Informe um valor de entrada para testar.<br>Clicando no campo deste parâmetro ou no botão de visualizar/editar abrirá uma janela com área de texto para entrada do valor ou bloco de código.|
|*Não suportado*|Marque esta opção para testar um valor não suportado.<br>Esta opção é útil para testar a etapa de pré-processamento *Verificar valores não suportados*.|
|*Horário*|Horário do valor de entrada é mostrado: `now` *(somente leitura)*, sendo 'now' o horário atual.|
|*Valor anterior*|Informe um valor de entrada anterior para fazer comparação.<br>Apenas para etapas de pré-processamento *Alteração* e *Supressão*.|
|*Horário anterior*|Informe um valor de horário anterior para fazer comparação.<br>Apenas para etapas de pré-processamento *Alteração* e *Supressão*.<br>O valor padrão é baseado no campo de 'Intervalo de atualização' do item (se '1m', então este campo é preenchido com `now-1m`). Se nada for especificado ou se o usuário não tiver acesso ao host, o padrão é `now-30s`.|
|*Macros*|Se quaisquer macros estiverem sendo usadas, elas são listadas junto de seus valores. Os valores são editáveis para propósitos de teste, mas as alterações serão salvas apenas dentro do contexto de teste.|
|*Sequência de fim de linha*|Selecione a sequência de fim de linha para múltiplos valores de entrada:<br>**LF (line feed)** - sequência de alimentação de linha *(comum Linux)*<br>**CRLF (carriage-return line-feed)** - sequência de alimentação de linha *(comum Windows)*.|
|*Etapas de pré-processamento*|Etapas de pré-processamento são listadas; o resultado do teste é mostrado para cada etapa depois que o botão *Testar* é acionado.<br>Se uma etapa falhar durante o teste, um ícone de erro é mostrado. A descrição do erro é apresentada ao mover o mouse sobre o ícone.<br>No caso de "Personalização em caso de falha" estar especificada para a etapa e tal ação for executada, uma nova linha aparecerá logo após a linha de teste da etapa de pré-processamento, mostrando qual ação foi executada e qual resultado foi produzido (erro ou valor).|
|*Resultado*|O resultado final do teste das etapas de pré-processamento é mostrado em todos os casos quando todas as etapas são testadas juntas (quando você clica em *Testar todas as etapas*).<br>O tipo de conversão para o tipo de valor do item também é mostrado, por exemplo `Resultado convertido para Numérico (unsigned)`.|

Clique em *Testar* para ver o resultado junto de cada etapa de pré-processamento.

Os valores de teste são armazenados entre as sessões de teste para cada
etapa individual ou todas as etapas, permitindo ao usuário alterar as etapas
de pré-processamento ou configuração de item e então retornar para a 
janela de teste sem ter que reinserir informações. Estes valores são perdidos, 
no entanto, caso a página seja atualizada.

O teste é executado pelo Zabbix Server. O Frontend envia uma requisição
correspondente para o Server e aguarda pelo resultado. A requisição contém
a entrada de valor e etapas de pré-processamento (com macros de usuário
expandidas). Para etapas de *Alteração* e *Supressão*, um valor anterior e 
horário opcionais podem ser especificados. O Server responde com os
resultados para cada etapa de pré-processamento.

Todos os erros técnicos ou erros de validação de entrada são mostrados
na caixa de erro no topo da janela de teste.

[comment]: # ({/8b3e7abb-45423233})

[comment]: # ({b5c19ce6-43637da5})

##### Testando valor real

Para testar o pré-processamento contra um valor real:

-   Marque a opção *Obter valor do host*
-   Informe ou verifique os parâmetros do host (endereço de host, porta, nome do proxy/sem
    proxy) e detalhes específicos do item (como comunidade SNMPv2 ou credenciais de segurança SNMPv3). Estes campos são sensíveis ao contexto:
    -   Os valores são pré-preenchidos quando possível, p.e. para itens que 
        requerem um Agent, pela aquisição da informação a partir da interface 
        de Agent do host selecionada
    -   Os valores tem de ser preenchidos manualmente para modelos de itens
    -   Valores de macro em texto-plano são resolvidos
    -   Campos onde o valor (ou parte do valor) seja secreto ou uma macro
        Vault aparecem vazios e devem ser informados manualmente. Se algum 
        parâmetro de item contém um valor de macro secreto, a seguinte 
        mensagem de alerta é mostrada: "O item contém macros definidas pelo
        usuário com valores secretos. Os valores para estas macros devem ser
        informados manualmente."
    -   Os campos ficam desabilitados quando desnecessários no contexto do
        tipo de item (p.e. o endereço do host e campos de Proxy ficam desabilitados
        para itens calculados)
-   Clique em *Obter valor e testar* para testar o pré-processamento

![](../../../../assets/en/manual/config/items/test_item_p2.png){width="600"}

Se você especificou um mapeamento de valor no formulário de configuração do item
(campo 'Mostrar valor'), o diálogo de teste do item mostrará uma outra linha após o 
resultado final, identificada como 'Resultado com mapeamento de valor aplicado'.

Parâmetros que são específicos para obteção de um valor real de um host:

|Parâmetro|Descrição|
|---------|-----------|
|*Obter valor do host*|Marque esta opção para obter um valor real do host.|
|*Endereço do Host*|Informe o endereço do host.<br>Este endereço é automaticamente preenchido pelo endereço da interface de host do item.|
|*Porta*|Informe a porta do host.<br>Este endereço é automaticamente preenchido pela porta da interface de host do item.|
|*Campos adicionais para interfaces SNMP<br>(Versão SNMP, Comunidade SNMP, Nome de contexto, etc.)*|Veja [Configurando monitoramento SNMP](/manual/config/items/itemtypes/snmp#step_2) para detalhes adicionais na configuração de uma interface SNMP (v1, v2 and v3).<br>Estes campos são automaticamente preenchidos a partir da interface de host do item.|
|*Proxy*|Especifique o Proxy se o host for monitorado por um Proxy.<br>Este campo é automaticamente preenchido pelo Proxy do host (se houver).|

Para os demais parâmetros, veja [Testando valores hipotéticos](#testing_hypothetical_value) acima.

[comment]: # ({/b5c19ce6-43637da5})
