[comment]: # translation:outdated

[comment]: # ({new-f89c7380})
# - \#6 Contadores de performance Windows

[comment]: # ({/new-f89c7380})

[comment]: # ({new-47e1ae8b})
#### Visão geral

Você pode monitorar os contadores de performance do Windows com a chave
*perf\_counter\[\]*.

Por exemplo:

    perf_counter["\Processor(0)\Interrupts/sec"]

ou

    perf_counter["\Processor(0)\Interrupts/sec", 10]

Para maiores detalhes sobre o uso desta chave, consulte o manual de
[chaves específicas para
Windows](/pt/manual/config/items/itemtypes/zabbix_agent/win_keys).

Para obter uma lista completa dos contadores de performance disponíveis
para monitorar, você pode executar:

    typeperf -qx

[comment]: # ({/new-47e1ae8b})

[comment]: # ({new-38d80c32})
#### Representação numérica

Os nomes dos contadores de performance podem variar dependendo da versão
do Windows, idioma ou de configurações locais e isso pode criar
barreiras para a criação de um template de monitoração para monitorar o
mesmo serviço em diferentes versões do sistema operacional ou idiomas.

Os contadores podem ser referenciados por seus nomes ou por seu numero,
sendo que este último não varia entre as diversas versões, idiomas e
configurações locais. Você poderá utilizar a representação numérica ao
invés dos nomes.

Para descobrir o número de um contador de performance, execute o
**regedit**, e navegue até
*HKEY\_LOCAL\_MACHINE\\SOFTWARE\\Microsoft\\Windows
NT\\CurrentVersion\\Perflib\\009*.

Esta entrada do registro deverá ter conteúdo similar ao apresentado a
seguir:

    1
    1847
    2
    System
    4
    Memory
    6
    % Processor Time
    10
    File Read Operations/sec
    12
    File Write Operations/sec
    14
    File Control Operations/sec
    16
    File Read Bytes/sec
    18
    File Write Bytes/sec
    ....

Aqui você poderá encontrar os números de cada contador de performance,
tal qual o do '\\System\\% Processor Time':

    System → 2
    % Processor Time → 6

Você poderá então utilizar estes números para representar o caminho do
contador:

    \2\6

[comment]: # ({/new-38d80c32})

[comment]: # ({new-96efda14})
#### Parâmetros dos contadores de performance

É possível criar parâmetros nos contadores de performance na
configuração do agente para possibilitar sua monitoração.

Exemplos da adição de um contador no arquivo de configuração do agente:

       PerfCounter=UserPerfCounter1,"\Memory\Page Reads/sec",30
       ou
       PerfCounter=UserPerfCounter2,"\4\24",30

Com os parâmetros em seu lugar você poderá utilizar *UserPerfCounter1*
ou *UserPerfCounter2* como as chaves nos itens.

Lembre-se de reiniciar o Zabbix Agent após alterar o arquivo de
configuração.

[comment]: # ({/new-96efda14})
