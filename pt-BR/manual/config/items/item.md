[comment]: # translation:outdated

[comment]: # ({4d20cd83-50342b23})
# 1 Criando um item

[comment]: # ({/4d20cd83-50342b23})

[comment]: # ({53303682-d3a04061})
#### Visão geral

Para criar um item no frontend do Zabbix, faça o seguinte:

-   Vá para: *Configuração* → *Hosts*
-   Clique em *Items* na linha do hosti
-   Clique em *Criar item* no canto superior direito da tela
-   Insira os parâmetros do item no formulário

Você também pode criar um item abrindo um existente, pressionando o botão *Clonar* e, em seguida, salvando com um nome diferente.

[comment]: # ({/53303682-d3a04061})

[comment]: # ({new-d0a0a311})
#### Configuração

![](../../../../assets/en/manual/config/item.png)

Atributos do item:

|Parâmetro|Descrição|
|----------|-----------|
|*Nome*|Nome do item.<br>É possível utilizar as macros posicionais para se referir a algum parâmetro utilizado na chave por ele coletada:<br>**$1, $2...$9** - se referindo respectivamente ao primeiro, segundo.... nono parâmetro na chave<br>Por exemplo: "Free disk space on $1"<br>Se a chave do item for "vfs.fs.size\[/,free\]", a descrição do item será automaticamente atualizada para "Free disk space on /"|
|*Tipo*|Tipo do item. Veja a sessão de [tipos de item](itemtypes).|
|*Chave*|Chave do item.<br>A lista de [chaves de item](itemtypes) poderá ser de grande valia.<br>A chave precisa ser única no host.<br>Se o tipo do item for 'Agente Zabbix', 'Agente Zabbix (ativo)', 'Verificação simples' ou 'Zabbix agregado', o vlor da chave precisa ser suportado pelo Zabbix Agent ou Zabbix Server.<br>Consulte teambém: o [formato das chaves](/pt/manual/config/items/item/key).|
|*Interface do Host*|Selecione a interface do host. Este campo está disponível somente quando o item é editado em nível de host.|
|*Tipo da informação*|Como será armazenado o dado no banco de dado após convertido, se for necessária a conversão.<br>**Numérico (inteiro)** - inteiro de 64bits<br>**Numérico (fracionário)** - número com ponto flutuante (decimais)<br>Valores negativos podem ser armazenados.<br>Range válido (para MySQL): de -999999999999.9999 a 999999999999.9999 (double(16,4)).<br>A partir da versão 2.2 do Zabbix também é suportado o recebimento de valores em notações científicas. Ex. 1e+70, 1e-70.<br>**Caractere** - texto curto, limitado a 255 bytes<br>**Log** - arquivo de log. Será necessária a configuração como log para chaves de 'eventlog' (a não ser que você planeje extrair outro tipo de dado deste item, com o parâmetro `output`).<br>**Texto** - texto sem limite de tamanho|
|*Tipo de dado*|Será utilizado o tipo inteiro para para itens com os tipos de dado:<br>**Lógico ou booleano** - a representação textual é traduzida em 0 ou 1. Onde, 'TRUE' (verdadeiro) é armazenado com o valor 1 e 'FALSE' com o valor 0. A conversão não é sensível ao caso. Então valores como:<br>*TRUE* - true, t, sim, s, on, up, rodando, habilitado, disponível<br>*FALSE* - false, f, não, n, off, down, não utilizado, desabilitado, indisponível<br>Adicionalmente, qualquer numero diferente de zero é considerado como VERDADEIRO e zero é considerado FALSO.<br>**Octal** - dado em notação octal<br>**Decimal** - dado em formato decimal<br>**Hexadecimal** - dado em formato hexadecimal<br>O zabbix fará automaticamente a conversão para o formato numérico.<br>A conversão será feita pelo Zabbix Server (mesmo quando o host é monitorado através de um Zabbix Proxy).|
|*Unidades*|Se for informado um símbolo para a unidade, o Zabbix irá processar o valor recebido e irá apresenta-lo com o correto sufixo.<br>Por padrão, se o valor for superior a 1000, ele será dividido por 1000 e apresentado de forma apropriada. Por exemplo, se você configurar a unidade como sendo *bps* e receber o valor 881764, o mesmo será exibido como 881.76 Kbps.<br>Ocorre um processamento especial para as unidades **B** (byte), **Bps** (bytes por segundo), nestes casos a divisão será por 1024. Desta forma, se a unidade for definida como **B** ou **Bps** o Zabbix irá apresentar:<br>1 como 1B/1Bps<br>1024 como 1KB/1KBps<br>1536 como 1.5KB/1.5KBps<br>Ocorrerá processamento especial para as unidades de tempo:<br>**unixtime** - traduzida para "yyyy.mm.dd hh:mm:ss". Para traduzir corretamente o valor recebido deverá ser do tipo *Número (inteiro)*.<br>**uptime** - traduzido para "hh:mm:ss" ou "N dias, hh:mm:ss"<br>Por exemplo, se você receber o valor 881764 (segundos), ele será apresentado como "10 dias, 04:56:04"<br>**s** - traduzido para "yyy mmm ddd hhh mmm sss ms"; o parâmetro é tratado como uma quantidade de segundos.<br>Por exemplo, se você receber o valor 881764 (segundos), ele será apresentado como "10d 4h 56m"<br>Apenas as três maiores unidades serão apresentadas. Ex. "1m 15d 5h" ou "2h 4m 46s". Se não existirem dias a apresentar, somente duas unidades serão apresentadas - "1m 5h" (sem informação de horas, segundos ou milisegundos). Se o valor for inferior a 0.001 o mesmo será traduzido para "< 1 ms".<br>Veja mais sobre o tema em [unidades](#unit_blacklist).|
|*Usar multiplicador customizado*|Se você ativar esta opção, todos os valores recebidos serão multiplicados pelo valor informado.<br>Use esta opção para converter valores recebidos no formato de KB, MBps, etc em B, Bps. De outra forma o Zabbix não conseguirá apresentar corretamente [os sufixos](/pt/manual/config/triggers/suffixes) (K, M, G etc).<br>A partir do Zabbix 2.2, o uso de notação científica também passou a ser suportado.|
|*Intervalo atualização (em segundos)*|Atualiza o dado a cada N segundos.<br>*Nota*: Se for configurado para '0', a coleta do item não será monitorada. Entretanto, se for informado um intervalo personalizado (flexível ou agendamento) e este campo estiver com valor diferente de '0' ele será monitorado com intervalo definido, durante a duração do intervalo personalizado.|
|*Intervalos personalizados*|Você pode criar regras personalizadas para a coleta do item:<br>**Flexível** - cria uma exceção ao *Intervalo de atualização* (um intervalo com frequência diferente em um momento específico)<br>**Agendamento** - cria um agendamento de coleta.<br>Para informações detalhadas veja [intervalos personalizados](/pt/manual/config/items/item/custom_intervals). O agendamento é suportado desde o Zabbix 3.0.0.<br>*Nota*: esta funcionalidade não está disponível para itens ativos.|
|*Período de retenção de histórico (em dias)*|Quantidade de dias a manter o histórico detalhado de coletas no banco de dados. Dados mais antigos do que o aqui definido serão removidos pelo processo de 'housekeeper' (limpeza de dados).<br>A partir do Zabbix 2.2, este valor pode ser sobrescrito pela configuração global disponível em *Administração → Geral → [Limpeza de dados](/pt/manual/web_interface/frontend_sections/administration/general#housekeeper)*. Se a configuração global existir, uma mensagem de alerta será apresentada:<br>![](../../../../assets/en/manual/config/override_item.png)<br>É recomendável manter os dados no banco pelo menor tempo possível para reduzir a quantidade de registros de histórico. Ao invés de guardar o histórico, prefira guardar as médias.<br>Veja também [Histórico e médias](/pt/manual/config/items/history_and_trends).|
|*Período de retenção de médias (em dias)*|A cada hora as médias irão guardar de forma agregada o resultado das funções 'min, max, avg e count' sobre os dados coletados no período).<br>A partir do Zabbix 2.2, este valor pode ser sobrescrito pela configuração global disponível em *Administração → Geral → [Limpeza de dados](/pt/manual/web_interface/frontend_sections/administration/general#housekeeper)*. Se a configuração global existir, será apresentada uma mensagem de alerta:<br>![](../../../../assets/en/manual/config/override_trends.png)<br>*Nota:* O armazenamento de médias não está disponível para itens não numérico, como os caracteres, log e texto.<br>Veja também [Histórico e médias](/pt/manual/config/items/history_and_trends).|
|*Armazenar valor*|**Sem alterar** - não ocorre nenhum pré-processamento<br>**Delta (alterações por segundo)** - ocorre pré-processamento através da fórmula: (**value**-**prev\_value**)/(**time**-**prev\_time**), onde<br>*value* - valor atua<br>*value\_prev* - valor anterior<br>*time* - momento atual<br>*prev\_time* - momento da coleta do valor anterior<br>Esta configuração é muito útil para velocidade por segundo em uma sequência de valores.<br>*Nota*: Se o valor atual for menor do que o anterior, o Zabbix irá descartar a diferença (não salva nada) e irá aguardar outro valor. Este ajuda a funcionar corretamente com, por exemplo, um estouro de um contador SNMP de 32 bits.<br>**Delta (alterações simples)** - ocorre pré-processamento através da fórmula (**value**-**prev\_value**), onde<br>*value* - valor atual<br>*value\_prev* - valor anterior|
|*Mostrar valor*|Aplica um mapeamento de valores ao item. O mapeamento de valores não modifica o dado recebido, ocorre apenas um processamento no momento de apresentar a informação.<br>Este recurso funciona apenas com números inteiros.<br>Por exemplo, "Estado de serviços do windows".|
|*Formato de hora em Log*|Disponível para itens do tipo **Log** apenas. Marcadores suportados:<br>\* **y**: *Ano (0001-9999)*<br>\* **M**: *Mês (01-12)*<br>\* **d**: *Dia (01-31)*<br>\* **h**: *Hora (00-23)*<br>\* **m**: *Minuto (00-59)*<br>\* **s**: *Segundo (00-59)*<br>Se estiver em branco não será processado.<br>Por exemplo, considere a seguinte linha de um log do Zabbix Agent:<br>" 23480:20100328:154718.045 Zabbix agent started. Zabbix 1.8.2 (revision 11211)."<br>Ela começa com seis caracteres relativos ao PID, seguido pela data, hora e o resto da linha.<br>O formato de hora do log poderia ser "pppppp:yyyyMMdd:hhmmss".<br>Note que os caracteres "p" e ":" são espaços diferentes de "yMdhms".|
|*Nova aplicação*|Informe o nome de uma nova aplicação para o item.|
|*Aplicações*|Associe o item a um ou mais aplicações já existentes.|
|*Popular o campo do inventário*|Você pode selecionar um campo do inventário que será preenchido automaticamente a medida que chegarem novos valores para este item. Observe que para ele funcionar o modo de inventário no host deverá estar como *Automático*. O manual do recurso de [inventário](/pt/manual/config/hosts/inventory) apresenta mais detalhes.|
|*Descrição*|Informe uma descrição para o item.|
|*Ativo*|Selecione o checkbox para habilita-lo para ser processado.|

Você também pode criar um item ao clicar para editar um outro item e
pressionar o botão *Clonar*, alterar os parâmetros e salvar com uma
chave diferente.

::: noteclassic
Quando se tenta editar no nível do host um item que é da
associação com um [template](/pt/manual/config/templates), vários campos
estarão habilitados apenas para leitura. No alto do formulário de edição
do item tem um link para acesso rápido ao item no nível de template,
onde você poderá modificar todos os campos.
:::

[comment]: # ({/new-d0a0a311})

[comment]: # ({new-4f9077b8})
##### Exceções a conversão de unidade

Por padrão, ao especificar uma unidade em um item o resultado será que
um multiplicador será adicionado junto com o sufixo, por exemplo, o
valor 2048 com a unidade B será apresentado como 2KB. Para um conjunto
pré-definido e travado diretamente no código esta conversão não será
feita:

-   ms
-   RPM
-   rpm
-   %

[comment]: # ({/new-4f9077b8})

[comment]: # ({new-006c45ea})
#### Itens não suportados

Um item passar ao estado "não suportado" caso não consiga receber o dado
no horário previsto por algum motivo. A tentativa de coleta de dado
destes itens será refeita em um intervalo fixo, configurável na sessão
de
[administração](/pt/manual/web_interface/frontend_sections/administration/general?&#other_parameters).

[comment]: # ({/new-006c45ea})

[comment]: # ({new-4b126a62})
#### Form buttons

Buttons at the bottom of the form allow to perform several operations.

|   |   |
|---|---|
|![](../../../../assets/en/manual/config/button_add.png)|Add an item. This button is only available for new items.|
|![](../../../../assets/en/manual/config/button_update.png)|Update the properties of an item.|
|![](../../../../assets/en/manual/config/button_clone.png)|Create another item based on the properties of the current item.|
|![](../../../../assets/en/manual/config/button_execute.png)|Execute a check for a new item value immediately. Supported for **passive** checks only (see [more details](/manual/config/items/check_now)).<br>*Note* that when checking for a value immediately, configuration cache is not updated, thus the value will not reflect very recent changes to item configuration.|
|![](../../../../assets/en/manual/config/button_test.png)|Test if item configuration is correct by getting a value.|
|![](../../../../assets/en/manual/config/button_clear.png)|Delete the item history and trends.|
|![](../../../../assets/en/manual/config/button_delete.png)|Delete the item.|
|![](../../../../assets/en/manual/config/button_cancel.png)|Cancel the editing of item properties.|

[comment]: # ({/new-4b126a62})

[comment]: # ({new-8cb32bf5})
#### Text data limits

Text data limits depend on the database backend. Before storing text
values in the database they get truncated to match the database value
type limit:

|Database|Type of information|<|<|
|--------|-------------------|-|-|
|^|Character|Log|Text|
|MySQL|255 characters|65536 bytes|65536 bytes|
|PostgreSQL|255 characters|65536 characters|65536 characters|
|Oracle|255 characters|65536 characters|65536 characters|

[comment]: # ({/new-8cb32bf5})




[comment]: # ({new-aeb2b058})
#### Unit conversion

By default, specifying a unit for an item results in a multiplier prefix
being added - for example, an incoming value '2048' with unit 'B' would
be displayed as '2KB'.

To prevent a unit from conversion, use the `!` prefix, for example,
`!B`. To better understand how the conversion works with and without the
exclamation mark, see the following examples of values and units:

    1024 !B → 1024 B
    1024 B → 1 KB
    61 !s → 61 s
    61 s → 1m 1s
    0 !uptime → 0 uptime
    0 uptime → 00:00:00
    0 !! → 0 !
    0 ! → 0

::: noteclassic
Before Zabbix 4.0, there was a hardcoded unit stoplist
consisting of `ms`, `rpm`, `RPM`, `%`. This stoplist has been
deprecated, thus the correct way to prevent converting such units is
`!ms`, `!rpm`, `!RPM`, `!%`.
:::

[comment]: # ({/new-aeb2b058})

[comment]: # ({new-d4406a73})
#### Custom script limit

Available custom script length depends on the database used:

|   |   |   |
|---|---|---|
|*Database*|*Limit in characters*|*Limit in bytes*|
|**MySQL**|65535|65535|
|**Oracle Database**|2048|4000|
|**PostgreSQL**|65535|not limited|
|**SQLite (only Zabbix proxy)**|65535|not limited|

[comment]: # ({/new-d4406a73})

[comment]: # ({new-2cd40177})
#### Unsupported items

An item can become unsupported if its value cannot be retrieved for some
reason. Such items are still rechecked at their standard *[Update
interval](/manual/config/items/item?#configuration)*.

Unsupported items are reported as having a NOT SUPPORTED state.

[comment]: # ({/new-2cd40177})
