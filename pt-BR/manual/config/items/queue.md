[comment]: # translation:outdated

[comment]: # ({new-c981b8ac})
# 10 Fila

[comment]: # ({/new-c981b8ac})

[comment]: # ({new-bf7ec8ec})
#### Visão geral

A fila apresenta os itens que estão com algum atraso em sua coleta. Ela
é uma representação **logical** dos dados. Não existe nenhuma fila "IPC"
ou qualquer outro mecanismo de fila no Zabbix.

As estatísticas apresentadas pela fila são um bom indicador de
performance do Zabbix Server.

A fila é coletada diretamente do processo do Zabbix Server através do
protocolo JSON. A informação só está disponível enquanto o processo do
Zabbix Server estiver em execução.

[comment]: # ({/new-bf7ec8ec})

[comment]: # ({new-f73c67a5})
#### Consultando a fila

Para consultar a fila acesse *Administração → Fila*. A opção *Visão
geral* pode ser selecionada no canto superior direito da tela.

![](../../../../assets/en/manual/config/items/queue.png){width="600"}

As células que estão verdes indicam normalidade naquela faixa de
análise. Na imagem acima a fila apresenta um item aguardando por 5
segundos e cinco itens aguardando por 30 segundos. Uma vez que temos
estes indicativos, a dúvida normal é: e o que está causando isso? Quais
itens estão atrasados?

Para ver o detalhamento dos itens em atraso, selecione *Detalhes* na
caixa de seleção situada no canto superior da tela. Agora você deverá
estar vendo uma lista com os itens em atraso.

![](../../../../assets/en/manual/config/items/queue_details.png){width="600"}

A partir da análise destes dados poderá ser possível localizar e
corrigir a causa raiz destes atrasos.

Ter um ou dois itens em atraso talvez não seja motivo para alarmes. Eles
poderão estar atualizados em poucos segundos. Entretanto, se você
visualizar uma grande quantidade de itens em atraso, isso poderá ser um
sério problema para sua monitoração.

![](../../../../assets/en/manual/config/items/queue2.png){width="600"}

[comment]: # ({/new-f73c67a5})

[comment]: # ({new-1e93992f})
#### Item da fila

Existe um item especial de uso interno ao Zabbix que responde pela chave
**zabbix\[queue,<from>,<to>\]** e pode ser utilizado para
monitorar a saúde da fila do Zabbix. Ele irá retornar a quantidade de
itens em atraso em um determinado tempo. Para maiores detalhes favor
consultar o manual de [itens
internos](/pt/manual/config/items/itemtypes/internal).

[comment]: # ({/new-1e93992f})
