[comment]: # translation:outdated

[comment]: # ({3e9b958a-52403269})
# 4 Verificações IPMI

[comment]: # ({/3e9b958a-52403269})

[comment]: # ({1824bdb3-2f30e610})
#### Visão geral

Você pode monitorar a saúde e disponibilidade de dispositivos IPMI 
(Interface Inteligente de Gerenciamento de Plataforma) no Zabbix. 
Para executar verificações IPMI o Zabbix Server deve ser inicialmente
[configurado](/manual/installation/install#configure_the_sources) com suporte a IPMI.


IPMI é uma interface padronizada para gerenciamento remoto "fora de banda" (OOB)
e "luzes apagadas" (LOM) de sistemas de computador. Ela permite monitorar o estado
do hardware diretamente das chamadas placas de gerenciamento fora de banda, 
independentemente do sistema operacional ou mesmo se a máquina está ou não ligada.

O monitoramento IPMI do Zabbix funciona apenas com dispositivos que possuem suporte a IPMI (HP
iLO, DELL DRAC, IBM RSA, Sun SSP, etc.).

Desde o Zabbix 3.4, um novo processo gerenciador de IPMI foi adicionado às verificações
IPMI agendadas por sondagem IPMI. Agora um host é sempre sondado por apenas um sondador
IPMI por vez, reduzindo o número de conexões abertas para controladores BMC. Com essas 
mudanças é seguro aumentar o número de sondadores IPMI sem se preocupar com sobrecarga de controlador BMC. O processo gerenciador de IPMI é automaticamente iniciado quando ao menos um
sondador IPMI é iniciado.

Veja também [problemas conhecidos](/manual/installation/known_issues#ipmi_checks) para verificações IPMI.

[comment]: # ({/1824bdb3-2f30e610})

[comment]: # ({new-5d32b87c})
#### Configuração

[comment]: # ({/new-5d32b87c})

[comment]: # ({a9808789-4f35db5e})
##### Configuração de Host

Um host deve estar configurado para processar verificações IPMI. Uma interface
IPMI deve ser adicionada, com os respectivos números de IP e porta, e os
parâmetros de autenticação IPMI devem estar definidos.

Consulte a [configuração de hosts](/manual/config/hosts/host) para mais detalhes.

[comment]: # ({/a9808789-4f35db5e})

[comment]: # ({b3bd355d-f7bb9e76})
##### Configuração do Server

Por padrão, o Zabbix Server não é configurado para iniciar qualquer sondador IPMI, 
portanto quaisquer itens IPMI adicionados não funcionarão. Para mudar isto, abra o 
arquivo de configuração do Zabbix Server ([zabbix\_server.conf](/manual/appendix/config/zabbix_server)) como root e procure pela seguinte linha:

    # StartIPMIPollers=0

Descomente esta linha e configure a quantidade de sondadores para, digamos, 3, ficando então
desta forma:

    StartIPMIPollers=3

Salve o arquivo e reinicie o zabbix\_server em seguida.

[comment]: # ({/b3bd355d-f7bb9e76})

[comment]: # ({616d319c-b0db4658})
##### Configuração de item

Quando [configurando um item](/manual/config/items/item) no nível de host:

-   Selecione 'Agente IPMI' como o *Tipo*
-   Informe uma [chave](/manual/config/items/item/key) de item única dentro do host (digamos, ipmi.fan.rpm)
-   Para *Interface de host* selecione a interface IPMI relevante (IP e porta). Note que uma interface IPMI deve existir no host.
-   Defina o *Sensor IPMI* (por exemplo 'FAN MOD 1A RPM' no Dell
    Poweredge) do qual obter as métricas. Por padrão, o ID do sensor
    deve ser especificado. É também possível usar prefixos antes do
    valor:
    -   `id:` - para especificar o ID do sensor;
    -   `name:` - para especificar o nome completo do sensor. Isto pode ser útil em 
        situação em que os sensores só podem ser distinguidos especificando o nome completo.
-   Selecione o tipo de informação respectivo ('Numérico (número flutuante)' neste caso;
    para sensores discretos - 'Numérico (unsigned)'), units (unidades - provavelmente
    'rpm') e quaisquer outros atributos de item necessários

[comment]: # ({/616d319c-b0db4658})

[comment]: # ({fabb57dd-af2b187b})
##### Verificações suportadas

A tabela abaixo descreve itens nativos que são suportados nas
verificações IPMI do agente.

|Chave de item|<|<|<|
|--------|-|-|-|
|▲|Descrição|Valore de retorno|Comentários|
|ipmi.get|<|<|<|
|<|Informação relacionada ao sensor IPMI.|Objeto JSON|Este item pode ser usado para [descoberta de sensores IPMI](/manual/discovery/low_level_discovery/examples/ipmi_sensors#overview).<br>Suportado desde o Zabbix 5.0.0.|

[comment]: # ({/fabb57dd-af2b187b})

[comment]: # ({ca4317e0-1e03516a})
#### Tempo limite (timeout) e término de sessão

O tempo limite das mensagens IPMI e contagem de novas tentativas são 
definidos na biblioteca OpenIPMI. Devido ao desenho atual da OpenIPMI, 
não é possível tornar estes valores configuráveis no Zabbix, nem na 
interface e nem em nível de item.

A expiração de sessão IPMI por inatividade é de 60 +/-3 segundos para LAN. 
Atualmente não é possível implementar envio periódico de comando Activate Session
com OpenIPMI. Se não houver verificações de item IPMI do Zabbix para um BMC
particular por mais do que o tempo limite de sessão configurado no BMC, então
a próxima verificação IPMI depois da expiração de tempo irá expirar devido ao 
tempo limite individual das mensagens, novas tentativas ou recebimento de erro. 
Após isso, uma nova sessão é aberta e é iniciada uma releitura completa do BMC. 
Se você quiser evitar releituras desnecessárias do BMC é aconselhável configurar
o intervalo de sondagem do item para um valor abaixo ao tempo limite de sessão do IPMI 
por inatividade configurado no BMC.

[comment]: # ({/ca4317e0-1e03516a})

[comment]: # ({62a878f8-3ea36861})
#### Notas sobre sensores discretos do IPMI

Para encontrar sensores em um host inicie o Zabbix Server com **DebugLevel=4**
habilitado. Aguarde alguns minutos e encontre os registros de descoberta de sensores
no arquivo de log do Zabbix Server:

    $ grep 'Added sensor' zabbix_server.log
    8358:20130318:111122.170 Added sensor: host:'192.168.1.12:623' id_type:0 id_sz:7 id:'CATERR' reading_type:0x3 ('discrete_state') type:0x7 ('processor') full_name:'(r0.32.3.0).CATERR'
    8358:20130318:111122.170 Added sensor: host:'192.168.1.12:623' id_type:0 id_sz:15 id:'CPU Therm Trip' reading_type:0x3 ('discrete_state') type:0x1 ('temperature') full_name:'(7.1).CPU Therm Trip'
    8358:20130318:111122.171 Added sensor: host:'192.168.1.12:623' id_type:0 id_sz:17 id:'System Event Log' reading_type:0x6f ('sensor specific') type:0x10 ('event_logging_disabled') full_name:'(7.1).System Event Log'
    8358:20130318:111122.171 Added sensor: host:'192.168.1.12:623' id_type:0 id_sz:17 id:'PhysicalSecurity' reading_type:0x6f ('sensor specific') type:0x5 ('physical_security') full_name:'(23.1).PhysicalSecurity'
    8358:20130318:111122.171 Added sensor: host:'192.168.1.12:623' id_type:0 id_sz:14 id:'IPMI Watchdog' reading_type:0x6f ('sensor specific') type:0x23 ('watchdog_2') full_name:'(7.7).IPMI Watchdog'
    8358:20130318:111122.171 Added sensor: host:'192.168.1.12:623' id_type:0 id_sz:16 id:'Power Unit Stat' reading_type:0x6f ('sensor specific') type:0x9 ('power_unit') full_name:'(21.1).Power Unit Stat'
    8358:20130318:111122.171 Added sensor: host:'192.168.1.12:623' id_type:0 id_sz:16 id:'P1 Therm Ctrl %' reading_type:0x1 ('threshold') type:0x1 ('temperature') full_name:'(3.1).P1 Therm Ctrl %'
    8358:20130318:111122.172 Added sensor: host:'192.168.1.12:623' id_type:0 id_sz:16 id:'P1 Therm Margin' reading_type:0x1 ('threshold') type:0x1 ('temperature') full_name:'(3.2).P1 Therm Margin'
    8358:20130318:111122.172 Added sensor: host:'192.168.1.12:623' id_type:0 id_sz:13 id:'System Fan 2' reading_type:0x1 ('threshold') type:0x4 ('fan') full_name:'(29.1).System Fan 2'
    8358:20130318:111122.172 Added sensor: host:'192.168.1.12:623' id_type:0 id_sz:13 id:'System Fan 3' reading_type:0x1 ('threshold') type:0x4 ('fan') full_name:'(29.1).System Fan 3'
    8358:20130318:111122.172 Added sensor: host:'192.168.1.12:623' id_type:0 id_sz:14 id:'P1 Mem Margin' reading_type:0x1 ('threshold') type:0x1 ('temperature') full_name:'(7.6).P1 Mem Margin'
    8358:20130318:111122.172 Added sensor: host:'192.168.1.12:623' id_type:0 id_sz:17 id:'Front Panel Temp' reading_type:0x1 ('threshold') type:0x1 ('temperature') full_name:'(7.6).Front Panel Temp'
    8358:20130318:111122.173 Added sensor: host:'192.168.1.12:623' id_type:0 id_sz:15 id:'Baseboard Temp' reading_type:0x1 ('threshold') type:0x1 ('temperature') full_name:'(7.6).Baseboard Temp'
    8358:20130318:111122.173 Added sensor: host:'192.168.1.12:623' id_type:0 id_sz:9 id:'BB +5.0V' reading_type:0x1 ('threshold') type:0x2 ('voltage') full_name:'(7.1).BB +5.0V'
    8358:20130318:111122.173 Added sensor: host:'192.168.1.12:623' id_type:0 id_sz:14 id:'BB +3.3V STBY' reading_type:0x1 ('threshold') type:0x2 ('voltage') full_name:'(7.1).BB +3.3V STBY'
    8358:20130318:111122.173 Added sensor: host:'192.168.1.12:623' id_type:0 id_sz:9 id:'BB +3.3V' reading_type:0x1 ('threshold') type:0x2 ('voltage') full_name:'(7.1).BB +3.3V'
    8358:20130318:111122.173 Added sensor: host:'192.168.1.12:623' id_type:0 id_sz:17 id:'BB +1.5V P1 DDR3' reading_type:0x1 ('threshold') type:0x2 ('voltage') full_name:'(7.1).BB +1.5V P1 DDR3'
    8358:20130318:111122.173 Added sensor: host:'192.168.1.12:623' id_type:0 id_sz:17 id:'BB +1.1V P1 Vccp' reading_type:0x1 ('threshold') type:0x2 ('voltage') full_name:'(7.1).BB +1.1V P1 Vccp'
    8358:20130318:111122.174 Added sensor: host:'192.168.1.12:623' id_type:0 id_sz:14 id:'BB +1.05V PCH' reading_type:0x1 ('threshold') type:0x2 ('voltage') full_name:'(7.1).BB +1.05V PCH'

Para compreender os tipos e estado dos sensores, acesse uma cópia das
especificações IPMI 2.0 em <http://www.intel.com/content/www/us/en/servers/ipmi/ipmi-specifications.html> (No momento da documentação o material mais recente era
<http://www.intel.com/content/dam/www/public/us/en/documents/product-briefs/second-gen-interface-spec-v2.pdf>, sendo que não foi encontrada versão em Português Brasileiro)

O primeiro parâmetro com o qual começar é o "reading\_type". Use a tabela "Table 42-1,
Event/Reading Type Code Ranges" das especificações para interpretar o código
"reading\_type" (tipo de leitura). A maioria dos sensores no nosso exemplo possui "reading\_type:0x1" 
o que significa sensor de limite, a tabela "Table 42-3, Sensor Type Codes" mostra que 
"type:0x1" significa sensor de temperatura, "type:0x2" -
sensor de tensão, "type:0x4" - Cooler, etc. Sensores de limite algumas vezes são
chamados de "analógicos" por medirem parâmetros contínuos como temperatura, tensão, 
revoluções por minuto.

Outro exemplo - um sensor com "reading\_type:0x3". A tabela "Table 42-1,
Event/Reading Type Code Ranges" diz que códigos de leitura 02h-0Ch
significam sensor "Discreto Genérico". Sensores discretos possuem até 15 
estados possíveis (em outras palavras - até 15 bits significativos). 
Por exemplo, para sensor 'CATERR' com "type:0x7" a "Table 42-3, Sensor Type Codes"
mostra que este tipo significa "Processor" (Processador) e o significado dos bits individuais
é: 00h (o último bit significante) - IERR, 01h - Thermal Trip, etc.

Há uns poucos sensores com "reading\_type:0x6f" em nosso exemplo. Para
estes sensores a tabela "Table 42-1, Event/Reading Type Code Ranges" aconselha
usar a "Table 42-3, Sensor Type Codes" para interpretar os bits significativos.
Por exemplo, o sensor 'Power Unit Stat' tem o tipo "type:0x9" que significa
"Power Unit" (Unidade de Potência). O deslocamento (offset) 00h significa "PowerOff/Power Down" (Desligar). 
Em outras palavras se o último bit significativo é 1, então o servidor está
desligado. Para testar este bit, pode ser usada a [função](/manual/appendix/triggers/functions) **bitand** com máscara '1'. A expressão de gatilho poderia ser algo como:

    bitand(last(/www.example.com/Power Unit Stat,#1),1)=1

para alertar sobre o desligamento de um servidor.

[comment]: # ({/62a878f8-3ea36861})

[comment]: # ({64f86fd7-761eb9da})
##### Notas sobre nomes de sensores discretos em OpenIPMI-2.0.16, 2.0.17, 2.0.18 e 2.0.19

Nomes de sensores discretos em OpenIPMI-2.0.16, 2.0.17 e 2.0.18 frequentemente
possuem um "`0`" adicional (ou algum outro dígito ou letra) na parte final. 
Por exemplo, enquanto `ipmitool` e OpenIPMI-2.0.19 mostram o nome de sensor
como "`PhysicalSecurity`" ou "`CATERR`", em OpenIPMI-2.0.16, 2.0.17
e 2.0.18 os nomes seriam "`PhysicalSecurity0`" ou "`CATERR0`", respectivamente.

Quando configurando um item IPMI com Zabbix Server usando OpenIPMI-2.0.16,
2.0.17 e 2.0.18, use terminações de nome com "0" no campo *sensor IPMI*
dos itens de agente IPMI. Quando o seu Zabbix Server for atualizado para uma nova
distribuição Linux, que usa OpenIPMI-2.0.19 (ou mais recente), os itens com estes
sensores IPMI discretos se tornarão "NÃO SUPORTADOS". Você terá que alterar seus
nomes de *sensor IPMI* (remova o '0' do final) e aguardar um momento até que eles
se tornem "Habilitados" novamente.

[comment]: # ({/64f86fd7-761eb9da})



[comment]: # ({f5e161d3-2adee003})
##### Notas sobre disponibilidade simultânea de sensor de limite (threshold) e sensor discreto

Alguns agentes IPMI oferecem suporte a um sensor de limite e um sensor discreto
sob o mesmo nome. Nas versões de Zabbix anteriores a 2.2.8 e 2.4.3, o primeiro
sensor fornecido era escolhido. Desde as versões 2.2.8 e 2.4.3, a preferência é
sempre dada ao sensor de limite.

[comment]: # ({/f5e161d3-2adee003})

[comment]: # ({67492977-69164d8f})
##### Notas sobre encerramento de conexão

Se as verificações IPMI não forem executadas (por qualquer razão: todos os
itens IPMI desabilitados/não suportados, host desabilitado/deletado, host
em manutenção, etc.) a conexão IPMI será encerrada no Zabbix Server ou Proxy
dentro de 3 a 4 horas, dependendo do horário em que o Zabbix Server/Proxy 
foi iniciado.

[comment]: # ({/67492977-69164d8f})
