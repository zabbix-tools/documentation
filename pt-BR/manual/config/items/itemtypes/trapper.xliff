<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="pt-BR" datatype="plaintext" original="manual/config/items/itemtypes/trapper.md">
    <body>
      <trans-unit id="5e482050" xml:space="preserve">
        <source># 12 Trapper items</source>
        <target state="needs-translation"># 12 Itens de captura (trapper)</target>
      </trans-unit>
      <trans-unit id="03e7aedb" xml:space="preserve">
        <source>#### Overview

Trapper items accept incoming data instead of querying for it.

It is useful for any data you might want to "push" into Zabbix.

To use a trapper item you must:

-   have a trapper item set up in Zabbix
-   send in the data into Zabbix</source>
        <target state="needs-translation">#### Visão geral

Itens de captura (trapper) aceitam entrada de dados em vez de
consultá-los.

É útil para qualquer dado que você queira "empurrar" para dentro
do Zabbix.

Para usar um item de captura você deve:

-   ter um item de captura (trapper) configurado no Zabbix
-   enviar o dado para dentro do Zabbix</target>
      </trans-unit>
      <trans-unit id="5d32b87c" xml:space="preserve">
        <source>#### Configuration</source>
      </trans-unit>
      <trans-unit id="62c22fbb" xml:space="preserve">
        <source>##### Item configuration

To configure a trapper item:

-   Go to: *Data collection* → *Hosts*
-   Click on *Items* in the row of the host
-   Click on *Create item*
-   Enter parameters of the item in the form

![](../../../../../assets/en/manual/config/items/itemtypes/trapper_item.png)

All mandatory input fields are marked with a red asterisk.

The fields that require specific information for trapper items are:

|   |   |
|--|--------|
|*Type*|Select **Zabbix trapper** here.|
|*Key*|Enter a key that will be used to recognize the item when sending in data.|
|*Type of information*|Select the type of information that will correspond the format of data that will be sent in.|
|*Allowed hosts*|List of comma delimited IP addresses, optionally in CIDR notation, or DNS names.&lt;br&gt;If specified, incoming connections will be accepted only from the hosts listed here.&lt;br&gt;If IPv6 support is enabled then '127.0.0.1', '::127.0.0.1', '::ffff:127.0.0.1' are treated equally and '::/0' will allow any IPv4 or IPv6 address.&lt;br&gt;'0.0.0.0/0' can be used to allow any IPv4 address.&lt;br&gt;Note, that "IPv4-compatible IPv6 addresses" (0000::/96 prefix) are supported but deprecated by [RFC4291](https://tools.ietf.org/html/rfc4291#section-2.5.5).&lt;br&gt;Example: 127.0.0.1, 192.168.1.0/24, 192.168.3.1-255, 192.168.1-10.1-255, ::1,2001:db8::/32, mysqlserver1, zabbix.example.com, {HOST.HOST}&lt;br&gt;Spaces and [user macros](/manual/config/macros/user_macros) are allowed in this field since Zabbix 2.2.0.&lt;br&gt;Host macros {HOST.HOST}, {HOST.NAME}, {HOST.IP}, {HOST.DNS}, {HOST.CONN} are allowed in this field since Zabbix 4.0.2.|

::: noteclassic
You may have to wait up to 60 seconds after saving the item
until the server picks up the changes from a configuration cache update,
before you can send in values.
:::</source>
        <target state="needs-translation">##### Configuração do item

Para configurar um item de captura (trapper):

-   Vá até: *Configuração* → *Hosts*
-   Clique em *Itens* na linha do host
-   Clique em *Criar item*
-   Preencha os parâmetros do item no formulário

![](../../../../../assets/en/manual/config/items/itemtypes/trapper_item.png)

Todos os campos obrigatórios estão marcados com um asterisco vermelho.

Os campos que requerem informação específica para os itens de captura são:

|   |   |
|---|---|
|*Tipo*|Selecione **Capturador Zabbix** (Trapper) aqui.|
|*Chave*|Informe uma chave que será utilizada para reconhecer o item no processo de envio de dados.|
|*Tipo de informação*|Selecione o tipo de informação que corresponderá ao formato de dado que será enviado para dentro do Zabbix.|
|*Hosts permitidos*|Lista de endereços IP separados por vírgula, opcionalmente em notação CIDR, ou nomes de host.&lt;br&gt;Se especificado, conexões de entrada serão aceitas apenas dos hosts listados aqui.&lt;br&gt;Se o suporte a IPv6 estiver habilitado então '127.0.0.1', '::127.0.0.1', '::ffff:127.0.0.1' são tratados como iguais e '::/0' permitirá qualquer endereço IPv4 ou IPv6.&lt;br&gt;'0.0.0.0/0' pode ser usado para permitir qualquer endereço IPv4.&lt;br&gt;Note que "endereços IPv6 compatíveis com IPv4" (com prefixo 0000::/96) são suportados mas obsoletos pela [RFC4291](https://tools.ietf.org/html/rfc4291#section-2.5.5).&lt;br&gt;Exemplo: Server=127.0.0.1, 192.168.1.0/24, 192.168.3.1-255, 192.168.1-10.1-255, ::1,2001:db8::/32, zabbix.domain&lt;br&gt;Espaços e [macros de usuário](/manual/config/macros/user_macros) são permitidos neste campo desde o Zabbix 2.2.0.&lt;br&gt;As macros de host: {HOST.HOST}, {HOST.NAME}, {HOST.IP}, {HOST.DNS}, {HOST.CONN} são permitidas neste campo desde o Zabbix 4.0.2.|

::: noteclassic
Você pode ter que esperar até 60 segundos após salvar o item,
até que o Server colete as mudanças de uma atualização de cache
de configuração, antes de poder enviar valores para o Zabbix.
:::</target>
      </trans-unit>
      <trans-unit id="5f246209" xml:space="preserve">
        <source>##### Sending in data

In the simplest of cases, we may use
[zabbix\_sender](/manual/concepts/sender) utility to send in some 'test
value':

    zabbix_sender -z &lt;server IP address&gt; -p 10051 -s "New host" -k trap -o "test value"

To send in the value we use these keys:

*-z* - to specify Zabbix server IP address

*-p* - to specify Zabbix server port number (10051 by default)

*-s* - to specify the host (make sure to use the 'technical' [host
name](/manual/config/hosts/host#configuration) here, instead of the
'visible' name)

*-k* - to specify the key of the item we just defined

*-o* - to specify the actual value to send

::: noteimportant
Zabbix trapper process does not expand macros used
in the item key in attempt to check corresponding item key existence for
targeted host.
:::</source>
        <target state="needs-translation">##### Envio de dados

Nos casos mais simples, podemos utilizar o utilitário
[zabbix\_sender](/manual/concepts/sender) para enviar algum
'valor de teste':

    zabbix_sender -z &lt;endereço IP do Server&gt; -p 10051 -s "Novo host" -k trap -o "valor de teste"

Para enviar o valor para o Zabbix usamos estas chaves:

*-z* - para informar o endereço IP do Zabbix Server

*-p* - para especificar o número de porta do Zabbix Server (10051 por padrão)

*-s* - para informar o host (certifique-se de usar o [nome de host](/manual/config/hosts/host#configuration) 'técnico' aqui, em vez do nome 'visível')

*-k* - para especificar a chave do item que acabamos de definir

*-o* - para especificar o valor real a ser enviado

::: noteimportant
O processo de captura do Zabbix (trapper) não expande macros usadas 
na chave do item na tentativa de verificar a existência de chave de 
item correspondente para o host de destino.
:::</target>
      </trans-unit>
      <trans-unit id="1e9d760a" xml:space="preserve">
        <source>##### Display

This is the result in *Monitoring → Latest data*:

![](../../../../../assets/en/manual/config/items/itemtypes/trapped_data.png){width="600"}

Note that if a single numeric value is sent in, the data graph will show
a horizontal line to the left and to the right of the time point of the
value.</source>
        <target state="needs-translation">##### Apresentação

Este é o resultado em *Monitoramento → Últimos dados*:

![](../../../../../assets/en/manual/config/items/itemtypes/trapped_data.png){width="600"}

Note que se um valor numérico único é enviado, o gráfico de dados mostrará
uma linha horizontal à esquerda e à direita do ponto temporal (data/hora) do valor.</target>
      </trans-unit>
    </body>
  </file>
</xliff>
