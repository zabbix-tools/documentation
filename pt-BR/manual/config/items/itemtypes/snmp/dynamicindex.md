[comment]: # translation:outdated

[comment]: # ({1e57128f-c5a81a82})
# 1 Índices Dinâmicos

[comment]: # ({/1e57128f-c5a81a82})

[comment]: # ({145569e4-e95b21bb})
#### Visão Geral

Embora possa ter encontrado o índice do item que deseja (por exemplo, uma interface de rede) entre os diversos OIDs SNMP você deve certificar que trata-se de um índice fixo, não assumindo com apenas uma pesquisa, que o número permanecerá sempre o mesmo.

Os índices podem ser dinâmicos, fazendo com que mudem após algum tempo e o monitoramento do item pare de funcionar.

Para evitar este cenário, é possível definir um OID que faz referência a um índice com possibilidade de sofrer alteração.

Por exemplo, se você precisa identificar o valor do índice que corresponda a interface **GigabitEthernet0/1** em um equipamento Cisco para adicionar em **ifInOctets** , use o OID a seguir:

    ifInOctets["index","ifDescr","GigabitEthernet0/1"]

[comment]: # ({/145569e4-e95b21bb})

[comment]: # ({new-6a6d7709})
##### A sintaxe

Uma sintaxe especial é utilizada para estas consultas:

**<OID of data>\["índice","<OID base>","<texto a
procurar>"\]**

|Parameter|Description|
|---------|-----------|
|OID base|OID principal para pesquisar o dado que se deseja o índice.|
|Índice|Método de processamento. Atualmente é suportado apenas um método:<br>**índice** – procura pelo índice para adicionar ao OID Base|
|OID base do índice|Este OID será utilizado junto com a informação de índice para recuperar o valor solicitado.|
|Texto a procurar|Texto utilizado para localizar o valor que se deseja. Sensitivo ao caso.|

[comment]: # ({/new-6a6d7709})

[comment]: # ({new-3da26466})
#### Exemplo

Recuperando uso de memória do processo do *apache*.

Se utilizar esta sintaxe de OID:

    HOST-RESOURCES-MIB::hrSWRunPerfMem["index","HOST-RESOURCES-MIB::hrSWRunPath", "/usr/sbin/apache2"]

O número do índice será localizado aqui:

    ...
    HOST-RESOURCES-MIB::hrSWRunPath.5376 = STRING: "/sbin/getty"
    HOST-RESOURCES-MIB::hrSWRunPath.5377 = STRING: "/sbin/getty"
    HOST-RESOURCES-MIB::hrSWRunPath.5388 = STRING: "/usr/sbin/apache2"
    HOST-RESOURCES-MIB::hrSWRunPath.5389 = STRING: "/sbin/sshd"
    ...

Neste caso nós encontramos o índice **5388**. Este número será
adicionado ao **OID base** para recuperar o valor que estamos
interessados:

    HOST-RESOURCES-MIB::hrSWRunPerfMem.5388 = INTEGER: 31468 KBytes

[comment]: # ({/new-3da26466})

[comment]: # ({new-beab1c6d})
#### Cache de índices

Quando um índice dinâmicos é solicitado, o Zabbix recupera e faz o cache
da tabela SNMP inteira pesquisada. Isso é feito de forma a acelerar caso
outro índice necessite de dados da mesma tabela. Observe que cada
'poller' do Zabbix mantêm um cache em separado.

Para todos os valores subsequentes, apenas o índice recuperado será
utilizado. Se ele não tiver sido alterado, solicita-se o valor desejado.
Se tiver sido alterado, a tabela SNMP será solicitada novamente.

[comment]: # ({/new-beab1c6d})
