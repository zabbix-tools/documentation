[comment]: # translation:outdated

[comment]: # ({c04902e3-78fa973d})
# 2 Special OIDs

Alguns dos OIDs SNMP mais usados são traduzidos automaticamente para uma
representação numérica pelo Zabbix. Por exemplo, **ifIndex** é traduzida
para **1.3.6.1.2.1.2.2.1.1**, **ifIndex.0** é traduzida para
**1.3.6.1.2.1.2.2.1.1.0**.

A tabela abaixo contém a lista dos OIDs especiais.

|Special OID|Identifier|Description|
|-----------|----------|-----------|
|ifIndex|1.3.6.1.2.1.2.2.1.1|Valor único para cada interface.|
|ifDescr|1.3.6.1.2.1.2.2.1.2|Um texto contendo informações sobre a interface. Deve incluir o nome do fabricante, nome do produto e versão da interface.|
|ifType|1.3.6.1.2.1.2.2.1.3|O tipo de interface, distinguido de acordo com o(s) protocolo(s) físico/link imediatamente 'abaixo' da camada de rede na pilha de protocolos.|
|ifMtu|1.3.6.1.2.1.2.2.1.4|O tamanho máximo do datagrama que pode ser enviado/recebido na interface, especificado em octetos.|
|ifSpeed|1.3.6.1.2.1.2.2.1.5|Uma estimativa da largura de banda atual da interface em bits por segundo.|
|ifPhysAddress|1.3.6.1.2.1.2.2.1.6|O endereço da interface na camada de protocolo imediatamente 'abaixo' da camada de rede na pilha de protocolos.|
|ifAdminStatus|1.3.6.1.2.1.2.2.1.7|O estado administrativo atual da interface.|
|ifOperStatus|1.3.6.1.2.1.2.2.1.8|O estado operacional atual da interface.|
|ifInOctets|1.3.6.1.2.1.2.2.1.10|O número total de octetos recebidos na interface, incluindo caracteres de enquadramento.|
|ifInUcastPkts|1.3.6.1.2.1.2.2.1.11|O número de pacotes unicast de sub-redes entregues a um protocolo de camada superior.|
|ifInNUcastPkts|1.3.6.1.2.1.2.2.1.12|O número de pacotes não unicast (ou seja, sub-rede multicast ou sub-rede broadcast) entregues a um protocolo de camada superior.|
|ifInDiscards|1.3.6.1.2.1.2.2.1.13|O número de pacotes de entrada que foram escolhidos para serem descartados mesmo que nenhum erro tenha sido detectado para evitar que sejam entregues a um protocolo de camada superior. Um possível motivo para descartar um pacote desse tipo pode ser liberar espaço no buffer.|
|ifInErrors|1.3.6.1.2.1.2.2.1.14|O número de pacotes de entrada que continham erros impedindo que fossem entregues a um protocolo de camada superior.|
|ifInUnknownProtos|1.3.6.1.2.1.2.2.1.15|O número de pacotes recebidos pela interface e que foram descartados devido a um protocolo desconhecido ou não suportado.|
|ifOutOctets|1.3.6.1.2.1.2.2.1.16|O número total de octetos transmitidos para fora da interface, incluindo caracteres de enquadramento.|
|ifOutUcastPkts|1.3.6.1.2.1.2.2.1.17|O número total de pacotes que os protocolos de nível superior solicitaram que fossem transmitidos e que não foram endereçados a um endereço multicast ou broadcast nesta subcamada, incluindo aqueles que foram descartados ou não enviados.|
|ifOutNUcastPkts|1.3.6.1.2.1.2.2.1.18|O número total de pacotes que os protocolos de nível superiror solicitaram que fossem transmitidos e que foram endereçados a um endereço multicast ou broadcast nesta subcamada, incluindo aqueles que foram descartados ou não enviados.|
|ifOutDiscards|1.3.6.1.2.1.2.2.1.19|O número de pacotes de saída que foram escolhidos para serem descartados mesmo que nenhum erro tenha sido detectado para impedir sua transmissão. Um possível motivo para descartar um pacote desse tipo pode ser liberar espaço no buffer.|
|ifOutErrors|1.3.6.1.2.1.2.2.1.20|O número de pacotes de saída que não puderam ser transmitidos devido a erros.|
|ifOutQLen|1.3.6.1.2.1.2.2.1.21|O comprimento da fila de pacotes de saída (em pacotes).|

[comment]: # ({/c04902e3-78fa973d})
