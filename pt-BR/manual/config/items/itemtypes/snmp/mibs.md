[comment]: # translation:outdated

[comment]: # ({e5c702ce-8afcd82f})
# 3 Arquivos MIB

[comment]: # ({/e5c702ce-8afcd82f})

[comment]: # ({dc23a784-f224bb08})
#### Intrudução

MIB é um acrônimo de Management Information Base. Os arquivos MIB permitem que você use a representação textual do OID (Object Identifier).

Por exemplo,

    ifHCOutOctets

a representação textual do OID é

    1.3.6.1.2.1.31.1.1.1.10

Você pode usar ambos ao monitorar dispositivos SNMP com o Zabbix, mas se sentir mais confortável ao usar a representação textual será necessário instalar os arquivos MIB.

[comment]: # ({/dc23a784-f224bb08})

[comment]: # ({c83a75e8-5aed9e1b})
#### Instalando arquivos MIB

Sistemas baseados em Debian:

    # apt install snmp-mibs-downloader
    # download-mibs

Sistemas baseados em RedHat:

    # yum install net-snmp-libs

[comment]: # ({/c83a75e8-5aed9e1b})

[comment]: # ({37833da6-5346f669})
#### Habilitando arquivo MIB

Em sistemas baseados em RedHat os arquivos MIB devem ser habilitados por padrão.
Em sistemas baseados em Debian  você tem que editar o arquivo `/etc/snmp/snmp.conf` e
comente a linha que possui `mibs :`

    # As the snmp packages come without MIB files due to license reasons, loading
    # of MIBs is disabled by default. If you added the MIBs you can re-enable
    # loading them by commenting out the following line.
    #mibs :

[comment]: # ({/37833da6-5346f669})

[comment]: # ({c14f6b7c-bb4c3c1f})
#### Testando os arquivos MIB

O teste de MIBs  SNMP pode ser feito usando o utilitário `snmpwalk`. Para instalar use as seguintes instruções.

Sistemas baseados em Debian:

    # apt install snmp

Sistemas baseados em RedHat:

    # yum install net-snmp-utils

Depois disso, o comando a seguir não deve apresentar erro ao consultar um dispositivo de rede:

    $ snmpwalk -v 2c -c public <NETWORK DEVICE IP> ifInOctets
    IF-MIB::ifInOctets.1 = Counter32: 176137634
    IF-MIB::ifInOctets.2 = Counter32: 0
    IF-MIB::ifInOctets.3 = Counter32: 240375057
    IF-MIB::ifInOctets.4 = Counter32: 220893420
    [...]

[comment]: # ({/c14f6b7c-bb4c3c1f})

[comment]: # ({e9649052-87dbd7d4})
#### Usando MIBs no Zabbix

O mais importante a ter em mente é que os processos do Zabbix não são informados sobre as alterações feitas nos arquivos MIB. Então, depois de cada mudança você deve reiniciar o Zabbix server ou Proxy:

    # service zabbix-server restart

Depois disso, as alterações feitas nos arquivos MIB entraram em vigor.

[comment]: # ({/e9649052-87dbd7d4})

[comment]: # ({dfc22742-efd836b8})
#### Usando arquivos MIB personalizados

Existem arquivos MIB padrão que vem com todas as distribuições GNU/Linux.
Mas alguns fornecedores de dispositivos fornecem seus próprios arquivos.

Digamos que você gostaria de usar o arquivo MIB
[CISCO-SMI](ftp://ftp.cisco.com/pub/mibs/v2/CISCO-SMI.my). As instruções a seguir irão baixar e instalar o arquivo MIB:

    # wget ftp://ftp.cisco.com/pub/mibs/v2/CISCO-SMI.my -P /tmp
    # mkdir -p /usr/local/share/snmp/mibs
    # grep -q '^mibdirs +/usr/local/share/snmp/mibs' /etc/snmp/snmp.conf 2>/dev/null || echo "mibdirs +/usr/local/share/snmp/mibs" >> /etc/snmp/snmp.conf
    # cp /tmp/CISCO-SMI.my /usr/local/share/snmp/mibs

Agora você deve ser capaz de usá-lo.
Tente traduzir o nome do objeto *ciscoProducts* do arquivo MIB para OID:

    # snmptranslate -IR -On CISCO-SMI::ciscoProducts
    .1.3.6.1.4.1.9.1

Se você receber erros em vez do OID, certifique-se de que todos os comandos foram executados sem erros.

Se tradução do nome do objeto funcionou, você está pronto para usar o arquivo MIB personalizado.
Observe o prefixo do nome MIB (*CISCO-SMI::*) usado na consulta. Você precisará disso ao usar uma ferramenta de linha de comando, bem como o Zabbix.

Não se esqueça de reiniciar o Zabbix server ou Proxy antes de usar o arquivo MIB no Zabbix.

::: Nota importante
Tenha em mente que os arquivos MIB podem ter dependências. Ou seja, um MIB pode exigir outro.
Para satisfazer essas dependências você tem que instalar todos os arquivos MIBs.
:::

[comment]: # ({/dfc22742-efd836b8})
