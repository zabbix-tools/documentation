[comment]: # translation:outdated

[comment]: # ({26f89f93-43646dd0})
# 2 Agente SNMP

[comment]: # ({/26f89f93-43646dd0})

[comment]: # ({new-f312fc02})
#### Visão geral

Você poderá precisar utilizar o protocolo SNMP para monitorar
dispositivos como impressoras, switches, roteadores ou nobreaks que,
normalmente, possuem interfaces SNMP habilitadas e onde é impraticável
manter um Zabbix Agent funcionando.

Para que o Zabbix Server esteja apto a receber dados coletados por um
agente SNMP, ele deverá ser
[configurado](/pt/manual/installation/install#configure_the_sources) com
o suporte SNMP.

As coletas SNMP são feitas somente através do protocolo UDP.

Desde o Zabbix 2.2.3 o Zabbix Server e o Zabbix Proxy tem a capacidade
de coletar múltiplos dados de um dispositivo a partir de uma única
requisição. Isso afeta todos os tipos de itens SNMP (itens normais SNMP,
itens SNMP com índice dinâmico, e o processo de autobusca SNMP) os
tornando mais eficientes. Mais detalhes sobre [coletas
múltiplas](#internal_workings_of_bulk_processing) podem ser obtidos.
Desde o Zabbix 2.4 existe uma opção chamada "Usar requisições em lote"
em cada interfce SNMP que permite habilitar ou desabilitar requisições
em lote naquela interface.

Desde o Zabbix 2.2.7 e o Zabbix 2.4.2 os processos do Zabbix Server e do
Zabbix Proxy registram de forma similar respostas incorretas do
SNMP:`SNMP response from host "gateway" does not contain all of the requested variable bindings`Enquanto
não forem mapeadas todas as situações problemáticas, elas são úteis para
identificar dispositivos cuja coleta em lote (bulk) pode ser
desabilitada.

Desde o Zabbix 2.2 os processos do Zabbix Server e do Zabbix Proxy
utilizam o parâmetro de 'Timeout' nas requisições SNMP. Adicionalmente
eles não realizam o reteste após uma requisição que falhar (por
'timeout' ou credenciais erradas). Anteriormente era padrão que a
bibilioteca SNMP tivesse o 'timeout' de 1 segundo e 5 tentativas.

Desde o Zabbix 2.2.8 e o Zabbix 2.4.2 os processos do Zabbix Server e do
Zabbix Proxy semre irão repetir pelo menos uma vez, seja através do
mecanismo interno da biblioteca SNMP ou através do [mecanismo interno de
processamento em lote](#internal_workings_of_bulk_processing).

::: notewarning
Se estiver monitorando dispositivos SNMPv3,
certifique-se que o 'msgAuthoritativeEngineID' (também conhecido como
'snmpEngineID' or "Engine ID") nunca seja repetido entre dois
dispositivos. Conforme a [RFC 2571](http://www.ietf.org/rfc/rfc2571.txt)
(seção 3.1.1.1) este valor deve ser único para cada
dispositivo.
:::

[comment]: # ({/new-f312fc02})

[comment]: # ({ce81cf68-cac544a8})
#### Configurando monitoramento SNMP

Para iniciar o monitoramento de um dispositivo através do protocolo SNMP é necessário executar estas etapas:

[comment]: # ({/ce81cf68-cac544a8})

[comment]: # ({new-501ff4eb})
##### Passo 1

[Criar um host](/pt/manual/config/hosts/host) para o dispositivo que
possui interface SNMP.

Informe o endereço IP de sua interface. Você pode aproveitar um dos
templates de SNMP (*Template SNMP Device* e outros) que são fornecidos
junto com o Zabbix, eles irão adicionar automaticamente um conjunto de
itens. Entretanto, o dispositivo que você deseja monitorar poderá não
ser compatível com eles. Clique em *Adicionar* para salvar o host.

::: notetip
As requisições SNMP não utilizam a porta do agente, ela
será ignorada.
:::

[comment]: # ({/new-501ff4eb})

[comment]: # ({new-929235a8})
##### Passo 2

Descubra o OID que você deseja monitorar (normalmente na documentação do
dispositivo).

Para obter uma lista dos OIDs do dispositivo o comando **snmpwalk**
poderá ser utilizado (ele é parte do pacote
[net-snmp](http://www.net-snmp.org/) que você adicionou durante a
instalação do Zabbix Server) ou alguma ferramenta equivalente:

    shell> snmpwalk -v 2c -c public <host IP> .

O '2c' utilizado no comando se refere à versão do SNMP, caso seu
dispositivo trabalhe, por exemplo, com a primeira versão do protocolo
você pode substitui-lo por '1', para indicar que o 'snmpwalk' deverá
utilizar esta versão do protocolo.

Você deverá ter como resultado uma lista contendo os nomes dos OIDs,
seus tipos e último valor de cada um. É possível que o nome de
comunidade configurada no dispositivo seja outro que não o 'public',
neste caso você deverá descobrir o nome correto e utiliza-lo.

Você pode recorrer a esta lista para localizar qual nome de OID que você
deseja monitorar, por exemplo, se você desejar monitorar o volume em
bytes que é recebido pela terceira porta do dispositivo, você poderia
utilizar o nome de OID `IF-MIB::ifInOctets.3`, conforme o exemplo a
seguir:

    IF-MIB::ifInOctets.3 = Counter32: 3409739121

Agora utilizaremos o comando **snmpget** para descobrir o número
associado ao nome de OID selecionado ('IF-MIB::ifInOctets.3'):

    shell> snmpget -v 2c -c public -On 10.62.1.22 IF-MIB::ifInOctets.3

Observe que o último número neste nome de OID é o número da porta que
você deseja monitorar. Mais detalhes em [índices dinâmicos do
SNMP](/pt/manual/config/items/itemtypes/snmp/dynamicindex).

O resultado do comando deve ser algo similar ao texto a seguir:

    .1.3.6.1.2.1.2.2.1.10.3 = Counter32: 3472126941

Novamente, o último número no OID é o número da porta.

::: noteclassic
Alguns fabricantes, como a 3COM, preferem nomear as suas
portas começando no número 100, exemplos: porta 1 = porta 101, porta 3 =
porta 103. A CISCO (e a maioria dos fornecedores) usa uma sequência
simples, exemplo: port 3 = 3.
:::

::: notetip
Alguns dos OIDs mais utilziados são [traduzidos
automaticamente para a representação
numérica](/pt/manual/config/items/itemtypes/snmp/special_mibs) de forma
nativa no Zabbix.
:::

No último exemplo o tipo do valor recebido é "Counter32", que
corresponde internamente ao tipo ASN\_COUNTER. A lista completa de tipos
suportados é: ASN\_COUNTER, ASN\_COUNTER64, ASN\_UINTEGER,
ASN\_UNSIGNED64, ASN\_INTEGER, ASN\_INTEGER64, ASN\_FLOAT, ASN\_DOUBLE,
ASN\_TIMETICKS, ASN\_GAUGE, ASN\_IPADDRESS, ASN\_OCTET\_STR e
ASN\_OBJECT\_ID (desde o Zabbix 2.2.8 ou 2.4.3). Estes tipos
correspondem, respectivamente, à: "Counter32", "Counter64",
"UInteger32", "INTEGER", "Float", "Double", "Timeticks", "Gauge32",
"IpAddress", "OCTET STRING", "OBJECT IDENTIFIER" na saida do
**snmpget**, mas também podem ser apresentados como "STRING",
"Hex-STRING", "OID" ou outro.

[comment]: # ({/new-929235a8})

[comment]: # ({new-290ebad1})
##### Passo 3

Criar o item de monitoração.

Volte ao Zabbix e clique no link *Itens* da linha do Host SNMP que você
criou mais cedo. Dependendo da associação que você fez, ou não fez, com
algum template você poderá visualizar uma lista com ou sem itens. Neste
tutorial estamos trabalhando assumindo que você criará manualmente os
itens a partir das informações conseguidas pelo 'snmpwalk' e 'snmpget'.
Clique em *Criar item*. No formulário de novo item, infomre o nome e se
certifique que o campo 'Interface do host' está com a configuração da
interface SNMP cadastrada. Informe o nome de comunidade (normalmente
'public') e o número do OID que se deseja monitorar, por exemplo:
.1.3.6.1.2.1.2.2.1.10.3

Infome a porta SNMP como sendo a 161 e 'chave' como algo significativo,
por exemplo: SNMP-InOctets-Bps. Informe o multiplicador que se deseja
trabalhar, além de informar o 'Intervalo de atualizações' e o 'Período
de retenção do histórico' se você desejar usar um valor diferente do
padrão. Defina o 'Tipo de informação' para *Numérico (fracionário)* e o
campo 'Armazenar valor' para "Delta (alterações por segundo)" (isso é
importante, sem esta configuração você terá o acumulado de valores e não
é isso que se deseja).

![](../../../../../assets/en/manual/config/items/itemtypes/snmpv3_item.png)

Agora salve o item e acesse *Monitoramento* → *Dados recentes* para
visualizar a coleta de dados!

Observe que existem opções específicas para o SNMPv3:

|Parâmetro|Descrição|<|
|----------|-----------|-|
|*Nome do contexto*|Informe o nome para identificar o item na subrede SNMP.<br>*Suportado desde o Zabbix 2.2.<br>Macros de usuário são aceitas neste campo. \| \|*Nome de segurança//|Infomre o nome de segurança.<br>Macros de usuário são aceitas neste campo.|
|*Nível de segurança*|Selecione o nível de segurança:<br>**noAuthNoPriv** - não será utilizado nenhum protocolo de privacidade ou autenticação<br>**AuthNoPriv** - protocolo de autenticação será utilizado, de privacidade não<br>**AuthPriv** - ambos os protocolos serão utilizados|<|
|*Protocolo de autenticação*|Selecione o tipo de protocolo de autenticação - *MD5* ou *SHA*.|<|
|*Senha autenticação*|Informe a frase de autenticação.<br>Macros de usuário são aceitas neste campo.|<|
|*Protocolo de privacidade*|Select privacy protocol - *DES* or *AES*.|<|
|*Frase privada*|Entre om a frase privada.<br>Macros de usuário são aceitas neste campo.|<|

::: noteclassic
Desde o Zabbix 2.2, os protocolos SHA e AES são suportados
para a autenticação e privacidade SNMPv3, em adição ao MD5 e ao DES que
já eram suportados.
:::

[comment]: # ({/new-290ebad1})

[comment]: # ({new-6021e1bd})
##### Exemplo 1

Exemplo geral:

|Parâmetro|Descrição|
|----------|-----------|
|**Comunidade**|public|
|**SNMP OID**|1.2.3.45.6.7.8.0 (ou .1.2.3.45.6.7.8.0)|
|**Chave**|<Texto único que será utilizado como referência nas triggers><br>Por exemplo: "my\_param".|

Observe que o **SNMP OID** poderá ser o número ou seu nome. Em alguns
casos pode ser que o Zabbix não consiga resolver o número a partir do
nome, para estas situações recomenda-se utilizar o 'snmpget' para fazer
esta resolução antes de configurar o item:

    shell> snmpget -On localhost public enterprises.ucdavis.memory.memTotalSwap.0

Monitoração através de SNMP só é possível se, durante a configuração do
binário do Zabbix Server ou Zabbix Proxy, tiver sido utilizado o
parâmetro --with-net-snmp.

[comment]: # ({/new-6021e1bd})

[comment]: # ({3a101cc7-45af5cf0})
##### Exemplo 2

Monitorando o Uptime (Disponibilidade):

|Parâmetro|Descrição|
|---------|-----------|
|**OID**|MIB::sysUpTime.0|
|**Key**|router.uptime|
|**Value type**|Float|
|**Units**|uptime|
|**Multiplier**|0.01|

[comment]: # ({/3a101cc7-45af5cf0})

[comment]: # ({new-ea70430a})

#### Native SNMP bulk requests

The **walk[OID1,OID2,...]** item allows to use native SNMP functionality for bulk requests (GetBulkRequest-PDUs), available in SNMP versions 2/3. 

A GetBulk request in SNMP executes multiple GetNext requests and returns the result in a single response. This may be used for regular SNMP items as well as for SNMP discovery to minimize network roundtrips.

The SNMP **walk[OID1,OID2,...]** item may be used as the master item that collects data in one request with dependent ityems that parse the response as needed using preprocessing. 

Note that using native SNMP bulk requests is not related to the option of combining SNMP requests, which is Zabbix own way of combining multiple SNMP requests (see next section).

[comment]: # ({/new-ea70430a})

[comment]: # ({new-c57b8645})
#### Funcionamento interno do processamento em lote

A partir do Zabbix 2.2.3 os processos do Zabbix Server e Zabbix Proxy
tem a capacidade de solicitar vários dados através de SNMP em uma única
requisição. Esta característica afeta diversos tipos de itens SNMP:

-   itens SNMP normais;
-   [Itens SNMP com índice
    dinâmico](/pt/manual/config/items/itemtypes/snmp/dynamicindex);
-   [Regras de autobusca
    SNMP](/pt/manual/discovery/low_level_discovery#discovery_of_snmp_oids).

Todos os itens SNMP, com igual intervalo de coletas, são agendados para
serem coletados juntos. Para os primeiros dois tipos de itens a fila
agrupa até 128 itens ao mesmo tempo, enquanto as regras de autobusca são
processadas de forma individual.

No nível mais baixo, dois tipos de operações são feitas para obter os
valores: obter múltiplos objetos e varrer árvore OID.

Para a ação de "obtenção", uma requisição 'GetRequest-PDU' é utilizada
com até 128 solicitações. Para a "varredura", uma requisição
"GetNextRequest-PDU" será utilizada com o SNMPv1 e uma 'GetBulkRequest'
com "max-repetitions" definido como 128 será utilizada com SNMPv2 e
SNMPv3.

Os benefícios de se utilizar o processamento em lote de grandes volumes
de itens SNMP são listados a seguir:

-   itens SNMP normais se beneficiam de uma "obtenção" otimizada;
-   itens SNMP com índices dinâmicos se beneficiam tanto da "obtenção"
    quanto da "varredura" otimizada: "obtenção" é utilizada para a
    verificação de índice e a "varredura" para construir o cache;
-   regras de autobusca SNMP se beneficiam da melhoria na "varredura".

Entretanto, existem limitações técnicas que fazem com que nem todos os
dispositivos sejam capazes de retornar 128 valores por requisição.
Algumas vezes o dispositivo pode retornar com a mensagem "(1) tooBig" ou
não responder a todos uma vez que a quantidade é superior ao limite do
equipamento.

Para definir o máximo de objetos para se solicitar a um dispositivo o
Zabbix In order to find an optimal number of objects to query for a
given device, Zabbix uses the following strategy: \* A primeira
requisição solicita 1 valor apenas; \* Se obteve sucesso, solicita 2
valores; \* Se obteve sucesso, solicita 3 valores; \* Se obteve sucesso,
solicita 4 valores; Obtendo sucesso nas tentativas as próximas consultas
requisitarão, respectivamente: 6, 9, 13, 19, 28, 42, 63, 94, 128
(multiplicação do último valor bem sucedido por 1,5).

Uma vez que o dispositivo não consiga dar uma resposta adequada a uma
quantidade de itens simultâneos (por exemplo, 42 valores), o Zabbix faz
duas coisas.

Primeiro, ele reduz o tamanho do lote que falhou pela metade (no caso
21). Se o dispositivo responder de forma adequada, será feito o ajuste
para 28 itens (fator anterior). Entretanto, se a falha persistir, o
Zabbix voltará a solicitar os itens 1 por 1. Se o dispositivo voltar a
falhar, o problema não será de quantidade de valores simultâneos.

A segunda coisa que ele fará para os lotes de itens subsequentes é
começar com o último número de valores obtidos com sucesso (28 em nosso
exemplo) e continuará solicitando valores somando 1 ao limite de
requisições simultâneas até encontrar o limite. Quando descobrir o
limite exato o Zabbix não mais fará estes testes e passará a trabalhar
somente com o limite descoberto neste dispositivo.

Se grandes consultas falharem com esta quantidade de valores
solicitados, pode significar uma de duas coisas. Os critérios exatos que
o dispositivo utiliza para limitar a quantidade de valores não pode ser
descoberta, mas tentamos aproximar o valor. Assim a primeira
possibilidade é que este é o número de valores simultâneos que
representa algo próximo do limite real pois, normalmente, a resposta é
menor do que o limite. A segunda possibilidade é que um pacote UDP, em
qualquer direção, simplesmente se perdeu. Por estas razões o Zabbix
considera que a consulta falhou e reduz o limite de valores simultâneos
para tentar trabalhar numa faixa mais confortável para o dispositivo
mas, (a partir do 2.2.8) somente até duas tentativas.

No exemplo acima, se uma consulta com 32 requisições simultâneas falhar,
o Zabbix irá reduzir o contador para 31. Se falhar novamente, reduz para
30. Entretanto, o Zabbix não irá reduzir de 30 pois entenderá que a
falha se deve a perda de pacotes UDP e não por limite do dispositivo.

Se um dispositivo não conseguir trabalhar com requisições em lote por
outras razões, e a eurística acima não funcionar, desde o Zabbix 2.4
existe uma opção chamada "Usar requisições em lote" em cada interface
que poderá ser desativada.

[comment]: # ({/new-c57b8645})
