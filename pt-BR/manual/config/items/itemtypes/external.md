[comment]: # translation:outdated

[comment]: # ({b475b525-0ee06d3b})
# 11 Verificações externas

[comment]: # ({/b475b525-0ee06d3b})

[comment]: # ({1dd5dc50-cd36d798})
#### Visão geral

Verificação externa é uma verificação executada pelo Zabbix Server pela 
[execução de um script shell](/manual/appendix/command_execution) ou um
binário. No entanto, quando hosts são monitorados por um Zabbix Proxy, 
as verificações externas são executadas pelo Proxy.

As verificações externas não requerem qualquer agente em execução no host
sendo monitorado.

A sintaxe da chave do item é:

    script[<parameter1>,<parameter2>,...]

Onde:

|ARGUMENTO|DEFINIÇÃO|
|--------|----------|
|**script**|Nome de um script shell ou um binário.|
|**parâmetro(s)**|Parâmetros de linha de comando opcionais.|

Se você não quer passar nenhum parâmetro para o script você pode usar:

    script[] ou
    script

O Zabbix Server consultará o diretório definido como localização para
scripts externos (parâmetro 'ExternalScripts' no [arquivo de configuração do Zabbix Server](/manual/appendix/config/zabbix_server)) e executará o comando. 
O comando será executado com o mesmo usuário com o qual o Zabbix Server
está sendo executado, então quaisquer permissões de acesso ou variáveis
de ambiente devem ser manipuladas em scripts agrupados (wrapper script), 
se necessário, e as permissões para o comando devem permitir execução para
o usuário. Apenas comandos no diretório especificado ficam disponíveis
para execução.

::: notewarning
Não abuse de verificações externas! Como cada script requer iniciar um
processo filho (fork) pelo Zabbix Server, a execução de muitos scripts
pode diminuir muito a performance do Zabbix.
:::

[comment]: # ({/1dd5dc50-cd36d798})

[comment]: # ({08ac5978-55e878ce})
#### Exemplo de uso

Executando o script **check\_oracle.sh** com o primeiro parâmetro
'-h'. O segundo parâmetro será substituído pelo endereço IP ou nome DNS,
dependendo da seleção nas propriedades do host.

    check_oracle.sh["-h","{HOST.CONN}"]

Assumindo que o host está configurado para usar o endereço IP, o Zabbix
irá executar:

    check_oracle.sh '-h' '192.168.1.4'

[comment]: # ({/08ac5978-55e878ce})

[comment]: # ({d879b95f-011b7534})
#### Resultado de verificação externa

O valor de retorno da verificação externa é a saída padrão junto com a
saída de erro padrão (a saída completa com espaços em branco finais 
removidos é retornada desde o Zabbix 2.0).

::: noteimportant
Um item texto (tipo de informação caracter, log ou texto) não se tornará
não suportado no caso de haver dados na saída de erro padrão.
:::

No caso de o script requisitado não ser encontrado ou o Zabbix Server 
não possuir permissões para executá-lo, o item se tornará não suportado
e a mensagem de erro correspondente será apresentada. No caso de tempo
esgotado, o item será marcado também como não suportado, uma mensagem
de erro relacionada será apresentada e o processo filho para o script
será destruído.

[comment]: # ({/d879b95f-011b7534})
