[comment]: # translation:outdated

[comment]: # ({75fae635-622befb9})
# 3 Traps SNMP 

[comment]: # ({/75fae635-622befb9})

[comment]: # ({2dcfe366-ff07904b})
#### Visão geral

Receber traps SNMP é o oposto de consultar dispositivos habilitados para SNMP.

Nesse caso, as informações são enviadas de um dispositivo habilitado para SNMP e são coletados ou "trapped"  pelo Zabbix

Normalmente, os traps são enviados mediante mudança de condição e o agente conecta-se ao servidor na porta 162 (em oposição à porta 161 no lado do agente que é usado para consultas).
O uso de traps pode detectar alguns pequenos problemas que ocorrem durante o intervalo de consulta e podem ser perdidos pelo dado consultado.

O recebimento de traps SNMP no Zabbix foi projetado para funcionar com **snmptrapd** e um dos mecanismos embutidos para passar as traps para o Zabbix - um script Perl ou SNMPTT.

O fluxo de trabalho de recebimento de uma trap:

1.  **snmptrapd** recebe uma trap
2.  snmptrapd passa a trap para SNMPTT ou chama o receptor de Perl trap
3.  O receptor SNMPTT or Perl trap analisa, formata e escreve a trap em um arquivo
4.  O Zabbix SNMP trapper lê e analisa o arquivo
5.  Para cada trap o Zabbix encontra todos os itens "SNMP trapper" com a interface do host que correspondem 
      ao endereço da trap recebida. Observe que apenas o "IP" ou "DNS" na interface do host é usado durante a 
      análise.
6.  Para cada item encontrado, a trap é comparada em "snmptrap\[regexp\]". A trap é definida como o valor 
      **all** nos itens correspondentes. Se nenhum item correspondente for encontrado e houver um item 
      "snmptrap.fallback", o trap é definido com esse valor. 
7.  Se o trap não foi definido como valor de nenhum item, o Zabbix por padrão registra a trap sem 
      correspondência. (Isso é configurado como "Registrar traps SNMP não correspondentes" em Administração 
      → Geral → Outros.)

[comment]: # ({/2dcfe366-ff07904b})

[comment]: # ({703ee9ce-a8df124c})
#### - Configurando traps SNMP

A configuração dos seguintes campos no frontend é específica para
Tipos de itens:

-   Seu host deve ter uma interface SNMP

Em *Configuração→ Hosts*, no campo **Interface do host** defina uma interface SNMP com o endereço IP ou DNS correto. o endereço de cada interceptação recebida é comparada com os endereços IP ou DNS de todos os endereços das interfaces SNMP para encontrar os hosts correspondentes.

-   Configurar o item

No campo **Chave**, use uma das chaves trap SNMP:

|Chave|<|<|
|---|-|-|
|Descrição|Valor de retorno|Comentários|
|snmptrap\[regexp\]|<|<|
|Captura todas as traps SNMP que correspondem à [expressão regular ](/manual/regular_expressions) especificada em **regexp**. Se regexp não for especificado, é capturado qualquer trap.|Trap SNMP|Este item pode ser definido apenas para interfaces SNMP<br>Este item é suportado desde o Zabbix **2.0.0.**<br>*Nota*: Iniciando com o Zabbix 2.0.5, macros de usuário e expressões regulares globais são suportadas no parâmetro desta chave de item.|
|snmptrap.fallback|<|<|
|Captura todas as traps SNMP que não foram capturadas por nenhum dos itens snmptrap\[\] dessa interface.|SNMP trap|Este item é suportado desde o Zabbix **2.0.0.**|

::: noteclassic
Expressões regulares de várias linhas não são suportadas neste momento
:::

Defina o **Tipo de informação** como 'Log' para que os registros de hora sejam processados. Observe que qualquer outro formato, tal qual o numérico, também são aceitáveis mas requerem um gerenciado de trap personalizado.

::: notetip
Para a monitoração de trap SNMP, você primeiro deve configurá-lo corretamente.
:::

[comment]: # ({/703ee9ce-a8df124c})

[comment]: # ({0f58a647-a08bce2a})
#### - Configurando a monitoração via trap SNMP

[comment]: # ({/0f58a647-a08bce2a})

[comment]: # ({40873d5b-581aa946})
##### Configurando o Zabbix Server/Proxy

Para receber as traps, o Zabbix Server/Proxy deverá estar configurado para iniciar o processo de SNMP trapper e apontado para o arquivo de traps que estará sendo alimentado pelo SNMPTT ou pelo 'perl trap receiver'.
Para fazer isso, edite o arquivo de configuração ([zabbix\_server.conf](/manual/appendix/config/zabbix_server) ou [zabbix\_proxy.conf](/manual/appendix/config/zabbix_proxy)):

1.  StartSNMPTrapper=1
2.  SNMPTrapperFile=\[TRAP FILE\]

::: notewarning
Se o parâmetro do systemd **[PrivateTmp](http://www.freedesktop.org/software/systemd/man/systemd.exec.html#PrivateTmp=)** tiver sido definido é improvável que funcione no */tmp*.
:::

[comment]: # ({/40873d5b-581aa946})

[comment]: # ({new-af1e38f9})
##### Configurando o SNMPTT

Primeiramente o 'snmptrapd' precisa estar configurado para utilizar o
SNMPTT.

::: notetip
Para uma melhor performance, o SNMPTT precisa estar
configurado como um daemon utilizando o **snmptthandler-embedded** para
enviar as traps. Veja mais intruções sobre configuração do SNMPTT neste
endereço:\
<http://snmptt.sourceforge.net/docs/snmptt.shtml>
:::

Quando o SNMPTT estiver apto a receber as 'traps', ajuste-o para
registrar as traps:

1.  registre as traps no arquivo de traps que será lido pelo Zabbix:\
    log\_enable = 1\
    log\_file = \[TRAP FILE\]
2.  defina o formato de data e hora:\
    date\_time\_format = %H:%M:%S %Y/%m/%d = \[DATE TIME FORMAT\]

Agora formate as traps de forma a possibilitar que o Zabbix as reconheça
(edite o arquivo snmptt.conf):

1.  Cada instrução de formato deverá começar com "ZBXTRAP \[address\]",
    onde \[address\] será o valor usado na comparação com as interfaces
    (pelo IP ou DNS). Exemplo.:\
    EVENT coldStart .1.3.6.1.6.3.1.1.5.1 "Status Events" Normal\
    FORMAT ZBXTRAP $aA Device reinitialized (coldStart)
2.  Veja mais sobre formato de SNMP trap a seguir

::: noteimportant
Não utilize traps 'não esperadas' - o Zabbix não
estará apto a reconhece-las. Traps não esperadas podem ser tratadas ao
definir um evento geral no `snmptt.conf`:\
EVENT general .\* "General event" Normal
:::

[comment]: # ({/new-af1e38f9})

[comment]: # ({new-0aa82990})
##### Configurando o `Perl trap receiver`

Requerimentos: Perl, pacote Net-SNMP compilado com
--enable-embedded-perl (já é padrão no Net-SNMP 5.4)\
\
'Perl trap receiver' (olhe em misc/snmptrap/zabbix\_trap\_receiver.pl)
poderá ser utilizado para encaminhar as traps para o Zabbix Server/Proxy
diretamente a partir do snmptrapd. Para configurar:

-   adicione o script perl no arquivo de configuração do 'snmptrapd'
    (snmptrapd.conf), Exemplo:\
    perl do "\[FULL PATH TO PERL RECEIVER SCRIPT\]";
-   configure o recebedor, exemplo:\
    $SNMPTrapperFile = '\[TRAP FILE\]';\
    $DateTimeFormat = '\[DATE TIME FORMAT\]';

::: notetip
Se o nome do script não estiver entre aspas, o snmptrapd
irá se recusar a iniciar com mensagens similares a estas:

    Regexp modifiers "/l" and "/a" are mutually exclusive at (eval 2) line 1, at end of line
    Regexp modifier "/l" may not appear twice at (eval 2) line 1, at end of line


:::

[comment]: # ({/new-0aa82990})

[comment]: # ({new-cd001892})
##### Formato do SNMP trap

Tanto os 'perl trap receivers' quanto o 'SNMPTT trap configuration'
precisam formatar a trap conforme o padrão a seguir: **\[timestamp\]
\[the trap, part 1\] ZBXTRAP \[address\] \[the trap, part 2\]**, onde

-   \[timestamp\] - momento de ocorrência do evento
-   ZBXTRAP - cabeçalho que indica o início de uma nova linha de trap
-   \[address\] - endereço IP para localizar o host a receber a trap

Observe que o "ZBXTRAP" e o "\[address\]" serão removidos da mensagem
durante o processamento. Se a trap utilizar outro formato, o Zabbix pode
analisa-las de forma inesperada.\
\
Exemplo de trap:\
11:30:15 2011/07/27 .1.3.6.1.6.3.1.1.5.3 Normal "Status Events"
localhost - ZBXTRAP 192.168.1.1 Link down on interface 2. Admin state:
1. Operational state: 2\
Esta trap será enviada para um host com a interface SNMP com
IP=192.168.1.1:\
11:30:15 2011/07/27 .1.3.6.1.6.3.1.1.5.3 Normal "Status Events"
localhost - Link down on interface 2. Admin state: 1.

[comment]: # ({/new-cd001892})

[comment]: # ({new-357f1824})
#### - Requerimentos de sistema

[comment]: # ({/new-357f1824})

[comment]: # ({new-6245c6ae})
##### Rotação de logs

O Zabbix não provê rotação de logs que possa ser gerida pelo usuário. A
rotação de logs primeiro irá renomear o arquivo antigo e após isso
apaga-lo para garantir que nenhuma trap será perdida, conforme processo
a seguir:\

1.  O Zabbix abre o arquivo de traps na última posição conhecida e vai
    para o passo 3
2.  O Zabbix verifica se o arquivo aberto já foi rotacionado ao comparar
    o número do inode definido para o mesmo. Se não existe arquivo
    aberto, o Zabbix reseta a informação de última posição conhecida e
    retorna ao passo 1.
3.  O Zabbix lê o dado do arquivo aberto e define uma nova localização.
4.  Os novos dados são analisados. Se era o arquivo rotacionado, o
    arquivo é fechado e retorna ao passo 2.
5.  Se não existe novo dado, o Zabbix aguarda 1 segundo e retorna ao
    passo 2.

::: noteimportant
O tamanho máximo de um arquivo de log para o
Zabbix é de 2GB. O arquivo de log precisa ser rotacionado antes deste
limite.
:::

[comment]: # ({/new-6245c6ae})

[comment]: # ({new-0f27a9bb})
##### Sistema de arquivos

Devido à forma de implementação, o Zabbix precisa que o sistema de
arquivos suporte inodes para diferenciar os arquivos (a informação é
obtida através da função `stat()`).

[comment]: # ({/new-0f27a9bb})

[comment]: # ({new-608a14bd})
#### - Exemplo de configuração

Este exemplo utiliza o snmptrapd + SNMPTT para enviar traps ao Zabbix
Server:

1.  **zabbix\_server.conf** - configure o Zabbix para iniciar o SNMP
    trapper e defina a localização do arquivo file:\
    StartSNMPTrapper=1\
    SNMPTrapperFile=/tmp/my\_zabbix\_traps.tmp
2.  **snmptrapd.conf** - adicione o SNMPTT como o gerenciador de traps:\
    traphandle default snmptt
3.  **snmptt.ini** - configure o arquivo de saída e o formato de hora:\
    log\_file = /tmp/my\_zabbix\_traps.tmp\
    date\_time\_format = %H:%M:%S %Y/%m/%d
4.  **snmptt.conf** - defina o formato padrão de trap:\
    EVENT general .\* "General event" Normal\
    FORMAT ZBXTRAP $aA $ar
5.  Crie um item SNMP TEST:\
    IP da Interface SNMP do Host IP: 127.0.0.1\
    Chave: snmptrap\["General"\]\
    Formato de hora do log: hh:mm:ss yyyy/MM/dd

Teste o funcionamento:

1.  Comando utilizado para enviar uma trap:\
    snmptrap -v 1 -c public 127.0.0.1 '.1.3.6.1.6.3.1.1.5.3' '0.0.0.0' 6
    33 '55' .1.3.6.1.6.3.1.1.5.3 s "teststring000"
2.  Que será recebida:\
    15:48:18 2011/07/26 .1.3.6.1.6.3.1.1.5.3.0.33 Normal "General event"
    localhost - ZBXTRAP 127.0.0.1 127.0.0.1
3.  Valor par ao teste do item:\
    15:48:18 2011/07/26 .1.3.6.1.6.3.1.1.5.3.0.33 Normal "General event"
    localhost - 127.0.0.1

::: notetip
Este exemplo simples utiliza o SNMPTT como
**traphandle**. Para melhor performance em ambientes de produção,
utilize um script Perl para encaminhar as traps do snmptrapd ao SNMPTT
ou diretamente ao Zabbix.
:::

[comment]: # ({/new-608a14bd})



[comment]: # ({new-ee69482d})
#### - Setup example

This example uses snmptrapd + SNMPTT to pass traps to Zabbix server.
Setup:

1.  **zabbix\_server.conf** - configure Zabbix to start SNMP trapper and
    set the trap file:\
    StartSNMPTrapper=1\
    SNMPTrapperFile=/tmp/my\_zabbix\_traps.tmp
2.  **snmptrapd.conf** - add SNMPTT as the trap handler:\
    traphandle default snmptt
3.  **snmptt.ini** -\
    enable the use of the Perl module from the NET-SNMP package:\
    net\_snmp\_perl\_enable = 1\
    configure output file and time format:\
    log\_file = /tmp/my\_zabbix\_traps.tmp\
    date\_time\_format = %H:%M:%S %Y/%m/%d
4.  **snmptt.conf** - define a default trap format:\
    EVENT general .\* "General event" Normal\
    FORMAT ZBXTRAP $aA $ar
5.  Create an SNMP item TEST:\
    Host's SNMP interface IP: 127.0.0.1\
    Key: snmptrap\["General"\]\
    Log time format: hh:mm:ss yyyy/MM/dd

This results in:

1.  Command used to send a trap:\
    snmptrap -v 1 -c public 127.0.0.1 '.1.3.6.1.6.3.1.1.5.3' '0.0.0.0' 6
    33 '55' .1.3.6.1.6.3.1.1.5.3 s "teststring000"
2.  The received trap:\
    15:48:18 2011/07/26 .1.3.6.1.6.3.1.1.5.3.0.33 Normal "General event"
    localhost - ZBXTRAP 127.0.0.1 127.0.0.1
3.  Value for item TEST:\
    15:48:18 2011/07/26 .1.3.6.1.6.3.1.1.5.3.0.33 Normal "General event"
    localhost - 127.0.0.1

::: notetip
This simple example uses SNMPTT as **traphandle**. For
better performance on production systems, use embedded Perl to pass
traps from snmptrapd to SNMPTT or directly to Zabbix.
:::

[comment]: # ({/new-ee69482d})

[comment]: # ({new-75026475})
#### - See also

-   [Zabbix blog article on SNMP
    traps](https://blog.zabbix.com/snmp-traps-in-zabbix)
-   [CentOS based SNMP trap tutorial on
    zabbix.org](https://www.zabbix.org/wiki/Start_with_SNMP_traps_in_Zabbix)

[comment]: # ({/new-75026475})
