[comment]: # translation:outdated

[comment]: # ({50885257-26d9bc44})
# 9 Verificações SSH

[comment]: # ({/50885257-26d9bc44})

[comment]: # ({4bbc12a9-4cdff2c1})
#### Visão geral

Verificações SSH são executadas como monitoramento sem agente. O Zabbix Agent
não é necessário para verificações SSH.

Para realizar verificações SSH o Zabbix Server deve ser inicialmente 
[configurado](/manual/installation/install#from_the_sources) com suporte a SSH2
(libssh2 ou libssh). Veja também: [Requisitos](/manual/installation/requirements#server).

::: noteimportant
Apenas libssh é suportado a partir do RHEL/CentOS
8.
:::

[comment]: # ({/4bbc12a9-4cdff2c1})

[comment]: # ({29850850-5d32b87c})
#### Configuração

[comment]: # ({/29850850-5d32b87c})

[comment]: # ({a802fb4b-753f5c30})
##### Autenticação por senha

As verificações SSH oferecem dois métodos de autenticação, um com
par de usuário/senha e outro baseado em arquivo de chave.

Se você não pretende usar chaves, nenhuma configuração adicional é
necessária, além de associar a libssh2/libssh ao Zabbix, se você estiver
compilando a partir dos fontes.

[comment]: # ({/a802fb4b-753f5c30})

[comment]: # ({f705ae8f-44fd07da})
##### Autenticação por arquivo de chave

Para usar autenticação baseada em chave para itens SSH, certas alterações
nas configurações do Server são necessárias.

Abra o arquivo de configuração do Zabbix Server ([zabbix\_server.conf](/manual/appendix/config/zabbix_server)) 
como `root` e procure pela seguinte linha:

    # SSHKeyLocation=

Descomente-a e informe o caminho completo para a pasta onde as chaves
pública e privada estão localizadas (ou estarão, caso ainda não criadas):

    SSHKeyLocation=/home/zabbix/.ssh

Salve o arquivo e reinicie o zabbix\_server em seguida.

No comando acima, */home/zabbix* é o diretório pessoal do usuário *zabbix*
e *.ssh* é um diretório (oculto) onde por padrão as chaves pública e privada serão
geradas por um comando [ssh-keygen](http://en.wikipedia.org/wiki/Ssh-keygen)
executado dentro do diretório pessoal.

Comumente os pacotes de instalação do zabbix-server de diferentes distribuições de 
SO criam a conta de usuário *zabbix* com um diretório pessoal em lugares não muito
comuns e conhecidos (diferente das contas do sistema), p.e. */var/lib/zabbix*.

Antes de iniciar a criação das chaves, uma abordagem para realocar o diretório 
pessoal para um local melhor conhecido (mais intuitivo) pode ser considerada.
Isto corresponderá com a configuração do parâmetro *SSHKeyLocation* do Zabbix Server
mencionada acima.

Estes passos podem ser pulados se a conta *zabbix* foi adicionada manualmente
de acordo com a [seção de instalação](/manual/installation/install#create_user_account) porque neste caso
muito provavelmente o diretório já estará localizado em */home/zabbix*.

Para alterar a configuração para a conta de usuário *zabbix* todos os processos
em execução que a estão usando devem ser parados:

    # service zabbix-agent stop
    # service zabbix-server stop

Para mudar a localização do diretório pessoal com uma tentativa de movê-lo
(caso ele exista) o seguinte comando deve ser executado:

    # usermod -m -d /home/zabbix zabbix

É absolutamente possível que um diretório pessoal não exista no antigo local
(no CentOS por exemplo), então ele deve ser criado no novo local. Uma tentativa
segura de fazê-lo é:

    # test -d /home/zabbix || mkdir /home/zabbix

Para certificar-se de que tudo está seguro, comandos adicionais podem ser
executados para configurar as permissões do novo diretório pessoal:

    # chown zabbix:zabbix /home/zabbix
    # chmod 700 /home/zabbix

Os processos previamente parados agora podem ser reiniciados:

    # service zabbix-agent start
    # service zabbix-server start

Agora as etapas de criação das chaves pública e privada podem ser
executadas pelo comando:

    # sudo -u zabbix ssh-keygen -t rsa
    Generating public/private rsa key pair.
    Enter file in which to save the key (/home/zabbix/.ssh/id_rsa): 
    Created directory '/home/zabbix/.ssh'.
    Enter passphrase (empty for no passphrase): 
    Enter same passphrase again: 
    Your identification has been saved in /home/zabbix/.ssh/id_rsa.
    Your public key has been saved in /home/zabbix/.ssh/id_rsa.pub.
    The key fingerprint is:
    90:af:e4:c7:e3:f0:2e:5a:8d:ab:48:a2:0c:92:30:b9 zabbix@it0
    The key's randomart image is:
    +--[ RSA 2048]----+
    |                 |
    |       .         |
    |      o          |
    | .     o         |
    |+     . S        |
    |.+   o =         |
    |E .   * =        |
    |=o . ..* .       |
    |... oo.o+        |
    +-----------------+

Nota: as chaves pública e privada (*id\_rsa.pub* e *id\_rsa* respectivamente)
foram geradas (acima) por padrão no diretório */home/zabbix/.ssh* que 
corresponde à configuração do parâmetro *SSHKeyLocation* do Zabbix Server.

::: noteimportant
Tipos de chave diferentes de "rsa" podem ser suportados pelo utilitário
ssh-keygen e servidores SSH, mas elas podem não ser suportadas pela
libssh2, usada pelo Zabbix.
:::

[comment]: # ({/f705ae8f-44fd07da})

[comment]: # ({16ebde70-7e6b0273})
##### Configuração do shell

Esta etapa deve ser executada apenas uma vez para cada host que será
monitorado por verificações SSH.

Usando o comando abaixo, o arquivo de chave **pública** pode ser 
instalado em um host remoto *10.10.10.10* de modo que então as 
verificações SSH podem ser executadas com a conta de usuário *root*:

    # sudo -u zabbix ssh-copy-id root@10.10.10.10
    A autenticidade do host '10.10.10.10 (10.10.10.10)' não pode ser estabelecida.
    RSA key fingerprint é 38:ba:f2:a4:b5:d9:8f:52:00:09:f7:1f:75:cc:0b:46.
    Você tem certeza que quer continuar com a conexão (sim/não)? sim
    Atenção: Adicionado permanentemente '10.10.10.10' (RSA) à lista de hosts conhecidos.
    senha para root@10.10.10.10': 
    Agora tente fazer login na máquina, com "ssh 'root@10.10.10.10'", e verifique:
      .ssh/authorized_keys
    para garantir que não adicionamos chaves extras que você não estava esperando.

Agora é possível verificar o login SSH usando a chave privada padrão 
(*/home/zabbix/.ssh/id\_rsa*) para a conta de usuário *zabbix*:

    # sudo -u zabbix ssh root@10.10.10.10

Se o login for bem-sucedido, então a parte de configuração do shell está
finalizada e a conexão remota SSH pode ser encerrada.

[comment]: # ({/16ebde70-7e6b0273})

[comment]: # ({3da9eb2d-fe23daa8})
##### Configuração de item

Os comandos reais a serem executados devem ser informados no campo
**Script executado** na configuração do item.

Múltiplos comandos podem ser executados um após o outro colocando-os
em uma nova linha. Neste caso os valores retornados também estarão
formatados como multilinha.

![](../../../../../assets/en/manual/config/items/itemtypes/ssh_item.png)

Todos os campos obrigatórios estão marcados com um asterisco vermelho.

Os campos que requerem informação específica para os itens SSH são:

|Parâmetro|Descrição|Comentários|
|---------|-----------|--------|
|*Tipo*|Selecione **Agente SSH** aqui.|<|
|*Chave*|Chave de item única (por host) no formato **ssh.run\[<descrição curta única>,<ip>,<porta>,<codificação>\]**|<descrição curta única> é obrigatória e deve ser única para todos os itens SSH por host<br>A porta padrão é 22, não a porta especificada na interface à qual este item está associado|
|*Método de autenticação*|Um de "Senha" ou "Chave pública"|<|
|*Nome de usuário*|Nome de usuário para autenticação no host remoto.<br>Obrigatório|<|
|*Arquivo de chave pública*|Nome do arquivo de chave pública se *Método de autenticação* é "Chave pública". Obrigatório|Exemplo: *id\_rsa.pub* - nome de arquivo de chave pública padrão gerado pelo comando [ssh-keygen](http://en.wikipedia.org/wiki/Ssh-keygen)|
|*Arquivo de chave privada*|Nome do arquivo de chave privada se *Método de autenticação* é "Chave pública". Obrigatório|Exemplo: *id\_rsa* - nome de arquivo de chave privada padrão|
|*Senha* ou<br>*Chave de senha (passphrase)*|Senha para autenticar ou<br>Chave de senha **se** tiver sido usada para a chave privada|Deixe o campo *Chave de senha* vazio se não tiver sido usada uma<br>Consulte também [problemas conhecidos](/manual/installation/known_issues#ssh_checks) em relação ao uso de chave de senha|
|*Script executado*|Comando(s) shell executado usando a sessão remota SSH|Exemplos:<br>*date +%s*<br>*service mysql-server status*<br>*ps auxww \| grep httpd \| wc -l*|

::: noteimportant
A biblioteca libssh2 pode truncar scripts executáveis para \~32kB.
:::

[comment]: # ({/3da9eb2d-fe23daa8})
