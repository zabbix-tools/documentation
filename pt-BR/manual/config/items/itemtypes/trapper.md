[comment]: # translation:outdated

[comment]: # ({71eb378b-5e482050})
# 12 Itens de captura (trapper)

[comment]: # ({/71eb378b-5e482050})

[comment]: # ({13a40e6f-03e7aedb})
#### Visão geral

Itens de captura (trapper) aceitam entrada de dados em vez de
consultá-los.

É útil para qualquer dado que você queira "empurrar" para dentro
do Zabbix.

Para usar um item de captura você deve:

-   ter um item de captura (trapper) configurado no Zabbix
-   enviar o dado para dentro do Zabbix

[comment]: # ({/13a40e6f-03e7aedb})

[comment]: # ({new-5d32b87c})
#### Configuração

[comment]: # ({/new-5d32b87c})

[comment]: # ({8df63193-62c22fbb})
##### Configuração do item

Para configurar um item de captura (trapper):

-   Vá até: *Configuração* → *Hosts*
-   Clique em *Itens* na linha do host
-   Clique em *Criar item*
-   Preencha os parâmetros do item no formulário

![](../../../../../assets/en/manual/config/items/itemtypes/trapper_item.png)

Todos os campos obrigatórios estão marcados com um asterisco vermelho.

Os campos que requerem informação específica para os itens de captura são:

|   |   |
|---|---|
|*Tipo*|Selecione **Capturador Zabbix** (Trapper) aqui.|
|*Chave*|Informe uma chave que será utilizada para reconhecer o item no processo de envio de dados.|
|*Tipo de informação*|Selecione o tipo de informação que corresponderá ao formato de dado que será enviado para dentro do Zabbix.|
|*Hosts permitidos*|Lista de endereços IP separados por vírgula, opcionalmente em notação CIDR, ou nomes de host.<br>Se especificado, conexões de entrada serão aceitas apenas dos hosts listados aqui.<br>Se o suporte a IPv6 estiver habilitado então '127.0.0.1', '::127.0.0.1', '::ffff:127.0.0.1' são tratados como iguais e '::/0' permitirá qualquer endereço IPv4 ou IPv6.<br>'0.0.0.0/0' pode ser usado para permitir qualquer endereço IPv4.<br>Note que "endereços IPv6 compatíveis com IPv4" (com prefixo 0000::/96) são suportados mas obsoletos pela [RFC4291](https://tools.ietf.org/html/rfc4291#section-2.5.5).<br>Exemplo: Server=127.0.0.1, 192.168.1.0/24, 192.168.3.1-255, 192.168.1-10.1-255, ::1,2001:db8::/32, zabbix.domain<br>Espaços e [macros de usuário](/manual/config/macros/user_macros) são permitidos neste campo desde o Zabbix 2.2.0.<br>As macros de host: {HOST.HOST}, {HOST.NAME}, {HOST.IP}, {HOST.DNS}, {HOST.CONN} são permitidas neste campo desde o Zabbix 4.0.2.|

::: noteclassic
Você pode ter que esperar até 60 segundos após salvar o item,
até que o Server colete as mudanças de uma atualização de cache
de configuração, antes de poder enviar valores para o Zabbix.
:::

[comment]: # ({/8df63193-62c22fbb})

[comment]: # ({edb57d67-5f246209})
##### Envio de dados

Nos casos mais simples, podemos utilizar o utilitário
[zabbix\_sender](/manual/concepts/sender) para enviar algum
'valor de teste':

    zabbix_sender -z <endereço IP do Server> -p 10051 -s "Novo host" -k trap -o "valor de teste"

Para enviar o valor para o Zabbix usamos estas chaves:

*-z* - para informar o endereço IP do Zabbix Server

*-p* - para especificar o número de porta do Zabbix Server (10051 por padrão)

*-s* - para informar o host (certifique-se de usar o [nome de host](/manual/config/hosts/host#configuration) 'técnico' aqui, em vez do nome 'visível')

*-k* - para especificar a chave do item que acabamos de definir

*-o* - para especificar o valor real a ser enviado

::: noteimportant
O processo de captura do Zabbix (trapper) não expande macros usadas 
na chave do item na tentativa de verificar a existência de chave de 
item correspondente para o host de destino.
:::

[comment]: # ({/edb57d67-5f246209})

[comment]: # ({fc786e01-1e9d760a})
##### Apresentação

Este é o resultado em *Monitoramento → Últimos dados*:

![](../../../../../assets/en/manual/config/items/itemtypes/trapped_data.png){width="600"}

Note que se um valor numérico único é enviado, o gráfico de dados mostrará
uma linha horizontal à esquerda e à direita do ponto temporal (data/hora) do valor.

[comment]: # ({/fc786e01-1e9d760a})
