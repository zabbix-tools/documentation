[comment]: # translation:outdated

[comment]: # ({83f420bc-aed6dc97})
# 6 Monitoramento de arquivo de log

[comment]: # ({/83f420bc-aed6dc97})

[comment]: # ({78a050ee-8d33723f})
#### Visão geral

O Zabbix pode ser usado para análise e monitoramento de arquivos de log
com/sem suporte a rotação de log.

Notificações podem ser usadas para alertar os usuários quando um arquivo
de log contiver alguma frase ou padrão de texto.

Para monitorar um arquivo de log você deve ter:

-   um Zabbix Agent sendo executado no host
-   um item de monitoramento de log configurado

::: noteimportant
O tamanho de um arquivo de log monitorado depende do
[suporte a arquivos grandes](/manual/appendix/items/large_file_support).
:::

[comment]: # ({/78a050ee-8d33723f})

[comment]: # ({new-5d32b87c})
#### Configuração

[comment]: # ({/new-5d32b87c})

[comment]: # ({874f42c3-9223a947})
##### Verifique os parâmetros do agente

Certifique-se de que no [arquivo de configuração do agente](/manual/appendix/config/zabbix_agentd):

-   O parâmetro 'Hostname' corresponde ao nome do host no Zabbix Frontend (interface web)
-   Os Zabbix Servers no parâmetro 'ServerActive' estão definidos para o processamento de verificações ativas

[comment]: # ({/874f42c3-9223a947})

[comment]: # ({1e4dbd6f-e137d9c7})
##### Configuração do item

Configure um [item](/manual/config/items/item#overview) de monitoramento de log.

![](../../../../../assets/en/manual/config/items/itemtypes/logfile_item.png)

Todos os campos obrigatórios estão marcados com um asterisco vermelho.

Especificamente para itens de monitoramento de log você informa:

|   |   |
|---|---|
|*Tipo*|Selecione **Zabbix Agent (ativo)** aqui.|
|*Chave*|Use uma das seguintes chaves de item:<br>**log\[\]** ou **logrt\[\]**:<br>Estas duas chaves de item permitem monitorar logs e filtrar entradas de log pelo conteúdo da expressão regular, se houver.<br>Por exemplo: `log[/var/log/syslog,error]`. Certifique-se de que o arquivo permite leitura pelo usuário 'zabbix', caso contrário o estado do item se tornará 'não suportado'.<br>**log.count\[\]** ou **logrt.count\[\]**:<br>Estas duas chaves de item permitem retornar apenas o número de linhas correspondentes.<br>Consulte a seção [chave de item suportado pelo Zabbix Agent](zabbix_agent#supported_item_keys) para detalhes de utilização destas chaves e seus parâmetros.|
|*Tipo de informação*|Preenchido automaticamente:<br>Para itens log\[\] ou logrt\[\] - `Log`;<br>Para itens log.count\[\] ou logrt.count\[\] - `Numérico (unsigned)`.<br>Se estiver usando o parâmetro opcional `output`, você pode selecionar manualmente o tipo de informação apropriado diferente de `Log`.<br>Note que a escolha de um tipo de informação não-log acarretará na perda do registro de data e hora local.|
|*Intervalo de atualização (em seg)*|O parâmetro define quão frequentemente o Zabbix Agent verificará por qualquer alteração no arquivo de log. Configurar este parâmetro para 1 segundo garantirá que você obtenha novos registros tão logo quanto possível.|
|*Formato de data e hora do log*|Neste campo você pode opcionalmente especificar o padrão para analisar o registro de data e hora do arquivo de log.<br>Se deixado em brando o registro de data e hora não será analisado.<br>Marcadores de posição suportados:<br>\* **y**: *Ano (0001-9999)*<br>\* **M**: *Mês (01-12)*<br>\* **d**: *Dia (01-31)*<br>\* **h**: *Hora (00-23)*<br>\* **m**: *Minuto (00-59)*<br>\* **s**: *Segundo (00-59)*<br>Por exemplo, considere a seguinte linha do arquivo de log do Zabbix Agent:<br>" 23480:20100328:154718.045 Zabbix agent started. Zabbix 1.8.2 (revision 11211)."<br>Ela começa com seis posições de caracter para o PID, seguido da data, hora, e o restante da linha.<br>O formato de data e hora para esta linha seria"pppppp:yyyyMMdd:hhmmss".<br>Note que os caracteres "p" e ":" são apenas marcadores de posição e podem ser qualquer coisa, exceto "yMdhms".|

[comment]: # ({/1e4dbd6f-e137d9c7})

[comment]: # ({c47067ec-2a661d49})
#### Notas importantes

-   O Zabbix Server e Agent mantêm o registro do tamanho e data da última
modificação (para logrt) do arquivo de log em dois contadores. Adicionalmente:
    -   O agente também usa internamente números de inode (no
        UNIX/GNU/Linux), índices de arquivos (no Microsoft Windows) e soma de verificação MD5
        dos primeiros 512 bytes do arquivo de log para melhorar a detecção de
        arquivos de log truncados e rotacionados.
    -   Nos sistemas UNIX/GNU/Linux é assumido que os sistemas de arquivo onde os
        arquivos de log são armazenados reportem números de inode, que podem ser
        usados para rastrear arquivos.
    -   No Microsoft Windows o Zabbix Agent determina o tipo de sistema de arquivo no qual
        os arquivos de log residem e usa:
        -   Nos sistemas de arquivo NTFS índices de arquivo 64-bit.
        -   Nos sistemas de arquivo ReFS (apenas de Microsoft Windows
            Server 2012) IDs de arquivo 128-bit.
        -   Nos sistemas de arquivo onde índices de arquivo mudam (p.e. FAT32,
            exFAT) um algoritmo alternativo é usado para uma abordagem sensível
            em condições incertas quando a rotação de log resulta em múltiplos
            arquivos de log com a mesma data e hora de modificação.
    -   Os números de inode, índices de arquivo e somas de verificação MD5 são internamente
        coletados pelo Zabbix Agent. Eles não são transmitidos para o Zabbix
        Server e são perdidos quando o Zabbix Agent é parado.
    -   Não modifique a última data de alteração dos arquivos de log com o utilitário
        'touch', não copie um arquivo de log com restauração anterior
        do arquivo original (isto mudará o número de inode do arquivo).
        Em ambos os casos o arquivo será considerado diferente e será
        analisado desde o início, o que pode resultar em alertas duplicados.
    -   Se houver vários arquivos de log correspondentes ao item `logrt[]` e
        o Zabbix Agent estiver acompanhando o mais recente deles e este arquivo
        de log mais recente for eliminado, uma mensagem de alerta
        `"não há arquivos correspondentes à "<máscara regexp>" no "<diretório>"`
        será mostrada. O Zabbix Agent ignora arquivos de log com data de mofificação
        menor que a data de modificação mais recente vista pelo agente para o item
        `logrt[]` sendo verificado.
-   O agente começa lendo o arquivo de log a partir do ponto em que parou
    na última vez.
-   A quantidade de bytes já analisados (o contador de tamanho) e a última data de
    modificação (o contador de data) são armazenados no banco de dados do Zabbix
    e são enviados para o agente para garantir que o agente inicie a leitura do
    arquivo de log deste ponto nos casos quando o agente é recém iniciado
    ou recebeu itens que foram previamente desabilitados ou não suportados.
    No entanto, se o agente recebeu um contador de tamanho diferente de zero
    do servidor, mas o item logrt\[\] ou logrt.count\[\] não foi encontrado
    e não há arquivos correspondentes, o contados de tamanho é reiniciado para 0 
    de modo a analisar desde o início no caso de os arquivos aparecerem mais tarde.
-   Sempre que o arquivo de log tornar-se menor que o contador de tamanho de log
    conhecido pelo agente, o contador é reconfigurado para zero e o agente começa
    a leitura do arquivo de log do começo, levando em conta o contador de data.
-   Se houver vários arquivos correspondentes com a mesma data de última alteração
    dentro do diretório, então o agente tenta analisar corretamente todos os arquivos
    de log com a mesma data de modificação evitando pular dados ou analisar dados
    repetidos, apesar deste comportamento não poder ser garantido em todas as situações.
    O agente não pressupõe qualquer esquema de rotação de arquivo de log e nem
    determina um. Quando forem apresentados múltiplos arquivos de log com a mesma
    data de última alteração, o agente os processará em uma ordem lexicográfica
    descendente. Assim, para alguns esquenas de rotação os arquivos de log
    serão analisados e reportados em sua ordem original. Para outros esquemas
    de rotação a ordem original dos arquivos de log não será honrada, o que
    pode acarretar na entrega de registros de arquivos de log em ordem alterada
    (o problema não ocorre se os arquivos de log tiverem datas de última alteração
    diferentes).
-   O Zabbix Agent processa novos registros de um arquivo de log uma vez a cada 
    *Intervalo de atualização* segundos.
-   O Zabbix Agent não envia mais do que **maxlines** de um arquivo de log por
    segundo. O limite evita sobrecarga de recursos de rede e CPU e sobrescreve
    o valor padrão fornecido pelo parâmetro **MaxLinesPerSecond** no
    [arquivo de configuração do agente](/manual/appendix/config/zabbix_agentd).
-   Para encontrar o texto requerido o Zabbix irá processar 10 vezes mais novas
    linhas do que configurado em MaxLinesPerSecond. Assim, por exemplo, se um item
    `log[]` ou `logrt[]` possui *Intervalo de atualização* de 1 segundo, por padrão
    o agente analisará não mais do que 200 registros do arquivo de log e enviará
    não mais do que 20 registros correspondentes para o Zabbix Server em uma
    verificaçãço. Aumentando **MaxLinesPerSecond** no arquivo de configuração do
    agente ou configurando o parâmetro **maxlines** na chave do item, o limite pode
    ser elevado até 10000 registros de log analisados e 1000 registros compatíveis
    enviados para o Zabbix Server em uma verificação. Se o *Intervalo de atualização*
    estiver configurado para 2 segundos então os limites para um verificação seriam
    configurados 2 vezes mais altos do que com *Intervalo de atualização* de 1 segundo.
-   Adicionalmente, os valores log e log.count são sempre limitados a 50% do tamanho
    de buffer de envio do agente, mesmo que não haja valores não-log configurados.
    Então para os valores **maxlines** serem enviados em uma conexão (e não em
    várias conexões), o parâmetro de [tamanho de buffer (BufferSize)](/manual/appendix/config/zabbix_agentd) do agente deve ser de pelo menos maxlines x 2.
-   Na ausência de itens de log todo o tamanho de buffer do agente é utilizado
    para valores não-log. Quando vierem valores de log estes substituirão os valores
    não-log mais antigos conforme necessário, até o limite de 50%.
-   Para registros de arquivo de log maiores que 256kB, apenas os primeiros 256kB
    são analisados com a expressão regular e o restante dos registros é ignorado.
    No entanto, se o Zabbix Agent for parado enquanto estiver lidando com um registro
    longo, o estado interno do agente é perdido e o registro longo pode ser analisado
    novamente e de forma diferente assim que o agente for reiniciado.
-   Nota especial para separadores de caminho "\\": se file\_format (formato
    do arquivo) for "file\\.log", então não deve existir um diretório "file", desde que
    não é possível de forma não ambígua definir se "." está escapado ou se é o 
    primeiro símbolo do nome do arquivo.
-   Expressões regulares para `logrt` são suportadas apenas no nome do arquivo; expressão
    regular para correspondência de diretório não é suportada.
-   Nas plataformas UNIX um item `logrt[]` se torna NÃO SUPORTADO (NOTSUPPORTED) se um
    diretório onde se espera encontrar os arquivos de log não existir.
-   No Microsoft Windows, se um diretório não existe o item não se tornará NÃO SUPORTADO
    (por exemplo, se o diretório contiver erro de digitação na chave do item).
-   A ausência de arquivos de log para o item `logrt[]` não o torna NÃO SUPORTADO.
    Erros de leitura de arquivos de log para o item `logrt[]` são regitrados como
    alertas dentro do arquivo de log do Zabbix Agent mas não tornam o item NÃO SUPORTADO.
-   O arquivo de log do Zabbix Agent pode ser útil para encontrar o por quê de um item 
    `log[]` ou `logrt[]` tornar-se NÃO SUPORTADO. O Zabbix pode monitorar seu arquivo de log, 
    com exceção de quando estiver em DebugLevel=4.

[comment]: # ({/c47067ec-2a661d49})

[comment]: # ({71e14f6b-5fc56a1f})
#### Extraindo a parte correspondente de uma expressão regular

Algumas vezes podemos querer extrair apenas um valor de interesse
de um arquivo alvo, em vez de retornar a linha completa de uma
correspondência à uma expressão regular.

Desde o Zabbix 2.2.0, itens de log têm a habilidade de extrair
valores desejados de linhas correspondentes. Isto é possível com o
parâmetro adicional **output** nos itens `log` e `logrt`.

O uso do parâmetro 'output' permite indicar o "grupo de captura" na
correspondência em que possamos estar interessados.

Então, por exemplo

 · log[/path/to/the/file,"large result buffer allocation.*Entries: ([0-9]+)",,,,\1]

deve permitir retornar a contagem de entradas (entries) de acordo com o encontrado
no conteúdo de::

 · Fr Feb 07 2014 11:07:36.6690 */ Thread Id 1400 (GLEWF) large result
 · buffer allocation - /Length: 437136/Entries: 5948/Client Ver: >=10/RPC
 · ID: 41726453/User: AUser/Form: CFG:ServiceLevelAgreement

Apenas o número será retornado porque **\\1** se refere apenas ao primeiro e
único grupo de captura: **(\[0-9\]+)**.

E, com a habilidade de extrair e retornar um número, o valor pode ser
usado para definir gatilhos.

[comment]: # ({/71e14f6b-5fc56a1f})


[comment]: # ({ead226c8-32045a26})
#### Notas sobre rotação de arquivo de log com 'copytruncate'

O `logrt` com a opção `copytruncate` assume que arquivos de log diferentes
possuem registros diferentes (ao menos seus registros de data são diferentes),
portanto a soma de verificação MD5 dos blocos iniciais (até os primeiros 
512 bytes) será diferente. Dois arquivos com a mesma soma de verificação MD5 
implica que um deles é o original, e outro uma cópia.

O `logrt` com a opção `copytruncate` se esforça para processar corretamente
cópias de arquivo de log sem reportar duplicatas. No entanto, situações como
a produção de vários arquivos de log com o mesmo registro de data, rotação de
arquivo de log mais frequente do que o intervalo de atualização do item logrt\[\], 
e reinício frequente do agente não são recomendados. O agente procura lidar com
todas estas situações de forma razoavelmente bem, mas bons resultados não podem
ser garantidos em todas as circunstâncias.

[comment]: # ({/ead226c8-32045a26})

[comment]: # ({f06f25d7-57bbb0a9})
#### Notas sobre arquivos persistentes para itens log\*\[\]

##### Propósito dos arquivos persistentes

Quando o Zabbix Agent é iniciado ele recebe uma lista de verificações ativas
do Zabbix Server ou Proxy. Para métricas de log\*\[\] ele recebe o tamanho do
log processado e a data de modificação para encontrar o ponto de partida para
monitorar o arquivo de log. Dependendo do tamanho real do arquivo de log e
data de modificação informada pelo sistema de arquivos o agente decide em
continuar o monitoramento do log a partir tamanho de log processado ou
re\-analisar o arquivo de log desde o início.

Um agente em execução mantém um extenso conjunto de atributos para o rastreio
de todos os arquivos de log monitorados entre as verificações. Este estado
em\-memória é perdido quando o agente é parado.

O novo parâmetro opcional **persistent\_dir** especifica o diretório para o
armazenamento deste estado dos itens de log\[\], log.count\[\], logrt\[\] ou 
logrt.count\[\] em um arquivo. O estado de um item de log é restaurado do arquivo
persistente após o Zabbix Agent ser reiniciado.

O caso de uso primário é o monitoramento de arquivo de log localizado em um
sistema de arquivo espelhado. Até algum momento em que o arquivo de log é
gravado em ambos os locais. Então os ambientes espelhados são separados.
Na cópia ativa o arquivo de log continua crescendo, recebendo novos registros.
O Zabbix Agent o analisa e envia os tamanhos de logs processados e data de
modificação para o Server. Na cópia passiva o arquivo de log continua o mesmo,
bem atrás da cópia ativa. Mais tarde o sistema operacional e o Zabbix Agent da
cópia passiva são reiniciados. O tamanho de log processado e a data de modificação
que o Agent recebe do Server pode não ser válida para a situação na cópia
passiva. Para continuar o monitoramento do log a partir do lugar em que o
agente parou no momento em que o espelhamento do sistema de arquivo foi
separado, o agente restaura seu estado de um arquivo persistente.

##### Operação do agente com arquivo persistente

Na inicialização o Zabbix Agent não sabe nada sobre arquivos persistentes.
Apenas após receber uma lista de verificações ativas do Zabbix Server \(proxy\) 
o agente identifica que alguns itens de log devem ser apoiados por arquivos
persistentes dentro de diretórios especificados.

Durante a operação do agente os arquivos persistentes são abertos para gravação
(com fopen(filename, "w")) e sobrescritos com os últimos dados. A chance de perder
dados de arquivo persistente se a sobrescrita e a separação dos espelhos de sistema
de arquivo ocorrerem ao mesmo tempo é muito pequena, portanto sem manipulação 
especial para este caso. A gravação em arquivo persistente NÃO é seguida por
sincronização forçada de mídia de armazenamento \(fsync\(\) não é chamado\).

A sobrescrita com os dados mais recentes é feita após o reporte bem-sucedido
de registros de arquivo de log ou metadados correspondentes \(tamanho de log
processado e data de modificação\) para o Zabbix Server. Isto pode ocorrer tão
frequentemente quanto cada verificação de item se o arquivo de log continuar
sendo alterado.

Após receber uma lista de verificações ativas o agente escaneia o diretório
de arquivo persistente e remove arquivos persistentes obsoletos. A remoção
é efetuado com um atraso de 24h porque arquivos de log em estado NÃO SUPORTADO
não são incluídos na lista de verificações ativas, mas eles podem se tornar
SUPORTADOS mais tarde e seus arquivos persistentes podem vir a ser úteis. 
Se o agente for parado e reiniciado antes que as 24h expirem, então os arquivos
obsoletos não serão apagados pois o Zabbix Agent não está mais recebendo do
Zabbix Server informações sobre suas localizações.

Sem ações especiais durante desligamento do agente.

##### Nomenclatura e localização de arquivos persistentes

O Zabbix Agent distingue as verificações ativas pelas suas chaves. Por exemplo,
logrt\[/home/zabbix/test.log\] e logrt\[/home/zabbix/test.log,\] são itens
diferentes. Modificar o item logrt\[/home/zabbix/test.log,,,10\] no Zabbix
Frontend para logrt\[/home/zabbix/test.log,,,20\] resultará na eliminação do
item logrt[/home/zabbix/test.log,,,10] da lista de verificações ativas do agente
e criará o item logrt\[/home/zabbix/test.log,,,20\] \(alguns atributos são
mantidos entre modificações no Frontend/Server, mas não no agente\).

O nome do arquivo é composto da soma de verificação MD5 da chave do item junto
com o seu comprimento para reduzir a possibilidade de colisões. Por exemplo,
o estado do item logrt\[/home/zabbix50/test.log,,,,,,,,/home/zabbix50/agent_private\]
será mantido no arquivo persistente c963ade4008054813bbc0a650bb8e09266.

Múltiplos itens de log podem usar o mesmo valor de **persistent\_dir**.

O parâmetro **persistent\_dir** é especificado levando em conta os leiautes 
do sistema de arquivo específico, pontos de montagem e configuração de espelhamento
de armazenamento - o arquivo persistente deve estar no mesmo sistema de arquivo
espelhado que o arquivo de log monitorado.

Se o diretório **persistent\_dir** não puder ser criado ou não existir, ou os
direitos de accesso para o Zabbix Agent não permitirem criar/gravar/ler/apagar
arquivos o item de log se torna NÃO SUPORTADO.

##### Carga de I/O

O arquivo persistente do item é atualizado após o envio bem-sucedido de cada
lote de dados (contendo dados do item) para o Server. Por exemplo, o tamanho de
'BufferSize' é 100. Se um item de log encontrou 70 registros correspondentes, então 
os primeiros 50 registros serão enviados em um lote, o arquivo persistente será
atualizado, e então os 20 registros restantes serão enviados \(talvez com algum 
atraso quando houver mais dado acumulado\) no segundo lote, e o arquivo persistente
será novamente atualizado.

[comment]: # ({/f06f25d7-57bbb0a9})


[comment]: # ({a01542e4-c5cdb98a})
#### Notas sobre arquivos persistentes para itens log\*\[\]

[comment]: # ({/a01542e4-c5cdb98a})

[comment]: # ({4993b6e8-975fe8d1})
##### Carga de I/O

O arquivo persistente do item é atualizado após o envio bem-sucedido de cada
lote de dados (contendo dados do item) para o Server. Por exemplo, o tamanho de
'BufferSize' é 100. Se um item de log encontrou 70 registros correspondentes, então 
os primeiros 50 registros serão enviados em um lote, o arquivo persistente será
atualizado, e então os 20 registros restantes serão enviados \(talvez com algum 
atraso quando houver mais dado acumulado\) no segundo lote, e o arquivo persistente
será novamente atualizado.

[comment]: # ({/4993b6e8-975fe8d1})

[comment]: # ({263a1e1e-df734a31})
#### Ações caso a comunicação entre Agent e Server falhe

Cada linha correspondente dos itens `log[]` e `logrt[]` e um resultado de
cada verificação de item `log.count[]` e `logrt.count[]` requerem um slot
livre na área designada de 50% no buffer de envio do agente. Os elementos
do buffer são regularmente enviados para o Server (ou Proxy) e os slots do
buffer ficam livres novamente.

Enquanto houver slots livres na área de log designada no buffer de envio
do agente e a comunicação falhar entre Agent e Server (ou Proxy), os
resultados de monitoramento de log são acumulados no buffer de envio. Isto
ajuda a mitigar falhas de comunicação curtas.

Durante longas falhas de comunicação todos os slots de log ficam ocupados
e as seguintes ações são tomadas:

-   itens de verificação `log[]` e `logrt[]` são parados. Quando a comunicação
    é restaurada e slots livres estão disponíveis no buffer as verificações
    são resumidas da posição anterior. Nenhuma linha correspondente é perdida,
    elas são apenas registradas mais tarde.
-   verificações `log.count[]` e `logrt.count[]` são paradas se `maxdelay = 0` 
    (padrão). O comportamento é similar aos itens `log[]` e `logrt[]` como
    descrito acima. Note que isto pode afetar os resultados de `log.count[]` e 
    `logrt.count[]`: por exemplo, uma verificação conta 100 linhas correspondentes
    em um arquivo de log, mas como não há slots livres no buffer a verificação
    é parada. Quando a comunicação é restabelecida o agente conta as mesmas
    100 linhas e mais 70 novas linhas correspondentes. O agente agora envia
    count = 170 como se tivesse sido encontrado em uma única verificação.
-   verificações `log.count[]` e `logrt.count[]` com `maxdelay > 0`: se
    não houve "salto" durante a verificação, então o comportamento é similar
    ao descrito acima. Se um "salto" sobre a linhas do arquivo de log ocorreu
    então a posição após o "salto" é mantida e o resultado contado é descartado.
    Assim, o agente tenta acompanhar um arquivo de log em crescimento mesmo
    em caso de falha de comunicação.

[comment]: # ({/263a1e1e-df734a31})

[comment]: # ({fc3e52d2-99ce1d02})
##### Propósito dos arquivos persistentes

Quando o Zabbix Agent é iniciado ele recebe uma lista de verificações ativas
do Zabbix Server ou Proxy. Para métricas de log\*\[\] ele recebe o tamanho do
log processado e a data de modificação para encontrar o ponto de partida para
monitorar o arquivo de log. Dependendo do tamanho real do arquivo de log e
data de modificação informada pelo sistema de arquivos o agente decide em
continuar o monitoramento do log a partir tamanho de log processado ou
re\-analisar o arquivo de log desde o início.

Um agente em execução mantém um extenso conjunto de atributos para o rastreio
de todos os arquivos de log monitorados entre as verificações. Este estado
em\-memória é perdido quando o agente é parado.

O novo parâmetro opcional **persistent\_dir** especifica o diretório para o
armazenamento deste estado dos itens de log\[\], log.count\[\], logrt\[\] ou 
logrt.count\[\] em um arquivo. O estado de um item de log é restaurado do arquivo
persistente após o Zabbix Agent ser reiniciado.

O caso de uso primário é o monitoramento de arquivo de log localizado em um
sistema de arquivo espelhado. Até algum momento em que o arquivo de log é
gravado em ambos os locais. Então os ambientes espelhados são separados.
Na cópia ativa o arquivo de log continua crescendo, recebendo novos registros.
O Zabbix Agent o analisa e envia os tamanhos de logs processados e data de
modificação para o Server. Na cópia passiva o arquivo de log continua o mesmo,
bem atrás da cópia ativa. Mais tarde o sistema operacional e o Zabbix Agent da
cópia passiva são reiniciados. O tamanho de log processado e a data de modificação
que o Agent recebe do Server pode não ser válida para a situação na cópia
passiva. Para continuar o monitoramento do log a partir do lugar em que o
agente parou no momento em que o espelhamento do sistema de arquivo foi
separado, o agente restaura seu estado de um arquivo persistente.

[comment]: # ({/fc3e52d2-99ce1d02})

[comment]: # ({e887034e-126798f0})
##### Operação do agente com arquivo persistente

Na inicialização o Zabbix Agent não sabe nada sobre arquivos persistentes.
Apenas após receber uma lista de verificações ativas do Zabbix Server \(proxy\) 
o agente identifica que alguns itens de log devem ser apoiados por arquivos
persistentes dentro de diretórios especificados.

Durante a operação do agente os arquivos persistentes são abertos para gravação
(com fopen(filename, "w")) e sobrescritos com os últimos dados. A chance de perder
dados de arquivo persistente se a sobrescrita e a separação dos espelhos de sistema
de arquivo ocorrerem ao mesmo tempo é muito pequena, portanto sem manipulação 
especial para este caso. A gravação em arquivo persistente NÃO é seguida por
sincronização forçada de mídia de armazenamento \(fsync\(\) não é chamado\).

A sobrescrita com os dados mais recentes é feita após o reporte bem-sucedido
de registros de arquivo de log ou metadados correspondentes \(tamanho de log
processado e data de modificação\) para o Zabbix Server. Isto pode ocorrer tão
frequentemente quanto cada verificação de item se o arquivo de log continuar
sendo alterado.

Sem ações especiais durante o desligamento do agente.

[comment]: # ({/e887034e-126798f0})

[comment]: # ({0bebe9ca-0cf19270})
Após receber uma lista de verificações ativas o agente marca os arquivos 
persistentes obsoletos para remoção. Um arquivo persistente se torna obsoleto
se: 1) o item de log correspondente não é mais monitorado, 2) um item de log 
é reconfigurado com uma localização de **persistent\_dir** diferente da anterior.


A remoção é efetuado com um atraso de 24h porque arquivos de log em estado 
NÃO SUPORTADO não são incluídos na lista de verificações ativas, mas eles podem 
se tornar SUPORTADOS mais tarde e seus arquivos persistentes podem vir a ser úteis.

Se o agente for parado e reiniciado antes que as 24h expirem, então os arquivos
obsoletos não serão apagados pois o Zabbix Agent não está mais recebendo do
Zabbix Server informações sobre suas localizações.

::: notewarning
Reconfigurar o **persistent\_dir** de um item de log de volta para a localização
**persistent\_dir** anterior enquanto o agente estiver parado, sem remover o
arquivo persistente antigo por usuário - ocasionará a restauração do estado do
agente a partir do arquivo persistente antigo, resultando em mensagens perdidas ou
falsos alertas.
:::

[comment]: # ({/0bebe9ca-0cf19270})

[comment]: # ({bd211244-2ce6da68})
##### Nomenclatura e localização de arquivos persistentes

O Zabbix Agent distingue as verificações ativas pelas suas chaves. Por exemplo,
logrt\[/home/zabbix/test.log\] e logrt\[/home/zabbix/test.log,\] são itens
diferentes. Modificar o item logrt\[/home/zabbix/test.log,,,10\] no Zabbix
Frontend para logrt\[/home/zabbix/test.log,,,20\] resultará na eliminação do
item logrt[/home/zabbix/test.log,,,10] da lista de verificações ativas do agente
e criará o item logrt\[/home/zabbix/test.log,,,20\] \(alguns atributos são
mantidos entre modificações no Frontend/Server, mas não no agente\).

O nome do arquivo é composto da soma de verificação MD5 da chave do item junto
com o seu comprimento para reduzir a possibilidade de colisões. Por exemplo,
o estado do item logrt\[/home/zabbix50/test.log,,,,,,,,/home/zabbix50/agent_private\]
será mantido no arquivo persistente c963ade4008054813bbc0a650bb8e09266.

Múltiplos itens de log podem usar o mesmo valor de **persistent\_dir**.

O parâmetro **persistent\_dir** é especificado levando em conta os leiautes 
do sistema de arquivo específico, pontos de montagem e configuração de espelhamento
de armazenamento - o arquivo persistente deve estar no mesmo sistema de arquivo
espelhado que o arquivo de log monitorado.

Se o diretório **persistent\_dir** não puder ser criado ou não existir, ou os
direitos de accesso para o Zabbix Agent não permitirem criar/gravar/ler/apagar
arquivos o item de log se torna NÃO SUPORTADO.

[comment]: # ({/bd211244-2ce6da68})

[comment]: # ({91a90d38-e74fd773})
Se os direitos de acesso ao armazenamento de arquivos persistentes forem removidos durante a operação do agente ou outros erros ocorrerem (p.e. disco cheio) então os erros são registrados no arquivo de log do agente, mas o item de log não se torna NÃO SUPORTADO.

[comment]: # ({/91a90d38-e74fd773})
