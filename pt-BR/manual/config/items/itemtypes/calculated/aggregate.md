[comment]: # translation:outdated

[comment]: # ({cc8dc0e6-bfe1a94d})
# Cálculos agregados

[comment]: # ({/cc8dc0e6-bfe1a94d})

[comment]: # ({42dbaf39-f8e62abe})
#### Visão geral

Em [cálculos](/manual/config/items/itemtypes/calculated) agregados a 
informação de vários itens pode ser coletada pelo Zabbix Server (pela
realização de consultas diretas ao banco de dados) para calcular uma
agregação, dependendo da função agregada utilizada.

Cálculos agregados não requerem qualquer agente em execução no
host sendo monitorado.

Para definir cálculos agregados, selecione o tipo de item **Calculado**.

[comment]: # ({/42dbaf39-f8e62abe})

[comment]: # ({91341322-63d83a0c})
#### Sintaxe

Agregados podem ser recuperados trabalhando com:

-   histórico de itens:

```{=html}
<!-- -->
```
    aggregate_function(function(/host/item,parameter),function(/host2/item2,parameter),...)

-   uma [função foreach](/manual/appendix/functions/aggregate/foreach)
    como único parâmetro:

```{=html}
<!-- -->
```
    aggregate_function(foreach_function(/*/key?[group="host group"],timeperiod))

onde:

- `aggregate_function` é uma das [funções agregadas](/manual/appendix/functions/aggregate#aggregate-functions-1) suportadas: avg, max, min, sum, etc.
- `foreach_function` é uma das funções foreach suportadas: avg_foreach, count_foreach, etc. 

Funções foreach usam um filtro de item, para operar com o histórico de 
múltiplos itens, e retornam um array de valores - um para cada item.

::: noteclassic
Se o agregado resultar em um valor numérico flutuante (float) ele será aparado
para um inteiro se o tipo de informação do item é *Numérico (unsigned)*.
:::

Um cálculo agregado pode se tornar não suportado se:

-   nenhum dos itens referenciados foi encontrado (o que pode ocorrer se a 
    chave do item está incorreta, nenhum dos itens existe ou todos os grupos
    incluídos estão incorretos)
-   nenhum dado para calcular uma função

[comment]: # ({/91341322-63d83a0c})

[comment]: # ({60d5ae34-3bbe172c})
#### Exemplos de uso

Exemplos de chaves para cálculos agregados.

[comment]: # ({/60d5ae34-3bbe172c})

[comment]: # ({ca408982-874bcf94})
##### Exemplo 1

Espaço em disco total do grupo de host 'MySQL Servers'.

    sum(last_foreach(/*/vfs.fs.size[/,total]?[group="MySQL Servers"]))

[comment]: # ({/ca408982-874bcf94})

[comment]: # ({4e380a77-3182672a})
##### Exemplo 2

Soma dos valores mais recentes de todos os itens correspondentes a net.if.in\[\*\] no host.

    sum(last_foreach(/host/net.if.in[*]))

[comment]: # ({/4e380a77-3182672a})

[comment]: # ({2e6cd982-cdf2da8b})
##### Exemplo 3

Média de carga de processador (load) do grupo de host 'MySQL Servers'.

    avg(last_foreach(/*/system.cpu.load[,avg1]?[group="MySQL Servers"]))

[comment]: # ({/2e6cd982-cdf2da8b})

[comment]: # ({5e55d8cb-db9c8fce})
##### Exemplo 4

Média de 5 minutos do número de consultas por segundo para grupo de host 'MySQL Servers'.

    avg(avg_foreach(/*/mysql.qps?[group="MySQL Servers"],5m))

[comment]: # ({/5e55d8cb-db9c8fce})

[comment]: # ({244bf858-0233edd3})
##### Exemplo 5

Média de carga de CPU (load) de todos os hosts em múltiplos grupos de host
que possuem etiquetas específicas.

    avg(last_foreach(/*/system.cpu.load?[(group="Servers A" or group="Servers B" or group="Servers C") and (tag="Service:" or tag="Importance:High")]))

[comment]: # ({/244bf858-0233edd3})

[comment]: # ({3c123f2c-30448f5d})
##### Exemplo 6

Cálculo usado nas somas dos valores mais recentes de um grupo de host inteiro.

    sum(last_foreach(/*/net.if.out[eth0,bytes]?[group="video"])) / sum(last_foreach(/*/nginx_stat.sh[active]?[group="video"])) 

[comment]: # ({/3c123f2c-30448f5d})

[comment]: # ({68236d8e-3e9404a6})
##### Exemplo 7

O número total de itens não suportados no grupo de host 'Zabbix servers'.

    sum(last_foreach(/*/zabbix[host,,items_unsupported]?[group="Zabbix servers"]))

[comment]: # ({/68236d8e-3e9404a6})

[comment]: # ({new-f094da29})

##### Examples of correct/incorrect syntax

Expressions (including function calls) cannot be used as history, trend, or foreach [function](/manual/appendix/functions) parameters. However, those functions themselves can be used in other (non-historical) function parameters.

|Expression|Example|
|-|---------|
|Valid|`avg(last(/host/key1),last(/host/key2)*10,last(/host/key1)*100)`<br>`max(avg(avg_foreach(/*/system.cpu.load?[group="Servers A"],5m)),avg(avg_foreach(/*/system.cpu.load?[group="Servers B"],5m)),avg(avg_foreach(/*/system.cpu.load?[group="Servers C"],5m)))`|
|Invalid|`sum(/host/key, 10+2)`<br>`sum(/host/key, avg(10, 2))`<br>`sum(/host/key, last(/host/key2))`|

Note that in an expression like:

    sum(sum_foreach(//resptime[*],5m))/sum(count_foreach(//resptime[*],5m))

it cannot be guaranteed that both parts of the equation will always have the same set of values. 
While one part of the expression is evaluated, a new value for the requested period may arrive and 
then the other part of the expression will have a different set of values.

[comment]: # ({/new-f094da29})
