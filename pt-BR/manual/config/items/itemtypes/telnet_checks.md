[comment]: # translation:outdated

[comment]: # ({a7debf1e-9e60926e})
# 10 Verificações telnet

[comment]: # ({/a7debf1e-9e60926e})

[comment]: # ({33bf0258-2cd46777})
#### Visão geral

Verificações Telnet são executadas como monitoramento sem agente. O
Zabbix Agent não é necessário para verificações Telnet.

[comment]: # ({/33bf0258-2cd46777})

[comment]: # ({c96d70d8-3db6c676})
#### Campos configuráveis

O(s) comando(s) a serem executados devem ser informados no campo
**Scripts executados** na configuração do item.\
Múltiplos comandos podem ser executados um após o outro colocando-os
em uma nova linha. Neste caso o valor retornado também será formatado
como multilinha.

Caracteres suportados para encerramento do prompt shell:

-   $
-   \#
-   >
-   %

::: noteclassic
Uma linha de prompt telnet que termine com um destes caracteres será
removida do valor retornado, mas apenas para o primeiro comando na
lista de comandos, p.e. apenas no início da sessão telnet.
:::

|Chave|Descrição|Comentários|
|---|-----------|--------|
|**telnet.run\[<descrição curta única>,<ip>,<porta>,<codificação>\]**|Executa um comando em um dispositivo remoto usando conexão telnet|<|

::: noteimportant
Se uma verificação telnet retorna um valor com caracteres
não-ASCII e em uma codificação não-UTF8 então o parâmetro
*<codificação>* da chave deve ser apropriadamente especificado.
Consulte a página [codificação de valores retornados](/manual/appendix/items/encoding_of_values) para mais detalhes.
:::

[comment]: # ({/c96d70d8-3db6c676})
