<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="pt-BR" datatype="plaintext" original="manual/config/items/itemtypes/external.md">
    <body>
      <trans-unit id="0ee06d3b" xml:space="preserve">
        <source># 11 External checks</source>
        <target state="needs-translation"># 11 Verificações externas</target>
      </trans-unit>
      <trans-unit id="cd36d798" xml:space="preserve">
        <source>#### Overview

External check is a check executed by Zabbix server by [running a shell
script](/manual/appendix/command_execution) or a binary. However, when
hosts are monitored by a Zabbix proxy, the external checks are executed
by the proxy.

External checks do not require any agent running on a host being
monitored.

The syntax of the item key is:

    script[&lt;parameter1&gt;,&lt;parameter2&gt;,...]

Where:

|ARGUMENT|DEFINITION|
|--------|----------|
|**script**|Name of a shell script or a binary.|
|**parameter(s)**|Optional command line parameters.|

If you don't want to pass any parameters to the script you may use:

    script[] or
    script

Zabbix server will look in the directory defined as the location for
external scripts (parameter 'ExternalScripts' in [Zabbix server
configuration file](/manual/appendix/config/zabbix_server)) and execute
the command. The command will be executed as the user Zabbix server runs
as, so any access permissions or environment variables should be handled
in a wrapper script, if necessary, and permissions on the command should
allow that user to execute it. Only commands in the specified directory
are available for execution.

::: notewarning
Do not overuse external checks! As each script
requires starting a fork process by Zabbix server, running many scripts
can decrease Zabbix performance a lot.
:::</source>
        <target state="needs-translation">#### Visão geral

Verificação externa é uma verificação executada pelo Zabbix Server pela 
[execução de um script shell](/manual/appendix/command_execution) ou um
binário. No entanto, quando hosts são monitorados por um Zabbix Proxy, 
as verificações externas são executadas pelo Proxy.

As verificações externas não requerem qualquer agente em execução no host
sendo monitorado.

A sintaxe da chave do item é:

    script[&lt;parameter1&gt;,&lt;parameter2&gt;,...]

Onde:

|ARGUMENTO|DEFINIÇÃO|
|--------|----------|
|**script**|Nome de um script shell ou um binário.|
|**parâmetro(s)**|Parâmetros de linha de comando opcionais.|

Se você não quer passar nenhum parâmetro para o script você pode usar:

    script[] ou
    script

O Zabbix Server consultará o diretório definido como localização para
scripts externos (parâmetro 'ExternalScripts' no [arquivo de configuração do Zabbix Server](/manual/appendix/config/zabbix_server)) e executará o comando. 
O comando será executado com o mesmo usuário com o qual o Zabbix Server
está sendo executado, então quaisquer permissões de acesso ou variáveis
de ambiente devem ser manipuladas em scripts agrupados (wrapper script), 
se necessário, e as permissões para o comando devem permitir execução para
o usuário. Apenas comandos no diretório especificado ficam disponíveis
para execução.

::: notewarning
Não abuse de verificações externas! Como cada script requer iniciar um
processo filho (fork) pelo Zabbix Server, a execução de muitos scripts
pode diminuir muito a performance do Zabbix.
:::</target>
      </trans-unit>
      <trans-unit id="55e878ce" xml:space="preserve">
        <source>#### Usage example

Executing the script **check\_oracle.sh** with the first parameters
'-h'. The second parameter will be replaced by IP address or DNS name,
depending on the selection in the host properties.

    check_oracle.sh["-h","{HOST.CONN}"]

Assuming host is configured to use IP address, Zabbix will execute:

    check_oracle.sh '-h' '192.168.1.4'</source>
        <target state="needs-translation">#### Exemplo de uso

Executando o script **check\_oracle.sh** com o primeiro parâmetro
'-h'. O segundo parâmetro será substituído pelo endereço IP ou nome DNS,
dependendo da seleção nas propriedades do host.

    check_oracle.sh["-h","{HOST.CONN}"]

Assumindo que o host está configurado para usar o endereço IP, o Zabbix
irá executar:

    check_oracle.sh '-h' '192.168.1.4'</target>
      </trans-unit>
      <trans-unit id="011b7534" xml:space="preserve">
        <source>#### External check result

The return value of the check is standard output together with standard
error (the full output with trimmed trailing whitespace is returned
since Zabbix 2.0).

::: noteimportant
A text (character, log or text type of
information) item will not become unsupported in case of standard error
output.
:::

In case the requested script is not found or Zabbix server has no
permissions to execute it, the item will become unsupported and
corresponding error message will be set. In case of a timeout, the item
will be marked as unsupported as well, an according error message will
be displayed and the forked process for the script will be killed.</source>
        <target state="needs-translation">#### Resultado de verificação externa

O valor de retorno da verificação externa é a saída padrão junto com a
saída de erro padrão (a saída completa com espaços em branco finais 
removidos é retornada desde o Zabbix 2.0).

::: noteimportant
Um item texto (tipo de informação caracter, log ou texto) não se tornará
não suportado no caso de haver dados na saída de erro padrão.
:::

No caso de o script requisitado não ser encontrado ou o Zabbix Server 
não possuir permissões para executá-lo, o item se tornará não suportado
e a mensagem de erro correspondente será apresentada. No caso de tempo
esgotado, o item será marcado também como não suportado, uma mensagem
de erro relacionada será apresentada e o processo filho para o script
será destruído.</target>
      </trans-unit>
    </body>
  </file>
</xliff>
