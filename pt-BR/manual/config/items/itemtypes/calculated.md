[comment]: # translation:outdated

[comment]: # ({0213f9bb-b8c65ad1})
# 7 Itens calculados

[comment]: # ({/0213f9bb-b8c65ad1})

[comment]: # ({7a8b4c0d-43ebe58c})
#### Visão geral

Com itens calculados é possível criar cálculos baseados nos valores
de outros itens.

Cálculos podem usar ambos:

-   valores únicos de itens individuais
-   filtros complexos para selecionar múltiplos itens para agregação (consulte
    [cálculos agregados](/manual/config/items/itemtypes/calculated/aggregate)
    para detalhes)

Assim, itens calculados são uma forma de criar fontes de dados virtuais. Todos
os cálculos são efetuados unicamente pelo Zabbix Server. Os valores são
periodicamente calculados com base na expressão aritmética utilizada.

O dado resultante é armazenado no banco de dados do Zabbix assim como para
qualquer outro item; ambos os valores históricos e de tendência são armazenados
de modo que gráficos podem ser gerados.

::: noteclassic
Se o resultado do cálculo for um valor numérico flutuante (float) ele será
aparado para um inteiro se o tipo de informação do item calculado for
*Numérico (unsigned)*.
:::

Itens calculados compartilham sua sintaxe com [expressões de gatilho](/manual/config/triggers/expression). A comparação com strings é permitida em itens calculados.
Itens calculados podem ser referenciados por macros ou outras entidades
assim como qualquer outro tipo de item.

Para usar itens calculados, escolha o tipo de item **Calculado**.

[comment]: # ({/7a8b4c0d-43ebe58c})

[comment]: # ({469441e7-ddc35784})
#### Campos configuráveis

A **chave** é um identificador de item único (por host). Você pode criar qualquer
nome de chave usando símbolos suportados.

Definições de cálculos devem ser inseridas no campo **Fórmula**. Não há conexão
virtual entre a fórmula e a chave. Os parâmetros de chave não são usados na fórmula
de nenhuma forma.

A sintaxe de uma fórmula simples é:

    function(/host/key,<parameter1>,<parameter2>,...)

onde:

|   |   |
|---|---|
|*função*|Uma das [funções suportadas](/manual/appendix/functions): last, min, max, avg, count, etc|
|*host*|Host do item usado para o cálculo.<br>O host atual pode ser omitido (p.e. como em `function(//key,parameter,...)`).|
|*chave*|Chave do item usado para o cálculo.|
|*parâmetro(s)*|Parâmetros da função, se exigido.|

::: noteimportant
[Macros de usuário](/manual/config/macros/user_macros)
na fórmula serão expandidas se usadas para referenciar um parâmetro de função
ou uma constante. Macros de usuário NÃO serão expandidas se referenciando
uma função, nome de host, chave de item, parâmetro ou operador de chave de item.
:::

Um fórmula mais complexa pode usar uma combinação de funções, operadores e
parêntesis. Você pode usar todas as funções e [operadores](/manual/config/triggers/expression#operators) suportados nas expressões de gatilho. A lógica e 
precedência de operador são exatamente as mesmas.

Diferente das expressões de gatilho, o Zabbix processa itens calculados
de acordo com o intervalo de atualização do item, não ao receber um novo valor.

Todos os itens que são referenciados pelas funções históricas na fórmula do item
calculado devem existir e estar coletando dados. Ademais, se você alterar a chave
de um item referenciado, você tem que atualizar manualmente quaisquer fórmulas
que façam uso dessa chave.

Um item calculado pode se tornar não suportado em vários casos:

-   item(ns) referenciado(s)
    -   não foi encontrado
    -   está desabilitado
    -   pertence a um host desabilitado
    -   não é suportado (exceto com função nodata() e [operadores](/manual/config/triggers/expression#operators) com valores desconhecidos)
-   nenhum dado para calcular a função
-   divisão por zero
-   sintaxe utilizada incorreta

[comment]: # ({/469441e7-ddc35784})

[comment]: # ({fca8deac-82c22f4f})
#### Exemplos de uso

[comment]: # ({/fca8deac-82c22f4f})

[comment]: # ({594fb47b-426ab71a})
##### Exemplo 1

Cálculo de porcentagem de espaço livre em disco em '/'.

Uso da função **last**:

    100*last(//vfs.fs.size[/,free])/last(//vfs.fs.size[/,total])

O Zabbix obterá os valores mais recentes para os espaços livre e total
do disco e calculará a porcentagem de acordo com a fórmula dada.

[comment]: # ({/594fb47b-426ab71a})

[comment]: # ({73fe4dad-77bcdfd9})
##### Exemplo 2

Cálculo da média de 10 minutos do número de valores processados pelo
Zabbix.

Uso da função **avg**:

    avg(/Zabbix Server/zabbix[wcache,values],10m)

Note que o uso extensivo de itens calculados com longos períodos de tempo
pode afetar a performance do Zabbix Server.

[comment]: # ({/73fe4dad-77bcdfd9})

[comment]: # ({0de41525-5d068733})
##### Exemplo 3

Cálculo de banda de rede total em eth0.

Soma de duas funções:

    last(//net.if.in[eth0,bytes])+last(//net.if.out[eth0,bytes])

[comment]: # ({/0de41525-5d068733})

[comment]: # ({0a83f0b4-f8b53af1})
##### Exemplo 4

Cálculo da porcentagem de tráfego de entrada.

Expressão mais complexa:

    100*last(//net.if.in[eth0,bytes])/(last(//net.if.in[eth0,bytes])+last(//net.if.out[eth0,bytes]))

Veja também: [Exemplos de cálculos agregados](/manual/config/items/itemtypes/calculated/aggregate#usage_examples)

[comment]: # ({/0a83f0b4-f8b53af1})
