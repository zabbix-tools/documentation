<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="pt-BR" datatype="plaintext" original="manual/config/items/itemtypes/simple_checks.md">
    <body>
      <trans-unit id="be4ab03d" xml:space="preserve">
        <source># 5 Simple checks</source>
        <target state="needs-translation"># 5 Verificações simples</target>
      </trans-unit>
      <trans-unit id="76023448" xml:space="preserve">
        <source>### Overview

Simple checks are normally used for remote agent-less checks of services.

Note that Zabbix agent is not needed for simple checks. Zabbix
server/proxy is responsible for the processing of simple checks (making
external connections, etc).

Examples of using simple checks:

    net.tcp.service[ftp,,155]
    net.tcp.service[http]
    net.tcp.service.perf[http,,8080]
    net.udp.service.perf[ntp]

::: noteclassic
*User name* and *Password* fields (limited to 255 characters) in simple check item configuration are used for VMware monitoring items; ignored otherwise.
:::</source>
        <target state="needs-translation">#### Visão geral

Verificações simples são normalmente usadas para verificações remotas 
de serviço, sem agente.

Note que o Zabbix Agent não é necessário para verificações simples. O Zabbix
Server/Proxy é responsável pelo processamento das verificações simples (fazendo
conexões externas, etc.).

Exemplos de uso de verificações simples:

    net.tcp.service[ftp,,155]
    net.tcp.service[http]
    net.tcp.service.perf[http,,8080]
    net.udp.service.perf[ntp]

::: noteclassic
Os campos *Nome de usuário* e *Senha* na configuração de um item de verificação simples
são usados para monitoramento de itens VMware; são ignorados de outra forma.
:::</target>
      </trans-unit>
      <trans-unit id="f59a5ccc" xml:space="preserve">
        <source>### Supported checks

The item keys are listed without optional parameters and additional information. Click on the item key to see the full details.

See also [VMware monitoring item keys](/manual/vm_monitoring/vmware_keys).

|Item key|Description|
|--|--------|
|[icmpping](#icmpping)|The host accessibility by ICMP ping.|
|[icmppingloss](#icmppingloss)|The percentage of lost packets.|
|[icmppingsec](#icmppingsec)|The ICMP ping response time.|
|[net.tcp.service](#nettcpservice)|Checks if a service is running and accepting TCP connections.|
|[net.tcp.service.perf](#nettcpserviceperf)|Checks the performance of a TCP service.|
|[net.udp.service](#netudpservice)|Checks if a service is running and responding to UDP requests.|
|[net.udp.service.perf](#netudpserviceperf)|Checks the performance of a UDP service.|</source>
        <target state="needs-translation">#### Verificações simples suportadas

Lista de verificações simples suportadas:

Veja também:

- [Chaves de item de monitoramento VMware](/manual/config/items/itemtypes/simple_checks/vmware_keys)

|Chave|&lt;|&lt;|&lt;|&lt;|
|---|-|-|-|-|
|&lt;|**Descrição**|**Valore de retorno**|**Parâmetros**|**Comentários**|
|icmpping\[&lt;target&gt;,&lt;packets&gt;,&lt;interval&gt;,&lt;size&gt;,&lt;timeout&gt;\]|&lt;|&lt;|&lt;|&lt;|
|&lt;|Acessibilidade de host por ping ICMP.|0 - falha de ping ICMP&lt;br&gt;&lt;br&gt;1 - ping ICMP bem-sucedido|**target** - IP do host ou nome DNS&lt;br&gt;**packets** - número de pacotes&lt;br&gt;**interval** - tempo entre pacotes sucessivos em milissegundos&lt;br&gt;**size** - tamanho do pacote em bytes&lt;br&gt;**timeout** - tempo limite em milissegundos|Exemplo:&lt;br&gt;=&gt; icmpping\[,4\] → se ao menos um pacote dos quatro for retornado, o item retornará 1.&lt;br&gt;&lt;br&gt;Veja também: tabela de [valores padrão](simple_checks#icmp_pings).|
|icmppingloss\[&lt;target&gt;,&lt;packets&gt;,&lt;interval&gt;,&lt;size&gt;,&lt;timeout&gt;\]|&lt;|&lt;|&lt;|&lt;|
|&lt;|Porcentagem de pacotes perdidos.|Número flutuante.|**target** - IP do host ou nome DNS&lt;br&gt;**packets** - número de pacotes&lt;br&gt;**interval** - tempo entre pacotes sucessivos em milissegundos&lt;br&gt;**size** - tamanho do pacote em bytes&lt;br&gt;**timeout** - tempo limite em milissegundos|Veja também: tabela de [valores padrão](simple_checks#icmp_pings).|
|icmppingsec\[&lt;target&gt;,&lt;packets&gt;,&lt;interval&gt;,&lt;size&gt;,&lt;timeout&gt;,&lt;mode&gt;\]|&lt;|&lt;|&lt;|&lt;|
|&lt;|tempo de resposta de ping ICMP (em segundos).|Número flutuante.|**target** - IP do host ou nome DNS&lt;br&gt;**packets** - número de pacotes&lt;br&gt;**interval** - tempo entre pacotes sucessivos em milissegundos&lt;br&gt;**size** - tamanho do pacote em bytes&lt;br&gt;**timeout** - tempo limite em milissegundos&lt;br&gt;**mode** - valores possíveis: *min*, *max*, *avg* (padrão)(mínimo, máximo e média)|Pacotes que são perdidos ou expirados não são usados no cálculo.&lt;br&gt;&lt;br&gt;Se host não está disponível (tempo limite alcançado), o item retornará 0.&lt;br&gt;Se o valor de retorno for menor que 0.0001 segundos, o valor será fizxado em 0.0001 segundos.&lt;br&gt;&lt;br&gt;Veja também: tabela de [valores padrão](simple_checks#icmp_pings).|
|net.tcp.service\[service,&lt;ip&gt;,&lt;port&gt;\]|&lt;|&lt;|&lt;|&lt;|
|&lt;|Verifica se o serviço está sendo executado e aceitando conexões TCP.|0 - serviço está parado&lt;br&gt;&lt;br&gt;1 - serviço está em execução|**service** - valores possíveis: *ssh*, *ldap*, *smtp*, *ftp*, *http*, *pop*, *nntp*, *imap*, *tcp*, *https*, *telnet* (veja [detalhes](/manual/appendix/items/service_check_details))&lt;br&gt;**ip** - endereço IP ou nome DNS (por padrão IP/DNS do host é usado)&lt;br&gt;**port** - número da porta (por padrão é usado o número de porta padrão do serviço).|Exemplo:&lt;br&gt;=&gt; net.tcp.service\[ftp,,45\] → pode ser usado para testar disponibilidade de servidor FTP na porta 45/TCP.&lt;br&gt;&lt;br&gt;Note que com serviço *tcp* a indicação de porta é obrigatória.&lt;br&gt;Estas verificações podem resultar em mensagens adicionais nos arquivos de log dos daemons de serviço (comumente sessões SMTP e SSH sendo logadas).&lt;br&gt;Verificação de protocolos criptografados (como IMAP na porta 993 ou POP na porta 995) não são suportados atualmente. Como alternativa, por favor utilize net.tcp.service\[tcp,&lt;ip&gt;,port\] para verificações como estas.&lt;br&gt;Os serviços *https* e *telnet* são suportados desde o Zabbix 2.0.|
|net.tcp.service.perf\[service,&lt;ip&gt;,&lt;port&gt;\]|&lt;|&lt;|&lt;|&lt;|
|&lt;|Verifica performance de serviço TCP.|Número flutuante.&lt;br&gt;&lt;br&gt;0.000000 - serviço está parado&lt;br&gt;&lt;br&gt;seconds - o número de segundos gastos aguardando pela resposta do serviço|**service** - valores possíveis: *ssh*, *ldap*, *smtp*, *ftp*, *http*, *pop*, *nntp*, *imap*, *tcp*, *https*, *telnet* (veja [detalhes](/manual/appendix/items/service_check_details))&lt;br&gt;**ip** - endereço IP ou nome DNS (por padrão IP/DNS do host é usado)&lt;br&gt;**port** - número de porta (por padrão é usado o número de porta padrão do serviço).|Exemplo:&lt;br&gt;=&gt; net.tcp.service.perf\[ssh\] → pode ser usado para testar a velocidade de resposta inicial de um servidor SSH.&lt;br&gt;&lt;br&gt;Note que com serviço *tcp* a indicação de porta é obrigatória.&lt;br&gt;Verificação de protocolos criptografados (como IMAP na porta 993 ou POP na porta 995) não são suportados atualmente. Como alternativa, por favor utilize net.tcp.service.perf\[tcp,&lt;ip&gt;,port\] para verificações como estas.&lt;br&gt;Os serviços *https* e *telnet* são suportados desde o Zabbix 2.0.&lt;br&gt;Chamado de tcp\_perf antes do Zabbix 2.0.|
|net.udp.service\[service,&lt;ip&gt;,&lt;port&gt;\]|&lt;|&lt;|&lt;|&lt;|
|&lt;|Verifica se o serviço está sendo executado e respondendo a requisições UDP.|0 - serviço está parado&lt;br&gt;&lt;br&gt;1 - serviço está sendo executado|**service** - valores possíveis: *ntp* (veja [detalhes](/manual/appendix/items/service_check_details))&lt;br&gt;**ip** - endereço IP ou nome DNS (por padrão o IP/DNS do host é usado)&lt;br&gt;**port** - número de porta (por padrão é usado o número de porta padrão do serviço).|Exemplo:&lt;br&gt;=&gt; net.udp.service\[ntp,,45\] → pode ser usado para testar a disponibilidade do serviço NTP na porta 45/UDP.&lt;br&gt;&lt;br&gt;Este item é suportado desde o Zabbix 3.0, mas o serviço *ntp* estava disponível para o item net.tcp.service\[\] em versões anteriores.|
|net.udp.service.perf\[service,&lt;ip&gt;,&lt;port&gt;\]|&lt;|&lt;|&lt;|&lt;|
|&lt;|Verifica a performance de serviço UDP.|Número flutunte.&lt;br&gt;&lt;br&gt;0.000000 - serviço está parado&lt;br&gt;&lt;br&gt;seconds - o número de segundos gastos aguardando pela resposta do serviço|**service** - valores possíveis: *ntp* (veja [detalhes](/manual/appendix/items/service_check_details))&lt;br&gt;**ip** - endereço IP ou nome DNS (por padrão IP/DNS do host é usado)&lt;br&gt;**port** - número de porta (por padrão é usado o número de porta padrão do serviço).|Exemplo:&lt;br&gt;=&gt; net.udp.service.perf\[ntp\] → pode ser usado para testar o tempo de resposta do serviço NTP.&lt;br&gt;&lt;br&gt;Este item é suportado desde o Zabbix 3.0, mas o serviço *ntp* estava disponível para o item net.tcp.service\[\] em versões anteriores.|</target>
      </trans-unit>
      <trans-unit id="23d7246f" xml:space="preserve">
        <source>
### Item key details

Parameters without angle brackets are mandatory. Parameters marked with angle brackets **&lt;** **&gt;** are optional.</source>
      </trans-unit>
      <trans-unit id="7d8dff2e" xml:space="preserve">
        <source>
##### icmpping\[&lt;target&gt;,&lt;packets&gt;,&lt;interval&gt;,&lt;size&gt;,&lt;timeout&gt;,&lt;options&gt;\] {#icmpping}

&lt;br&gt;
The host accessibility by ICMP ping.&lt;br&gt;
Return value: *0* - ICMP ping fails; *1* - ICMP ping successful.

Parameters:

-   **target** - the host IP or DNS name;
-   **packets** - the number of packets;
-   **interval** - the time between successive packets in milliseconds;
-   **size** - the packet size in bytes;
-   **timeout** - the timeout in milliseconds;
-   **options** - used for allowing redirect: if empty (default value), redirected responses are treated as target host down; if set to *allow_redirect*, redirected responses are treated as target host up.

See also the table of [default values](#default-values).

Example:

    icmpping[,4] #If at least one packet of the four is returned, the item will return 1.</source>
      </trans-unit>
      <trans-unit id="6cd0f1b1" xml:space="preserve">
        <source>
##### icmppingloss\[&lt;target&gt;,&lt;packets&gt;,&lt;interval&gt;,&lt;size&gt;,&lt;timeout&gt;,&lt;options&gt;\] {#icmppingloss}

&lt;br&gt;
The percentage of lost packets.&lt;br&gt;
Return value: *Float*.

Parameters:

-   **target** - the host IP or DNS name;
-   **packets** - the number of packets;
-   **interval** - the time between successive packets in milliseconds;
-   **size** - the packet size in bytes;
-   **timeout** - the timeout in milliseconds;
-   **options** - used for allowing redirect: if empty (default value), redirected responses are treated as target host down; if set to *allow_redirect*, redirected responses are treated as target host up.

See also the table of [default values](#default-values).</source>
      </trans-unit>
      <trans-unit id="b5636838" xml:space="preserve">
        <source>
##### icmppingsec\[&lt;target&gt;,&lt;packets&gt;,&lt;interval&gt;,&lt;size&gt;,&lt;timeout&gt;,&lt;mode&gt;,&lt;options&gt;\] {#icmppingsec}

&lt;br&gt;
The ICMP ping response time (in seconds).&lt;br&gt;
Return value: *Float*.

Parameters:

-   **target** - the host IP or DNS name;
-   **packets** - the number of packets;
-   **interval** - the time between successive packets in milliseconds;
-   **size** - the packet size in bytes;
-   **timeout** - the timeout in milliseconds;
-   **mode** - possible values: *min*, *max*, or *avg* (default);
-   **options** - used for allowing redirect: if empty (default value), redirected responses are treated as target host down; if set to *allow_redirect*, redirected responses are treated as target host up.

Comments:

-   Packets which are lost or timed out are not used in the calculation;
-   If the host is not available (timeout reached), the item will return 0;
-   If the return value is less than 0.0001 seconds, the value will be set to 0.0001 seconds;
-   See also the table of [default values](#default-values).</source>
      </trans-unit>
      <trans-unit id="b0a71170" xml:space="preserve">
        <source>
##### net.tcp.service[service,&lt;ip&gt;,&lt;port&gt;] {#nettcpservice}

&lt;br&gt;
Checks if a service is running and accepting TCP connections.&lt;br&gt;
Return value: *0* - the service is down; *1* - the service is running.

Parameters:

-   **service** - possible values: *ssh*, *ldap*, *smtp*, *ftp*, *http*, *pop*, *nntp*, *imap*, *tcp*, *https*, *telnet* (see [details](/manual/appendix/items/service_check_details));
-   **ip** - the IP address or DNS name (by default the host IP/DNS is used);
-   **port** - the port number (by default the standard service port number is used).

Comments:

-   Note that with *tcp* service indicating the port is mandatory;
-   These checks may result in additional messages in system daemon logfiles (SMTP and SSH sessions being logged usually);
-   Checking of encrypted protocols (like IMAP on port 993 or POP on port 995) is currently not supported. As a workaround, please use `net.tcp.service[tcp,&lt;ip&gt;,port]` for checks like these.

Example:

    net.tcp.service[ftp,,45] #This item can be used to test the availability of FTP server on TCP port 45.</source>
      </trans-unit>
      <trans-unit id="946385b4" xml:space="preserve">
        <source>
##### net.tcp.service.perf[service,&lt;ip&gt;,&lt;port&gt;] {#nettcpserviceperf}

&lt;br&gt;
Checks the performance of a TCP service.&lt;br&gt;
Return value: *Float*: *0.000000* - the service is down; *seconds* - the number of seconds spent while connecting to the service.

Parameters:

-   **service** - possible values: *ssh*, *ldap*, *smtp*, *ftp*, *http*, *pop*, *nntp*, *imap*, *tcp*, *https*, *telnet* (see [details](/manual/appendix/items/service_check_details));
-   **ip** - the IP address or DNS name (by default the host IP/DNS is used);
-   **port** - the port number (by default the standard service port number is used).

Comments:

-   Note that with *tcp* service indicating the port is mandatory;
-   Checking of encrypted protocols (like IMAP on port 993 or POP on port 995) is currently not supported. As a workaround, please use `net.tcp.service[tcp,&lt;ip&gt;,port]` for checks like these.

Example:

    net.tcp.service.perf[ssh] #This item can be used to test the speed of initial response from SSH server.</source>
      </trans-unit>
      <trans-unit id="2859223a" xml:space="preserve">
        <source>
##### net.udp.service[service,&lt;ip&gt;,&lt;port&gt;] {#netudpservice}

&lt;br&gt;
Checks if a service is running and responding to UDP requests.&lt;br&gt;
Return value: *0* - the service is down; *1* - the service is running.

Parameters:

-   **service** - possible values: *ntp* (see [details](/manual/appendix/items/service_check_details));
-   **ip** - the IP address or DNS name (by default the host IP/DNS is used);
-   **port** - the port number (by default the standard service port number is used).

Example:

    net.udp.service[ntp,,45] #This item can be used to test the availability of NTP service on UDP port 45.</source>
      </trans-unit>
      <trans-unit id="9cf5922c" xml:space="preserve">
        <source>
##### net.udp.service.perf[service,&lt;ip&gt;,&lt;port&gt;] {#netudpserviceperf}

&lt;br&gt;
Checks the performance of a UDP service.&lt;br&gt;
Return value: *Float*: *0.000000* - the service is down; *seconds* - the number of seconds spent waiting for response from the service.

Parameters:

-   **service** - possible values: *ntp* (see [details](/manual/appendix/items/service_check_details));
-   **ip** - the IP address or DNS name (by default the host IP/DNS is used);
-   **port** - the port number (by default the standard service port number is used).

Example:

    net.udp.service.perf[ntp] #This item can be used to test the response time from NTP service.</source>
      </trans-unit>
      <trans-unit id="85612a2b" xml:space="preserve">
        <source>  
::: noteimportant
For SourceIP support in LDAP simple checks (e.g. `net.tcp.service[ldap]`), OpenLDAP version 2.6.1 or above is required.
:::</source>
      </trans-unit>
      <trans-unit id="92a6c4ae" xml:space="preserve">
        <source>##### Timeout processing

Zabbix will not process a simple check longer than the Timeout seconds
defined in the Zabbix server/proxy configuration file.</source>
        <target state="needs-translation">##### Tempo limite de processamento

O Zabbix não processará uma verificação simples por mais tempo do que os segundos 
de limite (Timeout) definidos no arquivo de configuração do Zabbix Server/Proxy.</target>
      </trans-unit>
      <trans-unit id="b08a4508" xml:space="preserve">
        <source>### ICMP pings

Zabbix uses an external utility **[fping](https://fping.org/)** to process ICMP pings (**icmpping**, **icmppingloss**, **icmppingsec**).</source>
        <target state="needs-translation">#### Ping ICMP

O Zabbix usa o utilitário externo **fping** para processar os pings
ICMP.

O utilitário não faz parte da distribuição do Zabbix e precisa ser
instalado individualmente. Se ele estiver ausente, com permissões insuficientes ou
sua localização for diferente do valor definido no arquivo de configuração do
Zabbix Server (parâmetro 'FpingLocation'), os itens de ICMP
(**icmpping**, **icmppingloss**, **icmppingsec**) não serão processados.

Veja também: [problemas conhecidos](/manual/installation/known_issues#simple_checks)

O **fping** precisa ser executável pelos daemons do Zabbix rodando com
'setuid' do root. Execute estes comandos como **root** para configurar corretamente
as permissões:

    shell&gt; chown root:zabbix /usr/sbin/fping
    shell&gt; chmod 4710 /usr/sbin/fping

Após executar os dois comandos acima, verifique o proprietário do executável
**fping**. Em alguns casos o proprietário pode ser reconfigurado executando
o comando chmod.

Verifique também se o usuário zabbix pertence ao grupo zabbix executando:

    shell&gt; groups zabbix

e se não pertencer, adicione-o da seguinte forma:

    shell&gt; usermod -a -G zabbix zabbix

Padrões, limites e descrições dos valores para os parâmetros de verificações ICMP:

|Parâmetro|Unidade|Descrição|Fping's flag|Padrões definidos por|&lt;|Limites permitidos&lt;br&gt;pelo Zabbix|&lt;|
|---------|----|-----------|------------|--------- ------|-|---------------------------|-|
|||||**fping**|**Zabbix**|**min**|**max**|
|pacotes|number|número de pacotes de solicitação para um destino|-C||3|1|10000|
|intervalo|milissegundos|tempo de espera entre pacotes sucessivos|-p|1000||20|ilimitado|
|tamanho|bytes|tamanho do pacote em bytes&lt;br&gt;56 bytes em x86, 68 bytes em x86_64|-b|56 ou 68||24|65507|
|tempo limite|milissegundos|**fping v3.x** - tempo limite de espera após o último pacote enviado, afetado pelo sinalizador *-C*&lt;br&gt; **fping v4.x** - tempo limite individual para cada pacote|-t| **fping v3.x** - 500&lt;br&gt;**fping v4.x** - herdado do sinalizador *-p*, mas não superior a 2000||50|ilimitado|

Além disso, o Zabbix usa as opções de fping *-i interval ms* (não confunda
com o parâmetro de item *intervalo* mencionado na tabela acima, que
corresponde à opção de fping *-p*) e endereço IP de origem *-S* (ou *-I* em
versões mais antigas de fping). Essas opções são detectadas automaticamente executando verificações
com diferentes combinações de opções. O Zabbix tenta detectar o valor mínimo
em milissegundos que o fping permite usar com *-i* tentando 3
valores: 0, 1 e 10. O primeiro valor bem-sucedido é então usado para
verificações ICMP subsequentes. Esta ação é executada por cada [processo de ping ICMP](/manual/concepts/server#server_process_types) individualmente.

As opções de fping detectadas automaticamente são invalidadas a cada hora e detectadas
novamente na próxima tentativa de realizar a verificação ICMP. Defina DebugLevel&gt;=4 
para visualizar os detalhes desse processo no arquivo de log do Zabbix Server ou Proxy.

::: notewarning
Aviso: os padrões de fping podem diferir dependendo da
plataforma e versão - em caso de dúvida, verifique a documentação
do fping.
:::

O Zabbix grava os endereços IP a serem verificados por qualquer uma das três chaves *icmpping\**
em um arquivo temporário, que é então passado para o **fping**. Se os itens
têm parâmetros de chave diferentes, apenas aqueles com parâmetros de chave idênticos
são gravados em um único arquivo.\
Todos os endereços IP gravados no arquivo único serão verificados pelo fping em
paralelo, então o processo de ping icmp do Zabbix levará uma quantidade fixa de tempo,
desconsiderando o número de endereços IP no arquivo.</target>
      </trans-unit>
      <trans-unit id="e3dd9826" xml:space="preserve">
        <source>
##### Installation

fping is not included with Zabbix and needs to be installed separately:

- Various Unix-based platforms have the fping package in their default repositories, but it is not pre-installed. In this case you can use the package manager to install fping.

- Zabbix provides [fping packages](http://repo.zabbix.com/non-supported/rhel/) for RHEL. Please note that these packages are provided without official support.

- fping can also be compiled [from source](https://github.com/schweikert/fping/blob/develop/README.md#installation).</source>
      </trans-unit>
      <trans-unit id="9ced6345" xml:space="preserve">
        <source>
##### Configuration

Specify fping location in the *[FpingLocation](/manual/appendix/config/zabbix_server#fpinglocation)* parameter of Zabbix server/proxy configuration file 
(or *[Fping6Location](/manual/appendix/config/zabbix_server#fping6location)* parameter for using IPv6 addresses).

fping should be executable by the user Zabbix server/proxy run as and this user should have sufficient rights.

See also: [Known issues](/manual/installation/known_issues#simple_checks) for processing simple checks with fping versions below 3.10.</source>
      </trans-unit>
      <trans-unit id="3c21487d" xml:space="preserve">
        <source>
##### Default values

Defaults, limits and description of values for ICMP check parameters:

|Parameter|Unit|Description|Fping's flag|Defaults set by|&lt;|Allowed limits&lt;br&gt;by Zabbix|&lt;|
|--|--|--------|-|--|--|--|--|
|||||**fping**|**Zabbix**|**min**|**max**|
|packets|number|number of request packets sent to a target|-C||3|1|10000|
|interval|milliseconds|time to wait between successive packets to an individual target|-p|1000||20|unlimited|
|size|bytes|packet size in bytes&lt;br&gt;56 bytes on x86, 68 bytes on x86_64|-b|56 or 68||24|65507|
|timeout|milliseconds|**fping v3.x** - timeout to wait after last packet sent, affected by *-C* flag&lt;br&gt; **fping v4.x** - individual timeout for each packet|-t|**fping v3.x** - 500&lt;br&gt;**fping v4.x** and newer - inherited from *-p* flag, but not more than 2000||50|unlimited|

The defaults may differ slightly depending on the platform and version.

In addition, Zabbix uses fping options *-i interval ms* (do not mix up with the item parameter *interval* mentioned in the table above,
which corresponds to fping option *-p*) and *-S source IP address* (or *-I* in older fping versions).
These options are auto-detected by running checks with different option combinations.
Zabbix tries to detect the minimal value in milliseconds that fping allows to use with *-i* by trying 3 values: 0, 1 and 10.
The value that first succeeds is then used for subsequent ICMP checks.
This process is done by each [ICMP pinger](/manual/concepts/server#server_process_types) process individually.

Auto-detected fping options are invalidated every hour and detected again on the next attempt to perform ICMP check.
Set [DebugLevel](/manual/appendix/config/zabbix_server#debuglevel)&gt;=4 in order to view details of this process in the server or proxy log file.

Zabbix writes IP addresses to be checked by any of the three *icmpping\** keys to a temporary file, which is then passed to fping.
If items have different key parameters, only the ones with identical key parameters are written to a single file.
All IP addresses written to the single file will be checked by fping in parallel,
so Zabbix ICMP pinger process will spend fixed amount of time disregarding the number of IP addresses in the file.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
