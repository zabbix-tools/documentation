[comment]: # translation:outdated

[comment]: # ({b8a76c0a-5479dd79})
# 1 Chaves de item de monitoramento VMware

[comment]: # ({/b8a76c0a-5479dd79})

[comment]: # ({9fe7d501-98337d09})
#### Chaves de item

A tabela fornece detalhes sobre verificações simples que podem ser utilizadas
para monitoramento de [ambientes VMware](/manual/vm_monitoring).

|Chave|<|<|<|<|
|---|-|-|-|-|
|<|Descrição|Valor de retorno|Parâmetros|Comentários|
|vmware.cl.perfcounter\[<url>,<id>,<path>,<instance>\]|<|<|<|<|
|<|Métricas de contador de performance para cluster VMware.|Inteiro|**url** - URL de serviço VMware<br>**id** - ID do cluster VMware<br>**path** - caminho do contador de performance<br>**instance** - instância de contador de performance|O `id` pode ser recebido do vmware.cluster.discovery\[\] como {\#CLUSTER.ID}|
|vmware.cluster.discovery\[<url>\]|<|<|<|<|
|<|Descoberta de clusters VMware.|Objeto JSON|**url** - URL de serviço VMware|<|
|vmware.cluster.status\[<url>, <name>\]|<|<|<|<|
|<|estado do cluster VMwre.|Inteiro:<br>0 - cinza;<br>1 - verde;<br>2 - amarelo;<br>3 - vermelho|**url** - URL de serviço VMware<br>**name** - nome de cluster VMwre|<|
|vmware.datastore.discovery\[<url>\]|<|<|<|<|
|<|Descoberta de datastores VMware.|Objeto JSON|**url** - URL de serviço VMware|<|
|vmware.datastore.hv.list\[<url>,<datastore>\]|<|<|<|<|
|<|Lista de datastore hypervisors.|Objeto JSON|**url** - URL de serviço VMware<br>**datastore** - nome do datastore|<|
|vmware.datastore.read\[<url>,<datastore>,<mode>\]|<|<|<|<|
|<|Quantidade de tempo para uma operação de leitura a partir do datastore (milissegundos).|Inteiro ^**[2](vmware_keys#footnotes)**^|**url** - URL de serviço VMware<br>**datastore** - nome do datastore<br>**mode** - latência (valor médio, padrão), maxlatency (valor máximo)|<|
|vmware.datastore.size\[<url>,<datastore>,<mode>\]|<|<|<|<|
|<|Espaço em bytes ou em porcentagem do total do datastore VMware.|Inteiro - para bytes<br>Número flutuante - para porcentagem|**url** - URL de serviço VMware<br>**datastore** - nome do datastore<br>**mode** - valores possíveis:<br>total (padrão), free, pfree (livre, porcentagem), uncommitted|<|
|vmware.datastore.write\[<url>,<datastore>,<mode>\]|<|<|<|<|
|<|Quantidade de tempo para uma operação de leitura no datastore (milissegundos).|Inteiro ^**[2](vmware_keys#footnotes)**^|**url** - URL de serviço VMware<br>**datastore** - nome do datastore<br>**mode** - latência (valore médio, padrão), maxlatency (valor máximo)|<|
|vmware.dc.discovery\[<url>\]|<|<|<|<|
|<|Descoberta de datacenters VMware.|Objeto JSON|**url** - URL de serviço VMware|<|
|vmware.eventlog\[<url>,<mode>\]|<|<|<|<|
|<|Log de evento VMware.|Log|**url** - URL de serviço VMware<br>**mode** - *all* (padrão), *skip* - pula o processamento de dados antigos|Deve haver apenas uma chave de item vmware.eventlog\[\] por URL.<br><br>Veja também: [exemplo de filtragem](/manual/config/items/preprocessing/examples#filtering_vmware_event_log_records) de registros de log VMware.|
|vmware.fullname\[<url>\]|<|<|<|<|
|<|Nome completo do serviço VMware.|String|**url** - URL de serviço VMware|<|
|vmware.hv.cluster.name\[<url>,<uuid>\]|<|<|<|<|
|<|Nome do cluster do hypervisor VMware.|String|**url** - URL de serviço VMware<br>**uuid** - nome de host do hypervisor VMware|<|
|vmware.hv.cpu.usage\[<url>,<uuid>\]|<|<|<|<|
|<|Uso de processador pelo hypervisor VMware (Hz).|Inteiro|**url** - URL de serviço VMware<br>**uuid** - nome de host do hypervisor VMware|<|
|vmware.hv.cpu.usage.perf\[<url>,<uuid>\]|<|<|<|<|
|<|Uso de processador pelo hypervisor VMware como uma porcentagem durante o intervalo.|Número flutuante|**url** - URL de serviço VMware<br>**uuid** - nome de host do hypervisor VMware|<|
|vmware.hv.cpu.utilization\[<url>,<uuid>\]|<|<|<|<|
|<|Uso de processador pelo hypervisor VMware como uma porcentagem durante o intervalo, dependente do gerenciamento de energia ou HT.|Número flutuante|**url** - URL de serviço VMware<br>**uuid** - nome de host do hypervisor VMware|<|
|vmware.hv.datacenter.name\[<url>,<uuid>\]|<|<|<|<|
|<|Nome do datancenter do hypervisor VMware.|String|**url** - URL de serviço VMware<br>**uuid** - nome de host do hypervisor VMware|<|
|vmware.hv.datastore.discovery\[<url>,<uuid>\]|<|<|<|<|
|<|Descoberta de datastores de hypervisor VMware.|Objeto JSON|**url** - URL de serviço VMware<br>**uuid** - nome de host do hypervisor VMware|<|
|vmware.hv.datastore.list\[<url>,<uuid>\]|<|<|<|<|
|<|Lista de datastores de hypervisor VMware.|Objeto JSON|**url** - URL de serviço VMware<br>**uuid** - nome de host do hypervisor VMware|<|
|vmware.hv.datastore.multipath\[<url>,<uuid>,<datastore>,<partitionid>\]|<|<|<|<|
|<|Número de caminhos de datastores disponíveis.|Inteiro|**url** - URL de serviço VMware<br>**uuid** - nome de host do hypervisor VMware<br>**datastore** - nome do datastore<br>**partitionid** - ID interno do dispositibo físico do vmware.hv.datastore.discovery|<|
|vmware.hv.datastore.read\[<url>,<uuid>,<datastore>,<mode>\]|<|<|<|<|
|<|Quantidade de tempo médio para uma operação de leitura a partir do datastore (milissegundos).|Inteiro ^**[2](vmware_keys#footnotes)**^|**url** - URL de serviço VMware<br>**uuid** - nome de host do hypervisor VMware<br>**datastore** - nome do datastore<br>**mode** - latência (padrão)|<|
|vmware.hv.datastore.size\[<url>,<uuid>,<datastore>,<mode>\]|<|<|<|<|
|<|Espaço do datastore VMware em bytes ou em porcentagem do total.|Inteiro - para bytes<br>Número flutuante - para porcentagem|**url** - URL de serviço VMware<br>**uuid** - nome de host do hypervisor VMware<br>**datastore** - nome do datastore<br>**mode** - valores possíveis:<br>total (padrão), free, pfree (livre, porcentagem), uncommitted|<|
|vmware.hv.datastore.write\[<url>,<uuid>,<datastore>,<mode>\]|<|<|<|<|
|<|Quantidade de tempo médio para uma operação de escrita para o datastore (milissegundos).|Inteiro ^**[2](vmware_keys#footnotes)**^|**url** - URL de serviço VMware<br>**uuid** - nome de host do hypervisor VMware<br>**datastore** - nome do datastore<br>**mode** - latência (padrão)|<|
|vmware.hv.discovery\[<url>\]|<|<|<|<|
|<|Descoberta de hypervisors VMware.|Objeto JSON|**url** - URL de serviço VMware|<|
|vmware.hv.fullname\[<url>,<uuid>\]|<|<|<|<|
|<|Nome do hypervisor VMware.|String|**url** - URL de serviço VMware<br>**uuid** - nome de host do hypervisor VMware|<|
|vmware.hv.hw.cpu.freq\[<url>,<uuid>\]|<|<|<|<|
|<|Frequência de processador do hypervisor VMware (Hz).|Inteiro|**url** - URL de serviço VMware<br>**uuid** - nome de host do hypervisor VMware|<|
|vmware.hv.hw.cpu.model\[<url>,<uuid>\]|<|<|<|<|
|<|Modelo de processador do hypervisor VMware.|String|**url** - URL de serviço VMware<br>**uuid** - nome de host do hypervisor VMware|<|
|vmware.hv.hw.cpu.num\[<url>,<uuid>\]|<|<|<|<|
|<|Número de cores de processador no hypervisor VMware.|Inteiro|**url** - URL de serviço VMware<br>**uuid** - nome de host do hypervisor VMware|<|
|vmware.hv.hw.cpu.threads\[<url>,<uuid>\]|<|<|<|<|
|<|Número de threads de processador no hypervisor VMware.|Inteiro|**url** - URL de serviço VMware<br>**uuid** - nome de host do hypervisor VMware|<|
|vmware.hv.hw.memory\[<url>,<uuid>\]|<|<|<|<|
|<|Tamanho de memória total do VMware (bytes).|Inteiro|**url** - URL de serviço VMware<br>**uuid** - nome de host do hypervisor VMware|<|
|vmware.hv.hw.model\[<url>,<uuid>\]|<|<|<|<|
|<|Modelo de hypervisor VMware.|String|**url** - URL de serviço VMware<br>**uuid** - nome de host do hypervisor VMware|<|
|vmware.hv.hw.uuid\[<url>,<uuid>\]|<|<|<|<|
|<|UUID de BIOS do hypervisor VMware.|String|**url** - URL de serviço VMware<br>**uuid** - nome de host do hypervisor VMware|<|
|vmware.hv.hw.vendor\[<url>,<uuid>\]|<|<|<|<|
|<|Nome do fornecedor do hypervisor VMware.|String|**url** - URL de serviço VMware<br>**uuid** - nome de host do hypervisor VMware|<|
|vmware.hv.maintenance\[<url>,<uuid>\]|<|<|<|<|
|<|Estado de manutenção do hypervisor VMware.|Inteiro|**url** - URL de serviço VMware<br>**uuid** - nome de host do hypervisor VMware|Retorna '0' - não em manutenção ou '1' - em manutenção|
|vmware.hv.memory.size.ballooned\[<url>,<uuid>\]|<|<|<|<|
|<|Tamanho de memória inflada do hypervisor VMware (bytes).|Inteiro|**url** - URL de serviço VMware<br>**uuid** - nome de host do hypervisor VMware|<|
|vmware.hv.memory.used\[<url>,<uuid>\]|<|<|<|<|
|<|Tamanho de memória usada pelo hypervisor VMware (bytes).|Inteiro|**url** - URL de serviço VMware<br>**uuid** - nome de host do hypervisor VMware|<|
|vmware.hv.network.in\[<url>,<uuid>,<mode>\]|<|<|<|<|
|<|Estatísticas de entrada de rede do hypervisor VMware (bytes por segundo).|Inteiro ^**[2](vmware_keys#footnotes)**^|**url** - URL de serviço VMware<br>**uuid** - nome de host do hypervisor VMware<br>**mode** - bps (padrão)|<|
|vmware.hv.network.out\[<url>,<uuid>,<mode>\]|<|<|<|<|
|<|Estatísticas de saída de rede do hypervisor VMware (bytes por segundo).|Inteiro ^**[2](vmware_keys#footnotes)**^|**url** - URL de serviço VMware<br>**uuid** - nome de host do hypervisor VMware<br>**mode** - bps (padrão)|<|
|vmware.hv.perfcounter\[<url>,<uuid>,<path>,<instance>\]|<|<|<|<|
|<|Valor de contador de performance do hypervisor VMware.|Inteiro ^**[2](vmware_keys#footnotes)**^|**url** - URL de serviço VMware<br>**uuid** - nome de host do hypervisor VMware<br>**path** - caminho do contador de performance ^**[1](vmware_keys#footnotes)**^<br>**instance** - instância do contador de performance. Use instância vazia para agragar valores (padrão)|<|
|vmware.hv.power\[<url>,<uuid>,<max>\]|<|<|<|<|
|<|Uso de energia por hypervisor VMware (W).|Inteiro|**url** - URL de serviço VMware<br>**uuid** - nome de host do hypervisor VMware<br>**max** - máximo uso de energia permitido|<|
|vmware.hv.sensor.health.state\[<url>,<uuid>\]|<|<|<|<|
|<|Sensor de estado de integridade do hypervisor VMware.|Inteiro:<br>0 - cinza;<br>1 - verde;<br>2 - amarelo;<br>3 - vermelho|**url** - URL de serviço VMware<br>**uuid** - nome de host do hypervisor VMware|<|
|vmware.hv.sensors.get\[<url>,<uuid>\]|<|<|<|<|
|<|Sensores de estado de HW de fornecedor do hypervisor VMware.|JSON|**url** - URL de serviço VMware<br>**uuid** - nome de host do hypervisor VMware|<|
|vmware.hv.status\[<url>,<uuid>\]|<|<|<|<|
|<|Estado do hypervisor VMware.|Inteiro:<br>0 - cinza;<br>1 - verde;<br>2 - amarelo;<br>3 - vermelho|**url** - URL de serviço VMware<br>**uuid** - nome de host do hypervisor VMware|Usa a propriedade de estado geral do sistema do host.|
|vmware.hv.uptime\[<url>,<uuid>\]|<|<|<|<|
|<|Tempo ativo do  hypervisor VMware (segundos).|Inteiro|**url** - URL de serviço VMware<br>**uuid** - nome de host do hypervisor VMware|<|
|vmware.hv.version\[<url>,<uuid>\]|<|<|<|<|
|<|Versão do hypervisor VMware.|String|**url** - URL de serviço VMware<br>**uuid** - nome de host do hypervisor VMware|<|
|vmware.hv.vm.num\[<url>,<uuid>\]|<|<|<|<|
|<|Número de máquinas virtuais no hypervisor VMware.|Inteiro|**url** - URL de serviço VMware<br>**uuid** - nome de host do hypervisor VMware|<|
|vmware.version\[<url>\]|<|<|<|<|
|<|Versão do serviço VMware.|String|**url** - URL de serviço VMware|<|
|vmware.vm.cluster.name\[<url>,<uuid>\]|<|<|<|<|
|<|Nome da máquina virtual VMware.|String|**url** - URL de serviço VMware<br>**uuid** - nome de host de máquina virtual VMware|<|
|vmware.vm.cpu.latency\[<url>,<uuid>\]|<|<|<|<|
|<|Porcentagem de tempo em que a máquina virtual é incapaz de ser executada porque está disputando acesso a CPU física.|Número flutuante|**url** - URL de serviço VMware<br>**uuid** - nome de host da máquina virtual no VMware|<|
|vmware.vm.cpu.num\[<url>,<uuid>\]|<|<|<|<|
|<|Número de processadores nas máquinas virtuais no VMware.|Inteiro|**url** - URL de serviço VMware<br>**uuid** - nome de máquina virtual do VMware|<|
|vmware.vm.cpu.readiness\[<url>,<uuid>,<instance>\]|<|<|<|<|
|<|Porcentagem de tempo que a máquina virtual está pronta, mas não consegue agendar execução na CPU física.|Número flutuante|**url** - URL de serviço VMware<br>**uuid** - nome de host de máquina virtual VMware<br>**instance** - instância de CPU|<|
|vmware.vm.cpu.ready\[<url>,<uuid>\]|<|<|<|<|
|<|Tempo (em milisegundos) que a máquina virtual está pronta, mas não consegue agendar execução na CPU física. O tempo de CPU ready é dependente do número de máquinas virtuais no host e suas cargas de CPU (%).|Inteiro ^**[2](vmware_keys#footnotes)**^|**url** - URL de serviço VMware<br>**uuid** - nome de host de maquina virtual VMware|<|
|vmware.vm.cpu.swapwait\[<url>,<uuid>,<instance>\]|<|<|<|<|
|<|Porcentagem de tempo de CPU gasto aguardando por swap-in.|Número flutuante|**url** - URL de serviço VMware<br>**uuid** - nome de host de máquina virtual VMware<br>**instance** - instância de CPU|<|
|vmware.vm.cpu.usage\[<url>,<uuid>\]|<|<|<|<|
|<|Uso de processador por máquia virtual VMware (Hz).|Inteiro|**url** - URL de serviço VMware<br>**uuid** - nome de host de máquina virtual VMware|<|
|vmware.vm.cpu.usage.perf\[<url>,<uuid>\]|<|<|<|<|
|<|Uso de processador por máquina virtual VMware como uma porcentagem durante o intervalo.|Número flutuante|**url** - URL de serviço VMware<br>**uuid** - nome de host de máquina virtual VMware|<|
|vmware.vm.datacenter.name\[<url>,<uuid>\]|<|<|<|<|
|<|Nome do datacenter de máquinas virtuais VMware.|String|**url** - URL de serviço VMware<br>**uuid** - nome de host de máquina virtual VMware|<|
|vmware.vm.discovery\[<url>\]|<|<|<|<|
|<|Descoberta de máquinas virtuais VMware.|Objeto JSON|**url** - URL de serviço VMware|<|
|vmware.vm.guest.memory.size.swapped\[<url>,<uuid>\]|<|<|<|<|
|<|Quantidade de memória física de convidado que é enviada como swap para o espaço de swap (KB).|Inteiro|**url** - URL de serviço VMware<br>**uuid** - nome de host de máquina virtual VMware|<|
|vmware.vm.guest.osuptime\[<url>,<uuid>\]|<|<|<|<|
|<|Tempo total discorrido desde a última inicialização de sistema operacional (em segundos).|Inteiro|**url** - URL de serviço VMware<br>**uuid** - nome de host de máquina virtual VMware|<|
|vmware.vm.hv.name\[<url>,<uuid>\]|<|<|<|<|
|<|Nome de hypervisor de máquina virtual VMware.|String|**url** - URL de serviço VMware<br>**uuid** - nome de host de máquina virtual VMware|<|
|vmware.vm.memory.size\[<url>,<uuid>\]|<|<|<|<|
|<|Tamnho de memória total de máquina virtual VMware (bytes).|Inteiro|**url** - URL de serviço VMware<br>**uuid** - nome de host de máquina virtual VMware|<|
|vmware.vm.memory.size.ballooned\[<url>,<uuid>\]|<|<|<|<|
|<|Tamanho de memória inflada (balloon) da máquina virtual VMware (bytes).|Inteiro|**url** - URL de serviço VMware<br>**uuid** - nome de host de máquina virtual VMware|<|
|vmware.vm.memory.size.compressed\[<url>,<uuid>\]|<|<|<|<|
|<|Tamanho de memória comprimida da máquina virtual VMware (bytes).|Inteiro|**url** - URL de serviço VMware<br>**uuid** - nome de host de máquina virtual VMware|<|
|vmware.vm.memory.size.consumed\[<url>,<uuid>\]|<|<|<|<|
|<|Quantidade de memória física do host consumida para fazer backup das páginas de memória física de convidado (KB).|Inteiro|**url** - URL de serviço VMware<br>**uuid** - nome de host de máquina virtual VMware|<|
|vmware.vm.memory.size.private\[<url>,<uuid>\]|<|<|<|<|
|<|Tamanho de memória privada da máquina virtual VMware (bytes).|Inteiro|**url** - URL de serviço VMware<br>**uuid** - nome de host de máquina virtual VMware|<|
|vmware.vm.memory.size.shared\[<url>,<uuid>\]|<|<|<|<|
|<|Tamanho de memória compartilhada da máquina virtual (bytes).|Inteiro|**url** - URL de serviço VMware<br>**uuid** - nome de host de máquina virtual VMware|<|
|vmware.vm.memory.size.swapped\[<url>,<uuid>\]|<|<|<|<|
|<|Tamanho de memória em swap da máquina virtual VMware (bytes).|Inteiro|**url** - URL de serviço VMware<br>**uuid** - nome de host de máquina virtual VMware|<|
|vmware.vm.memory.size.usage.guest\[<url>,<uuid>\]|<|<|<|<|
|<|Uso de memória por máquina virtual VMware convidada (bytes).|Inteiro|**url** - URL de serviço VMware<br>**uuid** - nome de host de máquina virtual VMware|<|
|vmware.vm.memory.size.usage.host\[<url>,<uuid>\]|<|<|<|<|
|<|Uso de memória do host por máquina virtual VMware (bytes).|Inteiro|**url** - URL de serviço VMware<br>**uuid** - nome de host de máquina virtual VMware|<|
|vmware.vm.memory.usage\[<url>,<uuid>\]|<|<|<|<|
|<|Porcentagem de memória física do host que foi consumida.|Número flutuante|**url** - URL de serviço VMware<br>**uuid** - nome de host de máquina virtual VMware|<|
|vmware.vm.net.if.discovery\[<url>,<uuid>\]|<|<|<|<|
|<|Descoberta de interfaces de rede de máquina virtual VMware.|Objeto JSON|**url** - URL de serviço VMware<br>**uuid** - nome de host de máquina virtual VMware|<|
|vmware.vm.net.if.in\[<url>,<uuid>,<instance>,<mode>\]|<|<|<|<|
|<|Estatísticas de entrada de interface de rede da máquina virtual (bytes/pacotes por segundo).|Inteiro ^**[2](vmware_keys#footnotes)**^|**url** - URL de serviço VMware<br>**uuid** - nome de host de máquina virtual VMware<br>**instance** - instância de interface de rede<br>**mode** - bps (padrão)/pps - bytes/pacotes por segundo|<|
|vmware.vm.net.if.out\[<url>,<uuid>,<instance>,<mode>\]|<|<|<|<|
|<|Estatísticas de saída de interface de rede da máquina virtual VMware (bytes/pacotes por segundo).|Inteiro ^**[2](vmware_keys#footnotes)**^|**url** - URL de serviço VMware<br>**uuid** - nome de host de máquina virtual VMware<br>**instance** - intência de interface de rede<br>**mode** - bps (padrão)/pps - bytes/pacotes por segundo|<|
|vmware.vm.net.if.usage\[<url>,<uuid>,<instance>\]|<|<|<|<|
|<|Utilização de rede da máquina virtual VMware (combinadas taxa de transmissão e taxa de recepção (transmit-rates e receive-rates)) durante o intervalo (KBps).|Inteiro|**url** - URL de serviço VMware<br>**uuid** - nome de host de máquina virtual VMware<br>**instance** - instÂncia de interface de rede|<|
|vmware.vm.perfcounter\[<url>,<uuid>,<path>,<instance>\]|<|<|<|<|
|<|Valor de contador de performance da máquina virtual VMware.|Inteiro ^**[2](vmware_keys#footnotes)**^|**url** - URL de serviço VMware<br>**uuid** - nome de host de máquina virtual VMware<br>**path** - caminho do contador de performance ^**[1](vmware_keys#footnotes)**^<br>**instance** - instância do contador de performance. Use instância vazia para agregar valores (padrão)|<|
|vmware.vm.powerstate\[<url>,<uuid>\]|<|<|<|<|
|<|Estado de energia da máquina virtual VMware.|Inteiro:<br>0 - desligada;<br>1 - ligada;<br>2 - suspesa|**url** - URL de serviço VMware<br>**uuid** - nome de host de máquina virtual VMware|<|
|vmware.vm.storage.committed\[<url>,<uuid>\]|<|<|<|<|
|<|Espaço de armazenamento entregue à máquina virtual VMware (bytes).|Inteiro|**url** - URL de serviço VMware<br>**uuid** - nome de host de máquina virtual VMware|<|
|vmware.vm.storage.readoio\[<url>,<uuid>,<instance>\]|<|<|<|<|
|<|Número médio de solicitações de leitura pendentes para o disco virtual durante o intervalo de coleta.|Inteiro|**url** - URL de serviço VMware<br>**uuid** - nome de host de máquina virtual VMware<br>**instance** - instância de dispositivo de disco (obrigatório)|<|
|vmware.vm.storage.totalreadlatency\[<url>,<uuid>,<instance>\]|<|<|<|<|
|<|O tempo médio que uma leitura a partir dos discos virtuais leva (milissegundos).|Inteiro|**url** - URL de serviço VMware<br>**uuid** - nome de host de máquina virtual VMware<br>**instance** - instância de dispositivo de disco (obrigatório)|<|
|vmware.vm.storage.totalwritelatency\[<url>,<uuid>,<instance>\]|<|<|<|<|
|<|O tempo médio que uma escrita nos discos virtuais leva (milissegundos).|Inteiro|**url** - URL de serviço VMware<br>**uuid** - nome de host de máquina virtual VMware<br>**instance** - instância de dispositivo de disco (obrigatório)|<|
|vmware.vm.storage.uncommitted\[<url>,<uuid>\]|<|<|<|<|
|<|Espaço de armazenamento revogado da máquina virtual VMware (bytes).|Inteiro|**url** - URL de serviço VMware<br>**uuid** - nome de host de máquina virtual VMware|<|
|vmware.vm.storage.unshared\[<url>,<uuid>\]|<|<|<|<|
|<|Espaço de armazenamento descompartilhado na máquina virtual VMware (bytes).|Inteiro|**url** - URL de serviço VMware<br>**uuid** - nome de host de máquina virtual VMware|<|
|vmware.vm.storage.writeoio\[<url>,<uuid>,<instance>\]|<|<|<|<|
|<|Número médio de solicitações de escrita pendente para o disco virtual durante o intervalo de coleta.|Inteiro|**url** - URL de serviço VMware<br>**uuid** - nome de host de máquina virtual VMware<br>**instance** - instância de dispositivo de disco (obrigatório)|<|
|vmware.vm.uptime\[<url>,<uuid>\]|<|<|<|<|
|<|Tempo ativo da máquina virtual VMware (segundos).|Inteiro|**url** - URL de serviço VMware<br>**uuid** - nome de host de máquina virtual VMware|<|
|vmware.vm.vfs.dev.discovery\[<url>,<uuid>\]|<|<|<|<|
|<|Descoberta de dispositivos de disco de máquina virtual VMware.|Objeto JSON|**url** - URL de serviço VMware<br>**uuid** - nome de host de máquina virtual VMware|<|
|vmware.vm.vfs.dev.read\[<url>,<uuid>,<instance>,<mode>\]|<|<|<|<|
|<|Estatísticas de leitura de dispositivos de disco da máquina virtual VMware (bytes/operações por segundo).|Inteiro ^**[2](vmware_keys#footnotes)**^|**url** - URL de serviço VMware<br>**uuid** - nome de host de máquina virtual VMware<br>**instance** - instância de dispositivo de disco<br>**mode** - bps (padrão)/ops - bytes/operações por segundo|<|
|vmware.vm.vfs.dev.write\[<url>,<uuid>,<instance>,<mode>\]|<|<|<|<|
|<|Estatísticas de escrita dos dispositivos de disco da máquina virtual VMware (bytes/operações por segundo).|Inteiro ^**[2](vmware_keys#footnotes)**^|**url** - URL de serviço VMware<br>**uuid** - nome de host de máquina virtual VMware<br>**instance** - instância de dispositivo de disco<br>**mode** - bps (padrão)/ops - bytes/operações por segundo|<|
|vmware.vm.vfs.fs.discovery\[<url>,<uuid>\]|<|<|<|<|
|<|Descoberta de sistemas de arquivo de máquina virtual VMware.|Objeto JSON|**url** - URL de serviço VMware<br>**uuid** - nome de host de máquina virtual VMware|O VMware Tools deve estar instalado nas máquinas virtuais (guest).|
|vmware.vm.vfs.fs.size\[<url>,<uuid>,<fsname>,<mode>\]|<|<|<|<|
|<|Estatísticas de sistema de arquivo da máquina virtual VMware (bytes/porcentagens).|Inteiro|**url** - URL de serviço VMware<br>**uuid** - nome de host de máquina virtual VMware<br>**fsname** - nome do sistema de arquivo<br>**mode** - total/free/used/pfree/pused|O VMware Tools deve estar instalado nas máquinas virtuais (guest).|

[comment]: # ({/9fe7d501-98337d09})




