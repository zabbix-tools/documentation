<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="pt-BR" datatype="plaintext" original="manual/config/items/itemtypes/odbc_checks.md">
    <body>
      <trans-unit id="68c88a1c" xml:space="preserve">
        <source># 14 ODBC monitoring</source>
      </trans-unit>
      <trans-unit id="b5170fd3" xml:space="preserve">
        <source>#### Overview

ODBC monitoring corresponds to the *Database monitor* item type in the
Zabbix frontend.

ODBC is a C programming language middle-ware API for accessing database
management systems (DBMS). The ODBC concept was developed by Microsoft
and later ported to other platforms.

Zabbix may query any database, which is supported by ODBC. To do that,
Zabbix does not directly connect to the databases, but uses the ODBC
interface and drivers set up in ODBC. This function allows for more
efficient monitoring of different databases for multiple purposes - for
example, checking specific database queues, usage statistics and so on.
Zabbix supports unixODBC, which is one of the most commonly used open
source ODBC API implementations.

::: noteimportant
See also the [known
issues](/manual/installation/known_issues#odbc_checks) for ODBC
checks.
:::</source>
      </trans-unit>
      <trans-unit id="bf6fd8ba" xml:space="preserve">
        <source>#### Installing unixODBC

The suggested way of installing unixODBC is to use the Linux operating
system default package repositories. In the most popular Linux
distributions unixODBC is included in the package repository by default.
If it's not available, it can be obtained at the unixODBC homepage:
&lt;http://www.unixodbc.org/download.html&gt;.

Installing unixODBC on RedHat/Fedora based systems using the *dnf*
package manager:

    dnf -y install unixODBC unixODBC-devel

Installing unixODBC on SUSE based systems using the *zypper* package
manager:

    zypper in unixODBC-devel

::: noteclassic
The unixODBC-devel package is needed to compile Zabbix with
unixODBC support.
:::</source>
      </trans-unit>
      <trans-unit id="eea09ed7" xml:space="preserve">
        <source>#### Installing unixODBC drivers

A unixODBC database driver should be installed for the database, which
will be monitored. unixODBC has a list of supported databases and
drivers: &lt;http://www.unixodbc.org/drivers.html&gt;. In some Linux
distributions database drivers are included in package repositories.
Installing MySQL database driver on RedHat/Fedora based systems using
the *dnf* package manager:

    dnf install mysql-connector-odbc

Installing MySQL database driver on SUSE based systems using the
*zypper* package manager:

    zypper in MyODBC-unixODBC</source>
      </trans-unit>
      <trans-unit id="b15e771b" xml:space="preserve">
        <source>#### Configuring unixODBC

ODBC configuration is done by editing the **odbcinst.ini** and
**odbc.ini** files. To verify the configuration file location, type:

    odbcinst -j

**odbcinst.ini** is used to list the installed ODBC database drivers:

    [mysql]
    Description = ODBC for MySQL
    Driver      = /usr/lib/libmyodbc5.so

Parameter details:

|Attribute|Description|
|--|--------|
|*mysql*|Database driver name.|
|*Description*|Database driver description.|
|*Driver*|Database driver library location.|

**odbc.ini** is used to define data sources:

    [test]
    Description = MySQL test database
    Driver      = mysql
    Server      = 127.0.0.1
    User        = root
    Password    =
    Port        = 3306
    Database    = zabbix

Parameter details:

|Attribute|Description|
|--|--------|
|*test*|Data source name (DSN).|
|*Description*|Data source description.|
|*Driver*|Database driver name - as specified in odbcinst.ini|
|*Server*|Database server IP/DNS.|
|*User*|Database user for connection.|
|*Password*|Database user password.|
|*Port*|Database connection port.|
|*Database*|Database name.|

To verify if ODBC connection is working successfully, a connection to
database should be tested. That can be done with the **isql** utility
(included in the unixODBC package):

    shell&gt; isql test
    +---------------------------------------+
    | Connected!                            |
    |                                       |
    | sql-statement                         |
    | help [tablename]                      |
    | quit                                  |
    |                                       |
    +---------------------------------------+
    SQL&gt;</source>
      </trans-unit>
      <trans-unit id="9f87c9b6" xml:space="preserve">
        <source>#### Compiling Zabbix with ODBC support

To enable ODBC support, Zabbix should be compiled with the following
flag:

      --with-unixodbc[=ARG]   use odbc driver against unixODBC package

::: noteclassic
See more about Zabbix installation from the [source
code](/manual/installation/install#from_the_sources).
:::</source>
      </trans-unit>
      <trans-unit id="ca784f43" xml:space="preserve">
        <source>#### Item configuration in Zabbix frontend

Configure a database monitoring
[item](/manual/config/items/item#overview).

![](../../../../../assets/en/manual/config/items/itemtypes/db_monitor.png){width="600"}

All mandatory input fields are marked with a red asterisk.

Specifically for database monitoring items you must enter:

|   |   |
|--|--------|
|*Type*|Select *Database monitor* here.|
|*Key*|Enter one of the supported item keys:&lt;br&gt;[db.odbc.select](#db.odbc.select)[] - this item is designed to return one value, i.e. the first column of the first row of the SQL query result;&lt;br&gt;[db.odbc.get](#db.odbc.get)[] - this item is capable of returning multiple rows/columns in JSON format;&lt;br&gt;[db.odbc.discovery](#db.odbc.discovery)[] - this item is designed for low-level discovery.|
|*User name*|Enter the database user name (up to 255 characters).&lt;br&gt;This parameter is optional if user is specified in odbc.ini.&lt;br&gt;If connection string is used, and *User name* field is not empty, it is appended to the connection string as `UID=&lt;user&gt;`.|
|*Password*|Enter the database user password (up to 255 characters).&lt;br&gt;This parameter is optional if password is specified in odbc.ini.&lt;br&gt;If connection string is used, and *Password* field is not empty, it is appended to the connection string as `PWD=&lt;password&gt;`.&lt;br&gt; If a password contains semicolon, it should be wrapped in curly brackets, for example: &lt;br&gt; *Password*: {P?;)\*word}  (if an actual password is `P?;)*word`) &lt;br&gt;&lt;br&gt; The password will be appended to connection string after the username as:&lt;br&gt;` UID=&lt;username&gt;;PWD={P?;)*word} `&lt;br&gt;&lt;br&gt; To test the resulting string, run: &lt;br&gt;` isql -v -k 'Driver=libmaodbc.so;Database=zabbix;UID=zabbix;PWD={P?;)*word}'`|
|*SQL query*|Enter the SQL query.&lt;br&gt;Note that with the `db.odbc.select[]` item the query must return one value only.|
|*Type of information*|It is important to know what type of information will be returned by the query, so that it is selected correctly here. With an incorrect *type of information* the item will turn unsupported.|</source>
      </trans-unit>
      <trans-unit id="0188aaa2" xml:space="preserve">
        <source>#### Item key details

Parameters without angle brackets are mandatory. Parameters marked with angle brackets **&lt;** **&gt;** are optional.</source>
      </trans-unit>
      <trans-unit id="13edfcd4" xml:space="preserve">
        <source>
##### db.odbc.select[&lt;unique short description&gt;,&lt;dsn&gt;,&lt;connection string&gt;] {#db.odbc.select}

&lt;br&gt;
Returns one value, i.e. the first column of the first row of the SQL query result.&lt;br&gt;
Return value: Depending on the SQL query.

Parameters:

-   **unique short description** - a unique short description to identify the item (for use in triggers, etc);
-   **dsn** - the data source name (as specified in odbc.ini);
-   **connection string** - the connection string (may contain driver-specific arguments).

Comments:

-   Although `dsn` and `connection string` are optional parameters, at least one of them must be present. If both data source name (DSN) and connection string are defined, the DSN will be ignored.
-   If a query returns more than one column, only the first column is read. If a query returns more than one line, only the first line is read.</source>
      </trans-unit>
      <trans-unit id="ebf5d449" xml:space="preserve">
        <source>
##### db.odbc.get[&lt;unique short description&gt;,&lt;dsn&gt;,&lt;connection string&gt;] {#db.odbc.get}

&lt;br&gt;
Transforms the SQL query result into a JSON array.&lt;br&gt;
Return value: *JSON object*.

Parameters:

-   **unique short description** - a unique short description to identify the item (for use in triggers, etc);
-   **dsn** - the data source name (as specified in odbc.ini);
-   **connection string** - the connection string (may contain driver-specific arguments).

Comments:

-   Although `dsn` and `connection string` are optional parameters, at least one of them must be present. If both data source name (DSN) and connection string are defined, the DSN will be ignored.
-   Multiple rows/columns in JSON format may be returned. This item may be used as a master item that collects all data in one system call, while JSONPath preprocessing may be used in dependent items to extract individual values. For more information, see an [example](/manual/discovery/low_level_discovery/examples/sql_queries#using-db.odbc.get) of the returned format, used in low-level discovery.

Example:

    db.odbc.get[MySQL example,,"Driver=/usr/local/lib/libmyodbc5a.so;Database=master;Server=127.0.0.1;Port=3306"] #connection for MySQL ODBC driver 5</source>
      </trans-unit>
      <trans-unit id="c8148c95" xml:space="preserve">
        <source>
##### db.odbc.discovery[&lt;unique short description&gt;,&lt;dsn&gt;,&lt;connection string&gt;] {#db.odbc.discovery}

&lt;br&gt;
Transforms the SQL query result into a JSON array, used for [low-level discovery](/manual/discovery/low_level_discovery/examples/sql_queries). 
The column names from the query result are turned into low-level discovery macro 
names paired with the discovered field values. These macros can be used in creating 
item, trigger, etc prototypes.&lt;br&gt;
Return value: *JSON object*.

Parameters:

-   **unique short description** - a unique short description to identify the item (for use in triggers, etc);
-   **dsn** - the data source name (as specified in odbc.ini);
-   **connection string** - the connection string (may contain driver-specific arguments).

Comments:

-   Although `dsn` and `connection string` are optional parameters, at least one of them must be present. If both data source name (DSN) and connection string are defined, the DSN will be ignored.</source>
      </trans-unit>
      <trans-unit id="06f9d2eb" xml:space="preserve">
        <source>#### Important notes

-   Database monitoring items will become unsupported if no *odbc poller* processes are started in the server or proxy configuration. To activate ODBC pollers, set *StartODBCPollers* parameter in Zabbix [server](/manual/appendix/config/zabbix_server) configuration file or, for checks performed by proxy, in Zabbix [proxy](/manual/appendix/config/zabbix_proxy) configuration file.
-   Zabbix does not limit the query execution time. It is up to the user
    to choose queries that can be executed in a reasonable amount of
    time.
-   The [Timeout](/manual/appendix/config/zabbix_server) parameter value
    from Zabbix server is used as the ODBC login timeout (note that
    depending on ODBC drivers the login timeout setting might be
    ignored).
-   The SQL command must return a result set like any query with
    `select ...`. The query syntax will depend on the RDBMS which will
    process them. The syntax of request to a storage procedure must be
    started with `call` keyword.</source>
      </trans-unit>
      <trans-unit id="718edfdc" xml:space="preserve">
        <source>#### Error messages

ODBC error messages are structured into fields to provide detailed
information. For example:

    Cannot execute ODBC query: [SQL_ERROR]:[42601][7][ERROR: syntax error at or near ";"; Error while executing the query]
    └───────────┬───────────┘  └────┬────┘ └──┬──┘└┬┘└─────────────────────────────┬─────────────────────────────────────┘
                │                   │         │    └─ Native error code            └─ Native error message
                │                   │         └─ SQLState
                └─ Zabbix message   └─ ODBC return code

Note that the error message length is limited to 2048 bytes, so the
message can be truncated. If there is more than one ODBC diagnostic
record Zabbix tries to concatenate them (separated with `|`) as far as
the length limit allows.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
