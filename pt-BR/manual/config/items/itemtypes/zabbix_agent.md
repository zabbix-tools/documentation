[comment]: # attributes: notoc

[comment]: # translation:outdated

[comment]: # ({f0b7e415-22d75789})
# 1 Zabbix Agent

[comment]: # ({/f0b7e415-22d75789})

[comment]: # ({new-d63c67a4})
#### Visão geral

Essas verificações usam a comunicação com o Zabbix Agent para coleta de dados.

Existem verificações [passivas e ativas](/manual/appendix/items/activepassive). 
Ao configurar um item, você pode selecionar o modelo:

- *Agente Zabbix* - para verificações passivas
- *Agente Zabbix (ativo)* - para verificações ativas

[comment]: # ({/new-d63c67a4})

[comment]: # ({new-cea3fc45})

### Supported platforms

Except where specified differently in the item comments, the agent items (and all parameters) are supported on: 

-   **Linux**
-   **FreeBSD**
-   **Solaris**
-   **HP-UX**
-   **AIX**
-   **Tru64**
-   **MacOS X**
-   **OpenBSD**
-   **NetBSD**

Many agent items are also supported on **Windows**. See the [Windows agent item](/manual/config/items/itemtypes/zabbix_agent/win_keys) 
page for details.

[comment]: # ({/new-cea3fc45})

[comment]: # ({new-3077d649})
### Item key details

Parameters without angle brackets are mandatory. Parameters marked with angle brackets **<** **>** are optional.

[comment]: # ({/new-3077d649})

[comment]: # ({new-20fc641c})
##### kernel.maxfiles

<br>
The maximum number of opened files supported by OS.<br>
Return value: *Integer*.<br>
[Supported platforms](#supported-platforms): Linux, FreeBSD, MacOS X, OpenBSD, NetBSD.

[comment]: # ({/new-20fc641c})

[comment]: # ({new-6dbdc04a})
##### kernel.maxproc

<br>
The maximum number of processes supported by OS.<br>
Return value: *Integer*.<br>
[Supported platforms](#supported-platforms): Linux 2.6 and later, FreeBSD, Solaris, MacOS X, OpenBSD, NetBSD.

[comment]: # ({/new-6dbdc04a})

[comment]: # ({new-9f31067e})
##### kernel.openfiles

<br>
The number of currently open file descriptors.<br>
Return value: *Integer*.<br>
[Supported platforms](#supported-platforms): Linux (the item may work on other UNIX-like platforms).

[comment]: # ({/new-9f31067e})

[comment]: # ({new-f0ef2c24})

##### log[file,<regexp>,<encoding>,<maxlines>,<mode>,<output>,<maxdelay>,<options>,<persistent_dir>]

<br>
The monitoring of a log file.<br>
Return value: *Log*.<br>
See [supported platforms](#supported-platforms).

Parameters:

-   **file** - the full path and name of a log file;<br>
-   **regexp** - a regular [expression](/manual/regular_expressions#overview) describing the required pattern;<br>
-   **encoding** - the code page [identifier](/manual/config/items/itemtypes/zabbix_agent#encoding_settings);<br>
-   **maxlines** - the maximum number of new lines per second the agent will send to Zabbix server or proxy. This parameter overrides the value of 'MaxLinesPerSecond' in [zabbix\_agentd.conf](/manual/appendix/config/zabbix_agentd);<br>
-   **mode** - possible values: *all* (default) or *skip* - skip processing of older data (affects only newly created items);<br>**output** - an optional output formatting template. The **\\0** escape sequence is replaced with the matched part of text (from the first character where match begins until the character where match ends) while an **\\N** (where N=1...9) escape sequence is replaced with Nth matched group (or an empty string if the N exceeds the number of captured groups).<br>
-   **maxdelay** - the maximum delay in seconds. Type: float. Values: 0 - (default) never ignore log file lines; > 0.0 - ignore older lines in order to get the most recent lines analyzed within "maxdelay" seconds. Read the [maxdelay](log_items#using_maxdelay_parameter) notes before using it!<br>
-   **options** - additional options:<br>*mtime-noreread* - non-unique records, reread only if the file size changes (ignore modification time change). (This parameter is deprecated since 5.0.2, because now mtime is ignored.)<br>
-   **persistent\_dir** (only in zabbix\_agentd on Unix systems; not supported in Zabbix agent 2) - the absolute pathname of directory where to store persistent files. See also additional notes on [persistent files](log_items#notes-on-persistent-files-for-log-items).

Comments:

-   The item must be configured as an [active check](/manual/appendix/items/activepassive#active_checks);
-   If the file is missing or permissions do not allow access, the item turns unsupported;
-   If `output` is left empty - the whole line containing the matched text is returned. Note that all global regular expression types except 'Result is TRUE' always return the whole matched line and the `output` parameter is ignored.
-   Content extraction using the `output` parameter takes place on the agent.

Examples:

    log[/var/log/syslog]
    log[/var/log/syslog,error]
    log[/home/zabbix/logs/logfile,,,100]
    
Example of using the `output` parameter for extracting a number from log record:

    log[/app1/app.log,"task run [0-9.]+ sec, processed ([0-9]+) records, [0-9]+ errors",,,,\1] #this item will match a log record "2015-11-13 10:08:26 task run 6.08 sec, processed 6080 records, 0 errors" and send only '6080' to server. Because a numeric value is being sent, the "Type of information" for this item can be set to "Numeric (unsigned)" and the value can be used in graphs, triggers etc.

Example of using the `output` parameter for rewriting a log record before sending to server:

    log[/app1/app.log,"([0-9 :-]+) task run ([0-9.]+) sec, processed ([0-9]+) records, ([0-9]+) errors",,,,"\1 RECORDS: \3, ERRORS: \4, DURATION: \2"] #this item will match a log record "2015-11-13 10:08:26 task run 6.08 sec, processed 6080 records, 0 errors" and send a modified record "2015-11-13 10:08:26 RECORDS: 6080, ERRORS: 0, DURATION: 6.08" to the server.

[comment]: # ({/new-f0ef2c24})

[comment]: # ({new-0cc87dbe})

##### log.count[file,<regexp>,<encoding>,<maxproclines>,<mode>,<maxdelay>,<options>,<persistent_dir>]

<br>
The count of matched lines in a monitored log file.<br>
Return value: *Integer*.<br>
See [supported platforms](#supported-platforms).

Parameters:

-   **file** - the full path and name of log file;<br>
-   **regexp** - a regular [expression](/manual/regular_expressions#overview) describing the required pattern;<br>
-   **encoding** - the code page [identifier](/manual/config/items/itemtypes/zabbix_agent#encoding_settings);<br>
-   **maxproclines** - the maximum number of new lines per second the agent will analyze (cannot exceed 10000). The default value is 10\*'MaxLinesPerSecond' in [zabbix\_agentd.conf](/manual/appendix/config/zabbix_agentd).<br>
-   **mode** - possible values: *all* (default) or *skip* - skip processing of older data (affects only newly created items).<br>
-   **maxdelay** - the maximum delay in seconds. Type: float. Values: 0 - (default) never ignore log file lines; > 0.0 - ignore older lines in order to get the most recent lines analyzed within "maxdelay" seconds. Read the [maxdelay](log_items#using_maxdelay_parameter) notes before using it!<br>
-   **options** - additional options:<br>*mtime-noreread* - non-unique records, reread only if the file size changes (ignore modification time change). (This parameter is deprecated since 5.0.2, because now mtime is ignored.)<br>
-   **persistent\_dir** (only in zabbix\_agentd on Unix systems; not supported in Zabbix agent 2) - the absolute pathname of directory where to store persistent files. See also additional notes on [persistent files](log_items#notes-on-persistent-files-for-log-items).

Comments:

-   The item must be configured as an [active check](/manual/appendix/items/activepassive#active_checks);
-   Matching lines are counted in the new lines since the last log check by the agent, and thus depend on the item update interval;
-   If the file is missing or permissions do not allow access, the item turns unsupported.

[comment]: # ({/new-0cc87dbe})

[comment]: # ({new-95734be0})
##### logrt[file_regexp,<regexp>,<encoding>,<maxlines>,<mode>,<output>,<maxdelay>,<options>,<persistent_dir>]

<br>
The monitoring of a log file that is rotated.<br>
Return value: *Log*.<br>
See [supported platforms](#supported-platforms).

Parameters:

-   **file\_regexp** - the absolute path to file and the file name described by a regular [expression](/manual/regular_expressions#overview). *Note* that only the file name is a regular expression.<br>
-   **regexp** - a regular [expression](/manual/regular_expressions#overview) describing the required content pattern;<br>
-   **encoding** - the code page [identifier](/manual/config/items/itemtypes/zabbix_agent#encoding_settings);<br>
-   **maxlines** - the maximum number of new lines per second the agent will send to Zabbix server or proxy. This parameter overrides the value of 'MaxLinesPerSecond' in [zabbix\_agentd.conf](/manual/appendix/config/zabbix_agentd).<br>
-   **mode** - possible values: *all* (default) or *skip* - skip processing of older data (affects only newly created items).<br>
-   **output** - an optional output formatting template. The **\\0** escape sequence is replaced with the matched part of text (from the first character where match begins until the character where match ends) while an **\\N** (where N=1...9) escape sequence is replaced with Nth matched group (or an empty string if the N exceeds the number of captured groups).<br>
-   **maxdelay** - the maximum delay in seconds. Type: float. Values: 0 - (default) never ignore log file lines; > 0.0 - ignore older lines in order to get the most recent lines analyzed within "maxdelay" seconds. Read the [maxdelay](log_items#using_maxdelay_parameter) notes before using it!<br>
-   **options** - the type of log file rotation and other options. Possible values:<br>*rotate* (default),<br>*copytruncate* - note that *copytruncate* cannot be used together with *maxdelay*. In this case *maxdelay* must be 0 or not specified; see [copytruncate](log_items#notes_on_handling_copytruncate_log_file_rotation) notes,<br>*mtime-reread* - non-unique records, reread if modification time or size changes (default),<br>*mtime-noreread* - non-unique records, reread only if the size changes (ignore modification time change).<br>
-   **persistent\_dir** (only in zabbix\_agentd on Unix systems; not supported in Zabbix agent 2) - the absolute pathname of directory where to store persistent files. See also additional notes on [persistent files](log_items#notes-on-persistent-files-for-log-items).

Comments:

-   The item must be configured as an [active check](/manual/appendix/items/activepassive#active_checks);
-   Log rotation is based on the last modification time of files;
-   Note that logrt is designed to work with one currently active log file, with several other matching inactive files rotated. If, for example, a directory has many active log files, a separate logrt item should be created for each one. Otherwise if one logrt item picks up too many files it may lead to exhausted memory and a crash of monitoring.
-   If `output` is left empty - the whole line containing the matched text is returned. Note that all global regular expression types except 'Result is TRUE' always return the whole matched line and the `output` parameter is ignored.
-   Content extraction using the `output` parameter takes place on the agent.

Examples:

    logrt["/home/zabbix/logs/^logfile[0-9]{1,3}$",,,100] #this item will match a file like "logfile1" (will not match ".logfile1")
    logrt["/home/user/^logfile_.*_[0-9]{1,3}$","pattern_to_match","UTF-8",100] #this item will collect data from files such "logfile_abc_1" or "logfile__001"
    
Example of using the `output` parameter for extracting a number from log record:

    logrt[/app1/^test.*log$,"task run [0-9.]+ sec, processed ([0-9]+) records, [0-9]+ errors",,,,\1] #this item will match a log record "2015-11-13 10:08:26 task run 6.08 sec, processed 6080 records, 0 errors" and send only '6080' to server. Because a numeric value is being sent, the "Type of information" for this item can be set to "Numeric (unsigned)" and the value can be used in graphs, triggers etc.
    
Example of using the `output` parameter for rewriting a log record before sending to server:

    logrt[/app1/^test.*log$,"([0-9 :-]+) task run ([0-9.]+) sec, processed ([0-9]+) records, ([0-9]+) errors",,,,"\1 RECORDS: \3, ERRORS: \4, DURATION: \2"] #this item will match a log record "2015-11-13 10:08:26 task run 6.08 sec, processed 6080 records, 0 errors" and send a modified record "2015-11-13 10:08:26 RECORDS: 6080, ERRORS: 0, DURATION: 6.08" to server. |

[comment]: # ({/new-95734be0})

[comment]: # ({new-8ad8a2af})

##### logrt.count[file_regexp,<regexp>,<encoding>,<maxproclines>,<mode>,<maxdelay>,<options>,<persistent_dir>]

<br>
The count of matched lines in a monitored log file that is rotated.<br>
Return value: *Integer*.<br>
See [supported platforms](#supported-platforms).

Parameters:

-   **file_regexp** - the absolute path to file and regular [expression](/manual/regular_expressions#overview) describing the file name pattern;<br>
-   **regexp** - a regular [expression](/manual/regular_expressions#overview) describing the required pattern;<br>
-   **encoding** - the code page [identifier](/manual/config/items/itemtypes/zabbix_agent#encoding_settings);<br>
-   **maxproclines** - the maximum number of new lines per second the agent will analyze (cannot exceed 10000). The default value is 10\*'MaxLinesPerSecond' in [zabbix\_agentd.conf](/manual/appendix/config/zabbix_agentd).<br>
-   **mode** - possible values: *all* (default) or *skip* - skip processing of older data (affects only newly created items).<br>
-   **maxdelay** - the maximum delay in seconds. Type: float. Values: 0 - (default) never ignore log file lines; > 0.0 - ignore older lines in order to get the most recent lines analyzed within "maxdelay" seconds. Read the [maxdelay](log_items#using_maxdelay_parameter) notes before using it!<br>
-   **options** - the type of log file rotation and other options. Possible values:<br>*rotate* (default),<br>*copytruncate* - note that *copytruncate* cannot be used together with *maxdelay*. In this case *maxdelay* must be 0 or not specified; see [copytruncate](log_items#notes_on_handling_copytruncate_log_file_rotation) notes,<br>*mtime-reread* - non-unique records, reread if modification time or size changes (default),<br>*mtime-noreread* - non-unique records, reread only if the size changes (ignore modification time change).<br>
-   **persistent\_dir** (only in zabbix\_agentd on Unix systems; not supported in Zabbix agent 2) - the absolute pathname of directory where to store persistent files. See also additional notes on [persistent files](log_items#notes-on-persistent-files-for-log-items).

Comments:

-   The item must be configured as an [active check](/manual/appendix/items/activepassive#active_checks);
-   Matching lines are counted in the new lines since the last log check by the agent, and thus depend on the item update interval;
-   Log rotation is based on the last modification time of files..

[comment]: # ({/new-8ad8a2af})

[comment]: # ({new-518a2dd0})

##### modbus.get[endpoint,<slave id>,<function>,<address>,<count>,<type>,<endianness>,<offset>]

<br>
Reads Modbus data.<br>
Return value: *JSON object*.<br>
[Supported platforms](#supported-platforms): Linux. 

Parameters:

-   **endpoint** - the endpoint defined as `protocol://connection_string`;<br>
-   **slave id** - the slave ID;<br>
-   **function** - the Modbus function;<br>
-   **address** - the address of first registry, coil or input;<br>
-   **count** - the number of records to read;<br>
-   **type** - the type of data;<br>
-   **endianness** - the endianness configuration;<br>
-   **offset** - the number of registers, starting from 'address', the results of which will be discarded.

See a [detailed description](/manual/appendix/items/modbus) of parameters.

[comment]: # ({/new-518a2dd0})

[comment]: # ({new-ba7da5e1})

##### net.dns[<ip>,name,<type>,<timeout>,<count>,<protocol>]

<br>
Checks if the DNS service is up.<br>
Return values: 0 - DNS is down (server did not respond or DNS resolution failed); 1 - DNS is up.<br>
See [supported platforms](#supported-platforms).

Parameters:

-   **ip** - the IP address of DNS server (leave empty for the default DNS server, ignored on Windows unless using Zabbix agent 2);
-   **name** - the DNS name to query;
-   **type** - the record type to be queried (default is *SOA*);
-   **timeout** (ignored on Windows unless using Zabbix agent 2) - the timeout for the request in seconds (default is 1 second);
-   **count** (ignored on Windows unless using Zabbix agent 2) - the number of tries for the request (default is 2);
-   **protocol** - the protocol used to perform DNS queries: *udp* (default) or *tcp*.

Comments:

-   The possible values for `type` are: *ANY*, *A*, *NS*, *CNAME*, *MB*, *MG*, *MR*, *PTR*, *MD*, *MF*, *MX*, *SOA*, *NULL*, *WKS* (not supported for Zabbix agent on Windows, Zabbix agent 2 on all OS), *HINFO*, *MINFO*, *TXT*, *SRV*
-   Internationalized domain names are not supported, please use IDNA encoded names instead.

Example:

    net.dns[198.51.100.1,example.com,MX,2,1]

[comment]: # ({/new-ba7da5e1})

[comment]: # ({new-d5dc268a})

##### net.dns.record[<ip>,name,<type>,<timeout>,<count>,<protocol>]

<br>
Performs a DNS query.<br>
Return value: a character string with the required type of information.<br>
See [supported platforms](#supported-platforms).

Parameters:

-   **ip** - the IP address of DNS server (leave empty for the default DNS server, ignored on Windows unless using Zabbix agent 2);
-   **name** - the DNS name to query;
-   **type** - the record type to be queried (default is *SOA*);
-   **timeout** (ignored on Windows unless using Zabbix agent 2) - the timeout for the request in seconds (default is 1 second);
-   **count** (ignored on Windows unless using Zabbix agent 2) - the number of tries for the request (default is 2);
-   **protocol** - the protocol used to perform DNS queries: *udp* (default) or *tcp*.

Comments:

-   The possible values for `type` are:<br>*ANY*, *A*, *NS*, *CNAME*, *MB*, *MG*, *MR*, *PTR*, *MD*, *MF*, *MX*, *SOA*, *NULL*, *WKS* (not supported for Zabbix agent on Windows, Zabbix agent 2 on all OS), *HINFO*, *MINFO*, *TXT*, *SRV*
-   Internationalized domain names are not supported, please use IDNA encoded names instead.

Example:

    net.dns.record[198.51.100.1,example.com,MX,2,1]

[comment]: # ({/new-d5dc268a})

[comment]: # ({new-1e07380f})

##### net.if.collisions[if]

<br>
The number of out-of-window collisions.<br>
Return value: *Integer*.<br>
[Supported platforms](#supported-platforms): Linux, FreeBSD, Solaris, AIX, MacOS X, OpenBSD, NetBSD. Root privileges are required on NetBSD.

Parameter:

-   **if** - network interface name

[comment]: # ({/new-1e07380f})

[comment]: # ({new-8911d171})

##### net.if.discovery

<br>
The list of network interfaces. Used for low-level discovery.<br>
Return value: *JSON object*.<br>
[Supported platforms](#supported-platforms): Linux, FreeBSD, Solaris, HP-UX, AIX, OpenBSD, NetBSD.

[comment]: # ({/new-8911d171})

[comment]: # ({new-e53ce465})

##### net.if.in[if,<mode>]

<br>
The incoming traffic statistics on a network interface.<br>
Return value: *Integer*.<br>
[Supported platforms](#supported-platforms): Linux, FreeBSD, Solaris^**[5](#footnotes)**^, HP-UX, AIX, MacOS X, OpenBSD, NetBSD. Root privileges are required on NetBSD.

Parameters:

-   **if** - network interface name (Unix); network interface full description or IPv4 address; or, if in braces, network interface GUID (Windows);
-   **mode** - possible values:<br>*bytes* - number of bytes (default)<br>*packets* - number of packets<br>*errors* - number of errors<br>*dropped* - number of dropped packets<br>*overruns (fifo)* - the number of FIFO buffer errors<br>*frame* - the number of packet framing errors<br>*compressed* - the number of compressed packets transmitted or received by the device driver<br>*multicast* - the number of multicast frames received by the device driver

Comments:

-   You may use this key with the *Change per second* preprocessing step in order to get the bytes-per-second statistics;
-   The *dropped* mode is supported only on Linux, FreeBSD, HP-UX, MacOS X, OpenBSD, NetBSD;
-   The *overruns*, *frame*, *compressed*, *multicast* modes are supported only on Linux;
-   On HP-UX this item does not provide details on loopback interfaces (e.g. lo0).

Examples:

    net.if.in[eth0]
    net.if.in[eth0,errors]

[comment]: # ({/new-e53ce465})

[comment]: # ({new-ab67e043})

##### net.if.out[if,<mode>]

<br>
The outgoing traffic statistics on a network interface.<br>
Return value: *Integer*.<br>
[Supported platforms](#supported-platforms): Linux, FreeBSD, Solaris^**[5](#footnotes)**^, HP-UX, AIX, MacOS X, OpenBSD, NetBSD. Root privileges are required on NetBSD.

Parameters:

-   **if** - network interface name (Unix); network interface full description or IPv4 address; or, if in braces, network interface GUID (Windows);
-   **mode** - possible values:<br>*bytes* - number of bytes (default)<br>*packets* - number of packets<br>*errors* - number of errors<br>*dropped* - number of dropped packets<br>*overruns (fifo)* - the number of FIFO buffer errors<br>*collisions (colls)* - the number of collisions detected on the interface<br>*carrier* - the number of carrier losses detected by the device driver<br>*compressed* - the number of compressed packets transmitted by the device driver

Comments:

-   You may use this key with the *Change per second* preprocessing step in order to get the bytes-per-second statistics;
-   The *dropped* mode is supported only on Linux, HP-UX;
-   The *overruns*, *collision*, *carrier*, *compressed* modes are supported only on Linux;
-   On HP-UX this item does not provide details on loopback interfaces (e.g. lo0).

Examples:

    net.if.out[eth0]
    net.if.out[eth0,errors]

[comment]: # ({/new-ab67e043})

[comment]: # ({new-3c96d291})

##### net.if.total[if,<mode>]

<br>
The sum of incoming and outgoing traffic statistics on a network interface.<br>
Return value: *Integer*.<br>
[Supported platforms](#supported-platforms): Linux, FreeBSD, Solaris^**[5](#footnotes)**^, HP-UX, AIX, MacOS X, OpenBSD, NetBSD. Root privileges are required on NetBSD.

Parameters:

-   **if** - network interface name (Unix); network interface full description or IPv4 address; or, if in braces, network interface GUID (Windows);
-   **mode** - possible values:<br>*bytes* - number of bytes (default)<br>*packets* - number of packets<br>*errors* - number of errors<br>*dropped* - number of dropped packets<br>*overruns (fifo)* - the number of FIFO buffer errors<br>*collisions (colls)* - the number of collisions detected on the interface<br>*compressed* - the number of compressed packets transmitted or received by the device driver

Comments:

-   You may use this key with the *Change per second* preprocessing step in order to get the bytes-per-second statistics;
-   The *dropped* mode is supported only on Linux, HP-UX. Dropped packets are supported only if both `net.if.in` and `net.if.out` work for dropped packets on your platform.
-   The *overruns*, *collision*, *compressed* modes are supported only on Linux;
-   On HP-UX this item does not provide details on loopback interfaces (e.g. lo0).

Examples:

    net.if.total[eth0]
    net.if.total[eth0,errors]

[comment]: # ({/new-3c96d291})

[comment]: # ({new-a72f9721})

##### net.tcp.listen[port]

<br>
Checks if this TCP port is in LISTEN state.<br>
Return values: 0 - it is not in LISTEN state; 1 - it is in LISTEN state.<br>
[Supported platforms](#supported-platforms): Linux, FreeBSD, Solaris, MacOS X.

Parameter:

-   **port** - TCP port number

On Linux kernels 2.6.14 and above, the information about listening TCP sockets is obtained from the kernel's NETLINK interface, if possible. Otherwise, the information is retrieved from /proc/net/tcp and /roc/net/tcp6 files.

Example:

    net.tcp.listen[80]

[comment]: # ({/new-a72f9721})

[comment]: # ({new-000f516d})

##### net.tcp.port[<ip>,port]

<br>
Checks if it is possible to make a TCP connection to the specified port.<br>
Return values: 0 - cannot connect; 1 - can connect.<br>
See [supported platforms](#supported-platforms).

Parameters:

-   **ip** - the IP address or DNS name (default is 127.0.0.1);
-   **port** - the port number.

Comments:

-   For simple TCP performance testing use `net.tcp.service.perf[tcp,<ip>,<port>]`;
-   These checks may result in additional messages in system daemon logfiles (SMTP and SSH sessions being logged usually).

Example:

    net.tcp.port[,80] #this item can be used to test the web server availability running on port 80

[comment]: # ({/new-000f516d})

[comment]: # ({new-11ca536e})

##### net.tcp.service[service,<ip>,<port>]

<br>
Checks if a service is running and accepting TCP connections.<br>
Return values: 0 - service is down; 1 - service is running.<br>
See [supported platforms](#supported-platforms).

Parameters:

-   **service** - *ssh*, *ldap*, *smtp*, *ftp*, *http*, *pop*, *nntp*, *imap*, *tcp*, *https*, or *telnet* (see [details](/manual/appendix/items/service_check_details));
-   **ip** - the IP address (default is 127.0.0.1);
-   **port** - the port number (by default the standard service port number is used).

Comments:

-   These checks may result in additional messages in system daemon logfiles (SMTP and SSH sessions being logged usually);
-   Checking of encrypted protocols (like IMAP on port 993 or POP on port 995) is currently not supported. As a workaround, please use `net.tcp.port[]` for checks like these.
-   Checking of LDAP and HTTPS on Windows is only supported by Zabbix agent 2;
-   The telnet check looks for a login prompt (':' at the end);
-   See also [known issues](/manual/installation/known_issues#https_checks) of checking the HTTPS service.

Example:

    net.tcp.service[ftp,,45] #this item can be used to test the availability of FTP server on TCP port 45

[comment]: # ({/new-11ca536e})

[comment]: # ({new-49a8f9ac})

##### net.tcp.service.perf[service,<ip>,<port>]

<br>
Checks the performance of a TCP service.<br>
Return values: 0 - service is down; seconds - the number of seconds spent while connecting to the service.<br>
See [supported platforms](#supported-platforms).

Parameters:

-   **service** - *ssh*, *ldap*, *smtp*, *ftp*, *http*, *pop*, *nntp*, *imap*, *tcp*, *https*, or *telnet* (see [details](/manual/appendix/items/service_check_details));
-   **ip** - the IP address (default is 127.0.0.1);
-   **port** - the port number (by default the standard service port number is used).

Comments:

-   Checking of encrypted protocols (like IMAP on port 993 or POP on port 995) is currently not supported. As a workaround, please use `net.tcp.service.perf[tcp,<ip>,<port>]` for checks like these.
-   The telnet check looks for a login prompt (':' at the end);
-   See also [known issues](/manual/installation/known_issues#https_checks) of checking the HTTPS service.

Example:

    net.tcp.service.perf[ssh] #this item can be used to test the speed of initial response from the SSH server

[comment]: # ({/new-49a8f9ac})

[comment]: # ({new-29fba160})

##### net.tcp.socket.count[<laddr>,<lport>,<raddr>,<rport>,<state>]

<br>
Returns the number of TCP sockets that match parameters.<br>
Return value: *Integer*.<br>
[Supported platforms](#supported-platforms): Linux.

Parameters:

-   **laddr** - the local IPv4/6 address or CIDR subnet;
-   **lport** - the local port number or service name;
-   **raddr** - the remote IPv4/6 address or CIDR subnet;
-   **rport** - the remote port number or service name;
-   **state** - the connection state (*established*, *syn\_sent*, *syn\_recv*, *fin\_wait1*, *fin\_wait2*, *time\_wait*, *close*, *close\_wait*, *last\_ack*, *listen*, *closing*).

Example:

    net.tcp.socket.count[,80,,,established] #check if the local TCP port 80 is in "established" state

[comment]: # ({/new-29fba160})

[comment]: # ({new-b9f198f7})

##### net.udp.listen[port]

<br>
Checks if this UDP port is in LISTEN state.<br>
Return values: 0 - it is not in LISTEN state; 1 - it is in LISTEN state.<br>
[Supported platforms](#supported-platforms): Linux, FreeBSD, Solaris, MacOS X.

Parameter:

-   **port** - UDP port number

Example:

    net.udp.listen[68]

[comment]: # ({/new-b9f198f7})

[comment]: # ({new-7c60892f})

##### net.udp.service[service,<ip>,<port>]

<br>
Checks if a service is running and responding to UDP requests.<br>
Return values: 0 - service is down; 1 - service is running.<br>
See [supported platforms](#supported-platforms).

Parameters:

-   **service** - *ntp* (see [details](/manual/appendix/items/service_check_details));
-   **ip** - the IP address (default is 127.0.0.1);
-   **port** - the port number (by default the standard service port number is used).

Example:

    net.udp.service[ntp,,45] #this item can be used to test the availability of NTP service on UDP port 45

[comment]: # ({/new-7c60892f})

[comment]: # ({new-8697ef9c})

##### net.udp.service.perf[service,<ip>,<port>]

<br>
Checks the performance of a UDP service.<br>
Return values: 0 - service is down; seconds - the number of seconds spent waiting for response from the service.<br>
See [supported platforms](#supported-platforms).

Parameters:

-   **service** - *ntp* (see [details](/manual/appendix/items/service_check_details));
-   **ip** - the IP address (default is 127.0.0.1);
-   **port** - the port number (by default the standard service port number is used).

Example:

    net.udp.service.perf[ntp] #this item can be used to test response time from NTP service

[comment]: # ({/new-8697ef9c})

[comment]: # ({new-25a2206c})

##### net.udp.socket.count[<laddr>,<lport>,<raddr>,<rport>,<state>]

<br>
Returns the number of UDP sockets that match parameters.<br>
Return value: *Integer*.<br>
[Supported platforms](#supported-platforms): Linux.

Parameters:

-   **laddr** - the local IPv4/6 address or CIDR subnet;
-   **lport** - the local port number or service name;
-   **raddr** - the remote IPv4/6 address or CIDR subnet;
-   **rport** - the remote port number or service name;
-   **state** - the connection state (*established*, *unconn*).

Example:

    net.udp.socket.count[,,,,listening] → check if any UDP socket is in "listening" state

[comment]: # ({/new-25a2206c})

[comment]: # ({new-4c3ddb55})

##### proc.cpu.util[<name>,<user>,<type>,<cmdline>,<mode>,<zone>]

<br>
The process CPU utilization percentage.<br>
Return value: *Float*.<br>
[Supported platforms](#supported-platforms): Linux, Solaris^**[6](#footnotes)**^.

Parameters:

-   **name** - the process name (default is *all processes*);
-   **user** - the user name (default is *all users*);
-   **type** - the CPU utilization type: *total* (default), *user*, or *system*;
-   **cmdline** - filter by command line (it is a regular [expression](/manual/regular_expressions#overview));
-   **mode** - the data gathering mode: *avg1* (default), *avg5*, or *avg15*;
-   **zone** - the target zone: *current* (default) or *all*. This parameter is supported on Solaris only.

Comments:

-   The returned value is based on a single CPU core utilization percentage. For example, the CPU utilization of a process fully using two cores is 200%.
-   The process CPU utilization data is gathered by a collector which supports the maximum of 1024 unique (by name, user and command line) queries. Queries not accessed during the last 24 hours are removed from the collector.
-   When setting the `zone` parameter to *current* (or default) in case the agent has been compiled on a Solaris without zone support, but running on a newer Solaris where zones are supported, then the agent will return NOTSUPPORTED (the agent cannot limit results to only the current zone). However, *all* is supported in this case.

Examples:

    proc.cpu.util[,root] #CPU utilization of all processes running under the "root" user
    proc.cpu.util[zabbix_server,zabbix] #CPU utilization of all zabbix_server processes running under the zabbix user

[comment]: # ({/new-4c3ddb55})

[comment]: # ({new-02480d9a})

##### proc.get[<name>,<user>,<cmdline>,<mode>]

<br>
The list of OS processes and their parameters. Can be used for low-level discovery.<br>
Return value: *JSON object*.<br>
[Supported platforms](#supported-platforms): Linux, FreeBSD, Windows, OpenBSD, NetBSD.

Parameters:

-   **name** - the process name (default *all processes*);
-   **user** - the user name (default *all users*);
-   **cmdline** - filter by command line (it is a regular [expression](/manual/regular_expressions#overview)). This parameter is not supported for Windows; on other platforms it is not supported if mode is set to 'summary'.
-   **mode** - possible values:<br>*process* (default), *thread* (not supported for NetBSD), *summary*. See a list of [process parameters](/manual/appendix/items/proc_get) returned for each mode and OS.

Comments:

-   If a value cannot be retrieved, for example, because of an error (process already died, lack of permissions, system call failure), `-1` will be returned;
-   See [notes](/manual/appendix/items/proc_mem_num_notes) on selecting processes with `name` and `cmdline` parameters (Linux-specific).

Examples:

    proc.get[zabbix,,,process] #list of all Zabbix processes, returns one entry per PID
    proc.get[java,,,thread] #list of all Java processes, returns one entry per thread
    proc.get[zabbix,,,summary] #combined data for Zabbix processes of each type, returns one entry per process name

[comment]: # ({/new-02480d9a})

[comment]: # ({new-d3e42b1a})

##### proc.mem[<name>,<user>,<mode>,<cmdline>,<memtype>]

<br>
The memory used by the process in bytes.<br>
Return value: *Integer* - with `mode` as *max*, *min*, *sum*; *Float* - with `mode` as *avg*<br>
[Supported platforms](#supported-platforms): Linux, FreeBSD, Solaris, AIX, Tru64, OpenBSD, NetBSD.

Parameters:

-   **name** - the process name (default is *all processes*);
-   **user** - the user name (default is *all users*);
-   **mode** - possible values: *avg*, *max*, *min*, or *sum* (default);
-   **cmdline** - filter by command line (it is a regular [expression](/manual/regular_expressions#overview));
-   **memtype** - the [type of memory](/manual/appendix/items/proc_mem_notes) used by process

Comments:

-   The `memtype` parameter is supported only on Linux, FreeBSD, Solaris^**[6](#footnotes)**^, AIX;
-   When several processes use shared memory, the sum of memory used by processes may result in large, unrealistic values.<br><br>See [notes](/manual/appendix/items/proc_mem_num_notes) on selecting processes with `name` and `cmdline` parameters (Linux-specific).<br><br>When this item is invoked from the command line and contains a command line parameter (e.g. using the agent test mode: `zabbix_agentd -t proc.mem[,,,apache2]`), one extra process will be counted, as the agent will count itself.

Examples:

    proc.mem[,root] #the memory used by all processes running under the "root" user
    proc.mem[zabbix_server,zabbix] #the memory used by all zabbix_server processes running under the zabbix user
    proc.mem[,oracle,max,oracleZABBIX] #the memory used by the most memory-hungry process running under Oracle having oracleZABBIX in its command line

[comment]: # ({/new-d3e42b1a})

[comment]: # ({new-8e6bf30e})

##### proc.num[<name>,<user>,<state>,<cmdline>,<zone>]

<br>
The number of processes.<br>
Return value: *Integer*.<br>
[Supported platforms](#supported-platforms): Linux, FreeBSD, Solaris^**[6](#footnotes)**^, HP-UX, AIX, Tru64, OpenBSD, NetBSD.

Parameters:

-   **name** - the process name (default is *all processes*);
-   **user** - the user name (default is *all users*);
-   **state** - possible values:<br>*all* (default),<br>*disk* - uninterruptible sleep,<br>*run* - running,<br>*sleep* - interruptible sleep,<br>*trace* - stopped,<br>*zomb* - zombie;
-   **cmdline** - filter by command line (it is a regular [expression](/manual/regular_expressions#overview));
-   **zone** - the target zone: *current* (default), or *all*. This parameter is supported on Solaris only.

Comments:

-   The *disk* and *trace* state parameters are supported only on Linux, FreeBSD, OpenBSD, NetBSD;
-   When this item is invoked from the command line and contains a command line parameter (e.g. using the agent test mode: `zabbix_agentd -t proc.num[,,,apache2]`), one extra process will be counted, as the agent will count itself;
-   When setting the `zone` parameter to *current* (or default) in case the agent has been compiled on a Solaris without zone support, but running on a newer Solaris where zones are supported, then the agent will return NOTSUPPORTED (the agent cannot limit results to only the current zone). However, *all* is supported in this case.
-   See [notes](/manual/appendix/items/proc_mem_num_notes) on selecting processes with `name` and `cmdline` parameters (Linux-specific).

Examples:

    proc.num[,mysql] #the number of processes running under the mysql user
    proc.num[apache2,www-data] #the number of apache2 processes running under the www-data user
    proc.num[,oracle,sleep,oracleZABBIX] #the number of processes in sleep state running under Oracle having oracleZABBIX in its command line

[comment]: # ({/new-8e6bf30e})

[comment]: # ({new-7cdd8214})

##### sensor[device,sensor,<mode>]

<br>
Hardware sensor reading.<br>
Return value: *Float*.<br>
[Supported platforms](#supported-platforms): Linux, OpenBSD.

Parameters:

-   **device** - the device name;
-   **sensor** - the sensor name;
-   **mode** - possible values: *avg*, *max*, or *min* (if this parameter is omitted, device and sensor are treated verbatim).

Comments:

-   Reads /proc/sys/dev/sensors on Linux 2.4;
-   Reads /sys/class/hwmon on Linux 2.6+. See a more detailed description of [sensor](/manual/appendix/items/sensor) item on Linux.
-   Reads the *hw.sensors* MIB on OpenBSD.

Example:

    sensor[w83781d-i2c-0-2d,temp1]
    sensor[cpu0,temp0] #the temperature of one CPU
    sensor["cpu[0-2]$",temp,avg] #the average temperature of the first three CPUs

[comment]: # ({/new-7cdd8214})

[comment]: # ({new-f46449fd})

##### system.boottime

<br>
The system boot time.<br>
Return value: *Integer (Unix timestamp)*.<br>
[Supported platforms](#supported-platforms): Linux, FreeBSD, Solaris, MacOS X, OpenBSD, NetBSD.

[comment]: # ({/new-f46449fd})

[comment]: # ({new-b01f71c5})

##### system.cpu.discovery

<br>
The list of detected CPUs/CPU cores. Used for low-level discovery.<br>
Return value: *JSON object*.<br>
See [supported platforms](#supported-platforms).

[comment]: # ({/new-b01f71c5})

[comment]: # ({new-956c4ef9})

##### system.cpu.intr

<br>
The device interrupts.<br>
Return value: *Integer*.<br>
[Supported platforms](#supported-platforms): Linux, FreeBSD, Solaris, AIX, OpenBSD, NetBSD.

[comment]: # ({/new-956c4ef9})

[comment]: # ({new-c8c74d92})

##### system.cpu.load[<cpu>,<mode>]

<br>
The [CPU load](http://en.wikipedia.org/wiki/Load_(computing)).<br>
Return value: *Float*.<br>
See [supported platforms](#supported-platforms).

Parameters:

-   **cpu** - possible values: *all* (default) or *percpu* (the total load divided by online CPU count);
-   **mode** - possible values: *avg1* (one-minute average, default), *avg5*, or *avg15*.

The *percpu* parameter is not supported on Tru64.

Example:

    system.cpu.load[,avg5]

[comment]: # ({/new-c8c74d92})

[comment]: # ({new-e110ddf9})

##### system.cpu.num[<type>]

<br>
The number of CPUs.<br>
Return value: *Integer*.<br>
[Supported platforms](#supported-platforms): Linux, FreeBSD, Solaris, HP-UX, AIX, MacOS X, OpenBSD, NetBSD.

Parameter:

-   **type** - possible values: *online* (default) or *max*

The *max* type parameter is supported only on Linux, FreeBSD, Solaris, MacOS X.

Example: 
    
    system.cpu.num

[comment]: # ({/new-e110ddf9})

[comment]: # ({new-addfe1ba})

##### system.cpu.switches

<br>
The count of context switches.<br>
Return value: *Integer*.<br>
[Supported platforms](#supported-platforms): Linux, FreeBSD, Solaris, AIX, OpenBSD, NetBSD.

[comment]: # ({/new-addfe1ba})

[comment]: # ({new-d1abd90a})

##### system.cpu.util[<cpu>,<type>,<mode>,<logical_or_physical>]

<br>
The CPU utilization percentage.<br>
Return value: *Float*.<br>
[Supported platforms](#supported-platforms): Linux, FreeBSD, Solaris, HP-UX, AIX, Tru64, OpenBSD, NetBSD.

Parameters:

-   **cpu** - *<CPU number>* or *all* (default);
-   **type** - possible values: *user* (default), *idle*, *nice*, *system*, *iowait*, *interrupt*, *softirq*, *steal*, *guest* (on Linux kernels 2.6.24 and above), or *guest\_nice* (on Linux kernels 2.6.33 and above);
-   **mode** - possible values: *avg1* (one-minute average, default), *avg5*, or *avg15*;
-   **logical\_or\_physical** - possible values: *logical* (default) or *physical*. This parameter is supported on AIX only.

Comments:

-   The *nice* type parameter is supported only on Linux, FreeBSD, HP-UX, Tru64, OpenBSD, NetBSD.
-   The *iowait* type parameter is supported only on Linux 2.6 and later, Solaris, AIX.
-   The *interrupt* type parameter is supported only on Linux 2.6 and later, FreeBSD, OpenBSD.
-   The *softirq*, *steal*, *guest*, *guest_nice* type parameters are supported only on Linux 2.6 and later.
-   The *avg5* and *avg15* mode parameters are supported on Linux, FreeBSD, Solaris, HP-UX, AIX, OpenBSD, NetBSD.

Example:

    system.cpu.util[0,user,avg5]

[comment]: # ({/new-d1abd90a})

[comment]: # ({new-05f3200c})

##### system.hostname[<type>,<transform>]

<br>
The system host name.<br>
Return value: *String*.<br>
See [supported platforms](#supported-platforms).

Parameters:

-   **type** - possible values: *netbios* (default on Windows), *host* (default on Linux) or *shorthost* (since version 5.4.7; returns part of the hostname before the first dot, a full string for names without dots);
-   **transform** - possible values: *none* (default) or *lower* (convert to lowercase).

The value is acquired by taking `nodename` from the uname() system API output.

Examples of returned values:

    system.hostname → linux-w7x1
    system.hostname → example.com
    system.hostname[shorthost] → example

[comment]: # ({/new-05f3200c})

[comment]: # ({new-d14d8400})

##### system.hw.chassis[<info>]

<br>
The chassis information.<br>
Return value: *String*.<br>
[Supported platforms](#supported-platforms): Linux.

Parameter:

-   **info** - possible values: *full* (default), *model*, *serial*, *type*, or *vendor*

Comments:

-   This item key depends on the availability of the [SMBIOS](http://en.wikipedia.org/wiki/System_Management_BIOS) table;
-   It will try to read the DMI table from sysfs, if sysfs access fails then try reading directly from memory;
-   **Root permissions** are required because the value is acquired by reading from sysfs or memory.

Example: 

    system.hw.chassis[full] → Hewlett-Packard HP Pro 3010 Small Form Factor PC CZXXXXXXXX Desktop

[comment]: # ({/new-d14d8400})

[comment]: # ({new-33f13e22})

##### system.hw.cpu[<cpu>,<info>]

<br>
The CPU information.<br>
Return value: *String* or *Integer*.<br>
[Supported platforms](#supported-platforms): Linux.

Parameters:

-   **cpu** - *<CPU number>* or *all* (default);
-   **info** - possible values: *full* (default), *curfreq*, *maxfreq*, *model* or *vendor*.

Comments:

-   Gathers info from `/proc/cpuinfo` and `/sys/devices/system/cpu/[cpunum]/cpufreq/cpuinfo_max_freq`;
-   If a CPU number and *curfreq* or *maxfreq* is specified, a numeric value is returned (Hz).

Example:

    system.hw.cpu[0,vendor] → AuthenticAMD

[comment]: # ({/new-33f13e22})

[comment]: # ({new-87688787})

##### system.hw.devices[<type>]

<br>
The listing of PCI or USB devices.<br>
Return value: *Text*.<br>
[Supported platforms](#supported-platforms): Linux.

Parameter: 

-   **type** - *pci* (default) or *usb*

Returns the output of either the lspci or lsusb utility (executed without any parameters).

Example:

    system.hw.devices → 00:00.0 Host bridge: Advanced Micro Devices [AMD] RS780 Host Bridge

[comment]: # ({/new-87688787})

[comment]: # ({new-ee01b8b4})

##### system.hw.macaddr[<interface>,<format>]

<br>
The listing of MAC addresses.<br>
Return value: *String*.<br>
[Supported platforms](#supported-platforms): Linux.

Parameters:

-   **interface** - *all* (default) or a regular [expression](/manual/regular_expressions#overview);
-   **format** - *full* (default) or *short*

Comments:

-   Lists MAC addresses of the interfaces whose name matches the given `interface` regular [expression](/manual/regular_expressions#overview) (*all* lists for all interfaces);
-   If `format` is specified as *short*, interface names and identical MAC addresses are not listed.

Example:

    system.hw.macaddr["eth0$",full] → [eth0] 00:11:22:33:44:55

[comment]: # ({/new-ee01b8b4})

[comment]: # ({new-1729b15d})

##### system.localtime[<type>]

<br>
The system time.<br>
Return value: *Integer* - with `type` as *utc*; *String* - with `type` as *local*.<br>
See [supported platforms](#supported-platforms).

Parameters:

-   **type** - possible values: *utc* - (default) the time since the Epoch (00:00:00 UTC, January 1, 1970), measured in seconds or *local* - the time in the 'yyyy-mm-dd,hh:mm:ss.nnn,+hh:mm' format

Must be used as a [passive check](/manual/appendix/items/activepassive#passive_checks) only.

Example:

    system.localtime[local] #create an item using this key and then use it to display the host time in the *Clock* dashboard widget.

[comment]: # ({/new-1729b15d})

[comment]: # ({new-806250ba})

##### system.run[command,<mode>]

<br>
Run the specified command on the host.<br>
Return value: *Text* result of the command or 1 - with `mode` as *nowait* (regardless of the command result).<br>
See [supported platforms](#supported-platforms).

Parameters:

-   **command** - command for execution;<br>
-   **mode** - possible values: *wait* - wait end of execution (default) or *nowait* - do not wait.

Comments: 

-   This item is disabled by default. Learn how to [enable them](/manual/config/items/restrict_checks);
-   Up to 512KB of data can be returned, including the trailing whitespace that is truncated;
-   To be processed correctly, the output of the command must be text;
-   The return value of the item is standard output together with standard error produced by command. The exit code is not checked. Empty result is allowed.
-   See also: [Command execution](/manual/appendix/command_execution).

Example:

    system.run[ls -l /] #return a detailed file list of the root directory

[comment]: # ({/new-806250ba})

[comment]: # ({new-ed8aa2a2})

##### system.stat[resource,<type>]

<br>
The system statistics.<br>
Return value: *Integer* or *float*.<br>
[Supported platforms](#supported-platforms): AIX.

Parameters:

-   **ent** - the number of processor units this partition is entitled to receive (float);
-   **kthr,<type>** - information about kernel thread states:<br>*r* - average number of runnable kernel threads (float)<br>*b* - average number of kernel threads placed in the Virtual Memory Manager wait queue (float)
-   **memory,<type>** - information about the usage of virtual and real memory:<br>*avm* - active virtual pages (integer)<br>*fre* - size of the free list (integer)
-   **page,<type>** - information about page faults and paging activity:<br>*fi* - file page-ins per second (float)<br>*fo* - file page-outs per second (float)<br>*pi* - pages paged in from paging space (float)<br>*po* - pages paged out to paging space (float)<br>*fr* - pages freed (page replacement) (float)<br>*sr* - pages scanned by page-replacement algorithm (float)
-   **faults,<type>** - trap and interrupt rate:<br>*in* - device interrupts (float)<br>*sy* - system calls (float)<br>*cs* - kernel thread context switches (float)
-   **cpu,<type>** - breakdown of percentage usage of processor time:<br>*us* - user time (float)<br>*sy* - system time (float)<br>*id* - idle time (float)<br>*wa* - idle time during which the system had outstanding disk/NFS I/O request(s) (float)<br>*pc* - number of physical processors consumed (float)<br>*ec* - the percentage of entitled capacity consumed (float)<br>*lbusy* - indicates the percentage of logical processor(s) utilization that occurred while executing at the user and system level (float)<br>*app* - indicates the available physical processors in the shared pool (float)
-   **disk,<type>** - disk statistics:<br>*bps* - indicates the amount of data transferred (read or written) to the drive in bytes per second (integer)<br>*tps* - indicates the number of transfers per second that were issued to the physical disk/tape (float)|

Comments:

-   Take note of the following limitations in these items:<br>
    `system.stat[cpu,app]` - supported only on AIX LPAR of type "Shared"<br>
    `system.stat[cpu,ec]` - supported on AIX LPAR of type "Shared" and "Dedicated" ("Dedicated" always returns 100 (percent))<br>
    `system.stat[cpu,lbusy]` - supported only on AIX LPAR of type "Shared"<br>
    `system.stat[cpu,pc]` - supported on AIX LPAR of type "Shared" and "Dedicated"<br>
    `system.stat[ent]` - supported on AIX LPAR of type "Shared" and "Dedicated"

[comment]: # ({/new-ed8aa2a2})

[comment]: # ({new-087e6420})

##### system.sw.arch

<br>
The software architecture information.<br>
Return value: *String*.<br>
See [supported platforms](#supported-platforms).

The info is acquired from the `uname()` function.

Example:

    system.sw.arch → i686

[comment]: # ({/new-087e6420})

[comment]: # ({new-ed818157})

##### system.sw.os[<info>]

<br>
The operating system information.<br>
Return value: *String*.<br>
[Supported platforms](#supported-platforms): Linux, Windows. Supported on Windows since Zabbix 6.4.

Parameter:

-   **info** - possible values: *full* (default), *short*, or *name*

The info is acquired from (note that not all files and options are present in all distributions):

-   `/proc/version` (*full*) on Linux;
-   `/proc/version_signature` (*short*) on Linux;
-   the PRETTY_NAME parameter from `/etc/os-release` on Linux-systems supporting it or `/etc/issue.net` (*name*);
-   the `HKLM\\SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion` registry key on Windows.

Examples:

    system.sw.os[short] → Ubuntu 2.6.35-28.50-generic 2.6.35.11
    system.sw.os[full] → [s|Windows 10 Enterprise 22621.1.amd64fre.ni_release.220506-1250 Build 22621.963]

[comment]: # ({/new-ed818157})

[comment]: # ({new-38e87471})

##### system.sw.os.get

<br>
Detailed information about the operating system (version, type, distribution name, minor and major version, etc).<br>
Return value: *JSON object*.<br>
[Supported platforms](#supported-platforms): Linux, Windows. Supported since Zabbix 6.4.

[comment]: # ({/new-38e87471})

[comment]: # ({new-bc2ee313})

##### system.sw.packages[<regexp>,<manager>,<format>]

<br>
The listing of installed packages.<br>
Return value: *Text*.<br>
[Supported platforms](#supported-platforms): Linux.

Parameters:

-   **regexp** - *all* (default) or a regular [expression](/manual/regular_expressions#overview);
-   **manager** - *all* (default) or a package manager;
-   **format** - *full* (default) or *short*.

Comments:

-   Lists (alphabetically) installed packages whose name matches the given regular [expression](/manual/regular_expressions#overview) (*all* lists them all);
-   Supported package managers (executed command):<br>dpkg (dpkg --get-selections)<br>pkgtool (ls /var/log/packages)<br>rpm (rpm -qa)<br>pacman (pacman -Q)
-   If `format` is specified as *full*, packages are grouped by package managers (each manager on a separate line beginning with its name in square brackets);
-   If `format` is specified as *short*, packages are not grouped and are listed on a single line.

Example:

    system.sw.packages[mini,dpkg,short] → python-minimal, python2.6-minimal, ubuntu-minimal

[comment]: # ({/new-bc2ee313})

[comment]: # ({new-43f5d666})

##### system.sw.packages.get[<regexp>,<manager>]

<br>
A detailed listing of installed packages.<br>
Return value: *JSON object*.<br>
[Supported platforms](#supported-platforms): Linux. Supported since Zabbix 6.4.

Parameters:

-   **regexp** - *all* (default) or a regular [expression](/manual/regular_expressions#overview);
-   **manager** - *all* (default) or a package manager (possible values: *rpm*, *dpkg*, *pkgtool*, or *pacman*).

Comments:

-   Returns unformatted JSON with the installed packages whose name matches the given regular expression;
-   The output is an array of objects each containing the following keys: name, manager, version, size, architecture, buildtime and installtime (see [more details](/manual/appendix/items/return_values#system.sw.packages.get)).

[comment]: # ({/new-43f5d666})

[comment]: # ({new-b0c4a5e8})

##### system.swap.in[<device>,<type>]

<br>
The swap-in (from device into memory) statistics.<br>
Return value: *Integer*.<br>
[Supported platforms](#supported-platforms): Linux, FreeBSD, OpenBSD.

Parameters:

-   **device** - specify the device used for swapping (Linux only) or *all* (default);
-   **type** - possible values: *count* (number of swapins, default on non-Linux platforms), *sectors* (sectors swapped in), or *pages* (pages swapped in, default on Linux).

Comments:

-   The source of this information is:<br>/proc/swaps, /proc/partitions, /proc/stat (Linux 2.4)<br>/proc/swaps, /proc/diskstats, /proc/vmstat (Linux 2.6)
-   Note that *pages* will only work if device was not specified;
-   The *sectors* type parameter is supported only on Linux.

Example:

    system.swap.in[,pages]

[comment]: # ({/new-b0c4a5e8})

[comment]: # ({new-6cc86983})

##### system.swap.out[<device>,<type>]

<br>
The swap-out (from memory onto device) statistics.<br>
Return value: *Integer*.<br>
[Supported platforms](#supported-platforms): Linux, FreeBSD, OpenBSD.

Parameters:

-   **device** - specify the device used for swapping (Linux only) or *all* (default);
-   **type** - possible values: *count* (number of swapouts, default on non-Linux platforms), *sectors* (sectors swapped out), or *pages* (pages swapped out, default on Linux).

Comments:

-   The source of this information is:<br>`/proc/swaps`, `/proc/partitions`, `/proc/stat` (Linux 2.4)<br>`/proc/swaps`, `/proc/diskstats`, `/proc/vmstat` (Linux 2.6)
-   Note that *pages* will only work if device was not specified;
-   The *sectors* type parameter is supported only on Linux.

Example:

    system.swap.out[,pages]

[comment]: # ({/new-6cc86983})

[comment]: # ({new-5815bd72})

##### system.swap.size[<device>,<type>]

<br>
The swap space size in bytes or in percentage from total.<br>
Return value: *Integer* - for bytes; *Float* - for percentage.<br>
[Supported platforms](#supported-platforms): Linux, FreeBSD, Solaris, AIX, Tru64, OpenBSD.

Parameters:

-   **device** - specify the device used for swapping (FreeBSD only) or *all* (default);
-   **type** - possible values: *free* (free swap space, default), *pfree* (free swap space, in percent), *pused* (used swap space, in percent), *total* (total swap space), or *used* (used swap space).

Comments:

-   Note that *pfree*, *pused* are not supported on Windows if swap size is 0;
-   If device is not specified Zabbix agent will only take into account swap devices (files), the physical memory will be ignored. For example, on Solaris systems the `swap -s` command includes a portion of physical memory and swap devices (unlike `swap -l`).

Example:

    system.swap.size[,pfree] → free swap space percentage
    

[comment]: # ({/new-5815bd72})

[comment]: # ({new-6ceb79d2})

##### system.uname

<br>
Identification of the system.<br>
Return value: *String*.<br>
See [supported platforms](#supported-platforms).

Comments:

-   On UNIX the value for this item is obtained with the uname() system call;
-   On Windows the item returns the OS architecture, whereas on UNIX it returns the CPU architecture.|


Example (UNIX):

     system.uname → FreeBSD localhost 4.2-RELEASE FreeBSD 4.2-RELEASE #0: Mon Nov i386
     

[comment]: # ({/new-6ceb79d2})

[comment]: # ({new-545f6370})

##### system.uptime

<br>
The system uptime in seconds.<br>
Return value: *Integer*.<br>
[Supported platforms](#supported-platforms): Linux, FreeBSD, Solaris, AIX, MacOS X, OpenBSD, NetBSD. The support on Tru64 is unknown.

In [item configuration](/manual/config/items/item#configuration), use **s** or **uptime** units to get readable values.|

[comment]: # ({/new-545f6370})

[comment]: # ({new-c03c81d9})

##### system.users.num

<br>
The number of users logged in.<br>
Return value: *Integer*.<br>
See [supported platforms](#supported-platforms).

The **who** command is used on the agent side to obtain the value.

[comment]: # ({/new-c03c81d9})

[comment]: # ({new-48d7b263})

##### vfs.dev.discovery

<br>
The list of block devices and their type. Used for low-level discovery.<br>
Return value: *JSON object*.<br>
[Supported platforms](#supported-platforms): Linux.

[comment]: # ({/new-48d7b263})

[comment]: # ({new-bde9774e})

##### vfs.dev.read[<device>,<type>,<mode>]

<br>
The disk read statistics.<br>
Return value: *Integer* - with `type` in *sectors*, *operations*, *bytes*; *Float* - with `type` in *sps*, *ops*, *bps*.<br>
[Supported platforms](#supported-platforms): Linux, FreeBSD, Solaris, AIX, OpenBSD.

Parameters:

-   **device** - disk device (default is *all* ^**[3](#footnotes)**^);
-   **type** - possible values: *sectors*, *operations*, *bytes*, *sps*, *ops*, or *bps* (*sps*, *ops*, *bps* stand for: sectors, operations, bytes per second, respectively);
-   **mode** - possible values: *avg1* (one-minute average, default), *avg5*, or *avg15*. This parameter is supported only with `type` in: sps, ops, bps.

Comments:

-   If using an update interval of three hours or more^**[2](#footnotes)**^, this item will always return '0';
-   The *sectors* and *sps* type parameters are supported only on Linux;
-   The *ops* type parameter is supported only on Linux and FreeBSD;
-   The *bps* type parameter is supported only on FreeBSD;
-   The *bytes* type parameter is supported only on FreeBSD, Solaris, AIX, OpenBSD;
-   The `mode` parameter is supported only on Linux, FreeBSD;
-   You may use relative device names (for example, `sda`) as well as an optional /dev/ prefix (for example, `/dev/sda`);
-   LVM logical volumes are supported;
-   The default values of 'type' parameter for different OSes:<br>*AIX* - operations<br>*FreeBSD* - bps<br>*Linux* - sps<br>*OpenBSD* - operations<br>*Solaris* - bytes
-   *sps*, *ops* and *bps* on supported platforms is limited to 1024 devices (1023 individual and one for *all*).

Example:

    vfs.dev.read[,operations]

[comment]: # ({/new-bde9774e})

[comment]: # ({new-beef0883})

##### vfs.dev.write[<device>,<type>,<mode>]

<br>
The disk write statistics.<br>
Return value: *Integer* - with `type` in *sectors*, *operations*, *bytes*; *Float* - with `type` in *sps*, *ops*, *bps*.<br>
[Supported platforms](#supported-platforms): Linux, FreeBSD, Solaris, AIX, OpenBSD.

Parameters:

-   **device** - disk device (default is *all* ^**[3](#footnotes)**^);
-   **type** - possible values: *sectors*, *operations*, *bytes*, *sps*, *ops*, or *bps* (*sps*, *ops*, *bps* stand for: sectors, operations, bytes per second, respectively);
-   **mode** - possible values: *avg1* (one-minute average, default), *avg5*, or *avg15*. This parameter is supported only with `type` in: sps, ops, bps.

Comments:

-   If using an update interval of three hours or more^**[2](#footnotes)**^, this item will always return '0';
-   The *sectors* and *sps* type parameters are supported only on Linux;
-   The *ops* type parameter is supported only on Linux and FreeBSD;
-   The *bps* type parameter is supported only on FreeBSD;
-   The *bytes* type parameter is supported only on FreeBSD, Solaris, AIX, OpenBSD;
-   The `mode` parameter is supported only on Linux, FreeBSD;
-   You may use relative device names (for example, `sda`) as well as an optional /dev/ prefix (for example, `/dev/sda`);
-   LVM logical volumes are supported;
-   The default values of 'type' parameter for different OSes:<br>*AIX* - operations<br>*FreeBSD* - bps<br>*Linux* - sps<br>*OpenBSD* - operations<br>*Solaris* - bytes
-   *sps*, *ops* and *bps* on supported platforms is limited to 1024 devices (1023 individual and one for *all*).

Example:

    vfs.dev.write[,operations]

[comment]: # ({/new-beef0883})

[comment]: # ({new-a28e06f9})

##### vfs.dir.count[dir,<regex_incl>,<regex_excl>,<types_incl>,<types_excl>,<max_depth>,<min_size>,<max_size>,<min_age>,<max_age>,<regex_excl_dir>]

<br>
The directory entry count.<br>
Return value: *Integer*.<br>
See [supported platforms](#supported-platforms).

Parameters:

-   **dir** - the absolute path to directory;
-   **regex_incl** - a regular [expression](/manual/regular_expressions#overview) describing the name pattern of the entity (file, directory, symbolic link) to include; include all if empty (default value);
-   **regex_excl** - a regular [expression](/manual/regular_expressions#overview) describing the name pattern of the entity (file, directory, symbolic link) to exclude; don't exclude any if empty (default value);
-   **types_incl** - directory entry types to count, possible values: *file* - regular file, *dir* - subdirectory, *sym* - symbolic link, *sock* - socket, *bdev* - block device, *cdev* - character device, *fifo* - FIFO, *dev* - synonymous with "bdev,cdev", *all* - all types (default), i.e. "file,dir,sym,sock,bdev,cdev,fifo". Multiple types must be separated with comma and quoted.
-   **types_excl** - directory entry types (see <types_incl>) to NOT count. If some entry type is in both <types_incl> and <types_excl>, directory entries of this type are NOT counted.
-   **max_depth** - the maximum depth of subdirectories to traverse:<br>**-1** (default) - unlimited,<br>**0** - no descending into subdirectories.
-   **min_size** - the minimum size (in bytes) for file to be counted. Smaller files will not be counted. [Memory suffixes](/manual/appendix/suffixes#memory_suffixes) can be used.
-   **max_size** - the maximum size (in bytes) for file to be counted. Larger files will not be counted. [Memory suffixes](/manual/appendix/suffixes#memory_suffixes) can be used.
-   **min_age** - the minimum age (in seconds) of directory entry to be counted. More recent entries will not be counted. [Time suffixes](/manual/appendix/suffixes#time_suffixes) can be used.
-   **max_age** - the maximum age (in seconds) of directory entry to be counted. Entries so old and older will not be counted (modification time). [Time suffixes](/manual/appendix/suffixes#time_suffixes) can be used.
-   **regex_excl_dir** - a regular [expression](/manual/regular_expressions#overview) describing the name pattern of the directory to exclude. All content of the directory will be excluded (in contrast to regex\_excl)

Comments:

-   Environment variables, e.g. %APP_HOME%, $HOME and %TEMP% are not supported;
-   Pseudo-directories "." and ".." are never counted;
-   Symbolic links are never followed for directory traversal;
-   Both `regex_incl` and `regex_excl` are being applied to files and directories when calculating the entry size, but are ignored when picking subdirectories to traverse (if regex_incl is “(?i)\^.+\\.zip$” and max_depth is not set, then all subdirectories will be traversed, but only the files of type zip will be counted).
-   The execution time is limited by the default timeout value in agent [configuration](/manual/appendix/config/zabbix_agentd) (3 sec). Since large directory traversal may take longer than that, no data will be returned and the item will turn unsupported. Partial count will not be returned.
-   When filtering by size, only regular files have meaningful sizes. Under Linux and BSD, directories also have non-zero sizes (a few Kb typically). Devices have zero sizes, e.g. the size of **/dev/sda1** does not reflect the respective partition size. Therefore, when using `<min_size>` and `<max_size>`, it is advisable to specify `<types_incl>` as "*file*", to avoid surprises.

Examples:

    vfs.dir.count[/dev] #monitors the number of devices in /dev (Linux)
    

[comment]: # ({/new-a28e06f9})

[comment]: # ({new-4a354b20})

##### vfs.dir.get[dir,<regex_incl>,<regex_excl>,<types_incl>,<types_excl>,<max_depth>,<min_size>,<max_size>,<min_age>,<max_age>,<regex_excl_dir>]

<br>
The directory entry list.<br>
Return value: *JSON object*.<br>
See [supported platforms](#supported-platforms).

Parameters:

-   **dir** - the absolute path to directory;
-   **regex_incl** - a regular [expression](/manual/regular_expressions#overview) describing the name pattern of the entity (file, directory, symbolic link) to include; include all if empty (default value);
-   **regex_excl** - a regular [expression](/manual/regular_expressions#overview) describing the name pattern of the entity (file, directory, symbolic link) to exclude; don't exclude any if empty (default value);
-   **types_incl** - directory entry types to list, possible values: *file* - regular file, *dir* - subdirectory, *sym* - symbolic link, *sock* - socket, *bdev* - block device, *cdev* - character device, *fifo* - FIFO, *dev* - synonymous with "bdev,cdev", *all* - all types (default), i.e. "file,dir,sym,sock,bdev,cdev,fifo". Multiple types must be separated with comma and quoted.
-   **types_excl** - directory entry types (see <types_incl>) to NOT list. If some entry type is in both <types_incl> and <types_excl>, directory entries of this type are NOT listed.
-   **max_depth** - the maximum depth of subdirectories to traverse:<br>**-1** (default) - unlimited,<br>**0** - no descending into subdirectories.
-   **min_size** - the minimum size (in bytes) for file to be listed. Smaller files will not be listed. [Memory suffixes](/manual/appendix/suffixes#memory_suffixes) can be used.
-   **max_size** - the maximum size (in bytes) for file to be listed. Larger files will not be listed. [Memory suffixes](/manual/appendix/suffixes#memory_suffixes) can be used.
-   **min_age** - the minimum age (in seconds) of directory entry to be listed. More recent entries will not be listed. [Time suffixes](/manual/appendix/suffixes#time_suffixes) can be used.
-   **max_age** - the maximum age (in seconds) of directory entry to be listed. Entries so old and older will not be listed (modification time). [Time suffixes](/manual/appendix/suffixes#time_suffixes) can be used.
-   **regex_excl_dir** - a regular [expression](/manual/regular_expressions#overview) describing the name pattern of the directory to exclude. All content of the directory will be excluded (in contrast to regex\_excl)

Comments:

-   Environment variables, e.g. %APP_HOME%, $HOME and %TEMP% are not supported;
-   Pseudo-directories "." and ".." are never listed;
-   Symbolic links are never followed for directory traversal;
-   Both `regex_incl` and `regex_excl` are being applied to files and directories when calculating the entry size, but are ignored when picking subdirectories to traverse (if regex_incl is “(?i)\^.+\\.zip$” and max_depth is not set, then all subdirectories will be traversed, but only the files of type zip will be counted).
-   The execution time is limited by the default timeout value in agent [configuration](/manual/appendix/config/zabbix_agentd) (3 sec). Since large directory traversal may take longer than that, no data will be returned and the item will turn unsupported. Partial list will not be returned.
-   When filtering by size, only regular files have meaningful sizes. Under Linux and BSD, directories also have non-zero sizes (a few Kb typically). Devices have zero sizes, e.g. the size of **/dev/sda1** does not reflect the respective partition size. Therefore, when using `<min_size>` and `<max_size>`, it is advisable to specify `<types_incl>` as "*file*", to avoid surprises.

Examples:

    vfs.dir.get[/dev] #retrieves the device list in /dev (Linux)
    

[comment]: # ({/new-4a354b20})

[comment]: # ({new-c1571da2})

##### vfs.dir.size[dir,<regex_incl>,<regex_excl>,<mode>,<max_depth>,<regex_excl_dir>]

<br>
The directory size (in bytes).<br>
Return value: *Integer*.<br>
[Supported platforms](#supported-platforms): Linux. The item may work on other UNIX-like platforms.

Parameters:

-   **dir** - the absolute path to directory;
-   **regex_incl** - a regular [expression](/manual/regular_expressions#overview) describing the name pattern of the entity (file, directory, symbolic link) to include; include all if empty (default value);
-   **regex_excl** - a regular [expression](/manual/regular_expressions#overview) describing the name pattern of the entity (file, directory, symbolic link) to exclude; don't exclude any if empty (default value);
-   **mode** - possible values: *apparent* (default) - gets apparent file sizes rather than disk usage (acts as `du -sb dir`), *disk* - gets disk usage (acts as `du -s -B1 dir`). Unlike the `du` command, the vfs.dir.size item takes hidden files in account when calculating the directory size (acts as `du -sb .[^.]* *` within dir).
-   **max_depth** - the maximum depth of subdirectories to traverse: **-1** (default) - unlimited, **0** - no descending into subdirectories.
-   **regex_excl_dir** - a regular [expression](/manual/regular_expressions#overview) describing the name pattern of the directory to exclude. All content of the directory will be excluded (in contrast to regex_excl)

Comments:

-   Only directories with at least the read permission for *zabbix* user are calculated. For directories with read permission only, the size of the directory itself is calculated. Directories with read & execute permissions are calculated including contents.
-   With large directories or slow drives this item may time out due to the Timeout setting in [agent](/manual/appendix/config/zabbix_agentd) and [server](/manual/appendix/config/zabbix_server)/[proxy](/manual/appendix/config/zabbix_proxy) configuration files. Increase the timeout values as necessary.
-   The file size limit depends on [large file support](/manual/appendix/items/large_file_support).

Examples:

    vfs.dir.size[/tmp,log] #calculates the size of all files in /tmp which contain 'log'
    vfs.dir.size[/tmp,log,^.+\.old$] #calculates the size of all files in /tmp which contain 'log', excluding files containing '.old'

[comment]: # ({/new-c1571da2})

[comment]: # ({new-44cb41f6})

##### vfs.file.cksum[file,<mode>]

<br>
The file checksum, calculated by the UNIX cksum algorithm.<br>
Return value: *Integer* - with `mode` as *crc32*, *String* - with `mode` as *md5*, *sha256*.<br>
See [#supported platforms](supported-platforms).

Parameters:

-   **file** - the full path to file;
-   **mode** - *crc32* (default), *md5*, or *sha256*.

The file size limit depends on [large file support](/manual/appendix/items/large_file_support).

Example:

    vfs.file.cksum[/etc/passwd]
    
Example of returned values (crc32/md5/sha256 respectively):

    675436101
    9845acf68b73991eb7fd7ee0ded23c44
    ae67546e4aac995e5c921042d0cf0f1f7147703aa42bfbfb65404b30f238f2dc

[comment]: # ({/new-44cb41f6})

[comment]: # ({new-0f4f51a1})

##### vfs.file.contents[file,<encoding>]

<br>
Retrieving the contents of a file.<br>
Return value: *Text*.<br>
See [supported platforms](#supported-platforms).

Parameters:

-   **file** - the full path to file;
-   **encoding** - the code page [identifier](/manual/config/items/itemtypes/zabbix_agent#encoding_settings).

Comments:

-   This item is limited to files no larger than 64KB;
-   An empty string is returned if the file is empty or contains LF/CR characters only;
-   The byte order mark (BOM) is excluded from the output.

Example:

    vfs.file.contents[/etc/passwd]

[comment]: # ({/new-0f4f51a1})

[comment]: # ({new-cb77c4fb})

##### vfs.file.exists[file,<types_incl>,<types_excl>]

<br>
Checks if the file exists.<br>
Return value: 0 - not found; 1 - file of the specified type exists.<br>
See [supported platforms](#supported-platforms).

Parameters:

-   **file** - the full path to file;
-   **types_incl** - the list of file types to include, possible values: *file* (regular file, default (if types\_excl is not set)), *dir* (directory), *sym* (symbolic link), *sock* (socket), *bdev* (block device), *cdev* (character device), *fifo* (FIFO), *dev* (synonymous with "bdev,cdev"), *all* (all mentioned types, default if types\_excl is set).
-   **types_excl** - the list of file types to exclude, see types_incl for possible values (by default no types are excluded)

Comments:

-   Multiple types must be separated with a comma and the entire set enclosed in quotes "";
-   If the same type is in both <types_incl> and <types_excl>, files of this type are excluded;
-   The file size limit depends on [large file support](/manual/appendix/items/large_file_support).

Examples:

    vfs.file.exists[/tmp/application.pid]
    vfs.file.exists[/tmp/application.pid,"file,dir,sym"]
    vfs.file.exists[/tmp/application_dir,dir]

[comment]: # ({/new-cb77c4fb})

[comment]: # ({new-12c8307e})

##### vfs.file.get[file]

<br>
Returns information about a file.<br>
Return value: *JSON object*.<br>
See [supported platforms](#supported-platforms).

Parameter:

-   **file** - the full path to file

Supported file types on UNIX-like systems: regular file, directory, symbolic link, socket, block device, character device, FIFO.

Example:

    vfs.file.get[/etc/passwd] #return a JSON with information about the /etc/passwd file (type, user, permissions, SID, uid etc)

[comment]: # ({/new-12c8307e})

[comment]: # ({new-731f3af2})

##### vfs.file.md5sum[file]

<br>
The MD5 checksum of file.<br>
Return value: Character string (MD5 hash of the file).<br>
See [supported platforms](#supported-platforms).

Parameter:

-   **file** - the full path to file

The file size limit depends on [large file support](/manual/appendix/items/large_file_support).

Example:

    vfs.file.md5sum[/usr/local/etc/zabbix_agentd.conf]

Example of returned value:

    b5052decb577e0fffd622d6ddc017e82

[comment]: # ({/new-731f3af2})

[comment]: # ({new-c191e68e})

##### vfs.file.owner[file,<ownertype>,<resulttype>]

<br>
Retrieves the owner of a file.<br>
Return value: *String*.<br>
See [supported platforms](#supported-platforms).

Parameters:

-   **file** - the full path to file;
-   **ownertype** - *user* (default) or *group* (Unix only);
-   **resulttype** - *name* (default) or *id*; for id - return uid/gid on Unix, SID on Windows.

Example:

    vfs.file.owner[/tmp/zabbix_server.log] #return the file owner of /tmp/zabbix_server.log
    vfs.file.owner[/tmp/zabbix_server.log,,id] #return the file owner ID of /tmp/zabbix_server.log

[comment]: # ({/new-c191e68e})

[comment]: # ({new-781f8e91})

##### vfs.file.permissions[file]

<br>
Return a 4-digit string containing the octal number with UNIX permissions.<br>
Return value: *String*.<br>
[Supported platforms](#supported-platforms): Linux. The item may work on other UNIX-like platforms.

Parameters:

-   **file** - the full path to file

Example:

    vfs.file.permissions[/etc/passwd] #return permissions of /etc/passwd, for example, '0644'
    

[comment]: # ({/new-781f8e91})

[comment]: # ({new-679ce640})

##### vfs.file.regexp[file,regexp,<encoding>,<start line>,<end line>,<output>]

<br>
Retrieve a string in the file.<br>
Return value: The line containing the matched string, or as specified by the optional `output` parameter.<br>
See [supported platforms](#supported-platforms).

Parameters:

-   **file** - the full path to file;
-   **regexp** - a regular [expression](/manual/regular_expressions#overview) describing the required pattern;
-   **encoding** - the code page [identifier](/manual/config/items/itemtypes/zabbix_agent#encoding_settings);
-   **start line** - the number of the first line to search (first line of file by default);
-   **end line** - the number of the last line to search (last line of file by default);
-   **output** - an optional output formatting template. The **\\0** escape sequence is replaced with the matched part of text (from the first character where match begins until the character where match ends) while an **\\N** (where N=1...9) escape sequence is replaced with Nth matched group (or an empty string if the N exceeds the number of captured groups).

Comments:

-   Only the first matching line is returned;
-   An empty string is returned if no line matched the expression;
-   The byte order mark (BOM) is excluded from the output;
-   Content extraction using the `output` parameter takes place on the agent.

Examples:

    vfs.file.regexp[/etc/passwd,zabbix]
    vfs.file.regexp[/path/to/some/file,"([0-9]+)$",,3,5,\1]
    vfs.file.regexp[/etc/passwd,"^zabbix:.:([0-9]+)",,,,\1] → getting the ID of user *zabbix*
    

[comment]: # ({/new-679ce640})

[comment]: # ({new-5216f821})

##### vfs.file.regmatch[file,regexp,<encoding>,<start line>,<end line>]

<br>
Find a string in the file.<br>
Return values: 0 - match not found; 1 - found.<br>
See [supported platforms](#supported-platforms).

Parameters:

-   **file** - the full path to file;
-   **regexp** - a regular [expression](/manual/regular_expressions#overview) describing the required pattern;
-   **encoding** - the code page [identifier](/manual/config/items/itemtypes/zabbix_agent#encoding_settings);
-   **start line** - the number of the first line to search (first line of file by default);
-   **end line** - the number of the last line to search (last line of file by default).

Comments:

-   The byte order mark (BOM) is ignored.

Example:

    vfs.file.regmatch[/var/log/app.log,error]

[comment]: # ({/new-5216f821})

[comment]: # ({new-705b33c8})

##### vfs.file.size[file,<mode>]

<br>
The file size (in bytes).<br>
Return value: *Integer*.<br>
See [supported platforms](#supported-platforms).

Parameters:

-   **file** - the full path to file;
-   **mode** - possible values: *bytes* (default) or *lines* (empty lines are counted, too).

Comments:

-   The file must have read permissions for user *zabbix*;
-   The file size limit depends on [large file support](/manual/appendix/items/large_file_support).

Example:

    vfs.file.size[/var/log/syslog]

[comment]: # ({/new-705b33c8})

[comment]: # ({new-d590c33f})

##### vfs.file.time[file,<mode>]

<br>
The file time information.<br>
Return value: *Integer* (Unix timestamp).<br>
See [supported platforms](#supported-platforms).

Parameters:

-   **file** - the full path to file;
-   **mode** - possible values:<br>*modify* (default) - the last time of modifying file content,<br>*access* - the last time of reading file,<br>*change* - the last time of changing file properties

The file size limit depends on [large file support](/manual/appendix/items/large_file_support).

Example:

    vfs.file.time[/etc/passwd,modify]

[comment]: # ({/new-d590c33f})

[comment]: # ({new-5c6f1f9c})

##### vfs.fs.discovery

<br>
The list of mounted filesystems with their type and mount options. Used for low-level discovery.<br>
Return value: *JSON object*.<br>
[Supported platforms](#supported-platforms): Linux, FreeBSD, Solaris, HP-UX, AIX, MacOS X, OpenBSD, NetBSD.

[comment]: # ({/new-5c6f1f9c})

[comment]: # ({new-dee74854})

##### vfs.fs.get

<br>
The list of mounted filesystems with their type, available disk space, inode statistics and mount options. Can be used for low-level discovery.<br>
Return value: *JSON object*.<br> 
[Supported platforms](#supported-platforms): Linux, FreeBSD, Solaris, HP-UX, AIX, MacOS X, OpenBSD, NetBSD.

Comments:

-   File systems with the inode count equal to zero, which can be the case for file systems with dynamic inodes (e.g. btrfs), are also reported;
-   See also: [Discovery of mounted filesystems](/manual/discovery/low_level_discovery/examples/mounted_filesystems).

[comment]: # ({/new-dee74854})

[comment]: # ({new-c1ad7314})

##### vfs.fs.inode[fs,<mode>]

<br>
The number or percentage of inodes.<br>
Return value: *Integer* - for number; *Float* - for percentage.<br>
See [supported platforms](#supported-platforms).

Parameters:

-   **fs** - the filesystem;
-   **mode** - possible values: *total* (default), *free*, *used*, *pfree* (free, percentage), or *pused* (used, percentage).

If the inode count equals zero, which can be the case for file systems with dynamic inodes (e.g. btrfs), the pfree/pused values will be reported as "100" and "0" respectively.

Example:

    vfs.fs.inode[/,pfree]

[comment]: # ({/new-c1ad7314})

[comment]: # ({new-2aa216f1})

##### vfs.fs.size[fs,<mode>]

<br>
The disk space in bytes or in percentage from total.<br>
Return value: *Integer* - for bytes; *Float* - for percentage.<br>
See [supported platforms](#supported-platforms).

Parameters:

-   **fs** - the filesystem;
-   **mode** - possible values: *total* (default), *free*, *used*, *pfree* (free, percentage), or *pused* (used, percentage).

Comments:

-   In case of a mounted volume, the disk space for local file system is returned;
-   The reserved space of a file system is taken into account and not included when using the *free* mode.

Example:

    vfs.fs.size[/tmp,free]

[comment]: # ({/new-2aa216f1})

[comment]: # ({new-07413e69})

##### vm.memory.size[<mode>]

<br>
The memory size in bytes or in percentage from total.<br>
Return value: *Integer* - for bytes; *Float* - for percentage.<br>
See [supported platforms](#supported-platforms).

Parameter:

-   **mode** - possible values: *total* (default), *active*, *anon*, *buffers*, *cached*, *exec*, *file*, *free*, *inactive*, *pinned*, *shared*, *slab*, *wired*, *used*, *pused* (used, percentage), *available*, or *pavailable* (available, percentage).

Comments:

-   This item accepts three categories of parameters:<br>1) *total* - total amount of memory<br>2) platform-specific memory types: *active*, *anon*, *buffers*, *cached*, *exec*, *file*, *free*, *inactive*, *pinned*, *shared*, *slab*, *wired*<br>3) user-level estimates on how much memory is used and available: *used*, *pused*, *available*, *pavailable*
-   The *active* mode parameter is supported only on FreeBSD, HP-UX, MacOS X, OpenBSD, NetBSD;
-   The *anon*, *exec*, *file* mode parameters are supported only on NetBSD;
-   The *buffers* mode parameter is supported only on Linux, FreeBSD, OpenBSD, NetBSD;
-   The *cached* mode parameter is supported only on Linux, FreeBSD, AIX, OpenBSD, NetBSD;
-   The *inactive*, *wired* mode parameters are supported only on FreeBSD, MacOS X, OpenBSD, NetBSD;
-   The *pinned* mode parameter is supported only on AIX;
-   The *shared* mode parameter is supported only on Linux 2.4, FreeBSD, OpenBSD, NetBSD;
-   See also [additional details](/manual/appendix/items/vm.memory.size_params) for this item.

Example:

    vm.memory.size[pavailable]

[comment]: # ({/new-07413e69})

[comment]: # ({new-cd3defd2})

##### web.page.get[host,<path>,<port>]

<br>
Get the content of a web page.<br>
Return value: Web page source as text (including headers).<br>
See [supported platforms](#supported-platforms).

Parameters:

-   **host** - the hostname or URL (as `scheme://host:port/path`, where only *host* is mandatory). Allowed URL schemes: *http*, *https*^**[4](#footnotes)**^. A missing scheme will be treated as *http*. If a URL is specified `path` and `port` must be empty. Specifying user name/password when connecting to servers that require authentication, for example: `http://user:password@www.example.com` is only possible with cURL support ^**[4](#footnotes)**^. Punycode is supported in hostnames.
-   **path** - the path to an HTML document (default is /);
-   **port** - the port number (default is 80 for HTTP)

Comments:

-   This item turns unsupported if the resource specified in `host` does not exist or is unavailable;
-   `host` can be a hostname, domain name, IPv4 or IPv6 address. But for IPv6 address Zabbix agent must be compiled with IPv6 support enabled.

Example:

    web.page.get[www.example.com,index.php,80]
    web.page.get[https://www.example.com]
    web.page.get[https://blog.example.com/?s=zabbix]
    web.page.get[localhost:80]
    web.page.get["[::1]/server-status"]

[comment]: # ({/new-cd3defd2})

[comment]: # ({new-ef902e6e})

##### web.page.perf[host,<path>,<port>]

<br>
The loading time of a full web page (in seconds).<br>
Return value: *Float*.<br>
See [supported platforms](#supported-platforms).

Parameters:

-   **host** - the hostname or URL (as `scheme://host:port/path`, where only *host* is mandatory). Allowed URL schemes: *http*, *https*^**[4](#footnotes)**^. A missing scheme will be treated as *http*. If a URL is specified `path` and `port` must be empty. Specifying user name/password when connecting to servers that require authentication, for example: `http://user:password@www.example.com` is only possible with cURL support ^**[4](#footnotes)**^. Punycode is supported in hostnames.
-   **path** - the path to an HTML document (default is /);
-   **port** - the port number (default is 80 for HTTP)

Comments:

-   This item turns unsupported if the resource specified in `host` does not exist or is unavailable;
-   `host` can be a hostname, domain name, IPv4 or IPv6 address. But for IPv6 address Zabbix agent must be compiled with IPv6 support enabled.

Example:

    web.page.perf[www.example.com,index.php,80]
    web.page.perf[https://www.example.com]

[comment]: # ({/new-ef902e6e})

[comment]: # ({new-a78ae078})

##### web.page.regexp[host,<path>,<port>,regexp,<length>,<output>]

<br>
Find a string on the web page.<br>
Return value: The matched string, or as specified by the optional `output` parameter.<br>
See [supported platforms](#supported-platforms).

Parameters:

-   **host** - the hostname or URL (as `scheme://host:port/path`, where only *host* is mandatory). Allowed URL schemes: *http*, *https*^**[4](#footnotes)**^. A missing scheme will be treated as *http*. If a URL is specified `path` and `port` must be empty. Specifying user name/password when connecting to servers that require authentication, for example: `http://user:password@www.example.com` is only possible with cURL support ^**[4](#footnotes)**^. Punycode is supported in hostnames.
-   **path** - the path to an HTML document (default is /);
-   **port** - the port number (default is 80 for HTTP)
-   **regexp** - a regular [expression](/manual/regular_expressions#overview) describing the required pattern;
-   **length** - the maximum number of characters to return;
-   **output** - an optional output formatting template. The **\\0** escape sequence is replaced with the matched part of text (from the first character where match begins until the character where match ends) while an **\\N** (where N=1...9) escape sequence is replaced with Nth matched group (or an empty string if the N exceeds the number of captured groups).

Comments:

-   This item turns unsupported if the resource specified in `host` does not exist or is unavailable;
-   `host` can be a hostname, domain name, IPv4 or IPv6 address. But for IPv6 address Zabbix agent must be compiled with IPv6 support enabled.
-   Content extraction using the `output` parameter takes place on the agent.

Example:

    web.page.regexp[www.example.com,index.php,80,OK,2]
    web.page.regexp[https://www.example.com,,,OK,2]|

[comment]: # ({/new-a78ae078})

[comment]: # ({new-3030d112})

##### agent.hostmetadata

<br>
The agent host metadata.<br>
Return value: *String*.<br>
See [supported platforms](#supported-platforms).

Returns the value of HostMetadata or HostMetadataItem parameters, or empty string if none are defined.

[comment]: # ({/new-3030d112})

[comment]: # ({new-e20fbf57})

##### agent.hostname

<br>
The agent host name.<br>
Return value: *String*.<br>
See [supported platforms](#supported-platforms).

Returns:

-   As passive check - the name of the first host listed in the Hostname parameter of the agent configuration file;
-   As active check - the name of the current hostname.

[comment]: # ({/new-e20fbf57})

[comment]: # ({new-3a10c7e2})

##### agent.ping

<br>
The agent availability check.<br>
Return value: Nothing - unavailable; 1 - available.<br>
See [supported platforms](#supported-platforms).

Use the **nodata()** trigger function to check for host unavailability.

[comment]: # ({/new-3a10c7e2})

[comment]: # ({new-d276be7d})

##### agent.variant

<br>
The variant of Zabbix agent (Zabbix agent or Zabbix agent 2).<br>
Return value: 1 - Zabbix agent; 2 - Zabbix agent 2.<br>
See [supported platforms](#supported-platforms).

[comment]: # ({/new-d276be7d})

[comment]: # ({new-f391e3aa})

##### agent.version

<br>
The version of Zabbix agent.<br>
Return value: *String*.<br>
See [supported platforms](#supported-platforms).

Example of returned value: 

    6.0.3

[comment]: # ({/new-f391e3aa})

[comment]: # ({new-31497288})

##### zabbix.stats[<ip>,<port>]

<br>
Returns a set of Zabbix server or proxy internal metrics remotely.<br>
Return value: *JSON object*.<br>
See [supported platforms](#supported-platforms).

Parameters:

-   **ip** - the IP/DNS/network mask list of servers/proxies to be remotely queried (default is 127.0.0.1);
-   **port** - the port of server/proxy to be remotely queried (default is 10051)

Comments:

-   A selected set of internal metrics is returned by this item. For details, see [Remote monitoring of Zabbix stats](/manual/appendix/items/remote_stats#exposed_metrics);
-   Note that the stats request will only be accepted from the addresses listed in the 'StatsAllowedIP' [server](/manual/appendix/config/zabbix_server)/[proxy](/manual/appendix/config/zabbix_proxy) parameter on the target instance.

[comment]: # ({/new-31497288})

[comment]: # ({new-ea79fb35})

##### zabbix.stats[<ip>,<port>,queue,<from>,<to>]

<br>
Returns the number of monitored items in the queue which are delayed on Zabbix server or proxy remotely.<br>
Return value: *JSON object*.<br>
See [supported platforms](#supported-platforms).

Paramaters:

-   **ip** - the IP/DNS/network mask list of servers/proxies to be remotely queried (default is 127.0.0.1);
-   **port** - the port of server/proxy to be remotely queried (default is 10051)
-   **queue** - constant (to be used as is)
-   **from** - delayed by at least (default is 6 seconds)
-   **to** - delayed by at most (default is infinity)

Note that the stats request will only be accepted from the addresses listed in the 'StatsAllowedIP' [server](/manual/appendix/config/zabbix_server)/[proxy](/manual/appendix/config/zabbix_proxy) parameter on the target instance.

[comment]: # ({/new-ea79fb35})

[comment]: # ({new-cefe2e1b})
#### Chaves de itens compatíveis

A tabela fornece detalhes sobre as chaves de item que você pode usar 
com os itens do agente Zabbix.

Veja também:

- [Itens suportados por plataforma](/manual/appendix/items/supported_by_platform)
- [Chaves de item suportadas pelo Zabbix Agent 2](/manual/config/items/itemtypes/zabbix_agent/zabbix_agent2)
- [Chaves de item específicas para agente Windows](/manual/config/items/itemtypes/zabbix_agent/win_keys)
- [Nível de permissão mínimo para itens do agente Windows](/manual/appendix/items/win_permissions)


#### Chaves de itens compatíveis

A tabela fornece detalhes sobre as chaves de item que você pode usar 
com os itens do agente Zabbix.

Veja também:

- [Itens suportados por plataforma](/manual/appendix/items/supported_by_platform)
- [Chaves de item suportadas pelo Zabbix Agent 2](/manual/config/items/itemtypes/zabbix_agent/zabbix_agent2)
- [Chaves de item específicas para agente Windows](/manual/config/items/itemtypes/zabbix_agent/win_keys)
- [Nível de permissão mínimo para itens do agente Windows](/manual/appendix/items/win_permissions)



|Chave|<|<|<|<|
|---|-|-|-|-|
|<|**Descrição**|**Valor de retorno**|**Parâmetros**|**Comentários**|
|agent.hostmetadata|<|<|<|<|
|<|Metadados do host do agente.|String||Retorna o valor dos parâmetros HostMetadata ou HostMetadataItem, ou um texto vazio se nenhum for definido.<br><br>Suportado desde o Zabbix 6.0.|
|agent.hostname|<|<|<|<|
|<|Nome do host do agente.|String| |Retorno:<br>Como verificação passiva - o nome do primeiro host listado no parâmetro Hostname do arquivo de configuração do agente;<br>Como verificação ativa - o nome do host atual.|
|agent.ping|<|<|<|<|
|<|Verificação de disponibilidade do agente.|Nada - indisponível<br><br>1 - disponível| |Use a função de gatilho **nodata()** para verificar pela indisponibilidade do host.|
|agent.variant|<|<|<|<|
|<|Variante do agente Zabbix (Zabbix Agent ou Zabbix Agent 2).|Inteiro| |Exemplo de valor retornado:<br>1 - Zabbix Agent<br>2 - Zabbix Agent 2|
|agent.version|<|<|<|<|
|<|Versão do agente Zabbix.|String| |Exemplo de valor retornado:<br>6.0.3|
|kernel.maxfiles|<|<|<|<|
|<|Número máximo de arquivos abertos suportado pelo SO.|Inteiro| | |
|kernel.maxproc|<|<|<|<|
|<|Número máximo de processos suportado pelo SO.|Inteiro| | |
|kernel.openfiles|<|<|<|<|
|<|Retorna o número atual de descritores de arquivos abertos.|Inteiro||Este item é suportado no Linux desde o Zabbix 6.0.|
|log\[file,\<regexp\>,\<encoding\>,\<maxlines\>,\<mode\>,\<output\>,\<maxdelay\>,\<options\>,\<persistent\_dir\>\]|<|<|<|<|
|<|Monitoramento de um arquivo de log.|Log|**file** - caminho completo e nome do arquivo de log<br>**regexp** - [expressão regular](/manual/regular_expressions#overview) descrevendo o padrão requerido<br>**encoding** - [identificador](/manual/config/items/itemtypes/zabbix_agent#encoding_settings) de código da página<br>**maxlines** - número máximo de novas linhas por segundo que o agente enviará para o Zabbix Server ou Proxy. Este parâmetro sobrescreve o valor 'MaxLinesPerSecond' no [zabbix\_agentd.conf](/manual/appendix/config/zabbix_agentd)<br>**mode** (desde a versão 2.0) - valores possíveis:<br>*all* (padrão) - tudo, *skip* - pula o processamento de dados mais antigos (afeta apenas itens recém-criados).<br>**output** (desde a versão 2.2) - um modelo de formatação de saída opcional. A sequência de escape **\\0** é substituída pela parte de texto correspondente (do primeiro caracter onde a correspondência inicia até o caracter onde ela termina) enquanto uma sequência de escape **\\N** (onde N=1...9) é substituída com o N-ésimo grupo correspondente (ou um texto vazio se N excede o número de grupos capturados).<br>**maxdelay** (desde a versão 3.2) - tempo de espera máximo em segundos. Tipo: número flutuante. Valores: 0 - (padrão) nunca ignorar linhas do arquivo de log; > 0.0 - ignorar linhas mais antigas com o objetivo de analisar as linhas mais recentes dentro do valor de "maxdelay". Leia as notas sobre [espera máxima](log_items#using_maxdelay_parameter) antes de usá-la!<br>**options** (desde a versão 4.4.7) - opções adicionais:<br>*mtime-noreread* - registros não únicos, com releitura apenas se o tamanho do arquivo for alterado (ignora alteração de data). (Este parâmetro está em desuso desde a versão 5.0.2, porque agora mtime é ignorado.)<br>**persistent\_dir** (desde a versão 5.0.18, 5.4.9, apenas no zabbix\_agentd para sistemas Unix; não suportado com Agent2) - endereço absoluto do diretório onde armazenar arquivos persistentes. Veja também notas adicionais sobre [arquivos persistentes](log_items#notes-on-persistent-files-for-log-items).|O item deve ser configurado como uma [verificação ativa](/manual/appendix/items/activepassive#active_checks).<br>Se o arquivo não for encontrado ou o permissionamento não conceder o acesso necessário, o item se torna não suportado.<br><br>Se `output` for deixado em branco - a linha completa contendo o texto correspondente é retornada. Note que todos os tipos globais de expressão regular, exceto 'Result is TRUE', sempre retornam a linha correspondente completa e o parâmetro `output` é ignorado.<br><br>A extração de conteúdo usando o parâmetro `output` ocorre no agente.<br><br>Exemplos:<br>=> log\[/var/log/syslog\]<br>=> log\[/var/log/syslog,error\]<br>=> log\[/home/zabbix/logs/logfile,,,100\]<br><br>*Usando o parâmetro `output` para extrair um número de um registro de log:*<br>=> log\[/app1/app.log,"task run \[0-9.\]+ sec, processed (\[0-9\]+) records, \[0-9\]+ errors",,,,\\1\] → corresponderá a um registro de log "2015-11-13 10:08:26 task run 6.08 sec, processed 6080 records, 0 errors" e enviará apenas '6080' para o Server. Por um valor numérico estar sendo enviado, o "Tipo de informação" deste item pode ser configurada como "Numérico (unsigned)" e o valor pode ser usado em gráficos, gatilhos, etc.<br><br>*Usando o parâmetro `output` para reescrever um registro de log antes de enviar para o Server:*<br>=> log\[/app1/app.log,"(\[0-9 :-\]+) task run (\[0-9.\]+) sec, processed (\[0-9\]+) records, (\[0-9\]+) errors",,,,"\\1 RECORDS: \\3, ERRORS: \\4, DURATION: \\2"\] → corresponderá a um registro de log "2015-11-13 10:08:26 task run 6.08 sec, processed 6080 records, 0 errors" e enviará um registro modificado "2015-11-13 10:08:26 RECORDS: 6080, ERRORS: 0, DURATION: 6.08" ao Server.<br><br>Consulte também informações adicionais sobre [monitoramento de log](log_items).|
|log.count\[file,\<regexp\>,\<encoding\>,\<maxproclines\>,\<mode\>,\<maxdelay\>,\<options\>,\<persistent\_dir\>\]|<|<|<|<|
|<|Contagem de linhas correspondentes em um arquivo de log monitorado.|Inteiro|**file** - caminho completo e nome do arquivo de log<br>**regexp** - [expressão regular](/manual/regular_expressions#overview) descrevendo o padrão necessário<br>**encoding** - [identificador](/manual/config/items/itemtypes/zabbix_agent#encoding_settings) de código de página<br>**maxproclines** - número máximo de novas linhas por segundo que o agente analisará (não pode exceder 10000). O valor padrão é 10\*'MaxLinesPerSecond' no [zabbix\_agentd.conf](/manual/appendix/config/zabbix_agentd).<br>**mode** - valores possíveis:<br>*all* (padrão) - tudo, *skip* - pula o processamento de dados mais antigos (afeta apenas itens recém-criados).<br>**maxdelay** - tempo de espera máximo em segundos. Tipo: número flutuante. Valores: 0 - (padrão) nunca ignorar linhas do arquivo de log; > 0.0 - ignorar linhas mais antigas com o objetivo de analisar as linhas mais recentes dentro do valor de "maxdelay". Leia as notas sobre [espera máxima](log_items#using_maxdelay_parameter) antes de usá-la!<br>**options** (desde a versão 4.4.7) - opções adicionais:<br>*mtime-noreread* - registros não únicos, com releitura apenas se o tamanho do arquivo for alterado (ignora alteração de data). (Este parâmetro está em desuso desde a versão 5.0.2, porque agora mtime é ignorado.)<br>**persistent\_dir** (desde a versão 5.0.18, 5.4.9, apenas no zabbix\_agentd para sistemas Unix; não suportado com Agent2) - endereço absoluto do diretório onde armazenar arquivos persistentes. Veja também notas adicionais sobre [arquivos persistentes](log_items#notes-on-persistent-files-for-log-items).|O item deve ser configurado como uma [verificação ativa](/manual/appendix/items/activepassive#active_checks).<br>Se o arquivo não for encontrado ou o permissionamento não conceder o acesso necessário, o item se torna não suportado.<br><br>Consulte também informações adicionais sobre [monitoramento de log](log_items).<br><br>Este item não é suportado para logs de Evento do Windows.<br><br>Suportado desde a versão Zabbix 3.2.0.|
|logrt\[file\_regexp,\<regexp\>,\<encoding\>,\<maxlines\>,\<mode\>,\<output\>,\<maxdelay\>,\<options\>,\<persistent\_dir\>\]|<|<|<|<|
|<|Monitoramento de um arquivo de log que é rotacionado.|Log|**file\_regexp** - caminho absoluto para o arquivo com o nome do arquivo descrito por uma [expressão regular](/manual/regular_expressions#overview). *Note* que apenas o nome do arquivo é uma expressão regular<br>**regexp** - [expressão regular](/manual/regular_expressions#overview) descrevendo o padrão de conteúdo necessário<br>**encoding** - [identificador](/manual/config/items/itemtypes/zabbix_agent#encoding_settings) de código de página<br>**maxlines** - número máximo de novas linhas por segundo que o agente enviará para o Zabbix Server ou Proxy. Este parâmetro sobrescreve o valor de 'MaxLinesPerSecond' no [zabbix\_agentd.conf](/manual/appendix/config/zabbix_agentd)<br>**mode** (desde a versão 2.0) - valores possíveis:<br>*all* (default) - tudo, *skip* - pula o processamento de dados mais antigos (afeta apenas itens recém-criados).<br>**output** (desde a versão 2.2) - um modelo de formatação de saída opcional. A sequência de escape **\\0** é substituída pela parte de texto correspondente (do primeiro caracter onde a correspondência inicia até o caracter onde ela termina) enquanto uma sequência de escape **\\N** (onde N=1...9) é substituída com o N-ésimo grupo correspondente (ou um texto vazio se N excede o número de grupos capturados).<br>**maxdelay** (desde a versão 3.2) - tempo de espera máximo em segundos. Tipo: número flutuante. Valores: 0 - (padrão) nunca ignorar linhas do arquivo de log; > 0.0 - ignorar linhas mais antigas com o objetivo de analisar as linhas mais recentes dentro do valor de "maxdelay". Leia as notas sobre [espera máxima](log_items#using_maxdelay_parameter) antes de usá-la!<br>**options** (desde a versão 4.0; as opções *mtime-reread*, *mtime-noreread* desde a versão 4.4.7) - tipo de rotação de arquivo de log e outras opções. Valores possíveis:<br>*rotate* (padrão),<br>*copytruncate* - note que *copytruncate* não pode ser usado junto com *maxdelay*. Neste caso *maxdelay* deve ser 0 ou não declarado; veja notas para [copytruncate](log_items#notes_on_handling_copytruncate_log_file_rotation),<br>*mtime-reread* - registros não únicos, releitura se houver mudança de data ou tamanho do arquivo (padrão),<br>*mtime-noreread* - registros não únicos, releitura apenas se o tamanho mudar (ignora alteração de data).<br>**persistent\_dir** (desde a versão 5.0.18, 5.4.9, apenas no zabbix\_agentd para sistemas Unix; não suportado com Agent2) - endereço absoluto do diretório onde armazenar arquivos persistentes. Veja também notas adicionais sobre [arquivos persistentes](log_items#notes-on-persistent-files-for-log-items).|O item deve ser configurado como uma [verificação ativa](/manual/appendix/items/activepassive#active_checks).<br>A rotação de log é baseada na última data de modificação dos arquivos.<br><br>Note que o logrt foi construído para trabalhar com um arquivo de log ativo atual, com vários outros arquivos rotacionados correspondentes inativos. Se, por exemplo, um diretório tem muitos arquivos de log ativos, um item logrt deve ser criado para cada um deles. Caso contrário se um item logrt captar arquivos demais pode haver exaustão de memória e quebra do monitoramento.<br><br>Se `output` for deixado em branco - a linha completa contendo o texto correspondente é retornada. Note que todos os tipos globais de expressão regular, exceto 'Result is TRUE', sempre retornam a linha correspondente completa e o parâmetro `output` é ignorado.<br><br>A extração de conteúdo usando o parâmetro `output` ocorre no agente.<br><br>Exemplos:<br>=> logrt\["/home/zabbix/logs/\^logfile\[0-9\]{1,3}$",,,100\] → corresponderá a um arquivo como "logfile1" (não aceitará ".logfile1")<br>=> logrt\["/home/user/\^logfile\_.\*\_\[0-9\]{1,3}$","pattern\_to\_match","UTF-8",100\] → coletará dados de arquivos como "logfile\_abc\_1" ou "logfile\_\_001".<br><br>*Usando parâmetro `output` para extrair um número de um registro de log:*<br>=> logrt\[/app1/\^test.\*log$,"task run \[0-9.\]+ sec, processed (\[0-9\]+) records, \[0-9\]+ errors",,,,\\1\] → corresponderá a um registro de log "2015-11-13 10:08:26 task run 6.08 sec, processed 6080 records, 0 errors" e enviará apenas '6080' para o Server. Pelo fato de um valor numérico estar sendo enviado, o "Tipo de informação" para este item pode ser configurado para "Numérico (unsigned)" e o valor pode ser usado em gráficos, gatilhos, etc.<br><br>*Usando o parâmetro `output` para reescrever um registro de log antes de enviar para o Server:*<br>=> logrt\[/app1/\^test.\*log$,"(\[0-9 :-\]+) task run (\[0-9.\]+) sec, processed (\[0-9\]+) records, (\[0-9\]+) errors",,,,"\\1 RECORDS: \\3, ERRORS: \\4, DURATION: \\2"\] → corresponderá a um registro de log "2015-11-13 10:08:26 task run 6.08 sec, processed 6080 records, 0 errors" e enviará um registro modificado "2015-11-13 10:08:26 RECORDS: 6080, ERRORS: 0, DURATION: 6.08" para o Server.<br><br>Consulte também informações adicionais sobre [monitoramento de log](log_items).|
|logrt.count\[file\_regexp,\<regexp\>,\<encoding\>,\<maxproclines\>,\<mode\>,\<maxdelay\>,\<options\>,\<persistent\_dir\>\]|<|<|<|<|
|<|Contagem de linhas correspondidas em um arquivo de log monitorado que é rotacionado.|Inteiro|**file\_regexp** - caminho absoluto para o arquivo com o nome do arquivo descrito por uma [expressão regular](/manual/regular_expressions#overview).<br>**regexp** - [expressão regular](/manual/regular_expressions#overview) descrevendo o padrão de conteúdo necessário<br>**encoding** - [identificador](/manual/config/items/itemtypes/zabbix_agent#encoding_settings) de código de página<br>**maxproclines** - número máximo de novas linhas por segundo que o agente analisará (não pode exceder 10000). O valor padrão é 10\*'MaxLinesPerSecond' no [zabbix\_agentd.conf](/manual/appendix/config/zabbix_agentd).<br>**mode** - valores possíveis:<br>*all* (default) - tudo, *skip* - pula o processamento de dados mais antigos (afeta apenas itens recém-criados).<br>**maxdelay** - tempo de espera máximo em segundos. Tipo: número flutuante. Valores: 0 - (padrão) nunca ignorar linhas do arquivo de log; > 0.0 - ignorar linhas mais antigas com o objetivo de analisar as linhas mais recentes dentro do valor de "maxdelay". Leia as notas sobre [espera máxima](log_items#using_maxdelay_parameter) antes de usá-la!<br>**options** (desde a versão 4.0; as opções *mtime-reread*, *mtime-noreread* desde a versão 4.4.7) - tipo de rotação de arquivo de log e outras opções. Valores possíveis:<br>*rotate* (padrão),<br>*copytruncate* - note que *copytruncate* não pode ser usado junto com *maxdelay*. Neste caso *maxdelay* deve ser 0 ou não declarado; veja notas para [copytruncate](log_items#notes_on_handling_copytruncate_log_file_rotation),<br>*mtime-reread* - registros não únicos, releitura se houver mudança de data ou tamanho do arquivo (padrão),<br>*mtime-noreread* - registros não únicos, releitura apenas se o tamanho mudar (ignora alteração de data).<br>**persistent\_dir** (desde as versões 5.0.18, 5.4.9, apenas para zabbix\_agentd em sistemas Unix; são suportado no Agent2) - endereço absoluto do diretório onde armazenar arquivos persistentes. Consulte também notas adicionais sobre [arquivos persistentes](log_items#notes-on-persistent-files-for-log-items).|Este item deve ser configurado como uma [verificação ativa](/manual/appendix/items/activepassive#active_checks).<br>A rotação de log é baseada na última modificação de data dos arquivos.<br><br>Consulte também informações adicionais sobre [monitoramento de log](log_items).<br><br>Este item não é suportado para logs de Evento do Windows.<br><br>Suportado desde a versão Zabbix 3.2.0.|
|modbus.get\[endpoint,<slave id>,<function>,<address>,<count>,<type>,<endianness>,<offset>\]|<|<|<|<|
|<|Lê dados Modbus.|Objeto JSON|**endpoint** - ponto de conexão definido como `protocolo://string_de_conexão`<br>**slave id** - ID secundário<br>**function** - função Modbus<br>**address** - endereço do primeiro registro, bobina ou contato<br>**count** - número de registros a serem lidos<br>**type** - tipo de dado<br>**endianness** - configuração de extremidade<br>**offset** - número de registros, começando de 'address', do qual os resultados serão descartados.<br><br>Veja uma [descrição detalhada](/manual/appendix/items/modbus) dos parâmetros.|Suportado desde o Zabbix 5.2.0.|
|net.dns\[<ip>,name,<type>,<timeout>,<count>,<protocol>\]|<|<|<|<|
|<|Verifica se o serviço de DNS está no ar.|0 - DNS indisponível (servidor não respondeu ou resolução de DNS falhou)<br><br>1 - DNS disponível|**ip** - endereço IP do servidor DNS (deixe em branco para servidor de DNS padrão, ignorado no Windows)<br>**name** - Nome do DNS a ser questionado<br>**type** - tipo do registro a ser questionado (padrão é *SOA*)<br>**timeout** (ignorado no Windows) - tempo máximo para a requisição em segundos (o padrão é 1 segundo)<br>**count** (ignorado no Windows) - número de tentativas para a requisição (padrão é 2)<br>**protocol** (desde a versão 3.0) - o protocolo utilizado para executar as requisições DNS: *udp* (padrão) ou *tcp*|Exemplo:<br>=> net.dns\[8.8.8.8,example.com,MX,2,1\]<br><br>Os valores possíveis para `type` (tipo) são:<br>*ANY*, *A*, *NS*, *CNAME*, *MB*, *MG*, *MR*, *PTR*, *MD*, *MF*, *MX*, *SOA*, *NULL*, *WKS* (exceto para Windows), *HINFO*, *MINFO*, *TXT*, *SRV*<br><br>Nomes de domínio internacionalizados não são suportados, por favor utilize nomes com codificação IDNA em vez disso.<br><br>O tipo de registro SRV é suportado desde o Zabbix 1.8.6 (Unix) e 2.0.0 (Windows).<br><br>Nomenclatura anterior a Zabbix 2.0 (ainda suportado): *net.tcp.dns*|
|net.dns.record\[<ip>,name,<type>,<timeout>,<count>,<protocol>\]|<|<|<|<|
|<|Executa uma requisição DNS.|String de caracteres com o tipo de informação requerida|**ip** - endereço IP do servidor DNS (deixe em branco para servidor de DNS padrão, ignorado no Windows)<br>**name** - Nome do DNS a ser questionado<br>**type** - tipo do registro a ser questionado (padrão é *SOA*)<br>**timeout** (ignorado no Windows) - tempo máximo para a requisição em segundos (o padrão é 1 segundo)<br>**count** (ignorado no Windows) - número de tentativas para a requisição (padrão é 2)<br>**protocol** (desde a versão 3.0) - o protocolo utilizado para executar as requisições DNS: *udp* (padrão) ou *tcp*|Exemplo:<br>=> net.dns\[8.8.8.8,example.com,MX,2,1\]<br><br>Os valores possíveis para `type` (tipo) são:<br>*ANY*, *A*, *NS*, *CNAME*, *MB*, *MG*, *MR*, *PTR*, *MD*, *MF*, *MX*, *SOA*, *NULL*, *WKS* (exceto para Windows), *HINFO*, *MINFO*, *TXT*, *SRV*<br><br>Nomes de domínio internacionalizados não são suportados, por favor utilize nomes com codificação IDNA em vez disso.<br><br>O tipo de registro SRV é suportado desde o Zabbix 1.8.6 (Unix) e 2.0.0 (Windows).<br><br>Nomenclatura anterior a Zabbix 2.0 (ainda suportado): *net.tcp.dns.query*|
|net.if.collisions\[if\]|<|<|<|<|
|<|Número de colisões atrasadas (out-of-window).|Inteiro|**if** - nome da interface de rede| |
|net.if.discovery|<|<|<|<|
|<|Lista de interfaces de rede. Usada para descobertas de baixo-nivel.|Objeto JSON| |Suportado desde o Zabbix 2.0.<br><br>No FreeBSD, OpenBSD e NetBSD suportado desde o Zabbix 2.2.<br><br>Algumas versões de Windows (por exemplo, Server 2008) podem exigir a instalação das últimas atualizações para suportar caracteres não-ASCII nos nomes das interfaces.|
|net.if.in\[if,<mode>\]|<|<|<|<|
|<|Estatísticas de tráfego de entrada na interface de rede.|Inteiro|**if** - nome da interface de rede (Unix); descrição completa da interface de rede ou endereço IPv4; ou, se entre chaves,  GUID da interface de rede (Windows)<br>**mode** - valores possíveis:<br>*bytes* - número de bytes (padrão)<br>*packets* - número de pacotes<br>*errors* - número de erros<br>*dropped* - número de pacotes descartados<br>*overruns (fifo)* - o número de erros no buffer FIFO <br>*frame* - o número de pacotes com erros de frame (framing errors)<br>*compressed* - o número de pacotes compactados recebidos ou transmitidos pelo driver de dispositivo<br>*multicast* - o número de frames multicast recebidos pelo driver de dispositivo|No Windows, o item obtém valores de contadores 64-bit se disponível. Contadores de estatística de interface 64-bit foram introduzidos no Windows Vista e Windows Server 2008. Se contadores 64-bit não estão disponíveis, o agente usa contadores 32-bit.<br><br>Nomes de interface Multi-byte são suportados no Windows desde o Zabbix 1.8.6.<br><br>Exemplos:<br>=> net.if.in\[eth0,errors\]<br>=> net.if.in\[eth0\]<br><br>Você pode obter descrições de interface de rede no Windows com itens net.if.discovery ou net.if.list.<br><br>Você deve usar esta chave com a etapa de pré-processamento *Alterações por segundo* de forma a obter as estatísticas de bytes por segundo.|
|net.if.out\[if,<mode>\]|<|<|<|<|
|<|Estatísticas de tráfego de saída na interface de rede.|Inteiro|**if** - nome da interface de rede (Unix); descrição completa da interface de rede ou endereço IPv4; ou, se entre chaves,  GUID da interface de rede (Windows)<br>**mode** - valores possíveis:<br>*bytes* - número de bytes (padrão)<br>*packets* - número de pacotes<br>*errors* - número de erros<br>*dropped* - número de pacotes descartados<br>*overruns (fifo)* - o número de erros no buffer FIFO<br>*collisions (colls)* - o número de colisões detectadas na interface<br>*carrier* - o número de perdas no sensor de carrier detectados pelo driver de dispositivo<br>*compressed* - o número de pacotes compactados recebidos ou transmitidos pelo driver de dispositivo|No Windows, o item obtém valores de contadores 64-bit se disponível. Contadores de estatística de interface 64-bit foram introduzidos no Windows Vista e Windows Server 2008. Se contadores 64-bit não estão disponíveis, o agente usa contadores 32-bit.<br><br>Nomes de interface Multi-byte são suportados no Windows desde o Zabbix 1.8.6.<br><br>Exemplos:<br>=> net.if.out\[eth0,errors\]<br>=> net.if.out\[eth0\]<br><br>Você pode obter descrições de interface de rede no Windows com itens net.if.discovery ou net.if.list.<br><br>Você deve usar esta chave com a etapa de pré-processamento *Alterações por segundo* de forma a obter as estatísticas de bytes por segundo.|
|net.if.total\[if,<mode>\]|<|<|<|<|
|<|Soma das estatísticas de tráfego de entrada e saída na interface de rede.|Inteiro|**if** - nome da interface de rede (Unix); descrição completa da interface de rede ou endereço IPv4; ou, se entre chaves,  GUID da interface de rede (Windows)<br>**mode** - valores possíveis:<br>*bytes* - número de bytes (padrão)<br>*packets* - número de pacotes<br>*errors* - número de erros<br>*dropped* - número de pacotes descartados<br>*overruns (fifo)* - o número de erros no buffer FIFO<br>*compressed* - o número de pacotes compactados recebidos ou transmitidos pelo driver de dispositivo|No Windows, o item obtém valores de contadores 64-bit se disponível. Contadores de estatística de interface 64-bit foram introduzidos no Windows Vista e Windows Server 2008. Se contadores 64-bit não estão disponíveis, o agente usa contadores 32-bit.<br><br>Exemplos:<br>=> net.if.total\[eth0,errors\]<br>=> net.if.total\[eth0\]<br><br>Você pode obter descrições de interface de rede no Windows com itens net.if.discovery ou net.if.list.<br><br>Você deve usar esta chave com a etapa de pré-processamento *Alterações por segundo* de forma a obter as estatísticas de bytes por segundo.<br><br>Note que pacotes descartados são suportados apenas se ambos net.if.in e net.if.out funcionarem para pacotes descartados em sua plataforma.|
|net.tcp.listen\[port\]|<|<|<|<|
|<|Verifica se esta porta TCP está ouvindo (estado LISTEN).|0 - a porta não está ouvindo<br><br>1 - a porta está ouvindo|**port** - número da porta TCP|Exemplo:<br>=> net.tcp.listen\[80\]<br><br>Suportado no Linux desde o Zabbix 1.8.4<br><br>Desde o Zabbix 3.0.0, nos kernels Linux 2.6.14 e superiores, a informação sobre sockets TCP na escuta é obtida pela interface NETLINK do kernel, se possível. Caso contrário, a informação é obtida dos arquivos /proc/net/tcp e /proc/net/tcp6.|
|net.tcp.port\[<ip>,port\]|<|<|<|<|
|<|Verifica se é possível estabelecer conexão TCP na porta especificada.|0 - não é possível conectar<br><br>1 - é possível conectar|**ip** - endereço IP ou nome DNS (padrão é 127.0.0.1)<br>**port** - número da porta|Exemplo:<br>=> net.tcp.port\[,80\] → pode ser usado para testar a disponibilidade de servidores web rodando na porta 80.<br><br>Para teste simples de performance TCP use net.tcp.service.perf\[tcp,<ip>,<port>\]<br><br>Note que estas verificações podem resultar em mensagens adicionais nos arquivos de log dos daemons do sistema (sessões SMTP e SSH sendo logadas comumente).|
|net.tcp.service\[service,<ip>,<port>\]|<|<|<|<|
|<|Verifica se o serviço está em execução e aceitando conexões TCP.|0 - serviço está parado<br><br>1 - serviço está em execução|**service** - qualquer um de:<br>*ssh*, *ldap*, *smtp*, *ftp*, *http*, *pop*, *nntp*, *imap*, *tcp*, *https*, *telnet* (consulte [detalhes](/manual/appendix/items/service_check_details))<br>**ip** - endereço IP (padrão é 127.0.0.1)<br>**port** - número da porta (por padrão a porta de serviço padrão é usada)|Exemplo:<br>=> net.tcp.service\[ftp,,45\] → pode ser usado para testar a disponibilidade de um servidor FTP na porta 45/TCP.<br><br>Note que estas verificações podem resultar em mensagens adicionais nos arquivos de log dos daemons do sistema (sessões SMTP e SSH sendo logadas comumente).<br><br>Verificação de protocolos criptografados (como IMAP na porta 993 ou POP na prota 995) não é suportado atualmente. Como contingência, por favor use net.tcp.port para verificações como estas.<br><br>Verificações LDAP e HTTPS no Windows são suportadas apenas com Zabbix Agent 2.<br><br>Note que a verificação telnet procura por um prompt de login (':' no final).<br><br>Consulte também os [problemas conhecidos](/manual/installation/known_issues#https_checks) para verificação de serviço HTTPS.<br><br>Os serviços *https* e *telnet* são suportados desde o Zabbix 2.0.|
|net.tcp.service.perf\[service,<ip>,<port>\]|<|<|<|<|
|<|Verifica performance do serviço de TCP.|0 - serviço está parado<br><br>segundos - o número de segundos gastos enquanto conectando-se ao serviço|**service** - qualquer um de:<br>*ssh*, *ldap*, *smtp*, *ftp*, *http*, *pop*, *nntp*, *imap*, *tcp*, *https*, *telnet* (consulte [detalhes](/manual/appendix/items/service_check_details))<br>**ip** - endereço IP (padrão é 127.0.0.1)<br>**port** - número da porta (por padrão a porta de serviço padrão é usada)|Exemplo:<br>=> net.tcp.service.perf\[ssh\] → pode ser usado para testar a velocidade de resposta inicial de um servidor SSH.<br><br>Verificação de protocolos criptografados (como IMAP na porta 993 ou POP na prota 995) não é suportado atualmente. Como contingência, por favor use net.tcp.service.perf\[tcp,<ip>,<port>\] para verificações como estas.<br><br>Verificação de LDAP e HTTPS no Windows é suportada somente por Zabbix Agent 2.<br><br>Note que a verificação telnet procura por um prompt de login (':' no final).<br><br>Consulte também os [problemas conhecidos](/manual/installation/known_issues#https_checks) para verificação de serviço HTTPS.<br><br>Os serviços *https* e *telnet* são suportados desde o Zabbix 2.0.|
|net.tcp.socket.count\[<laddr>,<lport>,<raddr>,<rport>,<state>\]|<|<|<|<|
|<|Retorna o número de sockets TCP que correspondem aos parâmetros.|Inteiro|**laddr** - endereço local IPv4/6 ou CIDR para a subnet<br>**lport** - número da porta local ou nome do serviço<br>**raddr** - endereço IPv4/6 remoto ou CIDR para a subnet<br>**rport** - número da porta remota ou nome do serviço<br>**state** - estado de conexão (*established (estabelecido)*, *syn\_sent*, *syn\_recv*, *fin\_wait1*, *fin\_wait2*, *time\_wait*, *close*, *close\_wait*, *last\_ack*, *listen (ouvindo)*, *closing (fechando)*)|Este item é suportado, apenas no Linux, em ambos Zabbix Agent/Agent 2. Com Zabbix Agent 2 também é suportado em Windows 64-bit.<br><br>Exemplo:<br>=> net.tcp.socket.count\[,80,,,established\] → verifica se a porta local 80/TCP está em estado "estabelecido"<br><br>Este item é suportado desde o Zabbix 6.0.|
|net.udp.listen\[port\]|<|<|<|<|
|<|Verifica se esta porta UDP está ouvindo (estado LISTEN).|0 - não está ouvindo<br><br>1 - está ouvindo|**port** - número da porta UDP|Exemplo:<br>=> net.udp.listen\[68\]<br><br>Suportado no Linux desde o Zabbix 1.8.4|
|net.udp.service\[service,<ip>,<port>\]|<|<|<|<|
|<|Verifica se serviço está em execução e respondendo a requisições UDP.|0 - serviço está parado<br><br>1 - serviço está em execução|**service** - *ntp* (veja [detalhes](/manual/appendix/items/service_check_details))<br>**ip** - endereço IP (padrão é 127.0.0.1)<br>**port** - número de porta (por padrão a porta de serviço padrão é usada)|Exemplo:<br>=> net.udp.service\[ntp,,45\] → pode ser usado para testar a disponibilidade de serviço NTP na porta 45/UDP.<br><br>Este item é suportado desde o Zabbix 3.0.0, mas o serviço *ntp* estava disponível para o item net.tcp.service\[\] em versões anteriores.|
|net.udp.service.perf\[service,<ip>,<port>\]|<|<|<|<|
|<|Verifica a performance de serviço UDP.|0 - serviço está parado<br><br>segundos - o número de segundos gastos aguardando pela resposta do serviço|**service** - *ntp* (veja [detalhes](/manual/appendix/items/service_check_details))<br>**ip** - endereço IP (padrão é 127.0.0.1)<br>**port** - número de porta (por padrão a porta de serviço padrão é usada)|Exemplo:<br>=> net.udp.service.perf\[ntp\] → pode ser usado para testar a disponibilidade de serviço NTP.<br><br>Este item é suportado desde o Zabbix 3.0.0, mas o serviço *ntp* estava disponível para o item net.tcp.service\[\] em versões anteriores.|
|net.udp.socket.count\[<laddr>,<lport>,<raddr>,<rport>,<state>\]|<|<|<|<|
|<|Retorna o número de sockets TCP que correspondem aos parâmetros.|Inteiro|**laddr** - endereço local IPv4/6 ou CIDR da subnet<br>**lport** - número da porta local ou nome do serviço<br>**raddr** - endereço remoto IPv4/6 ou CIDR da subnet<br>**rport** - número da porta remota ou nome do serviço<br>**state** - estado da conexão (*established (estabelecido)*, *unconn*)|Este item é suportado apenas no Linux em ambos Zabbix Agent/Agent 2. Com Zabbix Agent 2 é também suportado em Windows 64-bit.<br><br>Exemplo:<br>=> net.udp.socket.count\[,,,,listening\] → verifica se qualquer socket UDP está ouvindo ("listening")<br><br>Este item é suportado desde o Zabbix 6.0.|
|proc.cpu.util\[<name>,<user>,<type>,<cmdline>,<mode>,<zone>\]|<|<|<|<|
|<|Porcentagem de uso de CPU por processo.|Número flutuante|**name** - nome do processo (o padrão é *todos os processos*)<br>**user** - nome de usuário (o padrão é *todos os usuários*)<br>**type** - tipo de utilização de CPU:<br>*total* (padrão), *user (usuário)*, *system (sistema)*<br>**cmdline** - filtro por linha de comando (é uma [expressão regular](/manual/regular_expressions#overview))<br>**mode** - modo de aquisição de dados: *avg1* (padrão), *avg5*, *avg15*<br>**zone** - zona alvo: *current* → atual (padrão), *all* → tudo. Este parâmetro é suportado apenas em Solaris.|Exemplos:<br>=> proc.cpu.util\[,root\] → utilização de CPU por todos os processos sendo executados sob o usuário "root"<br>=> proc.cpu.util\[zabbix\_server,zabbix\] → utilização de CPU por todos os processos zabbix\_server sendo executados sob o usuário zabbix<br><br>O valor retornado é baseado na porcentagem de utilização de um único core de CPU. Por exemplo, a utilização de CPU de um processo usando dois cores completos é 200%.<br><br>Os dados de uso de CPU são reunidos por um coletor que suporta um máximo de 1024 consultas únicas (por nome, usuário e linha de comando). Consultas não acessadas durante as últimas 24h são removidas do coletor.<br><br>*Note* que quando configurando o parâmetro `zone` para *current (atual))* (ou padrão) no caso de o agente ter sido compilado em um Solaris sem suporte a zona, mas executando em um Solaris mais recente onde zonas são suportadas, então o agente retornará NOTSUPPORTED (NÃO SUPORTADO)) (o agente não pode limitar os resultados apenas à zona atual). No entanto, *all (tudo)* é suportado neste caso.<br><br>Esta chave é suportada desde o Zabbix 3.0.0 e está disponível em várias plataformas (veja [Items suportados por plataforma](/manual/appendix/items/supported_by_platform)).|
|proc.mem\[<name>,<user>,<mode>,<cmdline>,<memtype>\]|<|<|<|<|
|<|Memória usada por processo em bytes.|Inteiro - com `mode` como *max*, *min*, *sum*<br><br>Número Flutuante - com `mode` como *avg*|**name** - nome do processo (o padrão é *todos os processos*)<br>**user** - nome de usuário (padrão é *todos os usuários*)<br>**mode** - valores possíveis:<br>*avg*, *max*, *min*, *sum* (padrão)<br>**cmdline** - filtro por comando (é uma [expressão regular](/manual/regular_expressions#overview))<br>**memtype** - [tipo de memória](/manual/appendix/items/proc_mem_notes) usado pelo processo|Exemplos:<br>=> proc.mem\[,root\] → memória usada por todos os processos sendo executados sob o usuário "root"<br>=> proc.mem\[zabbix\_server,zabbix\] → memória usada por todos os processos zabbix\_server sendo executados sob o usuário zabbix<br>=> proc.mem\[,oracle,max,oracleZABBIX\] → memória usada pelo processo com maior consumo de memória sendo executado sob oracle e tendo oracleZABBIX na sua linha de comando<br><br>*Nota*: Quando vários processos usam memória compartilhada, a soma da memória usada pelos processos pode resultar em valores grandes, não realísticos.<br><br>Veja [notas](/manual/appendix/items/proc_mem_num_notes) sobre seleção de processos pelos parâmetros `name` e `cmdline` (específico Linux).<br><br>Quando este item é chamado da linha de comando e contém um parâmetro de linha de comando (p.e. usando o modo de teste do agente: `zabbix_agentd -t proc.mem[,,,apache2]`), um processo extra será contado, pois o agente contará a si mesmo.<br><br>O parâmetro `memtype` é suportado em várias [plataformas](/manual/appendix/items/supported_by_platform) desde o Zabbix 3.0.0.|
|proc.num\[<name>,<user>,<state>,<cmdline>,<zone>\]|<|<|<|<|
|<|O número de processos.|Inteiro|**name** - nome do processo (padrão é *todos os processos*)<br>**user** - nome de usuário (padrão é *todos os usuários*)<br>**state** (opções *disk* e *trace* desde a versão 3.4.0) - valores possíveis:<br>*all* (padrão),<br>*disk* - inativo ininterruptamente,<br>*run* - em execução,<br>*sleep* - inativo interruptamente,<br>*trace* - parado,<br>*zomb* - zumbi<br>**cmdline** - filtro por linha de comando (é uma [expressão regular](/manual/regular_expressions#overview))<br>**zone** - zona alvo: *current* (padrão), *all*. Este parâmetro é suportado apenas em Solaris.|Exemplos:<br>=> proc.num\[,mysql\] → número de processos em execução sob o usuário mysql<br>=> proc.num\[apache2,www-data\] → número de processos em execução sob o usuário www-data<br>=> proc.num\[,oracle,sleep,oracleZABBIX\] → número de processos inativos sob o usuário oracle contendo oracleZABBIX em sua linha de comando<br><br>Consulte [notas](/manual/appendix/items/proc_mem_num_notes) sobre seleção de  processos com parâmetros `name` e `cmdline` (específico para Linux).<br><br>No Windows, somente os parâmetros `name` e `user` são suportados.<br><br>Quando este item é instanciado na linha de comando e contém um parâmetro de linha de comando (p.e. usando o modo de teste do agente: `zabbix_agentd -t proc.num[,,,apache2]`), um processo extra será contabilizado, pois o agente contará a si mesmo.<br><br>*Note* que quando configurando o parâmetro `zone` para *current (atual))* (ou padrão) no caso de o agente ter sido compilado em um Solaris sem suporte a zona, mas executando em um Solaris mais recente onde zonas são suportadas, então o agente retornará NOTSUPPORTED (NÃO SUPORTADO)) (o agente não pode limitar os resultados apenas à zona atual). No entanto, *all (tudo)* é suportado neste caso.|
|sensor\[device,sensor,<mode>\]|<|<|<|<|
|<|Leitura de sensor de hardware.|Número flutuante|**device** - nome do dispositivo<br>**sensor** - nome do sensor<br>**mode** - valores possíveis:<br>*avg*, *max*, *min* (se este parâmetro for omitido, dispositivo e sensor são tratados literalmente).|Lê /proc/sys/dev/sensors no Linux 2.4.<br><br>Exemplo:<br>=> sensor\[w83781d-i2c-0-2d,temp1\]<br><br>Antes do Zabbix 1.8.4, o formato *sensor\[temp1\]* era usado.|
|^|^|^|^|Lê /sys/class/hwmon no Linux 2.6+.<br><br>Consulte uma descrição mais detalhada sobre item de [sensor](/manual/appendix/items/sensor) no Linux.|
|^|^|^|^|Lê a MIB *hw.sensors* no OpenBSD.<br><br>Exemplos:<br>=> sensor\[cpu0,temp0\] → temperatura de uma CPU<br>=> sensor\["cpu\[0-2\]$",temp,avg\] → média de temperatura das primeiras três CPUs<br><br>Suportado no OpenBSD desde o Zabbix 1.8.4.|
|system.boottime|<|<|<|<|
|<|Horário de boot do sistema.|Inteiro (Unix timestamp)| | |
|system.cpu.discovery|<|<|<|<|
|<|Lista de CPUs/cores de CPU detectados. Usado para descoberta de baixo-nível.|Objeto JSON| |Suportado em todas as plataformas desde a versão 2.4.0.|
|system.cpu.intr|<|<|<|<|
|<|Interrupções de dispositivo.|Inteiro| | |
|system.cpu.load\[<cpu>,<mode>\]|<|<|<|<|
|<|[Carga (Load) de CPU](http://en.wikipedia.org/wiki/Load_(computing)).|Número flutuante|**cpu** - valores possíveis:<br>*all* (padrão), *percpu* (desde a versão 2.0; carga total dividida pela contagem de CPUs disponíveis (online))<br>**mode** - valores possíveis:<br>*avg1* (média de 1 minuto, padrão), *avg5*, *avg15*|Exemplo:<br>=> system.cpu.load\[,avg5\].|
|system.cpu.num\[<type>\]|<|<|<|<|
|<|Número de CPUs.|Inteiro|**type** - valores possíveis:<br>*online* (padrão), *max*|Exemplo:<br>=> system.cpu.num|
|system.cpu.switches|<|<|<|<|
|<|Contagem de alternâncias de contexto (context switches).|Inteiro| | |
|system.cpu.util\[<cpu>,<type>,<mode>,<logical\_or\_physical>\]|<|<|<|<|
|<|Porcentagem de utilização de CPU.|Número flutuante|**cpu** - *<número CPU>* ou *all* (padrão)<br>**type** - valores possíveis:<br>*user* (padrão), *idle*, *nice*, *system* (padrão para Windows), *iowait*, *interrupt*, *softirq*, *steal*, *guest* (No Linux com kernels 2.6.24 e superior), *guest\_nice* (No Linux com kernels 2.6.33 e superior).<br>Veja também detalhes [específicos de platforma](/manual/appendix/items/supported_by_platform) para este parâmetro.<br>**mode** - valores possíveis:<br>*avg1* (média de 1 minuto, padrão), *avg5*, *avg15*<br>**logical\_or\_physical** (desde a versão 5.0.3; apenas no AIX) - valores possíveis: *logical* (padrão), *physical*. Este parâmetro é suportado apenas no AIX.|Exemplo:<br>=> system.cpu.util\[0,user,avg5\]<br><br>Nomenclatura antiga: *system.cpu.idleX, system.cpu.niceX, system.cpu.systemX, system.cpu.userX*|
|system.hostname\[<type>, <transform>\]|<|<|<|<|
|<|Nome de host do sistema.|String|**type** (antes da versão 5.4.7 suportado apenas no Windows) - valores possíveis: *netbios* (padrão para Windows), *host* (padrão no Linux), *shorthost* (desde a versão 5.4.7; retorna parte do nome do host antes do primeiro ponto, e texto completo para nomes sem ponto).<br>**transform** (desde a versão 5.4.7) - valores possíveis:<br>*none* (padrão), *lower* (converte para minúsculo)|O valor é obtido pela função GetComputerName() (para **netbios**) ou gethostname() (para **host**) no Windows e pelo comando "hostname" nos outros sistemas.<br><br>Exemplos de valores retornados:<br>*no Linux*:<br>=> system.hostname → linux-w7x1<br>=> system.hostname → exemplo.com<br>=> system.hostname\[shorthost\] → exemplo<br>*no Windows*:<br>=> system.hostname → WIN-SERV2008-I6<br>=> system.hostname\[host\] → Win-Serv2008-I6LonG<br>=> system.hostname\[host,lower\] → win-serv2008-i6long<br><br>Consulte também uma [descrição mais detalhada](/manual/appendix/install/windows_agent#configuration).|
|system.hw.chassis\[<info>\]|<|<|<|<|
|<|Informação de Chassis.|String|**info** - uma das opções: <br>full → completo (padrão), <br>model → modelo, <br>serial, <br>type → tipo ou <br>vendor → fornecedor|Exemplo: system.hw.chassis\[full\]<br>Hewlett-Packard HP Pro 3010 Small Form Factor PC CZXXXXXXXX Desktop\]<br><br>Esta chave depende da disponibilidade da tabela [SMBIOS](http://en.wikipedia.org/wiki/System_Management_BIOS).<br>Tentará ler a tabela DMI de sysfs, se o acesso a sysfs falhar então tenta ler diretamente da memória.<br><br>**Permissões de root** são necessárias porque o valor é obtido pela leitura do sysfs ou memória.<br><br>Suportado desde o Zabbix 2.0.|
|system.hw.cpu\[<cpu>,<info>\]|<|<|<|<|
|<|Informação de CPU.|String ou inteiro|**cpu** - *<número da CPU>* ou <br>*all* → tudo (padrão)<br>**info** - valores possíveis:<br>*full* → completo (padrão), <br>*curfreq*, <br>*maxfreq*, <br>*model* → modelo ou <br>*vendor* → fornecedor|Exemplo:<br>=> system.hw.cpu\[0,vendor\] → AuthenticAMD<br><br>Reúne informação de /proc/cpuinfo e /sys/devices/system/cpu/\[cpunum\]/cpufreq/cpuinfo\_max\_freq.<br><br>Se o número de CPU e *curfreq* ou *maxfreq* for especificado, um valor numérico é retornado (Hz).<br><br>Suportado desde o Zabbix 2.0.|
|system.hw.devices\[<type>\]|<|<|<|<|
|<|Listagem de dispositivos PCI ou USB.|Texto|**type** (desde a versão 2.0) - *pci* (padrão) ou *usb*|Exemplo:<br>=> system.hw.devices\[pci\] → 00:00.0 Host bridge: Advanced Micro Devices \[AMD\] RS780 Host Bridge<br>\[..\]<br><br>Retorna a saída do utilitário lspci ou lsusb (executado sem quaisquer parâmetros).|
|system.hw.macaddr\[<interface>,<format>\]|<|<|<|<|
|<|Listagem de endereços MAC.|String|**interface** - *all* → tudo (padrão) ou uma [expressão regular](/manual/regular_expressions#overview)<br>**format** - *full* → completo (padrão) ou <br>*short* → curto|Lista endereços MAC das interfaces cujos nomes correspondem com a [expressão regular](/manual/regular_expressions#overview) para `interface` (*all* (tudo) lista todas as interfaces).<br><br>Exemplo:<br>=> system.hw.macaddr\["eth0$",full\] → \[eth0\] 00:11:22:33:44:55<br><br>Se `format` for especificado como *short*, nomes de interface e endereços de MAC idênticos não são listados.<br><br>Suportado desde o Zabbix 2.0.|
|system.localtime\[<type>\]|<|<|<|<|
|<|Horário de sistema.|Inteiro - com `type` como *utc*<br><br>String - com `type` como *local*|**type** (desde a versão 2.0) - (tipo) valores possíveis:<br>*utc* - (padrão) tempo decorrido desde Epoch (00:00:00 UTC, 1º de Janeiro, 1970), medido em segundos.<br>*local* - o tempo no formato 'yyyy-mm-dd,hh:mm:ss.nnn,+hh:mm', onde y → ano.<br>|Deve ser usado apenas como [verificação passiva](/manual/appendix/items/activepassive#passive_checks).<br><br>Exemplo:<br>=> system.localtime\[local\] → cria um item usando esta chave e então a usa para mostrar o horário do host no [widget](/manual/web_interface/frontend_sections/monitoring/dashboard/widgets#clock) de *Relógio* no dashboard.|
|system.run\[command,<mode>\]|<|<|<|<|
|<|Executa o comando especificado no host.|Resultado do comando em texto<br><br>1 - com `mode` como *nowait* (independente do resultado do comando)|**command** - comando para execução<br>**mode** - valores possíveis:<br>*wait* - aguarda término da execução (padrão),<br>*nowait* - não aguarda|Até 512KB de dados podem ser retornados, incluindo espaço em branco à direita que é truncado.<br>Para ser processado corretamente, a saída do comando deve ser texto.<br><br>Exemplo:<br>=> system.run\[ls -l /\] → lista de arquivo detalhada do diretório root.<br><br>*Nota*: itens system.run são desabilitados por padrão. Aprenda como [habilitá-los](/manual/config/items/restrict_checks).<br><br>O valor de retorno do item é a saída padrão junto com saída de erro padrão produzidas pelo comando. O código de saída não é verificado.<br><br>Resultado vazio é permitido a partir do Zabbix 2.4.0.<br>Veja também: [Execução de comando](/manual/appendix/command_execution).|
|system.stat\[resource,<type>\]|<|<|<|<|
|<|Estatísticas de sistema.|Inteiro ou número flutuante|**ent** - número de unidades de processador que esta partição tem direito a receber (número flutuante)<br>**kthr,<type>** - informação sobre estados das threads do kernel:<br>*r* - número médio de threads de kernel executáveis (número flutuante)<br>*b* - número médio de threads de kernel colocadas na fila de espera do Gerenciador de Memória Virtual (Virtual Memory Manager)(número flutuante)<br>**memory,<type>** - informação sobre uso de memória virtual e real:<br>*avm* - páginas virtuais ativas (inteiro)<br>*fre* - tamanho da lista livre (inteiro)<br>**page,<type>** - informação sobre falta de páginas e atividade de paginação:<br>*fi* - paginação de arquivo (file page-ins) por segundo (número flutuante)<br>*fo* - despaginação de arquivo (file page-outs) por segundo (número flutuante)<br>*pi* - páginas paginadas do espaço de paginação (número flutuante)<br>*po* - páginas despaginadas para o espaço de paginação (número flutuante)<br>*fr* - páginas liberadas (substituição de página) (número flutuante)<br>*sr* - páginas escaneadas pelo algoritmo de substituição de páginas (número flutuante)<br>**faults,<type>** - taxa de interceptação e interrupção:<br>*in* - interrupções de dispositivo (número flutuante)<br>*sy* - chamadas de sistema (número flutuante)<br>*cs* - alternâncias de contexto de thread de kernel (número flutuante)<br>**cpu,<type>** - detalhamento da porcentagem de uso de tempo de processador:<br>*us* - tempo de usuário (número flutuante)<br>*sy* - tempo de sistemas (número flutuante)<br>*id* - tempo ocioso (número flutuante)<br>*wa* - tempo ocioso durante o qual o sistema possui requisições de disco/NFS I/O pendentes (número flutuante)<br>*pc* - número de processadores físicos consumidos (número flutuantet)<br>*ec* - porcentagem de capacidade autorizada consumida (número flutuante)<br>*lbusy* - indica a porcentagem de utilização de processadores lógicos ocorrida durante a execução no nível de usuário ou sistema (número flutuante)<br>*app* - indica os processadores físicos disponíveis na área de compartilhamento (número flutuante)<br>**disk,<type>** - estatísticas de disco:<br>*bps* - indica quantidade de dados transferidos (leitura ou escrita) para o drive em bytes por segundo (inteiro)<br>*tps* - indica o número de transferências por segundo que foram emitidas para o disco físico/fita (número flutuante)|<|
|<|^|^|Comentários|<|
|<|^|^|Este item é [suportado](/manual/appendix/items/supported_by_platform) apenas no AIX, desde o Zabbix 1.8.1.<br>Esteja ciente das seguintes limitações nestes itens:<br>=> system.stat\[cpu,app\] - suportado apenas no AIX LPAR do tipo "Compartilhado (Shared)"<br>=> system.stat\[cpu,ec\] - suportado no AIX LPAR do tipo "Compartilhado (Shared)" e "Dedicado" ("Dedicado" sempre retorna 100 (porcento))<br>=> system.stat\[cpu,lbusy\] - suportado apenas no AIX LPAR do tipo "Compartilhado (Shared)"<br>=> system.stat\[cpu,pc\] - suportado no AIX LPAR do tipo "Compartilhado (Shared)" e "Dedicado"<br>=> system.stat\[ent\] - suportado no AIX LPAR do tipo "Compartilhado (Shared)" e "Dedicado"|<|
|system.sw.arch|<|<|<|<|
|<|Informação de arquitetura de software.|String| |Exemplo:<br>=> system.sw.arch → i686<br><br>Informação é obtida da função uname().<br><br>Suportado desde o Zabbix 2.0.|
|system.sw.os\[<info>\]|<|<|<|<|
|<|Informação de sistema operacional.|String|**info** - valores possíveis:<br>*full* → completo (padrão), <br>*short* → curto ou <br>*name* → nome|Exemplo:<br>=> system.sw.os\[short\]→ Ubuntu 2.6.35-28.50-generic 2.6.35.11<br><br>Informação obtida de (note que nem todas os arquivos e opções estão presentes em todas as distribuições):<br>/proc/version (*completo*)<br>/proc/version\_signature (*curto*)<br>Parâmetro PRETTY\_NAME de /etc/os-release nos sitemas que o suportam, ou /etc/issue.net (*nome*)<br><br>Suportado desde o Zabbix 2.0.|
|system.sw.packages\[<package>,<manager>,<format>\]|<|<|<|<|
|<|Listagem de pacotes instalados.|Texto|**package** - *all* → tudo (padrão) ou uma [expressão regular](/manual/regular_expressions#overview)<br>**manager** - *all* → tudo (padrão) ou um gerenciador de pacote<br>**format** - *full* → completo (padrão) ou *short* → curto|Lista (alfabeticamente) os pacotes instalados cujo nome de `pacote` corresponde à [expressão regular](/manual/regular_expressions#overview) fornecida (*tudo* lista todos).<br><br>Exemplo:<br>=> system.sw.packages\[mini,dpkg,short\] → python-minimal, python2.6-minimal, ubuntu-minimal<br><br>Gerenciadores de pacote suportados (comando executado):<br>dpkg (dpkg --get-selections)<br>pkgtool (ls /var/log/packages)<br>rpm (rpm -qa)<br>pacman (pacman -Q)<br><br>Se `format` é especificado como *completo*, os pacotes são agrupados por gerenciadores de pacote (cada gerenciador em uma linha separada iniciando com seu nome entre colchetes).<br>Se `format` é especificado como *curto*, os pacotes não são agrupados e são listados em uma única linha.<br><br>Suporto desde o Zabbix 2.0.|
|system.swap.in\[<device>,<type>\]|<|<|<|<|
|<|Estatísticas de leitura de swap (do dispositivo para memória).|Inteiro|**device** - dispositivo usado para o swapping (padrão é *all (todos)*)<br>**type** - valores possíveis:<br>*count* (número de leituras de swap), *sectors* (setores lidos), *pages* (páginas lidas).<br>Veja também detalhes [específicos de plataforma](/manual/appendix/items/supported_by_platform) para este parâmetro.|Exemplo:<br>=> system.swap.in\[,pages\]<br><br>A origem desta informação é:<br>/proc/swaps, /proc/partitions, /proc/stat (Linux 2.4)<br>/proc/swaps, /proc/diskstats, /proc/vmstat (Linux 2.6)|
|system.swap.out\[<device>,<type>\]|<|<|<|<|
|<|Estatísticas de gravação de swap (da memória para o dispositivo).|Inteiro|**device** - dispositivo usado para o swapping (padrão é *all (todos)*)<br>**type** - valores possíveis:<br>*count* (número de gravações), *sectors* (setores gravados), *pages* (páginas gravadas).<br>Veja também detalhes [específicos de plataforma](/manual/appendix/items/supported_by_platform) para este parâmetro.|Exemplo:<br>=> system.swap.out\[,pages\]<br><br>A origem desta informação é:<br>/proc/swaps, /proc/partitions, /proc/stat (Linux 2.4)<br>/proc/swaps, /proc/diskstats, /proc/vmstat (Linux 2.6)|
|system.swap.size\[<device>,<type>\]|<|<|<|<|
|<|Tamanho de espaço de swap em bytes ou em porcentagem do total.|Inteiro - para bytes<br><br>Número flutuante - para porcentagem|**device** - dispositivo usado para o swapping (padrão é *all (todos)*)<br>**type** - valores possíveis:<br>*free* (espaço de swap livre, padrão), *pfree* (espaço de swap livre, em porcento), *pused* (espaço de swap usado, em porcento), *total* (espaço de swap total), *used* (espaço de swap usado)<br>Note que *pfree*, *pused* e não são suportados no Windows se o tamanho de swap for 0.<br>Veja também detalhes [específicos de plataforma](/manual/appendix/items/supported_by_platform) para este parâmetro.|Exemplo:<br>=> system.swap.size\[,pfree\] → porcentagem de espaço de swap livre<br><br>Se *device* não for especificado o Zabbix Agent levará em conta apenas dispositivos de swap (arquivos), a memória física será ignorada. Por exemplo, nos sistemas Solaris o comando *swap -s* inclui uma porção de memória física e dispositivos de swap (diferente de *swap -l*).<br><br>Note que esta chave pode retornar tamanho/porcentagem de swap incorreto em plataformas Windows virtualizadas (VMware ESXi, VirtualBox). Neste caso você pode usar a chave `perf_counter[\700(_Total)\702]` para obter a porcentagem de espaço correta.|
|system.uname|<|<|<|<|
|<|Identificação do sistema.|String| |Exemplo de valor retornado (Unix):<br>FreeBSD localhost 4.2-RELEASE FreeBSD 4.2-RELEASE \#0: Mon Nov i386<br><br>Exemplo de valor retornado (Windows):<br>Windows ZABBIX-WIN 6.0.6001 Microsoft® Windows Server® 2008 Standard Service Pack 1 x86<br><br>No Unix desde o Zabbix 2.2.0 o valor para este item é obtido com a chamada de sistema uname(). Previamente era obtido invocando "uname -a". O valor deste item pode diferir da saída de "uname -a" e não inclui informação adicional que "uname -a" mostra baseado em outras fontes.<br><br>No Windows desde o Zabbix 3.0 o valor para este item é obtido das classes WMI Win32\_OperatingSystem e Win32\_Processor. Previamente era obtido de APIs Windows voláteis e chaves de registro não documentadas. O nome de SO (incluindo edição) pode ser traduzido para o idioma de apresentação do ususário. Em algumas versões de Windows há símbolos de marca registrada e espaços extras.<br><br>Note que no Windows o item retorna a arquitetura do SO, enquanto no Unix ele retorna a arquitetura da CPU.|
|system.uptime|<|<|<|<|
|<|Tempo de atividade do sistema em segundos.|Inteiro| |Em [configuração de item](/manual/config/items/item#configuration), use unidades **s** ou **uptime** para obter valores legíveis.|
|system.users.num|<|<|<|<|
|<|Número de usuários logados.|Inteiro| |O comando **who** é usado no lado do agente para obter o valor.|
|vfs.dev.discovery|<|<|<|<|
|<|Lista de dispositivos de bloco e seu tipo. Usado para descoberta de baixo-nível.|Objeto JSON| |Este item é suportado apenas na plataforma Linux.<br><br>Suportado desde o Zabbix 4.4.0.|
|vfs.dev.read\[<device>,<type>,<mode>\]|<|<|<|<|
|<|Estatísticas de leitura de disco.|Inteiro - com `type` em *sectors*, *operations*, *bytes*<br><br>Número flutuante - com `type` em *sps*, *ops*, *bps*<br><br>*Nota*: se usando um intervalo de atualização de três horas ou mais^**[2](#footnotes)**^, retornará sempre '0'|**device** - dispositivo de disco (padrão é *all (todos)* ^**[3](#footnotes)**^)<br>**type** - valores possíveis: *sectors*, *operations*, *bytes*, *sps*, *ops*, *bps*<br>Note que o suporte e padrões para o parâmetro 'type' dependem da plataforma. Consulte detalhes [específicos de plataforma](/manual/appendix/items/supported_by_platform).<br>*sps*, *ops*, *bps* correspondem a: setores, operações, bytes por segundo, respectivamente.<br>**mode** - valores possíveis: *avg1* (média de 1 minuto, padrão), *avg5*, *avg15*.<br>Este parâmetro é suportado apenas com `type` como: sps, ops, bps.|Você pode usar nomes de dispositivo relativos (por exemplo, `sda`) bem como com um prefixo opcional /dev/ (por exemplo, `/dev/sda`).<br><br>Volumes lógicos LVM são suportados.<br><br>Valores padrão do parâmetro 'type' para diferentes SOs:<br>AIX - operations<br>FreeBSD - bps<br>Linux - sps<br>OpenBSD - operations<br>Solaris - bytes<br><br>Exemplo:<br>=> vfs.dev.read\[,operations\]<br><br>*sps*, *ops* e *bps* em plataformas suportadas costumam estar limitados a 8 dispositivos (7 individuais e um *all*). Desde o Zabbix 2.0.1 este limite é de 1024 dispositivos (1023 individuais e um para *all*).|
|vfs.dev.write\[<device>,<type>,<mode>\]|<|<|<|<|
|<|Estatísticas de escrita em disco.|Inteiro - com `type` como *sectors*, *operations*, *bytes*<br><br>Número flutuante - com `type` como *sps*, *ops*, *bps*<br><br>*Nota*: se usando um intervalo de atualização de três horas ou mais^**[2](#footnotes)**^, retornará sempre '0'|**device** - dispositivo de disco (padrão é *all (todos)* ^**[3](#footnotes)**^)<br>**type** - valores possíveis: *sectors*, *operations*, *bytes*, *sps*, *ops*, *bps*<br>Note que o suporte e padrões para o parâmetro 'type' depende da plataforma. Consulte detalhes [específicos de plataforma](/manual/appendix/items/supported_by_platform).<br>*sps*, *ops*, *bps* correspondem a: setores, operações, bytes por segundo, respectivamente.<br>**mode** - valores possíveis: *avg1* (média de 1 minuto, padrão), *avg5*, *avg15*.<br>Este parâmetro é suportado apenas com `type` como: sps, ops, bps.|Você pode usar nomes de dispositivo relativos (por exemplo, `sda`) bem como com um prefixo opcional /dev/ (por exemplo, `/dev/sda`).<br><br>Volumes lógicos LVM são suportados.<br><br>Valores padrão do parâmetro 'type' para diferentes SOs:<br>AIX - operations<br>FreeBSD - bps<br>Linux - sps<br>OpenBSD - operations<br>Solaris - bytes<br><br>Exemplo:<br>=> vfs.dev.write\[,operations\]<br><br>*sps*, *ops* e *bps* em plataformas suportadas costumam estar limitados a 8 dispositivos (7 individuais e um *all*). Desde o Zabbix 2.0.1 este limite é de 1024 dispositivos (1023 individuais e um para *all*).|
|vfs.dir.count\[dir,<regex\_incl>,<regex\_excl>,<types\_incl>,<types\_excl>,<max\_depth>,<min\_size>,<max\_size>,<min\_age>,<max\_age>,<regex\_excl\_dir>\]|<|<|<|<|
|<|Contagem de entrada de diretório.|Inteiro|**dir** - caminho absoluto para o diretório<br>**regex\_incl** - [expressão regular](/manual/regular_expressions#overview) descrevendo o padrão de nome da entidade (arquivo, diretório, link simbólico) a ser incluído; inclui tudo se vazio (valor padrão)<br>**regex\_excl** - [expressão regular](/manual/regular_expressions#overview) descrevendo o padrão de nome da entidade (arquivo, diretório, link simbólico) a ser excluído; não exclui nada se vazio (valor padrão)<br>**types\_incl** - tipos de entrada de diretório para contagem, valores possíveis:<br>*file* - arquivo regular, *dir* - subdiretório, *sym* - link simbólico, *sock* - socket, *bdev* - dispositivo de bloco, *cdev* - dispositivo de caracter, *fifo* - FIFO, *dev* - sinônimo de "bdev,cdev", *all* - todos os tipos (padrão), p.e. "file,dir,sym,sock,bdev,cdev,fifo". Tipos múltiplos deve ser separados com vírgula e entre aspas duplas.<br>**types\_excl** - tipos de entrada de diretório (veja <types\_incl>) para NÃO contar. Se algum tipo de entrada estiver em ambos <types\_incl> e <types\_excl>, as entradas de diretório deste tipo NÃO são contadas.<br>**max\_depth** - profundidade máxima de subdiretórios a serem examinados. **-1** (padrão) - ilimitado, **0** - sem descida aos subdiretórios.<br>**min\_size** - tamanho mínimo (em bytes) para o arquivo ser contado. Arquivos menores não serão contados. [Sufixos de memória](/manual/appendix/suffixes#memory_suffixes) podem ser usados.<br>**max\_size** - tamanho máximo (em bytes) para o arquivo ser contado. Arquivos maiores não serão contados. [Sufixos de memória](/manual/appendix/suffixes#memory_suffixes) podem ser usados.<br>**min\_age** - idade mínima (em segundos) da entrada de diretório para ser contada. Entradas mais recentes não serão contadas. [Sufixos de tempo](/manual/appendix/suffixes#time_suffixes) podem ser usados.<br>**max\_age** - idade máxima (em segundos) da entrada de diretório para ser contada. Entradas tão antigas quanto ou mais antigas não serão contadas (data de modificação). [Sufixos de tempo](/manual/appendix/suffixes#time_suffixes) podem ser usados.<br>**regex\_excl\_dir** - [expressão regular](/manual/regular_expressions#overview) decrevendo o padrão de nome de diretório a excluir. Todo o conteúdo do diretório será excluído (em contraste ao regex\_excl)|Variáveis de ambiente, p.e. %APP\_HOME%, $HOME e %TEMP% não são suportadas.<br><br>Pseudo-diretórios "." e ".." nunca são contados.<br><br>Links simbólicos nunca são seguidos nas passagens de diretório.<br><br>No Windows, links de diretório simbólicos são pulados e links físicos são contados apenas uma vez.<br><br>Ambos `regex_incl` e `regex_excl` são aplicados a arquivos e diretórios quando calculando tamanho da entrada, mas são ignorados quando selecionando subdiretórios para passagem (se regex\_incl é “(?i)\^.+\\.zip$” e max\_depth não estiver configurado, então todos os subdiretórios serão passados, mas apenas do tipo zip serão contados).<br><br>O tempo de execução é limitado pelo valor de tempo máximo (timeout) padrão na [configuração](/manual/appendix/config/zabbix_agentd) do agente (3 seg). Dado que a passagem por grandes diretórios pode levar mais tempo que isso, nenhum dado será retornado e o item não será suportado. Contagem parcial não será retornada.<br><br>Quando filtrando por tamanho, apenas arquivos regulares têm tamanhos significativos. Sob Linux e BSD, diretórios também têm tamanhos não-zerados (tipicamente alguns Kb). Dispositivos têm tamanhos zerados, p.e. o tamanho de **/dev/sda1** não reflete no tamanho da respectiva partição. Deste modo, quando usando `<min_size>`e `<max_size>`, é aconselhável especificar `<types_incl>` como "*file*", para evitar surpresas.<br><br>Exemplos:<br>⇒ vfs.dir.count\[/dev\] - monitora o número de dispositivos em /dev (Linux)<br>⇒ vfs.dir.count\["C:\\Users\\ADMINI\~1\\AppData\\Local\\Temp"\] - monitora o número de arquivos no diretório temporário (Windows)<br><br>Suportado desde o Zabbix 4.0.0.|
|vfs.dir.get\[dir,<regex\_incl>,<regex\_excl>,<types\_incl>,<types\_excl>,<max\_depth>,<min\_size>,<max\_size>,<min\_age>,<max\_age>,<regex\_excl\_dir>\]|<|<|<|<|
|<|Lista de entradas de diretório.|JSON|**dir** - caminho absoluto para o diretório<br>**regex\_incl** - [expressão regular](/manual/regular_expressions#overview) descrevendo o padrão de nome da entidade (arquivo, diretório, link simbólico) para inclusão; inclui tudo se vazio (valor padrão)<br>**regex\_excl** - [expressão regular](/manual/regular_expressions#overview) descrevendo o padrão de nome da entidade (arquivo, diretório, link simbólico) para exclusão; não exclui nenhum se estiver vazio (valor padrão)<br>**types\_incl** - tipos de entrada de diretório para listar, valores possíveis:<br>*file* - arquivo regular, *dir* - subdiretório, *sym* - link simbólico, *sock* - socket, *bdev* - dispositivo de bloco, *cdev* - dispositivo de caracter, *fifo* - FIFO, *dev* - sinônimo de "bdev,cdev", *all* - todos os tipos (padrão), p.e. "file,dir,sym,sock,bdev,cdev,fifo". Tipos múltiplos devem ser separados com vírgula e entre aspas duplas.<br>**types\_excl** - tipos de entrada de diretório (veja <types\_incl>) para NÃO listar. Se algum tipo de entrada está em ambos <types\_incl> e <types\_excl>, entradas de diretório deste tipo NÃO são listadas.<br>**max\_depth** - profundidade máxima de subdiretórios a serem examinados. **-1** (padrão) - ilimitado, **0** - sem descida aos subdiretórios.<br>**min\_size** - tamanho mínimo (em bytes) para o arquivo ser contado. Arquivos menores não serão contados. [Sufixos de memória](/manual/appendix/suffixes#memory_suffixes) podem ser usados.<br>**max\_size** - tamanho máximo (em bytes) para o arquivo ser contado. Arquivos maiores não serão contados. [Sufixos de memória](/manual/appendix/suffixes#memory_suffixes) podem ser usados.<br>**min\_age** - idade mínima (em segundos) da entrada de diretório para ser contada. Entradas mais recentes não serão contadas. [Sufixos de tempo](/manual/appendix/suffixes#time_suffixes) podem ser usados.<br>**max\_age** - idade máxima (em segundos) da entrada de diretório para ser contada. Entradas tão antigas quanto ou mais antigas não serão contadas (data de modificação). [Sufixos de tempo](/manual/appendix/suffixes#time_suffixes) podem ser usados.<br>**regex\_excl\_dir** - [expressão regular](/manual/regular_expressions#overview) decrevendo o padrão de nome de diretório a excluir. Todo o conteúdo do diretório será excluído (em contraste ao regex\_excl)|Variáveis de ambiente, p.e. %APP\_HOME%, $HOME e %TEMP% não são suportadas.<br><br>Pseudo-diretórios "." e ".." nunca são contados.<br><br>Links simbólicos nunca são seguidos nas passagens de diretório.<br><br>No Windows, links de diretório simbólicos são pulados e links físicos são contados apenas uma vez.<br><br>Ambos `regex_incl` e `regex_excl` são aplicados a arquivos e diretórios quando calculando tamanho da entrada, mas são ignorados quando selecionando subdiretórios para passagem (se regex\_incl é “(?i)\^.+\\.zip$” e max\_depth não estiver configurado, então todos os subdiretórios serão passados, mas apenas do tipo zip serão contados).<br><br>O tempo de execução é limitado pelo valor de tempo máximo (timeout) padrão na [configuração](/manual/appendix/config/zabbix_agentd) do agente (3 seg). Dado que a passagem por grandes diretórios pode levar mais tempo que isso, nenhum dado será retornado e o item não será suportado. Contagem parcial não será retornada.<br><br>Quando filtrando por tamanho, apenas arquivos regulares têm tamanhos significativos. Sob Linux e BSD, diretórios também têm tamanhos não-zerados (tipicamente alguns Kb). Dispositivos têm tamanhos zerados, p.e. o tamanho de **/dev/sda1** não reflete no tamanho da respectiva partição. Deste modo, quando usando `<min_size>`e `<max_size>`, é aconselhável especificar `<types_incl>` como "*file*", para evitar surpresas.<br><br>Exemplos:<br>⇒ vfs.dir.get\[/dev\] - coleta lista de dispositivos em /dev (Linux)<br>⇒ vfs.dir.get\["C:\\Users\\ADMINI\~1\\AppData\\Local\\Temp"\] - coleta lista de arquivos no diretório temporário (Windows)<br><br>Suportado desde o Zabbix 6.0.0.|
|vfs.dir.size\[dir,<regex\_incl>,<regex\_excl>,<mode>,<max\_depth>,<regex\_excl\_dir>\]|<|<|<|<|
|<|Tamanho de diretório (em bytes).|Inteiro|**dir** - caminho absoluto para o diretório<br>**regex\_incl** - [expressão regular](/manual/regular_expressions#overview) descrevendo o padrão de nome da entidade (arquivo, diretório, link simbólico) para inclusão; inclui tudo se vazio (valor padrão)<br>**regex\_excl** - [expressão regular](/manual/regular_expressions#overview) descrevendo o padrão de nome da entidade (arquivo, diretório, link simbólico) para exclusão; não exclui nenhum se estiver vazio (valor padrão)<br>**mode** - valores possíveis:<br>*apparent* (padrão) - obtém o valor aparente do tamanho dos arquivos em vez do uso em disco (age como `du -sb dir`), *disk* - obtém o uso em disco (age como `du -s -B1 dir`). Diferente do comando du, o item vfs.dir.size leva em conta arquivos ocultos quando calculando o tamanho do diretório (age como `du -sb .[^.]* *` de dentro do diretório).<br>**max\_depth** - profundidade máxima de subdiretórios a serem examinados. **-1** (padrão) - ilimitado, **0** - sem descida aos subdiretórios.<br>**regex\_excl\_dir** - [expressão regular](/manual/regular_expressions#overview) decrevendo o padrão de nome de diretório a excluir. Todo o conteúdo do diretório será excluído (em contraste ao regex\_excl)|Apenas diretórios com no mínimo permissão de leitura para o usuário *zabbix* serão calculados.<br><br>No Windows, links de diretório simbólicos são pulados e links físicos são contados apenas uma vez.<br><br>Com diretórios grandes ou dispositivos lentos este item pode esgotar o tempo de execução devido à configuração de Timeout nos arquivos de configuração do [Agent](/manual/appendix/config/zabbix_agentd) e [Server](/manual/appendix/config/zabbix_server)/[Proxy](/manual/appendix/config/zabbix_proxy). Aumente o valor de timeout conforme necessário.<br><br>Exemplos:<br>⇒ vfs.dir.size\[/tmp,log\] - calcula o tamanho de todos os arquivos em /tmp que contêm 'log'<br>⇒ vfs.dir.size\[/tmp,log,\^.+\\.old$\] - calcula o tamanho de todos os arquivos em /tmp que contêm 'log', excluindo arquivos contendo '.old'<br><br>O limite de tamanho de arquivo depende do [suporte a grandes arquivos](/manual/appendix/items/large_file_support).<br><br>Suportado desde o Zabbix 3.4.0.|
|vfs.file.cksum\[file,<mode>\]|<|<|<|<|
|<|Checksum de arquivo, calculado pelo algoritmo cksum do UNIX.|Inteiro - com `mode` como *crc32*<br><br>String - com `mode` como *md5*, *sha256*|**file** - caminho completo para o arquivo<br>**mode** - *crc32* (padrão), *md5*, *sha256*|Exemplo:<br>=> vfs.file.cksum\[/etc/passwd\]<br><br>Exemplo de valores retornados (crc32/md5/sha256 respectivamente):<br>675436101<br>9845acf68b73991eb7fd7ee0ded23c44<br>ae67546e4aac995e5c921042d0cf0f1f7147703aa42bfbfb65404b30f238f2dc<br><br>O limite de tamanho de arquivo depende do [suporte a grandes arquivos](/manual/appendix/items/large_file_support).<br><br>O parâmetro `mode` é suportado desde o Zabbix 6.0.|
|vfs.file.contents\[file,<encoding>\]|<|<|<|<|
|<|Obtendo o conteúdo de um arquivo.|Text|**file** - caminho completo para o arquivo<br>**encoding** - [identificador](/manual/config/items/itemtypes/zabbix_agent#encoding_settings) de código de página|Retorna uma string vazia se o arquivo estiver vazio ou conter apens caracteres LF/CR.<br><br>Byte order mark (BOM) é excluída da saída.<br><br>Exemplo:<br>=> vfs.file.contents\[/etc/passwd\]<br><br>Este item é limitado a arquivos não maiores que 64 Kbytes.<br><br>Suportado desde o Zabbix 2.0.|
|vfs.file.exists\[file,<types\_incl>,<types\_excl>\]|<|<|<|<|
|<|Verifica se arquivo existe.|0 - não encontrado<br><br>1 - arquivo do tipo especificado existe|**file** - caminho completo para o arquivo<br>**types\_incl** - lista de tipos de arquivo a incluir, valores possíveis: *file* (arquivo regular, padrão (se types\_excl não estiver configurado)), *dir* (diretório), *sym* (link simbólico), *sock* (socket), *bdev* (dispositivo de bloco), *cdev* (dispositivo de caracter), *fifo* (FIFO), *dev* (sinônimo de "bdev,cdev"), *all* (todos os tipos mencionados, padrão se types\_excl estiver especificado).<br>**types\_excl** - lista de tipos de arquivos a excluir, veja types\_incl para valores possíveis (por padrão nenhum tipo é excluído)|Tipos múltiplos devem ser separados com vírgula e entre aspas duplas "".<br>No Windows as aspas duplas devem ser escapadas '\\' com contrabarra e a chave do item completamente quotadas com aspas duplas quando usando o utilitário de linha de comando para chamar o zabbix\_get.exe ou agent2.<br><br>Se o mesmo tipo estiver em ambos <types\_incl> e <types\_excl>, os arquivos deste tipo são excluídos.<br><br>Exemplos:<br>=> vfs.file.exists\[/tmp/application.pid\]<br>=> vfs.file.exists\[/tmp/application.pid,"file,dir,sym"\]<br>=> vfs.file.exists\[/tmp/application\_dir,dir\]<br><br>O limite de tamanho de arquivo depende do [suporte a grandes arquivos](/manual/appendix/items/large_file_support).<br><br>Note que o item pode se tornar não suportado no Windows se um diretório for procurado dentro de um diretório não existente, p.e. vfs.file.exists\[C:\\no\\dir,dir\] (onde 'no' não existe).|
|vfs.file.get\[file\]|<|<|<|<|
|<|Retorna informação sobre um arquivo.|Objeto JSON|**file** - caminho completo para o arquivo|Tipos de arquivo suportados em sistemas tipo-UNIX: arquivo regular, diretório, link simbólico, socket, dispositivo de bloco, dispositivo de caracter, FIFO<br><br>Tipos de arquivo suportados no Windows: arquivo regular, diretório, link simbólico<br><br>Exemplo:<br>=> vfs.file.get\[/etc/passwd\] → retorna um JSON com informação sobre o arquivo /etc/passwd (tipo, usuário, permissões, SID, uid, etc.)<br><br>Suportado desde o Zabbix 6.0.|
|vfs.file.md5sum\[file\]|<|<|<|<|
|<|Checksum MD5 de um arquivo.|String de caracter (hash MD5 do arquivo)|**file** - caminho completo para o arquivo|Exemplo:<br>=> vfs.file.md5sum\[/usr/local/etc/zabbix\_agentd.conf\]<br><br>Exemplo de valor retornado:<br>b5052decb577e0fffd622d6ddc017e82<br><br>O limite de tamanho de arquivo (64 MB) para este item foi removido na versão 1.8.6.<br><br>O limite de tamanho de arquivo depende do [suporte a grandes arquivos](/manual/appendix/items/large_file_support).|
|vfs.file.owner\[file,<ownertype>,<resulttype>\]|<|<|<|<|
|<|Obtém o proprietário de um arquivo.|String de caracter|**file** - caminho completo para o arquivo<br>**ownertype** - *user* → usuário (padrão) ou *group* → grupo (apenas Unix)<br>**resulttype** - *name* (padrão) ou *id*; para id - retorna uid/gid no Unix, SID no Windows|Exemplo:<br>=> vfs.file.owner\[/tmp/zabbix\_server.log\] → retorna o proprietário de /tmp/zabbix\_server.log<br>=> vfs.file.owner\[/tmp/zabbix\_server.log,,id\] → retorna o ID do proprietário de /tmp/zabbix\_server.log<br><br>Suportado desde o Zabbix 6.0.|
|vfs.file.permissions\[file\]|<|<|<|<|
|<|Retorna uma string de 4 dígitos contendo o número octal de permissões Unix.|String|**file** - caminho completo para o arquivo|Não suportado no Windows.<br><br>Exemplo:<br>=> vfs.file.permissions\[/etc/passwd\] → retorna as permissões de /etc/passwd, por exemplo, '0644'<br><br>Suportado desde o Zabbix 6.0.|
|vfs.file.regexp\[file,regexp,<encoding>,<start line>,<end line>,<output>\]|<|<|<|<|
|<|Encontre um texto em um arquivo.|A linha contendo o texto correspondente, ou como especificado pelo parâmetro opcional `output`|**file** - caminho completo para o arquivo<br>**regexp** - [expressão regular](/manual/regular_expressions#overview) descrevendo o padrão requerido<br>**encoding** - [identificador](/manual/config/items/itemtypes/zabbix_agent#encoding_settings) de código de página<br>**start line** - o número da primeira linha para busca (primeira linha do arquivo por padrão).<br>**end line** - o número da última linha para busca (última linha do arquivo por padrão).<br>**output** - um modelo opcional de formatação de saída. A sequência de escape **\\0** é substituída pela parte de texto correspondente (do primeiro caracter onde a correspondência começa até o caracter onde a correspondência termina) enquanto uma sequência de escape **\\N** (onde N=1...9) é substituída pelo N-ésimo grupo correspondente (ou um texto vazio caso N exceda o número de grupos capturados).|Apenas a primeira linha correspondente é retornada.<br>Um texto vazio é retornado caso nenhuma linha corresponda à expressão.<br><br>Byte order mark (BOM) é excluído da saída.<br><br>A extração de conteúdo usando o parâmetro `output` ocorre no agente.<br><br>Os parâmetros `start line`, `end line` e `output` são suportadps desde a versão 2.2.<br><br>Exemplos:<br>=> vfs.file.regexp\[/etc/passwd,zabbix\]<br>=> vfs.file.regexp\[/path/to/some/file,"(\[0-9\]+)$",,3,5,\\1\]<br>=> vfs.file.regexp\[/etc/passwd,"\^zabbix:.:(\[0-9\]+)",,,,\\1\] → obtendo o ID do usuário *zabbix*|
|vfs.file.regmatch\[file,regexp,<encoding>,<start line>,<end line>\]|<|<|<|<|
|<|Encontre um texto em um arquivo.|0 - correspondência não encontrada<br><br>1 - encontrada|**file** - caminho completo para o arquivo<br>**regexp** - [expressão regular](/manual/regular_expressions#overview) descrevendo o padrão requerido<br>**encoding** - [identificador](/manual/config/items/itemtypes/zabbix_agent#encoding_settings) de código de página<br>**start line** - o número da primeira linha para busca (primeira linha do arquivo por padrão).<br>**end line** - o número da última linha para busca (última linha do arquivo por padrão).|Byte order mark (BOM) é ignorado.<br><br>Os parâmetros `start line` e `end line` são suportados desde a versão 2.2.<br><br>Exemplo:<br>=> vfs.file.regmatch\[/var/log/app.log,error\]|
|vfs.file.size\[file,<mode>\]|<|<|<|<|
|<|Tamanho de arquivo (em bytes).|Inteiro|**file** - caminho completo para o arquivo<br>**mode** - valores possíveis:<br>*bytes* (padrão) ou *lines* (linhas vazias são contadas, também)|O arquivo deve ter permissões de leitura para o usuário *zabbix*.<br><br>Exemplo:<br>=> vfs.file.size\[/var/log/syslog\]<br><br>O limite de tamanho de arquivo depende do [suporte a grandes arquivos](/manual/appendix/items/large_file_support).<br><br>O parâmetro `mode` é suportado desde o Zabbix 6.0.|
|vfs.file.time\[file,<mode>\]|<|<|<|<|
|<|Informação de data de arquivo.|Inteiro (Unix timestamp)|**file** - caminho completo para o arquivo<br>**mode** - valores possíveis:<br>*modify* (padrão) - última data de modificação de conteúdo do arquivo,<br>*access* - última data de leitura do arquivo,<br>*change* - última data de alteração das propriedades do arquivo|Exemplo:<br>=> vfs.file.time\[/etc/passwd,modify\]<br><br>O limite de tamanho de arquivo depende do [suporte a grandes arquivos](/manual/appendix/items/large_file_support).|
|vfs.fs.discovery|<|<|<|<|
|<|Lista de sistemas de arquivo montados e seus tipos. Usado para descoberta de baixo-nível.|Objeto JSON| |Suportado desde o Zabbix 2.0.<br><br>A macro {\#FSDRIVETYPE} é suportada no Windows desde o Zabbix 3.0.<br><br>A macro {\#FSLABEL} é suportada no Windows desde o Zabbix 6.0.|
|vfs.fs.get|<|<|<|<|
|<|Lista de sistemas de arquivo montados, seus tipos, espaço em disco e estatísticas de inode. Pode ser usado para descoberta de baixo-nível.|Objeto JSON| |Suportado desde o Zabbix 4.4.5.<br><br>A macro {\#FSLABEL} é suportada no Windows desde o Zabbix 6.0.|
|vfs.fs.inode\[fs,<mode>\]|<|<|<|<|
|<|Número ou porcentagem de inodes.|Inteiro - para número<br><br>Número flutuante - para porcentagem|**fs** - sistema de arquivo<br>**mode** - valores possíveis:<br>*total* (padrão), *free*, *used*, //pfree // (livre, porcentagem), *pused* (usado, porcentagem)|Exemplo:<br>=> vfs.fs.inode\[/,pfree\]|
|vfs.fs.size\[fs,<mode>\]|<|<|<|<|
|<|Espaço em disco em bytes ou em porcentagem do total.|Inteiro - para bytes<br><br>Número flutuante - para porcentagem|**fs** - sistema de arquivo<br>**mode** - valores possíveis:<br>*total* (padrão), *free*, *used*, *pfree* (livre, porcentagem), *pused* (usado, porcentagem)|No caso de um volume montado, o espaço em disco para o sistema de arquivo local é retornado.<br><br>Exemplo:<br>=> vfs.fs.size\[/tmp,free\]<br><br>O espaço reservado de um sistema de arquivo é levado em conta e não incluído quando usado o modo *free*.|
|vm.memory.size\[<mode>\]|<|<|<|<|
|<|Espaço em memória em bytes ou porcentagem do total.|Inteiro - para bytes<br><br>Número flutuante - para porcentagem|**mode** - valores possíveis:<br>*total* (padrão), *active*, *anon*, *buffers*, *cached*, *exec*, *file*, *free*, *inactive*, *pinned*, *shared*, *slab*, *wired*, *used*, *pused* (usado, porcentagem), *available*, *pavailable* (disponível, porcentagem)<br>Consulte também suporte [específico para plataforma](/manual/appendix/items/supported_by_platform) e [detalhes adicionais](/manual/appendix/items/vm.memory.size_params) para este parâmetro.|Este item aceita três categorias de parâmetros:<br><br>1) *total* - quantidade total de memória;<br>2) tipos específicos de memória por plataforma: *active*, *anon*, *buffers*, *cached*, *exec*, *file*, *free*, *inactive*, *pinned*, *shared*, *slab*, *wired*;<br>3) estimativas de nível de usuário para quanta memória é usada e disponível: *used*, *pused*, *available*, *pavailable*.|
|web.page.get\[host,<path>,<port>\]|<|<|<|<|
|<|Obtém conteúdo de uma página web.|Código da página web como texto (incluindo cabeçalhos (headers))|**host** - nome de host ou URL (como `scheme://host:port/path`, onde apenas *host* é obrigatório).<br>Esquemas URL permitidos: *http*, *https*^**[4](#footnotes)**^. A ausência de esquema será tratada como *http*. Se um URL é especificada, `path` e `port` devem ser vazios. Especificação de nome de usuário/senha quando conectando a servidores que requerem autenticação, por exemplo: `http://user:password@www.example.com` só é possível com suporte a cURL ^**[4](#footnotes)**^.<br>Punycode é suportado nos nomes de host.<br>**path** - caminho para o documeto HTML (padrão é /)<br>**port** - número de porta (padrão é 80 para HTTP)|Este item se torna não suportado se o recurso especificado em `host` não existir ou estiver indisponível.<br><br>`host` pode ser nome de host, nome de domínio, endereço IPv4 ou IPv6. Mas para endereço IPv6 o Zabbix Agent deve ser compilado com suporte a IPv6 habilitado.<br><br>Exemplo:<br>=> web.page.get\[www.example.com,index.php,80\]<br>=> web.page.get\[https://www.example.com\]<br>=> web.page.get\[https://blog.example.com/?s=zabbix\]<br>=> web.page.get\[localhost:80\]<br>=> web.page.get\["\[::1\]/server-status"\]|
|web.page.perf\[host,<path>,<port>\]|<|<|<|<|
|<|Tempo de carregamento da página web completa (em segundos).|Número flutuante|**host** - nome de host ou URL (como `scheme://host:port/path`, onde apenas *host* é obrigatório).<br>Esquemas URL permitidos: *http*, *https*^**[4](#footnotes)**^. A ausência de esquema será tratada como *http*. Se um URL é especificada, `path` e `port` devem ser vazios. Especificação de nome de usuário/senha quando conectando a servidores que requerem autenticação, por exemplo: `http://user:password@www.example.com` só é possível com suporte a cURL ^**[4](#footnotes)**^.<br>Punycode é suportado nos nomes de host.<br>**path** - caminho para o documeto HTML (padrão é /)<br>**port** - número de porta (padrão é 80 para HTTP)|Este item se torna não suportado se o recurso especificado em `host` não existir ou estiver indisponível.<br><br>`host` pode ser nome de host, nome de domínio, endereço IPv4 ou IPv6. Mas para endereço IPv6 o Zabbix Agent deve ser compilado com suporte a IPv6 habilitado.<br><br>Exemplo:<br>=> web.page.perf\[www.example.com,index.php,80\]<br>=> web.page.perf\[https://www.example.com\]|
|web.page.regexp\[host,<path>,<port>,regexp,<length>,<output>\]|<|<|<|<|
|<|Encontre texto em página web.|O texto correspondente, ou como especificado pelo parâmetro opcional `output`|**host** - nome de host ou URL (como `scheme://host:port/path`, onde apenas *host* é obrigatório).<br>Esquemas URL permitidos: *http*, *https*^**[4](#footnotes)**^. A ausência de esquema será tratada como *http*. Se um URL é especificada, `path` e `port` devem ser vazios. Especificação de nome de usuário/senha quando conectando a servidores que requerem autenticação, por exemplo: `http://user:password@www.example.com` só é possível com suporte a cURL ^**[4](#footnotes)**^.<br>Punycode é suportado nos nomes de host.<br>**path** - caminho para o documeto HTML (padrão é /)<br>**port** - número de porta (padrão é 80 para HTTP)<br>**regexp** - regular [expression](/manual/regular_expressions#overview) descrevendo o padrão requerido<br>**length** - número máximo de caracteres a retornar<br>**output** - um modelo de formatação de saída opcional. A sequência de escape **\\0** é substituída pela parte de texto correspondente (do primeiro caracter onde a correspondência começa até o caracter onde a correspondência termina) enquanto uma sequência de escape **\\N** (onde N=1...9) é substituída pelo N-ésimo grupo correspondente (ou um texto vazio caso N exceda o número de grupos capturados).|Este item se torna não suportado se o recurso especificado em `host` não existir ou estiver indisponível.<br><br>`host` pode ser nome de host, nome de domínio, endereço IPv4 ou IPv6. Mas para endereço IPv6 o Zabbix Agent deve ser compilado com suporte a IPv6 habilitado.<br><br>A extração de conteúdo usando o parâmetro `output` ocorre no agente.<br><br>O parâmetro `output` é suportado desde a versão 2.2.<br><br>Exemplo:<br>=> web.page.regexp\[www.example.com,index.php,80,OK,2\]<br>=> web.page.regexp\[https://www.example.com,,,OK,2\]|
|zabbix.stats\[<ip>,<port>\]|<|<|<|<|
|<|Retorna um conjunto de métricas internas do Zabbix Server ou Proxy remotamente.|Objeto JSON|**ip** - lista de IP/DNS/máscara de rede de Servers/Proxies para serem consultados remotamente (padrão é 127.0.0.1)<br>**port** - porta do Server/Proxy para ser consultada remotamente (padrão é 10051)|Note que a requisição de estatísticas só será aceita a partir dos endereços listados no parâmetro 'StatsAllowedIP' do [Server](/manual/appendix/config/zabbix_server)/[Proxy](/manual/appendix/config/zabbix_proxy) na instância de destino.<br><br>Um conjunto selecionado de métricas internas é retornado por este item. Para detalhes, veja [Monitoramento remoto de estatísticas do Zabbix](/manual/appendix/items/remote_stats#exposed_metrics).|
|zabbix.stats\[<ip>,<port>,queue,<from>,<to>\]|<|<|<|<|
|<|Retorna número de itens monitorados atrasados na fila do Zabbix Server ou Proxy remotamente.|Objeto JSON|**ip** - lista de IP/DNS/máscara de rede de Servers/Proxies para serem consultados remotamente (padrão é 127.0.0.1)<br>**port** - porta do Server/Proxy para ser consultada remotamente (padrão é 10051)<br>**queue** - constante (para ser usado como está)<br>**from** - atrasado por pelo menos (padrão é 6 segundos)<br>**to** - atrasado por até (padrão é infinito)|Note que a requisição de estatísticas só será aceita a partir dos endereços listados no parâmetro 'StatsAllowedIP' do [Server](/manual/appendix/config/zabbix_server)/[Proxy](/manual/appendix/config/zabbix_proxy) na instância de destino.|


[comment]: # ({/new-cefe2e1b})



[comment]: # ({new-a2a6198d})

#### Resolução de problemas com itens do Agente

 - Se for utilizada verificação passiva, o valor de *Timeout* no arquivo de configuração do Server 
	precisa ser maior do que o *Timeout* definido no arquivo de configuração do Agent. Caso contrário 
	o item pode não receber nenhum valor devido a requisição do Server ao Agent terminar antes.

[comment]: # ({/new-a2a6198d})

[comment]: # ({new-2e3a2e64})

### Usage with command-line utilities**

Note that when testing or using item keys with zabbix_agentd or zabbix_get
from the command line you should consider shell syntax too.

For example, if a certain parameter of the key has to be enclosed in double quotes
you have to explicitly escape double quotes, otherwise they will be trimmed by the shell
as special characters and will not be passed to the Zabbix utility.

Examples:

    $ zabbix_agentd -t 'vfs.dir.count[/var/log,,,"file,dir",,0]'

    $ zabbix_agentd -t vfs.dir.count[/var/log,,,\"file,dir\",,0]

[comment]: # ({/new-2e3a2e64})

[comment]: # ({new-b7e5172f})

** Mandatory and optional parameters **

Parameters without angle brackets are mandatory. Parameters marked with
angle brackets **<** **>** are optional.

** Usage with command-line utilities **

Note that when testing or using item keys with zabbix_agentd or zabbix_get
from the command line you should consider shell syntax too.

For example, if a certain parameter of the key has to be enclosed in double quotes
you have to explicitly escape double quotes, otherwise they will be trimmed by the shell
as special characters and will not be passed to the Zabbix utility.

Examples:

    $ zabbix_agentd -t 'vfs.dir.count[/var/log,,,"file,dir",,0]'

    $ zabbix_agentd -t vfs.dir.count[/var/log,,,\"file,dir\",,0]

[comment]: # ({/new-b7e5172f})

[comment]: # ({new-40450e88})
#### Troubleshooting agent items

-   If used with the passive agent, *Timeout* value in server
    configuration may need to be higher than *Timeout* in the agent
    configuration file. Otherwise the item may not get any value because
    the server request to agent timed out first.

[comment]: # ({/new-40450e88})











