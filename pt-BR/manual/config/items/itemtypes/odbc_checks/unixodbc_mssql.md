[comment]: # translation:outdated

[comment]: # ({new-6fa4a853})
# 4 Recommended UnixODBC settings for MSSQL

[comment]: # ({/new-6fa4a853})

[comment]: # ({new-29c905c8})
#### Installation

-   \*\* Red Hat Enterprise Linux/CentOS\*\*:

```{=html}
<!-- -->
```
    # yum -y install freetds unixODBC 

-   **Debian/Ubuntu**:

Please refer to [FreeTDS user guide](http://www.freetds.org/userguide/)
to download necessary database driver for the corresponding platform.

For some additional information please refer to: [installing
unixODBC](/manual/config/items/itemtypes/odbc_checks/).

[comment]: # ({/new-29c905c8})

[comment]: # ({new-aa4d8324})
#### Configuration

ODBC configuration is done by editing the **odbcinst.ini** and
**odbc.ini** files. These configuration files can be found in */etc*
folder. The file **odbcinst.ini** may be missing and in this case it is
necessary to create it manually.

Please consider the following examples:

**odbcinst.ini**

    $ vi /etc/odbcinst.ini
    [FreeTDS]
    Driver = /usr/lib64/libtdsodbc.so.0

**odbc.ini**

    $ vi /etc/odbc.ini
    [sql1]
    Driver = FreeTDS
    Server = <SQL server 1 IP>
    PORT = 1433
    TDS_Version = 8.0

[comment]: # ({/new-aa4d8324})
