[comment]: # translation:outdated

[comment]: # ({new-596937b2})
# 3 Recommended UnixODBC settings for Oracle

[comment]: # ({/new-596937b2})

[comment]: # ({new-4bfd360f})
#### Installation

Please refer to [Oracle
documentation](https://docs.oracle.com/database/121/ADFNS/adfns_odbc.htm)
for all the necessary instructions.

For some additional information please refer to: [Installing
unixODBC](/manual/config/items/itemtypes/odbc_checks/).

[comment]: # ({/new-4bfd360f})
