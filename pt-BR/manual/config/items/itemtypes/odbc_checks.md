[comment]: # translation:outdated

[comment]: # ({new-68c88a1c})
# - \#15 Monitoração de Banco de dados (ODBC)

[comment]: # ({/new-68c88a1c})

[comment]: # ({new-b5170fd3})
#### - Visão geral

O ODBC é uma API 'middle-ware' escrita em C para o acesso a sistemas
gerenciadores de bancos de dados (DBMS). O conceito do ODBC foi
desenvolvido pela Microsoft e portado para outras plataformas.

O Zabbix pode consultar qualquer banco de dados, bastando para isso que
exista o suporte através do driver ODBC. É importante ressaltar que o
Zabbix não acessa diretamente os SGDBs, todo o acesso é feito através
das interfaces e drivers OBDC. Esta funcionalidade permite uma
monitoração mais eficiente de bancos de dados para diversos propósitos -
por exemplo, verificar filas no banco de dados, estatísticas, quantidade
de registros, etc. O Zabbix suporta o unixODBC, que uma das
implementações mais comuns da API OBDC.

[comment]: # ({/new-b5170fd3})

[comment]: # ({new-bf6fd8ba})
#### - Instalando o unixODBC

A forma que sugerimos a instalação do unixODBC é através do gerenciador
de pacotes do Linux. A maioria das distribuições Linux tem este pacote
em seu repositório padrão. Se ele não estiver disponível em sua
distribuição, poderá ser obtido na página do unixODBC:
<http://www.unixodbc.org/download.html>.

Para instalar o unixODBC com o *yum* (ambientes baseados em
RedHat/Centos/Fedora):

    shell> yum -y install unixODBC unixODBC-devel

Para instalar o unixODBC com o *zypper* (SUSE):

    # zypper in unixODBC-devel

::: noteclassic
O pacote 'unixODBC-devel' é necessário para compilar o
Zabbix com o suporte ao unixODBC.
:::

[comment]: # ({/new-bf6fd8ba})

[comment]: # ({new-eea09ed7})
#### - Instalando os drivers unixODBC

Para cada banco de dados, além do unixOBDC, deverá ser instalado o
driver apropriado. O unixODBC possui uma lista de bancos de dados e
drivers suportados: <http://www.unixodbc.org/drivers.html>. Em algumas
distribuições os drivers OBDC são instalados em conjunto com o unixOBDC.
O comando a seguir instala o driver MySQL através do *yum*:

    shell> yum install mysql-connector-odbc

A mesma instalação pode ser feita em um ambiente SUSE com o comando a
seguir:

    shell> zypper install MyODBC-unixODBC

[comment]: # ({/new-eea09ed7})

[comment]: # ({new-b15e771b})
#### - Configurando o unixODBC

A configuração do ODBC é feita através dos arquivos **odbcinst.ini** e
**odbc.ini**. Para verificar a localização dos mesmos pode ser utilizado
o comando a seguir:

    shell> odbcinst -j

O arquivo **odbcinst.ini** é utilizado para listar os drivers OBDC
instalados:

    [mysql]
    Description = ODBC for MySQL
    Driver      = /usr/lib/libmyodbc5.so

Detalhes dos parâmetros:

|Atributo|Descrição|
|--------|-----------|
|*mysql*|Nome do driver de BD.|
|*Description*|Descrição do driver de BD.|
|*Driver*|Localização do driver de BD.|

O arquivo **odbc.ini** é utilizado para definir as fontes de dados:

    [test]
    Description = MySQL test database
    Driver      = mysql
    Server      = 127.0.0.1
    User        = root
    Password    =
    Port        = 3306
    Database    = zabbix

Detalhes dos parâmetros:

|Atributo|Descrição|
|--------|-----------|
|*test*|Nome da fonte de dados (DSN).|
|*Description*|Descrição da fonte de dados.|
|*Driver*|Nome do driver OBDC, conforme definido no `odbcinst.ini`|
|*Server*|Endereço/DNS do servidor de BD.|
|*User*|Usuário para conexão.|
|*Password*|Senha de conexão.|
|*Port*|Porta do BD.|
|*Database*|Nome da base de dados.|

Para verificar se a conexão OBDC está funcional, é possível um teste via
linha de comando verificando se é possível se conectar ao banco de
dados. Isso pode ser feito através do utilitário **isql** (incluso no
pacote unixOBDC):

    shell> isql test
    +---------------------------------------+
    | Connected!                            |
    |                                       |
    | sql-statement                         |
    | help [tablename]                      |
    | quit                                  |
    |                                       |
    +---------------------------------------+
    SQL>

[comment]: # ({/new-b15e771b})

[comment]: # ({new-9f87c9b6})
#### - Compilando o Zabbix com o suporte ao OBDC

Para habilitar o suporte ao OBDC, o Zabbix precisa ser compilado com o
parâmetro abaixo:

      --with-unixodbc[=ARG]   utilizar o drive OBDC ao invés do pacote unixOBDC

::: noteclassic
Leia mais sobre a instalação do Zabbix em [instalação a
partir dos
fontes](/pt/manual/installation/install#from_the_sources).
:::

[comment]: # ({/new-9f87c9b6})

[comment]: # ({new-ca784f43})
#### - Configuração do item no frontend do Zabbix

Configure um monitoramento de banco de dados
[item](/pt/manual/config/items/item#overview):

![](../../../../../assets/en/manual/config/items/itemtypes/odbc.png)

Para itens de monitoração de banco de dados, os dados abaixo deve ser
fornecidos:

|   |   |
|---|---|
|*Tipo*|Selecione *Monitoração de banco de dados*.|
|*Chave*|Informe **db.odbc.select**\[identificação única, ao nível de nome da fonte de dados\]<br>A identificação única serve para identificar o item em triggers e outros locais.<br>A fonte de dados (DSN) precisa ser definida no arquivo odbc.ini.|
|*Nome de usuário*|Informe o nome de usuário para acesso ao BD (opcional se o usuário já estiver definido em odbc.ini)|
|*Senha*|Informe a senha para o banco de dados (opcional se a senha já for definida em obdc.ini)|
|*Consulta SQL*|Informe a consulta|
|*Tipo da informação*|É muito importante saber qual tipo de informação será retornada pela consulta, pois um tipo incorreto de dados nesta configuração poderá fazer com que o item passe para o estado 'não suportado'.|

[comment]: # ({/new-ca784f43})

[comment]: # ({new-0188aaa2})
#### Item key details

Parameters without angle brackets are mandatory. Parameters marked with angle brackets **<** **>** are optional.

[comment]: # ({/new-0188aaa2})

[comment]: # ({new-13edfcd4})

##### db.odbc.select[<unique short description>,<dsn>,<connection string>] {#db.odbc.select}

<br>
Returns one value, i.e. the first column of the first row of the SQL query result.
Return value: depending on the SQL query.

Parameters:

-   **unique short description** - a unique short description to identify the item (for use in triggers, etc);
-   **dsn** - the data source name (as specified in odbc.ini);
-   **connection string** - the connection string (may contain driver-specific arguments).

Comments:

-   Although `dsn` and `connection string` are optional parameters, at least one of them must be present. If both data source name (DSN) and connection string are defined, the DSN will be ignored.
-   If a query returns more than one column, only the first column is read. If a query returns more than one line, only the first line is read.

[comment]: # ({/new-13edfcd4})

[comment]: # ({new-ebf5d449})

##### db.odbc.get[<unique short description>,<dsn>,<connection string>] {#db.odbc.get}

<br>
Transforms the SQL query result into a JSON array.<br>
Return value: *JSON object*.

Parameters:

-   **unique short description** - a unique short description to identify the item (for use in triggers, etc);
-   **dsn** - the data source name (as specified in odbc.ini);
-   **connection string** - the connection string (may contain driver-specific arguments).

Comments:

-   Although `dsn` and `connection string` are optional parameters, at least one of them must be present. If both data source name (DSN) and connection string are defined, the DSN will be ignored.
-   Multiple rows/columns in JSON format may be returned. This item may be used as a master item that collects all data in one system call, while JSONPath preprocessing may be used in dependent items to extract individual values. For more information, see an [example](/manual/discovery/low_level_discovery/examples/sql_queries#using-db.odbc.get) of the returned format, used in low-level discovery.

Example:

    db.odbc.get[MySQL example,,"Driver=/usr/local/lib/libmyodbc5a.so;Database=master;Server=127.0.0.1;Port=3306"] #connection for MySQL ODBC driver 5

[comment]: # ({/new-ebf5d449})

[comment]: # ({new-c8148c95})

##### db.odbc.discovery[<unique short description>,<dsn>,<connection string>] {#db.odbc.discovery}

<br>
Transforms the SQL query result into a JSON array, used for [low-level disovery](/manual/discovery/low_level_discovery/examples/sql_queries). 
The column names from the query result are turned into low-level discovery macro 
names paired with the discovered field values. These macros can be used in creating 
item, trigger, etc prototypes.<br>
Return value: *JSON object*.

Parameters:

-   **unique short description** - a unique short description to identify the item (for use in triggers, etc);
-   **dsn** - the data source name (as specified in odbc.ini);
-   **connection string** - the connection string (may contain driver-specific arguments).

Comments:

-   Although `dsn` and `connection string` are optional parameters, at least one of them must be present. If both data source name (DSN) and connection string are defined, the DSN will be ignored.

[comment]: # ({/new-c8148c95})

[comment]: # ({new-06f9d2eb})
#### - Observações Importantes

-   A consulta não pode demorar mais do que o tempo definido no
    parâmetro [Timeout](/pt/manual/appendix/config/zabbix_server) do
    arquivo de configuração do Zabbix Server. A partir do Zabbix 2.0.8 o
    parâmetro [Timeout](/pt/manual/appendix/config/zabbix_server) também
    pode ser utilizado para limitar o tempo para a autenticação OBDC
    (observe que dependendo do driver OBDC o 'timeout' de autenticação
    poderá ser ignorado).
-   A consulta precisa retornar somente um valor.
    -   Se a consulta retornar mais de uma coluna, apenas a primeira
        será lida.
    -   Se a consulta retornar masi de uma linha, apenas a primeira
        linha será lida.
-   O comando SQL deverá iniciar com `select`.
-   O comando SQL não pode conter quebra de linha.

Consulte [problemas
conhecidos](/pt/manual/installation/known_issues#odbc_checks) para
verificações ODBC.

[comment]: # ({/new-06f9d2eb})

[comment]: # ({new-718edfdc})
#### - Mensagens de erro

A partir do Zabbix 2.0.8 as mensagens de erro ODBC foram estruturadas em
campso para prover maior detalhamento de informações. Exemplo:

    Cannot execute ODBC query:[SQL_ERROR]:[42601][7][ERROR: syntax error at or near ";"; Error while executing the query]|
    -------------------------  ---------   -----  |  ------------------------------------------------------------------- |
                |                  |         |    `- Código de erro nativo            `- Mensagem de erro.                      `- Separador de registros
                |                  |         `-SQLState
                `- Mensagem Zabbix  `- Código de retorno ODBC

Observe que o tamanho da mensagem de erro é limitado a 2048 bytes, se a
mensagem for maior ela será truncada. Se existir mais de um diagnóstico
OBDC o Zabbix tentará concatenar os valores até o tamanho máximo
permitido.

[comment]: # ({/new-718edfdc})
