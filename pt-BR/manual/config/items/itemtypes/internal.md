[comment]: # attributes: notoc

[comment]: # translation:outdated

[comment]: # ({f3adfa92-8981e17d})
# 8 Verificações internas

[comment]: # ({/f3adfa92-8981e17d})

[comment]: # ({a57cadb0-34928067})
#### Visão geral

Verificações internas permitem monitorar os processos internos do Zabbix.
Em outras palavras, você pode monitorar o que está ocorrendo com o 
Zabbix Server ou Zabbix Proxy.

Verificações internas são calculadas:

-   no Zabbix Server - se o host é monitorado pelo Server
-   no Zabbix Proxy - se o host é monitorado pelo Proxy

Verificações internas são processadas pelo Server ou Proxy independentemente
do estado de manutenção do host.

Para usar este item, escolha o tipo de item **Zabbix interno**.

::: notetip
Verificações internas são processadas pelos poolers do Zabbix.
:::

[comment]: # ({/a57cadb0-34928067})

[comment]: # ({new-5d8f3b63})
#### Performance

Using some internal items may negatively affect performance. These items are:

-   `zabbix[host,,items]`
-   `zabbix[host,,items_unsupported]`
-   `zabbix[hosts]`
-   `zabbix[items]`
-   `zabbix[items_unsupported]`
-   `zabbix[queue]`
-   `zabbix[required_performance]`
-   `zabbix[stats,,,queue]`
-   `zabbix[triggers]`

The [System information](/manual/web_interface/frontend_sections/reports/status_of_zabbix) and [Queue](/manual/web_interface/frontend_sections/administration/queue) 
frontend sections are also affected.

[comment]: # ({/new-5d8f3b63})

[comment]: # ({new-74b446f4})
#### Verificações suportadas

-   Parâmetros sem colchetes angulares são constantes - por exemplo,
    'host' e 'available' em `zabbix[host,<type>,available]`. Use-os na
    chave de item *como são*.
-   Valores para itens e parâmetros de itens que "não são suportados no
    Proxy" só podem ser obtidos se o host estiver sendo monitorado por
    um Server. E vice e versa, valores "não suportados no Server" só podem ser
    obtidos se o host for monitorado por um Proxy.

|Chave|<|<|<|<|<|
|---|-|-|-|-|-|
|▲|Descrição|<|<|Valor de retorno|Comentários|
|zabbix\[boottime\]|<|<|<|<|<|
|<|Tempo de inicialização de processo do Zabbix Server ou Zabbix Proxy em segundos.|<|<|Inteiro.|<|
|zabbix\[cluster,discovery,nodes\]|<|<|<|<|<|
|<|Descoberta de nós de [cluster de alta disponibilidade](/manual/concepts/server/ha).|<|<|JSON.|Este item pode ser usado para descoberta de baixo-nível.|
|zabbix\[history\]|<|<|<|<|<|
|<|Número de valores armazenados na tabela HISTORY.|<|<|Inteiro.|Não use se utilizando MySQL InnoDB, Oracle ou PostgreSQL!<br>*(não suportado no Proxy)*|
|zabbix\[history\_log\]|<|<|<|<|<|
|<|Número de valores armazenados na tabela HISTORY\_LOG.|<|<|Inteiro.|Não use se utilizando MySQL InnoDB, Oracle ou PostgreSQL!<br>*(não suportado no Proxy)*|
|zabbix\[history\_str\]|<|<|<|<|<|
|<|Número de valores armazenados na tabela HISTORY\_STR.|<|<|Inteiro.|Não use se utilizando MySQL InnoDB, Oracle ou PostgreSQL!<br>*(não suportado no Proxy)*|
|zabbix\[history\_text\]|<|<|<|<|<|
|<|Número de valores armazenados na tabela HISTORY\_TEXT.|<|<|Inteiro.|Não use se utilizando MySQL InnoDB, Oracle ou PostgreSQL!<br>*(não suportado no Proxy)*|
|zabbix\[history\_uint\]|<|<|<|<|<|
|<|Número de valores armazenados na tabela HISTORY\_UINT.|<|<|Inteiro.|Não use se utilizando MySQL InnoDB, Oracle ou PostgreSQL!<br>Este item é suportado desde o Zabbix 1.8.3.<br>*(não suportado no Proxy)*|
|zabbix\[host,,items\]|<|<|<|<|<|
|<|Número de itens habilitados (suportado e não suportado) no host.|<|<|Inteiro.|Este item é suportado desde o Zabbix 3.0.0.|
|zabbix\[host,,items\_unsupported\]|<|<|<|<|<|
|<|Número de itens não suportados habilitados no host.|<|<|Inteiro.|Este item é suportado desde o Zabbix 3.0.0.\*|
|zabbix\[host,,maintenance\]|<|<|<|<|<|
|<|Estado de manutenção atual de um host.|<|<|0 - host em estado normal,<br>1 - host em manutenção com coleta de dados,<br>2 - host em manutenção sem coleta de dados.|Este item é sempre processado pelo Zabbix Server independentemente da localização do host (no Server ou Proxy). O Proxy não receberá este item com dado de configuração.<br>O segundo parâmetro deve estar vazio e é reservado para uso futuro.|
|zabbix\[host,discovery,interfaces\]|<|<|<|<|<|
|<|Detalhes de todas as interfaces configuradas no host do Zabbix Frontend.|<|<|Objeto JSON.|Este item pode ser usado em [descoberta de baixo-nível](/manual/discovery/low_level_discovery/examples/host_interfaces).<br>Este item é suportado desde o Zabbix 3.4.0.<br>*(não suportado no Proxy)*|
|zabbix\[host,<type>,available\]|<|<|<|<|<|
|<|Disponibilidade da interface principal de um tipo particular de verificações no host.|<|<|0 - não disponível, 1 - disponível, 2 - desconhecido.|**Tipos (type)** válidos são:<br>*agent*, *snmp*, *ipmi*, *jmx*<br><br>O valor do item é calculado de acordo com os parâmetros de configuração em relação à [inacessibilidade/indisponibilidade](/manual/appendix/items/unreachability) do host.<br><br>Este item é suportado desde o Zabbix 2.0.0.<br>|
|zabbix\[hosts\]|<|<|<|<|<|
|<|Número de hosts monitorados.|<|<|Inteiro.|<|
|zabbix\[items\]|<|<|<|<|<|
|<|Número de itens habilitados (suportado e não suportado).|<|<|Inteiro.|<|
|zabbix\[items\_unsupported\]|<|<|<|<|<|
|<|Número de itens não suportados.|<|<|Inteiro.|<|
|zabbix\[java,,<param>\]|<|<|<|<|<|
|<|Informação sobre Zabbix Java Gateway.|<|<|Se <param> for *ping*, "1" é retornado. Pode ser usado para verificar disponibilidade do Java Gateway usando a função de gatilho nodata().<br><br>Se <param> for *version*, a versão do Java Gateway é retornada. Exemplo: "2.0.0".|Valores válidos para **param** são:<br>*ping*, *version*<br><br>O segundo parâmetro deve estar vazio e é reservado para uso futuro.|
|zabbix\[lld\_queue\]|<|<|<|<|<|
|<|Contagem de valores enfileirados na fila de processamento de descobertas de baixo-nível.|<|<|Inteiro.|Este item pode ser usado para monitorar o comprimento da fila de processamento de descobertas de baixo-nível.<br><br>Este item é suportado desde o Zabbix 4.2.0.|
|zabbix\[preprocessing\_queue\]|<|<|<|<|<|
|<|Contagem de valores enfileirados na fila de processamento.|<|<|Inteiro.|Este item pode ser usado para monitorar o comprimento da fila de processamento.<br><br>Este item é suportado desde o Zabbix 3.4.0.|
|zabbix\[process,<type>,<mode>,<state>\]|<|<|<|<|<|
|<|Tempo gasto em <state> por um processo em particular do Zabbix ou um grupo de processos (identificado por <type> e <mode>) em porcentagem. Ele é calculado apenas para o último minuto.<br><br>Se <mode> é o número do processo Zabbix que não está em execução (por exemplo, com 5 pollers em execução <mode> é definido como 6), tal item passará para o estado não suportado.<br>Mínimo e máximo se referem à porcentagem de uso para um processo único. Assim, se em um grupo de 3 pollers a porcentagem de uso por processo for 2, 18 e 66, min retornaria 2 e max retornaria 66.<br>Os processos informam o que estão fazendo em memória compartilhada e o processo de automonitoramento resume tais dados a cada segundo. Mudanças de estado (ocupado/ocioso) são registradas na mudança - portanto um processo que se torna ocupado se registra como tal e não muda ou atualiza seu estado até que se torne ocioso. Isto garante que mesmo processos totalmente suspensos sejam corretamente registrados como 100% ocupados.<br>Atualmente, "ocupado" significa "não dormente", mas no futuro estados adicionais podem ser introduzidos - aguardando por travas, executando consultas de banco de dados, etc.<br>No Linux e na maioria dos outros sistemas, a resolução é 1/100 de um segundo.|<|<|Porcentagem de tempo.<br>float.|**Tipos** de [processos de Server](/manual/concepts/server#server_process_types) suportados:<br>*alert manager*, *alert syncer*, *alerter*, *availability manager*, *configuration syncer*, *discoverer*, *escalator*, *history poller*, *history syncer*, *housekeeper*, *http poller*, *icmp pinger*, *ipmi manager*, *ipmi poller*, *java poller*, *lld manager*, *lld worker*, *odbc poller*, *poller*, *preprocessing manager*, *preprocessing worker*, *proxy poller*, *self-monitoring*, *snmp trapper*, *task manager*, *timer*, *trapper*, *unreachable poller*, *vmware collector*<br><br>**Tipos** de [processos de Proxy](/manual/concepts/proxy#proxy_process_types) suportados:<br>*availability manager*, *configuration syncer*, *data sender*, *discoverer*, *heartbeat sender*, *history poller*, *history syncer*, *housekeeper*, *http poller*, *icmp pinger*, *ipmi manager*, *ipmi poller*, *java poller*, *odbs poller*, *poller*, *preprocessing manager*, *preprocessing worker*, *self-monitoring*, *snmp trapper*, *task manager*, *trapper*, *unreachable poller*, *vmware collector*<br><br>**Modos (modes)** válidos são:<br>*avg* - valor médio para todos os processos de um dado tipo (padrão)<br>*count* - retorna o número de forks para um dado tipo de processo, <state> não deve ser especificado<br>*max* - valor máximo<br>*min* - valor mínimo<br>*<process number>* - número do processo (entre 1 e o número de instâncias pre-forked). Por exemplo, se 4 trappers estão em execução, o valor está entre 1 e 4.<br><br>**Estados (states)** válidos são:<br>*busy* - processo está no estado ocupado, por exemplo, processando requisição (padrão).<br>*idle* - processo está em estado ocioso, fazendo nada.<br><br>Exemplos:<br>=> zabbix\[process,poller,avg,busy\] → tempo médio de processo de poller gasto fazendo alguma coisa durante o último minuto<br>=> zabbix\[process,"icmp pinger",max,busy\] → tempo máximo gasto fazendo alguma coisa por qualquer processo de ping ICMP durante o último minuto<br>=> zabbix\[process,"history syncer",2,busy\] → tempo gasto fazendo alguma coisa pelo sincronizador de histórico número 2 no último minuto<br>=> zabbix\[process,trapper,count\] → quantidade de processos trapper atualmente em execução|
|zabbix\[proxy,<name>,<param>\]|<|<|<|<|<|
|<|Informação sobre Zabbix Proxy.|<|<|Inteiro.|**name**: nome do Proxy<br><br>Valores válidos para **param** são:<br>*lastaccess* - registro de data da última mensagem de heartbeat recebida do Proxy<br>*delay* - por quanto tempo os valores coletados não são enviados, calculados como "proxy delay" (diferença entre o tempo atual do Proxy e o registro de data valor não enviado mais antigo no Proxy) + ("tempo atual do Server" - "último acesso do Proxy")<br><br>Exemplo:<br>=> zabbix\[proxy,"Germany",lastaccess\]<br><br>A [função](/manual/appendix/functions/history) `fuzzytime()` pode ser usada para verificar a disponibilidade de Proxies.<br>Este item é sempre processado pelo Zabbix Server independentemente da localização do host (no Server ou Proxy).|
|zabbix\[proxy\_history\]|<|<|<|<|<|
|<|Número de valores na tabela history do Proxy aguardando serem enviados para o Server.|<|<|Inteiro.|*(não suportado no Server)*|
|zabbix\[queue,<from>,<to>\]|<|<|<|<|<|
|<|Número de itens monitorados na fila que está atrasada por pelo menos <from> segundos mas menos do que <to> segundos.|<|<|Inteiro.|**from** - padrão: 6 segundos<br>**to** - padrão: infinito<br>[Símbolos de unidade de tempo](/manual/appendix/suffixes) (s,m,h,d,w) são suportados para estes parâmetros.|
|zabbix\[rcache,<cache>,<mode>\]|<|<|<|<|<|
|<|Estatísticas de disponibilidade do cache de configuração do Zabbix.|<|<|Inteiro (para tamanho); float (para porcentagem).|**cache**: *buffer*<br><br>**Modos (modes)** válidos são:<br>*total* - tamanho total do buffer<br>*free* - tamanho do buffer livre<br>*pfree* - porcentagem do buffer livre<br>*used* - tamanho do buffer usado<br>*pused* - porcentagem do buffer usado<br><br>O modo *pused* é suportado desde o Zabbix 4.0.0.|
|zabbix\[requiredperformance\]|<|<|<|<|<|
|<|Performance requerida do Zabbix Server ou Zabbix Proxy, em novos valores por segundo esperados.|<|<|float.|Aproximadamente correlaciona-se com "Performance requerida do Server, novos valores por segundo" em *Relatórios → [Informação do sistema](/manual/web_interface/frontend_sections/reports/status_of_zabbix)*.|
|zabbix\[stats,<ip>,<port>\]|<|<|<|<|<|
|<|Métricas internas remotas do Zabbix Server ou Proxy.|<|<|Objeto JSON.|**ip** - lista IP/DNS/máscara de rede de Servers/Proxies para serem consultados remotamente (padrão é 127.0.0.1)<br>**port** - porta do Server/Proxy a ser consultada remotamente (padrão é 10051)<br><br>Note que as solicitações de estatísticas (stats) somente serão aceitas de endereços listados no parâmetro 'StatsAllowedIP' do [Server](/manual/appendix/config/zabbix_server)/[Proxy](/manual/appendix/config/zabbix_proxy) na instância de destino.<br><br>Um conjunto selecionado de métricas internas é retornado por este item. Para detalhes, consulte [Monitoramento remoto de estatísticas do Zabbix](/manual/appendix/items/remote_stats#exposed_metrics).<br><br>Suportado desde 4.2.0.|
|zabbix\[stats,<ip>,<port>,queue,<from>,<to>\]|<|<|<|<|<|
|<|Métricas de fila internas do Zabbix Server ou Proxy (veja `zabbix[queue,<from>,<to>]`).|<|<|Objeto JSON.|**ip** - lista IP/DNS/máscara de rede de Servers/Proxies para serem consultados remotamente (padrão é 127.0.0.1)<br>**port** - porta do Server/Proxy a ser consultada remotamente (padrão é 10051)<br>**from** - atrasado por pelo menos (padrão é 6 segundos)<br>**to** - atrasado por no máximo (padrão é infinito)<br><br>Note que as solicitações de estatísticas (stats) somente serão aceitas de endereços listados no parâmetro 'StatsAllowedIP' do [Server](/manual/appendix/config/zabbix_server)/[Proxy](/manual/appendix/config/zabbix_proxy) na instância de destino.<br><br>Suportado desde 4.2.0.|
|zabbix\[tcache,cache,<parameter>\]|<|<|<|<|<|
|<|Estatísticas de efetividade do cache de função de tendência do Zabbix.|<|<|Inteiro (para tamanho); float (para porcentagem).|**Parâmetros (parameter)** válidos são:<br>*all* - solicitações totais de cache (padrão)<br>*hits* - acertos de cache<br>*phits* - porcentagem de acertos de cache<br>*misses* - falhas de cache<br>*pmisses* - porcentagem de falhas de cache<br>*items* - o número de itens em cache<br>*requests* - o número de solicitações em cache<br>*pitems* - porcentagem de itens em cache a partir de itens em cache + solicitações. Baixa porcentagem provavelmente significa que o tamanho do cache pode ser reduzido.<br><br>Suportado desde 5.4.0.<br><br>*(não suportado no Proxy)*|
|zabbix\[trends\]|<|<|<|<|<|
|<|Número de valores armazenados na tabela TRENDS.|<|<|Inteiro.|Não use se utilizando MySQL InnoDB, Oracle ou PostgreSQL!<br>*(não suportado no Proxy)*|
|zabbix\[trends\_uint\]|<|<|<|<|<|
|<|Número de valores armazenados na tabela TRENDS\_UINT.|<|<|Inteiro.|Não use se utilizando MySQL InnoDB, Oracle ou PostgreSQL!<br>Este item é suportado desde o Zabbix 1.8.3.<br>*(não suportado no Proxy)*|
|zabbix\[triggers\]|<|<|<|<|<|
|<|Número de gatilhos (trigger) habilitados no banco de dados do Zabbix, com todos os itens habilitados em hosts habilitados.|<|<|Inteiro.|*(não suportado no Proxy)*|
|zabbix\[uptime\]|<|<|<|<|<|
|<|Tempo ativo de processo do Zabbix Server ou Zabbix Proxy em segundos.|<|<|Inteiro.|<|
|zabbix\[vcache,buffer,<mode>\]|<|<|<|<|<|
|<|Estatísticas de disponibilidade do cache de valor do Zabbix.|<|<|Inteiro (para tamanho); float (para porcentagem).|**Modos (modes)** válidos são:<br>*total* - tamanho total do buffer<br>*free* - tamanho do buffer livre<br>*pfree* - porcentagem de buffer livre<br>*used* - tamanho de buffer usado<br>*pused* - porcentagem de buffer usado<br><br>*(não suportado no Proxy)*|
|zabbix\[vcache,cache,<parameter>\]|<|<|<|<|<|
|<|Estatísticas de efetividade do cache de valor do Zabbix.|<|<|Inteiro.<br><br>Com o parâmetro *mode*:<br>0 - modo normal,<br>1 - modo de baixa memória|**Parâmetros (parameter)** válidos são:<br>*requests* - número total de solicitações<br>*hits* - número de acertos de cache (valores históricos retirados do cache)<br>*misses* - número de falhas de cache (valores históricos retirados do banco de dados)<br>*mode* - valor de modo de operação de cache<br><br>Este item é suportado desde o Zabbix 2.2.0 e o parâmetro *mode* desde o Zabbix 3.0.0.<br>*(não suportado no Proxy)*<br><br>Uma vez que o modo de baixa memória seja ativado, o cache de valor permanecerá neste estado por 24 horas, mesmo que o problema que provocou este modo seja resolvido antes.<br><br>Você pode usar esta chave com a etapa de pré-processamento *Alterações por segundo* de modo a obter estatísticas de valores por segundo.|
|zabbix\[version\]|<|<|<|<|<|
|<|Versão do Zabbix Server ou Proxy.|<|<|String.|Este item é suportado desde o Zabbix 5.0.0.<br><br>Exemplo de valor de retorno: 5.0.0beta1|
|zabbix\[vmware,buffer,<mode>\]|<|<|<|<|<|
|<|Availability statistics of Zabbix vmware cache.|<|<|Inteiro (para tamanho); float (para porcentagem).|**Modos (modes)** válidos são:<br>*total* - tamanho total de buffer<br>*free* - tamanho de buffer livre<br>*pfree* - porcentagem de buffer livre<br>*used* - tamanho de buffer usado<br>*pused* - porcentagem de buffer usado|
|zabbix\[wcache,<cache>,<mode>\]|<|<|<|<|<|
|<|Estatísticas e disponibilidade de cache de escrita do Zabbix.|<|<|<|É obrigatório especificar <cache>.|
|<|**Cache**|**Mode**|<|<|<|
|^|valores|all<br>*(padrão)*|Número total de valores processados pelo Zabbix Server ou Zabbix Proxy, exceto itens não suportados.|Inteiro.|Contador (counter).<br>Você deve usar esta chave com a etapa de pré-processamento *Alterações por segundo* de modo a obter estatísticas de valores por segundo.|
|^|^|float|Número de valores flutuantes (float) processados.|Inteiro.|Contador (counter).|
|^|^|uint|Número de valores inteiros (unsigned) processados.|Inteiro.|Contador (counter).|
|^|^|str|Número de valores caracter/string processados.|Inteiro.|Contador (counter).|
|^|^|log|Número de valores de log processados.|Inteiro.|Contador (counter).|
|^|^|text|Número de valores de texto processados.|Inteiro.|Contador (counter).|
|^|^|not supported|Número de vezes que processamento de item resultou no item se tornando não suportado ou mantendo este estado.|Inteiro.|Contador (counter).<br>|
|^|history|pfree<br>*(padrão)*|Porcentagem de buffer histórico livre.|Float.|O cache histórico é usado para armazenar valores de item. Um número baixo indica problemas de performance no lado do banco de dados.|
|^|^|free|Tamanho do buffer histórico livre.|Inteiro.|<|
|^|^|total|Tamanho total do buffer histórico.|Inteiro.|<|
|^|^|used|Tamanho do buffer histórico usado.|Inteiro.|<|
|^|^|pused|Porcentagem de buffer histórico usado.|Float.|O modo *pused* é suportado desde o Zabbix 4.0.0.|
|^|index|pfree<br>*(padrão)*|Porcentagem de buffer de índice histórico livre.|Float.|O cache de índice histórico é usado para indexar valores armazenados no cache histórico.<br>O cache de índice (*index*) é suportado desde o Zabbix 3.0.0.|
|^|^|free|Tamanho do buffer de índice histórico livre.|Inteiro.|<|
|^|^|total|Tamanho total do buffer de índice histórico.|Inteiro.|<|
|^|^|used|Tamanho do buffer de índice histórico usado.|Inteiro.|<|
|^|^|pused|Porcentagem do buffer de índice histórico usado.|Float.|O modo *pused* é suportado desde o Zabbix 4.0.0.|
|^|trend|pfree<br>*(padrão)*|Porcentagem de cache de tendência livre.|Float.|O cache de tendência armazena agregados para a hora atual de todos os itens que recebem dados.<br>*(não suportado no Proxy)*|
|^|^|free|Tamanho do buffer de tendência livre.|Inteiro.|*(não suportado no Proxy)*|
|^|^|total|Tamanho total do buffer de tendência.|Inteiro.|*(não suportado no Proxy)*|
|^|^|used|Tamanho do buffer de tendência usado.|Inteiro.|*(não suportado no Proxy)*|
|^|^|pused|Porcentagem de buffer de tendência usado.|Float.|*(não suportado no Proxy)*<br><br>O modo *pused* é suportado desde o Zabbix 4.0.0.|

[comment]: # ({/new-74b446f4})

[comment]: # ({new-0f6066ab})

### Item key details

-   Parameters without angle brackets are constants - for example,
    'host' and 'available' in `zabbix[host,<type>,available]`. Use them in the item key *as is*.
-   Values for items and item parameters that are "not supported on proxy" can only be retrieved if the host is monitored by server. And vice versa, values "not supported on server" can only be retrieved if the host is monitored by proxy.

[comment]: # ({/new-0f6066ab})

[comment]: # ({new-e52bbd1a})

##### zabbix[boottime] {#boottime}

<br>
The startup time of Zabbix server or Zabbix proxy process in seconds.<br>
Return value: *Integer*.

[comment]: # ({/new-e52bbd1a})

[comment]: # ({new-d1cde038})

##### zabbix[cluster,discovery,nodes] {#cluster.discovery}

<br>
Discovers the [high availability cluster](/manual/concepts/server/ha) nodes.<br>
Return value: *JSON object*.

This item can be used in low-level discovery.

[comment]: # ({/new-d1cde038})

[comment]: # ({new-09c4f0cf})

##### zabbix[connector_queue] {#connector.queue}

<br>
The count of values enqueued in the connector queue.<br>
Return value: *Integer*.

This item is supported since Zabbix 6.4.0.

[comment]: # ({/new-09c4f0cf})

[comment]: # ({new-fcd989ee})

##### zabbix[discovery_queue] {#discovery.queue}

<br>
The count of network checks enqueued in the discovery queue.<br>
Return value: *Integer*.

[comment]: # ({/new-fcd989ee})

[comment]: # ({new-f065c85f})

##### zabbix[host,,items] {#host.items}

<br>
The number of enabled items (supported and not supported) on the host.<br>
Return value: *Integer*.

[comment]: # ({/new-f065c85f})

[comment]: # ({new-31a0bdcc})

##### zabbix[host,,items_unsupported] {#host.items.unsupported}

<br>
The number of enabled unsupported items on the host.<br>
Return value: *Integer*.

[comment]: # ({/new-31a0bdcc})

[comment]: # ({new-6220db31})

##### zabbix[host,,maintenance] {#maintenance}

<br>
The current maintenance status of the host.<br>
Return values: *0* - normal state; *1* - maintenance with data collection; *2* - maintenance without data collection.

Comments:

-   This item is always processed by Zabbix server regardless of the host location (on server or proxy). The proxy will not receive this item with configuration data. 
-   The second parameter must be empty and is reserved for future use.

[comment]: # ({/new-6220db31})

[comment]: # ({new-ec7187d4})

##### zabbix[host,active_agent,available] {#active.available}

<br>
The availability of active agent checks on the host.<br>
Return values: *0* - unknown; *1* - available; *2* - not available.

[comment]: # ({/new-ec7187d4})

[comment]: # ({new-48ad1e26})

##### zabbix[host,discovery,interfaces] {#discovery.interfaces}

<br>
The details of all configured interfaces of the host in Zabbix frontend.<br>
Return value: *JSON object*.

Comments:

-   This item can be used in [low-level discovery](/manual/discovery/low_level_discovery/examples/host_interfaces);
-   This item is not supported on Zabbix proxy.

[comment]: # ({/new-48ad1e26})

[comment]: # ({new-9752a684})

##### zabbix[host,<type>,available] {#host.available}

<br>
The availability of the main interface of a particular type of checks on the host.<br>
Return values: *0* - not available; *1* - available; *2* - unknown.

Comments:

-   Valid **types** are: *agent*, *snmp*, *ipmi*, *jmx*;
-   The item value is calculated according to the configuration parameters regarding host [unreachability/unavailability](/manual/appendix/items/unreachability).

[comment]: # ({/new-9752a684})

[comment]: # ({new-a65ef5bf})

##### zabbix[hosts] {#hosts}

<br>
The number of monitored hosts.<br>
Return value: *Integer*.

[comment]: # ({/new-a65ef5bf})

[comment]: # ({new-f291ac50})

##### zabbix[items] {#items}

<br>
The number of enabled items (supported and not supported).<br>
Return value: *Integer*.

[comment]: # ({/new-f291ac50})

[comment]: # ({new-53295b6d})

##### zabbix[items_unsupported] {#items.unsupported}

<br>
The number of unsupported items.<br>
Return value: *Integer*.

[comment]: # ({/new-53295b6d})

[comment]: # ({new-ba37fff4})

##### zabbix[java,,<param>] {#java}

<br>
The information about Zabbix Java gateway.<br>
Return values: *1* - if <param> is *ping*; *Java gateway version* -  if <param> is *version* (for example: "2.0.0").

Comments:

-   Valid values for **param** are: *ping*, *version*;
-   This item can be used to check Java gateway availability using the `nodata()` trigger function;
-   The second parameter must be empty and is reserved for future use.

[comment]: # ({/new-ba37fff4})

[comment]: # ({new-8f8235d6})

##### zabbix[lld_queue] {#lld.queue}

<br>
The count of values enqueued in the low-level discovery processing queue.<br>
Return value: *Integer*.

This item can be used to monitor the low-level discovery processing queue length.

[comment]: # ({/new-8f8235d6})

[comment]: # ({new-3e07a93d})

##### zabbix[preprocessing_queue] {#preprocessing.queue}

<br>
The count of values enqueued in the preprocessing queue.<br>
Return value: *Integer*.

This item can be used to monitor the preprocessing queue length.

[comment]: # ({/new-3e07a93d})

[comment]: # ({new-b74b9537})

##### zabbix[process,<type>,<mode>,<state>] {#process}

<br>
The percentage of time a particular Zabbix process or a group of processes (identified by \<type\> and \<mode\>) spent in \<state\>. It is calculated for the last minute only.<br>
Return value: *Float*.

Parameters:

-   **type** - for [server processes](/manual/concepts/server#server_process_types): *alert manager*, *alert syncer*, *alerter*, *availability manager*, *configuration syncer*, *connector manager*, *connector worker*, *discoverer*, *escalator*, *history poller*, *history syncer*, *housekeeper*, *http poller*, *icmp pinger*, *ipmi manager*, *ipmi poller*, *java poller*, *lld manager*, *lld worker*, *odbc poller*, *poller*, *preprocessing manager*, *preprocessing worker*, *proxy poller*, *self-monitoring*, *snmp trapper*, *task manager*, *timer*, *trapper*, *unreachable poller*, *vmware collector*;<br>for [proxy processes](/manual/concepts/proxy#proxy_process_types): *availability manager*, *configuration syncer*, *data sender*, *discoverer*, *history syncer*, *housekeeper*, *http poller*, *icmp pinger*, *ipmi manager*, *ipmi poller*, *java poller*, *odbc poller*, *poller*, *preprocessing manager*, *preprocessing worker*, *self-monitoring*, *snmp trapper*, *task manager*, *trapper*, *unreachable poller*, *vmware collector*
-   **mode** - *avg* - average value for all processes of a given type (default)<br>*count* - returns number of forks for a given process type, <state> should not be specified<br>*max* - maximum value<br>*min* - minimum value<br>*<process number>* - process number (between 1 and the number of pre-forked instances). For example, if 4 trappers are running, the value is between 1 and 4.
-   **state** - *busy* - process is in busy state, for example, the processing request (default); *idle* - process is in idle state doing nothing.

Comments:

-   If \<mode\> is a Zabbix process number that is not running (for example, with 5 pollers running the \<mode\> is specified to be 6), such an item will turn unsupported;
-   Minimum and maximum refers to the usage percentage for a single process. So if in a group of 3 pollers usage percentages per process were 2, 18 and 66, min would return 2 and max would return 66.
-   Processes report what they are doing in shared memory and the self-monitoring process summarizes that data each second. State changes (busy/idle) are registered upon change - thus a process that becomes busy registers as such and doesn't change or update the state until it becomes idle. This ensures that even fully hung processes will be correctly registered as 100% busy.
-   Currently, "busy" means "not sleeping", but in the future additional states might be introduced - waiting for locks, performing database queries, etc.
-   On Linux and most other systems, resolution is 1/100 of a second.

Examples:

    zabbix[process,poller,avg,busy] #the average time of poller processes spent doing something during the last minute
    zabbix[process,"icmp pinger",max,busy] #the maximum time spent doing something by any ICMP pinger process during the last minute
    zabbix[process,"history syncer",2,busy] #the time spent doing something by history syncer number 2 during the last minute
    zabbix[process,trapper,count] #the amount of currently running trapper processes

[comment]: # ({/new-b74b9537})

[comment]: # ({new-d90bc125})

##### zabbix[proxy,<name>,<param>] {#proxy}

<br>
The information about Zabbix proxy.<br>
Return value: *Integer*.

Parameters:

-   **name** - the proxy name;
-   **param** - *delay* - how long the collected values are unsent, calculated as "proxy delay" (difference between the current proxy time and the timestamp of the oldest unsent value on proxy) + ("current server time" - "proxy lastaccess").

[comment]: # ({/new-d90bc125})

[comment]: # ({new-fd9ea3f4})

##### zabbix[proxy,discovery] {#proxy.discovery}

<br>
The list of Zabbix proxies with name, mode, encryption, compression, version, last seen, host count, item count, required values per second (vps) and version status (current/outdated/unsupported).<br>
Return value: *JSON object*.

[comment]: # ({/new-fd9ea3f4})

[comment]: # ({new-5b463b30})

##### zabbix[proxy_history] {#proxy.history}

<br>
The number of values in the proxy history table waiting to be sent to the server.<br>
Return values: *Integer*.

This item is not supported on Zabbix server.

[comment]: # ({/new-5b463b30})

[comment]: # ({new-ebe20dec})

##### zabbix[queue,<from>,<to>] {#queue}

<br>
The number of monitored items in the queue which are delayed at least by \<from\> seconds, but less than \<to\> seconds.<br>
Return value: *Integer*.

Parameters:

-   **from** - default: 6 seconds;
-   **to** - default: infinity.

[Time-unit symbols](/manual/appendix/suffixes) (s,m,h,d,w) are supported in the parameters.

[comment]: # ({/new-ebe20dec})

[comment]: # ({new-04aafd0c})

##### zabbix[rcache,<cache>,<mode>] {#rcache}

<br>
The availability statistics of the Zabbix configuration cache.<br>
Return values: *Integer* (for size); *Float* (for percentage).

Parameters:

-   **cache** - *buffer*;
-   **mode** - *total* - the total size of buffer<br>*free* - the size of free buffer<br>*pfree* - the percentage of free buffer<br>*used* - the size of used buffer<br>*pused* - the percentage of used buffer

[comment]: # ({/new-04aafd0c})

[comment]: # ({new-f2b37a90})

##### zabbix[requiredperformance] {#required.performance}

<br>
The required performance of Zabbix server or Zabbix proxy, in new values per second expected.<br>
Return value: *Float*.

Approximately correlates with "Required server performance, new values per second" in *Reports → [System information](/manual/web_interface/frontend_sections/reports/status_of_zabbix)*.

[comment]: # ({/new-f2b37a90})

[comment]: # ({new-8054c8ab})

##### zabbix[stats,<ip>,<port>] {#stats}

<br>
The internal metrics of a remote Zabbix server or proxy.<br>
Return values: *JSON object*.

Parameters:

-   **ip** - the IP/DNS/network mask list of servers/proxies to be remotely queried (default is 127.0.0.1);
-   **port** - the port of server/proxy to be remotely queried (default is 10051).

Comments:

-   The stats request will only be accepted from the addresses listed in the 'StatsAllowedIP' [server](/manual/appendix/config/zabbix_server)/[proxy](/manual/appendix/config/zabbix_proxy) parameter on the target instance;
-   A selected set of internal metrics is returned by this item. For details, see [Remote monitoring of Zabbix stats](/manual/appendix/items/remote_stats#exposed_metrics).

[comment]: # ({/new-8054c8ab})

[comment]: # ({new-d7c2c9be})

##### zabbix[stats,<ip>,<port>,queue,<from>,<to>] {#stats.queue}

<br>
The internal queue metrics (see `zabbix[queue,<from>,<to>]`) of a remote Zabbix server or proxy.<br>
Return values: *JSON object*.

Parameters:

-   **ip** - the IP/DNS/network mask list of servers/proxies to be remotely queried (default is 127.0.0.1);
-   **port** - the port of server/proxy to be remotely queried (default is 10051);
-   **from** - delayed by at least (default is 6 seconds);
-   **to** - delayed by at most (default is infinity).

Comments:

-   The stats request will only be accepted from the addresses listed in the 'StatsAllowedIP' [server](/manual/appendix/config/zabbix_server)/[proxy](/manual/appendix/config/zabbix_proxy) parameter on the target instance;
-   A selected set of internal metrics is returned by this item. For details, see [Remote monitoring of Zabbix stats](/manual/appendix/items/remote_stats#exposed_metrics).

[comment]: # ({/new-d7c2c9be})

[comment]: # ({new-c688d590})

##### zabbix[tcache,<cache>,<parameter>] {#tcache}

<br>
The effectiveness statistics of the Zabbix trend function cache.<br>
Return values: *Integer* (for size); *Float* (for percentage).

Parameters:

-   **cache** - *buffer*;
-   **mode** - **all* - total cache requests (default)<br>*hits* - cache hits<br>*phits* - percentage of cache hits<br>*misses* - cache misses<br>*pmisses* - percentage of cache misses<br>*items* - the number of cached items<br>*requests* - the number of cached requests<br>*pitems* - percentage of cached items from cached items + requests. Low percentage most likely means that the cache size can be reduced.

This item is not supported on Zabbix proxy.

[comment]: # ({/new-c688d590})

[comment]: # ({new-9699e4ca})

##### zabbix[triggers] {#triggers}

<br>
The number of enabled triggers in Zabbix database, with all items enabled on enabled hosts.<br>
Return value: *Integer*.

This item is not supported on Zabbix proxy.

[comment]: # ({/new-9699e4ca})

[comment]: # ({new-a5c8cdec})

##### zabbix[uptime] {#uptime}

<br>
The uptime of the Zabbix server or proxy process in seconds.<br>
Return value: *Integer*.

[comment]: # ({/new-a5c8cdec})

[comment]: # ({new-304cb09c})

##### zabbix[vcache,buffer,<mode>] {#vcache}

<br>
The availability statistics of the Zabbix value cache.<br>
Return values: *Integer* (for size); *Float* (for percentage).

Parameter:

-   **mode** - *total* - the total size of buffer<br>*free* - the size of free buffer<br>*pfree* - the percentage of free buffer<br>*used* - the size of used buffer<br>*pused* - the percentage of used buffer

This item is not supported on Zabbix proxy.

[comment]: # ({/new-304cb09c})

[comment]: # ({new-1454b994})

##### zabbix[vcache,cache,<parameter>] {#vcache.parameter}

<br>
The effectiveness statistics of the Zabbix value cache.<br>
Return values: *Integer*. With the *mode* parameter returns: 0 - normal mode; 1 - low memory mode.

Parameters:

-   **parameter** - *requests* - the total number of requests<br>*hits* - the number of cache hits (history values taken from the cache)<br>*misses* - the number of cache misses (history values taken from the database)<br>*mode* - the value cache operating mode

Comments:

-   Once the low-memory mode has been switched on, the value cache will remain in this state for 24 hours, even if the problem that triggered this mode is resolved sooner;
-   You may use this key with the *Change per second* preprocessing step in order to get values-per-second statistics;
-   This item is not supported on Zabbix proxy.

[comment]: # ({/new-1454b994})

[comment]: # ({new-ddde46a0})

##### zabbix[version] {#version}

<br>
The version of Zabbix server or proxy.<br>
Return value: *String*. For example: `6.0.0beta1`.

[comment]: # ({/new-ddde46a0})

[comment]: # ({new-dda0c008})

##### zabbix[vmware,buffer,<mode>] {#vmware}

<br>
The availability statistics of the Zabbix vmware cache.<br>
Return values: *Integer* (for size); *Float* (for percentage).

Parameters:

-   **mode** - *total* - the total size of buffer<br>*free* - the size of free buffer<br>*pfree* - the percentage of free buffer<br>*used* - the size of used buffer<br>*pused* - the percentage of used buffer

[comment]: # ({/new-dda0c008})

[comment]: # ({new-9fa76fcd})

##### zabbix[wcache,<cache>,<mode>] {#wcache}

<br>
The statistics and availability of the Zabbix write cache.<br>
Return values: *Integer* (for number/size); *Float* (for percentage).

Parameters:

-   **cache** - *values*, *history*, *index*, or *trend*;
-   **mode** - (with *values*) *all* (default) - the total number of values processed by Zabbix server/proxy, except unsupported items (counter)<br>*float* - the number of processed float values (counter)<br>*uint* - the number of processed unsigned integer values (counter)<br>*str* - the number of processed character/string values (counter)<br>*log* - the number of processed log values (counter)<br>*text* - the number of processed text values (counter)<br>*not supported* - the number of times item processing resulted in item becoming unsupported or keeping that state (counter)<br>(with *history*, *index*, *trend* cache) *pfree* (default) - the percentage of free buffer<br>*total* - the total size of buffer<br>*free* - the size of free buffer<br>*used* - the size of used buffer<br>*pused* - the percentage of used buffer

Comments:

-   Specifying \<cache\> is mandatory. The `trend` cache parameter is not supported with Zabbix proxy.
-   The history cache is used to store item values. A low number indicates performance problems on the database side.
-   The history index cache is used to index the values stored in the history cache;
-   The trend cache stores the aggregate for the current hour for all items that receive data;
-   You may use the zabbix[wcache,values] key with the *Change per second* preprocessing step in order to get values-per-second statistics.

[comment]: # ({/new-9fa76fcd})
