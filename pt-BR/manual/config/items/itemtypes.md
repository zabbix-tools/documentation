[comment]: # translation:outdated

[comment]: # ({new-06812ee1})
# - \#2 Tipos de itens

[comment]: # ({/new-06812ee1})

[comment]: # ({new-f03e2e9a})
#### Visão geral

No zabbix os itens podem ser de diversos tipos - *Zabbix agent, Simple
checks, SNMP, Zabbix internal, IPMI, JMX monitoring* etc.

Algumas coletas de valores são executadas diretamente pelo Zabbix Server
ou Zabbix Proxy (são coletas sem agente) enquanto outras precisam de um
agente específico (Zabbix Agent, SNMP Agent, IPMI Agent, Zabbix Java
Gateway, etc).

Cada tipo de item poderá ter parâmetros específicos e terá um conjunto
diferente de chaves suportadas.

Os detalhes de todos os itens padrões da solução está incluso aqui e nas
subseções desta..

::: noteimportant


1.  A partir do Zabbix 2.0, é possível se definir várias interfaces em
    um mesmo host e com vários tipos (Zabbix Agent, SNMP, JMX, e IPMI).
    Se um item em particular precisa de um tipo específico de interface
    (por exemplo um item IPMI precisa que o host tenha uma interface
    IPMI), esta interface deverá existir na definição do host. Além
    disso, se um item puder utilizar mais de uma interface, será buscado
    na lista de interfaces disponíveis (na sequência: Zabbix
    Agent→SNMP→JMX→IPMI) pela primeira que couber.\


:::

[comment]: # ({/new-f03e2e9a})
