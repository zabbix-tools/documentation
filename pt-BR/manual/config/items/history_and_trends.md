[comment]: # translation:outdated

[comment]: # ({new-b5fd3df7})
# - \#3 Histórico e médias

[comment]: # ({/new-b5fd3df7})

[comment]: # ({new-e9319443})
#### Visão geral

O Zabbix guarda os dados coletados em dois formatos: históricos e
médias. O primeiro tipo (histórico), como o próprio nome indica, guarda
todos os valores coletados, sem nenhuma sumarização. Por outro lado, as
médias guardam os dados sumarizados de hora a hora visando possibilitar
a geração de gráficos e outras informações com um custo menor para o
banco de dados.

[comment]: # ({/new-e9319443})

[comment]: # ({new-e5d848c2})
#### Guardando o histórico

Você pode configurar por quanto tempo (dias) o histórico deverá ser
armazenado de três formas:

-   nas propriedades dos [itens](/pt/manual/config/items/item)
-   quando utiliza o recurso de atualização de massa nos itens
-   quando [configura as tarefas
    do](/pt;manual/web_interface/frontend_sections/administration/general#housekeeper)
    'housekeeper'

Qualquer dado mais antigo que o limite definido será removido pelo
'housekeeper'.

Em geral, é altamente recomendável guardar o menor histórico o possível
para não sobrecarregar o BD com muitos valores históricos.

Ao invés de guardar um longo histórico, você pode guardar os dados por
mais tempo nas médias. Por exemplo, você pode guardar um histórico de 14
dias e médias por 5 anos.

Você pode obter uma boa ideia de quanto espaço é necessário pelo
históricos e pelas médias ao consultar o manual de [estimativa de
tamanho do banco de
dados](/pt/manual/installation/requirements#database_size).

Mesmo guardando um curto histórico, você poderá ver os dados antigos em
gráficos. Os gráficos utilizam as médias para mostrar os dados antigos.

::: noteclassic
Se o histórico estiver definido como '0', o Zabbix só
conseguirá processar triggers que utilizem somente o último valor. Daods
históricos não serão armazenados no banco de dados, a exceção do último
valor coletado.
:::

[comment]: # ({/new-e5d848c2})

[comment]: # ({new-3d94ff60})
#### Guardando as médias

As médias são um recurso nativo para reduzir o tamanho dos dados
históricos. Em seu algorítimo tá previsto o armazenamento, a cada hora,
de 4 métricas para cada item coletado: mínimo, média, máximo e
quantidade. Ela está disponível apenas para os tipos de dados numéricos.

Você pode configurar o período de armazenamento de médias de três
formas:

-   editando diretamente as propriedades do
    [item](/pt/manual/config/items/item)
-   através do recurso de atualização em massa de itens
-   através das configurações do 'housekeeper'

Normalmente as médias são muito mais baratas para se armazenar por
longos períodos que os dados históricos. E de forma similar ao que
acontece com a remoção de registros antigos no histórico, o
'housekeeper' também tem a capacidade de remover dados antigos das
médias.

::: noteclassic
Se o período de retenção de médias for configurado para '0',
o Zabbix Server não irá calcular nem armazenar seus
valores.
:::

::: noteclassic
As médias são calculadas e armazenadas com o mesmo tipo de
dados original. Para os dados do tipo 'Numérico (inteiro sem sinal)' a
média será arredondada para baixo de forma a manter o tipo de dado. Por
exemplo, se um item receber somente valores 0 e 1 (o um item de icmpping
por exemplo) a média calculada não será de 0,5 e sim de 0.

Quando o Zabbix Server for reiniciado a precisão de hora no cálculo a
precisão do cálculo das médias naquela hora poderá ser
comprometida.
:::

[comment]: # ({/new-3d94ff60})
