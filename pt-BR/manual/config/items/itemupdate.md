[comment]: # translation:outdated

[comment]: # ({new-109e2be5})
# - \#7 Atualização em massa

[comment]: # ({/new-109e2be5})

[comment]: # ({new-7958bf1e})
#### Visão geral

Algumas vezes você precisa alterar um atributo em um grande número de
itens de uma só vez. Ao invés de alterar item a item, você pode utilizar
o recurso de atualização em massa para fazer esta tarefa.

[comment]: # ({/new-7958bf1e})

[comment]: # ({new-69655416})
#### Usando a atualização em massa

Para atualizar em massa alguns itens, são necessários os passos a
seguir:

-   Selecione os 'checkboxes' dos itens que deseja atualizar, na lista
    de itens do host ou do template
-   Clique no botão *Atualização em massa* na parte inferior da lista
-   Selecione os 'checkboxes' dos atributos desejados
-   Informe os novos valores para estes atributos e clique no botão
    *Atualizar*

![](/pt/manual/config/item_mass_update.png)

*Substituir aplicações* irá remover do item qualquer referência a
aplicações, substituindo pelas informadas neste campo.

*Adicionar aplicação nova ou existente* permite adicionar aplicações às
já existentes no item, podendo inclusive criar uma nova aplicação para
isso.

Ambos os campos possuem o recurso de auto-completar, ao iniciar a
digitação do nome da aplicação aparecerá um menu para que você selecione
a correspondente. Se a aplicação for nova, será apresentado o menu e ao
final haverá a indicação *(novo)* após o texto digitado. Utilize as
setas do teclado ou o mouse para selecionar.

[comment]: # ({/new-69655416})
