[comment]: # translation:outdated

[comment]: # ({3634f8be-ffc3ea50})
# 2 Intervalos customizados

[comment]: # ({/3634f8be-ffc3ea50})

[comment]: # ({d9637045-2c00d21c})

#### Visão geral

É possível criar regras customizadas em relação ao tempo em que um
item é verificado. Os dois métodos disponíveis são *Intervalos flexíveis*, 
que permitem redefinir o intervalo de atualização padrão, e *Agendamento*, 
pelo qual uma verificação pode ser executada em um horário específico 
ou sequência de horários.

[comment]: # ({/d9637045-2c00d21c})

[comment]: # ({new-e2fa2aee})

::: noteclassic
Zabbix agent 2 supports custom intervals for both passive and active checks, whereas Zabbix agent supports custom intervals only for passive checks. See  [Zabbix agent vs agent 2 comparison](/manual/appendix/agent_comparison). 
:::

[comment]: # ({/new-e2fa2aee})

[comment]: # ({2e39425e-41315093})

#### Intervalos flexíveis

Intervalos flexíveis permitem redefinir o intervalo de atualização padrão
para peíodos de tempo específicos. Um intervalo flexível é definido com
*Intervalo* e *Período* onde:

-   *Intervalo* – o intervalo de atualização para o período de tempo especificado
-   *Período* – o período de tempo quando o intervalo flexível está ativo (consulte
    [períodos de tempo](/manual/appendix/time_period) para uma descrição detalhada do formato para *Período*)

Até sete intervalos fléxíveis podem ser definidos. Se múltiplos intervalos 
se sobrepõem, o menor valor de *intervalo* é utilizado para o período de
sobreposição. Note que se o menor valor de sobreposição de intervalos
for '0', não haverá verificação passiva (polling). Fora dos intervalos flexíveis 
é usado o intervalo de atualização padrão.

Note que se o intervalo flexível se iguala à largura do período, o
item será verificado exatamente uma vez. Se o intervalo é maior
que o período, o item pode ser verificado uma vez ou ele pode
nem ser verificado (por isso tal configuração não é aconselhável). 
Se o intervalo flexível é menor que o período, o item será verificado
ao menos uma vez.

Se o intervalo flexível for configurado para '0', o item não é verificado
durante o período do intervalo flexível e retorna a verificação de acordo
com o *Intervalo de atualização* padrão assim que o período tiver findado. 
Exemplos:

|Intervalo|Período|Descrição|
|--------|------|-----------|
|10|1-5,09:00-18:00|O item será verificado a cada 10 segundos durante o horário de trabalho.|
|0|1-7,00:00-7:00|O item não será verificado durante a madrugada.|
|0|7-7,00:00-24:00|O item não será verificado aos domingos.|
|60|1-7,12:00-12:01|O item será verificado todos os dias ao meio-dia. Note que isto foi usado como contingência para verificações agendadas e a partir do Zabbix 3.0 é recomendado usar intervalos de agendamento para tais verificações.|

[comment]: # ({/2e39425e-41315093})

[comment]: # ({new-128c5833})

#### Intervalos de agendamento

Intevalos de agendamento são utilizados para verificar itens em horários
específicos. Enquanto os intervalos flexíveis são projetados para redefinir
o intervalo de atualização padrão do item, os intervalos de agendamento 
são usados para especificar uma agenda de verificação independente, que
é executada de forma paralela.

Um intervalo de agendamento é definido como:
`md<filtro>wd<filtro>h<filtro>m<filtro>s<filtro>` onde:

-   **md** - mês dias
-   **wd** - semana dias
-   **h** - horas
-   **m** - minutos
-   **s** – segundos

O `<filtro>` é usado para especificar valores para seu prefixo (dias, horas,
minutos, segundos) e é definido como:
`[<de>[-<até>]][/<salto>][,<filtro>]` onde:

-   `<de>` e `<até>` definem o intervalo de valores considerados (inclusive eles próprios).
    Se `<até>` for omitido então o filtro interpreta o intervalo como `<de> - <de>`
    Se `<de>` também for omitido então o filtro considerará como intervalo todos os valores possíveis.
-   `<salto>` define o salto dentro do intervalo. Por padrão `<salto>` tem o valor 1, o que significa
    que todos os valores do intervalo definido são considerados.

Mesmo sendo as definições de filtros opcionais, ao menos um filtro deve ser usado.
Um filtro deve definir um intervalo ou um valor de *<salto>* .

Um filtro vazio corresponde a '0' se não houver definição de filtros nos níveis abaixo, caso contrário, 
todos os valores possíveis serão considerados. Por exemplo, se o filtro de hora é omitido então
apenas a hora '0' será considerada, contanto que os filtros de minutos e segundos também tenham
sido omitidos, caso contrário o filtro de hora vazio considerará todos os valores de hora possíveis.

Valores válidos para `<de>` e `<até>` em seus respectivos prefixos são:

|Prefixo|Descrição|*<de>*|*<ate>*|
|------|-----------|--------------|------------|
|md|Mês dias|1-31|1-31|
|wd|Semana dias|1-7|1-7|
|h|Horas|0-23|0-23|
|m|Minutos|0-59|0-59|
|s|Segundos|0-59|0-59|

O valor `<de>` deve ser menor ou igual ao valor de `<até>`. O `<salto>`
deve ser maior ou igual a 1 e menor ou igual a `<até>` -
`<de>`.

Valores simples para dias do mês, horas, minutos e segundos podem ser
prefixados com 0. Por exemplo, `md01-31` e `h/02` são intervalos válidos,
mas `md01-031` e `wd01-07` não são.

No Zabbix Frontend, múltiplos intervalos de agendamento são informados em 
linhas separadas. No Zabbix API, eles são concatenados em um texto único com 
ponto-e-vírgula `;` como separador.

Se um tempo é correspondido por vários intervalos ele é executado apenas
uma vez. Por exemplo, `wd1h9;h9` será executado apenas uma vez na
segunda às 9h.

Exemplos:

|Intervalo|Será executado|
|--------|----------------|
|m0-59|a cada minuto|
|h9-17/2|a cada 2 horas começando às 9h (9h, 11h ...)|
|m0,30 or m/30|de hora em horas às Xh e Xh30|
|m0,5,10,15,20,25,30,35,40,45,50,55 or m/5|a cada cinco minutos|
|wd1-5h9|toda segunda a sexta às 9h|
|wd1-5h9-18|toda segunda a sexta às 9h,10h,...,18h|
|h9,10,11 or h9-11|todo dia às 9h, 10h e 11h|
|md1h9m30|todo 1º dia de cada mês às 9h30|
|md1wd1h9m30|todo 1º dia de cada mês às 9h30 se for segunda|
|h9m/30|todo dia às 9h, 9h30|
|h9m0-59/30|todo dia às 9h, 9h30|
|h9,10m/30|todo dia às 9h, 9h30, 10h, 10h30|
|h9-10m30|todo dia às 9h30, 10h30|
|h9m10-40/30|todo dia às 9h10, 9h40|
|h9,10m10-40/30|todo dia às 9h10, 9h40, 10h10, 10h40|
|h9-10m10-40/30|todo dia às 9h10, 9h40, 10h10, 10h40|
|h9m10-40|todo dia às 9h10, 9h11, 9h12, ... 9h40|
|h9m10-40/1|todo dia às 9h10, 9h11, 9h12, ... 9h40|
|h9-12,15|todo dia às 9h, 10h, 11h, 12h, 15h|
|h9-12,15m0|todo dia às 9h, 10h, 11h, 12h, 15h|
|h9-12,15m0s30|todo dia às 9h0m30s, 10h0m30s, 11h0m30s, 12h0m30s, 15h0m30s|
|h9-12s30|todo dia às 9h0m30s, 9h01m30s, 9h02m30s ... 12h58m30s, 12h59m30s|
|h9m/30;h10 (*Sintaxe específica da API*)|todo dia às 9h, 9h30, 10h|
|h9m/30<br>h10 (*adicione este como outra linha no Frontend*)|todo dia às 9h, 9h30, 10h|

[comment]: # ({/new-128c5833})
