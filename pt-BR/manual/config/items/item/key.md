[comment]: # translation:outdated

[comment]: # ({1e44841e-856c911b})

# 1 Formato da chave de item

O formato da chave do item, incluindo parâmetros da chave, deve seguir as regras de sintaxe. 
As seguintes ilustrações mostram a sintaxe suportada. Elementos e caracteres permitidos
em cada ponto podem ser determinados seguindo as setas - se algum bloco pode ser 
alcançado pela linha, ele é permitido, se não - ele não é permitido.

![](../../../../../assets/en/manual/config/item_key_2.png){width="600"}

Para construir uma chave de item válida, se começa especificando o nome da chave,
então há a escolha de ter parâmetros ou não - como representado pelas duas linhas que
poderiam ser seguidas.

[comment]: # ({/1e44841e-856c911b})

[comment]: # ({d745f077-63ccf8e4})

#### Nome da chave

O nome da chave tem um intervalo limitado de caracteres permitidos, 
que se seguem. Os caracteres permitidos são:

    0-9a-zA-Z_-.

O que significa:

-   todos os números;
-   todas as letras minúsculas;
-   todas as letras maiúsculas;
-   sublinhado;
-   traço;
-   ponto.

![](../../../../../assets/en/manual/config/key_name.png)

[comment]: # ({/d745f077-63ccf8e4})

[comment]: # ({1a4d0727-af5ae7f1})

#### Parâmetros de chave

Uma chave de item pode ter múltiplos parâmetros que são separados por vírgula..

![](../../../../../assets/en/manual/config/key_parameters.png)

Cada parâmetro da chave pode ser ou um texto com aspas, um texto sem aspas
ou um array.

![](../../../../../assets/en/manual/config/item_parameter.png)

O parâmetro também pode ser deixado vazio, usando assim o valor padrão. Deste 
modo, o número apropriado de vírgulas deve ser informado se quaisquer parâmetros 
adicionais forem especificados. Por exemplo, a chave **icmpping\[,,200,,500\]** especificaria 
que o intervalo entre cada ping é de 200 milissegundos, tempo máximo (timeout) de 
500 milissegundos, e todos os outros parâmetros continuam com seus valores padrão.

[comment]: # ({/1a4d0727-af5ae7f1})




[comment]: # ({c53d8b1a-b4a70ec9})

#### Parâmetro - texto com aspas (quotado)

Se o parâmetro da chave é um texto quotado, qualquer caracter
Unicode é permitido.

Se o parâmetro contém vírgula, este parâmetro tem que ser
quotado.

Se o texto do parâmetro contém aspas, este parâmetro tem que 
ser quotado e todas as aspas que fazem parte do parâmetro têm
que ser escapadas com um caracter de contrabarra (`\`).

![](../../../../../assets/en/manual/config/key_param_quoted_string.png)

::: notewarning
Para quotar os parâmetros da chave, use apenas aspas duplas.
Aspas simples não são suportadas.
:::

[comment]: # ({/c53d8b1a-b4a70ec9})

[comment]: # ({e9fb817a-3376836e})

#### Parâmetro - texto sem aspas (não quotado)

Se o parâmetro da chave é um texto não quotado, qualquer caracter
Unicode é permitido, exceto vírgula e colchete final (\]). Um parâmetro
não quotado não pode começar com colchete inicial (\[).

![](../../../../../assets/en/manual/config/key_param_unquoted_string.png)

[comment]: # ({/e9fb817a-3376836e})

[comment]: # ({adf50c23-5289a973})

#### Parâmetro - array

Se o parâmetro da chave é um array, ele é novamente envolto em 
colchetes, onde parâmetros individuais se adequam às regras e sintaxe
para especificar múltiplos parâmetros.

![](../../../../../assets/en/manual/config/key_param_array.png)

::: noteimportant
Parâmetros com arrays multinível, p.e.
`[a,[b,[c,d]],e]`, não são permitidos.
:::

[comment]: # ({/adf50c23-5289a973})
