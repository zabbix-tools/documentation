[comment]: # translation:outdated

[comment]: # ({new-23de852a})
# 11 Cache de valores

[comment]: # ({/new-23de852a})

[comment]: # ({new-f73e0537})
#### Visão geral

Para efetuar os cálculos das expressões de triggers, itens calculados,
itens agregados, e macros de forma mais ágil o Zabbix (a partir da
versão 2.2) suporta manter um cache de valores no Zabbix Server.

Este cache é em memória e pode ser usado para acessar valores
históricos, ao invés de proceder com consultas SQL diretas ao banco de
dados. Se o histórico de valores não estiver presente no cache, os
valores ausentes serão solicitados do BD e o cache será atualizado.

Para habilitar a funcionalidade de cache de valores você deverá definir
o parâmetro opcional **ValueCacheSize** no [arquivo de configuração do
Zabbix Server](/pt/manual/appendix/config/zabbix_server).

Dois itens internos são suportados para monitorar o cache de valores:
**zabbix\[vcache,buffer,<mode>\]** e
**zabbix\[vcache,cache,<parameter>\]**. Veja mais detalhes em
[itens internos](/pt/manual/config/items/itemtypes/internal).

[comment]: # ({/new-f73e0537})

[comment]: # ({new-3add2a5c})
Item values remain in value cache either until:

-   the item is deleted (cached values are deleted after the next configuration sync);
-   the item value is outside the time or count range specified in the trigger/calculated item expression
    (cached value is removed when a new value is received);
-   the time or count range specified in the trigger/calculated item expression is changed
    so that less data is required for calculation (unnecessary cached values are removed after 24 hours).

::: notetip
Value cache status can be observed by using the server [runtime control](/manual/concepts/server#runtime-control) option
`diaginfo` (or `diaginfo=valuecache`) and inspecting the section for value cache diagnostic information.
This can be useful for determining misconfigured triggers or calculated items.
:::

[comment]: # ({/new-3add2a5c})

[comment]: # ({new-1fb59f91})
To enable the value cache functionality, an optional **ValueCacheSize**
parameter is supported by the Zabbix server
[configuration](/manual/appendix/config/zabbix_server) file.

Two internal items are supported for monitoring the value cache:
**zabbix\[vcache,buffer,<mode>\]** and
**zabbix\[vcache,cache,<parameter>\]**. See more details with
[internal items](/manual/config/items/itemtypes/internal).

[comment]: # ({/new-1fb59f91})
