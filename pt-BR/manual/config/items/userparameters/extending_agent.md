[comment]: # translation:outdated

[comment]: # ({new-ffe98fbd})
# 1 Estendendo os Agentes Zabbix

Este tutorial apresenta um passo a passo de como estender as
funcionalidades de um Zabbix Agent através de [parâmetros de
usuário](/pt/manual/config/items/userparameters).

[comment]: # ({/new-ffe98fbd})

[comment]: # ({new-8f2323c0})
##### Passo 1

Escreva um script ou uma linha de comando para obter o parâmetro
desejado.

Por exemplo, nós podemos usar o comando a seguir para obter a quantidade
de consultas executadas por um MySQL server:

    mysqladmin -uroot status|cut -f4 -d":"|cut -f1 -d"S"

Quando for executado o comando retornará a quantidade de consultas SQL.

[comment]: # ({/new-8f2323c0})

[comment]: # ({new-6117d271})
##### Passo 2

Adicione o seguinte comando ao arquivo de configuração do Zabbix Agent
(zabbix\_agentd.conf):

    UserParameter=mysql.questions,mysqladmin -uroot status|cut -f4 -d":"|cut -f1 -d"S"

**mysql.questions** é um identificador único. Ele pode ser qualquer
texto, por exemplo, **queries**.

Teste o parâmetro utilizando o utilitário
[zabbix\_get](/pt/manual/concepts/get).

[comment]: # ({/new-6117d271})

[comment]: # ({new-67b38df5})
##### Passo 3

Reinicie o Zabbix Agent. Quando iniciado o agente sempre recarregará o
arquivo de configuração.

[comment]: # ({/new-67b38df5})

[comment]: # ({new-4a46fd71})
##### Passo 4

Adicione um novo item para monitorar. Neste item defina que a sua chave
é **mysql.questions**. O tipo do item deverá ser **Agente Zabbix** ou
**Agente Zabbix (ativo)**.

É importante se certificar que o valor retornável seja compatível com o
tipo definido na interface web. De outra forma o Zabbix não irá
aceita-lo.

[comment]: # ({/new-4a46fd71})
