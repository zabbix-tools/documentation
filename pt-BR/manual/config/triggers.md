[comment]: # translation:outdated

[comment]: # ({new-e02835c7})
# - \#3 Triggers

[comment]: # ({/new-e02835c7})

[comment]: # ({new-d4a1b8b6})
#### Visão geral

As triggers são expressões lógicas que analisam os dados coletados pelos
itens e representam o estado do sistema em relação aos mesmos.

Enquanto os itens são utilizados para coletar dados, é impensável
esperar que alguém tenha tempo para analisar manualmente todos os
valores coletados para alertar sobre desvios e limites. O trabalho de
analisar os dados deve ser deixado a cargo das expressões das trigger.

Uma expressão de trigger permite definir um limite aceitável de dados.
Logo, quando o dado recebido fugir do limite aceitável a trigger será
acionada, mudando seu estado para "INCIDENTE".

Uma trigger pode ter os seguintes estados:

|Valor|Descrição|
|-----|-----------|
|OK|Este é o estado normal de uma trigger. Em versões anteriores do Zabbix era chamado de "FALSE/FALSO".|
|INCIDENTE|Normalmente indica que algo ocorreu. Por exemplo, o "load de CPU" está muito alto. Em versões anteriores do Zabbix era chamado de "TRUE/VERDADEIRO".|

O estado de uma trigger (a expressão) é recalculada toda vez que o
Zabbix Server recebe um novo valor que afete a expressão.

Se funções baseadas em tempo (**nodata()**, **date()**,
**dayofmonth()**, **dayofweek()**, **time()**, **now()**) forem
utilizadas na expressão, o estado da trigger será recalculado a cada
**30 segundos** pelo processo de *timer* do Zabbix. Se forem utilizadas
funções baseadas em tempo e funções normais, a trigger será recalculada
quando receber um novo valour **E** a cada 30 segundos.

Você pode obter maiores detalhes através do manual de [construção de
expressões de trigger](/pt/manual/config/triggers/expression).

[comment]: # ({/new-d4a1b8b6})

[comment]: # ({new-7c263ca6})

::: notetip
After changing a trigger's status from *disabled* to *enabled*, the trigger expression will be evaluated as soon as an item associated with it receives a value or the time to handle a time-based function comes.
:::

[comment]: # ({/new-7c263ca6})

[comment]: # ({new-a45a4e79})

In a more complex trigger, the expression may include a **combination**
of multiple functions and multiple thresholds. See also: [Trigger
expression](/manual/config/triggers/expression).

Most trigger functions are evaluated based on
[history](/manual/config/items/history_and_trends) data, while some
trigger functions for long-term analytics, e.g. **trendavg()**,
**trendcount()**, etc, use trend data.

[comment]: # ({/new-a45a4e79})



[comment]: # ({new-6781ee38})
#### Calculation time

A trigger is recalculated every time Zabbix server receives a new value
that is part of the expression. When a new value is received, each
function that is included in the expression is recalculated (not just
the one that received the new value).

Additionally, a trigger is recalculated each time when a new value is
received **and** every 30 seconds if time-based functions are used in
the expression.

Time-based functions are **nodata()**, **date()**, **dayofmonth()**,
**dayofweek()**, **time()**, **now()**); they are recalculated every 30
seconds by the Zabbix history syncer process.

[comment]: # ({/new-6781ee38})

[comment]: # ({new-1fa7644e})
#### Evaluation period

An evaluation period is used in functions referencing the item history.
It allows to specify the interval we are interested in. It can be
specified as time period (30s, 10m, 1h) or as a value range (\#5 - for
five latest values).

The evaluation period is measured up to "now" - where "now" is the
latest recalculation time of the trigger (see [Calculation
time](#calculation_time) above); "now" is not the "now" time of the
server.

The evaluation period specifies either:

-   To consider all values between "now-time period" and "now" (or, with
    time shift, between "now-time shift-time period" and
    "now-time\_shift")
-   To consider no more than the num count of values from the past, up
    to "now"
    -   If there are 0 available values for the time period or num count
        specified - then the trigger or calculated item that uses this
        function becomes unsupported

Note that:

-   If only a single function (referencing data history) is used in the
    trigger, "now" is always the latest received value. For example, if
    the last value was received an hour ago, the evaluation period will
    be regarded as up to the latest value an hour ago.
-   A new trigger is calculated as soon as the first value is received
    (history functions); it will be calculated within 30 seconds for
    time-based functions. Thus the trigger will be calculated even
    though perhaps the set evaluation period (for example, one hour) has
    not yet passed since the trigger was created. The trigger will also
    be calculated after the first value, even though the evaluation
    range was set, for example, to ten latest values.

[comment]: # ({/new-1fa7644e})

[comment]: # ({new-cad1df72})

#### Unknown state

It is possible that an unknown operand appears in a trigger expression if:

-   an unsupported item is used
-   the function evaluation for a supported item results in an error

In this case a trigger generally evaluates to "unknown" (although there are some exceptions). For more details, see [Expressions with unknown operands](/manual/config/triggers/expression#expressions-with-unknown-operands).

It is possible to [get notified](/manual/config/events/sources#internal-events) on unknown triggers.

[comment]: # ({/new-cad1df72})

