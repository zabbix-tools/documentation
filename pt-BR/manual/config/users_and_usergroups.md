[comment]: # translation:outdated

[comment]: # ({new-9685d873})
# - \#9 Usuários e grupos de usuários

[comment]: # ({/new-9685d873})

[comment]: # ({new-07f1c89b})
#### Visão geral

O gerenciamento de configurações de monitoração do Zabbix é feito por
sua interface web e é através desta que os usuários se identificam
através de um usuário e senha.

Todos as senhas de usuários são salvas de forma criptografada no banco
de dados do Zabbix. A comunicação entre o usuário e o servidor web
poderá ser protegida através de SSL.

Com um esquema de permissionamento
[flexível](/manual/config/users_and_usergroups/permissions) você poderá
restringir de forma personalizada o acesso a:

-   funções administrativas do zabbix
-   hosts monitorados

A configuração inicial do Zabbix possui dois usuários: 'Admin' e
'guest'. O usuário 'guest' é utilizado por usuários não autenticados.
Antes de se autenticar como 'Admin', você é o 'guest' (convidado). Mais
detalhes no [manual de configuração de
usuários](/manual/config/users_and_usergroups/user).

[comment]: # ({/new-07f1c89b})
