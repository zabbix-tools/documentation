[comment]: # translation:outdated

[comment]: # ({new-d5d0048d})
# 3 Dependências entre triggers

[comment]: # ({/new-d5d0048d})

[comment]: # ({new-26bda053})
#### Visão geral

Algumas vezes um host só está disponível se outro também estiver. Um
servidor que está atrás de um roteador estará indisponível se o roteador
ficar indisponível. Caso tenhamos triggers definidas em ambos os hosts,
nós receberemos notificações sobre a indisponibilidade de ambos.

Temos aqui uma dependência funcional entre os hosts. Neste cenário pode
ser interessante que as notificações sobre as indisponibilidades no
servidor atrás do roteador possam ser configuradas para só ocorrerem se
o elemento principal (o roteador) não tiver problema naquele momento.

Neste momento o Zabbix não suporta dependências diretas entre os hosts,
suportando uma outra forma mais flexível de dependência: a dependência
entre triggers. Uma trigger pode possuir várias outras triggers
dependendo dela.

No exemplo citado anteriormente nós precisamos então abrir o formulário
de configuração de triggers e configurar a dependência com uma trigger
do roteador. Com esta dependência o estado da trigger do 'servidor' não
irá ser modificada enquanto a trigger do 'roteador' estiver em estado de
'Incidente' e nenhuma notificação será enviada.

Se tanto o servidor quanto o roteador estiverem inacessíveis e existir a
relação de dependência, o Zabbix não irá executar nenhuma ação baseado
na trigger dependente (neste caso a trigger do servidor).

É importante observar que os eventos/ações de triggers com dependência
não serão suprimidos se a trigger "mestre" estiver com o estado
"Desativado", ou se um dos items por ela referenciados estiver neste
estado.

Também:

-   Uma dependência entre triggers pode ser adicionada entre quaisquer
    hosts, não sendo permitido, entretanto, uma dependência circular.
-   Uma dependência entre triggers pode ser adicionada entre quaisquer
    templates. Se uma trigger do template A depende de outra no template
    B, o template A só poderá ser associado a um host (ou outro
    template) se o template B também o for. Por outro lado o template B
    (que é o "mestre" na relação de dependência) poderá ser associado a
    qualquer outro host ou template sozinho.
-   Uma dependência entre triggers pode ser adicionada à uma trigger de
    template para uma trigger de host. Este tipo de ligação é muito útil
    para situações onde os vários hosts dependem do estado de um
    roteador. Com esta associação todos os hosts associados ao template
    terão relação de dependência com o host "master" da trigger.
-   Não pode ser adicionada dependência entre uma trigger de host e uma
    trigger de template.
-   Uma dependência entre triggers pode ser adicionada entre protótipos
    de trigger (na mesma regra de autobusca) ou à uma trigger real. Um
    protótipo de trigger não pode depender de protótipos de trigger
    definidos em outras regras de autobusca. Os protótipos de trigger em
    nível de host não podem depender de trigger de um template.

[comment]: # ({/new-26bda053})

[comment]: # ({new-c4578b73})
#### Configuração

Para definir um dependência, abra a aba de
[dependências](trigger#configuration) da trigger. Clique em *Adicionar*
no bloco de 'Dependências' e selecione uma ou mais triggers das quais
irá depender.

![](../../../../assets/en/manual/config/triggers/dependency.png)

Clique em *Atualizar*. Agora a trigger tem a indicação de sua
dependência na lista.

![](../../../../assets/en/manual/config/triggers/dependency_list.png)

[comment]: # ({/new-c4578b73})

[comment]: # ({new-4dc2ce6b})
##### Exemplos de dependências

Por exemplo, o Servidor\_1 está atrás do Roteador\_2 e o Roteador\_2
está atrás do Roteador\_1.

    Zabbix - Roteador_1 - Roteador_2 - Servidor_1

Se o Roteador\_1 estiver inacessível, tanto o Servidor\_1 quanto o
Roteador\_2 também estarão inacessíveis, e sem as corretas dependências
serão gerados três alertas.

Neste caso precisamos definir duas dependências:

    A trigger 'Servidor_1 is down' depende da trigger 'Roteador_2 is down'
    A trigger 'Roteador_2 is down' depende da trigger 'Roteador_1 is down'

Antes de mudar o estado da trigger 'Servidor\_1 is down', o Zabbix irá
verificar o estado de suas dependências. Se encontrar, em qualquer uma
delas, o estado de 'Incidente' o estado da trigger não será modificado e
as notificações e ações não serão enviadas.

O Zabbix executa esta verificação em modo recursivo, se o Roteador\_1 ou
o Roteador\_2 estiverem inacessíveis a trigger do Host não será
atualizada.

[comment]: # ({/new-4dc2ce6b})
