[comment]: # translation:outdated

[comment]: # ({new-510ac9cc})
# 2 Expressões de Trigger

[comment]: # ({/new-510ac9cc})

[comment]: # ({new-473bcd5e})
#### Visão geral

As expressões de triggers são muito flexíveis. Você pode utiliza-las
para criar testes com lógicas complexas em relação a dados e
estatísticas da monitoração.

A sintaxe básica de uma expressão pode ser definida assim:

    {<server>:<key>.<function>(<parameter>)}<operator><constant>

[comment]: # ({/new-473bcd5e})

[comment]: # ({new-d4e5ef03})
#### - Funções

As funções de Trigger permitem referenciar dados coletados, horário
atual e outros fatores.

Uma lista completa das [funções
suportadas](/pt/manual/appendix/triggers/functions) está disponível.

[comment]: # ({/new-d4e5ef03})

[comment]: # ({new-b255de16})
#### - Parâmetros das funções

A maioria das funções numéricas aceita uma quantidade de segundos como
parâmetro (uma unidade de tempo).

Você pode utilizar o prefixo **\#** para especificar um parâmetro com
significado diferente:

|Chamada da Função|Significa|
|-------------------|---------|
|**sum(600)**|Sumarização de todos os valores nos últimos 600 segundos (10 minutos)|
|**sum(\#5)**|Sumarização dos últimos 5 valores|

A função **last** utiliza uma forma diferente para valores prefixados
com **\#**, para ela isso indicará que ela deverá recuperar o enézimo
valor anterior (do mais recente para o mais antigo). Exemplo, suponhamos
que os últimos 10 valores coletados são: (2, 3, 4, 9, 10, 11, 11, 16, 7,
0)

-   **last(\#2)** - irá retornar o penúltimo valor *7*
-   **last(\#5)** - irá retornar *10*.

As funções **avg**, **count**, **last**, **min** e **max** possuem um
parâmetro adicional: o `time_shift`. Este parâmetro permite referenciar
o dado em determinado período de tempo no passado. Por exemplo,
**avg(1h,1d)** irá retornar o valor médio analisando 1 hora de valores
de 1 dia antes do momento da coleta.

::: notetip
As triggers só analisam os dados que estão no histórico.
Se o dado desejado não estiver no histórico, mas estiver nas médias, a
informação das médias **não será utilizada**. Logo, é necessário que se
mantenha o histórico por tempo compatível com as triggers que se deseja
criar.
:::

Você pode utilizar os [símbolos de unidades](suffixes) nas expressões,
por exemplo '5m' (minutos) ao invés de '300' segundos ou '1d' (dia) ao
invés de '86400' segundos, '1K' ao invés de '1024' bytes.

[comment]: # ({/new-b255de16})

[comment]: # ({new-cf9e09bb})
#### - Operadores

Os seguintes operadores são suportados nas expressões de triggers **(em
ordem descedente de prioridade de execução)**:

|Prioridade|Operador|Definição|
|----------|--------|-----------|
|**1**|**-**|Símbolo de negativo|
|**2**|**not**|Não Lógico|
|**3**|\*\***\* \|Multiplicação \| \| \|**/\*\*|Divisão|
|**4**|**+**|Soma aritmética|
|<|**-**|Redução aritmética|
|**5**|**<**|Menor que. O operador é definido como:<br>A<B ⇔ (A<=B-0.000001)|
|<|**<=**|Menor ou igual a.|
|<|**>**|Maior que. O operador é definido como:<br>A>B ⇔ (A>=B+0.000001)|
|<|**>=**|Maior ou igual a.|
|**6**|**=**|É igual. O operador é definido como:<br>A=B ⇔ (A>B-0.000001) e (A<B+0.000001)|
|<|**<>**|Diferente. O operador é definido comoas:<br>A<>B ⇔ (A<=B-0.000001) ou (A>=B+0.000001)|
|**7**|**and**|Operador lógico E|
|**8**|**or**|Operador lógico OU|

Os operadores **not**, **and** e **or** são sensíveis ao caso e deverão
ser escritos em letra minúscula. Eles devem estar envoltos em espaços ou
parênteses.

Todos os operadores, exceto o "Símbolo de negativo" e "Não lógico", tem
associação da esquerda para a direita.

[comment]: # ({/new-cf9e09bb})

[comment]: # ({new-6699e631})
#### - Exemplos de triggers

[comment]: # ({/new-6699e631})

[comment]: # ({new-c5d3ec54})
##### Exemplo 1

Análise de carga de processamento na CPU: "Processor load is too high on
www.zabbix.com"

    {www.zabbix.com:system.cpu.load[all,avg1].last()}>5

'www.zabbix.com:system.cpu.load\[all,avg1\]' provê um nome curto para o
parâmetro monitorado. Neste caso se refere ao host 'www.zabbix.com' com
a chave 'system.cpu.load\[all,avg1\]'. Utilizando a função 'last()', nós
estaremos referindo ao valor mais recente. Finalmente, '>5' define
que a trigger deverá ir para o estado de "INCIDENTE" quando o valor mais
recente desta chave, neste host for superior a 5.

[comment]: # ({/new-c5d3ec54})

[comment]: # ({new-10bc91a2})
##### Exemplo 2

www.zabbix.com is overloaded

    {www.zabbix.com:system.cpu.load[all,avg1].last()}>5 ou {www.zabbix.com:system.cpu.load[all,avg1].min(10m)}>2 

A expressão será verdadeira quando o último valor coletado para a carga
da CPU for superior a 5 ou superior a 2 nos últimos 10 coletados.

[comment]: # ({/new-10bc91a2})

[comment]: # ({new-195c3f4f})
##### Exemplo 3

/etc/passwd has been changed

Utilize a função 'diff()':

    {www.zabbix.com:vfs.file.cksum[/etc/passwd].diff()}=1

A expressão será verdadeira quando o último valor da verificação
'checksum' do arquivo '/etc/passwd' for diferente da penúltima
verificação.

De forma similar esta técnica pode ser utilizada para monitorar vários
outros arquivos, tais quais: /etc/inetd.conf, /kernel, etc.

[comment]: # ({/new-195c3f4f})

[comment]: # ({new-abac99e3})
##### Exemplo 4

Alguém está baixando um arquivo muito grande da internet (ou um tráfego
intenso por um longo período)

Utilize a função 'min()':

    {www.zabbix.com:net.if.in[eth0,bytes].min(5m)}>100K

A expressão será verdadeira quando a quantidade de bytes recebidos nos
últimos 5 minutos na interface 'eth0' for superior a 100 KB.

[comment]: # ({/new-abac99e3})

[comment]: # ({new-17b607dc})
##### Exemplo 5

Ambos os nós do cluster de SMTP estão indisponíveis

Observe que a expressão utiliza dados de dois hosts diferentes:

    {smtp1.zabbix.com:net.tcp.service[smtp].last()}=0 and {smtp2.zabbix.com:net.tcp.service[smtp].last()}=0

A expressão será verdadeira quando ambos os servidores (smtp1.zabbix.com
e smtp2.zabbix.com) SMTP estiverem fora do ar.

[comment]: # ({/new-17b607dc})

[comment]: # ({new-9a129732})
##### Exemplo 6

A versão do Zabbix Agent precisa ser atualizada

Use a função 'str()':

    {zabbix.zabbix.com:agent.version.str("beta8")}=1

A expressão será verdadeira se a versão do Zabbix Agent possuir o texto
"beta8" (por exemplo 1.0beta8).

[comment]: # ({/new-9a129732})

[comment]: # ({new-49592749})
##### Exemplo 7

Servidor indisponível

    {zabbix.zabbix.com:icmpping.count(30m,0)}>5

A expressão será verdadeira se o host "zabbix.zabbix.com" estiver
inacessível por mais de 5 vezes nos últimos 30 minutos.

[comment]: # ({/new-49592749})

[comment]: # ({new-46e72843})
##### Exemplo 8

Sem dados nos últimos 3 minutos

Use a função 'nodata()':

    {zabbix.zabbix.com:tick.nodata(3m)}=1

::: noteclassic
Neste exemplo 'tick' é um item do tipo 'Zabbix trapper'.
Para que esta trigger funcione o item 'tick' precisará ter sido
definido. O host precisará enviar periodicamente o dado para este item
através do comando 'zabbix\_sender' ou similar. 
:::

A expressão será verdadeira se nenhum dado for recebido nos últimos 180
segundos.

[comment]: # ({/new-46e72843})

[comment]: # ({new-7956e04e})
##### Exemplo 9

Alta carga de CPU no período noturno

Utilize a função 'time()':

    {zabbix:system.cpu.load[all,avg1].min(5m)}>2 and {zabbix:system.cpu.load[all,avg1].time()}>000000 and {zabbix:system.cpu.load[all,avg1].time()}<060000

A expressão será verdadeira se a carga de CPU for superior a 2, entre a
meia noite e as seis da manhã.

[comment]: # ({/new-7956e04e})

[comment]: # ({new-62cfb014})
##### Exemplo 10

Verifica se o horário local do host monitorado e do servidor do Zabbix
estão sincronizados

Use a função 'fuzzytime()':

    {MySQL_DB:system.localtime.fuzzytime(10)}=0

A expressão será verdadeira se o horário do servidor 'MySQL\_DB' tiver
uma diferença maior que 10 segundos em relação ao horário do Zabbix
Server.

[comment]: # ({/new-62cfb014})

[comment]: # ({new-567b88a9})
##### Exemplo 11

Comparando a carga atual de CPU com a carga no mesmo horário do dia
anterior (usando o parâmetro de `time_shift`).

    {server:system.cpu.load.avg(1h)}/{server:system.cpu.load.avg(1h,1d)}>2

A expressão será verdadeira se a carga da última hora for duas vezes
superior a carga deste mesmo período um dia antes (24 horas).

[comment]: # ({/new-567b88a9})

[comment]: # ({new-2f13cb92})
##### Exemplo 12

Usando o valor de outro item como limite para a trigger:

    {Template PfSense:hrStorageFree[{#SNMPVALUE}].last()}<{Template PfSense:hrStorageSize[{#SNMPVALUE}].last()}*0.1

A expressão será verdadeira se o espaço livre for inferior a 10%.

[comment]: # ({/new-2f13cb92})

[comment]: # ({new-2c48905a})
#### - Técnicas 'anti-flapping'

Algumas vezes você precisa ter condições diferentes para estados
diferentes (INCIDENTE/OK). Por exemplo, nós podemos ter que definir uma
trigger para avisar quando a temperatura de uma sala for superior a 20C
(vinte graus) que é o máximo suportável para os servidores funcionarem
com segurança, mas a temperatura ideal de funcionamento deveria ser de
até 15C (quinze graus). Temos como definir uma trigger desta forma no
Zabbix, ela será ativada (mudar para o estado de INCIDENTE) se a
temperatura ultrapassar o máximo aceitável, mas não será inativada
(retornar ao estado OK) enquanto a temperatura não for inferior à
temperatura ideal.

Para fazer isso podemos definir uma trigger como a do "Exemplo 1". A
trigger do "Exemplo 2" apresenta a mesma técnica de "anti-flapping" para
espaço em disco.

[comment]: # ({/new-2c48905a})

[comment]: # ({new-7e0ba13a})
##### Example 10

CPU activity at any time with exception.

Use of function time() and **not** operator:

    min(/zabbix/system.cpu.load[all,avg1],5m)>2
    and not (dayofweek()=7 and time()>230000)
    and not (dayofweek()=1 and time()<010000)

The trigger may change its state to true at any time,
except for 2 hours on a week change (Sunday, 23:00 - Monday, 01:00).

[comment]: # ({/new-7e0ba13a})

[comment]: # ({new-0e6bfc51})
##### Exemplo 1

A temperatura na sala dos servidores está muito alta

    ({TRIGGER.VALUE}=0 and {server:temp.last()}>20) or
    ({TRIGGER.VALUE}=1 and {server:temp.last()}>15)

[comment]: # ({/new-0e6bfc51})

[comment]: # ({new-47b2e2fd})
##### Exemplo 2

Pouco espaço livre no disco

Incidente: se for menor que 10GB nos últimos 5 minutos

Recuperação (OK): se for maior que 40GB nos últimos 10 minutos

    ({TRIGGER.VALUE}=0 and {server:vfs.fs.size[/,free].max(5m)}<10G) or
    ({TRIGGER.VALUE}=1 and {server:vfs.fs.size[/,free].min(10m)}<40G)

::: noteclassic
Observe que a macro *{TRIGGER.VALUE}* retorna o estado
corrente da trigger (0 - OK, 1 - Incidente).
:::

[comment]: # ({/new-47b2e2fd})









[comment]: # ({new-bb0f5278})
##### Example 2

Free disk space is too low.

Problem expression: it is less than 10GB for last 5 minutes

    max(/server/vfs.fs.size[/,free],5m)<10G

Recovery expression: it is more than 40GB for last 10 minutes

    min(/server/vfs.fs.size[/,free],10m)>40G

[comment]: # ({/new-bb0f5278})

[comment]: # ({new-0cc34ec7})
#### Expressions with unsupported items and unknown values

Versions before Zabbix 3.2 are very strict about unsupported items in a
trigger expression. Any unsupported item in the expression immediately
renders trigger value to `Unknown`.

Since Zabbix 3.2 there is a more flexible approach to unsupported items
by admitting unknown values into expression evaluation:

-   For the nodata() function, the values are not affected by whether an
    item is supported or unsupported. The function is evaluated even if
    it refers to an unsupported item.
-   Logical expressions with OR and AND can be evaluated to known values
    in two cases regardless of unknown operands:
    -   "1 `or` Unsuported\_item1.some\_function() `or`
        Unsuported\_item2.some\_function() `or` ..." can be evaluated to
        '1' (True),
    -   "0 `and` Unsuported\_item1.some\_function() `and`
        Unsuported\_item2.some\_function() `and` ..." can be evaluated
        to '0' (False).\
        Zabbix tries to evaluate logical expressions taking unsupported
        items as `Unknown` values. In the two cases mentioned above a
        known value will be produced; in other cases trigger value will
        be `Unknown`.
-   If a function evaluation for supported item results in error, the
    function value is `Unknown` and it takes part in further expression
    evaluation.

Note that unknown values may "disappear" only in logical expressions as
described above. In arithmetic expressions unknown values always lead to
result `Unknown` (except division by 0).

If a trigger expression with several unsupported items evaluates to
`Unknown` the error message in the frontend refers to the last
unsupported item evaluated.

[comment]: # ({/new-0cc34ec7})

[comment]: # ({new-adfc6b9d})
##### Example 12

Using the value of another item to get a trigger threshold:

    last(/Template PfSense/hrStorageFree[{#SNMPVALUE}])<last(/Template PfSense/hrStorageSize[{#SNMPVALUE}])*0.1

The trigger will fire if the free storage drops below 10 percent.

[comment]: # ({/new-adfc6b9d})

[comment]: # ({new-c2b4949f})
##### Example 13

Using [evaluation result](#operators) to get the number of triggers over
a threshold:

    (last(/server1/system.cpu.load[all,avg1])>5) + (last(/server2/system.cpu.load[all,avg1])>5) + (last(/server3/system.cpu.load[all,avg1])>5)>=2

The trigger will fire if at least two of the triggers in the expression
are over 5.

[comment]: # ({/new-c2b4949f})

[comment]: # ({new-60fad4fc})
##### Example 14

Comparing string values of two items - operands here are functions that
return strings.

Problem: create an alert if Ubuntu version is different on different
hosts

    last(/NY Zabbix server/vfs.file.contents[/etc/os-release])<>last(/LA Zabbix server/vfs.file.contents[/etc/os-release])

[comment]: # ({/new-60fad4fc})

[comment]: # ({new-6906462b})
##### Example 15

Comparing two string values - operands are:

-   a function that returns a string
-   a combination of macros and strings

Problem: detect changes in the DNS query

The item key is:

    net.dns.record[8.8.8.8,{$WEBSITE_NAME},{$DNS_RESOURCE_RECORD_TYPE},2,1]

with macros defined as

    {$WEBSITE_NAME} = example.com
    {$DNS_RESOURCE_RECORD_TYPE} = MX

and normally returns:

    example.com           MX       0 mail.example.com

So our trigger expression to detect if the DNS query result deviated
from the expected result is:

    last(/Zabbix server/net.dns.record[8.8.8.8,{$WEBSITE_NAME},{$DNS_RESOURCE_RECORD_TYPE},2,1])<>"{$WEBSITE_NAME}           {$DNS_RESOURCE_RECORD_TYPE}       0 mail.{$WEBSITE_NAME}"

Notice the quotes around the second operand.

[comment]: # ({/new-6906462b})

[comment]: # ({new-3f1b1c81})
##### Example 16

Comparing two string values - operands are:

-   a function that returns a string
-   a string constant with special characters \\ and "

Problem: detect if the `/tmp/hello` file content is equal to:

    \" //hello ?\"

Option 1) write the string directly

    last(/Zabbix server/vfs.file.contents[/tmp/hello])="\\\" //hello ?\\\""

Notice how \\ and " characters are escaped when the string gets compared
directly.

Option 2) use a macro

    {$HELLO_MACRO} = \" //hello ?\"

in the expression:

    last(/Zabbix server/vfs.file.contents[/tmp/hello])={$HELLO_MACRO}

[comment]: # ({/new-3f1b1c81})

[comment]: # ({new-942a7e0e})
##### Example 17

Comparing long-term periods.

Problem: Load of Exchange server increased by more than 10% last month

    trendavg(/Exchange/system.cpu.load,1M:now/M)>1.1*trendavg(/Exchange/system.cpu.load,1M:now/M-1M)

You may also use the [Event
name](/manual/config/triggers/trigger#configuration) field in trigger
configuration to build a meaningful alert message, for example to
receive something like

`"Load of Exchange server increased by 24% in July (0.69) comparing to June (0.56)"`

the event name must be defined as:

    Load of {HOST.HOST} server increased by {{?100*trendavg(//system.cpu.load,1M:now/M)/trendavg(//system.cpu.load,1M:now/M-1M)}.fmtnum(0)}% in {{TIME}.fmttime(%B,-1M)} ({{?trendavg(//system.cpu.load,1M:now/M)}.fmtnum(2)}) comparing to {{TIME}.fmttime(%B,-2M)} ({{?trendavg(//system.cpu.load,1M:now/M-1M)}.fmtnum(2)})

It is also useful to allow manual closing in trigger configuration for
this kind of problem.

[comment]: # ({/new-942a7e0e})

[comment]: # ({new-bddf0717})
#### Hysteresis

Sometimes an interval is needed between problem and recovery states,
rather than a simple threshold. For example, if we want to define a
trigger that reports a problem when server room temperature goes above
20°C and we want it to stay in the problem state until the temperature
drops below 15°C, a simple trigger threshold at 20°C will not be enough.

Instead, we need to define a trigger expression for the problem event
first (temperature above 20°C). Then we need to define an additional
recovery condition (temperature below 15°C). This is done by defining an
additional *Recovery expression* parameter when
[defining](/manual/config/triggers/trigger) a trigger.

In this case, problem recovery will take place in two steps:

-   First, the problem expression (temperature above 20°C) will have to
    evaluate to FALSE
-   Second, the recovery expression (temperature below 15°C) will have
    to evaluate to TRUE

The recovery expression will be evaluated only when the problem event is
resolved first.

::: notewarning
The recovery expression being TRUE alone does not
resolve a problem if the problem expression is still TRUE!
:::

[comment]: # ({/new-bddf0717})

[comment]: # ({new-6fc2eb1c})
##### Example 1

Temperature in server room is too high.

Problem expression:

    last(/server/temp)>20

Recovery expression:

    last(/server/temp)<=15

[comment]: # ({/new-6fc2eb1c})

[comment]: # ({new-contribute})

::: note-contribute
Have a trigger expressions example that might be useful to others? Use the [Example suggestion form](#report-example) to send it to Zabbix developers.
:::

[comment]: # ({/new-contribute})
