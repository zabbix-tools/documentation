[comment]: # translation:outdated

[comment]: # ({new-29480506})
# 1 Configuração de trigger

[comment]: # ({/new-29480506})

[comment]: # ({new-ea19cfe1})
#### Visão geral

Para configurar uma trigger será necessário:

-   Acessar: *Configuração* → *Hosts* ou *Configuração* → *Templates*
-   Clicar no link *Triggers* na linha do host ou template
-   Clicar no botão *Criar trigger* situado no canto superior da tela
    (ou no nome da trigger que se deseje atualizar a configuração)
-   Informar os parâmetros da trigger no formulário

[comment]: # ({/new-ea19cfe1})

[comment]: # ({new-f29d794b})
#### Configuração

A aba **Trigger** contêm todos os atributos essenciais de uma trigger.

![](../../../../assets/en/manual/config/triggers/trigger.png)

|Parâmetro|Descrição|
|----------|-----------|
|*Nome*|Nome da trigger.<br>O nome poderá conter as macros [suportadas](/pt/manual/appendix/macros/supported_by_location): `{HOST.HOST}, {HOST.NAME}, {HOST.CONN}, {HOST.DNS}, {HOST.IP}, {ITEM.VALUE}, {ITEM.LASTVALUE}` e `{$MACRO}`.<br>As macros referente aos parâmetros da chave (**$1, $2...$9**) podem ser utilizadas também.<br>*Observe*: As macros $1-$9 irão resolver corretamente se estiverem se referindo a constantes em expressões relativamente simples. Por exemplo, o nome "Processor load above $1 on {HOST.NAME}" irá automaticamente modificar para "Processor load above 5 on New host" se a expressão for {New host:system.cpu.load\[percpu,avg1\].last()}>5|
|*Expressão*|[Expressão](expression) lógica utilizada para calcular o estado da trigger.|
|*Geração de múltiplos eventos de INCIDENTE*|Ao marcar esta opção você configura que este evento poderá gerar um novo evento a cada nova validação da expressão em estado de 'Incidente'.|
|*Descrição*|Campo texto utilizado para prover mais informações sobre a trigger. Poderá conter, por exemplo, informações osbre como corrigir problemas específicos, detalhes de contato do pessoal de suporte, etc.<br>*Adicionado no Zabbix 2.2*, a descrição pode conter o mesmo conjunto de macros que o nome da trigger.|
|*URL*|Se não estiver vazio, a URL informada aqui estará disponível em um link quando clicar no nome da trigger em *Monitoramento → Triggers*.<br>Algumas macros podem ser utilizadas neste campo - {TRIGGER.ID}, diversas [{HOST.\*}](/pt/manual/appendix/macros/supported_by_location) macros (desde o Zabbix 3.0.0) e macros de usuário (desde o Zabbix 3.0.0).|
|*Severidade*|Defina o nível de [severidade](severity) ao clicar no botão desejado.|
|*Enabled*|Desmarque esta opção para inativar a trigger.|

A aba de **Dependências** contêm todas as [dependências](dependencies)
da trigger.

Clique em *Adicionar* para adicionar uma nova dependência.

::: noteclassic
Você também pode configurar uma trigger ao abrir uma
pré-existente, clicar no botão *Clone*, modificar os dados e clicar no
botão *Adicionar*.
:::

[comment]: # ({/new-f29d794b})


[comment]: # ({new-dbfe2e70})
#### Testing expressions

It is possible to test the configured trigger expression as to what the
expression result would be depending on the received value.

The following expression from an official template is taken as an
example:

    avg(/Cisco IOS SNMPv2/sensor.temp.value[ciscoEnvMonTemperatureValue.{#SNMPINDEX}],5m)>{$TEMP_WARN}
    or
    last(/Cisco IOS SNMPv2/sensor.temp.status[ciscoEnvMonTemperatureState.{#SNMPINDEX}])={$TEMP_WARN_STATUS}

To test the expression, click on *Expression constructor* under the
expression field.

![](../../../../assets/en/manual/config/triggers/trigger_test.png)

In the Expression constructor, all individual expressions are listed. To
open the testing window, click on *Test* below the expression list.

![](../../../../assets/en/manual/config/triggers/expr_test_button.png){width="600"}

In the testing window you can enter sample values ('80', '70', '0', '1'
in this example) and then see the expression result, by clicking on the
*Test* button.

![](../../../../assets/en/manual/config/triggers/expr_test.png){width="600"}

The result of the individual expressions as well as the whole expression
can be seen.

"TRUE" means that the specified expression is correct. In this
particular case A, "80" is greater than the {$TEMP\_WARN} specified
value, "70" in this example. As expected, a "TRUE" result appears.

"FALSE" means that the specified expression is incorrect. In this
particular case B, {$TEMP\_WARN\_STATUS} "1" needs to be equal with
specified value, "0" in this example. As expected, a "FALSE" result
appears.

The chosen expression type is "OR". If at least one of the specified
conditions (A or B in this case) is TRUE, the overall result will be
TRUE as well. Meaning that the current value exceeds the warning value
and a problem has occurred.

[comment]: # ({/new-dbfe2e70})
