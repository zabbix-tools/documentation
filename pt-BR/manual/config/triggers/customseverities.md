[comment]: # translation:outdated

[comment]: # ({new-81dd14e7})
# 5 Customizando as severidades de trigger

Os nomes e cores das severidades de trigger associadas aos elementos da
interface web do Zabbix podem ser configurados em *Administração → Geral
→ Severidades de Trigger*.

::: noteclassic
As cores são compartilhadas entre todos os temas da
interface do Zabbix.
:::

[comment]: # ({/new-81dd14e7})

[comment]: # ({new-78725316})
#### Customizando os nomes de severidades

::: noteimportant
Se uma tradução da interface web do Zabbix for
utilizada, os nomes customizados de severidades irão sobrepor a
tradução.
::: Os nomes padrões de severidades de trigger estão
disponíveis para tradução em todas as localizações. Se o nome de uma
severidade for modificado, será necessária a atualização manual de todas
as traduções.

Processo de customização de um nome traduzido:

-   Defina o nome de severidade, por exemplo 'Important'
-   Edite o arquivo
    <frontend\_dir>/locale/<required\_locale>/LC\_MESSAGES/frontend.po
-   Adicione 2 linhas:

```{=html}
<!-- -->
```
    msgid "Important"
    msgstr "<translation string>"

e salve o arquivo.

-   crie os arquivos '.mo' através do processo descrito em
    <frontend\_dir>/locale/README

O **msgid** precisa ser compatível com o novo nome de severidade e
**msgstr** precisa ser traduzido para cada idioma.

Este processo deve ser feito após cada mudança de nome de severidade.

[comment]: # ({/new-78725316})
