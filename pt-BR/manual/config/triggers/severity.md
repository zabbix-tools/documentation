[comment]: # translation:outdated

[comment]: # ({new-f6de9b10})
# 4 Severidade das Triggers

A severidade de uma trigger define o quão importante ela é. O Zabbix
suporta seis níveis de severidade:

|SEVERIDADE|DEFINIÇÃO|COR|
|----------|-----------|---|
|**Não classificado**|Severidade desconhecida.|Cinza|
|**Informação**|Apenas informacional.|Verde claro|
|**Atenção**|Esteja avisado.|Amarelo|
|**Média**|Problema (incidente) médio.|Laranja|
|**Alta**|Algo importante ocorreu.|Vermelho|
|**Desastre**|Desastre. Perdas financeiras, perda total de capacidade, etc.|Vermelho escuro|

As severidades são utilizadas para:

-   Diferenciação visual de triggers. Cores diferentes para níveis
    diferentes de severidade.
-   Alarmes sonoros. Diferentes áudios para diferentes severidades.
-   Definição de qual mídia usar para notificação. Diferentes mídias
    (canais de comunicação) para diferentes severidades. Por exemplo,
    SMS - Alta severidade, e-mail - outras.
-   Limitando ações através das condições baseadas em severidades de
    trigger (só enviar a notificação se a severidade for igual ou
    superior a um determinado nível)

É possível [customizar os nomes e cores de
severidades](customseverities), embora não seja recomendável pois a
documentação não irá se adequar à esta customização.

[comment]: # ({/new-f6de9b10})
