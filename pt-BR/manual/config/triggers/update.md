[comment]: # translation:outdated

[comment]: # ({new-3a1b4ec0})
# - \#7 Atualização em massa

[comment]: # ({/new-3a1b4ec0})

[comment]: # ({new-66063302})
#### Visão geral

Através do recurso de atualização em massa você pode modificar alguns
atributos das triggers de forma global, por exemplo você pode adicionar
ou remover uma dependência entre triggers.

[comment]: # ({/new-66063302})

[comment]: # ({new-00b96653})
#### Utilizando a atualização em massa

Para atualizar em massa algumas triggers, faça o seguinte:

-   Selecione as triggers a atualizar a partir da lista
-   Clique no botão *Atualização em massa* na parte inferior da lista
-   Selecione os atributos que deseja atualizar
-   Defina os novos valores para os atributos e clique em *Atualizar*

![](../../../../assets/en/manual/config/trigger_mass_update.png)

*Substituir dependências* irá substituir as dependências entre triggers
(se existirem) pela que for informada na atualização em massa.

[comment]: # ({/new-00b96653})
