[comment]: # translation:outdated

[comment]: # ({new-526f6fc0})
# 2 Ações

[comment]: # ({/new-526f6fc0})

[comment]: # ({new-b19d9072})
#### Visão geral

Se você precisa que algumas operações ocorram como resultado de eventos
(por exemplo, enviar uma notificação), você precisa configurar ações.

Ações podem ser definidas como respostas a eventos, independente do seu
tipo:

-   Eventos de trigger (incidentes) - quando o status de uma trigger é
    modificado de *OK* para *INCIDENTE* ou quando retorna
-   Eventos de descoberta - quando algo é detectado
-   Eventos de autorregistro - quando novos agentes se autorregistram
-   Eventos internos - quando itens passam para o estado de 'não
    suportado' ou triggers vão para o estado 'desconhecido'

[comment]: # ({/new-b19d9072})

[comment]: # ({new-cf7fda79})
#### Configurando uma ação

Para configurar:

-   Acesse *Configuração → Ações*
-   A partir da caixa de seleção *Origem do evento* selecione a origem
    desejada
-   Clique no botão *Criar ação*
-   Defina os atributos gerais da ação
-   Defina as
    [condições](/pt/manual/config/notifications/action/conditions) que
    acionarão esta ação, através da aba **Condições**
-   Defina as
    [operações](/pt/manual/config/notifications/action/operation) a
    serem executadas, através da aba **Operações**

Atributos gerais:

![](../../../../assets/en/manual/config/action.png)

|Parâmetro|Descrição|
|----------|-----------|
|*Nome*|Nome único da ação.|
|*Assunto padrão*|Assunto padrão da mensagem. O assunto pode conter [macros](/pt/manual/config/notifications/action/operation/macros).|
|*Mensagem padrão*|A mensagem pode conter [macros](/pt/manual/config/notifications/action/operation/macros).|
|*Mensagem de recuperação*|Marque esta opção para habilitar uma mensagem de recuperação diferente.<br>Uma *Mensagem da recuperação* é uma forma especial de ser notificado quando um incidente é resolvido. Se estiver marcado, uma mensagem **simples** com um assunto e corpo personalizado será enviado quando a trigger retornar ao estado **OK**.<br>*Nota*: Para receber uma mensagem de recuperação, a [condição](/pt/manual/config/notifications/action/conditions) "Valor da trigger=*INCIDENTE*" deverá estar presente; Por outro lado, a condição "Valor da trigger=*OK*", não pode estar presente. (Se "Valor da trigger=*OK*" é definida a mensagem de recuperação não funciona; ao invés disso você terá um escalonamento de mensagens definidas e/ou comandos remotos da mesma forma que ocorre no modo de problema).<br>*Mensagem de recuperação* será enviada apenas para os que receberam mensagens sobre o início do problema antes.<br>Uma mensagem de recuperação herda o status de reconhecimento e histórico do problema (como quando expandidas as macros {EVENT.ACK.HISTORY} e {EVENT.ACK.STATUS}).<br>Se utilizar as macros {EVENT.\*} na mensagem de recuperação, elas irão se referir ao evento de problema (não ao de resolução).<br>As macros {EVENT.RECOVERY.\*} só serão expandidas na mensagem de recuperação e irão se referir ao evento de recuperação (OK).|
|*Assunto da recuperação*|Assunto da mensagem de recuperação. Pode conter macros.|
|*Mensagem da recuperação*|Mensagem específica para recuperação. Pode conter macros.|
|*Ativo*|Maque esta opção para habilitar a ação, desmarcar irá colocar a ação em modo inativo.|

[comment]: # ({/new-cf7fda79})
