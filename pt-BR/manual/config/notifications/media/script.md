[comment]: # translation:outdated

[comment]: # ({new-e25ccc09})
# 5 Scripts de alerta

[comment]: # ({/new-e25ccc09})

[comment]: # ({new-bc410af8})
#### Visão geral

Se os tipos de mídia que vem com o Zabbix não forem suficientes para
suas necessidades de notificação você poderá definir seus próprios
scripts de notificação para fazer as coisas da forma que precisar.

Os scripts de alerta são executados no Zabbix Server e deverão estar
situados no diretório definido no [arquivo de configuração do
servidor](/pt/manual/appendix/config/zabbix_server) através da diretiva
**AlertScriptsPath**.

Um exemplo de script personalizado de alerta:

``` {.bash}
#!/bin/bash

to=$1
subject=$2
body=$3

cat <<EOF | mail -s "$subject" "$to"
$body
EOF
```

As variáveis de ambiente não são preservadas ou criadas para o script,
logo ele terá que tratar isso por sí próprio.

[comment]: # ({/new-bc410af8})

[comment]: # ({new-47055351})
#### Configuração

Para configurar um tipo de mídia baseada em Script:

-   Acesse *Administração → Tipos de mídia*
-   Clique no botão *Criar tipo de mídia*.

![](../../../../../assets/en/manual/config/media_script.png)

Atributos do tipo de mídia:

|Parâmetro|Descrição|
|----------|-----------|
|*Nome*|Nome do tipo de mídia.|
|*Tipo*|Selecione *Script*.|
|*Nome do Script*|Informe o nome do script a ser executado.|
|*Parâmetros do script*|Adicione parâmetros de linha de comando ao script.<br>As [macros](/pt/manual/appendix/macros/supported_by_location) {ALERT.SENDTO}, {ALERT.SUBJECT} e {ALERT.MESSAGE} são suportadas nos parâmetros do script.<br>Suportado desde o Zabbix 3.0.|

[comment]: # ({/new-47055351})

[comment]: # ({new-6a270eac})

#### Media type testing

To test a configured script media type:

-   Locate the relevant script in the [list](/manual/config/notifications/media#overview) of media types.
-   Click on *Test* in the last column of the list (a testing window
    will open).
-   Edit the script parameter values, if needed (editing affects the test procedure only, the actual values will not be changed).
-   Click on *Test*.

![](../../../../../assets/en/manual/config/notifications/media/script_test.png){width="600"}


[comment]: # ({/new-6a270eac})

[comment]: # ({new-525cb972})
#### Mídia de usuário

Para definir o script para o usuaŕio:

-   Acesse *Administração → Usuários*
-   Abra o formulário de propriedades do usuário
-   Na aba **Mídia**, clique no link *Adicionar*

Atributos da mídia do usuário:

|Parâmetro|Descrição|
|----------|-----------|
|*Tipo*|Defina com o nome do tipo de mídia de Script cadastrado previamente.|
|*Enviar para*|Defina endereço Jabber para o qual as mensagens serão enviadas.|
|*Ativo quando*|Você pode limitar os dias e horários em que as notificações serão enviadas, por exemplo, apenas em dias e horários de trabalho (1-5,09:00-18:00).<br>Consulte as [especificações de períodos de hora](/pt/manual/appendix/time_period) para maiores detalhes sobre o formato.|
|*Usar se severidade*|Marque os níveis de severidade que você gostaria que o usuário recebesse as notificações.<br>*Observação* para [eventos](/pt/manual/config/events) não baseados em triggers o valor padrão de severidade é ('Não classificado'), então deixe esta opção marcada se você deseja receber notificações sobre eventos não baseados em trigger (autobusca, autorregistro, etc).|
|*Status*|Marque esta opção para ativar esta mídia para este usuário.|

[comment]: # ({/new-525cb972})
