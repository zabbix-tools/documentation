[comment]: # translation:outdated

[comment]: # ({new-d40475f4})
# 1 E-mail

[comment]: # ({/new-d40475f4})

[comment]: # ({new-9d59e93d})
#### Visão geral

Para configurar o e-mail como um canal de entrega de mensagens, você
precisa configurar o tipo de mídia **E-mail** e informar os endereços de
e-mail dos usuários.

[comment]: # ({/new-9d59e93d})

[comment]: # ({new-3711e99d})
#### Configuração

Para configurar o e-mail como um tipo de mídia:

-   Acesse *Administração → Tipos de mídia*
-   Clique em *Criar tipo de mídia* (ou clique em *E-mail* na lista de
    tipos de mídia pré-definidos).

![](../../../../../assets/en/manual/config/media_email.png)

Atributos dos tipos de mídia:

|Parâmetro|Descrição|
|----------|-----------|
|*Nome*|Nome do tipo de mídia.|
|*Tipo*|Selecione *E-mail* como o tipo.|
|*Servidor SMTP*|O endereço do servidor SMTP responsável pelo envio das mensagens aos destinatários (relay).|
|*Porta do servidor SMTP*|Porta do servidor SMTP.<br>Esta opção é suportada a partir do Zabbix 3.0.|
|*SMTP helo*|Mensagem de "SMTP helo", normalmente o nome do domínio.|
|*E-mail SMTP*|Endereço de e-mail que será utilizado no campo **De** (**From**) nas mensagens enviadas (o remetente).<br>A adição de nome de exibição no remetente (Ex. "Zabbix-HQ" em *Zabbix-HQ <zabbix\@company.com>* na imagem a seguir) junto do endereço do remetente é suportado desde o Zabbix 2.2.<br>Os nomes de exibição deverão ser compatíveis com a RFC 5322. Exemplos:<br>Validos:<br>*zabbix\@company.com* (apenas o e-mail sem colchetes)<br>*Zabbix HQ <zabbix\@company.com>* (nome de exibição e endereço de e-mail com colchetes)<br>*∑Ω-monitoring <zabbix\@company.com>* (caracteres UTF-8 no nome de exibição)<br>Inválidos:<br>*Zabbix HQ zabbix\@company.com* (nome de exibição e endereço de e-mail presentes mas sem ser separados por colchetes)<br>*"Zabbix\\@\\<H(comment)Q\\>" <zabbix\@company.com>* (embora válido pela RFC 5322, citações e comentários não são suportados no Zabbix)|
|*Segurança de conexão*|Nível de segurança da conexão:<br>**Nenhum** - não será utilizada a opção [CURLOPT\_USE\_SSL](http://curl.haxx.se/libcurl/c/CURLOPT_USE_SSL.html)<br>**STARTTLS** - será utilizado o CURLOPT\_USE\_SSL com CURLUSESSL\_ALL<br>**SSL/TLS** - o uso de CURLOPT\_USE\_SSL será opcional<br>Esta opção é suportada desde o Zabbix 3.0.|
|*Verificação de par SSL*|Marque esta opção para verificar o certificado SSL do servidor SMTP.<br>O valor do servidor de "SSLCALocation" pode ser configurado através da diretiva [CURLOPT\_CAPATH](http://curl.haxx.se/libcurl/c/CURLOPT_CAPATH.html) para a validação do certificado.<br>Isso configura a opção cURL [CURLOPT\_SSL\_VERIFYPEER](http://curl.haxx.se/libcurl/c/CURLOPT_SSL_VERIFYPEER.html).<br>Suportado desde o Zabbix 3.0.|
|*Verificação SSL do host*|Marque esta opção para verificar o campo *Common Name* com o *Subject Alternate Name* do servidor SMTP.<br>Esta é a opção do cURL [CURLOPT\_SSL\_VERIFYHOST](http://curl.haxx.se/libcurl/c/CURLOPT_SSL_VERIFYHOST.html).<br>Suportado desde o Zabbix 3.0.|
|*Autenticação*|Nível de autenticação:<br>**Nenhum** - nenhuma configuração adicional no cURL<br>**Senha normal** - [CURLOPT\_LOGIN\_OPTIONS](http://curl.haxx.se/libcurl/c/CURLOPT_LOGIN_OPTIONS.html) será definido como "AUTH=PLAIN"<br>Suportado desde o Zabbix 3.0.|
|*Usuário*|Nome de usuário a ser utilizado na autenticação.<br>Configura a opção cURL [CURLOPT\_USERNAME](http://curl.haxx.se/libcurl/c/CURLOPT_USERNAME.html).<br>Suportado desde o Zabbix 3.0.|
|*Password*|Senha a ser utilizada na autenticação.<br>Configura a opção cURL [CURLOPT\_PASSWORD](http://curl.haxx.se/libcurl/c/CURLOPT_PASSWORD.html).<br>Suportado desde o Zabbix 3.0.|
|*Ativo*|Maque esta opção para ativar este tipo de mídia.|

::: noteimportant
Para que a autenticação SMTP seja possível o
Zabbix Server deverá ter a opção --with-libcurl [definida durante a
compilação](/pt/manual/installation/install#configure_the_sources).

:::

[comment]: # ({/new-3711e99d})

[comment]: # ({new-98f29ec0})

#### Media type testing

To test whether a configured e-mail media type works correctly:

-   Locate the relevant e-mail in the [list](/manual/config/notifications/media#overview) of media types.
-   Click on *Test* in the last column of the list (a testing window
    will open).
-   Enter a *Send to* recipient address, message body and, optionally,
    subject.
-   Click on *Test* to send a test message.

Test success or failure message will be displayed in the same window:

![](../../../../../assets/en/manual/config/notifications/media/test_email0.png){width="600"}

[comment]: # ({/new-98f29ec0})

[comment]: # ({new-5e74f274})
#### Mídia de usuário

Para definir o endereço específico de cada usuário:

-   Acesse *Administração → Usuários*
-   Abra o formulário de propriedades do usuário
-   Na aba **Mídia**, clique no botão *Adicionar*

![](../../../../../assets/en/manual/config/user_email2.png)

Atributos da mídia de usuário:

|Parâmetro|Descrição|
|----------|-----------|
|*Tipo*|Defina como *E-mail*.|
|*Enviar para*|Defina o endereço de e-mail de destino das mensagens. É suportado o envio com o nome de exibição (Ex. “Some User” em *Some User <user\@domain.tld>* como na imagem acima) desde o Zabbix 2.2.<br>Veja exemplos e restrições sobre nome de exibição e endereço de e-mail no atributo [E-mail SMTP](email#configuration).|
|*Ativo quando*|Você pode limitar os dias e horários em que as notificações serão enviadas, por exemplo, apenas em dias e horários de trabalho (1-5,09:00-18:00).<br>Consulte as [especificações de períodos de hora](/pt/manual/appendix/time_period) para maiores detalhes sobre o formato.|
|*Usar se severidade*|Marque os níveis de severidade que você gostaria que o usuário recebesse as notificações.<br>*Observação* para [eventos](/pt/manual/config/events) não baseados em triggers o valor padrão de severidade é ('Não classificado'), então deixe esta opção marcada se você deseja receber notificações sobre eventos não baseados em trigger (autobusca, autorregistro, etc).|
|*Status*|Marque esta opção para ativar esta mídia para este usuário.|

[comment]: # ({/new-5e74f274})
