[comment]: # translation:outdated

[comment]: # ({new-adbf1c90})
# 2 SMS

[comment]: # ({/new-adbf1c90})

[comment]: # ({new-657f31be})
#### Visão geral

O Zabbix suporta o envio de mensagens SMS utilizando modem GSM serial
conectado ao servidor Zabbix.

Certifique-se que:

-   A velocidade do dispositivo serial (normalmente /dev/ttyS0 no Linux)
    é a mesma do modem GSM. O Zabbix não irá configurar a velocidade
    serial, utilizará a configuração padrão.
-   O usuário 'zabbix' possui permissão de leitura/escrita no
    dispositivo serial. Execute o comando 'ls –l /dev/ttyS0' para ver as
    permissões atuais do dispositivo.
-   O modem GSM tem o PIN informado e o mantêm após um reinicio.
    Alternativamente você pode desativar o PIN no cartão SIM. O PIN pode
    ser informado com o comando 'AT+CPIN="NNNN"' (NNNN é o seu número
    PIN, as aspas duplas devem ser mantidas) em um software de terminal,
    tanto em um 'Unix minicom' quanto em um 'Windows HyperTerminal'.

O Zabbix é testado com estes modens GSM:

-   Siemens MC35
-   Teltonika ModemCOM/G10

Para configurar o SMS como um canal de entrega de mensagens você precisa
configurar o tipo de mídia no Zabbix e definir uma mídia para cada
usuário com o seu número SMS.

[comment]: # ({/new-657f31be})

[comment]: # ({new-e7ed9491})
#### Configuração

Para configurar o SMS como um tipo de mídia:

-   Acesse *Administração → Tipos de mídia*
-   Clique no botão *Criar tipo de mídia* (ou clique no *SMS* na lista
    pré-definida de tipos de mídia).

Atributos do tipo de mídia:

|Parâmetro|Descrição|
|----------|-----------|
|*Nome*|Nome do tipo de mídia.|
|*Tipo*|Selecione *SMS*.|
|*Modem GSM*|Defina o nome do dispositivo serial associado ao modem GSM.|

[comment]: # ({/new-e7ed9491})

[comment]: # ({new-84a44424})
#### Mídia de usuário

Para definir o número de telefone do usuário:

-   Acesse *Administração → Usuários*
-   Abra o formulário de propriedades do usuário
-   Na aba **Mídia**, clique no link *Adicionar*

Atributos da mídia do usuário:

|Parâmetro|Descrição|
|----------|-----------|
|*Tipo*|Defina como *SMS*.|
|*Enviar para*|Defina número de telefone para o qual as mensagens serão enviadas.|
|*Ativo quando*|Você pode limitar os dias e horários em que as notificações serão enviadas, por exemplo, apenas em dias e horários de trabalho (1-5,09:00-18:00).<br>Consulte as [especificações de períodos de hora](/pt/manual/appendix/time_period) para maiores detalhes sobre o formato.|
|*Usar se severidade*|Marque os níveis de severidade que você gostaria que o usuário recebesse as notificações.<br>*Observação* para [eventos](/pt/manual/config/events) não baseados em triggers o valor padrão de severidade é ('Não classificado'), então deixe esta opção marcada se você deseja receber notificações sobre eventos não baseados em trigger (autobusca, autorregistro, etc).|
|*Status*|Marque esta opção para ativar esta mídia para este usuário.|

[comment]: # ({/new-84a44424})
