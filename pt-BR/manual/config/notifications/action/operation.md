[comment]: # translation:outdated

[comment]: # ({new-531c9a02})
# - \#1 Operações

[comment]: # ({/new-531c9a02})

[comment]: # ({new-49564975})
#### Visão geral

Você pode definir estas ações para todas as origens de eventos:

-   Enviar uma mensagem
-   Executar um comando remoto (incluindo IPMI)

Adicionalmente, para eventos de descoberta, existem as seguintes
operações:

-   adicionar host
-   remover host
-   habilitar host
-   desabilitar host
-   adicionar a grupo
-   remover do grupo
-   associar a template
-   desassociar a template
-   definir o modo de inventário

Adicionalmente, para eventos de autorregistro, existem as seguintes
operações:

-   adicionar host
-   desabilitar host
-   adicionar a grupo
-   associar a template
-   definir o modo de inventário

[comment]: # ({/new-49564975})

[comment]: # ({new-4b8a4b65})
#### Configurando uma operação

Para configurar uma operação:

-   Acesse a aba *Operações* do formulário de propriedades da ação
-   Clique no link *Nova*
-   Informe os atributos da operação e clique no link *Adicionar* do
    bloco *Detalhes da operação* para adiciona-la a lista de *Operações
    da ação*

Atributos da operação:

![](../../../../../assets/en/manual/config/action_operation.png){width="600"}

|Parâmetro|<|<|<|<|Descrição|
|----------|-|-|-|-|-----------|
|*Duração padrão do passo da operação*|<|<|<|<|Duração padrão de um passo de operação (o mínimo é de 60 segundos).<br>Por exemplo, um passo de uma hora quer dizer que quando um passo for executado a ação irá aguardar mais uma hora para executar o próximo passo.|
|*Operações da ação*|<|<|<|<|Lista resumida das operações da ação, com os seguintes detalhes:<br>**Passos** - passo(s) do escalonamento ao qual a operação está associada<br>**Detalhes** - tipo da operação e destinatário.<br>Desde o Zabbix 2.2, a lista de operações também apresenta o tipo de mídia (e-mail, SMS, Jabber, etc) utilizado para enviar uma mensagem assim como o nome e sobrenome do destinatário (em parênteses após o apelido).<br>**Iniciar em** - tempo de espera antes da operação<br>**Duração (segundos)** - duração do passo. *Padrão* será apresentado caso o passo utilize o tempo padrão, se a duração for customizada será apresentado o tempo configurado.<br>**Ação** - links para editar ou remover a operação.<br>Pra configurar uma nova operação clique no link *Nova* do bloco *Operações da ação*.<br>|
|*Detalhes da operação*|<|<|<|<|Este bloco é utilizado para configurar os detalhes de uma operação.|
|<|*Passo*|<|<|<|Define o(s) passo(s) associados a operação no processo de [escalonamento](escalations):<br>**De** - comece a executar neste passo<br>**Até** - continue executando até este passo (0=infinito, a execução não será limitada)<br>**Duração do passo** - duração personalizada para esta operação (0=usa o padrão).<br>Várias operações podem ser definidas ao mesmo passo. Se estas operações possuírem tempos diferentes de duração, o menor tempo será aplicado ao passo.|
|^|*Tipo da operação*|<|<|<|Dois tipos de operação estão disponíveis para todos os eventos:<br>**Enviar mensagem** - envia mensagem a um usuário ou grupo de usuários<br>**Comando remoto** - executa um comando remoto<br>Mais tipos de operação estão disponíveis para ações de descoberta e autorregistro.|
|^|<|Tipo da operação: [enviar mensagem](/pt/manual/config/notifications/action/operation/message)|<|<|<|
|^|^|*Enviar para grupos de usuários*|<|<|Clique no link *Adicionar* dentro deste bloco para selecionar os grupos de usuários para os quais a mensagem deverá ser enviada.<br>O grupo de usuário deverá ter, no mínimo, [permissão](/pt/manual/config/users_and_usergroups/permissions) de leitura no host para receber a notificação.|
|^|^|*Enviar para usuários*|<|<|Clique no link *Adicionar* dentro deste bloco para selecionar os usuários para os quais a mensagem deverá ser enviada.<br>O usuário deverá ter, no mínimo, [permissão](/pt/manual/config/users_and_usergroups/permissions) de leitura no host para ser notificado.|
|^|^|*Enviar apenas para*|<|<|Envia a mensagem para todos os tipos de mídia disponíveis ou apenas para um selecionado.|
|^|^|*Mensagem padrão*|<|<|Se ativo, a mensagem padrão será utilizada conforme definido na aba [Ação](/pt/manual/config/notifications/action).|
|^|^|*Assunto*|<|<|Assunto personalizado da mensagem. O assunto pode conter macros.|
|^|^|*Mensagem*|<|<|Mensagem personalizada. A mensagem pode conter macros.|
|^|^|Tipo da operação: [Comando remoto](/pt/manual/config/notifications/action/operation/remote_command)|<|<|<|
|^|^|*Lista de destinos*|<|<|Selecione como destino o **Host atual**, **Host** (outro host) ou um **Grupo de hosts** contra os quais o comando remoto será executado.|
|^|^|*Tipo*|<|<|Selecione o tipo do comando:<br>**IPMI** - comando IPMI<br>**Script personalizado** - executa um conjunto personalizado de comandos. Você pode executar os comandos no Zabbix Server ou no Zabbix Agent.<br>**SSH** - executa um comando SSH<br>**Telnet** - executa um comando Telnet<br>**Script global** - executa um dos scripts globais definidos em *Administração → Scripts*.|
|^|^|*Comandos*|<|<|Informe o(s) comando(s).|
|<|*Condições*|<|<|<|Condições para execução do comando na operação:<br>**Não reconhecido** - o comando só será executado se o evento não tiver sido reconhecido<br>**Reconhecido** - o comando só será executado se o evento tiver sido reconhecido.|

[comment]: # ({/new-4b8a4b65})


[comment]: # ({new-4a37f720})
#### Operation details

![](../../../../../assets/en/manual/config/operation_details.png)

|Parameter|<|<|<|Description|
|---------|-|-|-|-----------|
|*Operation*|<|<|<|Select the operation:<br>**Send message** - send message to user<br>**<remote command name>** - execute a remote command. Commands are available for execution if previously defined in [global scripts](/manual/web_interface/frontend_sections/administration/scripts#configuring_a_global_script) with *Action operation* selected as its scope.<br>More operations are available for discovery and autoregistration based events (see above).|
|*Steps*|<|<|<|Select the step(s) to assign the operation to in an [escalation](escalations) schedule:<br>**From** - execute starting with this step<br>**To** - execute until this step (0=infinity, execution will not be limited)|
|*Step duration*|<|<|<|Custom duration for these steps (0=use default step duration).<br>[Time suffixes](/manual/appendix/suffixes) are supported, e.g. 60s, 1m, 2h, 1d, since Zabbix 3.4.0.<br>[User macros](/manual/config/macros/user_macros) are supported, since Zabbix 3.4.0.<br>Several operations can be assigned to the same step. If these operations have different step duration defined, the shortest one is taken into account and applied to the step.|
|<|Operation type: [send message](/manual/config/notifications/action/operation/message)|<|<|<|
|^|*Send to user groups*|<|<|Click on *Add* to select user groups to send the message to.<br>The user group must have at least "read" [permissions](/manual/config/users_and_usergroups/permissions) to the host in order to be notified.|
|^|*Send to users*|<|<|Click on *Add* to select users to send the message to.<br>The user must have at least "read" [permissions](/manual/config/users_and_usergroups/permissions) to the host in order to be notified.|
|^|*Send only to*|<|<|Send message to all defined media types or a selected one only.|
|^|*Custom message*|<|<|If selected, the custom message can be configured.<br>For notifications about internal events via [webhooks](/manual/config/notifications/media/webhook), custom message is mandatory.|
|^|*Subject*|<|<|Subject of the custom message. The subject may contain macros. It is limited to 255 characters.|
|^|*Message*|<|<|The custom message. The message may contain macros. It is limited to certain amount of characters depending on the type of database (see [Sending message](/manual/config/notifications/action/operation/message) for more information).|
|^|Operation type: [remote command](/manual/config/notifications/action/operation/remote_command)|<|<|<|
|^|*Target list*|<|<|Select targets to execute the command on:<br>**Current host** - command is executed on the host of the trigger that caused the problem event. This option will not work if there are multiple hosts in the trigger.<br>**Host** - select host(s) to execute the command on.<br>**Host group** - select host group(s) to execute the command on. Specifying a parent host group implicitly selects all nested host groups. Thus the remote command will also be executed on hosts from nested groups.<br>A command on a host is executed only once, even if the host matches more than once (e.g. from several host groups; individually and from a host group).<br>The target list is meaningless if a custom script is executed on Zabbix server. Selecting more targets in this case only results in the script being executed on the server more times.<br>Note that for global scripts, the target selection also depends on the *Host group* setting in global script [configuration](/manual/web_interface/frontend_sections/administration/scripts#configuring_a_global_script).<br>*Target list* option is not available for *Service actions* because in this case remote commands are always executed on Zabbix server.|
|*Conditions*|<|<|<|Condition for performing the operation:<br>**Not ack** - only when the event is unacknowledged<br>**Ack** - only when the event is acknowledged.<br>*Conditions* option is not available for *Service actions*.|

When done, click on *Add* to add the operation to the list of
*Operations*.

[comment]: # ({/new-4a37f720})
