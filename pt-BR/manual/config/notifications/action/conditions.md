[comment]: # translation:outdated

[comment]: # ({new-aa1f9956})
# - \#2 Condições

[comment]: # ({/new-aa1f9956})

[comment]: # ({new-6ef58449})
#### Visão geral

Uma ação só é executada somente quando o evento atende à um conjunto de
condições.

[comment]: # ({/new-6ef58449})

[comment]: # ({new-6e7e1ac8})
#### Configuração

Para configurar:

-   Acesse a aba *Condições* do formulário de propriedades da ação
-   Selecione as condições desejadas a partir da caixa de seleção *Nova
    condição* e clique no link *Adicionar*
-   Selecione um tipo de cálculo (caso possua mais de uma condição)

![](../../../../../assets/en/manual/config/action_condition.png){width="600"}

As condições a seguir podem ser utilizadas para eventos com origem em
triggers (incidentes):

|Tipo da condição|Operadores suportados|Descrição|
|------------------|---------------------|-----------|
|*Aplicação*|=<br>como<br>diferente|Especifica ou exclui uma aplicação.<br>**=** - o evento pertence a uma trigger que possui item associado a uma aplicação.<br>**like** - o evento pertence a uma trigger que possui item associado a uma aplicação que contêm determinado texto.<br>**diferente** - o evento pertence a uma trigger que possui item associado a uma aplicação que NÃO CONTÊM determinado texto.|
|*Grupo do host*|=<br><>|Especifica ou exclui um grupo de hosts.<br>**=** - o evento pertence a determinado grupo.<br>**<>** - o evento NÃO PERTENCE a determinado grupo.|
|*Template*|=<br><>|Especifica ou exclui templates.<br>**=** - o evento pertence a uma trigger herdada de determinado template.<br>**<>** - o evento pertence a uma trigger QUE NÃO FOI HERDADA de determinado template.|
|*Host*|=<br><>|Especifica ou exclui hosts.<br>**=** - o evento pertence a determinado host.<br>**<>** - o evento NÃO PERTENCE a determinado host.|
|*Trigger*|=<br><>|Especifica ou exclui triggers.<br>**=** - o evento foi gerado por determinada trigger.<br>**<>** - o evento foi gerado por qualquer outra trigger que não a definida.|
|*Nome da trigger*|como<br>diferente|Especifica ou exclui a necessidade de ocorrência de determinado texto no nome da trigger.<br>**como** - o evento foi gerado por trigger cujo nome contêm determinado texto. Sensível ao caso.<br>**diferente** - o evento foi gerado por trigger cujo nome NÃO CONTÊM determinado texto. Sensível ao caso.<br>*Nota*: O valor informado será verificado com o nome da trigger após a expansão do valor de todas as macros.|
|*Severidade da trigger*|=<br><><br>>=<br><=|Define a severidade da trigger.<br>**=** - igual a severidade informada<br>**<>** - diferente da severidade informada<br>**>=** - maior ou igual à severidade informada<br>**<=** - menor ou igual à severidade informada.|
|*Valor da trigger*|=|Define um valor para a trigger.<br>**=** - igual ao valor da trigger (OK ou INCIDENTE)|
|*Intervalo*|em<br>não em|Especifica ou exclui um intervalo de tempo.<br>**em** - o evento deve começar dentro do intervalo.<br>**não em** - o evento não pode começar dentro do intervalo.<br>Consulte o manual de [definição de períodos de tempo](/pt/manual/appendix/time_period) para maiores informações sobre o formato.|
|*Status de manutenção*|em<br>não em|Especifica ou exclui que o host deva estar dentro de periodo de manutenção.<br>**em** - o host deverá estar em período de manuteção.<br>**não em** - o host não poderá estar em período de manutenção.<br>*Nota*: Se diversos hosts estiverem envolvidos na expressão da trigger a condição irá considerar se pelo menos um deles está ou deixa de estar em modo de manutenção.|

Valor da trigger:

-   se o valor da trigger mudar de OK para INCIDENTE, o valor da trigger
    é INCIDENTE
-   se o valor da trigger mudar de INCIDENTE para OK, o valor da trigger
    é OK

::: noteclassic
Quando uma nova ação para triggers é criada, ela vem com
duas condições automáticas (que podem ser removidas pelo usuário):

-   "Valor da trigger = *INCIDENTE*" - assim somente notificações de
    problema são enviadas. ISso quer dizer que se você configurar uma
    ação sem mais nenhuma condição específica ela só enviará mensagens
    quando os incidentes começarem. Ter esta condição como padrão é
    importante para que você possa receber uma única [mensagem de
    recuperação](/pt/manual/config/notifications/action#configuring_an_action).
-   "Status da manutenção = não em *manutenção*" - assim as mensagens
    não serão enviadas se os hosts estiverem em período de manutenção.


:::

As seguintes condições podem ser definidas para ações com origem em
descoberta de rede:

|Tipo da condição|Operadores suportados|Descrição|
|------------------|---------------------|-----------|
|*IP do host*|=<br><>|Especifica ou exclui um range de IP da descoberta de rede.<br>**=** - o IP do host está no range.<br>**<>** - o IP do host NÃO ESTÁ no range.<br>São suportados os seguintes formatos:<br>Simples IP: 192.168.1.33<br>Range de endereços IP: 192.168.1-10.1-254<br>Máscara de IPs: 192.168.4.0/24<br>Lista: 192.168.1.1-254, 192.168.2.1-100, 192.168.2.200, 192.168.4.0/24<br>O suporte a espaços na lista de IPs começou no Zabbix 3.0.0.|
|*Tipo do serviço*|=<br><>|Especifica ou exclui determinado tipo de serviço na descoberta de rede.<br>**=** - o serviço é o selecionado.<br>**<>** - o serviço não é do tipo selecionado.<br>Tipos disponíveis: SSH, LDAP, SMTP, FTP, HTTP, HTTPS *(disponível desde o Zabbix 2.2)*, POP, NNTP, IMAP, TCP, Zabbix agent, SNMPv1 agent, SNMPv2 agent, SNMPv3 agent, ICMP ping, telnet *(disponível desde o Zabbix 2.2)*.|
|*Porta do serviço*|=<br><>|Especifica ou exclui uma porta ou um range de portas TCP na descoberta de rede.<br>**=** - porta do serviço está no range.<br>**<>** - porta do serviço NÃO ESTÁ no range.|
|*Regra de descoberta*|=<br><>|Especifica ou exclui determinada regra de descoberta.<br>**=** - o host foi descoberto pela descoberta de rede selecionada.<br>**<>** - o host NÃO FOI descoberto pela descoberta de rede selecionada.|
|*Verificação de descoberta*|=<br><>|Especifica ou exclui determinada verificação de descoberta.<br>**=** - host foi descoberto pela regra de verificação selecionada.<br>**<>** - host NÃO FOI descoberto pela regra de verificação selecionada.|
|*Objeto de descoberta*|=|Define o tipo de objeto descoberto.<br>**=** - igual ao objeto selecionado (dispositivo ou serviço).|
|*Status da descoberta*|=|**Ligado** - tanto o host quanto os serviços verificados estão 'UP'<br>**Fora** - tanto o host quanto o serviço estão 'DOWN'<br>**Descoberto** - tanto o host quanto o serviço foram 'descobertos'<br>**Perdido** - tanto o host quanto o serviço não estão mais localizáveis|
|*Uptime/Downtime*|>=<br><=|Tempo em carga para o 'Host Up' e para 'Service Up'. Tempo fora de carga para 'Host Down' e 'Service Down'.<br>**>=** - é maior ou igual a. O parâmetro será informado em segundos.<br>**<=** - é menor ou igual a. O parâmetro será informado em segundos.|
|*Valor recebido*|=<br><><br>>=<br><=<br>como<br>diferente|Considera o valor recebido pelo agente (Zabbix, SNMP). Sensível ao caso. Se múltiplas verificações do mesmo tipo de agente existirem todos serão verificados (cada evento compatível gerará verificação de todas as demais condições).<br>**=** - igual ao valor.<br>**<>** - diferente do valor.<br>**>=** - maior ou igual ao valor.<br>**<=** - menor ou igual ao valor.<br>**como** - contêm o texto informado.<br>**diferente** - não contêm o texto informado.|
|*Proxy*|=<br><>|Especifica ou exclui determinado proxy.<br>**=** - usa o proxy informado.<br>**<>** - usa qualquer outro proxy ou o Zabbix Server.|

As condições a seguir podem ser utilizadas para eventos com origem em
autorregistro:

|Tipo da condição|Operadores suportados|Descrição|
|------------------|---------------------|-----------|
|*Metadados do host*|como<br>diferente|Especifica ou exclui que determinado metadado deverá estar presente.<br>**como** - metadado contêm texto.<br>**diferente** - metadado não contêm dado.<br>O metadado de host pode ser definido no [arquivo de configuração do agente](/pt/manual/appendix/config/zabbix_agentd).|
|*Nome do host*|como<br>diferente|Especifica ou exclui que determinado texto deverá estar presente no nome do host.<br>**como** - nome do host contêm o texto.<br>**diferente** - nome do host não contêm o texto.|
|*Proxy*|=<br><>|Especifica ou exclui determinado proxy.<br>**=** - usa o proxy informado.<br>**<>** - usa qualquer outro proxy ou o Zabbix Server.|

As condições a seguir podem ser utilizadas para eventos com origem
interna:

|Tipo da condição|Operadores suportados|Descrição|
|------------------|---------------------|-----------|
|*Aplicação*|=<br>como<br>diferente|Especifica ou exclui uma aplicação.<br>**=** - o evento pertence a uma trigger que possui item associado a uma aplicação.<br>**like** - o evento pertence a uma trigger que possui item associado a uma aplicação que contêm determinado texto.<br>**diferente** - o evento pertence a uma trigger que possui item associado a uma aplicação que NÃO CONTÊM determinado texto.|
|*Tipo do evento*|=|**O item está como 'não suportado'** - quando um item muda seu estado para 'não suportado'<br>**O item está em seu estado normal** - quando um item muda seu estado para 'normal'<br>**A regra de autobusca (LLD) está em estado 'não suportado'** - quando uma regra de autobusca muda seu estado para 'não suportado'<br>**A regra de autobusca (LLD) está em estado 'normal'** - quando uma regra de autobusca muda seu estado para 'normal'<br>**A trigger está no estado 'desconhecido'** - quando uma trigger muda seu estado para 'desconhecido'<br>**A trigger está no estado 'normal'** - quando uma trigger muda seu estado para 'normal'|
|*Grupo do host*|=<br><>|Especifica ou exclui um grupo de hosts.<br>**=** - o evento pertence a determinado grupo.<br>**<>** - o evento NÃO PERTENCE a determinado grupo.|
|*Template*|=<br><>|Especifica ou exclui templates.<br>**=** - o evento pertence a uma trigger herdada de determinado template.<br>**<>** - o evento pertence a uma trigger QUE NÃO FOI HERDADA de determinado template.|
|*Host*|=<br><>|Especifica ou exclui hosts.<br>**=** - o evento pertence a determinado host.<br>**<>** - o evento NÃO PERTENCE a determinado host.|

[comment]: # ({/new-6e7e1ac8})

[comment]: # ({new-79be7a27})
#### Service actions

The following conditions can be used in service actions:

|Condition type|Supported operators|Description|
|--|--|------|
|*Service*|equals<br>does not equal|Specify a service or a service to exclude.<br>**equals** - event belongs to this service.<br>**does not equal** - event does not belong to this service.<br>Specifying a parent service implicitly selects all child services. To specify the parent service only, all nested services have to be additionally set with the **does not equal** operator.|
|*Service name*|contains<br>does not contain|Specify a string in the service name or a string to exclude.<br>**contains** - event is generated by a service, containing this string in the name.<br>**does not contain** - this string cannot be found in the service name.|
|*Service tag name*|equals<br>does not equal<br>contains<br>does not contain|Specify an event tag or an event tag to exclude. Service event tags can be defined in the service configuration section *Tags*.<br>**equals** - event has this tag<br>**does not equal** - event does not have this tag<br>**contains** - event has a tag containing this string<br>**does not contain** - event does not have a tag containing this string.|
|*Service tag value*|equals<br>does not equal<br>contains<br>does not contain|Specify an event tag and value combination or a tag and value combination to exclude. Service event tags can be defined in the service configuration section *Tags*.<br>**equals** - event has this tag and value<br>**does not equal** - event does not have this tag and value<br>**contains** - event has a tag and value containing these strings<br>**does not contain** - event does not have a tag and value containing these strings.|

:::noteimportant
Make sure to define [message templates](/manual/config/notifications/media#overview) for Service actions in the *Alerts -> Media types* menu. Otherwise, the notifications will not be sent. 
:::

[comment]: # ({/new-79be7a27})


[comment]: # ({new-6df27208})
#### Ações inativas por objetos excluídos

Se determinado objeto (host, template, trigger, etc) é utilizado em uma
condição/operação de uma ação for excluído a ação será inativada para
evitar execução incorreta da ação. A ação poderá ser reativada pelo
usuário.

Isso ocorrerá quando excluir:

-   grupos de hosts que esteja em: condição "grupo de host", "comando
    remoto" de uma operação;
-   hosts que esteja em: condição "host, "comando remoto" de operação;
-   templates que estejam em: condição "template", operações de
    "associar a template" ou "desassociar de template" ;
-   triggers que esteja em condição de "trigger" ;
-   regras de descoberta que esteja em condição de "regra de descoberta"
    ou "verificação de descoberta";
-   proxies que esteja em condição de "proxy".

*Nota*: se um comando remoto tem como alvo vários hosts e for excluido
apenas um dos hosts o mesmo será removido da lista de alvos mas a
operação continuará ativa. Mas, se ele for o único host, a operação será
removida também. O mesmo ocorrerá com operações de associação e
desassociação com templates.

Ações não são inativadas quando se excluem usuários ou grupos de
usuários.

[comment]: # ({/new-6df27208})





[comment]: # ({new-e9b18b74})
#### Autoregistration actions

The following conditions can be set for actions based on active agent
autoregistration:

|Condition type|Supported operators|Description|
|--------------|-------------------|-----------|
|*Host metadata*|contains<br>does not contain<br>matches<br>does not match|Specify host metadata or host metadata to exclude.<br>**contains** - host metadata contains the string.<br>**does not contain** - host metadata does not contain the string.<br>Host metadata can be specified in an [agent configuration file](/manual/appendix/config/zabbix_agentd).<br>**matches** - host metadata matches regular expression.<br>**does not match** - host metadata does not match regular expression.|
|*Host name*|contains<br>does not contain<br>matches<br>does not match|Specify a host name or a host name to exclude.<br>**contains** - host name contains the string.<br>**does not contain** - host name does not contain the string.<br>**matches** - host name matches regular expression.<br>**does not match** - host name does not match regular expression.|
|*Proxy*|equals<br>does not equal|Specify a proxy or a proxy to exclude.<br>**equals** - using this proxy.<br>**does not equal** - using any other proxy except this one.|

[comment]: # ({/new-e9b18b74})

[comment]: # ({new-133d6ae1})
#### Internal event actions

The following conditions can be set for actions based on internal
events:

|Condition type|Supported operators|Description|
|--------------|-------------------|-----------|
|*Event type*|equals|**Item in "not supported" state** - matches events where an item goes from a 'normal' to 'not supported' state<br>**Low-level discovery rule in "not supported" state** - matches events where a low-level discovery rule goes from a 'normal' to 'not supported' state<br>**Trigger in "unknown" state** - matches events where a trigger goes from a 'normal' to 'unknown' state|
|*Host group*|equals<br>does not equal|Specify host groups or host groups to exclude.<br>**equals** - event belongs to this host group.<br>**does not equal** - event does not belong to this host group.|
|*Tag name*|equals<br>does not equal<br>contains<br>does not contain|Specify event tag or event tag to exclude.<br>**equals** - event has this tag<br>**does not equal** - event does not have this tag<br>**contains** - event has a tag containing this string<br>**does not contain** - event does not have a tag containing this string|
|*Tag value*|equals<br>does not equal<br>contains<br>does not contain|Specify event tag and value combination or tag and value combination to exclude.<br>**equals** - event has this tag and value<br>**does not equal** - event does not have this tag and value<br>**contains** - event has a tag and value containing these strings<br>**does not contain** - event does not have a tag and value containing these strings|
|*Template*|equals<br>does not equal|Specify templates or templates to exclude.<br>**equals** - event belongs to an item/trigger/low-level discovery rule inherited from this template.<br>**does not equal** - event does not belong to an item/trigger/low-level discovery rule inherited from this template.|
|*Host*|equals<br>does not equal|Specify hosts or hosts to exclude.<br>**equals** - event belongs to this host.<br>**does not equal** - event does not belong to this host.|

[comment]: # ({/new-133d6ae1})

[comment]: # ({new-8c003f8b})
#### Type of calculation

The following options of calculating conditions are available:

-   **And** - all conditions must be met

Note that using "And" calculation is disallowed between several triggers
when they are selected as a `Trigger=` condition. Actions can only be
executed based on the event of one trigger.

-   **Or** - enough if one condition is met
-   **And/Or** - combination of the two: AND with different condition
    types and OR with the same condition type, for example:

*Host group* equals Oracle servers\
*Host group* equals MySQL servers\
*Trigger name* contains 'Database is down'\
*Trigger name* contains 'Database is unavailable'

is evaluated as

**(**Host group equals Oracle servers **or** Host group equals MySQL
servers**)** **and** **(**Trigger name contains 'Database is down'
**or** Trigger name contains 'Database is unavailable'**)**

-   **Custom expression** - a user-defined calculation formula for
    evaluating action conditions. It must include all conditions
    (represented as uppercase letters A, B, C, ...) and may include
    spaces, tabs, brackets ( ), **and** (case sensitive), **or** (case
    sensitive), **not** (case sensitive).

While the previous example with `And/Or` would be represented as (A or
B) and (C or D), in a custom expression you may as well have multiple
other ways of calculation:

(A and B) and (C or D)\
(A and B) or (C and D)\
((A or B) and C) or D\
(not (A or B) and C) or not D\
etc.

[comment]: # ({/new-8c003f8b})

[comment]: # ({new-fca3d6de})
#### Actions disabled due to deleted objects

If a certain object (host, template, trigger, etc.) used in an action
condition/operation is deleted, the condition/operation is removed and
the action is disabled to avoid incorrect execution of the action. The
action can be re-enabled by the user.

This behavior takes place when deleting:

-   host groups ("host group" condition, "remote command" operation on a
    specific host group);
-   hosts ("host" condition, "remote command" operation on a specific
    host);
-   templates ("template" condition, "link to template" and "unlink from
    template" operations);
-   triggers ("trigger" condition);
-   discovery rules (when using "discovery rule" and "discovery check"
    conditions).

*Note*: If a remote command has many target hosts, and we delete one of
them, only this host will be removed from the target list, the operation
itself will remain. But, if it's the only host, the operation will be
removed, too. The same goes for "link to template" and "unlink from
template" operations.

Actions are not disabled when deleting a user or user group used in a
"send message" operation.

[comment]: # ({/new-fca3d6de})
