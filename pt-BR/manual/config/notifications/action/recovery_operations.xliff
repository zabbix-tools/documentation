<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="pt-BR" datatype="plaintext" original="manual/config/notifications/action/recovery_operations.md">
    <body>
      <trans-unit id="10ba847f" xml:space="preserve">
        <source># 3 Recovery operations</source>
      </trans-unit>
      <trans-unit id="e5432fa9" xml:space="preserve">
        <source>### Overview

Recovery operations allow you to be notified when problems are resolved.

Both messages and remote commands are supported in recovery operations.
While several operations can be added, escalation is not supported - all
operations are assigned to a single step and therefore will be performed
simultaneously.</source>
      </trans-unit>
      <trans-unit id="960d3247" xml:space="preserve">
        <source>### Use cases

Some use cases for recovery operations are as follows:

1.  Notify on a recovery all users that were notified on the problem:
    -  Select *Notify all involved* as operation type.

2.  Have multiple operations upon recovery: send a notification and execute a remote command:
    -  Add operation types for sending a message and executing a command.

3.  Open a ticket in external helpdesk/ticketing system and close it when the problem is resolved:
    -  Create an external script that communicates with the helpdesk system.
    -  Create an action having operation that executes this script and thus opens a ticket.
    -  Have a recovery operation that executes this script with other parameters and closes the ticket.
    -  Use the {EVENT.ID} macro to reference the original problem.</source>
      </trans-unit>
      <trans-unit id="82501aec" xml:space="preserve">
        <source>### Configuring a recovery operation

To configure a recovery operation, go to the *Operations* tab in
[action](/manual/config/notifications/action) configuration.

![](../../../../../assets/en/manual/config/notifications/action_operation.png){width="600"}

To configure details of a new recovery operation,
click on ![](../../../../../assets/en/manual/config/add_link.png) in the *Recovery operations* block.
To edit an existing operation,
click on ![](../../../../../assets/en/manual/config/edit_link.png) next to the operation.
A pop-up window will open where you can edit the operation step details.</source>
      </trans-unit>
      <trans-unit id="f2127d8b" xml:space="preserve">
        <source>#### Recovery operation details

![](../../../../../assets/en/manual/config/recovery_operation_details.png){width="600"}

Three operation types are available for recovery events:

-   **Send message** - send recovery message to specified user.
-   **Notify all involved** - send recovery message to all users who were notified on the problem event.
-   **&lt;remote command name&gt;** - execute a remote command. Commands are available for execution if previously defined in [global scripts](/manual/web_interface/frontend_sections/alerts/scripts#configuring_a_global_script) with *Action operation* selected as its scope.

Parameters for each operation type are described below. All mandatory input fields are marked with a red asterisk.
When done, click on *Add* to add operation to the list of *Recovery operations*.

::: noteclassic
Note that if the same recipient is defined in several operation types without specified *Custom message*,
duplicate notifications are not sent.
:::</source>
      </trans-unit>
      <trans-unit id="7dbd8d4d" xml:space="preserve">
        <source>
#### Operation type: [send message](/manual/config/notifications/action/operation/message)

|Parameter|&lt;|Description|
|-|--|---------------------------|
|*Send to user groups*|&lt;|Select user groups to send the recovery message to.&lt;br&gt;The user group must have at least "read" [permissions](/manual/config/users_and_usergroups/permissions) to the host in order to be notified.|
|*Send to users*|&lt;|Select users to send the recovery message to.&lt;br&gt;The user must have at least "read" [permissions](/manual/config/users_and_usergroups/permissions) to the host in order to be notified.|
|*Send only to*|&lt;|Send default recovery message to all defined media types or a selected one only.|
|*Custom message*|&lt;|If selected, a custom message can be defined.|
| |*Subject*|Subject of the custom message. The subject may contain macros.|
|^|*Message*|The custom message. The message may contain macros.|</source>
      </trans-unit>
      <trans-unit id="37fc532b" xml:space="preserve">
        <source>
#### Operation type: [remote command](/manual/config/notifications/action/operation/remote_command)

|Parameter|Description|
|--|--------|
|*Target list*|Select targets to execute the command on:&lt;br&gt;**Current host** - command is executed on the host of the trigger that caused the problem event. This option will not work if there are multiple hosts in the trigger.&lt;br&gt;**Host** - select host(s) to execute the command on.&lt;br&gt;**Host group** - select host group(s) to execute the command on. Specifying a parent host group implicitly selects all nested host groups. Thus the remote command will also be executed on hosts from nested groups.&lt;br&gt;A command on a host is executed only once, even if the host matches more than once (e.g. from several host groups; individually and from a host group).&lt;br&gt;The target list is meaningless if the command is executed on Zabbix server. Selecting more targets in this case only results in the command being executed on the server more times.&lt;br&gt;Note that for global scripts, the target selection also depends on the *Host group* setting in global script [configuration](/manual/web_interface/frontend_sections/alerts/scripts#configuring_a_global_script).|</source>
      </trans-unit>
      <trans-unit id="945f9a49" xml:space="preserve">
        <source>
#### Operation type: notify all involved

|Parameter|&lt;|Description|
|-|--|-----------------|
|*Custom message*|&lt;|If selected, a custom message can be defined.|
| |*Subject*|Subject of the custom message. The subject may contain macros.|
|^|*Message*|The custom message. The message may contain macros.|</source>
      </trans-unit>
    </body>
  </file>
</xliff>
