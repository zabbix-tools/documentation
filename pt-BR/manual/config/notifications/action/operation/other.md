[comment]: # translation:outdated

[comment]: # ({new-ec342157})
# 3 Operações adicionais

[comment]: # ({/new-ec342157})

[comment]: # ({new-748f7486})
#### Visão geral

Adicionalmente, para eventos de descoberta, existem as seguintes
operações:

-   adicionar host
-   remover host
-   habilitar host
-   desabilitar host
-   adicionar a grupo
-   remover do grupo
-   associar a template
-   desassociar a template
-   definir o modo de inventário

Adicionalmente, para eventos de autorregistro, existem as seguintes
operações:

-   adicionar host
-   desabilitar host
-   adicionar a grupo
-   associar a template
-   definir o modo de inventário

[comment]: # ({/new-748f7486})

[comment]: # ({new-903c037b})
##### Adicionar host

Os hosts são adicionados **durante** o processo de descoberta de rede,
não é necessário aguardar o final do processo de descoberta (a varredura
de todo o range de IPs e todos os testes) para que o mesmo seja
adicionado.

::: notetip
As descobertas de rede podem demorar muito por conta do
tamanho do range de IPs a ser varrido e da quantidade de hosts /
serviços indisponíveis, paciência é recomendável assim como a redução do
tamanho dos ranges.
:::

Quando um host é adicionado o seu nome é definido pela função padrão
**gethostbyname**. Se o nome do host puder ser resolvido, ele será
utilizado, se não puder o seu endereço IP será usado. Se um endereço
IPV6 precisar ser utilizado como nome do host todos os caracteres ":"
(dois pontos) serão substituídos por "\_" (underscores), pois os dois
pontos não são permitidos nos nomes de hosts.

::: noteimportant
Se a descoberta for executada por um proxy a
resolução de nomes continuará sendo feita pelo Zabbix
Server.
:::

::: noteimportant
Se um host já existir na configuração do Zabbix
com o mesmo nome do host recém descoberto será adicionado um contado no
padrão **\_N** ao nome do host, onde **N** é um número crescente
iniciando em 2.
:::

[comment]: # ({/new-903c037b})
