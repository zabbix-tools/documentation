[comment]: # translation:outdated

[comment]: # ({new-0cdc5e2e})
# 2 Comandos remotos

[comment]: # ({/new-0cdc5e2e})

[comment]: # ({new-36945095})
#### Visão geral

Com os comandos remotos você pode definir um comando a ser executado
automaticamente sob determinada condição.

Desta maneira, os comandos remotos são um mecanismo eficiente para
monitoração pró-ativa.

As situações mais comuns onde se utiliza este recurso são:

-   Reiniciar automaticamente alguma aplicação (servidor web,
    aplicações, CRM) se ele não estiver respondendo
-   Utilizar o comando IPMI de 'reboot' para reiniciar remotamente um
    servidor que não responde
-   Liberar automaticamente espaço em disco (removendo arquivos antigos,
    limpando o /tmp) se o espaço estiver acabando
-   Migrar uma VM para outro servidor físico dependendo do esgotamento
    de CPU do servidor físico atual
-   Adicionar novos nodes em um ambiente de nuvem que estiver com seus
    recursos esgotando (armazenamento, memória, CPU, etc)

Configurar uma ação para comandos remotos é muito similar a enviar uma
mensagem, a única diferença é que o Zabbix irá executar o comando ao
invés de enviar a mensagem.

::: noteimportant
Os comandos remotos não são suportados através de
Proxy, pois o servidor Zabbix se conecta diretamente ao agente para
solicitar a execução remota.
:::

O comando remoto é limitado a 255 caracteres. Múltiplos comandos podem
ser executados, bastando que sejam separados por novas linhas. É
permitido o uso de macros. Consulte o manual de [execução de
comandos](/pt/manual/appendix/command_execution) para maiores detalhes.

O tutorial a seguir apresenta como um comando remoto poderia ser
configurado.

[comment]: # ({/new-36945095})

[comment]: # ({new-7ca4c9c3})
#### Configuração

Para os comandos remotos serem executados pelo Zabbix Agent (scripts
personalizados) o mesmo deverá ter seu [arquivo de
configuração](/pt/manual/appendix/config/zabbix_agentd) devidamente
configurado para permitir isso.

Certifique-se que o parâmetro **EnableRemoteCommands** está definido
como **1** e sua linha não está comentada. Reinicie o agente após
modificar este parâmetro.

::: noteimportant
Comandos remotos não funcionam através de agentes
ativos.
:::

Para configurar uma nova ação:

-   Acesse *Configuração→ Ações*
-   Na aba de *Operações*, selecione o tipo **Comando remoto**
-   Selecione o tipo de comando remoto (IPMI, Custom script, SSH,
    Telnet, Script global)
-   Informe o comando remoto

Por exemplo:

    sudo /etc/init.d/apache restart 

Neste caso o Zabbix Agent tentara reiniciar o processo do apache.
Certifique-se que a opção *Executar em* esteja definida como *Agente
Zabbix*.

::: noteimportant
O comando exemplificado utiliza-se do **sudo** e o
usuário 'zabbix' por padrão não possui permissionamento para reiniciar
serviços. Veja a seguir algumas dicas sobre configuração do
**sudo**.
:::

::: noteclassic
O Zabbix Agent precisa estar em execução no host remoto e
aceitar conexões de entrada (monitoração passiva). A execução dos
comando será feita pelo agente em segundo plano.
:::

::: noteimportant
O Zabbix não verifica se o comando foi ou não
executado com sucesso.
:::

Comandos remotos no Zabbix Agent são executados no agente sem limite de
tempo ('system.run\[,nowait\]'). No Zabbix Server eles serão executados
com limite de tempo definido pelo parâmetro **TrapperTimeout** presente
no [arquivo de configuração do
servidor](/pt/manual/appendix/config/zabbix_server).

-   Na aba de *Condições*, defina as condições apropriadas. Neste
    exemplo, a ação será executada somente quando ocorrerem eventos com
    severidade em nível de **Desastre** e estiverem associados à
    aplicação **Apache**.

![](../../../../../../assets/en/manual/config/conditions_restart.png)

[comment]: # ({/new-7ca4c9c3})

[comment]: # ({new-349859a6})
#### Permissão de acesso

Certifique-se que o usuário 'zabbix' possua permissões de execução nos
comandos configurados. Uma forma interessante de garantir isso é através
do comando **sudo** para os comandos privilegiados. Para configurar,
execute como o usuário 'root':

    # visudo

Exemplos de linhas que podem ser utilizadas no arquivo *sudoers*:

    # permite o usuário 'zabbix' executar sem precisar de senha.
    zabbix ALL=NOPASSWD: ALL

    # permite que o usuário 'zabbix' reinicie o apache sem precisar de senha.
    zabbix ALL=NOPASSWD: /etc/init.d/apache restart

::: notetip
Em alguns ambientes o arquvio *sudoers* irá proibir que
usuários remotos executem comandos. Para modificar isso, remova o
comentário da opção **requiretty** no *sudoers*.
:::

[comment]: # ({/new-349859a6})

[comment]: # ({new-c5296379})
#### Comandos remotos e múltiplas interfaces

Se o sistema de destino possuir múltiplas interfaces de um mesmo tipo
(Zabbix Agent ou IPMI), os comandos remotos serão executados apenas
através da interface padrão.

[comment]: # ({/new-c5296379})

[comment]: # ({new-a1b1e7ea})
#### Exemplos

[comment]: # ({/new-a1b1e7ea})

[comment]: # ({new-1f3becef})
##### Exemplo 1

Reiniciar o Windows em determinada condição.

Para reiniciar automaticamente o windows quando problemas forem
detectados pelo Zabbix, defina a operação a seguir:

|Parâmetro|Descrição|
|----------|-----------|
|Tipo da operação|'Remote command'|
|Tipo|'Script personalizado'|
|Comando|c:\\windows\\system32\\shutdown.exe -r -f|

[comment]: # ({/new-1f3becef})

[comment]: # ({new-26b199db})
##### Exemplo 2

Reiniciar o host através do controle IPMI.

|Parâmetro|Descrição|
|----------|-----------|
|Tipo da operação|'Remote command'|
|Tipo|'IPMI'|
|Comando|reset on|

[comment]: # ({/new-26b199db})

[comment]: # ({new-c230845b})
##### Exemplo 3

Desligar o host através do controle IPMI.

|Parâmetro|Descrição|
|----------|-----------|
|Tipo da operação|'Remote command'|
|Tipo|'IPMI'|
|Comando|power off|

[comment]: # ({/new-c230845b})




[comment]: # ({new-1d68d18d})
##### Example 1

Restart of Windows on certain condition.

In order to automatically restart Windows upon a problem detected by
Zabbix, define the following script:

|Script parameter|Value|
|----------------|-----|
|*Scope*|'Action operation'|
|*Type*|'Script'|
|*Command*|c:\\windows\\system32\\shutdown.exe -r -f|

[comment]: # ({/new-1d68d18d})

[comment]: # ({new-8e3d9a17})
##### Example 2

Restart the host by using IPMI control.

|Script parameter|Value|
|----------------|-----|
|*Scope*|'Action operation'|
|*Type*|'IPMI'|
|*Command*|reset|

[comment]: # ({/new-8e3d9a17})

[comment]: # ({new-c9d4e7c6})
##### Example 3

Power off the host by using IPMI control.

|Script parameter|Value|
|----------------|-----|
|*Scope*|'Action operation'|
|*Type*|'IPMI'|
|*Command*|power off|

[comment]: # ({/new-c9d4e7c6})
