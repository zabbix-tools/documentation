[comment]: # translation:outdated

[comment]: # ({new-66eca2c9})
# 1 Enviando mensagem

[comment]: # ({/new-66eca2c9})

[comment]: # ({new-a1d73332})
#### Visão geral

O envio de mensagem é uma das melhores formas de notificar os usuários
sobre os incidentes. É por isso que ele é uma das ações primárias do
Zabbix.

[comment]: # ({/new-a1d73332})

[comment]: # ({new-2540ef35})
#### Configuração

Para conseguir enviar e receber notificações no Zabbix você precisa:

-   [definir uma mídia](/pt/manual/config/notifications/media) pela qual
    a mensagem será enviada
-   [configurar uma ação com
    operação](/pt/manual/config/notifications/action/operation) que irá
    enviar a mensagem para um ou mais tipos de mídia definidos

::: noteimportant
O Zabbix envia notificações apenas para os
usuários que possuem, no mínimo, permissão de 'leitura' no host que
gerou o evento. Para os eventos iniciados por ocorrências em triggers de
maior complexidade que envolvam vários hosts o requisito é que o usuário
possua permissão de leitura em pelo menos um dos hosts envolvidos na
expressão da trigger.
:::

Você pode configurar cenários customizados para o envio de mensagens
através do recurso de
[escalonamento](/pt/manual/config/notifications/action/escalations).

Para receber e visualizar corretamente e-mails enviados pelo Zabbix o
servidor de e-mail e o cliente de e-mail deverá suportar o formato
padrão de 'SMTP/MIME' para e-mail com a codificação UTF-8 (Se o assunto
contiver somente caracteres ASCII, poderá não ser codificado como
UTF-8.). O assunto e o corpo da mensagem são codificados usando 'base64'
para aderir ao padrão 'SMTP/MIME e-mail'.

[comment]: # ({/new-2540ef35})

[comment]: # ({new-c0442483})
#### Rastreando mensagens

Você pode visualizar o status das mensagens enviadas em *Monitoramento →
Eventos*.

Na coluna *Ações* você pode ver a informação sumarizada sobre as ações
executadas. Números na cor verde indicam mensagens enviadas com sucesso,
cor vermelha - mensagens que falharam. O status *Em progresso* indica
que o processo de ação foi iniciado. *Falhou* informa que nenhuma ação
foi executada com sucesso.

Se você clicar no horário do evento para visualizar os seus detalhes,
você poderá ver o bloco de *Ações da mensagem* que conterá detalhes das
mensagens (enviadas ou não) durante o evento.

Em *Relatórios → Log de ações*, você poderá ver o resumo das mensagens
enviadas para todos os usuários ou para usuários específicos.

[comment]: # ({/new-c0442483})
