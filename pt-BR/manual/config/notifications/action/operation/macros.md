[comment]: # translation:outdated

[comment]: # ({new-b1624dc4})
# 4 Usando macros em mensagens

[comment]: # ({/new-b1624dc4})

[comment]: # ({new-72998395})
#### Visão geral

As macros podem ser usadas de forma eficiente tanto no assunto quanto no
corpo da mensagem para melhorar a qualidade da informação fornecida
quando se alerta sobre um incidente.

Existe manual com a [lista completa de
macros](/pt/manual/appendix/macros/supported_by_location) que são
suportadas pelo Zabbix.

[comment]: # ({/new-72998395})

[comment]: # ({new-e5ad63df})
#### Exemplos

Os exemplos a seguir ilustram como as macros podem ser utilizadas nas
mensagens.

[comment]: # ({/new-e5ad63df})

[comment]: # ({new-102863de})
##### Exemplo 1

Assunto da mensagem:

    {TRIGGER.NAME}: {TRIGGER.STATUS}

Quando você receber a mensagem, o assunto da mensagem será substituído
por algo similar à:

    Processor load is too high on server zabbix.zabbix.com: INCIDENTE

[comment]: # ({/new-102863de})

[comment]: # ({new-1344257c})
##### Exemplo 2

Mensagem:

    Processor load is: {zabbix.zabbix.com:system.cpu.load[,avg1].last()}

Quando você receber a mensagem, o corpo da mensagem será substituído por
algo similar à:

    Processor load is: 1.45

[comment]: # ({/new-1344257c})

[comment]: # ({new-3713b8a5})
##### Exemplo 3

Mensagem:

    Latest value: {{HOST.HOST}:{ITEM.KEY}.last()}
    MAX for 15 minutes: {{HOST.HOST}:{ITEM.KEY}.max(900)}
    MIN for 15 minutes: {{HOST.HOST}:{ITEM.KEY}.min(900)}

Quando você receber a mensagem, o corpo da mensagem será substituído por
algo similar à:

    Latest value: 1.45
    MAX for 15 minutes: 2.33
    MIN for 15 minutes: 1.01

[comment]: # ({/new-3713b8a5})

[comment]: # ({new-88afe91c})
##### Exemplo 4

Informando sobre valores de diversos hosts em uma mesma expressão de
trigger.

Mensagem:

    Trigger: {TRIGGER.NAME}
    Trigger expression: {TRIGGER.EXPRESSION}
     
    1. Item value on {HOST.NAME1}: {ITEM.VALUE1} ({ITEM.NAME1})
    2. Item value on {HOST.NAME2}: {ITEM.VALUE2} ({ITEM.NAME2})

Quando você receber a mensagem, o corpo da mensagem será substituído por
algo similar à:

    Trigger: Processor load is too high on a local host
    Trigger expression: {Myhost:system.cpu.load[percpu,avg1].last()}>5 | {Myotherhost:system.cpu.load[percpu,avg1].last()}>5

    1. Item value on Myhost: 0.83 (Processor load (1 min average per core))
    2. Item value on Myotherhost: 5.125 (Processor load (1 min average per core))

[comment]: # ({/new-88afe91c})

[comment]: # ({new-d41797c6})
##### Exemplo 5

Recebendo detalhes tanto do evento de problema quanto do evento de
[recuperação](/pt/manual/config/notifications/action#configuring_an_action):

Mensagem:

    Problem:

    Event ID: {EVENT.ID}
    Event value: {EVENT.VALUE} 
    Event status: {EVENT.STATUS} 
    Event time: {EVENT.TIME}
    Event date: {EVENT.DATE}
    Event age: {EVENT.AGE}
    Event acknowledgement: {EVENT.ACK.STATUS} 
    Event acknowledgement history: {EVENT.ACK.HISTORY}

    Recovery: 

    Event ID: {EVENT.RECOVERY.ID}
    Event value: {EVENT.RECOVERY.VALUE} 
    Event status: {EVENT.RECOVERY.STATUS} 
    Event time: {EVENT.RECOVERY.TIME}
    Event date: {EVENT.RECOVERY.DATE}

Quando você receber a mensagem as macros serão substituídas por algo
similar à:

    Problem:

    Event ID: 21874
    Event value: 1 
    Event status: PROBLEM 
    Event time: 13:04:30
    Event date: 2014.01.02
    Event age: 5m
    Event acknowledgement: Yes 
    Event acknowledgement history: 2014.01.02 13:05:51 "John Smith (Admin)"
    -acknowledged-

    Recovery: 

    Event ID: 21896
    Event value: 0 
    Event status: OK 
    Event time: 13:10:07
    Event date: 2014.01.02

::: noteimportant
As notificações em separado dos problemas e
recuperações são suportadas desde o Zabbix 2.2.0.
:::

[comment]: # ({/new-d41797c6})


[comment]: # ({new-0ee2664c})
##### Example 6

Receiving details of both the problem event and recovery event in a
[recovery](/manual/config/notifications/action/recovery_operations)
message:

Message:

    Problem:

    Event ID: {EVENT.ID}
    Event value: {EVENT.VALUE} 
    Event status: {EVENT.STATUS} 
    Event time: {EVENT.TIME}
    Event date: {EVENT.DATE}
    Event age: {EVENT.AGE}
    Event acknowledgment: {EVENT.ACK.STATUS} 
    Event update history: {EVENT.UPDATE.HISTORY}

    Recovery: 

    Event ID: {EVENT.RECOVERY.ID}
    Event value: {EVENT.RECOVERY.VALUE} 
    Event status: {EVENT.RECOVERY.STATUS} 
    Event time: {EVENT.RECOVERY.TIME}
    Event date: {EVENT.RECOVERY.DATE}
    Operational data: {EVENT.OPDATA}

When you receive the message, the macros will be replaced by something
like:

    Problem:

    Event ID: 21874
    Event value: 1 
    Event status: PROBLEM 
    Event time: 13:04:30
    Event date: 2018.01.02
    Event age: 5m
    Event acknowledgment: Yes 
    Event update history: 2018.01.02 13:05:51 "John Smith (Admin)"
    Actions: acknowledged.

    Recovery: 

    Event ID: 21896
    Event value: 0 
    Event status: OK 
    Event time: 13:10:07
    Event date: 2018.01.02
    Operational data: Current value is 0.83

::: noteimportant
Separate notification macros for the original
problem event and recovery event are supported since Zabbix
2.2.0.
:::

[comment]: # ({/new-0ee2664c})
