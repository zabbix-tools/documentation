<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="pt-BR" datatype="plaintext" original="devel/modules/widgets/configuration.md">
    <body>
      <trans-unit id="f7477fb0" xml:space="preserve">
        <source># Configuration

This page describes classes that can be used to create a widget configuration view with custom configuration fields.
The widget configuration view is the part of the widget that allows the user to configure widget parameters for [presentation](/presentation).</source>
      </trans-unit>
      <trans-unit id="adf59b13" xml:space="preserve">
        <source>### Widget

Primary widget class, extends the base class of all dashboard widgets - *CWidget*.
Required for overriding the default widget behavior.

The *Widget* class should be located in the root directory of the widget (e.g., *ui/modules/my_custom_widget*).

**Widget.php example**

```php
&lt;?php

namespace Modules\MyCustomWidget;

use Zabbix\Core\CWidget;

class Widget extends CWidget {

    public const MY_CONSTANT = 0;

    public function getTranslationStrings(): array {
        return [
            'class.widget.js' =&gt; [
                'No data' =&gt; _('No data')
            ]
        ];
    }
}
```</source>
      </trans-unit>
      <trans-unit id="1ff26877" xml:space="preserve">
        <source>### WidgetForm

The *WidgetForm* class extends the default class *CWidgetForm* and contains a set of [*CWidgetField*](#cwidgetfield) fields
that are required for defining widget configuration storage structure in the database and handling input validation.

The *WidgetForm* class should be located in the *includes* directory.
If the class has a different name, the name should be specified in the [*widget/form_class*](/devel/modules/file_structure/manifest#widget) parameter in the *manifest.json* file.

**includes/WidgetForm.php example**

```php
&lt;?php

namespace Modules\MyCustomWidget\Includes;

use Modules\MyCustomWidget\Widget;

use Zabbix\Widgets\{
    CWidgetField,
    CWidgetForm
};

use Zabbix\Widgets\Fields\{
    CWidgetFieldMultiSelectItem,
    CWidgetFieldTextBox,
    CWidgetFieldColor
};


class WidgetForm extends CWidgetForm {

    public function addFields(): self {
        return $this
            -&gt;addField(
                (new CWidgetFieldMultiSelectItem('itemid', _('Item')))
                    -&gt;setFlags(CWidgetField::FLAG_NOT_EMPTY | CWidgetField::FLAG_LABEL_ASTERISK)
                    -&gt;setMultiple(false)
            )
            -&gt;addField(
                new CWidgetFieldTextBox('description', _('Description'))
            )
            -&gt;addField(
                (new CWidgetFieldColor('chart_color', _('Color')))-&gt;setDefault('FF0000')
            );
    }
}
```</source>
      </trans-unit>
      <trans-unit id="61f06fc3" xml:space="preserve">
        <source>### CWidgetFormView

The *CWidgetFormView* class is required for specifying the presentation logic of the fields defined in the *WidgetForm* class,
determining their appearance and behavior when rendered in the configuration view.

The *CWidgetFormView* class supports the following methods:

-   *addField()* - receives an instance of the *CWidgetFieldView* class as a parameter;
    each [*CWidgetField*](#cwidgetfield) class, has a respective *CWidgetFieldView* class for using in the widget configuration view.
-   *includeJsFile()* - allows to add a JavaScript file to the widget configuration view.
-   *addJavaScript()* - allows to add inline JavaScript that will be executed as soon as the widget configuration view is loaded.

The *CWidgetFormView* class should be located in the *views* directory.

**views/widget.edit.php example**

```php
&lt;?php

/**
 * My custom widget form view.
 *
 * @var CView $this
 * @var array $data
 */

use Zabbix\Widgets\Fields\CWidgetFieldGraphDataSet;

(new CWidgetFormView($data))
    -&gt;addField(
        (new CWidgetFieldMultiSelectItemView($data['fields']['itemid'], $data['captions']['items']['itemid']))
            -&gt;setFilterParameter('numeric', true)
    )
    -&gt;addField(
        new CWidgetFieldTextBoxView($data['fields']['description']),
        'js-advanced-configuration'
    )
    -&gt;addField(
        new CWidgetFieldColorView($data['fields']['chart_color'])
    )
    -&gt;includeJsFile('widget.edit.js.php')
    -&gt;addJavaScript('my_custom_widget_form.init('.json_encode([
        'color_palette' =&gt; CWidgetFieldGraphDataSet::DEFAULT_COLOR_PALETTE
    ], JSON_THROW_ON_ERROR).');')
    -&gt;show();
```</source>
      </trans-unit>
      <trans-unit id="d1c62429" xml:space="preserve">
        <source>### JavaScript

A JavaScript class can be used to add dynamic behavior and interactivity to the widget configuration view.
For example, you can initialize a color picker, defined in the [*CWidgetFormView*](#cwidgetformview) class.

The JavaScript class should be loaded with the form, therefore it should be referenced in the [*CWidgetFormView*](#cwidgetformview) class by using the methods *includeJsFile()* and *addJavaScript()*.

In the example below, a singleton class instance is immediately created and stored under the *window.my_custom_widget_form* name.
Thus, opening the form for the second time will re-create the instance.

The JavaScript class should be located in the *views* directory.

**views/widget.edit.js.php example**

```php
&lt;?php

use Modules\MyCustomWidget\Widget;

?&gt;

window.my_custom_widget_form = new class {

    init({color_palette}) {
        colorPalette.setThemeColors(color_palette);

        for (const colorpicker of jQuery('.&lt;?= ZBX_STYLE_COLOR_PICKER ?&gt; input')) {
            jQuery(colorpicker).colorpicker();
        }

        const overlay = overlays_stack.getById('widget_properties');

        for (const event of ['overlay.reload', 'overlay.close']) {
            overlay.$dialogue[0].addEventListener(event, () =&gt; { jQuery.colorpicker('hide'); });
        }
    }
};
```</source>
      </trans-unit>
      <trans-unit id="708ecb6b" xml:space="preserve">
        <source>### CWidgetField

The *CWidgetField* class is a base class from which all form field classes (*CWidgetFieldCheckBox*, *CWidgetFieldTextArea*, *CWidgetFieldRadioButtonList*, etc.) are inherited.
Classes extending *CWidgetField* are responsible for receiving, saving, and validating widget configuration values.

The following *CWidgetField* classes are available.

|CWidgetField class|Database field type|Description|
|----|--|------------|
|*CWidgetFieldCheckBox*|int32|Single checkbox.|
|*CWidgetFieldCheckBoxList*|array of int32|Multiple checkboxes under a single configuration field.|
|*CWidgetFieldColor*|string|Color selection field.|
|*CWidgetFieldDatePicker*|string|Date selection field.|
|*CWidgetFieldHostPatternSelect*|string|Multiselect field that allows to select one or multiple hosts. Supports defining host name patterns (all matching hosts will be selected).|
|*CWidgetFieldIntegerBox*|int32|Field to enter an integer. Can be used to configure minimum and maximum values.|
|*CWidgetFieldLatLng*|string|Text box that allows to enter comma-separated latitude, longitude, and map zoom level.|
|*CWidgetFieldMultiSelectAction*|ID|Multiselect field for selecting actions (from the list of actions defined in the *Alerts → Actions*).|
|*CWidgetFieldMultiSelectGraph*|ID|Multiselect field for selecting custom graphs.|
|*CWidgetFieldMultiSelectGraphPrototype*|ID|Multiselect field for selecting custom graph prototypes.|
|*CWidgetFieldMultiSelectGroup*|ID|Multiselect field for selecting host groups.|
|*CWidgetFieldMultiSelectHost*|ID|Multiselect field for selecting hosts.|
|*CWidgetFieldMultiSelectItem*|ID|Multiselect field for selecting items.|
|*CWidgetFieldMultiSelectItemPrototype*|ID|Multiselect field for selecting item prototypes.|
|*CWidgetFieldMultiSelectMediaType*|ID|Multiselect field for selecting media types.|
|*CWidgetFieldMultiSelectService*|ID|Multiselect field for selecting services.|
|*CWidgetFieldMultiSelectSla*|ID|Multiselect field for selecting SLAs.|
|*CWidgetFieldMultiSelectUser*|ID|Multiselect field for selecting users.|
|*CWidgetFieldNumericBox*|string|Field to enter a float number.|
|*CWidgetFieldRadioButtonList*|int32|Radio box group that consists of one or more radio boxes.|
|*CWidgetFieldRangeControl*|int32|Slider to select an integer type value.|
|*CWidgetFieldSelect*|int32|Dropdown select box.|
|*CWidgetFieldSeverities*|array of int32|*CWidgetFieldCheckBoxList* preset with trigger severities.|
|*CWidgetFieldTags*|array of (string, int32, string)|Allows to configure one or more tag filter rows.|
|*CWidgetFieldTextArea*|string|Text area for entering multi-line text.|
|*CWidgetFieldTextBox*|string|Text box for entering single-line text.|
|*CWidgetFieldTimeZone*|string|Dropdown with timezones.|
|*CWidgetFieldUrl*|string|Text box that allows to enter URLs.|

The following *CWidgetField* classes have been created for particular widgets.
These classes have very specific use cases, but they can also be reused if needed.

|CWidgetField class|Database field type|Description|
|----|--|------------|
|*CWidgetFieldColumnsList*|array of (multiple mixed)|For *Top hosts* widget. Create a table with custom columns of allowed types.|
|*CWidgetFieldGraphDataSet*|array of (multiple mixed)|For *Graph* widget. Setup dataset configuration and all related options.|
|*CWidgetFieldGraphOverride*|array of (multiple mixed)|For *Graph* widget. Setup overrides for specific hosts/items. Any dataset configuration can be overridden.|
|*CWidgetFieldNavTree*|string|For *Map navigation tree* widget. Replaces widget view in edit mode with the map selection tree.|
|*CWidgetFieldReference*|string|For *Map navigation tree* widget. Creates a unique identifier for this widget on dashboard. It is used to reference this widget from other widgets.|
|*CWidgetFieldSelectResource*|ID|For *Map* widget. Allows to select Zabbix network map.|
|*CWidgetFieldThresholds*|array of (string, string)|For *Top hosts* widget. Allows to configure color and number pairs.|
|*CWidgetFieldWidgetSelect*|string|For *Map* widget. Allows to select a map navigation tree from the current dashboard. Must be used in combination with *CWidgetFieldReference* in the *Map navigation tree* widget.|</source>
      </trans-unit>
    </body>
  </file>
</xliff>
