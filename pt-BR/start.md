[comment]: # translation:outdated

[comment]: # ({7274a972-63820343})
# Documentação Zabbix

Estas páginas contêm a documentação oficial do Zabbix.

Use a barra lateral para navegar pelas páginas de documentação.

Para estar apto a acompanhar páginas, faça login com seu
usuário e senha dos [fóruns Zabbix](http://www.zabbix.com/forum/).

[comment]: # ({/7274a972-63820343})

