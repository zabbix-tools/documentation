[comment]: # translation:outdated

[comment]: # ({new-e0cabf53})
# zabbix\_proxy

Seção: comandos de manutenção (8)\
Atualizado: 10 de novembro de 2011\
[Índice](#index) [Retornar ao conteúdo
principal](/documentation/2.4/pt/manpages)

------------------------------------------------------------------------

[ ]{#lbAB}

[comment]: # ({/new-e0cabf53})

[comment]: # ({new-c7ba44c5})
## NOME

zabbix\_proxy - daemon do proxy Zabbix. [ ]{#lbAC}

[comment]: # ({/new-c7ba44c5})

[comment]: # ({new-6590818d})
## SINOPSE

**zabbix\_proxy \[-hV\] \[-c** *<arquivo-de-configuração>***\]
\[-R** *<opção>***\]** [ ]{#lbAD}

[comment]: # ({/new-6590818d})

[comment]: # ({new-5c6b9587})
## DESCRIÇÃO

**zabbix\_proxy** é um daemon utilizado para coleta de dados remotos.
[ ]{#lbAE}

[comment]: # ({/new-5c6b9587})

[comment]: # ({new-daee5c55})
### Opções

-c, --config *<arquivo-de-configuração>*  
Usa o *arquivo-de-configuração* alternativo ao invés do padrão. O
caminho absoluto deve ser especificado.

-R, --runtime-control *<opção>*  
Realiza funções administrativas de acordo com a *opção*.

[ ]{#lbAF}

[comment]: # ({/new-daee5c55})

[comment]: # ({new-045894c6})
### 

  
Opções de controle em tempo de execução

  
config\_cache\_reload  
Recarrega o cache de configurações. Ignorado se o cache está sendo
atualmente carregado. Um proxy Zabbix ativo vai conectar-se ao Zabbix
server e requisitar dados de configuração. O arquivo de configuração
padrão será usado (exceto se opção **-c** for especificada) para
encontrar o arquivo com o PID, sendo o sinal enviado para o processo
listado no arquivo.

  
log\_level\_increase\[=<alvo>\]  
Aumenta o nível de log, afetando todos os processos se alvo não for
especificado

  
log\_level\_decrease\[=<alvo>\]  
Diminui o nível de log, afetando todos os processos se alvo não for
especificado

[ ]{#lbAG}

[comment]: # ({/new-045894c6})

[comment]: # ({new-b863520e})
### 

  
Alvos do controle de nível de log

  
<pid>  
Identificador de processo

  
<tipo do processo>  
Todos os processos do tipo especificado (p.ex., poller)

  
<tipo do processo>,N  
Tipo de processo e número (p.ex., poller,3)

```{=html}
<!-- -->
```
-h, --help  
Apresenta esta ajuda e sai.

-V, --version  
Apresenta informações sobre a versão e sai.

[ ]{#lbAH}

[comment]: # ({/new-b863520e})

[comment]: # ({new-583a1725})
## ARQUIVOS

*/usr/local/etc/zabbix\_proxy.conf*  
Local padrão para o arquivo de configuração do proxy Zabbix (se não
modificado em tempo de compilação).

[ ]{#lbAI}

[comment]: # ({/new-583a1725})

[comment]: # ({new-41feb9bb})
## VEJA TAMBÉM

**[zabbix\_agentd](zabbix_agentd)**(8),
**[zabbix\_get](zabbix_get)**(8),
**[zabbix\_sender](zabbix_sender)**(8),
**[zabbix\_server](zabbix_server)**(8) [ ]{#lbAJ}

[comment]: # ({/new-41feb9bb})

[comment]: # ({new-e55ed07e})
## AUTOR

Alexei Vladishev <<alex@zabbix.com>>

------------------------------------------------------------------------

[ ]{#index}

[comment]: # ({/new-e55ed07e})

[comment]: # ({new-9f3a9d2a})
## Índice

[NOME](#lbAB)

[SINOPSE](#lbAC)

[DESCRIÇÃO](#lbAD)

[Opções](#lbAE)

[](#lbAF)

[](#lbAG)

  

[ARQUIVOS](#lbAH)

[VEJA TAMBÉM](#lbAI)

[AUTOR](#lbAJ)

------------------------------------------------------------------------

Este documento foi criado pelo man2html, usando as páginas de manual.\
Horário: 23:35:33 GMT, 15 de setembro de 2014

[comment]: # ({/new-9f3a9d2a})
