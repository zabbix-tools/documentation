[comment]: # translation:outdated

[comment]: # ({new-fdbd84bb})
# zabbix\_server

Seção: comandos de manutenção (8)\
Atualizado: 10 de novembro de 2011\
[Índice](#index) [Retornar ao conteúdo
principal](/documentation/2.4/pt/manpages)

------------------------------------------------------------------------

[ ]{#lbAB}

[comment]: # ({/new-fdbd84bb})

[comment]: # ({new-1090a00e})
## NOME

zabbix\_server - daemon do servidor Zabbix [ ]{#lbAC}

[comment]: # ({/new-1090a00e})

[comment]: # ({new-2a5507db})
## SINOPSE

**zabbix\_server \[-hV\] \[-c** *<arquivo-de-configuração>*\] \[-R
*<opção>*\] [ ]{#lbAD}

[comment]: # ({/new-2a5507db})

[comment]: # ({new-f39aa640})
## DESCRIÇÃO

**zabbix\_server** é o principal daemon do software Zabbix. [ ]{#lbAE}

[comment]: # ({/new-f39aa640})

[comment]: # ({new-455125db})
### Opções

-c, --config *<arquivo-de-configuração>*  
Usa o *arquivo-de-configuração* alternativo ao invés do padrão. É
necessário especificar o caminho absoluto.

-R, --runtime-control *<opção>*  
Realiza ações administrativas de acordo com a *opção*

[ ]{#lbAF}

[comment]: # ({/new-455125db})

[comment]: # ({new-d7d7f728})
### 

  
Opções de controle em tempo de execução

  
config\_cache\_reload  
Recarrega o cache de configurações. Ignorado se o cache está sendo
atualmente carregado. O arquivo de configuração padrão será usado
(exceto se opção **-c** for especificada) para encontrar o arquivo com o
PID, sendo o sinal enviado para o processo listado no arquivo.

  
log\_level\_increase\[=<alvo>\]  
Aumenta o nível de log, afetando todos os processos se alvo não for
especificado

  
log\_level\_decrease\[=<alvo>\]  
Diminui o nível de log, afetando todos os processos se alvo não for
especificado

[ ]{#lbAG}

[comment]: # ({/new-d7d7f728})

[comment]: # ({new-800a1525})

**ha\_remove\_node**\[=*target*\]  
Remove the high availability (HA) node specified by its name or ID.
Note that active/standby nodes cannot be removed.

**ha\_set\_failover\_delay**\[=*delay*\]  
Set high availability (HA) failover delay.
Time suffixes are supported, e.g. 10s, 1m.

**secrets\_reload**  
Reload secrets from Vault.

**service\_cache\_reload**  
Reload the service manager cache.

**snmp\_cache\_reload**  
Reload SNMP cache, clear the SNMP properties (engine time, engine boots, engine id, credentials) for all hosts.

**prof\_enable**\[=*target*\]  
Enable profiling.
Affects all processes if target is not specified.
Enabled profiling provides details of all rwlocks/mutexes by function name.
Supported since Zabbix 6.0.13.

**prof\_disable**\[=*target*\]  
Disable profiling.
Affects all processes if target is not specified.
Supported since Zabbix 6.0.13.
  
**log\_level\_increase**\[=*target*\]  
Increase log level, affects all processes if target is not specified

  
**log\_level\_decrease**\[=*target*\]  
Decrease log level, affects all processes if target is not specified

[ ]{#lbAG}

[comment]: # ({/new-800a1525})

[comment]: # ({new-858c64d1})
### 

  
Alvos do controle de nível de log

  
<pid>  
Identificador de processo

  
<tipo do processo>  
Todos os processos do tipo especificado (p.ex., poller)

  
<tipo do processo>,N  
Tipo de processo e número (p.ex., poller,3)

```{=html}
<!-- -->
```
-h, --help  
Apresenta esta ajuda e sai.

-V, --version  
Apresenta informações sobre a versão e sai.

[ ]{#lbAH}

[comment]: # ({/new-858c64d1})

[comment]: # ({new-00ee0d1e})
## ARQUIVOS

*/usr/local/etc/zabbix\_server.conf*  
Local padrão para o arquivo de configuração do servidor Zabbix (se não
modificado em tempo de compilação).

[ ]{#lbAI}

[comment]: # ({/new-00ee0d1e})

[comment]: # ({new-d095490c})
## VEJA TAMBÉM

**[zabbix\_agentd](zabbix_agentd)**(8),
**[zabbix\_get](zabbix_get)**(8), **[zabbix\_proxy](zabbix_proxy)**(8),
**[zabbix\_sender](zabbix_sender)**(8) [ ]{#lbAJ}

[comment]: # ({/new-d095490c})

[comment]: # ({new-e55ed07e})
## AUTOR

Alexei Vladishev <<alex@zabbix.com>>

------------------------------------------------------------------------

[ ]{#index}

[comment]: # ({/new-e55ed07e})

[comment]: # ({new-b105a0f1})
## Índice

[NOME](#lbAB)

[SINOPSE](#lbAC)

[DESCRIÇÃO](#lbAD)

[Opções](#lbAE)

[](#lbAF)

[](#lbAG)

  

[ARQUIVOS](#lbAH)

[VEJA TAMBÉM](#lbAI)

[AUTOR](#lbAJ)

------------------------------------------------------------------------

Este documento foi criado pelo man2html, usando as páginas de manual.\
Horário: 23:38:26 GMT, 15 de setembro de 2014

[comment]: # ({/new-b105a0f1})
