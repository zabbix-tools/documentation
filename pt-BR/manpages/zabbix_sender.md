[comment]: # translation:outdated

[comment]: # ({new-4dfa5936})
# zabbix\_sender

Seção: comandos de manutenção (8)\
Atualizado: 10 de novembro de 2011\
[Índice](#index) [Retornar ao conteúdo
principal](/documentation/2.4/pt/manpages)

------------------------------------------------------------------------

[ ]{#lbAB}

[comment]: # ({/new-4dfa5936})

[comment]: # ({new-1fb3a028})
## NOME

zabbix\_sender - utilitário Zabbix sender. [ ]{#lbAC}

[comment]: # ({/new-1fb3a028})

[comment]: # ({new-3f811652})
## SINOPSE

**zabbix\_sender \[-hpzvIV\] {-kso | \[-T\] -i**
*<arquivo-de-entrada>***} \[-c**
*<arquivo-de-configuração>***\]** [ ]{#lbAD}

[comment]: # ({/new-3f811652})

[comment]: # ({new-14895cca})
## DESCRIÇÃO

**zabbix\_sender** é um utlitário de linha de comando para envio de
dados para um servidor Zabbix remoto. No servidor Zabbix um item de tipo
**Zabbix trapper** deve ser criado com correspondente chave. Note que
valores só serão aceitos pelo server se vindos de hosts especificados no
campo **Hosts permitidos** para este item.

[ ]{#lbAE}

[comment]: # ({/new-14895cca})

[comment]: # ({new-cd7de714})
### Optções

-c, --config *<arquivo-de-configuração>*  
Usa o *arquivo-de-configuração*. O Zabbix sender lê detalhes sobre o
servidor do arquivo de configuração do agente. Por padrão o Zabbix
sender não lê nenhum arquivo de configuração. Deve ser especificado um
caminho absoluto. Somente os parâmetros **Hostname**, **ServerActive**
and **SourceIP** são suportados. A primeira entrada do parâmetro
**ServerActive** é usada.

-z, --zabbix-server *<servidor>*  
Hostname ou endereço IP do servidor Zabbix. Se um host é monitorado por
proxy, o hostname ou endereço IP do proxy deve ser usado.

-p, --port *<porta>*  
Especifica a porta do servidor trapper rodando no server. O padrão
é 10051.

-s, --host *<host>*  
Especifica o hostname tal como registrado no frontend Zabbix. O endereço
IP do host e nome DNS não vão funcionar.

-I, --source-address *<IP>*  
Especifica o endereço IP de origem.

-k, --key *<chave>*  
Especifica a chave de item para a qual enviar o valor.

-o, --value *<valor>*  
Especifica o valor.

-i, --input-file *<arquivo-de-entrada>*  
Carrega valores de um arquivo. Especificar **-** para entrada padrão
(standard input). Cada linha do arquivo contém os seguintes campos,
delimitados por espaço em branco: **<hostname> <chave>
<valor>**. Especificar **-** no **<hostname>** para usar
hostname do arquivo de configuração, ou então o argumento **--host**.
Todas as entradas são enviadas em ordem sequencial, top-down.

-T, --with-timestamps  
Cada linha do arquivo contém os seguintes campos, delimitados por espaço
em branco: **<hostname> <key> <timestamp>
<valor>**. Pode ser usado com a opção **--input-file**. Timestamp
deve ser especificano formato Unix timestamp. Se o item alvo tem
triggers fazendo referência a ele, todos os timestamps devem estar em
ordem crescente. do contrário o cálculo de eventos não funcionará
corretamente.

-r, --real-time  
Enviar valores um por um assim que forem recebidos. Pode ser usado
quando lendo da entrada padrão (standard input).

-v, --verbose  
Modo verbose. -vv para mais detalhes.

-h, --help  
Apresenta esta ajuda e sai.

-V, --version  
Apresenta informações sobre a versão e sai.

[comment]: # ({/new-cd7de714})

[comment]: # ({new-4029bd0a})
## STATUS DE SAÍDA

O status de saída é 0 se os valores foram enviados e todos processados
com sucesso pelo servidor. Se os dados form enviados, mas o
processamento de pelo menos um dos valores falhou, o status é 2. Se o
envio dos dados falhou, o status de saída é 1. [ ]{#lbAF}

[comment]: # ({/new-4029bd0a})

[comment]: # ({new-965bb13b})
## EXEMPLOS

**zabbix\_sender -c /usr/local/etc/zabbix\_agentd.conf -s host
monitorado -k mysql.queries -o 342.45**

Envia **342.45** como o valor para a chave **mysql.queries** em **host
monitorado** usando o servidor Zabbix definido no arquivo de
configuração do agente.

**zabbix\_sender -z 192.168.1.113 -i data\_values.txt**

Envia valores lidos do arquivo **data\_values.txt** para o servidor com
IP **192.168.1.113**. Hostnames e chaves são definidos no arquivo.

**echo - hw.serial.number 1287872261 SQ4321ASDF | zabbix\_sender -c
/usr/local/etc/zabbix\_agentd.conf -T -i -**

Envia um valor com timestamp pela linha de comando para o servidor
Zabbix especificado no arquivo de configuração do agente. Um traço
(dash, "-") nos dados de entrada indica que o hostname a ser utilizado
também deve ser lido do arquivo de configuração.

[ ]{#lbAG}

[comment]: # ({/new-965bb13b})

[comment]: # ({new-9b276166})
## VEJA TAMBÉM

**[zabbix\_agentd](zabbix_agentd)**(8),
**[zabbix\_get](zabbix_get)**(8), **[zabbix\_proxy](zabbix_proxy)**(8),
**[zabbix\_server](zabbix_server)**(8) [ ]{#lbAH}

[comment]: # ({/new-9b276166})

[comment]: # ({new-554afaaf})
## AUTOR

Alexei Vladishev <<alex@zabbix.com>>

------------------------------------------------------------------------

[ ]{#index}

[comment]: # ({/new-554afaaf})

[comment]: # ({new-c84769dd})
## Índice

[NOME](#lbAB)  

[SINOPSE](#lbAC)  

[DESCRIÇÃO](#lbAD)  
[Opções](#lbAE)  

[EXEMPLOS](#lbAF)  

[VEJA TAMBÈM](#lbAG)  

[AUTOR](#lbAH)  

------------------------------------------------------------------------

Este documento foi criado pelo man2html, usando as páginas de manual.\
Horário: 14:48:41 GMT, 23 de julho de 2012

[comment]: # ({/new-c84769dd})


[comment]: # ({new-0c5a6f3e})
## Index

[NAME](#lbAB)

[SYNOPSIS](#lbAC)

[DESCRIPTION](#lbAD)

[OPTIONS](#lbAE)

[](#lbAF)

  

[EXIT STATUS](#lbAG)

[EXAMPLES](#lbAH)

[SEE ALSO](#lbAI)

[AUTHOR](#lbAJ)

------------------------------------------------------------------------

This document was created by [man2html](/cgi-bin/man/man2html), using
the manual pages.\
Time: 08:42:39 GMT, June 11, 2021

[comment]: # ({/new-0c5a6f3e})
