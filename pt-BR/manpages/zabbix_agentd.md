[comment]: # translation:outdated

[comment]: # ({new-2c290a27})
# zabbix\_agentd

Section: comandos de manutenção (8)\
Atualizado: 10 de novembro de 2011\
[Índice](#index) [Retornar ao conteúdo
principal](/documentation/2.4/pt/manpages)

------------------------------------------------------------------------

[ ]{#lbAB}

[comment]: # ({/new-2c290a27})

[comment]: # ({new-75de926e})
## NOME

zabbix\_agentd - daemon do agente Zabbix [ ]{#lbAC}

[comment]: # ({/new-75de926e})

[comment]: # ({new-b8c27067})
## SINOPSE

**zabbix\_agentd \[-hpV\] \[-c** *<arquivo-de-configuração>***\]
\[-t** *<chave-do-item>***\]** [ ]{#lbAD}

[comment]: # ({/new-b8c27067})

[comment]: # ({new-ecdda52c})
## DESCRIÇÃO

**zabbix\_agentd** é um daemon para monitoramento de vários parâmetros
de um servidor. [ ]{#lbAE}

[comment]: # ({/new-ecdda52c})

[comment]: # ({new-63567f56})
### Opções

-c, --config *<arquivo-de-configuração>*  
Usa o *arquivo de configuração* alternativo ao invés do padrão. É
necessário especificar o caminho absoluto.

-R, --runtime-control *<opção>*  
Realiza ações administrativas de acordo com a *opção*.

[ ]{#lbAF}

[comment]: # ({/new-63567f56})

[comment]: # ({new-5333e0f4})
### 

  
Opções de controle em tempo de execução

  
log\_level\_increase\[=<alvo>\]  
Aumenta o nível de log, afetando todos os processos se alvo não for
especificado

  
log\_level\_decrease\[=<alvo>\]  
Diminui o nível de log, afetando todos os processos se alvo não for
especificado

[ ]{#lbAG}

[comment]: # ({/new-5333e0f4})

[comment]: # ({new-199def5e})
### 

  
Alvos do controle de nível de log

  
<pid>  
Identificador de processo

  
<tipo do processo>  
Todos os processos do tipo especificado (p.ex., listener)

  
<tipo do processo>,N  
Tipo de processo e número (p.ex., listener,3)

```{=html}
<!-- -->
```
-p, --print  
Apresenta itens conhecidos e sai. Para cada item, padrões genéricos são
usados, ou padrões específicos para testes são fornecidos. Estes padrões
são listados em colchetes como parâmetros de chaves de item. Valores
retornados são apresentados entre colchetes e prefixados com o tipo do
valor retornado, separados por um caracter pipe. Para parâmetros de
usuário o tipo é sempre **t**, pois o agente não pode determinar todos
os valores possíveis de retorno. Itens, mesmo que apresentados como
funcionando, não tem garantia de funcionamento através de consultas do
Zabbix server ou zabbix get a um agente, pois permissões ou ambiente
podem ser diferentes. Os tipos de valores retornados são:

  
d  
Número com uma parte decimal.

```{=html}
<!-- -->
```
  
m  
Não suportado. Isto pode ser causado pela consulta a um item que só
funciona em modo ativo, tal como monitoramento de log, ou um item que
requer múltiplos valores coletados. Questões de permissões ou parâmetros
de usuário incorretos também podem resultar em um estado não suportado.

```{=html}
<!-- -->
```
  
s  
Texto. Sem limite de tamanho.

```{=html}
<!-- -->
```
  
t  
Texto. Igual a **s**.

```{=html}
<!-- -->
```
  
u  
Inteiro sem sinal.

```{=html}
<!-- -->
```
-t, --test *<chave-de-item>*  
Testa um item e sai. Ver **--print** para descrição da saída.

-h, --help  
Apresenta esta ajuda e sai.

-V, --version  
Apresenta informações sobre a versão e sai.

[ ]{#lbAH}

[comment]: # ({/new-199def5e})

[comment]: # ({new-fcfd4796})
## ARQUIVOS

*/usr/local/etc/zabbix\_agentd.conf*  
Local padrão para o arquivo de configuração do agente Zabbix (se não
modificado em tempo de compilação).

[ ]{#lbAI}

[comment]: # ({/new-fcfd4796})

[comment]: # ({new-539868a7})
## VEJA TAMBÉM

**[zabbix\_get](zabbix_get)**(8), **[zabbix\_proxy](zabbix_proxy)**(8),
**[zabbix\_sender](zabbix_sender)**(8),
**[zabbix\_server](zabbix_server)**(8) [ ]{#lbAJ}

[comment]: # ({/new-539868a7})

[comment]: # ({new-e55ed07e})
## AUTOR

Alexei Vladishev <<alex@zabbix.com>>

------------------------------------------------------------------------

[ ]{#index}

[comment]: # ({/new-e55ed07e})

[comment]: # ({new-6ea4e50b})
## Índice

[NOME](#lbAB)

[SINOPSE](#lbAC)

[DESCRIÇÃO](#lbAD)

[Opções](#lbAE)

[](#lbAF)

[](#lbAG)

  

[ARQUIVOS](#lbAH)

[VEJA TAMBÉM](#lbAI)

[AUTOR](#lbAJ)

------------------------------------------------------------------------

Este documento foi criado pelo man2html, usando as páginas de manual.\
Horário 23:32:57 GMT, 15 de setembro de 2014

[comment]: # ({/new-6ea4e50b})
