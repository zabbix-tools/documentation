[comment]: # translation:outdated

[comment]: # ({new-dabb0bd5})
# zabbix\_get

Seção: comandos de manutenção (8)\
Atualizado: 5 de julho de 2011\
[Índice](#index) [Retornar ao conteúdo
principal](/documentation/2.4/pt/manpages)

------------------------------------------------------------------------

[ ]{#lbAB}

[comment]: # ({/new-dabb0bd5})

[comment]: # ({new-0ba1ce53})
## NOME

zabbix\_get - utilitário Zabbix get. [ ]{#lbAC}

[comment]: # ({/new-0ba1ce53})

[comment]: # ({new-60e7e93b})
## SINOPSE

**zabbix\_get \[-hV\] \[-s** *<hostname ou IP>***\] \[-p**
*<porta>***\] \[-I** *<endereço IP>***\] \[-k** *<chave
de item>***\]** [ ]{#lbAD}

[comment]: # ({/new-60e7e93b})

[comment]: # ({new-da08bc1a})
## DESCRIÇÃO

**zabbix\_get** é um utilitário de linha de comando para recuperar dados
de um agente Zabbix remoto. [ ]{#lbAE}

[comment]: # ({/new-da08bc1a})

[comment]: # ({new-4ab1a759})
### Opções

-s, --host *<hostname ou IP>*  
Especifica hostname ou endereço IP de um host.

-p, --port *<porta>*  
Especifica a porta de um agente rodando no host. O padrão é 10050.

-I, --source-address *<endereço IP>*  
Especifica endereço IP de origem.

-k, --key *<item key>*  
Especifica a chave do item cujo valor deve ser retornado.

-h, --help  
Apresenta esta ajuda e sai.

-V, --version  
Apresenta informações sobre a versão e sai.

[ ]{#lbAF}

[comment]: # ({/new-4ab1a759})

[comment]: # ({new-465bc4c8})
## EXEMPLOS

**zabbix\_get -s 127.0.0.1 -p 10050 -k system.cpu.load\[all,avg1\]**
[ ]{#lbAG}

[comment]: # ({/new-465bc4c8})

[comment]: # ({new-c5e4d00b})
## VEJA TAMBÉM

**[zabbix\_agentd](zabbix_agentd)**(8),
**[zabbix\_proxy](zabbix_proxy)**(8),
**[zabbix\_sender](zabbix_sender)**(8),
**[zabbix\_server](zabbix_server)**(8) [ ]{#lbAH}

[comment]: # ({/new-c5e4d00b})

[comment]: # ({new-0ed7cca7})
## AUTOR

Alexei Vladishev <<alex@zabbix.com>>

------------------------------------------------------------------------

[ ]{#index}

[comment]: # ({/new-0ed7cca7})

[comment]: # ({new-aaed5529})
## Índice

[NOME](#lbAB)  

[SINOPSE](#lbAC)  

[DESCRIÇÃO](#lbAD)  
[Opções](#lbAE)  

[EXEMPLOS](#lbAF)  

[VEJA TAMBÉM](#lbAG)  

[AUTOR](#lbAH)  

------------------------------------------------------------------------

Este documento foi criado pelo man2html, usando as páginas de manual.\
Horário: 14:47:43 GMT, 23 de julho de 2012

[comment]: # ({/new-aaed5529})


[comment]: # ({new-bdb17437})
## Index

[NAME](#lbAB)

[SYNOPSIS](#lbAC)

[DESCRIPTION](#lbAD)

[OPTIONS](#lbAE)

[](#lbAF)

  

[EXAMPLES](#lbAG)

[SEE ALSO](#lbAH)

[AUTHOR](#lbAI)

------------------------------------------------------------------------

This document was created by [man2html](/cgi-bin/man/man2html), using
the manual pages.\
Time: 08:42:29 GMT, June 11, 2021

[comment]: # ({/new-bdb17437})
