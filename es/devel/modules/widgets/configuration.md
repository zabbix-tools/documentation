[comment]: # translation:outdated

[comment]: # aside:1

[comment]: # ({new-f7477fb0})
# Configuration

Widget configuration is part of the widget module that allows the user to define widget settings for [presentation](/presentation). This page describes classes that can be used to create a widget configuration form with custom fields.

[comment]: # ({/new-f7477fb0})

[comment]: # ({new-adf59b13})
### Widget

Primary widget class that extends the default class CWidget. Required to override default widget methods or to add constants.

**Widget.php example**

```php
<?php
    
namespace Modules\WidgetNameHere;
    
use Zabbix\Core\CWidget;
    
class Widget extends CWidget {
    
    public const MY_CONSTANT = 0;
    
    public function getTranslationStrings(): array {
        return [
            'class.widget.js' => [
                'No data' => _('No data')
            ]
        ];
    }
}
```

[comment]: # ({/new-adf59b13})

[comment]: # ({new-1ff26877})
### WidgetForm

Contains a set of CWidgetField fields that are needed to define widget configuration storage structure in the database and handle input validation.

WidgetForm class is inherited from CWidgetForm and should be located in the *includes* directory.
May have a different name, in this case the name should be specified in the manifest.json section [widget/form_class](/devel/modules/file_structure/manifest#widget).

**includes/WidgetForm.php example**

```php
<?php
    
namespace Modules\WidgetNameHere\Includes;
    
use Modules\WidgetNameHere\Widget;
use Zabbix\Widgets\CWidgetField;
use Zabbix\Widgets\CWidgetForm;
    
use Zabbix\Widgets\Fields\CWidgetFieldMultiSelectItem;
use Zabbix\Widgets\Fields\CWidgetFieldTextBox;


class WidgetForm extends CWidgetForm {
    
    public function addFields(): self {
        return $this
            ->addField(
                (new CWidgetFieldMultiSelectItem('itemid', _('Item')))
                    ->setFlags(CWidgetField::FLAG_NOT_EMPTY | CWidgetField::FLAG_LABEL_ASTERISK)
                    ->setMultiple(false)
                    ->setFilterParameter('numeric', true)
            )
            ->addField(
                new CWidgetFieldTextBox('description', _('Description'))
            );
    }
}
```

[comment]: # ({/new-1ff26877})


[comment]: # ({new-61f06fc3})
### Widget configuration view

While WidgetForm defines what configuration fields widget will have and what data they will process,
widget configuration view file defines how these fields will be shown to the user in the configuration popup.

Widget configuration view is built by the class *CWidgetFormView*.

To add one of the previously configured CFormField classes to the configuration form, use the *addField()* method of the *CWidgetFormView*.

*addField()* method receives an instance of *CWidgetFieldView* class as a parameter.
Each *CWidgetField* class has a respective *CWidgetFieldView* class for using in the widget configuration view.
This class defines how exactly the widget field will look in the configuration view.

To add a JavaScript file to the configuration view, use the *includeJsFile()* method.

To add an inline JavaScript that will be executed as soon as widget configuration form is loaded, use the *addJavaScript()* method.

**views/widget.edit.php example**

```php
<?php
    
/**
 * My custom widget form view.
 *
 * @var CView $this
 * @var array $data
 */

use Zabbix\Widgets\Fields\CWidgetFieldMultiSelectItem;
use Zabbix\Widgets\Fields\CWidgetFieldTextBox;

(new CWidgetFormView($data))
    ->addField(
        new CWidgetFieldMultiSelectItemView($data['fields']['itemid'], $data['captions']['ms']['items']['itemid'])
    )
    ->addField(
        new CWidgetFieldTextBoxView($data['fields']['description']),
        'js-advanced-configuration'
    )
    ->includeJsFile('widget.edit.js.php')
    ->addJavaScript('my_widget_form.init('.json_encode([
            'color_palette' => CWidgetFieldGraphDataSet::DEFAULT_COLOR_PALETTE
        ], JSON_THROW_ON_ERROR).');')
    ->show();   
```

[comment]: # ({/new-61f06fc3})

[comment]: # ({new-d1c62429})
### JavaScript

A JavaScript class can be used to add dynamic behavior and interactivity to the widget configuration view.
For example, you can initialize a color picker, defined in the [*CWidgetFormView*](#cwidgetformview) class.

The JavaScript class should be loaded with the form, therefore it should be referenced in the [*CWidgetFormView*](#cwidgetformview) class by using the methods *includeJsFile()* and *addJavaScript()*.

In the example below, a singleton class instance is immediately created and stored under the *window.my_custom_widget_form* name.
Thus, opening the form for the second time will re-create the instance.

The JavaScript class should be located in the *views* directory.

**views/widget.edit.js.php example**

```php
<?php

use Modules\MyCustomWidget\Widget;

?>

window.my_custom_widget_form = new class {

    init({color_palette}) {
        colorPalette.setThemeColors(color_palette);

        for (const colorpicker of jQuery('.<?= ZBX_STYLE_COLOR_PICKER ?> input')) {
            jQuery(colorpicker).colorpicker();
        }

        const overlay = overlays_stack.getById('widget_properties');

        for (const event of ['overlay.reload', 'overlay.close']) {
            overlay.$dialogue[0].addEventListener(event, () => { jQuery.colorpicker('hide'); });
        }
    }
};
```

[comment]: # ({/new-d1c62429})

[comment]: # ({new-708ecb6b})
### CWidgetField

The *CWidgetField* class is a base class from which all form field classes (*CWidgetFieldCheckBox*, *CWidgetFieldTextArea*, *CWidgetFieldRadioButtonList*, etc.) are inherited.
Classes extending *CWidgetField* are responsible for receiving, saving, and validating widget configuration values.

The following *CWidgetField* classes are available.

|CWidgetField class|Database field type|Description|
|----|--|------------|
|*CWidgetFieldCheckBox*|int32|Single checkbox.|
|*CWidgetFieldCheckBoxList*|array of int32|Multiple checkboxes under a single configuration field.|
|*CWidgetFieldColor*|string|Color selection field.|
|*CWidgetFieldDatePicker*|string|Date selection field.|
|*CWidgetFieldHostPatternSelect*|string|Multiselect field that allows to select one or multiple hosts. Supports defining host name patterns (all matching hosts will be selected).|
|*CWidgetFieldIntegerBox*|int32|Field to enter an integer. Can be used to configure minimum and maximum values.|
|*CWidgetFieldLatLng*|string|Text box that allows to enter comma-separated latitude, longitude, and map zoom level.|
|*CWidgetFieldMultiSelectAction*|ID|Multiselect field for selecting actions (from the list of actions defined in the *Alerts → Actions*).|
|*CWidgetFieldMultiSelectGraph*|ID|Multiselect field for selecting custom graphs.|
|*CWidgetFieldMultiSelectGraphPrototype*|ID|Multiselect field for selecting custom graph prototypes.|
|*CWidgetFieldMultiSelectGroup*|ID|Multiselect field for selecting host groups.|
|*CWidgetFieldMultiSelectHost*|ID|Multiselect field for selecting hosts.|
|*CWidgetFieldMultiSelectItem*|ID|Multiselect field for selecting items.|
|*CWidgetFieldMultiSelectItemPrototype*|ID|Multiselect field for selecting item prototypes.|
|*CWidgetFieldMultiSelectMediaType*|ID|Multiselect field for selecting media types.|
|*CWidgetFieldMultiSelectService*|ID|Multiselect field for selecting services.|
|*CWidgetFieldMultiSelectSla*|ID|Multiselect field for selecting SLAs.|
|*CWidgetFieldMultiSelectUser*|ID|Multiselect field for selecting users.|
|*CWidgetFieldNumericBox*|string|Field to enter a float number.|
|*CWidgetFieldRadioButtonList*|int32|Radio box group that consists of one or more radio boxes.|
|*CWidgetFieldRangeControl*|int32|Slider to select an integer type value.|
|*CWidgetFieldSelect*|int32|Dropdown select box.|
|*CWidgetFieldSeverities*|array of int32|*CWidgetFieldCheckBoxList* preset with trigger severities.|
|*CWidgetFieldTags*|array of (string, int32, string)|Allows to configure one or more tag filter rows.|
|*CWidgetFieldTextArea*|string|Text area for entering multi-line text.|
|*CWidgetFieldTextBox*|string|Text box for entering single-line text.|
|*CWidgetFieldTimeZone*|string|Dropdown with timezones.|
|*CWidgetFieldUrl*|string|Text box that allows to enter URLs.|

The following *CWidgetField* classes have been created for particular widgets.
These classes have very specific use cases, but they can also be reused if needed.

|CWidgetField class|Database field type|Description|
|----|--|------------|
|*CWidgetFieldColumnsList*|array of (multiple mixed)|For top host widget. Create a table with custom columns of allowed types.|
|*CWidgetFieldGraphDataSet*|array of (multiple mixed)|For graph widget. Setup dataset configuration and all related options.|
|*CWidgetFieldGraphOverride*|array of (multiple mixed)|For graph widget. Setup overrides for specific hosts/items. Any dataset configuration can be overridden.|
|*CWidgetFieldNavTree*|string|For map navigation tree widget. Replaces widget view in edit mode with the map selection tree.|
|*CWidgetFieldReference*|string|For map navigation tree widget. Creates a unique identifier for this widget on dashboard. It is used to reference this widget from other widgets.|
|*CWidgetFieldSelectResource*|ID|For map widget. Allows to select Zabbix network map.|
|*CWidgetFieldThresholds*|array of (string, string)|For top hosts widget. Allows to configure color and number pairs.|
|*CWidgetFieldWidgetSelect*|string|For map widget. Allows to select a map navigation tree from the current dashboard. Must be used in combination with CWidgetFieldReference in the Map Navigation Tree widget.|

[comment]: # ({/new-708ecb6b})
