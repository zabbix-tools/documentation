[comment]: # translation:outdated

[comment]: # ({new-7ed38ae8})
# 2 Inventory

[comment]: # ({/new-7ed38ae8})

[comment]: # ({new-65644c90})
#### Overview

The Inventory menu features sections providing an overview of host
inventory data by a chosen parameter as well as the ability to view host
inventory details.

[comment]: # ({/new-65644c90})
