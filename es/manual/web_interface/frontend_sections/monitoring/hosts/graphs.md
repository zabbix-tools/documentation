[comment]: # translation:outdated

[comment]: # ({new-0808b0e0})
# 1 Graphs

[comment]: # ({/new-0808b0e0})

[comment]: # ({new-f1736522})
#### Overview

Host graphs can be accessed from *Monitoring → Hosts* by clicking on
Graphs for the respective host.

Any [custom graph](/manual/config/ visualization/graphs/custom) that has
been configured for the host can be displayed, however, no more than 20
graphs can be displayed at one time.

![](../../../../../../assets/en/manual/web_interface/graphs.png){width="600"}

The *View as* option allows to view the data graphically or as values.
Graphs for disabled hosts are also accessible.

[comment]: # ({/new-f1736522})

[comment]: # ({new-1c009200})
##### Time period selector

Take note of the time period selector above the graph. It allows
selecting often required periods with one mouse click.

See also: [Time period
selector](/manual/config/visualization/graphs/simple#time_period_selector)

[comment]: # ({/new-1c009200})

[comment]: # ({new-1d7a6bb0})
##### Using filter

To view a specific graph, select it in the filter. The filter allows to
specify one host at a time (host is mandatory), and then specify host
graphs either by selecting from the list or by searching by the graph
name pattern.

![](../../../../../../assets/en/manual/web_interface/graph_filter.png){width="600"}

[comment]: # ({/new-1d7a6bb0})

[comment]: # ({new-29352749})
#### Using subfilter

The subfilter is useful for a quick one-click access to related graphs. 
The subfilter operates autonomously from the main filter - results are filtered 
immediately, no need to click on *Apply* in the main filter.

Note that the subfilter only allows to further modify the filtering from the main filter. 

Unlike the main filter, the subfilter is updated together with each table refresh request 
to always get up-to-date information of available filtering options and their counter 
numbers.

The subfilter shows **clickable links** allowing to filter graphs based on a common 
entity - the tag name or tag value. As soon as the entity is clicked, graphs are 
immediately filtered; the selected entity is highlighted with gray background. 
To remove the filtering, click on the entity again. To add another 
entity to the filtered results, click on another entity.

The number of entities displayed is limited to 100 horizontally. If there are more, 
a three-dot icon is displayed at the end; it is not clickable. Vertical lists 
(such as tags with their values) are limited to 20 entries. If there are more, 
a three-dot icon is displayed; it is not clickable.

A number next to each clickable entity indicates the number of graphs it has in the 
results of the main filter.

Once one entity is selected, the numbers with other available entities are displayed 
with a plus sign indicating how many graphs may be added to the current selection.

[comment]: # ({/new-29352749})

[comment]: # ({new-1e6dcc6e})
##### Buttons

Buttons to the right offer the following options:

|   |   |
|---|---|
|![](../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/add_to_fav1.png)|Add the graph to the favorites widget in the [Dashboard](/manual/web_interface/frontend_sections/monitoring/dashboard).|
|![](../../../../../../assets/en/manual/web_interface/button_add_fav2.png)|The graph is in the favorites widget in the [Dashboard](/manual/web_interface/frontend_sections/monitoring/dashboard). Click to remove the graph from the favorites widget.|

View mode buttons, being common for all sections, are described on the
[Monitoring](/manual/web_interface/frontend_sections/monitoring#view_mode_buttons)
page.

[comment]: # ({/new-1e6dcc6e})
