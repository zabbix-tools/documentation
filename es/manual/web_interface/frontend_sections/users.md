[comment]: # translation:outdated

[comment]: # ({new-318de170})
# 8 Users

[comment]: # ({/new-318de170})

[comment]: # ({new-7d66c187})
#### Overview

This menu features sections that are related to configuring 
users in Zabbix. This menu is available to [SuperAdmin](/manual/config/users_and_usergroups/permissions) user type users only.

[comment]: # ({/new-7d66c187})
