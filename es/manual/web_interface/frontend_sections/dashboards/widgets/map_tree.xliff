<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="es" datatype="plaintext" original="manual/web_interface/frontend_sections/dashboards/widgets/map_tree.md">
    <body>
      <trans-unit id="0f95e7b7" xml:space="preserve">
        <source># 14 Map navigation tree</source>
      </trans-unit>
      <trans-unit id="e61997b9" xml:space="preserve">
        <source>#### Overview

This widget allows building a hierarchy of existing maps while also
displaying problem statistics with each included map and map group.

It becomes even more powerful if you link the *Map* widget to the
navigation tree. In this case, clicking on a map name in the navigation
tree displays the map in full in the *Map* widget.

![](../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/map_to_display.png){width="600"}

Statistics with the top-level map in the hierarchy display a sum of
problems of all submaps and their own problems.</source>
      </trans-unit>
      <trans-unit id="80a3a493" xml:space="preserve">
        <source>#### Configuration

To configure, select *Map navigation tree* as type:

![](../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/map_tree.png)

In addition to the parameters that are [common](/manual/web_interface/frontend_sections/dashboards/widgets#common-parameters) 
for all widgets, you may set the following specific options:

|   |   |
|--|--------|
|*Show unavailable maps*|Mark this checkbox to display maps that the user does not have read permission to.&lt;br&gt;Unavailable maps in the navigation tree will be displayed with a grayed-out icon.&lt;br&gt;Note that if this checkbox is marked, available [submaps](/manual/config/visualization/maps/map#adding-elements) are displayed even if the parent level map is unavailable. If unmarked, available submaps to an unavailable parent map will not be displayed at all.&lt;br&gt;Problem count is calculated based on available maps and available map elements.|</source>
      </trans-unit>
      <trans-unit id="947854a0" xml:space="preserve">
        <source>
Navigation tree elements are displayed in a list. You can:

-   drag an element (including its child elements) to a new place in the list;
-   expand or collapse an element to display or hide its child elements;
-   add a child element (with or without a linked map) to an element;
-   add multiple child elements (with linked maps) to an element;
-   edit an element;
-   remove an element (including its child elements).

![](../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/map_tree_element.png)</source>
      </trans-unit>
      <trans-unit id="b55502b3" xml:space="preserve">
        <source>##### Element configuration</source>
      </trans-unit>
      <trans-unit id="bcccd25d" xml:space="preserve">
        <source>
To configure a navigation tree element, either add a new element or edit an existing element.

![](../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/map_tree_element_configuration.png)

The following navigation tree element configuration parameters are available:

|   |   |
|--|--------|
|*Name*|Enter the navigation tree element name.|
|*Linked map*|Select the map to link to the navigation tree element.&lt;br&gt;This field is auto-complete so starting to type the name of a map will offer a dropdown of matching maps.|
|*Add submaps*|Mark this checkbox to add the [submaps](/manual/config/visualization/maps/map#adding-elements) of the linked map as child elements to the navigation tree element.|</source>
      </trans-unit>
    </body>
  </file>
</xliff>
