<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="es" datatype="plaintext" original="manual/appliance.md">
    <body>
      <trans-unit id="8c911baa" xml:space="preserve">
        <source># 6. Zabbix appliance</source>
      </trans-unit>
      <trans-unit id="cfa1a361" xml:space="preserve">
        <source>#### Overview

As an alternative to setting up manually or reusing an existing server
for Zabbix, users may
[download](http://www.zabbix.com/download_appliance) a Zabbix appliance
or a Zabbix appliance installation CD image.

Zabbix appliance and installation CD versions are based on AlmaLinux 8
(x86\_64).

Zabbix appliance installation CD can be used for instant deployment of
Zabbix server (MySQL).

::: noteimportant
 You can use this Appliance to evaluate Zabbix.
The Appliance is not intended for serious production use. 
:::</source>
        <target state="needs-translation">#### Descripción general

Como alternativa a la configuración manual o la reutilización de un servidor existente
para Zabbix, los usuarios pueden
[descargar](http://www.zabbix.com/download_appliance) un dispositivo Zabbix
o una imagen de CD de instalación del dispositivo Zabbix.

Las versiones del CD de instalación y el dispositivo Zabbix se basan en CentOS 8
(x86\_64).

El CD de instalación del dispositivo Zabbix se puede usar para la implementación instantánea de
Servidor Zabbix (MySQL).

::: notaimportante
 Puede usar este dispositivo para evaluar Zabbix.
El dispositivo no está diseñado para un uso serio en producción.
:::</target>
      </trans-unit>
      <trans-unit id="cffa82a3" xml:space="preserve">
        <source>##### System requirements:

-   *RAM*: 1.5 GB
-   *Disk space*: at least 8 GB should be allocated for the virtual
    machine
-   *CPU*: 2 cores minimum 

Zabbix installation CD/DVD boot menu:

![](../../assets/en/manual/installation_cd_boot_menu1.png){width="600"}

Zabbix appliance contains a Zabbix server (configured and running on
MySQL) and a frontend.

Zabbix virtual appliance is available in the following formats:

-   VMware (.vmx)
-   Open virtualization format (.ovf)
-   Microsoft Hyper-V 2012 (.vhdx)
-   Microsoft Hyper-V 2008 (.vhd)
-   KVM, Parallels, QEMU, USB stick, VirtualBox, Xen (.raw)
-   KVM, QEMU (.qcow2)

To get started, boot the appliance and point a browser at the IP the
appliance has received over DHCP.

::: noteimportant
 DHCP must be enabled on the host. 
:::

To get the IP address from inside the virtual machine run:

    ip addr show

To access Zabbix frontend, go to **http://&lt;host\_ip&gt;** (for access
from the host's browser bridged mode should be enabled in the VM network
settings).

::: notetip
If the appliance fails to start up in Hyper-V, you may
want to press `Ctrl+Alt+F2` to switch tty sessions.
:::</source>
        <target state="needs-translation">##### Requisitos del sistema:

- *RAM*: 1,5GB
- *Espacio en disco*: se deben asignar al menos 8 GB para la máquina virtual.

Menú de arranque del CD/DVD de instalación de Zabbix:

![](../../assets/en/manual/installation_cd_boot_menu1.png){width="600"}

El dispositivo Zabbix contiene un servidor Zabbix (configurado y ejecutándose en
MySQL) y una interfaz.

El dispositivo virtual Zabbix está disponible en los siguientes formatos:

-VMWare (.vmx)
- Formato de virtualización abierto (.ovf)
-Microsoft Hyper-V 2012 (.vhdx)
-Microsoft Hyper-V 2008 (.vhd)
- KVM, Parallels, QEMU, memoria USB, VirtualBox, Xen (.raw)
-KVM, QEMU (.qcow2)

Para comenzar, inicie el dispositivo y apunte un navegador a la dirección IP
dispositivo ha recibido a través de DHCP.

::: notaimportante
 DHCP debe estar habilitado en el equipo.
:::

Para obtener la dirección IP desde el interior de la máquina virtual, ejecute:

    ip addr show

Para acceder a la interfaz de Zabbix, vaya a **http://&lt;host\_ip&gt;** (para acceder
desde el navegador del host, el modo puente debe estar habilitado en la red de VM
ajustes).

::: nota
Si el dispositivo no se inicia en Hyper-V, puede
desea presionar `Ctrl+Alt+F2` para cambiar de sesión de tty.
:::</target>
      </trans-unit>
      <trans-unit id="589fd5e2" xml:space="preserve">
        <source>#### - Changes to AlmaLinux 8 configuration

The appliance is based on AlmaLinux 8. There are some changes applied to
the base AlmaLinux configuration.</source>
        <target state="needs-translation">#### - Cambios en la configuración de CentOS 8

El dispositivo se basa en CentOS 8. Se han aplicado algunos cambios a
la configuración básica de CentOS.</target>
      </trans-unit>
      <trans-unit id="193d3b23" xml:space="preserve">
        <source>##### - Repositories

Official Zabbix
[repository](/manual/installation/install_from_packages/rhel) has
been added to */etc/yum.repos.d*:

    [zabbix]
    name=Zabbix Official Repository - $basearch
    baseurl=http://repo.zabbix.com/zabbix/5.2/rhel/8/$basearch/
    enabled=1
    gpgcheck=1
    gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-ZABBIX-A14FE591</source>
        <target state="needs-translation">##### - Repositorios

El [repositorio] (/manual/installation/install_from_packages/rhel_centos) oficial de Zabbix tiene
agregado a */etc/yum.repos.d*:

[zabbix]
name=Zabbix Official Repository - $basearch
baseurl=http://repo.zabbix.com/zabbix/5.2/rhel/8/$basearch/
enabled=1
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-ZABBIX-A14FE591</target>
      </trans-unit>
      <trans-unit id="589dc798" xml:space="preserve">
        <source>##### - Firewall configuration

The appliance uses iptables firewall with predefined rules:

-   Opened SSH port (22 TCP);
-   Opened Zabbix agent (10050 TCP) and Zabbix trapper (10051 TCP)
    ports;
-   Opened HTTP (80 TCP) and HTTPS (443 TCP) ports;
-   Opened SNMP trap port (162 UDP);
-   Opened outgoing connections to NTP port (53 UDP);
-   ICMP packets limited to 5 packets per second;
-   All other incoming connections are dropped.</source>
        <target state="needs-translation">##### - Configuración del cortafuegos

El dispositivo utiliza el cortafuegos de iptables con reglas predefinidas:

- Puerto SSH abierto (22 TCP);
- Apertura de puertos del agente Zabbix (10050 TCP) y Zabbix trapper (10051 TCP);
- Apertura de puertos HTTP (80 TCP) y HTTPS (443 TCP);
- Puerto de captura SNMP abierto (162 UDP);
- Conexiones salientes abiertas al puerto NTP (53 UDP);
- Paquetes ICMP limitados a 5 paquetes por segundo;
- Todas las demás conexiones entrantes se interrumpen.</target>
      </trans-unit>
      <trans-unit id="b2283a9a" xml:space="preserve">
        <source>##### - Using a static IP address

By default the appliance uses DHCP to obtain the IP address. To specify
a static IP address:

-   Log in as root user;
-   Open */etc/sysconfig/network-scripts/ifcfg-eth0* file;
-   Replace *BOOTPROTO=dhcp* with *BOOTPROTO=none*
-   Add the following lines:
    -   *IPADDR=&lt;IP address of the appliance&gt;*
    -   *PREFIX=&lt;CIDR prefix&gt;*
    -   *GATEWAY=&lt;gateway IP address&gt;*
    -   *DNS1=&lt;DNS server IP address&gt;*
-   Run **systemctl restart network** command.

Consult the official Red Hat
[documentation](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/deployment_guide/s1-networkscripts-interfaces)
if needed.</source>
        <target state="needs-translation">##### - Uso de una dirección IP estática

De forma predeterminada, el dispositivo utiliza DHCP para obtener la dirección IP. Para especificar
una dirección IP estática:

- Inicie sesión como usuario root;
- Abra el archivo */etc/sysconfig/network-scripts/ifcfg-eth0*;
- Reemplace *BOOTPROTO=dhcp* con *BOOTPROTO=none*
- Agregue las siguientes líneas:
    - *IPADDR=&lt;dirección IP del dispositivo&gt;*
    - *PREFIX=&lt;prefijo CIDR&gt;*
    - *GATEWAY=&lt;dirección IP de la puerta de enlace&gt;*
    - *DNS1=&lt;dirección IP del servidor DNS&gt;*
- Ejecute el comando **systemctl restart network**.

Consulte la [documentación](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/deployment_guide/s1-networkscripts-interfaces) oficial de Red Hat
si es necesario.</target>
      </trans-unit>
      <trans-unit id="acf06935" xml:space="preserve">
        <source>##### - Changing time zone

By default the appliance uses UTC for the system clock. To change the
time zone, copy the appropriate file from */usr/share/zoneinfo* to
*/etc/localtime*, for example:

    cp /usr/share/zoneinfo/Europe/Riga /etc/localtime</source>
        <target state="needs-translation">##### - Cambio de zona horaria

De forma predeterminada, el dispositivo utiliza UTC para el reloj del sistema. Para cambiar la
zona horaria, copie el archivo apropiado de */usr/share/zoneinfo* a
*/etc/localtime*, por ejemplo:

    cp /usr/share/zoneinfo/Europa/Riga /etc/localtime</target>
      </trans-unit>
      <trans-unit id="2c378c8d" xml:space="preserve">
        <source>#### - Zabbix configuration

Zabbix appliance setup has the following passwords and configuration
changes:</source>
        <target state="needs-translation">#### - Configuración de Zabbix

La configuración del dispositivo Zabbix tiene las siguientes contraseñas y cambios de configuración:</target>
      </trans-unit>
      <trans-unit id="d39b5151" xml:space="preserve">
        <source>##### - Credentials (login:password)

System:

-   root:zabbix

Zabbix frontend:

-   Admin:zabbix

Database:

-   root:&lt;random&gt;
-   zabbix:&lt;random&gt;

::: noteclassic
Database passwords are randomly generated during the
installation process.\
Root password is stored inside the /root/.my.cnf file. It is not
required to input a password under the "root" account.
:::

To change the database user password, changes have to be made in the
following locations:

-   MySQL;
-   /etc/zabbix/zabbix\_server.conf;
-   /etc/zabbix/web/zabbix.conf.php.

::: noteclassic
 Separate users `zabbix_srv` and `zabbix_web` are defined
for the server and the frontend respectively. 
:::</source>
        <target state="needs-translation">##### - Credenciales (inicio de sesión: contraseña)

Sistema:

- root: zabbix

Interfaz de Zabbix:

- Admin: zabbix

Base de datos:

- root: &lt;aleatorio&gt;
- zabbix:&lt;aleatorio&gt;

::: notaclásica
Las contraseñas de la base de datos se generan aleatoriamente durante el
proceso de instalación.\
La contraseña de root se almacena dentro del archivo /root/.my.cnf. Esto no es
requerido para ingresar una contraseña en la cuenta "root".
:::

Para cambiar la contraseña de usuario de la base de datos, se deben realizar cambios en el
siguientes lugares:

- mysql;
- /etc/zabbix/zabbix\_server.conf;
- /etc/zabbix/web/zabbix.conf.php.

::: notaclásica
 Se definen usuarios separados `zabbix_srv` y `zabbix_web`
para el servidor y la interfaz respectivamente.
:::</target>
      </trans-unit>
      <trans-unit id="f11ee379" xml:space="preserve">
        <source>##### - File locations

-   Configuration files are located in **/etc/zabbix**.
-   Zabbix server, proxy and agent logfiles are located in
    **/var/log/zabbix**.
-   Zabbix frontend is located in **/usr/share/zabbix**.
-   Home directory for the user **zabbix** is **/var/lib/zabbix**.</source>
        <target state="needs-translation">##### - Ubicaciones de los archivos

- Los archivos de configuración se encuentran en **/etc/zabbix**.
- Los archivos de registro del servidor, proxy y agente de Zabbix se encuentran en
    **/var/log/zabbix**.
- La interfaz de Zabbix se encuentra en **/usr/share/zabbix**.
- El directorio de inicio del usuario **zabbix** es **/var/lib/zabbix**.</target>
      </trans-unit>
      <trans-unit id="e00773c5" xml:space="preserve">
        <source>##### - Changes to Zabbix configuration

-   Frontend timezone is set to Europe/Riga (this can be modified in
    **/etc/php-fpm.d/zabbix.conf**);</source>
        <target state="needs-translation">##### - Cambios en la configuración de Zabbix

- La zona horaria de la interfaz está configurada en Europa/Riga (esto se puede modificar en
    **/etc/php-fpm.d/zabbix.conf**);</target>
      </trans-unit>
      <trans-unit id="2c4e4239" xml:space="preserve">
        <source>#### - Frontend access

By default, access to the frontend is allowed from anywhere.

The frontend can be accessed at *http://&lt;host&gt;*.

This can be customized in **/etc/nginx/conf.d/zabbix.conf**. Nginx has
to be restarted after modifying this file. To do so, log in using SSH as
**root** user and execute:

    systemctl restart nginx</source>
        <target state="needs-translation">#### - Acceso a la interfaz

De forma predeterminada, el acceso a la interfaz está permitido desde cualquier lugar.

Se puede acceder a la interfaz en *http://&lt;host&gt;*.

Esto se puede personalizar en **/etc/nginx/conf.d/zabbix.conf**. Nginx tiene
que ser reiniciado después de modificar este archivo. Para hacerlo, inicie sesión usando SSH como
Usuario **root** y ejecute:

    systemctl restart nginx</target>
      </trans-unit>
      <trans-unit id="4fbc391b" xml:space="preserve">
        <source>#### - Firewall

By default, only the ports listed in the [configuration
changes](#firewall_configuration) above are open. To open additional
ports, modify "*/etc/sysconfig/iptables*" file and reload firewall
rules:

    systemctl reload iptables</source>
        <target state="needs-translation">#### - Cortafuegos

De manera predeterminada, solo los puertos enumerados en los [cambios de configuración
](#firewall_configuration) arriba están abiertos. Para abrir puertos adicionales, modifique el archivo "*/etc/sysconfig/iptables*" y vuelva a cargar las reglas del firewall:

    systemctl reload iptables</target>
      </trans-unit>
      <trans-unit id="bf60554b" xml:space="preserve">
        <source>#### - Upgrading

The Zabbix appliance packages may be upgraded. To do so, run:

    dnf update zabbix*</source>
        <target state="needs-translation">#### - Actualización

Los paquetes del dispositivo Zabbix pueden actualizarse. Para hacerlo, ejecuta:

    dnf update zabbix*</target>
      </trans-unit>
      <trans-unit id="d167c766" xml:space="preserve">
        <source>#### - System Services

Systemd services are available:

    systemctl list-units zabbix*</source>
        <target state="needs-translation">#### - Servicios del sistema

Los servicios de Systemd están disponibles:

    systemctl list-units zabbix*</target>
      </trans-unit>
      <trans-unit id="a582d1bf" xml:space="preserve">
        <source>#### - Format-specific notes</source>
        <target state="needs-translation">#### - Notas específicas del formato</target>
      </trans-unit>
      <trans-unit id="a45df55b" xml:space="preserve">
        <source>##### - VMware

The images in *vmdk* format are usable directly in VMware Player, Server
and Workstation products. For use in ESX, ESXi and vSphere they must be
converted using [VMware
converter](http://www.vmware.com/products/converter/).
If you use VMWare Converter, you may encounter issues with the hybrid network adapter. In that case, you can try
specifying the E1000 adapter during the conversion process. Alternatively, after the conversion is complete,
you can delete the existing adapter and add an E1000 adapter.</source>
        <target state="needs-translation">##### - VMware

Las imágenes en formato *vmdk* se pueden usar directamente en VMware Player, Server
y productos para estaciones de trabajo. Para su uso en ESX, ESXi y vSphere deben ser
convertido usando [VMware
converter](http://www.vmware.com/products/converter/).</target>
      </trans-unit>
      <trans-unit id="7d1c1440" xml:space="preserve">
        <source>##### - HDD/flash image (raw)

    dd if=./zabbix_appliance_5.2.0.raw of=/dev/sdc bs=4k conv=fdatasync

Replace */dev/sdc* with your Flash/HDD disk device.</source>
        <target state="needs-translation">##### - Imagen HDD/flash (sin procesar)

    dd if=./zabbix_appliance_5.2.0.raw of=/dev/sdc bs=4k conv=fdatasync

Reemplace */dev/sdc* con su dispositivo de disco Flash/HDD.</target>
      </trans-unit>
    </body>
  </file>
</xliff>
