[comment]: # translation:outdated

[comment]: # ({cfd53080-061a30b2})
#2 Inventario

[comment]: # ({/cfd53080-061a30b2})

[comment]: # ({ecaf5917-4b8ec855})
#### Descripción general

Puede mantener el inventario de dispositivos en red en Zabbix.

Hay un menú especial de *Inventario* en la interfaz de Zabbix. Sin embargo, usted
no verá ningún dato allí inicialmente y no es donde ingresa
datos. La creación de datos de inventario se realiza manualmente al configurar un host
o automáticamente mediante el uso de algunas opciones de llenado automático.

[comment]: # ({/ecaf5917-4b8ec855})

[comment]: # ({a1cac8bb-69c69326})
#### Construyendo un inventario

[comment]: # ({/a1cac8bb-69c69326})

[comment]: # ({7a81a30d-9a68f9a7})
##### Modo manual

Al [configurar un host](host), en la pestaña *Inventario de equipo* puede
ingresar detalles tales como el tipo de dispositivo, número de serie, ubicación,
persona responsable, etc. - datos que completarán la información del inventario.

Si se incluye una URL en la información del inventario del equipo y comienza con
'http' o 'https', dará como resultado un enlace en el que se puede hacer clic en la sección *Inventario*.

[comment]: # ({/7a81a30d-9a68f9a7})

[comment]: # ({93cbca2a-a9359c4e})
##### Modo automático

El inventario de equipo también se puede completar automáticamente. Para que eso funcione,
al configurar un host, el modo de inventario en la pestaña *Inventario de host*
debe establecerse en *Automático*.

Luego puede [configurar los elementos del host](/manual/config/items/item) para
rellenar cualquier campo de inventario de host con su valor, indicando el
campo de destino con el atributo respectivo (llamado *Item will
rellenar el campo de inventario de host*) en la configuración del artículo.

Elementos que son especialmente útiles para la colección de datos de inventario automatizados:

- system.hw.chassis\[full|type|vendor|model|serial\] - el valor predeterminado es
    \[completo\], se necesitan permisos de root
- system.hw.cpu\[all|cpunum,full|maxfreq|vendor|model|curfreq\] -
    el valor predeterminado es \[todo, completo\]
- system.hw.devices\[pci|usb\] - el valor predeterminado es \[pci\]
- system.hw.macaddr\[interface,short|full\] - el valor predeterminado es \[all,full\],
    la interfaz es expresión regular
- sistema.sw.arch
- system.sw.os\[nombre|corto|completo\] - el valor predeterminado es \[nombre\]
- system.sw.packages\[paquete,gestor,corto|completo\] - el valor predeterminado es
    \[todos,todos,completos\], el paquete es expresión regular

[comment]: # ({/93cbca2a-a9359c4e})

[comment]: # ({8aeb70c1-84351dfb})
##### Selección del modo de inventario

El modo de inventario se puede seleccionar en el formulario de configuración del host.

El modo de inventario de forma predeterminada para nuevos hosts se selecciona en función de la
Configuración de *Modo de inventario de host predeterminado* en *Administración* → *General* →
*[Otro](/manual/web_interface/frontend_sections/administration/general#other_parameters)*.

Para hosts agregados por descubrimiento de red o acciones de autorregistro, es
es posible definir una operación *Establecer modo de inventario de host* seleccionando
Modo manual o automático. Esta operación anula el *Host predeterminado
configuración del modo de inventario*.

[comment]: # ({/8aeb70c1-84351dfb})

[comment]: # ({8dc73da6-8291099a})
#### Resumen de inventario

Los detalles de todos los datos de inventario existentes están disponibles en el
Menú *Inventario*.

En *Inventario → Resumen* puede obtener un conteo de equipos por varios campos de
el inventario.

En *Inventario → Equipos* puede ver todos los equipos que tienen información de inventario. Al hacer clic en el nombre del host, se revelarán los detalles del inventario.
en un formulario.

![](../../../../assets/en/manual/web_interface/inventory_host.png){width="600"}

La pestaña **Resumen** muestra:

|Parámetro|<|<|<|<|Descripción|
|---------|-|-|-|-|-----------|
|*Nombre de host*|<|<|<|<|Nombre del host.<br>Al hacer clic en el nombre, se abre un menú con los scripts definidos para el host.<br>El nombre del host se muestra con un icono naranja, si el host esta en mantenimiento.|
|*Nombre visible*|<|<|<|<|Nombre visible del host (si está definido).|
|*Host (Agente, SNMP, JMX, IPMI)<br>interfaces*|<|<|<|<|Este bloque proporciona detalles de las interfaces configuradas para el host.|
|*OS*|<|<|<|<|Campo de inventario del sistema operativo del host (si está definido).|
|*Hardware*|<|<|<|<|Campo de inventario de hardware del host (si está definido).|
|*Software*|<|<|<|<|Campo de inventario de software de host (si está definido).|
|*Descripción*|<|<|<|<|Descripción del host.|
|*Monitoreo*|<|<|<|<|Enlaces a secciones de monitoreo con datos para este host: *Web*, *Últimos datos*, *Problemas*, *Gráficos*, *Paneles*.|
|*Configuración*|<|<|<|<|Vínculos a secciones de configuración para este host: *Host*, *Aplicaciones*, *Elementos*, *Activadores*, *Gráficos*, *Descubrimiento*, *Web*.< br>La cantidad de entidades configuradas se muestra entre paréntesis después de cada enlace.|

La pestaña **Detalles** muestra todos los campos de inventario que se completan (están
no vacíos).

[comment]: # ({/8dc73da6-8291099a})

[comment]: # ({82032414-6a9ca6a1})
#### Macros de inventario

Hay macros de inventario de host {INVENTARIO.\*} disponibles para su uso en
notificaciones, por ejemplo:

"El servidor en {INVENTORY.LOCATION1} tiene un problema, la persona responsable es
{INVENTARIO.CONTACTO1}, número de teléfono {INVENTARIO.POC.PRIMARY.TELÉFONO.A1}".

Para obtener más detalles, consulte [supported
macro](/manual/apéndice/macros/compatible_por_ubicación).

[comment]: # ({/82032414-6a9ca6a1})
