[comment]: # translation:outdated

[comment]: # ({5bfb89bf-1da35dcd})
#1 Configuración de un equipo

[comment]: # ({/5bfb89bf-1da35dcd})

[comment]: # ({b05d31f4-0ee38f7e})
#### Descripción general

Para configurar un equipo en la interfaz de Zabbix, haga lo siguiente:

- Vaya a: *Configuración → Equipo* o *Monitoreo → Equipo*
- Haga clic en *Crear equipo* a la derecha (o en el nombre del equipo para editar un
    equipo existente)
- Ingrese los parámetros del equipo en el formulario

También puede usar los botones *Clonar* y *Clonar completo* en formulario del
equipo existente para crear un nuevo equipo. Al hacer clic en *Clonar* se conservarán todos
parámetros del equipo y enlace de la plantilla (manteniendo todas las entidades de esas
plantillas). *Clon completo* también se mantendrá directamente conectado las 
entidades (aplicaciones, elementos, disparadores, gráficos, descubrimiento de bajo nivel
reglas y escenarios web).

*Nota*: Cuando se clona un equipo, conservará todas las entidades de la plantilla como
están en el equipo original. Cualquier cambio en esas entidades realizado
en el nivel de equipo existente (como intervalo de artículo cambiado, expresión regular modificada o prototipos agregados a la regla de descubrimiento de bajo nivel) no se clonará en el nuevo equipo; en cambio, serán como en la plantilla.

[comment]: # ({/b05d31f4-0ee38f7e})

[comment]: # ({435ccf5d-e2f56cfc})
#### Configuración

La pestaña **Equipo** contiene atributos generales de equipo:

![](../../../../assets/en/manual/config/host_a.png)

Todos los campos de entrada obligatorios están marcados con un asterisco rojo.

|Parámetro|<|Descripción|
|---------|-|-----------|
|*Nombre de equipo*|<|Ingrese un nombre único de equipo. Se permiten caracteres alfanuméricos, espacios, puntos, guiones y guiones bajos. Sin embargo, los espacios iniciales y finales no están permitidos.<br>*Nota:* Con el agente Zabbix ejecutándose en el equipo que está configurando, el [archivo de configuración](/manual/appendix/config/zabbix_agentd) parámetro *Hostname* del agente debe tener el mismo valor que el nombre de host ingresado aquí. El nombre en el parámetro es necesario en el procesamiento de [cheques activos](/manual/appendix/items/activepassive).|
|*Nombre visible*|<|Ingrese un nombre visible único para el equipo. Si establece este nombre, será el visible en listas, mapas, etc. en lugar del nombre técnico del equipo. Este atributo es compatible con UTF-8.|
|*Plantillas*|<|Enlace [plantillas](/manual/config/templates) al equipo. Todas las entidades (elementos, disparadores, gráficos, etc.) se heredarán de la plantilla.<br>Para vincular una nueva plantilla, comience a escribir el nombre de la plantilla en el campo *Vincular nuevas plantillas*. Aparecerá una lista de plantillas coincidentes; desplácese hacia abajo para seleccionar. Alternativamente, puede hacer clic en *Seleccionar* junto al campo y seleccionar plantillas de la lista en una ventana emergente. Las plantillas que se seleccionen en el campo *Vincular nuevas plantillas* se vincularán al host cuando se guarde o actualice el formulario de configuración del host.<br>Para desvincular una plantilla, utilice una de las dos opciones en el bloque *Plantillas vinculadas* :<br>*Desvincular*: desvincular la plantilla, pero conservar sus elementos, activadores y gráficos<br>*Desvincular y borrar*: desvincular la plantilla y eliminar todos sus elementos, activadores y gráficos<br>Los nombres de plantillas enumerados son enlaces en los que se puede hacer clic que lleva al formulario de configuración de la plantilla.|
|*Grupos*|<|Seleccione los grupos de host a los que pertenece el host. Un equipo debe pertenecer al menos a un grupo de equipos. Se puede crear un nuevo grupo y vincularlo al grupo de equipos agregando un nombre de grupo que no existe.|
|*Interfaces*|<|Se admiten varios tipos de interfaz de equipo para un equipo: *Agent*, *SNMP*, *JMX* e *IPMI*.<br>No hay interfaces definidas de forma predeterminada. Para agregar una nueva interfaz, haga clic en *Agregar* en el bloque *Interfaces*, seleccione el tipo de interfaz e ingrese *IP/DNS*, *Conectar a* e Información de *Puerto*.<br>*Nota:* Las interfaces que son utilizados en cualquier elemento no se pueden eliminar y el enlace *Eliminar* está atenuado para ellos.<br>Consulte [Configuración de la supervisión SNMP](/manual/config/items/itemtypes/snmp#configuring_snmp_monitoring) para obtener detalles adicionales sobre la configuración de una interfaz SNMP ( v1, v2 y v3).|
|<|*Dirección IP*|Dirección IP del equipo (opcional).|
|^|*Nombre DNS*|Nombre DNS del equipo (opcional).|
|^|*Conectarse a*|Al hacer clic en el botón respectivo, se le indicará al servidor Zabbix qué usar para recuperar datos de los agentes:<br>**IP**: conectarse a la dirección IP del equipo (recomendado)<br>**DNS* * - Conéctese al nombre DNS del equipo|
|^|*Puerto*|Número de puerto TCP/UDP. Los valores predeterminados son: 10050 para el agente Zabbix, 161 para el agente SNMP, 12345 para JMX y 623 para IPMI.|
|^|*Predeterminado*|Marque el botón de opción para establecer la interfaz predeterminada.|
|*Descripción*|<|Introduzca la descripción del equipo.|
|*Supervisado por proxy*|<|El equipo puede ser monitoreado por el servidor Zabbix o uno de los proxies Zabbix:<br>**(sin proxy)** - el equipo es monitoreado por el servidor Zabbix<br>**Nombre del proxy* * - el equipo es monitoreado por el proxy Zabbix "Nombre del proxy" |
|*Habilitado*|<|Marque la casilla de verificación para que el equipo esté activo, listo para ser monitoreado. Si no se marca, el equipo no está activo y, por lo tanto, no se supervisa.|

La pestaña **IPMI** contiene atributos de administración de IPMI.

|Parámetro|Descripción|
|---------|-----------|
|*Algoritmo de autenticación*|Seleccione el algoritmo de autenticación.|
|*Nivel de privilegio*|Seleccione el nivel de privilegio.|
|*Username*|Nombre de usuario para autenticación. Se pueden utilizar macros de usuario.|
|*Contraseña*|Contraseña para autenticación. Se pueden utilizar macros de usuario.|

La pestaña **Etiquetas** le permite definir el nivel de equipo
[etiquetas](/manual/config/tagging). Todos los problemas de este equipo serán etiquetados
con los valores ingresados aquí.

![](../../../../assets/en/manual/config/host_d.png)

Macros de usuario, {INVENTORY.\*} macros, {HOST.HOST}, {HOST.NAME},
Las macros {HOST.CONN}, {HOST.DNS}, {HOST.IP}, {HOST.PORT} y {HOST.ID} son
compatible con etiquetas.

La pestaña **Macros** le permite definir el nivel de equipo [usuario
macros](/manual/config/macros/user_macros) como pares de nombre y valor. Note
que los valores de macro se pueden mantener como texto sin formato, texto secreto o Vault
secreto. También se admite agregar una descripción.

![](../../../../assets/en/manual/config/host_e.png)

También puede ver aquí macros de usuario globales y a nivel de plantilla si
selecciona la opción *Macros heredados y del equipo*. Ahí es donde se define
todas las macros de usuario para el equipo que se muestran con el valor al que se resuelven 
así como su origen.

![](../../../../assets/en/manual/config/host_e2.png){ancho="600"}

Para mayor comodidad, los enlaces a plantillas respectivas y macro global
se proporciona la configuración. También es posible editar un
plantilla/macro global a nivel de equipo, creando efectivamente una copia de
la macro en el equipo.

La pestaña **Inventario de equipo** le permite ingresar manualmente
[inventario](inventario) información para el equipo. También puede seleccionar para que
habilite el llenado de inventario *Automático* o deshabilite el llenado de inventario
para este equipo.

![](../../../../assets/en/manual/config/host_f.png)

Si el inventario está habilitado (manual o automático), se muestra un punto verde
con el nombre de la pestaña.

[comment]: # ({/435ccf5d-e2f56cfc})

[comment]: # ({ac6f1280-6875b717})
##### Cifrado

La pestaña **Cifrado** le permite solicitar
Conexiones [cifradas](/manual/cifrado) con el host.

|Parámetro|Descripción|
|---------|-----------|
|*Conexiones al host*|Cómo se conecta el servidor Zabbix o el proxy al agente Zabbix en un equipo: sin cifrado (predeterminado), usando PSK (clave precompartida) o certificado.|
|*Conexiones desde el equipo*|Seleccione qué tipo de conexiones se permiten desde el equipo (es decir, desde el agente de Zabbix y el remitente de Zabbix). Se pueden seleccionar varios tipos de conexión al mismo tiempo (útil para probar y cambiar a otro tipo de conexión). El valor predeterminado es "Sin cifrado".|
|*Emisor*|Emisor permitido del certificado. El certificado se valida primero con CA (autoridad de certificación). Si es válido, firmado por la CA, entonces el campo *Emisor* se puede usar para restringir aún más la CA permitida. Este campo está diseñado para usarse si su instalación de Zabbix usa certificados de varias CA. Si este campo está vacío, se acepta cualquier CA.|
|*Asunto*|Asunto permitido del certificado. El certificado se valida primero con CA. Si es válido, firmado por la CA, entonces el campo *Asunto* se puede usar para permitir solo un valor de la cadena *Asunto*. Si este campo está vacío, se acepta cualquier certificado válido firmado por la CA configurada.|
|*Identidad de PSK*|Cadena de identidad de clave precompartida.<br>No coloque información confidencial en la identidad de PSK, se transmite sin cifrar a través de la red para informar al receptor qué PSK debe usar.|
|*PSK*|Clave previamente compartida (cadena hexadecimal). Longitud máxima: 512 dígitos hexadecimales (PSK de 256 bytes) si Zabbix usa la biblioteca GnuTLS o OpenSSL, 64 dígitos hexadecimales (PSK de 32 bytes) si Zabbix usa la biblioteca mbed TLS (PolarSSL). Ejemplo: 1f87b595725ac58dd977beef14b97461a7c1045b9a1c963065002c5473194952|

[comment]: # ({/ac6f1280-6875b717})

[comment]: # ({01fcba5d-513311ca})
##### Asignación de valores

La pestaña **Asignación de valores** permite configurar la 
representación de los datos del artículo en [valor
asignaciones](/manual/config/items/mapping).

[comment]: # ({/01fcba5d-513311ca})

[comment]: # ({new-7cc0883e})
#### Creating a host group

::: noteimportant
Only Super Admin users can create host groups.
:::

To create a host group in Zabbix frontend, do the following:

-   Go to: *Configuration → Host groups*
-   Click on *Create host group* in the upper right corner of the screen
-   Enter the group name in the form

![](../../../../assets/en/manual/config/host_group.png)

To create a nested host group, use the '/' forward slash separator, for example `Europe/Latvia/Riga/Zabbix servers`. You can create this group even if none of the three parent host groups (`Europe/Latvia/Riga/`) exist. In this case creating these parent host groups is up to the user; they will not be created automatically.<br>Leading and trailing slashes, several slashes in a row are not allowed. Escaping of '/' is not supported.

[comment]: # ({/new-7cc0883e})

[comment]: # ({new-22cf222a})

Once the group is created, you can click on the group name in the list to edit group name, clone the group or set additional option:

![](../../../../assets/en/manual/config/host_group2.png)

*Apply permissions and tag filters to all subgroups* - mark this checkbox and click on *Update* to apply the same level of permissions/tag filters to all nested host groups. For user groups that may have had differing [permissions](/manual/config/users_and_usergroups/usergroup#configuration) assigned to nested host groups, the permission level of the parent host group will be enforced on the nested groups. This is a one-time option that is not saved in the database.

**Permissions to nested host groups**

-   When creating a child host group to an existing parent host group,
    [user group](/manual/config/users_and_usergroups/usergroup)
    permissions to the child are inherited from the parent (for example,
    when creating `Riga/Zabbix servers` if `Riga` already exists)
-   When creating a parent host group to an existing child host group,
    no permissions to the parent are set (for example, when creating
    `Riga` if `Riga/Zabbix servers` already exists)

[comment]: # ({/new-22cf222a})


