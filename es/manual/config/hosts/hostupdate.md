[comment]: # translation:outdated

[comment]: # ({12ab0ad4-1c6301ba})
# 3 Actualización de masa

[comment]: # ({/12ab0ad4-1c6301ba})

[comment]: # ({5801025a-7205743f})
#### Visión general

A veces puede querer cambiar algún atributo para un número de hosts a la vez. En lugar de abrir cada uno de los hosts para editarlos, puedes usar la función de actualización masiva para ello.

[comment]: # ({/5801025a-7205743f})

[comment]: # ({b924a398-929367ac})
#### Utilizando la actualización de la masa

Para actualizar en masa algunos hosts, haga lo siguiente:

- · Marque las casillas de verificación antes de los hosts que desea actualizar en la [lista de hosts] (/manual/web_interface/frontend_sections/configuration/hosts)
- · Haga clic en *Actualización masiva* debajo de la lista
- · Navegue hasta la pestaña con los atributos requeridos (*Host*, *IPMI*, *Etiquetas*, *Macros*, *Inventario*, *Encryption* o *Value mapping*)
- · Marque las casillas de verificación de cualquier atributo que desee actualizar e introduzca un nuevo valor
- · para ellos

![](../../../../assets/en/manual/config/hosts/host_mass.png)

Al seleccionar el botón correspondiente, se dispone de las siguientes opciones para la actualización de la vinculación de **plantillas**:

- · *Enlazar* - especificar qué plantillas adicionales enlazar
- · *Reemplazar* - especificar qué plantillas vincular mientras se desvincula cualquier plantilla que estaba vinculada a los hosts anteriores.
- · *Desenlazar* - especificar qué plantillas desenlazar

Para especificar las plantillas a enlazar/desenlazar comience a escribir el nombre de la plantilla en el campo de autocompletar hasta que aparezca un menú desplegable que ofrece las plantillas correspondientes. Desplácese hacia abajo para seleccionar la plantilla deseada.

La opción *Borrar al desvincular* permitirá no sólo desvincular cualquier plantilla previamente vinculada, sino también eliminar todos los elementos heredados de ellos (elementos, activadores, etc.).

Las siguientes opciones están disponibles al seleccionar el botón respectivo para la actualización del **grupo de hosts**:

- · *Añadir* - permite especificar grupos de hosts adicionales a partir de los existentes o introducir grupos de hosts completamente nuevos para los hosts
- · *Reemplazar* - eliminará el host de cualquier grupo de hosts existente y los sustituye por los especificados en este campo (grupos de hosts existentes o nuevos grupos de hosts)
- · *Remove* - eliminará grupos de hosts específicos de los hosts

Estos campos se autocompletan - al empezar a escribir en ellos se ofrece un menú desplegable de grupos de hosts que coinciden. Si el grupo de hosts es nuevo, también aparece en el desplegable y se indica con *(nuevo)* después de la cadena.

Simplemente desplázate hacia abajo para seleccionar.

![](../../../../assets/en/manual/config/hosts/host_mass_c.png)

![](../../../../activos/es/manual/config/hosts/host_mass_d.png)

Macros de usuario, macros {INVENTORY.\N*}, {HOST.HOST}, {HOST.NAME}, {HOST.CONN}, {HOST.DNS}, {HOST.IP}, {HOST.PORT} y {HOST.ID} son soportadas en las etiquetas. Tenga en cuenta que las etiquetas con el mismo nombre, pero diferentes
valores diferentes no se consideran "duplicados" y pueden añadirse al mismo host.

![](../../../../assets/es/manual/config/hosts/host_mass_e.png)

Al seleccionar el botón correspondiente, se dispone de las siguientes opciones para la actualización de las macros:
- · *Añadir* - permite especificar macros de usuario adicionales para los hosts. 
- · *Actualizar las existentes* está marcada, el valor, el tipo y la descripción para el nombre de la macro especificada se actualizarán. Si no está marcada, si una macro con ese nombre ya existe en el(los) host(es), no será actualizada.
- · *Actualizar* - reemplazará los valores, tipos y descripciones de las macros especificadas en esta lista. Si se marca la casilla *Agregar faltantes*, las macros que no existían previamente en un host se añadirán como nuevas macros.
- · Si la casilla no está marcada, sólo se actualizarán las macros que ya existen en un host actualizadas.
- · *Remover* - eliminará las macros especificadas de los hosts. Si la casilla *Excepto seleccionada* está marcada, todas las macros excepto las especificadas en la lista serán eliminadas. Si no está marcada, sólo se eliminarán las macros especificadas en la lista.
- · *Eliminar todo* - eliminará todas las macros de usuario de los hosts. Si *confirmo para eliminar todas las macros* no está marcada, se abrirá una nueva ventana emergente pidiendo que se confirme la eliminación de todas las macros.

![](../../../../assets/en/manual/config/hosts/host_mass_f.png)

Para poder actualizar en masa los campos del inventario, el *Modo de inventario* debe estar configurado como 'Manual' o 'Automático'.

![](../../../../assets/en/manual/config/hosts/host_mass_g.png)
![](../../../../assets/en/manual/config/hosts/host_mass_h.png)

Los botones con las siguientes opciones están disponibles para la actualización del mapa de valores:

- ·  *Añadir* - añadir mapas de valores a los hosts. Si marca *Actualizar existente* se actualizarán todas las propiedades del mapa de valores con ese nombre. En caso contrario, si ya existe un mapa de valores con ese nombre, no se actualizará.
- · *Actualizar*: actualiza los mapas de valores existentes. Si se marca *Añadir falta*, se actualizará un mapa de valores que no existía previamente en un host se añadirá como un nuevo mapa de valores. En caso contrario, sólo se actualizarán los mapas de valores que ya existen en un host.
- · *Rename* - dar un nuevo nombre a un mapa de valores existente
- · *Remove* - eliminar los mapas de valores especificados de los hosts. Si usted marca *Excepto seleccionado*, se eliminarán todos los mapas de valores **excepto*** los especificados.
- · *Remove all* - eliminar todos los mapas de valores de los hosts. Si * confirmar la eliminación de todos los mapas de valores* no está marcada, se abrirá una nueva ventana emergente pidiendo que se confirme la eliminación.
Cuando haya terminado con todos los cambios necesarios, haga clic en *Actualizar*. Los atributos se actualizarán en consecuencia para todos los hosts seleccionados.

[comment]: # ({/b924a398-929367ac})
