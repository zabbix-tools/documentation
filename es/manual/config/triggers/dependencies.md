[comment]: # translation:outdated

[comment]: # ({d5686fc3-d5d0048d})
# 3 Dependencias de Trigger

[comment]: # ({/d5686fc3-d5d0048d})

[comment]: # ({e0ee4b5e-26bda053})
#### Visión general

A veces la disponibilidad de un servidor depende de otro. Un servidor que está detrás de algún router se volverá inalcanzable si el router se cae.
Con disparadores configurados para ambos, podrías recibir notificaciones sobre dos hosts caídos - mientras que sólo el router era el culpable.
Aquí es donde alguna dependencia entre hosts podría ser útil. Con dependencia establecida las notificaciones de los dependientes podrían ser retenidas y sólo la notificación para el problema de raíz enviada.
Aunque Zabbix no soporta dependencias entre hosts directamente, éstas pueden ser definidas con otro método más flexible - trigger
dependencias. Un trigger puede tener uno o más triggers de los que depende.

Así que en nuestro sencillo ejemplo abrimos el formulario de configuración del trigger del servidor y establecemos que depende del respectivo trigger del router. Con tal dependencia el trigger del servidor no cambiará de estado mientras esté en estado "PROBLEMA" - y por lo tanto no se tomarán acciones dependientes y no se enviarán notificaciones. Si tanto el servidor como el router están caídos y la dependencia está ahí, Zabbix no ejecutará acciones para el trigger dependiente.
Las acciones de los disparadores dependientes no se ejecutarán si el disparador del que dependen:
- · cambia su estado de 'PROBLEMA' a 'DESCONOCIDO'
- · se cierra manualmente, por correlación o con la ayuda de funciones basadas en el tiempo
- · se resuelve mediante un valor de un elemento no implicado en el desencadenante dependiente-
- · está desactivado, tiene un elemento desactivado o un host de elementos desactivados
Tenga en cuenta que el activador "secundario" (dependiente) en los casos mencionados no se actualizará inmediatamente. Mientras el disparador principal esté en estado PROBLEMA sus dependientes pueden informar de valores en los que no podemos confiar. Por lo tanto,  el disparador dependiente sólo será reevaluado, y cambiará su estado, sólo después de que el disparador padre esté en estado OK y hayamos recibido métricas confiables.

También:

- · La dependencia del trigger puede ser añadida desde cualquier trigger host a cualquier otro - de host, siempre y cuando no resulte en una dependencia circular.
- · La dependencia de un activador puede añadirse de una plantilla a otra plantilla. Si un disparador de la plantilla A depende de un disparador de la plantilla B, la plantilla A sólo puede estar vinculada a un host (u otra plantilla) junto con la plantilla B, pero la plantilla B puede estar vinculada a un host (o otra plantilla) sola.
- · La dependencia del trigger puede ser añadida desde el trigger de la plantilla a un host desencadenante. En este caso, la vinculación de dicha plantilla a un host creará un trigger de host que depende del mismo trigger de plantilla del que estaba.
- · Esto permite, por ejemplo, tener una plantilla donde algunos activadores dependen de activadores del router (host). Todos los hosts vinculados a esta plantilla dependerán de ese router específico.
- · La dependencia de un disparador de host a un disparador de plantilla no puede ser añadida.
- · La dependencia del trigger de un prototipo de trigger a otro prototipo de trigger (dentro de la misma plantilla).
- · prototipo de trigger (dentro de la misma regla de descubrimiento de bajo nivel) o un disparador real. Un prototipo de trigger no puede depender de un prototipo de trigger de una regla LLD diferente o de un trigger creado a partir de prototipo de trigger. Un prototipo de trigger no puede depender de un trigger de una plantilla.

[comment]: # ({/e0ee4b5e-26bda053})

[comment]: # ({d1d03994-c4578b73})
#### Configuración

Para definir una dependencia, abra la pestaña Dependencias en un trigger [formulario de configuración](trigger#configuration). Haga clic en *Add* en el bloque Dependencias' y seleccione uno o más disparadores de los que dependerá nuestro disparador.

![](../../../../assets/en/manual/config/triggers/dependency.png)

Haga clic en *Actualizar*. Ahora el activador tiene una indicación de su dependencia en la lista.

(../../../../assets/en/manual/config/triggers/dependency_list.png)

[comment]: # ({/d1d03994-c4578b73})

[comment]: # ({f5ba0919-4dc2ce6b})
##### Ejemplo de varias dependencias

Por ejemplo, un Host está detrás de un Router2 y el Router2 está detrás de un Router1.

 - Zabbix - Router1 - Router2 - Host

Si el Router1 está caído, entonces obviamente el Host y el Router2 también están inalcanzables, sin embargo, no queremos recibir tres notificaciones sobre Host, Router1 y Router2 están caídos.

Así que en este caso definimos dos dependencias:

 · El disparador 'Host is down' depende del disparador 'Router2 is down'
 · El disparador "Router2 está caído" depende del disparador "Router1 está caído".

Antes de cambiar el estado del trigger 'Host is down', Zabbix buscará las dependencias de los disparos correspondientes. Si se encuentra, y uno de esos disparadores está en estado 'Problema', entonces el estado del disparador no se cambiará y por lo tanto las acciones no se ejecutarán y las notificaciones no se enviarán.

Zabbix realiza esta comprobación de forma recursiva. Si el Router1 o el Router2 es inalcanzable, el trigger Host no se actualizará.

[comment]: # ({/f5ba0919-4dc2ce6b})
