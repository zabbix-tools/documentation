[comment]: # translation:outdated

[comment]: # ({d78eff4f-29480506})
#1 Configurando un disparador

[comment]: # ({/d78eff4f-29480506})

[comment]: # ({d1d86140-ea19cfe1})
#### Descripción general

Para configurar un disparador, haga lo siguiente:

- Ir a: *Configuración* → *Equipo*
- Haga clic en *Disparadores* en la fila del equipo
- Haga clic en *Crear disparador* a la derecha (o en el nombre del disparador para
    editar un disparador existente)
- Ingrese los parámetros del disparador en el formulario

Ver también [información general](/manual/config/triggers) sobre disparadores y
sus tiempos de cálculo.

[comment]: # ({/d1d86140-ea19cfe1})

[comment]: # ({c97701b1-f29d794b})
#### Configuración

La pestaña **Disparador** contiene todos los atributos de activación esenciales.

![](../../../../assets/en/manual/config/triggers/trigger.png)

Todos los campos de entrada obligatorios están marcados con un asterisco rojo.

|Parámetro|Descripción|
|---------|-----------|
|*Nombre*|Nombre del activador.<br>Las [macros] (/manual/appendix/macros/supported_by_location) admitidas son: {HOST.HOST}, {HOST.NAME}, {HOST.PORT}, {HOST.CONN} , {HOST.DNS}, {HOST.IP}, {ITEM.VALUE}, {ITEM.LASTVALUE}, {ITEM.LOG.\*} y {$MACRO} macros de usuario.<br>**$1, $2. ..$9** macros se pueden usar para referirse a la primera, segunda...novena constante de la expresión.<br>*Nota*: Las macros $1-$9 se resolverán correctamente si se refieren a constantes en expresiones relativamente simples y directas. Por ejemplo, el nombre "Carga del procesador superior a $1 en {HOST.NOMBRE}" cambiará automáticamente a "Carga del procesador superior a 5 en el nuevo host" si la expresión es last(/New host/system.cpu.load\[percpu,avg1 \])>5|
|*Nombre del evento*|Si se define, este nombre se usará para crear el nombre del evento del problema, en lugar del nombre del activador.<br>El nombre del evento se puede usar para crear alertas significativas que contengan datos del problema (consulte [ejemplo](/ manual/config/triggers/expression#example_17)).<br>Se admite el mismo conjunto de macros que en el nombre del activador, más macros de expresión {TIME} y {?EXPRESSION}.<br>Compatible desde Zabbix 5.2.0. |
|*Datos operativos*|Los datos operativos permiten definir cadenas arbitrarias junto con macros. Las macros se resolverán dinámicamente en datos en tiempo real en *Monitoring* → *[Problems](/manual/web_interface/frontend_sections/monitoring/problems)*. Mientras que las macros en el nombre del activador (ver arriba) se resolverán en sus valores en el momento en que ocurre un problema y se convertirán en la base de un nombre de problema estático, las macros en los datos operativos mantienen la capacidad de mostrar la información más reciente de forma dinámica. <br>Se admite el mismo conjunto de macros que en el nombre del disparador.|
|*Gravedad*|Establezca el disparador requerido [gravedad](gravedad) haciendo clic en los botones.|
|*Expresión*|[expresión](expresión) lógica utilizada para definir las condiciones de un problema.<br>Un problema se crea después de que se cumplen todas las condiciones incluidas en la expresión, es decir, la expresión se evalúa como VERDADERO. El problema se resolverá tan pronto como la expresión se evalúe como FALSO, a menos que se especifiquen condiciones de recuperación adicionales en *Expresión de recuperación*.|
|*Generación de eventos OK*|Opciones de generación de eventos OK:<br>**Expresión**: los eventos OK se generan en función de la misma expresión que los eventos problemáticos;<br>**Expresión de recuperación**: los eventos OK se generan si la expresión del problema se evalúa como FALSO y la expresión de recuperación se evalúa como VERDADERO;<br>**Ninguno**: en este caso, el disparador nunca volverá a un estado correcto por sí solo.|
|*Expresión de recuperación*|[expresión](expresión) lógica (opcional) que define condiciones adicionales que deben cumplirse antes de que se resuelva el problema, después de que la expresión del problema original ya haya sido evaluada como FALSA.<br>La expresión de recuperación es útil para desencadenar [histéresis](/manual/config/triggers/expression#hysteresis). **No** es posible resolver un problema solo con la expresión de recuperación si la expresión del problema sigue siendo VERDADERA.<br>Este campo solo está disponible si se selecciona 'Expresión de recuperación' para *Generación de evento OK*.|
|*Modo de generación de eventos PROBLEMA*|Modo para generar eventos problemáticos:<br>**Único**: se genera un evento único cuando un activador pasa al estado 'Problema' por primera vez;<br>**Múltiple* * - se genera un evento en *cada* evaluación de 'Problema' del activador.|
|*Se cierra el evento OK*|Seleccione si se cierra el evento OK:<br>**Todos los problemas**: todos los problemas de este activador<br>**Todos los problemas si los valores de la etiqueta coinciden**: solo los que desencadenan problemas con la etiqueta de evento coincidente valores|
|*Etiqueta para coincidencia*|Ingrese el nombre de la etiqueta de evento que se usará para la correlación de eventos.<br>Este campo se muestra si se selecciona 'Todos los problemas si los valores de etiqueta coinciden' para la propiedad *OK cierres de eventos* y es obligatorio en este caso. |
|*Permitir cierre manual*|Marque para permitir [cierre manual](/manual/config/events/manual_close) de eventos problemáticos generados por este disparador. El cierre manual es posible cuando se confirman eventos problemáticos.|
|*URL*|Si no está vacío, la URL ingresada aquí está disponible como un enlace en varias ubicaciones de interfaz, p. al hacer clic en el nombre del problema en *Supervisión → Problemas* (opción *URL* en el menú *Activador*) y el widget del panel de *Problemas*.<br>Se admite el mismo conjunto de macros que en el nombre del activador, más {EVENTO .ID}, {HOST.ID} y {TRIGGER.ID}. Tenga en cuenta que las macros de usuario con valores secretos no se resolverán en la URL.|
|*Descripción*|Campo de texto utilizado para proporcionar más información sobre este disparador. Puede contener instrucciones para solucionar un problema específico, detalles de contacto del personal responsable, etc.<br>Se admite el mismo conjunto de macros que en el nombre del disparador.|
|*Habilitado*|Al desmarcar esta casilla se deshabilitará el disparador si es necesario.<br>Los problemas de un disparador deshabilitado ya no se muestran en la interfaz, pero no se eliminan.|

La pestaña **Etiquetas** le permite definir el nivel de activación
[etiquetas](/manual/config/tagging). Todos los problemas de este disparador serán
etiquetados con los valores introducidos aquí.

![](../../../../assets/en/manual/config/triggers/trigger_b.png)

Además, la opción *Etiquetas heredadas y desencadenantes* permite ver las etiquetas
definido en el nivel de plantilla, si el disparador proviene de esa plantilla. Si
hay varias plantillas con la misma etiqueta, estas etiquetas se muestran
una vez y los nombres de las plantillas se separan con comas. Un disparador no
"hereda" y muestra etiquetas a nivel de equipo.

|Parámetro|Descripción|
|---------|-----------|
|*Nombre/Valor*|Establezca etiquetas personalizadas para marcar eventos desencadenantes.<br>Las etiquetas son un par de nombre y valor de etiqueta. Puede usar solo el nombre o emparejarlo con un valor. Un activador puede tener varias etiquetas con el mismo nombre, pero valores diferentes.<br>Macros de usuario, contexto de macro de usuario, macros de descubrimiento de bajo nivel y macro [funciones] (/manual/config/macros/macro_functions) con `{{ITEM .VALUE}}`, `{{ITEM.LASTVALUE}}` y las macros de descubrimiento de bajo nivel son compatibles con las etiquetas de eventos. Las macros de descubrimiento de bajo nivel se pueden usar dentro del contexto de la macro.<br>La macro {TRIGGER.ID} es compatible con los valores de las etiquetas de activación. Puede ser útil para identificar disparadores creados a partir de prototipos de disparadores y, por ejemplo, suprimir problemas de estos disparadores durante el mantenimiento.<br>Si la longitud total del valor expandido supera los 255, se reducirá a 255 caracteres.<br>Ver todo [macros](/manual/config/tagging#macro_support) compatibles con etiquetas de eventos.<br>[Etiquetas de eventos](/manual/config/tagging) se pueden usar para la correlación de eventos, en condiciones de acción y también se verán en * Supervisión* → *Problemas* o el widget *Problemas*.|

La pestaña **Dependencias** contiene todas las [dependencias](dependencias)
del disparador

Haga clic en *Agregar* para agregar una nueva dependencia.

::: notaclásica
También puede configurar un disparador abriendo uno existente,
presionando el botón * Clonar * y luego guardando con un nombre diferente.
:::

[comment]: # ({/c97701b1-f29d794b})

[comment]: # ({1db8511d-dbfe2e70})
#### Expresiones de prueba

Es posible probar la expresión de activación configurada en cuanto a lo que
el resultado de la expresión dependería del valor recibido.

La siguiente expresión de una plantilla oficial se toma como
ejemplo:

    avg(/Cisco IOS SNMPv2/sensor.temp.value[ciscoEnvMonTemperatureValue.{#SNMPINDEX}],5m)>{$TEMP_WARN}
    o
    last(/Cisco IOS SNMPv2/sensor.temp.status[ciscoEnvMonTemperatureState.{#SNMPINDEX}])={$TEMP_WARN_STATUS}

Para probar la expresión, haga clic en *Constructor de expresiones* bajo el
campo de expresión.

![](../../../../assets/en/manual/config/triggers/trigger_test.png)

En el constructor de expresiones, se enumeran todas las expresiones individuales. Para
abrir la ventana de prueba, haga clic en *Prueba* debajo de la lista de expresiones.

![](../../../../assets/en/manual/config/triggers/expr_test_button.png){width="600"}

En la ventana de prueba, puede ingresar valores de muestra ('80', '70', '0', '1'
en este ejemplo) y luego ver el resultado de la expresión, haciendo clic en el
Botón *Prueba*.

![](../../../../assets/en/manual/config/triggers/expr_test.png){width="600"}

El resultado de las expresiones individuales así como la expresión completa
puede ser visto.

"VERDADERO" significa que la expresión especificada es correcta. En esto
caso particular A, "80" es mayor que el {$TEMP\_WARN} especificado
valor, "70" en este ejemplo. Como era de esperar, aparece un resultado "VERDADERO".

"FALSO" significa que la expresión especificada es incorrecta. En esto
caso particular B, {$TEMP\_WARN\_STATUS} "1" debe ser igual a
valor especificado, "0" en este ejemplo. Como era de esperar, un resultado "FALSO"
aparece

El tipo de expresión elegido es "OR". Si al menos uno de los especificados
condiciones (A o B en este caso) es VERDADERO, el resultado general será
VERDADERO también. Lo que significa que el valor actual excede el valor de advertencia
y ha ocurrido un problema.

[comment]: # ({/1db8511d-dbfe2e70})
