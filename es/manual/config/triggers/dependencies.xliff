<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="es" datatype="plaintext" original="manual/config/triggers/dependencies.md">
    <body>
      <trans-unit id="d5d0048d" xml:space="preserve">
        <source># 3 Trigger dependencies</source>
        <target state="needs-translation"># 3 Dependencias de Trigger</target>
      </trans-unit>
      <trans-unit id="26bda053" xml:space="preserve">
        <source>### Overview

Sometimes the availability of one host depends on another. A server that
is behind a router will become unreachable if the router goes down.
With triggers configured for both, you might get notifications about two
hosts down - while only the router was the guilty party.

This is where some dependency between hosts might be useful. With
dependency set, notifications of the dependents could be withheld and
only the notification on the root problem sent.

While Zabbix does not support dependencies between hosts directly, they
may be defined with another, more flexible method - trigger
dependencies. A trigger may have one or more triggers it depends on.

So in our simple example we open the server trigger configuration form
and set that it depends on the respective trigger of the router. With
such dependency, the server trigger will not change its state as long as the
trigger it depends on is in the 'PROBLEM' state - and thus no dependent
actions will be taken and no notifications sent.

If both the server and the router are down and dependency is there,
Zabbix will not execute actions for the dependent trigger.

While the parent trigger is in the PROBLEM state, its dependents may report values that cannot be trusted.
Therefore dependent triggers will not be re-evaluated until the parent trigger (the router in the example above):

-   goes back from 'PROBLEM' to 'OK' state;
-   changes its state from 'PROBLEM' to 'UNKNOWN';
-   is closed manually, by correlation or with the help of time-based functions;
-   is resolved by a value of an item not involved in the dependent trigger;
-   is disabled, has a disabled item or a disabled item host

In all of the cases mentioned above, the dependent trigger (server) will be re-evaluated only when a new metric for it is received.
This means that the dependent trigger may not be updated immediately.

Also:

-   Trigger dependency may be added from any host trigger to any other
    host trigger, as long as it doesn't result in a circular
    dependency.
-   Trigger dependency may be added from one template to another. If some
    trigger from template A depends on some trigger from template B,
    template A may only be linked to a host (or another template)
    together with template B, but template B may be linked to a host (or
    another template) alone.
-   Trigger dependency may be added from a template trigger to a host
    trigger. In this case, linking such a template to a host will create
    a host trigger that depends on the same trigger template that the trigger was
    depending on. This allows to, for example, have a template where
    some triggers depend on the router (host) triggers. All hosts linked to
    this template will depend on that specific router.
-   Trigger dependency may not be added from a host trigger to a template trigger.
-   Trigger dependency may be added from a trigger prototype to another
    trigger prototype (within the same low-level discovery rule) or a
    real trigger. A trigger prototype may not depend on a trigger
    prototype from a different LLD rule or on a trigger created from
    trigger prototype. A host trigger prototype cannot depend on a trigger
    from a template.</source>
        <target state="needs-translation">#### Visión general

A veces la disponibilidad de un servidor depende de otro. Un servidor que está detrás de algún router se volverá inalcanzable si el router se cae.
Con disparadores configurados para ambos, podrías recibir notificaciones sobre dos hosts caídos - mientras que sólo el router era el culpable.
Aquí es donde alguna dependencia entre hosts podría ser útil. Con dependencia establecida las notificaciones de los dependientes podrían ser retenidas y sólo la notificación para el problema de raíz enviada.
Aunque Zabbix no soporta dependencias entre hosts directamente, éstas pueden ser definidas con otro método más flexible - trigger
dependencias. Un trigger puede tener uno o más triggers de los que depende.

Así que en nuestro sencillo ejemplo abrimos el formulario de configuración del trigger del servidor y establecemos que depende del respectivo trigger del router. Con tal dependencia el trigger del servidor no cambiará de estado mientras esté en estado "PROBLEMA" - y por lo tanto no se tomarán acciones dependientes y no se enviarán notificaciones. Si tanto el servidor como el router están caídos y la dependencia está ahí, Zabbix no ejecutará acciones para el trigger dependiente.
Las acciones de los disparadores dependientes no se ejecutarán si el disparador del que dependen:
- · cambia su estado de 'PROBLEMA' a 'DESCONOCIDO'
- · se cierra manualmente, por correlación o con la ayuda de funciones basadas en el tiempo
- · se resuelve mediante un valor de un elemento no implicado en el desencadenante dependiente-
- · está desactivado, tiene un elemento desactivado o un host de elementos desactivados
Tenga en cuenta que el activador "secundario" (dependiente) en los casos mencionados no se actualizará inmediatamente. Mientras el disparador principal esté en estado PROBLEMA sus dependientes pueden informar de valores en los que no podemos confiar. Por lo tanto,  el disparador dependiente sólo será reevaluado, y cambiará su estado, sólo después de que el disparador padre esté en estado OK y hayamos recibido métricas confiables.

También:

- · La dependencia del trigger puede ser añadida desde cualquier trigger host a cualquier otro - de host, siempre y cuando no resulte en una dependencia circular.
- · La dependencia de un activador puede añadirse de una plantilla a otra plantilla. Si un disparador de la plantilla A depende de un disparador de la plantilla B, la plantilla A sólo puede estar vinculada a un host (u otra plantilla) junto con la plantilla B, pero la plantilla B puede estar vinculada a un host (o otra plantilla) sola.
- · La dependencia del trigger puede ser añadida desde el trigger de la plantilla a un host desencadenante. En este caso, la vinculación de dicha plantilla a un host creará un trigger de host que depende del mismo trigger de plantilla del que estaba.
- · Esto permite, por ejemplo, tener una plantilla donde algunos activadores dependen de activadores del router (host). Todos los hosts vinculados a esta plantilla dependerán de ese router específico.
- · La dependencia de un disparador de host a un disparador de plantilla no puede ser añadida.
- · La dependencia del trigger de un prototipo de trigger a otro prototipo de trigger (dentro de la misma plantilla).
- · prototipo de trigger (dentro de la misma regla de descubrimiento de bajo nivel) o un disparador real. Un prototipo de trigger no puede depender de un prototipo de trigger de una regla LLD diferente o de un trigger creado a partir de prototipo de trigger. Un prototipo de trigger no puede depender de un trigger de una plantilla.</target>
      </trans-unit>
      <trans-unit id="c4578b73" xml:space="preserve">
        <source>### Configuration

To define a dependency, open the Dependencies tab in the trigger
[configuration form](trigger#configuration). Click on *Add* in the
'Dependencies' block and select one or more triggers that the trigger
will depend on.

![](../../../../assets/en/manual/config/triggers/dependency.png)

Click *Update*. Now the trigger has the indication of its dependency in
the list.

![](../../../../assets/en/manual/config/triggers/dependency_list.png)</source>
        <target state="needs-translation">#### Configuración

Para definir una dependencia, abra la pestaña Dependencias en un trigger [formulario de configuración](trigger#configuration). Haga clic en *Add* en el bloque Dependencias' y seleccione uno o más disparadores de los que dependerá nuestro disparador.

![](../../../../assets/en/manual/config/triggers/dependency.png)

Haga clic en *Actualizar*. Ahora el activador tiene una indicación de su dependencia en la lista.

(../../../../assets/en/manual/config/triggers/dependency_list.png)</target>
      </trans-unit>
      <trans-unit id="4dc2ce6b" xml:space="preserve">
        <source>##### Example of several dependencies

For example, the Host is behind the Router2 and the Router2 is behind
the Router1.

    Zabbix - Router1 - Router2 - Host

If the Router1 is down, then obviously the Host and the Router2 are also unreachable,
yet receiving three notifications about the Host, the Router1 and
the Router2 all being down is excessive.

So in this case we define two dependencies:

    the 'Host is down' trigger depends on the 'Router2 is down' trigger
    the 'Router2 is down' trigger depends on the 'Router1 is down' trigger

Before changing the status of the 'Host is down' trigger, Zabbix will
check for the corresponding trigger dependencies. If such are found and one of those
triggers is in the 'Problem' state, then the trigger status will not be
changed, the actions will not be executed and no notifications will 
be sent.

Zabbix performs this check recursively. If the Router1 or the Router2 is
unreachable, the Host trigger won't be updated.</source>
        <target state="needs-translation">##### Ejemplo de varias dependencias

Por ejemplo, un Host está detrás de un Router2 y el Router2 está detrás de un Router1.

 - Zabbix - Router1 - Router2 - Host

Si el Router1 está caído, entonces obviamente el Host y el Router2 también están inalcanzables, sin embargo, no queremos recibir tres notificaciones sobre Host, Router1 y Router2 están caídos.

Así que en este caso definimos dos dependencias:

 · El disparador 'Host is down' depende del disparador 'Router2 is down'
 · El disparador "Router2 está caído" depende del disparador "Router1 está caído".

Antes de cambiar el estado del trigger 'Host is down', Zabbix buscará las dependencias de los disparos correspondientes. Si se encuentra, y uno de esos disparadores está en estado 'Problema', entonces el estado del disparador no se cambiará y por lo tanto las acciones no se ejecutarán y las notificaciones no se enviarán.

Zabbix realiza esta comprobación de forma recursiva. Si el Router1 o el Router2 es inalcanzable, el trigger Host no se actualizará.</target>
      </trans-unit>
    </body>
  </file>
</xliff>
