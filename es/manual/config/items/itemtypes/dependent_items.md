[comment]: # translation:outdated

[comment]: # ({a5972fb8-02a3a9ac})
# 15 elementos dependientes

[comment]: # ({/a5972fb8-02a3a9ac})

[comment]: # ({a6361d4a-4eb72974})
#### Visión general

Hay situaciones en las que un elemento recoge varias métricas a la vez o incluso tiene más sentido recoger métricas relacionadas simultáneamente, por ejemplo:

- · Utilización de la CPU de los núcleos individuales
- · Tráfico de red entrante/saliente/total

Para permitir la recopilación de métricas a gran escala y el uso simultáneo en varios elementos relacionados, Zabbix soporta elementos dependientes. Los elementos dependientes dependen
del elemento maestro que recopila sus datos simultáneamente, en una consulta. Un nuevo valor para el elemento maestro rellena automáticamente los valores de los elementos dependientes. Los elementos dependientes no pueden tener un intervalo de actualización diferente al del elemento maestro.
intervalo de actualización que el elemento maestro.

Las opciones de preprocesamiento de Zabbix se pueden utilizar para extraer la parte que es necesaria para el elemento dependiente de los datos del elemento maestro.

El preprocesamiento es gestionado por un proceso `preprocessing manager`, que ha ha añadido en Zabbix 3.4, junto con los trabajadores que realizan los pasos de preprocesamiento. Todos los valores (con o sin preprocesamiento) de diferentes recolectores de datos pasan por el gestor de preprocesamiento antes de ser añadidos a la caché del historial. Se utiliza la comunicación IPC basada en sockets entre los recolectores de datos (pollers, trappers, etc) y el proceso de preprocesamiento.
El servidor Zabbix o el proxy Zabbix (si el host es monitoreado por el proxy) están realizando pasos de preprocesamiento y procesando elementos dependientes.
Los elementos de cualquier tipo, incluso los dependientes, pueden establecerse como elementos maestros. Se pueden utilizar niveles adicionales de elementos dependientes para extraer partes más pequeñas partes del valor de un elemento dependiente existente.

[comment]: # ({/a6361d4a-4eb72974})

[comment]: # ({b76ecd08-3e1d96f5})
##### Limitaciones

- · Sólo se permiten dependencias del mismo host (plantilla)
- · Un prototipo de elemento puede depender de otro prototipo de elemento o de un elemento del mismo host
- · El número máximo de elementos dependientes para un elemento maestro está limitado a 29999 (independientemente del número de niveles de dependencia)
- · Se permite un máximo de 3 niveles de dependencia
- · El elemento dependiente de un anfitrión con un elemento maestro de la plantilla no se exportará a XML.
   

[comment]: # ({/b76ecd08-3e1d96f5})

[comment]: # ({310ff975-3f705816})
#### Configuración del artículo

Un elemento dependiente depende de su elemento maestro para obtener datos. Por eso el **elemento maestro** debe ser configurado (o existir) primero:

- · Ir a: *Configuración* → *Hosts*
- · Haz clic en *Items* en la fila del host
- · Haga clic en *Crear elemento*.
- · Introducir los parámetros del elemento en el formulario

![](../../../../../assets/en/manual/config/items/itemtypes/master_item.png)

Todos los campos obligatorios están marcados con un asterisco rojo.

Haga clic en *Añadir* para guardar el elemento maestro.

A continuación, puede configurar un **artículo dependiente**.

(../../../../../assets/es/manual/config/items/itemtypes/dependent_item.png)

Todos los campos obligatorios están marcados con un asterisco rojo. Los campos que requieren información específica para los elementos dependientes son:

| - | - |
|---|---|
|*Tipo*|Seleccione aquí el **artículo dependiente**.
|*Introduzca una clave que se utilizará para reconocer el artículo.
|*Seleccione el elemento principal. El valor del elemento maestro se utilizará para rellenar el valor del elemento dependiente.
|*Tipo de información*|Seleccione el tipo de información que corresponderá al formato de los datos que se almacenarán.

Puede utilizar el valor del ítem (/manual/config/items/item#item_value_preprocessing) para extraer la parte necesaria del valor del artículo maestro.

![](../../../../../assets/es/manual/config/items/itemtypes/dependent_item_preprocessing.png){width="600"}

Sin el preprocesamiento, el valor del elemento dependiente será exactamente el mismo que el valor del elemento principal.

Haga clic en *Añadir* para guardar el elemento dependiente.
Un atajo para crear un elemento dependiente más rápido es utilizar el asistente en la lista de artículos:

(../../../../../assets/es/manual/config/items/itemtypes/dependent_item_wizard.png)

[comment]: # ({/310ff975-3f705816})

[comment]: # ({647ecb03-8323a97a})
##### Visualizar

En la lista de artículos los artículos dependientes se muestran con su nombre de artículo maestro nombre como prefijo.

![](../../../../../assets/es/manual/config/items/itemtypes/dependent_items.png){width="600"}

Si se elimina un elemento maestro, también se eliminan todos sus elementos dependientes.

[comment]: # ({/647ecb03-8323a97a})
