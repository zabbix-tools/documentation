[comment]: # translation:outdated

[comment]: # ({1075f716-c5a81a82})
#1 Índices dinámicos

[comment]: # ({/1075f716-c5a81a82})

[comment]: # ({9809db7b-e95b21bb})
#### Descripción general

Si bien puede encontrar el número de índice requerido (por ejemplo, de una red
interfaz) entre los OID de SNMP, a veces es posible que el número de índice no permanezca igual.

Los números de índice pueden ser dinámicos; pueden cambiar con el tiempo y su artículo
puede dejar de funcionar como consecuencia.

Para evitar este escenario, es posible definir un OID que tenga en
en cuenta la posibilidad de que cambie un número de índice.

Por ejemplo, si necesita recuperar el valor del índice para agregarlo a
**ifInOctets** que corresponde a la interfaz **GigabitEthernet0/1**
en un dispositivo Cisco, use el siguiente OID:

    ifInOctets["índice","ifDescr","GigabitEthernet0/1"]

[comment]: # ({/9809db7b-e95b21bb})

[comment]: # ({c4dfa5a1-6a6d7709})
##### La sintaxis

Se utiliza una sintaxis especial para OID:

**<OID de datos>\["índice","<OID base de índice>","<cadena
para buscar>"\]**

|Parámetro|Descripción|
|---------|-----------|
|OID de datos|OID principal a usar para la recuperación de datos en el elemento.|
|índice|Método de procesamiento. Actualmente se admite un método:<br>**índice**: busque el índice y agréguelo al OID de datos|
|OID base del índice|Este OID se buscará para obtener el valor de índice correspondiente a la cadena.|
|cadena para buscar|La cadena que se utilizará para una coincidencia exacta con un valor al realizar una búsqueda. Sensible a mayúsculas y minúsculas.|

[comment]: # ({/c4dfa5a1-6a6d7709})

[comment]: # ({f64bab8c-3da26466})
#### Ejemplo

Obtener el uso de memoria del proceso *apache*.

Si usa esta sintaxis OID:

    HOST-RESOURCES-MIB::hrSWRunPerfMem["index","HOST-RESOURCES-MIB::hrSWRunPath", "/usr/sbin/apache2"]

el número de índice se buscará aquí:

    ...
    HOST-RESOURCES-MIB::hrSWRunPath.5376 = CADENA: "/sbin/getty"
    HOST-RESOURCES-MIB::hrSWRunPath.5377 = CADENA: "/sbin/getty"
    HOST-RESOURCES-MIB::hrSWRunPath.5388 = CADENA: "/usr/sbin/apache2"
    HOST-RESOURCES-MIB::hrSWRunPath.5389 = CADENA: "/sbin/sshd"
    ...

Ahora tenemos el índice, 5388. El índice se agregará al OID de datos
para recibir el valor que nos interesa:

    HOST-RESOURCES-MIB::hrSWRunPerfMem.5388 = ENTERO: 31468 KBytes

[comment]: # ({/f64bab8c-3da26466})

[comment]: # ({f42f504e-beab1c6d})
#### Almacenamiento en caché de búsqueda de índice

Cuando se solicita un elemento de índice dinámico, Zabbix recupera y almacena en caché
toda la tabla SNMP bajo el OID base para el índice, incluso si una coincidencia sería
encontrado antes. Esto se hace en caso de que otro elemento se refiera al mismo
OID base más tarde: Zabbix buscaría el índice en el caché, en lugar de
consultando el host monitoreado nuevamente. Tenga en cuenta que cada proceso de sondeo utiliza
caché separado.

En todas las operaciones posteriores de recuperación de valores, solo se tiene en cuenta el índice encontrado.
verificado Si no ha cambiado, se solicita valor. Si ha cambiado,
la caché se reconstruye: cada encuestador que encuentra un índice modificado recorre el
indexe la tabla SNMP nuevamente.

[comment]: # ({/f42f504e-beab1c6d})
