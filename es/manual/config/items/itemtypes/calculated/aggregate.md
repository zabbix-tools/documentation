[comment]: # translation:outdated

[comment]: # ({new-bfe1a94d})
# Aggregate calculations

[comment]: # ({/new-bfe1a94d})

[comment]: # ({new-f8e62abe})
#### Overview

In aggregate [calculations](/manual/config/items/itemtypes/calculated)
information from several items may be collected by Zabbix server (by
doing direct database queries) to calculate an aggregate, depending on
the aggregate function used.

Aggregate calculations do not require any agent running on the host
being monitored.

To define aggregate calculations, select the **Calculated** item type.

[comment]: # ({/new-f8e62abe})

[comment]: # ({new-63d83a0c})
#### Syntax

Aggregates can be retrieved by working with either:

-   history of items:

```{=html}
<!-- -->
```
    aggregate_function(function(/host/item,parameter),function(/host2/item2,parameter),...)

-   a [foreach function](/manual/appendix/functions/aggregate/foreach)
    as the only parameter:

```{=html}
<!-- -->
```
    aggregate_function(foreach_function(/*/key?[group="host group"],timeperiod))

where:

- `aggregate_function` is one of the supported [aggregate functions](/manual/appendix/functions/aggregate#aggregate-functions-1): avg, max, min, sum, etc.
- `foreach_function` is one of the supported foreach functions: avg_foreach, count_foreach, etc. 

Foreach functions use an item filter, to work with the history of
multiple items, and return an array of values - one for each item.

::: noteclassic
If the aggregate results in a float value it will be trimmed
to an integer if the aggregated item type of information is *Numeric
(unsigned)*.
:::

An aggregate calculation may become unsupported if:

-   none of the referenced items is found (which may happen if the item
    key is incorrect, none of the items exists or all included groups
    are incorrect)
-   no data to calculate a function

[comment]: # ({/new-63d83a0c})

[comment]: # ({new-3bbe172c})
#### Usage examples

Examples of keys for aggregate calculations.

[comment]: # ({/new-3bbe172c})

[comment]: # ({new-874bcf94})
##### Example 1

Total disk space of host group 'MySQL Servers'.

    sum(last_foreach(/*/vfs.fs.size[/,total]?[group="MySQL Servers"]))

[comment]: # ({/new-874bcf94})

[comment]: # ({new-3182672a})
##### Example 2

Sum of latest values of all items matching net.if.in\[\*\] on the host.

    sum(last_foreach(/host/net.if.in[*]))

[comment]: # ({/new-3182672a})

[comment]: # ({new-cdf2da8b})
##### Example 3

Average processor load of host group 'MySQL Servers'.

    avg(last_foreach(/*/system.cpu.load[,avg1]?[group="MySQL Servers"]))

[comment]: # ({/new-cdf2da8b})

[comment]: # ({new-db9c8fce})
##### Example 4

5-minute average of the number of queries per second for host group
'MySQL Servers'.

    avg(avg_foreach(/*/mysql.qps?[group="MySQL Servers"],5m))

[comment]: # ({/new-db9c8fce})

[comment]: # ({new-0233edd3})
##### Example 5

Average CPU load on all hosts in multiple host groups that have the
specific tags.

    avg(last_foreach(/*/system.cpu.load?[(group="Servers A" or group="Servers B" or group="Servers C") and (tag="Service:" or tag="Importance:High")]))

[comment]: # ({/new-0233edd3})

[comment]: # ({new-30448f5d})
##### Example 6

Calculation used on the latest item value sums of a whole host group.

    sum(last_foreach(/*/net.if.out[eth0,bytes]?[group="video"])) / sum(last_foreach(/*/nginx_stat.sh[active]?[group="video"])) 

[comment]: # ({/new-30448f5d})

[comment]: # ({new-3e9404a6})
##### Example 7

The total number of unsupported items in host group 'Zabbix servers'.

    sum(last_foreach(/*/zabbix[host,,items_unsupported]?[group="Zabbix servers"]))

[comment]: # ({/new-3e9404a6})

[comment]: # ({new-f094da29})

##### Examples of correct/incorrect syntax

Expressions (including function calls) cannot be used as history, trend, or foreach [function](/manual/appendix/functions) parameters. However, those functions themselves can be used in other (non-historical) function parameters.

|Expression|Example|
|-|---------|
|Valid|`avg(last(/host/key1),last(/host/key2)*10,last(/host/key1)*100)`<br>`max(avg(avg_foreach(/*/system.cpu.load?[group="Servers A"],5m)),avg(avg_foreach(/*/system.cpu.load?[group="Servers B"],5m)),avg(avg_foreach(/*/system.cpu.load?[group="Servers C"],5m)))`|
|Invalid|`sum(/host/key, 10+2)`<br>`sum(/host/key, avg(10, 2))`<br>`sum(/host/key, last(/host/key2))`|

Note that in an expression like:

    sum(sum_foreach(//resptime[*],5m))/sum(count_foreach(//resptime[*],5m))

it cannot be guaranteed that both parts of the equation will always have the same set of values. 
While one part of the expression is evaluated, a new value for the requested period may arrive and 
then the other part of the expression will have a different set of values.

[comment]: # ({/new-f094da29})
