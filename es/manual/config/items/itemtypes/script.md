[comment]: # translation:outdated

[comment]: # ({c5128d06-1a1e2756})
# 18 elementos scripts

[comment]: # ({/c5128d06-1a1e2756})

[comment]: # ({93454dda-cb89bc38})
#### Visión general

Los elementos de script se pueden utilizar para recopilar datos mediante la ejecución de un código JavaScript definido por el usuario con la capacidad de recuperar datos a través de HTTP/HTTPS. Además del script, se puede incluir una lista opcional de parámetros (pares de nombre y valor) y el tiempo de espera puede ser especificado.

Este tipo de elemento puede ser útil en escenarios de recogida de datos que requieren múltiples pasos o una lógica compleja. Por ejemplo, un ítem Script puede ser configurado para hacer una llamada HTTP, luego procesar los datos recibidos en el primer paso de alguna manera, y pasar el valor transformado a la segunda llamada HTTP.

Los ítems Script son procesados por el servidor Zabbix o por los proxy pollers.

[comment]: # ({/93454dda-cb89bc38})

[comment]: # ({new-7ed55e87})
#### Configuration

In the *Type* field of [item configuration
form](/manual/config/items/item) select Script then fill out required
fields.

![script\_item.png](../../../../../assets/en/manual/config/items/itemtypes/script_item.png)

All mandatory input fields are marked with a red asterisk.

The fields that require specific information for Script items are:

|Field|Description|
|-----|-----------|
|Key|Enter a unique key that will be used to identify the item.|
|Parameters|Specify the variables to be passed to the script as the attribute and value pairs.<br>[Built-in macros](/manual/config/macros) {HOST.CONN}, {HOST.DNS}, {HOST.HOST}, {HOST.IP}, {HOST.NAME}, {ITEM.ID}, {ITEM.KEY}, {ITEM.KEY.ORIG} and [user macros](/manual/config/macros/user_macros) are supported.|
|Script|Enter JavaScript code in the block that appears when clicking in the parameter field (or on the view/edit button next to it). This code must provide the logic for returning the metric value.<br>The code has access to all parameters, it may perform HTTP GET, POST, PUT and DELETE requests and has control over HTTP headers and request body.<br>See also: [Additional JavaScript objects](/manual/config/items/preprocessing/javascript/javascript_objects), [JavaScript Guide](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide).|
|Timeout|JavaScript execution timeout (1-60s, default 3s).<br>Time suffixes are supported, e.g. 30s, 1m.|

[comment]: # ({/new-7ed55e87})

[comment]: # ({594a29cf-4c860844})
#### Ejemplos

[comment]: # ({/594a29cf-4c860844})

[comment]: # ({72fd5e42-9c6c78ee})
##### Recogida de datos sencilla

Recoge el contenido de *https://www.example.com/release_notes*:
   
- · Crea un elemento de tipo "Script".
- · En el campo *Script*, introduce el siguiente código:
``` {.java}
var request = new HttpRequest();
return request.get("https://www.example.com/release_notes");
```

[comment]: # ({/72fd5e42-9c6c78ee})

[comment]: # ({new-7115c5ef})
##### Dat collection with parameters

Get the content of a specific page and make use of parameters: 

- Create an item with type Script and two parameters:
  - **url : {$DOMAIN}** (the user macro {$DOMAIN} should be defined, preferably on a host level)
  - **subpage : /release_notes**

![](../../../../../assets/en/manual/config/items/itemtypes/script_example1.png)

- In the script field, enter: 

   var obj = JSON.parse(value);
   var url = obj.url;
   var subpage = obj.subpage;
   var request = new HttpRequest();
   return request.get(url + subpage);

[comment]: # ({/new-7115c5ef})


[comment]: # ({d6607aab-18a089f4})
##### Múltiples peticiones HTTP

Recoge el contenido de ambos *https://www.example.com* y *https://www.example.com/release_notas*:

- · Crear un elemento con el tipo "Script".\
- · En el campo *Script*, introduzca el siguiente código:

``` {.java}
var request = new HttpRequest();
return request.get("https://www.example.com") + request.get("https://www.example.com/release_notes");
```

[comment]: # ({/d6607aab-18a089f4})

[comment]: # ({77699cac-d837bcdd})
##### Registro de datos

Añade la entrada "Log test" al registro del servidor Zabbix y recibe el elemento valor "1" a cambio:

- · Cree un elemento de tipo "Script".\
- · En el campo *Script*, introduce el siguiente código:

``` {.java}
Zabbix.log(3, 'Log test');
return 1;
```

[comment]: # ({/77699cac-d837bcdd})
