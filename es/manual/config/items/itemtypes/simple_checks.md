[comment]: # attributes: notoc

[comment]: # translation:outdated

[comment]: # ({new-be4ab03d})
# 5 Simple checks

[comment]: # ({/new-be4ab03d})

[comment]: # ({new-76023448})
#### Overview

Simple checks are normally used for remote agent-less checks of
services.

Note that Zabbix agent is not needed for simple checks. Zabbix
server/proxy is responsible for the processing of simple checks (making
external connections, etc).

Examples of using simple checks:

    net.tcp.service[ftp,,155]
    net.tcp.service[http]
    net.tcp.service.perf[http,,8080]
    net.udp.service.perf[ntp]

::: noteclassic
*User name* and *Password* fields in simple check item
configuration are used for VMware monitoring items; ignored
otherwise.
:::

[comment]: # ({/new-76023448})

[comment]: # ({new-f59a5ccc})
#### Supported simple checks

List of supported simple checks:

See also:

-   [VMware monitoring item
    keys](/manual/config/items/itemtypes/simple_checks/vmware_keys)

|Key|<|<|<|<|
|---|-|-|-|-|
|<|**Description**|**Return value**|**Parameters**|**Comments**|
|icmpping\[<target>,<packets>,<interval>,<size>,<timeout>\]|<|<|<|<|
|<|Host accessibility by ICMP ping.|0 - ICMP ping fails<br><br>1 - ICMP ping successful|**target** - host IP or DNS name<br>**packets** - number of packets<br>**interval** - time between successive packets in milliseconds<br>**size** - packet size in bytes<br>**timeout** - timeout in milliseconds|Example:<br>=> icmpping\[,4\] → if at least one packet of the four is returned, the item will return 1.<br><br>See also: table of [default values](simple_checks#icmp_pings).|
|icmppingloss\[<target>,<packets>,<interval>,<size>,<timeout>\]|<|<|<|<|
|<|Percentage of lost packets.|Float.|**target** - host IP or DNS name<br>**packets** - number of packets<br>**interval** - time between successive packets in milliseconds<br>**size** - packet size in bytes<br>**timeout** - timeout in milliseconds|See also: table of [default values](simple_checks#icmp_pings).|
|icmppingsec\[<target>,<packets>,<interval>,<size>,<timeout>,<mode>\]|<|<|<|<|
|<|ICMP ping response time (in seconds).|Float.|**target** - host IP or DNS name<br>**packets** - number of packets<br>**interval** - time between successive packets in milliseconds<br>**size** - packet size in bytes<br>**timeout** - timeout in milliseconds<br>**mode** - possible values: *min*, *max*, *avg* (default)|Packets which are lost or timed out are not used in the calculation.<br><br>If host is not available (timeout reached), the item will return 0.<br>If the return value is less than 0.0001 seconds, the value will be set to 0.0001 seconds.<br><br>See also: table of [default values](simple_checks#icmp_pings).|
|net.tcp.service\[service,<ip>,<port>\]|<|<|<|<|
|<|Checks if service is running and accepting TCP connections.|0 - service is down<br><br>1 - service is running|**service** - possible values: *ssh*, *ldap*, *smtp*, *ftp*, *http*, *pop*, *nntp*, *imap*, *tcp*, *https*, *telnet* (see [details](/manual/appendix/items/service_check_details))<br>**ip** - IP address or DNS name (by default host IP/DNS is used)<br>**port** - port number (by default standard service port number is used).|Example:<br>=> net.tcp.service\[ftp,,45\] → can be used to test the availability of FTP server on TCP port 45.<br><br>Note that with *tcp* service indicating the port is mandatory.<br>These checks may result in additional messages in system daemon logfiles (SMTP and SSH sessions being logged usually).<br>Checking of encrypted protocols (like IMAP on port 993 or POP on port 995) is currently not supported. As a workaround, please use net.tcp.service\[tcp,<ip>,port\] for checks like these.<br>*https* and *telnet* services are supported since Zabbix 2.0.|
|net.tcp.service.perf\[service,<ip>,<port>\]|<|<|<|<|
|<|Checks performance of TCP service.|Float.<br><br>0.000000 - service is down<br><br>seconds - the number of seconds spent while connecting to the service|**service** - possible values: *ssh*, *ldap*, *smtp*, *ftp*, *http*, *pop*, *nntp*, *imap*, *tcp*, *https*, *telnet* (see [details](/manual/appendix/items/service_check_details))<br>**ip** - IP address or DNS name (by default, host IP/DNS is used)<br>**port** - port number (by default standard service port number is used).|Example:<br>=> net.tcp.service.perf\[ssh\] → can be used to test the speed of initial response from SSH server.<br><br>Note that with *tcp* service indicating the port is mandatory.<br>Checking of encrypted protocols (like IMAP on port 993 or POP on port 995) is currently not supported. As a workaround, please use net.tcp.service.perf\[tcp,<ip>,port\] for checks like these.<br>*https* and *telnet* services are supported since Zabbix 2.0.<br>Called tcp\_perf before Zabbix 2.0.|
|net.udp.service\[service,<ip>,<port>\]|<|<|<|<|
|<|Checks if service is running and responding to UDP requests.|0 - service is down<br><br>1 - service is running|**service** - possible values: *ntp* (see [details](/manual/appendix/items/service_check_details))<br>**ip** - IP address or DNS name (by default host IP/DNS is used)<br>**port** - port number (by default standard service port number is used).|Example:<br>=> net.udp.service\[ntp,,45\] → can be used to test the availability of NTP service on UDP port 45.<br><br>This item is supported since Zabbix 3.0, but *ntp* service was available for net.tcp.service\[\] item in prior versions.|
|net.udp.service.perf\[service,<ip>,<port>\]|<|<|<|<|
|<|Checks performance of UDP service.|Float.<br><br>0.000000 - service is down<br><br>seconds - the number of seconds spent waiting for response from the service|**service** - possible values: *ntp* (see [details](/manual/appendix/items/service_check_details))<br>**ip** - IP address or DNS name (by default, host IP/DNS is used)<br>**port** - port number (by default standard service port number is used).|Example:<br>=> net.udp.service.perf\[ntp\] → can be used to test response time from NTP service.<br><br>This item is supported since Zabbix 3.0, but *ntp* service was available for net.tcp.service\[\] item in prior versions.|

[comment]: # ({/new-f59a5ccc})

[comment]: # ({new-23d7246f})

### Item key details

[comment]: # ({/new-23d7246f})

[comment]: # ({new-7d8dff2e})

##### icmpping\[<target>,<packets>,<interval>,<size>,<timeout>,<options>\] {#icmpping}

<br>
The host accessibility by ICMP ping.<br>
Return value: *0* - ICMP ping fails; *1* - ICMP ping successful.

Parameters:

-   **target** - the host IP or DNS name;
-   **packets** - the number of packets;
-   **interval** - the time between successive packets in milliseconds;
-   **size** - the packet size in bytes;
-   **timeout** - the timeout in milliseconds;
-   **options** - used for allowing redirect: if empty (default value), redirected responses are treated as target host down; if set to *allow_redirect*, redirected responses are treated as target host up.

See also the table of [default values](#default-values).

Example:

    icmpping[,4] #If at least one packet of the four is returned, the item will return 1.

[comment]: # ({/new-7d8dff2e})

[comment]: # ({new-6cd0f1b1})

##### icmppingloss\[<target>,<packets>,<interval>,<size>,<timeout>,<options>\] {#icmppingloss}

<br>
The percentage of lost packets.<br>
Return value: *Float*.

Parameters:

-   **target** - the host IP or DNS name;
-   **packets** - the number of packets;
-   **interval** - the time between successive packets in milliseconds;
-   **size** - the packet size in bytes;
-   **timeout** - the timeout in milliseconds;
-   **options** - used for allowing redirect: if empty (default value), redirected responses are treated as target host down; if set to *allow_redirect*, redirected responses are treated as target host up.

See also the table of [default values](#default-values).

[comment]: # ({/new-6cd0f1b1})

[comment]: # ({new-b5636838})

##### icmppingsec\[<target>,<packets>,<interval>,<size>,<timeout>,<mode>,<options>\] {#icmppingsec}

<br>
The ICMP ping response time (in seconds).<br>
Return value: *Float*.

Parameters:

-   **target** - the host IP or DNS name;
-   **packets** - the number of packets;
-   **interval** - the time between successive packets in milliseconds;
-   **size** - the packet size in bytes;
-   **timeout** - the timeout in milliseconds;
-   **mode** - possible values: *min*, *max*, or *avg* (default);
-   **options** - used for allowing redirect: if empty (default value), redirected responses are treated as target host down; if set to *allow_redirect*, redirected responses are treated as target host up.

Comments:

-   Packets which are lost or timed out are not used in the calculation;
-   If the host is not available (timeout reached), the item will return 0;
-   If the return value is less than 0.0001 seconds, the value will be set to 0.0001 seconds;
-   See also the table of [default values](#default-values).

[comment]: # ({/new-b5636838})

[comment]: # ({new-b0a71170})

##### net.tcp.service[service,<ip>,<port>] {#nettcpservice}

<br>
Checks if a service is running and accepting TCP connections.<br>
Return value: *0* - the service is down; *1* - the service is running.

Parameters:

-   **service** - possible values: *ssh*, *ldap*, *smtp*, *ftp*, *http*, *pop*, *nntp*, *imap*, *tcp*, *https*, *telnet* (see [details](/manual/appendix/items/service_check_details));
-   **ip** - the IP address or DNS name (by default the host IP/DNS is used);
-   **port** - the port number (by default the standard service port number is used).

Comments:

-   Note that with *tcp* service indicating the port is mandatory;
-   These checks may result in additional messages in system daemon logfiles (SMTP and SSH sessions being logged usually);
-   Checking of encrypted protocols (like IMAP on port 993 or POP on port 995) is currently not supported. As a workaround, please use `net.tcp.service[tcp,<ip>,port]` for checks like these.

Example:

    net.tcp.service[ftp,,45] #This item can be used to test the availability of FTP server on TCP port 45.

[comment]: # ({/new-b0a71170})

[comment]: # ({new-946385b4})

##### net.tcp.service.perf[service,<ip>,<port>] {#nettcpserviceperf}

<br>
Checks the performance of a TCP service.<br>
Return value: *Float*: *0.000000* - the service is down; *seconds* - the number of seconds spent while connecting to the service.

Parameters:

-   **service** - possible values: *ssh*, *ldap*, *smtp*, *ftp*, *http*, *pop*, *nntp*, *imap*, *tcp*, *https*, *telnet* (see [details](/manual/appendix/items/service_check_details));
-   **ip** - the IP address or DNS name (by default the host IP/DNS is used);
-   **port** - the port number (by default the standard service port number is used).

Comments:

-   Note that with *tcp* service indicating the port is mandatory;
-   Checking of encrypted protocols (like IMAP on port 993 or POP on port 995) is currently not supported. As a workaround, please use `net.tcp.service[tcp,<ip>,port]` for checks like these.

Example:

    net.tcp.service.perf[ssh] #This item can be used to test the speed of initial response from SSH server.

[comment]: # ({/new-946385b4})

[comment]: # ({new-2859223a})

##### net.udp.service[service,<ip>,<port>] {#netudpservice}

<br>
Checks if a service is running and responding to UDP requests.<br>
Return value: *0* - the service is down; *1* - the service is running.

Parameters:

-   **service** - possible values: *ntp* (see [details](/manual/appendix/items/service_check_details));
-   **ip** - the IP address or DNS name (by default the host IP/DNS is used);
-   **port** - the port number (by default the standard service port number is used).

Example:

    net.udp.service[ntp,,45] #This item can be used to test the availability of NTP service on UDP port 45.

[comment]: # ({/new-2859223a})

[comment]: # ({new-9cf5922c})

##### net.udp.service.perf[service,<ip>,<port>] {#netudpserviceperf}

<br>
Checks the performance of a UDP service.<br>
Return value: *Float*: *0.000000* - the service is down; *seconds* - the number of seconds spent waiting for response from the service.

Parameters:

-   **service** - possible values: *ntp* (see [details](/manual/appendix/items/service_check_details));
-   **ip** - the IP address or DNS name (by default the host IP/DNS is used);
-   **port** - the port number (by default the standard service port number is used).

Example:

    net.udp.service.perf[ntp] #This item can be used to test the response time from NTP service.

[comment]: # ({/new-9cf5922c})

[comment]: # ({new-85612a2b})
  
*Note* that for SourceIP support in LDAP simple checks, OpenLDAP version 2.6.1 or above is required.

[comment]: # ({/new-85612a2b})

[comment]: # ({new-92a6c4ae})
##### Timeout processing

Zabbix will not process a simple check longer than the Timeout seconds
defined in the Zabbix server/proxy configuration file.

[comment]: # ({/new-92a6c4ae})


[comment]: # ({new-b08a4508})
#### ICMP pings

Zabbix uses external utility **fping** for processing of ICMP pings.

The utility is not part of Zabbix distribution and has to be
additionally installed. If the utility is missing, has wrong permissions
or its location does not match the location set in the Zabbix
server/proxy configuration file ('FpingLocation' parameter), ICMP pings
(**icmpping**, **icmppingloss**, **icmppingsec**) will not be processed.

See also: [known
issues](/manual/installation/known_issues#simple_checks)

**fping** must be executable by the user Zabbix daemons run as and
setuid root. Run these commands as user **root** in order to set up
correct permissions:

    shell> chown root:zabbix /usr/sbin/fping
    shell> chmod 4710 /usr/sbin/fping

After performing the two commands above check ownership of the **fping**
executable. In some cases the ownership can be reset by executing the
chmod command.

Also check, if user zabbix belongs to group zabbix by running:

    shell> groups zabbix

and if it's not add by issuing:

    shell> usermod -a -G zabbix zabbix

Defaults, limits and description of values for ICMP check parameters:

|Parameter|Unit|Description|Fping's flag|Defaults set by|<|Allowed limits<br>by Zabbix|<|
|---------|----|-----------|------------|---------------|-|---------------------------|-|
|||||**fping**|**Zabbix**|**min**|**max**|
|packets|number|number of request packets to a target|-C||3|1|10000|
|interval|milliseconds|time to wait between successive packets|-p|1000||20|unlimited|
|size|bytes|packet size in bytes<br>56 bytes on x86, 68 bytes on x86_64|-b|56 or 68||24|65507|
|timeout|milliseconds|**fping v3.x** - timeout to wait after last packet sent, affected by *-C* flag<br> **fping v4.x** - individual timeout for each packet|-t|**fping v3.x** - 500<br>**fping v4.x** - inherited from *-p* flag, but not more than 2000||50|unlimited|

In addition Zabbix uses fping options *-i interval ms* (do not mix up
with the item parameter *interval* mentioned in the table above, which
corresponds to fping option *-p*) and *-S source IP address* (or *-I* in
older fping versions). Those options are auto-detected by running checks
with different option combinations. Zabbix tries to detect the minimal
value in milliseconds that fping allows to use with *-i* by trying 3
values: 0, 1 and 10. The value that first succeeds is then used for
subsequent ICMP checks. This process is done by each [ICMP
pinger](/manual/concepts/server#server_process_types) process
individually.

Auto-detected fping options are invalidated every hour and detected
again on the next attempt to perform ICMP check. Set DebugLevel>=4 in
order to view details of this process in the server or proxy log file.

::: notewarning
Warning: fping defaults can differ depending on
platform and version - if in doubt, check fping
documentation.
:::

Zabbix writes IP addresses to be checked by any of three *icmpping\**
keys to a temporary file, which is then passed to **fping**. If items
have different key parameters, only ones with identical key parameters
are written to a single file.\
All IP addresses written to the single file will be checked by fping in
parallel, so Zabbix icmp pinger process will spend fixed amount of time
disregarding the number of IP addresses in the file.

[comment]: # ({/new-b08a4508})

[comment]: # ({new-e3dd9826})

##### Installation

fping is not included with Zabbix and needs to be installed separately:

- Various Unix-based platforms have the fping package in their default repositories, but it is not pre-installed. In this case you can use the package manager to install fping.

- Zabbix provides [fping packages](http://repo.zabbix.com/non-supported/rhel/) for RHEL. Please note that these packages are provided without official support.

- fping can also be compiled [from source](https://github.com/schweikert/fping/blob/develop/README.md#installation).

[comment]: # ({/new-e3dd9826})

[comment]: # ({new-9ced6345})

##### Configuration

Specify fping location in the *[FpingLocation](/manual/appendix/config/zabbix_server#fpinglocation)* parameter of Zabbix server/proxy configuration file 
(or *[Fping6Location](/manual/appendix/config/zabbix_server#fping6location)* parameter for using IPv6 addresses).

fping should be executable by the user Zabbix server/proxy run as and this user should have sufficient rights.

See also: [Known issues](/manual/installation/known_issues#simple_checks) for processing simple checks with fping versions below 3.10.

[comment]: # ({/new-9ced6345})

[comment]: # ({new-3c21487d})

##### Default values

Defaults, limits and description of values for ICMP check parameters:

|Parameter|Unit|Description|Fping's flag|Defaults set by|<|Allowed limits<br>by Zabbix|<|
|--|--|--------|-|--|--|--|--|
|||||**fping**|**Zabbix**|**min**|**max**|
|packets|number|number of request packets sent to a target|-C||3|1|10000|
|interval|milliseconds|time to wait between successive packets to an individual target|-p|1000||20|unlimited|
|size|bytes|packet size in bytes<br>56 bytes on x86, 68 bytes on x86_64|-b|56 or 68||24|65507|
|timeout|milliseconds|**fping v3.x** - timeout to wait after last packet sent, affected by *-C* flag<br> **fping v4.x** - individual timeout for each packet|-t|**fping v3.x** - 500<br>**fping v4.x** and newer - inherited from *-p* flag, but not more than 2000||50|unlimited|

The defaults may differ slightly depending on the platform and version.

In addition, Zabbix uses fping options *-i interval ms* (do not mix up with the item parameter *interval* mentioned in the table above,
which corresponds to fping option *-p*) and *-S source IP address* (or *-I* in older fping versions).
These options are auto-detected by running checks with different option combinations.
Zabbix tries to detect the minimal value in milliseconds that fping allows to use with *-i* by trying 3 values: 0, 1 and 10.
The value that first succeeds is then used for subsequent ICMP checks.
This process is done by each [ICMP pinger](/manual/concepts/server#server_process_types) process individually.

Auto-detected fping options are invalidated every hour and detected again on the next attempt to perform ICMP check.
Set [DebugLevel](/manual/appendix/config/zabbix_server#debuglevel)>=4 in order to view details of this process in the server or proxy log file.

Zabbix writes IP addresses to be checked by any of the three *icmpping\** keys to a temporary file, which is then passed to fping.
If items have different key parameters, only the ones with identical key parameters are written to a single file.
All IP addresses written to the single file will be checked by fping in parallel,
so Zabbix ICMP pinger process will spend fixed amount of time disregarding the number of IP addresses in the file.

[comment]: # ({/new-3c21487d})
