[comment]: # translation:outdated

[comment]: # ({new-43646dd0})
# 2 SNMP agent

[comment]: # ({/new-43646dd0})

[comment]: # ({new-f312fc02})
#### Overview

You may want to use SNMP monitoring on devices such as printers, network
switches, routers or UPS that usually are SNMP-enabled and on which it
would be impractical to attempt setting up complete operating systems
and Zabbix agents.

To be able to retrieve data provided by SNMP agents on these devices,
Zabbix server must be [initially
configured](/manual/installation/install#configure_the_sources) with
SNMP support.

SNMP checks are performed over the UDP protocol only.

Zabbix server and proxy daemons query SNMP devices for multiple values
in a single request. This affects all kinds of SNMP items (regular SNMP
items, SNMP items with dynamic indexes, and SNMP low-level discovery)
and should make SNMP processing much more efficient. See the [bulk
processing](#internal_workings_of_bulk_processing) section for technical
details on how it works internally. Bulk requests can also be disabled
for devices that cannot handle them properly using the "Use bulk
requests" setting for each interface.

Zabbix server and proxy daemons log lines similar to the following if
they receive an incorrect SNMP response:

    SNMP response from host "gateway" does not contain all of the requested variable bindings

While they do not cover all the problematic cases, they are useful for
identifying individual SNMP devices for which bulk requests should be
disabled.

Zabbix server/proxy will always retry at least one time after an
unsuccessful query attempt: either through the SNMP library's retrying
mechanism or through the internal [bulk
processing](#internal_workings_of_bulk_processing) mechanism.

::: notewarning
If monitoring SNMPv3 devices, make sure that
msgAuthoritativeEngineID (also known as snmpEngineID or "Engine ID") is
never shared by two devices. According to [RFC
2571](http://www.ietf.org/rfc/rfc2571.txt) (section 3.1.1.1) it must be
unique for each device.
:::

::: notewarning
 RFC3414 requires the SNMPv3 devices to persist
their engineBoots. Some devices do not do that, which results in their
SNMP messages being discarded as outdated after being restarted. In such
situation, SNMP cache needs to be manually cleared on a server/proxy (by
using [-R snmp\_cache\_reload](/manual/concepts/server#runtime_control))
or the server/proxy needs to be restarted. 
:::

[comment]: # ({/new-f312fc02})

[comment]: # ({new-cac544a8})
#### Configuring SNMP monitoring

To start monitoring a device through SNMP, the following steps have to
be performed:

[comment]: # ({/new-cac544a8})

[comment]: # ({new-501ff4eb})
##### Step 1

Find out the SNMP string (or OID) of the item you want to monitor.

To get a list of SNMP strings, use the **snmpwalk** command (part of
[net-snmp](http://www.net-snmp.org/) software which you should have
installed as part of the Zabbix installation) or equivalent tool:

    shell> snmpwalk -v 2c -c public <host IP> .

As '2c' here stands for SNMP version, you may also substitute it with
'1', to indicate SNMP Version 1 on the device.

This should give you a list of SNMP strings and their last value. If it
doesn't then it is possible that the SNMP 'community' is different from
the standard 'public' in which case you will need to find out what it
is.

You can then go through the list until you find the string you want to
monitor, e.g. if you wanted to monitor the bytes coming in to your
switch on port 3 you would use the `IF-MIB::ifInOctets.3` string from
this line:

    IF-MIB::ifInOctets.3 = Counter32: 3409739121

You may now use the **snmpget** command to find out the numeric OID for
'IF-MIB::ifInOctets.3':

    shell> snmpget -v 2c -c public -On 10.62.1.22 IF-MIB::ifInOctets.3

Note that the last number in the string is the port number you are
looking to monitor. See also: [Dynamic
indexes](/manual/config/items/itemtypes/snmp/dynamicindex).

This should give you something like the following:

    .1.3.6.1.2.1.2.2.1.10.3 = Counter32: 3472126941

Again, the last number in the OID is the port number.

::: noteclassic
3COM seem to use port numbers in the hundreds, e.g. port 1 =
port 101, port 3 = port 103, but Cisco use regular numbers, e.g. port 3
= 3.
:::

::: notetip
Some of the most used SNMP OIDs are [translated
automatically to a numeric
representation](/manual/config/items/itemtypes/snmp/special_mibs) by
Zabbix.
:::

In the last example above value type is "Counter32", which internally
corresponds to ASN\_COUNTER type. The full list of supported types is
ASN\_COUNTER, ASN\_COUNTER64, ASN\_UINTEGER, ASN\_UNSIGNED64,
ASN\_INTEGER, ASN\_INTEGER64, ASN\_FLOAT, ASN\_DOUBLE, ASN\_TIMETICKS,
ASN\_GAUGE, ASN\_IPADDRESS, ASN\_OCTET\_STR and ASN\_OBJECT\_ID (since
2.2.8, 2.4.3). These types roughly correspond to "Counter32",
"Counter64", "UInteger32", "INTEGER", "Float", "Double", "Timeticks",
"Gauge32", "IpAddress", "OCTET STRING", "OBJECT IDENTIFIER" in
**snmpget** output, but might also be shown as "STRING", "Hex-STRING",
"OID" and other, depending on the presence of a display hint.

[comment]: # ({/new-501ff4eb})

[comment]: # ({new-929235a8})
##### Step 2

[Create a host](/manual/config/hosts/host) corresponding to a device.

![](../../../../../assets/en/manual/config/items/itemtypes/snmp/snmp_host.png)

Add an SNMP interface for the host:

-   Enter the IP address/DNS name and port number
-   Select the *SNMP version* from the dropdown
-   Add interface credentials depending on the selected SNMP version:
    -   SNMPv1, v2 require only the community (usually 'public')
    -   SNMPv3 requires more specific options (see below)
-   Leave the *Use bulk requests* checkbox marked to allow bulk
    processing of SNMP requests

|SNMPv3 parameter|Description|
|----------------|-----------|
|*Context name*|Enter context name to identify item on SNMP subnet.<br>*Context name* is supported for SNMPv3 items since Zabbix 2.2.<br>User macros are resolved in this field.|
|*Security name*|Enter security name.<br>User macros are resolved in this field.|
|*Security level*|Select security level:<br>**noAuthNoPriv** - no authentication nor privacy protocols are used<br>**AuthNoPriv** - authentication protocol is used, privacy protocol is not<br>**AuthPriv** - both authentication and privacy protocols are used|
|*Authentication protocol*|Select authentication protocol - *MD5*, *SHA1*, *SHA224*, *SHA256*, *SHA384* or *SHA512*.|
|*Authentication passphrase*|Enter authentication passphrase.<br>User macros are resolved in this field.|
|*Privacy protocol*|Select privacy protocol - *DES*, *AES128*, *AES192*, *AES256*, *AES192C* (Cisco) or *AES256C* (Cisco).|
|*Privacy passphrase*|Enter privacy passphrase.<br>User macros are resolved in this field.|

In case of wrong SNMPv3 credentials (security name, authentication
protocol/passphrase, privacy protocol) Zabbix receives an ERROR from
net-snmp, except for wrong *Privacy passphrase* in which case Zabbix
receives a TIMEOUT error from net-snmp.

::: notewarning
 Changes in *Authentication protocol*,
*Authentication passphrase*, *Privacy protocol* or *Privacy passphrase*,
made without changing the *Security name*, will take effect only after
the cache on a server/proxy is manually cleared (by using [-R
snmp\_cache\_reload](/manual/concepts/server#running_server)) or the
server/proxy is restarted. In cases, where *Security name* is also
changed, all parameters will be updated immediately. 
:::

You can use one of the provided SNMP templates (*Template SNMP Device*
and others) that will automatically add a set of items. However, the
template may not be compatible with the host. Click on *Add* to save the
host.

[comment]: # ({/new-929235a8})

[comment]: # ({new-290ebad1})
##### Step 3

Create an item for monitoring.

So, now go back to Zabbix and click on *Items* for the SNMP host you
created earlier. Depending on whether you used a template or not when
creating your host, you will have either a list of SNMP items associated
with your host or just an empty list. We will work on the assumption
that you are going to create the item yourself using the information you
have just gathered using snmpwalk and snmpget, so click on *Create
item*. In the new item form:

-   Enter the item name
-   Change the 'Type' field to 'SNMP agent'
-   Enter the 'Key' as something meaningful, e.g. SNMP-InOctets-Bps
-   Make sure the 'Host interface' field has your switch/router in it
-   Enter the textual or numeric OID that you retrieved earlier into the
    'SNMP OID' field, for example: .1.3.6.1.2.1.2.2.1.10.3
-   Set the 'Type of information' to *Numeric (float)*
-   Enter an 'Update interval' and 'History storage' period if you want
    them to be different from the default
-   In the *Preprocessing* tab, add a *Change per second* step
    (important, otherwise you will get cumulative values from the SNMP
    device instead of the latest change). Choose a custom multiplier if
    you want one.

![](../../../../../assets/en/manual/config/items/itemtypes/snmp_item.png)

All mandatory input fields are marked with a red asterisk.

Now save the item and go to *Monitoring* → *Latest data* for your SNMP
data!

[comment]: # ({/new-290ebad1})

[comment]: # ({new-6021e1bd})
##### Example 1

General example:

|Parameter|Description|
|---------|-----------|
|**OID**|1.2.3.45.6.7.8.0 (or .1.2.3.45.6.7.8.0)|
|**Key**|<Unique string to be used as reference to triggers><br>For example, "my\_param".|

Note that OID can be given in either numeric or string form. However, in
some cases, string OID must be converted to numeric representation.
Utility snmpget may be used for this purpose:

    shell> snmpget -On localhost public enterprises.ucdavis.memory.memTotalSwap.0

Monitoring of SNMP parameters is possible if --with-net-snmp flag was
specified while configuring Zabbix sources.

[comment]: # ({/new-6021e1bd})

[comment]: # ({new-45af5cf0})
##### Example 2

Monitoring of uptime:

|Parameter|Description|
|---------|-----------|
|**OID**|MIB::sysUpTime.0|
|**Key**|router.uptime|
|**Value type**|Float|
|**Units**|uptime|
|**Multiplier**|0.01|

[comment]: # ({/new-45af5cf0})

[comment]: # ({new-ea70430a})

#### Native SNMP bulk requests

The **walk[OID1,OID2,...]** item allows to use native SNMP functionality for bulk requests (GetBulkRequest-PDUs), available in SNMP versions 2/3. 

A GetBulk request in SNMP executes multiple GetNext requests and returns the result in a single response. This may be used for regular SNMP items as well as for SNMP discovery to minimize network roundtrips.

The SNMP **walk[OID1,OID2,...]** item may be used as the master item that collects data in one request with dependent ityems that parse the response as needed using preprocessing. 

Note that using native SNMP bulk requests is not related to the option of combining SNMP requests, which is Zabbix own way of combining multiple SNMP requests (see next section).

[comment]: # ({/new-ea70430a})

[comment]: # ({new-c57b8645})
#### Internal workings of bulk processing

Starting from 2.2.3 Zabbix server and proxy query SNMP devices for
multiple values in a single request. This affects several types of SNMP
items:

-   regular SNMP items
-   SNMP items [with dynamic
    indexes](/manual/config/items/itemtypes/snmp/dynamicindex)
-   SNMP [low-level discovery
    rules](/manual/discovery/low_level_discovery/examples/snmp_oids)

All SNMP items on a single interface with identical parameters are
scheduled to be queried at the same time. The first two types of items
are taken by pollers in batches of at most 128 items, whereas low-level
discovery rules are processed individually, as before.

On the lower level, there are two kinds of operations performed for
querying values: getting multiple specified objects and walking an OID
tree.

For "getting", a GetRequest-PDU is used with at most 128 variable
bindings. For "walking", a GetNextRequest-PDU is used for SNMPv1 and
GetBulkRequest with "max-repetitions" field of at most 128 is used for
SNMPv2 and SNMPv3.

Thus, the benefits of bulk processing for each SNMP item type are
outlined below:

-   regular SNMP items benefit from "getting" improvements;
-   SNMP items with dynamic indexes benefit from both "getting" and
    "walking" improvements: "getting" is used for index verification and
    "walking" for building the cache;
-   SNMP low-level discovery rules benefit from "walking" improvements.

However, there is a technical issue that not all devices are capable of
returning 128 values per request. Some always return a proper response,
but others either respond with a "tooBig(1)" error or do not respond at
all once the potential response is over a certain limit.

In order to find an optimal number of objects to query for a given
device, Zabbix uses the following strategy. It starts cautiously with
querying 1 value in a request. If that is successful, it queries 2
values in a request. If that is successful again, it queries 3 values in
a request and continues similarly by multiplying the number of queried
objects by 1.5, resulting in the following sequence of request sizes: 1,
2, 3, 4, 6, 9, 13, 19, 28, 42, 63, 94, 128.

However, once a device refuses to give a proper response (for example,
for 42 variables), Zabbix does two things.

First, for the current item batch it halves the number of objects in a
single request and queries 21 variables. If the device is alive, then
the query should work in the vast majority of cases, because 28
variables were known to work and 21 is significantly less than that.
However, if that still fails, then Zabbix falls back to querying values
one by one. If it still fails at this point, then the device is
definitely not responding and request size is not an issue.

The second thing Zabbix does for subsequent item batches is it starts
with the last successful number of variables (28 in our example) and
continues incrementing request sizes by 1 until the limit is hit. For
example, assuming the largest response size is 32 variables, the
subsequent requests will be of sizes 29, 30, 31, 32, and 33. The last
request will fail and Zabbix will never issue a request of size 33
again. From that point on, Zabbix will query at most 32 variables for
this device.

If large queries fail with this number of variables, it can mean one of
two things. The exact criteria that a device uses for limiting response
size cannot be known, but we try to approximate that using the number of
variables. So the first possibility is that this number of variables is
around the device's actual response size limit in the general case:
sometimes response is less than the limit, sometimes it is greater than
that. The second possibility is that a UDP packet in either direction
simply got lost. For these reasons, if Zabbix gets a failed query, it
reduces the maximum number of variables to try to get deeper into the
device's comfortable range, but (starting from 2.2.8) only up to two
times.

In the example above, if a query with 32 variables happens to fail,
Zabbix will reduce the count to 31. If that happens to fail, too, Zabbix
will reduce the count to 30. However, Zabbix will not reduce the count
below 30, because it will assume that further failures are due to UDP
packets getting lost, rather than the device's limit.

If, however, a device cannot handle bulk requests properly for other
reasons and the heuristic described above does not work, since Zabbix
2.4 there is a "Use bulk requests" setting for each interface that
allows to disable bulk requests for that device.

[comment]: # ({/new-c57b8645})
