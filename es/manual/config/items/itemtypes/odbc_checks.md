[comment]: # translation:outdated

[comment]: # ({472ac9fb-68c88a1c})
# 14 Monitorizando ODBC

[comment]: # ({/472ac9fb-68c88a1c})

[comment]: # ({ddabc941-b5170fd3})
#### Visión general

La monitorización de ODBC se corresponde con el tipo de elemento *Monitor de base de datos* en el Zabbix frontend.

ODBC es una API de software intermedio en lenguaje C para acceder a sistemas de gestión de bases de datos (DBMS). El concepto ODBC fue desarrollado por Microsoft y posteriormente fue portado a otras plataformas.

Zabbix puede consultar cualquier base de datos soportada por ODBC. Para ello, Zabbix no se conecta directamente a las bases de datos, sino que utiliza la interfaz y los controladores establecidos en ODBC. Esta función permite una supervisión más eficiente de las diferentes bases de datos para múltiples propósitos - por ejemplo, comprobar colas específicas de bases de datos, estadísticas de uso, etc.
Zabbix soporta unixODBC, que es una de las implementaciones de la API ODBC de código abierto más utilizadas.

::: notaimportante
Ver también la sección [problemas conocidos](/manual/installation/known_issues#odbc_checks) para las comprobaciones de cheques ODBC.
:::

[comment]: # ({/ddabc941-b5170fd3})

[comment]: # ({c9f90170-bf6fd8ba})
#### Instalación de unixODBC

La forma sugerida de instalar unixODBC es utilizar los repositorios de paquetes por defecto del sistema operativo Linux. En las distribuciones más populares de Linux, unixODBC está incluido por defecto en el repositorio de paquetes. Si no está disponible, puede obtenerse en la página web de unixODBC:

<http://www.unixodbc.org/download.html>.

Instalar unixODBC en sistemas basados en RedHat/Fedora utilizando el *yum* gestor de paquetes:

 · shell> yum -y install unixODBC unixODBC-devel

Instalación de unixODBC en sistemas basados en SUSE utilizando el gestor de paquetes *zypper* manager:

 · # zypper in unixODBC-devel

::: notaclásica
El paquete unixODBC-devel es necesario para compilar Zabbix con
soporte para unixODBC.
:::

[comment]: # ({/c9f90170-bf6fd8ba})

[comment]: # ({2ea7461e-eea09ed7})
#### Instalación de los drivers unixODBC
Debe instalarse un controlador de base de datos unixODBC para la base de datos que será monitoreada. unixODBC tiene una lista de bases de datos soportadas y controladores: <http://www.unixodbc.org/drivers.html>. 

En algunas distribuciones de Linux los controladores de bases de datos están incluidos en los repositorios de paquetes. Instalar el driver de la base de datos MySQL en sistemas basados en RedHat/Fedora usando el gestor de paquetes *yum*:

 · shell> yum install mysql-connector-odbc

Instalación del controlador de la base de datos MySQL en sistemas basados en SUSE utilizando el gestor de paquetes *zypper
*zypper* gestor de paquetes:

 · zypper in MyODBC-unixODBC

[comment]: # ({/2ea7461e-eea09ed7})

[comment]: # ({6a0ba916-b15e771b})
#### Configuración de unixODBC

La configuración de ODBC se realiza editando los archivos **odbcinst.ini** y **odbc.ini**. Para verificar la ubicación del archivo de configuración, escriba:

  · shell> odbcinst -j

**odbcinst.ini** se utiliza para listar los controladores de bases de datos ODBC instalados:

 · [mysql]
 · Description = ODBC for MySQL
 · Driver · = /usr/lib/libmyodbc5.so

Detalles de parámetros:

|Atributo|Descripción|
|---------|-----------|
|*mysql*|Nombre driver de la base de datos.|
|*Description*|Descripción driver de la base de datos.|
|*Driver*|Localización de la biblioteca del driver de la base de datos.

**odbc.ini** se utiliza para definir las fuentes de datos:

 · [test]
 · Description = MySQL test database
 · Driver · = mysql
 · Server · = 127.0.0.1
 · User · = root
 · Password · =
 · Port · = 3306
 · Database · = zabbix

Detalles de los parámetros:

|Atributo|Descripción|
|---------|-----------|
|*Nombre de la fuente de datos (DSN).
|*Descripción de la fuente de datos
|*Driver*|Nombre del controlador de la base de datos - como se especifica en odbcinst.ini|
|*Servidor*: IP/DNS del servidor de la base de datos.
|*Usuario de la base de datos para la conexión
|*Contraseña del usuario de la base de datos
|*Puerto de conexión de la base de datos
|*Base de datos*: Nombre de la base de datos.

Para verificar si la conexión ODBC funciona correctamente, debe probarse una conexión a la base de datos. Eso puede hacerse con la utilidad **isql** (incluida en el paquete unixODBC):

 · shell> isql test
 · +---------------------------------------+
 · | Connected! · |
 · | · |
 · | sql-statement · |
 · | help [tablename] · |
 · | quit · |
 · | · |
 · +---------------------------------------+
 · SQL>

[comment]: # ({/6a0ba916-b15e771b})

[comment]: # ({d668684c-9f87c9b6})
#### Compilación de Zabbix con soporte ODBC

Para habilitar el soporte de ODBC, Zabbix debe ser compilado con la siguiente bandera:

 · --with-unixodbc[=ARG] · use odbc driver against unixODBC package

::: notaclásica
Ver más sobre la instalación de Zabbix desde el [código fuente](/manual/installation/install#from_the_sources)
:::

[comment]: # ({/d668684c-9f87c9b6})

[comment]: # ({new-ca784f43})
#### Item configuration in Zabbix frontend

Configure a database monitoring
[item](/manual/config/items/item#overview).

![](../../../../../assets/en/manual/config/items/itemtypes/db_monitor.png)

All mandatory input fields are marked with a red asterisk.

Specifically for database monitoring items you must enter:

|   |   |
|---|---|
|*Type*|Select *Database monitor* here.|
|*Key*|Enter one of the two supported item keys:<br>**db.odbc.select**\[<unique short description>,<dsn>,<connection string>\] - this item is designed to return one value, i.e. the first column of the first row of the SQL query result. If a query returns more than one column, only the first column is read. If a query returns more than one line, only the first line is read.<br>**db.odbc.get**\[<unique short description>,<dsn>,<connection string>\] - this item is capable of returning multiple rows/columns in JSON format. Thus it may be used as a master item that collects all data in one system call, while JSONPath preprocessing may be used in dependent items to extract individual values. For more information, see an [example](/manual/discovery/low_level_discovery/examples/sql_queries#using_dbodbcget) of the returned format, used in low-level discovery. This item is supported since Zabbix 4.4.<br>The unique description will serve to identify the item in triggers, etc.<br>Although `dsn` and `connection string` are optional parameters, at least one of them should be present. If both data source name (DSN) and connection string are defined, the DSN will be ignored.<br>The data source name, if used, must be set as specified in odbc.ini.<br>The connection string may contain driver-specific arguments.<br><br>Example (connection for MySQL ODBC driver 5):<br>=> db.odbc.get\[MySQL example,,"Driver=/usr/local/lib/libmyodbc5a.so;Database=master;Server=127.0.0.1;Port=3306"\]|
|*User name*|Enter the database user name<br>This parameter is optional if user is specified in odbc.ini.<br>If connection string is used, and *User name* field is not empty, it is appended to the connection string as `UID=<user>`|
|*Password*|Enter the database user password<br>This parameter is optional if password is specified in odbc.ini.<br>If connection string is used, and *Password* field is not empty, it is appended to the connection string as `PWD=<password>`.|
|*SQL query*|Enter the SQL query.<br>Note that with the `db.odbc.select[]` item the query must return one value only.|
|*Type of information*|It is important to know what type of information will be returned by the query, so that it is selected correctly here. With an incorrect *type of information* the item will turn unsupported.|

[comment]: # ({/new-ca784f43})

[comment]: # ({new-0188aaa2})
#### Item key details

Parameters without angle brackets are mandatory. Parameters marked with angle brackets **<** **>** are optional.

[comment]: # ({/new-0188aaa2})

[comment]: # ({new-13edfcd4})

##### db.odbc.select[<unique short description>,<dsn>,<connection string>] {#db.odbc.select}

<br>
Returns one value, i.e. the first column of the first row of the SQL query result.
Return value: depending on the SQL query.

Parameters:

-   **unique short description** - a unique short description to identify the item (for use in triggers, etc);
-   **dsn** - the data source name (as specified in odbc.ini);
-   **connection string** - the connection string (may contain driver-specific arguments).

Comments:

-   Although `dsn` and `connection string` are optional parameters, at least one of them must be present. If both data source name (DSN) and connection string are defined, the DSN will be ignored.
-   If a query returns more than one column, only the first column is read. If a query returns more than one line, only the first line is read.

[comment]: # ({/new-13edfcd4})

[comment]: # ({new-ebf5d449})

##### db.odbc.get[<unique short description>,<dsn>,<connection string>] {#db.odbc.get}

<br>
Transforms the SQL query result into a JSON array.<br>
Return value: *JSON object*.

Parameters:

-   **unique short description** - a unique short description to identify the item (for use in triggers, etc);
-   **dsn** - the data source name (as specified in odbc.ini);
-   **connection string** - the connection string (may contain driver-specific arguments).

Comments:

-   Although `dsn` and `connection string` are optional parameters, at least one of them must be present. If both data source name (DSN) and connection string are defined, the DSN will be ignored.
-   Multiple rows/columns in JSON format may be returned. This item may be used as a master item that collects all data in one system call, while JSONPath preprocessing may be used in dependent items to extract individual values. For more information, see an [example](/manual/discovery/low_level_discovery/examples/sql_queries#using-db.odbc.get) of the returned format, used in low-level discovery.

Example:

    db.odbc.get[MySQL example,,"Driver=/usr/local/lib/libmyodbc5a.so;Database=master;Server=127.0.0.1;Port=3306"] #connection for MySQL ODBC driver 5

[comment]: # ({/new-ebf5d449})

[comment]: # ({new-c8148c95})

##### db.odbc.discovery[<unique short description>,<dsn>,<connection string>] {#db.odbc.discovery}

<br>
Transforms the SQL query result into a JSON array, used for [low-level disovery](/manual/discovery/low_level_discovery/examples/sql_queries). 
The column names from the query result are turned into low-level discovery macro 
names paired with the discovered field values. These macros can be used in creating 
item, trigger, etc prototypes.<br>
Return value: *JSON object*.

Parameters:

-   **unique short description** - a unique short description to identify the item (for use in triggers, etc);
-   **dsn** - the data source name (as specified in odbc.ini);
-   **connection string** - the connection string (may contain driver-specific arguments).

Comments:

-   Although `dsn` and `connection string` are optional parameters, at least one of them must be present. If both data source name (DSN) and connection string are defined, the DSN will be ignored.

[comment]: # ({/new-c8148c95})

[comment]: # ({new-06f9d2eb})
#### Important notes

-   Zabbix does not limit the query execution time. It is up to the user
    to choose queries that can be executed in a reasonable amount of
    time.
-   The [Timeout](/manual/appendix/config/zabbix_server) parameter value
    from Zabbix server is used as the ODBC login timeout (note that
    depending on ODBC drivers the login timeout setting might be
    ignored).
-   The SQL command must return a result set like any query with
    `select ...`. The query syntax will depend on the RDBMS which will
    process them. The syntax of request to a storage procedure must be
    started with `call` keyword.

[comment]: # ({/new-06f9d2eb})

[comment]: # ({new-718edfdc})
#### Error messages

ODBC error messages are structured into fields to provide detailed
information. For example:

    Cannot execute ODBC query: [SQL_ERROR]:[42601][7][ERROR: syntax error at or near ";"; Error while executing the query]
    └───────────┬───────────┘  └────┬────┘ └──┬──┘└┬┘└─────────────────────────────┬─────────────────────────────────────┘
                │                   │         │    └─ Native error code            └─ Native error message
                │                   │         └─ SQLState
                └─ Zabbix message   └─ ODBC return code

Note that the error message length is limited to 2048 bytes, so the
message can be truncated. If there is more than one ODBC diagnostic
record Zabbix tries to concatenate them (separated with `|`) as far as
the length limit allows.

[comment]: # ({/new-718edfdc})
