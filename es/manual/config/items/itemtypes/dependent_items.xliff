<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="es" datatype="plaintext" original="manual/config/items/itemtypes/dependent_items.md">
    <body>
      <trans-unit id="02a3a9ac" xml:space="preserve">
        <source># 15 Dependent items</source>
        <target state="needs-translation"># 15 elementos dependientes</target>
      </trans-unit>
      <trans-unit id="4eb72974" xml:space="preserve">
        <source>#### Overview

There are situations when one item gathers multiple metrics at a time or
it even makes more sense to collect related metrics simultaneously, for
example:

-   CPU utilization of individual cores
-   Incoming/outgoing/total network traffic

To allow for bulk metric collection and simultaneous use in several
related items, Zabbix supports dependent items. Dependent items depend
on the master item that collects their data simultaneously, in one
query. A new value for the master item automatically populates the
values of the dependent items. Dependent items cannot have a different
update interval than the master item.

Zabbix preprocessing options can be used to extract the part that is
needed for the dependent item from the master item data.

Preprocessing is managed by a `preprocessing manager` process, which has
been added in Zabbix 3.4, along with workers that perform the
preprocessing steps. All values (with or without preprocessing) from
different data gatherers pass through the preprocessing manager before
being added to the history cache. Socket-based IPC communication is used
between data gatherers (pollers, trappers, etc) and the preprocessing
process.

Zabbix server or Zabbix proxy (if host is monitored by proxy) are
performing preprocessing steps and processing dependent items.

Item of any type, even dependent item, can be set as master item.
Additional levels of dependent items can be used to extract smaller
parts from the value of an existing dependent item.</source>
        <target state="needs-translation">#### Visión general

Hay situaciones en las que un elemento recoge varias métricas a la vez o incluso tiene más sentido recoger métricas relacionadas simultáneamente, por ejemplo:

- · Utilización de la CPU de los núcleos individuales
- · Tráfico de red entrante/saliente/total

Para permitir la recopilación de métricas a gran escala y el uso simultáneo en varios elementos relacionados, Zabbix soporta elementos dependientes. Los elementos dependientes dependen
del elemento maestro que recopila sus datos simultáneamente, en una consulta. Un nuevo valor para el elemento maestro rellena automáticamente los valores de los elementos dependientes. Los elementos dependientes no pueden tener un intervalo de actualización diferente al del elemento maestro.
intervalo de actualización que el elemento maestro.

Las opciones de preprocesamiento de Zabbix se pueden utilizar para extraer la parte que es necesaria para el elemento dependiente de los datos del elemento maestro.

El preprocesamiento es gestionado por un proceso `preprocessing manager`, que ha ha añadido en Zabbix 3.4, junto con los trabajadores que realizan los pasos de preprocesamiento. Todos los valores (con o sin preprocesamiento) de diferentes recolectores de datos pasan por el gestor de preprocesamiento antes de ser añadidos a la caché del historial. Se utiliza la comunicación IPC basada en sockets entre los recolectores de datos (pollers, trappers, etc) y el proceso de preprocesamiento.
El servidor Zabbix o el proxy Zabbix (si el host es monitoreado por el proxy) están realizando pasos de preprocesamiento y procesando elementos dependientes.
Los elementos de cualquier tipo, incluso los dependientes, pueden establecerse como elementos maestros. Se pueden utilizar niveles adicionales de elementos dependientes para extraer partes más pequeñas partes del valor de un elemento dependiente existente.</target>
      </trans-unit>
      <trans-unit id="3e1d96f5" xml:space="preserve">
        <source>##### Limitations

-   Only same host (template) dependencies are allowed
-   An item prototype can depend on another item prototype or regular
    item from the same host
-   Maximum count of dependent items for one master item is limited to
    29999 (regardless of the number of dependency levels)
-   Maximum 3 dependency levels allowed
-   Dependent item on a host with master item from template will not be
    exported to XML</source>
        <target state="needs-translation">##### Limitaciones

- · Sólo se permiten dependencias del mismo host (plantilla)
- · Un prototipo de elemento puede depender de otro prototipo de elemento o de un elemento del mismo host
- · El número máximo de elementos dependientes para un elemento maestro está limitado a 29999 (independientemente del número de niveles de dependencia)
- · Se permite un máximo de 3 niveles de dependencia
- · El elemento dependiente de un anfitrión con un elemento maestro de la plantilla no se exportará a XML.
   </target>
      </trans-unit>
      <trans-unit id="3f705816" xml:space="preserve">
        <source>#### Item configuration

A dependent item depends on its master item for data. That is why the
**master item** must be configured (or exist) first:

-   Go to: *Data collection* → *Hosts*
-   Click on *Items* in the row of the host
-   Click on *Create item*
-   Enter parameters of the item in the form

![](../../../../../assets/en/manual/config/items/itemtypes/master_item.png)

All mandatory input fields are marked with a red asterisk.

Click on *Add* to save the master item.

Then you can configure a **dependent item**.

![](../../../../../assets/en/manual/config/items/itemtypes/dependent_item.png)

All mandatory input fields are marked with a red asterisk.

The fields that require specific information for dependent items are:

|   |   |
|--|--------|
|*Type*|Select **Dependent item** here.|
|*Key*|Enter a key that will be used to recognize the item.|
|*Master item*|Select the master item. Master item value will be used to populate dependent item value.|
|*Type of information*|Select the type of information that will correspond the format of data that will be stored.|

You may use item value
[preprocessing](/manual/config/items/item#item_value_preprocessing) to
extract the required part of the master item value.

![](../../../../../assets/en/manual/config/items/itemtypes/dependent_item_preprocessing.png){width="600"}

Without preprocessing, the dependent item value will be exactly the same
as the master item value.

Click on *Add* to save the dependent item.

A shortcut to creating a dependent item quicker can be accessed by clicking on the ![](../../../../../assets/en/manual/config/items/itemtypes/dependent_item_button.png) button in the item list
and selecting *Create dependent item*.

![](../../../../../assets/en/manual/config/items/itemtypes/dependent_item_menu.png){width="350"}</source>
        <target state="needs-translation">#### Configuración del artículo

Un elemento dependiente depende de su elemento maestro para obtener datos. Por eso el **elemento maestro** debe ser configurado (o existir) primero:

- · Ir a: *Configuración* → *Hosts*
- · Haz clic en *Items* en la fila del host
- · Haga clic en *Crear elemento*.
- · Introducir los parámetros del elemento en el formulario

![](../../../../../assets/en/manual/config/items/itemtypes/master_item.png)

Todos los campos obligatorios están marcados con un asterisco rojo.

Haga clic en *Añadir* para guardar el elemento maestro.

A continuación, puede configurar un **artículo dependiente**.

(../../../../../assets/es/manual/config/items/itemtypes/dependent_item.png)

Todos los campos obligatorios están marcados con un asterisco rojo. Los campos que requieren información específica para los elementos dependientes son:

| - | - |
|---|---|
|*Tipo*|Seleccione aquí el **artículo dependiente**.
|*Introduzca una clave que se utilizará para reconocer el artículo.
|*Seleccione el elemento principal. El valor del elemento maestro se utilizará para rellenar el valor del elemento dependiente.
|*Tipo de información*|Seleccione el tipo de información que corresponderá al formato de los datos que se almacenarán.

Puede utilizar el valor del ítem (/manual/config/items/item#item_value_preprocessing) para extraer la parte necesaria del valor del artículo maestro.

![](../../../../../assets/es/manual/config/items/itemtypes/dependent_item_preprocessing.png){width="600"}

Sin el preprocesamiento, el valor del elemento dependiente será exactamente el mismo que el valor del elemento principal.

Haga clic en *Añadir* para guardar el elemento dependiente.
Un atajo para crear un elemento dependiente más rápido es utilizar el asistente en la lista de artículos:

(../../../../../assets/es/manual/config/items/itemtypes/dependent_item_wizard.png)</target>
      </trans-unit>
      <trans-unit id="8323a97a" xml:space="preserve">
        <source>##### Display

In the item list dependent items are displayed with their master item
name as prefix.

![](../../../../../assets/en/manual/config/items/itemtypes/dependent_items.png){width="600"}

If a master item is deleted, so are all its dependent items.</source>
        <target state="needs-translation">##### Visualizar

En la lista de artículos los artículos dependientes se muestran con su nombre de artículo maestro nombre como prefijo.

![](../../../../../assets/es/manual/config/items/itemtypes/dependent_items.png){width="600"}

Si se elimina un elemento maestro, también se eliminan todos sus elementos dependientes.</target>
      </trans-unit>
    </body>
  </file>
</xliff>
