[comment]: # translation:outdated

[comment]: # ({090c6691-596937b2})
# 3 Configuración recomendada de UnixODBC para Oracle

[comment]: # ({/090c6691-596937b2})

[comment]: # ({ba89004c-4bfd360f})
#### Instalación

Por favor consulte[documentación de Oracle](https://docs.oracle.com/database/121/ADFNS/adfns_odbc.htm) para todas las instrucciones necesarias.

Para obtener información adicional, consulte: [Instalando unixODBC](/manual/config/items/itemtypes/odbc_checks/).

[comment]: # ({/ba89004c-4bfd360f})
