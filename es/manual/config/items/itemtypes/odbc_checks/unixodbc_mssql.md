[comment]: # translation:outdated

[comment]: # ({fc75b9dd-6fa4a853})
# 4 Configuración recomendada de UnixODBC para MSSQL

[comment]: # ({/fc75b9dd-6fa4a853})

[comment]: # ({a163efce-29c905c8})
#### Instalación

- · \*\* Red Hat Enterprise Linux/CentOS\*\*:

```{=html}
<!-- -->
```
 · # yum -y install freetds unixODBC 

- · **Debian/Ubuntu**:

Por favor consulte [guía de usuario de FreeTDS](http://www.freetds.org/userguide/) para descargar el controlador de base de datos necesario para la plataforma correspondiente.

Para obtener información adicional, consulte: [instalando unixODBC](/manual/config/items/itemtypes/odbc_checks/).

[comment]: # ({/a163efce-29c905c8})

[comment]: # ({1cbe3158-aa4d8324})
#### Configuración

La configuración de ODBC se realiza editando los archivos **odbcinst.ini** y **odbc.ini**. Estos archivos de configuración se encuentran en la carpeta */etc* carpeta. El archivo **odbcinst.ini** puede faltar y en este caso es necesario crearlo manualmente.
Tenga en cuenta los siguientes ejemplos:

**odbcinst.ini**

 · $ vi /etc/odbcinst.ini
 · [FreeTDS]
 · Driver = /usr/lib64/libtdsodbc.so.0

**odbc.ini**

 · $ vi /etc/odbc.ini
 · [sql1]
 · Driver = FreeTDS
 · Server = <SQL server 1 IP>
 · PORT = 1433
 · TDS_Version = 8.0

[comment]: # ({/1cbe3158-aa4d8324})
