[comment]: # translation:outdated

[comment]: # ({01e4795f-e966b3a3})
# 1 Configuración recomendada de UnixODBC para MySQL

[comment]: # ({/01e4795f-e966b3a3})

[comment]: # ({9b3d6c3d-e5f46c02})
#### Instalación

 · *** Red Hat Enterprise Linux/CentOS**:

 · # yum install mysql-connector-odbc

 · ***Debian/Ubuntu**:

Por favor, consulte la [documentación MySQL](https://dev.mysql.com/downloads/connector/odbc/) para descargar el controlador de base de datos necesario para la plataforma correspondiente.

Para obtener información adicional, por favor, consulte: [instalando unixODBC](/manual/config/items/itemtypes/odbc_checks/).

[comment]: # ({/9b3d6c3d-e5f46c02})

[comment]: # ({6702bdf1-dcea6601})
#### Configuración

La configuración de ODBC se realiza editando los archivos **odbcinst.ini** y **odbc.ini**
. Estos archivos de configuración se encuentran en la carpeta */etc*. Estos archivos de configuración se encuentran en la carpeta */etc*. El fichero **odbcinst.ini** puede faltar y en este caso es necesario crearlo manualmente.

**odbcinst.ini**

 · [mysql]
 · Descripción = General ODBC para MySQL
 · Driver · = /usr/lib64/libmyodbc5.so
 · Setup · = /usr/lib64/libodbcmyS.so 
 · FileUsage · = 1

Tenga en cuenta los siguientes ejemplos de configuración de **odbc.ini**.
parámetros.

- ·  Un ejemplo con una conexión a través de una IP:
```{=html}
<!-- -->
```
 · [TEST_MYSQL] · 
 · Description = MySQL base de datos 1 · 
 · Driver · = mysql · 
 · Port = 3306 · 
 · Server = 127.0.0.1

- · Un ejemplo con una conexión a través de una IP y con el uso de credenciales. Por defecto se utiliza una base de datos Zabbix:
```{=html}
<!-- -->
```
 · [TEST_MYSQL_FILLED_CRED] · 
 · Description = MySQL database 2 · 
 · Driver · = mysql · 
 · User = root · 
 · Port = 3306 · 
 · Password = zabbix · 
 · Database = zabbix · 
 · Server = 127.0.0.1 · 

-Un ejemplo con una conexión a través de un socket y con el uso de  credenciales. Se utiliza por defecto una base de datos Zabbix:
```{=html}
<!-- -->
```
 · [TEST_MYSQL_FILLED_CRED_SOCK] · 
 · Description = MySQL database 3 · 
 · Driver · = mysql · 
 · User = root · 
 · Password = zabbix · 
 · Socket = /var/run/mysqld/mysqld.sock · 
 · Database = zabbix

Todas las demás opciones posibles para los parámetros de configuración se pueden encontrar en [oficial documentación MySQL](https://dev.mysql.com/doc/connector-odbc/en/connector-odbc-configuration-connection-parameters.html) web page.

[comment]: # ({/6702bdf1-dcea6601})
