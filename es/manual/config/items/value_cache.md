[comment]: # translation:outdated

[comment]: # ({new-23de852a})
# 11 Value cache

[comment]: # ({/new-23de852a})

[comment]: # ({new-f73e0537})
#### Overview

To make the calculation of trigger expressions, calculated items and
some macros much faster, a value cache option is supported by the Zabbix
server.

This in-memory cache can be used for accessing historical data, instead
of making direct SQL calls to the database. If historical values are not
present in the cache, the missing values are requested from the database
and the cache updated accordingly.

To enable the value cache functionality, an optional **ValueCacheSize**
parameter is supported by the Zabbix server
[configuration](/manual/appendix/config/zabbix_server) file.

Two internal items are supported for monitoring the value cache:
**zabbix\[vcache,buffer,<mode>\]** and
**zabbix\[vcache,cache,<parameter>\]**. See more details with
[internal items](/manual/config/items/itemtypes/internal).

[comment]: # ({/new-f73e0537})

[comment]: # ({new-3add2a5c})
Item values remain in value cache either until:

-   the item is deleted (cached values are deleted after the next configuration sync);
-   the item value is outside the time or count range specified in the trigger/calculated item expression
    (cached value is removed when a new value is received);
-   the time or count range specified in the trigger/calculated item expression is changed
    so that less data is required for calculation (unnecessary cached values are removed after 24 hours).

::: notetip
Value cache status can be observed by using the server [runtime control](/manual/concepts/server#runtime-control) option
`diaginfo` (or `diaginfo=valuecache`) and inspecting the section for value cache diagnostic information.
This can be useful for determining misconfigured triggers or calculated items.
:::

[comment]: # ({/new-3add2a5c})

[comment]: # ({new-1fb59f91})
To enable the value cache functionality, an optional **ValueCacheSize**
parameter is supported by the Zabbix server
[configuration](/manual/appendix/config/zabbix_server) file.

Two internal items are supported for monitoring the value cache:
**zabbix\[vcache,buffer,<mode>\]** and
**zabbix\[vcache,cache,<parameter>\]**. See more details with
[internal items](/manual/config/items/itemtypes/internal).

[comment]: # ({/new-1fb59f91})
