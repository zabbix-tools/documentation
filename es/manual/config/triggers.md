[comment]: # translation:outdated

[comment]: # ({1b9cf174-e02835c7})
#3 Disparadores

[comment]: # ({/1b9cf174-e02835c7})

[comment]: # ({7dcf64e9-d4a1b8b6})
#### Descripción general

Los disparadores son expresiones lógicas que "evalúan" los datos recopilados por los elementos.
y representan el estado actual del sistema.

Si bien los elementos se utilizan para recopilar datos del sistema, es muy poco práctico
seguir estos datos todo el tiempo esperando una condición que sea alarmante
o merece atención. El trabajo de "evaluar" los datos se puede dejar de desencadenar expresiones.

Las expresiones de activación permiten definir un umbral del estado de los datos
"aceptable". Por lo tanto, si los datos entrantes superan el estado aceptable, se "dispara" un activador o cambia de estado a PROBLEMA.

Un disparador puede tener el siguiente estado:

|VALOR|DESCRIPCIÓN|
|-----|-----------|
|OK|Este es un estado de disparo normal.|
|PROBLEMA|Normalmente significa que algo sucedió. Por ejemplo, la carga del procesador es demasiado alta.|

En un disparador básico, es posible que deseemos establecer un umbral para un período de cinco minutos.
promedio de algunos datos, por ejemplo, la carga de la CPU. Esto se logra por
definiendo una expresión desencadenante donde:

- aplicamos la función 'avg' al valor recibido en la clave del artículo
- utilizamos un período de cinco minutos para la evaluación
- establecemos un umbral de '2'

```{=html}
<!-- -->
```
    promedio (host/clave, 5 m)> 2

Este activador "disparará" (se convertirá en PROBLEMA) si el promedio de cinco minutos es
mas de 2.

En un activador más complejo, la expresión puede incluir una **combinación**
de múltiples funciones y múltiples umbrales. Ver también: [Disparador
expresión](/manual/config/triggers/expresión).

La mayoría de las funciones de activación se evalúan en función de
[historial](/manual/config/items/history_and_trends), mientras que algunos
activar funciones para análisis a largo plazo, p. **promedio de tendencia()**,
**trendcount()**, etc., use datos de tendencia.

[comment]: # ({/7dcf64e9-d4a1b8b6})

[comment]: # ({new-7c263ca6})

::: notetip
After changing a trigger's status from *disabled* to *enabled*, the trigger expression will be evaluated as soon as an item associated with it receives a value or the time to handle a time-based function comes.
:::

[comment]: # ({/new-7c263ca6})

[comment]: # ({new-a45a4e79})

In a more complex trigger, the expression may include a **combination**
of multiple functions and multiple thresholds. See also: [Trigger
expression](/manual/config/triggers/expression).

Most trigger functions are evaluated based on
[history](/manual/config/items/history_and_trends) data, while some
trigger functions for long-term analytics, e.g. **trendavg()**,
**trendcount()**, etc, use trend data.

[comment]: # ({/new-a45a4e79})

[comment]: # ({new-6781ee38})
#### Tiempo de cálculo

Se vuelve a calcular un disparador cada vez que el servidor Zabbix recibe un nuevo valor
que es parte de la expresión. Cuando se recibe un nuevo valor, cada
función que está incluida en la expresión se vuelve a calcular (no sólo
el que recibió el nuevo valor).

Además, un disparador se vuelve a calcular cada vez que se establece un nuevo valor
recibido **y** cada 30 segundos si se utilizan funciones basadas en el tiempo en
la expresion.

Las funciones basadas en el tiempo son **nodata()**, **date()**, **dayofmonth()**,
**día de la semana()**, **hora()**, **ahora()**); se recalculan cada 30
segundos por el proceso sincronizador de historial de Zabbix.

[comment]: # ({/new-6781ee38})

[comment]: # ({fb56420d-1fa7644e})
#### Periodo de evaluacion

Un período de evaluación se utiliza en funciones que hacen referencia al historial del artículo.
Permite especificar el intervalo que nos interesa. Puede ser
especificado como período de tiempo (30s, 10m, 1h) o como un rango de valores (\#5 - para
cinco últimos valores).

El período de evaluación se mide hasta "ahora" - donde "ahora" es el
último tiempo de recálculo del disparador (ver [Cálculo
time](#calculation_time) arriba); "ahora" no es el tiempo "ahora" del
servidor.

El período de evaluación especifica ya sea:

- Para considerar todos los valores entre "período de tiempo ahora" y "ahora" (o, con
    cambio de tiempo, entre "ahora-cambio de tiempo-período de tiempo" y
    "hora actual\_shift")
- Considerar no más que el número de valores del pasado, hasta
    para ahora"
    - Si hay 0 valores disponibles para el período de tiempo o num count
        especificado - entonces el activador o elemento calculado que utiliza este
        la función deja de ser compatible

Tenga en cuenta que:

- Si solo se utiliza una sola función (referenciar el historial de datos) en el
    disparador, "ahora" es siempre el último valor recibido. Por ejemplo, si
    el último valor se recibió hace una hora, el período de evaluación
    considerarse como hasta el último valor de hace una hora.
- Se calcula un nuevo disparador tan pronto como se recibe el primer valor
    (funciones de historia); se calculará en 30 segundos para
    funciones basadas en el tiempo. Por lo tanto, el disparador se calculará incluso
    aunque quizás el período de evaluación establecido (por ejemplo, una hora) ha
    aún no ha pasado desde que se creó el disparador. El gatillo también
    calcularse después del primer valor, aunque la evaluación
    el rango se estableció, por ejemplo, en diez valores más recientes.

[comment]: # ({/fb56420d-1fa7644e})

[comment]: # ({new-cad1df72})

#### Unknown state

It is possible that an unknown operand appears in a trigger expression if:

-   an unsupported item is used
-   the function evaluation for a supported item results in an error

In this case a trigger generally evaluates to "unknown" (although there are some exceptions). For more details, see [Expressions with unknown operands](/manual/config/triggers/expression#expressions-with-unknown-operands).

It is possible to [get notified](/manual/config/events/sources#internal-events) on unknown triggers.

[comment]: # ({/new-cad1df72})

