[comment]: # translation:outdated

[comment]: # ({8be35c9e-31b6b7d8})
#1 Equipos y grupo de equipos

[comment]: # ({/8be35c9e-31b6b7d8})

[comment]: # ({5a412ba2-61c2f2bf})
#### ¿Qué es un "equipo"?

Típicamente los equipos de Zabbix son los dispositivos que desea monitorear (servidores,
estaciones de trabajo, conmutadores, etc.).

La creación de equipos es una de las primeras tareas de monitoreo en Zabbix. Por
ejemplo, si desea monitorear algunos parámetros en un servidor "x", debe
primero debe crear un equipo llamado, digamos, "Servidor X" y luego puede buscarlo
para agregarle elementos de monitoreo.

Los equipos se organizan en grupos de equipos.

Continúe con [crear y configurar un equipo](/manual/config/hosts/host).

[comment]: # ({/5a412ba2-61c2f2bf})
