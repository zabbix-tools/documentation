<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="es" datatype="plaintext" original="manual/config/notifications/action/conditions.md">
    <body>
      <trans-unit id="aa1f9956" xml:space="preserve">
        <source># 1 Conditions</source>
      </trans-unit>
      <trans-unit id="6ef58449" xml:space="preserve">
        <source>#### Overview

It is possible to define that an action is executed only if the event
matches a defined set of conditions. Conditions are set when configuring
the [action](/manual/config/notifications/action#configuring_an_action).

Condition matching is case-sensitive.</source>
      </trans-unit>
      <trans-unit id="6e7e1ac8" xml:space="preserve">
        <source>#### Trigger actions

The following conditions can be used in trigger-based actions:

|Condition type|Supported operators|Description|
|--|--|------|
|*Host group*|equals&lt;br&gt;does not equal|Specify host groups or host groups to exclude.&lt;br&gt;**equals** - event belongs to this host group.&lt;br&gt;**does not equal** - event does not belong to this host group.&lt;br&gt;Specifying a parent host group implicitly selects all nested host groups. To specify the parent group only, all nested groups have to be additionally set with the **does not equal** operator.|
|*Template*|equals&lt;br&gt;does not equal|Specify templates or templates to exclude.&lt;br&gt;**equals** - event belongs to a trigger inherited from this template.&lt;br&gt;**does not equal** - event does not belong to a trigger inherited from this template.|
|*Host*|equals&lt;br&gt;does not equal|Specify hosts or hosts to exclude.&lt;br&gt;**equals** - event belongs to this host.&lt;br&gt;**does not equal** - event does not belong to this host.|
|*Tag name*|equals&lt;br&gt;does not equal&lt;br&gt;contains&lt;br&gt;does not contain|Specify event tag or event tag to exclude.&lt;br&gt;**equals** - event has this tag.&lt;br&gt;**does not equal** - event does not have this tag.&lt;br&gt;**contains** - event has a tag containing this string.&lt;br&gt;**does not contain** - event does not have a tag containing this string.|
|*Tag value*|equals&lt;br&gt;does not equal&lt;br&gt;contains&lt;br&gt;does not contain|Specify event tag and value combination or tag and value combination to exclude.&lt;br&gt;**equals** - event has this tag and value.&lt;br&gt;**does not equal** - event does not have this tag and value.&lt;br&gt;**contains** - event has a tag and value containing these strings.&lt;br&gt;**does not contain** - event does not have a tag and value containing these strings.|
|*Trigger*|equals&lt;br&gt;does not equal|Specify triggers or triggers to exclude.&lt;br&gt;**equals** - event is generated by this trigger.&lt;br&gt;**does not equal** - event is generated by any other trigger, except this one.|
|*Trigger name*|contains&lt;br&gt;does not contain|Specify a string in the trigger name or a string to exclude.&lt;br&gt;**contains** - event is generated by a trigger, containing this string in the name.&lt;br&gt;**does not contain** - this string cannot be found in the trigger name.&lt;br&gt;Note: Entered value will be compared to trigger name with all macros expanded.|
|*Trigger severity*|equals&lt;br&gt;does not equal&lt;br&gt;is greater than or equals&lt;br&gt;is less than or equals|Specify trigger severity.&lt;br&gt;**equals** - equal to trigger severity.&lt;br&gt;**does not equal** - not equal to trigger severity.&lt;br&gt;**is greater than or equals** - more or equal to trigger severity.&lt;br&gt;**is less than or equals** - less or equal to trigger severity.|
|*Time period*|in&lt;br&gt;not in|Specify a time period or a time period to exclude.&lt;br&gt;**in** - event time is within the time period.&lt;br&gt;**not in** - event time is not within the time period.&lt;br&gt;See the [time period specification](/manual/appendix/time_period) page for description of the format.&lt;br&gt;[User macros](/manual/config/macros/user_macros) are supported, since Zabbix 3.4.0.|
|*Problem is suppressed*|no&lt;br&gt;yes|Specify if the problem is suppressed (not shown) because of host maintenance.&lt;br&gt;**no** - problem is not suppressed.&lt;br&gt;**yes** - problem is suppressed.&lt;br&gt;|</source>
      </trans-unit>
      <trans-unit id="79be7a27" xml:space="preserve">
        <source>#### Service actions

The following conditions can be used in service actions:

|Condition type|Supported operators|Description|
|--|--|------|
|*Service*|equals&lt;br&gt;does not equal|Specify a service or a service to exclude.&lt;br&gt;**equals** - event belongs to this service.&lt;br&gt;**does not equal** - event does not belong to this service.&lt;br&gt;Specifying a parent service implicitly selects all child services. To specify the parent service only, all nested services have to be additionally set with the **does not equal** operator.|
|*Service name*|contains&lt;br&gt;does not contain|Specify a string in the service name or a string to exclude.&lt;br&gt;**contains** - event is generated by a service, containing this string in the name.&lt;br&gt;**does not contain** - this string cannot be found in the service name.|
|*Service tag name*|equals&lt;br&gt;does not equal&lt;br&gt;contains&lt;br&gt;does not contain|Specify an event tag or an event tag to exclude. Service event tags can be defined in the service configuration section *Tags*.&lt;br&gt;**equals** - event has this tag.&lt;br&gt;**does not equal** - event does not have this tag.&lt;br&gt;**contains** - event has a tag containing this string.&lt;br&gt;**does not contain** - event does not have a tag containing this string.|
|*Service tag value*|equals&lt;br&gt;does not equal&lt;br&gt;contains&lt;br&gt;does not contain|Specify an event tag and value combination or a tag and value combination to exclude. Service event tags can be defined in the service configuration section *Tags*.&lt;br&gt;**equals** - event has this tag and value.&lt;br&gt;**does not equal** - event does not have this tag and value.&lt;br&gt;**contains** - event has a tag and value containing these strings.&lt;br&gt;**does not contain** - event does not have a tag and value containing these strings.|

:::noteimportant
Make sure to define [message templates](/manual/config/notifications/media#overview) for Service actions in the *Alerts → Media types* menu.
Otherwise, the notifications will not be sent. 
:::</source>
      </trans-unit>
      <trans-unit id="6df27208" xml:space="preserve">
        <source>#### Discovery actions

The following conditions can be used in discovery-based events:

|Condition type|Supported operators|Description|
|--|--|------|
|*Host IP*|equals&lt;br&gt;does not equal|Specify an IP address range or a range to exclude for a discovered host.&lt;br&gt;**equals** - host IP is in the range.&lt;br&gt;**does not equal** - host IP is not in the range.&lt;br&gt;It may have the following formats:&lt;br&gt;Single IP: 192.168.1.33&lt;br&gt;Range of IP addresses: 192.168.1-10.1-254&lt;br&gt;IP mask: 192.168.4.0/24&lt;br&gt;List: 192.168.1.1-254, 192.168.2.1-100, 192.168.2.200, 192.168.4.0/24&lt;br&gt;Support for spaces in the list format is provided since Zabbix 3.0.0.|
|*Service type*|equals&lt;br&gt;does not equal|Specify a service type of a discovered service or a service type to exclude.&lt;br&gt;**equals** - matches the discovered service.&lt;br&gt;**does not equal** - does not match the discovered service.&lt;br&gt;Available service types: SSH, LDAP, SMTP, FTP, HTTP, HTTPS *(available since Zabbix 2.2 version)*, POP, NNTP, IMAP, TCP, Zabbix agent, SNMPv1 agent, SNMPv2 agent, SNMPv3 agent, ICMP ping, telnet *(available since Zabbix 2.2 version)*.|
|*Service port*|equals&lt;br&gt;does not equal|Specify a TCP port range of a discovered service or a range to exclude.&lt;br&gt;**equals** - service port is in the range.&lt;br&gt;**does not equal** - service port is not in the range.|
|*Discovery rule*|equals&lt;br&gt;does not equal|Specify a discovery rule or a discovery rule to exclude.&lt;br&gt;**equals** - using this discovery rule.&lt;br&gt;**does not equal** - using any other discovery rule, except this one.|
|*Discovery check*|equals&lt;br&gt;does not equal|Specify a discovery check or a discovery check to exclude.&lt;br&gt;**equals** - using this discovery check.&lt;br&gt;**does not equal** - using any other discovery check, except this one.|
|*Discovery object*|equals|Specify the discovered object.&lt;br&gt;**equals** - equal to discovered object (a device or a service).|
|*Discovery status*|equals|**Up** - matches 'Host Up' and 'Service Up' events.&lt;br&gt;**Down** - matches 'Host Down' and 'Service Down' events.&lt;br&gt;**Discovered** - matches 'Host Discovered' and 'Service Discovered' events.&lt;br&gt;**Lost** - matches 'Host Lost' and 'Service Lost' events.|
|*Uptime/Downtime*|is greater than or equals&lt;br&gt;is less than or equals|Uptime for 'Host Up' and 'Service Up' events. Downtime for 'Host Down' and 'Service Down' events.&lt;br&gt;**is greater than or equals** - is more or equal to. Parameter is given in seconds.&lt;br&gt;**is less than or equals** - is less or equal to. Parameter is given in seconds.|
|*Received value*|equals&lt;br&gt;does not equal&lt;br&gt;is greater than or equals&lt;br&gt;is less than or equals&lt;br&gt;contains&lt;br&gt;does not contain|Specify the value received from an agent (Zabbix, SNMP) check in a discovery rule. String comparison. If several Zabbix agent or SNMP checks are configured for a rule, received values for each of them are checked (each check generates a new event which is matched against all conditions).&lt;br&gt;**equals** - equal to the value.&lt;br&gt;**does not equal** - not equal to the value.&lt;br&gt;**is greater than or equals** - more or equal to the value.&lt;br&gt;**is less than or equals** - less or equal to the value.&lt;br&gt;**contains** - contains the substring. Parameter is given as a string.&lt;br&gt;**does not contain** - does not contain the substring. Parameter is given as a string.|
|*Proxy*|equals&lt;br&gt;does not equal|Specify a proxy or a proxy to exclude.&lt;br&gt;**equals** - using this proxy.&lt;br&gt;**does not equal** - using any other proxy except this one.|

::: noteclassic
Service checks in a discovery rule, which result in
discovery events, do not take place simultaneously. Therefore, if
**multiple** values are configured for `Service type`, `Service port` or
`Received value` conditions in the action, they will be compared to one
discovery event at a time, but **not** to several events simultaneously.
As a result, actions with multiple values for the same check types may
not be executed correctly.
:::</source>
      </trans-unit>
      <trans-unit id="e9b18b74" xml:space="preserve">
        <source>#### Autoregistration actions

The following conditions can be used in actions based on active agent
autoregistration:

|Condition type|Supported operators|Description|
|--|--|------|
|*Host metadata*|contains&lt;br&gt;does not contain&lt;br&gt;matches&lt;br&gt;does not match|Specify host metadata or host metadata to exclude.&lt;br&gt;**contains** - host metadata contains the string.&lt;br&gt;**does not contain** - host metadata does not contain the string.&lt;br&gt;Host metadata can be specified in an [agent configuration file](/manual/appendix/config/zabbix_agentd).&lt;br&gt;**matches** - host metadata matches regular expression.&lt;br&gt;**does not match** - host metadata does not match regular expression.|
|*Host name*|contains&lt;br&gt;does not contain&lt;br&gt;matches&lt;br&gt;does not match|Specify a host name or a host name to exclude.&lt;br&gt;**contains** - host name contains the string.&lt;br&gt;**does not contain** - host name does not contain the string.&lt;br&gt;**matches** - host name matches regular expression.&lt;br&gt;**does not match** - host name does not match regular expression.|
|*Proxy*|equals&lt;br&gt;does not equal|Specify a proxy or a proxy to exclude.&lt;br&gt;**equals** - using this proxy.&lt;br&gt;**does not equal** - using any other proxy except this one.|</source>
      </trans-unit>
      <trans-unit id="133d6ae1" xml:space="preserve">
        <source>#### Internal event actions

The following conditions can be set for actions based on internal
events:

|Condition type|Supported operators|Description|
|--|--|------|
|*Event type*|equals|**Item in "not supported" state** - matches events where an item goes from a 'normal' to 'not supported' state.&lt;br&gt;**Low-level discovery rule in "not supported" state** - matches events where a low-level discovery rule goes from a 'normal' to 'not supported' state.&lt;br&gt;**Trigger in "unknown" state** - matches events where a trigger goes from a 'normal' to 'unknown' state.|
|*Host group*|equals&lt;br&gt;does not equal|Specify host groups or host groups to exclude.&lt;br&gt;**equals** - event belongs to this host group.&lt;br&gt;**does not equal** - event does not belong to this host group.|
|*Tag name*|equals&lt;br&gt;does not equal&lt;br&gt;contains&lt;br&gt;does not contain|Specify event tag or event tag to exclude.&lt;br&gt;**equals** - event has this tag.&lt;br&gt;**does not equal** - event does not have this tag.&lt;br&gt;**contains** - event has a tag containing this string.&lt;br&gt;**does not contain** - event does not have a tag containing this string.|
|*Tag value*|equals&lt;br&gt;does not equal&lt;br&gt;contains&lt;br&gt;does not contain|Specify event tag and value combination or tag and value combination to exclude.&lt;br&gt;**equals** - event has this tag and value.&lt;br&gt;**does not equal** - event does not have this tag and value.&lt;br&gt;**contains** - event has a tag and value containing these strings.&lt;br&gt;**does not contain** - event does not have a tag and value containing these strings.|
|*Template*|equals&lt;br&gt;does not equal|Specify templates or templates to exclude.&lt;br&gt;**equals** - event belongs to an item/trigger/low-level discovery rule inherited from this template.&lt;br&gt;**does not equal** - event does not belong to an item/trigger/low-level discovery rule inherited from this template.|
|*Host*|equals&lt;br&gt;does not equal|Specify hosts or hosts to exclude.&lt;br&gt;**equals** - event belongs to this host.&lt;br&gt;**does not equal** - event does not belong to this host.|</source>
      </trans-unit>
      <trans-unit id="8c003f8b" xml:space="preserve">
        <source>#### Type of calculation

The following options of calculating conditions are available:

-   **And** - all conditions must be met

Note that using "And" calculation is disallowed between several triggers
when they are selected as a `Trigger=` condition. Actions can only be
executed based on the event of one trigger.

-   **Or** - enough if one condition is met
-   **And/Or** - combination of the two: AND with different condition
    types and OR with the same condition type, for example:

*Host group* equals Oracle servers\
*Host group* equals MySQL servers\
*Trigger name* contains 'Database is down'\
*Trigger name* contains 'Database is unavailable'

is evaluated as

**(**Host group equals Oracle servers **or** Host group equals MySQL
servers**)** **and** **(**Trigger name contains 'Database is down'
**or** Trigger name contains 'Database is unavailable'**)**

-   **Custom expression** - a user-defined calculation formula for
    evaluating action conditions. It must include all conditions
    (represented as uppercase letters A, B, C, ...) and may include
    spaces, tabs, brackets ( ), **and** (case sensitive), **or** (case
    sensitive), **not** (case sensitive).

While the previous example with `And/Or` would be represented as (A or
B) and (C or D), in a custom expression you may as well have multiple
other ways of calculation:

(A and B) and (C or D)\
(A and B) or (C and D)\
((A or B) and C) or D\
(not (A or B) and C) or not D\
etc.</source>
      </trans-unit>
      <trans-unit id="fca3d6de" xml:space="preserve">
        <source>#### Actions disabled due to deleted objects

If a certain object (host, template, trigger, etc.) used in an action
condition/operation is deleted, the condition/operation is removed and
the action is disabled to avoid incorrect execution of the action. The
action can be re-enabled by the user.

This behavior takes place when deleting:

-   host groups ("host group" condition, "remote command" operation on a specific host group);
-   hosts ("host" condition, "remote command" operation on a specific host);
-   templates ("template" condition, "link template" and "unlink template" operations);
-   triggers ("trigger" condition);
-   discovery rules (when using "discovery rule" and "discovery check" conditions).

:::notetip
If a remote command has many target hosts, and we delete one of
them, only this host will be removed from the target list, the operation
itself will remain. But, if it's the only host, the operation will be
removed, too. The same goes for "link template" and "unlink
template" operations.
:::

Actions are not disabled when deleting a user or user group used in a
"send message" operation.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
