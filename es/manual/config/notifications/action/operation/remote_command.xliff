<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="es" datatype="plaintext" original="manual/config/notifications/action/operation/remote_command.md">
    <body>
      <trans-unit id="0cdc5e2e" xml:space="preserve">
        <source># 2 Remote commands</source>
      </trans-unit>
      <trans-unit id="36945095" xml:space="preserve">
        <source>#### Overview

With remote commands you can define that a certain pre-defined command
is automatically executed on the monitored host upon some condition.

Thus remote commands are a powerful mechanism for smart pro-active
monitoring.

In the most obvious uses of the feature you can try to:

-   Automatically restart some application (web server, middleware, CRM)
    if it does not respond
-   Use IPMI 'reboot' command to reboot some remote server if it does
    not answer requests
-   Automatically free disk space (removing older files, cleaning /tmp)
    if running out of disk space
-   Migrate a VM from one physical box to another depending on the CPU
    load
-   Add new nodes to a cloud environment upon insufficient CPU (disk,
    memory, whatever) resources

Configuring an action for remote commands is similar to that for sending
a message, the only difference being that Zabbix will execute a command
instead of sending a message.

Remote commands can be executed by Zabbix server, proxy or agent. Remote
commands on Zabbix agent can be executed directly by Zabbix server or
through Zabbix proxy. Both on Zabbix agent and Zabbix proxy remote
commands are disabled by default. They can be enabled by:

-   adding an `AllowKey=system.run[*]` parameter in agent configuration;
-   setting the EnableRemoteCommands parameter to '1' in proxy
    configuration.

Remote commands executed by Zabbix server are run as described in
[Command execution](/manual/appendix/command_execution) including exit
code checking.

Remote commands are executed even if the target host is in maintenance.</source>
      </trans-unit>
      <trans-unit id="7ca4c9c3" xml:space="preserve">
        <source>##### Remote command limit

Remote command limit after resolving all macros depends on the type of
database and character set (non- ASCII characters require more than one
byte to be stored):

|   |   |   |
|---|---|---|
|*Database*|*Limit in characters*|*Limit in bytes*|
|**MySQL**|65535|65535|
|**Oracle Database**|2048|4000|
|**PostgreSQL**|65535|not limited|
|**SQLite (only Zabbix proxy)**|65535|not limited|

The following tutorial provides step-by-step instructions on how to set
up remote commands.</source>
      </trans-unit>
      <trans-unit id="349859a6" xml:space="preserve">
        <source>#### Configuration

Those remote commands that are executed on Zabbix agent (custom scripts)
must be first enabled in the agent
[configuration](/manual/appendix/config/zabbix_agentd).

Make sure that the AllowKey=system.run[&lt;command&gt;,\*] parameter is added for each allowed command in agent configuration to allow specific command with nowait mode. Restart agent daemon if changing this parameter.

Then, when configuring a new action in *Alerts → Actions → Trigger actions*:

1.  Define the appropriate conditions,
    for example, set that the action is activated upon any disaster problems with one of Apache applications.

![](../../../../../../assets/en/manual/config/notifications/action/conditions_restart.png){width="600"}

2.  In the [*Operations*](/manual/config/notifications/action/operation#configuring_an_operation) tab,
    click on *Add* in the *Operations*, *Recovery operations*, or *Update operations* block.

![](../../../../../../assets/en/manual/config/notifications/action/operations.png){width="600"}

3.  Select one of the predefined scripts from the *Operation* dropdown list and set the *Target list* for the script.

![](../../../../../../assets/en/manual/config/notifications/action/operation_restart_webserver.png){width="600"}</source>
      </trans-unit>
      <trans-unit id="c5296379" xml:space="preserve">
        <source>#### Predefined scripts

Scripts that are available for action operations (webhook, script, SSH, Telnet, IPMI) are defined in 
[global scripts](/manual/web_interface/frontend_sections/alerts/scripts).

For example:

    sudo /etc/init.d/apache restart 

In this case, Zabbix will try to restart an Apache process. With this
command, make sure that the command is executed on Zabbix agent (click
the *Zabbix agent* button against *Execute on*).

::: noteimportant
Note the use of **sudo** - Zabbix user does not
have permissions to restart system services by default. See below for
hints on how to configure **sudo**.
:::

::: noteclassic
Starting with Zabbix agent 7.0, remote commands can also be executed on an agent that is operating in active mode.
Zabbix agent - whether active or passive - should run on the remote host, and executes the commands in background.
:::

Remote commands on Zabbix agent are executed without timeout by the
system.run\[,nowait\] key and are not checked for execution results. On
Zabbix server and Zabbix proxy, remote commands are executed with
timeout as set in the TrapperTimeout parameter of zabbix\_server.conf or
zabbix\_proxy.conf file and are
[checked](/manual/appendix/command_execution#exit_code_checking) for
execution results. For additional information, see [*Script timeout*](/manual/web_interface/frontend_sections/alerts/scripts#script-timeout).</source>
      </trans-unit>
      <trans-unit id="a1b1e7ea" xml:space="preserve">
        <source>#### Access permissions

Make sure that the 'zabbix' user has execute permissions for configured
commands. One may be interested in using **sudo** to give access to
privileged commands. To configure access, execute as root:

    visudo

Example lines that could be used in *sudoers* file:

    # allows 'zabbix' user to run all commands without password.
    zabbix ALL=NOPASSWD: ALL

    # allows 'zabbix' user to restart apache without password.
    zabbix ALL=NOPASSWD: /etc/init.d/apache restart

::: notetip
On some systems *sudoers* file will prevent non-local
users from executing commands. To change this, comment out
**requiretty** option in */etc/sudoers*.
:::</source>
      </trans-unit>
      <trans-unit id="1f3becef" xml:space="preserve">
        <source>#### Remote commands with multiple interfaces

If the target system has multiple interfaces of the selected type
(Zabbix agent or IPMI), remote commands will be executed on the default
interface.

It is possible to execute remote commands via SSH and Telnet using
another interface than the Zabbix agent one. The available interface to
use is selected in the following order:

* Zabbix agent default interface
* SNMP default interface
* JMX default interface
* IPMI default interface</source>
      </trans-unit>
      <trans-unit id="26b199db" xml:space="preserve">
        <source>#### IPMI remote commands

For IPMI remote commands the following syntax should be used:

    &lt;command&gt; [&lt;value&gt;]

where

-   &lt;command&gt; - one of IPMI commands without spaces
-   &lt;value&gt; - 'on', 'off' or any unsigned integer. &lt;value&gt;
    is an optional parameter.</source>
      </trans-unit>
      <trans-unit id="c230845b" xml:space="preserve">
        <source>#### Examples

Examples of [global
scripts](/manual/web_interface/frontend_sections/alerts/scripts#configuring_a_global_script)
that may be used as remote commands in action operations.</source>
      </trans-unit>
      <trans-unit id="1d68d18d" xml:space="preserve">
        <source>##### Example 1

Restart of Windows on certain condition.

In order to automatically restart Windows upon a problem detected by
Zabbix, define the following script:

|Script parameter|Value|
|--|--------|
|*Scope*|'Action operation'|
|*Type*|'Script'|
|*Command*|c:\\windows\\system32\\shutdown.exe -r -f|</source>
      </trans-unit>
      <trans-unit id="8e3d9a17" xml:space="preserve">
        <source>##### Example 2

Restart the host by using IPMI control.

|Script parameter|Value|
|--|--------|
|*Scope*|'Action operation'|
|*Type*|'IPMI'|
|*Command*|reset|</source>
      </trans-unit>
      <trans-unit id="c9d4e7c6" xml:space="preserve">
        <source>##### Example 3

Power off the host by using IPMI control.

|Script parameter|Value|
|--|--------|
|*Scope*|'Action operation'|
|*Type*|'IPMI'|
|*Command*|power off|</source>
      </trans-unit>
    </body>
  </file>
</xliff>
