[comment]: # translation:outdated

[comment]: # ({new-11452be8})
# 2 Custom graphs

[comment]: # ({/new-11452be8})

[comment]: # ({new-12dcf0a4})
#### Overview

Custom graphs, as the name suggests, offer customization capabilities.

While simple graphs are good for viewing data of a single item, they do
not offer configuration capabilities.

Thus, if you want to change graph style or the way lines are displayed
or compare several items, for example, incoming and outgoing traffic in
a single graph, you need a custom graph.

Custom graphs are configured manually.

They can be created for a host or several hosts or for a single
template.

[comment]: # ({/new-12dcf0a4})

[comment]: # ({new-96c61306})
#### Configuring custom graphs

To create a custom graph, do the following:

-   Go to *Configuration → Hosts (or Templates)*
-   Click on *Graphs* in the row next to the desired host or template
-   In the Graphs screen click on *Create graph*
-   Edit graph attributes

![](../../../../../assets/en/manual/config/visualization/graph2.png){width="600"}

All mandatory input fields are marked with a red asterisk.

Graph attributes:

|Parameter|<|Description|
|---------|-|-----------|
|*Name*|<|Unique graph name.<br>Expression [macros](/manual/appendix/macros/supported_by_location) are supported in this field, but only with `avg`, `last`, `min` and `max` functions, with time as parameter (for example, `{?avg(/host/key,1h)}`).<br>{HOST.HOST<1-9>} macros are supported for the use within this macro, referencing the first, second, third, etc. host in the graph, for example `{?avg(/{HOST.HOST2}/key,1h)}`. Note that referencing the first host with this macro is redundant, as the first host can be referenced implicitly, for example `{?avg(//key,1h)}`.|
|*Width*|<|Graph width in pixels (for preview and pie/exploded graphs only).|
|*Height*|<|Graph height in pixels.|
|*Graph type*|<|Graph type:<br>**Normal** - normal graph, values displayed as lines<br>**Stacked** - stacked graph, filled areas displayed<br>**Pie** - pie graph<br>**Exploded** - "exploded" pie graph, portions displayed as "cut out" of the pie|
|*Show legend*|<|Checking this box will set to display the graph legend.|
|*Show working time*|<|If selected, non-working hours will be shown with a gray background. Not available for pie and exploded pie graphs.|
|*Show triggers*|<|If selected, simple triggers will be displayed as lines with black dashes over trigger severity color. Not available for pie and exploded pie graphs.|
|*Percentile line (left)*|<|Display percentile for left Y-axis. If, for example, 95% percentile is set, then the percentile line will be at the level where 95 percent of the values fall under. Displayed as a bright green line. Only available for normal graphs.|
|*Percentile line (right)*|<|Display percentile for right Y-axis. If, for example, 95% percentile is set, then the percentile line will be at the level where 95 percent of the values fall under. Displayed as a bright red line. Only available for normal graphs.|
|*Y axis MIN value*|<|Minimum value of Y-axis:<br>**Calculated** - Y axis minimum value will be automatically calculated<br>**Fixed** - fixed minimum value for Y-axis. Not available for pie and exploded pie graphs.<br>**Item** - last value of the selected item will be the minimum value|
|*Y axis MAX value*|<|Maximum value of Y-axis:<br>**Calculated** - Y axis maximum value will be automatically calculated<br>**Fixed** - fixed maximum value for Y-axis. Not available for pie and exploded pie graphs.<br>**Item** - last value of the selected item will be the maximum value|
|*3D view*|<|Enable 3D style. For pie and exploded pie graphs only.|
|*Items*|<|Items, data of which are to be displayed in this graph. Click on *Add* to select items. You can also select various displaying options (function, draw style, left/right axis display, color).|
|<|*Sort order (0→100)*|Draw order. 0 will be processed first. Can be used to draw lines or regions behind (or in front of) another.<br>You can drag and drop items by the arrow at the beginning of a line to set the sort order or which item is displayed in front of the other.|
|^|*Name*|Name of the selected item is displayed as a link. Clicking on the link opens the list of other available items.|
|^|*Type*|Type (only available for pie and exploded pie graphs):<br>**Simple** - the value of the item is represented proportionally on the pie<br>**Graph sum** - the value of the item represents the whole pie<br>Note that coloring of the "graph sum" item will only be visible to the extent that it is not taken up by "proportional" items.|
|^|*Function*|Select what values will be displayed when more than one value exists per vertical graph pixel for an item:<br>**all** - display all possible values (minimum, maximum, average) in the graph. Note that for shorter periods this setting has no effect; only for longer periods, when data congestion in a vertical graph pixel increases, 'all' starts displaying minimum, maximum, and average values. This function is only available for *Normal* graph type. See also: Generating graphs [from history/trends](/manual/config/visualization/graphs/simple#generating_from_historytrends).<br>**avg** - display the average values<br>**last** - display the latest values. This function is only available if either *Pie/Exploded pie* is selected as graph type.<br>**max** - display the maximum values<br>**min** - display the minimum values|
|^|*Draw style*|Select the draw style (only available for normal graphs; for stacked graphs filled region is always used) to apply to the item data - *Line*, *Bold line*, *Filled region*, *Dot*, *Dashed line*, *Gradient line*.|
|^|*Y axis side*|Select the Y axis side to show the item data - *Left*, *Right*.|
|^|*Color*|Select the color to apply to the item data.|

[comment]: # ({/new-96c61306})

[comment]: # ({new-f02a35a5})
##### Graph preview

In the *Preview* tab, a preview of the graph is displayed so you can
immediately see what you are creating.

![](../../../../../assets/en/manual/config/visualization/graph_preview.png){width="600"}

Note that the preview will not show any data for template items.

![](../../../../../assets/en/manual/config/visualization/graph_preview2.png){width="600"}

In this example, pay attention to the dashed bold line displaying the
trigger level and the trigger information displayed in the legend.

::: noteclassic
No more than 3 trigger lines can be displayed. If there are
more triggers then the triggers with lower severity are prioritized for
display.\
\
If graph height is set as less than 120 pixels, no trigger will be
displayed in the legend.
:::

[comment]: # ({/new-f02a35a5})
