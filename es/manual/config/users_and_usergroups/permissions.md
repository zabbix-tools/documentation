[comment]: # translation:outdated

[comment]: # ({new-ba72dbf1})
# 2 Permissions

[comment]: # ({/new-ba72dbf1})

[comment]: # ({new-a9157075})
#### Overview

You can differentiate user permissions in Zabbix by defining the
respective user role. Then the unprivileged users need to be included in
user groups that have access to host group data.

[comment]: # ({/new-a9157075})

[comment]: # ({new-ac897fff})

#### User types

Permissions in Zabbix depend, primarily, on the user type:

   - *User* - has limited access rights to menu sections (see below) and no access to any resources by default. Any permissions to host or template groups must be explicitly assigned;
   - *Admin* - has incomplete access rights to menu sections (see below). The user has no access to any host groups by default. Any permissions to host or template groups must be explicitly given;
   - *Super admin* - has access to all menu sections. The user has a read-write access to all host and template groups. Permissions cannot be revoked by denying access to specific groups.

**Menu access**

The following table illustrates access to Zabbix menu sections per user type:

|Menu section|<|User|Admin|Super admin|
|-|-------------|------|------|------|
|**Dashboards**|<|+|+|+|
|**Monitoring**|<|+|+|+|
| |*Problems*|+|+|+|
|^|*Hosts*|+|+|+|
|^|*Latest data*|+|+|+|
|^|*Maps*|+|+|+|
|^|*Discovery*| |+|+|
|**Services**|<|+|+|+|
| |*Services*|+|+|+|
|^|*SLA*| |+|+|
|^|*SLA report*|+|+|+|
|**Inventory**|<|+|+|+|
| |*Overview*|+|+|+|
|^|*Hosts*|+|+|+|
|**Reports**|<|+|+|+|
| |*System information*| | |+|
|^|*Scheduled reports*| |+|+|
|^|*Availability report*|+|+|+|
|^|*Triggers top 100*|+|+|+|
|^|*Audit log*| | |+|
|^|*Action log*| | |+|
|^|*Notifications*| |+|+|
|**Data collection**|<| |+|+|
| |*Template groups*| |+|+|
|^|*Host groups*| |+|+|
|^|*Templates*| |+|+|
|^|*Hosts*| |+|+|
|^|*Maintenance*| |+|+|
|^|*Event correlation*| | |+|
|^|*Discovery*| |+|+|
|**Alerts**|<| |+|+|
| |*Trigger actions*| |+|+|
| |*Service actions*| |+|+|
| |*Discovery actions*| |+|+|
| |*Autoregistration actions*| |+|+|
| |*Internal actions*| |+|+|
|^|*Media types*| | |+|
|^|*Scripts*| | |+|
|**Users**|<| | |+|
| |*User groups*| | |+|
|^|*User roles*| | |+|
|^|*Users*| | |+|
|^|*API tokens*| | |+|
|^|*Authentication*| | |+|
|**Administration**|<| | |+|
| |*General*| | |+|
|^|*Audit log*| | |+|
|^|*Housekeeping*| | |+|
|^|*Proxies*| | |+|
|^|*Macros*| | |+|
|^|*Queue*| | |+|

[comment]: # ({/new-ac897fff})

[comment]: # ({new-a852da1b})
#### User role

The user role defines which parts of UI, which API methods, and which
actions are available to the user. The following roles are pre-defined
in Zabbix:

|User type|Description|
|---------|-----------|
|*Guest role*|The user has access to the Monitoring, Inventory, and Reports menu sections, but without the rights to perform any actions.|
|*User role*|The user has access to the Monitoring, Inventory, and Reports menu sections. The user has no access to any resources by default. Any permissions to host groups must be explicitly assigned.|
|*Admin role*|The user has access to the Monitoring, Inventory, Reports and Configuration menu sections. The user has no access to any host groups by default. Any permissions to host groups must be explicitly given.|
|*Super Admin role*|The user has access to all menu sections. The user has a read-write access to all host groups. Permissions cannot be revoked by denying access to specific host groups.|

[User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
are configured in the *Administration→User roles* section. Super Admins
can modify or delete pre-defined roles and create more roles with custom
sets of permissions.

To assign a role to the user, go to the Permissions tab in the user
configuration form, locate the *Role* field and select a role. Once a
role is selected a list of associated permissions will be displayed
below.

![user\_permissions.png](../../../../assets/en/manual/config/user_permissions.png)

[comment]: # ({/new-a852da1b})

[comment]: # ({new-f38cb50e})
#### Permissions to host groups

Access to any host data in Zabbix is granted to [user
groups](/manual/config/users_and_usergroups/usergroup) on the host group
level only.

That means that an individual user cannot be directly granted access to
a host (or host group). It can only be granted access to a host by being
part of a user group that is granted access to the host group that
contains the host.

[comment]: # ({/new-f38cb50e})
