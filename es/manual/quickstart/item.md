[comment]: # translation:outdated

[comment]: # ({new-8cf8bd5f})
# 3 New item

[comment]: # ({/new-8cf8bd5f})

[comment]: # ({new-73502ab6})
#### Overview

In this section, you will learn how to set up an item.

Items are the basis of gathering data in Zabbix. Without items, there is
no data - because only an item defines a single metric or what kind of
data to collect from a host.

[comment]: # ({/new-73502ab6})

[comment]: # ({new-8cca3d66})
#### Adding item

All items are grouped around hosts. That is why to configure a sample
item we go to *Configuration → Hosts* and find the "New host" we have
created.

Click on the *Items* link in the row of "New host", and then click on
*Create item*. This will present us with an item definition form.

![](../../../assets/en/manual/quickstart/new_item.png)

All mandatory input fields are marked with a red asterisk.

For our sample item, the essential information to enter is:

***Name***

-   Enter *CPU load* as the value. This will be the item name displayed
    in lists and elsewhere.

***Key***

-   Manually enter *system.cpu.load* as the value. This is the technical
    name of an item that identifies the type of information that will be
    gathered. The particular key is just one of [pre-defined
    keys](/manual/config/items/itemtypes/zabbix_agent) that come with
    Zabbix agent.

***Type of information***

-   This attribute defines the format of the expected data. For the
    *system.cpu.load* key, this field will be automatically set to
    *Numeric (float)*.

::: noteclassic
You may also want to reduce the number of days [item
history](/manual/config/items/history_and_trends) will be kept, to 7 or
14. This is good practice to relieve the database from keeping lots of
historical values.
:::

[Other options](/manual/config/items/item#configuration) will suit us
with their defaults for now.

When done, click *Add*. The new item should appear in the item list.
Click on *Details* above the list to view what exactly was done.

![](../../../assets/en/manual/quickstart/item_created.png)

[comment]: # ({/new-8cca3d66})

[comment]: # ({new-c33e07f1})
#### Seeing data

With an item defined, you might be curious if it is actually gathering
data. For that, go to *Monitoring → Latest data*, select 'New host' in
the filter and click on *Apply*.

![](../../../assets/en/manual/quickstart/latest_data.png){width="600"}

With that said, it may take up to 60 seconds for the first data to
arrive. That, by default, is how often the server reads configuration
changes and picks up new items to execute.

If you see no value in the 'Change' column, maybe only one value has
been received so far. Wait 30 seconds for another value to arrive.

If you do not see information about the item as in the screenshot, make
sure that:

-   you have filled out the item 'Key' and 'Type of information' fields
    exactly as in the screenshot
-   both the agent and the server are running
-   host status is 'Monitored' and its availability icon is green
-   a host is selected in the host dropdown, the item is active

[comment]: # ({/new-c33e07f1})

[comment]: # ({new-7e0cbbcd})
##### Graphs

With the item working for a while, it might be time to see something
visual. [Simple graphs](/manual/config/visualization/graphs/simple) are
available for any monitored numeric item without any additional
configuration. These graphs are generated on runtime.

To view the graph, go to *Monitoring → Latest data* and click on the
'Graph' link next to the item.

![](../../../assets/en/manual/quickstart/simple_graph.png){width="600"}

[comment]: # ({/new-7e0cbbcd})
