[comment]: # translation:outdated

[comment]: # ({new-923acd1d})
# 5 Media types

[comment]: # ({/new-923acd1d})

[comment]: # ({new-e0b517c9})
#### Overview

Media types are [exported](/manual/xml_export_import) with all related
objects and object relations.

[comment]: # ({/new-e0b517c9})

[comment]: # ({new-7e0a36b8})
#### Exporting

To export media types, do the following:

-   Go to: *Administration* → *Media types*
-   Mark the checkboxes of the media types to export
-   Click on *Export* below the list

![](../../../assets/en/manual/xml_export_import/export_mediatypes.png)

Depending on the selected format, media types are exported to a local
file with a default name:

-   *zabbix\_export\_mediatypes.yaml* - in YAML export (default option
    for export)
-   *zabbix\_export\_mediatypes.xml* - in XML export
-   *zabbix\_export\_mediatypes.json* - in JSON export

[comment]: # ({/new-7e0a36b8})

[comment]: # ({new-7f828bf2})
#### Importing

To import media types, do the following:

-   Go to: *Administration* → *Media types*
-   Click on *Import* to the right
-   Select the import file
-   Mark the required options in import rules
-   Click on *Import*

![](../../../assets/en/manual/xml_export_import/import_media.png)

A success or failure message of the import will be displayed in the
frontend.

Import rules:

|Rule|Description|
|----|-----------|
|*Update existing*|Existing elements will be updated with data taken from the import file. Otherwise they will not be updated.|
|*Create new*|The import will add new elements using data from the import file. Otherwise it will not add them.|
|*Delete missing*|The import will remove existing elements not present in the import file. Otherwise it will not remove them.|

[comment]: # ({/new-7f828bf2})

[comment]: # ({new-480f411a})
#### Export format

Export to YAML:

```yaml
zabbix_export:
  version: '6.0'
  date: '2021-08-31T13:34:17Z'
  media_types:
    -
      name: Pushover
      type: WEBHOOK
      parameters:
        -
          name: endpoint
          value: 'https://api.pushover.net/1/messages.json'
        -
          name: eventid
          value: '{EVENT.ID}'
        -
          name: event_nseverity
          value: '{EVENT.NSEVERITY}'
        -
          name: event_source
          value: '{EVENT.SOURCE}'
        -
          name: event_value
          value: '{EVENT.VALUE}'
        -
          name: expire
          value: '1200'
        -
          name: message
          value: '{ALERT.MESSAGE}'
        -
          name: priority_average
          value: '0'
        -
          name: priority_default
          value: '0'
        -
          name: priority_disaster
          value: '0'
        -
          name: priority_high
          value: '0'
        -
          name: priority_information
          value: '0'
        -
          name: priority_not_classified
          value: '0'
        -
          name: priority_warning
          value: '0'
        -
          name: retry
          value: '60'
        -
          name: title
          value: '{ALERT.SUBJECT}'
        -
          name: token
          value: '<PUSHOVER TOKEN HERE>'
        -
          name: triggerid
          value: '{TRIGGER.ID}'
        -
          name: url
          value: '{$ZABBIX.URL}'
        -
          name: url_title
          value: Zabbix
        -
          name: user
          value: '{ALERT.SENDTO}'
      max_sessions: '0'
      script: |
        try {
            var params = JSON.parse(value),
                request = new HttpRequest(),
                data,
                response,
                severities = [
                    {name: 'not_classified', color: '#97AAB3'},
                    {name: 'information', color: '#7499FF'},
                    {name: 'warning', color: '#FFC859'},
                    {name: 'average', color: '#FFA059'},
                    {name: 'high', color: '#E97659'},
                    {name: 'disaster', color: '#E45959'},
                    {name: 'resolved', color: '#009900'},
                    {name: 'default', color: '#000000'}
                ],
                priority;
        
            if (typeof params.HTTPProxy === 'string' && params.HTTPProxy.trim() !== '') {
                request.setProxy(params.HTTPProxy);
            }
        
            if ([0, 1, 2, 3].indexOf(parseInt(params.event_source)) === -1) {
                throw 'Incorrect "event_source" parameter given: "' + params.event_source + '".\nMust be 0-3.';
            }
        
            if (params.event_value !== '0' && params.event_value !== '1'
                && (params.event_source === '0' || params.event_source === '3')) {
                throw 'Incorrect "event_value" parameter given: ' + params.event_value + '\nMust be 0 or 1.';
            }
        
            if ([0, 1, 2, 3, 4, 5].indexOf(parseInt(params.event_nseverity)) === -1) {
                params.event_nseverity = '7';
            }
        
            if (params.event_value === '0') {
                params.event_nseverity = '6';
            }
        
            priority = params['priority_' + severities[params.event_nseverity].name] || params.priority_default;
        
            if (isNaN(priority) || priority < -2 || priority > 2) {
                throw '"priority" should be -2..2';
            }
        
            if (params.event_source === '0' && isNaN(params.triggerid)) {
                throw 'field "triggerid" is not a number';
            }
        
            if (isNaN(params.eventid)) {
                throw 'field "eventid" is not a number';
            }
        
            if (typeof params.message !== 'string' || params.message.trim() === '') {
                throw 'field "message" cannot be empty';
            }
        
            data = {
                token: params.token,
                user: params.user,
                title: params.title,
                message: params.message,
                url: (params.event_source === '0') 
                    ? params.url + '/tr_events.php?triggerid=' + params.triggerid + '&eventid=' + params.eventid
                    : params.url,
                url_title: params.url_title,
                priority: priority
            };
        
            if (priority == 2) {
                if (isNaN(params.retry) || params.retry < 30) {
                    throw 'field "retry" should be a number with value of at least 30 if "priority" is set to 2';
                }
        
                if (isNaN(params.expire) || params.expire > 10800) {
                    throw 'field "expire" should be a number with value of at most 10800 if "priority" is set to 2';
                }
        
                data.retry = params.retry;
                data.expire = params.expire;
            }
        
            data = JSON.stringify(data);
            Zabbix.log(4, '[ Pushover Webhook ] Sending request: ' + params.endpoint + '\n' + data);
        
            request.addHeader('Content-Type: application/json');
            response = request.post(params.endpoint, data);
        
            Zabbix.log(4, '[ Pushover Webhook ] Received response with status code ' + request.getStatus() + '\n' + response);
        
            if (response !== null) {
                try {
                    response = JSON.parse(response);
                }
                catch (error) {
                    Zabbix.log(4, '[ Pushover Webhook ] Failed to parse response received from Pushover');
                    response = null;
                }
            }
        
            if (request.getStatus() != 200 || response === null || typeof response !== 'object' || response.status !== 1) {
                if (response !== null && typeof response === 'object' && typeof response.errors === 'object'
                        && typeof response.errors[0] === 'string') {
                    throw response.errors[0];
                }
                else {
                    throw 'Unknown error. Check debug log for more information.';
                }
            }
        
            return 'OK';
        }
        catch (error) {
            Zabbix.log(4, '[ Pushover Webhook ] Pushover notification failed: ' + error);
            throw 'Pushover notification failed: ' + error;
        }
      description: |
        Please refer to setup guide here: https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/pushover
        
        Set token parameter with to your Pushover application key.
        When assigning Pushover media to the Zabbix user - add user key into send to field.
      message_templates:
        -
          event_source: TRIGGERS
          operation_mode: PROBLEM
          subject: 'Problem: {EVENT.NAME}'
          message: |
            Problem started at {EVENT.TIME} on {EVENT.DATE}
            Problem name: {EVENT.NAME}
            Host: {HOST.NAME}
            Severity: {EVENT.SEVERITY}
            Operational data: {EVENT.OPDATA}
            Original problem ID: {EVENT.ID}
            {TRIGGER.URL}
        -
          event_source: TRIGGERS
          operation_mode: RECOVERY
          subject: 'Resolved in {EVENT.DURATION}: {EVENT.NAME}'
          message: |
            Problem has been resolved at {EVENT.RECOVERY.TIME} on {EVENT.RECOVERY.DATE}
            Problem name: {EVENT.NAME}
            Problem duration: {EVENT.DURATION}
            Host: {HOST.NAME}
            Severity: {EVENT.SEVERITY}
            Original problem ID: {EVENT.ID}
            {TRIGGER.URL}
        -
          event_source: TRIGGERS
          operation_mode: UPDATE
          subject: 'Updated problem in {EVENT.AGE}: {EVENT.NAME}'
          message: |
            {USER.FULLNAME} {EVENT.UPDATE.ACTION} problem at {EVENT.UPDATE.DATE} {EVENT.UPDATE.TIME}.
            {EVENT.UPDATE.MESSAGE}
            
            Current problem status is {EVENT.STATUS}, age is {EVENT.AGE}, acknowledged: {EVENT.ACK.STATUS}.
        -
          event_source: DISCOVERY
          operation_mode: PROBLEM
          subject: 'Discovery: {DISCOVERY.DEVICE.STATUS} {DISCOVERY.DEVICE.IPADDRESS}'
          message: |
            Discovery rule: {DISCOVERY.RULE.NAME}
            
            Device IP: {DISCOVERY.DEVICE.IPADDRESS}
            Device DNS: {DISCOVERY.DEVICE.DNS}
            Device status: {DISCOVERY.DEVICE.STATUS}
            Device uptime: {DISCOVERY.DEVICE.UPTIME}
            
            Device service name: {DISCOVERY.SERVICE.NAME}
            Device service port: {DISCOVERY.SERVICE.PORT}
            Device service status: {DISCOVERY.SERVICE.STATUS}
            Device service uptime: {DISCOVERY.SERVICE.UPTIME}
        -
          event_source: AUTOREGISTRATION
          operation_mode: PROBLEM
          subject: 'Autoregistration: {HOST.HOST}'
          message: |
            Host name: {HOST.HOST}
            Host IP: {HOST.IP}
            Agent port: {HOST.PORT}
```

[comment]: # ({/new-480f411a})

[comment]: # ({new-f0190043})
#### Element tags

Element tag values are explained in the table below.

|Element|Element property|Required|Type|Range^**[1](#footnotes)**^|Description|
|-------|----------------|--------|----|--------------------------|-----------|
|media\_types|<|\-|<|<|Root element for media\_types.|
|<|name|x|`string`|<|Media type name.|
|<|type|x|`string`|0 - EMAIL<br>1 - SMS<br>2 - SCRIPT<br>4 - WEBHOOK|Transport used by the media type.|
|<|status|\-|`string`|0 - ENABLED (default)<br>1 - DISABLED|Whether the media type is enabled.|
|<|max\_sessions|\-|`integer`|Possible values for SMS: 1 - (default)<br><br>Possible values for other media types: 0-100, 0 - unlimited|The maximum number of alerts that can be processed in parallel.|
|<|attempts|\-|`integer`|1-10 (default: 3)|The maximum number of attempts to send an alert.|
|<|attempt\_interval|\-|`string`|0-60s (default: 10s)|The interval between retry attempts.<br><br>Accepts seconds and time unit with suffix.|
|<|description|\-|`string`|<|Media type description.|
|message\_templates|<|\-|<|<|Root element for media type message templates.|
|<|event\_source|x|`string`|0 - TRIGGERS<br>1 - DISCOVERY<br>2 - AUTOREGISTRATION<br>3 - INTERNAL|Event source.|
|<|operation\_mode|x|`string`|0 - PROBLEM<br>1 - RECOVERY<br>2 - UPDATE|Operation mode.|
|<|subject|\-|`string`|<|Message subject.|
|<|message|\-|`string`|<|Message body.|
|Used only by e-mail media type|<|<|<|<|<|
|<|smtp\_server|x|`string`|<|SMTP server.|
|<|smtp\_port|\-|`integer`|Default: 25|SMTP server port to connect to.|
|<|smtp\_helo|x|`string`|<|SMTP helo.|
|<|smtp\_email|x|`string`|<|Email address from which notifications will be sent.|
|<|smtp\_security|\-|`string`|0 - NONE (default)<br>1 - STARTTLS<br>2 - SSL\_OR\_TLS|SMTP connection security level to use.|
|<|smtp\_verify\_host|\-|`string`|0 - NO (default)<br>1 - YES|SSL verify host for SMTP. Optional if smtp\_security is STARTTLS or SSL\_OR\_TLS.|
|<|smtp\_verify\_peer|\-|`string`|0 - NO (default)<br>1 - YES|SSL verify peer for SMTP. Optional if smtp\_security is STARTTLS or SSL\_OR\_TLS.|
|<|smtp\_authentication|\-|`string`|0 - NONE (default)<br>1 - PASSWORD|SMTP authentication method to use.|
|<|username|\-|`string`|<|Username.|
|<|password|\-|`string`|<|Authentication password.|
|<|content\_type|\-|`string`|0 - TEXT<br>1 - HTML (default)|Message format.|
|Used only by SMS media type|<|<|<|<|<|
|<|gsm\_modem|x|`string`|<|Serial device name of the GSM modem.|
|Used only by script media type|<|<|<|<|<|
|<|script name|x|`string`|<|Script name.|
|parameters|<|\-|<|<|Root element for script parameters.|
|Used only by webhook media type|<|<|<|<|<|
|<|script|x|`string`|<|Script.|
|<|timeout|\-|`string`|1-60s (default: 30s)|Javascript script HTTP request timeout interval.|
|<|process\_tags|\-|`string`|0 - NO (default)<br>1 - YES|Whether to process returned tags.|
|<|show\_event\_menu|\-|`string`|0 - NO (default)<br>1 - YES|If {EVENT.TAGS.\*} were successfully resolved in event\_menu\_url and event\_menu\_name fields, this field indicates presence of entry in the event menu.|
|<|event\_menu\_url|\-|`string`|<|URL of the event menu entry. Supports {EVENT.TAGS.\*} macro.|
|<|event\_menu\_name|\-|`string`|<|Name of the event menu entry. Supports {EVENT.TAGS.\*} macro.|
|parameters|<|\-|<|<|Root element for webhook media type parameters.|
|<|name|x|`string`|<|Webhook parameter name.|
|<|value|\-|`string`|<|Webhook parameter value.|

[comment]: # ({/new-f0190043})

[comment]: # ({new-c1b1c6ab})
##### Footnotes

^**1**^ For string values, only the string will be exported (e.g.
"EMAIL") without the numbering used in this table. The numbers for range
values (corresponding to the API values) in this table is used for
ordering only.

[comment]: # ({/new-c1b1c6ab})
