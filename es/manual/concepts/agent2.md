[comment]: # translation:outdated

[comment]: # ({0db5260c-d60be363})
# 3 Agente 2

[comment]: # ({/0db5260c-d60be363})

[comment]: # ({new-734dc3ed})
#### Descripción general

Zabbix agent 2 es una nueva generación de Zabbix agent y se puede usar en
lugar del agente de Zabbix. El agente 2 de Zabbix ha sido desarrollado para:

- reducir el número de conexiones TCP
- tener mayor concurrencia 
- ser fácilmente extensible con complementos. Un complemento debe ser capaz de:
    - proporcionar revisiones triviales que consisten en solo unas pocas simples líneas de
        código
    - proporcionar comprobaciones complejas que consisten en scripts de larga ejecución y
        recopilación de datos independiente con envío periódico de los datos
- ser un reemplazo directo para el agente Zabbix (en el sentido de que es compatible con todos
    la funcionalidad anterior)

El agente 2 está escrito en Go (con algún código C del agente Zabbix reutilizado). Un 
entorno Go configurado con un [Go
version](https://golang.org/doc/devel/release#policy) es necesario para
construir el agente 2 de Zabbix.

Agent 2 no tiene soporte de daemonización incorporado en Linux; puede ser
ejecutar como [servicio de Windows](/manual/appendix/install/windows_agent).

Los controles pasivos funcionan de manera similar al agente de Zabbix. Las revisiones activas soportan
intervalos programados/flexibles y verifica la concurrencia dentro de un servidor activo
.

**Comprobar concurrencia**

Las comprobaciones de diferentes complementos se pueden ejecutar al mismo tiempo. El número
de comprobaciones simultáneas dentro de un complemento está limitada por el ajuste de capacidad del complemento
. Cada complemento puede tener una configuración de capacidad codificada (siendo 100
predeterminado) que se puede reducir con el
Ajuste `Plugins.<Nombre del complemento>.Capacity=N` en *Complementos*
configuración [parámetro](#configuration_file).

Ver también: [Desarrollo de complementos
directrices] (https://www.zabbix.com/documentation/guidelines/plugins).

[comment]: # ({/new-734dc3ed})

[comment]: # ({new-eaa0f2bb})
##### Passive and active checks

Passive checks work similarly to Zabbix agent. Active checks support
scheduled/flexible intervals and check concurrency within one active
server. 

::: noteclassic
  By default, after a restart, Zabbix agent 2 will schedule the first data collection for active checks at a conditionally random time within the item's update interval to prevent spikes in resource usage. To perform active checks that do not have *Scheduling* [update interval](/manual/config/items/item/custom_intervals#scheduling-intervals) immediately after the agent restart, set `ForceActiveChecksOnStart` parameter (global-level) or `Plugins.<Plugin name>.System.ForceActiveChecksOnStart` (affects only specific plugin checks) in the [configuration file](/manual/appendix/config/zabbix_agent2). Plugin-level parameter, if set, will override the global parameter. Forcing active checks on start is supported since Zabbix 6.0.2. 
:::

[comment]: # ({/new-eaa0f2bb})

[comment]: # ({new-05ea1336})
##### Check concurrency

Checks from different plugins can be executed concurrently. The number
of concurrent checks within one plugin is limited by the plugin capacity
setting. Each plugin may have a hardcoded capacity setting (100 being
default) that can be lowered using the `Plugins.<PluginName>.System.Capacity=N` setting in the *Plugins*
configuration [parameter](#configuration_file). Former name of this parameter `Plugins.<PluginName>.Capacity` is still supported, but has been deprecated in Zabbix 6.0.

[comment]: # ({/new-05ea1336})

[comment]: # ({38286af4-a2064519})
#### Plataformas compatibles

Agent 2 es compatible con las plataformas Linux y Windows.

Si se instala desde paquetes, el Agente 2 es compatible con:

-RHEL/CentOS 6, 7, 8
-SLES 15 SP1+
-Debian 9, 10
-Ubuntu 18.04, 20.04

En Windows Agent 2 es compatible con:

- Windows Server 2008 R2 y posterior
- Windows 7 y posteriores

[comment]: # ({/38286af4-a2064519})

[comment]: # ({37f854b5-cabc0f5f})
#### Instalación

Zabbix agent 2 está disponible en paquetes Zabbix precompilados. Compilar
Agente 2 de Zabbix de las fuentes, debe especificar la opción de configuración`--enable-agent2`

[comment]: # ({/37f854b5-cabc0f5f})

[comment]: # ({8b0810c9-45c02f12})
#### Opciones

Los siguientes parámetros de línea de comando se pueden usar con el agente 2 de Zabbix:

|**Parámetro**|**Descripción**|
|-------------|---------------|
|-c --config <archivo de configuración>|Ruta al archivo de configuración.<br>Puede usar esta opción para especificar un archivo de configuración que no sea el predeterminado.<br>En UNIX, el predeterminado es /usr/local /etc/zabbix\_agent2.conf o según lo establecido por [compile-time](/manual/installation/install#installing_zabbix_daemons) variables *--sysconfdir* o *--prefix*|
|-f --foreground|Ejecutar el agente Zabbix en primer plano (predeterminado: verdadero).|
|-p --print|Imprimir elementos conocidos y salir.<br>*Nota*: Para devolver también los resultados de [parámetro de usuario](/manual/config/items/userparameters), debe especificar el archivo de configuración (si es no en la ubicación predeterminada).|
|-t --test <elemento clave>|Prueba el elemento especificado y sale.<br>*Nota*: Para devolver también los resultados de [parámetro de usuario](/manual/config/items/userparameters), debe especificar el archivo de configuración (si no está en la ubicación predeterminada).|
|-h --help|Imprime información de ayuda y sale.|
|-v --verbose|Imprimir información de depuración. Utilice esta opción con las opciones -p y -t.|
|-V --version|Imprime el número de versión del agente y sale.|
|-R --runtime-control <opción>|Realizar funciones administrativas. Consulte [control de tiempo de ejecución](/manual/concepts/agent2#runtime_control).|

**Ejemplos** específicos del uso de parámetros de línea de comando:

- imprimir todos los elementos de agente integrados con valores
- probar un parámetro de usuario con la clave "mysql.ping" definida en el especificado
    archivo de configuración

```{=html}
<!-- -->
```
    shell> zabbix_agent2 --imprimir
    shell> zabbix_agent2 -t "mysql.ping" -c /etc/zabbix/zabbix_agentd.conf

[comment]: # ({/8b0810c9-45c02f12})

[comment]: # ({88b126cc-11aa0de0})
##### Control de tiempo de ejecución

El control de tiempo de ejecución proporciona algunas opciones para el control remoto.

|Opción|Descripción|
|------|-----------|
|log\_level\_increase|Aumentar el nivel de registro.|
|log\_level\_decrease|Reducir el nivel de registro.|
|métricas|Lista de métricas disponibles.|
|versión|Mostrar versión del agente.|
|userparameter\_reload|Recargar parámetros de usuario desde el archivo de configuración actual.<br>Tenga en cuenta que UserParameter es la única opción de configuración del agente que se recargará.|
|ayuda|Muestra información de ayuda sobre el control de tiempo de ejecución.|

Ejemplos:

- aumento del nivel de registro para el agente 2
- opciones de control de tiempo de ejecución de impresión

```{=html}
<!-- -->
```
    shell> zabbix_agent2 -R log_level_increase
    shell> zabbix_agent2 -R ayuda

[comment]: # ({/88b126cc-11aa0de0})

[comment]: # ({12df2b52-77e9b590})
#### Archivo de configuración

Los parámetros de configuración del agente 2 son en su mayoría compatibles con
Agente Zabbix con algunas excepciones.

|Nuevos parámetros|Descripción|
|--------------|-----------|
|*ControlSocket*|La ruta del socket de control en tiempo de ejecución. El agente 2 usa un socket de control para [comandos de tiempo de ejecución] (#runtime_control).|
|*EnablePersistentBuffer*,<br>*PersistentBufferFile*,<br>*PersistentBufferPeriod*|Estos parámetros se utilizan para configurar el almacenamiento persistente en el agente 2 para elementos activos.|
|*Complementos*|Los complementos pueden tener sus propios parámetros, en el formato `Complementos.<Nombre del complemento>.<Parámetro>=<valor>`. Un parámetro de complemento común es *Capacidad*, que establece el límite de comprobaciones que se pueden ejecutar al mismo tiempo.|
|*StatusPort*|El agente de puerto 2 escuchará la solicitud de estado HTTP y mostrará una lista de complementos configurados y algunos parámetros internos|
|Parámetros descartados|Descripción|
|*AllowRoot*, *User*|No se admite porque no se admite la daemonización.|
|*LoadModule*, *LoadModulePath*|Los módulos cargables no son compatibles.|
|*StartAgents*|Este parámetro se usó en el agente Zabbix para aumentar la concurrencia de verificación pasiva o deshabilitarla. En el Agente 2, la concurrencia se configura a nivel de complemento y se puede limitar mediante una configuración de capacidad. Mientras que actualmente no se admite la desactivación de comprobaciones pasivas.|
|*HostInterface*, *HostInterfaceItem*|Todavía no compatible.|

Para obtener más detalles, consulte las opciones del archivo de configuración para
[zabbix\_agent2](/manual/appendix/config/zabbix_agent2).

[comment]: # ({/12df2b52-77e9b590})

[comment]: # ({ac52b5eb-472820ac})
#### Códigos de salida

A partir de la versión 4.4.8 Zabbix agent 2 también se puede compilar con
versiones anteriores de OpenSSL (1.0.1, 1.0.2).

En este caso, Zabbix proporciona mutexes para bloquear OpenSSL. Si un mutex
el bloqueo o desbloqueo falla, entonces se imprime un mensaje de error en el 
flujo de error estándar (STDERR) y el Agente 2 sale con el código de retorno 2 o 3,
respectivamente.

[comment]: # ({/ac52b5eb-472820ac})
