<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="es" datatype="plaintext" original="manual/appendix/config/zabbix_agent2_plugins/mongodb_plugin.md">
    <body>
      <trans-unit id="f68118e5" xml:space="preserve">
        <source># 5 MongoDB plugin</source>
      </trans-unit>
      <trans-unit id="a831e9b6" xml:space="preserve">
        <source>#### Overview

This section lists parameters supported in the MongoDB Zabbix agent 2 plugin configuration file (mongo.conf).

This is a loadable plugin, which is available and fully described in the [MongoDB plugin repository](https://git.zabbix.com/projects/AP/repos/mongodb/browse).

Note that:

-   The default values reflect process defaults, not the values in the
    shipped configuration files;
-   Zabbix supports configuration files only in UTF-8 encoding without
    [BOM](https://en.wikipedia.org/wiki/Byte_order_mark);
-   Comments starting with "\#" are only supported at the beginning of
    the line.</source>
      </trans-unit>
      <trans-unit id="eaba6cbf" xml:space="preserve">
        <source>
#### Options

|Parameter|Description|
|---------|-----------|
|-V --version|Print the plugin version and license information.|
|-h --help|Print help information (shorthand).|</source>
      </trans-unit>
      <trans-unit id="83498ede" xml:space="preserve">
        <source>
#### Parameters

|Parameter|Mandatory|Range|Default|Description|
|--|--|--|--|----------|
|Plugins.MongoDB.Default.Password|no| | |Default password for connecting to MongoDB; used if no value is specified in an item key or named session.|
|Plugins.MongoDB.Default.Uri|no| | |Default URI for connecting to MongoDB; used if no value is specified in an item key or named session. &lt;br&gt;&lt;br&gt;Should not include embedded credentials (they will be ignored).&lt;br&gt;Must match the URI format.&lt;br&gt;Only `tcp` scheme is supported; a scheme can be omitted.&lt;br&gt;A port can be omitted (default=27017).&lt;br&gt;Examples: `tcp://127.0.0.1:27017`, `tcp:localhost`, `localhost`|
|Plugins.MongoDB.Default.User|no| | |Default username for connecting to MongoDB; used if no value is specified in an item key or named session.|
|Plugins.MongoDB.KeepAlive|no|60-900|300|The maximum time of waiting (in seconds) before unused plugin connections are closed.|
|Plugins.MongoDB.Sessions.&lt;SessionName&gt;.Password|no| | |Named session password.&lt;br&gt;**&lt;SessionName&gt;** - define name of a session for using in item keys.|
|Plugins.MongoDB.Sessions.&lt;SessionName&gt;.TLSCAFile|no &lt;br&gt;(yes, if Plugins.MongoDB.Sessions.&lt;SessionName&gt;.TLSConnect is set to one of: verify_ca, verify_full)| | |Full pathname of a file containing the top-level CA(s) certificates for peer certificate verification, used for encrypted communications between Zabbix agent 2 and monitored databases.&lt;br&gt;**&lt;SessionName&gt;** - define name of a session for using in item keys. |
|Plugins.MongoDB.Sessions.&lt;SessionName&gt;.TLSCertFile|no &lt;br&gt;(yes, if Plugins.MongoDB.Sessions.&lt;SessionName&gt;.TLSConnect is set to one of: verify_ca, verify_full)| | |Full pathname of a file containing the agent certificate or certificate chain, used for encrypted communications between Zabbix agent 2 and monitored databases.&lt;br&gt;**&lt;SessionName&gt;** - define name of a session for using in item keys. |
|Plugins.MongoDB.Sessions.&lt;SessionName&gt;.TLSConnect|no | | |Encryption type for communications between Zabbix agent 2 and monitored databases.&lt;br&gt;**&lt;SessionName&gt;** - define name of a session for using in item keys.&lt;br&gt;&lt;br&gt;Supported values:&lt;br&gt;*required* - require TLS connection;&lt;br&gt;*verify\_ca* - verify certificates;&lt;br&gt;*verify\_full* - verify certificates and IP address. &lt;br&gt;&lt;br&gt; Supported since plugin version 1.2.1|
|Plugins.MongoDB.Sessions.&lt;SessionName&gt;.TLSKeyFile|no &lt;br&gt;(yes, if Plugins.MongoDB.Sessions.&lt;SessionName&gt;.TLSConnect is set to one of: verify_ca, verify_full)| | |Full pathname of a file containing the database private key used for encrypted communications between Zabbix agent 2 and monitored databases.&lt;br&gt;**&lt;SessionName&gt;** - define name of a session for using in item keys. |
|Plugins.MongoDB.Sessions.&lt;SessionName&gt;.Uri|no| | |Connection string of a named session.&lt;br&gt;**&lt;SessionName&gt;** - define name of a session for using in item keys.&lt;br&gt;&lt;br&gt;Should not include embedded credentials (they will be ignored).&lt;br&gt;Must match the URI format.&lt;br&gt;Only `tcp` scheme is supported; a scheme can be omitted.&lt;br&gt;A port can be omitted (default=27017).&lt;br&gt;Examples: `tcp://127.0.0.1:27017`, `tcp:localhost`, `localhost`|
|Plugins.MongoDB.Sessions.&lt;SessionName&gt;.User|no| | |Named session username.&lt;br&gt;**&lt;SessionName&gt;** - define name of a session for using in item keys.|
|Plugins.MongoDB.System.Path|no| | |Path to plugin executable.|
|Plugins.MongoDB.Timeout|no|1-30|global timeout|Request execution timeout (how long to wait for a request to complete before shutting it down).|

See also:

-   Description of general Zabbix agent 2 configuration parameters:
    [Zabbix agent 2 (UNIX)](/manual/appendix/config/zabbix_agent2) /
    [Zabbix agent 2
    (Windows)](/manual/appendix/config/zabbix_agent2_win)
-   Instructions for configuring [plugins](/manual/extensions/plugins)</source>
      </trans-unit>
    </body>
  </file>
</xliff>
