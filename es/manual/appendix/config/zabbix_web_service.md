[comment]: # translation:outdated

[comment]: # ({new-66a8ced4})
# 9 Zabbix web service

[comment]: # ({/new-66a8ced4})

[comment]: # ({new-132b4156})
#### Overview

Zabbix web service is a process that is used for communication with
external web services.

This section lists parameters supported in Zabbix web service
configuration file (zabbix\_web\_service.conf). Note that:

-   The default values reflect process defaults, not the values in the
    shipped configuration files;
-   Zabbix supports configuration files only in UTF-8 encoding without
    [BOM](https://en.wikipedia.org/wiki/Byte_order_mark);
-   Comments starting with "\#" are only supported at the beginning of
    the line.

[comment]: # ({/new-132b4156})

[comment]: # ({new-69d7c913})
### Parameter details

[comment]: # ({/new-69d7c913})

[comment]: # ({new-0f829aa7})
##### AllowedIP
A list of comma delimited IP addresses, optionally in CIDR notation, or DNS names of Zabbix servers and Zabbix proxies. Incoming connections will be accepted only from the hosts listed here.<br>If IPv6 support is enabled then `127.0.0.1`, `::127.0.0.1`, `::ffff:127.0.0.1` are treated equally and `::/0` will allow any IPv4 or IPv6 address. `0.0.0.0/0` can be used to allow any IPv4 address.

Example: 

    127.0.0.1,192.168.1.0/24,::1,2001:db8::/32,zabbix.example.com

Mandatory: yes

[comment]: # ({/new-0f829aa7})

[comment]: # ({new-e5288ea7})
##### DebugLevel

Specify the debug level:<br>*0* - basic information about starting and stopping of Zabbix processes<br>*1* - critical information;<br>*2* - error information;<br>*3* - warnings;<br>*4* - for debugging (produces lots of information);<br>*5* - extended debugging (produces even more information).

Default: `3`<br>
Range: 0-5

[comment]: # ({/new-e5288ea7})

[comment]: # ({new-e99b72c0})
##### ListenPort

The service will listen on this port for connections from the server.

Default: `10053`<br>
Range: 1024-32767

[comment]: # ({/new-e99b72c0})

[comment]: # ({new-bb5252d8})
##### LogFile

The name of the log file.

Example:

    /tmp/zabbix_web_service.log

Mandatory: Yes, if LogType is set to *file*; otherwise no

[comment]: # ({/new-bb5252d8})

[comment]: # ({new-778f1edc})
##### LogFileSize

The maximum size of a log file in MB.<br>0 - disable automatic log rotation.<br>*Note*: If the log file size limit is reached and file rotation fails, for whatever reason, the existing log file is truncated and started anew.

Default: `1`<br>
Range: 0-1024

[comment]: # ({/new-778f1edc})

[comment]: # ({new-9d26f327})
##### LogType

The type of the log output:<br>*file* - write log to the file specified by LogFile parameter;<br>*system* - write log to syslog;<br>*console* - write log to standard output.

Default: `file`

[comment]: # ({/new-9d26f327})

[comment]: # ({new-ee64bcdf})
##### Timeout

Spend no more than Timeout seconds on processing.

Default: `3`<br>
Range: 1-30

[comment]: # ({/new-ee64bcdf})

[comment]: # ({new-849f8496})
##### TLSAccept

What incoming connections to accept:<br>*unencrypted* - accept connections without encryption (default)<br>*cert* - accept connections with TLS and a certificate

Default: `unencrypted`

[comment]: # ({/new-849f8496})

[comment]: # ({new-336a620d})
##### TLSCAFile

The full pathname of the file containing the top-level CA(s) certificates for peer certificate verification, used for encrypted communications between Zabbix components.

[comment]: # ({/new-336a620d})

[comment]: # ({new-185ad3c6})
##### TLSCertFile

The full pathname of the file containing the service certificate or certificate chain, used for encrypted communications with Zabbix components.

[comment]: # ({/new-185ad3c6})

[comment]: # ({new-cbcbba0a})
##### TLSKeyFile

The full pathname of the file containing the service private key, used for encrypted communications between Zabbix components.

[comment]: # ({/new-cbcbba0a})

