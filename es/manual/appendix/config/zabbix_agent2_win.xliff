<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="es" datatype="plaintext" original="manual/appendix/config/zabbix_agent2_win.md">
    <body>
      <trans-unit id="4963afdf" xml:space="preserve">
        <source># 6 Zabbix agent 2 (Windows)</source>
      </trans-unit>
      <trans-unit id="9fe069f9" xml:space="preserve">
        <source>### Overview

Zabbix agent 2 is a new generation of Zabbix agent and may be used in
place of Zabbix agent.

The parameters supported by the Windows Zabbix agent 2 configuration file (zabbix\_agent2.win.conf) are listed in this section. 

The parameters are listed without additional information. Click on the parameter to see the full details.

|Parameter|Description|
|--|--------|
|[Alias](#alias)|Sets an alias for an item key.|
|[AllowKey](#allowkey)|Allow the execution of those item keys that match a pattern.|
|[BufferSend](#buffersend)|Do not keep data longer than N seconds in buffer.|
|[BufferSize](#buffersize)|The maximum number of values in the memory buffer.|
|[ControlSocket](#controlsocket)|The control socket, used to send runtime commands with the '-R' option.|
|[DebugLevel](#debuglevel)|The debug level.|
|[DenyKey](#denykey)|Deny the execution of those item keys that match a pattern.|
|[EnablePersistentBuffer](#enablepersistentbuffer)|Enable the usage of local persistent storage for active items.|
|[ForceActiveChecksOnStart](#forceactivechecksonstart)|Perform active checks immediately after the restart for the first received configuration.|
|[HeartbeatFrequency](#heartbeatfrequency)|The frequency of heartbeat messages in seconds.|
|[HostInterface](#hostinterface)|An optional parameter that defines the host interface.|
|[HostInterfaceItem](#hostinterfaceitem)|An optional parameter that defines an item used for getting the host interface.|
|[HostMetadata](#hostmetadata)|An optional parameter that defines the host metadata.|
|[HostMetadataItem](#hostmetadataitem)|An optional parameter that defines a Zabbix agent item used for getting the host metadata.|
|[Hostname](#hostname)|An optional parameter that defines the hostname.|
|[HostnameItem](#hostnameitem)|An optional parameter that defines a Zabbix agent item used for getting the hostname.|
|[Include](#include)|You may include individual files or all files in a directory in the configuration file.|
|[ListenIP](#listenip)|A list of comma-delimited IP addresses that the agent should listen on.|
|[ListenPort](#listenport)|The agent will listen on this port for connections from the server.|
|[LogFile](#logfile)|The name of the log file.|
|[LogFileSize](#logfilesize)|The maximum size of the log file.|
|[LogType](#logtype)|The type of the log output.|
|[PersistentBufferFile](#persistentbufferfile)|The file where Zabbix agent 2 should keep the SQLite database.|
|[PersistentBufferPeriod](#persistentbufferperiod)|The time period for which data should be stored when there is no connection to the server or proxy.|
|[Plugins.Log.MaxLinesPerSecond](#plugins.log.maxlinespersecond)|The maximum number of new lines the agent will send per second to Zabbix server or proxy when processing 'log' and 'logrt' active checks.|
|[Plugins.SystemRun.LogRemoteCommands](#plugins.systemrun.logremotecommands)|Enable the logging of the executed shell commands as warnings.|
|[PluginSocket](#pluginsocket)|The path to the UNIX socket for loadable plugin communications.|
|[PluginTimeout](#plugintimeout)|The timeout for connections with loadable plugins, in seconds.|
|[RefreshActiveChecks](#refreshactivechecks)|How often the list of active checks is refreshed.|
|[Server](#server)|A list of comma-delimited IP addresses, optionally in CIDR notation, or DNS names of Zabbix servers and Zabbix proxies.|
|[ServerActive](#serveractive)|The Zabbix server/proxy address or cluster configuration to get active checks from.|
|[SourceIP](#sourceip)|The source IP address.|
|[StatusPort](#statusport)|If set, the agent will listen on this port for HTTP status requests (http://localhost:&lt;port&gt;/status).|
|[Timeout](#timeout)|Spend no more than Timeout seconds on processing.|
|[TLSAccept](#tlsaccept)|What incoming connections to accept.|
|[TLSCAFile](#tlscafile)|The full pathname of a file containing the top-level CA(s) certificates for peer certificate verification, used for encrypted communications between Zabbix components.|
|[TLSCertFile](#tlscertfile)|The full pathname of a file containing the agent certificate or certificate chain, used for encrypted communications between Zabbix components.|
|[TLSConnect](#tlsconnect)|How the agent should connect to Zabbix server or proxy.|
|[TLSCRLFile](#tlscrlfile)|The full pathname of a file containing revoked certificates. This parameter is used for encrypted communications between Zabbix components.|
|[TLSKeyFile](#tlskeyfile)|The full pathname of a file containing the agent private key, used for encrypted communications between Zabbix components.|
|[TLSPSKFile](#tlspskfile)|The full pathname of a file containing the agent pre-shared key, used for encrypted communications with Zabbix server.|
|[TLSPSKIdentity](#tlspskidentity)|The pre-shared key identity string, used for encrypted communications with Zabbix server.|
|[TLSServerCertIssuer](#tlsservercertissuer)|The allowed server (proxy) certificate issuer.|
|[TLSServerCertSubject](#tlsservercertsubject)|The allowed server (proxy) certificate subject.|
|[UnsafeUserParameters](#unsafeuserparameters)|Allow all characters to be passed in arguments to user-defined parameters.|
|[UserParameter](#userparameter)|A user-defined parameter to monitor.|
|[UserParameterDir](#userparameterdir)|The default search path for UserParameter commands.|

All parameters are non-mandatory unless explicitly stated that the parameter is mandatory. 

Note that:

-   The default values reflect process defaults, not the values in the
    shipped configuration files;
-   Zabbix supports configuration files only in UTF-8 encoding without
    [BOM](https://en.wikipedia.org/wiki/Byte_order_mark);
-   Comments starting with "\#" are only supported in the beginning of
    the line.
&lt;br&gt;</source>
      </trans-unit>
      <trans-unit id="324d4c30" xml:space="preserve">
        <source>### Parameter details</source>
      </trans-unit>
      <trans-unit id="fa6993fd" xml:space="preserve">
        <source>##### Alias

Sets an alias for an item key. It can be used to substitute a long and complex item key with a shorter and simpler one. Multiple *Alias* parameters with the same *Alias* key may be present.&lt;br&gt;Different *Alias* keys may reference the same item key.&lt;br&gt;Aliases can be used in *HostMetadataItem* but not in the *HostnameItem* parameter.

Example 1: Retrieving the paging file usage in percentage from the server.

    Alias=pg_usage:perf_counter[\Paging File(_Total)\% Usage]
    
Now the shorthand key **pg_usage** may be used to retrieve data.

Example 2: Getting the CPU load with default and custom parameters.

    Alias=cpu.load:system.cpu.load
    Alias=cpu.load[*]:system.cpu.load[*]

This allows use **cpu.load** key to get the CPU load with default parameters as well as use **cpu.load[percpu,avg15]** to get specific data about the CPU load.

Example 3: Running multiple [low-level discovery](/manual/discovery/low_level_discovery) rules processing the same discovery items.

    Alias=vfs.fs.discovery[*]:vfs.fs.discovery

Now it is possible to set up several discovery rules using **vfs.fs.discovery** with different parameters for each rule, e.g., **vfs.fs.discovery[foo]**, **vfs.fs.discovery[bar]**, etc.</source>
      </trans-unit>
      <trans-unit id="e05b8a23" xml:space="preserve">
        <source>##### AllowKey

Allow the execution of those item keys that match a pattern. The key pattern is a wildcard expression that supports the "\*" character to match any number of any characters.&lt;br&gt;Multiple key matching rules may be defined in combination with DenyKey. The parameters are processed one by one according to their appearance order. See also: [Restricting agent checks](/manual/config/items/restrict_checks).</source>
      </trans-unit>
      <trans-unit id="5d573779" xml:space="preserve">
        <source>##### BufferSend

The time interval in seconds which determines how often values are sent from the buffer to Zabbix server.&lt;br&gt;Note, that if the buffer is full, the data will be sent sooner.

Default: `5`&lt;br&gt;
Range: 1-3600</source>
      </trans-unit>
      <trans-unit id="adbf2018" xml:space="preserve">
        <source>##### BufferSize

The maximum number of values in the memory buffer. The agent will send all collected data to the Zabbix server or proxy if the buffer is full.&lt;br&gt;This parameter should only be used if persistent buffer is disabled (*EnablePersistentBuffer=0*).

Default: `1000`&lt;br&gt;
Range: 2-65535</source>
      </trans-unit>
      <trans-unit id="dee4d084" xml:space="preserve">
        <source>##### ControlSocket

The control socket, used to send runtime commands with the '-R' option.

Default: `\\.\pipe\agent.sock`</source>
      </trans-unit>
      <trans-unit id="74594585" xml:space="preserve">
        <source>##### DebugLevel

Specify the debug level:&lt;br&gt;*0* - basic information about starting and stopping of Zabbix processes&lt;br&gt;*1* - critical information;&lt;br&gt;*2* - error information;&lt;br&gt;*3* - warnings;&lt;br&gt;*4* - for debugging (produces lots of information);&lt;br&gt;*5* - extended debugging (produces even more information).

Default: `3`&lt;br&gt;
Range: 0-5</source>
      </trans-unit>
      <trans-unit id="ad027e43" xml:space="preserve">
        <source>##### DenyKey

Deny the execution of those item keys that match a pattern. The key pattern is a wildcard expression that supports the "\*" character to match any number of any characters.&lt;br&gt;Multiple key matching rules may be defined in combination with AllowKey. The parameters are processed one by one according to their appearance order. See also: [Restricting agent checks](/manual/config/items/restrict_checks).</source>
      </trans-unit>
      <trans-unit id="d9d538db" xml:space="preserve">
        <source>##### EnablePersistentBuffer

Enable the usage of local persistent storage for active items. If persistent storage is disabled, the memory buffer will be used.

Default: `0`&lt;br&gt;
Values: 0 - disabled, 1 - enabled</source>
      </trans-unit>
      <trans-unit id="1970c2c5" xml:space="preserve">
        <source>##### ForceActiveChecksOnStart

Perform active checks immediately after the restart for the first received configuration. Also available as a per-plugin configuration parameter, for example: `Plugins.Uptime.System.ForceActiveChecksOnStart=1`

Default: `0`&lt;br&gt;
Values: 0 - disabled, 1 - enabled</source>
      </trans-unit>
      <trans-unit id="bce527af" xml:space="preserve">
        <source>##### HeartbeatFrequency

The frequency of heartbeat messages in seconds. Used for monitoring the availability of active checks.&lt;br&gt;0 - heartbeat messages disabled.

Default: `60`&lt;br&gt;
Range: 0-3600</source>
      </trans-unit>
      <trans-unit id="9d3624af" xml:space="preserve">
        <source>##### HostInterface

An optional parameter that defines the host interface. The host interface is used at host [autoregistration](/manual/discovery/auto_registration#using_dns_as_default_interface) process. If not defined, the value will be acquired from HostInterfaceItem.&lt;br&gt;The agent will issue an error and not start if the value is over the limit of 255 characters.

Range: 0-255 characters</source>
      </trans-unit>
      <trans-unit id="7378e07f" xml:space="preserve">
        <source>##### HostInterfaceItem

An optional parameter that defines an item used for getting the host interface.&lt;br&gt;Host interface is used at host [autoregistration](/manual/discovery/auto_registration#using_dns_as_default_interface) process. This option is only used when HostInterface is not defined.&lt;br&gt;The system.run\[\] item is supported regardless of AllowKey/DenyKey values.&lt;br&gt;During an autoregistration request the agent will log a warning message if the value returned by the specified item is over the limit of 255 characters.</source>
      </trans-unit>
      <trans-unit id="3b42353f" xml:space="preserve">
        <source>##### HostMetadata

An optional parameter that defines host metadata. Host metadata is used only at host autoregistration process (active agent). If not defined, the value will be acquired from HostMetadataItem.&lt;br&gt;The agent will issue an error and not start if the specified value is over the limit of 2034 bytes or a non-UTF-8 string.

Range: 0-2034 bytes</source>
      </trans-unit>
      <trans-unit id="eff97518" xml:space="preserve">
        <source>##### HostMetadataItem

An optional parameter that defines an item used for getting host metadata. This option is only used when HostMetadata is not defined. User parameters and aliases are supported. The system.run\[\] item is supported regardless of AllowKey/DenyKey values.&lt;br&gt;The HostMetadataItem value is retrieved on each autoregistration attempt and is used only at host autoregistration process.&lt;br&gt;During an autoregistration request the agent will log a warning message if the value returned by the specified item is over the limit of 65535 UTF-8 code points. The value returned by the item must be a UTF-8 string otherwise it will be ignored.</source>
      </trans-unit>
      <trans-unit id="542b591a" xml:space="preserve">
        <source>##### Hostname

A list of comma-delimited, unique, case-sensitive hostnames. Required for active checks and must match hostnames as configured on the server. The value is acquired from HostnameItem if undefined.&lt;br&gt;Allowed characters: alphanumeric, '.', ' ', '\_' and '-'. Maximum length: 128 characters per hostname, 2048 characters for the entire line.

Default: Set by HostnameItem</source>
      </trans-unit>
      <trans-unit id="085b8ddc" xml:space="preserve">
        <source>##### HostnameItem

An optional parameter that defines an item used for getting the host name. This option is only used when Hostname is not defined. User parameters or aliases are not supported, but the system.run\[\] item is supported regardless of AllowKey/DenyKey values.&lt;br&gt;The output length is limited to 512KB.

Default: `system.hostname`</source>
      </trans-unit>
      <trans-unit id="d06cc342" xml:space="preserve">
        <source>##### Include

You may include individual files or all files in a directory in the configuration file. During the installation Zabbix will create the include directory in /usr/local/etc, unless modified during the compile time. The path can be relative to the *zabbix\_agent2.win.conf* file location.&lt;br&gt;To only include relevant files in the specified directory, the asterisk wildcard character is supported for pattern matching.&lt;br&gt;See [special notes](special_notes_include) about limitations.

Example:

    Include=C:\Program Files\Zabbix Agent\zabbix_agent2.d\*.conf</source>
      </trans-unit>
      <trans-unit id="21abf7b1" xml:space="preserve">
        <source>##### ListenIP

A list of comma-delimited IP addresses that the agent should listen on. The first IP address is sent to the Zabbix server, if connecting to it, to retrieve the list of active checks.

Default: `0.0.0.0`</source>
      </trans-unit>
      <trans-unit id="2f2c8140" xml:space="preserve">
        <source>##### ListenPort

The agent will listen on this port for connections from the server.

Default: `10050`&lt;br&gt;
Range: 1024-32767</source>
      </trans-unit>
      <trans-unit id="988826b6" xml:space="preserve">
        <source>##### LogFile

The name of the agent log file.

Default: `c:\\zabbix_agent2.log`&lt;br&gt;
Mandatory: Yes, if LogType is set to *file*; otherwise no</source>
      </trans-unit>
      <trans-unit id="ae05fc8d" xml:space="preserve">
        <source>##### LogFileSize

The maximum size of a log file in MB.&lt;br&gt;0 - disable automatic log rotation.&lt;br&gt;*Note*: If the log file size limit is reached and file rotation fails, for whatever reason, the existing log file is truncated and started anew.

Default: `1`&lt;br&gt;
Range: 0-1024</source>
      </trans-unit>
      <trans-unit id="e8dc6df9" xml:space="preserve">
        <source>##### LogType

The type of the log output:&lt;br&gt;*file* - write log to the file specified by LogFile parameter;&lt;br&gt;*console* - write log to standard output.

Default: `file`</source>
      </trans-unit>
      <trans-unit id="dde3e6b9" xml:space="preserve">
        <source>##### PersistentBufferFile

The file where Zabbix agent 2 should keep the SQLite database. Must be a full filename. This parameter is only used if persistent buffer is enabled (*EnablePersistentBuffer=1*).</source>
      </trans-unit>
      <trans-unit id="379dc265" xml:space="preserve">
        <source>##### PersistentBufferPeriod

The time period for which data should be stored when there is no connection to the server or proxy. Older data will be lost. Log data will be preserved. This parameter is only used if persistent buffer is enabled (*EnablePersistentBuffer=1*).

Default: `1h`&lt;br&gt;
Range: 1m-365d</source>
      </trans-unit>
      <trans-unit id="c6daef84" xml:space="preserve">
        <source>##### Plugins.Log.MaxLinesPerSecond

The maximum number of new lines the agent will send per second to Zabbix server or proxy when processing 'log', 'logrt' and 'eventlog' active checks. The provided value will be overridden by the 'maxlines' parameter, provided in the 'log', 'logrt' or 'eventlog' item key.&lt;br&gt;*Note*: Zabbix will process 10 times more new lines than set in *MaxLinesPerSecond* to seek the required string in log items.

Default: `20`&lt;br&gt;
Range: 1-1000</source>
      </trans-unit>
      <trans-unit id="d7133c6e" xml:space="preserve">
        <source>##### Plugins.SystemRun.LogRemoteCommands

Enable the logging of the executed shell commands as warnings. The commands will be logged only if executed remotely. Log entries will not be created if system.run\[\] is launched locally by the HostMetadataItem, HostInterfaceItem or HostnameItem parameters.

Default: `0`&lt;br&gt;
Values: 0 - disabled, 1 - enabled</source>
      </trans-unit>
      <trans-unit id="c712bfb8" xml:space="preserve">
        <source>##### PluginSocket

The path to the UNIX socket for loadable plugin communications.

Default: `\\.\pipe\agent.plugin.sock`</source>
      </trans-unit>
      <trans-unit id="1db534a6" xml:space="preserve">
        <source>##### PluginTimeout

The timeout for connections with loadable plugins, in seconds.

Default: `Timeout`&lt;br&gt;
Range: 1-30</source>
      </trans-unit>
      <trans-unit id="25e3871b" xml:space="preserve">
        <source>##### RefreshActiveChecks

How often the list of active checks is refreshed, in seconds. Note that after failing to refresh active checks the next refresh will be attempted in 60 seconds.

Default: `5`&lt;br&gt;
Range: 1-86400</source>
      </trans-unit>
      <trans-unit id="0ad80cbc" xml:space="preserve">
        <source>##### Server

A list of comma-delimited IP addresses, optionally in CIDR notation, or DNS names of Zabbix servers or Zabbix proxies. Incoming connections will be accepted only from the hosts listed here. If IPv6 support is enabled then '127.0.0.1', '::127.0.0.1', '::ffff:127.0.0.1' are treated equally and '::/0' will allow any IPv4 or IPv6 address. '0.0.0.0/0' can be used to allow any IPv4 address. Spaces are allowed.

Example: 

    Server=127.0.0.1,192.168.1.0/24,::1,2001:db8::/32,zabbix.example.com

Mandatory: yes</source>
      </trans-unit>
      <trans-unit id="0d31e03a" xml:space="preserve">
        <source>##### ServerActive

The Zabbix server/proxy address or cluster configuration to get active checks from. The server/proxy address is an IP address or DNS name and optional port separated by colon.&lt;br&gt;The cluster configuration is one or more server addresses separated by semicolon. Multiple Zabbix servers/clusters and Zabbix proxies can be specified, separated by comma. More than one Zabbix proxy should not be specified from each Zabbix server/cluster. If a Zabbix proxy is specified then Zabbix server/cluster for that proxy should not be specified.&lt;br&gt;Multiple comma-delimited addresses can be provided to use several independent Zabbix servers in parallel. Spaces are allowed.&lt;br&gt;If the port is not specified, default port is used.&lt;br&gt;IPv6 addresses must be enclosed in square brackets if port for that host is specified. If port is not specified, square brackets for IPv6 addresses are optional.&lt;br&gt;If this parameter is not specified, active checks are disabled.

Example for Zabbix proxy: 

    ServerActive=127.0.0.1:10051

Example for multiple servers: 

    ServerActive=127.0.0.1:20051,zabbix.domain,\[::1\]:30051,::1,\[12fc::1\]

Example for high availability:

    ServerActive=zabbix.cluster.node1;zabbix.cluster.node2:20051;zabbix.cluster.node3

Example for high availability with two clusters and one server:

    ServerActive=zabbix.cluster.node1;zabbix.cluster.node2:20051,zabbix.cluster2.node1;zabbix.cluster2.node2,zabbix.domain</source>
      </trans-unit>
      <trans-unit id="f96761ad" xml:space="preserve">
        <source>##### SourceIP

The source IP address for:&lt;br&gt;- outgoing connections to Zabbix server or Zabbix proxy;&lt;br&gt;- making connections while executing some items (web.page.get, net.tcp.port, etc.).</source>
      </trans-unit>
      <trans-unit id="8b9a658a" xml:space="preserve">
        <source>##### StatusPort

If set, the agent will listen on this port for HTTP status requests (http://localhost:&lt;port&gt;/status).

Range: 1024-32767</source>
      </trans-unit>
      <trans-unit id="baa42b03" xml:space="preserve">
        <source>##### Timeout

Spend no more than Timeout seconds on processing.

Default: `3`&lt;br&gt;
Range: 1-30</source>
      </trans-unit>
      <trans-unit id="5fffe23e" xml:space="preserve">
        <source>##### TLSAccept

The incoming connections to accept. Used for passive checks. Multiple values can be specified, separated by comma:&lt;br&gt;*unencrypted* - accept connections without encryption (default)&lt;br&gt;*psk* - accept connections with TLS and a pre-shared key (PSK)&lt;br&gt;*cert* - accept connections with TLS and a certificate

Mandatory: yes, if TLS certificate or PSK parameters are defined (even for *unencrypted* connection); otherwise no</source>
      </trans-unit>
      <trans-unit id="01a58fb9" xml:space="preserve">
        <source>##### TLSCAFile

The full pathname of the file containing the top-level CA(s) certificates for peer certificate verification, used for encrypted communications between Zabbix components.</source>
      </trans-unit>
      <trans-unit id="64ff2b38" xml:space="preserve">
        <source>##### TLSCertFile

The full pathname of the file containing the agent certificate or certificate chain, used for encrypted communications with Zabbix components.</source>
      </trans-unit>
      <trans-unit id="0c14341a" xml:space="preserve">
        <source>##### TLSConnect

How the agent should connect to Zabbix server or proxy. Used for active checks. Only one value can be specified:&lt;br&gt;*unencrypted* - connect without encryption (default)&lt;br&gt;*psk* - connect using TLS and a pre-shared key (PSK)&lt;br&gt;*cert* - connect using TLS and a certificate

Mandatory: yes, if TLS certificate or PSK parameters are defined (even for *unencrypted* connection); otherwise no</source>
      </trans-unit>
      <trans-unit id="353fa6b2" xml:space="preserve">
        <source>##### TLSCRLFile

The full pathname of the file containing revoked certificates. This parameter is used for encrypted communications between Zabbix components.</source>
      </trans-unit>
      <trans-unit id="40db7da5" xml:space="preserve">
        <source>##### TLSKeyFile

The full pathname of the file containing the agent private key, used for encrypted communications between Zabbix components.</source>
      </trans-unit>
      <trans-unit id="216346d5" xml:space="preserve">
        <source>##### TLSPSKFile

The full pathname of the file containing the agent pre-shared key, used for encrypted communications with Zabbix server.</source>
      </trans-unit>
      <trans-unit id="d9d037af" xml:space="preserve">
        <source>##### TLSPSKIdentity

The pre-shared key identity string, used for encrypted communications with Zabbix server.</source>
      </trans-unit>
      <trans-unit id="4a08c667" xml:space="preserve">
        <source>##### TLSServerCertIssuer

The allowed server (proxy) certificate issuer.</source>
      </trans-unit>
      <trans-unit id="6446ac76" xml:space="preserve">
        <source>##### TLSServerCertSubject

The allowed server (proxy) certificate subject.</source>
      </trans-unit>
      <trans-unit id="42a95de3" xml:space="preserve">
        <source>##### UnsafeUserParameters

Allow all characters to be passed in arguments to user-defined parameters. The following characters are not allowed: \\ ' " \` \* ? \[ \] { } \~ $ ! &amp; ; ( ) &lt; &gt; \| \# @&lt;br&gt;Additionally, newline characters are not allowed.

Default: `0`&lt;br&gt;
Values: 0 - do not allow, 1 - allow</source>
      </trans-unit>
      <trans-unit id="cba7618d" xml:space="preserve">
        <source>##### UserParameter

A user-defined parameter to monitor. There can be several user-defined parameters.&lt;br&gt;Format: UserParameter=&lt;key&gt;,&lt;shell command&gt;&lt;br&gt;Note that the shell command must not return empty string or EOL only. Shell commands may have relative paths, if the UserParameterDir parameter is specified.

Example:

    UserParameter=system.test,who|wc -l
    UserParameter=check_cpu,./custom_script.sh</source>
      </trans-unit>
      <trans-unit id="f6be7620" xml:space="preserve">
        <source>##### UserParameterDir

The default search path for UserParameter commands. If used, the agent will change its working directory to the one specified here before executing a command. Thereby, UserParameter commands can have a relative `./` prefix instead of a full path.&lt;br&gt;Only one entry is allowed.

Example:

    UserParameterDir=/opt/myscripts</source>
      </trans-unit>
    </body>
  </file>
</xliff>
