<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="es" datatype="plaintext" original="manual/appendix/compatibility.md">
    <body>
      <trans-unit id="d25f4c9e" xml:space="preserve">
        <source># 11 Version compatibility</source>
      </trans-unit>
      <trans-unit id="eefa9a73" xml:space="preserve">
        <source>#### Supported agents

To be compatible with Zabbix 7.0, Zabbix agent must not be older than
version 1.4 and must not be newer than 7.0.

You may need to review the configuration of older agents as some
parameters have changed, for example, parameters related to
[logging](https://www.zabbix.com/documentation/3.0/manual/installation/upgrade_notes_300#changes_in_configuration_parameters_related_to_logging)
for versions before 3.0.

To take full advantage of the latest functionality, metrics, improved performance and
reduced memory usage, use the latest supported agent.</source>
      </trans-unit>
      <trans-unit id="1a5df00a" xml:space="preserve">
        <source>#### Supported agents 2

Older Zabbix agents 2 from version 4.4 onwards are compatible with
Zabbix 7.0; Zabbix agent 2 must not be newer than 7.0.

Note that when using Zabbix agent 2 versions 4.4 and 5.0, the default
interval of 10 minutes is used for refreshing unsupported items.

To take full advantage of the latest functionality, metrics, improved performance and
reduced memory usage, use the latest supported agent 2.</source>
      </trans-unit>
      <trans-unit id="5be689b0" xml:space="preserve">
        <source>#### Supported Zabbix proxies

To be fully compatible with Zabbix 7.0, the proxies must be of the same major version; thus only Zabbix 7.0.x proxies
are fully compatible with Zabbix 7.0.x server. However, outdated proxies are also supported, although only partially.</source>
      </trans-unit>
      <trans-unit id="78c6278a" xml:space="preserve">
        <source>
In relation to Zabbix server, proxies can be:

-   *Current* (proxy and server have the same major version);
-   *Outdated* (proxy version is older than server version, but is partially supported);
-   *Unsupported* (proxy version is older than server previous LTS release version *or* proxy version is newer than server major version).

Examples:

|Server version|*Current* proxy version|*Outdated* proxy version|*Unsupported* proxy version|
|--------------|-----------------------|------------------------|---------------------------|
|6.4|6.4|6.0, 6.2|Older than 6.0; newer than 6.4|
|7.0|7.0|6.0, 6.2, 6.4|Older than 6.0; newer than 7.0|
|7.2|7.2|7.0|Older than 7.0; newer than 7.2|</source>
      </trans-unit>
      <trans-unit id="9f9fce98" xml:space="preserve">
        <source>
Functionality supported by proxies:

|Proxy version|Data update|Configuration update|Tasks|
|--|--|--|----|
|*Current*|Yes|Yes|Yes|
|*Outdated*|Yes|No|[Remote commands](/manual/config/notifications/action/operation/remote_command) (e.g., shell scripts);&lt;br&gt;Immediate item value checks (i.e., *[Execute now](/manual/config/items/check_now)*);&lt;br&gt;Note: Preprocessing [tests with a real value](/manual/config/items/preprocessing#testing-real-value) are not supported.|
|*Unsupported*|No|No|No|</source>
      </trans-unit>
      <trans-unit id="f9a2762b" xml:space="preserve">
        <source>
Warnings about using incompatible Zabbix daemon versions are logged.</source>
      </trans-unit>
      <trans-unit id="5f9e54ca" xml:space="preserve">
        <source>#### Supported XML files

XML files not older than version 1.8 are supported for import in Zabbix
7.0.

::: noteimportant
In the XML export format, trigger dependencies are
stored by name only. If there are several triggers with the same name
(for example, having different severities and expressions) that have a
dependency defined between them, it is not possible to import them. Such
dependencies must be manually removed from the XML file and re-added
after import.
:::</source>
      </trans-unit>
    </body>
  </file>
</xliff>
