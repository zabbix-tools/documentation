[comment]: # translation:outdated

[comment]: # ({new-f65323a6})
# 2 PostgreSQL encryption configuration

[comment]: # ({/new-f65323a6})

[comment]: # ({new-63be0cfa})
### Overview

This section provides several encryption configuration examples for
CentOS 8.2 and PostgreSQL 13.

::: noteclassic
 Connection between Zabbix frontend and PostgreSQL cannot be
encrypted (parameters in GUI are disabled), if the value of *Database
host* field begins with a slash or the field is empty. 
:::

[comment]: # ({/new-63be0cfa})

[comment]: # ({new-47df7706})
### Pre-requisites

Install the PostgreSQL database using the [official
repository](https://www.postgresql.org/download/linux/redhat/).

PostgreSQL is not configured to accept TLS connections out-of-the-box.
Please follow instructions from PostgreSQL documentation for
[certificate preparation with
postgresql.conf](https://www.postgresql.org/docs/13/ssl-tcp.html) and
also for [user access
control](https://www.postgresql.org/docs/13/auth-pg-hba-conf.html)
through ph\_hba.conf.

By default, the PostgreSQL socket is binded to the localhost, for the
network remote connections allow to listen on the real network
interface.

PostgreSQL settings for all
[modes](/manual/appendix/install/db_encrypt#terminology) can look like
this:

**/var/lib/pgsql/13/data/postgresql.conf:**

    ...
    ssl = on
    ssl_ca_file = 'root.crt'
    ssl_cert_file = 'server.crt'
    ssl_key_file = 'server.key'
    ssl_ciphers = 'HIGH:MEDIUM:+3DES:!aNULL'
    ssl_prefer_server_ciphers = on
    ssl_min_protocol_version = 'TLSv1.3'
    ...

For access control adjust */var/lib/pgsql/13/data/pg\_hba.conf*:

    ...
    ### require
    hostssl all all 0.0.0.0/0 md5

    ### verify CA
    hostssl all all 0.0.0.0/0 md5 clientcert=verify-ca

    ### verify full
    hostssl all all 0.0.0.0/0 md5 clientcert=verify-full
    ...

[comment]: # ({/new-47df7706})

[comment]: # ({new-fda36821})
### Required mode

[comment]: # ({/new-fda36821})

[comment]: # ({new-3c0e876a})
#### Frontend

To enable transport-only encryption for connections between Zabbix
frontend and the database:

-   Check *Database TLS encryption*
-   Leave *Verify database certificate* unchecked

![](../../../../../assets/en/manual/appendix/install/encrypt_db_transport2.png){width="600"}

[comment]: # ({/new-3c0e876a})

[comment]: # ({new-99c0e65b})
#### Server

To enable transport-only encryption for connections between server and
the database, configure */etc/zabbix/zabbix\_server.conf*:

    ...
    DBHost=10.211.55.9
    DBName=zabbix
    DBUser=zbx_srv
    DBPassword=<strong_password>
    DBTLSConnect=required
    ...

[comment]: # ({/new-99c0e65b})

[comment]: # ({new-ef3e06b2})
### Verify CA mode

[comment]: # ({/new-ef3e06b2})

[comment]: # ({new-e8c8dbd8})
#### Frontend

To enable encryption with certificate authority verification for
connections between Zabbix frontend and the database:

-   Check *Database TLS encryption* and *Verify database certificate*
-   Specify path to *Database TLS key file*
-   Specify path to *Database TLS CA file*
-   Specify path to *Database TLS certificate file*

![](../../../../../assets/en/manual/appendix/install/encrypt_db_verify_ca2.png){width="600"}

Alternatively, this can be set in */etc/zabbix/web/zabbix.conf.php:*

    ...
    $DB['ENCRYPTION'] = true;
    $DB['KEY_FILE'] = '';
    $DB['CERT_FILE'] = '';
    $DB['CA_FILE'] = '/etc/ssl/pgsql/root.crt';
    $DB['VERIFY_HOST'] = false;
    $DB['CIPHER_LIST'] = '';
    ...

[comment]: # ({/new-e8c8dbd8})

[comment]: # ({new-bbddedec})
#### Server

To enable encryption with certificate verification for connections
between Zabbix server and the database, configure
*/etc/zabbix/zabbix\_server.conf:*

    ...
    DBHost=10.211.55.9
    DBName=zabbix
    DBUser=zbx_srv
    DBPassword=<strong_password>
    DBTLSConnect=verify_ca
    DBTLSCAFile=/etc/ssl/pgsql/root.crt
    ...

[comment]: # ({/new-bbddedec})

[comment]: # ({new-ef07b8a6})
### Verify full mode

[comment]: # ({/new-ef07b8a6})

[comment]: # ({new-83615cf7})
#### Frontend

To enable encryption with certificate and database host identity
verification for connections between Zabbix frontend and the database:

-   Check *Database TLS encryption* and *Verify database certificate*
-   Specify path to *Database TLS key file*
-   Specify path to *Database TLS CA file*
-   Specify path to *Database TLS certificate file*
-   Check *Database host verification*

![](../../../../../assets/en/manual/appendix/install/encrypt_db_verify_full2.png){width="600"}

Alternatively, this can be set in */etc/zabbix/web/zabbix.conf.php:*

    $DB['ENCRYPTION'] = true;
    $DB['KEY_FILE'] = '';
    $DB['CERT_FILE'] = '';
    $DB['CA_FILE'] = '/etc/ssl/pgsql/root.crt';
    $DB['VERIFY_HOST'] = true;
    $DB['CIPHER_LIST'] = '';
    ...

[comment]: # ({/new-83615cf7})

[comment]: # ({new-61b8c8f3})
#### Server

To enable encryption with certificate and database host identity
verification for connections between Zabbix server and the database,
configure */etc/zabbix/zabbix\_server.conf*:

    ...
    DBHost=10.211.55.9
    DBName=zabbix
    DBUser=zbx_srv
    DBPassword=<strong_password>
    DBTLSConnect=verify_full
    DBTLSCAFile=/etc/ssl/pgsql/root.crt
    DBTLSCertFile=/etc/ssl/pgsql/client.crt
    DBTLSKeyFile=/etc/ssl/pgsql/client.key
    ...

[comment]: # ({/new-61b8c8f3})
