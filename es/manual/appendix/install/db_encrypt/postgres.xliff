<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="es" datatype="plaintext" original="manual/appendix/install/db_encrypt/postgres.md">
    <body>
      <trans-unit id="f65323a6" xml:space="preserve">
        <source># 2 PostgreSQL encryption configuration</source>
      </trans-unit>
      <trans-unit id="63be0cfa" xml:space="preserve">
        <source>### Overview

This section provides several encryption configuration examples for
CentOS 8.2 and PostgreSQL 13.

::: noteclassic
 Connection between Zabbix frontend and PostgreSQL cannot be
encrypted (parameters in GUI are disabled), if the value of *Database
host* field begins with a slash or the field is empty. 
:::</source>
      </trans-unit>
      <trans-unit id="47df7706" xml:space="preserve">
        <source>### Pre-requisites

Install the PostgreSQL database using the [official
repository](https://www.postgresql.org/download/linux/redhat/).

PostgreSQL is not configured to accept TLS connections out-of-the-box.
Please follow instructions from PostgreSQL documentation for
[certificate preparation with
postgresql.conf](https://www.postgresql.org/docs/13/ssl-tcp.html) and
also for [user access
control](https://www.postgresql.org/docs/13/auth-pg-hba-conf.html)
through ph\_hba.conf.

By default, the PostgreSQL socket is binded to the localhost, for the
network remote connections allow to listen on the real network
interface.

PostgreSQL settings for all
[modes](/manual/appendix/install/db_encrypt#terminology) can look like
this:

**/var/lib/pgsql/13/data/postgresql.conf:**

    ...
    ssl = on
    ssl_ca_file = 'root.crt'
    ssl_cert_file = 'server.crt'
    ssl_key_file = 'server.key'
    ssl_ciphers = 'HIGH:MEDIUM:+3DES:!aNULL'
    ssl_prefer_server_ciphers = on
    ssl_min_protocol_version = 'TLSv1.3'
    ...

For access control adjust */var/lib/pgsql/13/data/pg\_hba.conf*:

    ...
    ### require
    hostssl all all 0.0.0.0/0 md5

    ### verify CA
    hostssl all all 0.0.0.0/0 md5 clientcert=verify-ca

    ### verify full
    hostssl all all 0.0.0.0/0 md5 clientcert=verify-full
    ...</source>
      </trans-unit>
      <trans-unit id="fda36821" xml:space="preserve">
        <source>### Required mode</source>
      </trans-unit>
      <trans-unit id="3c0e876a" xml:space="preserve">
        <source>#### Frontend

To enable transport-only encryption for connections between Zabbix
frontend and the database:

-   Check *Database TLS encryption*
-   Leave *Verify database certificate* unchecked

![](../../../../../assets/en/manual/appendix/install/encrypt_db_transport2.png){width="600"}</source>
      </trans-unit>
      <trans-unit id="99c0e65b" xml:space="preserve">
        <source>#### Server

To enable transport-only encryption for connections between server and
the database, configure */etc/zabbix/zabbix\_server.conf*:

    ...
    DBHost=10.211.55.9
    DBName=zabbix
    DBUser=zbx_srv
    DBPassword=&lt;strong_password&gt;
    DBTLSConnect=required
    ...</source>
      </trans-unit>
      <trans-unit id="ef3e06b2" xml:space="preserve">
        <source>### Verify CA mode</source>
      </trans-unit>
      <trans-unit id="e8c8dbd8" xml:space="preserve">
        <source>#### Frontend

To enable encryption with certificate authority verification for
connections between Zabbix frontend and the database:

-   Check *Database TLS encryption* and *Verify database certificate*
-   Specify path to *Database TLS key file*
-   Specify path to *Database TLS CA file*
-   Specify path to *Database TLS certificate file*

![](../../../../../assets/en/manual/appendix/install/encrypt_db_verify_ca2.png){width="600"}

Alternatively, this can be set in */etc/zabbix/web/zabbix.conf.php:*

    ...
    $DB['ENCRYPTION'] = true;
    $DB['KEY_FILE'] = '';
    $DB['CERT_FILE'] = '';
    $DB['CA_FILE'] = '/etc/ssl/pgsql/root.crt';
    $DB['VERIFY_HOST'] = false;
    $DB['CIPHER_LIST'] = '';
    ...</source>
      </trans-unit>
      <trans-unit id="bbddedec" xml:space="preserve">
        <source>#### Server

To enable encryption with certificate verification for connections
between Zabbix server and the database, configure
*/etc/zabbix/zabbix\_server.conf:*

    ...
    DBHost=10.211.55.9
    DBName=zabbix
    DBUser=zbx_srv
    DBPassword=&lt;strong_password&gt;
    DBTLSConnect=verify_ca
    DBTLSCAFile=/etc/ssl/pgsql/root.crt
    ...</source>
      </trans-unit>
      <trans-unit id="ef07b8a6" xml:space="preserve">
        <source>### Verify full mode</source>
      </trans-unit>
      <trans-unit id="83615cf7" xml:space="preserve">
        <source>#### Frontend

To enable encryption with certificate and database host identity
verification for connections between Zabbix frontend and the database:

-   Check *Database TLS encryption* and *Verify database certificate*
-   Specify path to *Database TLS key file*
-   Specify path to *Database TLS CA file*
-   Specify path to *Database TLS certificate file*
-   Check *Database host verification*

![](../../../../../assets/en/manual/appendix/install/encrypt_db_verify_full2.png){width="600"}

Alternatively, this can be set in */etc/zabbix/web/zabbix.conf.php:*

    $DB['ENCRYPTION'] = true;
    $DB['KEY_FILE'] = '';
    $DB['CERT_FILE'] = '';
    $DB['CA_FILE'] = '/etc/ssl/pgsql/root.crt';
    $DB['VERIFY_HOST'] = true;
    $DB['CIPHER_LIST'] = '';
    ...</source>
      </trans-unit>
      <trans-unit id="61b8c8f3" xml:space="preserve">
        <source>#### Server

To enable encryption with certificate and database host identity
verification for connections between Zabbix server and the database,
configure */etc/zabbix/zabbix\_server.conf*:

    ...
    DBHost=10.211.55.9
    DBName=zabbix
    DBUser=zbx_srv
    DBPassword=&lt;strong_password&gt;
    DBTLSConnect=verify_full
    DBTLSCAFile=/etc/ssl/pgsql/root.crt
    DBTLSCertFile=/etc/ssl/pgsql/client.crt
    DBTLSKeyFile=/etc/ssl/pgsql/client.key
    ...</source>
      </trans-unit>
    </body>
  </file>
</xliff>
