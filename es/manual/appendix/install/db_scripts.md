[comment]: # translation:outdated

[comment]: # ({1f045d3f-7af1bbaa})
# 1 Creación de base de datos

[comment]: # ({/1f045d3f-7af1bbaa})

[comment]: # ({d1c1d1c5-fab90562})
#### Descripción general

Se debe crear una base de datos Zabbix durante la instalación del servidor o proxy de Zabbix.

Esta sección proporciona instrucciones para crear una base de datos Zabbix. Un conjunto de instrucciones independiente está disponible para cada base de datos soportada.

UTF-8 es la única codificación compatible con Zabbix. Se sabe que funciona
sin fallas de seguridad. Los usuarios deben ser conscientes de que existen
problemas de seguridad si se utilizan algunas de las otras codificaciones.

[comment]: # ({/d1c1d1c5-fab90562})

[comment]: # ({ebc6690d-fad527fc})

::: notaclásica
Si instala desde [Zabbix Git
repositorio](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse), debe ejecutar:

`$ make dbschema`

antes de continuar con los siguientes pasos.
:::

[comment]: # ({/ebc6690d-fad527fc})

[comment]: # ({1f24ea2a-1e36e539})
#### MySQL

Se admiten conjuntos de caracteres utf8 (también conocido como utf8mb3) y utf8mb4 (con
utf8\_bin y utf8mb4\_bin colación respectivamente) para Zabbix
servidor/proxy para que funcione correctamente con la base de datos MySQL. Se recomienda a
use utf8mb4 para nuevas instalaciones.

shell> mysql -uroot -p<password>
mysql> create database zabbix character set utf8mb4 collate utf8mb4_bin;
mysql> create user 'zabbix'@'localhost' identified by '<password>';
mysql> grant all privileges on zabbix.* to 'zabbix'@'localhost';
mysql> quit;

::: nota de advertencia
Si está instalando desde **paquetes** de Zabbix, deténgase
aquí y continúe con las instrucciones para
[RHEL/CentOS](https://www.zabbix.com/download?zabbix=5.0&os_distribution=red_hat_enterprise_linux&os_version=8&db=mysql)
o
[Debian/Ubuntu](https://www.zabbix.com/download?zabbix=5.0&os_distribution=debian&os_version=10_buster&db=mysql)
para importar los datos a la base de datos.
:::

Si está instalando Zabbix desde fuentes, proceda a importar los datos
en la base de datos. Para una base de datos proxy Zabbix, solo `schema.sql` debería
ser importado (no images.sql ni data.sql):

shell> cd database/mysql
shell> mysql -uzabbix -p<password> zabbix < schema.sql
# deténgase aquí si está creando una base de datos para el proxy Zabbix
shell> mysql -uzabbix -p<password> zabbix < images.sql
shell> mysql -uzabbix -p<password> zabbix < data.sql

[comment]: # ({/1f24ea2a-1e36e539})

[comment]: # ({1c0bbf24-61d6043c})
#### PostgreSQL

Necesita tener un usuario de base de datos con permisos para crear una base de datos
objetos. El siguiente comando de shell creará el usuario `zabbix`. Especificar
contraseña cuando se le solicite y repita la contraseña (tenga en cuenta que es posible que primero se le solicite
para `sudo` contraseña):

    shell> sudo -u postgres createuser --pwprompt zabbix

Ahora configuraremos la base de datos `zabbix` (último parámetro) con el
usuario creado previamente como propietario (`-O zabbix`).

    shell> sudo -u postgres createdb -O zabbix -E Unicode -T template0 zabbix

::: nota de advertencia
Si está instalando desde **paquetes** de Zabbix, deténgase
aquí y continúe con las instrucciones para
[RHEL/CentOS](https://www.zabbix.com/download?zabbix=5.0&os_distribution=red_hat_enterprise_linux&os_version=8&db=postgresql)
o
[Debian/Ubuntu](https://www.zabbix.com/download?zabbix=5.0&os_distribution=debian&os_version=10_buster&db=postgresql)
para importar el esquema inicial y los datos a la base de datos.
:::

Si está instalando Zabbix desde fuentes, proceda a importar el archivo inicial
esquema y datos (suponiendo que esté en el directorio raíz de Zabbix
fuentes). Para una base de datos proxy de Zabbix, solo debe incluirse `schema.sql`.
importado (no images.sql ni data.sql).

shell> cd database/postgresql
shell> cat schema.sql | sudo -u zabbix psql zabbix
# deténgase aquí si está creando una base de datos para el proxy Zabbix
shell> cat images.sql | sudo -u zabbix psql zabbix
shell> cat data.sql | sudo -u zabbix psql zabbix
::: notaimportante
Los comandos anteriores se proporcionan como un ejemplo que
funcionará en la mayoría de las instalaciones de GNU/Linux. Puedes usar diferentes
comandos, Es decir "psql -U <nombre de usuario>" dependiendo de cómo el
sistema/base de datos están configurados. Si tiene problemas para configurar la
base de datos, consulte a su administrador de base de datos.
:::

[comment]: # ({/1c0bbf24-61d6043c})

[comment]: # ({82ce0421-cc68ca58})
#### TimescaleDB

Las instrucciones para crear y configurar TimescaleDB se proporcionan en un
[sección] separada (/manual/appendix/install/timescaledb).

[comment]: # ({/82ce0421-cc68ca58})

[comment]: # ({82014cf5-7b4d56a7})
#### Oracle

Se proporcionan instrucciones para crear y configurar la base de datos de Oracle
en una [sección](/manual/appendix/install/oracle) separada .

[comment]: # ({/82014cf5-7b4d56a7})

[comment]: # ({e6395f36-02d49e4f})
#### SQLite

¡El uso de SQLite solo es compatible con **Zabbix proxy**!

::: notaclásica
Si usa SQLite con el proxy Zabbix, la base de datos será
se crea automáticamente si no existe.
:::

shell> cd database/sqlite3
shell> sqlite3 /var/lib/sqlite/zabbix.db < schema.sql

Regrese a la [sección de instalación](/manual/installation/install).

[comment]: # ({/e6395f36-02d49e4f})
