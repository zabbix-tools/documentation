[comment]: # translation:outdated

[comment]: # ({new-98c082ce})
# 4 TimescaleDB setup

[comment]: # ({/new-98c082ce})

[comment]: # ({new-acb21280})
#### Overview

Zabbix supports TimescaleDB, a PostgreSQL-based database solution of
automatically partitioning data into time-based chunks to support faster
performance at scale.

::: notewarning
Currently TimescaleDB is not supported by Zabbix
proxy.
:::

Instructions on this page can be used for creating TimescaleDB database
or migrating from existing PostgreSQL tables to TimescaleDB.

[comment]: # ({/new-acb21280})

[comment]: # ({new-f731725c})
#### Configuration

We assume that TimescaleDB extension has been already installed on the
database server (see [installation
instructions](https://docs.timescale.com/latest/getting-started/installation)).

TimescaleDB extension must also be enabled for the specific DB by
executing:

    echo "CREATE EXTENSION IF NOT EXISTS timescaledb CASCADE;" | sudo -u postgres psql zabbix

Running this command requires database administrator privileges.

::: noteclassic
If you use a database schema other than 'public' you need to
add a SCHEMA clause to the command above. E.g.:\
`echo "CREATE EXTENSION IF NOT EXISTS timescaledb SCHEMA yourschema CASCADE;" | sudo -u postgres psql zabbix`
:::

Then run the `timescaledb.sql` script located in database/postgresql.
For new installations the script must be run after the regular
PostgreSQL database has been created with initial schema/data (see
[database creation](/manual/appendix/install/db_scripts)):

    zcat /usr/share/doc/zabbix-sql-scripts/postgresql/timescaledb.sql.gz | sudo -u zabbix psql zabbix

The migration of existing history and trend data may take a lot of time.
Zabbix server and frontend must be down for the period of migration.

The `timescaledb.sql` script sets the following housekeeping parameters:

-   Override item history period
-   Override item trend period

In order to use partitioned housekeeping for history and trends, both
these options must be on. It's possible to use TimescaleDB partitioning
only for trends (by setting *Override item trend period*) or only for
history (*Override item history period*).

For PostgreSQL version 10.2 or higher and TimescaleDB version 1.5 or
higher, the `timescaledb.sql` script sets two additional parameters:

-   Enable compression
-   Compress records older than 7 days

All of these parameters can be changed in *Administration* → *General* →
*Housekeeping* after the installation.

::: notetip
You may want to run the timescaledb-tune tool provided
by TimescaleDB to optimize PostgreSQL configuration parameters in your
`postgresql.conf`.
:::

[comment]: # ({/new-f731725c})

[comment]: # ({new-60e46db2})

Compression can be used only if both *Override item history period*
and *Override item trend period* options are enabled.

[comment]: # ({/new-60e46db2})

[comment]: # ({new-8c3a80a9})

All of these parameters can be changed in *Administration* →
*Housekeeping* after the installation.

::: notetip
You may want to run the timescaledb-tune tool provided
by TimescaleDB to optimize PostgreSQL configuration parameters in your
`postgresql.conf`.
:::

[comment]: # ({/new-8c3a80a9})

[comment]: # ({new-2012f0a6})
#### TimescaleDB compression

Native TimescaleDB compression is supported starting from Zabbix 5.0 for
PostgreSQL version 10.2 or higher and TimescaleDB version 1.5 or higher
for all Zabbix tables that are managed by TimescaleDB. During the
upgrade or migration to TimescaleDB, initial compression of the large
tables may take a lot of time.

::: notetip
Users are encouraged to get familiar with
[TimescaleDB](https://docs.timescale.com/latest/using-timescaledb/compression)
compression documentation before using compression.
:::

Note, that there are certain limitations imposed by compression,
specifically:

-   Compressed chunk modifications (inserts, deletes, updates) are not
    allowed
-   Schema changes for compressed tables are not allowed.

Compression settings can be changed in the *History and trends
compression* block in *Administration* → *General* → *Housekeeping*
section of Zabbix frontend.

|Parameter|Default|Comments|
|---------|-------|--------|
|*Enable compression*|Enabled|Checking or unchecking the checkbox does not activate/deactivate compression immediately. Because compression is handled by the Housekeeper, the changes will take effect in up to 2 times `HousekeepingFrequency` hours (set in [zabbix\_server.conf](/manual/appendix/config/zabbix_server))<br><br>After disabling compression, new chunks that fall into the compression period will not be compressed. However, all previously compressed data will stay compressed. To uncompress previously compressed chunks, follow instructions in [TimescaleDB](https://docs.timescale.com/latest/using-timescaledb/compression) documentation.<br><br>When upgrading from older versions of Zabbix with TimescaleDB support, compression will not be enabled by default.|
|*Compress records older than*|7d|This parameter cannot be less than 7 days.<br><br>Due to immutability of compressed chunks all late data (e.g. data delayed by a proxy) that is older than this value will be discarded.|

[comment]: # ({/new-2012f0a6})
