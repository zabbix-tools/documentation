[comment]: # attributes: notoc

[comment]: # translation:outdated

[comment]: # ({new-7da5ea1c})
# 4 History functions

All functions listed here are supported in:

-   [Trigger expressions](/manual/config/triggers/expression)
-   [Calculated items](/manual/config/items/itemtypes/calculated)

Some general notes on function parameters:

-   Function parameters are separated by a comma
-   Optional function parameters (or parameter parts) are indicated by
    `<` `>`
-   Function-specific parameters are described with each function
-   `/host/key` and `(sec|#num)<:time shift>` parameters must never be
    quoted

[comment]: # ({/new-7da5ea1c})

[comment]: # ({new-4d343eb8})
##### Common parameters

-   `/host/key` is a common mandatory first parameter for the functions
    referencing the host item history
-   `(sec|#num)<:time shift>` is a common second parameter for the
    functions referencing the host item history, where:
    -   **sec** - maximum [evaluation
        period](/manual/config/triggers#evaluation_period) in seconds
        (time [suffixes](/manual/appendix/suffixes) can be used), or
    -   **\#num** - maximum [evaluation
        range](/manual/config/triggers#evaluation_period) in latest
        collected values (if preceded by a hash mark)
    -   **time shift** (optional) allows to move the evaluation point
        back in time. See [more
        details](/manual/config/triggers/expression#time_shift) on
        specifying time shift.

[comment]: # ({/new-4d343eb8})

[comment]: # ({new-8f3a0028})
### Function details

Some general notes on function parameters:

-   Function parameters are separated by a comma
-   Optional function parameters (or parameter parts) are indicated by
    `<` `>`
-   Function-specific parameters are described with each function
-   `/host/key` and `(sec|#num)<:time shift>` parameters must never be
    quoted

[comment]: # ({/new-8f3a0028})

[comment]: # ({new-4afc9a02})

##### change(/host/key) {#change}

The amount of difference between the previous and latest value.<br>
Supported value types: *Float*, *Integer*, *String*, *Text*, *Log*.<br>
For strings returns: 0 - values are equal; 1 - values differ.

Parameters: see [common parameters](#common-parameters).

Comments:

-   Numeric difference will be calculated, as seen with these incoming example values ('previous' and 'latest' value = difference):<br>'1' and '5' = `+4`<br>'3' and '1' = `-2`<br>'0' and '-2.5' = `-2.5`<br>
-   See also: [abs](/manual/appendix/functions/math) for comparison.

Examples:

    change(/host/key)>10

[comment]: # ({/new-4afc9a02})

[comment]: # ({new-57458a90})

##### changecount(/host/key,(sec|#num)<:time shift>,<mode>) {#changecount}

The number of changes between adjacent values within the defined evaluation period.<br>
Supported value types: *Float*, *Integer*, *String*, *Text*, *Log*.

Parameters: 

-   See [common parameters](#common-parameters);<br>
-   **mode** (must be double-quoted) - possible values: *all* - count all changes (default); *dec* - count decreases; *inc* - count increases

For non-numeric value types, the *mode* parameter is ignored.

Examples:

    changecount(/host/key,1w) #the number of value changes for the last week until now
    changecount(/host/key,#10,"inc") #the number of value increases (relative to the adjacent value) among the last 10 values
    changecount(/host/key,24h,"dec") #the number of value decreases (relative to the adjacent value) for the last 24 hours until now

[comment]: # ({/new-57458a90})

[comment]: # ({new-f9ce5dd9})

##### count(/host/key,(sec|#num)<:time shift>,<operator>,<pattern>) {#count}

The number of values within the defined evaluation period.<br>
Supported value types: *Float*, *Integer*, *String*, *Text*, *Log*.

Parameters: 

-   See [common parameters](#common-parameters);<br>
-   **operator** (must be double-quoted). Supported `operators`:<br>*eq* - equal (default)<br>*ne* - not equal<br>*gt* - greater<br>*ge* - greater or equal<br>*lt* - less<br>*le* - less or equal<br>*like* - matches if contains pattern (case-sensitive)<br>*bitand* - bitwise AND<br>*regexp* - case-sensitive match of the regular expression given in `pattern`<br>*iregexp* - case-insensitive match of the regular expression given in `pattern`<br>
-   **pattern** - the required pattern (string arguments must be double-quoted).

Comments:

-   Float items match with the precision of 2.22e-16; if database is [not upgraded](/manual/appendix/install/db_float_range) the precision is 0.000001.
-   With *bitand* as the third parameter, the fourth `pattern` parameter can be specified as two numbers, separated by '/': **number_to_compare_with/mask**. count() calculates "bitwise AND" from the value and the *mask* and compares the result to *number_to_compare_with*. If the result of "bitwise AND" is equal to *number_to_compare_with*, the value is counted.<br>If *number_to_compare_with* and *mask* are equal, only the *mask* need be specified (without '/').
-   With *regexp* or *iregexp* as the third parameter, the fourth `pattern` parameter can be an ordinary or [global](/manual/regular_expressions#global_regular_expressions) (starting with '@') regular expression. In case of global regular expressions case sensitivity is inherited from global regular expression settings. For the purpose of regexp matching, float values will always be represented with 4 decimal digits after '.'. Also note that for large numbers difference in decimal (stored in database) and binary (used by Zabbix server) representation may affect the 4th decimal digit.

Examples:

    count(/host/key,10m) #the values for the last 10 minutes until now
    count(/host/key,10m,"like","error") #the number of values for the last 10 minutes until now that contain 'error'
    count(/host/key,10m,,12) #the number of values for the last 10 minutes until now that equal '12'
    count(/host/key,10m,"gt",12) #the number of values for the last 10 minutes until now that are over '12'
    count(/host/key,#10,"gt",12) #the number of values within the last 10 values until now that are over '12'
    count(/host/key,10m:now-1d,"gt",12) #the number of values between 24 hours and 10 minutes and 24 hours ago from now that were over '12'
    count(/host/key,10m,"bitand","6/7") #the number of values for the last 10 minutes until now having '110' (in binary) in the 3 least significant bits
    count(/host/key,10m:now-1d) #the number of values between 24 hours and 10 minutes and 24 hours ago from now

[comment]: # ({/new-f9ce5dd9})

[comment]: # ({new-bf8dc20c})

##### countunique(/host/key,(sec|#num)<:time shift>,<operator>,<pattern>) {#countunique}

The number of unique values within the defined evaluation period.<br>
Supported value types: *Float*, *Integer*, *String*, *Text*, *Log*.

Parameters: 

-   See [common parameters](#common-parameters);<br>
-   **operator** (must be double-quoted). Supported `operators`:<br>*eq* - equal (default)<br>*ne* - not equal<br>*gt* - greater<br>*ge* - greater or equal<br>*lt* - less<br>*le* - less or equal<br>*like* - matches if contains pattern (case-sensitive)<br>*bitand* - bitwise AND<br>*regexp* - case-sensitive match of the regular expression given in `pattern`<br>*iregexp* - case-insensitive match of the regular expression given in `pattern`<br>
-   **pattern** - the required pattern (string arguments must be double-quoted).

Comments:

-   Float items match with the precision of 2.22e-16; if database is [not upgraded](/manual/appendix/install/db_float_range) the precision is 0.000001.
-   With *bitand* as the third parameter, the fourth `pattern` parameter can be specified as two numbers, separated by '/': **number_to_compare_with/mask**. countunique() calculates "bitwise AND" from the value and the *mask* and compares the result to *number_to_compare_with*. If the result of "bitwise AND" is equal to *number_to_compare_with*, the value is counted.<br>If *number_to_compare_with* and *mask* are equal, only the *mask* need be specified (without '/').
-   With *regexp* or *iregexp* as the third parameter, the fourth `pattern` parameter can be an ordinary or [global](/manual/regular_expressions#global_regular_expressions) (starting with '@') regular expression. In case of global regular expressions case sensitivity is inherited from global regular expression settings. For the purpose of regexp matching, float values will always be represented with 4 decimal digits after '.'. Also note that for large numbers difference in decimal (stored in database) and binary (used by Zabbix server) representation may affect the 4th decimal digit.

Examples:

    countunique(/host/key,10m) #the number of unique values for the last 10 minutes until now
    countunique(/host/key,10m,"like","error") #the number of unique values for the last 10 minutes until now that contain 'error'
    countunique(/host/key,10m,,12) #the number of unique values for the last 10 minutes until now that equal '12'
    countunique(/host/key,10m,"gt",12) #the number of unique values for the last 10 minutes until now that are over '12'
    countunique(/host/key,#10,"gt",12) #the number of unique values within the last 10 values until now that are over '12'
    countunique(/host/key,10m:now-1d,"gt",12) #the number of unique values between 24 hours and 10 minutes and 24 hours ago from now that were over '12'
    countunique(/host/key,10m,"bitand","6/7") #the number of unique values for the last 10 minutes until now having '110' (in binary) in the 3 least significant bits
    countunique(/host/key,10m:now-1d) #the number of unique values between 24 hours and 10 minutes and 24 hours ago from now

[comment]: # ({/new-bf8dc20c})

[comment]: # ({new-148d7d16})

##### find(/host/key,(sec|#num)<:time shift>,<operator>,<pattern>) {#count}

Find a value match within the defined evaluation period.<br>
Supported value types: *Float*, *Integer*, *String*, *Text*, *Log*.<br>
Returns: 1 - found; 0 - otherwise.

Parameters: 

-   See [common parameters](#common-parameters);<br>
-   **sec** or **#num** (optional) - defaults to the latest value if not specified
-   **operator** (must be double-quoted). Supported `operators`:<br>*eq* - equal (default)<br>*ne* - not equal<br>*gt* - greater<br>*ge* - greater or equal<br>*lt* - less<br>*le* - less or equal<br>*like* - matches if contains the string given in `pattern` (case-sensitive)<br>*bitand* - bitwise AND<br>*regexp* - case-sensitive match of the regular expression given in `pattern`<br>*iregexp* - case-insensitive match of the regular expression given in `pattern`<br>
-   **pattern** - the required pattern (string arguments must be double-quoted); [Perl Compatible Regular Expression](https://en.wikipedia.org/wiki/Perl_Compatible_Regular_Expressions) (PCRE) regular expression if `operator` is *regexp*, *iregexp*.

Comments:

-   If more than one value is processed, '1' is returned if there is at least one matching value.
-   With *regexp* or *iregexp* as the third parameter, the fourth `pattern` parameter can be an ordinary or [global](/manual/regular_expressions#global_regular_expressions) (starting with '@') regular expression. In case of global regular expressions case sensitivity is inherited from the global regular expression settings.

Example:

    find(/host/key,10m,"like","error") #find a value that contains 'error' within the last 10 minutes until now

[comment]: # ({/new-148d7d16})

[comment]: # ({new-0ae5f587})

##### first(/host/key,sec<:time shift>) {#first}

The first (the oldest) value within the defined evaluation period.<br>
Supported value types: *Float*, *Integer*, *String*, *Text*, *Log*.

Parameters: 

-   See [common parameters](#common-parameters).

See also [last()](#last).

Example:

    first(/host/key,1h) #retrieve the oldest value within the last hour until now

[comment]: # ({/new-0ae5f587})

[comment]: # ({new-1c3609f8})

##### fuzzytime(/host/key,sec) {#fuzzytime}

Check how much the passive agent time differs from the Zabbix server/proxy time.<br>
Supported value types: *Float*, *Integer*.<br>
Returns: 1 - difference between the passive item value (as timestamp) and Zabbix server/proxy timestamp (the clock of value collection) is less than or equal to T seconds; 0 - otherwise.

Parameters: 

-   See [common parameters](#common-parameters).

Comments:

-   Usually used with the 'system.localtime' item to check that local time is in sync with the local time of Zabbix server. *Note* that 'system.localtime' must be configured as a [passive check](/manual/appendix/items/activepassive#passive_checks).
-   Can be used also with the `vfs.file.time[/path/file,modify]` key to check that the file did not get updates for long time;
-   This function is not recommended for use in complex trigger expressions (with multiple items involved), because it may cause unexpected results (time difference will be measured with the most recent metric), e.g. in `fuzzytime(/Host/system.localtime,60s)=0 or last(/Host/trap)<>0`.

Example:

    fuzzytime(/host/key,60s)=0 #detect a problem if the time difference is over 60 seconds<br><br>

[comment]: # ({/new-1c3609f8})

[comment]: # ({new-8059d345})

##### last(/host/key,<#num<:time shift>>) {#last}

The most recent value.<br>
Supported value types: *Float*, *Integer*, *String*, *Text*, *Log*.

Parameters: 

-   See [common parameters](#common-parameters);<br>
-   **#num** (optional) - the Nth most recent value.

Comments:

-   Take note that a hash-tagged time period (#N) works differently here than with many other functions. For example: `last()` is always equal to `last(#1)`; `last(#3)` - the third most recent value (*not* three latest values);
-   Zabbix does not guarantee the exact order of values if more than two values exist within one second in history;
-   See also [first()](#first).

Example:

    last(/host/key) #retrieve the last value
    last(/host/key,#2) #retrieve the previous value
    last(/host/key,#1) <> last(/host/key,#2) #the last and previous values differ

[comment]: # ({/new-8059d345})

[comment]: # ({new-ff5dcd5e})

##### logeventid(/host/key,<#num<:time shift>>,<pattern>) {#logeventid}

Check if the event ID of the last log entry matches a regular expression.<br>
Supported value types: *Log*.<br>
Returns: 0 - does not match; 1 - matches.

Parameters: 

-   See [common parameters](#common-parameters);<br>
-   **#num** (optional) - the Nth most recent value;<br>
-   **pattern** (optional) - the regular expression describing the required pattern, [Perl Compatible Regular Expression](https://en.wikipedia.org/wiki/Perl_Compatible_Regular_Expressions) (PCRE) style (string arguments must be double-quoted).

[comment]: # ({/new-ff5dcd5e})

[comment]: # ({new-5053b990})

##### logseverity(/host/key,<#num<:time shift>>) {#logseverity}

Log severity of the last log entry.<br>
Supported value types: *Log*.<br>
Returns: 0 - default severity; N - severity (integer, useful for Windows event logs: 1 - Information, 2 - Warning, 4 - Error, 7 - Failure Audit, 8 - Success Audit, 9 - Critical, 10 - Verbose).

Parameters: 

-   See [common parameters](#common-parameters);<br>
-   **#num** (optional) - the Nth most recent value.

Zabbix takes log severity from the **Information** field of Windows event log.

[comment]: # ({/new-5053b990})

[comment]: # ({new-a9148f5c})

##### logsource(/host/key,<#num<:time shift>>,<pattern>) {#logsource}

Check if log source of the last log entry matches a regular expression.<br>
Supported value types: *Log*.<br>
Returns: 0 - does not match; 1 - matches.

Parameters: 

-   See [common parameters](#common-parameters);<br>
-   **#num** (optional) - the Nth most recent value;<br>
-   **pattern** (optional) - the regular expression describing the required pattern, [Perl Compatible Regular Expression](https://en.wikipedia.org/wiki/Perl_Compatible_Regular_Expressions) (PCRE) style (string arguments must be double-quoted).

Normally used for Windows event logs.

Example:

    logsource(/host/key,,"VMware Server")

[comment]: # ({/new-a9148f5c})

[comment]: # ({new-6a63b5c8})

##### monodec(/host/key,(sec|#num)<:time shift>,<mode>) {#monodec}

Check if there has been a monotonous decrease in values.<br>
Supported value types: *Integer*.<br>
Returns: 1 - if all elements in the time period continuously decrease; 0 - otherwise.

Parameters: 

-   See [common parameters](#common-parameters);<br>
-   **mode** (must be double-quoted) - *weak* (every value is smaller or the same as the previous one; default) or *strict* (every value has decreased).

Example:

    monodec(/Host1/system.swap.size[all,free],60s) + monodec(/Host2/system.swap.size[all,free],60s) + monodec(/Host3/system.swap.size[all,free],60s) #calculate in how many hosts there has been a decrease in free swap size

[comment]: # ({/new-6a63b5c8})

[comment]: # ({new-755c1821})

##### monoinc(/host/key,(sec|#num)<:time shift>,<mode>) {#monoinc}

Check if there has been a monotonous increase in values.<br>
Supported value types: *Integer*.<br>
Returns: 1 - if all elements in the time period continuously increase; 0 - otherwise.

Parameters: 

-   See [common parameters](#common-parameters);<br>
-   **mode** (must be double-quoted) - *weak* (every value is bigger or the same as the previous one; default) or *strict* (every value has increased).

Example:

    monoinc(/Host1/system.localtime,#3,"strict")=0 #check if the system local time has been increasing consistently

[comment]: # ({/new-755c1821})

[comment]: # ({new-178e3950})

##### nodata(/host/key,sec,<mode>) {#nodata}

Check for no data received.<br>
Supported value types: *Integer*, *Float*, *Character*, *Text*, *Log*.<br>
Returns: 1 - if no data received during the defined period of time; 0 - otherwise.

Parameters: 

-   See [common parameters](#common-parameters);<br>
-   **sec** - the period should not be less than 30 seconds because the history syncer process calculates this function only every 30 seconds; `nodata(/host/key,0)` is disallowed.
-   **mode** - if set to *strict* (double-quoted), this function will be insensitive to proxy availability (see comments for details).

Comments:

-   the 'nodata' triggers monitored by proxy are, by default, sensitive to proxy availability - if proxy becomes unavailable, the 'nodata' triggers will not fire immediately after a restored connection, but will skip the data for the delayed period. Note that for passive proxies suppression is activated if connection is restored more than 15 seconds and no less than 2 & ProxyUpdateFrequency seconds later. For active proxies suppression is activated if connection is restored more than 15 seconds later. To turn off sensitiveness to proxy availability, use the third parameter, e.g.: `nodata(/host/key,5m,"strict")`; in this case the function will fire as soon as the evaluation period (five minutes) without data has past.<br>
-   This function will display an error if, within the period of the 1st parameter:<br>- there's no data and Zabbix server was restarted<br>- there's no data and maintenance was completed<br>- there's no data and the item was added or re-enabled<br>
-   Errors are displayed in the *Info* column in trigger [configuration](/manual/web_interface/frontend_sections/data_collection/hosts/triggers);<br>
-   This function may not work properly if there are time differences between Zabbix server, proxy and agent. See also: [Time synchronization requirement](/manual/installation/requirements#time_synchronization).

[comment]: # ({/new-178e3950})

[comment]: # ({new-e8041c00})

##### percentile(/host/key,(sec|#num)<:time shift>,percentage) {#percentile}

The P-th percentile of a period, where P (percentage) is specified by the third parameter.<br>
Supported value types: *Float*, *Integer*.

Parameters: 

-   See [common parameters](#common-parameters);<br>
-   **percentage** - a floating-point number between 0 and 100 (inclusive) with up to 4 digits after the decimal point.

[comment]: # ({/new-e8041c00})

[comment]: # ({new-3b1de601})

##### rate(/host/key,sec<:time shift>) {#rate}

The per-second average rate of the increase in a monotonically increasing counter within the defined time period.<br>
Supported value types: *Float*, *Integer*.

Parameters: 

-   See [common parameters](#common-parameters).

Functionally corresponds to '[rate](https://prometheus.io/docs/prometheus/latest/querying/functions/#rate)' of PromQL.

Example:

    rate(/host/key,30s) #if the monotonic increase over 30 seconds is 20, this function will return 0.67.

[comment]: # ({/new-3b1de601})


[comment]: # ({new-ce01d749})
#### History functions

|FUNCTION|<|<|<|
|--------|-|-|-|
|<|**Description**|**Function-specific parameters**|**Comments**|
|**change** (/host/key)|<|<|<|
|<|The amount of difference between the previous and latest value.|<|Supported value types: float, int, str, text, log<br><br>For strings returns:<br>0 - values are equal<br>1 - values differ<br><br>Example:<br>=> **change**(/host/key)>10<br><br>Numeric difference will be calculated, as seen with these incoming example values ('previous' and 'latest' value = difference):<br>'1' and '5' = `+4`<br>'3' and '1' = `-2`<br>'0' and '-2.5' = `-2.5`<br><br>See also: [abs](/manual/appendix/functions/math) for comparison|
|**changecount** (/host/key,(sec\|\#num)<:time shift>,<mode>)|<|<|<|
|<|Number of changes between adjacent values within the defined evaluation period.|See [common parameters](#common-parameters).<br><br>**mode** (optional; must be double-quoted)<br><br>Supported `modes`:<br>*all* - count all changes (default)<br>*dec* - count decreases<br>*inc* - count increases|Supported value types: float, int, str, text, log<br><br>For non-numeric value types, *mode* parameter is ignored.<br><br>Examples:<br>=> **changecount**(/host/key, 1w) → number of value changes for the last week until [now](/manual/config/triggers#evaluation_period)<br>=> **changecount**(/host/key,\#10,"inc") → number of value increases (relative to the adjacent value) among the last 10 values<br>=> **changecount**(/host/key,24h,"dec") → number of value decreases (relative to the adjacent value) for the last 24 hours until [now](/manual/config/triggers#evaluation_period)|
|**count** (/host/key,(sec\|\#num)<:time shift>,<operator>,<pattern>)|<|<|<|
|<|Number of values within the defined evaluation period.|See [common parameters](#common-parameters).<br><br>**operator** (optional; must be double-quoted)<br><br>Supported `operators`:<br>*eq* - equal (default)<br>*ne* - not equal<br>*gt* - greater<br>*ge* - greater or equal<br>*lt* - less<br>*le* - less or equal<br>*like* - matches if contains pattern (case-sensitive)<br>*bitand* - bitwise AND<br>*regexp* - case-sensitive match of the regular expression given in `pattern`<br>*iregexp* - case-insensitive match of the regular expression given in `pattern`<br><br>**pattern** (optional) - required pattern (string arguments must be double-quoted)|Supported value types: float, integer, string, text, log<br><br>Float items match with the precision of 2.22e-16; if database is [not upgraded](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500#enabling_extended_range_of_numeric_float_values) the precision is 0.000001.<br><br>With *bitand* as the third parameter, the fourth `pattern` parameter can be specified as two numbers, separated by '/': **number\_to\_compare\_with/mask**. count() calculates "bitwise AND" from the value and the *mask* and compares the result to *number\_to\_compare\_with*. If the result of "bitwise AND" is equal to *number\_to\_compare\_with*, the value is counted.<br>If *number\_to\_compare\_with* and *mask* are equal, only the *mask* need be specified (without '/').<br><br>With *regexp* or *iregexp* as the third parameter, the fourth `pattern` parameter can be an ordinary or [global](/manual/regular_expressions#global_regular_expressions) (starting with '@') regular expression. In case of global regular expressions case sensitivity is inherited from global regular expression settings. For the purpose of regexp matching, float values will always be represented with 4 decimal digits after '.'. Also note that for large numbers difference in decimal (stored in database) and binary (used by Zabbix server) representation may affect the 4th decimal digit.<br><br>Examples:<br>=> **count**(/host/key,**10m**) → number of values for the last 10 minutes until [now](/manual/config/triggers#evaluation_period)<br>=> **count**(/host/key,**10m**,"like","error") → number of values for the last 10 minutes until [now](/manual/config/triggers#evaluation_period) that contain 'error'<br>=> **count**(/host/key,**10m**,,12) → number of values for the last 10 minutes until [now](/manual/config/triggers#evaluation_period) that equal '12'<br>=> **count**(/host/key,**10m**,"gt",12) → number of values for the last 10 minutes until [now](/manual/config/triggers#evaluation_period) that are over '12'<br>=> **count**(/host/key,**\#10**,"gt",12) → number of values within the last 10 values until [now](/manual/config/triggers#evaluation_period) that are over '12'<br>=> **count**(/host/key,**10m:now-1d**,"gt",12) → number of values between 24 hours and 10 minutes and 24 hours ago from [now](/manual/config/triggers#evaluation_period) that were over '12'<br>=> **count**(/host/key,**10m**,"bitand","6/7") → number of values for the last 10 minutes until [now](/manual/config/triggers#evaluation_period) having '110' (in binary) in the 3 least significant bits.<br>=> **count**(/host/key,**10m:now-1d**) → number of values between 24 hours and 10 minutes and 24 hours ago from [now](/manual/config/triggers#evaluation_period)|
|**countunique** (/host/key,(sec\|\#num)<:time shift>,<operator>,<pattern>)|<|<|<|
|<|Number of unique values within the defined evaluation period.|See [common parameters](#common-parameters).<br><br>**operator** (optional; must be double-quoted)<br><br>Supported `operators`:<br>*eq* - equal (default)<br>*ne* - not equal<br>*gt* - greater<br>*ge* - greater or equal<br>*lt* - less<br>*le* - less or equal<br>*like* - matches if contains pattern (case-sensitive)<br>*bitand* - bitwise AND<br>*regexp* - case-sensitive match of the regular expression given in `pattern`<br>*iregexp* - case-insensitive match of the regular expression given in `pattern`<br><br>**pattern** (optional) - required pattern (string arguments must be double-quoted)|Supported value types: float, integer, string, text, log<br><br>Float items match with the precision of 2.22e-16; if database is [not upgraded](https://www.zabbix.com/documentation/5.0/manual/installation/upgrade_notes_500#enabling_extended_range_of_numeric_float_values) the precision is 0.000001.<br><br>With *bitand* as the third parameter, the fourth `pattern` parameter can be specified as two numbers, separated by '/': **number\_to\_compare\_with/mask**. count() calculates "bitwise AND" from the value and the *mask* and compares the result to *number\_to\_compare\_with*. If the result of "bitwise AND" is equal to *number\_to\_compare\_with*, the value is counted.<br>If *number\_to\_compare\_with* and *mask* are equal, only the *mask* need be specified (without '/').<br><br>With *regexp* or *iregexp* as the third parameter, the fourth `pattern` parameter can be an ordinary or [global](/manual/regular_expressions#global_regular_expressions) (starting with '@') regular expression. In case of global regular expressions case sensitivity is inherited from global regular expression settings. For the purpose of regexp matching, float values will always be represented with 4 decimal digits after '.'. Also note that for large numbers difference in decimal (stored in database) and binary (used by Zabbix server) representation may affect the 4th decimal digit.<br><br>Examples:<br>=> **countunique**(/host/key,**10m**) → number of unique values for the last 10 minutes until [now](/manual/config/triggers#evaluation_period)<br>=> **countunique**(/host/key,**10m**,"like","error") → number of unique values for the last 10 minutes until [now](/manual/config/triggers#evaluation_period) that contain 'error'<br>=> **countunique**(/host/key,**10m**,"gt",12) → number of unique values for the last 10 minutes until [now](/manual/config/triggers#evaluation_period) that are over '12'<br>=> **countunique**(/host/key,**\#10**,"gt",12) → number of unique values within the last 10 values until [now](/manual/config/triggers#evaluation_period) that are over '12'<br>=> **countunique**(/host/key,**10m:now-1d**,"gt",12) → number of unique values between 24 hours and 10 minutes and 24 hours ago from [now](/manual/config/triggers#evaluation_period) that were over '12'<br>=> **countunique**(/host/key,**10m**,"bitand","6/7") → number of unique values for the last 10 minutes until [now](/manual/config/triggers#evaluation_period) having '110' (in binary) in the 3 least significant bits.<br>=> **countunique**(/host/key,**10m:now-1d**) → number of unique values between 24 hours and 10 minutes and 24 hours ago from [now](/manual/config/triggers#evaluation_period)|
|**find** (/host/key,<(sec\|\#num)<:time shift>>,<operator>,<pattern>)|<|<|<|
|<|Find a value match.|See [common parameters](#common-parameters).<br><br>**sec** or **\#num** (optional) - defaults to the latest value if not specified<br><br>**operator** (optional; must be double-quoted)<br><br>Supported `operators`:<br>*eq* - equal (default)<br>*ne* - not equal<br>*gt* - greater<br>*ge* - greater or equal<br>*lt* - less<br>*le* - less or equal<br>*like* - value contains the string given in `pattern` (case-sensitive)<br>*bitand* - bitwise AND<br>*regexp* - case-sensitive match of the regular expression given in `pattern`<br>*iregexp* - case-insensitive match of the regular expression given in `pattern`<br><br>**pattern** - required pattern (string arguments must be double-quoted); [Perl Compatible Regular Expression](https://en.wikipedia.org/wiki/Perl_Compatible_Regular_Expressions) (PCRE) regular expression if `operator` is *regexp*, *iregexp*.|Supported value types: float, int, str, text, log<br><br>Returns:<br>1 - found<br>0 - otherwise<br><br>If more than one value is processed, '1' is returned if there is at least one matching value.<br><br>With *regexp* or *iregexp* as the third parameter, the fourth `pattern` parameter can be an ordinary or [global](/manual/regular_expressions#global_regular_expressions) (starting with '@') regular expression. In case of global regular expressions case sensitivity is inherited from global regular expression settings.<br><br>Example:<br>=> **find**(/host/key,**10m**,"like","error") → find a value that contains 'error' within the last 10 minutes until [now](/manual/config/triggers#evaluation_period)|
|**first** (/host/key,sec<:time shift>)|<|<|<|
|<|The first (the oldest) value within the defined evaluation period.|See [common parameters](#common-parameters).|Supported value types: float, int, str, text, log<br><br>Example:<br>=> **first**(/host/key,**1h**) → retrieve the oldest value within the last hour until [now](/manual/config/triggers#evaluation_period)<br><br>See also last().|
|**fuzzytime** (/host/key,sec)|<|<|<|
|<|Checking how much the passive agent time differs from the Zabbix server/proxy time.|See [common-parameters](#common parameters).|Supported value types: float, int<br><br>Returns:<br>1 - difference between the passive item value (as timestamp) and Zabbix server/proxy timestamp is less than or equal to T seconds<br>0 - otherwise<br><br>Usually used with the 'system.localtime' item to check that local time is in sync with the local time of Zabbix server. *Note* that 'system.localtime' must be configured as a [passive check](/manual/appendix/items/activepassive#passive_checks).<br>Can be used also with vfs.file.time\[/path/file,modify\] key to check that file didn't get updates for long time.<br><br>Example:<br>=> **fuzzytime**(/host/key,**60s**)=0 → detect a problem if the time difference is over 60 seconds<br><br>This function is not recommended for use in complex trigger expressions (with multiple items involved), because it may cause unexpected results (time difference will be measured with the most recent metric), e.g. in `fuzzytime(/Host/system.localtime,60s)=0 or last(/Host/trap)<>0`|
|**last** (/host/key,<\#num<:time shift>>)|<|<|<|
|<|The most recent value.|See [common parameters](#common-parameters).<br><br>**\#num** (optional) - the Nth most recent value|Supported value types: float, int, str, text, log<br><br>Take note that a hash-tagged time period (\#N) works differently here than with many other functions.<br>For example:<br>last() is always equal to last(\#1)<br>last(\#3) - third most recent value (*not* three latest values)<br><br>Zabbix does not guarantee the exact order of values if more than two values exist within one second in history.<br><br>Example:<br>=> **last**(/host/key) → retrieve the last value<br>=> **last**(/host/key,**\#2**) → retrieve the previous value<br>=> **last**(/host/key,**\#1**) <> **last**(/host/key,**\#2**) → the last and previous values differ<br><br>See also first().|
|**logeventid** (/host/key,<\#num<:time shift>>,<pattern>)|<|<|<|
|<|Checking if event ID of the last log entry matches a regular expression.|See [common parameters](#common-parameters).<br><br>**\#num** (optional) - the Nth most recent value<br><br>**pattern** (optional) - regular expression describing the required pattern, [Perl Compatible Regular Expression](https://en.wikipedia.org/wiki/Perl_Compatible_Regular_Expressions) (PCRE) style (string arguments must be double-quoted).|Supported value types: log<br><br>Returns:<br>0 - does not match<br>1 - matches|
|**logseverity** (/host/key,<\#num<:time shift>>)|<|<|<|
|<|Log severity of the last log entry.|See [common parameters](#common-parameters).<br><br>**\#num** (optional) - the Nth most recent value|Supported value types: log<br><br>Returns:<br>0 - default severity<br>N - severity (integer, useful for Windows event logs: 1 - Information, 2 - Warning, 4 - Error, 7 - Failure Audit, 8 - Success Audit, 9 - Critical, 10 - Verbose).<br>Zabbix takes log severity from **Information** field of Windows event log.|
|**logsource** (/host/key,<\#num<:time shift>>,<pattern>)|<|<|<|
|<|Checking if log source of the last log entry matches a regular expression.|See [common parameters](#common-parameters).<br><br>**\#num** (optional) - the Nth most recent value<br><br>**pattern** (optional) - regular expression describing the required pattern, [Perl Compatible Regular Expression](https://en.wikipedia.org/wiki/Perl_Compatible_Regular_Expressions) (PCRE) style (string arguments must be double-quoted).|Supported value types: log<br><br>Returns:<br>0 - does not match<br>1 - matches<br><br>Normally used for Windows event logs. For example, logsource("VMware Server").|
|**monodec** (/host/key,(sec\|\#num)<:time shift>,<mode>)|<|<|<|
|<|Check if there has been a monotonous decrease in values.|See [common parameters](#common-parameters).<br><br>**mode** (must be double-quoted) - *weak* (every value is smaller or the same as the previous one; default) or *strict* (every value has decreased)|Supported value types: int<br><br>Returns 1 if all elements in the time period continuously decrease, 0 otherwise.<br><br>Example:<br>=> **monodec**(/Host1/system.swap.size\[all,free\],**60s**) + **monodec**(/Host2/system.swap.size\[all,free\],**60s**) + **monodec**(/Host3/system.swap.size\[all,free\],**60s**) - calculate in how many hosts there has been a decrease in free swap size|
|**monoinc** (/host/key,(sec\|\#num)<:time shift>,<mode>)|<|<|<|
|<|Check if there has been a monotonous increase in values.|See [common parameters](#common-parameters).<br><br>**mode** (must be double-quoted) - *weak* (every value is bigger or the same as the previous one; default) or *strict* (every value has increased)|Supported value types: int<br><br>Returns 1 if all elements in the time period continuously increase, 0 otherwise.<br><br>Example:<br>=> **monoinc**(/Host1/system.localtime,**\#3**,"strict")=0 - check if system local time has been increasing consistently|
|**nodata** (/host/key,sec,<mode>)|<|<|<|
|<|Checking for no data received.|See [common parameters](#common-parameters).<br><br>**sec** period should not be less than 30 seconds because the history syncer process calculates this function only every 30 seconds.<br><br>nodata(/host/key,0) is disallowed.<br><br>**mode** - if set to *strict* (double-quoted), this function will be insensitive to proxy availability (see comments for details).|All value types are supported.<br><br>Returns:<br>1 - if no data received during the defined period of time<br>0 - otherwise<br><br>Since Zabbix 5.0, the 'nodata' triggers monitored by proxy are, by default, sensitive to proxy availability - if proxy becomes unavailable, the 'nodata' triggers will not fire immediately after a restored connection, but will skip the data for the delayed period. Note that for passive proxies suppression is activated if connection is restored more than 15 seconds and no less than 2 & ProxyUpdateFrequency seconds later. For active proxies suppression is activated if connection is restored more than 15 seconds later.<br><br>To turn off sensitiveness to proxy availability, use the third parameter, e.g.: **nodata**(/host/key,**5m**,**"strict"**); in this case the function will work the same as before 5.0.0 and fire as soon as the evaluation period (five minutes) without data has past.<br><br>Note that this function will display an error if, within the period of the 1st parameter:<br>- there's no data and Zabbix server was restarted<br>- there's no data and maintenance was completed<br>- there's no data and the item was added or re-enabled<br>Errors are displayed in the *Info* column in trigger [configuration](/manual/web_interface/frontend_sections/configuration/hosts/triggers).<br><br>This function may not work properly if there are time differences between Zabbix server, proxy and agent. See also: [Time synchronization requirement](/manual/installation/requirements#time_synchronization).|
|**percentile** (/host/key,(sec\|\#num)<:time shift>,percentage)|<|<|<|
|<|P-th percentile of a period, where P (percentage) is specified by the third parameter.|See [common parameters](#common-parameters).<br><br>**percentage** - a floating-point number between 0 and 100 (inclusive) with up to 4 digits after the decimal point|Supported value types: float, int|
|**rate** (/host/key,sec<:time shift>)|<|<|<|
|<|Per-second average rate of the increase in a monotonically increasing counter within the defined time period.|See [common parameters](#common-parameters).|Supported value types: float, int<br><br>Functionally corresponds to '[rate](https://prometheus.io/docs/prometheus/latest/querying/functions/#rate)' of PromQL.<br><br>Example:<br>=> **rate**(/host/key,**30s**) → If the monotonic increase over 30 seconds is 20, this function will return 0.67.|
|**trendavg** (/host/key,time period:time shift)|<|<|<|
|<|Average of trend values within the defined time period.|**time period** - the time period (minimum '1h'), defined as <N><time unit> where<br>`N` - number of time units<br>`time unit` - h (hour), d (day), w (week), M (month) or y (year).<br><br>[Time shift](/manual/config/triggers/expression#function-specific_parameters) - the time period offset (see examples)|Examples:<br>=> **trendavg**(/host/key,**1h:now/h**) → average for the previous hour (e.g. 12:00-13:00)<br>=> **trendavg**(/host/key,**1h:now/h-1h**) → average for two hours ago (11:00-12:00)<br>=> **trendavg**(/host/key,**1h:now/h-2h**) → average for three hours ago (10:00-11:00)<br>=> **trendavg**(/host/key,**1M:now/M-1y**) → average for the previous month a year ago|
|**trendcount** (/host/key,time period:time shift)|<|<|<|
|<|Number of successfully retrieved trend values within the defined time period.|**time period** - the time period (minimum '1h'), defined as <N><time unit> where<br>`N` - number of time units<br>`time unit` - h (hour), d (day), w (week), M (month) or y (year).<br><br>[Time shift](/manual/config/triggers/expression#function-specific_parameters) - the time period offset (see examples)|Examples:<br>=> **trendcount**(/host/key,**1h:now/h**) → count for the previous hour (e.g. 12:00-13:00)<br>=> **trendcount**(/host/key,**1h:now/h-1h**) → count for two hours ago (11:00-12:00)<br>=> **trendcount**(/host/key,**1h:now/h-2h**) → count for three hours ago (10:00-11:00)<br>=> **trendcount**(/host/key,**1M:now/M-1y**) → count for the previous month a year ago|
|**trendmax** (/host/key,time period:time shift)|<|<|<|
|<|The maximum in trend values within the defined time period.|**time period** - the time period (minimum '1h'), defined as <N><time unit> where<br>`N` - number of time units<br>`time unit` - h (hour), d (day), w (week), M (month) or y (year).<br><br>[Time shift](/manual/config/triggers/expression#function-specific_parameters) - the time period offset (see examples)|Examples:<br>=> **trendmax**(/host/key,**1h:now/h**) → maximum for the previous hour (e.g. 12:00-13:00)<br>=> **trendmax**(/host/key,**1h:now/h**) - **trendmin**(/host/key,**1h:now/h**) → calculate the difference between the maximum and minimum values (trend delta) for the previous hour (12:00-13:00)<br>=> **trendmax**(/host/key,**1h:now/h-1h**) → maximum for two hours ago (11:00-12:00)<br>=> **trendmax**(/host/key,**1h:now/h-2h**) → maximum for three hours ago (10:00-11:00)<br>=> **trendmax**(/host/key,**1M:now/M-1y**) → maximum for the previous month a year ago|
|**trendmin** (/host/key,time period:time shift)|<|<|<|
|<|The minimum in trend values within the defined time period.|**time period** - the time period (minimum '1h'), defined as <N><time unit> where<br>`N` - number of time units<br>`time unit` - h (hour), d (day), w (week), M (month) or y (year).<br><br>[Time shift](/manual/config/triggers/expression#function-specific_parameters) - the time period offset (see examples)|Examples:<br>=> **trendmin**(/host/key,**1h:now/h**) → minimum for the previous hour (e.g. 12:00-13:00)<br>=> **trendmin**(/host/key,**1h:now/h**) - **trendmin**(/host/key,**1h:now/h**) → calculate the difference between the maximum and minimum values (trend delta) for the previous hour (12:00-13:00)<br>=> **trendmin**(/host/key,**1h:now/h-1h**) → minimum for two hours ago (11:00-12:00)<br>=> **trendmin**(/host/key,**1h:now/h-2h**) → minimum for three hours ago (10:00-11:00)<br>=> **trendmin**(/host/key,**1M:now/M-1y**) → minimum for the previous month a year ago|
|**trendstl** (/host/key,eval period:time shift,detection period,season,<deviations>,<devalg>,<s_window>)|<|<|<|
|<|Returns an anomaly rate - a decimal value between 0 and 1 that is ((the number of anomaly values in detect period) / (total number of values in detect period).|**eval period** - the time period that must be decomposed (minimum '1h'), defined as <N><time unit> where<br>`N` - number of time units<br>`time unit` - h (hour), d (day), w (week), M (month) or y (year).<br><br>[Time shift](/manual/config/triggers/expression#function-specific_parameters) - the time period offset (see examples)<br><br>**detection period** - the time period starting from the end of eval period for which anomalies are calculated (minimum '1h', cannot be longer than eval period), defined as <N><time unit> where<br>`N` - number of time units<br>`time unit` - h (hour), d (day), w (week).<br><br>**season** - the shortest time period where seasonality (repeating patterns) is expected (minimum '2h', cannot be longer than eval period, number of entries in the eval period must be greater than the two times of the resulting frequency (season/h)), defined as <N><time unit> where<br>`N` - number of time units<br>`time unit` - h (hour), d (day), w (week).<br><br>**deviations** - the number of deviations (calculated with devalg) to count as anomaly (can be decimal), (must be greater than or equal to 1, default is 3)<br><br>**devalg** - deviation algorithm, can be 'stddevpop', 'stddevsamp' or 'mad' (default)<br><br>**s_window** - the span (in lags) of the loess window for seasonal extraction (default is 10 * number of entries in eval period + 1)|Examples:<br>=> **trendstl**(/host/key,**100h:now/h**,10h,2h) →  analysing the last 100 hours of trend data,<br>find the anomalies rate for the previous 10 hours of that period,<br>expecting the periodicity to be 2h,<br>the remainder series values of the evaluation period are considered anomalies if they reach the value of 3 deviations of the MAD of that remainder series<br>=> **trendstl**(/host/key,**100h:now/h-10h**,100h,2h,2.1,'mad') →  analysing the period of previous 100 hours of trend data, starting counting 10 hours ago<br>find the anomalies rate for that entire period<br>expecting the periodicity to be 2h,<br>the remainder series values of the evaluation period are considered anomalies if they reach the value of  2,1 deviations of the MAD of that remainder series<br>=> **trendstl**(/host/key,**100d:now/d-1d**,10d,1d,4,,10) → analysing previous 100 days of trend data starting from a day ago,<br>find the anomalies rate for the period of last 10d of that period,<br>expecting the periodicity to be 1d,<br>the remainder series values of the evaluation period are considered anomalies if they reach the value of 4 deviations of the MAD of that remainder series,<br>overriding the default span of the loess window for seasonal extraction of "10 * number of entries in eval period + 1" with the span of 10 lags<br>=> **trendstl**(/host/key,**1M:now/M-1y**,1d,2h,,'stddevsamp') → minimum for the previous month a year ago<br>analysing previous month of trend data starting from a year ago,<br>find the anomalies rate of the last day of that period<br>expecting the periodicity to be 2h,<br>the remainder series values of the evaluation period are considered anomalies if they reach the value of 3 deviation of the sample standard deviation of that remainder series|
|**trendsum** (/host/key,time period:time shift)|<|<|<|
|<|Sum of trend values within the defined time period.|**time period** - the time period (minimum '1h'), defined as <N><time unit> where<br>`N` - number of time units<br>`time unit` - h (hour), d (day), w (week), M (month) or y (year).<br><br>[Time shift](/manual/config/triggers/expression#function-specific_parameters) - the time period offset (see examples)|Examples:<br>=> **trendsum**(/host/key,**1h:now/h**) → sum for the previous hour (e.g. 12:00-13:00)<br>=> **trendsum**(/host/key,**1h:now/h-1h**) → sum for two hours ago (11:00-12:00)<br>=> **trendsum**(/host/key,**1h:now/h-2h**) → sum for three hours ago (10:00-11:00)<br>=> **trendsum**(/host/key,**1M:now/M-1y**) → sum for the previous month a year ago|

[comment]: # ({/new-ce01d749})
