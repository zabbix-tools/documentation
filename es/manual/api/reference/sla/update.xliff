<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="es" datatype="plaintext" original="manual/api/reference/sla/update.md">
    <body>
      <trans-unit id="cd4a930c" xml:space="preserve">
        <source># sla.update</source>
      </trans-unit>
      <trans-unit id="79afe9b7" xml:space="preserve">
        <source>### Description

`object sla.update(object/array slaids)`

This method allows to update existing SLA entries.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::</source>
      </trans-unit>
      <trans-unit id="5fb5ebb3" xml:space="preserve">
        <source>### Parameters

`(object/array)` SLA properties to be updated.

The `slaid` property must be defined for each SLA, all other
properties are optional. Only the passed properties will be updated, all
others will remain unchanged.

Additionally to the [standard SLA properties](object#sla), the
method accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|service\_tags|array|[SLA service tags](/manual/api/reference/sla/object#sla-service-tag) to replace the current SLA service tags.|
|schedule|array|[SLA schedule](/manual/api/reference/sla/object#sla-schedule) to replace the current one.&lt;br&gt;&lt;br&gt;Specifying parameter as empty will be interpreted as a 24x7 schedule.|
|excluded\_downtimes|array|[SLA excluded downtimes](/manual/api/reference/sla/object#sla-excluded-downtime) to replace the current ones.|</source>
      </trans-unit>
      <trans-unit id="9465ee4d" xml:space="preserve">
        <source>### Return values

`(object)` Returns an object containing the IDs of the updated SLAs under the `slaids` property.</source>
      </trans-unit>
      <trans-unit id="b41637d2" xml:space="preserve">
        <source>### Examples</source>
      </trans-unit>
      <trans-unit id="b9918036" xml:space="preserve">
        <source>#### Updating service tags

Make SLA with ID "5" to be calculated at monthly intervals for NoSQL related services,
without changing its schedule or excluded downtimes; set SLO to 95%.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "sla.update",
    "params": [
        {
            "slaid": "5",
            "name": "NoSQL Database engines",
            "slo": "95",
            "period": 2,
            "service_tags": [
                {
                    "tag": "Database",
                    "operator": "0",
                    "value": "Redis"
                },
                {
                    "tag": "Database",
                    "operator": "0",
                    "value": "MongoDB"
                }
            ]
        }
    ],
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "slaids": [
            "5"
        ]
    },
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="263b1baa" xml:space="preserve">
        <source>#### Changing the schedule of an SLA

Switch the SLA with ID "5" to a 24x7 schedule.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "service.update",
    "params": {
        "slaid": "5",
        "schedule": []
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "slaids": [
            "5"
        ]
    },
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="d4685b1d" xml:space="preserve">
        <source>#### Changing the excluded downtimes for an SLA

Add a planned 4 hour long RAM upgrade downtime on the 6th of April, 2022,
while keeping (needs to be defined anew) a previously existing software upgrade planned on the 4th of July
for the SLA with ID "5".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "service.update",
    "params": {
        "slaid": "5",
        "excluded_downtimes": [
            {
                "name": "Software version upgrade rollout",
                "period_from": "1648760400",
                "period_to": "1648764900"
            },
            {
                "name": "RAM upgrade",
                "period_from": "1649192400",
                "period_to": "1649206800"
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "slaids": [
            "5"
        ]
    },
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="fd1b6894" xml:space="preserve">
        <source>### Source

CSla::update() in *ui/include/classes/api/services/CSla.php*.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
