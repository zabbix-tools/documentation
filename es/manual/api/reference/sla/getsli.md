[comment]: # translation:outdated

[comment]: # ({new-f9178426})
# sla.getsli

[comment]: # ({/new-f9178426})

[comment]: # ({new-531f7cc6})
### Description

`object sla.getsli(object parameters)`

This method allows to calculate the Service Level Indicator (SLI) data.

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-531f7cc6})

[comment]: # ({new-bcbf1cf2})
### Parameters

`(object)` Parameters containing the SLA ID, reporting periods and, optionally,
the IDs of the services - to calculate the SLI for.

| Parameter               | [Type](/manual/api/reference_commentary#data_types) | Description                                                                         |
|-------------------------|-----------------------------------------------------|-------------------------------------------------------------------------------------|
| **slaid**<br>(required) | string                                              | IDs of services to return availability information for.                             |
| period\_from            | integer                                             | Starting date (inclusive) to report the SLI for.<br><br>Possible values: timestamp. |
| period\_to              | integer                                             | Ending date (exclusive) to report the SLI for.<br><br>Possible values: timestamp.   |
| periods                 | array                                               | Preferred number of periods to report.<br><br>Possible values: 1-100                |
| serviceids              | string/array                                        | IDs of services to return the SLI for.                                              |

[comment]: # ({/new-bcbf1cf2})

[comment]: # ({new-31bc72e4})
#### Partitioning of periods

The following demonstrates the arrangement of returned _period slices_ based on combinations of parameters.

| Parameters       |                |             | Description                                                                                                                                                               |
|------------------|----------------|-------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **period\_from** | **period\_to** | **periods** |                                                                                                                                                                           |
| -                | -              | -           | The last 20 periods (including the current one) but not past the first available period based on the effective date of the SLA.                                           |
| -                | -              | specified   | The last periods specified by the **periods** parameter.                                                                                                                  |
| -                | specified      | -           | The last 20 periods before the **specified date**, but not past the first available period based on the effective date of the SLA.                                        |
| -                | specified      | specified   | The last periods specified by the **periods** parameter before the **specified date**.                                                                                    |
| specified        | -              | -           | The first 20 periods (including the current one) but not past the current one.                                                                                            |
| specified        | -              | specified   | The first periods specified by the **periods** parameter starting with the **specified date**.                                                                            |
| specified        | specified      | -           | Periods within the specified date range, but no more than 100 and not past the first available period based on the effective date of the SLA.                             |
| specified        | specified      | specified   | Periods within the specified date range, but no more than the specified number of periods and not past the first available period based on the effective date of the SLA. |

[comment]: # ({/new-31bc72e4})

[comment]: # ({new-b46f1257})
### Return values

`(object)` Returns the results of the calculation.

| Property   | [Type](/manual/api/reference_commentary#data_types) | Description                                                                                                                                                                                                                                                                                                  |
|------------|-----------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| periods    | array                                               | List of the reported periods.<br><br>Each reported period is represented as an object consisting of:<br>- _period\_from_ - Starting date of the reported period (timestamp).<br>- _period\_to_ - Ending date of the reported period (timestamp).<br><br>Periods are sorted by `period_from` field ascending. |
| serviceids | array                                               | List of service IDs in the reported periods.<br><br>The sorting order of the list is not defined. Even if `serviceids` parameter was passed to the `sla.getsli` method.                                                                                                                                      |
| sli        | array                                               | SLI data (as a **two-dimensional array**) for each reported period and service.<br><br>The index of the `periods` property is used as the **first** dimension of the `sli` property.<br><br>The index of the `serviceids` property is used as the **second** dimension of the `sli` property.                |

[comment]: # ({/new-b46f1257})

[comment]: # ({new-ba0b5441})
#### SLI data

The SLI data returned for each reported period and service consists of:

| Property            | [Type](/manual/api/reference_commentary#data_types) | Description                                                                                                                                                                                                                                                                                                                                                                                         |
|---------------------|-----------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| uptime              | integer                                             | Amount of time service spent in an _OK_ state during scheduled uptime, less the excluded downtimes.                                                                                                                                                                                                                                                                                                 |
| downtime            | integer                                             | Amount of time service spent in a _not OK_ state during scheduled uptime, less the excluded downtimes.                                                                                                                                                                                                                                                                                              |
| sli                 | float                                               | SLI (per cent of total uptime), based on uptime and downtime.                                                                                                                                                                                                                                                                                                                                       |
| error\_budget       | integer                                             | Error budget (in seconds), based on the SLI and the SLO.                                                                                                                                                                                                                                                                                                                                            |
| excluded\_downtimes | array                                               | Array of excluded downtimes in this reporting period.<br><br>Each object will contain the following parameters:<br>- _name_ - Name of the excluded downtime.<br>- _period\_from_ - Starting date and time (inclusive) of the excluded downtime.<br>- _period\_to_ - Ending date and time (exclusive) of the excluded downtime.<br><br>Excluded downtimes are sorted by period_from field ascending. |

[comment]: # ({/new-ba0b5441})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-c6c818e1})
#### Calculating SLI

Retrieve SLI on services with IDs "50, 60 and 70" linked to an SLA with ID of "5" for 3 periods starting from Nov 01, 2021.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "service.getsla",
    "params": {
        "slaid": "5",
        "serviceids": [
            50,
            60,
            70
        ],
        "periods": 3,
        "period_from": "1635724800"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "periods": [
            {
                "period_from": 1635724800,
                "period_to": 1638316800
            },
            {
                "period_from": 1638316800,
                "period_to": 1640995200
            },
            {
                "period_from": 1640995200,
                "period_to": 1643673600
            }
        ],
        "serviceids": [
            50,
            60,
            70
        ],
        "sli": [
            [
                {
                    "uptime": 1186212,
                    "downtime": 0,
                    "sli": 100,
                    "error_budget": 0,
                    "excluded_downtimes": [
                        {
                            "name": "Excluded Downtime - 1",
                            "period_from": 1637836212,
                            "period_to": 1638316800
                        }
                    ]
                },
                {
                    "uptime": 1186212,
                    "downtime": 0,
                    "sli": 100,
                    "error_budget": 0,
                    "excluded_downtimes": [
                        {
                            "name": "Excluded Downtime - 1",
                            "period_from": 1637836212,
                            "period_to": 1638316800
                        }
                    ]
                },
                {
                    "uptime": 1186212,
                    "downtime": 0,
                    "sli": 100,
                    "error_budget": 0,
                    "excluded_downtimes": [
                        {
                            "name": "Excluded Downtime - 1",
                            "period_from": 1637836212,
                            "period_to": 1638316800
                        }
                    ]
                }
            ],
            [
                {
                    "uptime": 1147548,
                    "downtime": 0,
                    "sli": 100,
                    "error_budget": 0,
                    "excluded_downtimes": [
                        {
                            "name": "Excluded Downtime - 1",
                            "period_from": 1638439200,
                            "period_to": 1639109652
                        }
                    ]
                },
                {
                    "uptime": 1147548,
                    "downtime": 0,
                    "sli": 100,
                    "error_budget": 0,
                    "excluded_downtimes": [
                        {
                            "name": "Excluded Downtime - 1",
                            "period_from": 1638439200,
                            "period_to": 1639109652
                        }
                    ]
                },
                {
                    "uptime": 1147548,
                    "downtime": 0,
                    "sli": 100,
                    "error_budget": 0,
                    "excluded_downtimes": [
                        {
                            "name": "Excluded Downtime - 1",
                            "period_from": 1638439200,
                            "period_to": 1639109652
                        }
                    ]
                }
            ],
            [
                {
                    "uptime": 1674000,
                    "downtime": 0,
                    "sli": 100,
                    "error_budget": 0,
                    "excluded_downtimes": []
                },
                {
                    "uptime": 1674000,
                    "downtime": 0,
                    "sli": 100,
                    "error_budget": 0,
                    "excluded_downtimes": []
                },
                {
                    "uptime": 1674000,
                    "downtime": 0,
                    "sli": 100,
                    "error_budget": 0,
                    "excluded_downtimes": []
                }
            ]
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-c6c818e1})

[comment]: # ({new-a5414084})
### Source

CService::getSla() in *ui/include/classes/api/services/CService.php*.

[comment]: # ({/new-a5414084})
