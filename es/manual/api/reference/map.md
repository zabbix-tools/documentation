[comment]: # translation:outdated

[comment]: # ({new-79c270c0})
# Map

This class is designed to work with maps.

Object references:\

-   [Map](/manual/api/reference/map/object#map)
-   [Map element](/manual/api/reference/map/object#map_element)
-   [Map link](/manual/api/reference/map/object#map_link)
-   [Map URL](/manual/api/reference/map/object#map_url)
-   [Map user](/manual/api/reference/map/object#map_user)
-   [Map user group](/manual/api/reference/map/object#map_user_group)
-   [Map shape](/manual/api/reference/map/object#map_shapes)
-   [Map line](/manual/api/reference/map/object#map_lines)

Available methods:\

-   [map.create](/manual/api/reference/map/create) - create new maps
-   [map.delete](/manual/api/reference/map/delete) - delete maps
-   [map.get](/manual/api/reference/map/get) - retrieve maps
-   [map.update](/manual/api/reference/map/update) - update maps

[comment]: # ({/new-79c270c0})
