[comment]: # translation:outdated

[comment]: # ({new-17e99ab8})
# housekeeping.update

[comment]: # ({/new-17e99ab8})

[comment]: # ({new-e666a98a})
### Description

`object housekeeping.update(object housekeeping)`

This method allows to update existing housekeeping settings.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-e666a98a})

[comment]: # ({new-59628702})
### Parameters

`(object)` Housekeeping properties to be updated.

[comment]: # ({/new-59628702})

[comment]: # ({new-440363c6})
### Return values

`(array)` Returns array with the names of updated parameters.

[comment]: # ({/new-440363c6})

[comment]: # ({new-16b418fc})
### Examples

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "housekeeping.update",
    "params": {
        "hk_events_mode": "1",
        "hk_events_trigger": "200d",
        "hk_events_internal": "2d",
        "hk_events_discovery": "2d"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        "hk_events_mode",
        "hk_events_trigger",
        "hk_events_internal",
        "hk_events_discovery"
    ],
    "id": 1
}
```

[comment]: # ({/new-16b418fc})

[comment]: # ({new-d089883f})
### Source

CHousekeeping::update() in
*ui/include/classes/api/services/CHousekeeping.php*.

[comment]: # ({/new-d089883f})
