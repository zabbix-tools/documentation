[comment]: # translation:outdated

[comment]: # ({new-517ea62c})
# Host prototype

This class is designed to work with host prototypes.

Object references:\

-   [Host
    prototype](/manual/api/reference/hostprototype/object#host_prototype)
-   [Host prototype
    inventory](/manual/api/reference/hostprototype/object#host_prototype_inventory)
-   [Group link](/manual/api/reference/hostprototype/object#group_link)
-   [Group
    prototype](/manual/api/reference/hostprototype/object#group_prototype)

Available methods:\

-   [hostprototype.create](/manual/api/reference/hostprototype/create) -
    creating new host prototypes
-   [hostprototype.delete](/manual/api/reference/hostprototype/delete) -
    deleting host prototypes
-   [hostprototype.get](/manual/api/reference/hostprototype/get) -
    retrieving host prototypes
-   [hostprototype.update](/manual/api/reference/hostprototype/update) -
    updating host prototypes

[comment]: # ({/new-517ea62c})
