<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="es" datatype="plaintext" original="manual/api/reference/triggerprototype/update.md">
    <body>
      <trans-unit id="be70f0cc" xml:space="preserve">
        <source># triggerprototype.update</source>
      </trans-unit>
      <trans-unit id="842b5955" xml:space="preserve">
        <source>### Description

`object triggerprototype.update(object/array triggerPrototypes)`

This method allows to update existing trigger prototypes.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::</source>
      </trans-unit>
      <trans-unit id="827e4920" xml:space="preserve">
        <source>### Parameters

`(object/array)` Trigger prototype properties to be updated.

The `triggerid` property must be defined for each trigger prototype, all
other properties are optional. Only the passed properties will be
updated, all others will remain unchanged.

Additionally to the [standard trigger prototype
properties](object#trigger_prototype) the method accepts the following
parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|dependencies|array|Triggers and trigger prototypes that the trigger prototype is dependent on.&lt;br&gt;&lt;br&gt;The triggers must have the `triggerid` property defined.|
|tags|array|Trigger prototype [tags](/manual/api/reference/triggerprototype/object#trigger_prototype_tag).|

::: noteimportant
The trigger expression has to be given in its
expanded form and must contain at least one item prototype.
:::</source>
      </trans-unit>
      <trans-unit id="200d7b9d" xml:space="preserve">
        <source>### Return values

`(object)` Returns an object containing the IDs of the updated trigger
prototypes under the `triggerids` property.</source>
      </trans-unit>
      <trans-unit id="b41637d2" xml:space="preserve">
        <source>### Examples</source>
      </trans-unit>
      <trans-unit id="bcca41dc" xml:space="preserve">
        <source>#### Enabling a trigger prototype

Enable a trigger prototype, that is, set its status to "0".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "triggerprototype.update",
    "params": {
        "triggerid": "13938",
        "status": 0
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "triggerids": [
            "13938"
        ]
    },
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="724af3ce" xml:space="preserve">
        <source>#### Replacing trigger prototype tags

Replace tags for one trigger prototype.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "triggerprototype.update",
    "params": {
        "triggerid": "17373",
        "tags": [
            {
                "tag": "volume",
                "value": "{#FSNAME}"
            },
            {
                "tag": "type",
                "value": "{#FSTYPE}"
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "triggerids": [
            "17373"
        ]
    },
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="585ae96a" xml:space="preserve">
        <source>### Source

CTriggerPrototype::update() in
*ui/include/classes/api/services/CTriggerPrototype.php*.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
