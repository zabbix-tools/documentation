[comment]: # translation:outdated

[comment]: # ({new-f9e8f670})
# hostgroup.propagate


[comment]: # ({/new-f9e8f670})



[comment]: # ({new-b5ff62ed})
### Description

`object hostgroup.propagate(object parameters)`

This method allows to apply permissions and tag filters to all hosts groups' subgroups.

::: noteclassic
This method is only available to *Super admin* user types. 
Permissions to call the method can be revoked in user role settings. 
See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::


[comment]: # ({/new-b5ff62ed})



[comment]: # ({new-6102edd8})
### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

| Parameter            | [Type](/manual/api/reference_commentary#data_types) | Description                                                                             |
|----------------------|---------------------------------------------------|-----------------------------------------------------------------------------------------|
|**groups**<br>(required)|object/array| Host groups to propagate.<br><br>The host groups must have the `groupid` property defined. |
|permissions|boolean|Set `true` if need to propagate permissions.|
|tag_filters|boolean|Set `true` if need to propagate tag filters.|

At least one parameter `permissions` or `tag_filters` is required.


[comment]: # ({/new-6102edd8})



[comment]: # ({new-42ead635})
### Return values

`(object)` Returns an object containing the IDs of the propagated host
groups under the `groupids` property.


[comment]: # ({/new-42ead635})



[comment]: # ({new-018502e3})
### Examples


[comment]: # ({/new-018502e3})



[comment]: # ({new-8ec85132})
#### Propagating host group permissions and tag filters to its subgroups.

Propagate host group permissions and tag filters to its subgroups.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "hostgroup.propagate",
    "params": {
        "groups": [
            {
                "groupid": "6"
            }
        ],
        "permissions": true,
        "tag_filters": true
    },
    "auth": "f223adf833b2bf2ff38574a67bba6372",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "groupids": [
            "6",
        ]
    },
    "id": 1
}
```


[comment]: # ({/new-8ec85132})



[comment]: # ({new-10bafee5})
### See also

-   [hostgroup.update](update)
-   [hostgroup.massadd](massadd)
-   [Host](/manual/api/reference/host/object#host)


[comment]: # ({/new-10bafee5})



[comment]: # ({new-a745b72a})
### Source

CHostGroup::propagate() in
*ui/include/classes/api/services/CHostGroup.php*.


[comment]: # ({/new-a745b72a})
