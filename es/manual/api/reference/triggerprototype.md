[comment]: # translation:outdated

[comment]: # ({new-2d53bfd4})
# Trigger prototype

This class is designed to work with trigger prototypes.

Object references:\

-   [Trigger
    prototype](/manual/api/reference/triggerprototype/object#trigger_prototype)

Available methods:\

-   [triggerprototype.create](/manual/api/reference/triggerprototype/create) -
    creating new trigger prototypes
-   [triggerprototype.delete](/manual/api/reference/triggerprototype/delete) -
    deleting trigger prototypes
-   [triggerprototype.get](/manual/api/reference/triggerprototype/get) -
    retrieving trigger prototypes
-   [triggerprototype.update](/manual/api/reference/triggerprototype/update) -
    updating trigger prototypes

[comment]: # ({/new-2d53bfd4})
