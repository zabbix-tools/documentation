[comment]: # translation:outdated

[comment]: # ({new-11ad57db})
# Authentication

This class is designed to work with authentication settings.

Object references:\

-   [Authentication](/manual/api/reference/authentication/object#authentication)

Available methods:\

-   [authentication.get](/manual/api/reference/authentication/get) -
    retrieve authentication
-   [authentication.update](/manual/api/reference/authentication/update) -
    update authentication

[comment]: # ({/new-11ad57db})
