[comment]: # translation:outdated

[comment]: # ({new-e205e572})
# > Correlation object

The following objects are directly related to the `correlation` API.

[comment]: # ({/new-e205e572})

[comment]: # ({new-935ba6ef})
### Correlation

The correlation object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|correlationid|string|*(readonly)* ID of the correlation.|
|**name**<br>(required)|string|Name of the correlation.|
|description|string|Description of the correlation.|
|status|integer|Whether the correlation is enabled or disabled.<br><br>Possible values are:<br>0 - *(default)* enabled;<br>1 - disabled.|

[comment]: # ({/new-935ba6ef})

[comment]: # ({new-d12c2169})
### Correlation operation

The correlation operation object defines an operation that will be
performed when a correlation is executed. It has the following
properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**type**<br>(required)|integer|Type of operation.<br><br>Possible values:<br>0 - close old events;<br>1 - close new event.|

[comment]: # ({/new-d12c2169})

[comment]: # ({new-d0662dba})
### Correlation filter

The correlation filter object defines a set of conditions that must be
met to perform the configured correlation operations. It has the
following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**evaltype**<br>(required)|integer|Filter condition evaluation method.<br><br>Possible values:<br>0 - and/or;<br>1 - and;<br>2 - or;<br>3 - custom expression.|
|**conditions**<br>(required)|array|Set of filter conditions to use for filtering results.|
|eval\_formula|string|*(readonly)* Generated expression that will be used for evaluating filter conditions. The expression contains IDs that reference specific filter conditions by its `formulaid`. The value of `eval_formula` is equal to the value of `formula` for filters with a custom expression.|
|formula|string|User-defined expression to be used for evaluating conditions of filters with a custom expression. The expression must contain IDs that reference specific filter conditions by its `formulaid`. The IDs used in the expression must exactly match the ones defined in the filter conditions: no condition can remain unused or omitted.<br><br>Required for custom expression filters.|

[comment]: # ({/new-d0662dba})

[comment]: # ({new-72ee5420})
#### Correlation filter condition

The correlation filter condition object defines a specific condition
that must be checked before running the correlation operations.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**type**<br>(required)|integer|Type of condition.<br><br>Possible values:<br>0 - old event tag;<br>1 - new event tag;<br>2 - new event host group;<br>3 - event tag pair;<br>4 - old event tag value;<br>5 - new event tag value.|
|tag|string|Event tag (old or new). Required when type of condition is: 0, 1, 4, 5.|
|groupid|string|Host group ID. Required when type of condition is: 2.|
|oldtag|string|Old event tag. Required when type of condition is: 3.|
|newtag|string|Old event tag. Required when type of condition is: 3.|
|value|string|Event tag (old or new) value. Required when type of condition is: 4, 5.|
|formulaid|string|Arbitrary unique ID that is used to reference the condition from a custom expression. Can only contain capital-case letters. The ID must be defined by the user when modifying filter conditions, but will be generated anew when requesting them afterward.|
|operator|integer|Condition operator.<br><br>Required when type of condition is: 2, 4, 5.|

::: notetip
To better understand how to use filters with various
types of expressions, see examples on the
[correlation.get](get#retrieve_correlations) and
[correlation.create](create#using_a_custom_expression_filter) method
pages.
:::

The following operators and values are supported for each condition
type.

|Condition|Condition name|Supported operators|Expected value|
|---------|--------------|-------------------|--------------|
|2|Host group|=, <>|Host group ID.|
|4|Old event tag value|=, <>, like, not like|string|
|5|New event tag value|=, <>, like, not like|string|

[comment]: # ({/new-72ee5420})
