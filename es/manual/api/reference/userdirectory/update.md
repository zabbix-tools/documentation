[comment]: # translation:outdated

[comment]: # ({new-f9e8f670})
# userdirectory.update

[comment]: # ({/new-f9e8f670})

[comment]: # ({new-b5ff62ed})
### Description

`object userdirectory.update(object/array userDirectory)`

This method allows to update existing user directories.

::: noteclassic
This method is only available to *Super admin* user type.
:::

[comment]: # ({/new-b5ff62ed})

[comment]: # ({new-d99c84a0})
### Parameters

`(object/array)` User directory properties to be updated.

The `userdirectoryid` property must be defined for each user directory, all other properties are optional.\
Only the passed properties will be updated, all others will remain unchanged.

[comment]: # ({/new-d99c84a0})

[comment]: # ({new-e6134a80})
### Return values

`(object)` Returns an object containing the IDs of the updated user directories
under the `userdirectoryids` property.

[comment]: # ({/new-e6134a80})

[comment]: # ({new-8e924efa})
### Examples

[comment]: # ({/new-8e924efa})

[comment]: # ({new-27b63737})
#### Update bind password for user directory

Set new bind password for a user directory.

Request:

```json
{
    "jsonrpc": "2.0",
    "method": "userdirectory.update",
    "params": {
        "userdirectory": "2",
        "bind_password": "newldappassword"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "userdirectoryids": [
            "2"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-27b63737})

[comment]: # ({new-7e502f9e})
##### Update mappings for user directory

Update provisioning groups mappings and media type mappings for user directory "2".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "userdirectory.update",
    "params": {
        "userdirectoryid": "2",
        "provision_media": [
                {
                    "name": "example.com",
                    "mediatypeid": "1",
                    "attribute": "admin@example.com"
                }
            ],
            "provision_groups": [
                {
                    "name": "Zabbix administrators",
                    "roleid": "2",
                    "user_groups": [
                        {
                            "usrgrpid": "7"
                        },
                        {
                            "usrgrpid": "8"
                        },
                        {
                            "usrgrpid": "11"
                        }
                    ]
                }
            ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "userdirectoryids": [
            "2"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-7e502f9e})

[comment]: # ({new-33fc154f})
### Source

CUserDirectory::update() in *ui/include/classes/api/services/CUserDirectory.php*.

[comment]: # ({/new-33fc154f})
