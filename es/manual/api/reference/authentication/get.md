[comment]: # translation:outdated

[comment]: # ({new-558a988d})
# authentication.get

[comment]: # ({/new-558a988d})

[comment]: # ({new-f493fd10})
### Description

`object authentication.get(object parameters)`

The method allows to retrieve authentication object according to the
given parameters.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-f493fd10})

[comment]: # ({new-24b20f8d})
### Parameters

`(object)` Parameters defining the desired output.

The method supports only one parameter.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|output|query|This parameter being common for all `get` methods described in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters).|

[comment]: # ({/new-24b20f8d})

[comment]: # ({new-6394c247})
### Return values

`(object)` Returns authentication object.

[comment]: # ({/new-6394c247})

[comment]: # ({new-cad6afa3})
### Examples

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "authentication.get",
    "params": {
        "output": "extend"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "authentication_type": "0",
        "http_auth_enabled": "0",
        "http_login_form": "0",
        "http_strip_domains": "",
        "http_case_sensitive": "1",
        "ldap_configured": "0",
        "ldap_host": "",
        "ldap_port": "389",
        "ldap_base_dn": "",
        "ldap_search_attribute": "",
        "ldap_bind_dn": "",
        "ldap_case_sensitive": "1",
        "ldap_bind_password": "",
        "saml_auth_enabled": "0",
        "saml_idp_entityid": "",
        "saml_sso_url": "",
        "saml_slo_url": "",
        "saml_username_attribute": "",
        "saml_sp_entityid": "",
        "saml_nameid_format": "",
        "saml_sign_messages": "0",
        "saml_sign_assertions": "0",
        "saml_sign_authn_requests": "0",
        "saml_sign_logout_requests": "0",
        "saml_sign_logout_responses": "0",
        "saml_encrypt_nameid": "0",
        "saml_encrypt_assertions": "0",
        "saml_case_sensitive": "0",
        "passwd_min_length": "8",
        "passwd_check_rules": "8"
    },
    "id": 1
}
```

[comment]: # ({/new-cad6afa3})

[comment]: # ({new-b8be0c1f})
### Source

CAuthentication::get() in
*ui/include/classes/api/services/CAuthentication.php*.

[comment]: # ({/new-b8be0c1f})
