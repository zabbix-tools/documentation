[comment]: # translation:outdated

[comment]: # ({new-8102730f})
# drule.create

[comment]: # ({/new-8102730f})

[comment]: # ({new-9e7d03a2})
### Description

`object drule.create(object/array discoveryRules)`

This method allows to create new discovery rules.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-9e7d03a2})

[comment]: # ({new-7afdb3b3})
### Parameters

`(object/array)` Discovery rules to create.

Additionally to the [standard discovery rule
properties](object#discovery_rule), the method accepts the following
parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|**dchecks**<br>(required)|array|Discovery [checks](/manual/api/reference/dcheck/object) to create for the discovery rule.|

[comment]: # ({/new-7afdb3b3})

[comment]: # ({new-2eb6d0d5})
### Return values

`(object)` Returns an object containing the IDs of the created
discrovery rules under the `druleids` property. The order of the
returned IDs matches the order of the passed discrovery rules.

[comment]: # ({/new-2eb6d0d5})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-b03b87c1})
#### Create a discovery rule

Create a discovery rule to find machines running the Zabbix agent in the
local network. The rule must use a single Zabbix agent check on port
10050.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "drule.create",
    "params": {
        "name": "Zabbix agent discovery",
        "iprange": "192.168.1.1-255",
        "dchecks": [
            {
                "type": "9",
                "key_": "system.uname",
                "ports": "10050",
                "uniq": "0"
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "druleids": [
            "6"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-b03b87c1})

[comment]: # ({new-54664cce})
### See also

-   [Discovery
    check](/manual/api/reference/dcheck/object#discovery_check)

[comment]: # ({/new-54664cce})

[comment]: # ({new-1333854c})
### Source

CDRule::create() in *ui/include/classes/api/services/CDRule.php*.

[comment]: # ({/new-1333854c})
