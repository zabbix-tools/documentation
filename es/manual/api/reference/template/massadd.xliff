<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="es" datatype="plaintext" original="manual/api/reference/template/massadd.md">
    <body>
      <trans-unit id="8c618db1" xml:space="preserve">
        <source># template.massadd</source>
      </trans-unit>
      <trans-unit id="399b9834" xml:space="preserve">
        <source>### Description

`object template.massadd(object parameters)`

This method allows to simultaneously add multiple related objects to the
given templates.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::</source>
      </trans-unit>
      <trans-unit id="4167b841" xml:space="preserve">
        <source>### Parameters

`(object)` Parameters containing the IDs of the templates to update and
the objects to add to the templates.

The method accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|templates|object/array|Templates to be updated.&lt;br&gt;&lt;br&gt;The templates must have the `templateid` property defined.&lt;br&gt;&lt;br&gt;[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):&lt;br&gt;- *required*|
|groups|object/array|[Template groups](/manual/api/reference/templategroup/object) to add the given templates to.&lt;br&gt;&lt;br&gt;The template groups must have the `groupid` property defined.|
|macros|object/array|[User macros](/manual/api/reference/usermacro/object) to be created for the given templates.|
|templates\_link|object/array|Templates to link to the given templates.&lt;br&gt;&lt;br&gt;The templates must have the `templateid` property defined.|</source>
      </trans-unit>
      <trans-unit id="dcba01c8" xml:space="preserve">
        <source>### Return values

`(object)` Returns an object containing the IDs of the updated templates
under the `templateids` property.</source>
      </trans-unit>
      <trans-unit id="b41637d2" xml:space="preserve">
        <source>### Examples</source>
      </trans-unit>
      <trans-unit id="f8fe44e4" xml:space="preserve">
        <source>#### Link a group to templates

Add template group "2" to two templates.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "template.massadd",
    "params": {
        "templates": [
            {
                "templateid": "10085"
            },
            {
                "templateid": "10086"
            }
        ],
        "groups": [
            {
                "groupid": "2"
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "templateids": [
            "10085",
            "10086"
        ]
    },
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="7dbed7e5" xml:space="preserve">
        <source>#### Link two templates to a template

Link templates "10106" and "10104" to template "10073".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "template.massadd",
    "params": {
        "templates": [
            {
                "templateid": "10073"
            }
        ],
        "templates_link": [
            {
                "templateid": "10106"
            },
            {
                "templateid": "10104"
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "templateids": [
            "10073"
        ]
    },
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="bd8dd370" xml:space="preserve">
        <source>### See also

-   [template.update](update)
-   [Host](/manual/api/reference/host/object#host)
-   [Template group](/manual/api/reference/templategroup/object#template_group)
-   [User macro](/manual/api/reference/usermacro/object#hosttemplate_level_macro)</source>
      </trans-unit>
      <trans-unit id="2f7abcb1" xml:space="preserve">
        <source>### Source

CTemplate::massAdd() in *ui/include/classes/api/services/CTemplate.php*.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
