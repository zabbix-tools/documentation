[comment]: # translation:outdated

[comment]: # ({new-a7242998})
# template.delete

[comment]: # ({/new-a7242998})

[comment]: # ({new-484b4ad2})
### Description

`object template.delete(array templateIds)`

This method allows to delete templates.

Deleting a template will cause deletion of all template entities (items,
triggers, graphs, etc.). To leave template entities with the hosts, but
delete the template itself, first unlink the template from required
hosts using one of these methods:
[template.update](/manual/api/reference/template/update),
[template.massupdate](/manual/api/reference/template/massupdate),
[host.update](/manual/api/reference/host/update),
[host.massupdate](/manual/api/reference/host/massupdate).

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-484b4ad2})

[comment]: # ({new-1fbebaa3})
### Parameters

`(array)` IDs of the templates to delete.

[comment]: # ({/new-1fbebaa3})

[comment]: # ({new-fdd33aa6})
### Return values

`(object)` Returns an object containing the IDs of the deleted templates
under the `templateids` property.

[comment]: # ({/new-fdd33aa6})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-3c8960bb})
#### Deleting multiple templates

Delete two templates.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "template.delete",
    "params": [
        "13",
        "32"
    ],
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "templateids": [
            "13",
            "32"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-3c8960bb})

[comment]: # ({new-bfde186e})
### Source

CTemplate::delete() in *ui/include/classes/api/services/CTemplate.php*.

[comment]: # ({/new-bfde186e})
