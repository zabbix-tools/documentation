[comment]: # translation:outdated

[comment]: # ({new-392125b2})
# User

This class is designed to work with users.

Object references:\

-   [User](/manual/api/reference/user/object#user)

Available methods:\

-   [user.checkauthentication](/manual/api/reference/user/checkauthentication) -
    checking and prolonging user sessions
-   [user.create](/manual/api/reference/user/create) - creating new
    users
-   [user.delete](/manual/api/reference/user/delete) - deleting users
-   [user.get](/manual/api/reference/user/get) - retrieving users
-   [user.login](/manual/api/reference/user/login) - logging in to the
    API
-   [user.logout](/manual/api/reference/user/logout) - logging out of
    the API
-   [user.unblock](/manual/api/reference/user/unblock) - unblocking
    users
-   [user.update](/manual/api/reference/user/update) - updating users

[comment]: # ({/new-392125b2})
