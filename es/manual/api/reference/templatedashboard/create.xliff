<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="es" datatype="plaintext" original="manual/api/reference/templatedashboard/create.md">
    <body>
      <trans-unit id="2aef6e95" xml:space="preserve">
        <source># templatedashboard.create</source>
      </trans-unit>
      <trans-unit id="8072f2f2" xml:space="preserve">
        <source>### Description

`object templatedashboard.create(object/array templateDashboards)`

This method allows to create new template dashboards.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::</source>
      </trans-unit>
      <trans-unit id="1caae149" xml:space="preserve">
        <source>### Parameters

`(object/array)` Template dashboards to create.

Additionally to the [standard template dashboard
properties](object#template_dashboard), the method accepts the following
parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|pages|array|Template dashboard [pages](object#dashboard_page) to be created for the dashboard. Dashboard pages will be ordered in the same order as specified.&lt;br&gt;&lt;br&gt;[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):&lt;br&gt;- *required*|</source>
      </trans-unit>
      <trans-unit id="4bfe9ed0" xml:space="preserve">
        <source>### Return values

`(object)` Returns an object containing the IDs of the created template
dashboards under the `dashboardids` property. The order of the returned
IDs matches the order of the passed template dashboards.</source>
      </trans-unit>
      <trans-unit id="b41637d2" xml:space="preserve">
        <source>### Examples</source>
      </trans-unit>
      <trans-unit id="49684d2f" xml:space="preserve">
        <source>#### Creating a template dashboard

Create a template dashboard named “Graphs” with one Graph widget on a
single dashboard page.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "templatedashboard.create",
    "params": {
        "templateid": "10318",
        "name": "Graphs",
        "pages": [
            {
                "widgets": [
                    {
                        "type": "graph",
                        "x": 0,
                        "y": 0,
                        "width": 12,
                        "height": 5,
                        "view_mode": 0,
                        "fields": [
                            {
                                "type": 6,
                                "name": "graphid",
                                "value": "1123"
                            }
                        ]
                    }
                ]

            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "dashboardids": [
            "32"
        ]
    },
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="1171cc63" xml:space="preserve">
        <source>### See also

-   [Template dashboard page](object#template_dashboard_page)
-   [Template dashboard widget](object#template_dashboard_widget)
-   [Template dashboard widget
    field](object#template_dashboard_widget_field)</source>
      </trans-unit>
      <trans-unit id="78c6e281" xml:space="preserve">
        <source>### Source

CTemplateDashboard::create() in
*ui/include/classes/api/services/CTemplateDashboard.php*.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
