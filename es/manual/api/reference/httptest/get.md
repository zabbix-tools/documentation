[comment]: # translation:outdated

[comment]: # ({new-7f8cd3a3})
# httptest.get

[comment]: # ({/new-7f8cd3a3})

[comment]: # ({new-96cbc36d})
### Description

`integer/array httptest.get(object parameters)`

The method allows to retrieve web scenarios according to the given
parameters.

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-96cbc36d})

[comment]: # ({new-7be2c19f})
### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|groupids|string/array|Return only web scenarios that belong to the given host groups.|
|hostids|string/array|Return only web scenarios that belong to the given hosts.|
|httptestids|string/array|Return only web scenarios with the given IDs.|
|inherited|boolean|If set to `true` return only web scenarios inherited from a template.|
|monitored|boolean|If set to `true` return only enabled web scenarios that belong to monitored hosts.|
|templated|boolean|If set to `true` return only web scenarios that belong to templates.|
|templateids|string/array|Return only web scenarios that belong to the given templates.|
|expandName|flag|Expand macros in the name of the web scenario.|
|expandStepName|flag|Expand macros in the names of scenario steps.|
|evaltype|integer|Rules for tag searching.<br><br>Possible values:<br>0 - (default) And/Or;<br>2 - Or.|
|tags|array of objects|Return only web scenarios with given tags. Exact match by tag and case-sensitive or case-insensitive search by tag value depending on operator value.<br>Format: `[{"tag": "<tag>", "value": "<value>", "operator": "<operator>"}, ...]`.<br>An empty array returns all web scenarios.<br><br>Possible operator types:<br>0 - (default) Like;<br>1 - Equal;<br>2 - Not like;<br>3 - Not equal<br>4 - Exists;<br>5 - Not exists.|
|selectHosts|query|Return the hosts that the web scenario belongs to as an array in the [hosts](/manual/api/reference/host/object) property.|
|selectSteps|query|Return web scenario steps in the [steps](/manual/api/reference/httptest/object#scenario_step) property.<br><br>Supports `count`.|
|selectTags|query|Return the web scenario tags in [tags](/manual/api/reference/httptest/object#web_scenario_tag) property.|
|sortfield|string/array|Sort the result by the given properties.<br><br>Possible values are: `httptestid` and `name`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters).|
|editable|boolean|^|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

[comment]: # ({/new-7be2c19f})

[comment]: # ({new-7223bab1})
### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-c93618d6})
#### Retrieving a web scenario

Retrieve all data about web scenario "4".

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "httptest.get",
    "params": {
        "output": "extend",
        "selectSteps": "extend",
        "httptestids": "9"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "httptestid": "9",
            "name": "Homepage check",
            "nextcheck": "0",
            "delay": "1m",
            "status": "0",
            "variables": [],
            "agent": "Zabbix",
            "authentication": "0",
            "http_user": "",
            "http_password": "",
            "hostid": "10084",
            "templateid": "0",
            "http_proxy": "",
            "retries": "1",
            "ssl_cert_file": "",
            "ssl_key_file": "",
            "ssl_key_password": "",
            "verify_peer": "0",
            "verify_host": "0",
            "headers": [],
            "steps": [
                {
                    "httpstepid": "36",
                    "httptestid": "9",
                    "name": "Homepage",
                    "no": "1",
                    "url": "http://example.com",
                    "timeout": "15s",
                    "posts": "",
                    "required": "",
                    "status_codes": "200",
                    "variables": [  
                        {  
                            "name":"{var}",
                            "value":"12"
                        }
                    ],
                    "follow_redirects": "1",
                    "retrieve_mode": "0",
                    "headers": [],
                    "query_fields": []
                },
                {
                    "httpstepid": "37",
                    "httptestid": "9",
                    "name": "Homepage / About",
                    "no": "2",
                    "url": "http://example.com/about",
                    "timeout": "15s",
                    "posts": "",
                    "required": "",
                    "status_codes": "200",
                    "variables": [],
                    "follow_redirects": "1",
                    "retrieve_mode": "0",
                    "headers": [],
                    "query_fields": []
                }
            ]
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-c93618d6})

[comment]: # ({new-ce685d69})
### See also

-   [Host](/manual/api/reference/host/object#host)
-   [Scenario step](object#scenario_step)

[comment]: # ({/new-ce685d69})

[comment]: # ({new-8ec619ff})
### Source

CHttpTest::get() in *ui/include/classes/api/services/CHttpTest.php*.

[comment]: # ({/new-8ec619ff})
