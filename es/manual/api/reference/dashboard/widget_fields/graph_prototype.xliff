<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="es" datatype="plaintext" original="manual/api/reference/dashboard/widget_fields/graph_prototype.md">
    <body>
      <trans-unit id="2be71f02" xml:space="preserve">
        <source># 10 Graph prototype</source>
      </trans-unit>
      <trans-unit id="4ce2099a" xml:space="preserve">
        <source>### Description

These parameters and the possible property values for the respective dashboard widget field objects allow to configure
the [*Graph prototype*](/manual/web_interface/frontend_sections/dashboards/widgets/graph_prototype) widget in `dashboard.create` and `dashboard.update` methods.</source>
      </trans-unit>
      <trans-unit id="83184d1c" xml:space="preserve">
        <source>### Parameters

The following parameters are supported for the *Graph prototype* widget.

|Parameter|[type](/manual/api/reference/dashboard/object#dashboard-widget-field)|name|value|
|-----|-|-----|-------------------|
|*Refresh interval*|0|rf_rate|0 - No refresh;&lt;br&gt;10 - 10 seconds;&lt;br&gt;30 - 30 seconds;&lt;br&gt;60 - *(default)* 1 minute;&lt;br&gt;120 - 2 minutes;&lt;br&gt;600 - 10 minutes;&lt;br&gt;900 - 15 minutes.|
|*Source*|0|source_type|2 - *(default)* Graph prototype;&lt;br&gt;3 - Simple graph prototype.|
|*Graph prototype*|7|graphid|[Graph prototype](/manual/api/reference/graphprototype/get) ID.&lt;br&gt;&lt;br&gt;[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):&lt;br&gt;- *required* if *Source* is set to "Graph prototype"|
|*Item prototype*|5|itemid|[Item prototype](/manual/api/reference/itemprototype/get) ID.&lt;br&gt;&lt;br&gt;[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):&lt;br&gt;- *required* if *Source* is set to "Simple graph prototype"|
|*Show legend*|0|show_legend|0 - Disabled;&lt;br&gt;1 - *(default)* Enabled.|
|*Enable host selection*|0|dynamic|0 - *(default)* Disabled;&lt;br&gt;1 - Enabled.|
|*Columns*|0|columns|Valid values range from 1-24.&lt;br&gt;&lt;br&gt; Default: 2.|
|*Rows*|0|rows|Valid values range from 1-16.&lt;br&gt;&lt;br&gt; Default: 1.|</source>
      </trans-unit>
      <trans-unit id="df884afa" xml:space="preserve">
        <source>### Examples

The following examples aim to only describe the configuration of the dashboard widget field objects for the *Graph prototype* widget.
For more information on configuring a dashboard, see [`dashboard.create`](/manual/api/reference/dashboard/create).</source>
      </trans-unit>
      <trans-unit id="826a8d29" xml:space="preserve">
        <source>#### Configuring a *Graph prototype* widget

Configure a *Graph prototype* widget that displays a grid of 3 graphs (3 columns, 1 row) created from an item prototype (ID: "42316") by low-level discovery.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "dashboard.create",
    "params": {
        "name": "My dashboard",
        "display_period": 30,
        "auto_start": 1,
        "pages": [
            {
                "widgets": [
                    {
                        "type": "graphprototype",
                        "name": "Graph prototype",
                        "x": 0,
                        "y": 0,
                        "width": 16,
                        "height": 5,
                        "view_mode": 0,
                        "fields": [
                            {
                                "type": 0,
                                "name": "source_type",
                                "value": 3
                            },
                            {
                                "type": 5,
                                "name": "itemid",
                                "value": 42316
                            },
                            {
                                "type": 0,
                                "name": "columns",
                                "value": 3
                            }
                        ]
                    }
                ]
            }
        ],
        "userGroups": [
            {
                "usrgrpid": 7,
                "permission": 2
            }
        ],
        "users": [
            {
                "userid": 1,
                "permission": 3
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "dashboardids": [
            "3"
        ]
    },
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="46917953" xml:space="preserve">
        <source>### See also

-   [Dashboard widget field](/manual/api/reference/dashboard/object#dashboard-widget-field)
-   [`dashboard.create`](/manual/api/reference/dashboard/create)
-   [`dashboard.update`](/manual/api/reference/dashboard/update)</source>
      </trans-unit>
    </body>
  </file>
</xliff>
