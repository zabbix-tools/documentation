[comment]: # translation:outdated

[comment]: # ({new-6d33dfbd})
# 8 Graph

[comment]: # ({/new-6d33dfbd})

[comment]: # ({new-5c2abed1})
### Description

These parameters and the possible property values for the respective dashboard widget field objects allow to configure
the [*Graph*](/manual/web_interface/frontend_sections/dashboards/widgets/graph) widget in `dashboard.create` and `dashboard.update` methods.

[comment]: # ({/new-5c2abed1})

[comment]: # ({new-75d80010})
### Parameters

The following parameters are supported for the *Graph* widget.

|Parameter|[type](/manual/api/reference/dashboard/object#dashboard-widget-field)|name|value|
|-----|-|-----|-------------------|
|*Refresh interval*|0|rf_rate|0 - No refresh;<br>10 - 10 seconds;<br>30 - 30 seconds;<br>60 - *(default)* 1 minute;<br>120 - 2 minutes;<br>600 - 10 minutes;<br>900 - 15 minutes.|

[comment]: # ({/new-75d80010})

[comment]: # ({new-9cf31911})
#### Data set

The following parameters are supported for configuring a *Data set*.

::: noteclassic
The first number in the property name (e.g. ds.hosts.0.0, ds.items.0.0) represents the particular data set,
while the second number, if present, represents the configured host or item.
:::

|Parameter|<|[type](/manual/api/reference/dashboard/object#dashboard-widget-field)|name|value|
|-|--------|--|--------|-------------------------------|
|*Data set type*|<|0|ds.dataset_type.0|0 - Item list;<br>1 - *(default)* Item pattern.|
|Parameters if *Data set type* is set to "Item list"|<|<|<|<|
|<|***Items***<br>(required)|4|ds.itemids.0.0|[Item](/manual/api/reference/item/get) ID.<br><br>Note: To configure multiple items, create a dashboard widget field object for each item.|
|^|***Color***<br>(required)|1|ds.color.0.0|Hexadecimal color code (e.g. `FF0000`).|
|Parameters if *Data set type* is set to "Item pattern"|<|<|<|<|
|<|***Host pattern***<br>(required)|1|ds.hosts.0.0|[Host](/manual/api/reference/host/get) name or pattern (e.g. `Zabbix*`).|
|^|***Item pattern***<br>(required)|1|ds.items.0.0|[Item](/manual/api/reference/item/get) name or pattern (e.g. `*: Number of processed *values per second`).|
|^|*Color*|1|ds.color.0|Hexadecimal color code (e.g. `FF0000`).<br><br>Default: `FF465C`.|
|*Draw*|<|0|ds.type.0|0 - *(default)* Line;<br>1 - Points;<br>2 - Staircase;<br>3 - Bar.|
|*Stacked*|<|0|ds.stacked.0|0 - *(default)* Disabled;<br>1 - Enabled.<br><br>Parameter *Stacked* not available if *Draw* is set to "Points".|
|*Width*|<|0|ds.width.0|Valid values range from 1-10.<br><br>Default: 1.<br><br>Parameter *Width* not available if *Draw* is set to "Points" or "Bar".|
|*Point size*|<|0|ds.pointsize.0|Valid values range from 1-10.<br><br>Default: 3.<br><br>Parameter *Point size* not available if *Draw* is set to "Line", "Staircase" or "Bar".|
|*Transparency*|<|0|ds.transparency.0|Valid values range from 1-10.<br><br>Default: 5.|
|*Fill*|<|0|ds.fill.0|Valid values range from 1-10.<br><br>Default: 3.<br><br>Parameter *Fill* not available if *Draw* is set to "Points" or "Bar".|
|*Missing data*|<|0|ds.missingdatafunc.0|0 - *(default)* None;<br>1 - Connected;<br>2 - Treat as 0;<br>3 - Last known.<br><br>Parameter *Missing data* not available if *Draw* is set to "Points" or "Bar".|
|*Y-axis*|<|0|ds.axisy.0|0 - *(default)* Left;<br>1 - Right.|
|*Time shift*|<|1|ds.timeshift.0|Valid time string (e.g. `3600`, `1h`, etc.).<br>You may use [time suffixes](/manual/appendix/suffixes#time-suffixes). Negative values are also allowed.<br><br>Default: `""` (empty).|
|*Aggregation function*|<|0|ds.aggregate_function.0|0 - *(default)* none;<br>1 - min;<br>2 - max;<br>3 - avg;<br>4 - count;<br>5 - sum;<br>6 - first;<br>7 - last.|
|*Aggregation interval*|<|1|ds.aggregate_interval.0|Valid time string (e.g. `3600`, `1h`, etc.).<br>You may use [time suffixes](/manual/appendix/suffixes#time-suffixes).<br><br>Default: `1h`.|
|*Aggregate*|<|0|ds.aggregate_grouping.0|0 - *(default)* Each item;<br>1 - Data set.<br><br>Parameter *Aggregate* not available if *Aggregation function* is set to "none".|
|*Approximation*|<|0|ds.approximation.0|1 - min;<br>2 - *(default)* avg;<br>4 - max;<br>7 - all.|
|*Data set label*|<|1|ds.data_set_label.0|Any string value.<br><br>Default: `""` (empty).|

[comment]: # ({/new-9cf31911})

[comment]: # ({new-1e2d4083})
#### Display options

The following parameters are supported for configuring *Display options*.

|Parameter|<|[type](/manual/api/reference/dashboard/object#dashboard-widget-field)|name|value|
|-|--------|--|--------|-------------------------------|
|*History data selection*|<|0|source|0 - *(default)* Auto;<br>1 - History;<br>2 - Trends.|
|*Simple triggers*|<|0|simple_triggers|0 - *(default)* Disabled;<br>1 - Enabled.|
|*Working time*|<|0|working_time|0 - *(default)* Disabled;<br>1 - Enabled.|
|*Percentile line (left)* (parameter available if *Y-axis* (in *Data set* configuration) is set to "Left")|<|<|<|<|
|<|*Status*|0|percentile_left|0 - *(default)* Disabled;<br>1 - Enabled.|
|^|*Value*|0|percentile_left_value|Valid values range from 1-100.|
|*Percentile line (right)* (parameter available if *Y-axis* (in *Data set* configuration) is set to "Right")|<|<|<|<|
|<|*Status*|0|percentile_right|0 - *(default)* Disabled;<br>1 - Enabled.|
|^|*Value*|0|percentile_right_value|Valid values range from 1-100.|

[comment]: # ({/new-1e2d4083})

[comment]: # ({new-2fc4e743})
#### Time period

The following parameters are supported for configuring *Time period*.

|Parameter|[type](/manual/api/reference/dashboard/object#dashboard-widget-field)|name|value|
|-----|-|-----|-------------------|
|*Set custom time period*|0|graph_time|0 - *(default)* Disabled;<br>1 - Enabled.|
|*From*|1|time_from|Valid time string in format `YYYY-MM-DD hh:mm:ss`.<br>[Relative time period](/manual/config/visualization/graphs/simple#time-period-selector) values (`now`, `now/d`, `now/w-1w`, etc.) are also supported.<br><br>Default: `now-1h`.|
|*To*|1|time_to|Valid time string value in format `YYYY-MM-DD hh:mm:ss`.<br>[Relative time period](/manual/config/visualization/graphs/simple#time-period-selector) values (`now`, `now/d`, `now/w-1w`, etc.) are also supported.<br><br>Default: `now`.|

[comment]: # ({/new-2fc4e743})

[comment]: # ({new-bca60ab8})
#### Axes

The following parameters are supported for configuring *Axes*.

|Parameter|[type](/manual/api/reference/dashboard/object#dashboard-widget-field)|name|value|
|-----|-|-----|-------------------|
|*Left Y*|0|lefty|0 - Disabled;<br>1 - *(default)* Enabled.<br><br>Parameter available if *Y-axis* (in *Data set* configuration) is set to "Left".|
|*Right Y*|0|righty|0 - *(default)* Disabled;<br>1 - Enabled.<br><br>Parameter available if *Y-axis* (in *Data set* configuration) is set to "Right".|
|*Min*|1|lefty_min|Any numeric value.<br><br>Default: `""` (empty).|
|^|^|righty_min|^|
|*Max*|1|lefty_max|Any numeric value.<br><br>Default: `""` (empty).|
|^|^|righty_max|^|
|*Units* (type)|0|lefty_units|0 - *(default)* Auto;<br>1 - Static.|
|^|^|righty_units|^|
|*Units* (value)|1|lefty_static_units|Any string value.<br><br>Default: `""` (empty).|
|^|^|righty_static_units|^|
|*X-Axis*|0|xaxis|0 - Disabled;<br>1 - *(default)* Enabled.|

[comment]: # ({/new-bca60ab8})

[comment]: # ({new-09de7a04})
#### Legend

The following parameters are supported for configuring *Legend*.

|Parameter|[type](/manual/api/reference/dashboard/object#dashboard-widget-field)|name|value|
|-----|-|-----|-------------------|
|*Show legend*|0|legend|0 - Disabled;<br>1 - *(default)* Enabled.|
|*Display min/max/avg*|0|legend_statistic|0 - *(default)* Disabled;<br>1 - Enabled.|
|*Number of rows*|0|legend_lines|Valid values range from 1-10.<br><br>Default: 1.|
|*Number of columns*|0|legend_columns|Valid values range from 1-4.<br><br>Default: 4.|

[comment]: # ({/new-09de7a04})

[comment]: # ({new-82002e68})
#### Problems

The following parameters are supported for configuring *Problems*.

|Parameter|<|[type](/manual/api/reference/dashboard/object#dashboard-widget-field)|name|value|
|-|--------|--|--------|-------------------------------|
|*Show problems*|<|0|show_problems|0 - *(default)* Disabled;<br>1 - Enabled.|
|*Selected items only*|<|0|graph_item_problems|0 - Disabled;<br>1 - *(default)* Enabled.|
|*Problem hosts*|<|1|problemhosts.0|[Host](/manual/api/reference/host/get) name.<br><br>Note: The number in the property name references the configured host.<br>To configure multiple hosts, create a dashboard widget field object for each host.|
|*Severity*|<|0|severities|0 - Not classified;<br>1 - Information;<br>2 - Warning;<br>3 - Average;<br>4 - High;<br>5 - Disaster.<br><br>Default: 1, 2, 3, 4, 5 (all enabled).<br><br>Note: To configure multiple values, create a dashboard widget field object for each value.|
|*Problem*|<|1|problem_name|Problem [event name](/manual/config/triggers/trigger#configuration) (case insensitive, full name or part of it).|
|*Tags* (the number in the property name (e.g. tags.tag.0) references tag order in the tag evaluation list)|<|<|<|<|
|<|*Evaluation type*|0|evaltype|0 - *(default)* And/Or;<br>2 - Or.|
|^|*Tag name*|1|tags.tag.0|Any string value.<br><br>Parameter *Tag name* required if configuring *Tags*.|
|^|*Operator*|0|tags.operator.0|0 - Contains;<br>1 - Equals;<br>2 - Does not contain;<br>3 - Does not equal;<br>4 - Exists;<br>5 - Does not exist.<br><br>Parameter *Operator* required if configuring *Tags*.|
|^|*Tag value*|1|tags.value.0|Any string value.<br><br>Parameter *Tag value* required if configuring *Tags*.|

[comment]: # ({/new-82002e68})

[comment]: # ({new-4097ae32})
#### Overrides

The following parameters are supported for configuring *Overrides*.

::: noteclassic
The first number in the property name (e.g. or.hosts.0.0, or.items.0.0) represents the particular data set,
while the second number, if present, represents the configured host or item.
:::

|Parameter|[type](/manual/api/reference/dashboard/object#dashboard-widget-field)|name|value|
|-----|-|-----|-------------------|
|***Host pattern***<br>(required)|1|or.hosts.0.0|[Host](/manual/api/reference/host/get) name or pattern (e.g. `Zabbix*`).|
|***Item pattern***<br>(required)|1|or.items.0.0|[Item](/manual/api/reference/item/get) name or pattern (e.g. `*: Number of processed *values per second`).|
|*Base color*|1|or.color.0|Hexadecimal color code (e.g. `FF0000`).|
|*Width*|0|or.width.0|Valid values range from 1-10.|
|*Draw*|0|or.type.0|0 - Line;<br>1 - Points;<br>2 - Staircase;<br>3 - Bar.|
|*Transparency*|0|or.transparency.0|Valid values range from 1-10.|
|*Fill*|0|or.fill.0|Valid values range from 1-10.|
|*Point size*|0|or.pointsize.0|Valid values range from 1-10.|
|*Missing data*|0|or.missingdatafunc.0|0 - None;<br>1 - Connected;<br>2 - Treat as 0;<br>3 - Last known.|
|*Y-axis*|0|or.axisy.0|0 - Left;<br>1 - Right.|
|*Time shift*|1|or.timeshift.0|Valid time string (e.g. `3600`, `1h`, etc.).<br>You may use [time suffixes](/manual/appendix/suffixes#time-suffixes). Negative values are allowed.|

[comment]: # ({/new-4097ae32})

[comment]: # ({new-5887cb51})
### Examples

The following examples aim to only describe the configuration of the dashboard widget field objects for the *Graph* widget.
For more information on configuring a dashboard, see [`dashboard.create`](/manual/api/reference/dashboard/create).

[comment]: # ({/new-5887cb51})

[comment]: # ({new-102e83b4})
#### Configuring a *Graph* widget

Configure a *Graph* widget in the following way:

-   2 data sets for a total of 9 items on 1 host.
-   The first data set is of type "Item list" and consists of 3 items that are represented by lines with a different color, but the same width, transparency, and fill.
-   The second data set is of type "Item pattern", consists of 6 items, has a configured aggregation, and is represented by a line with a custom color, width, transparency, and fill.
-   The second data set also has a custom data set label.
-   Data in the graph are displayed for a time period of the last 3 hours.
-   Problems in the graph are displayed only for the configured items.
-   Graph has two Y axes of which the right Y axis displays values only for the second data set.
-   Graph legend displays configured items in 4 rows, as well as minimum, maximum and average values of the data sets.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "dashboard.create",
    "params": {
        "name": "My dashboard",
        "display_period": 30,
        "auto_start": 1,
        "pages": [
            {
                "widgets": [
                    {
                        "type": "svggraph",
                        "name": "Graph",
                        "x": 0,
                        "y": 0,
                        "width": 12,
                        "height": 5,
                        "view_mode": 0,
                        "fields": [
                            {
                                "type": 0,
                                "name": "ds.dataset_type.0",
                                "value": 0
                            },
                            {
                                "type": 4,
                                "name": "ds.itemids.0.1",
                                "value": 23264
                            },
                            {
                                "type": 1,
                                "name": "ds.color.0.1",
                                "value": "FF0000"
                            },
                            {
                                "type": 4,
                                "name": "ds.itemids.0.2",
                                "value": 23269
                            },
                            {
                                "type": 1,
                                "name": "ds.color.0.2",
                                "value": "BF00FF"
                            },
                            {
                                "type": 4,
                                "name": "ds.itemids.0.3",
                                "value": 23257
                            },
                            {
                                "type": 1,
                                "name": "ds.color.0.3",
                                "value": "0040FF"
                            },
                            {
                                "type": 0,
                                "name": "ds.width.0",
                                "value": 3
                            },
                            {
                                "type": 0,
                                "name": "ds.transparency.0",
                                "value": 3
                            },
                            {
                                "type": 0,
                                "name": "ds.fill.0",
                                "value": 1
                            },
                            {
                                "type": 1,
                                "name": "ds.hosts.1.0",
                                "value": "Zabbix server"
                            },
                            {
                                "type": 1,
                                "name": "ds.items.1.0",
                                "value": "*: Number of processed *values per second"
                            },
                            {
                                "type": 1,
                                "name": "ds.color.1",
                                "value": "000000"
                            },
                            {
                                "type": 0,
                                "name": "ds.transparency.1",
                                "value": 0
                            },
                            {
                                "type": 0,
                                "name": "ds.fill.1",
                                "value": 0
                            },
                            {
                                "type": 0,
                                "name": "ds.axisy.1",
                                "value": 1
                            },
                            {
                                "type": 0,
                                "name": "ds.aggregate_function.1",
                                "value": 3
                            },
                            {
                                "type": 1,
                                "name": "ds.aggregate_interval.1",
                                "value": "1m"
                            },
                            {
                                "type": 0,
                                "name": "ds.aggregate_grouping.1",
                                "value": 1
                            },
                            {
                                "type": 1,
                                "name": "ds.data_set_label.1",
                                "value": "Number of processed values per second"
                            },
                            {
                                "type": 0,
                                "name": "graph_time",
                                "value": 1
                            },
                            {
                                "type": 1,
                                "name": "time_from",
                                "value": "now-3h"
                            },
                            {
                                "type": 0,
                                "name": "legend_statistic",
                                "value": 1
                            },
                            {
                                "type": 0,
                                "name": "legend_lines",
                                "value": 4
                            },
                            {
                                "type": 0,
                                "name": "show_problems",
                                "value": 1
                            }
                        ]
                    }
                ]
            }
        ],
        "userGroups": [
            {
                "usrgrpid": 7,
                "permission": 2
            }
        ],
        "users": [
            {
                "userid": 1,
                "permission": 3
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "dashboardids": [
            "3"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-102e83b4})

[comment]: # ({new-3c27dd53})
### See also

-   [Dashboard widget field](/manual/api/reference/dashboard/object#dashboard-widget-field)
-   [`dashboard.create`](/manual/api/reference/dashboard/create)
-   [`dashboard.update`](/manual/api/reference/dashboard/update)

[comment]: # ({/new-3c27dd53})
