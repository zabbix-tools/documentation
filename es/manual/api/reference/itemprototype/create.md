[comment]: # translation:outdated

[comment]: # ({new-450c25c2})
# itemprototype.create

[comment]: # ({/new-450c25c2})

[comment]: # ({new-0ad3a6f6})
### Description

`object itemprototype.create(object/array itemPrototypes)`

This method allows to create new item prototypes.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-0ad3a6f6})

[comment]: # ({new-56c4ce2f})
### Parameters

`(object/array)` Item prototype to create.

Additionally to the [standard item prototype
properties](object#item_prototype), the method accepts the following
parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|**ruleid**<br>(required)|string|ID of the LLD rule that the item belongs to.|
|preprocessing|array|Item prototype [preprocessing](/manual/api/reference/itemprototype/object#item_prototype_preprocessing) options.|
|tags|array|Item prototype [tags](/manual/api/reference/itemprototype/object#item_prototype_tag).|

[comment]: # ({/new-56c4ce2f})

[comment]: # ({new-4682a6ee})
### Return values

`(object)` Returns an object containing the IDs of the created item
prototypes under the `itemids` property. The order of the returned IDs
matches the order of the passed item prototypes.

[comment]: # ({/new-4682a6ee})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-9956fee4})
#### Creating an item prototype

Create an item prototype to monitor free disc space on a discovered file
system. Discovered items should be numeric Zabbix agent items updated
every 30 seconds.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "itemprototype.create",
    "params": {
        "name": "Free disk space on {#FSNAME}",
        "key_": "vfs.fs.size[{#FSNAME},free]",
        "hostid": "10197",
        "ruleid": "27665",
        "type": 0,
        "value_type": 3,
        "interfaceid": "112",
        "delay": "30s"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "27666"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-9956fee4})

[comment]: # ({new-d22f43ae})
#### Creating an item prototype with preprocessing

Create an item using change per second and a custom multiplier as a
second step.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "itemprototype.create",
    "params": {
        "name": "Incoming network traffic on {#IFNAME}",
        "key_": "net.if.in[{#IFNAME}]",
        "hostid": "10001",
        "ruleid": "27665",
        "type": 0,
        "value_type": 3,
        "delay": "60s",
        "units": "bps",
        "interfaceid": "1155",
        "preprocessing": [
            {
                "type": "10",
                "params": "",
                "error_handler": "0",
                "error_handler_params": ""
            },
            {
                "type": "1",
                "params": "8",
                "error_handler": "2",
                "error_handler_params": "10"
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "44211"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-d22f43ae})

[comment]: # ({new-76dad6d6})
#### Creating dependent item prototype

Create Dependent item prototype for Master item prototype with ID 44211.
Only dependencies on same host (template/discovery rule) are allowed,
therefore Master and Dependent item should have same hostid and ruleid.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "itemprototype.create",
    "params": {
      "hostid": "10001",
      "ruleid": "27665",
      "name": "Dependent test item prototype",
      "key_": "dependent.prototype",
      "type": "18",
      "master_itemid": "44211",
      "value_type": "3"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "44212"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-76dad6d6})

[comment]: # ({new-94e7709f})
#### Create HTTP agent item prototype

Create item prototype with URL using user macro, query fields and custom
headers.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "itemprototype.create",
    "params": {
        "type": "19",
        "hostid": "10254",
        "ruleid":"28256",
        "interfaceid":"2",
        "name": "api item prototype example",
        "key_": "api_http_item",
        "value_type": "3",
        "url": "{$URL_PROTOTYPE}",
        "query_fields": [
            {
                "min": "10"
            },
            {
                "max": "100"
            }
        ],
        "headers": {
            "X-Source": "api"
        },
        "delay":"35"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "28305"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-94e7709f})

[comment]: # ({new-9e8477fc})
#### Create script item prototype

Create a simple data collection using a script item prototype.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "itemprototype.create",
    "params": {
        "name": "Script example",
        "key_": "custom.script.itemprototype",
        "hostid": "12345",
        "type": 21,
        "value_type": 4,
        "params": "var request = new CurlHttpRequest();\nreturn request.Post(\"https://postman-echo.com/post\", JSON.parse(value));",
        "parameters": [{
            "name": "host",
            "value": "{HOST.CONN}"
        }],
        "timeout": "6s",
        "delay": "30s"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 2
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "23865"
        ]
    },
    "id": 3
}
```

[comment]: # ({/new-9e8477fc})

[comment]: # ({new-4f193e2e})
### Source

CItemPrototype::create() in
*ui/include/classes/api/services/CItemPrototype.php*.

[comment]: # ({/new-4f193e2e})
