[comment]: # translation:outdated

[comment]: # ({new-2a0a5084})
# host.massremove

[comment]: # ({/new-2a0a5084})

[comment]: # ({new-9c943195})
### Description

`object host.massremove(object parameters)`

This method allows to remove related objects from multiple hosts.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-9c943195})

[comment]: # ({new-689d39a3})
### Parameters

`(object)` Parameters containing the IDs of the hosts to update and the
objects that should be removed.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|**hostids**<br>(required)|string/array|IDs of the hosts to be updated.|
|groupids|string/array|Host groups to remove the given hosts from.|
|interfaces|object/array|Host interfaces to remove from the given hosts.<br><br>The host interface object must have the `ip`, `dns` and `port` properties defined.|
|macros|string/array|User macros to delete from the given hosts.|
|templateids|string/array|Templates to unlink from the given hosts.|
|templateids\_clear|string/array|Templates to unlink and clear from the given hosts.|

[comment]: # ({/new-689d39a3})

[comment]: # ({new-d166b99b})
### Return values

`(object)` Returns an object containing the IDs of the updated hosts
under the `hostids` property.

[comment]: # ({/new-d166b99b})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-8c635a7b})
#### Unlinking templates

Unlink a template from two hosts and delete all of the templated
entities.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.massremove",
    "params": {
        "hostids": ["69665", "69666"],
        "templateids_clear": "325"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "hostids": [
            "69665",
            "69666"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-8c635a7b})

[comment]: # ({new-abd9a8d3})
### See also

-   [host.update](update)
-   [User
    macro](/manual/api/reference/usermacro/object#hosttemplate_level_macro)
-   [Host
    interface](/manual/api/reference/hostinterface/object#host_interface)

[comment]: # ({/new-abd9a8d3})

[comment]: # ({new-bda21fe6})
### Source

CHost::massRemove() in *ui/include/classes/api/services/CHost.php*.

[comment]: # ({/new-bda21fe6})
