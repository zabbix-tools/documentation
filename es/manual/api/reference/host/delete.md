[comment]: # translation:outdated

[comment]: # ({new-dd2b9519})
# host.delete

[comment]: # ({/new-dd2b9519})

[comment]: # ({new-8f1cd3fa})
### Description

`object host.delete(array hosts)`

This method allows to delete hosts.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-8f1cd3fa})

[comment]: # ({new-cede3d95})
### Parameters

`(array)` IDs of hosts to delete.

[comment]: # ({/new-cede3d95})

[comment]: # ({new-54b4a855})
### Return values

`(object)` Returns an object containing the IDs of the deleted hosts
under the `hostids` property.

[comment]: # ({/new-54b4a855})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-ef767d02})
#### Deleting multiple hosts

Delete two hosts.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.delete",
    "params": [
        "13",
        "32"
    ],
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "hostids": [
            "13",
            "32"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-ef767d02})

[comment]: # ({new-9d5f3989})
### Source

CHost::delete() in *ui/include/classes/api/services/CHost.php*.

[comment]: # ({/new-9d5f3989})
