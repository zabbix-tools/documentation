[comment]: # translation:outdated

[comment]: # ({new-f9e8f670})
# templategroup.massremove

[comment]: # ({/new-f9e8f670})

[comment]: # ({new-b5ff62ed})
### Description

`object templategroup.massremove(object parameters)`

This method allows to remove related objects from multiple template groups.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-b5ff62ed})

[comment]: # ({new-6102edd8})
### Parameters

`(object)` Parameters containing the IDs of the template groups to update
and the objects that should be removed.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|**groupids**<br>(required)|string/array|IDs of the template groups to be updated.|
|templateids|string/array|Templates to remove from all template groups.|

[comment]: # ({/new-6102edd8})

[comment]: # ({new-2acda80b})
### Return values

`(object)` Returns an object containing the IDs of the updated template
groups under the `groupids` property.

[comment]: # ({/new-2acda80b})

[comment]: # ({new-6044f589})
### Examples

[comment]: # ({/new-6044f589})

[comment]: # ({new-2f48a174})
#### Removing templates from template groups

Remove two templates from the given template groups.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "hostgroup.massremove",
    "params": {
        "groupids": [
            "5",
            "6"
        ],
        "hostids": [
            "30050",
            "30001"
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "groupids": [
            "5",
            "6"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-2f48a174})

[comment]: # ({new-1d275edf})
### Source

CTemplateGroup::massRemove() in
*ui/include/classes/api/services/CTemplateGroup.php*.

[comment]: # ({/new-1d275edf})
