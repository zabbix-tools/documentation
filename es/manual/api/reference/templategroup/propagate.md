[comment]: # translation:outdated

[comment]: # ({new-f9e8f670})
# templategroup.propagate


[comment]: # ({/new-f9e8f670})



[comment]: # ({new-b5ff62ed})
### Description

`object templategroup.propagate(object parameters)`

This method allows to apply permissions to all template groups' subgroups.

::: noteclassic
This method is only available to *Super admin* user types. 
Permissions to call the method can be revoked in user role settings. 
See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::


[comment]: # ({/new-b5ff62ed})



[comment]: # ({new-6102edd8})
### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

| Parameter            | [Type](/manual/api/reference_commentary#data_types) | Description                                                                             |
|----------------------|---------------------------------------------------|-----------------------------------------------------------------------------------------|
|**groups**<br>(required)|object/array|Template groups to propagate.<br><br>The template groups must have the `groupid` property defined. |
|**permissions**<br>(required)|boolean|Set `true` if need to propagate permissions.|


[comment]: # ({/new-6102edd8})



[comment]: # ({new-fac1a1ce})
### Return values

`(object)` Returns an object containing the IDs of the propagated template
groups under the `groupids` property.


[comment]: # ({/new-fac1a1ce})



[comment]: # ({new-0ed127f4})
### Examples


[comment]: # ({/new-0ed127f4})



[comment]: # ({new-58284f05})
#### Propagating template group permissions to its subgroups.

Propagate template group permissions to its subgroups.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "templategroup.propagate",
    "params": {
        "groups": [
            {
                "groupid": "15"
            }
        ],
        "permissions": true
    },
    "auth": "f223adf833b2bf2ff38574a67bba6372",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "groupids": [
            "15",
        ]
    },
    "id": 1
}
```


[comment]: # ({/new-58284f05})



[comment]: # ({new-de4769f6})
### See also

-   [templategroup.update](update)
-   [templategroup.massadd](massadd)
-   [Template](/manual/api/reference/template/object#template)


[comment]: # ({/new-de4769f6})



[comment]: # ({new-870c4596})
### Source

CTemplateGroup::propagate() in
*ui/include/classes/api/services/CTemplateGroup.php*.


[comment]: # ({/new-870c4596})
