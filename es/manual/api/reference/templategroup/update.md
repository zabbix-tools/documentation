[comment]: # translation:outdated

[comment]: # ({new-f9e8f670})
# templategroup.update

[comment]: # ({/new-f9e8f670})

[comment]: # ({new-b5ff62ed})
### Description

`object templategroup.update(object/array templateGroups)`

This method allows to update existing template groups.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-b5ff62ed})

[comment]: # ({new-6102edd8})
### Parameters

`(object/array)` [Template group properties](object#template_group) to be
updated.

The `groupid` property must be defined for each template group, all other
properties are optional. Only the given properties will be updated, all
others will remain unchanged.

[comment]: # ({/new-6102edd8})

[comment]: # ({new-1275df2c})
### Return values

`(object)` Returns an object containing the IDs of the updated template
groups under the `groupids` property.

[comment]: # ({/new-1275df2c})

[comment]: # ({new-9341f38c})
### Examples

[comment]: # ({/new-9341f38c})

[comment]: # ({new-8889f8e9})
#### Renaming a template group

Rename a template group to "Templates/Databases"

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "templtegroup.update",
    "params": {
        "groupid": "7",
        "name": "Templates/Databases"
    },
    "auth": "700ca65537074ec963db7efabda78259",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "groupids": [
            "7"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-8889f8e9})

[comment]: # ({new-931c9c76})
### Source

CTemplateGroup::update() in
*ui/include/classes/api/services/CTemplateGroup.php*.

[comment]: # ({/new-931c9c76})
