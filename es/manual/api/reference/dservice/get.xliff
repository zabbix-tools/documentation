<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="es" datatype="plaintext" original="manual/api/reference/dservice/get.md">
    <body>
      <trans-unit id="30b4a228" xml:space="preserve">
        <source># dservice.get</source>
      </trans-unit>
      <trans-unit id="84c200be" xml:space="preserve">
        <source>### Description

`integer/array dservice.get(object parameters)`

The method allows to retrieve discovered services according to the given
parameters.

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::</source>
      </trans-unit>
      <trans-unit id="cdd07a5d" xml:space="preserve">
        <source>### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|dserviceids|string/array|Return only discovered services with the given IDs.|
|dhostids|string/array|Return only discovered services that belong to the given discovered hosts.|
|dcheckids|string/array|Return only discovered services that have been detected by the given discovery checks.|
|druleids|string/array|Return only discovered services that have been detected by the given discovery rules.|
|selectDRules|query|Return a [`drules`](/manual/api/reference/drule/object) property with an array of the discovery rules that detected the service.|
|selectDHosts|query|Return a [`dhosts`](/manual/api/reference/dhost/object) property with an array the discovered hosts that the service belongs to.|
|selectHosts|query|Return a [`hosts`](/manual/api/reference/host/object) property with the hosts with the same IP address and proxy as the service.&lt;br&gt;&lt;br&gt;Supports `count`.|
|limitSelects|integer|Limits the number of records returned by subselects.&lt;br&gt;&lt;br&gt;Applies to the following subselects:&lt;br&gt;`selectHosts` - result will be sorted by `hostid`.|
|sortfield|string/array|Sort the result by the given properties.&lt;br&gt;&lt;br&gt;Possible values: `dserviceid`, `dhostid`, `ip`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters).|
|editable|boolean|^|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|</source>
      </trans-unit>
      <trans-unit id="7223bab1" xml:space="preserve">
        <source>### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.</source>
      </trans-unit>
      <trans-unit id="b41637d2" xml:space="preserve">
        <source>### Examples</source>
      </trans-unit>
      <trans-unit id="09e9b2a3" xml:space="preserve">
        <source>#### Retrieve services discovered on a host

Retrieve all discovered services detected on discovered host "11".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "dservice.get",
    "params": {
        "output": "extend",
        "dhostids": "11"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": [
        {
            "dserviceid": "12",
            "dhostid": "11",
            "value": "",
            "port": "80",
            "status": "1",
            "lastup": "0",
            "lastdown": "1348650607",
            "dcheckid": "5",
            "ip": "192.168.1.134",
            "dns": "john.local"
        },
        {
            "dserviceid": "13",
            "dhostid": "11",
            "value": "",
            "port": "21",
            "status": "1",
            "lastup": "0",
            "lastdown": "1348650610",
            "dcheckid": "6",
            "ip": "192.168.1.134",
            "dns": "john.local"
        }
    ],
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="2647a177" xml:space="preserve">
        <source>### See also

-   [Discovered
    host](/manual/api/reference/dhost/object#discovered_host)
-   [Discovery
    check](/manual/api/reference/dcheck/object#discovery_check)
-   [Host](/manual/api/reference/host/object#host)</source>
      </trans-unit>
      <trans-unit id="76ef9e21" xml:space="preserve">
        <source>### Source

CDService::get() in *ui/include/classes/api/services/CDService.php*.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
