[comment]: # translation:outdated

[comment]: # ({new-b75e4c49})
# regexp.get

[comment]: # ({/new-b75e4c49})

[comment]: # ({new-15aa8348})
### Description

`integer/array regexp.get(object parameters)`

The method allows to retrieve global regular expressions according to
the given parameters.

::: noteclassic
This method is available only to *Super Admin*. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-15aa8348})

[comment]: # ({new-9693dfa8})
### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|regexpids|string/array|Return only regular expressions with the given IDs.|
|selectExpressions|query|Return a [expressions](/manual/api/reference/regexp/object#expressions_object) property.|
|sortfield|string/array|Sort the result by the given properties.<br><br>Possible values are: `regexpid` and `name`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters).|
|editable|boolean|^|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

[comment]: # ({/new-9693dfa8})

[comment]: # ({new-7223bab1})
### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-196e1642})
#### Retrieving global regular expressions.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "regexp.get",
    "params": {
        "output": ["regexpid", "name"],
        "selectExpressions": ["expression", "expression_type"],
        "regexpids": [1, 2],
        "preservekeys": true
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
  "jsonrpc": "2.0",
  "result": {
    "1": {
      "regexpid": "1",
      "name": "File systems for discovery",
      "expressions": [
        {
          "expression": "^(btrfs|ext2|ext3|ext4|reiser|xfs|ffs|ufs|jfs|jfs2|vxfs|hfs|apfs|refs|ntfs|fat32|zfs)$",
          "expression_type": "3"
        }
      ]
    },
    "2": {
      "regexpid": "2",
      "name": "Network interfaces for discovery",
      "expressions": [
        {
          "expression": "^Software Loopback Interface",
          "expression_type": "4"
        },
        {
          "expression": "^(In)?[Ll]oop[Bb]ack[0-9._]*$",
          "expression_type": "4"
        },
        {
          "expression": "^NULL[0-9.]*$",
          "expression_type": "4"
        },
        {
          "expression": "^[Ll]o[0-9.]*$",
          "expression_type": "4"
        },
        {
          "expression": "^[Ss]ystem$",
          "expression_type": "4"
        },
        {
          "expression": "^Nu[0-9.]*$",
          "expression_type": "4"
        }
      ]
    }
  },
  "id": 1
}
```

[comment]: # ({/new-196e1642})

[comment]: # ({new-71855296})
### Source

CRegexp::get() in *ui/include/classes/api/services/CRegexp.php*.

[comment]: # ({/new-71855296})
