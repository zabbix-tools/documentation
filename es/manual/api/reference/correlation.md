[comment]: # translation:outdated

[comment]: # ({new-b1a4e412})
# Correlation

This class is designed to work with correlations.

Object references:\

-   [Correlation](/manual/api/reference/correlation/object#correlation)

Available methods:\

-   [correlation.create](/manual/api/reference/correlation/create) -
    creating new correlations
-   [correlation.delete](/manual/api/reference/correlation/delete) -
    deleting correlations
-   [correlation.get](/manual/api/reference/correlation/get) -
    retrieving correlations
-   [correlation.update](/manual/api/reference/correlation/update) -
    updating correlations

[comment]: # ({/new-b1a4e412})
