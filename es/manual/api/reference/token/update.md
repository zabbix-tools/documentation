[comment]: # translation:outdated

[comment]: # ({new-7063b41c})
# token.update

[comment]: # ({/new-7063b41c})

[comment]: # ({new-f2679bab})
### Description

`object token.update(object/array tokens)`

This method allows to update existing tokens.

::: noteclassic
Only *Super admin* user type is allowed to manage tokens for
other users.
:::

[comment]: # ({/new-f2679bab})

[comment]: # ({new-416a8426})
### Parameters

`(object/array)` Token properties to be updated.

The `tokenid` property must be defined for each token, all other
properties are optional. Only the passed properties will be updated, all
others will remain unchanged.

The method accepts tokens with the [standard token properties](object).

[comment]: # ({/new-416a8426})

[comment]: # ({new-d0adc398})
### Return values

`(object)` Returns an object containing the IDs of the updated tokens
under the `tokenids` property.

[comment]: # ({/new-d0adc398})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-4f041664})
#### Rename token

Remove expiry date from token.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "token.update",
    "params": {
        "tokenid": "2",
        "expires_at": "0"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "tokenids": [
            "2"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-4f041664})

[comment]: # ({new-63651b34})
### Source

CToken::update() in *ui/include/classes/api/services/CToken.php*.

[comment]: # ({/new-63651b34})
