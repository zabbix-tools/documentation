[comment]: # translation:outdated

[comment]: # ({new-293a482d})
# discoveryrule.delete

[comment]: # ({/new-293a482d})

[comment]: # ({new-1760a96c})
### Description

`object discoveryrule.delete(array lldRuleIds)`

This method allows to delete LLD rules.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-1760a96c})

[comment]: # ({new-27340eaa})
### Parameters

`(array)` IDs of the LLD rules to delete.

[comment]: # ({/new-27340eaa})

[comment]: # ({new-ae1ac187})
### Return values

`(object)` Returns an object containing the IDs of the deleted LLD rules
under the `itemids` property.

[comment]: # ({/new-ae1ac187})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-98660baf})
#### Deleting multiple LLD rules

Delete two LLD rules.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "discoveryrule.delete",
    "params": [
        "27665",
        "27668"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "ruleids": [
            "27665",
            "27668"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-98660baf})

[comment]: # ({new-76a51bc0})
### Source

CDiscoveryRule::delete() in
*ui/include/classes/api/services/CDiscoveryRule.php*.

[comment]: # ({/new-76a51bc0})
