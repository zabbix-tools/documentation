<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="es" datatype="plaintext" original="manual/api/reference/discoveryrule/update.md">
    <body>
      <trans-unit id="d756b50a" xml:space="preserve">
        <source># discoveryrule.update</source>
      </trans-unit>
      <trans-unit id="0d23e0d8" xml:space="preserve">
        <source>### Description

`object discoveryrule.update(object/array lldRules)`

This method allows to update existing LLD rules.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::</source>
      </trans-unit>
      <trans-unit id="cf98658c" xml:space="preserve">
        <source>### Parameters

`(object/array)` LLD rule properties to be updated.

The `itemid` property must be defined for each LLD rule, all other
properties are optional. Only the passed properties will be updated, all
others will remain unchanged.

Additionally to the [standard LLD rule properties](object#lld_rule), the
method accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|filter|object|LLD rule [filter](/manual/api/reference/discoveryrule/object#lld_rule_filter) object to replace the current filter.|
|preprocessing|array|LLD rule [preprocessing](/manual/api/reference/discoveryrule/object#lld_rule_preprocessing) options to replace the current preprocessing options.&lt;br&gt;&lt;br&gt;[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):&lt;br&gt;- *read-only* for inherited objects|
|lld\_macro\_paths|array|LLD rule [lld\_macro\_path](/manual/api/reference/discoveryrule/object#lld_macro_path) options.&lt;br&gt;&lt;br&gt;[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):&lt;br&gt;- *read-only* for inherited objects|
|overrides|array|LLD rule [overrides](/manual/api/reference/discoveryrule/object#lld_rule_overrides) options.&lt;br&gt;&lt;br&gt;[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):&lt;br&gt;- *read-only* for inherited objects|</source>
      </trans-unit>
      <trans-unit id="6f962e26" xml:space="preserve">
        <source>### Return values

`(object)` Returns an object containing the IDs of the updated LLD rules
under the `itemids` property.</source>
      </trans-unit>
      <trans-unit id="b41637d2" xml:space="preserve">
        <source>### Examples</source>
      </trans-unit>
      <trans-unit id="67c9278f" xml:space="preserve">
        <source>#### Adding a filter to an LLD rule

Add a filter so that the contents of the *{\#FSTYPE}* macro would match
the *\@File systems for discovery* regexp.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "discoveryrule.update",
    "params": {
        "itemid": "22450",
        "filter": {
            "evaltype": 1,
            "conditions": [
                {
                    "macro": "{#FSTYPE}",
                    "value": "@File systems for discovery"
                }
            ]
        }
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "22450"
        ]
    },
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="f3fd4d6c" xml:space="preserve">
        <source>#### Adding LLD macro paths

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "discoveryrule.update",
    "params": {
        "itemid": "22450",
        "lld_macro_paths": [
            {
                "lld_macro": "{#MACRO1}",
                "path": "$.json.path"
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "22450"
        ]
    },
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="8d8cc019" xml:space="preserve">
        <source>#### Disable trapping

Disable LLD trapping for discovery rule.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "discoveryrule.update",
    "params": {
        "itemid": "28336",
        "allow_traps": 0
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "28336"
        ]
    },
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="bef0a5f5" xml:space="preserve">
        <source>#### Updating LLD rule preprocessing options

Update an LLD rule with preprocessing rule “JSONPath”.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "discoveryrule.update",
    "params": {
        "itemid": "44211",
        "preprocessing": [
            {
                "type": 12,
                "params": "$.path.to.json",
                "error_handler": 2,
                "error_handler_params": "5"
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "44211"
        ]
    },
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="3f18a1c3" xml:space="preserve">
        <source>#### Updating LLD rule script

Update an LLD rule script with a different script and remove unnecessary
parameters that were used by previous script.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "discoveryrule.update",
    "params": {
        "itemid": "23865",
        "parameters": [],
        "script": "Zabbix.log(3, 'Log test');\nreturn 1;"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "23865"
        ]
    },
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="6a73b645" xml:space="preserve">
        <source>### Source

CDiscoveryRule::update() in
*ui/include/classes/api/services/CDiscoveryRule.php*.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
