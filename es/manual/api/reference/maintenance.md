[comment]: # translation:outdated

[comment]: # ({new-81ae720b})
# Maintenance

This class is designed to work with maintenances.

Object references:\

-   [Maintenance](/manual/api/reference/maintenance/object#maintenance)
-   [Time period](/manual/api/reference/maintenance/object#time_period)

Available methods:\

-   [maintenance.create](/manual/api/reference/maintenance/create) -
    creating new maintenances
-   [maintenance.delete](/manual/api/reference/maintenance/delete) -
    deleting maintenances
-   [maintenance.get](/manual/api/reference/maintenance/get) -
    retrieving maintenances
-   [maintenance.update](/manual/api/reference/maintenance/update) -
    updating maintenances

[comment]: # ({/new-81ae720b})
