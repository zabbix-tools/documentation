<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="es" datatype="plaintext" original="manual/api/reference/hostinterface/massremove.md">
    <body>
      <trans-unit id="9230837a" xml:space="preserve">
        <source># hostinterface.massremove</source>
      </trans-unit>
      <trans-unit id="4559aff6" xml:space="preserve">
        <source>### Description

`object hostinterface.massremove(object parameters)`

This method allows to remove host interfaces from the given hosts.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::</source>
      </trans-unit>
      <trans-unit id="8970fdbb" xml:space="preserve">
        <source>### Parameters

`(object)` Parameters containing the IDs of the hosts to be updated and
the interfaces to be removed.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|interfaces|object/array|Host interfaces to remove from the given hosts.&lt;br&gt;&lt;br&gt;The host interface object must have the `ip`, `dns` and `port` properties defined.&lt;br&gt;&lt;br&gt;[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):&lt;br&gt;- *required*|
|hostids|string/array|IDs of the hosts to be updated.&lt;br&gt;&lt;br&gt;[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):&lt;br&gt;- *required*|</source>
      </trans-unit>
      <trans-unit id="61a8b976" xml:space="preserve">
        <source>### Return values

`(object)` Returns an object containing the IDs of the deleted host
interfaces under the `interfaceids` property.</source>
      </trans-unit>
      <trans-unit id="b41637d2" xml:space="preserve">
        <source>### Examples</source>
      </trans-unit>
      <trans-unit id="cceffaf2" xml:space="preserve">
        <source>#### Removing interfaces

Remove the "127.0.0.1" SNMP interface from two hosts.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "hostinterface.massremove",
    "params": {
        "hostids": [
            "30050",
            "30052"
        ],
        "interfaces": {
            "dns": "",
            "ip": "127.0.0.1",
            "port": "161"
        }
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "interfaceids": [
            "30069",
            "30070"
        ]
    },
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="a3245281" xml:space="preserve">
        <source>### See also

-   [hostinterface.delete](delete)
-   [host.massremove](/manual/api/reference/host/massremove)</source>
      </trans-unit>
      <trans-unit id="6853cf7a" xml:space="preserve">
        <source>### Source

CHostInterface::massRemove() in
*ui/include/classes/api/services/CHostInterface.php*.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
