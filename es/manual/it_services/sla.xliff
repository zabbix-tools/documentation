<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="es" datatype="plaintext" original="manual/it_services/sla.md">
    <body>
      <trans-unit id="f9e8f670" xml:space="preserve">
        <source># 2 SLA</source>
      </trans-unit>
      <trans-unit id="b5ff62ed" xml:space="preserve">
        <source>### Overview

Once the [services](/manual/it_services/service_tree) are created, you can start monitoring whether their performance is on track with service level agreement (SLA).  

*Services-&gt;SLA* menu section allows to configure SLAs for various services. An SLA in Zabbix defines service level objective (SLO), expected uptime schedule and planned downtimes. 

SLAs and services are matched by [service tags](/manual/it_services/service_tree#tags). The same SLA may be applied to multiple services - performance will be measured for each matching service separately. A single service may have multiple SLAs assigned - data for each of the SLAs will be displayed separately.

In SLA reports Zabbix provides Service level indicator (SLI) data, which measures real service availability. Whether a service meets the SLA targets is determined by comparing SLO (expected availability in %) with SLI (real-life availability in %). </source>
      </trans-unit>
      <trans-unit id="d99c84a0" xml:space="preserve">
        <source>### Configuration

To create a new SLA, click on the *Create SLA* button.

The **SLA** tab allows to specify general 
SLA parameters.

![](../../../assets/en/manual/web_interface/sla.png){width=600}

|Parameter|Description|
|--|--------|
|*Name*|Enter the SLA name. |
|*SLO*|Enter the service level objective (SLO) as percentage. |
|*Reporting period*|Selecting the period will affect what periods are used in the [SLA report](/manual/web_interface/frontend_sections/services/sla_report) - *daily*, *weekly*, *monthly*, *quarterly*, or *annually*. |
|*Time zone*|Select the SLA time zone. |
|*Schedule*|Select the SLA schedule - 24x7 or custom. |
|*Effective date*|Select the date of starting SLA calculation. |
|*Service tags*|Add service tags to identify the services towards which this SLA should be applied. &lt;br&gt; **Name** - service tag name, must be exact match, case-sensitive. &lt;br&gt; **Operation** - select *Equals* if the tag value must match exactly (case-sensitive) or *Contains* if part of the tag value must match (case-insensitive). &lt;br&gt; **Value** - service tag value to search for according to selected operation.&lt;br&gt; The SLA is applied to a service, if at least one service tag matches.  |
|*Description*|Add a description for the SLA. |
|*Enabled*|Mark the checkbox to enable the SLA calculation. |

The **Excluded downtimes** tab allows to specify downtimes 
that are excluded from the SLA calculation.

![](../../../assets/en/manual/web_interface/sla_b.png){width=600}

Click on *Add* to configure excluded downtimes, then enter the period name, start date and duration. </source>
      </trans-unit>
      <trans-unit id="fb78ef0c" xml:space="preserve">
        <source>
### SLA reports

How a service performs compared to an SLA is visible in the [SLA report](/manual/web_interface/frontend_sections/services/sla_report). SLA reports can be viewed:

  - from the *SLA* section by clicking on the SLA report hyperlink;
  - from the *Services* section by clicking on the SLA name in the info tab;
  - in the Dashboard [widget](/manual/web_interface/frontend_sections/dashboards/widgets/sla_report) *SLA report*.
  
  Once an SLA is configured, the *Info* tab in the services section will also display some information about service performance. </source>
      </trans-unit>
    </body>
  </file>
</xliff>
