[comment]: # translation:outdated

[comment]: # ({new-f9e8f670})

# 22. Quick reference guides

## Overview

This documentation section contains quick recipes for setting up Zabbix for some commonly required monitoring goals.

It is designed with the new Zabbix user in mind and can be used as a navigator through other documentation sections that
 contain information required for resolving the task.

[comment]: # ({/new-f9e8f670})

[comment]: # ({new-b2664a94})

The following quick reference guides are available:

- [Monitor Windows with Zabbix agent](/manual/guides/monitor_windows)

[comment]: # ({/new-b2664a94})
