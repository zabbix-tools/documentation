<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="es" datatype="plaintext" original="manual/installation/frontend.md">
    <body>
      <trans-unit id="9cceae9f" xml:space="preserve">
        <source># 6 Web interface installation

This section provides step-by-step instructions for installing Zabbix
web interface. Zabbix frontend is written in PHP, so to run it a PHP
supported webserver is needed.</source>
        <target state="needs-translation"># 6 Instalación de la interfaz web

Esta sección proporciona instrucciones paso a paso para instalar la interfaz web de Zabbix. El frontend de Zabbix está escrito en PHP, por lo que para ejecutarlo se necesita un servidor web que soporte PHP.</target>
      </trans-unit>
      <trans-unit id="0c649c0e" xml:space="preserve">
        <source>:::notetip
You can find out more about setting up SSL for Zabbix frontend by referring to these [best practices](/manual/installation/requirements/best_practices).
:::</source>
      </trans-unit>
      <trans-unit id="6b173526" xml:space="preserve">
        <source>#### Welcome screen

Open Zabbix frontend URL in the browser. If you have installed Zabbix
from packages, the URL is:

-   for Apache: *http://&lt;server\_ip\_or\_name&gt;/zabbix*
-   for Nginx: *http://&lt;server\_ip\_or\_name&gt;*

You should see the first screen of the frontend installation wizard.

Use the *Default language* drop-down menu to change system default
language and continue the installation process in the selected language
(optional). For more information, see [Installation of additional
frontend languages](/manual/appendix/install/locales).

![](../../../assets/en/manual/installation/install_1.png){width="550"}</source>
        <target state="needs-translation">#### Pantalla de bienvenida

Abre la URL del frontend de Zabbix en el navegador. Si has instalado Zabbix desde los paquetes, la URL es:

- · Para Apache: *http://&lt;server\_ip\_or\_name&gt;/zabbix*
- · Para Nginx: *http://&lt;server\_ip\_or\_name&gt;*

Debería ver la primera pantalla del asistente de instalación del frontend.
Utilice el menú desplegable *Idioma por defecto* para cambiar el idioma por defecto del sistema y continua el proceso de instalación en el idioma seleccionado (opcional). Para más información, consulte [Instalación de idiomas adicionales idiomas del frontend](/manual/appendix/install/locales).

![](../../../assets/en/manual/installation/install_1.png){width="550"}</target>
      </trans-unit>
      <trans-unit id="2458c643" xml:space="preserve">
        <source>#### Check of pre-requisites

Make sure that all software prerequisites are met.

![](../../../assets/en/manual/installation/install_2.png){width="550"}

|Pre-requisite|Minimum value|Description|
|--|--|------|
|*PHP version*|7.4.0| |
|*PHP memory\_limit option*|128MB|In php.ini:&lt;br&gt;memory\_limit = 128M|
|*PHP post\_max\_size option*|16MB|In php.ini:&lt;br&gt;post\_max\_size = 16M|
|*PHP upload\_max\_filesize option*|2MB|In php.ini:&lt;br&gt;upload\_max\_filesize = 2M|
|*PHP max\_execution\_time option*|300 seconds (values 0 and -1 are allowed)|In php.ini:&lt;br&gt;max\_execution\_time = 300|
|*PHP max\_input\_time option*|300 seconds (values 0 and -1 are allowed)|In php.ini:&lt;br&gt;max\_input\_time = 300|
|*PHP session.auto\_start option*|must be disabled|In php.ini:&lt;br&gt;session.auto\_start = 0|
|*Database support*|One of: MySQL, Oracle, PostgreSQL.|One of the following modules must be installed:&lt;br&gt;mysql, oci8, pgsql|
|*bcmath*| |php-bcmath|
|*mbstring*| |php-mbstring|
|*PHP mbstring.func\_overload option*|must be disabled|In php.ini:&lt;br&gt;mbstring.func\_overload = 0|
|*sockets*| |php-net-socket. Required for user script support.|
|*gd*|2.0.28|php-gd. PHP GD extension must support PNG images (*--with-png-dir*), JPEG (*--with-jpeg-dir*) images and FreeType 2 (*--with-freetype-dir*).|
|*libxml*|2.6.15|php-xml|
|*xmlwriter*| |php-xmlwriter|
|*xmlreader*| |php-xmlreader|
|*ctype*| |php-ctype|
|*session*| |php-session|
|*gettext*| |php-gettext&lt;br&gt;Since Zabbix 2.2.1, the PHP gettext extension is not a mandatory requirement for installing Zabbix. If gettext is not installed, the frontend will work as usual, however, the translations will not be available.|

Optional pre-requisites may also be present in the list. A failed
optional prerequisite is displayed in orange and has a *Warning* status.
With a failed optional pre-requisite, the setup may continue.

::: noteimportant
If there is a need to change the Apache user or
user group, permissions to the session folder must be verified.
Otherwise Zabbix setup may be unable to continue.
:::</source>
        <target state="needs-translation">#### Comprobación de los requisitos previos


Asegúrese de que se cumplen todos los requisitos previos del software.

![](../../../assets/en/manual/installation/install_2.png){width="550"}

|Prerequisitos| Valor Mínimo | Descripción|
|-------------|-------------|-----------|
|*PHP version*|7.2.5|&lt;|
|*PHP memory\_limit option*|128MB|In php.ini:&lt;br&gt;memory\_limit = 128M|
|*PHP post\_max\_size option*|16MB|In php.ini:&lt;br&gt;post\_max\_size = 16M|
|*PHP upload\_max\_filesize option*|2MB|In php.ini:&lt;br&gt;upload\_max\_filesize = 2M|
|*PHP max\_execution\_time option*|300 seconds (values 0 and -1 are allowed)|In php.ini:&lt;br&gt;max\_execution\_time = 300|
|*PHP max\_input\_time option*|300 seconds (values 0 and -1 are allowed)|In php.ini:&lt;br&gt;max\_input\_time = 300|
|*PHP session.auto\_start option*|debe estar desactivado|In php.ini:&lt;br&gt;session.auto\_start = 0|
|*Database support*|Uno de: MySQL, Oracle, PostgreSQL.|Uno de los siguientes módulos debe estar instalado:&lt;br&gt;mysql, oci8, pgsql|
|*bcmath*|&lt;|php-bcmath|
|*mbstring*|&lt;|php-mbstring|
|*PHP mbstring.func\_overload option*|debe estar desactivado|In php.ini:&lt;br&gt;mbstring.func\_overload = 0|
|*sockets*|&lt;|php-net-socket.  Necesario para el soporte de scripts de usuario.||*gd*|2.0.28|php-gd. La extensión PHP GD debe soportar la imagen PNG (*--with-png-dir*), JPEG (*--with-jpeg-dir*) images and FreeType 2 (*--with-freetype-dir*).|
|*libxml*|2.6.15|php-xml|
|*xmlwriter*|&lt;|php-xmlwriter|
|*xmlreader*|&lt;|php-xmlreader|
|*ctype*|&lt;|php-ctype|
|*session*|&lt;|php-session|
|*gettext*|&lt;|php-gettext&lt;br&gt;Desde Zabbix 2.2.1, la extensión PHP gettext no es un requisito obligatorio para instalar Zabbix. Si gettext no está instalado, el frontend funcionará como siempre, sin embargo, las traducciones no estarán disponibles.|

Los prerrequisitos opcionales también pueden estar presentes en la lista. Un prerrequisito opcional fallido se muestra en naranja y tiene un estado de *Atención*.
Con un prerrequisito opcional fallido, la configuración puede continuar.

::: notaimportante
Si es necesario cambiar el usuario de Apache o grupo de usuarios, los permisos de la carpeta de sesión deben ser verificados. De lo contrario, la instalación de Zabbix no podrá continuar.
:::</target>
      </trans-unit>
      <trans-unit id="879dc06f" xml:space="preserve">
        <source>#### Configure DB connection

Enter details for connecting to the database. Zabbix database must
already be created.

![](../../../assets/en/manual/installation/install_3a.png){width="550"}

If the *Database TLS encryption* option is checked, then additional
fields for [configuring the TLS
connection](/manual/appendix/install/db_encrypt) to the database appear
in the form (MySQL or PostgreSQL only).

If *Store credentials in* is set to HashiCorp Vault or CyberArk Vault,
additional parameters will become available:

- for [HashiCorp Vault](/manual/config/secrets/hashicorp): Vault API endpoint, secret path and authentication token;

- for [CyberArk Vault](/manual/config/secrets/cyberark): Vault API endpoint, secret query string and certificates. Upon marking *Vault certificates* checkbox, two new fields for specifying paths to SSL certificate file and SSL key file will appear.

![](../../../assets/en/manual/installation/install_3b.png){width="550"}</source>
        <target state="needs-translation">#### Configurar la conexión a la base de datos

Introduzca los detalles para conectarse a la base de datos. La base de datos Zabbix debe estar ya creada.

![](../../../assets/en/manual/installation/install_3a.png){width="550"}

Si la opción *Encriptación TLS de la base de datos* está marcada, entonces los campos adicionales para [configurar la conexión TLS conexión](/manual/appendix/install/db_encrypt) a la base de datos aparecen en el formulario (sólo en MySQL o PostgreSQL).

Si se selecciona la opción HashiCorp Vault para almacenar las credenciales, hay campos adicionales disponibles para especificar el punto final de la API de Vault, ruta secreta y token de autenticación:

![](../../../assets/en/manual/installation/install_3b.png){width="550"}</target>
      </trans-unit>
      <trans-unit id="98277238" xml:space="preserve">
        <source>#### Settings

Entering a name for Zabbix server is optional, however, if submitted, it
will be displayed in the menu bar and page titles.

Set the default [time zone](/manual/web_interface/time_zone#overview)
and theme for the frontend.

![](../../../assets/en/manual/installation/install_4.png){width="550"}</source>
        <target state="needs-translation">#### Configuración

Introducir un nombre para el servidor de Zabbix es opcional, sin embargo, si se envía, éste se mostrará en la barra de menú y en los títulos de las páginas.

Establezca la [zona horaria] por defecto (/manual/web_interface/time_zone#overview) y el tema para el frontend.

![](../../../assets/en/manual/installation/install_4.png){width="550"}</target>
      </trans-unit>
      <trans-unit id="42398398" xml:space="preserve">
        <source>#### Pre-installation summary

Review a summary of settings.

![](../../../assets/en/manual/installation/install_5.png){width="550"}</source>
        <target state="needs-translation">#### Resumen de preinstalación

Revise un resumen de la configuración.

![](../../../assets/en/manual/installation/install_5.png){width="550"}</target>
      </trans-unit>
      <trans-unit id="1124dd9e" xml:space="preserve">
        <source>#### Install

If installing Zabbix from sources, download the configuration file and
place it under conf/ in the webserver HTML documents subdirectory where
you copied Zabbix PHP files to.

![](../../../assets/en/manual/installation/install_6.png){width="550"}

![](../../../assets/en/manual/installation/saving_zabbix_conf.png){width="350"}

::: notetip
Providing the webserver user has write access to conf/
directory the configuration file would be saved automatically and it
would be possible to proceed to the next step right away.
:::

Finish the installation.

![](../../../assets/en/manual/installation/install_7.png){width="550"}</source>
        <target state="needs-translation">#### Instalar
Si instalas Zabbix desde las fuentes, descarga el archivo de configuración y colóquelo bajo conf/ en el subdirectorio de documentos HTML del servidor web donde copió los archivos PHP de Zabbix.

![](../../../assets/en/manual/installation/install_6.png){width="550"}

![](../../../assets/en/manual/installation/saving_zabbix_conf.png){width="350"}

::: notetip
Siempre que el usuario del servidor web tenga acceso de escritura al directorio conf/ el archivo de configuración se guardaría automáticamente y sería posible proceder al siguiente paso de inmediato.
:::

Finalizar la instalación.

![](../../../assets/en/manual/installation/install_7.png){width="550"}</target>
      </trans-unit>
      <trans-unit id="d59dc4b9" xml:space="preserve">
        <source>#### Log in

Zabbix frontend is ready! The default user name is **Admin**, password
**zabbix**.

![](../../../assets/en/manual/quickstart/login.png){width="350"}

Proceed to [getting started with Zabbix](/manual/quickstart/login).</source>
        <target state="needs-translation">#### Entrar
¡El frontend de Zabbix está listo! El nombre de usuario por defecto es **Admin**, contraseña **zabbix**.

![](../../../assets/en/manual/quickstart/login.png){width="350"}

Proceda a [getting started with Zabbix](/manual/quickstart/login).</target>
      </trans-unit>
    </body>
  </file>
</xliff>
