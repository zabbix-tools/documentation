[comment]: # translation:outdated

[comment]: # ({42ac6e14-8d986b3b})
#1 Obtener Zabbix

[comment]: # ({/42ac6e14-8d986b3b})

[comment]: # ({e4515af2-74ef7c01})
#### Descripción general

Hay cuatro formas de obtener Zabbix:

- Instalarlo desde los [paquetes de la distribución](install_from_packages#From_distribution_packages)
- Descargue el archivo fuente más reciente y [compile
    usted mismo](install#Installation-from-sources)
- Instalarlo desde los [contenedores](contenedores)
- Descarga el [dispositivo virtual](/manual/appliance)

Para descargar los últimos paquetes de la distribución, fuentes precompiladas o
el dispositivo virtual, vaya a la [página de descarga de Zabbix
] (https://www.zabbix.com/download), donde enlaces directos a las últimas versiones son proporcionados.


[comment]: # ({/e4515af2-74ef7c01})

[comment]: # ({a2e36b9f-e88f42af})
#### Obtener el código fuente de Zabbix

Hay varias formas de obtener el código fuente de Zabbix:

- Puedes [descargar](https://www.zabbix.com/download_sources) las versiones estables lanzadas del sitio web oficial de Zabbix
- Puede [descargar] (https://www.zabbix.com/developers) compilaciones nocturnas de la página oficial del desarrollador del sitio web de Zabbix
- Puede obtener la última versión de desarrollo del sistema de repositorio código fuente de Git:
    - La ubicación principal del repositorio completo esta en
        <https://git.zabbix.com/scm/zbx/zabbix.git>
    - Las versiones maestras y compatibles también se reflejan en Github en
        <https://github.com/zabbix/zabbix>

Se debe instalar un cliente Git para clonar el repositorio. El paquete oficial de la línea de comandos del cliente Git se denomina comúnmente **git** en las distribuciones. Para instalar, por ejemplo, en Debian/Ubuntu, ejecute:

    sudo apt-get update
    sudo apt-get install git

Para obtener todas las fuentes de Zabbix, cambie al directorio en el que desea colocar el
codigo y ejecute:

    git clone https://git.zabbix.com/scm/zbx/zabbix.git

[comment]: # ({/a2e36b9f-e88f42af})
