[comment]: # translation:outdated

[comment]: # ({c85c8fad-02b10f8d})
#2 Requisitos

[comment]: # ({/c85c8fad-02b10f8d})

[comment]: # ({new-80330f65})
#### Hardware

[comment]: # ({/new-80330f65})

[comment]: # ({58c32b96-4ae00568})
##### Memoria

Zabbix requiere memoria física y de disco. 128 MB de memoria física
y 256 MB de espacio libre en disco podría ser un buen punto de partida. Sin embargo,
la cantidad de memoria de disco requerida obviamente depende de la cantidad de
equipos y parámetros que están siendo monitoreados. Si estás planeando
mantener un largo historial de parámetros monitoreados, debe pensar en
al menos un par de gigabytes para tener suficiente espacio para almacenar el historial
en la base de datos Cada proceso demonio de Zabbix requiere varias conexiones
al servidor de base de datos. La cantidad de memoria asignada para la conexión
depende de la configuración del motor de la base de datos.

::: notaclásica
Cuanta más memoria física tenga, más rápida funciona la base de datos (y por lo tanto
Zabbix) 
:::

[comment]: # ({/58c32b96-4ae00568})

[comment]: # ({80ac785c-7967309a})
##### CPU

Zabbix y especialmente la base de datos de Zabbix pueden requerir significativos recursos de CPU 
dependiendo del número de parámetros monitoreados y elegidos en el motor de base de datos.

[comment]: # ({/80ac785c-7967309a})

[comment]: # ({b0842840-1cbee7bf})
##### Otro hardware

Se requiere un puerto de comunicación serial y un módem GSM serie para
el soporte de notificación por SMS en Zabbix. El convertidor de USB a serie también funciona.

[comment]: # ({/b0842840-1cbee7bf})

[comment]: # ({new-42d1d93e})
#### Examples of hardware configuration

The table provides examples of hardware configuration, assuming a **Linux/BSD/Unix** platform.

These are size and hardware configuration examples to start with. Each Zabbix installation is unique. 
Make sure to benchmark the performance of your Zabbix system in a staging or development environment, 
so that you can fully understand your requirements before deploying the Zabbix installation to its 
production environment.

|Installation size|Monitored metrics^**1**^|CPU/vCPU cores|Memory<br>(GiB)|Database|Amazon EC2^**2**^|
|-|-|-|-|-|-|
|Small|1 000|2|8|MySQL Server,<br>Percona Server,<br>MariaDB Server,<br>PostgreSQL|m6i.large/m6g.large|
|Medium|10 000|4|16|MySQL Server,<br>Percona Server,<br>MariaDB Server,<br>PostgreSQL|m6i.xlarge/m6g.xlarge|
|Large|100 000|16|64|MySQL Server,<br>Percona Server,<br>MariaDB Server,<br>PostgreSQL,<br>Oracle|m6i.4xlarge/m6g.4xlarge|
|Very large|1 000 000|32|96|MySQL Server,<br>Percona Server,<br>MariaDB Server,<br>Oracle|m6i.8xlarge/m6g.8xlarge|

^**1**^ 1 metric = 1 item + 1 trigger + 1 graph<br>
^**2**^ Example with Amazon general purpose EC2 instances, using ARM64 or x86_64 architecture, a 
proper instance type like Compute/Memory/Storage optimised should be selected during Zabbix 
installation evaluation and testing before installing in its production environment.

::: noteclassic
Actual configuration depends on the number of active items
and refresh rates very much (see [database
size](/manual/installation/requirements#Database_size) section of this
page for details). It is highly recommended to run the database on a
separate box for large installations.
:::

[comment]: # ({/new-42d1d93e})



[comment]: # ({d84e6dc0-ad0c32cf})
#### Plataformas compatibles

Debido a los requisitos de seguridad y la naturaleza de misión crítica del
servidor de monitoreo, UNIX es el único sistema operativo que puede
entregar constantemente el rendimiento necesario, la tolerancia a fallas y
resiliencia. Zabbix opera en versiones líderes en el mercado.

Los componentes de Zabbix están disponibles y probados para las siguientes plataformas:

|Plataforma|Servidor|Agente|Agente2|
|--------|------|-----|------|
|Linux|x|x|x|
|IBM AIX|x|x|\-|
|FreeBSD|x|x|\-|
|NetBSD|x|x|\-|
|OpenBSD|x|x|\-|
|HP-UX|x|x|\-|
|Mac OS X|x|x|\-|
|Solaris|x|x|\-|
|Windows|\-|x|x|

::: notaclásica
El servidor/agente de Zabbix puede funcionar en otros sistemas operativos similares a Unix.
sistemas también. El agente Zabbix es compatible con todos los escritorios de Windows y
versiones del servidor desde XP.
:::

::: notaimportante
Zabbix deshabilita los volcados del núcleo si se compila con
cifrado y no se inicia si el sistema no permite la desactivación de
volcado del núcleo.
:::

[comment]: # ({/d84e6dc0-ad0c32cf})

[comment]: # ({43785b79-323b6241})
#### Software necesario

Zabbix se basa en servidores web modernos, motores de bases de datos líderes y
lenguaje de programación PHP.

[comment]: # ({/43785b79-323b6241})

[comment]: # ({new-fd454df8})
##### Sistema de administración de base de datos

|Software|Versiones compatibles|Comentarios|
|--------|------------------|--------|
|*MySQL/Percona*|8.0.X|Requerido si se utiliza MySQL (o Percona) como base de datos backend de Zabbix. Se requiere el motor InnoDB. Recomendamos usar la biblioteca [MariaDB Connector/C](https://downloads.mariadb.org/connector-c/) para construir el servidor/proxy.|
|*MariaDB*|10.5.00-10.6.X|Se requiere motor InnoDB. Recomendamos usar la biblioteca [MariaDB Connector/C](https://downloads.mariadb.org/connector-c/) para construir el servidor/proxy.|
|*Oracle*|19c - 21c|Requerido si se usa Oracle como base de datos back-end de Zabbix.|
|*PostgreSQL*|13.X|Requerido si se usa PostgreSQL como base de datos backend de Zabbix.|
|*TimescaleDB* para PostgreSQL|2.0.1-2.3|Requerido si TimescaleDB se usa como base de datos back-end de Zabbix. Asegúrese de instalar la distribución de TimescaleDB con la compresión compatible.|
|*SQLite*|3.3.5-3.34.X|SQLite solo es compatible con proxies Zabbix. Obligatorio si se utiliza SQLite como base de datos proxy de Zabbix.|

::: notaclásica
 Aunque Zabbix puede trabajar con bases de datos disponibles en los
sistemas operativos, para una mejor experiencia, recomendamos usar bases de datos
instalado desde los repositorios oficiales de desarrolladores de bases de datos.

:::

[comment]: # ({/new-fd454df8})

[comment]: # ({4d9062ad-75f0586e})
##### Interfaz

El ancho de pantalla mínimo admitido para la interfaz de Zabbix es de 1200 px.

|Software|Versión|Comentarios|
|--------|-------|--------|
|*Apache*|1.3.12 o posterior|<|
|*PHP*|7.2.5 o posterior|PHP 8.0 no es compatible.|
|Extensiones PHP:|<|<|
|*gd*|2.0.28 o posterior|La extensión PHP GD debe ser compatible con imágenes PNG (*--with-png-dir*), imágenes JPEG (*--with-jpeg-dir*) y FreeType 2 (*-- con-tipo-libre-dir*).|
|*bcmath*|<|php-bcmath (*--enable-bcmath*)|
|*ctype*|<|php-ctype (*--enable-ctype*)|
|*libXML*|2.6.15 o posterior|php-xml, si el distribuidor lo proporciona como un paquete separado.|
|*xmlreader*|<|php-xmlreader, si el distribuidor lo proporciona como un paquete separado.|
|*xmlwriter*|<|php-xmlwriter, si el distribuidor lo proporciona como un paquete separado.|
|*session*|<|php-session, si el distribuidor lo proporciona como un paquete separado.|
|*sockets*|<|php-net-socket (*--enable-sockets*). Requerido para soporte de secuencias de comandos de usuario.|
|*mbstring*|<|php-mbstring (*--enable-mbstring*)|
|*gettext*|<|php-gettext (*--with-gettext*). Requerido para que las traducciones funcionen.|
|*ldap*|<|php-ldap. Obligatorio solo si se utiliza la autenticación LDAP en la interfaz.|
|*openssl*|<|php-openssl. Obligatorio solo si se utiliza la autenticación SAML en la interfaz.|
|*mysqli*|<|Requerido si se usa MySQL como base de datos backend de Zabbix.|
|*oci8*|<|Requerido si se utiliza Oracle como base de datos backend de Zabbix.|
|*pgsql*|<|Requerido si se usa PostgreSQL como base de datos back-end de Zabbix.|

::: notaclásica
Zabbix puede funcionar en versiones anteriores de Apache, MySQL,
Oracle y PostgreSQL también.
:::

::: notaimportante
Para otras fuentes además de la predeterminada DejaVu, PHP
función
[imagerotate](http://php.net/manual/en/function.imagerotate.php) podría
ser requerido. Si falta, es posible que estas fuentes se representen incorrectamente
cuando se muestra un gráfico. Esta función solo está disponible si PHP está
compilado con GD incluido, que no es el caso en Debian y otras
distribuciones.
:::

[comment]: # ({/4d9062ad-75f0586e})

[comment]: # ({6bb3f765-dda86afe})
##### Navegador web en el lado del cliente

Las cookies y JavaScript deben estar habilitados.

Las últimas versiones estables de Google Chrome, Mozilla Firefox, Microsoft
Edge, Apple Safari y Opera están soportadas.

::: nota de advertencia
Se implementa la política del mismo origen para IFrames,
lo que significa que Zabbix no se puede colocar en marcos en un diferente
dominio.\
\
Aún así, las páginas colocadas en un marco de Zabbix tendrán acceso a la interfaz de Zabbix
(a través de JavaScript) si la página que se coloca en el marco
y la interfaz de Zabbix están en el mismo dominio. una pagina como
`http://secure-zabbix.com/cms/page.html`, si se coloca en paneles en
`http://secure-zabbix.com/zabbix/`, tendrá acceso JS completo a
Zabbix.
:::

[comment]: # ({/6bb3f765-dda86afe})

[comment]: # ({a27bcc70-096d332c})
##### Servidor

Los requisitos obligatorios son necesarios siempre. Los requisitos opcionales son
necesarios para el apoyo de la función específica.

|Requisito|Estado|Descripción|
|-----------|------|-----------|
|*libpcre*|Obligatorio|La biblioteca PCRE es necesaria para la compatibilidad con [Expresiones regulares compatibles con Perl](https://en.wikipedia.org/wiki/Perl_Compatible_Regular_Expressions) (PCRE).<br>El nombre puede diferir según el GNU/ Distribución de Linux, por ejemplo, 'libpcre3' o 'libpcre1'. Se admiten PCRE v8.x y PCRE2 v10.x (desde Zabbix 6.0.0).|
|*libevent*|^|Requerido para soporte de métricas masivas y monitoreo de IPMI. Versión 1.4 o superior.<br>Tenga en cuenta que para el proxy Zabbix este requisito es opcional; es necesario para el soporte de monitoreo de IPMI.|
|*libpthread*|^|Requerido para mutex y soporte de bloqueo de lectura y escritura.|
|*zlib*|^|Requerido para soporte de compresión.|
|*OpenIPMI*|Opcional|Requerido para la compatibilidad con IPMI.|
|*libssh2* o *libssh*|^|Requerido para [comprobaciones SSH](/manual/config/items/itemtypes/ssh_checks#overview). Versión 1.0 o superior (libssh2); 0.6.0 o superior (libssh).<br>libssh es compatible desde Zabbix 4.4.6.|
|*fping*|^|Requerido para [elementos de ping ICMP](/manual/config/items/itemtypes/simple_checks#icmp_pings).|
|*libcurl*|^|Requerido para monitoreo web, monitoreo de VMware, autenticación SMTP, `web.page.*` Zabbix agent [items](/manual/config/items/itemtypes/zabbix_agent), elementos de agente HTTP y Elasticsearch (si usado). Se recomienda la versión 7.28.0 o superior.<br>Requisitos de la versión de Libcurl:<br>- Autenticación SMTP: versión 7.20.0 o superior<br>- Elasticsearch: versión 7.28.0 o superior|
|*libxml2*|^|Obligatorio para la supervisión de VMware y el preprocesamiento XML XPath.|
|*net-snmp*|^|Requerido para compatibilidad con SNMP. Versión 5.3.0 o superior.|
|*GnuTLS*, *OpenSSL* o *LibreSSL*|^|Requerido cuando se usa [encryption](/manual/encryption#compiling_zabbix_with_encryption_support).|

[comment]: # ({/a27bcc70-096d332c})

[comment]: # ({4a0ff4f6-800c8308})
##### Agente

|Requisito|Estado|Descripción|
|-----------|------|-----------|
|*libpcre*|Obligatorio|La biblioteca PCRE es necesaria para la compatibilidad con [Expresiones regulares compatibles con Perl](https://en.wikipedia.org/wiki/Perl_Compatible_Regular_Expressions) (PCRE).<br>El nombre puede diferir según el GNU/ Distribución de Linux, por ejemplo, 'libpcre3' o 'libpcre1'. Se admiten PCRE v8.x y PCRE2 v10.x (desde Zabbix 6.0.0).|
|*GnuTLS*, *OpenSSL* o *LibreSSL*|Opcional|Requerido al usar [cifrado](/manual/encryption#compiling_zabbix_with_encryption_support).<br>En sistemas Microsoft Windows, se requiere OpenSSL 1.1.1 o posterior.|

::: notaclásica
 A partir de la versión 5.0.3, el agente Zabbix no funcionará en
Plataformas AIX por debajo de las versiones 6.1 TL07 / AIX 7.1 TL01.
:::

[comment]: # ({/4a0ff4f6-800c8308})

[comment]: # ({1b74393d-4aa86212})
##### Agente 

|Requisito|Estado|Descripción|
|-----------|------|-----------|
|*libpcre*|Obligatorio|La biblioteca PCRE es necesaria para la compatibilidad con [Expresiones regulares compatibles con Perl](https://en.wikipedia.org/wiki/Perl_Compatible_Regular_Expressions) (PCRE).<br>El nombre puede diferir según el GNU/ Distribución de Linux, por ejemplo, 'libpcre3' o 'libpcre1'. Se admiten PCRE v8.x y PCRE2 v10.x (desde Zabbix 6.0.0).|
|*GnuTLS*, *OpenSSL* o *LibreSSL*|Opcional|Requerido al usar [cifrado](/manual/encryption#compiling_zabbix_with_encryption_support).<br>En sistemas Microsoft Windows, se requiere OpenSSL 1.1.1 o posterior.|

::: notaclásica
 A partir de la versión 5.0.3, el agente Zabbix no funcionará en
Plataformas AIX por debajo de las versiones 6.1 TL07 / AIX 7.1 TL01.
:::

[comment]: # ({/1b74393d-4aa86212})

[comment]: # ({1582b6b6-149075a3})
##### Pasarela Java

Si obtuvo Zabbix del repositorio fuente o de un archivo, entonces
las dependencias necesarias ya están incluidas en el árbol fuente.

Si obtuvo Zabbix del paquete de su distribución, entonces el
el sistema de empaquetado ya proporciona las dependencias necesarias.

En los dos casos anteriores, el software está listo para ser utilizado y no requiere
las descargas son necesarias.

Sin embargo, si desea proporcionar sus versiones de estas dependencias
(por ejemplo, si está preparando un paquete para alguna distribución Linux), a continuación se muestra la lista de versiones de la bibliotecase sabe que trabaja con la puerta de enlace de Java Zabbix puede funcionar con otras versiones de estos bibliotecas también.

La siguiente tabla enumera los archivos JAR que actualmente se incluyen con Java
puerta de enlace en el código original:

|Biblioteca|Licencia|Sitio web|Comentarios|
|-------|-------|-------|--------|
|*logback-core-1.2.3.jar*|EPL 1.0, LGPL 2.1|<http://logback.qos.ch/>|Probado con 0.9.27, 1.0.13, 1.1.1 y 1.2.3. |
|*logback-classic-1.2.3.jar*|EPL 1.0, LGPL 2.1|<http://logback.qos.ch/>|Probado con 0.9.27, 1.0.13, 1.1.1 y 1.2.3. |
|*slf4j-api-1.7.30.jar*|Licencia MIT|<http://www.slf4j.org/>|Probado con 1.6.1, 1.6.6, 1.7.6 y 1.7.30.|
|*android-json-4.3\_r3.1.jar*|Licencia Apache 2.0|<https://android.googlesource.com/platform/libcore/+/master/json>|Probado con 2.3.3\_r1.1 y 4.3\_r3.1. Consulte src/zabbix\_java/lib/README para obtener instrucciones sobre cómo crear un archivo JAR.|

La puerta de enlace de Java se puede construir utilizando Oracle Java o código abierto
OpenJDK (versión 1.6 o posterior). Los paquetes proporcionados por Zabbix se compilan
utilizando OpenJDK. La siguiente tabla proporciona información sobre OpenJDK
versiones utilizadas para construir paquetes Zabbix por distribución:

|Distribución|Versión de OpenJDK|
|------------------|---------------|
|RHEL/Cent OS 8|1.8.0|
|RHEL/Cent OS 7|1.8.0|
|SLES 15|11.0.4|
|SLES 12|1.8.0|
|Debian 10|11.0.8|
|Ubuntu 20.04|11.0.8|
|Ubuntu 18.04|11.0.8|

[comment]: # ({/1582b6b6-149075a3})

[comment]: # ({new-c8024c35})

#### Default port numbers

The following list of open ports per component is applicable for default configuration:

|Zabbix component|Port number|Protocol|Type of connection|
|-------|-------|-------|-------|
|Zabbix agent|10050|TCP|on demand|
|Zabbix agent 2|10050|TCP|on demand|
|Zabbix server|10051|TCP|on demand|
|Zabbix proxy|10051|TCP|on demand|
|Zabbix Java gateway|10052|TCP|on demand|
|Zabbix web service|10053|TCP|on demand|
|Zabbix frontend|80|HTTP|on demand|
|^|443|HTTPS|on demand|
|Zabbix trapper|10051 |TCP| on demand|

::: noteclassic
The port numbers should be open in firewall to enable Zabbix communications. Outgoing TCP connections usually do not require explicit firewall settings.
::: 

[comment]: # ({/new-c8024c35})


[comment]: # ({afe2e43c-1d73b238})
#### Tamaño de la base de datos

Los datos de configuración de Zabbix requieren una cantidad fija de espacio en disco y no
no crecen mucho.

El tamaño de la base de datos de Zabbix depende principalmente de estas variables, que definen el
cantidad de datos históricos almacenados:

- Número de valores procesados por segundo

Esta es la cantidad promedio de valores nuevos que el servidor Zabbix recibe cada
segundo. Por ejemplo, si tenemos 3000 artículos para monitorear con una actualización
tasa de 60 segundos, el número de valores por segundo se calcula como
3000/60 = **50**.

Significa que se agregan 50 valores nuevos a la base de datos de Zabbix cada segundo.

- Configuración de ama de llaves para el histórico 

Zabbix mantiene los valores durante un período de tiempo fijo, normalmente varias semanas
o meses. Cada nuevo valor requiere una cierta cantidad de espacio en disco para
datos e índice.

Entonces, si nos gustaría mantener 30 días de historial y recibimos 50 valores
por segundo, el número total de valores será de alrededor
(**30**\*24\*3600)\* **50** = 129.600.000, o alrededor de 130M de valores.

Según el motor de base de datos utilizado, el tipo de valores recibidos (flotantes,
números enteros, cadenas, archivos de registro, etc.), el espacio en disco para guardar un solo
el valor puede variar de 40 bytes a cientos de bytes. Normalmente está alrededor
90 bytes por valor para elementos numéricos^**2**^. En nuestro caso, significa que
130 millones de valores requerirán 130 millones \* 90 bytes = **10,9 GB** de espacio en disco.

::: notaclásica
El tamaño de los valores de los elementos de texto/registro es imposible de predecir
exactamente, pero puede esperar alrededor de 500 bytes por valor.
:::

- Ajuste de ama de llaves para tendencias.

Zabbix mantiene un conjunto de valores máximo/mínimo/promedio/recuento de 1 hora para cada elemento en
la tabla **tendencias**. Los datos se utilizan para tendencias y periodos largos.
gráficos El período de una hora no se puede personalizar.

La base de datos Zabbix, según el tipo de base de datos, requiere alrededor de 90 bytes
por cada total. Supongamos que nos gustaría mantener los datos de tendencia durante 5 años.
Los valores para 3000 artículos requerirán 3000\*24\*365\* **90** = **2.2GB**
por año, o **11GB** por 5 años.

- Ajustes de ama de llaves para eventos.

Cada evento de Zabbix requiere aproximadamente 250 bytes de espacio en disco^**1**^.
Es difícil estimar la cantidad de eventos generados por Zabbix diariamente.
En el peor de los casos, podemos suponer que Zabbix genera uno
evento por segundo.

Para cada evento recuperado, se crea un registro event\_recovery. Normalmente
la mayoría de los eventos se recuperarán para que podamos asumir uno
event\_recovery registro por evento. Eso significa 80 bytes adicionales por
evento.

Opcionalmente, los eventos pueden tener etiquetas, cada registro de etiqueta requiere aproximadamente
100 bytes de espacio en disco^**1**^. El número de etiquetas por evento (\#tags)
depende de la configuración. Entonces, cada uno necesitará \#etiquetas \* 100 adicionales
bytes de espacio en disco.

Significa que si queremos mantener 3 años de eventos, esto requeriría
3\*365\*24\*3600\* (250+80+\#etiquetas\*100) = **\~30GB**+\#etiquetas\*100B disco
espacio^**2**^.

::: notaclásica
 ^**1**^ Más cuando se tienen nombres de eventos, etiquetas y
valores.

^**2**^ Las aproximaciones de tamaño se basan en MySQL y pueden ser
diferente para otras bases de datos.
:::

La tabla contiene fórmulas que se pueden usar para calcular el espacio en disco
requerido para el sistema Zabbix:

|Parámetro|Fórmula para el espacio en disco requerido (en bytes)|
|---------|--------------------------------------- ---|
|*Configuración Zabbix*|Tamaño fijo. Normalmente 10 MB o menos.|
|*Historial*|días\*(elementos/frecuencia de actualización)\*24\*3600\*bytes<br>elementos: número de elementos<br>días: número de días para mantener el historial<br>frecuencia de actualización: actualización promedio tasa de elementos<br>bytes: número de bytes necesarios para mantener un valor único, depende del motor de la base de datos, normalmente \~90 bytes.|
|*Tendencias*|days\*(items/3600)\*24\*3600\*bytes<br>items : cantidad de elementos<br>days : cantidad de días para mantener el historial<br>bytes : cantidad de bytes necesarios para mantener una sola tendencia, depende del motor de la base de datos, normalmente \~90 bytes.|
|*Eventos*|días\*eventos\*24\*3600\*bytes<br>eventos : número de eventos por segundo. Un (1) evento por segundo en el peor de los casos.<br>días: número de días para mantener el historial<br>bytes: número de bytes necesarios para mantener una sola tendencia, depende del motor de la base de datos, normalmente \~330 + promedio número de etiquetas por evento \* 100 bytes.|

Entonces, el espacio en disco total requerido se puede calcular como:\
**Configuración + Historial + Tendencias + Eventos**\
El espacio en disco NO se usará inmediatamente después de la instalación de Zabbix.
El tamaño de la base de datos crecerá y luego dejará de crecer en algún momento, lo que
depende de la configuración del ama de llaves.

[comment]: # ({/afe2e43c-1d73b238})

[comment]: # ({2c5b6201-520ea0fa})
#### Sincronización de tiempo

Es muy importante tener la hora precisa del sistema en el servidor con
Zabbix corriendo. [ntpd](http://www.ntp.org/) es el demonio más popular
que sincroniza la hora del equipo con la hora de otras máquinas.
Se recomienda encarecidamente mantener la hora del sistema sincronizada en todos los componentes de Zabbix que se están ejecutando.

[comment]: # ({/2c5b6201-520ea0fa})

[comment]: # ({new-9e23ba76})

#### Network requirements

A following list of open ports per component is applicable for default configuration.


|Port|Components |
|------------|---------------|
|Frontend|http on 80, https on 443|
|Server|10051 (for use with active proxy/agents)|
|Active Proxy |10051 ( not sure about fetching config)|
|Passive Proxy|10051|
|Agent2|10050|
|Trapper| |
|JavaGateway|10053|
|WebService|10053|

::: noteclassic
In case the configuration is completed, the port numbers should be edited accordingly to prevent firewall from blocking communications with these ports.Outgoing TCP connections usually do not require explicit firewall settings.
::: 

[comment]: # ({/new-9e23ba76})
