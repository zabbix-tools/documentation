[comment]: # translation:outdated

[comment]: # ({1043cd10-bfa3f768})
Instalación del agente de Mac OS desde PKG

[comment]: # ({/1043cd10-bfa3f768})

[comment]: # ({daeb8c7e-5eec6c30})
#### Visión general

El agente de Zabbix para Mac OS se puede instalar desde los paquetes de instalación PKG
disponibles para [descargar](https://www.zabbix.com/download_agents#tab:44).

Hay disponibles versiones con o sin encriptación.

[comment]: # ({/daeb8c7e-5eec6c30})

[comment]: # ({865c1bec-3688af62})
#### Agente instalador

El agente puede instalarse mediante la interfaz gráfica de usuario o desde la línea de comandos, por ejemplo: 

 · sudo installer -pkg zabbix_agent-5.4.0-macos-amd64-openssl.pkg -target /

Asegúrese de utilizar la versión correcta del paquete de Zabbix en el comando. Debe coincidir con el nombre del paquete descargado.

[comment]: # ({/865c1bec-3688af62})

[comment]: # ({8182b831-052e551c})
#### Ejecución Agente

El agente se iniciará automáticamente tras la instalación o el reinicio.

Puede editar el archivo de configuración en
`/usr/local/etc/zabbix/zabbix_agentd.conf` si es necesario.

Para iniciar el agente manualmente, puede ejecutar:

 · sudo launchctl start com.zabbix.zabbix_agentd

Para detener el agente manualmente:

 · sudo launchctl stop com.zabbix.zabbix_agentd

Durante la actualización, el archivo de configuración existente no se sobrescribe. En su lugar se crea un nuevo archivo `zabbix_agentd.conf.NEW` que se utilizará para revisar y actualizar el archivo de configuración existente, si es necesario.
Recuerde reiniciar el agente después de realizar cambios manuales en el fichero de configuración archivo.

[comment]: # ({/8182b831-052e551c})

[comment]: # ({812c9de1-3150eaf4})
#### Solución de problemas y eliminación de agentes

Esta sección enumera algunos comandos útiles que se pueden utilizar para solucionar problemas y eliminar la instalación del agente de Zabbix. 
Para ver si el agente Zabbix se está ejecutando:

 · ps aux | grep zabbix_agentd

Ver si el agente Zabbix ha sido instalado desde los paquetes:

 · $ pkgutil --pkgs | grep zabbix
 · com.zabbix.pkg.ZabbixAgent

Vea los archivos que se instalaron desde el paquete de instalación (tenga en cuenta que la inicial `/` no se muestra en esta vista):
 · $ pkgutil --only-files --files com.zabbix.pkg.ZabbixAgent
 · Library/LaunchDaemons/com.zabbix.zabbix_agentd.plist · 
 · usr/local/bin/zabbix_get · 
 · usr/local/bin/zabbix_sender · 
 · usr/local/etc/zabbix/zabbix_agentd/userparameter_examples.conf.NEW · 
 · usr/local/etc/zabbix/zabbix_agentd/userparameter_mysql.conf.NEW · 
 · usr/local/etc/zabbix/zabbix_agentd.conf.NEW · 
 · usr/local/sbin/zabbix_agentd

Detenga el agente Zabbix si fue lanzado con `launchctl`
 · sudo launchctl unload /Library/LaunchDaemons/com.zabbix.zabbix_agentd.plist

Elimina los archivos (incluyendo la configuración y los registros) que fueron instalados con paquete de instalación:
 · sudo rm -f /Library/LaunchDaemons/com.zabbix.zabbix_agentd.plist
 · sudo rm -f /usr/local/sbin/zabbix_agentd
 · sudo rm -f /usr/local/bin/zabbix_get
 · sudo rm -f /usr/local/bin/zabbix_sender
 · sudo rm -rf /usr/local/etc/zabbix
 · sudo rm -rf /var/log/zabbix

Olvida que el agente Zabbix ha sido instalado:
- sudo pkgutil --forget com.zabbix.pkg.ZabbixAgent

[comment]: # ({/812c9de1-3150eaf4})
