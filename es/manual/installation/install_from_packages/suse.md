[comment]: # translation:outdated

[comment]: # ({116f4d6c-51abb0f6})
# 3 SUSE Servidor empresarial Linux

[comment]: # ({/116f4d6c-51abb0f6})

[comment]: # ({new-ee73b6da})
### Visión General

Paquetes oficiales de Zabbix están disponibles para:

|   |   |
|---|---|
|SUSE Linux Enterprise Server 15|[Descarga](https://www.zabbix.com/download?zabbix=6.0&os_distribution=suse_linux_enterprise_server&os_version=15&db=mysql)|
|SUSE Linux Enterprise Server 12|[Descarga](https://www.zabbix.com/download?zabbix=6.0&os_distribution=suse_linux_enterprise_server&os_version=12&db=mysql)|

::: nota importante
Zabbix 6.0 aún no ha sido lanzado. Los enlaces de descarga conducen a paquetes anteriores a la versión 6.0.
:::

::: nota clássica
 *Verify CA* [modo cifrado](/manual/appendix/install/db_encrypt/mysql) no trabaja en SLES 12 (todas las versiones menores del sistema operativo) con MySQL debido a las bibliotecas de MySQL más antiguas.
:::

[comment]: # ({/new-ee73b6da})

[comment]: # ({new-37fbf2db})
### Añadiendo repositorio de Zabbix

Insralar el paquete de configuración del repositorio. Este paquete contiene archivos de configuración de yum (gestor de paquetes de software).
SLES 15:

    # rpm -Uvh --nosignature https://repo.zabbix.com/zabbix/6.0/sles/15/x86_64/zabbix-release-6.0-1.sles15.noarch.rpm
    # zypper --gpg-auto-import-keys refresh 'Zabbix Official Repository' 

SLES 12:

    # rpm -Uvh --nosignature https://repo.zabbix.com/zabbix/6.0/sles/12/x86_64/zabbix-release-6.0-1.sles12.noarch.rpm
    # zypper --gpg-auto-import-keys refresh 'Zabbix Official Repository' 

Por favor anota, el proceso de servicio web de Zabbix, que es usado para [generación de informe programado](/manual/web_interface/frontend_sections/reports/scheduled),
requiere navegador Google Chrome. El navegador no está incluido en los paquetes y ha sido instalado manualmente.

[comment]: # ({/new-37fbf2db})

[comment]: # ({76a84811-ef4f3e50})
### Servidor/frontend/instalación del agente

Para instalar servidor Zabbix /frontend/agente con soporte MySQL:

    # zypper install zabbix-server-mysql zabbix-web-mysql zabbix-apache-conf zabbix-agent

Sustituye 'apache' en el comando con 'nginx' si usas el paquete para Nginx servidor web. Consulta también: [Configuración Nginx para Zabbix en SLES 12/15](/manual/appendix/install/nginx).

Sustituye 'zabbix-agent' por 'zabbix-agent2' en esos comandos si vas a usar Zabbix agent 2 (solo SLES 15 SP1+).

Para instalar el proxy de Zabbix con soporte MySQL:

    # zypper install zabbix-proxy-mysql

Sustituye 'mysql' en los comandos con 'pgsql' para usar PostgreSQL.

[comment]: # ({/76a84811-ef4f3e50})

[comment]: # ({dabf0716-c573e862})
#### Creando base de datos

Para Zabbix [servidor](/manual/concepts/server) y [proxy](/manual/concepts/proxy) demonios, una base de datos es requerida. 
No es necesaria para arrancar Zabbix [agente](/manual/concepts/agent).

::: nota de aviso
Se necesitan bases de datos separadas para el servidor Zabbix y el
Zabbix proxy; no pueden utilizar la misma base de datos. Por lo tanto, si están
instalados en el mismo host, sus bases de datos deben ser creadas con
nombres diferentes!
:::

Cree la base de datos utilizando las instrucciones proporcionadas para
[MySQL](/manual/appendix/install/db_scripts#mysql) o
[PostgreSQL](/manual/appendix/install/db_scripts#postgresql).

[comment]: # ({/dabf0716-c573e862})

[comment]: # ({new-35bc057d})
#### Importing data

Now import initial schema and data for the **server** with MySQL:

    # zcat /usr/share/doc/packages/zabbix-sql-scripts/mysql/create.sql.gz | mysql -uzabbix -p zabbix

You will be prompted to enter your newly created database password.

With PostgreSQL:

    # zcat /usr/share/doc/packages/zabbix-sql-scripts/postgresql/create.sql.gz | sudo -u zabbix psql zabbix

With TimescaleDB, in addition to the previous command, also run:

    # zcat /usr/share/doc/packages/zabbix-sql-scripts/postgresql/timescaledb.sql.gz | sudo -u <username> psql zabbix

::: notewarning
TimescaleDB is supported with Zabbix server
only.
:::

For proxy, import initial schema:

    # zcat /usr/share/doc/packages/zabbix-sql-scripts/mysql/schema.sql.gz | mysql -uzabbix -p zabbix

For proxy with PostgreSQL:

    # zcat /usr/share/doc/packages/zabbix-sql-scripts/postgresql/schema.sql.gz | sudo -u zabbix psql zabbix

[comment]: # ({/new-35bc057d})

[comment]: # ({new-0ea127cd})
#### Configure database for Zabbix server/proxy

Edit /etc/zabbix/zabbix\_server.conf (and zabbix\_proxy.conf) to use
their respective databases. For example:

    # vi /etc/zabbix/zabbix_server.conf
    DBHost=localhost
    DBName=zabbix
    DBUser=zabbix
    DBPassword=<password>

In DBPassword use Zabbix database password for MySQL; PosgreSQL user
password for PosgreSQL.

Use `DBHost=` with PostgreSQL. You might want to keep the default
setting `DBHost=localhost` (or an IP address), but this would make
PostgreSQL use a network socket for connecting to Zabbix.

[comment]: # ({/new-0ea127cd})

[comment]: # ({new-d162545b})
#### Zabbix frontend configuration

Depending on the web server used (Apache/Nginx) edit the corresponding
configuration file for Zabbix frontend:

-   For Apache the configuration file is located in
    `/etc/apache2/conf.d/zabbix.conf`. Some PHP settings are already
    configured. But it's necessary to uncomment the "date.timezone"
    setting and [set the right
    timezone](http://php.net/manual/en/timezones.php) for you.

```{=html}
<!-- -->
```
    php_value max_execution_time 300
    php_value memory_limit 128M
    php_value post_max_size 16M
    php_value upload_max_filesize 2M
    php_value max_input_time 300
    php_value max_input_vars 10000
    php_value always_populate_raw_post_data -1
    # php_value date.timezone Europe/Riga

-   The zabbix-nginx-conf package installs a separate Nginx server for
    Zabbix frontend. Its configuration file is located in
    `/etc/nginx/conf.d/zabbix.conf`. For Zabbix frontend to work, it's
    necessary to uncomment and set `listen` and/or `server_name`
    directives.

```{=html}
<!-- -->
```
    # listen 80;
    # server_name example.com;

-   Zabbix uses its own dedicated php-fpm connection pool with Nginx:

Its configuration file is located in
`/etc/php7/fpm/php-fpm.d/zabbix.conf`. Some PHP settings are already
configured. But it's necessary to set the right
[date.timezone](http://php.net/manual/en/timezones.php) setting for you.

    php_value[max_execution_time] = 300
    php_value[memory_limit] = 128M
    php_value[post_max_size] = 16M
    php_value[upload_max_filesize] = 2M
    php_value[max_input_time] = 300
    php_value[max_input_vars] = 10000
    ; php_value[date.timezone] = Europe/Riga

Now you are ready to proceed with [frontend installation
steps](/manual/installation/install#installing_frontend) which will
allow you to access your newly installed Zabbix.

Note that a Zabbix proxy does not have a frontend; it communicates with
Zabbix server only.

[comment]: # ({/new-d162545b})

[comment]: # ({new-71fda19d})
#### Starting Zabbix server/agent process

Start Zabbix server and agent processes and make it start at system
boot.

With Apache web server:

    # systemctl restart zabbix-server zabbix-agent apache2 php-fpm
    # systemctl enable zabbix-server zabbix-agent apache2 php-fpm

Substitute 'apache2' with 'nginx' for Nginx web server.

[comment]: # ({/new-71fda19d})

[comment]: # ({new-8e553867})
### Installing debuginfo packages

To enable debuginfo repository edit */etc/zypp/repos.d/zabbix.repo*
file. Change `enabled=0` to `enabled=1` for zabbix-debuginfo repository.

    [zabbix-debuginfo]
    name=Zabbix Official Repository debuginfo
    type=rpm-md
    baseurl=http://repo.zabbix.com/zabbix/4.5/sles/15/x86_64/debuginfo/
    gpgcheck=1
    gpgkey=http://repo.zabbix.com/zabbix/4.5/sles/15/x86_64/debuginfo/repodata/repomd.xml.key
    enabled=0
    update=1

This will allow you to install zabbix-***<component>***-debuginfo
packages.

[comment]: # ({/new-8e553867})
