[comment]: # translation:outdated

[comment]: # ({new-d96d3275})
# 2 Debian/Ubuntu/Raspbian

[comment]: # ({/new-d96d3275})

[comment]: # ({new-9df568e9})
Paquetes oficiales de Zabbix están disponibles para:

|   |   |
|---|---|
|Debian 10 (Buster)|[Descarga](https://www.zabbix.com/download?zabbix=5.4&os_distribution=debian&os_version=10_buster&db=mysql)|
|Debian 9 (Stretch)|[Descarga](https://www.zabbix.com/download?zabbix=5.4&os_distribution=debian&os_version=9_stretch&db=mysql)|
|Debian 8 (Jessie)|[Descarga](https://www.zabbix.com/download?zabbix=5.4&os_distribution=debian&os_version=8_jessie&db=mysql)|
|Ubuntu 20.04 (Focal Fossa) LTS|[Descarga](https://www.zabbix.com/download?zabbix=5.4&os_distribution=ubuntu&os_version=20.04_focal&db=mysql)|
|Ubuntu 18.04 (Bionic Beaver) LTS|[Descarga](https://www.zabbix.com/download?zabbix=5.4&os_distribution=ubuntu&os_version=18.04_bionic&db=mysql)|
|Ubuntu 16.04 (Xenial Xerus) LTS|[Descarga](https://www.zabbix.com/download?zabbix=5.4&os_distribution=ubuntu&os_version=16.04_xenial&db=mysql)|
|Ubuntu 14.04 (Trusty Tahr) LTS|[Descarga](https://www.zabbix.com/download?zabbix=5.4&os_distribution=ubuntu&os_version=14.04_trusty&db=mysql)|
|Raspbian (Buster)|[Descarga](https://www.zabbix.com/download?zabbix=5.4&os_distribution=raspbian&os_version=10_buster&db=mysql)|
|Raspbian (Stretch)|[Descarga](https://www.zabbix.com/download?zabbix=5.4&os_distribution=raspbian&os_version=9_stretch&db=mysql)|

Los paquetes están disponibles con cualquier MySQL/PostgreSQL base de datos y
Apache/Nginx páginas web.

::: nota importante
Zabbix 5.4 todavía no está emitido. El link de descarga conduce a pre-5.4 paquetes.
:::


[comment]: # ({/new-9df568e9})

[comment]: # ({new-ae631a63})
### Notas en la instalación

Mira las [instrucciones de instalación](https://www.zabbix.com/download?zabbix=5.0&os_distribution=debian&os_version=10_buster&db=mysql)
por plataforma en la página de descargas para:

- instalar el repositorio
- instalar el servidor/agente/frontend
- crear la base de datos inicial, importar los datos iniciales
- configurar la base de datos para el servidor Zabbix
- configurar PHP para el frontend de Zabbix
- iniciar los procesos del servidor/agente
- configurando el frontend de Zabbix

Si quieres ejecutar el agente de Zabbix como root, consulta [ejecutar el agente como
root](manual/appendix/install/run_agent_as_root).

El proceso de servicio web de Zabbix, que se utiliza para la [generación programada de informes
(/manual/web_interface/frontend_sections/reports/scheduled),
requiere el navegador Google Chrome. El navegador no está incluido en
paquetes y tiene que ser instalado manualmente.

[comment]: # ({/new-ae631a63})

[comment]: # ({new-99a402fa})
#### Importando datos con Timescale DB

Con TimescaleDB, además del comando de importación para PostgreSQL, también ejecutar:

    # zcat /usr/share/doc/zabbix-sql-scripts/postgresql/timescaledb.sql.gz | sudo -u zabbix psql zabbix

::: aviso de advertencia
TimescaleDB es soportado solo con Zabbix server.
:::

[comment]: # ({/new-99a402fa})


[comment]: # ({2993fccc-fa73411a})
#### SELinux configuración

Mira [SELinux configuración](/manual/installation/install_from_packages/rhel_centos#selinux_configuration)
for RHEL/CentOS.

Después del frontend y SELinux configuración está hecha, reinicia el servidor de Apache:

    # service apache2 restart

[comment]: # ({/2993fccc-fa73411a})

[comment]: # ({00739d13-a12da003})
### Proxy installation

Una vez que el repositorio requerido esté añadido, tú puedes instalarel proxy de Zabbix ejecutando:

    # apt install zabbix-proxy-mysql

Sustituye 'mysql' en el comando por 'pgsql' para usar PostgreSQL, o cambia por 'sqlite3' para usar SQLite3.

[comment]: # ({/00739d13-a12da003})

[comment]: # ({46b80620-fe6abb8e})
##### Creando una base de datos

[Crear](/manual/appendix/install/db_scripts) una base de datos separada para Zabbix proxy.

El servidor Zabbix y el proxy Zabbix no pueden utilizar la misma base de datos. Si están
instalados en el mismo host, la base de datos del proxy debe tener un nombre
nombre.

[comment]: # ({/46b80620-fe6abb8e})

[comment]: # ({new-2ab835d7})
##### Importando datos

Importar diagrama inicial:

    # zcat /usr/share/doc/zabbix-sql-scripts/mysql/schema.sql.gz | mysql -uzabbix -p zabbix

Para el proxy con PostgreSQL (o SQLite):

    # zcat /usr/share/doc/zabbix-sql-scripts/postgresql/schema.sql.gz | sudo -u zabbix psql zabbix
    # zcat /usr/share/doc/zabbix-sql-scripts/sqlite3/schema.sql.gz | sqlite3 zabbix.db

[comment]: # ({/new-2ab835d7})

[comment]: # ({e290f1f2-d0a225c7})
##### Configurar la base de datos para el proxy de Zabbix

Editar zabbix\_proxy.conf:

    # vi /etc/zabbix/zabbix_proxy.conf
    DBHost=localhost
    DBName=zabbix
    DBUser=zabbix
    DBPassword=<password>

En DBName para el proxy de Zabbix utilice una base de datos separada del servidor de Zabbix.

En DBPassword use la contraseña de la base de datos Zabbix para MySQL; el usuario y contraseña PosgreSQL para PosgreSQL.

Utilice `DBHost=` con PostgreSQL. Es posible que desee mantener la configuración por defecto
`DBHost=localhost` (o una dirección IP), pero esto haría que
PostgreSQL utilice un socket de red para conectarse a Zabbix. Consulte la
[sección respectiva](/manual/installation/install_from_packages/rhel_centos#selinux_configuration)
para RHEL/CentOS para obtener instrucciones.

[comment]: # ({/e290f1f2-d0a225c7})

[comment]: # ({26b4ab3d-27de2ced})
##### Iniciando un proceso del proxy de Zabbix

Para iniciar un proceso proxy de Zabbix y hacer que se inicie en el arranque del sistema:

    # systemctl restart zabbix-proxy
    # systemctl enable zabbix-proxy

[comment]: # ({/26b4ab3d-27de2ced})

[comment]: # ({db7a5dd1-871a973b})
##### Configuración del frontend

Un proxy de Zabbix no tiene un frontend; se comunica con el servidoir de Zabbix
solamente.

[comment]: # ({/db7a5dd1-871a973b})

[comment]: # ({fc34696a-cd9340bd})
### Instalación del Java gateway

Es necesario instalar [Java gateway](/manual/concepts/java) sólo si
desea supervisar las aplicaciones JMX. Java Gateway es ligero y
no requiere base de datos.

Una vez añadido el repositorio requerido, puede instalar Zabbix Java
gateway ejecutando:

    # apt install zabbix-java-gateway

Proceda a [setup](/manual/concepts/java/from_debian_ubuntu) para más
detalles sobre la configuración y ejecución de la puerta de enlace de Java.

[comment]: # ({/fc34696a-cd9340bd})
