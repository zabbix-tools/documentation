[comment]: # translation:outdated

[comment]: # ({39663bb4-9cceae9f})
# 6 Instalación de la interfaz web

Esta sección proporciona instrucciones paso a paso para instalar la interfaz web de Zabbix. El frontend de Zabbix está escrito en PHP, por lo que para ejecutarlo se necesita un servidor web que soporte PHP.

[comment]: # ({/39663bb4-9cceae9f})

[comment]: # ({new-0c649c0e})
:::notetip
You can find out more about setting up SSL for Zabbix frontend by referring to these [best practices](/manual/installation/requirements/best_practices).
:::

[comment]: # ({/new-0c649c0e})

[comment]: # ({b66c228e-6b173526})
#### Pantalla de bienvenida

Abre la URL del frontend de Zabbix en el navegador. Si has instalado Zabbix desde los paquetes, la URL es:

- · Para Apache: *http://<server\_ip\_or\_name>/zabbix*
- · Para Nginx: *http://<server\_ip\_or\_name>*

Debería ver la primera pantalla del asistente de instalación del frontend.
Utilice el menú desplegable *Idioma por defecto* para cambiar el idioma por defecto del sistema y continua el proceso de instalación en el idioma seleccionado (opcional). Para más información, consulte [Instalación de idiomas adicionales idiomas del frontend](/manual/appendix/install/locales).

![](../../../assets/en/manual/installation/install_1.png){width="550"}

[comment]: # ({/b66c228e-6b173526})

[comment]: # ({2eed9cdb-2458c643})
#### Comprobación de los requisitos previos


Asegúrese de que se cumplen todos los requisitos previos del software.

![](../../../assets/en/manual/installation/install_2.png){width="550"}

|Prerequisitos| Valor Mínimo | Descripción|
|-------------|-------------|-----------|
|*PHP version*|7.2.5|<|
|*PHP memory\_limit option*|128MB|In php.ini:<br>memory\_limit = 128M|
|*PHP post\_max\_size option*|16MB|In php.ini:<br>post\_max\_size = 16M|
|*PHP upload\_max\_filesize option*|2MB|In php.ini:<br>upload\_max\_filesize = 2M|
|*PHP max\_execution\_time option*|300 seconds (values 0 and -1 are allowed)|In php.ini:<br>max\_execution\_time = 300|
|*PHP max\_input\_time option*|300 seconds (values 0 and -1 are allowed)|In php.ini:<br>max\_input\_time = 300|
|*PHP session.auto\_start option*|debe estar desactivado|In php.ini:<br>session.auto\_start = 0|
|*Database support*|Uno de: MySQL, Oracle, PostgreSQL.|Uno de los siguientes módulos debe estar instalado:<br>mysql, oci8, pgsql|
|*bcmath*|<|php-bcmath|
|*mbstring*|<|php-mbstring|
|*PHP mbstring.func\_overload option*|debe estar desactivado|In php.ini:<br>mbstring.func\_overload = 0|
|*sockets*|<|php-net-socket.  Necesario para el soporte de scripts de usuario.||*gd*|2.0.28|php-gd. La extensión PHP GD debe soportar la imagen PNG (*--with-png-dir*), JPEG (*--with-jpeg-dir*) images and FreeType 2 (*--with-freetype-dir*).|
|*libxml*|2.6.15|php-xml|
|*xmlwriter*|<|php-xmlwriter|
|*xmlreader*|<|php-xmlreader|
|*ctype*|<|php-ctype|
|*session*|<|php-session|
|*gettext*|<|php-gettext<br>Desde Zabbix 2.2.1, la extensión PHP gettext no es un requisito obligatorio para instalar Zabbix. Si gettext no está instalado, el frontend funcionará como siempre, sin embargo, las traducciones no estarán disponibles.|

Los prerrequisitos opcionales también pueden estar presentes en la lista. Un prerrequisito opcional fallido se muestra en naranja y tiene un estado de *Atención*.
Con un prerrequisito opcional fallido, la configuración puede continuar.

::: notaimportante
Si es necesario cambiar el usuario de Apache o grupo de usuarios, los permisos de la carpeta de sesión deben ser verificados. De lo contrario, la instalación de Zabbix no podrá continuar.
:::

[comment]: # ({/2eed9cdb-2458c643})

[comment]: # ({439b480c-879dc06f})
#### Configurar la conexión a la base de datos

Introduzca los detalles para conectarse a la base de datos. La base de datos Zabbix debe estar ya creada.

![](../../../assets/en/manual/installation/install_3a.png){width="550"}

Si la opción *Encriptación TLS de la base de datos* está marcada, entonces los campos adicionales para [configurar la conexión TLS conexión](/manual/appendix/install/db_encrypt) a la base de datos aparecen en el formulario (sólo en MySQL o PostgreSQL).

Si se selecciona la opción HashiCorp Vault para almacenar las credenciales, hay campos adicionales disponibles para especificar el punto final de la API de Vault, ruta secreta y token de autenticación:

![](../../../assets/en/manual/installation/install_3b.png){width="550"}

[comment]: # ({/439b480c-879dc06f})

[comment]: # ({823d2524-98277238})
#### Configuración

Introducir un nombre para el servidor de Zabbix es opcional, sin embargo, si se envía, éste se mostrará en la barra de menú y en los títulos de las páginas.

Establezca la [zona horaria] por defecto (/manual/web_interface/time_zone#overview) y el tema para el frontend.

![](../../../assets/en/manual/installation/install_4.png){width="550"}

[comment]: # ({/823d2524-98277238})

[comment]: # ({14743934-42398398})
#### Resumen de preinstalación

Revise un resumen de la configuración.

![](../../../assets/en/manual/installation/install_5.png){width="550"}

[comment]: # ({/14743934-42398398})

[comment]: # ({6724c0ec-1124dd9e})
#### Instalar
Si instalas Zabbix desde las fuentes, descarga el archivo de configuración y colóquelo bajo conf/ en el subdirectorio de documentos HTML del servidor web donde copió los archivos PHP de Zabbix.

![](../../../assets/en/manual/installation/install_6.png){width="550"}

![](../../../assets/en/manual/installation/saving_zabbix_conf.png){width="350"}

::: notetip
Siempre que el usuario del servidor web tenga acceso de escritura al directorio conf/ el archivo de configuración se guardaría automáticamente y sería posible proceder al siguiente paso de inmediato.
:::

Finalizar la instalación.

![](../../../assets/en/manual/installation/install_7.png){width="550"}

[comment]: # ({/6724c0ec-1124dd9e})

[comment]: # ({82aa1ba3-d59dc4b9})
#### Entrar
¡El frontend de Zabbix está listo! El nombre de usuario por defecto es **Admin**, contraseña **zabbix**.

![](../../../assets/en/manual/quickstart/login.png){width="350"}

Proceda a [getting started with Zabbix](/manual/quickstart/login).

[comment]: # ({/82aa1ba3-d59dc4b9})
