[comment]: # translation:outdated

[comment]: # ({15433e5a-e2c1904c})
#4 Instalación desde paquetes

[comment]: # ({/15433e5a-e2c1904c})

[comment]: # ({6f51b3f2-8d855b7c})
#### Desde el repositorio oficial de Zabbix

Zabbix SIA proporciona paquetes oficiales RPM y DEB para:

- [Red Hat Enterprise Linux/CentOS](/manual/installation/install_from_packages/rhel_centos)
- [Debian/Ubuntu/Raspbian](/manual/installation/install_from_packages/debian_ubuntu)
- [SUSE Linux Enterprise Server](/manual/installation/install_from_packages/suse)

Los archivos de paquete para repositorios yum/dnf, apt y zypper para las diferentes distribuciones de SO están disponibles en
[repo.zabbix.com](https://repo.zabbix.com/).

Tenga en cuenta que, aunque algunas distribuciones (en particular, las distribuciones basadas en Debian)
proporcionan sus propios paquetes de Zabbix, estos paquetes no son soportados por Zabbix. Los paquetes de Zabbix proporcionados por terceros pueden estar descontinuados y carecer de las últimas funciones y correcciones de errores. Se recomienda usar solo paquetes oficiales de [repo.zabbix.com](https://repo.zabbix.com/). Si ha utilizado anteriormente paquetes no oficiales de Zabbix, consulte las notas sobre [actualización de paquetes de Zabbix desde repositorios](/manual/installation/upgrade/packages#zabbix_packages_from_os_repositories).

[comment]: # ({/6f51b3f2-8d855b7c})
