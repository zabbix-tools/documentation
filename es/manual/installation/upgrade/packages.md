[comment]: # translation:outdated

[comment]: # ({8567c709-490a37c2})
# Actualizar desde los paquetes

[comment]: # ({/8567c709-490a37c2})

[comment]: # ({86492c71-0fa07e9f})
#### Visión general

Esta sección proporciona los pasos necesarios para una exitosa [actualización](/manual/installation/upgrade) usando paquetes oficiales RPM y DEB proporcionados por Zabbix para:

- · [Red Hat Enterprise
 · Linux/CentOS](/manual/installation/upgrade/packages/rhel_centos)
- · [Debian/Ubuntu](/manual/installation/upgrade/packages/debian_ubuntu)

[comment]: # ({/86492c71-0fa07e9f})

[comment]: # ({3f48b6a5-69a2013d})
##### paquetes Zabbix desde los repositorios del sistema operativo

A menudo, las distribuciones del sistema operativo (en particular, las distribuciones basadas en Debian) proporcionan sus propios paquetes Zabbix. Tenga en cuenta que estos paquetes no están soportados por Zabbix, están normalmente desactualizados y carecen de las últimas características y correcciones de errores. Sólo los paquetes de [repo.zabbix.com](https://repo.zabbix.com/) están soportados oficialmente.
Si está actualizando los paquetes proporcionados por las distribuciones del sistema operativo (o los tenía instalados en algún momento), siga este procedimiento para cambiar a paquetes oficiales de Zabbix:

1. · Desinstala siempre primero los paquetes antiguos.
2. ·  Compruebe si hay archivos residuales que puedan haber quedado después de la desinstalación.  
3. · Instale los paquetes oficiales siguiendo las [instrucciones de instalación    
 · instrucciones](https://www.zabbix.com/download) proporcionadas por Zabbix.

Nunca haga una actualización directa, ya que esto puede resultar en una instalación rota.

[comment]: # ({/3f48b6a5-69a2013d})
