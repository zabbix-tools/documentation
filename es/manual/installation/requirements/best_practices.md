[comment]: # translation:outdated

[comment]: # ({215edb47-7ef4b2b9})
#Mejores prácticas para una configuración segura de Zabbix

[comment]: # ({/215edb47-7ef4b2b9})

[comment]: # ({260de232-b9719f19})
#### Visión general

Esta sección contiene las mejores prácticas que deben ser observadas para configurar Zabbix de forma segura.

Las prácticas aquí contenidas no son necesarias para el funcionamiento de Zabbix. Se recomiendan para una mejor seguridad del sistema.

[comment]: # ({/260de232-b9719f19})

[comment]: # ({new-cebf8306})
### Access control

[comment]: # ({/new-cebf8306})

[comment]: # ({af6dd258-dc572a02})
#### Principio del menor privilegio

El principio de mínimo privilegio debe ser utilizado en todo momento para Zabbix.
Este principio significa que las cuentas de usuario (en el frontend de Zabbix) o proceso de usuario (para el servidor/proxy o agente de Zabbix) tienen sólo aquellos privilegios que son esenciales para realizar las funciones previstas. En otras palabras, las cuentas de usuario cuentas de usuario en todo momento deben ejecutarse con el menor número de privilegios posible.

::: notaimportante
Dando permisos extra al usuario 'zabbix' se permitirle acceder a los archivos de configuración y ejecutar operaciones que pueden comprometer la seguridad general de la infraestructura.
:::
Al implementar el principio de mínimo privilegio para las cuentas de usuario, Zabbix [frontend user types](/manual/config/users_and_usergroups/permissions) debe tenerse en cuenta. Es importante entender que mientras un usuario "Admin" tiene menos privilegios que el tipo de usuario "Super Admin", tiene permisos administrativos que permiten gestionar la configuración y ejecutar scripts personalizados.

::: notaclásica
Cierta información está disponible incluso para los usuarios sin privilegios. Por ejemplo, mientras que *Administración* → *Scripts* no está disponible para los que no son superadministradores, los propios scripts están disponibles para ser recuperados por utilizando la API de Zabbix. Limitar los permisos de los scripts y no añadir información sensible (como credenciales de acceso, etc.) debe ser utilizado para evitar la exposición de la información sensible disponible en los scripts globales.
:::

[comment]: # ({/af6dd258-dc572a02})

[comment]: # ({1b0c5732-1631be73})
#### Usuario seguro para el agente Zabbix

En la configuración por defecto, el servidor Zabbix y los procesos del agente Zabbix comparten un usuario 'zabbix'. Si desea asegurarse de que el agente no puede acceder a detalles sensibles en la configuración del servidor (por ejemplo, información de inicio de sesión de la base de datos base de datos), el agente debe ejecutarse como un usuario diferente:

1. - Cree un usuario seguro
2. - Especifique este usuario en el fichero [configuration
 - archivo](/manual/apéndice/config/zabbix_agentd) (parámetro 'User')
3. - Reinicie el agente con privilegios de administrador. Los privilegios serán
 - se le quitarán al usuario especificado.

[comment]: # ({/1b0c5732-1631be73})

[comment]: # ({c220fb21-40e70f9e})
#### codificación UTF-8

UTF-8 es la única codificación soportada por Zabbix. Se sabe que funciona sin ningún fallo de seguridad. Los usuarios deben ser conscientes de que hay problemas de seguridad si se utilizan algunas de las otras codificaciones.

[comment]: # ({/c220fb21-40e70f9e})

[comment]: # ({new-62eeb2ec})
### Windows installer paths
When using Windows installers, it is recommended to use default paths provided by the installer as using custom paths without proper permissions could compromise the security of the installation.

[comment]: # ({/new-62eeb2ec})

[comment]: # ({5cda7d37-f8f5054e})
#### Configuración de SSL para el frontend de Zabbix

En RHEL/Centos, instale el paquete mod\_ssl:

  · yum install mod_ssl

Cree un directorio para las claves SSL:

 · mkdir -p /etc/httpd/ssl/private
 · chmod 700 /etc/httpd/ssl/private

Crear certificado SSL:

 · openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/httpd/ssl/private/apache-selfsigned.key -out /etc/httpd/ssl/apache-selfsigned.crt

Rellene las indicaciones de forma adecuada. La línea más importante es la que que solicita el nombre común. Necesita introducir el nombre de dominio que quieres que se asocie a tu servidor. Puede introducir la dirección IP pública si no tiene un dominio. Utilizaremos *ejemplo.com* en este artículo.

 · Nombre del país (código de 2 letras) [XX]:
 · Nombre del Estado o Provincia (nombre completo) []:
 · Nombre de la localidad (por ejemplo, ciudad) [Ciudad por defecto]:
 · Nombre de la organización (p. ej., empresa) [Empresa por defecto]:
 · Nombre de la unidad organizativa (por ejemplo, sección) []:
 · Nombre común (por ejemplo, su nombre o el nombre de host de su servidor) []:ejemplo.com
 · Dirección de correo electrónico []:

Editar la configuración de Apache SSL:

 · /etc/httpd/conf.d/ssl.conf

 · DocumentRoot "/usr/share/zabbix"
 · ServerName example.com:443
 · SSLCertificateFile /etc/httpd/ssl/apache-selfsigned.crt
 · SSLCertificateKeyFile /etc/httpd/ssl/private/apache-selfsigned.key

Reinicie el servicio Apache para aplicar los cambios:

· systemctl restart httpd.service

[comment]: # ({/5cda7d37-f8f5054e})

[comment]: # ({new-40cfdec2})
### Web server hardening

[comment]: # ({/new-40cfdec2})

[comment]: # ({5c0334ed-bb3706f4})
#### Habilitar Zabbix en el directorio raíz de la URL

Añadir un host virtual a la configuración de Apache y establecer una redirección permanente para la raíz del documento a la URL de Zabbix SSL. No olvide reemplazar *ejemplo.com* por el nombre real del servidor.

 · /etc/httpd/conf/httpd.conf

 · #Añadir líneas#

 · <VirtualHost *:*>
 · ServerName example.com
 · Redirección permanente / https://example.com
 · <VirtualHost>

Reinicie el servicio Apache para aplicar los cambios:

 · systemctl restart httpd.service

[comment]: # ({/5c0334ed-bb3706f4})

[comment]: # ({12a9cccf-c7ee0bb2})
#### Habilitación de HTTP Strict Transport Security (HSTS) en el servidor web

Para proteger el frontend de Zabbix contra ataques de bajada de protocolo, nosotros recomendamos habilitar [HSTS](https://en.wikipedia.org/wiki/HTTP_Strict_Transport_Security) política en el servidor web.

Por ejemplo, para habilitar la política HSTS para su frontend Zabbix en Apache configuración:

 · /etc/httpd/conf/httpd.conf

añada la siguiente directiva a la configuración de su host virtual:

 · <Host virtual *:443>
 · Header set Strict-Transport-Security "max-age=31536000"
 · <VirtualHost>

Reinicie el servicio Apache para aplicar los cambios:

· systemctl restart httpd.service

[comment]: # ({/12a9cccf-c7ee0bb2})

[comment]: # ({59be4cf5-cd09dcd1})
#### Desactivar la exposición de la información del servidor web

Se recomienda deshabilitar todas las firmas del servidor web como parte del proceso de endurecimiento del servidor web. El servidor web expone el software firma por defecto:

![](../../../../assets/en/manual/installation/requirements/software_signature.png)

La firma puede desactivarse añadiendo dos líneas al archivo de configuración de Apache (utilizado como ejemplo) archivo de configuración:

 · ServerSignature Off
 · ServerTokens Prod

La firma de PHP (cabecera HTTP X-Powered-By) puede ser desactivada cambiando el archivo de configuración de archivo de configuración php.ini (la firma está desactivada por defecto):

 · expose_php = Off

Es necesario reiniciar el servidor web para que los cambios en el archivo de configuración se apliquen.

Se puede conseguir un nivel de seguridad adicional utilizando el mod\_security (paquete libapache2-mod-security2) con Apache. mod_security permite eliminar la firma del servidor en lugar de sólo eliminar la versión del servidor. La firma puede ser alterada a cualquier valor cambiando "SecServerSignature" a cualquier valor deseado después de instalar mod_security.

Por favor, consulte la documentación de su servidor web para encontrar ayuda sobre cómo eliminar/cambiar las firmas de software.

[comment]: # ({/59be4cf5-cd09dcd1})

[comment]: # ({53825f68-720052da})
#### Desactivar las páginas de error del servidor web por defecto

Se recomienda deshabilitar las páginas de error por defecto para evitar la exposición de la información exposición. El servidor web utiliza páginas de error incorporadas por defecto:

![](../../../../assets/en/manual/installation/requirements/error_page_text.png)

Las páginas de error por defecto deben ser reemplazadas/eliminadas como parte del proceso de endurecimiento. La directiva "ErrorDocument" puede utilizarse para definir una página/texto de error personalizado para el servidor web Apache (utilizado como ejemplo).

Por favor, consulte la documentación de su servidor web para encontrar ayuda sobre cómo reemplazar/eliminar las páginas de error por defecto.

[comment]: # ({/53825f68-720052da})

[comment]: # ({0ce7e5d1-ba1547c0})
#### Eliminación de la página de prueba del servidor web

Se recomienda eliminar la página de prueba del servidor web para evitar la exposición de la información. Por defecto, el servidor webroot contiene una página de prueba de prueba llamada index.html (Apache2 en Ubuntu se utiliza como ejemplo):

![](../../../../assets/en/manual/installation/requirements/test_page.png)

La página de prueba debe ser eliminada o no debe estar disponible como parte del proceso de endurecimiento del servidor web.

[comment]: # ({/0ce7e5d1-ba1547c0})

[comment]: # ({6fb04d18-7e4e4a45})
#### Configuración de Zabbix

Por defecto, Zabbix está configurado con la cabecera *X-Frame-Options HTTP de respuesta HTTP* a `SAMEORIGIN`, lo que significa que el contenido sólo puede ser cargado en un marco que tenga el mismo origen que la propia página.

Los elementos del frontend de Zabbix que extraen contenido de URLs externas (concretamente, la URL [dashboard widget](/manual/web_interface/frontend_sections/monitoring/dashboard/widgets#url)) mostrar el contenido recuperado en una sandbox con todas las restricciones del sandboxing activadas.

Estos ajustes mejoran la seguridad del frontend de Zabbix y proporcionan protección contra ataques XSS y clickjacking. Los superadministradores pueden [modificar](/manual/web_interface/frontend_sections/administration/general#security) *iframe sandboxing* y *X-Frame-Options HTTP response header* según sea necesario. Por favor, sopese cuidadosamente los riesgos y beneficios antes de cambiar la configuración por defecto. No se recomienda desactivar por completo el sandboxing o X-Frame-Options por completo no es recomendable.

[comment]: # ({/6fb04d18-7e4e4a45})

[comment]: # ({3cdf6424-c02c850f})
#### Agente Zabbix Windows con OpenSSL

El agente de Zabbix para Windows compilado con OpenSSL intentará llegar al archivo de configuración de SSL en c:\\enssl-64bit. El directorio "openssl-64bit" en el disco C: puede ser creado por usuarios sin privilegios.

Así que para reforzar la seguridad, es necesario crear este directorio manualmente y revocar el acceso de escritura de los usuarios sin privilegios.

Tenga en cuenta que los nombres de los directorios serán diferentes en las versiones de 32 y 64 bits de Windows.

[comment]: # ({/3cdf6424-c02c850f})

[comment]: # ({new-6cfe5a3b})
### Cryptography

[comment]: # ({/new-6cfe5a3b})

[comment]: # ({new-3f76ae70})
#### Ocultar el archivo con la lista de contraseñas comunes

Para aumentar la complejidad de los ataques de fuerza bruta a las contraseñas, se sugiere limitar el acceso al archivo `ui/data/top_passwords.txt` modificando la configuración del servidor web. Este archivo contiene una lista de las contraseñas más comunes y específicas del contexto, y se utiliza para evitar que los usuarios establezcan dichas contraseñas si el parámetro *Evitar contraseñas fáciles de adivinar está activado en la [política de contraseñas
policy](/manual/web_interface/frontend_sections/administration/authentication#internal_authentication).

Por ejemplo, en NGINX el acceso a los archivos puede limitarse utilizando la directiva `location`

 · location = /data/top_passwords.txt {​​​​​​​
 · deny all;
 · return 404;
 · }​​​​​​​

En Apache - utilizando el archivo `.htacess`:

 · <Files "top_passwords.txt"> · 
 · Order Allow,Deny
 · Deny from all
 · </Files>

[comment]: # ({/new-3f76ae70})

[comment]: # ({new-ba0d4036})

#### Security vulnerabilities

##### CVE-2021-42550

In Zabbix Java gateway with logback version 1.2.7 and prior versions, an attacker with the required 
privileges to edit configuration files could craft a malicious configuration allowing to execute 
arbitrary code loaded from LDAP servers.

Vulnerability to [CVE-2021-42550](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-42550) 
has been fixed since Zabbix 5.4.9. However, as an additional security measure it is recommended 
to check permissions to the `/etc/zabbix/zabbix_java_gateway_logback.xml` file and set it read-only, 
if write permissions are available for the "zabbix" user. 

[comment]: # ({/new-ba0d4036})
