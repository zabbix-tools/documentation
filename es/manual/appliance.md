[comment]: # translation:outdated

[comment]: # ({new-8c911baa})
# 6. Zabbix appliance

[comment]: # ({/new-8c911baa})

[comment]: # ({cf785d63-cfa1a361})
#### Descripción general

Como alternativa a la configuración manual o la reutilización de un servidor existente
para Zabbix, los usuarios pueden
[descargar](http://www.zabbix.com/download_appliance) un dispositivo Zabbix
o una imagen de CD de instalación del dispositivo Zabbix.

Las versiones del CD de instalación y el dispositivo Zabbix se basan en CentOS 8
(x86\_64).

El CD de instalación del dispositivo Zabbix se puede usar para la implementación instantánea de
Servidor Zabbix (MySQL).

::: notaimportante
 Puede usar este dispositivo para evaluar Zabbix.
El dispositivo no está diseñado para un uso serio en producción.
:::

[comment]: # ({/cf785d63-cfa1a361})

[comment]: # ({36c33d32-cffa82a3})
##### Requisitos del sistema:

- *RAM*: 1,5GB
- *Espacio en disco*: se deben asignar al menos 8 GB para la máquina virtual.

Menú de arranque del CD/DVD de instalación de Zabbix:

![](../../assets/en/manual/installation_cd_boot_menu1.png){width="600"}

El dispositivo Zabbix contiene un servidor Zabbix (configurado y ejecutándose en
MySQL) y una interfaz.

El dispositivo virtual Zabbix está disponible en los siguientes formatos:

-VMWare (.vmx)
- Formato de virtualización abierto (.ovf)
-Microsoft Hyper-V 2012 (.vhdx)
-Microsoft Hyper-V 2008 (.vhd)
- KVM, Parallels, QEMU, memoria USB, VirtualBox, Xen (.raw)
-KVM, QEMU (.qcow2)

Para comenzar, inicie el dispositivo y apunte un navegador a la dirección IP
dispositivo ha recibido a través de DHCP.

::: notaimportante
 DHCP debe estar habilitado en el equipo.
:::

Para obtener la dirección IP desde el interior de la máquina virtual, ejecute:

    ip addr show

Para acceder a la interfaz de Zabbix, vaya a **http://<host\_ip>** (para acceder
desde el navegador del host, el modo puente debe estar habilitado en la red de VM
ajustes).

::: nota
Si el dispositivo no se inicia en Hyper-V, puede
desea presionar `Ctrl+Alt+F2` para cambiar de sesión de tty.
:::

[comment]: # ({/36c33d32-cffa82a3})

[comment]: # ({6036d23e-589fd5e2})
#### - Cambios en la configuración de CentOS 8

El dispositivo se basa en CentOS 8. Se han aplicado algunos cambios a
la configuración básica de CentOS.

[comment]: # ({/6036d23e-589fd5e2})

[comment]: # ({4ae5b88e-193d3b23})
##### - Repositorios

El [repositorio] (/manual/installation/install_from_packages/rhel_centos) oficial de Zabbix tiene
agregado a */etc/yum.repos.d*:

[zabbix]
name=Zabbix Official Repository - $basearch
baseurl=http://repo.zabbix.com/zabbix/5.2/rhel/8/$basearch/
enabled=1
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-ZABBIX-A14FE591

[comment]: # ({/4ae5b88e-193d3b23})

[comment]: # ({6e4e7e7b-589dc798})
##### - Configuración del cortafuegos

El dispositivo utiliza el cortafuegos de iptables con reglas predefinidas:

- Puerto SSH abierto (22 TCP);
- Apertura de puertos del agente Zabbix (10050 TCP) y Zabbix trapper (10051 TCP);
- Apertura de puertos HTTP (80 TCP) y HTTPS (443 TCP);
- Puerto de captura SNMP abierto (162 UDP);
- Conexiones salientes abiertas al puerto NTP (53 UDP);
- Paquetes ICMP limitados a 5 paquetes por segundo;
- Todas las demás conexiones entrantes se interrumpen.

[comment]: # ({/6e4e7e7b-589dc798})

[comment]: # ({5d6ff3a6-b2283a9a})
##### - Uso de una dirección IP estática

De forma predeterminada, el dispositivo utiliza DHCP para obtener la dirección IP. Para especificar
una dirección IP estática:

- Inicie sesión como usuario root;
- Abra el archivo */etc/sysconfig/network-scripts/ifcfg-eth0*;
- Reemplace *BOOTPROTO=dhcp* con *BOOTPROTO=none*
- Agregue las siguientes líneas:
    - *IPADDR=<dirección IP del dispositivo>*
    - *PREFIX=<prefijo CIDR>*
    - *GATEWAY=<dirección IP de la puerta de enlace>*
    - *DNS1=<dirección IP del servidor DNS>*
- Ejecute el comando **systemctl restart network**.

Consulte la [documentación](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/deployment_guide/s1-networkscripts-interfaces) oficial de Red Hat
si es necesario.

[comment]: # ({/5d6ff3a6-b2283a9a})

[comment]: # ({44c9fae9-acf06935})
##### - Cambio de zona horaria

De forma predeterminada, el dispositivo utiliza UTC para el reloj del sistema. Para cambiar la
zona horaria, copie el archivo apropiado de */usr/share/zoneinfo* a
*/etc/localtime*, por ejemplo:

    cp /usr/share/zoneinfo/Europa/Riga /etc/localtime

[comment]: # ({/44c9fae9-acf06935})

[comment]: # ({c52a5a2c-2c378c8d})
#### - Configuración de Zabbix

La configuración del dispositivo Zabbix tiene las siguientes contraseñas y cambios de configuración:

[comment]: # ({/c52a5a2c-2c378c8d})

[comment]: # ({97639724-d39b5151})
##### - Credenciales (inicio de sesión: contraseña)

Sistema:

- root: zabbix

Interfaz de Zabbix:

- Admin: zabbix

Base de datos:

- root: <aleatorio>
- zabbix:<aleatorio>

::: notaclásica
Las contraseñas de la base de datos se generan aleatoriamente durante el
proceso de instalación.\
La contraseña de root se almacena dentro del archivo /root/.my.cnf. Esto no es
requerido para ingresar una contraseña en la cuenta "root".
:::

Para cambiar la contraseña de usuario de la base de datos, se deben realizar cambios en el
siguientes lugares:

- mysql;
- /etc/zabbix/zabbix\_server.conf;
- /etc/zabbix/web/zabbix.conf.php.

::: notaclásica
 Se definen usuarios separados `zabbix_srv` y `zabbix_web`
para el servidor y la interfaz respectivamente.
:::

[comment]: # ({/97639724-d39b5151})

[comment]: # ({cb9bb3ee-f11ee379})
##### - Ubicaciones de los archivos

- Los archivos de configuración se encuentran en **/etc/zabbix**.
- Los archivos de registro del servidor, proxy y agente de Zabbix se encuentran en
    **/var/log/zabbix**.
- La interfaz de Zabbix se encuentra en **/usr/share/zabbix**.
- El directorio de inicio del usuario **zabbix** es **/var/lib/zabbix**.

[comment]: # ({/cb9bb3ee-f11ee379})

[comment]: # ({59632061-e00773c5})
##### - Cambios en la configuración de Zabbix

- La zona horaria de la interfaz está configurada en Europa/Riga (esto se puede modificar en
    **/etc/php-fpm.d/zabbix.conf**);

[comment]: # ({/59632061-e00773c5})

[comment]: # ({94d2afa4-2c4e4239})
#### - Acceso a la interfaz

De forma predeterminada, el acceso a la interfaz está permitido desde cualquier lugar.

Se puede acceder a la interfaz en *http://<host>*.

Esto se puede personalizar en **/etc/nginx/conf.d/zabbix.conf**. Nginx tiene
que ser reiniciado después de modificar este archivo. Para hacerlo, inicie sesión usando SSH como
Usuario **root** y ejecute:

    systemctl restart nginx

[comment]: # ({/94d2afa4-2c4e4239})

[comment]: # ({19ac1aa7-4fbc391b})
#### - Cortafuegos

De manera predeterminada, solo los puertos enumerados en los [cambios de configuración
](#firewall_configuration) arriba están abiertos. Para abrir puertos adicionales, modifique el archivo "*/etc/sysconfig/iptables*" y vuelva a cargar las reglas del firewall:

    systemctl reload iptables

[comment]: # ({/19ac1aa7-4fbc391b})

[comment]: # ({8edbd520-bf60554b})
#### - Actualización

Los paquetes del dispositivo Zabbix pueden actualizarse. Para hacerlo, ejecuta:

    dnf update zabbix*

[comment]: # ({/8edbd520-bf60554b})

[comment]: # ({25b987d7-d167c766})
#### - Servicios del sistema

Los servicios de Systemd están disponibles:

    systemctl list-units zabbix*

[comment]: # ({/25b987d7-d167c766})

[comment]: # ({6d9aa194-a582d1bf})
#### - Notas específicas del formato

[comment]: # ({/6d9aa194-a582d1bf})

[comment]: # ({d03f9132-a45df55b})
##### - VMware

Las imágenes en formato *vmdk* se pueden usar directamente en VMware Player, Server
y productos para estaciones de trabajo. Para su uso en ESX, ESXi y vSphere deben ser
convertido usando [VMware
converter](http://www.vmware.com/products/converter/).

[comment]: # ({/d03f9132-a45df55b})

[comment]: # ({ac67623c-7d1c1440})
##### - Imagen HDD/flash (sin procesar)

    dd if=./zabbix_appliance_5.2.0.raw of=/dev/sdc bs=4k conv=fdatasync

Reemplace */dev/sdc* con su dispositivo de disco Flash/HDD.

[comment]: # ({/ac67623c-7d1c1440})
