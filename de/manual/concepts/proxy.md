[comment]: # translation:outdated

[comment]: # ({new-f9458f4e})
# 4 Proxy

[comment]: # ({/new-f9458f4e})

[comment]: # ({new-05d8de8a})
#### Overview

Zabbix proxy is a process that may collect monitoring data from one or
more monitored devices and send the information to the Zabbix server,
essentially working on behalf of the server. All collected data is
buffered locally and then transferred to the Zabbix server the proxy
belongs to.

Deploying a proxy is optional, but may be very beneficial to distribute
the load of a single Zabbix server. If only proxies collect data,
processing on the server becomes less CPU and disk I/O hungry.

A Zabbix proxy is the ideal solution for centralized monitoring of
remote locations, branches and networks with no local administrators.

Zabbix proxy requires a separate database.

::: noteimportant
Note that databases supported with Zabbix proxy
are SQLite, MySQL and PostgreSQL. Using Oracle is at your own risk and
may contain some limitations as, for example, in [return
values](/manual/discovery/low_level_discovery#overview) of low-level
discovery rules.
:::

See also: [Using proxies in a distributed
environment](/manual/distributed_monitoring/proxies)

[comment]: # ({/new-05d8de8a})

[comment]: # ({new-709824a5})
#### Running proxy

[comment]: # ({/new-709824a5})

[comment]: # ({new-a0a6c8d1})
##### If installed as package

Zabbix proxy runs as a daemon process. The proxy can be started by
executing:

    shell> service zabbix-proxy start

This will work on most of GNU/Linux systems. On other systems you may
need to run:

    shell> /etc/init.d/zabbix-proxy start

Similarly, for stopping/restarting/viewing status of Zabbix proxy, use
the following commands:

    shell> service zabbix-proxy stop
    shell> service zabbix-proxy restart
    shell> service zabbix-proxy status

[comment]: # ({/new-a0a6c8d1})

[comment]: # ({new-0b35181b})
##### Start up manually

If the above does not work you have to start it manually. Find the path
to the zabbix\_proxy binary and execute:

    shell> zabbix_proxy

You can use the following command line parameters with Zabbix proxy:

    -c --config <file>              path to the configuration file
    -f --foreground                 run Zabbix proxy in foreground
    -R --runtime-control <option>   perform administrative functions
    -h --help                       give this help
    -V --version                    display version number

::: noteclassic
Runtime control is not supported on OpenBSD and
NetBSD.
:::

Examples of running Zabbix proxy with command line parameters:

    shell> zabbix_proxy -c /usr/local/etc/zabbix_proxy.conf
    shell> zabbix_proxy --help
    shell> zabbix_proxy -V

[comment]: # ({/new-0b35181b})

[comment]: # ({new-ee4dd6f0})
##### Runtime control

Runtime control options:

|Option|Description|Target|
|------|-----------|------|
|config\_cache\_reload|Reload configuration cache. Ignored if cache is being currently loaded.<br>Active Zabbix proxy will connect to the Zabbix server and request configuration data.|<|
|diaginfo\[=<**target**>\]|Gather diagnostic information in the proxy log file.|**historycache** - history cache statistics<br>**preprocessing** - preprocessing manager statistics<br>**locks** - list of mutexes|
|snmp\_cache\_reload|Reload SNMP cache, clear the SNMP properties (engine time, engine boots, engine id, credentials) for all hosts.|<|
|housekeeper\_execute|Start the housekeeping procedure. Ignored if the housekeeping procedure is currently in progress.|<|
|log\_level\_increase\[=<**target**>\]|Increase log level, affects all processes if target is not specified.|**process type** - All processes of specified type (e.g., poller)<br>See all [proxy process types](#proxy_process_types).<br>**process type,N** - Process type and number (e.g., poller,3)<br>**pid** - Process identifier (1 to 65535). For larger values specify target as 'process type,N'.|
|log\_level\_decrease\[=<**target**>\]|Decrease log level, affects all processes if target is not specified.|^|

Example of using runtime control to reload the proxy configuration
cache:

    shell> zabbix_proxy -c /usr/local/etc/zabbix_proxy.conf -R config_cache_reload

Examples of using runtime control to gather diagnostic information:

    Gather all available diagnostic information in the proxy log file:
    shell> zabbix_proxy -R diaginfo

    Gather history cache statistics in the proxy log file:
    shell> zabbix_proxy -R diaginfo=historycache

Example of using runtime control to reload the SNMP cache:

    shell> zabbix_proxy -R snmp_cache_reload  

Example of using runtime control to trigger execution of housekeeper

    shell> zabbix_proxy -c /usr/local/etc/zabbix_proxy.conf -R housekeeper_execute

Examples of using runtime control to change log level:

    Increase log level of all processes:
    shell> zabbix_proxy -c /usr/local/etc/zabbix_proxy.conf -R log_level_increase

    Increase log level of second poller process:
    shell> zabbix_proxy -c /usr/local/etc/zabbix_proxy.conf -R log_level_increase=poller,2

    Increase log level of process with PID 1234:
    shell> zabbix_proxy -c /usr/local/etc/zabbix_proxy.conf -R log_level_increase=1234

    Decrease log level of all http poller processes:
    shell> zabbix_proxy -c /usr/local/etc/zabbix_proxy.conf -R log_level_decrease="http poller"

[comment]: # ({/new-ee4dd6f0})

[comment]: # ({new-a9af55d3})
##### Process user

Zabbix proxy is designed to run as a non-root user. It will run as
whatever non-root user it is started as. So you can run proxy as any
non-root user without any issues.

If you will try to run it as 'root', it will switch to a hardcoded
'zabbix' user, which must be present on your system. You can only run
proxy as 'root' if you modify the 'AllowRoot' parameter in the proxy
configuration file accordingly.

[comment]: # ({/new-a9af55d3})

[comment]: # ({new-441f5ec7})
##### Configuration file

See the [configuration file](/manual/appendix/config/zabbix_proxy)
options for details on configuring zabbix\_proxy.

[comment]: # ({/new-441f5ec7})

[comment]: # ({new-2cd46511})
#### Proxy process types

-   `availability manager` - process for host availability updates
-   `configuration syncer` - process for managing in-memory cache of
    configuration data
-   `data sender` - proxy data sender
-   `discoverer` - process for discovery of devices
-   `heartbeat sender` - proxy heartbeat sender
-   `history poller` - process for handling calculated, aggregated and
    internal checks requiring a database connection
-   `history syncer` - history DB writer
-   `housekeeper` - process for removal of old historical data
-   `http poller` - web monitoring poller
-   `icmp pinger` - poller for icmpping checks
-   `ipmi manager` - IPMI poller manager
-   `ipmi poller` - poller for IPMI checks
-   `java poller` - poller for Java checks
-   `odbc poller` - poller for ODBC checks
-   `poller` - normal poller for passive checks
-   `preprocessing manager` - manager of preprocessing tasks
-   `preprocessing worker` - process for data preprocessing
-   `self-monitoring` - process for collecting internal server
    statistics
-   `snmp trapper` - trapper for SNMP traps
-   `task manager` - process for remote execution of tasks requested by
    other components (e.g. close problem, acknowledge problem, check
    item value now, remote command functionality)
-   `trapper` - trapper for active checks, traps, proxy communication
-   `unreachable poller` - poller for unreachable devices
-   `vmware collector` - VMware data collector responsible for data
    gathering from VMware services

The proxy log file can be used to observe these process types.

Various types of Zabbix proxy processes can be monitored using the
**zabbix\[process,<type>,<mode>,<state>\]** internal
[item](/manual/config/items/itemtypes/internal).

[comment]: # ({/new-2cd46511})

[comment]: # ({new-087c822f})
#### Supported platforms

Zabbix proxy runs on the same list of
[server\#supported platforms](/manual/concepts/server#supported platforms)
as Zabbix server.

[comment]: # ({/new-087c822f})

[comment]: # ({new-c703b792})
#### Locale

Note that the proxy requires a UTF-8 locale so that some textual items
can be interpreted correctly. Most modern Unix-like systems have a UTF-8
locale as default, however, there are some systems where that may need
to be set specifically.

[comment]: # ({/new-c703b792})
