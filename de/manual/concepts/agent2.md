[comment]: # translation:outdated

[comment]: # ({new-d60be363})
# 3 Agent 2

[comment]: # ({/new-d60be363})

[comment]: # ({new-734dc3ed})
#### Overview

Zabbix agent 2 is a new generation of Zabbix agent and may be used in
place of Zabbix agent. Zabbix agent 2 has been developed to:

-   reduce the number of TCP connections
-   have greater check concurrency
-   be easily extendible with plugins. A plugin should be able to:
    -   provide trivial checks consisting of only a few simple lines of
        code
    -   provide complex checks consisting of long-running scripts and
        standalone data gathering with periodic sending back of the data
-   be a drop-in replacement for Zabbix agent (in that it supports all
    the previous functionality)

Agent 2 is written in Go (with some C code of Zabbix agent reused). A
configured Go environment with a currently supported [Go
version](https://golang.org/doc/devel/release#policy) is required for
building Zabbix agent 2.

Agent 2 does not have built-in daemonization support on Linux; it can be
run as a [Windows service](/manual/appendix/install/windows_agent).

Passive checks work similarly to Zabbix agent. Active checks support
scheduled/flexible intervals and check concurrency within one active
server.

**Check concurrency**

Checks from different plugins can be executed concurrently. The number
of concurrent checks within one plugin is limited by the plugin capacity
setting. Each plugin may have a hardcoded capacity setting (100 being
default) that can be lowered using the
`Plugins.<Plugin name>.Capacity=N` setting in the *Plugins*
configuration [parameter](#configuration_file).

See also: [Plugin development
guidelines](https://www.zabbix.com/documentation/guidelines/plugins).

[comment]: # ({/new-734dc3ed})

[comment]: # ({new-eaa0f2bb})
##### Passive and active checks

Passive checks work similarly to Zabbix agent. Active checks support
scheduled/flexible intervals and check concurrency within one active
server. 

::: noteclassic
  By default, after a restart, Zabbix agent 2 will schedule the first data collection for active checks at a conditionally random time within the item's update interval to prevent spikes in resource usage. To perform active checks that do not have *Scheduling* [update interval](/manual/config/items/item/custom_intervals#scheduling-intervals) immediately after the agent restart, set `ForceActiveChecksOnStart` parameter (global-level) or `Plugins.<Plugin name>.System.ForceActiveChecksOnStart` (affects only specific plugin checks) in the [configuration file](/manual/appendix/config/zabbix_agent2). Plugin-level parameter, if set, will override the global parameter. Forcing active checks on start is supported since Zabbix 6.0.2. 
:::

[comment]: # ({/new-eaa0f2bb})

[comment]: # ({new-05ea1336})
##### Check concurrency

Checks from different plugins can be executed concurrently. The number
of concurrent checks within one plugin is limited by the plugin capacity
setting. Each plugin may have a hardcoded capacity setting (100 being
default) that can be lowered using the `Plugins.<PluginName>.System.Capacity=N` setting in the *Plugins*
configuration [parameter](#configuration_file). Former name of this parameter `Plugins.<PluginName>.Capacity` is still supported, but has been deprecated in Zabbix 6.0.

[comment]: # ({/new-05ea1336})

[comment]: # ({new-a2064519})
#### Supported platforms

Agent 2 is supported for Linux and Windows platforms.

If installing from packages, Agent 2 is supported on:

-   RHEL/CentOS 6, 7, 8
-   SLES 15 SP1+
-   Debian 9, 10
-   Ubuntu 18.04, 20.04

On Windows Agent 2 is supported on:

-   Windows Server 2008 R2 and later
-   Windows 7 and later

[comment]: # ({/new-a2064519})

[comment]: # ({new-cabc0f5f})
#### Installation

Zabbix agent 2 is available in pre-compiled Zabbix packages. To compile
Zabbix agent 2 from sources you have to specify the `--enable-agent2`
configure option.

[comment]: # ({/new-cabc0f5f})

[comment]: # ({new-45c02f12})
#### Options

The following command line parameters can be used with Zabbix agent 2:

|**Parameter**|**Description**|
|-------------|---------------|
|-c --config <config-file>|Path to the configuration file.<br>You may use this option to specify a configuration file that is not the default one.<br>On UNIX, default is /usr/local/etc/zabbix\_agent2.conf or as set by [compile-time](/manual/installation/install#installing_zabbix_daemons) variables *--sysconfdir* or *--prefix*|
|-f --foreground|Run Zabbix agent in foreground (default: true).|
|-p --print|Print known items and exit.<br>*Note*: To return [user parameter](/manual/config/items/userparameters) results as well, you must specify the configuration file (if it is not in the default location).|
|-t --test <item key>|Test specified item and exit.<br>*Note*: To return [user parameter](/manual/config/items/userparameters) results as well, you must specify the configuration file (if it is not in the default location).|
|-h --help|Print help information and exit.|
|-v --verbose|Print debugging information. Use this option with -p and -t options.|
|-V --version|Print agent version number and exit.|
|-R --runtime-control <option>|Perform administrative functions. See [runtime control](/manual/concepts/agent2#runtime_control).|

Specific **examples** of using command line parameters:

-   print all built-in agent items with values
-   test a user parameter with "mysql.ping" key defined in the specified
    configuration file

```{=html}
<!-- -->
```
    shell> zabbix_agent2 --print
    shell> zabbix_agent2 -t "mysql.ping" -c /etc/zabbix/zabbix_agentd.conf

[comment]: # ({/new-45c02f12})

[comment]: # ({new-11aa0de0})
##### Runtime control

Runtime control provides some options for remote control.

|Option|Description|
|------|-----------|
|log\_level\_increase|Increase log level.|
|log\_level\_decrease|Decrease log level.|
|metrics|List available metrics.|
|version|Display agent version.|
|userparameter\_reload|Reload user parameters from the current configuration file.<br>Note that UserParameter is the only agent configuration option that will be reloaded.|
|help|Display help information on runtime control.|

Examples:

-   increasing log level for agent 2
-   print runtime control options

```{=html}
<!-- -->
```
    shell> zabbix_agent2 -R log_level_increase
    shell> zabbix_agent2 -R help

[comment]: # ({/new-11aa0de0})

[comment]: # ({new-77e9b590})
#### Configuration file

The configuration parameters of agent 2 are mostly compatible with
Zabbix agent with some exceptions.

|New parameters|Description|
|--------------|-----------|
|*ControlSocket*|The runtime control socket path. Agent 2 uses a control socket for [runtime commands](#runtime_control).|
|*EnablePersistentBuffer*,<br>*PersistentBufferFile*,<br>*PersistentBufferPeriod*|These parameters are used to configure persistent storage on agent 2 for active items.|
|*Plugins*|Plugins may have their own parameters, in the format `Plugins.<Plugin name>.<Parameter>=<value>`. A common plugin parameter is *Capacity*, setting the limit of checks that can be executed at the same time.|
|*StatusPort*|The port agent 2 will be listening on for HTTP status request and display of a list of configured plugins and some internal parameters|
|Dropped parameters|Description|
|*AllowRoot*, *User*|Not supported because daemonization is not supported.|
|*LoadModule*, *LoadModulePath*|Loadable modules are not supported.|
|*StartAgents*|This parameter was used in Zabbix agent to increase passive check concurrency or disable them. In Agent 2, the concurrency is configured at a plugin level and can be limited by a capacity setting. Whereas disabling passive checks is not currently supported.|
|*HostInterface*, *HostInterfaceItem*|Not yet supported.|

For more details see the configuration file options for
[zabbix\_agent2](/manual/appendix/config/zabbix_agent2).

[comment]: # ({/new-77e9b590})

[comment]: # ({new-472820ac})
#### Exit codes

Starting from version 4.4.8 Zabbix agent 2 can also be compiled with
older OpenSSL versions (1.0.1, 1.0.2).

In this case Zabbix provides mutexes for locking in OpenSSL. If a mutex
lock or unlock fails then an error message is printed to the standard
error stream (STDERR) and Agent 2 exits with return code 2 or 3,
respectively.

[comment]: # ({/new-472820ac})
