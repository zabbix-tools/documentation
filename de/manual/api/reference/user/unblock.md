[comment]: # translation:outdated

[comment]: # ({new-8790eaaa})
# user.unblock

[comment]: # ({/new-8790eaaa})

[comment]: # ({new-ae802b92})
### Description

`object user.unblock(array userids)`

This method allows to unblock users.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-ae802b92})

[comment]: # ({new-cdc313c2})
### Parameters

`(array)` IDs of users to unblock.

[comment]: # ({/new-cdc313c2})

[comment]: # ({new-751d77e0})
### Return values

`(object)` Returns an object containing the IDs of the unblocked users
under the `userids` property.

[comment]: # ({/new-751d77e0})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-2d447a22})
#### Unblocking multiple users

Unblock two users.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "user.unblock",
    "params": [
        "1",
        "5"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "userids": [
            "1",
            "5"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-2d447a22})

[comment]: # ({new-e10b30be})
### Source

CUser::unblock() in *ui/include/classes/api/services/CUser.php*.

[comment]: # ({/new-e10b30be})
