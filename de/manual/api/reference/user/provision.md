[comment]: # translation:outdated

[comment]: # ({new-82b6f6ff})
# user.provision

[comment]: # ({/new-82b6f6ff})

[comment]: # ({new-a78ac238})
### Description

`object user.provision(object/array users)`

This method allows to provision LDAP users.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings.
See [User roles](/manual/web_interface/frontend_sections/users/user_roles) for more information.
:::

[comment]: # ({/new-a78ac238})

[comment]: # ({new-d03ad838})
### Parameters

`(array)` IDs of users to provision.

[comment]: # ({/new-d03ad838})

[comment]: # ({new-26affb31})
### Return values

`(object)` Returns an object containing the IDs of the provisioned users under the `userids` property.

[comment]: # ({/new-26affb31})

[comment]: # ({new-b507f273})
### Examples

[comment]: # ({/new-b507f273})

[comment]: # ({new-aef28656})
#### Provisioning multiple users

Provision two users.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "user.provision",
    "params": [
        "1",
        "5"
    ],
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "userids": [
            "1",
            "5"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-aef28656})

[comment]: # ({new-ab3a6b91})
### Source

CUser::provision() in *ui/include/classes/api/services/CUser.php*.

[comment]: # ({/new-ab3a6b91})
