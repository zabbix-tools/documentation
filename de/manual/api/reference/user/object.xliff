<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="de" datatype="plaintext" original="manual/api/reference/user/object.md">
    <body>
      <trans-unit id="0834cf88" xml:space="preserve">
        <source># &gt; User object

The following objects are directly related to the `user` API.</source>
      </trans-unit>
      <trans-unit id="0ecc45be" xml:space="preserve">
        <source>### User

The user object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|userid|string|ID of the user.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *read-only*&lt;br&gt;- *required* for update operations|
|username|string|User's name.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* for create operations&lt;br&gt;- *read-only* for [provisioned users](/manual/web_interface/frontend_sections/users/authentication#default-authentication) if the user is linked to a [user directory](/manual/api/reference/userdirectory/object) (`userdirectoryid` is set to a valid value that is not "0"), and user directory provisioning status is enabled (`provision_status` of [User directory object](/manual/api/reference/userdirectory/object#userdirectory) is set to "1"), and authentication status of all LDAP or SAML provisioning is enabled (`ldap_jit_status` of [Authentication object](/manual/api/reference/authentication/object#authentication) is set to "Enabled for configured LDAP IdPs" or `saml_jit_status` of [Authentication object](/manual/api/reference/authentication/object#authentication) is set to "Enabled for configured SAML IdPs")|
|passwd|string|User's password.&lt;br&gt;&lt;br&gt;The value of this parameter can be an empty string if the user is linked to a [user directory](/manual/api/reference/userdirectory/object).&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *write-only*|
|roleid|string|Role ID of the user.|
|attempt\_clock|timestamp|Time of the last unsuccessful login attempt.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *read-only*|
|attempt\_failed|integer|Recent failed login attempt count.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *read-only*|
|attempt\_ip|string|IP address from where the last unsuccessful login attempt came from.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *read-only*|
|autologin|integer|Whether to enable auto-login.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* auto-login disabled;&lt;br&gt;1 - auto-login enabled.|
|autologout|string|User session life time. Accepts seconds and time unit with suffix. If set to 0s, the session will never expire.&lt;br&gt;&lt;br&gt;Default: 15m.|
|lang|string|Language code of the user's language, for example, `en_GB`.&lt;br&gt;&lt;br&gt;Default: `default` - system default.|
|name|string|Name of the user.|
|refresh|string|Automatic refresh period. Accepts seconds and time unit with suffix.&lt;br&gt;&lt;br&gt;Default: 30s.|
|rows\_per\_page|integer|Amount of object rows to show per page.&lt;br&gt;&lt;br&gt;Default: 50.|
|surname|string|Surname of the user.|
|theme|string|User's theme.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;`default` - *(default)* system default;&lt;br&gt;`blue-theme` - Blue;&lt;br&gt;`dark-theme` - Dark.|
|ts_provisioned|timestamp|Time when the latest [provisioning](/manual/api/reference/user/provision) operation was made.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *read-only*|
|url|string|URL of the page to redirect the user to after logging in.|
|userdirectoryid|string|ID of the [user directory](/manual/api/reference/userdirectory/object) that the user in linked to.&lt;br&gt;&lt;br&gt;Used for provisioning (creating or updating), as well as to login a user that is linked to a user directory.&lt;br&gt;&lt;br&gt;For login operations the value of this property will have priority over the `userdirectoryid` property of [user groups](/manual/api/reference/usergroup/object#user-group) that the user belongs to.&lt;br&gt;&lt;br&gt;Default: 0.|
|timezone|string|User's time zone, for example, `Europe/London`, `UTC`.&lt;br&gt;&lt;br&gt;Default: `default` - system default.&lt;br&gt;&lt;br&gt;For the full list of supported time zones please refer to [PHP documentation](https://www.php.net/manual/en/timezones.php).|</source>
      </trans-unit>
      <trans-unit id="1ad487be" xml:space="preserve">
        <source>### Media

The media object has the following properties.

|Property|Type|Description|
|--|--|------|
|mediatypeid|string|ID of the media type used by the media.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|
|sendto|string/array|Address, user name or other identifier of the recipient.&lt;br&gt;&lt;br&gt;If `type` of [Media type](/manual/api/reference/mediatype/object#mediatype) is set to "Email", values are represented as array. For other types of [Media types](/manual/api/reference/mediatype/object#mediatype), value is represented as a string.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|
|active|integer|Whether the media is enabled.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* enabled;&lt;br&gt;1 - disabled.|
|severity|integer|Trigger severities to send notifications about.&lt;br&gt;&lt;br&gt;Severities are stored in binary form with each bit representing the corresponding severity. For example, "12" equals "1100" in binary and means that notifications will be sent from triggers with severities "warning" and "average".&lt;br&gt;&lt;br&gt;Refer to the [trigger object page](/manual/api/reference/trigger/object#trigger) for a list of supported trigger severities.&lt;br&gt;&lt;br&gt;Default: 63.|
|period|string|Time when the notifications can be sent as a [time period](/manual/appendix/time_period) or user macros separated by a semicolon.&lt;br&gt;&lt;br&gt;Default: 1-7,00:00-24:00.|</source>
      </trans-unit>
    </body>
  </file>
</xliff>
