[comment]: # translation:outdated

[comment]: # ({new-7f5b147d})
# template.create

[comment]: # ({/new-7f5b147d})

[comment]: # ({new-d9142f8c})
### Description

`object template.create(object/array templates)`

This method allows to create new templates.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-d9142f8c})

[comment]: # ({new-95e9c6ad})
### Parameters

`(object/array)` Templates to create.

Additionally to the [standard template properties](object#template), the
method accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|**groups**<br>(required)|object/array|Host [groups](/manual/api/reference/hostgroup/object) to add the template to.<br><br>The host groups must have the `groupid` property defined.|
|tags|object/array|Template [tags](/manual/api/reference/template/object#template_tag).|
|templates|object/array|[Templates](/manual/api/reference/template/object) to be linked to the template.<br><br>The templates must have the `templateid` property defined.|
|macros|object/array|[User macros](/manual/api/reference/usermacro/object) to be created for the template.|

[comment]: # ({/new-95e9c6ad})

[comment]: # ({new-68ed5aa8})
### Return values

`(object)` Returns an object containing the IDs of the created templates
under the `templateids` property. The order of the returned IDs matches
the order of the passed templates.

[comment]: # ({/new-68ed5aa8})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-104d61d1})
#### Creating a template

Create a template with tags and link two templates to this template.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "template.create",
    "params": {
        "host": "Linux template",
        "groups": {
            "groupid": 1
        },
        "templates": [
            {
                "templateid": "11115"
            },
            {
                "templateid": "11116"
            }
        ],
        "tags": [
            {
                "tag": "Host name",
                "value": "{HOST.NAME}"
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "templateids": [
            "11117"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-104d61d1})

[comment]: # ({new-15290398})
### Source

CTemplate::create() in *ui/include/classes/api/services/CTemplate.php*.

[comment]: # ({/new-15290398})
