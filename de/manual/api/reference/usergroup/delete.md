[comment]: # translation:outdated

[comment]: # ({new-7e2bc778})
# usergroup.delete

[comment]: # ({/new-7e2bc778})

[comment]: # ({new-c1ef47a5})
### Description

`object usergroup.delete(array userGroupIds)`

This method allows to delete user groups.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-c1ef47a5})

[comment]: # ({new-22b6416d})
### Parameters

`(array)` IDs of the user groups to delete.

[comment]: # ({/new-22b6416d})

[comment]: # ({new-dde57d64})
### Return values

`(object)` Returns an object containing the IDs of the deleted user
groups under the `usrgrpids` property.

[comment]: # ({/new-dde57d64})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-156418d8})
#### Deleting multiple user groups

Delete two user groups.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "usergroup.delete",
    "params": [
        "20",
        "21"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "usrgrpids": [
            "20",
            "21"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-156418d8})

[comment]: # ({new-9a88f22f})
### Source

CUserGroup::delete() in
*ui/include/classes/api/services/CUserGroup.php*.

[comment]: # ({/new-9a88f22f})
