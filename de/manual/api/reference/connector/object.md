[comment]: # translation:outdated

[comment]: # ({new-d96a863c})
# > Connector object

The following objects are directly related to the `connector` API.

[comment]: # ({/new-d96a863c})

[comment]: # ({new-bb03100b})
### Connector

The connector object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|connectorid|string|*(readonly)* ID of the connector.|
|**name**<br>(required)|string|Name of the connector.|
|**url**<br>(required)|integer|Endpoint URL, i.e. URL of the receiver.<br><br>User macros are supported.|
|protocol|integer|Communication protocol.<br><br>Possible values:<br>0 - *(default)* Zabbix Streaming Protocol v1.0.|
|data_type|integer|Data type.<br><br>Possible values:<br>0 - *(default)* Item values;<br>1 - Events.|
|max\_records|integer|Maximum number of events or items that can be sent within one message.<br><br>Possible values range: 0 - 2147483647 (max value of 32-bit signed integer).<br><br>Default: 0 - Unlimited.|
|max\_senders|integer|Number of sender processes to run for this connector.<br><br>Possible values range: 1-100.<br><br>Default: 1.|
|max\_attempts|integer|Number of attempts.<br><br>Possible values range: 1-5.<br><br>Default: 1.|
|timeout|string|Timeout.<br><br>Time suffixes are supported, e.g. 30s, 1m.<br>User macros are supported.<br><br>Possible values range: 1-60 seconds.<br><br>Default: 5s.|
|http\_proxy|string|HTTP(S) proxy connection string given as *\[protocol\]://\[username\[:password\]@\]proxy.example.com\[:port\]*.<br><br>User macros are supported.|
|authtype|integer|HTTP authentication method.<br><br>Possible values:<br>0 - *(default)* None;<br>1 - Basic;<br>2 - NTLM;<br>3 - Kerberos;<br>4 - Digest;<br>5 - Bearer.|
|username|string|User name.<br><br>Available if HTTP authentication method is _Basic_, _NTLM_, _Kerberos_ or _Digest_.<br><br>User macros are supported.|
|password|string|Password.<br><br>Available if HTTP authentication method is _Basic_, _NTLM_, _Kerberos_ or _Digest_.<br><br>User macros are supported.|
|token|string|Bearer token<br><br>Required if HTTP authentication method is _Bearer_.<br><br>User macros are supported.|
|verify\_peer|integer|Validate host name in URL is in Common Name field or a Subject Alternate Name field of host certificate.<br><br>0 - Do not validate.<br>1 - *(default)* Validate.|
|verify\_host|integer|Validate is host certificate authentic.<br><br>0 - Do not validate.<br>1 - *(default)* Validate.|
|ssl\_cert\_file|integer|Public SSL Key file path.<br><br>User macros are supported.|
|ssl\_key\_file|integer|Private SSL Key file path.<br><br>User macros are supported.|
|ssl\_key\_password|integer|Password for SSL Key file.<br><br>User macros are supported.|
|description|string|Description of the connector.|
|status|integer|Whether the connector is enabled.<br><br>Possible values:<br>0 - Disabled;<br>1 - *(default)* Enabled.|
|tags\_evaltype|integer|Tag evaluation method.<br><br>Possible values:<br>0 - *(default)* And/Or;<br>2 - Or.|

Note that for some methods (update, delete) the required/optional parameter combination is different.

[comment]: # ({/new-bb03100b})

[comment]: # ({new-228356ed})
### Tag filter

Tag filter allows to export only matching item values or events. If not set then everything will be exported.
The tag filter object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|**tag**<br>(required)|string|Tag name.|
|operator|integer|Condition operator.<br><br>Possible values:<br>0 - *(default)* Equals;<br>1 - Does not equal;<br>2 - Contains;<br>3 - Does not contain;<br>12 - Exists;<br>1 - Does not exist.|
|value|string|Tag value.|

[comment]: # ({/new-228356ed})
