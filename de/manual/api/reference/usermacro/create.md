[comment]: # translation:outdated

[comment]: # ({new-83b3ff24})
# usermacro.create

[comment]: # ({/new-83b3ff24})

[comment]: # ({new-db3d5c68})
### Description

`object usermacro.create(object/array hostMacros)`

This method allows to create new host macros.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-db3d5c68})

[comment]: # ({new-7c7df9b5})
### Parameters

`(object/array)` Host macros to create.

The method accepts host macros with the [standard host macro
properties](object#host_macro).

[comment]: # ({/new-7c7df9b5})

[comment]: # ({new-bd0c3c49})
### Return values

`(object)` Returns an object containing the IDs of the created host
macros under the `hostmacroids` property. The order of the returned IDs
matches the order of the passed host macros.

[comment]: # ({/new-bd0c3c49})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-7c0da88a})
#### Creating a host macro

Create a host macro "{$SNMP\_COMMUNITY}" with the value "public" on host
"10198".

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "usermacro.create",
    "params": {
        "hostid": "10198",
        "macro": "{$SNMP_COMMUNITY}",
        "value": "public"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "hostmacroids": [
            "11"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-7c0da88a})

[comment]: # ({new-084b8f3c})
### Source

CUserMacro::create() in
*ui/include/classes/api/services/CUserMacro.php*.

[comment]: # ({/new-084b8f3c})
