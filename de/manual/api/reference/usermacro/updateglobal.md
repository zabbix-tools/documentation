[comment]: # translation:outdated

[comment]: # ({new-a16741b3})
# usermacro.updateglobal

[comment]: # ({/new-a16741b3})

[comment]: # ({new-19b16b0b})
### Description

`object usermacro.updateglobal(object/array globalMacros)`

This method allows to update existing global macros.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-19b16b0b})

[comment]: # ({new-be7c969e})
### Parameters

`(object/array)` [Global macro properties](object#global_macro) to be
updated.

The `globalmacroid` property must be defined for each global macro, all
other properties are optional. Only the passed properties will be
updated, all others will remain unchanged.

[comment]: # ({/new-be7c969e})

[comment]: # ({new-9c6d43ad})
### Return values

`(object)` Returns an object containing the IDs of the updated global
macros under the `globalmacroids` property.

[comment]: # ({/new-9c6d43ad})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-bd208b05})
#### Changing the value of a global macro

Change the value of a global macro to "public".

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "usermacro.updateglobal",
    "params": {
        "globalmacroid": "1",
        "value": "public"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "globalmacroids": [
            "1"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-bd208b05})

[comment]: # ({new-85ca09bc})
### Source

CUserMacro::updateGlobal() in
*ui/include/classes/api/services/CUserMacro.php*.

[comment]: # ({/new-85ca09bc})
