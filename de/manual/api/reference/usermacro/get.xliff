<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="de" datatype="plaintext" original="manual/api/reference/usermacro/get.md">
    <body>
      <trans-unit id="02ea8022" xml:space="preserve">
        <source># usermacro.get</source>
      </trans-unit>
      <trans-unit id="28befc82" xml:space="preserve">
        <source>### Description

`integer/array usermacro.get(object parameters)`

The method allows to retrieve host and global macros according to the
given parameters.

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::</source>
      </trans-unit>
      <trans-unit id="141835f9" xml:space="preserve">
        <source>### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|globalmacro|flag|Return global macros instead of host macros.|
|globalmacroids|string/array|Return only global macros with the given IDs.|
|groupids|string/array|Return only host macros that belong to hosts or templates from the given host groups or template groups.|
|hostids|string/array|Return only macros that belong to the given hosts or templates.|
|hostmacroids|string/array|Return only host macros with the given IDs.|
|inherited|boolean|If set to `true` return only host prototype user macros inherited from a template.|
|selectHostGroups|query|Return host groups that the host macro belongs to in the [`hostgroups`](/manual/api/reference/hostgroup/object) property.&lt;br&gt;&lt;br&gt;Used only when retrieving host macros.|
|selectHosts|query|Return hosts that the host macro belongs to in the [`hosts`](/manual/api/reference/host/object) property.&lt;br&gt;&lt;br&gt;Used only when retrieving host macros.|
|selectTemplateGroups|query|Return template groups that the template macro belongs to in the [`templategroups`](/manual/api/reference/templategroup/object) property.&lt;br&gt;&lt;br&gt;Used only when retrieving template macros.|
|selectTemplates|query|Return templates that the host macro belongs to in the [`templates`](/manual/api/reference/template/object) property.&lt;br&gt;&lt;br&gt;Used only when retrieving host macros.|
|sortfield|string/array|Sort the result by the given properties.&lt;br&gt;&lt;br&gt;Possible values: `macro`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters) page.|
|editable|boolean|^|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|
|selectGroups&lt;br&gt;(deprecated)|query|This parameter is deprecated, please use `selectHostGroups` or `selectTemplateGroups` instead.&lt;br&gt;Return host groups and template groups that the host macro belongs to in the `groups` property.&lt;br&gt;&lt;br&gt;Used only when retrieving host macros.|</source>
      </trans-unit>
      <trans-unit id="7223bab1" xml:space="preserve">
        <source>### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.</source>
      </trans-unit>
      <trans-unit id="b41637d2" xml:space="preserve">
        <source>### Examples</source>
      </trans-unit>
      <trans-unit id="7089e052" xml:space="preserve">
        <source>#### Retrieving host macros for a host

Retrieve all host macros defined for host "10198".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "usermacro.get",
    "params": {
        "output": "extend",
        "hostids": "10198"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": [
        {
            "hostmacroid": "9",
            "hostid": "10198",
            "macro": "{$INTERFACE}",
            "value": "eth0",
            "description": "",
            "type": "0",
            "automatic": "0"
        },
        {
            "hostmacroid": "11",
            "hostid": "10198",
            "macro": "{$SNMP_COMMUNITY}",
            "value": "public",
            "description": "",
            "type": "0",
            "automatic": "0"
        }
    ],
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="29b64a7a" xml:space="preserve">
        <source>#### Retrieving global macros

Retrieve all global macros.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "usermacro.get",
    "params": {
        "output": "extend",
        "globalmacro": true
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": [
        {
            "globalmacroid": "6",
            "macro": "{$SNMP_COMMUNITY}",
            "value": "public",
            "description": "",
            "type": "0"
        }
    ],
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="26db0ec2" xml:space="preserve">
        <source>### Source

CUserMacro::get() in *ui/include/classes/api/services/CUserMacro.php*.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
