[comment]: # translation:outdated

[comment]: # ({new-a139f9f9})
# 12 Item value

[comment]: # ({/new-a139f9f9})

[comment]: # ({new-b003922b})
### Description

These parameters and the possible property values for the respective dashboard widget field objects allow to configure
the [*Item value*](/manual/web_interface/frontend_sections/dashboards/widgets/item_value) widget in `dashboard.create` and `dashboard.update` methods.

[comment]: # ({/new-b003922b})

[comment]: # ({new-2afe65e8})
### Parameters

The following parameters are supported for the *Item value* widget.

|Parameter|[type](/manual/api/reference/dashboard/object#dashboard-widget-field)|name|value|
|-----|-|-----|-------------------|
|*Refresh interval*|0|rf_rate|0 - No refresh;<br>10 - 10 seconds;<br>30 - 30 seconds;<br>60 - *(default)* 1 minute;<br>120 - 2 minutes;<br>600 - 10 minutes;<br>900 - 15 minutes.|
|***Item***<br>(required)|4|itemid|[Item](/manual/api/reference/item/get) ID.|
|*Show*|0|show|1 - Description;<br>2 - Value;<br>3 - Time;<br>4 - Change indicator.<br><br>Default: 1, 2, 3, 4 (all enabled).<br><br>Note: To configure multiple values, create a dashboard widget field object for each value.|
|*Enable host selection*|0|dynamic|0 - *(default)* Disabled;<br>1 - Enabled.|
|*Advanced configuration*|0|adv_conf|0 - *(default)* Disabled;<br>1 - Enabled.|

[comment]: # ({/new-2afe65e8})

[comment]: # ({new-3cc5f221})
#### Advanced configuration

The following parameters are supported if *Advanced configuration* is set to "Enabled".

::: noteclassic
The number in the *Thresholds* property name (e.g. thresholds.color.0) references the threshold place in a list, sorted in ascending order.
However, if thresholds are configured in a different order, the values will be sorted in ascending order after updating widget configuration in Zabbix frontend
(e.g. `"threshold.threshold.0":"5"` → `"threshold.threshold.0":"1"`; `"threshold.threshold.1":"1"` → `"threshold.threshold.1": "5"`).
:::

|Parameter|<|[type](/manual/api/reference/dashboard/object#dashboard-widget-field)|name|value|
|-|--------|--|--------|-------------------------------|
|*Background color*|<|1|bg_color|Hexadecimal color code (e.g. `FF0000`).<br><br>Default: `""` (empty).|
|*Thresholds*|<|<|<|<|
|<|*Color*|1|thresholds.color.0|Hexadecimal color code (e.g. `FF0000`).|
|^|*Threshold*|1|thresholds.threshold.0|Any string value.|

[comment]: # ({/new-3cc5f221})

[comment]: # ({new-1925c46d})
##### Description

The following parameters are supported if *Advanced configuration* is set to "Enabled", and *Show* is set to "Description".

|Parameter|[type](/manual/api/reference/dashboard/object#dashboard-widget-field)|name|value|
|-----|-|-----|-------------------|
|*Description*|1|description|Any string value, including macros.<br>Supported macros: {HOST.\*}, {ITEM.\*}, {INVENTORY.\*}, User macros.<br><br>Default: {ITEM.NAME}.|
|*Horizontal position*|0|desc_h_pos|0 - Left;<br>1 - *(default)* Center;<br>2 - Right.<br><br>Two or more elements (Description, Value, Time) cannot share the same *Horizontal position* and *Vertical position*.|
|*Vertical position*|0|desc_v_pos|0 - Top;<br>1 - Middle;<br>2 - *(default)* Bottom.<br><br>Two or more elements (Description, Value, Time) cannot share the same *Horizontal position* and *Vertical position*.|
|*Size*|0|desc_size|Valid values range from 1-100.<br><br>Default: 15.|
|*Bold*|0|desc_bold|0 - *(default)* Disabled;<br>1 - Enabled.|
|*Color*|1|desc_color|Hexadecimal color code (e.g. `FF0000`).<br><br>Default: `""` (empty).|

[comment]: # ({/new-1925c46d})

[comment]: # ({new-9657b88c})
##### Value

The following parameters are supported if *Advanced configuration* is set to "Enabled", and *Show* is set to "Value".

|Parameter|<|[type](/manual/api/reference/dashboard/object#dashboard-widget-field)|name|value|
|-|--------|--|--------|-------------------------------|
|*Decimal places*|<|<|<|<|
|<|*Decimal places*|0|decimal_places|Valid values range from 1-10.<br><br>Default: 2.|
|^|*Size*|0|decimal_size|Valid values range from 1-100.<br><br>Default: 35.|
|*Position*|<|<|<|<|
|<|*Horizontal position*|0|value_h_pos|0 - Left;<br>1 - *(default)* Center;<br>2 - Right.<br><br>Two or more elements (Description, Value, Time) cannot share the same *Horizontal position* and *Vertical position*.|
|^|*Vertical position*|0|value_v_pos|0 - Top;<br>1 - *(default)* Middle;<br>2 - Bottom.<br><br>Two or more elements (Description, Value, Time) cannot share the same *Horizontal position* and *Vertical position*.|
|^|*Size*|0|value_size|Valid values range from 1-100.<br><br>Default: 45.|
|^|*Bold*|0|value_bold|0 - Disabled;<br>1 - *(default)* Enabled.|
|^|*Color*|1|value_color|Hexadecimal color code (e.g. `FF0000`).<br><br>Default: `""` (empty).|
|*Units*|<|<|<|<|
|<|*Units* (checkbox)|0|units_show|0 - Disabled;<br>1 - *(default)* Enabled.|
|^|*Units* (value)|1|units|Any string value.|
|^|*Position*|0|units_pos|0 - Before value;<br>1 - Above value;<br>2 - *(default)* After value;<br>3 - Below value.|
|^|*Size*|0|units_size|Valid values range from 1-100.<br><br>Default: 35.|
|^|*Bold*|0|units_bold|0 - Disabled;<br>1 - *(default)* Enabled.|
|^|*Color*|1|units_color|Hexadecimal color code (e.g. `FF0000`).<br><br>Default: `""` (empty).|

[comment]: # ({/new-9657b88c})

[comment]: # ({new-c0007d65})
##### Time

The following parameters are supported if *Advanced configuration* is set to "Enabled", and *Show* is set to "Time".

|Parameter|[type](/manual/api/reference/dashboard/object#dashboard-widget-field)|name|value|
|-----|-|-----|-------------------|
|*Horizontal position*|0|time_h_pos|0 - Left;<br>1 - *(default)* Center;<br>2 - Right.<br><br>Two or more elements (Description, Value, Time) cannot share the same *Horizontal position* and *Vertical position*.|
|*Vertical position*|0|time_v_pos|0 - *(default)* Top;<br>1 - Middle;<br>2 - Bottom.<br><br>Two or more elements (Description, Value, Time) cannot share the same *Horizontal position* and *Vertical position*.|
|*Size*|0|time_size|Valid values range from 1-100.<br><br>Default: 15.|
|*Bold*|0|time_bold|0 - *(default)* Disabled;<br>1 - Enabled.|
|*Color*|1|time_color|Hexadecimal color code (e.g. `FF0000`).<br><br>Default: `""` (empty).|

[comment]: # ({/new-c0007d65})

[comment]: # ({new-f824a1aa})
##### Change indicator

The following parameters are supported if *Advanced configuration* is set to "Enabled", and *Show* is set to "Change indicator".

|Parameter|[type](/manual/api/reference/dashboard/object#dashboard-widget-field)|name|value|
|-----|-|-----|-------------------|
|*Change indicator ↑ color*|1|up_color|Hexadecimal color code (e.g. `FF0000`).<br><br>Default: `""` (empty).|
|*Change indicator ↓ color*|1|down_color|Hexadecimal color code (e.g. `FF0000`).<br><br>Default: `""` (empty).|
|*Change indicator ↕ color*|1|updown_color|Hexadecimal color code (e.g. `FF0000`).<br><br>Default: `""` (empty).|

[comment]: # ({/new-f824a1aa})

[comment]: # ({new-7c28c6ce})
### Examples

The following examples aim to only describe the configuration of the dashboard widget field objects for the *Item value* widget.
For more information on configuring a dashboard, see [`dashboard.create`](/manual/api/reference/dashboard/create).

[comment]: # ({/new-7c28c6ce})

[comment]: # ({new-e1b71728})
#### Configuring an *Item value* widget

Configure an *Item value* widget that displays the item value for the item "42266" (Zabbix agent availability).
In addition, visually fine-tune the widget with multiple advanced options, including a dynamic background color
that changes based on the availability status of Zabbix agent.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "dashboard.create",
    "params": {
        "name": "My dashboard",
        "display_period": 30,
        "auto_start": 1,
        "pages": [
            {
                "widgets": [
                    {
                        "type": "item",
                        "name": "Item value",
                        "x": 0,
                        "y": 0,
                        "width": 4,
                        "height": 3,
                        "view_mode": 0,
                        "fields": [
                            {
                                "type": 4,
                                "name": "itemid",
                                "value": 42266
                            },
                            {
                                "type": 0,
                                "name": "show",
                                "value": 1
                            },
                            {
                                "type": 0,
                                "name": "show",
                                "value": 2
                            },
                            {
                                "type": 0,
                                "name": "show",
                                "value": 3
                            },
                            {
                                "type": 0,
                                "name": "adv_conf",
                                "value": 1
                            },
                            {
                                "type": 1,
                                "name": "description",
                                "value": "Agent status"
                            },
                            {
                                "type": 0,
                                "name": "desc_h_pos",
                                "value": 0
                            },
                            {
                                "type": 0,
                                "name": "desc_v_pos",
                                "value": 0
                            },
                            {
                                "type": 0,
                                "name": "desc_bold",
                                "value": 1
                            },
                            {
                                "type": 1,
                                "name": "desc_color",
                                "value": "F06291"
                            },
                            {
                                "type": 0,
                                "name": "value_h_pos",
                                "value": 0
                            },
                            {
                                "type": 0,
                                "name": "value_size",
                                "value": 25
                            },
                            {
                                "type": 1,
                                "name": "value_color",
                                "value": "FFFF00"
                            },
                            {
                                "type": 0,
                                "name": "units_show",
                                "value": 0
                            },
                            {
                                "type": 0,
                                "name": "time_h_pos",
                                "value": 2
                            },
                            {
                                "type": 0,
                                "name": "time_v_pos",
                                "value": 2
                            },
                            {
                                "type": 0,
                                "name": "time_size",
                                "value": 10
                            },
                            {
                                "type": 0,
                                "name": "time_bold",
                                "value": 1
                            },
                            {
                                "type": 1,
                                "name": "time_color",
                                "value": "9FA8DA"
                            },
                            {
                                "type": 1,
                                "name": "thresholds.color.0",
                                "value": "E1E1E1"
                            },
                            {
                                "type": 1,
                                "name": "thresholds.threshold.0",
                                "value": "0"
                            },
                            {
                                "type": 1,
                                "name": "thresholds.color.1",
                                "value": "D1C4E9"
                            },
                            {
                                "type": 1,
                                "name": "thresholds.threshold.1",
                                "value": "1"
                            }
                        ]
                    }
                ]
            }
        ],
        "userGroups": [
            {
                "usrgrpid": 7,
                "permission": 2
            }
        ],
        "users": [
            {
                "userid": 1,
                "permission": 3
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "dashboardids": [
            "3"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-e1b71728})

[comment]: # ({new-d0066a0f})
### See also

-   [Dashboard widget field](/manual/api/reference/dashboard/object#dashboard-widget-field)
-   [`dashboard.create`](/manual/api/reference/dashboard/create)
-   [`dashboard.update`](/manual/api/reference/dashboard/update)

[comment]: # ({/new-d0066a0f})
