<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="de" datatype="plaintext" original="manual/api/reference/dashboard/widget_fields/item_value.md">
    <body>
      <trans-unit id="a139f9f9" xml:space="preserve">
        <source># 12 Item value</source>
      </trans-unit>
      <trans-unit id="b003922b" xml:space="preserve">
        <source>### Description

These parameters and the possible property values for the respective dashboard widget field objects allow to configure
the [*Item value*](/manual/web_interface/frontend_sections/dashboards/widgets/item_value) widget in `dashboard.create` and `dashboard.update` methods.</source>
      </trans-unit>
      <trans-unit id="2afe65e8" xml:space="preserve">
        <source>### Parameters

The following parameters are supported for the *Item value* widget.

|Parameter|[type](/manual/api/reference/dashboard/object#dashboard-widget-field)|name|value|
|-----|-|-----|-------------------|
|*Refresh interval*|0|rf_rate|0 - No refresh;&lt;br&gt;10 - 10 seconds;&lt;br&gt;30 - 30 seconds;&lt;br&gt;60 - *(default)* 1 minute;&lt;br&gt;120 - 2 minutes;&lt;br&gt;600 - 10 minutes;&lt;br&gt;900 - 15 minutes.|
|*Item*|4|itemid|[Item](/manual/api/reference/item/get) ID.&lt;br&gt;&lt;br&gt;[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):&lt;br&gt;- *required*|
|*Show*|0|show|1 - Description;&lt;br&gt;2 - Value;&lt;br&gt;3 - Time;&lt;br&gt;4 - Change indicator.&lt;br&gt;&lt;br&gt;Default: 1, 2, 3, 4 (all enabled).&lt;br&gt;&lt;br&gt;Note: To configure multiple values, create a dashboard widget field object for each value.|
|*Enable host selection*|0|dynamic|0 - *(default)* Disabled;&lt;br&gt;1 - Enabled.|</source>
      </trans-unit>
      <trans-unit id="3cc5f221" xml:space="preserve">
        <source>#### Advanced configuration

The following advanced configuration parameters are supported for the *Item value* widget.

::: noteclassic
The number in the *Thresholds* property name (e.g. thresholds.color.0) references the threshold place in a list, sorted in ascending order.
However, if thresholds are configured in a different order, the values will be sorted in ascending order after updating widget configuration in Zabbix frontend
(e.g. `"threshold.threshold.0":"5"` → `"threshold.threshold.0":"1"`; `"threshold.threshold.1":"1"` → `"threshold.threshold.1": "5"`).
:::

|Parameter|&lt;|[type](/manual/api/reference/dashboard/object#dashboard-widget-field)|name|value|
|-|--------|--|--------|-------------------------------|
|*Background color*|&lt;|1|bg_color|Hexadecimal color code (e.g. `FF0000`).&lt;br&gt;&lt;br&gt;Default: `""` (empty).|
|*Thresholds*|&lt;|&lt;|&lt;|&lt;|
|&lt;|*Color*|1|thresholds.color.0|Hexadecimal color code (e.g. `FF0000`).|
|^|*Threshold*|1|thresholds.threshold.0|Any string value.|</source>
      </trans-unit>
      <trans-unit id="1925c46d" xml:space="preserve">
        <source>##### Description

The following advanced configuration parameters are supported if *Show* is set to "Description".

|Parameter|[type](/manual/api/reference/dashboard/object#dashboard-widget-field)|name|value|
|-----|-|-----|-------------------|
|*Description*|1|description|Any string value, including macros.&lt;br&gt;Supported macros: {HOST.\*}, {ITEM.\*}, {INVENTORY.\*}, User macros.&lt;br&gt;&lt;br&gt;Default: {ITEM.NAME}.|
|*Horizontal position*|0|desc_h_pos|0 - Left;&lt;br&gt;1 - *(default)* Center;&lt;br&gt;2 - Right.&lt;br&gt;&lt;br&gt;Two or more elements (Description, Value, Time) cannot share the same *Horizontal position* and *Vertical position*.|
|*Vertical position*|0|desc_v_pos|0 - Top;&lt;br&gt;1 - Middle;&lt;br&gt;2 - *(default)* Bottom.&lt;br&gt;&lt;br&gt;Two or more elements (Description, Value, Time) cannot share the same *Horizontal position* and *Vertical position*.|
|*Size*|0|desc_size|Valid values range from 1-100.&lt;br&gt;&lt;br&gt;Default: 15.|
|*Bold*|0|desc_bold|0 - *(default)* Disabled;&lt;br&gt;1 - Enabled.|
|*Color*|1|desc_color|Hexadecimal color code (e.g. `FF0000`).&lt;br&gt;&lt;br&gt;Default: `""` (empty).|</source>
      </trans-unit>
      <trans-unit id="9657b88c" xml:space="preserve">
        <source>##### Value

The following advanced configuration parameters are supported if *Show* is set to "Value".

|Parameter|&lt;|[type](/manual/api/reference/dashboard/object#dashboard-widget-field)|name|value|
|-|--------|--|--------|-------------------------------|
|*Decimal places*|&lt;|&lt;|&lt;|&lt;|
|&lt;|*Decimal places*|0|decimal_places|Valid values range from 1-10.&lt;br&gt;&lt;br&gt;Default: 2.|
|^|*Size*|0|decimal_size|Valid values range from 1-100.&lt;br&gt;&lt;br&gt;Default: 35.|
|*Position*|&lt;|&lt;|&lt;|&lt;|
|&lt;|*Horizontal position*|0|value_h_pos|0 - Left;&lt;br&gt;1 - *(default)* Center;&lt;br&gt;2 - Right.&lt;br&gt;&lt;br&gt;Two or more elements (Description, Value, Time) cannot share the same *Horizontal position* and *Vertical position*.|
|^|*Vertical position*|0|value_v_pos|0 - Top;&lt;br&gt;1 - *(default)* Middle;&lt;br&gt;2 - Bottom.&lt;br&gt;&lt;br&gt;Two or more elements (Description, Value, Time) cannot share the same *Horizontal position* and *Vertical position*.|
|^|*Size*|0|value_size|Valid values range from 1-100.&lt;br&gt;&lt;br&gt;Default: 45.|
|^|*Bold*|0|value_bold|0 - Disabled;&lt;br&gt;1 - *(default)* Enabled.|
|^|*Color*|1|value_color|Hexadecimal color code (e.g. `FF0000`).&lt;br&gt;&lt;br&gt;Default: `""` (empty).|
|*Units*|&lt;|&lt;|&lt;|&lt;|
|&lt;|*Units* (checkbox)|0|units_show|0 - Disabled;&lt;br&gt;1 - *(default)* Enabled.|
|^|*Units* (value)|1|units|Any string value.|
|^|*Position*|0|units_pos|0 - Before value;&lt;br&gt;1 - Above value;&lt;br&gt;2 - *(default)* After value;&lt;br&gt;3 - Below value.|
|^|*Size*|0|units_size|Valid values range from 1-100.&lt;br&gt;&lt;br&gt;Default: 35.|
|^|*Bold*|0|units_bold|0 - Disabled;&lt;br&gt;1 - *(default)* Enabled.|
|^|*Color*|1|units_color|Hexadecimal color code (e.g. `FF0000`).&lt;br&gt;&lt;br&gt;Default: `""` (empty).|</source>
      </trans-unit>
      <trans-unit id="c0007d65" xml:space="preserve">
        <source>##### Time

The following advanced configuration parameters are supported if *Show* is set to "Time".

|Parameter|[type](/manual/api/reference/dashboard/object#dashboard-widget-field)|name|value|
|-----|-|-----|-------------------|
|*Horizontal position*|0|time_h_pos|0 - Left;&lt;br&gt;1 - *(default)* Center;&lt;br&gt;2 - Right.&lt;br&gt;&lt;br&gt;Two or more elements (Description, Value, Time) cannot share the same *Horizontal position* and *Vertical position*.|
|*Vertical position*|0|time_v_pos|0 - *(default)* Top;&lt;br&gt;1 - Middle;&lt;br&gt;2 - Bottom.&lt;br&gt;&lt;br&gt;Two or more elements (Description, Value, Time) cannot share the same *Horizontal position* and *Vertical position*.|
|*Size*|0|time_size|Valid values range from 1-100.&lt;br&gt;&lt;br&gt;Default: 15.|
|*Bold*|0|time_bold|0 - *(default)* Disabled;&lt;br&gt;1 - Enabled.|
|*Color*|1|time_color|Hexadecimal color code (e.g. `FF0000`).&lt;br&gt;&lt;br&gt;Default: `""` (empty).|</source>
      </trans-unit>
      <trans-unit id="f824a1aa" xml:space="preserve">
        <source>##### Change indicator

The following advanced configuration parameters are supported if *Show* is set to "Change indicator".

|Parameter|[type](/manual/api/reference/dashboard/object#dashboard-widget-field)|name|value|
|-----|-|-----|-------------------|
|*Change indicator ↑ color*|1|up_color|Hexadecimal color code (e.g. `FF0000`).&lt;br&gt;&lt;br&gt;Default: `""` (empty).|
|*Change indicator ↓ color*|1|down_color|Hexadecimal color code (e.g. `FF0000`).&lt;br&gt;&lt;br&gt;Default: `""` (empty).|
|*Change indicator ↕ color*|1|updown_color|Hexadecimal color code (e.g. `FF0000`).&lt;br&gt;&lt;br&gt;Default: `""` (empty).|</source>
      </trans-unit>
      <trans-unit id="7c28c6ce" xml:space="preserve">
        <source>### Examples

The following examples aim to only describe the configuration of the dashboard widget field objects for the *Item value* widget.
For more information on configuring a dashboard, see [`dashboard.create`](/manual/api/reference/dashboard/create).</source>
      </trans-unit>
      <trans-unit id="e1b71728" xml:space="preserve">
        <source>#### Configuring an *Item value* widget

Configure an *Item value* widget that displays the item value for the item "42266" (Zabbix agent availability).
In addition, visually fine-tune the widget with multiple advanced options, including a dynamic background color
that changes based on the availability status of Zabbix agent.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "dashboard.create",
    "params": {
        "name": "My dashboard",
        "display_period": 30,
        "auto_start": 1,
        "pages": [
            {
                "widgets": [
                    {
                        "type": "item",
                        "name": "Item value",
                        "x": 0,
                        "y": 0,
                        "width": 4,
                        "height": 3,
                        "view_mode": 0,
                        "fields": [
                            {
                                "type": 4,
                                "name": "itemid",
                                "value": 42266
                            },
                            {
                                "type": 0,
                                "name": "show",
                                "value": 1
                            },
                            {
                                "type": 0,
                                "name": "show",
                                "value": 2
                            },
                            {
                                "type": 0,
                                "name": "show",
                                "value": 3
                            },
                            {
                                "type": 1,
                                "name": "description",
                                "value": "Agent status"
                            },
                            {
                                "type": 0,
                                "name": "desc_h_pos",
                                "value": 0
                            },
                            {
                                "type": 0,
                                "name": "desc_v_pos",
                                "value": 0
                            },
                            {
                                "type": 0,
                                "name": "desc_bold",
                                "value": 1
                            },
                            {
                                "type": 1,
                                "name": "desc_color",
                                "value": "F06291"
                            },
                            {
                                "type": 0,
                                "name": "value_h_pos",
                                "value": 0
                            },
                            {
                                "type": 0,
                                "name": "value_size",
                                "value": 25
                            },
                            {
                                "type": 1,
                                "name": "value_color",
                                "value": "FFFF00"
                            },
                            {
                                "type": 0,
                                "name": "units_show",
                                "value": 0
                            },
                            {
                                "type": 0,
                                "name": "time_h_pos",
                                "value": 2
                            },
                            {
                                "type": 0,
                                "name": "time_v_pos",
                                "value": 2
                            },
                            {
                                "type": 0,
                                "name": "time_size",
                                "value": 10
                            },
                            {
                                "type": 0,
                                "name": "time_bold",
                                "value": 1
                            },
                            {
                                "type": 1,
                                "name": "time_color",
                                "value": "9FA8DA"
                            },
                            {
                                "type": 1,
                                "name": "thresholds.color.0",
                                "value": "E1E1E1"
                            },
                            {
                                "type": 1,
                                "name": "thresholds.threshold.0",
                                "value": "0"
                            },
                            {
                                "type": 1,
                                "name": "thresholds.color.1",
                                "value": "D1C4E9"
                            },
                            {
                                "type": 1,
                                "name": "thresholds.threshold.1",
                                "value": "1"
                            }
                        ]
                    }
                ]
            }
        ],
        "userGroups": [
            {
                "usrgrpid": 7,
                "permission": 2
            }
        ],
        "users": [
            {
                "userid": 1,
                "permission": 3
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "dashboardids": [
            "3"
        ]
    },
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="d0066a0f" xml:space="preserve">
        <source>### See also

-   [Dashboard widget field](/manual/api/reference/dashboard/object#dashboard-widget-field)
-   [`dashboard.create`](/manual/api/reference/dashboard/create)
-   [`dashboard.update`](/manual/api/reference/dashboard/update)</source>
      </trans-unit>
    </body>
  </file>
</xliff>
