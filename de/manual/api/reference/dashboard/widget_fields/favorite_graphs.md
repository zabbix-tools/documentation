[comment]: # translation:outdated

[comment]: # ({new-fb4eeabf})
# 5 Favorite graphs

[comment]: # ({/new-fb4eeabf})

[comment]: # ({new-40650cb4})
### Description

These parameters and the possible property values for the respective dashboard widget field objects allow to configure
the [*Favorite graphs*](/manual/web_interface/frontend_sections/dashboards/widgets/favorite_graphs) widget in `dashboard.create` and `dashboard.update` methods.

[comment]: # ({/new-40650cb4})

[comment]: # ({new-8dfaf481})
### Parameters

The following parameters are supported for the *Favorite graphs* widget.

|Parameter|[type](/manual/api/reference/dashboard/object#dashboard-widget-field)|name|value|
|-----|-|-----|-------------------|
|*Refresh interval*|0|rf_rate|0 - No refresh;<br>10 - 10 seconds;<br>30 - 30 seconds;<br>60 - 1 minute;<br>120 - 2 minutes;<br>600 - 10 minutes;<br>900 - *(default)* 15 minutes.|

[comment]: # ({/new-8dfaf481})

[comment]: # ({new-5ce1276c})
### Examples

The following examples aim to only describe the configuration of the dashboard widget field objects for the *Favorite graphs* widget.
For more information on configuring a dashboard, see [`dashboard.create`](/manual/api/reference/dashboard/create).

[comment]: # ({/new-5ce1276c})

[comment]: # ({new-80db0acb})
#### Configuring a *Favorite graphs* widget

Configure a *Favorite graphs* widget with the refresh interval set to 10 minutes.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "dashboard.create",
    "params": {
        "name": "My dashboard",
        "display_period": 30,
        "auto_start": 1,
        "pages": [
            {
                "widgets": [
                    {
                        "type": "favgraphs",
                        "name": "Favorite graphs",
                        "x": 0,
                        "y": 0,
                        "width": 4,
                        "height": 3,
                        "view_mode": 0,
                        "fields": [
                            {
                                "type": 0,
                                "name": "rf_rate",
                                "value": 600
                            }
                        ]
                    }
                ]
            }
        ],
        "userGroups": [
            {
                "usrgrpid": 7,
                "permission": 2
            }
        ],
        "users": [
            {
                "userid": 1,
                "permission": 3
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "dashboardids": [
            "3"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-80db0acb})

[comment]: # ({new-24a59ae7})
### See also

-   [Dashboard widget field](/manual/api/reference/dashboard/object#dashboard-widget-field)
-   [`dashboard.create`](/manual/api/reference/dashboard/create)
-   [`dashboard.update`](/manual/api/reference/dashboard/update)

[comment]: # ({/new-24a59ae7})
