<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="de" datatype="plaintext" original="manual/api/reference/httptest/get.md">
    <body>
      <trans-unit id="7f8cd3a3" xml:space="preserve">
        <source># httptest.get</source>
      </trans-unit>
      <trans-unit id="96cbc36d" xml:space="preserve">
        <source>### Description

`integer/array httptest.get(object parameters)`

The method allows to retrieve web scenarios according to the given
parameters.

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::</source>
      </trans-unit>
      <trans-unit id="7be2c19f" xml:space="preserve">
        <source>### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|groupids|string/array|Return only web scenarios that belong to the given host groups.|
|hostids|string/array|Return only web scenarios that belong to the given hosts.|
|httptestids|string/array|Return only web scenarios with the given IDs.|
|inherited|boolean|If set to `true` return only web scenarios inherited from a template.|
|monitored|boolean|If set to `true` return only enabled web scenarios that belong to monitored hosts.|
|templated|boolean|If set to `true` return only web scenarios that belong to templates.|
|templateids|string/array|Return only web scenarios that belong to the given templates.|
|expandName|flag|Expand macros in the name of the web scenario.|
|expandStepName|flag|Expand macros in the names of scenario steps.|
|evaltype|integer|Rules for tag searching.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - (default) And/Or;&lt;br&gt;2 - Or.|
|tags|array of objects|Return only web scenarios with given tags. Exact match by tag and case-sensitive or case-insensitive search by tag value depending on operator value.&lt;br&gt;Format: `[{"tag": "&lt;tag&gt;", "value": "&lt;value&gt;", "operator": "&lt;operator&gt;"}, ...]`.&lt;br&gt;An empty array returns all web scenarios.&lt;br&gt;&lt;br&gt;Possible operator types:&lt;br&gt;0 - (default) Like;&lt;br&gt;1 - Equal;&lt;br&gt;2 - Not like;&lt;br&gt;3 - Not equal&lt;br&gt;4 - Exists;&lt;br&gt;5 - Not exists.|
|selectHosts|query|Return the hosts that the web scenario belongs to as an array in the [`hosts`](/manual/api/reference/host/object) property.|
|selectSteps|query|Return web scenario steps in the [`steps`](/manual/api/reference/httptest/object#scenario_step) property.&lt;br&gt;&lt;br&gt;Supports `count`.|
|selectTags|query|Return web scenario tags in the [`tags`](/manual/api/reference/httptest/object#web_scenario_tag) property.|
|sortfield|string/array|Sort the result by the given properties.&lt;br&gt;&lt;br&gt;Possible values: `httptestid`, `name`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters).|
|editable|boolean|^|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|</source>
      </trans-unit>
      <trans-unit id="7223bab1" xml:space="preserve">
        <source>### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.</source>
      </trans-unit>
      <trans-unit id="b41637d2" xml:space="preserve">
        <source>### Examples</source>
      </trans-unit>
      <trans-unit id="c93618d6" xml:space="preserve">
        <source>#### Retrieving a web scenario

Retrieve all data about web scenario "4".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "httptest.get",
    "params": {
        "output": "extend",
        "selectSteps": "extend",
        "httptestids": "9"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": [
        {
            "httptestid": "9",
            "name": "Homepage check",
            "delay": "1m",
            "status": "0",
            "variables": [],
            "agent": "Zabbix",
            "authentication": "0",
            "http_user": "",
            "http_password": "",
            "hostid": "10084",
            "templateid": "0",
            "http_proxy": "",
            "retries": "1",
            "ssl_cert_file": "",
            "ssl_key_file": "",
            "ssl_key_password": "",
            "verify_peer": "0",
            "verify_host": "0",
            "headers": [],
            "steps": [
                {
                    "httpstepid": "36",
                    "httptestid": "9",
                    "name": "Homepage",
                    "no": "1",
                    "url": "http://example.com",
                    "timeout": "15s",
                    "posts": "",
                    "required": "",
                    "status_codes": "200",
                    "variables": [
                        {
                            "name":"{var}",
                            "value":"12"
                        }
                    ],
                    "follow_redirects": "1",
                    "retrieve_mode": "0",
                    "headers": [],
                    "query_fields": []
                },
                {
                    "httpstepid": "37",
                    "httptestid": "9",
                    "name": "Homepage / About",
                    "no": "2",
                    "url": "http://example.com/about",
                    "timeout": "15s",
                    "posts": "",
                    "required": "",
                    "status_codes": "200",
                    "variables": [],
                    "follow_redirects": "1",
                    "retrieve_mode": "0",
                    "headers": [],
                    "query_fields": []
                }
            ]
        }
    ],
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="ce685d69" xml:space="preserve">
        <source>### See also

-   [Host](/manual/api/reference/host/object#host)
-   [Scenario step](object#scenario_step)</source>
      </trans-unit>
      <trans-unit id="8ec619ff" xml:space="preserve">
        <source>### Source

CHttpTest::get() in *ui/include/classes/api/services/CHttpTest.php*.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
