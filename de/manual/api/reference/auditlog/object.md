[comment]: # translation:outdated

[comment]: # ({new-e0b9dd2d})
# > Audit log object

The following objects are directly related to the `auditlog` API.

[comment]: # ({/new-e0b9dd2d})

[comment]: # ({new-63741e1f})
### Audit log

The audit log object contains information about user actions. It has the
following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|auditid|string|*(readonly)* ID of audit log entry. Generated using CUID algorithm.|
|userid|string|Audit log entry author userid.|
|username|string|Audit log entry author username.|
|clock|timestamp|Audit log entry creation timestamp.|
|ip|string|Audit log entry author IP address.|
|action|integer|Audit log entry action.<br><br>Possible values are:<br>0 - Add;<br>1 - Update;<br>2 - Delete;<br>4 - Logout;<br>7 - Execute;<br>8 - Login;<br>9 - Failed login;<br>10 - History clear.|
|resourcetype|integer|Audit log entry resource type.<br><br>Possible values are:<br>0 - User;<br>3 - Media type;<br>4 - Host;<br>5 - Action;<br>6 - Graph;<br>11 - User group;<br>13 - Trigger;<br>14 - Host group;<br>15 - Item;<br>16 - Image;<br>17 - Value map;<br>18 - Service;<br>19 - Map;<br>22 - Web scenario;<br>23 - Discovery rule;<br>25 - Script;<br>26 - Proxy;<br>27 - Maintenance;<br>28 - Regular expression;<br>29 - Macro;<br>30 - Template;<br>31 - Trigger prototype;<br>32 - Icon mapping;<br>33 - Dashboard;<br>34 - Event correlation;<br>35 - Graph prototype;<br>36 - Item prototype;<br>37 - Host prototype;<br>38 - Autoregistration;<br>39 - Module;<br>40 - Settings;<br>41 - Housekeeping;<br>42 - Authentication;<br>43 - Template dashboard;<br>44 - User role;<br>45 - Auth token;<br>46 - Scheduled report.|
|resourceid|string|Audit log entry resource identifier.|
|resourcename|string|Audit log entry resource human readable name.|
|recordsetid|string|Audit log entry recordset ID. The audit log records created during the same operation will have the same recordset ID. Generated using CUID algorithm.|
|details|text|Audit log entry details. The details are stored as JSON object where each property name is a path to property or nested object in which change occurred, and each value contain the data about the change of this property in array format.<br><br>Possible value formats are:<br>\["add"\] - Nested object has been added;<br>\["add", "<value>"\] - The property of added object contain <value>;<br>\["update"\] - Nested object has been updated;<br>\["update", "<new value>", "<old value>"\] - The value of property of updated object was changed from <old value> to <new value>;<br>\["delete"\] - Nested object has been deleted.|

[comment]: # ({/new-63741e1f})
