[comment]: # translation:outdated

[comment]: # ({new-7fceb632})
# API info

This class is designed to retrieve meta information about the API.

Available methods:\

-   [apiinfo.version](/manual/api/reference/apiinfo/version) -
    retrieving the version of the Zabbix API

[comment]: # ({/new-7fceb632})
