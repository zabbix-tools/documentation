[comment]: # translation:outdated

[comment]: # ({new-66c5a546})
# host.massadd

[comment]: # ({/new-66c5a546})

[comment]: # ({new-55030171})
### Description

`object host.massadd(object parameters)`

This method allows to simultaneously add multiple related objects to all
the given hosts.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-55030171})

[comment]: # ({new-c25e2be4})
### Parameters

`(object)` Parameters containing the IDs of the hosts to update and the
objects to add to all the hosts.

The method accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|**hosts**<br>(required)|object/array|Hosts to be updated.<br><br>The hosts must have the `hostid` property defined.|
|groups|object/array|Host groups to add to the given hosts.<br><br>The host groups must have the `groupid` property defined.|
|interfaces|object/array|[Host interfaces](/manual/api/reference/hostinterface/object) to be created for the given hosts.|
|macros|object/array|[User macros](/manual/api/reference/usermacro/object) to be created for the given hosts.|
|templates|object/array|Templates to link to the given hosts.<br><br>The templates must have the `templateid` property defined.|

[comment]: # ({/new-c25e2be4})

[comment]: # ({new-d166b99b})
### Return values

`(object)` Returns an object containing the IDs of the updated hosts
under the `hostids` property.

[comment]: # ({/new-d166b99b})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-453a1298})
#### Adding macros

Add two new macros to two hosts.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.massadd",
    "params": {
        "hosts": [
            {
                "hostid": "10160"
            },
            {
                "hostid": "10167"
            }
        ],
        "macros": [
            {
                "macro": "{$TEST1}",
                "value": "MACROTEST1"
            },
            {
                "macro": "{$TEST2}",
                "value": "MACROTEST2",
                "description": "Test description"
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "hostids": [
            "10160",
            "10167"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-453a1298})

[comment]: # ({new-ae4115e5})
### See also

-   [host.update](update)
-   [Host group](/manual/api/reference/hostgroup/object#host_group)
-   [Template](/manual/api/reference/template/object#template)
-   [User
    macro](/manual/api/reference/usermacro/object#hosttemplate_level_macro)
-   [Host
    interface](/manual/api/reference/hostinterface/object#host_interface)

[comment]: # ({/new-ae4115e5})

[comment]: # ({new-d5e419cc})
### Source

CHost::massAdd() in *ui/include/classes/api/services/CHost.php*.

[comment]: # ({/new-d5e419cc})
