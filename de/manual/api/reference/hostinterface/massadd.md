[comment]: # translation:outdated

[comment]: # ({new-fff19f5f})
# hostinterface.massadd

[comment]: # ({/new-fff19f5f})

[comment]: # ({new-30999da7})
### Description

`object hostinterface.massadd(object parameters)`

This method allows to simultaneously add host interfaces to multiple
hosts.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-30999da7})

[comment]: # ({new-35c0864e})
### Parameters

`(object)` Parameters containing the host interfaces to be created on
the given hosts.

The method accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|**hosts**<br>(required)|object/array|Hosts to be updated.<br><br>The hosts must have the `hostid` property defined.|
|**interfaces**<br>(required)|object/array|[Host interfaces](/manual/api/reference/hostinterface/object) to create on the given hosts.|

[comment]: # ({/new-35c0864e})

[comment]: # ({new-88283807})
### Return values

`(object)` Returns an object containing the IDs of the created host
interfaces under the `interfaceids` property.

[comment]: # ({/new-88283807})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-ad87458a})
#### Creating interfaces

Create an interface on two hosts.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "hostinterface.massadd",
    "params": {
        "hosts": [
            {
                "hostid": "30050"
            },
            {
                "hostid": "30052"
            }
        ],
        "interfaces": {
            "dns": "",
            "ip": "127.0.0.1",
            "main": 0,
            "port": "10050",
            "type": 1,
            "useip": 1
        }
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "interfaceids": [
            "30069",
            "30070"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-ad87458a})

[comment]: # ({new-a2a46209})
### See also

-   [hostinterface.create](create)
-   [host.massadd](/manual/api/reference/host/massadd)
-   [Host](/manual/api/reference/host/object#host)

[comment]: # ({/new-a2a46209})

[comment]: # ({new-9b65afcc})
### Source

CHostInterface::massAdd() in
*ui/include/classes/api/services/CHostInterface.php*.

[comment]: # ({/new-9b65afcc})
