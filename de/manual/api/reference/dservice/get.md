[comment]: # translation:outdated

[comment]: # ({new-30b4a228})
# dservice.get

[comment]: # ({/new-30b4a228})

[comment]: # ({new-84c200be})
### Description

`integer/array dservice.get(object parameters)`

The method allows to retrieve discovered services according to the given
parameters.

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-84c200be})

[comment]: # ({new-cdd07a5d})
### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|dserviceids|string/array|Return only discovered services with the given IDs.|
|dhostids|string/array|Return only discovered services that belong to the given discovered hosts.|
|dcheckids|string/array|Return only discovered services that have been detected by the given discovery checks.|
|druleids|string/array|Return only discovered services that have been detected by the given discovery rules.|
|selectDRules|query|Return a [drules](/manual/api/reference/drule/object) property with an array of the discovery rules that detected the service.|
|selectDHosts|query|Return a [dhosts](/manual/api/reference/dhost/object) property with an array the discovered hosts that the service belongs to.|
|selectHosts|query|Return a [hosts](/manual/api/reference/host/object) property with the hosts with the same IP address and proxy as the service.<br><br>Supports `count`.|
|limitSelects|integer|Limits the number of records returned by subselects.<br><br>Applies to the following subselects:<br>`selectHosts` - result will be sorted by `hostid`.|
|sortfield|string/array|Sort the result by the given properties.<br><br>Possible values are: `dserviceid`, `dhostid` and `ip`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters).|
|editable|boolean|^|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

[comment]: # ({/new-cdd07a5d})

[comment]: # ({new-7223bab1})
### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-09e9b2a3})
#### Retrieve services discovered on a host

Retrieve all discovered services detected on discovered host "11".

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "dservice.get",
    "params": {
        "output": "extend",
        "dhostids": "11"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "dserviceid": "12",
            "dhostid": "11",
            "value": "",
            "port": "80",
            "status": "1",
            "lastup": "0",
            "lastdown": "1348650607",
            "dcheckid": "5",
            "ip": "192.168.1.134",
            "dns": "john.local"
        },
        {
            "dserviceid": "13",
            "dhostid": "11",
            "value": "",
            "port": "21",
            "status": "1",
            "lastup": "0",
            "lastdown": "1348650610",
            "dcheckid": "6",
            "ip": "192.168.1.134",
            "dns": "john.local"
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-09e9b2a3})

[comment]: # ({new-2647a177})
### See also

-   [Discovered
    host](/manual/api/reference/dhost/object#discovered_host)
-   [Discovery
    check](/manual/api/reference/dcheck/object#discovery_check)
-   [Host](/manual/api/reference/host/object#host)

[comment]: # ({/new-2647a177})

[comment]: # ({new-76ef9e21})
### Source

CDService::get() in *ui/include/classes/api/services/CDService.php*.

[comment]: # ({/new-76ef9e21})
