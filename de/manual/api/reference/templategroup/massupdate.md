[comment]: # translation:outdated

[comment]: # ({new-f9e8f670})
# templategroup.massupdate

[comment]: # ({/new-f9e8f670})

[comment]: # ({new-b5ff62ed})
### Description

`object templategroup.massupdate(object parameters)`

This method allows to replace templates with the specified
ones in multiple template groups.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-b5ff62ed})

[comment]: # ({new-3ff1a765})
### Parameters

`(object)` Parameters containing the IDs of the template groups to update
and the objects that should be updated.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|**groups**<br>(required)|object/array|Template groups to be updated.<br><br>The template groups must have the `groupid` property defined.|
|**templates**<br>(required)|object/array|Templates to replace the current template on the given template groups.<br>All other template, except the ones mentioned, will be excluded from template groups.<br><br>The templates must have the `templateid` property defined.|

[comment]: # ({/new-3ff1a765})

[comment]: # ({new-2acda80b})
### Return values

`(object)` Returns an object containing the IDs of the updated template
groups under the `groupids` property.

[comment]: # ({/new-2acda80b})

[comment]: # ({new-6044f589})
### Examples

[comment]: # ({/new-6044f589})

[comment]: # ({new-2f48a174})
#### Replacing templates in a template group

Replace all templates in a template group to ones mentioned templates.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "templategroup.massupdate",
    "params": {
        "groups": [
            {
                "groupid": "8"
            }
        ],
        "templates": [
            {
                "templateid": "40050"
            }
        ]
    },
    "auth": "f223adf833b2bf2ff38574a67bba6372",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "groupids": [
            "8",
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-2f48a174})

[comment]: # ({new-b67f136e})
### See also

-   [templategroup.update](update)
-   [templategroup.massadd](massadd)
-   [Template](/manual/api/reference/template/object#template)

[comment]: # ({/new-b67f136e})

[comment]: # ({new-1d8a51d4})
### Source

CTemplateGroup::massUpdate() in
*ui/include/classes/api/services/CTemplateGroup.php*.

[comment]: # ({/new-1d8a51d4})
