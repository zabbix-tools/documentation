[comment]: # translation:outdated

[comment]: # ({new-35521717})
# authentication.update

[comment]: # ({/new-35521717})

[comment]: # ({new-4244aae4})
### Description

`object authentication.update(object authentication)`

This method allows to update existing authentication settings.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-4244aae4})

[comment]: # ({new-6894664c})
### Parameters

`(object)` Authentication properties to be updated.

[comment]: # ({/new-6894664c})

[comment]: # ({new-440363c6})
### Return values

`(array)` Returns array with the names of updated parameters.

[comment]: # ({/new-440363c6})

[comment]: # ({new-f95d3166})
### Examples

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "authentication.update",
    "params": {
        "http_auth_enabled": 1,
        "http_case_sensitive": 0,
        "http_login_form": 1
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        "http_auth_enabled",
        "http_case_sensitive",
        "http_login_form"
    ],
    "id": 1
}
```

[comment]: # ({/new-f95d3166})

[comment]: # ({new-8e7357d5})
### Source

CAuthentication::update() in
*ui/include/classes/api/services/CAuthentication.php*.

[comment]: # ({/new-8e7357d5})
