[comment]: # translation:outdated

[comment]: # ({new-5ebe3ff8})
# hostgroup.create

[comment]: # ({/new-5ebe3ff8})

[comment]: # ({new-06c5b0b6})
### Description

`object hostgroup.create(object/array hostGroups)`

This method allows to create new host groups.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-06c5b0b6})

[comment]: # ({new-7c6d41ff})
### Parameters

`(object/array)` Host groups to create. The method accepts host groups
with the [standard host group properties](object#host_group).

[comment]: # ({/new-7c6d41ff})

[comment]: # ({new-e3c164f1})
### Return values

`(object)` Returns an object containing the IDs of the created host
groups under the `groupids` property. The order of the returned IDs
matches the order of the passed host groups.

[comment]: # ({/new-e3c164f1})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-37cc26c2})
#### Creating a host group

Create a host group called "Linux servers".

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "hostgroup.create",
    "params": {
        "name": "Linux servers"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "groupids": [
            "107819"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-37cc26c2})

[comment]: # ({new-df4dc6d7})
### Source

CHostGroup::create() in
*ui/include/classes/api/services/CHostGroup.php*.

[comment]: # ({/new-df4dc6d7})
