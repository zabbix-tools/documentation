[comment]: # translation:outdated

[comment]: # ({new-c07c9092})
# hostgroup.massupdate

[comment]: # ({/new-c07c9092})

[comment]: # ({new-0ec0a392})
### Description

`object hostgroup.massupdate(object parameters)`

This method allows to replace hosts and templates with the specified
ones in multiple host groups.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-0ec0a392})

[comment]: # ({new-d4028a54})
### Parameters

`(object)` Parameters containing the IDs of the host groups to update
and the objects that should be updated.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|**groups**<br>(required)|object/array|Host groups to be updated.<br><br>The host groups must have the `groupid` property defined.|
|**hosts**<br>(required)|object/array|Hosts to replace the current hosts on the given host groups.<br>All other hosts, except the ones mentioned, will be excluded from host groups.<br>Discovered hosts will not be affected.<br><br>The hosts must have the `hostid` property defined.|
|**templates**<br>(required)|object/array|Templates to replace the current templates on the given host groups.<br>All other templates, except the ones mentioned, will be excluded from host groups.<br><br>The templates must have the `templateid` property defined.|

[comment]: # ({/new-d4028a54})

[comment]: # ({new-736f3b05})
### Return values

`(object)` Returns an object containing the IDs of the updated host
groups under the `groupids` property.

[comment]: # ({/new-736f3b05})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-b4adf299})
#### Replacing hosts and templates in a host group

Replace all hosts in a host group to ones mentioned host and unlink all
templates from host group.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "hostgroup.massupdate",
    "params": {
        "groups": [
            {
                "groupid": "6"
            }
        ],
        "hosts": [
            {
                "hostid": "30050"
            }
        ],
        "templates": []
    },
    "auth": "f223adf833b2bf2ff38574a67bba6372",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "groupids": [
            "6",
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-b4adf299})

[comment]: # ({new-a1410648})
### See also

-   [hostgroup.update](update)
-   [hostgroup.massadd](massadd)
-   [Host](/manual/api/reference/host/object#host)
-   [Template](/manual/api/reference/template/object#template)

[comment]: # ({/new-a1410648})

[comment]: # ({new-243072aa})
### Source

CHostGroup::massUpdate() in
*ui/include/classes/api/services/CHostGroup.php*.

[comment]: # ({/new-243072aa})
