<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="de" datatype="plaintext" original="manual/api/reference/templatedashboard/object.md">
    <body>
      <trans-unit id="1d7fc2e5" xml:space="preserve">
        <source># &gt; Template dashboard object

The following objects are directly related to the `templatedashboard`
API.</source>
      </trans-unit>
      <trans-unit id="e0343baf" xml:space="preserve">
        <source>### Template dashboard

The template dashboard object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|dashboardid|string|ID of the template dashboard.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *read-only*&lt;br&gt;- *required* for update operations|
|name|string|Name of the template dashboard.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* for create operations|
|templateid|string|ID of the template the dashboard belongs to.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *constant*&lt;br&gt;- *required* for create operations|
|display\_period|integer|Default page display period (in seconds).&lt;br&gt;&lt;br&gt;Possible values: 10, 30, 60, 120, 600, 1800, 3600.&lt;br&gt;&lt;br&gt;Default: 30.|
|auto\_start|integer|Auto start slideshow.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - do not auto start slideshow;&lt;br&gt;1 - *(default)* auto start slideshow.|
|uuid|string|Universal unique identifier, used for linking imported template dashboards to already existing ones. Auto-generated, if not given.|</source>
      </trans-unit>
      <trans-unit id="69f1df50" xml:space="preserve">
        <source>### Template dashboard page

The template dashboard page object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|dashboard\_pageid|string|ID of the dashboard page.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *read-only*|
|name|string|Dashboard page name.&lt;br&gt;&lt;br&gt;Default: empty string.|
|display\_period|integer|Dashboard page display period (in seconds).&lt;br&gt;&lt;br&gt;Possible values: 0, 10, 30, 60, 120, 600, 1800, 3600.&lt;br&gt;&lt;br&gt;Default: 0 (will use the default page display period).|
|widgets|array|Array of the [template dashboard widget](object#template_dashboard_widget) objects.|</source>
      </trans-unit>
      <trans-unit id="9c7cc873" xml:space="preserve">
        <source>### Template dashboard widget

The template dashboard widget object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|widgetid|string|ID of the dashboard widget.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *read-only*|
|type|string|Type of the dashboard widget.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;actionlog - Action log;&lt;br&gt;clock - Clock;&lt;br&gt;*(deprecated)* dataover - Data overview;&lt;br&gt;discovery - Discovery status;&lt;br&gt;favgraphs - Favorite graphs;&lt;br&gt;favmaps - Favorite maps;&lt;br&gt;graph - Graph (classic);&lt;br&gt;graphprototype - Graph prototype;&lt;br&gt;hostavail - Host availability;&lt;br&gt;item - Item value;&lt;br&gt;map - Map;&lt;br&gt;navtree - Map Navigation Tree;&lt;br&gt;plaintext - Plain text;&lt;br&gt;problemhosts - Problem hosts;&lt;br&gt;problems - Problems;&lt;br&gt;problemsbysv - Problems by severity;&lt;br&gt;slareport - SLA report;&lt;br&gt;svggraph - Graph;&lt;br&gt;systeminfo - System information;&lt;br&gt;tophosts - Top hosts;&lt;br&gt;trigover - Trigger overview;&lt;br&gt;url - URL;&lt;br&gt;web - Web monitoring.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|
|name|string|Custom widget name.|
|x|integer|A horizontal position from the left side of the dashboard.&lt;br&gt;&lt;br&gt;Valid values range from 0 to 23.|
|y|integer|A vertical position from the top of the dashboard.&lt;br&gt;&lt;br&gt;Valid values range from 0 to 62.|
|width|integer|The widget width.&lt;br&gt;&lt;br&gt;Valid values range from 1 to 24.|
|height|integer|The widget height.&lt;br&gt;&lt;br&gt;Valid values range from 2 to 32.|
|view\_mode|integer|The widget view mode.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* default widget view;&lt;br&gt;1 - with hidden header;|
|fields|array|Array of the [template dashboard widget field](object#template_dashboard_widget_field) objects.|</source>
      </trans-unit>
      <trans-unit id="76d2eb3b" xml:space="preserve">
        <source>### Template dashboard widget field

The template dashboard widget field object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|type|integer|Type of the widget field.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - Integer;&lt;br&gt;1 - String;&lt;br&gt;4 - Item;&lt;br&gt;5 - Item prototype;&lt;br&gt;6 - Graph;&lt;br&gt;7 - Graph prototype;&lt;br&gt;8 - Map;&lt;br&gt;9 - Service;&lt;br&gt;10 - SLA;&lt;br&gt;11 - User;&lt;br&gt;12 - Action;&lt;br&gt;13 - Media type.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|
|name|string|Widget field name.&lt;br&gt;&lt;br&gt;Possible values: see [Dashboard widget fields](/manual/api/reference/dashboard/widget_fields). Note that some host-related parameters (e.g., *Host groups*, *Exclude host groups* and *Hosts* in the [*Problems*](/manual/api/reference/dashboard/widget_fields/problems#parameters) widget, *Host groups* in the [*Host availability*](/manual/api/reference/dashboard/widget_fields/host_availability#parameters) widget, etc.) are not available when configuring the widget on a template dashboard. This is because template dashboards display data only from the host that the template is linked to.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|
|value|mixed|Widget field value depending on the type.&lt;br&gt;&lt;br&gt;Possible values: see [Dashboard widget fields](/manual/api/reference/dashboard/widget_fields). Note that some host-related parameters (e.g., *Host groups*, *Exclude host groups* and *Hosts* in the [*Problems*](/manual/api/reference/dashboard/widget_fields/problems#parameters) widget, *Host groups* in the [*Host availability*](/manual/api/reference/dashboard/widget_fields/host_availability#parameters) widget, etc.) are not available when configuring the widget on a template dashboard. This is because template dashboards display data only from the host that the template is linked to.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|</source>
      </trans-unit>
    </body>
  </file>
</xliff>
