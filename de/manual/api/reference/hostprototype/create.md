[comment]: # translation:outdated

[comment]: # ({new-984854ac})
# hostprototype.create

[comment]: # ({/new-984854ac})

[comment]: # ({new-3ddb6b5d})
### Description

`object hostprototype.create(object/array hostPrototypes)`

This method allows to create new host prototypes.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-3ddb6b5d})

[comment]: # ({new-c4127cc1})
### Parameters

`(object/array)` Host prototypes to create.

Additionally to the [standard host prototype
properties](object#host_prototype), the method accepts the following
parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|**groupLinks**<br>(required)|array|Group [links](/manual/api/reference/hostprototype/object#group_link) to be created for the host prototype.|
|**ruleid**<br>(required)|string|ID of the LLD rule that the host prototype belongs to.|
|groupPrototypes|array|Group [prototypes](/manual/api/reference/hostprototype/object#group_prototype) to be created for the host prototype.|
|macros|object/array|[User macros](/manual/api/reference/usermacro/object) to be created for the host prototype.|
|tags|object/array|Host prototype [tags](/manual/api/reference/hostprototype/object#host_prototype_tag).|
|interfaces|object/array|Host prototype [custom interfaces](/manual/api/reference/hostprototype/object#custom_interface).|
|templates|object/array|[Templates](/manual/api/reference/template/object) to be linked to the host prototype.<br><br>The templates must have the `templateid` property defined.|

[comment]: # ({/new-c4127cc1})

[comment]: # ({new-b9613119})
### Return values

`(object)` Returns an object containing the IDs of the created host
prototypes under the `hostids` property. The order of the returned IDs
matches the order of the passed host prototypes.

[comment]: # ({/new-b9613119})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-0ce023ac})
#### Creating a host prototype

Create a host prototype "{\#VM.NAME}" on LLD rule "23542" with a group
prototype "{\#HV.NAME}", tag pair "Datacenter": "{\#DATACENTER.NAME}"
and custom SNMPv2 interface 127.0.0.1:161 with community
{$SNMP\_COMMUNITY}. Link it to host group "2".

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "hostprototype.create",
    "params": {
        "host": "{#VM.NAME}",
        "ruleid": "23542",
        "custom_interfaces": "1",
        "groupLinks": [
            {
                "groupid": "2"
            }
        ],
        "groupPrototypes": [
            {
                "name": "{#HV.NAME}"
            }
        ],
        "tags": [
            {
                "tag": "Datacenter",
                "value": "{#DATACENTER.NAME}"
            }
        ],
        "interfaces": [
            {
                "main": "1",
                "type": "2",
                "useip": "1",
                "ip": "127.0.0.1",
                "dns": "",
                "port": "161",
                "details": {
                    "version": "2",
                    "bulk": "1",
                    "community": "{$SNMP_COMMUNITY}"
                }
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "hostids": [
            "10103"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-0ce023ac})

[comment]: # ({new-c4a38bf3})
### See also

-   [Group link](object#group_link)
-   [Group prototype](object#group_prototype)
-   [Host prototype tag](object#host_prototype_tag)
-   [Custom interface](object#custom_interface)
-   [User
    macro](/manual/api/reference/usermacro/object#hosttemplate_level_macro)

[comment]: # ({/new-c4a38bf3})

[comment]: # ({new-4d10a354})
### Source

CHostPrototype::create() in
*ui/include/classes/api/services/CHostPrototype.php*.

[comment]: # ({/new-4d10a354})
