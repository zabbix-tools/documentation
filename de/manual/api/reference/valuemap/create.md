[comment]: # translation:outdated

[comment]: # ({new-6698fb90})
# valuemap.create

[comment]: # ({/new-6698fb90})

[comment]: # ({new-37547a16})
### Description

`object valuemap.create(object/array valuemaps)`

This method allows to create new value maps.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-37547a16})

[comment]: # ({new-efa9b6ec})
### Parameters

`(object/array)` Value maps to create.

The method accepts value maps with the [standard value map
properties](object#value_map).

[comment]: # ({/new-efa9b6ec})

[comment]: # ({new-6809040a})
### Return values

`(object)` Returns an object containing the IDs of the created value
maps the `valuemapids` property. The order of the returned IDs matches
the order of the passed value maps.

[comment]: # ({/new-6809040a})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-ace763eb})
#### Creating a value map

Create one value map with two mappings.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "valuemap.create",
    "params": {
        "hostid": "50009",
        "name": "Service state",
        "mappings": [
            {
                "type": "1",
                "value": "1",
                "newvalue": "Up"
            },
            {
                "type": "5",
                "newvalue": "Down"
            }
        ]
    },
    "auth": "57562fd409b3b3b9a4d916d45207bbcb",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "valuemapids": [
            "1"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-ace763eb})

[comment]: # ({new-58f99a13})
### Source

CValueMap::create() in *ui/include/classes/api/services/CValueMap.php*.

[comment]: # ({/new-58f99a13})
