<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="de" datatype="plaintext" original="manual/api/reference/userdirectory/test.md">
    <body>
      <trans-unit id="f9e8f670" xml:space="preserve">
        <source># userdirectory.test</source>
      </trans-unit>
      <trans-unit id="b5ff62ed" xml:space="preserve">
        <source>### Description

`object userdirectory.test(array userDirectory)`

This method allows to test user directory connection settings.

::: noteclassic
This method also allows to test what configured data matches the user directory settings for user provisioning
(e.g., what user role, user groups, user medias will be assigned to the user).
For this type of test the API request should be made for a [user directory](/manual/api/reference/userdirectory/object#userdirectory) that has `provision_status` set to enabled.
:::

::: noteclassic
This method is only available to *Super admin* user type.
:::</source>
      </trans-unit>
      <trans-unit id="d99c84a0" xml:space="preserve">
        <source>### Parameters

`(object)` User directory properties.

Since `userdirectory.get` API does not return `bind_password` field, `userdirectoryid` and/or `bind_password` should be supplied.\
Additionally to the [standard user directory properties](object#userdirectory), the method accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|test_username|string|Username to test in user directory.|
|test_password|string|Username associated password to test in user directory.|</source>
      </trans-unit>
      <trans-unit id="99a75467" xml:space="preserve">
        <source>### Return values

`(bool)` Returns true on success.</source>
      </trans-unit>
      <trans-unit id="47b9eb66" xml:space="preserve">
        <source>### Examples</source>
      </trans-unit>
      <trans-unit id="9f73d089" xml:space="preserve">
        <source>##### Test user directory for existing user

Test user directory "3" for "user1".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "userdirectory.test",
    "params": {
        "userdirectoryid": "3",
        "host": "127.0.0.1",
        "port": "389",
        "base_dn": "ou=Users,dc=example,dc=org",
        "search_attribute": "uid",
        "bind_dn": "cn=ldap_search,dc=example,dc=org",
        "bind_password": "password",
        "test_username": "user1",
        "test_password": "password"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": true,
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="a8a25347" xml:space="preserve">
        <source>##### Test user directory for non-existing user

Test user directory "3" for non-existing "user2".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "userdirectory.test",
    "params": {
        "userdirectoryid": "3",
        "host": "127.0.0.1",
        "port": "389",
        "base_dn": "ou=Users,dc=example,dc=org",
        "search_attribute": "uid",
        "bind_dn": "cn=ldap_search,dc=example,dc=org",
        "test_username": "user2",
        "test_password": "password"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "error": {
        "code": -32500,
        "message": "Application error.",
        "data": "Incorrect user name or password or account is temporarily blocked."
    },
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="54c18b14" xml:space="preserve">
        <source>##### Test user directory for user provisioning

Test userdirectory "3" for what configured data matches the user directory settings for "user3" provisioning
(e.g., what user role, user groups, user medias will be assigned to the user).

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "userdirectory.test",
    "params": {
        "userdirectoryid": "2",
        "host": "host.example.com",
        "port": "389",
        "base_dn": "DC=zbx,DC=local",
        "search_attribute": "sAMAccountName",
        "bind_dn": "CN=Admin,OU=Users,OU=Zabbix,DC=zbx,DC=local",
        "test_username": "user3",
        "test_password": "password"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "username": "user3",
        "name": "John",
        "surname": "Doe",
        "medias": [],
        "usrgrps": [
            {
                "usrgrpid": "8"
            },
            {
                "usrgrpid": "7"
            }
        ],
        "roleid": "2",
        "userdirectoryid": "2"
    },
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="0402a0f4" xml:space="preserve">
        <source>### Source

CUserDirectory::test() in *ui/include/classes/api/services/CUserDirectory.php*.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
