[comment]: # translation:outdated

[comment]: # ({new-be32ecd0})
# Trend

This class is designed to work with trend data.

Object references:\

-   [Trend](/manual/api/reference/trend/object#trend)

Available methods:\

-   [trend.get](/manual/api/reference/trend/get) - retrieving trends

[comment]: # ({/new-be32ecd0})
