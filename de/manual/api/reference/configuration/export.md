[comment]: # translation:outdated

[comment]: # ({new-690a015b})
# configuration.export

[comment]: # ({/new-690a015b})

[comment]: # ({new-98d36809})
### Description

`string configuration.export(object parameters)`

This method allows to export configuration data as a serialized string.

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-98d36809})

[comment]: # ({new-ef6ad2c8})
### Parameters

`(object)` Parameters defining the objects to be exported and the format
to use.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|**format**<br>(required)|string|Format in which the data must be exported.<br><br>Possible values:<br>`yaml` - YAML;<br>`xml` - XML;<br>`json` - JSON;<br>`raw` - unprocessed PHP array.|
|prettyprint|boolean|Make the output more human readable by adding indentation.<br><br>Possible values:<br>`true` - add indentation;<br>`false` - *(default)* do not add indentation.|
|**options**<br>(required)|object|Objects to be exported.<br><br>The `options` object has the following parameters:<br>`groups` - `(array)` IDs of host groups to export;<br>`hosts` - `(array)` IDs of hosts to export;<br>`images` - `(array)` IDs of images to export;<br>`maps` - `(array)` IDs of maps to export;<br>`mediaTypes` - `(array)` IDs of media types to export;<br>`templates` - `(array)` IDs of templates to export.<br>|

[comment]: # ({/new-ef6ad2c8})

[comment]: # ({new-0bfd9762})
### Return values

`(string)` Returns a serialized string containing the requested
configuration data.

[comment]: # ({/new-0bfd9762})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-5c446795})
#### Exporting a host

Export the configuration of a host as an XML string.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "configuration.export",
    "params": {
        "options": {
            "hosts": [
                "10161"
            ]
        },
        "format": "xml"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<zabbix_export><version>5.4</version><date>2020-03-13T15:31:45Z</date><groups><group><uuid>6f6799aa69e844b4b3918f779f2abf08</uuid><name>Zabbix servers</name></group></groups><hosts><host><host>Export host</host><name>Export host</name><groups><group><name>Zabbix servers</name></group></groups><interfaces><interface><interface_ref>if1</interface_ref></interface></interfaces><items><item><name>Item</name><key>item.key</key><delay>30s</delay><tags><tag><tag>Application</tag><value>CPU</value></tag></tags><valuemap><name>Host status</name></valuemap><interface_ref>if1</interface_ref><request_method>POST</request_method></item></items><valuemaps><valuemap><name>Host status</name><mappings><mapping><value>0</value><newvalue>Up</newvalue></mapping><mapping><value>2</value><newvalue>Unreachable</newvalue></mapping></mappings></valuemap></valuemaps></host></hosts></zabbix_export>\n",
    "id": 1
}
```

[comment]: # ({/new-5c446795})

[comment]: # ({new-a3a5fdbf})
### Source

CConfiguration::export() in
*ui/include/classes/api/services/CConfiguration.php*.

[comment]: # ({/new-a3a5fdbf})
