[comment]: # translation:outdated

[comment]: # ({new-4a04064b})
# Audit log

This class is designed to work with audit log.

Object references:\

-   [Audit log object](/manual/api/reference/auditlog/object)

Available methods:\

-   [auditlog.get](/manual/api/reference/auditlog/get) - retrieve audit
    log records

[comment]: # ({/new-4a04064b})
