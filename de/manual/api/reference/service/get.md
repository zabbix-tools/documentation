[comment]: # translation:outdated

[comment]: # ({new-2587902b})
# service.get

[comment]: # ({/new-2587902b})

[comment]: # ({new-bfd5de7f})
### Description

`integer/array service.get(object parameters)`

The method allows to retrieve services according to the given
parameters.

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-bfd5de7f})

[comment]: # ({new-e7637a1d})
### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|serviceids|string/array|Return only services with the given IDs.|
|parentids|string/array|Return only services that are linked to the given parent services.|
|deep\_parentids|flag|Return all direct and indirect child services. Used together with `parentids`.|
|childids|string/array|Return only services that are linked to the given child services.|
|evaltype|integer|Rules for tag searching.<br><br>Possible values:<br>0 - *(default)* And/Or;<br>2 - Or.|
|tags|object/array of objects|Return only services with given tags. Exact match by tag and case-sensitive or case-insensitive search by tag value depending on operator value.<br>Format: `[{"tag": "<tag>", "value": "<value>", "operator": "<operator>"}, ...]`.<br>An empty array returns all services.<br><br>Possible operator values:<br>0 - *(default)* Contains;<br>1 - Equals;<br>2 - Does not contain;<br>3 - Does not equal;<br>4 - Exists;<br>5 - Does not exist.|
|problem\_tags|object/array of objects|Return only services with given problem tags. Exact match by tag and case-sensitive or case-insensitive search by tag value depending on operator value.<br>Format: `[{"tag": "<tag>", "value": "<value>", "operator": "<operator>"}, ...]`.<br>An empty array returns all services.<br><br>Possible operator values:<br>0 - *(default)* Contains;<br>1 - Equals;<br>2 - Does not contain;<br>3 - Does not equal;<br>4 - Exists;<br>5 - Does not exist.|
|without\_problem\_tags|flag|Return only services without problem tags.|
|selectAlarms|query|Return an [alarms](/manual/api/reference/service/object#service_alarm) property with service alarms.<br><br>Supports `count`.|
|selectChildren|query|Return a `children` property with the child services.<br><br>Supports `count`.|
|selectParents|query|Return a `parents` property with the parent services.<br><br>Supports `count`.|
|selectTags|query|Return a [tags](/manual/api/reference/service/object#service_tag) property with service tags.<br><br>Supports `count`.|
|selectTimes|query|Return a [times](/manual/api/reference/service/object#service_time) property with service times.<br><br>Supports `count`.|
|selectProblemEvents|query|Return a `problem_events` property with an array of problem event objects.<br><br>The problem event object has the following properties:<br>`eventid` - *(string)* Event ID;<br>`severity` - *(string)* Current event severity;<br>`name` - *(string)* Resolved event name.<br><br>Supports `count`.|
|selectProblemTags|query|Return a [problem\_tags](/manual/api/reference/service/object#problem_tag) property with problem tags.<br><br>Supports `count`.|
|selectStatusRules|query|Return a [status\_rules](/manual/api/reference/service/object#status_rule) property with status rules.<br><br>Supports `count`.|

|   |   |   |
|---|---|---|
|sortfield|string/array|Sort the result by the given properties.<br><br>Possible values are: `name` and `sortorder`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters).|
|editable|boolean|^|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

[comment]: # ({/new-e7637a1d})

[comment]: # ({new-7223bab1})
### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-cd4e4011})
#### Retrieving all services

Retrieve all data about all services and their dependencies.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "service.get",
    "params": {
        "output": "extend",
        "selectChildren": "extend",
        "selectParents": "extend"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "serviceid": "1",
            "name": "Data centers",
            "status": "0",
            "algorithm": "1",
            "showsla": "0",
            "goodsla": "99.9",
            "sortorder": "0",
            "parents": [],
            "children": [
                {
                    "serviceid": "2",
                    "name": "Server 1",
                    "status": "0",
                    "algorithm": "1",
                    "showsla": "0",
                    "goodsla": "99.9",
                    "sortorder": "0"
                },
                {
                    "serviceid": "3",
                    "name": "Server 2",
                    "status": "0",
                    "algorithm": "1",
                    "showsla": "0",
                    "goodsla": "99.9",
                    "sortorder": "0"
                }
            ]
        },
        {
            "serviceid": "2",
            "name": "Server 1",
            "status": "0",
            "algorithm": "1",
            "showsla": "0",
            "goodsla": "99.9",
            "sortorder": "0",
            "parents": [
                {
                    "serviceid": "1",
                    "name": "Data centers",
                    "status": "0",
                    "algorithm": "1",
                    "showsla": "0",
                    "goodsla": "99.9",
                    "sortorder": "0"
                }
            ],
            "children": []
        },
        {
            "serviceid": "3",
            "name": "Server 2",
            "status": "0",
            "algorithm": "1",
            "showsla": "0",
            "goodsla": "99.9",
            "sortorder": "0",
            "parents": [
                {
                    "serviceid": "1",
                    "name": "Data centers",
                    "status": "0",
                    "algorithm": "1",
                    "showsla": "0",
                    "goodsla": "99.9",
                    "sortorder": "0"
                }
            ],
            "children": []
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-cd4e4011})

[comment]: # ({new-05e3cf18})
### Source

CService::get() in *ui/include/classes/api/services/CService.php*.

[comment]: # ({/new-05e3cf18})
