[comment]: # translation:outdated

[comment]: # ({new-bf12fc32})
# service.delete

[comment]: # ({/new-bf12fc32})

[comment]: # ({new-8ea5261f})
### Description

`object service.delete(array serviceIds)`

This method allows to delete services.

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-8ea5261f})

[comment]: # ({new-7af05f92})
### Parameters

`(array)` IDs of the services to delete.

[comment]: # ({/new-7af05f92})

[comment]: # ({new-23960c29})
### Return values

`(object)` Returns an object containing the IDs of the deleted services
under the `serviceids` property.

[comment]: # ({/new-23960c29})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-5036ec70})
#### Deleting multiple services

Delete two services.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "service.delete",
    "params": [
        "4",
        "5"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "serviceids": [
            "4",
            "5"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-5036ec70})

[comment]: # ({new-c99e5046})
### Source

CService::delete() in *ui/include/classes/api/services/CService.php*.

[comment]: # ({/new-c99e5046})
