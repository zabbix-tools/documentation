[comment]: # translation:outdated

[comment]: # ({new-7cbac97f})
# report.delete

[comment]: # ({/new-7cbac97f})

[comment]: # ({new-c6ffcbad})
### Description

`object report.delete(array reportids)`

This method allows to delete scheduled reports.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user type. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-c6ffcbad})

[comment]: # ({new-d8777019})
### Parameters

`(array)` IDs of the scheduled reports to delete.

[comment]: # ({/new-d8777019})

[comment]: # ({new-ee60543b})
### Return values

`(object)` Returns an object containing the IDs of the deleted scheduled
reports under the `reportids` property.

[comment]: # ({/new-ee60543b})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-71c35868})
#### Deleting multiple scheduled reports

Delete two scheduled reports.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "report.delete",
    "params": [
        "1",
        "2"
    ],
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "reportids": [
            "1",
            "2"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-71c35868})

[comment]: # ({new-3c3c511e})
### Source

CReport::delete() in *ui/include/classes/api/services/CReport.php*.

[comment]: # ({/new-3c3c511e})
