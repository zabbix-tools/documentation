[comment]: # translation:outdated

[comment]: # ({new-2ac76b92})
# 8. Service monitoring

[comment]: # ({/new-2ac76b92})

[comment]: # ({new-90a2641a})
#### Overview

Service monitoring functionality is intended for those who want to get a
high-level (business) view of monitored infrastructure. In many cases,
we are not interested in low-level details, like the lack of disk space,
high processor load, etc. What we are interested in is the availability
of service provided by our IT department. We can also be interested in
identifying weak places of IT infrastructure, SLA of various IT
services, the structure of existing IT infrastructure, and other
information of a higher level.

Zabbix service monitoring provides answers to all mentioned questions.

Services is a hierarchy representation of monitored data.

A very simple service structure may look like:

    Service
    |
    |-Workstations
    | |
    | |-Workstation1
    | |
    | |-Workstation2
    |
    |-Servers

Each node of the structure has attribute status. The status is
calculated and propagated to upper levels according to the selected
algorithm. The status of individual nodes is affected by the status of
the mapped problems. Mapping to problems is done using tags.

[comment]: # ({/new-90a2641a})

