<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="de" datatype="plaintext" original="manual/guides/monitor_linux.md">
    <body>
      <trans-unit id="db070ecb" xml:space="preserve">
        <source>
# 1. Monitor Linux with Zabbix agent</source>
      </trans-unit>
      <trans-unit id="92985fd3" xml:space="preserve">
        <source>
## Introduction

This page walks you through the steps required to start basic monitoring of Linux machines with Zabbix. 
The steps described in this tutorial can be applied to any Linux-based operating system. </source>
      </trans-unit>
      <trans-unit id="50d3f021" xml:space="preserve">
        <source>
**Who this guide is for**

This guide is designed for new Zabbix users and contains the minimum set of steps required to enable basic monitoring of 
your Linux machine. If you are looking for deep customization options or require more advanced configuration, 
see [Configuration](/manual/config) section of Zabbix manual.  </source>
      </trans-unit>
      <trans-unit id="4c9eae4f" xml:space="preserve">
        <source>
**Prerequisites**

Before proceeding with this installation guide, you must [download and install](https://www.zabbix.com/download) Zabbix 
server and Zabbix frontend according to instructions for your OS.  </source>
      </trans-unit>
      <trans-unit id="7e39c6ae" xml:space="preserve">
        <source>
## Install Zabbix agent

Zabbix agent is the process responsible for gathering data. 

Check your Zabbix server version:

    zabbix_server -V

Install Zabbix agent of the same version (recommended) on the Linux machine that you want to monitor. 
Based on your monitoring needs, it may be the same machine, where Zabbix server is installed, or a completely different 
machine. 

Choose the most suitable installation method:

- Run as a Docker container - see the list of available images in 
[Zabbix Docker repository](https://hub.docker.com/r/zabbix/zabbix-agent/).

- Install from Zabbix [packages](https://www.zabbix.com/download) (available for Alma Linux, CentOS, Debian, 
Oracle Linux, Raspberry Pi OS, RHEL, Rocky Linux, SUSE Linux Enterprise Server, Ubuntu). 

- Compile [from sources](/manual/installation/install). </source>
      </trans-unit>
      <trans-unit id="7c299256" xml:space="preserve">
        <source>
## Configure Zabbix for monitoring

Zabbix agent can collect metrics in active or passive mode (simultaneously). 

::: noteclassic
A passive check is a simple data request. Zabbix server or proxy asks for some data (for example, CPU load) and Zabbix 
agent sends back the result to the server. Active checks require more complex processing. The agent must first retrieve 
from the server(s) a list of items for independent processing and then bulk send the data back. 
See [Passive and active agent checks](/manual/appendix/items/activepassive) for more info.
:::

Monitoring templates provided by Zabbix usually offer two alternatives - a template for Zabbix agent and a template for 
Zabbix agent (active). With the first option, the agent will collect metrics in passive mode. 
Such templates will deliver identical monitoring results, but using different communication protocols. 

Further Zabbix configuration depends on whether you select a template for [active](#active-checks) or 
[passive](#passive-checks) Zabbix agent checks.</source>
      </trans-unit>
      <trans-unit id="05a855b6" xml:space="preserve">
        <source>
### Passive checks</source>
      </trans-unit>
      <trans-unit id="c8f5b8a4" xml:space="preserve">
        <source>
#### Zabbix frontend

1\. Log in to Zabbix frontend.

2\. [Create a host](/manual/config/hosts/host) in Zabbix web interface. 

This host will represent your Linux machine. 

3\. In the *Interfaces* parameter, add *Agent* interface and specify the IP address or DNS name of the Linux machine 
where the agent is installed. 

4\. In the *Templates* parameter, type or select *Linux by Zabbix agent*. 

![](../../../assets/en/manual/guides/linux_host_passive.png){width="600"}</source>
      </trans-unit>
      <trans-unit id="255a7ce8" xml:space="preserve">
        <source>
#### Zabbix agent

Open Zabbix agent configuration file (by default, the path is */usr/local/etc/zabbix_agentd.conf*):
 
    sudo vi /usr/local/etc/zabbix_agentd.conf

Add the IP address or DNS name of your Zabbix server to the *Server* parameter. 

For example:

    Server=192.0.2.22</source>
      </trans-unit>
      <trans-unit id="bccd276a" xml:space="preserve">
        <source>
### Active checks</source>
      </trans-unit>
      <trans-unit id="0ad06d68" xml:space="preserve">
        <source>
#### Zabbix frontend

1\. Log in to Zabbix frontend.

2\. [Create a host](/manual/config/hosts/host) in Zabbix web interface. 

This host will represent your Linux machine. 

3\. In the *Templates* parameter, type or select  *Linux by Zabbix agent active*.

![](../../../assets/en/manual/guides/linux_host_active.png){width="600"}</source>
      </trans-unit>
      <trans-unit id="2174196f" xml:space="preserve">
        <source>
#### Zabbix agent

Open Zabbix agent configuration file (by default, the path is */usr/local/etc/zabbix_agentd.conf*):
 
    sudo vi /usr/local/etc/zabbix_agentd.conf

Add:

- The name of the host you created in Zabbix web interface to the *Hostname* parameter.
- The IP address or DNS name of your Zabbix server to the *ServerActive* parameter.

For example:
  
    ServerActive= 192.0.2.22
    Hostname=Linux server</source>
      </trans-unit>
      <trans-unit id="3f93d055" xml:space="preserve">
        <source>
## View collected metrics

Congratulations! At this point, Zabbix is already monitoring your Linux machine. 

To view collected metrics, open the *Monitoring-&gt;Hosts* 
[menu section](/manual/web_interface/frontend_sections/monitoring/hosts) and click on the *Latest data* next to the host. 

![](../../../assets/en/manual/guides/linux_latest_data.png){width="600"}

This action will open a list of all the latest metrics collected from Linux server host.

![](../../../assets/en/manual/guides/linux_latest_data1.png){width="600"}</source>
      </trans-unit>
      <trans-unit id="642d128d" xml:space="preserve">
        <source>
## Set up problem alerts

Zabbix can notify you about a problem with your infrastructure using a variety of methods. 
This guide provides configuration steps for sending email alerts. 

1\. Go to the *User settings -&gt; Profile*, switch to the tab *Media* and 
[add your email](/manual/quickstart/login#adding-user).

![](../../../assets/en/manual/quickstart/new_media.png){width="600"}

2\. Follow the guide for [Receiving problem notification](/manual/quickstart/notification).

Next time, when Zabbix detects a problem you should receive an alert via email.</source>
      </trans-unit>
      <trans-unit id="655dbd9e" xml:space="preserve">
        <source>
## Test your configuration

On Linux, you can simulate high CPU load and as a result receive a problem alert by running:

    cat /dev/urandom | md5sum

You may need to run several [md5sum](https://en.wikipedia.org/wiki/Md5sum) processes for CPU load to exceed the threshold.

When Zabbix detects the problem, it will appear in the Monitoring-&gt;Problems section.

![](../../../assets/en/manual/guides/linux_problem.png){width="600"}

If the alerts are [configured](#set-up-problem-alerts), you will also receive the problem notification. </source>
      </trans-unit>
      <trans-unit id="f1a71ba4" xml:space="preserve">
        <source>
**See also:**

- [Creating an item](/manual/config/items/item) - 
how to start monitoring additional metrics (custom monitoring without templates).
- [Zabbix agent items](/manual/config/items/itemtypes/zabbix_agent), 
[Zabbix agent items for Windows](/manual/config/items/itemtypes/zabbix_agent/win_keys) - 
full list of metrics you can monitor using Zabbix agent on Windows.
- [Problem escalations](/manual/config/notifications/action/escalations) - how to create multi-step alert scenarios 
(e.g., first send message to the system administrator, then, if a problem is not resolved in 45 minutes, 
send message to the data center manager).</source>
      </trans-unit>
    </body>
  </file>
</xliff>
