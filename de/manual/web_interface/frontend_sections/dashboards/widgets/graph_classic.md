[comment]: # translation:outdated

[comment]: # ({new-84b81c15})
# 9 Graph (classic)

[comment]: # ({/new-84b81c15})

[comment]: # ({new-57f0025a})
#### Overview

In the classic graph widget, you can display a single custom graph or
simple graph.

[comment]: # ({/new-57f0025a})

[comment]: # ({new-70ee38b8})
#### Configuration

To configure, select *Graph (classic)* as type:

![](../../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/graph_classic.png)

In addition to the parameters that are [common](/manual/web_interface/frontend_sections/dashboards/widgets#common-parameters) 
for all widgets, you may set the following specific options:

|   |   |
|--|--------|
|*Source*|Select graph type:<br>**Graph** - custom graph<br>**Simple graph** - simple graph|
|*Graph*|Select the custom graph to display.<br>This option is available if 'Graph' is selected as *Source*.|
|*Item*|Select the item to display in a simple graph.<br>This option is available if 'Simple graph' is selected as *Source*.|
|*Show legend*|Unmark this checkbox to hide the legend on the graph (marked by default).|
|*[Dynamic item](/manual/web_interface/frontend_sections/dashboards#dynamic_widgets)*|Set graph to display different data depending on the selected host.|

Information displayed by the classic graph widget can be downloaded as
.png image using the [widget
menu](/manual/web_interface/frontend_sections/dashboards#widget_menu):

![](../../../../../../../assets/en/manual/web_interface/frontend_sections/dashboards/graph_download.png)

A screenshot of the widget will be saved to the Downloads folder.

[comment]: # ({/new-70ee38b8})
