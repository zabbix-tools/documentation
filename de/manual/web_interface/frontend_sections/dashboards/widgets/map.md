[comment]: # translation:outdated

[comment]: # ({new-5d9c8f6f})
# 13 Map

[comment]: # ({/new-5d9c8f6f})

[comment]: # ({new-4389647e})
#### Overview

In the map widget you can display either:

-   a single configured network map
-   one of the configured network maps in the map navigation tree (when
    clicking on the map name in the tree).

[comment]: # ({/new-4389647e})

[comment]: # ({new-574343be})
#### Configuration

To configure, select *Map* as type:

![](../../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/map.png)

In addition to the parameters that are [common](/manual/web_interface/frontend_sections/dashboards/widgets#common-parameters) 
for all widgets, you may set the following specific options:

|   |   |
|--|--------|
|*Source type*|Select to display:<br>**Map** - network map<br>**Map navigation tree** - one of the maps in the selected map navigation tree|
|*Map*|Select the map to display.<br>This option is available if 'Map' is selected as *Source type*.|
|*Filter*|Select the map navigation tree to display the maps of.<br>This option is available if 'Map navigation tree' is selected as *Source type*.|

See also: [known issue with
IE11](/manual/installation/known_issues#ie11_issue_with_map_resizing_in_dashboard_widgets)

[comment]: # ({/new-574343be})
