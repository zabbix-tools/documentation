<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="de" datatype="plaintext" original="manual/web_interface/frontend_sections/dashboards/widgets/problem_hosts.md">
    <body>
      <trans-unit id="ebb3a9a4" xml:space="preserve">
        <source># 16 Problem hosts</source>
      </trans-unit>
      <trans-unit id="3853d2c7" xml:space="preserve">
        <source>#### Overview

In the problem host widget, you can display problem count by host group and the highest problem severity within a group.

The problem count is displayed only for cause problems.</source>
      </trans-unit>
      <trans-unit id="5709a5f0" xml:space="preserve">
        <source>#### Configuration

To configure, select *Problem hosts* as type:

![](../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/problem_hosts.png){width="600"}

In addition to the parameters that are [common](/manual/web_interface/frontend_sections/dashboards/widgets#common-parameters) 
for all widgets, you may set the following specific options:

|Parameter|Description|
|--|--------|
|*Host groups*|Enter host groups to display in the widget.&lt;br&gt;This field is auto-complete so starting to type the name of a group will offer a dropdown of matching groups.&lt;br&gt;Specifying a parent host group implicitly selects all nested host groups.&lt;br&gt;Host data from these host groups will be displayed in the widget; if no host groups are entered, all host groups will be displayed.&lt;br&gt;This parameter is not available when configuring the widget on a [template dashboard](/manual/config/templates/template#adding-dashboards).|
|*Exclude host groups*|Enter host groups to hide from the widget.&lt;br&gt;This field is auto-complete so starting to type the name of a group will offer a dropdown of matching groups.&lt;br&gt;Specifying a parent host group implicitly selects all nested host groups.&lt;br&gt;Host data from these host groups will not be displayed in the widget. For example, hosts 001, 002, 003 may be in Group A and hosts 002, 003 in Group B as well. If we select to *show* Group A and *exclude* Group B at the same time, only data from host 001 will be displayed in the dashboard.&lt;br&gt;This parameter is not available when configuring the widget on a [template dashboard](/manual/config/templates/template#adding-dashboards).|
|*Hosts*|Enter hosts to display in the widget.&lt;br&gt;This field is auto-complete so starting to type the name of a host will offer a dropdown of matching hosts.&lt;br&gt;If no hosts are entered, all hosts will be displayed.&lt;br&gt;This parameter is not available when configuring the widget on a [template dashboard](/manual/config/templates/template#adding-dashboards).|
|*Problem*|You can limit the number of problem hosts displayed by the problem name.&lt;br&gt;If you enter a string here, only those hosts with problems whose name contains the entered string will be displayed.&lt;br&gt;Macros are not expanded.|
|*Severity*|Mark the problem severities to be displayed in the widget.|
|*Problem tags*|Specify problem tags to limit the number of problems displayed in the widget.&lt;br&gt;It is possible to include as well as exclude specific tags and tag values. Several conditions can be set. Tag name matching is always case-sensitive.&lt;br&gt;&lt;br&gt;There are several operators available for each condition:&lt;br&gt;**Exists** - include the specified tag names;&lt;br&gt;**Equals** - include the specified tag names and values (case-sensitive);&lt;br&gt;**Contains** - include the specified tag names where the tag values contain the entered string (substring match, case-insensitive);&lt;br&gt;**Does not exist** - exclude the specified tag names;&lt;br&gt;**Does not equal** - exclude the specified tag names and values (case-sensitive);&lt;br&gt;**Does not contain** - exclude the specified tag names where the tag values contain the entered string (substring match, case-insensitive).&lt;br&gt;&lt;br&gt;There are two calculation types for conditions:&lt;br&gt;**And/Or** - all conditions must be met, conditions having the same tag name will be grouped by the *Or* condition;&lt;br&gt;**Or** - enough if one condition is met.|
|*Show suppressed problems*|Mark the checkbox to display problems that would otherwise be suppressed (not shown) because of host maintenance.|
|*Hide groups without problems*|Mark the *Hide groups without problems* option to hide data from host groups without problems in the widget.&lt;br&gt;This parameter is not available when configuring the widget on a [template dashboard](/manual/config/templates/template#adding-dashboards).|
|*Problem display*|Display problem count as:&lt;br&gt;**All** - full problem count will be displayed;&lt;br&gt;**Separated** - unacknowledged problem count will be displayed separated as a number of the total problem count;&lt;br&gt;**Unacknowledged only** - only the unacknowledged problem count will be displayed.|</source>
      </trans-unit>
    </body>
  </file>
</xliff>
