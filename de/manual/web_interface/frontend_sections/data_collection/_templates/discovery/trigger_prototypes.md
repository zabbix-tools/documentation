[comment]: # translation:outdated

[comment]: # ({new-2b77bced})
# 2 Trigger prototypes

[comment]: # ({/new-2b77bced})

[comment]: # ({new-bcb47212})
#### Overview

In this section the configured trigger prototypes of a low-level discovery rule on the template are 
displayed. 

If the template is linked to the host, trigger prototypes will become the basis of 
creating real host [triggers](/manual/web_interface/frontend_sections/data_collection/hosts/triggers) 
during low-level discovery.

![](../../../../../../../assets/en/manual/web_interface/template_trigger_prototypes.png){width="600"}

Displayed data:

|Column|Description|
|--|--------|
|*Name*|Name of the trigger prototype, displayed as a blue link.<br>Clicking on the name opens the trigger prototype [configuration form](/manual/discovery/low_level_discovery/trigger_prototypes).<br>If the trigger prototype belongs to a linked template, the template name is displayed before the trigger name, as a gray link. Clicking on the template link will open the trigger prototype list on the linked template level.|
|*Operational data*|Format of the operational data of the trigger is displayed, containing arbitrary strings and macros that will resolve dynamically in *Monitoring* → *Problems*.|
|*Create enabled*|Create the trigger based on this prototype as:<br>**Yes** - enabled<br>**No** - disabled. You can switch between 'Yes' and 'No' by clicking on them.|
|*Discover*|Discover the trigger based on this prototype:<br>**Yes** - discover<br>**No** - do not discover. You can switch between 'Yes' and 'No' by clicking on them.|
|*Tags*|Tags of the trigger prototype are displayed.|

To configure a new trigger prototype, click on the *Create
trigger prototype* button at the top right corner.

[comment]: # ({/new-bcb47212})

[comment]: # ({new-9d1d0458})
##### Mass editing options

Buttons below the list offer some mass-editing options:

-   *Create enabled* - create these triggers as *Enabled*
-   *Create disabled* - create these triggers as *Disabled*
-   *Mass update* - mass update these trigger prototypes
-   *Delete* - delete these trigger prototypes

To use these options, mark the checkboxes before the respective
trigger prototypes, then click on the required button.

[comment]: # ({/new-9d1d0458})
