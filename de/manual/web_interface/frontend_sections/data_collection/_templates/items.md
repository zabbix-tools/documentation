[comment]: # translation:outdated

[comment]: # ({new-f72762fa})
# 1 Items

[comment]: # ({/new-f72762fa})

[comment]: # ({new-0f9f590e})
#### Overview

The item list for a template can be accessed from *Data collection →
Templates* by clicking on *Items* for the respective template.

A list of existing items is displayed.

![](../../../../../../assets/en/manual/web_interface/template_items.png){width="600"}

Displayed data:

|Column|Description|
|--|--------|
|*Item menu*|Click on the three-dot icon to open the menu for this specific item with these options:<br>**Create trigger** - create a trigger based on this item<br>**Triggers** - click to see a list with links to already-configured trigger of this item<br>**Create dependent item** - create a dependent item for this item<br>**Create dependent discovery rule** - create a dependent discovery rule for this item|
|*Template*|Template the item belongs to.<br>This column is displayed only if multiple templates are selected in the filter.|
|*Name*|Name of the item displayed as a blue link to item details.<br>Clicking on the item name link opens the item [configuration form](/manual/config/items/item#configuration).<br>If the item is inherited from another template, the template name is displayed before the item name, as a gray link. Clicking on the template link will open the item list on that template level.|
|*Triggers*|Moving the mouse over Triggers will display an infobox displaying the triggers associated with the item.<br>The number of the triggers is displayed in gray.|
|*Key*|Item key is displayed.|
|*Interval*|Frequency of the check is displayed.|
|*History*|How many days item data history will be kept is displayed.|
|*Trends*|How many days item trends history will be kept is displayed.|
|*Type*|Item type is displayed (Zabbix agent, SNMP agent, simple check, etc).|
|*Status*|Item status is displayed - *Enabled* or *Disabled*. By clicking on the status you can change it - from Enabled to Disabled (and back).|
|*Tags*|Item tags are displayed.<br>Up to three tags (name:value pairs) can be displayed. If there are more tags, a "..." link is displayed that allows to see all tags on mouseover.|

To configure a new item, click on the *Create item* button at the top
right corner.

[comment]: # ({/new-0f9f590e})

[comment]: # ({new-eaf9d65a})
##### Mass editing options

Buttons below the list offer some mass-editing options:

-   *Enable* - change item status to *Enabled*.
-   *Disable* - change item status to *Disabled*.
-   *Copy* - copy the items to other hosts or templates.
-   *Mass update* - [update several
    properties](/manual/config/items/itemupdate) for a number of items
    at once.
-   *Delete* - delete the items.

To use these options, mark the checkboxes before the respective items,
then click on the required button.

[comment]: # ({/new-eaf9d65a})

[comment]: # ({new-b1ec9c56})
##### Using filter

The item list may contain a lot of items. By using the filter, you can
filter out some of them to quickly locate the items you'ŗe looking for.
For better search performance, data is searched with macros unresolved.

The *Filter* icon is available at the top right corner. Clicking on it
will open a filter where you can specify the desired filtering criteria.

![](../../../../../../assets/en/manual/web_interface/template_item_filter.png){width="600"}

|Parameter|Description|
|--|--------|
|*Template groups*|Filter by one or more template groups.<br>Specifying a parent template group implicitly selects all nested groups.|
|*Templates*|Filter by one or more templates.|
|*Name*|Filter by item name.|
|*Key*|Filter by item key.|
|*Value mapping*|Filter by the value map used.<br>This parameter is not displayed if the *Templates* option is empty.|
|*Type*|Filter by item type (Zabbix agent, SNMP agent, etc.).|
|*Type of information*|Filter by type of information (Numeric unsigned, float, etc.).|
|*History*|Filter by how long item history is kept.|
|*Trends*|Filter by how long item trends are kept.|
|*Update interval*|Filter by item update interval.|
|*Tags*|Specify tags to limit the number of items displayed. It is possible to include as well as exclude specific tags and tag values. Several conditions can be set. Tag name matching is always case-sensitive.<br>There are several operators available for each condition:<br>**Exists** - include the specified tag names<br>**Equals** - include the specified tag names and values (case-sensitive)<br>**Contains** - include the specified tag names where the tag values contain the entered string (substring match, case-insensitive)<br>**Does not exist** - exclude the specified tag names<br>**Does not equal** - exclude the specified tag names and values (case-sensitive)<br>**Does not contain** - exclude the specified tag names where the tag values contain the entered string (substring match, case-insensitive)<br>There are two calculation types for conditions:<br>**And/Or** - all conditions must be met, conditions having the same tag name will be grouped by the Or condition<br>**Or** - enough if one condition is met|
|*Status*|Filter by item status - *Enabled* or *Disabled*.|
|*Triggers*|Filter items with (or without) triggers.|
|*Inherited*|Filter items inherited (or not inherited) from linked templates.|

The **Subfilter** below the filter offers further filtering options (for
the data already filtered). You can select groups of items with a common
parameter value. Upon clicking on a group, it gets highlighted and only
the items with this parameter value remain in the list.

[comment]: # ({/new-b1ec9c56})
