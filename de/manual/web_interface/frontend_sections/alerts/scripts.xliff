<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="de" datatype="plaintext" original="manual/web_interface/frontend_sections/alerts/scripts.md">
    <body>
      <trans-unit id="542796d3" xml:space="preserve">
        <source>
# 3 Scripts</source>
      </trans-unit>
      <trans-unit id="58923c7d" xml:space="preserve">
        <source>#### Overview

In the *Alerts → Scripts* section user-defined global scripts can be configured and maintained.

Global scripts, depending on the configured scope and also user permissions, are available for execution:

-   from the [host menu](/manual/web_interface/menu/host_menu) in various frontend locations 
    (*Dashboard*, *Problems*, *Latest data*, *Maps*, etc.)
-   from the [event menu](/manual/web_interface/menu/event_menu)
-   can be run as an action operation

The scripts are executed on Zabbix agent, Zabbix server (proxy) or Zabbix server only. 
See also [Command execution](/manual/appendix/command_execution).

Both on Zabbix agent and Zabbix proxy remote scripts are disabled by default. 
They can be enabled by:

-   For remote commands executed on Zabbix agent:
    - adding an AllowKey=system.run[&lt;command&gt;,\*] parameter for each allowed command in agent configuration,
     \* stands for wait and nowait mode;
-   For remote commands executed on Zabbix proxy:
    - **Warning: It is not required to enable remote commands on Zabbix proxy if remote commands are executed 
    on Zabbix agent that is monitored by Zabbix proxy.** If, however, it is required to execute remote commands 
    on Zabbix proxy, set *EnableRemoteCommands* parameter to '1' in the proxy configuration.

A listing of existing scripts with their details is displayed.

![](../../../../../assets/en/manual/web_interface/scripts.png){width="600"}

Displayed data:

|Column|Description|
|--|--------|
|*Name*|Name of the script. Clicking on the script name opens the script [configuration form](scripts#configuring_a_global_script).|
|*Scope*|Scope of the script - action operation, manual host action or manual event action. This setting determines where the script is available.|
|*Used in actions*|Actions where the script is used are displayed.|
|*Type*|Script type is displayed - *URL*, *Webhook*, *Script*, *SSH*, *Telnet* or *IPMI* command.|
|*Execute on*|It is displayed whether the script will be executed on Zabbix agent, Zabbix server (proxy) or Zabbix server only.|
|*Commands*|All commands to be executed within the script are displayed.|
|*User group*|The user group that the script is available to is displayed (or *All* for all user groups).|
|*Host group*|The host group that the script is available for is displayed (or *All* for all host groups).|
|*Host access*|The permission level for the host group is displayed - *Read* or *Write*. Only users with the required permission level will have access to executing the script.|

To configure a new script, click on the *Create script* button in the top right-hand corner.</source>
      </trans-unit>
      <trans-unit id="44c45ea0" xml:space="preserve">
        <source>##### Mass editing options

A button below the list offers one mass-editing option:

-   *Delete* - delete the scripts

To use this option, mark the checkboxes before the respective scripts and click on *Delete*.</source>
      </trans-unit>
      <trans-unit id="1c5dd978" xml:space="preserve">
        <source>##### Using filter

You can use the filter to display only the scripts you are interested in. For better search performance, data is 
searched with macros unresolved.

The *Filter* link is available above the list of scripts. If you click on it, a filter becomes available where you can 
filter scripts by name and scope.

![](../../../../../assets/en/manual/web_interface/script_filter.png){width="600"}</source>
      </trans-unit>
      <trans-unit id="201a0633" xml:space="preserve">
        <source>#### Configuring a global script

![](../../../../../assets/en/manual/web_interface/script.png){width="600"}

Script attributes:

|Parameter|&lt;|Description|
|-|----------|----------------------------------------|
|*Name*|&lt;|Unique name of the script.&lt;br&gt;E.g. `Clear /tmp filesystem`|
|*Scope*|&lt;|Scope of the script - action operation, manual host action or manual event action. This setting determines where the script can be used - in remote commands of action operations, from the [host menu](/manual/web_interface/menu/host_menu) or from the [event menu](/manual/web_interface/menu/event_menu) respectively.&lt;br&gt;Setting the scope to 'Action operation' makes the script available for all users with access to *Alerts* → *Actions*.&lt;br&gt;If a script is actually used in an action, its scope cannot be changed away from 'action operation'.&lt;br&gt;**Macro support**&lt;br&gt;The scope affects the range of available macros. For example, user-related macros ({USER.\*}) are supported in scripts to allow passing information about the user that launched the script. However, they are not supported if the script scope is action operation, as action operations are executed automatically.&lt;br&gt;To find out which macros are supported, do a search for 'Trigger-based notifications and commands/Trigger-based commands', 'Manual host action scripts' and 'Manual event action scripts' in the [supported macro](/manual/appendix/macros/supported_by_location) table. Note that if a macro may resolve to a value with spaces (for example, host name), don't forget to quote as needed.|
|*Menu path*|&lt;|The desired menu path to the script. For example, `Default` or `Default/`, will display the script in the respective directory. Menus can be nested, e.g. `Main menu/Sub menu1/Sub menu2`. When accessing scripts through the host/event menu in monitoring sections, they will be organized according to the given directories.&lt;br&gt;This field is displayed only if 'Manual host action' or 'Manual event action' is selected as *Scope*.|
|*Type*|&lt;|Click the respective button to select script type:&lt;br&gt;**URL**, **Webhook**, **Script**, **SSH**, **Telnet** or **[IPMI](/manual/config/notifications/action/operation/remote_command#ipmi_remote_commands)** command.&lt;br&gt; The type **URL** is available only when 'Manual host action' or 'Manual event action' is selected as *Scope*. |
| |Script type: URL|&lt;|
|^|*URL*|Specify the URL for quick access from the [host menu](/manual/web_interface/menu/host_menu) or [event menu](/manual/web_interface/menu/event_menu).&lt;br&gt;[Macros](/manual/appendix/macros/supported_by_location) and custom [user macros](/manual/config/macros/user_macros) are supported. Macro support depends on the scope of the script (see *Scope* above). &lt;br&gt; Macro values must not be URL-encoded. |
|^|*Open in new window*|Determines whether the URL should be opened in a new or the same browser tab.|
| |Script type: Webhook|&lt;|
|^|*Parameters*|Specify the webhook variables as attribute-value pairs.&lt;br&gt;See also: [Webhook](/manual/config/notifications/media/webhook) media configuration.&lt;br&gt;[Macros](/manual/appendix/macros/supported_by_location) and custom [user macros](/manual/config/macros/user_macros) are supported in parameter values. Macro support depends on the scope of the script (see *Scope* above).|
|^|*Script*|Enter the JavaScript code in the block that appears when clicking in the parameter field (or on the view/edit button next to it).&lt;br&gt;Macro support depends on the scope of the script (see *Scope* above).&lt;br&gt;See also: [Webhook](/manual/config/notifications/media/webhook) media configuration, [Additional Javascript objects](/manual/config/items/preprocessing/javascript/javascript_objects).|
|^|*Timeout*|JavaScript execution timeout (1-60s, default 30s).&lt;br&gt;Time suffixes are supported, e.g. 30s, 1m.|
| |Script type: Script|&lt;|
|^|*Execute on*|Click the respective button to execute the shell script on:&lt;br&gt;**Zabbix agent** - the script will be executed by Zabbix agent (if the system.run item is [allowed](/manual/config/items/restrict_checks)) on the host&lt;br&gt;**Zabbix server (proxy)** - the script will be executed by Zabbix server or proxy (if enabled by [EnableRemoteCommands](/manual/appendix/config/zabbix_proxy)) - depending on whether the host is monitored by server or proxy&lt;br&gt;**Zabbix server** - the script will be executed by Zabbix server only|
|^|*Commands*|Enter full path to the commands to be executed within the script.&lt;br&gt;Macro support depends on the scope of the script (see *Scope* above). Custom [user macros](/manual/config/macros/user_macros) are supported.|
| |Script type: SSH|&lt;|
|^|*Authentication method*|Select authentication method - password or public key.|
|^|*Username*|Enter the username.|
|^|*Password*|Enter the password.&lt;br&gt;This field is available if 'Password' is selected as the authentication method.|
|^|*Public key file*|Enter the path to the public key file.&lt;br&gt;This field is available if 'Public key' is selected as the authentication method.|
|^|*Private key file*|Enter the path to the private key file.&lt;br&gt;This field is available if 'Public key' is selected as the authentication method.|
|^|*Passphrase*|Enter the passphrase.&lt;br&gt;This field is available if 'Public key' is selected as the authentication method.|
|^|*Port*|Enter the port.|
|^|*Commands*|Enter the commands.&lt;br&gt;Macro support depends on the scope of the script (see *Scope* above). Custom [user macros](/manual/config/macros/user_macros) are supported.|
| |Script type: Telnet|&lt;|
|^|*Username*|Enter the username.|
|^|*Password*|Enter the password.|
|^|*Port*|Enter the port.|
|^|*Commands*|Enter the commands.&lt;br&gt;Macro support depends on the scope of the script (see *Scope* above). Custom [user macros](/manual/config/macros/user_macros) are supported.|
| |Script type: IPMI|&lt;|
|^|*Command*|Enter the IPMI command.&lt;br&gt;Macro support depends on the scope of the script (see *Scope* above). Custom [user macros](/manual/config/macros/user_macros) are supported.|
|*Description*|&lt;|Enter a description for the script.|
|*Host group*|&lt;|Select the host group that the script will be available for (or *All* for all host groups).|
|*User group*|&lt;|Select the user group that the script will be available to (or *All* for all user groups).&lt;br&gt;This field is displayed only if 'Manual host action' or 'Manual event action' is selected as *Scope*.|
|*Required host permissions*|&lt;|Select the permission level for the host group - *Read* or *Write*. Only users with the required permission level will have access to executing the script.&lt;br&gt;This field is displayed only if 'Manual host action' or 'Manual event action' is selected as *Scope*.|
|*Enable confirmation*|&lt;|Mark the checkbox to display a confirmation message before executing the script. This feature might be especially useful with potentially dangerous operations (like a reboot script) or ones that might take a long time.&lt;br&gt;This option is displayed only if 'Manual host action' or 'Manual event action' is selected as *Scope*.|
|*Confirmation text*|&lt;|Enter a custom confirmation text for the confirmation popup enabled with the checkbox above (for example, *Remote system will be rebooted. Are you sure?*). To see how the text will look like, click on *Test confirmation* next to the field.&lt;br&gt;[Macros](/manual/appendix/macros/supported_by_location) and custom [user macros](/manual/config/macros/user_macros) are supported.&lt;br&gt;*Note:* the macros will not be expanded when testing the confirmation message.&lt;br&gt;This field is displayed only if 'Manual host action' or 'Manual event action' is selected as *Scope*.|</source>
      </trans-unit>
      <trans-unit id="ca68e16a" xml:space="preserve">
        <source>#### Script execution and result

Scripts run by Zabbix server are executed by the order described in 
[Command execution](/manual/appendix/command_execution) section including exit code checking. The script result will 
be displayed in a pop-up window that will appear after the script is run.

*Note:* The return value of the script is standard output together with standard error.

See an example of a script and the result window below:

    uname -v
    /tmp/non_existing_script.sh
    echo "This script was started by {USER.USERNAME}"

![](../../../../../assets/en/manual/web_interface/frontend_sections/administration/script_result.png)

The script result does not display the script itself.</source>
      </trans-unit>
      <trans-unit id="96d6e598" xml:space="preserve">
        <source>#### Script timeout</source>
      </trans-unit>
      <trans-unit id="5b600786" xml:space="preserve">
        <source>##### Zabbix agent

You may encounter a situation when a timeout occurs while executing a script.

See an example of a script running on Zabbix agent and the result window below:

    sleep 5
    df -h

![](../../../../../assets/en/manual/web_interface/frontend_sections/administration/script_timeout_1.png)

The error message, in this case, is the following:

    Timeout while executing a shell script.

To avoid such situations, it is advised to optimize the script itself (in the example above, "5") instead of adjusting the `Timeout` parameter in [Zabbix agent configuration](/manual/appendix/config/zabbix_agentd#timeout) and [Zabbix server configuration](/manual/appendix/config/zabbix_server#timeout).
However, for Zabbix agent in active mode, the `Timeout` parameter in [Zabbix server configuration](/manual/appendix/config/zabbix_server#timeout) should be at least several seconds longer than the `RefreshActiveChecks` parameter in [Zabbix agent configuration](/manual/appendix/config/zabbix_agentd#refreshactivechecks).
This ensures that the server has enough time to receive the active check results from the agent. Note that script execution on an active agent is supported since Zabbix agent 7.0.

In case the `Timeout` parameter has been changed in [Zabbix agent configuration](/manual/appendix/config/zabbix_agentd), the following error message will appear:

    Get value from agent failed: ZBX_TCP_READ() timed out.

It means that the modification has been made in [Zabbix agent configuration](/manual/appendix/config/zabbix_agentd), but it is required to modify the `Timeout` parameter in [Zabbix server configuration](/manual/appendix/config/zabbix_server) as well.</source>
      </trans-unit>
      <trans-unit id="0b38e381" xml:space="preserve">
        <source>##### Zabbix server/proxy

See an example of a script running on Zabbix server and the result window below:

    sleep 11
    df -h

![](../../../../../assets/en/manual/web_interface/frontend_sections/administration/script_timeout_3.png)

It is also advised to optimize the script itself (instead of adjusting `TrapperTimeout` parameter to a corresponding value 
(in our case, &gt; ‘11’) by modifying the [Zabbix server configuration](/manual/appendix/config/zabbix_server)).</source>
      </trans-unit>
    </body>
  </file>
</xliff>
