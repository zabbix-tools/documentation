[comment]: # translation:outdated

[comment]: # ({new-fa188d70})
# 5 Administration

[comment]: # ({/new-fa188d70})

[comment]: # ({new-cded1b68})
#### Overview

The Administration menu is for administrative functions of Zabbix. This
menu is available to users of [Super
Administrators](/manual/config/users_and_usergroups/permissions) type
only.

[comment]: # ({/new-cded1b68})
