[comment]: # translation:outdated

[comment]: # ({new-7d551978})
# 6 Action log

[comment]: # ({/new-7d551978})

[comment]: # ({new-8be899f9})
#### Overview

In the Reports → Action log section users can view details of operations
(notifications, remote commands) executed within an action.

![](../../../../../assets/en/manual/web_interface/action_log2.png){width="600"}

Displayed data:

|Column|Description|
|------|-----------|
|*Time*|Timestamp of the operation.|
|*Action*|Name of the action causing operations is displayed.|
|*Type*|Operation type is displayed - *Email* or *Command*.|
|*Recipient(s)*|Username, name, surname (in parentheses) and e-mail address of the notification recipient is displayed.|
|*Message*|The content of the message/remote command is displayed.<br>A remote command is separated from the target host with a colon symbol: `<host>:<command>`. If the remote command is executed on Zabbix server, then the information has the following format: `Zabbix server:<command>`|
|*Status*|Operation status is displayed:<br>*In progress* - action is in progress<br>For actions in progress the number of retries left is displayed - the remaining number of times the server will try to send the notification.<br>*Sent* - notification has been sent<br>*Executed* - command has been executed<br>*Not sent* - action has not been completed.|
|*Info*|Error information (if any) regarding the action execution is displayed.|

**Using filter**

You may use the filter to narrow down the records by the message
recipient(s). For better search performance, data is searched with
macros unresolved.

The filter is located below the *Action log* bar. It can be opened and
collapsed by clicking on the *Filter* tab on the left.

**Time period selector**

The [time period
selector](/manual/config/visualization/graphs/simple#time_period_selector)
allows to select often required periods with one mouse click. The time
period selector can be opened by clicking on the time period tab next to
the filter.

[comment]: # ({/new-8be899f9})

[comment]: # ({new-d84e7bb0})
#### Buttons

The button at the top right corner of the page offers the following option:

|   |   |
|--|--------|
|![](../../../../../assets/en/manual/web_interface/button_csv.png)|Export action log records from all pages to a CSV file. If a filter is applied, only the filtered records will be exported.<br>In the exported CSV file the columns "Recipient" and "Message" are divided into several columns - "Recipient's Zabbix username", "Recipient's name", "Recipient's surname", "Recipient", and "Subject", "Message", "Command".|

[comment]: # ({/new-d84e7bb0})

[comment]: # ({new-accea306})
#### Using filter

The filter is located below the *Action log* bar.
It can be opened and collapsed by clicking on the *Filter* tab at the top right corner of the page.

![](../../../../../assets/en/manual/web_interface/action_log_filter.png){width="600"}

You may use the filter to narrow down the records by notification recipients, actions, media types, status,
or by the message/remote command content (*Search string*).
For better search performance, data is searched with macros unresolved.

[comment]: # ({/new-accea306})

[comment]: # ({new-3eac5e48})
#### Time period selector

The [time period selector](/manual/config/visualization/graphs/simple#time_period_selector) allows to select often required periods with one mouse click.
The time period selector can be opened by clicking on the time period tab next to the filter.

[comment]: # ({/new-3eac5e48})
