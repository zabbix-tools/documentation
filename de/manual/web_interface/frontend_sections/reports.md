[comment]: # translation:outdated

[comment]: # ({new-9ada3186})
# 3 Reports

[comment]: # ({/new-9ada3186})

[comment]: # ({new-8b50039d})
#### Overview

The Reports menu features several sections that contain a variety of
predefined and user-customizable reports focused on displaying an
overview of such parameters as system information, triggers and gathered
data.

[comment]: # ({/new-8b50039d})
