[comment]: # translation:outdated

[comment]: # ({new-e923eaa6})

# 1 Event menu

[comment]: # ({/new-e923eaa6})

[comment]: # ({new-17351e40})

## Overview

The event menu is accessible by clicking on the problem name in [supported locations](#supported-locations).

![](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/event_menu.png)

[comment]: # ({/new-17351e40})

[comment]: # ({new-2adcfe6d})

## Content

The event menu allows to:

- view all unresolved problems of the underlying trigger
- access the trigger configuration
- access a simple graph/item history of the underlying item(s)
- access an external ticket of the problem (if configured, see the *Include event menu entry* option when configuring
  [webhooks](/manual/config/notifications/media/webhook))
- access a [trigger URL](/manual/config/triggers/trigger#configuration) (if defined)
- add custom links.  To add a URL, you need to create a global
  [script](/manual/web_interface/frontend_sections/alerts/scripts) with scope 'Manual event action' and type 'URL'.
  Click on the script name to open the URL.
- execute global [scripts](/manual/web_interface/frontend_sections/alerts/scripts) (these scripts need to have their
  scope defined as 'Manual event action'). This feature may be handy for running scripts used for managing problem
  tickets in external systems.

[comment]: # ({/new-2adcfe6d})

[comment]: # ({new-25185444})

## Supported locations

The event menu is accessible by clicking on a problem or event name in various frontend sections, for example:

- Dashboards [widgets](/manual/web_interface/frontend_sections/dashboards/widgets), such as Problems, Trigger overview,
  etc.
- Monitoring → [Problems](/manual/web_interface/frontend_sections/monitoring/problems)
- Monitoring → [Problems](/manual/web_interface/frontend_sections/monitoring/problems) → Event details
- Reports → [Triggers top 100](/manual/web_interface/frontend_sections/reports/triggers_top) (global scripts and access
  to external ticket are not supported in this location)

[comment]: # ({/new-25185444})
