[comment]: # translation:outdated

[comment]: # ({new-39f5a4d3})
# 7 Definitions

[comment]: # ({/new-39f5a4d3})

[comment]: # ({new-a9697948})
#### Overview

While many things in the frontend can be configured using the frontend
itself, some customisations are currently only possible by editing a
definitions file.

This file is `defines.inc.php` located in /include of the Zabbix HTML
document directory.

[comment]: # ({/new-a9697948})

[comment]: # ({new-3260c875})
#### Parameters

Parameters in this file that could be of interest to users:

-   ZBX\_MIN\_PERIOD

Minimum graph period, in seconds. One minute by default.

-   GRAPH\_YAXIS\_SIDE\_DEFAULT

Default location of Y axis in simple graphs and default value for drop
down box when adding items to custom graphs. Possible values: 0 - left,
1 - right.

Default: 0

-   ZBX\_SESSION\_NAME (available since 4.0.0)

String used as the name of the Zabbix frontend session cookie.

Default: zbx\_sessionid

-   ZBX\_DATA\_CACHE\_TTL (available since 5.2.0)

TTL timeout in seconds used to invalidate data cache of [Vault
response](/manual/config/secrets). Set 0 to disable Vault response
caching.

Default: 60

[comment]: # ({/new-3260c875})
