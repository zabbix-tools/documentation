[comment]: # translation:outdated

[comment]: # ({new-ec525f2f})
# 7 Discovery of systemd services

[comment]: # ({/new-ec525f2f})

[comment]: # ({new-6e5dadb8})
#### Overview

It is possible to [discover](/manual/discovery/low_level_discovery)
systemd units (services, by default) with Zabbix.

[comment]: # ({/new-6e5dadb8})

[comment]: # ({new-e655c9cf})
#### Item key

The item to use in the [discovery
rule](/manual/discovery/low_level_discovery#discovery_rule) is the

    systemd.unit.discovery

::: noteimportant
This
[item](/manual/config/items/itemtypes/zabbix_agent/zabbix_agent2) key is
only supported in Zabbix agent 2.
:::

This item returns a JSON with information about systemd units, for
example:

    [{
        "{#UNIT.NAME}": "mysqld.service",
        "{#UNIT.DESCRIPTION}": "MySQL Server",
        "{#UNIT.LOADSTATE}": "loaded",
        "{#UNIT.ACTIVESTATE}": "active",
        "{#UNIT.SUBSTATE}": "running",
        "{#UNIT.FOLLOWED}": "",
        "{#UNIT.PATH}": "/org/freedesktop/systemd1/unit/mysqld_2eservice",
        "{#UNIT.JOBID}": 0,
        "{#UNIT.JOBTYPE}": ""
        "{#UNIT.JOBPATH}": "/",
        "{#UNIT.UNITFILESTATE}": "enabled"
    }, {
        "{#UNIT.NAME}": "systemd-journald.socket",
        "{#UNIT.DESCRIPTION}": "Journal Socket",
        "{#UNIT.LOADSTATE}": "loaded",
        "{#UNIT.ACTIVESTATE}": "active",
        "{#UNIT.SUBSTATE}": "running",
        "{#UNIT.FOLLOWED}": "",
        "{#UNIT.PATH}": "/org/freedesktop/systemd1/unit/systemd_2djournald_2esocket",
        "{#UNIT.JOBID}": 0,
        "{#UNIT.JOBTYPE}": "",
        "{#UNIT.JOBPATH}": "/"
        "{#UNIT.UNITFILESTATE}": "enabled"
    }]

[comment]: # ({/new-e655c9cf})

[comment]: # ({new-2d3b32d7})

##### Discovery of disabled systemd units

Since Zabbix 6.0.1 it is also possible to discover **disabled** systemd units. In this case 
three macros are returned in the resulting JSON: 

-    {#UNIT.PATH}
-    {#UNIT.ACTIVESTATE}
-    {#UNIT.UNITFILESTATE}. 

::: noteimportant
To have items and triggers created from prototypes for disabled systemd units, make sure to 
adjust (or remove) prohibiting LLD filters for {#UNIT.ACTIVESTATE} and {#UNIT.UNITFILESTATE}.
:::

[comment]: # ({/new-2d3b32d7})

[comment]: # ({new-0272b801})
#### Supported macros

The following macros are supported for use in the discovery rule
[filter](/manual/discovery/low_level_discovery#discovery_rule_filter)
and prototypes of items, triggers and graphs:

|Macro|Description|
|-----|-----------|
|{\#UNIT.NAME}|Primary unit name.|
|{\#UNIT.DESCRIPTION}|Human readable description.|
|{\#UNIT.LOADSTATE}|Load state (i.e. whether the unit file has been loaded successfully)|
|{\#UNIT.ACTIVESTATE}|Active state (i.e. whether the unit is currently started or not)|
|{\#UNIT.SUBSTATE}|Sub state (a more fine-grained version of the active state that is specific to the unit type, which the active state is not)|
|{\#UNIT.FOLLOWED}|Unit that is being followed in its state by this unit, if there is any; otherwise an empty string.|
|{\#UNIT.PATH}|Unit object path.|
|{\#UNIT.JOBID}|Numeric job ID if there is a job queued for the job unit; 0 otherwise.|
|{\#UNIT.JOBTYPE}|Job type.|
|{\#UNIT.JOBPATH}|Job object path.|
|{\#UNIT.UNITFILESTATE}|The install state of the unit file.|

[comment]: # ({/new-0272b801})

[comment]: # ({new-c8363d40})
#### Item prototypes

Item prototypes that can be created based on systemd service discovery
include, for example:

-   Item name: `{#UNIT.DESCRIPTION}`; item key:
    `systemd.unit.info["{#UNIT.NAME}"]`
-   Item name: `{#UNIT.DESCRIPTION}`; item key:
    `systemd.unit.info["{#UNIT.NAME}",LoadState]`

`systemd.unit.info` [agent
items](/manual/config/items/itemtypes/zabbix_agent/zabbix_agent2) are
supported since Zabbix 4.4.

[comment]: # ({/new-c8363d40})
