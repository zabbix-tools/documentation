[comment]: # translation:outdated

[comment]: # ({new-bd3d4e41})
# 2. Definitions

[comment]: # ({/new-bd3d4e41})

[comment]: # ({new-99b1c1f4})
#### Overview

In this section you can learn the meaning of some terms commonly used in
Zabbix.

[comment]: # ({/new-99b1c1f4})

[comment]: # ({new-77400725})
#### Definitions

***[host](/manual/config/hosts)***

\- *a networked device that you want to monitor, with IP/DNS.*

***[host group](/manual/config/hosts)***

\- *a logical grouping of hosts; it may contain hosts and templates.
Hosts and templates within a host group are not in any way linked to
each other. Host groups are used when assigning access rights to hosts
for different user groups.*

***[item](/manual/config/items)***

\- *a particular piece of data that you want to receive off of a host, a
metric of data.*

***[value
preprocessing](/manual/config/items/item#item_value_preprocessing)***

\- *a transformation of received metric value* before saving it to the
database.

***[trigger](/manual/config/triggers)***

\- *a logical expression that defines a problem threshold and is used to
"evaluate" data received in items.*

When received data are above the threshold, triggers go from 'Ok' into a
'Problem' state. When received data are below the threshold, triggers
stay in/return to an 'Ok' state.

***[event](/manual/config/events)***

\- *a single occurrence of something that deserves attention such as a
trigger changing state or a discovery/agent autoregistration taking
place.*

***[event tag](/manual/config/event_correlation/trigger/event_tags)***

\- *a pre-defined marker for the event.* It may be used in event
correlation, permission granulation, etc.

***[event correlation](/manual/config/event_correlation)***

\- *a method of correlating problems to their resolution flexibly and
precisely.*

For example, you may define that a problem reported by one trigger may
be resolved by another trigger, which may even use a different data
collection method.

***[problem](/manual/web_interface/frontend_sections/monitoring/problems)***

\- *a trigger that is in "Problem" state.*

***[problem update](/manual/acknowledges#updating_problems)***

\- *problem management options provided by Zabbix, such as adding
comment, acknowledging, changing severity or closing manually.*

***[action](/manual/config/notifications/action)***

\- *a predefined means of reacting to an event.*

An action consists of operations (e.g. sending a notification) and
conditions (*when* the operation is carried out)

***[escalation](/manual/config/notifications/action/escalations)***

\- *a custom scenario for executing operations within an action; a
sequence of sending notifications/executing remote commands.*

***[media](/manual/config/notifications/media)***

\- *a means of delivering notifications; delivery channel.*

***[notification](/manual/config/notifications/action/operation/message)***

\- *a message about some event sent to a user via the chosen media
channel.*

***[remote
command](/manual/config/notifications/action/operation/remote_command)***

\- *a pre-defined command that is automatically executed on a monitored
host upon some condition.*

***[template](/manual/config/templates)***

\- *a set of entities (items, triggers, graphs, low-level discovery
rules, web scenarios) ready to be applied to one or several hosts.*

The job of templates is to speed up the deployment of monitoring tasks
on a host; also to make it easier to apply mass changes to monitoring
tasks. Templates are linked directly to individual hosts.

***[web scenario](/manual/web_monitoring)***

\- *one or several HTTP requests to check the availability of a web
site.*

***[frontend](/manual/introduction/overview#architecture)***

\- *the web interface provided with Zabbix.*

***[dashboard](/manual/web_interface/frontend_sections/monitoring/dashboard)***

\- *customizable section of the web interface displaying summaries and
vizualizations* of important information in visual units called widgets.

***[widget](/manual/web_interface/frontend_sections/monitoring/dashboard/widgets)***

\- *visual unit displaying information of a certain kind and source* (a
summary, a map, a graph, the clock, etc), used in the dashboard.

***[Zabbix API](/manual/api)***

\- *Zabbix API allows you to use the JSON RPC protocol to create, update
and fetch Zabbix objects (like hosts, items, graphs and others) or
perform any other custom tasks.*

***[Zabbix server](/manual/concepts/server)***

\- *a central process of Zabbix software that performs monitoring,
interacts with Zabbix proxies and agents, calculates triggers, sends
notifications; a central repository of data.*

***[Zabbix proxy](/manual/concepts/proxy)***

\- *a process that may collect data on behalf of Zabbix server, taking
some processing load off of the server.*

***[Zabbix agent](/manual/concepts/agent)***

\- *a process deployed on monitoring targets to actively monitor local
resources and applications.*

***[Zabbix agent 2](/manual/concepts/agent2)***

\- *a new generation of Zabbix agent to actively monitor local resources
and applications, allowing to use custom plugins for monitoring.*

::: noteimportant
Because Zabbix agent 2 shares much functionality
with Zabbix agent, the term "Zabbix agent" in documentation stands for
both - Zabbix agent and Zabbix agent 2, if the functional behavior is
the same. Zabbix agent 2 is only specifically named where its
functionality differs.
:::

***[encryption](/manual/encryption)***

\- *support of encrypted communications between Zabbix components
(server, proxy, agent, zabbix\_sender and zabbix\_get utilities)* using
Transport Layer Security (TLS) protocol.

***[network discovery](/manual/discovery/network_discovery)***

\- *automated discovery of network devices*.

***[low-level discovery](/manual/discovery/low_level_discovery)***

\- *automated discovery of low-level entities on a particular device*
(e.g. file systems, network interfaces, etc).

***[low-level discovery
rule](/manual/discovery/low_level_discovery#discovery_rule)***

\- *set of definitions for automated discovery of low-level entities* on
a device.

***[item
prototype](/manual/discovery/low_level_discovery#item_prototypes)***

\- *a metric with certain parameters as variables, ready for low-level
discovery*. After low-level discovery the variables are automatically
substituted with the real discovered parameters and the metric
automatically starts gathering data.

***[trigger
prototype](/manual/discovery/low_level_discovery#trigger_prototypes)***

\- *a trigger with certain parameters as variables, ready for low-level
discovery*. After low-level discovery the variables are automatically
substituted with the real discovered parameters and the trigger
automatically starts evaluating data.

*Prototypes* of some other Zabbix entities are also in use in low-level
discovery - graph prototypes, host prototypes, host group prototypes.

***[agent autoregistration](/manual/discovery/auto_registration)***

\- *automated process whereby a Zabbix agent itself is registered* as a
host and started to monitor.

[comment]: # ({/new-77400725})
