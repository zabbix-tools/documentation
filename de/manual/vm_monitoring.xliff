<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="de" datatype="plaintext" original="manual/vm_monitoring.md">
    <body>
      <trans-unit id="3ca85094" xml:space="preserve">
        <source># 10. Virtual machine monitoring</source>
      </trans-unit>
      <trans-unit id="621e23e6" xml:space="preserve">
        <source>### Overview

Support of monitoring VMware environments is available in Zabbix starting with version 2.2.0.

Zabbix can use [low-level discovery](/manual/discovery/low_level_discovery) rules to automatically discover VMware hypervisors and virtual machines and create hosts to monitor them,
based on pre-defined [host prototypes](/manual/discovery/low_level_discovery/host_prototypes).

The default dataset in Zabbix offers several ready-to-use templates for monitoring VMware vCenter or ESX hypervisor.

The minimum required VMware vCenter or vSphere version is 5.1.</source>
      </trans-unit>
      <trans-unit id="4c2e8939" xml:space="preserve">
        <source>#### Data collection

The virtual machine monitoring is performed in two steps: 

1. *vmware collector* Zabbix processes gather virtual machine data. 
The processes obtain necessary information from VMware web services over the SOAP protocol, pre-process it and store in Zabbix server shared memory. 

2. Poller processes retrieve the data using Zabbix simple check [VMware keys](/manual/config/items/itemtypes/simple_checks/vmware_keys).

Zabbix divides collected data into two types: VMware configuration data and VMware performance counter data.
Both types are collected independently by *vmware collectors*. 
It is recommended to enable more collectors than the number of monitored VMware services. 
Otherwise, the retrieval of VMware performance counter statistics might be delayed by the retrieval of VMware configuration data (which takes a while for large installations).

The following statistics are available based on the VMware performance counter information:

- datastore;
- disk device;
- CPU;
- power;
- network interface;
- custom performance counter items;
- etc.

See [VMware item keys](/manual/vm_monitoring/vmware_keys#footnotes) for the full list of items that obtain data from VMware performance counters. </source>
      </trans-unit>
      <trans-unit id="68c3f7d5" xml:space="preserve">
        <source>### Configuration

If Zabbix server is compiled from sources, 
it should be [compiled](/manual/installation/install#configure_the_sources) with the **--with-libxml2** and **--with-libcurl** compilation options to enable virtual machine monitoring.
Zabbix packages are compiled with these options enabled.

The following configuration file options can be used to tune the Virtual machine monitoring:

-   **StartVMwareCollectors** - the number of pre-forked *vmware collector* instances.\
    This value depends on the number of VMware services you are going to monitor. For the most cases this should be:\
    *servicenum &lt; StartVMwareCollectors &lt; (servicenum \* 2)*\
    where *servicenum* is the number of VMware services.\ 
    E. g., if you have 1 VMware service to monitor set StartVMwareCollectors to 2, 
    if you have 3 VMware services set it to 5. 
    In most cases, this value should not be less than 2 and should not be 2 times greater than the number of monitored VMware services. 
    Required number of collectors also depends on your VMware environment size and *VMwareFrequency* and *VMwarePerfFrequency* configuration parameters (see below).
-   **VMwareCacheSize**
-   **VMwareFrequency**
-   **VMwarePerfFrequency**
-   **VMwareTimeout**

For more details, see the configuration file pages for Zabbix [server](/manual/appendix/config/zabbix_server) and [proxy](/manual/appendix/config/zabbix_proxy).

::: noteimportant
To support datastore capacity metrics, Zabbix requires VMware configuration parameter *vpxd.stats.maxQueryMetrics* to be at least 64. 
See also the VMware knowledge base [article](https://kb.vmware.com/s/article/2107096).
:::</source>
      </trans-unit>
      <trans-unit id="e5966479" xml:space="preserve">
        <source>#### Discovery

Zabbix can use a low-level discovery rule (for example, *vmware.hv.discovery\[{$VMWARE.URL}\]*) to automatically discover VMware hypervisors and virtual machines. 
Host prototypes can be created with the low-level discovery rule. 
When virtual machines are discovered, these prototypes become real hosts. 
See [low-level discovery](/manual/discovery/low_level_discovery) for more information.</source>
      </trans-unit>
      <trans-unit id="2f026fa1" xml:space="preserve">
        <source>#### Ready-to-use templates

The default dataset in Zabbix offers several ready-to-use templates for monitoring VMware vCenter or directly ESX hypervisor. 
These templates contain pre-configured [LLD](/manual/discovery/low_level_discovery) rules as well as a number of built-in checks for monitoring virtual installations.

Templates for VMware vCenter and ESX hypervisor monitoring:

-   *VMware* - uses UUID data for corresponding macros;
-   *VMware FQDN* - uses FQDN data for corresponding macros.

::: noteclassic
 In order for the *VMware FQDN* template to work correctly each monitored VM should have a unique OS name compliant with FQDN rules and VMware Tools must be installed on every machine. 
 If these conditions are met, it is recommended to use *VMware FQDN* template. 
 The creation of *VMware FQDN* template became possible after introducing the ability to create hosts with custom interfaces in Zabbix 5.2. &lt;br&gt;
A classic *VMware* template is still available and can be used if FQDN requirements cannot be met. 
Please keep in mind, that the *VMware* template has a known issue. 
Hosts for discovered virtual machines will be created with the names saved in the vCenter (for example, VM1, VM2, etc.). 
If Zabbix agent active is installed on these hosts later with autoregistration enabled, 
the autoregistration process will read host names as they have been registered upon launch 
(for example, vm1.example.com, vm2.example.com, etc.) and create new hosts since no name matches have been found. 
As a result, there will be two duplicate hosts for each machine with different names. 
:::

Templates used by discovery (normally, these templates should not be manually linked to a host):

-   *VMware Hypervisor*;
-   *VMware Guest*.

![](../../assets/en/manual/vm_monitoring/templates_vm.png){width="600"}</source>
      </trans-unit>
      <trans-unit id="317ec2ff" xml:space="preserve">
        <source>#### Host configuration

To use VMware simple checks the host must have the following user macros defined:

-   **{$VMWARE.URL}** - VMware service (vCenter or ESX hypervisor) SDK
    URL (&lt;https://servername/sdk&gt;)
-   **{$VMWARE.USERNAME}** - VMware service user name
-   **{$VMWARE.PASSWORD}** - VMware service {$VMWARE.USERNAME} user
    password</source>
      </trans-unit>
      <trans-unit id="96cbfb15" xml:space="preserve">
        <source>#### Example

The following example demonstrates how to quickly setup VMware monitoring on Zabbix:

::: noteclassic
It is assumed that VMware installation is up and running.
:::

1. Set the StartVMwareCollectors option in Zabbix server configuration file to 1 or more (the default value is 0).
2. Create a new host:
- *Host name* any name of your choice.
- *Templates* - type or select *VMware*

![](../../assets/en/manual/vm_monitoring/vm_host_templates.png)

- *Host group* - any host group
- *Interface* - any dummy interface; this field is required, but the interface will not be used.
- *Macros* - set host macros required for VMware authentication: {$USERNAME}, {$PASSWORD}, {$URL}.

![](../../assets/en/manual/vm_monitoring/vm_host_macros.png)
    
-   click on the **Add** button to save the host.</source>
      </trans-unit>
      <trans-unit id="44948bb1" xml:space="preserve">
        <source>### Extended logging

The data gathered by VMware collector can be logged for detailed debugging using debug level 5. 
This level can be set in [server](/manual/appendix/config/zabbix_server) and [proxy](/manual/appendix/config/zabbix_proxy) configuration files or
using a runtime control option (`-R log_level_increase="vmware collector,N"`, where N is a process number). 

For example, if debug level 4 has already been set, you can increase it to level 5 by running:

    # Increase log level of all vmware collectors:
    zabbix_server -R log_level_increase="vmware collector"

    # Increase log level of the second vmware collector:
    zabbix_server -R log_level_increase="vmware collector,2"
  
If extended logging of VMware collector data is not required it can be stopped using the `-R log_level_decrease` option.</source>
      </trans-unit>
      <trans-unit id="f6f9d3ef" xml:space="preserve">
        <source>### Troubleshooting

-   In case of unavailable metrics, please make sure that they are not made unavailable or turned off by default in recent VMware vSphere versions 
or if some limits are not placed on performance-metric database queries. 
See [ZBX-12094](https://support.zabbix.com/browse/ZBX-12094) for additional details.
-   If 'config.vpxd.stats.maxQueryMetrics' is invalid or
    exceeds the maximum number of characters permitted error, 
    add a `config.vpxd.stats.maxQueryMetrics` parameter to the vCenter Server settings. 
    The value of this parameter should be the same as the value of `maxQuerysize` in VMware's *web.xml*. 
    See the VMware knowledge base [article](https://kb.vmware.com/s/article/2107096) for details.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
