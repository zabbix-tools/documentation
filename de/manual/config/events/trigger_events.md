[comment]: # translation:outdated

[comment]: # ({new-e0e52cee})
# 1 Trigger event generation

[comment]: # ({/new-e0e52cee})

[comment]: # ({new-5a424184})
#### Overview

Change of trigger status is the most frequent and most important source
of events. Each time the trigger changes its state, an event is
generated. The event contains details of the trigger state's change -
when it happened and what the new state is.

Two types of events are created by triggers - Problem and OK.

[comment]: # ({/new-5a424184})

[comment]: # ({new-b13ee7a9})
#### Problem events

A problem event is created:

-   when a trigger expression evaluates to TRUE if the trigger is in OK
    state;
-   each time a trigger expression evaluates to TRUE if multiple problem
    event generation is enabled for the trigger.

[comment]: # ({/new-b13ee7a9})

[comment]: # ({new-6f89a5a0})
#### OK events

An OK event closes the related problem event(s) and may be created by 3
components:

-   triggers - based on 'OK event generation' and 'OK event closes'
    settings;
-   event correlation
-   task manager – when an event is [manually
    closed](/manual/config/events/manual_close)

[comment]: # ({/new-6f89a5a0})

[comment]: # ({new-8e1a9fc3})
##### Triggers

Triggers have an 'OK event generation' setting that controls how OK
events are generated:

-   *Expression* - an OK event is generated for a trigger in problem
    state when its expression evaluates to FALSE. This is the simplest
    setting, enabled by default.
-   *Recovery expression* - an OK event is generated for a trigger in
    problem state when its expression evaluates to FALSE and the
    recovery expression evaluates to TRUE. This can be used if trigger
    recovery criteria is different from problem criteria.
-   *None* - an OK event is never generated. This can be used in
    conjunction with multiple problem event generation to simply send a
    notification when something happens.

Additionally triggers have an 'OK event closes' setting that controls
which problem events are closed:

-   *All problems* - an OK event will close all open problems created by
    the trigger
-   *All problems if tag values match* - an OK event will close open
    problems created by the trigger and having at least one matching tag
    value. The tag is defined by 'Tag for matching' trigger setting. If
    there are no problem events to close then OK event is not generated.
    This is often called trigger level event correlation.

[comment]: # ({/new-8e1a9fc3})

[comment]: # ({new-ad7b6e93})
##### Event correlation

Event correlation (also called global event correlation) is a way to set
up custom event closing (resulting in OK event generation) rules.

The rules define how the new problem events are paired with existing
problem events and allow to close the new event or the matched events by
generating corresponding OK events.

However, event correlation must be configured very carefully, as it can
negatively affect event processing performance or, if misconfigured,
close more events than intended (in the worst case even all problem
events could be closed). A few configuration tips:

1.  always reduce the correlation scope by setting a unique tag for the
    control event (the event that is paired with old events) and use the
    'new event tag' correlation condition
2.  don't forget to add a condition based on the old event when using
    'close old event' operation, or all existing problems could be
    closed
3.  avoid using common tag names used by different correlation
    configurations

[comment]: # ({/new-ad7b6e93})

[comment]: # ({new-00be2f5b})
##### Task manager

If the 'Allow manual close' setting is enabled for trigger, then it's
possible to manually close problem events generated by the trigger. This
is done in the frontend when [updating a
problem](/manual/acknowledges#updating_problems). The event is not
closed directly – instead a 'close event' task is created, which is
handled by the task manager shortly. The task manager will generate a
corresponding OK event and the problem event will be closed.

[comment]: # ({/new-00be2f5b})
