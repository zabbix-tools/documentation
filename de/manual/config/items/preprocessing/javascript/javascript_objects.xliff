<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="de" datatype="plaintext" original="manual/config/items/preprocessing/javascript/javascript_objects.md">
    <body>
      <trans-unit id="d3accdff" xml:space="preserve">
        <source># Additional JavaScript objects</source>
      </trans-unit>
      <trans-unit id="ab8bd378" xml:space="preserve">
        <source>### Overview

This section describes Zabbix additions to the JavaScript language implemented with Duktape and supported global JavaScript functions.</source>
      </trans-unit>
      <trans-unit id="c73f2449" xml:space="preserve">
        <source>### Built-in objects</source>
      </trans-unit>
      <trans-unit id="e5dc8dd0" xml:space="preserve">
        <source>#### Zabbix

The Zabbix object provides interaction with the internal Zabbix functionality.

|Method|Description|
|--|--------|
|`log(loglevel, message)`|Writes &lt;message&gt; into Zabbix log using &lt;loglevel&gt; log level (see configuration file DebugLevel parameter).|

Example:

    Zabbix.log(3, "this is a log entry written with 'Warning' log level")

You may use the following aliases:

|Alias|Alias to|
|-----|--------|
|console.log(object)|Zabbix.log(4, JSON.stringify(object))|
|console.warn(object)|Zabbix.log(3, JSON.stringify(object))|
|console.error(object)|Zabbix.log(2, JSON.stringify(object))|

::: noteimportant
The total size of all logged messages is limited to 8 MB per script execution.
:::

|Method|Description|
|--|--------|
|`sleep(delay)`|Delay JavaScript execution by `delay` milliseconds.|

Example (delay execution by 15 seconds):

    Zabbix.sleep(15000)
  </source>
      </trans-unit>
      <trans-unit id="413974f8" xml:space="preserve">
        <source>#### HttpRequest

This object encapsulates cURL handle allowing to make simple HTTP requests.
Errors are thrown as exceptions.

::: noteimportant
The initialization of multiple `HttpRequest` objects is limited to 10 per script execution.
:::

|Method|Description|
|--|--------|
|`addHeader(name, value)`|Adds HTTP header field. This field is used for all following requests until cleared with the `clearHeader()` method.&lt;br&gt;The total length of header fields that can be added to a single `HttpRequest` object is limited to 128 Kbytes (special characters and header names included).|
|`clearHeader()`|Clears HTTP header. If no header fields are set, `HttpRequest` will set Content-Type to application/json if the data being posted is JSON-formatted; text/plain otherwise.|
|`connect(url)`|Sends HTTP CONNECT request to the URL and returns the response.|
|`customRequest(method, url, data)`|Allows to specify any HTTP method in the first parameter. Sends the method request to the URL with optional *data* payload and returns the response.|
|`delete(url, data)`|Sends HTTP DELETE request to the URL with optional *data* payload and returns the response.|
|`getHeaders(&lt;asArray&gt;)`|Returns the object of received HTTP header fields.&lt;br&gt;The `asArray` parameter may be set to "true" (e.g., `getHeaders(true)`), "false" or be undefined. If set to "true", the received HTTP header field values will be returned as arrays; this should be used to retrieve the field values of multiple same-name headers.&lt;br&gt;If not set or set to "false", the received HTTP header field values will be returned as strings.|
|`get(url, data)`|Sends HTTP GET request to the URL with optional *data* payload and returns the response.|
|`head(url)`|Sends HTTP HEAD request to the URL and returns the response.|
|`options(url)`|Sends HTTP OPTIONS request to the URL and returns the response.|
|`patch(url, data)`|Sends HTTP PATCH request to the URL with optional *data* payload and returns the response.|
|`put(url, data)`|Sends HTTP PUT request to the URL with optional *data* payload and returns the response.|
|`post(url, data)`|Sends HTTP POST request to the URL with optional *data* payload and returns the response.|
|`getStatus()`|Returns the status code of the last HTTP request.|
|`setProxy(proxy)`|Sets HTTP proxy to "proxy" value. If this parameter is empty, then no proxy is used.|
|`setHttpAuth(bitmask, username, password)`|Sets enabled HTTP authentication methods (HTTPAUTH\_BASIC, HTTPAUTH\_DIGEST, HTTPAUTH\_NEGOTIATE, HTTPAUTH\_NTLM, HTTPAUTH\_NONE) in the 'bitmask' parameter.&lt;br&gt;The HTTPAUTH\_NONE flag allows to disable HTTP authentication.&lt;br&gt;Examples:&lt;br&gt;`request.setHttpAuth(HTTPAUTH_NTLM \| HTTPAUTH_BASIC, username, password)`&lt;br&gt;`request.setHttpAuth(HTTPAUTH_NONE)`|
|`trace(url, data)`|Sends HTTP TRACE request to the URL with optional *data* payload and returns the response.|

Example:

```javascript
try {
    Zabbix.log(4, 'jira webhook script value='+value);
  
    var result = {
        'tags': {
            'endpoint': 'jira'
        }
    },
    params = JSON.parse(value),
    req = new HttpRequest(),
    fields = {},
    resp;
  
    req.addHeader('Content-Type: application/json');
    req.addHeader('Authorization: Basic '+params.authentication);
  
    fields.summary = params.summary;
    fields.description = params.description;
    fields.project = {"key": params.project_key};
    fields.issuetype = {"id": params.issue_id};
    resp = req.post('https://tsupport.zabbix.lan/rest/api/2/issue/',
        JSON.stringify({"fields": fields})
    );
  
    if (req.getStatus() != 201) {
        throw 'Response code: '+req.getStatus();
    }
  
    resp = JSON.parse(resp);
    result.tags.issue_id = resp.id;
    result.tags.issue_key = resp.key;
} catch (error) {
    Zabbix.log(4, 'jira issue creation failed json : '+JSON.stringify({"fields": fields}));
    Zabbix.log(4, 'jira issue creation failed : '+error);
  
    result = {};
}
  
return JSON.stringify(result);
```</source>
      </trans-unit>
      <trans-unit id="06cac1ca" xml:space="preserve">
        <source>#### XML

The XML object allows the processing of XML data in the item and
low-level discovery preprocessing and webhooks.

::: noteimportant
 In order to use XML object, server/proxy must be
compiled with libxml2 support.
:::

|Method|Description|
|--|--------|
|`XML.query(data, expression)`|Retrieves node content using XPath. Returns null if node is not found.&lt;br&gt;**expression** - an XPath expression;&lt;br&gt;**data** - XML data as a string.|
|`XML.toJson(data)`|Converts data in XML format to JSON.|
|`XML.fromJson(object)`|Converts data in JSON format to XML.|

Example:

*Input:*

    &lt;menu&gt;
        &lt;food type = "breakfast"&gt;
            &lt;name&gt;Chocolate&lt;/name&gt;
            &lt;price&gt;$5.95&lt;/price&gt;
            &lt;description&gt;&lt;/description&gt;
            &lt;calories&gt;650&lt;/calories&gt;
        &lt;/food&gt;
    &lt;/menu&gt;

*Output:*

```json
{
    "menu": {
        "food": {
            "@type": "breakfast",
            "name": "Chocolate",
            "price": "$5.95",
            "description": null,
            "calories": "650"
        }
    }
}
```</source>
      </trans-unit>
      <trans-unit id="a12fbdc2" xml:space="preserve">
        <source>##### Serialization rules

XML to JSON conversion will be processed according to the following
rules (for JSON to XML conversions reversed rules are applied):

1\. XML attributes will be converted to keys that have their names
prepended with '@'.

Example:

*Input:*

     &lt;xml foo="FOO"&gt;
       &lt;bar&gt;
         &lt;baz&gt;BAZ&lt;/baz&gt;
       &lt;/bar&gt;
     &lt;/xml&gt;

*Output:*

```json
 {
   "xml": {
     "@foo": "FOO",
     "bar": {
       "baz": "BAZ"
     }
   }
 }
```

2\. Self-closing elements (&lt;foo/&gt;) will be converted as having
'null' value.

Example:

*Input:*

    &lt;xml&gt;
      &lt;foo/&gt;
    &lt;/xml&gt;

*Output:*

```json
{
  "xml": {
    "foo": null
  }
}
```

3\. Empty attributes (with "" value) will be converted as having empty
string ('') value.

Example:

*Input:*

    &lt;xml&gt;
      &lt;foo bar="" /&gt;
    &lt;/xml&gt;

*Output:*

```json
{
  "xml": {
    "foo": {
      "@bar": ""
    }
  }
}
```

4\. Multiple child nodes with the same element name will be converted to
a single key that has an array of values as its value.

Example:

*Input:*

    &lt;xml&gt;
      &lt;foo&gt;BAR&lt;/foo&gt;
      &lt;foo&gt;BAZ&lt;/foo&gt;
      &lt;foo&gt;QUX&lt;/foo&gt;
    &lt;/xml&gt;

*Output:*

```json
{
  "xml": {
    "foo": ["BAR", "BAZ", "QUX"]
  }
}
```

5\. If a text element has no attributes and no children, it will be
converted as a string.

Example:

*Input:*

    &lt;xml&gt;
        &lt;foo&gt;BAZ&lt;/foo&gt;
    &lt;/xml&gt;

*Output:*

```json
{
  "xml": {
    "foo": "BAZ"
   }
}
```

6\. If a text element has no children but has attributes, text content
will be converted to an element with the key '\#text' and content as a
value; attributes will be converted as described in the serialization
rule 1.

Example:

*Input:*

    &lt;xml&gt;
      &lt;foo bar="BAR"&gt;
        BAZ
      &lt;/foo&gt;
    &lt;/xml&gt;

*Output:*

```json
{
  "xml": {
    "foo": {
      "@bar": "BAR",
      "#text": "BAZ"
    }
  }
}
```</source>
      </trans-unit>
      <trans-unit id="1368a2b0" xml:space="preserve">
        <source>### Global JavaScript functions

Additional global JavaScript functions have been implemented with
Duktape:

-   btoa(data) - encodes the data to base64 string
-   atob(base64\_string) - decodes base64 string

```javascript
try {
    b64 = btoa("utf8 string");
    utf8 = atob(b64);
} 
catch (error) {
    return {'error.name' : error.name, 'error.message' : error.message}
}
```

-   md5(data) - calculates the MD5 hash of the data
-   sha256(data) - calculates the SHA256 hash of the data
-   hmac('\&lt;hash type\&gt;',key,data) - returns HMAC hash as hex formatted string;
    MD5 and SHA256 hash types are supported;
    key and data parameters support binary data. Examples:
    -    `hmac('md5',key,data)`
    -    `hmac('sha256',key,data)`
-   sign(hash,key,data) - returns calculated signature (RSA signature with SHA-256) as a string, where:&lt;br&gt;
    **hash** - only 'sha256' is allowed, otherwise an error is thrown;&lt;br&gt;
    **key** - the private key. It should correspond to PKCS#1 or PKCS#8 standard. The key can be provided in different forms:&lt;br&gt;
    -    with spaces instead of newlines;
    -    with escaped or non-escaped '\n's instead of newlines;
    -    without any newlines as a single-line string;
    -    as a JSON-formatted string.

    The key also can be loaded from a user macro/secret macro/vault.

    **data** - the data that will be signed. It can be a string (binary data also supported) or buffer (Uint8Array/ArrayBuffer).&lt;br&gt;
    OpenSSL or GnuTLS is used to calculate the signatures. If Zabbix was built without any of these encryption libraries, an error will be thrown ('missing OpenSSL or GnuTLS library').</source>
      </trans-unit>
    </body>
  </file>
</xliff>
