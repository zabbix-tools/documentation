<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="de" datatype="plaintext" original="manual/config/items/check_now.md">
    <body>
      <trans-unit id="17ac0201" xml:space="preserve">
        <source># 11 Execute now</source>
      </trans-unit>
      <trans-unit id="e417ef3e" xml:space="preserve">
        <source>#### Overview

Checking for a new item value in Zabbix is a cyclic process that is
based on configured update intervals. While for many items the update
intervals are quite short, there are others (including low-level
discovery rules) for which the update intervals are quite long, so in
real-life situations there may be a need to check for a new value
quicker - to pick up changes in discoverable resources, for example. To
accommodate such a necessity, it is possible to reschedule a passive
check and retrieve a new value immediately.

This functionality is supported for **passive** checks only. The
following item types are supported:

-   Zabbix agent (passive)
-   SNMPv1/v2/v3 agent
-   IPMI agent
-   Simple check
-   Zabbix internal
-   External check
-   Database monitor
-   JMX agent
-   SSH agent
-   Telnet
-   Calculated
-   HTTP agent
-   Dependent item
-   Script

::: noteimportant
The check must be present in the configuration
cache in order to get executed; for more information see
[CacheUpdateFrequency](/manual/appendix/config/zabbix_server). Before
executing the check, the configuration cache is **not** updated, thus
very recent changes to item/discovery rule configuration will not be
picked up. Therefore, it is also not possible to check for a new value
for an item/rule that is being created or has been created just now; use
the *Test* option while configuring an item for that.
:::</source>
      </trans-unit>
      <trans-unit id="d51c596d" xml:space="preserve">
        <source>#### Configuration

To execute a passive check immediately:

-   click on *Execute now* for selected items in the list of
    latest data:

![](../../../../assets/en/manual/config/items/execute_now_latest_data.png)

Several items can be selected and "executed now" at once. 

In latest data this option is available only for hosts with read-write access. 
Accessing this option for hosts with read-only permissions depends on the [user 
role](/manual/web_interface/frontend_sections/users/user_roles) option 
called *Invoke "Execute now" on read-only hosts*.

-   click on *Execute now* in an existing item (or discovery rule)
    configuration form:

![execute\_now.png](../../../../assets/en/manual/config/items/execute_now.png)

-   click on *Execute now* for selected items/rules in the list of
    items/discovery rules:

![](../../../../assets/en/manual/config/items/execute_now_list.png)

Several items/rules can be selected and "executed
now" at once.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
