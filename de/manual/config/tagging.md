[comment]: # translation:outdated

[comment]: # ({new-12728bfa})
# 6 Tagging

[comment]: # ({/new-12728bfa})

[comment]: # ({new-700f39aa})
#### Overview

There is an option to tag various entities in Zabbix. Tags can be
defined for:

-   templates
-   hosts
-   items
-   web scenarios
-   triggers
-   template items and triggers
-   host, item and trigger prototypes

Tags have several uses, most notably, to mark events. If entities are
tagged, the corresponding new events get marked accordingly:

-   with tagged templates - any host problems created by relevant
    entities (items, triggers, etc) from this template will be marked
-   with tagged hosts - any problem of the host will be marked
-   with tagged items, web scenarios - any data/problem of this item or
    web scenario will be marked
-   with tagged triggers - any problem of this trigger will be marked

A problem event inherits all tags from the whole chain of templates,
hosts, items, web scenarios, triggers. Completely identical `tag:value`
combinations (after resolved macros) are merged into one rather than
being duplicated, when marking the event.

Having custom event tags allows for more flexibility. Importantly,
events can be [correlated](/manual/config/event_correlation) based on
event tags. In other uses, actions can be defined based on tagged
events. Item problems can be grouped based on tags.

Tagging is realized as a pair of *tag name* and *value*. You can use
only the name or pair it with a value:

    MySQL, Service:MySQL, Services, Services:Customer, Applications, Application:Java, Priority:High 

An entity (template, host, item, web scenario, trigger or event) may be
tagged with the same name, but different values - these tags will not be
considered 'duplicates'. Similarly, a tag without value and the same tag
with value can be used simultaneously.

[comment]: # ({/new-700f39aa})

[comment]: # ({new-1a2fe757})
#### Use cases

Some use cases for this functionality are as follows:

1.  Mark trigger events in the frontend
    -   Define tags on trigger level;
    -   See how all trigger problems are marked with these tags in
        *Monitoring* → *Problems*.
2.  Mark all template-inherited problems
    -   Define a tag on template level, for example 'App=MySQL';
    -   See how those host problems that are created by triggers from
        this template are marked with these tags in *Monitoring* →
        *Problems*.
3.  Mark all host problems
    -   Define a tag on host level, for example 'Service=JIRA';
    -   See how all problems of the host triggers are marked with these
        tags in *Monitoring* → *Problems*
4.  Group related items
    -   Define a tag on item level, for example 'MySQL';
    -   See all items tagged as 'MySQL' in *Latest data* by using the
        tag filter
5.  Identify problems in a log file and close them separately
    -   Define tags in the log trigger that will identify events using
        value extraction by the `{{ITEM.VALUE<N>}.regsub()}` macro;
    -   In trigger configuration, have multiple problem event generation
        mode;
    -   In trigger configuration, use [event
        correlation](/manual/config/event_correlation): select the
        option that OK event closes only matching events and choose the
        tag for matching;
    -   See problem events created with a tag and closed individually.
6.  Use it to filter notifications
    -   Define tags on the trigger level to mark events by different
        tags;
    -   Use tag filtering in action conditions to receive notifications
        only on the events that match tag data.
7.  Use information extracted from item value as tag value
    -   Use an `{{ITEM.VALUE<N>}.regsub()}` macro in the tag value;
    -   See tag values in *Monitoring* → *Problems* as extracted data
        from item value.
8.  Identify problems better in notifications
    -   Define tags on the trigger level;
    -   Use an {EVENT.TAGS} macro in the problem notification;
    -   Easier identify which application/service the notification
        belongs to.
9.  Simplify configuration tasks by using tags on the template level
    -   Define tags on the template trigger level;
    -   See these tags on all triggers created from template triggers.
10. Create triggers with tags from low-level discovery (LLD)
    -   Define tags on trigger prototypes;
    -   Use LLD macros in the tag name or value;
    -   See these tags on all triggers created from trigger prototypes.

[comment]: # ({/new-1a2fe757})

[comment]: # ({new-4793fc2c})
#### Configuration

Tags can be entered in a dedicated tab, for example, in trigger
configuration:

![](../../../assets/en/manual/config/tags_trigger.png)

[comment]: # ({/new-4793fc2c})

[comment]: # ({new-aace9fc0})
#### Macro support

The following macros may be used in trigger tags:

-   {ITEM.VALUE}, {ITEM.LASTVALUE}, {HOST.HOST}, {HOST.NAME},
    {HOST.CONN}, {HOST.DNS}, {HOST.IP}, {HOST.PORT} and {HOST.ID} macros
    can be used to populate the tag name or tag value
-   {INVENTORY.\*}
    [macros](/manual/appendix/macros/supported_by_location) can be used
    to reference host inventory values from one or several hosts in a
    trigger expression
-   [User macros](/manual/config/macros/user_macros) and user macro
    context is supported for the tag name/value. User macro context may
    include low-level discovery macros
-   Low-level discovery macros can be used for the tag name/value in
    trigger prototypes

The following macros may be used in trigger-based notifications:

-   {EVENT.TAGS} and {EVENT.RECOVERY.TAGS} macros will resolve to a
    comma separated list of event tags or recovery event tags
-   {EVENT.TAGSJSON} and {EVENT.RECOVERY.TAGSJSON} macros will resolve
    to a JSON array containing event tag
    [objects](/manual/api/reference/event/object#event_tag) or recovery
    event tag objects

The following macros may be used in template, host, item and web
scenario tags:

-   {HOST.HOST}, {HOST.NAME}, {HOST.CONN}, {HOST.DNS}, {HOST.IP},
    {HOST.PORT} and {HOST.ID} macros
-   {INVENTORY.\*}
    [macros](/manual/appendix/macros/supported_by_location)
-   [User macros](/manual/config/macros/user_macros)
-   Low-level discovery macros can be used in item prototype tags

The following macros may be used in host prototype tags:

-   {HOST.HOST}, {HOST.NAME}, {HOST.CONN}, {HOST.DNS}, {HOST.IP},
    {HOST.PORT} and {HOST.ID} macros
-   {INVENTORY.\*}
    [macros](/manual/appendix/macros/supported_by_location)
-   [User macros](/manual/config/macros/user_macros)
-   [Low-level discovery macros](/manual/config/macros/lld_macros) will
    be resolved during discovery process and then added to the
    discovered host

[comment]: # ({/new-aace9fc0})

[comment]: # ({new-20ae36ff})
##### Substring extraction in trigger tags

Substring extraction is supported for populating the tag name or tag
value, using a macro [function](/manual/config/macros/macro_functions) -
applying a regular expression to the value obtained by the {ITEM.VALUE},
{ITEM.LASTVALUE} macro or a low-level discovery macro. For example:

    {{ITEM.VALUE}.regsub(pattern, output)}
    {{ITEM.VALUE}.iregsub(pattern, output)}

    {{#LLDMACRO}.regsub(pattern, output)}
    {{#LLDMACRO}.iregsub(pattern, output)}

Tag name and value will be cut to 255 characters if their length exceeds
255 characters after macro resolution.

See also: Using macro functions in [low-level discovery
macros](/manual/config/macros/lld_macros#using_macro_functions) for
event tagging.

[comment]: # ({/new-20ae36ff})

[comment]: # ({new-048b2b9c})
#### Viewing event tags

Tagging, if defined, can be seen with new events in:

-   *Monitoring* → *Problems*
-   *Monitoring* → *Problems* → *Event details*
-   *Monitoring* → *Dashboard* → *Problems* widget (in popup window that
    opens when rolling the mouse over problem name|

![](../../../assets/en/manual/config/triggers/event_tags_view.png){width="600"}

Only the first three tag entries are displayed. If there are more than
three tag entries, it is indicated by three dots. If you roll your mouse
over these three dots, all tag entries are displayed in a pop-up window.

Note that the order in which tags are displayed is affected by tag
filtering and the *Tag display priority* option in the filter of
*Monitoring* → *Problems* or the *Problems* dashboard widget.

[comment]: # ({/new-048b2b9c})
