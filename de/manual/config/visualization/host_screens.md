[comment]: # translation:outdated

[comment]: # ({new-9902534f})
# 4 Host dashboards

[comment]: # ({/new-9902534f})

[comment]: # ({new-3cdd1f21})
#### Overview

Host dashboards look similar to [global
dashboards](/manual/web_interface/frontend_sections/monitoring/dashboard),
however, host dashboards display data about the host only. Host
dashboards have no owner.

Host dashboards are configured on the
[template](/manual/config/templates/template#adding_dashboards) level
and then are generated for a host, once the template is linked to the
host. Widgets of host dashboards can only be copied to host dashboards
of the same template. Widgets from global dashboards cannot be copied
onto host dashboards.

Host dashboards *cannot* be configured or directly accessed in the
*Monitoring* →
*[Dashboard](/manual/web_interface/frontend_sections/monitoring/dashboard)*
section, which is reserved for global dashboards. The ways to access
host dashboards are listed below in this section.

![](../../../../assets/en/manual/config/visualization/host_dashboards.png){width="600"}

When viewing host dashboards you may switch between the configured
dashboards using the dropdown in the upper right corner. To switch to
*Monitoring→Hosts* section, click *All hosts* navigation link below the
dashboard name in the upper left corner.

Widgets of the host dashboards cannot be edited.

Note that host dashboards used to be host screens before Zabbix 5.2.
When importing an older template containing screens, the screen import
will be ignored.

[comment]: # ({/new-3cdd1f21})

[comment]: # ({new-81326ded})
#### Accessing host dashboards

Access to host dashboards is provided:

-   From the [host
    menu](/manual/web_interface/frontend_sections/monitoring/dashboard#host_menu)
    that is available in many frontend locations:
    -   click on the host name and then select *Dashboards* from the
        drop-down menu

![](../../../../assets/en/manual/config/visualization/host_menu_dashboards.png)

-   When searching for a host name in [global
    search](/manual/web_interface/global_search):
    -   click on the *Dashboards* link provided in search results

```{=html}
<!-- -->
```
-   When clicking on a host name in *Inventory* →
    *[Hosts](/manual/web_interface/frontend_sections/inventory/hosts)*:
    -   click on the *Dashboards* link provided

[comment]: # ({/new-81326ded})
