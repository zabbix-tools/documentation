[comment]: # translation:outdated

[comment]: # ({new-6d7c832e})
# 5 Event correlation

[comment]: # ({/new-6d7c832e})

[comment]: # ({new-fff764b2})
#### Overview

Event correlation allows to correlate problem events to their resolution
in a manner that is very precise and flexible.

Event correlation can be defined:

-   [on trigger level](/manual/config/event_correlation/trigger) - one
    trigger may be used to relate separate problems to their solution
-   [globally](/manual/config/event_correlation/global) - problems can
    be correlated to their solution from a different trigger/polling
    method using global correlation rules

[comment]: # ({/new-fff764b2})
