[comment]: # translation:outdated

[comment]: # ({new-72ab9926})
# 13 Storage of secrets

[comment]: # ({/new-72ab9926})

[comment]: # ({new-5a5d507b})
## Overview

Zabbix can be configured to retrieve sensitive information from a secure vault. The following secret management services are supported: HashiCorp Vault KV Secrets Engine - Version 2, CyberArk Vault CV12.

Secrets can be used for retrieving:

-   [user macro values](/manual/config/macros/secret_macros#vault_secret)
-   database access credentials

Zabbix provides read-only access to the secrets in a vault, assuming that secrets are managed by someone else.

[comment]: # ({/new-5a5d507b})

[comment]: # ({new-5cd2f0a1})

For information about specific vault provider configuration, see:
- [HashiCorp configuration](/manual/config/secrets/hashicorp)
- [CyberArk configuration](/manual/config/secrets/cyberark)

[comment]: # ({/new-5cd2f0a1})

[comment]: # ({new-44c0f649})
## Caching of secret values

Vault secret macro values are retrieved by Zabbix server on every refresh of configuration data and then stored in the configuration cache. Zabbix proxy receives values of vault secret macros from Zabbix server on each configuration sync and stores them in its own configuration cache. 

::: noteimportant
Encryption must be enabled between Zabbix server and proxy; otherwise a server warning message is logged.
:::

To manually trigger refresh of cached secret values from a vault, use the 'secrets_reload' command-line [option](/manual/concepts/server#runtime_control).

For Zabbix frontend database credentials caching is disabled by default, but can be enabled by setting the option ``$DB['VAULT_CACHE'] = true`` in zabbix.conf.php. The credentials will be stored in a local cache using the filesystem temporary file directory. The web server must allow writing in a private temporary folder (for example, for Apache the configuration option ``PrivateTmp=True`` must be set). To control how often the data cache is refreshed/invalidated, use the ZBX\_DATA\_CACHE\_TTL [constant](/manual/web_interface/definitions) .

[comment]: # ({/new-44c0f649})

[comment]: # ({new-df569dd2})
## TLS configuration

To configure TLS for communication between Zabbix components and the vault, add a certificate signed by a certificate authority (CA) to the system-wide default CA store. 
To use another location, specify the directory in the SSLCALocation Zabbix [server](/manual/appendix/config/zabbix_server)/[proxy](/manual/appendix/config/zabbix_proxy) configuration parameter, 
place the certificate file inside that directory, then run the CLI [command](https://www.openssl.org/docs/manmaster/man1/c_rehash.html):

    $ c_rehash .

[comment]: # ({/new-df569dd2})





