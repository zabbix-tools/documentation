[comment]: # translation:outdated

[comment]: # ({new-4c8ef099})
# 5 Receiving problem notification

[comment]: # ({/new-4c8ef099})

[comment]: # ({new-055c3bc8})
#### Overview

In this section you will learn how to set up alerting in the form of
notifications in Zabbix.

With items collecting data and triggers designed to "fire" upon problem
situations, it would also be useful to have some alerting mechanism in
place that would notify us about important events even when we are not
directly looking at Zabbix frontend.

This is what notifications do. E-mail being the most popular delivery
method for problem notifications, we will learn how to set up an e-mail
notification.

[comment]: # ({/new-055c3bc8})

[comment]: # ({new-237d8e2e})
#### E-mail settings

Initially there are several predefined notification [delivery
methods](/manual/config/notifications/media) in Zabbix.
[E-mail](/manual/config/notifications/media/email) is one of those.

To configure e-mail settings, go to *Administration → Media types* and
click on *Email* in the list of pre-defined media types.

![](../../../assets/en/manual/quickstart/media_types.png)

This will present us with the e-mail settings definition form.\
![](../../../assets/en/manual/quickstart/media_type_email.png)

All mandatory input fields are marked with a red asterisk.

Set the values of SMTP server, SMTP helo and SMTP e-mail to the
appropriate for your environment.

::: noteclassic
'SMTP email' will be used as the 'From' address for the
notifications sent from Zabbix.
:::

Press *Update* when ready.

Now you have configured 'Email' as a working media type. A media type
must be linked to users by defining specific delivery addresses (like we
did when [configuring a new user](login#adding_user)), otherwise it will
not be used.

[comment]: # ({/new-237d8e2e})

[comment]: # ({new-c1762601})
#### New action

Delivering notifications is one of the things
[actions](/manual/config/notifications/action) do in Zabbix. Therefore,
to set up a notification, go to *Configuration → Actions* and click on
*Create action*.\
![](../../../assets/en/manual/quickstart/new_action.png)

All mandatory input fields are marked with a red asterisk.

In this form, enter a name for the action.

In the most simple case, if we do not add any more specific
[conditions](/manual/config/notifications/action/conditions), the action
will be taken upon any trigger change from 'Ok' to 'Problem'.

We still should define what the action should do - and that is done in
the *Operations* tab. Click on *Add* in the Operations block, which
opens a new operation form.

![](../../../assets/en/manual/quickstart/new_operation.png)

All mandatory input fields are marked with a red asterisk.

Here, click on *Add* in the *Send to Users* block and select the user
('user') we have defined. Select 'Email' as the value of *Send only to*.
When done with this, click on *Add*, and the operation should be added:

![](../../../assets/en/manual/quickstart/operation_list.png){width="600"}

That is all for a simple action configuration, so click *Add* in the
action form.

[comment]: # ({/new-c1762601})

[comment]: # ({new-61999602})
#### Receiving notification

Now, with delivering notifications configured it would be fun to
actually receive one. To help with that, we might on purpose increase
the load on our host - so that our [trigger](trigger#adding_trigger)
"fires" and we receive a problem notification.

Open the console on your host and run:

    cat /dev/urandom | md5sum

You may run one or several of [these
processes](http://en.wikipedia.org/wiki/Md5sum).

Now go to *Monitoring → Latest data* and see how the values of 'CPU
Load' have increased. Remember, for our trigger to *fire*, the 'CPU
Load' value has to go over '2' for 3 minutes running. Once it does:

-   in *Monitoring → Problems* you should see the trigger with a
    flashing 'Problem' status
-   you should receive a problem notification in your e-mail

::: noteimportant
If notifications do not work:

-   verify once again that both the e-mail settings and the action have
    been configured properly
-   make sure the user you created has at least read permissions on the
    host which generated the event, as noted in the *[Adding
    user](login#adding_user)* step. The user, being part of the 'Zabbix
    administrators' user group must have at least read access to 'Linux
    servers' host group that our host belongs to.
-   Additionally, you can check out the action log by going to *Reports
    → Action log*.


:::

[comment]: # ({/new-61999602})
