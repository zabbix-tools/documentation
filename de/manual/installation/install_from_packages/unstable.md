[comment]: # translation:outdated

[comment]: # ({new-b2259b50})
# 6 Unstable releases

[comment]: # ({/new-b2259b50})

[comment]: # ({new-9e4d6713})
### Overview

The instructions below are for enabling unstable Zabbix release repositories (disabled by default) used for minor Zabbix version release candidates.

First, install or update to the latest zabbix-release package. To enable rc packages on your system do the following:

[comment]: # ({/new-9e4d6713})

[comment]: # ({new-05fb2800})
### Red Hat Enterprise Linux

Open the `/etc/yum.repos.d/zabbix.repo` file and set enabled=1 for the `zabbix-unstable` repo.

```
[zabbix-unstable]
name=Zabbix Official Repository (unstable) - $basearch
baseurl=https://repo.zabbix.com/zabbix/6.3/rhel/8/$basearch/
enabled=1
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-ZABBIX-A14FE591
```

[comment]: # ({/new-05fb2800})

[comment]: # ({new-f13e36b3})
### Debian/Ubuntu

Open the `/etc/apt/sources.list.d/zabbix.list` and uncomment "Zabbix unstable repository".

```
# Zabbix unstable repository
deb https://repo.zabbix.com/zabbix/6.3/debian bullseye main
deb-src https://repo.zabbix.com/zabbix/6.3/debian bullseye main
```

[comment]: # ({/new-f13e36b3})

[comment]: # ({new-37a29df2})
### SUSE

Open the `/etc/zypp/repos.d/zabbix.repo` file and set enable=1 for the `zabbix-unstable` repo.

```
[zabbix-unstable]
name=Zabbix Official Repository
type=rpm-md
baseurl=https://repo.zabbix.com/zabbix/6.3/sles/15/x86_64/
gpgcheck=1
gpgkey=https://repo.zabbix.com/zabbix/6.3/sles/15/x86_64/repodata/repomd.xml.key
enabled=1
update=1
```

[comment]: # ({/new-37a29df2})
