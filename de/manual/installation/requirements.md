[comment]: # translation:outdated

[comment]: # ({new-02b10f8d})
# 2 Requirements

[comment]: # ({/new-02b10f8d})

[comment]: # ({new-80330f65})
#### Hardware

[comment]: # ({/new-80330f65})

[comment]: # ({new-4ae00568})
##### Memory

Zabbix requires both physical and disk memory. 128 MB of physical memory
and 256 MB of free disk space could be a good starting point. However,
the amount of required disk memory obviously depends on the number of
hosts and parameters that are being monitored. If you're planning to
keep a long history of monitored parameters, you should be thinking of
at least a couple of gigabytes to have enough space to store the history
in the database. Each Zabbix daemon process requires several connections
to a database server. The amount of memory allocated for the connection
depends on the configuration of the database engine. 

::: noteclassic
The
more physical memory you have, the faster the database (and therefore
Zabbix) works! 
:::

[comment]: # ({/new-4ae00568})

[comment]: # ({new-7967309a})
##### CPU

Zabbix and especially Zabbix database may require significant CPU
resources depending on number of monitored parameters and chosen
database engine.

[comment]: # ({/new-7967309a})

[comment]: # ({new-1cbee7bf})
##### Other hardware

A serial communication port and a serial GSM modem are required for
using SMS notification support in Zabbix. USB-to-serial converter will
also work.

[comment]: # ({/new-1cbee7bf})

[comment]: # ({new-42d1d93e})
#### Examples of hardware configuration

The table provides examples of hardware configuration, assuming a **Linux/BSD/Unix** platform.

These are size and hardware configuration examples to start with. Each Zabbix installation is unique. 
Make sure to benchmark the performance of your Zabbix system in a staging or development environment, 
so that you can fully understand your requirements before deploying the Zabbix installation to its 
production environment.

|Installation size|Monitored metrics^**1**^|CPU/vCPU cores|Memory<br>(GiB)|Database|Amazon EC2^**2**^|
|-|-|-|-|-|-|
|Small|1 000|2|8|MySQL Server,<br>Percona Server,<br>MariaDB Server,<br>PostgreSQL|m6i.large/m6g.large|
|Medium|10 000|4|16|MySQL Server,<br>Percona Server,<br>MariaDB Server,<br>PostgreSQL|m6i.xlarge/m6g.xlarge|
|Large|100 000|16|64|MySQL Server,<br>Percona Server,<br>MariaDB Server,<br>PostgreSQL,<br>Oracle|m6i.4xlarge/m6g.4xlarge|
|Very large|1 000 000|32|96|MySQL Server,<br>Percona Server,<br>MariaDB Server,<br>Oracle|m6i.8xlarge/m6g.8xlarge|

^**1**^ 1 metric = 1 item + 1 trigger + 1 graph<br>
^**2**^ Example with Amazon general purpose EC2 instances, using ARM64 or x86_64 architecture, a 
proper instance type like Compute/Memory/Storage optimised should be selected during Zabbix 
installation evaluation and testing before installing in its production environment.

::: noteclassic
Actual configuration depends on the number of active items
and refresh rates very much (see [database
size](/manual/installation/requirements#Database_size) section of this
page for details). It is highly recommended to run the database on a
separate box for large installations.
:::

[comment]: # ({/new-42d1d93e})



[comment]: # ({new-ad0c32cf})
#### Supported platforms

Due to security requirements and the mission-critical nature of the
monitoring server, UNIX is the only operating system that can
consistently deliver the necessary performance, fault tolerance, and
resilience. Zabbix operates on market-leading versions.

Zabbix components are available and tested for the following platforms:

|Platform|Server|Agent|Agent2|
|--------|------|-----|------|
|Linux|x|x|x|
|IBM AIX|x|x|\-|
|FreeBSD|x|x|\-|
|NetBSD|x|x|\-|
|OpenBSD|x|x|\-|
|HP-UX|x|x|\-|
|Mac OS X|x|x|\-|
|Solaris|x|x|\-|
|Windows|\-|x|x|

::: noteclassic
Zabbix server/agent may work on other Unix-like operating
systems as well. Zabbix agent is supported on all Windows desktop and
server versions since XP.
:::

::: noteimportant
Zabbix disables core dumps if compiled with
encryption and does not start if the system does not allow disabling of
core dumps.
:::

[comment]: # ({/new-ad0c32cf})

[comment]: # ({new-323b6241})
#### Required software

Zabbix is built around modern web servers, leading database engines, and
PHP scripting language.

[comment]: # ({/new-323b6241})

[comment]: # ({new-fd454df8})
##### Database management system

|Software|Supported versions|Comments|
|--------|------------------|--------|
|*MySQL/Percona*|8.0.X|Required if MySQL (or Percona) is used as Zabbix backend database. InnoDB engine is required. We recommend using the [MariaDB Connector/C](https://downloads.mariadb.org/connector-c/) library for building server/proxy.|
|*MariaDB*|10.5.00-10.6.X|InnoDB engine is required. We recommend using the [MariaDB Connector/C](https://downloads.mariadb.org/connector-c/) library for building server/proxy.|
|*Oracle*|19c - 21c|Required if Oracle is used as Zabbix backend database.|
|*PostgreSQL*|13.X|Required if PostgreSQL is used as Zabbix backend database.|
|*TimescaleDB* for PostgreSQL|2.0.1-2.3|Required if TimescaleDB is used as Zabbix backend database. Make sure to install the distribution of TimescaleDB with the compression supported.|
|*SQLite*|3.3.5-3.34.X|SQLite is only supported with Zabbix proxies. Required if SQLite is used as Zabbix proxy database.|

::: noteclassic
 Although Zabbix can work with databases available in the
operating systems, for the best experience, we recommend using databases
installed from the official database developer repositories.

:::

[comment]: # ({/new-fd454df8})

[comment]: # ({new-75f0586e})
##### Frontend

The minimum supported screen width for Zabbix frontend is 1200px.

|Software|Version|Comments|
|--------|-------|--------|
|*Apache*|1.3.12 or later|<|
|*PHP*|7.2.5 or later|PHP 8.0 is not supported.|
|PHP extensions:|<|<|
|*gd*|2.0.28 or later|PHP GD extension must support PNG images (*--with-png-dir*), JPEG (*--with-jpeg-dir*) images and FreeType 2 (*--with-freetype-dir*).|
|*bcmath*|<|php-bcmath (*--enable-bcmath*)|
|*ctype*|<|php-ctype (*--enable-ctype*)|
|*libXML*|2.6.15 or later|php-xml, if provided as a separate package by the distributor.|
|*xmlreader*|<|php-xmlreader, if provided as a separate package by the distributor.|
|*xmlwriter*|<|php-xmlwriter, if provided as a separate package by the distributor.|
|*session*|<|php-session, if provided as a separate package by the distributor.|
|*sockets*|<|php-net-socket (*--enable-sockets*). Required for user script support.|
|*mbstring*|<|php-mbstring (*--enable-mbstring*)|
|*gettext*|<|php-gettext (*--with-gettext*). Required for translations to work.|
|*ldap*|<|php-ldap. Required only if LDAP authentication is used in the frontend.|
|*openssl*|<|php-openssl. Required only if SAML authentication is used in the frontend.|
|*mysqli*|<|Required if MySQL is used as Zabbix backend database.|
|*oci8*|<|Required if Oracle is used as Zabbix backend database.|
|*pgsql*|<|Required if PostgreSQL is used as Zabbix backend database.|

::: noteclassic
Zabbix may work on previous versions of Apache, MySQL,
Oracle, and PostgreSQL as well.
:::

::: noteimportant
For other fonts than the default DejaVu, PHP
function
[imagerotate](http://php.net/manual/en/function.imagerotate.php) might
be required. If it is missing, these fonts might be rendered incorrectly
when a graph is displayed. This function is only available if PHP is
compiled with bundled GD, which is not the case in Debian and other
distributions.
:::

[comment]: # ({/new-75f0586e})

[comment]: # ({new-dda86afe})
##### Web browser on client side

Cookies and JavaScript must be enabled.

The latest stable versions of Google Chrome, Mozilla Firefox, Microsoft
Edge, Apple Safari, and Opera are supported.

::: notewarning
The same-origin policy for IFrames is implemented,
which means that Zabbix cannot be placed in frames on a different
domain.\
\
Still, pages placed into a Zabbix frame will have access to Zabbix
frontend (through JavaScript) if the page that is placed in the frame
and Zabbix frontend are on the same domain. A page like
`http://secure-zabbix.com/cms/page.html`, if placed into dashboards on
`http://secure-zabbix.com/zabbix/`, will have full JS access to
Zabbix.
:::

[comment]: # ({/new-dda86afe})

[comment]: # ({new-096d332c})
##### Server

Mandatory requirements are needed always. Optional requirements are
needed for the support of the specific function.

|Requirement|Status|Description|
|-----------|------|-----------|
|*libpcre*|Mandatory|PCRE library is required for [Perl Compatible Regular Expression](https://en.wikipedia.org/wiki/Perl_Compatible_Regular_Expressions) (PCRE) support.<br>The naming may differ depending on the GNU/Linux distribution, for example 'libpcre3' or 'libpcre1'. PCRE v8.x and PCRE2 v10.x (from Zabbix 6.0.0) are supported.|
|*libevent*|^|Required for bulk metric support and IPMI monitoring. Version 1.4 or higher.<br>Note that for Zabbix proxy this requirement is optional; it is needed for IPMI monitoring support.|
|*libpthread*|^|Required for mutex and read-write lock support.|
|*zlib*|^|Required for compression support.|
|*OpenIPMI*|Optional|Required for IPMI support.|
|*libssh2* or *libssh*|^|Required for [SSH checks](/manual/config/items/itemtypes/ssh_checks#overview). Version 1.0 or higher (libssh2); 0.6.0 or higher (libssh).<br>libssh is supported since Zabbix 4.4.6.|
|*fping*|^|Required for [ICMP ping items](/manual/config/items/itemtypes/simple_checks#icmp_pings).|
|*libcurl*|^|Required for web monitoring, VMware monitoring, SMTP authentication, `web.page.*` Zabbix agent [items](/manual/config/items/itemtypes/zabbix_agent), HTTP agent items and Elasticsearch (if used). Version 7.28.0 or higher is recommended.<br>Libcurl version requirements:<br>- SMTP authentication: version 7.20.0 or higher<br>- Elasticsearch: version 7.28.0 or higher|
|*libxml2*|^|Required for VMware monitoring and XML XPath preprocessing.|
|*net-snmp*|^|Required for SNMP support. Version 5.3.0 or higher.|
|*GnuTLS*, *OpenSSL* or *LibreSSL*|^|Required when using [encryption](/manual/encryption#compiling_zabbix_with_encryption_support).|

[comment]: # ({/new-096d332c})

[comment]: # ({new-800c8308})
##### Agent

|Requirement|Status|Description|
|-----------|------|-----------|
|*libpcre*|Mandatory|PCRE library is required for [Perl Compatible Regular Expression](https://en.wikipedia.org/wiki/Perl_Compatible_Regular_Expressions) (PCRE) support.<br>The naming may differ depending on the GNU/Linux distribution, for example 'libpcre3' or 'libpcre1'. PCRE v8.x and PCRE2 v10.x (from Zabbix 6.0.0) are supported.|
|*GnuTLS*, *OpenSSL* or *LibreSSL*|Optional|Required when using [encryption](/manual/encryption#compiling_zabbix_with_encryption_support).<br>On Microsoft Windows systems OpenSSL 1.1.1 or later is required.|

::: noteclassic
 Starting from version 5.0.3, Zabbix agent will not work on
AIX platforms below versions 6.1 TL07 / AIX 7.1 TL01. 
:::

[comment]: # ({/new-800c8308})

[comment]: # ({new-4aa86212})
##### Agent 2

|Requirement|Status|Description|
|-----------|------|-----------|
|*libpcre*|Mandatory|PCRE library is required for [Perl Compatible Regular Expression](https://en.wikipedia.org/wiki/Perl_Compatible_Regular_Expressions) (PCRE) support.<br>The naming may differ depending on the GNU/Linux distribution, for example 'libpcre3' or 'libpcre1'. PCRE v8.x and PCRE2 v10.x (from Zabbix 6.0.0) are supported. |
|*OpenSSL*|Optional|Required when using encryption.<br>OpenSSL 1.0.1 or later is required on UNIX platforms.<br>The OpenSSL library must have PSK support enabled. LibreSSL is not supported.<br>On Microsoft Windows systems OpenSSL 1.1.1 or later is required.|

[comment]: # ({/new-4aa86212})

[comment]: # ({new-149075a3})
##### Java gateway

If you obtained Zabbix from the source repository or an archive, then
the necessary dependencies are already included in the source tree.

If you obtained Zabbix from your distribution's package, then the
necessary dependencies are already provided by the packaging system.

In both cases above, the software is ready to be used and no additional
downloads are necessary.

If, however, you wish to provide your versions of these dependencies
(for instance, if you are preparing a package for some Linux
distribution), below is the list of library versions that Java gateway
is known to work with. Zabbix may work with other versions of these
libraries, too.

The following table lists JAR files that are currently bundled with Java
gateway in the original code:

|Library|License|Website|Comments|
|-------|-------|-------|--------|
|*logback-core-1.2.3.jar*|EPL 1.0, LGPL 2.1|<http://logback.qos.ch/>|Tested with 0.9.27, 1.0.13, 1.1.1 and 1.2.3.|
|*logback-classic-1.2.3.jar*|EPL 1.0, LGPL 2.1|<http://logback.qos.ch/>|Tested with 0.9.27, 1.0.13, 1.1.1 and 1.2.3.|
|*slf4j-api-1.7.30.jar*|MIT License|<http://www.slf4j.org/>|Tested with 1.6.1, 1.6.6, 1.7.6 and 1.7.30.|
|*android-json-4.3\_r3.1.jar*|Apache License 2.0|<https://android.googlesource.com/platform/libcore/+/master/json>|Tested with 2.3.3\_r1.1 and 4.3\_r3.1. See src/zabbix\_java/lib/README for instructions on creating a JAR file.|

Java gateway can be built using either Oracle Java or open-source
OpenJDK (version 1.6 or newer). Packages provided by Zabbix are compiled
using OpenJDK. The table below provides information about OpenJDK
versions used for building Zabbix packages by distribution:

|Distribution|OpenJDK version|
|------------|---------------|
|RHEL/CentOS 8|1.8.0|
|RHEL/CentOS 7|1.8.0|
|SLES 15|11.0.4|
|SLES 12|1.8.0|
|Debian 10|11.0.8|
|Ubuntu 20.04|11.0.8|
|Ubuntu 18.04|11.0.8|

[comment]: # ({/new-149075a3})

[comment]: # ({new-c8024c35})

#### Default port numbers

The following list of open ports per component is applicable for default configuration:

|Zabbix component|Port number|Protocol|Type of connection|
|-------|-------|-------|-------|
|Zabbix agent|10050|TCP|on demand|
|Zabbix agent 2|10050|TCP|on demand|
|Zabbix server|10051|TCP|on demand|
|Zabbix proxy|10051|TCP|on demand|
|Zabbix Java gateway|10052|TCP|on demand|
|Zabbix web service|10053|TCP|on demand|
|Zabbix frontend|80|HTTP|on demand|
|^|443|HTTPS|on demand|
|Zabbix trapper|10051 |TCP| on demand|

::: noteclassic
The port numbers should be open in firewall to enable Zabbix communications. Outgoing TCP connections usually do not require explicit firewall settings.
::: 

[comment]: # ({/new-c8024c35})


[comment]: # ({new-1d73b238})
#### Database size

Zabbix configuration data require a fixed amount of disk space and do
not grow much.

Zabbix database size mainly depends on these variables, which define the
amount of stored historical data:

-   Number of processed values per second

This is the average number of new values Zabbix server receives every
second. For example, if we have 3000 items for monitoring with a refresh
rate of 60 seconds, the number of values per second is calculated as
3000/60 = **50**.

It means that 50 new values are added to Zabbix database every second.

-   Housekeeper settings for history

Zabbix keeps values for a fixed period of time, normally several weeks
or months. Each new value requires a certain amount of disk space for
data and index.

So, if we would like to keep 30 days of history and we receive 50 values
per second, the total number of values will be around
(**30**\*24\*3600)\* **50** = 129.600.000, or about 130M of values.

Depending on the database engine used, type of received values (floats,
integers, strings, log files, etc), the disk space for keeping a single
value may vary from 40 bytes to hundreds of bytes. Normally it is around
90 bytes per value for numeric items^**2**^. In our case, it means that
130M of values will require 130M \* 90 bytes = **10.9GB** of disk space.

::: noteclassic
The size of text/log item values is impossible to predict
exactly, but you may expect around 500 bytes per value.
:::

-   Housekeeper setting for trends

Zabbix keeps a 1-hour max/min/avg/count set of values for each item in
the table **trends**. The data is used for trending and long period
graphs. The one hour period can not be customized.

Zabbix database, depending on the database type, requires about 90 bytes
per each total. Suppose we would like to keep trend data for 5 years.
Values for 3000 items will require 3000\*24\*365\* **90** = **2.2GB**
per year, or **11GB** for 5 years.

-   Housekeeper settings for events

Each Zabbix event requires approximately 250 bytes of disk space^**1**^.
It is hard to estimate the number of events generated by Zabbix daily.
In the worst-case scenario, we may assume that Zabbix generates one
event per second.

For each recovered event, an event\_recovery record is created. Normally
most of the events will be recovered so we can assume one
event\_recovery record per event. That means additional 80 bytes per
event.

Optionally events can have tags, each tag record requiring approximately
100 bytes of disk space^**1**^. The number of tags per event (\#tags)
depends on configuration. So each will need an additional \#tags \* 100
bytes of disk space.

It means that if we want to keep 3 years of events, this would require
3\*365\*24\*3600\* (250+80+\#tags\*100) = **\~30GB**+\#tags\*100B disk
space^**2**^.

::: noteclassic
 ^**1**^ More when having non-ASCII event names, tags and
values.

^**2**^ The size approximations are based on MySQL and might be
different for other databases. 
:::

The table contains formulas that can be used to calculate the disk space
required for Zabbix system:

|Parameter|Formula for required disk space (in bytes)|
|---------|------------------------------------------|
|*Zabbix configuration*|Fixed size. Normally 10MB or less.|
|*History*|days\*(items/refresh rate)\*24\*3600\*bytes<br>items : number of items<br>days : number of days to keep history<br>refresh rate : average refresh rate of items<br>bytes : number of bytes required to keep single value, depends on database engine, normally \~90 bytes.|
|*Trends*|days\*(items/3600)\*24\*3600\*bytes<br>items : number of items<br>days : number of days to keep history<br>bytes : number of bytes required to keep single trend, depends on the database engine, normally \~90 bytes.|
|*Events*|days\*events\*24\*3600\*bytes<br>events : number of event per second. One (1) event per second in worst-case scenario.<br>days : number of days to keep history<br>bytes : number of bytes required to keep single trend, depends on the database engine, normally \~330 + average number of tags per event \* 100 bytes.|

So, the total required disk space can be calculated as:\
**Configuration + History + Trends + Events**\
The disk space will NOT be used immediately after Zabbix installation.
Database size will grow then it will stop growing at some point, which
depends on housekeeper settings.

[comment]: # ({/new-1d73b238})

[comment]: # ({new-520ea0fa})
#### Time synchronization

It is very important to have precise system time on the server with
Zabbix running. [ntpd](http://www.ntp.org/) is the most popular daemon
that synchronizes the host's time with the time of other machines. It's
strongly recommended to maintain synchronized system time on all systems
Zabbix components are running on.

[comment]: # ({/new-520ea0fa})

[comment]: # ({new-9e23ba76})

#### Network requirements

A following list of open ports per component is applicable for default configuration.


|Port|Components |
|------------|---------------|
|Frontend|http on 80, https on 443|
|Server|10051 (for use with active proxy/agents)|
|Active Proxy |10051 ( not sure about fetching config)|
|Passive Proxy|10051|
|Agent2|10050|
|Trapper| |
|JavaGateway|10053|
|WebService|10053|

::: noteclassic
In case the configuration is completed, the port numbers should be edited accordingly to prevent firewall from blocking communications with these ports.Outgoing TCP connections usually do not require explicit firewall settings.
::: 

[comment]: # ({/new-9e23ba76})
