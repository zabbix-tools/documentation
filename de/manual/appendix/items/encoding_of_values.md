[comment]: # translation:outdated

[comment]: # ({new-df5bbb96})
# 6 Encoding of returned values

Zabbix server expects every returned text value in the UTF8 encoding.
This is related to any type of checks: zabbix agent, ssh, telnet, etc.

Different monitored systems/devices and checks can return non-ASCII
characters in the value. For such cases, almost all possible zabbix keys
contain an additional item key parameter - **<encoding>**. This
key parameter is optional but it should be specified if the returned
value is not in the UTF8 encoding and it contains non-ASCII characters.
Otherwise the result can be unexpected and unpredictable.

A description of behavior with different database backends in such cases
follows.

[comment]: # ({/new-df5bbb96})

[comment]: # ({new-b4119079})
#### MySQL

If a value contains a non-ASCII character in non UTF8 encoding - this
character and the following will be discarded when the database stores
this value. No warning messages will be written to the
*zabbix\_server.log*.\
Relevant for at least MySQL version 5.1.61

[comment]: # ({/new-b4119079})

[comment]: # ({new-c305c3e1})
#### PostgreSQL

If a value contains a non-ASCII character in non UTF8 encoding - this
will lead to a failed SQL query (PGRES\_FATAL\_ERROR:ERROR invalid byte
sequence for encoding) and data will not be stored. An appropriate
warning message will be written to the *zabbix\_server.log*.\
Relevant for at least PostgreSQL version 9.1.3

[comment]: # ({/new-c305c3e1})
