[comment]: # translation:outdated

[comment]: # ({new-d25f4c9e})
# 13 Version compatibility

[comment]: # ({/new-d25f4c9e})

[comment]: # ({new-eefa9a73})
#### Supported agents

To be compatible with Zabbix 6.0, Zabbix agent must not be older than
version 1.4 and must not be newer than 6.0.

You may need to review the configuration of older agents as some
parameters have changed, for example, parameters related to
[logging](https://www.zabbix.com/documentation/3.0/manual/installation/upgrade_notes_300#changes_in_configuration_parameters_related_to_logging)
for versions before 3.0.

To take full advantage of the latest metrics, improved performance and
reduced memory usage, use the latest supported agent.

[comment]: # ({/new-eefa9a73})

[comment]: # ({new-1a5df00a})
#### Supported agents 2

Older Zabbix agents 2 from version 4.4 onwards are compatible with
Zabbix 6.0; Zabbix agent 2 must not be newer than 6.0.

Note that when using Zabbix agent 2 versions 4.4 and 5.0, the default
interval of 10 minutes is used for refreshing unsupported items.

To take full advantage of the latest metrics, improved performance and
reduced memory usage, use the latest supported agent 2.

[comment]: # ({/new-1a5df00a})

[comment]: # ({new-5be689b0})
#### Supported Zabbix proxies

To be compatible with Zabbix 6.0, the proxy must be of the same major
version; thus only Zabbix 6.0.x proxies can work with Zabbix 6.0.x
server.

::: noteimportant
It is no longer possible to start the upgraded
server and have older, yet unupgraded proxies report data to a newer
server. This approach, which was never recommended nor supported by
Zabbix, now is officially disabled, as the server will ignore data from
unupgraded proxies. See also the [upgrade
procedure](/manual/installation/upgrade).
:::

Warnings about using incompatible Zabbix daemon versions are logged.

[comment]: # ({/new-5be689b0})

[comment]: # ({new-78c6278a})

In relation to Zabbix server, proxies can be:
-   *Current* (proxy and server have the same major version);
-   *Outdated* (proxy version is older than server version, but is partially supported);
-   *Unsupported* (proxy version is older than server previous LTS release version *or* proxy version is newer than
server major version).

Examples:

|Server version|*Current* proxy version|*Outdated* proxy version|*Unsupported* proxy version|
|--------------|-----------------------|------------------------|---------------------------|
|6.4|6.4|6.0, 6.2|Older than 6.0; newer than 6.4|
|7.0|7.0|6.0, 6.2, 6.4|Older than 6.0; newer than 7.0|
|7.2|7.2|7.0|Older than 7.0; newer than 7.2|

[comment]: # ({/new-78c6278a})

[comment]: # ({new-9f9fce98})

Functionality supported by proxies:

|Proxy version|Data update|Configuration update|Tasks|
|--|--|--|----|
|*Current*|Yes|Yes|Yes|
|*Outdated*|Yes|No|[Remote commands](/manual/config/notifications/action/operation/remote_command) (e.g., shell scripts);<br>Immediate item value checks (i.e., *[Execute now](/manual/config/items/check_now)*);<br>Note: Preprocessing [tests with a real value](/manual/config/items/preprocessing#testing-real-value) are not supported.|
|*Unsupported*|No|No|No|

[comment]: # ({/new-9f9fce98})

[comment]: # ({new-f9a2762b})

Warnings about using incompatible Zabbix daemon versions are logged.

[comment]: # ({/new-f9a2762b})

[comment]: # ({new-5f9e54ca})
#### Supported XML files

XML files not older than version 1.8 are supported for import in Zabbix
6.0.

::: noteimportant
In the XML export format, trigger dependencies are
stored by name only. If there are several triggers with the same name
(for example, having different severities and expressions) that have a
dependency defined between them, it is not possible to import them. Such
dependencies must be manually removed from the XML file and re-added
after import.
:::

[comment]: # ({/new-5f9e54ca})
