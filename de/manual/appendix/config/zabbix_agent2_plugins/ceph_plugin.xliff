<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="de" datatype="plaintext" original="manual/appendix/config/zabbix_agent2_plugins/ceph_plugin.md">
    <body>
      <trans-unit id="7549d0ca" xml:space="preserve">
        <source># 1 Ceph plugin</source>
      </trans-unit>
      <trans-unit id="e123c786" xml:space="preserve">
        <source>#### Overview

This section lists parameters supported in the Ceph Zabbix agent 2
plugin configuration file (ceph.conf). 

Note that:

-   The default values reflect process defaults, not the values in the
    shipped configuration files;
-   Zabbix supports configuration files only in UTF-8 encoding without
    [BOM](https://en.wikipedia.org/wiki/Byte_order_mark);
-   Comments starting with "\#" are only supported at the beginning of
    the line.</source>
      </trans-unit>
      <trans-unit id="30c690c4" xml:space="preserve">
        <source>#### Parameters

|Parameter|Mandatory|Range|Default|Description|
|--|--|--|--|----------|
|Plugins.Ceph.Default.ApiKey|no| | |Default API key for connecting to Ceph; used if no value is specified in an item key or named session.|
|Plugins.Ceph.Default.User|no| | |Default username for connecting to Ceph; used if no value is specified in an item key or named session.|
|Plugins.Ceph.Default.Uri|no| |https://localhost:8003 |Default URI for connecting to Ceph; used if no value is specified in an item key or named session. &lt;br&gt;&lt;br&gt;Should not include embedded credentials (they will be ignored).&lt;br&gt;Must match the URI format.&lt;br&gt;Only `https` scheme is supported; a scheme can be omitted.&lt;br&gt;A port can be omitted (default=8003).&lt;br&gt;Examples: `https://127.0.0.1:8003`&lt;br&gt;`localhost`|
|Plugins.Ceph.InsecureSkipVerify|no|false / true|false|Determines whether an http client should verify the server's certificate chain and host name.&lt;br&gt;If *true*, TLS accepts any certificate presented by the server and any host name in that certificate. In this mode, TLS is susceptible to man-in-the-middle attacks (should be used only for testing).|
|Plugins.Ceph.KeepAlive|no|60-900|300|The maximum time of waiting (in seconds) before unused plugin connections are closed.|
|Plugins.Ceph.Sessions.&lt;SessionName&gt;.ApiKey|no| | |Named session API key.&lt;br&gt;**&lt;SessionName&gt;** - define name of a session for using in item keys.|
|Plugins.Ceph.Sessions.&lt;SessionName&gt;.User|no| | |Named session username.&lt;br&gt;**&lt;SessionName&gt;** - define name of a session for using in item keys.|
|Plugins.Ceph.Sessions.&lt;SessionName&gt;.Uri|no| | |Connection string of a named session.&lt;br&gt;**&lt;SessionName&gt;** - define name of a session for using in item keys.&lt;br&gt;&lt;br&gt;Should not include embedded credentials (they will be ignored).&lt;br&gt;Must match the URI format.&lt;br&gt;Only `https` scheme is supported; a scheme can be omitted.&lt;br&gt;A port can be omitted (default=8003).&lt;br&gt;Examples: `https://127.0.0.1:8003`&lt;br&gt;`localhost`|
|Plugins.Ceph.Timeout|no|1-30|global timeout|Request execution timeout (how long to wait for a request to complete before shutting it down).|

See also:

-   Description of general Zabbix agent 2 configuration parameters:
    [Zabbix agent 2 (UNIX)](/manual/appendix/config/zabbix_agent2) /
    [Zabbix agent 2
    (Windows)](/manual/appendix/config/zabbix_agent2_win)
-   Instructions for configuring [plugins](/manual/extensions/plugins)</source>
      </trans-unit>
    </body>
  </file>
</xliff>
