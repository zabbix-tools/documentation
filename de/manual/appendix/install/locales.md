[comment]: # translation:outdated

[comment]: # ({new-2dc59dbb})
# 14 Additional frontend languages

[comment]: # ({/new-2dc59dbb})

[comment]: # ({new-50c452f4})
#### Overview

In order to use any other language than English in Zabbix web interface,
its locale should be installed on the web server. Additionally, the PHP
gettext extension is required for the translations to work.

[comment]: # ({/new-50c452f4})

[comment]: # ({new-bb6c0b89})
#### Installing locales

To list all installed languages, run:

    locale -a

If some languages that are needed are not listed, open the
*/etc/locale.gen* file and uncomment the required locales. Since Zabbix
uses UTF-8 encoding, you need to select locales with UTF-8 charset.

Now, run:

    locale-gen 

Restart the web server.

The locales should now be installed. It may be required to reload Zabbix
frontend page in browser using Ctrl + F5 for new languages to appear.

[comment]: # ({/new-bb6c0b89})

[comment]: # ({new-b0831966})
#### Installing Zabbix

If installing Zabbix directly from [Zabbix git
repository](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse),
translation files should be generated manually. To generate translation
files, run:

    make gettext
    locale/make_mo.sh

This step is not needed when installing Zabbix from packages or source
tar.gz files.

[comment]: # ({/new-b0831966})

[comment]: # ({new-5dccdfbc})
#### Selecting a language

There are several ways to select a language in Zabbix web interface:

-   When installing web interface - in the frontend [installation
    wizard](/manual/installation/frontend#welcome_screen). Selected
    language will be set as system default.
-   After the installation, system default language can be changed in
    the *Administration→General→GUI* [menu
    section](/manual/web_interface/frontend_sections/administration/general#gui).
-   Language for a particular user can be changed in the [user
    profile](/manual/web_interface/user_profile#user_profile).

If a locale for a language is not installed on the machine, this
language will be greyed out in Zabbix language selector. A red icon is
displayed next to the language selector if at least one locale is
missing. Upon pressing on this icon the following message will be
displayed: "You are not able to choose some of the languages, because
locales for them are not installed on the web server."

![locale\_warning.png](../../../../assets/en/manual/appendix/install/locale_warning.png){width="600"}

[comment]: # ({/new-5dccdfbc})
