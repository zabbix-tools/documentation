[comment]: # translation:outdated

[comment]: # aside:2

[comment]: # ({new-7b40086b})

# Module file structure

All code related to a module is stored in a single directory inside **zabbix/ui/modules/**.

[comment]: # ({/new-7b40086b})

[comment]: # ({new-dffd3e26})
### Module file tree

````yaml
example_module_directory/          (required)
   manifest.json                   (required)  Metadata and action definition.
   Module.php                                  Module initialization and event handling.
   actions/                                    Action controller files.
       SomethingView.php
       SomethingCreate.php
       SomethingDelete.php
       data_export/
           ExportAsXml.php
           ExportAsExcel.php
   views/                                      View files.
       example.something.view.php
       example.something.delete.php
   assets/                                     Any additional files to be used in views. Must be specified in manifest.json.
       js/                                     JavaScript files used in views.
           example.something.view.js.php
       css/                                    CSS files used in views.
           example.something.css
        image.png                              Images used in views.
        example.something.file                 Any file for using in views.
````

[comment]: # ({/new-dffd3e26})

[comment]: # ({new-1d438d3e})
### Writing a module

A sample module writing process consists of the following steps (where available, click on the file or folder name to view additional details about the step):

1. Create a new directory for the module inside **zabbix/ui/modules/**.
2. Add [manifest.json](manifest) file with module metadata.
3. Create *[views](views)* folder and define a module view(s).
4. Create *[actions](actions)* folder and define a module action(s).
5. Create Module.php (or Widget.php for dashboard widgets) file and define initialization and event handling rules.
6. Create *[assets](assets)* folder for JavaScript files (place into *assets/js*), CSS styles (place into *assets/css*), or any other additional files.
7. Make sure to specify required views, actions and asset files in the manifest.json.
8. [Register](register) the module in Zabbix frontend and start using it.

:::notetip
You can register and enable a module as soon as you create manifest.json file.
Once the module is enabled, you can preview all changes made to module files immediately by refreshing Zabbix frontend.
:::

[comment]: # ({/new-1d438d3e})
