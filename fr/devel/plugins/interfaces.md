[comment]: # translation:outdated

[comment]: # ({new-3e219327})
# Plugin interfaces

The following plugin interfaces are available:

[comment]: # ({/new-3e219327})

[comment]: # ({new-bd74c36c})

####  `plugin.Exporter`

*Exporter* is the simplest interface that performs a poll and returns a value (values), nothing, or error. It accepts a
preparsed item key, parameters and context. Exporter interface is the only interface that can be accessed concurrently.
Access to all the other plugin interfaces is exclusive and no method can be called when a plugin is already performing
some task. Also, there is a limit of 100 maximum concurrent *Export()* calls per plugin, which can be reduced according
to the requirements for each plugin.

[comment]: # ({/new-bd74c36c})


[comment]: # ({new-a9e7836a})

####  `plugin.Configurator`

*Configurator* is used to provide a plugin its configuration parameters from the agent 2 configuration files.

[comment]: # ({/new-a9e7836a})

[comment]: # ({new-d6cfe5ec})

####  `plugin.Runner`

*Runner* interface provides the means for performing some initialization when a plugin is started (activated) and
deinitialization when a plugin is stopped (deactivated). For example, a plugin could start/stop some background
*goroutine* by implementing the Runner interface.

[comment]: # ({/new-d6cfe5ec})

