[comment]: # aside: 2

[comment]: # translation:outdated

[comment]: # ({new-12a4ad08})

# Modules

[comment]: # ({/new-12a4ad08})

[comment]: # ({new-f1b884d6})

### Overview

Custom modules extend Zabbix frontend functionality. Zabbix frontend modules are written in PHP.

Jump to:

- [Write your first module](/devel/modules/how_to)

[comment]: # ({/new-f1b884d6})

[comment]: # ({new-4e4bad2b})

### What a module can be used for

-   Adding new functionality via custom frontend sections;
-   Creating custom dashboard widget types (see [widget modules](/devel/modules/widgets));
-   Overriding or extending the existing functionality.

[comment]: # ({/new-4e4bad2b})

[comment]: # ({new-064ddd80})

### What a module cannot be used for

-   Registering a new API method or modifying an existing one.

[comment]: # ({/new-064ddd80})

[comment]: # ({new-bfe0360a})

### How modules work

-   An enabled module is launched on each HTTP request, before executing the action code.
-   The module will register new actions or redefine the existing ones.
-   The module will add new frontend sections and remove or redefine the existing ones.
-   The module will hook to frontend events like onBeforeAction and onTerminate, if needed.
-   The requested action is finally executed by running the action code - either the default one, or module-defined.

[comment]: # ({/new-bfe0360a})

[comment]: # ({new-7aea12f6})
### Where to start

- [A step by step tutorial for writing your first module](tutorials)
- [Module file structure](file_structure)
- [Dashboard widgets](widgets)
- [Register module](file_structure/register)

[comment]: # ({/new-7aea12f6})
