[comment]: # translation:outdated

[comment]: # aside:3

[comment]: # ({new-c4b57655})

# Views

View file receives the data from a controller and then prepares the HTML look of it.

:::noteclassic
Defining view(s) for a frontend module is optional, unless the module is a widget.\
Dashboard widgets need at least two views: one for the edit mode and one for the view mode (should be stored in the *views* directory).
:::

It is possible to use pre-defined Zabbix HTML classes (from the */zabbix/ui/include/classes/html*) in the view as well as add new HTML and CSS classes.
New classes should be stored in the module's *assets* folder.

Example:

````
...
(new CColHeader(_('Name')))
````

This will add a new column name *Name* and style the top table row as on other Zabbix pages.

[comment]: # ({/new-c4b57655})

[comment]: # ({new-6d27cbe1})

### Action view

This is a reference file for defining an action view.

````php
<?php declare(strict_types = 1);

/**
* @var CView $this
*/

$this->includeJsFile('example.something.view.js.php');

(new CWidget())
    ->setTitle(_('Something view'))
    ->addItem(new CDiv($data['name']))
    ->addItem(new CPartial('module.example.something.reusable', [
        'contacts' => $data['contacts']
    ])
    ->show();
````

[comment]: # ({/new-6d27cbe1})
