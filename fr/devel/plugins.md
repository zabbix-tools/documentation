[comment]: # aside: 3

[comment]: # translation:outdated


[comment]: # ({new-df3774f3})

# Plugins

[comment]: # ({/new-df3774f3})

[comment]: # ({new-d4691cc1})

### Overview

Custom plugins extend Zabbix agent 2 functionality. 
A plugin is a *Go* package that defines the structure and implements one or several plugin interfaces
(*Exporter*, *Collector*, *Configurator*, *Runner*, *Watcher*).

Jump to:

- [Write your first plugin](/devel/plugins/how_to)
- [Plugin interfaces](/devel/plugins/interfaces) 

The communication of Zabbix agent 2 with a loadable plugin and metrics collection process is depicted in the Connection Diagram.

![](../../assets/en/connection_diagram_.png)

[comment]: # ({/new-d4691cc1})

