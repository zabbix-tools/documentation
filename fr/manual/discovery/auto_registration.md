[comment]: # translation:outdated

[comment]: # ({new-1c2455b7})
# 2 Active agent auto-registration

[comment]: # ({/new-1c2455b7})

[comment]: # ({new-81edbbe2})
#### Overview

It is possible to allow active Zabbix agent auto-registration, after
which the server can start monitoring them. This way new hosts can be
added for monitoring without configuring them manually on the server.

Auto registration can happen when a previously unknown active agent asks
for checks.

The feature might be very handy for automatic monitoring of new Cloud
nodes. As soon as you have a new node in the Cloud Zabbix will
automatically start the collection of performance and availability data
of the host.

Active agent auto-registration also supports the monitoring of added
hosts with passive checks. When the active agent asks for checks,
providing it has the 'ListenIP' or 'ListenPort' configuration parameters
defined in the configuration file, these are sent along to the server.
(If multiple IP addresses are specified, the first one is sent to the
server.)

Server, when adding the new auto-registered host, uses the received IP
address and port to configure the agent. If no IP address value is
received, the one used for the incoming connection is used. If no port
value is received, 10050 is used.

Auto-registration is rerun:

-   if host [metadata](#using_host_metadata) information changes
-   for manually created hosts with metadata missing
-   if a host is manually changed to be monitored by another Zabbix
    proxy
-   if auto-registration for the same host comes from a new Zabbix proxy

[comment]: # ({/new-81edbbe2})

[comment]: # ({new-5d32b87c})
#### Configuration

[comment]: # ({/new-5d32b87c})

[comment]: # ({new-bf3762f0})
##### Specify server

Make sure you have the Zabbix server identified in the agent
[configuration file](/fr/manual/appendix/config/zabbix_agentd) -
zabbix\_agentd.conf

    ServerActive=10.0.0.1

Unless you specifically define a *Hostname* in zabbix\_agentd.conf, the
system hostname of agent location will be used by server for naming the
host. The system hostname in Linux can be obtained by running the
'hostname' command.

Restart the agent after making any changes to the configuration file.

[comment]: # ({/new-bf3762f0})

[comment]: # ({new-7b103292})
##### Action for active agent auto-registration

When server receives an auto-registration request from an agent it calls
an [action](/fr/manual/config/notifications/action). An action of event
source "Auto registration" must be configured for agent
auto-registration.

::: noteclassic
Setting up [network discovery](network_discovery) is not
required to have active agents auto-register.
:::

In the Zabbix frontend, go to *Configuration → Actions*, select *Auto
registration* as the event source and click on *Create action*:

-   In the Action tab, give your action a name
-   Optionally specify conditions. If you are going to use the "Host
    metadata" condition, see the next section.
-   In the Operations tab, add relevant operations, such as - 'Add
    host', 'Add to host groups' (for example, *Discovered hosts*), 'Link
    to templates', etc.

::: notetip
If the hosts that will be auto-registering are likely to
be supported for active monitoring only (such as hosts that are
firewalled from your Zabbix server) then you might want to create a
specific template like *Template\_Linux-active* to link to.
:::

[comment]: # ({/new-7b103292})

[comment]: # ({new-66cac4ef})
#### Using host metadata

When agent is sending an auto-registration request to the server it
sends its hostname. In some cases (for example, Amazon cloud nodes) a
hostname is not enough for Zabbix server to differentiate discovered
hosts. Host metadata can be optionally used to send other information
from an agent to the server.

Host metadata is configured in the agent [configuration
file](/fr/manual/appendix/config/zabbix_agentd) - zabbix\_agentd.conf.
There are 2 ways of specifying host metadata in the configuration file:

    HostMetadata
    HostMetadataItem

See the description of the options in the link above.

<note:important>An auto-registration attempt happens every time an
active agent sends a request to refresh active checks to the server. The
delay between requests is specified in the
[RefreshActiveChecks](/fr/manual/appendix/config/zabbix_agentd)
parameter of the agent. The first request is sent immediately after the
agent is restarted.
:::

[comment]: # ({/new-66cac4ef})

[comment]: # ({new-9c275675})
##### Example 1

Using host metadata to distinguish between Linux and Windows hosts.

Say you would like the hosts to be auto-registered by the Zabbix server.
You have active Zabbix agents (see "Configuration" section above) on
your network. There are Windows hosts and Linux hosts on your network
and you have "Template OS Linux" and "Template OS Windows" templates
available in your Zabbix frontend. So at host registration you would
like the appropriate Linux/Windows template to be applied to the host
being registered. By default only the hostname is sent to the server at
auto-registration, which might not be enough. In order to make sure the
proper template is applied to the host you should use host metadata.

[comment]: # ({/new-9c275675})

[comment]: # ({new-af247ed6})
##### Frontend configuration

The first thing to do is to configure the frontend. Create 2 actions.
The first action:

-   Name: Linux host autoregistration
-   Conditions: Host metadata like *Linux*
-   Operations: Link to templates: Template OS Linux

::: noteclassic
You can skip an "Add host" operation in this case. Linking
to a template requires adding a host first so the server will do that
automatically.
:::

The second action:

-   Name: Windows host autoregistration
-   Conditions: Host metadata like *Windows*
-   Operations: Link to templates: Template OS Windows

[comment]: # ({/new-af247ed6})

[comment]: # ({new-070aaeb4})
##### Agent configuration

Now you need to configure the agents. Add the next line to the agent
configuration files:

    HostMetadataItem=system.uname

This way you make sure host metadata will contain "Linux" or "Windows"
depending on the host an agent is running on. An example of host
metadata in this case:

    Linux: Linux server3 3.2.0-4-686-pae #1 SMP Debian 3.2.41-2 i686 GNU/Linux
    Windows: Windows WIN-0PXGGSTYNHO 6.0.6001 Windows Server 2008 Service Pack 1 Intel IA-32

Do not forget to restart the agent after making any changes to the
configuration file.

[comment]: # ({/new-070aaeb4})

[comment]: # ({new-9543f562})
##### Example 2

*Step 1*

Using host metadata to allow some basic protection against unwanted
hosts registering.

[comment]: # ({/new-9543f562})

[comment]: # ({new-110dbc2b})
##### Frontend configuration

Create an action in the frontend, using some hard-to-guess secret code
to disallow unwanted hosts:

-   Name: Auto registration action Linux
-   Conditions:

```{=html}
<!-- -->
```
        * Type of calculation: AND
        * Condition (A): Host metadata like //Linux//
        * Condition (B): Host metadata like //21df83bf21bf0be663090bb8d4128558ab9b95fba66a6dbf834f8b91ae5e08ae//
    * Operations: 
        * Send message to users: Admin via all media
        * Add to host groups: Linux servers
        * Link to templates: Template OS Linux

Please note that this method alone does not provide strong protection
because data is transmitted in plain text. Configuration cache reload is
required for changes to have an immediate effect.

[comment]: # ({/new-110dbc2b})

[comment]: # ({new-85f695d6})
##### Agent configuration

Add the next line to the agent configuration file:

    HostMetadata=Linux    21df83bf21bf0be663090bb8d4128558ab9b95fba66a6dbf834f8b91ae5e08ae

where "Linux" is a platform, and the rest of the string is the
hard-to-guess secret text.

Do not forget to restart the agent after making any changes to the
configuration file.

*Step 2*

It is possible to add additional monitoring for an already registered
host.

[comment]: # ({/new-85f695d6})

[comment]: # ({new-c5ffa817})
##### Frontend configuration

Update the action in the frontend:

-   Name: Auto registration action Linux
-   Conditions:

```{=html}
<!-- -->
```
        * Type of calculation: AND
        * Condition (A): Host metadata like Linux
        * Condition (B): Host metadata like 21df83bf21bf0be663090bb8d4128558ab9b95fba66a6dbf834f8b91ae5e08ae
    * Operations:
        * Send message to users: Admin via all media
        * Add to host groups: Linux servers
        * Link to templates: Template OS Linux
        * Link to templates: Template DB MySQL

[comment]: # ({/new-c5ffa817})

[comment]: # ({new-9fca761d})
##### Agent configuration

Update the next line in the agent configuration file:

    HostMetadata=MySQL on Linux 21df83bf21bf0be663090bb8d4128558ab9b95fba66a6dbf834f8b91ae5e08ae

Do not forget to restart the agent after making any changes to the
configuration file.

[comment]: # ({/new-9fca761d})

[comment]: # ({new-6eb51339})
##### Frontend configuration

Update the action in the frontend:

-   Name: Autoregistration action Linux
-   Conditions:

```{=html}
<!-- -->
```
        * Type of calculation: AND
        * Condition (A): Host metadata contains Linux
        * Condition (B): Host metadata contains 21df83bf21bf0be663090bb8d4128558ab9b95fba66a6dbf834f8b91ae5e08ae
    * Operations:
        * Send message to users: Admin via all media
        * Add to host groups: Linux servers
        * Link to templates: Linux
        * Link to templates: MySQL by Zabbix Agent

[comment]: # ({/new-6eb51339})

[comment]: # ({new-15c86816})
##### Agent configuration

Update the next line in the agent configuration file:

    HostMetadata=MySQL on Linux 21df83bf21bf0be663090bb8d4128558ab9b95fba66a6dbf834f8b91ae5e08ae

Do not forget to restart the agent after making any changes to the
configuration file.

[comment]: # ({/new-15c86816})
