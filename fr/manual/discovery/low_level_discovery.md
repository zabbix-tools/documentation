[comment]: # translation:outdated

[comment]: # ({new-a14b62f1})
# 3 Low-level discovery

[comment]: # ({/new-a14b62f1})

[comment]: # ({new-4fc0f22c})
#### Overview

Low-level discovery provides a way to automatically create items,
triggers, and graphs for different entities on a computer. For instance,
Zabbix can automatically start monitoring file systems or network
interfaces on your machine, without the need to create items for each
file system or network interface manually. Additionally it is possible
to configure Zabbix to remove unneeded entities automatically based on
actual results of periodically performed discovery.

A user can define their own types of discovery, provided they follow a
particular JSON protocol.

The general architecture of the discovery process is as follows.

First, a user creates a discovery rule in "Configuration" → "Templates"
→ "Discovery" column. A discovery rule consists of (1) an item that
discovers the necessary entities (for instance, file systems or network
interfaces) and (2) prototypes of items, triggers, and graphs that
should be created based on the value of that item.

An item that discovers the necessary entities is like a regular item
seen elsewhere: the server asks a Zabbix agent (or whatever the type of
the item is set to) for a value of that item, the agent responds with a
textual value. The difference is that the value the agent responds with
should contain a list of discovered entities in a specific JSON format.
While the details of this format are only important for implementers of
custom discovery checks, it is necessary to know that the returned value
contains a list of macro → value pairs. For instance, item
"net.if.discovery" might return two pairs: "{\#IFNAME}" → "lo" and
"{\#IFNAME}" → "eth0".

These macros are used in names, keys and other prototype fields where
they are then substituted with the received values for creating real
items, triggers, graphs or even hosts for each discovered entity. See
the full list of [options](/manual/config/macros/lld_macros) for using
LLD macros.

When the server receives a value for a discovery item, it looks at the
macro → value pairs and for each pair generates real items, triggers,
and graphs, based on their prototypes. In the example with
"net.if.discovery" above, the server would generate one set of items,
triggers, and graphs for the loopback interface "lo", and another set
for interface "eth0".

[comment]: # ({/new-4fc0f22c})

[comment]: # ({new-61b5f21c})
#### Configuring low-level discovery

We will illustrate low-level discovery based on an example of file
system discovery.

To configure the discovery, do the following:

-   Go to: *Configuration* → *Templates*
-   Click on *Discovery* in the row of an appropriate template

![](../../../assets/en/manual/discovery/low_level_discovery/fs_templates.png)

-   Click on *Create discovery rule* in the upper right corner of the
    screen
-   Fill in the discovery rule form with the required details

[comment]: # ({/new-61b5f21c})

[comment]: # ({new-ae1c521e})
#### Discovery rule

The **Discovery rule** tab contains general discovery rule attributes:

![](../../../assets/en/manual/discovery/low_level_discovery/lld_rule_fs.png){width="600"}

All mandatory input fields are marked with a red asterisk.

|Parameter|Description|
|---------|-----------|
|*Name*|Name of discovery rule.|
|*Type*|The type of check to perform discovery; should be *Zabbix agent* or *Zabbix agent (active)* for file system discovery.|
|*Key*|An item with "vfs.fs.discovery" key is built into Zabbix agent since version 2.0 on many platforms (see [supported item key list](/fr/manual/appendix/items/supported_by_platform) for details), and will return a JSON with the list of file systems present on the computer and their types.|
|*Update interval*|This field specifies how often Zabbix performs discovery. In the beginning, when you are just setting up file system discovery, you might wish to set it to a small interval, but once you know it works you can set it to 30 minutes or more, because file systems usually do not change very often.<br>[Time suffixes](/manual/appendix/suffixes) are supported, e.g. 30s, 1m, 2h, 1d, since Zabbix 3.4.0.<br>[User macros](/manual/config/macros/user_macros) are supported, since Zabbix 3.4.0.<br>*Note*: If set to '0', the item will not be polled. However, if a flexible interval also exists with a non-zero value, the item will be polled during the flexible interval duration.<br>*Note* that for an existing discovery rule the discovery can be performed immediately by pushing the *Check now* [button](#form_buttons).|
|*Custom intervals*|You can create custom rules for checking the item:<br>**Flexible** - create an exception to the *Update interval* (interval with different frequency)<br>**Scheduling** - create a custom polling schedule.<br>For detailed information see [Custom intervals](/fr/manual/config/items/item/custom_intervals). Scheduling is supported since Zabix 3.0.0.|
|*Keep lost resources period*|This field allows you to specify the duration for how long the discovered entity will be retained (won't be deleted) once its discovery status becomes "Not discovered anymore" (min 1 hour, max 25 years).<br>[Time suffixes](/manual/appendix/suffixes) are supported, e.g. 2h, 1d, since Zabbix 3.4.0.<br>[User macros](/manual/config/macros/user_macros) are supported, since Zabbix 3.4.0.<br>*Note:* If set to "0", entities will be deleted immediately. Using "0" is not recommended, since just wrongly editing the filter may end up in the entity being deleted with all the historical data.|
|*Description*|Enter a description.|
|*Enabled*|If checked, the rule will be processed.|

[comment]: # ({/new-ae1c521e})

[comment]: # ({new-0e438e85})
#### Discovery rule filter

The **Filters** tab contains discovery rule filter definitions:

![](../../../assets/en/manual/discovery/low_level_discovery/lld_rule_fs2_new.png)

|Parameter|Description|
|---------|-----------|
|*Type of calculation*|The following options for calculating filters are available:<br>**And** - all filters must be passed;<br>**Or** - enough if one filter is passed;<br>**And/Or** - uses *And* with different macro names and *Or* with the same macro name;<br>**Custom expression** - offers the possibility to define a custom calculation of filters. The formula must include all filters in the list. Limited to 255 symbols.|
|*Filters*|A filter can be used to generate real items, triggers, and graphs only for certain file systems. It expects a [Perl Compatible Regular Expression](https://en.wikipedia.org/wiki/Perl_Compatible_Regular_Expressions) (PCRE). For instance, if you are only interested in C:, D:, and E: file systems, you could put {\#FSNAME} into "Macro" and "\^C\|\^D\|\^E" regular expression into "Regular expression" text fields. Filtering is also possible by file system types using {\#FSTYPE} macro (e.g. "\^ext\|\^reiserfs") and by drive types (supported only by Windows agent) using {\#FSDRIVETYPE} macro (e.g., "fixed").<br>You can enter a regular expression or reference a global [regular expression](/fr/manual/regular_expressions) in "Regular expression" field.<br>In order to test a regular expression you can use "grep -E", for example: `for f in ext2 nfs reiserfs smbfs; do echo $f \| grep -E '^ext\|^reiserfs' \|\| echo "SKIP: $f"; done`{.bash}{\#FSDRIVETYPE} macro on Windows is supported since Zabbix **3.0.0**.<br>Defining several filters is supported since Zabbix **2.4.0**.<br>Note that if some macro from the filter is missing in the response, the found entity will be ignored.<br>Filter drop-down offers two values to specify whether a macro matches a regular expression or does not match.|

::: noteimportant
Zabbix database in MySQL must be created as
case-sensitive if file system names that differ only by case are to be
discovered correctly.
:::

::: noteimportant
The mistake or typo in regex used in LLD rule may
cause deleting thousands of configuration elements, historical values
and events for many hosts. For example, incorrect “File systems for
discovery” regular expression may cause deleting thousands of items,
triggers, historical values and events.
:::

::: noteclassic
Discovery rule history is not preserved.
:::

[comment]: # ({/new-0e438e85})

[comment]: # ({new-c5db0ca2})
##### Form buttons

Buttons at the bottom of the form allow to perform several operations.

|   |   |
|---|---|
|![](../../../assets/en/manual/config/button_add.png)|Add a discovery rule. This button is only available for new discovery rules.|
|![](../../../assets/en/manual/config/button_update.png)|Update the properties of a discovery rule. This button is only available for existing discovery rules.|
|![](../../../assets/en/manual/config/button_clone.png)|Create another discovery rule based on the properties of the current discovery rule.|
|![](../../../assets/en/manual/config/button_check_now.png)|Perform discovery based on the discovery rule immediately. The discovery rule must already exist. See [more details](/manual/config/items/check_now).<br>*Note* that when performing discovery immediately, configuration cache is not updated, thus the result will not reflect very recent changes to discovery rule configuration.|
|![](../../../assets/en/manual/config/button_delete.png)|Delete the discovery rule.|
|![](../../../assets/en/manual/config/button_cancel.png)|Cancel the editing of discovery rule properties.|

[comment]: # ({/new-c5db0ca2})

[comment]: # ({new-db0f5889})
#### Item prototypes

Once a rule is created, go to the items for that rule and press "Create
prototype" to create an item prototype. Note how macro {\#FSNAME} is
used where a file system name is required. When the discovery rule is
processed, this macro will be substituted with the discovered file
system.

![](../../../assets/en/manual/discovery/low_level_discovery/item_prototype_fs.png)

Low-level discovery [macros](/manual/config/macros/lld_macros) and user
[macros](/manual/appendix/macros/supported_by_location_user) may be used
in item prototype configuration and item value preprocessing
[parameters](/manual/config/items/item#item_value_preprocessing).

::: noteclassic
Context-specific escaping of low-level discovery macros is
performed for safe use in regular expression and XPath preprocessing
parameters.
:::

Attributes that are specific for item prototypes:

|Parameter|Description|
|---------|-----------|
|*New application prototype*|You may define a new application prototype.<br>In application prototypes you can use low-level discovery macros that, after discovery, will be substituted with real values to create applications that are specific for the discovered entity. See also [application discovery notes](/fr/manual/discovery/low_level_discovery/notes) for more specific information.|
|*Application prototypes*|Select from the existing application prototypes.|
|*Create enabled*|If checked the item will be added in an enabled state.<br>If unchecked, the item will be added to a discovered entity, but in a disabled state.|

We can create several item prototypes for each file system metric we are
interested in:

![](../../../assets/en/manual/discovery/low_level_discovery/item_prototypes_fs.png)

[comment]: # ({/new-db0f5889})

[comment]: # ({new-8d616544})
#### Trigger prototypes

We create trigger prototypes in a similar way as item prototypes:

![](../../../assets/en/manual/discovery/low_level_discovery/trigger_prototype_fs.png)

Attributes that are specific for trigger prototypes:

|Parameter|Description|
|---------|-----------|
|*Create enabled*|If checked the trigger will be added in an enabled state.<br>If unchecked, the trigger will be added to a discovered entity, but in a disabled state.|

When real triggers are created from the prototypes, there may be a need
to be flexible as to what constant ('20' in our example) is used for
comparison in the expression. See how [user macros with
context](/manual/discovery/low_level_discovery#using_lld_macros_in_user_macro_contexts)
can be useful to accomplish such flexibility.

You can define [dependencies](/manual/config/triggers/dependencies)
between trigger prototypes as well (supported since Zabbix 3.0). To do
that, go to the *Dependencies* tab. A trigger prototype may depend on
another trigger prototype from the same low-level discovery (LLD) rule
or on a regular trigger. A trigger prototype may not depend on a trigger
prototype from a different LLD rule or on a trigger created from trigger
prototype. Host trigger prototype cannot depend on a trigger from a
template.

![](../../../assets/en/manual/discovery/low_level_discovery/trigger_prototypes_fs.png)

[comment]: # ({/new-8d616544})

[comment]: # ({new-50cefaf6})
#### Graph prototypes

We can create graph prototypes, too:

![](../../../assets/en/manual/discovery/low_level_discovery/graph_prototype_fs.png)

![](../../../assets/en/manual/discovery/low_level_discovery/graph_prototypes_fs.png)

Finally, we have created a discovery rule that looks like shown below.
It has five item prototypes, two trigger prototypes, and one graph
prototype.

![](../../../assets/en/manual/discovery/low_level_discovery/lld_rules_fs.png)

*Note*: For configuring host prototypes, see the section about [host
prototype](/manual/vm_monitoring#host_prototypes) configuration in
virtual machine monitoring.

[comment]: # ({/new-50cefaf6})

[comment]: # ({new-0dea10fa})
##### Discovered entities

The screenshots below illustrate how discovered items, triggers, and
graphs look like in the host's configuration. Discovered entities are
prefixed with an orange link to a discovery rule they come from.

![](../../../assets/en/manual/discovery/low_level_discovery/discovered_items.png)

Note that discovered entities will not be created in case there are
already existing entities with the same uniqueness criteria, for
example, an item with the same key or graph with the same name.

Items (similarly, triggers and graphs) created by a low-level discovery
rule will be deleted automatically if a discovered entity (file system,
interface, etc) stops being discovered (or does not pass the filter
anymore). In this case the items, triggers and graphs will be deleted
after the days defined in the *Keep lost resources period* field pass.

When discovered entities become 'Not discovered anymore', a lifetime
indicator is displayed in the item list. Move your mouse pointer over it
and a message will be displayed indicating how many days are left until
the item is deleted.

![](../../../assets/en/manual/discovery/low_level_discovery/not_discovered_message.png)

If entities were marked for deletion, but were not deleted at the
expected time (disabled discovery rule or item host), they will be
deleted the next time the discovery rule is processed.

Entities containing other entities, which are marked for deletion, will
not update if changed on the discovery rule level. For example,
LLD-based triggers will not update if they contain items that are marked
for deletion.

![](../../../assets/en/manual/discovery/low_level_discovery/discovered_triggers.png)

![](../../../assets/en/manual/discovery/low_level_discovery/discovered_graphs.png)

[comment]: # ({/new-0dea10fa})

[comment]: # ({new-ab744aed})
#### Other types of discovery

More detail and how-tos on other types of out-of-the-box discovery is
available in the following sections:

-   discovery of [network
    interfaces](/manual/discovery/low_level_discovery/network_interfaces);
-   discovery of [CPUs and CPU
    cores](/manual/discovery/low_level_discovery/cpu);
-   discovery of [SNMP
    OIDs](/manual/discovery/low_level_discovery/snmp_oids);
-   discovery of [JMX
    objects](/manual/discovery/low_level_discovery/jmx);
-   discovery using [ODBC SQL
    queries](/manual/discovery/low_level_discovery/sql_queries);
-   discovery of [Windows
    services](/manual/discovery/low_level_discovery/windows_services);
-   discovery of [host
    interfaces](/manual/discovery/low_level_discovery/host_interfaces)
    in Zabbix.

For more detail on the JSON format for discovery items and an example of
how to implement your own file system discoverer as a Perl script, see
[creating custom LLD rules](#creating_custom_lld_rules).

[comment]: # ({/new-ab744aed})

[comment]: # ({new-554f9404})
#### Data limits for return values

There is no limit for low-level discovery rule JSON data if it is
received directly by Zabbix server, because return values are processed
without being stored in a database. There's also no limit for custom
low-level discovery rules, however, if it is intended to acquire custom
LLD data using a user parameter, then user parameter return value limit
applies (512 KB).

If data has to go through Zabbix proxy it has to store this data in
database so [database
limits](/manual/config/items/item#text_data_limits) apply, for example,
2048 bytes on a Zabbix proxy run with IBM DB2 database.

#### Multiple LLD rules for same item

Since Zabbix agent version 3.2 it is possible to define several
low-level discovery rules with the same discovery item.

To do that you need to define the Alias agent
[parameter](/fr/manual/appendix/config/zabbix_agentd), allowing to use
altered discovery item keys in different discovery rules, for example
`vfs.fs.discovery[foo]`, `vfs.fs.discovery[bar]`, etc.

#### Creating custom LLD rules

It is also possible to create a completely custom LLD rule, discovering
any type of entities - for example, databases on a database server.

To do so, a custom item should be created that returns JSON, specifying
found objects and optionally - some properties of them. The amount of
macros per entity is not limited - while the built-in discovery rules
return either one or two macros (for example, two for filesystem
discovery), it is possible to return more.

The required JSON format is best illustrated with an example. Suppose we
are running an old Zabbix 1.8 agent (one that does not support
"vfs.fs.discovery"), but we still need to discover file systems. Here is
a simple Perl script for Linux that discovers mounted file systems and
outputs JSON, which includes both file system name and type. One way to
use it would be as a UserParameter with key "vfs.fs.discovery\_perl":

``` {.perl}
#!/usr/bin/perl

$first = 1;

print "{\n";
print "\t\"data\":[\n\n";

for (`cat /proc/mounts`)
{
    ($fsname, $fstype) = m/\S+ (\S+) (\S+)/;

    print "\t,\n" if not $first;
    $first = 0;

    print "\t{\n";
    print "\t\t\"{#FSNAME}\":\"$fsname\",\n";
    print "\t\t\"{#FSTYPE}\":\"$fstype\"\n";
    print "\t}\n";
}

print "\n\t]\n";
print "}\n";
```

::: noteimportant
Allowed symbols for LLD macro names are **0-9** ,
**A-Z** , **\_** , **.**\
\
Lowercase letters are not supported in the names.
:::

An example of its output (reformatted for clarity) is shown below. JSON
for custom discovery checks has to follow the same format.

    {
      "data":[
      
      { "{#FSNAME}":"/",                           "{#FSTYPE}":"rootfs"   },
      { "{#FSNAME}":"/sys",                        "{#FSTYPE}":"sysfs"    },
      { "{#FSNAME}":"/proc",                       "{#FSTYPE}":"proc"     },
      { "{#FSNAME}":"/dev",                        "{#FSTYPE}":"devtmpfs" },
      { "{#FSNAME}":"/dev/pts",                    "{#FSTYPE}":"devpts"   },
      { "{#FSNAME}":"/lib/init/rw",                "{#FSTYPE}":"tmpfs"    },
      { "{#FSNAME}":"/dev/shm",                    "{#FSTYPE}":"tmpfs"    },
      { "{#FSNAME}":"/home",                       "{#FSTYPE}":"ext3"     },
      { "{#FSNAME}":"/tmp",                        "{#FSTYPE}":"ext3"     },
      { "{#FSNAME}":"/usr",                        "{#FSTYPE}":"ext3"     },
      { "{#FSNAME}":"/var",                        "{#FSTYPE}":"ext3"     },
      { "{#FSNAME}":"/sys/fs/fuse/connections",    "{#FSTYPE}":"fusectl"  }
      
      ]
    }

Then, in the discovery rule's "Filter" field, we could specify
"{\#FSTYPE}" as a macro and "rootfs|ext3" as a regular expression.

::: noteclassic
You don't have to use macro names FSNAME/FSTYPE with custom
LLD rules, you are free to use whatever names you like.
:::

Note that, if using a user parameter, the return value is limited to 512
KB. For more details, see [data limits for LLD return
values](/manual/discovery/low_level_discovery#data_limits_for_return_values).

#### Using LLD macros in user macro contexts

User macros [with
context](/manual/config/macros/user_macros#user_macro_context) can be
used to accomplish more flexible thresholds in trigger expressions.
Different thresholds may be defined on user macro level and then used in
trigger constants depending on the discovered context. Discovered
context appears when the [low-level discovery
macros](/manual/config/macros/lld_macros) used in the macros are
resolved to real values.

To illustrate we can use data from the example above and assume that the
following file systems will be discovered: `/`, `/home`, `/tmp`, `/usr`,
`/var`.

We may define a free-disk-space trigger prototype for a host, where the
threshold is expressed by a user macro with context:

`{host:vfs.fs.size[{#FSNAME},pfree].last()}<{$LOW_SPACE_LIMIT:"{#FSNAME}"}`

Then add user macros:

-   `{$LOW_SPACE_LIMIT}` **10**
-   `{$LOW_SPACE_LIMIT:/home}` **20**
-   `{$LOW_SPACE_LIMIT:/tmp}` **50**

Now, once the file systems are discovered, events will be generated if
`/`, `/usr` and `/var` filesystems have less than **10**% of free disk
space, the `/home` filesystem - less than **20**% of free disk space or
the `/tmp` filesystem - less than **50**% of free disk space.

[comment]: # ({/new-554f9404})
