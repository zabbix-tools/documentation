[comment]: # translation:outdated

[comment]: # ({new-bd6224b0})
# 1 Configuring a network discovery rule

[comment]: # ({/new-bd6224b0})

[comment]: # ({new-466f5982})
#### Overview

To configure a network discovery rule used by Zabbix to discover hosts
and services:

-   Go to *Configuration → Discovery*
-   Click on *Create rule* (or on the rule name to edit an existing one)
-   Edit the discovery rule attributes

[comment]: # ({/new-466f5982})

[comment]: # ({new-f020a6f3})
#### Rule attributes

![](../../../../assets/en/manual/discovery/network_discovery/d_rule.png)

|Parameter|Description|
|---------|-----------|
|*Name*|Unique name of the rule. For example, "Local network".|
|*Discovery by proxy*|What performs discovery:<br>**no proxy** - Zabbix server is doing discovery<br>**<proxy name>** - this proxy performs discovery|
|*IP range*|The range of IP addresses for discovery. It may have the following formats:<br>Single IP: 192.168.1.33<br>Range of IP addresses: 192.168.1-10.1-255. The range is limited by the total number of covered addresses (less than 64K).<br>IP mask: 192.168.4.0/24<br>supported IP masks:<br>/16 - /30 for IPv4 addresses<br>/112 - /128 for IPv6 addresses<br>List: 192.168.1.1-255, 192.168.2.1-100, 192.168.2.200, 192.168.4.0/24<br>Since Zabbix 3.0.0 this field supports spaces, tabulation and multiple lines.|
|*Update interval*|This parameter defines how often Zabbix will execute the rule.<br>The interval is measured after the execution of previous discovery instance ends so there is no overlap.<br>[Time suffixes](/manual/appendix/suffixes) are supported, e.g. 30s, 1m, 2h, 1d, since Zabbix 3.4.0.<br>[User macros](/manual/config/macros/user_macros) are supported, since Zabbix 3.4.0.<br>*Note* that if a user macro is used and its value is changed (e.g. 1w → 1h), the next check will be executed according to the previous value (far in the future with the example values).|
|*Checks*|Zabbix will use this list of checks for discovery.<br>Supported checks: SSH, LDAP, SMTP, FTP, HTTP, HTTPS, POP, NNTP, IMAP, TCP, Telnet, Zabbix agent, SNMPv1 agent, SNMPv2 agent, SNMPv3 agent, ICMP ping.<br>A protocol-based discovery uses the **net.tcp.service\[\]** functionality to test each host, except for SNMP which queries an SNMP OID. Zabbix agent is tested by querying an item in unencrypted mode. Please see [agent items](/fr/manual/config/items/itemtypes/zabbix_agent) for more details.<br>The 'Ports' parameter may be one of following:<br>Single port: 22<br>Range of ports: 22-45<br>List: 22-45,55,60-70|
|*Device uniqueness criteria*|Uniqueness criteria may be:<br>**IP address** - no processing of multiple single-IP devices. If a device with the same IP already exists it will be considered already discovered and a new host will not be added.<br>**Type of discovery check** - either SNMP or Zabbix agent check.|
|*Enabled*|With the check-box marked the rule is active and will be executed by Zabbix server.<br>If unmarked, the rule is not active. It won't be executed.|

[comment]: # ({/new-f020a6f3})

[comment]: # ({new-cd84714a})
#### Changing proxy setting

Since Zabbix 2.2.0 the hosts discovered by different proxies are always
treated as different hosts. While this allows to perform discovery on
matching IP ranges used by different subnets, changing proxy for an
already monitored subnet is complicated because the proxy changes must
be also applied to all discovered hosts. For example the steps to
replace proxy in a discovery rule:

1.  disable discovery rule
2.  sync proxy configuration
3.  replace the proxy in the discovery rule
4.  replace the proxy for all hosts discovered by this rule
5.  enable discovery rule

[comment]: # ({/new-cd84714a})

[comment]: # ({new-78f4f491})
#### A real life scenario

In this example we would like to set up network discovery for the local
network having an IP range of 192.168.1.1-192.168.1.254.

In our scenario we want to:

-   discover those hosts that have Zabbix agent running
-   run discovery every 10 minutes
-   add a host to monitoring if the host uptime is more than 1 hour
-   remove hosts if the host downtime is more than 24 hours
-   add Linux hosts to the "Linux servers" group
-   add Windows hosts to the "Windows servers" group
-   use *Template OS Linux* for Linux hosts
-   use *Template OS Windows* for Windows hosts

[comment]: # ({/new-78f4f491})

[comment]: # ({new-b8e28285})
##### Step 1

Defining a network discovery rule for our IP range.

![](../../../../assets/en/manual/discovery/network_discovery/discovery.png)

Zabbix will try to discover hosts in the IP range of
192.168.1.1-192.168.1.254 by connecting to Zabbix agents and getting the
value from **system.uname** key. The value received from the agent can
be used to apply different actions for different operating systems. For
example, link Windows servers to Template OS Windows, Linux servers to
Template OS Linux.

The rule will be executed every 10 minutes (600 seconds).

When this rule is added, Zabbix will automatically start the discovery
and generating discovery-based events for further processing.

[comment]: # ({/new-b8e28285})

[comment]: # ({new-b1d8800e})
##### Step 2

Defining an [action](/fr/manual/config/notifications/action) for adding
the discovered Linux servers to the respective group/template.

![](../../../../assets/en/manual/discovery/network_discovery/disc_action_1.png)

The action will be activated if:

-   the "Zabbix agent" service is "up"
-   the value of system.uname (the Zabbix agent key we used in rule
    definition) contains "Linux"
-   Uptime is 1 hour (3600 seconds) or more

![](../../../../assets/en/manual/discovery/network_discovery/disc_action_1b.png)

The action will execute the following operations:

-   add the discovered host to the "Linux servers" group (and also add
    host if it wasn't added previously)
-   link host to the "Template OS Linux" template. Zabbix will
    automatically start monitoring the host using items and triggers
    from "Template OS Linux".

[comment]: # ({/new-b1d8800e})

[comment]: # ({new-79eefb1b})
##### Step 3

Defining an action for adding the discovered Windows servers to the
respective group/template.

![](../../../../assets/en/manual/discovery/network_discovery/disc_action_2.png)

![](../../../../assets/en/manual/discovery/network_discovery/disc_action_2b.png)

##### Step 4

Defining an action for removing lost servers.

![](../../../../assets/en/manual/discovery/network_discovery/disc_action_3.png)

![](../../../../assets/en/manual/discovery/network_discovery/disc_action_3b.png)

A server will be removed if "Zabbix agent" service is 'down' for more
than 24 hours (86400 seconds).

[comment]: # ({/new-79eefb1b})
