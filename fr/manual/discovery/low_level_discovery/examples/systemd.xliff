<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="fr" datatype="plaintext" original="manual/discovery/low_level_discovery/examples/systemd.md">
    <body>
      <trans-unit id="ec525f2f" xml:space="preserve">
        <source># 8 Discovery of systemd services</source>
      </trans-unit>
      <trans-unit id="6e5dadb8" xml:space="preserve">
        <source>### Overview

It is possible to [discover](/manual/discovery/low_level_discovery)
systemd units (services, by default) with Zabbix.</source>
      </trans-unit>
      <trans-unit id="e655c9cf" xml:space="preserve">
        <source>### Item key

The item to use in the [discovery
rule](/manual/discovery/low_level_discovery#discovery_rule) is the

    systemd.unit.discovery

::: noteimportant
This
[item](/manual/config/items/itemtypes/zabbix_agent/zabbix_agent2) key is
only supported in Zabbix agent 2.
:::

This item returns a JSON with information about systemd units, for
example:

    [{
        "{#UNIT.NAME}": "mysqld.service",
        "{#UNIT.DESCRIPTION}": "MySQL Server",
        "{#UNIT.LOADSTATE}": "loaded",
        "{#UNIT.ACTIVESTATE}": "active",
        "{#UNIT.SUBSTATE}": "running",
        "{#UNIT.FOLLOWED}": "",
        "{#UNIT.PATH}": "/org/freedesktop/systemd1/unit/mysqld_2eservice",
        "{#UNIT.JOBID}": 0,
        "{#UNIT.JOBTYPE}": "",
        "{#UNIT.JOBPATH}": "/",
        "{#UNIT.UNITFILESTATE}": "enabled"
    }, {
        "{#UNIT.NAME}": "systemd-journald.socket",
        "{#UNIT.DESCRIPTION}": "Journal Socket",
        "{#UNIT.LOADSTATE}": "loaded",
        "{#UNIT.ACTIVESTATE}": "active",
        "{#UNIT.SUBSTATE}": "running",
        "{#UNIT.FOLLOWED}": "",
        "{#UNIT.PATH}": "/org/freedesktop/systemd1/unit/systemd_2djournald_2esocket",
        "{#UNIT.JOBID}": 0,
        "{#UNIT.JOBTYPE}": "",
        "{#UNIT.JOBPATH}": "/",
        "{#UNIT.UNITFILESTATE}": "enabled"
    }]</source>
      </trans-unit>
      <trans-unit id="2d3b32d7" xml:space="preserve">
        <source>
##### Discovery of disabled systemd units

Since Zabbix 6.0.1 it is also possible to discover **disabled** systemd units. In this case 
three macros are returned in the resulting JSON: 

-    {#UNIT.PATH}
-    {#UNIT.ACTIVESTATE}
-    {#UNIT.UNITFILESTATE}. 

::: noteimportant
To have items and triggers created from prototypes for disabled systemd units, make sure to 
adjust (or remove) prohibiting LLD filters for {#UNIT.ACTIVESTATE} and {#UNIT.UNITFILESTATE}.
:::</source>
      </trans-unit>
      <trans-unit id="0272b801" xml:space="preserve">
        <source>### Supported macros

The following macros are supported for use in the discovery rule
[filter](/manual/discovery/low_level_discovery#discovery_rule_filter)
and prototypes of items, triggers and graphs:

|Macro|Description|
|-----|-----------|
|{\#UNIT.NAME}|Primary unit name.|
|{\#UNIT.DESCRIPTION}|Human readable description.|
|{\#UNIT.LOADSTATE}|Load state (i.e. whether the unit file has been loaded successfully)|
|{\#UNIT.ACTIVESTATE}|Active state (i.e. whether the unit is currently started or not)|
|{\#UNIT.SUBSTATE}|Sub state (a more fine-grained version of the active state that is specific to the unit type, which the active state is not)|
|{\#UNIT.FOLLOWED}|Unit that is being followed in its state by this unit, if there is any; otherwise an empty string.|
|{\#UNIT.PATH}|Unit object path.|
|{\#UNIT.JOBID}|Numeric job ID if there is a job queued for the job unit; 0 otherwise.|
|{\#UNIT.JOBTYPE}|Job type.|
|{\#UNIT.JOBPATH}|Job object path.|
|{\#UNIT.UNITFILESTATE}|The install state of the unit file.|</source>
      </trans-unit>
      <trans-unit id="c8363d40" xml:space="preserve">
        <source>### Item prototypes

Item prototypes that can be created based on systemd service discovery
include, for example:

-   Item name: `{#UNIT.DESCRIPTION}`; item key:
    `systemd.unit.info["{#UNIT.NAME}"]`
-   Item name: `{#UNIT.DESCRIPTION}`; item key:
    `systemd.unit.info["{#UNIT.NAME}",LoadState]`

`systemd.unit.info` [agent
items](/manual/config/items/itemtypes/zabbix_agent/zabbix_agent2) are
supported since Zabbix 4.4.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
