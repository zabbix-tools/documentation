[comment]: # translation:outdated

[comment]: # ({new-0a2d9a93})
# 1 Proxys

[comment]: # ({/new-0a2d9a93})

[comment]: # ({new-5a59cd81})
#### Vue d'ensemble

Un proxy Zabbix peut collecter des données de performances et de
disponibilité pour le compte du serveur Zabbix. De cette façon, un proxy
peut prendre en charge une partie de la collecte des données et
décharger le serveur Zabbix.

De plus, l'utilisation d'un proxy est le moyen le plus simple
d'implémenter la surveillance centralisée et distribuée, lorsque tous
les agents et proxys sont rattachés à un serveur Zabbix et que toutes
les données sont collectées de manière centralisée.

Un proxy Zabbix peut être utilisé pour:

-   Surveiller les emplacements distants
-   Surveiller les emplacements ayant des communications non fiables
-   Décharger le serveur Zabbix lors de la surveillance de milliers de
    périphériques
-   Simplifier la maintenance de la surveillance distribuée

![](../../../assets/fr/manual/distributed_monitoring/proxy.png)

Le proxy ne nécessite qu'une seule connexion TCP au serveur Zabbix. De
cette façon, il est plus facile de passer par un pare-feu, car il suffit
de configurer une seule règle.

::: noteimportant
Le proxy Zabbix doit utiliser une base de données
distincte. Pointer sur la base de données du serveur Zabbix cassera la
configuration.
:::

Toutes les données collectées par le proxy sont stockées localement
avant d'être transmises au serveur. De cette façon, aucune donnée n'est
perdue en raison de problèmes de communication temporaires avec le
serveur. Les paramètres *ProxyLocalBuffer* et *ProxyOfflineBuffer* du
[fichier de configuration du
proxy](/fr/manual/appendix/config/zabbix_proxy) contrôlent la durée de
rétention des données en local.

::: noteimportant
Il peut arriver qu'un proxy, qui reçoit les
dernières modifications de configuration directement depuis la base de
données du serveur Zabbix, ait une configuration plus à jour que le
serveur Zabbix dont la configuration risque de ne pas être mise à jour
aussi rapidement en raison de la valeur de
[CacheUpdateFrequency](/fr/manual/appendix/config/zabbix_server). Par
conséquent, le proxy peut commencer à collecter des données et les
envoyer au serveur Zabbix qui les ignorera.
:::

Le proxy Zabbix est un collecteur de données. Il ne calcule pas les
déclencheurs, ne traite pas les événements et n'envoie pas d'alertes.
Pour un aperçu des fonctionnalités du proxy, voir le tableau suivant:

|Fonction|<|Supportée par le proxy|
|--------|-|-----------------------|
|Éléments|<|<|
|<|*Vérifications de l'agent Zabbix*|**Oui**|
|^|*Vérifications de l'agent Zabbix (actif)*|**Oui** ^1^|
|^|*Vérifications simples*|**Oui**|
|^|*Éléments trappés*|**Oui**|
|^|*Vérifications SNMP*|**Oui**|
|^|*Traps SNMP*|**Oui**|
|^|*Vérifications IPMI*|**Oui**|
|^|*Vérifications JMX*|**Oui**|
|^|*Surveillance des fichiers de logs*|**Oui**|
|^|*Vérifications internes*|**Oui**|
|^|*Vérifications SSH*|**Oui**|
|^|*Vérifications Telnet*|**Oui**|
|^|*Vérifications externes*|**Oui**|
|^|*Éléments dépendants*|**Oui** ^2^|
|Surveillance Web intégrée|<|**Oui**|
|Découverte du réseau|<|**Oui**|
|Découverte de bas niveau|<|**Oui**|
|Commandes à distance|<|**Oui**|
|Déclencheurs calculés|<|*Non*|
|Traitement des événements|<|*Non*|
|Corrélation d'événement|<|*Non*|
|Envoi d'alertes|<|*Non*|
|Pré-traitement de la valeur de l'élément|<|*Non*|

::: noteclassic
\[1\] Pour s'assurer qu'un agent demande au proxy (et non au
serveur) des vérifications actives, le proxy doit être listé dans le
paramètre **ServerActive** du fichier de configuration de
l'agent.
:::

::: noteclassic
\[2\] Le pré-traitement de la valeur de l'élément par le
serveur Zabbix est requis pour extraire la valeur requise des données de
l'élément principal.
:::

[comment]: # ({/new-5a59cd81})

[comment]: # ({new-737fe0c2})
##### Protection from overloading

If Zabbix server was down for some time, and proxies have collected a lot of data, 
and then the server starts, it may get overloaded (history cache usage stays at 95-100% for some time). 
This overload could result in a performance hit, where checks are processed slower 
than they should. Protection from this scenario was implemented to avoid problems that arise 
due to overloading history cache.

When Zabbix server history cache is full the history cache write access is being 
throttled, stalling server data gathering processes. The most common history cache 
overload case is after server downtime when proxies are uploading gathered data. To avoid 
this proxy throttling was added (currently it cannot be disabled).

Zabbix sever will stop accepting data from proxies when history cache usage reaches 80%. 
Instead those proxies will be put on a throttling list. This will continue until the cache usage 
falls down to 60%. Now server will start accepting data from proxies one by one, defined by the 
throttling list. This means the first proxy that attempted to upload data during the throttling 
period will be served first and until it's done the server will not accept data from other proxies.

This throttling mode will continue until either cache usage hits 80% again or falls down to 20% 
or the throttling list is empty. In the first case the server will stop accepting proxy data again. 
In the other two cases the server will start working normally, accepting data from all proxies.

You may use the `zabbix[wcache,history,pused]` internal item to correlate this behavior of Zabbix 
server with a metric.

[comment]: # ({/new-737fe0c2})

[comment]: # ({new-e3203196})
#### Configuration

Une fois le proxy [installé](/fr/manual/installation/install) et
[configuré](/fr/manual/appendix/config/zabbix_proxy), il est temps de le
configurer dans l'interface Zabbix.

[comment]: # ({/new-e3203196})

[comment]: # ({new-a5555aaf})
##### Ajouter des proxys

Pour configurer un proxy dans l'interface Zabbix:

-   Aller sur la page: *Administration → Proxys*
-   Cliquer sur *Créer un proxy*

![](../../../assets/fr/manual/distributed_monitoring/proxy.png){width="600"}

|Paramètre|<|Description|
|----------|-|-----------|
|*Nom du proxy*|<|Saisir le nom du proxy. Il doit avoir le même nom que dans le paramètre *Hostname* du fichier de configuration du proxy.|
|*Mode proxy*|<|Sélectionnerz le mode proxy.<br>**Actif** - le proxy se connecte au serveur Zabbix et demande les données de configuration<br>**Passif** - Le serveur Zabbix se connecte au proxy<br>*A Noter* que sans communications chiffrées (sensibles), les données de configuration du proxy peuvent être disponibles pour les tiers ayant accès au port du trappeur du serveur Zabbix lors de l'utilisation d'un proxy actif. Cela est possible car tout le monde peut prétendre être un proxy actif et demander des données de configuration si l'authentification n'a pas lieu ou si les adresses proxys ne sont pas limitées dans le champ *Adresse du proxy*.|
|*Adresse du proxy*|<|Si spécifié, les demandes de proxy actives sont acceptées uniquement à partir de cette liste d'adresses IP séparées par des virgules, éventuellement en notation CIDR ou de noms DNS du proxy Zabbix actif.<br>Ce champ est disponible uniquement si un proxy actif est sélectionné dans le champ *Mode proxy*. Les macros ne sont pas prises en charge.<br>Cette option est prise en charge depuis Zabbix 4.0.0.|
|*Interface*|<|Saisir les détails de l'interface du proxy passif.<br>Ce champ n'est disponible que si un proxy passif est sélectionné dans le champ *Mode proxy*.|
|<|*adresse IP*|Adresse IP du proxy passif (facultatif).|
|^|*Nom DNS*|Nom DNS du proxy passif (facultatif).|
|^|// Connexion à//|En cliquant sur le bouton correspondant, vous indiquerez au serveur Zabbix comment récupérer les données du proxy:<br>**IP** - Se connecter via l'adresse IP du proxy (recommandé)<br>**DNS** - Se connecter via le nom DNS du proxy|
|^|*Port*|Numéro de port TCP/UDP du proxy passif (10051 par défaut).|
|*Description*|<|Saisir la description du proxy.|

L'onglet **Chiffrement** permet d'exiger des connexions chiffrées avec
le proxy.

|Paramètre|Description|
|----------|-----------|
|*Connexion au proxy*|Comment le serveur se connecte au proxy passif: pas de chiffrement (par défaut), en utilisant PSK (clé pré-partagée) ou un certificat.|
|*Connexions du proxy*|Sélectionner le type de connexion autorisé à partir du proxy actif. Plusieurs types de connexion peuvent être sélectionnés en même temps (utile pour tester et passer à un autre type de connexion). "Pas de chiffrement" par défaut.|
|*Délivré par*|Émetteur autorisé du certificat. Le certificat est d'abord validé avec l'autorité de certification (CA). S'il est valide, signé par l'autorité de certification, le champ *Délivré par* peut être utilisé pour restreindre davantage l'autorité de certification autorisée. Ce champ est facultatif, à utiliser si votre installation Zabbix utilise des certificats provenant de plusieurs autorités de certification.|
|*Sujet*|Sujet autorisé du certificat. Le certificat est d'abord validé avec CA. S'il est valide, signé par l'autorité de certification, le champ *Sujet* peut être utilisé pour autoriser une seule valeur pour la chaîne *Sujet*. Si ce champ est vide, tout certificat valide signé par l'autorité de certification configurée est accepté.|
|*Identité PSK*|Chaîne d'identité de clé pré-partagée.|
|*PSK*|Clé pré-partagée (chaîne hexadécimale). Longueur maximale: 512 chiffres hexadécimaux (PSK 256 octets) si Zabbix utilise la bibliothèque GnuTLS ou OpenSSL, 64 chiffres hexadécimaux (PSK 32 octets) si Zabbix utilise la bibliothèque mbed TLS (PolarSSL). Exemple: 1f87b595725ac58dd977beef14b97461a7c1045b9a1c963065002c5473194952|

[comment]: # ({/new-a5555aaf})

[comment]: # ({new-96c17aaf})
##### Configuration de l'hôte

Vous pouvez spécifier qu'un hôte spécifique doit être surveillé par un
proxy dans le formulaire de [configuration de
l'hôte](/fr/manual/config/hosts/host), en utilisant le champ *Surveillé
via le proxy*.

![](../../../assets/fr/manual/distributed_monitoring/proxy_set.png)

La [mise à jour groupée](/fr/manual/config/hosts/hostupdate) des hôtes
peut être une autre façon de spécifier que les hôtes doivent être
surveillés par un proxy.

[comment]: # ({/new-96c17aaf})
