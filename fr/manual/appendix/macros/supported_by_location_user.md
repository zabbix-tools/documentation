[comment]: # translation:outdated

[comment]: # ({new-d428738a})
# 2 User macros supported by location

[comment]: # ({/new-d428738a})

[comment]: # ({new-f0afb8e0})
#### Overview

[User-definable](/manual/config/macros/user_macros) macros are supported
in the following locations:

-   Hosts
    -   Interface IP/DNS
    -   Interface port

```{=html}
<!-- -->
```
-   Passive proxy
    -   Interface port

```{=html}
<!-- -->
```
-   Items and item prototypes
    -   Name
    -   Key parameters
    -   Update interval
    -   Custom intervals
    -   History storage period
    -   Trend storage period
    -   SNMPv3 context name
    -   SNMPv3 security name
    -   SNMPv3 auth pass
    -   SNMPv3 priv pass
    -   SNMPv1/v2 community
    -   SNMP OID
    -   SNMP port
    -   SSH username
    -   SSH public key
    -   SSH private key
    -   SSH password
    -   SSH script
    -   Telnet username
    -   Telnet password
    -   Telnet script
    -   Calculated item
        [formula](/fr/manual/config/items/itemtypes/calculated#configurable_fields)
    -   Trapper item "Allowed hosts" field
    -   Database monitoring additional parameters
    -   JMX item endpoint field
    -   since Zabbix 4.0 also in:
        -   item value preprocessing steps
        -   HTTP agent URL field
        -   HTTP agent HTTP query fields field
        -   HTTP agent request body field
        -   HTTP agent required status codes field
        -   HTTP agent headers field key and value
        -   HTTP agent HTTP authentication username field
        -   HTTP agent HTTP authentication password field
        -   HTTP agent HTTP proxy field
        -   HTTP agent SSL certificate file field
        -   HTTP agent SSL key file field
        -   HTTP agent SSL key password field
        -   HTTP agent HTTP timeout field
        -   HTTP agent HTTP allowed hosts field

```{=html}
<!-- -->
```
-   Discovery

```{=html}
<!-- -->
```
        * Update interval
        * SNMPv3 context name
        * SNMPv3 security name
        * SNMPv3 auth pass
        * SNMPv3 priv pass
        * SNMPv1/v2 community
        * SNMP OID

-   Low-level discovery rule
    -   Name
    -   Key parameters
    -   Update interval
    -   Custom intervals
    -   SNMPv3 context name
    -   SNMPv3 security name
    -   SNMPv3 auth pass
    -   SNMPv3 priv pass
    -   SNMPv1/v2 community
    -   SNMP OID
    -   SNMP port
    -   SSH username
    -   SSH public key
    -   SSH private key
    -   SSH password
    -   SSH script
    -   Telnet username
    -   Telnet password
    -   Telnet script
    -   Trapper item "Allowed hosts" field
    -   Database monitoring additional parameters
    -   JMX item endpoint field
    -   Keep lost resources period
    -   Filter regular expressions
    -   since Zabbix 4.0 also in:
        -   HTTP agent URL field
        -   HTTP agent HTTP query fields field
        -   HTTP agent request body field
        -   HTTP agent required status codes field
        -   HTTP agent headers field key and value
        -   HTTP agent HTTP authentication username field
        -   HTTP agent HTTP authentication password field
        -   HTTP agent HTTP timeout field

```{=html}
<!-- -->
```
-   Web scenario

```{=html}
<!-- -->
```
        * Name
        * Update interval
        * Agent
        * HTTP proxy
        * Variables
        * Headers
        * Step name
        * Step URL
        * Step post variables
        * Step headers
        * Step timeout
        * Required string
        * Required status codes
        * Authentication (user and password)
        * SSL certificate file
        * SSL key file
        * SSL key password

-   Triggers

```{=html}
<!-- -->
```
        * Name
        * Expression (only in constants and function parameters)
        * Description
        * URLs

-   Trigger-based notifications
-   Trigger-based internal notifications

```{=html}
<!-- -->
```
-   Event tags

```{=html}
<!-- -->
```
        * Tag name 
        * Tag value 
        * Tag for matching 

-   Action operations

```{=html}
<!-- -->
```
        * Default operation step duration
        * Step duration

-   Action conditions

```{=html}
<!-- -->
```
        * Time period condition

-   Global scripts (including confirmation text)

```{=html}
<!-- -->
```
-   URL field of dynamic URL screen element

```{=html}
<!-- -->
```
-   Administration → Users → Media: 'When active' field
-   Administration → General → Working time: 'Working time' field

For a complete list of all macros supported Zabbix, see [macros
supported by location](/manual/appendix/macros/supported_by_location).

[comment]: # ({/new-f0afb8e0})

[comment]: # ({new-7cb4c86b})
#### Actions

In [actions](/manual/config/notifications/action), user macros can be
used in the following fields:

|Location|<|Multiple macros/mix with text^[1](supported_by_location_user#footnotes)^|
|--------|-|------------------------------------------------------------------------|
|Trigger-based notifications and commands|<|yes|
|Trigger-based internal notifications|<|yes|
|Problem update notifications|<|yes|
|Service-based notifications and commands|<|yes|
|Service update notifications|<|yes|
|Time period condition|<|no|
|*Operations*|<|<|
|<|Default operation step duration|no|
|^|Step duration|no|

[comment]: # ({/new-7cb4c86b})

[comment]: # ({new-f30645e0})
#### Hosts/host prototypes

In a [host](/manual/config/hosts/host) and [host
prototype](/manual/vm_monitoring#host_prototypes) configuration, user
macros can be used in the following fields:

|Location|<|Multiple macros/mix with text^[1](supported_by_location_user#footnotes)^|
|--------|-|------------------------------------------------------------------------|
|Interface IP/DNS|<|DNS only|
|Interface port|<|no|
|*SNMP v1, v2*|<|<|
|<|SNMP community|yes|
|*SNMP v3*|<|<|
|<|Context name|yes|
|^|Security name|yes|
|^|Authentication passphrase|yes|
|^|Privacy passphrase|yes|
|*IPMI*|<|<|
|<|Username|yes|
|^|Password|yes|
|//Tags //|<|<|
|<|Tag names|yes|
|^|Tag values|yes|

[comment]: # ({/new-f30645e0})


[comment]: # ({new-747bd0ca})
#### Low-level discovery

In a [low-level discovery
rule](/manual/discovery/low_level_discovery#configuring_low-level_discovery),
user macros can be used in the following fields:

|Location|<|Multiple macros/mix with text^[1](supported_by_location_user#footnotes)^|
|--------|-|------------------------------------------------------------------------|
|Key parameters|<|yes|
|Update interval|<|no|
|Custom interval|<|no|
|Keep lost resources period|<|no|
|Description|<|yes|
|*SNMP agent*|<|<|
|<|SNMP OID|yes|
|*SSH agent*|<|<|
|<|Username|yes|
|^|Public key file|yes|
|^|Private key file|yes|
|^|Password|yes|
|^|Script|yes|
|*TELNET agent*|<|<|
|<|Username|yes|
|^|Password|yes|
|^|Script|yes|
|*Zabbix trapper*|<|<|
|<|Allowed hosts|yes|
|*Database monitor*|<|<|
|<|Additional parameters|yes|
|*JMX agent*|<|<|
|<|JMX endpoint|yes|
|*HTTP agent*|<|<|
|<|URL^[2](supported_by_location_user#footnotes)^|yes|
|^|Query fields|yes|
|^|Timeout|no|
|^|Request body|yes|
|^|Headers (names and values)|yes|
|^|Required status codes|yes|
|^|HTTP authentication username|yes|
|^|HTTP authentication password|yes|
|*Filters*|<|<|
|<|Regular expression|yes|
|*Overrides*|<|<|
|<|Filters: regular expression|yes|
|^|Operations: update interval (for item prototypes)|no|
|^|Operations: history storage period (for item prototypes)|no|
|^|Operations: trend storage period (for item prototypes)|no|

[comment]: # ({/new-747bd0ca})

[comment]: # ({new-95f2f372})
#### Network discovery

In a [network discovery rule](/manual/discovery/network_discovery/rule),
user macros can be used in the following fields:

|Location|<|Multiple macros/mix with text^[1](supported_by_location_user#footnotes)^|
|--------|-|------------------------------------------------------------------------|
|Update interval|<|no|
|*SNMP v1, v2*|<|<|
|<|SNMP community|yes|
|^|SNMP OID|yes|
|*SNMP v3*|<|<|
|<|Context name|yes|
|^|Security name|yes|
|^|Authentication passphrase|yes|
|^|Privacy passphrase|yes|
|^|SNMP OID|yes|

[comment]: # ({/new-95f2f372})

[comment]: # ({new-575d88fc})
#### Proxies

In a [proxy](/manual/distributed_monitoring/proxies#configuration)
configuration, user macros can be used in the following field:

|Location|<|Multiple macros/mix with text^[1](supported_by_location_user#footnotes)^|
|--------|-|------------------------------------------------------------------------|
|Interface port (for passive proxy)|<|no|

[comment]: # ({/new-575d88fc})

[comment]: # ({new-32b6aef1})
#### Templates

In a [template](/manual/config/templates/template) configuration, user
macros can be used in the following fields:

|Location|<|Multiple macros/mix with text^[1](supported_by_location_user#footnotes)^|
|--------|-|------------------------------------------------------------------------|
|*Tags*|<|<|
|<|Tag names|yes|
|^|Tag values|yes|

[comment]: # ({/new-32b6aef1})

[comment]: # ({new-c78b04ef})
#### Triggers

In a [trigger](/manual/config/triggers/trigger) configuration, user
macros can be used in the following fields:

|Location|<|Multiple macros/mix with text^[1](supported_by_location_user#footnotes)^|
|--------|-|------------------------------------------------------------------------|
|Name|<|yes|
|Operational data|<|yes|
|Expression (only in constants and function parameters; secret macros are not supported).|<|yes|
|Description|<|yes|
|URL^[2](supported_by_location_user#footnotes)^|<|yes|
|Tag for matching|<|yes|
|*Tags*|<|<|
|<|Tag names|yes|
|^|Tag values|yes|

[comment]: # ({/new-c78b04ef})

[comment]: # ({new-78f1f511})
#### Web scenario

In a [web scenario](/manual/web_monitoring) configuration, user macros
can be used in the following fields:

|Location|<|Multiple macros/mix with text^[1](supported_by_location_user#footnotes)^|
|--------|-|------------------------------------------------------------------------|
|Name|<|yes|
|Update interval|<|no|
|Agent|<|yes|
|HTTP proxy|<|yes|
|Variables (values only)|<|yes|
|Headers (names and values)|<|yes|
|*Steps*|<|<|
|<|Name|yes|
|^|URL^[2](supported_by_location_user#footnotes)^|yes|
|^|Variables (values only)|yes|
|^|Headers (names and values)|yes|
|^|Timeout|no|
|^|Required string|yes|
|^|Required status codes|no|
|*Authentication*|<|<|
|<|User|yes|
|^|Password|yes|
|^|SSL certificate|yes|
|^|SSL key file|yes|
|^|SSL key password|yes|
|*Tags*|<|<|
|<|Tag names|yes|
|^|Tag values|yes|

[comment]: # ({/new-78f1f511})

[comment]: # ({new-efe84e1d})
#### Other locations

In addition to the locations listed here, user macros can be used in the
following fields:

|Location|<|Multiple macros/mix with text^[1](supported_by_location_user#footnotes)^|
|--------|-|------------------------------------------------------------------------|
|Global scripts (script, SSH, Telnet, IPMI), including confirmation text|<|yes|
|Webhooks|<|<|
|<|JavaScript script|no|
|<|JavaScript script parameter name|no|
|<|JavaScript script parameter value|yes|
|*Monitoring → Dashboards*|<|<|
|<|Description field of *Item value* dashboard widget|yes|
|<|URL^[2](supported_by_location_user#footnotes)^ field of *dynamic URL* dashboard widget|yes|
|*Administration → Users → Media*|<|<|
|<|When active|no|
|*Administration → General → GUI*|<|<|
|<|Working time|no|
|*Administration → Media types → Message templates*|<|<|
|<|Subject|yes|
|^|Message|yes|

For a complete list of all macros supported in Zabbix, see [supported
macros](/manual/appendix/macros/supported_by_location).

[comment]: # ({/new-efe84e1d})

[comment]: # ({new-5a370e78})
##### Footnotes

^**1**^ If multiple macros in a field or macros mixed with text are not
supported for the location, a single macro has to fill the whole field.

^**2**^ URLs that contain a [secret
macro](/manual/config/macros/user_macros#configuration) will not work,
as the macro in them will be resolved as "\*\*\*\*\*\*".

[comment]: # ({/new-5a370e78})

[comment]: # ({new-d51563d6})
#### Items / item prototypes

In an [item](/manual/config/items/item) or an [item
prototype](/manual/discovery/low_level_discovery#item_prototypes)
configuration, user macros can be used in the following fields:

|Location|<|Multiple macros/mix with text^[1](supported_by_location_user#footnotes)^|
|--------|-|------------------------------------------------------------------------|
|Item key parameters|<|yes|
|Update interval|<|no|
|Custom intervals|<|no|
|History storage period|<|no|
|Trend storage period|<|no|
|Description|<|yes|
|*Calculated item*|<|<|
|<|Formula|yes|
|*Database monitor*|<|<|
|<|Username|yes|
|^|Password|yes|
|^|SQL query|yes|
|*HTTP agent*|<|<|
|<|URL^[2](supported_by_location_user#footnotes)^|yes|
|^|Query fields|yes|
|^|Timeout|no|
|^|Request body|yes|
|^|Headers (names and values)|yes|
|^|Required status codes|yes|
|^|HTTP proxy|yes|
|^|HTTP authentication username|yes|
|^|HTTP authentication password|yes|
|^|SSl certificate file|yes|
|^|SSl key file|yes|
|^|SSl key password|yes|
|^|Allowed hosts|yes|
|*JMX agent*|<|<|
|<|JMX endpoint|yes|
|*Script item*|<|<|
|<|Parameter names and values|yes|
|*SNMP agent*|<|<|
|<|SNMP OID|yes|
|*SSH agent*|<|<|
|<|Username|yes|
|^|Public key file|yes|
|^|Private key file|yes|
|^|Password|yes|
|^|Script|yes|
|*TELNET agent*|<|<|
|<|Username|yes|
|^|Password|yes|
|^|Script|yes|
|*Zabbix trapper*|<|<|
|<|Allowed hosts|yes|
|*Tags*|<|<|
|<|Tag names|yes|
|^|Tag values|yes|
|*Preprocessing*|<|<|
|<|Step parameters (including custom scripts)|yes|

[comment]: # ({/new-d51563d6})
