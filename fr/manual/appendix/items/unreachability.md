[comment]: # translation:outdated

[comment]: # ({new-02f012f5})
# 12 Unreachable/unavailable host settings

[comment]: # ({/new-02f012f5})

[comment]: # ({new-b32082d6})
#### Overview

Several configuration
[parameters](/fr/manual/appendix/config/zabbix_server) define how Zabbix
server should behave when an agent check (Zabbix, SNMP, IPMI, JMX) fails
and a host becomes unreachable.

[comment]: # ({/new-b32082d6})

[comment]: # ({new-e56dd03b})
#### Unreachable host

A host is treated as unreachable after a failed check (network error,
timeout) by Zabbix, SNMP, IPMI or JMX agents. Note that Zabbix agent
active checks do not influence host availability in any way.

From that moment **UnreachableDelay** defines how often a host is
rechecked using one of the items (including LLD rules) in this
unreachability situation and such rechecks will be performed already by
unreachable pollers (or IPMI pollers for IPMI checks). By default it is
15 seconds before the next check.

In the Zabbix server log unreachability is indicated by messages like
these:

    Zabbix agent item "system.cpu.load[percpu,avg1]" on host "New host" failed: first network error, wait for 15 seconds
    Zabbix agent item "system.cpu.load[percpu,avg15]" on host "New host" failed: another network error, wait for 15 seconds

Note that the exact item that failed is indicated and the item type
(Zabbix agent).

::: noteclassic
The *Timeout* parameter will also affect how early a host is
rechecked during unreachability. If the Timeout is 20 seconds and
UnreachableDelay 30 seconds, the next check will be in 50 seconds after
the first attempt.
:::

The **UnreachablePeriod** parameter defines how long the unreachability
period is in total. By default UnreachablePeriod is 45 seconds.
UnreachablePeriod should be several times bigger than UnreachableDelay,
so that a host is rechecked more than once before a host becomes
unavailable.

If the unreachable host reappears, the monitoring returns to normal
automatically:

    resuming Zabbix agent checks on host "New host": connection restored

[comment]: # ({/new-e56dd03b})

[comment]: # ({new-ba32db14})
#### Unavailable host

After the UnreachablePeriod ends and the host has not reappeared, the
host is treated as unavailable.

In the server log it is indicated by messages like these:

    temporarily disabling Zabbix agent checks on host "New host": host unavailable

and in the
[frontend](/manual/web_interface/frontend_sections/configuration/hosts)
the host availability icon for the respective interface goes from green
(or gray) to red (note that on mouseover a tooltip with the error
description is displayed):

![](../../../../assets/en/manual/config/unavailable.png)

The **UnavailableDelay** parameter defines how often a host is checked
during host unavailability.

By default it is 60 seconds (so in this case "temporarily disabling",
from the log message above, will mean disabling checks for one minute).

When the connection to the host is restored, the monitoring returns to
normal automatically, too:

    enabling Zabbix agent checks on host "New host": host became available

[comment]: # ({/new-ba32db14})

[comment]: # ({new-36969217})
#### Unavailable interface

After the UnreachablePeriod ends and the interface has not reappeared,
the interface is treated as unavailable.

In the server log it is indicated by messages like these:

    temporarily disabling Zabbix agent checks on host "New host": interface unavailable

and in the
[frontend](/manual/web_interface/frontend_sections/configuration/hosts)
the host availability icon goes from green/gray to yellow/red (the
unreachable interface details can be seen in the hint box that is
displayed when a mouse is positioned on the host availability icon):

![](../../../../assets/en/manual/config/unavailable.png)

The **UnavailableDelay** parameter defines how often an interface is
checked during interface unavailability.

By default it is 60 seconds (so in this case "temporarily disabling",
from the log message above, will mean disabling checks for one minute).

When the connection to the interface is restored, the monitoring returns
to normal automatically, too:

    enabling Zabbix agent checks on host "New host": interface became available

[comment]: # ({/new-36969217})
