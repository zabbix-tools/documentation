[comment]: # translation:outdated

[comment]: # ({new-e8dfa374})
# 2 vm.memory.size parameters

[comment]: # ({/new-e8dfa374})

[comment]: # ({new-0863fe63})
#### Overview

This section provides more details and platform-specific information on
the parameters of the `vm.memory.size[<mode>]` [agent
item](/manual/config/items/itemtypes/zabbix_agent).

[comment]: # ({/new-0863fe63})

[comment]: # ({new-4ea4e379})
#### Parameters

The following parameters are possible for this item:

-   **active** - memory currently in use or very recently used, and so
    it is in RAM
-   **anon** - memory not associated with a file (cannot be re-read from
    it)
-   **available** - available memory, calculated differently depending
    on the platform (see the table below)
-   **buffers** - cache for things like file system metadata
-   **cached** - cache for various things
-   **exec** - executable code, typically from a (program) file
-   **file** - cache for contents of recently accessed files
-   **free** - memory that is readily available to any entity requesting
    memory
-   **inactive** - memory that is marked as not used
-   **pavailable** - inactive + cached + free memory as percentage of
    'total'
-   **pinned** - same as 'wired'
-   **pused** - active + wired memory as percentage of 'total'
-   **shared** - memory that may be simultaneously accessed by multiple
    processes
-   **slab** - total amount of memory used by the kernel to cache data
    structures for its own use
-   **total** - total physical memory available
-   **used** - used memory, calculated differently depending on the
    platform (see the table below)
-   **wired** - memory that is marked to always stay in RAM. It is never
    moved to disk.

Platform-specific calculation of **available** and **used**:

|Platform|**"available"**|**"used"**|
|--------|---------------|----------|
|*AIX*|free + cached|real memory in use|
|*FreeBSD*|inactive + cached + free|active + wired + cached|
|*HP UX*|free|total - free|
|*Linux<3.14*|free + buffers|total - free|
|*Linux 3.14+*|/proc/meminfo, "Cached":+"MemAvailable:"|total - free|
|*NetBSD*|inactive + execpages + file + free|total - free|
|*OpenBSD*|inactive + free + cached|active + wired|
|*OSX*|inactive + free|active + wired|
|*Solaris*|free|total - free|
|*Win32*|free|total - free|

::: noteimportant
The sum of *vm.memory.size\[used\]* and
*vm.memory.size\[available\]* does not necessarily equal total. For
instance, on FreeBSD:\
\* Active, inactive, wired, cached memories are considered used, because
they store some useful information.\
\* At the same time inactive, cached, free memories are considered
available, because these kinds of memories can be given instantly to
processes that request more memory.\
\
So inactive memory is both used and available simultaneously. Because of
this, the *vm.memory.size\[used\]* item is designed for informational
purposes only, while *vm.memory.size\[available\]* is designed to be
used in triggers.
:::

See the ["See also"](#see_also) section at the bottom of this page to
find more detailed information about memory calculation in different OS.

[comment]: # ({/new-4ea4e379})

[comment]: # ({new-f80ad839})
##### Platform-specific notes

-   on Linux **shared** works only on kernel 2.4

### See also

1.  [Detailed information about memory calculation in different
    OS](http://blog.zabbix.com/when-alexei-isnt-looking#vm.memory.size)

[comment]: # ({/new-f80ad839})
