[comment]: # translation:outdated

[comment]: # ({new-4d5c1b80})
# 8 Unit symbols

[comment]: # ({/new-4d5c1b80})

[comment]: # ({new-393f9290})
#### Overview

Having to use some large numbers, for example '86400' to represent the
number of seconds in one day, is both difficult and error-prone. This is
why you can use some appropriate unit symbols (or suffixes) to simplify
Zabbix trigger expressions and item keys.

Instead of '86400' for the number of seconds you can simply enter '1d'.
Suffixes function as multipliers.

[comment]: # ({/new-393f9290})

[comment]: # ({new-163b4f50})
#### Time suffixes

For time you can use:

-   **s** - seconds (when used, works the same as the raw value)
-   **m** - minutes
-   **h** - hours
-   **d** - days
-   **w** - weeks

Time suffixes are supported in:

-   trigger [expression](/manual/config/triggers/expression) constants
    and function parameters
-   item configuration ('Update interval', 'Custom intervals', 'History
    storage period' and 'Trend storage period' fields)
-   item prototype configuration ('Update interval', 'Custom intervals',
    'History storage period' and 'Trend storage period' fields)
-   low-level discovery rule configuration ('Update interval', 'Custom
    intervals', 'Keep lost resources' fields)
-   network discovery configuration ('Update interval' field)
-   web scenario configuration ('Update interval', 'Timeout' fields)
-   action operation configuration ('Default operation step duration',
    'Step duration' fields)
-   slide show configuration ('Default delay' field)
-   user profile settings ('Auto-logout', 'Refresh', 'Message timeout'
    fields)
-   *Administration* → *General* → *Housekeeping* (storage period
    fields)
-   *Administration* → *General* → *Trigger displaying options*
    ('Display OK triggers for', 'On status change triggers blink for'
    fields)
-   *Administration* → *General* → *Other* ('Refresh unsupported items'
    field)
-   parameters of the **zabbix\[queue,<from>,<to>\]**
    [internal item](/fr/manual/config/items/itemtypes/internal)
-   last parameter of [aggregate
    checks](/fr/manual/config/items/itemtypes/aggregate)

[comment]: # ({/new-163b4f50})

[comment]: # ({new-11368220})
#### Memory suffixes

Memory size suffixes are supported in trigger
[expression](/manual/config/triggers/expression) constants and function
parameters.

For memory size you can use:

-   **K** - kilobyte
-   **M** - megabyte
-   **G** - gigabyte
-   **T** - terabyte

[comment]: # ({/new-11368220})

[comment]: # ({new-c9f73444})
#### Other uses

Unit symbols are also used for a human-readable representation of data
in the frontend.

In both Zabbix server and frontend these symbols are supported:

-   **K** - kilo
-   **M** - mega
-   **G** - giga
-   **T** - tera

When item values in B, Bps are displayed in the frontend, base 2 is
applied (1K = 1024). Otherwise a base of 10 is used (1K = 1000).

Additionally the frontend also supports the display of:

-   **P** - peta
-   **E** - exa
-   **Z** - zetta
-   **Y** - yotta

[comment]: # ({/new-c9f73444})

[comment]: # ({new-46f2ffdf})
#### Usage examples

By using some appropriate suffixes you can write trigger expressions
that are easier to understand and maintain, for example these
expressions:

    {host:zabbix[proxy,zabbix_proxy,lastaccess]}>120
    {host:system.uptime[].last()}<86400
    {host:system.cpu.load.avg(600)}<10
    {host:vm.memory.size[available].last()}<20971520

could be changed to:

    {host:zabbix[proxy,zabbix_proxy,lastaccess]}>2m
    {host:system.uptime.last()}<1d
    {host:system.cpu.load.avg(10m)}<10
    {host:vm.memory.size[available].last()}<20M

[comment]: # ({/new-46f2ffdf})
