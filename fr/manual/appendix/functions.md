[comment]: # translation:outdated

[comment]: # ({new-1c860945})
# 6 Supported functions

Click on the respective function group to see more details.

|Function group|Functions|
|--------------|---------|
|[Aggregate functions](/manual/appendix/functions/aggregate)|avg, bucket\_percentile, count, histogram\_quantile, item\_count, kurtosis, mad, max, min, skewness, stddevpop, stddevsamp, sum, sumofsquares, varpop, varsamp|
|[Bitwise functions](/manual/appendix/functions/bitwise)|bitand, bitlshift, bitnot, bitor, bitrshift, bitxor|
|[Date and time functions](/manual/appendix/functions/time)|date, dayofmonth, dayofweek, now, time|
|[History functions](/manual/appendix/functions/history)|change, changecount, count, countunique, first, fuzzytime, last, logeventid, logseverity, logsource, monodec, monoinc, nodata, percentile, rate, trendavg, trendcount, trendmax, trendmin, trendstl, trendsum|
|[Mathematical functions](/manual/appendix/functions/math)|abs, acos, asin, atan, atan2, avg, cbrt, ceil, cos, cosh, cot, degrees, e, exp, expm1, floor, log, log10, max, min, mod, pi, power, radians, rand, round, signum, sin, sinh, sqrt, sum, tan, truncate|
|[Operator functions](/manual/appendix/functions/operator)|between, in|
|[Prediction functions](/manual/appendix/functions/prediction)|forecast, timeleft|
|[String functions](/manual/appendix/functions/string)|ascii, bitlength, bytelength, char, concat, find, insert, left, length, ltrim, mid, repeat, replace, right, rtrim, trim|

These functions are supported in [trigger
expressions](/manual/config/triggers/expression) and [calculated
items](/manual/config/items/itemtypes/calculated).

[comment]: # ({/new-1c860945})
