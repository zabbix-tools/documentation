<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="fr" datatype="plaintext" original="manual/appendix/config/zabbix_agentd_win.md">
    <body>
      <trans-unit id="a36cb226" xml:space="preserve">
        <source># 5 Zabbix agent (Windows)</source>
      </trans-unit>
      <trans-unit id="c0c75d19" xml:space="preserve">
        <source>### Overview

The parameters supported by the Windows Zabbix agent configuration file (zabbix\_agent.conf) are listed in this section. 

The parameters are listed without additional information. Click on the parameter to see the full details.

|Parameter|Description|
|--|--------|
|[Alias](#alias)|Sets an alias for an item key.|
|[AllowKey](#allowkey)|Allow the execution of those item keys that match a pattern.|
|[BufferSend](#buffersend)|Do not keep data longer than N seconds in buffer.|
|[BufferSize](#buffersize)|The maximum number of values in the memory buffer.|
|[DebugLevel](#debuglevel)|The debug level.|
|[DenyKey](#denykey)|Deny the execution of those item keys that match a pattern.|
|[EnableRemoteCommands](#enableremotecommands)|Whether remote commands from Zabbix server are allowed.|
|[HeartbeatFrequency](#heartbeatfrequency)|The frequency of heartbeat messages in seconds.|
|[HostInterface](#hostinterface)|An optional parameter that defines the host interface.|
|[HostInterfaceItem](#hostinterfaceitem)|An optional parameter that defines an item used for getting the host interface.|
|[HostMetadata](#hostmetadata)|An optional parameter that defines the host metadata.|
|[HostMetadataItem](#hostmetadataitem)|An optional parameter that defines a Zabbix agent item used for getting the host metadata.|
|[Hostname](#hostname)|An optional parameter that defines the hostname.|
|[HostnameItem](#hostnameitem)|An optional parameter that defines a Zabbix agent item used for getting the hostname.|
|[Include](#include)|You may include individual files or all files in a directory in the configuration file.|
|[ListenBacklog](#listenbacklog)|The maximum number of pending connections in the TCP queue.|
|[ListenIP](#listenip)|A list of comma-delimited IP addresses that the agent should listen on.|
|[ListenPort](#listenport)|The agent will listen on this port for connections from the server.|
|[LogFile](#logfile)|The name of the log file.|
|[LogFileSize](#logfilesize)|The maximum size of the log file.|
|[LogRemoteCommands](#logremotecommands)|Enable logging of executed shell commands as warnings.|
|[LogType](#logtype)|The type of the log output.|
|[MaxLinesPerSecond](#maxlinespersecond)|The maximum number of new lines the agent will send per second to Zabbix server or proxy when processing 'log' and 'logrt' active checks.|
|[PerfCounter](#perfcounter)|Defines a new parameter &lt;parameter\_name&gt; which is the average value for system performance counter &lt;perf\_counter\_path&gt; for the specified time period &lt;period&gt; (in seconds).|
|[PerfCounterEn](#perfcounteren)|Defines a new parameter &lt;parameter\_name&gt; which is the average value for system performance counter &lt;perf\_counter\_path&gt; for the specified time period &lt;period&gt; (in seconds). Compared to PerfCounter, the perfcounter paths must be in English.|
|[RefreshActiveChecks](#refreshactivechecks)|How often the list of active checks is refreshed.|
|[Server](#server)|A list of comma-delimited IP addresses, optionally in CIDR notation, or DNS names of Zabbix servers and Zabbix proxies.|
|[ServerActive](#serveractive)|The Zabbix server/proxy address or cluster configuration to get active checks from.|
|[SourceIP](#sourceip)|The source IP address.|
|[StartAgents](#startagents)|The number of pre-forked instances of zabbix\_agentd that process passive checks.|
|[Timeout](#timeout)|Spend no more than Timeout seconds on processing.|
|[TLSAccept](#tlsaccept)|What incoming connections to accept.|
|[TLSCAFile](#tlscafile)|The full pathname of a file containing the top-level CA(s) certificates for peer certificate verification, used for encrypted communications between Zabbix components.|
|[TLSCertFile](#tlscertfile)|The full pathname of a file containing the agent certificate or certificate chain, used for encrypted communications between Zabbix components.|
|[TLSConnect](#tlsconnect)|How the agent should connect to Zabbix server or proxy.|
|[TLSCRLFile](#tlscrlfile)|The full pathname of a file containing revoked certificates. This parameter is used for encrypted communications between Zabbix components.|
|[TLSKeyFile](#tlskeyfile)|The full pathname of a file containing the agent private key, used for encrypted communications between Zabbix components.|
|[TLSPSKFile](#tlspskfile)|The full pathname of a file containing the agent pre-shared key, used for encrypted communications with Zabbix server.|
|[TLSPSKIdentity](#tlspskidentity)|The pre-shared key identity string, used for encrypted communications with Zabbix server.|
|[TLSServerCertIssuer](#tlsservercertissuer)|The allowed server (proxy) certificate issuer.|
|[TLSServerCertSubject](#tlsservercertsubject)|The allowed server (proxy) certificate subject.|
|[UnsafeUserParameters](#unsafeuserparameters)|Allow all characters to be passed in arguments to user-defined parameters.|
|[UserParameter](#userparameter)|A user-defined parameter to monitor.|
|[UserParameterDir](#userparameterdir)|The default search path for UserParameter commands.|

All parameters are non-mandatory unless explicitly stated that the parameter is mandatory. 

Note that:

-   The default values reflect daemon defaults, not the values in the
    shipped configuration files;
-   Zabbix supports configuration files only in UTF-8 encoding without
    [BOM](https://en.wikipedia.org/wiki/Byte_order_mark);
-   Comments starting with "\#" are only supported in the beginning of
    the line.</source>
      </trans-unit>
      <trans-unit id="d041e5fe" xml:space="preserve">
        <source>### Parameter details</source>
      </trans-unit>
      <trans-unit id="dbac533e" xml:space="preserve">
        <source>##### Alias

Sets an alias for an item key. It can be used to substitute a long and complex item key with a shorter and simpler one. Multiple *Alias* parameters with the same *Alias* key may be present.&lt;br&gt;Different *Alias* keys may reference the same item key.&lt;br&gt;Aliases can be used in *HostMetadataItem* but not in *HostnameItem* or *PerfCounter* parameters.

Example 1: Retrieving the paging file usage in percentage from the server.

    Alias=pg_usage:perf_counter[\Paging File(_Total)\% Usage]
    
Now the shorthand key **pg_usage** may be used to retrieve data.

Example 2: Getting the CPU load with default and custom parameters.

    Alias=cpu.load:system.cpu.load
    Alias=cpu.load[*]:system.cpu.load[*]

This allows use **cpu.load** key to get the CPU load with default parameters as well as use **cpu.load[percpu,avg15]** to get specific data about the CPU load.

Example 3: Running multiple [low-level discovery](/manual/discovery/low_level_discovery) rules processing the same discovery items.

    Alias=vfs.fs.discovery[*]:vfs.fs.discovery

Now it is possible to set up several discovery rules using **vfs.fs.discovery** with different parameters for each rule, e.g., **vfs.fs.discovery[foo]**, **vfs.fs.discovery[bar]**, etc.</source>
      </trans-unit>
      <trans-unit id="049a5192" xml:space="preserve">
        <source>##### AllowKey

Allow the execution of those item keys that match a pattern. The key pattern is a wildcard expression that supports the "\*" character to match any number of any characters.&lt;br&gt;Multiple key matching rules may be defined in combination with DenyKey. The parameters are processed one by one according to their appearance order. See also: [Restricting agent checks](/manual/config/items/restrict_checks).</source>
      </trans-unit>
      <trans-unit id="9341dc8c" xml:space="preserve">
        <source>##### BufferSend

Do not keep data longer than N seconds in buffer.

Default: `5`&lt;br&gt;
Range: 1-3600</source>
      </trans-unit>
      <trans-unit id="11405d19" xml:space="preserve">
        <source>##### BufferSize

The maximum number of values in the memory buffer. The agent will send all collected data to the Zabbix server or proxy if the buffer is full.

Default: `100`&lt;br&gt;
Range: 2-65535</source>
      </trans-unit>
      <trans-unit id="b292af40" xml:space="preserve">
        <source>##### DebugLevel

Specify the debug level:&lt;br&gt;*0* - basic information about starting and stopping of Zabbix processes&lt;br&gt;*1* - critical information;&lt;br&gt;*2* - error information;&lt;br&gt;*3* - warnings;&lt;br&gt;*4* - for debugging (produces lots of information);&lt;br&gt;*5* - extended debugging (produces even more information).

Default: `3`&lt;br&gt;
Range: 0-5</source>
      </trans-unit>
      <trans-unit id="9a278481" xml:space="preserve">
        <source>##### DenyKey

Deny the execution of those item keys that match a pattern. The key pattern is a wildcard expression that supports the "\*" character to match any number of any characters.&lt;br&gt;Multiple key matching rules may be defined in combination with AllowKey. The parameters are processed one by one according to their appearance order. See also: [Restricting agent checks](/manual/config/items/restrict_checks).</source>
      </trans-unit>
      <trans-unit id="acca1798" xml:space="preserve">
        <source>##### EnableRemoteCommands

Whether remote commands from Zabbix server are allowed. This parameter is **deprecated**, use AllowKey=system.run\[\*\] or DenyKey=system.run\[\*\] instead.&lt;br&gt;It is an internal alias for AllowKey/DenyKey parameters depending on value:&lt;br&gt;0 - DenyKey=system.run\[\*\]&lt;br&gt;1 - AllowKey=system.run\[\*\]

Default: `0`&lt;br&gt;
Values: 0 - do not allow, 1 - allow</source>
      </trans-unit>
      <trans-unit id="672f4f6e" xml:space="preserve">
        <source>##### HeartbeatFrequency

The frequency of heartbeat messages in seconds. Used for monitoring the availability of active checks.&lt;br&gt;0 - heartbeat messages disabled.

Default: `60`&lt;br&gt;
Range: 0-3600</source>
      </trans-unit>
      <trans-unit id="99752970" xml:space="preserve">
        <source>##### HostInterface

An optional parameter that defines the host interface. The host interface is used at host [autoregistration](/manual/discovery/auto_registration#using_dns_as_default_interface) process. If not defined, the value will be acquired from HostInterfaceItem.&lt;br&gt;The agent will issue an error and not start if the value is over the limit of 255 characters.

Range: 0-255 characters</source>
      </trans-unit>
      <trans-unit id="8ac8b769" xml:space="preserve">
        <source>##### HostInterfaceItem

An optional parameter that defines an item used for getting the host interface.&lt;br&gt;Host interface is used at host [autoregistration](/manual/discovery/auto_registration#using_dns_as_default_interface) process.&lt;br&gt;During an autoregistration request the agent will log a warning message if the value returned by the specified item is over the limit of 255 characters.&lt;br&gt;The system.run\[\] item is supported regardless of AllowKey/DenyKey values.&lt;br&gt;This option is only used when HostInterface is not defined.</source>
      </trans-unit>
      <trans-unit id="098e9a8d" xml:space="preserve">
        <source>##### HostMetadata

An optional parameter that defines host metadata. Host metadata is used only at host autoregistration process (active agent). If not defined, the value will be acquired from HostMetadataItem.&lt;br&gt;The agent will issue an error and not start if the specified value is over the limit of 2034 bytes or a non-UTF-8 string.

Range: 0-2034 bytes</source>
      </trans-unit>
      <trans-unit id="9a501923" xml:space="preserve">
        <source>##### HostMetadataItem

An optional parameter that defines a Zabbix agent item used for getting host metadata. This option is only used when HostMetadata is not defined. User parameters, performance counters and aliases are supported. The system.run\[\] item is supported regardless of AllowKey/DenyKey values.&lt;br&gt;The HostMetadataItem value is retrieved on each autoregistration attempt and is used only at host autoregistration process (active agent).&lt;br&gt;During an autoregistration request the agent will log a warning message if the value returned by the specified item is over the limit of 65535 UTF-8 code points. The value returned by the item must be a UTF-8 string otherwise it will be ignored.</source>
      </trans-unit>
      <trans-unit id="7ba40433" xml:space="preserve">
        <source>##### Hostname

A list of comma-delimited, unique, case-sensitive hostnames. Required for active checks and must match hostnames as configured on the server. The value is acquired from HostnameItem if undefined.&lt;br&gt;Allowed characters: alphanumeric, '.', ' ', '\_' and '-'. Maximum length: 128 characters per hostname, 2048 characters for the entire line.

Default: Set by HostnameItem</source>
      </trans-unit>
      <trans-unit id="3d732bce" xml:space="preserve">
        <source>##### HostnameItem

An optional parameter that defines a Zabbix agent item used for getting the host name. This option is only used when Hostname is not defined. User parameters, performance counters or aliases are not supported, but the system.run\[\] item is supported regardless of AllowKey/DenyKey values.&lt;br&gt;The output length is limited to 512KB.&lt;br&gt;See also a [more detailed description](/manual/appendix/install/windows_agent#configuration).

Default: `system.hostname`</source>
      </trans-unit>
      <trans-unit id="4f66fa26" xml:space="preserve">
        <source>##### Include

You may include individual files or all files in a directory in the configuration file. To only include relevant files in the specified directory, the asterisk wildcard character is supported for pattern matching.&lt;br&gt;See [special notes](special_notes_include) about limitations.

Example:

    Include=C:\Program Files\Zabbix Agent\zabbix_agentd.d\*.conf</source>
      </trans-unit>
      <trans-unit id="f18a2cd5" xml:space="preserve">
        <source>##### ListenBacklog

The maximum number of pending connections in the TCP queue.&lt;br&gt;The default value is a hard-coded constant, which depends on the system.&lt;br&gt;The maximum supported value depends on the system, too high values may be silently truncated to the 'implementation-specified maximum'.

Default: `SOMAXCONN`&lt;br&gt;
Range: 0 - INT\_MAX</source>
      </trans-unit>
      <trans-unit id="fa6fe20d" xml:space="preserve">
        <source>##### ListenIP

A list of comma-delimited IP addresses that the agent should listen on.

Default: `0.0.0.0`</source>
      </trans-unit>
      <trans-unit id="04498be8" xml:space="preserve">
        <source>##### ListenPort

The agent will listen on this port for connections from the server.

Default: `10050`&lt;br&gt;
Range: 1024-32767</source>
      </trans-unit>
      <trans-unit id="7775e414" xml:space="preserve">
        <source>##### LogFile

The name of the agent log file.

Default: `C:\\zabbix_agentd.log`&lt;br&gt;
Mandatory: Yes, if LogType is set to *file*; otherwise no</source>
      </trans-unit>
      <trans-unit id="9135a275" xml:space="preserve">
        <source>##### LogFileSize

The maximum size of a log file in MB.&lt;br&gt;0 - disable automatic log rotation.&lt;br&gt;*Note*: If the log file size limit is reached and file rotation fails, for whatever reason, the existing log file is truncated and started anew.

Default: `1`&lt;br&gt;
Range: 0-1024</source>
      </trans-unit>
      <trans-unit id="c654859c" xml:space="preserve">
        <source>##### LogRemoteCommands

Enable the logging of the executed shell commands as warnings. Commands will be logged only if executed remotely. Log entries will not be created if system.run\[\] is launched locally by HostMetadataItem, HostInterfaceItem or HostnameItem parameters.

Default: `0`&lt;br&gt;
Values: 0 - disabled, 1 - enabled</source>
      </trans-unit>
      <trans-unit id="ed73f4f7" xml:space="preserve">
        <source>##### LogType

The type of the log output:&lt;br&gt;*file* - write log to the file specified by LogFile parameter;&lt;br&gt;*system* - write log to Windows Event Log;&lt;br&gt;*console* - write log to standard output.

Default: `file`</source>
      </trans-unit>
      <trans-unit id="6eaa719f" xml:space="preserve">
        <source>##### MaxLinesPerSecond

The maximum number of new lines the agent will send per second to Zabbix server or proxy when processing 'log', 'logrt', and 'eventlog' active checks. The provided value will be overridden by the 'maxlines' parameter, provided in the 'log', 'logrt', or 'eventlog' item key.&lt;br&gt;*Note*: Zabbix will process 10 times more new lines than set in *MaxLinesPerSecond* to seek the required string in log items.

Default: `20`&lt;br&gt;
Range: 1-1000</source>
      </trans-unit>
      <trans-unit id="fb950f2f" xml:space="preserve">
        <source>
##### PerfCounter

Defines a new parameter &lt;parameter\_name&gt; which is the average value for system performance counter &lt;perf\_counter\_path&gt; for the specified time period &lt;period&gt; (in seconds).&lt;br&gt;Syntax: &lt;parameter\_name&gt;,"&lt;perf\_counter\_path&gt;",&lt;period&gt;

For example, if you wish to receive the average number of processor interrupts per second for the last minute, you can define a new parameter "interrupts" as the following:&lt;br&gt;

    PerfCounter = interrupts,"\Processor(0)\Interrupts/sec",60

Please note the double quotes around the performance counter path. The parameter name (interrupts) is to be used as the item key when creating an item. Samples for calculating the average value will be taken every second.&lt;br&gt;You may run "typeperf -qx" to get the list of all performance counters available in Windows.</source>
      </trans-unit>
      <trans-unit id="0f61ea9b" xml:space="preserve">
        <source>
##### PerfCounterEn

Defines a new parameter &lt;parameter\_name&gt; which is the average value for system performance counter &lt;perf\_counter\_path&gt; for the specified time period &lt;period&gt; (in seconds). Compared to PerfCounter, the perfcounter paths must be in English. Supported only on **Windows Server 2008/Vista** and later.&lt;br&gt;Syntax: &lt;parameter\_name&gt;,"&lt;perf\_counter\_path&gt;",&lt;period&gt;

For example, if you wish to receive the average number of processor interrupts per second for the last minute, you can define a new parameter "interrupts" as the following:&lt;br&gt;

    PerfCounterEn = interrupts,"\Processor(0)\Interrupts/sec",60

Please note the double quotes around the performance counter path. The parameter name (interrupts) is to be used as the item key when creating an item. Samples for calculating the average value will be taken every second.&lt;br&gt;You can find the list of English strings by viewing the following registry key: `HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Perflib\009`.</source>
      </trans-unit>
      <trans-unit id="eba563a4" xml:space="preserve">
        <source>##### RefreshActiveChecks

How often the list of active checks is refreshed, in seconds. Note that after failing to refresh active checks the next refresh will be attempted in 60 seconds.

Default: `5`&lt;br&gt;
Range: 1-86400</source>
      </trans-unit>
      <trans-unit id="f2ed869a" xml:space="preserve">
        <source>##### Server

A list of comma-delimited IP addresses, optionally in CIDR notation, or DNS names of Zabbix servers or Zabbix proxies. Incoming connections will be accepted only from the hosts listed here. If IPv6 support is enabled then '127.0.0.1', '::127.0.0.1', '::ffff:127.0.0.1' are treated equally and '::/0' will allow any IPv4 or IPv6 address. '0.0.0.0/0' can be used to allow any IPv4 address. Note, that "IPv4-compatible IPv6 addresses" (0000::/96 prefix) are supported but deprecated by [RFC4291](https://tools.ietf.org/html/rfc4291#section-2.5.5). Spaces are allowed.

Example: 

    Server=127.0.0.1,192.168.1.0/24,::1,2001:db8::/32,zabbix.example.com

Mandatory: yes, if StartAgents is not explicitly set to 0</source>
      </trans-unit>
      <trans-unit id="35ed1929" xml:space="preserve">
        <source>##### ServerActive

The Zabbix server/proxy address or cluster configuration to get active checks from. The server/proxy address is an IP address or DNS name and optional port separated by colon.&lt;br&gt;The cluster configuration is one or more server addresses separated by semicolon. Multiple Zabbix servers/clusters and Zabbix proxies can be specified, separated by comma. More than one Zabbix proxy should not be specified from each Zabbix server/cluster. If Zabbix proxy is specified then Zabbix server/cluster for that proxy should not be specified.&lt;br&gt;Multiple comma-delimited addresses can be provided to use several independent Zabbix servers in parallel. Spaces are allowed.&lt;br&gt;If the port is not specified, default port is used.&lt;br&gt;IPv6 addresses must be enclosed in square brackets if port for that host is specified. If port is not specified, square brackets for IPv6 addresses are optional.&lt;br&gt;If this parameter is not specified, active checks are disabled.

Example for Zabbix proxy: 

    ServerActive=127.0.0.1:10051

Example for multiple servers: 

    ServerActive=127.0.0.1:20051,zabbix.domain,[::1]:30051,::1,[12fc::1]

Example for high availability:

    ServerActive=zabbix.cluster.node1;zabbix.cluster.node2:20051;zabbix.cluster.node3

Example for high availability with two clusters and one server:

    ServerActive=zabbix.cluster.node1;zabbix.cluster.node2:20051,zabbix.cluster2.node1;zabbix.cluster2.node2,zabbix.domain

Range: (\*)</source>
      </trans-unit>
      <trans-unit id="10fc73c6" xml:space="preserve">
        <source>##### SourceIP

The source IP address for:&lt;br&gt;- outgoing connections to Zabbix server or Zabbix proxy;&lt;br&gt;- making connections while executing some items (web.page.get, net.tcp.port, etc.).</source>
      </trans-unit>
      <trans-unit id="d8dd10c4" xml:space="preserve">
        <source>##### StartAgents

The number of pre-forked instances of zabbix\_agentd that process passive checks. If set to 0, passive checks are disabled and the agent will not listen on any TCP port.

Default: `3`&lt;br&gt;
Range: 0-63 (\*)</source>
      </trans-unit>
      <trans-unit id="895d5e50" xml:space="preserve">
        <source>##### Timeout

Spend no more than Timeout seconds on processing.

Default: `3`&lt;br&gt;
Range: 1-30</source>
      </trans-unit>
      <trans-unit id="fb75760f" xml:space="preserve">
        <source>##### TLSAccept

The incoming connections to accept. Used for passive checks. Multiple values can be specified, separated by comma:&lt;br&gt;*unencrypted* - accept connections without encryption (default)&lt;br&gt;*psk* - accept connections with TLS and a pre-shared key (PSK)&lt;br&gt;*cert* - accept connections with TLS and a certificate

Mandatory: yes, if TLS certificate or PSK parameters are defined (even for *unencrypted* connection); otherwise no</source>
      </trans-unit>
      <trans-unit id="8301867c" xml:space="preserve">
        <source>##### TLSCAFile

The full pathname of the file containing the top-level CA(s) certificates for peer certificate verification, used for encrypted communications between Zabbix components.</source>
      </trans-unit>
      <trans-unit id="8da364a3" xml:space="preserve">
        <source>##### TLSCertFile

The full pathname of the file containing the agent certificate or certificate chain, used for encrypted communications with Zabbix components.</source>
      </trans-unit>
      <trans-unit id="3f5548eb" xml:space="preserve">
        <source>##### TLSConnect

How the agent should connect to Zabbix server or proxy. Used for active checks. Only one value can be specified:&lt;br&gt;*unencrypted* - connect without encryption (default)&lt;br&gt;*psk* - connect using TLS and a pre-shared key (PSK)&lt;br&gt;*cert* - connect using TLS and a certificate

Mandatory: yes, if TLS certificate or PSK parameters are defined (even for *unencrypted* connection); otherwise no</source>
      </trans-unit>
      <trans-unit id="95b41620" xml:space="preserve">
        <source>##### TLSCRLFile

The full pathname of the file containing revoked certificates. This parameter is used for encrypted communications between Zabbix components.</source>
      </trans-unit>
      <trans-unit id="89503f46" xml:space="preserve">
        <source>##### TLSKeyFile

The full pathname of the file containing the agent private key, used for encrypted communications between Zabbix components.</source>
      </trans-unit>
      <trans-unit id="2936868d" xml:space="preserve">
        <source>##### TLSPSKFile

The full pathname of the file containing the agent pre-shared key, used for encrypted communications with Zabbix server.</source>
      </trans-unit>
      <trans-unit id="262fa198" xml:space="preserve">
        <source>##### TLSPSKIdentity

The pre-shared key identity string, used for encrypted communications with Zabbix server.</source>
      </trans-unit>
      <trans-unit id="9eef27fc" xml:space="preserve">
        <source>##### TLSServerCertIssuer

The allowed server (proxy) certificate issuer.</source>
      </trans-unit>
      <trans-unit id="abaebd84" xml:space="preserve">
        <source>##### TLSServerCertSubject

The allowed server (proxy) certificate subject.</source>
      </trans-unit>
      <trans-unit id="a721fb91" xml:space="preserve">
        <source>##### UnsafeUserParameters

Allow all characters to be passed in arguments to user-defined parameters. The following characters are not allowed: \\ ' " \` \* ? \[ \] { } \~ $ ! &amp; ; ( ) &lt; &gt; \| \# @&lt;br&gt;Additionally, newline characters are not allowed.

Default: `0`&lt;br&gt;
Values: 0 - do not allow, 1 - allow</source>
      </trans-unit>
      <trans-unit id="8f64ff97" xml:space="preserve">
        <source>##### UserParameter

A user-defined parameter to monitor. There can be several user-defined parameters.&lt;br&gt;Format: UserParameter=&lt;key&gt;,&lt;shell command&gt;&lt;br&gt;Note that the shell command must not return empty string or EOL only. Shell commands may have relative paths, if the UserParameterDir parameter is specified.

Example:

    UserParameter=system.test,who|wc -l
    UserParameter=check_cpu,./custom_script.sh</source>
      </trans-unit>
      <trans-unit id="9e2d0466" xml:space="preserve">
        <source>##### UserParameterDir

The default search path for UserParameter commands. If used, the agent will change its working directory to the one specified here before executing a command. Thereby, UserParameter commands can have a relative `./` prefix instead of a full path. Only one entry is allowed.

Example:

    UserParameterDir=/opt/myscripts</source>
      </trans-unit>
      <trans-unit id="7142df61" xml:space="preserve">
        <source>
::: noteclassic
 (\*) The number of active servers listed in ServerActive
plus the number of pre-forked instances for passive checks specified in
StartAgents must be less than 64.
:::</source>
      </trans-unit>
      <trans-unit id="179d1dff" xml:space="preserve">
        <source>### See also

1.  [Differences in the Zabbix agent configuration for active and
    passive checks starting from version
    2.0.0](http://blog.zabbix.com/multiple-servers-for-active-agent-sure/858).</source>
      </trans-unit>
    </body>
  </file>
</xliff>
