[comment]: # attributes: notoc

[comment]: # translation:outdated

[comment]: # ({new-452d5bc5})
# 2 Zabbix proxy

::: noteclassic
The default values reflect daemon defaults, not the values
in the shipped configuration files.
:::

The parameters supported in a Zabbix proxy configuration file:

|Parameter|Mandatory|Range|Default|Description|
|---------|---------|-----|-------|-----------|
|AllowRoot|no|<|0|Allow the proxy to run as 'root'. If disabled and the proxy is started by 'root', the proxy will try to switch to the 'zabbix' user instead. Has no effect if started under a regular user.<br>0 - do not allow<br>1 - allow<br>This parameter is supported since Zabbix 2.2.0.|
|CacheSize|no|128K-8G|8M|Size of configuration cache, in bytes.<br>Shared memory size, for storing host and item data.<br>Upper limit used to be 2GB before Zabbix 2.2.3.|
|ConfigFrequency|no|1-604800|3600|How often proxy retrieves configuration data from Zabbix server in seconds.<br>Active proxy parameter. Ignored for passive proxies (see ProxyMode parameter).|
|DBHost|no|<|localhost|Database host name.<br>In case of MySQL localhost or empty string results in using a socket. In case of PostgreSQL<br>only empty string results in attempt to use socket.|
|DBName|yes|<|<|Database name or path to database file for SQLite3 (multi-process architecture of Zabbix does not allow to use [in-memory database](https://www.sqlite.org/inmemorydb.html), e.g. `:memory:`, `file::memory:?cache=shared` or `file:memdb1?mode=memory&cache=shared`).<br><br>Warning: Do not attempt to use the same database Zabbix server is using.|
|DBPassword|no|<|<|Database password. Ignored for SQLite.<br>Comment this line if no password is used.|
|DBSchema|no|<|<|Schema name. Used for IBM DB2 and PostgreSQL.|
|DBSocket|no|<|3306|Path to MySQL socket.<br>Database port when not using local socket. Ignored for SQLite.|
|DBUser|<|<|<|Database user. Ignored for SQLite.|
|DataSenderFrequency|no|1-3600|1|Proxy will send collected data to the server every N seconds.<br>Active proxy parameter. Ignored for passive proxies (see ProxyMode parameter).|
|DebugLevel|no|0-5|3|Specifies debug level:<br>0 - basic information about starting and stopping of Zabbix processes<br>1 - critical information<br>2 - error information<br>3 - warnings<br>4 - for debugging (produces lots of information)<br>5 - extended debugging (produces even more information)|
|EnableRemoteCommands|no|<|0|Whether remote commands from Zabbix server are allowed.<br>0 - not allowed<br>1 - allowed<br>This parameter is supported since Zabbix 3.4.0.|
|ExternalScripts|no|<|/usr/local/share/zabbix/externalscripts|Location of external scripts (depends on compile-time installation variable *datadir*).|
|Fping6Location|no|<|/usr/sbin/fping6|Location of fping6.<br>Make sure that fping6 binary has root ownership and SUID flag set.<br>Make empty ("Fping6Location=") if your fping utility is capable to process IPv6 addresses.|
|FpingLocation|no|<|/usr/sbin/fping|Location of fping.<br>Make sure that fping binary has root ownership and SUID flag set!|
|HeartbeatFrequency|no|0-3600|60|Frequency of heartbeat messages in seconds.<br>Used for monitoring availability of proxy on server side.<br>0 - heartbeat messages disabled.<br>Active proxy parameter. Ignored for passive proxies (see ProxyMode parameter).|
|HistoryCacheSize|no|128K-2G|16M|Size of history cache, in bytes.<br>Shared memory size for storing history data.|
|HistoryIndexCacheSize|no|128K-2G|4M|Size of history index cache, in bytes.<br>Shared memory size for indexing history data stored in history cache.<br>The index cache size needs roughly 100 bytes to cache one item.<br>This parameter is supported since Zabbix 3.0.0.|
|Hostname|no|<|Set by HostnameItem|Unique, case sensitive Proxy name. Make sure the proxy name is known to the server!<br>Allowed characters: alphanumeric, '.', ' ', '\_' and '-'.<br>Maximum length: 64|
|HostnameItem|no|<|system.hostname|Item used for setting Hostname if it is undefined (this will be run on the proxy similarly as on an agent).<br>Does not support UserParameters, performance counters or aliases, but does support system.run\[\].<br><br>Ignored if Hostname is set.<br><br>This parameter is supported since Zabbix 1.8.6.|
|HousekeepingFrequency|no|0-24|1|How often Zabbix will perform housekeeping procedure (in hours).<br>Housekeeping is removing outdated information from the database.<br>*Note*: To prevent housekeeper from being overloaded (for example, when configuration parameters ProxyLocalBuffer or ProxyOfflineBuffer are greatly reduced), no more than 4 times HousekeepingFrequency hours of outdated information are deleted in one housekeeping cycle. Thus, if HousekeepingFrequency is 1, no more than 4 hours of outdated information (starting from the oldest entry) will be deleted per cycle.<br>*Note*: To lower load on proxy startup housekeeping is postponed for 30 minutes after proxy start. Thus, if HousekeepingFrequency is 1, the very first housekeeping procedure after proxy start will run after 30 minutes, and will repeat every hour thereafter. This postponing behavior is in place since Zabbix 2.4.0.<br>Since Zabbix 3.0.0 it is possible to disable automatic housekeeping by setting HousekeepingFrequency to 0. In this case the housekeeping procedure can only be started by *housekeeper\_execute* runtime control option and the period of outdated information deleted in one housekeeping cycle is 4 times the period since the last housekeeping cycle, but not less than 4 hours and not greater than 4 days.|
|Include|no|<|<|You may include individual files or all files in a directory in the configuration file.<br>To only include relevant files in the specified directory, the asterisk wildcard character is supported for pattern matching. For example: `/absolute/path/to/config/files/*.conf`. Pattern matching is supported since Zabbix 2.4.0.<br>See [special notes](special_notes_include) about limitations.|
|JavaGateway|no|<|<|IP address (or hostname) of Zabbix Java gateway.<br>Only required if Java pollers are started.<br>This parameter is supported since Zabbix 2.0.0.|
|JavaGatewayPort|no|1024-32767|10052|Port that Zabbix Java gateway listens on.<br>This parameter is supported since Zabbix 2.0.0.|
|ListenIP|no|<|0.0.0.0|List of comma delimited IP addresses that the trapper should listen on.<br>Trapper will listen on all network interfaces if this parameter is missing.<br>Multiple IP addresses are supported since Zabbix 1.8.3.|
|ListenPort|no|1024-32767|10051|Listen port for trapper.|
|LoadModule|no|<|<|Module to load at proxy startup. Modules are used to extend functionality of the proxy.<br>Format: LoadModule=<module.so><br>The modules must be located in directory specified by LoadModulePath.<br>It is allowed to include multiple LoadModule parameters.|
|LoadModulePath|no|<|<|Full path to location of proxy modules.<br>Default depends on compilation options.|
|LogFile|yes, if LogType is set to *file*, otherwise<br>no|<|<|Name of log file.|
|LogFileSize|no|0-1024|1|Maximum size of log file in MB.<br>0 - disable automatic log rotation.<br>*Note*: If the log file size limit is reached and file rotation fails, for whatever reason, the existing log file is truncated and started anew.|
|LogRemoteCommands|no|<|0|Enable logging of executed shell commands as warnings.<br>0 - disabled<br>1 - enabled<br>This parameter is supported since Zabbix 3.4.0.|
|LogType|no|<|file|Log output type:<br>*file* - write log to file specified by LogFile parameter,<br>*system* - write log to syslog,<br>*console* - write log to standard output.<br>This parameter is supported since Zabbix 3.0.0.|
|LogSlowQueries|no|0-3600000|0|How long a database query may take before being logged (in milliseconds).<br>0 - don't log slow queries.<br>This option becomes enabled starting with DebugLevel=3.<br>This parameter is supported since Zabbix 1.8.2.|
|PidFile|no|<|/tmp/zabbix\_proxy.pid|Name of PID file.|
|ProxyLocalBuffer|no|0-720|0|Proxy will keep data locally for N hours, even if the data have already been synced with the server.<br>This parameter may be used if local data will be used by third party applications.|
|ProxyMode|no|0-1|0|Proxy operating mode.<br>0 - proxy in the active mode<br>1 - proxy in the passive mode<br>This parameter is supported since Zabbix 1.8.3.<br>*Note* that (sensitive) proxy configuration data may become available to parties having access to the Zabbix server trapper port when using an active proxy. This is possible because anyone may pretend to be an active proxy and request configuration data; authentication does not take place.|
|ProxyOfflineBuffer|no|1-720|1|Proxy will keep data for N hours in case of no connectivity with Zabbix server.<br>Older data will be lost.|
|ServerPort|no|1024-32767|10051|Port of Zabbix trapper on Zabbix server.<br>Active proxy parameter. Ignored for passive proxies (see ProxyMode parameter).|
|Server|yes|<|<|If ProxyMode is set to *active mode*:<br>IP address or DNS name of Zabbix server to get configuration data from and send data to.<br><br>If ProxyMode is set to *passive mode*:<br>List of comma delimited IP addresses, optionally in CIDR notation, or DNS names of Zabbix server. Incoming connections will be accepted only from the addresses listed here. If IPv6 support is enabled then '127.0.0.1', '::127.0.0.1', '::ffff:127.0.0.1' are treated equally and '::/0' will allow any IPv4 or IPv6 address. '0.0.0.0/0' can be used to allow any IPv4 address.<br>*Example*: Server=127.0.0.1,192.168.1.0/24,::1,2001:db8::/32,zabbix.example.com|
|SNMPTrapperFile|no|<|/tmp/zabbix\_traps.tmp|Temporary file used for passing data from SNMP trap daemon to the proxy.<br>Must be the same as in zabbix\_trap\_receiver.pl or SNMPTT configuration file.<br>This parameter is supported since Zabbix 2.0.0.|
|SocketDir|no|<|/tmp|Directory to store IPC sockets used by internal Zabbix services.<br>This parameter is supported since Zabbix 3.4.0.|
|SourceIP|no|<|<|Source IP address for outgoing connections.|
|SSHKeyLocation|no|<|<|Location of public and private keys for SSH checks and actions|
|SSLCertLocation|no|<|<|Location of SSL client certificate files for client authentication.<br>This parameter is used in web monitoring only and is supported since Zabbix 2.4.0.|
|SSLKeyLocation|no|<|<|Location of SSL private key files for client authentication.<br>This parameter is used in web monitoring only and is supported since Zabbix 2.4.0.|
|SSLCALocation|no|<|<|Location of certificate authority (CA) files for SSL server certificate verification.<br>Note that the value of this parameter will be set as libcurl option CURLOPT\_CAPATH. For libcurl versions before 7.42.0, this only has effect if libcurl was compiled to use OpenSSL. For more information see [cURL web page](http://curl.haxx.se/libcurl/c/CURLOPT_CAPATH.html).<br>This parameter is used in web monitoring since Zabbix 2.4.0 and in SMTP authentication since Zabbix 3.0.0.|
|StartDBSyncers|no|1-100|4|Number of pre-forked instances of DB Syncers.<br>The upper limit used to be 64 before version 1.8.5.<br>This parameter is supported since Zabbix 1.8.3.|
|StartDiscoverers|no|0-250|1|Number of pre-forked instances of discoverers.<br>The upper limit used to be 255 before version 1.8.5.|
|StartHTTPPollers|no|0-1000|1|Number of pre-forked instances of HTTP pollers.|
|StartIPMIPollers|no|0-1000|0|Number of pre-forked instances of IPMI pollers.<br>The upper limit used to be 255 before version 1.8.5.|
|StartJavaPollers|no|0-1000|0|Number of pre-forked instances of Java pollers.<br>This parameter is supported since Zabbix 2.0.0.|
|StartPingers|no|0-1000|1|Number of pre-forked instances of ICMP pingers.<br>The upper limit used to be 255 before version 1.8.5.|
|StartPollersUnreachable|no|0-1000|1|Number of pre-forked instances of pollers for unreachable hosts (including IPMI and Java).<br>Since Zabbix 2.4.0, at least one poller for unreachable hosts must be running if regular, IPMI or Java pollers are started.<br>The upper limit used to be 255 before version 1.8.5.<br>This option is missing in version 1.8.3.|
|StartPollers|no|0-1000|5|Number of pre-forked instances of pollers.<br>The upper limit used to be 255 before version 1.8.5.|
|StartSNMPTrapper|no|0-1|0|If set to 1, SNMP trapper process will be started.<br>This parameter is supported since Zabbix 2.0.0.|
|StartTrappers|no|0-1000|5|Number of pre-forked instances of trappers.<br>Trappers accept incoming connections from Zabbix sender and active agents.<br>The upper limit used to be 255 before version 1.8.5.|
|StartVMwareCollectors|no|0-250|0|Number of pre-forked vmware collector instances.<br>This parameter is supported since Zabbix 2.2.0.|
|Timeout|no|1-30|3|Specifies how long we wait for agent, SNMP device or external check (in seconds).|
|TLSAccept|yes, if TLS certificate or PSK parameters are defined (even for *unencrypted* connection), otherwise no|<|<|What incoming connections to accept from Zabbix server. Used for a passive proxy, ignored on an active proxy. Multiple values can be specified, separated by comma:<br>*unencrypted* - accept connections without encryption (default)<br>*psk* - accept connections with TLS and a pre-shared key (PSK)<br>*cert* - accept connections with TLS and a certificate<br>This parameter is supported since Zabbix 3.0.0.|
|TLSCAFile|no|<|<|Full pathname of a file containing the top-level CA(s) certificates for peer certificate verification, used for encrypted communications between Zabbix components.<br>This parameter is supported since Zabbix 3.0.0.|
|TLSCertFile|no|<|<|Full pathname of a file containing the proxy certificate or certificate chain, used for encrypted communications between Zabbix components.<br>This parameter is supported since Zabbix 3.0.0.|
|TLSConnect|yes, if TLS certificate or PSK parameters are defined (even for *unencrypted* connection), otherwise no|<|<|How the proxy should connect to Zabbix server. Used for an active proxy, ignored on a passive proxy. Only one value can be specified:<br>*unencrypted* - connect without encryption (default)<br>*psk* - connect using TLS and a pre-shared key (PSK)<br>*cert* - connect using TLS and a certificate<br>This parameter is supported since Zabbix 3.0.0.|
|TLSCRLFile|no|<|<|Full pathname of a file containing revoked certificates.This parameter is used for encrypted communications between Zabbix components.<br>This parameter is supported since Zabbix 3.0.0.|
|TLSKeyFile|no|<|<|Full pathname of a file containing the proxy private key, used for encrypted communications between Zabbix components.<br>This parameter is supported since Zabbix 3.0.0.|
|TLSPSKFile|no|<|<|Full pathname of a file containing the proxy pre-shared key. used for encrypted communications with Zabbix server.<br>This parameter is supported since Zabbix 3.0.0.|
|TLSPSKIdentity|no|<|<|Pre-shared key identity string, used for encrypted communications with Zabbix server.<br>This parameter is supported since Zabbix 3.0.0.|
|TLSServerCertIssuer|no|<|<|Allowed server certificate issuer.<br>This parameter is supported since Zabbix 3.0.0.|
|TLSServerCertSubject|no|<|<|Allowed server certificate subject.<br>This parameter is supported since Zabbix 3.0.0.|
|TmpDir|no|<|/tmp|Temporary directory.|
|TrapperTimeout|no|1-300|300|Specifies how many seconds trapper may spend processing new data.|
|User|no|<|zabbix|Drop privileges to a specific, existing user on the system.<br>Only has effect if run as 'root' and AllowRoot is disabled.<br>This parameter is supported since Zabbix 2.4.0.|
|UnavailableDelay|no|1-3600|60|How often host is checked for availability during the [unavailability](/fr/manual/appendix/items/unreachability#unavailable_host) period, in seconds.|
|UnreachableDelay|no|1-3600|15|How often host is checked for availability during the [unreachability](/fr/manual/appendix/items/unreachability#unreachable_host) period, in seconds.|
|UnreachablePeriod|no|1-3600|45|After how many seconds of [unreachability](/fr/manual/appendix/items/unreachability#unreachable_host) treat a host as unavailable.|
|VMwareCacheSize|no|256K-2G|8M|Shared memory size for storing VMware data.<br>A VMware internal check zabbix\[vmware,buffer,...\] can be used to monitor the VMware cache usage (see [Internal checks](/manual/config/items/itemtypes/internal)).<br>Note that shared memory is not allocated if there are no vmware collector instances configured to start.<br>This parameter is supported since Zabbix 2.2.0.|
|VMwareFrequency|no|10-86400|60|Delay in seconds between data gathering from a single VMware service.<br>This delay should be set to the least update interval of any VMware monitoring item.<br>This parameter is supported since Zabbix 2.2.0.|
|VMwarePerfFrequency|no|10-86400|60|Delay in seconds between performance counter statistics retrieval from a single VMware service.<br>This delay should be set to the least update interval of any VMware monitoring [item](/manual/config/items/itemtypes/simple_checks/vmware_keys#footnotes) that uses VMware performance counters.<br>This parameter is supported since Zabbix 2.2.9, 2.4.4|
|VMwareTimeout|no|1-300|10|The maximum number of seconds vmware collector will wait for a response from VMware service (vCenter or ESX hypervisor).<br>This parameter is supported since Zabbix 2.2.9, 2.4.4|

::: noteclassic
Zabbix supports configuration files only in UTF-8 encoding
without [BOM](https://en.wikipedia.org/wiki/Byte_order_mark).\
\
Comments starting with "\#" are only supported in the beginning of the
line.
:::

[comment]: # ({/new-452d5bc5})

[comment]: # ({new-221978cc})
#### Overview

This section lists parameters supported in a Zabbix proxy configuration
file (zabbix\_proxy.conf). Note that:

-   The default values reflect daemon defaults, not the values in the
    shipped configuration files;
-   Zabbix supports configuration files only in UTF-8 encoding without
    [BOM](https://en.wikipedia.org/wiki/Byte_order_mark);
-   Comments starting with "\#" are only supported in the beginning of
    the line.

[comment]: # ({/new-221978cc})

[comment]: # ({new-7bf44544})
### Parameter details

[comment]: # ({/new-7bf44544})



[comment]: # ({new-fc4d81ba})
##### `AllowRoot`
Allow the proxy to run as 'root'. If disabled and the proxy is started by 'root', the proxy will try to switch to the 'zabbix' user instead. Has no effect if started under a regular user.

Default: `0` | Values: 0 - do not allow; 1 - allow

[comment]: # ({/new-fc4d81ba})

[comment]: # ({new-c26999ee})
##### `AllowUnsupportedDBVersions`
Allow the proxy to work with unsupported database versions.

Default: `0` | Values: 0 - do not allow; 1 - allow

[comment]: # ({/new-c26999ee})

[comment]: # ({new-317e46d4})
##### `CacheSize`
The size of the configuration cache, in bytes. The shared memory size for storing host and item data.

Default: `32M` | Range: 128K-64G

[comment]: # ({/new-317e46d4})

[comment]: # ({new-a1148208})
##### `ConfigFrequency`
This parameter is **deprecated** (use ProxyConfigFrequency instead).<br>How often the proxy retrieves configuration data from Zabbix server in seconds.<br>Active proxy parameter. Ignored for passive proxies (see ProxyMode parameter).

Default: `3600` | Range: 1-604800

[comment]: # ({/new-a1148208})

[comment]: # ({new-797b5d4b})
##### `DataSenderFrequency`
The proxy will send collected data to the server every N seconds. Note that an active proxy will still poll Zabbix server every second for remote command tasks.<br>Active proxy parameter. Ignored for passive proxies (see ProxyMode parameter).

Default: `1` | Range: 1-3600

[comment]: # ({/new-797b5d4b})

[comment]: # ({new-68d91db6})
##### `DBHost`
The database host name.<br>With MySQL `localhost` or empty string results in using a socket. With PostgreSQL only empty string results in attempt to use socket. With [Oracle](/manual/appendix/install/oracle#connection_set_up) empty string results in using the Net Service Name connection method; in this case consider using the TNS\_ADMIN environment variable to specify the directory of the tnsnames.ora file.

Default: `localhost`

[comment]: # ({/new-68d91db6})

[comment]: # ({new-a63deb51})
##### `DBName`
The database name or path to the database file for SQLite3 (the multi-process architecture of Zabbix does not allow to use [in-memory database](https://www.sqlite.org/inmemorydb.html), e.g. `:memory:`, `file::memory:?cache=shared` or `file:memdb1?mode=memory&cache=shared`).<br>*Warning*: Do not attempt to use the same database the Zabbix server is using.<br>With [Oracle](/manual/appendix/install/oracle#connection_set_up), if the Net Service Name connection method is used, specify the service name from tnsnames.ora or set to empty string; set the TWO\_TASK environment variable if DBName is set to empty string.

Mandatory: Yes

[comment]: # ({/new-a63deb51})

[comment]: # ({new-41ef1a35})
##### `DBPassword`
The database password. Comment this line if no password is used. Ignored for SQLite.

[comment]: # ({/new-41ef1a35})

[comment]: # ({new-4f65edaa})
##### `DBPort`
The database port when not using local socket. Ignored for SQLite.<br>With [Oracle](/manual/appendix/install/oracle#connection_set_up), if the Net Service Name connection method is used, this parameter will be ignored; the port number from the tnsnames.ora file will be used instead.

Range: 1024-65535

[comment]: # ({/new-4f65edaa})

[comment]: # ({new-432321c5})
##### `DBSchema`
The database schema name. Used for PostgreSQL.

[comment]: # ({/new-432321c5})

[comment]: # ({new-c1dbb854})
##### `DBSocket`
The path to the MySQL socket file.<br>The database port when not using local socket. Ignored for SQLite.

Default: `3306`

[comment]: # ({/new-c1dbb854})

[comment]: # ({new-c2cec654})
##### `DBUser`
The database user. Ignored for SQLite.

[comment]: # ({/new-c2cec654})

[comment]: # ({new-c6670742})
##### `DBTLSConnect`
Setting this option enforces to use TLS connection to the database:<br>*required* - connect using TLS<br>*verify\_ca* - connect using TLS and verify certificate<br>*verify\_full* - connect using TLS, verify certificate and verify that database identity specified by DBHost matches its certificate<br>On MySQL starting from 5.7.11 and PostgreSQL the following values are supported: "required", "verify", "verify\_full".<br>On MariaDB starting from version 10.2.6 "required" and "verify\_full" values are supported.<br>By default not set to any option and the behavior depends on database configuration.

[comment]: # ({/new-c6670742})

[comment]: # ({new-fd8ea2b2})
##### `DBTLSCAFile`
The full pathname of a file containing the top-level CA(s) certificates for database certificate verification.

Mandatory: no (yes, if DBTLSConnect set to *verify\_ca* or *verify\_full*)

[comment]: # ({/new-fd8ea2b2})

[comment]: # ({new-6ec7937a})
##### `DBTLSCertFile`
The full pathname of a file containing the Zabbix proxy certificate for authenticating to database.

[comment]: # ({/new-6ec7937a})

[comment]: # ({new-eda6649f})
##### `DBTLSKeyFile`
The full pathname of a file containing the private key for authenticating to the database.

[comment]: # ({/new-eda6649f})

[comment]: # ({new-150712b8})
##### `DBTLSCipher`
The list of encryption ciphers that Zabbix proxy permits for TLS protocols up through TLS v1.2. Supported only for MySQL.

[comment]: # ({/new-150712b8})

[comment]: # ({new-1594abd5})
##### `DBTLSCipher13`
The list of encryption ciphersuites that Zabbix proxy permits for the TLS v1.3 protocol. Supported only for MySQL, starting from version 8.0.16.

[comment]: # ({/new-1594abd5})

[comment]: # ({new-c65cefa5})
##### `DebugLevel`
Specify the debug level:<br>*0* - basic information about starting and stopping of Zabbix processes<br>*1* - critical information;<br>*2* - error information;<br>*3* - warnings;<br>*4* - for debugging (produces lots of information);<br>*5* - extended debugging (produces even more information).

Default: `3` | Range: 0-5

[comment]: # ({/new-c65cefa5})

[comment]: # ({new-404189de})
##### `EnableRemoteCommands`
Whether remote commands from Zabbix server are allowed.<br>0 - not allowed<br>1 - allowed

Default: `0`

[comment]: # ({/new-404189de})

[comment]: # ({new-62d00bff})
##### `ExternalScripts`
The location of external scripts (depends on the `datadir` compile-time installation variable).

Default: `/usr/local/share/zabbix/externalscripts`

[comment]: # ({/new-62d00bff})

[comment]: # ({new-c6a5104d})
##### `Fping6Location`
The location of fping6. Make sure that the fping6 binary has root ownership and the SUID flag set. Make empty ("Fping6Location=") if your fping utility is capable to process IPv6 addresses.

Default: `/usr/sbin/fping6`

[comment]: # ({/new-c6a5104d})

[comment]: # ({new-1be0920c})
##### `FpingLocation`
The location of fping. Make sure that the fping binary has root ownership and the SUID flag set.

Default: `/usr/sbin/fping`

[comment]: # ({/new-1be0920c})

[comment]: # ({new-b516dcd6})
##### `HistoryCacheSize`
The size of the history cache, in bytes. The shared memory size for storing history data.

Default: `16M` | Range: 128K-2G

[comment]: # ({/new-b516dcd6})

[comment]: # ({new-026e7a12})
##### `HistoryIndexCacheSize`
The size of the history index cache, in bytes. The shared memory size for indexing the history data stored in history cache. The index cache size needs roughly 100 bytes to cache one item.

Default: `4M` | Range: 128K-2G

[comment]: # ({/new-026e7a12})

[comment]: # ({new-6281710b})
##### `Hostname`
A unique, case sensitive proxy name. Make sure the proxy name is known to the server.<br>Allowed characters: alphanumeric, '.', ' ', '\_' and '-'.<br>Maximum length: 128

Default: Set by HostnameItem

[comment]: # ({/new-6281710b})

[comment]: # ({new-76df478d})
##### `HostnameItem`
The item used for setting Hostname if it is undefined (this will be run on the proxy similarly as on an agent). Ignored if Hostname is set.m<br>Does not support UserParameters, performance counters or aliases, but does support system.run\[\].

Default: system.hostname

[comment]: # ({/new-76df478d})

[comment]: # ({new-44c82860})
##### `HousekeepingFrequency`
How often Zabbix will perform housekeeping procedure (in hours). Housekeeping is removing outdated information from the database.<br>*Note*: To prevent housekeeper from being overloaded (for example, when configuration parameters ProxyLocalBuffer or ProxyOfflineBuffer are greatly reduced), no more than 4 times HousekeepingFrequency hours of outdated information are deleted in one housekeeping cycle. Thus, if HousekeepingFrequency is 1, no more than 4 hours of outdated information (starting from the oldest entry) will be deleted per cycle.<br>*Note*: To lower load on proxy startup housekeeping is postponed for 30 minutes after proxy start. Thus, if HousekeepingFrequency is 1, the very first housekeeping procedure after proxy start will run after 30 minutes, and will repeat every hour thereafter.<br>Since Zabbix 3.0.0 it is possible to disable automatic housekeeping by setting HousekeepingFrequency to 0. In this case the housekeeping procedure can only be started by *housekeeper\_execute* runtime control option and the period of outdated information deleted in one housekeeping cycle is 4 times the period since the last housekeeping cycle, but not less than 4 hours and not greater than 4 days.

Default: `1` | Range: 0-24

[comment]: # ({/new-44c82860})

[comment]: # ({new-b4d0a8d4})
##### `Include`
You may include individual files or all files in a directory in the configuration file.<br>To only include relevant files in the specified directory, the asterisk wildcard character is supported for pattern matching.<br>See [special notes](special_notes_include) about limitations.

Example:

    Include=/absolute/path/to/config/files/*.conf

[comment]: # ({/new-b4d0a8d4})

[comment]: # ({new-39256ab5})
##### `JavaGateway`
The IP address (or hostname) of Zabbix Java gateway. Only required if Java pollers are started.

[comment]: # ({/new-39256ab5})

[comment]: # ({new-d68c6bf4})
##### `JavaGatewayPort`
The port that Zabbix Java gateway listens on.

Default: `10052` | Range: 1024-32767

[comment]: # ({/new-d68c6bf4})

[comment]: # ({new-8ac8d5c2})
##### `ListenBacklog`
The maximum number of pending connections in the TCP queue.<br>The default value is a hard-coded constant, which depends on the system.<br>The maximum supported value depends on the system, too high values may be silently truncated to the 'implementation-specified maximum'.

Default: `SOMAXCONN` | Range: 0 - INT\_MAX

[comment]: # ({/new-8ac8d5c2})

[comment]: # ({new-2f753245})
##### `ListenIP`
A list of comma-delimited IP addresses that the trapper should listen on.<br>Trapper will listen on all network interfaces if this parameter is missing.

Default: `0.0.0.0`

[comment]: # ({/new-2f753245})

[comment]: # ({new-8d927671})
##### `ListenPort`
The listen port for trapper.

Default: `10051` | Range: 1024-32767

[comment]: # ({/new-8d927671})

[comment]: # ({new-0cc86b53})
##### `LoadModule`
The module to load at proxy startup. Modules are used to extend the functionality of the proxy. The module must be located in the directory specified by LoadModulePath or the path must precede the module name. If the preceding path is absolute (starts with '/') then LoadModulePath is ignored.<br>Formats:<br>LoadModule=<module.so><br>LoadModule=<path/module.so><br>LoadModule=</abs\_path/module.so><br>It is allowed to include multiple LoadModule parameters.

[comment]: # ({/new-0cc86b53})

[comment]: # ({new-7cb0d475})
##### `LoadModulePath`
Full path to the location of proxy modules. The default depends on compilation options.

[comment]: # ({/new-7cb0d475})

[comment]: # ({new-4c7ecf23})
##### `LogFile`
Name of the log file.

Mandatory: Yes, if LogType is set to *file*; otherwise no

[comment]: # ({/new-4c7ecf23})

[comment]: # ({new-13e6856d})
##### `LogFileSize`
The maximum size of a log file in MB.<br>0 - disable automatic log rotation.<br>*Note*: If the log file size limit is reached and file rotation fails, for whatever reason, the existing log file is truncated and started anew.

Default: `1` | Range: 0-1024

[comment]: # ({/new-13e6856d})

[comment]: # ({new-333e6ac4})
##### `LogRemoteCommands`
Enable logging of executed shell commands as warnings.

Default: `0` | Values: 0 - disabled, 1 - enabled

[comment]: # ({/new-333e6ac4})

[comment]: # ({new-3d118128})
##### `LogType`
Type of the log output:<br>*file* - write log to the file specified by LogFile parameter;<br>*system* - write log to syslog;<br>*console* - write log to standard output.

Default: `file`

[comment]: # ({/new-3d118128})

[comment]: # ({new-c4c01b03})
##### `LogSlowQueries`
How long a database query may take before being logged (in milliseconds).<br>0 - don't log slow queries.<br>This option becomes enabled starting with DebugLevel=3.

Default: `0` | Range: 0-3600000

[comment]: # ({/new-c4c01b03})

[comment]: # ({new-3bbd77c8})
##### `PidFile`
Name of the PID file.

Default: `/tmp/zabbix\_proxy.pid`

[comment]: # ({/new-3bbd77c8})

[comment]: # ({new-1289de66})
##### `ProxyConfigFrequency`
How often the proxy retrieves configuration data from Zabbix server in seconds.<br>Active proxy parameter. Ignored for passive proxies (see ProxyMode parameter).

Default: `10` | Range: 1-604800

[comment]: # ({/new-1289de66})

[comment]: # ({new-a8e60c77})
##### `ProxyLocalBuffer`
The proxy will keep data locally for N hours, even if the data have already been synced with the server.<br>This parameter may be used if local data will be used by third-party applications.

Default: `0` | Range: 0-720

[comment]: # ({/new-a8e60c77})

[comment]: # ({new-9ff179d3})
##### `ProxyMode`
The proxy operating mode.<br>0 - proxy in the active mode<br>1 - proxy in the passive mode<br>*Note* that (sensitive) proxy configuration data may become available to parties having access to the Zabbix server trapper port when using an active proxy. This is possible because anyone may pretend to be an active proxy and request configuration data; authentication does not take place.

Default: `0` | Range: 0-1

[comment]: # ({/new-9ff179d3})

[comment]: # ({new-de891fc0})
##### `ProxyOfflineBuffer`
The proxy will keep data for N hours in case of no connectivity with Zabbix server.<br>Older data will be lost.

Default: `1` | Range: 1-720

[comment]: # ({/new-de891fc0})

[comment]: # ({new-a5cda34e})
##### `Server`
If ProxyMode is set to *active mode*:<br>Zabbix server IP address or DNS name (address:port) or [cluster](/manual/concepts/server/ha) (address:port;address2:port) to get configuration data from and send data to.<br>If port is not specified, the default port is used.<br>Cluster nodes must be separated by a semicolon.<br><br>If ProxyMode is set to *passive mode*:<br>List of comma delimited IP addresses, optionally in CIDR notation, or DNS names of Zabbix server. Incoming connections will be accepted only from the addresses listed here. If IPv6 support is enabled then '127.0.0.1', '::127.0.0.1', '::ffff:127.0.0.1' are treated equally.<br>'::/0' will allow any IPv4 or IPv6 address. '0.0.0.0/0' can be used to allow any IPv4 address.

Example: 

    Server=127.0.0.1,192.168.1.0/24,::1,2001:db8::/32,zabbix.example.com

Mandatory: yes

[comment]: # ({/new-a5cda34e})

[comment]: # ({new-6984d380})
##### `SNMPTrapperFile`
A temporary file used for passing data from the SNMP trap daemon to the proxy.<br>Must be the same as in zabbix\_trap\_receiver.pl or SNMPTT configuration file.

Default: `/tmp/zabbix\_traps.tmp`

[comment]: # ({/new-6984d380})

[comment]: # ({new-8ab25b82})
##### `SocketDir`
The directory to store IPC sockets used by internal Zabbix services.

Default: `/tmp`

[comment]: # ({/new-8ab25b82})

[comment]: # ({new-94ec4682})
##### `SourceIP`
The source IP address for:<br>- outgoing connections to Zabbix server;<br>- agentless connections (VMware, SSH, JMX, SNMP, Telnet and simple checks);<br>- HTTP agent connections;<br>- script item JavaScript HTTP requests;<br>- preprocessing JavaScript HTTP requests;<br>- connections to the Vault

[comment]: # ({/new-94ec4682})

[comment]: # ({new-d3fc6fd2})
##### `SSHKeyLocation`
The location of public and private keys for SSH checks and actions.

[comment]: # ({/new-d3fc6fd2})

[comment]: # ({new-752b95c3})
##### `SSLCertLocation`
The location of SSL client certificate files for client authentication.<br>This parameter is used in web monitoring only.

[comment]: # ({/new-752b95c3})

[comment]: # ({new-005ddd3a})
##### `SSLKeyLocation`
The location of SSL private key files for client authentication.<br>This parameter is used in web monitoring only.

[comment]: # ({/new-005ddd3a})

[comment]: # ({new-9e6c8051})
##### `SSLCALocation`
The location of certificate authority (CA) files for SSL server certificate verification.<br>Note that the value of this parameter will be set as the CURLOPT\_CAPATH libcurl option. For libcurl versions before 7.42.0, this only has effect if libcurl was compiled to use OpenSSL. For more information see the [cURL web page](http://curl.haxx.se/libcurl/c/CURLOPT_CAPATH.html).<br>This parameter is used in web monitoring and in SMTP authentication.

[comment]: # ({/new-9e6c8051})

[comment]: # ({new-13933066})
##### `StartDBSyncers`
The number of pre-forked instances of [history syncers](/manual/concepts/proxy#proxy_process_types).<br>*Note*: Be careful when changing this value, increasing it may do more harm than good.

Default: `4` | Range: 1-100

[comment]: # ({/new-13933066})

[comment]: # ({new-2d92ef95})
##### `StartDiscoverers`
The number of pre-forked instances of [discoverers](/manual/concepts/proxy#proxy_process_types).

Default: `1` | Range: 0-250

[comment]: # ({/new-2d92ef95})

[comment]: # ({new-4493a1db})
##### `StartHTTPPollers`
The number of pre-forked instances of [HTTP pollers](/manual/concepts/proxy#proxy_process_types).

Default: `1` | Range: 0-1000

[comment]: # ({/new-4493a1db})

[comment]: # ({new-bff9bcdc})
##### `StartIPMIPollers`
The number of pre-forked instances of [IPMI pollers](/manual/concepts/proxy#proxy_process_types).

Default: `0` | Range: 0-1000

[comment]: # ({/new-bff9bcdc})

[comment]: # ({new-9f5d0d89})
##### `StartJavaPollers`
The number of pre-forked instances of [Java pollers](/manual/concepts/proxy#proxy_process_types).

Default: `0` | Range: 0-1000

[comment]: # ({/new-9f5d0d89})

[comment]: # ({new-e8b76471})
##### `StartODBCPollers`
The number of pre-forked instances of [ODBC pollers](/manual/concepts/proxy#proxy_process_types).

Default: `1` | Range: 0-1000

[comment]: # ({/new-e8b76471})

[comment]: # ({new-fa161a46})
##### `StartPingers`
The number of pre-forked instances of [ICMP pingers](/manual/concepts/proxy#proxy_process_types).

Default: `1` | Range: 0-1000

[comment]: # ({/new-fa161a46})

[comment]: # ({new-eef37c0d})
##### `StartPollersUnreachable`
The number of pre-forked instances of [pollers for unreachable hosts](/manual/concepts/proxy#proxy_process_types) (including IPMI and Java). At least one poller for unreachable hosts must be running if regular, IPMI or Java pollers are started.

Default: `1` | Range: 0-1000

[comment]: # ({/new-eef37c0d})

[comment]: # ({new-9d7927c0})
##### `StartPollers`
The number of pre-forked instances of [pollers](/manual/concepts/proxy#proxy_process_types).

Default: `5` | Range: 0-1000

[comment]: # ({/new-9d7927c0})

[comment]: # ({new-8e40eddf})
##### `StartPreprocessors`
The number of pre-forked instances of preprocessing [workers](/manual/concepts/proxy#proxy_process_types).<br>The preprocessing manager process is automatically started when a preprocessor worker is started.

Default: `3` | Range: 1-1000

[comment]: # ({/new-8e40eddf})

[comment]: # ({new-43ff6725})
##### `StartSNMPTrapper`
If set to 1, an [SNMP trapper](/manual/concepts/proxy#proxy_process_types) process will be started.

Default: `0` | Range: 0-1

[comment]: # ({/new-43ff6725})

[comment]: # ({new-3943d442})
##### `StartTrappers`
The number of pre-forked instances of [trappers](/manual/concepts/proxy#proxy_process_types).<br>Trappers accept incoming connections from Zabbix sender and active agents.

Default: `5` | Range: 0-1000

[comment]: # ({/new-3943d442})

[comment]: # ({new-3a4ddabb})
##### `StartVMwareCollectors`
The number of pre-forked [VMware collector](/manual/concepts/proxy#proxy_process_types) instances.

Default: `0` | Range: 0-250

[comment]: # ({/new-3a4ddabb})

[comment]: # ({new-32b0fe66})
##### `StatsAllowedIP`
A list of comma-delimited IP addresses, optionally in CIDR notation, or DNS names of external Zabbix instances. The stats request will be accepted only from the addresses listed here. If this parameter is not set no stats requests will be accepted.<br>If IPv6 support is enabled then '127.0.0.1', '::127.0.0.1', '::ffff:127.0.0.1' are treated equally and '::/0' will allow any IPv4 or IPv6 address. '0.0.0.0/0' can be used to allow any IPv4 address.

Example:

    StatsAllowedIP=127.0.0.1,192.168.1.0/24,::1,2001:db8::/32,zabbix.example.com

[comment]: # ({/new-32b0fe66})

[comment]: # ({new-136f6bfe})
##### `Timeout`
Specifies how long we wait for agent, SNMP device or external check in seconds.

Default: `3` | Range: 1-30

[comment]: # ({/new-136f6bfe})

[comment]: # ({new-b2420c82})
##### `TLSAccept`
What incoming connections to accept from Zabbix server. Used for a passive proxy, ignored on an active proxy. Multiple values can be specified, separated by comma:<br>*unencrypted* - accept connections without encryption (default)<br>*psk* - accept connections with TLS and a pre-shared key (PSK)<br>*cert* - accept connections with TLS and a certificate

Mandatory: yes for passive proxy, if TLS certificate or PSK parameters are defined (even for *unencrypted* connection); otherwise no

[comment]: # ({/new-b2420c82})

[comment]: # ({new-a1178240})
##### `TLSCAFile`
Full pathname of a file containing the top-level CA(s) certificates for peer certificate verification, used for encrypted communications between Zabbix components.

[comment]: # ({/new-a1178240})

[comment]: # ({new-a8ec843d})
##### `TLSCertFile`
Full pathname of a file containing the proxy certificate or certificate chain, used for encrypted communications between Zabbix components.

[comment]: # ({/new-a8ec843d})

[comment]: # ({new-c9ea574f})
##### `TLSCipherAll`
GnuTLS priority string or OpenSSL (TLS 1.2) cipher string. Override the default ciphersuite selection criteria for certificate- and PSK-based encryption.

Example:

    TLS\_AES\_256\_GCM\_SHA384:TLS\_CHACHA20\_POLY1305\_SHA256:TLS\_AES\_128\_GCM\_SHA256

[comment]: # ({/new-c9ea574f})

[comment]: # ({new-985d238a})
##### `TLSCipherAll13`
Cipher string for OpenSSL 1.1.1 or newer in TLS 1.3. Override the default ciphersuite selection criteria for certificate- and PSK-based encryption.

Example for GnuTLS: 

    NONE:+VERS-TLS1.2:+ECDHE-RSA:+RSA:+ECDHE-PSK:+PSK:+AES-128-GCM:+AES-128-CBC:+AEAD:+SHA256:+SHA1:+CURVE-ALL:+COMP-NULL::+SIGN-ALL:+CTYPE-X.509

Example for OpenSSL: 

    EECDH+aRSA+AES128:RSA+aRSA+AES128:kECDHEPSK+AES128:kPSK+AES128

[comment]: # ({/new-985d238a})

[comment]: # ({new-ccd8c768})
##### `TLSCipherCert`
GnuTLS priority string or OpenSSL (TLS 1.2) cipher string. Override the default ciphersuite selection criteria for certificate-based encryption.

Example for GnuTLS: 

    NONE:+VERS-TLS1.2:+ECDHE-RSA:+RSA:+AES-128-GCM:+AES-128-CBC:+AEAD:+SHA256:+SHA1:+CURVE-ALL:+COMP-NULL:+SIGN-ALL:+CTYPE-X.509

Example for OpenSSL: 

    EECDH+aRSA+AES128:RSA+aRSA+AES128

[comment]: # ({/new-ccd8c768})

[comment]: # ({new-d746a953})
##### `TLSCipherCert13`
Cipher string for OpenSSL 1.1.1 or newer in TLS 1.3. Override the default ciphersuite selection criteria for certificate-based encryption.

[comment]: # ({/new-d746a953})

[comment]: # ({new-5d6b530a})
##### `TLSCipherPSK`
GnuTLS priority string or OpenSSL (TLS 1.2) cipher string. Override the default ciphersuite selection criteria for PSK-based encryption.

Example for GnuTLS: 

    NONE:+VERS-TLS1.2:+ECDHE-PSK:+PSK:+AES-128-GCM:+AES-128-CBC:+AEAD:+SHA256:+SHA1:+CURVE-ALL:+COMP-NULL:+SIGN-ALL

Example for OpenSSL: 

    kECDHEPSK+AES128:kPSK+AES128

[comment]: # ({/new-5d6b530a})

[comment]: # ({new-afe5c125})
##### `TLSCipherPSK13`
Cipher string for OpenSSL 1.1.1 or newer in TLS 1.3. Override the default ciphersuite selection criteria for PSK-based encryption.

Example:

    TLS_CHACHA20_POLY1305_SHA256:TLS_AES_128_GCM_SHA256

[comment]: # ({/new-afe5c125})

[comment]: # ({new-b6a5938b})
##### `TLSConnect`
How the proxy should connect to Zabbix server. Used for an active proxy, ignored on a passive proxy. Only one value can be specified:<br>*unencrypted* - connect without encryption (default)<br>*psk* - connect using TLS and a pre-shared key (PSK)<br>*cert* - connect using TLS and a certificate

Mandatory: yes for active proxy, if TLS certificate or PSK parameters are defined (even for *unencrypted* connection); otherwise no

[comment]: # ({/new-b6a5938b})

[comment]: # ({new-4ff06d66})
##### `TLSCRLFile`
The full pathname of a file containing revoked certificates. This parameter is used for encrypted communications between Zabbix components.

[comment]: # ({/new-4ff06d66})

[comment]: # ({new-2cc39b48})
##### `TLSKeyFile`
The full pathname of a file containing the proxy private key, used for encrypted communications between Zabbix components.

[comment]: # ({/new-2cc39b48})

[comment]: # ({new-0f2bf96e})
##### `TLSPSKFile`
The full pathname of a file containing the proxy pre-shared key, used for encrypted communications with Zabbix server.

[comment]: # ({/new-0f2bf96e})

[comment]: # ({new-b57e989d})
##### `TLSPSKIdentity`
The pre-shared key identity string, used for encrypted communications with Zabbix server.

[comment]: # ({/new-b57e989d})

[comment]: # ({new-c8d8910c})
##### `TLSServerCertIssuer`
The allowed server certificate issuer.

[comment]: # ({/new-c8d8910c})

[comment]: # ({new-89726b49})
##### `TLSServerCertSubject`
The allowed server certificate subject.

[comment]: # ({/new-89726b49})

[comment]: # ({new-18ebe014})
##### `TmpDir`
Temporary directory.

Default: `/tmp`

[comment]: # ({/new-18ebe014})

[comment]: # ({new-4880108e})
##### `TrapperTimeout`
Specifies how many seconds the trapper may spend processing new data.

Default: `300` | Range: 1-300

[comment]: # ({/new-4880108e})

[comment]: # ({new-4d4f80c6})
##### `UnavailableDelay`
Determines how often host is checked for availability during the [unavailability](/manual/appendix/items/unreachability#unavailable_host) period in seconds.

Default: `60` | Range: 1-3600

[comment]: # ({/new-4d4f80c6})

[comment]: # ({new-fad94d05})
##### `UnreachableDelay`
Determines how often host is checked for availability during the [unreachability](/manual/appendix/items/unreachability#unreachable_host) period in seconds.

Default: `15` | Range: 1-3600

[comment]: # ({/new-fad94d05})

[comment]: # ({new-316715df})
##### `UnreachablePeriod`
After how many seconds of [unreachability](/manual/appendix/items/unreachability#unreachable_host) treat a host as unavailable.

Default: `45` | Range: 1-3600

[comment]: # ({/new-316715df})

[comment]: # ({new-12e5d15b})
##### `User`
Drop privileges to a specific, existing user on the system.<br>Only has effect if run as 'root' and AllowRoot is disabled.

Default: `zabbix`

[comment]: # ({/new-12e5d15b})

[comment]: # ({new-df11844d})
##### `Vault`
Specifies the vault provider:<br>*HashiCorp* - HashiCorp KV Secrets Engine version 2<br>*CyberArk*  - CyberArk Central Credential Provider<br>Must match the vault provider set in the frontend.

Default: `HashiCorp`

[comment]: # ({/new-df11844d})

[comment]: # ({new-b096e1d2})
##### `VaultDBPath`
Specifies a location, from where database credentials should be retrieved by keys. Depending on the vault, can be vault path or query.<br> The keys used for HashiCorp are 'password' and 'username'. 

Example: 

    secret/zabbix/database

The keys used for CyberArk are 'Content' and 'UserName'.

Example: 

    AppID=zabbix_server&Query=Safe=passwordSafe;Object=zabbix_proxy_database

This option can only be used if DBUser and DBPassword are not specified.

[comment]: # ({/new-b096e1d2})

[comment]: # ({new-2a2c177c})
##### `VaultTLSCertFile`
Name of the SSL certificate file used for client authentication. The certificate file must be in PEM1 format.<br>If the certificate file contains also the private key, leave the SSL key file field empty.<br>The directory containing this file is specified by the SSLCertLocation configuration parameter.<br>This option can be omitted, but is recommended for CyberArkCCP vault.

[comment]: # ({/new-2a2c177c})

[comment]: # ({new-7d66e718})
##### `VaultTLSKeyFile`
Name of the SSL private key file used for client authentication. The private key file must be in PEM1 format.<br>The directory containing this file is specified by the SSLKeyLocation configuration parameter.<br>This option can be omitted, but is recommended for CyberArkCCP vault.

[comment]: # ({/new-7d66e718})

[comment]: # ({new-54de6e11})
##### `VaultToken`
HashiCorp Vault authentication token that should have been generated exclusively for Zabbix proxy with read-only permission to the path specified in the optional VaultDBPath configuration parameter.<br>It is an error if VaultToken and the VAULT\_TOKEN environment variable are defined at the same time.

Mandatory: Yes, if Vault is set to *HashiCorp*; otherwise no

[comment]: # ({/new-54de6e11})

[comment]: # ({new-0ad4c413})
##### `VaultURL`
Vault server HTTP\[S\] URL. The system-wide CA certificates directory will be used if SSLCALocation is not specified.

Default: `https://127.0.0.1:8200`

[comment]: # ({/new-0ad4c413})

[comment]: # ({new-25ec9d34})
##### `VMwareCacheSize`
Shared memory size for storing VMware data.<br>A VMware internal check zabbix\[vmware,buffer,...\] can be used to monitor the VMware cache usage (see [Internal checks](/manual/config/items/itemtypes/internal)).<br>Note that shared memory is not allocated if there are no vmware collector instances configured to start.

Default: `8M` | Range: 256K-2G

[comment]: # ({/new-25ec9d34})

[comment]: # ({new-cd0433e9})
##### `VMwareFrequency`
Delay in seconds between data gathering from a single VMware service.<br>This delay should be set to the least update interval of any VMware monitoring item.

Default: `60` | Range: 10-86400

[comment]: # ({/new-cd0433e9})

[comment]: # ({new-4eb71305})
##### `VMwarePerfFrequency`
Delay in seconds between performance counter statistics retrieval from a single VMware service.<br>This delay should be set to the least update interval of any VMware monitoring [item](/manual/config/items/itemtypes/simple_checks/vmware_keys#footnotes) that uses VMware performance counters.

Default: `60` | Range: 10-86400

[comment]: # ({/new-4eb71305})

[comment]: # ({new-c2a39b44})
##### `VMwareTimeout`
The maximum number of seconds a vmware collector will wait for a response from VMware service (vCenter or ESX hypervisor).

Default: `10` | Range: 1-300

[comment]: # ({/new-c2a39b44})
