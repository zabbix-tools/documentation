[comment]: # translation:outdated

[comment]: # ({new-7af1bbaa})
# 1 Création de la base de données

[comment]: # ({/new-7af1bbaa})

[comment]: # ({new-fab90562})
#### Overview

A Zabbix database must be created during the installation of Zabbix
server or proxy.

This section provides instructions for creating a Zabbix database. A
separate set of instructions is available for each supported database.

::: notetip
`schema.sql`, `images.sql` and `data.sql` files are
located in the *database* subdirectory of Zabbix sources. If Zabbix was
installed from distribution packages, refer to the distribution
documentation.
:::

::: noteimportant
For a Zabbix proxy database, **only** `schema.sql`
should be imported (no images.sql nor data.sql)
:::

[comment]: # ({/new-fab90562})

[comment]: # ({new-fad527fc})
#### MySQL

    shell> mysql -uroot -p<password>
    mysql> create database zabbix character set utf8 collate utf8_bin;
    mysql> grant all privileges on zabbix.* to zabbix@localhost identified by '<password>';
    mysql> quit;

If you use Zabbix packages continue with instructions for
[Debian/Ubuntu](/fr/manual/installation/install_from_packages/debian_ubuntu#importing_data)
or
[RHEL/CentOS](/manual/installation/install_from_packages/rhel_centos#importing_data)
to import the data into the database.

    shell> cd database/mysql
    shell> mysql -uzabbix -p<password> zabbix < schema.sql
    # stop here if you are creating database for Zabbix proxy
    shell> mysql -uzabbix -p<password> zabbix < images.sql
    shell> mysql -uzabbix -p<password> zabbix < data.sql

[comment]: # ({/new-fad527fc})

[comment]: # ({new-1e36e539})
#### PostgreSQL

You need to have database user with permissions to create database
objects. The following shell command will create user `zabbix`. Specify
password when prompted and repeat password (note, you may first be asked
for `sudo` password):

    shell> sudo -u postgres createuser --pwprompt zabbix

Now we will set up the database `zabbix` (last parameter) with the
previously created user as the owner (`-O zabbix`) and import initial
schema and data (assuming you are in the root directory of Zabbix
sources):

    shell> sudo -u postgres createdb -O zabbix zabbix

If you use Zabbix packages continue with instructions for
[Debian/Ubuntu](/fr/manual/installation/install_from_packages/debian_ubuntu#importing_data)
or
[RHEL/CentOS](/manual/installation/install_from_packages/rhel_centos#importing_data)
to import the data into the database.

    shell> cd database/postgresql
    shell> cat schema.sql | sudo -u zabbix psql zabbix
    # stop here if you are creating database for Zabbix proxy
    shell> cat images.sql | sudo -u zabbix psql zabbix
    shell> cat data.sql | sudo -u zabbix psql zabbix

::: noteimportant
The above commands are provided as an example that
will work in most of GNU/Linux installations. You can use different
commands, e. g. "psql -U <username>" depending on how your
system/database are configured. If you have troubles setting up the
database please consult your Database administrator. 
:::

[comment]: # ({/new-1e36e539})

[comment]: # ({new-61d6043c})
#### Oracle

We assume that a *zabbix* database user with *password* password exists
and has permissions to create database objects in ORCL service located
on the *host* Oracle database server with a *user* shell user having
write access to /tmp directory. Zabbix requires a Unicode database
character set and a `UTF8` national character set. Check current
settings:

    sqlplus> select parameter,value from v$nls_parameters where parameter='NLS_CHARACTERSET' or parameter='NLS_NCHAR_CHARACTERSET';

If you are creating a database for Zabbix server you need to have images
from Zabbix sources on the host where Oracle is running. Copy them to a
directory */tmp/zabbix\_images* on the Oracle host:

    shell> cd /path/to/zabbix-sources
    shell> ssh user@oracle_host "mkdir /tmp/zabbix_images"
    shell> scp -r misc/images/png_modern user@oracle_host:/tmp/zabbix_images/

Now prepare the database:

    shell> cd /path/to/zabbix-sources/database/oracle
    shell> sqlplus zabbix/password@oracle_host/ORCL
    sqlplus> @schema.sql
    # stop here if you are creating database for Zabbix proxy
    sqlplus> @images.sql
    sqlplus> @data.sql

Now the temporary directory can be removed:

    shell> ssh user@oracle_host "rm -rf /tmp/zabbix_images"

[comment]: # ({/new-61d6043c})

[comment]: # ({new-cc68ca58})
#### IBM DB2

    shell> db2 "create database zabbix using codeset utf-8 territory us pagesize 32768"
    shell> cd database/ibm_db2
    shell> db2batch -d zabbix -f schema.sql
    # stop here if you are creating database for Zabbix proxy
    shell> db2batch -d zabbix -f images.sql
    shell> db2batch -d zabbix -f data.sql 

::: noteclassic
It is important to set UTF-8 locale for Zabbix server,
Zabbix proxy and the web server running Zabbix frontend. Otherwise text
information from Zabbix will be interpreted by IBM DB2 server as
non-UTF-8 and will be additionally converted on the way from Zabbix to
the database and back. The database will store corrupted non-ASCII
characters.
:::

Zabbix frontend uses `OFFSET` and `LIMIT` clauses in SQL queries. For
this to work, IBM DB2 server must have DB2\_COMPATIBILITY\_VECTOR
variable be set to 3. Run the following command before starting the
database server:

    shell> db2set DB2_COMPATIBILITY_VECTOR=3

[comment]: # ({/new-cc68ca58})

[comment]: # ({new-7b4d56a7})
#### SQLite

Using SQLite is supported for **Zabbix proxy** only!

::: noteclassic
If using SQLite with Zabbix proxy, database will be
automatically created if it does not exist.
:::

    shell> cd database/sqlite3
    shell> sqlite3 /var/lib/sqlite/zabbix.db < schema.sql

Return to the [installation section](/fr/manual/installation/install).

[comment]: # ({/new-7b4d56a7})

[comment]: # ({new-02d49e4f})
#### SQLite

Using SQLite is supported for **Zabbix proxy** only!

::: noteclassic
If using SQLite with Zabbix proxy, database will be
automatically created if it does not exist.
:::

    shell> cd database/sqlite3
    shell> sqlite3 /var/lib/sqlite/zabbix.db < schema.sql

Return to the [installation section](/manual/installation/install).

[comment]: # ({/new-02d49e4f})
