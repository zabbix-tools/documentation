<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="fr" datatype="plaintext" original="manual/appendix/install/onelogin.md">
    <body>
      <trans-unit id="23bd62a5" xml:space="preserve">
        <source># 12 SAML setup with OneLogin</source>
      </trans-unit>
      <trans-unit id="2fcac093" xml:space="preserve">
        <source>#### Overview

This section provides guidelines for configuring single sign-on and user provisioning into Zabbix 
from [OneLogin](https://onelogin.com) using SAML 2.0 authentication.</source>
      </trans-unit>
      <trans-unit id="04a34e60" xml:space="preserve">
        <source>#### OneLogin configuration</source>
      </trans-unit>
      <trans-unit id="f0ff96b7" xml:space="preserve">
        <source>
##### Creating application

1\. Log into your account at OneLogin. For testing purposes, you may create a free developer account in OneLogin.

2\. In the OneLogin web interface navigate to *Applications → Applications*.

3\. Click on "Add App" and search for the appropriate app. The guidelines in this page are based on the *SCIM Provisioner with SAML (SCIM v2 Enterprise, full SAML)* app example.

4\. To begin with, you may want to customize the display name of your app. You may also want to add the icon and app details. After that, click on *Save*.</source>
      </trans-unit>
      <trans-unit id="58a8c759" xml:space="preserve">
        <source>
##### Setting up SSO/SCIM provisioning

1\. In *Configuration* -&gt; *Application details*, set the Zabbix single sign-on endpoint `http://&lt;zabbix-instance-url&gt;/zabbix/index_sso.php?acs` as the value of these fields: 

-   *ACS (Consumer) URL Validator*
-   *ACS (Consumer) URL*

Note the use of "http", and not "https", so that the `acs` parameter is not cut out in the request.

![](../../../../assets/en/manual/appendix/install/onelogin_endpoints.png){width="600"}

It is also possible to use "https". To make that work with Zabbix, it is necessary to add to `conf/zabbix.conf.php` the following line:

```
$SSO['SETTINGS'] = ['use_proxy_headers' =&gt; true];
```

Leave other options with their default values.

2\. In *Configuration* -&gt; *API connection*, set the following values:

-   *SCIM Base URL*: `https://&lt;zabbix-instance-url&gt;/zabbix/api_scim.php`
-   *SCIM JSON Template*: should contain all custom attributes that you would like to pass to Zabbix via SCIM such as `user_name`, `user_lastname`, `user_email`, and `user_mobile`:

```
{
  "schemas": [
    "urn:ietf:params:scim:schemas:core:2.0:User"
  ],
  "userName": "{$parameters.scimusername}",
  "name": {
    "familyName": "{$user.lastname}",
    "givenName": "{$user.firstname}"
  },
   "user_name": "{$user.firstname}",
   "user_lastname": "{$user.lastname}",
   "user_mobile": "{$user.phone}",
   "user_email": "{$user.email}"
}
```
The attribute names are arbitrary. Different attribute names may be used, however, it is required that they match the respective field value in Zabbix SAML settings.

Note that for user provisioning to work, OneLogin needs to receive in response a 'name' attribute with 'givenName' and 'familyName', even if it was not required by the service provider. Thus it is necessary to specify this in the schema in the application configuration part.

-   *SCIM Bearer Token*: enter a Zabbix API token with Super admin permissions. 

Click on *Enable* to activate the connection.

![](../../../../assets/en/manual/appendix/install/onelogin_api_connection.png){width="600"}

3\. In the *Provisioning* page, enable the Provisioning option:

![](../../../../assets/en/manual/appendix/install/onelogin_provisioning.png){width="600"}

4\. The *Parameters* page contains a list of default parameters:

-   Make sure that the 'scimusername' matches the user login value in OneLogin (e.g. email); 
-   Mark the *Include in User Provisioning* option for the 'Groups' parameter;
-   Click on "+" to create the custom parameters that are required for SAML assertions and user provisioning such as `user_name`, `user_lastname`, `user_email`, and `user_mobile`:

![](../../../../assets/en/manual/appendix/install/onelogin_param_add.png)

When adding a parameter, make sure to mark both the *Include in SAML assertion* and *Include in User Provisioning* options.

-   Add a 'group' parameter that matches user roles in OneLogin. User roles will be passed as a string, separated by a semicolon `;`. The OneLogin user roles will be the used for creating user groups in Zabbix:

![](../../../../assets/en/manual/appendix/install/onelogin_param_group.png)

Verify the list of parameters:

![](../../../../assets/en/manual/appendix/install/onelogin_params.png){width="600"}

5\. In the *Rules* page, create user role mappings to the default Groups parameter.

![](../../../../assets/en/manual/appendix/install/onelogin_rule_edit.png){width="600"}

You may use a regular expression to pass specific roles as groups. The role names should not contain `;` as OneLogin uses it as a separator when sending an attribute with several roles.</source>
      </trans-unit>
      <trans-unit id="9be86dbb" xml:space="preserve">
        <source>#### Zabbix configuration

1\. In Zabbix, go to the [SAML settings](/manual/web_interface/frontend_sections/users/authentication/saml#setting-up-zabbix) 
and fill the configuration options based on the OneLogin configuration:

![](../../../../assets/en/manual/appendix/install/onelogin_zabbix_conf.png){width="600"}

|Zabbix field|Setup field in OneLogin|Sample value|
|----|----|--|
|*IdP entity ID*|Issuer URL&lt;br&gt;(see SSO tab of your application in OneLogin)||
|*SSO service URL*|SAML 2.0 Endpoint (HTTP)&lt;br&gt;(see SSO tab of your application in OneLogin)||
|*SLO service URL*|SLO Endpoint (HTTP)&lt;br&gt;(see SSO tab of your application in OneLogin)||
|*Username attribute*|Custom parameter|`user_email`|
|*Group name attribute*|Custom parameter|`group`|
|*User name attribute*|Custom parameter|`user_name`|
|*User last name attribute*|Custom parameter|`user_lastname`|

It is also required to configure user group mapping. Media mapping is optional. Click on *Update* to save these settings.

2\. Download the certificate provided by OneLogin and place it into `conf/certs` of the Zabbix frontend installation, as idp.crt. 

Set 644 permissions to it by running:

    chmod 644 idp.crt

You can access the certificate download in OneLogin in *Applications* -&gt; *SSO* -&gt; click on *View details* under the current certificate.

It is possible to use a different certificate name and location. In that case, make sure to add to `conf/zabbix.conf.php` the following line:

```
$SSO['IDP_CERT'] = 'path/to/certname.crt';
```</source>
      </trans-unit>
      <trans-unit id="09c52023" xml:space="preserve">
        <source>
#### SCIM user provisioning

With user provisioning enabled, it is now possible to add/update users and their roles in OneLogin and have them immediately provisioned to Zabbix.

For example, you may create a new user:

![](../../../../assets/en/manual/appendix/install/onelogin_user.png){width="600"}

Add it to a user role and the application that will provision the user:

![](../../../../assets/en/manual/appendix/install/onelogin_add_to_role.png){width="600"}

When saving the user, it will be provisioned to Zabbix. In Application -&gt; Users you can check the provisioning status of current application users:

![](../../../../assets/en/manual/appendix/install/onelogin_provisioned.png){width="600"}

If successfully provisioned, the user can be seen in the Zabbix user list.

![](../../../../assets/en/manual/appendix/install/onelogin_users_zbx.png){width="600"}</source>
      </trans-unit>
    </body>
  </file>
</xliff>
