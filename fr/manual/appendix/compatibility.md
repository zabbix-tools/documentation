[comment]: # translation:outdated

[comment]: # ({new-d25f4c9e})
# 13 Version compatibility

[comment]: # ({/new-d25f4c9e})

[comment]: # ({new-eefa9a73})
#### Supported agents

Zabbix agents starting with version 1.4 are compatible with Zabbix 4.0.
However, you may need to review the configuration of older agents as
some parameters have changed, for example, parameters related to
[logging](https://www.zabbix.com/documentation/3.0/manual/installation/upgrade_notes_300#changes_in_configuration_parameters_related_to_logging)
for versions before 3.0.

To take full advantage of new and improved items, improved performance
and reduced memory usage, use the latest 4.0 agent.

[comment]: # ({/new-eefa9a73})

[comment]: # ({new-1a5df00a})
#### Supported Zabbix proxies

Both Zabbix 4.0 proxies and Zabbix 4.0 server are supported to work only
with Zabbix 4.0 server and Zabbix 4.0 proxies respectively.

::: noteimportant
It is known to be possible to start an upgraded
server and have older, yet unupgraded proxies report data to a newer
server (the proxies can't refresh their configuration though). This
approach, however, is not recommended and not supported by Zabbix and
choosing it is entirely at your own risk. For more information, see the
[upgrade procedure](/manual/installation/upgrade).
:::

[comment]: # ({/new-1a5df00a})

[comment]: # ({new-5be689b0})
#### Supported XML files

XML files, exported with 1.8, 2.0, 2.2, 2.4, 3.0, 3.2 and 3.4 are
supported for import in Zabbix 4.0.

::: noteimportant
In Zabbix 1.8 XML export format, trigger
dependencies are stored by name only. If there are several triggers with
the same name (for example, having different severities and expressions)
that have a dependency defined between them, it is not possible to
import them. Such dependencies must be manually removed from the XML
file and re-added after import.
:::

[comment]: # ({/new-5be689b0})

[comment]: # ({new-78c6278a})

In relation to Zabbix server, proxies can be:
-   *Current* (proxy and server have the same major version);
-   *Outdated* (proxy version is older than server version, but is partially supported);
-   *Unsupported* (proxy version is older than server previous LTS release version *or* proxy version is newer than
server major version).

Examples:

|Server version|*Current* proxy version|*Outdated* proxy version|*Unsupported* proxy version|
|--------------|-----------------------|------------------------|---------------------------|
|6.4|6.4|6.0, 6.2|Older than 6.0; newer than 6.4|
|7.0|7.0|6.0, 6.2, 6.4|Older than 6.0; newer than 7.0|
|7.2|7.2|7.0|Older than 7.0; newer than 7.2|

[comment]: # ({/new-78c6278a})

[comment]: # ({new-9f9fce98})

Functionality supported by proxies:

|Proxy version|Data update|Configuration update|Tasks|
|--|--|--|----|
|*Current*|Yes|Yes|Yes|
|*Outdated*|Yes|No|[Remote commands](/manual/config/notifications/action/operation/remote_command) (e.g., shell scripts);<br>Immediate item value checks (i.e., *[Execute now](/manual/config/items/check_now)*);<br>Note: Preprocessing [tests with a real value](/manual/config/items/preprocessing#testing-real-value) are not supported.|
|*Unsupported*|No|No|No|

[comment]: # ({/new-9f9fce98})

[comment]: # ({new-f9a2762b})

Warnings about using incompatible Zabbix daemon versions are logged.

[comment]: # ({/new-f9a2762b})

[comment]: # ({new-5f9e54ca})
#### Supported XML files

XML files not older than version 1.8 are supported for import in Zabbix
6.0.

::: noteimportant
In the XML export format, trigger dependencies are
stored by name only. If there are several triggers with the same name
(for example, having different severities and expressions) that have a
dependency defined between them, it is not possible to import them. Such
dependencies must be manually removed from the XML file and re-added
after import.
:::

[comment]: # ({/new-5f9e54ca})
