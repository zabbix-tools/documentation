[comment]: # translation:outdated

[comment]: # ({new-9153def3})
# 4 Header and data length

[comment]: # ({/new-9153def3})

[comment]: # ({new-faa3c87b})
#### Overview

Header and data length are present in response and request messages
between Zabbix components. It is required to determine the length of
message.

    <HEADER> - "ZBXD\x01" (5 bytes)
    <DATALEN> - data length (8 bytes). 1 will be formatted as 01/00/00/00/00/00/00/00 (eight bytes, 64 bit number in little-endian format)

To not exhaust memory (potentially) Zabbix protocol is limited to accept
only 128MB in one connection.

[comment]: # ({/new-faa3c87b})

[comment]: # ({new-409c40cc})

#### Structure

The header consists of four fields. All numbers in the header are formatted as little-endian.

|Field|Size|Size<br>(large packet)|Description|
|--|-|-|------|
|`<PROTOCOL>`|4|4|`"ZBXD"` or `5A 42 58 44`|
|`<FLAGS>`|1|1|Protocol flags:<br>`0x01` - Zabbix communications protocol<br>`0x02` - compression<br>`0x04` - large packet|
|`<DATALEN>`|4|8|Data length.|
|`<RESERVED>`|4|8|When compression is used (`0x02` flag) - the length of uncompressed data<br>When compression is not used - `00 00 00 00`|

[comment]: # ({/new-409c40cc})

[comment]: # ({new-8698de59})
#### Implementation

Here are code snippets showing how to add Zabbix protocol header to the
`data` you *want* to send in order to obtain `packet` you *should* send
to Zabbix so it is interpreted correctly.

|Language|Code|
|--------|----|
|bash|`printf -v LENGTH '%016x' "${#DATA}"PACK=""for i in {14..0..-2}; do PACK="$PACK\\x${LENGTH:$i:2}"; doneprintf "ZBXD\1$PACK%s" $DATA`{.bash}|
|Java|`byte[] header = new byte[] {'Z', 'B', 'X', 'D', '\1',(byte)(data.length & 0xFF),(byte)((data.length >> 8) & 0xFF),(byte)((data.length >> 16) & 0xFF),(byte)((data.length >> 24) & 0xFF),'\0', '\0', '\0', '\0'};|
|<|byte[] packet = new byte[header.length + data.length];System.arraycopy(header, 0, packet, 0, header.length);System.arraycopy(data, 0, packet, header.length, data.length);`{.Java}|
|PHP|`$packet = "ZBXD\1" . pack('P', strlen($data)) . $data;`{.PHP}or`$packet = "ZBXD\1" . pack('V', strlen($data)) . "\0\0\0\0" . $data;`{.PHP}|
|Perl|`my $packet = "ZBXD\1" . pack('<Q', length($data)) . $data;`{.Perl}or`my $packet = "ZBXD\1" . pack('V', length($data)) . "\0\0\0\0" . $data;`{.Perl}|
|Python|`packet = "ZBXD\1" + struct.pack('<Q', len(data)) + data`{.Python}|

[comment]: # ({/new-8698de59})
