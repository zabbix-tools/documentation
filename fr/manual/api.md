[comment]: # translation:outdated

[comment]: # ({new-92f30379})
# 19. API

[comment]: # ({/new-92f30379})

[comment]: # ({a9b3b7c2-13405b49})
### Aperçu

L'API Zabbix vous permet de récupérer et de modifier par programmation la configuration de Zabbix et donne accès aux données historiques. Elle est largement utilisée pour :

-   Créer de nouvelles applications pour s'interfacer avec Zabbix;
-   Intégrer Zabbix avec un logiciel tiers;
-   Automatiser les tâches de routine.

L'API Zabbix est une API Web et est fournie avec l'interface Web. Elle utilise le protocole JSON-RPC 2.0, ce qui implique deux choses :

-   L'API consiste en un ensemble de méthodes distinctes;
-   Les demandes et les réponses entre les clients et l'API sont codées au format JSON.

Vous trouverez plus d'informations sur le protocole et JSON dans la [spécification JSON-RPC 2.0](http://www.jsonrpc.org/specification) et la [page d'accueil du format JSON](http://json.org/).

[comment]: # ({/a9b3b7c2-13405b49})

[comment]: # ({ceda6781-9cb56d09})
### Structure

L'API consiste en un certain nombre de méthodes qui sont nominalement regroupées dans des API distinctes. Chacune des méthodes effectue une tâche spécifique. Par exemple, la méthode 'host.create' appartient à l'API *Hôte* et est utilisée pour créer de nouveaux hôtes. Historiquement, les API sont parfois appelées "classes".

::: notetip
La plupart des API contiennent au moins quatre méthodes: `get`, `create`, `update` et `delete` pour respectivement récupérer, créer, mettre à jour et supprimer des données, mais certaines API peuvent fournir un ensemble de méthodes totalement différentes.
:::

[comment]: # ({/ceda6781-9cb56d09})

[comment]: # ({bba87492-9cb2f5ca})
### Effectuer des requêtes

Une fois que vous avez configuré l'interface web, vous pouvez utiliser des requêtes HTTP distantes pour appeler l'API. Pour ce faire, vous devez envoyer des requêtes HTTP POST au fichier `api_jsonrpc.php` situé dans le répertoire frontend. Par exemple, si votre interface Zabbix est installée sous *http://company.com/zabbix*, la requête HTTP permettant d'appeler la méthode `apiinfo.version` peut ressembler à ceci:

``` {.http}
POST http://company.com/zabbix/api_jsonrpc.php HTTP/1.1
Content-Type: application/json-rpc

{"jsonrpc":"2.0","method":"apiinfo.version","id":1,"auth":null,"params":{}}
```

L'en-tête `Content-Type` de la demande doit être défini sur l'une des valeurs suivantes : `application/json-rpc`, `application/json` ou `application/jsonrequest`.

::: notetip
Vous pouvez utiliser n’importe quel client HTTP ou outil de test JSON-RPC pour exécuter des requêtes d’API manuellement, mais pour le développement d’applications, nous vous suggérons d’utiliser l’une des [bibliothèques gérées par la communauté](http://zabbix.org/wiki/Docs/api/libraries).
:::

[comment]: # ({/bba87492-9cb2f5ca})

[comment]: # ({b6032941-ec5a9f60})
### Exemple de flux de travail

La section suivante vous guidera à travers quelques exemples d'utilisation plus détaillés.

[comment]: # ({/b6032941-ec5a9f60})

[comment]: # ({7e6ce0b7-ef3f5841})
#### Authentification

Avant de pouvoir accéder aux données de Zabbix, vous devez vous connecter et obtenir un jeton d'authentification. Ceci peut être fait en utilisant la méthode [user.login](/manual/api/reference/user/login). Supposons que vous souhaitiez vous connecter en tant qu’administrateur Zabbix standard. Votre demande JSON ressemblera à ceci :

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "user.login",
    "params": {
        "user": "Admin",
        "password": "zabbix"
    },
    "id": 1,
    "auth": null
}
```

Examinons de plus près l'objet de la requête. Il a les propriétés suivantes :

-   `jsonrpc` - version du protocole JSON-RPC utilisée par l'API ; l'API Zabbix implémente JSON-RPC version 2.0 ;
-   `method` - la méthode API appelée ;
-   `params` - paramètres qui seront transmis à la méthode API ;
-   `id` - un identifiant arbitraire de la requête;
-   `auth` - un jeton d'authentification utilisateur ; comme nous n'en avons pas encore, il est défini à `null`.

Si vous avez correctement fourni les informations d'identification, la réponse renvoyée par l'API contiendra le jeton d'authentification de l'utilisateur :

``` {.java}
{
    "jsonrpc": "2.0",
    "result": "0424bd59b807674191e7d77572075f33",
    "id": 1
}
```

L'objet de réponse contient à son tour les propriétés suivantes :

-   `jsonrpc` - encore une fois, la version du protocole JSON-RPC ;
-   `result` - les données retournées par la méthode ;
-   `id` - identifiant de la requête correspondante.

[comment]: # ({/7e6ce0b7-ef3f5841})

[comment]: # ({new-37011a71})
#### Authorization methods

[comment]: # ({/new-37011a71})

[comment]: # ({new-130188f5})
##### By "Authorization" header

All API requests require an authentication or an API token.
You can provide the credentials by using the "Authorization" request header:

```bash
curl --request POST \
  --url 'https://company.com/zabbix/ui/api_jsonrpc.php' \
  --header 'Authorization: Bearer 0424bd59b807674191e7d77572075f33'
```

[comment]: # ({/new-130188f5})

[comment]: # ({new-38cc7fd6})
##### By "auth" property

An API request can be authorized by the "auth" property.

::: noteimportant
Note that the "auth" property is deprecated. It will be removed in the future releases.
:::

```bash
curl --request POST \
  --url 'https://company.com/zabbix/ui/api_jsonrpc.php' \
  --header 'Content-Type: application/json-rpc' \
  --data '{"jsonrpc":"2.0","method":"host.get","params":{"output":["hostid"]},"auth":"0424bd59b807674191e7d77572075f33","id":1}'
```

[comment]: # ({/new-38cc7fd6})

[comment]: # ({new-8950965e})
##### By Zabbix cookie

A *"zbx_session"* cookie is used to authorize an API request from Zabbix UI performed using JavaScript (from a module or
a custom widget).

[comment]: # ({/new-8950965e})

[comment]: # ({bfcaaf47-f572ecc2})
#### Récupération des hôtes

Nous avons maintenant un jeton d'authentification utilisateur valide qui peut être utilisé pour accéder aux données dans Zabbix. Par exemple, utilisons la méthode [host.get](/manual/api/reference/host/get) pour récupérer les ID, les noms d'hôtes et les interfaces de tous les [hôtes](/manual/api/reference/host/object) :

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.get",
    "params": {
        "output": [
            "hostid",
            "host"
        ],
        "selectInterfaces": [
            "interfaceid",
            "ip"
        ]
    },
    "id": 2,
    "auth": "0424bd59b807674191e7d77572075f33"
}
```

::: noteimportant
Notez que la propriété `auth` est maintenant définie sur le jeton d'authentification que nous avons obtenu en appelant `user.login`.
:::

L'objet de réponse contiendra les données demandées au sujet des hôtes :

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "hostid": "10084",
            "host": "Zabbix server",
            "interfaces": [
                {
                    "interfaceid": "1",
                    "ip": "127.0.0.1"
                }
            ]
        }
    ],
    "id": 2
}
```

::: notetip
Pour des raisons de performances, nous vous recommandons de toujours lister les propriétés d'objet que vous souhaitez récupérer et d'éviter de tout récupérer.
:::

[comment]: # ({/bfcaaf47-f572ecc2})

[comment]: # ({ece61bea-ee2c324f})
#### Création d'un nouvel élément

Créons un nouvel [élément](/manual/api/reference/item/object) sur l'hôte "Zabbix server" en utilisant les données que nous avons obtenues de la précédente requête `host.get`. Cela peut être fait en utilisant la méthode [item.create](/manual/api/reference/item/create) :

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "item.create",
    "params": {
        "name": "Free disk space on /home/joe/",
        "key_": "vfs.fs.size[/home/joe/,free]",
        "hostid": "10084",
        "type": 0,
        "value_type": 3,
        "interfaceid": "1",
        "delay": 30
    },
    "auth": "0424bd59b807674191e7d77572075f33",
    "id": 3
}
```

Une réponse réussie contiendra l'ID du nouvel élément créé, qui peut être utilisé pour référencer l'élément dans les demandes suivantes :

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "24759"
        ]
    },
    "id": 3
}
```

::: notetip
La méthode `item.create` ainsi que d'autres méthodes de création peuvent également accepter des tableaux d'objets et créer plusieurs éléments avec un seul appel d'API.
:::

[comment]: # ({/ece61bea-ee2c324f})

[comment]: # ({a544a13e-5ed44978})
#### Création de plusieurs déclencheurs

Donc, si les méthodes  create acceptent les tableaux, nous pouvons ajouter plusieurs [déclencheurs](/manual/api/reference/trigger/object) comme ceci :

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "trigger.create",
    "params": [
        {
            "description": "Processor load is too high on {HOST.NAME}",
            "expression": "last(/Linux server/system.cpu.load[percpu,avg1])>5",
        },
        {
            "description": "Too many processes on {HOST.NAME}",
            "expression": "avg(/Linux server/proc.num[],5m)>300",
        }
    ],
    "auth": "0424bd59b807674191e7d77572075f33",
    "id": 4
}
```

Une réponse réussie contiendra les identifiants des déclencheurs nouvellement créés :

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "triggerids": [
            "17369",
            "17370"
        ]
    },
    "id": 4
}
```

[comment]: # ({/a544a13e-5ed44978})

[comment]: # ({360c80a1-aa174634})
#### Mise à jour d'un élément

Activer un élément, autrement dit, définir son statut à "0" :

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "item.update",
    "params": {
        "itemid": "10092",
        "status": 0
    },
    "auth": "0424bd59b807674191e7d77572075f33",
    "id": 5
}
```

Une réponse réussie contiendra l'ID de l'élément mis à jour :

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "10092"
        ]
    },
    "id": 5
}
```

::: notetip
La méthode `item.update` ainsi que d'autres méthodes de mise à jour peuvent également accepter des tableaux d'objets et mettre à jour plusieurs éléments avec un seul appel d'API.
:::

[comment]: # ({/360c80a1-aa174634})

[comment]: # ({a7e1fb3b-e217b4a2})
#### Mise à jour de plusieurs déclencheurs

Activer plusieurs déclencheurs, autrement dit, définir leur statut à "0" :

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "trigger.update",
    "params": [
        {
            "triggerid": "13938",
            "status": 0
        },
        {
            "triggerid": "13939",
            "status": 0
        }
    ],
    "auth": "0424bd59b807674191e7d77572075f33",
    "id": 6
}
```

Une réponse réussie contiendra les identifiants des déclencheurs mis à jour :

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "triggerids": [
            "13938",
            "13939"
        ]
    },
    "id": 6
}
```

::: notetip
C'est la méthode à privilégier pour la mise à jour. Certaines méthodes de l'API telles que `host.massupdate` permettent d'écrire un code plus simple, mais il n'est pas recommandé d'utiliser ces méthodes, car elles seront supprimées dans les prochaines versions.
:::

[comment]: # ({/a7e1fb3b-e217b4a2})

[comment]: # ({18dd3361-7665e280})
#### Gestion des erreurs

Jusque là, tout ce que nous avons essayé a bien fonctionné. Mais que se passe-t-il si nous essayons de faire un appel incorrect à l'API ? Essayons de créer un autre hôte en appelant la méthode [host.create](/manual/api/reference/host/create) mais en omettant le paramètre obligatoire `groups`.

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.create",
    "params": {
        "host": "Linux server",
        "interfaces": [
            {
                "type": 1,
                "main": 1,
                "useip": 1,
                "ip": "192.168.3.1",
                "dns": "",
                "port": "10050"
            }
        ]
    },
    "id": 7,
    "auth": "0424bd59b807674191e7d77572075f33"
}
```

La réponse contiendra alors un message d'erreur :

``` {.java}
{
    "jsonrpc": "2.0",
    "error": {
        "code": -32602,
        "message": "Invalid params.",
        "data": "No groups for host \"Linux server\"."
    },
    "id": 7
}
```

Si une erreur survient, au lieu de la propriété `result`, l'objet de réponse contiendra une propriété `error` avec les données suivantes :

-   `code` - un code d'erreur ;
-   `message` - un résumé succinct de l'erreur ;
-   `data` - un message d'erreur plus détaillé.

Des erreurs peuvent survenir dans différents cas, tels que l'utilisation de valeurs d'entrée incorrectes, l'expiration du délai d'une session ou la tentative d'accès à des objets non existants. Votre application doit pouvoir gérer ce type d’erreurs.

[comment]: # ({/18dd3361-7665e280})

[comment]: # ({c34573ee-c1074d48})
### Versions de l'API

TPour simplifier la gestion des versions de l'API, depuis Zabbix 2.0.4, la version de l'API correspond à la version de Zabbix elle-même. Vous pouvez utiliser la méthode [apiinfo.version](/manual/api/reference/apiinfo/version) pour connaître la version de l'API avec laquelle vous travaillez. Cela peut être utile pour ajuster votre application afin qu'elle utilise les fonctionnalités spécifiques à la version.

Nous garantissons la compatibilité ascendante des fonctionnalités dans une version majeure. Lorsque nous effectuons des modifications incompatibles entre les versions majeures, nous conservons généralement les anciennes fonctionnalités comme obsolètes dans la version suivante et nous les supprimons ensuite de la version suivante. Parfois, nous pouvons supprimer des fonctionnalités entre versions majeures sans fournir de compatibilité ascendante. Il est important que vous ne vous reposiez jamais sur des fonctionnalités obsolètes et que vous migriez vers de nouvelles alternatives dès que possible.

::: notetip
Vous pouvez suivre toutes les modifications apportées à l'API dans le [journal des modifications de l'API](/manual/api/changes_5.4_-_6.0).
:::

[comment]: # ({/c34573ee-c1074d48})

[comment]: # ({9cb2868a-dfd7315f})
### Lectures complémentaires

Vous en savez maintenant assez pour commencer à travailler avec l'API Zabbix, mais ne vous arrêtez pas là. Pour en savoir plus, nous vous conseillons de consulter la [liste des API disponibles](/manual/api/reference).

[comment]: # ({/9cb2868a-dfd7315f})
