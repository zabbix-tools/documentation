[comment]: # translation:outdated

[comment]: # ({new-566c4be6})
# 14. Configuration export/import

[comment]: # ({/new-566c4be6})

[comment]: # ({new-265e7dfe})
#### Overview

Zabbix export/import functionality makes it possible to exchange various
configuration entities between one Zabbix system and another.

Typical use cases for this functionality:

-   share templates or network maps - Zabbix users may share their
    configuration parameters
-   share web scenarios on *share.zabbix.com* - export a template with
    the web scenarios and upload to *share.zabbix.com*. Then others can
    download the template and import the XML into Zabbix.
-   integrate with third-party tools - the universal XML format makes
    integration and data import/export possible with third party tools
    and applications

[comment]: # ({/new-265e7dfe})

[comment]: # ({new-19950447})
##### What can be exported/imported

Objects that can be exported/imported are:

-   [host groups](/manual/xml_export_import/groups) (*through Zabbix API
    only*)
-   [templates](/manual/xml_export_import/templates)
-   [hosts](/manual/xml_export_import/hosts)
-   [network maps](/manual/xml_export_import/maps)
-   images
-   [screens](/manual/xml_export_import/screens)
-   value maps

[comment]: # ({/new-19950447})

[comment]: # ({new-44b0f5b3})
##### Export format

Data can be exported using the Zabbix web frontend or [Zabbix
API](/fr/manual/api/reference/configuration). Supported export formats
are:

-   XML - in the frontend
-   XML or JSON - in Zabbix API

[comment]: # ({/new-44b0f5b3})

[comment]: # ({new-b87bd84d})
#### Details about export

-   All supported elements are exported in one file.
-   Host and template entities (items, triggers, graphs, discovery
    rules) that are inherited from linked templates are not exported.
    Any changes made to those entities on a host level (such as changed
    item interval, modified regular expression or added prototypes to
    the low-level discovery rule) will be lost when exporting; when
    importing, all entities from linked templates are re-created as on
    the original linked template.
-   Entities created by low-level discovery and any entities depending
    on them are not exported. For example, a trigger created for an
    LLD-rule generated item will not be exported.

[comment]: # ({/new-b87bd84d})

[comment]: # ({new-6ea18fa7})
#### Details about import

-   Import stops at the first error.
-   When updating existing images during image import, "imagetype" field
    is ignored, i.e. it is impossible to change image type via import.
-   When importing hosts/templates using the "Delete missing" option,
    host/template macros not present in the imported XML file will be
    deleted too.
-   Empty tags for items, triggers, graphs, host/template applications,
    discoveryRules, itemPrototypes, triggerPrototypes, graphPrototypes
    are meaningless i.e. it's the same as if it was missing. Other tags,
    for example, item applications, are meaningful i.e. empty tag means
    no applications for item, missing tag means don't update
    applications.
-   Import supports both XML and JSON, the import file must have a
    correct file extension: .xml for XML and .json for JSON.
-   See [compatibility information](/fr/manual/appendix/compatibility)
    about supported XML versions.

[comment]: # ({/new-6ea18fa7})

[comment]: # ({new-790544d4})
#### XML base format

``` {.xml}
<?xml version="1.0" encoding="UTF-8"?>
<zabbix_export>
    <version>4.0</version>
    <date>2016-10-04T06:20:11Z</date>
</zabbix_export>
```

    <?xml version="1.0" encoding="UTF-8"?>

Default header for XML documents.

    <zabbix_export>

Root element for Zabbix XML export.

    <version>4.0</version>

Export version.

    <date>2016-10-04T06:20:11Z</date>

Date when export was created in ISO 8601 long format.

Other tags are dependent on exported objects.

[comment]: # ({/new-790544d4})

[comment]: # ({new-8c839240})
#### XML format

``` {.xml}
<?xml version="1.0" encoding="UTF-8"?>
<zabbix_export>
    <version>6.0</version>
    <date>2020-04-22T06:20:11Z</date>
</zabbix_export>
```

    <?xml version="1.0" encoding="UTF-8"?>

Default header for XML documents.

    <zabbix_export>

Root element for Zabbix XML export.

    <version>6.0</version>

Export version.

    <date>2020-04-22T06:20:11Z</date>

Date when export was created in ISO 8601 long format.

Other tags are dependent on exported objects.

[comment]: # ({/new-8c839240})

[comment]: # ({new-239421b8})
#### JSON format

``` {.json}
{
    "zabbix_export": {
        "version": "6.0",
        "date": "2020-04-22T06:20:11Z"
    }
}
```

      "zabbix_export":

Root node for Zabbix JSON export.

          "version": "6.0"

Export version.

          "date": "2020-04-22T06:20:11Z"

Date when export was created in ISO 8601 long format.

Other nodes are dependent on exported objects.

[comment]: # ({/new-239421b8})
