[comment]: # translation:outdated

[comment]: # ({new-0a5cc230})
# 8 Modifications des modèles

Cette page répertorie toutes les modifications apportées aux modèles
livrés avec Zabbix. Il est suggéré de modifier ces modèles dans les
installations existantes. En fonction des modifications, il est possible
d'importer la dernière version ou d'effectuer la modification
manuellement.

[comment]: # ({/new-0a5cc230})

[comment]: # ({new-94950308})
### CHANGES IN 7.0.0

[comment]: # ({/new-94950308})

[comment]: # ({new-651539db})
#### Updated templates

**Zabbix server/proxy health**

Templates for Zabbix server/proxy health have been updated according to the [changes](/manual/introduction/whatsnew700#concurrency-in-network-discovery) 
in network discovery. The items for monitoring the discoverer process (now removed) has been replaced by items to measure the 
discovery manager and discovery worker process utilization. A new internal item has been added for monitoring the discovery queue. 

[comment]: # ({/new-651539db})







