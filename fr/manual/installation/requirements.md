[comment]: # translation:outdated

[comment]: # ({new-02b10f8d})
# 2 Pré-requis

[comment]: # ({/new-02b10f8d})

[comment]: # ({new-80330f65})
#### Matériel

[comment]: # ({/new-80330f65})

[comment]: # ({new-4ae00568})
##### Mémoire

Zabbix nécessite à la fois de la mémoire physique et de la mémoire
disque. 128 Mo de mémoire physique et 256 Mo d'espace disque libre
pourraient être un bon point de départ. Cependant, la quantité de
mémoire disque requise dépend évidemment du nombre d'hôtes et de
paramètres surveillés. Si vous prévoyez de conserver un historique
conséquent des paramètres surveillés, vous devriez penser à au moins
quelques gigaoctets pour avoir assez d'espace pour stocker l'historique
dans la base de données. Chaque processus démon Zabbix nécessite
plusieurs connexions à un serveur de base de données. La quantité de
mémoire allouée à la connexion dépend de la configuration du moteur de
base de données.

::: noteclassic
Plus vous aurez de mémoire physique, plus la base de données
(et donc Zabbix) fonctionnera rapidement !
:::

[comment]: # ({/new-4ae00568})

[comment]: # ({new-7967309a})
##### CPU

Zabbix et en particulier la base de données Zabbix peuvent nécessiter
des ressources CPU importantes en fonction du nombre de paramètres
surveillés et du moteur de base de données choisi.

[comment]: # ({/new-7967309a})

[comment]: # ({new-1cbee7bf})
##### Autre materiel

Un port de communication série et un modem GSM sont requis pour
l'utilisation des notifications par SMS dans Zabbix. Un convertisseur
USB vers série fonctionnera également.

[comment]: # ({/new-1cbee7bf})

[comment]: # ({new-42d1d93e})
#### Examples of hardware configuration

The table provides examples of hardware configuration, assuming a **Linux/BSD/Unix** platform.

These are size and hardware configuration examples to start with. Each Zabbix installation is unique. 
Make sure to benchmark the performance of your Zabbix system in a staging or development environment, 
so that you can fully understand your requirements before deploying the Zabbix installation to its 
production environment.

|Installation size|Monitored metrics^**1**^|CPU/vCPU cores|Memory<br>(GiB)|Database|Amazon EC2^**2**^|
|-|-|-|-|-|-|
|Small|1 000|2|8|MySQL Server,<br>Percona Server,<br>MariaDB Server,<br>PostgreSQL|m6i.large/m6g.large|
|Medium|10 000|4|16|MySQL Server,<br>Percona Server,<br>MariaDB Server,<br>PostgreSQL|m6i.xlarge/m6g.xlarge|
|Large|100 000|16|64|MySQL Server,<br>Percona Server,<br>MariaDB Server,<br>PostgreSQL,<br>Oracle|m6i.4xlarge/m6g.4xlarge|
|Very large|1 000 000|32|96|MySQL Server,<br>Percona Server,<br>MariaDB Server,<br>Oracle|m6i.8xlarge/m6g.8xlarge|

^**1**^ 1 metric = 1 item + 1 trigger + 1 graph<br>
^**2**^ Example with Amazon general purpose EC2 instances, using ARM64 or x86_64 architecture, a 
proper instance type like Compute/Memory/Storage optimised should be selected during Zabbix 
installation evaluation and testing before installing in its production environment.

::: noteclassic
Actual configuration depends on the number of active items
and refresh rates very much (see [database
size](/manual/installation/requirements#Database_size) section of this
page for details). It is highly recommended to run the database on a
separate box for large installations.
:::

[comment]: # ({/new-42d1d93e})



[comment]: # ({new-ad0c32cf})
#### Supported platforms

En raison des exigences de sécurité et de la nature critique du serveur
de supervision, UNIX est le seul système d'exploitation capable de
fournir systématiquement les performances, la tolérance aux pannes et la
résilience nécessaires. Zabbix fonctionne sur les meilleures versions du
marché.

Zabbix a été testé sur les plateforme suivantes :

-   Linux
-   IBM AIX
-   FreeBSD
-   NetBSD
-   OpenBSD
-   HP-UX
-   Mac OS X
-   Solaris
-   Windows: toutes les versions bureautiques et serveurs depuis XP
    (agent Zabbix uniquement)

::: noteclassic
Zabbix peut également fonctionner sur d'autres systèmes
d'exploitation de type Unix.
:::

::: noteimportant
Zabbix désactive les core dumps s'ils sont
compilés avec chiffrement et ne démarre pas si le système n'autorise pas
la désactivation des core dumps.
:::

[comment]: # ({/new-ad0c32cf})

[comment]: # ({new-323b6241})
#### Logiciel

Zabbix est construit autour d'un serveur Web Apache moderne, de moteurs
de base de données leaders et d'un langage PHP.

[comment]: # ({/new-323b6241})

[comment]: # ({new-fd454df8})
##### Système de gestion de base de données

|Logiciel|Version|Commentaires|
|--------|-------|------------|
|*MySQL*|5.0.3 - 8.0.x|Obligatoire si MySQL est utilisé comme base de données principale. Le moteur InnoDB est requis.<br>MariaDB fonctionne également avec Zabbix.|
|*Oracle*|10g ou ultérieur|Obligatoire si Oracle est utilisé comme base de données principale.|
|*PostgreSQL*|8.1 ou ultérieur|Obligatoire si PostgreSQL est utilisé comme base de données principale.<br>Il est suggéré d'utiliser au minimum PostgreSQL 8.3, qui [introduit de bien meilleures performances au niveau du VACCUM](http://www.postgresql.org/docs/8.3/static/release-8-3.html).|
|*IBM DB2*|9.7 ou ultérieur|Obligatoire si IBM DB2 est utilisé comme base de données principale.|
|*SQLite*|3.3.5 ultérieur|SQLite est uniquement pris en charge avec les proxys Zabbix. Obligatoire si SQLite est utilisé comme base de données du proxy Zabbix.|

::: noteimportant
Le support de IBM DB2 est expérimental
!
:::

[comment]: # ({/new-fd454df8})

[comment]: # ({new-75f0586e})
##### Interface Web

Les logiciels suivants sont nécessaires pour exécuter l'interface Web
Zabbix :

|Logiciel|Version|Commentaires|
|--------|-------|------------|
|*Apache*|1.3.12 ou ultérieur|<|
|*PHP*|5.4.0 ou ultérieur|<|
|Extensions PHP :|<|<|
|*gd*|2.0 ou ultérieur|L'extension PHP GD doit prendre en charge les images au format PNG. (*--with-png-dir*), JPEG (*--with-jpeg-dir*) et FreeType 2 (*--with-freetype-dir*).|
|*bcmath*|<|php-bcmath (*--enable-bcmath*)|
|*ctype*|<|php-ctype (*--enable-ctype*)|
|*libXML*|2.6.15 ou ultérieur|php-xml or php5-dom, si fourni séparément par le distributeur.|
|*xmlreader*|<|php-xmlreader, si fourni séparément par le distributeur.|
|*xmlwriter*|<|php-xmlwriter, si fourni séparément par le distributeur.|
|*session*|<|php-session, si fourni séparément par le distributeur.|
|*sockets*|<|php-net-socket (*--enable-sockets*). Nécessaire pour la prise en charge des scripts utilisateurs.|
|*mbstring*|<|php-mbstring (*--enable-mbstring*)|
|*gettext*|<|php-gettext (*--with-gettext*). Nécessaire pour que les traductions fonctionnent.|
|*ldap*|<|php-ldap. Nécessaire uniquement si l’authentification LDAP est utilisée sur l’interface utilisateur.|
|*ibm\_db2*|<|Nécessaire si IBM DB2 est utilisée comme base de données principale.|
|*mysqli*|<|Nécessaire si MySQL est utilisée comme base de données principale.|
|*oci8*|<|Nécessaire si Oracle est utilisée comme base de données principale.|
|*pgsql*|<|Nécessaire si PostgreSQL est utilisée comme base de données principale.|

::: noteclassic
Zabbix peut également fonctionner sur les versions
précédentes d'Apache, MySQL, Oracle et PostgreSQL.
:::

::: noteimportant
Pour les autres polices d’écriture que le DejaVu
par défaut, la fonction PHP
[imagerotate](http://php.net/manual/en/function.imagerotate.php) peut
être requise. S'il est manquant, ces polices peuvent être affichées de
façon incorrecte lorsqu'un graphique est affiché. Cette fonction n'est
disponible que si PHP est compilé avec le GD fourni, ce qui n'est pas le
cas dans Debian et d'autres distributions.
:::

[comment]: # ({/new-75f0586e})

[comment]: # ({new-dda86afe})
##### Navigateur côté client

Les cookies et les scripts Java doivent être activés.

Les dernières versions de Google Chrome, Mozilla Firefox, Microsoft
Internet Explorer et Opera sont prises en charge. D'autres navigateurs
(Apple Safari, Konqueror) peuvent également fonctionner avec Zabbix.

::: notewarning
La stratégie d'origine pour les IFrames est
implémentée, ce qui signifie que Zabbix ne peut pas être placé dans des
cadres sur un domaine différent.\
\
Cependant, les pages placées dans un cadre Zabbix auront accès à
l’interface utilisateur Zabbix (via JavaScript) si la page qui est
placée dans le cadre et l’interface utilisateur Zabbix sont sur le même
domaine. Une page comme `http://secure-zabbix.com/cms/page.html`, si
elle est placée dans les écrans ou les tableaux de bord sur
`http://secure-zabbix.com/zabbix/`, aura un accès JavaScript complet à
Zabbix.
:::

[comment]: # ({/new-dda86afe})

[comment]: # ({new-096d332c})
##### Serveur

Les pré-requis obligatoires sont toujours nécessaires. Des pré-requis
facultatifs sont nécessaires pour le support de certaines fonctions
spécifiques.

|Pré-requis|Statut|Description|
|-----------|------|-----------|
|*libpcre*|Obligatoires|La librairie PCRE est obligatoire pour la prise en charge des [expressions régulières Perl](https://en.wikipedia.org/wiki/Perl_Compatible_Regular_Expressions) (PCRE).<br>La dénomination peut différer selon la distribution GNU/Linux, par exemple 'libpcre3' ou 'libpcre1'. Notez que vous avez besoin exactement de PCRE (v8.x); La bibliothèque PCRE2 (v10.x) n'est pas utilisée.|
|*libevent*|^|Requis pour le support des métriques groupées et la surveillance IPMI. Version 1.4 ou supérieure.<br>Notez que pour le proxy Zabbix ce pré-requis est facultatif ; il est seulement nécessaire pour le support de la surveillance IPMI.|
|*OpenIPMI*|Facultatifs|Nécessaire pour la prise en charge IPMI.|
|*libssh2*|^|Nécessaire pour la prise en charge de SSH. Version 1.0 ou supplémentaire.|
|*fping*|^|Nécessaire pour les [éléments ping ICMP](/fr/manual/config/items/itemtypes/simple_checks#icmp_pings).|
|*libcurl*|^|Requis pour la supervision Web, la supervision VMware et l'authentification SMTP. Pour l'authentification SMTP, la version 7.20.0 ou ultérieure est requise. Également requis pour Elasticsearch.|
|*libiksemel*|^|Obligatoire pour la prise en charge de Jabber.|
|*libxml2*|^|Obligatoire pour la supervision VMware.|
|*net-snmp*|^|Obligatoire pour la prise en charge de SNMP.|
|*zlib*|^|Obligatoire pour la prise en charge de la compression.|

[comment]: # ({/new-096d332c})

[comment]: # ({new-800c8308})
##### Passerelle Java

Si vous avez obtenu Zabbix à partir du dépôt ou d'une archive, les
dépendances nécessaires sont déjà incluses dans l'arborescence source.

Si vous avez obtenu Zabbix à partir du package de votre distribution,
les dépendances nécessaires sont déjà fournies par le système de
package.

Dans les deux cas ci-dessus, le logiciel est prêt à être utilisé et
aucun téléchargement supplémentaire n'est nécessaire.

Toutefois, si vous souhaitez fournir vos versions de ces dépendances
(par exemple, si vous préparez un package pour une distribution Linux),
vous trouverez ci-dessous la liste des versions des bibliothèques avec
lesquelles la passerelle Java fonctionne. Zabbix peut aussi fonctionner
avec d'autres versions de ces bibliothèques.

Le tableau suivant répertorie les fichiers JAR actuellement associés à
la passerelle Java dans le code d'origine :

|Bibliothèque|Licence|Site Web|Commentaires|
|-------------|-------|--------|------------|
|*logback-core-0.9.27.jar*|EPL 1.0, LGPL 2.1|<http://logback.qos.ch/>|Testée avec 0.9.27, 1.0.13, et 1.1.1.|
|*logback-classic-0.9.27.jar*|EPL 1.0, LGPL 2.1|<http://logback.qos.ch/>|Testée avec 0.9.27, 1.0.13, et 1.1.1.|
|*slf4j-api-1.6.1.jar*|MIT License|<http://www.slf4j.org/>|Testée avec 1.6.1, 1.6.6, et 1.7.6.|
|*android-json-4.3\_r3.1.jar*|Apache License 2.0|<https://android.googlesource.com/platform/libcore/+/master/json>|Testée avec 2.3.3\_r1.1 et 4.3\_r3.1. Voir src/zabbix\_java/lib/README pour les instructions pour créer un fichier JAR.|

La passerelle Java se compile et s'exécute avec Java 1.6 et ultérieurs.
Il est recommandé pour ceux qui fournissent une version pré-compilee de
la passerelle pour d'autres usages d'utiliser la compilation sous Java
1.6 de sorte qu'elle soit compatible avec toute autre version de Java
jusqu'à la dernière.

[comment]: # ({/new-800c8308})

[comment]: # ({new-4aa86212})
#### Taille de la base de données

Les données de configuration Zabbix nécessitent une quantité d'espace
disque fixe et ne grossissent pas beaucoup.

La taille de la base de données Zabbix dépend principalement des
variables suivantes, qui définissent la quantité de données historiques
stockées :

-   Nombre de valeurs traitées par seconde

C'est le nombre moyen de nouvelles valeurs que le serveur Zabbix reçoit
chaque seconde. Par exemple, si nous avons 3000 éléments à superviser
avec un taux de rafraîchissement de 60 secondes, le nombre de valeurs
par seconde est calculé comme 3000/60 = **50**.

Cela signifie que 50 nouvelles valeurs sont ajoutées à la base de
données Zabbix chaque seconde.

-   Paramètres de nettoyage pour l’historique

Zabbix conserve les valeurs pour une période déterminée, normalement
plusieurs semaines ou mois. Chaque nouvelle valeur nécessite une
certaine quantité d'espace disque pour les données et les index.

Donc, si nous voulons garder 30 jours d'historique et que nous recevons
50 valeurs par seconde, le nombre total de valeurs sera d'environ
(**30**\*24\*3600) \* **50** = 129.600.000, soit environ 130M de
valeurs.

En fonction du moteur de base de données utilisé et du type de valeurs
reçues (flottants, entiers, chaînes, fichiers journaux, etc.), l'espace
disque pour conserver une seule valeur peut varier de 40 octets à des
centaines d'octets. Normalement, il s'agit d'environ 90 octets par
valeur pour les éléments numériques. Dans notre cas, cela signifie que
130M de valeurs nécessiteront 130M \* 90 octets = **10.9 Go** d'espace
disque.

::: noteclassic
La taille des valeurs des éléments texte/log est impossible
à prévoir exactement, mais vous pouvez vous attendre à environ 500
octets par valeur.
:::

-   Paramètres de nettoyage pour les tendances

Zabbix conserve un ensemble de valeurs de 1 heure max/min/moy/count pour
chaque élément dans la table **tendances**. Les données sont utilisées
pour les graphiques de tendance et de longue période. La période d'une
heure ne peut pas être personnalisée.

La base de données Zabbix, en fonction du type de base de données,
nécessite environ 90 octets pour chaque total. Supposons que nous
aimerions conserver les données sur les tendances pendant 5 ans. Les
valeurs pour 3000 articles exigeront 3000 \* 24 \* 365 \* **90** = **2.2
Go** par an, ou **11 Go** pour 5 années.

-   Paramètres de nettoyage pour les événements

Chaque événement Zabbix nécessite environ 170 octets d'espace disque. Il
est difficile d'estimer le nombre d'événements générés quotidiennement
par Zabbix. Dans le pire des cas, nous pouvons supposer que Zabbix
génère un événement par seconde.

Cela signifie que si nous voulons garder 3 ans d'événements, cela
nécessitera **3**\*365\*24\*3600\* **170** = **15 Go**

Le tableau suivant contient des formules qui peuvent être utilisées pour
calculer l'espace disque requis pour le système Zabbix :

|Paramètre|Formule pour l'espace disque nécessaire (en octets)|
|----------|----------------------------------------------------|
|*Configuration Zabbix*|Taille fixe. Environ 10Mo ou moins.|
|*Historique*|jours\*(éléments/taux de rafraîchissement)\*24\*3600\*octets<br>éléments : nombre d'éléments<br>jours : nombre de jours d'historique à garder<br>taux de rafraîchissement : moyenne de rafraîchissement des éléments<br>octets : nombre d'octets nécessaires pour garder une seule valeur, dépend du moteur de base de données, généralement \~90 octets.|
|*Tendances*|jours\*(élémentss/3600)\*24\*3600\*octets<br>éléments : nombre d'éléments<br>jours : nombre de jours à garder<br>octets : nombre d'octets nécessaires pour garder une seule tendance, dépend du moteur de base de données, généralement \~90 octets.|
|*événements*|jours\*événements\*24\*3600\*octets<br>événements : nombre d'événement par seconde. Un (1) événement par seconde dans le pire scenario.<br>jours : nombre de jour à garder<br>octets : nombre d'octets nécessaire pour garder une seule tendance, dépend du moteur de base de données, généralement \~170 octets.|

::: noteclassic
Les valeurs moyenne comme : \~90 octets pour les éléments
numériques, \~170 octets pour les événements ont été collectées à partir
de statistiques réelles en utilisant une base de données
MySQL.
:::

Donc, l'espace disque total nécessaire peut être calculé de la manière
suivante :\
**Configuration + Historique + Tendances + Événements**\
L'espace disque ne sera pas immédiatement utilisé après l'installation
de Zabbix. La base de données va grossir et arrêtera de grossir à un
certain moment, qui dépendra des paramètres de nettoyage.

[comment]: # ({/new-4aa86212})

[comment]: # ({new-149075a3})
#### Synchronisation de l'heure

Il est très important d'avoir une date précise sur le serveur qui
exécute Zabbix. [ntpd](http://www.ntp.org/) est le démon le plus
populaire qui synchronise l'heure de l'hôte avec l'heure des autres
machines. Il est fortement recommandé de maintenir la date du système
synchronisée sur tous les systèmes où des composants Zabbix s'éxécutent.

Si l'heure n'est pas synchronisée, Zabbix convertira les horodatages des
données collectées en heure serveur Zabbix en prenant des horodatages
client/serveur après l'établissement de la connexion de données et en
ajustant les horodatages de la valeur d'élément reçue par le décalage
client-serveur. Pour rester simple et éviter les complications
possibles, la latence de la connexion est ignorée. Pour cette raison, la
latence de connexion est ajoutée aux horodatages des données acquises à
partir des connexions actives (agent actif, proxy actif, sender) et
soustraites des horodatages des données acquises à partir des connexions
passives (proxy passif). Toutes les autres vérifications sont effectuées
au moment du serveur et leurs horodatages ne sont pas ajustés.

[comment]: # ({/new-149075a3})

[comment]: # ({new-c8024c35})

#### Default port numbers

The following list of open ports per component is applicable for default configuration:

|Zabbix component|Port number|Protocol|Type of connection|
|-------|-------|-------|-------|
|Zabbix agent|10050|TCP|on demand|
|Zabbix agent 2|10050|TCP|on demand|
|Zabbix server|10051|TCP|on demand|
|Zabbix proxy|10051|TCP|on demand|
|Zabbix Java gateway|10052|TCP|on demand|
|Zabbix web service|10053|TCP|on demand|
|Zabbix frontend|80|HTTP|on demand|
|^|443|HTTPS|on demand|
|Zabbix trapper|10051 |TCP| on demand|

::: noteclassic
The port numbers should be open in firewall to enable Zabbix communications. Outgoing TCP connections usually do not require explicit firewall settings.
::: 

[comment]: # ({/new-c8024c35})


[comment]: # ({new-1d73b238})
#### Database size

Zabbix configuration data require a fixed amount of disk space and do
not grow much.

Zabbix database size mainly depends on these variables, which define the
amount of stored historical data:

-   Number of processed values per second

This is the average number of new values Zabbix server receives every
second. For example, if we have 3000 items for monitoring with a refresh
rate of 60 seconds, the number of values per second is calculated as
3000/60 = **50**.

It means that 50 new values are added to Zabbix database every second.

-   Housekeeper settings for history

Zabbix keeps values for a fixed period of time, normally several weeks
or months. Each new value requires a certain amount of disk space for
data and index.

So, if we would like to keep 30 days of history and we receive 50 values
per second, the total number of values will be around
(**30**\*24\*3600)\* **50** = 129.600.000, or about 130M of values.

Depending on the database engine used, type of received values (floats,
integers, strings, log files, etc), the disk space for keeping a single
value may vary from 40 bytes to hundreds of bytes. Normally it is around
90 bytes per value for numeric items^**2**^. In our case, it means that
130M of values will require 130M \* 90 bytes = **10.9GB** of disk space.

::: noteclassic
The size of text/log item values is impossible to predict
exactly, but you may expect around 500 bytes per value.
:::

-   Housekeeper setting for trends

Zabbix keeps a 1-hour max/min/avg/count set of values for each item in
the table **trends**. The data is used for trending and long period
graphs. The one hour period can not be customized.

Zabbix database, depending on the database type, requires about 90 bytes
per each total. Suppose we would like to keep trend data for 5 years.
Values for 3000 items will require 3000\*24\*365\* **90** = **2.2GB**
per year, or **11GB** for 5 years.

-   Housekeeper settings for events

Each Zabbix event requires approximately 250 bytes of disk space^**1**^.
It is hard to estimate the number of events generated by Zabbix daily.
In the worst-case scenario, we may assume that Zabbix generates one
event per second.

For each recovered event, an event\_recovery record is created. Normally
most of the events will be recovered so we can assume one
event\_recovery record per event. That means additional 80 bytes per
event.

Optionally events can have tags, each tag record requiring approximately
100 bytes of disk space^**1**^. The number of tags per event (\#tags)
depends on configuration. So each will need an additional \#tags \* 100
bytes of disk space.

It means that if we want to keep 3 years of events, this would require
3\*365\*24\*3600\* (250+80+\#tags\*100) = **\~30GB**+\#tags\*100B disk
space^**2**^.

::: noteclassic
 ^**1**^ More when having non-ASCII event names, tags and
values.

^**2**^ The size approximations are based on MySQL and might be
different for other databases. 
:::

The table contains formulas that can be used to calculate the disk space
required for Zabbix system:

|Parameter|Formula for required disk space (in bytes)|
|---------|------------------------------------------|
|*Zabbix configuration*|Fixed size. Normally 10MB or less.|
|*History*|days\*(items/refresh rate)\*24\*3600\*bytes<br>items : number of items<br>days : number of days to keep history<br>refresh rate : average refresh rate of items<br>bytes : number of bytes required to keep single value, depends on database engine, normally \~90 bytes.|
|*Trends*|days\*(items/3600)\*24\*3600\*bytes<br>items : number of items<br>days : number of days to keep history<br>bytes : number of bytes required to keep single trend, depends on the database engine, normally \~90 bytes.|
|*Events*|days\*events\*24\*3600\*bytes<br>events : number of event per second. One (1) event per second in worst-case scenario.<br>days : number of days to keep history<br>bytes : number of bytes required to keep single trend, depends on the database engine, normally \~330 + average number of tags per event \* 100 bytes.|

So, the total required disk space can be calculated as:\
**Configuration + History + Trends + Events**\
The disk space will NOT be used immediately after Zabbix installation.
Database size will grow then it will stop growing at some point, which
depends on housekeeper settings.

[comment]: # ({/new-1d73b238})

[comment]: # ({new-520ea0fa})
#### Time synchronization

It is very important to have precise system time on the server with
Zabbix running. [ntpd](http://www.ntp.org/) is the most popular daemon
that synchronizes the host's time with the time of other machines. It's
strongly recommended to maintain synchronized system time on all systems
Zabbix components are running on.

[comment]: # ({/new-520ea0fa})

[comment]: # ({new-9e23ba76})

#### Network requirements

A following list of open ports per component is applicable for default configuration.


|Port|Components |
|------------|---------------|
|Frontend|http on 80, https on 443|
|Server|10051 (for use with active proxy/agents)|
|Active Proxy |10051 ( not sure about fetching config)|
|Passive Proxy|10051|
|Agent2|10050|
|Trapper| |
|JavaGateway|10053|
|WebService|10053|

::: noteclassic
In case the configuration is completed, the port numbers should be edited accordingly to prevent firewall from blocking communications with these ports.Outgoing TCP connections usually do not require explicit firewall settings.
::: 

[comment]: # ({/new-9e23ba76})
