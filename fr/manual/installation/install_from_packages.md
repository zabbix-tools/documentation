[comment]: # translation:outdated

[comment]: # ({new-e2c1904c})
# 4 Installation depuis les packages

[comment]: # ({/new-e2c1904c})

[comment]: # ({new-8d855b7c})
#### Depuis les packages de distribution

Plusieurs distributions populaires d'OS disposent des packages Zabbix.
Vous pouvez utiliser ces packages pour installer Zabbix.

::: noteclassic
La dernière version de Zabbix peut être absente du dépôt de
certaines distributions d’OS.
:::

#### Depuis le dépôt officiel Zabbix

Zabbix SIA fournis les packages RPM et DEB officiels pour :

-   [Red Hat Enterprise
    Linux/CentOS](/fr/manual/installation/install_from_packages/rhel_centos)
-   [Debian/Ubuntu](/fr/manual/installation/install_from_packages/debian_ubuntu)

Les packages sont disponibles sur
[repo.zabbix.com](http://repo.zabbix.com/). Les dépôts yum et apt sont
également disponibles sur le serveur. Un didacticiel étape par étape
pour l'installation de Zabbix à partir de packages est fourni dans les
sous-pages de cette section.

[comment]: # ({/new-8d855b7c})
