<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="fr" datatype="plaintext" original="manual/installation/containers.md">
    <body>
      <trans-unit id="e62e1497" xml:space="preserve">
        <source># 5 Installation from containers</source>
      </trans-unit>
      <trans-unit id="a3ac452e" xml:space="preserve">
        <source>
### Overview

This section describes how to deploy Zabbix with [Docker](#docker) or [Docker Compose](#docker-compose).

Zabbix officially provides:

- Separate Docker images for each Zabbix
component to run as portable and self-sufficient containers.
- Compose files for defining and running
multi-container Zabbix components in Docker.


::: noteimportant
Since Zabbix 6.0, deterministic triggers need to be created during the installation.
If binary logging is enabled for MySQL/MariaDB, this requires superuser privileges or setting the variable/configuration parameter  *log_bin_trust_function_creators = 1*. See [Database creation scripts](/manual/appendix/install/db_scripts#mysql) for instructions how to set the variable.\
Note that if executing from a console, the variable will only be set temporarily and will be dropped when a Docker is restarted.
In this case, keep your SQL service running, only stop zabbix-server service by running 'docker compose down zabbix-server' and then 'docker compose up -d zabbix-server'.\
Alternatively, you can set this variable in the configuration file.
:::</source>
      </trans-unit>
      <trans-unit id="f9acea87" xml:space="preserve">
        <source>### Docker


Zabbix provides images based on a variety of OS base
images. To get the list of supported base operating system images for a specific Zabbix component, see the component's description in [Docker Hub](https://hub.docker.com/u/zabbix). All Zabbix images are configured 
to rebuild latest images if base images are updated.

#### Installation

To get Zabbix component image, run:

    docker pull zabbix/zabbix-server-mysql

Replace `zabbix/zabbix-server-mysql` with the name of the required docker repository. 

This command will pull the latest stable Zabbix component version based on the Alpine Linux OS. You can append [tags](#tags) to the repository name to get an image based on another operating system or of the specific Zabbix major or minor version.

The following repositories are available in Docker Hub: 

|Component|&lt;|Docker repository|
|-|---------|----------|
|*Zabbix agent*|&lt;|[zabbix/zabbix-agent](https://hub.docker.com/r/zabbix/zabbix-agent/)|
|*Zabbix server*|&lt;|&lt;|
| |with MySQL support |[zabbix/zabbix-server-mysql](https://hub.docker.com/r/zabbix/zabbix-server-mysql/)|
|^|with PostgreSQL support |[zabbix/zabbix-server-pgsql](https://hub.docker.com/r/zabbix/zabbix-server-pgsql/)|
|*Zabbix web interface* |&lt;|&lt;|
| |based on Apache2 web server with MySQL support|[zabbix/zabbix-web-apache-mysql](https://hub.docker.com/r/zabbix/zabbix-web-apache-mysql/)|
|^|based on Apache2 web server with PostgreSQL support|[zabbix/zabbix-web-apache-pgsql](https://hub.docker.com/r/zabbix/zabbix-web-apache-pgsql/)|
|^|based on Nginx web server with MySQL support |[zabbix/zabbix-web-nginx-mysql](https://hub.docker.com/r/zabbix/zabbix-web-nginx-mysql/)|
|^|based on Nginx web server with PostgreSQL support |[zabbix/zabbix-web-nginx-pgsql](https://hub.docker.com/r/zabbix/zabbix-web-nginx-pgsql/)|
|*Zabbix proxy* |&lt;|&lt;|
| |with SQLite3 support |[zabbix/zabbix-proxy-sqlite3](https://hub.docker.com/r/zabbix/zabbix-proxy-sqlite3/)|
|^|with MySQL support |[zabbix/zabbix-proxy-mysql](https://hub.docker.com/r/zabbix/zabbix-proxy-mysql/)|
|*Zabbix Java gateway* |&lt;|[zabbix/zabbix-java-gateway](https://hub.docker.com/r/zabbix/zabbix-java-gateway/)|

::: noteclassic
SNMP trap support is provided in a separate
repository [zabbix/zabbix-snmptraps](https://hub.docker.com/r/zabbix/zabbix-snmptraps/). It can be linked with Zabbix server and
Zabbix proxy.
:::</source>
      </trans-unit>
      <trans-unit id="e498c2aa" xml:space="preserve">
        <source>
#### Tags

Official Zabbix component images may contain the following tags:

|Tag|Description|Example|
|--|----------|----|
|latest            | The latest stable version of a Zabbix component based on Alpine Linux image. |zabbix-agent:latest |
|&lt;OS&gt;-trunk | The latest nightly build of the Zabbix version that is currently being developed on a specific operating system. &lt;br&gt; &lt;br&gt; **&lt;OS&gt;** - the base operating system. Supported values: &lt;br&gt; *alpine* - Alpine Linux; &lt;br&gt; *ltsc2019* -  Windows 10 LTSC 2019 (agent only); &lt;br&gt; *ol* - Oracle Linux; &lt;br&gt; *ltsc2022* - Windows 11 LTSC 2022 (agent only); &lt;br&gt; *ubuntu* - Ubuntu  |zabbix agent:ubuntu-trunk |
|&lt;OS&gt;-latest     | The latest stable version of a Zabbix component on a specific operating system. &lt;br&gt; &lt;br&gt; **&lt;OS&gt;** - the base operating system. Supported values:  &lt;br&gt; *alpine* - Alpine Linux; &lt;br&gt; *ltsc2019* -  Windows 10 LTSC 2019 (agent only); &lt;br&gt; *ol* - Oracle Linux; &lt;br&gt; *ltsc2022* - Windows 11 LTSC 2022 (agent only); &lt;br&gt; *ubuntu* - Ubuntu|zabbix-agent:ol-latest |
|&lt;OS&gt;-X.X-latest |The latest minor version of a Zabbix component of a specific major version and operating system. &lt;br&gt; &lt;br&gt; **&lt;OS&gt;** - the base operating system. Supported values:  &lt;br&gt; *alpine* - Alpine Linux;  &lt;br&gt; *ltsc2019* -  Windows 10 LTSC 2019 (agent only); &lt;br&gt; *ol* - Oracle Linux; &lt;br&gt; *ltsc2022* - Windows 11 LTSC 2022 (agent only); &lt;br&gt; *ubuntu* - Ubuntu&lt;br&gt;&lt;br&gt;**X.X** - the major Zabbix version (supported: *4.0*, *5.0*, *6.0*, *6.2*). |zabbix-agent:alpine-6.0-latest |
|&lt;OS&gt;-X.X.*      |The latest minor version of a Zabbix component of a specific major version and operating system. &lt;br&gt; &lt;br&gt; **&lt;OS&gt;** - the base operating system. Supported values:  &lt;br&gt; *alpine* - Alpine Linux;  &lt;br&gt; *ltsc2019* -  Windows 10 LTSC 2019 (agent only); &lt;br&gt; *ol* - Oracle Linux; &lt;br&gt; *ltsc2022* - Windows 11 LTSC 2022 (agent only); &lt;br&gt; *ubuntu* - Ubuntu&lt;br&gt;&lt;br&gt;**X.X** - the major Zabbix version (supported: *4.0*, *5.0*, *6.0*, *6.2*). &lt;br&gt;&lt;br&gt; **\*** - the Zabbix minor version  |zabbix-agent:alpine-6.4.1|</source>
      </trans-unit>
      <trans-unit id="fba82ded" xml:space="preserve">
        <source>#### Source files

Docker file sources are stored in the Zabbix [official repository](https://github.com/zabbix/zabbix-docker) on GitHub,
where you can follow latest file changes or fork the project to make your own images.</source>
      </trans-unit>
      <trans-unit id="0b4b68c7" xml:space="preserve">
        <source>#### Initial configuration

After downloading the images, start the containers by executing `docker run` command followed by additional arguments to specify required [environment variables](#environment-variables) and/or [mount points](#volumes). Some [configuration examples](#examples) are provided below. 

::: noteclassic
To enable communication between Zabbix components, some ports, such as 10051/TCP for Zabbix server (trapper), 10050/TCP for Zabbix agent, 162/UDP for SNMP traps and 80/TCP for Zabbix web interface will be exposed to a host machine. Full list of default ports used by Zabbix components is available on the [Requirements](/manual/installation/requirements#default-port-numbers) page. For Zabbix server and agent the default port can be changed by setting ZBX_LISTENPORT [environment variable](#environment-variables). 
:::</source>
      </trans-unit>
      <trans-unit id="de9f41d4" xml:space="preserve">
        <source>##### Environment variables

All Zabbix component images provide environment variables to control
configuration. Supported environment variables are listed in the component
repository. 

These environment variables are options from Zabbix
configuration files, but with different naming method. For example,
`ZBX_LOGSLOWQUERIES` is equal to `LogSlowQueries` from Zabbix [server](/manual/appendix/config/zabbix_server) and
Zabbix [proxy](/manual/appendix/config/zabbix_server) configuration files.

::: noteimportant
 Some of configuration options cannot be
changed. For example, `PIDFile` and `LogType`.
:::

The following environment variables are specific to Docker components and do not
exist in Zabbix configuration files:

|Variable|Components|Default value |Description|
|--|--|--|------|
|`DB_SERVER_HOST`|Server&lt;br&gt;Proxy&lt;br&gt;Web interface|`mysql-server` for MYSQL &lt;br&gt; `postgres-server` for PostgreSQL | IP or DNS name of MySQL or PostgreSQL server.|
|`DB_SERVER_PORT`|Server&lt;br&gt;Proxy&lt;br&gt;Web interface|`3306` for MYSQL &lt;br&gt; `5432` for PostgreSQL |Port of MySQL or PostgreSQL server.|
|`MYSQL_USER`|Server&lt;br&gt;Proxy&lt;br&gt;Web-interface| `zabbix` |  MySQL database user.|
|`MYSQL_PASSWORD`|Server&lt;br&gt;Proxy&lt;br&gt;Web interface|`zabbix` |MySQL database password.|
|`MYSQL_DATABASE`|Server&lt;br&gt;Proxy&lt;br&gt;Web interface|`zabbix` for Zabbix server &lt;br&gt; 'zabbix\_proxy' for Zabbix proxy |Zabbix database name.|
|`POSTGRES_USER`|Server&lt;br&gt;Web interface|`zabbix` |PostgreSQL database user.|
|`POSTGRES_PASSWORD`|Server&lt;br&gt;Web interface|`zabbix` |PostgreSQL database password.|
|`POSTGRES_DB`|Server&lt;br&gt;Web interface|`zabbix` for Zabbix server &lt;br&gt; 'zabbix\_proxy' for Zabbix proxy |Zabbix database name.|
|`PHP_TZ`|Web-interface|`Europe/Riga`|Timezone in PHP format. Full list of supported timezones is available on [php.net](http://php.net/manual/en/timezones.php).|
|`ZBX_SERVER_NAME`|Web interface|`Zabbix Docker`|Visible Zabbix installation name in right top corner of the web interface. |
|`ZBX_JAVAGATEWAY_ENABLE`|Server&lt;br&gt;Proxy|`false`|Enables communication with Zabbix Java gateway to collect Java related checks.|
|`ZBX_ENABLE_SNMP_TRAPS`|Server&lt;br&gt;Proxy| |Enables SNMP trap feature. It requires **zabbix-snmptraps** instance and shared volume */var/lib/zabbix/snmptraps* to Zabbix server or Zabbix proxy.|</source>
      </trans-unit>
      <trans-unit id="7d10f7dc" xml:space="preserve">
        <source>##### Volumes

The images allow to mount volumes using the following mount points:

|Volume|&lt;|Description|
|-|---------|----------------------------------------------|
|**Zabbix agent**|&lt;|&lt;|
| |*/etc/zabbix/zabbix\_agentd.d*|The volume allows to include *\*.conf* files and extend Zabbix agent using the `UserParameter` feature|
|^|*/var/lib/zabbix/modules*|The volume allows to load additional modules and extend Zabbix agent using the [LoadModule](/manual/extensions/loadablemodules) feature|
|^|*/var/lib/zabbix/enc*|The volume is used to store TLS-related files. These file names are specified using `ZBX_TLSCAFILE`, `ZBX_TLSCRLFILE`, `ZBX_TLSKEY_FILE` and `ZBX_TLSPSKFILE` environment variables|
|**Zabbix server**|&lt;|&lt;|
| |*/usr/lib/zabbix/alertscripts*|The volume is used for custom alert scripts. It is the `AlertScriptsPath` parameter in [zabbix\_server.conf](/manual/appendix/config/zabbix_server)|
|^|*/usr/lib/zabbix/externalscripts*|The volume is used by [external checks](/manual/config/items/itemtypes/external). It is the `ExternalScripts` parameter in [zabbix\_server.conf](/manual/appendix/config/zabbix_server)|
|^|*/var/lib/zabbix/modules*|The volume allows to load additional modules and extend Zabbix server using the [LoadModule](/manual/extensions/loadablemodules) feature|
|^|*/var/lib/zabbix/enc*|The volume is used to store TLS related files. These file names are specified using `ZBX_TLSCAFILE`, `ZBX_TLSCRLFILE`, `ZBX_TLSKEY_FILE` and `ZBX_TLSPSKFILE` environment variables|
|^|*/var/lib/zabbix/ssl/certs*|The volume is used as location of SSL client certificate files for client authentication. It is the `SSLCertLocation` parameter in zabbix\_server.conf|
|^|*/var/lib/zabbix/ssl/keys*|The volume is used as location of SSL private key files for client authentication. It is the `SSLKeyLocation` parameter in [zabbix\_server.conf](/manual/appendix/config/zabbix_server)|
|^|*/var/lib/zabbix/ssl/ssl\_ca*|The volume is used as location of certificate authority (CA) files for SSL server certificate verification. It is the `SSLCALocation` parameter in [zabbix\_server.conf](/manual/appendix/config/zabbix_server)|
|^|*/var/lib/zabbix/snmptraps*|The volume is used as location of snmptraps.log file. It could be shared by zabbix-snmptraps container and inherited using the volumes\_from Docker option while creating a new instance of Zabbix server. SNMP trap processing feature could be enabled by using shared volume and switching the `ZBX_ENABLE_SNMP_TRAPS` environment variable to 'true'|
|^|*/var/lib/zabbix/mibs*|The volume allows to add new MIB files. It does not support subdirectories, all MIBs must be placed in `/var/lib/zabbix/mibs`|
|**Zabbix proxy**|&lt;|&lt;|
| |*/usr/lib/zabbix/externalscripts*|The volume is used by [external checks](/manual/config/items/itemtypes/external). It is the `ExternalScripts` parameter in [zabbix\_proxy.conf](/manual/appendix/config/zabbix_proxy)|
|^|*/var/lib/zabbix/db_data/*| The volume allows to store database files on external devices. Supported only for Zabbix proxy with SQLite3|
|^|*/var/lib/zabbix/modules*|The volume allows to load additional modules and extend Zabbix server using the [LoadModule](/manual/extensions/loadablemodules) feature|
|^|*/var/lib/zabbix/enc*|The volume is used to store TLS related files. These file names are specified using `ZBX_TLSCAFILE`, `ZBX_TLSCRLFILE`, `ZBX_TLSKEY_FILE` and `ZBX_TLSPSKFILE` environment variables|
|^|*/var/lib/zabbix/ssl/certs*|The volume is used as location of SSL client certificate files for client authentication. It is the `SSLCertLocation` parameter in [zabbix\_proxy.conf](/manual/appendix/config/zabbix_proxy)|
|^|*/var/lib/zabbix/ssl/keys*|The volume is used as location of SSL private key files for client authentication. It is the `SSLKeyLocation` parameter in [zabbix\_proxy.conf](/manual/appendix/config/zabbix_proxy)|
|^|*/var/lib/zabbix/ssl/ssl\_ca*|The volume is used as location of certificate authority (CA) files for SSL server certificate verification. It is the `SSLCALocation` parameter in [zabbix\_proxy.conf](/manual/appendix/config/zabbix_proxy)|
|^|*/var/lib/zabbix/snmptraps*|The volume is used as location of snmptraps.log file. It could be shared by the zabbix-snmptraps container and inherited using the volumes\_from Docker option while creating a new instance of Zabbix server. SNMP trap processing feature could be enabled by using shared volume and switching the `ZBX_ENABLE_SNMP_TRAPS` environment variable to 'true'|
|^|*/var/lib/zabbix/mibs*|The volume allows to add new MIB files. It does not support subdirectories, all MIBs must be placed in `/var/lib/zabbix/mibs`|
|**Zabbix web interface based on Apache2 web server**|&lt;|&lt;|
| |*/etc/ssl/apache2*|The volume allows to enable HTTPS for Zabbix web interface. The volume must contain the two `ssl.crt` and `ssl.key` files prepared for Apache2 SSL connections|
|**Zabbix web interface based on Nginx web server**|&lt;|&lt;|
| |*/etc/ssl/nginx*|The volume allows to enable HTTPS for Zabbix web interface. The volume must contain the two `ssl.crt`, `ssl.key` files and `dhparam.pem` prepared for Nginx SSL connections|
|**Zabbix snmptraps**|&lt;|&lt;|
| |*/var/lib/zabbix/snmptraps*|The volume contains the `snmptraps.log` log file named with received SNMP traps|
|^|*/var/lib/zabbix/mibs*|The volume allows to add new MIB files. It does not support subdirectories, all MIBs must be placed in `/var/lib/zabbix/mibs`|

For additional information, see Zabbix official repositories in Docker
Hub.</source>
      </trans-unit>
      <trans-unit id="492bd3ba" xml:space="preserve">
        <source>##### Examples

**Example 1**

The example demonstrates how to run Zabbix server with MySQL database
support, Zabbix web interface based on the Nginx web server and Zabbix
Java gateway.

1\. Create network dedicated for Zabbix component containers:

    docker network create --subnet 172.20.0.0/16 --ip-range 172.20.240.0/20 zabbix-net

2\. Start empty MySQL server instance

    docker run --name mysql-server -t \
          -e MYSQL_DATABASE="zabbix" \
          -e MYSQL_USER="zabbix" \
          -e MYSQL_PASSWORD="zabbix_pwd" \
          -e MYSQL_ROOT_PASSWORD="root_pwd" \
          --network=zabbix-net \
          --restart unless-stopped \
          -d mysql:8.0-oracle \
          --character-set-server=utf8 --collation-server=utf8_bin \
          --default-authentication-plugin=mysql_native_password

3\. Start Zabbix Java gateway instance

    docker run --name zabbix-java-gateway -t \
          --network=zabbix-net \
          --restart unless-stopped \
          -d zabbix/zabbix-java-gateway:alpine-6.4-latest

4\. Start Zabbix server instance and link the instance with created
MySQL server instance

    docker run --name zabbix-server-mysql -t \
          -e DB_SERVER_HOST="mysql-server" \
          -e MYSQL_DATABASE="zabbix" \
          -e MYSQL_USER="zabbix" \
          -e MYSQL_PASSWORD="zabbix_pwd" \
          -e MYSQL_ROOT_PASSWORD="root_pwd" \
          -e ZBX_JAVAGATEWAY="zabbix-java-gateway" \
          --network=zabbix-net \
          -p 10051:10051 \
          --restart unless-stopped \
          -d zabbix/zabbix-server-mysql:alpine-6.4-latest

5\. Start Zabbix web interface and link the instance with created MySQL
server and Zabbix server instances

    docker run --name zabbix-web-nginx-mysql -t \
          -e ZBX_SERVER_HOST="zabbix-server-mysql" \
          -e DB_SERVER_HOST="mysql-server" \
          -e MYSQL_DATABASE="zabbix" \
          -e MYSQL_USER="zabbix" \
          -e MYSQL_PASSWORD="zabbix_pwd" \
          -e MYSQL_ROOT_PASSWORD="root_pwd" \
          --network=zabbix-net \
          -p 80:8080 \
          --restart unless-stopped \
          -d zabbix/zabbix-web-nginx-mysql:alpine-6.4-latest



**Example 2**

The example demonstrates how to run Zabbix server with PostgreSQL
database support, Zabbix web interface based on the Nginx web server and
SNMP trap feature.

1\. Create network dedicated for Zabbix component containers:

    docker network create --subnet 172.20.0.0/16 --ip-range 172.20.240.0/20 zabbix-net

2\. Start empty PostgreSQL server instance

    docker run --name postgres-server -t \
          -e POSTGRES_USER="zabbix" \
          -e POSTGRES_PASSWORD="zabbix_pwd" \
          -e POSTGRES_DB="zabbix" \
          --network=zabbix-net \
          --restart unless-stopped \
          -d postgres:latest

3\. Start Zabbix snmptraps instance

    docker run --name zabbix-snmptraps -t \
          -v /zbx_instance/snmptraps:/var/lib/zabbix/snmptraps:rw \
          -v /var/lib/zabbix/mibs:/usr/share/snmp/mibs:ro \
          --network=zabbix-net \
          -p 162:1162/udp \
          --restart unless-stopped \
          -d zabbix/zabbix-snmptraps:alpine-6.4-latest


4\. Start Zabbix server instance and link the instance with created
PostgreSQL server instance

    docker run --name zabbix-server-pgsql -t \
          -e DB_SERVER_HOST="postgres-server" \
          -e POSTGRES_USER="zabbix" \
          -e POSTGRES_PASSWORD="zabbix_pwd" \
          -e POSTGRES_DB="zabbix" \
          -e ZBX_ENABLE_SNMP_TRAPS="true" \
          --network=zabbix-net \
          -p 10051:10051 \
          --volumes-from zabbix-snmptraps \
          --restart unless-stopped \
          -d zabbix/zabbix-server-pgsql:alpine-6.4-latest


5\. Start Zabbix web interface and link the instance with created
PostgreSQL server and Zabbix server instances

    docker run --name zabbix-web-nginx-pgsql -t \
          -e ZBX_SERVER_HOST="zabbix-server-pgsql" \
          -e DB_SERVER_HOST="postgres-server" \
          -e POSTGRES_USER="zabbix" \
          -e POSTGRES_PASSWORD="zabbix_pwd" \
          -e POSTGRES_DB="zabbix" \
          --network=zabbix-net \
          -p 443:8443 \
          -p 80:8080 \
          -v /etc/ssl/nginx:/etc/ssl/nginx:ro \
          --restart unless-stopped \
          -d zabbix/zabbix-web-nginx-pgsql:alpine-6.4-latest


**Example 3**

The example demonstrates how to run Zabbix server with MySQL database support,
Zabbix web interface based on the Nginx web server and Zabbix Java gateway using `podman` on Red Hat 8.

1\. Create new pod with name `zabbix` and exposed ports (web-interface,
Zabbix server trapper):

    podman pod create --name zabbix -p 80:8080 -p 10051:10051

2\. (optional) Start Zabbix agent container in `zabbix` pod location:

    podman run --name zabbix-agent \
        -e ZBX_SERVER_HOST="127.0.0.1,localhost" \
        --restart=always \
        --pod=zabbix \
        -d registry.connect.redhat.com/zabbix/zabbix-agent-70:latest

3\. Create `./mysql/` directory on host and start Oracle MySQL server
8.0:

    podman run --name mysql-server -t \
          -e MYSQL_DATABASE="zabbix" \
          -e MYSQL_USER="zabbix" \
          -e MYSQL_PASSWORD="zabbix_pwd" \
          -e MYSQL_ROOT_PASSWORD="root_pwd" \
          -v ./mysql/:/var/lib/mysql/:Z \
          --restart=always \
          --pod=zabbix \
          -d mysql:8.0 \
          --character-set-server=utf8 --collation-server=utf8_bin \
          --default-authentication-plugin=mysql_native_password

4\. Start Zabbix server container:

    podman run --name zabbix-server-mysql -t \
                      -e DB_SERVER_HOST="127.0.0.1" \
                      -e MYSQL_DATABASE="zabbix" \
                      -e MYSQL_USER="zabbix" \
                      -e MYSQL_PASSWORD="zabbix_pwd" \
                      -e MYSQL_ROOT_PASSWORD="root_pwd" \
                      -e ZBX_JAVAGATEWAY="127.0.0.1" \
                      --restart=always \
                      --pod=zabbix \
                      -d registry.connect.redhat.com/zabbix/zabbix-server-mysql-50

5\. Start Zabbix Java Gateway container:

    podman run --name zabbix-java-gateway -t \
          --restart=always \
          --pod=zabbix \
          -d registry.connect.redhat.com/zabbix/zabbix-java-gateway-50

6\. Start Zabbix web-interface container:

    podman run --name zabbix-web-mysql -t \
                      -e ZBX_SERVER_HOST="127.0.0.1" \
                      -e DB_SERVER_HOST="127.0.0.1" \
                      -e MYSQL_DATABASE="zabbix" \
                      -e MYSQL_USER="zabbix" \
                      -e MYSQL_PASSWORD="zabbix_pwd" \
                      -e MYSQL_ROOT_PASSWORD="root_pwd" \
                      --restart=always \
                      --pod=zabbix \
                      -d registry.connect.redhat.com/zabbix/zabbix-web-mysql-50

::: noteclassic
Pod `zabbix` exposes 80/TCP port (HTTP) to host machine from
8080/TCP of `zabbix-web-mysql` container.
:::</source>
      </trans-unit>
      <trans-unit id="c443c22e" xml:space="preserve">
        <source>### Docker Compose
Alternatively, Zabbix can be installed using Docker Compose plugin.
Compose files for defining and running multi-container Zabbix components are
available in the official [Zabbix Docker repository](https://github.com/zabbix/zabbix-docker) on GitHub.

::: noteimportant
Official Zabbix compose files support version 3 of Docker Compose.
:::

These compose files are added as examples; they are overloaded.
For example, they contain proxies with both MySQL and SQLite3 support.

To obtain Docker compose files provided by Zabbix, clone the repository:

    git clone https://github.com/zabbix/zabbix-docker.git

Switch to the required version: 

    git checkout 6.4

Compose configuration files and create and start containers:

    docker compose -f ./docker-compose_v3_alpine_mysql_latest.yaml up

Replace `docker-compose_v3_alpine_mysql_latest.yaml` in the command above with the required configuration file.

The following options are available:

|File name|Description|
|--|--------|
|`docker-compose_v3_alpine_mysql_latest.yaml`|The compose file runs the latest version of Zabbix 6.4 components on Alpine Linux with MySQL database support.|
|`docker-compose_v3_alpine_mysql_local.yaml`|The compose file locally builds the latest version of Zabbix 6.4 and runs Zabbix components on Alpine Linux with MySQL database support.|
|`docker-compose_v3_alpine_pgsql_latest.yaml`|The compose file runs the latest version of Zabbix 6.4 components on Alpine Linux with PostgreSQL database support.|
|`docker-compose_v3_alpine_pgsql_local.yaml`|The compose file locally builds the latest version of Zabbix 6.4 and runs Zabbix components on Alpine Linux with PostgreSQL database support.|
|`docker-compose_v3_ol_mysql_latest.yaml`|The compose file runs the latest version of Zabbix 6.4 components on Oracle Linux with MySQL database support.|
|`docker-compose_v3_ol_mysql_local.yaml`|The compose file locally builds the latest version of Zabbix 6.4 and runs Zabbix components on Oracle Linux with MySQL database support.|
|`docker-compose_v3_ol_pgsql_latest.yaml`|The compose file runs the latest version of Zabbix 6.4 components on Oracle Linux with PostgreSQL database support.|
|`docker-compose_v3_ol_pgsql_local.yaml`|The compose file locally builds the latest version of Zabbix 6.4 and runs Zabbix components on Oracle Linux with PostgreSQL database support.|
|`docker-compose_v3_ubuntu_mysql_latest.yaml`|The compose file runs the latest version of Zabbix 6.4 components on Ubuntu 20.04 with MySQL database support.|
|`docker-compose_v3_ubuntu_mysql_local.yaml`|The compose file locally builds the latest version of Zabbix 6.4 and runs Zabbix components on Ubuntu 20.04 with MySQL database support.|
|`docker-compose_v3_ubuntu_pgsql_latest.yaml`|The compose file runs the latest version of Zabbix 6.4 components on Ubuntu 20.04 with PostgreSQL database support.|
|`docker-compose_v3_ubuntu_pgsql_local.yaml`|The compose file locally builds the latest version of Zabbix 6.4 and runs Zabbix components on Ubuntu 20.04 with PostgreSQL database support.|</source>
      </trans-unit>
      <trans-unit id="52e39127" xml:space="preserve">
        <source>#### Storage

Compose files are configured to support local storage on a host machine.
Docker Compose will create a `zbx_env` directory in the folder with the compose file when you run Zabbix components using the compose file.
The directory will contain the same structure as described in the [Volumes](#Volumes) section and directory for database storage.

There are also volumes in read-only mode for `/etc/localtime` and `/etc/timezone` files.</source>
      </trans-unit>
      <trans-unit id="0be3c140" xml:space="preserve">
        <source>#### Environment variables

The variable files have the following naming structure: `.env_&lt;type of component&gt;` and are located in the *env_vars* [directory](https://github.com/zabbix/zabbix-docker/tree/6.4/env_vars). See [environment variables](#environment-variables) for details about variable naming and available selection.</source>
      </trans-unit>
      <trans-unit id="ad55959c" xml:space="preserve">
        <source>#### Examples

**Example 1**

    git checkout 6.4
    docker compose -f ./docker-compose_v3_alpine_mysql_latest.yaml up -d

The command will download the latest Zabbix 6.4 images for each Zabbix component and run them in detach mode.

::: noteimportant
Do not forget to download `.env_&lt;type of component&gt;` files from github.com official Zabbix repository with compose files.
:::

**Example 2**

    git checkout 6.4
    docker compose -f ./docker-compose_v3_ubuntu_mysql_local.yaml up -d

The command will download base image Ubuntu 22.04 (jammy), then build Zabbix 6.4 components locally and run them in detach mode.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
