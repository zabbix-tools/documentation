[comment]: # translation:outdated

[comment]: # ({new-7ef4b2b9})
# Bonnes pratiques pour la configuration sécurisée de Zabbix

[comment]: # ({/new-7ef4b2b9})

[comment]: # ({new-b9719f19})
#### Aperçu

Cette section contient les bonnes pratiques à suivre pour configurer
Zabbix de manière sécurisée.

Les pratiques expliquées ici ne sont pas obligatoire pour faire
fonctionner Zabbix. Elles sont recommandées pour une meilleure sécurité
du système.

[comment]: # ({/new-b9719f19})

[comment]: # ({new-cebf8306})
### Access control

[comment]: # ({/new-cebf8306})

[comment]: # ({new-dc572a02})
#### Utilisateur sécurisé pour l'agent Zabbix

Dans la configuration par défaut, les processus du serveur et de l'agent
Zabbix partage le même utilisateur 'zabbix'. Si vous voulez être sûr que
l'agent ne puisse pas accéder à certains détails sensibles de la
configuration du serveur (ex : informations de connexion à base de
données), l’agent devrait s’exécuter avec un utilisateur différent :

1.  Créer un utilisateur sécurisé
2.  Indiquer cet utilisateur dans le
    \[\[:manual/appendix/config/zabbix\_agentd|fichier de
    configuration\] de l'agent (paramètre : ’User’).
3.  Redémarrer l'agent avec des droits administrateurs. Les droits
    seront transférés à l'utilisateur indiqué.

[comment]: # ({/new-dc572a02})

[comment]: # ({new-1631be73})
#### Mettre en place le SSL pour l'interface Web

Sur RHEL/Centos, installez le package mod\_ssl :

    yum install mod_ssl

Créez un repertoire pour les clés SSL :

    mkdir /etc/httpd/ssl

Ajoutez les paramètres SSL :

    Country Name (2 letter code) [XX]:
    State or Province Name (full name) []:
    Locality Name (eg, city) [Default City]:
    Organization Name (eg, company) [Default Company Ltd]:
    Organizational Unit Name (eg, section) []:
    Common Name (eg, your name or your server's hostname) []:localhost
    Email Address []:

Éditez la configuration SSL Apache :

    /etc/httpd/conf.d/ssl.conf

    DocumentRoot "/usr/share/zabbix"
    ServerName localhost:443
    SSLCertificateFile /etc/httpd/ssl/apache.crt
    SSLCertificateKeyFile /etc/httpd/ssl/apache.key

Redémarrez le service Apache pour appliquer les changements :

    systemctl restart httpd.service

[comment]: # ({/new-1631be73})

[comment]: # ({new-40e70f9e})
#### Activation de Zabbix dans le répertoire racine de l'URL

Ajoutez un hôte virtuel à la configuration Apache et définissez une
redirection permanente pour le document racine vers l'URL sécurisée de
Zabbix. Remplacez *localhost* par le nom réel du serveur.

    /etc/httpd/conf/httpd.conf

    #Add lines

    <VirtualHost *:*>
        ServerName localhost
        Redirect permanent / http://localhost
    </VirtualHost>

Redémarrez le service Apache pour appliquer les modifications :

    systemctl restart httpd.service

[comment]: # ({/new-40e70f9e})

[comment]: # ({new-62eeb2ec})
### Windows installer paths
When using Windows installers, it is recommended to use default paths provided by the installer as using custom paths without proper permissions could compromise the security of the installation.

[comment]: # ({/new-62eeb2ec})

[comment]: # ({new-f8f5054e})
#### Désactiver l'exposition des informations du serveur Web

Il est recommandé de désactiver toutes les signatures de serveur Web
dans le cadre du processus de renforcement du serveur Web. Le serveur
Web expose la signature logicielle par défaut :

![](../../../../assets/en/manual/installation/requirements/software_signature.png)

La signature peut être désactivée en ajoutant deux lignes au fichier de
configuration Apache (utilisé comme exemple) :

    ServerSignature Off
    ServerTokens Prod

La signature PHP (X-Powered-By HTTP header) peut être désactivée en
changeant le fichier de configuration php.ini (la signature est
désactivée par défaut) :

    expose_php = Off

Le redémarrage du serveur Web est obligatoire pour qur les changements
dans le fichier de configuration soient appliqués.

Un niveau de sécurité supplémentaire peut être atteint en utilisant le
mod\_security (package libapache2-mod-security2) avec Apache.
mod\_security permet de supprimer la signature du serveur au lieu de
supprimer uniquement la version de la signature du serveur. La signature
peut être modifiée en n'importe quelle valeur en changeant
"SecServerSignature" après l'installation de mod\_security.

Reportez-vous à la documentation de votre serveur Web pour obtenir de
l'aide sur la suppression/modification des signatures logicielles.

[comment]: # ({/new-f8f5054e})

[comment]: # ({new-40cfdec2})
### Web server hardening

[comment]: # ({/new-40cfdec2})

[comment]: # ({new-bb3706f4})
#### Désactiver les pages d'erreurs par défaut du serveur web

Il est recommandé de désactiver les pages d'erreur par défaut pour
éviter toute exposition d'informations. Le serveur Web utilise par
défaut les pages d'erreur intégrées :

![](../../../../assets/en/manual/installation/requirements/error_page.png)

Les pages d'erreur par défaut doivent être remplacées/supprimées dans le
cadre du processus de renforcement du serveur Web. La directive
"ErrorDocument" peut être utilisée pour définir une page/texte d'erreur
personnalisé pour le serveur web Apache (utilisé comme exemple).

Reportez-vous à la documentation de votre serveur Web pour obtenir de
l'aide sur la façon de remplacer/supprimer les pages d'erreur par
défaut.

[comment]: # ({/new-bb3706f4})

[comment]: # ({new-c7ee0bb2})
#### Suppression de la page de test du serveur web

Il est recommandé de supprimer la page de test du serveur Web pour
éviter toute exposition d'informations. Par défaut, le webroot du
serveur web contient une page de test appelée index.html (Apache2 sur
Ubuntu est utilisé comme exemple) :

![](../../../../assets/en/manual/installation/requirements/test_page.png)

La page de test doit être supprimée ou doit être rendue indisponible
dans le cadre du processus de renforcement du serveur Web.

[comment]: # ({/new-c7ee0bb2})

[comment]: # ({new-cd09dcd1})
#### Disabling web server information exposure

It is recommended to disable all web server signatures as part of the
web server hardening process. The web server is exposing software
signature by default:

![](../../../../assets/en/manual/installation/requirements/software_signature.png)

The signature can be disabled by adding two lines to the Apache (used as
an example) configuration file:

    ServerSignature Off
    ServerTokens Prod

PHP signature (X-Powered-By HTTP header) can be disabled by changing the
php.ini configuration file (signature is disabled by default):

    expose_php = Off

Web server restart is required for configuration file changes to be
applied.

Additional security level can be achieved by using the mod\_security
(package libapache2-mod-security2) with Apache. mod\_security allows to
remove server signature instead of only removing version from server
signature. Signature can be altered to any value by changing
"SecServerSignature" to any desired value after installing
mod\_security.

Please refer to documentation of your web server to find help on how to
remove/change software signatures.

[comment]: # ({/new-cd09dcd1})

[comment]: # ({new-720052da})
#### Disabling default web server error pages

It is recommended to disable default error pages to avoid information
exposure. Web server is using built-in error pages by default:

![](../../../../assets/en/manual/installation/requirements/error_page_text.png)

Default error pages should be replaced/removed as part of the web server
hardening process. The "ErrorDocument" directive can be used to define a
custom error page/text for Apache web server (used as an example).

Please refer to documentation of your web server to find help on how to
replace/remove default error pages.

[comment]: # ({/new-720052da})

[comment]: # ({new-ba1547c0})
#### Removing web server test page

It is recommended to remove the web server test page to avoid
information exposure. By default, web server webroot contains a test
page called index.html (Apache2 on Ubuntu is used as an example):

![](../../../../assets/en/manual/installation/requirements/test_page.png)

The test page should be removed or should be made unavailable as part of
the web server hardening process.

[comment]: # ({/new-ba1547c0})

[comment]: # ({new-7e4e4a45})
#### Zabbix settings

By default, Zabbix is configured with *X-Frame-Options HTTP response
header* set to `SAMEORIGIN`, meaning that content can only be loaded in
a frame that has the same origin as the page itself.

Zabbix frontend elements that pull content from external URLs (namely,
the URL [dashboard
widget](/manual/web_interface/frontend_sections/monitoring/dashboard/widgets#url))
display retrieved content in a sandbox with all sandboxing restrictions
enabled.

These settings enhance the security of the Zabbix frontend and provide
protection against XSS and clickjacking attacks. Super Admins can
[modify](/manual/web_interface/frontend_sections/administration/general#security)
*iframe sandboxing* and *X-Frame-Options HTTP response header*
parameters as needed. Please carefully weigh the risks and benefits
before changing default settings. Turning sandboxing or X-Frame-Options
off completely is not recommended.

[comment]: # ({/new-7e4e4a45})

[comment]: # ({new-c02c850f})
#### Zabbix Windows agent with OpenSSL

Zabbix Windows agent compiled with OpenSSL will try to reach the SSL
configuration file in c:\\openssl-64bit. The "openssl-64bit" directory
on disk C: can be created by non-privileged users.

So for security hardening, it is required to create this directory
manually and revoke write access from non-admin users.

Please note that the directory names will be different on 32-bit and
64-bit versions of Windows.

[comment]: # ({/new-c02c850f})

[comment]: # ({new-6cfe5a3b})
### Cryptography

[comment]: # ({/new-6cfe5a3b})

[comment]: # ({new-3f76ae70})
#### Hiding the file with list of common passwords

To increase the complexity of password brute force attacks, it is
suggested to limit access to the file `ui/data/top_passwords.txt` by
modifying web server configuration. This file contains a list of the
most common and context-specific passwords, and is used to prevent users
from setting such passwords if *Avoid easy-to-guess passwords* parameter
is enabled in the [password
policy](/manual/web_interface/frontend_sections/administration/authentication#internal_authentication).

For example, on NGINX file access can be limited by using the `location`
directive:

    location = /data/top_passwords.txt {
        deny all;
        return 404;
    }

On Apache - by using `.htacess` file:

    <Files "top_passwords.txt">  
      Order Allow,Deny
      Deny from all
    </Files>



[comment]: # ({/new-3f76ae70})

[comment]: # ({new-ba0d4036})

#### Security vulnerabilities

##### CVE-2021-42550

In Zabbix Java gateway with logback version 1.2.7 and prior versions, an attacker with the required 
privileges to edit configuration files could craft a malicious configuration allowing to execute 
arbitrary code loaded from LDAP servers.

Vulnerability to [CVE-2021-42550](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-42550) 
has been fixed since Zabbix 5.4.9. However, as an additional security measure it is recommended 
to check permissions to the `/etc/zabbix/zabbix_java_gateway_logback.xml` file and set it read-only, 
if write permissions are available for the "zabbix" user. 

[comment]: # ({/new-ba0d4036})
