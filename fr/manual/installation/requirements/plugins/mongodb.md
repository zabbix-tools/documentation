[comment]: # translation:outdated

[comment]: # ({new-c01fc2f1})
# 2 MongoDB plugin dependencies

[comment]: # ({/new-c01fc2f1})

[comment]: # ({new-52a311ab})
#### Overview

The required libraries for the MongoDB loadable plugin are listed in this page.

[comment]: # ({/new-52a311ab})

[comment]: # ({new-a3fefda0})

#### Golang libraries

|Requirement|Mandatory status|Minimum version|Description|
|--|-|-|-------|
|[git.zabbix.com/ap/plugin-support](https://git.zabbix.com/projects/AP/repos/plugin-support)|Yes|1.X.X|Zabbix own support library. Mostly for plugins. |
|[go.mongodb.org/mongo-driver](https://go.mongodb.org/mongo-driver)|^|1.7.6|Named read/write locks, access sync. |
|[github.com/go-stack/stack](https://github.com/go-stack/stack)|Indirect^**1**^|1.8.0|Required package for MongoDB plugin mongo-driver lib. |
|[github.com/golang/snappy](https://github.com/golang/snappy)|^|0.0.1|Required package for MongoDB plugin mongo-driver lib. |
|[github.com/klauspost/compress](https://github.com/klauspost/compress)|^|1.13.6|Required package for MongoDB plugin mongo-driver lib. |
|[github.com/natefinch/npipe](https://github.com/natefinch/npipe)|^|0.0.0|Required package for MongoDB plugin mongo-driver lib. |
|[github.com/pkg/errors](https://github.com/pkg/errors)|^|0.9.1|Required package for MongoDB plugin mongo-driver lib. |
|[github.com/xdg-go/pbkdf2](https://github.com/xdg-go/pbkdf2)|^|1.0.0|Required package for MongoDB plugin mongo-driver lib. |
|[github.com/xdg-go/scram](https://github.com/xdg-go/scram)|^|1.0.2|Required package for MongoDB plugin mongo-driver lib. |
|[github.com/xdg-go/stringprep](https://github.com/xdg-go/stringprep)|^|1.0.2|Required package for MongoDB plugin mongo-driver lib. |
|[github.com/youmark/pkcs8](https://github.com/youmark/pkcs8)|^|0.0.0|Required package for MongoDB plugin mongo-driver lib. |
|[golang.org/x/crypto](https://golang.org/x/crypto)|^|0.0.0|Required package for MongoDB plugin mongo-driver lib. |
|[golang.org/x/sync](https://golang.org/x/sync)|^|0.0.0|Required package for MongoDB plugin mongo-driver lib. |
|[golang.org/x/sys](https://golang.org/x/sys)|^|0.0.0|Required package for MongoDB plugin mongo-driver lib. |
|[golang.org/x/text](https://golang.org/x/text)|^|0.3.7|Required package for MongoDB plugin mongo-driver lib. |

^**1**^ "Indirect" means that it is used in one of the libraries that the agent uses. It's required since Zabbix uses the library that uses the package.

[comment]: # ({/new-a3fefda0})
